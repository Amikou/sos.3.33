package defpackage;

import android.view.View;
import android.widget.TextView;
import com.google.android.material.button.MaterialButton;
import com.google.android.material.card.MaterialCardView;
import com.google.android.material.imageview.ShapeableImageView;
import com.google.android.material.textview.MaterialTextView;
import net.safemoon.androidwallet.R;
import net.safemoon.androidwallet.views.OtpEditText;

/* compiled from: FragmentGoogle2faVerfiedAuthBinding.java */
/* renamed from: fa1  reason: default package */
/* loaded from: classes2.dex */
public final class fa1 {
    public final MaterialButton a;
    public final MaterialButton b;
    public final MaterialButton c;
    public final OtpEditText d;
    public final TextView e;
    public final MaterialTextView f;

    public fa1(MaterialCardView materialCardView, MaterialButton materialButton, MaterialButton materialButton2, MaterialButton materialButton3, OtpEditText otpEditText, TextView textView, ShapeableImageView shapeableImageView, MaterialTextView materialTextView, MaterialTextView materialTextView2) {
        this.a = materialButton;
        this.b = materialButton2;
        this.c = materialButton3;
        this.d = otpEditText;
        this.e = textView;
        this.f = materialTextView2;
    }

    public static fa1 a(View view) {
        int i = R.id.btnContinue;
        MaterialButton materialButton = (MaterialButton) ai4.a(view, R.id.btnContinue);
        if (materialButton != null) {
            i = R.id.btnPasteCode;
            MaterialButton materialButton2 = (MaterialButton) ai4.a(view, R.id.btnPasteCode);
            if (materialButton2 != null) {
                i = R.id.dialog_cross;
                MaterialButton materialButton3 = (MaterialButton) ai4.a(view, R.id.dialog_cross);
                if (materialButton3 != null) {
                    i = R.id.edtOTP;
                    OtpEditText otpEditText = (OtpEditText) ai4.a(view, R.id.edtOTP);
                    if (otpEditText != null) {
                        i = R.id.errorMessage;
                        TextView textView = (TextView) ai4.a(view, R.id.errorMessage);
                        if (textView != null) {
                            i = R.id.imgAuthIcon;
                            ShapeableImageView shapeableImageView = (ShapeableImageView) ai4.a(view, R.id.imgAuthIcon);
                            if (shapeableImageView != null) {
                                i = R.id.txtAuthEnableTitle;
                                MaterialTextView materialTextView = (MaterialTextView) ai4.a(view, R.id.txtAuthEnableTitle);
                                if (materialTextView != null) {
                                    i = R.id.txtNote;
                                    MaterialTextView materialTextView2 = (MaterialTextView) ai4.a(view, R.id.txtNote);
                                    if (materialTextView2 != null) {
                                        return new fa1((MaterialCardView) view, materialButton, materialButton2, materialButton3, otpEditText, textView, shapeableImageView, materialTextView, materialTextView2);
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
        throw new NullPointerException("Missing required view with ID: ".concat(view.getResources().getResourceName(i)));
    }
}
