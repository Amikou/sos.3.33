package defpackage;

/* compiled from: Channel.kt */
/* renamed from: uj3  reason: default package */
/* loaded from: classes2.dex */
public interface uj3<E> {

    /* compiled from: Channel.kt */
    /* renamed from: uj3$a */
    /* loaded from: classes2.dex */
    public static final class a {
        public static /* synthetic */ boolean a(uj3 uj3Var, Throwable th, int i, Object obj) {
            if (obj == null) {
                if ((i & 1) != 0) {
                    th = null;
                }
                return uj3Var.l(th);
            }
            throw new UnsupportedOperationException("Super calls with default arguments not supported in this target, function: close");
        }

        /* JADX WARN: Multi-variable type inference failed */
        public static <E> boolean b(uj3<? super E> uj3Var, E e) {
            Object p = uj3Var.p(e);
            if (tx.i(p)) {
                return true;
            }
            Throwable e2 = tx.e(p);
            if (e2 == null) {
                return false;
            }
            throw hs3.k(e2);
        }
    }

    Object h(E e, q70<? super te4> q70Var);

    boolean l(Throwable th);

    boolean offer(E e);

    Object p(E e);
}
