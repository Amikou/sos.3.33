package defpackage;

import androidx.paging.LoadType;
import defpackage.w02;
import java.util.List;

/* compiled from: PageEvent.kt */
/* renamed from: yo2  reason: default package */
/* loaded from: classes.dex */
public abstract class yo2<T> {

    /* compiled from: PageEvent.kt */
    /* renamed from: yo2$a */
    /* loaded from: classes.dex */
    public static final class a<T> extends yo2<T> {
        public final LoadType a;
        public final int b;
        public final int c;
        public final int d;

        /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
        public a(LoadType loadType, int i, int i2, int i3) {
            super(null);
            fs1.f(loadType, "loadType");
            this.a = loadType;
            this.b = i;
            this.c = i2;
            this.d = i3;
            if (loadType != LoadType.REFRESH) {
                if (!(d() > 0)) {
                    throw new IllegalArgumentException(("Drop count must be > 0, but was " + d()).toString());
                }
                if (i3 >= 0) {
                    return;
                }
                throw new IllegalArgumentException(("Invalid placeholdersRemaining " + i3).toString());
            }
            throw new IllegalArgumentException("Drop load type must be PREPEND or APPEND".toString());
        }

        public final LoadType a() {
            return this.a;
        }

        public final int b() {
            return this.c;
        }

        public final int c() {
            return this.b;
        }

        public final int d() {
            return (this.c - this.b) + 1;
        }

        public final int e() {
            return this.d;
        }

        public boolean equals(Object obj) {
            if (this != obj) {
                if (obj instanceof a) {
                    a aVar = (a) obj;
                    return fs1.b(this.a, aVar.a) && this.b == aVar.b && this.c == aVar.c && this.d == aVar.d;
                }
                return false;
            }
            return true;
        }

        public int hashCode() {
            LoadType loadType = this.a;
            return ((((((loadType != null ? loadType.hashCode() : 0) * 31) + this.b) * 31) + this.c) * 31) + this.d;
        }

        public String toString() {
            return "Drop(loadType=" + this.a + ", minPageOffset=" + this.b + ", maxPageOffset=" + this.c + ", placeholdersRemaining=" + this.d + ")";
        }
    }

    /* compiled from: PageEvent.kt */
    /* renamed from: yo2$b */
    /* loaded from: classes.dex */
    public static final class b<T> extends yo2<T> {
        public static final b<Object> f;
        public static final a g;
        public final LoadType a;
        public final List<xa4<T>> b;
        public final int c;
        public final int d;
        public final a30 e;

        /* compiled from: PageEvent.kt */
        /* renamed from: yo2$b$a */
        /* loaded from: classes.dex */
        public static final class a {
            public a() {
            }

            public final <T> b<T> a(List<xa4<T>> list, int i, a30 a30Var) {
                fs1.f(list, "pages");
                fs1.f(a30Var, "combinedLoadStates");
                return new b<>(LoadType.APPEND, list, -1, i, a30Var, null);
            }

            public final <T> b<T> b(List<xa4<T>> list, int i, a30 a30Var) {
                fs1.f(list, "pages");
                fs1.f(a30Var, "combinedLoadStates");
                return new b<>(LoadType.PREPEND, list, i, -1, a30Var, null);
            }

            public final <T> b<T> c(List<xa4<T>> list, int i, int i2, a30 a30Var) {
                fs1.f(list, "pages");
                fs1.f(a30Var, "combinedLoadStates");
                return new b<>(LoadType.REFRESH, list, i, i2, a30Var, null);
            }

            public final b<Object> d() {
                return b.f;
            }

            public /* synthetic */ a(qi0 qi0Var) {
                this();
            }
        }

        static {
            a aVar = new a(null);
            g = aVar;
            List<xa4<T>> b = a20.b(xa4.f.a());
            w02.c.a aVar2 = w02.c.d;
            f = aVar.c(b, 0, 0, new a30(aVar2.b(), aVar2.a(), aVar2.a(), new x02(aVar2.b(), aVar2.a(), aVar2.a()), null, 16, null));
        }

        public /* synthetic */ b(LoadType loadType, List list, int i, int i2, a30 a30Var, qi0 qi0Var) {
            this(loadType, list, i, i2, a30Var);
        }

        public static /* synthetic */ b c(b bVar, LoadType loadType, List list, int i, int i2, a30 a30Var, int i3, Object obj) {
            if ((i3 & 1) != 0) {
                loadType = bVar.a;
            }
            List<xa4<T>> list2 = list;
            if ((i3 & 2) != 0) {
                list2 = bVar.b;
            }
            List list3 = list2;
            if ((i3 & 4) != 0) {
                i = bVar.c;
            }
            int i4 = i;
            if ((i3 & 8) != 0) {
                i2 = bVar.d;
            }
            int i5 = i2;
            if ((i3 & 16) != 0) {
                a30Var = bVar.e;
            }
            return bVar.b(loadType, list3, i4, i5, a30Var);
        }

        public final b<T> b(LoadType loadType, List<xa4<T>> list, int i, int i2, a30 a30Var) {
            fs1.f(loadType, "loadType");
            fs1.f(list, "pages");
            fs1.f(a30Var, "combinedLoadStates");
            return new b<>(loadType, list, i, i2, a30Var);
        }

        public final a30 d() {
            return this.e;
        }

        public final LoadType e() {
            return this.a;
        }

        public boolean equals(Object obj) {
            if (this != obj) {
                if (obj instanceof b) {
                    b bVar = (b) obj;
                    return fs1.b(this.a, bVar.a) && fs1.b(this.b, bVar.b) && this.c == bVar.c && this.d == bVar.d && fs1.b(this.e, bVar.e);
                }
                return false;
            }
            return true;
        }

        public final List<xa4<T>> f() {
            return this.b;
        }

        public final int g() {
            return this.d;
        }

        public final int h() {
            return this.c;
        }

        public int hashCode() {
            LoadType loadType = this.a;
            int hashCode = (loadType != null ? loadType.hashCode() : 0) * 31;
            List<xa4<T>> list = this.b;
            int hashCode2 = (((((hashCode + (list != null ? list.hashCode() : 0)) * 31) + this.c) * 31) + this.d) * 31;
            a30 a30Var = this.e;
            return hashCode2 + (a30Var != null ? a30Var.hashCode() : 0);
        }

        public String toString() {
            return "Insert(loadType=" + this.a + ", pages=" + this.b + ", placeholdersBefore=" + this.c + ", placeholdersAfter=" + this.d + ", combinedLoadStates=" + this.e + ")";
        }

        public b(LoadType loadType, List<xa4<T>> list, int i, int i2, a30 a30Var) {
            super(null);
            this.a = loadType;
            this.b = list;
            this.c = i;
            this.d = i2;
            this.e = a30Var;
            boolean z = false;
            if (loadType == LoadType.APPEND || i >= 0) {
                if (loadType == LoadType.PREPEND || i2 >= 0) {
                    if (!((loadType != LoadType.REFRESH || (list.isEmpty() ^ true)) ? true : z)) {
                        throw new IllegalArgumentException("Cannot create a REFRESH Insert event with no TransformablePages as this could permanently stall pagination. Note that this check does not prevent empty LoadResults and is instead usually an indication of an internal error in Paging itself.".toString());
                    }
                    return;
                }
                throw new IllegalArgumentException(("Append insert defining placeholdersAfter must be > 0, but was " + i2).toString());
            }
            throw new IllegalArgumentException(("Prepend insert defining placeholdersBefore must be > 0, but was " + i).toString());
        }
    }

    /* compiled from: PageEvent.kt */
    /* renamed from: yo2$c */
    /* loaded from: classes.dex */
    public static final class c<T> extends yo2<T> {
        public static final a d = new a(null);
        public final LoadType a;
        public final boolean b;
        public final w02 c;

        /* compiled from: PageEvent.kt */
        /* renamed from: yo2$c$a */
        /* loaded from: classes.dex */
        public static final class a {
            public a() {
            }

            public final boolean a(w02 w02Var, boolean z) {
                fs1.f(w02Var, "loadState");
                return (w02Var instanceof w02.b) || (w02Var instanceof w02.a) || z;
            }

            public /* synthetic */ a(qi0 qi0Var) {
                this();
            }
        }

        /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
        public c(LoadType loadType, boolean z, w02 w02Var) {
            super(null);
            fs1.f(loadType, "loadType");
            fs1.f(w02Var, "loadState");
            this.a = loadType;
            this.b = z;
            this.c = w02Var;
            if ((loadType == LoadType.REFRESH && !z && (w02Var instanceof w02.c) && w02Var.a()) ? false : true) {
                if (!d.a(w02Var, z)) {
                    throw new IllegalArgumentException("LoadStateUpdates cannot be used to dispatch NotLoading unless it is from remote mediator and remote mediator reached end of pagination.".toString());
                }
                return;
            }
            throw new IllegalArgumentException("LoadStateUpdate for local REFRESH may not set endOfPaginationReached = true".toString());
        }

        public final boolean a() {
            return this.b;
        }

        public final w02 b() {
            return this.c;
        }

        public final LoadType c() {
            return this.a;
        }

        public boolean equals(Object obj) {
            if (this != obj) {
                if (obj instanceof c) {
                    c cVar = (c) obj;
                    return fs1.b(this.a, cVar.a) && this.b == cVar.b && fs1.b(this.c, cVar.c);
                }
                return false;
            }
            return true;
        }

        /* JADX WARN: Multi-variable type inference failed */
        public int hashCode() {
            LoadType loadType = this.a;
            int hashCode = (loadType != null ? loadType.hashCode() : 0) * 31;
            boolean z = this.b;
            int i = z;
            if (z != 0) {
                i = 1;
            }
            int i2 = (hashCode + i) * 31;
            w02 w02Var = this.c;
            return i2 + (w02Var != null ? w02Var.hashCode() : 0);
        }

        public String toString() {
            return "LoadStateUpdate(loadType=" + this.a + ", fromMediator=" + this.b + ", loadState=" + this.c + ")";
        }
    }

    public yo2() {
    }

    public /* synthetic */ yo2(qi0 qi0Var) {
        this();
    }
}
