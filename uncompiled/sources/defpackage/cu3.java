package defpackage;

import androidx.media3.common.j;
import androidx.media3.common.util.b;
import com.google.common.collect.ImmutableList;

/* compiled from: StreamFormatChunk.java */
/* renamed from: cu3  reason: default package */
/* loaded from: classes.dex */
public final class cu3 implements sl {
    public final j a;

    public cu3(j jVar) {
        this.a = jVar;
    }

    public static String a(int i) {
        switch (i) {
            case 808802372:
            case 877677894:
            case 1145656883:
            case 1145656920:
            case 1482049860:
            case 1684633208:
            case 2021026148:
                return "video/mp4v-es";
            case 826496577:
            case 828601953:
            case 875967048:
                return "video/avc";
            case 842289229:
                return "video/mp42";
            case 859066445:
                return "video/mp43";
            case 1196444237:
            case 1735420525:
                return "video/mjpeg";
            default:
                return null;
        }
    }

    public static String b(int i) {
        if (i != 1) {
            if (i != 85) {
                if (i != 255) {
                    if (i != 8192) {
                        if (i != 8193) {
                            return null;
                        }
                        return "audio/vnd.dts";
                    }
                    return "audio/ac3";
                }
                return "audio/mp4a-latm";
            }
            return "audio/mpeg";
        }
        return "audio/raw";
    }

    public static sl c(op2 op2Var) {
        op2Var.Q(4);
        int q = op2Var.q();
        int q2 = op2Var.q();
        op2Var.Q(4);
        int q3 = op2Var.q();
        String a = a(q3);
        if (a == null) {
            p12.i("StreamFormatChunk", "Ignoring track with unsupported compression " + q3);
            return null;
        }
        j.b bVar = new j.b();
        bVar.j0(q).Q(q2).e0(a);
        return new cu3(bVar.E());
    }

    public static sl d(int i, op2 op2Var) {
        if (i == 2) {
            return c(op2Var);
        }
        if (i == 1) {
            return e(op2Var);
        }
        p12.i("StreamFormatChunk", "Ignoring strf box for unsupported track type: " + b.i0(i));
        return null;
    }

    public static sl e(op2 op2Var) {
        int v = op2Var.v();
        String b = b(v);
        if (b == null) {
            p12.i("StreamFormatChunk", "Ignoring track with unsupported format tag " + v);
            return null;
        }
        int v2 = op2Var.v();
        int q = op2Var.q();
        op2Var.Q(6);
        int Y = b.Y(op2Var.J());
        int v3 = op2Var.v();
        byte[] bArr = new byte[v3];
        op2Var.j(bArr, 0, v3);
        j.b bVar = new j.b();
        bVar.e0(b).H(v2).f0(q);
        if ("audio/raw".equals(b) && Y != 0) {
            bVar.Y(Y);
        }
        if ("audio/mp4a-latm".equals(b) && v3 > 0) {
            bVar.T(ImmutableList.of(bArr));
        }
        return new cu3(bVar.E());
    }

    @Override // defpackage.sl
    public int getType() {
        return 1718776947;
    }
}
