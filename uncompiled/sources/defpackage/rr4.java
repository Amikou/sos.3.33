package defpackage;

import org.bouncycastle.asn1.h;
import org.bouncycastle.asn1.j0;
import org.bouncycastle.asn1.k;

/* renamed from: rr4  reason: default package */
/* loaded from: classes2.dex */
public class rr4 extends h {
    public final f4 a;
    public xs0 f0;
    public pt0 g0;

    public rr4(pt0 pt0Var) {
        this(pt0Var, false);
    }

    public rr4(pt0 pt0Var, boolean z) {
        this.g0 = pt0Var.A();
        this.a = new j0(pt0Var.l(z));
    }

    public rr4(xs0 xs0Var, f4 f4Var) {
        this(xs0Var, f4Var.D());
    }

    public rr4(xs0 xs0Var, byte[] bArr) {
        this.f0 = xs0Var;
        this.a = new j0(wh.e(bArr));
    }

    @Override // org.bouncycastle.asn1.h, defpackage.c4
    public k i() {
        return this.a;
    }

    public synchronized pt0 o() {
        if (this.g0 == null) {
            this.g0 = this.f0.k(this.a.D()).A();
        }
        return this.g0;
    }
}
