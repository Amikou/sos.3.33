package defpackage;

import android.annotation.TargetApi;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Build;
import android.util.Log;
import java.io.File;
import java.util.Arrays;
import java.util.concurrent.atomic.AtomicBoolean;

/* compiled from: HardwareConfigState.java */
/* renamed from: vj1  reason: default package */
/* loaded from: classes.dex */
public final class vj1 {
    public static final boolean g;
    public static final boolean h;
    public static final File i;
    public static volatile vj1 j;
    public static volatile int k;
    public final int b;
    public final int c;
    public int d;
    public boolean e = true;
    public final AtomicBoolean f = new AtomicBoolean(false);
    public final boolean a = f();

    static {
        int i2 = Build.VERSION.SDK_INT;
        g = i2 < 29;
        h = i2 >= 26;
        i = new File("/proc/self/fd");
        k = -1;
    }

    public vj1() {
        if (Build.VERSION.SDK_INT >= 28) {
            this.b = 20000;
            this.c = 0;
            return;
        }
        this.b = 700;
        this.c = 128;
    }

    public static vj1 b() {
        if (j == null) {
            synchronized (vj1.class) {
                if (j == null) {
                    j = new vj1();
                }
            }
        }
        return j;
    }

    public static boolean f() {
        return (g() || h()) ? false : true;
    }

    public static boolean g() {
        if (Build.VERSION.SDK_INT != 26) {
            return false;
        }
        for (String str : Arrays.asList("SC-04J", "SM-N935", "SM-J720", "SM-G570F", "SM-G570M", "SM-G960", "SM-G965", "SM-G935", "SM-G930", "SM-A520", "SM-A720F", "moto e5", "moto e5 play", "moto e5 plus", "moto e5 cruise", "moto g(6) forge", "moto g(6) play")) {
            if (Build.MODEL.startsWith(str)) {
                return true;
            }
        }
        return false;
    }

    public static boolean h() {
        if (Build.VERSION.SDK_INT != 27) {
            return false;
        }
        return Arrays.asList("LG-M250", "LG-M320", "LG-Q710AL", "LG-Q710PL", "LGM-K121K", "LGM-K121L", "LGM-K121S", "LGM-X320K", "LGM-X320L", "LGM-X320S", "LGM-X401L", "LGM-X401S", "LM-Q610.FG", "LM-Q610.FGN", "LM-Q617.FG", "LM-Q617.FGN", "LM-Q710.FG", "LM-Q710.FGN", "LM-X220PM", "LM-X220QMA", "LM-X410PM").contains(Build.MODEL);
    }

    public final boolean a() {
        return g && !this.f.get();
    }

    public final int c() {
        if (k != -1) {
            return k;
        }
        return this.b;
    }

    public final synchronized boolean d() {
        boolean z = true;
        int i2 = this.d + 1;
        this.d = i2;
        if (i2 >= 50) {
            this.d = 0;
            int length = i.list().length;
            long c = c();
            if (length >= c) {
                z = false;
            }
            this.e = z;
            if (!z && Log.isLoggable("Downsampler", 5)) {
                StringBuilder sb = new StringBuilder();
                sb.append("Excluding HARDWARE bitmap config because we're over the file descriptor limit, file descriptors ");
                sb.append(length);
                sb.append(", limit ");
                sb.append(c);
            }
        }
        return this.e;
    }

    public boolean e(int i2, int i3, boolean z, boolean z2) {
        int i4;
        return z && this.a && h && !a() && !z2 && i2 >= (i4 = this.c) && i3 >= i4 && d();
    }

    @TargetApi(26)
    public boolean i(int i2, int i3, BitmapFactory.Options options, boolean z, boolean z2) {
        boolean e = e(i2, i3, z, z2);
        if (e) {
            options.inPreferredConfig = Bitmap.Config.HARDWARE;
            options.inMutable = false;
        }
        return e;
    }
}
