package defpackage;

import android.os.RemoteException;
import com.google.android.gms.internal.measurement.m;
import com.google.android.gms.measurement.internal.d;
import com.google.android.gms.measurement.internal.p;
import com.google.android.gms.measurement.internal.zzas;

/* compiled from: com.google.android.gms:play-services-measurement-impl@@19.0.0 */
/* renamed from: nr5  reason: default package */
/* loaded from: classes.dex */
public final class nr5 implements Runnable {
    public final /* synthetic */ zzas a;
    public final /* synthetic */ String f0;
    public final /* synthetic */ m g0;
    public final /* synthetic */ p h0;

    public nr5(p pVar, zzas zzasVar, String str, m mVar) {
        this.h0 = pVar;
        this.a = zzasVar;
        this.f0 = str;
        this.g0 = mVar;
    }

    @Override // java.lang.Runnable
    public final void run() {
        ck5 ck5Var;
        d dVar;
        byte[] bArr = null;
        try {
            try {
                dVar = this.h0.d;
                if (dVar == null) {
                    this.h0.a.w().l().a("Discarding data. Failed to send event to service to bundle");
                    ck5Var = this.h0.a;
                } else {
                    bArr = dVar.l0(this.a, this.f0);
                    this.h0.D();
                    ck5Var = this.h0.a;
                }
            } catch (RemoteException e) {
                this.h0.a.w().l().b("Failed to send event to the service to bundle", e);
                ck5Var = this.h0.a;
            }
            ck5Var.G().U(this.g0, bArr);
        } catch (Throwable th) {
            this.h0.a.G().U(this.g0, bArr);
            throw th;
        }
    }
}
