package defpackage;

import java.util.List;
import kotlin.KotlinNothingValueException;
import kotlinx.coroutines.internal.MainDispatcherFactory;

/* compiled from: MainDispatchers.kt */
/* renamed from: g32  reason: default package */
/* loaded from: classes2.dex */
public final class g32 {
    public static final boolean a = true;

    public static final a92 a(Throwable th, String str) {
        if (a) {
            return new a92(th, str);
        }
        if (th == null) {
            c();
            throw new KotlinNothingValueException();
        }
        throw th;
    }

    public static /* synthetic */ a92 b(Throwable th, String str, int i, Object obj) {
        if ((i & 1) != 0) {
            th = null;
        }
        if ((i & 2) != 0) {
            str = null;
        }
        return a(th, str);
    }

    public static final Void c() {
        throw new IllegalStateException("Module with the Main dispatcher is missing. Add dependency providing the Main dispatcher, e.g. 'kotlinx-coroutines-android' and ensure it has the same version as 'kotlinx-coroutines-core'");
    }

    public static final e32 d(MainDispatcherFactory mainDispatcherFactory, List<? extends MainDispatcherFactory> list) {
        try {
            return mainDispatcherFactory.createDispatcher(list);
        } catch (Throwable th) {
            return a(th, mainDispatcherFactory.hintOnError());
        }
    }
}
