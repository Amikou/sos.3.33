package defpackage;

import android.graphics.Bitmap;
import android.graphics.Canvas;

/* compiled from: IdentiUtility.kt */
/* renamed from: nn1  reason: default package */
/* loaded from: classes2.dex */
public final class nn1 {
    public static final nn1 a = new nn1();

    public static /* synthetic */ Bitmap b(nn1 nn1Var, int i, int i2, int i3, int i4, Object obj) {
        if ((i4 & 1) != 0) {
            i = 200;
        }
        if ((i4 & 2) != 0) {
            i2 = 200;
        }
        return nn1Var.a(i, i2, i3);
    }

    public final Bitmap a(int i, int i2, int i3) {
        Bitmap createBitmap = Bitmap.createBitmap(i, i2, Bitmap.Config.ARGB_8888);
        new ez(i, i2, i3).draw(new Canvas(createBitmap));
        fs1.e(createBitmap, "bitmap");
        return createBitmap;
    }
}
