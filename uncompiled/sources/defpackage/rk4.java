package defpackage;

import android.annotation.SuppressLint;
import android.view.View;

/* compiled from: ViewUtilsApi22.java */
/* renamed from: rk4  reason: default package */
/* loaded from: classes.dex */
public class rk4 extends qk4 {
    public static boolean j = true;

    @Override // defpackage.uk4
    @SuppressLint({"NewApi"})
    public void f(View view, int i, int i2, int i3, int i4) {
        if (j) {
            try {
                view.setLeftTopRightBottom(i, i2, i3, i4);
            } catch (NoSuchMethodError unused) {
                j = false;
            }
        }
    }
}
