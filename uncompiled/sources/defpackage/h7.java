package defpackage;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import androidx.appcompat.widget.AppCompatImageView;
import androidx.appcompat.widget.AppCompatTextView;
import androidx.appcompat.widget.LinearLayoutCompat;
import androidx.constraintlayout.widget.ConstraintLayout;
import com.google.android.material.button.MaterialButton;
import com.google.android.material.textview.MaterialTextView;
import net.safemoon.androidwallet.R;

/* compiled from: ActivityImportWalletOptionBinding.java */
/* renamed from: h7  reason: default package */
/* loaded from: classes2.dex */
public final class h7 {
    public final ConstraintLayout a;
    public final MaterialButton b;
    public final AppCompatTextView c;
    public final MaterialTextView d;
    public final MaterialTextView e;

    public h7(ConstraintLayout constraintLayout, MaterialButton materialButton, ConstraintLayout constraintLayout2, ImageView imageView, AppCompatImageView appCompatImageView, AppCompatImageView appCompatImageView2, AppCompatImageView appCompatImageView3, LinearLayoutCompat linearLayoutCompat, AppCompatTextView appCompatTextView, MaterialTextView materialTextView, MaterialTextView materialTextView2, TextView textView, TextView textView2) {
        this.a = constraintLayout;
        this.b = materialButton;
        this.c = appCompatTextView;
        this.d = materialTextView;
        this.e = materialTextView2;
    }

    public static h7 a(View view) {
        int i = R.id.btnBack;
        MaterialButton materialButton = (MaterialButton) ai4.a(view, R.id.btnBack);
        if (materialButton != null) {
            ConstraintLayout constraintLayout = (ConstraintLayout) view;
            i = R.id.imageView16;
            ImageView imageView = (ImageView) ai4.a(view, R.id.imageView16);
            if (imageView != null) {
                i = R.id.img_logo;
                AppCompatImageView appCompatImageView = (AppCompatImageView) ai4.a(view, R.id.img_logo);
                if (appCompatImageView != null) {
                    i = R.id.img_safemoon;
                    AppCompatImageView appCompatImageView2 = (AppCompatImageView) ai4.a(view, R.id.img_safemoon);
                    if (appCompatImageView2 != null) {
                        i = R.id.img_wallet;
                        AppCompatImageView appCompatImageView3 = (AppCompatImageView) ai4.a(view, R.id.img_wallet);
                        if (appCompatImageView3 != null) {
                            i = R.id.img_wallet_parent;
                            LinearLayoutCompat linearLayoutCompat = (LinearLayoutCompat) ai4.a(view, R.id.img_wallet_parent);
                            if (linearLayoutCompat != null) {
                                i = R.id.importPrivateKey;
                                AppCompatTextView appCompatTextView = (AppCompatTextView) ai4.a(view, R.id.importPrivateKey);
                                if (appCompatTextView != null) {
                                    i = R.id.openExtWallet12;
                                    MaterialTextView materialTextView = (MaterialTextView) ai4.a(view, R.id.openExtWallet12);
                                    if (materialTextView != null) {
                                        i = R.id.openExtWallet24;
                                        MaterialTextView materialTextView2 = (MaterialTextView) ai4.a(view, R.id.openExtWallet24);
                                        if (materialTextView2 != null) {
                                            i = R.id.textView;
                                            TextView textView = (TextView) ai4.a(view, R.id.textView);
                                            if (textView != null) {
                                                i = R.id.textView2;
                                                TextView textView2 = (TextView) ai4.a(view, R.id.textView2);
                                                if (textView2 != null) {
                                                    return new h7(constraintLayout, materialButton, constraintLayout, imageView, appCompatImageView, appCompatImageView2, appCompatImageView3, linearLayoutCompat, appCompatTextView, materialTextView, materialTextView2, textView, textView2);
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
        throw new NullPointerException("Missing required view with ID: ".concat(view.getResources().getResourceName(i)));
    }

    public static h7 c(LayoutInflater layoutInflater) {
        return d(layoutInflater, null, false);
    }

    public static h7 d(LayoutInflater layoutInflater, ViewGroup viewGroup, boolean z) {
        View inflate = layoutInflater.inflate(R.layout.activity_import_wallet_option, viewGroup, false);
        if (z) {
            viewGroup.addView(inflate);
        }
        return a(inflate);
    }

    public ConstraintLayout b() {
        return this.a;
    }
}
