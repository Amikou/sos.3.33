package defpackage;

import android.content.Context;
import android.content.Intent;

/* renamed from: ty4  reason: default package */
/* loaded from: classes2.dex */
public final class ty4 implements ax4 {
    public final /* synthetic */ tr3 a;
    public final /* synthetic */ Intent b;
    public final /* synthetic */ Context c;
    public final /* synthetic */ yy4 d;

    public ty4(yy4 yy4Var, tr3 tr3Var, Intent intent, Context context) {
        this.d = yy4Var;
        this.a = tr3Var;
        this.b = intent;
        this.c = context;
    }

    @Override // defpackage.ax4
    public final void a() {
        r0.g.post(new vy4(this.d, this.a, 5, 0));
    }

    @Override // defpackage.ax4
    public final void b() {
        it4 it4Var;
        if (this.b.getBooleanExtra("triggered_from_app_after_verification", false)) {
            it4Var = this.d.a;
            it4Var.b("Splits copied and verified more than once.", new Object[0]);
            return;
        }
        this.b.putExtra("triggered_from_app_after_verification", true);
        this.c.sendBroadcast(this.b);
    }

    @Override // defpackage.ax4
    public final void c(int i) {
        r0.g.post(new vy4(this.d, this.a, 6, i));
    }
}
