package defpackage;

import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.res.TypedArray;
import android.graphics.drawable.AnimatedVectorDrawable;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.text.TextUtils;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import androidx.appcompat.app.a;
import androidx.fragment.app.FragmentActivity;
import androidx.lifecycle.l;

/* compiled from: FingerprintDialogFragment.java */
/* renamed from: x41  reason: default package */
/* loaded from: classes.dex */
public class x41 extends sn0 {
    public TextView A0;
    public final Handler u0 = new Handler(Looper.getMainLooper());
    public final Runnable v0 = new a();
    public androidx.biometric.e w0;
    public int x0;
    public int y0;
    public ImageView z0;

    /* compiled from: FingerprintDialogFragment.java */
    /* renamed from: x41$a */
    /* loaded from: classes.dex */
    public class a implements Runnable {
        public a() {
        }

        @Override // java.lang.Runnable
        public void run() {
            x41.this.z();
        }
    }

    /* compiled from: FingerprintDialogFragment.java */
    /* renamed from: x41$b */
    /* loaded from: classes.dex */
    public class b implements DialogInterface.OnClickListener {
        public b() {
        }

        @Override // android.content.DialogInterface.OnClickListener
        public void onClick(DialogInterface dialogInterface, int i) {
            x41.this.w0.W(true);
        }
    }

    /* compiled from: FingerprintDialogFragment.java */
    /* renamed from: x41$c */
    /* loaded from: classes.dex */
    public class c implements tl2<Integer> {
        public c() {
        }

        @Override // defpackage.tl2
        /* renamed from: a */
        public void onChanged(Integer num) {
            x41 x41Var = x41.this;
            x41Var.u0.removeCallbacks(x41Var.v0);
            x41.this.B(num.intValue());
            x41.this.C(num.intValue());
            x41 x41Var2 = x41.this;
            x41Var2.u0.postDelayed(x41Var2.v0, 2000L);
        }
    }

    /* compiled from: FingerprintDialogFragment.java */
    /* renamed from: x41$d */
    /* loaded from: classes.dex */
    public class d implements tl2<CharSequence> {
        public d() {
        }

        @Override // defpackage.tl2
        /* renamed from: a */
        public void onChanged(CharSequence charSequence) {
            x41 x41Var = x41.this;
            x41Var.u0.removeCallbacks(x41Var.v0);
            x41.this.D(charSequence);
            x41 x41Var2 = x41.this;
            x41Var2.u0.postDelayed(x41Var2.v0, 2000L);
        }
    }

    /* compiled from: FingerprintDialogFragment.java */
    /* renamed from: x41$e */
    /* loaded from: classes.dex */
    public static class e {
        public static void a(Drawable drawable) {
            if (drawable instanceof AnimatedVectorDrawable) {
                ((AnimatedVectorDrawable) drawable).start();
            }
        }
    }

    /* compiled from: FingerprintDialogFragment.java */
    /* renamed from: x41$f */
    /* loaded from: classes.dex */
    public static class f {
        public static int a() {
            return ay2.colorError;
        }
    }

    public static x41 y() {
        return new x41();
    }

    public final boolean A(int i, int i2) {
        if (i == 0 && i2 == 1) {
            return false;
        }
        if (i == 1 && i2 == 2) {
            return true;
        }
        return i == 2 && i2 == 1;
    }

    public void B(int i) {
        int m;
        Drawable w;
        if (this.z0 == null || Build.VERSION.SDK_INT < 23 || (w = w((m = this.w0.m()), i)) == null) {
            return;
        }
        this.z0.setImageDrawable(w);
        if (A(m, i)) {
            e.a(w);
        }
        this.w0.T(i);
    }

    public void C(int i) {
        TextView textView = this.A0;
        if (textView != null) {
            textView.setTextColor(i == 2 ? this.x0 : this.y0);
        }
    }

    public void D(CharSequence charSequence) {
        TextView textView = this.A0;
        if (textView != null) {
            textView.setText(charSequence);
        }
    }

    @Override // defpackage.sn0
    public Dialog m(Bundle bundle) {
        CharSequence q;
        a.C0008a c0008a = new a.C0008a(requireContext());
        c0008a.setTitle(this.w0.s());
        View inflate = LayoutInflater.from(c0008a.getContext()).inflate(v03.fingerprint_dialog_layout, (ViewGroup) null);
        TextView textView = (TextView) inflate.findViewById(h03.fingerprint_subtitle);
        if (textView != null) {
            CharSequence r = this.w0.r();
            if (TextUtils.isEmpty(r)) {
                textView.setVisibility(8);
            } else {
                textView.setVisibility(0);
                textView.setText(r);
            }
        }
        TextView textView2 = (TextView) inflate.findViewById(h03.fingerprint_description);
        if (textView2 != null) {
            CharSequence k = this.w0.k();
            if (TextUtils.isEmpty(k)) {
                textView2.setVisibility(8);
            } else {
                textView2.setVisibility(0);
                textView2.setText(k);
            }
        }
        this.z0 = (ImageView) inflate.findViewById(h03.fingerprint_icon);
        this.A0 = (TextView) inflate.findViewById(h03.fingerprint_error);
        if (androidx.biometric.b.c(this.w0.a())) {
            q = getString(l13.confirm_device_credential_password);
        } else {
            q = this.w0.q();
        }
        c0008a.g(q, new b());
        c0008a.setView(inflate);
        androidx.appcompat.app.a create = c0008a.create();
        create.setCanceledOnTouchOutside(false);
        return create;
    }

    @Override // defpackage.sn0, android.content.DialogInterface.OnCancelListener
    public void onCancel(DialogInterface dialogInterface) {
        super.onCancel(dialogInterface);
        this.w0.Q(true);
    }

    @Override // defpackage.sn0, androidx.fragment.app.Fragment
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        v();
        if (Build.VERSION.SDK_INT >= 26) {
            this.x0 = x(f.a());
        } else {
            Context context = getContext();
            this.x0 = context != null ? m70.d(context, oy2.biometric_error_color) : 0;
        }
        this.y0 = x(16842808);
    }

    @Override // androidx.fragment.app.Fragment
    public void onPause() {
        super.onPause();
        this.u0.removeCallbacksAndMessages(null);
    }

    @Override // androidx.fragment.app.Fragment
    public void onResume() {
        super.onResume();
        this.w0.T(0);
        this.w0.U(1);
        this.w0.S(getString(l13.fingerprint_dialog_touch_sensor));
    }

    public final void v() {
        FragmentActivity activity = getActivity();
        if (activity == null) {
            return;
        }
        androidx.biometric.e eVar = (androidx.biometric.e) new l(activity).a(androidx.biometric.e.class);
        this.w0 = eVar;
        eVar.n().observe(this, new c());
        this.w0.l().observe(this, new d());
    }

    public final Drawable w(int i, int i2) {
        int i3;
        Context context = getContext();
        if (context == null) {
            return null;
        }
        if (i == 0 && i2 == 1) {
            i3 = nz2.fingerprint_dialog_fp_icon;
        } else if (i == 1 && i2 == 2) {
            i3 = nz2.fingerprint_dialog_error;
        } else if (i == 2 && i2 == 1) {
            i3 = nz2.fingerprint_dialog_fp_icon;
        } else if (i != 1 || i2 != 3) {
            return null;
        } else {
            i3 = nz2.fingerprint_dialog_fp_icon;
        }
        return m70.f(context, i3);
    }

    public final int x(int i) {
        Context context = getContext();
        FragmentActivity activity = getActivity();
        if (context == null || activity == null) {
            return 0;
        }
        TypedValue typedValue = new TypedValue();
        context.getTheme().resolveAttribute(i, typedValue, true);
        TypedArray obtainStyledAttributes = activity.obtainStyledAttributes(typedValue.data, new int[]{i});
        int color = obtainStyledAttributes.getColor(0, 0);
        obtainStyledAttributes.recycle();
        return color;
    }

    public void z() {
        Context context = getContext();
        if (context == null) {
            return;
        }
        this.w0.U(1);
        this.w0.S(context.getString(l13.fingerprint_dialog_touch_sensor));
    }
}
