package defpackage;

import android.content.ServiceConnection;
import android.net.Uri;
import android.os.Bundle;
import com.google.android.gms.internal.measurement.f;

/* compiled from: com.google.android.gms:play-services-measurement-impl@@19.0.0 */
/* renamed from: oi5  reason: default package */
/* loaded from: classes.dex */
public final class oi5 implements Runnable {
    public final /* synthetic */ f a;
    public final /* synthetic */ ServiceConnection f0;
    public final /* synthetic */ ri5 g0;

    public oi5(ri5 ri5Var, f fVar, ServiceConnection serviceConnection) {
        this.g0 = ri5Var;
        this.a = fVar;
        this.f0 = serviceConnection;
    }

    @Override // java.lang.Runnable
    public final void run() {
        String str;
        ri5 ri5Var = this.g0;
        ui5 ui5Var = ri5Var.f0;
        str = ri5Var.a;
        f fVar = this.a;
        ServiceConnection serviceConnection = this.f0;
        ui5Var.a.q().e();
        Bundle bundle = new Bundle();
        bundle.putString("package_name", str);
        Bundle bundle2 = null;
        try {
            Bundle W = fVar.W(bundle);
            if (W == null) {
                ui5Var.a.w().l().a("Install Referrer Service returned a null response");
            } else {
                bundle2 = W;
            }
        } catch (Exception e) {
            ui5Var.a.w().l().b("Exception occurred while retrieving the Install Referrer", e.getMessage());
        }
        ui5Var.a.q().e();
        if (bundle2 != null) {
            long j = bundle2.getLong("install_begin_timestamp_seconds", 0L) * 1000;
            if (j == 0) {
                ui5Var.a.w().p().a("Service response is missing Install Referrer install timestamp");
            } else {
                String string = bundle2.getString("install_referrer");
                if (string != null && !string.isEmpty()) {
                    ui5Var.a.w().v().b("InstallReferrer API result", string);
                    Bundle k0 = ui5Var.a.G().k0(Uri.parse(string.length() != 0 ? "?".concat(string) : new String("?")));
                    if (k0 == null) {
                        ui5Var.a.w().l().a("No campaign params defined in Install Referrer result");
                    } else {
                        String string2 = k0.getString("medium");
                        if (string2 != null && !"(not set)".equalsIgnoreCase(string2) && !"organic".equalsIgnoreCase(string2)) {
                            long j2 = bundle2.getLong("referrer_click_timestamp_seconds", 0L) * 1000;
                            if (j2 == 0) {
                                ui5Var.a.w().l().a("Install Referrer is missing click timestamp for ad campaign");
                            } else {
                                k0.putLong("click_timestamp", j2);
                            }
                        }
                        if (j == ui5Var.a.A().f.a()) {
                            ui5Var.a.w().v().a("Install Referrer campaign has already been logged");
                        } else if (ui5Var.a.h()) {
                            ui5Var.a.A().f.b(j);
                            ui5Var.a.w().v().b("Logging Install Referrer campaign from sdk with ", "referrer API");
                            k0.putString("_cis", "referrer API");
                            ui5Var.a.F().X("auto", "_cmp", k0);
                        }
                    }
                } else {
                    ui5Var.a.w().l().a("No referrer defined in Install Referrer response");
                }
            }
        }
        v50.b().c(ui5Var.a.m(), serviceConnection);
    }
}
