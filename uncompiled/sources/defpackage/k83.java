package defpackage;

import java.math.BigInteger;
import java.util.Date;

/* compiled from: BlockTime.kt */
/* renamed from: k83  reason: default package */
/* loaded from: classes2.dex */
public final class k83 {
    public final Date a;
    public final BigInteger b;

    public k83(Date date, BigInteger bigInteger) {
        fs1.f(date, "date");
        fs1.f(bigInteger, "block");
        this.a = date;
        this.b = bigInteger;
    }

    public final BigInteger a() {
        return this.b;
    }

    public final Date b() {
        return this.a;
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj instanceof k83) {
            k83 k83Var = (k83) obj;
            return fs1.b(this.a, k83Var.a) && fs1.b(this.b, k83Var.b);
        }
        return false;
    }

    public int hashCode() {
        return (this.a.hashCode() * 31) + this.b.hashCode();
    }

    public String toString() {
        return "Result(date=" + this.a + ", block=" + this.b + ')';
    }
}
