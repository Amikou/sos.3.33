package defpackage;

import android.graphics.Typeface;

/* compiled from: TextAppearanceFontCallback.java */
/* renamed from: f44  reason: default package */
/* loaded from: classes2.dex */
public abstract class f44 {
    public abstract void a(int i);

    public abstract void b(Typeface typeface, boolean z);
}
