package defpackage;

import androidx.constraintlayout.core.widgets.analyzer.DependencyNode;
import androidx.constraintlayout.core.widgets.analyzer.WidgetRun;
import androidx.constraintlayout.core.widgets.analyzer.b;
import androidx.constraintlayout.core.widgets.analyzer.c;
import androidx.constraintlayout.core.widgets.d;
import com.github.mikephil.charting.utils.Utils;
import java.util.ArrayList;

/* compiled from: RunGroup.java */
/* renamed from: aa3  reason: default package */
/* loaded from: classes.dex */
public class aa3 {
    public static int c;
    public WidgetRun a;
    public ArrayList<WidgetRun> b = new ArrayList<>();

    public aa3(WidgetRun widgetRun, int i) {
        this.a = null;
        c++;
        this.a = widgetRun;
    }

    public void a(WidgetRun widgetRun) {
        this.b.add(widgetRun);
    }

    public long b(d dVar, int i) {
        long j;
        int i2;
        WidgetRun widgetRun = this.a;
        if (widgetRun instanceof gx) {
            if (((gx) widgetRun).f != i) {
                return 0L;
            }
        } else if (i == 0) {
            if (!(widgetRun instanceof c)) {
                return 0L;
            }
        } else if (!(widgetRun instanceof androidx.constraintlayout.core.widgets.analyzer.d)) {
            return 0L;
        }
        DependencyNode dependencyNode = (i == 0 ? dVar.d : dVar.e).h;
        DependencyNode dependencyNode2 = (i == 0 ? dVar.d : dVar.e).i;
        boolean contains = widgetRun.h.l.contains(dependencyNode);
        boolean contains2 = this.a.i.l.contains(dependencyNode2);
        long j2 = this.a.j();
        if (contains && contains2) {
            long d = d(this.a.h, 0L);
            long c2 = c(this.a.i, 0L);
            long j3 = d - j2;
            WidgetRun widgetRun2 = this.a;
            int i3 = widgetRun2.i.f;
            if (j3 >= (-i3)) {
                j3 += i3;
            }
            int i4 = widgetRun2.h.f;
            long j4 = ((-c2) - j2) - i4;
            if (j4 >= i4) {
                j4 -= i4;
            }
            float s = widgetRun2.b.s(i);
            float f = (float) (s > Utils.FLOAT_EPSILON ? (((float) j4) / s) + (((float) j3) / (1.0f - s)) : 0L);
            long j5 = (f * s) + 0.5f + j2 + (f * (1.0f - s)) + 0.5f;
            WidgetRun widgetRun3 = this.a;
            j = widgetRun3.h.f + j5;
            i2 = widgetRun3.i.f;
        } else if (contains) {
            DependencyNode dependencyNode3 = this.a.h;
            return Math.max(d(dependencyNode3, dependencyNode3.f), this.a.h.f + j2);
        } else if (contains2) {
            DependencyNode dependencyNode4 = this.a.i;
            return Math.max(-c(dependencyNode4, dependencyNode4.f), (-this.a.i.f) + j2);
        } else {
            WidgetRun widgetRun4 = this.a;
            j = widgetRun4.h.f + widgetRun4.j();
            i2 = this.a.i.f;
        }
        return j - i2;
    }

    public final long c(DependencyNode dependencyNode, long j) {
        WidgetRun widgetRun = dependencyNode.d;
        if (widgetRun instanceof b) {
            return j;
        }
        int size = dependencyNode.k.size();
        long j2 = j;
        for (int i = 0; i < size; i++) {
            im0 im0Var = dependencyNode.k.get(i);
            if (im0Var instanceof DependencyNode) {
                DependencyNode dependencyNode2 = (DependencyNode) im0Var;
                if (dependencyNode2.d != widgetRun) {
                    j2 = Math.min(j2, c(dependencyNode2, dependencyNode2.f + j));
                }
            }
        }
        if (dependencyNode == widgetRun.i) {
            long j3 = j - widgetRun.j();
            return Math.min(Math.min(j2, c(widgetRun.h, j3)), j3 - widgetRun.h.f);
        }
        return j2;
    }

    public final long d(DependencyNode dependencyNode, long j) {
        WidgetRun widgetRun = dependencyNode.d;
        if (widgetRun instanceof b) {
            return j;
        }
        int size = dependencyNode.k.size();
        long j2 = j;
        for (int i = 0; i < size; i++) {
            im0 im0Var = dependencyNode.k.get(i);
            if (im0Var instanceof DependencyNode) {
                DependencyNode dependencyNode2 = (DependencyNode) im0Var;
                if (dependencyNode2.d != widgetRun) {
                    j2 = Math.max(j2, d(dependencyNode2, dependencyNode2.f + j));
                }
            }
        }
        if (dependencyNode == widgetRun.h) {
            long j3 = j + widgetRun.j();
            return Math.max(Math.max(j2, d(widgetRun.i, j3)), j3 - widgetRun.i.f);
        }
        return j2;
    }
}
