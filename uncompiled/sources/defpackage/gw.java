package defpackage;

import android.graphics.Rect;
import android.view.View;
import androidx.recyclerview.widget.RecyclerView;

/* compiled from: CarouselItemDecoration.java */
/* renamed from: gw  reason: default package */
/* loaded from: classes2.dex */
public class gw extends RecyclerView.n {
    public int a;
    public int b;

    public gw(int i, int i2) {
        this.a = i2;
        this.b = i;
    }

    @Override // androidx.recyclerview.widget.RecyclerView.n
    public void getItemOffsets(Rect rect, View view, RecyclerView recyclerView, RecyclerView.x xVar) {
        super.getItemOffsets(rect, view, recyclerView, xVar);
        int i = this.b;
        int i2 = this.a;
        if (i > 0) {
            i2 /= 2;
        }
        rect.right = i2;
        rect.left = i > 0 ? this.a / 2 : 0;
        if (xVar.b() - 1 == recyclerView.g0(view)) {
            rect.right = this.b > 0 ? (recyclerView.getMeasuredWidth() / 2) - (this.b / 2) : 0;
        }
        if (recyclerView.g0(view) == 0) {
            rect.left = this.b > 0 ? (recyclerView.getMeasuredWidth() / 2) - (this.b / 2) : 0;
        }
    }
}
