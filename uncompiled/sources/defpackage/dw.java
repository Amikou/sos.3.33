package defpackage;

import android.graphics.drawable.Drawable;
import android.view.View;

/* compiled from: CardViewDelegate.java */
/* renamed from: dw  reason: default package */
/* loaded from: classes.dex */
public interface dw {
    void a(int i, int i2, int i3, int i4);

    void b(int i, int i2);

    void c(Drawable drawable);

    boolean d();

    boolean e();

    Drawable f();

    View g();
}
