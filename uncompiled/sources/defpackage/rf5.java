package defpackage;

import com.google.android.gms.internal.clearcut.t;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;

/* renamed from: rf5  reason: default package */
/* loaded from: classes.dex */
public final class rf5 {
    public static final rf5 c = new rf5();
    public final bg5 a;
    public final ConcurrentMap<Class<?>, t<?>> b = new ConcurrentHashMap();

    public rf5() {
        String[] strArr = {"com.google.protobuf.AndroidProto3SchemaFactory"};
        bg5 bg5Var = null;
        for (int i = 0; i <= 0; i++) {
            bg5Var = c(strArr[0]);
            if (bg5Var != null) {
                break;
            }
        }
        this.a = bg5Var == null ? new ld5() : bg5Var;
    }

    public static rf5 a() {
        return c;
    }

    public static bg5 c(String str) {
        try {
            return (bg5) Class.forName(str).getConstructor(new Class[0]).newInstance(new Object[0]);
        } catch (Throwable unused) {
            return null;
        }
    }

    public final <T> t<T> b(Class<T> cls) {
        gb5.e(cls, "messageType");
        t<T> tVar = (t<T>) this.b.get(cls);
        if (tVar == null) {
            t<T> a = this.a.a(cls);
            gb5.e(cls, "messageType");
            gb5.e(a, "schema");
            t<T> tVar2 = (t<T>) this.b.putIfAbsent(cls, a);
            return tVar2 != null ? tVar2 : a;
        }
        return tVar;
    }

    public final <T> t<T> d(T t) {
        return b(t.getClass());
    }
}
