package defpackage;

import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;

/* compiled from: FragmentOnAttachListener.java */
/* renamed from: qa1  reason: default package */
/* loaded from: classes.dex */
public interface qa1 {
    void a(FragmentManager fragmentManager, Fragment fragment);
}
