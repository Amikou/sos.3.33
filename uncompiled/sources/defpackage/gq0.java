package defpackage;

/* compiled from: DoubleCheck.java */
/* renamed from: gq0  reason: default package */
/* loaded from: classes.dex */
public final class gq0<T> implements ew2<T> {
    public static final Object c = new Object();
    public volatile ew2<T> a;
    public volatile Object b = c;

    public gq0(ew2<T> ew2Var) {
        this.a = ew2Var;
    }

    public static <P extends ew2<T>, T> ew2<T> a(P p) {
        yt2.b(p);
        return p instanceof gq0 ? p : new gq0(p);
    }

    public static Object b(Object obj, Object obj2) {
        if (!(obj != c) || obj == obj2) {
            return obj2;
        }
        throw new IllegalStateException("Scoped provider was invoked recursively returning different results: " + obj + " & " + obj2 + ". This is likely due to a circular dependency.");
    }

    @Override // defpackage.ew2
    public T get() {
        T t = (T) this.b;
        Object obj = c;
        if (t == obj) {
            synchronized (this) {
                t = this.b;
                if (t == obj) {
                    t = this.a.get();
                    this.b = b(this.b, t);
                    this.a = null;
                }
            }
        }
        return t;
    }
}
