package defpackage;

import android.view.View;
import android.widget.FrameLayout;
import androidx.appcompat.widget.AppCompatImageView;
import androidx.constraintlayout.widget.ConstraintLayout;
import com.google.android.material.button.MaterialButton;
import com.google.android.material.textfield.TextInputLayout;
import com.google.android.material.textview.MaterialTextView;
import net.safemoon.androidwallet.R;
import net.safemoon.androidwallet.views.MyTextInputLayout;

/* compiled from: FragmentAddCustomContractBinding.java */
/* renamed from: n91  reason: default package */
/* loaded from: classes2.dex */
public final class n91 {
    public final MaterialButton a;
    public final MaterialButton b;
    public final AppCompatImageView c;
    public final AppCompatImageView d;
    public final ConstraintLayout e;
    public final MyTextInputLayout f;
    public final TextInputLayout g;
    public final TextInputLayout h;
    public final TextInputLayout i;
    public final rp3 j;

    public n91(FrameLayout frameLayout, MaterialButton materialButton, MaterialButton materialButton2, FrameLayout frameLayout2, AppCompatImageView appCompatImageView, AppCompatImageView appCompatImageView2, ConstraintLayout constraintLayout, MyTextInputLayout myTextInputLayout, TextInputLayout textInputLayout, TextInputLayout textInputLayout2, TextInputLayout textInputLayout3, MaterialTextView materialTextView, MaterialTextView materialTextView2, rp3 rp3Var, MaterialTextView materialTextView3) {
        this.a = materialButton;
        this.b = materialButton2;
        this.c = appCompatImageView;
        this.d = appCompatImageView2;
        this.e = constraintLayout;
        this.f = myTextInputLayout;
        this.g = textInputLayout;
        this.h = textInputLayout2;
        this.i = textInputLayout3;
        this.j = rp3Var;
    }

    public static n91 a(View view) {
        int i = R.id.btnNetwork;
        MaterialButton materialButton = (MaterialButton) ai4.a(view, R.id.btnNetwork);
        if (materialButton != null) {
            i = R.id.clkAddToken;
            MaterialButton materialButton2 = (MaterialButton) ai4.a(view, R.id.clkAddToken);
            if (materialButton2 != null) {
                FrameLayout frameLayout = (FrameLayout) view;
                i = R.id.imgChain;
                AppCompatImageView appCompatImageView = (AppCompatImageView) ai4.a(view, R.id.imgChain);
                if (appCompatImageView != null) {
                    i = R.id.imgToken;
                    AppCompatImageView appCompatImageView2 = (AppCompatImageView) ai4.a(view, R.id.imgToken);
                    if (appCompatImageView2 != null) {
                        i = R.id.parentNetWork;
                        ConstraintLayout constraintLayout = (ConstraintLayout) ai4.a(view, R.id.parentNetWork);
                        if (constraintLayout != null) {
                            i = R.id.tilContractAddress;
                            MyTextInputLayout myTextInputLayout = (MyTextInputLayout) ai4.a(view, R.id.tilContractAddress);
                            if (myTextInputLayout != null) {
                                i = R.id.tilContractDecimal;
                                TextInputLayout textInputLayout = (TextInputLayout) ai4.a(view, R.id.tilContractDecimal);
                                if (textInputLayout != null) {
                                    i = R.id.tilContractName;
                                    TextInputLayout textInputLayout2 = (TextInputLayout) ai4.a(view, R.id.tilContractName);
                                    if (textInputLayout2 != null) {
                                        i = R.id.tilContractSymbol;
                                        TextInputLayout textInputLayout3 = (TextInputLayout) ai4.a(view, R.id.tilContractSymbol);
                                        if (textInputLayout3 != null) {
                                            i = R.id.tilImportantDetails;
                                            MaterialTextView materialTextView = (MaterialTextView) ai4.a(view, R.id.tilImportantDetails);
                                            if (materialTextView != null) {
                                                i = R.id.tilImportantTitle;
                                                MaterialTextView materialTextView2 = (MaterialTextView) ai4.a(view, R.id.tilImportantTitle);
                                                if (materialTextView2 != null) {
                                                    i = R.id.toolbar;
                                                    View a = ai4.a(view, R.id.toolbar);
                                                    if (a != null) {
                                                        rp3 a2 = rp3.a(a);
                                                        i = R.id.txtTokenLabel;
                                                        MaterialTextView materialTextView3 = (MaterialTextView) ai4.a(view, R.id.txtTokenLabel);
                                                        if (materialTextView3 != null) {
                                                            return new n91(frameLayout, materialButton, materialButton2, frameLayout, appCompatImageView, appCompatImageView2, constraintLayout, myTextInputLayout, textInputLayout, textInputLayout2, textInputLayout3, materialTextView, materialTextView2, a2, materialTextView3);
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
        throw new NullPointerException("Missing required view with ID: ".concat(view.getResources().getResourceName(i)));
    }
}
