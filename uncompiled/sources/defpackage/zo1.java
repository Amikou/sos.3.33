package defpackage;

/* compiled from: ImageTranscodeResult.java */
/* renamed from: zo1  reason: default package */
/* loaded from: classes.dex */
public class zo1 {
    public final int a;

    public zo1(int i) {
        this.a = i;
    }

    public int a() {
        return this.a;
    }

    public String toString() {
        return String.format(null, "Status: %d", Integer.valueOf(this.a));
    }
}
