package defpackage;

import androidx.biometric.BiometricPrompt;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentActivity;

/* compiled from: IRequestBiometricAuthUseCase.kt */
/* renamed from: qm1  reason: default package */
/* loaded from: classes2.dex */
public interface qm1 {

    /* compiled from: IRequestBiometricAuthUseCase.kt */
    /* renamed from: qm1$a */
    /* loaded from: classes2.dex */
    public interface a {
        void a();

        void b(int i);
    }

    BiometricPrompt a(Fragment fragment, a aVar);

    BiometricPrompt b(FragmentActivity fragmentActivity, a aVar);
}
