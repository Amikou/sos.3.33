package defpackage;

import kotlinx.coroutines.CoroutineDispatcher;

/* compiled from: Dispatcher.kt */
/* renamed from: pk0  reason: default package */
/* loaded from: classes2.dex */
public final class pk0 extends z01 {
    public static final pk0 k0;
    public static final CoroutineDispatcher l0;

    static {
        int d;
        pk0 pk0Var = new pk0();
        k0 = pk0Var;
        d = c34.d("kotlinx.coroutines.io.parallelism", u33.b(64, a34.a()), 0, 0, 12, null);
        l0 = new wz1(pk0Var, d, "Dispatchers.IO", 1);
    }

    public pk0() {
        super(0, 0, null, 7, null);
    }

    public final CoroutineDispatcher N() {
        return l0;
    }

    @Override // java.io.Closeable, java.lang.AutoCloseable
    public void close() {
        throw new UnsupportedOperationException("Dispatchers.Default cannot be closed");
    }

    @Override // kotlinx.coroutines.CoroutineDispatcher
    public String toString() {
        return "Dispatchers.Default";
    }
}
