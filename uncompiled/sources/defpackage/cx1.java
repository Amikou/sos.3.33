package defpackage;

/* renamed from: cx1  reason: default package */
/* loaded from: classes2.dex */
public class cx1 extends wl implements Cloneable {
    public cx1(int i) {
        super(new dx1(i));
    }

    @Override // java.security.MessageDigest, java.security.MessageDigestSpi
    public Object clone() throws CloneNotSupportedException {
        wl wlVar = (wl) super.clone();
        wlVar.a = new dx1((dx1) this.a);
        return wlVar;
    }
}
