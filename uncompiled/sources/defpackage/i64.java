package defpackage;

import android.content.res.ColorStateList;
import android.graphics.PorterDuff;

/* compiled from: TintAwareDrawable.java */
/* renamed from: i64  reason: default package */
/* loaded from: classes.dex */
public interface i64 {
    void setTint(int i);

    void setTintList(ColorStateList colorStateList);

    void setTintMode(PorterDuff.Mode mode);
}
