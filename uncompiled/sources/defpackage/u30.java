package defpackage;

/* compiled from: CompletionState.kt */
/* renamed from: u30  reason: default package */
/* loaded from: classes2.dex */
public final class u30 {
    public final Object a;
    public final tc1<Throwable, te4> b;

    /* JADX WARN: Multi-variable type inference failed */
    public u30(Object obj, tc1<? super Throwable, te4> tc1Var) {
        this.a = obj;
        this.b = tc1Var;
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj instanceof u30) {
            u30 u30Var = (u30) obj;
            return fs1.b(this.a, u30Var.a) && fs1.b(this.b, u30Var.b);
        }
        return false;
    }

    public int hashCode() {
        Object obj = this.a;
        return ((obj == null ? 0 : obj.hashCode()) * 31) + this.b.hashCode();
    }

    public String toString() {
        return "CompletedWithCancellation(result=" + this.a + ", onCancellation=" + this.b + ')';
    }
}
