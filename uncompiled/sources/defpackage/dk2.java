package defpackage;

import org.json.JSONException;
import org.json.JSONObject;

/* compiled from: OSOutcomeEventParams.kt */
/* renamed from: dk2  reason: default package */
/* loaded from: classes2.dex */
public final class dk2 {
    public final String a;
    public final pk2 b;
    public float c;
    public long d;

    public dk2(String str, pk2 pk2Var, float f, long j) {
        fs1.f(str, "outcomeId");
        this.a = str;
        this.b = pk2Var;
        this.c = f;
        this.d = j;
    }

    public final String a() {
        return this.a;
    }

    public final pk2 b() {
        return this.b;
    }

    public final long c() {
        return this.d;
    }

    public final float d() {
        return this.c;
    }

    public final boolean e() {
        pk2 pk2Var = this.b;
        return pk2Var == null || (pk2Var.a() == null && this.b.b() == null);
    }

    public final void f(long j) {
        this.d = j;
    }

    public final JSONObject g() throws JSONException {
        JSONObject put = new JSONObject().put("id", this.a);
        pk2 pk2Var = this.b;
        if (pk2Var != null) {
            put.put("sources", pk2Var.e());
        }
        float f = this.c;
        if (f > 0) {
            put.put("weight", Float.valueOf(f));
        }
        long j = this.d;
        if (j > 0) {
            put.put("timestamp", j);
        }
        fs1.e(put, "json");
        return put;
    }

    public String toString() {
        return "OSOutcomeEventParams{outcomeId='" + this.a + "', outcomeSource=" + this.b + ", weight=" + this.c + ", timestamp=" + this.d + '}';
    }
}
