package defpackage;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import androidx.recyclerview.widget.RecyclerView;
import java.util.List;
import net.safemoon.androidwallet.R;
import net.safemoon.androidwallet.model.nft.NFTData;

/* compiled from: NFTPropertyAdapter.kt */
/* renamed from: pc2  reason: default package */
/* loaded from: classes2.dex */
public final class pc2 extends RecyclerView.Adapter<a> {
    public final List<NFTData.Property> a;

    /* compiled from: NFTPropertyAdapter.kt */
    /* renamed from: pc2$a */
    /* loaded from: classes2.dex */
    public static final class a extends RecyclerView.a0 {
        public final TextView a;
        public final TextView b;

        /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
        public a(View view) {
            super(view);
            fs1.f(view, "ItemView");
            fs1.e(this.itemView, "itemView");
            View findViewById = this.itemView.findViewById(R.id.nftPropertyName);
            fs1.e(findViewById, "itemView.findViewById(R.id.nftPropertyName)");
            this.a = (TextView) findViewById;
            View findViewById2 = this.itemView.findViewById(R.id.nftPropertyValue);
            fs1.e(findViewById2, "itemView.findViewById(R.id.nftPropertyValue)");
            this.b = (TextView) findViewById2;
        }

        public final TextView a() {
            return this.a;
        }

        public final TextView b() {
            return this.b;
        }
    }

    public pc2(List<NFTData.Property> list) {
        fs1.f(list, "mList");
        this.a = list;
    }

    @Override // androidx.recyclerview.widget.RecyclerView.Adapter
    /* renamed from: a */
    public void onBindViewHolder(a aVar, int i) {
        fs1.f(aVar, "holder");
        NFTData.Property property = this.a.get(i);
        aVar.a().setText(property.getName());
        aVar.b().setText(property.getValue());
    }

    @Override // androidx.recyclerview.widget.RecyclerView.Adapter
    /* renamed from: b */
    public a onCreateViewHolder(ViewGroup viewGroup, int i) {
        fs1.f(viewGroup, "parent");
        View inflate = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.item_collectible_property, viewGroup, false);
        fs1.e(inflate, "view");
        return new a(inflate);
    }

    @Override // androidx.recyclerview.widget.RecyclerView.Adapter
    public int getItemCount() {
        return this.a.size();
    }
}
