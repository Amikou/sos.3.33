package defpackage;

import java.io.IOException;
import java.io.OutputStream;

/* compiled from: ImageTranscoder.java */
/* renamed from: ap1  reason: default package */
/* loaded from: classes.dex */
public interface ap1 {
    String a();

    boolean b(wn1 wn1Var);

    boolean c(zu0 zu0Var, p93 p93Var, p73 p73Var);

    zo1 d(zu0 zu0Var, OutputStream outputStream, p93 p93Var, p73 p73Var, wn1 wn1Var, Integer num) throws IOException;
}
