package defpackage;

import com.google.android.gms.internal.vision.zzht;

/* compiled from: com.google.android.gms:play-services-vision-common@@19.1.3 */
/* renamed from: iy5  reason: default package */
/* loaded from: classes.dex */
public final class iy5 {
    public static String a(zzht zzhtVar) {
        oy5 oy5Var = new oy5(zzhtVar);
        StringBuilder sb = new StringBuilder(oy5Var.zza());
        for (int i = 0; i < oy5Var.zza(); i++) {
            byte d = oy5Var.d(i);
            if (d == 34) {
                sb.append("\\\"");
            } else if (d == 39) {
                sb.append("\\'");
            } else if (d != 92) {
                switch (d) {
                    case 7:
                        sb.append("\\a");
                        continue;
                    case 8:
                        sb.append("\\b");
                        continue;
                    case 9:
                        sb.append("\\t");
                        continue;
                    case 10:
                        sb.append("\\n");
                        continue;
                    case 11:
                        sb.append("\\v");
                        continue;
                    case 12:
                        sb.append("\\f");
                        continue;
                    case 13:
                        sb.append("\\r");
                        continue;
                    default:
                        if (d >= 32 && d <= 126) {
                            sb.append((char) d);
                            continue;
                        } else {
                            sb.append('\\');
                            sb.append((char) (((d >>> 6) & 3) + 48));
                            sb.append((char) (((d >>> 3) & 7) + 48));
                            sb.append((char) ((d & 7) + 48));
                            break;
                        }
                }
            } else {
                sb.append("\\\\");
            }
        }
        return sb.toString();
    }
}
