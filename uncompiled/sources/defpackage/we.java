package defpackage;

import java.lang.annotation.Annotation;
import java.util.Collections;
import java.util.HashMap;

/* compiled from: AnnotationMap.java */
/* renamed from: we  reason: default package */
/* loaded from: classes.dex */
public final class we implements xe {
    public HashMap<Class<?>, Annotation> a;

    public we() {
    }

    public static we h(we weVar, we weVar2) {
        HashMap<Class<?>, Annotation> hashMap;
        HashMap<Class<?>, Annotation> hashMap2;
        if (weVar == null || (hashMap = weVar.a) == null || hashMap.isEmpty()) {
            return weVar2;
        }
        if (weVar2 == null || (hashMap2 = weVar2.a) == null || hashMap2.isEmpty()) {
            return weVar;
        }
        HashMap hashMap3 = new HashMap();
        for (Annotation annotation : weVar2.a.values()) {
            hashMap3.put(annotation.annotationType(), annotation);
        }
        for (Annotation annotation2 : weVar.a.values()) {
            hashMap3.put(annotation2.annotationType(), annotation2);
        }
        return new we(hashMap3);
    }

    @Override // defpackage.xe
    public <A extends Annotation> A a(Class<A> cls) {
        HashMap<Class<?>, Annotation> hashMap = this.a;
        if (hashMap == null) {
            return null;
        }
        return (A) hashMap.get(cls);
    }

    public final boolean b(Annotation annotation) {
        if (this.a == null) {
            this.a = new HashMap<>();
        }
        Annotation put = this.a.put(annotation.annotationType(), annotation);
        return put == null || !put.equals(annotation);
    }

    public boolean c(Annotation annotation) {
        return b(annotation);
    }

    public boolean d(Annotation annotation) {
        HashMap<Class<?>, Annotation> hashMap = this.a;
        if (hashMap == null || !hashMap.containsKey(annotation.annotationType())) {
            b(annotation);
            return true;
        }
        return false;
    }

    public Iterable<Annotation> e() {
        HashMap<Class<?>, Annotation> hashMap = this.a;
        if (hashMap != null && hashMap.size() != 0) {
            return this.a.values();
        }
        return Collections.emptyList();
    }

    public boolean f(Class<?> cls) {
        HashMap<Class<?>, Annotation> hashMap = this.a;
        if (hashMap == null) {
            return false;
        }
        return hashMap.containsKey(cls);
    }

    public boolean g(Class<? extends Annotation>[] clsArr) {
        if (this.a != null) {
            for (Class<? extends Annotation> cls : clsArr) {
                if (this.a.containsKey(cls)) {
                    return true;
                }
            }
        }
        return false;
    }

    public int i() {
        HashMap<Class<?>, Annotation> hashMap = this.a;
        if (hashMap == null) {
            return 0;
        }
        return hashMap.size();
    }

    public String toString() {
        HashMap<Class<?>, Annotation> hashMap = this.a;
        return hashMap == null ? "[null]" : hashMap.toString();
    }

    public we(HashMap<Class<?>, Annotation> hashMap) {
        this.a = hashMap;
    }
}
