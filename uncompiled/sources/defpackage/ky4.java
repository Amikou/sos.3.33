package defpackage;

import com.google.android.play.core.splitcompat.a;
import com.google.android.play.core.splitcompat.b;
import java.util.Set;

/* renamed from: ky4  reason: default package */
/* loaded from: classes2.dex */
public final class ky4 implements Runnable {
    public final /* synthetic */ Set a;
    public final /* synthetic */ a f0;

    public ky4(a aVar, Set set) {
        this.f0 = aVar;
        this.a = set;
    }

    @Override // java.lang.Runnable
    public final void run() {
        b bVar;
        try {
            for (String str : this.a) {
                bVar = this.f0.a;
                bVar.n(str);
            }
        } catch (Exception unused) {
        }
    }
}
