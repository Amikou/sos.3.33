package defpackage;

import java.util.Arrays;

/* renamed from: gx4  reason: default package */
/* loaded from: classes2.dex */
public final class gx4 {
    public final String a;
    public final long b;
    public final int c;
    public final boolean d;
    public final boolean e;
    public final byte[] f;

    public gx4() {
    }

    public gx4(String str, long j, int i, boolean z, boolean z2, byte[] bArr) {
        this();
        this.a = str;
        this.b = j;
        this.c = i;
        this.d = z;
        this.e = z2;
        this.f = bArr;
    }

    public static gx4 a(String str, long j, int i, boolean z, byte[] bArr, boolean z2) {
        return new gx4(str, j, i, z, z2, bArr);
    }

    public final boolean b() {
        if (d() == null) {
            return false;
        }
        return d().endsWith("/");
    }

    public final boolean c() {
        return f() == 0;
    }

    public String d() {
        return this.a;
    }

    public long e() {
        return this.b;
    }

    public boolean equals(Object obj) {
        if (obj == this) {
            return true;
        }
        if (obj instanceof gx4) {
            gx4 gx4Var = (gx4) obj;
            String str = this.a;
            if (str != null ? str.equals(gx4Var.d()) : gx4Var.d() == null) {
                if (this.b == gx4Var.e() && this.c == gx4Var.f() && this.d == gx4Var.g() && this.e == gx4Var.h() && Arrays.equals(this.f, gx4Var.f)) {
                    return true;
                }
            }
        }
        return false;
    }

    public int f() {
        return this.c;
    }

    public boolean g() {
        return this.d;
    }

    public boolean h() {
        return this.e;
    }

    public int hashCode() {
        String str = this.a;
        int hashCode = str == null ? 0 : str.hashCode();
        long j = this.b;
        return ((((((((((hashCode ^ 1000003) * 1000003) ^ ((int) (j ^ (j >>> 32)))) * 1000003) ^ this.c) * 1000003) ^ (true != this.d ? 1237 : 1231)) * 1000003) ^ (true == this.e ? 1231 : 1237)) * 1000003) ^ Arrays.hashCode(this.f);
    }

    public byte[] i() {
        return this.f;
    }

    public String toString() {
        String str = this.a;
        long j = this.b;
        int i = this.c;
        boolean z = this.d;
        boolean z2 = this.e;
        String arrays = Arrays.toString(this.f);
        StringBuilder sb = new StringBuilder(String.valueOf(str).length() + 126 + String.valueOf(arrays).length());
        sb.append("ZipEntry{name=");
        sb.append(str);
        sb.append(", size=");
        sb.append(j);
        sb.append(", compressionMethod=");
        sb.append(i);
        sb.append(", isPartial=");
        sb.append(z);
        sb.append(", isEndOfArchive=");
        sb.append(z2);
        sb.append(", headerBytes=");
        sb.append(arrays);
        sb.append("}");
        return sb.toString();
    }
}
