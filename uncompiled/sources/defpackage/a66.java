package defpackage;

import android.database.ContentObserver;
import android.os.Handler;
import java.util.concurrent.atomic.AtomicBoolean;

/* renamed from: a66  reason: default package */
/* loaded from: classes.dex */
public final class a66 extends ContentObserver {
    public a66(Handler handler) {
        super(null);
    }

    @Override // android.database.ContentObserver
    public final void onChange(boolean z) {
        AtomicBoolean atomicBoolean;
        atomicBoolean = v56.e;
        atomicBoolean.set(true);
    }
}
