package defpackage;

/* compiled from: com.google.android.gms:play-services-measurement-impl@@19.0.0 */
/* renamed from: rz5  reason: default package */
/* loaded from: classes.dex */
public final class rz5 implements yp5<zz5> {
    public static final rz5 f0 = new rz5();
    public final yp5<zz5> a = gq5.a(gq5.b(new a06()));

    public static boolean a() {
        return f0.zza().zza();
    }

    @Override // defpackage.yp5
    /* renamed from: b */
    public final zz5 zza() {
        return this.a.zza();
    }
}
