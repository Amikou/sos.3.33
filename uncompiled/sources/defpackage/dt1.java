package defpackage;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import androidx.appcompat.widget.LinearLayoutCompat;
import net.safemoon.androidwallet.R;

/* compiled from: ItemManageWalletAddressBinding.java */
/* renamed from: dt1  reason: default package */
/* loaded from: classes2.dex */
public final class dt1 {
    public final LinearLayoutCompat a;
    public final TextView b;
    public final TextView c;

    public dt1(LinearLayoutCompat linearLayoutCompat, LinearLayoutCompat linearLayoutCompat2, TextView textView, TextView textView2) {
        this.a = linearLayoutCompat;
        this.b = textView;
        this.c = textView2;
    }

    public static dt1 a(View view) {
        LinearLayoutCompat linearLayoutCompat = (LinearLayoutCompat) view;
        int i = R.id.tvWalletAddress;
        TextView textView = (TextView) ai4.a(view, R.id.tvWalletAddress);
        if (textView != null) {
            i = R.id.tvWalletName;
            TextView textView2 = (TextView) ai4.a(view, R.id.tvWalletName);
            if (textView2 != null) {
                return new dt1(linearLayoutCompat, linearLayoutCompat, textView, textView2);
            }
        }
        throw new NullPointerException("Missing required view with ID: ".concat(view.getResources().getResourceName(i)));
    }

    public static dt1 c(LayoutInflater layoutInflater, ViewGroup viewGroup, boolean z) {
        View inflate = layoutInflater.inflate(R.layout.item_manage_wallet_address, viewGroup, false);
        if (z) {
            viewGroup.addView(inflate);
        }
        return a(inflate);
    }

    public LinearLayoutCompat b() {
        return this.a;
    }
}
