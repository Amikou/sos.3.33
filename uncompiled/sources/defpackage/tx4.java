package defpackage;

/* renamed from: tx4  reason: default package */
/* loaded from: classes2.dex */
public final class tx4<ResultT> {
    public final fy4<ResultT> a = new fy4<>();

    public final void a(ResultT resultt) {
        this.a.g(resultt);
    }

    public final void b(Exception exc) {
        this.a.i(exc);
    }

    public final l34<ResultT> c() {
        return this.a;
    }

    public final void d(Exception exc) {
        this.a.j(exc);
    }

    public final void e(ResultT resultt) {
        this.a.h(resultt);
    }
}
