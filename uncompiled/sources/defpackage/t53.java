package defpackage;

import androidx.lifecycle.LiveData;
import java.util.List;
import net.safemoon.androidwallet.model.reflections.RoomReflectionsData;
import net.safemoon.androidwallet.model.reflections.RoomReflectionsDataAndToken;
import net.safemoon.androidwallet.model.reflections.RoomReflectionsToken;
import net.safemoon.androidwallet.model.reflections.RoomReflectionsTokenAndData;

/* compiled from: ReflectionsDao.kt */
/* renamed from: t53  reason: default package */
/* loaded from: classes2.dex */
public interface t53 {
    Object a(double d, String str, q70<? super te4> q70Var);

    Object b(RoomReflectionsToken roomReflectionsToken, q70<? super Long> q70Var);

    LiveData<List<RoomReflectionsTokenAndData>> c(int i);

    LiveData<List<RoomReflectionsDataAndToken>> d(String str, long j);

    Object e(RoomReflectionsData roomReflectionsData, q70<? super Long> q70Var);

    LiveData<List<RoomReflectionsToken>> f(int i);

    Object g(long j, long j2, String str, q70<? super te4> q70Var);

    Object h(String str, q70<? super RoomReflectionsData> q70Var);

    Object i(String str, q70<? super RoomReflectionsToken> q70Var);

    Object j(String str, long j, q70<? super Boolean> q70Var);

    Object k(q70<? super te4> q70Var);

    Object l(RoomReflectionsToken roomReflectionsToken, q70<? super te4> q70Var);

    Object m(q70<? super List<RoomReflectionsToken>> q70Var);

    LiveData<List<RoomReflectionsDataAndToken>> n(String str);

    LiveData<RoomReflectionsTokenAndData> o(String str);

    Object p(int i, String str, q70<? super te4> q70Var);

    Object q(long j, int i, q70<? super te4> q70Var);

    Object r(String str, q70<? super te4> q70Var);

    Object s(String str, int i, q70<? super Boolean> q70Var);
}
