package defpackage;

import android.os.Bundle;
import android.os.RemoteException;
import com.google.android.play.core.assetpacks.a;
import com.google.android.play.core.internal.p;
import java.util.ArrayList;
import java.util.List;

/* renamed from: gz4  reason: default package */
/* loaded from: classes2.dex */
public final class gz4 extends jt4 {
    public final /* synthetic */ List f0;
    public final /* synthetic */ tx4 g0;
    public final /* synthetic */ st4 h0;

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public gz4(st4 st4Var, tx4 tx4Var, List list, tx4 tx4Var2) {
        super(tx4Var);
        this.h0 = st4Var;
        this.f0 = list;
        this.g0 = tx4Var2;
    }

    @Override // defpackage.jt4
    public final void a() {
        it4 it4Var;
        zt4 zt4Var;
        String str;
        Bundle j;
        ArrayList k = st4.k(this.f0);
        try {
            zt4Var = this.h0.c;
            str = this.h0.a;
            j = st4.j();
            ((p) zt4Var.c()).p(str, k, j, new a(this.h0, this.g0, (byte[]) null));
        } catch (RemoteException e) {
            it4Var = st4.f;
            it4Var.c(e, "cancelDownloads(%s)", this.f0);
        }
    }
}
