package defpackage;

import android.os.Build;
import android.view.DisplayCutout;

/* compiled from: DisplayCutoutCompat.java */
/* renamed from: vp0  reason: default package */
/* loaded from: classes.dex */
public final class vp0 {
    public final Object a;

    public vp0(Object obj) {
        this.a = obj;
    }

    public static vp0 e(Object obj) {
        if (obj == null) {
            return null;
        }
        return new vp0(obj);
    }

    public int a() {
        if (Build.VERSION.SDK_INT >= 28) {
            return ((DisplayCutout) this.a).getSafeInsetBottom();
        }
        return 0;
    }

    public int b() {
        if (Build.VERSION.SDK_INT >= 28) {
            return ((DisplayCutout) this.a).getSafeInsetLeft();
        }
        return 0;
    }

    public int c() {
        if (Build.VERSION.SDK_INT >= 28) {
            return ((DisplayCutout) this.a).getSafeInsetRight();
        }
        return 0;
    }

    public int d() {
        if (Build.VERSION.SDK_INT >= 28) {
            return ((DisplayCutout) this.a).getSafeInsetTop();
        }
        return 0;
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null || vp0.class != obj.getClass()) {
            return false;
        }
        return sl2.a(this.a, ((vp0) obj).a);
    }

    public int hashCode() {
        Object obj = this.a;
        if (obj == null) {
            return 0;
        }
        return obj.hashCode();
    }

    public String toString() {
        return "DisplayCutoutCompat{" + this.a + "}";
    }
}
