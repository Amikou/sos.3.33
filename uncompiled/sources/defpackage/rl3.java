package defpackage;

import java.util.Iterator;
import kotlin.coroutines.intrinsics.IntrinsicsKt__IntrinsicsJvmKt;

/* compiled from: SequenceBuilder.kt */
/* renamed from: rl3  reason: default package */
/* loaded from: classes2.dex */
public class rl3 {

    /* compiled from: Sequences.kt */
    /* renamed from: rl3$a */
    /* loaded from: classes2.dex */
    public static final class a implements ol3<T> {
        public final /* synthetic */ hd1 a;

        public a(hd1 hd1Var) {
            this.a = hd1Var;
        }

        @Override // defpackage.ol3
        public Iterator<T> iterator() {
            return rl3.a(this.a);
        }
    }

    public static final <T> Iterator<T> a(hd1<? super ql3<? super T>, ? super q70<? super te4>, ? extends Object> hd1Var) {
        fs1.f(hd1Var, "block");
        pl3 pl3Var = new pl3();
        pl3Var.g(IntrinsicsKt__IntrinsicsJvmKt.b(hd1Var, pl3Var, pl3Var));
        return pl3Var;
    }

    public static final <T> ol3<T> b(hd1<? super ql3<? super T>, ? super q70<? super te4>, ? extends Object> hd1Var) {
        fs1.f(hd1Var, "block");
        return new a(hd1Var);
    }
}
