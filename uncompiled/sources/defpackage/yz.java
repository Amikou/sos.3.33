package defpackage;

import java.io.Closeable;

/* compiled from: Closeable.kt */
/* renamed from: yz  reason: default package */
/* loaded from: classes2.dex */
public final class yz {
    public static final void a(Closeable closeable, Throwable th) {
        if (closeable == null) {
            return;
        }
        if (th == null) {
            closeable.close();
            return;
        }
        try {
            closeable.close();
        } catch (Throwable th2) {
            py0.a(th, th2);
        }
    }
}
