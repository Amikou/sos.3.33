package defpackage;

import android.app.Activity;
import android.content.Intent;
import androidx.annotation.RecentlyNonNull;
import androidx.annotation.RecentlyNullable;
import com.google.android.gms.common.api.internal.LifecycleCallback;

/* compiled from: com.google.android.gms:play-services-basement@@17.4.0 */
/* renamed from: nz1  reason: default package */
/* loaded from: classes.dex */
public interface nz1 {
    void a(@RecentlyNonNull String str, @RecentlyNonNull LifecycleCallback lifecycleCallback);

    @RecentlyNullable
    <T extends LifecycleCallback> T b(@RecentlyNonNull String str, @RecentlyNonNull Class<T> cls);

    @RecentlyNonNull
    Activity c();

    void startActivityForResult(@RecentlyNonNull Intent intent, @RecentlyNonNull int i);
}
