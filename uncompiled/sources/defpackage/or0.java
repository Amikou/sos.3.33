package defpackage;

import androidx.media3.exoplayer.drm.DrmSession;

/* compiled from: DrmSession.java */
/* renamed from: or0  reason: default package */
/* loaded from: classes.dex */
public final /* synthetic */ class or0 {
    public static void a(DrmSession drmSession, DrmSession drmSession2) {
        if (drmSession == drmSession2) {
            return;
        }
        if (drmSession2 != null) {
            drmSession2.a(null);
        }
        if (drmSession != null) {
            drmSession.e(null);
        }
    }
}
