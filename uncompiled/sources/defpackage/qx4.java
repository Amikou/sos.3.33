package defpackage;

import android.content.Context;
import com.google.android.play.core.internal.d;

/* renamed from: qx4  reason: default package */
/* loaded from: classes2.dex */
public final class qx4 implements jw4<Object> {
    public final jw4 a;
    public final /* synthetic */ int b = 3;

    public qx4(jw4<Context> jw4Var, short[] sArr) {
        this.a = jw4Var;
    }

    public static qx4 b(jw4<Context> jw4Var) {
        return new qx4(jw4Var, null);
    }

    @Override // defpackage.jw4
    public final /* bridge */ /* synthetic */ Object a() {
        int i = this.b;
        if (i != 0) {
            return i != 1 ? i != 2 ? new oy4((Context) this.a.a()) : new uy4(((sx4) this.a).a()) : new ct4(((sx4) this.a).a());
        }
        cx4 cx4Var = (cx4) this.a.a();
        d.k(cx4Var);
        return cx4Var;
    }
}
