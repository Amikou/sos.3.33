package defpackage;

import java.io.FilterInputStream;
import java.io.IOException;
import java.io.InputStream;

/* compiled from: ContentLengthInputStream.java */
/* renamed from: g70  reason: default package */
/* loaded from: classes.dex */
public final class g70 extends FilterInputStream {
    public final long a;
    public int f0;

    public g70(InputStream inputStream, long j) {
        super(inputStream);
        this.a = j;
    }

    public static InputStream b(InputStream inputStream, long j) {
        return new g70(inputStream, j);
    }

    public final int a(int i) throws IOException {
        if (i >= 0) {
            this.f0 += i;
        } else if (this.a - this.f0 > 0) {
            throw new IOException("Failed to read all expected data, expected: " + this.a + ", but read: " + this.f0);
        }
        return i;
    }

    @Override // java.io.FilterInputStream, java.io.InputStream
    public synchronized int available() throws IOException {
        return (int) Math.max(this.a - this.f0, ((FilterInputStream) this).in.available());
    }

    @Override // java.io.FilterInputStream, java.io.InputStream
    public synchronized int read() throws IOException {
        int read;
        read = super.read();
        a(read >= 0 ? 1 : -1);
        return read;
    }

    @Override // java.io.FilterInputStream, java.io.InputStream
    public int read(byte[] bArr) throws IOException {
        return read(bArr, 0, bArr.length);
    }

    @Override // java.io.FilterInputStream, java.io.InputStream
    public synchronized int read(byte[] bArr, int i, int i2) throws IOException {
        return a(super.read(bArr, i, i2));
    }
}
