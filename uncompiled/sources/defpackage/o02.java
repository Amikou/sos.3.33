package defpackage;

import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import androidx.media3.common.i;
import java.util.ArrayDeque;
import java.util.Iterator;
import java.util.concurrent.CopyOnWriteArraySet;

/* compiled from: ListenerSet.java */
/* renamed from: o02  reason: default package */
/* loaded from: classes.dex */
public final class o02<T> {
    public final tz a;
    public final pj1 b;
    public final b<T> c;
    public final CopyOnWriteArraySet<c<T>> d;
    public final ArrayDeque<Runnable> e;
    public final ArrayDeque<Runnable> f;
    public boolean g;

    /* compiled from: ListenerSet.java */
    /* renamed from: o02$a */
    /* loaded from: classes.dex */
    public interface a<T> {
        void invoke(T t);
    }

    /* compiled from: ListenerSet.java */
    /* renamed from: o02$b */
    /* loaded from: classes.dex */
    public interface b<T> {
        void a(T t, i iVar);
    }

    /* compiled from: ListenerSet.java */
    /* renamed from: o02$c */
    /* loaded from: classes.dex */
    public static final class c<T> {
        public final T a;
        public i.b b = new i.b();
        public boolean c;
        public boolean d;

        public c(T t) {
            this.a = t;
        }

        public void a(int i, a<T> aVar) {
            if (this.d) {
                return;
            }
            if (i != -1) {
                this.b.a(i);
            }
            this.c = true;
            aVar.invoke(this.a);
        }

        public void b(b<T> bVar) {
            if (this.d || !this.c) {
                return;
            }
            i e = this.b.e();
            this.b = new i.b();
            this.c = false;
            bVar.a(this.a, e);
        }

        public void c(b<T> bVar) {
            this.d = true;
            if (this.c) {
                bVar.a(this.a, this.b.e());
            }
        }

        public boolean equals(Object obj) {
            if (this == obj) {
                return true;
            }
            if (obj == null || c.class != obj.getClass()) {
                return false;
            }
            return this.a.equals(((c) obj).a);
        }

        public int hashCode() {
            return this.a.hashCode();
        }
    }

    public o02(Looper looper, tz tzVar, b<T> bVar) {
        this(new CopyOnWriteArraySet(), looper, tzVar, bVar);
    }

    public static /* synthetic */ void h(CopyOnWriteArraySet copyOnWriteArraySet, int i, a aVar) {
        Iterator it = copyOnWriteArraySet.iterator();
        while (it.hasNext()) {
            ((c) it.next()).a(i, aVar);
        }
    }

    public void c(T t) {
        if (this.g) {
            return;
        }
        ii.e(t);
        this.d.add(new c<>(t));
    }

    public o02<T> d(Looper looper, tz tzVar, b<T> bVar) {
        return new o02<>(this.d, looper, tzVar, bVar);
    }

    public o02<T> e(Looper looper, b<T> bVar) {
        return d(looper, this.a, bVar);
    }

    public void f() {
        if (this.f.isEmpty()) {
            return;
        }
        if (!this.b.e(0)) {
            pj1 pj1Var = this.b;
            pj1Var.b(pj1Var.d(0));
        }
        boolean z = !this.e.isEmpty();
        this.e.addAll(this.f);
        this.f.clear();
        if (z) {
            return;
        }
        while (!this.e.isEmpty()) {
            this.e.peekFirst().run();
            this.e.removeFirst();
        }
    }

    public final boolean g(Message message) {
        Iterator<c<T>> it = this.d.iterator();
        while (it.hasNext()) {
            it.next().b(this.c);
            if (this.b.e(0)) {
                return true;
            }
        }
        return true;
    }

    public void i(final int i, final a<T> aVar) {
        final CopyOnWriteArraySet copyOnWriteArraySet = new CopyOnWriteArraySet(this.d);
        this.f.add(new Runnable() { // from class: n02
            @Override // java.lang.Runnable
            public final void run() {
                o02.h(copyOnWriteArraySet, i, aVar);
            }
        });
    }

    public void j() {
        Iterator<c<T>> it = this.d.iterator();
        while (it.hasNext()) {
            it.next().c(this.c);
        }
        this.d.clear();
        this.g = true;
    }

    public void k(T t) {
        Iterator<c<T>> it = this.d.iterator();
        while (it.hasNext()) {
            c<T> next = it.next();
            if (next.a.equals(t)) {
                next.c(this.c);
                this.d.remove(next);
            }
        }
    }

    public void l(int i, a<T> aVar) {
        i(i, aVar);
        f();
    }

    public o02(CopyOnWriteArraySet<c<T>> copyOnWriteArraySet, Looper looper, tz tzVar, b<T> bVar) {
        this.a = tzVar;
        this.d = copyOnWriteArraySet;
        this.c = bVar;
        this.e = new ArrayDeque<>();
        this.f = new ArrayDeque<>();
        this.b = tzVar.c(looper, new Handler.Callback() { // from class: m02
            @Override // android.os.Handler.Callback
            public final boolean handleMessage(Message message) {
                boolean g;
                g = o02.this.g(message);
                return g;
            }
        });
    }
}
