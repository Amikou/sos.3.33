package defpackage;

import android.content.ComponentName;
import android.content.Context;
import android.content.pm.ActivityInfo;
import android.content.pm.ComponentInfo;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.ProviderInfo;
import android.content.pm.ServiceInfo;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/* renamed from: zs4  reason: default package */
/* loaded from: classes2.dex */
public final class zs4 {
    public static final it4 c = new it4("MissingSplitsAppComponentsHelper");
    public final Context a;
    public final PackageManager b;

    public zs4(Context context, PackageManager packageManager) {
        this.a = context;
        this.b = packageManager;
    }

    public final boolean a() {
        for (ComponentInfo componentInfo : e()) {
            if (this.b.getComponentEnabledSetting(new ComponentName(componentInfo.packageName, componentInfo.name)) != 2) {
                c.a("Not all non-activity components are disabled", new Object[0]);
                return false;
            }
        }
        c.a("All non-activity components are disabled", new Object[0]);
        return true;
    }

    public final void b() {
        c.d("Disabling all non-activity components", new Object[0]);
        d(e(), 2);
    }

    public final void c() {
        c.d("Resetting enabled state of all non-activity components", new Object[0]);
        d(e(), 0);
    }

    public final void d(List<ComponentInfo> list, int i) {
        for (ComponentInfo componentInfo : list) {
            this.b.setComponentEnabledSetting(new ComponentName(componentInfo.packageName, componentInfo.name), i, 1);
        }
    }

    public final List<ComponentInfo> e() {
        try {
            ArrayList arrayList = new ArrayList();
            PackageInfo packageInfo = this.b.getPackageInfo(this.a.getPackageName(), 526);
            ProviderInfo[] providerInfoArr = packageInfo.providers;
            if (providerInfoArr != null) {
                Collections.addAll(arrayList, providerInfoArr);
            }
            ActivityInfo[] activityInfoArr = packageInfo.receivers;
            if (activityInfoArr != null) {
                Collections.addAll(arrayList, activityInfoArr);
            }
            ServiceInfo[] serviceInfoArr = packageInfo.services;
            if (serviceInfoArr != null) {
                Collections.addAll(arrayList, serviceInfoArr);
            }
            return arrayList;
        } catch (PackageManager.NameNotFoundException e) {
            c.e("Failed to resolve own package : %s", e);
            return Collections.emptyList();
        }
    }
}
