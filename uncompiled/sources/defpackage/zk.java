package defpackage;

import defpackage.r90;
import java.util.Objects;

/* compiled from: AutoValue_CrashlyticsReport_Session_Event_Application_Execution_Exception.java */
/* renamed from: zk  reason: default package */
/* loaded from: classes2.dex */
public final class zk extends r90.e.d.a.b.c {
    public final String a;
    public final String b;
    public final ip1<r90.e.d.a.b.AbstractC0272e.AbstractC0274b> c;
    public final r90.e.d.a.b.c d;
    public final int e;

    /* compiled from: AutoValue_CrashlyticsReport_Session_Event_Application_Execution_Exception.java */
    /* renamed from: zk$b */
    /* loaded from: classes2.dex */
    public static final class b extends r90.e.d.a.b.c.AbstractC0269a {
        public String a;
        public String b;
        public ip1<r90.e.d.a.b.AbstractC0272e.AbstractC0274b> c;
        public r90.e.d.a.b.c d;
        public Integer e;

        @Override // defpackage.r90.e.d.a.b.c.AbstractC0269a
        public r90.e.d.a.b.c a() {
            String str = "";
            if (this.a == null) {
                str = " type";
            }
            if (this.c == null) {
                str = str + " frames";
            }
            if (this.e == null) {
                str = str + " overflowCount";
            }
            if (str.isEmpty()) {
                return new zk(this.a, this.b, this.c, this.d, this.e.intValue());
            }
            throw new IllegalStateException("Missing required properties:" + str);
        }

        @Override // defpackage.r90.e.d.a.b.c.AbstractC0269a
        public r90.e.d.a.b.c.AbstractC0269a b(r90.e.d.a.b.c cVar) {
            this.d = cVar;
            return this;
        }

        @Override // defpackage.r90.e.d.a.b.c.AbstractC0269a
        public r90.e.d.a.b.c.AbstractC0269a c(ip1<r90.e.d.a.b.AbstractC0272e.AbstractC0274b> ip1Var) {
            Objects.requireNonNull(ip1Var, "Null frames");
            this.c = ip1Var;
            return this;
        }

        @Override // defpackage.r90.e.d.a.b.c.AbstractC0269a
        public r90.e.d.a.b.c.AbstractC0269a d(int i) {
            this.e = Integer.valueOf(i);
            return this;
        }

        @Override // defpackage.r90.e.d.a.b.c.AbstractC0269a
        public r90.e.d.a.b.c.AbstractC0269a e(String str) {
            this.b = str;
            return this;
        }

        @Override // defpackage.r90.e.d.a.b.c.AbstractC0269a
        public r90.e.d.a.b.c.AbstractC0269a f(String str) {
            Objects.requireNonNull(str, "Null type");
            this.a = str;
            return this;
        }
    }

    @Override // defpackage.r90.e.d.a.b.c
    public r90.e.d.a.b.c b() {
        return this.d;
    }

    @Override // defpackage.r90.e.d.a.b.c
    public ip1<r90.e.d.a.b.AbstractC0272e.AbstractC0274b> c() {
        return this.c;
    }

    @Override // defpackage.r90.e.d.a.b.c
    public int d() {
        return this.e;
    }

    @Override // defpackage.r90.e.d.a.b.c
    public String e() {
        return this.b;
    }

    public boolean equals(Object obj) {
        String str;
        r90.e.d.a.b.c cVar;
        if (obj == this) {
            return true;
        }
        if (obj instanceof r90.e.d.a.b.c) {
            r90.e.d.a.b.c cVar2 = (r90.e.d.a.b.c) obj;
            return this.a.equals(cVar2.f()) && ((str = this.b) != null ? str.equals(cVar2.e()) : cVar2.e() == null) && this.c.equals(cVar2.c()) && ((cVar = this.d) != null ? cVar.equals(cVar2.b()) : cVar2.b() == null) && this.e == cVar2.d();
        }
        return false;
    }

    @Override // defpackage.r90.e.d.a.b.c
    public String f() {
        return this.a;
    }

    public int hashCode() {
        int hashCode = (this.a.hashCode() ^ 1000003) * 1000003;
        String str = this.b;
        int hashCode2 = (((hashCode ^ (str == null ? 0 : str.hashCode())) * 1000003) ^ this.c.hashCode()) * 1000003;
        r90.e.d.a.b.c cVar = this.d;
        return ((hashCode2 ^ (cVar != null ? cVar.hashCode() : 0)) * 1000003) ^ this.e;
    }

    public String toString() {
        return "Exception{type=" + this.a + ", reason=" + this.b + ", frames=" + this.c + ", causedBy=" + this.d + ", overflowCount=" + this.e + "}";
    }

    public zk(String str, String str2, ip1<r90.e.d.a.b.AbstractC0272e.AbstractC0274b> ip1Var, r90.e.d.a.b.c cVar, int i) {
        this.a = str;
        this.b = str2;
        this.c = ip1Var;
        this.d = cVar;
        this.e = i;
    }
}
