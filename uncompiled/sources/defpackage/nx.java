package defpackage;

/* compiled from: FlowExt.kt */
/* renamed from: nx  reason: default package */
/* loaded from: classes.dex */
public final class nx<T> implements k71<T> {
    public final uj3<T> a;

    /* JADX WARN: Multi-variable type inference failed */
    public nx(uj3<? super T> uj3Var) {
        fs1.f(uj3Var, "channel");
        this.a = uj3Var;
    }

    @Override // defpackage.k71
    public Object emit(T t, q70<? super te4> q70Var) {
        Object h = this.a.h(t, q70Var);
        return h == gs1.d() ? h : te4.a;
    }
}
