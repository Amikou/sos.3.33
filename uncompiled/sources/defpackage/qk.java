package defpackage;

import defpackage.r90;
import java.util.Arrays;
import java.util.Objects;

/* compiled from: AutoValue_CrashlyticsReport_FilesPayload_File.java */
/* renamed from: qk  reason: default package */
/* loaded from: classes2.dex */
public final class qk extends r90.d.b {
    public final String a;
    public final byte[] b;

    /* compiled from: AutoValue_CrashlyticsReport_FilesPayload_File.java */
    /* renamed from: qk$b */
    /* loaded from: classes2.dex */
    public static final class b extends r90.d.b.a {
        public String a;
        public byte[] b;

        @Override // defpackage.r90.d.b.a
        public r90.d.b a() {
            String str = "";
            if (this.a == null) {
                str = " filename";
            }
            if (this.b == null) {
                str = str + " contents";
            }
            if (str.isEmpty()) {
                return new qk(this.a, this.b);
            }
            throw new IllegalStateException("Missing required properties:" + str);
        }

        @Override // defpackage.r90.d.b.a
        public r90.d.b.a b(byte[] bArr) {
            Objects.requireNonNull(bArr, "Null contents");
            this.b = bArr;
            return this;
        }

        @Override // defpackage.r90.d.b.a
        public r90.d.b.a c(String str) {
            Objects.requireNonNull(str, "Null filename");
            this.a = str;
            return this;
        }
    }

    @Override // defpackage.r90.d.b
    public byte[] b() {
        return this.b;
    }

    @Override // defpackage.r90.d.b
    public String c() {
        return this.a;
    }

    public boolean equals(Object obj) {
        if (obj == this) {
            return true;
        }
        if (obj instanceof r90.d.b) {
            r90.d.b bVar = (r90.d.b) obj;
            if (this.a.equals(bVar.c())) {
                if (Arrays.equals(this.b, bVar instanceof qk ? ((qk) bVar).b : bVar.b())) {
                    return true;
                }
            }
            return false;
        }
        return false;
    }

    public int hashCode() {
        return ((this.a.hashCode() ^ 1000003) * 1000003) ^ Arrays.hashCode(this.b);
    }

    public String toString() {
        return "File{filename=" + this.a + ", contents=" + Arrays.toString(this.b) + "}";
    }

    public qk(String str, byte[] bArr) {
        this.a = str;
        this.b = bArr;
    }
}
