package defpackage;

import org.web3j.ens.EnsResolutionException;

/* compiled from: Contracts.java */
/* renamed from: k80  reason: default package */
/* loaded from: classes3.dex */
public class k80 {
    public static final String MAINNET = "0x00000000000C2E074eC69A0dFb2997BA6C7d2e1e";
    public static final String RINKEBY = "0x00000000000C2E074eC69A0dFb2997BA6C7d2e1e";
    public static final String ROPSTEN = "0x00000000000C2E074eC69A0dFb2997BA6C7d2e1e";

    public static String resolveRegistryContract(String str) {
        Long valueOf = Long.valueOf(Long.parseLong(str));
        if (valueOf.equals(1L) || valueOf.equals(3L) || valueOf.equals(4L)) {
            return "0x00000000000C2E074eC69A0dFb2997BA6C7d2e1e";
        }
        throw new EnsResolutionException("Unable to resolve ENS registry contract for network id: " + str);
    }
}
