package defpackage;

import android.os.Parcel;
import android.os.Parcelable;
import com.google.android.gms.common.internal.safeparcel.SafeParcelReader;
import com.google.android.gms.internal.clearcut.zzr;

/* renamed from: n46  reason: default package */
/* loaded from: classes.dex */
public final class n46 implements Parcelable.Creator<zzr> {
    @Override // android.os.Parcelable.Creator
    public final /* synthetic */ zzr createFromParcel(Parcel parcel) {
        int J = SafeParcelReader.J(parcel);
        int i = 0;
        int i2 = 0;
        boolean z = false;
        int i3 = 0;
        String str = null;
        String str2 = null;
        String str3 = null;
        String str4 = null;
        boolean z2 = true;
        while (parcel.dataPosition() < J) {
            int C = SafeParcelReader.C(parcel);
            switch (SafeParcelReader.v(C)) {
                case 2:
                    str = SafeParcelReader.p(parcel, C);
                    break;
                case 3:
                    i = SafeParcelReader.E(parcel, C);
                    break;
                case 4:
                    i2 = SafeParcelReader.E(parcel, C);
                    break;
                case 5:
                    str2 = SafeParcelReader.p(parcel, C);
                    break;
                case 6:
                    str3 = SafeParcelReader.p(parcel, C);
                    break;
                case 7:
                    z2 = SafeParcelReader.w(parcel, C);
                    break;
                case 8:
                    str4 = SafeParcelReader.p(parcel, C);
                    break;
                case 9:
                    z = SafeParcelReader.w(parcel, C);
                    break;
                case 10:
                    i3 = SafeParcelReader.E(parcel, C);
                    break;
                default:
                    SafeParcelReader.I(parcel, C);
                    break;
            }
        }
        SafeParcelReader.u(parcel, J);
        return new zzr(str, i, i2, str2, str3, z2, str4, z, i3);
    }

    @Override // android.os.Parcelable.Creator
    public final /* synthetic */ zzr[] newArray(int i) {
        return new zzr[i];
    }
}
