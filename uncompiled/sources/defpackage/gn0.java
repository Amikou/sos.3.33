package defpackage;

import android.view.View;
import androidx.cardview.widget.CardView;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.recyclerview.widget.RecyclerView;
import net.safemoon.androidwallet.R;

/* compiled from: DialogAnchorPopUpMyTokensBinding.java */
/* renamed from: gn0  reason: default package */
/* loaded from: classes2.dex */
public final class gn0 {
    public final RecyclerView a;
    public final zd3 b;

    public gn0(ConstraintLayout constraintLayout, CardView cardView, RecyclerView recyclerView, zd3 zd3Var) {
        this.a = recyclerView;
        this.b = zd3Var;
    }

    public static gn0 a(View view) {
        int i = R.id.ccItemWrapper;
        CardView cardView = (CardView) ai4.a(view, R.id.ccItemWrapper);
        if (cardView != null) {
            i = R.id.recyclerView;
            RecyclerView recyclerView = (RecyclerView) ai4.a(view, R.id.recyclerView);
            if (recyclerView != null) {
                i = R.id.searchBar;
                View a = ai4.a(view, R.id.searchBar);
                if (a != null) {
                    return new gn0((ConstraintLayout) view, cardView, recyclerView, zd3.a(a));
                }
            }
        }
        throw new NullPointerException("Missing required view with ID: ".concat(view.getResources().getResourceName(i)));
    }
}
