package defpackage;

import com.google.android.gms.internal.clearcut.zzbb;

/* renamed from: jh5  reason: default package */
/* loaded from: classes.dex */
public final class jh5 implements mh5 {
    public final /* synthetic */ zzbb a;

    public jh5(zzbb zzbbVar) {
        this.a = zzbbVar;
    }

    @Override // defpackage.mh5
    public final byte a(int i) {
        return this.a.zzj(i);
    }

    @Override // defpackage.mh5
    public final int size() {
        return this.a.size();
    }
}
