package defpackage;

import android.content.res.Resources;
import android.text.TextUtils;
import androidx.media3.common.j;
import androidx.media3.common.util.b;
import java.util.Locale;

/* compiled from: DefaultTrackNameProvider.java */
/* renamed from: zk0  reason: default package */
/* loaded from: classes.dex */
public class zk0 implements d84 {
    public final Resources a;

    public zk0(Resources resources) {
        this.a = (Resources) ii.e(resources);
    }

    public static int i(j jVar) {
        int i = y82.i(jVar.p0);
        if (i != -1) {
            return i;
        }
        if (y82.l(jVar.m0) != null) {
            return 2;
        }
        if (y82.b(jVar.m0) != null) {
            return 1;
        }
        if (jVar.u0 == -1 && jVar.v0 == -1) {
            return (jVar.C0 == -1 && jVar.D0 == -1) ? -1 : 1;
        }
        return 2;
    }

    @Override // defpackage.d84
    public String a(j jVar) {
        String e;
        int i = i(jVar);
        if (i == 2) {
            e = j(h(jVar), g(jVar), c(jVar));
        } else if (i == 1) {
            e = j(e(jVar), b(jVar), c(jVar));
        } else {
            e = e(jVar);
        }
        return e.length() == 0 ? this.a.getString(m13.exo_track_unknown) : e;
    }

    public final String b(j jVar) {
        int i = jVar.C0;
        if (i == -1 || i < 1) {
            return "";
        }
        if (i != 1) {
            if (i != 2) {
                if (i == 6 || i == 7) {
                    return this.a.getString(m13.exo_track_surround_5_point_1);
                }
                if (i != 8) {
                    return this.a.getString(m13.exo_track_surround);
                }
                return this.a.getString(m13.exo_track_surround_7_point_1);
            }
            return this.a.getString(m13.exo_track_stereo);
        }
        return this.a.getString(m13.exo_track_mono);
    }

    public final String c(j jVar) {
        int i = jVar.l0;
        return i == -1 ? "" : this.a.getString(m13.exo_track_bitrate, Float.valueOf(i / 1000000.0f));
    }

    public final String d(j jVar) {
        return TextUtils.isEmpty(jVar.f0) ? "" : jVar.f0;
    }

    public final String e(j jVar) {
        String j = j(f(jVar), h(jVar));
        return TextUtils.isEmpty(j) ? d(jVar) : j;
    }

    public final String f(j jVar) {
        String str = jVar.g0;
        if (TextUtils.isEmpty(str) || "und".equals(str)) {
            return "";
        }
        Locale forLanguageTag = b.a >= 21 ? Locale.forLanguageTag(str) : new Locale(str);
        Locale N = b.N();
        String displayName = forLanguageTag.getDisplayName(N);
        if (TextUtils.isEmpty(displayName)) {
            return "";
        }
        try {
            int offsetByCodePoints = displayName.offsetByCodePoints(0, 1);
            return displayName.substring(0, offsetByCodePoints).toUpperCase(N) + displayName.substring(offsetByCodePoints);
        } catch (IndexOutOfBoundsException unused) {
            return displayName;
        }
    }

    public final String g(j jVar) {
        int i = jVar.u0;
        int i2 = jVar.v0;
        return (i == -1 || i2 == -1) ? "" : this.a.getString(m13.exo_track_resolution, Integer.valueOf(i), Integer.valueOf(i2));
    }

    public final String h(j jVar) {
        String string = (jVar.i0 & 2) != 0 ? this.a.getString(m13.exo_track_role_alternate) : "";
        if ((jVar.i0 & 4) != 0) {
            string = j(string, this.a.getString(m13.exo_track_role_supplementary));
        }
        if ((jVar.i0 & 8) != 0) {
            string = j(string, this.a.getString(m13.exo_track_role_commentary));
        }
        return (jVar.i0 & 1088) != 0 ? j(string, this.a.getString(m13.exo_track_role_closed_captions)) : string;
    }

    public final String j(String... strArr) {
        String str = "";
        for (String str2 : strArr) {
            if (str2.length() > 0) {
                str = TextUtils.isEmpty(str) ? str2 : this.a.getString(m13.exo_item_list, str, str2);
            }
        }
        return str;
    }
}
