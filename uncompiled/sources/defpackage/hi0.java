package defpackage;

import com.fasterxml.jackson.annotation.r;
import java.math.BigInteger;

/* compiled from: DefaultBlockParameterNumber.java */
/* renamed from: hi0  reason: default package */
/* loaded from: classes3.dex */
public class hi0 implements gi0 {
    private BigInteger blockNumber;

    public hi0(BigInteger bigInteger) {
        this.blockNumber = bigInteger;
    }

    public BigInteger getBlockNumber() {
        return this.blockNumber;
    }

    @Override // defpackage.gi0
    @r
    public String getValue() {
        return ej2.encodeQuantity(this.blockNumber);
    }

    public hi0(long j) {
        this(BigInteger.valueOf(j));
    }
}
