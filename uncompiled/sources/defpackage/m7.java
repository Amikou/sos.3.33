package defpackage;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import androidx.appcompat.widget.AppCompatImageView;
import androidx.appcompat.widget.AppCompatTextView;
import androidx.appcompat.widget.LinearLayoutCompat;
import androidx.constraintlayout.widget.ConstraintLayout;
import com.google.android.material.button.MaterialButton;
import com.google.android.material.checkbox.MaterialCheckBox;
import com.google.android.material.textview.MaterialTextView;
import net.safemoon.androidwallet.R;

/* compiled from: ActivityMainBinding.java */
/* renamed from: m7  reason: default package */
/* loaded from: classes2.dex */
public final class m7 {
    public final ConstraintLayout a;
    public final MaterialCheckBox b;
    public final MaterialTextView c;
    public final AppCompatTextView d;
    public final MaterialButton e;
    public final MaterialTextView f;

    public m7(ConstraintLayout constraintLayout, MaterialCheckBox materialCheckBox, MaterialTextView materialTextView, ConstraintLayout constraintLayout2, AppCompatTextView appCompatTextView, ImageView imageView, AppCompatImageView appCompatImageView, AppCompatImageView appCompatImageView2, AppCompatImageView appCompatImageView3, LinearLayoutCompat linearLayoutCompat, MaterialButton materialButton, MaterialTextView materialTextView2, TextView textView, TextView textView2) {
        this.a = constraintLayout;
        this.b = materialCheckBox;
        this.c = materialTextView;
        this.d = appCompatTextView;
        this.e = materialButton;
        this.f = materialTextView2;
    }

    public static m7 a(View view) {
        int i = R.id.chkTermsAgree;
        MaterialCheckBox materialCheckBox = (MaterialCheckBox) ai4.a(view, R.id.chkTermsAgree);
        if (materialCheckBox != null) {
            i = R.id.chkTermsAgreeTC;
            MaterialTextView materialTextView = (MaterialTextView) ai4.a(view, R.id.chkTermsAgreeTC);
            if (materialTextView != null) {
                ConstraintLayout constraintLayout = (ConstraintLayout) view;
                i = R.id.create_wallet;
                AppCompatTextView appCompatTextView = (AppCompatTextView) ai4.a(view, R.id.create_wallet);
                if (appCompatTextView != null) {
                    i = R.id.imageView16;
                    ImageView imageView = (ImageView) ai4.a(view, R.id.imageView16);
                    if (imageView != null) {
                        i = R.id.img_logo;
                        AppCompatImageView appCompatImageView = (AppCompatImageView) ai4.a(view, R.id.img_logo);
                        if (appCompatImageView != null) {
                            i = R.id.img_safemoon;
                            AppCompatImageView appCompatImageView2 = (AppCompatImageView) ai4.a(view, R.id.img_safemoon);
                            if (appCompatImageView2 != null) {
                                i = R.id.img_wallet;
                                AppCompatImageView appCompatImageView3 = (AppCompatImageView) ai4.a(view, R.id.img_wallet);
                                if (appCompatImageView3 != null) {
                                    i = R.id.img_wallet_parent;
                                    LinearLayoutCompat linearLayoutCompat = (LinearLayoutCompat) ai4.a(view, R.id.img_wallet_parent);
                                    if (linearLayoutCompat != null) {
                                        i = R.id.iv_back;
                                        MaterialButton materialButton = (MaterialButton) ai4.a(view, R.id.iv_back);
                                        if (materialButton != null) {
                                            i = R.id.openExtWallet;
                                            MaterialTextView materialTextView2 = (MaterialTextView) ai4.a(view, R.id.openExtWallet);
                                            if (materialTextView2 != null) {
                                                i = R.id.textView;
                                                TextView textView = (TextView) ai4.a(view, R.id.textView);
                                                if (textView != null) {
                                                    i = R.id.textView2;
                                                    TextView textView2 = (TextView) ai4.a(view, R.id.textView2);
                                                    if (textView2 != null) {
                                                        return new m7(constraintLayout, materialCheckBox, materialTextView, constraintLayout, appCompatTextView, imageView, appCompatImageView, appCompatImageView2, appCompatImageView3, linearLayoutCompat, materialButton, materialTextView2, textView, textView2);
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
        throw new NullPointerException("Missing required view with ID: ".concat(view.getResources().getResourceName(i)));
    }

    public static m7 c(LayoutInflater layoutInflater) {
        return d(layoutInflater, null, false);
    }

    public static m7 d(LayoutInflater layoutInflater, ViewGroup viewGroup, boolean z) {
        View inflate = layoutInflater.inflate(R.layout.activity_main, viewGroup, false);
        if (z) {
            viewGroup.addView(inflate);
        }
        return a(inflate);
    }

    public ConstraintLayout b() {
        return this.a;
    }
}
