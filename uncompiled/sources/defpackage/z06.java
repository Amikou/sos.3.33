package defpackage;

import java.lang.ref.ReferenceQueue;
import java.lang.ref.WeakReference;

/* compiled from: com.google.firebase:firebase-messaging@@22.0.0 */
/* renamed from: z06  reason: default package */
/* loaded from: classes.dex */
public final class z06 extends WeakReference<Throwable> {
    public final int a;

    public z06(Throwable th, ReferenceQueue<Throwable> referenceQueue) {
        super(th, referenceQueue);
        this.a = System.identityHashCode(th);
    }

    public final boolean equals(Object obj) {
        if (obj != null && obj.getClass() == z06.class) {
            if (this == obj) {
                return true;
            }
            z06 z06Var = (z06) obj;
            if (this.a == z06Var.a && get() == z06Var.get()) {
                return true;
            }
        }
        return false;
    }

    public final int hashCode() {
        return this.a;
    }
}
