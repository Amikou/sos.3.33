package defpackage;

import com.github.mikephil.charting.utils.Utils;

/* compiled from: VelocityMatrix.java */
/* renamed from: fh4  reason: default package */
/* loaded from: classes.dex */
public class fh4 {
    public float a;
    public float b;
    public float c;
    public float d;
    public float e;
    public float f;

    public void a(float f, float f2, int i, int i2, float[] fArr) {
        float f3;
        float f4 = fArr[0];
        float f5 = fArr[1];
        float f6 = (f2 - 0.5f) * 2.0f;
        float f7 = f4 + this.c;
        float f8 = f5 + this.d;
        float f9 = f7 + (this.a * (f - 0.5f) * 2.0f);
        float f10 = f8 + (this.b * f6);
        float radians = (float) Math.toRadians(this.e);
        double radians2 = (float) Math.toRadians(this.f);
        double d = i2 * f6;
        fArr[0] = f9 + (((float) ((((-i) * f3) * Math.sin(radians2)) - (Math.cos(radians2) * d))) * radians);
        fArr[1] = f10 + (radians * ((float) (((i * f3) * Math.cos(radians2)) - (d * Math.sin(radians2)))));
    }

    public void b() {
        this.e = Utils.FLOAT_EPSILON;
        this.d = Utils.FLOAT_EPSILON;
        this.c = Utils.FLOAT_EPSILON;
        this.b = Utils.FLOAT_EPSILON;
        this.a = Utils.FLOAT_EPSILON;
    }

    public void c(hx1 hx1Var, float f) {
        if (hx1Var != null) {
            this.e = hx1Var.b(f);
        }
    }

    public void d(sr3 sr3Var, float f) {
        if (sr3Var != null) {
            this.e = sr3Var.b(f);
            this.f = sr3Var.a(f);
        }
    }

    public void e(hx1 hx1Var, hx1 hx1Var2, float f) {
        if (hx1Var != null) {
            this.a = hx1Var.b(f);
        }
        if (hx1Var2 != null) {
            this.b = hx1Var2.b(f);
        }
    }

    public void f(sr3 sr3Var, sr3 sr3Var2, float f) {
        if (sr3Var != null) {
            this.a = sr3Var.b(f);
        }
        if (sr3Var2 != null) {
            this.b = sr3Var2.b(f);
        }
    }

    public void g(hx1 hx1Var, hx1 hx1Var2, float f) {
        if (hx1Var != null) {
            this.c = hx1Var.b(f);
        }
        if (hx1Var2 != null) {
            this.d = hx1Var2.b(f);
        }
    }

    public void h(sr3 sr3Var, sr3 sr3Var2, float f) {
        if (sr3Var != null) {
            this.c = sr3Var.b(f);
        }
        if (sr3Var2 != null) {
            this.d = sr3Var2.b(f);
        }
    }
}
