package defpackage;

import com.fasterxml.jackson.core.Base64Variant;
import com.fasterxml.jackson.core.JsonLocation;
import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.JsonToken;
import com.fasterxml.jackson.core.c;
import com.fasterxml.jackson.core.io.a;
import java.io.IOException;
import java.io.OutputStream;
import java.io.Reader;
import org.web3j.ens.contracts.generated.PublicResolver;

/* compiled from: ReaderBasedJsonParser.java */
/* renamed from: b43  reason: default package */
/* loaded from: classes.dex */
public class b43 extends qp2 {
    public static final int[] Y0 = a.g();
    public Reader O0;
    public char[] P0;
    public boolean Q0;
    public c R0;
    public final zx S0;
    public final int T0;
    public boolean U0;
    public long V0;
    public int W0;
    public int X0;

    public b43(jm1 jm1Var, int i, Reader reader, c cVar, zx zxVar, char[] cArr, int i2, int i3, boolean z) {
        super(jm1Var, i);
        this.O0 = reader;
        this.P0 = cArr;
        this.j0 = i2;
        this.k0 = i3;
        this.R0 = cVar;
        this.S0 = zxVar;
        this.T0 = zxVar.p();
        this.Q0 = z;
    }

    public void A2() throws IOException {
        if (z2()) {
            return;
        }
        y1();
    }

    public final void B2() throws IOException {
        int i;
        char c;
        int i2 = this.j0;
        if (i2 + 4 < this.k0) {
            char[] cArr = this.P0;
            if (cArr[i2] == 'a') {
                int i3 = i2 + 1;
                if (cArr[i3] == 'l') {
                    int i4 = i3 + 1;
                    if (cArr[i4] == 's') {
                        int i5 = i4 + 1;
                        if (cArr[i5] == 'e' && ((c = cArr[(i = i5 + 1)]) < '0' || c == ']' || c == '}')) {
                            this.j0 = i;
                            return;
                        }
                    }
                }
            }
        }
        D2("false", 1);
    }

    @Override // com.fasterxml.jackson.core.JsonParser
    public Object C() {
        return this.O0;
    }

    public final void C2() throws IOException {
        int i;
        char c;
        int i2 = this.j0;
        if (i2 + 3 < this.k0) {
            char[] cArr = this.P0;
            if (cArr[i2] == 'u') {
                int i3 = i2 + 1;
                if (cArr[i3] == 'l') {
                    int i4 = i3 + 1;
                    if (cArr[i4] == 'l' && ((c = cArr[(i = i4 + 1)]) < '0' || c == ']' || c == '}')) {
                        this.j0 = i;
                        return;
                    }
                }
            }
        }
        D2("null", 1);
    }

    public final void D2(String str, int i) throws IOException {
        int i2;
        char c;
        int length = str.length();
        do {
            if (this.j0 >= this.k0 && !z2()) {
                P2(str.substring(0, i));
            }
            if (this.P0[this.j0] != str.charAt(i)) {
                P2(str.substring(0, i));
            }
            i2 = this.j0 + 1;
            this.j0 = i2;
            i++;
        } while (i < length);
        if ((i2 < this.k0 || z2()) && (c = this.P0[this.j0]) >= '0' && c != ']' && c != '}' && Character.isJavaIdentifierPart(c)) {
            P2(str.substring(0, i));
        }
    }

    public final void E2() throws IOException {
        int i;
        char c;
        int i2 = this.j0;
        if (i2 + 3 < this.k0) {
            char[] cArr = this.P0;
            if (cArr[i2] == 'r') {
                int i3 = i2 + 1;
                if (cArr[i3] == 'u') {
                    int i4 = i3 + 1;
                    if (cArr[i4] == 'e' && ((c = cArr[(i = i4 + 1)]) < '0' || c == ']' || c == '}')) {
                        this.j0 = i;
                        return;
                    }
                }
            }
        }
        D2("true", 1);
    }

    public final JsonToken F2() {
        this.v0 = false;
        JsonToken jsonToken = this.s0;
        this.s0 = null;
        if (jsonToken == JsonToken.START_ARRAY) {
            this.r0 = this.r0.l(this.p0, this.q0);
        } else if (jsonToken == JsonToken.START_OBJECT) {
            this.r0 = this.r0.m(this.p0, this.q0);
        }
        this.g0 = jsonToken;
        return jsonToken;
    }

    public final JsonToken G2(int i) throws IOException {
        if (i == 34) {
            this.U0 = true;
            JsonToken jsonToken = JsonToken.VALUE_STRING;
            this.g0 = jsonToken;
            return jsonToken;
        }
        if (i != 44) {
            if (i == 45) {
                JsonToken L2 = L2();
                this.g0 = L2;
                return L2;
            } else if (i == 91) {
                this.r0 = this.r0.l(this.p0, this.q0);
                JsonToken jsonToken2 = JsonToken.START_ARRAY;
                this.g0 = jsonToken2;
                return jsonToken2;
            } else if (i != 93) {
                if (i == 102) {
                    D2("false", 1);
                    JsonToken jsonToken3 = JsonToken.VALUE_FALSE;
                    this.g0 = jsonToken3;
                    return jsonToken3;
                } else if (i == 110) {
                    D2("null", 1);
                    JsonToken jsonToken4 = JsonToken.VALUE_NULL;
                    this.g0 = jsonToken4;
                    return jsonToken4;
                } else if (i == 116) {
                    D2("true", 1);
                    JsonToken jsonToken5 = JsonToken.VALUE_TRUE;
                    this.g0 = jsonToken5;
                    return jsonToken5;
                } else if (i != 123) {
                    switch (i) {
                        case 48:
                        case 49:
                        case 50:
                        case 51:
                        case 52:
                        case 53:
                        case 54:
                        case 55:
                        case 56:
                        case 57:
                            JsonToken N2 = N2(i);
                            this.g0 = N2;
                            return N2;
                    }
                    JsonToken y2 = y2(i);
                    this.g0 = y2;
                    return y2;
                } else {
                    this.r0 = this.r0.m(this.p0, this.q0);
                    JsonToken jsonToken6 = JsonToken.START_OBJECT;
                    this.g0 = jsonToken6;
                    return jsonToken6;
                }
            }
        }
        if (J0(JsonParser.Feature.ALLOW_MISSING_VALUES)) {
            this.j0--;
            JsonToken jsonToken7 = JsonToken.VALUE_NULL;
            this.g0 = jsonToken7;
            return jsonToken7;
        }
        JsonToken y22 = y2(i);
        this.g0 = y22;
        return y22;
    }

    public String H2() throws IOException {
        int i = this.j0;
        int i2 = this.T0;
        int i3 = this.k0;
        if (i < i3) {
            int[] iArr = Y0;
            int length = iArr.length;
            do {
                char[] cArr = this.P0;
                char c = cArr[i];
                if (c == '\'') {
                    int i4 = this.j0;
                    this.j0 = i + 1;
                    return this.S0.o(cArr, i4, i - i4, i2);
                } else if (c < length && iArr[c] != 0) {
                    break;
                } else {
                    i2 = (i2 * 33) + c;
                    i++;
                }
            } while (i < i3);
        }
        int i5 = this.j0;
        this.j0 = i;
        return K2(i5, i2, 39);
    }

    /*  JADX ERROR: JadxRuntimeException in pass: InitCodeVariables
        jadx.core.utils.exceptions.JadxRuntimeException: Several immutable types in one variable: [int, char], vars: [r9v0 ??, r9v1 ??, r9v18 ??, r9v12 ??, r9v5 ??, r9v3 ??, r9v9 ??, r9v7 ??, r9v10 ??]
        	at jadx.core.dex.visitors.InitCodeVariables.setCodeVarType(InitCodeVariables.java:107)
        	at jadx.core.dex.visitors.InitCodeVariables.setCodeVar(InitCodeVariables.java:83)
        	at jadx.core.dex.visitors.InitCodeVariables.initCodeVar(InitCodeVariables.java:74)
        	at jadx.core.dex.visitors.InitCodeVariables.initCodeVar(InitCodeVariables.java:57)
        	at jadx.core.dex.visitors.InitCodeVariables.initCodeVars(InitCodeVariables.java:45)
        	at jadx.core.dex.visitors.InitCodeVariables.visit(InitCodeVariables.java:29)
        */
    /* JADX WARN: Unsupported multi-entry loop pattern (BACK_EDGE: B:41:0x006e -> B:30:0x0050). Please submit an issue!!! */
    public final com.fasterxml.jackson.core.JsonToken I2(
    /*  JADX ERROR: JadxRuntimeException in pass: InitCodeVariables
        jadx.core.utils.exceptions.JadxRuntimeException: Several immutable types in one variable: [int, char], vars: [r9v0 ??, r9v1 ??, r9v18 ??, r9v12 ??, r9v5 ??, r9v3 ??, r9v9 ??, r9v7 ??, r9v10 ??]
        	at jadx.core.dex.visitors.InitCodeVariables.setCodeVarType(InitCodeVariables.java:107)
        	at jadx.core.dex.visitors.InitCodeVariables.setCodeVar(InitCodeVariables.java:83)
        	at jadx.core.dex.visitors.InitCodeVariables.initCodeVar(InitCodeVariables.java:74)
        	at jadx.core.dex.visitors.InitCodeVariables.initCodeVar(InitCodeVariables.java:57)
        	at jadx.core.dex.visitors.InitCodeVariables.initCodeVars(InitCodeVariables.java:45)
        */
    /*  JADX ERROR: Method generation error
        jadx.core.utils.exceptions.JadxRuntimeException: Code variable not set in r9v0 ??
        	at jadx.core.dex.instructions.args.SSAVar.getCodeVar(SSAVar.java:227)
        	at jadx.core.codegen.MethodGen.addMethodArguments(MethodGen.java:222)
        	at jadx.core.codegen.MethodGen.addDefinition(MethodGen.java:167)
        	at jadx.core.codegen.ClassGen.addMethodCode(ClassGen.java:372)
        	at jadx.core.codegen.ClassGen.addMethod(ClassGen.java:306)
        	at jadx.core.codegen.ClassGen.lambda$addInnerClsAndMethods$2(ClassGen.java:272)
        	at java.base/java.util.stream.ForEachOps$ForEachOp$OfRef.accept(ForEachOps.java:183)
        	at java.base/java.util.ArrayList.forEach(ArrayList.java:1511)
        	at java.base/java.util.stream.SortedOps$RefSortingSink.end(SortedOps.java:395)
        	at java.base/java.util.stream.Sink$ChainedReference.end(Sink.java:258)
        */

    public final String J2() throws IOException {
        int i = this.j0;
        int i2 = this.T0;
        int[] iArr = Y0;
        while (true) {
            if (i >= this.k0) {
                break;
            }
            char[] cArr = this.P0;
            char c = cArr[i];
            if (c >= iArr.length || iArr[c] == 0) {
                i2 = (i2 * 33) + c;
                i++;
            } else if (c == '\"') {
                int i3 = this.j0;
                this.j0 = i + 1;
                return this.S0.o(cArr, i3, i - i3, i2);
            }
        }
        int i4 = this.j0;
        this.j0 = i;
        return K2(i4, i2, 34);
    }

    public final String K2(int i, int i2, int i3) throws IOException {
        this.t0.v(this.P0, i, this.j0 - i);
        char[] o = this.t0.o();
        int p = this.t0.p();
        while (true) {
            if (this.j0 >= this.k0 && !z2()) {
                z1(" in field name", JsonToken.FIELD_NAME);
            }
            char[] cArr = this.P0;
            int i4 = this.j0;
            this.j0 = i4 + 1;
            char c = cArr[i4];
            if (c <= '\\') {
                if (c == '\\') {
                    c = R1();
                } else if (c <= i3) {
                    if (c == i3) {
                        this.t0.z(p);
                        com.fasterxml.jackson.core.util.c cVar = this.t0;
                        return this.S0.o(cVar.q(), cVar.r(), cVar.A(), i2);
                    } else if (c < ' ') {
                        L1(c, PublicResolver.FUNC_NAME);
                    }
                }
            }
            i2 = (i2 * 33) + c;
            int i5 = p + 1;
            o[p] = c;
            if (i5 >= o.length) {
                o = this.t0.n();
                p = 0;
            } else {
                p = i5;
            }
        }
    }

    public final JsonToken L2() throws IOException {
        int i = this.j0;
        int i2 = i - 1;
        int i3 = this.k0;
        if (i >= i3) {
            return M2(true, i2);
        }
        int i4 = i + 1;
        char c = this.P0[i];
        if (c > '9' || c < '0') {
            this.j0 = i4;
            return v2(c, true);
        } else if (c == '0') {
            return M2(true, i2);
        } else {
            int i5 = 1;
            while (i4 < i3) {
                int i6 = i4 + 1;
                char c2 = this.P0[i4];
                if (c2 < '0' || c2 > '9') {
                    if (c2 != '.' && c2 != 'e' && c2 != 'E') {
                        int i7 = i6 - 1;
                        this.j0 = i7;
                        if (this.r0.g()) {
                            h3(c2);
                        }
                        this.t0.v(this.P0, i2, i7 - i2);
                        return p2(true, i5);
                    }
                    this.j0 = i6;
                    return I2(c2, i2, i6, true, i5);
                }
                i5++;
                i4 = i6;
            }
            return M2(true, i2);
        }
    }

    public final JsonToken M2(boolean z, int i) throws IOException {
        int i2;
        char j3;
        boolean z2;
        int i3;
        char i32;
        if (z) {
            i++;
        }
        this.j0 = i;
        char[] k = this.t0.k();
        int i4 = 0;
        if (z) {
            k[0] = '-';
            i2 = 1;
        } else {
            i2 = 0;
        }
        int i5 = this.j0;
        if (i5 < this.k0) {
            char[] cArr = this.P0;
            this.j0 = i5 + 1;
            j3 = cArr[i5];
        } else {
            j3 = j3("No digit following minus sign", JsonToken.VALUE_NUMBER_INT);
        }
        if (j3 == '0') {
            j3 = g3();
        }
        int i6 = 0;
        while (j3 >= '0' && j3 <= '9') {
            i6++;
            if (i2 >= k.length) {
                k = this.t0.n();
                i2 = 0;
            }
            int i7 = i2 + 1;
            k[i2] = j3;
            if (this.j0 >= this.k0 && !z2()) {
                j3 = 0;
                i2 = i7;
                z2 = true;
                break;
            }
            char[] cArr2 = this.P0;
            int i8 = this.j0;
            this.j0 = i8 + 1;
            j3 = cArr2[i8];
            i2 = i7;
        }
        z2 = false;
        if (i6 == 0) {
            return v2(j3, z);
        }
        if (j3 == '.') {
            if (i2 >= k.length) {
                k = this.t0.n();
                i2 = 0;
            }
            k[i2] = j3;
            i2++;
            i3 = 0;
            while (true) {
                if (this.j0 >= this.k0 && !z2()) {
                    z2 = true;
                    break;
                }
                char[] cArr3 = this.P0;
                int i9 = this.j0;
                this.j0 = i9 + 1;
                j3 = cArr3[i9];
                if (j3 < '0' || j3 > '9') {
                    break;
                }
                i3++;
                if (i2 >= k.length) {
                    k = this.t0.n();
                    i2 = 0;
                }
                k[i2] = j3;
                i2++;
            }
            if (i3 == 0) {
                l2(j3, "Decimal point not followed by a digit");
            }
        } else {
            i3 = 0;
        }
        if (j3 == 'e' || j3 == 'E') {
            if (i2 >= k.length) {
                k = this.t0.n();
                i2 = 0;
            }
            int i10 = i2 + 1;
            k[i2] = j3;
            int i11 = this.j0;
            if (i11 < this.k0) {
                char[] cArr4 = this.P0;
                this.j0 = i11 + 1;
                i32 = cArr4[i11];
            } else {
                i32 = i3("expected a digit for number exponent");
            }
            if (i32 == '-' || i32 == '+') {
                if (i10 >= k.length) {
                    k = this.t0.n();
                    i10 = 0;
                }
                int i12 = i10 + 1;
                k[i10] = i32;
                int i13 = this.j0;
                if (i13 < this.k0) {
                    char[] cArr5 = this.P0;
                    this.j0 = i13 + 1;
                    i32 = cArr5[i13];
                } else {
                    i32 = i3("expected a digit for number exponent");
                }
                i10 = i12;
            }
            int i14 = 0;
            j3 = i32;
            while (j3 <= '9' && j3 >= '0') {
                i14++;
                if (i10 >= k.length) {
                    k = this.t0.n();
                    i10 = 0;
                }
                i2 = i10 + 1;
                k[i10] = j3;
                if (this.j0 >= this.k0 && !z2()) {
                    i4 = i14;
                    z2 = true;
                    break;
                }
                char[] cArr6 = this.P0;
                int i15 = this.j0;
                this.j0 = i15 + 1;
                j3 = cArr6[i15];
                i10 = i2;
            }
            i4 = i14;
            i2 = i10;
            if (i4 == 0) {
                l2(j3, "Exponent indicator not followed by a digit");
            }
        }
        if (!z2) {
            this.j0--;
            if (this.r0.g()) {
                h3(j3);
            }
        }
        this.t0.z(i2);
        return m2(z, i6, i3, i4);
    }

    public final JsonToken N2(int i) throws IOException {
        int i2 = this.j0;
        int i3 = i2 - 1;
        int i4 = this.k0;
        if (i == 48) {
            return M2(false, i3);
        }
        int i5 = 1;
        while (i2 < i4) {
            int i6 = i2 + 1;
            char c = this.P0[i2];
            if (c < '0' || c > '9') {
                if (c != '.' && c != 'e' && c != 'E') {
                    int i7 = i6 - 1;
                    this.j0 = i7;
                    if (this.r0.g()) {
                        h3(c);
                    }
                    this.t0.v(this.P0, i3, i7 - i3);
                    return p2(false, i5);
                }
                this.j0 = i6;
                return I2(c, i3, i6, false, i5);
            }
            i5++;
            i2 = i6;
        }
        this.j0 = i3;
        return M2(false, i3);
    }

    @Override // com.fasterxml.jackson.core.JsonParser
    public String O0() throws IOException {
        JsonToken L2;
        this.y0 = 0;
        JsonToken jsonToken = this.g0;
        JsonToken jsonToken2 = JsonToken.FIELD_NAME;
        if (jsonToken == jsonToken2) {
            F2();
            return null;
        }
        if (this.U0) {
            Z2();
        }
        int a3 = a3();
        if (a3 < 0) {
            close();
            this.g0 = null;
            return null;
        }
        this.x0 = null;
        if (a3 == 93) {
            d3();
            if (!this.r0.e()) {
                Z1(a3, '}');
            }
            this.r0 = this.r0.k();
            this.g0 = JsonToken.END_ARRAY;
            return null;
        } else if (a3 == 125) {
            d3();
            if (!this.r0.f()) {
                Z1(a3, ']');
            }
            this.r0 = this.r0.k();
            this.g0 = JsonToken.END_OBJECT;
            return null;
        } else {
            if (this.r0.o()) {
                a3 = W2(a3);
            }
            if (!this.r0.f()) {
                d3();
                G2(a3);
                return null;
            }
            e3();
            String J2 = a3 == 34 ? J2() : w2(a3);
            this.r0.t(J2);
            this.g0 = jsonToken2;
            int U2 = U2();
            d3();
            if (U2 == 34) {
                this.U0 = true;
                this.s0 = JsonToken.VALUE_STRING;
                return J2;
            }
            if (U2 == 45) {
                L2 = L2();
            } else if (U2 == 91) {
                L2 = JsonToken.START_ARRAY;
            } else if (U2 == 102) {
                B2();
                L2 = JsonToken.VALUE_FALSE;
            } else if (U2 == 110) {
                C2();
                L2 = JsonToken.VALUE_NULL;
            } else if (U2 == 116) {
                E2();
                L2 = JsonToken.VALUE_TRUE;
            } else if (U2 != 123) {
                switch (U2) {
                    case 48:
                    case 49:
                    case 50:
                    case 51:
                    case 52:
                    case 53:
                    case 54:
                    case 55:
                    case 56:
                    case 57:
                        L2 = N2(U2);
                        break;
                    default:
                        L2 = y2(U2);
                        break;
                }
            } else {
                L2 = JsonToken.START_OBJECT;
            }
            this.s0 = L2;
            return J2;
        }
    }

    @Override // defpackage.qp2
    public void O1() throws IOException {
        if (this.O0 != null) {
            if (this.h0.n() || J0(JsonParser.Feature.AUTO_CLOSE_SOURCE)) {
                this.O0.close();
            }
            this.O0 = null;
        }
    }

    public int O2(Base64Variant base64Variant, OutputStream outputStream, byte[] bArr) throws IOException {
        int i;
        int length = bArr.length - 3;
        int i2 = 0;
        int i3 = 0;
        while (true) {
            if (this.j0 >= this.k0) {
                A2();
            }
            char[] cArr = this.P0;
            int i4 = this.j0;
            this.j0 = i4 + 1;
            char c = cArr[i4];
            if (c > ' ') {
                int decodeBase64Char = base64Variant.decodeBase64Char(c);
                if (decodeBase64Char < 0) {
                    if (c == '\"') {
                        break;
                    }
                    decodeBase64Char = P1(base64Variant, c, 0);
                    if (decodeBase64Char < 0) {
                        continue;
                    }
                }
                if (i2 > length) {
                    i3 += i2;
                    outputStream.write(bArr, 0, i2);
                    i2 = 0;
                }
                if (this.j0 >= this.k0) {
                    A2();
                }
                char[] cArr2 = this.P0;
                int i5 = this.j0;
                this.j0 = i5 + 1;
                char c2 = cArr2[i5];
                int decodeBase64Char2 = base64Variant.decodeBase64Char(c2);
                if (decodeBase64Char2 < 0) {
                    decodeBase64Char2 = P1(base64Variant, c2, 1);
                }
                int i6 = (decodeBase64Char << 6) | decodeBase64Char2;
                if (this.j0 >= this.k0) {
                    A2();
                }
                char[] cArr3 = this.P0;
                int i7 = this.j0;
                this.j0 = i7 + 1;
                char c3 = cArr3[i7];
                int decodeBase64Char3 = base64Variant.decodeBase64Char(c3);
                if (decodeBase64Char3 < 0) {
                    if (decodeBase64Char3 != -2) {
                        if (c3 == '\"' && !base64Variant.usesPadding()) {
                            bArr[i2] = (byte) (i6 >> 4);
                            i2++;
                            break;
                        }
                        decodeBase64Char3 = P1(base64Variant, c3, 2);
                    }
                    if (decodeBase64Char3 == -2) {
                        if (this.j0 >= this.k0) {
                            A2();
                        }
                        char[] cArr4 = this.P0;
                        int i8 = this.j0;
                        this.j0 = i8 + 1;
                        char c4 = cArr4[i8];
                        if (base64Variant.usesPaddingChar(c4)) {
                            i = i2 + 1;
                            bArr[i2] = (byte) (i6 >> 4);
                            i2 = i;
                        } else {
                            throw h2(base64Variant, c4, 3, "expected padding character '" + base64Variant.getPaddingChar() + "'");
                        }
                    }
                }
                int i9 = (i6 << 6) | decodeBase64Char3;
                if (this.j0 >= this.k0) {
                    A2();
                }
                char[] cArr5 = this.P0;
                int i10 = this.j0;
                this.j0 = i10 + 1;
                char c5 = cArr5[i10];
                int decodeBase64Char4 = base64Variant.decodeBase64Char(c5);
                if (decodeBase64Char4 < 0) {
                    if (decodeBase64Char4 != -2) {
                        if (c5 == '\"' && !base64Variant.usesPadding()) {
                            int i11 = i9 >> 2;
                            int i12 = i2 + 1;
                            bArr[i2] = (byte) (i11 >> 8);
                            i2 = i12 + 1;
                            bArr[i12] = (byte) i11;
                            break;
                        }
                        decodeBase64Char4 = P1(base64Variant, c5, 3);
                    }
                    if (decodeBase64Char4 == -2) {
                        int i13 = i9 >> 2;
                        int i14 = i2 + 1;
                        bArr[i2] = (byte) (i13 >> 8);
                        i2 = i14 + 1;
                        bArr[i14] = (byte) i13;
                    }
                }
                int i15 = (i9 << 6) | decodeBase64Char4;
                int i16 = i2 + 1;
                bArr[i2] = (byte) (i15 >> 16);
                int i17 = i16 + 1;
                bArr[i16] = (byte) (i15 >> 8);
                i = i17 + 1;
                bArr[i17] = (byte) i15;
                i2 = i;
            }
        }
        this.U0 = false;
        if (i2 > 0) {
            int i18 = i3 + i2;
            outputStream.write(bArr, 0, i2);
            return i18;
        }
        return i3;
    }

    public void P2(String str) throws IOException {
        Q2(str, "'null', 'true', 'false' or NaN");
    }

    public void Q2(String str, String str2) throws IOException {
        StringBuilder sb = new StringBuilder(str);
        while (sb.length() < 256 && (this.j0 < this.k0 || z2())) {
            char c = this.P0[this.j0];
            if (!Character.isJavaIdentifierPart(c)) {
                break;
            }
            this.j0++;
            sb.append(c);
        }
        if (sb.length() == 256) {
            sb.append("...");
        }
        w1("Unrecognized token '" + sb.toString() + "': was expecting " + str2);
    }

    @Override // defpackage.qp2
    public char R1() throws IOException {
        if (this.j0 >= this.k0 && !z2()) {
            z1(" in character escape sequence", JsonToken.VALUE_STRING);
        }
        char[] cArr = this.P0;
        int i = this.j0;
        this.j0 = i + 1;
        char c = cArr[i];
        if (c == '\"' || c == '/' || c == '\\') {
            return c;
        }
        if (c != 'b') {
            if (c != 'f') {
                if (c != 'n') {
                    if (c != 'r') {
                        if (c != 't') {
                            if (c != 'u') {
                                return s1(c);
                            }
                            int i2 = 0;
                            for (int i3 = 0; i3 < 4; i3++) {
                                if (this.j0 >= this.k0 && !z2()) {
                                    z1(" in character escape sequence", JsonToken.VALUE_STRING);
                                }
                                char[] cArr2 = this.P0;
                                int i4 = this.j0;
                                this.j0 = i4 + 1;
                                char c2 = cArr2[i4];
                                int b = a.b(c2);
                                if (b < 0) {
                                    I1(c2, "expected a hex-digit for character escape sequence");
                                }
                                i2 = (i2 << 4) | b;
                            }
                            return (char) i2;
                        }
                        return '\t';
                    }
                    return '\r';
                }
                return '\n';
            }
            return '\f';
        }
        return '\b';
    }

    public final int R2() throws IOException {
        char c;
        while (true) {
            if (this.j0 >= this.k0 && !z2()) {
                throw a("Unexpected end-of-input within/between " + this.r0.i() + " entries");
            }
            char[] cArr = this.P0;
            int i = this.j0;
            int i2 = i + 1;
            this.j0 = i2;
            c = cArr[i];
            if (c > ' ') {
                if (c == '/') {
                    X2();
                } else if (c != '#' || !c3()) {
                    break;
                }
            } else if (c < ' ') {
                if (c == '\n') {
                    this.m0++;
                    this.n0 = i2;
                } else if (c == '\r') {
                    T2();
                } else if (c != '\t') {
                    K1(c);
                }
            }
        }
        return c;
    }

    @Override // com.fasterxml.jackson.core.JsonParser
    public final String S0() throws IOException {
        if (this.g0 == JsonToken.FIELD_NAME) {
            this.v0 = false;
            JsonToken jsonToken = this.s0;
            this.s0 = null;
            this.g0 = jsonToken;
            if (jsonToken == JsonToken.VALUE_STRING) {
                if (this.U0) {
                    this.U0 = false;
                    r2();
                }
                return this.t0.j();
            }
            if (jsonToken == JsonToken.START_ARRAY) {
                this.r0 = this.r0.l(this.p0, this.q0);
            } else if (jsonToken == JsonToken.START_OBJECT) {
                this.r0 = this.r0.m(this.p0, this.q0);
            }
            return null;
        } else if (T0() == JsonToken.VALUE_STRING) {
            return X();
        } else {
            return null;
        }
    }

    /* JADX WARN: Code restructure failed: missing block: B:13:0x0026, code lost:
        z1(" in a comment", null);
     */
    /* JADX WARN: Code restructure failed: missing block: B:14:0x002c, code lost:
        return;
     */
    /*
        Code decompiled incorrectly, please refer to instructions dump.
        To view partially-correct code enable 'Show inconsistent code' option in preferences
    */
    public final void S2() throws java.io.IOException {
        /*
            r3 = this;
        L0:
            int r0 = r3.j0
            int r1 = r3.k0
            if (r0 < r1) goto Lc
            boolean r0 = r3.z2()
            if (r0 == 0) goto L26
        Lc:
            char[] r0 = r3.P0
            int r1 = r3.j0
            int r2 = r1 + 1
            r3.j0 = r2
            char r0 = r0[r1]
            r1 = 42
            if (r0 > r1) goto L0
            if (r0 != r1) goto L3c
            int r0 = r3.k0
            if (r2 < r0) goto L2d
            boolean r0 = r3.z2()
            if (r0 != 0) goto L2d
        L26:
            r0 = 0
            java.lang.String r1 = " in a comment"
            r3.z1(r1, r0)
            return
        L2d:
            char[] r0 = r3.P0
            int r1 = r3.j0
            char r0 = r0[r1]
            r2 = 47
            if (r0 != r2) goto L0
            int r1 = r1 + 1
            r3.j0 = r1
            return
        L3c:
            r1 = 32
            if (r0 >= r1) goto L0
            r1 = 10
            if (r0 != r1) goto L4d
            int r0 = r3.m0
            int r0 = r0 + 1
            r3.m0 = r0
            r3.n0 = r2
            goto L0
        L4d:
            r1 = 13
            if (r0 != r1) goto L55
            r3.T2()
            goto L0
        L55:
            r1 = 9
            if (r0 == r1) goto L0
            r3.K1(r0)
            goto L0
        */
        throw new UnsupportedOperationException("Method not decompiled: defpackage.b43.S2():void");
    }

    @Override // com.fasterxml.jackson.core.JsonParser
    public final JsonToken T0() throws IOException {
        JsonToken jsonToken;
        JsonToken jsonToken2 = this.g0;
        JsonToken jsonToken3 = JsonToken.FIELD_NAME;
        if (jsonToken2 == jsonToken3) {
            return F2();
        }
        this.y0 = 0;
        if (this.U0) {
            Z2();
        }
        int a3 = a3();
        if (a3 < 0) {
            close();
            this.g0 = null;
            return null;
        }
        this.x0 = null;
        if (a3 == 93) {
            d3();
            if (!this.r0.e()) {
                Z1(a3, '}');
            }
            this.r0 = this.r0.k();
            JsonToken jsonToken4 = JsonToken.END_ARRAY;
            this.g0 = jsonToken4;
            return jsonToken4;
        } else if (a3 == 125) {
            d3();
            if (!this.r0.f()) {
                Z1(a3, ']');
            }
            this.r0 = this.r0.k();
            JsonToken jsonToken5 = JsonToken.END_OBJECT;
            this.g0 = jsonToken5;
            return jsonToken5;
        } else {
            if (this.r0.o()) {
                a3 = W2(a3);
            }
            boolean f = this.r0.f();
            if (f) {
                e3();
                this.r0.t(a3 == 34 ? J2() : w2(a3));
                this.g0 = jsonToken3;
                a3 = U2();
            }
            d3();
            if (a3 == 34) {
                this.U0 = true;
                jsonToken = JsonToken.VALUE_STRING;
            } else if (a3 == 45) {
                jsonToken = L2();
            } else if (a3 == 91) {
                if (!f) {
                    this.r0 = this.r0.l(this.p0, this.q0);
                }
                jsonToken = JsonToken.START_ARRAY;
            } else if (a3 == 102) {
                B2();
                jsonToken = JsonToken.VALUE_FALSE;
            } else if (a3 != 110) {
                if (a3 != 116) {
                    if (a3 == 123) {
                        if (!f) {
                            this.r0 = this.r0.m(this.p0, this.q0);
                        }
                        jsonToken = JsonToken.START_OBJECT;
                    } else if (a3 != 125) {
                        switch (a3) {
                            case 48:
                            case 49:
                            case 50:
                            case 51:
                            case 52:
                            case 53:
                            case 54:
                            case 55:
                            case 56:
                            case 57:
                                jsonToken = N2(a3);
                                break;
                            default:
                                jsonToken = y2(a3);
                                break;
                        }
                    } else {
                        I1(a3, "expected a value");
                    }
                }
                E2();
                jsonToken = JsonToken.VALUE_TRUE;
            } else {
                C2();
                jsonToken = JsonToken.VALUE_NULL;
            }
            if (f) {
                this.s0 = jsonToken;
                return this.g0;
            }
            this.g0 = jsonToken;
            return jsonToken;
        }
    }

    public final void T2() throws IOException {
        if (this.j0 < this.k0 || z2()) {
            char[] cArr = this.P0;
            int i = this.j0;
            if (cArr[i] == '\n') {
                this.j0 = i + 1;
            }
        }
        this.m0++;
        this.n0 = this.j0;
    }

    public final int U2() throws IOException {
        int i = this.j0;
        if (i + 4 >= this.k0) {
            return V2(false);
        }
        char[] cArr = this.P0;
        char c = cArr[i];
        if (c == ':') {
            int i2 = i + 1;
            this.j0 = i2;
            char c2 = cArr[i2];
            if (c2 > ' ') {
                if (c2 != '/' && c2 != '#') {
                    this.j0 = i2 + 1;
                    return c2;
                }
                return V2(true);
            }
            if (c2 == ' ' || c2 == '\t') {
                int i3 = i2 + 1;
                this.j0 = i3;
                char c3 = cArr[i3];
                if (c3 > ' ') {
                    if (c3 != '/' && c3 != '#') {
                        this.j0 = i3 + 1;
                        return c3;
                    }
                    return V2(true);
                }
            }
            return V2(true);
        }
        if (c == ' ' || c == '\t') {
            int i4 = i + 1;
            this.j0 = i4;
            c = cArr[i4];
        }
        if (c == ':') {
            int i5 = this.j0 + 1;
            this.j0 = i5;
            char c4 = cArr[i5];
            if (c4 > ' ') {
                if (c4 != '/' && c4 != '#') {
                    this.j0 = i5 + 1;
                    return c4;
                }
                return V2(true);
            }
            if (c4 == ' ' || c4 == '\t') {
                int i6 = i5 + 1;
                this.j0 = i6;
                char c5 = cArr[i6];
                if (c5 > ' ') {
                    if (c5 != '/' && c5 != '#') {
                        this.j0 = i6 + 1;
                        return c5;
                    }
                    return V2(true);
                }
            }
            return V2(true);
        }
        return V2(false);
    }

    public final int V2(boolean z) throws IOException {
        while (true) {
            if (this.j0 >= this.k0 && !z2()) {
                z1(" within/between " + this.r0.i() + " entries", null);
                return -1;
            }
            char[] cArr = this.P0;
            int i = this.j0;
            int i2 = i + 1;
            this.j0 = i2;
            char c = cArr[i];
            if (c > ' ') {
                if (c == '/') {
                    X2();
                } else if (c != '#' || !c3()) {
                    if (z) {
                        return c;
                    }
                    if (c != ':') {
                        I1(c, "was expecting a colon to separate field name and value");
                    }
                    z = true;
                }
            } else if (c < ' ') {
                if (c == '\n') {
                    this.m0++;
                    this.n0 = i2;
                } else if (c == '\r') {
                    T2();
                } else if (c != '\t') {
                    K1(c);
                }
            }
        }
    }

    public final int W2(int i) throws IOException {
        if (i != 44) {
            I1(i, "was expecting comma to separate " + this.r0.i() + " entries");
        }
        while (true) {
            int i2 = this.j0;
            if (i2 < this.k0) {
                char[] cArr = this.P0;
                int i3 = i2 + 1;
                this.j0 = i3;
                char c = cArr[i2];
                if (c > ' ') {
                    if (c == '/' || c == '#') {
                        this.j0 = i3 - 1;
                        return R2();
                    }
                    return c;
                } else if (c < ' ') {
                    if (c == '\n') {
                        this.m0++;
                        this.n0 = i3;
                    } else if (c == '\r') {
                        T2();
                    } else if (c != '\t') {
                        K1(c);
                    }
                }
            } else {
                return R2();
            }
        }
    }

    @Override // com.fasterxml.jackson.core.JsonParser
    public final String X() throws IOException {
        JsonToken jsonToken = this.g0;
        if (jsonToken == JsonToken.VALUE_STRING) {
            if (this.U0) {
                this.U0 = false;
                r2();
            }
            return this.t0.j();
        }
        return t2(jsonToken);
    }

    public final void X2() throws IOException {
        if (!J0(JsonParser.Feature.ALLOW_COMMENTS)) {
            I1(47, "maybe a (non-standard) comment? (not recognized as one since Feature 'ALLOW_COMMENTS' not enabled for parser)");
        }
        if (this.j0 >= this.k0 && !z2()) {
            z1(" in a comment", null);
        }
        char[] cArr = this.P0;
        int i = this.j0;
        this.j0 = i + 1;
        char c = cArr[i];
        if (c == '/') {
            Y2();
        } else if (c == '*') {
            S2();
        } else {
            I1(c, "was expecting either '*' or '/' for a comment");
        }
    }

    @Override // defpackage.qp2
    public void Y1() throws IOException {
        char[] cArr;
        super.Y1();
        this.S0.u();
        if (!this.Q0 || (cArr = this.P0) == null) {
            return;
        }
        this.P0 = null;
        this.h0.s(cArr);
    }

    public final void Y2() throws IOException {
        while (true) {
            if (this.j0 >= this.k0 && !z2()) {
                return;
            }
            char[] cArr = this.P0;
            int i = this.j0;
            int i2 = i + 1;
            this.j0 = i2;
            char c = cArr[i];
            if (c < ' ') {
                if (c == '\n') {
                    this.m0++;
                    this.n0 = i2;
                    return;
                } else if (c == '\r') {
                    T2();
                    return;
                } else if (c != '\t') {
                    K1(c);
                }
            }
        }
    }

    @Override // com.fasterxml.jackson.core.JsonParser
    public int Z0(Base64Variant base64Variant, OutputStream outputStream) throws IOException {
        if (this.U0 && this.g0 == JsonToken.VALUE_STRING) {
            byte[] d = this.h0.d();
            try {
                return O2(base64Variant, outputStream, d);
            } finally {
                this.h0.o(d);
            }
        }
        byte[] j = j(base64Variant);
        outputStream.write(j);
        return j.length;
    }

    public final void Z2() throws IOException {
        this.U0 = false;
        int i = this.j0;
        int i2 = this.k0;
        char[] cArr = this.P0;
        while (true) {
            if (i >= i2) {
                this.j0 = i;
                if (!z2()) {
                    z1(": was expecting closing quote for a string value", JsonToken.VALUE_STRING);
                }
                i = this.j0;
                i2 = this.k0;
            }
            int i3 = i + 1;
            char c = cArr[i];
            if (c <= '\\') {
                if (c == '\\') {
                    this.j0 = i3;
                    R1();
                    i = this.j0;
                    i2 = this.k0;
                } else if (c <= '\"') {
                    if (c == '\"') {
                        this.j0 = i3;
                        return;
                    } else if (c < ' ') {
                        this.j0 = i3;
                        L1(c, "string value");
                    }
                }
            }
            i = i3;
        }
    }

    @Override // com.fasterxml.jackson.core.JsonParser
    public final char[] a0() throws IOException {
        JsonToken jsonToken = this.g0;
        if (jsonToken != null) {
            int id = jsonToken.id();
            if (id != 5) {
                if (id != 6) {
                    if (id != 7 && id != 8) {
                        return this.g0.asCharArray();
                    }
                } else if (this.U0) {
                    this.U0 = false;
                    r2();
                }
                return this.t0.q();
            }
            if (!this.v0) {
                String b = this.r0.b();
                int length = b.length();
                char[] cArr = this.u0;
                if (cArr == null) {
                    this.u0 = this.h0.f(length);
                } else if (cArr.length < length) {
                    this.u0 = new char[length];
                }
                b.getChars(0, length, this.u0, 0);
                this.v0 = true;
            }
            return this.u0;
        }
        return null;
    }

    public final int a3() throws IOException {
        if (this.j0 >= this.k0 && !z2()) {
            return S1();
        }
        char[] cArr = this.P0;
        int i = this.j0;
        int i2 = i + 1;
        this.j0 = i2;
        char c = cArr[i];
        if (c > ' ') {
            if (c == '/' || c == '#') {
                this.j0 = i2 - 1;
                return b3();
            }
            return c;
        }
        if (c != ' ') {
            if (c == '\n') {
                this.m0++;
                this.n0 = i2;
            } else if (c == '\r') {
                T2();
            } else if (c != '\t') {
                K1(c);
            }
        }
        while (true) {
            int i3 = this.j0;
            if (i3 < this.k0) {
                char[] cArr2 = this.P0;
                int i4 = i3 + 1;
                this.j0 = i4;
                char c2 = cArr2[i3];
                if (c2 > ' ') {
                    if (c2 == '/' || c2 == '#') {
                        this.j0 = i4 - 1;
                        return b3();
                    }
                    return c2;
                } else if (c2 != ' ') {
                    if (c2 == '\n') {
                        this.m0++;
                        this.n0 = i4;
                    } else if (c2 == '\r') {
                        T2();
                    } else if (c2 != '\t') {
                        K1(c2);
                    }
                }
            } else {
                return b3();
            }
        }
    }

    @Override // com.fasterxml.jackson.core.JsonParser
    public final int b0() throws IOException {
        JsonToken jsonToken = this.g0;
        if (jsonToken != null) {
            int id = jsonToken.id();
            if (id != 5) {
                if (id != 6) {
                    if (id != 7 && id != 8) {
                        return this.g0.asCharArray().length;
                    }
                } else if (this.U0) {
                    this.U0 = false;
                    r2();
                }
                return this.t0.A();
            }
            return this.r0.b().length();
        }
        return 0;
    }

    public final int b3() throws IOException {
        char c;
        while (true) {
            if (this.j0 >= this.k0 && !z2()) {
                return S1();
            }
            char[] cArr = this.P0;
            int i = this.j0;
            int i2 = i + 1;
            this.j0 = i2;
            c = cArr[i];
            if (c > ' ') {
                if (c == '/') {
                    X2();
                } else if (c != '#' || !c3()) {
                    break;
                }
            } else if (c != ' ') {
                if (c == '\n') {
                    this.m0++;
                    this.n0 = i2;
                } else if (c == '\r') {
                    T2();
                } else if (c != '\t') {
                    K1(c);
                }
            }
        }
        return c;
    }

    public final boolean c3() throws IOException {
        if (J0(JsonParser.Feature.ALLOW_YAML_COMMENTS)) {
            Y2();
            return true;
        }
        return false;
    }

    public final void d3() {
        int i = this.j0;
        this.o0 = this.l0 + i;
        this.p0 = this.m0;
        this.q0 = i - this.n0;
    }

    /* JADX WARN: Code restructure failed: missing block: B:9:0x0011, code lost:
        if (r0 != 8) goto L15;
     */
    @Override // com.fasterxml.jackson.core.JsonParser
    /*
        Code decompiled incorrectly, please refer to instructions dump.
        To view partially-correct code enable 'Show inconsistent code' option in preferences
    */
    public final int e0() throws java.io.IOException {
        /*
            r3 = this;
            com.fasterxml.jackson.core.JsonToken r0 = r3.g0
            r1 = 0
            if (r0 == 0) goto L24
            int r0 = r0.id()
            r2 = 6
            if (r0 == r2) goto L14
            r2 = 7
            if (r0 == r2) goto L1d
            r2 = 8
            if (r0 == r2) goto L1d
            goto L24
        L14:
            boolean r0 = r3.U0
            if (r0 == 0) goto L1d
            r3.U0 = r1
            r3.r2()
        L1d:
            com.fasterxml.jackson.core.util.c r0 = r3.t0
            int r0 = r0.r()
            return r0
        L24:
            return r1
        */
        throw new UnsupportedOperationException("Method not decompiled: defpackage.b43.e0():int");
    }

    public final void e3() {
        int i = this.j0;
        this.V0 = i;
        this.W0 = this.m0;
        this.X0 = i - this.n0;
    }

    @Override // com.fasterxml.jackson.core.JsonParser
    public JsonLocation f0() {
        Object m = this.h0.m();
        if (this.g0 == JsonToken.FIELD_NAME) {
            return new JsonLocation(m, -1L, (this.V0 - 1) + this.l0, this.W0, this.X0);
        }
        return new JsonLocation(m, -1L, this.o0 - 1, this.p0, this.q0);
    }

    public final char f3() throws IOException {
        char c;
        if ((this.j0 < this.k0 || z2()) && (c = this.P0[this.j0]) >= '0' && c <= '9') {
            if (!J0(JsonParser.Feature.ALLOW_NUMERIC_LEADING_ZEROS)) {
                i2("Leading zeroes not allowed");
            }
            this.j0++;
            if (c == '0') {
                do {
                    if (this.j0 >= this.k0 && !z2()) {
                        break;
                    }
                    char[] cArr = this.P0;
                    int i = this.j0;
                    c = cArr[i];
                    if (c < '0' || c > '9') {
                        return '0';
                    }
                    this.j0 = i + 1;
                } while (c == '0');
            }
            return c;
        }
        return '0';
    }

    public final char g3() throws IOException {
        char c;
        int i = this.j0;
        if (i >= this.k0 || ((c = this.P0[i]) >= '0' && c <= '9')) {
            return f3();
        }
        return '0';
    }

    public final void h3(int i) throws IOException {
        int i2 = this.j0 + 1;
        this.j0 = i2;
        if (i != 9) {
            if (i == 10) {
                this.m0++;
                this.n0 = i2;
            } else if (i == 13) {
                T2();
            } else if (i != 32) {
                H1(i);
            }
        }
    }

    @Deprecated
    public char i3(String str) throws IOException {
        return j3(str, null);
    }

    @Override // com.fasterxml.jackson.core.JsonParser
    public byte[] j(Base64Variant base64Variant) throws IOException {
        JsonToken jsonToken = this.g0;
        if (jsonToken != JsonToken.VALUE_STRING && (jsonToken != JsonToken.VALUE_EMBEDDED_OBJECT || this.x0 == null)) {
            w1("Current token (" + this.g0 + ") not VALUE_STRING or VALUE_EMBEDDED_OBJECT, can not access as binary");
        }
        if (this.U0) {
            try {
                this.x0 = q2(base64Variant);
                this.U0 = false;
            } catch (IllegalArgumentException e) {
                throw a("Failed to decode VALUE_STRING as base64 (" + base64Variant + "): " + e.getMessage());
            }
        } else if (this.x0 == null) {
            ms T1 = T1();
            o1(X(), T1, base64Variant);
            this.x0 = T1.n();
        }
        return this.x0;
    }

    public char j3(String str, JsonToken jsonToken) throws IOException {
        if (this.j0 >= this.k0 && !z2()) {
            z1(str, jsonToken);
        }
        char[] cArr = this.P0;
        int i = this.j0;
        this.j0 = i + 1;
        return cArr[i];
    }

    @Override // com.fasterxml.jackson.core.JsonParser
    public c n() {
        return this.R0;
    }

    @Override // com.fasterxml.jackson.core.JsonParser
    public JsonLocation q() {
        return new JsonLocation(this.h0.m(), -1L, this.j0 + this.l0, this.m0, (this.j0 - this.n0) + 1);
    }

    public byte[] q2(Base64Variant base64Variant) throws IOException {
        ms T1 = T1();
        while (true) {
            if (this.j0 >= this.k0) {
                A2();
            }
            char[] cArr = this.P0;
            int i = this.j0;
            this.j0 = i + 1;
            char c = cArr[i];
            if (c > ' ') {
                int decodeBase64Char = base64Variant.decodeBase64Char(c);
                if (decodeBase64Char < 0) {
                    if (c == '\"') {
                        return T1.n();
                    }
                    decodeBase64Char = P1(base64Variant, c, 0);
                    if (decodeBase64Char < 0) {
                        continue;
                    }
                }
                if (this.j0 >= this.k0) {
                    A2();
                }
                char[] cArr2 = this.P0;
                int i2 = this.j0;
                this.j0 = i2 + 1;
                char c2 = cArr2[i2];
                int decodeBase64Char2 = base64Variant.decodeBase64Char(c2);
                if (decodeBase64Char2 < 0) {
                    decodeBase64Char2 = P1(base64Variant, c2, 1);
                }
                int i3 = (decodeBase64Char << 6) | decodeBase64Char2;
                if (this.j0 >= this.k0) {
                    A2();
                }
                char[] cArr3 = this.P0;
                int i4 = this.j0;
                this.j0 = i4 + 1;
                char c3 = cArr3[i4];
                int decodeBase64Char3 = base64Variant.decodeBase64Char(c3);
                if (decodeBase64Char3 < 0) {
                    if (decodeBase64Char3 != -2) {
                        if (c3 == '\"' && !base64Variant.usesPadding()) {
                            T1.b(i3 >> 4);
                            return T1.n();
                        }
                        decodeBase64Char3 = P1(base64Variant, c3, 2);
                    }
                    if (decodeBase64Char3 == -2) {
                        if (this.j0 >= this.k0) {
                            A2();
                        }
                        char[] cArr4 = this.P0;
                        int i5 = this.j0;
                        this.j0 = i5 + 1;
                        char c4 = cArr4[i5];
                        if (base64Variant.usesPaddingChar(c4)) {
                            T1.b(i3 >> 4);
                        } else {
                            throw h2(base64Variant, c4, 3, "expected padding character '" + base64Variant.getPaddingChar() + "'");
                        }
                    }
                }
                int i6 = (i3 << 6) | decodeBase64Char3;
                if (this.j0 >= this.k0) {
                    A2();
                }
                char[] cArr5 = this.P0;
                int i7 = this.j0;
                this.j0 = i7 + 1;
                char c5 = cArr5[i7];
                int decodeBase64Char4 = base64Variant.decodeBase64Char(c5);
                if (decodeBase64Char4 < 0) {
                    if (decodeBase64Char4 != -2) {
                        if (c5 == '\"' && !base64Variant.usesPadding()) {
                            T1.d(i6 >> 2);
                            return T1.n();
                        }
                        decodeBase64Char4 = P1(base64Variant, c5, 3);
                    }
                    if (decodeBase64Char4 == -2) {
                        T1.d(i6 >> 2);
                    }
                }
                T1.c((i6 << 6) | decodeBase64Char4);
            }
        }
    }

    public final void r2() throws IOException {
        int i = this.j0;
        int i2 = this.k0;
        if (i < i2) {
            int[] iArr = Y0;
            int length = iArr.length;
            while (true) {
                char[] cArr = this.P0;
                char c = cArr[i];
                if (c >= length || iArr[c] == 0) {
                    i++;
                    if (i >= i2) {
                        break;
                    }
                } else if (c == '\"') {
                    com.fasterxml.jackson.core.util.c cVar = this.t0;
                    int i3 = this.j0;
                    cVar.v(cArr, i3, i - i3);
                    this.j0 = i + 1;
                    return;
                }
            }
        }
        com.fasterxml.jackson.core.util.c cVar2 = this.t0;
        char[] cArr2 = this.P0;
        int i4 = this.j0;
        cVar2.t(cArr2, i4, i - i4);
        this.j0 = i;
        s2();
    }

    public void s2() throws IOException {
        char[] o = this.t0.o();
        int p = this.t0.p();
        int[] iArr = Y0;
        int length = iArr.length;
        while (true) {
            if (this.j0 >= this.k0 && !z2()) {
                z1(": was expecting closing quote for a string value", JsonToken.VALUE_STRING);
            }
            char[] cArr = this.P0;
            int i = this.j0;
            this.j0 = i + 1;
            char c = cArr[i];
            if (c < length && iArr[c] != 0) {
                if (c == '\"') {
                    this.t0.z(p);
                    return;
                } else if (c == '\\') {
                    c = R1();
                } else if (c < ' ') {
                    L1(c, "string value");
                }
            }
            if (p >= o.length) {
                o = this.t0.n();
                p = 0;
            }
            o[p] = c;
            p++;
        }
    }

    public final String t2(JsonToken jsonToken) {
        if (jsonToken == null) {
            return null;
        }
        int id = jsonToken.id();
        if (id != 5) {
            if (id != 6 && id != 7 && id != 8) {
                return jsonToken.asString();
            }
            return this.t0.j();
        }
        return this.r0.b();
    }

    public JsonToken u2() throws IOException {
        char[] k = this.t0.k();
        int p = this.t0.p();
        while (true) {
            if (this.j0 >= this.k0 && !z2()) {
                z1(": was expecting closing quote for a string value", JsonToken.VALUE_STRING);
            }
            char[] cArr = this.P0;
            int i = this.j0;
            this.j0 = i + 1;
            char c = cArr[i];
            if (c <= '\\') {
                if (c == '\\') {
                    c = R1();
                } else if (c <= '\'') {
                    if (c == '\'') {
                        this.t0.z(p);
                        return JsonToken.VALUE_STRING;
                    } else if (c < ' ') {
                        L1(c, "string value");
                    }
                }
            }
            if (p >= k.length) {
                k = this.t0.n();
                p = 0;
            }
            k[p] = c;
            p++;
        }
    }

    /*  JADX ERROR: JadxRuntimeException in pass: InitCodeVariables
        jadx.core.utils.exceptions.JadxRuntimeException: Several immutable types in one variable: [int, char], vars: [r9v0 ??, r9v1 ??, r9v5 ??]
        	at jadx.core.dex.visitors.InitCodeVariables.setCodeVarType(InitCodeVariables.java:107)
        	at jadx.core.dex.visitors.InitCodeVariables.setCodeVar(InitCodeVariables.java:83)
        	at jadx.core.dex.visitors.InitCodeVariables.initCodeVar(InitCodeVariables.java:74)
        	at jadx.core.dex.visitors.InitCodeVariables.initCodeVar(InitCodeVariables.java:57)
        	at jadx.core.dex.visitors.InitCodeVariables.initCodeVars(InitCodeVariables.java:45)
        	at jadx.core.dex.visitors.InitCodeVariables.visit(InitCodeVariables.java:29)
        */
    public com.fasterxml.jackson.core.JsonToken v2(
    /*  JADX ERROR: JadxRuntimeException in pass: InitCodeVariables
        jadx.core.utils.exceptions.JadxRuntimeException: Several immutable types in one variable: [int, char], vars: [r9v0 ??, r9v1 ??, r9v5 ??]
        	at jadx.core.dex.visitors.InitCodeVariables.setCodeVarType(InitCodeVariables.java:107)
        	at jadx.core.dex.visitors.InitCodeVariables.setCodeVar(InitCodeVariables.java:83)
        	at jadx.core.dex.visitors.InitCodeVariables.initCodeVar(InitCodeVariables.java:74)
        	at jadx.core.dex.visitors.InitCodeVariables.initCodeVar(InitCodeVariables.java:57)
        	at jadx.core.dex.visitors.InitCodeVariables.initCodeVars(InitCodeVariables.java:45)
        */
    /*  JADX ERROR: Method generation error
        jadx.core.utils.exceptions.JadxRuntimeException: Code variable not set in r9v0 ??
        	at jadx.core.dex.instructions.args.SSAVar.getCodeVar(SSAVar.java:227)
        	at jadx.core.codegen.MethodGen.addMethodArguments(MethodGen.java:222)
        	at jadx.core.codegen.MethodGen.addDefinition(MethodGen.java:167)
        	at jadx.core.codegen.ClassGen.addMethodCode(ClassGen.java:372)
        	at jadx.core.codegen.ClassGen.addMethod(ClassGen.java:306)
        	at jadx.core.codegen.ClassGen.lambda$addInnerClsAndMethods$2(ClassGen.java:272)
        	at java.base/java.util.stream.ForEachOps$ForEachOp$OfRef.accept(ForEachOps.java:183)
        	at java.base/java.util.ArrayList.forEach(ArrayList.java:1511)
        	at java.base/java.util.stream.SortedOps$RefSortingSink.end(SortedOps.java:395)
        	at java.base/java.util.stream.Sink$ChainedReference.end(Sink.java:258)
        */

    public String w2(int i) throws IOException {
        boolean isJavaIdentifierPart;
        if (i == 39 && J0(JsonParser.Feature.ALLOW_SINGLE_QUOTES)) {
            return H2();
        }
        if (!J0(JsonParser.Feature.ALLOW_UNQUOTED_FIELD_NAMES)) {
            I1(i, "was expecting double-quote to start field name");
        }
        int[] h = a.h();
        int length = h.length;
        if (i < length) {
            isJavaIdentifierPart = h[i] == 0;
        } else {
            isJavaIdentifierPart = Character.isJavaIdentifierPart((char) i);
        }
        if (!isJavaIdentifierPart) {
            I1(i, "was expecting either valid name character (for unquoted name) or double-quote (for quoted) to start field name");
        }
        int i2 = this.j0;
        int i3 = this.T0;
        int i4 = this.k0;
        if (i2 < i4) {
            do {
                char[] cArr = this.P0;
                char c = cArr[i2];
                if (c < length) {
                    if (h[c] != 0) {
                        int i5 = this.j0 - 1;
                        this.j0 = i2;
                        return this.S0.o(cArr, i5, i2 - i5, i3);
                    }
                } else if (!Character.isJavaIdentifierPart(c)) {
                    int i6 = this.j0 - 1;
                    this.j0 = i2;
                    return this.S0.o(this.P0, i6, i2 - i6, i3);
                }
                i3 = (i3 * 33) + c;
                i2++;
            } while (i2 < i4);
            this.j0 = i2;
            return x2(this.j0 - 1, i3, h);
        }
        this.j0 = i2;
        return x2(this.j0 - 1, i3, h);
    }

    @Override // defpackage.rp2, com.fasterxml.jackson.core.JsonParser
    public final String x0() throws IOException {
        JsonToken jsonToken = this.g0;
        if (jsonToken == JsonToken.VALUE_STRING) {
            if (this.U0) {
                this.U0 = false;
                r2();
            }
            return this.t0.j();
        } else if (jsonToken == JsonToken.FIELD_NAME) {
            return r();
        } else {
            return super.y0(null);
        }
    }

    /* JADX WARN: Removed duplicated region for block: B:24:0x0069 A[SYNTHETIC] */
    /* JADX WARN: Removed duplicated region for block: B:25:0x0061 A[SYNTHETIC] */
    /*
        Code decompiled incorrectly, please refer to instructions dump.
        To view partially-correct code enable 'Show inconsistent code' option in preferences
    */
    public final java.lang.String x2(int r5, int r6, int[] r7) throws java.io.IOException {
        /*
            r4 = this;
            com.fasterxml.jackson.core.util.c r0 = r4.t0
            char[] r1 = r4.P0
            int r2 = r4.j0
            int r2 = r2 - r5
            r0.v(r1, r5, r2)
            com.fasterxml.jackson.core.util.c r5 = r4.t0
            char[] r5 = r5.o()
            com.fasterxml.jackson.core.util.c r0 = r4.t0
            int r0 = r0.p()
            int r1 = r7.length
        L17:
            int r2 = r4.j0
            int r3 = r4.k0
            if (r2 < r3) goto L24
            boolean r2 = r4.z2()
            if (r2 != 0) goto L24
            goto L37
        L24:
            char[] r2 = r4.P0
            int r3 = r4.j0
            char r2 = r2[r3]
            if (r2 > r1) goto L31
            r3 = r7[r2]
            if (r3 == 0) goto L51
            goto L37
        L31:
            boolean r3 = java.lang.Character.isJavaIdentifierPart(r2)
            if (r3 != 0) goto L51
        L37:
            com.fasterxml.jackson.core.util.c r5 = r4.t0
            r5.z(r0)
            com.fasterxml.jackson.core.util.c r5 = r4.t0
            char[] r7 = r5.q()
            int r0 = r5.r()
            int r5 = r5.A()
            zx r1 = r4.S0
            java.lang.String r5 = r1.o(r7, r0, r5, r6)
            return r5
        L51:
            int r3 = r4.j0
            int r3 = r3 + 1
            r4.j0 = r3
            int r6 = r6 * 33
            int r6 = r6 + r2
            int r3 = r0 + 1
            r5[r0] = r2
            int r0 = r5.length
            if (r3 < r0) goto L69
            com.fasterxml.jackson.core.util.c r5 = r4.t0
            char[] r5 = r5.n()
            r0 = 0
            goto L17
        L69:
            r0 = r3
            goto L17
        */
        throw new UnsupportedOperationException("Method not decompiled: defpackage.b43.x2(int, int, int[]):java.lang.String");
    }

    @Override // defpackage.rp2, com.fasterxml.jackson.core.JsonParser
    public final String y0(String str) throws IOException {
        JsonToken jsonToken = this.g0;
        if (jsonToken == JsonToken.VALUE_STRING) {
            if (this.U0) {
                this.U0 = false;
                r2();
            }
            return this.t0.j();
        } else if (jsonToken == JsonToken.FIELD_NAME) {
            return r();
        } else {
            return super.y0(str);
        }
    }

    /* JADX WARN: Code restructure failed: missing block: B:13:0x0017, code lost:
        if (r4 != 44) goto L18;
     */
    /* JADX WARN: Code restructure failed: missing block: B:23:0x0042, code lost:
        if (r3.r0.e() == false) goto L18;
     */
    /* JADX WARN: Code restructure failed: missing block: B:26:0x004b, code lost:
        if (J0(com.fasterxml.jackson.core.JsonParser.Feature.ALLOW_MISSING_VALUES) == false) goto L18;
     */
    /* JADX WARN: Code restructure failed: missing block: B:27:0x004d, code lost:
        r3.j0--;
     */
    /* JADX WARN: Code restructure failed: missing block: B:28:0x0054, code lost:
        return com.fasterxml.jackson.core.JsonToken.VALUE_NULL;
     */
    /*
        Code decompiled incorrectly, please refer to instructions dump.
        To view partially-correct code enable 'Show inconsistent code' option in preferences
    */
    public com.fasterxml.jackson.core.JsonToken y2(int r4) throws java.io.IOException {
        /*
            r3 = this;
            r0 = 39
            if (r4 == r0) goto L89
            r0 = 73
            r1 = 1
            if (r4 == r0) goto L6f
            r0 = 78
            if (r4 == r0) goto L55
            r0 = 93
            if (r4 == r0) goto L3c
            r0 = 43
            if (r4 == r0) goto L1b
            r0 = 44
            if (r4 == r0) goto L45
            goto L96
        L1b:
            int r4 = r3.j0
            int r0 = r3.k0
            if (r4 < r0) goto L2c
            boolean r4 = r3.z2()
            if (r4 != 0) goto L2c
            com.fasterxml.jackson.core.JsonToken r4 = com.fasterxml.jackson.core.JsonToken.VALUE_NUMBER_INT
            r3.F1(r4)
        L2c:
            char[] r4 = r3.P0
            int r0 = r3.j0
            int r1 = r0 + 1
            r3.j0 = r1
            char r4 = r4[r0]
            r0 = 0
            com.fasterxml.jackson.core.JsonToken r4 = r3.v2(r4, r0)
            return r4
        L3c:
            hv1 r0 = r3.r0
            boolean r0 = r0.e()
            if (r0 != 0) goto L45
            goto L96
        L45:
            com.fasterxml.jackson.core.JsonParser$Feature r0 = com.fasterxml.jackson.core.JsonParser.Feature.ALLOW_MISSING_VALUES
            boolean r0 = r3.J0(r0)
            if (r0 == 0) goto L96
            int r4 = r3.j0
            int r4 = r4 - r1
            r3.j0 = r4
            com.fasterxml.jackson.core.JsonToken r4 = com.fasterxml.jackson.core.JsonToken.VALUE_NULL
            return r4
        L55:
            java.lang.String r0 = "NaN"
            r3.D2(r0, r1)
            com.fasterxml.jackson.core.JsonParser$Feature r1 = com.fasterxml.jackson.core.JsonParser.Feature.ALLOW_NON_NUMERIC_NUMBERS
            boolean r1 = r3.J0(r1)
            if (r1 == 0) goto L69
            r1 = 9221120237041090560(0x7ff8000000000000, double:NaN)
            com.fasterxml.jackson.core.JsonToken r4 = r3.n2(r0, r1)
            return r4
        L69:
            java.lang.String r0 = "Non-standard token 'NaN': enable JsonParser.Feature.ALLOW_NON_NUMERIC_NUMBERS to allow"
            r3.w1(r0)
            goto L96
        L6f:
            java.lang.String r0 = "Infinity"
            r3.D2(r0, r1)
            com.fasterxml.jackson.core.JsonParser$Feature r1 = com.fasterxml.jackson.core.JsonParser.Feature.ALLOW_NON_NUMERIC_NUMBERS
            boolean r1 = r3.J0(r1)
            if (r1 == 0) goto L83
            r1 = 9218868437227405312(0x7ff0000000000000, double:Infinity)
            com.fasterxml.jackson.core.JsonToken r4 = r3.n2(r0, r1)
            return r4
        L83:
            java.lang.String r0 = "Non-standard token 'Infinity': enable JsonParser.Feature.ALLOW_NON_NUMERIC_NUMBERS to allow"
            r3.w1(r0)
            goto L96
        L89:
            com.fasterxml.jackson.core.JsonParser$Feature r0 = com.fasterxml.jackson.core.JsonParser.Feature.ALLOW_SINGLE_QUOTES
            boolean r0 = r3.J0(r0)
            if (r0 == 0) goto L96
            com.fasterxml.jackson.core.JsonToken r4 = r3.u2()
            return r4
        L96:
            boolean r0 = java.lang.Character.isJavaIdentifierStart(r4)
            if (r0 == 0) goto Lb3
            java.lang.StringBuilder r0 = new java.lang.StringBuilder
            r0.<init>()
            java.lang.String r1 = ""
            r0.append(r1)
            char r1 = (char) r4
            r0.append(r1)
            java.lang.String r0 = r0.toString()
            java.lang.String r1 = "('true', 'false' or 'null')"
            r3.Q2(r0, r1)
        Lb3:
            java.lang.String r0 = "expected a valid value (number, String, array, object, 'true', 'false' or 'null')"
            r3.I1(r4, r0)
            r4 = 0
            return r4
        */
        throw new UnsupportedOperationException("Method not decompiled: defpackage.b43.y2(int):com.fasterxml.jackson.core.JsonToken");
    }

    public boolean z2() throws IOException {
        int i = this.k0;
        long j = i;
        this.l0 += j;
        this.n0 -= i;
        this.V0 -= j;
        Reader reader = this.O0;
        if (reader != null) {
            char[] cArr = this.P0;
            int read = reader.read(cArr, 0, cArr.length);
            if (read > 0) {
                this.j0 = 0;
                this.k0 = read;
                return true;
            }
            O1();
            if (read == 0) {
                throw new IOException("Reader returned 0 characters when trying to read " + this.k0);
            }
        }
        return false;
    }

    public b43(jm1 jm1Var, int i, Reader reader, c cVar, zx zxVar) {
        super(jm1Var, i);
        this.O0 = reader;
        this.P0 = jm1Var.h();
        this.j0 = 0;
        this.k0 = 0;
        this.R0 = cVar;
        this.S0 = zxVar;
        this.T0 = zxVar.p();
        this.Q0 = true;
    }
}
