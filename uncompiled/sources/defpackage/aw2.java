package defpackage;

import java.security.AccessControlException;
import java.security.AccessController;
import java.security.PrivilegedAction;
import java.util.Map;

/* renamed from: aw2  reason: default package */
/* loaded from: classes2.dex */
public class aw2 {
    public static final ThreadLocal a = new ThreadLocal();

    /* renamed from: aw2$a */
    /* loaded from: classes2.dex */
    public static class a implements PrivilegedAction {
        public final /* synthetic */ String a;

        public a(String str) {
            this.a = str;
        }

        @Override // java.security.PrivilegedAction
        public Object run() {
            Map map = (Map) aw2.a.get();
            return map != null ? map.get(this.a) : System.getProperty(this.a);
        }
    }

    public static String b(String str) {
        return (String) AccessController.doPrivileged(new a(str));
    }

    public static boolean c(String str) {
        try {
            String b = b(str);
            if (b != null) {
                return "true".equals(su3.f(b));
            }
        } catch (AccessControlException unused) {
        }
        return false;
    }
}
