package defpackage;

import android.view.View;
import android.view.ViewTreeObserver;
import java.util.Objects;

/* compiled from: OneShotPreDrawListener.java */
/* renamed from: vm2  reason: default package */
/* loaded from: classes.dex */
public final class vm2 implements ViewTreeObserver.OnPreDrawListener, View.OnAttachStateChangeListener {
    public final View a;
    public ViewTreeObserver f0;
    public final Runnable g0;

    public vm2(View view, Runnable runnable) {
        this.a = view;
        this.f0 = view.getViewTreeObserver();
        this.g0 = runnable;
    }

    public static vm2 a(View view, Runnable runnable) {
        Objects.requireNonNull(view, "view == null");
        Objects.requireNonNull(runnable, "runnable == null");
        vm2 vm2Var = new vm2(view, runnable);
        view.getViewTreeObserver().addOnPreDrawListener(vm2Var);
        view.addOnAttachStateChangeListener(vm2Var);
        return vm2Var;
    }

    public void b() {
        if (this.f0.isAlive()) {
            this.f0.removeOnPreDrawListener(this);
        } else {
            this.a.getViewTreeObserver().removeOnPreDrawListener(this);
        }
        this.a.removeOnAttachStateChangeListener(this);
    }

    @Override // android.view.ViewTreeObserver.OnPreDrawListener
    public boolean onPreDraw() {
        b();
        this.g0.run();
        return true;
    }

    @Override // android.view.View.OnAttachStateChangeListener
    public void onViewAttachedToWindow(View view) {
        this.f0 = view.getViewTreeObserver();
    }

    @Override // android.view.View.OnAttachStateChangeListener
    public void onViewDetachedFromWindow(View view) {
        b();
    }
}
