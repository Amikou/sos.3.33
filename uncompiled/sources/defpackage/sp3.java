package defpackage;

import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import androidx.appcompat.widget.Toolbar;
import net.safemoon.androidwallet.R;

/* compiled from: SimpleTransparentToolbarWithTitleBinding.java */
/* renamed from: sp3  reason: default package */
/* loaded from: classes2.dex */
public final class sp3 {
    public final Toolbar a;
    public final ImageView b;
    public final TextView c;
    public final TextView d;
    public final TextView e;

    public sp3(Toolbar toolbar, ImageView imageView, TextView textView, TextView textView2, TextView textView3) {
        this.a = toolbar;
        this.b = imageView;
        this.c = textView;
        this.d = textView2;
        this.e = textView3;
    }

    public static sp3 a(View view) {
        int i = R.id.ivToolbarBack;
        ImageView imageView = (ImageView) ai4.a(view, R.id.ivToolbarBack);
        if (imageView != null) {
            i = R.id.leftMenu;
            TextView textView = (TextView) ai4.a(view, R.id.leftMenu);
            if (textView != null) {
                i = R.id.rightMenu;
                TextView textView2 = (TextView) ai4.a(view, R.id.rightMenu);
                if (textView2 != null) {
                    i = R.id.tvToolbarTitle;
                    TextView textView3 = (TextView) ai4.a(view, R.id.tvToolbarTitle);
                    if (textView3 != null) {
                        return new sp3((Toolbar) view, imageView, textView, textView2, textView3);
                    }
                }
            }
        }
        throw new NullPointerException("Missing required view with ID: ".concat(view.getResources().getResourceName(i)));
    }

    public Toolbar b() {
        return this.a;
    }
}
