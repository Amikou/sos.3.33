package defpackage;

/* compiled from: com.google.android.gms:play-services-vision-common@@19.1.3 */
/* renamed from: aq5  reason: default package */
/* loaded from: classes.dex */
public final class aq5 implements lp5 {
    public aq5() {
    }

    @Override // defpackage.lp5
    public final byte[] a(byte[] bArr, int i, int i2) {
        byte[] bArr2 = new byte[i2];
        System.arraycopy(bArr, i, bArr2, 0, i2);
        return bArr2;
    }

    public /* synthetic */ aq5(so5 so5Var) {
        this();
    }
}
