package defpackage;

import java.math.BigInteger;
import org.bouncycastle.asn1.g;
import org.bouncycastle.asn1.h;
import org.bouncycastle.asn1.k;
import org.bouncycastle.asn1.n0;

/* renamed from: pr4  reason: default package */
/* loaded from: classes2.dex */
public class pr4 extends h implements vr4 {
    public static final BigInteger k0 = BigInteger.valueOf(1);
    public tr4 a;
    public xs0 f0;
    public rr4 g0;
    public BigInteger h0;
    public BigInteger i0;
    public byte[] j0;

    public pr4(h4 h4Var) {
        if (!(h4Var.D(0) instanceof g) || !((g) h4Var.D(0)).B().equals(k0)) {
            throw new IllegalArgumentException("bad version in X9ECParameters");
        }
        this.h0 = ((g) h4Var.D(4)).B();
        if (h4Var.size() == 6) {
            this.i0 = ((g) h4Var.D(5)).B();
        }
        or4 or4Var = new or4(tr4.p(h4Var.D(1)), this.h0, this.i0, h4.z(h4Var.D(2)));
        this.f0 = or4Var.o();
        c4 D = h4Var.D(3);
        if (D instanceof rr4) {
            this.g0 = (rr4) D;
        } else {
            this.g0 = new rr4(this.f0, (f4) D);
        }
        this.j0 = or4Var.p();
    }

    public pr4(xs0 xs0Var, pt0 pt0Var, BigInteger bigInteger, BigInteger bigInteger2, byte[] bArr) {
        this(xs0Var, new rr4(pt0Var), bigInteger, bigInteger2, bArr);
    }

    public pr4(xs0 xs0Var, rr4 rr4Var, BigInteger bigInteger, BigInteger bigInteger2) {
        this(xs0Var, rr4Var, bigInteger, bigInteger2, (byte[]) null);
    }

    public pr4(xs0 xs0Var, rr4 rr4Var, BigInteger bigInteger, BigInteger bigInteger2, byte[] bArr) {
        tr4 tr4Var;
        this.f0 = xs0Var;
        this.g0 = rr4Var;
        this.h0 = bigInteger;
        this.i0 = bigInteger2;
        this.j0 = bArr;
        if (vs0.l(xs0Var)) {
            tr4Var = new tr4(xs0Var.t().c());
        } else if (!vs0.j(xs0Var)) {
            throw new IllegalArgumentException("'curve' is of an unsupported type");
        } else {
            int[] a = ((qs2) xs0Var.t()).a().a();
            if (a.length == 3) {
                tr4Var = new tr4(a[2], a[1]);
            } else if (a.length != 5) {
                throw new IllegalArgumentException("Only trinomial and pentomial curves are supported");
            } else {
                tr4Var = new tr4(a[4], a[1], a[2], a[3]);
            }
        }
        this.a = tr4Var;
    }

    public static pr4 s(Object obj) {
        if (obj instanceof pr4) {
            return (pr4) obj;
        }
        if (obj != null) {
            return new pr4(h4.z(obj));
        }
        return null;
    }

    @Override // org.bouncycastle.asn1.h, defpackage.c4
    public k i() {
        d4 d4Var = new d4();
        d4Var.a(new g(k0));
        d4Var.a(this.a);
        d4Var.a(new or4(this.f0, this.j0));
        d4Var.a(this.g0);
        d4Var.a(new g(this.h0));
        BigInteger bigInteger = this.i0;
        if (bigInteger != null) {
            d4Var.a(new g(bigInteger));
        }
        return new n0(d4Var);
    }

    public xs0 o() {
        return this.f0;
    }

    public pt0 p() {
        return this.g0.o();
    }

    public BigInteger q() {
        return this.i0;
    }

    public BigInteger t() {
        return this.h0;
    }

    public byte[] w() {
        return this.j0;
    }
}
