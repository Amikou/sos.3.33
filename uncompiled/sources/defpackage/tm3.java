package defpackage;

import java.util.LinkedHashSet;
import java.util.Set;
import kotlin.collections.EmptySet;

/* compiled from: Sets.kt */
/* renamed from: tm3  reason: default package */
/* loaded from: classes2.dex */
public class tm3 extends sm3 {
    public static final <T> Set<T> b() {
        return EmptySet.INSTANCE;
    }

    public static final <T> LinkedHashSet<T> c(T... tArr) {
        fs1.f(tArr, "elements");
        return (LinkedHashSet) ai.H(tArr, new LinkedHashSet(y32.a(tArr.length)));
    }

    /* JADX WARN: Multi-variable type inference failed */
    public static final <T> Set<T> d(Set<? extends T> set) {
        fs1.f(set, "$this$optimizeReadOnlySet");
        int size = set.size();
        if (size != 0) {
            return size != 1 ? set : sm3.a(set.iterator().next());
        }
        return b();
    }
}
