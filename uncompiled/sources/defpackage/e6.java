package defpackage;

import android.os.Bundle;
import android.view.View;

/* compiled from: AccessibilityViewCommand.java */
/* renamed from: e6  reason: default package */
/* loaded from: classes.dex */
public interface e6 {

    /* compiled from: AccessibilityViewCommand.java */
    /* renamed from: e6$a */
    /* loaded from: classes.dex */
    public static abstract class a {
        public void a(Bundle bundle) {
        }
    }

    /* compiled from: AccessibilityViewCommand.java */
    /* renamed from: e6$b */
    /* loaded from: classes.dex */
    public static final class b extends a {
    }

    /* compiled from: AccessibilityViewCommand.java */
    /* renamed from: e6$c */
    /* loaded from: classes.dex */
    public static final class c extends a {
    }

    /* compiled from: AccessibilityViewCommand.java */
    /* renamed from: e6$d */
    /* loaded from: classes.dex */
    public static final class d extends a {
    }

    /* compiled from: AccessibilityViewCommand.java */
    /* renamed from: e6$e */
    /* loaded from: classes.dex */
    public static final class e extends a {
    }

    /* compiled from: AccessibilityViewCommand.java */
    /* renamed from: e6$f */
    /* loaded from: classes.dex */
    public static final class f extends a {
    }

    /* compiled from: AccessibilityViewCommand.java */
    /* renamed from: e6$g */
    /* loaded from: classes.dex */
    public static final class g extends a {
    }

    /* compiled from: AccessibilityViewCommand.java */
    /* renamed from: e6$h */
    /* loaded from: classes.dex */
    public static final class h extends a {
    }

    boolean a(View view, a aVar);
}
