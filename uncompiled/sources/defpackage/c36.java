package defpackage;

/* compiled from: com.google.android.gms:play-services-measurement-impl@@19.0.0 */
/* renamed from: c36  reason: default package */
/* loaded from: classes.dex */
public final class c36 implements b36 {
    public static final wo5<Boolean> a = new ro5(bo5.a("com.google.android.gms.measurement")).b("measurement.module.pixie.ees", false);

    @Override // defpackage.b36
    public final boolean zza() {
        return true;
    }

    @Override // defpackage.b36
    public final boolean zzb() {
        return a.e().booleanValue();
    }
}
