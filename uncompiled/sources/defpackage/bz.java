package defpackage;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import kotlin.Pair;
import kotlin.text.StringsKt__StringsKt;
import org.web3j.abi.datatypes.Int;

/* compiled from: ClassReference.kt */
/* renamed from: bz  reason: default package */
/* loaded from: classes2.dex */
public final class bz implements qw1<Object>, az {
    public static final Map<Class<? extends pd1<?>>, Integer> f0;
    public static final HashMap<String, String> g0;
    public static final HashMap<String, String> h0;
    public static final HashMap<String, String> i0;
    public final Class<?> a;

    /* compiled from: ClassReference.kt */
    /* renamed from: bz$a */
    /* loaded from: classes2.dex */
    public static final class a {
        public a() {
        }

        public /* synthetic */ a(qi0 qi0Var) {
            this();
        }
    }

    /* JADX WARN: Multi-variable type inference failed */
    static {
        new a(null);
        int i = 0;
        List j = b20.j(rc1.class, tc1.class, hd1.class, kd1.class, md1.class, od1.class, qd1.class, rd1.class, sd1.class, td1.class, sc1.class, uc1.class, vc1.class, wc1.class, xc1.class, yc1.class, zc1.class, ad1.class, bd1.class, cd1.class, ed1.class, fd1.class, gd1.class);
        ArrayList arrayList = new ArrayList(c20.q(j, 10));
        for (Object obj : j) {
            int i2 = i + 1;
            if (i < 0) {
                b20.p();
            }
            arrayList.add(qc4.a((Class) obj, Integer.valueOf(i)));
            i = i2;
        }
        f0 = z32.i(arrayList);
        HashMap<String, String> hashMap = new HashMap<>();
        hashMap.put("boolean", "kotlin.Boolean");
        hashMap.put("char", "kotlin.Char");
        hashMap.put("byte", "kotlin.Byte");
        hashMap.put("short", "kotlin.Short");
        hashMap.put(Int.TYPE_NAME, "kotlin.Int");
        hashMap.put("float", "kotlin.Float");
        hashMap.put("long", "kotlin.Long");
        hashMap.put("double", "kotlin.Double");
        g0 = hashMap;
        HashMap<String, String> hashMap2 = new HashMap<>();
        hashMap2.put("java.lang.Boolean", "kotlin.Boolean");
        hashMap2.put("java.lang.Character", "kotlin.Char");
        hashMap2.put("java.lang.Byte", "kotlin.Byte");
        hashMap2.put("java.lang.Short", "kotlin.Short");
        hashMap2.put("java.lang.Integer", "kotlin.Int");
        hashMap2.put("java.lang.Float", "kotlin.Float");
        hashMap2.put("java.lang.Long", "kotlin.Long");
        hashMap2.put("java.lang.Double", "kotlin.Double");
        h0 = hashMap2;
        HashMap<String, String> hashMap3 = new HashMap<>();
        hashMap3.put("java.lang.Object", "kotlin.Any");
        hashMap3.put("java.lang.String", "kotlin.String");
        hashMap3.put("java.lang.CharSequence", "kotlin.CharSequence");
        hashMap3.put("java.lang.Throwable", "kotlin.Throwable");
        hashMap3.put("java.lang.Cloneable", "kotlin.Cloneable");
        hashMap3.put("java.lang.Number", "kotlin.Number");
        hashMap3.put("java.lang.Comparable", "kotlin.Comparable");
        hashMap3.put("java.lang.Enum", "kotlin.Enum");
        hashMap3.put("java.lang.annotation.Annotation", "kotlin.Annotation");
        hashMap3.put("java.lang.Iterable", "kotlin.collections.Iterable");
        hashMap3.put("java.util.Iterator", "kotlin.collections.Iterator");
        hashMap3.put("java.util.Collection", "kotlin.collections.Collection");
        hashMap3.put("java.util.List", "kotlin.collections.List");
        hashMap3.put("java.util.Set", "kotlin.collections.Set");
        hashMap3.put("java.util.ListIterator", "kotlin.collections.ListIterator");
        hashMap3.put("java.util.Map", "kotlin.collections.Map");
        hashMap3.put("java.util.Map$Entry", "kotlin.collections.Map.Entry");
        hashMap3.put("kotlin.jvm.internal.StringCompanionObject", "kotlin.String.Companion");
        hashMap3.put("kotlin.jvm.internal.EnumCompanionObject", "kotlin.Enum.Companion");
        hashMap3.putAll(hashMap);
        hashMap3.putAll(hashMap2);
        Collection<String> values = hashMap.values();
        fs1.e(values, "primitiveFqNames.values");
        for (String str : values) {
            StringBuilder sb = new StringBuilder();
            sb.append("kotlin.jvm.internal.");
            fs1.e(str, "kotlinName");
            sb.append(StringsKt__StringsKt.F0(str, '.', null, 2, null));
            sb.append("CompanionObject");
            String sb2 = sb.toString();
            Pair a2 = qc4.a(sb2, str + ".Companion");
            hashMap3.put(a2.getFirst(), a2.getSecond());
        }
        for (Map.Entry<Class<? extends pd1<?>>, Integer> entry : f0.entrySet()) {
            int intValue = entry.getValue().intValue();
            String name = entry.getKey().getName();
            hashMap3.put(name, "kotlin.Function" + intValue);
        }
        i0 = hashMap3;
        LinkedHashMap linkedHashMap = new LinkedHashMap(y32.a(hashMap3.size()));
        for (Map.Entry entry2 : hashMap3.entrySet()) {
            linkedHashMap.put(entry2.getKey(), StringsKt__StringsKt.F0((String) entry2.getValue(), '.', null, 2, null));
        }
    }

    public bz(Class<?> cls) {
        fs1.f(cls, "jClass");
        this.a = cls;
    }

    @Override // defpackage.az
    public Class<?> a() {
        return this.a;
    }

    public boolean equals(Object obj) {
        return (obj instanceof bz) && fs1.b(nw1.b(this), nw1.b((qw1) obj));
    }

    public int hashCode() {
        return nw1.b(this).hashCode();
    }

    public String toString() {
        return a().toString() + " (Kotlin reflection is not available)";
    }
}
