package defpackage;

import defpackage.dt2;
import java.util.concurrent.ScheduledExecutorService;

/* compiled from: com.google.android.gms:play-services-basement@@17.4.0 */
/* renamed from: x35  reason: default package */
/* loaded from: classes.dex */
public final class x35 implements dt2.a {
    @Override // defpackage.dt2.a
    public final ScheduledExecutorService a() {
        return mf5.a().i(1, bs5.a);
    }
}
