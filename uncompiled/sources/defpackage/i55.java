package defpackage;

import java.util.Iterator;

/* compiled from: com.google.android.gms:play-services-measurement@@19.0.0 */
/* renamed from: i55  reason: default package */
/* loaded from: classes.dex */
public final class i55 implements Iterator<z55> {
    public final /* synthetic */ Iterator a;

    public i55(Iterator it) {
        this.a = it;
    }

    @Override // java.util.Iterator
    public final boolean hasNext() {
        return this.a.hasNext();
    }

    @Override // java.util.Iterator
    public final /* bridge */ /* synthetic */ z55 next() {
        return new f65((String) this.a.next());
    }
}
