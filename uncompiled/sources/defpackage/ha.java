package defpackage;

import com.google.crypto.tink.g;
import com.google.crypto.tink.proto.KeyData;
import com.google.crypto.tink.proto.f;
import com.google.crypto.tink.proto.h;
import com.google.crypto.tink.shaded.protobuf.ByteString;
import com.google.crypto.tink.shaded.protobuf.InvalidProtocolBufferException;
import com.google.crypto.tink.shaded.protobuf.n;
import java.security.GeneralSecurityException;

/* compiled from: AesCtrKeyManager.java */
/* renamed from: ha  reason: default package */
/* loaded from: classes2.dex */
public class ha extends g<f> {

    /* compiled from: AesCtrKeyManager.java */
    /* renamed from: ha$a */
    /* loaded from: classes2.dex */
    public class a extends g.b<gq1, f> {
        public a(Class cls) {
            super(cls);
        }

        @Override // com.google.crypto.tink.g.b
        /* renamed from: c */
        public gq1 a(f fVar) throws GeneralSecurityException {
            return new ga(fVar.I().toByteArray(), fVar.J().E());
        }
    }

    /* compiled from: AesCtrKeyManager.java */
    /* renamed from: ha$b */
    /* loaded from: classes2.dex */
    public class b extends g.a<com.google.crypto.tink.proto.g, f> {
        public b(Class cls) {
            super(cls);
        }

        @Override // com.google.crypto.tink.g.a
        /* renamed from: e */
        public f a(com.google.crypto.tink.proto.g gVar) throws GeneralSecurityException {
            return f.L().t(gVar.G()).s(ByteString.copyFrom(p33.c(gVar.E()))).u(ha.this.k()).build();
        }

        @Override // com.google.crypto.tink.g.a
        /* renamed from: f */
        public com.google.crypto.tink.proto.g c(ByteString byteString) throws InvalidProtocolBufferException {
            return com.google.crypto.tink.proto.g.H(byteString, n.b());
        }

        @Override // com.google.crypto.tink.g.a
        /* renamed from: g */
        public void d(com.google.crypto.tink.proto.g gVar) throws GeneralSecurityException {
            ug4.a(gVar.E());
            ha.this.n(gVar.G());
        }
    }

    public ha() {
        super(f.class, new a(gq1.class));
    }

    @Override // com.google.crypto.tink.g
    public String c() {
        return "type.googleapis.com/google.crypto.tink.AesCtrKey";
    }

    @Override // com.google.crypto.tink.g
    public g.a<?, f> e() {
        return new b(com.google.crypto.tink.proto.g.class);
    }

    @Override // com.google.crypto.tink.g
    public KeyData.KeyMaterialType f() {
        return KeyData.KeyMaterialType.SYMMETRIC;
    }

    public int k() {
        return 0;
    }

    @Override // com.google.crypto.tink.g
    /* renamed from: l */
    public f g(ByteString byteString) throws InvalidProtocolBufferException {
        return f.M(byteString, n.b());
    }

    @Override // com.google.crypto.tink.g
    /* renamed from: m */
    public void i(f fVar) throws GeneralSecurityException {
        ug4.c(fVar.K(), k());
        ug4.a(fVar.I().size());
        n(fVar.J());
    }

    public final void n(h hVar) throws GeneralSecurityException {
        if (hVar.E() < 12 || hVar.E() > 16) {
            throw new GeneralSecurityException("invalid IV size");
        }
    }
}
