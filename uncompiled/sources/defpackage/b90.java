package defpackage;

import kotlin.coroutines.CoroutineContext;

/* compiled from: CoroutineName.kt */
/* renamed from: b90  reason: default package */
/* loaded from: classes2.dex */
public final class b90 extends u4 {
    public static final a f0 = new a(null);
    public final String a;

    /* compiled from: CoroutineName.kt */
    /* renamed from: b90$a */
    /* loaded from: classes2.dex */
    public static final class a implements CoroutineContext.b<b90> {
        public a() {
        }

        public /* synthetic */ a(qi0 qi0Var) {
            this();
        }
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        return (obj instanceof b90) && fs1.b(this.a, ((b90) obj).a);
    }

    public final String h() {
        return this.a;
    }

    public int hashCode() {
        return this.a.hashCode();
    }

    public String toString() {
        return "CoroutineName(" + this.a + ')';
    }
}
