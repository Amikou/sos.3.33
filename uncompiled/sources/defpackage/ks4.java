package defpackage;

import androidx.media3.common.ParserException;
import androidx.media3.common.util.c;
import com.google.common.collect.ImmutableList;
import defpackage.y92;
import java.io.IOException;
import java.io.StringReader;
import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserException;
import org.xmlpull.v1.XmlPullParserFactory;
import zendesk.support.request.CellBase;

/* compiled from: XmpMotionPhotoDescriptionParser.java */
/* renamed from: ks4  reason: default package */
/* loaded from: classes.dex */
public final class ks4 {
    public static final String[] a = {"Camera:MotionPhoto", "GCamera:MotionPhoto", "Camera:MicroVideo", "GCamera:MicroVideo"};
    public static final String[] b = {"Camera:MotionPhotoPresentationTimestampUs", "GCamera:MotionPhotoPresentationTimestampUs", "Camera:MicroVideoPresentationTimestampUs", "GCamera:MicroVideoPresentationTimestampUs"};
    public static final String[] c = {"Camera:MicroVideoOffset", "GCamera:MicroVideoOffset"};

    public static y92 a(String str) throws IOException {
        try {
            return b(str);
        } catch (ParserException | NumberFormatException | XmlPullParserException unused) {
            p12.i("MotionPhotoXmpParser", "Ignoring unexpected XMP metadata");
            return null;
        }
    }

    public static y92 b(String str) throws XmlPullParserException, IOException {
        XmlPullParser newPullParser = XmlPullParserFactory.newInstance().newPullParser();
        newPullParser.setInput(new StringReader(str));
        newPullParser.next();
        if (c.f(newPullParser, "x:xmpmeta")) {
            long j = CellBase.ID_SYSTEM_MESSAGE_REQUEST_CLOSED;
            ImmutableList<y92.a> of = ImmutableList.of();
            do {
                newPullParser.next();
                if (c.f(newPullParser, "rdf:Description")) {
                    if (!d(newPullParser)) {
                        return null;
                    }
                    j = e(newPullParser);
                    of = c(newPullParser);
                } else if (c.f(newPullParser, "Container:Directory")) {
                    of = f(newPullParser, "Container", "Item");
                } else if (c.f(newPullParser, "GContainer:Directory")) {
                    of = f(newPullParser, "GContainer", "GContainerItem");
                }
            } while (!c.d(newPullParser, "x:xmpmeta"));
            if (of.isEmpty()) {
                return null;
            }
            return new y92(j, of);
        }
        throw ParserException.createForMalformedContainer("Couldn't find xmp metadata", null);
    }

    public static ImmutableList<y92.a> c(XmlPullParser xmlPullParser) {
        for (String str : c) {
            String a2 = c.a(xmlPullParser, str);
            if (a2 != null) {
                return ImmutableList.of(new y92.a("image/jpeg", "Primary", 0L, 0L), new y92.a("video/mp4", "MotionPhoto", Long.parseLong(a2), 0L));
            }
        }
        return ImmutableList.of();
    }

    public static boolean d(XmlPullParser xmlPullParser) {
        for (String str : a) {
            String a2 = c.a(xmlPullParser, str);
            if (a2 != null) {
                return Integer.parseInt(a2) == 1;
            }
        }
        return false;
    }

    public static long e(XmlPullParser xmlPullParser) {
        for (String str : b) {
            String a2 = c.a(xmlPullParser, str);
            if (a2 != null) {
                long parseLong = Long.parseLong(a2);
                return parseLong == -1 ? CellBase.ID_SYSTEM_MESSAGE_REQUEST_CLOSED : parseLong;
            }
        }
        return CellBase.ID_SYSTEM_MESSAGE_REQUEST_CLOSED;
    }

    public static ImmutableList<y92.a> f(XmlPullParser xmlPullParser, String str, String str2) throws XmlPullParserException, IOException {
        ImmutableList.a builder = ImmutableList.builder();
        String str3 = str + ":Item";
        String str4 = str + ":Directory";
        do {
            xmlPullParser.next();
            if (c.f(xmlPullParser, str3)) {
                String a2 = c.a(xmlPullParser, str2 + ":Mime");
                String a3 = c.a(xmlPullParser, str2 + ":Semantic");
                String a4 = c.a(xmlPullParser, str2 + ":Length");
                String a5 = c.a(xmlPullParser, str2 + ":Padding");
                if (a2 != null && a3 != null) {
                    builder.a(new y92.a(a2, a3, a4 != null ? Long.parseLong(a4) : 0L, a5 != null ? Long.parseLong(a5) : 0L));
                } else {
                    return ImmutableList.of();
                }
            }
        } while (!c.d(xmlPullParser, str4));
        return builder.l();
    }
}
