package defpackage;

import com.google.android.gms.internal.vision.zzht;
import java.util.AbstractList;
import java.util.Iterator;
import java.util.List;
import java.util.ListIterator;
import java.util.RandomAccess;

/* compiled from: com.google.android.gms:play-services-vision-common@@19.1.3 */
/* renamed from: uy5  reason: default package */
/* loaded from: classes.dex */
public final class uy5 extends AbstractList<String> implements gu5, RandomAccess {
    public final gu5 a;

    public uy5(gu5 gu5Var) {
        this.a = gu5Var;
    }

    @Override // defpackage.gu5
    public final List<?> b() {
        return this.a.b();
    }

    @Override // defpackage.gu5
    public final gu5 c() {
        return this;
    }

    @Override // defpackage.gu5
    public final void f1(zzht zzhtVar) {
        throw new UnsupportedOperationException();
    }

    @Override // java.util.AbstractList, java.util.List
    public final /* synthetic */ Object get(int i) {
        return (String) this.a.get(i);
    }

    @Override // defpackage.gu5
    public final Object h(int i) {
        return this.a.h(i);
    }

    @Override // java.util.AbstractList, java.util.AbstractCollection, java.util.Collection, java.lang.Iterable, java.util.List
    public final Iterator<String> iterator() {
        return new ez5(this);
    }

    @Override // java.util.AbstractList, java.util.List
    public final ListIterator<String> listIterator(int i) {
        return new sy5(this, i);
    }

    @Override // java.util.AbstractCollection, java.util.Collection, java.util.List
    public final int size() {
        return this.a.size();
    }
}
