package defpackage;

import androidx.paging.LoadType;
import defpackage.w02;
import defpackage.yo2;
import java.util.ArrayDeque;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

/* compiled from: CachedPageEventFlow.kt */
/* renamed from: x61  reason: default package */
/* loaded from: classes.dex */
public final class x61<T> {
    public int a;
    public int b;
    public final ArrayDeque<xa4<T>> c = new ArrayDeque<>();
    public final hb2 d = new hb2();

    public final void a(yo2<T> yo2Var) {
        fs1.f(yo2Var, "event");
        if (yo2Var instanceof yo2.b) {
            c((yo2.b) yo2Var);
        } else if (yo2Var instanceof yo2.a) {
            e((yo2.a) yo2Var);
        } else if (yo2Var instanceof yo2.c) {
            d((yo2.c) yo2Var);
        }
    }

    public final List<yo2<T>> b() {
        x02 x02Var;
        x02 x02Var2;
        ArrayList arrayList = new ArrayList();
        if (!this.c.isEmpty()) {
            arrayList.add(yo2.b.g.c(j20.k0(this.c), this.a, this.b, this.d.h()));
        } else {
            hb2 hb2Var = this.d;
            x02Var = hb2Var.d;
            LoadType loadType = LoadType.REFRESH;
            w02 g = x02Var.g();
            yo2.c.a aVar = yo2.c.d;
            if (aVar.a(g, false)) {
                arrayList.add(new yo2.c(loadType, false, g));
            }
            LoadType loadType2 = LoadType.PREPEND;
            w02 f = x02Var.f();
            if (aVar.a(f, false)) {
                arrayList.add(new yo2.c(loadType2, false, f));
            }
            LoadType loadType3 = LoadType.APPEND;
            w02 e = x02Var.e();
            if (aVar.a(e, false)) {
                arrayList.add(new yo2.c(loadType3, false, e));
            }
            x02Var2 = hb2Var.e;
            if (x02Var2 != null) {
                w02 g2 = x02Var2.g();
                if (aVar.a(g2, true)) {
                    arrayList.add(new yo2.c(loadType, true, g2));
                }
                w02 f2 = x02Var2.f();
                if (aVar.a(f2, true)) {
                    arrayList.add(new yo2.c(loadType2, true, f2));
                }
                w02 e2 = x02Var2.e();
                if (aVar.a(e2, true)) {
                    arrayList.add(new yo2.c(loadType3, true, e2));
                }
            }
        }
        return arrayList;
    }

    public final void c(yo2.b<T> bVar) {
        this.d.e(bVar.d());
        int i = w61.b[bVar.e().ordinal()];
        if (i == 1) {
            this.c.clear();
            this.b = bVar.g();
            this.a = bVar.h();
            this.c.addAll(bVar.f());
        } else if (i != 2) {
            if (i != 3) {
                return;
            }
            this.b = bVar.g();
            this.c.addAll(bVar.f());
        } else {
            this.a = bVar.h();
            Iterator<Integer> it = u33.i(bVar.f().size() - 1, 0).iterator();
            while (it.hasNext()) {
                this.c.addFirst(bVar.f().get(((or1) it).b()));
            }
        }
    }

    public final void d(yo2.c<T> cVar) {
        this.d.g(cVar.c(), cVar.a(), cVar.b());
    }

    public final void e(yo2.a<T> aVar) {
        int i = 0;
        this.d.g(aVar.a(), false, w02.c.d.b());
        int i2 = w61.a[aVar.a().ordinal()];
        if (i2 == 1) {
            this.a = aVar.e();
            int d = aVar.d();
            while (i < d) {
                this.c.removeFirst();
                i++;
            }
        } else if (i2 == 2) {
            this.b = aVar.e();
            int d2 = aVar.d();
            while (i < d2) {
                this.c.removeLast();
                i++;
            }
        } else {
            throw new IllegalArgumentException("Page drop type must be prepend or append");
        }
    }
}
