package defpackage;

import zendesk.support.request.CellBase;

/* compiled from: CeaUtil.java */
/* renamed from: uw  reason: default package */
/* loaded from: classes.dex */
public final class uw {
    public static void a(long j, op2 op2Var, f84[] f84VarArr) {
        while (true) {
            if (op2Var.a() <= 1) {
                return;
            }
            int c = c(op2Var);
            int c2 = c(op2Var);
            int e = op2Var.e() + c2;
            if (c2 == -1 || c2 > op2Var.a()) {
                p12.i("CeaUtil", "Skipping remainder of malformed SEI NAL unit.");
                e = op2Var.f();
            } else if (c == 4 && c2 >= 8) {
                int D = op2Var.D();
                int J = op2Var.J();
                int n = J == 49 ? op2Var.n() : 0;
                int D2 = op2Var.D();
                if (J == 47) {
                    op2Var.Q(1);
                }
                boolean z = D == 181 && (J == 49 || J == 47) && D2 == 3;
                if (J == 49) {
                    z &= n == 1195456820;
                }
                if (z) {
                    b(j, op2Var, f84VarArr);
                }
            }
            op2Var.P(e);
        }
    }

    public static void b(long j, op2 op2Var, f84[] f84VarArr) {
        int D = op2Var.D();
        if ((D & 64) != 0) {
            op2Var.Q(1);
            int i = (D & 31) * 3;
            int e = op2Var.e();
            for (f84 f84Var : f84VarArr) {
                op2Var.P(e);
                f84Var.a(op2Var, i);
                if (j != CellBase.ID_SYSTEM_MESSAGE_REQUEST_CLOSED) {
                    f84Var.b(j, 1, i, 0, null);
                }
            }
        }
    }

    public static int c(op2 op2Var) {
        int i = 0;
        while (op2Var.a() != 0) {
            int D = op2Var.D();
            i += D;
            if (D != 255) {
                return i;
            }
        }
        return -1;
    }
}
