package defpackage;

import io.reactivex.internal.util.ExceptionHelper;
import java.util.concurrent.Callable;

/* compiled from: RxJavaPlugins.java */
/* renamed from: da3  reason: default package */
/* loaded from: classes2.dex */
public final class da3 {
    public static volatile ld1<? super Callable<bd3>, ? extends bd3> a;
    public static volatile ld1<? super Callable<bd3>, ? extends bd3> b;
    public static volatile ld1<? super Callable<bd3>, ? extends bd3> c;
    public static volatile ld1<? super Callable<bd3>, ? extends bd3> d;
    public static volatile ld1<? super bd3, ? extends bd3> e;
    public static volatile ld1<? super q71, ? extends q71> f;

    public static <T, R> R a(ld1<T, R> ld1Var, T t) {
        try {
            return ld1Var.apply(t);
        } catch (Throwable th) {
            throw ExceptionHelper.a(th);
        }
    }

    public static bd3 b(ld1<? super Callable<bd3>, ? extends bd3> ld1Var, Callable<bd3> callable) {
        return (bd3) il2.a(a(ld1Var, callable), "Scheduler Callable result can't be null");
    }

    public static bd3 c(Callable<bd3> callable) {
        try {
            return (bd3) il2.a(callable.call(), "Scheduler Callable result can't be null");
        } catch (Throwable th) {
            throw ExceptionHelper.a(th);
        }
    }

    public static bd3 d(Callable<bd3> callable) {
        il2.a(callable, "Scheduler Callable can't be null");
        ld1<? super Callable<bd3>, ? extends bd3> ld1Var = a;
        if (ld1Var == null) {
            return c(callable);
        }
        return b(ld1Var, callable);
    }

    public static bd3 e(Callable<bd3> callable) {
        il2.a(callable, "Scheduler Callable can't be null");
        ld1<? super Callable<bd3>, ? extends bd3> ld1Var = c;
        if (ld1Var == null) {
            return c(callable);
        }
        return b(ld1Var, callable);
    }

    public static bd3 f(Callable<bd3> callable) {
        il2.a(callable, "Scheduler Callable can't be null");
        ld1<? super Callable<bd3>, ? extends bd3> ld1Var = d;
        if (ld1Var == null) {
            return c(callable);
        }
        return b(ld1Var, callable);
    }

    public static bd3 g(Callable<bd3> callable) {
        il2.a(callable, "Scheduler Callable can't be null");
        ld1<? super Callable<bd3>, ? extends bd3> ld1Var = b;
        if (ld1Var == null) {
            return c(callable);
        }
        return b(ld1Var, callable);
    }

    public static <T> q71<T> h(q71<T> q71Var) {
        ld1<? super q71, ? extends q71> ld1Var = f;
        return ld1Var != null ? (q71) a(ld1Var, q71Var) : q71Var;
    }

    public static bd3 i(bd3 bd3Var) {
        ld1<? super bd3, ? extends bd3> ld1Var = e;
        return ld1Var == null ? bd3Var : (bd3) a(ld1Var, bd3Var);
    }
}
