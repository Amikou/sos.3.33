package defpackage;

/* compiled from: com.google.android.gms:play-services-measurement-impl@@19.0.0 */
/* renamed from: x26  reason: default package */
/* loaded from: classes.dex */
public final class x26 implements yp5<y26> {
    public static final x26 f0 = new x26();
    public final yp5<y26> a = gq5.a(gq5.b(new z26()));

    public static boolean a() {
        return f0.zza().zza();
    }

    public static double b() {
        return f0.zza().zzb();
    }

    public static long c() {
        return f0.zza().zzc();
    }

    public static long d() {
        return f0.zza().b();
    }

    public static String e() {
        return f0.zza().c();
    }

    @Override // defpackage.yp5
    /* renamed from: f */
    public final y26 zza() {
        return this.a.zza();
    }
}
