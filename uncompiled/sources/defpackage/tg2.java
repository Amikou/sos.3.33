package defpackage;

/* compiled from: JobSupport.kt */
/* renamed from: tg2  reason: default package */
/* loaded from: classes2.dex */
public final class tg2 extends j12 implements dq1 {
    @Override // defpackage.dq1
    public boolean b() {
        return true;
    }

    @Override // defpackage.dq1
    public tg2 e() {
        return this;
    }

    @Override // defpackage.l12
    public String toString() {
        return ze0.c() ? z("Active") : super.toString();
    }

    public final String z(String str) {
        StringBuilder sb = new StringBuilder();
        sb.append("List{");
        sb.append(str);
        sb.append("}[");
        boolean z = true;
        for (l12 l12Var = (l12) n(); !fs1.b(l12Var, this); l12Var = l12Var.o()) {
            if (l12Var instanceof au1) {
                au1 au1Var = (au1) l12Var;
                if (z) {
                    z = false;
                } else {
                    sb.append(", ");
                }
                sb.append(au1Var);
            }
        }
        sb.append("]");
        String sb2 = sb.toString();
        fs1.e(sb2, "StringBuilder().apply(builderAction).toString()");
        return sb2;
    }
}
