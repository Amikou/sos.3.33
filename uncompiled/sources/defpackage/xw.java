package defpackage;

import java.security.InvalidKeyException;

/* compiled from: ChaCha20.java */
/* renamed from: xw  reason: default package */
/* loaded from: classes2.dex */
public class xw extends yw {
    public xw(byte[] bArr, int i) throws InvalidKeyException {
        super(bArr, i);
    }

    @Override // defpackage.yw
    public int[] d(int[] iArr, int i) {
        if (iArr.length == g() / 4) {
            int[] iArr2 = new int[16];
            yw.k(iArr2, this.a);
            iArr2[12] = i;
            System.arraycopy(iArr, 0, iArr2, 13, iArr.length);
            return iArr2;
        }
        throw new IllegalArgumentException(String.format("ChaCha20 uses 96-bit nonces, but got a %d-bit nonce", Integer.valueOf(iArr.length * 32)));
    }

    @Override // defpackage.yw
    public int g() {
        return 12;
    }
}
