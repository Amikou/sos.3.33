package defpackage;

import androidx.media3.common.j;
import defpackage.gc4;
import defpackage.la2;
import zendesk.support.request.CellBase;

/* compiled from: MpegAudioReader.java */
/* renamed from: ka2  reason: default package */
/* loaded from: classes.dex */
public final class ka2 implements ku0 {
    public final op2 a;
    public final la2.a b;
    public final String c;
    public f84 d;
    public String e;
    public int f;
    public int g;
    public boolean h;
    public boolean i;
    public long j;
    public int k;
    public long l;

    public ka2() {
        this(null);
    }

    @Override // defpackage.ku0
    public void a(op2 op2Var) {
        ii.i(this.d);
        while (op2Var.a() > 0) {
            int i = this.f;
            if (i == 0) {
                b(op2Var);
            } else if (i == 1) {
                h(op2Var);
            } else if (i == 2) {
                g(op2Var);
            } else {
                throw new IllegalStateException();
            }
        }
    }

    public final void b(op2 op2Var) {
        byte[] d = op2Var.d();
        int f = op2Var.f();
        for (int e = op2Var.e(); e < f; e++) {
            boolean z = (d[e] & 255) == 255;
            boolean z2 = this.i && (d[e] & 224) == 224;
            this.i = z;
            if (z2) {
                op2Var.P(e + 1);
                this.i = false;
                this.a.d()[1] = d[e];
                this.g = 2;
                this.f = 1;
                return;
            }
        }
        op2Var.P(f);
    }

    @Override // defpackage.ku0
    public void c() {
        this.f = 0;
        this.g = 0;
        this.i = false;
        this.l = CellBase.ID_SYSTEM_MESSAGE_REQUEST_CLOSED;
    }

    @Override // defpackage.ku0
    public void d() {
    }

    @Override // defpackage.ku0
    public void e(long j, int i) {
        if (j != CellBase.ID_SYSTEM_MESSAGE_REQUEST_CLOSED) {
            this.l = j;
        }
    }

    @Override // defpackage.ku0
    public void f(r11 r11Var, gc4.d dVar) {
        dVar.a();
        this.e = dVar.b();
        this.d = r11Var.f(dVar.c(), 1);
    }

    public final void g(op2 op2Var) {
        int min = Math.min(op2Var.a(), this.k - this.g);
        this.d.a(op2Var, min);
        int i = this.g + min;
        this.g = i;
        int i2 = this.k;
        if (i < i2) {
            return;
        }
        long j = this.l;
        if (j != CellBase.ID_SYSTEM_MESSAGE_REQUEST_CLOSED) {
            this.d.b(j, 1, i2, 0, null);
            this.l += this.j;
        }
        this.g = 0;
        this.f = 0;
    }

    public final void h(op2 op2Var) {
        int min = Math.min(op2Var.a(), 4 - this.g);
        op2Var.j(this.a.d(), this.g, min);
        int i = this.g + min;
        this.g = i;
        if (i < 4) {
            return;
        }
        this.a.P(0);
        if (!this.b.a(this.a.n())) {
            this.g = 0;
            this.f = 1;
            return;
        }
        la2.a aVar = this.b;
        this.k = aVar.c;
        if (!this.h) {
            this.j = (aVar.g * 1000000) / aVar.d;
            this.d.f(new j.b().S(this.e).e0(this.b.b).W(4096).H(this.b.e).f0(this.b.d).V(this.c).E());
            this.h = true;
        }
        this.a.P(0);
        this.d.a(this.a, 4);
        this.f = 2;
    }

    public ka2(String str) {
        this.f = 0;
        op2 op2Var = new op2(4);
        this.a = op2Var;
        op2Var.d()[0] = -1;
        this.b = new la2.a();
        this.l = CellBase.ID_SYSTEM_MESSAGE_REQUEST_CLOSED;
        this.c = str;
    }
}
