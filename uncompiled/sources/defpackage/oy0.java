package defpackage;

import java.io.File;

/* compiled from: Exceptions.kt */
/* renamed from: oy0  reason: default package */
/* loaded from: classes2.dex */
public final class oy0 {
    public static final String b(File file, File file2, String str) {
        StringBuilder sb = new StringBuilder(file.toString());
        if (file2 != null) {
            sb.append(" -> " + file2);
        }
        if (str != null) {
            sb.append(": " + str);
        }
        String sb2 = sb.toString();
        fs1.e(sb2, "sb.toString()");
        return sb2;
    }
}
