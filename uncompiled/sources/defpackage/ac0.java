package defpackage;

import defpackage.pt0;

/* renamed from: ac0  reason: default package */
/* loaded from: classes2.dex */
public class ac0 extends pt0.c {
    public ac0(xs0 xs0Var, ct0 ct0Var, ct0 ct0Var2) {
        this(xs0Var, ct0Var, ct0Var2, false);
    }

    public ac0(xs0 xs0Var, ct0 ct0Var, ct0 ct0Var2, boolean z) {
        super(xs0Var, ct0Var, ct0Var2);
        if ((ct0Var == null) != (ct0Var2 == null)) {
            throw new IllegalArgumentException("Exactly one of the field elements is null");
        }
        this.e = z;
    }

    public ac0(xs0 xs0Var, ct0 ct0Var, ct0 ct0Var2, ct0[] ct0VarArr, boolean z) {
        super(xs0Var, ct0Var, ct0Var2, ct0VarArr);
        this.e = z;
    }

    @Override // defpackage.pt0
    public pt0 H() {
        return (u() || this.c.i()) ? this : N(false).a(this);
    }

    @Override // defpackage.pt0
    public pt0 J() {
        if (u()) {
            return this;
        }
        return this.c.i() ? i().v() : N(true);
    }

    @Override // defpackage.pt0
    public pt0 K(pt0 pt0Var) {
        return this == pt0Var ? H() : u() ? pt0Var : pt0Var.u() ? J() : this.c.i() ? pt0Var : N(false).a(pt0Var);
    }

    public zb0 L(zb0 zb0Var, int[] iArr) {
        zb0 zb0Var2 = (zb0) i().o();
        if (zb0Var.h()) {
            return zb0Var2;
        }
        zb0 zb0Var3 = new zb0();
        if (iArr == null) {
            iArr = zb0Var3.f;
            yb0.j(zb0Var.f, iArr);
        }
        yb0.j(iArr, zb0Var3.f);
        int[] iArr2 = zb0Var3.f;
        yb0.e(iArr2, zb0Var2.f, iArr2);
        return zb0Var3;
    }

    public zb0 M() {
        ct0[] ct0VarArr = this.d;
        zb0 zb0Var = (zb0) ct0VarArr[1];
        if (zb0Var == null) {
            zb0 L = L((zb0) ct0VarArr[0], null);
            ct0VarArr[1] = L;
            return L;
        }
        return zb0Var;
    }

    public ac0 N(boolean z) {
        zb0 zb0Var = (zb0) this.b;
        zb0 zb0Var2 = (zb0) this.c;
        zb0 zb0Var3 = (zb0) this.d[0];
        zb0 M = M();
        int[] h = ed2.h();
        yb0.j(zb0Var.f, h);
        yb0.i(ed2.b(h, h, h) + ed2.d(M.f, h), h);
        int[] h2 = ed2.h();
        yb0.o(zb0Var2.f, h2);
        int[] h3 = ed2.h();
        yb0.e(h2, zb0Var2.f, h3);
        int[] h4 = ed2.h();
        yb0.e(h3, zb0Var.f, h4);
        yb0.o(h4, h4);
        int[] h5 = ed2.h();
        yb0.j(h3, h5);
        yb0.o(h5, h5);
        zb0 zb0Var4 = new zb0(h3);
        yb0.j(h, zb0Var4.f);
        int[] iArr = zb0Var4.f;
        yb0.n(iArr, h4, iArr);
        int[] iArr2 = zb0Var4.f;
        yb0.n(iArr2, h4, iArr2);
        zb0 zb0Var5 = new zb0(h4);
        yb0.n(h4, zb0Var4.f, zb0Var5.f);
        int[] iArr3 = zb0Var5.f;
        yb0.e(iArr3, h, iArr3);
        int[] iArr4 = zb0Var5.f;
        yb0.n(iArr4, h5, iArr4);
        zb0 zb0Var6 = new zb0(h2);
        if (!ed2.t(zb0Var3.f)) {
            int[] iArr5 = zb0Var6.f;
            yb0.e(iArr5, zb0Var3.f, iArr5);
        }
        zb0 zb0Var7 = null;
        if (z) {
            zb0Var7 = new zb0(h5);
            int[] iArr6 = zb0Var7.f;
            yb0.e(iArr6, M.f, iArr6);
            int[] iArr7 = zb0Var7.f;
            yb0.o(iArr7, iArr7);
        }
        return new ac0(i(), zb0Var4, zb0Var5, new ct0[]{zb0Var6, zb0Var7}, this.e);
    }

    @Override // defpackage.pt0
    public pt0 a(pt0 pt0Var) {
        int[] iArr;
        int[] iArr2;
        int[] iArr3;
        int[] iArr4;
        if (u()) {
            return pt0Var;
        }
        if (pt0Var.u()) {
            return this;
        }
        if (this == pt0Var) {
            return J();
        }
        xs0 i = i();
        zb0 zb0Var = (zb0) this.b;
        zb0 zb0Var2 = (zb0) this.c;
        zb0 zb0Var3 = (zb0) this.d[0];
        zb0 zb0Var4 = (zb0) pt0Var.q();
        zb0 zb0Var5 = (zb0) pt0Var.r();
        zb0 zb0Var6 = (zb0) pt0Var.s(0);
        int[] j = ed2.j();
        int[] h = ed2.h();
        int[] h2 = ed2.h();
        int[] h3 = ed2.h();
        boolean h4 = zb0Var3.h();
        if (h4) {
            iArr = zb0Var4.f;
            iArr2 = zb0Var5.f;
        } else {
            yb0.j(zb0Var3.f, h2);
            yb0.e(h2, zb0Var4.f, h);
            yb0.e(h2, zb0Var3.f, h2);
            yb0.e(h2, zb0Var5.f, h2);
            iArr = h;
            iArr2 = h2;
        }
        boolean h5 = zb0Var6.h();
        if (h5) {
            iArr3 = zb0Var.f;
            iArr4 = zb0Var2.f;
        } else {
            yb0.j(zb0Var6.f, h3);
            yb0.e(h3, zb0Var.f, j);
            yb0.e(h3, zb0Var6.f, h3);
            yb0.e(h3, zb0Var2.f, h3);
            iArr3 = j;
            iArr4 = h3;
        }
        int[] h6 = ed2.h();
        yb0.n(iArr3, iArr, h6);
        yb0.n(iArr4, iArr2, h);
        if (ed2.v(h6)) {
            return ed2.v(h) ? J() : i.v();
        }
        int[] h7 = ed2.h();
        yb0.j(h6, h7);
        int[] h8 = ed2.h();
        yb0.e(h7, h6, h8);
        yb0.e(h7, iArr3, h2);
        yb0.g(h8, h8);
        ed2.y(iArr4, h8, j);
        yb0.i(ed2.b(h2, h2, h8), h8);
        zb0 zb0Var7 = new zb0(h3);
        yb0.j(h, zb0Var7.f);
        int[] iArr5 = zb0Var7.f;
        yb0.n(iArr5, h8, iArr5);
        zb0 zb0Var8 = new zb0(h8);
        yb0.n(h2, zb0Var7.f, zb0Var8.f);
        yb0.f(zb0Var8.f, h, j);
        yb0.h(j, zb0Var8.f);
        zb0 zb0Var9 = new zb0(h6);
        if (!h4) {
            int[] iArr6 = zb0Var9.f;
            yb0.e(iArr6, zb0Var3.f, iArr6);
        }
        if (!h5) {
            int[] iArr7 = zb0Var9.f;
            yb0.e(iArr7, zb0Var6.f, iArr7);
        }
        return new ac0(i, zb0Var7, zb0Var8, new ct0[]{zb0Var9, L(zb0Var9, (h4 && h5) ? null : null)}, this.e);
    }

    @Override // defpackage.pt0
    public pt0 d() {
        return new ac0(null, f(), g());
    }

    @Override // defpackage.pt0
    public ct0 s(int i) {
        return i == 1 ? M() : super.s(i);
    }

    @Override // defpackage.pt0
    public pt0 z() {
        return u() ? this : new ac0(i(), this.b, this.c.m(), this.d, this.e);
    }
}
