package defpackage;

/* compiled from: Random.kt */
/* renamed from: r33  reason: default package */
/* loaded from: classes2.dex */
public final class r33 {
    public static final String a(Object obj, Object obj2) {
        fs1.f(obj, "from");
        fs1.f(obj2, "until");
        return "Random range is empty: [" + obj + ", " + obj2 + ").";
    }

    public static final void b(double d, double d2) {
        if (!(d2 > d)) {
            throw new IllegalArgumentException(a(Double.valueOf(d), Double.valueOf(d2)).toString());
        }
    }

    public static final void c(int i, int i2) {
        if (!(i2 > i)) {
            throw new IllegalArgumentException(a(Integer.valueOf(i), Integer.valueOf(i2)).toString());
        }
    }

    public static final void d(long j, long j2) {
        if (!(j2 > j)) {
            throw new IllegalArgumentException(a(Long.valueOf(j), Long.valueOf(j2)).toString());
        }
    }

    public static final int e(int i) {
        return 31 - Integer.numberOfLeadingZeros(i);
    }

    public static final int f(int i, int i2) {
        return (i >>> (32 - i2)) & ((-i2) >> 31);
    }
}
