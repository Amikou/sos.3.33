package defpackage;

import androidx.constraintlayout.core.widgets.ConstraintWidget;
import java.util.ArrayList;

/* compiled from: WidgetContainer.java */
/* renamed from: bp4  reason: default package */
/* loaded from: classes.dex */
public class bp4 extends ConstraintWidget {
    public ArrayList<ConstraintWidget> P0 = new ArrayList<>();

    public void b(ConstraintWidget constraintWidget) {
        this.P0.add(constraintWidget);
        if (constraintWidget.M() != null) {
            ((bp4) constraintWidget.M()).q1(constraintWidget);
        }
        constraintWidget.Z0(this);
    }

    public ArrayList<ConstraintWidget> o1() {
        return this.P0;
    }

    public void p1() {
        ArrayList<ConstraintWidget> arrayList = this.P0;
        if (arrayList == null) {
            return;
        }
        int size = arrayList.size();
        for (int i = 0; i < size; i++) {
            ConstraintWidget constraintWidget = this.P0.get(i);
            if (constraintWidget instanceof bp4) {
                ((bp4) constraintWidget).p1();
            }
        }
    }

    public void q1(ConstraintWidget constraintWidget) {
        this.P0.remove(constraintWidget);
        constraintWidget.s0();
    }

    public void r1() {
        this.P0.clear();
    }

    @Override // androidx.constraintlayout.core.widgets.ConstraintWidget
    public void s0() {
        this.P0.clear();
        super.s0();
    }

    @Override // androidx.constraintlayout.core.widgets.ConstraintWidget
    public void w0(ut utVar) {
        super.w0(utVar);
        int size = this.P0.size();
        for (int i = 0; i < size; i++) {
            this.P0.get(i).w0(utVar);
        }
    }
}
