package defpackage;

import android.util.Log;
import defpackage.fp0;
import defpackage.yo0;
import java.io.File;
import java.io.IOException;

/* compiled from: DiskLruCacheWrapper.java */
/* renamed from: hp0  reason: default package */
/* loaded from: classes.dex */
public class hp0 implements yo0 {
    public final File b;
    public final long c;
    public fp0 e;
    public final cp0 d = new cp0();
    public final wb3 a = new wb3();

    @Deprecated
    public hp0(File file, long j) {
        this.b = file;
        this.c = j;
    }

    public static yo0 c(File file, long j) {
        return new hp0(file, j);
    }

    @Override // defpackage.yo0
    public File a(fx1 fx1Var) {
        String b = this.a.b(fx1Var);
        if (Log.isLoggable("DiskLruCacheWrapper", 2)) {
            StringBuilder sb = new StringBuilder();
            sb.append("Get: Obtained: ");
            sb.append(b);
            sb.append(" for for Key: ");
            sb.append(fx1Var);
        }
        try {
            fp0.e v = d().v(b);
            if (v != null) {
                return v.a(0);
            }
            return null;
        } catch (IOException unused) {
            return null;
        }
    }

    @Override // defpackage.yo0
    public void b(fx1 fx1Var, yo0.b bVar) {
        fp0 d;
        String b = this.a.b(fx1Var);
        this.d.a(b);
        try {
            if (Log.isLoggable("DiskLruCacheWrapper", 2)) {
                StringBuilder sb = new StringBuilder();
                sb.append("Put: Obtained: ");
                sb.append(b);
                sb.append(" for for Key: ");
                sb.append(fx1Var);
            }
            try {
                d = d();
            } catch (IOException unused) {
                Log.isLoggable("DiskLruCacheWrapper", 5);
            }
            if (d.v(b) != null) {
                return;
            }
            fp0.c q = d.q(b);
            if (q != null) {
                try {
                    if (bVar.a(q.f(0))) {
                        q.e();
                    }
                    q.b();
                    return;
                } catch (Throwable th) {
                    q.b();
                    throw th;
                }
            }
            throw new IllegalStateException("Had two simultaneous puts for: " + b);
        } finally {
            this.d.b(b);
        }
    }

    public final synchronized fp0 d() throws IOException {
        if (this.e == null) {
            this.e = fp0.x(this.b, 1, 1, this.c);
        }
        return this.e;
    }
}
