package defpackage;

import java.math.BigInteger;

/* renamed from: su2  reason: default package */
/* loaded from: classes2.dex */
public class su2 implements z41 {
    public final BigInteger a;

    public su2(BigInteger bigInteger) {
        this.a = bigInteger;
    }

    @Override // defpackage.z41
    public int b() {
        return 1;
    }

    @Override // defpackage.z41
    public BigInteger c() {
        return this.a;
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj instanceof su2) {
            return this.a.equals(((su2) obj).a);
        }
        return false;
    }

    public int hashCode() {
        return this.a.hashCode();
    }
}
