package defpackage;

import android.content.res.AssetFileDescriptor;
import android.content.res.AssetManager;
import android.net.Uri;
import com.bumptech.glide.load.data.d;
import com.bumptech.glide.load.data.h;
import com.bumptech.glide.load.data.m;
import defpackage.j92;
import java.io.InputStream;

/* compiled from: AssetUriLoader.java */
/* renamed from: mi  reason: default package */
/* loaded from: classes.dex */
public class mi<Data> implements j92<Uri, Data> {
    public static final int c = 22;
    public final AssetManager a;
    public final a<Data> b;

    /* compiled from: AssetUriLoader.java */
    /* renamed from: mi$a */
    /* loaded from: classes.dex */
    public interface a<Data> {
        d<Data> b(AssetManager assetManager, String str);
    }

    /* compiled from: AssetUriLoader.java */
    /* renamed from: mi$b */
    /* loaded from: classes.dex */
    public static class b implements k92<Uri, AssetFileDescriptor>, a<AssetFileDescriptor> {
        public final AssetManager a;

        public b(AssetManager assetManager) {
            this.a = assetManager;
        }

        @Override // defpackage.k92
        public void a() {
        }

        @Override // defpackage.mi.a
        public d<AssetFileDescriptor> b(AssetManager assetManager, String str) {
            return new h(assetManager, str);
        }

        @Override // defpackage.k92
        public j92<Uri, AssetFileDescriptor> c(qa2 qa2Var) {
            return new mi(this.a, this);
        }
    }

    /* compiled from: AssetUriLoader.java */
    /* renamed from: mi$c */
    /* loaded from: classes.dex */
    public static class c implements k92<Uri, InputStream>, a<InputStream> {
        public final AssetManager a;

        public c(AssetManager assetManager) {
            this.a = assetManager;
        }

        @Override // defpackage.k92
        public void a() {
        }

        @Override // defpackage.mi.a
        public d<InputStream> b(AssetManager assetManager, String str) {
            return new m(assetManager, str);
        }

        @Override // defpackage.k92
        public j92<Uri, InputStream> c(qa2 qa2Var) {
            return new mi(this.a, this);
        }
    }

    public mi(AssetManager assetManager, a<Data> aVar) {
        this.a = assetManager;
        this.b = aVar;
    }

    @Override // defpackage.j92
    /* renamed from: c */
    public j92.a<Data> b(Uri uri, int i, int i2, vn2 vn2Var) {
        return new j92.a<>(new ll2(uri), this.b.b(this.a, uri.toString().substring(c)));
    }

    @Override // defpackage.j92
    /* renamed from: d */
    public boolean a(Uri uri) {
        return "file".equals(uri.getScheme()) && !uri.getPathSegments().isEmpty() && "android_asset".equals(uri.getPathSegments().get(0));
    }
}
