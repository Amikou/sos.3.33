package defpackage;

import java.util.NoSuchElementException;

/* compiled from: MediaChunkIterator.java */
/* renamed from: t52  reason: default package */
/* loaded from: classes.dex */
public interface t52 {
    public static final t52 a = new a();

    /* compiled from: MediaChunkIterator.java */
    /* renamed from: t52$a */
    /* loaded from: classes.dex */
    public class a implements t52 {
        @Override // defpackage.t52
        public long a() {
            throw new NoSuchElementException();
        }

        @Override // defpackage.t52
        public long b() {
            throw new NoSuchElementException();
        }

        @Override // defpackage.t52
        public boolean next() {
            return false;
        }
    }

    long a();

    long b();

    boolean next();
}
