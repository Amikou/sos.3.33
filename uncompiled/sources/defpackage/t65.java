package defpackage;

import com.google.android.gms.internal.measurement.zzbl;
import java.util.List;

/* compiled from: com.google.android.gms:play-services-measurement@@19.0.0 */
/* renamed from: t65  reason: default package */
/* loaded from: classes.dex */
public final class t65 extends o65 {
    public t65() {
        this.a.add(zzbl.EQUALS);
        this.a.add(zzbl.GREATER_THAN);
        this.a.add(zzbl.GREATER_THAN_EQUALS);
        this.a.add(zzbl.IDENTITY_EQUALS);
        this.a.add(zzbl.IDENTITY_NOT_EQUALS);
        this.a.add(zzbl.LESS_THAN);
        this.a.add(zzbl.LESS_THAN_EQUALS);
        this.a.add(zzbl.NOT_EQUALS);
    }

    public static boolean c(z55 z55Var, z55 z55Var2) {
        if (z55Var instanceof m55) {
            z55Var = new f65(z55Var.zzc());
        }
        if (z55Var2 instanceof m55) {
            z55Var2 = new f65(z55Var2.zzc());
        }
        if ((z55Var instanceof f65) && (z55Var2 instanceof f65)) {
            return z55Var.zzc().compareTo(z55Var2.zzc()) < 0;
        }
        double doubleValue = z55Var.b().doubleValue();
        double doubleValue2 = z55Var2.b().doubleValue();
        return (Double.isNaN(doubleValue) || Double.isNaN(doubleValue2) || Double.compare(doubleValue, doubleValue2) >= 0) ? false : true;
    }

    public static boolean d(z55 z55Var, z55 z55Var2) {
        if (z55Var.getClass().equals(z55Var2.getClass())) {
            if ((z55Var instanceof i65) || (z55Var instanceof t55)) {
                return true;
            }
            if (z55Var instanceof z45) {
                if (Double.isNaN(z55Var.b().doubleValue()) || Double.isNaN(z55Var2.b().doubleValue())) {
                    return false;
                }
                return z55Var.b().equals(z55Var2.b());
            } else if (z55Var instanceof f65) {
                return z55Var.zzc().equals(z55Var2.zzc());
            } else {
                if (z55Var instanceof s45) {
                    return z55Var.c().equals(z55Var2.c());
                }
                return z55Var == z55Var2;
            }
        } else if (((z55Var instanceof i65) || (z55Var instanceof t55)) && ((z55Var2 instanceof i65) || (z55Var2 instanceof t55))) {
            return true;
        } else {
            boolean z = z55Var instanceof z45;
            if (z && (z55Var2 instanceof f65)) {
                return d(z55Var, new z45(z55Var2.b()));
            }
            boolean z2 = z55Var instanceof f65;
            if (z2 && (z55Var2 instanceof z45)) {
                return d(new z45(z55Var.b()), z55Var2);
            }
            if (z55Var instanceof s45) {
                return d(new z45(z55Var.b()), z55Var2);
            }
            if (z55Var2 instanceof s45) {
                return d(z55Var, new z45(z55Var2.b()));
            }
            if ((!z2 && !z) || !(z55Var2 instanceof m55)) {
                if ((z55Var instanceof m55) && ((z55Var2 instanceof f65) || (z55Var2 instanceof z45))) {
                    return d(new f65(z55Var.zzc()), z55Var2);
                }
                return false;
            }
            return d(z55Var, new f65(z55Var2.zzc()));
        }
    }

    public static boolean e(z55 z55Var, z55 z55Var2) {
        if (z55Var instanceof m55) {
            z55Var = new f65(z55Var.zzc());
        }
        if (z55Var2 instanceof m55) {
            z55Var2 = new f65(z55Var2.zzc());
        }
        return (((z55Var instanceof f65) && (z55Var2 instanceof f65)) || !(Double.isNaN(z55Var.b().doubleValue()) || Double.isNaN(z55Var2.b().doubleValue()))) && !c(z55Var2, z55Var);
    }

    @Override // defpackage.o65
    public final z55 a(String str, wk5 wk5Var, List<z55> list) {
        boolean d;
        boolean d2;
        vm5.a(vm5.e(str).name(), 2, list);
        z55 a = wk5Var.a(list.get(0));
        z55 a2 = wk5Var.a(list.get(1));
        int ordinal = vm5.e(str).ordinal();
        if (ordinal != 23) {
            if (ordinal == 48) {
                d2 = d(a, a2);
            } else if (ordinal == 42) {
                d = c(a, a2);
            } else if (ordinal != 43) {
                switch (ordinal) {
                    case 37:
                        d = c(a2, a);
                        break;
                    case 38:
                        d = e(a2, a);
                        break;
                    case 39:
                        d = vm5.f(a, a2);
                        break;
                    case 40:
                        d2 = vm5.f(a, a2);
                        break;
                    default:
                        return super.b(str);
                }
            } else {
                d = e(a, a2);
            }
            d = !d2;
        } else {
            d = d(a, a2);
        }
        return d ? z55.c0 : z55.d0;
    }
}
