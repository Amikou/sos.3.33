package defpackage;

/* compiled from: AnimationFrameCacheKey.java */
/* renamed from: ie  reason: default package */
/* loaded from: classes.dex */
public class ie implements wt {
    public final String a;
    public final boolean b;

    public ie(int i, boolean z) {
        this.a = "anim://" + i;
        this.b = z;
    }

    @Override // defpackage.wt
    public boolean a() {
        return false;
    }

    @Override // defpackage.wt
    public String b() {
        return this.a;
    }

    @Override // defpackage.wt
    public boolean equals(Object obj) {
        if (this.b) {
            if (this == obj) {
                return true;
            }
            if (obj == null || ie.class != obj.getClass()) {
                return false;
            }
            return this.a.equals(((ie) obj).a);
        }
        return super.equals(obj);
    }

    @Override // defpackage.wt
    public int hashCode() {
        if (!this.b) {
            return super.hashCode();
        }
        return this.a.hashCode();
    }
}
