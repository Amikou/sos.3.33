package defpackage;

import android.view.View;
import android.webkit.WebView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import androidx.constraintlayout.widget.ConstraintLayout;
import com.google.android.material.button.MaterialButton;
import com.google.android.material.checkbox.MaterialCheckBox;
import net.safemoon.androidwallet.R;

/* compiled from: FragmentGraphTradingViewBinding.java */
/* renamed from: ia1  reason: default package */
/* loaded from: classes2.dex */
public final class ia1 {
    public final MaterialCheckBox a;
    public final MaterialCheckBox b;
    public final MaterialButton c;
    public final ImageView d;
    public final View e;
    public final TextView f;
    public final TextView g;
    public final TextView h;
    public final TextView i;
    public final WebView j;

    public ia1(ConstraintLayout constraintLayout, ImageView imageView, MaterialCheckBox materialCheckBox, MaterialCheckBox materialCheckBox2, MaterialButton materialButton, ImageView imageView2, LinearLayout linearLayout, RelativeLayout relativeLayout, LinearLayout linearLayout2, LinearLayout linearLayout3, View view, TextView textView, TextView textView2, LinearLayout linearLayout4, TextView textView3, TextView textView4, TextView textView5, TextView textView6, WebView webView) {
        this.a = materialCheckBox;
        this.b = materialCheckBox2;
        this.c = materialButton;
        this.d = imageView2;
        this.e = view;
        this.f = textView;
        this.g = textView2;
        this.h = textView4;
        this.i = textView6;
        this.j = webView;
    }

    public static ia1 a(View view) {
        int i = R.id.IvPercentDialog;
        ImageView imageView = (ImageView) ai4.a(view, R.id.IvPercentDialog);
        if (imageView != null) {
            i = R.id.chk_candle_graph_view;
            MaterialCheckBox materialCheckBox = (MaterialCheckBox) ai4.a(view, R.id.chk_candle_graph_view);
            if (materialCheckBox != null) {
                i = R.id.chk_full_screen;
                MaterialCheckBox materialCheckBox2 = (MaterialCheckBox) ai4.a(view, R.id.chk_full_screen);
                if (materialCheckBox2 != null) {
                    i = R.id.dialog_cross;
                    MaterialButton materialButton = (MaterialButton) ai4.a(view, R.id.dialog_cross);
                    if (materialButton != null) {
                        i = R.id.ivDialog;
                        ImageView imageView2 = (ImageView) ai4.a(view, R.id.ivDialog);
                        if (imageView2 != null) {
                            i = R.id.l1;
                            LinearLayout linearLayout = (LinearLayout) ai4.a(view, R.id.l1);
                            if (linearLayout != null) {
                                i = R.id.lSafemoon;
                                RelativeLayout relativeLayout = (RelativeLayout) ai4.a(view, R.id.lSafemoon);
                                if (relativeLayout != null) {
                                    i = R.id.llCaption;
                                    LinearLayout linearLayout2 = (LinearLayout) ai4.a(view, R.id.llCaption);
                                    if (linearLayout2 != null) {
                                        i = R.id.llStatus;
                                        LinearLayout linearLayout3 = (LinearLayout) ai4.a(view, R.id.llStatus);
                                        if (linearLayout3 != null) {
                                            i = R.id.loader;
                                            View a = ai4.a(view, R.id.loader);
                                            if (a != null) {
                                                i = R.id.loading;
                                                TextView textView = (TextView) ai4.a(view, R.id.loading);
                                                if (textView != null) {
                                                    i = R.id.providerLink;
                                                    TextView textView2 = (TextView) ai4.a(view, R.id.providerLink);
                                                    if (textView2 != null) {
                                                        i = R.id.tools;
                                                        LinearLayout linearLayout4 = (LinearLayout) ai4.a(view, R.id.tools);
                                                        if (linearLayout4 != null) {
                                                            i = R.id.tvBalanceDialog;
                                                            TextView textView3 = (TextView) ai4.a(view, R.id.tvBalanceDialog);
                                                            if (textView3 != null) {
                                                                i = R.id.tvNameDialog;
                                                                TextView textView4 = (TextView) ai4.a(view, R.id.tvNameDialog);
                                                                if (textView4 != null) {
                                                                    i = R.id.tvPercentDialog;
                                                                    TextView textView5 = (TextView) ai4.a(view, R.id.tvPercentDialog);
                                                                    if (textView5 != null) {
                                                                        i = R.id.tvPriceDialog;
                                                                        TextView textView6 = (TextView) ai4.a(view, R.id.tvPriceDialog);
                                                                        if (textView6 != null) {
                                                                            i = R.id.webView;
                                                                            WebView webView = (WebView) ai4.a(view, R.id.webView);
                                                                            if (webView != null) {
                                                                                return new ia1((ConstraintLayout) view, imageView, materialCheckBox, materialCheckBox2, materialButton, imageView2, linearLayout, relativeLayout, linearLayout2, linearLayout3, a, textView, textView2, linearLayout4, textView3, textView4, textView5, textView6, webView);
                                                                            }
                                                                        }
                                                                    }
                                                                }
                                                            }
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
        throw new NullPointerException("Missing required view with ID: ".concat(view.getResources().getResourceName(i)));
    }
}
