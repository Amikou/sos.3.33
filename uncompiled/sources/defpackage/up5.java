package defpackage;

import com.google.android.gms.tasks.c;
import java.util.concurrent.Executor;

/* compiled from: com.google.android.gms:play-services-tasks@@17.2.0 */
/* renamed from: up5  reason: default package */
/* loaded from: classes.dex */
public final class up5<TResult> implements l46<TResult> {
    public final Executor a;
    public final Object b = new Object();
    public im2<TResult> c;

    public up5(Executor executor, im2<TResult> im2Var) {
        this.a = executor;
        this.c = im2Var;
    }

    @Override // defpackage.l46
    public final void c(c<TResult> cVar) {
        synchronized (this.b) {
            if (this.c == null) {
                return;
            }
            this.a.execute(new hs5(this, cVar));
        }
    }
}
