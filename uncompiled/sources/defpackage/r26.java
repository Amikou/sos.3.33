package defpackage;

/* compiled from: com.google.android.gms:play-services-measurement-impl@@19.0.0 */
/* renamed from: r26  reason: default package */
/* loaded from: classes.dex */
public final class r26 implements yp5<s26> {
    public static final r26 f0 = new r26();
    public final yp5<s26> a = gq5.a(gq5.b(new t26()));

    public static boolean a() {
        return f0.zza().zza();
    }

    @Override // defpackage.yp5
    /* renamed from: b */
    public final s26 zza() {
        return this.a.zza();
    }
}
