package defpackage;

import android.os.Build;

/* compiled from: AutoSizeableTextView.java */
/* renamed from: jk  reason: default package */
/* loaded from: classes.dex */
public interface jk {
    public static final boolean b;

    static {
        b = Build.VERSION.SDK_INT >= 27;
    }
}
