package defpackage;

import android.os.Handler;
import android.os.Message;
import defpackage.pj1;
import java.util.ArrayList;
import java.util.List;

/* compiled from: SystemHandlerWrapper.java */
/* renamed from: t24  reason: default package */
/* loaded from: classes.dex */
public final class t24 implements pj1 {
    public static final List<b> b = new ArrayList(50);
    public final Handler a;

    /* compiled from: SystemHandlerWrapper.java */
    /* renamed from: t24$b */
    /* loaded from: classes.dex */
    public static final class b implements pj1.a {
        public Message a;

        public b() {
        }

        @Override // defpackage.pj1.a
        public void a() {
            ((Message) ii.e(this.a)).sendToTarget();
            b();
        }

        public final void b() {
            this.a = null;
            t24.n(this);
        }

        public boolean c(Handler handler) {
            boolean sendMessageAtFrontOfQueue = handler.sendMessageAtFrontOfQueue((Message) ii.e(this.a));
            b();
            return sendMessageAtFrontOfQueue;
        }

        public b d(Message message, t24 t24Var) {
            this.a = message;
            return this;
        }
    }

    public t24(Handler handler) {
        this.a = handler;
    }

    public static b m() {
        b remove;
        List<b> list = b;
        synchronized (list) {
            if (list.isEmpty()) {
                remove = new b();
            } else {
                remove = list.remove(list.size() - 1);
            }
        }
        return remove;
    }

    public static void n(b bVar) {
        List<b> list = b;
        synchronized (list) {
            if (list.size() < 50) {
                list.add(bVar);
            }
        }
    }

    @Override // defpackage.pj1
    public pj1.a a(int i, int i2, int i3) {
        return m().d(this.a.obtainMessage(i, i2, i3), this);
    }

    @Override // defpackage.pj1
    public boolean b(pj1.a aVar) {
        return ((b) aVar).c(this.a);
    }

    @Override // defpackage.pj1
    public boolean c(Runnable runnable) {
        return this.a.post(runnable);
    }

    @Override // defpackage.pj1
    public pj1.a d(int i) {
        return m().d(this.a.obtainMessage(i), this);
    }

    @Override // defpackage.pj1
    public boolean e(int i) {
        return this.a.hasMessages(i);
    }

    @Override // defpackage.pj1
    public boolean f(int i) {
        return this.a.sendEmptyMessage(i);
    }

    @Override // defpackage.pj1
    public pj1.a g(int i, int i2, int i3, Object obj) {
        return m().d(this.a.obtainMessage(i, i2, i3, obj), this);
    }

    @Override // defpackage.pj1
    public boolean h(int i, long j) {
        return this.a.sendEmptyMessageAtTime(i, j);
    }

    @Override // defpackage.pj1
    public void i(int i) {
        this.a.removeMessages(i);
    }

    @Override // defpackage.pj1
    public pj1.a j(int i, Object obj) {
        return m().d(this.a.obtainMessage(i, obj), this);
    }

    @Override // defpackage.pj1
    public void k(Object obj) {
        this.a.removeCallbacksAndMessages(obj);
    }
}
