package defpackage;

import android.text.Layout;

/* compiled from: TtmlStyle.java */
/* renamed from: mc4  reason: default package */
/* loaded from: classes.dex */
public final class mc4 {
    public String a;
    public int b;
    public boolean c;
    public int d;
    public boolean e;
    public float k;
    public String l;
    public Layout.Alignment o;
    public Layout.Alignment p;
    public j44 r;
    public int f = -1;
    public int g = -1;
    public int h = -1;
    public int i = -1;
    public int j = -1;
    public int m = -1;
    public int n = -1;
    public int q = -1;
    public float s = Float.MAX_VALUE;

    public mc4 A(String str) {
        this.l = str;
        return this;
    }

    public mc4 B(boolean z) {
        this.i = z ? 1 : 0;
        return this;
    }

    public mc4 C(boolean z) {
        this.f = z ? 1 : 0;
        return this;
    }

    public mc4 D(Layout.Alignment alignment) {
        this.p = alignment;
        return this;
    }

    public mc4 E(int i) {
        this.n = i;
        return this;
    }

    public mc4 F(int i) {
        this.m = i;
        return this;
    }

    public mc4 G(float f) {
        this.s = f;
        return this;
    }

    public mc4 H(Layout.Alignment alignment) {
        this.o = alignment;
        return this;
    }

    public mc4 I(boolean z) {
        this.q = z ? 1 : 0;
        return this;
    }

    public mc4 J(j44 j44Var) {
        this.r = j44Var;
        return this;
    }

    public mc4 K(boolean z) {
        this.g = z ? 1 : 0;
        return this;
    }

    public mc4 a(mc4 mc4Var) {
        return r(mc4Var, true);
    }

    public int b() {
        if (this.e) {
            return this.d;
        }
        throw new IllegalStateException("Background color has not been defined.");
    }

    public int c() {
        if (this.c) {
            return this.b;
        }
        throw new IllegalStateException("Font color has not been defined.");
    }

    public String d() {
        return this.a;
    }

    public float e() {
        return this.k;
    }

    public int f() {
        return this.j;
    }

    public String g() {
        return this.l;
    }

    public Layout.Alignment h() {
        return this.p;
    }

    public int i() {
        return this.n;
    }

    public int j() {
        return this.m;
    }

    public float k() {
        return this.s;
    }

    public int l() {
        int i = this.h;
        if (i == -1 && this.i == -1) {
            return -1;
        }
        return (i == 1 ? 1 : 0) | (this.i == 1 ? 2 : 0);
    }

    public Layout.Alignment m() {
        return this.o;
    }

    public boolean n() {
        return this.q == 1;
    }

    public j44 o() {
        return this.r;
    }

    public boolean p() {
        return this.e;
    }

    public boolean q() {
        return this.c;
    }

    public final mc4 r(mc4 mc4Var, boolean z) {
        int i;
        Layout.Alignment alignment;
        Layout.Alignment alignment2;
        String str;
        if (mc4Var != null) {
            if (!this.c && mc4Var.c) {
                w(mc4Var.b);
            }
            if (this.h == -1) {
                this.h = mc4Var.h;
            }
            if (this.i == -1) {
                this.i = mc4Var.i;
            }
            if (this.a == null && (str = mc4Var.a) != null) {
                this.a = str;
            }
            if (this.f == -1) {
                this.f = mc4Var.f;
            }
            if (this.g == -1) {
                this.g = mc4Var.g;
            }
            if (this.n == -1) {
                this.n = mc4Var.n;
            }
            if (this.o == null && (alignment2 = mc4Var.o) != null) {
                this.o = alignment2;
            }
            if (this.p == null && (alignment = mc4Var.p) != null) {
                this.p = alignment;
            }
            if (this.q == -1) {
                this.q = mc4Var.q;
            }
            if (this.j == -1) {
                this.j = mc4Var.j;
                this.k = mc4Var.k;
            }
            if (this.r == null) {
                this.r = mc4Var.r;
            }
            if (this.s == Float.MAX_VALUE) {
                this.s = mc4Var.s;
            }
            if (z && !this.e && mc4Var.e) {
                u(mc4Var.d);
            }
            if (z && this.m == -1 && (i = mc4Var.m) != -1) {
                this.m = i;
            }
        }
        return this;
    }

    public boolean s() {
        return this.f == 1;
    }

    public boolean t() {
        return this.g == 1;
    }

    public mc4 u(int i) {
        this.d = i;
        this.e = true;
        return this;
    }

    public mc4 v(boolean z) {
        this.h = z ? 1 : 0;
        return this;
    }

    public mc4 w(int i) {
        this.b = i;
        this.c = true;
        return this;
    }

    public mc4 x(String str) {
        this.a = str;
        return this;
    }

    public mc4 y(float f) {
        this.k = f;
        return this;
    }

    public mc4 z(int i) {
        this.j = i;
        return this;
    }
}
