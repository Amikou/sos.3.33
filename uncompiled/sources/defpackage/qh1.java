package defpackage;

import android.annotation.TargetApi;
import android.app.NotificationManager;
import android.content.Context;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageInfo;
import android.content.pm.PackageInstaller;
import android.content.pm.PackageManager;
import android.content.res.Resources;
import android.os.Bundle;
import android.os.UserManager;
import android.util.Log;
import androidx.annotation.RecentlyNonNull;
import androidx.annotation.RecentlyNullable;
import com.google.android.gms.common.ConnectionResult;
import java.util.concurrent.atomic.AtomicBoolean;

/* compiled from: com.google.android.gms:play-services-basement@@17.4.0 */
/* renamed from: qh1  reason: default package */
/* loaded from: classes.dex */
public class qh1 {
    @RecentlyNonNull
    @Deprecated
    public static final int a = 12451000;
    public static boolean b = false;
    public static boolean c = false;
    public static final AtomicBoolean d = new AtomicBoolean();
    public static final AtomicBoolean e = new AtomicBoolean();

    @Deprecated
    public static void a(@RecentlyNonNull Context context) {
        if (d.getAndSet(true)) {
            return;
        }
        try {
            NotificationManager notificationManager = (NotificationManager) context.getSystemService("notification");
            if (notificationManager != null) {
                notificationManager.cancel(10436);
            }
        } catch (SecurityException unused) {
        }
    }

    @RecentlyNonNull
    @Deprecated
    public static int b(@RecentlyNonNull Context context) {
        try {
            return context.getPackageManager().getPackageInfo("com.google.android.gms", 0).versionCode;
        } catch (PackageManager.NameNotFoundException unused) {
            return 0;
        }
    }

    @Deprecated
    public static String c(@RecentlyNonNull int i) {
        return ConnectionResult.N1(i);
    }

    @RecentlyNullable
    public static Context d(@RecentlyNonNull Context context) {
        try {
            return context.createPackageContext("com.google.android.gms", 3);
        } catch (PackageManager.NameNotFoundException unused) {
            return null;
        }
    }

    @RecentlyNullable
    public static Resources e(@RecentlyNonNull Context context) {
        try {
            return context.getPackageManager().getResourcesForApplication("com.google.android.gms");
        } catch (PackageManager.NameNotFoundException unused) {
            return null;
        }
    }

    @RecentlyNonNull
    public static boolean f(@RecentlyNonNull Context context) {
        if (!c) {
            try {
                PackageInfo e2 = kr4.a(context).e("com.google.android.gms", 64);
                rh1.a(context);
                if (e2 != null && !rh1.f(e2, false) && rh1.f(e2, true)) {
                    b = true;
                } else {
                    b = false;
                }
            } catch (PackageManager.NameNotFoundException unused) {
            } finally {
                c = true;
            }
        }
        return b || !vm0.c();
    }

    @RecentlyNonNull
    @Deprecated
    public static int g(@RecentlyNonNull Context context, @RecentlyNonNull int i) {
        try {
            context.getResources().getString(q13.common_google_play_services_unknown_issue);
        } catch (Throwable unused) {
        }
        if (!"com.google.android.gms".equals(context.getPackageName()) && !e.get()) {
            int a2 = s46.a(context);
            if (a2 != 0) {
                int i2 = a;
                if (a2 != i2) {
                    StringBuilder sb = new StringBuilder(320);
                    sb.append("The meta-data tag in your app's AndroidManifest.xml does not have the right value.  Expected ");
                    sb.append(i2);
                    sb.append(" but found ");
                    sb.append(a2);
                    sb.append(".  You must have the following declaration within the <application> element:     <meta-data android:name=\"com.google.android.gms.version\" android:value=\"@integer/google_play_services_version\" />");
                    throw new IllegalStateException(sb.toString());
                }
            } else {
                throw new IllegalStateException("A required meta-data tag in your app's AndroidManifest.xml does not exist.  You must have the following declaration within the <application> element:     <meta-data android:name=\"com.google.android.gms.version\" android:value=\"@integer/google_play_services_version\" />");
            }
        }
        return l(context, (vm0.f(context) || vm0.g(context)) ? false : true, i);
    }

    @RecentlyNonNull
    @Deprecated
    public static boolean h(@RecentlyNonNull Context context, @RecentlyNonNull int i) {
        if (i == 18) {
            return true;
        }
        if (i == 1) {
            return m(context, "com.google.android.gms");
        }
        return false;
    }

    @RecentlyNonNull
    @TargetApi(18)
    public static boolean i(@RecentlyNonNull Context context) {
        Bundle applicationRestrictions;
        return jr2.c() && (applicationRestrictions = ((UserManager) zt2.j(context.getSystemService("user"))).getApplicationRestrictions(context.getPackageName())) != null && "true".equals(applicationRestrictions.getString("restricted_profile"));
    }

    @RecentlyNonNull
    @Deprecated
    public static boolean j(@RecentlyNonNull int i) {
        return i == 1 || i == 2 || i == 3 || i == 9;
    }

    @RecentlyNonNull
    @TargetApi(19)
    @Deprecated
    public static boolean k(@RecentlyNonNull Context context, @RecentlyNonNull int i, @RecentlyNonNull String str) {
        return oe4.b(context, i, str);
    }

    public static int l(Context context, boolean z, int i) {
        zt2.a(i >= 0);
        String packageName = context.getPackageName();
        PackageManager packageManager = context.getPackageManager();
        PackageInfo packageInfo = null;
        if (z) {
            try {
                packageInfo = packageManager.getPackageInfo("com.android.vending", 8256);
            } catch (PackageManager.NameNotFoundException unused) {
                String.valueOf(packageName).concat(" requires the Google Play Store, but it is missing.");
                return 9;
            }
        }
        try {
            PackageInfo packageInfo2 = packageManager.getPackageInfo("com.google.android.gms", 64);
            rh1.a(context);
            if (!rh1.f(packageInfo2, true)) {
                String.valueOf(packageName).concat(" requires Google Play services, but their signature is invalid.");
                return 9;
            } else if (z && (!rh1.f((PackageInfo) zt2.j(packageInfo), true) || !packageInfo.signatures[0].equals(packageInfo2.signatures[0]))) {
                String.valueOf(packageName).concat(" requires Google Play Store, but its signature is invalid.");
                return 9;
            } else if (z35.a(packageInfo2.versionCode) < z35.a(i)) {
                int i2 = packageInfo2.versionCode;
                StringBuilder sb = new StringBuilder(String.valueOf(packageName).length() + 82);
                sb.append("Google Play services out of date for ");
                sb.append(packageName);
                sb.append(".  Requires ");
                sb.append(i);
                sb.append(" but found ");
                sb.append(i2);
                return 2;
            } else {
                ApplicationInfo applicationInfo = packageInfo2.applicationInfo;
                if (applicationInfo == null) {
                    try {
                        applicationInfo = packageManager.getApplicationInfo("com.google.android.gms", 0);
                    } catch (PackageManager.NameNotFoundException e2) {
                        Log.wtf("GooglePlayServicesUtil", String.valueOf(packageName).concat(" requires Google Play services, but they're missing when getting application info."), e2);
                        return 1;
                    }
                }
                return !applicationInfo.enabled ? 3 : 0;
            }
        } catch (PackageManager.NameNotFoundException unused2) {
            String.valueOf(packageName).concat(" requires Google Play services, but they are missing.");
            return 1;
        }
    }

    @TargetApi(21)
    public static boolean m(Context context, String str) {
        ApplicationInfo applicationInfo;
        boolean equals = str.equals("com.google.android.gms");
        if (jr2.f()) {
            try {
                for (PackageInstaller.SessionInfo sessionInfo : context.getPackageManager().getPackageInstaller().getAllSessions()) {
                    if (str.equals(sessionInfo.getAppPackageName())) {
                        return true;
                    }
                }
            } catch (Exception unused) {
                return false;
            }
        }
        try {
            applicationInfo = context.getPackageManager().getApplicationInfo(str, 8192);
        } catch (PackageManager.NameNotFoundException unused2) {
        }
        if (equals) {
            return applicationInfo.enabled;
        }
        return applicationInfo.enabled && !i(context);
    }
}
