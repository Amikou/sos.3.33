package defpackage;

import java.io.IOException;
import java.util.Enumeration;
import org.bouncycastle.asn1.g;
import org.bouncycastle.asn1.h;
import org.bouncycastle.asn1.j0;
import org.bouncycastle.asn1.k;
import org.bouncycastle.asn1.n0;
import org.bouncycastle.asn1.s0;

/* renamed from: zu2  reason: default package */
/* loaded from: classes2.dex */
public class zu2 extends h {
    public f4 a;
    public va f0;
    public j4 g0;

    public zu2(h4 h4Var) {
        Enumeration E = h4Var.E();
        if (((g) E.nextElement()).B().intValue() != 0) {
            throw new IllegalArgumentException("wrong version for private key info");
        }
        this.f0 = va.p(E.nextElement());
        this.a = f4.B(E.nextElement());
        if (E.hasMoreElements()) {
            this.g0 = j4.B((k4) E.nextElement(), false);
        }
    }

    public zu2(va vaVar, c4 c4Var) throws IOException {
        this(vaVar, c4Var, null);
    }

    public zu2(va vaVar, c4 c4Var, j4 j4Var) throws IOException {
        this.a = new j0(c4Var.i().n("DER"));
        this.f0 = vaVar;
        this.g0 = j4Var;
    }

    public static zu2 o(Object obj) {
        if (obj instanceof zu2) {
            return (zu2) obj;
        }
        if (obj != null) {
            return new zu2(h4.z(obj));
        }
        return null;
    }

    @Override // org.bouncycastle.asn1.h, defpackage.c4
    public k i() {
        d4 d4Var = new d4();
        d4Var.a(new g(0L));
        d4Var.a(this.f0);
        d4Var.a(this.a);
        if (this.g0 != null) {
            d4Var.a(new s0(false, 0, this.g0));
        }
        return new n0(d4Var);
    }

    public va p() {
        return this.f0;
    }

    public c4 q() throws IOException {
        return k.s(this.a.D());
    }
}
