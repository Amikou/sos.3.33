package defpackage;

import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.RectF;
import com.rd.draw.data.Orientation;

/* compiled from: ThinWormDrawer.java */
/* renamed from: f54  reason: default package */
/* loaded from: classes2.dex */
public class f54 extends fr4 {
    public f54(Paint paint, mq1 mq1Var) {
        super(paint, mq1Var);
    }

    @Override // defpackage.fr4
    public void a(Canvas canvas, wg4 wg4Var, int i, int i2) {
        if (wg4Var instanceof e54) {
            e54 e54Var = (e54) wg4Var;
            int b = e54Var.b();
            int a = e54Var.a();
            int e = e54Var.e() / 2;
            int m = this.b.m();
            int t = this.b.t();
            int p = this.b.p();
            if (this.b.g() == Orientation.HORIZONTAL) {
                RectF rectF = this.c;
                rectF.left = b;
                rectF.right = a;
                rectF.top = i2 - e;
                rectF.bottom = e + i2;
            } else {
                RectF rectF2 = this.c;
                rectF2.left = i - e;
                rectF2.right = e + i;
                rectF2.top = b;
                rectF2.bottom = a;
            }
            this.a.setColor(t);
            float f = i;
            float f2 = i2;
            float f3 = m;
            canvas.drawCircle(f, f2, f3, this.a);
            this.a.setColor(p);
            canvas.drawRoundRect(this.c, f3, f3, this.a);
        }
    }
}
