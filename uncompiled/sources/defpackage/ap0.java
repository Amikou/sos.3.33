package defpackage;

import android.content.Context;
import com.facebook.cache.common.CacheErrorLogger;
import com.facebook.cache.common.CacheEventListener;
import com.facebook.cache.common.c;
import java.io.File;

/* compiled from: DiskCacheConfig.java */
/* renamed from: ap0  reason: default package */
/* loaded from: classes.dex */
public class ap0 {
    public final int a;
    public final String b;
    public final fw3<File> c;
    public final long d;
    public final long e;
    public final long f;
    public final aw0 g;
    public final CacheErrorLogger h;
    public final CacheEventListener i;
    public final lp0 j;
    public final Context k;
    public final boolean l;

    /* compiled from: DiskCacheConfig.java */
    /* renamed from: ap0$a */
    /* loaded from: classes.dex */
    public class a implements fw3<File> {
        public a() {
        }

        @Override // defpackage.fw3
        /* renamed from: a */
        public File get() {
            xt2.g(ap0.this.k);
            return ap0.this.k.getApplicationContext().getCacheDir();
        }
    }

    /* compiled from: DiskCacheConfig.java */
    /* renamed from: ap0$b */
    /* loaded from: classes.dex */
    public static class b {
        public int a;
        public String b;
        public fw3<File> c;
        public long d;
        public long e;
        public long f;
        public aw0 g;
        public CacheErrorLogger h;
        public CacheEventListener i;
        public lp0 j;
        public boolean k;
        public final Context l;

        public /* synthetic */ b(Context context, a aVar) {
            this(context);
        }

        public ap0 n() {
            return new ap0(this);
        }

        public b(Context context) {
            this.a = 1;
            this.b = "image_cache";
            this.d = 41943040L;
            this.e = 10485760L;
            this.f = 2097152L;
            this.g = new com.facebook.cache.disk.a();
            this.l = context;
        }
    }

    public ap0(b bVar) {
        CacheErrorLogger cacheErrorLogger;
        CacheEventListener cacheEventListener;
        lp0 lp0Var;
        Context context = bVar.l;
        this.k = context;
        xt2.j((bVar.c == null && context == null) ? false : true, "Either a non-null context or a base directory path or supplier must be provided.");
        if (bVar.c == null && context != null) {
            bVar.c = new a();
        }
        this.a = bVar.a;
        this.b = (String) xt2.g(bVar.b);
        this.c = (fw3) xt2.g(bVar.c);
        this.d = bVar.d;
        this.e = bVar.e;
        this.f = bVar.f;
        this.g = (aw0) xt2.g(bVar.g);
        if (bVar.h != null) {
            cacheErrorLogger = bVar.h;
        } else {
            cacheErrorLogger = c.b();
        }
        this.h = cacheErrorLogger;
        if (bVar.i != null) {
            cacheEventListener = bVar.i;
        } else {
            cacheEventListener = lg2.h();
        }
        this.i = cacheEventListener;
        if (bVar.j != null) {
            lp0Var = bVar.j;
        } else {
            lp0Var = og2.b();
        }
        this.j = lp0Var;
        this.l = bVar.k;
    }

    public static b m(Context context) {
        return new b(context, null);
    }

    public String b() {
        return this.b;
    }

    public fw3<File> c() {
        return this.c;
    }

    public CacheErrorLogger d() {
        return this.h;
    }

    public CacheEventListener e() {
        return this.i;
    }

    public long f() {
        return this.d;
    }

    public lp0 g() {
        return this.j;
    }

    public aw0 h() {
        return this.g;
    }

    public boolean i() {
        return this.l;
    }

    public long j() {
        return this.e;
    }

    public long k() {
        return this.f;
    }

    public int l() {
        return this.a;
    }
}
