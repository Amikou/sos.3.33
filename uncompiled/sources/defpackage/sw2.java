package defpackage;

import android.content.Context;
import android.database.Cursor;
import android.net.Uri;
import android.os.Build;
import android.os.Environment;
import android.os.ParcelFileDescriptor;
import android.provider.MediaStore;
import android.text.TextUtils;
import com.bumptech.glide.Priority;
import com.bumptech.glide.load.DataSource;
import com.bumptech.glide.load.data.d;
import defpackage.j92;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.InputStream;

/* compiled from: QMediaStoreUriLoader.java */
/* renamed from: sw2  reason: default package */
/* loaded from: classes.dex */
public final class sw2<DataT> implements j92<Uri, DataT> {
    public final Context a;
    public final j92<File, DataT> b;
    public final j92<Uri, DataT> c;
    public final Class<DataT> d;

    /* compiled from: QMediaStoreUriLoader.java */
    /* renamed from: sw2$a */
    /* loaded from: classes.dex */
    public static abstract class a<DataT> implements k92<Uri, DataT> {
        public final Context a;
        public final Class<DataT> b;

        public a(Context context, Class<DataT> cls) {
            this.a = context;
            this.b = cls;
        }

        @Override // defpackage.k92
        public final void a() {
        }

        @Override // defpackage.k92
        public final j92<Uri, DataT> c(qa2 qa2Var) {
            return new sw2(this.a, qa2Var.d(File.class, this.b), qa2Var.d(Uri.class, this.b), this.b);
        }
    }

    /* compiled from: QMediaStoreUriLoader.java */
    /* renamed from: sw2$b */
    /* loaded from: classes.dex */
    public static final class b extends a<ParcelFileDescriptor> {
        public b(Context context) {
            super(context, ParcelFileDescriptor.class);
        }
    }

    /* compiled from: QMediaStoreUriLoader.java */
    /* renamed from: sw2$c */
    /* loaded from: classes.dex */
    public static final class c extends a<InputStream> {
        public c(Context context) {
            super(context, InputStream.class);
        }
    }

    /* compiled from: QMediaStoreUriLoader.java */
    /* renamed from: sw2$d */
    /* loaded from: classes.dex */
    public static final class d<DataT> implements com.bumptech.glide.load.data.d<DataT> {
        public static final String[] o0 = {"_data"};
        public final Context a;
        public final j92<File, DataT> f0;
        public final j92<Uri, DataT> g0;
        public final Uri h0;
        public final int i0;
        public final int j0;
        public final vn2 k0;
        public final Class<DataT> l0;
        public volatile boolean m0;
        public volatile com.bumptech.glide.load.data.d<DataT> n0;

        public d(Context context, j92<File, DataT> j92Var, j92<Uri, DataT> j92Var2, Uri uri, int i, int i2, vn2 vn2Var, Class<DataT> cls) {
            this.a = context.getApplicationContext();
            this.f0 = j92Var;
            this.g0 = j92Var2;
            this.h0 = uri;
            this.i0 = i;
            this.j0 = i2;
            this.k0 = vn2Var;
            this.l0 = cls;
        }

        @Override // com.bumptech.glide.load.data.d
        public Class<DataT> a() {
            return this.l0;
        }

        @Override // com.bumptech.glide.load.data.d
        public void b() {
            com.bumptech.glide.load.data.d<DataT> dVar = this.n0;
            if (dVar != null) {
                dVar.b();
            }
        }

        public final j92.a<DataT> c() throws FileNotFoundException {
            if (Environment.isExternalStorageLegacy()) {
                return this.f0.b(h(this.h0), this.i0, this.j0, this.k0);
            }
            return this.g0.b(g() ? MediaStore.setRequireOriginal(this.h0) : this.h0, this.i0, this.j0, this.k0);
        }

        @Override // com.bumptech.glide.load.data.d
        public void cancel() {
            this.m0 = true;
            com.bumptech.glide.load.data.d<DataT> dVar = this.n0;
            if (dVar != null) {
                dVar.cancel();
            }
        }

        @Override // com.bumptech.glide.load.data.d
        public DataSource d() {
            return DataSource.LOCAL;
        }

        @Override // com.bumptech.glide.load.data.d
        public void e(Priority priority, d.a<? super DataT> aVar) {
            try {
                com.bumptech.glide.load.data.d<DataT> f = f();
                if (f == null) {
                    aVar.c(new IllegalArgumentException("Failed to build fetcher for: " + this.h0));
                    return;
                }
                this.n0 = f;
                if (this.m0) {
                    cancel();
                } else {
                    f.e(priority, aVar);
                }
            } catch (FileNotFoundException e) {
                aVar.c(e);
            }
        }

        public final com.bumptech.glide.load.data.d<DataT> f() throws FileNotFoundException {
            j92.a<DataT> c = c();
            if (c != null) {
                return c.c;
            }
            return null;
        }

        public final boolean g() {
            return this.a.checkSelfPermission("android.permission.ACCESS_MEDIA_LOCATION") == 0;
        }

        public final File h(Uri uri) throws FileNotFoundException {
            Cursor cursor = null;
            try {
                Cursor query = this.a.getContentResolver().query(uri, o0, null, null, null);
                if (query != null && query.moveToFirst()) {
                    String string = query.getString(query.getColumnIndexOrThrow("_data"));
                    if (!TextUtils.isEmpty(string)) {
                        File file = new File(string);
                        query.close();
                        return file;
                    }
                    throw new FileNotFoundException("File path was empty in media store for: " + uri);
                }
                throw new FileNotFoundException("Failed to media store entry for: " + uri);
            } catch (Throwable th) {
                if (0 != 0) {
                    cursor.close();
                }
                throw th;
            }
        }
    }

    public sw2(Context context, j92<File, DataT> j92Var, j92<Uri, DataT> j92Var2, Class<DataT> cls) {
        this.a = context.getApplicationContext();
        this.b = j92Var;
        this.c = j92Var2;
        this.d = cls;
    }

    @Override // defpackage.j92
    /* renamed from: c */
    public j92.a<DataT> b(Uri uri, int i, int i2, vn2 vn2Var) {
        return new j92.a<>(new ll2(uri), new d(this.a, this.b, this.c, uri, i, i2, vn2Var, this.d));
    }

    @Override // defpackage.j92
    /* renamed from: d */
    public boolean a(Uri uri) {
        return Build.VERSION.SDK_INT >= 29 && d72.b(uri);
    }
}
