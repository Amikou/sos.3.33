package defpackage;

import com.google.android.gms.internal.vision.n0;
import java.util.Map;

/* compiled from: com.google.android.gms:play-services-vision-common@@19.1.3 */
/* renamed from: ut5  reason: default package */
/* loaded from: classes.dex */
public final class ut5<K> implements Map.Entry<K, Object> {
    public Map.Entry<K, ot5> a;

    public ut5(Map.Entry<K, ot5> entry) {
        this.a = entry;
    }

    public final ot5 a() {
        return this.a.getValue();
    }

    @Override // java.util.Map.Entry
    public final K getKey() {
        return this.a.getKey();
    }

    @Override // java.util.Map.Entry
    public final Object getValue() {
        if (this.a.getValue() == null) {
            return null;
        }
        return ot5.e();
    }

    @Override // java.util.Map.Entry
    public final Object setValue(Object obj) {
        if (obj instanceof n0) {
            return this.a.getValue().a((n0) obj);
        }
        throw new IllegalArgumentException("LazyField now only used for MessageSet, and the value of MessageSet must be an instance of MessageLite");
    }
}
