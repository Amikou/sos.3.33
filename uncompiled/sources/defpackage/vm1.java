package defpackage;

import android.os.Binder;
import android.os.IBinder;
import android.os.IInterface;

/* compiled from: ITrustedWebActivityCallback.java */
/* renamed from: vm1  reason: default package */
/* loaded from: classes.dex */
public interface vm1 extends IInterface {

    /* compiled from: ITrustedWebActivityCallback.java */
    /* renamed from: vm1$a */
    /* loaded from: classes.dex */
    public static abstract class a extends Binder implements vm1 {

        /* compiled from: ITrustedWebActivityCallback.java */
        /* renamed from: vm1$a$a  reason: collision with other inner class name */
        /* loaded from: classes.dex */
        public static class C0286a implements vm1 {
            public IBinder a;

            public C0286a(IBinder iBinder) {
                this.a = iBinder;
            }

            @Override // android.os.IInterface
            public IBinder asBinder() {
                return this.a;
            }
        }

        public static vm1 b(IBinder iBinder) {
            if (iBinder == null) {
                return null;
            }
            IInterface queryLocalInterface = iBinder.queryLocalInterface("android.support.customtabs.trusted.ITrustedWebActivityCallback");
            if (queryLocalInterface != null && (queryLocalInterface instanceof vm1)) {
                return (vm1) queryLocalInterface;
            }
            return new C0286a(iBinder);
        }
    }
}
