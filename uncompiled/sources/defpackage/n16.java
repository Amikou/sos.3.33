package defpackage;

/* compiled from: com.google.android.gms:play-services-measurement-impl@@19.0.0 */
/* renamed from: n16  reason: default package */
/* loaded from: classes.dex */
public final class n16 implements yp5<o16> {
    public static final n16 f0 = new n16();
    public final yp5<o16> a = gq5.a(gq5.b(new p16()));

    public static boolean a() {
        f0.zza().zza();
        return true;
    }

    public static boolean b() {
        return f0.zza().zzb();
    }

    @Override // defpackage.yp5
    /* renamed from: c */
    public final o16 zza() {
        return this.a.zza();
    }
}
