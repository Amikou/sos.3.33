package defpackage;

import com.google.android.gms.internal.vision.zzht;
import com.google.android.gms.internal.vision.zzid;
import com.google.android.gms.internal.vision.zzii;

/* compiled from: com.google.android.gms:play-services-vision-common@@19.1.3 */
/* renamed from: wp5  reason: default package */
/* loaded from: classes.dex */
public final class wp5 {
    public final zzii a;
    public final byte[] b;

    public wp5(int i) {
        byte[] bArr = new byte[i];
        this.b = bArr;
        this.a = zzii.f(bArr);
    }

    public final zzht a() {
        this.a.N();
        return new zzid(this.b);
    }

    public final zzii b() {
        return this.a;
    }

    public /* synthetic */ wp5(int i, so5 so5Var) {
        this(i);
    }
}
