package defpackage;

import com.fasterxml.jackson.core.JsonFactory;
import com.fasterxml.jackson.core.util.InternCache;
import java.util.Arrays;
import java.util.concurrent.atomic.AtomicReference;

/* compiled from: ByteQuadsCanonicalizer.java */
/* renamed from: vs  reason: default package */
/* loaded from: classes.dex */
public final class vs {
    public final vs a;
    public final AtomicReference<a> b;
    public final int c;
    public boolean d;
    public final boolean e;
    public int[] f;
    public int g;
    public int h;
    public int i;
    public int j;
    public int k;
    public String[] l;
    public int m;
    public int n;
    public transient boolean o;
    public boolean p;

    public vs(int i, boolean z, int i2, boolean z2) {
        this.a = null;
        this.c = i2;
        this.d = z;
        this.e = z2;
        int i3 = 16;
        if (i >= 16) {
            if (((i - 1) & i) != 0) {
                while (i3 < i) {
                    i3 += i3;
                }
            }
            this.b = new AtomicReference<>(a.a(i));
        }
        i = i3;
        this.b = new AtomicReference<>(a.a(i));
    }

    public static vs A() {
        long currentTimeMillis = System.currentTimeMillis();
        return B((((int) currentTimeMillis) + ((int) (currentTimeMillis >>> 32))) | 1);
    }

    public static vs B(int i) {
        return new vs(64, true, i, true);
    }

    public static int c(int i) {
        int i2 = i >> 2;
        if (i2 < 64) {
            return 4;
        }
        if (i2 <= 256) {
            return 5;
        }
        return i2 <= 1024 ? 6 : 7;
    }

    public String C(int i) {
        int b = b(w(i));
        int[] iArr = this.f;
        int i2 = iArr[b + 3];
        if (i2 == 1) {
            if (iArr[b] == i) {
                return this.l[b >> 2];
            }
        } else if (i2 == 0) {
            return null;
        }
        int i3 = this.h + ((b >> 3) << 2);
        int i4 = iArr[i3 + 3];
        if (i4 == 1) {
            if (iArr[i3] == i) {
                return this.l[i3 >> 2];
            }
        } else if (i4 == 0) {
            return null;
        }
        return e(b, i);
    }

    public String D(int i, int i2) {
        int b = b(x(i, i2));
        int[] iArr = this.f;
        int i3 = iArr[b + 3];
        if (i3 == 2) {
            if (i == iArr[b] && i2 == iArr[b + 1]) {
                return this.l[b >> 2];
            }
        } else if (i3 == 0) {
            return null;
        }
        int i4 = this.h + ((b >> 3) << 2);
        int i5 = iArr[i4 + 3];
        if (i5 == 2) {
            if (i == iArr[i4] && i2 == iArr[i4 + 1]) {
                return this.l[i4 >> 2];
            }
        } else if (i5 == 0) {
            return null;
        }
        return f(b, i, i2);
    }

    public String E(int i, int i2, int i3) {
        int b = b(y(i, i2, i3));
        int[] iArr = this.f;
        int i4 = iArr[b + 3];
        if (i4 == 3) {
            if (i == iArr[b] && iArr[b + 1] == i2 && iArr[b + 2] == i3) {
                return this.l[b >> 2];
            }
        } else if (i4 == 0) {
            return null;
        }
        int i5 = this.h + ((b >> 3) << 2);
        int i6 = iArr[i5 + 3];
        if (i6 == 3) {
            if (i == iArr[i5] && iArr[i5 + 1] == i2 && iArr[i5 + 2] == i3) {
                return this.l[i5 >> 2];
            }
        } else if (i6 == 0) {
            return null;
        }
        return g(b, i, i2, i3);
    }

    public String F(int[] iArr, int i) {
        if (i < 4) {
            if (i == 3) {
                return E(iArr[0], iArr[1], iArr[2]);
            }
            if (i == 2) {
                return D(iArr[0], iArr[1]);
            }
            return C(iArr[0]);
        }
        int z = z(iArr, i);
        int b = b(z);
        int[] iArr2 = this.f;
        int i2 = iArr2[b + 3];
        if (z == iArr2[b] && i2 == i && k(iArr, i, iArr2[b + 1])) {
            return this.l[b >> 2];
        }
        if (i2 == 0) {
            return null;
        }
        int i3 = this.h + ((b >> 3) << 2);
        int i4 = iArr2[i3 + 3];
        if (z == iArr2[i3] && i4 == i && k(iArr, i, iArr2[i3 + 1])) {
            return this.l[i3 >> 2];
        }
        return h(b, z, iArr, i);
    }

    public vs G(int i) {
        return new vs(this, JsonFactory.Feature.INTERN_FIELD_NAMES.enabledIn(i), this.c, JsonFactory.Feature.FAIL_ON_SYMBOL_HASH_OVERFLOW.enabledIn(i), this.b.get());
    }

    public boolean H() {
        return !this.p;
    }

    public final void I(a aVar) {
        int i = aVar.b;
        a aVar2 = this.b.get();
        if (i == aVar2.b) {
            return;
        }
        if (i > 6000) {
            aVar = a.a(64);
        }
        this.b.compareAndSet(aVar2, aVar);
    }

    public final void J(boolean z) {
        this.k = 0;
        this.m = j();
        this.n = this.g << 3;
        if (z) {
            Arrays.fill(this.f, 0);
            Arrays.fill(this.l, (Object) null);
        }
    }

    public int K() {
        int i = this.h;
        int i2 = 0;
        for (int i3 = 3; i3 < i; i3 += 4) {
            if (this.f[i3] != 0) {
                i2++;
            }
        }
        return i2;
    }

    public final void L() {
        this.o = false;
        this.p = false;
        int[] iArr = this.f;
        String[] strArr = this.l;
        int i = this.g;
        int i2 = this.k;
        int i3 = i + i;
        int i4 = this.m;
        if (i3 > 65536) {
            J(true);
            return;
        }
        this.f = new int[iArr.length + (i << 3)];
        this.g = i3;
        int i5 = i3 << 2;
        this.h = i5;
        this.i = i5 + (i5 >> 1);
        this.j = c(i3);
        this.l = new String[strArr.length << 1];
        J(false);
        int[] iArr2 = new int[16];
        int i6 = 0;
        for (int i7 = 0; i7 < i4; i7 += 4) {
            int i8 = iArr[i7 + 3];
            if (i8 != 0) {
                i6++;
                String str = strArr[i7 >> 2];
                if (i8 == 1) {
                    iArr2[0] = iArr[i7];
                    v(str, iArr2, 1);
                } else if (i8 == 2) {
                    iArr2[0] = iArr[i7];
                    iArr2[1] = iArr[i7 + 1];
                    v(str, iArr2, 2);
                } else if (i8 != 3) {
                    if (i8 > iArr2.length) {
                        iArr2 = new int[i8];
                    }
                    System.arraycopy(iArr, iArr[i7 + 1], iArr2, 0, i8);
                    v(str, iArr2, i8);
                } else {
                    iArr2[0] = iArr[i7];
                    iArr2[1] = iArr[i7 + 1];
                    iArr2[2] = iArr[i7 + 2];
                    v(str, iArr2, 3);
                }
            }
        }
        if (i6 == i2) {
            return;
        }
        throw new IllegalStateException("Failed rehash(): old count=" + i2 + ", copyCount=" + i6);
    }

    public void M() {
        if (this.a == null || !H()) {
            return;
        }
        this.a.I(new a(this));
        this.p = true;
    }

    public int N() {
        int i = this.i;
        int i2 = 0;
        for (int i3 = this.h + 3; i3 < i; i3 += 4) {
            if (this.f[i3] != 0) {
                i2++;
            }
        }
        return i2;
    }

    public int O() {
        return (this.m - j()) >> 2;
    }

    public int P() {
        int i = this.i + 3;
        int i2 = this.g + i;
        int i3 = 0;
        while (i < i2) {
            if (this.f[i] != 0) {
                i3++;
            }
            i += 4;
        }
        return i3;
    }

    public int Q() {
        int i = this.g << 3;
        int i2 = 0;
        for (int i3 = 3; i3 < i; i3 += 4) {
            if (this.f[i3] != 0) {
                i2++;
            }
        }
        return i2;
    }

    public final int a(int[] iArr, int i) {
        int i2 = this.n;
        int i3 = i2 + i;
        int[] iArr2 = this.f;
        if (i3 > iArr2.length) {
            this.f = Arrays.copyOf(this.f, this.f.length + Math.max(i3 - iArr2.length, Math.min(4096, this.g)));
        }
        System.arraycopy(iArr, 0, this.f, i2, i);
        this.n += i;
        return i2;
    }

    public final int b(int i) {
        return (i & (this.g - 1)) << 2;
    }

    public final int d(int i) {
        int b = b(i);
        int[] iArr = this.f;
        if (iArr[b + 3] == 0) {
            return b;
        }
        int i2 = this.h + ((b >> 3) << 2);
        if (iArr[i2 + 3] == 0) {
            return i2;
        }
        int i3 = this.i;
        int i4 = this.j;
        int i5 = i3 + ((b >> (i4 + 2)) << i4);
        int i6 = (1 << i4) + i5;
        while (i5 < i6) {
            if (iArr[i5 + 3] == 0) {
                return i5;
            }
            i5 += 4;
        }
        int i7 = this.m;
        int i8 = i7 + 4;
        this.m = i8;
        if (i8 >= (this.g << 3)) {
            if (this.e) {
                i();
            }
            this.o = true;
        }
        return i7;
    }

    public final String e(int i, int i2) {
        int i3 = this.i;
        int i4 = this.j;
        int i5 = i3 + ((i >> (i4 + 2)) << i4);
        int[] iArr = this.f;
        int i6 = (1 << i4) + i5;
        while (i5 < i6) {
            int i7 = iArr[i5 + 3];
            if (i2 == iArr[i5] && 1 == i7) {
                return this.l[i5 >> 2];
            }
            if (i7 == 0) {
                return null;
            }
            i5 += 4;
        }
        for (int j = j(); j < this.m; j += 4) {
            if (i2 == iArr[j] && 1 == iArr[j + 3]) {
                return this.l[j >> 2];
            }
        }
        return null;
    }

    public final String f(int i, int i2, int i3) {
        int i4 = this.i;
        int i5 = this.j;
        int i6 = i4 + ((i >> (i5 + 2)) << i5);
        int[] iArr = this.f;
        int i7 = (1 << i5) + i6;
        while (i6 < i7) {
            int i8 = iArr[i6 + 3];
            if (i2 == iArr[i6] && i3 == iArr[i6 + 1] && 2 == i8) {
                return this.l[i6 >> 2];
            }
            if (i8 == 0) {
                return null;
            }
            i6 += 4;
        }
        for (int j = j(); j < this.m; j += 4) {
            if (i2 == iArr[j] && i3 == iArr[j + 1] && 2 == iArr[j + 3]) {
                return this.l[j >> 2];
            }
        }
        return null;
    }

    public final String g(int i, int i2, int i3, int i4) {
        int i5 = this.i;
        int i6 = this.j;
        int i7 = i5 + ((i >> (i6 + 2)) << i6);
        int[] iArr = this.f;
        int i8 = (1 << i6) + i7;
        while (i7 < i8) {
            int i9 = iArr[i7 + 3];
            if (i2 == iArr[i7] && i3 == iArr[i7 + 1] && i4 == iArr[i7 + 2] && 3 == i9) {
                return this.l[i7 >> 2];
            }
            if (i9 == 0) {
                return null;
            }
            i7 += 4;
        }
        for (int j = j(); j < this.m; j += 4) {
            if (i2 == iArr[j] && i3 == iArr[j + 1] && i4 == iArr[j + 2] && 3 == iArr[j + 3]) {
                return this.l[j >> 2];
            }
        }
        return null;
    }

    public final String h(int i, int i2, int[] iArr, int i3) {
        int i4 = this.i;
        int i5 = this.j;
        int i6 = i4 + ((i >> (i5 + 2)) << i5);
        int[] iArr2 = this.f;
        int i7 = (1 << i5) + i6;
        while (i6 < i7) {
            int i8 = iArr2[i6 + 3];
            if (i2 == iArr2[i6] && i3 == i8 && k(iArr, i3, iArr2[i6 + 1])) {
                return this.l[i6 >> 2];
            }
            if (i8 == 0) {
                return null;
            }
            i6 += 4;
        }
        for (int j = j(); j < this.m; j += 4) {
            if (i2 == iArr2[j] && i3 == iArr2[j + 3] && k(iArr, i3, iArr2[j + 1])) {
                return this.l[j >> 2];
            }
        }
        return null;
    }

    public void i() {
        if (this.g <= 1024) {
            return;
        }
        throw new IllegalStateException("Spill-over slots in symbol table with " + this.k + " entries, hash area of " + this.g + " slots is now full (all " + (this.g >> 3) + " slots -- suspect a DoS attack based on hash collisions. You can disable the check via `JsonFactory.Feature.FAIL_ON_SYMBOL_HASH_OVERFLOW`");
    }

    public final int j() {
        int i = this.g;
        return (i << 3) - i;
    }

    /* JADX WARN: Removed duplicated region for block: B:13:0x0023 A[RETURN] */
    /* JADX WARN: Removed duplicated region for block: B:14:0x0024  */
    /* JADX WARN: Removed duplicated region for block: B:18:0x0031 A[RETURN] */
    /* JADX WARN: Removed duplicated region for block: B:19:0x0032  */
    /* JADX WARN: Removed duplicated region for block: B:23:0x003f A[RETURN] */
    /* JADX WARN: Removed duplicated region for block: B:24:0x0040  */
    /* JADX WARN: Removed duplicated region for block: B:28:0x004d A[RETURN] */
    /* JADX WARN: Removed duplicated region for block: B:29:0x004e  */
    /*
        Code decompiled incorrectly, please refer to instructions dump.
        To view partially-correct code enable 'Show inconsistent code' option in preferences
    */
    public final boolean k(int[] r6, int r7, int r8) {
        /*
            r5 = this;
            int[] r0 = r5.f
            r1 = 0
            r2 = 1
            switch(r7) {
                case 4: goto L42;
                case 5: goto L34;
                case 6: goto L26;
                case 7: goto L18;
                case 8: goto Lc;
                default: goto L7;
            }
        L7:
            boolean r6 = r5.l(r6, r7, r8)
            return r6
        Lc:
            r7 = r6[r1]
            int r3 = r8 + 1
            r8 = r0[r8]
            if (r7 == r8) goto L15
            return r1
        L15:
            r7 = r2
            r8 = r3
            goto L19
        L18:
            r7 = r1
        L19:
            int r3 = r7 + 1
            r7 = r6[r7]
            int r4 = r8 + 1
            r8 = r0[r8]
            if (r7 == r8) goto L24
            return r1
        L24:
            r8 = r4
            goto L27
        L26:
            r3 = r1
        L27:
            int r7 = r3 + 1
            r3 = r6[r3]
            int r4 = r8 + 1
            r8 = r0[r8]
            if (r3 == r8) goto L32
            return r1
        L32:
            r8 = r4
            goto L35
        L34:
            r7 = r1
        L35:
            int r3 = r7 + 1
            r7 = r6[r7]
            int r4 = r8 + 1
            r8 = r0[r8]
            if (r7 == r8) goto L40
            return r1
        L40:
            r8 = r4
            goto L43
        L42:
            r3 = r1
        L43:
            int r7 = r3 + 1
            r3 = r6[r3]
            int r4 = r8 + 1
            r8 = r0[r8]
            if (r3 == r8) goto L4e
            return r1
        L4e:
            int r8 = r7 + 1
            r7 = r6[r7]
            int r3 = r4 + 1
            r4 = r0[r4]
            if (r7 == r4) goto L59
            return r1
        L59:
            int r7 = r8 + 1
            r8 = r6[r8]
            int r4 = r3 + 1
            r3 = r0[r3]
            if (r8 == r3) goto L64
            return r1
        L64:
            r6 = r6[r7]
            r7 = r0[r4]
            if (r6 == r7) goto L6b
            return r1
        L6b:
            return r2
        */
        throw new UnsupportedOperationException("Method not decompiled: defpackage.vs.k(int[], int, int):boolean");
    }

    public final boolean l(int[] iArr, int i, int i2) {
        int i3 = 0;
        while (true) {
            int i4 = i3 + 1;
            int i5 = i2 + 1;
            if (iArr[i3] != this.f[i2]) {
                return false;
            }
            if (i4 >= i) {
                return true;
            }
            i3 = i4;
            i2 = i5;
        }
    }

    public final void m() {
        if (this.k > (this.g >> 1)) {
            int j = (this.m - j()) >> 2;
            int i = this.k;
            if (j > ((i + 1) >> 7) || i > this.g * 0.8d) {
                this.o = true;
            }
        }
    }

    public final void n() {
        if (this.p) {
            int[] iArr = this.f;
            this.f = Arrays.copyOf(iArr, iArr.length);
            String[] strArr = this.l;
            this.l = (String[]) Arrays.copyOf(strArr, strArr.length);
            this.p = false;
            m();
        }
        if (this.o) {
            L();
        }
    }

    public String toString() {
        int K = K();
        int N = N();
        int P = P();
        int O = O();
        return String.format("[%s: size=%d, hashSize=%d, %d/%d/%d/%d pri/sec/ter/spill (=%s), total:%d]", vs.class.getName(), Integer.valueOf(this.k), Integer.valueOf(this.g), Integer.valueOf(K), Integer.valueOf(N), Integer.valueOf(P), Integer.valueOf(O), Integer.valueOf(K + N + P + O), Integer.valueOf(Q()));
    }

    public String v(String str, int[] iArr, int i) {
        int d;
        n();
        if (this.d) {
            str = InternCache.instance.intern(str);
        }
        if (i == 1) {
            d = d(w(iArr[0]));
            int[] iArr2 = this.f;
            iArr2[d] = iArr[0];
            iArr2[d + 3] = 1;
        } else if (i == 2) {
            d = d(x(iArr[0], iArr[1]));
            int[] iArr3 = this.f;
            iArr3[d] = iArr[0];
            iArr3[d + 1] = iArr[1];
            iArr3[d + 3] = 2;
        } else if (i != 3) {
            int z = z(iArr, i);
            d = d(z);
            this.f[d] = z;
            int a2 = a(iArr, i);
            int[] iArr4 = this.f;
            iArr4[d + 1] = a2;
            iArr4[d + 3] = i;
        } else {
            int d2 = d(y(iArr[0], iArr[1], iArr[2]));
            int[] iArr5 = this.f;
            iArr5[d2] = iArr[0];
            iArr5[d2 + 1] = iArr[1];
            iArr5[d2 + 2] = iArr[2];
            iArr5[d2 + 3] = 3;
            d = d2;
        }
        this.l[d >> 2] = str;
        this.k++;
        m();
        return str;
    }

    public int w(int i) {
        int i2 = i ^ this.c;
        int i3 = i2 + (i2 >>> 16);
        int i4 = i3 ^ (i3 << 3);
        return i4 + (i4 >>> 12);
    }

    public int x(int i, int i2) {
        int i3 = i + (i >>> 15);
        int i4 = ((i3 ^ (i3 >>> 9)) + (i2 * 33)) ^ this.c;
        int i5 = i4 + (i4 >>> 16);
        int i6 = i5 ^ (i5 >>> 4);
        return i6 + (i6 << 3);
    }

    public int y(int i, int i2, int i3) {
        int i4 = i ^ this.c;
        int i5 = (((i4 + (i4 >>> 9)) * 31) + i2) * 33;
        int i6 = (i5 + (i5 >>> 15)) ^ i3;
        int i7 = i6 + (i6 >>> 4);
        int i8 = i7 + (i7 >>> 15);
        return i8 ^ (i8 << 9);
    }

    public int z(int[] iArr, int i) {
        if (i >= 4) {
            int i2 = iArr[0] ^ this.c;
            int i3 = i2 + (i2 >>> 9) + iArr[1];
            int i4 = ((i3 + (i3 >>> 15)) * 33) ^ iArr[2];
            int i5 = i4 + (i4 >>> 4);
            for (int i6 = 3; i6 < i; i6++) {
                int i7 = iArr[i6];
                i5 += i7 ^ (i7 >> 21);
            }
            int i8 = i5 * 65599;
            int i9 = i8 + (i8 >>> 19);
            return (i9 << 5) ^ i9;
        }
        throw new IllegalArgumentException();
    }

    public vs(vs vsVar, boolean z, int i, boolean z2, a aVar) {
        this.a = vsVar;
        this.c = i;
        this.d = z;
        this.e = z2;
        this.b = null;
        this.k = aVar.b;
        int i2 = aVar.a;
        this.g = i2;
        int i3 = i2 << 2;
        this.h = i3;
        this.i = i3 + (i3 >> 1);
        this.j = aVar.c;
        this.f = aVar.d;
        this.l = aVar.e;
        this.m = aVar.f;
        this.n = aVar.g;
        this.o = false;
        this.p = true;
    }

    /* compiled from: ByteQuadsCanonicalizer.java */
    /* renamed from: vs$a */
    /* loaded from: classes.dex */
    public static final class a {
        public final int a;
        public final int b;
        public final int c;
        public final int[] d;
        public final String[] e;
        public final int f;
        public final int g;

        public a(int i, int i2, int i3, int[] iArr, String[] strArr, int i4, int i5) {
            this.a = i;
            this.b = i2;
            this.c = i3;
            this.d = iArr;
            this.e = strArr;
            this.f = i4;
            this.g = i5;
        }

        public static a a(int i) {
            int i2 = i << 3;
            return new a(i, 0, vs.c(i), new int[i2], new String[i << 1], i2 - i, i2);
        }

        public a(vs vsVar) {
            this.a = vsVar.g;
            this.b = vsVar.k;
            this.c = vsVar.j;
            this.d = vsVar.f;
            this.e = vsVar.l;
            this.f = vsVar.m;
            this.g = vsVar.n;
        }
    }
}
