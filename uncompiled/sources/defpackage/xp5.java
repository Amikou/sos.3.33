package defpackage;

/* compiled from: com.google.android.gms:play-services-measurement@@19.0.0 */
/* renamed from: xp5  reason: default package */
/* loaded from: classes.dex */
public final class xp5 extends jv5 {
    public xp5(fw5 fw5Var) {
        super(fw5Var);
    }

    public static final String j(String str, String str2) {
        throw new SecurityException("This implementation should not be used.");
    }

    @Override // defpackage.jv5
    public final boolean h() {
        return false;
    }
}
