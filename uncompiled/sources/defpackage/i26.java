package defpackage;

/* compiled from: com.google.android.gms:play-services-measurement-impl@@19.0.0 */
/* renamed from: i26  reason: default package */
/* loaded from: classes.dex */
public final class i26 implements yp5<j26> {
    public static final i26 f0 = new i26();
    public final yp5<j26> a = gq5.a(gq5.b(new k26()));

    public static boolean a() {
        return f0.zza().zza();
    }

    public static boolean b() {
        return f0.zza().zzb();
    }

    @Override // defpackage.yp5
    /* renamed from: c */
    public final j26 zza() {
        return this.a.zza();
    }
}
