package defpackage;

import android.graphics.Canvas;
import android.graphics.Paint;
import com.rd.animation.type.AnimationType;

/* compiled from: BasicDrawer.java */
/* renamed from: io  reason: default package */
/* loaded from: classes2.dex */
public class io extends hn {
    public Paint c;

    public io(Paint paint, mq1 mq1Var) {
        super(paint, mq1Var);
        Paint paint2 = new Paint();
        this.c = paint2;
        paint2.setStyle(Paint.Style.STROKE);
        this.c.setAntiAlias(true);
        this.c.setStrokeWidth(mq1Var.s());
    }

    public void a(Canvas canvas, int i, boolean z, int i2, int i3) {
        Paint paint;
        float m = this.b.m();
        int s = this.b.s();
        float o = this.b.o();
        int p = this.b.p();
        int t = this.b.t();
        int q = this.b.q();
        AnimationType b = this.b.b();
        if ((b == AnimationType.SCALE && !z) || (b == AnimationType.SCALE_DOWN && z)) {
            m *= o;
        }
        if (i != q) {
            p = t;
        }
        if (b == AnimationType.FILL && i != q) {
            paint = this.c;
            paint.setStrokeWidth(s);
        } else {
            paint = this.a;
        }
        paint.setColor(p);
        canvas.drawCircle(i2, i3, m, paint);
    }
}
