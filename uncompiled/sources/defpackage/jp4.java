package defpackage;

import android.annotation.SuppressLint;
import android.graphics.Rect;
import android.os.Build;
import android.view.View;
import android.view.WindowInsets;
import java.lang.reflect.Constructor;
import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.util.Objects;

/* compiled from: WindowInsetsCompat.java */
/* renamed from: jp4  reason: default package */
/* loaded from: classes.dex */
public class jp4 {
    public static final jp4 b;
    public final l a;

    /* compiled from: WindowInsetsCompat.java */
    @SuppressLint({"SoonBlockedPrivateApi"})
    /* renamed from: jp4$a */
    /* loaded from: classes.dex */
    public static class a {
        public static Field a;
        public static Field b;
        public static Field c;
        public static boolean d;

        static {
            try {
                Field declaredField = View.class.getDeclaredField("mAttachInfo");
                a = declaredField;
                declaredField.setAccessible(true);
                Class<?> cls = Class.forName("android.view.View$AttachInfo");
                Field declaredField2 = cls.getDeclaredField("mStableInsets");
                b = declaredField2;
                declaredField2.setAccessible(true);
                Field declaredField3 = cls.getDeclaredField("mContentInsets");
                c = declaredField3;
                declaredField3.setAccessible(true);
                d = true;
            } catch (ReflectiveOperationException e) {
                StringBuilder sb = new StringBuilder();
                sb.append("Failed to get visible insets from AttachInfo ");
                sb.append(e.getMessage());
            }
        }

        public static jp4 a(View view) {
            if (d && view.isAttachedToWindow()) {
                try {
                    Object obj = a.get(view.getRootView());
                    if (obj != null) {
                        Rect rect = (Rect) b.get(obj);
                        Rect rect2 = (Rect) c.get(obj);
                        if (rect != null && rect2 != null) {
                            jp4 a2 = new b().b(cr1.c(rect)).c(cr1.c(rect2)).a();
                            a2.u(a2);
                            a2.d(view.getRootView());
                            return a2;
                        }
                    }
                } catch (IllegalAccessException e) {
                    StringBuilder sb = new StringBuilder();
                    sb.append("Failed to get insets from AttachInfo. ");
                    sb.append(e.getMessage());
                }
            }
            return null;
        }
    }

    /* compiled from: WindowInsetsCompat.java */
    /* renamed from: jp4$e */
    /* loaded from: classes.dex */
    public static class e extends d {
        public e() {
        }

        public e(jp4 jp4Var) {
            super(jp4Var);
        }
    }

    /* compiled from: WindowInsetsCompat.java */
    /* renamed from: jp4$f */
    /* loaded from: classes.dex */
    public static class f {
        public final jp4 a;
        public cr1[] b;

        public f() {
            this(new jp4((jp4) null));
        }

        public final void a() {
            cr1[] cr1VarArr = this.b;
            if (cr1VarArr != null) {
                cr1 cr1Var = cr1VarArr[m.a(1)];
                cr1 cr1Var2 = this.b[m.a(2)];
                if (cr1Var2 == null) {
                    cr1Var2 = this.a.f(2);
                }
                if (cr1Var == null) {
                    cr1Var = this.a.f(1);
                }
                f(cr1.a(cr1Var, cr1Var2));
                cr1 cr1Var3 = this.b[m.a(16)];
                if (cr1Var3 != null) {
                    e(cr1Var3);
                }
                cr1 cr1Var4 = this.b[m.a(32)];
                if (cr1Var4 != null) {
                    c(cr1Var4);
                }
                cr1 cr1Var5 = this.b[m.a(64)];
                if (cr1Var5 != null) {
                    g(cr1Var5);
                }
            }
        }

        public jp4 b() {
            a();
            return this.a;
        }

        public void c(cr1 cr1Var) {
        }

        public void d(cr1 cr1Var) {
        }

        public void e(cr1 cr1Var) {
        }

        public void f(cr1 cr1Var) {
        }

        public void g(cr1 cr1Var) {
        }

        public f(jp4 jp4Var) {
            this.a = jp4Var;
        }
    }

    /* compiled from: WindowInsetsCompat.java */
    /* renamed from: jp4$i */
    /* loaded from: classes.dex */
    public static class i extends h {
        public i(jp4 jp4Var, WindowInsets windowInsets) {
            super(jp4Var, windowInsets);
        }

        @Override // defpackage.jp4.l
        public jp4 a() {
            return jp4.x(this.c.consumeDisplayCutout());
        }

        @Override // defpackage.jp4.g, defpackage.jp4.l
        public boolean equals(Object obj) {
            if (this == obj) {
                return true;
            }
            if (obj instanceof i) {
                i iVar = (i) obj;
                return Objects.equals(this.c, iVar.c) && Objects.equals(this.g, iVar.g);
            }
            return false;
        }

        @Override // defpackage.jp4.l
        public vp0 f() {
            return vp0.e(this.c.getDisplayCutout());
        }

        @Override // defpackage.jp4.l
        public int hashCode() {
            return this.c.hashCode();
        }

        public i(jp4 jp4Var, i iVar) {
            super(jp4Var, iVar);
        }
    }

    /* compiled from: WindowInsetsCompat.java */
    /* renamed from: jp4$k */
    /* loaded from: classes.dex */
    public static class k extends j {
        public static final jp4 q = jp4.x(WindowInsets.CONSUMED);

        public k(jp4 jp4Var, WindowInsets windowInsets) {
            super(jp4Var, windowInsets);
        }

        @Override // defpackage.jp4.g, defpackage.jp4.l
        public final void d(View view) {
        }

        @Override // defpackage.jp4.g, defpackage.jp4.l
        public cr1 g(int i) {
            return cr1.d(this.c.getInsets(n.a(i)));
        }

        public k(jp4 jp4Var, k kVar) {
            super(jp4Var, kVar);
        }
    }

    /* compiled from: WindowInsetsCompat.java */
    /* renamed from: jp4$l */
    /* loaded from: classes.dex */
    public static class l {
        public static final jp4 b = new b().a().a().b().c();
        public final jp4 a;

        public l(jp4 jp4Var) {
            this.a = jp4Var;
        }

        public jp4 a() {
            return this.a;
        }

        public jp4 b() {
            return this.a;
        }

        public jp4 c() {
            return this.a;
        }

        public void d(View view) {
        }

        public void e(jp4 jp4Var) {
        }

        public boolean equals(Object obj) {
            if (this == obj) {
                return true;
            }
            if (obj instanceof l) {
                l lVar = (l) obj;
                return o() == lVar.o() && n() == lVar.n() && sl2.a(k(), lVar.k()) && sl2.a(i(), lVar.i()) && sl2.a(f(), lVar.f());
            }
            return false;
        }

        public vp0 f() {
            return null;
        }

        public cr1 g(int i) {
            return cr1.e;
        }

        public cr1 h() {
            return k();
        }

        public int hashCode() {
            return sl2.b(Boolean.valueOf(o()), Boolean.valueOf(n()), k(), i(), f());
        }

        public cr1 i() {
            return cr1.e;
        }

        public cr1 j() {
            return k();
        }

        public cr1 k() {
            return cr1.e;
        }

        public cr1 l() {
            return k();
        }

        public jp4 m(int i, int i2, int i3, int i4) {
            return b;
        }

        public boolean n() {
            return false;
        }

        public boolean o() {
            return false;
        }

        public void p(cr1[] cr1VarArr) {
        }

        public void q(cr1 cr1Var) {
        }

        public void r(jp4 jp4Var) {
        }

        public void s(cr1 cr1Var) {
        }
    }

    /* compiled from: WindowInsetsCompat.java */
    /* renamed from: jp4$m */
    /* loaded from: classes.dex */
    public static final class m {
        public static int a(int i) {
            if (i != 1) {
                if (i != 2) {
                    if (i != 4) {
                        if (i != 8) {
                            if (i != 16) {
                                if (i != 32) {
                                    if (i != 64) {
                                        if (i != 128) {
                                            if (i == 256) {
                                                return 8;
                                            }
                                            throw new IllegalArgumentException("type needs to be >= FIRST and <= LAST, type=" + i);
                                        }
                                        return 7;
                                    }
                                    return 6;
                                }
                                return 5;
                            }
                            return 4;
                        }
                        return 3;
                    }
                    return 2;
                }
                return 1;
            }
            return 0;
        }

        public static int b() {
            return 7;
        }
    }

    /* compiled from: WindowInsetsCompat.java */
    /* renamed from: jp4$n */
    /* loaded from: classes.dex */
    public static final class n {
        public static int a(int i) {
            int statusBars;
            int i2 = 0;
            for (int i3 = 1; i3 <= 256; i3 <<= 1) {
                if ((i & i3) != 0) {
                    if (i3 == 1) {
                        statusBars = WindowInsets.Type.statusBars();
                    } else if (i3 == 2) {
                        statusBars = WindowInsets.Type.navigationBars();
                    } else if (i3 == 4) {
                        statusBars = WindowInsets.Type.captionBar();
                    } else if (i3 == 8) {
                        statusBars = WindowInsets.Type.ime();
                    } else if (i3 == 16) {
                        statusBars = WindowInsets.Type.systemGestures();
                    } else if (i3 == 32) {
                        statusBars = WindowInsets.Type.mandatorySystemGestures();
                    } else if (i3 == 64) {
                        statusBars = WindowInsets.Type.tappableElement();
                    } else if (i3 == 128) {
                        statusBars = WindowInsets.Type.displayCutout();
                    }
                    i2 |= statusBars;
                }
            }
            return i2;
        }
    }

    static {
        if (Build.VERSION.SDK_INT >= 30) {
            b = k.q;
        } else {
            b = l.b;
        }
    }

    public jp4(WindowInsets windowInsets) {
        int i2 = Build.VERSION.SDK_INT;
        if (i2 >= 30) {
            this.a = new k(this, windowInsets);
        } else if (i2 >= 29) {
            this.a = new j(this, windowInsets);
        } else if (i2 >= 28) {
            this.a = new i(this, windowInsets);
        } else if (i2 >= 21) {
            this.a = new h(this, windowInsets);
        } else if (i2 >= 20) {
            this.a = new g(this, windowInsets);
        } else {
            this.a = new l(this);
        }
    }

    public static cr1 p(cr1 cr1Var, int i2, int i3, int i4, int i5) {
        int max = Math.max(0, cr1Var.a - i2);
        int max2 = Math.max(0, cr1Var.b - i3);
        int max3 = Math.max(0, cr1Var.c - i4);
        int max4 = Math.max(0, cr1Var.d - i5);
        return (max == i2 && max2 == i3 && max3 == i4 && max4 == i5) ? cr1Var : cr1.b(max, max2, max3, max4);
    }

    public static jp4 x(WindowInsets windowInsets) {
        return y(windowInsets, null);
    }

    public static jp4 y(WindowInsets windowInsets, View view) {
        jp4 jp4Var = new jp4((WindowInsets) du2.e(windowInsets));
        if (view != null && ei4.V(view)) {
            jp4Var.u(ei4.L(view));
            jp4Var.d(view.getRootView());
        }
        return jp4Var;
    }

    @Deprecated
    public jp4 a() {
        return this.a.a();
    }

    @Deprecated
    public jp4 b() {
        return this.a.b();
    }

    @Deprecated
    public jp4 c() {
        return this.a.c();
    }

    public void d(View view) {
        this.a.d(view);
    }

    public vp0 e() {
        return this.a.f();
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj instanceof jp4) {
            return sl2.a(this.a, ((jp4) obj).a);
        }
        return false;
    }

    public cr1 f(int i2) {
        return this.a.g(i2);
    }

    @Deprecated
    public cr1 g() {
        return this.a.h();
    }

    @Deprecated
    public cr1 h() {
        return this.a.i();
    }

    public int hashCode() {
        l lVar = this.a;
        if (lVar == null) {
            return 0;
        }
        return lVar.hashCode();
    }

    @Deprecated
    public cr1 i() {
        return this.a.j();
    }

    @Deprecated
    public int j() {
        return this.a.k().d;
    }

    @Deprecated
    public int k() {
        return this.a.k().a;
    }

    @Deprecated
    public int l() {
        return this.a.k().c;
    }

    @Deprecated
    public int m() {
        return this.a.k().b;
    }

    @Deprecated
    public boolean n() {
        return !this.a.k().equals(cr1.e);
    }

    public jp4 o(int i2, int i3, int i4, int i5) {
        return this.a.m(i2, i3, i4, i5);
    }

    public boolean q() {
        return this.a.n();
    }

    @Deprecated
    public jp4 r(int i2, int i3, int i4, int i5) {
        return new b(this).c(cr1.b(i2, i3, i4, i5)).a();
    }

    public void s(cr1[] cr1VarArr) {
        this.a.p(cr1VarArr);
    }

    public void t(cr1 cr1Var) {
        this.a.q(cr1Var);
    }

    public void u(jp4 jp4Var) {
        this.a.r(jp4Var);
    }

    public void v(cr1 cr1Var) {
        this.a.s(cr1Var);
    }

    public WindowInsets w() {
        l lVar = this.a;
        if (lVar instanceof g) {
            return ((g) lVar).c;
        }
        return null;
    }

    /* compiled from: WindowInsetsCompat.java */
    /* renamed from: jp4$c */
    /* loaded from: classes.dex */
    public static class c extends f {
        public static Field e = null;
        public static boolean f = false;
        public static Constructor<WindowInsets> g = null;
        public static boolean h = false;
        public WindowInsets c;
        public cr1 d;

        public c() {
            this.c = h();
        }

        public static WindowInsets h() {
            if (!f) {
                try {
                    e = WindowInsets.class.getDeclaredField("CONSUMED");
                } catch (ReflectiveOperationException unused) {
                }
                f = true;
            }
            Field field = e;
            if (field != null) {
                try {
                    WindowInsets windowInsets = (WindowInsets) field.get(null);
                    if (windowInsets != null) {
                        return new WindowInsets(windowInsets);
                    }
                } catch (ReflectiveOperationException unused2) {
                }
            }
            if (!h) {
                try {
                    g = WindowInsets.class.getConstructor(Rect.class);
                } catch (ReflectiveOperationException unused3) {
                }
                h = true;
            }
            Constructor<WindowInsets> constructor = g;
            if (constructor != null) {
                try {
                    return constructor.newInstance(new Rect());
                } catch (ReflectiveOperationException unused4) {
                }
            }
            return null;
        }

        @Override // defpackage.jp4.f
        public jp4 b() {
            a();
            jp4 x = jp4.x(this.c);
            x.s(this.b);
            x.v(this.d);
            return x;
        }

        @Override // defpackage.jp4.f
        public void d(cr1 cr1Var) {
            this.d = cr1Var;
        }

        @Override // defpackage.jp4.f
        public void f(cr1 cr1Var) {
            WindowInsets windowInsets = this.c;
            if (windowInsets != null) {
                this.c = windowInsets.replaceSystemWindowInsets(cr1Var.a, cr1Var.b, cr1Var.c, cr1Var.d);
            }
        }

        public c(jp4 jp4Var) {
            super(jp4Var);
            this.c = jp4Var.w();
        }
    }

    /* compiled from: WindowInsetsCompat.java */
    /* renamed from: jp4$d */
    /* loaded from: classes.dex */
    public static class d extends f {
        public final WindowInsets.Builder c;

        public d() {
            this.c = new WindowInsets.Builder();
        }

        @Override // defpackage.jp4.f
        public jp4 b() {
            a();
            jp4 x = jp4.x(this.c.build());
            x.s(this.b);
            return x;
        }

        @Override // defpackage.jp4.f
        public void c(cr1 cr1Var) {
            this.c.setMandatorySystemGestureInsets(cr1Var.e());
        }

        @Override // defpackage.jp4.f
        public void d(cr1 cr1Var) {
            this.c.setStableInsets(cr1Var.e());
        }

        @Override // defpackage.jp4.f
        public void e(cr1 cr1Var) {
            this.c.setSystemGestureInsets(cr1Var.e());
        }

        @Override // defpackage.jp4.f
        public void f(cr1 cr1Var) {
            this.c.setSystemWindowInsets(cr1Var.e());
        }

        @Override // defpackage.jp4.f
        public void g(cr1 cr1Var) {
            this.c.setTappableElementInsets(cr1Var.e());
        }

        public d(jp4 jp4Var) {
            super(jp4Var);
            WindowInsets.Builder builder;
            WindowInsets w = jp4Var.w();
            if (w != null) {
                builder = new WindowInsets.Builder(w);
            } else {
                builder = new WindowInsets.Builder();
            }
            this.c = builder;
        }
    }

    /* compiled from: WindowInsetsCompat.java */
    /* renamed from: jp4$h */
    /* loaded from: classes.dex */
    public static class h extends g {
        public cr1 m;

        public h(jp4 jp4Var, WindowInsets windowInsets) {
            super(jp4Var, windowInsets);
            this.m = null;
        }

        @Override // defpackage.jp4.l
        public jp4 b() {
            return jp4.x(this.c.consumeStableInsets());
        }

        @Override // defpackage.jp4.l
        public jp4 c() {
            return jp4.x(this.c.consumeSystemWindowInsets());
        }

        @Override // defpackage.jp4.l
        public final cr1 i() {
            if (this.m == null) {
                this.m = cr1.b(this.c.getStableInsetLeft(), this.c.getStableInsetTop(), this.c.getStableInsetRight(), this.c.getStableInsetBottom());
            }
            return this.m;
        }

        @Override // defpackage.jp4.l
        public boolean n() {
            return this.c.isConsumed();
        }

        @Override // defpackage.jp4.l
        public void s(cr1 cr1Var) {
            this.m = cr1Var;
        }

        public h(jp4 jp4Var, h hVar) {
            super(jp4Var, hVar);
            this.m = null;
            this.m = hVar.m;
        }
    }

    /* compiled from: WindowInsetsCompat.java */
    /* renamed from: jp4$g */
    /* loaded from: classes.dex */
    public static class g extends l {
        public static boolean h = false;
        public static Method i;
        public static Class<?> j;
        public static Field k;
        public static Field l;
        public final WindowInsets c;
        public cr1[] d;
        public cr1 e;
        public jp4 f;
        public cr1 g;

        public g(jp4 jp4Var, WindowInsets windowInsets) {
            super(jp4Var);
            this.e = null;
            this.c = windowInsets;
        }

        @SuppressLint({"PrivateApi"})
        public static void x() {
            try {
                i = View.class.getDeclaredMethod("getViewRootImpl", new Class[0]);
                Class<?> cls = Class.forName("android.view.View$AttachInfo");
                j = cls;
                k = cls.getDeclaredField("mVisibleInsets");
                l = Class.forName("android.view.ViewRootImpl").getDeclaredField("mAttachInfo");
                k.setAccessible(true);
                l.setAccessible(true);
            } catch (ReflectiveOperationException e) {
                StringBuilder sb = new StringBuilder();
                sb.append("Failed to get visible insets. (Reflection error). ");
                sb.append(e.getMessage());
            }
            h = true;
        }

        @Override // defpackage.jp4.l
        public void d(View view) {
            cr1 w = w(view);
            if (w == null) {
                w = cr1.e;
            }
            q(w);
        }

        @Override // defpackage.jp4.l
        public void e(jp4 jp4Var) {
            jp4Var.u(this.f);
            jp4Var.t(this.g);
        }

        @Override // defpackage.jp4.l
        public boolean equals(Object obj) {
            if (super.equals(obj)) {
                return Objects.equals(this.g, ((g) obj).g);
            }
            return false;
        }

        @Override // defpackage.jp4.l
        public cr1 g(int i2) {
            return t(i2, false);
        }

        @Override // defpackage.jp4.l
        public final cr1 k() {
            if (this.e == null) {
                this.e = cr1.b(this.c.getSystemWindowInsetLeft(), this.c.getSystemWindowInsetTop(), this.c.getSystemWindowInsetRight(), this.c.getSystemWindowInsetBottom());
            }
            return this.e;
        }

        @Override // defpackage.jp4.l
        public jp4 m(int i2, int i3, int i4, int i5) {
            b bVar = new b(jp4.x(this.c));
            bVar.c(jp4.p(k(), i2, i3, i4, i5));
            bVar.b(jp4.p(i(), i2, i3, i4, i5));
            return bVar.a();
        }

        @Override // defpackage.jp4.l
        public boolean o() {
            return this.c.isRound();
        }

        @Override // defpackage.jp4.l
        public void p(cr1[] cr1VarArr) {
            this.d = cr1VarArr;
        }

        @Override // defpackage.jp4.l
        public void q(cr1 cr1Var) {
            this.g = cr1Var;
        }

        @Override // defpackage.jp4.l
        public void r(jp4 jp4Var) {
            this.f = jp4Var;
        }

        @SuppressLint({"WrongConstant"})
        public final cr1 t(int i2, boolean z) {
            cr1 cr1Var = cr1.e;
            for (int i3 = 1; i3 <= 256; i3 <<= 1) {
                if ((i2 & i3) != 0) {
                    cr1Var = cr1.a(cr1Var, u(i3, z));
                }
            }
            return cr1Var;
        }

        public cr1 u(int i2, boolean z) {
            cr1 h2;
            int i3;
            vp0 f;
            if (i2 == 1) {
                if (z) {
                    return cr1.b(0, Math.max(v().b, k().b), 0, 0);
                }
                return cr1.b(0, k().b, 0, 0);
            }
            if (i2 == 2) {
                if (z) {
                    cr1 v = v();
                    cr1 i4 = i();
                    return cr1.b(Math.max(v.a, i4.a), 0, Math.max(v.c, i4.c), Math.max(v.d, i4.d));
                }
                cr1 k2 = k();
                jp4 jp4Var = this.f;
                h2 = jp4Var != null ? jp4Var.h() : null;
                int i5 = k2.d;
                if (h2 != null) {
                    i5 = Math.min(i5, h2.d);
                }
                return cr1.b(k2.a, 0, k2.c, i5);
            } else if (i2 != 8) {
                if (i2 != 16) {
                    if (i2 != 32) {
                        if (i2 != 64) {
                            if (i2 != 128) {
                                return cr1.e;
                            }
                            jp4 jp4Var2 = this.f;
                            if (jp4Var2 != null) {
                                f = jp4Var2.e();
                            } else {
                                f = f();
                            }
                            if (f != null) {
                                return cr1.b(f.b(), f.d(), f.c(), f.a());
                            }
                            return cr1.e;
                        }
                        return l();
                    }
                    return h();
                }
                return j();
            } else {
                cr1[] cr1VarArr = this.d;
                h2 = cr1VarArr != null ? cr1VarArr[m.a(8)] : null;
                if (h2 != null) {
                    return h2;
                }
                cr1 k3 = k();
                cr1 v2 = v();
                int i6 = k3.d;
                if (i6 > v2.d) {
                    return cr1.b(0, 0, 0, i6);
                }
                cr1 cr1Var = this.g;
                if (cr1Var != null && !cr1Var.equals(cr1.e) && (i3 = this.g.d) > v2.d) {
                    return cr1.b(0, 0, 0, i3);
                }
                return cr1.e;
            }
        }

        public final cr1 v() {
            jp4 jp4Var = this.f;
            if (jp4Var != null) {
                return jp4Var.h();
            }
            return cr1.e;
        }

        public final cr1 w(View view) {
            if (Build.VERSION.SDK_INT < 30) {
                if (!h) {
                    x();
                }
                Method method = i;
                if (method != null && j != null && k != null) {
                    try {
                        Object invoke = method.invoke(view, new Object[0]);
                        if (invoke == null) {
                            return null;
                        }
                        Rect rect = (Rect) k.get(l.get(invoke));
                        if (rect != null) {
                            return cr1.c(rect);
                        }
                        return null;
                    } catch (ReflectiveOperationException e) {
                        StringBuilder sb = new StringBuilder();
                        sb.append("Failed to get visible insets. (Reflection error). ");
                        sb.append(e.getMessage());
                    }
                }
                return null;
            }
            throw new UnsupportedOperationException("getVisibleInsets() should not be called on API >= 30. Use WindowInsets.isVisible() instead.");
        }

        public g(jp4 jp4Var, g gVar) {
            this(jp4Var, new WindowInsets(gVar.c));
        }
    }

    /* compiled from: WindowInsetsCompat.java */
    /* renamed from: jp4$j */
    /* loaded from: classes.dex */
    public static class j extends i {
        public cr1 n;
        public cr1 o;
        public cr1 p;

        public j(jp4 jp4Var, WindowInsets windowInsets) {
            super(jp4Var, windowInsets);
            this.n = null;
            this.o = null;
            this.p = null;
        }

        @Override // defpackage.jp4.l
        public cr1 h() {
            if (this.o == null) {
                this.o = cr1.d(this.c.getMandatorySystemGestureInsets());
            }
            return this.o;
        }

        @Override // defpackage.jp4.l
        public cr1 j() {
            if (this.n == null) {
                this.n = cr1.d(this.c.getSystemGestureInsets());
            }
            return this.n;
        }

        @Override // defpackage.jp4.l
        public cr1 l() {
            if (this.p == null) {
                this.p = cr1.d(this.c.getTappableElementInsets());
            }
            return this.p;
        }

        @Override // defpackage.jp4.g, defpackage.jp4.l
        public jp4 m(int i, int i2, int i3, int i4) {
            return jp4.x(this.c.inset(i, i2, i3, i4));
        }

        @Override // defpackage.jp4.h, defpackage.jp4.l
        public void s(cr1 cr1Var) {
        }

        public j(jp4 jp4Var, j jVar) {
            super(jp4Var, jVar);
            this.n = null;
            this.o = null;
            this.p = null;
        }
    }

    /* compiled from: WindowInsetsCompat.java */
    /* renamed from: jp4$b */
    /* loaded from: classes.dex */
    public static final class b {
        public final f a;

        public b() {
            int i = Build.VERSION.SDK_INT;
            if (i >= 30) {
                this.a = new e();
            } else if (i >= 29) {
                this.a = new d();
            } else if (i >= 20) {
                this.a = new c();
            } else {
                this.a = new f();
            }
        }

        public jp4 a() {
            return this.a.b();
        }

        @Deprecated
        public b b(cr1 cr1Var) {
            this.a.d(cr1Var);
            return this;
        }

        @Deprecated
        public b c(cr1 cr1Var) {
            this.a.f(cr1Var);
            return this;
        }

        public b(jp4 jp4Var) {
            int i = Build.VERSION.SDK_INT;
            if (i >= 30) {
                this.a = new e(jp4Var);
            } else if (i >= 29) {
                this.a = new d(jp4Var);
            } else if (i >= 20) {
                this.a = new c(jp4Var);
            } else {
                this.a = new f(jp4Var);
            }
        }
    }

    public jp4(jp4 jp4Var) {
        if (jp4Var != null) {
            l lVar = jp4Var.a;
            int i2 = Build.VERSION.SDK_INT;
            if (i2 >= 30 && (lVar instanceof k)) {
                this.a = new k(this, (k) lVar);
            } else if (i2 >= 29 && (lVar instanceof j)) {
                this.a = new j(this, (j) lVar);
            } else if (i2 >= 28 && (lVar instanceof i)) {
                this.a = new i(this, (i) lVar);
            } else if (i2 >= 21 && (lVar instanceof h)) {
                this.a = new h(this, (h) lVar);
            } else if (i2 >= 20 && (lVar instanceof g)) {
                this.a = new g(this, (g) lVar);
            } else {
                this.a = new l(this);
            }
            lVar.e(this);
            return;
        }
        this.a = new l(this);
    }
}
