package defpackage;

import android.content.Context;

/* compiled from: com.google.android.gms:play-services-measurement-api@@19.0.0 */
/* renamed from: l75  reason: default package */
/* loaded from: classes2.dex */
public final /* synthetic */ class l75 implements e40 {
    public static final e40 a = new l75();

    @Override // defpackage.e40
    public final Object a(b40 b40Var) {
        kb d;
        d = lb.d((c51) b40Var.a(c51.class), (Context) b40Var.a(Context.class), (mv3) b40Var.a(mv3.class));
        return d;
    }
}
