package kotlin.collections;

/* compiled from: AbstractIterator.kt */
/* loaded from: classes2.dex */
public enum State {
    Ready,
    NotReady,
    Done,
    Failed
}
