package kotlin.jvm.internal;

/* loaded from: classes2.dex */
public class FunctionReference extends CallableReference implements vd1, sw1 {
    private final int arity;
    private final int flags;

    public FunctionReference(int i) {
        this(i, CallableReference.NO_RECEIVER, null, null, null, 0);
    }

    @Override // kotlin.jvm.internal.CallableReference
    public pw1 computeReflected() {
        return d53.a(this);
    }

    public boolean equals(Object obj) {
        if (obj == this) {
            return true;
        }
        if (obj instanceof FunctionReference) {
            FunctionReference functionReference = (FunctionReference) obj;
            return fs1.b(getOwner(), functionReference.getOwner()) && getName().equals(functionReference.getName()) && getSignature().equals(functionReference.getSignature()) && this.flags == functionReference.flags && this.arity == functionReference.arity && fs1.b(getBoundReceiver(), functionReference.getBoundReceiver());
        } else if (obj instanceof sw1) {
            return obj.equals(compute());
        } else {
            return false;
        }
    }

    @Override // defpackage.vd1
    public int getArity() {
        return this.arity;
    }

    public int hashCode() {
        return (((getOwner() == null ? 0 : getOwner().hashCode() * 31) + getName().hashCode()) * 31) + getSignature().hashCode();
    }

    @Override // defpackage.sw1
    public boolean isExternal() {
        return getReflected().isExternal();
    }

    @Override // defpackage.sw1
    public boolean isInfix() {
        return getReflected().isInfix();
    }

    @Override // defpackage.sw1
    public boolean isInline() {
        return getReflected().isInline();
    }

    @Override // defpackage.sw1
    public boolean isOperator() {
        return getReflected().isOperator();
    }

    @Override // kotlin.jvm.internal.CallableReference, defpackage.pw1
    public boolean isSuspend() {
        return getReflected().isSuspend();
    }

    public String toString() {
        pw1 compute = compute();
        if (compute != this) {
            return compute.toString();
        }
        if ("<init>".equals(getName())) {
            return "constructor (Kotlin reflection is not available)";
        }
        return "function " + getName() + " (Kotlin reflection is not available)";
    }

    public FunctionReference(int i, Object obj) {
        this(i, obj, null, null, null, 0);
    }

    @Override // kotlin.jvm.internal.CallableReference
    public sw1 getReflected() {
        return (sw1) super.getReflected();
    }

    public FunctionReference(int i, Object obj, Class cls, String str, String str2, int i2) {
        super(obj, cls, str, str2, (i2 & 1) == 1);
        this.arity = i;
        this.flags = i2 >> 1;
    }
}
