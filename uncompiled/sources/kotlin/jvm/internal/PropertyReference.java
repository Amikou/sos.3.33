package kotlin.jvm.internal;

import defpackage.yw1;

/* loaded from: classes2.dex */
public abstract class PropertyReference extends CallableReference implements yw1 {
    public PropertyReference() {
    }

    public boolean equals(Object obj) {
        if (obj == this) {
            return true;
        }
        if (obj instanceof PropertyReference) {
            PropertyReference propertyReference = (PropertyReference) obj;
            return getOwner().equals(propertyReference.getOwner()) && getName().equals(propertyReference.getName()) && getSignature().equals(propertyReference.getSignature()) && fs1.b(getBoundReceiver(), propertyReference.getBoundReceiver());
        } else if (obj instanceof yw1) {
            return obj.equals(compute());
        } else {
            return false;
        }
    }

    public abstract /* synthetic */ yw1.a<V> getGetter();

    public int hashCode() {
        return (((getOwner().hashCode() * 31) + getName().hashCode()) * 31) + getSignature().hashCode();
    }

    @Override // defpackage.yw1
    public boolean isConst() {
        return getReflected().isConst();
    }

    @Override // defpackage.yw1
    public boolean isLateinit() {
        return getReflected().isLateinit();
    }

    public String toString() {
        pw1 compute = compute();
        if (compute != this) {
            return compute.toString();
        }
        return "property " + getName() + " (Kotlin reflection is not available)";
    }

    public PropertyReference(Object obj) {
        super(obj);
    }

    @Override // kotlin.jvm.internal.CallableReference
    public yw1 getReflected() {
        return (yw1) super.getReflected();
    }

    public PropertyReference(Object obj, Class cls, String str, String str2, int i) {
        super(obj, cls, str, str2, (i & 1) == 1);
    }
}
