package kotlin.jvm.internal;

import java.io.ObjectStreamException;
import java.io.Serializable;
import java.lang.annotation.Annotation;
import java.util.List;
import java.util.Map;
import kotlin.jvm.KotlinReflectionNotSupportedError;
import kotlin.reflect.KVisibility;

/* loaded from: classes2.dex */
public abstract class CallableReference implements pw1, Serializable {
    public static final Object NO_RECEIVER = NoReceiver.a;
    private final boolean isTopLevel;
    private final String name;
    private final Class owner;
    public final Object receiver;
    private transient pw1 reflected;
    private final String signature;

    /* loaded from: classes2.dex */
    public static class NoReceiver implements Serializable {
        public static final NoReceiver a = new NoReceiver();

        private Object readResolve() throws ObjectStreamException {
            return a;
        }
    }

    public CallableReference() {
        this(NO_RECEIVER);
    }

    @Override // defpackage.pw1
    public Object call(Object... objArr) {
        return getReflected().call(objArr);
    }

    @Override // defpackage.pw1
    public Object callBy(Map map) {
        return getReflected().callBy(map);
    }

    public pw1 compute() {
        pw1 pw1Var = this.reflected;
        if (pw1Var == null) {
            pw1 computeReflected = computeReflected();
            this.reflected = computeReflected;
            return computeReflected;
        }
        return pw1Var;
    }

    public abstract pw1 computeReflected();

    @Override // defpackage.ow1
    public List<Annotation> getAnnotations() {
        return getReflected().getAnnotations();
    }

    public Object getBoundReceiver() {
        return this.receiver;
    }

    public String getName() {
        return this.name;
    }

    public rw1 getOwner() {
        Class cls = this.owner;
        if (cls == null) {
            return null;
        }
        return this.isTopLevel ? d53.c(cls) : d53.b(cls);
    }

    @Override // defpackage.pw1
    public List<Object> getParameters() {
        return getReflected().getParameters();
    }

    public pw1 getReflected() {
        pw1 compute = compute();
        if (compute != this) {
            return compute;
        }
        throw new KotlinReflectionNotSupportedError();
    }

    @Override // defpackage.pw1
    public ax1 getReturnType() {
        return getReflected().getReturnType();
    }

    public String getSignature() {
        return this.signature;
    }

    @Override // defpackage.pw1
    public List<Object> getTypeParameters() {
        return getReflected().getTypeParameters();
    }

    @Override // defpackage.pw1
    public KVisibility getVisibility() {
        return getReflected().getVisibility();
    }

    @Override // defpackage.pw1
    public boolean isAbstract() {
        return getReflected().isAbstract();
    }

    @Override // defpackage.pw1
    public boolean isFinal() {
        return getReflected().isFinal();
    }

    @Override // defpackage.pw1
    public boolean isOpen() {
        return getReflected().isOpen();
    }

    @Override // defpackage.pw1
    public boolean isSuspend() {
        return getReflected().isSuspend();
    }

    public CallableReference(Object obj) {
        this(obj, null, null, null, false);
    }

    public CallableReference(Object obj, Class cls, String str, String str2, boolean z) {
        this.receiver = obj;
        this.owner = cls;
        this.name = str;
        this.signature = str2;
        this.isTopLevel = z;
    }
}
