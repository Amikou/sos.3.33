package kotlin.jvm.internal;

import defpackage.xw1;
import defpackage.zw1;

/* loaded from: classes2.dex */
public abstract class MutablePropertyReference0 extends MutablePropertyReference implements xw1 {
    public MutablePropertyReference0() {
    }

    @Override // kotlin.jvm.internal.CallableReference
    public pw1 computeReflected() {
        return d53.d(this);
    }

    public abstract /* synthetic */ V get();

    @Override // defpackage.zw1
    public Object getDelegate() {
        return ((xw1) getReflected()).getDelegate();
    }

    @Override // defpackage.rc1
    public Object invoke() {
        return get();
    }

    public abstract /* synthetic */ void set(V v);

    public MutablePropertyReference0(Object obj) {
        super(obj);
    }

    @Override // kotlin.jvm.internal.MutablePropertyReference, kotlin.jvm.internal.PropertyReference
    public zw1.a getGetter() {
        return ((xw1) getReflected()).getGetter();
    }

    @Override // kotlin.jvm.internal.MutablePropertyReference, defpackage.xw1
    public xw1.a getSetter() {
        return ((xw1) getReflected()).getSetter();
    }

    public MutablePropertyReference0(Object obj, Class cls, String str, String str2, int i) {
        super(obj, cls, str, str2, i);
    }
}
