package kotlin.jvm.internal;

import java.io.Serializable;

/* compiled from: Lambda.kt */
/* loaded from: classes2.dex */
public abstract class Lambda<R> implements vd1<R>, Serializable {
    private final int arity;

    public Lambda(int i) {
        this.arity = i;
    }

    @Override // defpackage.vd1
    public int getArity() {
        return this.arity;
    }

    public String toString() {
        String f = d53.f(this);
        fs1.e(f, "Reflection.renderLambdaToString(this)");
        return f;
    }
}
