package kotlin.coroutines.jvm.internal;

import kotlin.coroutines.CoroutineContext;

/* compiled from: ContinuationImpl.kt */
/* loaded from: classes2.dex */
public abstract class ContinuationImpl extends BaseContinuationImpl {
    private final CoroutineContext _context;
    private transient q70<Object> intercepted;

    public ContinuationImpl(q70<Object> q70Var, CoroutineContext coroutineContext) {
        super(q70Var);
        this._context = coroutineContext;
    }

    @Override // kotlin.coroutines.jvm.internal.BaseContinuationImpl, defpackage.q70
    public CoroutineContext getContext() {
        CoroutineContext coroutineContext = this._context;
        fs1.d(coroutineContext);
        return coroutineContext;
    }

    public final q70<Object> intercepted() {
        q70<Object> q70Var = this.intercepted;
        if (q70Var == null) {
            r70 r70Var = (r70) getContext().get(r70.d);
            if (r70Var == null || (q70Var = r70Var.v(this)) == null) {
                q70Var = this;
            }
            this.intercepted = q70Var;
        }
        return q70Var;
    }

    @Override // kotlin.coroutines.jvm.internal.BaseContinuationImpl
    public void releaseIntercepted() {
        q70<?> q70Var = this.intercepted;
        if (q70Var != null && q70Var != this) {
            CoroutineContext.a aVar = getContext().get(r70.d);
            fs1.d(aVar);
            ((r70) aVar).A(q70Var);
        }
        this.intercepted = s30.a;
    }

    public ContinuationImpl(q70<Object> q70Var) {
        this(q70Var, q70Var != null ? q70Var.getContext() : null);
    }
}
