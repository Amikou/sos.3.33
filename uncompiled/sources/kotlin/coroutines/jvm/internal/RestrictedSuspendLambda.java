package kotlin.coroutines.jvm.internal;

/* compiled from: ContinuationImpl.kt */
/* loaded from: classes2.dex */
public abstract class RestrictedSuspendLambda extends RestrictedContinuationImpl implements vd1<Object> {
    private final int arity;

    public RestrictedSuspendLambda(int i, q70<Object> q70Var) {
        super(q70Var);
        this.arity = i;
    }

    @Override // defpackage.vd1
    public int getArity() {
        return this.arity;
    }

    @Override // kotlin.coroutines.jvm.internal.BaseContinuationImpl
    public String toString() {
        if (getCompletion() == null) {
            String e = d53.e(this);
            fs1.e(e, "Reflection.renderLambdaToString(this)");
            return e;
        }
        return super.toString();
    }

    public RestrictedSuspendLambda(int i) {
        this(i, null);
    }
}
