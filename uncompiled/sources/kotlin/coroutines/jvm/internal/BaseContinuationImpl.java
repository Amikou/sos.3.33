package kotlin.coroutines.jvm.internal;

import java.io.Serializable;
import kotlin.Result;
import kotlin.coroutines.CoroutineContext;

/* compiled from: ContinuationImpl.kt */
/* loaded from: classes2.dex */
public abstract class BaseContinuationImpl implements q70<Object>, e90, Serializable {
    private final q70<Object> completion;

    public BaseContinuationImpl(q70<Object> q70Var) {
        this.completion = q70Var;
    }

    public q70<te4> create(q70<?> q70Var) {
        fs1.f(q70Var, "completion");
        throw new UnsupportedOperationException("create(Continuation) has not been overridden");
    }

    @Override // defpackage.e90
    public e90 getCallerFrame() {
        q70<Object> q70Var = this.completion;
        if (!(q70Var instanceof e90)) {
            q70Var = null;
        }
        return (e90) q70Var;
    }

    public final q70<Object> getCompletion() {
        return this.completion;
    }

    @Override // defpackage.q70
    public abstract /* synthetic */ CoroutineContext getContext();

    @Override // defpackage.e90
    public StackTraceElement getStackTraceElement() {
        return af0.d(this);
    }

    public abstract Object invokeSuspend(Object obj);

    public void releaseIntercepted() {
    }

    @Override // defpackage.q70
    public final void resumeWith(Object obj) {
        Object invokeSuspend;
        BaseContinuationImpl baseContinuationImpl = this;
        while (true) {
            ef0.b(baseContinuationImpl);
            q70<Object> q70Var = baseContinuationImpl.completion;
            fs1.d(q70Var);
            try {
                invokeSuspend = baseContinuationImpl.invokeSuspend(obj);
            } catch (Throwable th) {
                Result.a aVar = Result.Companion;
                obj = Result.m52constructorimpl(o83.a(th));
            }
            if (invokeSuspend == gs1.d()) {
                return;
            }
            Result.a aVar2 = Result.Companion;
            obj = Result.m52constructorimpl(invokeSuspend);
            baseContinuationImpl.releaseIntercepted();
            if (q70Var instanceof BaseContinuationImpl) {
                baseContinuationImpl = (BaseContinuationImpl) q70Var;
            } else {
                q70Var.resumeWith(obj);
                return;
            }
        }
    }

    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("Continuation at ");
        Object stackTraceElement = getStackTraceElement();
        if (stackTraceElement == null) {
            stackTraceElement = getClass().getName();
        }
        sb.append(stackTraceElement);
        return sb.toString();
    }

    public q70<te4> create(Object obj, q70<?> q70Var) {
        fs1.f(q70Var, "completion");
        throw new UnsupportedOperationException("create(Any?;Continuation) has not been overridden");
    }
}
