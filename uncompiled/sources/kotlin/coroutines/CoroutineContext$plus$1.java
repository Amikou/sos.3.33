package kotlin.coroutines;

import defpackage.r70;
import kotlin.coroutines.CoroutineContext;
import kotlin.jvm.internal.Lambda;

/* compiled from: CoroutineContext.kt */
/* loaded from: classes2.dex */
public final class CoroutineContext$plus$1 extends Lambda implements hd1<CoroutineContext, CoroutineContext.a, CoroutineContext> {
    public static final CoroutineContext$plus$1 INSTANCE = new CoroutineContext$plus$1();

    public CoroutineContext$plus$1() {
        super(2);
    }

    @Override // defpackage.hd1
    public final CoroutineContext invoke(CoroutineContext coroutineContext, CoroutineContext.a aVar) {
        CombinedContext combinedContext;
        fs1.f(coroutineContext, "acc");
        fs1.f(aVar, "element");
        CoroutineContext minusKey = coroutineContext.minusKey(aVar.getKey());
        EmptyCoroutineContext emptyCoroutineContext = EmptyCoroutineContext.INSTANCE;
        if (minusKey == emptyCoroutineContext) {
            return aVar;
        }
        r70.b bVar = r70.d;
        r70 r70Var = (r70) minusKey.get(bVar);
        if (r70Var == null) {
            combinedContext = new CombinedContext(minusKey, aVar);
        } else {
            CoroutineContext minusKey2 = minusKey.minusKey(bVar);
            if (minusKey2 == emptyCoroutineContext) {
                return new CombinedContext(aVar, r70Var);
            }
            combinedContext = new CombinedContext(new CombinedContext(minusKey2, aVar), r70Var);
        }
        return combinedContext;
    }
}
