package kotlin.coroutines.intrinsics;

import java.util.Objects;
import kotlin.coroutines.CoroutineContext;
import kotlin.coroutines.EmptyCoroutineContext;
import kotlin.coroutines.jvm.internal.BaseContinuationImpl;
import kotlin.coroutines.jvm.internal.ContinuationImpl;
import kotlin.coroutines.jvm.internal.RestrictedContinuationImpl;

/* compiled from: IntrinsicsJvm.kt */
/* loaded from: classes2.dex */
public class IntrinsicsKt__IntrinsicsJvmKt {
    public static final <T> q70<te4> a(final tc1<? super q70<? super T>, ? extends Object> tc1Var, q70<? super T> q70Var) {
        q70<te4> q70Var2;
        fs1.f(tc1Var, "$this$createCoroutineUnintercepted");
        fs1.f(q70Var, "completion");
        final q70<?> a = ef0.a(q70Var);
        if (tc1Var instanceof BaseContinuationImpl) {
            return ((BaseContinuationImpl) tc1Var).create(a);
        }
        final CoroutineContext context = a.getContext();
        if (context == EmptyCoroutineContext.INSTANCE) {
            q70Var2 = new RestrictedContinuationImpl(a) { // from class: kotlin.coroutines.intrinsics.IntrinsicsKt__IntrinsicsJvmKt$createCoroutineUnintercepted$$inlined$createCoroutineFromSuspendFunction$IntrinsicsKt__IntrinsicsJvmKt$1
                private int label;

                @Override // kotlin.coroutines.jvm.internal.BaseContinuationImpl
                public Object invokeSuspend(Object obj) {
                    int i = this.label;
                    if (i != 0) {
                        if (i == 1) {
                            this.label = 2;
                            o83.b(obj);
                            return obj;
                        }
                        throw new IllegalStateException("This coroutine had already completed".toString());
                    }
                    this.label = 1;
                    o83.b(obj);
                    tc1 tc1Var2 = tc1Var;
                    Objects.requireNonNull(tc1Var2, "null cannot be cast to non-null type (kotlin.coroutines.Continuation<T>) -> kotlin.Any?");
                    return ((tc1) qd4.c(tc1Var2, 1)).invoke(this);
                }
            };
        } else {
            q70Var2 = new ContinuationImpl(a, context) { // from class: kotlin.coroutines.intrinsics.IntrinsicsKt__IntrinsicsJvmKt$createCoroutineUnintercepted$$inlined$createCoroutineFromSuspendFunction$IntrinsicsKt__IntrinsicsJvmKt$2
                private int label;

                @Override // kotlin.coroutines.jvm.internal.BaseContinuationImpl
                public Object invokeSuspend(Object obj) {
                    int i = this.label;
                    if (i != 0) {
                        if (i == 1) {
                            this.label = 2;
                            o83.b(obj);
                            return obj;
                        }
                        throw new IllegalStateException("This coroutine had already completed".toString());
                    }
                    this.label = 1;
                    o83.b(obj);
                    tc1 tc1Var2 = tc1Var;
                    Objects.requireNonNull(tc1Var2, "null cannot be cast to non-null type (kotlin.coroutines.Continuation<T>) -> kotlin.Any?");
                    return ((tc1) qd4.c(tc1Var2, 1)).invoke(this);
                }
            };
        }
        return q70Var2;
    }

    public static final <R, T> q70<te4> b(final hd1<? super R, ? super q70<? super T>, ? extends Object> hd1Var, final R r, q70<? super T> q70Var) {
        q70<te4> q70Var2;
        fs1.f(hd1Var, "$this$createCoroutineUnintercepted");
        fs1.f(q70Var, "completion");
        final q70<?> a = ef0.a(q70Var);
        if (hd1Var instanceof BaseContinuationImpl) {
            return ((BaseContinuationImpl) hd1Var).create(r, a);
        }
        final CoroutineContext context = a.getContext();
        if (context == EmptyCoroutineContext.INSTANCE) {
            q70Var2 = new RestrictedContinuationImpl(a) { // from class: kotlin.coroutines.intrinsics.IntrinsicsKt__IntrinsicsJvmKt$createCoroutineUnintercepted$$inlined$createCoroutineFromSuspendFunction$IntrinsicsKt__IntrinsicsJvmKt$3
                private int label;

                @Override // kotlin.coroutines.jvm.internal.BaseContinuationImpl
                public Object invokeSuspend(Object obj) {
                    int i = this.label;
                    if (i != 0) {
                        if (i == 1) {
                            this.label = 2;
                            o83.b(obj);
                            return obj;
                        }
                        throw new IllegalStateException("This coroutine had already completed".toString());
                    }
                    this.label = 1;
                    o83.b(obj);
                    hd1 hd1Var2 = hd1Var;
                    Objects.requireNonNull(hd1Var2, "null cannot be cast to non-null type (R, kotlin.coroutines.Continuation<T>) -> kotlin.Any?");
                    return ((hd1) qd4.c(hd1Var2, 2)).invoke(r, this);
                }
            };
        } else {
            q70Var2 = new ContinuationImpl(a, context) { // from class: kotlin.coroutines.intrinsics.IntrinsicsKt__IntrinsicsJvmKt$createCoroutineUnintercepted$$inlined$createCoroutineFromSuspendFunction$IntrinsicsKt__IntrinsicsJvmKt$4
                private int label;

                @Override // kotlin.coroutines.jvm.internal.BaseContinuationImpl
                public Object invokeSuspend(Object obj) {
                    int i = this.label;
                    if (i != 0) {
                        if (i == 1) {
                            this.label = 2;
                            o83.b(obj);
                            return obj;
                        }
                        throw new IllegalStateException("This coroutine had already completed".toString());
                    }
                    this.label = 1;
                    o83.b(obj);
                    hd1 hd1Var2 = hd1Var;
                    Objects.requireNonNull(hd1Var2, "null cannot be cast to non-null type (R, kotlin.coroutines.Continuation<T>) -> kotlin.Any?");
                    return ((hd1) qd4.c(hd1Var2, 2)).invoke(r, this);
                }
            };
        }
        return q70Var2;
    }

    /* JADX WARN: Multi-variable type inference failed */
    public static final <T> q70<T> c(q70<? super T> q70Var) {
        q70<T> q70Var2;
        fs1.f(q70Var, "$this$intercepted");
        ContinuationImpl continuationImpl = !(q70Var instanceof ContinuationImpl) ? null : q70Var;
        return (continuationImpl == null || (q70Var2 = (q70<T>) continuationImpl.intercepted()) == null) ? q70Var : q70Var2;
    }
}
