package kotlin.coroutines;

/* compiled from: CoroutineContext.kt */
/* loaded from: classes2.dex */
public interface CoroutineContext {

    /* compiled from: CoroutineContext.kt */
    /* loaded from: classes2.dex */
    public static final class DefaultImpls {
        public static CoroutineContext a(CoroutineContext coroutineContext, CoroutineContext coroutineContext2) {
            fs1.f(coroutineContext2, "context");
            return coroutineContext2 == EmptyCoroutineContext.INSTANCE ? coroutineContext : (CoroutineContext) coroutineContext2.fold(coroutineContext, CoroutineContext$plus$1.INSTANCE);
        }
    }

    /* compiled from: CoroutineContext.kt */
    /* loaded from: classes2.dex */
    public interface a extends CoroutineContext {

        /* compiled from: CoroutineContext.kt */
        /* renamed from: kotlin.coroutines.CoroutineContext$a$a  reason: collision with other inner class name */
        /* loaded from: classes2.dex */
        public static final class C0196a {
            public static <R> R a(a aVar, R r, hd1<? super R, ? super a, ? extends R> hd1Var) {
                fs1.f(hd1Var, "operation");
                return hd1Var.invoke(r, aVar);
            }

            /* JADX WARN: Multi-variable type inference failed */
            public static <E extends a> E b(a aVar, b<E> bVar) {
                fs1.f(bVar, "key");
                if (fs1.b(aVar.getKey(), bVar)) {
                    return aVar;
                }
                return null;
            }

            public static CoroutineContext c(a aVar, b<?> bVar) {
                fs1.f(bVar, "key");
                return fs1.b(aVar.getKey(), bVar) ? EmptyCoroutineContext.INSTANCE : aVar;
            }

            public static CoroutineContext d(a aVar, CoroutineContext coroutineContext) {
                fs1.f(coroutineContext, "context");
                return DefaultImpls.a(aVar, coroutineContext);
            }
        }

        @Override // kotlin.coroutines.CoroutineContext
        <E extends a> E get(b<E> bVar);

        b<?> getKey();
    }

    /* compiled from: CoroutineContext.kt */
    /* loaded from: classes2.dex */
    public interface b<E extends a> {
    }

    <R> R fold(R r, hd1<? super R, ? super a, ? extends R> hd1Var);

    <E extends a> E get(b<E> bVar);

    CoroutineContext minusKey(b<?> bVar);

    CoroutineContext plus(CoroutineContext coroutineContext);
}
