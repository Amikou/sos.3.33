package kotlin;

import java.io.Serializable;

/* compiled from: LazyJVM.kt */
/* loaded from: classes2.dex */
public final class SynchronizedLazyImpl<T> implements sy1<T>, Serializable {
    private volatile Object _value;
    private rc1<? extends T> initializer;
    private final Object lock;

    public SynchronizedLazyImpl(rc1<? extends T> rc1Var, Object obj) {
        fs1.f(rc1Var, "initializer");
        this.initializer = rc1Var;
        this._value = he4.a;
        this.lock = obj == null ? this : obj;
    }

    private final Object writeReplace() {
        return new InitializedLazyImpl(getValue());
    }

    @Override // defpackage.sy1
    public T getValue() {
        T t;
        T t2 = (T) this._value;
        he4 he4Var = he4.a;
        if (t2 != he4Var) {
            return t2;
        }
        synchronized (this.lock) {
            t = (T) this._value;
            if (t == he4Var) {
                rc1<? extends T> rc1Var = this.initializer;
                fs1.d(rc1Var);
                t = rc1Var.invoke();
                this._value = t;
                this.initializer = null;
            }
        }
        return t;
    }

    public boolean isInitialized() {
        return this._value != he4.a;
    }

    public String toString() {
        return isInitialized() ? String.valueOf(getValue()) : "Lazy value not initialized yet.";
    }

    public /* synthetic */ SynchronizedLazyImpl(rc1 rc1Var, Object obj, int i, qi0 qi0Var) {
        this(rc1Var, (i & 2) != 0 ? null : obj);
    }
}
