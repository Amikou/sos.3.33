package kotlin.text;

import kotlin.jvm.internal.Lambda;

/* compiled from: Regex.kt */
/* loaded from: classes2.dex */
public final class Regex$findAll$1 extends Lambda implements rc1<h42> {
    public final /* synthetic */ CharSequence $input;
    public final /* synthetic */ int $startIndex;
    public final /* synthetic */ Regex this$0;

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public Regex$findAll$1(Regex regex, CharSequence charSequence, int i) {
        super(0);
        this.this$0 = regex;
        this.$input = charSequence;
        this.$startIndex = i;
    }

    /* JADX WARN: Can't rename method to resolve collision */
    @Override // defpackage.rc1
    public final h42 invoke() {
        return this.this$0.find(this.$input, this.$startIndex);
    }
}
