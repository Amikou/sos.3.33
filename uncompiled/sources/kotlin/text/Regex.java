package kotlin.text;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collections;
import java.util.EnumSet;
import java.util.List;
import java.util.Set;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/* compiled from: Regex.kt */
/* loaded from: classes2.dex */
public final class Regex implements Serializable {
    public static final a Companion = new a(null);
    private Set<? extends RegexOption> _options;
    private final Pattern nativePattern;

    /* compiled from: Regex.kt */
    /* loaded from: classes2.dex */
    public static final class Serialized implements Serializable {
        public static final a Companion = new a(null);
        private static final long serialVersionUID = 0;
        private final int flags;
        private final String pattern;

        /* compiled from: Regex.kt */
        /* loaded from: classes2.dex */
        public static final class a {
            public a() {
            }

            public /* synthetic */ a(qi0 qi0Var) {
                this();
            }
        }

        public Serialized(String str, int i) {
            fs1.f(str, "pattern");
            this.pattern = str;
            this.flags = i;
        }

        private final Object readResolve() {
            Pattern compile = Pattern.compile(this.pattern, this.flags);
            fs1.e(compile, "Pattern.compile(pattern, flags)");
            return new Regex(compile);
        }

        public final int getFlags() {
            return this.flags;
        }

        public final String getPattern() {
            return this.pattern;
        }
    }

    /* compiled from: Regex.kt */
    /* loaded from: classes2.dex */
    public static final class a {
        public a() {
        }

        public final int b(int i) {
            return (i & 2) != 0 ? i | 64 : i;
        }

        public /* synthetic */ a(qi0 qi0Var) {
            this();
        }
    }

    public Regex(Pattern pattern) {
        fs1.f(pattern, "nativePattern");
        this.nativePattern = pattern;
    }

    public static /* synthetic */ h42 find$default(Regex regex, CharSequence charSequence, int i, int i2, Object obj) {
        if ((i2 & 2) != 0) {
            i = 0;
        }
        return regex.find(charSequence, i);
    }

    public static /* synthetic */ ol3 findAll$default(Regex regex, CharSequence charSequence, int i, int i2, Object obj) {
        if ((i2 & 2) != 0) {
            i = 0;
        }
        return regex.findAll(charSequence, i);
    }

    public static /* synthetic */ List split$default(Regex regex, CharSequence charSequence, int i, int i2, Object obj) {
        if ((i2 & 2) != 0) {
            i = 0;
        }
        return regex.split(charSequence, i);
    }

    public static /* synthetic */ ol3 splitToSequence$default(Regex regex, CharSequence charSequence, int i, int i2, Object obj) {
        if ((i2 & 2) != 0) {
            i = 0;
        }
        return regex.splitToSequence(charSequence, i);
    }

    private final Object writeReplace() {
        String pattern = this.nativePattern.pattern();
        fs1.e(pattern, "nativePattern.pattern()");
        return new Serialized(pattern, this.nativePattern.flags());
    }

    public final boolean containsMatchIn(CharSequence charSequence) {
        fs1.f(charSequence, "input");
        return this.nativePattern.matcher(charSequence).find();
    }

    public final h42 find(CharSequence charSequence, int i) {
        fs1.f(charSequence, "input");
        Matcher matcher = this.nativePattern.matcher(charSequence);
        fs1.e(matcher, "nativePattern.matcher(input)");
        return f63.a(matcher, i, charSequence);
    }

    public final ol3<h42> findAll(CharSequence charSequence, int i) {
        fs1.f(charSequence, "input");
        if (i >= 0 && i <= charSequence.length()) {
            return tl3.e(new Regex$findAll$1(this, charSequence, i), Regex$findAll$2.INSTANCE);
        }
        throw new IndexOutOfBoundsException("Start index out of bounds: " + i + ", input length: " + charSequence.length());
    }

    public final Set<RegexOption> getOptions() {
        Set set = this._options;
        if (set != null) {
            return set;
        }
        int flags = this.nativePattern.flags();
        EnumSet allOf = EnumSet.allOf(RegexOption.class);
        g20.B(allOf, new Regex$fromInt$$inlined$apply$lambda$1(flags));
        Set<RegexOption> unmodifiableSet = Collections.unmodifiableSet(allOf);
        fs1.e(unmodifiableSet, "Collections.unmodifiable…mask == it.value }\n    })");
        this._options = unmodifiableSet;
        return unmodifiableSet;
    }

    public final String getPattern() {
        String pattern = this.nativePattern.pattern();
        fs1.e(pattern, "nativePattern.pattern()");
        return pattern;
    }

    public final h42 matchAt(CharSequence charSequence, int i) {
        fs1.f(charSequence, "input");
        Matcher region = this.nativePattern.matcher(charSequence).useAnchoringBounds(false).useTransparentBounds(true).region(i, charSequence.length());
        if (region.lookingAt()) {
            fs1.e(region, "this");
            return new MatcherMatchResult(region, charSequence);
        }
        return null;
    }

    public final h42 matchEntire(CharSequence charSequence) {
        fs1.f(charSequence, "input");
        Matcher matcher = this.nativePattern.matcher(charSequence);
        fs1.e(matcher, "nativePattern.matcher(input)");
        return f63.b(matcher, charSequence);
    }

    public final boolean matches(CharSequence charSequence) {
        fs1.f(charSequence, "input");
        return this.nativePattern.matcher(charSequence).matches();
    }

    public final boolean matchesAt(CharSequence charSequence, int i) {
        fs1.f(charSequence, "input");
        return this.nativePattern.matcher(charSequence).useAnchoringBounds(false).useTransparentBounds(true).region(i, charSequence.length()).lookingAt();
    }

    public final String replace(CharSequence charSequence, String str) {
        fs1.f(charSequence, "input");
        fs1.f(str, "replacement");
        String replaceAll = this.nativePattern.matcher(charSequence).replaceAll(str);
        fs1.e(replaceAll, "nativePattern.matcher(in…).replaceAll(replacement)");
        return replaceAll;
    }

    public final String replaceFirst(CharSequence charSequence, String str) {
        fs1.f(charSequence, "input");
        fs1.f(str, "replacement");
        String replaceFirst = this.nativePattern.matcher(charSequence).replaceFirst(str);
        fs1.e(replaceFirst, "nativePattern.matcher(in…replaceFirst(replacement)");
        return replaceFirst;
    }

    public final List<String> split(CharSequence charSequence, int i) {
        fs1.f(charSequence, "input");
        StringsKt__StringsKt.r0(i);
        Matcher matcher = this.nativePattern.matcher(charSequence);
        if (i != 1 && matcher.find()) {
            ArrayList arrayList = new ArrayList(i > 0 ? u33.d(i, 10) : 10);
            int i2 = 0;
            int i3 = i - 1;
            do {
                arrayList.add(charSequence.subSequence(i2, matcher.start()).toString());
                i2 = matcher.end();
                if (i3 >= 0 && arrayList.size() == i3) {
                    break;
                }
            } while (matcher.find());
            arrayList.add(charSequence.subSequence(i2, charSequence.length()).toString());
            return arrayList;
        }
        return a20.b(charSequence.toString());
    }

    public final ol3<String> splitToSequence(CharSequence charSequence, int i) {
        fs1.f(charSequence, "input");
        StringsKt__StringsKt.r0(i);
        return rl3.b(new Regex$splitToSequence$1(this, charSequence, i, null));
    }

    public final Pattern toPattern() {
        return this.nativePattern;
    }

    public String toString() {
        String pattern = this.nativePattern.toString();
        fs1.e(pattern, "nativePattern.toString()");
        return pattern;
    }

    /* JADX WARN: Illegal instructions before constructor call */
    /*
        Code decompiled incorrectly, please refer to instructions dump.
        To view partially-correct code enable 'Show inconsistent code' option in preferences
    */
    public Regex(java.lang.String r2) {
        /*
            r1 = this;
            java.lang.String r0 = "pattern"
            defpackage.fs1.f(r2, r0)
            java.util.regex.Pattern r2 = java.util.regex.Pattern.compile(r2)
            java.lang.String r0 = "Pattern.compile(pattern)"
            defpackage.fs1.e(r2, r0)
            r1.<init>(r2)
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: kotlin.text.Regex.<init>(java.lang.String):void");
    }

    public final String replace(CharSequence charSequence, tc1<? super h42, ? extends CharSequence> tc1Var) {
        fs1.f(charSequence, "input");
        fs1.f(tc1Var, "transform");
        int i = 0;
        h42 find$default = find$default(this, charSequence, 0, 2, null);
        if (find$default != null) {
            int length = charSequence.length();
            StringBuilder sb = new StringBuilder(length);
            do {
                fs1.d(find$default);
                sb.append(charSequence, i, find$default.a().i().intValue());
                sb.append(tc1Var.invoke(find$default));
                i = find$default.a().k().intValue() + 1;
                find$default = find$default.next();
                if (i >= length) {
                    break;
                }
            } while (find$default != null);
            if (i < length) {
                sb.append(charSequence, i, length);
            }
            String sb2 = sb.toString();
            fs1.e(sb2, "sb.toString()");
            return sb2;
        }
        return charSequence.toString();
    }

    /* JADX WARN: Illegal instructions before constructor call */
    /*
        Code decompiled incorrectly, please refer to instructions dump.
        To view partially-correct code enable 'Show inconsistent code' option in preferences
    */
    public Regex(java.lang.String r2, kotlin.text.RegexOption r3) {
        /*
            r1 = this;
            java.lang.String r0 = "pattern"
            defpackage.fs1.f(r2, r0)
            java.lang.String r0 = "option"
            defpackage.fs1.f(r3, r0)
            kotlin.text.Regex$a r0 = kotlin.text.Regex.Companion
            int r3 = r3.getValue()
            int r3 = kotlin.text.Regex.a.a(r0, r3)
            java.util.regex.Pattern r2 = java.util.regex.Pattern.compile(r2, r3)
            java.lang.String r3 = "Pattern.compile(pattern,…nicodeCase(option.value))"
            defpackage.fs1.e(r2, r3)
            r1.<init>(r2)
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: kotlin.text.Regex.<init>(java.lang.String, kotlin.text.RegexOption):void");
    }

    /* JADX WARN: Illegal instructions before constructor call */
    /*
        Code decompiled incorrectly, please refer to instructions dump.
        To view partially-correct code enable 'Show inconsistent code' option in preferences
    */
    public Regex(java.lang.String r2, java.util.Set<? extends kotlin.text.RegexOption> r3) {
        /*
            r1 = this;
            java.lang.String r0 = "pattern"
            defpackage.fs1.f(r2, r0)
            java.lang.String r0 = "options"
            defpackage.fs1.f(r3, r0)
            kotlin.text.Regex$a r0 = kotlin.text.Regex.Companion
            int r3 = defpackage.f63.e(r3)
            int r3 = kotlin.text.Regex.a.a(r0, r3)
            java.util.regex.Pattern r2 = java.util.regex.Pattern.compile(r2, r3)
            java.lang.String r3 = "Pattern.compile(pattern,…odeCase(options.toInt()))"
            defpackage.fs1.e(r2, r3)
            r1.<init>(r2)
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: kotlin.text.Regex.<init>(java.lang.String, java.util.Set):void");
    }
}
