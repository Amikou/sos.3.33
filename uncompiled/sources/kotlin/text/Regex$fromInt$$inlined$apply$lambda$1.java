package kotlin.text;

import kotlin.jvm.internal.Lambda;

/* compiled from: Regex.kt */
/* loaded from: classes2.dex */
public final class Regex$fromInt$$inlined$apply$lambda$1 extends Lambda implements tc1<Enum, Boolean> {
    public final /* synthetic */ int $value$inlined;

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public Regex$fromInt$$inlined$apply$lambda$1(int i) {
        super(1);
        this.$value$inlined = i;
    }

    @Override // defpackage.tc1
    public /* bridge */ /* synthetic */ Boolean invoke(Enum r1) {
        return Boolean.valueOf(invoke2(r1));
    }

    /* renamed from: invoke  reason: avoid collision after fix types in other method */
    public final boolean invoke2(Enum r3) {
        u61 u61Var = (u61) r3;
        return (this.$value$inlined & u61Var.getMask()) == u61Var.getValue();
    }
}
