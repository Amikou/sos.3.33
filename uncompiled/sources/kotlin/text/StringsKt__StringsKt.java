package kotlin.text;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import kotlin.Pair;
import org.web3j.abi.datatypes.Utf8String;

/* compiled from: Strings.kt */
/* loaded from: classes2.dex */
public class StringsKt__StringsKt extends dv3 {
    public static /* synthetic */ boolean A0(CharSequence charSequence, CharSequence charSequence2, boolean z, int i, Object obj) {
        if ((i & 2) != 0) {
            z = false;
        }
        return z0(charSequence, charSequence2, z);
    }

    public static final String B0(CharSequence charSequence, sr1 sr1Var) {
        fs1.f(charSequence, "$this$substring");
        fs1.f(sr1Var, "range");
        return charSequence.subSequence(sr1Var.i().intValue(), sr1Var.k().intValue() + 1).toString();
    }

    public static final String C0(String str, String str2, String str3) {
        fs1.f(str, "$this$substringAfter");
        fs1.f(str2, "delimiter");
        fs1.f(str3, "missingDelimiterValue");
        int Z = Z(str, str2, 0, false, 6, null);
        if (Z == -1) {
            return str3;
        }
        String substring = str.substring(Z + str2.length(), str.length());
        fs1.e(substring, "(this as java.lang.Strin…ing(startIndex, endIndex)");
        return substring;
    }

    public static /* synthetic */ String D0(String str, String str2, String str3, int i, Object obj) {
        if ((i & 2) != 0) {
            str3 = str;
        }
        return C0(str, str2, str3);
    }

    public static final String E0(String str, char c, String str2) {
        fs1.f(str, "$this$substringAfterLast");
        fs1.f(str2, "missingDelimiterValue");
        int d0 = d0(str, c, 0, false, 6, null);
        if (d0 == -1) {
            return str2;
        }
        String substring = str.substring(d0 + 1, str.length());
        fs1.e(substring, "(this as java.lang.Strin…ing(startIndex, endIndex)");
        return substring;
    }

    public static /* synthetic */ String F0(String str, char c, String str2, int i, Object obj) {
        if ((i & 2) != 0) {
            str2 = str;
        }
        return E0(str, c, str2);
    }

    public static final String G0(String str, char c, String str2) {
        fs1.f(str, "$this$substringBefore");
        fs1.f(str2, "missingDelimiterValue");
        int Y = Y(str, c, 0, false, 6, null);
        if (Y == -1) {
            return str2;
        }
        String substring = str.substring(0, Y);
        fs1.e(substring, "(this as java.lang.Strin…ing(startIndex, endIndex)");
        return substring;
    }

    public static final String H0(String str, String str2, String str3) {
        fs1.f(str, "$this$substringBefore");
        fs1.f(str2, "delimiter");
        fs1.f(str3, "missingDelimiterValue");
        int Z = Z(str, str2, 0, false, 6, null);
        if (Z == -1) {
            return str3;
        }
        String substring = str.substring(0, Z);
        fs1.e(substring, "(this as java.lang.Strin…ing(startIndex, endIndex)");
        return substring;
    }

    public static /* synthetic */ String I0(String str, char c, String str2, int i, Object obj) {
        if ((i & 2) != 0) {
            str2 = str;
        }
        return G0(str, c, str2);
    }

    public static final boolean J(CharSequence charSequence, char c, boolean z) {
        fs1.f(charSequence, "$this$contains");
        return Y(charSequence, c, 0, z, 2, null) >= 0;
    }

    public static /* synthetic */ String J0(String str, String str2, String str3, int i, Object obj) {
        if ((i & 2) != 0) {
            str3 = str;
        }
        return H0(str, str2, str3);
    }

    public static final boolean K(CharSequence charSequence, CharSequence charSequence2, boolean z) {
        fs1.f(charSequence, "$this$contains");
        fs1.f(charSequence2, "other");
        if (charSequence2 instanceof String) {
            if (Z(charSequence, (String) charSequence2, 0, z, 2, null) >= 0) {
                return true;
            }
        } else if (X(charSequence, charSequence2, 0, charSequence.length(), z, false, 16, null) >= 0) {
            return true;
        }
        return false;
    }

    public static final CharSequence K0(CharSequence charSequence) {
        fs1.f(charSequence, "$this$trim");
        int length = charSequence.length() - 1;
        int i = 0;
        boolean z = false;
        while (i <= length) {
            boolean c = xx.c(charSequence.charAt(!z ? i : length));
            if (z) {
                if (!c) {
                    break;
                }
                length--;
            } else if (c) {
                i++;
            } else {
                z = true;
            }
        }
        return charSequence.subSequence(i, length + 1);
    }

    public static /* synthetic */ boolean L(CharSequence charSequence, char c, boolean z, int i, Object obj) {
        if ((i & 2) != 0) {
            z = false;
        }
        return J(charSequence, c, z);
    }

    public static final CharSequence L0(CharSequence charSequence) {
        fs1.f(charSequence, "$this$trimEnd");
        int length = charSequence.length();
        do {
            length--;
            if (length < 0) {
                return "";
            }
        } while (xx.c(charSequence.charAt(length)));
        return charSequence.subSequence(0, length + 1);
    }

    public static /* synthetic */ boolean M(CharSequence charSequence, CharSequence charSequence2, boolean z, int i, Object obj) {
        if ((i & 2) != 0) {
            z = false;
        }
        return K(charSequence, charSequence2, z);
    }

    public static final boolean N(CharSequence charSequence, char c, boolean z) {
        fs1.f(charSequence, "$this$endsWith");
        return charSequence.length() > 0 && yx.d(charSequence.charAt(T(charSequence)), c, z);
    }

    public static final boolean O(CharSequence charSequence, CharSequence charSequence2, boolean z) {
        fs1.f(charSequence, "$this$endsWith");
        fs1.f(charSequence2, "suffix");
        if (!z && (charSequence instanceof String) && (charSequence2 instanceof String)) {
            return dv3.s((String) charSequence, (String) charSequence2, false, 2, null);
        }
        return m0(charSequence, charSequence.length() - charSequence2.length(), charSequence2, 0, charSequence2.length(), z);
    }

    public static /* synthetic */ boolean P(CharSequence charSequence, char c, boolean z, int i, Object obj) {
        if ((i & 2) != 0) {
            z = false;
        }
        return N(charSequence, c, z);
    }

    public static /* synthetic */ boolean Q(CharSequence charSequence, CharSequence charSequence2, boolean z, int i, Object obj) {
        if ((i & 2) != 0) {
            z = false;
        }
        return O(charSequence, charSequence2, z);
    }

    public static final Pair<Integer, String> R(CharSequence charSequence, Collection<String> collection, int i, boolean z, boolean z2) {
        Object obj;
        Object obj2;
        if (!z && collection.size() == 1) {
            String str = (String) j20.c0(collection);
            int Z = !z2 ? Z(charSequence, str, i, false, 4, null) : e0(charSequence, str, i, false, 4, null);
            if (Z < 0) {
                return null;
            }
            return qc4.a(Integer.valueOf(Z), str);
        }
        qr1 sr1Var = !z2 ? new sr1(u33.b(i, 0), charSequence.length()) : u33.i(u33.d(i, T(charSequence)), 0);
        if (charSequence instanceof String) {
            int m = sr1Var.m();
            int n = sr1Var.n();
            int o = sr1Var.o();
            if (o < 0 ? m >= n : m <= n) {
                while (true) {
                    Iterator<T> it = collection.iterator();
                    while (true) {
                        if (!it.hasNext()) {
                            obj2 = null;
                            break;
                        }
                        obj2 = it.next();
                        String str2 = (String) obj2;
                        if (dv3.x(str2, 0, (String) charSequence, m, str2.length(), z)) {
                            break;
                        }
                    }
                    String str3 = (String) obj2;
                    if (str3 == null) {
                        if (m == n) {
                            break;
                        }
                        m += o;
                    } else {
                        return qc4.a(Integer.valueOf(m), str3);
                    }
                }
            }
        } else {
            int m2 = sr1Var.m();
            int n2 = sr1Var.n();
            int o2 = sr1Var.o();
            if (o2 < 0 ? m2 >= n2 : m2 <= n2) {
                while (true) {
                    Iterator<T> it2 = collection.iterator();
                    while (true) {
                        if (!it2.hasNext()) {
                            obj = null;
                            break;
                        }
                        obj = it2.next();
                        String str4 = (String) obj;
                        if (m0(str4, 0, charSequence, m2, str4.length(), z)) {
                            break;
                        }
                    }
                    String str5 = (String) obj;
                    if (str5 == null) {
                        if (m2 == n2) {
                            break;
                        }
                        m2 += o2;
                    } else {
                        return qc4.a(Integer.valueOf(m2), str5);
                    }
                }
            }
        }
        return null;
    }

    public static final sr1 S(CharSequence charSequence) {
        fs1.f(charSequence, "$this$indices");
        return new sr1(0, charSequence.length() - 1);
    }

    public static final int T(CharSequence charSequence) {
        fs1.f(charSequence, "$this$lastIndex");
        return charSequence.length() - 1;
    }

    public static final int U(CharSequence charSequence, char c, int i, boolean z) {
        fs1.f(charSequence, "$this$indexOf");
        if (!z && (charSequence instanceof String)) {
            return ((String) charSequence).indexOf(c, i);
        }
        return a0(charSequence, new char[]{c}, i, z);
    }

    public static final int V(CharSequence charSequence, String str, int i, boolean z) {
        fs1.f(charSequence, "$this$indexOf");
        fs1.f(str, Utf8String.TYPE_NAME);
        if (!z && (charSequence instanceof String)) {
            return ((String) charSequence).indexOf(str, i);
        }
        return X(charSequence, str, i, charSequence.length(), z, false, 16, null);
    }

    public static final int W(CharSequence charSequence, CharSequence charSequence2, int i, int i2, boolean z, boolean z2) {
        qr1 i3;
        if (!z2) {
            i3 = new sr1(u33.b(i, 0), u33.d(i2, charSequence.length()));
        } else {
            i3 = u33.i(u33.d(i, T(charSequence)), u33.b(i2, 0));
        }
        if ((charSequence instanceof String) && (charSequence2 instanceof String)) {
            int m = i3.m();
            int n = i3.n();
            int o = i3.o();
            if (o >= 0) {
                if (m > n) {
                    return -1;
                }
            } else if (m < n) {
                return -1;
            }
            while (!dv3.x((String) charSequence2, 0, (String) charSequence, m, charSequence2.length(), z)) {
                if (m == n) {
                    return -1;
                }
                m += o;
            }
            return m;
        }
        int m2 = i3.m();
        int n2 = i3.n();
        int o2 = i3.o();
        if (o2 >= 0) {
            if (m2 > n2) {
                return -1;
            }
        } else if (m2 < n2) {
            return -1;
        }
        while (!m0(charSequence2, 0, charSequence, m2, charSequence2.length(), z)) {
            if (m2 == n2) {
                return -1;
            }
            m2 += o2;
        }
        return m2;
    }

    public static /* synthetic */ int X(CharSequence charSequence, CharSequence charSequence2, int i, int i2, boolean z, boolean z2, int i3, Object obj) {
        if ((i3 & 16) != 0) {
            z2 = false;
        }
        return W(charSequence, charSequence2, i, i2, z, z2);
    }

    public static /* synthetic */ int Y(CharSequence charSequence, char c, int i, boolean z, int i2, Object obj) {
        if ((i2 & 2) != 0) {
            i = 0;
        }
        if ((i2 & 4) != 0) {
            z = false;
        }
        return U(charSequence, c, i, z);
    }

    public static /* synthetic */ int Z(CharSequence charSequence, String str, int i, boolean z, int i2, Object obj) {
        if ((i2 & 2) != 0) {
            i = 0;
        }
        if ((i2 & 4) != 0) {
            z = false;
        }
        return V(charSequence, str, i, z);
    }

    public static final int a0(CharSequence charSequence, char[] cArr, int i, boolean z) {
        boolean z2;
        fs1.f(charSequence, "$this$indexOfAny");
        fs1.f(cArr, "chars");
        if (!z && cArr.length == 1 && (charSequence instanceof String)) {
            return ((String) charSequence).indexOf(ai.D(cArr), i);
        }
        int b = u33.b(i, 0);
        int T = T(charSequence);
        if (b > T) {
            return -1;
        }
        while (true) {
            char charAt = charSequence.charAt(b);
            int length = cArr.length;
            int i2 = 0;
            while (true) {
                if (i2 >= length) {
                    z2 = false;
                    break;
                } else if (yx.d(cArr[i2], charAt, z)) {
                    z2 = true;
                    break;
                } else {
                    i2++;
                }
            }
            if (z2) {
                return b;
            }
            if (b == T) {
                return -1;
            }
            b++;
        }
    }

    public static final int b0(CharSequence charSequence, char c, int i, boolean z) {
        fs1.f(charSequence, "$this$lastIndexOf");
        if (!z && (charSequence instanceof String)) {
            return ((String) charSequence).lastIndexOf(c, i);
        }
        return f0(charSequence, new char[]{c}, i, z);
    }

    public static final int c0(CharSequence charSequence, String str, int i, boolean z) {
        fs1.f(charSequence, "$this$lastIndexOf");
        fs1.f(str, Utf8String.TYPE_NAME);
        if (!z && (charSequence instanceof String)) {
            return ((String) charSequence).lastIndexOf(str, i);
        }
        return W(charSequence, str, i, 0, z, true);
    }

    public static /* synthetic */ int d0(CharSequence charSequence, char c, int i, boolean z, int i2, Object obj) {
        if ((i2 & 2) != 0) {
            i = T(charSequence);
        }
        if ((i2 & 4) != 0) {
            z = false;
        }
        return b0(charSequence, c, i, z);
    }

    public static /* synthetic */ int e0(CharSequence charSequence, String str, int i, boolean z, int i2, Object obj) {
        if ((i2 & 2) != 0) {
            i = T(charSequence);
        }
        if ((i2 & 4) != 0) {
            z = false;
        }
        return c0(charSequence, str, i, z);
    }

    public static final int f0(CharSequence charSequence, char[] cArr, int i, boolean z) {
        fs1.f(charSequence, "$this$lastIndexOfAny");
        fs1.f(cArr, "chars");
        if (!z && cArr.length == 1 && (charSequence instanceof String)) {
            return ((String) charSequence).lastIndexOf(ai.D(cArr), i);
        }
        for (int d = u33.d(i, T(charSequence)); d >= 0; d--) {
            char charAt = charSequence.charAt(d);
            int length = cArr.length;
            boolean z2 = false;
            int i2 = 0;
            while (true) {
                if (i2 >= length) {
                    break;
                } else if (yx.d(cArr[i2], charAt, z)) {
                    z2 = true;
                    break;
                } else {
                    i2++;
                }
            }
            if (z2) {
                return d;
            }
        }
        return -1;
    }

    public static final ol3<String> g0(CharSequence charSequence) {
        fs1.f(charSequence, "$this$lineSequence");
        return y0(charSequence, new String[]{"\r\n", "\n", "\r"}, false, 0, 6, null);
    }

    public static final List<String> h0(CharSequence charSequence) {
        fs1.f(charSequence, "$this$lines");
        return vl3.m(g0(charSequence));
    }

    public static final ol3<sr1> i0(CharSequence charSequence, char[] cArr, int i, boolean z, int i2) {
        r0(i2);
        return new em0(charSequence, i, i2, new StringsKt__StringsKt$rangesDelimitedBy$1(cArr, z));
    }

    public static final ol3<sr1> j0(CharSequence charSequence, String[] strArr, int i, boolean z, int i2) {
        r0(i2);
        return new em0(charSequence, i, i2, new StringsKt__StringsKt$rangesDelimitedBy$2(zh.c(strArr), z));
    }

    public static /* synthetic */ ol3 k0(CharSequence charSequence, char[] cArr, int i, boolean z, int i2, int i3, Object obj) {
        if ((i3 & 2) != 0) {
            i = 0;
        }
        if ((i3 & 4) != 0) {
            z = false;
        }
        if ((i3 & 8) != 0) {
            i2 = 0;
        }
        return i0(charSequence, cArr, i, z, i2);
    }

    public static /* synthetic */ ol3 l0(CharSequence charSequence, String[] strArr, int i, boolean z, int i2, int i3, Object obj) {
        if ((i3 & 2) != 0) {
            i = 0;
        }
        if ((i3 & 4) != 0) {
            z = false;
        }
        if ((i3 & 8) != 0) {
            i2 = 0;
        }
        return j0(charSequence, strArr, i, z, i2);
    }

    public static final boolean m0(CharSequence charSequence, int i, CharSequence charSequence2, int i2, int i3, boolean z) {
        fs1.f(charSequence, "$this$regionMatchesImpl");
        fs1.f(charSequence2, "other");
        if (i2 < 0 || i < 0 || i > charSequence.length() - i3 || i2 > charSequence2.length() - i3) {
            return false;
        }
        for (int i4 = 0; i4 < i3; i4++) {
            if (!yx.d(charSequence.charAt(i + i4), charSequence2.charAt(i2 + i4), z)) {
                return false;
            }
        }
        return true;
    }

    public static final String n0(String str, CharSequence charSequence) {
        fs1.f(str, "$this$removePrefix");
        fs1.f(charSequence, "prefix");
        if (A0(str, charSequence, false, 2, null)) {
            String substring = str.substring(charSequence.length());
            fs1.e(substring, "(this as java.lang.String).substring(startIndex)");
            return substring;
        }
        return str;
    }

    public static final String o0(String str, CharSequence charSequence) {
        fs1.f(str, "$this$removeSuffix");
        fs1.f(charSequence, "suffix");
        if (Q(str, charSequence, false, 2, null)) {
            String substring = str.substring(0, str.length() - charSequence.length());
            fs1.e(substring, "(this as java.lang.Strin…ing(startIndex, endIndex)");
            return substring;
        }
        return str;
    }

    public static final String p0(String str, CharSequence charSequence) {
        fs1.f(str, "$this$removeSurrounding");
        fs1.f(charSequence, "delimiter");
        return q0(str, charSequence, charSequence);
    }

    public static final String q0(String str, CharSequence charSequence, CharSequence charSequence2) {
        fs1.f(str, "$this$removeSurrounding");
        fs1.f(charSequence, "prefix");
        fs1.f(charSequence2, "suffix");
        if (str.length() >= charSequence.length() + charSequence2.length() && A0(str, charSequence, false, 2, null) && Q(str, charSequence2, false, 2, null)) {
            String substring = str.substring(charSequence.length(), str.length() - charSequence2.length());
            fs1.e(substring, "(this as java.lang.Strin…ing(startIndex, endIndex)");
            return substring;
        }
        return str;
    }

    public static final void r0(int i) {
        if (i >= 0) {
            return;
        }
        throw new IllegalArgumentException(("Limit must be non-negative, but was " + i).toString());
    }

    public static final List<String> s0(CharSequence charSequence, char[] cArr, boolean z, int i) {
        fs1.f(charSequence, "$this$split");
        fs1.f(cArr, "delimiters");
        if (cArr.length == 1) {
            return u0(charSequence, String.valueOf(cArr[0]), z, i);
        }
        Iterable<sr1> f = vl3.f(k0(charSequence, cArr, 0, z, i, 2, null));
        ArrayList arrayList = new ArrayList(c20.q(f, 10));
        for (sr1 sr1Var : f) {
            arrayList.add(B0(charSequence, sr1Var));
        }
        return arrayList;
    }

    public static final List<String> t0(CharSequence charSequence, String[] strArr, boolean z, int i) {
        fs1.f(charSequence, "$this$split");
        fs1.f(strArr, "delimiters");
        if (strArr.length == 1) {
            String str = strArr[0];
            if (!(str.length() == 0)) {
                return u0(charSequence, str, z, i);
            }
        }
        Iterable<sr1> f = vl3.f(l0(charSequence, strArr, 0, z, i, 2, null));
        ArrayList arrayList = new ArrayList(c20.q(f, 10));
        for (sr1 sr1Var : f) {
            arrayList.add(B0(charSequence, sr1Var));
        }
        return arrayList;
    }

    public static final List<String> u0(CharSequence charSequence, String str, boolean z, int i) {
        r0(i);
        int i2 = 0;
        int V = V(charSequence, str, 0, z);
        if (V != -1 && i != 1) {
            boolean z2 = i > 0;
            ArrayList arrayList = new ArrayList(z2 ? u33.d(i, 10) : 10);
            do {
                arrayList.add(charSequence.subSequence(i2, V).toString());
                i2 = str.length() + V;
                if (z2 && arrayList.size() == i - 1) {
                    break;
                }
                V = V(charSequence, str, i2, z);
            } while (V != -1);
            arrayList.add(charSequence.subSequence(i2, charSequence.length()).toString());
            return arrayList;
        }
        return a20.b(charSequence.toString());
    }

    public static /* synthetic */ List v0(CharSequence charSequence, char[] cArr, boolean z, int i, int i2, Object obj) {
        if ((i2 & 2) != 0) {
            z = false;
        }
        if ((i2 & 4) != 0) {
            i = 0;
        }
        return s0(charSequence, cArr, z, i);
    }

    public static /* synthetic */ List w0(CharSequence charSequence, String[] strArr, boolean z, int i, int i2, Object obj) {
        if ((i2 & 2) != 0) {
            z = false;
        }
        if ((i2 & 4) != 0) {
            i = 0;
        }
        return t0(charSequence, strArr, z, i);
    }

    public static final ol3<String> x0(CharSequence charSequence, String[] strArr, boolean z, int i) {
        fs1.f(charSequence, "$this$splitToSequence");
        fs1.f(strArr, "delimiters");
        return vl3.k(l0(charSequence, strArr, 0, z, i, 2, null), new StringsKt__StringsKt$splitToSequence$1(charSequence));
    }

    public static /* synthetic */ ol3 y0(CharSequence charSequence, String[] strArr, boolean z, int i, int i2, Object obj) {
        if ((i2 & 2) != 0) {
            z = false;
        }
        if ((i2 & 4) != 0) {
            i = 0;
        }
        return x0(charSequence, strArr, z, i);
    }

    public static final boolean z0(CharSequence charSequence, CharSequence charSequence2, boolean z) {
        fs1.f(charSequence, "$this$startsWith");
        fs1.f(charSequence2, "prefix");
        if (!z && (charSequence instanceof String) && (charSequence2 instanceof String)) {
            return dv3.H((String) charSequence, (String) charSequence2, false, 2, null);
        }
        return m0(charSequence, 0, charSequence2, 0, charSequence2.length(), z);
    }
}
