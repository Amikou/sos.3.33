package kotlin.text;

import kotlin.jvm.internal.FunctionReferenceImpl;

/* compiled from: Regex.kt */
/* loaded from: classes2.dex */
public final /* synthetic */ class Regex$findAll$2 extends FunctionReferenceImpl implements tc1<h42, h42> {
    public static final Regex$findAll$2 INSTANCE = new Regex$findAll$2();

    public Regex$findAll$2() {
        super(1, h42.class, "next", "next()Lkotlin/text/MatchResult;", 0);
    }

    @Override // defpackage.tc1
    public final h42 invoke(h42 h42Var) {
        fs1.f(h42Var, "p1");
        return h42Var.next();
    }
}
