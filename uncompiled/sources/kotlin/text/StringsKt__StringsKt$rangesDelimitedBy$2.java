package kotlin.text;

import java.util.List;
import kotlin.Pair;
import kotlin.jvm.internal.Lambda;

/* compiled from: Strings.kt */
/* loaded from: classes2.dex */
public final class StringsKt__StringsKt$rangesDelimitedBy$2 extends Lambda implements hd1<CharSequence, Integer, Pair<? extends Integer, ? extends Integer>> {
    public final /* synthetic */ List $delimitersList;
    public final /* synthetic */ boolean $ignoreCase;

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public StringsKt__StringsKt$rangesDelimitedBy$2(List list, boolean z) {
        super(2);
        this.$delimitersList = list;
        this.$ignoreCase = z;
    }

    @Override // defpackage.hd1
    public /* bridge */ /* synthetic */ Pair<? extends Integer, ? extends Integer> invoke(CharSequence charSequence, Integer num) {
        return invoke(charSequence, num.intValue());
    }

    public final Pair<Integer, Integer> invoke(CharSequence charSequence, int i) {
        Pair R;
        fs1.f(charSequence, "$receiver");
        R = StringsKt__StringsKt.R(charSequence, this.$delimitersList, i, this.$ignoreCase, false);
        if (R != null) {
            return qc4.a(R.getFirst(), Integer.valueOf(((String) R.getSecond()).length()));
        }
        return null;
    }
}
