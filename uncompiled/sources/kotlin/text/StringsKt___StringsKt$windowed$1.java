package kotlin.text;

import kotlin.jvm.internal.Lambda;

/* compiled from: _Strings.kt */
/* loaded from: classes2.dex */
public final class StringsKt___StringsKt$windowed$1 extends Lambda implements tc1<CharSequence, String> {
    public static final StringsKt___StringsKt$windowed$1 INSTANCE = new StringsKt___StringsKt$windowed$1();

    public StringsKt___StringsKt$windowed$1() {
        super(1);
    }

    @Override // defpackage.tc1
    public final String invoke(CharSequence charSequence) {
        fs1.f(charSequence, "it");
        return charSequence.toString();
    }
}
