package kotlin.text;

import kotlin.jvm.internal.Lambda;

/* compiled from: Indent.kt */
/* loaded from: classes2.dex */
public final class StringsKt__IndentKt$getIndentFunction$2 extends Lambda implements tc1<String, String> {
    public final /* synthetic */ String $indent;

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public StringsKt__IndentKt$getIndentFunction$2(String str) {
        super(1);
        this.$indent = str;
    }

    @Override // defpackage.tc1
    public final String invoke(String str) {
        fs1.f(str, "line");
        return this.$indent + str;
    }
}
