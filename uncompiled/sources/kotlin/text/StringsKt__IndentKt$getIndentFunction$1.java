package kotlin.text;

import kotlin.jvm.internal.Lambda;

/* compiled from: Indent.kt */
/* loaded from: classes2.dex */
public final class StringsKt__IndentKt$getIndentFunction$1 extends Lambda implements tc1<String, String> {
    public static final StringsKt__IndentKt$getIndentFunction$1 INSTANCE = new StringsKt__IndentKt$getIndentFunction$1();

    public StringsKt__IndentKt$getIndentFunction$1() {
        super(1);
    }

    @Override // defpackage.tc1
    public final String invoke(String str) {
        fs1.f(str, "line");
        return str;
    }
}
