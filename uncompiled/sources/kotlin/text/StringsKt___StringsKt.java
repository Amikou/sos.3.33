package kotlin.text;

import java.util.ArrayList;
import java.util.List;

/* compiled from: _Strings.kt */
/* loaded from: classes2.dex */
public class StringsKt___StringsKt extends ev3 {
    public static final List<String> M0(CharSequence charSequence, int i) {
        fs1.f(charSequence, "$this$chunked");
        return Q0(charSequence, i, i, true);
    }

    public static final String N0(String str, int i) {
        fs1.f(str, "$this$drop");
        if (i >= 0) {
            String substring = str.substring(u33.d(i, str.length()));
            fs1.e(substring, "(this as java.lang.String).substring(startIndex)");
            return substring;
        }
        throw new IllegalArgumentException(("Requested character count " + i + " is less than zero.").toString());
    }

    public static final String O0(String str, int i) {
        fs1.f(str, "$this$dropLast");
        if (i >= 0) {
            return P0(str, u33.b(str.length() - i, 0));
        }
        throw new IllegalArgumentException(("Requested character count " + i + " is less than zero.").toString());
    }

    public static final String P0(String str, int i) {
        fs1.f(str, "$this$take");
        if (i >= 0) {
            String substring = str.substring(0, u33.d(i, str.length()));
            fs1.e(substring, "(this as java.lang.Strin…ing(startIndex, endIndex)");
            return substring;
        }
        throw new IllegalArgumentException(("Requested character count " + i + " is less than zero.").toString());
    }

    public static final List<String> Q0(CharSequence charSequence, int i, int i2, boolean z) {
        fs1.f(charSequence, "$this$windowed");
        return R0(charSequence, i, i2, z, StringsKt___StringsKt$windowed$1.INSTANCE);
    }

    public static final <R> List<R> R0(CharSequence charSequence, int i, int i2, boolean z, tc1<? super CharSequence, ? extends R> tc1Var) {
        fs1.f(charSequence, "$this$windowed");
        fs1.f(tc1Var, "transform");
        uq3.a(i, i2);
        int length = charSequence.length();
        int i3 = 0;
        ArrayList arrayList = new ArrayList((length / i2) + (length % i2 == 0 ? 0 : 1));
        while (i3 >= 0 && length > i3) {
            int i4 = i3 + i;
            if (i4 < 0 || i4 > length) {
                if (!z) {
                    break;
                }
                i4 = length;
            }
            arrayList.add(tc1Var.invoke(charSequence.subSequence(i3, i4)));
            i3 += i2;
        }
        return arrayList;
    }
}
