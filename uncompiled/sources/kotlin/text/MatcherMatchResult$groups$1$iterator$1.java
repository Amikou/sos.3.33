package kotlin.text;

import kotlin.jvm.internal.Lambda;

/* compiled from: Regex.kt */
/* loaded from: classes2.dex */
public final class MatcherMatchResult$groups$1$iterator$1 extends Lambda implements tc1<Integer, f42> {
    public final /* synthetic */ MatcherMatchResult$groups$1 this$0;

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public MatcherMatchResult$groups$1$iterator$1(MatcherMatchResult$groups$1 matcherMatchResult$groups$1) {
        super(1);
        this.this$0 = matcherMatchResult$groups$1;
    }

    @Override // defpackage.tc1
    public /* bridge */ /* synthetic */ f42 invoke(Integer num) {
        return invoke(num.intValue());
    }

    public final f42 invoke(int i) {
        return this.this$0.k(i);
    }
}
