package kotlin.text;

import java.util.Iterator;
import java.util.regex.MatchResult;
import kotlin.collections.AbstractCollection;

/* compiled from: Regex.kt */
/* loaded from: classes2.dex */
public final class MatcherMatchResult$groups$1 extends AbstractCollection<f42> {
    public final /* synthetic */ MatcherMatchResult a;

    public MatcherMatchResult$groups$1(MatcherMatchResult matcherMatchResult) {
        this.a = matcherMatchResult;
    }

    @Override // kotlin.collections.AbstractCollection, java.util.Collection
    public final /* bridge */ boolean contains(Object obj) {
        if (obj != null ? obj instanceof f42 : true) {
            return i((f42) obj);
        }
        return false;
    }

    @Override // kotlin.collections.AbstractCollection
    public int e() {
        MatchResult c;
        c = this.a.c();
        return c.groupCount() + 1;
    }

    public /* bridge */ boolean i(f42 f42Var) {
        return super.contains(f42Var);
    }

    @Override // kotlin.collections.AbstractCollection, java.util.Collection
    public boolean isEmpty() {
        return false;
    }

    @Override // java.util.Collection, java.lang.Iterable
    public Iterator<f42> iterator() {
        return vl3.k(j20.D(b20.h(this)), new MatcherMatchResult$groups$1$iterator$1(this)).iterator();
    }

    public f42 k(int i) {
        MatchResult c;
        sr1 i2;
        MatchResult c2;
        c = this.a.c();
        i2 = f63.i(c, i);
        if (i2.i().intValue() >= 0) {
            c2 = this.a.c();
            String group = c2.group(i);
            fs1.e(group, "matchResult.group(index)");
            return new f42(group, i2);
        }
        return null;
    }
}
