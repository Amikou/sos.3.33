package kotlin.text;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

/* compiled from: Indent.kt */
/* loaded from: classes2.dex */
public class StringsKt__IndentKt extends wu3 {
    public static final tc1<String, String> b(String str) {
        return str.length() == 0 ? StringsKt__IndentKt$getIndentFunction$1.INSTANCE : new StringsKt__IndentKt$getIndentFunction$2(str);
    }

    public static final int c(String str) {
        int length = str.length();
        int i = 0;
        while (true) {
            if (i >= length) {
                i = -1;
                break;
            } else if (!xx.c(str.charAt(i))) {
                break;
            } else {
                i++;
            }
        }
        return i == -1 ? str.length() : i;
    }

    public static final String d(String str, String str2) {
        String str3;
        String invoke;
        fs1.f(str, "$this$replaceIndent");
        fs1.f(str2, "newIndent");
        List<String> h0 = StringsKt__StringsKt.h0(str);
        ArrayList<String> arrayList = new ArrayList();
        for (Object obj : h0) {
            if (!dv3.w((String) obj)) {
                arrayList.add(obj);
            }
        }
        ArrayList arrayList2 = new ArrayList(c20.q(arrayList, 10));
        for (String str4 : arrayList) {
            arrayList2.add(Integer.valueOf(c(str4)));
        }
        Integer num = (Integer) j20.W(arrayList2);
        int i = 0;
        int intValue = num != null ? num.intValue() : 0;
        int length = str.length() + (str2.length() * h0.size());
        tc1<String, String> b = b(str2);
        int i2 = b20.i(h0);
        ArrayList arrayList3 = new ArrayList();
        for (Object obj2 : h0) {
            int i3 = i + 1;
            if (i < 0) {
                b20.p();
            }
            String str5 = (String) obj2;
            if ((i == 0 || i == i2) && dv3.w(str5)) {
                str3 = null;
            } else {
                String N0 = StringsKt___StringsKt.N0(str5, intValue);
                if (N0 != null && (invoke = b.invoke(N0)) != null) {
                    str5 = invoke;
                }
                str3 = str5;
            }
            if (str3 != null) {
                arrayList3.add(str3);
            }
            i = i3;
        }
        String sb = ((StringBuilder) j20.Q(arrayList3, new StringBuilder(length), "\n", null, null, 0, null, null, 124, null)).toString();
        fs1.e(sb, "mapIndexedNotNull { inde…\"\\n\")\n        .toString()");
        return sb;
    }

    public static final String e(String str, String str2, String str3) {
        int i;
        String invoke;
        fs1.f(str, "$this$replaceIndentByMargin");
        fs1.f(str2, "newIndent");
        fs1.f(str3, "marginPrefix");
        if (!dv3.w(str3)) {
            List<String> h0 = StringsKt__StringsKt.h0(str);
            int length = str.length() + (str2.length() * h0.size());
            tc1<String, String> b = b(str2);
            int i2 = b20.i(h0);
            ArrayList arrayList = new ArrayList();
            int i3 = 0;
            for (Object obj : h0) {
                int i4 = i3 + 1;
                if (i3 < 0) {
                    b20.p();
                }
                String str4 = (String) obj;
                String str5 = null;
                if ((i3 != 0 && i3 != i2) || !dv3.w(str4)) {
                    int length2 = str4.length();
                    int i5 = 0;
                    while (true) {
                        if (i5 >= length2) {
                            i = -1;
                            break;
                        } else if (!xx.c(str4.charAt(i5))) {
                            i = i5;
                            break;
                        } else {
                            i5++;
                        }
                    }
                    if (i != -1) {
                        int i6 = i;
                        if (dv3.G(str4, str3, i, false, 4, null)) {
                            Objects.requireNonNull(str4, "null cannot be cast to non-null type java.lang.String");
                            str5 = str4.substring(i6 + str3.length());
                            fs1.e(str5, "(this as java.lang.String).substring(startIndex)");
                        }
                    }
                    if (str5 != null && (invoke = b.invoke(str5)) != null) {
                        str4 = invoke;
                    }
                    str5 = str4;
                }
                if (str5 != null) {
                    arrayList.add(str5);
                }
                i3 = i4;
            }
            String sb = ((StringBuilder) j20.Q(arrayList, new StringBuilder(length), "\n", null, null, 0, null, null, 124, null)).toString();
            fs1.e(sb, "mapIndexedNotNull { inde…\"\\n\")\n        .toString()");
            return sb;
        }
        throw new IllegalArgumentException("marginPrefix must be non-blank string.".toString());
    }

    public static final String f(String str) {
        fs1.f(str, "$this$trimIndent");
        return d(str, "");
    }

    public static final String g(String str, String str2) {
        fs1.f(str, "$this$trimMargin");
        fs1.f(str2, "marginPrefix");
        return e(str, "", str2);
    }

    public static /* synthetic */ String h(String str, String str2, int i, Object obj) {
        if ((i & 1) != 0) {
            str2 = "|";
        }
        return g(str, str2);
    }
}
