package kotlin.text;

import kotlin.Pair;
import kotlin.jvm.internal.Lambda;

/* compiled from: Strings.kt */
/* loaded from: classes2.dex */
public final class StringsKt__StringsKt$rangesDelimitedBy$1 extends Lambda implements hd1<CharSequence, Integer, Pair<? extends Integer, ? extends Integer>> {
    public final /* synthetic */ char[] $delimiters;
    public final /* synthetic */ boolean $ignoreCase;

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public StringsKt__StringsKt$rangesDelimitedBy$1(char[] cArr, boolean z) {
        super(2);
        this.$delimiters = cArr;
        this.$ignoreCase = z;
    }

    @Override // defpackage.hd1
    public /* bridge */ /* synthetic */ Pair<? extends Integer, ? extends Integer> invoke(CharSequence charSequence, Integer num) {
        return invoke(charSequence, num.intValue());
    }

    public final Pair<Integer, Integer> invoke(CharSequence charSequence, int i) {
        fs1.f(charSequence, "$receiver");
        int a0 = StringsKt__StringsKt.a0(charSequence, this.$delimiters, i, this.$ignoreCase);
        if (a0 < 0) {
            return null;
        }
        return qc4.a(Integer.valueOf(a0), 1);
    }
}
