package kotlin.text;

import java.util.regex.MatchResult;
import java.util.regex.Matcher;

/* compiled from: Regex.kt */
/* loaded from: classes2.dex */
public final class MatcherMatchResult implements h42 {
    public final Matcher a;
    public final CharSequence b;

    public MatcherMatchResult(Matcher matcher, CharSequence charSequence) {
        fs1.f(matcher, "matcher");
        fs1.f(charSequence, "input");
        this.a = matcher;
        this.b = charSequence;
        new MatcherMatchResult$groups$1(this);
    }

    @Override // defpackage.h42
    public sr1 a() {
        sr1 h;
        h = f63.h(c());
        return h;
    }

    public final MatchResult c() {
        return this.a;
    }

    @Override // defpackage.h42
    public h42 next() {
        h42 f;
        int end = c().end() + (c().end() == c().start() ? 1 : 0);
        if (end <= this.b.length()) {
            Matcher matcher = this.a.pattern().matcher(this.b);
            fs1.e(matcher, "matcher.pattern().matcher(input)");
            f = f63.f(matcher, end, this.b);
            return f;
        }
        return null;
    }
}
