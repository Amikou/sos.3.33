package kotlin.text;

import kotlin.coroutines.jvm.internal.RestrictedSuspendLambda;
import kotlin.coroutines.jvm.internal.a;

/* compiled from: Regex.kt */
@a(c = "kotlin.text.Regex$splitToSequence$1", f = "Regex.kt", l = {243, 251, 255}, m = "invokeSuspend")
/* loaded from: classes2.dex */
public final class Regex$splitToSequence$1 extends RestrictedSuspendLambda implements hd1<ql3<? super String>, q70<? super te4>, Object> {
    public final /* synthetic */ CharSequence $input;
    public final /* synthetic */ int $limit;
    public int I$0;
    private /* synthetic */ Object L$0;
    public Object L$1;
    public int label;
    public final /* synthetic */ Regex this$0;

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public Regex$splitToSequence$1(Regex regex, CharSequence charSequence, int i, q70 q70Var) {
        super(2, q70Var);
        this.this$0 = regex;
        this.$input = charSequence;
        this.$limit = i;
    }

    @Override // kotlin.coroutines.jvm.internal.BaseContinuationImpl
    public final q70<te4> create(Object obj, q70<?> q70Var) {
        fs1.f(q70Var, "completion");
        Regex$splitToSequence$1 regex$splitToSequence$1 = new Regex$splitToSequence$1(this.this$0, this.$input, this.$limit, q70Var);
        regex$splitToSequence$1.L$0 = obj;
        return regex$splitToSequence$1;
    }

    @Override // defpackage.hd1
    public final Object invoke(ql3<? super String> ql3Var, q70<? super te4> q70Var) {
        return ((Regex$splitToSequence$1) create(ql3Var, q70Var)).invokeSuspend(te4.a);
    }

    /* JADX WARN: Removed duplicated region for block: B:20:0x0072 A[RETURN] */
    /* JADX WARN: Removed duplicated region for block: B:23:0x007d  */
    /* JADX WARN: Removed duplicated region for block: B:27:0x009e A[RETURN] */
    /* JADX WARN: Unsupported multi-entry loop pattern (BACK_EDGE: B:19:0x0070 -> B:21:0x0073). Please submit an issue!!! */
    @Override // kotlin.coroutines.jvm.internal.BaseContinuationImpl
    /*
        Code decompiled incorrectly, please refer to instructions dump.
        To view partially-correct code enable 'Show inconsistent code' option in preferences
    */
    public final java.lang.Object invokeSuspend(java.lang.Object r11) {
        /*
            r10 = this;
            java.lang.Object r0 = defpackage.gs1.d()
            int r1 = r10.label
            r2 = 0
            r3 = 3
            r4 = 2
            r5 = 1
            if (r1 == 0) goto L33
            if (r1 == r5) goto L2e
            if (r1 == r4) goto L1f
            if (r1 != r3) goto L17
            defpackage.o83.b(r11)
            goto L9f
        L17:
            java.lang.IllegalStateException r11 = new java.lang.IllegalStateException
            java.lang.String r0 = "call to 'resume' before 'invoke' with coroutine"
            r11.<init>(r0)
            throw r11
        L1f:
            int r1 = r10.I$0
            java.lang.Object r2 = r10.L$1
            java.util.regex.Matcher r2 = (java.util.regex.Matcher) r2
            java.lang.Object r6 = r10.L$0
            ql3 r6 = (defpackage.ql3) r6
            defpackage.o83.b(r11)
            r7 = r10
            goto L73
        L2e:
            defpackage.o83.b(r11)
            goto Lb1
        L33:
            defpackage.o83.b(r11)
            java.lang.Object r11 = r10.L$0
            ql3 r11 = (defpackage.ql3) r11
            kotlin.text.Regex r1 = r10.this$0
            java.util.regex.Pattern r1 = kotlin.text.Regex.access$getNativePattern$p(r1)
            java.lang.CharSequence r6 = r10.$input
            java.util.regex.Matcher r1 = r1.matcher(r6)
            int r6 = r10.$limit
            if (r6 == r5) goto La2
            boolean r6 = r1.find()
            if (r6 != 0) goto L51
            goto La2
        L51:
            r7 = r10
            r6 = r11
            r11 = r2
            r2 = r1
            r1 = r11
        L56:
            java.lang.CharSequence r8 = r7.$input
            int r9 = r2.start()
            java.lang.CharSequence r11 = r8.subSequence(r11, r9)
            java.lang.String r11 = r11.toString()
            r7.L$0 = r6
            r7.L$1 = r2
            r7.I$0 = r1
            r7.label = r4
            java.lang.Object r11 = r6.a(r11, r7)
            if (r11 != r0) goto L73
            return r0
        L73:
            int r11 = r2.end()
            int r1 = r1 + r5
            int r8 = r7.$limit
            int r8 = r8 - r5
            if (r1 == r8) goto L83
            boolean r8 = r2.find()
            if (r8 != 0) goto L56
        L83:
            java.lang.CharSequence r1 = r7.$input
            int r2 = r1.length()
            java.lang.CharSequence r11 = r1.subSequence(r11, r2)
            java.lang.String r11 = r11.toString()
            r1 = 0
            r7.L$0 = r1
            r7.L$1 = r1
            r7.label = r3
            java.lang.Object r11 = r6.a(r11, r7)
            if (r11 != r0) goto L9f
            return r0
        L9f:
            te4 r11 = defpackage.te4.a
            return r11
        La2:
            java.lang.CharSequence r1 = r10.$input
            java.lang.String r1 = r1.toString()
            r10.label = r5
            java.lang.Object r11 = r11.a(r1, r10)
            if (r11 != r0) goto Lb1
            return r0
        Lb1:
            te4 r11 = defpackage.te4.a
            return r11
        */
        throw new UnsupportedOperationException("Method not decompiled: kotlin.text.Regex$splitToSequence$1.invokeSuspend(java.lang.Object):java.lang.Object");
    }
}
