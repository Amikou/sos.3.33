package kotlin.text;

import kotlin.jvm.internal.Lambda;

/* compiled from: Strings.kt */
/* loaded from: classes2.dex */
public final class StringsKt__StringsKt$splitToSequence$1 extends Lambda implements tc1<sr1, String> {
    public final /* synthetic */ CharSequence $this_splitToSequence;

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public StringsKt__StringsKt$splitToSequence$1(CharSequence charSequence) {
        super(1);
        this.$this_splitToSequence = charSequence;
    }

    @Override // defpackage.tc1
    public final String invoke(sr1 sr1Var) {
        fs1.f(sr1Var, "it");
        return StringsKt__StringsKt.B0(this.$this_splitToSequence, sr1Var);
    }
}
