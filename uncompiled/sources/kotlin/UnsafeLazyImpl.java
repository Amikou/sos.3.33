package kotlin;

import java.io.Serializable;

/* compiled from: Lazy.kt */
/* loaded from: classes2.dex */
public final class UnsafeLazyImpl<T> implements sy1<T>, Serializable {
    private Object _value;
    private rc1<? extends T> initializer;

    public UnsafeLazyImpl(rc1<? extends T> rc1Var) {
        fs1.f(rc1Var, "initializer");
        this.initializer = rc1Var;
        this._value = he4.a;
    }

    private final Object writeReplace() {
        return new InitializedLazyImpl(getValue());
    }

    @Override // defpackage.sy1
    public T getValue() {
        if (this._value == he4.a) {
            rc1<? extends T> rc1Var = this.initializer;
            fs1.d(rc1Var);
            this._value = rc1Var.invoke();
            this.initializer = null;
        }
        return (T) this._value;
    }

    public boolean isInitialized() {
        return this._value != he4.a;
    }

    public String toString() {
        return isInitialized() ? String.valueOf(getValue()) : "Lazy value not initialized yet.";
    }
}
