package kotlin.io;

import java.io.File;

/* compiled from: Utils.kt */
/* loaded from: classes2.dex */
public final class TerminateException extends FileSystemException {
    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public TerminateException(File file) {
        super(file, null, null, 6, null);
        fs1.f(file, "file");
    }
}
