package kotlin.io;

import java.io.File;
import java.io.IOException;
import kotlin.jvm.internal.Lambda;

/* compiled from: Utils.kt */
/* loaded from: classes2.dex */
public final class FilesKt__UtilsKt$copyRecursively$2 extends Lambda implements hd1<File, IOException, te4> {
    public final /* synthetic */ hd1 $onError;

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public FilesKt__UtilsKt$copyRecursively$2(hd1 hd1Var) {
        super(2);
        this.$onError = hd1Var;
    }

    @Override // defpackage.hd1
    public /* bridge */ /* synthetic */ te4 invoke(File file, IOException iOException) {
        invoke2(file, iOException);
        return te4.a;
    }

    /* renamed from: invoke  reason: avoid collision after fix types in other method */
    public final void invoke2(File file, IOException iOException) {
        fs1.f(file, "f");
        fs1.f(iOException, "e");
        if (((OnErrorAction) this.$onError.invoke(file, iOException)) == OnErrorAction.TERMINATE) {
            throw new TerminateException(file);
        }
    }
}
