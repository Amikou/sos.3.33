package kotlin.io;

import java.io.File;
import java.io.IOException;
import kotlin.jvm.internal.Lambda;

/* compiled from: Utils.kt */
/* loaded from: classes2.dex */
public final class FilesKt__UtilsKt$copyRecursively$1 extends Lambda implements hd1 {
    public static final FilesKt__UtilsKt$copyRecursively$1 INSTANCE = new FilesKt__UtilsKt$copyRecursively$1();

    public FilesKt__UtilsKt$copyRecursively$1() {
        super(2);
    }

    @Override // defpackage.hd1
    public final Void invoke(File file, IOException iOException) {
        fs1.f(file, "<anonymous parameter 0>");
        fs1.f(iOException, "exception");
        throw iOException;
    }
}
