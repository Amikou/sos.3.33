package kotlin.io;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.util.ArrayList;
import java.util.List;

/* compiled from: Utils.kt */
/* loaded from: classes2.dex */
public class FilesKt__UtilsKt extends d41 {
    /* JADX WARN: Removed duplicated region for block: B:55:0x00b2 A[SYNTHETIC] */
    /* JADX WARN: Removed duplicated region for block: B:58:0x00a0 A[SYNTHETIC] */
    /*
        Code decompiled incorrectly, please refer to instructions dump.
        To view partially-correct code enable 'Show inconsistent code' option in preferences
    */
    public static final boolean f(java.io.File r11, java.io.File r12, boolean r13, defpackage.hd1<? super java.io.File, ? super java.io.IOException, ? extends kotlin.io.OnErrorAction> r14) {
        /*
            Method dump skipped, instructions count: 229
            To view this dump change 'Code comments level' option to 'DEBUG'
        */
        throw new UnsupportedOperationException("Method not decompiled: kotlin.io.FilesKt__UtilsKt.f(java.io.File, java.io.File, boolean, hd1):boolean");
    }

    public static /* synthetic */ boolean g(File file, File file2, boolean z, hd1 hd1Var, int i, Object obj) {
        if ((i & 2) != 0) {
            z = false;
        }
        if ((i & 4) != 0) {
            hd1Var = FilesKt__UtilsKt$copyRecursively$1.INSTANCE;
        }
        return f(file, file2, z, hd1Var);
    }

    public static final File h(File file, File file2, boolean z, int i) {
        fs1.f(file, "$this$copyTo");
        fs1.f(file2, "target");
        if (file.exists()) {
            if (file2.exists()) {
                if (z) {
                    if (!file2.delete()) {
                        throw new FileAlreadyExistsException(file, file2, "Tried to overwrite the destination, but failed to delete it.");
                    }
                } else {
                    throw new FileAlreadyExistsException(file, file2, "The destination file already exists.");
                }
            }
            if (file.isDirectory()) {
                if (!file2.mkdirs()) {
                    throw new FileSystemException(file, file2, "Failed to create target directory.");
                }
            } else {
                File parentFile = file2.getParentFile();
                if (parentFile != null) {
                    parentFile.mkdirs();
                }
                FileInputStream fileInputStream = new FileInputStream(file);
                try {
                    FileOutputStream fileOutputStream = new FileOutputStream(file2);
                    xs.a(fileInputStream, fileOutputStream, i);
                    yz.a(fileOutputStream, null);
                    yz.a(fileInputStream, null);
                } finally {
                }
            }
            return file2;
        }
        throw new NoSuchFileException(file, null, "The source file doesn't exist.", 2, null);
    }

    public static /* synthetic */ File i(File file, File file2, boolean z, int i, int i2, Object obj) {
        if ((i2 & 2) != 0) {
            z = false;
        }
        if ((i2 & 4) != 0) {
            i = 8192;
        }
        return h(file, file2, z, i);
    }

    public static final boolean j(File file) {
        fs1.f(file, "$this$deleteRecursively");
        while (true) {
            boolean z = true;
            for (File file2 : d41.d(file)) {
                if (file2.delete() || !file2.exists()) {
                    if (z) {
                        break;
                    }
                }
                z = false;
            }
            return z;
        }
    }

    public static final p31 k(p31 p31Var) {
        return new p31(p31Var.a(), l(p31Var.b()));
    }

    public static final List<File> l(List<? extends File> list) {
        ArrayList arrayList = new ArrayList(list.size());
        for (File file : list) {
            String name = file.getName();
            if (name != null) {
                int hashCode = name.hashCode();
                if (hashCode != 46) {
                    if (hashCode == 1472 && name.equals("..")) {
                        if (arrayList.isEmpty() || !(!fs1.b(((File) j20.U(arrayList)).getName(), ".."))) {
                            arrayList.add(file);
                        } else {
                            arrayList.remove(arrayList.size() - 1);
                        }
                    }
                } else if (name.equals(".")) {
                }
            }
            arrayList.add(file);
        }
        return arrayList;
    }

    public static final String m(File file, File file2) {
        fs1.f(file, "$this$toRelativeString");
        fs1.f(file2, "base");
        String n = n(file, file2);
        if (n != null) {
            return n;
        }
        throw new IllegalArgumentException("this and base files have different roots: " + file + " and " + file2 + '.');
    }

    public static final String n(File file, File file2) {
        p31 k = k(b41.b(file));
        p31 k2 = k(b41.b(file2));
        if (!fs1.b(k.a(), k2.a())) {
            return null;
        }
        int c = k2.c();
        int c2 = k.c();
        int i = 0;
        int min = Math.min(c2, c);
        while (i < min && fs1.b(k.b().get(i), k2.b().get(i))) {
            i++;
        }
        StringBuilder sb = new StringBuilder();
        int i2 = c - 1;
        if (i2 >= i) {
            while (!fs1.b(k2.b().get(i2).getName(), "..")) {
                sb.append("..");
                if (i2 != i) {
                    sb.append(File.separatorChar);
                }
                if (i2 != i) {
                    i2--;
                }
            }
            return null;
        }
        if (i < c2) {
            if (i < c) {
                sb.append(File.separatorChar);
            }
            List G = j20.G(k.b(), i);
            String str = File.separator;
            fs1.e(str, "File.separator");
            j20.P(G, sb, (r14 & 2) != 0 ? ", " : str, (r14 & 4) != 0 ? "" : null, (r14 & 8) == 0 ? null : "", (r14 & 16) != 0 ? -1 : 0, (r14 & 32) != 0 ? "..." : null, (r14 & 64) != 0 ? null : null);
        }
        return sb.toString();
    }
}
