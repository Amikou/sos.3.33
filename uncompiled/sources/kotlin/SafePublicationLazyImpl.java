package kotlin;

import java.io.Serializable;
import java.util.concurrent.atomic.AtomicReferenceFieldUpdater;

/* compiled from: LazyJVM.kt */
/* loaded from: classes2.dex */
public final class SafePublicationLazyImpl<T> implements sy1<T>, Serializable {
    public static final a Companion = new a(null);
    public static final AtomicReferenceFieldUpdater<SafePublicationLazyImpl<?>, Object> a = AtomicReferenceFieldUpdater.newUpdater(SafePublicationLazyImpl.class, Object.class, "_value");
    private volatile Object _value;

    /* renamed from: final  reason: not valid java name */
    private final Object f0final;
    private volatile rc1<? extends T> initializer;

    /* compiled from: LazyJVM.kt */
    /* loaded from: classes2.dex */
    public static final class a {
        public a() {
        }

        public /* synthetic */ a(qi0 qi0Var) {
            this();
        }
    }

    public SafePublicationLazyImpl(rc1<? extends T> rc1Var) {
        fs1.f(rc1Var, "initializer");
        this.initializer = rc1Var;
        he4 he4Var = he4.a;
        this._value = he4Var;
        this.f0final = he4Var;
    }

    private final Object writeReplace() {
        return new InitializedLazyImpl(getValue());
    }

    @Override // defpackage.sy1
    public T getValue() {
        T t = (T) this._value;
        he4 he4Var = he4.a;
        if (t != he4Var) {
            return t;
        }
        rc1<? extends T> rc1Var = this.initializer;
        if (rc1Var != null) {
            T invoke = rc1Var.invoke();
            if (a.compareAndSet(this, he4Var, invoke)) {
                this.initializer = null;
                return invoke;
            }
        }
        return (T) this._value;
    }

    public boolean isInitialized() {
        return this._value != he4.a;
    }

    public String toString() {
        return isInitialized() ? String.valueOf(getValue()) : "Lazy value not initialized yet.";
    }
}
