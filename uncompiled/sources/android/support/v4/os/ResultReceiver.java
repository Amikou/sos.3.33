package android.support.v4.os;

import android.annotation.SuppressLint;
import android.os.Bundle;
import android.os.Handler;
import android.os.Parcel;
import android.os.Parcelable;
import android.os.RemoteException;
import android.support.v4.os.a;

@SuppressLint({"BanParcelableUsage"})
/* loaded from: classes.dex */
public class ResultReceiver implements Parcelable {
    public static final Parcelable.Creator<ResultReceiver> CREATOR = new a();
    public final boolean a = false;
    public final Handler f0 = null;
    public android.support.v4.os.a g0;

    /* loaded from: classes.dex */
    public class a implements Parcelable.Creator<ResultReceiver> {
        @Override // android.os.Parcelable.Creator
        /* renamed from: a */
        public ResultReceiver createFromParcel(Parcel parcel) {
            return new ResultReceiver(parcel);
        }

        @Override // android.os.Parcelable.Creator
        /* renamed from: b */
        public ResultReceiver[] newArray(int i) {
            return new ResultReceiver[i];
        }
    }

    /* loaded from: classes.dex */
    public class b extends a.AbstractBinderC0005a {
        public b() {
        }

        @Override // android.support.v4.os.a
        public void D1(int i, Bundle bundle) {
            ResultReceiver resultReceiver = ResultReceiver.this;
            Handler handler = resultReceiver.f0;
            if (handler != null) {
                handler.post(new c(i, bundle));
            } else {
                resultReceiver.a(i, bundle);
            }
        }
    }

    /* loaded from: classes.dex */
    public class c implements Runnable {
        public final int a;
        public final Bundle f0;

        public c(int i, Bundle bundle) {
            this.a = i;
            this.f0 = bundle;
        }

        @Override // java.lang.Runnable
        public void run() {
            ResultReceiver.this.a(this.a, this.f0);
        }
    }

    public ResultReceiver(Parcel parcel) {
        this.g0 = a.AbstractBinderC0005a.b(parcel.readStrongBinder());
    }

    public void a(int i, Bundle bundle) {
    }

    public void b(int i, Bundle bundle) {
        if (this.a) {
            Handler handler = this.f0;
            if (handler != null) {
                handler.post(new c(i, bundle));
                return;
            } else {
                a(i, bundle);
                return;
            }
        }
        android.support.v4.os.a aVar = this.g0;
        if (aVar != null) {
            try {
                aVar.D1(i, bundle);
            } catch (RemoteException unused) {
            }
        }
    }

    @Override // android.os.Parcelable
    public int describeContents() {
        return 0;
    }

    @Override // android.os.Parcelable
    public void writeToParcel(Parcel parcel, int i) {
        synchronized (this) {
            if (this.g0 == null) {
                this.g0 = new b();
            }
            parcel.writeStrongBinder(this.g0.asBinder());
        }
    }
}
