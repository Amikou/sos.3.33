package android.support.v4.media.session;

import android.content.Context;
import android.media.MediaMetadata;
import android.media.session.MediaController;
import android.media.session.MediaSession;
import android.media.session.PlaybackState;
import android.os.Build;
import android.os.Bundle;
import android.os.IBinder;
import android.os.RemoteException;
import android.os.ResultReceiver;
import android.support.v4.media.MediaMetadataCompat;
import android.support.v4.media.session.MediaSessionCompat;
import android.support.v4.media.session.a;
import android.support.v4.media.session.b;
import android.view.KeyEvent;
import androidx.media.AudioAttributesCompat;
import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.concurrent.ConcurrentHashMap;

/* loaded from: classes.dex */
public final class MediaControllerCompat {
    public final b a;

    /* loaded from: classes.dex */
    public static class MediaControllerImplApi21 implements b {
        public final MediaController a;
        public final Object b = new Object();
        public final List<a> c = new ArrayList();
        public HashMap<a, a> d = new HashMap<>();
        public final MediaSessionCompat.Token e;

        /* loaded from: classes.dex */
        public static class ExtraBinderRequestResultReceiver extends ResultReceiver {
            public WeakReference<MediaControllerImplApi21> a;

            public ExtraBinderRequestResultReceiver(MediaControllerImplApi21 mediaControllerImplApi21) {
                super(null);
                this.a = new WeakReference<>(mediaControllerImplApi21);
            }

            @Override // android.os.ResultReceiver
            public void onReceiveResult(int i, Bundle bundle) {
                MediaControllerImplApi21 mediaControllerImplApi21 = this.a.get();
                if (mediaControllerImplApi21 == null || bundle == null) {
                    return;
                }
                synchronized (mediaControllerImplApi21.b) {
                    mediaControllerImplApi21.e.e(b.a.b(bs.a(bundle, "android.support.v4.media.session.EXTRA_BINDER")));
                    mediaControllerImplApi21.e.f(lp2.b(bundle, "android.support.v4.media.session.SESSION_TOKEN2"));
                    mediaControllerImplApi21.b();
                }
            }
        }

        /* loaded from: classes.dex */
        public static class a extends a.b {
            public a(a aVar) {
                super(aVar);
            }

            @Override // android.support.v4.media.session.MediaControllerCompat.a.b, android.support.v4.media.session.a
            public void E(Bundle bundle) throws RemoteException {
                throw new AssertionError();
            }

            @Override // android.support.v4.media.session.MediaControllerCompat.a.b, android.support.v4.media.session.a
            public void E1(ParcelableVolumeInfo parcelableVolumeInfo) throws RemoteException {
                throw new AssertionError();
            }

            @Override // android.support.v4.media.session.MediaControllerCompat.a.b, android.support.v4.media.session.a
            public void H(List<MediaSessionCompat.QueueItem> list) throws RemoteException {
                throw new AssertionError();
            }

            @Override // android.support.v4.media.session.MediaControllerCompat.a.b, android.support.v4.media.session.a
            public void q0(CharSequence charSequence) throws RemoteException {
                throw new AssertionError();
            }

            @Override // android.support.v4.media.session.MediaControllerCompat.a.b, android.support.v4.media.session.a
            public void w0() throws RemoteException {
                throw new AssertionError();
            }

            @Override // android.support.v4.media.session.MediaControllerCompat.a.b, android.support.v4.media.session.a
            public void x0(MediaMetadataCompat mediaMetadataCompat) throws RemoteException {
                throw new AssertionError();
            }
        }

        public MediaControllerImplApi21(Context context, MediaSessionCompat.Token token) {
            this.e = token;
            this.a = new MediaController(context, (MediaSession.Token) token.d());
            if (token.c() == null) {
                c();
            }
        }

        @Override // android.support.v4.media.session.MediaControllerCompat.b
        public boolean a(KeyEvent keyEvent) {
            return this.a.dispatchMediaButtonEvent(keyEvent);
        }

        public void b() {
            if (this.e.c() == null) {
                return;
            }
            for (a aVar : this.c) {
                a aVar2 = new a(aVar);
                this.d.put(aVar, aVar2);
                aVar.a = aVar2;
                try {
                    this.e.c().k(aVar2);
                    aVar.i(13, null, null);
                } catch (RemoteException unused) {
                }
            }
            this.c.clear();
        }

        public final void c() {
            d("android.support.v4.media.session.command.GET_EXTRA_BINDER", null, new ExtraBinderRequestResultReceiver(this));
        }

        public void d(String str, Bundle bundle, ResultReceiver resultReceiver) {
            this.a.sendCommand(str, bundle, resultReceiver);
        }
    }

    /* loaded from: classes.dex */
    public static abstract class a implements IBinder.DeathRecipient {
        public android.support.v4.media.session.a a;

        /* renamed from: android.support.v4.media.session.MediaControllerCompat$a$a  reason: collision with other inner class name */
        /* loaded from: classes.dex */
        public static class C0002a extends MediaController.Callback {
            public final WeakReference<a> a;

            public C0002a(a aVar) {
                this.a = new WeakReference<>(aVar);
            }

            @Override // android.media.session.MediaController.Callback
            public void onAudioInfoChanged(MediaController.PlaybackInfo playbackInfo) {
                a aVar = this.a.get();
                if (aVar != null) {
                    aVar.a(new d(playbackInfo.getPlaybackType(), AudioAttributesCompat.c(playbackInfo.getAudioAttributes()), playbackInfo.getVolumeControl(), playbackInfo.getMaxVolume(), playbackInfo.getCurrentVolume()));
                }
            }

            @Override // android.media.session.MediaController.Callback
            public void onExtrasChanged(Bundle bundle) {
                MediaSessionCompat.a(bundle);
                a aVar = this.a.get();
                if (aVar != null) {
                    aVar.b(bundle);
                }
            }

            @Override // android.media.session.MediaController.Callback
            public void onMetadataChanged(MediaMetadata mediaMetadata) {
                a aVar = this.a.get();
                if (aVar != null) {
                    aVar.c(MediaMetadataCompat.a(mediaMetadata));
                }
            }

            @Override // android.media.session.MediaController.Callback
            public void onPlaybackStateChanged(PlaybackState playbackState) {
                a aVar = this.a.get();
                if (aVar == null || aVar.a != null) {
                    return;
                }
                aVar.d(PlaybackStateCompat.a(playbackState));
            }

            @Override // android.media.session.MediaController.Callback
            public void onQueueChanged(List<MediaSession.QueueItem> list) {
                a aVar = this.a.get();
                if (aVar != null) {
                    aVar.e(MediaSessionCompat.QueueItem.b(list));
                }
            }

            @Override // android.media.session.MediaController.Callback
            public void onQueueTitleChanged(CharSequence charSequence) {
                a aVar = this.a.get();
                if (aVar != null) {
                    aVar.f(charSequence);
                }
            }

            @Override // android.media.session.MediaController.Callback
            public void onSessionDestroyed() {
                a aVar = this.a.get();
                if (aVar != null) {
                    aVar.g();
                }
            }

            @Override // android.media.session.MediaController.Callback
            public void onSessionEvent(String str, Bundle bundle) {
                MediaSessionCompat.a(bundle);
                a aVar = this.a.get();
                if (aVar != null) {
                    if (aVar.a == null || Build.VERSION.SDK_INT >= 23) {
                        aVar.h(str, bundle);
                    }
                }
            }
        }

        /* loaded from: classes.dex */
        public static class b extends a.AbstractBinderC0003a {
            public final WeakReference<a> a;

            public b(a aVar) {
                this.a = new WeakReference<>(aVar);
            }

            @Override // android.support.v4.media.session.a
            public void A1(PlaybackStateCompat playbackStateCompat) throws RemoteException {
                a aVar = this.a.get();
                if (aVar != null) {
                    aVar.i(2, playbackStateCompat, null);
                }
            }

            @Override // android.support.v4.media.session.a
            public void D() throws RemoteException {
                a aVar = this.a.get();
                if (aVar != null) {
                    aVar.i(13, null, null);
                }
            }

            public void E(Bundle bundle) throws RemoteException {
                a aVar = this.a.get();
                if (aVar != null) {
                    aVar.i(7, bundle, null);
                }
            }

            public void E1(ParcelableVolumeInfo parcelableVolumeInfo) throws RemoteException {
                a aVar = this.a.get();
                if (aVar != null) {
                    aVar.i(4, parcelableVolumeInfo != null ? new d(parcelableVolumeInfo.a, parcelableVolumeInfo.f0, parcelableVolumeInfo.g0, parcelableVolumeInfo.h0, parcelableVolumeInfo.i0) : null, null);
                }
            }

            public void H(List<MediaSessionCompat.QueueItem> list) throws RemoteException {
                a aVar = this.a.get();
                if (aVar != null) {
                    aVar.i(5, list, null);
                }
            }

            @Override // android.support.v4.media.session.a
            public void S0(int i) throws RemoteException {
                a aVar = this.a.get();
                if (aVar != null) {
                    aVar.i(12, Integer.valueOf(i), null);
                }
            }

            @Override // android.support.v4.media.session.a
            public void a0(boolean z) throws RemoteException {
                a aVar = this.a.get();
                if (aVar != null) {
                    aVar.i(11, Boolean.valueOf(z), null);
                }
            }

            @Override // android.support.v4.media.session.a
            public void d(String str, Bundle bundle) throws RemoteException {
                a aVar = this.a.get();
                if (aVar != null) {
                    aVar.i(1, str, bundle);
                }
            }

            @Override // android.support.v4.media.session.a
            public void o0(boolean z) throws RemoteException {
            }

            public void q0(CharSequence charSequence) throws RemoteException {
                a aVar = this.a.get();
                if (aVar != null) {
                    aVar.i(6, charSequence, null);
                }
            }

            @Override // android.support.v4.media.session.a
            public void s(int i) throws RemoteException {
                a aVar = this.a.get();
                if (aVar != null) {
                    aVar.i(9, Integer.valueOf(i), null);
                }
            }

            public void w0() throws RemoteException {
                a aVar = this.a.get();
                if (aVar != null) {
                    aVar.i(8, null, null);
                }
            }

            public void x0(MediaMetadataCompat mediaMetadataCompat) throws RemoteException {
                a aVar = this.a.get();
                if (aVar != null) {
                    aVar.i(3, mediaMetadataCompat, null);
                }
            }
        }

        public a() {
            if (Build.VERSION.SDK_INT >= 21) {
                new C0002a(this);
            } else {
                this.a = new b(this);
            }
        }

        public void a(d dVar) {
        }

        public void b(Bundle bundle) {
        }

        @Override // android.os.IBinder.DeathRecipient
        public void binderDied() {
            i(8, null, null);
        }

        public void c(MediaMetadataCompat mediaMetadataCompat) {
        }

        public void d(PlaybackStateCompat playbackStateCompat) {
        }

        public void e(List<MediaSessionCompat.QueueItem> list) {
        }

        public void f(CharSequence charSequence) {
        }

        public void g() {
        }

        public void h(String str, Bundle bundle) {
        }

        public void i(int i, Object obj, Bundle bundle) {
        }
    }

    /* loaded from: classes.dex */
    public interface b {
        boolean a(KeyEvent keyEvent);
    }

    /* loaded from: classes.dex */
    public static class c implements b {
        public android.support.v4.media.session.b a;

        public c(MediaSessionCompat.Token token) {
            this.a = b.a.b((IBinder) token.d());
        }

        @Override // android.support.v4.media.session.MediaControllerCompat.b
        public boolean a(KeyEvent keyEvent) {
            if (keyEvent != null) {
                try {
                    this.a.n0(keyEvent);
                    return false;
                } catch (RemoteException unused) {
                    return false;
                }
            }
            throw new IllegalArgumentException("event may not be null.");
        }
    }

    /* loaded from: classes.dex */
    public static final class d {
        public d(int i, int i2, int i3, int i4, int i5) {
            this(i, new AudioAttributesCompat.a().b(i2).a(), i3, i4, i5);
        }

        public d(int i, AudioAttributesCompat audioAttributesCompat, int i2, int i3, int i4) {
        }
    }

    public MediaControllerCompat(Context context, MediaSessionCompat.Token token) {
        new ConcurrentHashMap();
        if (token != null) {
            if (Build.VERSION.SDK_INT >= 21) {
                this.a = new MediaControllerImplApi21(context, token);
                return;
            } else {
                this.a = new c(token);
                return;
            }
        }
        throw new IllegalArgumentException("sessionToken must not be null");
    }

    public boolean a(KeyEvent keyEvent) {
        if (keyEvent != null) {
            return this.a.a(keyEvent);
        }
        throw new IllegalArgumentException("KeyEvent may not be null");
    }
}
