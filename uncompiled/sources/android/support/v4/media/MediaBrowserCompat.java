package android.support.v4.media;

import android.annotation.SuppressLint;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.media.MediaDescription;
import android.media.browse.MediaBrowser;
import android.os.BadParcelableException;
import android.os.Binder;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.IBinder;
import android.os.Message;
import android.os.Messenger;
import android.os.Parcel;
import android.os.Parcelable;
import android.os.Process;
import android.os.RemoteException;
import android.support.v4.media.session.MediaSessionCompat;
import android.support.v4.media.session.b;
import android.support.v4.os.ResultReceiver;
import android.text.TextUtils;
import android.util.Log;
import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Objects;

/* loaded from: classes.dex */
public final class MediaBrowserCompat {
    public static final boolean b = Log.isLoggable("MediaBrowserCompat", 3);
    public final d a;

    /* loaded from: classes.dex */
    public static class CustomActionResultReceiver extends ResultReceiver {
        @Override // android.support.v4.os.ResultReceiver
        public void a(int i, Bundle bundle) {
        }
    }

    /* loaded from: classes.dex */
    public static class ItemReceiver extends ResultReceiver {
        @Override // android.support.v4.os.ResultReceiver
        public void a(int i, Bundle bundle) {
            if (bundle != null) {
                bundle = MediaSessionCompat.b(bundle);
            }
            if (i == 0 && bundle != null && bundle.containsKey("media_item")) {
                Parcelable parcelable = bundle.getParcelable("media_item");
                if (parcelable != null && !(parcelable instanceof MediaItem)) {
                    throw null;
                }
                MediaItem mediaItem = (MediaItem) parcelable;
                throw null;
            }
            throw null;
        }
    }

    /* loaded from: classes.dex */
    public static class SearchResultReceiver extends ResultReceiver {
        @Override // android.support.v4.os.ResultReceiver
        public void a(int i, Bundle bundle) {
            if (bundle != null) {
                bundle = MediaSessionCompat.b(bundle);
            }
            if (i == 0 && bundle != null && bundle.containsKey("search_results")) {
                Parcelable[] parcelableArray = bundle.getParcelableArray("search_results");
                Objects.requireNonNull(parcelableArray);
                ArrayList arrayList = new ArrayList();
                for (Parcelable parcelable : parcelableArray) {
                    arrayList.add((MediaItem) parcelable);
                }
                throw null;
            }
            throw null;
        }
    }

    /* loaded from: classes.dex */
    public static class a {
        public static MediaDescription a(MediaBrowser.MediaItem mediaItem) {
            return mediaItem.getDescription();
        }

        public static int b(MediaBrowser.MediaItem mediaItem) {
            return mediaItem.getFlags();
        }
    }

    /* loaded from: classes.dex */
    public static class b extends Handler {
        public final WeakReference<i> a;
        public WeakReference<Messenger> b;

        public b(i iVar) {
            this.a = new WeakReference<>(iVar);
        }

        public void a(Messenger messenger) {
            this.b = new WeakReference<>(messenger);
        }

        @Override // android.os.Handler
        public void handleMessage(Message message) {
            WeakReference<Messenger> weakReference = this.b;
            if (weakReference == null || weakReference.get() == null || this.a.get() == null) {
                return;
            }
            Bundle data = message.getData();
            MediaSessionCompat.a(data);
            i iVar = this.a.get();
            Messenger messenger = this.b.get();
            try {
                int i = message.what;
                if (i == 1) {
                    Bundle bundle = data.getBundle("data_root_hints");
                    MediaSessionCompat.a(bundle);
                    iVar.a(messenger, data.getString("data_media_item_id"), (MediaSessionCompat.Token) data.getParcelable("data_media_session_token"), bundle);
                } else if (i == 2) {
                    iVar.g(messenger);
                } else if (i != 3) {
                    StringBuilder sb = new StringBuilder();
                    sb.append("Unhandled message: ");
                    sb.append(message);
                    sb.append("\n  Client version: ");
                    sb.append(1);
                    sb.append("\n  Service version: ");
                    sb.append(message.arg1);
                } else {
                    Bundle bundle2 = data.getBundle("data_options");
                    MediaSessionCompat.a(bundle2);
                    Bundle bundle3 = data.getBundle("data_notify_children_changed_options");
                    MediaSessionCompat.a(bundle3);
                    iVar.d(messenger, data.getString("data_media_item_id"), data.getParcelableArrayList("data_media_item_list"), bundle2, bundle3);
                }
            } catch (BadParcelableException unused) {
                if (message.what == 1) {
                    iVar.g(messenger);
                }
            }
        }
    }

    /* loaded from: classes.dex */
    public static class c {
        public final MediaBrowser.ConnectionCallback a;
        public b b;

        /* loaded from: classes.dex */
        public class a extends MediaBrowser.ConnectionCallback {
            public a() {
            }

            @Override // android.media.browse.MediaBrowser.ConnectionCallback
            public void onConnected() {
                b bVar = c.this.b;
                if (bVar != null) {
                    bVar.h();
                }
                c.this.a();
            }

            @Override // android.media.browse.MediaBrowser.ConnectionCallback
            public void onConnectionFailed() {
                b bVar = c.this.b;
                if (bVar != null) {
                    bVar.i();
                }
                c.this.b();
            }

            @Override // android.media.browse.MediaBrowser.ConnectionCallback
            public void onConnectionSuspended() {
                b bVar = c.this.b;
                if (bVar != null) {
                    bVar.e();
                }
                c.this.c();
            }
        }

        /* loaded from: classes.dex */
        public interface b {
            void e();

            void h();

            void i();
        }

        public c() {
            if (Build.VERSION.SDK_INT >= 21) {
                this.a = new a();
            } else {
                this.a = null;
            }
        }

        public void a() {
            throw null;
        }

        public void b() {
            throw null;
        }

        public void c() {
            throw null;
        }

        public void d(b bVar) {
            this.b = bVar;
        }
    }

    /* loaded from: classes.dex */
    public interface d {
        void b();

        MediaSessionCompat.Token c();

        void f();
    }

    /* loaded from: classes.dex */
    public static class e implements d, i, c.b {
        public final Context a;
        public final MediaBrowser b;
        public final Bundle c;
        public final b d = new b(this);
        public final rh<String, k> e = new rh<>();
        public j f;
        public Messenger g;
        public MediaSessionCompat.Token h;

        public e(Context context, ComponentName componentName, c cVar, Bundle bundle) {
            this.a = context;
            Bundle bundle2 = bundle != null ? new Bundle(bundle) : new Bundle();
            this.c = bundle2;
            bundle2.putInt("extra_client_version", 1);
            bundle2.putInt("extra_calling_pid", Process.myPid());
            cVar.d(this);
            this.b = new MediaBrowser(context, componentName, cVar.a, bundle2);
        }

        @Override // android.support.v4.media.MediaBrowserCompat.i
        public void a(Messenger messenger, String str, MediaSessionCompat.Token token, Bundle bundle) {
        }

        @Override // android.support.v4.media.MediaBrowserCompat.d
        public void b() {
            Messenger messenger;
            j jVar = this.f;
            if (jVar != null && (messenger = this.g) != null) {
                try {
                    jVar.f(messenger);
                } catch (RemoteException unused) {
                }
            }
            this.b.disconnect();
        }

        @Override // android.support.v4.media.MediaBrowserCompat.d
        public MediaSessionCompat.Token c() {
            if (this.h == null) {
                this.h = MediaSessionCompat.Token.a(this.b.getSessionToken());
            }
            return this.h;
        }

        @Override // android.support.v4.media.MediaBrowserCompat.i
        public void d(Messenger messenger, String str, List<MediaItem> list, Bundle bundle, Bundle bundle2) {
            if (this.g != messenger) {
                return;
            }
            k kVar = this.e.get(str);
            if (kVar == null) {
                if (MediaBrowserCompat.b) {
                    StringBuilder sb = new StringBuilder();
                    sb.append("onLoadChildren for id that isn't subscribed id=");
                    sb.append(str);
                    return;
                }
                return;
            }
            l a = kVar.a(bundle);
            if (a != null) {
                if (bundle == null) {
                    if (list == null) {
                        a.c(str);
                    } else {
                        a.a(str, list);
                    }
                } else if (list == null) {
                    a.d(str, bundle);
                } else {
                    a.b(str, list, bundle);
                }
            }
        }

        @Override // android.support.v4.media.MediaBrowserCompat.c.b
        public void e() {
            this.f = null;
            this.g = null;
            this.h = null;
            this.d.a(null);
        }

        @Override // android.support.v4.media.MediaBrowserCompat.d
        public void f() {
            this.b.connect();
        }

        @Override // android.support.v4.media.MediaBrowserCompat.i
        public void g(Messenger messenger) {
        }

        @Override // android.support.v4.media.MediaBrowserCompat.c.b
        public void h() {
            try {
                Bundle extras = this.b.getExtras();
                if (extras == null) {
                    return;
                }
                extras.getInt("extra_service_version", 0);
                IBinder a = bs.a(extras, "extra_messenger");
                if (a != null) {
                    this.f = new j(a, this.c);
                    Messenger messenger = new Messenger(this.d);
                    this.g = messenger;
                    this.d.a(messenger);
                    try {
                        this.f.d(this.a, this.g);
                    } catch (RemoteException unused) {
                    }
                }
                android.support.v4.media.session.b b = b.a.b(bs.a(extras, "extra_session_binder"));
                if (b != null) {
                    this.h = MediaSessionCompat.Token.b(this.b.getSessionToken(), b);
                }
            } catch (IllegalStateException unused2) {
            }
        }

        @Override // android.support.v4.media.MediaBrowserCompat.c.b
        public void i() {
        }
    }

    /* loaded from: classes.dex */
    public static class f extends e {
        public f(Context context, ComponentName componentName, c cVar, Bundle bundle) {
            super(context, componentName, cVar, bundle);
        }
    }

    /* loaded from: classes.dex */
    public static class g extends f {
        public g(Context context, ComponentName componentName, c cVar, Bundle bundle) {
            super(context, componentName, cVar, bundle);
        }
    }

    /* loaded from: classes.dex */
    public static class h implements d, i {
        public final Context a;
        public final ComponentName b;
        public final c c;
        public final Bundle d;
        public final b e = new b(this);
        public final rh<String, k> f = new rh<>();
        public int g = 1;
        public c h;
        public j i;
        public Messenger j;
        public String k;
        public MediaSessionCompat.Token l;

        /* loaded from: classes.dex */
        public class a implements Runnable {
            public a() {
            }

            @Override // java.lang.Runnable
            public void run() {
                h hVar = h.this;
                if (hVar.g == 0) {
                    return;
                }
                hVar.g = 2;
                if (MediaBrowserCompat.b && hVar.h != null) {
                    throw new RuntimeException("mServiceConnection should be null. Instead it is " + h.this.h);
                } else if (hVar.i == null) {
                    if (hVar.j == null) {
                        Intent intent = new Intent("android.media.browse.MediaBrowserService");
                        intent.setComponent(h.this.b);
                        h hVar2 = h.this;
                        hVar2.h = new c();
                        boolean z = false;
                        try {
                            h hVar3 = h.this;
                            z = hVar3.a.bindService(intent, hVar3.h, 1);
                        } catch (Exception unused) {
                            StringBuilder sb = new StringBuilder();
                            sb.append("Failed binding to service ");
                            sb.append(h.this.b);
                        }
                        if (!z) {
                            h.this.h();
                            h.this.c.b();
                        }
                        if (MediaBrowserCompat.b) {
                            h.this.e();
                            return;
                        }
                        return;
                    }
                    throw new RuntimeException("mCallbacksMessenger should be null. Instead it is " + h.this.j);
                } else {
                    throw new RuntimeException("mServiceBinderWrapper should be null. Instead it is " + h.this.i);
                }
            }
        }

        /* loaded from: classes.dex */
        public class b implements Runnable {
            public b() {
            }

            @Override // java.lang.Runnable
            public void run() {
                h hVar = h.this;
                Messenger messenger = hVar.j;
                if (messenger != null) {
                    try {
                        hVar.i.c(messenger);
                    } catch (RemoteException unused) {
                        StringBuilder sb = new StringBuilder();
                        sb.append("RemoteException during connect for ");
                        sb.append(h.this.b);
                    }
                }
                h hVar2 = h.this;
                int i = hVar2.g;
                hVar2.h();
                if (i != 0) {
                    h.this.g = i;
                }
                if (MediaBrowserCompat.b) {
                    h.this.e();
                }
            }
        }

        /* loaded from: classes.dex */
        public class c implements ServiceConnection {

            /* loaded from: classes.dex */
            public class a implements Runnable {
                public final /* synthetic */ ComponentName a;
                public final /* synthetic */ IBinder f0;

                public a(ComponentName componentName, IBinder iBinder) {
                    this.a = componentName;
                    this.f0 = iBinder;
                }

                @Override // java.lang.Runnable
                public void run() {
                    boolean z = MediaBrowserCompat.b;
                    if (z) {
                        StringBuilder sb = new StringBuilder();
                        sb.append("MediaServiceConnection.onServiceConnected name=");
                        sb.append(this.a);
                        sb.append(" binder=");
                        sb.append(this.f0);
                        h.this.e();
                    }
                    if (c.this.a("onServiceConnected")) {
                        h hVar = h.this;
                        hVar.i = new j(this.f0, hVar.d);
                        h.this.j = new Messenger(h.this.e);
                        h hVar2 = h.this;
                        hVar2.e.a(hVar2.j);
                        h.this.g = 2;
                        if (z) {
                            try {
                                h.this.e();
                            } catch (RemoteException unused) {
                                StringBuilder sb2 = new StringBuilder();
                                sb2.append("RemoteException during connect for ");
                                sb2.append(h.this.b);
                                if (MediaBrowserCompat.b) {
                                    h.this.e();
                                    return;
                                }
                                return;
                            }
                        }
                        h hVar3 = h.this;
                        hVar3.i.b(hVar3.a, hVar3.j);
                    }
                }
            }

            /* loaded from: classes.dex */
            public class b implements Runnable {
                public final /* synthetic */ ComponentName a;

                public b(ComponentName componentName) {
                    this.a = componentName;
                }

                @Override // java.lang.Runnable
                public void run() {
                    if (MediaBrowserCompat.b) {
                        StringBuilder sb = new StringBuilder();
                        sb.append("MediaServiceConnection.onServiceDisconnected name=");
                        sb.append(this.a);
                        sb.append(" this=");
                        sb.append(this);
                        sb.append(" mServiceConnection=");
                        sb.append(h.this.h);
                        h.this.e();
                    }
                    if (c.this.a("onServiceDisconnected")) {
                        h hVar = h.this;
                        hVar.i = null;
                        hVar.j = null;
                        hVar.e.a(null);
                        h hVar2 = h.this;
                        hVar2.g = 4;
                        hVar2.c.c();
                    }
                }
            }

            public c() {
            }

            public boolean a(String str) {
                int i;
                h hVar = h.this;
                if (hVar.h != this || (i = hVar.g) == 0 || i == 1) {
                    int i2 = hVar.g;
                    if (i2 == 0 || i2 == 1) {
                        return false;
                    }
                    StringBuilder sb = new StringBuilder();
                    sb.append(str);
                    sb.append(" for ");
                    sb.append(h.this.b);
                    sb.append(" with mServiceConnection=");
                    sb.append(h.this.h);
                    sb.append(" this=");
                    sb.append(this);
                    return false;
                }
                return true;
            }

            public final void b(Runnable runnable) {
                if (Thread.currentThread() == h.this.e.getLooper().getThread()) {
                    runnable.run();
                } else {
                    h.this.e.post(runnable);
                }
            }

            @Override // android.content.ServiceConnection
            public void onServiceConnected(ComponentName componentName, IBinder iBinder) {
                b(new a(componentName, iBinder));
            }

            @Override // android.content.ServiceConnection
            public void onServiceDisconnected(ComponentName componentName) {
                b(new b(componentName));
            }
        }

        public h(Context context, ComponentName componentName, c cVar, Bundle bundle) {
            if (context == null) {
                throw new IllegalArgumentException("context must not be null");
            }
            if (componentName == null) {
                throw new IllegalArgumentException("service component must not be null");
            }
            if (cVar != null) {
                this.a = context;
                this.b = componentName;
                this.c = cVar;
                this.d = bundle == null ? null : new Bundle(bundle);
                return;
            }
            throw new IllegalArgumentException("connection callback must not be null");
        }

        public static String i(int i) {
            if (i != 0) {
                if (i != 1) {
                    if (i != 2) {
                        if (i != 3) {
                            if (i != 4) {
                                return "UNKNOWN/" + i;
                            }
                            return "CONNECT_STATE_SUSPENDED";
                        }
                        return "CONNECT_STATE_CONNECTED";
                    }
                    return "CONNECT_STATE_CONNECTING";
                }
                return "CONNECT_STATE_DISCONNECTED";
            }
            return "CONNECT_STATE_DISCONNECTING";
        }

        @Override // android.support.v4.media.MediaBrowserCompat.i
        public void a(Messenger messenger, String str, MediaSessionCompat.Token token, Bundle bundle) {
            if (k(messenger, "onConnect")) {
                if (this.g != 2) {
                    StringBuilder sb = new StringBuilder();
                    sb.append("onConnect from service while mState=");
                    sb.append(i(this.g));
                    sb.append("... ignoring");
                    return;
                }
                this.k = str;
                this.l = token;
                this.g = 3;
                if (MediaBrowserCompat.b) {
                    e();
                }
                this.c.a();
                try {
                    for (Map.Entry<String, k> entry : this.f.entrySet()) {
                        String key = entry.getKey();
                        k value = entry.getValue();
                        List<l> b2 = value.b();
                        List<Bundle> c2 = value.c();
                        for (int i = 0; i < b2.size(); i++) {
                            this.i.a(key, b2.get(i).a, c2.get(i), this.j);
                        }
                    }
                } catch (RemoteException unused) {
                }
            }
        }

        @Override // android.support.v4.media.MediaBrowserCompat.d
        public void b() {
            this.g = 0;
            this.e.post(new b());
        }

        @Override // android.support.v4.media.MediaBrowserCompat.d
        public MediaSessionCompat.Token c() {
            if (j()) {
                return this.l;
            }
            throw new IllegalStateException("getSessionToken() called while not connected(state=" + this.g + ")");
        }

        @Override // android.support.v4.media.MediaBrowserCompat.i
        public void d(Messenger messenger, String str, List<MediaItem> list, Bundle bundle, Bundle bundle2) {
            if (k(messenger, "onLoadChildren")) {
                boolean z = MediaBrowserCompat.b;
                if (z) {
                    StringBuilder sb = new StringBuilder();
                    sb.append("onLoadChildren for ");
                    sb.append(this.b);
                    sb.append(" id=");
                    sb.append(str);
                }
                k kVar = this.f.get(str);
                if (kVar == null) {
                    if (z) {
                        StringBuilder sb2 = new StringBuilder();
                        sb2.append("onLoadChildren for id that isn't subscribed id=");
                        sb2.append(str);
                        return;
                    }
                    return;
                }
                l a2 = kVar.a(bundle);
                if (a2 != null) {
                    if (bundle == null) {
                        if (list == null) {
                            a2.c(str);
                        } else {
                            a2.a(str, list);
                        }
                    } else if (list == null) {
                        a2.d(str, bundle);
                    } else {
                        a2.b(str, list, bundle);
                    }
                }
            }
        }

        public void e() {
            StringBuilder sb = new StringBuilder();
            sb.append("  mServiceComponent=");
            sb.append(this.b);
            StringBuilder sb2 = new StringBuilder();
            sb2.append("  mCallback=");
            sb2.append(this.c);
            StringBuilder sb3 = new StringBuilder();
            sb3.append("  mRootHints=");
            sb3.append(this.d);
            StringBuilder sb4 = new StringBuilder();
            sb4.append("  mState=");
            sb4.append(i(this.g));
            StringBuilder sb5 = new StringBuilder();
            sb5.append("  mServiceConnection=");
            sb5.append(this.h);
            StringBuilder sb6 = new StringBuilder();
            sb6.append("  mServiceBinderWrapper=");
            sb6.append(this.i);
            StringBuilder sb7 = new StringBuilder();
            sb7.append("  mCallbacksMessenger=");
            sb7.append(this.j);
            StringBuilder sb8 = new StringBuilder();
            sb8.append("  mRootId=");
            sb8.append(this.k);
            StringBuilder sb9 = new StringBuilder();
            sb9.append("  mMediaSessionToken=");
            sb9.append(this.l);
        }

        @Override // android.support.v4.media.MediaBrowserCompat.d
        public void f() {
            int i = this.g;
            if (i != 0 && i != 1) {
                throw new IllegalStateException("connect() called while neigther disconnecting nor disconnected (state=" + i(this.g) + ")");
            }
            this.g = 2;
            this.e.post(new a());
        }

        @Override // android.support.v4.media.MediaBrowserCompat.i
        public void g(Messenger messenger) {
            StringBuilder sb = new StringBuilder();
            sb.append("onConnectFailed for ");
            sb.append(this.b);
            if (k(messenger, "onConnectFailed")) {
                if (this.g != 2) {
                    StringBuilder sb2 = new StringBuilder();
                    sb2.append("onConnect from service while mState=");
                    sb2.append(i(this.g));
                    sb2.append("... ignoring");
                    return;
                }
                h();
                this.c.b();
            }
        }

        public void h() {
            c cVar = this.h;
            if (cVar != null) {
                this.a.unbindService(cVar);
            }
            this.g = 1;
            this.h = null;
            this.i = null;
            this.j = null;
            this.e.a(null);
            this.k = null;
            this.l = null;
        }

        public boolean j() {
            return this.g == 3;
        }

        public final boolean k(Messenger messenger, String str) {
            int i;
            if (this.j != messenger || (i = this.g) == 0 || i == 1) {
                int i2 = this.g;
                if (i2 == 0 || i2 == 1) {
                    return false;
                }
                StringBuilder sb = new StringBuilder();
                sb.append(str);
                sb.append(" for ");
                sb.append(this.b);
                sb.append(" with mCallbacksMessenger=");
                sb.append(this.j);
                sb.append(" this=");
                sb.append(this);
                return false;
            }
            return true;
        }
    }

    /* loaded from: classes.dex */
    public interface i {
        void a(Messenger messenger, String str, MediaSessionCompat.Token token, Bundle bundle);

        void d(Messenger messenger, String str, List<MediaItem> list, Bundle bundle, Bundle bundle2);

        void g(Messenger messenger);
    }

    /* loaded from: classes.dex */
    public static class j {
        public Messenger a;
        public Bundle b;

        public j(IBinder iBinder, Bundle bundle) {
            this.a = new Messenger(iBinder);
            this.b = bundle;
        }

        public void a(String str, IBinder iBinder, Bundle bundle, Messenger messenger) throws RemoteException {
            Bundle bundle2 = new Bundle();
            bundle2.putString("data_media_item_id", str);
            bs.b(bundle2, "data_callback_token", iBinder);
            bundle2.putBundle("data_options", bundle);
            e(3, bundle2, messenger);
        }

        public void b(Context context, Messenger messenger) throws RemoteException {
            Bundle bundle = new Bundle();
            bundle.putString("data_package_name", context.getPackageName());
            bundle.putInt("data_calling_pid", Process.myPid());
            bundle.putBundle("data_root_hints", this.b);
            e(1, bundle, messenger);
        }

        public void c(Messenger messenger) throws RemoteException {
            e(2, null, messenger);
        }

        public void d(Context context, Messenger messenger) throws RemoteException {
            Bundle bundle = new Bundle();
            bundle.putString("data_package_name", context.getPackageName());
            bundle.putInt("data_calling_pid", Process.myPid());
            bundle.putBundle("data_root_hints", this.b);
            e(6, bundle, messenger);
        }

        public final void e(int i, Bundle bundle, Messenger messenger) throws RemoteException {
            Message obtain = Message.obtain();
            obtain.what = i;
            obtain.arg1 = 1;
            obtain.setData(bundle);
            obtain.replyTo = messenger;
            this.a.send(obtain);
        }

        public void f(Messenger messenger) throws RemoteException {
            e(7, null, messenger);
        }
    }

    /* loaded from: classes.dex */
    public static class k {
        public final List<l> a = new ArrayList();
        public final List<Bundle> b = new ArrayList();

        public l a(Bundle bundle) {
            for (int i = 0; i < this.b.size(); i++) {
                if (r52.a(this.b.get(i), bundle)) {
                    return this.a.get(i);
                }
            }
            return null;
        }

        public List<l> b() {
            return this.a;
        }

        public List<Bundle> c() {
            return this.b;
        }
    }

    /* loaded from: classes.dex */
    public static abstract class l {
        public final IBinder a = new Binder();
        public WeakReference<k> b;

        /* loaded from: classes.dex */
        public class a extends MediaBrowser.SubscriptionCallback {
            public a() {
            }

            public List<MediaItem> a(List<MediaItem> list, Bundle bundle) {
                if (list == null) {
                    return null;
                }
                int i = bundle.getInt("android.media.browse.extra.PAGE", -1);
                int i2 = bundle.getInt("android.media.browse.extra.PAGE_SIZE", -1);
                if (i == -1 && i2 == -1) {
                    return list;
                }
                int i3 = i2 * i;
                int i4 = i3 + i2;
                if (i >= 0 && i2 >= 1 && i3 < list.size()) {
                    if (i4 > list.size()) {
                        i4 = list.size();
                    }
                    return list.subList(i3, i4);
                }
                return Collections.emptyList();
            }

            @Override // android.media.browse.MediaBrowser.SubscriptionCallback
            public void onChildrenLoaded(String str, List<MediaBrowser.MediaItem> list) {
                WeakReference<k> weakReference = l.this.b;
                k kVar = weakReference == null ? null : weakReference.get();
                if (kVar == null) {
                    l.this.a(str, MediaItem.b(list));
                    return;
                }
                List<MediaItem> b = MediaItem.b(list);
                List<l> b2 = kVar.b();
                List<Bundle> c = kVar.c();
                for (int i = 0; i < b2.size(); i++) {
                    Bundle bundle = c.get(i);
                    if (bundle == null) {
                        l.this.a(str, b);
                    } else {
                        l.this.b(str, a(b, bundle), bundle);
                    }
                }
            }

            @Override // android.media.browse.MediaBrowser.SubscriptionCallback
            public void onError(String str) {
                l.this.c(str);
            }
        }

        /* loaded from: classes.dex */
        public class b extends a {
            public b() {
                super();
            }

            @Override // android.media.browse.MediaBrowser.SubscriptionCallback
            public void onChildrenLoaded(String str, List<MediaBrowser.MediaItem> list, Bundle bundle) {
                MediaSessionCompat.a(bundle);
                l.this.b(str, MediaItem.b(list), bundle);
            }

            @Override // android.media.browse.MediaBrowser.SubscriptionCallback
            public void onError(String str, Bundle bundle) {
                MediaSessionCompat.a(bundle);
                l.this.d(str, bundle);
            }
        }

        public l() {
            int i = Build.VERSION.SDK_INT;
            if (i >= 26) {
                new b();
            } else if (i >= 21) {
                new a();
            }
        }

        public void a(String str, List<MediaItem> list) {
        }

        public void b(String str, List<MediaItem> list, Bundle bundle) {
        }

        public void c(String str) {
        }

        public void d(String str, Bundle bundle) {
        }
    }

    public MediaBrowserCompat(Context context, ComponentName componentName, c cVar, Bundle bundle) {
        int i2 = Build.VERSION.SDK_INT;
        if (i2 >= 26) {
            this.a = new g(context, componentName, cVar, bundle);
        } else if (i2 >= 23) {
            this.a = new f(context, componentName, cVar, bundle);
        } else if (i2 >= 21) {
            this.a = new e(context, componentName, cVar, bundle);
        } else {
            this.a = new h(context, componentName, cVar, bundle);
        }
    }

    public void a() {
        this.a.f();
    }

    public void b() {
        this.a.b();
    }

    public MediaSessionCompat.Token c() {
        return this.a.c();
    }

    @SuppressLint({"BanParcelableUsage"})
    /* loaded from: classes.dex */
    public static class MediaItem implements Parcelable {
        public static final Parcelable.Creator<MediaItem> CREATOR = new a();
        public final int a;
        public final MediaDescriptionCompat f0;

        /* loaded from: classes.dex */
        public class a implements Parcelable.Creator<MediaItem> {
            @Override // android.os.Parcelable.Creator
            /* renamed from: a */
            public MediaItem createFromParcel(Parcel parcel) {
                return new MediaItem(parcel);
            }

            @Override // android.os.Parcelable.Creator
            /* renamed from: b */
            public MediaItem[] newArray(int i) {
                return new MediaItem[i];
            }
        }

        public MediaItem(MediaDescriptionCompat mediaDescriptionCompat, int i) {
            if (mediaDescriptionCompat != null) {
                if (!TextUtils.isEmpty(mediaDescriptionCompat.c())) {
                    this.a = i;
                    this.f0 = mediaDescriptionCompat;
                    return;
                }
                throw new IllegalArgumentException("description must have a non-empty media id");
            }
            throw new IllegalArgumentException("description cannot be null");
        }

        public static MediaItem a(Object obj) {
            if (obj == null || Build.VERSION.SDK_INT < 21) {
                return null;
            }
            MediaBrowser.MediaItem mediaItem = (MediaBrowser.MediaItem) obj;
            return new MediaItem(MediaDescriptionCompat.a(a.a(mediaItem)), a.b(mediaItem));
        }

        public static List<MediaItem> b(List<?> list) {
            if (list == null || Build.VERSION.SDK_INT < 21) {
                return null;
            }
            ArrayList arrayList = new ArrayList(list.size());
            Iterator<?> it = list.iterator();
            while (it.hasNext()) {
                arrayList.add(a(it.next()));
            }
            return arrayList;
        }

        @Override // android.os.Parcelable
        public int describeContents() {
            return 0;
        }

        public String toString() {
            return "MediaItem{mFlags=" + this.a + ", mDescription=" + this.f0 + '}';
        }

        @Override // android.os.Parcelable
        public void writeToParcel(Parcel parcel, int i) {
            parcel.writeInt(this.a);
            this.f0.writeToParcel(parcel, i);
        }

        public MediaItem(Parcel parcel) {
            this.a = parcel.readInt();
            this.f0 = MediaDescriptionCompat.CREATOR.createFromParcel(parcel);
        }
    }
}
