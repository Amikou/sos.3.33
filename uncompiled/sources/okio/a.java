package okio;

import java.io.IOException;
import java.io.InterruptedIOException;
import java.util.concurrent.TimeUnit;

/* compiled from: AsyncTimeout.kt */
/* loaded from: classes2.dex */
public class a extends o {
    public static final C0255a Companion = new C0255a(null);
    private static final long IDLE_TIMEOUT_MILLIS;
    private static final long IDLE_TIMEOUT_NANOS;
    private static final int TIMEOUT_WRITE_SIZE = 65536;
    private static a head;
    private boolean inQueue;
    private a next;
    private long timeoutAt;

    /* compiled from: AsyncTimeout.kt */
    /* renamed from: okio.a$a  reason: collision with other inner class name */
    /* loaded from: classes2.dex */
    public static final class C0255a {
        public C0255a() {
        }

        public final a c() throws InterruptedException {
            a aVar = a.head;
            fs1.d(aVar);
            a aVar2 = aVar.next;
            if (aVar2 != null) {
                long remainingNanos = aVar2.remainingNanos(System.nanoTime());
                if (remainingNanos <= 0) {
                    a aVar3 = a.head;
                    fs1.d(aVar3);
                    aVar3.next = aVar2.next;
                    aVar2.next = null;
                    return aVar2;
                }
                long j = remainingNanos / 1000000;
                a.class.wait(j, (int) (remainingNanos - (1000000 * j)));
                return null;
            }
            long nanoTime = System.nanoTime();
            a.class.wait(a.IDLE_TIMEOUT_MILLIS);
            a aVar4 = a.head;
            fs1.d(aVar4);
            if (aVar4.next != null || System.nanoTime() - nanoTime < a.IDLE_TIMEOUT_NANOS) {
                return null;
            }
            return a.head;
        }

        public final boolean d(a aVar) {
            synchronized (a.class) {
                for (a aVar2 = a.head; aVar2 != null; aVar2 = aVar2.next) {
                    if (aVar2.next == aVar) {
                        aVar2.next = aVar.next;
                        aVar.next = null;
                        return false;
                    }
                }
                return true;
            }
        }

        public final void e(a aVar, long j, boolean z) {
            synchronized (a.class) {
                if (a.head == null) {
                    a.head = new a();
                    new b().start();
                }
                long nanoTime = System.nanoTime();
                int i = (j > 0L ? 1 : (j == 0L ? 0 : -1));
                if (i != 0 && z) {
                    aVar.timeoutAt = Math.min(j, aVar.deadlineNanoTime() - nanoTime) + nanoTime;
                } else if (i != 0) {
                    aVar.timeoutAt = j + nanoTime;
                } else if (z) {
                    aVar.timeoutAt = aVar.deadlineNanoTime();
                } else {
                    throw new AssertionError();
                }
                long remainingNanos = aVar.remainingNanos(nanoTime);
                a aVar2 = a.head;
                fs1.d(aVar2);
                while (aVar2.next != null) {
                    a aVar3 = aVar2.next;
                    fs1.d(aVar3);
                    if (remainingNanos < aVar3.remainingNanos(nanoTime)) {
                        break;
                    }
                    aVar2 = aVar2.next;
                    fs1.d(aVar2);
                }
                aVar.next = aVar2.next;
                aVar2.next = aVar;
                if (aVar2 == a.head) {
                    a.class.notify();
                }
                te4 te4Var = te4.a;
            }
        }

        public /* synthetic */ C0255a(qi0 qi0Var) {
            this();
        }
    }

    /* compiled from: AsyncTimeout.kt */
    /* loaded from: classes2.dex */
    public static final class b extends Thread {
        public b() {
            super("Okio Watchdog");
            setDaemon(true);
        }

        @Override // java.lang.Thread, java.lang.Runnable
        public void run() {
            a c;
            while (true) {
                try {
                    synchronized (a.class) {
                        c = a.Companion.c();
                        if (c == a.head) {
                            a.head = null;
                            return;
                        }
                        te4 te4Var = te4.a;
                    }
                    if (c != null) {
                        c.timedOut();
                    }
                } catch (InterruptedException unused) {
                }
            }
        }
    }

    /* compiled from: AsyncTimeout.kt */
    /* loaded from: classes2.dex */
    public static final class c implements m {
        public final /* synthetic */ m f0;

        public c(m mVar) {
            this.f0 = mVar;
        }

        @Override // okio.m
        /* renamed from: a */
        public a timeout() {
            return a.this;
        }

        @Override // okio.m, java.io.Closeable, java.lang.AutoCloseable, java.nio.channels.Channel
        public void close() {
            a aVar = a.this;
            aVar.enter();
            try {
                this.f0.close();
                te4 te4Var = te4.a;
                if (aVar.exit()) {
                    throw aVar.access$newTimeoutException(null);
                }
            } catch (IOException e) {
                if (!aVar.exit()) {
                    throw e;
                }
                throw aVar.access$newTimeoutException(e);
            } finally {
                aVar.exit();
            }
        }

        @Override // okio.m, java.io.Flushable
        public void flush() {
            a aVar = a.this;
            aVar.enter();
            try {
                this.f0.flush();
                te4 te4Var = te4.a;
                if (aVar.exit()) {
                    throw aVar.access$newTimeoutException(null);
                }
            } catch (IOException e) {
                if (!aVar.exit()) {
                    throw e;
                }
                throw aVar.access$newTimeoutException(e);
            } finally {
                aVar.exit();
            }
        }

        public String toString() {
            return "AsyncTimeout.sink(" + this.f0 + ')';
        }

        @Override // okio.m
        public void write(okio.b bVar, long j) {
            fs1.f(bVar, "source");
            defpackage.c.b(bVar.a0(), 0L, j);
            while (true) {
                long j2 = 0;
                if (j <= 0) {
                    return;
                }
                bj3 bj3Var = bVar.a;
                fs1.d(bj3Var);
                while (true) {
                    if (j2 >= 65536) {
                        break;
                    }
                    j2 += bj3Var.c - bj3Var.b;
                    if (j2 >= j) {
                        j2 = j;
                        break;
                    } else {
                        bj3Var = bj3Var.f;
                        fs1.d(bj3Var);
                    }
                }
                a aVar = a.this;
                aVar.enter();
                try {
                    this.f0.write(bVar, j2);
                    te4 te4Var = te4.a;
                    if (aVar.exit()) {
                        throw aVar.access$newTimeoutException(null);
                    }
                    j -= j2;
                } catch (IOException e) {
                    if (!aVar.exit()) {
                        throw e;
                    }
                    throw aVar.access$newTimeoutException(e);
                } finally {
                    aVar.exit();
                }
            }
        }
    }

    /* compiled from: AsyncTimeout.kt */
    /* loaded from: classes2.dex */
    public static final class d implements n {
        public final /* synthetic */ n f0;

        public d(n nVar) {
            this.f0 = nVar;
        }

        @Override // okio.n
        /* renamed from: a */
        public a timeout() {
            return a.this;
        }

        @Override // okio.n, java.io.Closeable, java.lang.AutoCloseable
        public void close() {
            a aVar = a.this;
            aVar.enter();
            try {
                this.f0.close();
                te4 te4Var = te4.a;
                if (aVar.exit()) {
                    throw aVar.access$newTimeoutException(null);
                }
            } catch (IOException e) {
                if (!aVar.exit()) {
                    throw e;
                }
                throw aVar.access$newTimeoutException(e);
            } finally {
                aVar.exit();
            }
        }

        @Override // okio.n
        public long read(okio.b bVar, long j) {
            fs1.f(bVar, "sink");
            a aVar = a.this;
            aVar.enter();
            try {
                long read = this.f0.read(bVar, j);
                if (aVar.exit()) {
                    throw aVar.access$newTimeoutException(null);
                }
                return read;
            } catch (IOException e) {
                if (aVar.exit()) {
                    throw aVar.access$newTimeoutException(e);
                }
                throw e;
            } finally {
                aVar.exit();
            }
        }

        public String toString() {
            return "AsyncTimeout.source(" + this.f0 + ')';
        }
    }

    static {
        long millis = TimeUnit.SECONDS.toMillis(60L);
        IDLE_TIMEOUT_MILLIS = millis;
        IDLE_TIMEOUT_NANOS = TimeUnit.MILLISECONDS.toNanos(millis);
    }

    /* JADX INFO: Access modifiers changed from: private */
    public final long remainingNanos(long j) {
        return this.timeoutAt - j;
    }

    public final IOException access$newTimeoutException(IOException iOException) {
        return newTimeoutException(iOException);
    }

    public final void enter() {
        if (!this.inQueue) {
            long timeoutNanos = timeoutNanos();
            boolean hasDeadline = hasDeadline();
            if (timeoutNanos != 0 || hasDeadline) {
                this.inQueue = true;
                Companion.e(this, timeoutNanos, hasDeadline);
                return;
            }
            return;
        }
        throw new IllegalStateException("Unbalanced enter/exit".toString());
    }

    public final boolean exit() {
        if (this.inQueue) {
            this.inQueue = false;
            return Companion.d(this);
        }
        return false;
    }

    public IOException newTimeoutException(IOException iOException) {
        InterruptedIOException interruptedIOException = new InterruptedIOException("timeout");
        if (iOException != null) {
            interruptedIOException.initCause(iOException);
        }
        return interruptedIOException;
    }

    public final m sink(m mVar) {
        fs1.f(mVar, "sink");
        return new c(mVar);
    }

    public final n source(n nVar) {
        fs1.f(nVar, "source");
        return new d(nVar);
    }

    public void timedOut() {
    }

    public final <T> T withTimeout(rc1<? extends T> rc1Var) {
        fs1.f(rc1Var, "block");
        enter();
        try {
            try {
                T invoke = rc1Var.invoke();
                uq1.b(1);
                if (exit()) {
                    throw access$newTimeoutException(null);
                }
                uq1.a(1);
                return invoke;
            } catch (IOException e) {
                if (exit()) {
                    throw access$newTimeoutException(e);
                }
                throw e;
            }
        } catch (Throwable th) {
            uq1.b(1);
            exit();
            uq1.a(1);
            throw th;
        }
    }
}
