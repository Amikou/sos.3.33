package okio;

import java.io.IOException;

/* compiled from: ForwardingSource.kt */
/* loaded from: classes2.dex */
public abstract class g implements n {
    private final n delegate;

    public g(n nVar) {
        fs1.f(nVar, "delegate");
        this.delegate = nVar;
    }

    /* renamed from: -deprecated_delegate  reason: not valid java name */
    public final n m200deprecated_delegate() {
        return this.delegate;
    }

    @Override // okio.n, java.io.Closeable, java.lang.AutoCloseable
    public void close() throws IOException {
        this.delegate.close();
    }

    public final n delegate() {
        return this.delegate;
    }

    @Override // okio.n
    public long read(b bVar, long j) throws IOException {
        fs1.f(bVar, "sink");
        return this.delegate.read(bVar, j);
    }

    @Override // okio.n
    public o timeout() {
        return this.delegate.timeout();
    }

    public String toString() {
        return getClass().getSimpleName() + '(' + this.delegate + ')';
    }
}
