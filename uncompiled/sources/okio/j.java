package okio;

import java.io.EOFException;
import java.io.IOException;
import java.util.zip.DataFormatException;
import java.util.zip.Inflater;

/* compiled from: InflaterSource.kt */
/* loaded from: classes2.dex */
public final class j implements n {
    public int a;
    public boolean f0;
    public final d g0;
    public final Inflater h0;

    public j(d dVar, Inflater inflater) {
        fs1.f(dVar, "source");
        fs1.f(inflater, "inflater");
        this.g0 = dVar;
        this.h0 = inflater;
    }

    public final long a(b bVar, long j) throws IOException {
        fs1.f(bVar, "sink");
        int i = (j > 0L ? 1 : (j == 0L ? 0 : -1));
        if (i >= 0) {
            if (!this.f0) {
                if (i == 0) {
                    return 0L;
                }
                try {
                    bj3 f0 = bVar.f0(1);
                    b();
                    int inflate = this.h0.inflate(f0.a, f0.c, (int) Math.min(j, 8192 - f0.c));
                    c();
                    if (inflate > 0) {
                        f0.c += inflate;
                        long j2 = inflate;
                        bVar.X(bVar.a0() + j2);
                        return j2;
                    }
                    if (f0.b == f0.c) {
                        bVar.a = f0.b();
                        dj3.b(f0);
                    }
                    return 0L;
                } catch (DataFormatException e) {
                    throw new IOException(e);
                }
            }
            throw new IllegalStateException("closed".toString());
        }
        throw new IllegalArgumentException(("byteCount < 0: " + j).toString());
    }

    public final boolean b() throws IOException {
        if (this.h0.needsInput()) {
            if (this.g0.c0()) {
                return true;
            }
            bj3 bj3Var = this.g0.o().a;
            fs1.d(bj3Var);
            int i = bj3Var.c;
            int i2 = bj3Var.b;
            int i3 = i - i2;
            this.a = i3;
            this.h0.setInput(bj3Var.a, i2, i3);
            return false;
        }
        return false;
    }

    public final void c() {
        int i = this.a;
        if (i == 0) {
            return;
        }
        int remaining = i - this.h0.getRemaining();
        this.a -= remaining;
        this.g0.skip(remaining);
    }

    @Override // okio.n, java.io.Closeable, java.lang.AutoCloseable
    public void close() throws IOException {
        if (this.f0) {
            return;
        }
        this.h0.end();
        this.f0 = true;
        this.g0.close();
    }

    @Override // okio.n
    public long read(b bVar, long j) throws IOException {
        fs1.f(bVar, "sink");
        do {
            long a = a(bVar, j);
            if (a > 0) {
                return a;
            }
            if (this.h0.finished() || this.h0.needsDictionary()) {
                return -1L;
            }
        } while (!this.g0.c0());
        throw new EOFException("source exhausted prematurely");
    }

    @Override // okio.n
    public o timeout() {
        return this.g0.timeout();
    }

    /* JADX WARN: 'this' call moved to the top of the method (can break code semantics) */
    public j(n nVar, Inflater inflater) {
        this(k.d(nVar), inflater);
        fs1.f(nVar, "source");
        fs1.f(inflater, "inflater");
    }
}
