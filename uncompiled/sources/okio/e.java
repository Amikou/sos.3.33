package okio;

import java.io.IOException;
import java.util.zip.Deflater;

/* compiled from: DeflaterSink.kt */
/* loaded from: classes2.dex */
public final class e implements m {
    public boolean a;
    public final c f0;
    public final Deflater g0;

    public e(c cVar, Deflater deflater) {
        fs1.f(cVar, "sink");
        fs1.f(deflater, "deflater");
        this.f0 = cVar;
        this.g0 = deflater;
    }

    public final void a(boolean z) {
        bj3 f0;
        int deflate;
        b o = this.f0.o();
        while (true) {
            f0 = o.f0(1);
            if (z) {
                Deflater deflater = this.g0;
                byte[] bArr = f0.a;
                int i = f0.c;
                deflate = deflater.deflate(bArr, i, 8192 - i, 2);
            } else {
                Deflater deflater2 = this.g0;
                byte[] bArr2 = f0.a;
                int i2 = f0.c;
                deflate = deflater2.deflate(bArr2, i2, 8192 - i2);
            }
            if (deflate > 0) {
                f0.c += deflate;
                o.X(o.a0() + deflate);
                this.f0.n0();
            } else if (this.g0.needsInput()) {
                break;
            }
        }
        if (f0.b == f0.c) {
            o.a = f0.b();
            dj3.b(f0);
        }
    }

    public final void b() {
        this.g0.finish();
        a(false);
    }

    @Override // okio.m, java.io.Closeable, java.lang.AutoCloseable, java.nio.channels.Channel
    public void close() throws IOException {
        if (this.a) {
            return;
        }
        Throwable th = null;
        try {
            b();
        } catch (Throwable th2) {
            th = th2;
        }
        try {
            this.g0.end();
        } catch (Throwable th3) {
            if (th == null) {
                th = th3;
            }
        }
        try {
            this.f0.close();
        } catch (Throwable th4) {
            if (th == null) {
                th = th4;
            }
        }
        this.a = true;
        if (th != null) {
            throw th;
        }
    }

    @Override // okio.m, java.io.Flushable
    public void flush() throws IOException {
        a(true);
        this.f0.flush();
    }

    @Override // okio.m
    public o timeout() {
        return this.f0.timeout();
    }

    public String toString() {
        return "DeflaterSink(" + this.f0 + ')';
    }

    @Override // okio.m
    public void write(b bVar, long j) throws IOException {
        fs1.f(bVar, "source");
        defpackage.c.b(bVar.a0(), 0L, j);
        while (j > 0) {
            bj3 bj3Var = bVar.a;
            fs1.d(bj3Var);
            int min = (int) Math.min(j, bj3Var.c - bj3Var.b);
            this.g0.setInput(bj3Var.a, bj3Var.b, min);
            a(false);
            long j2 = min;
            bVar.X(bVar.a0() - j2);
            int i = bj3Var.b + min;
            bj3Var.b = i;
            if (i == bj3Var.c) {
                bVar.a = bj3Var.b();
                dj3.b(bj3Var);
            }
            j -= j2;
        }
    }

    /* JADX WARN: 'this' call moved to the top of the method (can break code semantics) */
    public e(m mVar, Deflater deflater) {
        this(k.c(mVar), deflater);
        fs1.f(mVar, "sink");
        fs1.f(deflater, "deflater");
    }
}
