package okio;

import java.io.IOException;

/* compiled from: ForwardingSink.kt */
/* loaded from: classes2.dex */
public abstract class f implements m {
    private final m delegate;

    public f(m mVar) {
        fs1.f(mVar, "delegate");
        this.delegate = mVar;
    }

    /* renamed from: -deprecated_delegate  reason: not valid java name */
    public final m m199deprecated_delegate() {
        return this.delegate;
    }

    @Override // okio.m, java.io.Closeable, java.lang.AutoCloseable, java.nio.channels.Channel
    public void close() throws IOException {
        this.delegate.close();
    }

    public final m delegate() {
        return this.delegate;
    }

    @Override // okio.m, java.io.Flushable
    public void flush() throws IOException {
        this.delegate.flush();
    }

    @Override // okio.m
    public o timeout() {
        return this.delegate.timeout();
    }

    public String toString() {
        return getClass().getSimpleName() + '(' + this.delegate + ')';
    }

    @Override // okio.m
    public void write(b bVar, long j) throws IOException {
        fs1.f(bVar, "source");
        this.delegate.write(bVar, j);
    }
}
