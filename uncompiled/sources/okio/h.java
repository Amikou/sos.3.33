package okio;

import java.io.IOException;
import java.util.concurrent.TimeUnit;

/* compiled from: ForwardingTimeout.kt */
/* loaded from: classes2.dex */
public class h extends o {
    public o a;

    public h(o oVar) {
        fs1.f(oVar, "delegate");
        this.a = oVar;
    }

    public final o a() {
        return this.a;
    }

    public final h b(o oVar) {
        fs1.f(oVar, "delegate");
        this.a = oVar;
        return this;
    }

    @Override // okio.o
    public o clearDeadline() {
        return this.a.clearDeadline();
    }

    @Override // okio.o
    public o clearTimeout() {
        return this.a.clearTimeout();
    }

    @Override // okio.o
    public long deadlineNanoTime() {
        return this.a.deadlineNanoTime();
    }

    @Override // okio.o
    public boolean hasDeadline() {
        return this.a.hasDeadline();
    }

    @Override // okio.o
    public void throwIfReached() throws IOException {
        this.a.throwIfReached();
    }

    @Override // okio.o
    public o timeout(long j, TimeUnit timeUnit) {
        fs1.f(timeUnit, "unit");
        return this.a.timeout(j, timeUnit);
    }

    @Override // okio.o
    public long timeoutNanos() {
        return this.a.timeoutNanos();
    }

    @Override // okio.o
    public o deadlineNanoTime(long j) {
        return this.a.deadlineNanoTime(j);
    }
}
