package okio;

import java.io.Closeable;
import java.io.EOFException;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.nio.ByteBuffer;
import java.nio.channels.ByteChannel;
import java.nio.charset.Charset;
import java.util.Arrays;
import java.util.Objects;
import okhttp3.internal.connection.RealConnection;
import org.web3j.abi.datatypes.Utf8String;

/* compiled from: Buffer.kt */
/* loaded from: classes2.dex */
public final class b implements d, okio.c, Cloneable, ByteChannel {
    public bj3 a;
    public long f0;

    /* compiled from: Buffer.kt */
    /* loaded from: classes2.dex */
    public static final class a implements Closeable {
        public b a;
        public boolean f0;
        public bj3 g0;
        public byte[] i0;
        public long h0 = -1;
        public int j0 = -1;
        public int k0 = -1;

        public final int a() {
            long j = this.h0;
            b bVar = this.a;
            fs1.d(bVar);
            if (j != bVar.a0()) {
                long j2 = this.h0;
                return c(j2 == -1 ? 0L : j2 + (this.k0 - this.j0));
            }
            throw new IllegalStateException("no more bytes".toString());
        }

        public final long b(long j) {
            b bVar = this.a;
            if (bVar != null) {
                if (this.f0) {
                    long a0 = bVar.a0();
                    int i = (j > a0 ? 1 : (j == a0 ? 0 : -1));
                    int i2 = 1;
                    if (i <= 0) {
                        if (j >= 0) {
                            long j2 = a0 - j;
                            while (true) {
                                if (j2 <= 0) {
                                    break;
                                }
                                bj3 bj3Var = bVar.a;
                                fs1.d(bj3Var);
                                bj3 bj3Var2 = bj3Var.g;
                                fs1.d(bj3Var2);
                                int i3 = bj3Var2.c;
                                long j3 = i3 - bj3Var2.b;
                                if (j3 <= j2) {
                                    bVar.a = bj3Var2.b();
                                    dj3.b(bj3Var2);
                                    j2 -= j3;
                                } else {
                                    bj3Var2.c = i3 - ((int) j2);
                                    break;
                                }
                            }
                            this.g0 = null;
                            this.h0 = j;
                            this.i0 = null;
                            this.j0 = -1;
                            this.k0 = -1;
                        } else {
                            throw new IllegalArgumentException(("newSize < 0: " + j).toString());
                        }
                    } else if (i > 0) {
                        long j4 = j - a0;
                        boolean z = true;
                        while (j4 > 0) {
                            bj3 f0 = bVar.f0(i2);
                            int min = (int) Math.min(j4, 8192 - f0.c);
                            int i4 = f0.c + min;
                            f0.c = i4;
                            j4 -= min;
                            if (z) {
                                this.g0 = f0;
                                this.h0 = a0;
                                this.i0 = f0.a;
                                this.j0 = i4 - min;
                                this.k0 = i4;
                                z = false;
                            }
                            i2 = 1;
                        }
                    }
                    bVar.X(j);
                    return a0;
                }
                throw new IllegalStateException("resizeBuffer() only permitted for read/write buffers".toString());
            }
            throw new IllegalStateException("not attached to a buffer".toString());
        }

        public final int c(long j) {
            bj3 bj3Var;
            b bVar = this.a;
            if (bVar != null) {
                if (j >= -1 && j <= bVar.a0()) {
                    if (j != -1 && j != bVar.a0()) {
                        long j2 = 0;
                        long a0 = bVar.a0();
                        bj3 bj3Var2 = bVar.a;
                        bj3 bj3Var3 = this.g0;
                        if (bj3Var3 != null) {
                            long j3 = this.h0;
                            int i = this.j0;
                            fs1.d(bj3Var3);
                            long j4 = j3 - (i - bj3Var3.b);
                            if (j4 > j) {
                                bj3Var = bj3Var2;
                                bj3Var2 = this.g0;
                                a0 = j4;
                            } else {
                                bj3Var = this.g0;
                                j2 = j4;
                            }
                        } else {
                            bj3Var = bj3Var2;
                        }
                        if (a0 - j > j - j2) {
                            while (true) {
                                fs1.d(bj3Var);
                                int i2 = bj3Var.c;
                                int i3 = bj3Var.b;
                                if (j < (i2 - i3) + j2) {
                                    break;
                                }
                                j2 += i2 - i3;
                                bj3Var = bj3Var.f;
                            }
                        } else {
                            while (a0 > j) {
                                fs1.d(bj3Var2);
                                bj3Var2 = bj3Var2.g;
                                fs1.d(bj3Var2);
                                a0 -= bj3Var2.c - bj3Var2.b;
                            }
                            j2 = a0;
                            bj3Var = bj3Var2;
                        }
                        if (this.f0) {
                            fs1.d(bj3Var);
                            if (bj3Var.d) {
                                bj3 f = bj3Var.f();
                                if (bVar.a == bj3Var) {
                                    bVar.a = f;
                                }
                                bj3Var = bj3Var.c(f);
                                bj3 bj3Var4 = bj3Var.g;
                                fs1.d(bj3Var4);
                                bj3Var4.b();
                            }
                        }
                        this.g0 = bj3Var;
                        this.h0 = j;
                        fs1.d(bj3Var);
                        this.i0 = bj3Var.a;
                        int i4 = bj3Var.b + ((int) (j - j2));
                        this.j0 = i4;
                        int i5 = bj3Var.c;
                        this.k0 = i5;
                        return i5 - i4;
                    }
                    this.g0 = null;
                    this.h0 = j;
                    this.i0 = null;
                    this.j0 = -1;
                    this.k0 = -1;
                    return -1;
                }
                lu3 lu3Var = lu3.a;
                String format = String.format("offset=%s > size=%s", Arrays.copyOf(new Object[]{Long.valueOf(j), Long.valueOf(bVar.a0())}, 2));
                fs1.e(format, "java.lang.String.format(format, *args)");
                throw new ArrayIndexOutOfBoundsException(format);
            }
            throw new IllegalStateException("not attached to a buffer".toString());
        }

        @Override // java.io.Closeable, java.lang.AutoCloseable
        public void close() {
            if (this.a != null) {
                this.a = null;
                this.g0 = null;
                this.h0 = -1L;
                this.i0 = null;
                this.j0 = -1;
                this.k0 = -1;
                return;
            }
            throw new IllegalStateException("not attached to a buffer".toString());
        }
    }

    /* compiled from: Buffer.kt */
    /* loaded from: classes2.dex */
    public static final class c extends OutputStream {
        public c() {
        }

        @Override // java.io.OutputStream, java.io.Closeable, java.lang.AutoCloseable
        public void close() {
        }

        @Override // java.io.OutputStream, java.io.Flushable
        public void flush() {
        }

        public String toString() {
            return b.this + ".outputStream()";
        }

        @Override // java.io.OutputStream
        public void write(int i) {
            b.this.d0(i);
        }

        @Override // java.io.OutputStream
        public void write(byte[] bArr, int i, int i2) {
            fs1.f(bArr, "data");
            b.this.M0(bArr, i, i2);
        }
    }

    public static /* synthetic */ a M(b bVar, a aVar, int i, Object obj) {
        if ((i & 1) != 0) {
            aVar = new a();
        }
        return bVar.C(aVar);
    }

    public boolean A(long j, ByteString byteString, int i, int i2) {
        fs1.f(byteString, "bytes");
        if (j < 0 || i < 0 || i2 < 0 || a0() - j < i2 || byteString.size() - i < i2) {
            return false;
        }
        for (int i3 = 0; i3 < i2; i3++) {
            if (j(i3 + j) != byteString.getByte(i + i3)) {
                return false;
            }
        }
        return true;
    }

    @Override // okio.d
    public void A1(long j) throws EOFException {
        if (this.f0 < j) {
            throw new EOFException();
        }
    }

    @Override // okio.c
    /* renamed from: B0 */
    public b Q0(long j) {
        if (j == 0) {
            return d0(48);
        }
        long j2 = (j >>> 1) | j;
        long j3 = j2 | (j2 >>> 2);
        long j4 = j3 | (j3 >>> 4);
        long j5 = j4 | (j4 >>> 8);
        long j6 = j5 | (j5 >>> 16);
        long j7 = j6 | (j6 >>> 32);
        long j8 = j7 - ((j7 >>> 1) & 6148914691236517205L);
        long j9 = ((j8 >>> 2) & 3689348814741910323L) + (j8 & 3689348814741910323L);
        long j10 = ((j9 >>> 4) + j9) & 1085102592571150095L;
        long j11 = j10 + (j10 >>> 8);
        long j12 = j11 + (j11 >>> 16);
        int i = (int) ((((j12 & 63) + ((j12 >>> 32) & 63)) + 3) / 4);
        bj3 f0 = f0(i);
        byte[] bArr = f0.a;
        int i2 = f0.c;
        for (int i3 = (i2 + i) - 1; i3 >= i2; i3--) {
            bArr[i3] = ur.a()[(int) (15 & j)];
            j >>>= 4;
        }
        f0.c += i;
        X(a0() + i);
        return this;
    }

    public final a C(a aVar) {
        fs1.f(aVar, "unsafeCursor");
        if (aVar.a == null) {
            aVar.a = this;
            aVar.f0 = true;
            return aVar;
        }
        throw new IllegalStateException("already attached to a buffer".toString());
    }

    @Override // okio.c
    public OutputStream D1() {
        return new c();
    }

    /* JADX WARN: Removed duplicated region for block: B:32:0x009c  */
    /* JADX WARN: Removed duplicated region for block: B:33:0x00a6  */
    /* JADX WARN: Removed duplicated region for block: B:35:0x00aa  */
    /* JADX WARN: Removed duplicated region for block: B:42:0x00ae A[EDGE_INSN: B:42:0x00ae->B:37:0x00ae ?: BREAK  , SYNTHETIC] */
    @Override // okio.d
    /*
        Code decompiled incorrectly, please refer to instructions dump.
        To view partially-correct code enable 'Show inconsistent code' option in preferences
    */
    public long E1() throws java.io.EOFException {
        /*
            r14 = this;
            long r0 = r14.a0()
            r2 = 0
            int r0 = (r0 > r2 ? 1 : (r0 == r2 ? 0 : -1))
            if (r0 == 0) goto Lb8
            r0 = 0
            r1 = r0
            r4 = r2
        Ld:
            bj3 r6 = r14.a
            defpackage.fs1.d(r6)
            byte[] r7 = r6.a
            int r8 = r6.b
            int r9 = r6.c
        L18:
            if (r8 >= r9) goto L9a
            r10 = r7[r8]
            r11 = 48
            byte r11 = (byte) r11
            if (r10 < r11) goto L29
            r12 = 57
            byte r12 = (byte) r12
            if (r10 > r12) goto L29
            int r11 = r10 - r11
            goto L43
        L29:
            r11 = 97
            byte r11 = (byte) r11
            if (r10 < r11) goto L38
            r12 = 102(0x66, float:1.43E-43)
            byte r12 = (byte) r12
            if (r10 > r12) goto L38
        L33:
            int r11 = r10 - r11
            int r11 = r11 + 10
            goto L43
        L38:
            r11 = 65
            byte r11 = (byte) r11
            if (r10 < r11) goto L7b
            r12 = 70
            byte r12 = (byte) r12
            if (r10 > r12) goto L7b
            goto L33
        L43:
            r12 = -1152921504606846976(0xf000000000000000, double:-3.105036184601418E231)
            long r12 = r12 & r4
            int r12 = (r12 > r2 ? 1 : (r12 == r2 ? 0 : -1))
            if (r12 != 0) goto L53
            r10 = 4
            long r4 = r4 << r10
            long r10 = (long) r11
            long r4 = r4 | r10
            int r8 = r8 + 1
            int r0 = r0 + 1
            goto L18
        L53:
            okio.b r0 = new okio.b
            r0.<init>()
            okio.b r0 = r0.Q0(r4)
            okio.b r0 = r0.d0(r10)
            java.lang.NumberFormatException r1 = new java.lang.NumberFormatException
            java.lang.StringBuilder r2 = new java.lang.StringBuilder
            r2.<init>()
            java.lang.String r3 = "Number too large: "
            r2.append(r3)
            java.lang.String r0 = r0.j1()
            r2.append(r0)
            java.lang.String r0 = r2.toString()
            r1.<init>(r0)
            throw r1
        L7b:
            if (r0 == 0) goto L7f
            r1 = 1
            goto L9a
        L7f:
            java.lang.NumberFormatException r0 = new java.lang.NumberFormatException
            java.lang.StringBuilder r1 = new java.lang.StringBuilder
            r1.<init>()
            java.lang.String r2 = "Expected leading [0-9a-fA-F] character but was 0x"
            r1.append(r2)
            java.lang.String r2 = defpackage.c.e(r10)
            r1.append(r2)
            java.lang.String r1 = r1.toString()
            r0.<init>(r1)
            throw r0
        L9a:
            if (r8 != r9) goto La6
            bj3 r7 = r6.b()
            r14.a = r7
            defpackage.dj3.b(r6)
            goto La8
        La6:
            r6.b = r8
        La8:
            if (r1 != 0) goto Lae
            bj3 r6 = r14.a
            if (r6 != 0) goto Ld
        Lae:
            long r1 = r14.a0()
            long r6 = (long) r0
            long r1 = r1 - r6
            r14.X(r1)
            return r4
        Lb8:
            java.io.EOFException r0 = new java.io.EOFException
            r0.<init>()
            throw r0
        */
        throw new UnsupportedOperationException("Method not decompiled: okio.b.E1():long");
    }

    @Override // okio.d
    public boolean G0(long j, ByteString byteString) {
        fs1.f(byteString, "bytes");
        return A(j, byteString, 0, byteString.size());
    }

    @Override // okio.d
    public InputStream G1() {
        return new C0256b();
    }

    @Override // okio.d
    public b I() {
        return this;
    }

    @Override // okio.d
    public String I0(Charset charset) {
        fs1.f(charset, "charset");
        return R(this.f0, charset);
    }

    @Override // okio.d
    public ByteString J(long j) throws EOFException {
        if (j >= 0 && j <= ((long) Integer.MAX_VALUE)) {
            if (a0() >= j) {
                if (j >= 4096) {
                    ByteString e0 = e0((int) j);
                    skip(j);
                    return e0;
                }
                return new ByteString(g1(j));
            }
            throw new EOFException();
        }
        throw new IllegalArgumentException(("byteCount: " + j).toString());
    }

    @Override // okio.c
    /* renamed from: K0 */
    public b V(int i) {
        bj3 f0 = f0(4);
        byte[] bArr = f0.a;
        int i2 = f0.c;
        int i3 = i2 + 1;
        bArr[i2] = (byte) ((i >>> 24) & 255);
        int i4 = i3 + 1;
        bArr[i3] = (byte) ((i >>> 16) & 255);
        int i5 = i4 + 1;
        bArr[i4] = (byte) ((i >>> 8) & 255);
        bArr[i5] = (byte) (i & 255);
        f0.c = i5 + 1;
        X(a0() + 4);
        return this;
    }

    public b L0(long j) {
        bj3 f0 = f0(8);
        byte[] bArr = f0.a;
        int i = f0.c;
        int i2 = i + 1;
        bArr[i] = (byte) ((j >>> 56) & 255);
        int i3 = i2 + 1;
        bArr[i2] = (byte) ((j >>> 48) & 255);
        int i4 = i3 + 1;
        bArr[i3] = (byte) ((j >>> 40) & 255);
        int i5 = i4 + 1;
        bArr[i4] = (byte) ((j >>> 32) & 255);
        int i6 = i5 + 1;
        bArr[i5] = (byte) ((j >>> 24) & 255);
        int i7 = i6 + 1;
        bArr[i6] = (byte) ((j >>> 16) & 255);
        int i8 = i7 + 1;
        bArr[i7] = (byte) ((j >>> 8) & 255);
        bArr[i8] = (byte) (j & 255);
        f0.c = i8 + 1;
        X(a0() + 8);
        return this;
    }

    public int N() throws EOFException {
        return defpackage.c.c(readInt());
    }

    @Override // okio.c
    /* renamed from: O0 */
    public b P(int i) {
        bj3 f0 = f0(2);
        byte[] bArr = f0.a;
        int i2 = f0.c;
        int i3 = i2 + 1;
        bArr[i2] = (byte) ((i >>> 8) & 255);
        bArr[i3] = (byte) (i & 255);
        f0.c = i3 + 1;
        X(a0() + 2);
        return this;
    }

    @Override // okio.c
    public long P0(n nVar) throws IOException {
        fs1.f(nVar, "source");
        long j = 0;
        while (true) {
            long read = nVar.read(this, 8192);
            if (read == -1) {
                return j;
            }
            j += read;
        }
    }

    public short Q() throws EOFException {
        return defpackage.c.d(readShort());
    }

    public String R(long j, Charset charset) throws EOFException {
        fs1.f(charset, "charset");
        int i = (j > 0L ? 1 : (j == 0L ? 0 : -1));
        if (i >= 0 && j <= ((long) Integer.MAX_VALUE)) {
            if (this.f0 >= j) {
                if (i == 0) {
                    return "";
                }
                bj3 bj3Var = this.a;
                fs1.d(bj3Var);
                int i2 = bj3Var.b;
                if (i2 + j > bj3Var.c) {
                    return new String(g1(j), charset);
                }
                int i3 = (int) j;
                String str = new String(bj3Var.a, i2, i3, charset);
                int i4 = bj3Var.b + i3;
                bj3Var.b = i4;
                this.f0 -= j;
                if (i4 == bj3Var.c) {
                    this.a = bj3Var.b();
                    dj3.b(bj3Var);
                }
                return str;
            }
            throw new EOFException();
        }
        throw new IllegalArgumentException(("byteCount: " + j).toString());
    }

    @Override // okio.d
    public ByteString R0() {
        return J(a0());
    }

    public String S(long j) throws EOFException {
        return R(j, by.a);
    }

    public b S0(String str, int i, int i2, Charset charset) {
        fs1.f(str, Utf8String.TYPE_NAME);
        fs1.f(charset, "charset");
        if (!(i >= 0)) {
            throw new IllegalArgumentException(("beginIndex < 0: " + i).toString());
        }
        if (i2 >= i) {
            if (i2 <= str.length()) {
                if (fs1.b(charset, by.a)) {
                    return W0(str, i, i2);
                }
                String substring = str.substring(i, i2);
                fs1.e(substring, "(this as java.lang.Strin…ing(startIndex, endIndex)");
                Objects.requireNonNull(substring, "null cannot be cast to non-null type java.lang.String");
                byte[] bytes = substring.getBytes(charset);
                fs1.e(bytes, "(this as java.lang.String).getBytes(charset)");
                return M0(bytes, 0, bytes.length);
            }
            throw new IllegalArgumentException(("endIndex > string.length: " + i2 + " > " + str.length()).toString());
        }
        throw new IllegalArgumentException(("endIndex < beginIndex: " + i2 + " < " + i).toString());
    }

    public b T0(String str, Charset charset) {
        fs1.f(str, Utf8String.TYPE_NAME);
        fs1.f(charset, "charset");
        return S0(str, 0, str.length(), charset);
    }

    @Override // okio.c
    /* renamed from: U0 */
    public b C0(String str) {
        fs1.f(str, Utf8String.TYPE_NAME);
        return W0(str, 0, str.length());
    }

    @Override // okio.d
    public boolean V0(long j) {
        return this.f0 >= j;
    }

    public int W() throws EOFException {
        int i;
        int i2;
        int i3;
        if (a0() != 0) {
            byte j = j(0L);
            if ((j & 128) == 0) {
                i = j & Byte.MAX_VALUE;
                i3 = 0;
                i2 = 1;
            } else if ((j & 224) == 192) {
                i = j & 31;
                i2 = 2;
                i3 = 128;
            } else if ((j & 240) == 224) {
                i = j & 15;
                i2 = 3;
                i3 = 2048;
            } else if ((j & 248) != 240) {
                skip(1L);
                return 65533;
            } else {
                i = j & 7;
                i2 = 4;
                i3 = 65536;
            }
            long j2 = i2;
            if (a0() >= j2) {
                for (int i4 = 1; i4 < i2; i4++) {
                    long j3 = i4;
                    byte j4 = j(j3);
                    if ((j4 & 192) != 128) {
                        skip(j3);
                        return 65533;
                    }
                    i = (i << 6) | (j4 & 63);
                }
                skip(j2);
                if (i > 1114111) {
                    return 65533;
                }
                if ((55296 <= i && 57343 >= i) || i < i3) {
                    return 65533;
                }
                return i;
            }
            throw new EOFException("size < " + i2 + ": " + a0() + " (to read code point prefixed 0x" + defpackage.c.e(j) + ')');
        }
        throw new EOFException();
    }

    public b W0(String str, int i, int i2) {
        fs1.f(str, Utf8String.TYPE_NAME);
        if (!(i >= 0)) {
            throw new IllegalArgumentException(("beginIndex < 0: " + i).toString());
        }
        if (i2 >= i) {
            if (!(i2 <= str.length())) {
                throw new IllegalArgumentException(("endIndex > string.length: " + i2 + " > " + str.length()).toString());
            }
            while (i < i2) {
                char charAt = str.charAt(i);
                if (charAt < 128) {
                    bj3 f0 = f0(1);
                    byte[] bArr = f0.a;
                    int i3 = f0.c - i;
                    int min = Math.min(i2, 8192 - i3);
                    int i4 = i + 1;
                    bArr[i + i3] = (byte) charAt;
                    while (i4 < min) {
                        char charAt2 = str.charAt(i4);
                        if (charAt2 >= 128) {
                            break;
                        }
                        bArr[i4 + i3] = (byte) charAt2;
                        i4++;
                    }
                    int i5 = f0.c;
                    int i6 = (i3 + i4) - i5;
                    f0.c = i5 + i6;
                    X(a0() + i6);
                    i = i4;
                } else {
                    if (charAt < 2048) {
                        bj3 f02 = f0(2);
                        byte[] bArr2 = f02.a;
                        int i7 = f02.c;
                        bArr2[i7] = (byte) ((charAt >> 6) | 192);
                        bArr2[i7 + 1] = (byte) ((charAt & '?') | 128);
                        f02.c = i7 + 2;
                        X(a0() + 2);
                    } else if (charAt >= 55296 && charAt <= 57343) {
                        int i8 = i + 1;
                        char charAt3 = i8 < i2 ? str.charAt(i8) : (char) 0;
                        if (charAt <= 56319 && 56320 <= charAt3 && 57343 >= charAt3) {
                            int i9 = (((charAt & 1023) << 10) | (charAt3 & 1023)) + 65536;
                            bj3 f03 = f0(4);
                            byte[] bArr3 = f03.a;
                            int i10 = f03.c;
                            bArr3[i10] = (byte) ((i9 >> 18) | 240);
                            bArr3[i10 + 1] = (byte) (((i9 >> 12) & 63) | 128);
                            bArr3[i10 + 2] = (byte) (((i9 >> 6) & 63) | 128);
                            bArr3[i10 + 3] = (byte) ((i9 & 63) | 128);
                            f03.c = i10 + 4;
                            X(a0() + 4);
                            i += 2;
                        } else {
                            d0(63);
                            i = i8;
                        }
                    } else {
                        bj3 f04 = f0(3);
                        byte[] bArr4 = f04.a;
                        int i11 = f04.c;
                        bArr4[i11] = (byte) ((charAt >> '\f') | 224);
                        bArr4[i11 + 1] = (byte) ((63 & (charAt >> 6)) | 128);
                        bArr4[i11 + 2] = (byte) ((charAt & '?') | 128);
                        f04.c = i11 + 3;
                        X(a0() + 3);
                    }
                    i++;
                }
            }
            return this;
        }
        throw new IllegalArgumentException(("endIndex < beginIndex: " + i2 + " < " + i).toString());
    }

    public final void X(long j) {
        this.f0 = j;
    }

    public b X0(int i) {
        if (i < 128) {
            d0(i);
        } else if (i < 2048) {
            bj3 f0 = f0(2);
            byte[] bArr = f0.a;
            int i2 = f0.c;
            bArr[i2] = (byte) ((i >> 6) | 192);
            bArr[i2 + 1] = (byte) ((i & 63) | 128);
            f0.c = i2 + 2;
            X(a0() + 2);
        } else if (55296 <= i && 57343 >= i) {
            d0(63);
        } else if (i < 65536) {
            bj3 f02 = f0(3);
            byte[] bArr2 = f02.a;
            int i3 = f02.c;
            bArr2[i3] = (byte) ((i >> 12) | 224);
            bArr2[i3 + 1] = (byte) (((i >> 6) & 63) | 128);
            bArr2[i3 + 2] = (byte) ((i & 63) | 128);
            f02.c = i3 + 3;
            X(a0() + 3);
        } else if (i <= 1114111) {
            bj3 f03 = f0(4);
            byte[] bArr3 = f03.a;
            int i4 = f03.c;
            bArr3[i4] = (byte) ((i >> 18) | 240);
            bArr3[i4 + 1] = (byte) (((i >> 12) & 63) | 128);
            bArr3[i4 + 2] = (byte) (((i >> 6) & 63) | 128);
            bArr3[i4 + 3] = (byte) ((i & 63) | 128);
            f03.c = i4 + 4;
            X(a0() + 4);
        } else {
            throw new IllegalArgumentException("Unexpected code point: 0x" + defpackage.c.f(i));
        }
        return this;
    }

    @Override // okio.d
    public byte[] Z() {
        return g1(a0());
    }

    public final void a() {
        skip(a0());
    }

    public final long a0() {
        return this.f0;
    }

    @Override // okio.d
    public String a1() throws EOFException {
        return p0(Long.MAX_VALUE);
    }

    /* renamed from: b */
    public b clone() {
        return e();
    }

    public final ByteString b0() {
        if (a0() <= ((long) Integer.MAX_VALUE)) {
            return e0((int) a0());
        }
        throw new IllegalStateException(("size > Int.MAX_VALUE: " + a0()).toString());
    }

    @Override // okio.d
    public boolean c0() {
        return this.f0 == 0;
    }

    @Override // okio.n, java.io.Closeable, java.lang.AutoCloseable
    public void close() {
    }

    public final long d() {
        long a0 = a0();
        if (a0 == 0) {
            return 0L;
        }
        bj3 bj3Var = this.a;
        fs1.d(bj3Var);
        bj3 bj3Var2 = bj3Var.g;
        fs1.d(bj3Var2);
        int i = bj3Var2.c;
        if (i < 8192 && bj3Var2.e) {
            a0 -= i - bj3Var2.b;
        }
        return a0;
    }

    public final b e() {
        b bVar = new b();
        if (a0() != 0) {
            bj3 bj3Var = this.a;
            fs1.d(bj3Var);
            bj3 d = bj3Var.d();
            bVar.a = d;
            d.g = d;
            d.f = d;
            for (bj3 bj3Var2 = bj3Var.f; bj3Var2 != bj3Var; bj3Var2 = bj3Var2.f) {
                bj3 bj3Var3 = d.g;
                fs1.d(bj3Var3);
                fs1.d(bj3Var2);
                bj3Var3.c(bj3Var2.d());
            }
            bVar.X(a0());
        }
        return bVar;
    }

    public final ByteString e0(int i) {
        if (i == 0) {
            return ByteString.EMPTY;
        }
        defpackage.c.b(a0(), 0L, i);
        bj3 bj3Var = this.a;
        int i2 = 0;
        int i3 = 0;
        int i4 = 0;
        while (i3 < i) {
            fs1.d(bj3Var);
            int i5 = bj3Var.c;
            int i6 = bj3Var.b;
            if (i5 != i6) {
                i3 += i5 - i6;
                i4++;
                bj3Var = bj3Var.f;
            } else {
                throw new AssertionError("s.limit == s.pos");
            }
        }
        byte[][] bArr = new byte[i4];
        int[] iArr = new int[i4 * 2];
        bj3 bj3Var2 = this.a;
        int i7 = 0;
        while (i2 < i) {
            fs1.d(bj3Var2);
            bArr[i7] = bj3Var2.a;
            i2 += bj3Var2.c - bj3Var2.b;
            iArr[i7] = Math.min(i2, i);
            iArr[i7 + i4] = bj3Var2.b;
            bj3Var2.d = true;
            i7++;
            bj3Var2 = bj3Var2.f;
        }
        return new SegmentedByteString(bArr, iArr);
    }

    public boolean equals(Object obj) {
        if (this != obj) {
            if (!(obj instanceof b)) {
                return false;
            }
            b bVar = (b) obj;
            if (a0() != bVar.a0()) {
                return false;
            }
            if (a0() != 0) {
                bj3 bj3Var = this.a;
                fs1.d(bj3Var);
                bj3 bj3Var2 = bVar.a;
                fs1.d(bj3Var2);
                int i = bj3Var.b;
                int i2 = bj3Var2.b;
                long j = 0;
                while (j < a0()) {
                    long min = Math.min(bj3Var.c - i, bj3Var2.c - i2);
                    long j2 = 0;
                    while (j2 < min) {
                        int i3 = i + 1;
                        int i4 = i2 + 1;
                        if (bj3Var.a[i] != bj3Var2.a[i2]) {
                            return false;
                        }
                        j2++;
                        i = i3;
                        i2 = i4;
                    }
                    if (i == bj3Var.c) {
                        bj3Var = bj3Var.f;
                        fs1.d(bj3Var);
                        i = bj3Var.b;
                    }
                    if (i2 == bj3Var2.c) {
                        bj3Var2 = bj3Var2.f;
                        fs1.d(bj3Var2);
                        i2 = bj3Var2.b;
                    }
                    j += min;
                }
            }
        }
        return true;
    }

    public final b f(b bVar, long j, long j2) {
        fs1.f(bVar, "out");
        defpackage.c.b(a0(), j, j2);
        if (j2 != 0) {
            bVar.X(bVar.a0() + j2);
            bj3 bj3Var = this.a;
            while (true) {
                fs1.d(bj3Var);
                int i = bj3Var.c;
                int i2 = bj3Var.b;
                if (j < i - i2) {
                    break;
                }
                j -= i - i2;
                bj3Var = bj3Var.f;
            }
            while (j2 > 0) {
                fs1.d(bj3Var);
                bj3 d = bj3Var.d();
                int i3 = d.b + ((int) j);
                d.b = i3;
                d.c = Math.min(i3 + ((int) j2), d.c);
                bj3 bj3Var2 = bVar.a;
                if (bj3Var2 == null) {
                    d.g = d;
                    d.f = d;
                    bVar.a = d;
                } else {
                    fs1.d(bj3Var2);
                    bj3 bj3Var3 = bj3Var2.g;
                    fs1.d(bj3Var3);
                    bj3Var3.c(d);
                }
                j2 -= d.c - d.b;
                bj3Var = bj3Var.f;
                j = 0;
            }
        }
        return this;
    }

    public final bj3 f0(int i) {
        boolean z = true;
        if ((i < 1 || i > 8192) ? false : false) {
            bj3 bj3Var = this.a;
            if (bj3Var == null) {
                bj3 c2 = dj3.c();
                this.a = c2;
                c2.g = c2;
                c2.f = c2;
                return c2;
            }
            fs1.d(bj3Var);
            bj3 bj3Var2 = bj3Var.g;
            fs1.d(bj3Var2);
            return (bj3Var2.c + i > 8192 || !bj3Var2.e) ? bj3Var2.c(dj3.c()) : bj3Var2;
        }
        throw new IllegalArgumentException("unexpected capacity".toString());
    }

    @Override // okio.c, okio.m, java.io.Flushable
    public void flush() {
    }

    @Override // okio.c
    /* renamed from: g */
    public b O() {
        return this;
    }

    @Override // okio.c
    /* renamed from: g0 */
    public b n1(ByteString byteString) {
        fs1.f(byteString, "byteString");
        byteString.write$okio(this, 0, byteString.size());
        return this;
    }

    @Override // okio.d
    public byte[] g1(long j) throws EOFException {
        if (j >= 0 && j <= ((long) Integer.MAX_VALUE)) {
            if (a0() >= j) {
                byte[] bArr = new byte[(int) j];
                readFully(bArr);
                return bArr;
            }
            throw new EOFException();
        }
        throw new IllegalArgumentException(("byteCount: " + j).toString());
    }

    @Override // okio.c
    /* renamed from: h */
    public b n0() {
        return this;
    }

    @Override // okio.d
    public int h0(un2 un2Var) {
        fs1.f(un2Var, "options");
        int e = ur.e(this, un2Var, false, 2, null);
        if (e == -1) {
            return -1;
        }
        skip(un2Var.m()[e].size());
        return e;
    }

    public int hashCode() {
        bj3 bj3Var = this.a;
        if (bj3Var != null) {
            int i = 1;
            do {
                int i2 = bj3Var.c;
                for (int i3 = bj3Var.b; i3 < i2; i3++) {
                    i = (i * 31) + bj3Var.a[i3];
                }
                bj3Var = bj3Var.f;
                fs1.d(bj3Var);
            } while (bj3Var != this.a);
            return i;
        }
        return 0;
    }

    @Override // java.nio.channels.Channel
    public boolean isOpen() {
        return true;
    }

    public final byte j(long j) {
        defpackage.c.b(a0(), j, 1L);
        bj3 bj3Var = this.a;
        if (bj3Var != null) {
            if (a0() - j < j) {
                long a0 = a0();
                while (a0 > j) {
                    bj3Var = bj3Var.g;
                    fs1.d(bj3Var);
                    a0 -= bj3Var.c - bj3Var.b;
                }
                fs1.d(bj3Var);
                return bj3Var.a[(int) ((bj3Var.b + j) - a0)];
            }
            long j2 = 0;
            while (true) {
                long j3 = (bj3Var.c - bj3Var.b) + j2;
                if (j3 > j) {
                    fs1.d(bj3Var);
                    return bj3Var.a[(int) ((bj3Var.b + j) - j2)];
                }
                bj3Var = bj3Var.f;
                fs1.d(bj3Var);
                j2 = j3;
            }
        } else {
            fs1.d(null);
            throw null;
        }
    }

    @Override // okio.d
    public String j1() {
        return R(this.f0, by.a);
    }

    @Override // okio.d
    public void k0(b bVar, long j) throws EOFException {
        fs1.f(bVar, "sink");
        if (a0() >= j) {
            bVar.write(this, j);
        } else {
            bVar.write(this, a0());
            throw new EOFException();
        }
    }

    public long l(byte b, long j, long j2) {
        bj3 bj3Var;
        int i;
        long j3 = 0;
        if (0 <= j && j2 >= j) {
            if (j2 > a0()) {
                j2 = a0();
            }
            if (j == j2 || (bj3Var = this.a) == null) {
                return -1L;
            }
            if (a0() - j < j) {
                j3 = a0();
                while (j3 > j) {
                    bj3Var = bj3Var.g;
                    fs1.d(bj3Var);
                    j3 -= bj3Var.c - bj3Var.b;
                }
                while (j3 < j2) {
                    byte[] bArr = bj3Var.a;
                    int min = (int) Math.min(bj3Var.c, (bj3Var.b + j2) - j3);
                    i = (int) ((bj3Var.b + j) - j3);
                    while (i < min) {
                        if (bArr[i] != b) {
                            i++;
                        }
                    }
                    j3 += bj3Var.c - bj3Var.b;
                    bj3Var = bj3Var.f;
                    fs1.d(bj3Var);
                    j = j3;
                }
                return -1L;
            }
            while (true) {
                long j4 = (bj3Var.c - bj3Var.b) + j3;
                if (j4 > j) {
                    break;
                }
                bj3Var = bj3Var.f;
                fs1.d(bj3Var);
                j3 = j4;
            }
            while (j3 < j2) {
                byte[] bArr2 = bj3Var.a;
                int min2 = (int) Math.min(bj3Var.c, (bj3Var.b + j2) - j3);
                i = (int) ((bj3Var.b + j) - j3);
                while (i < min2) {
                    if (bArr2[i] != b) {
                        i++;
                    }
                }
                j3 += bj3Var.c - bj3Var.b;
                bj3Var = bj3Var.f;
                fs1.d(bj3Var);
                j = j3;
            }
            return -1L;
            return (i - bj3Var.b) + j3;
        }
        throw new IllegalArgumentException(("size=" + a0() + " fromIndex=" + j + " toIndex=" + j2).toString());
    }

    public b m0(n nVar, long j) throws IOException {
        fs1.f(nVar, "source");
        while (j > 0) {
            long read = nVar.read(this, j);
            if (read == -1) {
                throw new EOFException();
            }
            j -= read;
        }
        return this;
    }

    @Override // okio.d
    public b o() {
        return this;
    }

    /* JADX WARN: Removed duplicated region for block: B:36:0x00ad  */
    /* JADX WARN: Removed duplicated region for block: B:37:0x00b7  */
    /* JADX WARN: Removed duplicated region for block: B:39:0x00bd  */
    /* JADX WARN: Removed duplicated region for block: B:50:0x00c1 A[EDGE_INSN: B:50:0x00c1->B:41:0x00c1 ?: BREAK  , SYNTHETIC] */
    @Override // okio.d
    /*
        Code decompiled incorrectly, please refer to instructions dump.
        To view partially-correct code enable 'Show inconsistent code' option in preferences
    */
    public long o0() throws java.io.EOFException {
        /*
            Method dump skipped, instructions count: 213
            To view this dump change 'Code comments level' option to 'DEBUG'
        */
        throw new UnsupportedOperationException("Method not decompiled: okio.b.o0():long");
    }

    @Override // okio.d
    public String p0(long j) throws EOFException {
        if (j >= 0) {
            long j2 = j != Long.MAX_VALUE ? j + 1 : Long.MAX_VALUE;
            byte b = (byte) 10;
            long l = l(b, 0L, j2);
            if (l != -1) {
                return ur.c(this, l);
            }
            if (j2 < a0() && j(j2 - 1) == ((byte) 13) && j(j2) == b) {
                return ur.c(this, j2);
            }
            b bVar = new b();
            f(bVar, 0L, Math.min(32, a0()));
            throw new EOFException("\\n not found: limit=" + Math.min(a0(), j) + " content=" + bVar.R0().hex() + (char) 8230);
        }
        throw new IllegalArgumentException(("limit < 0: " + j).toString());
    }

    @Override // okio.d
    public d peek() {
        return k.d(new fq2(this));
    }

    public long r(ByteString byteString) throws IOException {
        fs1.f(byteString, "bytes");
        return u(byteString, 0L);
    }

    @Override // okio.c
    /* renamed from: r0 */
    public b l1(byte[] bArr) {
        fs1.f(bArr, "source");
        return M0(bArr, 0, bArr.length);
    }

    @Override // java.nio.channels.ReadableByteChannel
    public int read(ByteBuffer byteBuffer) throws IOException {
        fs1.f(byteBuffer, "sink");
        bj3 bj3Var = this.a;
        if (bj3Var != null) {
            int min = Math.min(byteBuffer.remaining(), bj3Var.c - bj3Var.b);
            byteBuffer.put(bj3Var.a, bj3Var.b, min);
            int i = bj3Var.b + min;
            bj3Var.b = i;
            this.f0 -= min;
            if (i == bj3Var.c) {
                this.a = bj3Var.b();
                dj3.b(bj3Var);
            }
            return min;
        }
        return -1;
    }

    @Override // okio.d
    public byte readByte() throws EOFException {
        if (a0() != 0) {
            bj3 bj3Var = this.a;
            fs1.d(bj3Var);
            int i = bj3Var.b;
            int i2 = bj3Var.c;
            int i3 = i + 1;
            byte b = bj3Var.a[i];
            X(a0() - 1);
            if (i3 == i2) {
                this.a = bj3Var.b();
                dj3.b(bj3Var);
            } else {
                bj3Var.b = i3;
            }
            return b;
        }
        throw new EOFException();
    }

    @Override // okio.d
    public void readFully(byte[] bArr) throws EOFException {
        fs1.f(bArr, "sink");
        int i = 0;
        while (i < bArr.length) {
            int read = read(bArr, i, bArr.length - i);
            if (read == -1) {
                throw new EOFException();
            }
            i += read;
        }
    }

    @Override // okio.d
    public int readInt() throws EOFException {
        if (a0() >= 4) {
            bj3 bj3Var = this.a;
            fs1.d(bj3Var);
            int i = bj3Var.b;
            int i2 = bj3Var.c;
            if (i2 - i < 4) {
                return ((readByte() & 255) << 24) | ((readByte() & 255) << 16) | ((readByte() & 255) << 8) | (readByte() & 255);
            }
            byte[] bArr = bj3Var.a;
            int i3 = i + 1;
            int i4 = i3 + 1;
            int i5 = ((bArr[i] & 255) << 24) | ((bArr[i3] & 255) << 16);
            int i6 = i4 + 1;
            int i7 = i5 | ((bArr[i4] & 255) << 8);
            int i8 = i6 + 1;
            int i9 = i7 | (bArr[i6] & 255);
            X(a0() - 4);
            if (i8 == i2) {
                this.a = bj3Var.b();
                dj3.b(bj3Var);
            } else {
                bj3Var.b = i8;
            }
            return i9;
        }
        throw new EOFException();
    }

    @Override // okio.d
    public long readLong() throws EOFException {
        if (a0() >= 8) {
            bj3 bj3Var = this.a;
            fs1.d(bj3Var);
            int i = bj3Var.b;
            int i2 = bj3Var.c;
            if (i2 - i < 8) {
                return ((readInt() & 4294967295L) << 32) | (4294967295L & readInt());
            }
            byte[] bArr = bj3Var.a;
            int i3 = i + 1;
            int i4 = i3 + 1;
            int i5 = i4 + 1;
            int i6 = i5 + 1;
            int i7 = i6 + 1;
            int i8 = i7 + 1;
            long j = ((bArr[i] & 255) << 56) | ((bArr[i3] & 255) << 48) | ((bArr[i4] & 255) << 40) | ((bArr[i5] & 255) << 32) | ((bArr[i6] & 255) << 24) | ((bArr[i7] & 255) << 16);
            int i9 = i8 + 1;
            int i10 = i9 + 1;
            long j2 = j | ((bArr[i8] & 255) << 8) | (bArr[i9] & 255);
            X(a0() - 8);
            if (i10 == i2) {
                this.a = bj3Var.b();
                dj3.b(bj3Var);
            } else {
                bj3Var.b = i10;
            }
            return j2;
        }
        throw new EOFException();
    }

    @Override // okio.d
    public short readShort() throws EOFException {
        if (a0() >= 2) {
            bj3 bj3Var = this.a;
            fs1.d(bj3Var);
            int i = bj3Var.b;
            int i2 = bj3Var.c;
            if (i2 - i < 2) {
                return (short) (((readByte() & 255) << 8) | (readByte() & 255));
            }
            byte[] bArr = bj3Var.a;
            int i3 = i + 1;
            int i4 = i3 + 1;
            int i5 = ((bArr[i] & 255) << 8) | (bArr[i3] & 255);
            X(a0() - 2);
            if (i4 == i2) {
                this.a = bj3Var.b();
                dj3.b(bj3Var);
            } else {
                bj3Var.b = i4;
            }
            return (short) i5;
        }
        throw new EOFException();
    }

    @Override // okio.d
    public void skip(long j) throws EOFException {
        while (j > 0) {
            bj3 bj3Var = this.a;
            if (bj3Var != null) {
                int min = (int) Math.min(j, bj3Var.c - bj3Var.b);
                long j2 = min;
                X(a0() - j2);
                j -= j2;
                int i = bj3Var.b + min;
                bj3Var.b = i;
                if (i == bj3Var.c) {
                    this.a = bj3Var.b();
                    dj3.b(bj3Var);
                }
            } else {
                throw new EOFException();
            }
        }
    }

    @Override // okio.d
    public long t1(m mVar) throws IOException {
        fs1.f(mVar, "sink");
        long a0 = a0();
        if (a0 > 0) {
            mVar.write(this, a0);
        }
        return a0;
    }

    @Override // okio.n
    public o timeout() {
        return o.NONE;
    }

    public String toString() {
        return b0().toString();
    }

    public long u(ByteString byteString, long j) throws IOException {
        long j2 = j;
        fs1.f(byteString, "bytes");
        if (byteString.size() > 0) {
            long j3 = 0;
            if (j2 >= 0) {
                bj3 bj3Var = this.a;
                if (bj3Var != null) {
                    if (a0() - j2 < j2) {
                        long a0 = a0();
                        while (a0 > j2) {
                            bj3Var = bj3Var.g;
                            fs1.d(bj3Var);
                            a0 -= bj3Var.c - bj3Var.b;
                        }
                        byte[] internalArray$okio = byteString.internalArray$okio();
                        byte b = internalArray$okio[0];
                        int size = byteString.size();
                        long a02 = (a0() - size) + 1;
                        while (a0 < a02) {
                            byte[] bArr = bj3Var.a;
                            long j4 = a0;
                            int min = (int) Math.min(bj3Var.c, (bj3Var.b + a02) - a0);
                            for (int i = (int) ((bj3Var.b + j2) - j4); i < min; i++) {
                                if (bArr[i] == b && ur.b(bj3Var, i + 1, internalArray$okio, 1, size)) {
                                    return (i - bj3Var.b) + j4;
                                }
                            }
                            a0 = j4 + (bj3Var.c - bj3Var.b);
                            bj3Var = bj3Var.f;
                            fs1.d(bj3Var);
                            j2 = a0;
                        }
                    } else {
                        while (true) {
                            long j5 = (bj3Var.c - bj3Var.b) + j3;
                            if (j5 > j2) {
                                break;
                            }
                            bj3Var = bj3Var.f;
                            fs1.d(bj3Var);
                            j3 = j5;
                        }
                        byte[] internalArray$okio2 = byteString.internalArray$okio();
                        byte b2 = internalArray$okio2[0];
                        int size2 = byteString.size();
                        long a03 = (a0() - size2) + 1;
                        while (j3 < a03) {
                            byte[] bArr2 = bj3Var.a;
                            long j6 = a03;
                            int min2 = (int) Math.min(bj3Var.c, (bj3Var.b + a03) - j3);
                            for (int i2 = (int) ((bj3Var.b + j2) - j3); i2 < min2; i2++) {
                                if (bArr2[i2] == b2 && ur.b(bj3Var, i2 + 1, internalArray$okio2, 1, size2)) {
                                    return (i2 - bj3Var.b) + j3;
                                }
                            }
                            j3 += bj3Var.c - bj3Var.b;
                            bj3Var = bj3Var.f;
                            fs1.d(bj3Var);
                            j2 = j3;
                            a03 = j6;
                        }
                    }
                }
                return -1L;
            }
            throw new IllegalArgumentException(("fromIndex < 0: " + j2).toString());
        }
        throw new IllegalArgumentException("bytes is empty".toString());
    }

    public long v(ByteString byteString) {
        fs1.f(byteString, "targetBytes");
        return x(byteString, 0L);
    }

    @Override // okio.c
    /* renamed from: w0 */
    public b M0(byte[] bArr, int i, int i2) {
        fs1.f(bArr, "source");
        long j = i2;
        defpackage.c.b(bArr.length, i, j);
        int i3 = i2 + i;
        while (i < i3) {
            bj3 f0 = f0(1);
            int min = Math.min(i3 - i, 8192 - f0.c);
            int i4 = i + min;
            zh.d(bArr, f0.a, f0.c, i, i4);
            f0.c += min;
            i = i4;
        }
        X(a0() + j);
        return this;
    }

    @Override // java.nio.channels.WritableByteChannel
    public int write(ByteBuffer byteBuffer) throws IOException {
        fs1.f(byteBuffer, "source");
        int remaining = byteBuffer.remaining();
        int i = remaining;
        while (i > 0) {
            bj3 f0 = f0(1);
            int min = Math.min(i, 8192 - f0.c);
            byteBuffer.get(f0.a, f0.c, min);
            i -= min;
            f0.c += min;
        }
        this.f0 += remaining;
        return remaining;
    }

    public long x(ByteString byteString, long j) {
        int i;
        int i2;
        fs1.f(byteString, "targetBytes");
        long j2 = 0;
        if (j >= 0) {
            bj3 bj3Var = this.a;
            if (bj3Var != null) {
                if (a0() - j < j) {
                    j2 = a0();
                    while (j2 > j) {
                        bj3Var = bj3Var.g;
                        fs1.d(bj3Var);
                        j2 -= bj3Var.c - bj3Var.b;
                    }
                    if (byteString.size() == 2) {
                        byte b = byteString.getByte(0);
                        byte b2 = byteString.getByte(1);
                        while (j2 < a0()) {
                            byte[] bArr = bj3Var.a;
                            i = (int) ((bj3Var.b + j) - j2);
                            int i3 = bj3Var.c;
                            while (i < i3) {
                                byte b3 = bArr[i];
                                if (b3 != b && b3 != b2) {
                                    i++;
                                }
                                i2 = bj3Var.b;
                            }
                            j2 += bj3Var.c - bj3Var.b;
                            bj3Var = bj3Var.f;
                            fs1.d(bj3Var);
                            j = j2;
                        }
                        return -1L;
                    }
                    byte[] internalArray$okio = byteString.internalArray$okio();
                    while (j2 < a0()) {
                        byte[] bArr2 = bj3Var.a;
                        i = (int) ((bj3Var.b + j) - j2);
                        int i4 = bj3Var.c;
                        while (i < i4) {
                            byte b4 = bArr2[i];
                            for (byte b5 : internalArray$okio) {
                                if (b4 == b5) {
                                    i2 = bj3Var.b;
                                }
                            }
                            i++;
                        }
                        j2 += bj3Var.c - bj3Var.b;
                        bj3Var = bj3Var.f;
                        fs1.d(bj3Var);
                        j = j2;
                    }
                    return -1L;
                }
                while (true) {
                    long j3 = (bj3Var.c - bj3Var.b) + j2;
                    if (j3 > j) {
                        break;
                    }
                    bj3Var = bj3Var.f;
                    fs1.d(bj3Var);
                    j2 = j3;
                }
                if (byteString.size() == 2) {
                    byte b6 = byteString.getByte(0);
                    byte b7 = byteString.getByte(1);
                    while (j2 < a0()) {
                        byte[] bArr3 = bj3Var.a;
                        i = (int) ((bj3Var.b + j) - j2);
                        int i5 = bj3Var.c;
                        while (i < i5) {
                            byte b8 = bArr3[i];
                            if (b8 != b6 && b8 != b7) {
                                i++;
                            }
                            i2 = bj3Var.b;
                        }
                        j2 += bj3Var.c - bj3Var.b;
                        bj3Var = bj3Var.f;
                        fs1.d(bj3Var);
                        j = j2;
                    }
                    return -1L;
                }
                byte[] internalArray$okio2 = byteString.internalArray$okio();
                while (j2 < a0()) {
                    byte[] bArr4 = bj3Var.a;
                    i = (int) ((bj3Var.b + j) - j2);
                    int i6 = bj3Var.c;
                    while (i < i6) {
                        byte b9 = bArr4[i];
                        for (byte b10 : internalArray$okio2) {
                            if (b9 == b10) {
                                i2 = bj3Var.b;
                            }
                        }
                        i++;
                    }
                    j2 += bj3Var.c - bj3Var.b;
                    bj3Var = bj3Var.f;
                    fs1.d(bj3Var);
                    j = j2;
                }
                return -1L;
                return (i - i2) + j2;
            }
            return -1L;
        }
        throw new IllegalArgumentException(("fromIndex < 0: " + j).toString());
    }

    @Override // okio.c
    /* renamed from: x0 */
    public b d0(int i) {
        bj3 f0 = f0(1);
        byte[] bArr = f0.a;
        int i2 = f0.c;
        f0.c = i2 + 1;
        bArr[i2] = (byte) i;
        X(a0() + 1);
        return this;
    }

    @Override // okio.c
    /* renamed from: z0 */
    public b B1(long j) {
        int i;
        int i2 = (j > 0L ? 1 : (j == 0L ? 0 : -1));
        if (i2 == 0) {
            return d0(48);
        }
        boolean z = false;
        int i3 = 1;
        if (i2 < 0) {
            j = -j;
            if (j < 0) {
                return C0("-9223372036854775808");
            }
            z = true;
        }
        if (j < 100000000) {
            if (j >= 10000) {
                i = j < 1000000 ? j < 100000 ? 5 : 6 : j < 10000000 ? 7 : 8;
            } else if (j >= 100) {
                i = j < 1000 ? 3 : 4;
            } else if (j >= 10) {
                i3 = 2;
            }
            i3 = i;
        } else if (j < 1000000000000L) {
            if (j < RealConnection.IDLE_CONNECTION_HEALTHY_NS) {
                i3 = j < 1000000000 ? 9 : 10;
            } else {
                i = j < 100000000000L ? 11 : 12;
                i3 = i;
            }
        } else if (j >= 1000000000000000L) {
            i3 = j < 100000000000000000L ? j < 10000000000000000L ? 16 : 17 : j < 1000000000000000000L ? 18 : 19;
        } else if (j < 10000000000000L) {
            i3 = 13;
        } else {
            i = j < 100000000000000L ? 14 : 15;
            i3 = i;
        }
        if (z) {
            i3++;
        }
        bj3 f0 = f0(i3);
        byte[] bArr = f0.a;
        int i4 = f0.c + i3;
        while (j != 0) {
            long j2 = 10;
            i4--;
            bArr[i4] = ur.a()[(int) (j % j2)];
            j /= j2;
        }
        if (z) {
            bArr[i4 - 1] = (byte) 45;
        }
        f0.c += i3;
        X(a0() + i3);
        return this;
    }

    /* compiled from: Buffer.kt */
    /* renamed from: okio.b$b  reason: collision with other inner class name */
    /* loaded from: classes2.dex */
    public static final class C0256b extends InputStream {
        public C0256b() {
        }

        @Override // java.io.InputStream
        public int available() {
            return (int) Math.min(b.this.a0(), Integer.MAX_VALUE);
        }

        @Override // java.io.InputStream, java.io.Closeable, java.lang.AutoCloseable
        public void close() {
        }

        @Override // java.io.InputStream
        public int read() {
            if (b.this.a0() > 0) {
                return b.this.readByte() & 255;
            }
            return -1;
        }

        public String toString() {
            return b.this + ".inputStream()";
        }

        @Override // java.io.InputStream
        public int read(byte[] bArr, int i, int i2) {
            fs1.f(bArr, "sink");
            return b.this.read(bArr, i, i2);
        }
    }

    @Override // okio.m
    public void write(b bVar, long j) {
        bj3 bj3Var;
        bj3 bj3Var2;
        fs1.f(bVar, "source");
        if (bVar != this) {
            defpackage.c.b(bVar.a0(), 0L, j);
            while (j > 0) {
                bj3 bj3Var3 = bVar.a;
                fs1.d(bj3Var3);
                int i = bj3Var3.c;
                fs1.d(bVar.a);
                if (j < i - bj3Var.b) {
                    bj3 bj3Var4 = this.a;
                    if (bj3Var4 != null) {
                        fs1.d(bj3Var4);
                        bj3Var2 = bj3Var4.g;
                    } else {
                        bj3Var2 = null;
                    }
                    if (bj3Var2 != null && bj3Var2.e) {
                        if ((bj3Var2.c + j) - (bj3Var2.d ? 0 : bj3Var2.b) <= 8192) {
                            bj3 bj3Var5 = bVar.a;
                            fs1.d(bj3Var5);
                            bj3Var5.g(bj3Var2, (int) j);
                            bVar.X(bVar.a0() - j);
                            X(a0() + j);
                            return;
                        }
                    }
                    bj3 bj3Var6 = bVar.a;
                    fs1.d(bj3Var6);
                    bVar.a = bj3Var6.e((int) j);
                }
                bj3 bj3Var7 = bVar.a;
                fs1.d(bj3Var7);
                long j2 = bj3Var7.c - bj3Var7.b;
                bVar.a = bj3Var7.b();
                bj3 bj3Var8 = this.a;
                if (bj3Var8 == null) {
                    this.a = bj3Var7;
                    bj3Var7.g = bj3Var7;
                    bj3Var7.f = bj3Var7;
                } else {
                    fs1.d(bj3Var8);
                    bj3 bj3Var9 = bj3Var8.g;
                    fs1.d(bj3Var9);
                    bj3Var9.c(bj3Var7).a();
                }
                bVar.X(bVar.a0() - j2);
                X(a0() + j2);
                j -= j2;
            }
            return;
        }
        throw new IllegalArgumentException("source == this".toString());
    }

    public int read(byte[] bArr, int i, int i2) {
        fs1.f(bArr, "sink");
        defpackage.c.b(bArr.length, i, i2);
        bj3 bj3Var = this.a;
        if (bj3Var != null) {
            int min = Math.min(i2, bj3Var.c - bj3Var.b);
            byte[] bArr2 = bj3Var.a;
            int i3 = bj3Var.b;
            zh.d(bArr2, bArr, i, i3, i3 + min);
            bj3Var.b += min;
            X(a0() - min);
            if (bj3Var.b == bj3Var.c) {
                this.a = bj3Var.b();
                dj3.b(bj3Var);
                return min;
            }
            return min;
        }
        return -1;
    }

    @Override // okio.n
    public long read(b bVar, long j) {
        fs1.f(bVar, "sink");
        if (j >= 0) {
            if (a0() == 0) {
                return -1L;
            }
            if (j > a0()) {
                j = a0();
            }
            bVar.write(this, j);
            return j;
        }
        throw new IllegalArgumentException(("byteCount < 0: " + j).toString());
    }
}
