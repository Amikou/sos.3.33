package okio;

import java.io.IOException;
import java.io.InterruptedIOException;
import java.util.concurrent.TimeUnit;

/* compiled from: Timeout.kt */
/* loaded from: classes2.dex */
public class o {
    public static final b Companion = new b(null);
    public static final o NONE = new a();
    private long deadlineNanoTime;
    private boolean hasDeadline;
    private long timeoutNanos;

    /* compiled from: Timeout.kt */
    /* loaded from: classes2.dex */
    public static final class a extends o {
        @Override // okio.o
        public o deadlineNanoTime(long j) {
            return this;
        }

        @Override // okio.o
        public void throwIfReached() {
        }

        @Override // okio.o
        public o timeout(long j, TimeUnit timeUnit) {
            fs1.f(timeUnit, "unit");
            return this;
        }
    }

    /* compiled from: Timeout.kt */
    /* loaded from: classes2.dex */
    public static final class b {
        public b() {
        }

        public final long a(long j, long j2) {
            return (j != 0 && (j2 == 0 || j < j2)) ? j : j2;
        }

        public /* synthetic */ b(qi0 qi0Var) {
            this();
        }
    }

    public o clearDeadline() {
        this.hasDeadline = false;
        return this;
    }

    public o clearTimeout() {
        this.timeoutNanos = 0L;
        return this;
    }

    public final o deadline(long j, TimeUnit timeUnit) {
        fs1.f(timeUnit, "unit");
        if (j > 0) {
            return deadlineNanoTime(System.nanoTime() + timeUnit.toNanos(j));
        }
        throw new IllegalArgumentException(("duration <= 0: " + j).toString());
    }

    public long deadlineNanoTime() {
        if (this.hasDeadline) {
            return this.deadlineNanoTime;
        }
        throw new IllegalStateException("No deadline".toString());
    }

    public boolean hasDeadline() {
        return this.hasDeadline;
    }

    public final void intersectWith(o oVar, rc1<te4> rc1Var) {
        fs1.f(oVar, "other");
        fs1.f(rc1Var, "block");
        long timeoutNanos = timeoutNanos();
        long a2 = Companion.a(oVar.timeoutNanos(), timeoutNanos());
        TimeUnit timeUnit = TimeUnit.NANOSECONDS;
        timeout(a2, timeUnit);
        if (hasDeadline()) {
            long deadlineNanoTime = deadlineNanoTime();
            if (oVar.hasDeadline()) {
                deadlineNanoTime(Math.min(deadlineNanoTime(), oVar.deadlineNanoTime()));
            }
            try {
                rc1Var.invoke();
                uq1.b(1);
                timeout(timeoutNanos, timeUnit);
                if (oVar.hasDeadline()) {
                    deadlineNanoTime(deadlineNanoTime);
                }
                uq1.a(1);
                return;
            } catch (Throwable th) {
                uq1.b(1);
                timeout(timeoutNanos, TimeUnit.NANOSECONDS);
                if (oVar.hasDeadline()) {
                    deadlineNanoTime(deadlineNanoTime);
                }
                uq1.a(1);
                throw th;
            }
        }
        if (oVar.hasDeadline()) {
            deadlineNanoTime(oVar.deadlineNanoTime());
        }
        try {
            rc1Var.invoke();
            uq1.b(1);
            timeout(timeoutNanos, timeUnit);
            if (oVar.hasDeadline()) {
                clearDeadline();
            }
            uq1.a(1);
        } catch (Throwable th2) {
            uq1.b(1);
            timeout(timeoutNanos, TimeUnit.NANOSECONDS);
            if (oVar.hasDeadline()) {
                clearDeadline();
            }
            uq1.a(1);
            throw th2;
        }
    }

    public void throwIfReached() throws IOException {
        if (!Thread.interrupted()) {
            if (this.hasDeadline && this.deadlineNanoTime - System.nanoTime() <= 0) {
                throw new InterruptedIOException("deadline reached");
            }
            return;
        }
        Thread.currentThread().interrupt();
        throw new InterruptedIOException("interrupted");
    }

    public o timeout(long j, TimeUnit timeUnit) {
        fs1.f(timeUnit, "unit");
        if (j >= 0) {
            this.timeoutNanos = timeUnit.toNanos(j);
            return this;
        }
        throw new IllegalArgumentException(("timeout < 0: " + j).toString());
    }

    public long timeoutNanos() {
        return this.timeoutNanos;
    }

    public final void waitUntilNotified(Object obj) throws InterruptedIOException {
        fs1.f(obj, "monitor");
        try {
            boolean hasDeadline = hasDeadline();
            long timeoutNanos = timeoutNanos();
            long j = 0;
            if (!hasDeadline && timeoutNanos == 0) {
                obj.wait();
                return;
            }
            long nanoTime = System.nanoTime();
            if (hasDeadline && timeoutNanos != 0) {
                timeoutNanos = Math.min(timeoutNanos, deadlineNanoTime() - nanoTime);
            } else if (hasDeadline) {
                timeoutNanos = deadlineNanoTime() - nanoTime;
            }
            if (timeoutNanos > 0) {
                long j2 = timeoutNanos / 1000000;
                Long.signum(j2);
                obj.wait(j2, (int) (timeoutNanos - (1000000 * j2)));
                j = System.nanoTime() - nanoTime;
            }
            if (j >= timeoutNanos) {
                throw new InterruptedIOException("timeout");
            }
        } catch (InterruptedException unused) {
            Thread.currentThread().interrupt();
            throw new InterruptedIOException("interrupted");
        }
    }

    public o deadlineNanoTime(long j) {
        this.hasDeadline = true;
        this.deadlineNanoTime = j;
        return this;
    }
}
