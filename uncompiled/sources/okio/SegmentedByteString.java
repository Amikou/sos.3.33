package okio;

import java.io.IOException;
import java.io.OutputStream;
import java.nio.ByteBuffer;
import java.nio.charset.Charset;
import java.security.InvalidKeyException;
import java.security.MessageDigest;
import java.util.Objects;
import javax.crypto.Mac;
import javax.crypto.spec.SecretKeySpec;

/* compiled from: SegmentedByteString.kt */
/* loaded from: classes2.dex */
public final class SegmentedByteString extends ByteString {
    public final transient byte[][] g0;
    public final transient int[] h0;

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public SegmentedByteString(byte[][] bArr, int[] iArr) {
        super(ByteString.EMPTY.getData$okio());
        fs1.f(bArr, "segments");
        fs1.f(iArr, "directory");
        this.g0 = bArr;
        this.h0 = iArr;
    }

    private final Object writeReplace() {
        ByteString a = a();
        Objects.requireNonNull(a, "null cannot be cast to non-null type java.lang.Object");
        return a;
    }

    public final ByteString a() {
        return new ByteString(toByteArray());
    }

    @Override // okio.ByteString
    public ByteBuffer asByteBuffer() {
        ByteBuffer asReadOnlyBuffer = ByteBuffer.wrap(toByteArray()).asReadOnlyBuffer();
        fs1.e(asReadOnlyBuffer, "ByteBuffer.wrap(toByteArray()).asReadOnlyBuffer()");
        return asReadOnlyBuffer;
    }

    @Override // okio.ByteString
    public String base64() {
        return a().base64();
    }

    @Override // okio.ByteString
    public String base64Url() {
        return a().base64Url();
    }

    @Override // okio.ByteString
    public ByteString digest$okio(String str) {
        fs1.f(str, "algorithm");
        MessageDigest messageDigest = MessageDigest.getInstance(str);
        int length = getSegments$okio().length;
        int i = 0;
        int i2 = 0;
        while (i < length) {
            int i3 = getDirectory$okio()[length + i];
            int i4 = getDirectory$okio()[i];
            messageDigest.update(getSegments$okio()[i], i3, i4 - i2);
            i++;
            i2 = i4;
        }
        byte[] digest = messageDigest.digest();
        fs1.e(digest, "digest.digest()");
        return new ByteString(digest);
    }

    @Override // okio.ByteString
    public boolean equals(Object obj) {
        if (obj == this) {
            return true;
        }
        if (obj instanceof ByteString) {
            ByteString byteString = (ByteString) obj;
            if (byteString.size() == size() && rangeEquals(0, byteString, 0, size())) {
                return true;
            }
        }
        return false;
    }

    public final int[] getDirectory$okio() {
        return this.h0;
    }

    public final byte[][] getSegments$okio() {
        return this.g0;
    }

    @Override // okio.ByteString
    public int getSize$okio() {
        return getDirectory$okio()[getSegments$okio().length - 1];
    }

    @Override // okio.ByteString
    public int hashCode() {
        int hashCode$okio = getHashCode$okio();
        if (hashCode$okio != 0) {
            return hashCode$okio;
        }
        int length = getSegments$okio().length;
        int i = 0;
        int i2 = 1;
        int i3 = 0;
        while (i < length) {
            int i4 = getDirectory$okio()[length + i];
            int i5 = getDirectory$okio()[i];
            byte[] bArr = getSegments$okio()[i];
            int i6 = (i5 - i3) + i4;
            while (i4 < i6) {
                i2 = (i2 * 31) + bArr[i4];
                i4++;
            }
            i++;
            i3 = i5;
        }
        setHashCode$okio(i2);
        return i2;
    }

    @Override // okio.ByteString
    public String hex() {
        return a().hex();
    }

    @Override // okio.ByteString
    public ByteString hmac$okio(String str, ByteString byteString) {
        fs1.f(str, "algorithm");
        fs1.f(byteString, "key");
        try {
            Mac mac = Mac.getInstance(str);
            mac.init(new SecretKeySpec(byteString.toByteArray(), str));
            int length = getSegments$okio().length;
            int i = 0;
            int i2 = 0;
            while (i < length) {
                int i3 = getDirectory$okio()[length + i];
                int i4 = getDirectory$okio()[i];
                mac.update(getSegments$okio()[i], i3, i4 - i2);
                i++;
                i2 = i4;
            }
            byte[] doFinal = mac.doFinal();
            fs1.e(doFinal, "mac.doFinal()");
            return new ByteString(doFinal);
        } catch (InvalidKeyException e) {
            throw new IllegalArgumentException(e);
        }
    }

    @Override // okio.ByteString
    public int indexOf(byte[] bArr, int i) {
        fs1.f(bArr, "other");
        return a().indexOf(bArr, i);
    }

    @Override // okio.ByteString
    public byte[] internalArray$okio() {
        return toByteArray();
    }

    @Override // okio.ByteString
    public byte internalGet$okio(int i) {
        defpackage.c.b(getDirectory$okio()[getSegments$okio().length - 1], i, 1L);
        int b = ej3.b(this, i);
        return getSegments$okio()[b][(i - (b == 0 ? 0 : getDirectory$okio()[b - 1])) + getDirectory$okio()[getSegments$okio().length + b]];
    }

    @Override // okio.ByteString
    public int lastIndexOf(byte[] bArr, int i) {
        fs1.f(bArr, "other");
        return a().lastIndexOf(bArr, i);
    }

    @Override // okio.ByteString
    public boolean rangeEquals(int i, ByteString byteString, int i2, int i3) {
        fs1.f(byteString, "other");
        if (i < 0 || i > size() - i3) {
            return false;
        }
        int i4 = i3 + i;
        int b = ej3.b(this, i);
        while (i < i4) {
            int i5 = b == 0 ? 0 : getDirectory$okio()[b - 1];
            int i6 = getDirectory$okio()[getSegments$okio().length + b];
            int min = Math.min(i4, (getDirectory$okio()[b] - i5) + i5) - i;
            if (!byteString.rangeEquals(i2, getSegments$okio()[b], i6 + (i - i5), min)) {
                return false;
            }
            i2 += min;
            i += min;
            b++;
        }
        return true;
    }

    @Override // okio.ByteString
    public String string(Charset charset) {
        fs1.f(charset, "charset");
        return a().string(charset);
    }

    @Override // okio.ByteString
    public ByteString substring(int i, int i2) {
        if (i >= 0) {
            if (!(i2 <= size())) {
                throw new IllegalArgumentException(("endIndex=" + i2 + " > length(" + size() + ')').toString());
            }
            int i3 = i2 - i;
            if (i3 >= 0) {
                if (i == 0 && i2 == size()) {
                    return this;
                }
                if (i == i2) {
                    return ByteString.EMPTY;
                }
                int b = ej3.b(this, i);
                int b2 = ej3.b(this, i2 - 1);
                byte[][] bArr = (byte[][]) zh.i(getSegments$okio(), b, b2 + 1);
                int[] iArr = new int[bArr.length * 2];
                if (b <= b2) {
                    int i4 = 0;
                    int i5 = b;
                    while (true) {
                        iArr[i4] = Math.min(getDirectory$okio()[i5] - i, i3);
                        int i6 = i4 + 1;
                        iArr[i4 + bArr.length] = getDirectory$okio()[getSegments$okio().length + i5];
                        if (i5 == b2) {
                            break;
                        }
                        i5++;
                        i4 = i6;
                    }
                }
                int i7 = b != 0 ? getDirectory$okio()[b - 1] : 0;
                int length = bArr.length;
                iArr[length] = iArr[length] + (i - i7);
                return new SegmentedByteString(bArr, iArr);
            }
            throw new IllegalArgumentException(("endIndex=" + i2 + " < beginIndex=" + i).toString());
        }
        throw new IllegalArgumentException(("beginIndex=" + i + " < 0").toString());
    }

    @Override // okio.ByteString
    public ByteString toAsciiLowercase() {
        return a().toAsciiLowercase();
    }

    @Override // okio.ByteString
    public ByteString toAsciiUppercase() {
        return a().toAsciiUppercase();
    }

    @Override // okio.ByteString
    public byte[] toByteArray() {
        byte[] bArr = new byte[size()];
        int length = getSegments$okio().length;
        int i = 0;
        int i2 = 0;
        int i3 = 0;
        while (i < length) {
            int i4 = getDirectory$okio()[length + i];
            int i5 = getDirectory$okio()[i];
            int i6 = i5 - i2;
            zh.d(getSegments$okio()[i], bArr, i3, i4, i4 + i6);
            i3 += i6;
            i++;
            i2 = i5;
        }
        return bArr;
    }

    @Override // okio.ByteString
    public String toString() {
        return a().toString();
    }

    @Override // okio.ByteString
    public void write(OutputStream outputStream) throws IOException {
        fs1.f(outputStream, "out");
        int length = getSegments$okio().length;
        int i = 0;
        int i2 = 0;
        while (i < length) {
            int i3 = getDirectory$okio()[length + i];
            int i4 = getDirectory$okio()[i];
            outputStream.write(getSegments$okio()[i], i3, i4 - i2);
            i++;
            i2 = i4;
        }
    }

    @Override // okio.ByteString
    public void write$okio(b bVar, int i, int i2) {
        fs1.f(bVar, "buffer");
        int i3 = i2 + i;
        int b = ej3.b(this, i);
        while (i < i3) {
            int i4 = b == 0 ? 0 : getDirectory$okio()[b - 1];
            int i5 = getDirectory$okio()[getSegments$okio().length + b];
            int min = Math.min(i3, (getDirectory$okio()[b] - i4) + i4) - i;
            int i6 = i5 + (i - i4);
            bj3 bj3Var = new bj3(getSegments$okio()[b], i6, i6 + min, true, false);
            bj3 bj3Var2 = bVar.a;
            if (bj3Var2 == null) {
                bj3Var.g = bj3Var;
                bj3Var.f = bj3Var;
                bVar.a = bj3Var;
            } else {
                fs1.d(bj3Var2);
                bj3 bj3Var3 = bj3Var2.g;
                fs1.d(bj3Var3);
                bj3Var3.c(bj3Var);
            }
            i += min;
            b++;
        }
        bVar.X(bVar.a0() + size());
    }

    @Override // okio.ByteString
    public boolean rangeEquals(int i, byte[] bArr, int i2, int i3) {
        fs1.f(bArr, "other");
        if (i < 0 || i > size() - i3 || i2 < 0 || i2 > bArr.length - i3) {
            return false;
        }
        int i4 = i3 + i;
        int b = ej3.b(this, i);
        while (i < i4) {
            int i5 = b == 0 ? 0 : getDirectory$okio()[b - 1];
            int i6 = getDirectory$okio()[getSegments$okio().length + b];
            int min = Math.min(i4, (getDirectory$okio()[b] - i5) + i5) - i;
            if (!defpackage.c.a(getSegments$okio()[b], i6 + (i - i5), bArr, i2, min)) {
                return false;
            }
            i2 += min;
            i += min;
            b++;
        }
        return true;
    }
}
