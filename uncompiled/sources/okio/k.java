package okio;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.Socket;

/* loaded from: classes2.dex */
public final class k {
    public static final m a(File file) throws FileNotFoundException {
        return l.b(file);
    }

    public static final m b() {
        return cm2.a();
    }

    public static final c c(m mVar) {
        return cm2.b(mVar);
    }

    public static final d d(n nVar) {
        return cm2.c(nVar);
    }

    public static final boolean e(AssertionError assertionError) {
        return l.c(assertionError);
    }

    public static final m f(File file) throws FileNotFoundException {
        return l.g(file, false, 1, null);
    }

    public static final m g(File file, boolean z) throws FileNotFoundException {
        return l.d(file, z);
    }

    public static final m h(OutputStream outputStream) {
        return l.e(outputStream);
    }

    public static final m i(Socket socket) throws IOException {
        return l.f(socket);
    }

    public static final n k(File file) throws FileNotFoundException {
        return l.h(file);
    }

    public static final n l(InputStream inputStream) {
        return l.i(inputStream);
    }

    public static final n m(Socket socket) throws IOException {
        return l.j(socket);
    }
}
