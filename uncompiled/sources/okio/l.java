package okio;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.Socket;
import java.util.logging.Logger;
import kotlin.text.StringsKt__StringsKt;

/* compiled from: JvmOkio.kt */
/* loaded from: classes2.dex */
public final /* synthetic */ class l {
    public static final Logger a = Logger.getLogger("okio.Okio");

    public static final m b(File file) throws FileNotFoundException {
        fs1.f(file, "$this$appendingSink");
        return k.h(new FileOutputStream(file, true));
    }

    public static final boolean c(AssertionError assertionError) {
        fs1.f(assertionError, "$this$isAndroidGetsocknameError");
        if (assertionError.getCause() != null) {
            String message = assertionError.getMessage();
            return message != null ? StringsKt__StringsKt.M(message, "getsockname failed", false, 2, null) : false;
        }
        return false;
    }

    public static final m d(File file, boolean z) throws FileNotFoundException {
        fs1.f(file, "$this$sink");
        return k.h(new FileOutputStream(file, z));
    }

    public static final m e(OutputStream outputStream) {
        fs1.f(outputStream, "$this$sink");
        return new do2(outputStream, new o());
    }

    public static final m f(Socket socket) throws IOException {
        fs1.f(socket, "$this$sink");
        br3 br3Var = new br3(socket);
        OutputStream outputStream = socket.getOutputStream();
        fs1.e(outputStream, "getOutputStream()");
        return br3Var.sink(new do2(outputStream, br3Var));
    }

    public static /* synthetic */ m g(File file, boolean z, int i, Object obj) throws FileNotFoundException {
        if ((i & 1) != 0) {
            z = false;
        }
        return k.g(file, z);
    }

    public static final n h(File file) throws FileNotFoundException {
        fs1.f(file, "$this$source");
        return k.l(new FileInputStream(file));
    }

    public static final n i(InputStream inputStream) {
        fs1.f(inputStream, "$this$source");
        return new ar1(inputStream, new o());
    }

    public static final n j(Socket socket) throws IOException {
        fs1.f(socket, "$this$source");
        br3 br3Var = new br3(socket);
        InputStream inputStream = socket.getInputStream();
        fs1.e(inputStream, "getInputStream()");
        return br3Var.source(new ar1(inputStream, br3Var));
    }
}
