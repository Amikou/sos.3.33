package okio;

import java.io.EOFException;
import java.io.IOException;
import java.util.Arrays;
import java.util.zip.CRC32;
import java.util.zip.Inflater;

/* compiled from: GzipSource.kt */
/* loaded from: classes2.dex */
public final class i implements n {
    public byte a;
    public final d43 f0;
    public final Inflater g0;
    public final j h0;
    public final CRC32 i0;

    public i(n nVar) {
        fs1.f(nVar, "source");
        d43 d43Var = new d43(nVar);
        this.f0 = d43Var;
        Inflater inflater = new Inflater(true);
        this.g0 = inflater;
        this.h0 = new j((d) d43Var, inflater);
        this.i0 = new CRC32();
    }

    public final void a(String str, int i, int i2) {
        if (i2 == i) {
            return;
        }
        String format = String.format("%s: actual 0x%08x != expected 0x%08x", Arrays.copyOf(new Object[]{str, Integer.valueOf(i2), Integer.valueOf(i)}, 3));
        fs1.e(format, "java.lang.String.format(this, *args)");
        throw new IOException(format);
    }

    public final void b() throws IOException {
        this.f0.A1(10L);
        byte j = this.f0.a.j(3L);
        boolean z = ((j >> 1) & 1) == 1;
        if (z) {
            d(this.f0.a, 0L, 10L);
        }
        a("ID1ID2", 8075, this.f0.readShort());
        this.f0.skip(8L);
        if (((j >> 2) & 1) == 1) {
            this.f0.A1(2L);
            if (z) {
                d(this.f0.a, 0L, 2L);
            }
            long Q = this.f0.a.Q();
            this.f0.A1(Q);
            if (z) {
                d(this.f0.a, 0L, Q);
            }
            this.f0.skip(Q);
        }
        if (((j >> 3) & 1) == 1) {
            long a = this.f0.a((byte) 0);
            if (a != -1) {
                if (z) {
                    d(this.f0.a, 0L, a + 1);
                }
                this.f0.skip(a + 1);
            } else {
                throw new EOFException();
            }
        }
        if (((j >> 4) & 1) == 1) {
            long a2 = this.f0.a((byte) 0);
            if (a2 != -1) {
                if (z) {
                    d(this.f0.a, 0L, a2 + 1);
                }
                this.f0.skip(a2 + 1);
            } else {
                throw new EOFException();
            }
        }
        if (z) {
            a("FHCRC", this.f0.e(), (short) this.i0.getValue());
            this.i0.reset();
        }
    }

    public final void c() throws IOException {
        a("CRC", this.f0.d(), (int) this.i0.getValue());
        a("ISIZE", this.f0.d(), (int) this.g0.getBytesWritten());
    }

    @Override // okio.n, java.io.Closeable, java.lang.AutoCloseable
    public void close() throws IOException {
        this.h0.close();
    }

    public final void d(b bVar, long j, long j2) {
        int i;
        bj3 bj3Var = bVar.a;
        fs1.d(bj3Var);
        while (true) {
            int i2 = bj3Var.c;
            int i3 = bj3Var.b;
            if (j < i2 - i3) {
                break;
            }
            j -= i2 - i3;
            bj3Var = bj3Var.f;
            fs1.d(bj3Var);
        }
        while (j2 > 0) {
            int min = (int) Math.min(bj3Var.c - i, j2);
            this.i0.update(bj3Var.a, (int) (bj3Var.b + j), min);
            j2 -= min;
            bj3Var = bj3Var.f;
            fs1.d(bj3Var);
            j = 0;
        }
    }

    @Override // okio.n
    public long read(b bVar, long j) throws IOException {
        fs1.f(bVar, "sink");
        int i = (j > 0L ? 1 : (j == 0L ? 0 : -1));
        if (!(i >= 0)) {
            throw new IllegalArgumentException(("byteCount < 0: " + j).toString());
        } else if (i == 0) {
            return 0L;
        } else {
            if (this.a == 0) {
                b();
                this.a = (byte) 1;
            }
            if (this.a == 1) {
                long a0 = bVar.a0();
                long read = this.h0.read(bVar, j);
                if (read != -1) {
                    d(bVar, a0, read);
                    return read;
                }
                this.a = (byte) 2;
            }
            if (this.a == 2) {
                c();
                this.a = (byte) 3;
                if (!this.f0.c0()) {
                    throw new IOException("gzip finished without exhausting source");
                }
            }
            return -1L;
        }
    }

    @Override // okio.n
    public o timeout() {
        return this.f0.timeout();
    }
}
