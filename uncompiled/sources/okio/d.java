package okio;

import java.io.IOException;
import java.io.InputStream;
import java.nio.channels.ReadableByteChannel;
import java.nio.charset.Charset;

/* compiled from: BufferedSource.kt */
/* loaded from: classes2.dex */
public interface d extends n, ReadableByteChannel {
    void A1(long j) throws IOException;

    long E1() throws IOException;

    boolean G0(long j, ByteString byteString) throws IOException;

    InputStream G1();

    b I();

    String I0(Charset charset) throws IOException;

    ByteString J(long j) throws IOException;

    ByteString R0() throws IOException;

    boolean V0(long j) throws IOException;

    byte[] Z() throws IOException;

    String a1() throws IOException;

    boolean c0() throws IOException;

    byte[] g1(long j) throws IOException;

    int h0(un2 un2Var) throws IOException;

    String j1() throws IOException;

    void k0(b bVar, long j) throws IOException;

    b o();

    long o0() throws IOException;

    String p0(long j) throws IOException;

    d peek();

    byte readByte() throws IOException;

    void readFully(byte[] bArr) throws IOException;

    int readInt() throws IOException;

    long readLong() throws IOException;

    short readShort() throws IOException;

    void skip(long j) throws IOException;

    long t1(m mVar) throws IOException;
}
