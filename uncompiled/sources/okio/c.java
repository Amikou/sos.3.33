package okio;

import java.io.IOException;
import java.io.OutputStream;
import java.nio.channels.WritableByteChannel;

/* compiled from: BufferedSink.kt */
/* loaded from: classes2.dex */
public interface c extends m, WritableByteChannel {
    c B1(long j) throws IOException;

    c C0(String str) throws IOException;

    OutputStream D1();

    c M0(byte[] bArr, int i, int i2) throws IOException;

    c O() throws IOException;

    c P(int i) throws IOException;

    long P0(n nVar) throws IOException;

    c Q0(long j) throws IOException;

    c V(int i) throws IOException;

    c d0(int i) throws IOException;

    @Override // okio.m, java.io.Flushable
    void flush() throws IOException;

    c l1(byte[] bArr) throws IOException;

    c n0() throws IOException;

    c n1(ByteString byteString) throws IOException;

    b o();
}
