package java8.util;

import java.util.AbstractList;
import java.util.Comparator;
import java.util.ConcurrentModificationException;
import java.util.List;
import sun.misc.Unsafe;

/* compiled from: RASpliterator.java */
/* loaded from: classes2.dex */
public final class q<E> implements s<E> {
    public static final Unsafe f;
    public static final long g;
    public final List<E> a;
    public int b;
    public int c;
    public final AbstractList<E> d;
    public int e;

    static {
        Unsafe unsafe = u.a;
        f = unsafe;
        try {
            g = unsafe.objectFieldOffset(AbstractList.class.getDeclaredField("modCount"));
        } catch (Exception e) {
            throw new Error(e);
        }
    }

    public q(List<E> list, int i, int i2, int i3) {
        this.a = list;
        this.b = i;
        this.c = i2;
        this.d = list instanceof AbstractList ? (AbstractList) list : null;
        this.e = i3;
    }

    public static void o(AbstractList<?> abstractList, int i) {
        if (abstractList != null && q(abstractList) != i) {
            throw new ConcurrentModificationException();
        }
    }

    public static <T> int q(List<T> list) {
        return f.getInt(list, g);
    }

    public static <T> s<T> r(List<T> list) {
        return new q(list, 0, -1, 0);
    }

    @Override // java8.util.s
    public void a(m60<? super E> m60Var) {
        rl2.f(m60Var);
        List<E> list = this.a;
        int p = p();
        this.b = p;
        for (int i = this.b; i < p; i++) {
            try {
                m60Var.accept((E) list.get(i));
            } catch (IndexOutOfBoundsException unused) {
                throw new ConcurrentModificationException();
            }
        }
        o(this.d, this.e);
    }

    @Override // java8.util.s
    public int b() {
        return 16464;
    }

    @Override // java8.util.s
    public s<E> c() {
        int p = p();
        int i = this.b;
        int i2 = (p + i) >>> 1;
        if (i >= i2) {
            return null;
        }
        List<E> list = this.a;
        this.b = i2;
        return new q(list, i, i2, this.e);
    }

    @Override // java8.util.s
    public boolean d(m60<? super E> m60Var) {
        rl2.f(m60Var);
        int p = p();
        int i = this.b;
        if (i < p) {
            this.b = i + 1;
            m60Var.accept((E) this.a.get(i));
            o(this.d, this.e);
            return true;
        }
        return false;
    }

    @Override // java8.util.s
    public Comparator<? super E> f() {
        return t.h(this);
    }

    @Override // java8.util.s
    public boolean h(int i) {
        return t.k(this, i);
    }

    @Override // java8.util.s
    public long i() {
        return t.i(this);
    }

    @Override // java8.util.s
    public long l() {
        return p() - this.b;
    }

    public final int p() {
        List<E> list = this.a;
        int i = this.c;
        if (i < 0) {
            AbstractList<E> abstractList = this.d;
            if (abstractList != null) {
                this.e = q(abstractList);
            }
            int size = list.size();
            this.c = size;
            return size;
        }
        return i;
    }
}
