package java8.util.concurrent;

import java.lang.Thread;
import java.security.AccessControlContext;
import java.security.AccessController;
import java.security.PrivilegedAction;
import java.security.ProtectionDomain;
import java8.util.concurrent.a;

/* compiled from: ForkJoinWorkerThread.java */
/* loaded from: classes2.dex */
public class b extends Thread {
    public final java8.util.concurrent.a a;
    public final a.g f0;

    /* compiled from: ForkJoinWorkerThread.java */
    /* loaded from: classes2.dex */
    public static final class a extends b {
        public static final ThreadGroup g0 = (ThreadGroup) AccessController.doPrivileged(new C0183a());
        public static final AccessControlContext h0 = new AccessControlContext(new ProtectionDomain[]{new ProtectionDomain(null, null)});

        /* compiled from: ForkJoinWorkerThread.java */
        /* renamed from: java8.util.concurrent.b$a$a  reason: collision with other inner class name */
        /* loaded from: classes2.dex */
        public static class C0183a implements PrivilegedAction<ThreadGroup> {
            @Override // java.security.PrivilegedAction
            /* renamed from: a */
            public ThreadGroup run() {
                ThreadGroup threadGroup = Thread.currentThread().getThreadGroup();
                while (true) {
                    ThreadGroup parent = threadGroup.getParent();
                    if (parent == null) {
                        return new ThreadGroup(threadGroup, "InnocuousForkJoinWorkerThreadGroup");
                    }
                    threadGroup = parent;
                }
            }
        }

        public a(java8.util.concurrent.a aVar) {
            super(aVar, ClassLoader.getSystemClassLoader(), g0, h0);
        }

        @Override // java8.util.concurrent.b
        public void a() {
            c.b(this);
        }

        @Override // java.lang.Thread
        public void setContextClassLoader(ClassLoader classLoader) {
            throw new SecurityException("setContextClassLoader");
        }

        @Override // java.lang.Thread
        public void setUncaughtExceptionHandler(Thread.UncaughtExceptionHandler uncaughtExceptionHandler) {
        }
    }

    public b(java8.util.concurrent.a aVar, ClassLoader classLoader) {
        super("aForkJoinWorkerThread");
        c.m(this, classLoader);
        this.a = aVar;
        this.f0 = aVar.C(this);
    }

    public void a() {
    }

    public java8.util.concurrent.a b() {
        return this.a;
    }

    public void c() {
    }

    public void d(Throwable th) {
    }

    @Override // java.lang.Thread, java.lang.Runnable
    public void run() {
        if (this.f0.h == null) {
            Throwable th = null;
            try {
                c();
                this.a.D(this.f0);
                try {
                    d(null);
                } catch (Throwable th2) {
                    th = th2;
                }
            } catch (Throwable th3) {
                th = th3;
                try {
                    d(th);
                } catch (Throwable unused) {
                }
            }
            this.a.h(this, th);
        }
    }

    public b(java8.util.concurrent.a aVar, ClassLoader classLoader, ThreadGroup threadGroup, AccessControlContext accessControlContext) {
        super(threadGroup, "aForkJoinWorkerThread");
        super.setContextClassLoader(classLoader);
        c.n(this, accessControlContext);
        c.b(this);
        this.a = aVar;
        this.f0 = aVar.C(this);
    }
}
