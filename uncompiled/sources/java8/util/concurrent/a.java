package java8.util.concurrent;

import java.lang.Thread;
import java.security.AccessControlContext;
import java.security.AccessController;
import java.security.Permission;
import java.security.Permissions;
import java.security.PrivilegedAction;
import java.security.ProtectionDomain;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.concurrent.AbstractExecutorService;
import java.util.concurrent.Callable;
import java.util.concurrent.Future;
import java.util.concurrent.RejectedExecutionException;
import java.util.concurrent.RunnableFuture;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.locks.LockSupport;
import java8.util.concurrent.ForkJoinTask;
import java8.util.concurrent.b;
import sun.misc.Unsafe;

/* compiled from: ForkJoinPool.java */
/* loaded from: classes2.dex */
public class a extends AbstractExecutorService {
    public static final Class<?> A0;
    public static final c p0;
    public static final RuntimePermission q0;
    public static final a r0;
    public static final int s0;
    public static final int t0;
    public static int u0;
    public static final Unsafe v0;
    public static final long w0;
    public static final long x0;
    public static final int y0;
    public static final int z0;
    public volatile long a;
    public volatile long f0;
    public final long g0;
    public int h0;
    public final int i0;
    public volatile int j0;
    public g[] k0;
    public final String l0;
    public final c m0;
    public final Thread.UncaughtExceptionHandler n0;
    public final fu2<? super a> o0;

    /* compiled from: ForkJoinPool.java */
    /* renamed from: java8.util.concurrent.a$a  reason: collision with other inner class name */
    /* loaded from: classes2.dex */
    public static class C0180a implements PrivilegedAction<a> {
        @Override // java.security.PrivilegedAction
        /* renamed from: a */
        public a run() {
            return new a((byte) 0, null);
        }
    }

    /* compiled from: ForkJoinPool.java */
    /* loaded from: classes2.dex */
    public static final class b implements c {
        public static final AccessControlContext a = a.f(new RuntimePermission("getClassLoader"));

        /* compiled from: ForkJoinPool.java */
        /* renamed from: java8.util.concurrent.a$b$a  reason: collision with other inner class name */
        /* loaded from: classes2.dex */
        public class C0181a implements PrivilegedAction<java8.util.concurrent.b> {
            public final /* synthetic */ a a;

            public C0181a(b bVar, a aVar) {
                this.a = aVar;
            }

            @Override // java.security.PrivilegedAction
            /* renamed from: a */
            public java8.util.concurrent.b run() {
                return new java8.util.concurrent.b(this.a, ClassLoader.getSystemClassLoader());
            }
        }

        public b() {
        }

        @Override // java8.util.concurrent.a.c
        public final java8.util.concurrent.b a(a aVar) {
            return (java8.util.concurrent.b) AccessController.doPrivileged(new C0181a(this, aVar), a);
        }

        public /* synthetic */ b(C0180a c0180a) {
            this();
        }
    }

    /* compiled from: ForkJoinPool.java */
    /* loaded from: classes2.dex */
    public interface c {
        java8.util.concurrent.b a(a aVar);
    }

    /* compiled from: ForkJoinPool.java */
    /* loaded from: classes2.dex */
    public static final class d implements c {
        public static final AccessControlContext a = a.f(a.q0, new RuntimePermission("enableContextClassLoaderOverride"), new RuntimePermission("modifyThreadGroup"), new RuntimePermission("getClassLoader"), new RuntimePermission("setContextClassLoader"));

        /* compiled from: ForkJoinPool.java */
        /* renamed from: java8.util.concurrent.a$d$a  reason: collision with other inner class name */
        /* loaded from: classes2.dex */
        public class C0182a implements PrivilegedAction<java8.util.concurrent.b> {
            public final /* synthetic */ a a;

            public C0182a(d dVar, a aVar) {
                this.a = aVar;
            }

            @Override // java.security.PrivilegedAction
            /* renamed from: a */
            public java8.util.concurrent.b run() {
                return new b.a(this.a);
            }
        }

        public d() {
        }

        @Override // java8.util.concurrent.a.c
        public final java8.util.concurrent.b a(a aVar) {
            return (java8.util.concurrent.b) AccessController.doPrivileged(new C0182a(this, aVar), a);
        }

        public /* synthetic */ d(C0180a c0180a) {
            this();
        }
    }

    /* compiled from: ForkJoinPool.java */
    /* loaded from: classes2.dex */
    public interface e {
        boolean block() throws InterruptedException;

        boolean isReleasable();
    }

    /* compiled from: ForkJoinPool.java */
    /* loaded from: classes2.dex */
    public static final class f {
        public static final f92 a = new f92();
        public static final Unsafe b;
        public static final long c;

        static {
            Unsafe unsafe = java8.util.concurrent.d.a;
            b = unsafe;
            try {
                c = unsafe.objectFieldOffset(f92.class.getDeclaredField("a"));
            } catch (Exception e) {
                throw new ExceptionInInitializerError(e);
            }
        }

        public static void a() {
            b.putIntVolatile(a, c, 0);
        }

        public static void b() {
            b.putOrderedInt(a, c, 0);
        }
    }

    /* compiled from: ForkJoinPool.java */
    /* loaded from: classes2.dex */
    public static final class g {
        public static final Unsafe k;
        public static final long l;
        public static final int m;
        public static final int n;
        public volatile int a;
        public int b;
        public int c;
        public int d;
        public volatile int e;
        public ForkJoinTask<?>[] h;
        public final a i;
        public final java8.util.concurrent.b j;
        public int g = 4096;
        public volatile int f = 4096;

        static {
            Unsafe unsafe = java8.util.concurrent.d.a;
            k = unsafe;
            try {
                l = unsafe.objectFieldOffset(g.class.getDeclaredField("a"));
                m = unsafe.arrayBaseOffset(ForkJoinTask[].class);
                int arrayIndexScale = unsafe.arrayIndexScale(ForkJoinTask[].class);
                if (((arrayIndexScale - 1) & arrayIndexScale) == 0) {
                    n = 31 - Integer.numberOfLeadingZeros(arrayIndexScale);
                    return;
                }
                throw new ExceptionInInitializerError("array index scale not a power of two");
            } catch (Exception e) {
                throw new ExceptionInInitializerError(e);
            }
        }

        public g(a aVar, java8.util.concurrent.b bVar) {
            this.i = aVar;
            this.j = bVar;
        }

        public final void a() {
            while (true) {
                ForkJoinTask<?> i = i();
                if (i == null) {
                    return;
                }
                ForkJoinTask.cancelIgnoringExceptions(i);
            }
        }

        public final ForkJoinTask<?>[] b() {
            int i;
            ForkJoinTask<?>[] forkJoinTaskArr = this.h;
            int length = forkJoinTaskArr != null ? forkJoinTaskArr.length : 0;
            int i2 = length > 0 ? length << 1 : 8192;
            if (i2 >= 8192 && i2 <= 67108864) {
                ForkJoinTask<?>[] forkJoinTaskArr2 = new ForkJoinTask[i2];
                this.h = forkJoinTaskArr2;
                if (forkJoinTaskArr != null && length - 1 > 0) {
                    int i3 = this.g;
                    int i4 = this.f;
                    if (i3 - i4 > 0) {
                        int i5 = i2 - 1;
                        int i6 = i4;
                        do {
                            long j = m + ((i6 & i) << n);
                            Unsafe unsafe = k;
                            ForkJoinTask<?> forkJoinTask = (ForkJoinTask) unsafe.getObjectVolatile(forkJoinTaskArr, j);
                            if (forkJoinTask != null && unsafe.compareAndSwapObject(forkJoinTaskArr, j, forkJoinTask, (Object) null)) {
                                forkJoinTaskArr2[i6 & i5] = forkJoinTask;
                            }
                            i6++;
                        } while (i6 != i3);
                        f.b();
                    }
                }
                return forkJoinTaskArr2;
            }
            throw new RejectedExecutionException("Queue capacity exceeded");
        }

        public final boolean c() {
            Thread.State state;
            java8.util.concurrent.b bVar = this.j;
            return (bVar == null || (state = bVar.getState()) == Thread.State.BLOCKED || state == Thread.State.WAITING || state == Thread.State.TIMED_WAITING) ? false : true;
        }

        public final int d(CountedCompleter<?> countedCompleter, int i) {
            boolean z;
            int i2;
            int length;
            if (countedCompleter != null) {
                int i3 = countedCompleter.status;
                if (i3 >= 0) {
                    while (true) {
                        int i4 = this.f;
                        int i5 = this.g;
                        ForkJoinTask<?>[] forkJoinTaskArr = this.h;
                        if (forkJoinTaskArr != null && i4 != i5 && (length = forkJoinTaskArr.length) > 0) {
                            int i6 = i5 - 1;
                            long j = (((length - 1) & i6) << n) + m;
                            ForkJoinTask forkJoinTask = (ForkJoinTask) k.getObject(forkJoinTaskArr, j);
                            if (forkJoinTask instanceof CountedCompleter) {
                                CountedCompleter<?> countedCompleter2 = (CountedCompleter) forkJoinTask;
                                CountedCompleter<?> countedCompleter3 = countedCompleter2;
                                while (true) {
                                    if (countedCompleter3 != countedCompleter) {
                                        countedCompleter3 = countedCompleter3.completer;
                                        if (countedCompleter3 == null) {
                                            break;
                                        }
                                    } else if (k.compareAndSwapObject(forkJoinTaskArr, j, countedCompleter2, (Object) null)) {
                                        this.g = i6;
                                        f.b();
                                        countedCompleter2.doExec();
                                        z = true;
                                    }
                                }
                            }
                        }
                        z = false;
                        i2 = countedCompleter.status;
                        if (i2 < 0 || !z || (i != 0 && i - 1 == 0)) {
                            break;
                        }
                    }
                    return i2;
                }
                return i3;
            }
            return 0;
        }

        public final void e(int i) {
            int i2;
            int length;
            do {
                int i3 = 0;
                while (true) {
                    int i4 = this.f;
                    int i5 = this.g;
                    ForkJoinTask<?>[] forkJoinTaskArr = this.h;
                    if (forkJoinTaskArr == null || (i2 = i4 - i5) >= 0 || (length = forkJoinTaskArr.length) <= 0) {
                        return;
                    }
                    int i6 = i4 + 1;
                    ForkJoinTask forkJoinTask = (ForkJoinTask) a.m(forkJoinTaskArr, ((i4 & (length - 1)) << n) + m, null);
                    if (forkJoinTask != null) {
                        this.f = i6;
                        forkJoinTask.doExec();
                        if (i != 0 && (i3 = i3 + 1) == i) {
                            return;
                        }
                    }
                }
            } while (i2 != -1);
        }

        public final void f(int i) {
            int length;
            while (true) {
                int i2 = this.f;
                int i3 = this.g;
                ForkJoinTask<?>[] forkJoinTaskArr = this.h;
                if (forkJoinTaskArr == null || i2 == i3 || (length = forkJoinTaskArr.length) <= 0) {
                    return;
                }
                int i4 = i3 - 1;
                ForkJoinTask forkJoinTask = (ForkJoinTask) a.m(forkJoinTaskArr, (((length - 1) & i4) << n) + m, null);
                if (forkJoinTask == null) {
                    return;
                }
                this.g = i4;
                f.b();
                forkJoinTask.doExec();
                if (i != 0 && i - 1 == 0) {
                    return;
                }
            }
        }

        public final ForkJoinTask<?> g() {
            return (this.d & 65536) != 0 ? i() : j();
        }

        public final ForkJoinTask<?> h() {
            int length;
            ForkJoinTask<?>[] forkJoinTaskArr = this.h;
            if (forkJoinTaskArr == null || (length = forkJoinTaskArr.length) <= 0) {
                return null;
            }
            return forkJoinTaskArr[(length - 1) & ((this.d & 65536) != 0 ? this.f : this.g - 1)];
        }

        public final ForkJoinTask<?> i() {
            int i;
            int length;
            while (true) {
                int i2 = this.f;
                int i3 = this.g;
                ForkJoinTask<?>[] forkJoinTaskArr = this.h;
                if (forkJoinTaskArr == null || (i = i2 - i3) >= 0 || (length = forkJoinTaskArr.length) <= 0) {
                    return null;
                }
                long j = (((length - 1) & i2) << n) + m;
                Unsafe unsafe = k;
                ForkJoinTask<?> forkJoinTask = (ForkJoinTask) unsafe.getObjectVolatile(forkJoinTaskArr, j);
                int i4 = i2 + 1;
                if (i2 == this.f) {
                    if (forkJoinTask != null) {
                        if (unsafe.compareAndSwapObject(forkJoinTaskArr, j, forkJoinTask, (Object) null)) {
                            this.f = i4;
                            return forkJoinTask;
                        }
                    } else if (i == -1) {
                        return null;
                    }
                }
            }
        }

        public final ForkJoinTask<?> j() {
            int length;
            int i = this.f;
            int i2 = this.g;
            ForkJoinTask<?>[] forkJoinTaskArr = this.h;
            if (forkJoinTaskArr == null || i == i2 || (length = forkJoinTaskArr.length) <= 0) {
                return null;
            }
            int i3 = i2 - 1;
            long j = (((length - 1) & i3) << n) + m;
            Unsafe unsafe = k;
            ForkJoinTask<?> forkJoinTask = (ForkJoinTask) unsafe.getObject(forkJoinTaskArr, j);
            if (forkJoinTask == null || !unsafe.compareAndSwapObject(forkJoinTaskArr, j, forkJoinTask, (Object) null)) {
                return null;
            }
            this.g = i3;
            f.b();
            return forkJoinTask;
        }

        public final void k(ForkJoinTask<?> forkJoinTask) {
            int length;
            int i = this.g;
            ForkJoinTask<?>[] forkJoinTaskArr = this.h;
            if (forkJoinTaskArr == null || (length = forkJoinTaskArr.length) <= 0) {
                return;
            }
            long j = (((length - 1) & i) << n) + m;
            a aVar = this.i;
            this.g = i + 1;
            k.putOrderedObject(forkJoinTaskArr, j, forkJoinTask);
            int i2 = this.f - i;
            if (i2 == 0 && aVar != null) {
                f.a();
                aVar.E();
            } else if (i2 + length == 1) {
                b();
            }
        }

        public final int l() {
            int i = this.f - this.g;
            if (i >= 0) {
                return 0;
            }
            return -i;
        }

        /* JADX WARN: Code restructure failed: missing block: B:11:0x0017, code lost:
            if (r12.length <= 0) goto L44;
         */
        /* JADX WARN: Code restructure failed: missing block: B:12:0x0019, code lost:
            r15 = r10 - 1;
            r13 = (((r0 - 1) & r15) << java8.util.concurrent.a.g.n) + java8.util.concurrent.a.g.m;
            r0 = (java8.util.concurrent.ForkJoinTask) java8.util.concurrent.a.g.k.getObject(r12, r13);
         */
        /* JADX WARN: Code restructure failed: missing block: B:13:0x0031, code lost:
            if ((r0 instanceof java8.util.concurrent.CountedCompleter) == false) goto L44;
         */
        /* JADX WARN: Code restructure failed: missing block: B:14:0x0033, code lost:
            r17 = (java8.util.concurrent.CountedCompleter) r0;
            r0 = r17;
         */
        /* JADX WARN: Code restructure failed: missing block: B:15:0x0039, code lost:
            if (r0 == r20) goto L18;
         */
        /* JADX WARN: Code restructure failed: missing block: B:16:0x003b, code lost:
            r0 = r0.completer;
         */
        /* JADX WARN: Code restructure failed: missing block: B:17:0x003d, code lost:
            if (r0 != null) goto L14;
         */
        /* JADX WARN: Code restructure failed: missing block: B:19:0x0040, code lost:
            r11 = java8.util.concurrent.a.g.k;
            r4 = java8.util.concurrent.a.g.l;
            r21 = r9;
         */
        /* JADX WARN: Code restructure failed: missing block: B:20:0x0057, code lost:
            if (r11.compareAndSwapInt(r19, r4, 0, 1) == false) goto L43;
         */
        /* JADX WARN: Code restructure failed: missing block: B:22:0x005b, code lost:
            if (r19.g != r10) goto L42;
         */
        /* JADX WARN: Code restructure failed: missing block: B:24:0x005f, code lost:
            if (r19.h != r12) goto L42;
         */
        /* JADX WARN: Code restructure failed: missing block: B:25:0x0061, code lost:
            r0 = r11;
         */
        /* JADX WARN: Code restructure failed: missing block: B:26:0x006b, code lost:
            if (r11.compareAndSwapObject(r12, r13, r17, (java.lang.Object) null) == false) goto L41;
         */
        /* JADX WARN: Code restructure failed: missing block: B:27:0x006d, code lost:
            r19.g = r15;
            r1 = true;
         */
        /* JADX WARN: Code restructure failed: missing block: B:28:0x0071, code lost:
            r0 = r11;
         */
        /* JADX WARN: Code restructure failed: missing block: B:29:0x0072, code lost:
            r1 = false;
         */
        /* JADX WARN: Code restructure failed: missing block: B:30:0x0073, code lost:
            r0.putOrderedInt(r19, r4, 0);
         */
        /* JADX WARN: Code restructure failed: missing block: B:31:0x0077, code lost:
            if (r1 == false) goto L31;
         */
        /* JADX WARN: Code restructure failed: missing block: B:32:0x0079, code lost:
            r17.doExec();
         */
        /* JADX WARN: Code restructure failed: missing block: B:33:0x007d, code lost:
            r21 = r9;
         */
        /* JADX WARN: Code restructure failed: missing block: B:34:0x007f, code lost:
            r1 = false;
         */
        /* JADX WARN: Code restructure failed: missing block: B:35:0x0081, code lost:
            r0 = r20.status;
         */
        /* JADX WARN: Code restructure failed: missing block: B:36:0x0083, code lost:
            if (r0 < 0) goto L40;
         */
        /* JADX WARN: Code restructure failed: missing block: B:37:0x0085, code lost:
            if (r1 == false) goto L39;
         */
        /* JADX WARN: Code restructure failed: missing block: B:38:0x0087, code lost:
            if (r21 == 0) goto L38;
         */
        /* JADX WARN: Code restructure failed: missing block: B:39:0x0089, code lost:
            r9 = r21 - 1;
         */
        /* JADX WARN: Code restructure failed: missing block: B:40:0x008b, code lost:
            if (r9 != 0) goto L6;
         */
        /* JADX WARN: Code restructure failed: missing block: B:50:?, code lost:
            return r0;
         */
        /* JADX WARN: Code restructure failed: missing block: B:5:0x0008, code lost:
            if (r0 >= 0) goto L5;
         */
        /* JADX WARN: Code restructure failed: missing block: B:6:0x000a, code lost:
            r9 = r21;
         */
        /* JADX WARN: Code restructure failed: missing block: B:7:0x000c, code lost:
            r0 = r19.f;
            r10 = r19.g;
            r12 = r19.h;
         */
        /* JADX WARN: Code restructure failed: missing block: B:8:0x0012, code lost:
            if (r12 == null) goto L44;
         */
        /* JADX WARN: Code restructure failed: missing block: B:9:0x0014, code lost:
            if (r0 == r10) goto L44;
         */
        /*
            Code decompiled incorrectly, please refer to instructions dump.
            To view partially-correct code enable 'Show inconsistent code' option in preferences
        */
        public final int m(java8.util.concurrent.CountedCompleter<?> r20, int r21) {
            /*
                r19 = this;
                r6 = r19
                r7 = r20
                if (r7 == 0) goto L8f
                int r0 = r7.status
                if (r0 < 0) goto L8d
            La:
                r9 = r21
            Lc:
                int r0 = r6.f
                int r10 = r6.g
                java8.util.concurrent.ForkJoinTask<?>[] r12 = r6.h
                if (r12 == 0) goto L7d
                if (r0 == r10) goto L7d
                int r0 = r12.length
                if (r0 <= 0) goto L7d
                int r0 = r0 + (-1)
                int r15 = r10 + (-1)
                r0 = r0 & r15
                long r0 = (long) r0
                int r2 = java8.util.concurrent.a.g.n
                long r0 = r0 << r2
                int r2 = java8.util.concurrent.a.g.m
                long r2 = (long) r2
                long r13 = r0 + r2
                sun.misc.Unsafe r0 = java8.util.concurrent.a.g.k
                java.lang.Object r0 = r0.getObject(r12, r13)
                java8.util.concurrent.ForkJoinTask r0 = (java8.util.concurrent.ForkJoinTask) r0
                boolean r1 = r0 instanceof java8.util.concurrent.CountedCompleter
                if (r1 == 0) goto L7d
                r17 = r0
                java8.util.concurrent.CountedCompleter r17 = (java8.util.concurrent.CountedCompleter) r17
                r0 = r17
            L39:
                if (r0 == r7) goto L40
                java8.util.concurrent.CountedCompleter<?> r0 = r0.completer
                if (r0 != 0) goto L39
                goto L7d
            L40:
                sun.misc.Unsafe r11 = java8.util.concurrent.a.g.k
                long r4 = java8.util.concurrent.a.g.l
                r16 = 0
                r18 = 1
                r0 = r11
                r1 = r19
                r2 = r4
                r21 = r9
                r8 = r4
                r4 = r16
                r5 = r18
                boolean r0 = r0.compareAndSwapInt(r1, r2, r4, r5)
                if (r0 == 0) goto L7f
                int r0 = r6.g
                if (r0 != r10) goto L71
                java8.util.concurrent.ForkJoinTask<?>[] r0 = r6.h
                if (r0 != r12) goto L71
                r16 = 0
                r0 = r11
                r10 = r15
                r15 = r17
                boolean r1 = r11.compareAndSwapObject(r12, r13, r15, r16)
                if (r1 == 0) goto L72
                r6.g = r10
                r1 = 1
                goto L73
            L71:
                r0 = r11
            L72:
                r1 = 0
            L73:
                r2 = 0
                r0.putOrderedInt(r6, r8, r2)
                if (r1 == 0) goto L81
                r17.doExec()
                goto L81
            L7d:
                r21 = r9
            L7f:
                r2 = 0
                r1 = r2
            L81:
                int r0 = r7.status
                if (r0 < 0) goto L8d
                if (r1 == 0) goto L8d
                if (r21 == 0) goto La
                int r9 = r21 + (-1)
                if (r9 != 0) goto Lc
            L8d:
                r8 = r0
                goto L91
            L8f:
                r2 = 0
                r8 = r2
            L91:
                return r8
            */
            throw new UnsupportedOperationException("Method not decompiled: java8.util.concurrent.a.g.m(java8.util.concurrent.CountedCompleter, int):int");
        }

        public final boolean n() {
            return k.compareAndSwapInt(this, l, 0, 1);
        }

        public final void o(ForkJoinTask<?> forkJoinTask) {
            ForkJoinTask<?>[] forkJoinTaskArr;
            int length;
            int i;
            int i2 = this.f;
            int i3 = this.g;
            if (i2 - i3 >= 0 || (forkJoinTaskArr = this.h) == null || (length = forkJoinTaskArr.length) <= 0) {
                return;
            }
            int i4 = length - 1;
            int i5 = i3 - 1;
            int i6 = i5;
            while (true) {
                long j = ((i6 & i4) << n) + m;
                Unsafe unsafe = k;
                ForkJoinTask<?> forkJoinTask2 = (ForkJoinTask) unsafe.getObject(forkJoinTaskArr, j);
                if (forkJoinTask2 == null) {
                    return;
                }
                if (forkJoinTask2 == forkJoinTask) {
                    if (unsafe.compareAndSwapObject(forkJoinTaskArr, j, forkJoinTask2, (Object) null)) {
                        this.g = i5;
                        while (i6 != i5) {
                            int i7 = i6 + 1;
                            int i8 = n;
                            long j2 = ((i7 & i4) << i8) + m;
                            Unsafe unsafe2 = k;
                            unsafe2.putObjectVolatile(forkJoinTaskArr, j2, (Object) null);
                            unsafe2.putOrderedObject(forkJoinTaskArr, ((i6 & i4) << i8) + i, (ForkJoinTask) unsafe2.getObject(forkJoinTaskArr, j2));
                            i6 = i7;
                        }
                        f.b();
                        forkJoinTask2.doExec();
                        return;
                    }
                    return;
                }
                i6--;
            }
        }

        public final boolean p(ForkJoinTask<?> forkJoinTask) {
            int length;
            boolean z = true;
            int i = this.g - 1;
            ForkJoinTask<?>[] forkJoinTaskArr = this.h;
            if (forkJoinTaskArr == null || (length = forkJoinTaskArr.length) <= 0) {
                return false;
            }
            long j = (((length - 1) & i) << n) + m;
            Unsafe unsafe = k;
            if (((ForkJoinTask) unsafe.getObject(forkJoinTaskArr, j)) == forkJoinTask) {
                long j2 = l;
                if (unsafe.compareAndSwapInt(this, j2, 0, 1)) {
                    if (this.g == i + 1 && this.h == forkJoinTaskArr && unsafe.compareAndSwapObject(forkJoinTaskArr, j, forkJoinTask, (Object) null)) {
                        this.g = i;
                    } else {
                        z = false;
                    }
                    unsafe.putOrderedInt(this, j2, 0);
                    return z;
                }
                return false;
            }
            return false;
        }

        public final boolean q(ForkJoinTask<?> forkJoinTask) {
            int length;
            int i = this.f;
            int i2 = this.g;
            ForkJoinTask<?>[] forkJoinTaskArr = this.h;
            if (forkJoinTaskArr == null || i == i2 || (length = forkJoinTaskArr.length) <= 0) {
                return false;
            }
            int i3 = i2 - 1;
            if (k.compareAndSwapObject(forkJoinTaskArr, (((length - 1) & i3) << n) + m, forkJoinTask, (Object) null)) {
                this.g = i3;
                f.b();
                return true;
            }
            return false;
        }
    }

    /*  JADX ERROR: JadxRuntimeException in pass: BlockProcessor
        jadx.core.utils.exceptions.JadxRuntimeException: Unreachable block: B:13:0x007d
        	at jadx.core.dex.visitors.blocks.BlockProcessor.checkForUnreachableBlocks(BlockProcessor.java:81)
        	at jadx.core.dex.visitors.blocks.BlockProcessor.processBlocksTree(BlockProcessor.java:47)
        	at jadx.core.dex.visitors.blocks.BlockProcessor.visit(BlockProcessor.java:39)
        */
    static {
        /*
            sun.misc.Unsafe r0 = java8.util.concurrent.d.a
            java8.util.concurrent.a.v0 = r0
            java.lang.Class<java8.util.concurrent.a> r1 = java8.util.concurrent.a.class
            java.lang.String r2 = "a"
            java.lang.reflect.Field r1 = r1.getDeclaredField(r2)     // Catch: java.lang.Exception -> L8c
            long r1 = r0.objectFieldOffset(r1)     // Catch: java.lang.Exception -> L8c
            java8.util.concurrent.a.w0 = r1     // Catch: java.lang.Exception -> L8c
            java.lang.Class<java8.util.concurrent.a> r1 = java8.util.concurrent.a.class
            java.lang.String r2 = "j0"
            java.lang.reflect.Field r1 = r1.getDeclaredField(r2)     // Catch: java.lang.Exception -> L8c
            long r1 = r0.objectFieldOffset(r1)     // Catch: java.lang.Exception -> L8c
            java8.util.concurrent.a.x0 = r1     // Catch: java.lang.Exception -> L8c
            java.lang.Class<java8.util.concurrent.ForkJoinTask[]> r1 = java8.util.concurrent.ForkJoinTask[].class
            int r1 = r0.arrayBaseOffset(r1)     // Catch: java.lang.Exception -> L8c
            java8.util.concurrent.a.y0 = r1     // Catch: java.lang.Exception -> L8c
            java.lang.Class<java8.util.concurrent.ForkJoinTask[]> r1 = java8.util.concurrent.ForkJoinTask[].class
            int r0 = r0.arrayIndexScale(r1)     // Catch: java.lang.Exception -> L8c
            int r1 = r0 + (-1)
            r1 = r1 & r0
            if (r1 != 0) goto L84
            int r0 = java.lang.Integer.numberOfLeadingZeros(r0)     // Catch: java.lang.Exception -> L8c
            int r0 = 31 - r0
            java8.util.concurrent.a.z0 = r0     // Catch: java.lang.Exception -> L8c
            java.lang.Class<java.util.concurrent.locks.LockSupport> r0 = java.util.concurrent.locks.LockSupport.class
            r0 = 256(0x100, float:3.59E-43)
            java.lang.String r1 = "java.util.concurrent.ForkJoinPool.common.maximumSpares"
            java.lang.String r1 = java.lang.System.getProperty(r1)     // Catch: java.lang.Exception -> L4b
            if (r1 == 0) goto L4b
            int r0 = java.lang.Integer.parseInt(r1)     // Catch: java.lang.Exception -> L4b
        L4b:
            java8.util.concurrent.a.t0 = r0
            java8.util.concurrent.a$b r0 = new java8.util.concurrent.a$b
            r1 = 0
            r0.<init>(r1)
            java8.util.concurrent.a.p0 = r0
            java.lang.RuntimePermission r0 = new java.lang.RuntimePermission
            java.lang.String r2 = "modifyThread"
            r0.<init>(r2)
            java8.util.concurrent.a.q0 = r0
            java8.util.concurrent.a$a r0 = new java8.util.concurrent.a$a
            r0.<init>()
            java.lang.Object r0 = java.security.AccessController.doPrivileged(r0)
            java8.util.concurrent.a r0 = (java8.util.concurrent.a) r0
            java8.util.concurrent.a.r0 = r0
            int r0 = r0.j0
            r2 = 65535(0xffff, float:9.1834E-41)
            r0 = r0 & r2
            r2 = 1
            int r0 = java.lang.Math.max(r0, r2)
            java8.util.concurrent.a.s0 = r0
            java.lang.Class<java8.util.concurrent.CompletableFuture$b> r0 = java8.util.concurrent.CompletableFuture.b.class
            java8.util.concurrent.a.A0 = r0
            goto L83
        L7d:
            r0 = move-exception
            java8.util.concurrent.a.A0 = r1
            throw r0
        L81:
            java8.util.concurrent.a.A0 = r1
        L83:
            return
        L84:
            java.lang.ExceptionInInitializerError r0 = new java.lang.ExceptionInInitializerError     // Catch: java.lang.Exception -> L8c
            java.lang.String r1 = "array index scale not a power of two"
            r0.<init>(r1)     // Catch: java.lang.Exception -> L8c
            throw r0     // Catch: java.lang.Exception -> L8c
        L8c:
            r0 = move-exception
            java.lang.ExceptionInInitializerError r1 = new java.lang.ExceptionInInitializerError
            r1.<init>(r0)
            throw r1
        */
        throw new UnsupportedOperationException("Method not decompiled: java8.util.concurrent.a.<clinit>():void");
    }

    public /* synthetic */ a(byte b2, C0180a c0180a) {
        this(b2);
    }

    public static void B() {
        r0.b(Long.MAX_VALUE, TimeUnit.NANOSECONDS);
    }

    public static void c() {
        SecurityManager securityManager = System.getSecurityManager();
        if (securityManager != null) {
            securityManager.checkPermission(q0);
        }
    }

    public static a d() {
        return r0;
    }

    public static g e() {
        g[] gVarArr;
        int length;
        a aVar = r0;
        int c2 = java8.util.concurrent.c.c();
        if (aVar == null || (gVarArr = aVar.k0) == null || (length = gVarArr.length) <= 0) {
            return null;
        }
        return gVarArr[c2 & (length - 1) & 126];
    }

    public static AccessControlContext f(Permission... permissionArr) {
        Permissions permissions = new Permissions();
        for (Permission permission : permissionArr) {
            permissions.add(permission);
        }
        return new AccessControlContext(new ProtectionDomain[]{new ProtectionDomain(null, permissions)});
    }

    public static long l(Object obj, long j, long j2) {
        Unsafe unsafe;
        long longVolatile;
        do {
            unsafe = v0;
            longVolatile = unsafe.getLongVolatile(obj, j);
        } while (!unsafe.compareAndSwapLong(obj, j, longVolatile, longVolatile + j2));
        return longVolatile;
    }

    public static Object m(Object obj, long j, Object obj2) {
        Unsafe unsafe;
        Object objectVolatile;
        do {
            unsafe = v0;
            objectVolatile = unsafe.getObjectVolatile(obj, j);
        } while (!unsafe.compareAndSwapObject(obj, j, objectVolatile, obj2));
        return objectVolatile;
    }

    public static int n() {
        return s0;
    }

    public static int p() {
        java8.util.concurrent.b bVar;
        a aVar;
        g gVar;
        Thread currentThread = Thread.currentThread();
        int i = 0;
        if (!(currentThread instanceof java8.util.concurrent.b) || (aVar = (bVar = (java8.util.concurrent.b) currentThread).a) == null || (gVar = bVar.f0) == null) {
            return 0;
        }
        int i2 = aVar.j0 & 65535;
        int i3 = ((int) (aVar.a >> 48)) + i2;
        int i4 = gVar.g - gVar.f;
        int i5 = i2 >>> 1;
        if (i3 <= i5) {
            int i6 = i5 >>> 1;
            if (i3 > i6) {
                i = 1;
            } else {
                int i7 = i6 >>> 1;
                i = i3 > i7 ? 2 : i3 > (i7 >>> 1) ? 4 : 8;
            }
        }
        return i4 - i;
    }

    /* JADX WARN: Removed duplicated region for block: B:19:0x0031 A[LOOP:0: B:19:0x0031->B:51:0x0031, LOOP_START] */
    /* JADX WARN: Removed duplicated region for block: B:57:? A[RETURN, SYNTHETIC] */
    /*
        Code decompiled incorrectly, please refer to instructions dump.
        To view partially-correct code enable 'Show inconsistent code' option in preferences
    */
    public static void q(java.util.concurrent.Executor r10, java8.util.concurrent.a.e r11) {
        /*
            if (r11 == 0) goto L7c
            boolean r0 = r10 instanceof java8.util.concurrent.a
            if (r0 == 0) goto L7c
            java8.util.concurrent.a r10 = (java8.util.concurrent.a) r10
            java.lang.Thread r0 = java.lang.Thread.currentThread()
            boolean r1 = r0 instanceof java8.util.concurrent.b
            if (r1 == 0) goto L19
            java8.util.concurrent.b r0 = (java8.util.concurrent.b) r0
            java8.util.concurrent.a r1 = r0.a
            if (r1 != r10) goto L19
            java8.util.concurrent.a$g r10 = r0.f0
            goto L2f
        L19:
            int r0 = java8.util.concurrent.c.c()
            if (r0 == 0) goto L2e
            java8.util.concurrent.a$g[] r10 = r10.k0
            if (r10 == 0) goto L2e
            int r1 = r10.length
            if (r1 <= 0) goto L2e
            int r1 = r1 + (-1)
            r0 = r0 & r1
            r0 = r0 & 126(0x7e, float:1.77E-43)
            r10 = r10[r0]
            goto L2f
        L2e:
            r10 = 0
        L2f:
            if (r10 == 0) goto L7c
        L31:
            int r0 = r10.f
            int r1 = r10.g
            java8.util.concurrent.ForkJoinTask<?>[] r3 = r10.h
            if (r3 == 0) goto L7c
            int r1 = r0 - r1
            if (r1 >= 0) goto L7c
            int r2 = r3.length
            if (r2 <= 0) goto L7c
            int r2 = r2 + (-1)
            r2 = r2 & r0
            long r4 = (long) r2
            int r2 = java8.util.concurrent.a.z0
            long r4 = r4 << r2
            int r2 = java8.util.concurrent.a.y0
            long r6 = (long) r2
            long r4 = r4 + r6
            sun.misc.Unsafe r2 = java8.util.concurrent.a.v0
            java.lang.Object r6 = r2.getObjectVolatile(r3, r4)
            r8 = r6
            java8.util.concurrent.ForkJoinTask r8 = (java8.util.concurrent.ForkJoinTask) r8
            boolean r6 = r11.isReleasable()
            if (r6 == 0) goto L5b
            goto L7c
        L5b:
            int r9 = r0 + 1
            int r6 = r10.f
            if (r0 != r6) goto L31
            if (r8 != 0) goto L67
            r0 = -1
            if (r1 != r0) goto L31
            goto L7c
        L67:
            boolean r0 = t(r8)
            if (r0 != 0) goto L6e
            goto L7c
        L6e:
            r7 = 0
            r6 = r8
            boolean r0 = r2.compareAndSwapObject(r3, r4, r6, r7)
            if (r0 == 0) goto L31
            r10.f = r9
            r8.doExec()
            goto L31
        L7c:
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: java8.util.concurrent.a.q(java.util.concurrent.Executor, java8.util.concurrent.a$e):void");
    }

    public static boolean t(ForkJoinTask<?> forkJoinTask) {
        Class<?> cls;
        if (forkJoinTask == null || (cls = A0) == null) {
            return false;
        }
        return cls.isAssignableFrom(forkJoinTask.getClass());
    }

    public static void v(e eVar) throws InterruptedException {
        java8.util.concurrent.b bVar;
        a aVar;
        g gVar;
        Thread currentThread = Thread.currentThread();
        if ((currentThread instanceof java8.util.concurrent.b) && (aVar = (bVar = (java8.util.concurrent.b) currentThread).a) != null && (gVar = bVar.f0) != null) {
            while (!eVar.isReleasable()) {
                int J = aVar.J(gVar);
                if (J != 0) {
                    do {
                        try {
                            if (eVar.isReleasable()) {
                                break;
                            }
                        } finally {
                            l(aVar, w0, J <= 0 ? 0L : 281474976710656L);
                        }
                    } while (!eVar.block());
                    return;
                }
            }
            return;
        }
        while (!eVar.isReleasable() && !eVar.block()) {
        }
    }

    public static Object w(String str) throws Exception {
        String property = System.getProperty(str);
        if (property == null) {
            return null;
        }
        return ClassLoader.getSystemClassLoader().loadClass(property).getConstructor(new Class[0]).newInstance(new Object[0]);
    }

    public static final synchronized int x() {
        int i;
        synchronized (a.class) {
            i = u0 + 1;
            u0 = i;
        }
        return i;
    }

    public ForkJoinTask<?> A() {
        return z(true);
    }

    public final g C(java8.util.concurrent.b bVar) {
        int i;
        int length;
        bVar.setDaemon(true);
        Thread.UncaughtExceptionHandler uncaughtExceptionHandler = this.n0;
        if (uncaughtExceptionHandler != null) {
            bVar.setUncaughtExceptionHandler(uncaughtExceptionHandler);
        }
        g gVar = new g(this, bVar);
        int i2 = this.j0 & 65536;
        String str = this.l0;
        if (str != null) {
            synchronized (str) {
                g[] gVarArr = this.k0;
                int i3 = this.h0 - 1640531527;
                this.h0 = i3;
                i = 0;
                if (gVarArr != null && (length = gVarArr.length) > 1) {
                    int i4 = length - 1;
                    int i5 = i3 & i4;
                    int i6 = ((i3 << 1) | 1) & i4;
                    int i7 = length >>> 1;
                    while (true) {
                        g gVar2 = gVarArr[i6];
                        if (gVar2 == null || gVar2.a == 1073741824) {
                            break;
                        }
                        i7--;
                        if (i7 == 0) {
                            i6 = length | 1;
                            break;
                        }
                        i6 = (i6 + 2) & i4;
                    }
                    int i8 = i2 | i6 | (i3 & 1073610752);
                    gVar.d = i8;
                    gVar.a = i8;
                    if (i6 < length) {
                        gVarArr[i6] = gVar;
                    } else {
                        int i9 = length << 1;
                        g[] gVarArr2 = new g[i9];
                        gVarArr2[i6] = gVar;
                        int i10 = i9 - 1;
                        while (i < length) {
                            g gVar3 = gVarArr[i];
                            if (gVar3 != null) {
                                gVarArr2[gVar3.d & i10 & 126] = gVar3;
                            }
                            int i11 = i + 1;
                            if (i11 >= length) {
                                break;
                            }
                            gVarArr2[i11] = gVarArr[i11];
                            i = i11 + 1;
                        }
                        this.k0 = gVarArr2;
                    }
                    i = i5;
                }
            }
            bVar.setName(str.concat(Integer.toString(i)));
        }
        return gVar;
    }

    /* JADX WARN: Code restructure failed: missing block: B:49:0x00c2, code lost:
        r10 = r24.a;
     */
    /* JADX WARN: Code restructure failed: missing block: B:50:0x00ce, code lost:
        if (r10 < 0) goto L58;
     */
    /* JADX WARN: Code restructure failed: missing block: B:51:0x00d0, code lost:
        r15 = (r10 + 65536) | Integer.MIN_VALUE;
        r24.a = r15;
     */
    /* JADX WARN: Code restructure failed: missing block: B:52:0x00d9, code lost:
        r4 = r23.a;
        r24.b = (int) r4;
     */
    /* JADX WARN: Code restructure failed: missing block: B:53:0x00f1, code lost:
        if (java8.util.concurrent.a.v0.compareAndSwapLong(r23, java8.util.concurrent.a.w0, r4, ((r4 - 281474976710656L) & (-4294967296L)) | (r15 & 4294967295L)) == false) goto L54;
     */
    /* JADX WARN: Code restructure failed: missing block: B:55:0x00f4, code lost:
        r15 = r24.b;
        r24.e = java8.util.concurrent.ForkJoinTask.CANCELLED;
        r2 = 0;
     */
    /* JADX WARN: Code restructure failed: missing block: B:57:0x00fd, code lost:
        if (r24.a < 0) goto L61;
     */
    /* JADX WARN: Code restructure failed: missing block: B:58:0x00ff, code lost:
        r24.e = 0;
     */
    /* JADX WARN: Code restructure failed: missing block: B:60:0x0106, code lost:
        r0 = r23.j0;
     */
    /* JADX WARN: Code restructure failed: missing block: B:61:0x0108, code lost:
        if (r0 >= 0) goto L63;
     */
    /* JADX WARN: Code restructure failed: missing block: B:62:0x010a, code lost:
        return;
     */
    /* JADX WARN: Code restructure failed: missing block: B:63:0x010b, code lost:
        r4 = r23.a;
        r1 = (65535 & r0) + ((int) (r4 >> 48));
     */
    /* JADX WARN: Code restructure failed: missing block: B:64:0x0117, code lost:
        if (r1 > 0) goto L94;
     */
    /* JADX WARN: Code restructure failed: missing block: B:66:0x011c, code lost:
        if ((r0 & 262144) == 0) goto L94;
     */
    /* JADX WARN: Code restructure failed: missing block: B:67:0x011e, code lost:
        r6 = false;
     */
    /* JADX WARN: Code restructure failed: missing block: B:68:0x0123, code lost:
        if (L(false, false) == false) goto L69;
     */
    /* JADX WARN: Code restructure failed: missing block: B:69:0x0125, code lost:
        return;
     */
    /* JADX WARN: Code restructure failed: missing block: B:70:0x0126, code lost:
        r6 = false;
     */
    /* JADX WARN: Code restructure failed: missing block: B:71:0x0127, code lost:
        r17 = r2 + 1;
     */
    /* JADX WARN: Code restructure failed: missing block: B:72:0x012d, code lost:
        if ((r17 & 1) != 0) goto L75;
     */
    /* JADX WARN: Code restructure failed: missing block: B:73:0x012f, code lost:
        java.lang.Thread.interrupted();
     */
    /* JADX WARN: Code restructure failed: missing block: B:75:0x0135, code lost:
        if (r1 > 0) goto L90;
     */
    /* JADX WARN: Code restructure failed: missing block: B:76:0x0137, code lost:
        if (r15 == 0) goto L90;
     */
    /* JADX WARN: Code restructure failed: missing block: B:78:0x013a, code lost:
        if (r10 != ((int) r4)) goto L90;
     */
    /* JADX WARN: Code restructure failed: missing block: B:79:0x013c, code lost:
        r0 = r23.g0 + java.lang.System.currentTimeMillis();
        java.util.concurrent.locks.LockSupport.parkUntil(r23, r0);
     */
    /* JADX WARN: Code restructure failed: missing block: B:80:0x014a, code lost:
        if (r23.a != r4) goto L72;
     */
    /* JADX WARN: Code restructure failed: missing block: B:82:0x0155, code lost:
        if ((r0 - java.lang.System.currentTimeMillis()) > 20) goto L72;
     */
    /* JADX WARN: Code restructure failed: missing block: B:84:0x0171, code lost:
        if (java8.util.concurrent.a.v0.compareAndSwapLong(r23, java8.util.concurrent.a.w0, r4, ((r4 - 4294967296L) & (-4294967296L)) | (r15 & 4294967295L)) == false) goto L89;
     */
    /* JADX WARN: Code restructure failed: missing block: B:85:0x0173, code lost:
        r24.a = 1073741824;
     */
    /* JADX WARN: Code restructure failed: missing block: B:86:0x0177, code lost:
        return;
     */
    /* JADX WARN: Code restructure failed: missing block: B:87:0x0178, code lost:
        java.util.concurrent.locks.LockSupport.park(r23);
     */
    /* JADX WARN: Code restructure failed: missing block: B:88:0x017d, code lost:
        r2 = r17;
     */
    /*
        Code decompiled incorrectly, please refer to instructions dump.
        To view partially-correct code enable 'Show inconsistent code' option in preferences
    */
    public final void D(java8.util.concurrent.a.g r24) {
        /*
            Method dump skipped, instructions count: 386
            To view this dump change 'Code comments level' option to 'DEBUG'
        */
        throw new UnsupportedOperationException("Method not decompiled: java8.util.concurrent.a.D(java8.util.concurrent.a$g):void");
    }

    public final void E() {
        int i;
        g gVar;
        while (true) {
            long j = this.a;
            if (j >= 0) {
                return;
            }
            int i2 = (int) j;
            if (i2 == 0) {
                if ((140737488355328L & j) != 0) {
                    I(j);
                    return;
                }
                return;
            }
            g[] gVarArr = this.k0;
            if (gVarArr == null || gVarArr.length <= (i = 65535 & i2) || (gVar = gVarArr[i]) == null) {
                return;
            }
            int i3 = i2 & Integer.MAX_VALUE;
            int i4 = gVar.a;
            long j2 = (gVar.b & 4294967295L) | ((-4294967296L) & (281474976710656L + j));
            java8.util.concurrent.b bVar = gVar.j;
            if (i2 == i4 && v0.compareAndSwapLong(this, w0, j, j2)) {
                gVar.a = i3;
                if (gVar.e < 0) {
                    LockSupport.unpark(bVar);
                    return;
                }
                return;
            }
        }
    }

    @Override // java.util.concurrent.AbstractExecutorService, java.util.concurrent.ExecutorService
    /* renamed from: F */
    public ForkJoinTask<?> submit(Runnable runnable) {
        rl2.f(runnable);
        return k(runnable instanceof ForkJoinTask ? (ForkJoinTask) runnable : new ForkJoinTask.AdaptedRunnableAction(runnable));
    }

    @Override // java.util.concurrent.AbstractExecutorService, java.util.concurrent.ExecutorService
    /* renamed from: G */
    public <T> ForkJoinTask<T> submit(Runnable runnable, T t) {
        return k(new ForkJoinTask.AdaptedRunnable(runnable, t));
    }

    @Override // java.util.concurrent.AbstractExecutorService, java.util.concurrent.ExecutorService
    /* renamed from: H */
    public <T> ForkJoinTask<T> submit(Callable<T> callable) {
        return k(new ForkJoinTask.AdaptedCallable(callable));
    }

    public final void I(long j) {
        long j2 = j;
        do {
            long j3 = ((-281474976710656L) & (281474976710656L + j2)) | (281470681743360L & (4294967296L + j2));
            if (this.a == j2 && v0.compareAndSwapLong(this, w0, j2, j3)) {
                g();
                return;
            }
            j2 = this.a;
            if ((140737488355328L & j2) == 0) {
                return;
            }
        } while (((int) j2) == 0);
    }

    public final int J(g gVar) {
        int length;
        boolean z;
        Thread.State state;
        long j = this.a;
        g[] gVarArr = this.k0;
        short s = (short) (j >>> 32);
        if (s >= 0) {
            if (gVarArr == null || (length = gVarArr.length) <= 0 || gVar == null) {
                return 0;
            }
            int i = (int) j;
            if (i != 0) {
                g gVar2 = gVarArr[i & (length - 1)];
                int i2 = gVar.a;
                long j2 = (-4294967296L) & (i2 < 0 ? 281474976710656L + j : j);
                int i3 = i & Integer.MAX_VALUE;
                if (gVar2 != null) {
                    int i4 = gVar2.a;
                    java8.util.concurrent.b bVar = gVar2.j;
                    long j3 = (gVar2.b & 4294967295L) | j2;
                    if (i4 == i && v0.compareAndSwapLong(this, w0, j, j3)) {
                        gVar2.a = i3;
                        if (gVar2.e < 0) {
                            LockSupport.unpark(bVar);
                        }
                        return i2 < 0 ? -1 : 1;
                    }
                    return 0;
                }
                return 0;
            } else if (((int) (j >> 48)) - ((short) (this.i0 & 65535)) > 0) {
                return v0.compareAndSwapLong(this, w0, j, ((-281474976710656L) & (j - 281474976710656L)) | (281474976710655L & j)) ? 1 : 0;
            } else {
                int i5 = this.j0 & 65535;
                int i6 = i5 + s;
                int i7 = i6;
                int i8 = 0;
                int i9 = 1;
                while (true) {
                    if (i9 >= length) {
                        z = false;
                        break;
                    }
                    g gVar3 = gVarArr[i9];
                    if (gVar3 != null) {
                        if (gVar3.e == 0) {
                            z = true;
                            break;
                        }
                        i7--;
                        java8.util.concurrent.b bVar2 = gVar3.j;
                        if (bVar2 != null && ((state = bVar2.getState()) == Thread.State.BLOCKED || state == Thread.State.WAITING)) {
                            i8++;
                        }
                    }
                    i9 += 2;
                }
                if (z || i7 != 0 || this.a != j) {
                    return 0;
                }
                if (i6 >= 32767 || s >= (this.i0 >>> 16)) {
                    fu2<? super a> fu2Var = this.o0;
                    if (fu2Var == null || !fu2Var.a(this)) {
                        if (i8 < i5) {
                            Thread.yield();
                            return 0;
                        }
                        throw new RejectedExecutionException("Thread limit exceeded replacing blocked worker");
                    }
                    return -1;
                }
            }
        }
        return (v0.compareAndSwapLong(this, w0, j, ((4294967296L + j) & 281470681743360L) | ((-281470681743361L) & j)) && g()) ? 1 : 0;
    }

    public final boolean K(ForkJoinTask<?> forkJoinTask) {
        int length;
        g gVar;
        int c2 = java8.util.concurrent.c.c();
        g[] gVarArr = this.k0;
        return gVarArr != null && (length = gVarArr.length) > 0 && (gVar = gVarArr[(c2 & (length - 1)) & 126]) != null && gVar.p(forkJoinTask);
    }

    public final boolean L(boolean z, boolean z2) {
        int i;
        int i2;
        while (true) {
            int i3 = this.j0;
            int i4 = 0;
            if ((i3 & 262144) != 0) {
                while (true) {
                    int i5 = this.j0;
                    int i6 = 65535;
                    long j = 0;
                    int i7 = 1;
                    if ((i5 & Integer.MIN_VALUE) != 0) {
                        while ((this.j0 & 524288) == 0) {
                            long j2 = 0;
                            while (true) {
                                long j3 = this.a;
                                g[] gVarArr = this.k0;
                                if (gVarArr != null) {
                                    for (int i8 = i4; i8 < gVarArr.length; i8++) {
                                        g gVar = gVarArr[i8];
                                        if (gVar != null) {
                                            java8.util.concurrent.b bVar = gVar.j;
                                            gVar.a();
                                            if (bVar != null) {
                                                try {
                                                    bVar.interrupt();
                                                } catch (Throwable unused) {
                                                }
                                            }
                                            j3 += (gVar.a << 32) + gVar.f;
                                        }
                                    }
                                }
                                i = this.j0;
                                i2 = i & 524288;
                                if (i2 != 0) {
                                    break;
                                }
                                if (this.k0 == gVarArr) {
                                    if (j2 == j3) {
                                        break;
                                    }
                                    j2 = j3;
                                }
                                i4 = 0;
                            }
                            if (i2 != 0 || (i & 65535) + ((short) (this.a >>> 32)) > 0) {
                                return true;
                            }
                            if (v0.compareAndSwapInt(this, x0, i, i | 524288)) {
                                synchronized (this) {
                                    notifyAll();
                                }
                                return true;
                            }
                            i4 = 0;
                        }
                        return true;
                    }
                    if (!z) {
                        while (true) {
                            long j4 = this.a;
                            g[] gVarArr2 = this.k0;
                            char c2 = '0';
                            if ((i5 & i6) + ((int) (j4 >> 48)) <= 0) {
                                if (gVarArr2 != null) {
                                    int i9 = i4;
                                    while (i9 < gVarArr2.length) {
                                        g gVar2 = gVarArr2[i9];
                                        if (gVar2 != null) {
                                            int i10 = gVar2.e;
                                            int i11 = gVar2.a;
                                            int i12 = gVar2.d;
                                            int i13 = gVar2.f;
                                            if (i13 != gVar2.g || ((i12 & 1) == i7 && (i10 >= 0 || i11 >= 0))) {
                                                i7 = 1;
                                                break;
                                            }
                                            j4 += (i10 << c2) + (i11 << 32) + (i13 << 16) + i12;
                                        }
                                        i9++;
                                        c2 = '0';
                                        i7 = 1;
                                    }
                                }
                                i7 = 0;
                            }
                            i5 = this.j0;
                            if ((i5 & Integer.MIN_VALUE) != 0) {
                                i4 = 0;
                                break;
                            } else if (i7 != 0) {
                                return false;
                            } else {
                                i4 = 0;
                                if (this.k0 == gVarArr2) {
                                    if (j == j4) {
                                        break;
                                    }
                                    j = j4;
                                }
                                i6 = 65535;
                                i7 = 1;
                            }
                        }
                    }
                    int i14 = i5;
                    if ((i14 & Integer.MIN_VALUE) == 0) {
                        v0.compareAndSwapInt(this, x0, i14, i14 | Integer.MIN_VALUE);
                    }
                }
            } else if (!z2 || this == r0) {
                break;
            } else {
                v0.compareAndSwapInt(this, x0, i3, i3 | 262144);
            }
        }
        return false;
    }

    /* JADX WARN: Removed duplicated region for block: B:44:0x0099  */
    /* JADX WARN: Removed duplicated region for block: B:65:0x00d5 A[SYNTHETIC] */
    /*
        Code decompiled incorrectly, please refer to instructions dump.
        To view partially-correct code enable 'Show inconsistent code' option in preferences
    */
    public final int a(java8.util.concurrent.a.g r22, java8.util.concurrent.ForkJoinTask<?> r23, long r24) {
        /*
            Method dump skipped, instructions count: 214
            To view this dump change 'Code comments level' option to 'DEBUG'
        */
        throw new UnsupportedOperationException("Method not decompiled: java8.util.concurrent.a.a(java8.util.concurrent.a$g, java8.util.concurrent.ForkJoinTask, long):int");
    }

    @Override // java.util.concurrent.ExecutorService
    public boolean awaitTermination(long j, TimeUnit timeUnit) throws InterruptedException {
        if (!Thread.interrupted()) {
            if (this == r0) {
                b(j, timeUnit);
                return false;
            }
            long nanos = timeUnit.toNanos(j);
            if (isTerminated()) {
                return true;
            }
            if (nanos <= 0) {
                return false;
            }
            long nanoTime = System.nanoTime() + nanos;
            synchronized (this) {
                while (!isTerminated()) {
                    if (nanos <= 0) {
                        return false;
                    }
                    long millis = TimeUnit.NANOSECONDS.toMillis(nanos);
                    if (millis <= 0) {
                        millis = 1;
                    }
                    wait(millis);
                    nanos = nanoTime - System.nanoTime();
                }
                return true;
            }
        }
        throw new InterruptedException();
    }

    public boolean b(long j, TimeUnit timeUnit) {
        long nanos = timeUnit.toNanos(j);
        Thread currentThread = Thread.currentThread();
        if (currentThread instanceof java8.util.concurrent.b) {
            java8.util.concurrent.b bVar = (java8.util.concurrent.b) currentThread;
            if (bVar.a == this) {
                s(bVar.f0);
                return true;
            }
        }
        long nanoTime = System.nanoTime();
        while (true) {
            ForkJoinTask<?> z = z(false);
            if (z != null) {
                z.doExec();
            } else if (u()) {
                return true;
            } else {
                if (System.nanoTime() - nanoTime > nanos) {
                    return false;
                }
                Thread.yield();
            }
        }
    }

    @Override // java.util.concurrent.Executor
    public void execute(Runnable runnable) {
        ForkJoinTask runnableExecuteAction;
        rl2.f(runnable);
        if (runnable instanceof ForkJoinTask) {
            runnableExecuteAction = (ForkJoinTask) runnable;
        } else {
            runnableExecuteAction = new ForkJoinTask.RunnableExecuteAction(runnable);
        }
        k(runnableExecuteAction);
    }

    public final boolean g() {
        java8.util.concurrent.b bVar;
        c cVar = this.m0;
        Throwable th = null;
        if (cVar != null) {
            try {
                bVar = cVar.a(this);
                if (bVar != null) {
                    try {
                        bVar.start();
                        return true;
                    } catch (Throwable th2) {
                        th = th2;
                    }
                }
            } catch (Throwable th3) {
                th = th3;
                bVar = null;
            }
        } else {
            bVar = null;
        }
        h(bVar, th);
        return false;
    }

    /* JADX WARN: Removed duplicated region for block: B:27:0x0040 A[LOOP:0: B:27:0x0040->B:28:0x0064, LOOP_START] */
    /* JADX WARN: Removed duplicated region for block: B:29:0x0066  */
    /* JADX WARN: Removed duplicated region for block: B:30:0x0068  */
    /* JADX WARN: Removed duplicated region for block: B:38:0x007c  */
    /* JADX WARN: Removed duplicated region for block: B:39:0x0080  */
    /*
        Code decompiled incorrectly, please refer to instructions dump.
        To view partially-correct code enable 'Show inconsistent code' option in preferences
    */
    public final void h(java8.util.concurrent.b r18, java.lang.Throwable r19) {
        /*
            r17 = this;
            r9 = r17
            r0 = r18
            r10 = 4294967295(0xffffffff, double:2.1219957905E-314)
            r1 = 0
            r12 = 0
            if (r0 == 0) goto L3a
            java8.util.concurrent.a$g r0 = r0.f0
            if (r0 == 0) goto L3b
            java.lang.String r2 = r9.l0
            int r3 = r0.c
            long r3 = (long) r3
            long r3 = r3 & r10
            int r5 = r0.d
            r6 = 65535(0xffff, float:9.1834E-41)
            r5 = r5 & r6
            if (r2 == 0) goto L37
            monitor-enter(r2)
            java8.util.concurrent.a$g[] r6 = r9.k0     // Catch: java.lang.Throwable -> L34
            if (r6 == 0) goto L2d
            int r7 = r6.length     // Catch: java.lang.Throwable -> L34
            if (r7 <= r5) goto L2d
            r7 = r6[r5]     // Catch: java.lang.Throwable -> L34
            if (r7 != r0) goto L2d
            r6[r5] = r1     // Catch: java.lang.Throwable -> L34
        L2d:
            long r5 = r9.f0     // Catch: java.lang.Throwable -> L34
            long r5 = r5 + r3
            r9.f0 = r5     // Catch: java.lang.Throwable -> L34
            monitor-exit(r2)     // Catch: java.lang.Throwable -> L34
            goto L37
        L34:
            r0 = move-exception
            monitor-exit(r2)     // Catch: java.lang.Throwable -> L34
            throw r0
        L37:
            int r1 = r0.a
            goto L3c
        L3a:
            r0 = r1
        L3b:
            r1 = r12
        L3c:
            r2 = 1073741824(0x40000000, float:2.0)
            if (r1 == r2) goto L66
        L40:
            sun.misc.Unsafe r1 = java8.util.concurrent.a.v0
            long r3 = java8.util.concurrent.a.w0
            long r5 = r9.a
            r7 = -281474976710656(0xffff000000000000, double:NaN)
            r13 = 281474976710656(0x1000000000000, double:1.390671161567E-309)
            long r13 = r5 - r13
            long r7 = r7 & r13
            r13 = 281470681743360(0xffff00000000, double:1.39064994160909E-309)
            r15 = 4294967296(0x100000000, double:2.121995791E-314)
            long r15 = r5 - r15
            long r13 = r13 & r15
            long r7 = r7 | r13
            long r13 = r5 & r10
            long r7 = r7 | r13
            r2 = r17
            boolean r1 = r1.compareAndSwapLong(r2, r3, r5, r7)
            if (r1 == 0) goto L40
        L66:
            if (r0 == 0) goto L6b
            r0.a()
        L6b:
            boolean r1 = r9.L(r12, r12)
            if (r1 != 0) goto L7a
            if (r0 == 0) goto L7a
            java8.util.concurrent.ForkJoinTask<?>[] r0 = r0.h
            if (r0 == 0) goto L7a
            r17.E()
        L7a:
            if (r19 != 0) goto L80
            java8.util.concurrent.ForkJoinTask.helpExpungeStaleExceptions()
            goto L83
        L80:
            java8.util.concurrent.ForkJoinTask.rethrow(r19)
        L83:
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: java8.util.concurrent.a.h(java8.util.concurrent.b, java.lang.Throwable):void");
    }

    public final int i(CountedCompleter<?> countedCompleter, int i) {
        int length;
        g gVar;
        int c2 = java8.util.concurrent.c.c();
        g[] gVarArr = this.k0;
        if (gVarArr == null || (length = gVarArr.length) <= 0 || (gVar = gVarArr[c2 & (length - 1) & 126]) == null) {
            return 0;
        }
        return gVar.m(countedCompleter, i);
    }

    @Override // java.util.concurrent.AbstractExecutorService, java.util.concurrent.ExecutorService
    public <T> List<Future<T>> invokeAll(Collection<? extends Callable<T>> collection) {
        ArrayList arrayList = new ArrayList(collection.size());
        try {
            for (Callable<T> callable : collection) {
                ForkJoinTask.AdaptedCallable adaptedCallable = new ForkJoinTask.AdaptedCallable(callable);
                arrayList.add(adaptedCallable);
                k(adaptedCallable);
            }
            int size = arrayList.size();
            for (int i = 0; i < size; i++) {
                ((ForkJoinTask) arrayList.get(i)).quietlyJoin();
            }
            return arrayList;
        } catch (Throwable th) {
            int size2 = arrayList.size();
            for (int i2 = 0; i2 < size2; i2++) {
                ((Future) arrayList.get(i2)).cancel(false);
            }
            throw th;
        }
    }

    @Override // java.util.concurrent.ExecutorService
    public boolean isShutdown() {
        return (this.j0 & 262144) != 0;
    }

    @Override // java.util.concurrent.ExecutorService
    public boolean isTerminated() {
        return (this.j0 & 524288) != 0;
    }

    public final void j(ForkJoinTask<?> forkJoinTask) {
        int length;
        boolean z;
        boolean z2;
        int length2;
        int length3;
        int length4;
        int c2 = java8.util.concurrent.c.c();
        if (c2 == 0) {
            java8.util.concurrent.c.i();
            c2 = java8.util.concurrent.c.c();
        }
        while (true) {
            int i = this.j0;
            g[] gVarArr = this.k0;
            if ((i & 262144) != 0 || gVarArr == null || (length = gVarArr.length) <= 0) {
                break;
            }
            g gVar = gVarArr[(length - 1) & c2 & 126];
            if (gVar == null) {
                String str = this.l0;
                int i2 = (c2 | 1073741824) & (-65538);
                g gVar2 = new g(this, null);
                gVar2.d = i2;
                gVar2.e = 1073741824;
                gVar2.a = 1;
                if (str != null) {
                    synchronized (str) {
                        g[] gVarArr2 = this.k0;
                        if (gVarArr2 != null && (length4 = gVarArr2.length) > 0) {
                            int i3 = i2 & (length4 - 1) & 126;
                            if (gVarArr2[i3] == null) {
                                gVarArr2[i3] = gVar2;
                                z = true;
                                z2 = z;
                            }
                        }
                        z = false;
                        z2 = z;
                    }
                } else {
                    z = false;
                    z2 = false;
                }
                gVar = gVar2;
            } else if (gVar.n()) {
                int i4 = gVar.f;
                int i5 = gVar.g;
                ForkJoinTask<?>[] forkJoinTaskArr = gVar.h;
                if (forkJoinTaskArr != null && (length2 = forkJoinTaskArr.length) > 0) {
                    int i6 = length2 - 1;
                    int i7 = i4 - i5;
                    if (i6 + i7 > 0) {
                        forkJoinTaskArr[i6 & i5] = forkJoinTask;
                        gVar.g = i5 + 1;
                        if (i7 < 0 && gVar.f - i5 < -1) {
                            return;
                        }
                        z2 = false;
                        z = true;
                    }
                }
                z2 = true;
                z = true;
            } else {
                z = false;
                z2 = false;
            }
            if (z) {
                if (z2) {
                    try {
                        gVar.b();
                        int i8 = gVar.g;
                        ForkJoinTask<?>[] forkJoinTaskArr2 = gVar.h;
                        if (forkJoinTaskArr2 != null && (length3 = forkJoinTaskArr2.length) > 0) {
                            forkJoinTaskArr2[(length3 - 1) & i8] = forkJoinTask;
                            gVar.g = i8 + 1;
                        }
                    } finally {
                        gVar.a = 0;
                    }
                }
                E();
                return;
            }
            c2 = java8.util.concurrent.c.a(c2);
        }
        throw new RejectedExecutionException();
    }

    public final <T> ForkJoinTask<T> k(ForkJoinTask<T> forkJoinTask) {
        g gVar;
        rl2.f(forkJoinTask);
        Thread currentThread = Thread.currentThread();
        if (currentThread instanceof java8.util.concurrent.b) {
            java8.util.concurrent.b bVar = (java8.util.concurrent.b) currentThread;
            if (bVar.a == this && (gVar = bVar.f0) != null) {
                gVar.k(forkJoinTask);
                return forkJoinTask;
            }
        }
        j(forkJoinTask);
        return forkJoinTask;
    }

    @Override // java.util.concurrent.AbstractExecutorService
    public <T> RunnableFuture<T> newTaskFor(Runnable runnable, T t) {
        return new ForkJoinTask.AdaptedRunnable(runnable, t);
    }

    public int o() {
        int i = this.j0 & 65535;
        if (i > 0) {
            return i;
        }
        return 1;
    }

    public final int r(g gVar, CountedCompleter<?> countedCompleter, int i) {
        if (gVar == null) {
            return 0;
        }
        return gVar.d(countedCompleter, i);
    }

    public final void s(g gVar) {
        boolean z;
        boolean z2;
        int i;
        g gVar2;
        ForkJoinTask<?>[] forkJoinTaskArr;
        int length;
        int i2 = gVar.e;
        int i3 = gVar.d & 65536;
        char c2 = 65535;
        int i4 = i2;
        char c3 = 65535;
        while (true) {
            if (i3 != 0) {
                gVar.e(0);
            } else {
                gVar.f(0);
            }
            if (c3 == c2 && gVar.a >= 0) {
                c3 = 1;
            }
            int l = java8.util.concurrent.c.l();
            g[] gVarArr = this.k0;
            long j = 281474976710656L;
            if (gVarArr != null) {
                int length2 = gVarArr.length;
                int i5 = length2 - 1;
                int i6 = length2;
                z2 = true;
                while (true) {
                    if (i6 <= 0) {
                        z = true;
                        break;
                    }
                    int i7 = (l - i6) & i5;
                    if (i7 >= 0 && i7 < length2 && (gVar2 = gVarArr[i7]) != null) {
                        int i8 = gVar2.f;
                        if (i8 - gVar2.g < 0 && (forkJoinTaskArr = gVar2.h) != null && (length = forkJoinTaskArr.length) > 0) {
                            if (c3 == 0) {
                                l(this, w0, j);
                                c3 = 1;
                            }
                            long j2 = (((length - 1) & i8) << z0) + y0;
                            Unsafe unsafe = v0;
                            ForkJoinTask forkJoinTask = (ForkJoinTask) unsafe.getObjectVolatile(forkJoinTaskArr, j2);
                            if (forkJoinTask != null) {
                                int i9 = i8 + 1;
                                if (i8 == gVar2.f && unsafe.compareAndSwapObject(forkJoinTaskArr, j2, forkJoinTask, (Object) null)) {
                                    gVar2.f = i9;
                                    gVar.e = gVar2.d;
                                    forkJoinTask.doExec();
                                    gVar.e = i2;
                                    i4 = i2;
                                }
                            }
                            z = false;
                            z2 = false;
                        } else if ((gVar2.e & 1073741824) == 0) {
                            z2 = false;
                        }
                    }
                    i6--;
                    j = 281474976710656L;
                }
            } else {
                z = true;
                z2 = true;
            }
            if (z2) {
                break;
            }
            if (z) {
                if (i4 != 1073741824) {
                    gVar.e = 1073741824;
                    i = 1073741824;
                } else {
                    i = i4;
                }
                if (c3 == 1) {
                    l(this, w0, -281474976710656L);
                    i4 = i;
                    c3 = 0;
                } else {
                    i4 = i;
                }
            }
            c2 = 65535;
        }
        if (c3 == 0) {
            l(this, w0, 281474976710656L);
        }
        gVar.e = i2;
    }

    @Override // java.util.concurrent.ExecutorService
    public void shutdown() {
        c();
        L(false, true);
    }

    @Override // java.util.concurrent.ExecutorService
    public List<Runnable> shutdownNow() {
        c();
        L(true, true);
        return Collections.emptyList();
    }

    public String toString() {
        int i;
        long j;
        long j2 = this.f0;
        g[] gVarArr = this.k0;
        long j3 = 0;
        long j4 = 0;
        if (gVarArr != null) {
            i = 0;
            for (int i2 = 0; i2 < gVarArr.length; i2++) {
                g gVar = gVarArr[i2];
                if (gVar != null) {
                    int l = gVar.l();
                    if ((i2 & 1) == 0) {
                        j4 += l;
                    } else {
                        j3 += l;
                        j2 += gVar.c & 4294967295L;
                        if (gVar.c()) {
                            i++;
                        }
                    }
                }
            }
        } else {
            i = 0;
        }
        int i3 = this.j0;
        int i4 = 65535 & i3;
        int i5 = ((short) (j >>> 32)) + i4;
        int i6 = ((int) (this.a >> 48)) + i4;
        int i7 = i6 >= 0 ? i6 : 0;
        return super.toString() + "[" + ((524288 & i3) != 0 ? "Terminated" : (Integer.MIN_VALUE & i3) != 0 ? "Terminating" : (i3 & 262144) != 0 ? "Shutting down" : "Running") + ", parallelism = " + i4 + ", size = " + i5 + ", active = " + i7 + ", running = " + i + ", steals = " + j2 + ", tasks = " + j3 + ", submissions = " + j4 + "]";
    }

    public boolean u() {
        while (true) {
            long j = this.a;
            int i = this.j0;
            int i2 = 65535 & i;
            int i3 = ((short) (j >>> 32)) + i2;
            int i4 = i2 + ((int) (j >> 48));
            if ((i & (-2146959360)) != 0) {
                return true;
            }
            if (i4 > 0) {
                return false;
            }
            g[] gVarArr = this.k0;
            if (gVarArr != null) {
                for (int i5 = 1; i5 < gVarArr.length; i5 += 2) {
                    g gVar = gVarArr[i5];
                    if (gVar != null) {
                        if ((gVar.e & 1073741824) == 0) {
                            return false;
                        }
                        i3--;
                    }
                }
            }
            if (i3 == 0 && this.a == j) {
                return true;
            }
        }
    }

    public final ForkJoinTask<?> y(g gVar) {
        if (gVar != null) {
            ForkJoinTask<?> i = (gVar.d & 65536) != 0 ? gVar.i() : gVar.j();
            if (i != null) {
                return i;
            }
        }
        return z(false);
    }

    /* JADX WARN: Code restructure failed: missing block: B:22:0x003d, code lost:
        r14 = (((r11 - 1) & r10) << java8.util.concurrent.a.z0) + java8.util.concurrent.a.y0;
        r12 = java8.util.concurrent.a.v0;
        r1 = (java8.util.concurrent.ForkJoinTask) r12.getObjectVolatile(r13, r14);
     */
    /* JADX WARN: Code restructure failed: missing block: B:23:0x0052, code lost:
        if (r1 == null) goto L36;
     */
    /* JADX WARN: Code restructure failed: missing block: B:24:0x0054, code lost:
        r2 = r10 + 1;
     */
    /* JADX WARN: Code restructure failed: missing block: B:25:0x0058, code lost:
        if (r10 != r9.f) goto L35;
     */
    /* JADX WARN: Code restructure failed: missing block: B:27:0x0062, code lost:
        if (r12.compareAndSwapObject(r13, r14, r1, (java.lang.Object) null) == false) goto L33;
     */
    /* JADX WARN: Code restructure failed: missing block: B:28:0x0064, code lost:
        r9.f = r2;
     */
    /* JADX WARN: Code restructure failed: missing block: B:29:0x0066, code lost:
        return r1;
     */
    /* JADX WARN: Code restructure failed: missing block: B:46:0x0002, code lost:
        continue;
     */
    /*
        Code decompiled incorrectly, please refer to instructions dump.
        To view partially-correct code enable 'Show inconsistent code' option in preferences
    */
    public final java8.util.concurrent.ForkJoinTask<?> z(boolean r19) {
        /*
            r18 = this;
            r0 = r18
        L2:
            int r1 = r0.j0
            r2 = -2147483648(0xffffffff80000000, float:-0.0)
            r1 = r1 & r2
            if (r1 != 0) goto L71
            java8.util.concurrent.a$g[] r1 = r0.k0
            if (r1 == 0) goto L71
            int r2 = r1.length
            if (r2 <= 0) goto L71
            int r2 = r2 + (-1)
            int r3 = java8.util.concurrent.c.l()
            int r4 = r3 >>> 16
            if (r19 == 0) goto L22
            r3 = r3 & (-2)
            r3 = r3 & r2
            r4 = r4 & (-2)
            r4 = r4 | 2
            goto L25
        L22:
            r3 = r3 & r2
            r4 = r4 | 1
        L25:
            r5 = 0
            r6 = r3
            r7 = r5
            r8 = r7
        L29:
            r9 = r1[r6]
            if (r9 == 0) goto L67
            int r10 = r9.f
            int r7 = r7 + r10
            int r11 = r9.g
            int r11 = r10 - r11
            if (r11 >= 0) goto L67
            java8.util.concurrent.ForkJoinTask<?>[] r13 = r9.h
            if (r13 == 0) goto L67
            int r11 = r13.length
            if (r11 <= 0) goto L67
            int r11 = r11 + (-1)
            r1 = r11 & r10
            long r1 = (long) r1
            int r3 = java8.util.concurrent.a.z0
            long r1 = r1 << r3
            int r3 = java8.util.concurrent.a.y0
            long r3 = (long) r3
            long r14 = r1 + r3
            sun.misc.Unsafe r12 = java8.util.concurrent.a.v0
            java.lang.Object r1 = r12.getObjectVolatile(r13, r14)
            java8.util.concurrent.ForkJoinTask r1 = (java8.util.concurrent.ForkJoinTask) r1
            if (r1 == 0) goto L2
            int r2 = r10 + 1
            int r3 = r9.f
            if (r10 != r3) goto L2
            r17 = 0
            r16 = r1
            boolean r3 = r12.compareAndSwapObject(r13, r14, r16, r17)
            if (r3 == 0) goto L2
            r9.f = r2
            return r1
        L67:
            int r6 = r6 + r4
            r6 = r6 & r2
            if (r6 != r3) goto L29
            if (r8 != r7) goto L6e
            goto L71
        L6e:
            r8 = r7
            r7 = r5
            goto L29
        L71:
            r1 = 0
            return r1
        */
        throw new UnsupportedOperationException("Method not decompiled: java8.util.concurrent.a.z(boolean):java8.util.concurrent.ForkJoinTask");
    }

    public a() {
        this(Math.min(32767, Runtime.getRuntime().availableProcessors()), p0, null, false, 0, 32767, 1, null, 60000L, TimeUnit.MILLISECONDS);
    }

    @Override // java.util.concurrent.AbstractExecutorService
    public <T> RunnableFuture<T> newTaskFor(Callable<T> callable) {
        return new ForkJoinTask.AdaptedCallable(callable);
    }

    public a(int i, c cVar, Thread.UncaughtExceptionHandler uncaughtExceptionHandler, boolean z, int i2, int i3, int i4, fu2<? super a> fu2Var, long j, TimeUnit timeUnit) {
        if (i > 0 && i <= 32767 && i3 >= i && j > 0) {
            rl2.f(cVar);
            long max = Math.max(timeUnit.toMillis(j), 20L);
            long j2 = (((-Math.min(Math.max(i2, i), 32767)) << 32) & 281470681743360L) | (((-i) << 48) & (-281474976710656L));
            int i5 = (z ? 65536 : 0) | i;
            int min = ((Math.min(i3, 32767) - i) << 16) | ((Math.min(Math.max(i4, 0), 32767) - i) & 65535);
            int i6 = i > 1 ? i - 1 : 1;
            int i7 = i6 | (i6 >>> 1);
            int i8 = i7 | (i7 >>> 2);
            int i9 = i8 | (i8 >>> 4);
            int i10 = i9 | (i9 >>> 8);
            this.l0 = "ForkJoinPool-" + x() + "-worker-";
            this.k0 = new g[((i10 | (i10 >>> 16)) + 1) << 1];
            this.m0 = cVar;
            this.n0 = uncaughtExceptionHandler;
            this.o0 = fu2Var;
            this.g0 = max;
            this.i0 = min;
            this.j0 = i5;
            this.a = j2;
            c();
            return;
        }
        throw new IllegalArgumentException();
    }

    /* JADX WARN: Removed duplicated region for block: B:12:0x0026  */
    /* JADX WARN: Removed duplicated region for block: B:23:0x0047  */
    /* JADX WARN: Removed duplicated region for block: B:26:0x0068  */
    /* JADX WARN: Removed duplicated region for block: B:27:0x006b  */
    /*
        Code decompiled incorrectly, please refer to instructions dump.
        To view partially-correct code enable 'Show inconsistent code' option in preferences
    */
    public a(byte r11) {
        /*
            r10 = this;
            r10.<init>()
            r11 = 0
            r0 = -1
            java.lang.String r1 = "java.util.concurrent.ForkJoinPool.common.parallelism"
            java.lang.String r1 = java.lang.System.getProperty(r1)     // Catch: java.lang.Exception -> L22
            if (r1 == 0) goto L11
            int r0 = java.lang.Integer.parseInt(r1)     // Catch: java.lang.Exception -> L22
        L11:
            java.lang.String r1 = "java.util.concurrent.ForkJoinPool.common.threadFactory"
            java.lang.Object r1 = w(r1)     // Catch: java.lang.Exception -> L22
            java8.util.concurrent.a$c r1 = (java8.util.concurrent.a.c) r1     // Catch: java.lang.Exception -> L22
            java.lang.String r2 = "java.util.concurrent.ForkJoinPool.common.exceptionHandler"
            java.lang.Object r2 = w(r2)     // Catch: java.lang.Exception -> L23
            java.lang.Thread$UncaughtExceptionHandler r2 = (java.lang.Thread.UncaughtExceptionHandler) r2     // Catch: java.lang.Exception -> L23
            goto L24
        L22:
            r1 = r11
        L23:
            r2 = r11
        L24:
            if (r1 != 0) goto L34
            java.lang.SecurityManager r1 = java.lang.System.getSecurityManager()
            if (r1 != 0) goto L2f
            java8.util.concurrent.a$c r1 = java8.util.concurrent.a.p0
            goto L34
        L2f:
            java8.util.concurrent.a$d r1 = new java8.util.concurrent.a$d
            r1.<init>(r11)
        L34:
            r3 = 1
            if (r0 >= 0) goto L43
            java.lang.Runtime r0 = java.lang.Runtime.getRuntime()
            int r0 = r0.availableProcessors()
            int r0 = r0 - r3
            if (r0 > 0) goto L43
            r0 = r3
        L43:
            r4 = 32767(0x7fff, float:4.5916E-41)
            if (r0 <= r4) goto L48
            r0 = r4
        L48:
            int r4 = -r0
            long r4 = (long) r4
            r6 = 32
            long r6 = r4 << r6
            r8 = 281470681743360(0xffff00000000, double:1.39064994160909E-309)
            long r6 = r6 & r8
            r8 = 48
            long r4 = r4 << r8
            r8 = -281474976710656(0xffff000000000000, double:NaN)
            long r4 = r4 & r8
            long r4 = r4 | r6
            int r6 = 1 - r0
            r7 = 65535(0xffff, float:9.1834E-41)
            r6 = r6 & r7
            int r7 = java8.util.concurrent.a.t0
            int r7 = r7 << 16
            r6 = r6 | r7
            if (r0 <= r3) goto L6b
            int r7 = r0 + (-1)
            goto L6c
        L6b:
            r7 = r3
        L6c:
            int r8 = r7 >>> 1
            r7 = r7 | r8
            int r8 = r7 >>> 2
            r7 = r7 | r8
            int r8 = r7 >>> 4
            r7 = r7 | r8
            int r8 = r7 >>> 8
            r7 = r7 | r8
            int r8 = r7 >>> 16
            r7 = r7 | r8
            int r7 = r7 + r3
            int r3 = r7 << 1
            java.lang.String r7 = "ForkJoinPool.commonPool-worker-"
            r10.l0 = r7
            java8.util.concurrent.a$g[] r3 = new java8.util.concurrent.a.g[r3]
            r10.k0 = r3
            r10.m0 = r1
            r10.n0 = r2
            r10.o0 = r11
            r1 = 60000(0xea60, double:2.9644E-319)
            r10.g0 = r1
            r10.i0 = r6
            r10.j0 = r0
            r10.a = r4
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: java8.util.concurrent.a.<init>(byte):void");
    }
}
