package java8.util.concurrent;

import java.security.AccessControlContext;
import java.security.AccessController;
import java.security.PrivilegedAction;
import java.security.SecureRandom;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.atomic.AtomicLong;
import sun.misc.Unsafe;

/* compiled from: TLRandom.java */
/* loaded from: classes2.dex */
public final class c {
    public static final Unsafe a;
    public static final boolean b;
    public static final boolean c;
    public static final long d;
    public static final long e;
    public static final long f;
    public static final long g;
    public static final ThreadLocal<C0184c> h;
    public static final AtomicInteger i;
    public static final AtomicLong j;

    /* compiled from: TLRandom.java */
    /* loaded from: classes2.dex */
    public static class a extends ThreadLocal<C0184c> {
        @Override // java.lang.ThreadLocal
        /* renamed from: a */
        public C0184c initialValue() {
            return new C0184c(null);
        }
    }

    /* compiled from: TLRandom.java */
    /* loaded from: classes2.dex */
    public static class b implements PrivilegedAction<Boolean> {
        @Override // java.security.PrivilegedAction
        /* renamed from: a */
        public Boolean run() {
            return Boolean.valueOf(Boolean.getBoolean("java.util.secureRandomSeed"));
        }
    }

    /* compiled from: TLRandom.java */
    /* renamed from: java8.util.concurrent.c$c  reason: collision with other inner class name */
    /* loaded from: classes2.dex */
    public static final class C0184c {
        public long a;
        public int b;
        public int c;

        public C0184c() {
        }

        public /* synthetic */ C0184c(a aVar) {
            this();
        }
    }

    static {
        Unsafe unsafe = d.a;
        a = unsafe;
        try {
            boolean h2 = h();
            b = h2;
            boolean f2 = f();
            c = f2;
            if (!f2) {
                d = unsafe.objectFieldOffset(Thread.class.getDeclaredField("threadLocals"));
                e = unsafe.objectFieldOffset(Thread.class.getDeclaredField("inheritableThreadLocals"));
                f = unsafe.objectFieldOffset(Thread.class.getDeclaredField(h2 ? "accessControlContext" : "inheritedAccessControlContext"));
                g = unsafe.objectFieldOffset(Thread.class.getDeclaredField("contextClassLoader"));
            } else {
                d = 0L;
                e = 0L;
                f = 0L;
                g = 0L;
            }
            h = new a();
            i = new AtomicInteger();
            j = new AtomicLong(k(System.currentTimeMillis()) ^ k(System.nanoTime()));
            if (((Boolean) AccessController.doPrivileged(new b())).booleanValue()) {
                byte[] seed = SecureRandom.getSeed(8);
                long j2 = seed[0] & 255;
                for (int i2 = 1; i2 < 8; i2++) {
                    j2 = (j2 << 8) | (seed[i2] & 255);
                }
                j.set(j2);
            }
        } catch (Exception e2) {
            throw new ExceptionInInitializerError(e2);
        }
    }

    public static final int a(int i2) {
        int i3 = i2 ^ (i2 << 13);
        int i4 = i3 ^ (i3 >>> 17);
        int i5 = i4 ^ (i4 << 5);
        o(i5);
        return i5;
    }

    public static final void b(Thread thread) {
        if (c) {
            return;
        }
        Unsafe unsafe = a;
        unsafe.putObject(thread, d, (Object) null);
        unsafe.putObject(thread, e, (Object) null);
    }

    public static final int c() {
        return d();
    }

    public static int d() {
        return h.get().b;
    }

    public static int e() {
        return h.get().c;
    }

    public static boolean f() {
        if (g("android.util.DisplayMetrics")) {
            return true;
        }
        return g("org.robovm.rt.bro.Bro");
    }

    public static boolean g(String str) {
        Class<?> cls;
        try {
            cls = Class.forName(str, false, c.class.getClassLoader());
        } catch (Throwable unused) {
            cls = null;
        }
        return cls != null;
    }

    public static boolean h() {
        String property;
        return g("com.ibm.misc.JarVersion") && (property = System.getProperty("java.class.version", "45")) != null && property.length() >= 2 && "52".compareTo(property.substring(0, 2)) > 0;
    }

    public static final void i() {
        int addAndGet = i.addAndGet(-1640531527);
        if (addAndGet == 0) {
            addAndGet = 1;
        }
        q(k(j.getAndAdd(-4942790177534073029L)));
        o(addAndGet);
    }

    public static int j(long j2) {
        long j3 = (j2 ^ (j2 >>> 33)) * (-49064778989728563L);
        return (int) (((j3 ^ (j3 >>> 33)) * (-4265267296055464877L)) >>> 32);
    }

    public static long k(long j2) {
        long j3 = (j2 ^ (j2 >>> 33)) * (-49064778989728563L);
        long j4 = (j3 ^ (j3 >>> 33)) * (-4265267296055464877L);
        return j4 ^ (j4 >>> 33);
    }

    public static final int l() {
        int j2;
        int e2 = e();
        if (e2 != 0) {
            int i2 = e2 ^ (e2 << 13);
            int i3 = i2 ^ (i2 >>> 17);
            j2 = i3 ^ (i3 << 5);
        } else {
            j2 = j(j.getAndAdd(-4942790177534073029L));
            if (j2 == 0) {
                j2 = 1;
            }
        }
        p(j2);
        return j2;
    }

    public static final void m(Thread thread, ClassLoader classLoader) {
        if (c) {
            return;
        }
        a.putObject(thread, g, classLoader);
    }

    public static final void n(Thread thread, AccessControlContext accessControlContext) {
        if (c) {
            return;
        }
        a.putOrderedObject(thread, f, accessControlContext);
    }

    public static void o(int i2) {
        h.get().b = i2;
    }

    public static void p(int i2) {
        h.get().c = i2;
    }

    public static void q(long j2) {
        h.get().a = j2;
    }
}
