package java8.util.concurrent;

import java.util.concurrent.CancellationException;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Executor;
import java.util.concurrent.Future;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;
import java.util.concurrent.locks.LockSupport;
import java8.util.concurrent.a;
import sun.misc.Unsafe;

/* loaded from: classes2.dex */
public class CompletableFuture<T> implements Future<T> {
    public static final a g0 = new a(null);
    public static final boolean h0;
    public static final Executor i0;
    public static final Unsafe j0;
    public static final long k0;
    public static final long l0;
    public static final long m0;
    public volatile Object a;
    public volatile Completion f0;

    /* loaded from: classes2.dex */
    public static final class AsyncRun extends ForkJoinTask<Void> implements Runnable, b {
        public CompletableFuture<Void> dep;
        public Runnable fn;

        public AsyncRun(CompletableFuture<Void> completableFuture, Runnable runnable) {
            this.dep = completableFuture;
            this.fn = runnable;
        }

        @Override // java8.util.concurrent.ForkJoinTask
        public final boolean exec() {
            run();
            return false;
        }

        @Override // java8.util.concurrent.ForkJoinTask
        public final Void getRawResult() {
            return null;
        }

        @Override // java.lang.Runnable
        public void run() {
            Runnable runnable;
            CompletableFuture<Void> completableFuture = this.dep;
            if (completableFuture == null || (runnable = this.fn) == null) {
                return;
            }
            this.dep = null;
            this.fn = null;
            if (completableFuture.a == null) {
                try {
                    runnable.run();
                    completableFuture.h();
                } catch (Throwable th) {
                    completableFuture.i(th);
                }
            }
            completableFuture.o();
        }

        @Override // java8.util.concurrent.ForkJoinTask
        public final void setRawResult(Void r1) {
        }
    }

    /* loaded from: classes2.dex */
    public static abstract class Completion extends ForkJoinTask<Void> implements Runnable, b {
        public volatile Completion next;

        @Override // java8.util.concurrent.ForkJoinTask
        public final boolean exec() {
            tryFire(1);
            return false;
        }

        @Override // java8.util.concurrent.ForkJoinTask
        public final Void getRawResult() {
            return null;
        }

        public abstract boolean isLive();

        @Override // java.lang.Runnable
        public final void run() {
            tryFire(1);
        }

        @Override // java8.util.concurrent.ForkJoinTask
        public final void setRawResult(Void r1) {
        }

        public abstract CompletableFuture<?> tryFire(int i);
    }

    /* loaded from: classes2.dex */
    public static final class Signaller extends Completion implements a.e {
        public final long deadline;
        public boolean interrupted;
        public final boolean interruptible;
        public long nanos;
        public volatile Thread thread = Thread.currentThread();

        public Signaller(boolean z, long j, long j2) {
            this.interruptible = z;
            this.nanos = j;
            this.deadline = j2;
        }

        @Override // java8.util.concurrent.a.e
        public boolean block() {
            while (!isReleasable()) {
                if (this.deadline == 0) {
                    LockSupport.park(this);
                } else {
                    LockSupport.parkNanos(this, this.nanos);
                }
            }
            return true;
        }

        @Override // java8.util.concurrent.CompletableFuture.Completion
        public final boolean isLive() {
            return this.thread != null;
        }

        @Override // java8.util.concurrent.a.e
        public boolean isReleasable() {
            if (Thread.interrupted()) {
                this.interrupted = true;
            }
            if (this.interrupted && this.interruptible) {
                return true;
            }
            long j = this.deadline;
            if (j != 0) {
                if (this.nanos <= 0) {
                    return true;
                }
                long nanoTime = j - System.nanoTime();
                this.nanos = nanoTime;
                if (nanoTime <= 0) {
                    return true;
                }
            }
            return this.thread == null;
        }

        @Override // java8.util.concurrent.CompletableFuture.Completion
        public final CompletableFuture<?> tryFire(int i) {
            Thread thread = this.thread;
            if (thread != null) {
                this.thread = null;
                LockSupport.unpark(thread);
            }
            return null;
        }
    }

    /* loaded from: classes2.dex */
    public static final class a {
        public final Throwable a;

        public a(Throwable th) {
            this.a = th;
        }
    }

    /* loaded from: classes2.dex */
    public interface b {
    }

    /* loaded from: classes2.dex */
    public static final class c implements Executor {
        @Override // java.util.concurrent.Executor
        public void execute(Runnable runnable) {
            new Thread(runnable).start();
        }
    }

    static {
        boolean z = java8.util.concurrent.a.n() > 1;
        h0 = z;
        i0 = z ? java8.util.concurrent.a.d() : new c();
        Unsafe unsafe = d.a;
        j0 = unsafe;
        try {
            k0 = unsafe.objectFieldOffset(CompletableFuture.class.getDeclaredField("a"));
            l0 = unsafe.objectFieldOffset(CompletableFuture.class.getDeclaredField("f0"));
            m0 = unsafe.objectFieldOffset(Completion.class.getDeclaredField("next"));
        } catch (Exception e) {
            throw new ExceptionInInitializerError(e);
        }
    }

    public static CompletableFuture<Void> a(Executor executor, Runnable runnable) {
        rl2.f(runnable);
        CompletableFuture<Void> completableFuture = new CompletableFuture<>();
        executor.execute(new AsyncRun(completableFuture, runnable));
        return completableFuture;
    }

    public static boolean b(Completion completion, Completion completion2, Completion completion3) {
        return j0.compareAndSwapObject(completion, m0, completion2, completion3);
    }

    public static a l(Throwable th) {
        if (!(th instanceof CompletionException)) {
            th = new CompletionException(th);
        }
        return new a(th);
    }

    public static void n(Completion completion, Completion completion2) {
        j0.putOrderedObject(completion, m0, completion2);
    }

    public static Object q(Object obj) throws InterruptedException, ExecutionException {
        Throwable cause;
        if (obj != null) {
            if (obj instanceof a) {
                Throwable th = ((a) obj).a;
                if (th == null) {
                    return null;
                }
                if (!(th instanceof CancellationException)) {
                    if ((th instanceof CompletionException) && (cause = th.getCause()) != null) {
                        th = cause;
                    }
                    throw new ExecutionException(th);
                }
                throw ((CancellationException) th);
            }
            return obj;
        }
        throw new InterruptedException();
    }

    public static CompletableFuture<Void> r(Runnable runnable, Executor executor) {
        return a(s(executor), runnable);
    }

    public static Executor s(Executor executor) {
        if (!h0 && executor == java8.util.concurrent.a.d()) {
            return i0;
        }
        return (Executor) rl2.f(executor);
    }

    public final boolean c(Completion completion, Completion completion2) {
        return j0.compareAndSwapObject(this, l0, completion, completion2);
    }

    @Override // java.util.concurrent.Future
    public boolean cancel(boolean z) {
        boolean z2 = this.a == null && m(new a(new CancellationException()));
        o();
        return z2 || isCancelled();
    }

    public final void e() {
        Completion completion;
        boolean z = false;
        while (true) {
            completion = this.f0;
            if (completion == null || completion.isLive()) {
                break;
            }
            z = c(completion, completion.next);
        }
        if (completion == null || z) {
            return;
        }
        Completion completion2 = completion.next;
        Completion completion3 = completion;
        while (completion2 != null) {
            Completion completion4 = completion2.next;
            if (!completion2.isLive()) {
                b(completion3, completion2, completion4);
                return;
            } else {
                completion3 = completion2;
                completion2 = completion4;
            }
        }
    }

    public boolean f(T t) {
        boolean j = j(t);
        o();
        return j;
    }

    public boolean g(Throwable th) {
        boolean m = m(new a((Throwable) rl2.f(th)));
        o();
        return m;
    }

    @Override // java.util.concurrent.Future
    public T get() throws InterruptedException, ExecutionException {
        Object obj = this.a;
        if (obj == null) {
            obj = v(true);
        }
        return (T) q(obj);
    }

    public final boolean h() {
        return j0.compareAndSwapObject(this, k0, (Object) null, g0);
    }

    public final boolean i(Throwable th) {
        return j0.compareAndSwapObject(this, k0, (Object) null, l(th));
    }

    @Override // java.util.concurrent.Future
    public boolean isCancelled() {
        Object obj = this.a;
        return (obj instanceof a) && (((a) obj).a instanceof CancellationException);
    }

    @Override // java.util.concurrent.Future
    public boolean isDone() {
        return this.a != null;
    }

    public final boolean j(T t) {
        Unsafe unsafe = j0;
        long j = k0;
        if (t == null) {
            t = (T) g0;
        }
        return unsafe.compareAndSwapObject(this, j, (Object) null, t);
    }

    public Executor k() {
        return i0;
    }

    public final boolean m(Object obj) {
        return j0.compareAndSwapObject(this, k0, (Object) null, obj);
    }

    /* JADX WARN: Multi-variable type inference failed */
    public final void o() {
        while (true) {
            CompletableFuture completableFuture = this;
            while (true) {
                Completion completion = completableFuture.f0;
                completableFuture = completableFuture;
                if (completion == null) {
                    if (completableFuture == this || (completion = this.f0) == null) {
                        return;
                    }
                    completableFuture = this;
                }
                Completion completion2 = completion.next;
                if (completableFuture.c(completion, completion2)) {
                    if (completion2 != null) {
                        if (completableFuture != this) {
                            p(completion);
                        } else {
                            b(completion, completion2, null);
                        }
                    }
                    completableFuture = completion.tryFire(-1);
                    if (completableFuture == null) {
                        break;
                    }
                }
            }
        }
    }

    public final void p(Completion completion) {
        do {
        } while (!u(completion));
    }

    public final Object t(long j) throws TimeoutException {
        Object obj;
        if (Thread.interrupted()) {
            return null;
        }
        if (j > 0) {
            long nanoTime = System.nanoTime() + j;
            if (nanoTime == 0) {
                nanoTime = 1;
            }
            boolean z = false;
            Signaller signaller = null;
            while (true) {
                obj = this.a;
                if (obj != null) {
                    break;
                } else if (signaller == null) {
                    Signaller signaller2 = new Signaller(true, j, nanoTime);
                    if (Thread.currentThread() instanceof java8.util.concurrent.b) {
                        java8.util.concurrent.a.q(k(), signaller2);
                    }
                    signaller = signaller2;
                } else if (!z) {
                    z = u(signaller);
                } else if (signaller.nanos <= 0) {
                    break;
                } else {
                    try {
                        java8.util.concurrent.a.v(signaller);
                    } catch (InterruptedException unused) {
                        signaller.interrupted = true;
                    }
                    if (signaller.interrupted) {
                        break;
                    }
                }
            }
            if (signaller != null && z) {
                signaller.thread = null;
                if (obj == null) {
                    e();
                }
            }
            if (obj != null || (obj = this.a) != null) {
                o();
            }
            if (obj != null || (signaller != null && signaller.interrupted)) {
                return obj;
            }
        }
        throw new TimeoutException();
    }

    public String toString() {
        String str;
        a aVar;
        Object obj = this.a;
        int i = 0;
        for (Completion completion = this.f0; completion != null; completion = completion.next) {
            i++;
        }
        StringBuilder sb = new StringBuilder();
        sb.append(super.toString());
        if (obj != null) {
            if (obj instanceof a) {
                if (((a) obj).a != null) {
                    str = "[Completed exceptionally: " + aVar.a + "]";
                }
            }
            str = "[Completed normally]";
        } else if (i == 0) {
            str = "[Not completed]";
        } else {
            str = "[Not completed, " + i + " dependents]";
        }
        sb.append(str);
        return sb.toString();
    }

    public final boolean u(Completion completion) {
        Completion completion2 = this.f0;
        n(completion, completion2);
        return j0.compareAndSwapObject(this, l0, completion2, completion);
    }

    public final Object v(boolean z) {
        Object obj;
        boolean z2 = false;
        Signaller signaller = null;
        while (true) {
            obj = this.a;
            if (obj == null) {
                if (signaller != null) {
                    if (!z2) {
                        z2 = u(signaller);
                    } else {
                        try {
                            java8.util.concurrent.a.v(signaller);
                        } catch (InterruptedException unused) {
                            signaller.interrupted = true;
                        }
                        if (signaller.interrupted && z) {
                            break;
                        }
                    }
                } else {
                    signaller = new Signaller(z, 0L, 0L);
                    if (Thread.currentThread() instanceof java8.util.concurrent.b) {
                        java8.util.concurrent.a.q(k(), signaller);
                    }
                }
            } else {
                break;
            }
        }
        if (signaller != null && z2) {
            signaller.thread = null;
            if (!z && signaller.interrupted) {
                Thread.currentThread().interrupt();
            }
            if (obj == null) {
                e();
            }
        }
        if (obj != null || (obj = this.a) != null) {
            o();
        }
        return obj;
    }

    @Override // java.util.concurrent.Future
    public T get(long j, TimeUnit timeUnit) throws InterruptedException, ExecutionException, TimeoutException {
        long nanos = timeUnit.toNanos(j);
        Object obj = this.a;
        if (obj == null) {
            obj = t(nanos);
        }
        return (T) q(obj);
    }
}
