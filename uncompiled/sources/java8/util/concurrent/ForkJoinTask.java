package java8.util.concurrent;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.lang.ref.Reference;
import java.lang.ref.ReferenceQueue;
import java.lang.ref.WeakReference;
import java.lang.reflect.Constructor;
import java.util.Collection;
import java.util.List;
import java.util.RandomAccess;
import java.util.concurrent.Callable;
import java.util.concurrent.CancellationException;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Future;
import java.util.concurrent.RunnableFuture;
import java.util.concurrent.locks.ReentrantLock;
import java8.util.concurrent.a;
import sun.misc.Unsafe;

/* loaded from: classes2.dex */
public abstract class ForkJoinTask<V> implements Future<V>, Serializable {
    public static final int CANCELLED = -1073741824;
    public static final int DONE_MASK = -268435456;
    public static final int EXCEPTIONAL = Integer.MIN_VALUE;
    public static final int NORMAL = -268435456;
    public static final int SIGNAL = 65536;
    public static final int SMASK = 65535;
    public static final a[] a = new a[32];
    public static final ReentrantLock f0 = new ReentrantLock();
    public static final ReferenceQueue<ForkJoinTask<?>> g0 = new ReferenceQueue<>();
    public static final Unsafe h0;
    public static final long i0;
    private static final long serialVersionUID = -7721805057305804111L;
    public volatile int status;

    /* loaded from: classes2.dex */
    public static final class AdaptedCallable<T> extends ForkJoinTask<T> implements RunnableFuture<T> {
        private static final long serialVersionUID = 2838392045355241008L;
        public final Callable<? extends T> callable;
        public T result;

        public AdaptedCallable(Callable<? extends T> callable) {
            this.callable = (Callable) rl2.f(callable);
        }

        @Override // java8.util.concurrent.ForkJoinTask
        public final boolean exec() {
            try {
                this.result = this.callable.call();
                return true;
            } catch (RuntimeException e) {
                throw e;
            } catch (Exception e2) {
                throw new RuntimeException(e2);
            }
        }

        @Override // java8.util.concurrent.ForkJoinTask
        public final T getRawResult() {
            return this.result;
        }

        @Override // java.util.concurrent.RunnableFuture, java.lang.Runnable
        public final void run() {
            invoke();
        }

        @Override // java8.util.concurrent.ForkJoinTask
        public final void setRawResult(T t) {
            this.result = t;
        }

        public String toString() {
            return super.toString() + "[Wrapped task = " + this.callable + "]";
        }
    }

    /* loaded from: classes2.dex */
    public static final class AdaptedRunnable<T> extends ForkJoinTask<T> implements RunnableFuture<T> {
        private static final long serialVersionUID = 5232453952276885070L;
        public T result;
        public final Runnable runnable;

        public AdaptedRunnable(Runnable runnable, T t) {
            this.runnable = (Runnable) rl2.f(runnable);
            this.result = t;
        }

        @Override // java8.util.concurrent.ForkJoinTask
        public final boolean exec() {
            this.runnable.run();
            return true;
        }

        @Override // java8.util.concurrent.ForkJoinTask
        public final T getRawResult() {
            return this.result;
        }

        @Override // java.util.concurrent.RunnableFuture, java.lang.Runnable
        public final void run() {
            invoke();
        }

        @Override // java8.util.concurrent.ForkJoinTask
        public final void setRawResult(T t) {
            this.result = t;
        }

        public String toString() {
            return super.toString() + "[Wrapped task = " + this.runnable + "]";
        }
    }

    /* loaded from: classes2.dex */
    public static final class AdaptedRunnableAction extends ForkJoinTask<Void> implements RunnableFuture<Void> {
        private static final long serialVersionUID = 5232453952276885070L;
        public final Runnable runnable;

        public AdaptedRunnableAction(Runnable runnable) {
            this.runnable = (Runnable) rl2.f(runnable);
        }

        @Override // java8.util.concurrent.ForkJoinTask
        public final boolean exec() {
            this.runnable.run();
            return true;
        }

        @Override // java8.util.concurrent.ForkJoinTask
        public final Void getRawResult() {
            return null;
        }

        @Override // java.util.concurrent.RunnableFuture, java.lang.Runnable
        public final void run() {
            invoke();
        }

        @Override // java8.util.concurrent.ForkJoinTask
        public final void setRawResult(Void r1) {
        }

        public String toString() {
            return super.toString() + "[Wrapped task = " + this.runnable + "]";
        }
    }

    /* loaded from: classes2.dex */
    public static final class RunnableExecuteAction extends ForkJoinTask<Void> {
        private static final long serialVersionUID = 5232453952276885070L;
        public final Runnable runnable;

        public RunnableExecuteAction(Runnable runnable) {
            this.runnable = (Runnable) rl2.f(runnable);
        }

        @Override // java8.util.concurrent.ForkJoinTask
        public final boolean exec() {
            this.runnable.run();
            return true;
        }

        @Override // java8.util.concurrent.ForkJoinTask
        public final Void getRawResult() {
            return null;
        }

        @Override // java8.util.concurrent.ForkJoinTask
        public void internalPropagateException(Throwable th) {
            ForkJoinTask.rethrow(th);
        }

        @Override // java8.util.concurrent.ForkJoinTask
        public final void setRawResult(Void r1) {
        }
    }

    /* loaded from: classes2.dex */
    public static final class a extends WeakReference<ForkJoinTask<?>> {
        public final Throwable a;
        public a b;
        public final long c;
        public final int d;

        public a(ForkJoinTask<?> forkJoinTask, Throwable th, a aVar, ReferenceQueue<ForkJoinTask<?>> referenceQueue) {
            super(forkJoinTask, referenceQueue);
            this.a = th;
            this.b = aVar;
            this.c = Thread.currentThread().getId();
            this.d = System.identityHashCode(forkJoinTask);
        }
    }

    static {
        Unsafe unsafe = d.a;
        h0 = unsafe;
        try {
            i0 = unsafe.objectFieldOffset(ForkJoinTask.class.getDeclaredField("status"));
        } catch (Exception e) {
            throw new ExceptionInInitializerError(e);
        }
    }

    public static ForkJoinTask<?> adapt(Runnable runnable) {
        return new AdaptedRunnableAction(runnable);
    }

    public static final void cancelIgnoringExceptions(ForkJoinTask<?> forkJoinTask) {
        if (forkJoinTask == null || forkJoinTask.status < 0) {
            return;
        }
        try {
            forkJoinTask.cancel(false);
        } catch (Throwable unused) {
        }
    }

    public static void e() {
        while (true) {
            Reference<? extends ForkJoinTask<?>> poll = g0.poll();
            if (poll == null) {
                return;
            }
            if (poll instanceof a) {
                a[] aVarArr = a;
                int length = ((a) poll).d & (aVarArr.length - 1);
                a aVar = aVarArr[length];
                a aVar2 = null;
                while (true) {
                    if (aVar != null) {
                        a aVar3 = aVar.b;
                        if (aVar != poll) {
                            aVar2 = aVar;
                            aVar = aVar3;
                        } else if (aVar2 == null) {
                            aVarArr[length] = aVar3;
                        } else {
                            aVar2.b = aVar3;
                        }
                    }
                }
            }
        }
    }

    public static java8.util.concurrent.a getPool() {
        Thread currentThread = Thread.currentThread();
        if (currentThread instanceof b) {
            return ((b) currentThread).a;
        }
        return null;
    }

    public static int getQueuedTaskCount() {
        a.g e;
        Thread currentThread = Thread.currentThread();
        if (currentThread instanceof b) {
            e = ((b) currentThread).f0;
        } else {
            e = java8.util.concurrent.a.e();
        }
        if (e == null) {
            return 0;
        }
        return e.l();
    }

    public static int getSurplusQueuedTaskCount() {
        return java8.util.concurrent.a.p();
    }

    public static final void helpExpungeStaleExceptions() {
        ReentrantLock reentrantLock = f0;
        if (reentrantLock.tryLock()) {
            try {
                e();
            } finally {
                reentrantLock.unlock();
            }
        }
    }

    public static void helpQuiesce() {
        Thread currentThread = Thread.currentThread();
        if (currentThread instanceof b) {
            b bVar = (b) currentThread;
            bVar.a.s(bVar.f0);
            return;
        }
        java8.util.concurrent.a.B();
    }

    public static boolean inForkJoinPool() {
        return Thread.currentThread() instanceof b;
    }

    public static void invokeAll(ForkJoinTask<?> forkJoinTask, ForkJoinTask<?> forkJoinTask2) {
        forkJoinTask2.fork();
        int b = forkJoinTask.b() & (-268435456);
        if (b != -268435456) {
            forkJoinTask.i(b);
        }
        int c = forkJoinTask2.c() & (-268435456);
        if (c != -268435456) {
            forkJoinTask2.i(c);
        }
    }

    public static ForkJoinTask<?> peekNextLocalTask() {
        a.g e;
        Thread currentThread = Thread.currentThread();
        if (currentThread instanceof b) {
            e = ((b) currentThread).f0;
        } else {
            e = java8.util.concurrent.a.e();
        }
        if (e == null) {
            return null;
        }
        return e.h();
    }

    public static ForkJoinTask<?> pollNextLocalTask() {
        Thread currentThread = Thread.currentThread();
        if (currentThread instanceof b) {
            return ((b) currentThread).f0.g();
        }
        return null;
    }

    public static ForkJoinTask<?> pollSubmission() {
        Thread currentThread = Thread.currentThread();
        if (currentThread instanceof b) {
            return ((b) currentThread).a.A();
        }
        return null;
    }

    public static ForkJoinTask<?> pollTask() {
        Thread currentThread = Thread.currentThread();
        if (currentThread instanceof b) {
            b bVar = (b) currentThread;
            return bVar.a.y(bVar.f0);
        }
        return null;
    }

    private void readObject(ObjectInputStream objectInputStream) throws IOException, ClassNotFoundException {
        objectInputStream.defaultReadObject();
        Object readObject = objectInputStream.readObject();
        if (readObject != null) {
            k((Throwable) readObject);
        }
    }

    public static void rethrow(Throwable th) {
        uncheckedThrow(th);
    }

    public static <T extends Throwable> void uncheckedThrow(Throwable th) throws Throwable {
        if (th != null) {
            throw th;
        }
        throw new Error("Unknown Exception");
    }

    private void writeObject(ObjectOutputStream objectOutputStream) throws IOException {
        objectOutputStream.defaultWriteObject();
        objectOutputStream.writeObject(getException());
    }

    public final void a() {
        int identityHashCode = System.identityHashCode(this);
        ReentrantLock reentrantLock = f0;
        reentrantLock.lock();
        try {
            a[] aVarArr = a;
            int length = identityHashCode & (aVarArr.length - 1);
            a aVar = aVarArr[length];
            a aVar2 = null;
            while (true) {
                if (aVar == null) {
                    break;
                }
                a aVar3 = aVar.b;
                if (aVar.get() != this) {
                    aVar2 = aVar;
                    aVar = aVar3;
                } else if (aVar2 == null) {
                    aVarArr[length] = aVar3;
                } else {
                    aVar2.b = aVar3;
                }
            }
            e();
            this.status = 0;
        } finally {
            reentrantLock.unlock();
        }
    }

    public final int b() {
        int doExec = doExec();
        if (doExec < 0) {
            return doExec;
        }
        Thread currentThread = Thread.currentThread();
        if (currentThread instanceof b) {
            b bVar = (b) currentThread;
            return bVar.a.a(bVar.f0, this, 0L);
        }
        return f();
    }

    public final int c() {
        int doExec;
        int i = this.status;
        if (i < 0) {
            return i;
        }
        Thread currentThread = Thread.currentThread();
        if (currentThread instanceof b) {
            b bVar = (b) currentThread;
            a.g gVar = bVar.f0;
            return (!gVar.q(this) || (doExec = doExec()) >= 0) ? bVar.a.a(gVar, this, 0L) : doExec;
        }
        return f();
    }

    @Override // java.util.concurrent.Future
    public boolean cancel(boolean z) {
        return (j(CANCELLED) & (-268435456)) == -1073741824;
    }

    public final boolean compareAndSetForkJoinTaskTag(short s, short s2) {
        int i;
        do {
            i = this.status;
            if (((short) i) != s) {
                return false;
            }
        } while (!h0.compareAndSwapInt(this, i0, i, (65535 & s2) | ((-65536) & i)));
        return true;
    }

    public void complete(V v) {
        try {
            setRawResult(v);
            j(-268435456);
        } catch (Throwable th) {
            k(th);
        }
    }

    public void completeExceptionally(Throwable th) {
        if (!(th instanceof RuntimeException) && !(th instanceof Error)) {
            th = new RuntimeException(th);
        }
        k(th);
    }

    public final int doExec() {
        int i = this.status;
        if (i >= 0) {
            try {
                return exec() ? j(-268435456) : i;
            } catch (Throwable th) {
                return k(th);
            }
        }
        return i;
    }

    public abstract boolean exec();

    public final int f() {
        int doExec;
        boolean z = false;
        if (this instanceof CountedCompleter) {
            doExec = java8.util.concurrent.a.r0.i((CountedCompleter) this, 0);
        } else {
            doExec = java8.util.concurrent.a.r0.K(this) ? doExec() : 0;
        }
        if (doExec >= 0) {
            int i = this.status;
            if (i >= 0) {
                int i2 = i;
                do {
                    if (h0.compareAndSwapInt(this, i0, i2, i2 | 65536)) {
                        synchronized (this) {
                            if (this.status >= 0) {
                                try {
                                    wait(0L);
                                } catch (InterruptedException unused) {
                                    z = true;
                                }
                            } else {
                                notifyAll();
                            }
                        }
                    }
                    i2 = this.status;
                } while (i2 >= 0);
                if (z) {
                    Thread.currentThread().interrupt();
                }
                return i2;
            }
            return i;
        }
        return doExec;
    }

    public final ForkJoinTask<V> fork() {
        Thread currentThread = Thread.currentThread();
        if (currentThread instanceof b) {
            ((b) currentThread).f0.k(this);
        } else {
            java8.util.concurrent.a.r0.j(this);
        }
        return this;
    }

    public final int g() throws InterruptedException {
        int doExec;
        if (!Thread.interrupted()) {
            int i = this.status;
            if (i < 0) {
                return i;
            }
            if (this instanceof CountedCompleter) {
                doExec = java8.util.concurrent.a.r0.i((CountedCompleter) this, 0);
            } else {
                doExec = java8.util.concurrent.a.r0.K(this) ? doExec() : 0;
            }
            if (doExec < 0) {
                return doExec;
            }
            while (true) {
                int i2 = this.status;
                if (i2 < 0) {
                    return i2;
                }
                if (h0.compareAndSwapInt(this, i0, i2, i2 | 65536)) {
                    synchronized (this) {
                        if (this.status >= 0) {
                            wait(0L);
                        } else {
                            notifyAll();
                        }
                    }
                }
            }
        } else {
            throw new InterruptedException();
        }
    }

    @Override // java.util.concurrent.Future
    public final V get() throws InterruptedException, ExecutionException {
        int c = (Thread.currentThread() instanceof b ? c() : g()) & (-268435456);
        if (c != -1073741824) {
            if (c != Integer.MIN_VALUE) {
                return getRawResult();
            }
            throw new ExecutionException(h());
        }
        throw new CancellationException();
    }

    public final Throwable getException() {
        int i = this.status & (-268435456);
        if (i >= -268435456) {
            return null;
        }
        if (i == -1073741824) {
            return new CancellationException();
        }
        return h();
    }

    public final short getForkJoinTaskTag() {
        return (short) this.status;
    }

    public abstract V getRawResult();

    public final Throwable h() {
        Throwable th;
        Constructor<?>[] constructors;
        int identityHashCode = System.identityHashCode(this);
        ReentrantLock reentrantLock = f0;
        reentrantLock.lock();
        try {
            e();
            a[] aVarArr = a;
            a aVar = aVarArr[identityHashCode & (aVarArr.length - 1)];
            while (aVar != null) {
                if (aVar.get() == this) {
                    break;
                }
                aVar = aVar.b;
            }
            reentrantLock.unlock();
            Constructor<?> constructor = null;
            if (aVar == null || (th = aVar.a) == null) {
                return null;
            }
            if (aVar.c != Thread.currentThread().getId()) {
                try {
                    for (Constructor<?> constructor2 : th.getClass().getConstructors()) {
                        Class<?>[] parameterTypes = constructor2.getParameterTypes();
                        if (parameterTypes.length == 0) {
                            constructor = constructor2;
                        } else if (parameterTypes.length == 1 && parameterTypes[0] == Throwable.class) {
                            return (Throwable) constructor2.newInstance(th);
                        }
                    }
                    if (constructor != null) {
                        Throwable th2 = (Throwable) constructor.newInstance(new Object[0]);
                        th2.initCause(th);
                        return th2;
                    }
                } catch (Exception unused) {
                }
            }
            return th;
        } catch (Throwable th3) {
            reentrantLock.unlock();
            throw th3;
        }
    }

    public final void i(int i) {
        if (i == -1073741824) {
            throw new CancellationException();
        }
        if (i == Integer.MIN_VALUE) {
            rethrow(h());
        }
    }

    public void internalPropagateException(Throwable th) {
    }

    public final void internalWait(long j) {
        int i = this.status;
        if (i < 0 || !h0.compareAndSwapInt(this, i0, i, i | 65536)) {
            return;
        }
        synchronized (this) {
            if (this.status >= 0) {
                try {
                    wait(j);
                } catch (InterruptedException unused) {
                }
            } else {
                notifyAll();
            }
        }
    }

    public final V invoke() {
        int b = b() & (-268435456);
        if (b != -268435456) {
            i(b);
        }
        return getRawResult();
    }

    @Override // java.util.concurrent.Future
    public final boolean isCancelled() {
        return (this.status & (-268435456)) == -1073741824;
    }

    public final boolean isCompletedAbnormally() {
        return this.status < -268435456;
    }

    public final boolean isCompletedNormally() {
        return (this.status & (-268435456)) == -268435456;
    }

    @Override // java.util.concurrent.Future
    public final boolean isDone() {
        return this.status < 0;
    }

    public final int j(int i) {
        int i2;
        do {
            i2 = this.status;
            if (i2 < 0) {
                return i2;
            }
        } while (!h0.compareAndSwapInt(this, i0, i2, i2 | i));
        if ((i2 >>> 16) != 0) {
            synchronized (this) {
                notifyAll();
            }
        }
        return i;
    }

    public final V join() {
        int c = c() & (-268435456);
        if (c != -268435456) {
            i(c);
        }
        return getRawResult();
    }

    public final int k(Throwable th) {
        int recordExceptionalCompletion = recordExceptionalCompletion(th);
        if (((-268435456) & recordExceptionalCompletion) == Integer.MIN_VALUE) {
            internalPropagateException(th);
        }
        return recordExceptionalCompletion;
    }

    public final void quietlyComplete() {
        j(-268435456);
    }

    public final void quietlyInvoke() {
        b();
    }

    public final void quietlyJoin() {
        c();
    }

    /* JADX WARN: Code restructure failed: missing block: B:7:0x001a, code lost:
        r2[r0] = new java8.util.concurrent.ForkJoinTask.a(r6, r7, r2[r0], java8.util.concurrent.ForkJoinTask.g0);
     */
    /*
        Code decompiled incorrectly, please refer to instructions dump.
        To view partially-correct code enable 'Show inconsistent code' option in preferences
    */
    public final int recordExceptionalCompletion(java.lang.Throwable r7) {
        /*
            r6 = this;
            int r0 = r6.status
            if (r0 < 0) goto L3e
            int r0 = java.lang.System.identityHashCode(r6)
            java.util.concurrent.locks.ReentrantLock r1 = java8.util.concurrent.ForkJoinTask.f0
            r1.lock()
            e()     // Catch: java.lang.Throwable -> L39
            java8.util.concurrent.ForkJoinTask$a[] r2 = java8.util.concurrent.ForkJoinTask.a     // Catch: java.lang.Throwable -> L39
            int r3 = r2.length     // Catch: java.lang.Throwable -> L39
            int r3 = r3 + (-1)
            r0 = r0 & r3
            r3 = r2[r0]     // Catch: java.lang.Throwable -> L39
        L18:
            if (r3 != 0) goto L26
            java8.util.concurrent.ForkJoinTask$a r3 = new java8.util.concurrent.ForkJoinTask$a     // Catch: java.lang.Throwable -> L39
            r4 = r2[r0]     // Catch: java.lang.Throwable -> L39
            java.lang.ref.ReferenceQueue<java8.util.concurrent.ForkJoinTask<?>> r5 = java8.util.concurrent.ForkJoinTask.g0     // Catch: java.lang.Throwable -> L39
            r3.<init>(r6, r7, r4, r5)     // Catch: java.lang.Throwable -> L39
            r2[r0] = r3     // Catch: java.lang.Throwable -> L39
            goto L2c
        L26:
            java.lang.Object r4 = r3.get()     // Catch: java.lang.Throwable -> L39
            if (r4 != r6) goto L36
        L2c:
            r1.unlock()
            r7 = -2147483648(0xffffffff80000000, float:-0.0)
            int r0 = r6.j(r7)
            goto L3e
        L36:
            java8.util.concurrent.ForkJoinTask$a r3 = r3.b     // Catch: java.lang.Throwable -> L39
            goto L18
        L39:
            r7 = move-exception
            r1.unlock()
            throw r7
        L3e:
            return r0
        */
        throw new UnsupportedOperationException("Method not decompiled: java8.util.concurrent.ForkJoinTask.recordExceptionalCompletion(java.lang.Throwable):int");
    }

    public void reinitialize() {
        if ((this.status & (-268435456)) == Integer.MIN_VALUE) {
            a();
        } else {
            this.status = 0;
        }
    }

    public final short setForkJoinTaskTag(short s) {
        Unsafe unsafe;
        long j;
        int i;
        do {
            unsafe = h0;
            j = i0;
            i = this.status;
        } while (!unsafe.compareAndSwapInt(this, j, i, ((-65536) & i) | (65535 & s)));
        return (short) i;
    }

    public abstract void setRawResult(V v);

    public boolean tryUnfork() {
        Thread currentThread = Thread.currentThread();
        if (currentThread instanceof b) {
            return ((b) currentThread).f0.q(this);
        }
        return java8.util.concurrent.a.r0.K(this);
    }

    public static <T> ForkJoinTask<T> adapt(Runnable runnable, T t) {
        return new AdaptedRunnable(runnable, t);
    }

    public static <T> ForkJoinTask<T> adapt(Callable<? extends T> callable) {
        return new AdaptedCallable(callable);
    }

    public static void invokeAll(ForkJoinTask<?>... forkJoinTaskArr) {
        int length = forkJoinTaskArr.length - 1;
        Throwable th = null;
        for (int i = length; i >= 0; i--) {
            ForkJoinTask<?> forkJoinTask = forkJoinTaskArr[i];
            if (forkJoinTask == null) {
                if (th == null) {
                    th = new NullPointerException();
                }
            } else if (i != 0) {
                forkJoinTask.fork();
            } else if (forkJoinTask.b() < -268435456 && th == null) {
                th = forkJoinTask.getException();
            }
        }
        for (int i2 = 1; i2 <= length; i2++) {
            ForkJoinTask<?> forkJoinTask2 = forkJoinTaskArr[i2];
            if (forkJoinTask2 != null) {
                if (th != null) {
                    forkJoinTask2.cancel(false);
                } else if (forkJoinTask2.c() < -268435456) {
                    th = forkJoinTask2.getException();
                }
            }
        }
        if (th != null) {
            rethrow(th);
        }
    }

    /* JADX WARN: Removed duplicated region for block: B:23:0x0052 A[LOOP:0: B:23:0x0052->B:66:0x0052, LOOP_START] */
    @Override // java.util.concurrent.Future
    /*
        Code decompiled incorrectly, please refer to instructions dump.
        To view partially-correct code enable 'Show inconsistent code' option in preferences
    */
    public final V get(long r11, java.util.concurrent.TimeUnit r13) throws java.lang.InterruptedException, java.util.concurrent.ExecutionException, java.util.concurrent.TimeoutException {
        /*
            r10 = this;
            long r11 = r13.toNanos(r11)
            boolean r13 = java.lang.Thread.interrupted()
            if (r13 != 0) goto Lb8
            int r13 = r10.status
            if (r13 < 0) goto L8b
            r0 = 0
            int r2 = (r11 > r0 ? 1 : (r11 == r0 ? 0 : -1))
            if (r2 <= 0) goto L8b
            long r2 = java.lang.System.nanoTime()
            long r2 = r2 + r11
            int r11 = (r2 > r0 ? 1 : (r2 == r0 ? 0 : -1))
            if (r11 != 0) goto L1f
            r2 = 1
        L1f:
            java.lang.Thread r11 = java.lang.Thread.currentThread()
            boolean r12 = r11 instanceof java8.util.concurrent.b
            if (r12 == 0) goto L32
            java8.util.concurrent.b r11 = (java8.util.concurrent.b) r11
            java8.util.concurrent.a r12 = r11.a
            java8.util.concurrent.a$g r11 = r11.f0
            int r13 = r12.a(r11, r10, r2)
            goto L8b
        L32:
            boolean r11 = r10 instanceof java8.util.concurrent.CountedCompleter
            r12 = 0
            if (r11 == 0) goto L42
            java8.util.concurrent.a r11 = java8.util.concurrent.a.r0
            r13 = r10
            java8.util.concurrent.CountedCompleter r13 = (java8.util.concurrent.CountedCompleter) r13
            int r11 = r11.i(r13, r12)
        L40:
            r13 = r11
            goto L50
        L42:
            java8.util.concurrent.a r11 = java8.util.concurrent.a.r0
            boolean r11 = r11.K(r10)
            if (r11 == 0) goto L4f
            int r11 = r10.doExec()
            goto L40
        L4f:
            r13 = r12
        L50:
            if (r13 < 0) goto L8b
        L52:
            int r8 = r10.status
            if (r8 < 0) goto L8a
            long r11 = java.lang.System.nanoTime()
            long r11 = r2 - r11
            int r13 = (r11 > r0 ? 1 : (r11 == r0 ? 0 : -1))
            if (r13 <= 0) goto L8a
            java.util.concurrent.TimeUnit r13 = java.util.concurrent.TimeUnit.NANOSECONDS
            long r11 = r13.toMillis(r11)
            int r13 = (r11 > r0 ? 1 : (r11 == r0 ? 0 : -1))
            if (r13 <= 0) goto L52
            sun.misc.Unsafe r4 = java8.util.concurrent.ForkJoinTask.h0
            long r6 = java8.util.concurrent.ForkJoinTask.i0
            r13 = 65536(0x10000, float:9.1835E-41)
            r9 = r8 | r13
            r5 = r10
            boolean r13 = r4.compareAndSwapInt(r5, r6, r8, r9)
            if (r13 == 0) goto L52
            monitor-enter(r10)
            int r13 = r10.status     // Catch: java.lang.Throwable -> L87
            if (r13 < 0) goto L82
            r10.wait(r11)     // Catch: java.lang.Throwable -> L87
            goto L85
        L82:
            r10.notifyAll()     // Catch: java.lang.Throwable -> L87
        L85:
            monitor-exit(r10)     // Catch: java.lang.Throwable -> L87
            goto L52
        L87:
            r11 = move-exception
            monitor-exit(r10)     // Catch: java.lang.Throwable -> L87
            throw r11
        L8a:
            r13 = r8
        L8b:
            if (r13 < 0) goto L8f
            int r13 = r10.status
        L8f:
            r11 = -268435456(0xfffffffff0000000, float:-1.5845633E29)
            r12 = r13 & r11
            if (r12 == r11) goto Lb3
            r11 = -1073741824(0xffffffffc0000000, float:-2.0)
            if (r12 == r11) goto Lad
            r11 = -2147483648(0xffffffff80000000, float:-0.0)
            if (r12 == r11) goto La3
            java.util.concurrent.TimeoutException r11 = new java.util.concurrent.TimeoutException
            r11.<init>()
            throw r11
        La3:
            java.util.concurrent.ExecutionException r11 = new java.util.concurrent.ExecutionException
            java.lang.Throwable r12 = r10.h()
            r11.<init>(r12)
            throw r11
        Lad:
            java.util.concurrent.CancellationException r11 = new java.util.concurrent.CancellationException
            r11.<init>()
            throw r11
        Lb3:
            java.lang.Object r11 = r10.getRawResult()
            return r11
        Lb8:
            java.lang.InterruptedException r11 = new java.lang.InterruptedException
            r11.<init>()
            throw r11
        */
        throw new UnsupportedOperationException("Method not decompiled: java8.util.concurrent.ForkJoinTask.get(long, java.util.concurrent.TimeUnit):java.lang.Object");
    }

    public static <T extends ForkJoinTask<?>> Collection<T> invokeAll(Collection<T> collection) {
        if ((collection instanceof RandomAccess) && (collection instanceof List)) {
            List list = (List) collection;
            Throwable th = null;
            int size = list.size() - 1;
            for (int i = size; i >= 0; i--) {
                ForkJoinTask forkJoinTask = (ForkJoinTask) list.get(i);
                if (forkJoinTask == null) {
                    if (th == null) {
                        th = new NullPointerException();
                    }
                } else if (i != 0) {
                    forkJoinTask.fork();
                } else if (forkJoinTask.b() < -268435456 && th == null) {
                    th = forkJoinTask.getException();
                }
            }
            for (int i2 = 1; i2 <= size; i2++) {
                ForkJoinTask forkJoinTask2 = (ForkJoinTask) list.get(i2);
                if (forkJoinTask2 != null) {
                    if (th != null) {
                        forkJoinTask2.cancel(false);
                    } else if (forkJoinTask2.c() < -268435456) {
                        th = forkJoinTask2.getException();
                    }
                }
            }
            if (th != null) {
                rethrow(th);
            }
            return collection;
        }
        invokeAll((ForkJoinTask[]) collection.toArray(new ForkJoinTask[0]));
        return collection;
    }
}
