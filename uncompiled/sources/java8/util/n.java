package java8.util;

import java.util.Map;
import java.util.concurrent.ConcurrentMap;
import java8.util.ImmutableCollections;

/* compiled from: Maps.java */
/* loaded from: classes2.dex */
public final class n {
    public static <K, V> Map<K, V> a(Map.Entry<? extends K, ? extends V>... entryArr) {
        if (entryArr.length == 0) {
            return ImmutableCollections.c();
        }
        if (entryArr.length == 1) {
            return new ImmutableCollections.Map1(entryArr[0].getKey(), entryArr[0].getValue());
        }
        Object[] objArr = new Object[entryArr.length << 1];
        int i = 0;
        for (Map.Entry<? extends K, ? extends V> entry : entryArr) {
            int i2 = i + 1;
            objArr[i] = entry.getKey();
            i = i2 + 1;
            objArr[i2] = entry.getValue();
        }
        return new ImmutableCollections.MapN(objArr);
    }

    public static <K, V> V b(Map<K, V> map, K k, V v) {
        rl2.f(map);
        if (map instanceof ConcurrentMap) {
            return (V) ((ConcurrentMap) map).putIfAbsent(k, v);
        }
        V v2 = map.get(k);
        return v2 == null ? map.put(k, v) : v2;
    }
}
