package java8.util;

import build.IgnoreJava8API;
import java.util.Comparator;
import java.util.Spliterator;
import java.util.function.Consumer;

/* compiled from: DelegatingSpliterator.java */
@IgnoreJava8API
/* loaded from: classes2.dex */
public final class f<T> implements s<T> {
    public final Spliterator<T> a;

    /* compiled from: DelegatingSpliterator.java */
    /* loaded from: classes2.dex */
    public static final class a<T> implements Consumer<T> {
        public final m60<T> a;

        /* compiled from: DelegatingSpliterator.java */
        /* renamed from: java8.util.f$a$a  reason: collision with other inner class name */
        /* loaded from: classes2.dex */
        public class C0185a implements m60<T> {
            public final /* synthetic */ Consumer a;

            public C0185a(a aVar, Consumer consumer) {
                this.a = consumer;
            }

            @Override // defpackage.m60
            public void accept(T t) {
                this.a.accept(t);
            }
        }

        public a(m60<T> m60Var) {
            rl2.f(m60Var);
            this.a = m60Var;
        }

        @Override // java.util.function.Consumer
        public void accept(T t) {
            this.a.accept(t);
        }

        @Override // java.util.function.Consumer
        @IgnoreJava8API
        public Consumer<T> andThen(Consumer<? super T> consumer) {
            rl2.f(consumer);
            return new a(p60.a(this.a, new C0185a(this, consumer)));
        }
    }

    public f(Spliterator<T> spliterator) {
        rl2.f(spliterator);
        this.a = spliterator;
    }

    @Override // java8.util.s
    public void a(m60<? super T> m60Var) {
        this.a.forEachRemaining(new a(m60Var));
    }

    @Override // java8.util.s
    public int b() {
        return this.a.characteristics();
    }

    @Override // java8.util.s
    public s<T> c() {
        Spliterator<T> trySplit = this.a.trySplit();
        if (trySplit == null) {
            return null;
        }
        return new f(trySplit);
    }

    @Override // java8.util.s
    public boolean d(m60<? super T> m60Var) {
        return this.a.tryAdvance(new a(m60Var));
    }

    @Override // java8.util.s
    public Comparator<? super T> f() {
        return this.a.getComparator();
    }

    @Override // java8.util.s
    public boolean h(int i) {
        return this.a.hasCharacteristics(i);
    }

    @Override // java8.util.s
    public long i() {
        return this.a.getExactSizeIfKnown();
    }

    @Override // java8.util.s
    public long l() {
        return this.a.estimateSize();
    }
}
