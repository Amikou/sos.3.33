package java8.util;

import java.io.IOException;
import java.io.InvalidObjectException;
import java.io.ObjectInputStream;
import java.io.Serializable;
import java.util.AbstractCollection;
import java.util.AbstractMap;
import java.util.AbstractSet;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import java.util.ListIterator;
import java.util.Map;
import java.util.NoSuchElementException;
import java.util.RandomAccess;
import java.util.Set;
import java8.util.h;

/* loaded from: classes2.dex */
public final class ImmutableCollections {
    public static final int a;

    /* loaded from: classes2.dex */
    public static abstract class AbstractImmutableMap<K, V> extends AbstractMap<K, V> implements Serializable {
        @Override // java.util.AbstractMap, java.util.Map
        public void clear() {
            throw ImmutableCollections.h();
        }

        @Override // java.util.AbstractMap, java.util.Map
        public V put(K k, V v) {
            throw ImmutableCollections.h();
        }

        @Override // java.util.AbstractMap, java.util.Map
        public void putAll(Map<? extends K, ? extends V> map) {
            throw ImmutableCollections.h();
        }

        @Override // java.util.AbstractMap, java.util.Map
        public V remove(Object obj) {
            throw ImmutableCollections.h();
        }
    }

    /* loaded from: classes2.dex */
    public static final class ListN<E> extends b<E> implements Serializable {
        public static final List<?> EMPTY_LIST = new ListN(new Object[0]);
        private final E[] elements;

        /* JADX WARN: Multi-variable type inference failed */
        public ListN(E... eArr) {
            E[] eArr2 = (E[]) new Object[eArr.length];
            for (int i = 0; i < eArr.length; i++) {
                eArr2[i] = rl2.f(eArr[i]);
            }
            this.elements = eArr2;
        }

        private void readObject(ObjectInputStream objectInputStream) throws IOException, ClassNotFoundException {
            throw new InvalidObjectException("not serial proxy");
        }

        private Object writeReplace() {
            return new ColSer(1, this.elements);
        }

        @Override // java.util.List
        public E get(int i) {
            return this.elements[i];
        }

        @Override // java.util.AbstractCollection, java.util.Collection, java.util.List
        public boolean isEmpty() {
            return size() == 0;
        }

        @Override // java.util.AbstractCollection, java.util.Collection, java.util.List
        public int size() {
            return this.elements.length;
        }
    }

    /* loaded from: classes2.dex */
    public static final class Map1<K, V> extends AbstractImmutableMap<K, V> {
        private final K k0;
        private final V v0;

        public Map1(K k, V v) {
            this.k0 = (K) rl2.f(k);
            this.v0 = (V) rl2.f(v);
        }

        private void readObject(ObjectInputStream objectInputStream) throws IOException, ClassNotFoundException {
            throw new InvalidObjectException("not serial proxy");
        }

        private Object writeReplace() {
            return new ColSer(3, this.k0, this.v0);
        }

        @Override // java.util.AbstractMap, java.util.Map
        public boolean containsKey(Object obj) {
            return obj.equals(this.k0);
        }

        @Override // java.util.AbstractMap, java.util.Map
        public boolean containsValue(Object obj) {
            return obj.equals(this.v0);
        }

        @Override // java.util.AbstractMap, java.util.Map
        public Set<Map.Entry<K, V>> entrySet() {
            return r.a(new px1(this.k0, this.v0));
        }

        @Override // java.util.AbstractMap, java.util.Map
        public int hashCode() {
            return this.k0.hashCode() ^ this.v0.hashCode();
        }
    }

    /* loaded from: classes2.dex */
    public static final class MapN<K, V> extends AbstractImmutableMap<K, V> {
        public static final Map<?, ?> EMPTY_MAP = new MapN(new Object[0]);
        public final int size;
        public final Object[] table;

        /* loaded from: classes2.dex */
        public class a extends AbstractSet<Map.Entry<K, V>> {
            public a() {
            }

            @Override // java.util.AbstractCollection, java.util.Collection, java.lang.Iterable, java.util.Set
            public Iterator<Map.Entry<K, V>> iterator() {
                return new b();
            }

            @Override // java.util.AbstractCollection, java.util.Collection, java.util.Set
            public int size() {
                return MapN.this.size;
            }
        }

        /* loaded from: classes2.dex */
        public class b extends h.a<Map.Entry<K, V>> {
            public int a;
            public int f0;

            public b() {
                int size = MapN.this.size();
                this.a = size;
                if (size > 0) {
                    this.f0 = ImmutableCollections.f(ImmutableCollections.a, MapN.this.table.length >> 1) << 1;
                }
            }

            @Override // java.util.Iterator
            /* renamed from: a */
            public Map.Entry<K, V> next() {
                if (hasNext()) {
                    do {
                    } while (MapN.this.table[nextIndex()] == null);
                    Object[] objArr = MapN.this.table;
                    int i = this.f0;
                    this.a--;
                    return new px1(objArr[i], objArr[i + 1]);
                }
                throw new NoSuchElementException();
            }

            @Override // java.util.Iterator
            public boolean hasNext() {
                return this.a > 0;
            }

            public final int nextIndex() {
                int i;
                int i2 = this.f0;
                if (ImmutableCollections.a >= 0) {
                    i = i2 + 2;
                    if (i >= MapN.this.table.length) {
                        i = 0;
                    }
                } else {
                    i = i2 - 2;
                    if (i < 0) {
                        i = MapN.this.table.length - 2;
                    }
                }
                this.f0 = i;
                return i;
            }
        }

        public MapN(Object... objArr) {
            if ((objArr.length & 1) == 0) {
                this.size = objArr.length >> 1;
                this.table = new Object[((objArr.length * 2) + 1) & (-2)];
                for (int i = 0; i < objArr.length; i += 2) {
                    Object f = rl2.f(objArr[i]);
                    Object f2 = rl2.f(objArr[i + 1]);
                    int a2 = a(f);
                    if (a2 < 0) {
                        int i2 = -(a2 + 1);
                        Object[] objArr2 = this.table;
                        objArr2[i2] = f;
                        objArr2[i2 + 1] = f2;
                    } else {
                        throw new IllegalArgumentException("duplicate key: " + f);
                    }
                }
                return;
            }
            throw new InternalError("length is odd");
        }

        private void readObject(ObjectInputStream objectInputStream) throws IOException, ClassNotFoundException {
            throw new InvalidObjectException("not serial proxy");
        }

        private Object writeReplace() {
            Object[] objArr = new Object[this.size * 2];
            int length = this.table.length;
            int i = 0;
            for (int i2 = 0; i2 < length; i2 += 2) {
                Object[] objArr2 = this.table;
                if (objArr2[i2] != null) {
                    int i3 = i + 1;
                    objArr[i] = objArr2[i2];
                    i = i3 + 1;
                    objArr[i3] = objArr2[i2 + 1];
                }
            }
            return new ColSer(3, objArr);
        }

        public final int a(Object obj) {
            int f = ImmutableCollections.f(obj.hashCode(), this.table.length >> 1) << 1;
            while (true) {
                Object obj2 = this.table[f];
                if (obj2 == null) {
                    return (-f) - 1;
                }
                if (obj.equals(obj2)) {
                    return f;
                }
                f += 2;
                if (f == this.table.length) {
                    f = 0;
                }
            }
        }

        @Override // java.util.AbstractMap, java.util.Map
        public boolean containsKey(Object obj) {
            rl2.f(obj);
            return this.size > 0 && a(obj) >= 0;
        }

        @Override // java.util.AbstractMap, java.util.Map
        public boolean containsValue(Object obj) {
            rl2.f(obj);
            int i = 1;
            while (true) {
                Object[] objArr = this.table;
                if (i >= objArr.length) {
                    return false;
                }
                Object obj2 = objArr[i];
                if (obj2 != null && obj.equals(obj2)) {
                    return true;
                }
                i += 2;
            }
        }

        @Override // java.util.AbstractMap, java.util.Map
        public Set<Map.Entry<K, V>> entrySet() {
            return new a();
        }

        @Override // java.util.AbstractMap, java.util.Map
        public V get(Object obj) {
            if (this.size == 0) {
                rl2.f(obj);
                return null;
            }
            int a2 = a(obj);
            if (a2 >= 0) {
                return (V) this.table[a2 + 1];
            }
            return null;
        }

        @Override // java.util.AbstractMap, java.util.Map
        public int hashCode() {
            int i = 0;
            int i2 = 0;
            while (true) {
                Object[] objArr = this.table;
                if (i >= objArr.length) {
                    return i2;
                }
                Object obj = objArr[i];
                if (obj != null) {
                    i2 += obj.hashCode() ^ this.table[i + 1].hashCode();
                }
                i += 2;
            }
        }

        @Override // java.util.AbstractMap, java.util.Map
        public int size() {
            return this.size;
        }
    }

    /* loaded from: classes2.dex */
    public static final class SetN<E> extends c<E> implements Serializable {
        public static final Set<?> EMPTY_SET = new SetN(new Object[0]);
        public final E[] elements;
        private final int size;

        /* loaded from: classes2.dex */
        public final class a extends h.a<E> {
            public int a;
            public int f0;

            public a() {
                int size = SetN.this.size();
                this.a = size;
                if (size > 0) {
                    this.f0 = ImmutableCollections.f(ImmutableCollections.a, SetN.this.elements.length);
                }
            }

            @Override // java.util.Iterator
            public boolean hasNext() {
                return this.a > 0;
            }

            @Override // java.util.Iterator
            public E next() {
                E e;
                if (hasNext()) {
                    do {
                        e = SetN.this.elements[nextIndex()];
                    } while (e == null);
                    this.a--;
                    return e;
                }
                throw new NoSuchElementException();
            }

            public final int nextIndex() {
                int i;
                int i2 = this.f0;
                if (ImmutableCollections.a >= 0) {
                    i = i2 + 1;
                    if (i >= SetN.this.elements.length) {
                        i = 0;
                    }
                } else {
                    i = i2 - 1;
                    if (i < 0) {
                        i = SetN.this.elements.length - 1;
                    }
                }
                this.f0 = i;
                return i;
            }
        }

        public SetN(E... eArr) {
            this.size = eArr.length;
            this.elements = (E[]) new Object[eArr.length * 2];
            for (E e : eArr) {
                int e2 = e(e);
                if (e2 < 0) {
                    this.elements[-(e2 + 1)] = e;
                } else {
                    throw new IllegalArgumentException("duplicate element: " + e);
                }
            }
        }

        private void readObject(ObjectInputStream objectInputStream) throws IOException, ClassNotFoundException {
            throw new InvalidObjectException("not serial proxy");
        }

        private Object writeReplace() {
            E[] eArr;
            Object[] objArr = new Object[this.size];
            int i = 0;
            for (E e : this.elements) {
                if (e != null) {
                    objArr[i] = e;
                    i++;
                }
            }
            return new ColSer(2, objArr);
        }

        @Override // java.util.AbstractCollection, java.util.Collection, java.util.Set
        public boolean contains(Object obj) {
            rl2.f(obj);
            return this.size > 0 && e(obj) >= 0;
        }

        public final int e(Object obj) {
            int f = ImmutableCollections.f(obj.hashCode(), this.elements.length);
            while (true) {
                E e = this.elements[f];
                if (e == null) {
                    return (-f) - 1;
                }
                if (obj.equals(e)) {
                    return f;
                }
                f++;
                if (f == this.elements.length) {
                    f = 0;
                }
            }
        }

        @Override // java8.util.ImmutableCollections.c, java.util.Collection, java.util.Set
        public int hashCode() {
            E[] eArr;
            int i = 0;
            for (E e : this.elements) {
                if (e != null) {
                    i += e.hashCode();
                }
            }
            return i;
        }

        @Override // java.util.AbstractCollection, java.util.Collection, java.lang.Iterable, java.util.Set
        public Iterator<E> iterator() {
            return new a();
        }

        @Override // java.util.AbstractCollection, java.util.Collection, java.util.Set
        public int size() {
            return this.size;
        }
    }

    /* loaded from: classes2.dex */
    public static abstract class a<E> extends AbstractCollection<E> {
        @Override // java.util.AbstractCollection, java.util.Collection
        public boolean add(E e) {
            throw ImmutableCollections.h();
        }

        @Override // java.util.AbstractCollection, java.util.Collection
        public boolean addAll(Collection<? extends E> collection) {
            throw ImmutableCollections.h();
        }

        @Override // java.util.AbstractCollection, java.util.Collection
        public void clear() {
            throw ImmutableCollections.h();
        }

        @Override // java.util.AbstractCollection, java.util.Collection
        public boolean remove(Object obj) {
            throw ImmutableCollections.h();
        }

        @Override // java.util.AbstractCollection, java.util.Collection
        public boolean removeAll(Collection<?> collection) {
            throw ImmutableCollections.h();
        }

        @Override // java.util.AbstractCollection, java.util.Collection
        public boolean retainAll(Collection<?> collection) {
            throw ImmutableCollections.h();
        }
    }

    /* loaded from: classes2.dex */
    public static abstract class b<E> extends a<E> implements List<E>, RandomAccess {
        public static void subListRangeCheck(int i, int i2, int i3) {
            if (i < 0) {
                throw new IndexOutOfBoundsException("fromIndex = " + i);
            } else if (i2 > i3) {
                throw new IndexOutOfBoundsException("toIndex = " + i2);
            } else if (i <= i2) {
            } else {
                throw new IllegalArgumentException("fromIndex(" + i + ") > toIndex(" + i2 + ")");
            }
        }

        @Override // java.util.List
        public void add(int i, E e) {
            throw ImmutableCollections.h();
        }

        @Override // java.util.List
        public boolean addAll(int i, Collection<? extends E> collection) {
            ImmutableCollections.g(i, size());
            throw ImmutableCollections.h();
        }

        @Override // java.util.AbstractCollection, java.util.Collection, java.util.List
        public boolean contains(Object obj) {
            return indexOf(obj) >= 0;
        }

        @Override // java.util.Collection, java.util.List
        public boolean equals(Object obj) {
            if (obj == this) {
                return true;
            }
            if (obj instanceof List) {
                Iterator<E> it = ((List) obj).iterator();
                int size = size();
                for (int i = 0; i < size; i++) {
                    if (!it.hasNext() || !get(i).equals(it.next())) {
                        return false;
                    }
                }
                return !it.hasNext();
            }
            return false;
        }

        @Override // java.util.Collection, java.util.List
        public int hashCode() {
            int size = size();
            int i = 1;
            for (int i2 = 0; i2 < size; i2++) {
                i = (i * 31) + get(i2).hashCode();
            }
            return i;
        }

        @Override // java.util.List
        public int indexOf(Object obj) {
            rl2.f(obj);
            int size = size();
            for (int i = 0; i < size; i++) {
                if (obj.equals(get(i))) {
                    return i;
                }
            }
            return -1;
        }

        @Override // java.util.AbstractCollection, java.util.Collection, java.lang.Iterable, java.util.List
        public Iterator<E> iterator() {
            return new d(this, size());
        }

        @Override // java.util.List
        public int lastIndexOf(Object obj) {
            rl2.f(obj);
            for (int size = size() - 1; size >= 0; size--) {
                if (obj.equals(get(size))) {
                    return size;
                }
            }
            return -1;
        }

        @Override // java.util.List
        public ListIterator<E> listIterator() {
            return listIterator(0);
        }

        public IndexOutOfBoundsException outOfBounds(int i) {
            return new IndexOutOfBoundsException("Index: " + i + " Size: " + size());
        }

        @Override // java.util.List
        public E remove(int i) {
            throw ImmutableCollections.h();
        }

        @Override // java.util.List
        public E set(int i, E e) {
            throw ImmutableCollections.h();
        }

        @Override // java.util.List
        public List<E> subList(int i, int i2) {
            subListRangeCheck(i, i2, size());
            return e.e(this, i, i2);
        }

        @Override // java.util.List
        public ListIterator<E> listIterator(int i) {
            int size = size();
            if (i >= 0 && i <= size) {
                return new d(this, size, i);
            }
            throw outOfBounds(i);
        }
    }

    /* loaded from: classes2.dex */
    public static abstract class c<E> extends a<E> implements Set<E> {
        /* JADX WARN: Removed duplicated region for block: B:14:0x0021  */
        @Override // java.util.Collection, java.util.Set
        /*
            Code decompiled incorrectly, please refer to instructions dump.
            To view partially-correct code enable 'Show inconsistent code' option in preferences
        */
        public boolean equals(java.lang.Object r5) {
            /*
                r4 = this;
                r0 = 1
                if (r5 != r4) goto L4
                return r0
            L4:
                boolean r1 = r5 instanceof java.util.Set
                r2 = 0
                if (r1 != 0) goto La
                return r2
            La:
                java.util.Collection r5 = (java.util.Collection) r5
                int r1 = r5.size()
                int r3 = r4.size()
                if (r1 == r3) goto L17
                return r2
            L17:
                java.util.Iterator r5 = r5.iterator()
            L1b:
                boolean r1 = r5.hasNext()
                if (r1 == 0) goto L2e
                java.lang.Object r1 = r5.next()
                if (r1 == 0) goto L2d
                boolean r1 = r4.contains(r1)
                if (r1 != 0) goto L1b
            L2d:
                return r2
            L2e:
                return r0
            */
            throw new UnsupportedOperationException("Method not decompiled: java8.util.ImmutableCollections.c.equals(java.lang.Object):boolean");
        }

        @Override // java.util.Collection, java.util.Set
        public abstract int hashCode();
    }

    /* loaded from: classes2.dex */
    public static final class e<E> extends b<E> {
        public final List<E> a;
        public final int f0;
        public final int g0;

        public e(List<E> list, int i, int i2) {
            this.a = list;
            this.f0 = i;
            this.g0 = i2;
        }

        public static <E> e<E> e(List<E> list, int i, int i2) {
            return new e<>(list, i, i2 - i);
        }

        public static <E> e<E> i(e<E> eVar, int i, int i2) {
            return new e<>(eVar.a, eVar.f0 + i, i2 - i);
        }

        @Override // java.util.List
        public E get(int i) {
            rl2.a(i, this.g0);
            return this.a.get(this.f0 + i);
        }

        @Override // java8.util.ImmutableCollections.b, java.util.AbstractCollection, java.util.Collection, java.lang.Iterable, java.util.List
        public Iterator<E> iterator() {
            return new d(this, size());
        }

        public final void k(int i) {
            if (i < 0 || i > this.g0) {
                throw outOfBounds(i);
            }
        }

        @Override // java8.util.ImmutableCollections.b, java.util.List
        public ListIterator<E> listIterator(int i) {
            k(i);
            return new d(this, size(), i);
        }

        @Override // java.util.AbstractCollection, java.util.Collection, java.util.List
        public int size() {
            return this.g0;
        }

        @Override // java8.util.ImmutableCollections.b, java.util.List
        public List<E> subList(int i, int i2) {
            b.subListRangeCheck(i, i2, this.g0);
            return i(this, i, i2);
        }
    }

    static {
        long nanoTime = System.nanoTime();
        a = (int) (nanoTime ^ (nanoTime >>> 32));
    }

    public static <E> List<E> b() {
        return (List<E>) ListN.EMPTY_LIST;
    }

    public static <K, V> Map<K, V> c() {
        return (Map<K, V>) MapN.EMPTY_MAP;
    }

    public static <E> Set<E> d() {
        return (Set<E>) SetN.EMPTY_SET;
    }

    public static int e(int i, int i2) {
        int i3 = i / i2;
        return ((i ^ i2) >= 0 || i2 * i3 == i) ? i3 : i3 - 1;
    }

    public static int f(int i, int i2) {
        return i - (e(i, i2) * i2);
    }

    public static void g(int i, int i2) {
        if (i < 0 || i > i2) {
            throw new IndexOutOfBoundsException("Index: " + i + ", Size: " + i2);
        }
    }

    public static UnsupportedOperationException h() {
        return new UnsupportedOperationException();
    }

    /* loaded from: classes2.dex */
    public static final class List12<E> extends b<E> implements Serializable {
        private final E e0;
        private final E e1;

        public List12(E e) {
            this.e0 = (E) rl2.f(e);
            this.e1 = null;
        }

        private void readObject(ObjectInputStream objectInputStream) throws IOException, ClassNotFoundException {
            throw new InvalidObjectException("not serial proxy");
        }

        private Object writeReplace() {
            E e = this.e1;
            if (e == null) {
                return new ColSer(1, this.e0);
            }
            return new ColSer(1, this.e0, e);
        }

        @Override // java.util.List
        public E get(int i) {
            E e;
            if (i == 0) {
                return this.e0;
            }
            if (i != 1 || (e = this.e1) == null) {
                throw outOfBounds(i);
            }
            return e;
        }

        @Override // java.util.AbstractCollection, java.util.Collection, java.util.List
        public int size() {
            return this.e1 != null ? 2 : 1;
        }

        public List12(E e, E e2) {
            this.e0 = (E) rl2.f(e);
            this.e1 = (E) rl2.f(e2);
        }
    }

    /* loaded from: classes2.dex */
    public static final class Set12<E> extends c<E> implements Serializable {
        public final E e0;
        public final E e1;

        /* loaded from: classes2.dex */
        public class a extends h.a<E> {
            public int a;

            public a() {
                this.a = Set12.this.size();
            }

            @Override // java.util.Iterator
            public boolean hasNext() {
                return this.a > 0;
            }

            @Override // java.util.Iterator
            public E next() {
                E e;
                int i = this.a;
                if (i == 1) {
                    this.a = 0;
                    return (ImmutableCollections.a >= 0 || (e = Set12.this.e1) == null) ? Set12.this.e0 : e;
                } else if (i == 2) {
                    this.a = 1;
                    return ImmutableCollections.a >= 0 ? Set12.this.e1 : Set12.this.e0;
                } else {
                    throw new NoSuchElementException();
                }
            }
        }

        public Set12(E e) {
            this.e0 = (E) rl2.f(e);
            this.e1 = null;
        }

        private void readObject(ObjectInputStream objectInputStream) throws IOException, ClassNotFoundException {
            throw new InvalidObjectException("not serial proxy");
        }

        private Object writeReplace() {
            E e = this.e1;
            if (e == null) {
                return new ColSer(2, this.e0);
            }
            return new ColSer(2, this.e0, e);
        }

        @Override // java.util.AbstractCollection, java.util.Collection, java.util.Set
        public boolean contains(Object obj) {
            return obj.equals(this.e0) || obj.equals(this.e1);
        }

        @Override // java8.util.ImmutableCollections.c, java.util.Collection, java.util.Set
        public int hashCode() {
            int hashCode = this.e0.hashCode();
            E e = this.e1;
            return hashCode + (e == null ? 0 : e.hashCode());
        }

        @Override // java.util.AbstractCollection, java.util.Collection, java.lang.Iterable, java.util.Set
        public Iterator<E> iterator() {
            return new a();
        }

        @Override // java.util.AbstractCollection, java.util.Collection, java.util.Set
        public int size() {
            return this.e1 == null ? 1 : 2;
        }

        public Set12(E e, E e2) {
            if (!e.equals(rl2.f(e2))) {
                this.e0 = e;
                this.e1 = e2;
                return;
            }
            throw new IllegalArgumentException("duplicate element: " + e);
        }
    }

    /* loaded from: classes2.dex */
    public static final class d<E> implements ListIterator<E> {
        public final List<E> a;
        public final int f0;
        public final boolean g0;
        public int h0;

        public d(List<E> list, int i) {
            this.a = list;
            this.f0 = i;
            this.h0 = 0;
            this.g0 = false;
        }

        @Override // java.util.ListIterator
        public void add(E e) {
            throw ImmutableCollections.h();
        }

        @Override // java.util.ListIterator, java.util.Iterator
        public boolean hasNext() {
            return this.h0 != this.f0;
        }

        @Override // java.util.ListIterator
        public boolean hasPrevious() {
            if (this.g0) {
                return this.h0 != 0;
            }
            throw ImmutableCollections.h();
        }

        @Override // java.util.ListIterator, java.util.Iterator
        public E next() {
            try {
                int i = this.h0;
                E e = this.a.get(i);
                this.h0 = i + 1;
                return e;
            } catch (IndexOutOfBoundsException unused) {
                throw new NoSuchElementException();
            }
        }

        @Override // java.util.ListIterator
        public int nextIndex() {
            if (this.g0) {
                return this.h0;
            }
            throw ImmutableCollections.h();
        }

        @Override // java.util.ListIterator
        public E previous() {
            if (this.g0) {
                try {
                    int i = this.h0 - 1;
                    E e = this.a.get(i);
                    this.h0 = i;
                    return e;
                } catch (IndexOutOfBoundsException unused) {
                    throw new NoSuchElementException();
                }
            }
            throw ImmutableCollections.h();
        }

        @Override // java.util.ListIterator
        public int previousIndex() {
            if (this.g0) {
                return this.h0 - 1;
            }
            throw ImmutableCollections.h();
        }

        @Override // java.util.ListIterator, java.util.Iterator
        public void remove() {
            throw ImmutableCollections.h();
        }

        @Override // java.util.ListIterator
        public void set(E e) {
            throw ImmutableCollections.h();
        }

        public d(List<E> list, int i, int i2) {
            this.a = list;
            this.f0 = i;
            this.h0 = i2;
            this.g0 = true;
        }
    }
}
