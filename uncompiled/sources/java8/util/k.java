package java8.util;

import java.util.Comparator;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.locks.ReentrantLock;
import sun.misc.Unsafe;

/* compiled from: LBQSpliterator.java */
/* loaded from: classes2.dex */
public final class k<E> implements s<E> {
    public static final Unsafe h;
    public static final long i;
    public static final long j;
    public static final long k;
    public static final long l;
    public static final long m;
    public final LinkedBlockingQueue<E> a;
    public final ReentrantLock b;
    public final ReentrantLock c;
    public Object d;
    public int e;
    public boolean f;
    public long g;

    static {
        Unsafe unsafe = u.a;
        h = unsafe;
        try {
            Class<?> cls = Class.forName("java.util.concurrent.LinkedBlockingQueue$Node");
            i = unsafe.objectFieldOffset(LinkedBlockingQueue.class.getDeclaredField("head"));
            j = unsafe.objectFieldOffset(cls.getDeclaredField("item"));
            k = unsafe.objectFieldOffset(cls.getDeclaredField("next"));
            l = unsafe.objectFieldOffset(LinkedBlockingQueue.class.getDeclaredField("putLock"));
            m = unsafe.objectFieldOffset(LinkedBlockingQueue.class.getDeclaredField("takeLock"));
        } catch (Exception e) {
            throw new Error(e);
        }
    }

    public k(LinkedBlockingQueue<E> linkedBlockingQueue) {
        this.a = linkedBlockingQueue;
        this.g = linkedBlockingQueue.size();
        this.b = u(linkedBlockingQueue);
        this.c = v(linkedBlockingQueue);
    }

    public static <T> Object r(LinkedBlockingQueue<T> linkedBlockingQueue) {
        return s(h.getObject(linkedBlockingQueue, i));
    }

    public static Object s(Object obj) {
        return h.getObject(obj, k);
    }

    public static <T> T t(Object obj) {
        return (T) h.getObject(obj, j);
    }

    public static ReentrantLock u(LinkedBlockingQueue<?> linkedBlockingQueue) {
        return (ReentrantLock) h.getObject(linkedBlockingQueue, l);
    }

    public static ReentrantLock v(LinkedBlockingQueue<?> linkedBlockingQueue) {
        return (ReentrantLock) h.getObject(linkedBlockingQueue, m);
    }

    public static <T> s<T> w(LinkedBlockingQueue<T> linkedBlockingQueue) {
        return new k(linkedBlockingQueue);
    }

    @Override // java8.util.s
    public void a(m60<? super E> m60Var) {
        rl2.f(m60Var);
        if (this.f) {
            return;
        }
        this.f = true;
        Object obj = this.d;
        this.d = null;
        o(m60Var, obj);
    }

    @Override // java8.util.s
    public int b() {
        return 4368;
    }

    /* JADX WARN: Finally extract failed */
    /* JADX WARN: Removed duplicated region for block: B:30:0x0058  */
    /* JADX WARN: Removed duplicated region for block: B:31:0x005d  */
    /* JADX WARN: Removed duplicated region for block: B:35:0x006b  */
    /* JADX WARN: Removed duplicated region for block: B:47:? A[RETURN, SYNTHETIC] */
    @Override // java8.util.s
    /*
        Code decompiled incorrectly, please refer to instructions dump.
        To view partially-correct code enable 'Show inconsistent code' option in preferences
    */
    public java8.util.s<E> c() {
        /*
            r10 = this;
            java.util.concurrent.LinkedBlockingQueue<E> r0 = r10.a
            boolean r1 = r10.f
            if (r1 != 0) goto L72
            java.lang.Object r1 = r10.d
            if (r1 != 0) goto L10
            java.lang.Object r1 = r(r0)
            if (r1 == 0) goto L72
        L10:
            java.lang.Object r1 = s(r1)
            if (r1 == 0) goto L72
            int r1 = r10.e
            r2 = 1
            int r1 = r1 + r2
            r3 = 33554432(0x2000000, float:9.403955E-38)
            int r1 = java.lang.Math.min(r1, r3)
            r10.e = r1
            java.lang.Object[] r3 = new java.lang.Object[r1]
            java.lang.Object r4 = r10.d
            r10.p()
            r5 = 0
            if (r4 != 0) goto L37
            java.lang.Object r4 = r(r0)     // Catch: java.lang.Throwable -> L35
            if (r4 == 0) goto L33
            goto L37
        L33:
            r0 = r5
            goto L4f
        L35:
            r0 = move-exception
            goto L4b
        L37:
            r0 = r5
        L38:
            if (r4 == 0) goto L4f
            if (r0 >= r1) goto L4f
            java.lang.Object r6 = t(r4)     // Catch: java.lang.Throwable -> L35
            r3[r0] = r6     // Catch: java.lang.Throwable -> L35
            if (r6 == 0) goto L46
            int r0 = r0 + 1
        L46:
            java.lang.Object r4 = r10.x(r4)     // Catch: java.lang.Throwable -> L35
            goto L38
        L4b:
            r10.q()
            throw r0
        L4f:
            r10.q()
            r10.d = r4
            r6 = 0
            if (r4 != 0) goto L5d
            r10.g = r6
            r10.f = r2
            goto L69
        L5d:
            long r1 = r10.g
            long r8 = (long) r0
            long r1 = r1 - r8
            r10.g = r1
            int r1 = (r1 > r6 ? 1 : (r1 == r6 ? 0 : -1))
            if (r1 >= 0) goto L69
            r10.g = r6
        L69:
            if (r0 <= 0) goto L72
            r1 = 4368(0x1110, float:6.121E-42)
            java8.util.s r0 = java8.util.t.y(r3, r5, r0, r1)
            return r0
        L72:
            r0 = 0
            return r0
        */
        throw new UnsupportedOperationException("Method not decompiled: java8.util.k.c():java8.util.s");
    }

    @Override // java8.util.s
    public boolean d(m60<? super E> m60Var) {
        rl2.f(m60Var);
        if (this.f) {
            return false;
        }
        Object obj = (Object) null;
        p();
        try {
            Object obj2 = this.d;
            if (obj2 != null || (obj2 = r(this.a)) != null) {
                do {
                    obj = (Object) t(obj2);
                    obj2 = x(obj2);
                    if (obj != null) {
                        break;
                    }
                } while (obj2 != null);
            }
            this.d = obj2;
            if (obj2 == null) {
                this.f = true;
            }
            if (obj != null) {
                m60Var.accept(obj);
                return true;
            }
            return false;
        } finally {
            q();
        }
    }

    @Override // java8.util.s
    public Comparator<? super E> f() {
        return t.h(this);
    }

    @Override // java8.util.s
    public boolean h(int i2) {
        return t.k(this, i2);
    }

    @Override // java8.util.s
    public long i() {
        return t.i(this);
    }

    @Override // java8.util.s
    public long l() {
        return this.g;
    }

    public void o(m60<? super E> m60Var, Object obj) {
        Object[] objArr = null;
        int i2 = 0;
        do {
            p();
            if (objArr == null) {
                if (obj == null) {
                    try {
                        obj = r(this.a);
                    } catch (Throwable th) {
                        q();
                        throw th;
                    }
                }
                Object obj2 = obj;
                while (obj2 != null && (t(obj2) == null || (i2 = i2 + 1) != 64)) {
                    obj2 = x(obj2);
                }
                objArr = new Object[i2];
            }
            int i3 = 0;
            while (obj != null && i3 < i2) {
                Object t = t(obj);
                objArr[i3] = t;
                if (t != null) {
                    i3++;
                }
                obj = x(obj);
            }
            q();
            for (int i4 = 0; i4 < i3; i4++) {
                m60Var.accept(objArr[i4]);
            }
            if (i3 <= 0) {
                return;
            }
        } while (obj != null);
    }

    public final void p() {
        this.b.lock();
        this.c.lock();
    }

    public final void q() {
        this.c.unlock();
        this.b.unlock();
    }

    public Object x(Object obj) {
        Object s = s(obj);
        return obj == s ? r(this.a) : s;
    }
}
