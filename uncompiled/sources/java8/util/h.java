package java8.util;

import java.util.Iterator;

/* compiled from: Iterators.java */
/* loaded from: classes2.dex */
public final class h {

    /* compiled from: Iterators.java */
    /* loaded from: classes2.dex */
    public static abstract class a<T> implements Iterator<T> {
        @Override // java.util.Iterator
        public void remove() {
            throw new UnsupportedOperationException("remove");
        }
    }

    public static <E> void a(Iterator<E> it, m60<? super E> m60Var) {
        rl2.f(it);
        rl2.f(m60Var);
        while (it.hasNext()) {
            m60Var.accept(it.next());
        }
    }
}
