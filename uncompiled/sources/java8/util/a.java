package java8.util;

import java.util.ArrayDeque;
import java.util.Comparator;
import java.util.ConcurrentModificationException;
import sun.misc.Unsafe;

/* compiled from: ArrayDequeSpliterator.java */
/* loaded from: classes2.dex */
public final class a<E> implements s<E> {
    public static final Unsafe d;
    public static final long e;
    public static final long f;
    public static final long g;
    public final ArrayDeque<E> a;
    public int b;
    public int c;

    static {
        Unsafe unsafe = u.a;
        d = unsafe;
        try {
            e = unsafe.objectFieldOffset(ArrayDeque.class.getDeclaredField("tail"));
            f = unsafe.objectFieldOffset(ArrayDeque.class.getDeclaredField("head"));
            g = unsafe.objectFieldOffset(ArrayDeque.class.getDeclaredField("elements"));
        } catch (Exception e2) {
            throw new Error(e2);
        }
    }

    public a(ArrayDeque<E> arrayDeque, int i, int i2) {
        this.a = arrayDeque;
        this.c = i;
        this.b = i2;
    }

    public static <T> Object[] o(ArrayDeque<T> arrayDeque) {
        return (Object[]) d.getObject(arrayDeque, g);
    }

    public static <T> int q(ArrayDeque<T> arrayDeque) {
        return d.getInt(arrayDeque, f);
    }

    public static <T> int r(ArrayDeque<T> arrayDeque) {
        return d.getInt(arrayDeque, e);
    }

    public static <T> s<T> s(ArrayDeque<T> arrayDeque) {
        return new a(arrayDeque, -1, -1);
    }

    @Override // java8.util.s
    public void a(m60<? super E> m60Var) {
        rl2.f(m60Var);
        Object[] o = o(this.a);
        int length = o.length - 1;
        int p = p();
        int i = this.c;
        this.c = p;
        while (i != p) {
            Object obj = o[i];
            i = (i + 1) & length;
            if (obj != null) {
                m60Var.accept(obj);
            } else {
                throw new ConcurrentModificationException();
            }
        }
    }

    @Override // java8.util.s
    public int b() {
        return 16720;
    }

    @Override // java8.util.s
    public boolean d(m60<? super E> m60Var) {
        rl2.f(m60Var);
        Object[] o = o(this.a);
        int length = o.length - 1;
        p();
        int i = this.c;
        if (i != this.b) {
            Object obj = o[i];
            this.c = length & (i + 1);
            if (obj != null) {
                m60Var.accept(obj);
                return true;
            }
            throw new ConcurrentModificationException();
        }
        return false;
    }

    @Override // java8.util.s
    public Comparator<? super E> f() {
        return t.h(this);
    }

    @Override // java8.util.s
    public boolean h(int i) {
        return t.k(this, i);
    }

    @Override // java8.util.s
    public long i() {
        return t.i(this);
    }

    @Override // java8.util.s
    public long l() {
        int p = p() - this.c;
        if (p < 0) {
            p += o(this.a).length;
        }
        return p;
    }

    public final int p() {
        int i = this.b;
        if (i < 0) {
            int r = r(this.a);
            this.b = r;
            this.c = q(this.a);
            return r;
        }
        return i;
    }

    @Override // java8.util.s
    /* renamed from: t */
    public a<E> c() {
        int p = p();
        int i = this.c;
        int length = o(this.a).length;
        if (i != p) {
            int i2 = length - 1;
            if (((i + 1) & i2) != p) {
                if (i > p) {
                    p += length;
                }
                int i3 = ((p + i) >>> 1) & i2;
                ArrayDeque<E> arrayDeque = this.a;
                this.c = i3;
                return new a<>(arrayDeque, i, i3);
            }
            return null;
        }
        return null;
    }
}
