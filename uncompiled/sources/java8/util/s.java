package java8.util;

import java.util.Comparator;

/* compiled from: Spliterator.java */
/* loaded from: classes2.dex */
public interface s<T> {

    /* compiled from: Spliterator.java */
    /* loaded from: classes2.dex */
    public interface a extends d<Double, hq0, a> {
        @Override // java8.util.s
        void a(m60<? super Double> m60Var);

        void m(hq0 hq0Var);

        boolean n(hq0 hq0Var);
    }

    /* compiled from: Spliterator.java */
    /* loaded from: classes2.dex */
    public interface b extends d<Integer, mr1, b> {
        @Override // java8.util.s
        void a(m60<? super Integer> m60Var);

        boolean e(mr1 mr1Var);

        void g(mr1 mr1Var);
    }

    /* compiled from: Spliterator.java */
    /* loaded from: classes2.dex */
    public interface c extends d<Long, f22, c> {
        @Override // java8.util.s
        void a(m60<? super Long> m60Var);

        void j(f22 f22Var);

        boolean k(f22 f22Var);
    }

    /* compiled from: Spliterator.java */
    /* loaded from: classes2.dex */
    public interface d<T, T_CONS, T_SPLITR extends d<T, T_CONS, T_SPLITR>> extends s<T> {
    }

    void a(m60<? super T> m60Var);

    int b();

    s<T> c();

    boolean d(m60<? super T> m60Var);

    Comparator<? super T> f();

    boolean h(int i);

    long i();

    long l();
}
