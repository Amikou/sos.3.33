package java8.util;

import java.util.Comparator;
import java.util.concurrent.LinkedBlockingDeque;
import java.util.concurrent.locks.ReentrantLock;
import sun.misc.Unsafe;

/* compiled from: LBDSpliterator.java */
/* loaded from: classes2.dex */
public final class j<E> implements s<E> {
    public static final Unsafe g;
    public static final long h;
    public static final long i;
    public static final long j;
    public static final long k;
    public final LinkedBlockingDeque<E> a;
    public final ReentrantLock b;
    public Object c;
    public int d;
    public boolean e;
    public long f;

    static {
        Unsafe unsafe = u.a;
        g = unsafe;
        try {
            Class<?> cls = Class.forName("java.util.concurrent.LinkedBlockingDeque$Node");
            h = unsafe.objectFieldOffset(LinkedBlockingDeque.class.getDeclaredField("first"));
            i = unsafe.objectFieldOffset(LinkedBlockingDeque.class.getDeclaredField("lock"));
            j = unsafe.objectFieldOffset(cls.getDeclaredField("item"));
            k = unsafe.objectFieldOffset(cls.getDeclaredField("next"));
        } catch (Exception e) {
            throw new Error(e);
        }
    }

    public j(LinkedBlockingDeque<E> linkedBlockingDeque) {
        this.a = linkedBlockingDeque;
        this.f = linkedBlockingDeque.size();
        this.b = s(linkedBlockingDeque);
    }

    public static Object p(Object obj) {
        return g.getObject(obj, k);
    }

    public static <T> T q(Object obj) {
        return (T) g.getObject(obj, j);
    }

    public static Object r(LinkedBlockingDeque<?> linkedBlockingDeque) {
        return g.getObject(linkedBlockingDeque, h);
    }

    public static ReentrantLock s(LinkedBlockingDeque<?> linkedBlockingDeque) {
        return (ReentrantLock) g.getObject(linkedBlockingDeque, i);
    }

    public static <T> s<T> t(LinkedBlockingDeque<T> linkedBlockingDeque) {
        return new j(linkedBlockingDeque);
    }

    @Override // java8.util.s
    public void a(m60<? super E> m60Var) {
        rl2.f(m60Var);
        if (this.e) {
            return;
        }
        this.e = true;
        Object obj = this.c;
        this.c = null;
        o(m60Var, obj);
    }

    @Override // java8.util.s
    public int b() {
        return 4368;
    }

    /* JADX WARN: Finally extract failed */
    /* JADX WARN: Removed duplicated region for block: B:30:0x005a  */
    /* JADX WARN: Removed duplicated region for block: B:31:0x005f  */
    /* JADX WARN: Removed duplicated region for block: B:35:0x006d  */
    /* JADX WARN: Removed duplicated region for block: B:47:? A[RETURN, SYNTHETIC] */
    @Override // java8.util.s
    /*
        Code decompiled incorrectly, please refer to instructions dump.
        To view partially-correct code enable 'Show inconsistent code' option in preferences
    */
    public java8.util.s<E> c() {
        /*
            r9 = this;
            java.util.concurrent.LinkedBlockingDeque<E> r0 = r9.a
            boolean r1 = r9.e
            if (r1 != 0) goto L74
            java.lang.Object r1 = r9.c
            if (r1 != 0) goto L10
            java.lang.Object r1 = r(r0)
            if (r1 == 0) goto L74
        L10:
            java.lang.Object r1 = p(r1)
            if (r1 == 0) goto L74
            int r1 = r9.d
            r2 = 1
            int r1 = r1 + r2
            r3 = 33554432(0x2000000, float:9.403955E-38)
            int r1 = java.lang.Math.min(r1, r3)
            r9.d = r1
            java.lang.Object[] r3 = new java.lang.Object[r1]
            java.util.concurrent.locks.ReentrantLock r4 = r9.b
            java.lang.Object r5 = r9.c
            r4.lock()
            r6 = 0
            if (r5 != 0) goto L39
            java.lang.Object r5 = r(r0)     // Catch: java.lang.Throwable -> L37
            if (r5 == 0) goto L35
            goto L39
        L35:
            r0 = r6
            goto L51
        L37:
            r0 = move-exception
            goto L4d
        L39:
            r0 = r6
        L3a:
            if (r5 == 0) goto L51
            if (r0 >= r1) goto L51
            java.lang.Object r7 = q(r5)     // Catch: java.lang.Throwable -> L37
            r3[r0] = r7     // Catch: java.lang.Throwable -> L37
            if (r7 == 0) goto L48
            int r0 = r0 + 1
        L48:
            java.lang.Object r5 = r9.u(r5)     // Catch: java.lang.Throwable -> L37
            goto L3a
        L4d:
            r4.unlock()
            throw r0
        L51:
            r4.unlock()
            r9.c = r5
            r7 = 0
            if (r5 != 0) goto L5f
            r9.f = r7
            r9.e = r2
            goto L6b
        L5f:
            long r1 = r9.f
            long r4 = (long) r0
            long r1 = r1 - r4
            r9.f = r1
            int r1 = (r1 > r7 ? 1 : (r1 == r7 ? 0 : -1))
            if (r1 >= 0) goto L6b
            r9.f = r7
        L6b:
            if (r0 <= 0) goto L74
            r1 = 4368(0x1110, float:6.121E-42)
            java8.util.s r0 = java8.util.t.y(r3, r6, r0, r1)
            return r0
        L74:
            r0 = 0
            return r0
        */
        throw new UnsupportedOperationException("Method not decompiled: java8.util.j.c():java8.util.s");
    }

    @Override // java8.util.s
    public boolean d(m60<? super E> m60Var) {
        rl2.f(m60Var);
        if (this.e) {
            return false;
        }
        Object obj = (Object) null;
        ReentrantLock reentrantLock = this.b;
        reentrantLock.lock();
        try {
            Object obj2 = this.c;
            if (obj2 != null || (obj2 = r(this.a)) != null) {
                do {
                    obj = (Object) q(obj2);
                    obj2 = u(obj2);
                    if (obj != null) {
                        break;
                    }
                } while (obj2 != null);
            }
            this.c = obj2;
            if (obj2 == null) {
                this.e = true;
            }
            if (obj != null) {
                m60Var.accept(obj);
                return true;
            }
            return false;
        } finally {
            reentrantLock.unlock();
        }
    }

    @Override // java8.util.s
    public Comparator<? super E> f() {
        return t.h(this);
    }

    @Override // java8.util.s
    public boolean h(int i2) {
        return t.k(this, i2);
    }

    @Override // java8.util.s
    public long i() {
        return t.i(this);
    }

    @Override // java8.util.s
    public long l() {
        return this.f;
    }

    public void o(m60<? super E> m60Var, Object obj) {
        ReentrantLock reentrantLock = this.b;
        Object[] objArr = null;
        int i2 = 0;
        do {
            reentrantLock.lock();
            if (objArr == null) {
                if (obj == null) {
                    try {
                        obj = r(this.a);
                    } catch (Throwable th) {
                        reentrantLock.unlock();
                        throw th;
                    }
                }
                Object obj2 = obj;
                while (obj2 != null && (q(obj2) == null || (i2 = i2 + 1) != 64)) {
                    obj2 = u(obj2);
                }
                objArr = new Object[i2];
            }
            int i3 = 0;
            while (obj != null && i3 < i2) {
                Object q = q(obj);
                objArr[i3] = q;
                if (q != null) {
                    i3++;
                }
                obj = u(obj);
            }
            reentrantLock.unlock();
            for (int i4 = 0; i4 < i3; i4++) {
                m60Var.accept(objArr[i4]);
            }
            if (i3 <= 0) {
                return;
            }
        } while (obj != null);
    }

    public Object u(Object obj) {
        Object p = p(obj);
        return obj == p ? r(this.a) : p;
    }
}
