package java8.util;

import java.util.AbstractList;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.ConcurrentModificationException;
import sun.misc.Unsafe;

/* compiled from: ArrayListSpliterator.java */
/* loaded from: classes2.dex */
public final class b<E> implements s<E> {
    public static final Unsafe e;
    public static final long f;
    public static final long g;
    public static final long h;
    public final ArrayList<E> a;
    public int b;
    public int c;
    public int d;

    static {
        Unsafe unsafe = u.a;
        e = unsafe;
        try {
            g = unsafe.objectFieldOffset(AbstractList.class.getDeclaredField("modCount"));
            f = unsafe.objectFieldOffset(ArrayList.class.getDeclaredField("size"));
            h = unsafe.objectFieldOffset(ArrayList.class.getDeclaredField(t.i ? "array" : "elementData"));
        } catch (Exception e2) {
            throw new Error(e2);
        }
    }

    public b(ArrayList<E> arrayList, int i, int i2, int i3) {
        this.a = arrayList;
        this.b = i;
        this.c = i2;
        this.d = i3;
    }

    public static <T> Object[] o(ArrayList<T> arrayList) {
        return (Object[]) e.getObject(arrayList, h);
    }

    public static <T> int q(ArrayList<T> arrayList) {
        return e.getInt(arrayList, g);
    }

    public static <T> int r(ArrayList<T> arrayList) {
        return e.getInt(arrayList, f);
    }

    public static <T> s<T> s(ArrayList<T> arrayList) {
        return new b(arrayList, 0, -1, 0);
    }

    @Override // java8.util.s
    public void a(m60<? super E> m60Var) {
        int i;
        rl2.f(m60Var);
        ArrayList<E> arrayList = this.a;
        Object[] o = o(arrayList);
        if (o != null) {
            int i2 = this.c;
            if (i2 < 0) {
                i = q(arrayList);
                i2 = r(arrayList);
            } else {
                i = this.d;
            }
            int i3 = this.b;
            if (i3 >= 0) {
                this.b = i2;
                if (i2 <= o.length) {
                    while (i3 < i2) {
                        m60Var.accept(o[i3]);
                        i3++;
                    }
                    if (i == q(arrayList)) {
                        return;
                    }
                }
            }
        }
        throw new ConcurrentModificationException();
    }

    @Override // java8.util.s
    public int b() {
        return 16464;
    }

    @Override // java8.util.s
    public boolean d(m60<? super E> m60Var) {
        rl2.f(m60Var);
        int p = p();
        int i = this.b;
        if (i < p) {
            this.b = i + 1;
            m60Var.accept(o(this.a)[i]);
            if (this.d == q(this.a)) {
                return true;
            }
            throw new ConcurrentModificationException();
        }
        return false;
    }

    @Override // java8.util.s
    public Comparator<? super E> f() {
        return t.h(this);
    }

    @Override // java8.util.s
    public boolean h(int i) {
        return t.k(this, i);
    }

    @Override // java8.util.s
    public long i() {
        return t.i(this);
    }

    @Override // java8.util.s
    public long l() {
        return p() - this.b;
    }

    public final int p() {
        int i = this.c;
        if (i < 0) {
            ArrayList<E> arrayList = this.a;
            this.d = q(arrayList);
            int r = r(arrayList);
            this.c = r;
            return r;
        }
        return i;
    }

    @Override // java8.util.s
    /* renamed from: t */
    public b<E> c() {
        int p = p();
        int i = this.b;
        int i2 = (p + i) >>> 1;
        if (i >= i2) {
            return null;
        }
        ArrayList<E> arrayList = this.a;
        this.b = i2;
        return new b<>(arrayList, i, i2, this.d);
    }
}
