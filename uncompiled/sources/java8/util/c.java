package java8.util;

import java.util.List;
import sun.misc.Unsafe;

/* compiled from: ArraysArrayListSpliterator.java */
/* loaded from: classes2.dex */
public final class c {
    public static final Unsafe a;
    public static final long b;

    static {
        Unsafe unsafe = u.a;
        a = unsafe;
        try {
            b = unsafe.objectFieldOffset(Class.forName("java.util.Arrays$ArrayList").getDeclaredField("a"));
        } catch (Exception e) {
            throw new Error(e);
        }
    }

    public static <T> Object[] a(List<T> list) {
        return (Object[]) a.getObject(list, b);
    }

    public static <T> s<T> b(List<T> list) {
        return t.x(a(list), 16);
    }
}
