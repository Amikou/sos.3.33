package java8.util;

import java.io.IOException;
import java.io.InvalidObjectException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.ObjectStreamException;
import java.io.Serializable;
import java8.util.ImmutableCollections;

/* compiled from: ImmutableCollections.java */
/* loaded from: classes2.dex */
public final class ColSer implements Serializable {
    public static final int IMM_LIST = 1;
    public static final int IMM_MAP = 3;
    public static final int IMM_SET = 2;
    private static final long serialVersionUID = 6309168927139932177L;
    public transient Object[] a;
    private final int tag;

    public ColSer(int i, Object... objArr) {
        this.tag = i;
        this.a = objArr;
    }

    public static InvalidObjectException a(RuntimeException runtimeException) {
        InvalidObjectException invalidObjectException = new InvalidObjectException("invalid object");
        invalidObjectException.initCause(runtimeException);
        return invalidObjectException;
    }

    private void readObject(ObjectInputStream objectInputStream) throws IOException, ClassNotFoundException {
        objectInputStream.defaultReadObject();
        int readInt = objectInputStream.readInt();
        if (readInt >= 0) {
            Object[] objArr = new Object[readInt];
            for (int i = 0; i < readInt; i++) {
                objArr[i] = objectInputStream.readObject();
            }
            this.a = objArr;
            return;
        }
        throw new InvalidObjectException("negative length " + readInt);
    }

    private Object readResolve() throws ObjectStreamException {
        try {
            Object[] objArr = this.a;
            if (objArr != null) {
                int i = this.tag & 255;
                if (i != 1) {
                    if (i != 2) {
                        if (i == 3) {
                            if (objArr.length == 0) {
                                return ImmutableCollections.c();
                            }
                            if (objArr.length == 2) {
                                Object[] objArr2 = this.a;
                                return new ImmutableCollections.Map1(objArr2[0], objArr2[1]);
                            }
                            return new ImmutableCollections.MapN(this.a);
                        }
                        throw new InvalidObjectException(String.format("invalid flags 0x%x", Integer.valueOf(this.tag)));
                    }
                    return r.b(objArr);
                }
                return m.a(objArr);
            }
            throw new InvalidObjectException("null array");
        } catch (IllegalArgumentException e) {
            throw a(e);
        } catch (NullPointerException e2) {
            throw a(e2);
        }
    }

    private void writeObject(ObjectOutputStream objectOutputStream) throws IOException {
        objectOutputStream.defaultWriteObject();
        objectOutputStream.writeInt(this.a.length);
        int i = 0;
        while (true) {
            Object[] objArr = this.a;
            if (i >= objArr.length) {
                return;
            }
            objectOutputStream.writeObject(objArr[i]);
            i++;
        }
    }
}
