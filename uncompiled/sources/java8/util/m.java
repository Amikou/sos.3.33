package java8.util;

import java.util.List;
import java8.util.ImmutableCollections;

/* compiled from: Lists.java */
/* loaded from: classes2.dex */
public final class m {
    public static <E> List<E> a(E... eArr) {
        int length = eArr.length;
        if (length != 0) {
            if (length != 1) {
                if (length != 2) {
                    return new ImmutableCollections.ListN(eArr);
                }
                return new ImmutableCollections.List12(eArr[0], eArr[1]);
            }
            return new ImmutableCollections.List12(eArr[0]);
        }
        return ImmutableCollections.b();
    }
}
