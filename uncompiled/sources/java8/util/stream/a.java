package java8.util.stream;

import java8.util.s;

/* compiled from: AbstractPipeline.java */
/* loaded from: classes2.dex */
public abstract class a<E_IN, E_OUT, S> extends e<E_OUT> {
    public final a a;
    public final a b;
    public final int c;
    public a d;
    public int e;
    public int f;
    public s<?> g;
    public ew3<? extends s<?>> h;
    public boolean i;
    public boolean j;
    public boolean k;

    public a(s<?> sVar, int i, boolean z) {
        this.b = null;
        this.g = sVar;
        this.a = this;
        int i2 = StreamOpFlag.STREAM_MASK & i;
        this.c = i2;
        this.f = (~(i2 << 1)) & StreamOpFlag.INITIAL_OPS_VALUE;
        this.e = 0;
        this.k = z;
    }

    public static /* synthetic */ Object[] r(int i) {
        return new Object[i];
    }

    @Override // java8.util.stream.e
    public final <P_IN> void g(g<P_IN> gVar, s<P_IN> sVar) {
        rl2.f(gVar);
        if (!StreamOpFlag.SHORT_CIRCUIT.isKnown(i())) {
            gVar.b(sVar.i());
            sVar.a(gVar);
            gVar.g();
            return;
        }
        m(gVar, sVar);
    }

    @Override // java8.util.stream.e
    public final <P_IN> long h(s<P_IN> sVar) {
        if (StreamOpFlag.SIZED.isKnown(i())) {
            return sVar.i();
        }
        return -1L;
    }

    @Override // java8.util.stream.e
    public final int i() {
        return this.f;
    }

    @Override // java8.util.stream.e
    public final <P_IN, S_ extends g<E_OUT>> S_ k(S_ s_, s<P_IN> sVar) {
        g(l((g) rl2.f(s_)), sVar);
        return s_;
    }

    @Override // java8.util.stream.e
    public final <P_IN> g<P_IN> l(g<E_OUT> gVar) {
        rl2.f(gVar);
        a<E_IN, E_OUT, S> aVar = this;
        m60 m60Var = gVar;
        while (aVar.e > 0) {
            aVar = aVar.b;
            m60Var = (g<E_IN>) aVar.v(aVar.b.f, (g) m60Var);
        }
        return (g<P_IN>) m60Var;
    }

    /* JADX WARN: Multi-variable type inference failed */
    public final <P_IN> boolean m(g<P_IN> gVar, s<P_IN> sVar) {
        a<E_IN, E_OUT, S> aVar = this;
        while (aVar.e > 0) {
            aVar = aVar.b;
        }
        gVar.b(sVar.i());
        boolean o = aVar.o(sVar, gVar);
        gVar.g();
        return o;
    }

    public final <R> R n(b44<E_OUT, R> b44Var) {
        if (!this.i) {
            this.i = true;
            if (q()) {
                return b44Var.e(this, w(b44Var.f()));
            }
            return b44Var.a(this, w(b44Var.f()));
        }
        throw new IllegalStateException("stream has already been operated upon or closed");
    }

    public abstract boolean o(s<E_OUT> sVar, g<E_OUT> gVar);

    public final boolean p() {
        return StreamOpFlag.ORDERED.isKnown(this.f);
    }

    public final boolean q() {
        return this.a.k;
    }

    public <P_IN> c<E_OUT> s(e<E_OUT> eVar, s<P_IN> sVar, nr1<E_OUT[]> nr1Var) {
        throw new UnsupportedOperationException("Parallel evaluation is not supported");
    }

    public <P_IN> s<E_OUT> t(e<E_OUT> eVar, s<P_IN> sVar) {
        return s(eVar, sVar, e5.b()).spliterator();
    }

    public abstract boolean u();

    public abstract g<E_IN> v(int i, g<E_OUT> gVar);

    public final s<?> w(int i) {
        s sVar;
        int i2;
        int i3;
        a aVar = this.a;
        s sVar2 = aVar.g;
        if (sVar2 != null) {
            aVar.g = null;
            sVar = sVar2;
        } else {
            ew3<? extends s<?>> ew3Var = aVar.h;
            if (ew3Var != null) {
                s sVar3 = ew3Var.get();
                this.a.h = null;
                sVar = sVar3;
            } else {
                throw new IllegalStateException("source already consumed or closed");
            }
        }
        if (q()) {
            a<E_IN, E_OUT, S> aVar2 = this.a;
            if (aVar2.j) {
                int i4 = 1;
                sVar = sVar;
                a aVar3 = aVar2.d;
                while (aVar2 != this) {
                    int i5 = aVar3.c;
                    if (aVar3.u()) {
                        i4 = 0;
                        if (StreamOpFlag.SHORT_CIRCUIT.isKnown(i5)) {
                            i5 &= ~StreamOpFlag.IS_SHORT_CIRCUIT;
                        }
                        sVar = (s<E_OUT>) aVar3.t(aVar2, sVar);
                        if (sVar.h(64)) {
                            i2 = i5 & (~StreamOpFlag.NOT_SIZED);
                            i3 = StreamOpFlag.IS_SIZED;
                        } else {
                            i2 = i5 & (~StreamOpFlag.IS_SIZED);
                            i3 = StreamOpFlag.NOT_SIZED;
                        }
                        i5 = i2 | i3;
                    }
                    aVar3.e = i4;
                    aVar3.f = StreamOpFlag.combineOpFlags(i5, aVar2.f);
                    i4++;
                    a<E_IN, E_OUT, S> aVar4 = aVar3;
                    aVar3 = aVar3.d;
                    aVar2 = aVar4;
                    sVar = sVar;
                }
            }
        }
        if (i != 0) {
            this.f = StreamOpFlag.combineOpFlags(i, this.f);
        }
        return sVar;
    }

    public final s<E_OUT> x() {
        a<E_IN, E_OUT, S> aVar = this.a;
        if (this == aVar) {
            if (!this.i) {
                this.i = true;
                s<E_OUT> sVar = (s<E_OUT>) aVar.g;
                if (sVar != null) {
                    aVar.g = null;
                    return sVar;
                }
                ew3<? extends s<?>> ew3Var = aVar.h;
                if (ew3Var != null) {
                    s<E_OUT> sVar2 = (s) ew3Var.get();
                    this.a.h = null;
                    return sVar2;
                }
                throw new IllegalStateException("source already consumed or closed");
            }
            throw new IllegalStateException("stream has already been operated upon or closed");
        }
        throw new IllegalStateException();
    }

    public a(a<?, E_IN, ?> aVar, int i) {
        if (!aVar.i) {
            aVar.i = true;
            aVar.d = this;
            this.b = aVar;
            this.c = StreamOpFlag.OP_MASK & i;
            this.f = StreamOpFlag.combineOpFlags(i, aVar.f);
            a aVar2 = aVar.a;
            this.a = aVar2;
            if (u()) {
                aVar2.j = true;
            }
            this.e = aVar.e + 1;
            return;
        }
        throw new IllegalStateException("stream has already been operated upon or closed");
    }
}
