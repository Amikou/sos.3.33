package java8.util.stream;

import java.util.Collections;
import java.util.EnumSet;
import java.util.List;
import java.util.Set;
import java8.util.stream.Collector;

/* compiled from: Collectors.java */
/* loaded from: classes2.dex */
public final class b {
    public static final Set<Collector.Characteristics> a;
    public static final ew3<iq0> b;
    public static final ew3<tr1> c;
    public static final ew3<j22> d;
    public static final ep<List<Object>, ?> e;

    static {
        Collector.Characteristics characteristics = Collector.Characteristics.CONCURRENT;
        Collector.Characteristics characteristics2 = Collector.Characteristics.UNORDERED;
        Collector.Characteristics characteristics3 = Collector.Characteristics.IDENTITY_FINISH;
        Collections.unmodifiableSet(EnumSet.of(characteristics, characteristics2, characteristics3));
        Collections.unmodifiableSet(EnumSet.of(characteristics, characteristics2));
        a = Collections.unmodifiableSet(EnumSet.of(characteristics3));
        Collections.unmodifiableSet(EnumSet.of(characteristics2, characteristics3));
        Collections.emptySet();
        Collections.unmodifiableSet(EnumSet.of(characteristics2));
        b = l20.a();
        c = m20.a();
        d = n20.a();
        o20.a();
        e = p20.b();
        q20.b();
    }

    public static <T> ew3<List<T>> a() {
        return k20.a();
    }

    public static final <T> ep<List<T>, T> d() {
        return (ep<List<T>, T>) e;
    }

    public static <T> Collector<T, ?, List<T>> e() {
        return new a(a(), d(), r20.a(), a);
    }

    /* compiled from: Collectors.java */
    /* loaded from: classes2.dex */
    public static class a<T, A, R> implements Collector<T, A, R> {
        public final ew3<A> a;
        public final ep<A, T> b;
        public final jp<A> c;
        public final nd1<A, R> d;
        public final Set<Collector.Characteristics> e;

        public a(ew3<A> ew3Var, ep<A, T> epVar, jp<A> jpVar, nd1<A, R> nd1Var, Set<Collector.Characteristics> set) {
            this.a = ew3Var;
            this.b = epVar;
            this.c = jpVar;
            this.d = nd1Var;
            this.e = set;
        }

        public static /* synthetic */ Object f(Object obj) {
            return obj;
        }

        @Override // java8.util.stream.Collector
        public jp<A> a() {
            return this.c;
        }

        @Override // java8.util.stream.Collector
        public Set<Collector.Characteristics> b() {
            return this.e;
        }

        @Override // java8.util.stream.Collector
        public nd1<A, R> c() {
            return this.d;
        }

        @Override // java8.util.stream.Collector
        public ew3<A> d() {
            return this.a;
        }

        @Override // java8.util.stream.Collector
        public ep<A, T> e() {
            return this.b;
        }

        public a(ew3<A> ew3Var, ep<A, T> epVar, jp<A> jpVar, Set<Collector.Characteristics> set) {
            this(ew3Var, epVar, jpVar, s20.a(), set);
        }
    }
}
