package java8.util.stream;

/* loaded from: classes2.dex */
public enum StreamShape {
    REFERENCE,
    INT_VALUE,
    LONG_VALUE,
    DOUBLE_VALUE
}
