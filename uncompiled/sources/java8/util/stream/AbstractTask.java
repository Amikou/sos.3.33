package java8.util.stream;

import java8.util.concurrent.CountedCompleter;
import java8.util.s;
import java8.util.stream.AbstractTask;

/* loaded from: classes2.dex */
public abstract class AbstractTask<P_IN, P_OUT, R, K extends AbstractTask<P_IN, P_OUT, R, K>> extends CountedCompleter<R> {
    public static final int l0 = java8.util.concurrent.a.n() << 2;
    public final e<P_OUT> helper;
    public K leftChild;
    private R localResult;
    public K rightChild;
    public s<P_IN> spliterator;
    public long targetSize;

    public AbstractTask(e<P_OUT> eVar, s<P_IN> sVar) {
        super(null);
        this.helper = eVar;
        this.spliterator = sVar;
        this.targetSize = 0L;
    }

    public static int getLeafTarget() {
        Thread currentThread = Thread.currentThread();
        if (currentThread instanceof java8.util.concurrent.b) {
            return ((java8.util.concurrent.b) currentThread).b().o() << 2;
        }
        return l0;
    }

    public static long suggestTargetSize(long j) {
        long leafTarget = j / getLeafTarget();
        if (leafTarget > 0) {
            return leafTarget;
        }
        return 1L;
    }

    @Override // java8.util.concurrent.CountedCompleter
    public void compute() {
        s<P_IN> c;
        s<P_IN> sVar = this.spliterator;
        long l = sVar.l();
        long targetSize = getTargetSize(l);
        boolean z = false;
        AbstractTask<P_IN, P_OUT, R, K> abstractTask = this;
        while (l > targetSize && (c = sVar.c()) != null) {
            K makeChild = abstractTask.makeChild(c);
            abstractTask.leftChild = makeChild;
            K makeChild2 = abstractTask.makeChild(sVar);
            abstractTask.rightChild = makeChild2;
            abstractTask.setPendingCount(1);
            if (z) {
                sVar = c;
                abstractTask = makeChild;
                makeChild = makeChild2;
            } else {
                abstractTask = makeChild2;
            }
            z = !z;
            makeChild.fork();
            l = sVar.l();
        }
        abstractTask.setLocalResult(abstractTask.doLeaf());
        abstractTask.tryComplete();
    }

    public abstract R doLeaf();

    public R getLocalResult() {
        return this.localResult;
    }

    public K getParent() {
        return (K) getCompleter();
    }

    @Override // java8.util.concurrent.CountedCompleter, java8.util.concurrent.ForkJoinTask
    public R getRawResult() {
        return this.localResult;
    }

    public final long getTargetSize(long j) {
        long j2 = this.targetSize;
        if (j2 != 0) {
            return j2;
        }
        long suggestTargetSize = suggestTargetSize(j);
        this.targetSize = suggestTargetSize;
        return suggestTargetSize;
    }

    public boolean isLeaf() {
        return this.leftChild == null;
    }

    public boolean isLeftmostNode() {
        AbstractTask<P_IN, P_OUT, R, K> abstractTask = this;
        while (abstractTask != null) {
            K parent = abstractTask.getParent();
            if (parent != null && parent.leftChild != abstractTask) {
                return false;
            }
            abstractTask = parent;
        }
        return true;
    }

    public boolean isRoot() {
        return getParent() == null;
    }

    public abstract K makeChild(s<P_IN> sVar);

    @Override // java8.util.concurrent.CountedCompleter
    public void onCompletion(CountedCompleter<?> countedCompleter) {
        this.spliterator = null;
        this.rightChild = null;
        this.leftChild = null;
    }

    public void setLocalResult(R r) {
        this.localResult = r;
    }

    @Override // java8.util.concurrent.CountedCompleter, java8.util.concurrent.ForkJoinTask
    public void setRawResult(R r) {
        if (r != null) {
            throw new IllegalStateException();
        }
    }

    public AbstractTask(K k, s<P_IN> sVar) {
        super(k);
        this.spliterator = sVar;
        this.helper = k.helper;
        this.targetSize = k.targetSize;
    }
}
