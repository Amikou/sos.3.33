package java8.util.stream;

import java.util.Collection;
import java8.util.s;
import java8.util.stream.f;
import java8.util.t;

/* compiled from: StreamSupport.java */
/* loaded from: classes2.dex */
public final class h {
    public static <T> au3<T> a(Collection<? extends T> collection) {
        return b(t.v(collection), false);
    }

    public static <T> au3<T> b(s<T> sVar, boolean z) {
        rl2.f(sVar);
        return new f.c(sVar, StreamOpFlag.fromCharacteristics((s<?>) sVar), z);
    }
}
