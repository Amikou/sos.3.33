package java8.util.stream;

import java8.util.o;
import java8.util.s;
import java8.util.stream.Collector;
import java8.util.stream.MatchOps;
import java8.util.stream.c;
import java8.util.stream.g;

/* compiled from: ReferencePipeline.java */
/* loaded from: classes2.dex */
public abstract class f<P_IN, P_OUT> extends java8.util.stream.a<P_IN, P_OUT, au3<P_OUT>> implements au3<P_OUT> {

    /* compiled from: ReferencePipeline.java */
    /* loaded from: classes2.dex */
    public class a extends d<P_OUT, P_OUT> {
        public final /* synthetic */ fu2 l;

        /* compiled from: ReferencePipeline.java */
        /* renamed from: java8.util.stream.f$a$a  reason: collision with other inner class name */
        /* loaded from: classes2.dex */
        public class C0192a extends g.a<P_OUT, P_OUT> {
            public C0192a(g gVar) {
                super(gVar);
            }

            @Override // defpackage.m60
            public void accept(P_OUT p_out) {
                if (a.this.l.a(p_out)) {
                    this.a.accept(p_out);
                }
            }

            @Override // java8.util.stream.g.a, java8.util.stream.g
            public void b(long j) {
                this.a.b(-1L);
            }
        }

        /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
        public a(f fVar, java8.util.stream.a aVar, StreamShape streamShape, int i, fu2 fu2Var) {
            super(aVar, streamShape, i);
            this.l = fu2Var;
        }

        @Override // java8.util.stream.a
        public g<P_OUT> v(int i, g<P_OUT> gVar) {
            return new C0192a(gVar);
        }
    }

    /* compiled from: ReferencePipeline.java */
    /* loaded from: classes2.dex */
    public class b extends d<P_OUT, R> {
        public final /* synthetic */ nd1 l;

        /* compiled from: ReferencePipeline.java */
        /* loaded from: classes2.dex */
        public class a extends g.a<P_OUT, R> {
            public a(g gVar) {
                super(gVar);
            }

            @Override // defpackage.m60
            public void accept(P_OUT p_out) {
                this.a.accept(b.this.l.apply(p_out));
            }
        }

        /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
        public b(f fVar, java8.util.stream.a aVar, StreamShape streamShape, int i, nd1 nd1Var) {
            super(aVar, streamShape, i);
            this.l = nd1Var;
        }

        @Override // java8.util.stream.a
        public g<P_OUT> v(int i, g<R> gVar) {
            return new a(gVar);
        }
    }

    /* compiled from: ReferencePipeline.java */
    /* loaded from: classes2.dex */
    public static class c<E_IN, E_OUT> extends f<E_IN, E_OUT> {
        public c(s<?> sVar, int i, boolean z) {
            super(sVar, i, z);
        }

        @Override // java8.util.stream.a
        public final boolean u() {
            throw new UnsupportedOperationException();
        }

        @Override // java8.util.stream.a
        public final g<E_IN> v(int i, g<E_OUT> gVar) {
            throw new UnsupportedOperationException();
        }

        /* JADX WARN: Multi-variable type inference failed */
        @Override // java8.util.stream.f
        public void y(m60<? super E_OUT> m60Var) {
            if (!q()) {
                x().a(m60Var);
            } else {
                super.y(m60Var);
            }
        }
    }

    /* compiled from: ReferencePipeline.java */
    /* loaded from: classes2.dex */
    public static abstract class d<E_IN, E_OUT> extends f<E_IN, E_OUT> {
        public d(java8.util.stream.a<?, E_IN, ?> aVar, StreamShape streamShape, int i) {
            super(aVar, i);
        }

        @Override // java8.util.stream.a
        public final boolean u() {
            return false;
        }
    }

    public f(s<?> sVar, int i, boolean z) {
        super(sVar, i, z);
    }

    @Override // defpackage.au3
    public final long a() {
        return ((Long) n(ReduceOps.b())).longValue();
    }

    @Override // defpackage.au3
    public final au3<P_OUT> b(fu2<? super P_OUT> fu2Var) {
        rl2.f(fu2Var);
        return new a(this, this, StreamShape.REFERENCE, StreamOpFlag.NOT_SIZED, fu2Var);
    }

    @Override // defpackage.au3
    public final <R> au3<R> c(nd1<? super P_OUT, ? extends R> nd1Var) {
        rl2.f(nd1Var);
        return new b(this, this, StreamShape.REFERENCE, StreamOpFlag.NOT_SORTED | StreamOpFlag.NOT_DISTINCT, nd1Var);
    }

    @Override // defpackage.au3
    public final o<P_OUT> d() {
        return (o) n(FindOps.a(false));
    }

    @Override // defpackage.au3
    public final boolean e(fu2<? super P_OUT> fu2Var) {
        return ((Boolean) n(MatchOps.b(fu2Var, MatchOps.MatchKind.ALL))).booleanValue();
    }

    /* JADX WARN: Type inference failed for: r0v14, types: [java.lang.Object] */
    @Override // defpackage.au3
    public final <R, A> R f(Collector<? super P_OUT, A, R> collector) {
        A a2;
        if (q() && collector.b().contains(Collector.Characteristics.CONCURRENT) && (!p() || collector.b().contains(Collector.Characteristics.UNORDERED))) {
            a2 = collector.d().get();
            y(c53.a(collector.e(), a2));
        } else {
            a2 = (R) n(ReduceOps.a(collector));
        }
        return collector.b().contains(Collector.Characteristics.IDENTITY_FINISH) ? (R) a2 : collector.c().apply(a2);
    }

    @Override // java8.util.stream.e
    public final c.a<P_OUT> j(long j, nr1<P_OUT[]> nr1Var) {
        return java8.util.stream.d.b(j, nr1Var);
    }

    @Override // java8.util.stream.a
    public final boolean o(s<P_OUT> sVar, g<P_OUT> gVar) {
        boolean d2;
        do {
            d2 = gVar.d();
            if (d2) {
                break;
            }
        } while (sVar.d(gVar));
        return d2;
    }

    public void y(m60<? super P_OUT> m60Var) {
        n(ForEachOps.a(m60Var, false));
    }

    public f(java8.util.stream.a<?, P_IN, ?> aVar, int i) {
        super(aVar, i);
    }
}
