package java8.util.stream;

import java8.util.concurrent.CountedCompleter;
import java8.util.s;
import java8.util.stream.Collector;

/* loaded from: classes2.dex */
public final class ReduceOps {

    /* loaded from: classes2.dex */
    public static final class ReduceTask<P_IN, P_OUT, R, S extends d<P_OUT, R, S>> extends AbstractTask<P_IN, P_OUT, S, ReduceTask<P_IN, P_OUT, R, S>> {
        private final g<P_OUT, R, S> op;

        public ReduceTask(g<P_OUT, R, S> gVar, java8.util.stream.e<P_OUT> eVar, s<P_IN> sVar) {
            super(eVar, sVar);
            this.op = gVar;
        }

        @Override // java8.util.stream.AbstractTask, java8.util.concurrent.CountedCompleter
        public void onCompletion(CountedCompleter<?> countedCompleter) {
            if (!isLeaf()) {
                d dVar = (d) ((ReduceTask) this.leftChild).getLocalResult();
                dVar.c((d) ((ReduceTask) this.rightChild).getLocalResult());
                setLocalResult(dVar);
            }
            super.onCompletion(countedCompleter);
        }

        @Override // java8.util.stream.AbstractTask
        public S doLeaf() {
            return (S) this.helper.k(this.op.b(), this.spliterator);
        }

        @Override // java8.util.stream.AbstractTask
        public ReduceTask<P_IN, P_OUT, R, S> makeChild(s<P_IN> sVar) {
            return new ReduceTask<>(this, sVar);
        }

        public ReduceTask(ReduceTask<P_IN, P_OUT, R, S> reduceTask, s<P_IN> sVar) {
            super(reduceTask, sVar);
            this.op = reduceTask.op;
        }
    }

    /* loaded from: classes2.dex */
    public static class a extends g<T, I, b> {
        public final /* synthetic */ jp a;
        public final /* synthetic */ ep b;
        public final /* synthetic */ ew3 c;
        public final /* synthetic */ Collector d;

        /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
        public a(StreamShape streamShape, jp jpVar, ep epVar, ew3 ew3Var, Collector collector) {
            super(streamShape);
            this.a = jpVar;
            this.b = epVar;
            this.c = ew3Var;
            this.d = collector;
        }

        @Override // java8.util.stream.ReduceOps.g
        /* renamed from: c */
        public b b() {
            return new b(this.c, this.b, this.a);
        }

        @Override // defpackage.b44
        public int f() {
            if (this.d.b().contains(Collector.Characteristics.UNORDERED)) {
                return StreamOpFlag.NOT_ORDERED;
            }
            return 0;
        }
    }

    /* loaded from: classes2.dex */
    public class b extends e<I> implements d<T, I, b> {
        public final /* synthetic */ ew3 b;
        public final /* synthetic */ ep c;
        public final /* synthetic */ jp d;

        public b(ew3 ew3Var, ep epVar, jp jpVar) {
            this.b = ew3Var;
            this.c = epVar;
            this.d = jpVar;
        }

        /* JADX WARN: Type inference failed for: r3v2, types: [U, java.lang.Object] */
        @Override // java8.util.stream.ReduceOps.d
        /* renamed from: a */
        public void c(b bVar) {
            this.a = this.d.apply(this.a, bVar.a);
        }

        @Override // defpackage.m60
        public void accept(T t) {
            this.c.a(this.a, t);
        }

        /* JADX WARN: Type inference failed for: r1v2, types: [U, java.lang.Object] */
        @Override // java8.util.stream.g
        public void b(long j) {
            this.a = this.b.get();
        }

        @Override // java8.util.stream.g
        public boolean d() {
            return false;
        }

        @Override // java8.util.stream.g
        public void g() {
        }
    }

    /* loaded from: classes2.dex */
    public static class c extends g<T, Long, f<T>> {
        public c(StreamShape streamShape) {
            super(streamShape);
        }

        @Override // java8.util.stream.ReduceOps.g, defpackage.b44
        /* renamed from: c */
        public <P_IN> Long e(java8.util.stream.e<T> eVar, s<P_IN> sVar) {
            if (StreamOpFlag.SIZED.isKnown(eVar.i())) {
                return Long.valueOf(sVar.i());
            }
            return (Long) super.e(eVar, sVar);
        }

        @Override // java8.util.stream.ReduceOps.g, defpackage.b44
        /* renamed from: d */
        public <P_IN> Long a(java8.util.stream.e<T> eVar, s<P_IN> sVar) {
            if (StreamOpFlag.SIZED.isKnown(eVar.i())) {
                return Long.valueOf(sVar.i());
            }
            return (Long) super.a(eVar, sVar);
        }

        @Override // defpackage.b44
        public int f() {
            return StreamOpFlag.NOT_ORDERED;
        }

        @Override // java8.util.stream.ReduceOps.g
        /* renamed from: g */
        public f<T> b() {
            return new f.a();
        }
    }

    /* loaded from: classes2.dex */
    public interface d<T, R, K extends d<T, R, K>> extends c44<T, R> {
        void c(K k);
    }

    /* loaded from: classes2.dex */
    public static abstract class e<U> {
        public U a;

        public U get() {
            return this.a;
        }
    }

    /* loaded from: classes2.dex */
    public static abstract class f<T> extends e<Long> implements d<T, Long, f<T>> {
        public long b;

        /* loaded from: classes2.dex */
        public static final class a<T> extends f<T> {
            @Override // defpackage.m60
            public void accept(T t) {
                this.b++;
            }

            @Override // java8.util.stream.ReduceOps.f, java8.util.stream.ReduceOps.d
            public /* bridge */ /* synthetic */ void c(d dVar) {
                super.c((f) dVar);
            }

            @Override // java8.util.stream.ReduceOps.f, java8.util.stream.ReduceOps.e, defpackage.ew3
            public /* bridge */ /* synthetic */ Object get() {
                return super.get();
            }
        }

        @Override // java8.util.stream.ReduceOps.d
        /* renamed from: a */
        public void c(f<T> fVar) {
            this.b += fVar.b;
        }

        @Override // java8.util.stream.g
        public void b(long j) {
            this.b = 0L;
        }

        @Override // java8.util.stream.g
        public boolean d() {
            return false;
        }

        @Override // java8.util.stream.ReduceOps.e, defpackage.ew3
        /* renamed from: e */
        public Long get() {
            return Long.valueOf(this.b);
        }

        @Override // java8.util.stream.g
        public void g() {
        }
    }

    /* loaded from: classes2.dex */
    public static abstract class g<T, R, S extends d<T, R, S>> implements b44<T, R> {
        public g(StreamShape streamShape) {
        }

        @Override // defpackage.b44
        public <P_IN> R a(java8.util.stream.e<T> eVar, s<P_IN> sVar) {
            return ((d) eVar.k(b(), sVar)).get();
        }

        public abstract S b();

        @Override // defpackage.b44
        public <P_IN> R e(java8.util.stream.e<T> eVar, s<P_IN> sVar) {
            return ((d) new ReduceTask(this, eVar, sVar).invoke()).get();
        }
    }

    public static <T, I> b44<T, I> a(Collector<? super T, I, ?> collector) {
        ew3 d2 = ((Collector) rl2.f(collector)).d();
        ep<I, ? super T> e2 = collector.e();
        return new a(StreamShape.REFERENCE, collector.a(), e2, d2, collector);
    }

    public static <T> b44<T, Long> b() {
        return new c(StreamShape.REFERENCE);
    }
}
