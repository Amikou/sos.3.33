package java8.util.stream;

import java.util.EnumMap;
import java.util.Map;
import java.util.concurrent.ConcurrentMap;
import java8.util.n;
import java8.util.s;

/* JADX WARN: Enum visitor error
jadx.core.utils.exceptions.JadxRuntimeException: Init of enum DISTINCT uses external variables
	at jadx.core.dex.visitors.EnumVisitor.createEnumFieldByConstructor(EnumVisitor.java:444)
	at jadx.core.dex.visitors.EnumVisitor.processEnumFieldByRegister(EnumVisitor.java:391)
	at jadx.core.dex.visitors.EnumVisitor.extractEnumFieldsFromFilledArray(EnumVisitor.java:320)
	at jadx.core.dex.visitors.EnumVisitor.extractEnumFieldsFromInsn(EnumVisitor.java:258)
	at jadx.core.dex.visitors.EnumVisitor.convertToEnum(EnumVisitor.java:151)
	at jadx.core.dex.visitors.EnumVisitor.visit(EnumVisitor.java:100)
 */
/* JADX WARN: Failed to restore enum class, 'enum' modifier and super class removed */
/* loaded from: classes2.dex */
public final class StreamOpFlag {
    public static final StreamOpFlag DISTINCT;
    public static final int INITIAL_OPS_VALUE;
    public static final int IS_DISTINCT;
    public static final int IS_ORDERED;
    public static final int IS_SHORT_CIRCUIT;
    public static final int IS_SIZED;
    public static final int IS_SORTED;
    public static final int NOT_DISTINCT;
    public static final int NOT_ORDERED;
    public static final int NOT_SIZED;
    public static final int NOT_SORTED;
    public static final int OP_MASK;
    public static final StreamOpFlag ORDERED;
    public static final StreamOpFlag SHORT_CIRCUIT;
    public static final StreamOpFlag SIZED;
    public static final StreamOpFlag SORTED;
    public static final int SPLITERATOR_CHARACTERISTICS_MASK;
    public static final int STREAM_MASK;
    public static final int TERMINAL_OP_MASK;
    public static final int UPSTREAM_TERMINAL_OP_MASK;
    public static final int a;
    public static final int f0;
    public static final int g0;
    public static final /* synthetic */ StreamOpFlag[] h0;
    private final int bitPosition;
    private final int clear;
    private final Map<Type, Integer> maskTable;
    private final int preserve;
    private final int set;

    /* loaded from: classes2.dex */
    public enum Type {
        SPLITERATOR,
        STREAM,
        OP,
        TERMINAL_OP,
        UPSTREAM_TERMINAL_OP
    }

    /* loaded from: classes2.dex */
    public static class a {
        public final Map<Type, Integer> a;

        public a(Map<Type, Integer> map) {
            this.a = map;
        }

        public Map<Type, Integer> a() {
            Map<Type, Integer> map = this.a;
            int i = 0;
            if (map instanceof ConcurrentMap) {
                ConcurrentMap concurrentMap = (ConcurrentMap) map;
                Type[] values = Type.values();
                int length = values.length;
                while (i < length) {
                    concurrentMap.putIfAbsent(values[i], 0);
                    i++;
                }
                return concurrentMap;
            }
            Type[] values2 = Type.values();
            int length2 = values2.length;
            while (i < length2) {
                n.b(this.a, values2[i], 0);
                i++;
            }
            return this.a;
        }

        public a b(Type type) {
            return c(type, 2);
        }

        public a c(Type type, Integer num) {
            this.a.put(type, num);
            return this;
        }

        public a d(Type type) {
            return c(type, 1);
        }

        public a e(Type type) {
            return c(type, 3);
        }
    }

    static {
        Type type = Type.SPLITERATOR;
        a f = f(type);
        Type type2 = Type.STREAM;
        a d = f.d(type2);
        Type type3 = Type.OP;
        StreamOpFlag streamOpFlag = new StreamOpFlag("DISTINCT", 0, 0, d.e(type3));
        DISTINCT = streamOpFlag;
        StreamOpFlag streamOpFlag2 = new StreamOpFlag("SORTED", 1, 1, f(type).d(type2).e(type3));
        SORTED = streamOpFlag2;
        a e = f(type).d(type2).e(type3);
        Type type4 = Type.TERMINAL_OP;
        a b = e.b(type4);
        Type type5 = Type.UPSTREAM_TERMINAL_OP;
        StreamOpFlag streamOpFlag3 = new StreamOpFlag("ORDERED", 2, 2, b.b(type5));
        ORDERED = streamOpFlag3;
        StreamOpFlag streamOpFlag4 = new StreamOpFlag("SIZED", 3, 3, f(type).d(type2).b(type3));
        SIZED = streamOpFlag4;
        StreamOpFlag streamOpFlag5 = new StreamOpFlag("SHORT_CIRCUIT", 4, 12, f(type3).d(type4));
        SHORT_CIRCUIT = streamOpFlag5;
        h0 = new StreamOpFlag[]{streamOpFlag, streamOpFlag2, streamOpFlag3, streamOpFlag4, streamOpFlag5};
        SPLITERATOR_CHARACTERISTICS_MASK = d(type);
        int d2 = d(type2);
        STREAM_MASK = d2;
        OP_MASK = d(type3);
        TERMINAL_OP_MASK = d(type4);
        UPSTREAM_TERMINAL_OP_MASK = d(type5);
        a = a();
        f0 = d2;
        int i = d2 << 1;
        g0 = i;
        INITIAL_OPS_VALUE = d2 | i;
        IS_DISTINCT = streamOpFlag.set;
        NOT_DISTINCT = streamOpFlag.clear;
        IS_SORTED = streamOpFlag2.set;
        NOT_SORTED = streamOpFlag2.clear;
        IS_ORDERED = streamOpFlag3.set;
        NOT_ORDERED = streamOpFlag3.clear;
        IS_SIZED = streamOpFlag4.set;
        NOT_SIZED = streamOpFlag4.clear;
        IS_SHORT_CIRCUIT = streamOpFlag5.set;
    }

    public StreamOpFlag(String str, int i, int i2, a aVar) {
        this.maskTable = aVar.a();
        int i3 = i2 * 2;
        this.bitPosition = i3;
        this.set = 1 << i3;
        this.clear = 2 << i3;
        this.preserve = 3 << i3;
    }

    public static int a() {
        int i = 0;
        for (StreamOpFlag streamOpFlag : values()) {
            i |= streamOpFlag.preserve;
        }
        return i;
    }

    public static int combineOpFlags(int i, int i2) {
        return i | (i2 & e(i));
    }

    public static int d(Type type) {
        StreamOpFlag[] values;
        int i = 0;
        for (StreamOpFlag streamOpFlag : values()) {
            i |= streamOpFlag.maskTable.get(type).intValue() << streamOpFlag.bitPosition;
        }
        return i;
    }

    public static int e(int i) {
        if (i == 0) {
            return a;
        }
        return ~(((i & g0) >> 1) | ((f0 & i) << 1) | i);
    }

    public static a f(Type type) {
        return new a(new EnumMap(Type.class)).d(type);
    }

    public static int fromCharacteristics(s<?> sVar) {
        int b = sVar.b();
        if ((b & 4) != 0 && sVar.f() != null) {
            return SPLITERATOR_CHARACTERISTICS_MASK & b & (-5);
        }
        return SPLITERATOR_CHARACTERISTICS_MASK & b;
    }

    public static int toCharacteristics(int i) {
        return i & SPLITERATOR_CHARACTERISTICS_MASK;
    }

    public static int toStreamFlags(int i) {
        return i & ((~i) >> 1) & f0;
    }

    public static StreamOpFlag valueOf(String str) {
        return (StreamOpFlag) Enum.valueOf(StreamOpFlag.class, str);
    }

    public static StreamOpFlag[] values() {
        return (StreamOpFlag[]) h0.clone();
    }

    public boolean canSet(Type type) {
        return (this.maskTable.get(type).intValue() & 1) > 0;
    }

    public int clear() {
        return this.clear;
    }

    public boolean isCleared(int i) {
        return (i & this.preserve) == this.clear;
    }

    public boolean isKnown(int i) {
        return (i & this.preserve) == this.set;
    }

    public boolean isPreserved(int i) {
        int i2 = this.preserve;
        return (i & i2) == i2;
    }

    public boolean isStreamFlag() {
        return this.maskTable.get(Type.STREAM).intValue() > 0;
    }

    public int set() {
        return this.set;
    }

    public static int fromCharacteristics(int i) {
        return i & SPLITERATOR_CHARACTERISTICS_MASK;
    }
}
