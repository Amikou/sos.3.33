package java8.util.stream;

import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;
import java8.util.concurrent.CountedCompleter;
import java8.util.s;
import java8.util.stream.c;

/* loaded from: classes2.dex */
public final class ForEachOps {

    /* loaded from: classes2.dex */
    public static abstract class a<T> implements b44<T, Void>, c44<T, Void> {
        public final boolean a;

        /* renamed from: java8.util.stream.ForEachOps$a$a  reason: collision with other inner class name */
        /* loaded from: classes2.dex */
        public static final class C0187a<T> extends a<T> {
            public final m60<? super T> b;

            public C0187a(m60<? super T> m60Var, boolean z) {
                super(z);
                this.b = m60Var;
            }

            @Override // java8.util.stream.ForEachOps.a, defpackage.b44
            public /* bridge */ /* synthetic */ Void a(e eVar, s sVar) {
                return super.a(eVar, sVar);
            }

            @Override // defpackage.m60
            public void accept(T t) {
                this.b.accept(t);
            }

            @Override // java8.util.stream.ForEachOps.a, defpackage.b44
            public /* bridge */ /* synthetic */ Void e(e eVar, s sVar) {
                return super.e(eVar, sVar);
            }

            @Override // java8.util.stream.ForEachOps.a, defpackage.ew3
            public /* bridge */ /* synthetic */ Object get() {
                return super.get();
            }
        }

        public a(boolean z) {
            this.a = z;
        }

        @Override // java8.util.stream.g
        public void b(long j) {
        }

        @Override // java8.util.stream.g
        public boolean d() {
            return false;
        }

        @Override // defpackage.b44
        public int f() {
            if (this.a) {
                return 0;
            }
            return StreamOpFlag.NOT_ORDERED;
        }

        @Override // java8.util.stream.g
        public void g() {
        }

        @Override // defpackage.b44
        /* renamed from: h */
        public <S> Void e(e<T> eVar, s<S> sVar) {
            if (this.a) {
                new ForEachOrderedTask(eVar, sVar, this).invoke();
                return null;
            }
            new ForEachTask(eVar, sVar, eVar.l(this)).invoke();
            return null;
        }

        /* JADX WARN: Multi-variable type inference failed */
        @Override // defpackage.b44
        /* renamed from: i */
        public <S> Void a(e<T> eVar, s<S> sVar) {
            return ((a) eVar.k(this, sVar)).get();
        }

        @Override // defpackage.ew3
        /* renamed from: j */
        public Void get() {
            return null;
        }
    }

    public static <T> b44<T, Void> a(m60<? super T> m60Var, boolean z) {
        rl2.f(m60Var);
        return new a.C0187a(m60Var, z);
    }

    /* loaded from: classes2.dex */
    public static final class ForEachTask<S, T> extends CountedCompleter<Void> {
        private final e<T> helper;
        private final g<S> sink;
        private s<S> spliterator;
        private long targetSize;

        public ForEachTask(e<T> eVar, s<S> sVar, g<S> gVar) {
            super(null);
            this.sink = gVar;
            this.helper = eVar;
            this.spliterator = sVar;
            this.targetSize = 0L;
        }

        @Override // java8.util.concurrent.CountedCompleter
        public void compute() {
            s<S> c;
            s<S> sVar = this.spliterator;
            long l = sVar.l();
            long j = this.targetSize;
            if (j == 0) {
                j = AbstractTask.suggestTargetSize(l);
                this.targetSize = j;
            }
            boolean isKnown = StreamOpFlag.SHORT_CIRCUIT.isKnown(this.helper.i());
            boolean z = false;
            g<S> gVar = this.sink;
            ForEachTask<S, T> forEachTask = this;
            while (true) {
                if (isKnown && gVar.d()) {
                    break;
                } else if (l <= j || (c = sVar.c()) == null) {
                    break;
                } else {
                    ForEachTask<S, T> forEachTask2 = new ForEachTask<>(forEachTask, c);
                    forEachTask.addToPendingCount(1);
                    if (z) {
                        sVar = c;
                    } else {
                        ForEachTask<S, T> forEachTask3 = forEachTask;
                        forEachTask = forEachTask2;
                        forEachTask2 = forEachTask3;
                    }
                    z = !z;
                    forEachTask.fork();
                    forEachTask = forEachTask2;
                    l = sVar.l();
                }
            }
            forEachTask.helper.g(gVar, sVar);
            forEachTask.spliterator = null;
            forEachTask.propagateCompletion();
        }

        public ForEachTask(ForEachTask<S, T> forEachTask, s<S> sVar) {
            super(forEachTask);
            this.spliterator = sVar;
            this.sink = forEachTask.sink;
            this.targetSize = forEachTask.targetSize;
            this.helper = forEachTask.helper;
        }
    }

    /* loaded from: classes2.dex */
    public static final class ForEachOrderedTask<S, T> extends CountedCompleter<Void> {
        private final g<T> action;
        private final ConcurrentMap<ForEachOrderedTask<S, T>, ForEachOrderedTask<S, T>> completionMap;
        private final e<T> helper;
        private final ForEachOrderedTask<S, T> leftPredecessor;
        private c<T> node;
        private s<S> spliterator;
        private final long targetSize;

        public ForEachOrderedTask(e<T> eVar, s<S> sVar, g<T> gVar) {
            super(null);
            this.helper = eVar;
            this.spliterator = sVar;
            this.targetSize = AbstractTask.suggestTargetSize(sVar.l());
            this.completionMap = new ConcurrentHashMap(Math.max(16, AbstractTask.getLeafTarget() << 1), 0.75f, java8.util.concurrent.a.n() + 1);
            this.action = gVar;
            this.leftPredecessor = null;
        }

        public static <S, T> void l(ForEachOrderedTask<S, T> forEachOrderedTask) {
            s<S> c;
            s<S> sVar = ((ForEachOrderedTask) forEachOrderedTask).spliterator;
            long j = ((ForEachOrderedTask) forEachOrderedTask).targetSize;
            boolean z = false;
            while (sVar.l() > j && (c = sVar.c()) != null) {
                ForEachOrderedTask<S, T> forEachOrderedTask2 = new ForEachOrderedTask<>(forEachOrderedTask, c, ((ForEachOrderedTask) forEachOrderedTask).leftPredecessor);
                ForEachOrderedTask<S, T> forEachOrderedTask3 = new ForEachOrderedTask<>(forEachOrderedTask, sVar, forEachOrderedTask2);
                forEachOrderedTask.addToPendingCount(1);
                forEachOrderedTask3.addToPendingCount(1);
                ((ForEachOrderedTask) forEachOrderedTask).completionMap.put(forEachOrderedTask2, forEachOrderedTask3);
                if (((ForEachOrderedTask) forEachOrderedTask).leftPredecessor != null) {
                    forEachOrderedTask2.addToPendingCount(1);
                    if (((ForEachOrderedTask) forEachOrderedTask).completionMap.replace(((ForEachOrderedTask) forEachOrderedTask).leftPredecessor, forEachOrderedTask, forEachOrderedTask2)) {
                        forEachOrderedTask.addToPendingCount(-1);
                    } else {
                        forEachOrderedTask2.addToPendingCount(-1);
                    }
                }
                if (z) {
                    sVar = c;
                    forEachOrderedTask = forEachOrderedTask2;
                    forEachOrderedTask2 = forEachOrderedTask3;
                } else {
                    forEachOrderedTask = forEachOrderedTask3;
                }
                z = !z;
                forEachOrderedTask2.fork();
            }
            if (forEachOrderedTask.getPendingCount() > 0) {
                nr1<T[]> b = p81.b();
                e<T> eVar = ((ForEachOrderedTask) forEachOrderedTask).helper;
                ((ForEachOrderedTask) forEachOrderedTask).node = ((c.a) ((ForEachOrderedTask) forEachOrderedTask).helper.k(eVar.j(eVar.h(sVar), b), sVar)).build();
                ((ForEachOrderedTask) forEachOrderedTask).spliterator = null;
            }
            forEachOrderedTask.tryComplete();
        }

        public static /* synthetic */ Object[] lambda$doCompute$80(int i) {
            return new Object[i];
        }

        @Override // java8.util.concurrent.CountedCompleter
        public final void compute() {
            l(this);
        }

        @Override // java8.util.concurrent.CountedCompleter
        public void onCompletion(CountedCompleter<?> countedCompleter) {
            c<T> cVar = this.node;
            if (cVar != null) {
                cVar.e(this.action);
                this.node = null;
            } else {
                s<S> sVar = this.spliterator;
                if (sVar != null) {
                    this.helper.k(this.action, sVar);
                    this.spliterator = null;
                }
            }
            ForEachOrderedTask<S, T> remove = this.completionMap.remove(this);
            if (remove != null) {
                remove.tryComplete();
            }
        }

        public ForEachOrderedTask(ForEachOrderedTask<S, T> forEachOrderedTask, s<S> sVar, ForEachOrderedTask<S, T> forEachOrderedTask2) {
            super(forEachOrderedTask);
            this.helper = forEachOrderedTask.helper;
            this.spliterator = sVar;
            this.targetSize = forEachOrderedTask.targetSize;
            this.completionMap = forEachOrderedTask.completionMap;
            this.action = forEachOrderedTask.action;
            this.leftPredecessor = forEachOrderedTask2;
        }
    }
}
