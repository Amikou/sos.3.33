package java8.util.stream;

import java8.util.s;

/* compiled from: Node.java */
/* loaded from: classes2.dex */
public interface c<T> {

    /* compiled from: Node.java */
    /* loaded from: classes2.dex */
    public interface a<T> extends g<T> {
        c<T> build();
    }

    /* compiled from: Node.java */
    /* loaded from: classes2.dex */
    public interface b extends e<Double, hq0, double[], s.a, b> {
    }

    /* compiled from: Node.java */
    /* renamed from: java8.util.stream.c$c  reason: collision with other inner class name */
    /* loaded from: classes2.dex */
    public interface InterfaceC0188c extends e<Integer, mr1, int[], s.b, InterfaceC0188c> {
    }

    /* compiled from: Node.java */
    /* loaded from: classes2.dex */
    public interface d extends e<Long, f22, long[], s.c, d> {
    }

    /* compiled from: Node.java */
    /* loaded from: classes2.dex */
    public interface e<T, T_CONS, T_ARR, T_SPLITR extends s.d<T, T_CONS, T_SPLITR>, T_NODE extends e<T, T_CONS, T_ARR, T_SPLITR, T_NODE>> extends c<T> {
        void a(T_CONS t_cons);

        @Override // java8.util.stream.c
        T_SPLITR spliterator();
    }

    void e(m60<? super T> m60Var);

    s<T> spliterator();
}
