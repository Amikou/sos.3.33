package java8.util.stream;

import java8.util.s;
import java8.util.stream.c;

/* compiled from: PipelineHelper.java */
/* loaded from: classes2.dex */
public abstract class e<P_OUT> {
    public abstract <P_IN> void g(g<P_IN> gVar, s<P_IN> sVar);

    public abstract <P_IN> long h(s<P_IN> sVar);

    public abstract int i();

    public abstract c.a<P_OUT> j(long j, nr1<P_OUT[]> nr1Var);

    public abstract <P_IN, S extends g<P_OUT>> S k(S s, s<P_IN> sVar);

    public abstract <P_IN> g<P_IN> l(g<P_OUT> gVar);
}
