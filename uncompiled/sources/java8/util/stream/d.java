package java8.util.stream;

import java.util.Arrays;
import java8.util.i;
import java8.util.s;
import java8.util.stream.c;
import java8.util.t;

/* compiled from: Nodes.java */
/* loaded from: classes2.dex */
public final class d {

    /* compiled from: Nodes.java */
    /* loaded from: classes2.dex */
    public static class b<T> implements java8.util.stream.c<T> {
        public final T[] a;
        public int b;

        public b(long j, nr1<T[]> nr1Var) {
            if (j < 2147483639) {
                this.a = nr1Var.a((int) j);
                this.b = 0;
                return;
            }
            throw new IllegalArgumentException("Stream size exceeds max array size");
        }

        @Override // java8.util.stream.c
        public void e(m60<? super T> m60Var) {
            for (int i = 0; i < this.b; i++) {
                m60Var.accept((Object) this.a[i]);
            }
        }

        @Override // java8.util.stream.c
        public s<T> spliterator() {
            return i.a(this.a, 0, this.b);
        }
    }

    /* compiled from: Nodes.java */
    /* loaded from: classes2.dex */
    public static abstract class c<T, T_ARR, T_CONS> implements java8.util.stream.c<T> {

        /* compiled from: Nodes.java */
        /* loaded from: classes2.dex */
        public static final class a extends c<Double, double[], hq0> implements c.b {
            @Override // java8.util.stream.c
            /* renamed from: b */
            public s.a spliterator() {
                return t.c();
            }

            @Override // java8.util.stream.c
            public void e(m60<? super Double> m60Var) {
                e.a(this, m60Var);
            }
        }

        /* compiled from: Nodes.java */
        /* loaded from: classes2.dex */
        public static final class b extends c<Integer, int[], mr1> implements c.InterfaceC0188c {
            @Override // java8.util.stream.c
            /* renamed from: b */
            public s.b spliterator() {
                return t.d();
            }

            @Override // java8.util.stream.c
            public void e(m60<? super Integer> m60Var) {
                f.a(this, m60Var);
            }
        }

        /* compiled from: Nodes.java */
        /* renamed from: java8.util.stream.d$c$c  reason: collision with other inner class name */
        /* loaded from: classes2.dex */
        public static final class C0189c extends c<Long, long[], f22> implements c.d {
            @Override // java8.util.stream.c
            /* renamed from: b */
            public s.c spliterator() {
                return t.e();
            }

            @Override // java8.util.stream.c
            public void e(m60<? super Long> m60Var) {
                g.a(this, m60Var);
            }
        }

        /* compiled from: Nodes.java */
        /* renamed from: java8.util.stream.d$c$d  reason: collision with other inner class name */
        /* loaded from: classes2.dex */
        public static class C0190d<T> extends c<T, T[], m60<? super T>> {
            @Override // java8.util.stream.c
            public /* bridge */ /* synthetic */ void e(m60 m60Var) {
                super.a(m60Var);
            }

            @Override // java8.util.stream.c
            public s<T> spliterator() {
                return t.f();
            }

            public C0190d() {
            }
        }

        public void a(T_CONS t_cons) {
        }
    }

    /* compiled from: Nodes.java */
    /* renamed from: java8.util.stream.d$d  reason: collision with other inner class name */
    /* loaded from: classes2.dex */
    public static final class C0191d<T> extends b<T> implements c.a<T> {
        public C0191d(long j, nr1<T[]> nr1Var) {
            super(j, nr1Var);
        }

        @Override // defpackage.m60
        public void accept(T t) {
            int i = this.b;
            T[] tArr = this.a;
            if (i < tArr.length) {
                this.b = i + 1;
                tArr[i] = t;
                return;
            }
            throw new IllegalStateException(String.format("Accept exceeded fixed size of %d", Integer.valueOf(this.a.length)));
        }

        @Override // java8.util.stream.g
        public void b(long j) {
            if (j == this.a.length) {
                this.b = 0;
                return;
            }
            throw new IllegalStateException(String.format("Begin size %d is not equal to fixed size %d", Long.valueOf(j), Integer.valueOf(this.a.length)));
        }

        @Override // java8.util.stream.c.a
        public java8.util.stream.c<T> build() {
            if (this.b >= this.a.length) {
                return this;
            }
            throw new IllegalStateException(String.format("Current size %d is less than fixed size %d", Integer.valueOf(this.b), Integer.valueOf(this.a.length)));
        }

        @Override // java8.util.stream.g
        public boolean d() {
            return false;
        }

        @Override // java8.util.stream.g
        public void g() {
            if (this.b < this.a.length) {
                throw new IllegalStateException(String.format("End size %d is less than fixed size %d", Integer.valueOf(this.b), Integer.valueOf(this.a.length)));
            }
        }

        public String toString() {
            return String.format("FixedNodeBuilder[%d][%s]", Integer.valueOf(this.a.length - this.b), Arrays.toString(this.a));
        }
    }

    /* compiled from: Nodes.java */
    /* loaded from: classes2.dex */
    public static final class e {
        public static void a(c.b bVar, m60<? super Double> m60Var) {
            if (m60Var instanceof hq0) {
                bVar.a((hq0) m60Var);
            } else {
                bVar.spliterator().a(m60Var);
            }
        }
    }

    /* compiled from: Nodes.java */
    /* loaded from: classes2.dex */
    public static final class f {
        public static void a(c.InterfaceC0188c interfaceC0188c, m60<? super Integer> m60Var) {
            if (m60Var instanceof mr1) {
                interfaceC0188c.a((mr1) m60Var);
            } else {
                interfaceC0188c.spliterator().a(m60Var);
            }
        }
    }

    /* compiled from: Nodes.java */
    /* loaded from: classes2.dex */
    public static final class g {
        public static void a(c.d dVar, m60<? super Long> m60Var) {
            if (m60Var instanceof f22) {
                dVar.a((f22) m60Var);
            } else {
                dVar.spliterator().a(m60Var);
            }
        }
    }

    /* compiled from: Nodes.java */
    /* loaded from: classes2.dex */
    public static final class h<T> extends qr3<T> implements java8.util.stream.c<T>, c.a<T> {
        @Override // defpackage.qr3, defpackage.m60
        public void accept(T t) {
            super.accept(t);
        }

        @Override // java8.util.stream.g
        public void b(long j) {
            i();
            j(j);
        }

        @Override // java8.util.stream.c.a
        public java8.util.stream.c<T> build() {
            return this;
        }

        @Override // java8.util.stream.g
        public boolean d() {
            return false;
        }

        /* JADX WARN: Multi-variable type inference failed */
        @Override // defpackage.qr3, java8.util.stream.c
        public void e(m60<? super T> m60Var) {
            super.e(m60Var);
        }

        @Override // java8.util.stream.g
        public void g() {
        }

        @Override // defpackage.qr3, java8.util.stream.c
        public s<T> spliterator() {
            return super.spliterator();
        }
    }

    static {
        new c.C0190d();
        new c.b();
        new c.C0189c();
        new c.a();
    }

    public static <T> c.a<T> a() {
        return new h();
    }

    public static <T> c.a<T> b(long j, nr1<T[]> nr1Var) {
        if (j >= 0 && j < 2147483639) {
            return new C0191d(j, nr1Var);
        }
        return a();
    }
}
