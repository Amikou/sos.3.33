package java8.util.stream;

import java.util.Set;

/* loaded from: classes2.dex */
public interface Collector<T, A, R> {

    /* loaded from: classes2.dex */
    public enum Characteristics {
        CONCURRENT,
        UNORDERED,
        IDENTITY_FINISH
    }

    jp<A> a();

    Set<Characteristics> b();

    nd1<A, R> c();

    ew3<A> d();

    ep<A, T> e();
}
