package java8.util.stream;

import java.util.concurrent.atomic.AtomicReference;
import java8.util.s;
import java8.util.stream.AbstractShortCircuitTask;

/* loaded from: classes2.dex */
public abstract class AbstractShortCircuitTask<P_IN, P_OUT, R, K extends AbstractShortCircuitTask<P_IN, P_OUT, R, K>> extends AbstractTask<P_IN, P_OUT, R, K> {
    public volatile boolean canceled;
    public final AtomicReference<R> sharedResult;

    public AbstractShortCircuitTask(e<P_OUT> eVar, s<P_IN> sVar) {
        super(eVar, sVar);
        this.sharedResult = new AtomicReference<>(null);
    }

    public void cancel() {
        this.canceled = true;
    }

    public void cancelLaterNodes() {
        AbstractShortCircuitTask<P_IN, P_OUT, R, K> abstractShortCircuitTask = this;
        for (AbstractShortCircuitTask<P_IN, P_OUT, R, K> abstractShortCircuitTask2 = (AbstractShortCircuitTask) getParent(); abstractShortCircuitTask2 != null; abstractShortCircuitTask2 = (AbstractShortCircuitTask) abstractShortCircuitTask2.getParent()) {
            if (abstractShortCircuitTask2.leftChild == abstractShortCircuitTask) {
                AbstractShortCircuitTask abstractShortCircuitTask3 = (AbstractShortCircuitTask) abstractShortCircuitTask2.rightChild;
                if (!abstractShortCircuitTask3.canceled) {
                    abstractShortCircuitTask3.cancel();
                }
            }
            abstractShortCircuitTask = abstractShortCircuitTask2;
        }
    }

    /* JADX WARN: Code restructure failed: missing block: B:18:0x004f, code lost:
        r8 = r7.doLeaf();
     */
    @Override // java8.util.stream.AbstractTask, java8.util.concurrent.CountedCompleter
    /*
        Code decompiled incorrectly, please refer to instructions dump.
        To view partially-correct code enable 'Show inconsistent code' option in preferences
    */
    public void compute() {
        /*
            r10 = this;
            java8.util.s<P_IN> r0 = r10.spliterator
            long r1 = r0.l()
            long r3 = r10.getTargetSize(r1)
            java.util.concurrent.atomic.AtomicReference<R> r5 = r10.sharedResult
            r6 = 0
            r7 = r10
        Le:
            java.lang.Object r8 = r5.get()
            if (r8 != 0) goto L53
            boolean r8 = r7.taskCanceled()
            if (r8 == 0) goto L1f
            java.lang.Object r8 = r7.getEmptyResult()
            goto L53
        L1f:
            int r1 = (r1 > r3 ? 1 : (r1 == r3 ? 0 : -1))
            if (r1 <= 0) goto L4f
            java8.util.s r1 = r0.c()
            if (r1 != 0) goto L2a
            goto L4f
        L2a:
            java8.util.stream.AbstractTask r2 = r7.makeChild(r1)
            java8.util.stream.AbstractShortCircuitTask r2 = (java8.util.stream.AbstractShortCircuitTask) r2
            r7.leftChild = r2
            java8.util.stream.AbstractTask r8 = r7.makeChild(r0)
            java8.util.stream.AbstractShortCircuitTask r8 = (java8.util.stream.AbstractShortCircuitTask) r8
            r7.rightChild = r8
            r9 = 1
            r7.setPendingCount(r9)
            if (r6 == 0) goto L44
            r0 = r1
            r7 = r2
            r2 = r8
            goto L45
        L44:
            r7 = r8
        L45:
            r6 = r6 ^ 1
            r2.fork()
            long r1 = r0.l()
            goto Le
        L4f:
            java.lang.Object r8 = r7.doLeaf()
        L53:
            r7.setLocalResult(r8)
            r7.tryComplete()
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: java8.util.stream.AbstractShortCircuitTask.compute():void");
    }

    public abstract R getEmptyResult();

    @Override // java8.util.stream.AbstractTask
    public R getLocalResult() {
        if (isRoot()) {
            R r = this.sharedResult.get();
            return r == null ? getEmptyResult() : r;
        }
        return (R) super.getLocalResult();
    }

    @Override // java8.util.stream.AbstractTask, java8.util.concurrent.CountedCompleter, java8.util.concurrent.ForkJoinTask
    public R getRawResult() {
        return getLocalResult();
    }

    @Override // java8.util.stream.AbstractTask
    public void setLocalResult(R r) {
        if (!isRoot()) {
            super.setLocalResult(r);
        } else if (r != null) {
            this.sharedResult.compareAndSet(null, r);
        }
    }

    public void shortCircuit(R r) {
        if (r != null) {
            this.sharedResult.compareAndSet(null, r);
        }
    }

    public boolean taskCanceled() {
        boolean z = this.canceled;
        if (!z) {
            Object parent = getParent();
            while (true) {
                AbstractShortCircuitTask abstractShortCircuitTask = (AbstractShortCircuitTask) parent;
                if (z || abstractShortCircuitTask == null) {
                    break;
                }
                z = abstractShortCircuitTask.canceled;
                parent = abstractShortCircuitTask.getParent();
            }
        }
        return z;
    }

    public AbstractShortCircuitTask(K k, s<P_IN> sVar) {
        super(k, sVar);
        this.sharedResult = k.sharedResult;
    }
}
