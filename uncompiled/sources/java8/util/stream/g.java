package java8.util.stream;

/* compiled from: Sink.java */
/* loaded from: classes2.dex */
public interface g<T> extends m60<T> {

    /* compiled from: Sink.java */
    /* loaded from: classes2.dex */
    public static abstract class a<T, E_OUT> implements g<T> {
        public final g<? super E_OUT> a;

        public a(g<? super E_OUT> gVar) {
            this.a = (g) rl2.f(gVar);
        }

        @Override // java8.util.stream.g
        public void b(long j) {
            this.a.b(j);
        }

        @Override // java8.util.stream.g
        public boolean d() {
            return this.a.d();
        }

        @Override // java8.util.stream.g
        public void g() {
            this.a.g();
        }
    }

    void b(long j);

    boolean d();

    void g();
}
