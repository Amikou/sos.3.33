package java8.util.stream;

import java8.util.concurrent.CountedCompleter;
import java8.util.o;
import java8.util.s;

/* loaded from: classes2.dex */
public final class FindOps {
    public static final fu2<o<Object>> a;
    public static final fu2<on2> b;
    public static final fu2<pn2> c;
    public static final fu2<nn2> d;
    public static final ew3<c44<Object, o<Object>>> e;
    public static final ew3<c44<Integer, on2>> f;
    public static final ew3<c44<Long, pn2>> g;
    public static final ew3<c44<Double, nn2>> h;
    public static final b44 i;
    public static final b44 j;

    /* loaded from: classes2.dex */
    public static final class FindTask<P_IN, P_OUT, O> extends AbstractShortCircuitTask<P_IN, P_OUT, O, FindTask<P_IN, P_OUT, O>> {
        private final boolean mustFindFirst;
        private final a<P_OUT, O> op;

        public FindTask(a<P_OUT, O> aVar, boolean z, e<P_OUT> eVar, s<P_IN> sVar) {
            super(eVar, sVar);
            this.mustFindFirst = z;
            this.op = aVar;
        }

        @Override // java8.util.stream.AbstractTask
        public O doLeaf() {
            O o = (O) ((c44) this.helper.k(this.op.d.get(), this.spliterator)).get();
            if (!this.mustFindFirst) {
                if (o != null) {
                    shortCircuit(o);
                }
                return null;
            } else if (o != null) {
                l(o);
                return o;
            } else {
                return null;
            }
        }

        @Override // java8.util.stream.AbstractShortCircuitTask
        public O getEmptyResult() {
            return this.op.b;
        }

        public final void l(O o) {
            if (isLeftmostNode()) {
                shortCircuit(o);
            } else {
                cancelLaterNodes();
            }
        }

        @Override // java8.util.stream.AbstractTask, java8.util.concurrent.CountedCompleter
        public void onCompletion(CountedCompleter<?> countedCompleter) {
            if (this.mustFindFirst) {
                FindTask findTask = (FindTask) this.leftChild;
                FindTask findTask2 = null;
                while (true) {
                    if (findTask != findTask2) {
                        O localResult = findTask.getLocalResult();
                        if (localResult != null && this.op.c.a(localResult)) {
                            setLocalResult(localResult);
                            l(localResult);
                            break;
                        }
                        findTask2 = findTask;
                        findTask = (FindTask) this.rightChild;
                    } else {
                        break;
                    }
                }
            }
            super.onCompletion(countedCompleter);
        }

        @Override // java8.util.stream.AbstractTask
        public FindTask<P_IN, P_OUT, O> makeChild(s<P_IN> sVar) {
            return new FindTask<>(this, sVar);
        }

        public FindTask(FindTask<P_IN, P_OUT, O> findTask, s<P_IN> sVar) {
            super(findTask, sVar);
            this.mustFindFirst = findTask.mustFindFirst;
            this.op = findTask.op;
        }
    }

    /* loaded from: classes2.dex */
    public static final class a<T, O> implements b44<T, O> {
        public final int a;
        public final O b;
        public final fu2<O> c;
        public final ew3<c44<T, O>> d;

        public a(boolean z, StreamShape streamShape, O o, fu2<O> fu2Var, ew3<c44<T, O>> ew3Var) {
            this.a = (z ? 0 : StreamOpFlag.NOT_ORDERED) | StreamOpFlag.IS_SHORT_CIRCUIT;
            this.b = o;
            this.c = fu2Var;
            this.d = ew3Var;
        }

        /* JADX WARN: Multi-variable type inference failed */
        @Override // defpackage.b44
        public <S> O a(e<T> eVar, s<S> sVar) {
            O o = (O) ((c44) eVar.k(this.d.get(), sVar)).get();
            return o != null ? o : this.b;
        }

        @Override // defpackage.b44
        public <P_IN> O e(e<T> eVar, s<P_IN> sVar) {
            return new FindTask(this, StreamOpFlag.ORDERED.isKnown(eVar.i()), eVar, sVar).invoke();
        }

        @Override // defpackage.b44
        public int f() {
            return this.a;
        }
    }

    /* loaded from: classes2.dex */
    public static abstract class b<T, O> implements c44<T, O> {
        public boolean a;
        public T b;

        /* loaded from: classes2.dex */
        public static final class a extends b<Double, nn2> implements g, hq0 {
            @Override // defpackage.ew3
            /* renamed from: a */
            public nn2 get() {
                if (this.a) {
                    return nn2.c(((Double) this.b).doubleValue());
                }
                return null;
            }
        }

        /* renamed from: java8.util.stream.FindOps$b$b  reason: collision with other inner class name */
        /* loaded from: classes2.dex */
        public static final class C0186b extends b<Integer, on2> implements g, mr1 {
            @Override // defpackage.ew3
            /* renamed from: a */
            public on2 get() {
                if (this.a) {
                    return on2.c(((Integer) this.b).intValue());
                }
                return null;
            }
        }

        /* loaded from: classes2.dex */
        public static final class c extends b<Long, pn2> implements g, f22 {
            @Override // defpackage.ew3
            /* renamed from: a */
            public pn2 get() {
                if (this.a) {
                    return pn2.c(((Long) this.b).longValue());
                }
                return null;
            }
        }

        /* loaded from: classes2.dex */
        public static final class d<T> extends b<T, o<T>> {
            @Override // defpackage.ew3
            /* renamed from: a */
            public o<T> get() {
                if (this.a) {
                    return o.d(this.b);
                }
                return null;
            }
        }

        @Override // defpackage.m60
        public void accept(T t) {
            if (this.a) {
                return;
            }
            this.a = true;
            this.b = t;
        }

        @Override // java8.util.stream.g
        public void b(long j) {
        }

        @Override // java8.util.stream.g
        public boolean d() {
            return this.a;
        }

        @Override // java8.util.stream.g
        public void g() {
        }
    }

    static {
        fu2<o<Object>> b2 = n41.b();
        a = b2;
        fu2<on2> b3 = o41.b();
        b = b3;
        fu2<pn2> b4 = p41.b();
        c = b4;
        fu2<nn2> b5 = q41.b();
        d = b5;
        ew3<c44<Object, o<Object>>> a2 = r41.a();
        e = a2;
        ew3<c44<Integer, on2>> a3 = s41.a();
        f = a3;
        ew3<c44<Long, pn2>> a4 = t41.a();
        g = a4;
        ew3<c44<Double, nn2>> a5 = u41.a();
        h = a5;
        StreamShape streamShape = StreamShape.REFERENCE;
        i = new a(true, streamShape, o.a(), b2, a2);
        j = new a(false, streamShape, o.a(), b2, a2);
        StreamShape streamShape2 = StreamShape.INT_VALUE;
        new a(true, streamShape2, on2.a(), b3, a3);
        new a(false, streamShape2, on2.a(), b3, a3);
        StreamShape streamShape3 = StreamShape.LONG_VALUE;
        new a(true, streamShape3, pn2.a(), b4, a4);
        new a(false, streamShape3, pn2.a(), b4, a4);
        StreamShape streamShape4 = StreamShape.DOUBLE_VALUE;
        new a(true, streamShape4, nn2.a(), b5, a5);
        new a(false, streamShape4, nn2.a(), b5, a5);
    }

    public static <T> b44<T, o<T>> a(boolean z) {
        return z ? i : j;
    }
}
