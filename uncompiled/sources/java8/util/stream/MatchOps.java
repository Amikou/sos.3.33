package java8.util.stream;

import java8.util.s;

/* loaded from: classes2.dex */
public final class MatchOps {

    /* loaded from: classes2.dex */
    public enum MatchKind {
        ANY(true, true),
        ALL(false, false),
        NONE(true, false);
        
        private final boolean shortCircuitResult;
        private final boolean stopOnPredicateMatches;

        MatchKind(boolean z, boolean z2) {
            this.stopOnPredicateMatches = z;
            this.shortCircuitResult = z2;
        }
    }

    /* loaded from: classes2.dex */
    public static final class MatchTask<P_IN, P_OUT> extends AbstractShortCircuitTask<P_IN, P_OUT, Boolean, MatchTask<P_IN, P_OUT>> {
        private final c<P_OUT> op;

        public MatchTask(c<P_OUT> cVar, e<P_OUT> eVar, s<P_IN> sVar) {
            super(eVar, sVar);
            this.op = cVar;
        }

        @Override // java8.util.stream.AbstractTask
        public Boolean doLeaf() {
            boolean a = ((b) this.helper.k(this.op.b.get(), this.spliterator)).a();
            if (a == this.op.a.shortCircuitResult) {
                shortCircuit(Boolean.valueOf(a));
                return null;
            }
            return null;
        }

        /* JADX WARN: Can't rename method to resolve collision */
        @Override // java8.util.stream.AbstractShortCircuitTask
        public Boolean getEmptyResult() {
            return Boolean.valueOf(!this.op.a.shortCircuitResult);
        }

        @Override // java8.util.stream.AbstractTask
        public MatchTask<P_IN, P_OUT> makeChild(s<P_IN> sVar) {
            return new MatchTask<>(this, sVar);
        }

        public MatchTask(MatchTask<P_IN, P_OUT> matchTask, s<P_IN> sVar) {
            super(matchTask, sVar);
            this.op = matchTask.op;
        }
    }

    /* loaded from: classes2.dex */
    public class a extends b<T> {
        public final /* synthetic */ MatchKind c;
        public final /* synthetic */ fu2 d;

        /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
        public a(MatchKind matchKind, fu2 fu2Var) {
            super(matchKind);
            this.c = matchKind;
            this.d = fu2Var;
        }

        @Override // defpackage.m60
        public void accept(T t) {
            if (this.a || this.d.a(t) != this.c.stopOnPredicateMatches) {
                return;
            }
            this.a = true;
            this.b = this.c.shortCircuitResult;
        }
    }

    /* loaded from: classes2.dex */
    public static abstract class b<T> implements g<T> {
        public boolean a;
        public boolean b;

        public b(MatchKind matchKind) {
            this.b = !matchKind.shortCircuitResult;
        }

        public boolean a() {
            return this.b;
        }

        @Override // java8.util.stream.g
        public void b(long j) {
        }

        @Override // java8.util.stream.g
        public boolean d() {
            return this.a;
        }

        @Override // java8.util.stream.g
        public void g() {
        }
    }

    /* loaded from: classes2.dex */
    public static final class c<T> implements b44<T, Boolean> {
        public final MatchKind a;
        public final ew3<b<T>> b;

        public c(StreamShape streamShape, MatchKind matchKind, ew3<b<T>> ew3Var) {
            this.a = matchKind;
            this.b = ew3Var;
        }

        @Override // defpackage.b44
        /* renamed from: b */
        public <S> Boolean e(e<T> eVar, s<S> sVar) {
            return new MatchTask(this, eVar, sVar).invoke();
        }

        /* JADX WARN: Multi-variable type inference failed */
        @Override // defpackage.b44
        /* renamed from: c */
        public <S> Boolean a(e<T> eVar, s<S> sVar) {
            return Boolean.valueOf(((b) eVar.k(this.b.get(), sVar)).a());
        }

        @Override // defpackage.b44
        public int f() {
            return StreamOpFlag.IS_SHORT_CIRCUIT | StreamOpFlag.NOT_ORDERED;
        }
    }

    public static /* synthetic */ b a(MatchKind matchKind, fu2 fu2Var) {
        return new a(matchKind, fu2Var);
    }

    public static <T> b44<T, Boolean> b(fu2<? super T> fu2Var, MatchKind matchKind) {
        rl2.f(fu2Var);
        rl2.f(matchKind);
        return new c(StreamShape.REFERENCE, matchKind, g42.a(matchKind, fu2Var));
    }
}
