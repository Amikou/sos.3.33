package java8.util;

import java.util.Collection;
import java.util.Comparator;
import java.util.ConcurrentModificationException;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import sun.misc.Unsafe;

/* compiled from: HMSpliterators.java */
/* loaded from: classes2.dex */
public final class g {
    public static final Unsafe a;
    public static final long b;
    public static final long c;
    public static final long d;
    public static final long e;

    /* compiled from: HMSpliterators.java */
    /* loaded from: classes2.dex */
    public static final class a<K, V> extends b<K, V> implements s<Map.Entry<K, V>> {
        public a(HashMap<K, V> hashMap, int i, int i2, int i3, int i4) {
            super(hashMap, i, i2, i3, i4);
        }

        @Override // java8.util.s
        public void a(m60<? super Map.Entry<K, V>> m60Var) {
            int i;
            int i2;
            rl2.f(m60Var);
            HashMap<K, V> hashMap = this.a;
            Object[] t = b.t(hashMap);
            int i3 = this.d;
            if (i3 < 0) {
                int p = b.p(hashMap);
                this.f = p;
                int length = t == null ? 0 : t.length;
                this.d = length;
                int i4 = length;
                i = p;
                i3 = i4;
            } else {
                i = this.f;
            }
            if (t == null || t.length < i3 || (i2 = this.c) < 0) {
                return;
            }
            this.c = i3;
            if (i2 < i3 || this.b != null) {
                Object obj = this.b;
                this.b = null;
                while (true) {
                    if (obj == null) {
                        obj = t[i2];
                        i2++;
                    } else {
                        m60Var.accept((Map.Entry) obj);
                        obj = b.q(obj);
                    }
                    if (obj == null && i2 >= i3) {
                        break;
                    }
                }
                if (i != b.p(hashMap)) {
                    throw new ConcurrentModificationException();
                }
            }
        }

        @Override // java8.util.s
        public int b() {
            return ((this.d < 0 || this.e == this.a.size()) ? 64 : 0) | 1;
        }

        @Override // java8.util.s
        public boolean d(m60<? super Map.Entry<K, V>> m60Var) {
            rl2.f(m60Var);
            Object[] t = b.t(this.a);
            if (t == null) {
                return false;
            }
            int length = t.length;
            int o = o();
            if (length < o || this.c < 0) {
                return false;
            }
            while (true) {
                Object obj = this.b;
                if (obj == null && this.c >= o) {
                    return false;
                }
                if (obj == null) {
                    int i = this.c;
                    this.c = i + 1;
                    this.b = t[i];
                } else {
                    this.b = b.q(obj);
                    m60Var.accept((Map.Entry) obj);
                    if (this.f == b.p(this.a)) {
                        return true;
                    }
                    throw new ConcurrentModificationException();
                }
            }
        }

        @Override // java8.util.s
        public Comparator<? super Map.Entry<K, V>> f() {
            return t.h(null);
        }

        @Override // java8.util.s
        /* renamed from: v */
        public a<K, V> c() {
            int o = o();
            int i = this.c;
            int i2 = (o + i) >>> 1;
            if (i >= i2 || this.b != null) {
                return null;
            }
            HashMap<K, V> hashMap = this.a;
            this.c = i2;
            int i3 = this.e >>> 1;
            this.e = i3;
            return new a<>(hashMap, i, i2, i3, this.f);
        }
    }

    /* compiled from: HMSpliterators.java */
    /* loaded from: classes2.dex */
    public static abstract class b<K, V> {
        public static final Unsafe g;
        public static final long h;
        public static final long i;
        public static final long j;
        public static final long k;
        public static final long l;
        public final HashMap<K, V> a;
        public Object b;
        public int c;
        public int d;
        public int e;
        public int f;

        static {
            Unsafe unsafe = u.a;
            g = unsafe;
            try {
                h = unsafe.objectFieldOffset(HashMap.class.getDeclaredField("table"));
                i = unsafe.objectFieldOffset(HashMap.class.getDeclaredField("modCount"));
                Class<?> u = u();
                j = unsafe.objectFieldOffset(u.getDeclaredField("key"));
                k = unsafe.objectFieldOffset(u.getDeclaredField("value"));
                l = unsafe.objectFieldOffset(u.getDeclaredField("next"));
            } catch (Exception e) {
                throw new Error(e);
            }
        }

        public b(HashMap<K, V> hashMap, int i2, int i3, int i4, int i5) {
            this.a = hashMap;
            this.c = i2;
            this.d = i3;
            this.e = i4;
            this.f = i5;
        }

        public static int p(HashMap<?, ?> hashMap) {
            return g.getInt(hashMap, i);
        }

        public static Object q(Object obj) {
            return g.getObject(obj, l);
        }

        public static <K> K r(Object obj) {
            return (K) g.getObject(obj, j);
        }

        public static <T> T s(Object obj) {
            return (T) g.getObject(obj, k);
        }

        public static Object[] t(HashMap<?, ?> hashMap) {
            return (Object[]) g.getObject(hashMap, h);
        }

        public static Class<?> u() throws ClassNotFoundException {
            StringBuilder sb = new StringBuilder("java.util.HashMap$");
            sb.append((t.h || t.l) ? "Node" : "Entry");
            try {
                return Class.forName(sb.toString());
            } catch (ClassNotFoundException e) {
                if (t.h) {
                    return Class.forName("java.util.HashMap$HashMapEntry");
                }
                throw e;
            }
        }

        public final boolean h(int i2) {
            return t.k((s) this, i2);
        }

        public final long i() {
            return t.i((s) this);
        }

        public final long l() {
            o();
            return this.e;
        }

        public final int o() {
            int i2 = this.d;
            if (i2 < 0) {
                HashMap<K, V> hashMap = this.a;
                this.e = hashMap.size();
                this.f = p(hashMap);
                Object[] t = t(hashMap);
                i2 = t == null ? 0 : t.length;
                this.d = i2;
            }
            return i2;
        }
    }

    /* compiled from: HMSpliterators.java */
    /* loaded from: classes2.dex */
    public static final class c<K, V> extends b<K, V> implements s<K> {
        public c(HashMap<K, V> hashMap, int i, int i2, int i3, int i4) {
            super(hashMap, i, i2, i3, i4);
        }

        @Override // java8.util.s
        public void a(m60<? super K> m60Var) {
            int i;
            int i2;
            rl2.f(m60Var);
            HashMap<K, V> hashMap = this.a;
            Object[] t = b.t(hashMap);
            int i3 = this.d;
            if (i3 < 0) {
                int p = b.p(hashMap);
                this.f = p;
                int length = t == null ? 0 : t.length;
                this.d = length;
                int i4 = length;
                i = p;
                i3 = i4;
            } else {
                i = this.f;
            }
            if (t == null || t.length < i3 || (i2 = this.c) < 0) {
                return;
            }
            this.c = i3;
            if (i2 < i3 || this.b != null) {
                Object obj = this.b;
                this.b = null;
                while (true) {
                    if (obj == null) {
                        obj = t[i2];
                        i2++;
                    } else {
                        m60Var.accept((Object) b.r(obj));
                        obj = b.q(obj);
                    }
                    if (obj == null && i2 >= i3) {
                        break;
                    }
                }
                if (i != b.p(hashMap)) {
                    throw new ConcurrentModificationException();
                }
            }
        }

        @Override // java8.util.s
        public int b() {
            return ((this.d < 0 || this.e == this.a.size()) ? 64 : 0) | 1;
        }

        @Override // java8.util.s
        public boolean d(m60<? super K> m60Var) {
            rl2.f(m60Var);
            Object[] t = b.t(this.a);
            if (t == null) {
                return false;
            }
            int length = t.length;
            int o = o();
            if (length < o || this.c < 0) {
                return false;
            }
            while (true) {
                Object obj = this.b;
                if (obj == null && this.c >= o) {
                    return false;
                }
                if (obj == null) {
                    int i = this.c;
                    this.c = i + 1;
                    this.b = t[i];
                } else {
                    this.b = b.q(this.b);
                    m60Var.accept((Object) b.r(obj));
                    if (this.f == b.p(this.a)) {
                        return true;
                    }
                    throw new ConcurrentModificationException();
                }
            }
        }

        @Override // java8.util.s
        public Comparator<? super K> f() {
            return t.h(null);
        }

        @Override // java8.util.s
        /* renamed from: v */
        public c<K, V> c() {
            int o = o();
            int i = this.c;
            int i2 = (o + i) >>> 1;
            if (i >= i2 || this.b != null) {
                return null;
            }
            HashMap<K, V> hashMap = this.a;
            this.c = i2;
            int i3 = this.e >>> 1;
            this.e = i3;
            return new c<>(hashMap, i, i2, i3, this.f);
        }
    }

    /* compiled from: HMSpliterators.java */
    /* loaded from: classes2.dex */
    public static final class d<K, V> extends b<K, V> implements s<V> {
        public d(HashMap<K, V> hashMap, int i, int i2, int i3, int i4) {
            super(hashMap, i, i2, i3, i4);
        }

        @Override // java8.util.s
        public void a(m60<? super V> m60Var) {
            int i;
            int i2;
            rl2.f(m60Var);
            HashMap<K, V> hashMap = this.a;
            Object[] t = b.t(hashMap);
            int i3 = this.d;
            if (i3 < 0) {
                int p = b.p(hashMap);
                this.f = p;
                int length = t == null ? 0 : t.length;
                this.d = length;
                int i4 = length;
                i = p;
                i3 = i4;
            } else {
                i = this.f;
            }
            if (t == null || t.length < i3 || (i2 = this.c) < 0) {
                return;
            }
            this.c = i3;
            if (i2 < i3 || this.b != null) {
                Object obj = this.b;
                this.b = null;
                while (true) {
                    if (obj == null) {
                        obj = t[i2];
                        i2++;
                    } else {
                        m60Var.accept((Object) b.s(obj));
                        obj = b.q(obj);
                    }
                    if (obj == null && i2 >= i3) {
                        break;
                    }
                }
                if (i != b.p(hashMap)) {
                    throw new ConcurrentModificationException();
                }
            }
        }

        @Override // java8.util.s
        public int b() {
            return (this.d < 0 || this.e == this.a.size()) ? 64 : 0;
        }

        @Override // java8.util.s
        public boolean d(m60<? super V> m60Var) {
            rl2.f(m60Var);
            Object[] t = b.t(this.a);
            if (t == null) {
                return false;
            }
            int length = t.length;
            int o = o();
            if (length < o || this.c < 0) {
                return false;
            }
            while (true) {
                Object obj = this.b;
                if (obj == null && this.c >= o) {
                    return false;
                }
                if (obj == null) {
                    int i = this.c;
                    this.c = i + 1;
                    this.b = t[i];
                } else {
                    this.b = b.q(this.b);
                    m60Var.accept((Object) b.s(obj));
                    if (this.f == b.p(this.a)) {
                        return true;
                    }
                    throw new ConcurrentModificationException();
                }
            }
        }

        @Override // java8.util.s
        public Comparator<? super V> f() {
            return t.h(null);
        }

        @Override // java8.util.s
        /* renamed from: v */
        public d<K, V> c() {
            int o = o();
            int i = this.c;
            int i2 = (o + i) >>> 1;
            if (i >= i2 || this.b != null) {
                return null;
            }
            HashMap<K, V> hashMap = this.a;
            this.c = i2;
            int i3 = this.e >>> 1;
            this.e = i3;
            return new d<>(hashMap, i, i2, i3, this.f);
        }
    }

    static {
        Unsafe unsafe = u.a;
        a = unsafe;
        try {
            Class<?> cls = Class.forName("java.util.HashMap$Values");
            Class<?> cls2 = Class.forName("java.util.HashMap$KeySet");
            Class<?> cls3 = Class.forName("java.util.HashMap$EntrySet");
            b = unsafe.objectFieldOffset(cls.getDeclaredField("this$0"));
            c = unsafe.objectFieldOffset(cls2.getDeclaredField("this$0"));
            d = unsafe.objectFieldOffset(cls3.getDeclaredField("this$0"));
            e = unsafe.objectFieldOffset(HashSet.class.getDeclaredField("map"));
        } catch (Exception e2) {
            throw new Error(e2);
        }
    }

    public static <K, V> s<Map.Entry<K, V>> a(Set<Map.Entry<K, V>> set) {
        return new a(b(set), 0, -1, 0, 0);
    }

    public static <K, V> HashMap<K, V> b(Set<Map.Entry<K, V>> set) {
        return (HashMap) a.getObject(set, d);
    }

    public static <K, V> HashMap<K, V> c(HashSet<K> hashSet) {
        return (HashMap) a.getObject(hashSet, e);
    }

    public static <K, V> HashMap<K, V> d(Set<K> set) {
        return (HashMap) a.getObject(set, c);
    }

    public static <K, V> HashMap<K, V> e(Collection<V> collection) {
        return (HashMap) a.getObject(collection, b);
    }

    public static <E> s<E> f(HashSet<E> hashSet) {
        return new c(c(hashSet), 0, -1, 0, 0);
    }

    public static <K> s<K> g(Set<K> set) {
        return new c(d(set), 0, -1, 0, 0);
    }

    public static <V> s<V> h(Collection<V> collection) {
        return new d(e(collection), 0, -1, 0, 0);
    }
}
