package java8.util;

import androidx.recyclerview.widget.RecyclerView;
import java.util.AbstractList;
import java.util.Comparator;
import java.util.ConcurrentModificationException;
import java.util.LinkedList;
import sun.misc.Unsafe;

/* compiled from: LinkedListSpliterator.java */
/* loaded from: classes2.dex */
public final class l<T> implements s<T> {
    public static final boolean g;
    public static final boolean h;
    public static final Unsafe i;
    public static final long j;
    public static final long k;
    public static final long l;
    public static final long m;
    public static final long n;
    public final LinkedList<T> a;
    public final Object b;
    public Object c;
    public int d;
    public int e;
    public int f;

    static {
        boolean z = t.i;
        g = z;
        boolean z2 = t.k;
        h = z2;
        Unsafe unsafe = u.a;
        i = unsafe;
        try {
            k = unsafe.objectFieldOffset(AbstractList.class.getDeclaredField("modCount"));
            String str = z ? "voidLink" : z2 ? "header" : "first";
            String str2 = z ? "java.util.LinkedList$Link" : z2 ? "java.util.LinkedList$Entry" : "java.util.LinkedList$Node";
            String str3 = z ? "data" : z2 ? "element" : "item";
            Class<?> cls = Class.forName(str2);
            j = unsafe.objectFieldOffset(LinkedList.class.getDeclaredField("size"));
            l = unsafe.objectFieldOffset(LinkedList.class.getDeclaredField(str));
            m = unsafe.objectFieldOffset(cls.getDeclaredField(str3));
            n = unsafe.objectFieldOffset(cls.getDeclaredField("next"));
        } catch (Exception e) {
            throw new Error(e);
        }
    }

    public l(LinkedList<T> linkedList, int i2, int i3) {
        this.a = linkedList;
        this.d = i2;
        this.e = i3;
        this.b = (h || g) ? q(linkedList) : null;
    }

    public static Object q(LinkedList<?> linkedList) {
        if (linkedList == null) {
            return null;
        }
        return i.getObject(linkedList, l);
    }

    public static int r(LinkedList<?> linkedList) {
        return i.getInt(linkedList, k);
    }

    public static Object s(Object obj) {
        if (obj != null) {
            return i.getObject(obj, n);
        }
        throw new ConcurrentModificationException();
    }

    public static <E> E t(Object obj) {
        if (obj != null) {
            return (E) i.getObject(obj, m);
        }
        throw new ConcurrentModificationException();
    }

    public static int u(LinkedList<?> linkedList) {
        return i.getInt(linkedList, j);
    }

    public static <E> s<E> v(LinkedList<E> linkedList) {
        return new l(linkedList, -1, 0);
    }

    @Override // java8.util.s
    public void a(m60<? super T> m60Var) {
        rl2.f(m60Var);
        Object obj = this.b;
        int o = o();
        if (o > 0 && (r2 = this.c) != obj) {
            this.c = obj;
            this.d = 0;
            do {
                Object obj2 = s(obj2);
                m60Var.accept((Object) t(obj2));
                if (obj2 == obj) {
                    break;
                }
                o--;
            } while (o > 0);
        }
        if (this.e != r(this.a)) {
            throw new ConcurrentModificationException();
        }
    }

    @Override // java8.util.s
    public int b() {
        return 16464;
    }

    @Override // java8.util.s
    public s<T> c() {
        Object obj;
        int i2;
        Object obj2 = this.b;
        int o = o();
        if (o <= 1 || (obj = this.c) == obj2) {
            return null;
        }
        int i3 = this.f + RecyclerView.a0.FLAG_ADAPTER_FULLUPDATE;
        if (i3 > o) {
            i3 = o;
        }
        if (i3 > 33554432) {
            i3 = 33554432;
        }
        Object[] objArr = new Object[i3];
        int i4 = 0;
        while (true) {
            i2 = i4 + 1;
            objArr[i4] = t(obj);
            obj = s(obj);
            if (obj == obj2 || i2 >= i3) {
                break;
            }
            i4 = i2;
        }
        this.c = obj;
        this.f = i2;
        this.d = o - i2;
        return t.y(objArr, 0, i2, 16);
    }

    @Override // java8.util.s
    public boolean d(m60<? super T> m60Var) {
        Object obj;
        rl2.f(m60Var);
        Object obj2 = this.b;
        if (o() <= 0 || (obj = this.c) == obj2) {
            return false;
        }
        this.d--;
        this.c = s(obj);
        m60Var.accept((Object) t(obj));
        if (this.e == r(this.a)) {
            return true;
        }
        throw new ConcurrentModificationException();
    }

    @Override // java8.util.s
    public Comparator<? super T> f() {
        return t.h(this);
    }

    @Override // java8.util.s
    public boolean h(int i2) {
        return t.k(this, i2);
    }

    @Override // java8.util.s
    public long i() {
        return t.i(this);
    }

    @Override // java8.util.s
    public long l() {
        return o();
    }

    public final int o() {
        int i2 = this.d;
        if (i2 < 0) {
            LinkedList<T> linkedList = this.a;
            if (linkedList == null) {
                this.d = 0;
                return 0;
            }
            this.e = r(linkedList);
            this.c = p(linkedList);
            int u = u(linkedList);
            this.d = u;
            return u;
        }
        return i2;
    }

    public final Object p(LinkedList<?> linkedList) {
        if (!h && !g) {
            return i.getObject(linkedList, l);
        }
        return s(this.b);
    }
}
