package java8.util;

import java.util.NoSuchElementException;

/* compiled from: Optional.java */
/* loaded from: classes2.dex */
public final class o<T> {
    public static final o<?> b = new o<>();
    public final T a;

    public o() {
        this.a = null;
    }

    public static <T> o<T> a() {
        return (o<T>) b;
    }

    public static <T> o<T> d(T t) {
        return new o<>(t);
    }

    public static <T> o<T> e(T t) {
        return t == null ? a() : d(t);
    }

    public T b() {
        return f();
    }

    public boolean c() {
        return this.a != null;
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj instanceof o) {
            return rl2.b(this.a, ((o) obj).a);
        }
        return false;
    }

    public T f() {
        T t = this.a;
        if (t != null) {
            return t;
        }
        throw new NoSuchElementException("No value present");
    }

    public <X extends Throwable> T g(ew3<? extends X> ew3Var) throws Throwable {
        T t = this.a;
        if (t != null) {
            return t;
        }
        throw ew3Var.get();
    }

    public int hashCode() {
        return rl2.d(this.a);
    }

    public String toString() {
        T t = this.a;
        return t != null ? String.format("Optional[%s]", t) : "Optional.empty";
    }

    public o(T t) {
        this.a = (T) rl2.f(t);
    }
}
