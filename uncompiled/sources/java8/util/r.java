package java8.util;

import java.util.Set;
import java8.util.ImmutableCollections;

/* compiled from: Sets.java */
/* loaded from: classes2.dex */
public final class r {
    public static <E> Set<E> a(E e) {
        return new ImmutableCollections.Set12(e);
    }

    public static <E> Set<E> b(E... eArr) {
        int length = eArr.length;
        if (length != 0) {
            if (length != 1) {
                if (length != 2) {
                    return new ImmutableCollections.SetN(eArr);
                }
                return new ImmutableCollections.Set12(eArr[0], eArr[1]);
            }
            return new ImmutableCollections.Set12(eArr[0]);
        }
        return ImmutableCollections.d();
    }
}
