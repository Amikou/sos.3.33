package java8.util;

import androidx.recyclerview.widget.RecyclerView;
import build.IgnoreJava8API;
import java.lang.reflect.Method;
import java.security.AccessController;
import java.security.PrivilegedAction;
import java.util.AbstractList;
import java.util.ArrayDeque;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Comparator;
import java.util.Deque;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedHashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.PriorityQueue;
import java.util.Queue;
import java.util.RandomAccess;
import java.util.Set;
import java.util.SortedSet;
import java.util.Vector;
import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.CopyOnWriteArrayList;
import java.util.concurrent.CopyOnWriteArraySet;
import java.util.concurrent.DelayQueue;
import java.util.concurrent.LinkedBlockingDeque;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.PriorityBlockingQueue;
import java.util.concurrent.SynchronousQueue;
import java8.util.s;
import okhttp3.internal.http2.Http2;

/* compiled from: Spliterators.java */
/* loaded from: classes2.dex */
public final class t {
    public static final String a;
    public static final String b;
    public static final String c;
    public static final boolean d;
    public static final boolean e;
    public static final boolean f;
    public static final boolean g = p();
    public static final boolean h;
    public static final boolean i;
    public static final boolean j;
    public static final boolean k;
    public static final boolean l;
    public static final boolean m;
    public static final s<Object> n;
    public static final s.b o;
    public static final s.c p;
    public static final s.a q;

    /* compiled from: Spliterators.java */
    /* loaded from: classes2.dex */
    public static class a extends e<T> {
        public final /* synthetic */ Set f;

        /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
        public a(Collection collection, int i, Set set) {
            super(collection, i);
            this.f = set;
        }

        @Override // java8.util.t.e, java8.util.s
        public Comparator<? super T> f() {
            return ((SortedSet) this.f).comparator();
        }
    }

    /* compiled from: Spliterators.java */
    /* loaded from: classes2.dex */
    public static class b implements PrivilegedAction<Boolean> {
        public final /* synthetic */ boolean a;
        public final /* synthetic */ String b;

        public b(boolean z, String str) {
            this.a = z;
            this.b = str;
        }

        @Override // java.security.PrivilegedAction
        /* renamed from: a */
        public Boolean run() {
            boolean z = this.a;
            try {
                z = Boolean.parseBoolean(System.getProperty(this.b, Boolean.toString(z)).trim());
            } catch (IllegalArgumentException | NullPointerException unused) {
            }
            return Boolean.valueOf(z);
        }
    }

    /* compiled from: Spliterators.java */
    /* loaded from: classes2.dex */
    public static final class c<T> implements s<T> {
        public final Object[] a;
        public int b;
        public final int c;
        public final int d;

        public c(Object[] objArr, int i) {
            this(objArr, 0, objArr.length, i);
        }

        @Override // java8.util.s
        public void a(m60<? super T> m60Var) {
            int i;
            rl2.f(m60Var);
            Object[] objArr = this.a;
            int length = objArr.length;
            int i2 = this.c;
            if (length < i2 || (i = this.b) < 0) {
                return;
            }
            this.b = i2;
            if (i < i2) {
                do {
                    m60Var.accept(objArr[i]);
                    i++;
                } while (i < i2);
            }
        }

        @Override // java8.util.s
        public int b() {
            return this.d;
        }

        @Override // java8.util.s
        public s<T> c() {
            int i = this.b;
            int i2 = (this.c + i) >>> 1;
            if (i >= i2) {
                return null;
            }
            Object[] objArr = this.a;
            this.b = i2;
            return new c(objArr, i, i2, this.d);
        }

        @Override // java8.util.s
        public boolean d(m60<? super T> m60Var) {
            rl2.f(m60Var);
            int i = this.b;
            if (i < 0 || i >= this.c) {
                return false;
            }
            Object[] objArr = this.a;
            this.b = i + 1;
            m60Var.accept(objArr[i]);
            return true;
        }

        @Override // java8.util.s
        public Comparator<? super T> f() {
            if (h(4)) {
                return null;
            }
            throw new IllegalStateException();
        }

        @Override // java8.util.s
        public boolean h(int i) {
            return t.k(this, i);
        }

        @Override // java8.util.s
        public long i() {
            return t.i(this);
        }

        @Override // java8.util.s
        public long l() {
            return this.c - this.b;
        }

        public c(Object[] objArr, int i, int i2, int i3) {
            this.a = objArr;
            this.b = i;
            this.c = i2;
            this.d = i3 | 64 | Http2.INITIAL_MAX_FRAME_SIZE;
        }
    }

    /* compiled from: Spliterators.java */
    /* loaded from: classes2.dex */
    public static abstract class d<T, S extends s<T>, C> {

        /* compiled from: Spliterators.java */
        /* loaded from: classes2.dex */
        public static final class a extends d<Double, s.a, hq0> implements s.a {
            @Override // java8.util.s.a, java8.util.s
            public void a(m60<? super Double> m60Var) {
                f.a(this, m60Var);
            }

            @Override // java8.util.s
            public boolean d(m60<? super Double> m60Var) {
                return f.c(this, m60Var);
            }

            @Override // java8.util.s
            public Comparator<? super Double> f() {
                throw new IllegalStateException();
            }

            @Override // java8.util.s
            public boolean h(int i) {
                return t.k(this, i);
            }

            @Override // java8.util.s
            public long i() {
                return t.i(this);
            }

            @Override // java8.util.s.a
            public /* bridge */ /* synthetic */ void m(hq0 hq0Var) {
                super.o(hq0Var);
            }

            @Override // java8.util.s.a
            public /* bridge */ /* synthetic */ boolean n(hq0 hq0Var) {
                return super.p(hq0Var);
            }
        }

        /* compiled from: Spliterators.java */
        /* loaded from: classes2.dex */
        public static final class b extends d<Integer, s.b, mr1> implements s.b {
            @Override // java8.util.s.b, java8.util.s
            public void a(m60<? super Integer> m60Var) {
                g.a(this, m60Var);
            }

            @Override // java8.util.s
            public boolean d(m60<? super Integer> m60Var) {
                return g.c(this, m60Var);
            }

            @Override // java8.util.s.b
            public /* bridge */ /* synthetic */ boolean e(mr1 mr1Var) {
                return super.p(mr1Var);
            }

            @Override // java8.util.s
            public Comparator<? super Integer> f() {
                throw new IllegalStateException();
            }

            @Override // java8.util.s.b
            public /* bridge */ /* synthetic */ void g(mr1 mr1Var) {
                super.o(mr1Var);
            }

            @Override // java8.util.s
            public boolean h(int i) {
                return t.k(this, i);
            }

            @Override // java8.util.s
            public long i() {
                return t.i(this);
            }
        }

        /* compiled from: Spliterators.java */
        /* loaded from: classes2.dex */
        public static final class c extends d<Long, s.c, f22> implements s.c {
            @Override // java8.util.s.c, java8.util.s
            public void a(m60<? super Long> m60Var) {
                h.a(this, m60Var);
            }

            @Override // java8.util.s
            public boolean d(m60<? super Long> m60Var) {
                return h.c(this, m60Var);
            }

            @Override // java8.util.s
            public Comparator<? super Long> f() {
                throw new IllegalStateException();
            }

            @Override // java8.util.s
            public boolean h(int i) {
                return t.k(this, i);
            }

            @Override // java8.util.s
            public long i() {
                return t.i(this);
            }

            @Override // java8.util.s.c
            public /* bridge */ /* synthetic */ void j(f22 f22Var) {
                super.o(f22Var);
            }

            @Override // java8.util.s.c
            public /* bridge */ /* synthetic */ boolean k(f22 f22Var) {
                return super.p(f22Var);
            }
        }

        /* compiled from: Spliterators.java */
        /* renamed from: java8.util.t$d$d  reason: collision with other inner class name */
        /* loaded from: classes2.dex */
        public static final class C0193d<T> extends d<T, s<T>, m60<? super T>> implements s<T> {
            @Override // java8.util.s
            public /* bridge */ /* synthetic */ void a(m60 m60Var) {
                super.o(m60Var);
            }

            @Override // java8.util.s
            public /* bridge */ /* synthetic */ boolean d(m60 m60Var) {
                return super.p(m60Var);
            }

            @Override // java8.util.s
            public Comparator<? super T> f() {
                throw new IllegalStateException();
            }

            @Override // java8.util.s
            public boolean h(int i) {
                return t.k(this, i);
            }

            @Override // java8.util.s
            public long i() {
                return t.i(this);
            }
        }

        public int b() {
            return 16448;
        }

        public S c() {
            return null;
        }

        public long l() {
            return 0L;
        }

        public void o(C c2) {
            rl2.f(c2);
        }

        public boolean p(C c2) {
            rl2.f(c2);
            return false;
        }
    }

    /* compiled from: Spliterators.java */
    /* loaded from: classes2.dex */
    public static class e<T> implements s<T> {
        public final Collection<? extends T> a;
        public Iterator<? extends T> b = null;
        public final int c;
        public long d;
        public int e;

        public e(Collection<? extends T> collection, int i) {
            this.a = collection;
            this.c = (i & 4096) == 0 ? i | 64 | Http2.INITIAL_MAX_FRAME_SIZE : i;
        }

        @Override // java8.util.s
        public void a(m60<? super T> m60Var) {
            rl2.f(m60Var);
            Iterator<? extends T> it = this.b;
            if (it == null) {
                it = this.a.iterator();
                this.b = it;
                this.d = this.a.size();
            }
            java8.util.h.a(it, m60Var);
        }

        @Override // java8.util.s
        public int b() {
            return this.c;
        }

        @Override // java8.util.s
        public s<T> c() {
            long j;
            Iterator<? extends T> it = this.b;
            if (it == null) {
                it = this.a.iterator();
                this.b = it;
                j = this.a.size();
                this.d = j;
            } else {
                j = this.d;
            }
            if (j <= 1 || !it.hasNext()) {
                return null;
            }
            int i = this.e + RecyclerView.a0.FLAG_ADAPTER_FULLUPDATE;
            if (i > j) {
                i = (int) j;
            }
            if (i > 33554432) {
                i = 33554432;
            }
            Object[] objArr = new Object[i];
            int i2 = 0;
            do {
                objArr[i2] = it.next();
                i2++;
                if (i2 >= i) {
                    break;
                }
            } while (it.hasNext());
            this.e = i2;
            long j2 = this.d;
            if (j2 != Long.MAX_VALUE) {
                this.d = j2 - i2;
            }
            return new c(objArr, 0, i2, this.c);
        }

        @Override // java8.util.s
        public boolean d(m60<? super T> m60Var) {
            rl2.f(m60Var);
            if (this.b == null) {
                this.b = this.a.iterator();
                this.d = this.a.size();
            }
            if (this.b.hasNext()) {
                m60Var.accept((T) this.b.next());
                return true;
            }
            return false;
        }

        @Override // java8.util.s
        public Comparator<? super T> f() {
            if (h(4)) {
                return null;
            }
            throw new IllegalStateException();
        }

        @Override // java8.util.s
        public boolean h(int i) {
            return t.k(this, i);
        }

        @Override // java8.util.s
        public long i() {
            return t.i(this);
        }

        @Override // java8.util.s
        public long l() {
            if (this.b == null) {
                this.b = this.a.iterator();
                long size = this.a.size();
                this.d = size;
                return size;
            }
            return this.d;
        }
    }

    /* compiled from: Spliterators.java */
    /* loaded from: classes2.dex */
    public static final class f {
        public static void a(s.a aVar, m60<? super Double> m60Var) {
            if (m60Var instanceof hq0) {
                aVar.m((hq0) m60Var);
            } else {
                aVar.m(b(m60Var));
            }
        }

        public static hq0 b(m60<? super Double> m60Var) {
            m60Var.getClass();
            return vr3.a(m60Var);
        }

        public static boolean c(s.a aVar, m60<? super Double> m60Var) {
            if (m60Var instanceof hq0) {
                return aVar.n((hq0) m60Var);
            }
            return aVar.n(b(m60Var));
        }
    }

    /* compiled from: Spliterators.java */
    /* loaded from: classes2.dex */
    public static final class g {
        public static void a(s.b bVar, m60<? super Integer> m60Var) {
            if (m60Var instanceof mr1) {
                bVar.g((mr1) m60Var);
            } else {
                bVar.g(b(m60Var));
            }
        }

        public static mr1 b(m60<? super Integer> m60Var) {
            m60Var.getClass();
            return wr3.a(m60Var);
        }

        public static boolean c(s.b bVar, m60<? super Integer> m60Var) {
            if (m60Var instanceof mr1) {
                return bVar.e((mr1) m60Var);
            }
            return bVar.e(b(m60Var));
        }
    }

    /* compiled from: Spliterators.java */
    /* loaded from: classes2.dex */
    public static final class h {
        public static void a(s.c cVar, m60<? super Long> m60Var) {
            if (m60Var instanceof f22) {
                cVar.j((f22) m60Var);
            } else {
                cVar.j(b(m60Var));
            }
        }

        public static f22 b(m60<? super Long> m60Var) {
            m60Var.getClass();
            return xr3.a(m60Var);
        }

        public static boolean c(s.c cVar, m60<? super Long> m60Var) {
            if (m60Var instanceof f22) {
                return cVar.k((f22) m60Var);
            }
            return cVar.k(b(m60Var));
        }
    }

    static {
        String str = t.class.getName() + ".assume.oracle.collections.impl";
        a = str;
        String str2 = t.class.getName() + ".jre.delegation.enabled";
        b = str2;
        String str3 = t.class.getName() + ".randomaccess.spliterator.enabled";
        c = str3;
        boolean z = true;
        d = g(str, true);
        e = g(str2, true);
        f = g(str3, true);
        boolean l2 = l();
        h = l2;
        i = l2 && !m("android.opengl.GLES32$DebugProc");
        j = l2 && m("java.time.DateTimeException");
        if (l2 || !o()) {
            z = false;
        }
        k = z;
        l = q();
        m = m("java.lang.StackWalker$Option");
        n = new d.C0193d();
        o = new d.b();
        p = new d.c();
        q = new d.a();
    }

    public static void a(int i2, int i3, int i4) {
        if (i3 <= i4) {
            if (i3 < 0) {
                throw new ArrayIndexOutOfBoundsException(i3);
            }
            if (i4 > i2) {
                throw new ArrayIndexOutOfBoundsException(i4);
            }
            return;
        }
        throw new ArrayIndexOutOfBoundsException("origin(" + i3 + ") > fence(" + i4 + ")");
    }

    @IgnoreJava8API
    public static <T> s<T> b(Collection<? extends T> collection) {
        return new java8.util.f(collection.spliterator());
    }

    public static s.a c() {
        return q;
    }

    public static s.b d() {
        return o;
    }

    public static s.c e() {
        return p;
    }

    public static <T> s<T> f() {
        return (s<T>) n;
    }

    public static boolean g(String str, boolean z) {
        return ((Boolean) AccessController.doPrivileged(new b(z, str))).booleanValue();
    }

    public static <T> Comparator<? super T> h(s<T> sVar) {
        throw new IllegalStateException();
    }

    public static <T> long i(s<T> sVar) {
        if ((sVar.b() & 64) == 0) {
            return -1L;
        }
        return sVar.l();
    }

    @IgnoreJava8API
    public static boolean j(Collection<?> collection) {
        if (!h || i || j || !collection.getClass().getName().startsWith("java.util.HashMap$")) {
            return false;
        }
        return collection.spliterator().hasCharacteristics(16);
    }

    public static <T> boolean k(s<T> sVar, int i2) {
        return (sVar.b() & i2) == i2;
    }

    public static boolean l() {
        return m("android.util.DisplayMetrics") || g;
    }

    public static boolean m(String str) {
        Class<?> cls;
        try {
            cls = Class.forName(str, false, t.class.getClassLoader());
        } catch (Throwable unused) {
            cls = null;
        }
        return cls != null;
    }

    public static boolean n(String str) {
        return str.startsWith("java.util.Collections$", 0) && str.endsWith("RandomAccessList");
    }

    public static boolean o() {
        return r("java.class.version", 51.0d);
    }

    public static boolean p() {
        return m("org.robovm.rt.bro.Bro");
    }

    public static boolean q() {
        if (l() || !r("java.class.version", 52.0d)) {
            String[] strArr = {"java.util.function.Consumer", "java.util.Spliterator"};
            Method method = null;
            Class<?> cls = null;
            for (int i2 = 0; i2 < 2; i2++) {
                try {
                    cls = Class.forName(strArr[i2]);
                } catch (Exception unused) {
                    return false;
                }
            }
            if (cls != null) {
                try {
                    method = Collection.class.getDeclaredMethod("spliterator", new Class[0]);
                } catch (Exception unused2) {
                    return false;
                }
            }
            return method != null;
        }
        return false;
    }

    public static boolean r(String str, double d2) {
        try {
            String property = System.getProperty(str);
            if (property != null) {
                return Double.parseDouble(property) < d2;
            }
            return false;
        } catch (Exception unused) {
            return false;
        }
    }

    public static <T> s<T> s(List<? extends T> list, String str) {
        if (d || h) {
            if (list instanceof ArrayList) {
                return java8.util.b.s((ArrayList) list);
            }
            if ("java.util.Arrays$ArrayList".equals(str)) {
                return java8.util.c.b(list);
            }
            if (list instanceof CopyOnWriteArrayList) {
                return java8.util.d.c((CopyOnWriteArrayList) list);
            }
            if (list instanceof LinkedList) {
                return l.v((LinkedList) list);
            }
            if (list instanceof Vector) {
                return v.s((Vector) list);
            }
        }
        if (f && (list instanceof RandomAccess)) {
            if (!(list instanceof AbstractList) && n(str)) {
                return w(list, 16);
            }
            if (!(list instanceof CopyOnWriteArrayList)) {
                return q.r(list);
            }
        }
        return w(list, 16);
    }

    public static <T> s<T> t(Queue<? extends T> queue) {
        if (queue instanceof ArrayBlockingQueue) {
            return w(queue, 4368);
        }
        if (d || h) {
            if (queue instanceof LinkedBlockingQueue) {
                return k.w((LinkedBlockingQueue) queue);
            }
            if (queue instanceof ArrayDeque) {
                return java8.util.a.s((ArrayDeque) queue);
            }
            if (queue instanceof LinkedBlockingDeque) {
                return j.t((LinkedBlockingDeque) queue);
            }
            if (queue instanceof PriorityBlockingQueue) {
                return jo2.p((PriorityBlockingQueue) queue);
            }
            if (queue instanceof PriorityQueue) {
                return p.s((PriorityQueue) queue);
            }
        }
        if ((queue instanceof Deque) || !(!queue.getClass().getName().startsWith("java.util") || (queue instanceof PriorityBlockingQueue) || (queue instanceof PriorityQueue) || (queue instanceof DelayQueue) || (queue instanceof SynchronousQueue))) {
            return w(queue, queue instanceof ArrayDeque ? 272 : 16);
        }
        return w(queue, 0);
    }

    public static <T> s<T> u(Set<? extends T> set, String str) {
        boolean z = i;
        if (!z && d) {
            if ("java.util.HashMap$EntrySet".equals(str)) {
                return java8.util.g.a(set);
            }
            if ("java.util.HashMap$KeySet".equals(str)) {
                return java8.util.g.g(set);
            }
        }
        if (set instanceof LinkedHashSet) {
            return w(set, 17);
        }
        if (!z && d && (set instanceof HashSet)) {
            return java8.util.g.f((HashSet) set);
        }
        if (set instanceof SortedSet) {
            return new a(set, 21, set);
        }
        if ((d || h) && (set instanceof CopyOnWriteArraySet)) {
            return java8.util.e.b((CopyOnWriteArraySet) set);
        }
        return w(set, 1);
    }

    public static <T> s<T> v(Collection<? extends T> collection) {
        rl2.f(collection);
        if (l && ((e || m) && !j(collection))) {
            return b(collection);
        }
        String name = collection.getClass().getName();
        if (collection instanceof List) {
            return s((List) collection, name);
        }
        if (collection instanceof Set) {
            return u((Set) collection, name);
        }
        if (collection instanceof Queue) {
            return t((Queue) collection);
        }
        if (!i && d && "java.util.HashMap$Values".equals(name)) {
            return java8.util.g.h(collection);
        }
        return w(collection, 0);
    }

    public static <T> s<T> w(Collection<? extends T> collection, int i2) {
        return new e((Collection) rl2.f(collection), i2);
    }

    public static <T> s<T> x(Object[] objArr, int i2) {
        return new c((Object[]) rl2.f(objArr), i2);
    }

    public static <T> s<T> y(Object[] objArr, int i2, int i3, int i4) {
        a(((Object[]) rl2.f(objArr)).length, i2, i3);
        return new c(objArr, i2, i3, i4);
    }
}
