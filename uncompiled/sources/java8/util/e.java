package java8.util;

import java.util.concurrent.CopyOnWriteArrayList;
import java.util.concurrent.CopyOnWriteArraySet;
import sun.misc.Unsafe;

/* compiled from: COWArraySetSpliterator.java */
/* loaded from: classes2.dex */
public final class e {
    public static final Unsafe a;
    public static final long b;

    static {
        Unsafe unsafe = u.a;
        a = unsafe;
        try {
            b = unsafe.objectFieldOffset(CopyOnWriteArraySet.class.getDeclaredField("al"));
        } catch (Exception e) {
            throw new Error(e);
        }
    }

    public static <T> CopyOnWriteArrayList<T> a(CopyOnWriteArraySet<T> copyOnWriteArraySet) {
        return (CopyOnWriteArrayList) a.getObject(copyOnWriteArraySet, b);
    }

    public static <T> s<T> b(CopyOnWriteArraySet<T> copyOnWriteArraySet) {
        return t.x(d.b(a(copyOnWriteArraySet)), 1025);
    }
}
