package java8.util;

import java.util.concurrent.CopyOnWriteArrayList;
import sun.misc.Unsafe;

/* compiled from: COWArrayListSpliterator.java */
/* loaded from: classes2.dex */
public final class d {
    public static final Unsafe a = u.a;
    public static final long b;

    static {
        b = a(t.h ? "elements" : "array", true);
    }

    public static long a(String str, boolean z) {
        try {
            return a.objectFieldOffset(CopyOnWriteArrayList.class.getDeclaredField(str));
        } catch (Exception e) {
            if (z && (e instanceof NoSuchFieldException) && t.h && !t.i) {
                return a("array", false);
            }
            throw new Error(e);
        }
    }

    public static <T> Object[] b(CopyOnWriteArrayList<T> copyOnWriteArrayList) {
        return (Object[]) a.getObject(copyOnWriteArrayList, b);
    }

    public static <T> s<T> c(CopyOnWriteArrayList<T> copyOnWriteArrayList) {
        return t.x(b(copyOnWriteArrayList), 1040);
    }
}
