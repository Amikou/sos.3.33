package java8.util;

import java.util.Comparator;
import java.util.ConcurrentModificationException;
import java.util.PriorityQueue;
import sun.misc.Unsafe;

/* compiled from: PQueueSpliterator.java */
/* loaded from: classes2.dex */
public final class p<E> implements s<E> {
    public static final boolean e;
    public static final Unsafe f;
    public static final long g;
    public static final long h;
    public static final long i;
    public final PriorityQueue<E> a;
    public int b;
    public int c;
    public int d;

    static {
        boolean z = t.i;
        e = z;
        Unsafe unsafe = u.a;
        f = unsafe;
        try {
            g = unsafe.objectFieldOffset(PriorityQueue.class.getDeclaredField("size"));
            if (!z) {
                h = unsafe.objectFieldOffset(PriorityQueue.class.getDeclaredField("modCount"));
            } else {
                h = 0L;
            }
            i = unsafe.objectFieldOffset(PriorityQueue.class.getDeclaredField(z ? "elements" : "queue"));
        } catch (Exception e2) {
            throw new Error(e2);
        }
    }

    public p(PriorityQueue<E> priorityQueue, int i2, int i3, int i4) {
        this.a = priorityQueue;
        this.b = i2;
        this.c = i3;
        this.d = i4;
    }

    public static <T> int p(PriorityQueue<T> priorityQueue) {
        if (e) {
            return 0;
        }
        return f.getInt(priorityQueue, h);
    }

    public static <T> Object[] q(PriorityQueue<T> priorityQueue) {
        return (Object[]) f.getObject(priorityQueue, i);
    }

    public static <T> int r(PriorityQueue<T> priorityQueue) {
        return f.getInt(priorityQueue, g);
    }

    public static <T> s<T> s(PriorityQueue<T> priorityQueue) {
        return new p(priorityQueue, 0, -1, 0);
    }

    @Override // java8.util.s
    public void a(m60<? super E> m60Var) {
        rl2.f(m60Var);
        PriorityQueue<E> priorityQueue = this.a;
        if (this.c < 0) {
            this.c = r(priorityQueue);
            this.d = p(priorityQueue);
        }
        Object[] q = q(priorityQueue);
        int i2 = this.c;
        this.b = i2;
        for (int i3 = this.b; i3 < i2; i3++) {
            Object obj = q[i3];
            if (obj == null) {
                break;
            }
            m60Var.accept(obj);
        }
        if (p(priorityQueue) != this.d) {
            throw new ConcurrentModificationException();
        }
    }

    @Override // java8.util.s
    public int b() {
        return 16704;
    }

    @Override // java8.util.s
    public boolean d(m60<? super E> m60Var) {
        rl2.f(m60Var);
        PriorityQueue<E> priorityQueue = this.a;
        if (this.c < 0) {
            this.c = r(priorityQueue);
            this.d = p(priorityQueue);
        }
        int i2 = this.b;
        if (i2 < this.c) {
            this.b = i2 + 1;
            Object obj = q(priorityQueue)[i2];
            if (obj != null && p(priorityQueue) == this.d) {
                m60Var.accept(obj);
                return true;
            }
            throw new ConcurrentModificationException();
        }
        return false;
    }

    @Override // java8.util.s
    public Comparator<? super E> f() {
        return t.h(this);
    }

    @Override // java8.util.s
    public boolean h(int i2) {
        return t.k(this, i2);
    }

    @Override // java8.util.s
    public long i() {
        return t.i(this);
    }

    @Override // java8.util.s
    public long l() {
        return o() - this.b;
    }

    public final int o() {
        int i2 = this.c;
        if (i2 < 0) {
            this.d = p(this.a);
            int r = r(this.a);
            this.c = r;
            return r;
        }
        return i2;
    }

    @Override // java8.util.s
    /* renamed from: t */
    public p<E> c() {
        int o = o();
        int i2 = this.b;
        int i3 = (o + i2) >>> 1;
        if (i2 >= i3) {
            return null;
        }
        PriorityQueue<E> priorityQueue = this.a;
        this.b = i3;
        return new p<>(priorityQueue, i2, i3, this.d);
    }
}
