package java8.util;

import java.util.AbstractList;
import java.util.Comparator;
import java.util.ConcurrentModificationException;
import java.util.Vector;
import sun.misc.Unsafe;

/* compiled from: VectorSpliterator.java */
/* loaded from: classes2.dex */
public final class v<E> implements s<E> {
    public static final Unsafe f;
    public static final long g;
    public static final long h;
    public static final long i;
    public final Vector<E> a;
    public Object[] b;
    public int c;
    public int d;
    public int e;

    static {
        Unsafe unsafe = u.a;
        f = unsafe;
        try {
            h = unsafe.objectFieldOffset(AbstractList.class.getDeclaredField("modCount"));
            g = unsafe.objectFieldOffset(Vector.class.getDeclaredField("elementCount"));
            i = unsafe.objectFieldOffset(Vector.class.getDeclaredField("elementData"));
        } catch (Exception e) {
            throw new Error(e);
        }
    }

    public v(Vector<E> vector, Object[] objArr, int i2, int i3, int i4) {
        this.a = vector;
        this.b = objArr;
        this.c = i2;
        this.d = i3;
        this.e = i4;
    }

    public static <T> Object[] o(Vector<T> vector) {
        return (Object[]) f.getObject(vector, i);
    }

    public static <T> int q(Vector<T> vector) {
        return f.getInt(vector, h);
    }

    public static <T> int r(Vector<T> vector) {
        return f.getInt(vector, g);
    }

    public static <T> s<T> s(Vector<T> vector) {
        return new v(vector, null, 0, -1, 0);
    }

    @Override // java8.util.s
    public void a(m60<? super E> m60Var) {
        rl2.f(m60Var);
        int p = p();
        Object[] objArr = this.b;
        this.c = p;
        for (int i2 = this.c; i2 < p; i2++) {
            m60Var.accept(objArr[i2]);
        }
        if (q(this.a) != this.e) {
            throw new ConcurrentModificationException();
        }
    }

    @Override // java8.util.s
    public int b() {
        return 16464;
    }

    @Override // java8.util.s
    public s<E> c() {
        int p = p();
        int i2 = this.c;
        int i3 = (p + i2) >>> 1;
        if (i2 >= i3) {
            return null;
        }
        Vector<E> vector = this.a;
        Object[] objArr = this.b;
        this.c = i3;
        return new v(vector, objArr, i2, i3, this.e);
    }

    @Override // java8.util.s
    public boolean d(m60<? super E> m60Var) {
        rl2.f(m60Var);
        int p = p();
        int i2 = this.c;
        if (p > i2) {
            this.c = i2 + 1;
            m60Var.accept(this.b[i2]);
            if (this.e == q(this.a)) {
                return true;
            }
            throw new ConcurrentModificationException();
        }
        return false;
    }

    @Override // java8.util.s
    public Comparator<? super E> f() {
        return t.h(this);
    }

    @Override // java8.util.s
    public boolean h(int i2) {
        return t.k(this, i2);
    }

    @Override // java8.util.s
    public long i() {
        return t.i(this);
    }

    @Override // java8.util.s
    public long l() {
        return p() - this.c;
    }

    public final int p() {
        int i2 = this.d;
        if (i2 < 0) {
            synchronized (this.a) {
                this.b = o(this.a);
                this.e = q(this.a);
                i2 = r(this.a);
                this.d = i2;
            }
        }
        return i2;
    }
}
