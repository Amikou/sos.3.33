package dagger.android;

import android.app.Application;

/* loaded from: classes2.dex */
public abstract class DaggerApplication extends Application implements xj1 {
    public volatile up0<Object> a;

    @Override // defpackage.xj1
    public wc<Object> a() {
        c();
        return this.a;
    }

    public abstract wc<? extends DaggerApplication> b();

    public final void c() {
        synchronized (this) {
            b().a(this);
            throw new IllegalStateException("The AndroidInjector returned from applicationInjector() did not inject the DaggerApplication");
        }
    }

    @Override // android.app.Application
    public void onCreate() {
        super.onCreate();
        c();
    }
}
