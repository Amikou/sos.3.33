package dagger.android;

import android.content.ContentProvider;

/* loaded from: classes2.dex */
public abstract class DaggerContentProvider extends ContentProvider {
    @Override // android.content.ContentProvider
    public boolean onCreate() {
        vc.f(this);
        return true;
    }
}
