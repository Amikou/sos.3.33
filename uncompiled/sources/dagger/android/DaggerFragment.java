package dagger.android;

import android.app.Fragment;
import android.content.Context;

@Deprecated
/* loaded from: classes2.dex */
public abstract class DaggerFragment extends Fragment implements xj1 {
    public up0<Object> a;

    @Override // defpackage.xj1
    public wc<Object> a() {
        return this.a;
    }

    @Override // android.app.Fragment
    public void onAttach(Context context) {
        vc.c(this);
        super.onAttach(context);
    }
}
