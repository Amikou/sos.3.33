package dagger.android;

import android.app.Activity;
import android.os.Bundle;

/* loaded from: classes2.dex */
public abstract class DaggerActivity extends Activity implements xj1 {
    public up0<Object> a;

    @Override // defpackage.xj1
    public wc<Object> a() {
        return this.a;
    }

    @Override // android.app.Activity
    public void onCreate(Bundle bundle) {
        vc.b(this);
        super.onCreate(bundle);
    }
}
