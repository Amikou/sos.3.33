package net.safemoon.androidwallet.dialogs;

import kotlin.jvm.internal.Lambda;

/* compiled from: AnchorPopUpMyTokens.kt */
/* loaded from: classes2.dex */
public final class AnchorPopUpMyTokens$show$1 extends Lambda implements tc1<q9, te4> {
    public final /* synthetic */ tc1<q9, te4> $onItemClick;
    public final /* synthetic */ AnchorPopUpMyTokens this$0;

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    /* JADX WARN: Multi-variable type inference failed */
    public AnchorPopUpMyTokens$show$1(tc1<? super q9, te4> tc1Var, AnchorPopUpMyTokens anchorPopUpMyTokens) {
        super(1);
        this.$onItemClick = tc1Var;
        this.this$0 = anchorPopUpMyTokens;
    }

    @Override // defpackage.tc1
    public /* bridge */ /* synthetic */ te4 invoke(q9 q9Var) {
        invoke2(q9Var);
        return te4.a;
    }

    /* renamed from: invoke  reason: avoid collision after fix types in other method */
    public final void invoke2(q9 q9Var) {
        fs1.f(q9Var, "it");
        this.$onItemClick.invoke(q9Var);
        this.this$0.d();
    }
}
