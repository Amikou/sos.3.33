package net.safemoon.androidwallet.dialogs;

import android.os.Bundle;
import kotlin.jvm.internal.Lambda;

/* compiled from: CMCListCheckable.kt */
/* loaded from: classes2.dex */
public final class CMCListCheckable$excludeAppTokens$2 extends Lambda implements rc1<Boolean> {
    public final /* synthetic */ CMCListCheckable this$0;

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public CMCListCheckable$excludeAppTokens$2(CMCListCheckable cMCListCheckable) {
        super(0);
        this.this$0 = cMCListCheckable;
    }

    /* JADX WARN: Can't rename method to resolve collision */
    @Override // defpackage.rc1
    public final Boolean invoke() {
        Bundle arguments = this.this$0.getArguments();
        if (arguments == null) {
            return null;
        }
        return Boolean.valueOf(arguments.getBoolean("ARG_EXCLUDE_APP_TOKENS", false));
    }
}
