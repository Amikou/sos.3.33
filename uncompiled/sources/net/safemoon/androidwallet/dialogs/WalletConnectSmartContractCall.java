package net.safemoon.androidwallet.dialogs;

import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import androidx.fragment.app.FragmentManager;
import com.fasterxml.jackson.databind.deser.std.ThrowableDeserializer;
import com.google.android.material.textview.MaterialTextView;
import com.trustwallet.walletconnect.models.WCPeerMeta;
import com.trustwallet.walletconnect.models.ethereum.WCEthereumTransaction;
import net.safemoon.androidwallet.R;
import net.safemoon.androidwallet.dialogs.WalletConnectSmartContractCall;
import net.safemoon.androidwallet.dialogs.common.BaseBottomSheetDialogFragment;
import net.safemoon.androidwallet.model.walletConnect.RoomConnectedInfo;
import net.safemoon.androidwallet.model.walletConnect.RoomExtensionKt;
import net.safemoon.androidwallet.model.wallets.Wallet;
import net.safemoon.androidwallet.provider.AskAuthorizeProvider;

/* compiled from: WalletConnectSmartContractCall.kt */
/* loaded from: classes2.dex */
public final class WalletConnectSmartContractCall extends BaseBottomSheetDialogFragment {
    public static final a C0 = new a(null);
    public ub1 A0;
    public final sy1 B0 = zy1.a(new WalletConnectSmartContractCall$userTokenListDao$2(this));
    public b w0;
    public RoomConnectedInfo x0;
    public WCEthereumTransaction y0;
    public Wallet z0;

    /* compiled from: WalletConnectSmartContractCall.kt */
    /* loaded from: classes2.dex */
    public static final class a {
        public a() {
        }

        public /* synthetic */ a(qi0 qi0Var) {
            this();
        }

        public final WalletConnectSmartContractCall a() {
            WalletConnectSmartContractCall walletConnectSmartContractCall = new WalletConnectSmartContractCall();
            Bundle bundle = new Bundle();
            te4 te4Var = te4.a;
            walletConnectSmartContractCall.setArguments(bundle);
            return walletConnectSmartContractCall;
        }
    }

    /* compiled from: WalletConnectSmartContractCall.kt */
    /* loaded from: classes2.dex */
    public interface b {
        void a();

        void b();
    }

    public static final void P(WalletConnectSmartContractCall walletConnectSmartContractCall, View view) {
        fs1.f(walletConnectSmartContractCall, "this$0");
        walletConnectSmartContractCall.h();
    }

    public static final void Q(WalletConnectSmartContractCall walletConnectSmartContractCall, ub1 ub1Var, View view) {
        fs1.f(walletConnectSmartContractCall, "this$0");
        fs1.f(ub1Var, "$this_run");
        Context requireContext = walletConnectSmartContractCall.requireContext();
        fs1.e(requireContext, "requireContext()");
        new AskAuthorizeProvider(requireContext, walletConnectSmartContractCall.A()).a(new WalletConnectSmartContractCall$onViewCreated$2$1$1(ub1Var, walletConnectSmartContractCall), WalletConnectSmartContractCall$onViewCreated$2$1$2.INSTANCE);
    }

    public final eg4 M() {
        return (eg4) this.B0.getValue();
    }

    /* JADX WARN: Removed duplicated region for block: B:10:0x0024  */
    /* JADX WARN: Removed duplicated region for block: B:14:0x0032  */
    /*
        Code decompiled incorrectly, please refer to instructions dump.
        To view partially-correct code enable 'Show inconsistent code' option in preferences
    */
    public final java.lang.Object N(long r6, defpackage.q70<? super net.safemoon.androidwallet.model.wallets.Wallet> r8) {
        /*
            r5 = this;
            boolean r0 = r8 instanceof net.safemoon.androidwallet.dialogs.WalletConnectSmartContractCall$getWallet$1
            if (r0 == 0) goto L13
            r0 = r8
            net.safemoon.androidwallet.dialogs.WalletConnectSmartContractCall$getWallet$1 r0 = (net.safemoon.androidwallet.dialogs.WalletConnectSmartContractCall$getWallet$1) r0
            int r1 = r0.label
            r2 = -2147483648(0xffffffff80000000, float:-0.0)
            r3 = r1 & r2
            if (r3 == 0) goto L13
            int r1 = r1 - r2
            r0.label = r1
            goto L18
        L13:
            net.safemoon.androidwallet.dialogs.WalletConnectSmartContractCall$getWallet$1 r0 = new net.safemoon.androidwallet.dialogs.WalletConnectSmartContractCall$getWallet$1
            r0.<init>(r5, r8)
        L18:
            java.lang.Object r8 = r0.result
            java.lang.Object r1 = defpackage.gs1.d()
            int r2 = r0.label
            r3 = 0
            r4 = 1
            if (r2 == 0) goto L32
            if (r2 != r4) goto L2a
            defpackage.o83.b(r8)
            goto L56
        L2a:
            java.lang.IllegalStateException r6 = new java.lang.IllegalStateException
            java.lang.String r7 = "call to 'resume' before 'invoke' with coroutine"
            r6.<init>(r7)
            throw r6
        L32:
            defpackage.o83.b(r8)
            androidx.fragment.app.FragmentActivity r8 = r5.getActivity()
            if (r8 != 0) goto L3c
            goto L59
        L3c:
            android.app.Application r8 = r8.getApplication()
            if (r8 != 0) goto L43
            goto L59
        L43:
            net.safemoon.androidwallet.database.mainRoom.MainRoomDatabase$f r2 = net.safemoon.androidwallet.database.mainRoom.MainRoomDatabase.n
            net.safemoon.androidwallet.database.mainRoom.MainRoomDatabase r8 = r2.b(r8)
            mm4 r8 = r8.P()
            r0.label = r4
            java.lang.Object r8 = r8.h(r6, r0)
            if (r8 != r1) goto L56
            return r1
        L56:
            r3 = r8
            net.safemoon.androidwallet.model.wallets.Wallet r3 = (net.safemoon.androidwallet.model.wallets.Wallet) r3
        L59:
            return r3
        */
        throw new UnsupportedOperationException("Method not decompiled: net.safemoon.androidwallet.dialogs.WalletConnectSmartContractCall.N(long, q70):java.lang.Object");
    }

    /* JADX WARN: Can't wrap try/catch for region: R(8:1|(2:3|(6:5|6|7|(1:(1:(1:(1:(1:(3:14|15|16)(2:18|19))(8:20|21|22|23|24|(1:26)|15|16))(4:27|28|29|(1:31)(6:32|23|24|(0)|15|16)))(4:33|34|35|(1:37)(3:38|29|(0)(0))))(2:39|40))(4:45|(3:47|(1:49)(1:55)|(2:51|(1:53)(1:54)))|15|16)|41|(1:43)(3:44|35|(0)(0))))|57|6|7|(0)(0)|41|(0)(0)) */
    /* JADX WARN: Removed duplicated region for block: B:10:0x002c  */
    /* JADX WARN: Removed duplicated region for block: B:32:0x0091  */
    /* JADX WARN: Removed duplicated region for block: B:46:0x00f3 A[RETURN] */
    /* JADX WARN: Removed duplicated region for block: B:47:0x00f4  */
    /* JADX WARN: Removed duplicated region for block: B:50:0x010c A[RETURN] */
    /* JADX WARN: Removed duplicated region for block: B:51:0x010d  */
    /* JADX WARN: Removed duplicated region for block: B:54:0x012e A[RETURN] */
    /* JADX WARN: Removed duplicated region for block: B:55:0x012f  */
    /* JADX WARN: Removed duplicated region for block: B:58:0x0153 A[RETURN] */
    /*
        Code decompiled incorrectly, please refer to instructions dump.
        To view partially-correct code enable 'Show inconsistent code' option in preferences
    */
    public final java.lang.Object O(defpackage.q70<? super defpackage.te4> r20) {
        /*
            Method dump skipped, instructions count: 343
            To view this dump change 'Code comments level' option to 'DEBUG'
        */
        throw new UnsupportedOperationException("Method not decompiled: net.safemoon.androidwallet.dialogs.WalletConnectSmartContractCall.O(q70):java.lang.Object");
    }

    public final void R(RoomConnectedInfo roomConnectedInfo, long j, WCEthereumTransaction wCEthereumTransaction, Wallet wallet2, b bVar) {
        fs1.f(roomConnectedInfo, "rc");
        fs1.f(wCEthereumTransaction, ThrowableDeserializer.PROP_NAME_MESSAGE);
        fs1.f(wallet2, "wallet");
        fs1.f(bVar, "iWalletConnectSmartContract");
        this.x0 = roomConnectedInfo;
        this.y0 = wCEthereumTransaction;
        this.z0 = wallet2;
        this.w0 = bVar;
    }

    public final void S(FragmentManager fragmentManager) {
        fs1.f(fragmentManager, "manager");
        super.u(fragmentManager, WalletConnectSmartContractCall.class.getCanonicalName());
    }

    @Override // defpackage.sn0, androidx.fragment.app.Fragment
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
    }

    @Override // androidx.fragment.app.Fragment
    public View onCreateView(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle) {
        fs1.f(layoutInflater, "inflater");
        return layoutInflater.inflate(R.layout.fragment_wallet_connect_smart_contract_call, viewGroup, false);
    }

    @Override // androidx.fragment.app.Fragment
    public void onDestroy() {
        super.onDestroy();
        b bVar = this.w0;
        if (bVar == null) {
            return;
        }
        bVar.a();
    }

    @Override // androidx.fragment.app.Fragment
    public void onViewCreated(View view, Bundle bundle) {
        String peerMeta;
        WCPeerMeta peerMeta2;
        fs1.f(view, "view");
        super.onViewCreated(view, bundle);
        ub1 a2 = ub1.a(view);
        this.A0 = a2;
        if (a2 != null) {
            a2.b.c.setText(getString(R.string.smart_contract_screen_title));
            a2.b.a.setOnClickListener(new View.OnClickListener() { // from class: km4
                @Override // android.view.View.OnClickListener
                public final void onClick(View view2) {
                    WalletConnectSmartContractCall.P(WalletConnectSmartContractCall.this, view2);
                }
            });
        }
        final ub1 ub1Var = this.A0;
        if (ub1Var != null) {
            ub1Var.a.setOnClickListener(new View.OnClickListener() { // from class: lm4
                @Override // android.view.View.OnClickListener
                public final void onClick(View view2) {
                    WalletConnectSmartContractCall.Q(WalletConnectSmartContractCall.this, ub1Var, view2);
                }
            });
            Wallet wallet2 = this.z0;
            if (wallet2 != null) {
                MaterialTextView materialTextView = ub1Var.i;
                materialTextView.setText(wallet2.displayName() + "\n(" + e30.d(wallet2.getAddress()) + ')');
            }
            RoomConnectedInfo roomConnectedInfo = this.x0;
            if (roomConnectedInfo != null && (peerMeta = roomConnectedInfo.getPeerMeta()) != null && (peerMeta2 = RoomExtensionKt.toPeerMeta(peerMeta)) != null) {
                ub1Var.f.setText(peerMeta2.getUrl());
            }
        }
        as.b(qg1.a, null, null, new WalletConnectSmartContractCall$onViewCreated$3(this, null), 3, null);
    }
}
