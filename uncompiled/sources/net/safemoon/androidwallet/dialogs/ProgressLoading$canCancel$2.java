package net.safemoon.androidwallet.dialogs;

import android.os.Bundle;
import kotlin.jvm.internal.Lambda;

/* compiled from: ProgressLoading.kt */
/* loaded from: classes2.dex */
public final class ProgressLoading$canCancel$2 extends Lambda implements rc1<Boolean> {
    public final /* synthetic */ ProgressLoading this$0;

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public ProgressLoading$canCancel$2(ProgressLoading progressLoading) {
        super(0);
        this.this$0 = progressLoading;
    }

    /* JADX WARN: Can't rename method to resolve collision */
    @Override // defpackage.rc1
    public final Boolean invoke() {
        Bundle arguments = this.this$0.getArguments();
        return Boolean.valueOf(arguments != null ? arguments.getBoolean("CAN_CANCEL", false) : false);
    }
}
