package net.safemoon.androidwallet.dialogs;

import kotlin.jvm.internal.Lambda;

/* compiled from: ConfirmSwapTransactionFragment.kt */
/* loaded from: classes2.dex */
public final class ConfirmSwapTransactionFragment$binding$2 extends Lambda implements rc1<w91> {
    public final /* synthetic */ ConfirmSwapTransactionFragment this$0;

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public ConfirmSwapTransactionFragment$binding$2(ConfirmSwapTransactionFragment confirmSwapTransactionFragment) {
        super(0);
        this.this$0 = confirmSwapTransactionFragment;
    }

    @Override // defpackage.rc1
    public final w91 invoke() {
        return w91.a(this.this$0.requireView());
    }
}
