package net.safemoon.androidwallet.dialogs;

import android.app.Dialog;
import android.content.Context;
import kotlin.jvm.internal.Lambda;

/* compiled from: CryptoPriceAlertFragment.kt */
/* loaded from: classes2.dex */
public final class CryptoPriceAlertFragment$alertDialog$2 extends Lambda implements rc1<dc0> {
    public final /* synthetic */ CryptoPriceAlertFragment this$0;

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public CryptoPriceAlertFragment$alertDialog$2(CryptoPriceAlertFragment cryptoPriceAlertFragment) {
        super(0);
        this.this$0 = cryptoPriceAlertFragment;
    }

    /* JADX WARN: Can't rename method to resolve collision */
    @Override // defpackage.rc1
    public final dc0 invoke() {
        Context requireContext = this.this$0.requireContext();
        fs1.e(requireContext, "requireContext()");
        return dc0.c(new dc0(requireContext, fb0.c(), new AnonymousClass1(this.this$0)), false, false, 3, null);
    }

    /* compiled from: CryptoPriceAlertFragment.kt */
    /* renamed from: net.safemoon.androidwallet.dialogs.CryptoPriceAlertFragment$alertDialog$2$1  reason: invalid class name */
    /* loaded from: classes2.dex */
    public static final class AnonymousClass1 extends Lambda implements hd1<gt, Dialog, te4> {
        public final /* synthetic */ CryptoPriceAlertFragment this$0;

        /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
        public AnonymousClass1(CryptoPriceAlertFragment cryptoPriceAlertFragment) {
            super(2);
            this.this$0 = cryptoPriceAlertFragment;
        }

        /* renamed from: invoke  reason: avoid collision after fix types in other method */
        public final void invoke2(gt gtVar, Dialog dialog) {
            fs1.f(gtVar, "ccButton");
            fs1.f(dialog, "dialog");
            if (gtVar.a() == 103) {
                super/*sn0*/.h();
            }
            dialog.dismiss();
        }

        @Override // defpackage.hd1
        public /* bridge */ /* synthetic */ te4 invoke(gt gtVar, Dialog dialog) {
            invoke2(gtVar, dialog);
            return te4.a;
        }
    }
}
