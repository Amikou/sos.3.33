package net.safemoon.androidwallet.dialogs;

import kotlin.jvm.internal.Lambda;
import net.safemoon.androidwallet.model.fiat.gson.Fiat;
import net.safemoon.androidwallet.model.fiat.room.RoomFiat;
import net.safemoon.androidwallet.views.CurrencyConverterLayout;
import net.safemoon.androidwallet.views.editText.autoSize.AutofitEdittext;

/* compiled from: AnchorCurrencyConverter.kt */
/* loaded from: classes2.dex */
public final class AnchorCurrencyConverter$2$8 extends Lambda implements tc1<RoomFiat, Boolean> {
    public final /* synthetic */ AutofitEdittext $convertFromEdt;
    public final /* synthetic */ AutofitEdittext $convertToEdt;
    public final /* synthetic */ dn0 $this_with;
    public final /* synthetic */ AnchorCurrencyConverter this$0;

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public AnchorCurrencyConverter$2$8(AnchorCurrencyConverter anchorCurrencyConverter, AutofitEdittext autofitEdittext, dn0 dn0Var, AutofitEdittext autofitEdittext2) {
        super(1);
        this.this$0 = anchorCurrencyConverter;
        this.$convertToEdt = autofitEdittext;
        this.$this_with = dn0Var;
        this.$convertFromEdt = autofitEdittext2;
    }

    @Override // defpackage.tc1
    public final Boolean invoke(RoomFiat roomFiat) {
        if (roomFiat != null) {
            AnchorCurrencyConverter anchorCurrencyConverter = this.this$0;
            AutofitEdittext autofitEdittext = this.$convertToEdt;
            dn0 dn0Var = this.$this_with;
            AutofitEdittext autofitEdittext2 = this.$convertFromEdt;
            Fiat fiat = new Fiat(roomFiat.getSymbol(), roomFiat.getName(), roomFiat.getRate());
            if (!fs1.b(fiat.getSymbol(), anchorCurrencyConverter.e.getSymbol())) {
                anchorCurrencyConverter.f = new Fiat(roomFiat.getSymbol(), roomFiat.getName(), roomFiat.getRate());
                if (autofitEdittext.hasFocus()) {
                    CurrencyConverterLayout currencyConverterLayout = dn0Var.c;
                    fs1.e(currencyConverterLayout, "convertTo");
                    CurrencyConverterLayout currencyConverterLayout2 = dn0Var.b;
                    fs1.e(currencyConverterLayout2, "convertFrom");
                    anchorCurrencyConverter.r(currencyConverterLayout, currencyConverterLayout2, autofitEdittext.getText(), false);
                } else {
                    CurrencyConverterLayout currencyConverterLayout3 = dn0Var.b;
                    fs1.e(currencyConverterLayout3, "convertFrom");
                    CurrencyConverterLayout currencyConverterLayout4 = dn0Var.c;
                    fs1.e(currencyConverterLayout4, "convertTo");
                    anchorCurrencyConverter.r(currencyConverterLayout3, currencyConverterLayout4, autofitEdittext2.getText(), true);
                }
                dn0Var.b.m(new RoomFiat(fiat));
                return Boolean.TRUE;
            }
        }
        return Boolean.FALSE;
    }
}
