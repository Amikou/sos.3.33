package net.safemoon.androidwallet.dialogs;

import androidx.fragment.app.FragmentActivity;
import kotlin.jvm.internal.Lambda;

/* compiled from: BestMaxFragment.kt */
/* loaded from: classes2.dex */
public final class BestMaxFragment$swapViewModel$2 extends Lambda implements rc1<hj4> {
    public final /* synthetic */ BestMaxFragment this$0;

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public BestMaxFragment$swapViewModel$2(BestMaxFragment bestMaxFragment) {
        super(0);
        this.this$0 = bestMaxFragment;
    }

    /* JADX WARN: Can't rename method to resolve collision */
    @Override // defpackage.rc1
    public final hj4 invoke() {
        FragmentActivity requireActivity = this.this$0.requireActivity();
        fs1.e(requireActivity, "requireActivity()");
        return requireActivity;
    }
}
