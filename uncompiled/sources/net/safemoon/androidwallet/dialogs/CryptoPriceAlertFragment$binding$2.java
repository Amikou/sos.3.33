package net.safemoon.androidwallet.dialogs;

import kotlin.jvm.internal.Lambda;

/* compiled from: CryptoPriceAlertFragment.kt */
/* loaded from: classes2.dex */
public final class CryptoPriceAlertFragment$binding$2 extends Lambda implements rc1<z91> {
    public final /* synthetic */ CryptoPriceAlertFragment this$0;

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public CryptoPriceAlertFragment$binding$2(CryptoPriceAlertFragment cryptoPriceAlertFragment) {
        super(0);
        this.this$0 = cryptoPriceAlertFragment;
    }

    @Override // defpackage.rc1
    public final z91 invoke() {
        return z91.a(this.this$0.requireView());
    }
}
