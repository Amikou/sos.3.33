package net.safemoon.androidwallet.dialogs;

import android.text.Editable;
import com.github.mikephil.charting.utils.Utils;
import java.math.BigDecimal;
import java.math.RoundingMode;
import kotlin.jvm.internal.Lambda;
import net.safemoon.androidwallet.model.priceAlert.PriceAlertToken;
import net.safemoon.androidwallet.viewmodels.CryptoPriceAlertViewModel;

/* compiled from: CryptoPriceAlertFragment.kt */
/* loaded from: classes2.dex */
public final class CryptoPriceAlertFragment$onViewCreated$1$1$5$3 extends Lambda implements tc1<Editable, te4> {
    public final /* synthetic */ xp1 $this_with;
    public final /* synthetic */ CryptoPriceAlertFragment this$0;

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public CryptoPriceAlertFragment$onViewCreated$1$1$5$3(CryptoPriceAlertFragment cryptoPriceAlertFragment, xp1 xp1Var) {
        super(1);
        this.this$0 = cryptoPriceAlertFragment;
        this.$this_with = xp1Var;
    }

    @Override // defpackage.tc1
    public /* bridge */ /* synthetic */ te4 invoke(Editable editable) {
        invoke2(editable);
        return te4.a;
    }

    /* renamed from: invoke  reason: avoid collision after fix types in other method */
    public final void invoke2(Editable editable) {
        CryptoPriceAlertViewModel N;
        String obj;
        BigDecimal L;
        Number scale;
        CryptoPriceAlertViewModel N2;
        String obj2;
        this.this$0.w0 = true;
        N = this.this$0.N();
        PriceAlertToken value = N.u().getValue();
        if (value == null) {
            return;
        }
        xp1 xp1Var = this.$this_with;
        CryptoPriceAlertFragment cryptoPriceAlertFragment = this.this$0;
        String str = "";
        if (editable != null && (obj2 = editable.toString()) != null) {
            str = obj2;
        }
        value.setDecreasePercentString(str);
        if (editable == null || (obj = editable.toString()) == null || (L = e30.L(obj)) == null || (scale = L.setScale(1, RoundingMode.HALF_DOWN)) == null) {
            scale = Double.valueOf((double) Utils.DOUBLE_EPSILON);
        }
        float floatValue = scale.floatValue();
        if (!(floatValue == xp1Var.e.getValue())) {
            xp1Var.e.setValue(floatValue);
        }
        N2 = cryptoPriceAlertFragment.N();
        CryptoPriceAlertViewModel.A(N2, value, null, 2, null);
    }
}
