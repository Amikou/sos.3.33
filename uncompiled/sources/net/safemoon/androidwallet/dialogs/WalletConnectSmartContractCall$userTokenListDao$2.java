package net.safemoon.androidwallet.dialogs;

import android.content.Context;
import kotlin.jvm.internal.Lambda;
import net.safemoon.androidwallet.database.room.ApplicationRoomDatabase;

/* compiled from: WalletConnectSmartContractCall.kt */
/* loaded from: classes2.dex */
public final class WalletConnectSmartContractCall$userTokenListDao$2 extends Lambda implements rc1<eg4> {
    public final /* synthetic */ WalletConnectSmartContractCall this$0;

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public WalletConnectSmartContractCall$userTokenListDao$2(WalletConnectSmartContractCall walletConnectSmartContractCall) {
        super(0);
        this.this$0 = walletConnectSmartContractCall;
    }

    /* JADX WARN: Can't rename method to resolve collision */
    @Override // defpackage.rc1
    public final eg4 invoke() {
        ApplicationRoomDatabase.k kVar = ApplicationRoomDatabase.n;
        Context requireContext = this.this$0.requireContext();
        fs1.e(requireContext, "requireContext()");
        return ApplicationRoomDatabase.k.c(kVar, requireContext, null, 2, null).Z();
    }
}
