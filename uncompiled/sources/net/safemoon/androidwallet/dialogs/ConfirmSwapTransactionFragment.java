package net.safemoon.androidwallet.dialogs;

import android.app.Dialog;
import android.content.Context;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.PopupWindow;
import android.widget.TextView;
import androidx.fragment.app.FragmentActivity;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentViewModelLazyKt;
import com.google.android.material.textfield.TextInputEditText;
import com.google.android.material.textview.MaterialTextView;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.concurrent.TimeUnit;
import net.safemoon.androidwallet.R;
import net.safemoon.androidwallet.dialogs.ConfirmSwapTransactionFragment;
import net.safemoon.androidwallet.model.swap.Swap;
import net.safemoon.androidwallet.provider.AskAuthorizeProvider;
import net.safemoon.androidwallet.viewmodels.SwapViewModel;
import net.safemoon.androidwallet.views.slidetoact.SlideToActView;

/* compiled from: ConfirmSwapTransactionFragment.kt */
/* loaded from: classes2.dex */
public final class ConfirmSwapTransactionFragment extends sn0 {
    public static final a x0 = new a(null);
    public final sy1 u0 = FragmentViewModelLazyKt.a(this, d53.b(SwapViewModel.class), new ConfirmSwapTransactionFragment$special$$inlined$viewModels$default$1(new ConfirmSwapTransactionFragment$swapViewModel$2(this)), null);
    public final sy1 v0 = zy1.a(new ConfirmSwapTransactionFragment$binding$2(this));
    public final sy1 w0 = zy1.a(new ConfirmSwapTransactionFragment$ccWrapperWidth$2(this));

    /* compiled from: ConfirmSwapTransactionFragment.kt */
    /* loaded from: classes2.dex */
    public static final class a {
        public a() {
        }

        public /* synthetic */ a(qi0 qi0Var) {
            this();
        }

        public final ConfirmSwapTransactionFragment a() {
            return new ConfirmSwapTransactionFragment();
        }
    }

    public static final void G(w91 w91Var, SwapViewModel.d dVar) {
        fs1.f(w91Var, "$this_run");
        if (dVar != null) {
            BigDecimal r = e30.r(dVar.d(), 18);
            w91Var.j.setText(fs1.l(r == null ? null : e30.i(r.doubleValue()), " BNB"));
        }
    }

    public static final void K(ConfirmSwapTransactionFragment confirmSwapTransactionFragment, View view) {
        fs1.f(confirmSwapTransactionFragment, "this$0");
        confirmSwapTransactionFragment.h();
    }

    public static final void L(w91 w91Var, ConfirmSwapTransactionFragment confirmSwapTransactionFragment, Long l) {
        fs1.f(w91Var, "$this_run");
        fs1.f(confirmSwapTransactionFragment, "this$0");
        if (l == null) {
            return;
        }
        w91Var.k.setText(confirmSwapTransactionFragment.getString(R.string.cst_recalculate_in, Long.valueOf(TimeUnit.MILLISECONDS.toSeconds(l.longValue()))));
    }

    public static final void M(w91 w91Var, ConfirmSwapTransactionFragment confirmSwapTransactionFragment, Boolean bool) {
        fs1.f(w91Var, "$this_run");
        fs1.f(confirmSwapTransactionFragment, "this$0");
        w91Var.b.setEnabled(fs1.b(bool, Boolean.FALSE));
        confirmSwapTransactionFragment.F();
    }

    public static final void P(ConfirmSwapTransactionFragment confirmSwapTransactionFragment, View view) {
        fs1.f(confirmSwapTransactionFragment, "this$0");
        fs1.e(view, "it");
        String string = confirmSwapTransactionFragment.getResources().getString(R.string.minimum_received_description);
        fs1.e(string, "resources.getString(R.st…mum_received_description)");
        confirmSwapTransactionFragment.S(view, string);
    }

    public static final void Q(ConfirmSwapTransactionFragment confirmSwapTransactionFragment, View view) {
        fs1.f(confirmSwapTransactionFragment, "this$0");
        fs1.e(view, "it");
        String string = confirmSwapTransactionFragment.getResources().getString(R.string.price_impact_description);
        fs1.e(string, "resources.getString(R.st…price_impact_description)");
        confirmSwapTransactionFragment.S(view, string);
    }

    public static final void R(ConfirmSwapTransactionFragment confirmSwapTransactionFragment, View view) {
        fs1.f(confirmSwapTransactionFragment, "this$0");
        fs1.e(view, "it");
        String string = confirmSwapTransactionFragment.getResources().getString(R.string.swap_fee_description);
        fs1.e(string, "resources.getString(R.string.swap_fee_description)");
        confirmSwapTransactionFragment.S(view, string);
    }

    public static final boolean T(PopupWindow popupWindow, View view, MotionEvent motionEvent) {
        fs1.f(popupWindow, "$popupWindow");
        popupWindow.dismiss();
        return true;
    }

    public final void F() {
        Swap value;
        BigDecimal f;
        SwapViewModel.a value2;
        final w91 H = H();
        SwapViewModel J = J();
        l84 I0 = J.I0();
        if (I0 != null) {
            ex3 R = J.R();
            BigDecimal q = R == null ? null : R.q(I0);
            if (q != null) {
                H.i.setText(q.doubleValue() < 0.01d ? "<0.01%" : fs1.l(e30.m(q.doubleValue()), "%"));
            }
        }
        l84 I02 = J.I0();
        if (I02 != null && (value2 = J.F0().getValue()) != null) {
            if (value2.b()) {
                Swap value3 = J.b0().getValue();
                if (value3 != null) {
                    H.g.setText(R.string.cst_min_received);
                    MaterialTextView materialTextView = H.h;
                    StringBuilder sb = new StringBuilder();
                    BigInteger E = J.E(I02.b());
                    Integer num = value3.decimals;
                    fs1.e(num, "swap.decimals");
                    sb.append(e30.p(e30.r(E, num.intValue()).doubleValue(), 0, null, false, 6, null));
                    sb.append(' ');
                    sb.append((Object) value3.symbol);
                    materialTextView.setText(sb.toString());
                }
            } else {
                Swap value4 = J.A0().getValue();
                if (value4 != null) {
                    H.g.setText(R.string.cst_max_sold);
                    MaterialTextView materialTextView2 = H.h;
                    StringBuilder sb2 = new StringBuilder();
                    BigInteger D = J.D(I02.a());
                    Integer num2 = value4.decimals;
                    fs1.e(num2, "swap.decimals");
                    sb2.append(e30.p(e30.r(D, num2.intValue()).doubleValue(), 0, null, false, 6, null));
                    sb2.append(' ');
                    sb2.append((Object) value4.symbol);
                    materialTextView2.setText(sb2.toString());
                }
            }
        }
        l84 I03 = J.I0();
        if (I03 != null && (value = J.A0().getValue()) != null && !J().P0()) {
            ex3 R2 = J.R();
            if (R2 == null) {
                f = null;
            } else {
                BigInteger a2 = I03.a();
                Integer num3 = value.decimals;
                fs1.e(num3, "swap.decimals");
                BigDecimal r = e30.r(a2, num3.intValue());
                fs1.e(r, "it.amountIn.fromWEI(swap.decimals)");
                f = R2.f(r, I03);
            }
            TextInputEditText textInputEditText = H.j;
            StringBuilder sb3 = new StringBuilder();
            sb3.append((Object) (f != null ? e30.p(f.doubleValue(), 2, null, false, 6, null) : null));
            sb3.append(' ');
            sb3.append((Object) value.symbol);
            textInputEditText.setText(sb3.toString());
        }
        J().K0().observe(getViewLifecycleOwner(), new tl2() { // from class: k50
            @Override // defpackage.tl2
            public final void onChanged(Object obj) {
                ConfirmSwapTransactionFragment.G(w91.this, (SwapViewModel.d) obj);
            }
        });
    }

    public final w91 H() {
        return (w91) this.v0.getValue();
    }

    public final int I() {
        return ((Number) this.w0.getValue()).intValue();
    }

    public final SwapViewModel J() {
        return (SwapViewModel) this.u0.getValue();
    }

    public final void N(FragmentManager fragmentManager) {
        fs1.f(fragmentManager, "manager");
        super.u(fragmentManager, ConfirmSwapTransactionFragment.class.getCanonicalName());
    }

    public final void O(View view) {
        w91 H = H();
        (H == null ? null : H.d).setOnClickListener(new View.OnClickListener() { // from class: o50
            @Override // android.view.View.OnClickListener
            public final void onClick(View view2) {
                ConfirmSwapTransactionFragment.P(ConfirmSwapTransactionFragment.this, view2);
            }
        });
        w91 H2 = H();
        (H2 != null ? H2.e : null).setOnClickListener(new View.OnClickListener() { // from class: q50
            @Override // android.view.View.OnClickListener
            public final void onClick(View view2) {
                ConfirmSwapTransactionFragment.Q(ConfirmSwapTransactionFragment.this, view2);
            }
        });
        H().f.setOnClickListener(new View.OnClickListener() { // from class: p50
            @Override // android.view.View.OnClickListener
            public final void onClick(View view2) {
                ConfirmSwapTransactionFragment.R(ConfirmSwapTransactionFragment.this, view2);
            }
        });
    }

    public final void S(View view, String str) {
        LayoutInflater layoutInflater;
        FragmentActivity activity = getActivity();
        View inflate = (activity == null || (layoutInflater = activity.getLayoutInflater()) == null) ? null : layoutInflater.inflate(R.layout.tooltip_view, (ViewGroup) null);
        int I = (int) (I() * 0.6d);
        TextView textView = inflate != null ? (TextView) inflate.findViewById(R.id.tooltipText) : null;
        if (textView != null) {
            textView.setText(str);
        }
        final PopupWindow popupWindow = new PopupWindow(inflate, I, -2, true);
        popupWindow.showAsDropDown(view, -((int) (I * 0.2d)), 0);
        if (inflate == null) {
            return;
        }
        inflate.setOnTouchListener(new View.OnTouchListener() { // from class: r50
            @Override // android.view.View.OnTouchListener
            public final boolean onTouch(View view2, MotionEvent motionEvent) {
                boolean T;
                T = ConfirmSwapTransactionFragment.T(popupWindow, view2, motionEvent);
                return T;
            }
        });
    }

    @Override // defpackage.sn0, androidx.fragment.app.Fragment
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
    }

    @Override // androidx.fragment.app.Fragment
    public View onCreateView(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle) {
        fs1.f(layoutInflater, "inflater");
        return layoutInflater.inflate(R.layout.fragment_confirm_swap_transaction, viewGroup, false);
    }

    @Override // defpackage.sn0, androidx.fragment.app.Fragment
    public void onStart() {
        Window window;
        super.onStart();
        Dialog k = k();
        if (k == null || (window = k.getWindow()) == null) {
            return;
        }
        window.setBackgroundDrawable(new ColorDrawable(0));
        window.setLayout(-1, -2);
    }

    @Override // androidx.fragment.app.Fragment
    public void onViewCreated(View view, Bundle bundle) {
        fs1.f(view, "view");
        super.onViewCreated(view, bundle);
        final w91 H = H();
        H.c.setOnClickListener(new View.OnClickListener() { // from class: n50
            @Override // android.view.View.OnClickListener
            public final void onClick(View view2) {
                ConfirmSwapTransactionFragment.K(ConfirmSwapTransactionFragment.this, view2);
            }
        });
        J().V().observe(getViewLifecycleOwner(), new tl2() { // from class: m50
            @Override // defpackage.tl2
            public final void onChanged(Object obj) {
                ConfirmSwapTransactionFragment.L(w91.this, this, (Long) obj);
            }
        });
        J().T().observe(getViewLifecycleOwner(), new tl2() { // from class: l50
            @Override // defpackage.tl2
            public final void onChanged(Object obj) {
                ConfirmSwapTransactionFragment.M(w91.this, this, (Boolean) obj);
            }
        });
        H.b.setOnSlideCompleteListener(new SlideToActView.b() { // from class: net.safemoon.androidwallet.dialogs.ConfirmSwapTransactionFragment$onViewCreated$1$4
            @Override // net.safemoon.androidwallet.views.slidetoact.SlideToActView.b
            public void a(SlideToActView slideToActView) {
                fs1.f(slideToActView, "view");
                gm1 gm1Var = ConfirmSwapTransactionFragment.this.requireActivity() instanceof gm1 ? (gm1) ConfirmSwapTransactionFragment.this.requireActivity() : null;
                Context requireContext = ConfirmSwapTransactionFragment.this.requireContext();
                fs1.e(requireContext, "requireContext()");
                new AskAuthorizeProvider(requireContext, gm1Var).a(new ConfirmSwapTransactionFragment$onViewCreated$1$4$onSlideComplete$1(ConfirmSwapTransactionFragment.this), ConfirmSwapTransactionFragment$onViewCreated$1$4$onSlideComplete$2.INSTANCE);
            }
        });
        O(view);
    }
}
