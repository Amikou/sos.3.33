package net.safemoon.androidwallet.dialogs;

import android.content.ClipboardManager;
import java.util.Objects;
import kotlin.jvm.internal.Lambda;

/* compiled from: G2FAVerfication.kt */
/* loaded from: classes2.dex */
public final class G2FAVerfication$manager$2 extends Lambda implements rc1<ClipboardManager> {
    public final /* synthetic */ G2FAVerfication this$0;

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public G2FAVerfication$manager$2(G2FAVerfication g2FAVerfication) {
        super(0);
        this.this$0 = g2FAVerfication;
    }

    /* JADX WARN: Can't rename method to resolve collision */
    @Override // defpackage.rc1
    public final ClipboardManager invoke() {
        Object systemService = this.this$0.requireActivity().getSystemService("clipboard");
        Objects.requireNonNull(systemService, "null cannot be cast to non-null type android.content.ClipboardManager");
        return (ClipboardManager) systemService;
    }
}
