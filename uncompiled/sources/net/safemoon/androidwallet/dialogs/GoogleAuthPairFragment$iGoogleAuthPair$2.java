package net.safemoon.androidwallet.dialogs;

import androidx.fragment.app.Fragment;
import java.util.Objects;
import kotlin.jvm.internal.Lambda;

/* compiled from: GoogleAuthPairFragment.kt */
/* loaded from: classes2.dex */
public final class GoogleAuthPairFragment$iGoogleAuthPair$2 extends Lambda implements rc1<fm1> {
    public final /* synthetic */ GoogleAuthPairFragment this$0;

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public GoogleAuthPairFragment$iGoogleAuthPair$2(GoogleAuthPairFragment googleAuthPairFragment) {
        super(0);
        this.this$0 = googleAuthPairFragment;
    }

    /* JADX WARN: Can't rename method to resolve collision */
    @Override // defpackage.rc1
    public final fm1 invoke() {
        if (this.this$0.getParentFragment() instanceof fm1) {
            Fragment parentFragment = this.this$0.getParentFragment();
            Objects.requireNonNull(parentFragment, "null cannot be cast to non-null type net.safemoon.androidwallet.interfaces.IGoogleAuthPair");
            return (fm1) parentFragment;
        } else if (this.this$0.requireContext() instanceof fm1) {
            return (fm1) this.this$0.requireContext();
        } else {
            return null;
        }
    }
}
