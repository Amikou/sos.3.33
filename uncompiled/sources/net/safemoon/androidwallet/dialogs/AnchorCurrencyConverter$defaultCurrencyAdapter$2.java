package net.safemoon.androidwallet.dialogs;

import android.widget.ViewSwitcher;
import defpackage.w21;
import kotlin.jvm.internal.Lambda;
import net.safemoon.androidwallet.model.fiat.gson.Fiat;

/* compiled from: AnchorCurrencyConverter.kt */
/* loaded from: classes2.dex */
public final class AnchorCurrencyConverter$defaultCurrencyAdapter$2 extends Lambda implements rc1<a> {
    public final /* synthetic */ AnchorCurrencyConverter this$0;

    /* compiled from: AnchorCurrencyConverter.kt */
    /* loaded from: classes2.dex */
    public static final class a extends w21 {
        public final /* synthetic */ AnchorCurrencyConverter c;

        /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
        public a(AnchorCurrencyConverter anchorCurrencyConverter, b bVar) {
            super(bVar);
            this.c = anchorCurrencyConverter;
        }

        @Override // defpackage.w21
        public Fiat c() {
            return this.c.i ? this.c.e : this.c.f;
        }
    }

    /* compiled from: AnchorCurrencyConverter.kt */
    /* loaded from: classes2.dex */
    public static final class b implements w21.a {
        public final /* synthetic */ AnchorCurrencyConverter a;

        public b(AnchorCurrencyConverter anchorCurrencyConverter) {
            this.a = anchorCurrencyConverter;
        }

        @Override // defpackage.w21.a
        public void a(Fiat fiat) {
            dn0 dn0Var;
            ViewSwitcher viewSwitcher;
            fs1.f(fiat, "item");
            dn0Var = this.a.d;
            if (dn0Var != null && (viewSwitcher = dn0Var.a) != null) {
                viewSwitcher.showPrevious();
            }
            if (!this.a.i || fs1.b(this.a.f.getSymbol(), fiat.getSymbol())) {
                if (fs1.b(this.a.e.getSymbol(), fiat.getSymbol())) {
                    return;
                }
                this.a.f = fiat;
                this.a.C();
                return;
            }
            this.a.e = fiat;
            this.a.B();
        }
    }

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public AnchorCurrencyConverter$defaultCurrencyAdapter$2(AnchorCurrencyConverter anchorCurrencyConverter) {
        super(0);
        this.this$0 = anchorCurrencyConverter;
    }

    @Override // defpackage.rc1
    public final a invoke() {
        return new a(this.this$0, new b(this.this$0));
    }
}
