package net.safemoon.androidwallet.dialogs;

import com.google.gson.Gson;
import com.trustwallet.walletconnect.models.WCPeerMeta;
import kotlin.jvm.internal.Lambda;

/* compiled from: WalletConnectInterfaceFragment.kt */
/* loaded from: classes2.dex */
public final class WalletConnectInterfaceFragment$peer$2 extends Lambda implements rc1<WCPeerMeta> {
    public final /* synthetic */ WalletConnectInterfaceFragment this$0;

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public WalletConnectInterfaceFragment$peer$2(WalletConnectInterfaceFragment walletConnectInterfaceFragment) {
        super(0);
        this.this$0 = walletConnectInterfaceFragment;
    }

    /* JADX WARN: Can't rename method to resolve collision */
    @Override // defpackage.rc1
    public final WCPeerMeta invoke() {
        return (WCPeerMeta) new Gson().fromJson(this.this$0.requireArguments().getString("ARG_PEER"), (Class<Object>) WCPeerMeta.class);
    }
}
