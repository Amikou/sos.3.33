package net.safemoon.androidwallet.dialogs;

import kotlin.jvm.internal.Lambda;
import net.safemoon.androidwallet.dialogs.WalletConnectSmartContractCall;

/* compiled from: WalletConnectSmartContractCall.kt */
/* loaded from: classes2.dex */
public final class WalletConnectSmartContractCall$onViewCreated$2$1$1 extends Lambda implements rc1<te4> {
    public final /* synthetic */ ub1 $this_run;
    public final /* synthetic */ WalletConnectSmartContractCall this$0;

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public WalletConnectSmartContractCall$onViewCreated$2$1$1(ub1 ub1Var, WalletConnectSmartContractCall walletConnectSmartContractCall) {
        super(0);
        this.$this_run = ub1Var;
        this.this$0 = walletConnectSmartContractCall;
    }

    @Override // defpackage.rc1
    public /* bridge */ /* synthetic */ te4 invoke() {
        invoke2();
        return te4.a;
    }

    @Override // defpackage.rc1
    /* renamed from: invoke  reason: avoid collision after fix types in other method */
    public final void invoke2() {
        WalletConnectSmartContractCall.b bVar;
        this.$this_run.a.setEnabled(false);
        bVar = this.this$0.w0;
        if (bVar == null) {
            return;
        }
        bVar.b();
    }
}
