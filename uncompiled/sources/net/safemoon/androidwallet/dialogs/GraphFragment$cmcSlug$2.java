package net.safemoon.androidwallet.dialogs;

import kotlin.jvm.internal.Lambda;

/* compiled from: GraphFragment.kt */
/* loaded from: classes2.dex */
public final class GraphFragment$cmcSlug$2 extends Lambda implements rc1<String> {
    public final /* synthetic */ GraphFragment this$0;

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public GraphFragment$cmcSlug$2(GraphFragment graphFragment) {
        super(0);
        this.this$0 = graphFragment;
    }

    @Override // defpackage.rc1
    public final String invoke() {
        return this.this$0.requireArguments().getString("SLUG");
    }
}
