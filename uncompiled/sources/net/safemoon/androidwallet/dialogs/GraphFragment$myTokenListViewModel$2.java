package net.safemoon.androidwallet.dialogs;

import androidx.lifecycle.l;
import java.lang.ref.WeakReference;
import kotlin.jvm.internal.Lambda;
import net.safemoon.androidwallet.viewmodels.factory.MyViewModelFactory;

/* compiled from: GraphFragment.kt */
/* loaded from: classes2.dex */
public final class GraphFragment$myTokenListViewModel$2 extends Lambda implements rc1<l.b> {
    public final /* synthetic */ GraphFragment this$0;

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public GraphFragment$myTokenListViewModel$2(GraphFragment graphFragment) {
        super(0);
        this.this$0 = graphFragment;
    }

    /* JADX WARN: Can't rename method to resolve collision */
    @Override // defpackage.rc1
    public final l.b invoke() {
        return new MyViewModelFactory(new WeakReference(this.this$0.requireActivity()));
    }
}
