package net.safemoon.androidwallet.dialogs;

import android.app.Dialog;
import android.net.Uri;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebResourceRequest;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.FrameLayout;
import android.widget.RelativeLayout;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.fragment.app.FragmentViewModelLazyKt;
import com.google.android.material.bottomsheet.BottomSheetBehavior;
import defpackage.dr3;
import java.util.Objects;
import net.safemoon.androidwallet.R;
import net.safemoon.androidwallet.dialogs.PaySendWyreBottomDialog;
import net.safemoon.androidwallet.model.common.PaymentMethod;
import net.safemoon.androidwallet.viewmodels.HomeViewModel;

/* compiled from: PaySendWyreBottomDialog.kt */
/* loaded from: classes2.dex */
public final class PaySendWyreBottomDialog extends com.google.android.material.bottomsheet.b {
    public tc1<? super String, te4> B0;
    public un0 v0;
    public dr3 w0;
    public final sy1 x0 = FragmentViewModelLazyKt.a(this, d53.b(HomeViewModel.class), new PaySendWyreBottomDialog$special$$inlined$activityViewModels$default$1(this), new PaySendWyreBottomDialog$special$$inlined$activityViewModels$default$2(this));
    public final sy1 y0 = zy1.a(new PaySendWyreBottomDialog$paymentMethod$2(this));
    public final sy1 z0 = zy1.a(new PaySendWyreBottomDialog$currencyCode$2(this));
    public final gb2<Boolean> A0 = new gb2<>(Boolean.TRUE);

    /* compiled from: PaySendWyreBottomDialog.kt */
    /* loaded from: classes2.dex */
    public static final class a {
        public a() {
        }

        public /* synthetic */ a(qi0 qi0Var) {
            this();
        }
    }

    /* compiled from: PaySendWyreBottomDialog.kt */
    /* loaded from: classes2.dex */
    public static final class b implements dr3.a {
        public b() {
        }

        @Override // defpackage.dr3.a
        public void a(int i) {
            if (PaySendWyreBottomDialog.this.v0 == null) {
                return;
            }
            un0 un0Var = PaySendWyreBottomDialog.this.v0;
            fs1.d(un0Var);
            WebView webView = un0Var.c;
            ViewGroup.LayoutParams layoutParams = webView.getLayoutParams();
            Objects.requireNonNull(layoutParams, "null cannot be cast to non-null type android.view.ViewGroup.MarginLayoutParams");
            ((ViewGroup.MarginLayoutParams) layoutParams).bottomMargin = i;
            webView.requestLayout();
        }
    }

    /* compiled from: PaySendWyreBottomDialog.kt */
    /* loaded from: classes2.dex */
    public static final class c extends BottomSheetBehavior.g {
        public final /* synthetic */ BottomSheetBehavior<FrameLayout> a;

        public c(BottomSheetBehavior<FrameLayout> bottomSheetBehavior) {
            this.a = bottomSheetBehavior;
        }

        @Override // com.google.android.material.bottomsheet.BottomSheetBehavior.g
        public void a(View view, float f) {
            fs1.f(view, "bottomSheet");
        }

        @Override // com.google.android.material.bottomsheet.BottomSheetBehavior.g
        public void b(View view, int i) {
            fs1.f(view, "bottomSheet");
            if (i == 1) {
                this.a.V(3);
            }
        }
    }

    static {
        new a(null);
    }

    public static final void H(PaySendWyreBottomDialog paySendWyreBottomDialog, Boolean bool) {
        fs1.f(paySendWyreBottomDialog, "this$0");
        if (bool.booleanValue()) {
            return;
        }
        paySendWyreBottomDialog.G();
    }

    public final String D() {
        return (String) this.z0.getValue();
    }

    public final HomeViewModel E() {
        return (HomeViewModel) this.x0.getValue();
    }

    public final PaymentMethod F() {
        return (PaymentMethod) this.y0.getValue();
    }

    public final void G() {
        un0 un0Var = this.v0;
        FrameLayout frameLayout = un0Var == null ? null : un0Var.d;
        if (frameLayout != null) {
            frameLayout.setVisibility(0);
        }
        un0 un0Var2 = this.v0;
        RelativeLayout relativeLayout = un0Var2 != null ? un0Var2.b : null;
        if (relativeLayout == null) {
            return;
        }
        relativeLayout.setVisibility(8);
    }

    @Override // defpackage.sn0, androidx.fragment.app.Fragment
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        s(0, R.style.AppBottomSheetDialogTheme);
    }

    @Override // androidx.fragment.app.Fragment
    public View onCreateView(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle) {
        fs1.f(layoutInflater, "inflater");
        this.v0 = un0.a(layoutInflater.inflate(R.layout.dialog_pay_sendwyre, (ViewGroup) null, false));
        this.w0 = new dr3(this).b();
        un0 un0Var = this.v0;
        fs1.d(un0Var);
        ConstraintLayout b2 = un0Var.b();
        fs1.e(b2, "binding!!.root");
        return b2;
    }

    @Override // defpackage.sn0, androidx.fragment.app.Fragment
    public void onStart() {
        super.onStart();
        dr3 dr3Var = this.w0;
        if (dr3Var == null) {
            return;
        }
        dr3Var.d(new b());
    }

    @Override // defpackage.sn0, androidx.fragment.app.Fragment
    public void onStop() {
        super.onStop();
        dr3 dr3Var = this.w0;
        if (dr3Var == null) {
            return;
        }
        dr3Var.d(null);
    }

    @Override // androidx.fragment.app.Fragment
    public void onViewCreated(View view, Bundle bundle) {
        fs1.f(view, "view");
        super.onViewCreated(view, bundle);
        Dialog k = k();
        if (k != null) {
            View findViewById = k.findViewById(R.id.design_bottom_sheet);
            Objects.requireNonNull(findViewById, "null cannot be cast to non-null type android.widget.FrameLayout");
            BottomSheetBehavior y = BottomSheetBehavior.y((FrameLayout) findViewById);
            fs1.e(y, "from(bottomSheet)");
            y.V(2);
            y.o(new c(y));
        }
        this.A0.observe(getViewLifecycleOwner(), new tl2() { // from class: eq2
            @Override // defpackage.tl2
            public final void onChanged(Object obj) {
                PaySendWyreBottomDialog.H(PaySendWyreBottomDialog.this, (Boolean) obj);
            }
        });
        un0 un0Var = this.v0;
        fs1.d(un0Var);
        WebView webView = un0Var.c;
        webView.getSettings().setJavaScriptEnabled(true);
        webView.getSettings().setSupportZoom(false);
        webView.getSettings().setJavaScriptCanOpenWindowsAutomatically(false);
        webView.getSettings().setBuiltInZoomControls(false);
        webView.getSettings().setGeolocationEnabled(false);
        webView.setWebViewClient(new d());
        HomeViewModel E = E();
        PaymentMethod F = F();
        String D = D();
        fs1.e(D, "currencyCode");
        E.h(F, D, new PaySendWyreBottomDialog$onViewCreated$4(this), new PaySendWyreBottomDialog$onViewCreated$5(this));
    }

    /* compiled from: PaySendWyreBottomDialog.kt */
    /* loaded from: classes2.dex */
    public static final class d extends WebViewClient {
        public d() {
        }

        @Override // android.webkit.WebViewClient
        public void onPageFinished(WebView webView, String str) {
            super.onPageFinished(webView, str);
            PaySendWyreBottomDialog.this.A0.postValue(Boolean.FALSE);
        }

        @Override // android.webkit.WebViewClient
        public boolean shouldOverrideUrlLoading(WebView webView, WebResourceRequest webResourceRequest) {
            Uri url;
            String path;
            boolean z = false;
            if (webResourceRequest != null && (url = webResourceRequest.getUrl()) != null && (path = url.getPath()) != null && dv3.H(path, "https://safemoon.net/", false, 2, null)) {
                z = true;
            }
            if (z) {
                PaySendWyreBottomDialog.this.h();
                return true;
            }
            return super.shouldOverrideUrlLoading(webView, webResourceRequest);
        }

        @Override // android.webkit.WebViewClient
        public boolean shouldOverrideUrlLoading(WebView webView, String str) {
            if (str != null && dv3.H(str, "https://safemoon.net/", false, 2, null)) {
                PaySendWyreBottomDialog.this.h();
                return true;
            }
            return false;
        }
    }
}
