package net.safemoon.androidwallet.dialogs;

import android.app.Dialog;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentViewModelLazyKt;
import net.safemoon.androidwallet.R;
import net.safemoon.androidwallet.dialogs.SwapTransactionSpeed;
import net.safemoon.androidwallet.model.common.Gas;
import net.safemoon.androidwallet.viewmodels.SwapMigrationViewModel;
import net.safemoon.androidwallet.viewmodels.SwapViewModel;

/* compiled from: SwapTransactionSpeed.kt */
/* loaded from: classes2.dex */
public final class SwapTransactionSpeed extends sn0 {
    public static final a x0 = new a(null);
    public io0 u0;
    public final sy1 v0 = FragmentViewModelLazyKt.a(this, d53.b(SwapViewModel.class), new SwapTransactionSpeed$special$$inlined$viewModels$default$1(new SwapTransactionSpeed$swapViewModel$2(this)), null);
    public final sy1 w0 = FragmentViewModelLazyKt.a(this, d53.b(SwapMigrationViewModel.class), new SwapTransactionSpeed$special$$inlined$viewModels$default$2(new SwapTransactionSpeed$swapMigrationViewModel$2(this)), null);

    /* compiled from: SwapTransactionSpeed.kt */
    /* loaded from: classes2.dex */
    public static final class a {
        public a() {
        }

        public /* synthetic */ a(qi0 qi0Var) {
            this();
        }

        public final SwapTransactionSpeed a() {
            SwapTransactionSpeed swapTransactionSpeed = new SwapTransactionSpeed();
            Bundle bundle = new Bundle();
            te4 te4Var = te4.a;
            swapTransactionSpeed.setArguments(bundle);
            return swapTransactionSpeed;
        }
    }

    /* compiled from: SwapTransactionSpeed.kt */
    /* loaded from: classes2.dex */
    public static final class b implements tl2<Gas> {

        /* compiled from: SwapTransactionSpeed.kt */
        /* loaded from: classes2.dex */
        public /* synthetic */ class a {
            public static final /* synthetic */ int[] a;

            static {
                int[] iArr = new int[Gas.values().length];
                iArr[Gas.Standard.ordinal()] = 1;
                iArr[Gas.Fast.ordinal()] = 2;
                iArr[Gas.Lightning.ordinal()] = 3;
                a = iArr;
            }
        }

        public b() {
        }

        @Override // defpackage.tl2
        /* renamed from: a */
        public void onChanged(Gas gas) {
            SwapTransactionSpeed.this.B().i0().removeObserver(this);
            SwapTransactionSpeed swapTransactionSpeed = SwapTransactionSpeed.this;
            int i = gas == null ? -1 : a.a[gas.ordinal()];
            io0 io0Var = null;
            if (i == 1) {
                io0 io0Var2 = swapTransactionSpeed.u0;
                if (io0Var2 == null) {
                    fs1.r("binding");
                } else {
                    io0Var = io0Var2;
                }
                io0Var.d.setChecked(true);
            } else if (i == 2) {
                io0 io0Var3 = swapTransactionSpeed.u0;
                if (io0Var3 == null) {
                    fs1.r("binding");
                } else {
                    io0Var = io0Var3;
                }
                io0Var.b.setChecked(true);
            } else if (i != 3) {
            } else {
                io0 io0Var4 = swapTransactionSpeed.u0;
                if (io0Var4 == null) {
                    fs1.r("binding");
                } else {
                    io0Var = io0Var4;
                }
                io0Var.c.setChecked(true);
            }
        }
    }

    /* compiled from: SwapTransactionSpeed.kt */
    /* loaded from: classes2.dex */
    public static final class c implements tl2<Gas> {

        /* compiled from: SwapTransactionSpeed.kt */
        /* loaded from: classes2.dex */
        public /* synthetic */ class a {
            public static final /* synthetic */ int[] a;

            static {
                int[] iArr = new int[Gas.values().length];
                iArr[Gas.Standard.ordinal()] = 1;
                iArr[Gas.Fast.ordinal()] = 2;
                iArr[Gas.Lightning.ordinal()] = 3;
                a = iArr;
            }
        }

        public c() {
        }

        @Override // defpackage.tl2
        /* renamed from: a */
        public void onChanged(Gas gas) {
            SwapTransactionSpeed.this.A().O().removeObserver(this);
            SwapTransactionSpeed swapTransactionSpeed = SwapTransactionSpeed.this;
            int i = gas == null ? -1 : a.a[gas.ordinal()];
            io0 io0Var = null;
            if (i == 1) {
                io0 io0Var2 = swapTransactionSpeed.u0;
                if (io0Var2 == null) {
                    fs1.r("binding");
                } else {
                    io0Var = io0Var2;
                }
                io0Var.d.setChecked(true);
            } else if (i == 2) {
                io0 io0Var3 = swapTransactionSpeed.u0;
                if (io0Var3 == null) {
                    fs1.r("binding");
                } else {
                    io0Var = io0Var3;
                }
                io0Var.b.setChecked(true);
            } else if (i != 3) {
            } else {
                io0 io0Var4 = swapTransactionSpeed.u0;
                if (io0Var4 == null) {
                    fs1.r("binding");
                } else {
                    io0Var = io0Var4;
                }
                io0Var.c.setChecked(true);
            }
        }
    }

    public static final void C(SwapTransactionSpeed swapTransactionSpeed, View view) {
        fs1.f(swapTransactionSpeed, "this$0");
        io0 io0Var = swapTransactionSpeed.u0;
        io0 io0Var2 = null;
        if (io0Var == null) {
            fs1.r("binding");
            io0Var = null;
        }
        if (io0Var.d.isChecked()) {
            swapTransactionSpeed.E(Gas.Standard);
        } else {
            io0 io0Var3 = swapTransactionSpeed.u0;
            if (io0Var3 == null) {
                fs1.r("binding");
                io0Var3 = null;
            }
            if (io0Var3.b.isChecked()) {
                swapTransactionSpeed.E(Gas.Fast);
            } else {
                io0 io0Var4 = swapTransactionSpeed.u0;
                if (io0Var4 == null) {
                    fs1.r("binding");
                } else {
                    io0Var2 = io0Var4;
                }
                if (io0Var2.c.isChecked()) {
                    swapTransactionSpeed.E(Gas.Lightning);
                }
            }
        }
        swapTransactionSpeed.h();
    }

    public static final void D(SwapTransactionSpeed swapTransactionSpeed, View view) {
        fs1.f(swapTransactionSpeed, "this$0");
        swapTransactionSpeed.h();
    }

    public final SwapMigrationViewModel A() {
        return (SwapMigrationViewModel) this.w0.getValue();
    }

    public final SwapViewModel B() {
        return (SwapViewModel) this.v0.getValue();
    }

    public final void E(Gas gas) {
        B().i0().setValue(gas);
        A().O().setValue(gas);
    }

    public final void F(FragmentManager fragmentManager) {
        fs1.f(fragmentManager, "manager");
        super.u(fragmentManager, SwapTransactionSpeed.class.getCanonicalName());
    }

    @Override // defpackage.sn0
    public Dialog m(Bundle bundle) {
        Dialog m = super.m(bundle);
        fs1.e(m, "super.onCreateDialog(savedInstanceState)");
        m.requestWindowFeature(1);
        return m;
    }

    @Override // androidx.fragment.app.Fragment
    public View onCreateView(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle) {
        fs1.f(layoutInflater, "inflater");
        return LayoutInflater.from(requireContext()).inflate(R.layout.dialog_swap_transaction_speed, viewGroup, false);
    }

    @Override // defpackage.sn0, androidx.fragment.app.Fragment
    public void onStart() {
        super.onStart();
        Dialog k = k();
        if (k == null) {
            return;
        }
        Window window = k.getWindow();
        if (window != null) {
            window.setBackgroundDrawable(new ColorDrawable(0));
        }
        Window window2 = k.getWindow();
        if (window2 == null) {
            return;
        }
        window2.setLayout(-1, -2);
    }

    @Override // androidx.fragment.app.Fragment
    public void onViewCreated(View view, Bundle bundle) {
        fs1.f(view, "view");
        super.onViewCreated(view, bundle);
        io0 a2 = io0.a(view);
        fs1.e(a2, "bind(view)");
        this.u0 = a2;
        B().i0().observe(getViewLifecycleOwner(), new b());
        A().O().observe(getViewLifecycleOwner(), new c());
        io0 io0Var = this.u0;
        io0 io0Var2 = null;
        if (io0Var == null) {
            fs1.r("binding");
            io0Var = null;
        }
        io0Var.a.setOnClickListener(new View.OnClickListener() { // from class: p14
            @Override // android.view.View.OnClickListener
            public final void onClick(View view2) {
                SwapTransactionSpeed.C(SwapTransactionSpeed.this, view2);
            }
        });
        io0 io0Var3 = this.u0;
        if (io0Var3 == null) {
            fs1.r("binding");
        } else {
            io0Var2 = io0Var3;
        }
        io0Var2.e.setOnClickListener(new View.OnClickListener() { // from class: o14
            @Override // android.view.View.OnClickListener
            public final void onClick(View view2) {
                SwapTransactionSpeed.D(SwapTransactionSpeed.this, view2);
            }
        });
    }
}
