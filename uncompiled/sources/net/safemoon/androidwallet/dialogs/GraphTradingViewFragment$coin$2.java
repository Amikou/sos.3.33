package net.safemoon.androidwallet.dialogs;

import java.io.Serializable;
import kotlin.jvm.internal.Lambda;
import net.safemoon.androidwallet.model.Coin;

/* compiled from: GraphTradingViewFragment.kt */
/* loaded from: classes2.dex */
public final class GraphTradingViewFragment$coin$2 extends Lambda implements rc1<Coin> {
    public final /* synthetic */ GraphTradingViewFragment this$0;

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public GraphTradingViewFragment$coin$2(GraphTradingViewFragment graphTradingViewFragment) {
        super(0);
        this.this$0 = graphTradingViewFragment;
    }

    @Override // defpackage.rc1
    public final Coin invoke() {
        Serializable serializable = this.this$0.requireArguments().getSerializable("argCoin");
        if (serializable == null) {
            serializable = new Coin();
        }
        return (Coin) serializable;
    }
}
