package net.safemoon.androidwallet.dialogs;

import kotlin.jvm.internal.Lambda;
import net.safemoon.androidwallet.viewmodels.SwapViewModel;

/* compiled from: ConfirmSwapTransactionFragment.kt */
/* loaded from: classes2.dex */
public final class ConfirmSwapTransactionFragment$onViewCreated$1$4$onSlideComplete$1 extends Lambda implements rc1<te4> {
    public final /* synthetic */ ConfirmSwapTransactionFragment this$0;

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public ConfirmSwapTransactionFragment$onViewCreated$1$4$onSlideComplete$1(ConfirmSwapTransactionFragment confirmSwapTransactionFragment) {
        super(0);
        this.this$0 = confirmSwapTransactionFragment;
    }

    @Override // defpackage.rc1
    public /* bridge */ /* synthetic */ te4 invoke() {
        invoke2();
        return te4.a;
    }

    @Override // defpackage.rc1
    /* renamed from: invoke  reason: avoid collision after fix types in other method */
    public final void invoke2() {
        SwapViewModel J;
        J = this.this$0.J();
        J.l1();
        this.this$0.h();
    }
}
