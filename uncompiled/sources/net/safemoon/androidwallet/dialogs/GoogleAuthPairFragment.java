package net.safemoon.androidwallet.dialogs;

import android.app.Dialog;
import android.content.ClipData;
import android.content.ClipboardManager;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentViewModelLazyKt;
import java.util.Objects;
import net.safemoon.androidwallet.R;
import net.safemoon.androidwallet.dialogs.GoogleAuthPairFragment;
import net.safemoon.androidwallet.viewmodels.GoogleAuthViewModel;

/* compiled from: GoogleAuthPairFragment.kt */
/* loaded from: classes2.dex */
public final class GoogleAuthPairFragment extends sn0 {
    public static final a x0 = new a(null);
    public ga1 v0;
    public final sy1 u0 = zy1.a(new GoogleAuthPairFragment$iGoogleAuthPair$2(this));
    public final sy1 w0 = FragmentViewModelLazyKt.a(this, d53.b(GoogleAuthViewModel.class), new GoogleAuthPairFragment$special$$inlined$activityViewModels$default$1(this), new GoogleAuthPairFragment$special$$inlined$activityViewModels$default$2(this));

    /* compiled from: GoogleAuthPairFragment.kt */
    /* loaded from: classes2.dex */
    public static final class a {
        public a() {
        }

        public /* synthetic */ a(qi0 qi0Var) {
            this();
        }

        public final GoogleAuthPairFragment a() {
            return new GoogleAuthPairFragment();
        }
    }

    public static final void G(GoogleAuthPairFragment googleAuthPairFragment, String str) {
        fs1.f(googleAuthPairFragment, "this$0");
        ga1 ga1Var = googleAuthPairFragment.v0;
        if (ga1Var == null) {
            fs1.r("binding");
            ga1Var = null;
        }
        ga1Var.h.setText(str);
    }

    public static final void H(GoogleAuthPairFragment googleAuthPairFragment, Bitmap bitmap) {
        fs1.f(googleAuthPairFragment, "this$0");
        ga1 ga1Var = googleAuthPairFragment.v0;
        if (ga1Var == null) {
            fs1.r("binding");
            ga1Var = null;
        }
        ga1Var.e.setImageBitmap(bitmap);
    }

    public static final void I(GoogleAuthPairFragment googleAuthPairFragment, View view) {
        fs1.f(googleAuthPairFragment, "this$0");
        googleAuthPairFragment.Q();
    }

    public static final void J(GoogleAuthPairFragment googleAuthPairFragment, View view) {
        fs1.f(googleAuthPairFragment, "this$0");
        googleAuthPairFragment.Q();
    }

    public static final void K(GoogleAuthPairFragment googleAuthPairFragment, View view) {
        fs1.f(googleAuthPairFragment, "this$0");
        googleAuthPairFragment.R();
    }

    public static final void L(GoogleAuthPairFragment googleAuthPairFragment, View view) {
        fs1.f(googleAuthPairFragment, "this$0");
        googleAuthPairFragment.R();
    }

    public static final void M(GoogleAuthPairFragment googleAuthPairFragment, View view) {
        fs1.f(googleAuthPairFragment, "this$0");
        Object systemService = googleAuthPairFragment.requireContext().getSystemService("clipboard");
        Objects.requireNonNull(systemService, "null cannot be cast to non-null type android.content.ClipboardManager");
        ((ClipboardManager) systemService).setPrimaryClip(ClipData.newPlainText("label", googleAuthPairFragment.E().g().getValue()));
        Context requireContext = googleAuthPairFragment.requireContext();
        fs1.e(requireContext, "requireContext()");
        e30.Z(requireContext, R.string.copied_to_clipboard);
    }

    public static final void N(GoogleAuthPairFragment googleAuthPairFragment, View view) {
        fs1.f(googleAuthPairFragment, "this$0");
        fm1 F = googleAuthPairFragment.F();
        if (F != null) {
            F.a();
        }
        googleAuthPairFragment.h();
    }

    public static final void O(GoogleAuthPairFragment googleAuthPairFragment, View view) {
        fs1.f(googleAuthPairFragment, "this$0");
        Dialog k = googleAuthPairFragment.k();
        if (k == null) {
            return;
        }
        k.dismiss();
    }

    public final GoogleAuthViewModel E() {
        return (GoogleAuthViewModel) this.w0.getValue();
    }

    public final fm1 F() {
        return (fm1) this.u0.getValue();
    }

    public final void P(FragmentManager fragmentManager) {
        fs1.f(fragmentManager, "manager");
        super.u(fragmentManager, GoogleAuthPairFragment.class.getCanonicalName());
    }

    public final void Q() {
        ga1 ga1Var = this.v0;
        ga1 ga1Var2 = null;
        if (ga1Var == null) {
            fs1.r("binding");
            ga1Var = null;
        }
        ga1Var.c.setVisibility(0);
        ga1 ga1Var3 = this.v0;
        if (ga1Var3 == null) {
            fs1.r("binding");
        } else {
            ga1Var2 = ga1Var3;
        }
        ga1Var2.d.setVisibility(8);
    }

    public final void R() {
        ga1 ga1Var = this.v0;
        ga1 ga1Var2 = null;
        if (ga1Var == null) {
            fs1.r("binding");
            ga1Var = null;
        }
        ga1Var.c.setVisibility(8);
        ga1 ga1Var3 = this.v0;
        if (ga1Var3 == null) {
            fs1.r("binding");
        } else {
            ga1Var2 = ga1Var3;
        }
        ga1Var2.d.setVisibility(0);
    }

    @Override // defpackage.sn0, androidx.fragment.app.Fragment
    public void onAttach(Context context) {
        fs1.f(context, "context");
        super.onAttach(context);
    }

    @Override // androidx.fragment.app.Fragment
    public View onCreateView(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle) {
        fs1.f(layoutInflater, "inflater");
        return LayoutInflater.from(requireContext()).inflate(R.layout.fragment_google_auth_pair, viewGroup, false);
    }

    @Override // defpackage.sn0, androidx.fragment.app.Fragment
    public void onDetach() {
        super.onDetach();
    }

    @Override // defpackage.sn0, androidx.fragment.app.Fragment
    public void onStart() {
        Window window;
        super.onStart();
        Dialog k = k();
        if (k == null || (window = k.getWindow()) == null) {
            return;
        }
        window.setBackgroundDrawable(new ColorDrawable(0));
        window.setLayout(-1, -2);
    }

    @Override // androidx.fragment.app.Fragment
    public void onViewCreated(View view, Bundle bundle) {
        fs1.f(view, "view");
        super.onViewCreated(view, bundle);
        ga1 a2 = ga1.a(view);
        fs1.e(a2, "bind(view)");
        this.v0 = a2;
        E().g().observe(getViewLifecycleOwner(), new tl2() { // from class: hh1
            @Override // defpackage.tl2
            public final void onChanged(Object obj) {
                GoogleAuthPairFragment.G(GoogleAuthPairFragment.this, (String) obj);
            }
        });
        E().f().observe(getViewLifecycleOwner(), new tl2() { // from class: gh1
            @Override // defpackage.tl2
            public final void onChanged(Object obj) {
                GoogleAuthPairFragment.H(GoogleAuthPairFragment.this, (Bitmap) obj);
            }
        });
        ga1 ga1Var = this.v0;
        ga1 ga1Var2 = null;
        if (ga1Var == null) {
            fs1.r("binding");
            ga1Var = null;
        }
        ga1Var.i.setOnClickListener(new View.OnClickListener() { // from class: nh1
            @Override // android.view.View.OnClickListener
            public final void onClick(View view2) {
                GoogleAuthPairFragment.I(GoogleAuthPairFragment.this, view2);
            }
        });
        ga1 ga1Var3 = this.v0;
        if (ga1Var3 == null) {
            fs1.r("binding");
            ga1Var3 = null;
        }
        ga1Var3.f.setOnClickListener(new View.OnClickListener() { // from class: mh1
            @Override // android.view.View.OnClickListener
            public final void onClick(View view2) {
                GoogleAuthPairFragment.J(GoogleAuthPairFragment.this, view2);
            }
        });
        ga1 ga1Var4 = this.v0;
        if (ga1Var4 == null) {
            fs1.r("binding");
            ga1Var4 = null;
        }
        ga1Var4.j.setOnClickListener(new View.OnClickListener() { // from class: kh1
            @Override // android.view.View.OnClickListener
            public final void onClick(View view2) {
                GoogleAuthPairFragment.K(GoogleAuthPairFragment.this, view2);
            }
        });
        ga1 ga1Var5 = this.v0;
        if (ga1Var5 == null) {
            fs1.r("binding");
            ga1Var5 = null;
        }
        ga1Var5.g.setOnClickListener(new View.OnClickListener() { // from class: oh1
            @Override // android.view.View.OnClickListener
            public final void onClick(View view2) {
                GoogleAuthPairFragment.L(GoogleAuthPairFragment.this, view2);
            }
        });
        ga1 ga1Var6 = this.v0;
        if (ga1Var6 == null) {
            fs1.r("binding");
            ga1Var6 = null;
        }
        ga1Var6.h.setOnCloseIconClickListener(new View.OnClickListener() { // from class: jh1
            @Override // android.view.View.OnClickListener
            public final void onClick(View view2) {
                GoogleAuthPairFragment.M(GoogleAuthPairFragment.this, view2);
            }
        });
        ga1 ga1Var7 = this.v0;
        if (ga1Var7 == null) {
            fs1.r("binding");
            ga1Var7 = null;
        }
        ga1Var7.a.setOnClickListener(new View.OnClickListener() { // from class: ih1
            @Override // android.view.View.OnClickListener
            public final void onClick(View view2) {
                GoogleAuthPairFragment.N(GoogleAuthPairFragment.this, view2);
            }
        });
        ga1 ga1Var8 = this.v0;
        if (ga1Var8 == null) {
            fs1.r("binding");
        } else {
            ga1Var2 = ga1Var8;
        }
        ga1Var2.b.setOnClickListener(new View.OnClickListener() { // from class: lh1
            @Override // android.view.View.OnClickListener
            public final void onClick(View view2) {
                GoogleAuthPairFragment.O(GoogleAuthPairFragment.this, view2);
            }
        });
        E().c();
    }
}
