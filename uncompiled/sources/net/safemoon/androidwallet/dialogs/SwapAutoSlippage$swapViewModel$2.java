package net.safemoon.androidwallet.dialogs;

import androidx.fragment.app.FragmentActivity;
import kotlin.jvm.internal.Lambda;

/* compiled from: SwapAutoSlippage.kt */
/* loaded from: classes2.dex */
public final class SwapAutoSlippage$swapViewModel$2 extends Lambda implements rc1<hj4> {
    public final /* synthetic */ SwapAutoSlippage this$0;

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public SwapAutoSlippage$swapViewModel$2(SwapAutoSlippage swapAutoSlippage) {
        super(0);
        this.this$0 = swapAutoSlippage;
    }

    /* JADX WARN: Can't rename method to resolve collision */
    @Override // defpackage.rc1
    public final hj4 invoke() {
        FragmentActivity requireActivity = this.this$0.requireActivity();
        fs1.e(requireActivity, "requireActivity()");
        return requireActivity;
    }
}
