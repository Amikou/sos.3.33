package net.safemoon.androidwallet.dialogs;

import android.app.Dialog;
import kotlin.jvm.internal.Lambda;
import net.safemoon.androidwallet.model.priceAlert.PriceAlertToken;
import net.safemoon.androidwallet.viewmodels.CryptoPriceAlertViewModel;

/* compiled from: CryptoPriceAlertFragment.kt */
/* loaded from: classes2.dex */
public final class CryptoPriceAlertFragment$onViewCreated$1$6$1 extends Lambda implements hd1<gt, Dialog, te4> {
    public final /* synthetic */ CryptoPriceAlertFragment this$0;

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public CryptoPriceAlertFragment$onViewCreated$1$6$1(CryptoPriceAlertFragment cryptoPriceAlertFragment) {
        super(2);
        this.this$0 = cryptoPriceAlertFragment;
    }

    /* renamed from: invoke  reason: avoid collision after fix types in other method */
    public final void invoke2(gt gtVar, Dialog dialog) {
        CryptoPriceAlertViewModel N;
        CryptoPriceAlertViewModel N2;
        CryptoPriceAlertViewModel N3;
        fs1.f(gtVar, "ccButton");
        fs1.f(dialog, "dialog");
        if (gtVar.a() == 104) {
            N = this.this$0.N();
            PriceAlertToken value = N.u().getValue();
            if (value != null) {
                CryptoPriceAlertFragment cryptoPriceAlertFragment = this.this$0;
                N2 = cryptoPriceAlertFragment.N();
                Integer value2 = N2.t().getValue();
                if (value2 != null) {
                    Integer chainId = value.getChainId();
                    if ((chainId == null ? 0 : chainId.intValue()) <= 0) {
                        value.setChainId(null);
                    }
                    N3 = cryptoPriceAlertFragment.N();
                    N3.i(value, value2.intValue());
                    cryptoPriceAlertFragment.w0 = false;
                }
            }
        }
        dialog.dismiss();
    }

    @Override // defpackage.hd1
    public /* bridge */ /* synthetic */ te4 invoke(gt gtVar, Dialog dialog) {
        invoke2(gtVar, dialog);
        return te4.a;
    }
}
