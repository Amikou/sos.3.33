package net.safemoon.androidwallet.dialogs;

import com.google.android.material.textview.MaterialTextView;
import kotlin.jvm.internal.Lambda;
import net.safemoon.androidwallet.model.priceAlert.PriceAlertToken;
import net.safemoon.androidwallet.viewmodels.CryptoPriceAlertViewModel;
import net.safemoon.androidwallet.views.editText.autoSize.AutofitEdittext;

/* compiled from: CryptoPriceAlertFragment.kt */
/* loaded from: classes2.dex */
public final class CryptoPriceAlertFragment$onViewCreated$1$1$2 extends Lambda implements tc1<Boolean, te4> {
    public final /* synthetic */ z91 $this_with;
    public final /* synthetic */ CryptoPriceAlertFragment this$0;

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public CryptoPriceAlertFragment$onViewCreated$1$1$2(CryptoPriceAlertFragment cryptoPriceAlertFragment, z91 z91Var) {
        super(1);
        this.this$0 = cryptoPriceAlertFragment;
        this.$this_with = z91Var;
    }

    @Override // defpackage.tc1
    public /* bridge */ /* synthetic */ te4 invoke(Boolean bool) {
        invoke(bool.booleanValue());
        return te4.a;
    }

    public final void invoke(boolean z) {
        CryptoPriceAlertViewModel N;
        CryptoPriceAlertViewModel N2;
        this.this$0.w0 = true;
        AutofitEdittext autofitEdittext = this.$this_with.c;
        fs1.e(autofitEdittext, "edtMaximum");
        autofitEdittext.setVisibility(z ? 0 : 8);
        MaterialTextView materialTextView = this.$this_with.m;
        fs1.e(materialTextView, "txtMaximumHint");
        materialTextView.setVisibility(z ? 0 : 8);
        N = this.this$0.N();
        PriceAlertToken value = N.u().getValue();
        if (value == null) {
            return;
        }
        CryptoPriceAlertFragment cryptoPriceAlertFragment = this.this$0;
        value.setPersistent(z);
        N2 = cryptoPriceAlertFragment.N();
        CryptoPriceAlertViewModel.A(N2, value, null, 2, null);
    }
}
