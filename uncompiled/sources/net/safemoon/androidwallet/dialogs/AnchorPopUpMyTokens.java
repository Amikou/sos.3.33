package net.safemoon.androidwallet.dialogs;

import android.content.Context;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.PopupWindow;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import com.google.android.material.textfield.TextInputEditText;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;
import java.util.Objects;
import kotlin.text.StringsKt__StringsKt;
import net.safemoon.androidwallet.R;

/* compiled from: AnchorPopUpMyTokens.kt */
/* loaded from: classes2.dex */
public final class AnchorPopUpMyTokens {
    public PopupWindow a;
    public ep3 b;
    public final List<q9> c = new ArrayList();

    /* compiled from: TextView.kt */
    /* loaded from: classes2.dex */
    public static final class a implements TextWatcher {
        public a() {
        }

        @Override // android.text.TextWatcher
        public void afterTextChanged(Editable editable) {
        }

        @Override // android.text.TextWatcher
        public void beforeTextChanged(CharSequence charSequence, int i, int i2, int i3) {
        }

        @Override // android.text.TextWatcher
        public void onTextChanged(CharSequence charSequence, int i, int i2, int i3) {
            ep3 ep3Var;
            List list = AnchorPopUpMyTokens.this.c;
            ArrayList arrayList = new ArrayList();
            Iterator it = list.iterator();
            while (true) {
                ep3Var = null;
                if (!it.hasNext()) {
                    break;
                }
                Object next = it.next();
                q9 q9Var = (q9) next;
                boolean z = true;
                if (charSequence != null) {
                    if (StringsKt__StringsKt.K0(charSequence).length() > 0) {
                        String g = q9Var.g();
                        Objects.requireNonNull(g, "null cannot be cast to non-null type java.lang.String");
                        Locale locale = Locale.ROOT;
                        String lowerCase = g.toLowerCase(locale);
                        fs1.e(lowerCase, "(this as java.lang.Strin….toLowerCase(Locale.ROOT)");
                        String obj = StringsKt__StringsKt.K0(charSequence).toString();
                        Objects.requireNonNull(obj, "null cannot be cast to non-null type java.lang.String");
                        String lowerCase2 = obj.toLowerCase(locale);
                        fs1.e(lowerCase2, "(this as java.lang.Strin….toLowerCase(Locale.ROOT)");
                        if (!StringsKt__StringsKt.M(lowerCase, lowerCase2, false, 2, null)) {
                            String f = q9Var.f();
                            Objects.requireNonNull(f, "null cannot be cast to non-null type java.lang.String");
                            String lowerCase3 = f.toLowerCase(locale);
                            fs1.e(lowerCase3, "(this as java.lang.Strin….toLowerCase(Locale.ROOT)");
                            if (!StringsKt__StringsKt.M(lowerCase3, StringsKt__StringsKt.K0(charSequence).toString(), false, 2, null)) {
                                z = false;
                            }
                        }
                    }
                }
                if (z) {
                    arrayList.add(next);
                }
            }
            ep3 ep3Var2 = AnchorPopUpMyTokens.this.b;
            if (ep3Var2 == null) {
                fs1.r("adapter");
            } else {
                ep3Var = ep3Var2;
            }
            ep3Var.submitList(arrayList);
        }
    }

    public final PopupWindow c(View view, View view2, int i) {
        PopupWindow popupWindow = new PopupWindow(view, (int) ((view2 == null ? -1 : view2.getWidth()) * 0.8d), i <= 3 ? -2 : (int) t40.a(300));
        popupWindow.setOutsideTouchable(true);
        popupWindow.setFocusable(true);
        popupWindow.setInputMethodMode(1);
        popupWindow.setAttachedInDecor(false);
        return popupWindow;
    }

    public final void d() {
        PopupWindow popupWindow;
        if (!e() || (popupWindow = this.a) == null) {
            return;
        }
        popupWindow.dismiss();
    }

    public final boolean e() {
        PopupWindow popupWindow = this.a;
        if (popupWindow == null) {
            return false;
        }
        return popupWindow.isShowing();
    }

    public final AnchorPopUpMyTokens f(Context context, View view, View view2, tc1<? super q9, te4> tc1Var, String str, PopupWindow.OnDismissListener onDismissListener) {
        fs1.f(context, "context");
        fs1.f(view, "anchorView");
        fs1.f(tc1Var, "onItemClick");
        fs1.f(onDismissListener, "onDismissListener");
        this.b = new ep3(context, str, new AnchorPopUpMyTokens$show$1(tc1Var, this));
        Object systemService = context.getSystemService("layout_inflater");
        Objects.requireNonNull(systemService, "null cannot be cast to non-null type android.view.LayoutInflater");
        ep3 ep3Var = null;
        View inflate = ((LayoutInflater) systemService).inflate(R.layout.dialog_anchor_pop_up_my_tokens, (ViewGroup) null);
        gn0 a2 = gn0.a(inflate);
        fs1.e(a2, "bind(view)");
        a2.a.setLayoutManager(new LinearLayoutManager(context, 1, false));
        RecyclerView recyclerView = a2.a;
        ep3 ep3Var2 = this.b;
        if (ep3Var2 == null) {
            fs1.r("adapter");
            ep3Var2 = null;
        }
        recyclerView.setAdapter(ep3Var2);
        ep3 ep3Var3 = this.b;
        if (ep3Var3 == null) {
            fs1.r("adapter");
        } else {
            ep3Var = ep3Var3;
        }
        ep3Var.submitList(this.c);
        TextInputEditText textInputEditText = a2.b.b;
        fs1.e(textInputEditText, "searchBar.etSearch");
        textInputEditText.addTextChangedListener(new a());
        fs1.e(inflate, "view");
        PopupWindow c = c(inflate, view2, this.c.size());
        this.a = c;
        if (c != null) {
            c.setOnDismissListener(onDismissListener);
        }
        PopupWindow popupWindow = this.a;
        if (popupWindow != null) {
            popupWindow.showAtLocation(view, 8388611, 0, -view.getHeight());
        }
        return this;
    }

    public final void g(List<q9> list) {
        fs1.f(list, "list");
        this.c.clear();
        this.c.addAll(list);
    }
}
