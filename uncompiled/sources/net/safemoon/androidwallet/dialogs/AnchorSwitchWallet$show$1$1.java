package net.safemoon.androidwallet.dialogs;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import com.google.android.material.button.MaterialButton;
import java.util.ArrayList;
import java.util.List;
import kotlin.jvm.internal.Lambda;
import net.safemoon.androidwallet.R;
import net.safemoon.androidwallet.activity.AKTHomeActivity;
import net.safemoon.androidwallet.dialogs.AnchorSwitchWallet;
import net.safemoon.androidwallet.dialogs.AnchorSwitchWallet$show$1$1;
import net.safemoon.androidwallet.model.wallets.Wallet;
import net.safemoon.androidwallet.viewmodels.MultiWalletViewModel;

/* compiled from: AnchorSwitchWallet.kt */
/* loaded from: classes2.dex */
public final class AnchorSwitchWallet$show$1$1 extends Lambda implements tc1<List<? extends Wallet>, te4> {
    public final /* synthetic */ Wallet $activeWallet;
    public final /* synthetic */ Context $context;
    public final /* synthetic */ LayoutInflater $inflater;
    public final /* synthetic */ jn0 $this_run;
    public final /* synthetic */ AnchorSwitchWallet this$0;

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public AnchorSwitchWallet$show$1$1(Context context, LayoutInflater layoutInflater, jn0 jn0Var, Wallet wallet2, AnchorSwitchWallet anchorSwitchWallet) {
        super(1);
        this.$context = context;
        this.$inflater = layoutInflater;
        this.$this_run = jn0Var;
        this.$activeWallet = wallet2;
        this.this$0 = anchorSwitchWallet;
    }

    public static final void b(AnchorSwitchWallet anchorSwitchWallet, Wallet wallet2, Context context, View view) {
        MultiWalletViewModel multiWalletViewModel;
        int i;
        fs1.f(anchorSwitchWallet, "this$0");
        fs1.f(wallet2, "$it");
        fs1.f(context, "$context");
        multiWalletViewModel = anchorSwitchWallet.a;
        multiWalletViewModel.A(wallet2);
        anchorSwitchWallet.f();
        i = anchorSwitchWallet.b;
        AKTHomeActivity.S0(context, Integer.valueOf(i));
    }

    @Override // defpackage.tc1
    public /* bridge */ /* synthetic */ te4 invoke(List<? extends Wallet> list) {
        invoke2((List<Wallet>) list);
        return te4.a;
    }

    /* renamed from: invoke  reason: avoid collision after fix types in other method */
    public final void invoke2(List<Wallet> list) {
        int i;
        fs1.f(list, "originList");
        if (!do3.a.c(this.$context) && list.size() != 1) {
            ArrayList arrayList = new ArrayList();
            for (Object obj : list) {
                if (!((Wallet) obj).isPrimaryWallet()) {
                    arrayList.add(obj);
                }
            }
            list = arrayList;
        }
        try {
            LayoutInflater layoutInflater = this.$inflater;
            jn0 jn0Var = this.$this_run;
            Wallet wallet2 = this.$activeWallet;
            final Context context = this.$context;
            final AnchorSwitchWallet anchorSwitchWallet = this.this$0;
            for (final Wallet wallet3 : list) {
                ht1 c = ht1.c(layoutInflater, jn0Var.a, false);
                c.b.setText(wallet3.displayName());
                if (fs1.b(wallet2 == null ? null : wallet2.getId(), wallet3.getId())) {
                    MaterialButton materialButton = c.b;
                    materialButton.setTypeface(materialButton.getTypeface(), 1);
                    MaterialButton materialButton2 = c.b;
                    i = anchorSwitchWallet.d;
                    materialButton2.setTextColor(m70.d(context, i));
                    c.b.setClickable(false);
                } else {
                    c.b.setTextColor(m70.d(context, R.color.t5));
                    c.b().setOnClickListener(new View.OnClickListener() { // from class: rc
                        @Override // android.view.View.OnClickListener
                        public final void onClick(View view) {
                            AnchorSwitchWallet$show$1$1.b(AnchorSwitchWallet.this, wallet3, context, view);
                        }
                    });
                }
                jn0Var.a.addView(c.b());
            }
        } catch (Exception unused) {
        }
    }
}
