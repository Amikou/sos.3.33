package net.safemoon.androidwallet.dialogs;

import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewParent;
import android.widget.CompoundButton;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentViewModelLazyKt;
import com.google.android.material.bottomsheet.BottomSheetBehavior;
import com.google.gson.Gson;
import com.trustwallet.walletconnect.models.WCPeerMeta;
import java.util.Objects;
import kotlin.text.StringsKt__StringsKt;
import net.safemoon.androidwallet.R;
import net.safemoon.androidwallet.common.TokenType;
import net.safemoon.androidwallet.dialogs.WalletConnectInterfaceFragment;
import net.safemoon.androidwallet.dialogs.common.BaseBottomSheetDialogFragment;
import net.safemoon.androidwallet.model.wallets.Wallet;
import net.safemoon.androidwallet.viewmodels.WalletConnectViewModel;

/* compiled from: WalletConnectInterfaceFragment.kt */
/* loaded from: classes2.dex */
public final class WalletConnectInterfaceFragment extends BaseBottomSheetDialogFragment {
    public static final a B0 = new a(null);
    public b A0;
    public final sy1 w0 = FragmentViewModelLazyKt.a(this, d53.b(WalletConnectViewModel.class), new WalletConnectInterfaceFragment$special$$inlined$viewModels$default$1(new WalletConnectInterfaceFragment$walletConnectVM$2(this)), null);
    public final sy1 x0 = zy1.a(new WalletConnectInterfaceFragment$peer$2(this));
    public final sy1 y0 = zy1.a(new WalletConnectInterfaceFragment$defaultChainId$2(this));
    public final sy1 z0 = zy1.a(new WalletConnectInterfaceFragment$binding$2(this));

    /* compiled from: WalletConnectInterfaceFragment.kt */
    /* loaded from: classes2.dex */
    public static final class a {
        public a() {
        }

        public /* synthetic */ a(qi0 qi0Var) {
            this();
        }

        public final WalletConnectInterfaceFragment a(WCPeerMeta wCPeerMeta, String str) {
            fs1.f(wCPeerMeta, "peer");
            WalletConnectInterfaceFragment walletConnectInterfaceFragment = new WalletConnectInterfaceFragment();
            Bundle bundle = new Bundle();
            bundle.putString("ARG_PEER", new Gson().toJson(wCPeerMeta));
            bundle.putString("ARG_DEFAULT_CHAIN_ID", str);
            te4 te4Var = te4.a;
            walletConnectInterfaceFragment.setArguments(bundle);
            return walletConnectInterfaceFragment;
        }
    }

    /* compiled from: WalletConnectInterfaceFragment.kt */
    /* loaded from: classes2.dex */
    public interface b {
        void c(TokenType tokenType);

        void d();
    }

    public static final void N(WalletConnectInterfaceFragment walletConnectInterfaceFragment, View view) {
        fs1.f(walletConnectInterfaceFragment, "this$0");
        walletConnectInterfaceFragment.h();
    }

    public static final void O(WalletConnectInterfaceFragment walletConnectInterfaceFragment, TokenType tokenType) {
        fs1.f(walletConnectInterfaceFragment, "this$0");
        if (tokenType == null) {
            return;
        }
        com.bumptech.glide.a.u(walletConnectInterfaceFragment.K().b).w(Integer.valueOf(tokenType.getIcon())).a(n73.v0()).I0(walletConnectInterfaceFragment.K().b);
    }

    public static final void P(WalletConnectInterfaceFragment walletConnectInterfaceFragment, View view) {
        b bVar;
        fs1.f(walletConnectInterfaceFragment, "this$0");
        TokenType value = walletConnectInterfaceFragment.M().e().getValue();
        if (value == null || (bVar = walletConnectInterfaceFragment.A0) == null) {
            return;
        }
        bVar.c(value);
    }

    public static final void Q(WalletConnectInterfaceFragment walletConnectInterfaceFragment, CompoundButton compoundButton, boolean z) {
        fs1.f(walletConnectInterfaceFragment, "this$0");
        walletConnectInterfaceFragment.M().g().postValue(Boolean.valueOf(z));
    }

    public static final void R(WalletConnectInterfaceFragment walletConnectInterfaceFragment, View view) {
        fs1.f(walletConnectInterfaceFragment, "this$0");
        b bVar = walletConnectInterfaceFragment.A0;
        if (bVar == null) {
            return;
        }
        bVar.d();
    }

    public final sb1 K() {
        return (sb1) this.z0.getValue();
    }

    public final WCPeerMeta L() {
        return (WCPeerMeta) this.x0.getValue();
    }

    public final WalletConnectViewModel M() {
        return (WalletConnectViewModel) this.w0.getValue();
    }

    public final void S(FragmentManager fragmentManager) {
        fs1.f(fragmentManager, "manager");
        super.u(fragmentManager, WalletConnectInterfaceFragment.class.getCanonicalName());
    }

    @Override // defpackage.sn0, androidx.fragment.app.Fragment
    public void onAttach(Context context) {
        fs1.f(context, "context");
        super.onAttach(context);
        if (requireParentFragment() instanceof b) {
            this.A0 = (b) requireParentFragment();
        }
    }

    @Override // defpackage.sn0, androidx.fragment.app.Fragment
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
    }

    @Override // androidx.fragment.app.Fragment
    public View onCreateView(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle) {
        fs1.f(layoutInflater, "inflater");
        return layoutInflater.inflate(R.layout.fragment_wallet_connect_interface, viewGroup, false);
    }

    @Override // defpackage.sn0, androidx.fragment.app.Fragment
    public void onDetach() {
        super.onDetach();
        this.A0 = null;
    }

    @Override // net.safemoon.androidwallet.dialogs.common.BaseBottomSheetDialogFragment, defpackage.sn0, androidx.fragment.app.Fragment
    public void onStart() {
        super.onStart();
        ViewParent parent = requireView().getParent();
        Objects.requireNonNull(parent, "null cannot be cast to non-null type android.view.View");
        BottomSheetBehavior y = BottomSheetBehavior.y((View) parent);
        fs1.e(y, "from(requireView().parent as View)");
        y.V(3);
    }

    @Override // androidx.fragment.app.Fragment
    public void onViewCreated(View view, Bundle bundle) {
        fs1.f(view, "view");
        super.onViewCreated(view, bundle);
        sb1 K = K();
        K.h.c.setText(getString(R.string.screen_title_wallet_connect));
        K.h.a.setOnClickListener(new View.OnClickListener() { // from class: dm4
            @Override // android.view.View.OnClickListener
            public final void onClick(View view2) {
                WalletConnectInterfaceFragment.N(WalletConnectInterfaceFragment.this, view2);
            }
        });
        sb1 K2 = K();
        K2.g.setText(getString(R.string.wc_title_interface, L().getName()));
        K2.d.setText(L().getUrl());
        if (!L().getIcons().isEmpty()) {
            String str = L().getIcons().get(0);
            int f = bo3.f(requireActivity().getApplicationContext(), "MODE_NIGHT", 1);
            if (StringsKt__StringsKt.M(str, "swap.safemoon.com", false, 2, null) && f == 1) {
                str = "https://safemoon.com/img/logo_dark.png";
            }
            com.bumptech.glide.a.u(K().c).y(str).a(n73.v0()).I0(K().c);
        }
        Context requireContext = requireContext();
        fs1.e(requireContext, "requireContext()");
        Wallet c = e30.c(requireContext);
        if (c != null) {
            K2.j.setText(c.displayName());
            K2.i.setText(e30.d(c.getAddress()));
        }
        M().e().observe(getViewLifecycleOwner(), new tl2() { // from class: cm4
            @Override // defpackage.tl2
            public final void onChanged(Object obj) {
                WalletConnectInterfaceFragment.O(WalletConnectInterfaceFragment.this, (TokenType) obj);
            }
        });
        K2.a.setOnClickListener(new View.OnClickListener() { // from class: em4
            @Override // android.view.View.OnClickListener
            public final void onClick(View view2) {
                WalletConnectInterfaceFragment.P(WalletConnectInterfaceFragment.this, view2);
            }
        });
        K2.f.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() { // from class: gm4
            @Override // android.widget.CompoundButton.OnCheckedChangeListener
            public final void onCheckedChanged(CompoundButton compoundButton, boolean z) {
                WalletConnectInterfaceFragment.Q(WalletConnectInterfaceFragment.this, compoundButton, z);
            }
        });
        K2.e.setOnClickListener(new View.OnClickListener() { // from class: fm4
            @Override // android.view.View.OnClickListener
            public final void onClick(View view2) {
                WalletConnectInterfaceFragment.R(WalletConnectInterfaceFragment.this, view2);
            }
        });
    }
}
