package net.safemoon.androidwallet.dialogs;

import android.app.Dialog;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.text.Editable;
import android.text.InputFilter;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.CompoundButton;
import android.widget.RadioButton;
import android.widget.TextView;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentViewModelLazyKt;
import com.github.mikephil.charting.utils.Utils;
import com.google.android.material.textfield.TextInputEditText;
import net.safemoon.androidwallet.R;
import net.safemoon.androidwallet.dialogs.SwapSlipPage;
import net.safemoon.androidwallet.viewmodels.SwapViewModel;

/* compiled from: SwapSlipPage.kt */
/* loaded from: classes2.dex */
public final class SwapSlipPage extends sn0 {
    public static final a w0 = new a(null);
    public bo0 u0;
    public final sy1 v0 = FragmentViewModelLazyKt.a(this, d53.b(SwapViewModel.class), new SwapSlipPage$special$$inlined$viewModels$default$1(new SwapSlipPage$swapViewModel$2(this)), null);

    /* compiled from: SwapSlipPage.kt */
    /* loaded from: classes2.dex */
    public static final class a {
        public a() {
        }

        public /* synthetic */ a(qi0 qi0Var) {
            this();
        }

        public final SwapSlipPage a() {
            SwapSlipPage swapSlipPage = new SwapSlipPage();
            Bundle bundle = new Bundle();
            te4 te4Var = te4.a;
            swapSlipPage.setArguments(bundle);
            return swapSlipPage;
        }
    }

    /* compiled from: SwapSlipPage.kt */
    /* loaded from: classes2.dex */
    public static final class b implements tl2<Double> {
        public b() {
        }

        @Override // defpackage.tl2
        /* renamed from: a */
        public void onChanged(Double d) {
            SwapSlipPage.this.F().w0().removeObserver(this);
            SwapSlipPage swapSlipPage = SwapSlipPage.this;
            if (fs1.a(d, 0.1d)) {
                bo0 bo0Var = swapSlipPage.u0;
                if (bo0Var == null) {
                    fs1.r("binding");
                    bo0Var = null;
                }
                bo0Var.b.setChecked(true);
            } else if (fs1.a(d, 0.5d)) {
                bo0 bo0Var2 = swapSlipPage.u0;
                if (bo0Var2 == null) {
                    fs1.r("binding");
                    bo0Var2 = null;
                }
                bo0Var2.c.setChecked(true);
            } else if (fs1.a(d, 1.0d)) {
                bo0 bo0Var3 = swapSlipPage.u0;
                if (bo0Var3 == null) {
                    fs1.r("binding");
                    bo0Var3 = null;
                }
                bo0Var3.d.setChecked(true);
            }
            bo0 bo0Var4 = swapSlipPage.u0;
            if (bo0Var4 == null) {
                fs1.r("binding");
                bo0Var4 = null;
            }
            bo0Var4.f.setText(d != null ? e30.n(d.doubleValue()) : null);
        }
    }

    /* compiled from: SwapSlipPage.kt */
    /* loaded from: classes2.dex */
    public static final class c extends cj2 {
        /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
        public c(TextInputEditText textInputEditText) {
            super(textInputEditText);
            fs1.e(textInputEditText, "edtSlippageManually");
        }
    }

    public static final void G(SwapSlipPage swapSlipPage, View view) {
        double K;
        fs1.f(swapSlipPage, "this$0");
        bo0 bo0Var = swapSlipPage.u0;
        if (bo0Var == null) {
            fs1.r("binding");
            bo0Var = null;
        }
        Editable text = bo0Var.f.getText();
        gb2<Double> w02 = swapSlipPage.F().w0();
        if (text != null) {
            if (text.length() == 0) {
                K = Utils.DOUBLE_EPSILON;
                w02.setValue(Double.valueOf(Math.min(100.0d, Math.max(0.1d, K))));
                swapSlipPage.h();
            }
        }
        K = e30.K(String.valueOf(text));
        w02.setValue(Double.valueOf(Math.min(100.0d, Math.max(0.1d, K))));
        swapSlipPage.h();
    }

    public static final boolean H(SwapSlipPage swapSlipPage, TextView textView, int i, KeyEvent keyEvent) {
        fs1.f(swapSlipPage, "this$0");
        if (i == 6) {
            swapSlipPage.h();
            return true;
        }
        return false;
    }

    public static final void I(SwapSlipPage swapSlipPage, CompoundButton compoundButton, boolean z) {
        fs1.f(swapSlipPage, "this$0");
        bo3.n(swapSlipPage.requireContext(), "SWAP_PAIR_DONT_SHOW_ME", Boolean.valueOf(z));
    }

    public static final void J(SwapSlipPage swapSlipPage, View view) {
        fs1.f(swapSlipPage, "this$0");
        bo0 bo0Var = swapSlipPage.u0;
        if (bo0Var == null) {
            fs1.r("binding");
            bo0Var = null;
        }
        TextInputEditText textInputEditText = bo0Var.f;
        StringBuilder sb = new StringBuilder();
        sb.append('0');
        sb.append(b30.a.y());
        sb.append('1');
        textInputEditText.setText(sb.toString());
    }

    public static final void K(SwapSlipPage swapSlipPage, View view) {
        fs1.f(swapSlipPage, "this$0");
        bo0 bo0Var = swapSlipPage.u0;
        if (bo0Var == null) {
            fs1.r("binding");
            bo0Var = null;
        }
        TextInputEditText textInputEditText = bo0Var.f;
        StringBuilder sb = new StringBuilder();
        sb.append('0');
        sb.append(b30.a.y());
        sb.append('5');
        textInputEditText.setText(sb.toString());
    }

    public static final void L(SwapSlipPage swapSlipPage, View view) {
        fs1.f(swapSlipPage, "this$0");
        bo0 bo0Var = swapSlipPage.u0;
        if (bo0Var == null) {
            fs1.r("binding");
            bo0Var = null;
        }
        bo0Var.f.setText("1");
    }

    public static final void M(SwapSlipPage swapSlipPage, View view) {
        fs1.f(swapSlipPage, "this$0");
        swapSlipPage.h();
    }

    public static final void N(SwapSlipPage swapSlipPage, View view) {
        fs1.f(swapSlipPage, "this$0");
        d14 a2 = d14.v0.a();
        FragmentManager childFragmentManager = swapSlipPage.getChildFragmentManager();
        fs1.e(childFragmentManager, "childFragmentManager");
        a2.x(childFragmentManager);
    }

    public final SwapViewModel F() {
        return (SwapViewModel) this.v0.getValue();
    }

    public final void O(FragmentManager fragmentManager) {
        fs1.f(fragmentManager, "manager");
        super.u(fragmentManager, SwapSlipPage.class.getCanonicalName());
    }

    @Override // defpackage.sn0
    public Dialog m(Bundle bundle) {
        Dialog m = super.m(bundle);
        fs1.e(m, "super.onCreateDialog(savedInstanceState)");
        m.requestWindowFeature(1);
        return m;
    }

    @Override // androidx.fragment.app.Fragment
    public View onCreateView(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle) {
        fs1.f(layoutInflater, "inflater");
        return LayoutInflater.from(requireContext()).inflate(R.layout.dialog_swap_slippage, viewGroup, false);
    }

    @Override // defpackage.sn0, androidx.fragment.app.Fragment
    public void onStart() {
        super.onStart();
        Dialog k = k();
        if (k == null) {
            return;
        }
        Window window = k.getWindow();
        if (window != null) {
            window.setBackgroundDrawable(new ColorDrawable(0));
        }
        Window window2 = k.getWindow();
        if (window2 == null) {
            return;
        }
        window2.setLayout(-1, -2);
    }

    @Override // androidx.fragment.app.Fragment
    public void onViewCreated(View view, Bundle bundle) {
        fs1.f(view, "view");
        super.onViewCreated(view, bundle);
        bo0 a2 = bo0.a(view);
        fs1.e(a2, "bind(view)");
        this.u0 = a2;
        F().w0().observe(getViewLifecycleOwner(), new b());
        bo0 bo0Var = this.u0;
        bo0 bo0Var2 = null;
        if (bo0Var == null) {
            fs1.r("binding");
            bo0Var = null;
        }
        bo0Var.f.setFilters(new InputFilter[]{new xq1(Double.valueOf((double) Utils.DOUBLE_EPSILON), Double.valueOf(100.0d))});
        bo0 bo0Var3 = this.u0;
        if (bo0Var3 == null) {
            fs1.r("binding");
            bo0Var3 = null;
        }
        TextInputEditText textInputEditText = bo0Var3.f;
        bo0 bo0Var4 = this.u0;
        if (bo0Var4 == null) {
            fs1.r("binding");
            bo0Var4 = null;
        }
        textInputEditText.addTextChangedListener(new c(bo0Var4.f));
        bo0 bo0Var5 = this.u0;
        if (bo0Var5 == null) {
            fs1.r("binding");
            bo0Var5 = null;
        }
        bo0Var5.a.setOnClickListener(new View.OnClickListener() { // from class: i14
            @Override // android.view.View.OnClickListener
            public final void onClick(View view2) {
                SwapSlipPage.G(SwapSlipPage.this, view2);
            }
        });
        bo0 bo0Var6 = this.u0;
        if (bo0Var6 == null) {
            fs1.r("binding");
            bo0Var6 = null;
        }
        TextInputEditText textInputEditText2 = bo0Var6.f;
        b30 b30Var = b30.a;
        textInputEditText2.setHint(getString(R.string.hint_0, Character.valueOf(b30Var.y())));
        bo0 bo0Var7 = this.u0;
        if (bo0Var7 == null) {
            fs1.r("binding");
            bo0Var7 = null;
        }
        bo0Var7.f.setOnEditorActionListener(new TextView.OnEditorActionListener() { // from class: l14
            @Override // android.widget.TextView.OnEditorActionListener
            public final boolean onEditorAction(TextView textView, int i, KeyEvent keyEvent) {
                boolean H;
                H = SwapSlipPage.H(SwapSlipPage.this, textView, i, keyEvent);
                return H;
            }
        });
        bo0 bo0Var8 = this.u0;
        if (bo0Var8 == null) {
            fs1.r("binding");
            bo0Var8 = null;
        }
        RadioButton radioButton = bo0Var8.b;
        radioButton.setText(getString(R.string.chip_slippage_tolerance_0_1, Character.valueOf(b30Var.y())));
        radioButton.setOnClickListener(new View.OnClickListener() { // from class: f14
            @Override // android.view.View.OnClickListener
            public final void onClick(View view2) {
                SwapSlipPage.J(SwapSlipPage.this, view2);
            }
        });
        bo0 bo0Var9 = this.u0;
        if (bo0Var9 == null) {
            fs1.r("binding");
            bo0Var9 = null;
        }
        RadioButton radioButton2 = bo0Var9.c;
        radioButton2.setText(getString(R.string.chip_slippage_tolerance_0_5, Character.valueOf(b30Var.y())));
        radioButton2.setOnClickListener(new View.OnClickListener() { // from class: e14
            @Override // android.view.View.OnClickListener
            public final void onClick(View view2) {
                SwapSlipPage.K(SwapSlipPage.this, view2);
            }
        });
        bo0 bo0Var10 = this.u0;
        if (bo0Var10 == null) {
            fs1.r("binding");
            bo0Var10 = null;
        }
        RadioButton radioButton3 = bo0Var10.d;
        radioButton3.setText(getString(R.string.chip_slippage_tolerance_1));
        radioButton3.setOnClickListener(new View.OnClickListener() { // from class: g14
            @Override // android.view.View.OnClickListener
            public final void onClick(View view2) {
                SwapSlipPage.L(SwapSlipPage.this, view2);
            }
        });
        bo0 bo0Var11 = this.u0;
        if (bo0Var11 == null) {
            fs1.r("binding");
            bo0Var11 = null;
        }
        bo0Var11.e.setOnClickListener(new View.OnClickListener() { // from class: h14
            @Override // android.view.View.OnClickListener
            public final void onClick(View view2) {
                SwapSlipPage.M(SwapSlipPage.this, view2);
            }
        });
        bo0 bo0Var12 = this.u0;
        if (bo0Var12 == null) {
            fs1.r("binding");
            bo0Var12 = null;
        }
        bo0Var12.g.setOnClickListener(new View.OnClickListener() { // from class: j14
            @Override // android.view.View.OnClickListener
            public final void onClick(View view2) {
                SwapSlipPage.N(SwapSlipPage.this, view2);
            }
        });
        boolean e = bo3.e(requireContext(), "SWAP_PAIR_DONT_SHOW_ME", true);
        bo0 bo0Var13 = this.u0;
        if (bo0Var13 == null) {
            fs1.r("binding");
            bo0Var13 = null;
        }
        bo0Var13.h.setChecked(e);
        bo0 bo0Var14 = this.u0;
        if (bo0Var14 == null) {
            fs1.r("binding");
        } else {
            bo0Var2 = bo0Var14;
        }
        bo0Var2.h.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() { // from class: k14
            @Override // android.widget.CompoundButton.OnCheckedChangeListener
            public final void onCheckedChanged(CompoundButton compoundButton, boolean z) {
                SwapSlipPage.I(SwapSlipPage.this, compoundButton, z);
            }
        });
    }
}
