package net.safemoon.androidwallet.dialogs;

import android.app.Dialog;
import android.content.ClipData;
import android.content.ClipboardManager;
import android.content.Context;
import android.content.DialogInterface;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentViewModelLazyKt;
import com.google.android.material.textview.MaterialTextView;
import net.safemoon.androidwallet.R;
import net.safemoon.androidwallet.dialogs.G2FAVerfication;
import net.safemoon.androidwallet.viewmodels.GoogleAuthViewModel;

/* compiled from: G2FAVerfication.kt */
/* loaded from: classes2.dex */
public final class G2FAVerfication extends sn0 {
    public static final a C0 = new a(null);
    public final gb2<Long> A0;
    public CountDownTimer B0;
    public final boolean u0;
    public final sy1 v0;
    public nb1 w0;
    public final sy1 x0;
    public boolean y0;
    public b z0;

    /* compiled from: G2FAVerfication.kt */
    /* loaded from: classes2.dex */
    public static final class a {
        public a() {
        }

        public /* synthetic */ a(qi0 qi0Var) {
            this();
        }

        public static /* synthetic */ G2FAVerfication b(a aVar, b bVar, boolean z, int i, Object obj) {
            if ((i & 2) != 0) {
                z = false;
            }
            return aVar.a(bVar, z);
        }

        public final G2FAVerfication a(b bVar, boolean z) {
            fs1.f(bVar, "verificationCallback");
            G2FAVerfication g2FAVerfication = new G2FAVerfication(z);
            g2FAVerfication.z0 = bVar;
            return g2FAVerfication;
        }
    }

    /* compiled from: G2FAVerfication.kt */
    /* loaded from: classes2.dex */
    public interface b {
        void a();

        void onSuccess();
    }

    /* compiled from: G2FAVerfication.kt */
    /* loaded from: classes2.dex */
    public static final class c implements TextWatcher {
        public c() {
        }

        @Override // android.text.TextWatcher
        public void afterTextChanged(Editable editable) {
            boolean z = false;
            if (editable != null && editable.length() == 6) {
                z = true;
            }
            if (z) {
                G2FAVerfication.this.I();
            }
        }

        @Override // android.text.TextWatcher
        public void beforeTextChanged(CharSequence charSequence, int i, int i2, int i3) {
        }

        @Override // android.text.TextWatcher
        public void onTextChanged(CharSequence charSequence, int i, int i2, int i3) {
            nb1 nb1Var = G2FAVerfication.this.w0;
            if (nb1Var == null) {
                fs1.r("binding");
                nb1Var = null;
            }
            nb1Var.c.setError(null);
        }
    }

    /* compiled from: G2FAVerfication.kt */
    /* loaded from: classes2.dex */
    public static final class d extends CountDownTimer {
        public d() {
            super(30000L, 1000L);
        }

        @Override // android.os.CountDownTimer
        public void onFinish() {
            if (G2FAVerfication.this.isVisible()) {
                if (G2FAVerfication.this.isStateSaved()) {
                    G2FAVerfication.this.i();
                } else {
                    G2FAVerfication.this.h();
                }
            }
        }

        @Override // android.os.CountDownTimer
        public void onTick(long j) {
            G2FAVerfication.this.A0.setValue(Long.valueOf(j / 1000));
        }
    }

    public G2FAVerfication() {
        this(false, 1, null);
    }

    public G2FAVerfication(boolean z) {
        this.u0 = z;
        this.v0 = zy1.a(new G2FAVerfication$manager$2(this));
        this.x0 = FragmentViewModelLazyKt.a(this, d53.b(GoogleAuthViewModel.class), new G2FAVerfication$special$$inlined$activityViewModels$default$1(this), new G2FAVerfication$special$$inlined$activityViewModels$default$2(this));
        this.A0 = new gb2<>(30L);
    }

    public static final void E(G2FAVerfication g2FAVerfication, View view) {
        fs1.f(g2FAVerfication, "this$0");
        Dialog k = g2FAVerfication.k();
        if (k == null) {
            return;
        }
        k.dismiss();
    }

    public static final void F(G2FAVerfication g2FAVerfication, View view) {
        fs1.f(g2FAVerfication, "this$0");
        ClipData primaryClip = g2FAVerfication.D().getPrimaryClip();
        if (primaryClip == null || primaryClip.getItemCount() <= 0 || primaryClip.getItemAt(0).getText() == null) {
            return;
        }
        String obj = primaryClip.getItemAt(0).getText().toString();
        if (obj.length() == 6) {
            nb1 nb1Var = g2FAVerfication.w0;
            if (nb1Var == null) {
                fs1.r("binding");
                nb1Var = null;
            }
            nb1Var.c.setText(obj);
        }
    }

    public static final void G(G2FAVerfication g2FAVerfication, Long l) {
        fs1.f(g2FAVerfication, "this$0");
        nb1 nb1Var = g2FAVerfication.w0;
        if (nb1Var == null) {
            fs1.r("binding");
            nb1Var = null;
        }
        MaterialTextView materialTextView = nb1Var.d;
        materialTextView.setText(':' + l + " sec.");
    }

    public final GoogleAuthViewModel C() {
        return (GoogleAuthViewModel) this.x0.getValue();
    }

    public final ClipboardManager D() {
        return (ClipboardManager) this.v0.getValue();
    }

    public final void H(FragmentManager fragmentManager) {
        fs1.f(fragmentManager, "manager");
        super.u(fragmentManager, G2FAVerfication.class.getCanonicalName());
    }

    public final void I() {
        nb1 nb1Var = this.w0;
        b bVar = null;
        if (nb1Var == null) {
            fs1.r("binding");
            nb1Var = null;
        }
        if (fs1.b(nb1Var.c.getEditableText().toString(), C().h(bo3.i(requireContext(), "AUTH_2FA_KEY")))) {
            this.y0 = true;
            Dialog k = k();
            if (k != null) {
                k.dismiss();
            }
            b bVar2 = this.z0;
            if (bVar2 == null) {
                fs1.r("verificationCallback");
            } else {
                bVar = bVar2;
            }
            bVar.onSuccess();
            return;
        }
        this.y0 = false;
        nb1 nb1Var2 = this.w0;
        if (nb1Var2 == null) {
            fs1.r("binding");
            nb1Var2 = null;
        }
        nb1Var2.c.setError(getString(R.string.auth_2fa_error));
        b bVar3 = this.z0;
        if (bVar3 == null) {
            fs1.r("verificationCallback");
        } else {
            bVar = bVar3;
        }
        bVar.a();
    }

    @Override // defpackage.sn0, androidx.fragment.app.Fragment
    public void onAttach(Context context) {
        fs1.f(context, "context");
        super.onAttach(context);
        pg4.b(requireActivity(), Boolean.TRUE);
    }

    @Override // defpackage.sn0, android.content.DialogInterface.OnCancelListener
    public void onCancel(DialogInterface dialogInterface) {
        fs1.f(dialogInterface, "dialog");
        super.onCancel(dialogInterface);
        if (this.B0 == null) {
            fs1.r("countDownTimer");
        }
        CountDownTimer countDownTimer = this.B0;
        if (countDownTimer == null) {
            fs1.r("countDownTimer");
            countDownTimer = null;
        }
        countDownTimer.cancel();
    }

    @Override // androidx.fragment.app.Fragment
    public View onCreateView(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle) {
        fs1.f(layoutInflater, "inflater");
        return layoutInflater.inflate(R.layout.fragment_verficationg2fa, viewGroup, false);
    }

    @Override // androidx.fragment.app.Fragment
    public void onDestroy() {
        super.onDestroy();
        if (this.B0 == null) {
            fs1.r("countDownTimer");
        }
        CountDownTimer countDownTimer = this.B0;
        if (countDownTimer == null) {
            fs1.r("countDownTimer");
            countDownTimer = null;
        }
        countDownTimer.cancel();
    }

    @Override // defpackage.sn0, androidx.fragment.app.Fragment
    public void onDetach() {
        super.onDetach();
        if (this.u0 && this.y0) {
            return;
        }
        pg4.b(requireActivity(), Boolean.FALSE);
    }

    @Override // defpackage.sn0, androidx.fragment.app.Fragment
    public void onStart() {
        Window window;
        super.onStart();
        Dialog k = k();
        if (k == null || (window = k.getWindow()) == null) {
            return;
        }
        window.setBackgroundDrawable(new ColorDrawable(0));
        window.setLayout(-1, -2);
    }

    @Override // androidx.fragment.app.Fragment
    public void onViewCreated(View view, Bundle bundle) {
        fs1.f(view, "view");
        super.onViewCreated(view, bundle);
        nb1 a2 = nb1.a(view);
        fs1.e(a2, "bind(view)");
        this.w0 = a2;
        nb1 nb1Var = null;
        if (a2 == null) {
            fs1.r("binding");
            a2 = null;
        }
        a2.b.setOnClickListener(new View.OnClickListener() { // from class: fe1
            @Override // android.view.View.OnClickListener
            public final void onClick(View view2) {
                G2FAVerfication.E(G2FAVerfication.this, view2);
            }
        });
        nb1 nb1Var2 = this.w0;
        if (nb1Var2 == null) {
            fs1.r("binding");
            nb1Var2 = null;
        }
        nb1Var2.a.setOnClickListener(new View.OnClickListener() { // from class: ee1
            @Override // android.view.View.OnClickListener
            public final void onClick(View view2) {
                G2FAVerfication.F(G2FAVerfication.this, view2);
            }
        });
        nb1 nb1Var3 = this.w0;
        if (nb1Var3 == null) {
            fs1.r("binding");
        } else {
            nb1Var = nb1Var3;
        }
        nb1Var.c.addTextChangedListener(new c());
        this.A0.observe(getViewLifecycleOwner(), new tl2() { // from class: de1
            @Override // defpackage.tl2
            public final void onChanged(Object obj) {
                G2FAVerfication.G(G2FAVerfication.this, (Long) obj);
            }
        });
        d dVar = new d();
        this.B0 = dVar;
        dVar.start();
    }

    public /* synthetic */ G2FAVerfication(boolean z, int i, qi0 qi0Var) {
        this((i & 1) != 0 ? false : z);
    }
}
