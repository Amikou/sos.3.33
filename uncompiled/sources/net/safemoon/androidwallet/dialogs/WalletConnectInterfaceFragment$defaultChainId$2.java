package net.safemoon.androidwallet.dialogs;

import kotlin.jvm.internal.Lambda;

/* compiled from: WalletConnectInterfaceFragment.kt */
/* loaded from: classes2.dex */
public final class WalletConnectInterfaceFragment$defaultChainId$2 extends Lambda implements rc1<String> {
    public final /* synthetic */ WalletConnectInterfaceFragment this$0;

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public WalletConnectInterfaceFragment$defaultChainId$2(WalletConnectInterfaceFragment walletConnectInterfaceFragment) {
        super(0);
        this.this$0 = walletConnectInterfaceFragment;
    }

    @Override // defpackage.rc1
    public final String invoke() {
        return this.this$0.requireArguments().getString("ARG_DEFAULT_CHAIN_ID", null);
    }
}
