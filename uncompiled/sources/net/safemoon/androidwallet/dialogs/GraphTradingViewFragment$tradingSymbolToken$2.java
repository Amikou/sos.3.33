package net.safemoon.androidwallet.dialogs;

import java.io.Serializable;
import kotlin.jvm.internal.Lambda;
import net.safemoon.androidwallet.model.graph.TradingViewSymbol;

/* compiled from: GraphTradingViewFragment.kt */
/* loaded from: classes2.dex */
public final class GraphTradingViewFragment$tradingSymbolToken$2 extends Lambda implements rc1<TradingViewSymbol> {
    public final /* synthetic */ GraphTradingViewFragment this$0;

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public GraphTradingViewFragment$tradingSymbolToken$2(GraphTradingViewFragment graphTradingViewFragment) {
        super(0);
        this.this$0 = graphTradingViewFragment;
    }

    @Override // defpackage.rc1
    public final TradingViewSymbol invoke() {
        Serializable serializable = this.this$0.requireArguments().getSerializable("argTradingView");
        if (serializable == null) {
            serializable = new TradingViewSymbol();
        }
        return (TradingViewSymbol) serializable;
    }
}
