package net.safemoon.androidwallet.dialogs;

import android.os.Bundle;
import kotlin.jvm.internal.Lambda;

/* compiled from: ProgressLoading.kt */
/* loaded from: classes2.dex */
public final class ProgressLoading$message$2 extends Lambda implements rc1<String> {
    public final /* synthetic */ ProgressLoading this$0;

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public ProgressLoading$message$2(ProgressLoading progressLoading) {
        super(0);
        this.this$0 = progressLoading;
    }

    @Override // defpackage.rc1
    public final String invoke() {
        String string;
        Bundle arguments = this.this$0.getArguments();
        return (arguments == null || (string = arguments.getString("MSG", "")) == null) ? "" : string;
    }
}
