package net.safemoon.androidwallet.dialogs;

import android.app.Dialog;
import kotlin.jvm.internal.Lambda;
import net.safemoon.androidwallet.model.priceAlert.PriceAlertToken;
import net.safemoon.androidwallet.viewmodels.CryptoPriceAlertViewModel;

/* compiled from: CryptoPriceAlertFragment.kt */
/* loaded from: classes2.dex */
public final class CryptoPriceAlertFragment$onViewCreated$1$7$1$1$1 extends Lambda implements hd1<gt, Dialog, te4> {
    public final /* synthetic */ Integer $index;
    public final /* synthetic */ PriceAlertToken $it;
    public final /* synthetic */ CryptoPriceAlertFragment this$0;

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public CryptoPriceAlertFragment$onViewCreated$1$7$1$1$1(CryptoPriceAlertFragment cryptoPriceAlertFragment, PriceAlertToken priceAlertToken, Integer num) {
        super(2);
        this.this$0 = cryptoPriceAlertFragment;
        this.$it = priceAlertToken;
        this.$index = num;
    }

    /* renamed from: invoke  reason: avoid collision after fix types in other method */
    public final void invoke2(gt gtVar, Dialog dialog) {
        CryptoPriceAlertViewModel N;
        CryptoPriceAlertViewModel N2;
        fs1.f(gtVar, "ccButton");
        fs1.f(dialog, "dialog");
        int a = gtVar.a();
        if (a == 100) {
            N = this.this$0.N();
            PriceAlertToken priceAlertToken = this.$it;
            fs1.e(priceAlertToken, "it");
            Integer num = this.$index;
            fs1.e(num, "index");
            N.k(priceAlertToken, num.intValue());
        } else if (a == 101) {
            N2 = this.this$0.N();
            PriceAlertToken priceAlertToken2 = this.$it;
            fs1.e(priceAlertToken2, "it");
            N2.j(priceAlertToken2);
        }
        dialog.dismiss();
    }

    @Override // defpackage.hd1
    public /* bridge */ /* synthetic */ te4 invoke(gt gtVar, Dialog dialog) {
        invoke2(gtVar, dialog);
        return te4.a;
    }
}
