package net.safemoon.androidwallet.dialogs;

import androidx.fragment.app.FragmentActivity;
import kotlin.jvm.internal.Lambda;

/* compiled from: ConfirmSwapTransactionFragment.kt */
/* loaded from: classes2.dex */
public final class ConfirmSwapTransactionFragment$swapViewModel$2 extends Lambda implements rc1<hj4> {
    public final /* synthetic */ ConfirmSwapTransactionFragment this$0;

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public ConfirmSwapTransactionFragment$swapViewModel$2(ConfirmSwapTransactionFragment confirmSwapTransactionFragment) {
        super(0);
        this.this$0 = confirmSwapTransactionFragment;
    }

    /* JADX WARN: Can't rename method to resolve collision */
    @Override // defpackage.rc1
    public final hj4 invoke() {
        FragmentActivity requireActivity = this.this$0.requireActivity();
        fs1.e(requireActivity, "requireActivity()");
        return requireActivity;
    }
}
