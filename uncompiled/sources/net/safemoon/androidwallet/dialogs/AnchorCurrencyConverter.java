package net.safemoon.androidwallet.dialogs;

import android.content.Context;
import android.text.Editable;
import android.text.InputFilter;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AnimationUtils;
import android.widget.PopupWindow;
import androidx.recyclerview.widget.LinearLayoutManager;
import com.github.mikephil.charting.utils.Utils;
import com.google.android.material.textfield.TextInputEditText;
import defpackage.u21;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import net.safemoon.androidwallet.R;
import net.safemoon.androidwallet.dialogs.AnchorCurrencyConverter;
import net.safemoon.androidwallet.dialogs.AnchorCurrencyConverter$defaultCurrencyAdapter$2;
import net.safemoon.androidwallet.model.common.HistoryListItem;
import net.safemoon.androidwallet.model.fiat.gson.Fiat;
import net.safemoon.androidwallet.model.fiat.room.RoomFiat;
import net.safemoon.androidwallet.viewmodels.SelectFiatViewModel;
import net.safemoon.androidwallet.views.CurrencyConverterLayout;
import net.safemoon.androidwallet.views.editText.autoSize.AutofitEdittext;

/* compiled from: AnchorCurrencyConverter.kt */
/* loaded from: classes2.dex */
public final class AnchorCurrencyConverter {
    public final Context a;
    public final SelectFiatViewModel b;
    public PopupWindow c;
    public dn0 d;
    public Fiat e;
    public Fiat f;
    public final List<HistoryListItem> g;
    public final sy1 h;
    public boolean i;

    /* compiled from: TextView.kt */
    /* loaded from: classes2.dex */
    public static final class a implements TextWatcher {
        public final /* synthetic */ AutofitEdittext a;
        public final /* synthetic */ AnchorCurrencyConverter f0;
        public final /* synthetic */ dn0 g0;

        public a(AutofitEdittext autofitEdittext, AnchorCurrencyConverter anchorCurrencyConverter, dn0 dn0Var) {
            this.a = autofitEdittext;
            this.f0 = anchorCurrencyConverter;
            this.g0 = dn0Var;
        }

        @Override // android.text.TextWatcher
        public void afterTextChanged(Editable editable) {
            if (this.a.hasFocus()) {
                AnchorCurrencyConverter anchorCurrencyConverter = this.f0;
                CurrencyConverterLayout currencyConverterLayout = this.g0.b;
                fs1.e(currencyConverterLayout, "convertFrom");
                CurrencyConverterLayout currencyConverterLayout2 = this.g0.c;
                fs1.e(currencyConverterLayout2, "convertTo");
                anchorCurrencyConverter.r(currencyConverterLayout, currencyConverterLayout2, editable, true);
            }
        }

        @Override // android.text.TextWatcher
        public void beforeTextChanged(CharSequence charSequence, int i, int i2, int i3) {
        }

        @Override // android.text.TextWatcher
        public void onTextChanged(CharSequence charSequence, int i, int i2, int i3) {
        }
    }

    /* compiled from: TextView.kt */
    /* loaded from: classes2.dex */
    public static final class b implements TextWatcher {
        public final /* synthetic */ AutofitEdittext a;
        public final /* synthetic */ AnchorCurrencyConverter f0;
        public final /* synthetic */ dn0 g0;

        public b(AutofitEdittext autofitEdittext, AnchorCurrencyConverter anchorCurrencyConverter, dn0 dn0Var) {
            this.a = autofitEdittext;
            this.f0 = anchorCurrencyConverter;
            this.g0 = dn0Var;
        }

        @Override // android.text.TextWatcher
        public void afterTextChanged(Editable editable) {
            if (this.a.hasFocus()) {
                AnchorCurrencyConverter anchorCurrencyConverter = this.f0;
                CurrencyConverterLayout currencyConverterLayout = this.g0.c;
                fs1.e(currencyConverterLayout, "convertTo");
                CurrencyConverterLayout currencyConverterLayout2 = this.g0.b;
                fs1.e(currencyConverterLayout2, "convertFrom");
                anchorCurrencyConverter.r(currencyConverterLayout, currencyConverterLayout2, editable, false);
            }
        }

        @Override // android.text.TextWatcher
        public void beforeTextChanged(CharSequence charSequence, int i, int i2, int i3) {
        }

        @Override // android.text.TextWatcher
        public void onTextChanged(CharSequence charSequence, int i, int i2, int i3) {
        }
    }

    /* compiled from: TextView.kt */
    /* loaded from: classes2.dex */
    public static final class c implements TextWatcher {
        public c() {
        }

        @Override // android.text.TextWatcher
        public void afterTextChanged(Editable editable) {
        }

        @Override // android.text.TextWatcher
        public void beforeTextChanged(CharSequence charSequence, int i, int i2, int i3) {
        }

        @Override // android.text.TextWatcher
        public void onTextChanged(CharSequence charSequence, int i, int i2, int i3) {
            String obj;
            if (charSequence == null || (obj = charSequence.toString()) == null) {
                return;
            }
            if (AnchorCurrencyConverter.this.i) {
                AnchorCurrencyConverter anchorCurrencyConverter = AnchorCurrencyConverter.this;
                anchorCurrencyConverter.s(new RoomFiat(anchorCurrencyConverter.f), AnchorCurrencyConverter.this.w().k(obj));
                return;
            }
            AnchorCurrencyConverter anchorCurrencyConverter2 = AnchorCurrencyConverter.this;
            anchorCurrencyConverter2.s(new RoomFiat(anchorCurrencyConverter2.e), AnchorCurrencyConverter.this.w().k(obj));
        }
    }

    public AnchorCurrencyConverter(Context context, SelectFiatViewModel selectFiatViewModel, View view, final rc1<te4> rc1Var) {
        fs1.f(context, "context");
        fs1.f(selectFiatViewModel, "selectFiatViewModel");
        fs1.f(rc1Var, "onDismissListener");
        this.a = context;
        this.b = selectFiatViewModel;
        u21.a aVar = u21.a;
        this.e = aVar.a();
        this.f = aVar.a();
        this.g = new ArrayList();
        this.h = zy1.a(new AnchorCurrencyConverter$defaultCurrencyAdapter$2(this));
        this.i = true;
        Object systemService = context.getSystemService("layout_inflater");
        Objects.requireNonNull(systemService, "null cannot be cast to non-null type android.view.LayoutInflater");
        View inflate = ((LayoutInflater) systemService).inflate(R.layout.dialog_anchor_currency_converter, (ViewGroup) null);
        this.d = dn0.a(inflate);
        fs1.e(inflate, "view");
        PopupWindow q = q(inflate, view);
        this.c = q;
        if (q != null) {
            q.setOnDismissListener(new PopupWindow.OnDismissListener() { // from class: cc
                @Override // android.widget.PopupWindow.OnDismissListener
                public final void onDismiss() {
                    AnchorCurrencyConverter.d(rc1.this);
                }
            });
        }
        final dn0 dn0Var = this.d;
        fs1.d(dn0Var);
        dn0Var.a.setInAnimation(AnimationUtils.loadAnimation(u(), 17432578));
        dn0Var.a.setOutAnimation(AnimationUtils.loadAnimation(u(), 17432579));
        dn0Var.f.setLayoutManager(new LinearLayoutManager(u(), 1, false));
        dn0Var.f.setAdapter(v());
        TextInputEditText textInputEditText = dn0Var.g.b;
        fs1.e(textInputEditText, "searchBar.etSearch");
        textInputEditText.addTextChangedListener(new c());
        dn0Var.d.setOnClickListener(new View.OnClickListener() { // from class: ac
            @Override // android.view.View.OnClickListener
            public final void onClick(View view2) {
                AnchorCurrencyConverter.z(dn0.this, view2);
            }
        });
        AutofitEdittext editText = dn0Var.b.getEditText();
        AutofitEdittext editText2 = dn0Var.c.getEditText();
        editText.setFilters(new InputFilter[]{new xq1(Double.valueOf((double) Utils.DOUBLE_EPSILON), Double.valueOf(Double.MAX_VALUE))});
        editText.addTextChangedListener(new cj2(dn0Var.b.getEditText()));
        editText.addTextChangedListener(new a(editText, this, dn0Var));
        dn0Var.b.l(new AnchorCurrencyConverter$2$4(this, dn0Var));
        dn0Var.b.k(new AnchorCurrencyConverter$2$5(this, editText, dn0Var, editText2));
        dn0Var.b.setItem(aVar.a());
        this.e = aVar.a();
        editText2.setFilters(new InputFilter[]{new xq1(Double.valueOf((double) Utils.DOUBLE_EPSILON), Double.valueOf(Double.MAX_VALUE))});
        editText2.addTextChangedListener(new cj2(dn0Var.c.getEditText()));
        editText2.addTextChangedListener(new b(editText2, this, dn0Var));
        dn0Var.c.l(new AnchorCurrencyConverter$2$7(this, dn0Var));
        dn0Var.c.k(new AnchorCurrencyConverter$2$8(this, editText2, dn0Var, editText));
        RoomFiat j = w().j();
        if (j != null) {
            dn0Var.c.setItem(j);
            dn0Var.c.setOtherSideItem(aVar.a());
            dn0Var.b.setOtherSideItem(j);
            this.f = new Fiat(j.getSymbol(), j.getName(), j.getRate());
        }
        dn0Var.e.setOnClickListener(new View.OnClickListener() { // from class: bc
            @Override // android.view.View.OnClickListener
            public final void onClick(View view2) {
                AnchorCurrencyConverter.y(dn0.this, this, view2);
            }
        });
    }

    public static final void d(rc1 rc1Var) {
        fs1.f(rc1Var, "$onDismissListener");
        rc1Var.invoke();
    }

    /* JADX WARN: Multi-variable type inference failed */
    public static /* synthetic */ void t(AnchorCurrencyConverter anchorCurrencyConverter, RoomFiat roomFiat, List list, int i, Object obj) {
        if ((i & 2) != 0) {
            list = anchorCurrencyConverter.g;
        }
        anchorCurrencyConverter.s(roomFiat, list);
    }

    public static final void y(dn0 dn0Var, AnchorCurrencyConverter anchorCurrencyConverter, View view) {
        fs1.f(dn0Var, "$this_with");
        fs1.f(anchorCurrencyConverter, "this$0");
        RoomFiat selectedItem = dn0Var.b.getSelectedItem();
        RoomFiat selectedItem2 = dn0Var.c.getSelectedItem();
        if (selectedItem2 != null) {
            dn0Var.b.setItem(selectedItem2);
            dn0Var.c.m(selectedItem2);
        }
        if (selectedItem != null) {
            dn0Var.c.setItem(selectedItem);
            dn0Var.b.m(selectedItem);
        }
        anchorCurrencyConverter.x();
    }

    public static final void z(dn0 dn0Var, View view) {
        fs1.f(dn0Var, "$this_with");
        dn0Var.a.showPrevious();
    }

    public final void A(List<HistoryListItem> list) {
        fs1.f(list, "items");
        this.g.clear();
        this.g.addAll(list);
        v().d(this.g);
    }

    public final void B() {
        CurrencyConverterLayout currencyConverterLayout;
        CurrencyConverterLayout currencyConverterLayout2;
        dn0 dn0Var = this.d;
        if (dn0Var != null && (currencyConverterLayout2 = dn0Var.b) != null) {
            currencyConverterLayout2.setItem(this.e);
        }
        dn0 dn0Var2 = this.d;
        if (dn0Var2 != null && (currencyConverterLayout = dn0Var2.c) != null) {
            currencyConverterLayout.m(new RoomFiat(this.e));
        }
        x();
    }

    public final void C() {
        CurrencyConverterLayout currencyConverterLayout;
        CurrencyConverterLayout currencyConverterLayout2;
        dn0 dn0Var = this.d;
        if (dn0Var != null && (currencyConverterLayout2 = dn0Var.c) != null) {
            currencyConverterLayout2.setItem(this.f);
        }
        dn0 dn0Var2 = this.d;
        if (dn0Var2 != null && (currencyConverterLayout = dn0Var2.b) != null) {
            currencyConverterLayout.m(new RoomFiat(this.f));
        }
        x();
    }

    public final AnchorCurrencyConverter D(View view) {
        fs1.f(view, "anchorView");
        PopupWindow popupWindow = this.c;
        if (popupWindow != null) {
            popupWindow.showAsDropDown(view);
        }
        return this;
    }

    public final PopupWindow q(View view, View view2) {
        PopupWindow popupWindow = new PopupWindow(view, (int) ((view2 == null ? -1 : view2.getWidth()) * 0.8d), (int) t40.a(400));
        popupWindow.setOutsideTouchable(true);
        popupWindow.setFocusable(true);
        popupWindow.setInputMethodMode(1);
        popupWindow.setAttachedInDecor(false);
        return popupWindow;
    }

    public final void r(CurrencyConverterLayout currencyConverterLayout, CurrencyConverterLayout currencyConverterLayout2, Editable editable, boolean z) {
        CurrencyConverterLayout currencyConverterLayout3;
        AutofitEdittext editText;
        CurrencyConverterLayout currencyConverterLayout4;
        AutofitEdittext editText2;
        CurrencyConverterLayout currencyConverterLayout5;
        AutofitEdittext editText3;
        CurrencyConverterLayout currencyConverterLayout6;
        AutofitEdittext editText4;
        RoomFiat selectedItem = currencyConverterLayout.getSelectedItem();
        RoomFiat selectedItem2 = currencyConverterLayout2.getSelectedItem();
        if (editable != null) {
            if (!(editable.length() > 0)) {
                if (z) {
                    dn0 dn0Var = this.d;
                    if (dn0Var == null || (currencyConverterLayout4 = dn0Var.c) == null || (editText2 = currencyConverterLayout4.getEditText()) == null) {
                        return;
                    }
                    editText2.setText("");
                    return;
                }
                dn0 dn0Var2 = this.d;
                if (dn0Var2 == null || (currencyConverterLayout3 = dn0Var2.b) == null || (editText = currencyConverterLayout3.getEditText()) == null) {
                    return;
                }
                editText.setText("");
                return;
            }
            double K = e30.K(editable.toString());
            if ((selectedItem == null ? null : selectedItem.getRate()) != null) {
                if ((selectedItem2 != null ? selectedItem2.getRate() : null) != null) {
                    double doubleValue = (K / selectedItem.getRate().doubleValue()) * selectedItem2.getRate().doubleValue();
                    if (z) {
                        dn0 dn0Var3 = this.d;
                        if (dn0Var3 == null || (currencyConverterLayout6 = dn0Var3.c) == null || (editText4 = currencyConverterLayout6.getEditText()) == null) {
                            return;
                        }
                        editText4.setText(e30.j(doubleValue));
                        return;
                    }
                    dn0 dn0Var4 = this.d;
                    if (dn0Var4 == null || (currencyConverterLayout5 = dn0Var4.b) == null || (editText3 = currencyConverterLayout5.getEditText()) == null) {
                        return;
                    }
                    editText3.setText(e30.j(doubleValue));
                }
            }
        }
    }

    /* JADX WARN: Code restructure failed: missing block: B:16:0x003b, code lost:
        if (defpackage.dv3.u(r2 == null ? null : r2.getSymbol(), r8.getSymbol(), false, 2, null) == false) goto L22;
     */
    /* JADX WARN: Removed duplicated region for block: B:23:0x0040 A[SYNTHETIC] */
    /* JADX WARN: Removed duplicated region for block: B:25:0x0009 A[SYNTHETIC] */
    /*
        Code decompiled incorrectly, please refer to instructions dump.
        To view partially-correct code enable 'Show inconsistent code' option in preferences
    */
    public final void s(net.safemoon.androidwallet.model.fiat.room.RoomFiat r8, java.util.List<net.safemoon.androidwallet.model.common.HistoryListItem> r9) {
        /*
            r7 = this;
            java.util.ArrayList r0 = new java.util.ArrayList
            r0.<init>()
            java.util.Iterator r9 = r9.iterator()
        L9:
            boolean r1 = r9.hasNext()
            if (r1 == 0) goto L44
            java.lang.Object r1 = r9.next()
            r2 = r1
            net.safemoon.androidwallet.model.common.HistoryListItem r2 = (net.safemoon.androidwallet.model.common.HistoryListItem) r2
            boolean r3 = r2 instanceof net.safemoon.androidwallet.model.common.FooterHistoryItem
            r4 = 0
            if (r3 != 0) goto L3d
            boolean r3 = r2 instanceof net.safemoon.androidwallet.model.common.HeaderItemHistory
            if (r3 != 0) goto L3d
            boolean r3 = r2 instanceof net.safemoon.androidwallet.model.fiat.ResultItemFiat
            if (r3 == 0) goto L3e
            net.safemoon.androidwallet.model.fiat.ResultItemFiat r2 = (net.safemoon.androidwallet.model.fiat.ResultItemFiat) r2
            net.safemoon.androidwallet.model.fiat.room.RoomFiat r2 = r2.getResult()
            r3 = 0
            if (r2 != 0) goto L2e
            r2 = r3
            goto L32
        L2e:
            java.lang.String r2 = r2.getSymbol()
        L32:
            java.lang.String r5 = r8.getSymbol()
            r6 = 2
            boolean r2 = defpackage.dv3.u(r2, r5, r4, r6, r3)
            if (r2 != 0) goto L3e
        L3d:
            r4 = 1
        L3e:
            if (r4 == 0) goto L9
            r0.add(r1)
            goto L9
        L44:
            net.safemoon.androidwallet.dialogs.AnchorCurrencyConverter$defaultCurrencyAdapter$2$a r8 = r7.v()
            java.util.List r9 = defpackage.j20.m0(r0)
            r8.d(r9)
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: net.safemoon.androidwallet.dialogs.AnchorCurrencyConverter.s(net.safemoon.androidwallet.model.fiat.room.RoomFiat, java.util.List):void");
    }

    public final Context u() {
        return this.a;
    }

    public final AnchorCurrencyConverter$defaultCurrencyAdapter$2.a v() {
        return (AnchorCurrencyConverter$defaultCurrencyAdapter$2.a) this.h.getValue();
    }

    public final SelectFiatViewModel w() {
        return this.b;
    }

    public final void x() {
        CurrencyConverterLayout currencyConverterLayout;
        CurrencyConverterLayout currencyConverterLayout2;
        dn0 dn0Var = this.d;
        AutofitEdittext autofitEdittext = null;
        AutofitEdittext editText = (dn0Var == null || (currencyConverterLayout = dn0Var.b) == null) ? null : currencyConverterLayout.getEditText();
        if (editText != null) {
            editText.setText(editText.getText());
        }
        dn0 dn0Var2 = this.d;
        if (dn0Var2 != null && (currencyConverterLayout2 = dn0Var2.c) != null) {
            autofitEdittext = currencyConverterLayout2.getEditText();
        }
        if (autofitEdittext == null) {
            return;
        }
        autofitEdittext.setText(autofitEdittext.getText());
    }
}
