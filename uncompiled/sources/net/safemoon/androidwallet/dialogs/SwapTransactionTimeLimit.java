package net.safemoon.androidwallet.dialogs;

import android.app.Dialog;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.text.Editable;
import android.text.InputFilter;
import android.text.TextWatcher;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.TextView;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentViewModelLazyKt;
import net.safemoon.androidwallet.R;
import net.safemoon.androidwallet.dialogs.SwapTransactionTimeLimit;
import net.safemoon.androidwallet.viewmodels.SwapViewModel;

/* compiled from: SwapTransactionTimeLimit.kt */
/* loaded from: classes2.dex */
public final class SwapTransactionTimeLimit extends sn0 {
    public static final a w0 = new a(null);
    public ho0 u0;
    public final sy1 v0 = FragmentViewModelLazyKt.a(this, d53.b(SwapViewModel.class), new SwapTransactionTimeLimit$special$$inlined$viewModels$default$1(new SwapTransactionTimeLimit$swapViewModel$2(this)), null);

    /* compiled from: SwapTransactionTimeLimit.kt */
    /* loaded from: classes2.dex */
    public static final class a {
        public a() {
        }

        public /* synthetic */ a(qi0 qi0Var) {
            this();
        }

        public final SwapTransactionTimeLimit a() {
            SwapTransactionTimeLimit swapTransactionTimeLimit = new SwapTransactionTimeLimit();
            Bundle bundle = new Bundle();
            te4 te4Var = te4.a;
            swapTransactionTimeLimit.setArguments(bundle);
            return swapTransactionTimeLimit;
        }
    }

    /* compiled from: SwapTransactionTimeLimit.kt */
    /* loaded from: classes2.dex */
    public static final class b implements tl2<Integer> {
        public b() {
        }

        @Override // defpackage.tl2
        /* renamed from: a */
        public void onChanged(Integer num) {
            SwapTransactionTimeLimit.this.A().Y().removeObserver(this);
            ho0 ho0Var = SwapTransactionTimeLimit.this.u0;
            if (ho0Var == null) {
                fs1.r("binding");
                ho0Var = null;
            }
            ho0Var.c.setText(String.valueOf(num));
        }
    }

    /* compiled from: SwapTransactionTimeLimit.kt */
    /* loaded from: classes2.dex */
    public static final class c implements TextWatcher {
        @Override // android.text.TextWatcher
        public void afterTextChanged(Editable editable) {
        }

        @Override // android.text.TextWatcher
        public void beforeTextChanged(CharSequence charSequence, int i, int i2, int i3) {
        }

        @Override // android.text.TextWatcher
        public void onTextChanged(CharSequence charSequence, int i, int i2, int i3) {
        }
    }

    public static final boolean B(SwapTransactionTimeLimit swapTransactionTimeLimit, TextView textView, int i, KeyEvent keyEvent) {
        fs1.f(swapTransactionTimeLimit, "this$0");
        if (i == 6) {
            ho0 ho0Var = swapTransactionTimeLimit.u0;
            if (ho0Var == null) {
                fs1.r("binding");
                ho0Var = null;
            }
            ho0Var.a.performClick();
            return true;
        }
        return false;
    }

    public static final void C(SwapTransactionTimeLimit swapTransactionTimeLimit, View view) {
        fs1.f(swapTransactionTimeLimit, "this$0");
        swapTransactionTimeLimit.h();
    }

    public static final void D(SwapTransactionTimeLimit swapTransactionTimeLimit, View view) {
        int valueOf;
        fs1.f(swapTransactionTimeLimit, "this$0");
        ho0 ho0Var = swapTransactionTimeLimit.u0;
        if (ho0Var == null) {
            fs1.r("binding");
            ho0Var = null;
        }
        Editable text = ho0Var.c.getText();
        gb2<Integer> Y = swapTransactionTimeLimit.A().Y();
        if (text != null) {
            if (text.length() == 0) {
                valueOf = 0;
                Y.setValue(valueOf);
                swapTransactionTimeLimit.h();
            }
        }
        valueOf = Integer.valueOf(Integer.parseInt(String.valueOf(text)));
        Y.setValue(valueOf);
        swapTransactionTimeLimit.h();
    }

    public final SwapViewModel A() {
        return (SwapViewModel) this.v0.getValue();
    }

    public final void E(FragmentManager fragmentManager) {
        fs1.f(fragmentManager, "manager");
        super.u(fragmentManager, SwapTransactionTimeLimit.class.getCanonicalName());
    }

    @Override // defpackage.sn0
    public Dialog m(Bundle bundle) {
        Dialog m = super.m(bundle);
        fs1.e(m, "super.onCreateDialog(savedInstanceState)");
        m.requestWindowFeature(1);
        return m;
    }

    @Override // androidx.fragment.app.Fragment
    public View onCreateView(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle) {
        fs1.f(layoutInflater, "inflater");
        return LayoutInflater.from(requireContext()).inflate(R.layout.dialog_swap_transaction_limit, viewGroup, false);
    }

    @Override // defpackage.sn0, androidx.fragment.app.Fragment
    public void onStart() {
        super.onStart();
        Dialog k = k();
        if (k == null) {
            return;
        }
        Window window = k.getWindow();
        if (window != null) {
            window.setBackgroundDrawable(new ColorDrawable(0));
        }
        Window window2 = k.getWindow();
        if (window2 == null) {
            return;
        }
        window2.setLayout(-1, -2);
    }

    @Override // androidx.fragment.app.Fragment
    public void onViewCreated(View view, Bundle bundle) {
        fs1.f(view, "view");
        super.onViewCreated(view, bundle);
        ho0 a2 = ho0.a(view);
        fs1.e(a2, "bind(view)");
        this.u0 = a2;
        A().Y().observe(getViewLifecycleOwner(), new b());
        ho0 ho0Var = this.u0;
        ho0 ho0Var2 = null;
        if (ho0Var == null) {
            fs1.r("binding");
            ho0Var = null;
        }
        ho0Var.c.setFilters(new InputFilter[]{new xq1("1", "60")});
        ho0 ho0Var3 = this.u0;
        if (ho0Var3 == null) {
            fs1.r("binding");
            ho0Var3 = null;
        }
        ho0Var3.c.addTextChangedListener(new c());
        ho0 ho0Var4 = this.u0;
        if (ho0Var4 == null) {
            fs1.r("binding");
            ho0Var4 = null;
        }
        ho0Var4.c.setOnEditorActionListener(new TextView.OnEditorActionListener() { // from class: s14
            @Override // android.widget.TextView.OnEditorActionListener
            public final boolean onEditorAction(TextView textView, int i, KeyEvent keyEvent) {
                boolean B;
                B = SwapTransactionTimeLimit.B(SwapTransactionTimeLimit.this, textView, i, keyEvent);
                return B;
            }
        });
        ho0 ho0Var5 = this.u0;
        if (ho0Var5 == null) {
            fs1.r("binding");
            ho0Var5 = null;
        }
        ho0Var5.b.setOnClickListener(new View.OnClickListener() { // from class: r14
            @Override // android.view.View.OnClickListener
            public final void onClick(View view2) {
                SwapTransactionTimeLimit.C(SwapTransactionTimeLimit.this, view2);
            }
        });
        ho0 ho0Var6 = this.u0;
        if (ho0Var6 == null) {
            fs1.r("binding");
        } else {
            ho0Var2 = ho0Var6;
        }
        ho0Var2.a.setOnClickListener(new View.OnClickListener() { // from class: q14
            @Override // android.view.View.OnClickListener
            public final void onClick(View view2) {
                SwapTransactionTimeLimit.D(SwapTransactionTimeLimit.this, view2);
            }
        });
    }
}
