package net.safemoon.androidwallet.dialogs;

import android.content.Context;
import kotlin.jvm.internal.Lambda;
import net.safemoon.androidwallet.provider.TokenInfoExistProvider;

/* compiled from: GraphFragment.kt */
/* loaded from: classes2.dex */
public final class GraphFragment$tokenInfoExistProvider$2 extends Lambda implements rc1<TokenInfoExistProvider> {
    public final /* synthetic */ GraphFragment this$0;

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public GraphFragment$tokenInfoExistProvider$2(GraphFragment graphFragment) {
        super(0);
        this.this$0 = graphFragment;
    }

    @Override // defpackage.rc1
    public final TokenInfoExistProvider invoke() {
        Context requireContext = this.this$0.requireContext();
        fs1.e(requireContext, "requireContext()");
        return new TokenInfoExistProvider(requireContext);
    }
}
