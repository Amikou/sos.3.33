package net.safemoon.androidwallet.dialogs;

import kotlin.jvm.internal.Lambda;

/* compiled from: PaySendWyreBottomDialog.kt */
/* loaded from: classes2.dex */
public final class PaySendWyreBottomDialog$currencyCode$2 extends Lambda implements rc1<String> {
    public final /* synthetic */ PaySendWyreBottomDialog this$0;

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public PaySendWyreBottomDialog$currencyCode$2(PaySendWyreBottomDialog paySendWyreBottomDialog) {
        super(0);
        this.this$0 = paySendWyreBottomDialog;
    }

    @Override // defpackage.rc1
    public final String invoke() {
        return this.this$0.requireArguments().getString("ARG_CURRENCY_CODE", "");
    }
}
