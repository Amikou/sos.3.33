package net.safemoon.androidwallet.dialogs;

import android.app.Dialog;
import android.content.ClipData;
import android.content.ClipboardManager;
import android.content.Context;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentViewModelLazyKt;
import net.safemoon.androidwallet.R;
import net.safemoon.androidwallet.dialogs.Google2FAVerfiedAuthFragment;
import net.safemoon.androidwallet.viewmodels.GoogleAuthViewModel;

/* compiled from: Google2FAVerfiedAuthFragment.kt */
/* loaded from: classes2.dex */
public final class Google2FAVerfiedAuthFragment extends sn0 {
    public static final a y0 = new a(null);
    public fa1 w0;
    public final sy1 u0 = zy1.a(new Google2FAVerfiedAuthFragment$manager$2(this));
    public final sy1 v0 = zy1.a(new Google2FAVerfiedAuthFragment$iVerfied$2(this));
    public final sy1 x0 = FragmentViewModelLazyKt.a(this, d53.b(GoogleAuthViewModel.class), new Google2FAVerfiedAuthFragment$special$$inlined$activityViewModels$default$1(this), new Google2FAVerfiedAuthFragment$special$$inlined$activityViewModels$default$2(this));

    /* compiled from: Google2FAVerfiedAuthFragment.kt */
    /* loaded from: classes2.dex */
    public static final class a {
        public a() {
        }

        public /* synthetic */ a(qi0 qi0Var) {
            this();
        }

        public final Google2FAVerfiedAuthFragment a() {
            return new Google2FAVerfiedAuthFragment();
        }
    }

    /* compiled from: Google2FAVerfiedAuthFragment.kt */
    /* loaded from: classes2.dex */
    public static final class b implements TextWatcher {
        public b() {
        }

        @Override // android.text.TextWatcher
        public void afterTextChanged(Editable editable) {
            fa1 fa1Var = Google2FAVerfiedAuthFragment.this.w0;
            if (fa1Var == null) {
                fs1.r("binding");
                fa1Var = null;
            }
            fa1Var.e.setText("");
        }

        @Override // android.text.TextWatcher
        public void beforeTextChanged(CharSequence charSequence, int i, int i2, int i3) {
        }

        @Override // android.text.TextWatcher
        public void onTextChanged(CharSequence charSequence, int i, int i2, int i3) {
        }
    }

    public static final void D(Google2FAVerfiedAuthFragment google2FAVerfiedAuthFragment, View view) {
        fs1.f(google2FAVerfiedAuthFragment, "this$0");
        Dialog k = google2FAVerfiedAuthFragment.k();
        if (k == null) {
            return;
        }
        k.dismiss();
    }

    public static final void E(Google2FAVerfiedAuthFragment google2FAVerfiedAuthFragment, View view) {
        fs1.f(google2FAVerfiedAuthFragment, "this$0");
        ClipData primaryClip = google2FAVerfiedAuthFragment.C().getPrimaryClip();
        if (primaryClip == null || primaryClip.getItemCount() <= 0 || primaryClip.getItemAt(0).getText() == null) {
            return;
        }
        String obj = primaryClip.getItemAt(0).getText().toString();
        if (obj.length() == 6) {
            fa1 fa1Var = google2FAVerfiedAuthFragment.w0;
            if (fa1Var == null) {
                fs1.r("binding");
                fa1Var = null;
            }
            fa1Var.d.setText(obj);
        }
    }

    public static final void F(Google2FAVerfiedAuthFragment google2FAVerfiedAuthFragment, View view) {
        fs1.f(google2FAVerfiedAuthFragment, "this$0");
        fa1 fa1Var = google2FAVerfiedAuthFragment.w0;
        fa1 fa1Var2 = null;
        if (fa1Var == null) {
            fs1.r("binding");
            fa1Var = null;
        }
        if (fa1Var.d.getEditableText().length() != 6) {
            fa1 fa1Var3 = google2FAVerfiedAuthFragment.w0;
            if (fa1Var3 == null) {
                fs1.r("binding");
            } else {
                fa1Var2 = fa1Var3;
            }
            fa1Var2.e.setText(google2FAVerfiedAuthFragment.getString(R.string.invalid_input));
            return;
        }
        fa1 fa1Var4 = google2FAVerfiedAuthFragment.w0;
        if (fa1Var4 == null) {
            fs1.r("binding");
            fa1Var4 = null;
        }
        if (fs1.b(fa1Var4.d.getEditableText().toString(), google2FAVerfiedAuthFragment.A().h(google2FAVerfiedAuthFragment.A().g().getValue()))) {
            bo3.o(google2FAVerfiedAuthFragment.requireContext(), "AUTH_2FA_KEY", google2FAVerfiedAuthFragment.A().g().getValue());
            Context requireContext = google2FAVerfiedAuthFragment.requireContext();
            Boolean bool = Boolean.TRUE;
            bo3.n(requireContext, "AUTH_2FA_IS_ENABLE", bool);
            google2FAVerfiedAuthFragment.A().e().setValue(bool);
            cn1 B = google2FAVerfiedAuthFragment.B();
            if (B != null) {
                B.b();
            }
            google2FAVerfiedAuthFragment.h();
            return;
        }
        fa1 fa1Var5 = google2FAVerfiedAuthFragment.w0;
        if (fa1Var5 == null) {
            fs1.r("binding");
        } else {
            fa1Var2 = fa1Var5;
        }
        fa1Var2.e.setText(google2FAVerfiedAuthFragment.getString(R.string.auth_2fa_error));
    }

    public static final void G(Google2FAVerfiedAuthFragment google2FAVerfiedAuthFragment, View view) {
        fs1.f(google2FAVerfiedAuthFragment, "this$0");
        Google2FABackup a2 = Google2FABackup.w0.a();
        FragmentManager childFragmentManager = google2FAVerfiedAuthFragment.getChildFragmentManager();
        fs1.e(childFragmentManager, "childFragmentManager");
        a2.G(childFragmentManager);
    }

    public final GoogleAuthViewModel A() {
        return (GoogleAuthViewModel) this.x0.getValue();
    }

    public final cn1 B() {
        return (cn1) this.v0.getValue();
    }

    public final ClipboardManager C() {
        return (ClipboardManager) this.u0.getValue();
    }

    public final void H(FragmentManager fragmentManager) {
        fs1.f(fragmentManager, "manager");
        super.u(fragmentManager, Google2FAVerfiedAuthFragment.class.getCanonicalName());
    }

    @Override // defpackage.sn0, androidx.fragment.app.Fragment
    public void onAttach(Context context) {
        fs1.f(context, "context");
        super.onAttach(context);
    }

    @Override // defpackage.sn0, androidx.fragment.app.Fragment
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
    }

    @Override // androidx.fragment.app.Fragment
    public View onCreateView(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle) {
        fs1.f(layoutInflater, "inflater");
        return layoutInflater.inflate(R.layout.fragment_google2fa_verfied_auth, viewGroup, false);
    }

    @Override // defpackage.sn0, androidx.fragment.app.Fragment
    public void onDetach() {
        super.onDetach();
    }

    @Override // defpackage.sn0, androidx.fragment.app.Fragment
    public void onStart() {
        Window window;
        super.onStart();
        Dialog k = k();
        if (k == null || (window = k.getWindow()) == null) {
            return;
        }
        window.setBackgroundDrawable(new ColorDrawable(0));
        window.setLayout(-1, -2);
    }

    @Override // androidx.fragment.app.Fragment
    public void onViewCreated(View view, Bundle bundle) {
        fs1.f(view, "view");
        super.onViewCreated(view, bundle);
        fa1 a2 = fa1.a(view);
        fs1.e(a2, "bind(view)");
        this.w0 = a2;
        fa1 fa1Var = null;
        if (a2 == null) {
            fs1.r("binding");
            a2 = null;
        }
        a2.c.setOnClickListener(new View.OnClickListener() { // from class: zg1
            @Override // android.view.View.OnClickListener
            public final void onClick(View view2) {
                Google2FAVerfiedAuthFragment.D(Google2FAVerfiedAuthFragment.this, view2);
            }
        });
        fa1 fa1Var2 = this.w0;
        if (fa1Var2 == null) {
            fs1.r("binding");
            fa1Var2 = null;
        }
        fa1Var2.b.setOnClickListener(new View.OnClickListener() { // from class: ch1
            @Override // android.view.View.OnClickListener
            public final void onClick(View view2) {
                Google2FAVerfiedAuthFragment.E(Google2FAVerfiedAuthFragment.this, view2);
            }
        });
        fa1 fa1Var3 = this.w0;
        if (fa1Var3 == null) {
            fs1.r("binding");
            fa1Var3 = null;
        }
        fa1Var3.a.setOnClickListener(new View.OnClickListener() { // from class: bh1
            @Override // android.view.View.OnClickListener
            public final void onClick(View view2) {
                Google2FAVerfiedAuthFragment.F(Google2FAVerfiedAuthFragment.this, view2);
            }
        });
        fa1 fa1Var4 = this.w0;
        if (fa1Var4 == null) {
            fs1.r("binding");
            fa1Var4 = null;
        }
        fa1Var4.f.setOnClickListener(new View.OnClickListener() { // from class: ah1
            @Override // android.view.View.OnClickListener
            public final void onClick(View view2) {
                Google2FAVerfiedAuthFragment.G(Google2FAVerfiedAuthFragment.this, view2);
            }
        });
        fa1 fa1Var5 = this.w0;
        if (fa1Var5 == null) {
            fs1.r("binding");
        } else {
            fa1Var = fa1Var5;
        }
        fa1Var.d.addTextChangedListener(new b());
    }
}
