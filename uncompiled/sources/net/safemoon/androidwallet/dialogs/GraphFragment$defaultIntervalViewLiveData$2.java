package net.safemoon.androidwallet.dialogs;

import android.view.View;
import kotlin.jvm.internal.Lambda;

/* compiled from: GraphFragment.kt */
/* loaded from: classes2.dex */
public final class GraphFragment$defaultIntervalViewLiveData$2 extends Lambda implements rc1<gb2<View>> {
    public final /* synthetic */ GraphFragment this$0;

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public GraphFragment$defaultIntervalViewLiveData$2(GraphFragment graphFragment) {
        super(0);
        this.this$0 = graphFragment;
    }

    /* JADX WARN: Can't rename method to resolve collision */
    @Override // defpackage.rc1
    public final gb2<View> invoke() {
        return new gb2<>(this.this$0.requireView().findViewById(this.this$0.requireArguments().getInt("DEFAULT_VIEW", 0)));
    }
}
