package net.safemoon.androidwallet.dialogs;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.PopupWindow;
import java.util.Objects;
import net.safemoon.androidwallet.R;
import net.safemoon.androidwallet.viewmodels.MultiWalletViewModel;

/* compiled from: AnchorSwitchWallet.kt */
/* loaded from: classes2.dex */
public final class AnchorSwitchWallet {
    public final MultiWalletViewModel a;
    public final int b;
    public PopupWindow c;
    public final int d;

    public AnchorSwitchWallet(MultiWalletViewModel multiWalletViewModel, int i) {
        fs1.f(multiWalletViewModel, "multiWalletVM");
        this.a = multiWalletViewModel;
        this.b = i;
        this.d = R.color.curve_green;
    }

    public static /* synthetic */ PopupWindow e(AnchorSwitchWallet anchorSwitchWallet, View view, View view2, int i, int i2, Object obj) {
        if ((i2 & 4) != 0) {
            i = 4;
        }
        return anchorSwitchWallet.d(view, view2, i);
    }

    public final PopupWindow d(View view, View view2, int i) {
        PopupWindow popupWindow = new PopupWindow(view, view2 == null ? -1 : view2.getWidth(), -2);
        popupWindow.setOutsideTouchable(true);
        popupWindow.setFocusable(true);
        popupWindow.setInputMethodMode(1);
        popupWindow.setAttachedInDecor(false);
        return popupWindow;
    }

    public final void f() {
        PopupWindow popupWindow;
        if (!g() || (popupWindow = this.c) == null) {
            return;
        }
        popupWindow.dismiss();
    }

    public final boolean g() {
        PopupWindow popupWindow = this.c;
        if (popupWindow == null) {
            return false;
        }
        return popupWindow.isShowing();
    }

    public final AnchorSwitchWallet h(Context context, View view, View view2) {
        fs1.f(context, "context");
        fs1.f(view, "anchorView");
        Object systemService = context.getSystemService("layout_inflater");
        Objects.requireNonNull(systemService, "null cannot be cast to non-null type android.view.LayoutInflater");
        LayoutInflater layoutInflater = (LayoutInflater) systemService;
        View inflate = layoutInflater.inflate(R.layout.dialog_anchor_switch_wallet, (ViewGroup) null);
        jn0 a = jn0.a(inflate);
        fs1.e(a, "bind(view)");
        a.a.removeAllViews();
        this.a.p(new AnchorSwitchWallet$show$1$1(context, layoutInflater, a, e30.c(context), this));
        fs1.e(inflate, "view");
        PopupWindow e = e(this, inflate, view2, 0, 4, null);
        this.c = e;
        if (e != null) {
            e.showAsDropDown(view);
        }
        return this;
    }
}
