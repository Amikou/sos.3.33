package net.safemoon.androidwallet.dialogs;

import android.app.Dialog;
import android.content.ClipData;
import android.content.ClipboardManager;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentViewModelLazyKt;
import java.util.Objects;
import net.safemoon.androidwallet.R;
import net.safemoon.androidwallet.dialogs.Google2FABackup;
import net.safemoon.androidwallet.viewmodels.GoogleAuthViewModel;

/* compiled from: Google2FABackup.kt */
/* loaded from: classes2.dex */
public final class Google2FABackup extends sn0 {
    public static final a w0 = new a(null);
    public ha1 u0;
    public final sy1 v0 = FragmentViewModelLazyKt.a(this, d53.b(GoogleAuthViewModel.class), new Google2FABackup$special$$inlined$activityViewModels$default$1(this), new Google2FABackup$special$$inlined$activityViewModels$default$2(this));

    /* compiled from: Google2FABackup.kt */
    /* loaded from: classes2.dex */
    public static final class a {
        public a() {
        }

        public /* synthetic */ a(qi0 qi0Var) {
            this();
        }

        public final Google2FABackup a() {
            return new Google2FABackup();
        }
    }

    public static final void B(Google2FABackup google2FABackup, View view) {
        fs1.f(google2FABackup, "this$0");
        Dialog k = google2FABackup.k();
        if (k == null) {
            return;
        }
        k.dismiss();
    }

    public static final void C(Google2FABackup google2FABackup, View view) {
        fs1.f(google2FABackup, "this$0");
        google2FABackup.h();
    }

    public static final void D(Google2FABackup google2FABackup, String str) {
        fs1.f(google2FABackup, "this$0");
        ha1 ha1Var = google2FABackup.u0;
        if (ha1Var == null) {
            fs1.r("binding");
            ha1Var = null;
        }
        ha1Var.e.setText(str);
    }

    public static final void E(Google2FABackup google2FABackup, Bitmap bitmap) {
        fs1.f(google2FABackup, "this$0");
        ha1 ha1Var = google2FABackup.u0;
        if (ha1Var == null) {
            fs1.r("binding");
            ha1Var = null;
        }
        ha1Var.c.setImageBitmap(bitmap);
    }

    public static final void F(Google2FABackup google2FABackup, View view) {
        fs1.f(google2FABackup, "this$0");
        Object systemService = google2FABackup.requireContext().getSystemService("clipboard");
        Objects.requireNonNull(systemService, "null cannot be cast to non-null type android.content.ClipboardManager");
        ((ClipboardManager) systemService).setPrimaryClip(ClipData.newPlainText("label", google2FABackup.A().g().getValue()));
        Context requireContext = google2FABackup.requireContext();
        fs1.e(requireContext, "requireContext()");
        e30.Z(requireContext, R.string.copied_to_clipboard);
    }

    public final GoogleAuthViewModel A() {
        return (GoogleAuthViewModel) this.v0.getValue();
    }

    public final void G(FragmentManager fragmentManager) {
        fs1.f(fragmentManager, "manager");
        super.u(fragmentManager, Google2FABackup.class.getCanonicalName());
    }

    @Override // defpackage.sn0, androidx.fragment.app.Fragment
    public void onAttach(Context context) {
        fs1.f(context, "context");
        super.onAttach(context);
    }

    @Override // defpackage.sn0, androidx.fragment.app.Fragment
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
    }

    @Override // androidx.fragment.app.Fragment
    public View onCreateView(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle) {
        fs1.f(layoutInflater, "inflater");
        return layoutInflater.inflate(R.layout.fragment_google_backup, viewGroup, false);
    }

    @Override // defpackage.sn0, androidx.fragment.app.Fragment
    public void onDetach() {
        super.onDetach();
    }

    @Override // defpackage.sn0, androidx.fragment.app.Fragment
    public void onStart() {
        Window window;
        super.onStart();
        Dialog k = k();
        if (k == null || (window = k.getWindow()) == null) {
            return;
        }
        window.setBackgroundDrawable(new ColorDrawable(0));
        window.setLayout(-1, -2);
    }

    @Override // androidx.fragment.app.Fragment
    public void onViewCreated(View view, Bundle bundle) {
        fs1.f(view, "view");
        super.onViewCreated(view, bundle);
        ha1 a2 = ha1.a(view);
        fs1.e(a2, "bind(view)");
        this.u0 = a2;
        ha1 ha1Var = null;
        if (a2 == null) {
            fs1.r("binding");
            a2 = null;
        }
        a2.b.setOnClickListener(new View.OnClickListener() { // from class: yg1
            @Override // android.view.View.OnClickListener
            public final void onClick(View view2) {
                Google2FABackup.B(Google2FABackup.this, view2);
            }
        });
        ha1 ha1Var2 = this.u0;
        if (ha1Var2 == null) {
            fs1.r("binding");
            ha1Var2 = null;
        }
        ha1Var2.a.setOnClickListener(new View.OnClickListener() { // from class: xg1
            @Override // android.view.View.OnClickListener
            public final void onClick(View view2) {
                Google2FABackup.C(Google2FABackup.this, view2);
            }
        });
        A().g().observe(getViewLifecycleOwner(), new tl2() { // from class: vg1
            @Override // defpackage.tl2
            public final void onChanged(Object obj) {
                Google2FABackup.D(Google2FABackup.this, (String) obj);
            }
        });
        A().f().observe(getViewLifecycleOwner(), new tl2() { // from class: ug1
            @Override // defpackage.tl2
            public final void onChanged(Object obj) {
                Google2FABackup.E(Google2FABackup.this, (Bitmap) obj);
            }
        });
        ha1 ha1Var3 = this.u0;
        if (ha1Var3 == null) {
            fs1.r("binding");
        } else {
            ha1Var = ha1Var3;
        }
        ha1Var.d.setOnClickListener(new View.OnClickListener() { // from class: wg1
            @Override // android.view.View.OnClickListener
            public final void onClick(View view2) {
                Google2FABackup.F(Google2FABackup.this, view2);
            }
        });
    }
}
