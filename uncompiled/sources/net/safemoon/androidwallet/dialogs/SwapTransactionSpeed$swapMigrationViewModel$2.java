package net.safemoon.androidwallet.dialogs;

import androidx.fragment.app.Fragment;
import kotlin.jvm.internal.Lambda;

/* compiled from: SwapTransactionSpeed.kt */
/* loaded from: classes2.dex */
public final class SwapTransactionSpeed$swapMigrationViewModel$2 extends Lambda implements rc1<hj4> {
    public final /* synthetic */ SwapTransactionSpeed this$0;

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public SwapTransactionSpeed$swapMigrationViewModel$2(SwapTransactionSpeed swapTransactionSpeed) {
        super(0);
        this.this$0 = swapTransactionSpeed;
    }

    /* JADX WARN: Can't rename method to resolve collision */
    @Override // defpackage.rc1
    public final hj4 invoke() {
        Fragment requireParentFragment = this.this$0.requireParentFragment();
        fs1.e(requireParentFragment, "requireParentFragment()");
        return requireParentFragment;
    }
}
