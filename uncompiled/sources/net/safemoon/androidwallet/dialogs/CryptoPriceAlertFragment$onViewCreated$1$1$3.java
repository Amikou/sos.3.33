package net.safemoon.androidwallet.dialogs;

import android.text.Editable;
import kotlin.jvm.internal.Lambda;
import net.safemoon.androidwallet.model.priceAlert.PriceAlertToken;
import net.safemoon.androidwallet.viewmodels.CryptoPriceAlertViewModel;

/* compiled from: CryptoPriceAlertFragment.kt */
/* loaded from: classes2.dex */
public final class CryptoPriceAlertFragment$onViewCreated$1$1$3 extends Lambda implements tc1<Editable, te4> {
    public final /* synthetic */ CryptoPriceAlertFragment this$0;

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public CryptoPriceAlertFragment$onViewCreated$1$1$3(CryptoPriceAlertFragment cryptoPriceAlertFragment) {
        super(1);
        this.this$0 = cryptoPriceAlertFragment;
    }

    @Override // defpackage.tc1
    public /* bridge */ /* synthetic */ te4 invoke(Editable editable) {
        invoke2(editable);
        return te4.a;
    }

    /* renamed from: invoke  reason: avoid collision after fix types in other method */
    public final void invoke2(Editable editable) {
        CryptoPriceAlertViewModel N;
        String obj;
        Integer l;
        this.this$0.w0 = true;
        N = this.this$0.N();
        PriceAlertToken value = N.u().getValue();
        if (value == null) {
            return;
        }
        int i = 0;
        if (editable != null && (obj = editable.toString()) != null && (l = cv3.l(obj)) != null) {
            i = l.intValue();
        }
        value.setRepeat(i);
    }
}
