package net.safemoon.androidwallet.dialogs;

import android.app.Dialog;
import android.content.Context;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import androidx.fragment.app.FragmentManager;
import com.bumptech.glide.e;
import net.safemoon.androidwallet.R;
import net.safemoon.androidwallet.dialogs.ProgressLoading;

/* compiled from: ProgressLoading.kt */
/* loaded from: classes2.dex */
public final class ProgressLoading extends sn0 {
    public static final a y0 = new a(null);
    public vn0 u0;
    public final sy1 v0 = zy1.a(new ProgressLoading$canCancel$2(this));
    public final sy1 w0 = zy1.a(new ProgressLoading$title$2(this));
    public final sy1 x0 = zy1.a(new ProgressLoading$message$2(this));

    /* compiled from: ProgressLoading.kt */
    /* loaded from: classes2.dex */
    public static final class a {
        public a() {
        }

        public /* synthetic */ a(qi0 qi0Var) {
            this();
        }

        public final ProgressLoading a(boolean z, String str, String str2) {
            fs1.f(str, "title");
            fs1.f(str2, "msg");
            ProgressLoading progressLoading = new ProgressLoading();
            Bundle bundle = new Bundle();
            bundle.putBoolean("CAN_CANCEL", z);
            bundle.putString("TITLE", str);
            bundle.putString("MSG", str2);
            te4 te4Var = te4.a;
            progressLoading.setArguments(bundle);
            return progressLoading;
        }
    }

    public static final void z(ProgressLoading progressLoading, View view) {
        fs1.f(progressLoading, "this$0");
        progressLoading.h();
    }

    public final void A(FragmentManager fragmentManager) {
        fs1.f(fragmentManager, "manager");
        super.u(fragmentManager, ProgressLoading.class.getCanonicalName());
    }

    @Override // defpackage.sn0
    public Dialog m(Bundle bundle) {
        Dialog m = super.m(bundle);
        fs1.e(m, "super.onCreateDialog(savedInstanceState)");
        m.requestWindowFeature(1);
        return m;
    }

    @Override // defpackage.sn0, androidx.fragment.app.Fragment
    public void onAttach(Context context) {
        fs1.f(context, "context");
        super.onAttach(context);
        pg4.b(requireActivity(), Boolean.TRUE);
    }

    @Override // androidx.fragment.app.Fragment
    public View onCreateView(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle) {
        fs1.f(layoutInflater, "inflater");
        return LayoutInflater.from(requireContext()).inflate(R.layout.dialog_progress_loading, viewGroup, false);
    }

    @Override // defpackage.sn0, androidx.fragment.app.Fragment
    public void onDetach() {
        super.onDetach();
        pg4.b(requireActivity(), Boolean.FALSE);
    }

    @Override // defpackage.sn0, androidx.fragment.app.Fragment
    public void onStart() {
        super.onStart();
        Dialog k = k();
        if (k == null) {
            return;
        }
        k.setCanceledOnTouchOutside(false);
        k.setCancelable(false);
        Window window = k.getWindow();
        if (window != null) {
            window.setBackgroundDrawable(new ColorDrawable(0));
        }
        Window window2 = k.getWindow();
        if (window2 == null) {
            return;
        }
        window2.setLayout(-1, -2);
    }

    @Override // androidx.fragment.app.Fragment
    public void onViewCreated(View view, Bundle bundle) {
        fs1.f(view, "view");
        super.onViewCreated(view, bundle);
        vn0 a2 = vn0.a(view);
        fs1.e(a2, "bind(view)");
        this.u0 = a2;
        vn0 vn0Var = null;
        if (a2 == null) {
            fs1.r("binding");
            a2 = null;
        }
        a2.a.setVisibility(e30.m0(w()));
        a2.a.setOnClickListener(new View.OnClickListener() { // from class: nv2
            @Override // android.view.View.OnClickListener
            public final void onClick(View view2) {
                ProgressLoading.z(ProgressLoading.this, view2);
            }
        });
        a2.c.setText(y());
        a2.b.setText(x());
        a2.b.setVisibility(e30.k0(x().length() == 0));
        e<Drawable> w = com.bumptech.glide.a.v(this).w(Integer.valueOf((int) R.raw.safemoon_animated_logo));
        vn0 vn0Var2 = this.u0;
        if (vn0Var2 == null) {
            fs1.r("binding");
        } else {
            vn0Var = vn0Var2;
        }
        w.I0(vn0Var.d);
    }

    public final boolean w() {
        return ((Boolean) this.v0.getValue()).booleanValue();
    }

    public final String x() {
        return (String) this.x0.getValue();
    }

    public final String y() {
        return (String) this.w0.getValue();
    }
}
