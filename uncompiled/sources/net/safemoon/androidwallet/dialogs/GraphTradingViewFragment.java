package net.safemoon.androidwallet.dialogs;

import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.res.Resources;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.webkit.WebChromeClient;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.CompoundButton;
import android.widget.TextView;
import androidx.fragment.app.FragmentActivity;
import androidx.fragment.app.FragmentManager;
import com.google.android.material.checkbox.MaterialCheckBox;
import net.safemoon.androidwallet.R;
import net.safemoon.androidwallet.dialogs.GraphTradingViewFragment;
import net.safemoon.androidwallet.model.Coin;
import net.safemoon.androidwallet.model.graph.TradingViewSymbol;

/* compiled from: GraphTradingViewFragment.kt */
/* loaded from: classes2.dex */
public final class GraphTradingViewFragment extends sn0 {
    public static final a x0 = new a(null);
    public final sy1 u0 = zy1.a(new GraphTradingViewFragment$coin$2(this));
    public final sy1 v0 = zy1.a(new GraphTradingViewFragment$tradingSymbolToken$2(this));
    public final sy1 w0 = zy1.a(new GraphTradingViewFragment$binding$2(this));

    /* compiled from: GraphTradingViewFragment.kt */
    /* loaded from: classes2.dex */
    public static final class a {
        public a() {
        }

        public /* synthetic */ a(qi0 qi0Var) {
            this();
        }

        public final GraphTradingViewFragment a(Coin coin, TradingViewSymbol tradingViewSymbol) {
            fs1.f(coin, "coin");
            fs1.f(tradingViewSymbol, "tradingSymbolToken");
            GraphTradingViewFragment graphTradingViewFragment = new GraphTradingViewFragment();
            Bundle bundle = new Bundle();
            bundle.putSerializable("argCoin", coin);
            bundle.putSerializable("argTradingView", tradingViewSymbol);
            te4 te4Var = te4.a;
            graphTradingViewFragment.setArguments(bundle);
            return graphTradingViewFragment;
        }
    }

    /* compiled from: GraphTradingViewFragment.kt */
    /* loaded from: classes2.dex */
    public static final class b extends WebViewClient {
        public final /* synthetic */ WebView b;

        public b(WebView webView) {
            this.b = webView;
        }

        @Override // android.webkit.WebViewClient
        public void onPageFinished(WebView webView, String str) {
            super.onPageFinished(webView, str);
            ia1 E = GraphTradingViewFragment.this.E();
            View view = E == null ? null : E.e;
            if (view != null) {
                view.setVisibility(8);
            }
            ia1 E2 = GraphTradingViewFragment.this.E();
            TextView textView = E2 != null ? E2.f : null;
            if (textView != null) {
                textView.setVisibility(8);
            }
            this.b.setVisibility(0);
        }
    }

    public static final void B(GraphTradingViewFragment graphTradingViewFragment, View view) {
        fs1.f(graphTradingViewFragment, "this$0");
        graphTradingViewFragment.h();
    }

    public static final void C(ia1 ia1Var, GraphTradingViewFragment graphTradingViewFragment, CompoundButton compoundButton, boolean z) {
        Window window;
        WebView webView;
        fs1.f(ia1Var, "$this_apply");
        fs1.f(graphTradingViewFragment, "this$0");
        Dialog k = graphTradingViewFragment.k();
        if (k == null || (window = k.getWindow()) == null) {
            return;
        }
        if (z) {
            ia1 E = graphTradingViewFragment.E();
            ViewGroup.LayoutParams layoutParams = null;
            if (E != null && (webView = E.j) != null) {
                layoutParams = webView.getLayoutParams();
            }
            if (layoutParams != null) {
                layoutParams.height = 0;
            }
            window.setLayout(-1, -1);
            FragmentActivity activity = graphTradingViewFragment.getActivity();
            if (activity == null) {
                return;
            }
            activity.setRequestedOrientation(2);
            return;
        }
        graphTradingViewFragment.L();
        window.setLayout(-1, -2);
        FragmentActivity activity2 = graphTradingViewFragment.getActivity();
        if (activity2 == null) {
            return;
        }
        activity2.setRequestedOrientation(1);
    }

    public static final void D(GraphTradingViewFragment graphTradingViewFragment, CompoundButton compoundButton, boolean z) {
        fs1.f(graphTradingViewFragment, "this$0");
        graphTradingViewFragment.I();
    }

    public static final void J(GraphTradingViewFragment graphTradingViewFragment, View view) {
        fs1.f(graphTradingViewFragment, "this$0");
        String obj = el1.a(graphTradingViewFragment.G().symbol, 0).toString();
        b30 b30Var = b30.a;
        Context requireContext = graphTradingViewFragment.requireContext();
        fs1.e(requireContext, "requireContext()");
        b30Var.u(requireContext, fs1.l("https://www.tradingview.com/chart/4Y7vkOum/?symbol=", obj));
    }

    public final void A() {
        final ia1 E = E();
        if (E != null) {
            E.c.setOnClickListener(new View.OnClickListener() { // from class: ni1
                @Override // android.view.View.OnClickListener
                public final void onClick(View view) {
                    GraphTradingViewFragment.B(GraphTradingViewFragment.this, view);
                }
            });
            E.b.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() { // from class: oi1
                @Override // android.widget.CompoundButton.OnCheckedChangeListener
                public final void onCheckedChanged(CompoundButton compoundButton, boolean z) {
                    GraphTradingViewFragment.C(ia1.this, this, compoundButton, z);
                }
            });
            E.a.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() { // from class: pi1
                @Override // android.widget.CompoundButton.OnCheckedChangeListener
                public final void onCheckedChanged(CompoundButton compoundButton, boolean z) {
                    GraphTradingViewFragment.D(GraphTradingViewFragment.this, compoundButton, z);
                }
            });
        }
        I();
    }

    public final ia1 E() {
        return (ia1) this.w0.getValue();
    }

    public final Coin F() {
        return (Coin) this.u0.getValue();
    }

    public final TradingViewSymbol G() {
        return (TradingViewSymbol) this.v0.getValue();
    }

    public final String H() {
        String str;
        String str2;
        MaterialCheckBox materialCheckBox;
        if (androidx.appcompat.app.b.l() == 2) {
            str = "#37474f";
            str2 = "dark";
        } else {
            str = "#FFFFFF";
            str2 = "light";
        }
        ia1 E = E();
        int i = E != null && (materialCheckBox = E.a) != null && materialCheckBox.isChecked() ? 1 : 2;
        String obj = el1.a(G().symbol, 0).toString();
        return "<!-- TradingView Widget BEGIN -->\n<style>body {background: " + str + "}</style><div class=\"tradingview-widget-container\" style='background: transparent'>\n  <div id=\"tradingview_f9b26\"></div>\n  <script type=\"text/javascript\" src=\"https://s3.tradingview.com/tv.js\"></script>\n  <script type=\"text/javascript\">\n  new TradingView.widget(\n  {\n  \"autosize\": true,\n  \"symbol\": \"" + ((Object) obj) + "\",\n  \"interval\": \"D\",\n  \"timezone\": \"Etc/UTC\",\n  \"theme\": \"" + str2 + "\",\n  \"style\": \"" + i + "\",\n  \"locale\": \"in\",\n  \"toolbar_bg\": \"#f1f3f6\",\n  \"enable_publishing\": false,\n  \"hide_top_toolbar\": true,\n  \"hide_legend\": true,\n  \"save_image\": false,\n  \"allow_symbol_change\": false,\n  \"container_id\": \"tradingview_f9b26\"\n}\n  );\n  </script>\n</div>\n<!-- TradingView Widget END -->";
    }

    public final void I() {
        WebView webView;
        ia1 E = E();
        WebView webView2 = E == null ? null : E.j;
        if (webView2 != null) {
            webView2.setVisibility(4);
        }
        ia1 E2 = E();
        if (E2 == null || (webView = E2.j) == null) {
            return;
        }
        webView.loadDataWithBaseURL(null, H(), "text/html", "UTF-8", null);
    }

    public final void K(WebView webView) {
        webView.setWebChromeClient(new WebChromeClient());
        webView.setWebViewClient(new b(webView));
    }

    public final void L() {
        Resources resources;
        WebView webView;
        FragmentActivity activity = getActivity();
        Float valueOf = (activity == null || (resources = activity.getResources()) == null) ? null : Float.valueOf(resources.getDimension(R.dimen.my_250dp));
        ia1 E = E();
        ViewGroup.LayoutParams layoutParams = (E == null || (webView = E.j) == null) ? null : webView.getLayoutParams();
        if (layoutParams == null) {
            return;
        }
        layoutParams.height = (valueOf != null ? Integer.valueOf((int) valueOf.floatValue()) : null).intValue();
    }

    public final void M(FragmentManager fragmentManager) {
        fs1.f(fragmentManager, "manager");
        super.u(fragmentManager, GraphTradingViewFragment.class.getCanonicalName());
    }

    @Override // defpackage.sn0
    public Dialog m(Bundle bundle) {
        Dialog m = super.m(bundle);
        fs1.e(m, "super.onCreateDialog(savedInstanceState)");
        m.requestWindowFeature(1);
        Window window = m.getWindow();
        if (window != null) {
            window.setBackgroundDrawable(new ColorDrawable(0));
        }
        Window window2 = m.getWindow();
        if (window2 != null) {
            window2.setLayout(-1, -1);
        }
        return m;
    }

    @Override // defpackage.sn0, androidx.fragment.app.Fragment
    public void onAttach(Context context) {
        fs1.f(context, "context");
        super.onAttach(context);
    }

    @Override // defpackage.sn0, androidx.fragment.app.Fragment
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        s(0, 2132017638);
    }

    @Override // androidx.fragment.app.Fragment
    public View onCreateView(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle) {
        fs1.f(layoutInflater, "inflater");
        return layoutInflater.inflate(R.layout.fragment_graph_trading_view, viewGroup, false);
    }

    @Override // defpackage.sn0, android.content.DialogInterface.OnDismissListener
    public void onDismiss(DialogInterface dialogInterface) {
        fs1.f(dialogInterface, "dialog");
        super.onDismiss(dialogInterface);
        FragmentActivity activity = getActivity();
        if (activity == null) {
            return;
        }
        activity.setRequestedOrientation(1);
    }

    @Override // androidx.fragment.app.Fragment
    public void onViewCreated(View view, Bundle bundle) {
        TextView textView;
        fs1.f(view, "view");
        super.onViewCreated(view, bundle);
        A();
        ia1 E = E();
        if (E != null) {
            TextView textView2 = E.h;
            StringBuilder sb = new StringBuilder();
            sb.append((Object) F().getName());
            sb.append('(');
            sb.append((Object) F().getSymbol());
            sb.append(')');
            textView2.setText(sb.toString());
            TextView textView3 = E.i;
            fs1.e(textView3, "tvPriceDialog");
            Double price = F().getQuote().getUSD().getPrice();
            fs1.e(price, "coin.quote.usd.price");
            e30.O(textView3, price.doubleValue(), true);
            k73 u = com.bumptech.glide.a.u(E.d);
            Integer id = F().getId();
            fs1.e(id, "coin.id");
            u.x(a4.f(id.intValue(), F().getSymbol())).a(n73.v0()).I0(E.d);
            try {
                WebSettings settings = E.j.getSettings();
                fs1.e(settings, "webView.settings");
                settings.setJavaScriptCanOpenWindowsAutomatically(true);
                settings.setSupportMultipleWindows(true);
                settings.setSupportZoom(false);
                settings.setBuiltInZoomControls(true);
                settings.setJavaScriptEnabled(true);
                settings.setCacheMode(-1);
                settings.setDatabaseEnabled(true);
                settings.setDomStorageEnabled(true);
                settings.setGeolocationEnabled(true);
                settings.setGeolocationEnabled(true);
            } catch (Exception e) {
                e.printStackTrace();
            }
            WebView webView = E.j;
            fs1.e(webView, "webView");
            K(webView);
        }
        ia1 E2 = E();
        if (E2 != null && (textView = E2.g) != null) {
            textView.setOnClickListener(new View.OnClickListener() { // from class: mi1
                @Override // android.view.View.OnClickListener
                public final void onClick(View view2) {
                    GraphTradingViewFragment.J(GraphTradingViewFragment.this, view2);
                }
            });
        }
        L();
    }
}
