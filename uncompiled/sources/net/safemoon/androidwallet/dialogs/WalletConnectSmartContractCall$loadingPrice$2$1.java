package net.safemoon.androidwallet.dialogs;

import androidx.appcompat.widget.AppCompatTextView;
import com.github.mikephil.charting.utils.Utils;
import com.google.android.material.textview.MaterialTextView;
import com.trustwallet.walletconnect.models.ethereum.WCEthereumTransaction;
import defpackage.u21;
import java.math.BigInteger;
import kotlin.coroutines.jvm.internal.SuspendLambda;
import kotlin.coroutines.jvm.internal.a;
import net.safemoon.androidwallet.common.TokenType;
import net.safemoon.androidwallet.model.token.room.RoomToken;

/* compiled from: WalletConnectSmartContractCall.kt */
@a(c = "net.safemoon.androidwallet.dialogs.WalletConnectSmartContractCall$loadingPrice$2$1", f = "WalletConnectSmartContractCall.kt", l = {}, m = "invokeSuspend")
/* loaded from: classes2.dex */
public final class WalletConnectSmartContractCall$loadingPrice$2$1 extends SuspendLambda implements hd1<c90, q70<? super te4>, Object> {
    public final /* synthetic */ BigInteger $gasLimit;
    public final /* synthetic */ BigInteger $gasPrice;
    public final /* synthetic */ RoomToken $nativeToken;
    public final /* synthetic */ TokenType $tt;
    public int label;
    public final /* synthetic */ WalletConnectSmartContractCall this$0;

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public WalletConnectSmartContractCall$loadingPrice$2$1(WalletConnectSmartContractCall walletConnectSmartContractCall, RoomToken roomToken, BigInteger bigInteger, BigInteger bigInteger2, TokenType tokenType, q70<? super WalletConnectSmartContractCall$loadingPrice$2$1> q70Var) {
        super(2, q70Var);
        this.this$0 = walletConnectSmartContractCall;
        this.$nativeToken = roomToken;
        this.$gasLimit = bigInteger;
        this.$gasPrice = bigInteger2;
        this.$tt = tokenType;
    }

    @Override // kotlin.coroutines.jvm.internal.BaseContinuationImpl
    public final q70<te4> create(Object obj, q70<?> q70Var) {
        return new WalletConnectSmartContractCall$loadingPrice$2$1(this.this$0, this.$nativeToken, this.$gasLimit, this.$gasPrice, this.$tt, q70Var);
    }

    @Override // defpackage.hd1
    public final Object invoke(c90 c90Var, q70<? super te4> q70Var) {
        return ((WalletConnectSmartContractCall$loadingPrice$2$1) create(c90Var, q70Var)).invokeSuspend(te4.a);
    }

    @Override // kotlin.coroutines.jvm.internal.BaseContinuationImpl
    public final Object invokeSuspend(Object obj) {
        ub1 ub1Var;
        WCEthereumTransaction wCEthereumTransaction;
        double d;
        WCEthereumTransaction wCEthereumTransaction2;
        gs1.d();
        if (this.label == 0) {
            o83.b(obj);
            ub1Var = this.this$0.A0;
            if (ub1Var == null) {
                return null;
            }
            RoomToken roomToken = this.$nativeToken;
            WalletConnectSmartContractCall walletConnectSmartContractCall = this.this$0;
            BigInteger bigInteger = this.$gasLimit;
            BigInteger bigInteger2 = this.$gasPrice;
            TokenType tokenType = this.$tt;
            if (roomToken == null) {
                return null;
            }
            MaterialTextView materialTextView = ub1Var.c;
            materialTextView.setText(roomToken.getName() + '(' + roomToken.getSymbol() + ')');
            wCEthereumTransaction = walletConnectSmartContractCall.y0;
            if ((wCEthereumTransaction == null ? null : wCEthereumTransaction.getValue()) != null) {
                wCEthereumTransaction2 = walletConnectSmartContractCall.y0;
                BigInteger decodeQuantity = ej2.decodeQuantity(wCEthereumTransaction2 != null ? wCEthereumTransaction2.getValue() : null);
                fs1.e(decodeQuantity, "decodeQuantity(transaction?.value)");
                d = e30.r(decodeQuantity, 18).doubleValue();
            } else {
                d = Utils.DOUBLE_EPSILON;
            }
            double d2 = (-1) * d;
            ub1Var.e.setText(fs1.l(e30.p(d2, 0, null, false, 6, null), roomToken.getSymbol()));
            AppCompatTextView appCompatTextView = ub1Var.d;
            u21.a aVar = u21.a;
            appCompatTextView.setText(fs1.l(aVar.b(), e30.p(v21.a(d2 * roomToken.getPriceInUsdt()), 0, null, false, 7, null)));
            BigInteger multiply = bigInteger.multiply(bigInteger2);
            fs1.e(multiply, "gasLimit.multiply(gasPrice)");
            double doubleValue = e30.r(multiply, 18).doubleValue();
            MaterialTextView materialTextView2 = ub1Var.h;
            materialTextView2.setText(e30.p(doubleValue, 0, null, false, 6, null) + tokenType.getNativeToken() + '(' + aVar.b() + e30.p(v21.a(roomToken.getPriceInUsdt() * doubleValue), 2, null, false, 6, null) + ')');
            ub1Var.g.setText(fs1.l(aVar.b(), e30.p(v21.a((doubleValue + d) * roomToken.getPriceInUsdt()), 2, null, false, 6, null)));
            return te4.a;
        }
        throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
    }
}
