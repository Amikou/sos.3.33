package net.safemoon.androidwallet.dialogs;

import android.view.View;
import kotlin.jvm.internal.Lambda;

/* compiled from: GraphTradingViewFragment.kt */
/* loaded from: classes2.dex */
public final class GraphTradingViewFragment$binding$2 extends Lambda implements rc1<ia1> {
    public final /* synthetic */ GraphTradingViewFragment this$0;

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public GraphTradingViewFragment$binding$2(GraphTradingViewFragment graphTradingViewFragment) {
        super(0);
        this.this$0 = graphTradingViewFragment;
    }

    /* JADX WARN: Can't rename method to resolve collision */
    @Override // defpackage.rc1
    public final ia1 invoke() {
        View view = this.this$0.getView();
        if (view == null) {
            return null;
        }
        return ia1.a(view);
    }
}
