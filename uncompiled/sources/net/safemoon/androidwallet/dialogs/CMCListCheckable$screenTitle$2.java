package net.safemoon.androidwallet.dialogs;

import android.os.Bundle;
import kotlin.jvm.internal.Lambda;

/* compiled from: CMCListCheckable.kt */
/* loaded from: classes2.dex */
public final class CMCListCheckable$screenTitle$2 extends Lambda implements rc1<String> {
    public final /* synthetic */ CMCListCheckable this$0;

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public CMCListCheckable$screenTitle$2(CMCListCheckable cMCListCheckable) {
        super(0);
        this.this$0 = cMCListCheckable;
    }

    @Override // defpackage.rc1
    public final String invoke() {
        Bundle arguments = this.this$0.getArguments();
        if (arguments == null) {
            return null;
        }
        return arguments.getString("ARG_SCREEN_TITLE");
    }
}
