package net.safemoon.androidwallet.dialogs;

import kotlin.coroutines.jvm.internal.ContinuationImpl;
import kotlin.coroutines.jvm.internal.a;

/* compiled from: WalletConnectSmartContractCall.kt */
@a(c = "net.safemoon.androidwallet.dialogs.WalletConnectSmartContractCall", f = "WalletConnectSmartContractCall.kt", l = {98, 99, 100, 101, 102}, m = "loadingPrice")
/* loaded from: classes2.dex */
public final class WalletConnectSmartContractCall$loadingPrice$1 extends ContinuationImpl {
    public int I$0;
    public Object L$0;
    public Object L$1;
    public Object L$2;
    public Object L$3;
    public int label;
    public /* synthetic */ Object result;
    public final /* synthetic */ WalletConnectSmartContractCall this$0;

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public WalletConnectSmartContractCall$loadingPrice$1(WalletConnectSmartContractCall walletConnectSmartContractCall, q70<? super WalletConnectSmartContractCall$loadingPrice$1> q70Var) {
        super(q70Var);
        this.this$0 = walletConnectSmartContractCall;
    }

    @Override // kotlin.coroutines.jvm.internal.BaseContinuationImpl
    public final Object invokeSuspend(Object obj) {
        Object O;
        this.result = obj;
        this.label |= Integer.MIN_VALUE;
        O = this.this$0.O(this);
        return O;
    }
}
