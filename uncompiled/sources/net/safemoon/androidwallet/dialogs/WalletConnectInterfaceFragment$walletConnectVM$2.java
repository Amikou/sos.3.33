package net.safemoon.androidwallet.dialogs;

import androidx.fragment.app.Fragment;
import kotlin.jvm.internal.Lambda;

/* compiled from: WalletConnectInterfaceFragment.kt */
/* loaded from: classes2.dex */
public final class WalletConnectInterfaceFragment$walletConnectVM$2 extends Lambda implements rc1<hj4> {
    public final /* synthetic */ WalletConnectInterfaceFragment this$0;

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public WalletConnectInterfaceFragment$walletConnectVM$2(WalletConnectInterfaceFragment walletConnectInterfaceFragment) {
        super(0);
        this.this$0 = walletConnectInterfaceFragment;
    }

    /* JADX WARN: Can't rename method to resolve collision */
    @Override // defpackage.rc1
    public final hj4 invoke() {
        Fragment requireParentFragment = this.this$0.requireParentFragment();
        fs1.e(requireParentFragment, "requireParentFragment()");
        return requireParentFragment;
    }
}
