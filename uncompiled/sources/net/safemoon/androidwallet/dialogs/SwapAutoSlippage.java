package net.safemoon.androidwallet.dialogs;

import android.app.Dialog;
import android.content.DialogInterface;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentViewModelLazyKt;
import com.google.android.material.button.MaterialButton;
import net.safemoon.androidwallet.R;
import net.safemoon.androidwallet.dialogs.SwapAutoSlippage;
import net.safemoon.androidwallet.viewmodels.SwapViewModel;

/* compiled from: SwapAutoSlippage.kt */
/* loaded from: classes2.dex */
public final class SwapAutoSlippage extends sn0 {
    public static final a w0 = new a(null);
    public static boolean x0;
    public kn0 u0;
    public final sy1 v0 = FragmentViewModelLazyKt.a(this, d53.b(SwapViewModel.class), new SwapAutoSlippage$special$$inlined$viewModels$default$1(new SwapAutoSlippage$swapViewModel$2(this)), null);

    /* compiled from: SwapAutoSlippage.kt */
    /* loaded from: classes2.dex */
    public static final class a {
        public a() {
        }

        public /* synthetic */ a(qi0 qi0Var) {
            this();
        }

        public final SwapAutoSlippage a() {
            SwapAutoSlippage swapAutoSlippage = new SwapAutoSlippage();
            Bundle bundle = new Bundle();
            te4 te4Var = te4.a;
            swapAutoSlippage.setArguments(bundle);
            return swapAutoSlippage;
        }
    }

    public static final void A(SwapAutoSlippage swapAutoSlippage, View view) {
        fs1.f(swapAutoSlippage, "this$0");
        swapAutoSlippage.h();
    }

    public static final void B(SwapAutoSlippage swapAutoSlippage, View view) {
        fs1.f(swapAutoSlippage, "this$0");
        swapAutoSlippage.h();
    }

    public static final void C(SwapAutoSlippage swapAutoSlippage, View view) {
        fs1.f(swapAutoSlippage, "this$0");
        bo3.n(swapAutoSlippage.requireContext(), "SWAP_PAIR_DONT_SHOW_ME", Boolean.FALSE);
        swapAutoSlippage.h();
    }

    public final void D(FragmentManager fragmentManager) {
        fs1.f(fragmentManager, "manager");
        if (x0) {
            return;
        }
        super.u(fragmentManager, SwapAutoSlippage.class.getCanonicalName());
        x0 = true;
    }

    @Override // defpackage.sn0
    public Dialog m(Bundle bundle) {
        Dialog m = super.m(bundle);
        fs1.e(m, "super.onCreateDialog(savedInstanceState)");
        m.requestWindowFeature(1);
        return m;
    }

    @Override // androidx.fragment.app.Fragment
    public View onCreateView(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle) {
        fs1.f(layoutInflater, "inflater");
        return LayoutInflater.from(requireContext()).inflate(R.layout.dialog_auto_slippage, viewGroup, false);
    }

    @Override // defpackage.sn0, android.content.DialogInterface.OnDismissListener
    public void onDismiss(DialogInterface dialogInterface) {
        fs1.f(dialogInterface, "dialog");
        x0 = false;
        super.onDismiss(dialogInterface);
    }

    @Override // defpackage.sn0, androidx.fragment.app.Fragment
    public void onStart() {
        super.onStart();
        Dialog k = k();
        if (k == null) {
            return;
        }
        Window window = k.getWindow();
        if (window != null) {
            window.setBackgroundDrawable(new ColorDrawable(0));
        }
        Window window2 = k.getWindow();
        if (window2 == null) {
            return;
        }
        window2.setLayout(-1, -2);
    }

    @Override // androidx.fragment.app.Fragment
    public void onViewCreated(View view, Bundle bundle) {
        fs1.f(view, "view");
        super.onViewCreated(view, bundle);
        kn0 a2 = kn0.a(view);
        fs1.e(a2, "bind(view)");
        this.u0 = a2;
        kn0 kn0Var = null;
        if (a2 == null) {
            fs1.r("binding");
            a2 = null;
        }
        a2.b.setOnClickListener(new View.OnClickListener() { // from class: bx3
            @Override // android.view.View.OnClickListener
            public final void onClick(View view2) {
                SwapAutoSlippage.A(SwapAutoSlippage.this, view2);
            }
        });
        kn0 kn0Var2 = this.u0;
        if (kn0Var2 == null) {
            fs1.r("binding");
            kn0Var2 = null;
        }
        bi4 bi4Var = kn0Var2.d;
        bi4Var.b.setOnClickListener(new View.OnClickListener() { // from class: cx3
            @Override // android.view.View.OnClickListener
            public final void onClick(View view2) {
                SwapAutoSlippage.B(SwapAutoSlippage.this, view2);
            }
        });
        bi4Var.a.setText(R.string.auto_slippage_ok);
        kn0 kn0Var3 = this.u0;
        if (kn0Var3 == null) {
            fs1.r("binding");
            kn0Var3 = null;
        }
        kn0Var3.a.setOnClickListener(new View.OnClickListener() { // from class: dx3
            @Override // android.view.View.OnClickListener
            public final void onClick(View view2) {
                SwapAutoSlippage.C(SwapAutoSlippage.this, view2);
            }
        });
        kn0 kn0Var4 = this.u0;
        if (kn0Var4 == null) {
            fs1.r("binding");
        } else {
            kn0Var = kn0Var4;
        }
        MaterialButton materialButton = kn0Var.c;
        StringBuilder sb = new StringBuilder();
        sb.append(y());
        sb.append('%');
        materialButton.setText(sb.toString());
    }

    public final double y() {
        return e30.B(qc4.a(z().A0().getValue(), z().b0().getValue()));
    }

    public final SwapViewModel z() {
        return (SwapViewModel) this.v0.getValue();
    }
}
