package net.safemoon.androidwallet.dialogs;

import kotlin.jvm.internal.Lambda;
import net.safemoon.androidwallet.model.priceAlert.PriceAlertToken;
import net.safemoon.androidwallet.viewmodels.CryptoPriceAlertViewModel;

/* compiled from: CryptoPriceAlertFragment.kt */
/* loaded from: classes2.dex */
public final class CryptoPriceAlertFragment$onViewCreated$1$1$6$1 extends Lambda implements tc1<Boolean, te4> {
    public final /* synthetic */ CryptoPriceAlertFragment this$0;

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public CryptoPriceAlertFragment$onViewCreated$1$1$6$1(CryptoPriceAlertFragment cryptoPriceAlertFragment) {
        super(1);
        this.this$0 = cryptoPriceAlertFragment;
    }

    @Override // defpackage.tc1
    public /* bridge */ /* synthetic */ te4 invoke(Boolean bool) {
        invoke(bool.booleanValue());
        return te4.a;
    }

    public final void invoke(boolean z) {
        CryptoPriceAlertViewModel N;
        CryptoPriceAlertViewModel N2;
        this.this$0.w0 = true;
        N = this.this$0.N();
        PriceAlertToken value = N.u().getValue();
        if (value == null) {
            return;
        }
        CryptoPriceAlertFragment cryptoPriceAlertFragment = this.this$0;
        if (z) {
            value.setPriceReachesOrHigher(z);
            value.setPriceReachesOrLower(!z);
        } else {
            value.setPriceReachesOrHigher(false);
        }
        N2 = cryptoPriceAlertFragment.N();
        CryptoPriceAlertViewModel.A(N2, value, null, 2, null);
    }
}
