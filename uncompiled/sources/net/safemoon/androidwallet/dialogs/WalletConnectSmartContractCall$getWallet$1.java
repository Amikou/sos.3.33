package net.safemoon.androidwallet.dialogs;

import kotlin.coroutines.jvm.internal.ContinuationImpl;
import kotlin.coroutines.jvm.internal.a;

/* compiled from: WalletConnectSmartContractCall.kt */
@a(c = "net.safemoon.androidwallet.dialogs.WalletConnectSmartContractCall", f = "WalletConnectSmartContractCall.kt", l = {89}, m = "getWallet")
/* loaded from: classes2.dex */
public final class WalletConnectSmartContractCall$getWallet$1 extends ContinuationImpl {
    public int label;
    public /* synthetic */ Object result;
    public final /* synthetic */ WalletConnectSmartContractCall this$0;

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public WalletConnectSmartContractCall$getWallet$1(WalletConnectSmartContractCall walletConnectSmartContractCall, q70<? super WalletConnectSmartContractCall$getWallet$1> q70Var) {
        super(q70Var);
        this.this$0 = walletConnectSmartContractCall;
    }

    @Override // kotlin.coroutines.jvm.internal.BaseContinuationImpl
    public final Object invokeSuspend(Object obj) {
        Object N;
        this.result = obj;
        this.label |= Integer.MIN_VALUE;
        N = this.this$0.N(0L, this);
        return N;
    }
}
