package net.safemoon.androidwallet.dialogs;

import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentViewModelLazyKt;
import androidx.lifecycle.LiveData;
import com.google.android.material.button.MaterialButton;
import java.math.BigDecimal;
import java.math.RoundingMode;
import net.safemoon.androidwallet.R;
import net.safemoon.androidwallet.dialogs.BestMaxFragment;
import net.safemoon.androidwallet.model.common.LoadingState;
import net.safemoon.androidwallet.viewmodels.SwapViewModel;

/* compiled from: BestMaxFragment.kt */
/* loaded from: classes2.dex */
public final class BestMaxFragment extends sn0 {
    public static final a w0 = new a(null);
    public ln0 u0;
    public final sy1 v0;

    /* compiled from: BestMaxFragment.kt */
    /* loaded from: classes2.dex */
    public static final class a {
        public a() {
        }

        public /* synthetic */ a(qi0 qi0Var) {
            this();
        }

        public final BestMaxFragment a() {
            BestMaxFragment bestMaxFragment = new BestMaxFragment();
            Bundle bundle = new Bundle();
            te4 te4Var = te4.a;
            bestMaxFragment.setArguments(bundle);
            return bestMaxFragment;
        }
    }

    public BestMaxFragment() {
        super(R.layout.dialog_best_max_fragment);
        this.v0 = FragmentViewModelLazyKt.a(this, d53.b(SwapViewModel.class), new BestMaxFragment$special$$inlined$viewModels$default$1(new BestMaxFragment$swapViewModel$2(this)), null);
    }

    public static final void A(BestMaxFragment bestMaxFragment, View view) {
        fs1.f(bestMaxFragment, "this$0");
        bestMaxFragment.h();
    }

    public static final void B(BestMaxFragment bestMaxFragment, LoadingState loadingState) {
        fs1.f(bestMaxFragment, "this$0");
        if (loadingState == null) {
            return;
        }
        ln0 ln0Var = bestMaxFragment.u0;
        if (ln0Var == null) {
            fs1.r("binding");
            ln0Var = null;
        }
        ln0Var.c.setVisibility(e30.m0(loadingState == LoadingState.Loading));
    }

    public static final void C(BestMaxFragment bestMaxFragment, BigDecimal bigDecimal) {
        fs1.f(bestMaxFragment, "this$0");
        if (bigDecimal == null) {
            return;
        }
        ln0 ln0Var = bestMaxFragment.u0;
        if (ln0Var == null) {
            fs1.r("binding");
            ln0Var = null;
        }
        ln0Var.b.setVisibility(e30.m0(bigDecimal.compareTo(BigDecimal.ZERO) > 0));
        ln0 ln0Var2 = bestMaxFragment.u0;
        if (ln0Var2 == null) {
            fs1.r("binding");
            ln0Var2 = null;
        }
        ln0Var2.a.setVisibility(e30.k0(bigDecimal.compareTo(BigDecimal.ZERO) > 0));
        if (bigDecimal.compareTo(BigDecimal.ZERO) > 0) {
            ln0 ln0Var3 = bestMaxFragment.u0;
            if (ln0Var3 == null) {
                fs1.r("binding");
                ln0Var3 = null;
            }
            MaterialButton materialButton = ln0Var3.b;
            BigDecimal scale = bigDecimal.setScale(5, RoundingMode.UP);
            fs1.e(scale, "it.setScale(5, RoundingMode.UP)");
            materialButton.setText(e30.h0(scale, 0, 1, null));
            return;
        }
        Context context = bestMaxFragment.getContext();
        if (context == null) {
            return;
        }
        e30.a0(context, "Please try with lower value we unable to find best max value for you.");
    }

    public static final void D(BestMaxFragment bestMaxFragment, View view) {
        fs1.f(bestMaxFragment, "this$0");
        BigDecimal value = bestMaxFragment.z().h0().getValue();
        if (value == null) {
            return;
        }
        if (bestMaxFragment.z().F0().getValue() != null) {
            bestMaxFragment.z().v0().postValue(value);
        }
        bestMaxFragment.h();
    }

    public final void E(FragmentManager fragmentManager) {
        fs1.f(fragmentManager, "manager");
        super.u(fragmentManager, BestMaxFragment.class.getCanonicalName());
    }

    @Override // defpackage.sn0
    public Dialog m(Bundle bundle) {
        Dialog m = super.m(bundle);
        fs1.e(m, "super.onCreateDialog(savedInstanceState)");
        m.requestWindowFeature(1);
        return m;
    }

    @Override // defpackage.sn0, androidx.fragment.app.Fragment
    public void onDetach() {
        super.onDetach();
    }

    @Override // defpackage.sn0, android.content.DialogInterface.OnDismissListener
    public void onDismiss(DialogInterface dialogInterface) {
        fs1.f(dialogInterface, "dialog");
        super.onDismiss(dialogInterface);
        z().G();
        z().g0().postValue(LoadingState.Normal);
        z().h0().postValue(null);
    }

    @Override // defpackage.sn0, androidx.fragment.app.Fragment
    public void onStart() {
        super.onStart();
        Dialog k = k();
        if (k == null) {
            return;
        }
        Window window = k.getWindow();
        if (window != null) {
            window.setBackgroundDrawable(new ColorDrawable(0));
        }
        Window window2 = k.getWindow();
        if (window2 == null) {
            return;
        }
        window2.setLayout(-1, -2);
    }

    @Override // androidx.fragment.app.Fragment
    public void onViewCreated(View view, Bundle bundle) {
        fs1.f(view, "view");
        super.onViewCreated(view, bundle);
        ln0 a2 = ln0.a(view);
        fs1.e(a2, "bind(view)");
        this.u0 = a2;
        Animation loadAnimation = AnimationUtils.loadAnimation(getContext(), R.anim.shake);
        loadAnimation.setRepeatMode(1);
        loadAnimation.setRepeatCount(-1);
        ln0 ln0Var = this.u0;
        ln0 ln0Var2 = null;
        if (ln0Var == null) {
            fs1.r("binding");
            ln0Var = null;
        }
        ln0Var.c.startAnimation(loadAnimation);
        ln0 ln0Var3 = this.u0;
        if (ln0Var3 == null) {
            fs1.r("binding");
            ln0Var3 = null;
        }
        ln0Var3.a.setOnClickListener(new View.OnClickListener() { // from class: cp
            @Override // android.view.View.OnClickListener
            public final void onClick(View view2) {
                BestMaxFragment.A(BestMaxFragment.this, view2);
            }
        });
        LiveData a3 = cb4.a(z().g0());
        fs1.e(a3, "Transformations.distinctUntilChanged(this)");
        a3.observe(getViewLifecycleOwner(), new tl2() { // from class: bp
            @Override // defpackage.tl2
            public final void onChanged(Object obj) {
                BestMaxFragment.B(BestMaxFragment.this, (LoadingState) obj);
            }
        });
        LiveData a4 = cb4.a(z().h0());
        fs1.e(a4, "Transformations.distinctUntilChanged(this)");
        a4.observe(getViewLifecycleOwner(), new tl2() { // from class: ap
            @Override // defpackage.tl2
            public final void onChanged(Object obj) {
                BestMaxFragment.C(BestMaxFragment.this, (BigDecimal) obj);
            }
        });
        ln0 ln0Var4 = this.u0;
        if (ln0Var4 == null) {
            fs1.r("binding");
        } else {
            ln0Var2 = ln0Var4;
        }
        ln0Var2.b.setOnClickListener(new View.OnClickListener() { // from class: dp
            @Override // android.view.View.OnClickListener
            public final void onClick(View view2) {
                BestMaxFragment.D(BestMaxFragment.this, view2);
            }
        });
    }

    public final SwapViewModel z() {
        return (SwapViewModel) this.v0.getValue();
    }
}
