package net.safemoon.androidwallet.dialogs;

import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.TextView;
import androidx.appcompat.widget.AppCompatButton;
import androidx.fragment.app.FragmentActivity;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentViewModelLazyKt;
import com.bumptech.glide.e;
import com.github.mikephil.charting.charts.CandleStickChart;
import com.github.mikephil.charting.charts.LineChart;
import com.github.mikephil.charting.components.AxisBase;
import com.github.mikephil.charting.components.XAxis;
import com.github.mikephil.charting.data.CandleData;
import com.github.mikephil.charting.data.CandleDataSet;
import com.github.mikephil.charting.data.CandleEntry;
import com.github.mikephil.charting.data.Entry;
import com.github.mikephil.charting.data.LineData;
import com.github.mikephil.charting.data.LineDataSet;
import com.github.mikephil.charting.formatter.ValueFormatter;
import com.github.mikephil.charting.utils.Utils;
import defpackage.u21;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import net.safemoon.androidwallet.R;
import net.safemoon.androidwallet.common.TokenType;
import net.safemoon.androidwallet.dialogs.GraphFragment;
import net.safemoon.androidwallet.model.Data;
import net.safemoon.androidwallet.model.HistoricalDatum;
import net.safemoon.androidwallet.model.HistoricalList;
import net.safemoon.androidwallet.model.USDT;
import net.safemoon.androidwallet.model.cmc.coinPrice.CoinPriceStats;
import net.safemoon.androidwallet.model.cmc.coinPrice.CoinPriceStatsData;
import net.safemoon.androidwallet.model.cmc.coinPrice.CoinPriceStatsDataQuoteList;
import net.safemoon.androidwallet.model.cmc.coinPrice.CoinPriceStatsUsd;
import net.safemoon.androidwallet.model.tokensInfo.CurrencyTokenInfo;
import net.safemoon.androidwallet.provider.TokenInfoExistProvider;
import net.safemoon.androidwallet.viewmodels.GraphViewModel;
import net.safemoon.androidwallet.viewmodels.MyTokensListViewModel;
import net.safemoon.androidwallet.views.marker.LineGraphMarkerView;
import org.web3j.ens.contracts.generated.PublicResolver;

/* compiled from: GraphFragment.kt */
/* loaded from: classes2.dex */
public final class GraphFragment extends sn0 {
    public static final b L0 = new b(null);
    public boolean D0;
    public uh1 E0;
    public final sy1 F0;
    public final sy1 G0;
    public final sy1 H0;
    public View I0;
    public final sy1 J0;
    public final gb2<String> K0;
    public String u0;
    public String v0;
    public String x0;
    public String y0;
    public double w0 = -1.0d;
    public final sy1 z0 = zy1.a(new GraphFragment$cmcId$2(this));
    public final sy1 A0 = zy1.a(new GraphFragment$contractAddress$2(this));
    public final sy1 B0 = zy1.a(new GraphFragment$cmcSlug$2(this));
    public final sy1 C0 = zy1.a(GraphFragment$userCurrencySymbol$2.INSTANCE);

    /* compiled from: GraphFragment.kt */
    /* loaded from: classes2.dex */
    public final class a extends ValueFormatter {
        public final boolean a;
        public final List<CoinPriceStatsDataQuoteList> b;

        public a(GraphFragment graphFragment, boolean z, List<CoinPriceStatsDataQuoteList> list) {
            fs1.f(graphFragment, "this$0");
            fs1.f(list, "data");
            this.a = z;
            this.b = list;
        }

        @Override // com.github.mikephil.charting.formatter.ValueFormatter
        public String getAxisLabel(float f, AxisBase axisBase) {
            SimpleDateFormat simpleDateFormat;
            if (f > this.b.size()) {
                return "";
            }
            if (!this.a && axisBase != null) {
                axisBase.setLabelCount(4);
            }
            simpleDateFormat = li1.a;
            Date parse = simpleDateFormat.parse(this.b.get((int) f).getOpenTimestamp());
            if (parse == null) {
                parse = new Date();
            }
            long time = parse.getTime();
            if (this.a) {
                String format = new SimpleDateFormat("MMMdd", Locale.getDefault()).format(new Date(time));
                fs1.e(format, "{\n                Simple…timeStamp))\n            }");
                return format;
            }
            String format2 = new SimpleDateFormat("hh:mmaa", Locale.getDefault()).format(new Date(time));
            fs1.e(format2, "SimpleDateFormat(DATE_FO…).format(Date(timeStamp))");
            return format2;
        }
    }

    /* compiled from: GraphFragment.kt */
    /* loaded from: classes2.dex */
    public static final class b {
        public b() {
        }

        public /* synthetic */ b(qi0 qi0Var) {
            this();
        }

        public final GraphFragment a(String str, String str2, double d, String str3, String str4, String str5, int i, String str6, int i2, boolean z, String str7) {
            fs1.f(str, PublicResolver.FUNC_NAME);
            fs1.f(str2, "symbol");
            return b(str, str2, d, str3, null, null, str4, str5, i, str6, i2, z, str7);
        }

        public final GraphFragment b(String str, String str2, double d, String str3, String str4, String str5, String str6, String str7, int i, String str8, int i2, boolean z, String str9) {
            GraphFragment graphFragment = new GraphFragment();
            Bundle bundle = new Bundle();
            bundle.putString("NAME", str);
            bundle.putString("SYMBOL", str2);
            bundle.putDouble("BALANCE", d);
            bundle.putString("CMCID", str3);
            bundle.putString("STARTDATE", str4);
            bundle.putString("INTERVAL", str5);
            bundle.putString("SLUG", str6);
            bundle.putString("CONTRACT_ADDRESS", str7);
            bundle.putInt("DEFAULT_VIEW", i);
            bundle.putString("ARG_ICON_URL", str8);
            bundle.putBoolean("ARG_SHOW_DEXSCREENER", z);
            bundle.putInt("ARG_ICON_RESID", i2);
            bundle.putString("ARG_DEXSCREENER_URL", str9);
            te4 te4Var = te4.a;
            graphFragment.setArguments(bundle);
            return graphFragment;
        }

        public final GraphFragment c(String str, String str2, String str3, String str4, String str5, int i, String str6, int i2, boolean z, String str7) {
            fs1.f(str, PublicResolver.FUNC_NAME);
            fs1.f(str2, "symbol");
            fs1.f(str3, "cmcId");
            fs1.f(str4, "slug");
            return b(str, str2, -1.0d, str3, null, null, str4, str5, i, str6, i2, z, str7);
        }
    }

    /* compiled from: GraphFragment.kt */
    /* loaded from: classes2.dex */
    public final class c extends ValueFormatter {
        public final boolean a;

        public c(GraphFragment graphFragment, boolean z) {
            fs1.f(graphFragment, "this$0");
            this.a = z;
        }

        @Override // com.github.mikephil.charting.formatter.ValueFormatter
        public String getAxisLabel(float f, AxisBase axisBase) {
            if (!this.a && axisBase != null) {
                axisBase.setLabelCount(4);
            }
            if (this.a) {
                String format = new SimpleDateFormat("MMMdd", Locale.getDefault()).format(new Date(f));
                fs1.e(format, "{\n                Simple….toLong()))\n            }");
                return format;
            }
            String format2 = new SimpleDateFormat("hh:mmaa", Locale.getDefault()).format(new Date(f));
            fs1.e(format2, "SimpleDateFormat(DATE_FO…mat(Date(value.toLong()))");
            return format2;
        }
    }

    public GraphFragment() {
        rc1 rc1Var = GraphFragment$graphViewModel$2.INSTANCE;
        this.F0 = FragmentViewModelLazyKt.a(this, d53.b(GraphViewModel.class), new GraphFragment$special$$inlined$activityViewModels$1(this), rc1Var == null ? new GraphFragment$special$$inlined$activityViewModels$2(this) : rc1Var);
        this.G0 = FragmentViewModelLazyKt.a(this, d53.b(MyTokensListViewModel.class), new GraphFragment$special$$inlined$activityViewModels$3(this), new GraphFragment$myTokenListViewModel$2(this));
        this.H0 = zy1.a(new GraphFragment$tokenInfoExistProvider$2(this));
        this.J0 = zy1.a(new GraphFragment$defaultIntervalViewLiveData$2(this));
        this.K0 = new gb2<>();
    }

    public static final void A0(GraphFragment graphFragment, View view) {
        fs1.f(graphFragment, "this$0");
        graphFragment.y0 = "1d";
        String U = graphFragment.U(2, -1);
        fs1.d(U);
        graphFragment.x0 = U;
        fs1.e(view, "it");
        graphFragment.o0(view);
        String str = graphFragment.x0;
        String str2 = null;
        if (str == null) {
            fs1.r("startDate");
            str = null;
        }
        String str3 = graphFragment.y0;
        if (str3 == null) {
            fs1.r("interval");
        } else {
            str2 = str3;
        }
        graphFragment.S(str, str2);
    }

    public static final void B0(GraphFragment graphFragment, View view) {
        fs1.f(graphFragment, "this$0");
        graphFragment.y0 = "3d";
        String U = graphFragment.U(2, -3);
        fs1.d(U);
        graphFragment.x0 = U;
        fs1.e(view, "it");
        graphFragment.o0(view);
        String str = graphFragment.x0;
        String str2 = null;
        if (str == null) {
            fs1.r("startDate");
            str = null;
        }
        String str3 = graphFragment.y0;
        if (str3 == null) {
            fs1.r("interval");
        } else {
            str2 = str3;
        }
        graphFragment.S(str, str2);
    }

    public static final void c0(GraphFragment graphFragment, CurrencyTokenInfo currencyTokenInfo) {
        fs1.f(graphFragment, "this$0");
        if (currencyTokenInfo == null) {
            return;
        }
        String priceUsd = currencyTokenInfo.getPriceUsd();
        uh1 uh1Var = null;
        Double valueOf = priceUsd == null ? null : Double.valueOf(Double.parseDouble(priceUsd));
        u21.a aVar = u21.a;
        String t = e30.t(aVar.b());
        if (valueOf == null) {
            return;
        }
        valueOf.doubleValue();
        uh1 uh1Var2 = graphFragment.E0;
        if (uh1Var2 == null) {
            fs1.r("binding");
        } else {
            uh1Var = uh1Var2;
        }
        TextView textView = uh1Var.s;
        lu3 lu3Var = lu3.a;
        String format = String.format(Locale.getDefault(), t, Arrays.copyOf(new Object[]{aVar.b(), e30.p(v21.a(valueOf.doubleValue()), 5, null, false, 6, null)}, 2));
        fs1.e(format, "java.lang.String.format(locale, format, *args)");
        textView.setText(format);
    }

    public static final void e0(GraphFragment graphFragment, CoinPriceStats coinPriceStats) {
        fs1.f(graphFragment, "this$0");
        if ((coinPriceStats == null ? null : coinPriceStats.getData()) != null && !coinPriceStats.getData().getQuote().isEmpty()) {
            graphFragment.k0(true);
            graphFragment.X(coinPriceStats.getData().getId());
            return;
        }
        graphFragment.l0();
        graphFragment.k0(false);
    }

    public static final void f0(GraphFragment graphFragment, HistoricalList historicalList) {
        fs1.f(graphFragment, "this$0");
        if ((historicalList == null ? null : historicalList.getData()) != null && !historicalList.getData().getQuotes().isEmpty()) {
            graphFragment.k0(true);
            Integer id = historicalList.getData().getId();
            fs1.e(id, "data.id");
            graphFragment.X(id.intValue());
            return;
        }
        graphFragment.m0();
        graphFragment.k0(false);
    }

    public static final void g0(GraphFragment graphFragment, Boolean bool) {
        fs1.f(graphFragment, "this$0");
        graphFragment.a0();
    }

    public static final void h0(GraphFragment graphFragment, Boolean bool) {
        fs1.f(graphFragment, "this$0");
        graphFragment.Z();
    }

    public static final void i0(View view) {
        if (view == null) {
            return;
        }
        view.performClick();
    }

    public static final void j0(GraphFragment graphFragment, View view) {
        fs1.f(graphFragment, "this$0");
        String value = graphFragment.K0.getValue();
        if (value == null) {
            return;
        }
        b30 b30Var = b30.a;
        Context requireContext = graphFragment.requireContext();
        fs1.e(requireContext, "requireContext()");
        b30Var.u(requireContext, fs1.l("https://coinmarketcap.com/currencies/", value));
    }

    public static final void t0(GraphFragment graphFragment, View view) {
        fs1.f(graphFragment, "this$0");
        graphFragment.y0 = "7d";
        String U = graphFragment.U(2, -6);
        fs1.d(U);
        graphFragment.x0 = U;
        fs1.e(view, "it");
        graphFragment.o0(view);
        String str = graphFragment.x0;
        String str2 = null;
        if (str == null) {
            fs1.r("startDate");
            str = null;
        }
        String str3 = graphFragment.y0;
        if (str3 == null) {
            fs1.r("interval");
        } else {
            str2 = str3;
        }
        graphFragment.S(str, str2);
    }

    public static final void u0(uh1 uh1Var, GraphFragment graphFragment, CompoundButton compoundButton, boolean z) {
        Window window;
        fs1.f(uh1Var, "$this_apply");
        fs1.f(graphFragment, "this$0");
        Dialog k = graphFragment.k();
        if (k == null || (window = k.getWindow()) == null) {
            return;
        }
        if (z) {
            window.setLayout(-1, -1);
            FragmentActivity activity = graphFragment.getActivity();
            if (activity != null) {
                activity.setRequestedOrientation(2);
            }
            graphFragment.n0();
            return;
        }
        graphFragment.p0();
        FragmentActivity activity2 = graphFragment.getActivity();
        if (activity2 != null) {
            activity2.setRequestedOrientation(1);
        }
        window.setLayout(-1, -2);
    }

    public static final void v0(GraphFragment graphFragment, CompoundButton compoundButton, boolean z) {
        fs1.f(graphFragment, "this$0");
        String str = graphFragment.x0;
        String str2 = null;
        if (str == null) {
            fs1.r("startDate");
            str = null;
        }
        String str3 = graphFragment.y0;
        if (str3 == null) {
            fs1.r("interval");
        } else {
            str2 = str3;
        }
        graphFragment.S(str, str2);
        graphFragment.E0();
    }

    public static final void w0(GraphFragment graphFragment, View view) {
        fs1.f(graphFragment, "this$0");
        graphFragment.h();
    }

    public static final void x0(GraphFragment graphFragment, View view) {
        fs1.f(graphFragment, "this$0");
        graphFragment.y0 = "1h";
        String U = graphFragment.U(10, -12);
        fs1.d(U);
        graphFragment.x0 = U;
        fs1.e(view, "it");
        graphFragment.o0(view);
        String str = graphFragment.x0;
        String str2 = null;
        if (str == null) {
            fs1.r("startDate");
            str = null;
        }
        String str3 = graphFragment.y0;
        if (str3 == null) {
            fs1.r("interval");
        } else {
            str2 = str3;
        }
        graphFragment.S(str, str2);
    }

    public static final void y0(GraphFragment graphFragment, View view) {
        fs1.f(graphFragment, "this$0");
        graphFragment.y0 = "1h";
        String U = graphFragment.U(10, -24);
        fs1.d(U);
        graphFragment.x0 = U;
        fs1.e(view, "it");
        graphFragment.o0(view);
        String str = graphFragment.x0;
        String str2 = null;
        if (str == null) {
            fs1.r("startDate");
            str = null;
        }
        String str3 = graphFragment.y0;
        if (str3 == null) {
            fs1.r("interval");
        } else {
            str2 = str3;
        }
        graphFragment.S(str, str2);
    }

    public static final void z0(GraphFragment graphFragment, View view) {
        fs1.f(graphFragment, "this$0");
        graphFragment.y0 = "4h";
        String U = graphFragment.U(4, -1);
        fs1.d(U);
        graphFragment.x0 = U;
        fs1.e(view, "it");
        graphFragment.o0(view);
        String str = graphFragment.x0;
        String str2 = null;
        if (str == null) {
            fs1.r("startDate");
            str = null;
        }
        String str3 = graphFragment.y0;
        if (str3 == null) {
            fs1.r("interval");
        } else {
            str2 = str3;
        }
        graphFragment.S(str, str2);
    }

    public final void C0(FragmentManager fragmentManager) {
        fs1.f(fragmentManager, "manager");
        super.u(fragmentManager, GraphFragment.class.getCanonicalName());
    }

    public final void D0() {
        uh1 uh1Var = this.E0;
        uh1 uh1Var2 = null;
        if (uh1Var == null) {
            fs1.r("binding");
            uh1Var = null;
        }
        uh1Var.h.setVisibility(4);
        uh1 uh1Var3 = this.E0;
        if (uh1Var3 == null) {
            fs1.r("binding");
            uh1Var3 = null;
        }
        uh1Var3.m.setVisibility(4);
        uh1 uh1Var4 = this.E0;
        if (uh1Var4 == null) {
            fs1.r("binding");
            uh1Var4 = null;
        }
        uh1Var4.t.setVisibility(0);
        uh1 uh1Var5 = this.E0;
        if (uh1Var5 == null) {
            fs1.r("binding");
            uh1Var5 = null;
        }
        uh1Var5.i.setVisibility(8);
        uh1 uh1Var6 = this.E0;
        if (uh1Var6 == null) {
            fs1.r("binding");
            uh1Var6 = null;
        }
        uh1Var6.n.setVisibility(8);
        uh1 uh1Var7 = this.E0;
        if (uh1Var7 == null) {
            fs1.r("binding");
            uh1Var7 = null;
        }
        uh1Var7.o.setVisibility(8);
        uh1 uh1Var8 = this.E0;
        if (uh1Var8 == null) {
            fs1.r("binding");
            uh1Var8 = null;
        }
        uh1Var8.b.setVisibility(8);
        uh1 uh1Var9 = this.E0;
        if (uh1Var9 == null) {
            fs1.r("binding");
            uh1Var9 = null;
        }
        uh1Var9.c.setVisibility(8);
        uh1 uh1Var10 = this.E0;
        if (uh1Var10 == null) {
            fs1.r("binding");
            uh1Var10 = null;
        }
        uh1Var10.e.setVisibility(8);
        uh1 uh1Var11 = this.E0;
        if (uh1Var11 == null) {
            fs1.r("binding");
            uh1Var11 = null;
        }
        uh1Var11.d.setVisibility(8);
        uh1 uh1Var12 = this.E0;
        if (uh1Var12 == null) {
            fs1.r("binding");
            uh1Var12 = null;
        }
        uh1Var12.f.setVisibility(8);
        uh1 uh1Var13 = this.E0;
        if (uh1Var13 == null) {
            fs1.r("binding");
            uh1Var13 = null;
        }
        uh1Var13.g.setVisibility(8);
        uh1 uh1Var14 = this.E0;
        if (uh1Var14 == null) {
            fs1.r("binding");
            uh1Var14 = null;
        }
        uh1Var14.r.setVisibility(8);
        String string = requireArguments().getString("ARG_ICON_URL", "");
        int i = requireArguments().getInt("ARG_ICON_RESID", 0);
        uh1 uh1Var15 = this.E0;
        if (uh1Var15 == null) {
            fs1.r("binding");
            uh1Var15 = null;
        }
        ImageView imageView = uh1Var15.l;
        fs1.e(imageView, "binding.ivDialog");
        String str = this.v0;
        if (str == null) {
            fs1.r("symbol");
            str = null;
        }
        e30.Q(imageView, i, string, str);
        uh1 uh1Var16 = this.E0;
        if (uh1Var16 == null) {
            fs1.r("binding");
            uh1Var16 = null;
        }
        uh1Var16.t.getSettings().setJavaScriptEnabled(true);
        uh1 uh1Var17 = this.E0;
        if (uh1Var17 == null) {
            fs1.r("binding");
            uh1Var17 = null;
        }
        uh1Var17.t.getSettings().setDatabaseEnabled(true);
        uh1 uh1Var18 = this.E0;
        if (uh1Var18 == null) {
            fs1.r("binding");
            uh1Var18 = null;
        }
        uh1Var18.t.getSettings().setDomStorageEnabled(true);
        uh1 uh1Var19 = this.E0;
        if (uh1Var19 == null) {
            fs1.r("binding");
            uh1Var19 = null;
        }
        uh1Var19.t.getSettings().setSupportZoom(true);
        uh1 uh1Var20 = this.E0;
        if (uh1Var20 == null) {
            fs1.r("binding");
            uh1Var20 = null;
        }
        uh1Var20.t.getSettings().setJavaScriptCanOpenWindowsAutomatically(true);
        uh1 uh1Var21 = this.E0;
        if (uh1Var21 == null) {
            fs1.r("binding");
            uh1Var21 = null;
        }
        uh1Var21.t.getSettings().setBuiltInZoomControls(true);
        uh1 uh1Var22 = this.E0;
        if (uh1Var22 == null) {
            fs1.r("binding");
            uh1Var22 = null;
        }
        uh1Var22.t.getSettings().setGeolocationEnabled(true);
        uh1 uh1Var23 = this.E0;
        if (uh1Var23 == null) {
            fs1.r("binding");
        } else {
            uh1Var2 = uh1Var23;
        }
        uh1Var2.t.loadUrl(P());
    }

    public final void E0() {
        uh1 uh1Var = this.E0;
        uh1 uh1Var2 = null;
        if (uh1Var == null) {
            fs1.r("binding");
            uh1Var = null;
        }
        if (uh1Var.i.isChecked()) {
            Z();
        } else {
            a0();
        }
        uh1 uh1Var3 = this.E0;
        if (uh1Var3 == null) {
            fs1.r("binding");
            uh1Var3 = null;
        }
        CandleStickChart candleStickChart = uh1Var3.h;
        uh1 uh1Var4 = this.E0;
        if (uh1Var4 == null) {
            fs1.r("binding");
            uh1Var4 = null;
        }
        candleStickChart.setVisibility(uh1Var4.i.isChecked() ? 0 : 4);
        uh1 uh1Var5 = this.E0;
        if (uh1Var5 == null) {
            fs1.r("binding");
            uh1Var5 = null;
        }
        LineChart lineChart = uh1Var5.m;
        uh1 uh1Var6 = this.E0;
        if (uh1Var6 == null) {
            fs1.r("binding");
        } else {
            uh1Var2 = uh1Var6;
        }
        lineChart.setVisibility(uh1Var2.i.isChecked() ? 4 : 0);
    }

    public final String L() {
        return (String) this.z0.getValue();
    }

    public final String M() {
        return (String) this.B0.getValue();
    }

    public final String N() {
        return (String) this.A0.getValue();
    }

    public final gb2<View> O() {
        return (gb2) this.J0.getValue();
    }

    public final String P() {
        return fs1.l(requireArguments().getString("ARG_DEXSCREENER_URL", ""), "?embed=1&theme=dark&trades=0&info=0");
    }

    public final String Q() {
        SimpleDateFormat simpleDateFormat;
        Calendar calendar = Calendar.getInstance();
        simpleDateFormat = li1.a;
        String format = simpleDateFormat.format(calendar.getTime());
        fs1.e(format, "FORMAT_SIMPLE_DATE.format(timeNow.time)");
        return format;
    }

    public final GraphViewModel R() {
        return (GraphViewModel) this.F0.getValue();
    }

    public final void S(String str, String str2) {
        String str3;
        String str4;
        if (this.v0 == null) {
            fs1.r("symbol");
        }
        if (str == null || str2 == null) {
            return;
        }
        String str5 = fs1.b(str2, "1h") ? "hourly" : "daily";
        this.y0 = str2;
        this.D0 = dv3.t(str5, "daily", true);
        uh1 uh1Var = this.E0;
        if (uh1Var == null) {
            fs1.r("binding");
            uh1Var = null;
        }
        if (uh1Var.i.isChecked()) {
            l0();
            uh1 uh1Var2 = this.E0;
            if (uh1Var2 == null) {
                fs1.r("binding");
                uh1Var2 = null;
            }
            uh1Var2.h.setNoDataText(getString(R.string.chart_loading));
            GraphViewModel R = R();
            String str6 = this.v0;
            if (str6 == null) {
                fs1.r("symbol");
                str4 = null;
            } else {
                str4 = str6;
            }
            R.e(str4, L(), str, Q(), str5);
            return;
        }
        m0();
        uh1 uh1Var3 = this.E0;
        if (uh1Var3 == null) {
            fs1.r("binding");
            uh1Var3 = null;
        }
        uh1Var3.m.setNoDataText(getString(R.string.chart_loading));
        GraphViewModel R2 = R();
        String str7 = this.v0;
        if (str7 == null) {
            fs1.r("symbol");
            str3 = null;
        } else {
            str3 = str7;
        }
        R2.f(str3, L(), str, Q(), str2);
    }

    public final void T() {
        String N = N();
        fs1.e(N, "tokenAddress");
        if (N.length() == 0) {
            TokenType.a aVar = TokenType.Companion;
            String str = this.v0;
            if (str == null) {
                fs1.r("symbol");
                str = null;
            }
            N = aVar.a(str).getWrapAddress();
        }
        if (N == null) {
            return;
        }
        if (N.length() > 0) {
            R().g(N);
        }
    }

    public final String U(int i, int i2) {
        SimpleDateFormat simpleDateFormat;
        Calendar calendar = Calendar.getInstance();
        calendar.add(i, i2);
        calendar.set(13, 0);
        calendar.set(14, 0);
        calendar.add(12, -(calendar.get(12) % 10));
        simpleDateFormat = li1.a;
        return simpleDateFormat.format(calendar.getTime());
    }

    public final TokenInfoExistProvider V() {
        return (TokenInfoExistProvider) this.H0.getValue();
    }

    public final String W() {
        return (String) this.C0.getValue();
    }

    public final void X(int i) {
        uh1 uh1Var = null;
        String str = null;
        if (Y()) {
            String string = requireArguments().getString("ARG_ICON_URL", "");
            int i2 = requireArguments().getInt("ARG_ICON_RESID", 0);
            uh1 uh1Var2 = this.E0;
            if (uh1Var2 == null) {
                fs1.r("binding");
                uh1Var2 = null;
            }
            ImageView imageView = uh1Var2.l;
            fs1.e(imageView, "binding.ivDialog");
            String str2 = this.v0;
            if (str2 == null) {
                fs1.r("symbol");
            } else {
                str = str2;
            }
            e30.Q(imageView, i2, string, str);
        } else {
            uh1 uh1Var3 = this.E0;
            if (uh1Var3 == null) {
                fs1.r("binding");
                uh1Var3 = null;
            }
            k73 u = com.bumptech.glide.a.u(uh1Var3.l);
            String str3 = this.v0;
            if (str3 == null) {
                fs1.r("symbol");
                str3 = null;
            }
            e<Drawable> a2 = u.x(a4.f(i, str3)).a(n73.v0());
            uh1 uh1Var4 = this.E0;
            if (uh1Var4 == null) {
                fs1.r("binding");
            } else {
                uh1Var = uh1Var4;
            }
            a2.I0(uh1Var.l);
        }
        E0();
    }

    public final boolean Y() {
        String str = this.v0;
        if (str == null) {
            fs1.r("symbol");
            str = null;
        }
        String lowerCase = str.toLowerCase(Locale.ROOT);
        fs1.e(lowerCase, "(this as java.lang.Strin….toLowerCase(Locale.ROOT)");
        switch (lowerCase.hashCode()) {
            case -2095018025:
                return lowerCase.equals("aquagoat");
            case 97686:
                return lowerCase.equals("bnb");
            case 113786:
                return lowerCase.equals("sfm");
            case 1765043214:
                return lowerCase.equals("safemoon");
            default:
                return false;
        }
    }

    public final void Z() {
        CoinPriceStatsData data;
        CoinPriceStats value = R().b().getValue();
        List<CoinPriceStatsDataQuoteList> quote = (value == null || (data = value.getData()) == null) ? null : data.getQuote();
        if (quote == null || quote.isEmpty()) {
            return;
        }
        try {
            double openValue = quote.get(0).getQuote().getUsd().getOpenValue();
            double closeValue = quote.get(quote.size() - 1).getQuote().getUsd().getCloseValue();
            u21.a aVar = u21.a;
            String t = e30.t(aVar.b());
            uh1 uh1Var = this.E0;
            if (uh1Var == null) {
                fs1.r("binding");
                uh1Var = null;
            }
            TextView textView = uh1Var.s;
            lu3 lu3Var = lu3.a;
            String format = String.format(Locale.getDefault(), t, Arrays.copyOf(new Object[]{aVar.b(), e30.p(closeValue, 2, null, false, 6, null)}, 2));
            fs1.e(format, "java.lang.String.format(locale, format, *args)");
            textView.setText(format);
            double d = ((closeValue - openValue) / openValue) * 100;
            uh1 uh1Var2 = this.E0;
            if (uh1Var2 == null) {
                fs1.r("binding");
                uh1Var2 = null;
            }
            uh1Var2.r.setText(fs1.l(e30.m(d), "%"));
            int i = d < Utils.DOUBLE_EPSILON ? R.drawable.arrow_down : R.drawable.arrow_up;
            uh1 uh1Var3 = this.E0;
            if (uh1Var3 == null) {
                fs1.r("binding");
                uh1Var3 = null;
            }
            e<Drawable> w = com.bumptech.glide.a.u(uh1Var3.a).w(Integer.valueOf(i));
            uh1 uh1Var4 = this.E0;
            if (uh1Var4 == null) {
                fs1.r("binding");
                uh1Var4 = null;
            }
            w.I0(uh1Var4.a);
        } catch (Exception e) {
            String localizedMessage = e.getLocalizedMessage();
            fs1.e(localizedMessage, "e.localizedMessage");
            e30.c0(localizedMessage, "GraphPercentage");
        }
        ArrayList arrayList = new ArrayList();
        int i2 = 0;
        for (CoinPriceStatsDataQuoteList coinPriceStatsDataQuoteList : quote) {
            int i3 = i2 + 1;
            try {
                CoinPriceStatsUsd usd = coinPriceStatsDataQuoteList.getQuote().getUsd();
                arrayList.add(new CandleEntry(i2, usd.getHighValue(), usd.getLowValue(), usd.getOpenValue(), usd.getCloseValue()));
            } catch (Exception e2) {
                String localizedMessage2 = e2.getLocalizedMessage();
                fs1.e(localizedMessage2, "E.localizedMessage");
                String simpleName = GraphFragment.class.getSimpleName();
                fs1.e(simpleName, "this::class.java.simpleName");
                e30.c0(localizedMessage2, simpleName);
            }
            i2 = i3;
        }
        String str = this.u0;
        if (str == null) {
            fs1.r("tokenName");
            str = null;
        }
        CandleDataSet candleDataSet = new CandleDataSet(arrayList, str);
        Boolean value2 = R().h().getValue();
        if (value2 == null) {
            value2 = Boolean.FALSE;
        }
        candleDataSet.setDrawValues(value2.booleanValue());
        candleDataSet.setColor(Color.rgb(80, 80, 80));
        candleDataSet.setShadowColor(m70.d(requireContext(), R.color.black));
        candleDataSet.setShadowWidth(1.0f);
        candleDataSet.setDecreasingColor(m70.d(requireContext(), R.color.candle_red));
        candleDataSet.setDecreasingPaintStyle(Paint.Style.FILL_AND_STROKE);
        candleDataSet.setIncreasingColor(m70.d(requireContext(), R.color.candle_green));
        candleDataSet.setIncreasingPaintStyle(Paint.Style.FILL_AND_STROKE);
        candleDataSet.setNeutralColor(-3355444);
        candleDataSet.setShowCandleBar(true);
        uh1 uh1Var5 = this.E0;
        if (uh1Var5 == null) {
            fs1.r("binding");
            uh1Var5 = null;
        }
        CandleStickChart candleStickChart = uh1Var5.h;
        CandleGraphMarkerView candleGraphMarkerView = new CandleGraphMarkerView(requireContext(), R.layout.marker_graph_view, quote, W());
        uh1 uh1Var6 = this.E0;
        if (uh1Var6 == null) {
            fs1.r("binding");
            uh1Var6 = null;
        }
        candleGraphMarkerView.setChartView(uh1Var6.h);
        te4 te4Var = te4.a;
        candleStickChart.setMarker(candleGraphMarkerView);
        candleStickChart.getXAxis().setValueFormatter(new a(this, this.D0, quote));
        candleStickChart.getXAxis().setTextColor(candleStickChart.getResources().getColor(R.color.t1));
        candleStickChart.getAxisLeft().setTextColor(candleStickChart.getResources().getColor(R.color.t1));
        candleStickChart.setData(new CandleData(candleDataSet));
        candleStickChart.invalidate();
    }

    public final void a0() {
        Data data;
        HistoricalList value = R().c().getValue();
        uh1 uh1Var = null;
        List<HistoricalDatum> quotes = (value == null || (data = value.getData()) == null) ? null : data.getQuotes();
        if (quotes != null) {
            try {
                Double price = quotes.get(0).getQuote().getUSD().getPrice();
                Double price2 = quotes.get(quotes.size() - 1).getQuote().getUSD().getPrice();
                u21.a aVar = u21.a;
                String t = e30.t(aVar.b());
                uh1 uh1Var2 = this.E0;
                if (uh1Var2 == null) {
                    fs1.r("binding");
                    uh1Var2 = null;
                }
                TextView textView = uh1Var2.s;
                lu3 lu3Var = lu3.a;
                Locale locale = Locale.getDefault();
                fs1.e(price2, "latest");
                String format = String.format(locale, t, Arrays.copyOf(new Object[]{aVar.b(), e30.p(price2.doubleValue(), 2, null, false, 6, null)}, 2));
                fs1.e(format, "java.lang.String.format(locale, format, *args)");
                textView.setText(format);
                double doubleValue = price2.doubleValue();
                fs1.e(price, "old");
                double doubleValue2 = ((doubleValue - price.doubleValue()) / price.doubleValue()) * 100;
                uh1 uh1Var3 = this.E0;
                if (uh1Var3 == null) {
                    fs1.r("binding");
                    uh1Var3 = null;
                }
                uh1Var3.r.setText(fs1.l(e30.m(doubleValue2), "%"));
                int i = doubleValue2 < Utils.DOUBLE_EPSILON ? R.drawable.arrow_down : R.drawable.arrow_up;
                uh1 uh1Var4 = this.E0;
                if (uh1Var4 == null) {
                    fs1.r("binding");
                    uh1Var4 = null;
                }
                e<Drawable> w = com.bumptech.glide.a.u(uh1Var4.a).w(Integer.valueOf(i));
                uh1 uh1Var5 = this.E0;
                if (uh1Var5 == null) {
                    fs1.r("binding");
                    uh1Var5 = null;
                }
                w.I0(uh1Var5.a);
            } catch (Exception e) {
                String localizedMessage = e.getLocalizedMessage();
                fs1.e(localizedMessage, "e.localizedMessage");
                e30.c0(localizedMessage, "GraphPercentage");
            }
            ArrayList arrayList = new ArrayList();
            int size = quotes.size() - 1;
            if (size >= 0) {
                int i2 = 0;
                while (true) {
                    int i3 = i2 + 1;
                    try {
                        USDT usd = quotes.get(i2).getQuote().getUSD();
                        re0 re0Var = re0.a;
                        String timestamp = usd.getTimestamp();
                        fs1.e(timestamp, "usdt.timestamp");
                        arrayList.add(new Entry((float) re0Var.b(timestamp), (float) usd.getPrice().doubleValue()));
                    } catch (Exception unused) {
                    }
                    if (i3 > size) {
                        break;
                    }
                    i2 = i3;
                }
            }
            String str = this.u0;
            if (str == null) {
                fs1.r("tokenName");
                str = null;
            }
            LineDataSet lineDataSet = new LineDataSet(arrayList, str);
            lineDataSet.setLineWidth(2.0f);
            lineDataSet.setColor(m70.d(requireContext(), R.color.curve_green));
            Boolean value2 = R().i().getValue();
            if (value2 == null) {
                value2 = Boolean.FALSE;
            }
            lineDataSet.setDrawValues(value2.booleanValue());
            LineData lineData = new LineData(lineDataSet);
            uh1 uh1Var6 = this.E0;
            if (uh1Var6 == null) {
                fs1.r("binding");
                uh1Var6 = null;
            }
            LineChart lineChart = uh1Var6.m;
            lineChart.clear();
            lineChart.setClipValuesToContent(true);
            lineChart.setData(lineData);
            lineChart.getDescription().setEnabled(false);
            lineChart.getXAxis().setEnabled(true);
            lineChart.getAxisRight().setEnabled(false);
            lineChart.getXAxis().setValueFormatter(new c(this, this.D0));
            uh1 uh1Var7 = this.E0;
            if (uh1Var7 == null) {
                fs1.r("binding");
            } else {
                uh1Var = uh1Var7;
            }
            uh1Var.m.notifyDataSetChanged();
        }
    }

    public final void b0() {
        R().d().observe(getViewLifecycleOwner(), new tl2() { // from class: fi1
            @Override // defpackage.tl2
            public final void onChanged(Object obj) {
                GraphFragment.c0(GraphFragment.this, (CurrencyTokenInfo) obj);
            }
        });
    }

    public final void d0() {
        R().b().observe(getViewLifecycleOwner(), new tl2() { // from class: ei1
            @Override // defpackage.tl2
            public final void onChanged(Object obj) {
                GraphFragment.e0(GraphFragment.this, (CoinPriceStats) obj);
            }
        });
        R().c().observe(getViewLifecycleOwner(), new tl2() { // from class: di1
            @Override // defpackage.tl2
            public final void onChanged(Object obj) {
                GraphFragment.f0(GraphFragment.this, (HistoricalList) obj);
            }
        });
        R().i().observe(getViewLifecycleOwner(), new tl2() { // from class: ci1
            @Override // defpackage.tl2
            public final void onChanged(Object obj) {
                GraphFragment.g0(GraphFragment.this, (Boolean) obj);
            }
        });
        R().h().observe(getViewLifecycleOwner(), new tl2() { // from class: vh1
            @Override // defpackage.tl2
            public final void onChanged(Object obj) {
                GraphFragment.h0(GraphFragment.this, (Boolean) obj);
            }
        });
        q02.a(O(), this, gi1.a);
    }

    public final void k0(boolean z) {
        uh1 uh1Var = this.E0;
        uh1 uh1Var2 = null;
        if (uh1Var == null) {
            fs1.r("binding");
            uh1Var = null;
        }
        ImageView imageView = uh1Var.l;
        if (imageView != null) {
            imageView.setVisibility(z ? 0 : 8);
        }
        uh1 uh1Var3 = this.E0;
        if (uh1Var3 == null) {
            fs1.r("binding");
            uh1Var3 = null;
        }
        ImageView imageView2 = uh1Var3.a;
        if (imageView2 != null) {
            imageView2.setVisibility(z ? 0 : 8);
        }
        uh1 uh1Var4 = this.E0;
        if (uh1Var4 == null) {
            fs1.r("binding");
            uh1Var4 = null;
        }
        TextView textView = uh1Var4.q;
        if (textView != null) {
            textView.setVisibility(z ? 0 : 8);
        }
        uh1 uh1Var5 = this.E0;
        if (uh1Var5 == null) {
            fs1.r("binding");
            uh1Var5 = null;
        }
        TextView textView2 = uh1Var5.s;
        if (textView2 != null) {
            textView2.setVisibility(z ? 0 : 8);
        }
        uh1 uh1Var6 = this.E0;
        if (uh1Var6 == null) {
            fs1.r("binding");
            uh1Var6 = null;
        }
        TextView textView3 = uh1Var6.p;
        if (textView3 != null) {
            textView3.setVisibility(z ? 0 : 8);
        }
        uh1 uh1Var7 = this.E0;
        if (uh1Var7 == null) {
            fs1.r("binding");
            uh1Var7 = null;
        }
        TextView textView4 = uh1Var7.r;
        if (textView4 != null) {
            textView4.setVisibility(z ? 0 : 8);
        }
        if (!(this.w0 == -1.0d)) {
            uh1 uh1Var8 = this.E0;
            if (uh1Var8 == null) {
                fs1.r("binding");
            } else {
                uh1Var2 = uh1Var8;
            }
            uh1Var2.p.setVisibility(0);
            return;
        }
        uh1 uh1Var9 = this.E0;
        if (uh1Var9 == null) {
            fs1.r("binding");
        } else {
            uh1Var2 = uh1Var9;
        }
        uh1Var2.p.setVisibility(8);
    }

    public final void l0() {
        uh1 uh1Var = this.E0;
        if (uh1Var == null) {
            fs1.r("binding");
            uh1Var = null;
        }
        CandleStickChart candleStickChart = uh1Var.h;
        candleStickChart.setNoDataText(getString(R.string.chart_no_data));
        candleStickChart.fitScreen();
        CandleData candleData = (CandleData) candleStickChart.getData();
        if (candleData != null) {
            candleData.clearValues();
        }
        candleStickChart.notifyDataSetChanged();
        candleStickChart.clear();
        candleStickChart.invalidate();
    }

    @Override // defpackage.sn0
    public Dialog m(Bundle bundle) {
        Dialog m = super.m(bundle);
        fs1.e(m, "super.onCreateDialog(savedInstanceState)");
        m.requestWindowFeature(1);
        Window window = m.getWindow();
        if (window != null) {
            window.setBackgroundDrawable(new ColorDrawable(0));
        }
        Window window2 = m.getWindow();
        if (window2 != null) {
            window2.setLayout(-1, -2);
        }
        return m;
    }

    public final void m0() {
        uh1 uh1Var = this.E0;
        if (uh1Var == null) {
            fs1.r("binding");
            uh1Var = null;
        }
        LineChart lineChart = uh1Var.m;
        lineChart.setNoDataText(getString(R.string.chart_no_data));
        lineChart.setData(null);
        lineChart.fitScreen();
        LineData lineData = (LineData) lineChart.getData();
        if (lineData != null) {
            lineData.clearValues();
        }
        lineChart.notifyDataSetChanged();
        lineChart.clear();
        lineChart.invalidate();
    }

    public final void n0() {
        uh1 uh1Var = this.E0;
        uh1 uh1Var2 = null;
        if (uh1Var == null) {
            fs1.r("binding");
            uh1Var = null;
        }
        if (uh1Var.r.getVisibility() == 8) {
            uh1 uh1Var3 = this.E0;
            if (uh1Var3 == null) {
                fs1.r("binding");
                uh1Var3 = null;
            }
            uh1Var3.r.setVisibility(4);
        }
        uh1 uh1Var4 = this.E0;
        if (uh1Var4 == null) {
            fs1.r("binding");
            uh1Var4 = null;
        }
        ViewGroup.LayoutParams layoutParams = uh1Var4.m.getLayoutParams();
        if (layoutParams != null) {
            layoutParams.height = 0;
        }
        uh1 uh1Var5 = this.E0;
        if (uh1Var5 == null) {
            fs1.r("binding");
            uh1Var5 = null;
        }
        ViewGroup.LayoutParams layoutParams2 = uh1Var5.h.getLayoutParams();
        if (layoutParams2 != null) {
            layoutParams2.height = 0;
        }
        uh1 uh1Var6 = this.E0;
        if (uh1Var6 == null) {
            fs1.r("binding");
            uh1Var6 = null;
        }
        ViewGroup.LayoutParams layoutParams3 = uh1Var6.t.getLayoutParams();
        if (layoutParams3 != null) {
            layoutParams3.height = 0;
        }
        uh1 uh1Var7 = this.E0;
        if (uh1Var7 == null) {
            fs1.r("binding");
            uh1Var7 = null;
        }
        uh1Var7.m.requestLayout();
        uh1 uh1Var8 = this.E0;
        if (uh1Var8 == null) {
            fs1.r("binding");
            uh1Var8 = null;
        }
        uh1Var8.h.requestLayout();
        uh1 uh1Var9 = this.E0;
        if (uh1Var9 == null) {
            fs1.r("binding");
        } else {
            uh1Var2 = uh1Var9;
        }
        uh1Var2.t.requestLayout();
    }

    public final void o0(View view) {
        Button[] buttonArr = new Button[6];
        uh1 uh1Var = this.E0;
        uh1 uh1Var2 = null;
        if (uh1Var == null) {
            fs1.r("binding");
            uh1Var = null;
        }
        AppCompatButton appCompatButton = uh1Var.b;
        fs1.e(appCompatButton, "binding.btn12Hours");
        buttonArr[0] = appCompatButton;
        uh1 uh1Var3 = this.E0;
        if (uh1Var3 == null) {
            fs1.r("binding");
            uh1Var3 = null;
        }
        AppCompatButton appCompatButton2 = uh1Var3.c;
        fs1.e(appCompatButton2, "binding.btn1Day");
        buttonArr[1] = appCompatButton2;
        uh1 uh1Var4 = this.E0;
        if (uh1Var4 == null) {
            fs1.r("binding");
            uh1Var4 = null;
        }
        AppCompatButton appCompatButton3 = uh1Var4.e;
        fs1.e(appCompatButton3, "binding.btn1Week");
        buttonArr[2] = appCompatButton3;
        uh1 uh1Var5 = this.E0;
        if (uh1Var5 == null) {
            fs1.r("binding");
            uh1Var5 = null;
        }
        AppCompatButton appCompatButton4 = uh1Var5.d;
        fs1.e(appCompatButton4, "binding.btn1Month");
        buttonArr[3] = appCompatButton4;
        uh1 uh1Var6 = this.E0;
        if (uh1Var6 == null) {
            fs1.r("binding");
            uh1Var6 = null;
        }
        AppCompatButton appCompatButton5 = uh1Var6.f;
        fs1.e(appCompatButton5, "binding.btn3Months");
        buttonArr[4] = appCompatButton5;
        uh1 uh1Var7 = this.E0;
        if (uh1Var7 == null) {
            fs1.r("binding");
        } else {
            uh1Var2 = uh1Var7;
        }
        AppCompatButton appCompatButton6 = uh1Var2.g;
        fs1.e(appCompatButton6, "binding.btn6Months");
        buttonArr[5] = appCompatButton6;
        ArrayList c2 = b20.c(buttonArr);
        ArrayList<Button> arrayList = new ArrayList();
        for (Object obj : c2) {
            if (!fs1.b(view, (Button) obj)) {
                arrayList.add(obj);
            }
        }
        for (Button button : arrayList) {
            button.setPaintFlags(button.getPaintFlags() & (-9));
        }
        ((Button) view).setPaintFlags(8);
    }

    @Override // defpackage.sn0, androidx.fragment.app.Fragment
    public void onAttach(Context context) {
        fs1.f(context, "context");
        super.onAttach(context);
    }

    @Override // androidx.fragment.app.Fragment, android.content.ComponentCallbacks
    public void onConfigurationChanged(Configuration configuration) {
        fs1.f(configuration, "newConfig");
        super.onConfigurationChanged(configuration);
    }

    @Override // defpackage.sn0, androidx.fragment.app.Fragment
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        s(0, 2132017638);
        Bundle requireArguments = requireArguments();
        String string = requireArguments.getString("NAME");
        fs1.d(string);
        fs1.e(string, "it.getString(ARG_NAME)!!");
        this.u0 = string;
        String string2 = requireArguments.getString("SYMBOL");
        fs1.d(string2);
        fs1.e(string2, "it.getString(ARG_SYMBOL)!!");
        this.v0 = string2;
        this.w0 = requireArguments.getDouble("BALANCE");
        String string3 = requireArguments.getString("STARTDATE", null);
        if (string3 == null) {
            string3 = U(10, -12);
            fs1.d(string3);
        }
        this.x0 = string3;
        String string4 = requireArguments.getString("INTERVAL", null);
        if (string4 == null) {
            string4 = "1h";
        }
        this.y0 = string4;
    }

    @Override // androidx.fragment.app.Fragment
    public View onCreateView(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle) {
        fs1.f(layoutInflater, "inflater");
        View inflate = LayoutInflater.from(requireContext()).inflate(R.layout.graph_dialog, viewGroup, false);
        this.I0 = inflate;
        return inflate;
    }

    @Override // defpackage.sn0, android.content.DialogInterface.OnDismissListener
    public void onDismiss(DialogInterface dialogInterface) {
        fs1.f(dialogInterface, "dialog");
        super.onDismiss(dialogInterface);
        FragmentActivity activity = getActivity();
        if (activity == null) {
            return;
        }
        activity.setRequestedOrientation(1);
    }

    @Override // androidx.fragment.app.Fragment
    public void onViewCreated(View view, Bundle bundle) {
        fs1.f(view, "view");
        super.onViewCreated(view, bundle);
        uh1 a2 = uh1.a(view);
        fs1.e(a2, "bind(view)");
        this.E0 = a2;
        s0();
        uh1 uh1Var = this.E0;
        uh1 uh1Var2 = null;
        if (uh1Var == null) {
            fs1.r("binding");
            uh1Var = null;
        }
        TextView textView = uh1Var.q;
        String str = this.u0;
        if (str == null) {
            fs1.r("tokenName");
            str = null;
        }
        textView.setText(str);
        double d = this.w0;
        if (!(d == -1.0d)) {
            if (d > 1.0d) {
                uh1 uh1Var3 = this.E0;
                if (uh1Var3 == null) {
                    fs1.r("binding");
                    uh1Var3 = null;
                }
                TextView textView2 = uh1Var3.p;
                fs1.e(textView2, "binding.tvBalanceDialog");
                e30.S(textView2, this.w0, 2);
            } else {
                uh1 uh1Var4 = this.E0;
                if (uh1Var4 == null) {
                    fs1.r("binding");
                    uh1Var4 = null;
                }
                TextView textView3 = uh1Var4.p;
                fs1.e(textView3, "binding.tvBalanceDialog");
                e30.R(textView3, this.w0);
            }
            uh1 uh1Var5 = this.E0;
            if (uh1Var5 == null) {
                fs1.r("binding");
                uh1Var5 = null;
            }
            uh1Var5.p.setVisibility(0);
        } else {
            uh1 uh1Var6 = this.E0;
            if (uh1Var6 == null) {
                fs1.r("binding");
                uh1Var6 = null;
            }
            uh1Var6.p.setVisibility(8);
        }
        if (requireArguments().getBoolean("ARG_SHOW_DEXSCREENER", false)) {
            D0();
            p0();
            b0();
            T();
            return;
        }
        q0();
        r0();
        d0();
        String str2 = this.x0;
        if (str2 == null) {
            fs1.r("startDate");
            str2 = null;
        }
        String str3 = this.y0;
        if (str3 == null) {
            fs1.r("interval");
            str3 = null;
        }
        S(str2, str3);
        p0();
        if (M() == null) {
            TokenInfoExistProvider V = V();
            String L = L();
            V.e(L == null ? null : cv3.l(L), this.K0);
        } else {
            this.K0.postValue(M());
        }
        uh1 uh1Var7 = this.E0;
        if (uh1Var7 == null) {
            fs1.r("binding");
        } else {
            uh1Var2 = uh1Var7;
        }
        uh1Var2.o.setOnClickListener(new View.OnClickListener() { // from class: ki1
            @Override // android.view.View.OnClickListener
            public final void onClick(View view2) {
                GraphFragment.j0(GraphFragment.this, view2);
            }
        });
    }

    @Override // defpackage.sn0, androidx.fragment.app.Fragment
    public void onViewStateRestored(Bundle bundle) {
        super.onViewStateRestored(bundle);
    }

    public final void p0() {
        Resources resources;
        Resources resources2;
        uh1 uh1Var = this.E0;
        uh1 uh1Var2 = null;
        if (uh1Var == null) {
            fs1.r("binding");
            uh1Var = null;
        }
        if (uh1Var.r.getVisibility() == 4) {
            uh1 uh1Var3 = this.E0;
            if (uh1Var3 == null) {
                fs1.r("binding");
                uh1Var3 = null;
            }
            uh1Var3.r.setVisibility(8);
        }
        FragmentActivity activity = getActivity();
        Float valueOf = (activity == null || (resources = activity.getResources()) == null) ? null : Float.valueOf(resources.getDimension(R.dimen.my_250dp));
        FragmentActivity activity2 = getActivity();
        Float valueOf2 = (activity2 == null || (resources2 = activity2.getResources()) == null) ? null : Float.valueOf(resources2.getDimension(R.dimen._350sdp));
        uh1 uh1Var4 = this.E0;
        if (uh1Var4 == null) {
            fs1.r("binding");
            uh1Var4 = null;
        }
        ViewGroup.LayoutParams layoutParams = uh1Var4.m.getLayoutParams();
        if (layoutParams != null) {
            layoutParams.height = (valueOf == null ? null : Integer.valueOf((int) valueOf.floatValue())).intValue();
        }
        uh1 uh1Var5 = this.E0;
        if (uh1Var5 == null) {
            fs1.r("binding");
            uh1Var5 = null;
        }
        ViewGroup.LayoutParams layoutParams2 = uh1Var5.h.getLayoutParams();
        if (layoutParams2 != null) {
            layoutParams2.height = (valueOf == null ? null : Integer.valueOf((int) valueOf.floatValue())).intValue();
        }
        uh1 uh1Var6 = this.E0;
        if (uh1Var6 == null) {
            fs1.r("binding");
            uh1Var6 = null;
        }
        ViewGroup.LayoutParams layoutParams3 = uh1Var6.t.getLayoutParams();
        if (layoutParams3 != null) {
            layoutParams3.height = (valueOf2 == null ? null : Integer.valueOf((int) valueOf2.floatValue())).intValue();
        }
        uh1 uh1Var7 = this.E0;
        if (uh1Var7 == null) {
            fs1.r("binding");
            uh1Var7 = null;
        }
        uh1Var7.m.requestLayout();
        uh1 uh1Var8 = this.E0;
        if (uh1Var8 == null) {
            fs1.r("binding");
            uh1Var8 = null;
        }
        uh1Var8.h.requestLayout();
        uh1 uh1Var9 = this.E0;
        if (uh1Var9 == null) {
            fs1.r("binding");
        } else {
            uh1Var2 = uh1Var9;
        }
        uh1Var2.t.requestLayout();
    }

    public final void q0() {
        uh1 uh1Var = this.E0;
        if (uh1Var == null) {
            fs1.r("binding");
            uh1Var = null;
        }
        CandleStickChart candleStickChart = uh1Var.h;
        candleStickChart.setNoDataText(getString(R.string.chart_loading));
        candleStickChart.setDrawBorders(false);
        candleStickChart.getDescription().setEnabled(false);
        candleStickChart.getXAxis().setEnabled(true);
        candleStickChart.getAxisRight().setEnabled(false);
        candleStickChart.setClipValuesToContent(true);
        candleStickChart.setTouchEnabled(true);
        candleStickChart.getLegend().setEnabled(false);
        candleStickChart.getXAxis().setPosition(XAxis.XAxisPosition.BOTTOM);
    }

    public final void r0() {
        uh1 uh1Var = this.E0;
        uh1 uh1Var2 = null;
        if (uh1Var == null) {
            fs1.r("binding");
            uh1Var = null;
        }
        LineChart lineChart = uh1Var.m;
        lineChart.setTouchEnabled(true);
        lineChart.setDrawBorders(false);
        lineChart.setNoDataText(getString(R.string.chart_loading));
        LineGraphMarkerView lineGraphMarkerView = new LineGraphMarkerView(requireContext(), R.layout.marker_graph_view, W());
        uh1 uh1Var3 = this.E0;
        if (uh1Var3 == null) {
            fs1.r("binding");
        } else {
            uh1Var2 = uh1Var3;
        }
        lineGraphMarkerView.setChartView(uh1Var2.m);
        te4 te4Var = te4.a;
        lineChart.setMarker(lineGraphMarkerView);
        lineChart.getLegend().setEnabled(false);
        lineChart.getXAxis().setPosition(XAxis.XAxisPosition.BOTTOM);
        lineChart.getXAxis().setTextColor(lineChart.getResources().getColor(R.color.t1));
        lineChart.getAxisLeft().setTextColor(lineChart.getResources().getColor(R.color.t1));
    }

    public final void s0() {
        final uh1 uh1Var = this.E0;
        if (uh1Var == null) {
            fs1.r("binding");
            uh1Var = null;
        }
        uh1Var.k.setOnClickListener(new View.OnClickListener() { // from class: ji1
            @Override // android.view.View.OnClickListener
            public final void onClick(View view) {
                GraphFragment.w0(GraphFragment.this, view);
            }
        });
        uh1Var.b.setPaintFlags(8);
        uh1Var.b.setOnClickListener(new View.OnClickListener() { // from class: yh1
            @Override // android.view.View.OnClickListener
            public final void onClick(View view) {
                GraphFragment.x0(GraphFragment.this, view);
            }
        });
        uh1Var.c.setPaintFlags(8);
        uh1Var.c.setOnClickListener(new View.OnClickListener() { // from class: ii1
            @Override // android.view.View.OnClickListener
            public final void onClick(View view) {
                GraphFragment.y0(GraphFragment.this, view);
            }
        });
        uh1Var.e.setPaintFlags(8);
        uh1Var.e.setOnClickListener(new View.OnClickListener() { // from class: wh1
            @Override // android.view.View.OnClickListener
            public final void onClick(View view) {
                GraphFragment.z0(GraphFragment.this, view);
            }
        });
        uh1Var.d.setPaintFlags(8);
        uh1Var.d.setOnClickListener(new View.OnClickListener() { // from class: xh1
            @Override // android.view.View.OnClickListener
            public final void onClick(View view) {
                GraphFragment.A0(GraphFragment.this, view);
            }
        });
        uh1Var.f.setPaintFlags(8);
        uh1Var.f.setOnClickListener(new View.OnClickListener() { // from class: hi1
            @Override // android.view.View.OnClickListener
            public final void onClick(View view) {
                GraphFragment.B0(GraphFragment.this, view);
            }
        });
        uh1Var.g.setPaintFlags(8);
        uh1Var.g.setOnClickListener(new View.OnClickListener() { // from class: zh1
            @Override // android.view.View.OnClickListener
            public final void onClick(View view) {
                GraphFragment.t0(GraphFragment.this, view);
            }
        });
        uh1Var.j.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() { // from class: ai1
            @Override // android.widget.CompoundButton.OnCheckedChangeListener
            public final void onCheckedChanged(CompoundButton compoundButton, boolean z) {
                GraphFragment.u0(uh1.this, this, compoundButton, z);
            }
        });
        uh1Var.i.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() { // from class: bi1
            @Override // android.widget.CompoundButton.OnCheckedChangeListener
            public final void onCheckedChanged(CompoundButton compoundButton, boolean z) {
                GraphFragment.v0(GraphFragment.this, compoundButton, z);
            }
        });
        AppCompatButton appCompatButton = uh1Var.b;
        fs1.e(appCompatButton, "btn12Hours");
        o0(appCompatButton);
    }
}
