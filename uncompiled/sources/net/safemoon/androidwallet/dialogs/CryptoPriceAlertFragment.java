package net.safemoon.androidwallet.dialogs;

import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.graphics.Rect;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.text.InputFilter;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.Button;
import android.widget.TextView;
import androidx.appcompat.widget.LinearLayoutCompat;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentViewModelLazyKt;
import com.bumptech.glide.a;
import com.github.mikephil.charting.utils.Utils;
import com.google.android.material.checkbox.MaterialCheckBox;
import com.google.android.material.slider.Slider;
import com.google.android.material.switchmaterial.SwitchMaterial;
import java.math.BigDecimal;
import java.util.List;
import net.safemoon.androidwallet.R;
import net.safemoon.androidwallet.dialogs.CryptoPriceAlertFragment;
import net.safemoon.androidwallet.model.common.LoadingState;
import net.safemoon.androidwallet.model.priceAlert.PAToken;
import net.safemoon.androidwallet.model.priceAlert.PriceAlertToken;
import net.safemoon.androidwallet.viewmodels.CryptoPriceAlertViewModel;
import net.safemoon.androidwallet.views.editText.autoSize.AutofitEdittext;

/* compiled from: CryptoPriceAlertFragment.kt */
/* loaded from: classes2.dex */
public final class CryptoPriceAlertFragment extends sn0 {
    public boolean w0;
    public final sy1 u0 = zy1.a(new CryptoPriceAlertFragment$binding$2(this));
    public final sy1 v0 = FragmentViewModelLazyKt.a(this, d53.b(CryptoPriceAlertViewModel.class), new CryptoPriceAlertFragment$special$$inlined$viewModels$default$2(new CryptoPriceAlertFragment$special$$inlined$viewModels$default$1(this)), null);
    public final sy1 x0 = zy1.a(new CryptoPriceAlertFragment$paToken$2(this));
    public final sy1 y0 = zy1.a(new CryptoPriceAlertFragment$loader$2(this));
    public final sy1 z0 = zy1.a(new CryptoPriceAlertFragment$alertDialog$2(this));

    public static final boolean Q(CryptoPriceAlertFragment cryptoPriceAlertFragment, DialogInterface dialogInterface, int i, KeyEvent keyEvent) {
        fs1.f(cryptoPriceAlertFragment, "this$0");
        if (i == 4) {
            cryptoPriceAlertFragment.h();
            return true;
        }
        return false;
    }

    public static final boolean R(CryptoPriceAlertFragment cryptoPriceAlertFragment, View view, MotionEvent motionEvent) {
        fs1.f(cryptoPriceAlertFragment, "this$0");
        if (motionEvent.getAction() == 0) {
            Rect rect = new Rect();
            view.getHitRect(rect);
            if (rect.contains((int) motionEvent.getX(), (int) motionEvent.getY())) {
                return true;
            }
            cryptoPriceAlertFragment.h();
            return false;
        }
        return true;
    }

    /* JADX WARN: Code restructure failed: missing block: B:80:0x01e2, code lost:
        if ((r9 == null ? 0.0d : r9.doubleValue()) <= com.github.mikephil.charting.utils.Utils.DOUBLE_EPSILON) goto L65;
     */
    /* JADX WARN: Code restructure failed: missing block: B:88:0x01f8, code lost:
        if ((r9 == null ? 0.0d : r9.doubleValue()) <= com.github.mikephil.charting.utils.Utils.DOUBLE_EPSILON) goto L73;
     */
    /*
        Code decompiled incorrectly, please refer to instructions dump.
        To view partially-correct code enable 'Show inconsistent code' option in preferences
    */
    public static final void S(defpackage.z91 r8, net.safemoon.androidwallet.dialogs.CryptoPriceAlertFragment r9, net.safemoon.androidwallet.model.priceAlert.PriceAlertToken r10) {
        /*
            Method dump skipped, instructions count: 524
            To view this dump change 'Code comments level' option to 'DEBUG'
        */
        throw new UnsupportedOperationException("Method not decompiled: net.safemoon.androidwallet.dialogs.CryptoPriceAlertFragment.S(z91, net.safemoon.androidwallet.dialogs.CryptoPriceAlertFragment, net.safemoon.androidwallet.model.priceAlert.PriceAlertToken):void");
    }

    /* JADX WARN: Removed duplicated region for block: B:33:0x0077 A[LOOP:0: B:32:0x0075->B:33:0x0077, LOOP_END] */
    /* JADX WARN: Removed duplicated region for block: B:37:0x0090  */
    /*
        Code decompiled incorrectly, please refer to instructions dump.
        To view partially-correct code enable 'Show inconsistent code' option in preferences
    */
    public static final void T(defpackage.z91 r10, final net.safemoon.androidwallet.dialogs.CryptoPriceAlertFragment r11, java.util.List r12) {
        /*
            Method dump skipped, instructions count: 269
            To view this dump change 'Code comments level' option to 'DEBUG'
        */
        throw new UnsupportedOperationException("Method not decompiled: net.safemoon.androidwallet.dialogs.CryptoPriceAlertFragment.T(z91, net.safemoon.androidwallet.dialogs.CryptoPriceAlertFragment, java.util.List):void");
    }

    public static final void U(CryptoPriceAlertFragment cryptoPriceAlertFragment, View view, View view2) {
        fs1.f(cryptoPriceAlertFragment, "this$0");
        gb2<Integer> t = cryptoPriceAlertFragment.N().t();
        Object tag = ((Button) view).getTag();
        t.postValue(tag instanceof Integer ? (Integer) tag : null);
    }

    public static final void V(z91 z91Var, Integer num) {
        fs1.f(z91Var, "$this_with");
        if (num == null) {
            return;
        }
        int intValue = num.intValue();
        LinearLayoutCompat linearLayoutCompat = z91Var.j;
        fs1.e(linearLayoutCompat, "parentOfNavigator");
        int childCount = linearLayoutCompat.getChildCount();
        if (childCount <= 0) {
            return;
        }
        int i = 0;
        while (true) {
            int i2 = i + 1;
            View childAt = linearLayoutCompat.getChildAt(i);
            fs1.e(childAt, "getChildAt(index)");
            if (childAt instanceof TextView) {
                ((TextView) childAt).setSelected(i == intValue);
            }
            if (i2 >= childCount) {
                return;
            }
            i = i2;
        }
    }

    public static final void W(CryptoPriceAlertFragment cryptoPriceAlertFragment, View view) {
        fs1.f(cryptoPriceAlertFragment, "this$0");
        cryptoPriceAlertFragment.h();
    }

    public static final void X(CryptoPriceAlertFragment cryptoPriceAlertFragment, View view) {
        fs1.f(cryptoPriceAlertFragment, "this$0");
        Context requireContext = cryptoPriceAlertFragment.requireContext();
        fs1.e(requireContext, "requireContext()");
        dc0.c(new dc0(requireContext, fb0.b(), new CryptoPriceAlertFragment$onViewCreated$1$6$1(cryptoPriceAlertFragment)), false, false, 3, null).i();
    }

    public static final void Y(CryptoPriceAlertFragment cryptoPriceAlertFragment, View view) {
        Integer value;
        fs1.f(cryptoPriceAlertFragment, "this$0");
        PriceAlertToken value2 = cryptoPriceAlertFragment.N().u().getValue();
        if (value2 == null || (value = cryptoPriceAlertFragment.N().t().getValue()) == null) {
            return;
        }
        if (value2.getId() == null) {
            cryptoPriceAlertFragment.N().k(value2, value.intValue());
            return;
        }
        Context requireContext = cryptoPriceAlertFragment.requireContext();
        fs1.e(requireContext, "requireContext()");
        dc0.c(new dc0(requireContext, fb0.a(), new CryptoPriceAlertFragment$onViewCreated$1$7$1$1$1(cryptoPriceAlertFragment, value2, value)), false, false, 3, null).i();
    }

    public static final void Z(CryptoPriceAlertFragment cryptoPriceAlertFragment, View view) {
        fs1.f(cryptoPriceAlertFragment, "this$0");
        cryptoPriceAlertFragment.w0 = true;
        cryptoPriceAlertFragment.N().h(cryptoPriceAlertFragment.P());
    }

    public static final void a0(CryptoPriceAlertFragment cryptoPriceAlertFragment, LoadingState loadingState) {
        fs1.f(cryptoPriceAlertFragment, "this$0");
        if (loadingState == LoadingState.Loading) {
            ProgressLoading O = cryptoPriceAlertFragment.O();
            FragmentManager parentFragmentManager = cryptoPriceAlertFragment.getParentFragmentManager();
            fs1.e(parentFragmentManager, "parentFragmentManager");
            O.A(parentFragmentManager);
            return;
        }
        cryptoPriceAlertFragment.O().h();
    }

    public static final void b0(CryptoPriceAlertFragment cryptoPriceAlertFragment, Slider slider, float f, boolean z) {
        fs1.f(cryptoPriceAlertFragment, "this$0");
        fs1.f(slider, "$noName_0");
        if (z) {
            cryptoPriceAlertFragment.w0 = true;
            PriceAlertToken value = cryptoPriceAlertFragment.N().u().getValue();
            if (value == null) {
                return;
            }
            value.setIncreasePercentString(e30.h0(new BigDecimal(String.valueOf(f)), 0, 1, null));
            CryptoPriceAlertViewModel.A(cryptoPriceAlertFragment.N(), value, null, 2, null);
        }
    }

    public static final void c0(CryptoPriceAlertFragment cryptoPriceAlertFragment, Slider slider, float f, boolean z) {
        fs1.f(cryptoPriceAlertFragment, "this$0");
        fs1.f(slider, "$noName_0");
        if (z) {
            cryptoPriceAlertFragment.w0 = true;
            PriceAlertToken value = cryptoPriceAlertFragment.N().u().getValue();
            if (value == null) {
                return;
            }
            value.setDecreasePercentString(e30.h0(new BigDecimal(String.valueOf(f)), 0, 1, null));
            CryptoPriceAlertViewModel.A(cryptoPriceAlertFragment.N(), value, null, 2, null);
        }
    }

    public final dc0 L() {
        return (dc0) this.z0.getValue();
    }

    public final z91 M() {
        return (z91) this.u0.getValue();
    }

    public final CryptoPriceAlertViewModel N() {
        return (CryptoPriceAlertViewModel) this.v0.getValue();
    }

    public final ProgressLoading O() {
        return (ProgressLoading) this.y0.getValue();
    }

    public final PAToken P() {
        return (PAToken) this.x0.getValue();
    }

    @Override // defpackage.sn0
    public void h() {
        if (!this.w0) {
            super.h();
        } else if (L().h()) {
        } else {
            L().i();
        }
    }

    @Override // defpackage.sn0
    public Dialog m(Bundle bundle) {
        View decorView;
        Dialog m = super.m(bundle);
        fs1.e(m, "super.onCreateDialog(savedInstanceState)");
        Window window = m.getWindow();
        if (window != null) {
            window.addFlags(262144);
        }
        m.setOnKeyListener(new DialogInterface.OnKeyListener() { // from class: bb0
            @Override // android.content.DialogInterface.OnKeyListener
            public final boolean onKey(DialogInterface dialogInterface, int i, KeyEvent keyEvent) {
                boolean Q;
                Q = CryptoPriceAlertFragment.Q(CryptoPriceAlertFragment.this, dialogInterface, i, keyEvent);
                return Q;
            }
        });
        Window window2 = m.getWindow();
        if (window2 != null && (decorView = window2.getDecorView()) != null) {
            decorView.setOnTouchListener(new View.OnTouchListener() { // from class: va0
                @Override // android.view.View.OnTouchListener
                public final boolean onTouch(View view, MotionEvent motionEvent) {
                    boolean R;
                    R = CryptoPriceAlertFragment.R(CryptoPriceAlertFragment.this, view, motionEvent);
                    return R;
                }
            });
        }
        return m;
    }

    @Override // defpackage.sn0, androidx.fragment.app.Fragment
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
    }

    @Override // androidx.fragment.app.Fragment
    public View onCreateView(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle) {
        fs1.f(layoutInflater, "inflater");
        View inflate = layoutInflater.inflate(R.layout.fragment_crypto_price_alert, viewGroup, false);
        fs1.e(inflate, "inflater.inflate(R.layou…_alert, container, false)");
        return inflate;
    }

    @Override // defpackage.sn0, android.content.DialogInterface.OnDismissListener
    public void onDismiss(DialogInterface dialogInterface) {
        fs1.f(dialogInterface, "dialog");
        super.onDismiss(dialogInterface);
        xd2 n = ka1.a(this).n();
        ec3 d = n == null ? null : n.d();
        if (d != null) {
            d.e("RESULT_FROM_CRYPTO_PRICE_ALERT", Boolean.TRUE);
        }
        ka1.a(this).w();
    }

    @Override // defpackage.sn0, androidx.fragment.app.Fragment
    public void onStart() {
        Window window;
        super.onStart();
        Dialog k = k();
        if (k == null || (window = k.getWindow()) == null) {
            return;
        }
        window.setBackgroundDrawable(new ColorDrawable(0));
        window.setLayout(-1, -2);
    }

    @Override // androidx.fragment.app.Fragment
    public void onViewCreated(View view, Bundle bundle) {
        fs1.f(view, "view");
        super.onViewCreated(view, bundle);
        final z91 M = M();
        PAToken P = P();
        N().l(P());
        a.u(M.h).x(P.getIcon()).I0(M.h);
        M.l.setText(getString(R.string.ca_notify_title_token, P.getName()));
        SwitchMaterial switchMaterial = M.l;
        fs1.e(switchMaterial, "titleSwitch");
        cj4.j(switchMaterial, new CryptoPriceAlertFragment$onViewCreated$1$1$1(this));
        SwitchMaterial switchMaterial2 = M.k;
        fs1.e(switchMaterial2, "switchPersistent");
        cj4.j(switchMaterial2, new CryptoPriceAlertFragment$onViewCreated$1$1$2(this, M));
        AutofitEdittext autofitEdittext = M.c;
        fs1.e(autofitEdittext, "edtMaximum");
        cj4.i(autofitEdittext, new CryptoPriceAlertFragment$onViewCreated$1$1$3(this));
        AutofitEdittext autofitEdittext2 = M.c;
        Double valueOf = Double.valueOf((double) Utils.DOUBLE_EPSILON);
        autofitEdittext2.setFilters(new InputFilter[]{new xq1(valueOf, Double.valueOf(10.0d))});
        xp1 xp1Var = M.i;
        xp1Var.g.setText(getString(R.string.ca_notify_on_increase));
        xp1Var.f.setText("%");
        MaterialCheckBox materialCheckBox = xp1Var.a;
        fs1.e(materialCheckBox, "cbEnableBox");
        cj4.j(materialCheckBox, new CryptoPriceAlertFragment$onViewCreated$1$1$4$1(this));
        AutofitEdittext autofitEdittext3 = xp1Var.d;
        Double valueOf2 = Double.valueOf(100.0d);
        autofitEdittext3.setFilters(new InputFilter[]{new xq1(valueOf, valueOf2)});
        AutofitEdittext autofitEdittext4 = xp1Var.d;
        fs1.e(autofitEdittext4, "newValue");
        autofitEdittext4.addTextChangedListener(new cj2(autofitEdittext4));
        xp1Var.e.h(new sn() { // from class: wa0
            @Override // defpackage.sn
            public final void a(Object obj, float f, boolean z) {
                CryptoPriceAlertFragment.b0(CryptoPriceAlertFragment.this, (Slider) obj, f, z);
            }
        });
        AutofitEdittext autofitEdittext5 = xp1Var.d;
        fs1.e(autofitEdittext5, "newValue");
        cj4.i(autofitEdittext5, new CryptoPriceAlertFragment$onViewCreated$1$1$4$3(this, xp1Var));
        xp1Var.a.setChecked(false);
        xp1 xp1Var2 = M.b;
        xp1Var2.g.setText(getString(R.string.ca_notify_on_decrease));
        xp1Var2.f.setText("%");
        MaterialCheckBox materialCheckBox2 = xp1Var2.a;
        fs1.e(materialCheckBox2, "cbEnableBox");
        cj4.j(materialCheckBox2, new CryptoPriceAlertFragment$onViewCreated$1$1$5$1(this));
        xp1Var2.d.setFilters(new InputFilter[]{new xq1(valueOf, valueOf2)});
        AutofitEdittext autofitEdittext6 = xp1Var2.d;
        fs1.e(autofitEdittext6, "newValue");
        autofitEdittext6.addTextChangedListener(new cj2(autofitEdittext6));
        xp1Var2.e.h(new sn() { // from class: sa0
            @Override // defpackage.sn
            public final void a(Object obj, float f, boolean z) {
                CryptoPriceAlertFragment.c0(CryptoPriceAlertFragment.this, (Slider) obj, f, z);
            }
        });
        AutofitEdittext autofitEdittext7 = xp1Var2.d;
        fs1.e(autofitEdittext7, "newValue");
        cj4.i(autofitEdittext7, new CryptoPriceAlertFragment$onViewCreated$1$1$5$3(this, xp1Var2));
        xp1Var2.a.setChecked(false);
        xp1 xp1Var3 = M.d;
        xp1Var3.g.setText(getString(R.string.ca_notify_when_equal));
        MaterialCheckBox materialCheckBox3 = xp1Var3.b;
        fs1.e(materialCheckBox3, "cbEnablepriceReachesOrHigher");
        cj4.j(materialCheckBox3, new CryptoPriceAlertFragment$onViewCreated$1$1$6$1(this));
        MaterialCheckBox materialCheckBox4 = xp1Var3.c;
        fs1.e(materialCheckBox4, "cbEnablepriceReachesOrLower");
        cj4.j(materialCheckBox4, new CryptoPriceAlertFragment$onViewCreated$1$1$6$2(this));
        Slider slider = xp1Var3.e;
        fs1.e(slider, "slider");
        slider.setVisibility(8);
        AutofitEdittext autofitEdittext8 = xp1Var3.d;
        fs1.e(autofitEdittext8, "newValue");
        autofitEdittext8.addTextChangedListener(new cj2(autofitEdittext8));
        MaterialCheckBox materialCheckBox5 = xp1Var3.b;
        fs1.e(materialCheckBox5, "cbEnablepriceReachesOrHigher");
        materialCheckBox5.setVisibility(0);
        MaterialCheckBox materialCheckBox6 = xp1Var3.c;
        fs1.e(materialCheckBox6, "cbEnablepriceReachesOrLower");
        materialCheckBox6.setVisibility(0);
        MaterialCheckBox materialCheckBox7 = xp1Var3.a;
        fs1.e(materialCheckBox7, "cbEnableBox");
        materialCheckBox7.setVisibility(8);
        AutofitEdittext autofitEdittext9 = xp1Var3.d;
        fs1.e(autofitEdittext9, "newValue");
        cj4.i(autofitEdittext9, new CryptoPriceAlertFragment$onViewCreated$1$1$6$3(this));
        N().u().observe(getViewLifecycleOwner(), new tl2() { // from class: za0
            @Override // defpackage.tl2
            public final void onChanged(Object obj) {
                CryptoPriceAlertFragment.S(z91.this, this, (PriceAlertToken) obj);
            }
        });
        N().r().observe(getViewLifecycleOwner(), new tl2() { // from class: ya0
            @Override // defpackage.tl2
            public final void onChanged(Object obj) {
                CryptoPriceAlertFragment.T(z91.this, this, (List) obj);
            }
        });
        N().t().observe(getViewLifecycleOwner(), new tl2() { // from class: xa0
            @Override // defpackage.tl2
            public final void onChanged(Object obj) {
                CryptoPriceAlertFragment.V(z91.this, (Integer) obj);
            }
        });
        M.e.setOnClickListener(new View.OnClickListener() { // from class: cb0
            @Override // android.view.View.OnClickListener
            public final void onClick(View view2) {
                CryptoPriceAlertFragment.W(CryptoPriceAlertFragment.this, view2);
            }
        });
        M.a.setOnClickListener(new View.OnClickListener() { // from class: eb0
            @Override // android.view.View.OnClickListener
            public final void onClick(View view2) {
                CryptoPriceAlertFragment.X(CryptoPriceAlertFragment.this, view2);
            }
        });
        M.g.setOnClickListener(new View.OnClickListener() { // from class: ta0
            @Override // android.view.View.OnClickListener
            public final void onClick(View view2) {
                CryptoPriceAlertFragment.Y(CryptoPriceAlertFragment.this, view2);
            }
        });
        M.f.setOnClickListener(new View.OnClickListener() { // from class: db0
            @Override // android.view.View.OnClickListener
            public final void onClick(View view2) {
                CryptoPriceAlertFragment.Z(CryptoPriceAlertFragment.this, view2);
            }
        });
        t40.b(N().o()).observe(getViewLifecycleOwner(), new tl2() { // from class: ab0
            @Override // defpackage.tl2
            public final void onChanged(Object obj) {
                CryptoPriceAlertFragment.a0(CryptoPriceAlertFragment.this, (LoadingState) obj);
            }
        });
    }
}
