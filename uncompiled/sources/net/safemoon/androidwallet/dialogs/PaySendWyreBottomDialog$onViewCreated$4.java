package net.safemoon.androidwallet.dialogs;

import android.webkit.WebView;
import kotlin.jvm.internal.Lambda;
import net.safemoon.androidwallet.model.wyre.CheckoutPage;

/* compiled from: PaySendWyreBottomDialog.kt */
/* loaded from: classes2.dex */
public final class PaySendWyreBottomDialog$onViewCreated$4 extends Lambda implements tc1<CheckoutPage, te4> {
    public final /* synthetic */ PaySendWyreBottomDialog this$0;

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public PaySendWyreBottomDialog$onViewCreated$4(PaySendWyreBottomDialog paySendWyreBottomDialog) {
        super(1);
        this.this$0 = paySendWyreBottomDialog;
    }

    @Override // defpackage.tc1
    public /* bridge */ /* synthetic */ te4 invoke(CheckoutPage checkoutPage) {
        invoke2(checkoutPage);
        return te4.a;
    }

    /* renamed from: invoke  reason: avoid collision after fix types in other method */
    public final void invoke2(CheckoutPage checkoutPage) {
        tc1 tc1Var;
        tc1 tc1Var2;
        if (!this.this$0.isVisible() || checkoutPage == null || checkoutPage.getUrl() == null) {
            return;
        }
        tc1Var = this.this$0.B0;
        if (tc1Var != null) {
            tc1Var2 = this.this$0.B0;
            if (tc1Var2 != null) {
                tc1Var2.invoke(checkoutPage.getUrl());
            }
            this.this$0.h();
            return;
        }
        un0 un0Var = this.this$0.v0;
        fs1.d(un0Var);
        WebView webView = un0Var.c;
        String url = checkoutPage.getUrl();
        fs1.d(url);
        webView.loadUrl(url);
    }
}
