package net.safemoon.androidwallet.dialogs.common;

import android.app.Activity;
import android.app.Dialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.util.DisplayMetrics;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewParent;
import android.widget.FrameLayout;
import com.google.android.material.bottomsheet.BottomSheetBehavior;
import com.google.android.material.bottomsheet.a;
import com.google.android.material.bottomsheet.b;
import java.util.Objects;
import net.safemoon.androidwallet.R;
import net.safemoon.androidwallet.dialogs.common.BaseBottomSheetDialogFragment;

/* compiled from: BaseBottomSheetDialogFragment.kt */
/* loaded from: classes2.dex */
public class BaseBottomSheetDialogFragment extends b {
    public final sy1 v0 = zy1.a(new BaseBottomSheetDialogFragment$iHomeInterface$2(this));

    public static final void C(BaseBottomSheetDialogFragment baseBottomSheetDialogFragment, DialogInterface dialogInterface) {
        fs1.f(baseBottomSheetDialogFragment, "this$0");
        Objects.requireNonNull(dialogInterface, "null cannot be cast to non-null type com.google.android.material.bottomsheet.BottomSheetDialog");
        a aVar = (a) dialogInterface;
        baseBottomSheetDialogFragment.E(aVar);
        View findViewById = aVar.findViewById(R.id.design_bottom_sheet);
        if (findViewById == null) {
            return;
        }
        BottomSheetBehavior y = BottomSheetBehavior.y(findViewById);
        fs1.e(y, "from(it)");
        baseBottomSheetDialogFragment.D(findViewById);
        y.V(3);
    }

    public final gm1 A() {
        return (gm1) this.v0.getValue();
    }

    public final int B() {
        DisplayMetrics displayMetrics = new DisplayMetrics();
        Activity activity = (Activity) getContext();
        fs1.d(activity);
        activity.getWindowManager().getDefaultDisplay().getMetrics(displayMetrics);
        return displayMetrics.heightPixels;
    }

    public final void D(View view) {
        ViewGroup.LayoutParams layoutParams = view.getLayoutParams();
        layoutParams.height = -1;
        view.setLayoutParams(layoutParams);
    }

    public final void E(a aVar) {
        View findViewById = aVar.findViewById(R.id.design_bottom_sheet);
        Objects.requireNonNull(findViewById, "null cannot be cast to non-null type android.widget.FrameLayout");
        FrameLayout frameLayout = (FrameLayout) findViewById;
        fs1.e(BottomSheetBehavior.y(frameLayout), "from(bottomSheet)");
        ViewGroup.LayoutParams layoutParams = frameLayout.getLayoutParams();
        int B = B();
        if (layoutParams != null) {
            layoutParams.height = B;
        }
        frameLayout.setLayoutParams(layoutParams);
    }

    @Override // com.google.android.material.bottomsheet.b, defpackage.ff, defpackage.sn0
    public Dialog m(Bundle bundle) {
        Dialog m = super.m(bundle);
        fs1.e(m, "super.onCreateDialog(savedInstanceState)");
        m.setOnShowListener(new DialogInterface.OnShowListener() { // from class: pm
            @Override // android.content.DialogInterface.OnShowListener
            public final void onShow(DialogInterface dialogInterface) {
                BaseBottomSheetDialogFragment.C(BaseBottomSheetDialogFragment.this, dialogInterface);
            }
        });
        return m;
    }

    @Override // defpackage.sn0, androidx.fragment.app.Fragment
    public void onStart() {
        super.onStart();
        ViewParent parent = requireView().getParent();
        Objects.requireNonNull(parent, "null cannot be cast to non-null type android.view.View");
        BottomSheetBehavior y = BottomSheetBehavior.y((View) parent);
        fs1.e(y, "from(requireView().parent as View)");
        y.V(3);
    }
}
