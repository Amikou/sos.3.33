package net.safemoon.androidwallet.dialogs;

import java.io.Serializable;
import java.util.Objects;
import kotlin.jvm.internal.Lambda;
import net.safemoon.androidwallet.model.common.PaymentMethod;

/* compiled from: PaySendWyreBottomDialog.kt */
/* loaded from: classes2.dex */
public final class PaySendWyreBottomDialog$paymentMethod$2 extends Lambda implements rc1<PaymentMethod> {
    public final /* synthetic */ PaySendWyreBottomDialog this$0;

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public PaySendWyreBottomDialog$paymentMethod$2(PaySendWyreBottomDialog paySendWyreBottomDialog) {
        super(0);
        this.this$0 = paySendWyreBottomDialog;
    }

    @Override // defpackage.rc1
    public final PaymentMethod invoke() {
        Serializable serializable = this.this$0.requireArguments().getSerializable("ARG_PAYMENT_METHOD");
        Objects.requireNonNull(serializable, "null cannot be cast to non-null type net.safemoon.androidwallet.model.common.PaymentMethod");
        return (PaymentMethod) serializable;
    }
}
