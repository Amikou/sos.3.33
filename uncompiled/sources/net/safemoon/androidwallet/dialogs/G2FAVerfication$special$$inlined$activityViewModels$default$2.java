package net.safemoon.androidwallet.dialogs;

import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentActivity;
import androidx.lifecycle.l;
import kotlin.jvm.internal.Lambda;

/* compiled from: FragmentViewModelLazy.kt */
/* loaded from: classes2.dex */
public final class G2FAVerfication$special$$inlined$activityViewModels$default$2 extends Lambda implements rc1<l.b> {
    public final /* synthetic */ Fragment $this_activityViewModels;

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public G2FAVerfication$special$$inlined$activityViewModels$default$2(Fragment fragment) {
        super(0);
        this.$this_activityViewModels = fragment;
    }

    /* JADX WARN: Can't rename method to resolve collision */
    @Override // defpackage.rc1
    public final l.b invoke() {
        FragmentActivity requireActivity = this.$this_activityViewModels.requireActivity();
        fs1.c(requireActivity, "requireActivity()");
        l.b defaultViewModelProviderFactory = requireActivity.getDefaultViewModelProviderFactory();
        fs1.c(defaultViewModelProviderFactory, "requireActivity().defaultViewModelProviderFactory");
        return defaultViewModelProviderFactory;
    }
}
