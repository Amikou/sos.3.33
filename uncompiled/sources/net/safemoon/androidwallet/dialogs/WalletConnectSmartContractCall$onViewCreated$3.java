package net.safemoon.androidwallet.dialogs;

import kotlin.coroutines.jvm.internal.SuspendLambda;
import kotlin.coroutines.jvm.internal.a;
import kotlinx.coroutines.CoroutineDispatcher;

/* compiled from: WalletConnectSmartContractCall.kt */
@a(c = "net.safemoon.androidwallet.dialogs.WalletConnectSmartContractCall$onViewCreated$3", f = "WalletConnectSmartContractCall.kt", l = {82}, m = "invokeSuspend")
/* loaded from: classes2.dex */
public final class WalletConnectSmartContractCall$onViewCreated$3 extends SuspendLambda implements hd1<c90, q70<? super te4>, Object> {
    public int label;
    public final /* synthetic */ WalletConnectSmartContractCall this$0;

    /* compiled from: WalletConnectSmartContractCall.kt */
    @a(c = "net.safemoon.androidwallet.dialogs.WalletConnectSmartContractCall$onViewCreated$3$1", f = "WalletConnectSmartContractCall.kt", l = {83}, m = "invokeSuspend")
    /* renamed from: net.safemoon.androidwallet.dialogs.WalletConnectSmartContractCall$onViewCreated$3$1  reason: invalid class name */
    /* loaded from: classes2.dex */
    public static final class AnonymousClass1 extends SuspendLambda implements hd1<c90, q70<? super te4>, Object> {
        public int label;
        public final /* synthetic */ WalletConnectSmartContractCall this$0;

        /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
        public AnonymousClass1(WalletConnectSmartContractCall walletConnectSmartContractCall, q70<? super AnonymousClass1> q70Var) {
            super(2, q70Var);
            this.this$0 = walletConnectSmartContractCall;
        }

        @Override // kotlin.coroutines.jvm.internal.BaseContinuationImpl
        public final q70<te4> create(Object obj, q70<?> q70Var) {
            return new AnonymousClass1(this.this$0, q70Var);
        }

        @Override // defpackage.hd1
        public final Object invoke(c90 c90Var, q70<? super te4> q70Var) {
            return ((AnonymousClass1) create(c90Var, q70Var)).invokeSuspend(te4.a);
        }

        @Override // kotlin.coroutines.jvm.internal.BaseContinuationImpl
        public final Object invokeSuspend(Object obj) {
            Object O;
            Object d = gs1.d();
            int i = this.label;
            if (i == 0) {
                o83.b(obj);
                WalletConnectSmartContractCall walletConnectSmartContractCall = this.this$0;
                this.label = 1;
                O = walletConnectSmartContractCall.O(this);
                if (O == d) {
                    return d;
                }
            } else if (i != 1) {
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            } else {
                o83.b(obj);
            }
            return te4.a;
        }
    }

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public WalletConnectSmartContractCall$onViewCreated$3(WalletConnectSmartContractCall walletConnectSmartContractCall, q70<? super WalletConnectSmartContractCall$onViewCreated$3> q70Var) {
        super(2, q70Var);
        this.this$0 = walletConnectSmartContractCall;
    }

    @Override // kotlin.coroutines.jvm.internal.BaseContinuationImpl
    public final q70<te4> create(Object obj, q70<?> q70Var) {
        return new WalletConnectSmartContractCall$onViewCreated$3(this.this$0, q70Var);
    }

    @Override // defpackage.hd1
    public final Object invoke(c90 c90Var, q70<? super te4> q70Var) {
        return ((WalletConnectSmartContractCall$onViewCreated$3) create(c90Var, q70Var)).invokeSuspend(te4.a);
    }

    @Override // kotlin.coroutines.jvm.internal.BaseContinuationImpl
    public final Object invokeSuspend(Object obj) {
        Object d = gs1.d();
        int i = this.label;
        if (i == 0) {
            o83.b(obj);
            CoroutineDispatcher b = tp0.b();
            AnonymousClass1 anonymousClass1 = new AnonymousClass1(this.this$0, null);
            this.label = 1;
            if (kotlinx.coroutines.a.e(b, anonymousClass1, this) == d) {
                return d;
            }
        } else if (i != 1) {
            throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        } else {
            o83.b(obj);
        }
        return te4.a;
    }
}
