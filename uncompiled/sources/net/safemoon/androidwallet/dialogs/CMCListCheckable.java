package net.safemoon.androidwallet.dialogs;

import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentViewModelLazyKt;
import androidx.recyclerview.widget.LinearLayoutManager;
import com.google.android.material.textfield.TextInputEditText;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import kotlin.Pair;
import net.safemoon.androidwallet.R;
import net.safemoon.androidwallet.dialogs.CMCListCheckable;
import net.safemoon.androidwallet.dialogs.common.BaseBottomSheetDialogFragment;
import net.safemoon.androidwallet.viewmodels.CMCListViewModel;

/* compiled from: CMCListCheckable.kt */
/* loaded from: classes2.dex */
public final class CMCListCheckable extends BaseBottomSheetDialogFragment {
    public static final a C0 = new a(null);
    public jp3 B0;
    public g00 x0;
    public rl1 y0;
    public final sy1 w0 = FragmentViewModelLazyKt.a(this, d53.b(CMCListViewModel.class), new CMCListCheckable$special$$inlined$viewModels$default$2(new CMCListCheckable$special$$inlined$viewModels$default$1(this)), null);
    public final sy1 z0 = zy1.a(new CMCListCheckable$screenTitle$2(this));
    public final sy1 A0 = zy1.a(new CMCListCheckable$excludeAppTokens$2(this));

    /* compiled from: CMCListCheckable.kt */
    /* loaded from: classes2.dex */
    public static final class a {
        public a() {
        }

        public /* synthetic */ a(qi0 qi0Var) {
            this();
        }

        public final CMCListCheckable a(String str, boolean z) {
            fs1.f(str, "screenTitle");
            CMCListCheckable cMCListCheckable = new CMCListCheckable();
            cMCListCheckable.setArguments(gs.a(new Pair("ARG_SCREEN_TITLE", str), new Pair("ARG_EXCLUDE_APP_TOKENS", Boolean.valueOf(z))));
            return cMCListCheckable;
        }
    }

    /* compiled from: TextView.kt */
    /* loaded from: classes2.dex */
    public static final class b implements TextWatcher {
        public b() {
        }

        @Override // android.text.TextWatcher
        public void afterTextChanged(Editable editable) {
            CMCListCheckable.this.I().p().postValue(editable == null ? null : editable.toString());
        }

        @Override // android.text.TextWatcher
        public void beforeTextChanged(CharSequence charSequence, int i, int i2, int i3) {
        }

        @Override // android.text.TextWatcher
        public void onTextChanged(CharSequence charSequence, int i, int i2, int i3) {
        }
    }

    public static final void L(CMCListCheckable cMCListCheckable, View view) {
        fs1.f(cMCListCheckable, "this$0");
        cMCListCheckable.h();
    }

    public static final void M(CMCListCheckable cMCListCheckable, List list) {
        fs1.f(cMCListCheckable, "this$0");
        g00 g00Var = cMCListCheckable.x0;
        if (g00Var == null) {
            return;
        }
        ArrayList arrayList = null;
        if (list != null) {
            ArrayList arrayList2 = new ArrayList(c20.q(list, 10));
            Iterator it = list.iterator();
            while (it.hasNext()) {
                arrayList2.add(d11.b((d11) it.next(), null, false, 3, null));
            }
            if (!arrayList2.isEmpty()) {
                arrayList = arrayList2;
            }
        }
        g00Var.submitList(arrayList);
    }

    public final CMCListViewModel I() {
        return (CMCListViewModel) this.w0.getValue();
    }

    public final Boolean J() {
        return (Boolean) this.A0.getValue();
    }

    public final String K() {
        return (String) this.z0.getValue();
    }

    public final void N(rl1 rl1Var) {
        fs1.f(rl1Var, "iCallBack");
        this.y0 = rl1Var;
    }

    public final void O(int... iArr) {
        fs1.f(iArr, "list");
        I().q().postValue(ai.J(iArr));
    }

    public final void P(FragmentManager fragmentManager) {
        fs1.f(fragmentManager, "manager");
        super.u(fragmentManager, CMCListCheckable.class.getCanonicalName());
    }

    @Override // defpackage.sn0, androidx.fragment.app.Fragment
    public void onAttach(Context context) {
        fs1.f(context, "context");
        super.onAttach(context);
    }

    @Override // androidx.fragment.app.Fragment
    public View onCreateView(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle) {
        Window window;
        fs1.f(layoutInflater, "inflater");
        View inflate = layoutInflater.inflate(R.layout.simple_list_view, viewGroup, false);
        Dialog k = k();
        if (k != null && (window = k.getWindow()) != null) {
            window.setSoftInputMode(16);
        }
        return inflate;
    }

    @Override // androidx.fragment.app.Fragment
    public void onViewCreated(View view, Bundle bundle) {
        fs1.f(view, "view");
        super.onViewCreated(view, bundle);
        this.B0 = jp3.a(view);
        this.x0 = new g00(this.y0);
        rl1 rl1Var = this.y0;
        if (rl1Var != null) {
            rl1Var.c();
        }
        jp3 jp3Var = this.B0;
        fs1.d(jp3Var);
        jp3Var.a.a.setOnClickListener(new View.OnClickListener() { // from class: mt
            @Override // android.view.View.OnClickListener
            public final void onClick(View view2) {
                CMCListCheckable.L(CMCListCheckable.this, view2);
            }
        });
        jp3Var.a.c.setText(K());
        TextInputEditText textInputEditText = jp3Var.c.b;
        fs1.e(textInputEditText, "searchBar.etSearch");
        textInputEditText.addTextChangedListener(new b());
        jp3Var.b.setLayoutManager(new LinearLayoutManager(requireContext(), 1, false));
        jp3Var.b.setAdapter(this.x0);
        I().n().postValue(Boolean.valueOf(fs1.b(J(), Boolean.TRUE)));
        I().k();
        I().m().observe(this, new tl2() { // from class: lt
            @Override // defpackage.tl2
            public final void onChanged(Object obj) {
                CMCListCheckable.M(CMCListCheckable.this, (List) obj);
            }
        });
    }
}
