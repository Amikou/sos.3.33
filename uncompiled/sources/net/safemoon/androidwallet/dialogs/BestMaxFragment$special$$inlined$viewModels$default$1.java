package net.safemoon.androidwallet.dialogs;

import kotlin.jvm.internal.Lambda;

/* compiled from: FragmentViewModelLazy.kt */
/* loaded from: classes2.dex */
public final class BestMaxFragment$special$$inlined$viewModels$default$1 extends Lambda implements rc1<gj4> {
    public final /* synthetic */ rc1 $ownerProducer;

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public BestMaxFragment$special$$inlined$viewModels$default$1(rc1 rc1Var) {
        super(0);
        this.$ownerProducer = rc1Var;
    }

    /* JADX WARN: Can't rename method to resolve collision */
    @Override // defpackage.rc1
    public final gj4 invoke() {
        gj4 viewModelStore = ((hj4) this.$ownerProducer.invoke()).getViewModelStore();
        fs1.c(viewModelStore, "ownerProducer().viewModelStore");
        return viewModelStore;
    }
}
