package net.safemoon.androidwallet.dialogs;

import java.io.Serializable;
import java.util.Objects;
import kotlin.jvm.internal.Lambda;
import net.safemoon.androidwallet.model.priceAlert.PAToken;

/* compiled from: CryptoPriceAlertFragment.kt */
/* loaded from: classes2.dex */
public final class CryptoPriceAlertFragment$paToken$2 extends Lambda implements rc1<PAToken> {
    public final /* synthetic */ CryptoPriceAlertFragment this$0;

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public CryptoPriceAlertFragment$paToken$2(CryptoPriceAlertFragment cryptoPriceAlertFragment) {
        super(0);
        this.this$0 = cryptoPriceAlertFragment;
    }

    @Override // defpackage.rc1
    public final PAToken invoke() {
        Serializable serializable = this.this$0.requireArguments().getSerializable("token");
        Objects.requireNonNull(serializable, "null cannot be cast to non-null type net.safemoon.androidwallet.model.priceAlert.PAToken");
        return (PAToken) serializable;
    }
}
