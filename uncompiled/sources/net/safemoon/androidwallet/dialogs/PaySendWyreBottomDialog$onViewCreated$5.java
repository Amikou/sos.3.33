package net.safemoon.androidwallet.dialogs;

import android.content.Context;
import kotlin.jvm.internal.Lambda;

/* compiled from: PaySendWyreBottomDialog.kt */
/* loaded from: classes2.dex */
public final class PaySendWyreBottomDialog$onViewCreated$5 extends Lambda implements tc1<String, te4> {
    public final /* synthetic */ PaySendWyreBottomDialog this$0;

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public PaySendWyreBottomDialog$onViewCreated$5(PaySendWyreBottomDialog paySendWyreBottomDialog) {
        super(1);
        this.this$0 = paySendWyreBottomDialog;
    }

    @Override // defpackage.tc1
    public /* bridge */ /* synthetic */ te4 invoke(String str) {
        invoke2(str);
        return te4.a;
    }

    /* renamed from: invoke  reason: avoid collision after fix types in other method */
    public final void invoke2(String str) {
        if (!this.this$0.isVisible() || str == null) {
            return;
        }
        Context requireContext = this.this$0.requireContext();
        fs1.e(requireContext, "requireContext()");
        e30.a0(requireContext, str);
    }
}
