package net.safemoon.androidwallet.dialogs;

import kotlin.jvm.internal.Lambda;

/* compiled from: ConfirmSwapTransactionFragment.kt */
/* loaded from: classes2.dex */
public final class ConfirmSwapTransactionFragment$ccWrapperWidth$2 extends Lambda implements rc1<Integer> {
    public final /* synthetic */ ConfirmSwapTransactionFragment this$0;

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public ConfirmSwapTransactionFragment$ccWrapperWidth$2(ConfirmSwapTransactionFragment confirmSwapTransactionFragment) {
        super(0);
        this.this$0 = confirmSwapTransactionFragment;
    }

    /* JADX WARN: Can't rename method to resolve collision */
    @Override // defpackage.rc1
    public final Integer invoke() {
        w91 H;
        H = this.this$0.H();
        return Integer.valueOf(H.a.getWidth());
    }
}
