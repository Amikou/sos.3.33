package net.safemoon.androidwallet.dialogs;

import kotlin.jvm.internal.Lambda;
import net.safemoon.androidwallet.dialogs.AnchorCurrencyConverter$defaultCurrencyAdapter$2;
import net.safemoon.androidwallet.model.fiat.room.RoomFiat;

/* compiled from: AnchorCurrencyConverter.kt */
/* loaded from: classes2.dex */
public final class AnchorCurrencyConverter$2$7 extends Lambda implements rc1<te4> {
    public final /* synthetic */ dn0 $this_with;
    public final /* synthetic */ AnchorCurrencyConverter this$0;

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public AnchorCurrencyConverter$2$7(AnchorCurrencyConverter anchorCurrencyConverter, dn0 dn0Var) {
        super(0);
        this.this$0 = anchorCurrencyConverter;
        this.$this_with = dn0Var;
    }

    @Override // defpackage.rc1
    public /* bridge */ /* synthetic */ te4 invoke() {
        invoke2();
        return te4.a;
    }

    @Override // defpackage.rc1
    /* renamed from: invoke  reason: avoid collision after fix types in other method */
    public final void invoke2() {
        AnchorCurrencyConverter$defaultCurrencyAdapter$2.a v;
        this.this$0.i = false;
        this.$this_with.a.showNext();
        AnchorCurrencyConverter anchorCurrencyConverter = this.this$0;
        AnchorCurrencyConverter.t(anchorCurrencyConverter, new RoomFiat(anchorCurrencyConverter.e), null, 2, null);
        v = this.this$0.v();
        v.notifyDataSetChanged();
    }
}
