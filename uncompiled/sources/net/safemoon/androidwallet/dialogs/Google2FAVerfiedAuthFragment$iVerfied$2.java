package net.safemoon.androidwallet.dialogs;

import androidx.fragment.app.Fragment;
import java.util.Objects;
import kotlin.jvm.internal.Lambda;

/* compiled from: Google2FAVerfiedAuthFragment.kt */
/* loaded from: classes2.dex */
public final class Google2FAVerfiedAuthFragment$iVerfied$2 extends Lambda implements rc1<cn1> {
    public final /* synthetic */ Google2FAVerfiedAuthFragment this$0;

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public Google2FAVerfiedAuthFragment$iVerfied$2(Google2FAVerfiedAuthFragment google2FAVerfiedAuthFragment) {
        super(0);
        this.this$0 = google2FAVerfiedAuthFragment;
    }

    /* JADX WARN: Can't rename method to resolve collision */
    @Override // defpackage.rc1
    public final cn1 invoke() {
        if (this.this$0.getParentFragment() instanceof cn1) {
            Fragment parentFragment = this.this$0.getParentFragment();
            Objects.requireNonNull(parentFragment, "null cannot be cast to non-null type net.safemoon.androidwallet.interfaces.IVerfied");
            return (cn1) parentFragment;
        } else if (this.this$0.requireContext() instanceof cn1) {
            return (cn1) this.this$0.requireContext();
        } else {
            return null;
        }
    }
}
