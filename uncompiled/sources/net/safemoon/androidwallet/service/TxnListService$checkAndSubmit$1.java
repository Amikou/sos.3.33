package net.safemoon.androidwallet.service;

import kotlin.coroutines.jvm.internal.SuspendLambda;
import kotlin.coroutines.jvm.internal.a;
import kotlinx.coroutines.CoroutineDispatcher;

/* compiled from: TxnListService.kt */
@a(c = "net.safemoon.androidwallet.service.TxnListService$checkAndSubmit$1", f = "TxnListService.kt", l = {82}, m = "invokeSuspend")
/* loaded from: classes2.dex */
public final class TxnListService$checkAndSubmit$1 extends SuspendLambda implements hd1<c90, q70<? super te4>, Object> {
    public int label;
    public final /* synthetic */ TxnListService this$0;

    /* compiled from: TxnListService.kt */
    @a(c = "net.safemoon.androidwallet.service.TxnListService$checkAndSubmit$1$1", f = "TxnListService.kt", l = {90, 106, 119}, m = "invokeSuspend")
    /* renamed from: net.safemoon.androidwallet.service.TxnListService$checkAndSubmit$1$1  reason: invalid class name */
    /* loaded from: classes2.dex */
    public static final class AnonymousClass1 extends SuspendLambda implements hd1<c90, q70<? super te4>, Object> {
        public Object L$0;
        public Object L$1;
        public Object L$2;
        public Object L$3;
        public int label;
        public final /* synthetic */ TxnListService this$0;

        /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
        public AnonymousClass1(TxnListService txnListService, q70<? super AnonymousClass1> q70Var) {
            super(2, q70Var);
            this.this$0 = txnListService;
        }

        @Override // kotlin.coroutines.jvm.internal.BaseContinuationImpl
        public final q70<te4> create(Object obj, q70<?> q70Var) {
            return new AnonymousClass1(this.this$0, q70Var);
        }

        @Override // defpackage.hd1
        public final Object invoke(c90 c90Var, q70<? super te4> q70Var) {
            return ((AnonymousClass1) create(c90Var, q70Var)).invokeSuspend(te4.a);
        }

        /* JADX WARN: Not initialized variable reg: 10, insn: 0x006d: MOVE  (r9 I:??[OBJECT, ARRAY]) = (r10 I:??[OBJECT, ARRAY]), block:B:21:0x006c */
        /* JADX WARN: Not initialized variable reg: 9, insn: 0x006c: MOVE  (r8 I:??[OBJECT, ARRAY]) = (r9 I:??[OBJECT, ARRAY]), block:B:21:0x006c */
        /* JADX WARN: Removed duplicated region for block: B:26:0x0091  */
        /* JADX WARN: Removed duplicated region for block: B:46:0x0108  */
        /* JADX WARN: Removed duplicated region for block: B:47:0x010a  */
        /* JADX WARN: Removed duplicated region for block: B:59:0x019c A[Catch: Exception -> 0x01ef, TryCatch #0 {Exception -> 0x01ef, blocks: (B:64:0x01cd, B:66:0x01d5, B:53:0x0171, B:55:0x0175, B:57:0x017b, B:59:0x019c, B:61:0x01a5, B:60:0x01a1), top: B:80:0x01cd }] */
        /* JADX WARN: Removed duplicated region for block: B:60:0x01a1 A[Catch: Exception -> 0x01ef, TryCatch #0 {Exception -> 0x01ef, blocks: (B:64:0x01cd, B:66:0x01d5, B:53:0x0171, B:55:0x0175, B:57:0x017b, B:59:0x019c, B:61:0x01a5, B:60:0x01a1), top: B:80:0x01cd }] */
        /* JADX WARN: Removed duplicated region for block: B:63:0x01cc A[RETURN] */
        /* JADX WARN: Removed duplicated region for block: B:66:0x01d5 A[Catch: Exception -> 0x01ef, TRY_LEAVE, TryCatch #0 {Exception -> 0x01ef, blocks: (B:64:0x01cd, B:66:0x01d5, B:53:0x0171, B:55:0x0175, B:57:0x017b, B:59:0x019c, B:61:0x01a5, B:60:0x01a1), top: B:80:0x01cd }] */
        /* JADX WARN: Removed duplicated region for block: B:77:0x01ff  */
        /* JADX WARN: Removed duplicated region for block: B:78:0x0206  */
        /* JADX WARN: Removed duplicated region for block: B:87:0x010d A[EXC_TOP_SPLITTER, SYNTHETIC] */
        /* JADX WARN: Unsupported multi-entry loop pattern (BACK_EDGE: B:68:0x01eb -> B:24:0x008b). Please submit an issue!!! */
        /* JADX WARN: Unsupported multi-entry loop pattern (BACK_EDGE: B:75:0x01fb -> B:24:0x008b). Please submit an issue!!! */
        /* JADX WARN: Unsupported multi-entry loop pattern (BACK_EDGE: B:77:0x01ff -> B:24:0x008b). Please submit an issue!!! */
        @Override // kotlin.coroutines.jvm.internal.BaseContinuationImpl
        /*
            Code decompiled incorrectly, please refer to instructions dump.
            To view partially-correct code enable 'Show inconsistent code' option in preferences
        */
        public final java.lang.Object invokeSuspend(java.lang.Object r37) {
            /*
                Method dump skipped, instructions count: 521
                To view this dump change 'Code comments level' option to 'DEBUG'
            */
            throw new UnsupportedOperationException("Method not decompiled: net.safemoon.androidwallet.service.TxnListService$checkAndSubmit$1.AnonymousClass1.invokeSuspend(java.lang.Object):java.lang.Object");
        }
    }

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public TxnListService$checkAndSubmit$1(TxnListService txnListService, q70<? super TxnListService$checkAndSubmit$1> q70Var) {
        super(2, q70Var);
        this.this$0 = txnListService;
    }

    @Override // kotlin.coroutines.jvm.internal.BaseContinuationImpl
    public final q70<te4> create(Object obj, q70<?> q70Var) {
        return new TxnListService$checkAndSubmit$1(this.this$0, q70Var);
    }

    @Override // defpackage.hd1
    public final Object invoke(c90 c90Var, q70<? super te4> q70Var) {
        return ((TxnListService$checkAndSubmit$1) create(c90Var, q70Var)).invokeSuspend(te4.a);
    }

    @Override // kotlin.coroutines.jvm.internal.BaseContinuationImpl
    public final Object invokeSuspend(Object obj) {
        Object d = gs1.d();
        int i = this.label;
        if (i == 0) {
            o83.b(obj);
            CoroutineDispatcher b = tp0.b();
            AnonymousClass1 anonymousClass1 = new AnonymousClass1(this.this$0, null);
            this.label = 1;
            if (kotlinx.coroutines.a.e(b, anonymousClass1, this) == d) {
                return d;
            }
        } else if (i != 1) {
            throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        } else {
            o83.b(obj);
        }
        return te4.a;
    }
}
