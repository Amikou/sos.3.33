package net.safemoon.androidwallet.service;

import android.app.NotificationManager;
import android.content.Context;
import defpackage.dh2;
import java.util.ArrayList;
import kotlin.coroutines.jvm.internal.SuspendLambda;
import kotlin.coroutines.jvm.internal.a;
import net.safemoon.androidwallet.model.notificationHistory.NotificationHistory;
import net.safemoon.androidwallet.model.notificationHistory.NotificationHistoryData;
import net.safemoon.androidwallet.model.notificationHistory.NotificationHistoryResult;

/* compiled from: MyFirebaseMessagingService.kt */
@a(c = "net.safemoon.androidwallet.service.MyFirebaseMessagingService$sendNotification$1", f = "MyFirebaseMessagingService.kt", l = {105}, m = "invokeSuspend")
/* loaded from: classes2.dex */
public final class MyFirebaseMessagingService$sendNotification$1 extends SuspendLambda implements hd1<c90, q70<? super te4>, Object> {
    public final /* synthetic */ dh2.e $notificationBuilder;
    public final /* synthetic */ NotificationManager $notificationManager;
    public int label;
    public final /* synthetic */ MyFirebaseMessagingService this$0;

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public MyFirebaseMessagingService$sendNotification$1(MyFirebaseMessagingService myFirebaseMessagingService, dh2.e eVar, NotificationManager notificationManager, q70<? super MyFirebaseMessagingService$sendNotification$1> q70Var) {
        super(2, q70Var);
        this.this$0 = myFirebaseMessagingService;
        this.$notificationBuilder = eVar;
        this.$notificationManager = notificationManager;
    }

    @Override // kotlin.coroutines.jvm.internal.BaseContinuationImpl
    public final q70<te4> create(Object obj, q70<?> q70Var) {
        return new MyFirebaseMessagingService$sendNotification$1(this.this$0, this.$notificationBuilder, this.$notificationManager, q70Var);
    }

    @Override // defpackage.hd1
    public final Object invoke(c90 c90Var, q70<? super te4> q70Var) {
        return ((MyFirebaseMessagingService$sendNotification$1) create(c90Var, q70Var)).invokeSuspend(te4.a);
    }

    @Override // kotlin.coroutines.jvm.internal.BaseContinuationImpl
    public final Object invokeSuspend(Object obj) {
        NotificationHistoryData data;
        ArrayList<NotificationHistoryResult> result;
        Object d = gs1.d();
        int i = this.label;
        int i2 = 1;
        try {
            if (i == 0) {
                o83.b(obj);
                ac3 m = a4.m();
                fs1.e(m, "getSafeMoonClient()");
                lf1 lf1Var = new lf1(m);
                String i3 = bo3.i(this.this$0.getApplicationContext(), "SAFEMOON_ADDRESS");
                fs1.e(i3, "getString(\n             …                        )");
                b30 b30Var = b30.a;
                Context applicationContext = this.this$0.getApplicationContext();
                fs1.e(applicationContext, "this@MyFirebaseMessagingService.applicationContext");
                String g = b30.g(b30Var, applicationContext, null, 2, null);
                this.label = 1;
                obj = lf1Var.a(i3, g, this);
                if (obj == d) {
                    return d;
                }
            } else if (i != 1) {
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            } else {
                o83.b(obj);
            }
            NotificationHistory notificationHistory = (NotificationHistory) obj;
            if (notificationHistory != null && (data = notificationHistory.getData()) != null && (result = data.getResult()) != null) {
                ArrayList arrayList = new ArrayList();
                for (Object obj2 : result) {
                    if (!((NotificationHistoryResult) obj2).read) {
                        arrayList.add(obj2);
                    }
                }
                i2 = arrayList.size();
            }
        } catch (RuntimeException e) {
            String message = e.getMessage();
            if (message != null) {
                String simpleName = this.this$0.getClass().getSimpleName();
                fs1.e(simpleName, "this@MyFirebaseMessaging…ce::class.java.simpleName");
                e30.c0(message, simpleName);
            }
        }
        this.$notificationBuilder.w(i2);
        this.$notificationManager.notify(0, this.$notificationBuilder.b());
        return te4.a;
    }
}
