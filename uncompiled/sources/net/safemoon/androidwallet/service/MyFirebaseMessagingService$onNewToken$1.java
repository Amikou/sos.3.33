package net.safemoon.androidwallet.service;

import defpackage.pm1;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Objects;
import kotlin.coroutines.jvm.internal.SuspendLambda;
import kotlin.coroutines.jvm.internal.a;
import net.safemoon.androidwallet.database.mainRoom.MainRoomDatabase;
import net.safemoon.androidwallet.model.wallets.Wallet;
import net.safemoon.androidwallet.repository.WalletDataSource;

/* compiled from: MyFirebaseMessagingService.kt */
@a(c = "net.safemoon.androidwallet.service.MyFirebaseMessagingService$onNewToken$1", f = "MyFirebaseMessagingService.kt", l = {134, 137}, m = "invokeSuspend")
/* loaded from: classes2.dex */
public final class MyFirebaseMessagingService$onNewToken$1 extends SuspendLambda implements hd1<c90, q70<? super te4>, Object> {
    public final /* synthetic */ String $newToken;
    public Object L$0;
    public int label;
    public final /* synthetic */ MyFirebaseMessagingService this$0;

    /* compiled from: MyFirebaseMessagingService.kt */
    @a(c = "net.safemoon.androidwallet.service.MyFirebaseMessagingService$onNewToken$1$2", f = "MyFirebaseMessagingService.kt", l = {}, m = "invokeSuspend")
    /* renamed from: net.safemoon.androidwallet.service.MyFirebaseMessagingService$onNewToken$1$2  reason: invalid class name */
    /* loaded from: classes2.dex */
    public static final class AnonymousClass2 extends SuspendLambda implements hd1<c90, q70<? super te4>, Object> {
        public final /* synthetic */ List<String> $listOFAddress;
        public final /* synthetic */ String $newToken;
        public int label;
        public final /* synthetic */ MyFirebaseMessagingService this$0;

        /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
        public AnonymousClass2(MyFirebaseMessagingService myFirebaseMessagingService, String str, List<String> list, q70<? super AnonymousClass2> q70Var) {
            super(2, q70Var);
            this.this$0 = myFirebaseMessagingService;
            this.$newToken = str;
            this.$listOFAddress = list;
        }

        @Override // kotlin.coroutines.jvm.internal.BaseContinuationImpl
        public final q70<te4> create(Object obj, q70<?> q70Var) {
            return new AnonymousClass2(this.this$0, this.$newToken, this.$listOFAddress, q70Var);
        }

        @Override // defpackage.hd1
        public final Object invoke(c90 c90Var, q70<? super te4> q70Var) {
            return ((AnonymousClass2) create(c90Var, q70Var)).invokeSuspend(te4.a);
        }

        @Override // kotlin.coroutines.jvm.internal.BaseContinuationImpl
        public final Object invokeSuspend(Object obj) {
            gs1.d();
            if (this.label == 0) {
                o83.b(obj);
                ac3 m = a4.m();
                fs1.e(m, "getSafeMoonClient()");
                g63 g63Var = new g63(m, this.this$0);
                String str = this.$newToken;
                Object[] array = this.$listOFAddress.toArray(new String[0]);
                Objects.requireNonNull(array, "null cannot be cast to non-null type kotlin.Array<T>");
                String[] strArr = (String[]) array;
                pm1.a.a(g63Var, str, (String[]) Arrays.copyOf(strArr, strArr.length), null, 4, null);
                return te4.a;
            }
            throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        }
    }

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public MyFirebaseMessagingService$onNewToken$1(MyFirebaseMessagingService myFirebaseMessagingService, String str, q70<? super MyFirebaseMessagingService$onNewToken$1> q70Var) {
        super(2, q70Var);
        this.this$0 = myFirebaseMessagingService;
        this.$newToken = str;
    }

    @Override // kotlin.coroutines.jvm.internal.BaseContinuationImpl
    public final q70<te4> create(Object obj, q70<?> q70Var) {
        return new MyFirebaseMessagingService$onNewToken$1(this.this$0, this.$newToken, q70Var);
    }

    @Override // defpackage.hd1
    public final Object invoke(c90 c90Var, q70<? super te4> q70Var) {
        return ((MyFirebaseMessagingService$onNewToken$1) create(c90Var, q70Var)).invokeSuspend(te4.a);
    }

    @Override // kotlin.coroutines.jvm.internal.BaseContinuationImpl
    public final Object invokeSuspend(Object obj) {
        List arrayList;
        Object d = gs1.d();
        int i = this.label;
        if (i == 0) {
            o83.b(obj);
            arrayList = new ArrayList();
            WalletDataSource walletDataSource = new WalletDataSource(MainRoomDatabase.n.b(this.this$0).P());
            this.L$0 = arrayList;
            this.label = 1;
            obj = walletDataSource.c(this);
            if (obj == d) {
                return d;
            }
        } else if (i == 1) {
            arrayList = (List) this.L$0;
            o83.b(obj);
        } else {
            if (i != 2) {
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
            o83.b(obj);
            return te4.a;
        }
        for (Wallet wallet2 : (Iterable) obj) {
            arrayList.add(wallet2.getAddress());
        }
        e32 c = tp0.c();
        AnonymousClass2 anonymousClass2 = new AnonymousClass2(this.this$0, this.$newToken, arrayList, null);
        this.L$0 = null;
        this.label = 2;
        if (kotlinx.coroutines.a.e(c, anonymousClass2, this) == d) {
            return d;
        }
        return te4.a;
    }
}
