package net.safemoon.androidwallet.service;

import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Intent;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Build;
import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;
import com.google.gson.Gson;
import defpackage.dh2;
import java.util.Map;
import java.util.Objects;
import net.safemoon.androidwallet.R;
import net.safemoon.androidwallet.activity.AKTHomeActivity;

/* compiled from: MyFirebaseMessagingService.kt */
/* loaded from: classes2.dex */
public final class MyFirebaseMessagingService extends FirebaseMessagingService {
    public static String k0;
    public static String l0;

    /* compiled from: MyFirebaseMessagingService.kt */
    /* loaded from: classes2.dex */
    public static final class a {
        public a() {
        }

        public /* synthetic */ a(qi0 qi0Var) {
            this();
        }
    }

    static {
        new a(null);
        k0 = "TRANSACTION_HASH";
        l0 = "ARG_NEW_TOKEN";
    }

    @Override // com.google.firebase.messaging.FirebaseMessagingService
    public void o(RemoteMessage remoteMessage) {
        ec0 ec0Var;
        fs1.f(remoteMessage, "remoteMessage");
        super.o(remoteMessage);
        Map<String, String> I1 = remoteMessage.I1();
        fs1.e(I1, "remoteMessage.data");
        RemoteMessage.b J1 = remoteMessage.J1();
        fs1.l("onMessageReceived:--> ", I1);
        String str = I1.get("transactionHash");
        if (J1 != null) {
            fs1.l("onMessageReceived:--> ", J1.c());
            fs1.l("onMessageReceived:--> ", J1.a());
            t(J1.c(), J1.a(), str);
        }
        if (!I1.containsKey("custom") || I1.get("custom") == null) {
            return;
        }
        try {
            ec0Var = (ec0) new Gson().fromJson(I1.get("custom"), (Class<Object>) ec0.class);
        } catch (Exception unused) {
            ec0Var = null;
        }
        if (ec0Var != null) {
            ec0Var.a();
        }
        if (fs1.b(null, "true")) {
            Intent intent = new Intent(this, AKTHomeActivity.class);
            intent.addFlags(335544320);
            intent.putExtra(l0, true);
            startActivity(intent);
        }
    }

    @Override // com.google.firebase.messaging.FirebaseMessagingService
    public void q(String str) {
        fs1.f(str, "newToken");
        super.q(str);
        if (bo3.j(getApplication(), "TEMP_FCM_TOKEN", null) == null) {
            as.b(qg1.a, tp0.b(), null, new MyFirebaseMessagingService$onNewToken$1(this, str, null), 2, null);
        }
    }

    public final void t(String str, String str2, String str3) {
        Intent intent = new Intent(this, AKTHomeActivity.class);
        intent.putExtra("isFrom", "Notification");
        intent.putExtra(k0, str3);
        intent.addFlags(536870912);
        PendingIntent activity = PendingIntent.getActivity(this, 0, intent, 201326592);
        Uri defaultUri = RingtoneManager.getDefaultUri(2);
        dh2.e y = new dh2.e(this, "net.safemoon.androidwallet").A(R.drawable.ic_notification).m(str).l(str2).g(true).E(new long[]{500, 500, 500, 500, 500}).n(-1).y(1);
        fs1.e(y, "Builder(this, MyApplicat…tionCompat.PRIORITY_HIGH)");
        y.u(-16776961, 1, 1).B(defaultUri).k(activity);
        Object systemService = getSystemService("notification");
        Objects.requireNonNull(systemService, "null cannot be cast to non-null type android.app.NotificationManager");
        NotificationManager notificationManager = (NotificationManager) systemService;
        if (Build.VERSION.SDK_INT >= 26) {
            NotificationChannel notificationChannel = new NotificationChannel("net.safemoon.androidwallet", getText(R.string.notification_channel_new_transaction), 4);
            notificationChannel.setDescription("Safemoon");
            notificationChannel.enableLights(true);
            notificationChannel.enableVibration(true);
            notificationChannel.setLightColor(-16711936);
            notificationChannel.setLockscreenVisibility(1);
            notificationChannel.setShowBadge(true);
            notificationManager.createNotificationChannel(notificationChannel);
            as.b(qg1.a, null, null, new MyFirebaseMessagingService$sendNotification$1(this, y, notificationManager, null), 3, null);
            return;
        }
        notificationManager.notify(0, y.b());
    }
}
