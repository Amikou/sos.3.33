package net.safemoon.androidwallet.service;

import android.content.SharedPreferences;
import kotlin.jvm.internal.Lambda;
import net.safemoon.androidwallet.service.TxnListService;
import net.safemoon.androidwallet.service.TxnListService$listener$2;

/* compiled from: TxnListService.kt */
/* loaded from: classes2.dex */
public final class TxnListService$listener$2 extends Lambda implements rc1<SharedPreferences.OnSharedPreferenceChangeListener> {
    public final /* synthetic */ TxnListService this$0;

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public TxnListService$listener$2(TxnListService txnListService) {
        super(0);
        this.this$0 = txnListService;
    }

    public static final void b(TxnListService txnListService, SharedPreferences sharedPreferences, String str) {
        fs1.f(txnListService, "this$0");
        if (sharedPreferences.contains(str)) {
            txnListService.o(30L);
        }
    }

    /* JADX WARN: Can't rename method to resolve collision */
    @Override // defpackage.rc1
    public final SharedPreferences.OnSharedPreferenceChangeListener invoke() {
        final TxnListService txnListService = this.this$0;
        return new SharedPreferences.OnSharedPreferenceChangeListener() { // from class: xc4
            @Override // android.content.SharedPreferences.OnSharedPreferenceChangeListener
            public final void onSharedPreferenceChanged(SharedPreferences sharedPreferences, String str) {
                TxnListService$listener$2.b(TxnListService.this, sharedPreferences, str);
            }
        };
    }
}
