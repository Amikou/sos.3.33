package net.safemoon.androidwallet.service;

import android.content.Context;
import androidx.work.ListenableWorker;
import androidx.work.Worker;
import androidx.work.WorkerParameters;

/* compiled from: DailyWorker.kt */
/* loaded from: classes2.dex */
public final class DailyWorker extends Worker {
    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public DailyWorker(Context context, WorkerParameters workerParameters) {
        super(context, workerParameters);
        fs1.f(context, "context");
        fs1.f(workerParameters, "workerParams");
    }

    @Override // androidx.work.Worker
    public ListenableWorker.a r() {
        System.out.print((Object) "JobRunning");
        e30.c0("JobRunning", "DAILY_JOB");
        ListenableWorker.a c = ListenableWorker.a.c();
        fs1.e(c, "success()");
        return c;
    }
}
