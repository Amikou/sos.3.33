package net.safemoon.androidwallet.service;

import android.content.SharedPreferences;
import kotlin.jvm.internal.Lambda;

/* compiled from: TxnListService.kt */
/* loaded from: classes2.dex */
public final class TxnListService$preference$2 extends Lambda implements rc1<SharedPreferences> {
    public final /* synthetic */ TxnListService this$0;

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public TxnListService$preference$2(TxnListService txnListService) {
        super(0);
        this.this$0 = txnListService;
    }

    /* JADX WARN: Can't rename method to resolve collision */
    @Override // defpackage.rc1
    public final SharedPreferences invoke() {
        return this.this$0.getSharedPreferences("TXLIST", 0);
    }
}
