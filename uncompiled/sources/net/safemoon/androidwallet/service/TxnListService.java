package net.safemoon.androidwallet.service;

import android.app.Application;
import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.Service;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Binder;
import android.os.Build;
import android.os.Handler;
import android.os.Looper;
import com.google.gson.Gson;
import defpackage.dh2;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import net.safemoon.androidwallet.model.request.RequestTransaction;
import net.safemoon.androidwallet.model.transaction.history.Result;
import net.safemoon.androidwallet.service.TxnListService;
import org.web3j.utils.Convert;

/* compiled from: TxnListService.kt */
/* loaded from: classes2.dex */
public final class TxnListService extends Service {
    public static final a o0 = new a(null);
    public NotificationManager h0;
    public final int a = 1;
    public final String f0 = "transaction_service";
    public final String g0 = "Transaction Service";
    public final b i0 = new b(this);
    public final c90 j0 = d90.a(tp0.c());
    public final sy1 k0 = zy1.a(new TxnListService$preference$2(this));
    public final sy1 l0 = zy1.a(new TxnListService$listener$2(this));
    public boolean m0 = true;
    public final Handler n0 = new Handler(Looper.getMainLooper());

    /* compiled from: TxnListService.kt */
    /* loaded from: classes2.dex */
    public static final class a {
        public a() {
        }

        public /* synthetic */ a(qi0 qi0Var) {
            this();
        }

        public final List<Result> a(Application application) {
            fs1.f(application, "application");
            ArrayList arrayList = new ArrayList();
            try {
                Iterator<T> it = application.getSharedPreferences("TXLIST", 0).getAll().entrySet().iterator();
                while (it.hasNext()) {
                    try {
                        RequestTransaction requestTransaction = (RequestTransaction) new Gson().fromJson(String.valueOf(((Map.Entry) it.next()).getValue()), (Class<Object>) RequestTransaction.class);
                        Result result = new Result();
                        Date x = b30.a.x(requestTransaction.getRequestTime());
                        if (x == null) {
                            x = new Date();
                        }
                        result.timeStamp = String.valueOf(x.getTime() / 1000);
                        result.hash = requestTransaction.getTransactionHash();
                        result.from = requestTransaction.getFrom();
                        result.to = requestTransaction.getTo();
                        result.tokenSymbol = requestTransaction.getSymbol();
                        result.value = String.valueOf(requestTransaction.getAmount());
                        result.offlinePending = true;
                        result.txreceiptStatus = "";
                        result.gasUsed = requestTransaction.getGasUsed();
                        BigDecimal wei = Convert.toWei(String.valueOf(requestTransaction.getGasPrice()), Convert.Unit.GWEI);
                        fs1.e(wei, "toWei(rT.gasPrice.toString(), Convert.Unit.GWEI)");
                        result.gasPrice = e30.h0(wei, 0, 1, null);
                        te4 te4Var = te4.a;
                        arrayList.add(result);
                    } catch (Exception unused) {
                    }
                }
            } catch (Exception unused2) {
            }
            return arrayList;
        }
    }

    /* compiled from: TxnListService.kt */
    /* loaded from: classes2.dex */
    public final class b extends Binder {
        public final /* synthetic */ TxnListService a;

        public b(TxnListService txnListService) {
            fs1.f(txnListService, "this$0");
            this.a = txnListService;
        }

        public final TxnListService a() {
            return this.a;
        }
    }

    /* compiled from: TxnListService.kt */
    /* loaded from: classes2.dex */
    public static final class c implements Runnable {
        public c() {
        }

        @Override // java.lang.Runnable
        public void run() {
            if (TxnListService.this.m0) {
                TxnListService.this.h();
                TxnListService.this.n0.postDelayed(this, 300000L);
            }
        }
    }

    public static final void p(TxnListService txnListService) {
        fs1.f(txnListService, "this$0");
        if (txnListService.m0) {
            txnListService.h();
        }
    }

    public final Notification g() {
        dh2.e j = j();
        Object systemService = getSystemService("notification");
        Objects.requireNonNull(systemService, "null cannot be cast to non-null type android.app.NotificationManager");
        this.h0 = (NotificationManager) systemService;
        if (Build.VERSION.SDK_INT >= 26) {
            NotificationChannel k = k();
            NotificationManager notificationManager = this.h0;
            fs1.d(notificationManager);
            notificationManager.createNotificationChannel(k);
        }
        Notification b2 = j.b();
        fs1.e(b2, "notificationBuilder.build()");
        return b2;
    }

    public final void h() {
        as.b(this.j0, null, null, new TxnListService$checkAndSubmit$1(this, null), 3, null);
    }

    public final SharedPreferences.OnSharedPreferenceChangeListener i() {
        return (SharedPreferences.OnSharedPreferenceChangeListener) this.l0.getValue();
    }

    public final dh2.e j() {
        dh2.e g = new dh2.e(getApplicationContext(), this.f0).g(true);
        fs1.e(g, "Builder(applicationConte…     .setAutoCancel(true)");
        return g;
    }

    public final NotificationChannel k() {
        return new NotificationChannel(this.f0, this.g0, 1);
    }

    public final SharedPreferences l() {
        return (SharedPreferences) this.k0.getValue();
    }

    @Override // android.app.Service
    /* renamed from: m */
    public b onBind(Intent intent) {
        return this.i0;
    }

    public final void n() {
        this.n0.removeCallbacksAndMessages(null);
        this.n0.postDelayed(new c(), 300000L);
    }

    public final void o(long j) {
        this.n0.removeCallbacksAndMessages(null);
        this.n0.postDelayed(new Runnable() { // from class: wc4
            @Override // java.lang.Runnable
            public final void run() {
                TxnListService.p(TxnListService.this);
            }
        }, j * 1000);
    }

    @Override // android.app.Service
    public void onCreate() {
        super.onCreate();
        this.m0 = true;
        l().registerOnSharedPreferenceChangeListener(i());
        h();
        n();
    }

    @Override // android.app.Service
    public void onDestroy() {
        super.onDestroy();
        this.m0 = false;
        this.n0.removeCallbacksAndMessages(null);
        l().unregisterOnSharedPreferenceChangeListener(i());
        NotificationManager notificationManager = this.h0;
        if (notificationManager != null) {
            notificationManager.cancel(this.a);
        }
        stopForeground(true);
    }

    @Override // android.app.Service
    public int onStartCommand(Intent intent, int i, int i2) {
        startForeground(this.a, g());
        return 2;
    }

    public final void q(RequestTransaction requestTransaction) {
        fs1.f(requestTransaction, "requestTransaction");
        l().edit().putString(String.valueOf(System.currentTimeMillis()), requestTransaction.toString()).apply();
    }
}
