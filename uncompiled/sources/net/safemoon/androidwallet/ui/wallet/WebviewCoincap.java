package net.safemoon.androidwallet.ui.wallet;

import android.content.DialogInterface;
import android.graphics.Bitmap;
import android.net.ConnectivityManager;
import android.net.http.SslError;
import android.os.Bundle;
import android.webkit.SslErrorHandler;
import android.webkit.WebChromeClient;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.ProgressBar;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.app.a;
import java.io.IOException;
import net.safemoon.androidwallet.R;

/* loaded from: classes2.dex */
public class WebviewCoincap extends AppCompatActivity {
    public WebView a;
    public ProgressBar f0;

    /* loaded from: classes2.dex */
    public class a extends WebViewClient {

        /* renamed from: net.safemoon.androidwallet.ui.wallet.WebviewCoincap$a$a  reason: collision with other inner class name */
        /* loaded from: classes2.dex */
        public class DialogInterface$OnClickListenerC0216a implements DialogInterface.OnClickListener {
            public final /* synthetic */ SslErrorHandler a;

            public DialogInterface$OnClickListenerC0216a(a aVar, SslErrorHandler sslErrorHandler) {
                this.a = sslErrorHandler;
            }

            @Override // android.content.DialogInterface.OnClickListener
            public void onClick(DialogInterface dialogInterface, int i) {
                this.a.proceed();
            }
        }

        /* loaded from: classes2.dex */
        public class b implements DialogInterface.OnClickListener {
            public final /* synthetic */ SslErrorHandler a;

            public b(a aVar, SslErrorHandler sslErrorHandler) {
                this.a = sslErrorHandler;
            }

            @Override // android.content.DialogInterface.OnClickListener
            public void onClick(DialogInterface dialogInterface, int i) {
                this.a.cancel();
            }
        }

        public a() {
        }

        @Override // android.webkit.WebViewClient
        public void onPageFinished(WebView webView, String str) {
            WebviewCoincap.this.f0.setVisibility(8);
        }

        @Override // android.webkit.WebViewClient
        public void onPageStarted(WebView webView, String str, Bitmap bitmap) {
            super.onPageStarted(webView, str, bitmap);
            WebviewCoincap.this.f0.setVisibility(0);
        }

        @Override // android.webkit.WebViewClient
        public void onReceivedSslError(WebView webView, SslErrorHandler sslErrorHandler, SslError sslError) {
            a.C0008a c0008a = new a.C0008a(WebviewCoincap.this);
            String string = WebviewCoincap.this.getString(R.string.ssl_message_certificate);
            int primaryError = sslError.getPrimaryError();
            if (primaryError == 0) {
                string = WebviewCoincap.this.getString(R.string.ssl_message_notyetvalid);
            } else if (primaryError == 1) {
                string = WebviewCoincap.this.getString(R.string.ssl_message_expired);
            } else if (primaryError == 2) {
                string = WebviewCoincap.this.getString(R.string.ssl_message_idmismatch);
            } else if (primaryError == 3) {
                string = WebviewCoincap.this.getString(R.string.ssl_message_untrusted);
            }
            c0008a.k(R.string.ssl_message_title);
            c0008a.f(string + WebviewCoincap.this.getString(R.string.ssl_message_post));
            c0008a.setPositiveButton(R.string.action_continue, new DialogInterface$OnClickListenerC0216a(this, sslErrorHandler));
            c0008a.setNegativeButton(R.string.cancel, new b(this, sslErrorHandler));
            c0008a.create().show();
        }
    }

    /* loaded from: classes2.dex */
    public class b extends WebChromeClient {
        public b() {
        }

        @Override // android.webkit.WebChromeClient
        public void onProgressChanged(WebView webView, int i) {
            WebviewCoincap.this.f0.setProgress(i);
        }
    }

    /* loaded from: classes2.dex */
    public class c implements DialogInterface.OnClickListener {
        public c() {
        }

        @Override // android.content.DialogInterface.OnClickListener
        public void onClick(DialogInterface dialogInterface, int i) {
            dialogInterface.cancel();
            WebviewCoincap.this.finish();
        }
    }

    public static boolean v() {
        try {
            int waitFor = Runtime.getRuntime().exec("/system/bin/ping -c 1 8.8.8.8").waitFor();
            StringBuilder sb = new StringBuilder();
            sb.append(waitFor);
            sb.append(" = internet");
            return waitFor == 0;
        } catch (IOException e) {
            e.getMessage().toString();
            return false;
        } catch (InterruptedException e2) {
            e2.getMessage().toString();
            return false;
        }
    }

    @Override // androidx.activity.ComponentActivity, android.app.Activity
    public void onBackPressed() {
        if (this.a.canGoBack()) {
            this.a.goBack();
        } else {
            finish();
        }
    }

    @Override // androidx.fragment.app.FragmentActivity, androidx.activity.ComponentActivity, androidx.core.app.ComponentActivity, android.app.Activity
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        setContentView(R.layout.webview_coincap);
        this.a = (WebView) findViewById(R.id.webView);
        this.f0 = (ProgressBar) findViewById(R.id.progressBar);
        getSupportActionBar().l();
        this.f0.setMax(100);
        this.f0.setProgress(1);
        this.a.getSettings().setJavaScriptEnabled(true);
        this.a.getSettings().setDatabaseEnabled(true);
        this.a.getSettings().setDomStorageEnabled(true);
        this.a.getSettings().setSupportZoom(true);
        this.a.getSettings().setJavaScriptCanOpenWindowsAutomatically(true);
        this.a.getSettings().setBuiltInZoomControls(true);
        this.a.getSettings().setGeolocationEnabled(true);
        this.a.setWebViewClient(new a());
        this.a.setWebChromeClient(new b());
        if (u() && v()) {
            this.a.loadUrl("https://coinmarketcap.com/");
        } else {
            t();
        }
    }

    public void t() {
        new a.C0008a(this).k(R.string.title_no_network).e(R.string.message_no_network).setPositiveButton(R.string.ok, new c()).l();
    }

    public final boolean u() {
        ConnectivityManager connectivityManager = (ConnectivityManager) getSystemService("connectivity");
        return connectivityManager.getActiveNetworkInfo() != null && connectivityManager.getActiveNetworkInfo().isConnected();
    }
}
