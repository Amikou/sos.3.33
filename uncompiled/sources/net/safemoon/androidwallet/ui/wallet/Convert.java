package net.safemoon.androidwallet.ui.wallet;

import java.math.BigDecimal;

/* loaded from: classes2.dex */
public final class Convert {

    /* loaded from: classes2.dex */
    public enum Unit {
        WEI("wei", 0),
        KWEI("kwei", 3),
        MWEI("mwei", 6),
        GWEI("gwei", 9),
        SZABO("szabo", 12),
        FINNEY("finney", 15),
        ETHER("ether", 18),
        KETHER("kether", 21),
        METHER("mether", 24),
        GETHER("gether", 27),
        CUSTOM("custom", 8);
        
        private int factor;
        private String name;
        private BigDecimal weiFactor;

        Unit(String str, int i) {
            this.name = str;
            this.factor = i;
            this.weiFactor = BigDecimal.TEN.pow(i);
        }

        public static Unit fromDecmal(int i) {
            Unit[] values;
            for (Unit unit : values()) {
                if (i == unit.factor) {
                    return unit;
                }
            }
            Unit unit2 = CUSTOM;
            unit2.factor = i;
            unit2.name = "custom_" + i;
            unit2.weiFactor = BigDecimal.TEN.pow(i);
            return unit2;
        }

        public static Unit fromString(String str) {
            Unit[] values;
            if (str != null) {
                for (Unit unit : values()) {
                    if (str.equalsIgnoreCase(unit.name)) {
                        return unit;
                    }
                }
            }
            return valueOf(str);
        }

        public int getFactor() {
            return this.factor;
        }

        public BigDecimal getWeiFactor() {
            return this.weiFactor;
        }

        @Override // java.lang.Enum
        public String toString() {
            return this.name;
        }
    }

    public static BigDecimal a(String str, Unit unit) {
        return b(new BigDecimal(str), unit);
    }

    public static BigDecimal b(BigDecimal bigDecimal, Unit unit) {
        return bigDecimal.divide(unit.getWeiFactor());
    }

    public static BigDecimal c(String str, Unit unit) {
        return d(new BigDecimal(str), unit);
    }

    public static BigDecimal d(BigDecimal bigDecimal, Unit unit) {
        return bigDecimal.multiply(unit.getWeiFactor());
    }
}
