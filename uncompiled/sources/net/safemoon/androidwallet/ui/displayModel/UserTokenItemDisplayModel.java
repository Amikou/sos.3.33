package net.safemoon.androidwallet.ui.displayModel;

import com.github.mikephil.charting.utils.Utils;
import com.google.gson.Gson;
import java.io.Serializable;
import net.safemoon.androidwallet.model.token.abstraction.IToken;
import net.safemoon.androidwallet.model.token.gson.GsonToken;
import org.web3j.ens.contracts.generated.PublicResolver;

/* compiled from: UserTokenItemDisplayModel.kt */
/* loaded from: classes2.dex */
public final class UserTokenItemDisplayModel implements Serializable {
    public static final a Companion = new a(null);
    private boolean allowSwap;
    private final double balanceInUSDT;
    private final int chainId;
    private final String cmcId;
    private final String contractAddress;
    private final int decimals;
    private final String iconFile;
    private final int iconResId;
    private final String name;
    private final double nativeBalance;
    private final double percentChange1h;
    private final double priceInUsdt;
    private final String symbol;
    private final String symbolWithType;

    /* compiled from: UserTokenItemDisplayModel.kt */
    /* loaded from: classes2.dex */
    public static final class a {
        public a() {
        }

        public /* synthetic */ a(qi0 qi0Var) {
            this();
        }
    }

    public UserTokenItemDisplayModel(String str, int i, String str2, String str3, String str4, int i2, int i3, boolean z, double d, double d2, double d3, double d4, String str5, String str6) {
        fs1.f(str, "symbolWithType");
        fs1.f(str2, PublicResolver.FUNC_NAME);
        fs1.f(str3, "symbol");
        fs1.f(str4, "contractAddress");
        fs1.f(str5, "iconFile");
        this.symbolWithType = str;
        this.iconResId = i;
        this.name = str2;
        this.symbol = str3;
        this.contractAddress = str4;
        this.decimals = i2;
        this.chainId = i3;
        this.allowSwap = z;
        this.priceInUsdt = d;
        this.nativeBalance = d2;
        this.percentChange1h = d3;
        this.balanceInUSDT = d4;
        this.iconFile = str5;
        this.cmcId = str6;
    }

    public final String component1() {
        return this.symbolWithType;
    }

    public final double component10() {
        return this.nativeBalance;
    }

    public final double component11() {
        return this.percentChange1h;
    }

    public final double component12() {
        return this.balanceInUSDT;
    }

    public final String component13() {
        return this.iconFile;
    }

    public final String component14() {
        return this.cmcId;
    }

    public final int component2() {
        return this.iconResId;
    }

    public final String component3() {
        return this.name;
    }

    public final String component4() {
        return this.symbol;
    }

    public final String component5() {
        return this.contractAddress;
    }

    public final int component6() {
        return this.decimals;
    }

    public final int component7() {
        return this.chainId;
    }

    public final boolean component8() {
        return this.allowSwap;
    }

    public final double component9() {
        return this.priceInUsdt;
    }

    public final UserTokenItemDisplayModel copy(String str, int i, String str2, String str3, String str4, int i2, int i3, boolean z, double d, double d2, double d3, double d4, String str5, String str6) {
        fs1.f(str, "symbolWithType");
        fs1.f(str2, PublicResolver.FUNC_NAME);
        fs1.f(str3, "symbol");
        fs1.f(str4, "contractAddress");
        fs1.f(str5, "iconFile");
        return new UserTokenItemDisplayModel(str, i, str2, str3, str4, i2, i3, z, d, d2, d3, d4, str5, str6);
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj instanceof UserTokenItemDisplayModel) {
            UserTokenItemDisplayModel userTokenItemDisplayModel = (UserTokenItemDisplayModel) obj;
            return fs1.b(this.symbolWithType, userTokenItemDisplayModel.symbolWithType) && this.iconResId == userTokenItemDisplayModel.iconResId && fs1.b(this.name, userTokenItemDisplayModel.name) && fs1.b(this.symbol, userTokenItemDisplayModel.symbol) && fs1.b(this.contractAddress, userTokenItemDisplayModel.contractAddress) && this.decimals == userTokenItemDisplayModel.decimals && this.chainId == userTokenItemDisplayModel.chainId && this.allowSwap == userTokenItemDisplayModel.allowSwap && fs1.b(Double.valueOf(this.priceInUsdt), Double.valueOf(userTokenItemDisplayModel.priceInUsdt)) && fs1.b(Double.valueOf(this.nativeBalance), Double.valueOf(userTokenItemDisplayModel.nativeBalance)) && fs1.b(Double.valueOf(this.percentChange1h), Double.valueOf(userTokenItemDisplayModel.percentChange1h)) && fs1.b(Double.valueOf(this.balanceInUSDT), Double.valueOf(userTokenItemDisplayModel.balanceInUSDT)) && fs1.b(this.iconFile, userTokenItemDisplayModel.iconFile) && fs1.b(this.cmcId, userTokenItemDisplayModel.cmcId);
        }
        return false;
    }

    public final boolean getAllowSwap() {
        return this.allowSwap;
    }

    public final double getBalanceInUSDT() {
        return this.balanceInUSDT;
    }

    public final int getChainId() {
        return this.chainId;
    }

    public final String getCmcId() {
        return this.cmcId;
    }

    public final String getContractAddress() {
        return this.contractAddress;
    }

    public final int getDecimals() {
        return this.decimals;
    }

    public final String getIconFile() {
        return this.iconFile;
    }

    public final int getIconResId() {
        return this.iconResId;
    }

    public final String getName() {
        return this.name;
    }

    public final double getNativeBalance() {
        return this.nativeBalance;
    }

    public final double getPercentChange1h() {
        return this.percentChange1h;
    }

    public final double getPriceInUsdt() {
        return this.priceInUsdt;
    }

    public final String getSymbol() {
        return this.symbol;
    }

    public final String getSymbolWithType() {
        return this.symbolWithType;
    }

    /* JADX WARN: Multi-variable type inference failed */
    public int hashCode() {
        int hashCode = ((((((((((((this.symbolWithType.hashCode() * 31) + this.iconResId) * 31) + this.name.hashCode()) * 31) + this.symbol.hashCode()) * 31) + this.contractAddress.hashCode()) * 31) + this.decimals) * 31) + this.chainId) * 31;
        boolean z = this.allowSwap;
        int i = z;
        if (z != 0) {
            i = 1;
        }
        int doubleToLongBits = (((((((((((hashCode + i) * 31) + Double.doubleToLongBits(this.priceInUsdt)) * 31) + Double.doubleToLongBits(this.nativeBalance)) * 31) + Double.doubleToLongBits(this.percentChange1h)) * 31) + Double.doubleToLongBits(this.balanceInUSDT)) * 31) + this.iconFile.hashCode()) * 31;
        String str = this.cmcId;
        return doubleToLongBits + (str == null ? 0 : str.hashCode());
    }

    public final void setAllowSwap(boolean z) {
        this.allowSwap = z;
    }

    public String toString() {
        String json = new Gson().toJson(this, UserTokenItemDisplayModel.class);
        fs1.e(json, "Gson().toJson(this, User…DisplayModel::class.java)");
        return json;
    }

    public final IToken toToken() {
        String str = this.symbolWithType;
        return new GsonToken(this.name, this.symbol, "", this.contractAddress, this.chainId, this.decimals, this.allowSwap, str, 0, null, this.priceInUsdt, this.nativeBalance, Utils.DOUBLE_EPSILON, 4864, null);
    }

    public /* synthetic */ UserTokenItemDisplayModel(String str, int i, String str2, String str3, String str4, int i2, int i3, boolean z, double d, double d2, double d3, double d4, String str5, String str6, int i4, qi0 qi0Var) {
        this(str, i, str2, str3, str4, i2, i3, z, d, d2, d3, (i4 & 2048) != 0 ? 0.0d : d4, str5, str6);
    }
}
