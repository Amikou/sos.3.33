package net.safemoon.androidwallet.provider;

import android.content.Intent;
import kotlin.jvm.internal.Lambda;

/* compiled from: RequiredAuthorizeProvider.kt */
/* loaded from: classes2.dex */
public final class RequiredAuthorizeProvider$launch$1 extends Lambda implements tc1<Intent, te4> {
    public final /* synthetic */ rc1<te4> $onFail;
    public final /* synthetic */ rc1<te4> $onSuccess;

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public RequiredAuthorizeProvider$launch$1(rc1<te4> rc1Var, rc1<te4> rc1Var2) {
        super(1);
        this.$onSuccess = rc1Var;
        this.$onFail = rc1Var2;
    }

    @Override // defpackage.tc1
    public /* bridge */ /* synthetic */ te4 invoke(Intent intent) {
        invoke2(intent);
        return te4.a;
    }

    /* renamed from: invoke  reason: avoid collision after fix types in other method */
    public final void invoke2(Intent intent) {
        if (intent != null) {
            this.$onSuccess.invoke();
        } else {
            this.$onFail.invoke();
        }
    }
}
