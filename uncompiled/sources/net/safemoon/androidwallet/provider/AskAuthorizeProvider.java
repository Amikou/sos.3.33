package net.safemoon.androidwallet.provider;

import android.content.Context;
import android.content.Intent;

/* compiled from: AskAuthorizeProvider.kt */
/* loaded from: classes2.dex */
public final class AskAuthorizeProvider {
    public final Context a;
    public final gm1 b;

    public AskAuthorizeProvider(Context context, gm1 gm1Var) {
        fs1.f(context, "context");
        this.a = context;
        this.b = gm1Var;
    }

    public final void a(rc1<te4> rc1Var, rc1<te4> rc1Var2) {
        hn2 a;
        w7<Intent> b;
        fs1.f(rc1Var, "onSuccess");
        fs1.f(rc1Var2, "onFail");
        if (bo3.e(this.a, "ASK_AUTH_TRANSACTION_SIGN", true)) {
            gm1 gm1Var = this.b;
            if (gm1Var == null || (a = gm1Var.a()) == null || (b = a.b(new AskAuthorizeProvider$launch$1(rc1Var, rc1Var2))) == null) {
                return;
            }
            b.a(this.b.j());
            return;
        }
        rc1Var.invoke();
    }
}
