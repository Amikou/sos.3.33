package net.safemoon.androidwallet.provider;

import android.content.Context;
import android.content.Intent;

/* compiled from: RequiredAuthorizeProvider.kt */
/* loaded from: classes2.dex */
public final class RequiredAuthorizeProvider {
    public final gm1 a;

    public RequiredAuthorizeProvider(Context context, gm1 gm1Var) {
        fs1.f(context, "context");
        this.a = gm1Var;
    }

    public final void a(rc1<te4> rc1Var, rc1<te4> rc1Var2) {
        hn2 a;
        w7<Intent> b;
        fs1.f(rc1Var, "onSuccess");
        fs1.f(rc1Var2, "onFail");
        gm1 gm1Var = this.a;
        if (gm1Var == null || (a = gm1Var.a()) == null || (b = a.b(new RequiredAuthorizeProvider$launch$1(rc1Var, rc1Var2))) == null) {
            return;
        }
        gm1 gm1Var2 = this.a;
        b.a(gm1Var2 == null ? null : gm1Var2.j());
    }
}
