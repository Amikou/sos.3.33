package net.safemoon.androidwallet.provider;

import kotlin.coroutines.jvm.internal.SuspendLambda;
import kotlin.coroutines.jvm.internal.a;
import kotlinx.coroutines.CoroutineDispatcher;

/* compiled from: TokenInfoExistProvider.kt */
@a(c = "net.safemoon.androidwallet.provider.TokenInfoExistProvider$getTokenInfoForSlug$1", f = "TokenInfoExistProvider.kt", l = {23}, m = "invokeSuspend")
/* loaded from: classes2.dex */
public final class TokenInfoExistProvider$getTokenInfoForSlug$1 extends SuspendLambda implements hd1<c90, q70<? super te4>, Object> {
    public final /* synthetic */ Integer $cmcId;
    public final /* synthetic */ gb2<String> $slugLiveData;
    public int label;
    public final /* synthetic */ TokenInfoExistProvider this$0;

    /* compiled from: TokenInfoExistProvider.kt */
    @a(c = "net.safemoon.androidwallet.provider.TokenInfoExistProvider$getTokenInfoForSlug$1$1", f = "TokenInfoExistProvider.kt", l = {25, 27, 29, 33}, m = "invokeSuspend")
    /* renamed from: net.safemoon.androidwallet.provider.TokenInfoExistProvider$getTokenInfoForSlug$1$1  reason: invalid class name */
    /* loaded from: classes2.dex */
    public static final class AnonymousClass1 extends SuspendLambda implements hd1<c90, q70<? super te4>, Object> {
        public final /* synthetic */ Integer $cmcId;
        public final /* synthetic */ gb2<String> $slugLiveData;
        public Object L$0;
        public int label;
        public final /* synthetic */ TokenInfoExistProvider this$0;

        /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
        public AnonymousClass1(TokenInfoExistProvider tokenInfoExistProvider, Integer num, gb2<String> gb2Var, q70<? super AnonymousClass1> q70Var) {
            super(2, q70Var);
            this.this$0 = tokenInfoExistProvider;
            this.$cmcId = num;
            this.$slugLiveData = gb2Var;
        }

        @Override // kotlin.coroutines.jvm.internal.BaseContinuationImpl
        public final q70<te4> create(Object obj, q70<?> q70Var) {
            return new AnonymousClass1(this.this$0, this.$cmcId, this.$slugLiveData, q70Var);
        }

        @Override // defpackage.hd1
        public final Object invoke(c90 c90Var, q70<? super te4> q70Var) {
            return ((AnonymousClass1) create(c90Var, q70Var)).invokeSuspend(te4.a);
        }

        /* JADX WARN: Removed duplicated region for block: B:34:0x008e A[RETURN] */
        /* JADX WARN: Removed duplicated region for block: B:37:0x0093  */
        /* JADX WARN: Removed duplicated region for block: B:38:0x0094 A[Catch: Exception -> 0x00f1, TryCatch #0 {Exception -> 0x00f1, blocks: (B:8:0x0017, B:53:0x00de, B:13:0x002a, B:35:0x008f, B:41:0x009d, B:44:0x00a4, B:47:0x00ae, B:50:0x00ba, B:38:0x0094, B:26:0x006f, B:29:0x0077, B:32:0x0084), top: B:59:0x000d }] */
        /* JADX WARN: Removed duplicated region for block: B:40:0x009c A[ADDED_TO_REGION] */
        /* JADX WARN: Removed duplicated region for block: B:41:0x009d A[Catch: Exception -> 0x00f1, TryCatch #0 {Exception -> 0x00f1, blocks: (B:8:0x0017, B:53:0x00de, B:13:0x002a, B:35:0x008f, B:41:0x009d, B:44:0x00a4, B:47:0x00ae, B:50:0x00ba, B:38:0x0094, B:26:0x006f, B:29:0x0077, B:32:0x0084), top: B:59:0x000d }] */
        /* JADX WARN: Removed duplicated region for block: B:49:0x00b9  */
        /* JADX WARN: Removed duplicated region for block: B:50:0x00ba A[Catch: Exception -> 0x00f1, TryCatch #0 {Exception -> 0x00f1, blocks: (B:8:0x0017, B:53:0x00de, B:13:0x002a, B:35:0x008f, B:41:0x009d, B:44:0x00a4, B:47:0x00ae, B:50:0x00ba, B:38:0x0094, B:26:0x006f, B:29:0x0077, B:32:0x0084), top: B:59:0x000d }] */
        @Override // kotlin.coroutines.jvm.internal.BaseContinuationImpl
        /*
            Code decompiled incorrectly, please refer to instructions dump.
            To view partially-correct code enable 'Show inconsistent code' option in preferences
        */
        public final java.lang.Object invokeSuspend(java.lang.Object r17) {
            /*
                Method dump skipped, instructions count: 244
                To view this dump change 'Code comments level' option to 'DEBUG'
            */
            throw new UnsupportedOperationException("Method not decompiled: net.safemoon.androidwallet.provider.TokenInfoExistProvider$getTokenInfoForSlug$1.AnonymousClass1.invokeSuspend(java.lang.Object):java.lang.Object");
        }
    }

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public TokenInfoExistProvider$getTokenInfoForSlug$1(TokenInfoExistProvider tokenInfoExistProvider, Integer num, gb2<String> gb2Var, q70<? super TokenInfoExistProvider$getTokenInfoForSlug$1> q70Var) {
        super(2, q70Var);
        this.this$0 = tokenInfoExistProvider;
        this.$cmcId = num;
        this.$slugLiveData = gb2Var;
    }

    @Override // kotlin.coroutines.jvm.internal.BaseContinuationImpl
    public final q70<te4> create(Object obj, q70<?> q70Var) {
        return new TokenInfoExistProvider$getTokenInfoForSlug$1(this.this$0, this.$cmcId, this.$slugLiveData, q70Var);
    }

    @Override // defpackage.hd1
    public final Object invoke(c90 c90Var, q70<? super te4> q70Var) {
        return ((TokenInfoExistProvider$getTokenInfoForSlug$1) create(c90Var, q70Var)).invokeSuspend(te4.a);
    }

    @Override // kotlin.coroutines.jvm.internal.BaseContinuationImpl
    public final Object invokeSuspend(Object obj) {
        Object d = gs1.d();
        int i = this.label;
        if (i == 0) {
            o83.b(obj);
            CoroutineDispatcher b = tp0.b();
            AnonymousClass1 anonymousClass1 = new AnonymousClass1(this.this$0, this.$cmcId, this.$slugLiveData, null);
            this.label = 1;
            if (kotlinx.coroutines.a.e(b, anonymousClass1, this) == d) {
                return d;
            }
        } else if (i != 1) {
            throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        } else {
            o83.b(obj);
        }
        return te4.a;
    }
}
