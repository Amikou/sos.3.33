package net.safemoon.androidwallet.provider;

import android.content.Context;
import kotlin.jvm.internal.Lambda;
import net.safemoon.androidwallet.database.room.ApplicationRoomDatabase;

/* compiled from: TokenInfoExistProvider.kt */
/* loaded from: classes2.dex */
public final class TokenInfoExistProvider$tokenInfoDataSource$2 extends Lambda implements rc1<x64> {
    public final /* synthetic */ Context $context;

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public TokenInfoExistProvider$tokenInfoDataSource$2(Context context) {
        super(0);
        this.$context = context;
    }

    @Override // defpackage.rc1
    public final x64 invoke() {
        return ApplicationRoomDatabase.k.c(ApplicationRoomDatabase.n, this.$context, null, 2, null).Y();
    }
}
