package net.safemoon.androidwallet.provider;

import android.content.Context;
import kotlin.jvm.internal.Lambda;

/* compiled from: TokenInfoExistProvider.kt */
/* loaded from: classes2.dex */
public final class TokenInfoExistProvider$customTokenProvider$2 extends Lambda implements rc1<yc0> {
    public final /* synthetic */ Context $context;

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public TokenInfoExistProvider$customTokenProvider$2(Context context) {
        super(0);
        this.$context = context;
    }

    @Override // defpackage.rc1
    public final yc0 invoke() {
        return zc0.a.a(this.$context);
    }
}
