package net.safemoon.androidwallet.provider;

import android.content.Context;
import java.util.List;

/* compiled from: MyAppInitializer.kt */
/* loaded from: classes2.dex */
public class MyAppInitializer implements rq1<rf> {
    @Override // defpackage.rq1
    public List<Class<? extends rq1<?>>> a() {
        return b20.g();
    }

    @Override // defpackage.rq1
    /* renamed from: c */
    public rf b(Context context) {
        fs1.f(context, "context");
        return rf.a.a(context);
    }
}
