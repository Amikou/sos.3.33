package net.safemoon.androidwallet.provider;

import android.content.Context;

/* compiled from: TokenInfoExistProvider.kt */
/* loaded from: classes2.dex */
public final class TokenInfoExistProvider {
    public final sy1 a;
    public final sy1 b;

    public TokenInfoExistProvider(Context context) {
        fs1.f(context, "context");
        this.a = zy1.a(new TokenInfoExistProvider$tokenInfoDataSource$2(context));
        this.b = zy1.a(new TokenInfoExistProvider$customTokenProvider$2(context));
    }

    public final yc0 c() {
        return (yc0) this.b.getValue();
    }

    public final x64 d() {
        return (x64) this.a.getValue();
    }

    public final void e(Integer num, gb2<String> gb2Var) {
        fs1.f(gb2Var, "slugLiveData");
        if (num == null) {
            return;
        }
        as.b(qg1.a, null, null, new TokenInfoExistProvider$getTokenInfoForSlug$1(this, num, gb2Var, null), 3, null);
    }
}
