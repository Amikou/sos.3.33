package net.safemoon.androidwallet.model.fiat;

import net.safemoon.androidwallet.model.common.HistoryListItem;
import net.safemoon.androidwallet.model.fiat.room.RoomFiat;

/* compiled from: ResultItemFiat.kt */
/* loaded from: classes2.dex */
public final class ResultItemFiat extends HistoryListItem {
    private RoomFiat result;

    public final RoomFiat getResult() {
        return this.result;
    }

    @Override // net.safemoon.androidwallet.model.common.HistoryListItem
    public int getType() {
        return 2;
    }

    public final void setResult(RoomFiat roomFiat) {
        this.result = roomFiat;
    }
}
