package net.safemoon.androidwallet.model.fiat.gson;

import com.google.gson.Gson;
import net.safemoon.androidwallet.model.fiat.abstraction.IFiat;
import org.web3j.abi.datatypes.Utf8String;
import org.web3j.ens.contracts.generated.PublicResolver;

/* compiled from: Fiat.kt */
/* loaded from: classes2.dex */
public final class Fiat implements IFiat {
    public static final Companion Companion = new Companion(null);
    private static final Fiat DEFAULT_CURRENCY;
    private static final String DEFAULT_CURRENCY_STRING;
    private final String name;
    private final Double rate;
    private final String symbol;

    /* compiled from: Fiat.kt */
    /* loaded from: classes2.dex */
    public static final class Companion {
        private Companion() {
        }

        public /* synthetic */ Companion(qi0 qi0Var) {
            this();
        }

        public final Fiat getDEFAULT_CURRENCY() {
            return Fiat.DEFAULT_CURRENCY;
        }

        public final String getDEFAULT_CURRENCY_STRING() {
            return Fiat.DEFAULT_CURRENCY_STRING;
        }

        public final Fiat to(String str) {
            fs1.f(str, Utf8String.TYPE_NAME);
            try {
                Object fromJson = new Gson().fromJson(str, (Class<Object>) Fiat.class);
                fs1.e(fromJson, "{\n            Gson().fro…at::class.java)\n        }");
                return (Fiat) fromJson;
            } catch (Exception unused) {
                return getDEFAULT_CURRENCY();
            }
        }
    }

    static {
        Fiat fiat = new Fiat("USD", "United States Dollar", Double.valueOf(1.0d));
        DEFAULT_CURRENCY = fiat;
        DEFAULT_CURRENCY_STRING = fiat.toString();
    }

    public Fiat(String str, String str2, Double d) {
        fs1.f(str, "symbol");
        this.symbol = str;
        this.name = str2;
        this.rate = d;
    }

    @Override // net.safemoon.androidwallet.model.fiat.abstraction.IFiat
    public String getName() {
        return this.name;
    }

    @Override // net.safemoon.androidwallet.model.fiat.abstraction.IFiat
    public Double getRate() {
        return this.rate;
    }

    @Override // net.safemoon.androidwallet.model.fiat.abstraction.IFiat
    public String getSymbol() {
        return this.symbol;
    }

    public String toString() {
        String json = new Gson().toJson(this);
        fs1.e(json, "Gson().toJson(this)");
        return json;
    }

    /* JADX WARN: 'this' call moved to the top of the method (can break code semantics) */
    public Fiat(String str, String str2) {
        this(str, str2, null);
        fs1.f(str, "symbol");
        fs1.f(str2, PublicResolver.FUNC_NAME);
    }

    /* JADX WARN: 'this' call moved to the top of the method (can break code semantics) */
    public Fiat(String str, double d) {
        this(str, null, Double.valueOf(d));
        fs1.f(str, "symbol");
    }
}
