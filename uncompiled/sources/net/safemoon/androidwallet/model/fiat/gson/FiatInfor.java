package net.safemoon.androidwallet.model.fiat.gson;

import com.google.gson.JsonObject;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import org.web3j.ens.contracts.generated.PublicResolver;

/* compiled from: FiatInfor.kt */
/* loaded from: classes2.dex */
public final class FiatInfor implements Serializable {
    @SerializedName("code")
    @Expose
    private final String code;
    @SerializedName("success")
    @Expose
    private final Boolean success;
    @SerializedName("symbols")
    @Expose
    private JsonObject symbols;

    public final List<Fiat> allFiatDetail() {
        ArrayList arrayList = new ArrayList();
        JsonObject jsonObject = this.symbols;
        if (jsonObject != null) {
            Set<String> keySet = jsonObject.keySet();
            fs1.e(keySet, "symbol.keySet()");
            for (String str : keySet) {
                try {
                    String asString = jsonObject.get(str).getAsString();
                    fs1.e(str, "it");
                    fs1.e(asString, PublicResolver.FUNC_NAME);
                    arrayList.add(new Fiat(str, asString));
                } catch (Exception unused) {
                }
            }
        }
        return arrayList;
    }

    public final String getCode() {
        return this.code;
    }

    public final Boolean getSuccess() {
        return this.success;
    }
}
