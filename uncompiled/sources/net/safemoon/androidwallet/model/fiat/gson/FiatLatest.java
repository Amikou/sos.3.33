package net.safemoon.androidwallet.model.fiat.gson;

import com.google.gson.JsonObject;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;

/* compiled from: FiatLatest.kt */
/* loaded from: classes2.dex */
public final class FiatLatest implements Serializable {
    @SerializedName("base")
    @Expose
    private final String base;
    @SerializedName("date")
    @Expose
    private final String date;
    @SerializedName("rates")
    @Expose
    private final JsonObject rates;
    @SerializedName("success")
    @Expose
    private final Boolean success;
    @SerializedName("timestamp")
    @Expose
    private final Long timestamp;

    public final List<Fiat> allFiatRates() {
        ArrayList arrayList = new ArrayList();
        JsonObject jsonObject = this.rates;
        if (jsonObject != null) {
            Set<String> keySet = jsonObject.keySet();
            fs1.e(keySet, "symbol.keySet()");
            for (String str : keySet) {
                try {
                    double asDouble = jsonObject.get(str).getAsDouble();
                    fs1.e(str, "it");
                    arrayList.add(new Fiat(str, asDouble));
                } catch (Exception unused) {
                }
            }
        }
        return arrayList;
    }

    public final String getBase() {
        return this.base;
    }

    public final String getDate() {
        return this.date;
    }

    public final Boolean getSuccess() {
        return this.success;
    }

    public final Long getTimestamp() {
        return this.timestamp;
    }
}
