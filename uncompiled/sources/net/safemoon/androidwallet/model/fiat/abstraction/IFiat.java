package net.safemoon.androidwallet.model.fiat.abstraction;

import java.io.Serializable;

/* compiled from: IFiat.kt */
/* loaded from: classes2.dex */
public interface IFiat extends Serializable {
    String getName();

    Double getRate();

    String getSymbol();
}
