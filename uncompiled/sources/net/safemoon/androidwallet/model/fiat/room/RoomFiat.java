package net.safemoon.androidwallet.model.fiat.room;

import net.safemoon.androidwallet.model.fiat.abstraction.IFiat;

/* compiled from: RoomFiat.kt */
/* loaded from: classes2.dex */
public final class RoomFiat implements IFiat {
    private final String name;
    private final Double rate;
    private final String symbol;

    public RoomFiat(String str, String str2, Double d) {
        fs1.f(str, "symbol");
        this.symbol = str;
        this.name = str2;
        this.rate = d;
    }

    public static /* synthetic */ RoomFiat copy$default(RoomFiat roomFiat, String str, String str2, Double d, int i, Object obj) {
        if ((i & 1) != 0) {
            str = roomFiat.getSymbol();
        }
        if ((i & 2) != 0) {
            str2 = roomFiat.getName();
        }
        if ((i & 4) != 0) {
            d = roomFiat.getRate();
        }
        return roomFiat.copy(str, str2, d);
    }

    public final String component1() {
        return getSymbol();
    }

    public final String component2() {
        return getName();
    }

    public final Double component3() {
        return getRate();
    }

    public final RoomFiat copy(String str, String str2, Double d) {
        fs1.f(str, "symbol");
        return new RoomFiat(str, str2, d);
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj instanceof RoomFiat) {
            RoomFiat roomFiat = (RoomFiat) obj;
            return fs1.b(getSymbol(), roomFiat.getSymbol()) && fs1.b(getName(), roomFiat.getName()) && fs1.b(getRate(), roomFiat.getRate());
        }
        return false;
    }

    @Override // net.safemoon.androidwallet.model.fiat.abstraction.IFiat
    public String getName() {
        return this.name;
    }

    @Override // net.safemoon.androidwallet.model.fiat.abstraction.IFiat
    public Double getRate() {
        return this.rate;
    }

    @Override // net.safemoon.androidwallet.model.fiat.abstraction.IFiat
    public String getSymbol() {
        return this.symbol;
    }

    public int hashCode() {
        return (((getSymbol().hashCode() * 31) + (getName() == null ? 0 : getName().hashCode())) * 31) + (getRate() != null ? getRate().hashCode() : 0);
    }

    public String toString() {
        return "RoomFiat(symbol=" + getSymbol() + ", name=" + ((Object) getName()) + ", rate=" + getRate() + ')';
    }

    /* JADX WARN: 'this' call moved to the top of the method (can break code semantics) */
    public RoomFiat(IFiat iFiat) {
        this(iFiat.getSymbol(), iFiat.getName(), iFiat.getRate());
        fs1.f(iFiat, "origin");
    }
}
