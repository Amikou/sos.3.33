package net.safemoon.androidwallet.model.collectible;

import java.util.List;

/* compiled from: RoomCollectionAndNft.kt */
/* loaded from: classes2.dex */
public final class RoomCollectionAndNft {
    private final RoomCollection collection;
    private final List<RoomNFT> nfts;
    private boolean showHideIcon;

    public RoomCollectionAndNft(RoomCollection roomCollection, List<RoomNFT> list) {
        fs1.f(roomCollection, "collection");
        fs1.f(list, "nfts");
        this.collection = roomCollection;
        this.nfts = list;
    }

    /* JADX WARN: Multi-variable type inference failed */
    public static /* synthetic */ RoomCollectionAndNft copy$default(RoomCollectionAndNft roomCollectionAndNft, RoomCollection roomCollection, List list, int i, Object obj) {
        if ((i & 1) != 0) {
            roomCollection = roomCollectionAndNft.collection;
        }
        if ((i & 2) != 0) {
            list = roomCollectionAndNft.nfts;
        }
        return roomCollectionAndNft.copy(roomCollection, list);
    }

    public final RoomCollection component1() {
        return this.collection;
    }

    public final List<RoomNFT> component2() {
        return this.nfts;
    }

    public final RoomCollectionAndNft copy(RoomCollection roomCollection, List<RoomNFT> list) {
        fs1.f(roomCollection, "collection");
        fs1.f(list, "nfts");
        return new RoomCollectionAndNft(roomCollection, list);
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj instanceof RoomCollectionAndNft) {
            RoomCollectionAndNft roomCollectionAndNft = (RoomCollectionAndNft) obj;
            return fs1.b(this.collection, roomCollectionAndNft.collection) && fs1.b(this.nfts, roomCollectionAndNft.nfts);
        }
        return false;
    }

    public final RoomCollection getCollection() {
        return this.collection;
    }

    public final List<RoomNFT> getNfts() {
        return this.nfts;
    }

    public final boolean getShowHideIcon() {
        return this.showHideIcon;
    }

    public int hashCode() {
        return (this.collection.hashCode() * 31) + this.nfts.hashCode();
    }

    public final void setShowHideIcon(boolean z) {
        this.showHideIcon = z;
    }

    public String toString() {
        return "RoomCollectionAndNft(collection=" + this.collection + ", nfts=" + this.nfts + ')';
    }

    public /* synthetic */ RoomCollectionAndNft(RoomCollection roomCollection, List list, int i, qi0 qi0Var) {
        this(roomCollection, (i & 2) != 0 ? b20.g() : list);
    }
}
