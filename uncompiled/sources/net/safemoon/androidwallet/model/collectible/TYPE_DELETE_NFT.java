package net.safemoon.androidwallet.model.collectible;

/* compiled from: TYPE_DELETE_NFT.kt */
/* loaded from: classes2.dex */
public enum TYPE_DELETE_NFT {
    VISIBLE(0),
    HIDE(1),
    DELETE_FOREVER(2);
    
    private final int value;

    TYPE_DELETE_NFT(int i) {
        this.value = i;
    }

    public final int getValue() {
        return this.value;
    }
}
