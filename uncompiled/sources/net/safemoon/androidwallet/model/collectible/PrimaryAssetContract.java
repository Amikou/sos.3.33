package net.safemoon.androidwallet.model.collectible;

/* compiled from: PrimaryAssetContract.kt */
/* loaded from: classes2.dex */
public final class PrimaryAssetContract {
    private String address;
    private String description;
    private String image_url;
    private String injectSlug;
    private String name;
    private String schema_name;
    private String symbol;

    public final String getAddress() {
        return this.address;
    }

    public final String getDescription() {
        return this.description;
    }

    public final String getImage_url() {
        return this.image_url;
    }

    public final String getInjectSlug() {
        return this.injectSlug;
    }

    public final String getName() {
        return this.name;
    }

    public final String getSchema_name() {
        return this.schema_name;
    }

    public final String getSymbol() {
        return this.symbol;
    }

    public final void setAddress(String str) {
        this.address = str;
    }

    public final void setDescription(String str) {
        this.description = str;
    }

    public final void setImage_url(String str) {
        this.image_url = str;
    }

    public final void setInjectSlug(String str) {
        this.injectSlug = str;
    }

    public final void setName(String str) {
        this.name = str;
    }

    public final void setSchema_name(String str) {
        this.schema_name = str;
    }

    public final void setSymbol(String str) {
        this.symbol = str;
    }
}
