package net.safemoon.androidwallet.model.collectible;

import java.util.List;

/* compiled from: Collectible.kt */
/* loaded from: classes2.dex */
public final class Collectible {
    private String banner_image_url;
    private String created_date;
    private String description;
    private String dev_buyer_fee_basis_points;
    private String dev_seller_fee_basis_points;
    private String external_url;
    private String image_url;
    private String name;
    private String payout_address;
    private List<PrimaryAssetContract> primary_asset_contracts;
    private String slug;

    public final String getBanner_image_url() {
        return this.banner_image_url;
    }

    public final String getCreated_date() {
        return this.created_date;
    }

    public final String getDescription() {
        return this.description;
    }

    public final String getDev_buyer_fee_basis_points() {
        return this.dev_buyer_fee_basis_points;
    }

    public final String getDev_seller_fee_basis_points() {
        return this.dev_seller_fee_basis_points;
    }

    public final String getExternal_url() {
        return this.external_url;
    }

    public final String getImage_url() {
        return this.image_url;
    }

    public final String getName() {
        return this.name;
    }

    public final String getPayout_address() {
        return this.payout_address;
    }

    public final List<PrimaryAssetContract> getPrimary_asset_contracts() {
        return this.primary_asset_contracts;
    }

    public final String getSlug() {
        return this.slug;
    }

    public final void setBanner_image_url(String str) {
        this.banner_image_url = str;
    }

    public final void setCreated_date(String str) {
        this.created_date = str;
    }

    public final void setDescription(String str) {
        this.description = str;
    }

    public final void setDev_buyer_fee_basis_points(String str) {
        this.dev_buyer_fee_basis_points = str;
    }

    public final void setDev_seller_fee_basis_points(String str) {
        this.dev_seller_fee_basis_points = str;
    }

    public final void setExternal_url(String str) {
        this.external_url = str;
    }

    public final void setImage_url(String str) {
        this.image_url = str;
    }

    public final void setName(String str) {
        this.name = str;
    }

    public final void setPayout_address(String str) {
        this.payout_address = str;
    }

    public final void setPrimary_asset_contracts(List<PrimaryAssetContract> list) {
        this.primary_asset_contracts = list;
    }

    public final void setSlug(String str) {
        this.slug = str;
    }
}
