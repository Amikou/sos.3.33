package net.safemoon.androidwallet.model.collectible;

import androidx.recyclerview.widget.RecyclerView;
import java.io.Serializable;

/* compiled from: RoomNFT.kt */
/* loaded from: classes2.dex */
public final class RoomNFT implements Serializable {
    private String animation_url;
    private String attributes;
    private final int chain;
    private final long collectionId;
    private String description;
    private final String fullData;
    private Long id;
    private String image_data;
    private String image_preview_url;
    private String name;
    private String openSeaUrl;
    private int order;
    private final String permalink;
    private final String token_id;
    private final String token_uri;
    private long updatedTime;

    public RoomNFT(Long l, int i, long j, String str, String str2, String str3, String str4, String str5, String str6, String str7, String str8, String str9, String str10, String str11, long j2, int i2) {
        this.id = l;
        this.chain = i;
        this.collectionId = j;
        this.token_id = str;
        this.name = str2;
        this.description = str3;
        this.animation_url = str4;
        this.image_preview_url = str5;
        this.image_data = str6;
        this.token_uri = str7;
        this.attributes = str8;
        this.fullData = str9;
        this.permalink = str10;
        this.openSeaUrl = str11;
        this.updatedTime = j2;
        this.order = i2;
    }

    public final Long component1() {
        return this.id;
    }

    public final String component10() {
        return this.token_uri;
    }

    public final String component11() {
        return this.attributes;
    }

    public final String component12() {
        return this.fullData;
    }

    public final String component13() {
        return this.permalink;
    }

    public final String component14() {
        return this.openSeaUrl;
    }

    public final long component15() {
        return this.updatedTime;
    }

    public final int component16() {
        return this.order;
    }

    public final int component2() {
        return this.chain;
    }

    public final long component3() {
        return this.collectionId;
    }

    public final String component4() {
        return this.token_id;
    }

    public final String component5() {
        return this.name;
    }

    public final String component6() {
        return this.description;
    }

    public final String component7() {
        return this.animation_url;
    }

    public final String component8() {
        return this.image_preview_url;
    }

    public final String component9() {
        return this.image_data;
    }

    public final RoomNFT copy(Long l, int i, long j, String str, String str2, String str3, String str4, String str5, String str6, String str7, String str8, String str9, String str10, String str11, long j2, int i2) {
        return new RoomNFT(l, i, j, str, str2, str3, str4, str5, str6, str7, str8, str9, str10, str11, j2, i2);
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj instanceof RoomNFT) {
            RoomNFT roomNFT = (RoomNFT) obj;
            return fs1.b(this.id, roomNFT.id) && this.chain == roomNFT.chain && this.collectionId == roomNFT.collectionId && fs1.b(this.token_id, roomNFT.token_id) && fs1.b(this.name, roomNFT.name) && fs1.b(this.description, roomNFT.description) && fs1.b(this.animation_url, roomNFT.animation_url) && fs1.b(this.image_preview_url, roomNFT.image_preview_url) && fs1.b(this.image_data, roomNFT.image_data) && fs1.b(this.token_uri, roomNFT.token_uri) && fs1.b(this.attributes, roomNFT.attributes) && fs1.b(this.fullData, roomNFT.fullData) && fs1.b(this.permalink, roomNFT.permalink) && fs1.b(this.openSeaUrl, roomNFT.openSeaUrl) && this.updatedTime == roomNFT.updatedTime && this.order == roomNFT.order;
        }
        return false;
    }

    public final String getAnimation_url() {
        return this.animation_url;
    }

    public final String getAttributes() {
        return this.attributes;
    }

    public final int getChain() {
        return this.chain;
    }

    public final long getCollectionId() {
        return this.collectionId;
    }

    public final String getDescription() {
        return this.description;
    }

    public final String getFullData() {
        return this.fullData;
    }

    public final Long getId() {
        return this.id;
    }

    public final String getImage_data() {
        return this.image_data;
    }

    public final String getImage_preview_url() {
        return this.image_preview_url;
    }

    public final String getName() {
        return this.name;
    }

    public final String getOpenSeaUrl() {
        return this.openSeaUrl;
    }

    public final int getOrder() {
        return this.order;
    }

    public final String getPermalink() {
        return this.permalink;
    }

    public final String getToken_id() {
        return this.token_id;
    }

    public final String getToken_uri() {
        return this.token_uri;
    }

    public final long getUpdatedTime() {
        return this.updatedTime;
    }

    public int hashCode() {
        Long l = this.id;
        int hashCode = (((((l == null ? 0 : l.hashCode()) * 31) + this.chain) * 31) + p80.a(this.collectionId)) * 31;
        String str = this.token_id;
        int hashCode2 = (hashCode + (str == null ? 0 : str.hashCode())) * 31;
        String str2 = this.name;
        int hashCode3 = (hashCode2 + (str2 == null ? 0 : str2.hashCode())) * 31;
        String str3 = this.description;
        int hashCode4 = (hashCode3 + (str3 == null ? 0 : str3.hashCode())) * 31;
        String str4 = this.animation_url;
        int hashCode5 = (hashCode4 + (str4 == null ? 0 : str4.hashCode())) * 31;
        String str5 = this.image_preview_url;
        int hashCode6 = (hashCode5 + (str5 == null ? 0 : str5.hashCode())) * 31;
        String str6 = this.image_data;
        int hashCode7 = (hashCode6 + (str6 == null ? 0 : str6.hashCode())) * 31;
        String str7 = this.token_uri;
        int hashCode8 = (hashCode7 + (str7 == null ? 0 : str7.hashCode())) * 31;
        String str8 = this.attributes;
        int hashCode9 = (hashCode8 + (str8 == null ? 0 : str8.hashCode())) * 31;
        String str9 = this.fullData;
        int hashCode10 = (hashCode9 + (str9 == null ? 0 : str9.hashCode())) * 31;
        String str10 = this.permalink;
        int hashCode11 = (hashCode10 + (str10 == null ? 0 : str10.hashCode())) * 31;
        String str11 = this.openSeaUrl;
        return ((((hashCode11 + (str11 != null ? str11.hashCode() : 0)) * 31) + p80.a(this.updatedTime)) * 31) + this.order;
    }

    public final void setAnimation_url(String str) {
        this.animation_url = str;
    }

    public final void setAttributes(String str) {
        this.attributes = str;
    }

    public final void setDescription(String str) {
        this.description = str;
    }

    public final void setId(Long l) {
        this.id = l;
    }

    public final void setImage_data(String str) {
        this.image_data = str;
    }

    public final void setImage_preview_url(String str) {
        this.image_preview_url = str;
    }

    public final void setName(String str) {
        this.name = str;
    }

    public final void setOpenSeaUrl(String str) {
        this.openSeaUrl = str;
    }

    public final void setOrder(int i) {
        this.order = i;
    }

    public final void setUpdatedTime(long j) {
        this.updatedTime = j;
    }

    public String toString() {
        return "RoomNFT(id=" + this.id + ", chain=" + this.chain + ", collectionId=" + this.collectionId + ", token_id=" + ((Object) this.token_id) + ", name=" + ((Object) this.name) + ", description=" + ((Object) this.description) + ", animation_url=" + ((Object) this.animation_url) + ", image_preview_url=" + ((Object) this.image_preview_url) + ", image_data=" + ((Object) this.image_data) + ", token_uri=" + ((Object) this.token_uri) + ", attributes=" + ((Object) this.attributes) + ", fullData=" + ((Object) this.fullData) + ", permalink=" + ((Object) this.permalink) + ", openSeaUrl=" + ((Object) this.openSeaUrl) + ", updatedTime=" + this.updatedTime + ", order=" + this.order + ')';
    }

    public /* synthetic */ RoomNFT(Long l, int i, long j, String str, String str2, String str3, String str4, String str5, String str6, String str7, String str8, String str9, String str10, String str11, long j2, int i2, int i3, qi0 qi0Var) {
        this((i3 & 1) != 0 ? null : l, i, j, (i3 & 8) != 0 ? null : str, (i3 & 16) != 0 ? null : str2, (i3 & 32) != 0 ? null : str3, (i3 & 64) != 0 ? null : str4, (i3 & 128) != 0 ? null : str5, (i3 & 256) != 0 ? null : str6, (i3 & RecyclerView.a0.FLAG_ADAPTER_POSITION_UNKNOWN) != 0 ? null : str7, (i3 & RecyclerView.a0.FLAG_ADAPTER_FULLUPDATE) != 0 ? null : str8, (i3 & 2048) != 0 ? null : str9, (i3 & 4096) != 0 ? null : str10, (i3 & 8192) != 0 ? null : str11, j2, (i3 & 32768) != 0 ? 0 : i2);
    }
}
