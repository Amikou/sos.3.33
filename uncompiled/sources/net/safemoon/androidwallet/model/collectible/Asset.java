package net.safemoon.androidwallet.model.collectible;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import java.util.List;

/* compiled from: Asset.kt */
/* loaded from: classes2.dex */
public final class Asset {
    public static final Companion Companion = new Companion(null);
    private AssetContract asset_contract;
    private Collectible collection;
    private String description;
    private String externalLink;
    private String external_link;
    private Integer id;
    private String image_original_url;
    private String image_preview_url;
    private String image_thumbnail_url;
    private String image_url;
    private String name;
    private String permalink;
    private String token_id;
    private List<AssetTrait> traits = b20.g();

    /* compiled from: Asset.kt */
    /* loaded from: classes2.dex */
    public static final class Companion {
        private Companion() {
        }

        public /* synthetic */ Companion(qi0 qi0Var) {
            this();
        }

        public final Asset fromString(String str) {
            try {
                return (Asset) new Gson().fromJson(str, (Class<Object>) Asset.class);
            } catch (Exception unused) {
                return null;
            }
        }
    }

    public final AssetContract getAsset_contract() {
        return this.asset_contract;
    }

    public final Collectible getCollection() {
        return this.collection;
    }

    public final String getDescription() {
        return this.description;
    }

    public final String getExternalLink() {
        return this.externalLink;
    }

    public final String getExternal_link() {
        return this.external_link;
    }

    public final Integer getId() {
        return this.id;
    }

    public final String getImage_original_url() {
        return this.image_original_url;
    }

    public final String getImage_preview_url() {
        return this.image_preview_url;
    }

    public final String getImage_thumbnail_url() {
        return this.image_thumbnail_url;
    }

    public final String getImage_url() {
        return this.image_url;
    }

    public final String getName() {
        return this.name;
    }

    public final String getPermalink() {
        return this.permalink;
    }

    public final String getToken_id() {
        return this.token_id;
    }

    public final List<AssetTrait> getTraits() {
        return this.traits;
    }

    public final void setAsset_contract(AssetContract assetContract) {
        this.asset_contract = assetContract;
    }

    public final void setCollection(Collectible collectible) {
        this.collection = collectible;
    }

    public final void setDescription(String str) {
        this.description = str;
    }

    public final void setExternalLink(String str) {
        this.externalLink = str;
    }

    public final void setExternal_link(String str) {
        this.external_link = str;
    }

    public final void setId(Integer num) {
        this.id = num;
    }

    public final void setImage_original_url(String str) {
        this.image_original_url = str;
    }

    public final void setImage_preview_url(String str) {
        this.image_preview_url = str;
    }

    public final void setImage_thumbnail_url(String str) {
        this.image_thumbnail_url = str;
    }

    public final void setImage_url(String str) {
        this.image_url = str;
    }

    public final void setName(String str) {
        this.name = str;
    }

    public final void setPermalink(String str) {
        this.permalink = str;
    }

    public final void setToken_id(String str) {
        this.token_id = str;
    }

    public final void setTraits(List<AssetTrait> list) {
        fs1.f(list, "<set-?>");
        this.traits = list;
    }

    public String toString() {
        String json = new Gson().toJson(this, Asset.class);
        fs1.e(json, "Gson().toJson(this, Asset::class.java)");
        return json;
    }

    public final String traitsInString() {
        String json = new Gson().toJson(this.traits, new TypeToken<List<? extends AssetTrait>>() { // from class: net.safemoon.androidwallet.model.collectible.Asset$traitsInString$type$1
        }.getType());
        fs1.e(json, "Gson().toJson(traits, type)");
        return json;
    }
}
