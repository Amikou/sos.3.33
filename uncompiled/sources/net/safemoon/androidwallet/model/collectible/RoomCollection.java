package net.safemoon.androidwallet.model.collectible;

import androidx.recyclerview.widget.RecyclerView;
import java.io.Serializable;

/* compiled from: RoomCollection.kt */
/* loaded from: classes2.dex */
public final class RoomCollection implements Serializable {
    private final int chain;
    private final String contractAddress;
    private final String contract_type;
    private final String description;
    private Long id;
    private String imageUrl;
    private final String marketPlace;
    private final String name;
    private int order;
    private String slug;
    private final String symbol;
    private int typeDeleteNft;
    private final long updatedTime;

    public RoomCollection(Long l, int i, String str, String str2, String str3, String str4, String str5, String str6, String str7, String str8, long j, int i2, int i3) {
        fs1.f(str, "contractAddress");
        fs1.f(str2, "contract_type");
        this.id = l;
        this.chain = i;
        this.contractAddress = str;
        this.contract_type = str2;
        this.name = str3;
        this.description = str4;
        this.imageUrl = str5;
        this.symbol = str6;
        this.marketPlace = str7;
        this.slug = str8;
        this.updatedTime = j;
        this.order = i2;
        this.typeDeleteNft = i3;
    }

    public final Long component1() {
        return this.id;
    }

    public final String component10() {
        return this.slug;
    }

    public final long component11() {
        return this.updatedTime;
    }

    public final int component12() {
        return this.order;
    }

    public final int component13() {
        return this.typeDeleteNft;
    }

    public final int component2() {
        return this.chain;
    }

    public final String component3() {
        return this.contractAddress;
    }

    public final String component4() {
        return this.contract_type;
    }

    public final String component5() {
        return this.name;
    }

    public final String component6() {
        return this.description;
    }

    public final String component7() {
        return this.imageUrl;
    }

    public final String component8() {
        return this.symbol;
    }

    public final String component9() {
        return this.marketPlace;
    }

    public final RoomCollection copy(Long l, int i, String str, String str2, String str3, String str4, String str5, String str6, String str7, String str8, long j, int i2, int i3) {
        fs1.f(str, "contractAddress");
        fs1.f(str2, "contract_type");
        return new RoomCollection(l, i, str, str2, str3, str4, str5, str6, str7, str8, j, i2, i3);
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj instanceof RoomCollection) {
            RoomCollection roomCollection = (RoomCollection) obj;
            return fs1.b(this.id, roomCollection.id) && this.chain == roomCollection.chain && fs1.b(this.contractAddress, roomCollection.contractAddress) && fs1.b(this.contract_type, roomCollection.contract_type) && fs1.b(this.name, roomCollection.name) && fs1.b(this.description, roomCollection.description) && fs1.b(this.imageUrl, roomCollection.imageUrl) && fs1.b(this.symbol, roomCollection.symbol) && fs1.b(this.marketPlace, roomCollection.marketPlace) && fs1.b(this.slug, roomCollection.slug) && this.updatedTime == roomCollection.updatedTime && this.order == roomCollection.order && this.typeDeleteNft == roomCollection.typeDeleteNft;
        }
        return false;
    }

    public final int getChain() {
        return this.chain;
    }

    public final String getContractAddress() {
        return this.contractAddress;
    }

    public final String getContract_type() {
        return this.contract_type;
    }

    public final String getDescription() {
        return this.description;
    }

    public final Long getId() {
        return this.id;
    }

    public final String getImageUrl() {
        return this.imageUrl;
    }

    public final String getMarketPlace() {
        return this.marketPlace;
    }

    public final String getName() {
        return this.name;
    }

    public final int getOrder() {
        return this.order;
    }

    public final String getSlug() {
        return this.slug;
    }

    public final String getSymbol() {
        return this.symbol;
    }

    public final int getTypeDeleteNft() {
        return this.typeDeleteNft;
    }

    public final long getUpdatedTime() {
        return this.updatedTime;
    }

    public int hashCode() {
        Long l = this.id;
        int hashCode = (((((((l == null ? 0 : l.hashCode()) * 31) + this.chain) * 31) + this.contractAddress.hashCode()) * 31) + this.contract_type.hashCode()) * 31;
        String str = this.name;
        int hashCode2 = (hashCode + (str == null ? 0 : str.hashCode())) * 31;
        String str2 = this.description;
        int hashCode3 = (hashCode2 + (str2 == null ? 0 : str2.hashCode())) * 31;
        String str3 = this.imageUrl;
        int hashCode4 = (hashCode3 + (str3 == null ? 0 : str3.hashCode())) * 31;
        String str4 = this.symbol;
        int hashCode5 = (hashCode4 + (str4 == null ? 0 : str4.hashCode())) * 31;
        String str5 = this.marketPlace;
        int hashCode6 = (hashCode5 + (str5 == null ? 0 : str5.hashCode())) * 31;
        String str6 = this.slug;
        return ((((((hashCode6 + (str6 != null ? str6.hashCode() : 0)) * 31) + p80.a(this.updatedTime)) * 31) + this.order) * 31) + this.typeDeleteNft;
    }

    public final void setId(Long l) {
        this.id = l;
    }

    public final void setImageUrl(String str) {
        this.imageUrl = str;
    }

    public final void setOrder(int i) {
        this.order = i;
    }

    public final void setSlug(String str) {
        this.slug = str;
    }

    public final void setTypeDeleteNft(int i) {
        this.typeDeleteNft = i;
    }

    public String toString() {
        return "RoomCollection(id=" + this.id + ", chain=" + this.chain + ", contractAddress=" + this.contractAddress + ", contract_type=" + this.contract_type + ", name=" + ((Object) this.name) + ", description=" + ((Object) this.description) + ", imageUrl=" + ((Object) this.imageUrl) + ", symbol=" + ((Object) this.symbol) + ", marketPlace=" + ((Object) this.marketPlace) + ", slug=" + ((Object) this.slug) + ", updatedTime=" + this.updatedTime + ", order=" + this.order + ", typeDeleteNft=" + this.typeDeleteNft + ')';
    }

    public /* synthetic */ RoomCollection(Long l, int i, String str, String str2, String str3, String str4, String str5, String str6, String str7, String str8, long j, int i2, int i3, int i4, qi0 qi0Var) {
        this((i4 & 1) != 0 ? null : l, i, str, str2, (i4 & 16) != 0 ? null : str3, (i4 & 32) != 0 ? null : str4, (i4 & 64) != 0 ? null : str5, (i4 & 128) != 0 ? null : str6, (i4 & 256) != 0 ? null : str7, (i4 & RecyclerView.a0.FLAG_ADAPTER_POSITION_UNKNOWN) != 0 ? null : str8, j, (i4 & 2048) != 0 ? 0 : i2, (i4 & 4096) != 0 ? 0 : i3);
    }
}
