package net.safemoon.androidwallet.model.collectible;

import java.io.Serializable;

/* compiled from: AssetContract.kt */
/* loaded from: classes2.dex */
public final class AssetContract implements Serializable {
    private String address;
    private String asset_contract_type;
    private Integer dev_buyer_fee_basis_points;
    private Integer dev_seller_fee_basis_points;
    private String name;
    private String nft_version;
    private Integer opensea_buyer_fee_basis_points;
    private String opensea_version;
    private String owner;
    private String payout_address;
    private String schema_name;
    private String symbol;
    private String total_supply;

    public final String getAddress() {
        return this.address;
    }

    public final String getAsset_contract_type() {
        return this.asset_contract_type;
    }

    public final Integer getDev_buyer_fee_basis_points() {
        return this.dev_buyer_fee_basis_points;
    }

    public final Integer getDev_seller_fee_basis_points() {
        return this.dev_seller_fee_basis_points;
    }

    public final String getName() {
        return this.name;
    }

    public final String getNft_version() {
        return this.nft_version;
    }

    public final Integer getOpensea_buyer_fee_basis_points() {
        return this.opensea_buyer_fee_basis_points;
    }

    public final String getOpensea_version() {
        return this.opensea_version;
    }

    public final String getOwner() {
        return this.owner;
    }

    public final String getPayout_address() {
        return this.payout_address;
    }

    public final String getSchema_name() {
        return this.schema_name;
    }

    public final String getSymbol() {
        return this.symbol;
    }

    public final String getTotal_supply() {
        return this.total_supply;
    }

    public final void setAddress(String str) {
        this.address = str;
    }

    public final void setAsset_contract_type(String str) {
        this.asset_contract_type = str;
    }

    public final void setDev_buyer_fee_basis_points(Integer num) {
        this.dev_buyer_fee_basis_points = num;
    }

    public final void setDev_seller_fee_basis_points(Integer num) {
        this.dev_seller_fee_basis_points = num;
    }

    public final void setName(String str) {
        this.name = str;
    }

    public final void setNft_version(String str) {
        this.nft_version = str;
    }

    public final void setOpensea_buyer_fee_basis_points(Integer num) {
        this.opensea_buyer_fee_basis_points = num;
    }

    public final void setOpensea_version(String str) {
        this.opensea_version = str;
    }

    public final void setOwner(String str) {
        this.owner = str;
    }

    public final void setPayout_address(String str) {
        this.payout_address = str;
    }

    public final void setSchema_name(String str) {
        this.schema_name = str;
    }

    public final void setSymbol(String str) {
        this.symbol = str;
    }

    public final void setTotal_supply(String str) {
        this.total_supply = str;
    }
}
