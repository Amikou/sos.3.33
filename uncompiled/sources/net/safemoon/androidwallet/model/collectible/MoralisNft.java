package net.safemoon.androidwallet.model.collectible;

import androidx.recyclerview.widget.RecyclerView;
import com.google.gson.Gson;
import com.google.gson.annotations.SerializedName;
import okhttp3.internal.http2.Http2;
import org.web3j.ens.contracts.generated.PublicResolver;

/* compiled from: MoralisNft.kt */
/* loaded from: classes2.dex */
public final class MoralisNft {
    @SerializedName("amount")
    private String amount;
    @SerializedName("block_number")
    private String blockNumber;
    @SerializedName("block_number_minted")
    private String blockNumberMinted;
    @SerializedName("contract_type")
    private String contractType;
    @SerializedName("last_metadata_sync")
    private String lastMetadataSync;
    @SerializedName("last_token_uri_sync")
    private String lastTokenUriSync;
    @SerializedName("metadata")
    private String metadata;
    @SerializedName(PublicResolver.FUNC_NAME)
    private String name;
    @SerializedName("owner_of")
    private String ownerOf;
    @SerializedName("symbol")
    private String symbol;
    @SerializedName("synced_at")
    private String syncedAt;
    @SerializedName("token_address")
    private String tokenAddress;
    @SerializedName("token_hash")
    private String tokenHash;
    @SerializedName("token_id")
    private String tokenId;
    @SerializedName("token_uri")
    private String tokenUri;

    public MoralisNft() {
        this(null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, 32767, null);
    }

    public MoralisNft(String str, String str2, String str3, String str4, String str5, String str6, String str7, String str8, String str9, String str10, String str11, String str12, String str13, String str14, String str15) {
        this.tokenAddress = str;
        this.tokenId = str2;
        this.ownerOf = str3;
        this.blockNumber = str4;
        this.blockNumberMinted = str5;
        this.tokenHash = str6;
        this.amount = str7;
        this.contractType = str8;
        this.name = str9;
        this.symbol = str10;
        this.tokenUri = str11;
        this.metadata = str12;
        this.syncedAt = str13;
        this.lastTokenUriSync = str14;
        this.lastMetadataSync = str15;
    }

    public final String component1() {
        return this.tokenAddress;
    }

    public final String component10() {
        return this.symbol;
    }

    public final String component11() {
        return this.tokenUri;
    }

    public final String component12() {
        return this.metadata;
    }

    public final String component13() {
        return this.syncedAt;
    }

    public final String component14() {
        return this.lastTokenUriSync;
    }

    public final String component15() {
        return this.lastMetadataSync;
    }

    public final String component2() {
        return this.tokenId;
    }

    public final String component3() {
        return this.ownerOf;
    }

    public final String component4() {
        return this.blockNumber;
    }

    public final String component5() {
        return this.blockNumberMinted;
    }

    public final String component6() {
        return this.tokenHash;
    }

    public final String component7() {
        return this.amount;
    }

    public final String component8() {
        return this.contractType;
    }

    public final String component9() {
        return this.name;
    }

    public final MoralisNft copy(String str, String str2, String str3, String str4, String str5, String str6, String str7, String str8, String str9, String str10, String str11, String str12, String str13, String str14, String str15) {
        return new MoralisNft(str, str2, str3, str4, str5, str6, str7, str8, str9, str10, str11, str12, str13, str14, str15);
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj instanceof MoralisNft) {
            MoralisNft moralisNft = (MoralisNft) obj;
            return fs1.b(this.tokenAddress, moralisNft.tokenAddress) && fs1.b(this.tokenId, moralisNft.tokenId) && fs1.b(this.ownerOf, moralisNft.ownerOf) && fs1.b(this.blockNumber, moralisNft.blockNumber) && fs1.b(this.blockNumberMinted, moralisNft.blockNumberMinted) && fs1.b(this.tokenHash, moralisNft.tokenHash) && fs1.b(this.amount, moralisNft.amount) && fs1.b(this.contractType, moralisNft.contractType) && fs1.b(this.name, moralisNft.name) && fs1.b(this.symbol, moralisNft.symbol) && fs1.b(this.tokenUri, moralisNft.tokenUri) && fs1.b(this.metadata, moralisNft.metadata) && fs1.b(this.syncedAt, moralisNft.syncedAt) && fs1.b(this.lastTokenUriSync, moralisNft.lastTokenUriSync) && fs1.b(this.lastMetadataSync, moralisNft.lastMetadataSync);
        }
        return false;
    }

    public final String getAmount() {
        return this.amount;
    }

    public final String getBlockNumber() {
        return this.blockNumber;
    }

    public final String getBlockNumberMinted() {
        return this.blockNumberMinted;
    }

    public final String getContractType() {
        return this.contractType;
    }

    public final String getLastMetadataSync() {
        return this.lastMetadataSync;
    }

    public final String getLastTokenUriSync() {
        return this.lastTokenUriSync;
    }

    public final String getMetadata() {
        return this.metadata;
    }

    public final String getName() {
        return this.name;
    }

    public final String getOwnerOf() {
        return this.ownerOf;
    }

    public final String getSymbol() {
        return this.symbol;
    }

    public final String getSyncedAt() {
        return this.syncedAt;
    }

    public final String getTokenAddress() {
        return this.tokenAddress;
    }

    public final String getTokenHash() {
        return this.tokenHash;
    }

    public final String getTokenId() {
        return this.tokenId;
    }

    public final String getTokenUri() {
        return this.tokenUri;
    }

    public int hashCode() {
        String str = this.tokenAddress;
        int hashCode = (str == null ? 0 : str.hashCode()) * 31;
        String str2 = this.tokenId;
        int hashCode2 = (hashCode + (str2 == null ? 0 : str2.hashCode())) * 31;
        String str3 = this.ownerOf;
        int hashCode3 = (hashCode2 + (str3 == null ? 0 : str3.hashCode())) * 31;
        String str4 = this.blockNumber;
        int hashCode4 = (hashCode3 + (str4 == null ? 0 : str4.hashCode())) * 31;
        String str5 = this.blockNumberMinted;
        int hashCode5 = (hashCode4 + (str5 == null ? 0 : str5.hashCode())) * 31;
        String str6 = this.tokenHash;
        int hashCode6 = (hashCode5 + (str6 == null ? 0 : str6.hashCode())) * 31;
        String str7 = this.amount;
        int hashCode7 = (hashCode6 + (str7 == null ? 0 : str7.hashCode())) * 31;
        String str8 = this.contractType;
        int hashCode8 = (hashCode7 + (str8 == null ? 0 : str8.hashCode())) * 31;
        String str9 = this.name;
        int hashCode9 = (hashCode8 + (str9 == null ? 0 : str9.hashCode())) * 31;
        String str10 = this.symbol;
        int hashCode10 = (hashCode9 + (str10 == null ? 0 : str10.hashCode())) * 31;
        String str11 = this.tokenUri;
        int hashCode11 = (hashCode10 + (str11 == null ? 0 : str11.hashCode())) * 31;
        String str12 = this.metadata;
        int hashCode12 = (hashCode11 + (str12 == null ? 0 : str12.hashCode())) * 31;
        String str13 = this.syncedAt;
        int hashCode13 = (hashCode12 + (str13 == null ? 0 : str13.hashCode())) * 31;
        String str14 = this.lastTokenUriSync;
        int hashCode14 = (hashCode13 + (str14 == null ? 0 : str14.hashCode())) * 31;
        String str15 = this.lastMetadataSync;
        return hashCode14 + (str15 != null ? str15.hashCode() : 0);
    }

    public final void setAmount(String str) {
        this.amount = str;
    }

    public final void setBlockNumber(String str) {
        this.blockNumber = str;
    }

    public final void setBlockNumberMinted(String str) {
        this.blockNumberMinted = str;
    }

    public final void setContractType(String str) {
        this.contractType = str;
    }

    public final void setLastMetadataSync(String str) {
        this.lastMetadataSync = str;
    }

    public final void setLastTokenUriSync(String str) {
        this.lastTokenUriSync = str;
    }

    public final void setMetadata(String str) {
        this.metadata = str;
    }

    public final void setName(String str) {
        this.name = str;
    }

    public final void setOwnerOf(String str) {
        this.ownerOf = str;
    }

    public final void setSymbol(String str) {
        this.symbol = str;
    }

    public final void setSyncedAt(String str) {
        this.syncedAt = str;
    }

    public final void setTokenAddress(String str) {
        this.tokenAddress = str;
    }

    public final void setTokenHash(String str) {
        this.tokenHash = str;
    }

    public final void setTokenId(String str) {
        this.tokenId = str;
    }

    public final void setTokenUri(String str) {
        this.tokenUri = str;
    }

    public String toString() {
        String json = new Gson().toJson(this, MoralisNft.class);
        fs1.e(json, "Gson().toJson(this, MoralisNft::class.java)");
        return json;
    }

    public /* synthetic */ MoralisNft(String str, String str2, String str3, String str4, String str5, String str6, String str7, String str8, String str9, String str10, String str11, String str12, String str13, String str14, String str15, int i, qi0 qi0Var) {
        this((i & 1) != 0 ? null : str, (i & 2) != 0 ? null : str2, (i & 4) != 0 ? null : str3, (i & 8) != 0 ? null : str4, (i & 16) != 0 ? null : str5, (i & 32) != 0 ? null : str6, (i & 64) != 0 ? null : str7, (i & 128) != 0 ? null : str8, (i & 256) != 0 ? null : str9, (i & RecyclerView.a0.FLAG_ADAPTER_POSITION_UNKNOWN) != 0 ? null : str10, (i & RecyclerView.a0.FLAG_ADAPTER_FULLUPDATE) != 0 ? null : str11, (i & 2048) != 0 ? null : str12, (i & 4096) != 0 ? null : str13, (i & 8192) != 0 ? null : str14, (i & Http2.INITIAL_MAX_FRAME_SIZE) == 0 ? str15 : null);
    }
}
