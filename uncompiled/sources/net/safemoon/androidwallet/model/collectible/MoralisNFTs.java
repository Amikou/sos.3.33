package net.safemoon.androidwallet.model.collectible;

import com.google.gson.annotations.SerializedName;
import java.util.ArrayList;

/* compiled from: MoralisNFTs.kt */
/* loaded from: classes2.dex */
public final class MoralisNFTs {
    @SerializedName("cursor")
    private String cursor;
    @SerializedName("page")
    private Integer page;
    @SerializedName("page_size")
    private Integer pageSize;
    @SerializedName("result")
    private ArrayList<MoralisNft> result;
    @SerializedName("status")
    private String status;
    @SerializedName("total")
    private Integer total;

    public MoralisNFTs() {
        this(null, null, null, null, null, null, 63, null);
    }

    public MoralisNFTs(Integer num, Integer num2, Integer num3, String str, ArrayList<MoralisNft> arrayList, String str2) {
        fs1.f(arrayList, "result");
        this.total = num;
        this.page = num2;
        this.pageSize = num3;
        this.cursor = str;
        this.result = arrayList;
        this.status = str2;
    }

    public static /* synthetic */ MoralisNFTs copy$default(MoralisNFTs moralisNFTs, Integer num, Integer num2, Integer num3, String str, ArrayList arrayList, String str2, int i, Object obj) {
        if ((i & 1) != 0) {
            num = moralisNFTs.total;
        }
        if ((i & 2) != 0) {
            num2 = moralisNFTs.page;
        }
        Integer num4 = num2;
        if ((i & 4) != 0) {
            num3 = moralisNFTs.pageSize;
        }
        Integer num5 = num3;
        if ((i & 8) != 0) {
            str = moralisNFTs.cursor;
        }
        String str3 = str;
        ArrayList<MoralisNft> arrayList2 = arrayList;
        if ((i & 16) != 0) {
            arrayList2 = moralisNFTs.result;
        }
        ArrayList arrayList3 = arrayList2;
        if ((i & 32) != 0) {
            str2 = moralisNFTs.status;
        }
        return moralisNFTs.copy(num, num4, num5, str3, arrayList3, str2);
    }

    public final Integer component1() {
        return this.total;
    }

    public final Integer component2() {
        return this.page;
    }

    public final Integer component3() {
        return this.pageSize;
    }

    public final String component4() {
        return this.cursor;
    }

    public final ArrayList<MoralisNft> component5() {
        return this.result;
    }

    public final String component6() {
        return this.status;
    }

    public final MoralisNFTs copy(Integer num, Integer num2, Integer num3, String str, ArrayList<MoralisNft> arrayList, String str2) {
        fs1.f(arrayList, "result");
        return new MoralisNFTs(num, num2, num3, str, arrayList, str2);
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj instanceof MoralisNFTs) {
            MoralisNFTs moralisNFTs = (MoralisNFTs) obj;
            return fs1.b(this.total, moralisNFTs.total) && fs1.b(this.page, moralisNFTs.page) && fs1.b(this.pageSize, moralisNFTs.pageSize) && fs1.b(this.cursor, moralisNFTs.cursor) && fs1.b(this.result, moralisNFTs.result) && fs1.b(this.status, moralisNFTs.status);
        }
        return false;
    }

    public final String getCursor() {
        return this.cursor;
    }

    public final Integer getPage() {
        return this.page;
    }

    public final Integer getPageSize() {
        return this.pageSize;
    }

    public final ArrayList<MoralisNft> getResult() {
        return this.result;
    }

    public final String getStatus() {
        return this.status;
    }

    public final Integer getTotal() {
        return this.total;
    }

    public int hashCode() {
        Integer num = this.total;
        int hashCode = (num == null ? 0 : num.hashCode()) * 31;
        Integer num2 = this.page;
        int hashCode2 = (hashCode + (num2 == null ? 0 : num2.hashCode())) * 31;
        Integer num3 = this.pageSize;
        int hashCode3 = (hashCode2 + (num3 == null ? 0 : num3.hashCode())) * 31;
        String str = this.cursor;
        int hashCode4 = (((hashCode3 + (str == null ? 0 : str.hashCode())) * 31) + this.result.hashCode()) * 31;
        String str2 = this.status;
        return hashCode4 + (str2 != null ? str2.hashCode() : 0);
    }

    public final void setCursor(String str) {
        this.cursor = str;
    }

    public final void setPage(Integer num) {
        this.page = num;
    }

    public final void setPageSize(Integer num) {
        this.pageSize = num;
    }

    public final void setResult(ArrayList<MoralisNft> arrayList) {
        fs1.f(arrayList, "<set-?>");
        this.result = arrayList;
    }

    public final void setStatus(String str) {
        this.status = str;
    }

    public final void setTotal(Integer num) {
        this.total = num;
    }

    public String toString() {
        return "MoralisNFTs(total=" + this.total + ", page=" + this.page + ", pageSize=" + this.pageSize + ", cursor=" + ((Object) this.cursor) + ", result=" + this.result + ", status=" + ((Object) this.status) + ')';
    }

    public /* synthetic */ MoralisNFTs(Integer num, Integer num2, Integer num3, String str, ArrayList arrayList, String str2, int i, qi0 qi0Var) {
        this((i & 1) != 0 ? null : num, (i & 2) != 0 ? null : num2, (i & 4) != 0 ? null : num3, (i & 8) != 0 ? null : str, (i & 16) != 0 ? new ArrayList() : arrayList, (i & 32) != 0 ? null : str2);
    }
}
