package net.safemoon.androidwallet.model.collectible;

import java.util.List;

/* compiled from: Assets.kt */
/* loaded from: classes2.dex */
public final class Assets {
    private List<Asset> assets;

    public final List<Asset> getAssets() {
        return this.assets;
    }

    public final void setAssets(List<Asset> list) {
        this.assets = list;
    }
}
