package net.safemoon.androidwallet.model.collectible;

import com.google.gson.Gson;

/* compiled from: Asset.kt */
/* loaded from: classes2.dex */
public final class AssetTrait {
    private String display_type;
    private String max_value;
    private Integer trait_count;
    private String trait_type;
    private Object value;

    public final String getDisplay_type() {
        return this.display_type;
    }

    public final String getMax_value() {
        return this.max_value;
    }

    public final Integer getTrait_count() {
        return this.trait_count;
    }

    public final String getTrait_type() {
        return this.trait_type;
    }

    public final Object getValue() {
        return this.value;
    }

    public final void setDisplay_type(String str) {
        this.display_type = str;
    }

    public final void setMax_value(String str) {
        this.max_value = str;
    }

    public final void setTrait_count(Integer num) {
        this.trait_count = num;
    }

    public final void setTrait_type(String str) {
        this.trait_type = str;
    }

    public final void setValue(Object obj) {
        this.value = obj;
    }

    public String toString() {
        String json = new Gson().toJson(this, AssetTrait.class);
        fs1.e(json, "Gson().toJson(this, AssetTrait::class.java)");
        return json;
    }
}
