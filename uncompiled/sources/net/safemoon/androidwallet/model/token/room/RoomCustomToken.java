package net.safemoon.androidwallet.model.token.room;

import net.safemoon.androidwallet.model.token.abstraction.IToken;
import org.web3j.ens.contracts.generated.PublicResolver;

/* compiled from: RoomCustomToken.kt */
/* loaded from: classes2.dex */
public final class RoomCustomToken implements IToken {
    private final boolean allowSwap;
    private final int chainId;
    private final String contractAddress;
    private final int decimals;
    private final String iconResName;
    private final String name;
    private final double nativeBalance;
    private final double percentChange1h;
    private final double priceInUsdt;
    private final String symbol;
    private final String symbolWithType;

    public RoomCustomToken(String str, String str2, String str3, String str4, String str5, int i, int i2, boolean z, double d, double d2, double d3) {
        fs1.f(str, "symbolWithType");
        fs1.f(str2, "symbol");
        fs1.f(str3, PublicResolver.FUNC_NAME);
        fs1.f(str4, "iconResName");
        fs1.f(str5, "contractAddress");
        this.symbolWithType = str;
        this.symbol = str2;
        this.name = str3;
        this.iconResName = str4;
        this.contractAddress = str5;
        this.chainId = i;
        this.decimals = i2;
        this.allowSwap = z;
        this.nativeBalance = d;
        this.priceInUsdt = d2;
        this.percentChange1h = d3;
    }

    @Override // net.safemoon.androidwallet.model.token.abstraction.IToken
    public boolean getAllowSwap() {
        return this.allowSwap;
    }

    @Override // net.safemoon.androidwallet.model.token.abstraction.IToken
    public int getChainId() {
        return this.chainId;
    }

    @Override // net.safemoon.androidwallet.model.token.abstraction.IToken
    public String getContractAddress() {
        return this.contractAddress;
    }

    @Override // net.safemoon.androidwallet.model.token.abstraction.IToken
    public int getDecimals() {
        return this.decimals;
    }

    @Override // net.safemoon.androidwallet.model.token.abstraction.IToken
    public String getIconResName() {
        return this.iconResName;
    }

    @Override // net.safemoon.androidwallet.model.token.abstraction.IToken
    public String getName() {
        return this.name;
    }

    @Override // net.safemoon.androidwallet.model.token.abstraction.IToken
    public double getNativeBalance() {
        return this.nativeBalance;
    }

    @Override // net.safemoon.androidwallet.model.token.abstraction.IToken
    public double getPercentChange1h() {
        return this.percentChange1h;
    }

    @Override // net.safemoon.androidwallet.model.token.abstraction.IToken
    public double getPriceInUsdt() {
        return this.priceInUsdt;
    }

    @Override // net.safemoon.androidwallet.model.token.abstraction.IToken
    public String getSymbol() {
        return this.symbol;
    }

    @Override // net.safemoon.androidwallet.model.token.abstraction.IToken
    public String getSymbolWithType() {
        return this.symbolWithType;
    }

    /* JADX WARN: 'this' call moved to the top of the method (can break code semantics) */
    public RoomCustomToken(IToken iToken) {
        this(iToken.getSymbolWithType(), iToken.getSymbol(), iToken.getName(), iToken.getIconResName(), iToken.getContractAddress(), iToken.getChainId(), iToken.getDecimals(), iToken.getAllowSwap(), iToken.getNativeBalance(), iToken.getPriceInUsdt(), iToken.getPercentChange1h());
        fs1.f(iToken, "origin");
    }
}
