package net.safemoon.androidwallet.model.token.room;

import net.safemoon.androidwallet.model.token.abstraction.IToken;
import org.web3j.ens.contracts.generated.PublicResolver;

/* compiled from: RoomToken.kt */
/* loaded from: classes2.dex */
public final class RoomToken implements IToken {
    private final boolean allowSwap;
    private final int chainId;
    private final String contractAddress;
    private final int decimals;
    private final String iconResName;
    private final String name;
    private final double nativeBalance;
    private int order;
    private final double percentChange1h;
    private final double priceInUsdt;
    private final String symbol;
    private final String symbolWithType;

    public RoomToken(String str, String str2, String str3, String str4, String str5, int i, int i2, boolean z, double d, double d2, double d3, int i3) {
        fs1.f(str, "symbolWithType");
        fs1.f(str2, "symbol");
        fs1.f(str3, PublicResolver.FUNC_NAME);
        fs1.f(str4, "iconResName");
        fs1.f(str5, "contractAddress");
        this.symbolWithType = str;
        this.symbol = str2;
        this.name = str3;
        this.iconResName = str4;
        this.contractAddress = str5;
        this.chainId = i;
        this.decimals = i2;
        this.allowSwap = z;
        this.nativeBalance = d;
        this.priceInUsdt = d2;
        this.percentChange1h = d3;
        this.order = i3;
    }

    public final String component1() {
        return getSymbolWithType();
    }

    public final double component10() {
        return getPriceInUsdt();
    }

    public final double component11() {
        return getPercentChange1h();
    }

    public final int component12() {
        return this.order;
    }

    public final String component2() {
        return getSymbol();
    }

    public final String component3() {
        return getName();
    }

    public final String component4() {
        return getIconResName();
    }

    public final String component5() {
        return getContractAddress();
    }

    public final int component6() {
        return getChainId();
    }

    public final int component7() {
        return getDecimals();
    }

    public final boolean component8() {
        return getAllowSwap();
    }

    public final double component9() {
        return getNativeBalance();
    }

    public final RoomToken copy(String str, String str2, String str3, String str4, String str5, int i, int i2, boolean z, double d, double d2, double d3, int i3) {
        fs1.f(str, "symbolWithType");
        fs1.f(str2, "symbol");
        fs1.f(str3, PublicResolver.FUNC_NAME);
        fs1.f(str4, "iconResName");
        fs1.f(str5, "contractAddress");
        return new RoomToken(str, str2, str3, str4, str5, i, i2, z, d, d2, d3, i3);
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj instanceof RoomToken) {
            RoomToken roomToken = (RoomToken) obj;
            return fs1.b(getSymbolWithType(), roomToken.getSymbolWithType()) && fs1.b(getSymbol(), roomToken.getSymbol()) && fs1.b(getName(), roomToken.getName()) && fs1.b(getIconResName(), roomToken.getIconResName()) && fs1.b(getContractAddress(), roomToken.getContractAddress()) && getChainId() == roomToken.getChainId() && getDecimals() == roomToken.getDecimals() && getAllowSwap() == roomToken.getAllowSwap() && fs1.b(Double.valueOf(getNativeBalance()), Double.valueOf(roomToken.getNativeBalance())) && fs1.b(Double.valueOf(getPriceInUsdt()), Double.valueOf(roomToken.getPriceInUsdt())) && fs1.b(Double.valueOf(getPercentChange1h()), Double.valueOf(roomToken.getPercentChange1h())) && this.order == roomToken.order;
        }
        return false;
    }

    @Override // net.safemoon.androidwallet.model.token.abstraction.IToken
    public boolean getAllowSwap() {
        return this.allowSwap;
    }

    @Override // net.safemoon.androidwallet.model.token.abstraction.IToken
    public int getChainId() {
        return this.chainId;
    }

    @Override // net.safemoon.androidwallet.model.token.abstraction.IToken
    public String getContractAddress() {
        return this.contractAddress;
    }

    @Override // net.safemoon.androidwallet.model.token.abstraction.IToken
    public int getDecimals() {
        return this.decimals;
    }

    @Override // net.safemoon.androidwallet.model.token.abstraction.IToken
    public String getIconResName() {
        return this.iconResName;
    }

    @Override // net.safemoon.androidwallet.model.token.abstraction.IToken
    public String getName() {
        return this.name;
    }

    @Override // net.safemoon.androidwallet.model.token.abstraction.IToken
    public double getNativeBalance() {
        return this.nativeBalance;
    }

    public final int getOrder() {
        return this.order;
    }

    @Override // net.safemoon.androidwallet.model.token.abstraction.IToken
    public double getPercentChange1h() {
        return this.percentChange1h;
    }

    @Override // net.safemoon.androidwallet.model.token.abstraction.IToken
    public double getPriceInUsdt() {
        return this.priceInUsdt;
    }

    @Override // net.safemoon.androidwallet.model.token.abstraction.IToken
    public String getSymbol() {
        return this.symbol;
    }

    @Override // net.safemoon.androidwallet.model.token.abstraction.IToken
    public String getSymbolWithType() {
        return this.symbolWithType;
    }

    public int hashCode() {
        int hashCode = ((((((((((((getSymbolWithType().hashCode() * 31) + getSymbol().hashCode()) * 31) + getName().hashCode()) * 31) + getIconResName().hashCode()) * 31) + getContractAddress().hashCode()) * 31) + getChainId()) * 31) + getDecimals()) * 31;
        boolean allowSwap = getAllowSwap();
        int i = allowSwap;
        if (allowSwap) {
            i = 1;
        }
        return ((((((((hashCode + i) * 31) + Double.doubleToLongBits(getNativeBalance())) * 31) + Double.doubleToLongBits(getPriceInUsdt())) * 31) + Double.doubleToLongBits(getPercentChange1h())) * 31) + this.order;
    }

    public final void setOrder(int i) {
        this.order = i;
    }

    public String toString() {
        return "RoomToken(symbolWithType=" + getSymbolWithType() + ", symbol=" + getSymbol() + ", name=" + getName() + ", iconResName=" + getIconResName() + ", contractAddress=" + getContractAddress() + ", chainId=" + getChainId() + ", decimals=" + getDecimals() + ", allowSwap=" + getAllowSwap() + ", nativeBalance=" + getNativeBalance() + ", priceInUsdt=" + getPriceInUsdt() + ", percentChange1h=" + getPercentChange1h() + ", order=" + this.order + ')';
    }

    public /* synthetic */ RoomToken(String str, String str2, String str3, String str4, String str5, int i, int i2, boolean z, double d, double d2, double d3, int i3, int i4, qi0 qi0Var) {
        this(str, str2, str3, str4, str5, i, i2, z, d, d2, d3, (i4 & 2048) != 0 ? 0 : i3);
    }

    /* JADX WARN: 'this' call moved to the top of the method (can break code semantics) */
    public RoomToken(IToken iToken) {
        this(iToken.getSymbolWithType(), iToken.getSymbol(), iToken.getName(), iToken.getIconResName(), iToken.getContractAddress(), iToken.getChainId(), iToken.getDecimals(), iToken.getAllowSwap(), iToken.getNativeBalance(), iToken.getPriceInUsdt(), iToken.getPercentChange1h(), 0);
        fs1.f(iToken, "origin");
    }
}
