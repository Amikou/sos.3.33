package net.safemoon.androidwallet.model.token.abstraction;

import java.io.Serializable;

/* compiled from: IToken.kt */
/* loaded from: classes2.dex */
public interface IToken extends Serializable {
    boolean getAllowSwap();

    int getChainId();

    String getContractAddress();

    int getDecimals();

    String getIconResName();

    String getName();

    double getNativeBalance();

    double getPercentChange1h();

    double getPriceInUsdt();

    String getSymbol();

    String getSymbolWithType();
}
