package net.safemoon.androidwallet.model.token.gson;

import androidx.recyclerview.widget.RecyclerView;
import com.google.gson.annotations.SerializedName;
import net.safemoon.androidwallet.model.token.abstraction.IToken;
import org.web3j.abi.datatypes.Address;
import org.web3j.ens.contracts.generated.PublicResolver;

/* compiled from: GsonToken.kt */
/* loaded from: classes2.dex */
public final class GsonToken implements IToken {
    @SerializedName("allowSwap")
    private final boolean allowSwap;
    @SerializedName("chainId")
    private final int chainId;
    @SerializedName("cmcId")
    private final int cmcId;
    @SerializedName("cmcSlug")
    private final String cmcSlug;
    @SerializedName(Address.TYPE_NAME)
    private final String contractAddress;
    @SerializedName("decimals")
    private final int decimals;
    @SerializedName("icon")
    private final String iconResName;
    @SerializedName(PublicResolver.FUNC_NAME)
    private final String name;
    private final double nativeBalance;
    private final double percentChange1h;
    private final double priceInUsdt;
    @SerializedName("symbol")
    private final String symbol;
    @SerializedName("symbolWithType")
    private final String symbolWithType;

    public GsonToken(String str, String str2, String str3, String str4, int i, int i2, boolean z, String str5, int i3, String str6, double d, double d2, double d3) {
        fs1.f(str, PublicResolver.FUNC_NAME);
        fs1.f(str2, "symbol");
        fs1.f(str3, "iconResName");
        fs1.f(str4, "contractAddress");
        fs1.f(str5, "symbolWithType");
        fs1.f(str6, "cmcSlug");
        this.name = str;
        this.symbol = str2;
        this.iconResName = str3;
        this.contractAddress = str4;
        this.chainId = i;
        this.decimals = i2;
        this.allowSwap = z;
        this.symbolWithType = str5;
        this.cmcId = i3;
        this.cmcSlug = str6;
        this.priceInUsdt = d;
        this.nativeBalance = d2;
        this.percentChange1h = d3;
    }

    public final String component1() {
        return getName();
    }

    public final String component10() {
        return this.cmcSlug;
    }

    public final double component11() {
        return getPriceInUsdt();
    }

    public final double component12() {
        return getNativeBalance();
    }

    public final double component13() {
        return getPercentChange1h();
    }

    public final String component2() {
        return getSymbol();
    }

    public final String component3() {
        return getIconResName();
    }

    public final String component4() {
        return getContractAddress();
    }

    public final int component5() {
        return getChainId();
    }

    public final int component6() {
        return getDecimals();
    }

    public final boolean component7() {
        return getAllowSwap();
    }

    public final String component8() {
        return getSymbolWithType();
    }

    public final int component9() {
        return this.cmcId;
    }

    public final GsonToken copy(String str, String str2, String str3, String str4, int i, int i2, boolean z, String str5, int i3, String str6, double d, double d2, double d3) {
        fs1.f(str, PublicResolver.FUNC_NAME);
        fs1.f(str2, "symbol");
        fs1.f(str3, "iconResName");
        fs1.f(str4, "contractAddress");
        fs1.f(str5, "symbolWithType");
        fs1.f(str6, "cmcSlug");
        return new GsonToken(str, str2, str3, str4, i, i2, z, str5, i3, str6, d, d2, d3);
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj instanceof GsonToken) {
            GsonToken gsonToken = (GsonToken) obj;
            return fs1.b(getName(), gsonToken.getName()) && fs1.b(getSymbol(), gsonToken.getSymbol()) && fs1.b(getIconResName(), gsonToken.getIconResName()) && fs1.b(getContractAddress(), gsonToken.getContractAddress()) && getChainId() == gsonToken.getChainId() && getDecimals() == gsonToken.getDecimals() && getAllowSwap() == gsonToken.getAllowSwap() && fs1.b(getSymbolWithType(), gsonToken.getSymbolWithType()) && this.cmcId == gsonToken.cmcId && fs1.b(this.cmcSlug, gsonToken.cmcSlug) && fs1.b(Double.valueOf(getPriceInUsdt()), Double.valueOf(gsonToken.getPriceInUsdt())) && fs1.b(Double.valueOf(getNativeBalance()), Double.valueOf(gsonToken.getNativeBalance())) && fs1.b(Double.valueOf(getPercentChange1h()), Double.valueOf(gsonToken.getPercentChange1h()));
        }
        return false;
    }

    @Override // net.safemoon.androidwallet.model.token.abstraction.IToken
    public boolean getAllowSwap() {
        return this.allowSwap;
    }

    @Override // net.safemoon.androidwallet.model.token.abstraction.IToken
    public int getChainId() {
        return this.chainId;
    }

    public final int getCmcId() {
        return this.cmcId;
    }

    public final String getCmcSlug() {
        return this.cmcSlug;
    }

    @Override // net.safemoon.androidwallet.model.token.abstraction.IToken
    public String getContractAddress() {
        return this.contractAddress;
    }

    @Override // net.safemoon.androidwallet.model.token.abstraction.IToken
    public int getDecimals() {
        return this.decimals;
    }

    @Override // net.safemoon.androidwallet.model.token.abstraction.IToken
    public String getIconResName() {
        return this.iconResName;
    }

    @Override // net.safemoon.androidwallet.model.token.abstraction.IToken
    public String getName() {
        return this.name;
    }

    @Override // net.safemoon.androidwallet.model.token.abstraction.IToken
    public double getNativeBalance() {
        return this.nativeBalance;
    }

    @Override // net.safemoon.androidwallet.model.token.abstraction.IToken
    public double getPercentChange1h() {
        return this.percentChange1h;
    }

    @Override // net.safemoon.androidwallet.model.token.abstraction.IToken
    public double getPriceInUsdt() {
        return this.priceInUsdt;
    }

    @Override // net.safemoon.androidwallet.model.token.abstraction.IToken
    public String getSymbol() {
        return this.symbol;
    }

    @Override // net.safemoon.androidwallet.model.token.abstraction.IToken
    public String getSymbolWithType() {
        return this.symbolWithType;
    }

    public int hashCode() {
        int hashCode = ((((((((((getName().hashCode() * 31) + getSymbol().hashCode()) * 31) + getIconResName().hashCode()) * 31) + getContractAddress().hashCode()) * 31) + getChainId()) * 31) + getDecimals()) * 31;
        boolean allowSwap = getAllowSwap();
        int i = allowSwap;
        if (allowSwap) {
            i = 1;
        }
        return ((((((((((((hashCode + i) * 31) + getSymbolWithType().hashCode()) * 31) + this.cmcId) * 31) + this.cmcSlug.hashCode()) * 31) + Double.doubleToLongBits(getPriceInUsdt())) * 31) + Double.doubleToLongBits(getNativeBalance())) * 31) + Double.doubleToLongBits(getPercentChange1h());
    }

    public String toString() {
        return "GsonToken(name=" + getName() + ", symbol=" + getSymbol() + ", iconResName=" + getIconResName() + ", contractAddress=" + getContractAddress() + ", chainId=" + getChainId() + ", decimals=" + getDecimals() + ", allowSwap=" + getAllowSwap() + ", symbolWithType=" + getSymbolWithType() + ", cmcId=" + this.cmcId + ", cmcSlug=" + this.cmcSlug + ", priceInUsdt=" + getPriceInUsdt() + ", nativeBalance=" + getNativeBalance() + ", percentChange1h=" + getPercentChange1h() + ')';
    }

    public /* synthetic */ GsonToken(String str, String str2, String str3, String str4, int i, int i2, boolean z, String str5, int i3, String str6, double d, double d2, double d3, int i4, qi0 qi0Var) {
        this(str, str2, str3, str4, i, i2, z, str5, (i4 & 256) != 0 ? 0 : i3, (i4 & RecyclerView.a0.FLAG_ADAPTER_POSITION_UNKNOWN) != 0 ? "" : str6, (i4 & RecyclerView.a0.FLAG_ADAPTER_FULLUPDATE) != 0 ? 0.0d : d, (i4 & 2048) != 0 ? 0.0d : d2, (i4 & 4096) != 0 ? 0.0d : d3);
    }
}
