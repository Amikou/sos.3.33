package net.safemoon.androidwallet.model.walletConnect;

import com.google.gson.Gson;
import com.trustwallet.walletconnect.models.WCPeerMeta;

/* compiled from: RoomExtension.kt */
/* loaded from: classes2.dex */
public final class RoomExtensionKt {
    public static final String changeToString(WCPeerMeta wCPeerMeta) {
        return new Gson().toJson(wCPeerMeta);
    }

    public static final WCPeerMeta toPeerMeta(String str) {
        fs1.f(str, "<this>");
        return (WCPeerMeta) new Gson().fromJson(str, (Class<Object>) WCPeerMeta.class);
    }

    public static final RoomConnectedInfo toRoomConnectedInfo(String str) {
        return (RoomConnectedInfo) new Gson().fromJson(str, (Class<Object>) RoomConnectedInfo.class);
    }

    public static final String changeToString(RoomConnectedInfo roomConnectedInfo) {
        return new Gson().toJson(roomConnectedInfo);
    }
}
