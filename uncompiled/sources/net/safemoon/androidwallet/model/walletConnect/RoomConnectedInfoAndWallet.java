package net.safemoon.androidwallet.model.walletConnect;

import net.safemoon.androidwallet.model.wallets.Wallet;

/* compiled from: RoomConnectedInfoAndWallet.kt */
/* loaded from: classes2.dex */
public final class RoomConnectedInfoAndWallet {
    private final RoomConnectedInfo dApp;

    /* renamed from: wallet  reason: collision with root package name */
    private final Wallet f14wallet;

    public RoomConnectedInfoAndWallet(RoomConnectedInfo roomConnectedInfo, Wallet wallet2) {
        fs1.f(roomConnectedInfo, "dApp");
        this.dApp = roomConnectedInfo;
        this.f14wallet = wallet2;
    }

    public static /* synthetic */ RoomConnectedInfoAndWallet copy$default(RoomConnectedInfoAndWallet roomConnectedInfoAndWallet, RoomConnectedInfo roomConnectedInfo, Wallet wallet2, int i, Object obj) {
        if ((i & 1) != 0) {
            roomConnectedInfo = roomConnectedInfoAndWallet.dApp;
        }
        if ((i & 2) != 0) {
            wallet2 = roomConnectedInfoAndWallet.f14wallet;
        }
        return roomConnectedInfoAndWallet.copy(roomConnectedInfo, wallet2);
    }

    public final RoomConnectedInfo component1() {
        return this.dApp;
    }

    public final Wallet component2() {
        return this.f14wallet;
    }

    public final RoomConnectedInfoAndWallet copy(RoomConnectedInfo roomConnectedInfo, Wallet wallet2) {
        fs1.f(roomConnectedInfo, "dApp");
        return new RoomConnectedInfoAndWallet(roomConnectedInfo, wallet2);
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj instanceof RoomConnectedInfoAndWallet) {
            RoomConnectedInfoAndWallet roomConnectedInfoAndWallet = (RoomConnectedInfoAndWallet) obj;
            return fs1.b(this.dApp, roomConnectedInfoAndWallet.dApp) && fs1.b(this.f14wallet, roomConnectedInfoAndWallet.f14wallet);
        }
        return false;
    }

    public final RoomConnectedInfo getDApp() {
        return this.dApp;
    }

    public final Wallet getWallet() {
        return this.f14wallet;
    }

    public int hashCode() {
        int hashCode = this.dApp.hashCode() * 31;
        Wallet wallet2 = this.f14wallet;
        return hashCode + (wallet2 == null ? 0 : wallet2.hashCode());
    }

    public String toString() {
        return "RoomConnectedInfoAndWallet(dApp=" + this.dApp + ", wallet=" + this.f14wallet + ')';
    }

    public /* synthetic */ RoomConnectedInfoAndWallet(RoomConnectedInfo roomConnectedInfo, Wallet wallet2, int i, qi0 qi0Var) {
        this(roomConnectedInfo, (i & 2) != 0 ? null : wallet2);
    }
}
