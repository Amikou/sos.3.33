package net.safemoon.androidwallet.model.walletConnect;

import net.safemoon.androidwallet.model.common.HistoryListItem;

/* compiled from: RoomConnectedInfoAndWalletListItem.kt */
/* loaded from: classes2.dex */
public final class RoomConnectedInfoAndWalletListItem extends HistoryListItem {
    private RoomConnectedInfoAndWallet result;

    public final RoomConnectedInfoAndWallet getResult() {
        return this.result;
    }

    @Override // net.safemoon.androidwallet.model.common.HistoryListItem
    public int getType() {
        return 2;
    }

    public final void setResult(RoomConnectedInfoAndWallet roomConnectedInfoAndWallet) {
        this.result = roomConnectedInfoAndWallet;
    }
}
