package net.safemoon.androidwallet.model.walletConnect;

import java.io.Serializable;

/* compiled from: RoomConnectedInfo.kt */
/* loaded from: classes2.dex */
public final class RoomConnectedInfo implements Serializable {
    private final int chainId;
    private final long connectedAtUnix;
    private final boolean isAutoDisconnect;
    private final String peerId;
    private final String peerMeta;
    private final String remotePeerId;
    private final String session;
    private final long walletId;

    public RoomConnectedInfo(String str, String str2, String str3, int i, String str4, long j, boolean z, long j2) {
        fs1.f(str, "session");
        fs1.f(str2, "peerId");
        fs1.f(str3, "remotePeerId");
        fs1.f(str4, "peerMeta");
        this.session = str;
        this.peerId = str2;
        this.remotePeerId = str3;
        this.chainId = i;
        this.peerMeta = str4;
        this.connectedAtUnix = j;
        this.isAutoDisconnect = z;
        this.walletId = j2;
    }

    public final String component1() {
        return this.session;
    }

    public final String component2() {
        return this.peerId;
    }

    public final String component3() {
        return this.remotePeerId;
    }

    public final int component4() {
        return this.chainId;
    }

    public final String component5() {
        return this.peerMeta;
    }

    public final long component6() {
        return this.connectedAtUnix;
    }

    public final boolean component7() {
        return this.isAutoDisconnect;
    }

    public final long component8() {
        return this.walletId;
    }

    public final RoomConnectedInfo copy(String str, String str2, String str3, int i, String str4, long j, boolean z, long j2) {
        fs1.f(str, "session");
        fs1.f(str2, "peerId");
        fs1.f(str3, "remotePeerId");
        fs1.f(str4, "peerMeta");
        return new RoomConnectedInfo(str, str2, str3, i, str4, j, z, j2);
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj instanceof RoomConnectedInfo) {
            RoomConnectedInfo roomConnectedInfo = (RoomConnectedInfo) obj;
            return fs1.b(this.session, roomConnectedInfo.session) && fs1.b(this.peerId, roomConnectedInfo.peerId) && fs1.b(this.remotePeerId, roomConnectedInfo.remotePeerId) && this.chainId == roomConnectedInfo.chainId && fs1.b(this.peerMeta, roomConnectedInfo.peerMeta) && this.connectedAtUnix == roomConnectedInfo.connectedAtUnix && this.isAutoDisconnect == roomConnectedInfo.isAutoDisconnect && this.walletId == roomConnectedInfo.walletId;
        }
        return false;
    }

    public final int getChainId() {
        return this.chainId;
    }

    public final long getConnectedAtUnix() {
        return this.connectedAtUnix;
    }

    public final String getPeerId() {
        return this.peerId;
    }

    public final String getPeerMeta() {
        return this.peerMeta;
    }

    public final String getRemotePeerId() {
        return this.remotePeerId;
    }

    public final String getSession() {
        return this.session;
    }

    public final long getWalletId() {
        return this.walletId;
    }

    /* JADX WARN: Multi-variable type inference failed */
    public int hashCode() {
        int hashCode = ((((((((((this.session.hashCode() * 31) + this.peerId.hashCode()) * 31) + this.remotePeerId.hashCode()) * 31) + this.chainId) * 31) + this.peerMeta.hashCode()) * 31) + p80.a(this.connectedAtUnix)) * 31;
        boolean z = this.isAutoDisconnect;
        int i = z;
        if (z != 0) {
            i = 1;
        }
        return ((hashCode + i) * 31) + p80.a(this.walletId);
    }

    public final boolean isAutoDisconnect() {
        return this.isAutoDisconnect;
    }

    public String toString() {
        return "RoomConnectedInfo(session=" + this.session + ", peerId=" + this.peerId + ", remotePeerId=" + this.remotePeerId + ", chainId=" + this.chainId + ", peerMeta=" + this.peerMeta + ", connectedAtUnix=" + this.connectedAtUnix + ", isAutoDisconnect=" + this.isAutoDisconnect + ", walletId=" + this.walletId + ')';
    }
}
