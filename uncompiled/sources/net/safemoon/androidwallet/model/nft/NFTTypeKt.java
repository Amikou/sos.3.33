package net.safemoon.androidwallet.model.nft;

/* compiled from: NFTType.kt */
/* loaded from: classes2.dex */
public final class NFTTypeKt {
    public static final NFTType toNFTType(String str) {
        fs1.f(str, "<this>");
        if (fs1.b(str, "ERC1155")) {
            return NFTType.ERC1155;
        }
        if (fs1.b(str, "ERC721")) {
            return NFTType.ERC721;
        }
        return null;
    }
}
