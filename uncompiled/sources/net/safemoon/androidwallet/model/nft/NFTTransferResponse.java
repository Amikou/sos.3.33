package net.safemoon.androidwallet.model.nft;

/* compiled from: NFTTransferResponse.kt */
/* loaded from: classes2.dex */
public final class NFTTransferResponse {
    private final String error;
    private final String hash;
    private final NFTType type;

    public NFTTransferResponse(NFTType nFTType, String str, String str2) {
        this.type = nFTType;
        this.hash = str;
        this.error = str2;
    }

    public static /* synthetic */ NFTTransferResponse copy$default(NFTTransferResponse nFTTransferResponse, NFTType nFTType, String str, String str2, int i, Object obj) {
        if ((i & 1) != 0) {
            nFTType = nFTTransferResponse.type;
        }
        if ((i & 2) != 0) {
            str = nFTTransferResponse.hash;
        }
        if ((i & 4) != 0) {
            str2 = nFTTransferResponse.error;
        }
        return nFTTransferResponse.copy(nFTType, str, str2);
    }

    public final NFTType component1() {
        return this.type;
    }

    public final String component2() {
        return this.hash;
    }

    public final String component3() {
        return this.error;
    }

    public final NFTTransferResponse copy(NFTType nFTType, String str, String str2) {
        return new NFTTransferResponse(nFTType, str, str2);
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj instanceof NFTTransferResponse) {
            NFTTransferResponse nFTTransferResponse = (NFTTransferResponse) obj;
            return this.type == nFTTransferResponse.type && fs1.b(this.hash, nFTTransferResponse.hash) && fs1.b(this.error, nFTTransferResponse.error);
        }
        return false;
    }

    public final String getError() {
        return this.error;
    }

    public final String getHash() {
        return this.hash;
    }

    public final NFTType getType() {
        return this.type;
    }

    public int hashCode() {
        NFTType nFTType = this.type;
        int hashCode = (nFTType == null ? 0 : nFTType.hashCode()) * 31;
        String str = this.hash;
        int hashCode2 = (hashCode + (str == null ? 0 : str.hashCode())) * 31;
        String str2 = this.error;
        return hashCode2 + (str2 != null ? str2.hashCode() : 0);
    }

    public String toString() {
        return "NFTTransferResponse(type=" + this.type + ", hash=" + ((Object) this.hash) + ", error=" + ((Object) this.error) + ')';
    }
}
