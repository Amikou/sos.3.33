package net.safemoon.androidwallet.model.nft;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/* compiled from: DeleteNfts.kt */
/* loaded from: classes2.dex */
public final class DeleteNfts {
    @SerializedName("data")
    @Expose
    private final DeleteNftIdList data;

    public final DeleteNftIdList getData() {
        return this.data;
    }
}
