package net.safemoon.androidwallet.model.nft;

/* compiled from: NFTBalance.kt */
/* loaded from: classes2.dex */
public final class NFTBalance {
    private final int balance;
    private final NFTType type;

    public NFTBalance(NFTType nFTType, int i) {
        fs1.f(nFTType, "type");
        this.type = nFTType;
        this.balance = i;
    }

    public static /* synthetic */ NFTBalance copy$default(NFTBalance nFTBalance, NFTType nFTType, int i, int i2, Object obj) {
        if ((i2 & 1) != 0) {
            nFTType = nFTBalance.type;
        }
        if ((i2 & 2) != 0) {
            i = nFTBalance.balance;
        }
        return nFTBalance.copy(nFTType, i);
    }

    public final NFTType component1() {
        return this.type;
    }

    public final int component2() {
        return this.balance;
    }

    public final NFTBalance copy(NFTType nFTType, int i) {
        fs1.f(nFTType, "type");
        return new NFTBalance(nFTType, i);
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj instanceof NFTBalance) {
            NFTBalance nFTBalance = (NFTBalance) obj;
            return this.type == nFTBalance.type && this.balance == nFTBalance.balance;
        }
        return false;
    }

    public final int getBalance() {
        return this.balance;
    }

    public final NFTType getType() {
        return this.type;
    }

    public int hashCode() {
        return (this.type.hashCode() * 31) + this.balance;
    }

    public String toString() {
        return "NFTBalance(type=" + this.type + ", balance=" + this.balance + ')';
    }
}
