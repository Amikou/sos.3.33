package net.safemoon.androidwallet.model.nft;

import com.google.gson.annotations.SerializedName;
import java.util.ArrayList;

/* compiled from: DeleteNftIdList.kt */
/* loaded from: classes2.dex */
public final class DeleteNftIdList {
    @SerializedName("ownerIds")
    private ArrayList<String> ownerIds;
    @SerializedName("ownerIdsForever")
    private ArrayList<String> ownerIdsForever;
    @SerializedName("walletAddress")
    private String walletAddress;

    public DeleteNftIdList() {
        this(null, null, null, 7, null);
    }

    public DeleteNftIdList(String str, ArrayList<String> arrayList, ArrayList<String> arrayList2) {
        fs1.f(arrayList, "ownerIds");
        fs1.f(arrayList2, "ownerIdsForever");
        this.walletAddress = str;
        this.ownerIds = arrayList;
        this.ownerIdsForever = arrayList2;
    }

    public final ArrayList<String> getOwnerIds() {
        return this.ownerIds;
    }

    public final ArrayList<String> getOwnerIdsForever() {
        return this.ownerIdsForever;
    }

    public final String getWalletAddress() {
        return this.walletAddress;
    }

    public final void setOwnerIds(ArrayList<String> arrayList) {
        fs1.f(arrayList, "<set-?>");
        this.ownerIds = arrayList;
    }

    public final void setOwnerIdsForever(ArrayList<String> arrayList) {
        fs1.f(arrayList, "<set-?>");
        this.ownerIdsForever = arrayList;
    }

    public final void setWalletAddress(String str) {
        this.walletAddress = str;
    }

    public /* synthetic */ DeleteNftIdList(String str, ArrayList arrayList, ArrayList arrayList2, int i, qi0 qi0Var) {
        this((i & 1) != 0 ? null : str, (i & 2) != 0 ? new ArrayList() : arrayList, (i & 4) != 0 ? new ArrayList() : arrayList2);
    }
}
