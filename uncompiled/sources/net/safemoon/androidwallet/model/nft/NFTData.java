package net.safemoon.androidwallet.model.nft;

import java.io.Serializable;
import java.util.ArrayList;
import net.safemoon.androidwallet.model.collectible.AssetContract;

/* compiled from: NFTData.kt */
/* loaded from: classes2.dex */
public final class NFTData implements Serializable {
    private String animationUrl;
    private AssetContract assetContract;
    private Integer chainID;
    private String description;
    private String imageData;
    private String imageUrl;
    private String name;
    private String openSeaUrl;
    private String permalink;
    private Integer position;
    private ArrayList<Property> properties;
    private Long roomId;
    private String slug;
    private String tokenId;

    /* compiled from: NFTData.kt */
    /* loaded from: classes2.dex */
    public static final class Property implements Serializable {
        private String name;
        private String value;

        public Property(String str, String str2) {
            this.name = str;
            this.value = str2;
        }

        public final String getName() {
            return this.name;
        }

        public final String getValue() {
            return this.value;
        }

        public final void setName(String str) {
            this.name = str;
        }

        public final void setValue(String str) {
            this.value = str;
        }
    }

    public NFTData() {
        this.properties = new ArrayList<>();
    }

    public final String getAnimationUrl() {
        return this.animationUrl;
    }

    public final AssetContract getAssetContract() {
        return this.assetContract;
    }

    public final Integer getChainID() {
        return this.chainID;
    }

    public final String getDescription() {
        return this.description;
    }

    public final String getImageData() {
        return this.imageData;
    }

    public final String getImageUrl() {
        return this.imageUrl;
    }

    public final String getName() {
        return this.name;
    }

    public final String getOpenSeaUrl() {
        return this.openSeaUrl;
    }

    public final String getPermalink() {
        return this.permalink;
    }

    public final Integer getPosition() {
        return this.position;
    }

    public final ArrayList<Property> getProperties() {
        return this.properties;
    }

    public final Long getRoomId() {
        return this.roomId;
    }

    public final String getSlug() {
        return this.slug;
    }

    public final String getTokenId() {
        return this.tokenId;
    }

    public final void setAnimationUrl(String str) {
        this.animationUrl = str;
    }

    public final void setAssetContract(AssetContract assetContract) {
        this.assetContract = assetContract;
    }

    public final void setChainID(Integer num) {
        this.chainID = num;
    }

    public final void setDescription(String str) {
        this.description = str;
    }

    public final void setImageData(String str) {
        this.imageData = str;
    }

    public final void setImageUrl(String str) {
        this.imageUrl = str;
    }

    public final void setName(String str) {
        this.name = str;
    }

    public final void setOpenSeaUrl(String str) {
        this.openSeaUrl = str;
    }

    public final void setPermalink(String str) {
        this.permalink = str;
    }

    public final void setPosition(Integer num) {
        this.position = num;
    }

    public final void setProperties(ArrayList<Property> arrayList) {
        this.properties = arrayList;
    }

    public final void setRoomId(Long l) {
        this.roomId = l;
    }

    public final void setSlug(String str) {
        this.slug = str;
    }

    public final void setTokenId(String str) {
        this.tokenId = str;
    }

    public /* synthetic */ NFTData(String str, String str2, String str3, String str4, String str5, String str6, String str7, AssetContract assetContract, int i, qi0 qi0Var) {
        this(str, str2, str3, str4, str5, str6, (i & 64) != 0 ? null : str7, (i & 128) != 0 ? null : assetContract);
    }

    public NFTData(String str, String str2, String str3, String str4, String str5, String str6, String str7, AssetContract assetContract) {
        this.properties = new ArrayList<>();
        this.name = str;
        this.imageUrl = str2;
        this.description = str3;
        this.permalink = str4;
        this.slug = str5;
        this.openSeaUrl = str6;
        this.tokenId = str7;
        this.assetContract = assetContract;
    }
}
