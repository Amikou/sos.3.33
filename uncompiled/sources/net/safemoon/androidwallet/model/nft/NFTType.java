package net.safemoon.androidwallet.model.nft;

/* compiled from: NFTType.kt */
/* loaded from: classes2.dex */
public enum NFTType {
    ERC721,
    ERC1155
}
