package net.safemoon.androidwallet.model.wallets;

import androidx.recyclerview.widget.RecyclerView;
import com.google.gson.Gson;
import net.safemoon.androidwallet.MyApplicationClass;
import net.safemoon.androidwallet.R;
import org.web3j.abi.datatypes.Address;
import org.web3j.ens.contracts.generated.PublicResolver;

/* compiled from: Wallet.kt */
/* loaded from: classes2.dex */
public final class Wallet {
    public static final Companion Companion = new Companion(null);
    private final String KA;
    private final String U5B64;
    private String address;
    private Long id;
    private boolean isPrimaryWallet;
    private int linkedState;
    private String name;
    private int order;
    private final String passPhrase;
    private final String prefix;
    private final String privateKey;

    /* compiled from: Wallet.kt */
    /* loaded from: classes2.dex */
    public static final class Companion {
        private Companion() {
        }

        public /* synthetic */ Companion(qi0 qi0Var) {
            this();
        }

        public final Wallet toWallet(String str) {
            fs1.f(str, "walletJson");
            try {
                return (Wallet) new Gson().fromJson(str, (Class<Object>) Wallet.class);
            } catch (Exception unused) {
                return null;
            }
        }
    }

    public Wallet(Long l, String str, String str2, String str3, String str4, String str5, String str6, String str7, int i, int i2, boolean z) {
        fs1.f(str, PublicResolver.FUNC_NAME);
        fs1.f(str2, "prefix");
        fs1.f(str3, "privateKey");
        fs1.f(str4, Address.TYPE_NAME);
        this.id = l;
        this.name = str;
        this.prefix = str2;
        this.privateKey = str3;
        this.address = str4;
        this.passPhrase = str5;
        this.KA = str6;
        this.U5B64 = str7;
        this.linkedState = i;
        this.order = i2;
        this.isPrimaryWallet = z;
    }

    public final Long component1() {
        return this.id;
    }

    public final int component10() {
        return this.order;
    }

    public final boolean component11() {
        return this.isPrimaryWallet;
    }

    public final String component2() {
        return this.name;
    }

    public final String component3() {
        return this.prefix;
    }

    public final String component4() {
        return this.privateKey;
    }

    public final String component5() {
        return this.address;
    }

    public final String component6() {
        return this.passPhrase;
    }

    public final String component7() {
        return this.KA;
    }

    public final String component8() {
        return this.U5B64;
    }

    public final int component9() {
        return this.linkedState;
    }

    public final Wallet copy(Long l, String str, String str2, String str3, String str4, String str5, String str6, String str7, int i, int i2, boolean z) {
        fs1.f(str, PublicResolver.FUNC_NAME);
        fs1.f(str2, "prefix");
        fs1.f(str3, "privateKey");
        fs1.f(str4, Address.TYPE_NAME);
        return new Wallet(l, str, str2, str3, str4, str5, str6, str7, i, i2, z);
    }

    public final String displayName() {
        if (this.isPrimaryWallet) {
            String string = MyApplicationClass.c().getString(R.string.primary_wallet_name);
            fs1.e(string, "get().getString(R.string.primary_wallet_name)");
            return string;
        }
        return this.name;
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj instanceof Wallet) {
            Wallet wallet2 = (Wallet) obj;
            return fs1.b(this.id, wallet2.id) && fs1.b(this.name, wallet2.name) && fs1.b(this.prefix, wallet2.prefix) && fs1.b(this.privateKey, wallet2.privateKey) && fs1.b(this.address, wallet2.address) && fs1.b(this.passPhrase, wallet2.passPhrase) && fs1.b(this.KA, wallet2.KA) && fs1.b(this.U5B64, wallet2.U5B64) && this.linkedState == wallet2.linkedState && this.order == wallet2.order && this.isPrimaryWallet == wallet2.isPrimaryWallet;
        }
        return false;
    }

    public final String getAddress() {
        return this.address;
    }

    public final Long getId() {
        return this.id;
    }

    public final String getKA() {
        return this.KA;
    }

    public final int getLinkedState() {
        return this.linkedState;
    }

    public final String getName() {
        return this.name;
    }

    public final int getOrder() {
        return this.order;
    }

    public final String getPassPhrase() {
        return this.passPhrase;
    }

    public final String getPrefix() {
        return this.prefix;
    }

    public final String getPrivateKey() {
        return this.privateKey;
    }

    public final String getU5B64() {
        return this.U5B64;
    }

    /* JADX WARN: Multi-variable type inference failed */
    public int hashCode() {
        Long l = this.id;
        int hashCode = (((((((((l == null ? 0 : l.hashCode()) * 31) + this.name.hashCode()) * 31) + this.prefix.hashCode()) * 31) + this.privateKey.hashCode()) * 31) + this.address.hashCode()) * 31;
        String str = this.passPhrase;
        int hashCode2 = (hashCode + (str == null ? 0 : str.hashCode())) * 31;
        String str2 = this.KA;
        int hashCode3 = (hashCode2 + (str2 == null ? 0 : str2.hashCode())) * 31;
        String str3 = this.U5B64;
        int hashCode4 = (((((hashCode3 + (str3 != null ? str3.hashCode() : 0)) * 31) + this.linkedState) * 31) + this.order) * 31;
        boolean z = this.isPrimaryWallet;
        int i = z;
        if (z != 0) {
            i = 1;
        }
        return hashCode4 + i;
    }

    public final boolean isLinked() {
        return this.linkedState == 1;
    }

    public final boolean isPrimaryWallet() {
        return this.isPrimaryWallet;
    }

    public final void setAddress(String str) {
        fs1.f(str, "<set-?>");
        this.address = str;
    }

    public final void setId(Long l) {
        this.id = l;
    }

    public final void setLinkedState(int i) {
        this.linkedState = i;
    }

    public final void setName(String str) {
        fs1.f(str, "<set-?>");
        this.name = str;
    }

    public final void setOrder(int i) {
        this.order = i;
    }

    public final void setPrimaryWallet(boolean z) {
        this.isPrimaryWallet = z;
    }

    public String toString() {
        String json = new Gson().toJson(this, Wallet.class);
        fs1.e(json, "Gson().toJson(this, Wallet::class.java)");
        return json;
    }

    public final void updateLinkedState(boolean z) {
        this.linkedState = z ? 1 : 0;
    }

    public /* synthetic */ Wallet(Long l, String str, String str2, String str3, String str4, String str5, String str6, String str7, int i, int i2, boolean z, int i3, qi0 qi0Var) {
        this((i3 & 1) != 0 ? null : l, str, str2, str3, str4, (i3 & 32) != 0 ? null : str5, (i3 & 64) != 0 ? null : str6, (i3 & 128) != 0 ? null : str7, (i3 & 256) != 0 ? 0 : i, (i3 & RecyclerView.a0.FLAG_ADAPTER_POSITION_UNKNOWN) != 0 ? 0 : i2, (i3 & RecyclerView.a0.FLAG_ADAPTER_FULLUPDATE) != 0 ? false : z);
    }
}
