package net.safemoon.androidwallet.model;

import com.fasterxml.jackson.databind.deser.std.ThrowableDeserializer;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import org.web3j.abi.datatypes.Address;

/* loaded from: classes2.dex */
public class WalletModel {
    @SerializedName("code")
    @Expose
    public String code;
    @SerializedName("data")
    @Expose
    public Data data;
    @SerializedName(ThrowableDeserializer.PROP_NAME_MESSAGE)
    @Expose
    public String message;

    /* loaded from: classes2.dex */
    public class Data {
        @SerializedName(Address.TYPE_NAME)
        @Expose
        public String address;
        @SerializedName("privateKey")
        @Expose
        public String privateKey;

        public Data() {
        }
    }
}
