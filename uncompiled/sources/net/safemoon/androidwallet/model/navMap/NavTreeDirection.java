package net.safemoon.androidwallet.model.navMap;

import androidx.navigation.NavController;
import com.google.gson.reflect.TypeToken;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import net.safemoon.androidwallet.R;

/* compiled from: NavTreeDirection.kt */
/* loaded from: classes2.dex */
public final class NavTreeDirection {
    public static final NavTreeDirection INSTANCE = new NavTreeDirection();
    private static final NavMap navMap;

    static {
        NavMap navMap2 = new NavMap(0, null, 2, null);
        navMap = navMap2;
        ArrayList<NavMap> data = navMap2.getData();
        NavMap navMap3 = new NavMap(R.id.navigation_collectibles, null, 2, null);
        ArrayList<NavMap> data2 = navMap3.getData();
        NavMap navMap4 = new NavMap(R.id.collectionFragment, null, 2, null);
        ArrayList<NavMap> data3 = navMap4.getData();
        NavMap navMap5 = new NavMap(R.id.nftDetailFragment, null, 2, null);
        ArrayList<NavMap> data4 = navMap5.getData();
        NavMap navMap6 = new NavMap(R.id.nftSendToFragment, null, 2, null);
        navMap6.getData().add(new NavMap(R.id.nftTransfer, null, 2, null));
        ArrayList<NavMap> data5 = navMap6.getData();
        NavMap navMap7 = new NavMap(R.id.manageContactsFragment, null, 2, null);
        ArrayList<NavMap> data6 = navMap7.getData();
        NavMap navMap8 = new NavMap(R.id.addContactFragment, null, 2, null);
        navMap8.getData().add(new NavMap(R.id.selectChainForContact, null, 2, null));
        te4 te4Var = te4.a;
        data6.add(navMap8);
        navMap7.getData().add(new NavMap(R.id.editContactFragment, null, 2, null));
        navMap7.getData().add(new NavMap(R.id.selectChainForContact, null, 2, null));
        data5.add(navMap7);
        data4.add(navMap6);
        data3.add(navMap5);
        data2.add(navMap4);
        data.add(navMap3);
        ArrayList<NavMap> data7 = navMap2.getData();
        NavMap navMap9 = new NavMap(R.id.navigation_settings, null, 2, null);
        navMap9.getData().add(new NavMap(R.id.switchWalletFragment, null, 2, null));
        ArrayList<NavMap> data8 = navMap9.getData();
        NavMap navMap10 = new NavMap(R.id.securityFragment, null, 2, null);
        navMap10.getData().add(new NavMap(R.id.changePasswordFragment, null, 2, null));
        data8.add(navMap10);
        navMap9.getData().add(new NavMap(R.id.notificationFragment, null, 2, null));
        ArrayList<NavMap> data9 = navMap9.getData();
        NavMap navMap11 = new NavMap(R.id.walletConnectFragment, null, 2, null);
        navMap11.getData().add(new NavMap(R.id.walletConnectDetailFragment, null, 2, null));
        data9.add(navMap11);
        ArrayList<NavMap> data10 = navMap9.getData();
        NavMap navMap12 = new NavMap(R.id.manageContactsFragment, null, 2, null);
        ArrayList<NavMap> data11 = navMap12.getData();
        NavMap navMap13 = new NavMap(R.id.addContactFragment, null, 2, null);
        navMap13.getData().add(new NavMap(R.id.selectChainForContact, null, 2, null));
        data11.add(navMap13);
        navMap12.getData().add(new NavMap(R.id.editContactFragment, null, 2, null));
        navMap12.getData().add(new NavMap(R.id.selectChainForContact, null, 2, null));
        data10.add(navMap12);
        navMap9.getData().add(new NavMap(R.id.fiatListFragment, null, 2, null));
        navMap9.getData().add(new NavMap(R.id.dateFormatFragment, null, 2, null));
        navMap9.getData().add(new NavMap(R.id.fullScreenWebViewFragment, null, 2, null));
        navMap9.getData().add(new NavMap(R.id.termScreenWebViewFragment, null, 2, null));
        navMap9.getData().add(new NavMap(R.id.joinCommunityFragment, null, 2, null));
        data7.add(navMap9);
    }

    private NavTreeDirection() {
    }

    private final void filterBackStack(NavMap navMap2, NavController navController, ArrayList<Integer> arrayList) {
        ArrayList<NavMap> data = navMap2.getData();
        ArrayList arrayList2 = new ArrayList();
        for (Object obj : data) {
            if (yd2.a(navController, ((NavMap) obj).getId())) {
                arrayList2.add(obj);
            }
        }
        navMap2.getData().clear();
        navMap2.getData().addAll(arrayList2);
        if (!navMap2.getData().isEmpty()) {
            NavMap navMap3 = (NavMap) j20.L(navMap2.getData());
            arrayList.add(Integer.valueOf(navMap3.getId()));
            INSTANCE.filterBackStack(navMap3, navController, arrayList);
        }
    }

    private final ArrayList<Integer> getPath(NavMap navMap2, int i, ArrayList<Integer> arrayList) {
        for (NavMap navMap3 : navMap2.getData()) {
            arrayList.add(Integer.valueOf(navMap3.getId()));
            if (i == navMap3.getId()) {
                return arrayList;
            }
            NavTreeDirection navTreeDirection = INSTANCE;
            ArrayList<Integer> arrayList2 = new ArrayList<>();
            arrayList2.addAll(arrayList);
            te4 te4Var = te4.a;
            navTreeDirection.getPath(navMap3, i, arrayList2);
        }
        return new ArrayList<>();
    }

    public final Integer getLastDestination(NavController navController, int i) {
        boolean z;
        fs1.f(navController, "<this>");
        Type type = new TypeToken<List<? extends NavMap>>() { // from class: net.safemoon.androidwallet.model.navMap.NavTreeDirection$getLastDestination$itemType$1
        }.getType();
        Object obj = null;
        if (yd2.a(navController, i)) {
            ArrayList<NavMap> data = navMap.getData();
            fs1.e(type, "itemType");
            Iterator it = e30.l(data, type).iterator();
            while (true) {
                if (!it.hasNext()) {
                    break;
                }
                Object next = it.next();
                if (((NavMap) next).getId() == i) {
                    z = true;
                    continue;
                } else {
                    z = false;
                    continue;
                }
                if (z) {
                    obj = next;
                    break;
                }
            }
            NavMap navMap2 = (NavMap) obj;
            ArrayList<Integer> arrayList = new ArrayList<>();
            if (navMap2 != null) {
                filterBackStack(navMap2, navController, arrayList);
            }
            return (Integer) j20.V(arrayList);
        }
        return null;
    }

    public final NavMap getMap() {
        return navMap;
    }
}
