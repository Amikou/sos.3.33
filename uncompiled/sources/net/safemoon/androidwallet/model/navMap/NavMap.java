package net.safemoon.androidwallet.model.navMap;

import java.util.ArrayList;

/* compiled from: NavMap.kt */
/* loaded from: classes2.dex */
public final class NavMap {
    private ArrayList<NavMap> data;
    private final int id;

    public NavMap(int i, ArrayList<NavMap> arrayList) {
        fs1.f(arrayList, "data");
        this.id = i;
        this.data = arrayList;
    }

    /* JADX WARN: Multi-variable type inference failed */
    public static /* synthetic */ NavMap copy$default(NavMap navMap, int i, ArrayList arrayList, int i2, Object obj) {
        if ((i2 & 1) != 0) {
            i = navMap.id;
        }
        if ((i2 & 2) != 0) {
            arrayList = navMap.data;
        }
        return navMap.copy(i, arrayList);
    }

    public final int component1() {
        return this.id;
    }

    public final ArrayList<NavMap> component2() {
        return this.data;
    }

    public final NavMap copy(int i, ArrayList<NavMap> arrayList) {
        fs1.f(arrayList, "data");
        return new NavMap(i, arrayList);
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj instanceof NavMap) {
            NavMap navMap = (NavMap) obj;
            return this.id == navMap.id && fs1.b(this.data, navMap.data);
        }
        return false;
    }

    public final ArrayList<NavMap> getData() {
        return this.data;
    }

    public final int getId() {
        return this.id;
    }

    public int hashCode() {
        return (this.id * 31) + this.data.hashCode();
    }

    public final void setData(ArrayList<NavMap> arrayList) {
        fs1.f(arrayList, "<set-?>");
        this.data = arrayList;
    }

    public String toString() {
        return "NavMap(id=" + this.id + ", data=" + this.data + ')';
    }

    public /* synthetic */ NavMap(int i, ArrayList arrayList, int i2, qi0 qi0Var) {
        this(i, (i2 & 2) != 0 ? new ArrayList() : arrayList);
    }
}
