package net.safemoon.androidwallet.model.cmcTokenInfo;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import java.io.Serializable;
import java.util.List;
import org.web3j.ens.contracts.generated.PublicResolver;

/* loaded from: classes2.dex */
public class TokenDetail implements Serializable {
    private static final long serialVersionUID = -3211774919578487863L;
    @SerializedName("category")
    @Expose
    public String category;
    @SerializedName("date_added")
    @Expose
    public String dateAdded;
    @SerializedName("description")
    @Expose
    public String description;
    @SerializedName("id")
    @Expose
    public Integer id;
    @SerializedName("is_hidden")
    @Expose
    public Integer isHidden;
    @SerializedName("logo")
    @Expose
    public String logo;
    @SerializedName(PublicResolver.FUNC_NAME)
    @Expose
    public String name;
    @SerializedName("notice")
    @Expose
    public String notice;
    @SerializedName("platform")
    @Expose
    public Platform platform;
    @SerializedName("slug")
    @Expose
    public String slug;
    @SerializedName("subreddit")
    @Expose
    public String subreddit;
    @SerializedName("symbol")
    @Expose
    public String symbol;
    @SerializedName("twitter_username")
    @Expose
    public String twitterUsername;
    @SerializedName("urls")
    @Expose
    public Urls urls;
    @SerializedName("tags")
    @Expose
    public List<String> tags = null;
    @SerializedName("tag-names")
    @Expose
    public List<String> tagNames = null;
    @SerializedName("tag-groups")
    @Expose
    public List<String> tagGroups = null;
}
