package net.safemoon.androidwallet.model.cmcTokenInfo;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import java.io.Serializable;
import org.web3j.ens.contracts.generated.PublicResolver;

/* loaded from: classes2.dex */
public class Platform implements Serializable {
    private static final long serialVersionUID = 8859644860166156163L;
    @SerializedName("id")
    @Expose
    public Integer id;
    @SerializedName(PublicResolver.FUNC_NAME)
    @Expose
    public String name;
    @SerializedName("slug")
    @Expose
    public String slug;
    @SerializedName("symbol")
    @Expose
    public String symbol;
    @SerializedName("token_address")
    @Expose
    public String tokenAddress;
}
