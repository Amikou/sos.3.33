package net.safemoon.androidwallet.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import java.io.Serializable;

/* loaded from: classes2.dex */
public class HistoricalList implements Serializable {
    @SerializedName("data")
    @Expose
    private Data data;
    @SerializedName("status")
    @Expose
    private Status status;

    public Data getData() {
        return this.data;
    }

    public Status getStatus() {
        return this.status;
    }

    public void setData(Data data) {
        this.data = data;
    }

    public void setStatus(Status status) {
        this.status = status;
    }
}
