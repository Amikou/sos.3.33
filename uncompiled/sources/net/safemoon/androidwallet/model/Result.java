package net.safemoon.androidwallet.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/* compiled from: EthGasPrice.kt */
/* loaded from: classes2.dex */
public final class Result {
    @SerializedName("FastGasPrice")
    @Expose
    private final String FastGasPrice;
    @SerializedName("LastBlock")
    @Expose
    private final String LastBlock;
    @SerializedName("ProposeGasPrice")
    @Expose
    private final String ProposeGasPrice;
    @SerializedName("SafeGasPrice")
    @Expose
    private final String SafeGasPrice;
    @SerializedName("gasUsedRatio")
    @Expose
    private final String gasUsedRatio;
    @SerializedName("suggestBaseFee")
    @Expose
    private final String suggestBaseFee;

    public Result(String str, String str2, String str3, String str4, String str5, String str6) {
        fs1.f(str, "LastBlock");
        fs1.f(str2, "SafeGasPrice");
        fs1.f(str3, "ProposeGasPrice");
        fs1.f(str4, "FastGasPrice");
        fs1.f(str5, "suggestBaseFee");
        fs1.f(str6, "gasUsedRatio");
        this.LastBlock = str;
        this.SafeGasPrice = str2;
        this.ProposeGasPrice = str3;
        this.FastGasPrice = str4;
        this.suggestBaseFee = str5;
        this.gasUsedRatio = str6;
    }

    public static /* synthetic */ Result copy$default(Result result, String str, String str2, String str3, String str4, String str5, String str6, int i, Object obj) {
        if ((i & 1) != 0) {
            str = result.LastBlock;
        }
        if ((i & 2) != 0) {
            str2 = result.SafeGasPrice;
        }
        String str7 = str2;
        if ((i & 4) != 0) {
            str3 = result.ProposeGasPrice;
        }
        String str8 = str3;
        if ((i & 8) != 0) {
            str4 = result.FastGasPrice;
        }
        String str9 = str4;
        if ((i & 16) != 0) {
            str5 = result.suggestBaseFee;
        }
        String str10 = str5;
        if ((i & 32) != 0) {
            str6 = result.gasUsedRatio;
        }
        return result.copy(str, str7, str8, str9, str10, str6);
    }

    public final String component1() {
        return this.LastBlock;
    }

    public final String component2() {
        return this.SafeGasPrice;
    }

    public final String component3() {
        return this.ProposeGasPrice;
    }

    public final String component4() {
        return this.FastGasPrice;
    }

    public final String component5() {
        return this.suggestBaseFee;
    }

    public final String component6() {
        return this.gasUsedRatio;
    }

    public final Result copy(String str, String str2, String str3, String str4, String str5, String str6) {
        fs1.f(str, "LastBlock");
        fs1.f(str2, "SafeGasPrice");
        fs1.f(str3, "ProposeGasPrice");
        fs1.f(str4, "FastGasPrice");
        fs1.f(str5, "suggestBaseFee");
        fs1.f(str6, "gasUsedRatio");
        return new Result(str, str2, str3, str4, str5, str6);
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj instanceof Result) {
            Result result = (Result) obj;
            return fs1.b(this.LastBlock, result.LastBlock) && fs1.b(this.SafeGasPrice, result.SafeGasPrice) && fs1.b(this.ProposeGasPrice, result.ProposeGasPrice) && fs1.b(this.FastGasPrice, result.FastGasPrice) && fs1.b(this.suggestBaseFee, result.suggestBaseFee) && fs1.b(this.gasUsedRatio, result.gasUsedRatio);
        }
        return false;
    }

    public final String getFastGasPrice() {
        return this.FastGasPrice;
    }

    public final String getGasUsedRatio() {
        return this.gasUsedRatio;
    }

    public final String getLastBlock() {
        return this.LastBlock;
    }

    public final String getProposeGasPrice() {
        return this.ProposeGasPrice;
    }

    public final String getSafeGasPrice() {
        return this.SafeGasPrice;
    }

    public final String getSuggestBaseFee() {
        return this.suggestBaseFee;
    }

    public int hashCode() {
        return (((((((((this.LastBlock.hashCode() * 31) + this.SafeGasPrice.hashCode()) * 31) + this.ProposeGasPrice.hashCode()) * 31) + this.FastGasPrice.hashCode()) * 31) + this.suggestBaseFee.hashCode()) * 31) + this.gasUsedRatio.hashCode();
    }

    public String toString() {
        return "Result(LastBlock=" + this.LastBlock + ", SafeGasPrice=" + this.SafeGasPrice + ", ProposeGasPrice=" + this.ProposeGasPrice + ", FastGasPrice=" + this.FastGasPrice + ", suggestBaseFee=" + this.suggestBaseFee + ", gasUsedRatio=" + this.gasUsedRatio + ')';
    }
}
