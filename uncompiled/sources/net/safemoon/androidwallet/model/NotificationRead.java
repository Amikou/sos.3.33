package net.safemoon.androidwallet.model;

import com.fasterxml.jackson.databind.deser.std.ThrowableDeserializer;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/* loaded from: classes2.dex */
public class NotificationRead {
    @SerializedName("code")
    @Expose
    public String code;
    @SerializedName("data")
    @Expose
    public Data data;
    @SerializedName(ThrowableDeserializer.PROP_NAME_MESSAGE)
    @Expose
    public String message;

    /* loaded from: classes2.dex */
    public class Data {
        @SerializedName("result")
        @Expose
        public String result;

        public Data() {
        }
    }
}
