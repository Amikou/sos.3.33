package net.safemoon.androidwallet.model.request;

import androidx.annotation.Keep;
import com.google.android.gms.common.annotation.KeepName;
import com.google.gson.Gson;

/* compiled from: RequestTransfer.kt */
@Keep
@KeepName
/* loaded from: classes2.dex */
public final class RequestTransfer {
    private final String amount;
    private final String from;
    private final String privateKey;
    private final String requestTime;
    private final String to;

    public RequestTransfer(String str, String str2, String str3, String str4, String str5) {
        fs1.f(str, "from");
        fs1.f(str2, "to");
        fs1.f(str3, "amount");
        fs1.f(str4, "privateKey");
        fs1.f(str5, "requestTime");
        this.from = str;
        this.to = str2;
        this.amount = str3;
        this.privateKey = str4;
        this.requestTime = str5;
    }

    public static /* synthetic */ RequestTransfer copy$default(RequestTransfer requestTransfer, String str, String str2, String str3, String str4, String str5, int i, Object obj) {
        if ((i & 1) != 0) {
            str = requestTransfer.from;
        }
        if ((i & 2) != 0) {
            str2 = requestTransfer.to;
        }
        String str6 = str2;
        if ((i & 4) != 0) {
            str3 = requestTransfer.amount;
        }
        String str7 = str3;
        if ((i & 8) != 0) {
            str4 = requestTransfer.privateKey;
        }
        String str8 = str4;
        if ((i & 16) != 0) {
            str5 = requestTransfer.requestTime;
        }
        return requestTransfer.copy(str, str6, str7, str8, str5);
    }

    public final String component1() {
        return this.from;
    }

    public final String component2() {
        return this.to;
    }

    public final String component3() {
        return this.amount;
    }

    public final String component4() {
        return this.privateKey;
    }

    public final String component5() {
        return this.requestTime;
    }

    public final RequestTransfer copy(String str, String str2, String str3, String str4, String str5) {
        fs1.f(str, "from");
        fs1.f(str2, "to");
        fs1.f(str3, "amount");
        fs1.f(str4, "privateKey");
        fs1.f(str5, "requestTime");
        return new RequestTransfer(str, str2, str3, str4, str5);
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj instanceof RequestTransfer) {
            RequestTransfer requestTransfer = (RequestTransfer) obj;
            return fs1.b(this.from, requestTransfer.from) && fs1.b(this.to, requestTransfer.to) && fs1.b(this.amount, requestTransfer.amount) && fs1.b(this.privateKey, requestTransfer.privateKey) && fs1.b(this.requestTime, requestTransfer.requestTime);
        }
        return false;
    }

    public final String getAmount() {
        return this.amount;
    }

    public final String getFrom() {
        return this.from;
    }

    public final String getPrivateKey() {
        return this.privateKey;
    }

    public final String getRequestTime() {
        return this.requestTime;
    }

    public final String getTo() {
        return this.to;
    }

    public int hashCode() {
        return (((((((this.from.hashCode() * 31) + this.to.hashCode()) * 31) + this.amount.hashCode()) * 31) + this.privateKey.hashCode()) * 31) + this.requestTime.hashCode();
    }

    public String toString() {
        String json = new Gson().toJson(this);
        fs1.e(json, "Gson().toJson(this)");
        return json;
    }
}
