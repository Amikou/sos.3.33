package net.safemoon.androidwallet.model.request;

import androidx.annotation.Keep;
import com.google.android.gms.common.annotation.KeepName;
import com.google.gson.Gson;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import java.io.Serializable;
import java.util.HashMap;

/* compiled from: RequestTransaction.kt */
@Keep
@KeepName
/* loaded from: classes2.dex */
public final class RequestTransaction implements Serializable {
    @SerializedName("amount")
    @Expose
    private final double amount;
    @SerializedName("blockHash")
    @Expose
    private String blockHash;
    @SerializedName("blockNumber")
    @Expose
    private String blockNumber;
    @SerializedName("chainName")
    @Expose
    private final String chainName;
    @SerializedName("from")
    @Expose
    private final String from;
    @SerializedName("gasPrice")
    @Expose
    private final long gasPrice;
    @SerializedName("gasUsed")
    @Expose
    private String gasUsed;
    @SerializedName("requestTime")
    @Expose
    private final String requestTime;
    @SerializedName("status")
    @Expose
    private Integer status;
    @SerializedName("symbol")
    @Expose
    private final String symbol;
    @SerializedName("to")
    @Expose
    private final String to;
    @SerializedName("transactionHash")
    @Expose
    private final String transactionHash;
    @SerializedName("transactionIndex")
    @Expose
    private String transactionIndex;

    public RequestTransaction(String str, String str2, String str3, double d, String str4, long j, String str5, String str6) {
        fs1.f(str, "transactionHash");
        fs1.f(str2, "from");
        fs1.f(str3, "to");
        fs1.f(str4, "symbol");
        fs1.f(str5, "chainName");
        fs1.f(str6, "requestTime");
        this.transactionHash = str;
        this.from = str2;
        this.to = str3;
        this.amount = d;
        this.symbol = str4;
        this.gasPrice = j;
        this.chainName = str5;
        this.requestTime = str6;
    }

    public final double getAmount() {
        return this.amount;
    }

    public final String getBlockHash() {
        return this.blockHash;
    }

    public final String getBlockNumber() {
        return this.blockNumber;
    }

    public final String getChainName() {
        return this.chainName;
    }

    public final String getFrom() {
        return this.from;
    }

    public final long getGasPrice() {
        return this.gasPrice;
    }

    public final String getGasUsed() {
        return this.gasUsed;
    }

    public final String getRequestTime() {
        return this.requestTime;
    }

    public final Integer getStatus() {
        return this.status;
    }

    public final String getSymbol() {
        return this.symbol;
    }

    public final String getTo() {
        return this.to;
    }

    public final String getTransactionHash() {
        return this.transactionHash;
    }

    public final String getTransactionIndex() {
        return this.transactionIndex;
    }

    public final void setBlockHash(String str) {
        this.blockHash = str;
    }

    public final void setBlockNumber(String str) {
        this.blockNumber = str;
    }

    public final void setGasUsed(String str) {
        this.gasUsed = str;
    }

    public final void setStatus(Integer num) {
        this.status = num;
    }

    public final void setTransactionIndex(String str) {
        this.transactionIndex = str;
    }

    public final HashMap<?, ?> toHashMap() {
        return (HashMap) new Gson().fromJson(toString(), (Class<Object>) HashMap.class);
    }

    public String toString() {
        String json = new Gson().toJson(this);
        fs1.e(json, "Gson().toJson(this)");
        return json;
    }
}
