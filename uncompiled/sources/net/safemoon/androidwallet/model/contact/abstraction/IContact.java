package net.safemoon.androidwallet.model.contact.abstraction;

import java.io.Serializable;

/* compiled from: IContact.kt */
/* loaded from: classes2.dex */
public interface IContact extends Serializable {
    String getAddress();

    int getChainAddress();

    Long getContactCreate();

    int getId();

    Long getLastSent();

    String getName();

    String getProfilePath();

    void setAddress(String str);

    void setChainAddress(int i);

    void setContactCreate(Long l);

    void setId(int i);

    void setLastSent(Long l);

    void setName(String str);

    void setProfilePath(String str);
}
