package net.safemoon.androidwallet.model.contact.room;

import net.safemoon.androidwallet.model.contact.abstraction.IContact;
import org.web3j.abi.datatypes.Address;
import org.web3j.ens.contracts.generated.PublicResolver;

/* compiled from: RoomContact.kt */
/* loaded from: classes2.dex */
public final class RoomContact implements IContact {
    private String address;
    private int chainAddress;
    private Long contactCreate;
    private int id;
    private Long lastSent;
    private String name;
    private String profilePath;

    public RoomContact(int i, String str, String str2, int i2, String str3, Long l, Long l2) {
        fs1.f(str, PublicResolver.FUNC_NAME);
        fs1.f(str2, Address.TYPE_NAME);
        fs1.f(str3, "profilePath");
        this.id = i;
        this.name = str;
        this.address = str2;
        this.chainAddress = i2;
        this.profilePath = str3;
        this.lastSent = l;
        this.contactCreate = l2;
    }

    public static /* synthetic */ RoomContact copy$default(RoomContact roomContact, int i, String str, String str2, int i2, String str3, Long l, Long l2, int i3, Object obj) {
        if ((i3 & 1) != 0) {
            i = roomContact.getId();
        }
        if ((i3 & 2) != 0) {
            str = roomContact.getName();
        }
        String str4 = str;
        if ((i3 & 4) != 0) {
            str2 = roomContact.getAddress();
        }
        String str5 = str2;
        if ((i3 & 8) != 0) {
            i2 = roomContact.getChainAddress();
        }
        int i4 = i2;
        if ((i3 & 16) != 0) {
            str3 = roomContact.getProfilePath();
        }
        String str6 = str3;
        if ((i3 & 32) != 0) {
            l = roomContact.getLastSent();
        }
        Long l3 = l;
        if ((i3 & 64) != 0) {
            l2 = roomContact.getContactCreate();
        }
        return roomContact.copy(i, str4, str5, i4, str6, l3, l2);
    }

    public final int component1() {
        return getId();
    }

    public final String component2() {
        return getName();
    }

    public final String component3() {
        return getAddress();
    }

    public final int component4() {
        return getChainAddress();
    }

    public final String component5() {
        return getProfilePath();
    }

    public final Long component6() {
        return getLastSent();
    }

    public final Long component7() {
        return getContactCreate();
    }

    public final RoomContact copy(int i, String str, String str2, int i2, String str3, Long l, Long l2) {
        fs1.f(str, PublicResolver.FUNC_NAME);
        fs1.f(str2, Address.TYPE_NAME);
        fs1.f(str3, "profilePath");
        return new RoomContact(i, str, str2, i2, str3, l, l2);
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj instanceof RoomContact) {
            RoomContact roomContact = (RoomContact) obj;
            return getId() == roomContact.getId() && fs1.b(getName(), roomContact.getName()) && fs1.b(getAddress(), roomContact.getAddress()) && getChainAddress() == roomContact.getChainAddress() && fs1.b(getProfilePath(), roomContact.getProfilePath()) && fs1.b(getLastSent(), roomContact.getLastSent()) && fs1.b(getContactCreate(), roomContact.getContactCreate());
        }
        return false;
    }

    @Override // net.safemoon.androidwallet.model.contact.abstraction.IContact
    public String getAddress() {
        return this.address;
    }

    @Override // net.safemoon.androidwallet.model.contact.abstraction.IContact
    public int getChainAddress() {
        return this.chainAddress;
    }

    @Override // net.safemoon.androidwallet.model.contact.abstraction.IContact
    public Long getContactCreate() {
        return this.contactCreate;
    }

    @Override // net.safemoon.androidwallet.model.contact.abstraction.IContact
    public int getId() {
        return this.id;
    }

    @Override // net.safemoon.androidwallet.model.contact.abstraction.IContact
    public Long getLastSent() {
        return this.lastSent;
    }

    @Override // net.safemoon.androidwallet.model.contact.abstraction.IContact
    public String getName() {
        return this.name;
    }

    @Override // net.safemoon.androidwallet.model.contact.abstraction.IContact
    public String getProfilePath() {
        return this.profilePath;
    }

    public int hashCode() {
        return (((((((((((getId() * 31) + getName().hashCode()) * 31) + getAddress().hashCode()) * 31) + getChainAddress()) * 31) + getProfilePath().hashCode()) * 31) + (getLastSent() == null ? 0 : getLastSent().hashCode())) * 31) + (getContactCreate() != null ? getContactCreate().hashCode() : 0);
    }

    @Override // net.safemoon.androidwallet.model.contact.abstraction.IContact
    public void setAddress(String str) {
        fs1.f(str, "<set-?>");
        this.address = str;
    }

    @Override // net.safemoon.androidwallet.model.contact.abstraction.IContact
    public void setChainAddress(int i) {
        this.chainAddress = i;
    }

    @Override // net.safemoon.androidwallet.model.contact.abstraction.IContact
    public void setContactCreate(Long l) {
        this.contactCreate = l;
    }

    @Override // net.safemoon.androidwallet.model.contact.abstraction.IContact
    public void setId(int i) {
        this.id = i;
    }

    @Override // net.safemoon.androidwallet.model.contact.abstraction.IContact
    public void setLastSent(Long l) {
        this.lastSent = l;
    }

    @Override // net.safemoon.androidwallet.model.contact.abstraction.IContact
    public void setName(String str) {
        fs1.f(str, "<set-?>");
        this.name = str;
    }

    @Override // net.safemoon.androidwallet.model.contact.abstraction.IContact
    public void setProfilePath(String str) {
        fs1.f(str, "<set-?>");
        this.profilePath = str;
    }

    public String toString() {
        return "RoomContact(id=" + getId() + ", name=" + getName() + ", address=" + getAddress() + ", chainAddress=" + getChainAddress() + ", profilePath=" + getProfilePath() + ", lastSent=" + getLastSent() + ", contactCreate=" + getContactCreate() + ')';
    }

    public /* synthetic */ RoomContact(int i, String str, String str2, int i2, String str3, Long l, Long l2, int i3, qi0 qi0Var) {
        this((i3 & 1) != 0 ? 0 : i, str, str2, i2, str3, l, l2);
    }

    /* JADX WARN: 'this' call moved to the top of the method (can break code semantics) */
    public RoomContact(IContact iContact) {
        this(iContact.getId(), iContact.getName(), iContact.getAddress(), iContact.getChainAddress(), iContact.getProfilePath(), iContact.getLastSent(), iContact.getContactCreate());
        fs1.f(iContact, "origin");
    }
}
