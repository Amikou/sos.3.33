package net.safemoon.androidwallet.model.contact;

import android.net.Uri;
import java.util.List;

/* compiled from: RequestContact.kt */
/* loaded from: classes2.dex */
public final class RequestContact {
    private String address;
    private Long contactCreate;
    private int id;
    private Long lastSent;
    private String name;
    private List<Integer> networks = b20.g();
    private String oldProfilePath;
    private Uri profilePath;

    public final String getAddress() {
        return this.address;
    }

    public final Long getContactCreate() {
        return this.contactCreate;
    }

    public final int getId() {
        return this.id;
    }

    public final Long getLastSent() {
        return this.lastSent;
    }

    public final String getName() {
        return this.name;
    }

    public final List<Integer> getNetworks() {
        return this.networks;
    }

    public final String getOldProfilePath() {
        return this.oldProfilePath;
    }

    public final Uri getProfilePath() {
        return this.profilePath;
    }

    public final void setAddress(String str) {
        this.address = str;
    }

    public final void setContactCreate(Long l) {
        this.contactCreate = l;
    }

    public final void setId(int i) {
        this.id = i;
    }

    public final void setLastSent(Long l) {
        this.lastSent = l;
    }

    public final void setName(String str) {
        this.name = str;
    }

    public final void setNetworks(List<Integer> list) {
        fs1.f(list, "<set-?>");
        this.networks = list;
    }

    public final void setOldProfilePath(String str) {
        this.oldProfilePath = str;
    }

    public final void setProfilePath(Uri uri) {
        this.profilePath = uri;
    }
}
