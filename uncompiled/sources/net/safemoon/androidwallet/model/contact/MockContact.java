package net.safemoon.androidwallet.model.contact;

import net.safemoon.androidwallet.model.contact.abstraction.IContact;
import org.web3j.abi.datatypes.Address;
import org.web3j.ens.contracts.generated.PublicResolver;

/* compiled from: MockContact.kt */
/* loaded from: classes2.dex */
public final class MockContact implements IContact {
    private String address;
    private int chainAddress;
    private Long contactCreate;
    private int id;
    private Long lastSent;
    private String name;
    private String profilePath;

    public MockContact() {
        this(0, null, null, 0, null, null, null, 127, null);
    }

    public MockContact(int i, String str, String str2, int i2, String str3, Long l, Long l2) {
        fs1.f(str, PublicResolver.FUNC_NAME);
        fs1.f(str2, Address.TYPE_NAME);
        fs1.f(str3, "profilePath");
        this.id = i;
        this.name = str;
        this.address = str2;
        this.chainAddress = i2;
        this.profilePath = str3;
        this.lastSent = l;
        this.contactCreate = l2;
    }

    public static /* synthetic */ MockContact copy$default(MockContact mockContact, int i, String str, String str2, int i2, String str3, Long l, Long l2, int i3, Object obj) {
        if ((i3 & 1) != 0) {
            i = mockContact.getId();
        }
        if ((i3 & 2) != 0) {
            str = mockContact.getName();
        }
        String str4 = str;
        if ((i3 & 4) != 0) {
            str2 = mockContact.getAddress();
        }
        String str5 = str2;
        if ((i3 & 8) != 0) {
            i2 = mockContact.getChainAddress();
        }
        int i4 = i2;
        if ((i3 & 16) != 0) {
            str3 = mockContact.getProfilePath();
        }
        String str6 = str3;
        if ((i3 & 32) != 0) {
            l = mockContact.getLastSent();
        }
        Long l3 = l;
        if ((i3 & 64) != 0) {
            l2 = mockContact.getContactCreate();
        }
        return mockContact.copy(i, str4, str5, i4, str6, l3, l2);
    }

    public final int component1() {
        return getId();
    }

    public final String component2() {
        return getName();
    }

    public final String component3() {
        return getAddress();
    }

    public final int component4() {
        return getChainAddress();
    }

    public final String component5() {
        return getProfilePath();
    }

    public final Long component6() {
        return getLastSent();
    }

    public final Long component7() {
        return getContactCreate();
    }

    public final MockContact copy(int i, String str, String str2, int i2, String str3, Long l, Long l2) {
        fs1.f(str, PublicResolver.FUNC_NAME);
        fs1.f(str2, Address.TYPE_NAME);
        fs1.f(str3, "profilePath");
        return new MockContact(i, str, str2, i2, str3, l, l2);
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj instanceof MockContact) {
            MockContact mockContact = (MockContact) obj;
            return getId() == mockContact.getId() && fs1.b(getName(), mockContact.getName()) && fs1.b(getAddress(), mockContact.getAddress()) && getChainAddress() == mockContact.getChainAddress() && fs1.b(getProfilePath(), mockContact.getProfilePath()) && fs1.b(getLastSent(), mockContact.getLastSent()) && fs1.b(getContactCreate(), mockContact.getContactCreate());
        }
        return false;
    }

    @Override // net.safemoon.androidwallet.model.contact.abstraction.IContact
    public String getAddress() {
        return this.address;
    }

    @Override // net.safemoon.androidwallet.model.contact.abstraction.IContact
    public int getChainAddress() {
        return this.chainAddress;
    }

    @Override // net.safemoon.androidwallet.model.contact.abstraction.IContact
    public Long getContactCreate() {
        return this.contactCreate;
    }

    @Override // net.safemoon.androidwallet.model.contact.abstraction.IContact
    public int getId() {
        return this.id;
    }

    @Override // net.safemoon.androidwallet.model.contact.abstraction.IContact
    public Long getLastSent() {
        return this.lastSent;
    }

    @Override // net.safemoon.androidwallet.model.contact.abstraction.IContact
    public String getName() {
        return this.name;
    }

    @Override // net.safemoon.androidwallet.model.contact.abstraction.IContact
    public String getProfilePath() {
        return this.profilePath;
    }

    public int hashCode() {
        return (((((((((((getId() * 31) + getName().hashCode()) * 31) + getAddress().hashCode()) * 31) + getChainAddress()) * 31) + getProfilePath().hashCode()) * 31) + (getLastSent() == null ? 0 : getLastSent().hashCode())) * 31) + (getContactCreate() != null ? getContactCreate().hashCode() : 0);
    }

    @Override // net.safemoon.androidwallet.model.contact.abstraction.IContact
    public void setAddress(String str) {
        fs1.f(str, "<set-?>");
        this.address = str;
    }

    @Override // net.safemoon.androidwallet.model.contact.abstraction.IContact
    public void setChainAddress(int i) {
        this.chainAddress = i;
    }

    @Override // net.safemoon.androidwallet.model.contact.abstraction.IContact
    public void setContactCreate(Long l) {
        this.contactCreate = l;
    }

    @Override // net.safemoon.androidwallet.model.contact.abstraction.IContact
    public void setId(int i) {
        this.id = i;
    }

    @Override // net.safemoon.androidwallet.model.contact.abstraction.IContact
    public void setLastSent(Long l) {
        this.lastSent = l;
    }

    @Override // net.safemoon.androidwallet.model.contact.abstraction.IContact
    public void setName(String str) {
        fs1.f(str, "<set-?>");
        this.name = str;
    }

    @Override // net.safemoon.androidwallet.model.contact.abstraction.IContact
    public void setProfilePath(String str) {
        fs1.f(str, "<set-?>");
        this.profilePath = str;
    }

    public String toString() {
        return "MockContact(id=" + getId() + ", name=" + getName() + ", address=" + getAddress() + ", chainAddress=" + getChainAddress() + ", profilePath=" + getProfilePath() + ", lastSent=" + getLastSent() + ", contactCreate=" + getContactCreate() + ')';
    }

    public /* synthetic */ MockContact(int i, String str, String str2, int i2, String str3, Long l, Long l2, int i3, qi0 qi0Var) {
        this((i3 & 1) != 0 ? 0 : i, (i3 & 2) != 0 ? "" : str, (i3 & 4) != 0 ? "" : str2, (i3 & 8) == 0 ? i2 : 0, (i3 & 16) == 0 ? str3 : "", (i3 & 32) != 0 ? 0L : l, (i3 & 64) != 0 ? 0L : l2);
    }
}
