package net.safemoon.androidwallet.model.priceAlert;

import androidx.recyclerview.widget.RecyclerView;
import com.google.gson.annotations.SerializedName;
import java.math.BigDecimal;
import okhttp3.internal.http2.Http2;
import org.web3j.abi.datatypes.Address;
import org.web3j.ens.contracts.generated.PublicResolver;

/* compiled from: PriceAlertToken.kt */
/* loaded from: classes2.dex */
public final class PriceAlertToken {
    private String cacheDecreasePercentString;
    private String cacheIncreasePercentString;
    private String cachePriceReachesString;
    private int cacheRepeat;
    @SerializedName("chainId")
    private Integer chainId;
    @SerializedName("currencyPrice")
    private Double currencyPrice;
    @SerializedName("currencySymbol")
    private String currencySymbol;
    @SerializedName("decreasePercent")
    private Double decreasePercent;
    private String decreasePercentString;
    @SerializedName("enableDecreasePercent")
    private boolean enableDecreasePercent;
    @SerializedName("enableIncreasePercent")
    private boolean enableIncreasePercent;
    @SerializedName("fcmToken")
    private String fcmToken;
    @SerializedName("id")
    private Integer id;
    @SerializedName("increasePercent")
    private Double increasePercent;
    private String increasePercentString;
    private boolean isPersistent;
    @SerializedName("platform")
    private final String platform;
    @SerializedName("price")
    private Double price;
    @SerializedName("priceReaches")
    private Double priceReaches;
    @SerializedName("priceReachesOrHigher")
    private boolean priceReachesOrHigher;
    @SerializedName("priceReachesOrLower")
    private boolean priceReachesOrLower;
    private String priceReachesString;
    @SerializedName("repeat")
    private int repeat;
    @SerializedName("status")
    private String status;
    @SerializedName(alternate = {Address.TYPE_NAME}, value = "tokenAddress")
    private String tokenAddress;
    @SerializedName(alternate = {"coinmarketid", "coinMarketId"}, value = "tokenId")
    private String tokenId;
    @SerializedName(alternate = {PublicResolver.FUNC_NAME}, value = "tokenName")
    private String tokenName;
    @SerializedName(alternate = {"symbol"}, value = "tokenSymbol")
    private String tokenSymbol;
    @SerializedName("walletAddress")
    private String walletAddress;

    public PriceAlertToken() {
        this(null, null, null, null, null, null, null, null, null, false, false, null, false, null, false, null, null, null, 0, null, null, 2097151, null);
    }

    public PriceAlertToken(Integer num, String str, String str2, String str3, String str4, Integer num2, String str5, String str6, String str7, boolean z, boolean z2, Double d, boolean z3, Double d2, boolean z4, Double d3, Double d4, String str8, int i, String str9, Double d5) {
        fs1.f(str7, "status");
        fs1.f(str8, "platform");
        this.id = num;
        this.walletAddress = str;
        this.tokenSymbol = str2;
        this.tokenName = str3;
        this.tokenAddress = str4;
        this.chainId = num2;
        this.tokenId = str5;
        this.fcmToken = str6;
        this.status = str7;
        this.priceReachesOrHigher = z;
        this.priceReachesOrLower = z2;
        this.priceReaches = d;
        this.enableIncreasePercent = z3;
        this.increasePercent = d2;
        this.enableDecreasePercent = z4;
        this.decreasePercent = d3;
        this.price = d4;
        this.platform = str8;
        this.repeat = i;
        this.currencySymbol = str9;
        this.currencyPrice = d5;
        this.cacheRepeat = 1;
        this.priceReachesString = "  ";
        this.increasePercentString = "";
        this.decreasePercentString = "";
    }

    public final Integer component1() {
        return this.id;
    }

    public final boolean component10() {
        return this.priceReachesOrHigher;
    }

    public final boolean component11() {
        return this.priceReachesOrLower;
    }

    public final Double component12() {
        return this.priceReaches;
    }

    public final boolean component13() {
        return this.enableIncreasePercent;
    }

    public final Double component14() {
        return this.increasePercent;
    }

    public final boolean component15() {
        return this.enableDecreasePercent;
    }

    public final Double component16() {
        return this.decreasePercent;
    }

    public final Double component17() {
        return this.price;
    }

    public final String component18() {
        return this.platform;
    }

    public final int component19() {
        return this.repeat;
    }

    public final String component2() {
        return this.walletAddress;
    }

    public final String component20() {
        return this.currencySymbol;
    }

    public final Double component21() {
        return this.currencyPrice;
    }

    public final String component3() {
        return this.tokenSymbol;
    }

    public final String component4() {
        return this.tokenName;
    }

    public final String component5() {
        return this.tokenAddress;
    }

    public final Integer component6() {
        return this.chainId;
    }

    public final String component7() {
        return this.tokenId;
    }

    public final String component8() {
        return this.fcmToken;
    }

    public final String component9() {
        return this.status;
    }

    public final PriceAlertToken copy(Integer num, String str, String str2, String str3, String str4, Integer num2, String str5, String str6, String str7, boolean z, boolean z2, Double d, boolean z3, Double d2, boolean z4, Double d3, Double d4, String str8, int i, String str9, Double d5) {
        fs1.f(str7, "status");
        fs1.f(str8, "platform");
        return new PriceAlertToken(num, str, str2, str3, str4, num2, str5, str6, str7, z, z2, d, z3, d2, z4, d3, d4, str8, i, str9, d5);
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj instanceof PriceAlertToken) {
            PriceAlertToken priceAlertToken = (PriceAlertToken) obj;
            return fs1.b(this.id, priceAlertToken.id) && fs1.b(this.walletAddress, priceAlertToken.walletAddress) && fs1.b(this.tokenSymbol, priceAlertToken.tokenSymbol) && fs1.b(this.tokenName, priceAlertToken.tokenName) && fs1.b(this.tokenAddress, priceAlertToken.tokenAddress) && fs1.b(this.chainId, priceAlertToken.chainId) && fs1.b(this.tokenId, priceAlertToken.tokenId) && fs1.b(this.fcmToken, priceAlertToken.fcmToken) && fs1.b(this.status, priceAlertToken.status) && this.priceReachesOrHigher == priceAlertToken.priceReachesOrHigher && this.priceReachesOrLower == priceAlertToken.priceReachesOrLower && fs1.b(this.priceReaches, priceAlertToken.priceReaches) && this.enableIncreasePercent == priceAlertToken.enableIncreasePercent && fs1.b(this.increasePercent, priceAlertToken.increasePercent) && this.enableDecreasePercent == priceAlertToken.enableDecreasePercent && fs1.b(this.decreasePercent, priceAlertToken.decreasePercent) && fs1.b(this.price, priceAlertToken.price) && fs1.b(this.platform, priceAlertToken.platform) && this.repeat == priceAlertToken.repeat && fs1.b(this.currencySymbol, priceAlertToken.currencySymbol) && fs1.b(this.currencyPrice, priceAlertToken.currencyPrice);
        }
        return false;
    }

    public final String getCacheDecreasePercentString() {
        return this.cacheDecreasePercentString;
    }

    public final String getCacheIncreasePercentString() {
        return this.cacheIncreasePercentString;
    }

    public final String getCachePriceReachesString() {
        return this.cachePriceReachesString;
    }

    public final int getCacheRepeat() {
        return this.cacheRepeat;
    }

    public final Integer getChainId() {
        return this.chainId;
    }

    public final Double getCurrencyPrice() {
        return this.currencyPrice;
    }

    public final String getCurrencySymbol() {
        return this.currencySymbol;
    }

    public final Double getDecreasePercent() {
        return this.decreasePercent;
    }

    public final String getDecreasePercentString() {
        String g0;
        String str = this.cacheDecreasePercentString;
        if (str == null) {
            Double d = this.decreasePercent;
            return (d == null || (g0 = e30.g0(new BigDecimal(String.valueOf(d.doubleValue())), 18)) == null) ? "" : g0;
        }
        return str;
    }

    public final boolean getEnableDecreasePercent() {
        return this.enableDecreasePercent;
    }

    public final boolean getEnableIncreasePercent() {
        return this.enableIncreasePercent;
    }

    public final String getFcmToken() {
        return this.fcmToken;
    }

    public final Integer getId() {
        return this.id;
    }

    public final Double getIncreasePercent() {
        return this.increasePercent;
    }

    public final String getIncreasePercentString() {
        String g0;
        String str = this.cacheIncreasePercentString;
        if (str == null) {
            Double d = this.increasePercent;
            return (d == null || (g0 = e30.g0(new BigDecimal(String.valueOf(d.doubleValue())), 18)) == null) ? "" : g0;
        }
        return str;
    }

    public final String getPlatform() {
        return this.platform;
    }

    public final Double getPrice() {
        return this.price;
    }

    public final Double getPriceReaches() {
        return this.priceReaches;
    }

    public final boolean getPriceReachesOrHigher() {
        return this.priceReachesOrHigher;
    }

    public final boolean getPriceReachesOrLower() {
        return this.priceReachesOrLower;
    }

    public final String getPriceReachesString() {
        String g0;
        String str = this.cachePriceReachesString;
        if (str == null) {
            Double d = this.priceReaches;
            return (d == null || (g0 = e30.g0(new BigDecimal(String.valueOf(d.doubleValue())), 18)) == null) ? "" : g0;
        }
        return str;
    }

    public final int getRepeat() {
        return this.repeat;
    }

    public final String getStatus() {
        return this.status;
    }

    public final String getTokenAddress() {
        return this.tokenAddress;
    }

    public final String getTokenId() {
        return this.tokenId;
    }

    public final String getTokenName() {
        return this.tokenName;
    }

    public final String getTokenSymbol() {
        return this.tokenSymbol;
    }

    public final String getWalletAddress() {
        return this.walletAddress;
    }

    /* JADX WARN: Multi-variable type inference failed */
    public int hashCode() {
        Integer num = this.id;
        int hashCode = (num == null ? 0 : num.hashCode()) * 31;
        String str = this.walletAddress;
        int hashCode2 = (hashCode + (str == null ? 0 : str.hashCode())) * 31;
        String str2 = this.tokenSymbol;
        int hashCode3 = (hashCode2 + (str2 == null ? 0 : str2.hashCode())) * 31;
        String str3 = this.tokenName;
        int hashCode4 = (hashCode3 + (str3 == null ? 0 : str3.hashCode())) * 31;
        String str4 = this.tokenAddress;
        int hashCode5 = (hashCode4 + (str4 == null ? 0 : str4.hashCode())) * 31;
        Integer num2 = this.chainId;
        int hashCode6 = (hashCode5 + (num2 == null ? 0 : num2.hashCode())) * 31;
        String str5 = this.tokenId;
        int hashCode7 = (hashCode6 + (str5 == null ? 0 : str5.hashCode())) * 31;
        String str6 = this.fcmToken;
        int hashCode8 = (((hashCode7 + (str6 == null ? 0 : str6.hashCode())) * 31) + this.status.hashCode()) * 31;
        boolean z = this.priceReachesOrHigher;
        int i = z;
        if (z != 0) {
            i = 1;
        }
        int i2 = (hashCode8 + i) * 31;
        boolean z2 = this.priceReachesOrLower;
        int i3 = z2;
        if (z2 != 0) {
            i3 = 1;
        }
        int i4 = (i2 + i3) * 31;
        Double d = this.priceReaches;
        int hashCode9 = (i4 + (d == null ? 0 : d.hashCode())) * 31;
        boolean z3 = this.enableIncreasePercent;
        int i5 = z3;
        if (z3 != 0) {
            i5 = 1;
        }
        int i6 = (hashCode9 + i5) * 31;
        Double d2 = this.increasePercent;
        int hashCode10 = (i6 + (d2 == null ? 0 : d2.hashCode())) * 31;
        boolean z4 = this.enableDecreasePercent;
        int i7 = (hashCode10 + (z4 ? 1 : z4 ? 1 : 0)) * 31;
        Double d3 = this.decreasePercent;
        int hashCode11 = (i7 + (d3 == null ? 0 : d3.hashCode())) * 31;
        Double d4 = this.price;
        int hashCode12 = (((((hashCode11 + (d4 == null ? 0 : d4.hashCode())) * 31) + this.platform.hashCode()) * 31) + this.repeat) * 31;
        String str7 = this.currencySymbol;
        int hashCode13 = (hashCode12 + (str7 == null ? 0 : str7.hashCode())) * 31;
        Double d5 = this.currencyPrice;
        return hashCode13 + (d5 != null ? d5.hashCode() : 0);
    }

    public final boolean isPersistent() {
        return this.repeat > 0;
    }

    public final boolean isStatus() {
        return fs1.b(this.status, "OPEN");
    }

    public final void setCacheDecreasePercentString(String str) {
        this.cacheDecreasePercentString = str;
    }

    public final void setCacheIncreasePercentString(String str) {
        this.cacheIncreasePercentString = str;
    }

    public final void setCachePriceReachesString(String str) {
        this.cachePriceReachesString = str;
    }

    public final void setCacheRepeat(int i) {
        this.cacheRepeat = i;
    }

    public final void setChainId(Integer num) {
        this.chainId = num;
    }

    public final void setCurrencyPrice(Double d) {
        this.currencyPrice = d;
    }

    public final void setCurrencySymbol(String str) {
        this.currencySymbol = str;
    }

    public final void setDecreasePercent(Double d) {
        this.decreasePercent = d;
    }

    public final void setDecreasePercentString(String str) {
        fs1.f(str, "value");
        this.cacheDecreasePercentString = str;
        BigDecimal L = e30.L(str);
        this.decreasePercent = L == null ? null : Double.valueOf(L.doubleValue());
        this.decreasePercentString = str;
    }

    public final void setEnableDecreasePercent(boolean z) {
        this.enableDecreasePercent = z;
    }

    public final void setEnableIncreasePercent(boolean z) {
        this.enableIncreasePercent = z;
    }

    public final void setFcmToken(String str) {
        this.fcmToken = str;
    }

    public final void setId(Integer num) {
        this.id = num;
    }

    public final void setIncreasePercent(Double d) {
        this.increasePercent = d;
    }

    public final void setIncreasePercentString(String str) {
        fs1.f(str, "value");
        this.cacheIncreasePercentString = str;
        BigDecimal L = e30.L(str);
        this.increasePercent = L == null ? null : Double.valueOf(L.doubleValue());
        this.increasePercentString = str;
    }

    public final void setPersistent(boolean z) {
        if (z) {
            this.repeat = this.cacheRepeat;
        } else {
            this.cacheRepeat = this.repeat;
            this.repeat = 0;
        }
        this.isPersistent = z;
    }

    public final void setPrice(Double d) {
        this.price = d;
    }

    public final void setPriceReaches(Double d) {
        this.priceReaches = d;
    }

    public final void setPriceReachesOrHigher(boolean z) {
        this.priceReachesOrHigher = z;
    }

    public final void setPriceReachesOrLower(boolean z) {
        this.priceReachesOrLower = z;
    }

    public final void setPriceReachesString(String str) {
        fs1.f(str, "value");
        this.cachePriceReachesString = str;
        BigDecimal L = e30.L(str);
        this.priceReaches = L == null ? null : Double.valueOf(L.doubleValue());
        this.priceReachesString = str;
    }

    public final void setRepeat(int i) {
        this.repeat = i;
    }

    public final void setStatus(String str) {
        fs1.f(str, "<set-?>");
        this.status = str;
    }

    public final void setTokenAddress(String str) {
        this.tokenAddress = str;
    }

    public final void setTokenId(String str) {
        this.tokenId = str;
    }

    public final void setTokenName(String str) {
        this.tokenName = str;
    }

    public final void setTokenSymbol(String str) {
        this.tokenSymbol = str;
    }

    public final void setWalletAddress(String str) {
        this.walletAddress = str;
    }

    public String toString() {
        return "PriceAlertToken(id=" + this.id + ", walletAddress=" + ((Object) this.walletAddress) + ", tokenSymbol=" + ((Object) this.tokenSymbol) + ", tokenName=" + ((Object) this.tokenName) + ", tokenAddress=" + ((Object) this.tokenAddress) + ", chainId=" + this.chainId + ", tokenId=" + ((Object) this.tokenId) + ", fcmToken=" + ((Object) this.fcmToken) + ", status=" + this.status + ", priceReachesOrHigher=" + this.priceReachesOrHigher + ", priceReachesOrLower=" + this.priceReachesOrLower + ", priceReaches=" + this.priceReaches + ", enableIncreasePercent=" + this.enableIncreasePercent + ", increasePercent=" + this.increasePercent + ", enableDecreasePercent=" + this.enableDecreasePercent + ", decreasePercent=" + this.decreasePercent + ", price=" + this.price + ", platform=" + this.platform + ", repeat=" + this.repeat + ", currencySymbol=" + ((Object) this.currencySymbol) + ", currencyPrice=" + this.currencyPrice + ')';
    }

    public final void setStatus(boolean z) {
        this.status = z ? "OPEN" : "CLOSED";
    }

    public /* synthetic */ PriceAlertToken(Integer num, String str, String str2, String str3, String str4, Integer num2, String str5, String str6, String str7, boolean z, boolean z2, Double d, boolean z3, Double d2, boolean z4, Double d3, Double d4, String str8, int i, String str9, Double d5, int i2, qi0 qi0Var) {
        this((i2 & 1) != 0 ? null : num, (i2 & 2) != 0 ? null : str, (i2 & 4) != 0 ? null : str2, (i2 & 8) != 0 ? null : str3, (i2 & 16) != 0 ? null : str4, (i2 & 32) != 0 ? null : num2, (i2 & 64) != 0 ? null : str5, (i2 & 128) != 0 ? null : str6, (i2 & 256) != 0 ? "CLOSED" : str7, (i2 & RecyclerView.a0.FLAG_ADAPTER_POSITION_UNKNOWN) != 0 ? false : z, (i2 & RecyclerView.a0.FLAG_ADAPTER_FULLUPDATE) != 0 ? false : z2, (i2 & 2048) != 0 ? null : d, (i2 & 4096) != 0 ? false : z3, (i2 & 8192) != 0 ? null : d2, (i2 & Http2.INITIAL_MAX_FRAME_SIZE) != 0 ? false : z4, (i2 & 32768) != 0 ? null : d3, (i2 & 65536) != 0 ? null : d4, (i2 & 131072) != 0 ? "ANDROID" : str8, (i2 & 262144) != 0 ? 0 : i, (i2 & 524288) != 0 ? null : str9, (i2 & 1048576) != 0 ? null : d5);
    }
}
