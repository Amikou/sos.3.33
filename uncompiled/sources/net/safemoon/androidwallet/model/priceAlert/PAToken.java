package net.safemoon.androidwallet.model.priceAlert;

import java.io.Serializable;
import org.web3j.ens.contracts.generated.PublicResolver;

/* compiled from: PAToken.kt */
/* loaded from: classes2.dex */
public final class PAToken implements Serializable {
    private final int chainId;
    private final Integer cmcId;
    private final String contractAddress;
    private boolean hasPriceAlert;
    private final Object icon;
    private final String name;
    private final String symbol;

    public PAToken(int i, String str, String str2, String str3, Integer num, Object obj, boolean z) {
        fs1.f(str, PublicResolver.FUNC_NAME);
        this.chainId = i;
        this.name = str;
        this.symbol = str2;
        this.contractAddress = str3;
        this.cmcId = num;
        this.icon = obj;
        this.hasPriceAlert = z;
    }

    public static /* synthetic */ PAToken copy$default(PAToken pAToken, int i, String str, String str2, String str3, Integer num, Object obj, boolean z, int i2, Object obj2) {
        if ((i2 & 1) != 0) {
            i = pAToken.chainId;
        }
        if ((i2 & 2) != 0) {
            str = pAToken.name;
        }
        String str4 = str;
        if ((i2 & 4) != 0) {
            str2 = pAToken.symbol;
        }
        String str5 = str2;
        if ((i2 & 8) != 0) {
            str3 = pAToken.contractAddress;
        }
        String str6 = str3;
        if ((i2 & 16) != 0) {
            num = pAToken.cmcId;
        }
        Integer num2 = num;
        if ((i2 & 32) != 0) {
            obj = pAToken.icon;
        }
        Object obj3 = obj;
        if ((i2 & 64) != 0) {
            z = pAToken.hasPriceAlert;
        }
        return pAToken.copy(i, str4, str5, str6, num2, obj3, z);
    }

    public final int component1() {
        return this.chainId;
    }

    public final String component2() {
        return this.name;
    }

    public final String component3() {
        return this.symbol;
    }

    public final String component4() {
        return this.contractAddress;
    }

    public final Integer component5() {
        return this.cmcId;
    }

    public final Object component6() {
        return this.icon;
    }

    public final boolean component7() {
        return this.hasPriceAlert;
    }

    public final PAToken copy(int i, String str, String str2, String str3, Integer num, Object obj, boolean z) {
        fs1.f(str, PublicResolver.FUNC_NAME);
        return new PAToken(i, str, str2, str3, num, obj, z);
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj instanceof PAToken) {
            PAToken pAToken = (PAToken) obj;
            return this.chainId == pAToken.chainId && fs1.b(this.name, pAToken.name) && fs1.b(this.symbol, pAToken.symbol) && fs1.b(this.contractAddress, pAToken.contractAddress) && fs1.b(this.cmcId, pAToken.cmcId) && fs1.b(this.icon, pAToken.icon) && this.hasPriceAlert == pAToken.hasPriceAlert;
        }
        return false;
    }

    public final int getChainId() {
        return this.chainId;
    }

    public final Integer getCmcId() {
        return this.cmcId;
    }

    public final String getContractAddress() {
        return this.contractAddress;
    }

    public final boolean getHasPriceAlert() {
        return this.hasPriceAlert;
    }

    public final Object getIcon() {
        return this.icon;
    }

    public final String getName() {
        return this.name;
    }

    public final String getSymbol() {
        return this.symbol;
    }

    /* JADX WARN: Multi-variable type inference failed */
    public int hashCode() {
        int hashCode = ((this.chainId * 31) + this.name.hashCode()) * 31;
        String str = this.symbol;
        int hashCode2 = (hashCode + (str == null ? 0 : str.hashCode())) * 31;
        String str2 = this.contractAddress;
        int hashCode3 = (hashCode2 + (str2 == null ? 0 : str2.hashCode())) * 31;
        Integer num = this.cmcId;
        int hashCode4 = (hashCode3 + (num == null ? 0 : num.hashCode())) * 31;
        Object obj = this.icon;
        int hashCode5 = (hashCode4 + (obj != null ? obj.hashCode() : 0)) * 31;
        boolean z = this.hasPriceAlert;
        int i = z;
        if (z != 0) {
            i = 1;
        }
        return hashCode5 + i;
    }

    public final void setHasPriceAlert(boolean z) {
        this.hasPriceAlert = z;
    }

    public String toString() {
        return "PAToken(chainId=" + this.chainId + ", name=" + this.name + ", symbol=" + ((Object) this.symbol) + ", contractAddress=" + ((Object) this.contractAddress) + ", cmcId=" + this.cmcId + ", icon=" + this.icon + ", hasPriceAlert=" + this.hasPriceAlert + ')';
    }

    public /* synthetic */ PAToken(int i, String str, String str2, String str3, Integer num, Object obj, boolean z, int i2, qi0 qi0Var) {
        this(i, str, str2, (i2 & 8) != 0 ? null : str3, (i2 & 16) != 0 ? null : num, (i2 & 32) != 0 ? null : obj, (i2 & 64) != 0 ? false : z);
    }
}
