package net.safemoon.androidwallet.model.priceAlert;

import com.google.gson.annotations.SerializedName;
import java.util.ArrayList;

/* compiled from: PriceAlertTokenListData.kt */
/* loaded from: classes2.dex */
public final class PriceAlertTokenListData {
    @SerializedName("data")
    private final ArrayList<PriceAlertToken> result;

    public final ArrayList<PriceAlertToken> getResult() {
        return this.result;
    }
}
