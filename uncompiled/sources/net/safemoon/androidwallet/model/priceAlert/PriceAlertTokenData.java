package net.safemoon.androidwallet.model.priceAlert;

import com.google.gson.annotations.SerializedName;

/* compiled from: PriceAlertTokenData.kt */
/* loaded from: classes2.dex */
public final class PriceAlertTokenData {
    @SerializedName("data")
    private final PriceAlertToken result;

    public final PriceAlertToken getResult() {
        return this.result;
    }
}
