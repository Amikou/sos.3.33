package net.safemoon.androidwallet.model.notificationHistory;

import com.google.gson.annotations.SerializedName;

/* loaded from: classes2.dex */
public class NotificationHistory {
    @SerializedName("data")
    private NotificationHistoryData data;

    public NotificationHistoryData getData() {
        return this.data;
    }
}
