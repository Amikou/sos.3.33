package net.safemoon.androidwallet.model.notificationHistory;

import net.safemoon.androidwallet.model.common.HistoryListItem;

/* compiled from: ResultItemHistory.kt */
/* loaded from: classes2.dex */
public final class ResultItemHistory extends HistoryListItem {
    private NotificationHistoryResult result;

    public final NotificationHistoryResult getResult() {
        return this.result;
    }

    @Override // net.safemoon.androidwallet.model.common.HistoryListItem
    public int getType() {
        return 2;
    }

    public final void setResult(NotificationHistoryResult notificationHistoryResult) {
        this.result = notificationHistoryResult;
    }
}
