package net.safemoon.androidwallet.model.notificationHistory;

import com.google.gson.annotations.SerializedName;

/* loaded from: classes2.dex */
public class NotificationHistoryResult {
    @SerializedName("body")
    public String body;
    @SerializedName("createdAt")
    public String createdAt;
    @SerializedName("data")
    public Data data;
    @SerializedName("id")
    public String id;
    @SerializedName("read")
    public boolean read;
    @SerializedName("title")
    public String title;

    /* loaded from: classes2.dex */
    public class Data {
        @SerializedName("status")
        public boolean status;
        @SerializedName("transactionHash")
        public String transactionHash;

        public Data() {
        }
    }
}
