package net.safemoon.androidwallet.model.notificationHistory;

import java.util.List;

/* compiled from: NotificationMarkReadRequest.kt */
/* loaded from: classes2.dex */
public final class NotificationMarkReadRequest {
    private String address;
    private List<String> ids;
    private Boolean readAll;

    public final String getAddress() {
        return this.address;
    }

    public final List<String> getIds() {
        return this.ids;
    }

    public final Boolean getReadAll() {
        return this.readAll;
    }

    public final void setAddress(String str) {
        this.address = str;
    }

    public final void setIds(List<String> list) {
        this.ids = list;
    }

    public final void setReadAll(Boolean bool) {
        this.readAll = bool;
    }
}
