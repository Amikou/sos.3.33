package net.safemoon.androidwallet.model.notificationHistory;

import java.util.List;

/* compiled from: NotificationDeleteRequest.kt */
/* loaded from: classes2.dex */
public final class NotificationDeleteRequest {
    private String address;
    private Boolean deleteAll;
    private List<String> ids;

    public final String getAddress() {
        return this.address;
    }

    public final Boolean getDeleteAll() {
        return this.deleteAll;
    }

    public final List<String> getIds() {
        return this.ids;
    }

    public final void setAddress(String str) {
        this.address = str;
    }

    public final void setDeleteAll(Boolean bool) {
        this.deleteAll = bool;
    }

    public final void setIds(List<String> list) {
        this.ids = list;
    }
}
