package net.safemoon.androidwallet.model;

import com.google.gson.annotations.SerializedName;

/* compiled from: PriceByAddress.kt */
/* loaded from: classes2.dex */
public final class PriceByAddress {
    @SerializedName("priceNative")
    private final String priceNative;
    @SerializedName("priceUsd")
    private final String priceUsd;

    public PriceByAddress(String str, String str2) {
        this.priceNative = str;
        this.priceUsd = str2;
    }

    public static /* synthetic */ PriceByAddress copy$default(PriceByAddress priceByAddress, String str, String str2, int i, Object obj) {
        if ((i & 1) != 0) {
            str = priceByAddress.priceNative;
        }
        if ((i & 2) != 0) {
            str2 = priceByAddress.priceUsd;
        }
        return priceByAddress.copy(str, str2);
    }

    public final String component1() {
        return this.priceNative;
    }

    public final String component2() {
        return this.priceUsd;
    }

    public final PriceByAddress copy(String str, String str2) {
        return new PriceByAddress(str, str2);
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj instanceof PriceByAddress) {
            PriceByAddress priceByAddress = (PriceByAddress) obj;
            return fs1.b(this.priceNative, priceByAddress.priceNative) && fs1.b(this.priceUsd, priceByAddress.priceUsd);
        }
        return false;
    }

    public final String getPriceNative() {
        return this.priceNative;
    }

    public final String getPriceUsd() {
        return this.priceUsd;
    }

    public int hashCode() {
        String str = this.priceNative;
        int hashCode = (str == null ? 0 : str.hashCode()) * 31;
        String str2 = this.priceUsd;
        return hashCode + (str2 != null ? str2.hashCode() : 0);
    }

    public String toString() {
        return "PriceByAddress(priceNative=" + ((Object) this.priceNative) + ", priceUsd=" + ((Object) this.priceUsd) + ')';
    }
}
