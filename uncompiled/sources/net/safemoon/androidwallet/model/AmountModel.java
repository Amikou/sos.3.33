package net.safemoon.androidwallet.model;

import com.google.gson.annotations.SerializedName;
import java.text.DecimalFormat;

/* loaded from: classes2.dex */
public class AmountModel {
    @SerializedName("amount")
    private float amount;

    public float getAmount() {
        return this.amount;
    }

    public String getAmountFormat() {
        return new DecimalFormat("#.##").format(this.amount);
    }

    public void setAmount(float f) {
        this.amount = f;
    }
}
