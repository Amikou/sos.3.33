package net.safemoon.androidwallet.model;

import net.safemoon.androidwallet.common.TokenType;

/* compiled from: MyTokenType.kt */
/* loaded from: classes2.dex */
public final class MyTokenType {
    private boolean isSelect;
    private final TokenType tokenType;

    public MyTokenType(TokenType tokenType, boolean z) {
        fs1.f(tokenType, "tokenType");
        this.tokenType = tokenType;
        this.isSelect = z;
    }

    public static /* synthetic */ MyTokenType copy$default(MyTokenType myTokenType, TokenType tokenType, boolean z, int i, Object obj) {
        if ((i & 1) != 0) {
            tokenType = myTokenType.tokenType;
        }
        if ((i & 2) != 0) {
            z = myTokenType.isSelect;
        }
        return myTokenType.copy(tokenType, z);
    }

    public final TokenType component1() {
        return this.tokenType;
    }

    public final boolean component2() {
        return this.isSelect;
    }

    public final MyTokenType copy(TokenType tokenType, boolean z) {
        fs1.f(tokenType, "tokenType");
        return new MyTokenType(tokenType, z);
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj instanceof MyTokenType) {
            MyTokenType myTokenType = (MyTokenType) obj;
            return this.tokenType == myTokenType.tokenType && this.isSelect == myTokenType.isSelect;
        }
        return false;
    }

    public final TokenType getTokenType() {
        return this.tokenType;
    }

    /* JADX WARN: Multi-variable type inference failed */
    public int hashCode() {
        int hashCode = this.tokenType.hashCode() * 31;
        boolean z = this.isSelect;
        int i = z;
        if (z != 0) {
            i = 1;
        }
        return hashCode + i;
    }

    public final boolean isSelect() {
        return this.isSelect;
    }

    public final void setSelect(boolean z) {
        this.isSelect = z;
    }

    public String toString() {
        return "MyTokenType(tokenType=" + this.tokenType + ", isSelect=" + this.isSelect + ')';
    }
}
