package net.safemoon.androidwallet.model;

import androidx.recyclerview.widget.RecyclerView;
import net.safemoon.androidwallet.model.token.abstraction.IToken;
import org.web3j.ens.contracts.generated.PublicResolver;

/* compiled from: DefaultIToken.kt */
/* loaded from: classes2.dex */
public final class DefaultIToken implements IToken {
    private final boolean allowSwap;
    private final int chainId;
    private final String contractAddress;
    private final int decimals;
    private final String iconResName;
    private final String name;
    private final double nativeBalance;
    private final double percentChange1h;
    private final double priceInUsdt;
    private final String symbol;
    private final String symbolWithType;

    public DefaultIToken(String str, String str2, String str3, String str4, String str5, int i, int i2, boolean z, double d, double d2, double d3) {
        fs1.f(str, "symbolWithType");
        fs1.f(str2, PublicResolver.FUNC_NAME);
        fs1.f(str3, "symbol");
        fs1.f(str4, "iconResName");
        fs1.f(str5, "contractAddress");
        this.symbolWithType = str;
        this.name = str2;
        this.symbol = str3;
        this.iconResName = str4;
        this.contractAddress = str5;
        this.chainId = i;
        this.decimals = i2;
        this.allowSwap = z;
        this.priceInUsdt = d;
        this.nativeBalance = d2;
        this.percentChange1h = d3;
    }

    @Override // net.safemoon.androidwallet.model.token.abstraction.IToken
    public boolean getAllowSwap() {
        return this.allowSwap;
    }

    @Override // net.safemoon.androidwallet.model.token.abstraction.IToken
    public int getChainId() {
        return this.chainId;
    }

    @Override // net.safemoon.androidwallet.model.token.abstraction.IToken
    public String getContractAddress() {
        return this.contractAddress;
    }

    @Override // net.safemoon.androidwallet.model.token.abstraction.IToken
    public int getDecimals() {
        return this.decimals;
    }

    @Override // net.safemoon.androidwallet.model.token.abstraction.IToken
    public String getIconResName() {
        return this.iconResName;
    }

    @Override // net.safemoon.androidwallet.model.token.abstraction.IToken
    public String getName() {
        return this.name;
    }

    @Override // net.safemoon.androidwallet.model.token.abstraction.IToken
    public double getNativeBalance() {
        return this.nativeBalance;
    }

    @Override // net.safemoon.androidwallet.model.token.abstraction.IToken
    public double getPercentChange1h() {
        return this.percentChange1h;
    }

    @Override // net.safemoon.androidwallet.model.token.abstraction.IToken
    public double getPriceInUsdt() {
        return this.priceInUsdt;
    }

    @Override // net.safemoon.androidwallet.model.token.abstraction.IToken
    public String getSymbol() {
        return this.symbol;
    }

    @Override // net.safemoon.androidwallet.model.token.abstraction.IToken
    public String getSymbolWithType() {
        return this.symbolWithType;
    }

    public /* synthetic */ DefaultIToken(String str, String str2, String str3, String str4, String str5, int i, int i2, boolean z, double d, double d2, double d3, int i3, qi0 qi0Var) {
        this((i3 & 1) != 0 ? "" : str, (i3 & 2) != 0 ? "" : str2, str3, (i3 & 8) != 0 ? "" : str4, (i3 & 16) != 0 ? "" : str5, i, (i3 & 64) != 0 ? 0 : i2, (i3 & 128) != 0 ? false : z, (i3 & 256) != 0 ? 0.0d : d, (i3 & RecyclerView.a0.FLAG_ADAPTER_POSITION_UNKNOWN) != 0 ? 0.0d : d2, (i3 & RecyclerView.a0.FLAG_ADAPTER_FULLUPDATE) != 0 ? 0.0d : d3);
    }
}
