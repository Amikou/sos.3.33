package net.safemoon.androidwallet.model.blackListAddress;

import com.google.gson.annotations.SerializedName;
import java.util.List;

/* compiled from: Request.kt */
/* loaded from: classes2.dex */
public final class Request {
    @SerializedName("addresses")
    private final List<String> list;

    public Request(List<String> list) {
        fs1.f(list, "list");
        this.list = list;
    }

    /* JADX WARN: Multi-variable type inference failed */
    public static /* synthetic */ Request copy$default(Request request, List list, int i, Object obj) {
        if ((i & 1) != 0) {
            list = request.list;
        }
        return request.copy(list);
    }

    public final List<String> component1() {
        return this.list;
    }

    public final Request copy(List<String> list) {
        fs1.f(list, "list");
        return new Request(list);
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        return (obj instanceof Request) && fs1.b(this.list, ((Request) obj).list);
    }

    public final List<String> getList() {
        return this.list;
    }

    public int hashCode() {
        return this.list.hashCode();
    }

    public String toString() {
        return "Request(list=" + this.list + ')';
    }
}
