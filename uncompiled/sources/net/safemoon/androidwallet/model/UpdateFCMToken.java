package net.safemoon.androidwallet.model;

import com.google.gson.annotations.SerializedName;
import java.util.Arrays;

/* compiled from: Setting.kt */
/* loaded from: classes2.dex */
public final class UpdateFCMToken {
    @SerializedName("newFcmToken")
    private String newFcmToken;
    @SerializedName("oldFcmToken")
    private String oldFcmToken;
    @SerializedName("walletAddresses")
    private String[] walletAddresses;

    public UpdateFCMToken(String[] strArr, String str, String str2) {
        fs1.f(strArr, "walletAddresses");
        fs1.f(str, "oldFcmToken");
        fs1.f(str2, "newFcmToken");
        this.walletAddresses = strArr;
        this.oldFcmToken = str;
        this.newFcmToken = str2;
    }

    public static /* synthetic */ UpdateFCMToken copy$default(UpdateFCMToken updateFCMToken, String[] strArr, String str, String str2, int i, Object obj) {
        if ((i & 1) != 0) {
            strArr = updateFCMToken.walletAddresses;
        }
        if ((i & 2) != 0) {
            str = updateFCMToken.oldFcmToken;
        }
        if ((i & 4) != 0) {
            str2 = updateFCMToken.newFcmToken;
        }
        return updateFCMToken.copy(strArr, str, str2);
    }

    public final String[] component1() {
        return this.walletAddresses;
    }

    public final String component2() {
        return this.oldFcmToken;
    }

    public final String component3() {
        return this.newFcmToken;
    }

    public final UpdateFCMToken copy(String[] strArr, String str, String str2) {
        fs1.f(strArr, "walletAddresses");
        fs1.f(str, "oldFcmToken");
        fs1.f(str2, "newFcmToken");
        return new UpdateFCMToken(strArr, str, str2);
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj instanceof UpdateFCMToken) {
            UpdateFCMToken updateFCMToken = (UpdateFCMToken) obj;
            return Arrays.equals(this.walletAddresses, updateFCMToken.walletAddresses) && fs1.b(this.oldFcmToken, updateFCMToken.oldFcmToken) && fs1.b(this.newFcmToken, updateFCMToken.newFcmToken);
        }
        return false;
    }

    public final String getNewFcmToken() {
        return this.newFcmToken;
    }

    public final String getOldFcmToken() {
        return this.oldFcmToken;
    }

    public final String[] getWalletAddresses() {
        return this.walletAddresses;
    }

    public int hashCode() {
        return (((Arrays.hashCode(this.walletAddresses) * 31) + this.oldFcmToken.hashCode()) * 31) + this.newFcmToken.hashCode();
    }

    public final void setNewFcmToken(String str) {
        fs1.f(str, "<set-?>");
        this.newFcmToken = str;
    }

    public final void setOldFcmToken(String str) {
        fs1.f(str, "<set-?>");
        this.oldFcmToken = str;
    }

    public final void setWalletAddresses(String[] strArr) {
        fs1.f(strArr, "<set-?>");
        this.walletAddresses = strArr;
    }

    public String toString() {
        return "UpdateFCMToken(walletAddresses=" + Arrays.toString(this.walletAddresses) + ", oldFcmToken=" + this.oldFcmToken + ", newFcmToken=" + this.newFcmToken + ')';
    }
}
