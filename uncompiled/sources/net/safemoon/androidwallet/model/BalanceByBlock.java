package net.safemoon.androidwallet.model;

import com.fasterxml.jackson.databind.deser.std.ThrowableDeserializer;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/* compiled from: BalanceByBlock.kt */
/* loaded from: classes2.dex */
public final class BalanceByBlock {
    @SerializedName(ThrowableDeserializer.PROP_NAME_MESSAGE)
    @Expose
    private String message;
    @SerializedName("result")
    @Expose
    private String result;
    @SerializedName("status")
    @Expose
    private String status;

    public final String getMessage() {
        return this.message;
    }

    public final String getResult() {
        return this.result;
    }

    public final String getStatus() {
        return this.status;
    }

    public final void setMessage(String str) {
        this.message = str;
    }

    public final void setResult(String str) {
        this.result = str;
    }

    public final void setStatus(String str) {
        this.status = str;
    }
}
