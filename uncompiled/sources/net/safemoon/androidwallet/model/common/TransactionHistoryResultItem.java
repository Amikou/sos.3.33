package net.safemoon.androidwallet.model.common;

import net.safemoon.androidwallet.model.transaction.history.Result;

/* compiled from: TransactionHistoryResultItem.kt */
/* loaded from: classes2.dex */
public final class TransactionHistoryResultItem extends HistoryListItem {
    private Result result;

    public final Result getResult() {
        return this.result;
    }

    @Override // net.safemoon.androidwallet.model.common.HistoryListItem
    public int getType() {
        return 2;
    }

    public final void setResult(Result result) {
        this.result = result;
    }
}
