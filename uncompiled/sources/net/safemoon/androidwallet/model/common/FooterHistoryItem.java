package net.safemoon.androidwallet.model.common;

/* compiled from: FooterHistoryItem.kt */
/* loaded from: classes2.dex */
public final class FooterHistoryItem extends HistoryListItem {
    private String title;

    public final String getTitle() {
        return this.title;
    }

    @Override // net.safemoon.androidwallet.model.common.HistoryListItem
    public int getType() {
        return 1;
    }

    public final void setTitle(String str) {
        this.title = str;
    }
}
