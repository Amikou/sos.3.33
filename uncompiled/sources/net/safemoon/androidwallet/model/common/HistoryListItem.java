package net.safemoon.androidwallet.model.common;

/* compiled from: HistoryListItem.kt */
/* loaded from: classes2.dex */
public abstract class HistoryListItem {
    public static final Companion Companion = new Companion(null);
    public static final int TYPE_EVENT = 2;
    public static final int TYPE_FOOTER = 1;
    public static final int TYPE_HEADER = 0;

    /* compiled from: HistoryListItem.kt */
    /* loaded from: classes2.dex */
    public static final class Companion {
        private Companion() {
        }

        public /* synthetic */ Companion(qi0 qi0Var) {
            this();
        }
    }

    public abstract int getType();
}
