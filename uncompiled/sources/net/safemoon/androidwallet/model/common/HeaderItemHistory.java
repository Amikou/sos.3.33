package net.safemoon.androidwallet.model.common;

/* compiled from: HeaderItemHistory.kt */
/* loaded from: classes2.dex */
public final class HeaderItemHistory extends HistoryListItem {
    private String title;

    public final String getTitle() {
        return this.title;
    }

    @Override // net.safemoon.androidwallet.model.common.HistoryListItem
    public int getType() {
        return 0;
    }

    public final void setTitle(String str) {
        this.title = str;
    }
}
