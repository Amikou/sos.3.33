package net.safemoon.androidwallet.model.common;

import com.github.mikephil.charting.utils.Utils;
import kotlin.NoWhenBranchMatchedException;
import net.safemoon.androidwallet.common.TokenType;

/* compiled from: Gas.kt */
/* loaded from: classes2.dex */
public final class GasPrice {
    public static final Companion Companion = new Companion(null);
    private static Double average;
    private static Double fast;
    private static Double fastest;

    /* compiled from: Gas.kt */
    /* loaded from: classes2.dex */
    public static final class Companion {
        private Companion() {
        }

        public /* synthetic */ Companion(qi0 qi0Var) {
            this();
        }

        public final Double getAverage() {
            return GasPrice.average;
        }

        public final Double getFast() {
            return GasPrice.fast;
        }

        public final Double getFastest() {
            return GasPrice.fastest;
        }

        public final void setAverage(Double d) {
            GasPrice.average = d;
        }

        public final void setFast(Double d) {
            GasPrice.fast = d;
        }

        public final void setFastest(Double d) {
            GasPrice.fastest = d;
        }
    }

    /* compiled from: Gas.kt */
    /* loaded from: classes2.dex */
    public /* synthetic */ class WhenMappings {
        public static final /* synthetic */ int[] $EnumSwitchMapping$0;

        static {
            int[] iArr = new int[Gas.values().length];
            iArr[Gas.Standard.ordinal()] = 1;
            iArr[Gas.Fast.ordinal()] = 2;
            iArr[Gas.Lightning.ordinal()] = 3;
            $EnumSwitchMapping$0 = iArr;
        }
    }

    public static /* synthetic */ Object fetchGasSuspended$default(GasPrice gasPrice, int i, Gas gas, q70 q70Var, int i2, Object obj) {
        if ((i2 & 2) != 0) {
            gas = null;
        }
        return gasPrice.fetchGasSuspended(i, gas, q70Var);
    }

    public final void applyNewGas(int i, double d, Gas gas) {
        fs1.f(gas, "gas");
        boolean z = true;
        if (i != 1 && i != 137 && i != TokenType.AVALANCHE_C.getChainId()) {
            z = false;
        }
        if (z) {
            int i2 = WhenMappings.$EnumSwitchMapping$0[gas.ordinal()];
        }
    }

    /* JADX WARN: Removed duplicated region for block: B:10:0x0025  */
    /* JADX WARN: Removed duplicated region for block: B:20:0x0044  */
    /* JADX WARN: Removed duplicated region for block: B:38:0x008a A[Catch: Exception -> 0x01ad, TryCatch #0 {Exception -> 0x01ad, blocks: (B:13:0x002e, B:68:0x014b, B:71:0x0156, B:75:0x015f, B:76:0x017e, B:80:0x018c, B:81:0x0195, B:82:0x019a, B:83:0x019b, B:84:0x01a4, B:18:0x0040, B:31:0x0074, B:34:0x0080, B:38:0x008a, B:39:0x00b6, B:43:0x00c4, B:44:0x00d1, B:45:0x00d6, B:46:0x00d7, B:47:0x00e4, B:28:0x0053, B:53:0x00f9, B:57:0x0107, B:58:0x010e, B:59:0x0113, B:60:0x0114, B:61:0x011b, B:63:0x0122, B:65:0x012a), top: B:89:0x0023 }] */
    /* JADX WARN: Removed duplicated region for block: B:39:0x00b6 A[Catch: Exception -> 0x01ad, TryCatch #0 {Exception -> 0x01ad, blocks: (B:13:0x002e, B:68:0x014b, B:71:0x0156, B:75:0x015f, B:76:0x017e, B:80:0x018c, B:81:0x0195, B:82:0x019a, B:83:0x019b, B:84:0x01a4, B:18:0x0040, B:31:0x0074, B:34:0x0080, B:38:0x008a, B:39:0x00b6, B:43:0x00c4, B:44:0x00d1, B:45:0x00d6, B:46:0x00d7, B:47:0x00e4, B:28:0x0053, B:53:0x00f9, B:57:0x0107, B:58:0x010e, B:59:0x0113, B:60:0x0114, B:61:0x011b, B:63:0x0122, B:65:0x012a), top: B:89:0x0023 }] */
    /* JADX WARN: Removed duplicated region for block: B:75:0x015f A[Catch: Exception -> 0x01ad, TryCatch #0 {Exception -> 0x01ad, blocks: (B:13:0x002e, B:68:0x014b, B:71:0x0156, B:75:0x015f, B:76:0x017e, B:80:0x018c, B:81:0x0195, B:82:0x019a, B:83:0x019b, B:84:0x01a4, B:18:0x0040, B:31:0x0074, B:34:0x0080, B:38:0x008a, B:39:0x00b6, B:43:0x00c4, B:44:0x00d1, B:45:0x00d6, B:46:0x00d7, B:47:0x00e4, B:28:0x0053, B:53:0x00f9, B:57:0x0107, B:58:0x010e, B:59:0x0113, B:60:0x0114, B:61:0x011b, B:63:0x0122, B:65:0x012a), top: B:89:0x0023 }] */
    /* JADX WARN: Removed duplicated region for block: B:76:0x017e A[Catch: Exception -> 0x01ad, TryCatch #0 {Exception -> 0x01ad, blocks: (B:13:0x002e, B:68:0x014b, B:71:0x0156, B:75:0x015f, B:76:0x017e, B:80:0x018c, B:81:0x0195, B:82:0x019a, B:83:0x019b, B:84:0x01a4, B:18:0x0040, B:31:0x0074, B:34:0x0080, B:38:0x008a, B:39:0x00b6, B:43:0x00c4, B:44:0x00d1, B:45:0x00d6, B:46:0x00d7, B:47:0x00e4, B:28:0x0053, B:53:0x00f9, B:57:0x0107, B:58:0x010e, B:59:0x0113, B:60:0x0114, B:61:0x011b, B:63:0x0122, B:65:0x012a), top: B:89:0x0023 }] */
    /*
        Code decompiled incorrectly, please refer to instructions dump.
        To view partially-correct code enable 'Show inconsistent code' option in preferences
    */
    public final java.lang.Object fetchGasSuspended(int r7, net.safemoon.androidwallet.model.common.Gas r8, defpackage.q70<? super java.lang.Double> r9) {
        /*
            Method dump skipped, instructions count: 431
            To view this dump change 'Code comments level' option to 'DEBUG'
        */
        throw new UnsupportedOperationException("Method not decompiled: net.safemoon.androidwallet.model.common.GasPrice.fetchGasSuspended(int, net.safemoon.androidwallet.model.common.Gas, q70):java.lang.Object");
    }

    public final double getPrice(Gas gas, int i) {
        fs1.f(gas, "gas");
        if (i == 56) {
            int i2 = WhenMappings.$EnumSwitchMapping$0[gas.ordinal()];
            if (i2 != 1) {
                if (i2 != 2) {
                    if (i2 == 3) {
                        return 7.0d;
                    }
                    throw new NoWhenBranchMatchedException();
                }
                return 6.0d;
            }
            return 5.0d;
        }
        if (i == 1 || i == 3) {
            int i3 = WhenMappings.$EnumSwitchMapping$0[gas.ordinal()];
            if (i3 == 1) {
                Double d = average;
                if (d == null) {
                    return 68.0d;
                }
                return d.doubleValue();
            } else if (i3 == 2) {
                Double d2 = fast;
                if (d2 == null) {
                    return 84.0d;
                }
                return d2.doubleValue();
            } else if (i3 == 3) {
                Double d3 = fastest;
                if (d3 == null) {
                    return 112.0d;
                }
                return d3.doubleValue();
            } else {
                throw new NoWhenBranchMatchedException();
            }
        } else if (i == 97) {
            return 10.0d;
        } else {
            if (i == 137) {
                int i4 = WhenMappings.$EnumSwitchMapping$0[gas.ordinal()];
                if (i4 == 1) {
                    Double d4 = average;
                    if (d4 == null) {
                        return 38.1d;
                    }
                    return d4.doubleValue();
                } else if (i4 == 2) {
                    Double d5 = fast;
                    if (d5 == null) {
                        return 50.1d;
                    }
                    return d5.doubleValue();
                } else if (i4 == 3) {
                    Double d6 = fastest;
                    if (d6 == null) {
                        return 54.6d;
                    }
                    return d6.doubleValue();
                } else {
                    throw new NoWhenBranchMatchedException();
                }
            } else if (i == TokenType.AVALANCHE_C.getChainId()) {
                int i5 = WhenMappings.$EnumSwitchMapping$0[gas.ordinal()];
                if (i5 == 1) {
                    Double d7 = average;
                    if (d7 == null) {
                        return 35.0d;
                    }
                    return d7.doubleValue();
                } else if (i5 == 2) {
                    Double d8 = fast;
                    if (d8 == null) {
                        return 40.0d;
                    }
                    return d8.doubleValue();
                } else if (i5 == 3) {
                    Double d9 = fastest;
                    if (d9 == null) {
                        return 45.0d;
                    }
                    return d9.doubleValue();
                } else {
                    throw new NoWhenBranchMatchedException();
                }
            } else {
                return Utils.DOUBLE_EPSILON;
            }
        }
    }
}
