package net.safemoon.androidwallet.model.common;

/* compiled from: Gas.kt */
/* loaded from: classes2.dex */
public enum Gas {
    Standard,
    Fast,
    Lightning
}
