package net.safemoon.androidwallet.model.common;

/* compiled from: LanguageItem.kt */
/* loaded from: classes2.dex */
public final class LanguageItem {
    private final String languageCode;
    private final int regionResId;
    private final int titleResId;

    public LanguageItem(String str, int i, int i2) {
        fs1.f(str, "languageCode");
        this.languageCode = str;
        this.titleResId = i;
        this.regionResId = i2;
    }

    public static /* synthetic */ LanguageItem copy$default(LanguageItem languageItem, String str, int i, int i2, int i3, Object obj) {
        if ((i3 & 1) != 0) {
            str = languageItem.languageCode;
        }
        if ((i3 & 2) != 0) {
            i = languageItem.titleResId;
        }
        if ((i3 & 4) != 0) {
            i2 = languageItem.regionResId;
        }
        return languageItem.copy(str, i, i2);
    }

    public final String component1() {
        return this.languageCode;
    }

    public final int component2() {
        return this.titleResId;
    }

    public final int component3() {
        return this.regionResId;
    }

    public final LanguageItem copy(String str, int i, int i2) {
        fs1.f(str, "languageCode");
        return new LanguageItem(str, i, i2);
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj instanceof LanguageItem) {
            LanguageItem languageItem = (LanguageItem) obj;
            return fs1.b(this.languageCode, languageItem.languageCode) && this.titleResId == languageItem.titleResId && this.regionResId == languageItem.regionResId;
        }
        return false;
    }

    public final String getLanguageCode() {
        return this.languageCode;
    }

    public final int getRegionResId() {
        return this.regionResId;
    }

    public final int getTitleResId() {
        return this.titleResId;
    }

    public int hashCode() {
        return (((this.languageCode.hashCode() * 31) + this.titleResId) * 31) + this.regionResId;
    }

    public String toString() {
        return "LanguageItem(languageCode=" + this.languageCode + ", titleResId=" + this.titleResId + ", regionResId=" + this.regionResId + ')';
    }
}
