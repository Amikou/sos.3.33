package net.safemoon.androidwallet.model.common;

import kotlin.coroutines.jvm.internal.ContinuationImpl;
import kotlin.coroutines.jvm.internal.a;

/* compiled from: Gas.kt */
@a(c = "net.safemoon.androidwallet.model.common.GasPrice", f = "Gas.kt", l = {72, 98}, m = "fetchGasSuspended")
/* loaded from: classes2.dex */
public final class GasPrice$fetchGasSuspended$1 extends ContinuationImpl {
    public Object L$0;
    public int label;
    public /* synthetic */ Object result;
    public final /* synthetic */ GasPrice this$0;

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public GasPrice$fetchGasSuspended$1(GasPrice gasPrice, q70<? super GasPrice$fetchGasSuspended$1> q70Var) {
        super(q70Var);
        this.this$0 = gasPrice;
    }

    @Override // kotlin.coroutines.jvm.internal.BaseContinuationImpl
    public final Object invokeSuspend(Object obj) {
        this.result = obj;
        this.label |= Integer.MIN_VALUE;
        return this.this$0.fetchGasSuspended(0, null, this);
    }
}
