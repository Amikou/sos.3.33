package net.safemoon.androidwallet.model;

import com.google.gson.annotations.SerializedName;

/* compiled from: Setting.kt */
/* loaded from: classes2.dex */
public final class SingleFCMRegister {
    @SerializedName("blockchainAccountAddress")
    private String blockchainAccountAddress;
    @SerializedName("currency")
    private String currency;
    @SerializedName("deviceToken")
    private String deviceToken;
    @SerializedName("isSendPushPriceAlert")
    private Boolean isSendPushPriceAlert;
    @SerializedName("lang")
    private String lang;
    @SerializedName("platForm")
    private String platForm;

    public SingleFCMRegister() {
        this(null, null, null, null, null, null, 63, null);
    }

    public SingleFCMRegister(String str, String str2, String str3, String str4, String str5, Boolean bool) {
        this.deviceToken = str;
        this.blockchainAccountAddress = str2;
        this.platForm = str3;
        this.lang = str4;
        this.currency = str5;
        this.isSendPushPriceAlert = bool;
    }

    public static /* synthetic */ SingleFCMRegister copy$default(SingleFCMRegister singleFCMRegister, String str, String str2, String str3, String str4, String str5, Boolean bool, int i, Object obj) {
        if ((i & 1) != 0) {
            str = singleFCMRegister.deviceToken;
        }
        if ((i & 2) != 0) {
            str2 = singleFCMRegister.blockchainAccountAddress;
        }
        String str6 = str2;
        if ((i & 4) != 0) {
            str3 = singleFCMRegister.platForm;
        }
        String str7 = str3;
        if ((i & 8) != 0) {
            str4 = singleFCMRegister.lang;
        }
        String str8 = str4;
        if ((i & 16) != 0) {
            str5 = singleFCMRegister.currency;
        }
        String str9 = str5;
        if ((i & 32) != 0) {
            bool = singleFCMRegister.isSendPushPriceAlert;
        }
        return singleFCMRegister.copy(str, str6, str7, str8, str9, bool);
    }

    public final String component1() {
        return this.deviceToken;
    }

    public final String component2() {
        return this.blockchainAccountAddress;
    }

    public final String component3() {
        return this.platForm;
    }

    public final String component4() {
        return this.lang;
    }

    public final String component5() {
        return this.currency;
    }

    public final Boolean component6() {
        return this.isSendPushPriceAlert;
    }

    public final SingleFCMRegister copy(String str, String str2, String str3, String str4, String str5, Boolean bool) {
        return new SingleFCMRegister(str, str2, str3, str4, str5, bool);
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj instanceof SingleFCMRegister) {
            SingleFCMRegister singleFCMRegister = (SingleFCMRegister) obj;
            return fs1.b(this.deviceToken, singleFCMRegister.deviceToken) && fs1.b(this.blockchainAccountAddress, singleFCMRegister.blockchainAccountAddress) && fs1.b(this.platForm, singleFCMRegister.platForm) && fs1.b(this.lang, singleFCMRegister.lang) && fs1.b(this.currency, singleFCMRegister.currency) && fs1.b(this.isSendPushPriceAlert, singleFCMRegister.isSendPushPriceAlert);
        }
        return false;
    }

    public final String getBlockchainAccountAddress() {
        return this.blockchainAccountAddress;
    }

    public final String getCurrency() {
        return this.currency;
    }

    public final String getDeviceToken() {
        return this.deviceToken;
    }

    public final String getLang() {
        return this.lang;
    }

    public final String getPlatForm() {
        return this.platForm;
    }

    public int hashCode() {
        String str = this.deviceToken;
        int hashCode = (str == null ? 0 : str.hashCode()) * 31;
        String str2 = this.blockchainAccountAddress;
        int hashCode2 = (hashCode + (str2 == null ? 0 : str2.hashCode())) * 31;
        String str3 = this.platForm;
        int hashCode3 = (hashCode2 + (str3 == null ? 0 : str3.hashCode())) * 31;
        String str4 = this.lang;
        int hashCode4 = (hashCode3 + (str4 == null ? 0 : str4.hashCode())) * 31;
        String str5 = this.currency;
        int hashCode5 = (hashCode4 + (str5 == null ? 0 : str5.hashCode())) * 31;
        Boolean bool = this.isSendPushPriceAlert;
        return hashCode5 + (bool != null ? bool.hashCode() : 0);
    }

    public final Boolean isSendPushPriceAlert() {
        return this.isSendPushPriceAlert;
    }

    public final void setBlockchainAccountAddress(String str) {
        this.blockchainAccountAddress = str;
    }

    public final void setCurrency(String str) {
        this.currency = str;
    }

    public final void setDeviceToken(String str) {
        this.deviceToken = str;
    }

    public final void setLang(String str) {
        this.lang = str;
    }

    public final void setPlatForm(String str) {
        this.platForm = str;
    }

    public final void setSendPushPriceAlert(Boolean bool) {
        this.isSendPushPriceAlert = bool;
    }

    public String toString() {
        return "SingleFCMRegister(deviceToken=" + ((Object) this.deviceToken) + ", blockchainAccountAddress=" + ((Object) this.blockchainAccountAddress) + ", platForm=" + ((Object) this.platForm) + ", lang=" + ((Object) this.lang) + ", currency=" + ((Object) this.currency) + ", isSendPushPriceAlert=" + this.isSendPushPriceAlert + ')';
    }

    public /* synthetic */ SingleFCMRegister(String str, String str2, String str3, String str4, String str5, Boolean bool, int i, qi0 qi0Var) {
        this((i & 1) != 0 ? null : str, (i & 2) != 0 ? null : str2, (i & 4) != 0 ? "ANDROID" : str3, (i & 8) != 0 ? null : str4, (i & 16) != 0 ? null : str5, (i & 32) != 0 ? null : bool);
    }
}
