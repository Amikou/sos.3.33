package net.safemoon.androidwallet.model.reflections;

import com.github.mikephil.charting.utils.Utils;

/* compiled from: RoomReflectionsDataAndToken.kt */
/* loaded from: classes2.dex */
public final class RoomReflectionsDataAndToken {
    private Float balance;
    private float blockBalance;
    private final RoomReflectionsData data;
    private Float diffBalance;
    private String displayDate;
    private final RoomReflectionsToken token;

    public RoomReflectionsDataAndToken(RoomReflectionsData roomReflectionsData, RoomReflectionsToken roomReflectionsToken) {
        fs1.f(roomReflectionsData, "data");
        fs1.f(roomReflectionsToken, "token");
        this.data = roomReflectionsData;
        this.token = roomReflectionsToken;
        Float valueOf = Float.valueOf((float) Utils.FLOAT_EPSILON);
        this.balance = valueOf;
        this.diffBalance = valueOf;
        this.displayDate = "";
    }

    public static /* synthetic */ RoomReflectionsDataAndToken copy$default(RoomReflectionsDataAndToken roomReflectionsDataAndToken, RoomReflectionsData roomReflectionsData, RoomReflectionsToken roomReflectionsToken, int i, Object obj) {
        if ((i & 1) != 0) {
            roomReflectionsData = roomReflectionsDataAndToken.data;
        }
        if ((i & 2) != 0) {
            roomReflectionsToken = roomReflectionsDataAndToken.token;
        }
        return roomReflectionsDataAndToken.copy(roomReflectionsData, roomReflectionsToken);
    }

    public final RoomReflectionsData component1() {
        return this.data;
    }

    public final RoomReflectionsToken component2() {
        return this.token;
    }

    public final RoomReflectionsDataAndToken copy(RoomReflectionsData roomReflectionsData, RoomReflectionsToken roomReflectionsToken) {
        fs1.f(roomReflectionsData, "data");
        fs1.f(roomReflectionsToken, "token");
        return new RoomReflectionsDataAndToken(roomReflectionsData, roomReflectionsToken);
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj instanceof RoomReflectionsDataAndToken) {
            RoomReflectionsDataAndToken roomReflectionsDataAndToken = (RoomReflectionsDataAndToken) obj;
            return fs1.b(this.data, roomReflectionsDataAndToken.data) && fs1.b(this.token, roomReflectionsDataAndToken.token);
        }
        return false;
    }

    public final Float getBalance() {
        return this.balance;
    }

    public final float getBlockBalance() {
        return this.blockBalance;
    }

    public final RoomReflectionsData getData() {
        return this.data;
    }

    public final Float getDiffBalance() {
        return this.diffBalance;
    }

    public final String getDisplayDate() {
        return this.displayDate;
    }

    public final RoomReflectionsToken getToken() {
        return this.token;
    }

    public int hashCode() {
        return (this.data.hashCode() * 31) + this.token.hashCode();
    }

    public final void setBalance(Float f) {
        this.balance = f;
    }

    public final void setBlockBalance(float f) {
        this.blockBalance = f;
    }

    public final void setDiffBalance(Float f) {
        this.diffBalance = f;
    }

    public final void setDisplayDate(String str) {
        this.displayDate = str;
    }

    public String toString() {
        return "RoomReflectionsDataAndToken(data=" + this.data + ", token=" + this.token + ')';
    }
}
