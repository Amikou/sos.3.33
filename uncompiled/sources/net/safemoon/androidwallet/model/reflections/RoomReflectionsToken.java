package net.safemoon.androidwallet.model.reflections;

import androidx.recyclerview.widget.RecyclerView;
import com.github.mikephil.charting.utils.Utils;
import java.math.BigDecimal;
import net.safemoon.androidwallet.model.token.abstraction.IToken;
import okhttp3.internal.http2.Http2;
import org.web3j.ens.contracts.generated.PublicResolver;

/* compiled from: RoomReflectionsToken.kt */
/* loaded from: classes2.dex */
public final class RoomReflectionsToken {
    public static final Companion Companion = new Companion(null);
    private final int chainId;
    private Long cmcId;
    private final String contractAddress;
    private final int decimals;
    private BigDecimal differenceBalance;
    private String displayDate;
    private boolean enableAdvanceMode;
    private Long firstTimeStamp;
    private Integer iconResId;
    private final String iconResName;
    private Long id;
    private Long latestBalance;
    private Long latestTimeStamp;
    private final String name;
    private String nativeBalance;
    private Double priceUsd;
    private final String symbol;
    private final String symbolWithType;

    /* compiled from: RoomReflectionsToken.kt */
    /* loaded from: classes2.dex */
    public static final class Companion {
        private Companion() {
        }

        public /* synthetic */ Companion(qi0 qi0Var) {
            this();
        }

        public final IToken toIToken(final RoomReflectionsToken roomReflectionsToken) {
            fs1.f(roomReflectionsToken, "roomReflectionsToken");
            return new IToken() { // from class: net.safemoon.androidwallet.model.reflections.RoomReflectionsToken$Companion$toIToken$1
                private final boolean allowSwap;
                private final int chainId;
                private final String contractAddress;
                private final int decimals;
                private final String iconResName;
                private final String name;
                private final double nativeBalance;
                private final double percentChange1h;
                private final double priceInUsdt;
                private final String symbol;
                private final String symbolWithType;

                {
                    this.symbolWithType = RoomReflectionsToken.this.getSymbolWithType();
                    this.name = RoomReflectionsToken.this.getName();
                    this.symbol = RoomReflectionsToken.this.getSymbol();
                    this.iconResName = RoomReflectionsToken.this.getIconResName();
                    this.contractAddress = RoomReflectionsToken.this.getContractAddress();
                    this.chainId = RoomReflectionsToken.this.getChainId();
                    this.decimals = RoomReflectionsToken.this.getDecimals();
                }

                @Override // net.safemoon.androidwallet.model.token.abstraction.IToken
                public boolean getAllowSwap() {
                    return this.allowSwap;
                }

                @Override // net.safemoon.androidwallet.model.token.abstraction.IToken
                public int getChainId() {
                    return this.chainId;
                }

                @Override // net.safemoon.androidwallet.model.token.abstraction.IToken
                public String getContractAddress() {
                    return this.contractAddress;
                }

                @Override // net.safemoon.androidwallet.model.token.abstraction.IToken
                public int getDecimals() {
                    return this.decimals;
                }

                @Override // net.safemoon.androidwallet.model.token.abstraction.IToken
                public String getIconResName() {
                    return this.iconResName;
                }

                @Override // net.safemoon.androidwallet.model.token.abstraction.IToken
                public String getName() {
                    return this.name;
                }

                @Override // net.safemoon.androidwallet.model.token.abstraction.IToken
                public double getNativeBalance() {
                    return this.nativeBalance;
                }

                @Override // net.safemoon.androidwallet.model.token.abstraction.IToken
                public double getPercentChange1h() {
                    return this.percentChange1h;
                }

                @Override // net.safemoon.androidwallet.model.token.abstraction.IToken
                public double getPriceInUsdt() {
                    return this.priceInUsdt;
                }

                @Override // net.safemoon.androidwallet.model.token.abstraction.IToken
                public String getSymbol() {
                    return this.symbol;
                }

                @Override // net.safemoon.androidwallet.model.token.abstraction.IToken
                public String getSymbolWithType() {
                    return this.symbolWithType;
                }
            };
        }
    }

    public RoomReflectionsToken(Long l, String str, String str2, String str3, String str4, String str5, int i, int i2, String str6, Long l2, boolean z, Long l3, Long l4, Long l5, Double d) {
        fs1.f(str, "symbolWithType");
        fs1.f(str2, "symbol");
        fs1.f(str3, PublicResolver.FUNC_NAME);
        fs1.f(str4, "iconResName");
        fs1.f(str5, "contractAddress");
        this.id = l;
        this.symbolWithType = str;
        this.symbol = str2;
        this.name = str3;
        this.iconResName = str4;
        this.contractAddress = str5;
        this.chainId = i;
        this.decimals = i2;
        this.nativeBalance = str6;
        this.firstTimeStamp = l2;
        this.enableAdvanceMode = z;
        this.latestBalance = l3;
        this.latestTimeStamp = l4;
        this.cmcId = l5;
        this.priceUsd = d;
        this.iconResId = 0;
        BigDecimal bigDecimal = BigDecimal.ZERO;
        fs1.e(bigDecimal, "ZERO");
        this.differenceBalance = bigDecimal;
        this.displayDate = "";
    }

    public final Long component1() {
        return this.id;
    }

    public final Long component10() {
        return this.firstTimeStamp;
    }

    public final boolean component11() {
        return this.enableAdvanceMode;
    }

    public final Long component12() {
        return this.latestBalance;
    }

    public final Long component13() {
        return this.latestTimeStamp;
    }

    public final Long component14() {
        return this.cmcId;
    }

    public final Double component15() {
        return this.priceUsd;
    }

    public final String component2() {
        return this.symbolWithType;
    }

    public final String component3() {
        return this.symbol;
    }

    public final String component4() {
        return this.name;
    }

    public final String component5() {
        return this.iconResName;
    }

    public final String component6() {
        return this.contractAddress;
    }

    public final int component7() {
        return this.chainId;
    }

    public final int component8() {
        return this.decimals;
    }

    public final String component9() {
        return this.nativeBalance;
    }

    public final RoomReflectionsToken copy(Long l, String str, String str2, String str3, String str4, String str5, int i, int i2, String str6, Long l2, boolean z, Long l3, Long l4, Long l5, Double d) {
        fs1.f(str, "symbolWithType");
        fs1.f(str2, "symbol");
        fs1.f(str3, PublicResolver.FUNC_NAME);
        fs1.f(str4, "iconResName");
        fs1.f(str5, "contractAddress");
        return new RoomReflectionsToken(l, str, str2, str3, str4, str5, i, i2, str6, l2, z, l3, l4, l5, d);
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj instanceof RoomReflectionsToken) {
            RoomReflectionsToken roomReflectionsToken = (RoomReflectionsToken) obj;
            return fs1.b(this.id, roomReflectionsToken.id) && fs1.b(this.symbolWithType, roomReflectionsToken.symbolWithType) && fs1.b(this.symbol, roomReflectionsToken.symbol) && fs1.b(this.name, roomReflectionsToken.name) && fs1.b(this.iconResName, roomReflectionsToken.iconResName) && fs1.b(this.contractAddress, roomReflectionsToken.contractAddress) && this.chainId == roomReflectionsToken.chainId && this.decimals == roomReflectionsToken.decimals && fs1.b(this.nativeBalance, roomReflectionsToken.nativeBalance) && fs1.b(this.firstTimeStamp, roomReflectionsToken.firstTimeStamp) && this.enableAdvanceMode == roomReflectionsToken.enableAdvanceMode && fs1.b(this.latestBalance, roomReflectionsToken.latestBalance) && fs1.b(this.latestTimeStamp, roomReflectionsToken.latestTimeStamp) && fs1.b(this.cmcId, roomReflectionsToken.cmcId) && fs1.b(this.priceUsd, roomReflectionsToken.priceUsd);
        }
        return false;
    }

    public final int getChainId() {
        return this.chainId;
    }

    public final Long getCmcId() {
        return this.cmcId;
    }

    public final String getContractAddress() {
        return this.contractAddress;
    }

    public final int getDecimals() {
        return this.decimals;
    }

    public final BigDecimal getDifferenceBalance() {
        return this.differenceBalance;
    }

    public final String getDisplayDate() {
        return this.displayDate;
    }

    public final boolean getEnableAdvanceMode() {
        return this.enableAdvanceMode;
    }

    public final Long getFirstTimeStamp() {
        return this.firstTimeStamp;
    }

    public final Integer getIconResId() {
        return this.iconResId;
    }

    public final String getIconResName() {
        return this.iconResName;
    }

    public final Long getId() {
        return this.id;
    }

    public final Long getLatestBalance() {
        return this.latestBalance;
    }

    public final Long getLatestTimeStamp() {
        return this.latestTimeStamp;
    }

    public final String getName() {
        return this.name;
    }

    public final String getNativeBalance() {
        return this.nativeBalance;
    }

    public final Double getPriceUsd() {
        return this.priceUsd;
    }

    public final String getSymbol() {
        return this.symbol;
    }

    public final String getSymbolWithType() {
        return this.symbolWithType;
    }

    /* JADX WARN: Multi-variable type inference failed */
    public int hashCode() {
        Long l = this.id;
        int hashCode = (((((((((((((((l == null ? 0 : l.hashCode()) * 31) + this.symbolWithType.hashCode()) * 31) + this.symbol.hashCode()) * 31) + this.name.hashCode()) * 31) + this.iconResName.hashCode()) * 31) + this.contractAddress.hashCode()) * 31) + this.chainId) * 31) + this.decimals) * 31;
        String str = this.nativeBalance;
        int hashCode2 = (hashCode + (str == null ? 0 : str.hashCode())) * 31;
        Long l2 = this.firstTimeStamp;
        int hashCode3 = (hashCode2 + (l2 == null ? 0 : l2.hashCode())) * 31;
        boolean z = this.enableAdvanceMode;
        int i = z;
        if (z != 0) {
            i = 1;
        }
        int i2 = (hashCode3 + i) * 31;
        Long l3 = this.latestBalance;
        int hashCode4 = (i2 + (l3 == null ? 0 : l3.hashCode())) * 31;
        Long l4 = this.latestTimeStamp;
        int hashCode5 = (hashCode4 + (l4 == null ? 0 : l4.hashCode())) * 31;
        Long l5 = this.cmcId;
        int hashCode6 = (hashCode5 + (l5 == null ? 0 : l5.hashCode())) * 31;
        Double d = this.priceUsd;
        return hashCode6 + (d != null ? d.hashCode() : 0);
    }

    public final void setCmcId(Long l) {
        this.cmcId = l;
    }

    public final void setDifferenceBalance(BigDecimal bigDecimal) {
        fs1.f(bigDecimal, "<set-?>");
        this.differenceBalance = bigDecimal;
    }

    public final void setDisplayDate(String str) {
        this.displayDate = str;
    }

    public final void setEnableAdvanceMode(boolean z) {
        this.enableAdvanceMode = z;
    }

    public final void setFirstTimeStamp(Long l) {
        this.firstTimeStamp = l;
    }

    public final void setIconResId(Integer num) {
        this.iconResId = num;
    }

    public final void setId(Long l) {
        this.id = l;
    }

    public final void setLatestBalance(Long l) {
        this.latestBalance = l;
    }

    public final void setLatestTimeStamp(Long l) {
        this.latestTimeStamp = l;
    }

    public final void setNativeBalance(String str) {
        this.nativeBalance = str;
    }

    public final void setPriceUsd(Double d) {
        this.priceUsd = d;
    }

    public String toString() {
        return "RoomReflectionsToken(id=" + this.id + ", symbolWithType=" + this.symbolWithType + ", symbol=" + this.symbol + ", name=" + this.name + ", iconResName=" + this.iconResName + ", contractAddress=" + this.contractAddress + ", chainId=" + this.chainId + ", decimals=" + this.decimals + ", nativeBalance=" + ((Object) this.nativeBalance) + ", firstTimeStamp=" + this.firstTimeStamp + ", enableAdvanceMode=" + this.enableAdvanceMode + ", latestBalance=" + this.latestBalance + ", latestTimeStamp=" + this.latestTimeStamp + ", cmcId=" + this.cmcId + ", priceUsd=" + this.priceUsd + ')';
    }

    public /* synthetic */ RoomReflectionsToken(Long l, String str, String str2, String str3, String str4, String str5, int i, int i2, String str6, Long l2, boolean z, Long l3, Long l4, Long l5, Double d, int i3, qi0 qi0Var) {
        this((i3 & 1) != 0 ? null : l, str, str2, str3, str4, str5, i, i2, (i3 & 256) != 0 ? "0" : str6, (i3 & RecyclerView.a0.FLAG_ADAPTER_POSITION_UNKNOWN) != 0 ? 0L : l2, (i3 & RecyclerView.a0.FLAG_ADAPTER_FULLUPDATE) != 0 ? false : z, (i3 & 2048) != 0 ? 0L : l3, (i3 & 4096) != 0 ? 0L : l4, (i3 & 8192) != 0 ? 0L : l5, (i3 & Http2.INITIAL_MAX_FRAME_SIZE) != 0 ? Double.valueOf((double) Utils.DOUBLE_EPSILON) : d);
    }
}
