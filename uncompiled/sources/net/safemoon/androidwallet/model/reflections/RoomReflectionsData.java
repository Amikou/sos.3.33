package net.safemoon.androidwallet.model.reflections;

/* compiled from: RoomReflectionsData.kt */
/* loaded from: classes2.dex */
public final class RoomReflectionsData {
    private final String block;
    private final String blockBalance;
    private Long id;
    private final String nativeBalance;
    private long startTime;
    private final String symbolWithType;
    private long timeStamp;

    public RoomReflectionsData(Long l, String str, String str2, String str3, String str4, long j) {
        fs1.f(str, "symbolWithType");
        fs1.f(str2, "nativeBalance");
        fs1.f(str3, "blockBalance");
        fs1.f(str4, "block");
        this.id = l;
        this.symbolWithType = str;
        this.nativeBalance = str2;
        this.blockBalance = str3;
        this.block = str4;
        this.timeStamp = j;
    }

    public static /* synthetic */ RoomReflectionsData copy$default(RoomReflectionsData roomReflectionsData, Long l, String str, String str2, String str3, String str4, long j, int i, Object obj) {
        if ((i & 1) != 0) {
            l = roomReflectionsData.id;
        }
        if ((i & 2) != 0) {
            str = roomReflectionsData.symbolWithType;
        }
        String str5 = str;
        if ((i & 4) != 0) {
            str2 = roomReflectionsData.nativeBalance;
        }
        String str6 = str2;
        if ((i & 8) != 0) {
            str3 = roomReflectionsData.blockBalance;
        }
        String str7 = str3;
        if ((i & 16) != 0) {
            str4 = roomReflectionsData.block;
        }
        String str8 = str4;
        if ((i & 32) != 0) {
            j = roomReflectionsData.timeStamp;
        }
        return roomReflectionsData.copy(l, str5, str6, str7, str8, j);
    }

    public final Long component1() {
        return this.id;
    }

    public final String component2() {
        return this.symbolWithType;
    }

    public final String component3() {
        return this.nativeBalance;
    }

    public final String component4() {
        return this.blockBalance;
    }

    public final String component5() {
        return this.block;
    }

    public final long component6() {
        return this.timeStamp;
    }

    public final RoomReflectionsData copy(Long l, String str, String str2, String str3, String str4, long j) {
        fs1.f(str, "symbolWithType");
        fs1.f(str2, "nativeBalance");
        fs1.f(str3, "blockBalance");
        fs1.f(str4, "block");
        return new RoomReflectionsData(l, str, str2, str3, str4, j);
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj instanceof RoomReflectionsData) {
            RoomReflectionsData roomReflectionsData = (RoomReflectionsData) obj;
            return fs1.b(this.id, roomReflectionsData.id) && fs1.b(this.symbolWithType, roomReflectionsData.symbolWithType) && fs1.b(this.nativeBalance, roomReflectionsData.nativeBalance) && fs1.b(this.blockBalance, roomReflectionsData.blockBalance) && fs1.b(this.block, roomReflectionsData.block) && this.timeStamp == roomReflectionsData.timeStamp;
        }
        return false;
    }

    public final String getBlock() {
        return this.block;
    }

    public final String getBlockBalance() {
        return this.blockBalance;
    }

    public final Long getId() {
        return this.id;
    }

    public final String getNativeBalance() {
        return this.nativeBalance;
    }

    public final long getStartTime() {
        return this.startTime;
    }

    public final String getSymbolWithType() {
        return this.symbolWithType;
    }

    public final long getTimeStamp() {
        return this.timeStamp;
    }

    public int hashCode() {
        Long l = this.id;
        return ((((((((((l == null ? 0 : l.hashCode()) * 31) + this.symbolWithType.hashCode()) * 31) + this.nativeBalance.hashCode()) * 31) + this.blockBalance.hashCode()) * 31) + this.block.hashCode()) * 31) + p80.a(this.timeStamp);
    }

    public final void setId(Long l) {
        this.id = l;
    }

    public final void setStartTime(long j) {
        this.startTime = j;
    }

    public final void setTimeStamp(long j) {
        this.timeStamp = j;
    }

    public String toString() {
        return "RoomReflectionsData(id=" + this.id + ", symbolWithType=" + this.symbolWithType + ", nativeBalance=" + this.nativeBalance + ", blockBalance=" + this.blockBalance + ", block=" + this.block + ", timeStamp=" + this.timeStamp + ')';
    }

    public /* synthetic */ RoomReflectionsData(Long l, String str, String str2, String str3, String str4, long j, int i, qi0 qi0Var) {
        this((i & 1) != 0 ? null : l, str, str2, str3, str4, j);
    }
}
