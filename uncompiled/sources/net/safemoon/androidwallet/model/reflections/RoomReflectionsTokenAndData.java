package net.safemoon.androidwallet.model.reflections;

import java.util.List;

/* compiled from: RoomReflectionsTokenAndData.kt */
/* loaded from: classes2.dex */
public final class RoomReflectionsTokenAndData {
    private final List<RoomReflectionsData> data;
    private final RoomReflectionsToken token;

    public RoomReflectionsTokenAndData(RoomReflectionsToken roomReflectionsToken, List<RoomReflectionsData> list) {
        fs1.f(roomReflectionsToken, "token");
        fs1.f(list, "data");
        this.token = roomReflectionsToken;
        this.data = list;
    }

    /* JADX WARN: Multi-variable type inference failed */
    public static /* synthetic */ RoomReflectionsTokenAndData copy$default(RoomReflectionsTokenAndData roomReflectionsTokenAndData, RoomReflectionsToken roomReflectionsToken, List list, int i, Object obj) {
        if ((i & 1) != 0) {
            roomReflectionsToken = roomReflectionsTokenAndData.token;
        }
        if ((i & 2) != 0) {
            list = roomReflectionsTokenAndData.data;
        }
        return roomReflectionsTokenAndData.copy(roomReflectionsToken, list);
    }

    public final RoomReflectionsToken component1() {
        return this.token;
    }

    public final List<RoomReflectionsData> component2() {
        return this.data;
    }

    public final RoomReflectionsTokenAndData copy(RoomReflectionsToken roomReflectionsToken, List<RoomReflectionsData> list) {
        fs1.f(roomReflectionsToken, "token");
        fs1.f(list, "data");
        return new RoomReflectionsTokenAndData(roomReflectionsToken, list);
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj instanceof RoomReflectionsTokenAndData) {
            RoomReflectionsTokenAndData roomReflectionsTokenAndData = (RoomReflectionsTokenAndData) obj;
            return fs1.b(this.token, roomReflectionsTokenAndData.token) && fs1.b(this.data, roomReflectionsTokenAndData.data);
        }
        return false;
    }

    public final List<RoomReflectionsData> getData() {
        return this.data;
    }

    public final RoomReflectionsToken getToken() {
        return this.token;
    }

    public int hashCode() {
        return (this.token.hashCode() * 31) + this.data.hashCode();
    }

    public String toString() {
        return "RoomReflectionsTokenAndData(token=" + this.token + ", data=" + this.data + ')';
    }
}
