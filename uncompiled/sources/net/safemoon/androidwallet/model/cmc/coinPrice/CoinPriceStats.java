package net.safemoon.androidwallet.model.cmc.coinPrice;

import com.google.gson.annotations.SerializedName;
import java.io.Serializable;
import net.safemoon.androidwallet.model.Status;

/* compiled from: CoinPriceStats.kt */
/* loaded from: classes2.dex */
public final class CoinPriceStats implements Serializable {
    @SerializedName("data")
    private final CoinPriceStatsData data;
    @SerializedName("status")
    private final Status status;

    public CoinPriceStats(CoinPriceStatsData coinPriceStatsData, Status status) {
        fs1.f(status, "status");
        this.data = coinPriceStatsData;
        this.status = status;
    }

    public static /* synthetic */ CoinPriceStats copy$default(CoinPriceStats coinPriceStats, CoinPriceStatsData coinPriceStatsData, Status status, int i, Object obj) {
        if ((i & 1) != 0) {
            coinPriceStatsData = coinPriceStats.data;
        }
        if ((i & 2) != 0) {
            status = coinPriceStats.status;
        }
        return coinPriceStats.copy(coinPriceStatsData, status);
    }

    public final CoinPriceStatsData component1() {
        return this.data;
    }

    public final Status component2() {
        return this.status;
    }

    public final CoinPriceStats copy(CoinPriceStatsData coinPriceStatsData, Status status) {
        fs1.f(status, "status");
        return new CoinPriceStats(coinPriceStatsData, status);
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj instanceof CoinPriceStats) {
            CoinPriceStats coinPriceStats = (CoinPriceStats) obj;
            return fs1.b(this.data, coinPriceStats.data) && fs1.b(this.status, coinPriceStats.status);
        }
        return false;
    }

    public final CoinPriceStatsData getData() {
        return this.data;
    }

    public final Status getStatus() {
        return this.status;
    }

    public int hashCode() {
        CoinPriceStatsData coinPriceStatsData = this.data;
        return ((coinPriceStatsData == null ? 0 : coinPriceStatsData.hashCode()) * 31) + this.status.hashCode();
    }

    public String toString() {
        return "CoinPriceStats(data=" + this.data + ", status=" + this.status + ')';
    }
}
