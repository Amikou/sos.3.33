package net.safemoon.androidwallet.model.cmc.coinPrice;

import com.google.gson.annotations.SerializedName;

/* compiled from: CoinPriceStatsDataQuoteList.kt */
/* loaded from: classes2.dex */
public final class CoinPriceStatsDataQuoteList {
    @SerializedName("time_close")
    private final String closeTimestamp;
    @SerializedName("time_high")
    private final String highTimestamp;
    @SerializedName("time_low")
    private final String lowTimestamp;
    @SerializedName("time_open")
    private final String openTimestamp;
    @SerializedName("quote")
    private CoinPriceStatsDataQuote quote;

    public CoinPriceStatsDataQuoteList(String str, String str2, String str3, String str4, CoinPriceStatsDataQuote coinPriceStatsDataQuote) {
        fs1.f(str, "openTimestamp");
        fs1.f(str2, "closeTimestamp");
        fs1.f(str3, "lowTimestamp");
        fs1.f(str4, "highTimestamp");
        fs1.f(coinPriceStatsDataQuote, "quote");
        this.openTimestamp = str;
        this.closeTimestamp = str2;
        this.lowTimestamp = str3;
        this.highTimestamp = str4;
        this.quote = coinPriceStatsDataQuote;
    }

    public static /* synthetic */ CoinPriceStatsDataQuoteList copy$default(CoinPriceStatsDataQuoteList coinPriceStatsDataQuoteList, String str, String str2, String str3, String str4, CoinPriceStatsDataQuote coinPriceStatsDataQuote, int i, Object obj) {
        if ((i & 1) != 0) {
            str = coinPriceStatsDataQuoteList.openTimestamp;
        }
        if ((i & 2) != 0) {
            str2 = coinPriceStatsDataQuoteList.closeTimestamp;
        }
        String str5 = str2;
        if ((i & 4) != 0) {
            str3 = coinPriceStatsDataQuoteList.lowTimestamp;
        }
        String str6 = str3;
        if ((i & 8) != 0) {
            str4 = coinPriceStatsDataQuoteList.highTimestamp;
        }
        String str7 = str4;
        if ((i & 16) != 0) {
            coinPriceStatsDataQuote = coinPriceStatsDataQuoteList.quote;
        }
        return coinPriceStatsDataQuoteList.copy(str, str5, str6, str7, coinPriceStatsDataQuote);
    }

    public final String component1() {
        return this.openTimestamp;
    }

    public final String component2() {
        return this.closeTimestamp;
    }

    public final String component3() {
        return this.lowTimestamp;
    }

    public final String component4() {
        return this.highTimestamp;
    }

    public final CoinPriceStatsDataQuote component5() {
        return this.quote;
    }

    public final CoinPriceStatsDataQuoteList copy(String str, String str2, String str3, String str4, CoinPriceStatsDataQuote coinPriceStatsDataQuote) {
        fs1.f(str, "openTimestamp");
        fs1.f(str2, "closeTimestamp");
        fs1.f(str3, "lowTimestamp");
        fs1.f(str4, "highTimestamp");
        fs1.f(coinPriceStatsDataQuote, "quote");
        return new CoinPriceStatsDataQuoteList(str, str2, str3, str4, coinPriceStatsDataQuote);
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj instanceof CoinPriceStatsDataQuoteList) {
            CoinPriceStatsDataQuoteList coinPriceStatsDataQuoteList = (CoinPriceStatsDataQuoteList) obj;
            return fs1.b(this.openTimestamp, coinPriceStatsDataQuoteList.openTimestamp) && fs1.b(this.closeTimestamp, coinPriceStatsDataQuoteList.closeTimestamp) && fs1.b(this.lowTimestamp, coinPriceStatsDataQuoteList.lowTimestamp) && fs1.b(this.highTimestamp, coinPriceStatsDataQuoteList.highTimestamp) && fs1.b(this.quote, coinPriceStatsDataQuoteList.quote);
        }
        return false;
    }

    public final String getCloseTimestamp() {
        return this.closeTimestamp;
    }

    public final String getHighTimestamp() {
        return this.highTimestamp;
    }

    public final String getLowTimestamp() {
        return this.lowTimestamp;
    }

    public final String getOpenTimestamp() {
        return this.openTimestamp;
    }

    public final CoinPriceStatsDataQuote getQuote() {
        return this.quote;
    }

    public int hashCode() {
        return (((((((this.openTimestamp.hashCode() * 31) + this.closeTimestamp.hashCode()) * 31) + this.lowTimestamp.hashCode()) * 31) + this.highTimestamp.hashCode()) * 31) + this.quote.hashCode();
    }

    public final void setQuote(CoinPriceStatsDataQuote coinPriceStatsDataQuote) {
        fs1.f(coinPriceStatsDataQuote, "<set-?>");
        this.quote = coinPriceStatsDataQuote;
    }

    public String toString() {
        return "CoinPriceStatsDataQuoteList(openTimestamp=" + this.openTimestamp + ", closeTimestamp=" + this.closeTimestamp + ", lowTimestamp=" + this.lowTimestamp + ", highTimestamp=" + this.highTimestamp + ", quote=" + this.quote + ')';
    }
}
