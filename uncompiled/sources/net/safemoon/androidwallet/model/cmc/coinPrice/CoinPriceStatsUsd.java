package net.safemoon.androidwallet.model.cmc.coinPrice;

import com.google.gson.annotations.SerializedName;
import java.io.Serializable;

/* compiled from: CoinPriceStatsUsd.kt */
/* loaded from: classes2.dex */
public final class CoinPriceStatsUsd implements Serializable {
    @SerializedName("close")
    private float closeValue;
    @SerializedName("high")
    private float highValue;
    @SerializedName("low")
    private float lowValue;
    @SerializedName("open")
    private float openValue;

    public CoinPriceStatsUsd(float f, float f2, float f3, float f4) {
        this.openValue = f;
        this.closeValue = f2;
        this.highValue = f3;
        this.lowValue = f4;
    }

    public static /* synthetic */ CoinPriceStatsUsd copy$default(CoinPriceStatsUsd coinPriceStatsUsd, float f, float f2, float f3, float f4, int i, Object obj) {
        if ((i & 1) != 0) {
            f = coinPriceStatsUsd.openValue;
        }
        if ((i & 2) != 0) {
            f2 = coinPriceStatsUsd.closeValue;
        }
        if ((i & 4) != 0) {
            f3 = coinPriceStatsUsd.highValue;
        }
        if ((i & 8) != 0) {
            f4 = coinPriceStatsUsd.lowValue;
        }
        return coinPriceStatsUsd.copy(f, f2, f3, f4);
    }

    public final float component1() {
        return this.openValue;
    }

    public final float component2() {
        return this.closeValue;
    }

    public final float component3() {
        return this.highValue;
    }

    public final float component4() {
        return this.lowValue;
    }

    public final CoinPriceStatsUsd copy(float f, float f2, float f3, float f4) {
        return new CoinPriceStatsUsd(f, f2, f3, f4);
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj instanceof CoinPriceStatsUsd) {
            CoinPriceStatsUsd coinPriceStatsUsd = (CoinPriceStatsUsd) obj;
            return fs1.b(Float.valueOf(this.openValue), Float.valueOf(coinPriceStatsUsd.openValue)) && fs1.b(Float.valueOf(this.closeValue), Float.valueOf(coinPriceStatsUsd.closeValue)) && fs1.b(Float.valueOf(this.highValue), Float.valueOf(coinPriceStatsUsd.highValue)) && fs1.b(Float.valueOf(this.lowValue), Float.valueOf(coinPriceStatsUsd.lowValue));
        }
        return false;
    }

    public final float getCloseValue() {
        return this.closeValue;
    }

    public final float getHighValue() {
        return this.highValue;
    }

    public final float getLowValue() {
        return this.lowValue;
    }

    public final float getOpenValue() {
        return this.openValue;
    }

    public int hashCode() {
        return (((((Float.floatToIntBits(this.openValue) * 31) + Float.floatToIntBits(this.closeValue)) * 31) + Float.floatToIntBits(this.highValue)) * 31) + Float.floatToIntBits(this.lowValue);
    }

    public final void setCloseValue(float f) {
        this.closeValue = f;
    }

    public final void setHighValue(float f) {
        this.highValue = f;
    }

    public final void setLowValue(float f) {
        this.lowValue = f;
    }

    public final void setOpenValue(float f) {
        this.openValue = f;
    }

    public String toString() {
        return "CoinPriceStatsUsd(openValue=" + this.openValue + ", closeValue=" + this.closeValue + ", highValue=" + this.highValue + ", lowValue=" + this.lowValue + ')';
    }
}
