package net.safemoon.androidwallet.model.cmc.coinPrice;

import com.google.gson.annotations.SerializedName;
import java.io.Serializable;

/* compiled from: CoinPriceStatsDataQuote.kt */
/* loaded from: classes2.dex */
public final class CoinPriceStatsDataQuote implements Serializable {
    @SerializedName("USD")
    private final CoinPriceStatsUsd usd;

    public CoinPriceStatsDataQuote(CoinPriceStatsUsd coinPriceStatsUsd) {
        fs1.f(coinPriceStatsUsd, "usd");
        this.usd = coinPriceStatsUsd;
    }

    public static /* synthetic */ CoinPriceStatsDataQuote copy$default(CoinPriceStatsDataQuote coinPriceStatsDataQuote, CoinPriceStatsUsd coinPriceStatsUsd, int i, Object obj) {
        if ((i & 1) != 0) {
            coinPriceStatsUsd = coinPriceStatsDataQuote.usd;
        }
        return coinPriceStatsDataQuote.copy(coinPriceStatsUsd);
    }

    public final CoinPriceStatsUsd component1() {
        return this.usd;
    }

    public final CoinPriceStatsDataQuote copy(CoinPriceStatsUsd coinPriceStatsUsd) {
        fs1.f(coinPriceStatsUsd, "usd");
        return new CoinPriceStatsDataQuote(coinPriceStatsUsd);
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        return (obj instanceof CoinPriceStatsDataQuote) && fs1.b(this.usd, ((CoinPriceStatsDataQuote) obj).usd);
    }

    public final CoinPriceStatsUsd getUsd() {
        return this.usd;
    }

    public int hashCode() {
        return this.usd.hashCode();
    }

    public String toString() {
        return "CoinPriceStatsDataQuote(usd=" + this.usd + ')';
    }
}
