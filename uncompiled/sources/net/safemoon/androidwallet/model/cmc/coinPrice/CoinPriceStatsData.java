package net.safemoon.androidwallet.model.cmc.coinPrice;

import com.google.gson.annotations.SerializedName;
import java.io.Serializable;
import java.util.List;
import org.web3j.ens.contracts.generated.PublicResolver;

/* compiled from: CoinPriceStatsData.kt */
/* loaded from: classes2.dex */
public final class CoinPriceStatsData implements Serializable {
    @SerializedName("id")
    private final int id;
    @SerializedName(PublicResolver.FUNC_NAME)
    private final String name;
    @SerializedName("quotes")
    private final List<CoinPriceStatsDataQuoteList> quote;
    @SerializedName("symbol")
    private String symbol;

    public CoinPriceStatsData(int i, String str, String str2, List<CoinPriceStatsDataQuoteList> list) {
        fs1.f(str, PublicResolver.FUNC_NAME);
        fs1.f(str2, "symbol");
        fs1.f(list, "quote");
        this.id = i;
        this.name = str;
        this.symbol = str2;
        this.quote = list;
    }

    /* JADX WARN: Multi-variable type inference failed */
    public static /* synthetic */ CoinPriceStatsData copy$default(CoinPriceStatsData coinPriceStatsData, int i, String str, String str2, List list, int i2, Object obj) {
        if ((i2 & 1) != 0) {
            i = coinPriceStatsData.id;
        }
        if ((i2 & 2) != 0) {
            str = coinPriceStatsData.name;
        }
        if ((i2 & 4) != 0) {
            str2 = coinPriceStatsData.symbol;
        }
        if ((i2 & 8) != 0) {
            list = coinPriceStatsData.quote;
        }
        return coinPriceStatsData.copy(i, str, str2, list);
    }

    public final int component1() {
        return this.id;
    }

    public final String component2() {
        return this.name;
    }

    public final String component3() {
        return this.symbol;
    }

    public final List<CoinPriceStatsDataQuoteList> component4() {
        return this.quote;
    }

    public final CoinPriceStatsData copy(int i, String str, String str2, List<CoinPriceStatsDataQuoteList> list) {
        fs1.f(str, PublicResolver.FUNC_NAME);
        fs1.f(str2, "symbol");
        fs1.f(list, "quote");
        return new CoinPriceStatsData(i, str, str2, list);
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj instanceof CoinPriceStatsData) {
            CoinPriceStatsData coinPriceStatsData = (CoinPriceStatsData) obj;
            return this.id == coinPriceStatsData.id && fs1.b(this.name, coinPriceStatsData.name) && fs1.b(this.symbol, coinPriceStatsData.symbol) && fs1.b(this.quote, coinPriceStatsData.quote);
        }
        return false;
    }

    public final int getId() {
        return this.id;
    }

    public final String getName() {
        return this.name;
    }

    public final List<CoinPriceStatsDataQuoteList> getQuote() {
        return this.quote;
    }

    public final String getSymbol() {
        return this.symbol;
    }

    public int hashCode() {
        return (((((this.id * 31) + this.name.hashCode()) * 31) + this.symbol.hashCode()) * 31) + this.quote.hashCode();
    }

    public final void setSymbol(String str) {
        fs1.f(str, "<set-?>");
        this.symbol = str;
    }

    public String toString() {
        return "CoinPriceStatsData(id=" + this.id + ", name=" + this.name + ", symbol=" + this.symbol + ", quote=" + this.quote + ')';
    }
}
