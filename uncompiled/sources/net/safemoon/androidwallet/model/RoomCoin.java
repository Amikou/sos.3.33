package net.safemoon.androidwallet.model;

import java.io.Serializable;
import org.web3j.ens.contracts.generated.PublicResolver;

/* compiled from: RoomCoin.kt */
/* loaded from: classes2.dex */
public class RoomCoin implements Serializable {
    private final String cmcData;
    private final int cmcId;
    private final String name;
    private final String slug;
    private final String symbol;

    public RoomCoin(int i, String str, String str2, String str3, String str4) {
        fs1.f(str, "symbol");
        fs1.f(str2, PublicResolver.FUNC_NAME);
        fs1.f(str3, "slug");
        fs1.f(str4, "cmcData");
        this.cmcId = i;
        this.symbol = str;
        this.name = str2;
        this.slug = str3;
        this.cmcData = str4;
    }

    public String getCmcData() {
        return this.cmcData;
    }

    public int getCmcId() {
        return this.cmcId;
    }

    public String getName() {
        return this.name;
    }

    public String getSlug() {
        return this.slug;
    }

    public String getSymbol() {
        return this.symbol;
    }
}
