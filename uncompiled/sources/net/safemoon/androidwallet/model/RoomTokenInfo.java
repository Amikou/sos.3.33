package net.safemoon.androidwallet.model;

/* compiled from: RoomTokenInfo.kt */
/* loaded from: classes2.dex */
public final class RoomTokenInfo {
    private final int chainId;
    private final Integer cmcId;
    private Long id;
    private final String slug;
    private final String symbolWithType;

    public RoomTokenInfo(Long l, String str, int i, String str2, Integer num) {
        fs1.f(str, "symbolWithType");
        this.id = l;
        this.symbolWithType = str;
        this.chainId = i;
        this.slug = str2;
        this.cmcId = num;
    }

    public static /* synthetic */ RoomTokenInfo copy$default(RoomTokenInfo roomTokenInfo, Long l, String str, int i, String str2, Integer num, int i2, Object obj) {
        if ((i2 & 1) != 0) {
            l = roomTokenInfo.id;
        }
        if ((i2 & 2) != 0) {
            str = roomTokenInfo.symbolWithType;
        }
        String str3 = str;
        if ((i2 & 4) != 0) {
            i = roomTokenInfo.chainId;
        }
        int i3 = i;
        if ((i2 & 8) != 0) {
            str2 = roomTokenInfo.slug;
        }
        String str4 = str2;
        if ((i2 & 16) != 0) {
            num = roomTokenInfo.cmcId;
        }
        return roomTokenInfo.copy(l, str3, i3, str4, num);
    }

    public final Long component1() {
        return this.id;
    }

    public final String component2() {
        return this.symbolWithType;
    }

    public final int component3() {
        return this.chainId;
    }

    public final String component4() {
        return this.slug;
    }

    public final Integer component5() {
        return this.cmcId;
    }

    public final RoomTokenInfo copy(Long l, String str, int i, String str2, Integer num) {
        fs1.f(str, "symbolWithType");
        return new RoomTokenInfo(l, str, i, str2, num);
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj instanceof RoomTokenInfo) {
            RoomTokenInfo roomTokenInfo = (RoomTokenInfo) obj;
            return fs1.b(this.id, roomTokenInfo.id) && fs1.b(this.symbolWithType, roomTokenInfo.symbolWithType) && this.chainId == roomTokenInfo.chainId && fs1.b(this.slug, roomTokenInfo.slug) && fs1.b(this.cmcId, roomTokenInfo.cmcId);
        }
        return false;
    }

    public final int getChainId() {
        return this.chainId;
    }

    public final Integer getCmcId() {
        return this.cmcId;
    }

    public final Long getId() {
        return this.id;
    }

    public final String getSlug() {
        return this.slug;
    }

    public final String getSymbolWithType() {
        return this.symbolWithType;
    }

    public int hashCode() {
        Long l = this.id;
        int hashCode = (((((l == null ? 0 : l.hashCode()) * 31) + this.symbolWithType.hashCode()) * 31) + this.chainId) * 31;
        String str = this.slug;
        int hashCode2 = (hashCode + (str == null ? 0 : str.hashCode())) * 31;
        Integer num = this.cmcId;
        return hashCode2 + (num != null ? num.hashCode() : 0);
    }

    public final void setId(Long l) {
        this.id = l;
    }

    public String toString() {
        return "RoomTokenInfo(id=" + this.id + ", symbolWithType=" + this.symbolWithType + ", chainId=" + this.chainId + ", slug=" + ((Object) this.slug) + ", cmcId=" + this.cmcId + ')';
    }

    public /* synthetic */ RoomTokenInfo(Long l, String str, int i, String str2, Integer num, int i2, qi0 qi0Var) {
        this((i2 & 1) != 0 ? null : l, str, i, (i2 & 8) != 0 ? null : str2, (i2 & 16) != 0 ? null : num);
    }
}
