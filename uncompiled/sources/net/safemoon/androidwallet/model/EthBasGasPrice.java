package net.safemoon.androidwallet.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.math.RoundingMode;

/* compiled from: EthBasGasPrice.kt */
/* loaded from: classes2.dex */
public final class EthBasGasPrice {
    @SerializedName("result")
    @Expose
    private final String result;

    public EthBasGasPrice(String str) {
        fs1.f(str, "result");
        this.result = str;
    }

    public static /* synthetic */ EthBasGasPrice copy$default(EthBasGasPrice ethBasGasPrice, String str, int i, Object obj) {
        if ((i & 1) != 0) {
            str = ethBasGasPrice.result;
        }
        return ethBasGasPrice.copy(str);
    }

    public final String component1() {
        return this.result;
    }

    public final EthBasGasPrice copy(String str) {
        fs1.f(str, "result");
        return new EthBasGasPrice(str);
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        return (obj instanceof EthBasGasPrice) && fs1.b(this.result, ((EthBasGasPrice) obj).result);
    }

    public final String getResult() {
        return this.result;
    }

    public int hashCode() {
        return this.result.hashCode();
    }

    public String toString() {
        return "EthBasGasPrice(result=" + this.result + ')';
    }

    /* renamed from: getResult  reason: collision with other method in class */
    public final BigDecimal m61getResult() {
        try {
            BigInteger decodeQuantity = ej2.decodeQuantity(this.result);
            fs1.e(decodeQuantity, "decodeQuantity(result)");
            BigDecimal bigDecimal = new BigDecimal(decodeQuantity);
            BigDecimal pow = BigDecimal.TEN.pow(9);
            fs1.e(pow, "TEN.pow(9)");
            BigDecimal divide = bigDecimal.divide(pow, RoundingMode.HALF_EVEN);
            fs1.e(divide, "this.divide(other, RoundingMode.HALF_EVEN)");
            return divide;
        } catch (Exception unused) {
            return null;
        }
    }
}
