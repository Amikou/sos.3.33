package net.safemoon.androidwallet.model.tokensInfo;

import com.google.gson.annotations.SerializedName;

/* compiled from: CurrencyTokenInfoResult.kt */
/* loaded from: classes2.dex */
public final class CurrencyTokenInfoResult {
    @SerializedName("data")
    private final CurrencyTokenInfo data;

    public CurrencyTokenInfoResult() {
        this(null, 1, null);
    }

    public CurrencyTokenInfoResult(CurrencyTokenInfo currencyTokenInfo) {
        this.data = currencyTokenInfo;
    }

    public static /* synthetic */ CurrencyTokenInfoResult copy$default(CurrencyTokenInfoResult currencyTokenInfoResult, CurrencyTokenInfo currencyTokenInfo, int i, Object obj) {
        if ((i & 1) != 0) {
            currencyTokenInfo = currencyTokenInfoResult.data;
        }
        return currencyTokenInfoResult.copy(currencyTokenInfo);
    }

    public final CurrencyTokenInfo component1() {
        return this.data;
    }

    public final CurrencyTokenInfoResult copy(CurrencyTokenInfo currencyTokenInfo) {
        return new CurrencyTokenInfoResult(currencyTokenInfo);
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        return (obj instanceof CurrencyTokenInfoResult) && fs1.b(this.data, ((CurrencyTokenInfoResult) obj).data);
    }

    public final CurrencyTokenInfo getData() {
        return this.data;
    }

    public int hashCode() {
        CurrencyTokenInfo currencyTokenInfo = this.data;
        if (currencyTokenInfo == null) {
            return 0;
        }
        return currencyTokenInfo.hashCode();
    }

    public String toString() {
        return "CurrencyTokenInfoResult(data=" + this.data + ')';
    }

    public /* synthetic */ CurrencyTokenInfoResult(CurrencyTokenInfo currencyTokenInfo, int i, qi0 qi0Var) {
        this((i & 1) != 0 ? null : currencyTokenInfo);
    }
}
