package net.safemoon.androidwallet.model.tokensInfo;

import com.google.gson.annotations.SerializedName;
import java.util.List;

/* compiled from: CurrencyTokensInfoResult.kt */
/* loaded from: classes2.dex */
public final class CurrencyTokensInfoResult {
    @SerializedName("data")
    private final List<CurrencyTokenInfo> data;

    public CurrencyTokensInfoResult() {
        this(null, 1, null);
    }

    public CurrencyTokensInfoResult(List<CurrencyTokenInfo> list) {
        this.data = list;
    }

    /* JADX WARN: Multi-variable type inference failed */
    public static /* synthetic */ CurrencyTokensInfoResult copy$default(CurrencyTokensInfoResult currencyTokensInfoResult, List list, int i, Object obj) {
        if ((i & 1) != 0) {
            list = currencyTokensInfoResult.data;
        }
        return currencyTokensInfoResult.copy(list);
    }

    public final List<CurrencyTokenInfo> component1() {
        return this.data;
    }

    public final CurrencyTokensInfoResult copy(List<CurrencyTokenInfo> list) {
        return new CurrencyTokensInfoResult(list);
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        return (obj instanceof CurrencyTokensInfoResult) && fs1.b(this.data, ((CurrencyTokensInfoResult) obj).data);
    }

    public final List<CurrencyTokenInfo> getData() {
        return this.data;
    }

    public int hashCode() {
        List<CurrencyTokenInfo> list = this.data;
        if (list == null) {
            return 0;
        }
        return list.hashCode();
    }

    public String toString() {
        return "CurrencyTokensInfoResult(data=" + this.data + ')';
    }

    public /* synthetic */ CurrencyTokensInfoResult(List list, int i, qi0 qi0Var) {
        this((i & 1) != 0 ? null : list);
    }
}
