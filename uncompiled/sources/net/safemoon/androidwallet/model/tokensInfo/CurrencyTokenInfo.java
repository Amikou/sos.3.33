package net.safemoon.androidwallet.model.tokensInfo;

import com.google.gson.annotations.SerializedName;
import org.web3j.abi.datatypes.Address;
import org.web3j.ens.contracts.generated.PublicResolver;

/* compiled from: CurrencyTokenInfo.kt */
/* loaded from: classes2.dex */
public final class CurrencyTokenInfo {
    @SerializedName("baseToken")
    private final BaseToken baseToken;
    @SerializedName("changeId")
    private final String changeId;
    @SerializedName("dexId")
    private final String dexId;
    @SerializedName("fdv")
    private final Double fdv;
    @SerializedName("liquidity")
    private final Liquidity liquidity;
    @SerializedName("pairAddress")
    private final String pairAddress;
    @SerializedName("priceChange")
    private final PriceChange priceChange;
    @SerializedName("priceNative")
    private final String priceNative;
    @SerializedName("priceUsd")
    private final String priceUsd;
    @SerializedName("quoteToken")
    private final QuoteToken quoteToken;
    @SerializedName("tokenAddress")
    private final String tokenAddress;
    @SerializedName("txns")
    private final Txns txns;
    @SerializedName("url")
    private final String url;
    @SerializedName("version")
    private final Integer version;
    @SerializedName("volume")
    private final Volume volume;

    /* compiled from: CurrencyTokenInfo.kt */
    /* loaded from: classes2.dex */
    public static final class BaseToken {
        @SerializedName(Address.TYPE_NAME)
        private final String address;
        @SerializedName(PublicResolver.FUNC_NAME)
        private final String name;
        @SerializedName("symbol")
        private final String symbol;

        public BaseToken(String str, String str2, String str3) {
            fs1.f(str, Address.TYPE_NAME);
            fs1.f(str2, PublicResolver.FUNC_NAME);
            fs1.f(str3, "symbol");
            this.address = str;
            this.name = str2;
            this.symbol = str3;
        }

        public static /* synthetic */ BaseToken copy$default(BaseToken baseToken, String str, String str2, String str3, int i, Object obj) {
            if ((i & 1) != 0) {
                str = baseToken.address;
            }
            if ((i & 2) != 0) {
                str2 = baseToken.name;
            }
            if ((i & 4) != 0) {
                str3 = baseToken.symbol;
            }
            return baseToken.copy(str, str2, str3);
        }

        public final String component1() {
            return this.address;
        }

        public final String component2() {
            return this.name;
        }

        public final String component3() {
            return this.symbol;
        }

        public final BaseToken copy(String str, String str2, String str3) {
            fs1.f(str, Address.TYPE_NAME);
            fs1.f(str2, PublicResolver.FUNC_NAME);
            fs1.f(str3, "symbol");
            return new BaseToken(str, str2, str3);
        }

        public boolean equals(Object obj) {
            if (this == obj) {
                return true;
            }
            if (obj instanceof BaseToken) {
                BaseToken baseToken = (BaseToken) obj;
                return fs1.b(this.address, baseToken.address) && fs1.b(this.name, baseToken.name) && fs1.b(this.symbol, baseToken.symbol);
            }
            return false;
        }

        public final String getAddress() {
            return this.address;
        }

        public final String getName() {
            return this.name;
        }

        public final String getSymbol() {
            return this.symbol;
        }

        public int hashCode() {
            return (((this.address.hashCode() * 31) + this.name.hashCode()) * 31) + this.symbol.hashCode();
        }

        public String toString() {
            return "BaseToken(address=" + this.address + ", name=" + this.name + ", symbol=" + this.symbol + ')';
        }
    }

    /* compiled from: CurrencyTokenInfo.kt */
    /* loaded from: classes2.dex */
    public static final class Liquidity {
        @SerializedName("base")
        private final Double base;
        @SerializedName("quote")
        private final Double quote;
        @SerializedName("usd")
        private final Double usd;

        public Liquidity(Double d, Double d2, Double d3) {
            this.usd = d;
            this.base = d2;
            this.quote = d3;
        }

        public static /* synthetic */ Liquidity copy$default(Liquidity liquidity, Double d, Double d2, Double d3, int i, Object obj) {
            if ((i & 1) != 0) {
                d = liquidity.usd;
            }
            if ((i & 2) != 0) {
                d2 = liquidity.base;
            }
            if ((i & 4) != 0) {
                d3 = liquidity.quote;
            }
            return liquidity.copy(d, d2, d3);
        }

        public final Double component1() {
            return this.usd;
        }

        public final Double component2() {
            return this.base;
        }

        public final Double component3() {
            return this.quote;
        }

        public final Liquidity copy(Double d, Double d2, Double d3) {
            return new Liquidity(d, d2, d3);
        }

        public boolean equals(Object obj) {
            if (this == obj) {
                return true;
            }
            if (obj instanceof Liquidity) {
                Liquidity liquidity = (Liquidity) obj;
                return fs1.b(this.usd, liquidity.usd) && fs1.b(this.base, liquidity.base) && fs1.b(this.quote, liquidity.quote);
            }
            return false;
        }

        public final Double getBase() {
            return this.base;
        }

        public final Double getQuote() {
            return this.quote;
        }

        public final Double getUsd() {
            return this.usd;
        }

        public int hashCode() {
            Double d = this.usd;
            int hashCode = (d == null ? 0 : d.hashCode()) * 31;
            Double d2 = this.base;
            int hashCode2 = (hashCode + (d2 == null ? 0 : d2.hashCode())) * 31;
            Double d3 = this.quote;
            return hashCode2 + (d3 != null ? d3.hashCode() : 0);
        }

        public String toString() {
            return "Liquidity(usd=" + this.usd + ", base=" + this.base + ", quote=" + this.quote + ')';
        }
    }

    /* compiled from: CurrencyTokenInfo.kt */
    /* loaded from: classes2.dex */
    public static final class PriceChange {
        @SerializedName("h1")
        private final Double h1;
        @SerializedName("h24")
        private final Double h24;
        @SerializedName("h6")
        private final Double h6;
        @SerializedName("m5")
        private final Double m5;

        public PriceChange(Double d, Double d2, Double d3, Double d4) {
            this.h24 = d;
            this.h6 = d2;
            this.h1 = d3;
            this.m5 = d4;
        }

        public static /* synthetic */ PriceChange copy$default(PriceChange priceChange, Double d, Double d2, Double d3, Double d4, int i, Object obj) {
            if ((i & 1) != 0) {
                d = priceChange.h24;
            }
            if ((i & 2) != 0) {
                d2 = priceChange.h6;
            }
            if ((i & 4) != 0) {
                d3 = priceChange.h1;
            }
            if ((i & 8) != 0) {
                d4 = priceChange.m5;
            }
            return priceChange.copy(d, d2, d3, d4);
        }

        public final Double component1() {
            return this.h24;
        }

        public final Double component2() {
            return this.h6;
        }

        public final Double component3() {
            return this.h1;
        }

        public final Double component4() {
            return this.m5;
        }

        public final PriceChange copy(Double d, Double d2, Double d3, Double d4) {
            return new PriceChange(d, d2, d3, d4);
        }

        public boolean equals(Object obj) {
            if (this == obj) {
                return true;
            }
            if (obj instanceof PriceChange) {
                PriceChange priceChange = (PriceChange) obj;
                return fs1.b(this.h24, priceChange.h24) && fs1.b(this.h6, priceChange.h6) && fs1.b(this.h1, priceChange.h1) && fs1.b(this.m5, priceChange.m5);
            }
            return false;
        }

        public final Double getH1() {
            return this.h1;
        }

        public final Double getH24() {
            return this.h24;
        }

        public final Double getH6() {
            return this.h6;
        }

        public final Double getM5() {
            return this.m5;
        }

        public int hashCode() {
            Double d = this.h24;
            int hashCode = (d == null ? 0 : d.hashCode()) * 31;
            Double d2 = this.h6;
            int hashCode2 = (hashCode + (d2 == null ? 0 : d2.hashCode())) * 31;
            Double d3 = this.h1;
            int hashCode3 = (hashCode2 + (d3 == null ? 0 : d3.hashCode())) * 31;
            Double d4 = this.m5;
            return hashCode3 + (d4 != null ? d4.hashCode() : 0);
        }

        public String toString() {
            return "PriceChange(h24=" + this.h24 + ", h6=" + this.h6 + ", h1=" + this.h1 + ", m5=" + this.m5 + ')';
        }
    }

    /* compiled from: CurrencyTokenInfo.kt */
    /* loaded from: classes2.dex */
    public static final class QuoteToken {
        @SerializedName("symbol")
        private final String symbol;

        public QuoteToken(String str) {
            fs1.f(str, "symbol");
            this.symbol = str;
        }

        public static /* synthetic */ QuoteToken copy$default(QuoteToken quoteToken, String str, int i, Object obj) {
            if ((i & 1) != 0) {
                str = quoteToken.symbol;
            }
            return quoteToken.copy(str);
        }

        public final String component1() {
            return this.symbol;
        }

        public final QuoteToken copy(String str) {
            fs1.f(str, "symbol");
            return new QuoteToken(str);
        }

        public boolean equals(Object obj) {
            if (this == obj) {
                return true;
            }
            return (obj instanceof QuoteToken) && fs1.b(this.symbol, ((QuoteToken) obj).symbol);
        }

        public final String getSymbol() {
            return this.symbol;
        }

        public int hashCode() {
            return this.symbol.hashCode();
        }

        public String toString() {
            return "QuoteToken(symbol=" + this.symbol + ')';
        }
    }

    /* compiled from: CurrencyTokenInfo.kt */
    /* loaded from: classes2.dex */
    public static final class Txns {
        @SerializedName("h1")
        private final TxnsUnit h1;
        @SerializedName("h24")
        private final TxnsUnit h24;
        @SerializedName("h6")
        private final TxnsUnit h6;
        @SerializedName("m5")
        private final TxnsUnit m5;

        public Txns(TxnsUnit txnsUnit, TxnsUnit txnsUnit2, TxnsUnit txnsUnit3, TxnsUnit txnsUnit4) {
            this.h24 = txnsUnit;
            this.h6 = txnsUnit2;
            this.h1 = txnsUnit3;
            this.m5 = txnsUnit4;
        }

        public static /* synthetic */ Txns copy$default(Txns txns, TxnsUnit txnsUnit, TxnsUnit txnsUnit2, TxnsUnit txnsUnit3, TxnsUnit txnsUnit4, int i, Object obj) {
            if ((i & 1) != 0) {
                txnsUnit = txns.h24;
            }
            if ((i & 2) != 0) {
                txnsUnit2 = txns.h6;
            }
            if ((i & 4) != 0) {
                txnsUnit3 = txns.h1;
            }
            if ((i & 8) != 0) {
                txnsUnit4 = txns.m5;
            }
            return txns.copy(txnsUnit, txnsUnit2, txnsUnit3, txnsUnit4);
        }

        public final TxnsUnit component1() {
            return this.h24;
        }

        public final TxnsUnit component2() {
            return this.h6;
        }

        public final TxnsUnit component3() {
            return this.h1;
        }

        public final TxnsUnit component4() {
            return this.m5;
        }

        public final Txns copy(TxnsUnit txnsUnit, TxnsUnit txnsUnit2, TxnsUnit txnsUnit3, TxnsUnit txnsUnit4) {
            return new Txns(txnsUnit, txnsUnit2, txnsUnit3, txnsUnit4);
        }

        public boolean equals(Object obj) {
            if (this == obj) {
                return true;
            }
            if (obj instanceof Txns) {
                Txns txns = (Txns) obj;
                return fs1.b(this.h24, txns.h24) && fs1.b(this.h6, txns.h6) && fs1.b(this.h1, txns.h1) && fs1.b(this.m5, txns.m5);
            }
            return false;
        }

        public final TxnsUnit getH1() {
            return this.h1;
        }

        public final TxnsUnit getH24() {
            return this.h24;
        }

        public final TxnsUnit getH6() {
            return this.h6;
        }

        public final TxnsUnit getM5() {
            return this.m5;
        }

        public int hashCode() {
            TxnsUnit txnsUnit = this.h24;
            int hashCode = (txnsUnit == null ? 0 : txnsUnit.hashCode()) * 31;
            TxnsUnit txnsUnit2 = this.h6;
            int hashCode2 = (hashCode + (txnsUnit2 == null ? 0 : txnsUnit2.hashCode())) * 31;
            TxnsUnit txnsUnit3 = this.h1;
            int hashCode3 = (hashCode2 + (txnsUnit3 == null ? 0 : txnsUnit3.hashCode())) * 31;
            TxnsUnit txnsUnit4 = this.m5;
            return hashCode3 + (txnsUnit4 != null ? txnsUnit4.hashCode() : 0);
        }

        public String toString() {
            return "Txns(h24=" + this.h24 + ", h6=" + this.h6 + ", h1=" + this.h1 + ", m5=" + this.m5 + ')';
        }
    }

    /* compiled from: CurrencyTokenInfo.kt */
    /* loaded from: classes2.dex */
    public static final class TxnsUnit {
        @SerializedName("buys")
        private final Integer buys;
        @SerializedName("sells")
        private final Integer sells;

        public TxnsUnit(Integer num, Integer num2) {
            this.buys = num;
            this.sells = num2;
        }

        public static /* synthetic */ TxnsUnit copy$default(TxnsUnit txnsUnit, Integer num, Integer num2, int i, Object obj) {
            if ((i & 1) != 0) {
                num = txnsUnit.buys;
            }
            if ((i & 2) != 0) {
                num2 = txnsUnit.sells;
            }
            return txnsUnit.copy(num, num2);
        }

        public final Integer component1() {
            return this.buys;
        }

        public final Integer component2() {
            return this.sells;
        }

        public final TxnsUnit copy(Integer num, Integer num2) {
            return new TxnsUnit(num, num2);
        }

        public boolean equals(Object obj) {
            if (this == obj) {
                return true;
            }
            if (obj instanceof TxnsUnit) {
                TxnsUnit txnsUnit = (TxnsUnit) obj;
                return fs1.b(this.buys, txnsUnit.buys) && fs1.b(this.sells, txnsUnit.sells);
            }
            return false;
        }

        public final Integer getBuys() {
            return this.buys;
        }

        public final Integer getSells() {
            return this.sells;
        }

        public int hashCode() {
            Integer num = this.buys;
            int hashCode = (num == null ? 0 : num.hashCode()) * 31;
            Integer num2 = this.sells;
            return hashCode + (num2 != null ? num2.hashCode() : 0);
        }

        public String toString() {
            return "TxnsUnit(buys=" + this.buys + ", sells=" + this.sells + ')';
        }
    }

    /* compiled from: CurrencyTokenInfo.kt */
    /* loaded from: classes2.dex */
    public static final class Volume {
        @SerializedName("h1")
        private final Double h1;
        @SerializedName("h24")
        private final Double h24;
        @SerializedName("h6")
        private final Double h6;
        @SerializedName("m5")
        private final Double m5;

        public Volume(Double d, Double d2, Double d3, Double d4) {
            this.h24 = d;
            this.h6 = d2;
            this.h1 = d3;
            this.m5 = d4;
        }

        public static /* synthetic */ Volume copy$default(Volume volume, Double d, Double d2, Double d3, Double d4, int i, Object obj) {
            if ((i & 1) != 0) {
                d = volume.h24;
            }
            if ((i & 2) != 0) {
                d2 = volume.h6;
            }
            if ((i & 4) != 0) {
                d3 = volume.h1;
            }
            if ((i & 8) != 0) {
                d4 = volume.m5;
            }
            return volume.copy(d, d2, d3, d4);
        }

        public final Double component1() {
            return this.h24;
        }

        public final Double component2() {
            return this.h6;
        }

        public final Double component3() {
            return this.h1;
        }

        public final Double component4() {
            return this.m5;
        }

        public final Volume copy(Double d, Double d2, Double d3, Double d4) {
            return new Volume(d, d2, d3, d4);
        }

        public boolean equals(Object obj) {
            if (this == obj) {
                return true;
            }
            if (obj instanceof Volume) {
                Volume volume = (Volume) obj;
                return fs1.b(this.h24, volume.h24) && fs1.b(this.h6, volume.h6) && fs1.b(this.h1, volume.h1) && fs1.b(this.m5, volume.m5);
            }
            return false;
        }

        public final Double getH1() {
            return this.h1;
        }

        public final Double getH24() {
            return this.h24;
        }

        public final Double getH6() {
            return this.h6;
        }

        public final Double getM5() {
            return this.m5;
        }

        public int hashCode() {
            Double d = this.h24;
            int hashCode = (d == null ? 0 : d.hashCode()) * 31;
            Double d2 = this.h6;
            int hashCode2 = (hashCode + (d2 == null ? 0 : d2.hashCode())) * 31;
            Double d3 = this.h1;
            int hashCode3 = (hashCode2 + (d3 == null ? 0 : d3.hashCode())) * 31;
            Double d4 = this.m5;
            return hashCode3 + (d4 != null ? d4.hashCode() : 0);
        }

        public String toString() {
            return "Volume(h24=" + this.h24 + ", h6=" + this.h6 + ", h1=" + this.h1 + ", m5=" + this.m5 + ')';
        }
    }

    public CurrencyTokenInfo(String str, String str2, String str3, String str4, String str5, BaseToken baseToken, QuoteToken quoteToken, String str6, String str7, Txns txns, Volume volume, PriceChange priceChange, Liquidity liquidity, Double d, Integer num) {
        fs1.f(str4, "dexId");
        fs1.f(str5, "url");
        fs1.f(baseToken, "baseToken");
        fs1.f(quoteToken, "quoteToken");
        this.tokenAddress = str;
        this.pairAddress = str2;
        this.changeId = str3;
        this.dexId = str4;
        this.url = str5;
        this.baseToken = baseToken;
        this.quoteToken = quoteToken;
        this.priceNative = str6;
        this.priceUsd = str7;
        this.txns = txns;
        this.volume = volume;
        this.priceChange = priceChange;
        this.liquidity = liquidity;
        this.fdv = d;
        this.version = num;
    }

    public final String component1() {
        return this.tokenAddress;
    }

    public final Txns component10() {
        return this.txns;
    }

    public final Volume component11() {
        return this.volume;
    }

    public final PriceChange component12() {
        return this.priceChange;
    }

    public final Liquidity component13() {
        return this.liquidity;
    }

    public final Double component14() {
        return this.fdv;
    }

    public final Integer component15() {
        return this.version;
    }

    public final String component2() {
        return this.pairAddress;
    }

    public final String component3() {
        return this.changeId;
    }

    public final String component4() {
        return this.dexId;
    }

    public final String component5() {
        return this.url;
    }

    public final BaseToken component6() {
        return this.baseToken;
    }

    public final QuoteToken component7() {
        return this.quoteToken;
    }

    public final String component8() {
        return this.priceNative;
    }

    public final String component9() {
        return this.priceUsd;
    }

    public final CurrencyTokenInfo copy(String str, String str2, String str3, String str4, String str5, BaseToken baseToken, QuoteToken quoteToken, String str6, String str7, Txns txns, Volume volume, PriceChange priceChange, Liquidity liquidity, Double d, Integer num) {
        fs1.f(str4, "dexId");
        fs1.f(str5, "url");
        fs1.f(baseToken, "baseToken");
        fs1.f(quoteToken, "quoteToken");
        return new CurrencyTokenInfo(str, str2, str3, str4, str5, baseToken, quoteToken, str6, str7, txns, volume, priceChange, liquidity, d, num);
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj instanceof CurrencyTokenInfo) {
            CurrencyTokenInfo currencyTokenInfo = (CurrencyTokenInfo) obj;
            return fs1.b(this.tokenAddress, currencyTokenInfo.tokenAddress) && fs1.b(this.pairAddress, currencyTokenInfo.pairAddress) && fs1.b(this.changeId, currencyTokenInfo.changeId) && fs1.b(this.dexId, currencyTokenInfo.dexId) && fs1.b(this.url, currencyTokenInfo.url) && fs1.b(this.baseToken, currencyTokenInfo.baseToken) && fs1.b(this.quoteToken, currencyTokenInfo.quoteToken) && fs1.b(this.priceNative, currencyTokenInfo.priceNative) && fs1.b(this.priceUsd, currencyTokenInfo.priceUsd) && fs1.b(this.txns, currencyTokenInfo.txns) && fs1.b(this.volume, currencyTokenInfo.volume) && fs1.b(this.priceChange, currencyTokenInfo.priceChange) && fs1.b(this.liquidity, currencyTokenInfo.liquidity) && fs1.b(this.fdv, currencyTokenInfo.fdv) && fs1.b(this.version, currencyTokenInfo.version);
        }
        return false;
    }

    public final BaseToken getBaseToken() {
        return this.baseToken;
    }

    public final String getChangeId() {
        return this.changeId;
    }

    public final String getDexId() {
        return this.dexId;
    }

    public final Double getFdv() {
        return this.fdv;
    }

    public final Liquidity getLiquidity() {
        return this.liquidity;
    }

    public final String getPairAddress() {
        return this.pairAddress;
    }

    public final PriceChange getPriceChange() {
        return this.priceChange;
    }

    public final String getPriceNative() {
        return this.priceNative;
    }

    public final String getPriceUsd() {
        return this.priceUsd;
    }

    public final QuoteToken getQuoteToken() {
        return this.quoteToken;
    }

    public final String getTokenAddress() {
        return this.tokenAddress;
    }

    public final Txns getTxns() {
        return this.txns;
    }

    public final String getUrl() {
        return this.url;
    }

    public final Integer getVersion() {
        return this.version;
    }

    public final Volume getVolume() {
        return this.volume;
    }

    public int hashCode() {
        String str = this.tokenAddress;
        int hashCode = (str == null ? 0 : str.hashCode()) * 31;
        String str2 = this.pairAddress;
        int hashCode2 = (hashCode + (str2 == null ? 0 : str2.hashCode())) * 31;
        String str3 = this.changeId;
        int hashCode3 = (((((((((hashCode2 + (str3 == null ? 0 : str3.hashCode())) * 31) + this.dexId.hashCode()) * 31) + this.url.hashCode()) * 31) + this.baseToken.hashCode()) * 31) + this.quoteToken.hashCode()) * 31;
        String str4 = this.priceNative;
        int hashCode4 = (hashCode3 + (str4 == null ? 0 : str4.hashCode())) * 31;
        String str5 = this.priceUsd;
        int hashCode5 = (hashCode4 + (str5 == null ? 0 : str5.hashCode())) * 31;
        Txns txns = this.txns;
        int hashCode6 = (hashCode5 + (txns == null ? 0 : txns.hashCode())) * 31;
        Volume volume = this.volume;
        int hashCode7 = (hashCode6 + (volume == null ? 0 : volume.hashCode())) * 31;
        PriceChange priceChange = this.priceChange;
        int hashCode8 = (hashCode7 + (priceChange == null ? 0 : priceChange.hashCode())) * 31;
        Liquidity liquidity = this.liquidity;
        int hashCode9 = (hashCode8 + (liquidity == null ? 0 : liquidity.hashCode())) * 31;
        Double d = this.fdv;
        int hashCode10 = (hashCode9 + (d == null ? 0 : d.hashCode())) * 31;
        Integer num = this.version;
        return hashCode10 + (num != null ? num.hashCode() : 0);
    }

    public String toString() {
        return "CurrencyTokenInfo(tokenAddress=" + ((Object) this.tokenAddress) + ", pairAddress=" + ((Object) this.pairAddress) + ", changeId=" + ((Object) this.changeId) + ", dexId=" + this.dexId + ", url=" + this.url + ", baseToken=" + this.baseToken + ", quoteToken=" + this.quoteToken + ", priceNative=" + ((Object) this.priceNative) + ", priceUsd=" + ((Object) this.priceUsd) + ", txns=" + this.txns + ", volume=" + this.volume + ", priceChange=" + this.priceChange + ", liquidity=" + this.liquidity + ", fdv=" + this.fdv + ", version=" + this.version + ')';
    }
}
