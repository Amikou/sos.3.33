package net.safemoon.androidwallet.model.arguments;

import java.util.ArrayList;
import net.safemoon.androidwallet.common.TokenType;

/* compiled from: Arguments.kt */
/* loaded from: classes2.dex */
public final class TokenTypes extends ArrayList<TokenType> {
    @Override // java.util.ArrayList, java.util.AbstractCollection, java.util.Collection, java.util.List
    public final /* bridge */ boolean contains(Object obj) {
        if (obj instanceof TokenType) {
            return contains((TokenType) obj);
        }
        return false;
    }

    public /* bridge */ int getSize() {
        return super.size();
    }

    @Override // java.util.ArrayList, java.util.AbstractList, java.util.List
    public final /* bridge */ int indexOf(Object obj) {
        if (obj instanceof TokenType) {
            return indexOf((TokenType) obj);
        }
        return -1;
    }

    @Override // java.util.ArrayList, java.util.AbstractList, java.util.List
    public final /* bridge */ int lastIndexOf(Object obj) {
        if (obj instanceof TokenType) {
            return lastIndexOf((TokenType) obj);
        }
        return -1;
    }

    @Override // java.util.ArrayList, java.util.AbstractList, java.util.List
    public final /* bridge */ TokenType remove(int i) {
        return removeAt(i);
    }

    public /* bridge */ TokenType removeAt(int i) {
        return (TokenType) super.remove(i);
    }

    @Override // java.util.ArrayList, java.util.AbstractCollection, java.util.Collection, java.util.List
    public final /* bridge */ int size() {
        return getSize();
    }

    public /* bridge */ boolean contains(TokenType tokenType) {
        return super.contains((Object) tokenType);
    }

    public /* bridge */ int indexOf(TokenType tokenType) {
        return super.indexOf((Object) tokenType);
    }

    public /* bridge */ int lastIndexOf(TokenType tokenType) {
        return super.lastIndexOf((Object) tokenType);
    }

    @Override // java.util.ArrayList, java.util.AbstractCollection, java.util.Collection, java.util.List
    public final /* bridge */ boolean remove(Object obj) {
        if (obj instanceof TokenType) {
            return remove((TokenType) obj);
        }
        return false;
    }

    public /* bridge */ boolean remove(TokenType tokenType) {
        return super.remove((Object) tokenType);
    }
}
