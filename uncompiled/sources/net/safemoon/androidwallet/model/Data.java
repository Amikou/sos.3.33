package net.safemoon.androidwallet.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import java.io.Serializable;
import java.util.List;
import org.web3j.ens.contracts.generated.PublicResolver;

/* loaded from: classes2.dex */
public class Data implements Serializable {
    @SerializedName("BNB")
    @Expose
    private Coin bnb;
    @SerializedName("id")
    @Expose
    private Integer id;
    @SerializedName(PublicResolver.FUNC_NAME)
    @Expose
    private String name;
    @SerializedName("SAFEMOON")
    @Expose
    private Coin safemoon;
    @SerializedName("SFM")
    @Expose
    private Coin safemoonv2;
    @SerializedName("symbol")
    @Expose
    private String symbol;
    private final String SAFE_MOON_TOKEN = "SAFEMOON";
    private final String SAFE_MOON_V2_TOKEN = "SFM";
    private final String BNB_TOKEN = "BNB";
    @SerializedName("quotes")
    @Expose
    private List<HistoricalDatum> quotes = null;

    public Coin getBnb() {
        return this.bnb;
    }

    public Integer getId() {
        return this.id;
    }

    public String getName() {
        return this.name;
    }

    public List<HistoricalDatum> getQuotes() {
        return this.quotes;
    }

    public Coin getSafemoon() {
        return this.safemoon;
    }

    public Coin getSafemoonV2() {
        return this.safemoonv2;
    }

    public String getSymbol() {
        return this.symbol;
    }

    public void setBnb(Coin coin) {
        this.bnb = coin;
    }

    public void setId(Integer num) {
        this.id = num;
    }

    public void setName(String str) {
        this.name = str;
    }

    public void setQuotes(List<HistoricalDatum> list) {
        this.quotes = list;
    }

    public void setSafemoon(Coin coin) {
        this.safemoon = coin;
    }

    public void setSafemoonv2(Coin coin) {
        this.safemoonv2 = coin;
    }

    public void setSymbol(String str) {
        this.symbol = str;
    }
}
