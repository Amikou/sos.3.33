package net.safemoon.androidwallet.model.swap;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import java.io.Serializable;

/* compiled from: AllSwapTokens.kt */
/* loaded from: classes2.dex */
public final class AllSwapTokens implements Serializable {
    @SerializedName("data")
    @Expose
    private Data data;
    private final long serialVersionUID = -844986117407541445L;

    public final Data getData() {
        return this.data;
    }

    public final void setData(Data data) {
        this.data = data;
    }
}
