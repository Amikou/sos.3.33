package net.safemoon.androidwallet.model.swap;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import java.io.Serializable;
import java.util.List;

/* compiled from: Pairs.kt */
/* loaded from: classes2.dex */
public final class Pairs implements Serializable {
    public static final Companion Companion = new Companion(null);
    private static final long serialVersionUID = 8345936799993963050L;
    @SerializedName("data")
    @Expose
    private List<PairsData> data;

    /* compiled from: Pairs.kt */
    /* loaded from: classes2.dex */
    public static final class Companion {
        private Companion() {
        }

        public /* synthetic */ Companion(qi0 qi0Var) {
            this();
        }
    }

    public final List<PairsData> getData() {
        return this.data;
    }

    public final void setData(List<PairsData> list) {
        this.data = list;
    }
}
