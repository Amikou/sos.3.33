package net.safemoon.androidwallet.model.swap;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import java.io.Serializable;
import java.util.List;

/* compiled from: BaseTokens.kt */
/* loaded from: classes2.dex */
public final class BaseTokens implements Serializable {
    public static final Companion Companion = new Companion(null);
    private static final long serialVersionUID = 7478795497065709230L;
    @SerializedName("data")
    @Expose
    private List<Token> data = b20.g();

    /* compiled from: BaseTokens.kt */
    /* loaded from: classes2.dex */
    public static final class Companion {
        private Companion() {
        }

        public /* synthetic */ Companion(qi0 qi0Var) {
            this();
        }
    }

    public final List<Token> getData() {
        return this.data;
    }

    public final void setData(List<Token> list) {
        this.data = list;
    }
}
