package net.safemoon.androidwallet.model.swap;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import java.io.Serializable;
import java.util.List;
import org.web3j.ens.contracts.generated.PublicResolver;

/* compiled from: AllSwapTokens.kt */
/* loaded from: classes2.dex */
public final class Data implements Serializable {
    public static final Companion Companion = new Companion(null);
    private static final long serialVersionUID = 4935914903518560697L;
    @SerializedName("keywords")
    @Expose
    private List<String> keywords;
    @SerializedName("logoURI")
    @Expose
    private String logoURI;
    @SerializedName(PublicResolver.FUNC_NAME)
    @Expose
    private String name;
    @SerializedName("timestamp")
    @Expose
    private String timestamp;
    @SerializedName("tokens")
    @Expose
    private List<? extends Swap> tokens;
    @SerializedName("version")
    @Expose
    private Version version;

    /* compiled from: AllSwapTokens.kt */
    /* loaded from: classes2.dex */
    public static final class Companion {
        private Companion() {
        }

        public /* synthetic */ Companion(qi0 qi0Var) {
            this();
        }
    }

    public final List<String> getKeywords() {
        return this.keywords;
    }

    public final String getLogoURI() {
        return this.logoURI;
    }

    public final String getName() {
        return this.name;
    }

    public final String getTimestamp() {
        return this.timestamp;
    }

    public final List<Swap> getTokens() {
        return this.tokens;
    }

    public final Version getVersion() {
        return this.version;
    }

    public final void setKeywords(List<String> list) {
        this.keywords = list;
    }

    public final void setLogoURI(String str) {
        this.logoURI = str;
    }

    public final void setName(String str) {
        this.name = str;
    }

    public final void setTimestamp(String str) {
        this.timestamp = str;
    }

    public final void setTokens(List<? extends Swap> list) {
        this.tokens = list;
    }

    public final void setVersion(Version version) {
        this.version = version;
    }
}
