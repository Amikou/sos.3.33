package net.safemoon.androidwallet.model.swap;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import java.io.Serializable;

/* compiled from: AllSwapTokens.kt */
/* loaded from: classes2.dex */
public final class Version implements Serializable {
    public static final Companion Companion = new Companion(null);
    private static final long serialVersionUID = 7759565028657507222L;
    @SerializedName("major")
    @Expose
    private Integer major;
    @SerializedName("minor")
    @Expose
    private Integer minor;
    @SerializedName("patch")
    @Expose
    private Integer patch;

    /* compiled from: AllSwapTokens.kt */
    /* loaded from: classes2.dex */
    public static final class Companion {
        private Companion() {
        }

        public /* synthetic */ Companion(qi0 qi0Var) {
            this();
        }
    }

    public final Integer getMajor() {
        return this.major;
    }

    public final Integer getMinor() {
        return this.minor;
    }

    public final Integer getPatch() {
        return this.patch;
    }

    public final void setMajor(Integer num) {
        this.major = num;
    }

    public final void setMinor(Integer num) {
        this.minor = num;
    }

    public final void setPatch(Integer num) {
        this.patch = num;
    }
}
