package net.safemoon.androidwallet.model.swap;

import com.github.mikephil.charting.utils.Utils;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import java.io.Serializable;
import java.math.BigDecimal;
import java.util.List;
import java.util.Objects;
import net.safemoon.androidwallet.model.token.abstraction.IToken;
import org.web3j.abi.datatypes.Address;
import org.web3j.ens.contracts.generated.PublicResolver;

/* loaded from: classes2.dex */
public class Swap implements Serializable {
    private static final long serialVersionUID = 7954856143702396824L;
    @SerializedName(Address.TYPE_NAME)
    @Expose
    public String address;
    @SerializedName("chainId")
    @Expose
    public Integer chainId;
    @SerializedName("decimals")
    @Expose
    public Integer decimals;
    @SerializedName(PublicResolver.FUNC_NAME)
    @Expose
    public String name;
    @SerializedName("symbol")
    @Expose
    public String symbol;
    @SerializedName("logoURI")
    @Expose
    public String logoURI = "";
    @SerializedName("slug")
    @Expose
    public String cmcSlug = null;
    @SerializedName("coinMarketId")
    @Expose
    public String cmcId = null;
    @SerializedName("buySlippage")
    @Expose
    public Double buySlippage = null;
    @SerializedName("sellSlippage")
    @Expose
    public Double sellSlippage = null;
    @SerializedName("pairs")
    @Expose
    public List<PairsData> pairs = null;
    public Double nativeBalance = Double.valueOf((double) Utils.DOUBLE_EPSILON);
    public int imageResource = 0;
    public BigDecimal balance = BigDecimal.ZERO;

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj instanceof Swap) {
            Swap swap = (Swap) obj;
            return this.imageResource == swap.imageResource && Objects.equals(this.name, swap.name) && Objects.equals(this.symbol, swap.symbol) && Objects.equals(this.address, swap.address) && Objects.equals(this.chainId, swap.chainId) && Objects.equals(this.decimals, swap.decimals) && Objects.equals(this.logoURI, swap.logoURI) && Objects.equals(this.cmcSlug, swap.cmcSlug) && Objects.equals(this.cmcId, swap.cmcId) && Objects.equals(this.buySlippage, swap.buySlippage) && Objects.equals(this.sellSlippage, swap.sellSlippage) && Objects.equals(this.balance, swap.balance);
        }
        return false;
    }

    public int hashCode() {
        return Objects.hash(this.name, this.symbol, this.address, this.chainId, this.decimals, this.logoURI, this.nativeBalance);
    }

    public IToken toIToken() {
        return new IToken() { // from class: net.safemoon.androidwallet.model.swap.Swap.1
            @Override // net.safemoon.androidwallet.model.token.abstraction.IToken
            public boolean getAllowSwap() {
                return true;
            }

            @Override // net.safemoon.androidwallet.model.token.abstraction.IToken
            public int getChainId() {
                return Swap.this.chainId.intValue();
            }

            @Override // net.safemoon.androidwallet.model.token.abstraction.IToken
            public String getContractAddress() {
                return Swap.this.address;
            }

            @Override // net.safemoon.androidwallet.model.token.abstraction.IToken
            public int getDecimals() {
                return Swap.this.decimals.intValue();
            }

            @Override // net.safemoon.androidwallet.model.token.abstraction.IToken
            public String getIconResName() {
                return Swap.this.logoURI;
            }

            @Override // net.safemoon.androidwallet.model.token.abstraction.IToken
            public String getName() {
                return Swap.this.name;
            }

            @Override // net.safemoon.androidwallet.model.token.abstraction.IToken
            public double getNativeBalance() {
                return Utils.DOUBLE_EPSILON;
            }

            @Override // net.safemoon.androidwallet.model.token.abstraction.IToken
            public double getPercentChange1h() {
                return Utils.DOUBLE_EPSILON;
            }

            @Override // net.safemoon.androidwallet.model.token.abstraction.IToken
            public double getPriceInUsdt() {
                return Utils.DOUBLE_EPSILON;
            }

            @Override // net.safemoon.androidwallet.model.token.abstraction.IToken
            public String getSymbol() {
                return Swap.this.symbol;
            }

            @Override // net.safemoon.androidwallet.model.token.abstraction.IToken
            public String getSymbolWithType() {
                return "API_" + Swap.this.address;
            }
        };
    }
}
