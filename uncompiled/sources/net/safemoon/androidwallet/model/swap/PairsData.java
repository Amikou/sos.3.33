package net.safemoon.androidwallet.model.swap;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import java.io.Serializable;

/* compiled from: Pairs.kt */
/* loaded from: classes2.dex */
public final class PairsData implements Serializable {
    public static final Companion Companion = new Companion(null);
    private static final long serialVersionUID = -1588178562492023974L;
    @SerializedName("chainId")
    @Expose
    private Integer chainId;
    @SerializedName("pairAddress")
    @Expose
    private String pairAddress;
    @SerializedName("pairId")
    @Expose
    private String pairId;
    @SerializedName("reserve0")
    @Expose
    private String reserve0;
    @SerializedName("reserve1")
    @Expose
    private String reserve1;
    @SerializedName("token0")
    @Expose
    private Token token0;
    @SerializedName("token1")
    @Expose
    private Token token1;

    /* compiled from: Pairs.kt */
    /* loaded from: classes2.dex */
    public static final class Companion {
        private Companion() {
        }

        public /* synthetic */ Companion(qi0 qi0Var) {
            this();
        }
    }

    public final Integer getChainId() {
        return this.chainId;
    }

    public final String getPairAddress() {
        return this.pairAddress;
    }

    public final String getPairId() {
        return this.pairId;
    }

    public final String getReserve0() {
        return this.reserve0;
    }

    public final String getReserve1() {
        return this.reserve1;
    }

    public final Token getToken0() {
        return this.token0;
    }

    public final Token getToken1() {
        return this.token1;
    }

    public final void setChainId(Integer num) {
        this.chainId = num;
    }

    public final void setPairAddress(String str) {
        this.pairAddress = str;
    }

    public final void setPairId(String str) {
        this.pairId = str;
    }

    public final void setReserve0(String str) {
        this.reserve0 = str;
    }

    public final void setReserve1(String str) {
        this.reserve1 = str;
    }

    public final void setToken0(Token token) {
        this.token0 = token;
    }

    public final void setToken1(Token token) {
        this.token1 = token;
    }
}
