package net.safemoon.androidwallet.model.swap;

import com.google.gson.Gson;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import java.io.Serializable;
import org.web3j.abi.datatypes.Address;
import org.web3j.ens.contracts.generated.PublicResolver;

/* compiled from: Token.kt */
/* loaded from: classes2.dex */
public final class Token implements Serializable {
    public static final Companion Companion = new Companion(null);
    private static final long serialVersionUID = 4229498155653876591L;
    @SerializedName(alternate = {Address.TYPE_NAME}, value = "contractAddress")
    @Expose
    private String contractAddress;
    @SerializedName("decimals")
    @Expose
    private Integer decimals;
    @SerializedName(PublicResolver.FUNC_NAME)
    @Expose
    private String name;
    @SerializedName(alternate = {"token"}, value = "symbol")
    @Expose
    private String symbol;

    /* compiled from: Token.kt */
    /* loaded from: classes2.dex */
    public static final class Companion {
        private Companion() {
        }

        public /* synthetic */ Companion(qi0 qi0Var) {
            this();
        }
    }

    public final String getContractAddress() {
        return this.contractAddress;
    }

    public final Integer getDecimals() {
        return this.decimals;
    }

    public final String getName() {
        return this.name;
    }

    public final String getSymbol() {
        return this.symbol;
    }

    public final void setContractAddress(String str) {
        this.contractAddress = str;
    }

    public final void setDecimals(Integer num) {
        this.decimals = num;
    }

    public final void setName(String str) {
        this.name = str;
    }

    public final void setSymbol(String str) {
        this.symbol = str;
    }

    public String toString() {
        String json = new Gson().toJson(this);
        fs1.e(json, "Gson().toJson(this)");
        return json;
    }
}
