package net.safemoon.androidwallet.model;

import com.fasterxml.jackson.databind.deser.std.ThrowableDeserializer;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import java.io.Serializable;

/* loaded from: classes2.dex */
public class ReceiptStatus implements Serializable {
    private static final long serialVersionUID = 1449916715373268921L;
    @SerializedName(ThrowableDeserializer.PROP_NAME_MESSAGE)
    @Expose
    public String message;
    @SerializedName("result")
    @Expose
    public Result result;
    @SerializedName("status")
    @Expose
    public String status;

    /* loaded from: classes2.dex */
    public class Result implements Serializable {
        private static final long serialVersionUID = -5884007262121049816L;
        @SerializedName("status")
        @Expose
        public String status;

        public Result() {
        }
    }
}
