package net.safemoon.androidwallet.model.wyre;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/* compiled from: CheckoutPage.kt */
/* loaded from: classes2.dex */
public final class CheckoutPage {
    @SerializedName("reservation")
    @Expose
    private String reservation;
    @SerializedName("url")
    @Expose
    private String url;

    public final String getReservation() {
        return this.reservation;
    }

    public final String getUrl() {
        return this.url;
    }

    public final void setReservation(String str) {
        this.reservation = str;
    }

    public final void setUrl(String str) {
        this.url = str;
    }
}
