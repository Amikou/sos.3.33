package net.safemoon.androidwallet.model;

import com.google.gson.annotations.SerializedName;
import java.util.Arrays;

/* compiled from: Setting.kt */
/* loaded from: classes2.dex */
public final class FCMNotification {
    @SerializedName("currency")
    private String currency;
    @SerializedName("deviceToken")
    private String deviceToken;
    @SerializedName("isSendPushPriceAlert")
    private Boolean isSendPushPriceAlert;
    @SerializedName("lang")
    private String lang;
    @SerializedName("platForm")
    private String platForm;
    @SerializedName("walletAddresses")
    private String[] walletAddresses;

    public FCMNotification() {
        this(null, null, null, null, null, null, 63, null);
    }

    public FCMNotification(String str, String[] strArr, String str2, String str3, String str4, Boolean bool) {
        fs1.f(strArr, "walletAddresses");
        fs1.f(str2, "platForm");
        this.deviceToken = str;
        this.walletAddresses = strArr;
        this.platForm = str2;
        this.lang = str3;
        this.currency = str4;
        this.isSendPushPriceAlert = bool;
    }

    public static /* synthetic */ FCMNotification copy$default(FCMNotification fCMNotification, String str, String[] strArr, String str2, String str3, String str4, Boolean bool, int i, Object obj) {
        if ((i & 1) != 0) {
            str = fCMNotification.deviceToken;
        }
        if ((i & 2) != 0) {
            strArr = fCMNotification.walletAddresses;
        }
        String[] strArr2 = strArr;
        if ((i & 4) != 0) {
            str2 = fCMNotification.platForm;
        }
        String str5 = str2;
        if ((i & 8) != 0) {
            str3 = fCMNotification.lang;
        }
        String str6 = str3;
        if ((i & 16) != 0) {
            str4 = fCMNotification.currency;
        }
        String str7 = str4;
        if ((i & 32) != 0) {
            bool = fCMNotification.isSendPushPriceAlert;
        }
        return fCMNotification.copy(str, strArr2, str5, str6, str7, bool);
    }

    public final String component1() {
        return this.deviceToken;
    }

    public final String[] component2() {
        return this.walletAddresses;
    }

    public final String component3() {
        return this.platForm;
    }

    public final String component4() {
        return this.lang;
    }

    public final String component5() {
        return this.currency;
    }

    public final Boolean component6() {
        return this.isSendPushPriceAlert;
    }

    public final FCMNotification copy(String str, String[] strArr, String str2, String str3, String str4, Boolean bool) {
        fs1.f(strArr, "walletAddresses");
        fs1.f(str2, "platForm");
        return new FCMNotification(str, strArr, str2, str3, str4, bool);
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj instanceof FCMNotification) {
            FCMNotification fCMNotification = (FCMNotification) obj;
            return fs1.b(this.deviceToken, fCMNotification.deviceToken) && Arrays.equals(this.walletAddresses, fCMNotification.walletAddresses) && fs1.b(this.platForm, fCMNotification.platForm) && fs1.b(this.lang, fCMNotification.lang) && fs1.b(this.currency, fCMNotification.currency) && fs1.b(this.isSendPushPriceAlert, fCMNotification.isSendPushPriceAlert);
        }
        return false;
    }

    public final String getCurrency() {
        return this.currency;
    }

    public final String getDeviceToken() {
        return this.deviceToken;
    }

    public final String getLang() {
        return this.lang;
    }

    public final String getPlatForm() {
        return this.platForm;
    }

    public final String[] getWalletAddresses() {
        return this.walletAddresses;
    }

    public int hashCode() {
        String str = this.deviceToken;
        int hashCode = (((((str == null ? 0 : str.hashCode()) * 31) + Arrays.hashCode(this.walletAddresses)) * 31) + this.platForm.hashCode()) * 31;
        String str2 = this.lang;
        int hashCode2 = (hashCode + (str2 == null ? 0 : str2.hashCode())) * 31;
        String str3 = this.currency;
        int hashCode3 = (hashCode2 + (str3 == null ? 0 : str3.hashCode())) * 31;
        Boolean bool = this.isSendPushPriceAlert;
        return hashCode3 + (bool != null ? bool.hashCode() : 0);
    }

    public final Boolean isSendPushPriceAlert() {
        return this.isSendPushPriceAlert;
    }

    public final void setCurrency(String str) {
        this.currency = str;
    }

    public final void setDeviceToken(String str) {
        this.deviceToken = str;
    }

    public final void setLang(String str) {
        this.lang = str;
    }

    public final void setPlatForm(String str) {
        fs1.f(str, "<set-?>");
        this.platForm = str;
    }

    public final void setSendPushPriceAlert(Boolean bool) {
        this.isSendPushPriceAlert = bool;
    }

    public final void setWalletAddresses(String[] strArr) {
        fs1.f(strArr, "<set-?>");
        this.walletAddresses = strArr;
    }

    public String toString() {
        return "FCMNotification(deviceToken=" + ((Object) this.deviceToken) + ", walletAddresses=" + Arrays.toString(this.walletAddresses) + ", platForm=" + this.platForm + ", lang=" + ((Object) this.lang) + ", currency=" + ((Object) this.currency) + ", isSendPushPriceAlert=" + this.isSendPushPriceAlert + ')';
    }

    public /* synthetic */ FCMNotification(String str, String[] strArr, String str2, String str3, String str4, Boolean bool, int i, qi0 qi0Var) {
        this((i & 1) != 0 ? null : str, (i & 2) != 0 ? new String[0] : strArr, (i & 4) != 0 ? "ANDROID" : str2, (i & 8) != 0 ? null : str3, (i & 16) != 0 ? null : str4, (i & 32) != 0 ? null : bool);
    }
}
