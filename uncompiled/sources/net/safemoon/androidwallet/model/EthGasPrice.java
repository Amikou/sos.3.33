package net.safemoon.androidwallet.model;

import com.fasterxml.jackson.databind.deser.std.ThrowableDeserializer;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/* compiled from: EthGasPrice.kt */
/* loaded from: classes2.dex */
public final class EthGasPrice {
    @SerializedName(ThrowableDeserializer.PROP_NAME_MESSAGE)
    @Expose
    private final String message;
    @SerializedName("result")
    @Expose
    private final Result result;
    @SerializedName("status")
    @Expose
    private final String status;

    public EthGasPrice(String str, String str2, Result result) {
        fs1.f(str, "status");
        fs1.f(str2, ThrowableDeserializer.PROP_NAME_MESSAGE);
        fs1.f(result, "result");
        this.status = str;
        this.message = str2;
        this.result = result;
    }

    public static /* synthetic */ EthGasPrice copy$default(EthGasPrice ethGasPrice, String str, String str2, Result result, int i, Object obj) {
        if ((i & 1) != 0) {
            str = ethGasPrice.status;
        }
        if ((i & 2) != 0) {
            str2 = ethGasPrice.message;
        }
        if ((i & 4) != 0) {
            result = ethGasPrice.result;
        }
        return ethGasPrice.copy(str, str2, result);
    }

    public final String component1() {
        return this.status;
    }

    public final String component2() {
        return this.message;
    }

    public final Result component3() {
        return this.result;
    }

    public final EthGasPrice copy(String str, String str2, Result result) {
        fs1.f(str, "status");
        fs1.f(str2, ThrowableDeserializer.PROP_NAME_MESSAGE);
        fs1.f(result, "result");
        return new EthGasPrice(str, str2, result);
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj instanceof EthGasPrice) {
            EthGasPrice ethGasPrice = (EthGasPrice) obj;
            return fs1.b(this.status, ethGasPrice.status) && fs1.b(this.message, ethGasPrice.message) && fs1.b(this.result, ethGasPrice.result);
        }
        return false;
    }

    public final String getMessage() {
        return this.message;
    }

    public final Result getResult() {
        return this.result;
    }

    public final String getStatus() {
        return this.status;
    }

    public int hashCode() {
        return (((this.status.hashCode() * 31) + this.message.hashCode()) * 31) + this.result.hashCode();
    }

    public String toString() {
        return "EthGasPrice(status=" + this.status + ", message=" + this.message + ", result=" + this.result + ')';
    }
}
