package net.safemoon.androidwallet.model.transaction.history;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import java.io.Serializable;

/* loaded from: classes2.dex */
public class Result implements Serializable {
    @SerializedName("blockHash")
    @Expose
    public String blockHash;
    @SerializedName("blockNumber")
    @Expose
    public String blockNumber;
    @SerializedName("confirmations")
    @Expose
    public String confirmations;
    @SerializedName("contractAddress")
    @Expose
    public String contractAddress;
    @SerializedName("cumulativeGasUsed")
    @Expose
    public String cumulativeGasUsed;
    @SerializedName("from")
    @Expose
    public String from;
    @SerializedName("gas")
    @Expose
    public String gas;
    @SerializedName("gasPrice")
    @Expose
    public String gasPrice;
    @SerializedName("gasUsed")
    @Expose
    public String gasUsed;
    @SerializedName("hash")
    @Expose
    public String hash;
    @SerializedName("input")
    @Expose
    public String input;
    @SerializedName("nonce")
    @Expose
    public String nonce;
    @SerializedName("timeStamp")
    @Expose
    public String timeStamp;
    @SerializedName("to")
    @Expose
    public String to;
    @SerializedName("tokenDecimal")
    @Expose
    public Integer tokenDecimal;
    @SerializedName("tokenName")
    @Expose
    public String tokenName;
    @SerializedName("tokenSymbol")
    @Expose
    public String tokenSymbol;
    @SerializedName("transactionIndex")
    @Expose
    public String transactionIndex;
    @SerializedName("value")
    @Expose
    public String value;
    @SerializedName("txreceipt_status")
    @Expose
    public String txreceiptStatus = "1";
    public boolean offlinePending = false;
}
