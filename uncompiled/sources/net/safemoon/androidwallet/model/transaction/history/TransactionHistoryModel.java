package net.safemoon.androidwallet.model.transaction.history;

import com.fasterxml.jackson.databind.deser.std.ThrowableDeserializer;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import java.util.ArrayList;

/* loaded from: classes2.dex */
public class TransactionHistoryModel {
    @SerializedName(ThrowableDeserializer.PROP_NAME_MESSAGE)
    @Expose
    public String message;
    @SerializedName("result")
    @Expose
    public ArrayList<Result> result = new ArrayList<>();
    @SerializedName("status")
    @Expose
    public String status;
}
