package net.safemoon.androidwallet.model.transaction.details;

import com.google.gson.annotations.SerializedName;

/* compiled from: TransactionDetails.kt */
/* loaded from: classes2.dex */
public final class TransactionDetails {
    @SerializedName("data")
    private final TransactionDetailsData data;

    public TransactionDetails(TransactionDetailsData transactionDetailsData) {
        fs1.f(transactionDetailsData, "data");
        this.data = transactionDetailsData;
    }

    public static /* synthetic */ TransactionDetails copy$default(TransactionDetails transactionDetails, TransactionDetailsData transactionDetailsData, int i, Object obj) {
        if ((i & 1) != 0) {
            transactionDetailsData = transactionDetails.data;
        }
        return transactionDetails.copy(transactionDetailsData);
    }

    public final TransactionDetailsData component1() {
        return this.data;
    }

    public final TransactionDetails copy(TransactionDetailsData transactionDetailsData) {
        fs1.f(transactionDetailsData, "data");
        return new TransactionDetails(transactionDetailsData);
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        return (obj instanceof TransactionDetails) && fs1.b(this.data, ((TransactionDetails) obj).data);
    }

    public final TransactionDetailsData getData() {
        return this.data;
    }

    public int hashCode() {
        return this.data.hashCode();
    }

    public String toString() {
        return "TransactionDetails(data=" + this.data + ')';
    }
}
