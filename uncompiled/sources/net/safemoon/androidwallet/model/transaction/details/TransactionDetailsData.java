package net.safemoon.androidwallet.model.transaction.details;

import com.google.gson.annotations.SerializedName;

/* compiled from: TransactionDetailsData.kt */
/* loaded from: classes2.dex */
public final class TransactionDetailsData {
    @SerializedName("amount")
    private final double amount;
    @SerializedName("chain")
    private final String chain;
    @SerializedName("createdAt")
    private final String createdAt;
    @SerializedName("from")
    private final String from;
    @SerializedName("gasUsed")
    private final String gasUsed;
    @SerializedName("transactionHash")
    private final String hash;
    @SerializedName("status")
    private final boolean status;
    @SerializedName("to")
    private final String to;
    @SerializedName("tokenSymbol")
    private final String tokenSymbol;
    @SerializedName("transactionIndex")
    private final String transactionIndex;
    @SerializedName("type")
    private final String type;

    public TransactionDetailsData(String str, boolean z, String str2, String str3, String str4, double d, String str5, String str6, String str7, String str8, String str9) {
        fs1.f(str, "hash");
        fs1.f(str2, "type");
        fs1.f(str3, "from");
        fs1.f(str4, "to");
        fs1.f(str5, "tokenSymbol");
        fs1.f(str6, "createdAt");
        fs1.f(str7, "gasUsed");
        fs1.f(str8, "transactionIndex");
        fs1.f(str9, "chain");
        this.hash = str;
        this.status = z;
        this.type = str2;
        this.from = str3;
        this.to = str4;
        this.amount = d;
        this.tokenSymbol = str5;
        this.createdAt = str6;
        this.gasUsed = str7;
        this.transactionIndex = str8;
        this.chain = str9;
    }

    public final String component1() {
        return this.hash;
    }

    public final String component10() {
        return this.transactionIndex;
    }

    public final String component11() {
        return this.chain;
    }

    public final boolean component2() {
        return this.status;
    }

    public final String component3() {
        return this.type;
    }

    public final String component4() {
        return this.from;
    }

    public final String component5() {
        return this.to;
    }

    public final double component6() {
        return this.amount;
    }

    public final String component7() {
        return this.tokenSymbol;
    }

    public final String component8() {
        return this.createdAt;
    }

    public final String component9() {
        return this.gasUsed;
    }

    public final TransactionDetailsData copy(String str, boolean z, String str2, String str3, String str4, double d, String str5, String str6, String str7, String str8, String str9) {
        fs1.f(str, "hash");
        fs1.f(str2, "type");
        fs1.f(str3, "from");
        fs1.f(str4, "to");
        fs1.f(str5, "tokenSymbol");
        fs1.f(str6, "createdAt");
        fs1.f(str7, "gasUsed");
        fs1.f(str8, "transactionIndex");
        fs1.f(str9, "chain");
        return new TransactionDetailsData(str, z, str2, str3, str4, d, str5, str6, str7, str8, str9);
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj instanceof TransactionDetailsData) {
            TransactionDetailsData transactionDetailsData = (TransactionDetailsData) obj;
            return fs1.b(this.hash, transactionDetailsData.hash) && this.status == transactionDetailsData.status && fs1.b(this.type, transactionDetailsData.type) && fs1.b(this.from, transactionDetailsData.from) && fs1.b(this.to, transactionDetailsData.to) && fs1.b(Double.valueOf(this.amount), Double.valueOf(transactionDetailsData.amount)) && fs1.b(this.tokenSymbol, transactionDetailsData.tokenSymbol) && fs1.b(this.createdAt, transactionDetailsData.createdAt) && fs1.b(this.gasUsed, transactionDetailsData.gasUsed) && fs1.b(this.transactionIndex, transactionDetailsData.transactionIndex) && fs1.b(this.chain, transactionDetailsData.chain);
        }
        return false;
    }

    public final double getAmount() {
        return this.amount;
    }

    public final String getChain() {
        return this.chain;
    }

    public final String getCreatedAt() {
        return this.createdAt;
    }

    public final String getFrom() {
        return this.from;
    }

    public final String getGasUsed() {
        return this.gasUsed;
    }

    public final String getHash() {
        return this.hash;
    }

    public final boolean getStatus() {
        return this.status;
    }

    public final String getTo() {
        return this.to;
    }

    public final String getTokenSymbol() {
        return this.tokenSymbol;
    }

    public final String getTransactionIndex() {
        return this.transactionIndex;
    }

    public final String getType() {
        return this.type;
    }

    /* JADX WARN: Multi-variable type inference failed */
    public int hashCode() {
        int hashCode = this.hash.hashCode() * 31;
        boolean z = this.status;
        int i = z;
        if (z != 0) {
            i = 1;
        }
        return ((((((((((((((((((hashCode + i) * 31) + this.type.hashCode()) * 31) + this.from.hashCode()) * 31) + this.to.hashCode()) * 31) + Double.doubleToLongBits(this.amount)) * 31) + this.tokenSymbol.hashCode()) * 31) + this.createdAt.hashCode()) * 31) + this.gasUsed.hashCode()) * 31) + this.transactionIndex.hashCode()) * 31) + this.chain.hashCode();
    }

    public String toString() {
        return "TransactionDetailsData(hash=" + this.hash + ", status=" + this.status + ", type=" + this.type + ", from=" + this.from + ", to=" + this.to + ", amount=" + this.amount + ", tokenSymbol=" + this.tokenSymbol + ", createdAt=" + this.createdAt + ", gasUsed=" + this.gasUsed + ", transactionIndex=" + this.transactionIndex + ", chain=" + this.chain + ')';
    }
}
