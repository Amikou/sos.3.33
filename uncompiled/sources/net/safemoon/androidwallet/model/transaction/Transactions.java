package net.safemoon.androidwallet.model.transaction;

import com.fasterxml.jackson.databind.deser.std.ThrowableDeserializer;
import com.google.gson.annotations.SerializedName;
import net.safemoon.androidwallet.model.transaction.history.TransactionHistoryModel;

/* compiled from: transactions.kt */
/* loaded from: classes2.dex */
public final class Transactions {
    @SerializedName("code")
    private final String code;
    @SerializedName("data")
    private final TransactionHistoryModel data;
    @SerializedName(ThrowableDeserializer.PROP_NAME_MESSAGE)
    private final String message;

    public Transactions(String str, String str2, TransactionHistoryModel transactionHistoryModel) {
        fs1.f(str, "code");
        fs1.f(str2, ThrowableDeserializer.PROP_NAME_MESSAGE);
        fs1.f(transactionHistoryModel, "data");
        this.code = str;
        this.message = str2;
        this.data = transactionHistoryModel;
    }

    public static /* synthetic */ Transactions copy$default(Transactions transactions, String str, String str2, TransactionHistoryModel transactionHistoryModel, int i, Object obj) {
        if ((i & 1) != 0) {
            str = transactions.code;
        }
        if ((i & 2) != 0) {
            str2 = transactions.message;
        }
        if ((i & 4) != 0) {
            transactionHistoryModel = transactions.data;
        }
        return transactions.copy(str, str2, transactionHistoryModel);
    }

    public final String component1() {
        return this.code;
    }

    public final String component2() {
        return this.message;
    }

    public final TransactionHistoryModel component3() {
        return this.data;
    }

    public final Transactions copy(String str, String str2, TransactionHistoryModel transactionHistoryModel) {
        fs1.f(str, "code");
        fs1.f(str2, ThrowableDeserializer.PROP_NAME_MESSAGE);
        fs1.f(transactionHistoryModel, "data");
        return new Transactions(str, str2, transactionHistoryModel);
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj instanceof Transactions) {
            Transactions transactions = (Transactions) obj;
            return fs1.b(this.code, transactions.code) && fs1.b(this.message, transactions.message) && fs1.b(this.data, transactions.data);
        }
        return false;
    }

    public final String getCode() {
        return this.code;
    }

    public final TransactionHistoryModel getData() {
        return this.data;
    }

    public final String getMessage() {
        return this.message;
    }

    public int hashCode() {
        return (((this.code.hashCode() * 31) + this.message.hashCode()) * 31) + this.data.hashCode();
    }

    public String toString() {
        return "Transactions(code=" + this.code + ", message=" + this.message + ", data=" + this.data + ')';
    }
}
