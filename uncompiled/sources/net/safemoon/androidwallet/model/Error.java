package net.safemoon.androidwallet.model;

import com.fasterxml.jackson.databind.deser.std.ThrowableDeserializer;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/* loaded from: classes2.dex */
public class Error {
    @SerializedName("code")
    @Expose
    public String code;
    @SerializedName(ThrowableDeserializer.PROP_NAME_MESSAGE)
    @Expose
    public String message;
}
