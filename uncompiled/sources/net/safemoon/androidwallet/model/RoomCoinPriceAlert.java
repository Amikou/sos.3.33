package net.safemoon.androidwallet.model;

import com.google.gson.Gson;
import org.web3j.ens.contracts.generated.PublicResolver;

/* compiled from: RoomCoinPriceAlert.kt */
/* loaded from: classes2.dex */
public final class RoomCoinPriceAlert extends RoomCoin {
    private final String cmcData;
    private final int cmcId;
    private final String name;
    private final String slug;
    private final String symbol;

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public RoomCoinPriceAlert(int i, String str, String str2, String str3, String str4) {
        super(i, str, str2, str3, str4);
        fs1.f(str, "symbol");
        fs1.f(str2, PublicResolver.FUNC_NAME);
        fs1.f(str3, "slug");
        fs1.f(str4, "cmcData");
        this.cmcId = i;
        this.symbol = str;
        this.name = str2;
        this.slug = str3;
        this.cmcData = str4;
    }

    @Override // net.safemoon.androidwallet.model.RoomCoin
    public String getCmcData() {
        return this.cmcData;
    }

    @Override // net.safemoon.androidwallet.model.RoomCoin
    public int getCmcId() {
        return this.cmcId;
    }

    public final Coin getCoinData() {
        try {
            return (Coin) new Gson().fromJson(getCmcData(), (Class<Object>) Coin.class);
        } catch (Exception unused) {
            return null;
        }
    }

    @Override // net.safemoon.androidwallet.model.RoomCoin
    public String getName() {
        return this.name;
    }

    @Override // net.safemoon.androidwallet.model.RoomCoin
    public String getSlug() {
        return this.slug;
    }

    @Override // net.safemoon.androidwallet.model.RoomCoin
    public String getSymbol() {
        return this.symbol;
    }
}
