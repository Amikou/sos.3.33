package net.safemoon.androidwallet.domain.useCase.wipe;

import kotlin.coroutines.jvm.internal.SuspendLambda;
import kotlin.coroutines.jvm.internal.a;
import kotlinx.coroutines.CoroutineDispatcher;

/* compiled from: AsyncWipeDataUseCase.kt */
@a(c = "net.safemoon.androidwallet.domain.useCase.wipe.AsyncWipeDataUseCase$execute$1", f = "AsyncWipeDataUseCase.kt", l = {14}, m = "invokeSuspend")
/* loaded from: classes2.dex */
public final class AsyncWipeDataUseCase$execute$1 extends SuspendLambda implements hd1<c90, q70<? super te4>, Object> {
    public int label;
    public final /* synthetic */ AsyncWipeDataUseCase this$0;

    /* compiled from: AsyncWipeDataUseCase.kt */
    @a(c = "net.safemoon.androidwallet.domain.useCase.wipe.AsyncWipeDataUseCase$execute$1$1", f = "AsyncWipeDataUseCase.kt", l = {}, m = "invokeSuspend")
    /* renamed from: net.safemoon.androidwallet.domain.useCase.wipe.AsyncWipeDataUseCase$execute$1$1  reason: invalid class name */
    /* loaded from: classes2.dex */
    public static final class AnonymousClass1 extends SuspendLambda implements hd1<c90, q70<? super te4>, Object> {
        public int label;
        public final /* synthetic */ AsyncWipeDataUseCase this$0;

        /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
        public AnonymousClass1(AsyncWipeDataUseCase asyncWipeDataUseCase, q70<? super AnonymousClass1> q70Var) {
            super(2, q70Var);
            this.this$0 = asyncWipeDataUseCase;
        }

        @Override // kotlin.coroutines.jvm.internal.BaseContinuationImpl
        public final q70<te4> create(Object obj, q70<?> q70Var) {
            return new AnonymousClass1(this.this$0, q70Var);
        }

        @Override // defpackage.hd1
        public final Object invoke(c90 c90Var, q70<? super te4> q70Var) {
            return ((AnonymousClass1) create(c90Var, q70Var)).invokeSuspend(te4.a);
        }

        @Override // kotlin.coroutines.jvm.internal.BaseContinuationImpl
        public final Object invokeSuspend(Object obj) {
            en1 en1Var;
            gs1.d();
            if (this.label == 0) {
                o83.b(obj);
                en1Var = this.this$0.a;
                en1Var.execute();
                return te4.a;
            }
            throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        }
    }

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public AsyncWipeDataUseCase$execute$1(AsyncWipeDataUseCase asyncWipeDataUseCase, q70<? super AsyncWipeDataUseCase$execute$1> q70Var) {
        super(2, q70Var);
        this.this$0 = asyncWipeDataUseCase;
    }

    @Override // kotlin.coroutines.jvm.internal.BaseContinuationImpl
    public final q70<te4> create(Object obj, q70<?> q70Var) {
        return new AsyncWipeDataUseCase$execute$1(this.this$0, q70Var);
    }

    @Override // defpackage.hd1
    public final Object invoke(c90 c90Var, q70<? super te4> q70Var) {
        return ((AsyncWipeDataUseCase$execute$1) create(c90Var, q70Var)).invokeSuspend(te4.a);
    }

    @Override // kotlin.coroutines.jvm.internal.BaseContinuationImpl
    public final Object invokeSuspend(Object obj) {
        Object d = gs1.d();
        int i = this.label;
        if (i == 0) {
            o83.b(obj);
            CoroutineDispatcher b = tp0.b();
            AnonymousClass1 anonymousClass1 = new AnonymousClass1(this.this$0, null);
            this.label = 1;
            if (kotlinx.coroutines.a.e(b, anonymousClass1, this) == d) {
                return d;
            }
        } else if (i != 1) {
            throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        } else {
            o83.b(obj);
        }
        return te4.a;
    }
}
