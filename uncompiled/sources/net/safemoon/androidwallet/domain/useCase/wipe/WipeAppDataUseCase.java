package net.safemoon.androidwallet.domain.useCase.wipe;

import android.content.Context;
import com.google.android.gms.tasks.c;
import com.google.firebase.messaging.FirebaseMessaging;
import java.util.List;
import net.safemoon.androidwallet.database.mainRoom.MainRoomDatabase;
import net.safemoon.androidwallet.database.room.ApplicationRoomDatabase;

/* compiled from: WipeAppDataUseCase.kt */
/* loaded from: classes2.dex */
public final class WipeAppDataUseCase implements en1 {
    public final Context a;
    public final List<rm1> b;
    public final sy1 c;

    /* JADX WARN: Multi-variable type inference failed */
    public WipeAppDataUseCase(Context context, List<? extends rm1> list) {
        fs1.f(context, "context");
        fs1.f(list, "providerList");
        this.a = context;
        this.b = list;
        this.c = zy1.a(WipeAppDataUseCase$safemoonAPIInterface$2.INSTANCE);
    }

    public static final void c(c cVar) {
        fs1.f(cVar, "it");
    }

    public final void b() {
        MainRoomDatabase.n.a(this.a);
        ApplicationRoomDatabase.n.a(this.a);
        bo3.a(this.a);
        FirebaseMessaging.g().d().b(kp4.a);
    }

    public final void d() {
        for (rm1 rm1Var : this.b) {
            rm1Var.a();
        }
    }

    @Override // defpackage.en1
    public void execute() {
        b();
        d();
    }
}
