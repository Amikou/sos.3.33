package net.safemoon.androidwallet.domain.useCase.startup.forceIcon;

import kotlin.coroutines.jvm.internal.ContinuationImpl;
import kotlin.coroutines.jvm.internal.a;

/* compiled from: ForceTokenLogoUpdateUseCase.kt */
@a(c = "net.safemoon.androidwallet.domain.useCase.startup.forceIcon.ForceTokenLogoUpdateUseCase", f = "ForceTokenLogoUpdateUseCase.kt", l = {27}, m = "updateInTokenRepo")
/* loaded from: classes2.dex */
public final class ForceTokenLogoUpdateUseCase$updateInTokenRepo$1 extends ContinuationImpl {
    public Object L$0;
    public int label;
    public /* synthetic */ Object result;
    public final /* synthetic */ ForceTokenLogoUpdateUseCase this$0;

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public ForceTokenLogoUpdateUseCase$updateInTokenRepo$1(ForceTokenLogoUpdateUseCase forceTokenLogoUpdateUseCase, q70<? super ForceTokenLogoUpdateUseCase$updateInTokenRepo$1> q70Var) {
        super(q70Var);
        this.this$0 = forceTokenLogoUpdateUseCase;
    }

    @Override // kotlin.coroutines.jvm.internal.BaseContinuationImpl
    public final Object invokeSuspend(Object obj) {
        Object e;
        this.result = obj;
        this.label |= Integer.MIN_VALUE;
        e = this.this$0.e(this);
        return e;
    }
}
