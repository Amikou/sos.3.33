package net.safemoon.androidwallet.domain.useCase.startup.forceIcon;

import net.safemoon.androidwallet.extension.ThreadExtension;
import net.safemoon.androidwallet.repository.ReflectionDataSource;

/* compiled from: ForceTokenLogoUpdateUseCase.kt */
/* loaded from: classes2.dex */
public final class ForceTokenLogoUpdateUseCase implements am1 {
    public final bn1 a;
    public final ReflectionDataSource b;
    public final c90 c;
    public final String d;
    public final String e;

    public ForceTokenLogoUpdateUseCase(bn1 bn1Var, ReflectionDataSource reflectionDataSource, c90 c90Var, String str, String str2) {
        fs1.f(bn1Var, "userTokenListRepository");
        fs1.f(reflectionDataSource, "reflectionDataSource");
        fs1.f(c90Var, "coroutineScope");
        fs1.f(str, "tokenSymbol");
        fs1.f(str2, "newIconResName");
        this.a = bn1Var;
        this.b = reflectionDataSource;
        this.c = c90Var;
        this.d = str;
        this.e = str2;
    }

    @Override // defpackage.am1
    public void a() {
        ThreadExtension.a.a(this.c, new ForceTokenLogoUpdateUseCase$update$1(this, null));
    }

    /* JADX WARN: Removed duplicated region for block: B:10:0x0028  */
    /* JADX WARN: Removed duplicated region for block: B:24:0x004c  */
    /* JADX WARN: Removed duplicated region for block: B:35:0x00af  */
    /* JADX WARN: Removed duplicated region for block: B:36:0x00b0  */
    /* JADX WARN: Removed duplicated region for block: B:39:0x00c1 A[RETURN] */
    /* JADX WARN: Removed duplicated region for block: B:44:0x00ca  */
    /*
        Code decompiled incorrectly, please refer to instructions dump.
        To view partially-correct code enable 'Show inconsistent code' option in preferences
    */
    public final java.lang.Object d(defpackage.q70<? super defpackage.te4> r25) {
        /*
            r24 = this;
            r1 = r24
            r0 = r25
            boolean r2 = r0 instanceof net.safemoon.androidwallet.domain.useCase.startup.forceIcon.ForceTokenLogoUpdateUseCase$updateInReflectionRepo$1
            if (r2 == 0) goto L17
            r2 = r0
            net.safemoon.androidwallet.domain.useCase.startup.forceIcon.ForceTokenLogoUpdateUseCase$updateInReflectionRepo$1 r2 = (net.safemoon.androidwallet.domain.useCase.startup.forceIcon.ForceTokenLogoUpdateUseCase$updateInReflectionRepo$1) r2
            int r3 = r2.label
            r4 = -2147483648(0xffffffff80000000, float:-0.0)
            r5 = r3 & r4
            if (r5 == 0) goto L17
            int r3 = r3 - r4
            r2.label = r3
            goto L1c
        L17:
            net.safemoon.androidwallet.domain.useCase.startup.forceIcon.ForceTokenLogoUpdateUseCase$updateInReflectionRepo$1 r2 = new net.safemoon.androidwallet.domain.useCase.startup.forceIcon.ForceTokenLogoUpdateUseCase$updateInReflectionRepo$1
            r2.<init>(r1, r0)
        L1c:
            java.lang.Object r0 = r2.result
            java.lang.Object r3 = defpackage.gs1.d()
            int r4 = r2.label
            r5 = 2
            r6 = 1
            if (r4 == 0) goto L4c
            if (r4 == r6) goto L40
            if (r4 != r5) goto L38
            java.lang.Object r2 = r2.L$0
            net.safemoon.androidwallet.domain.useCase.startup.forceIcon.ForceTokenLogoUpdateUseCase r2 = (net.safemoon.androidwallet.domain.useCase.startup.forceIcon.ForceTokenLogoUpdateUseCase) r2
            defpackage.o83.b(r0)     // Catch: java.lang.RuntimeException -> L35
            goto Ldc
        L35:
            r0 = move-exception
            goto Lc4
        L38:
            java.lang.IllegalStateException r0 = new java.lang.IllegalStateException
            java.lang.String r2 = "call to 'resume' before 'invoke' with coroutine"
            r0.<init>(r2)
            throw r0
        L40:
            java.lang.Object r4 = r2.L$0
            net.safemoon.androidwallet.domain.useCase.startup.forceIcon.ForceTokenLogoUpdateUseCase r4 = (net.safemoon.androidwallet.domain.useCase.startup.forceIcon.ForceTokenLogoUpdateUseCase) r4
            defpackage.o83.b(r0)     // Catch: java.lang.RuntimeException -> L48
            goto L5f
        L48:
            r0 = move-exception
            r2 = r4
            goto Lc4
        L4c:
            defpackage.o83.b(r0)
            net.safemoon.androidwallet.repository.ReflectionDataSource r0 = r1.b     // Catch: java.lang.RuntimeException -> Lc2
            java.lang.String r4 = r1.d     // Catch: java.lang.RuntimeException -> Lc2
            r2.L$0 = r1     // Catch: java.lang.RuntimeException -> Lc2
            r2.label = r6     // Catch: java.lang.RuntimeException -> Lc2
            java.lang.Object r0 = r0.c(r4, r2)     // Catch: java.lang.RuntimeException -> Lc2
            if (r0 != r3) goto L5e
            return r3
        L5e:
            r4 = r1
        L5f:
            net.safemoon.androidwallet.model.reflections.RoomReflectionsToken r0 = (net.safemoon.androidwallet.model.reflections.RoomReflectionsToken) r0     // Catch: java.lang.RuntimeException -> L48
            if (r0 == 0) goto Ldc
            java.lang.String r7 = r0.getIconResName()     // Catch: java.lang.RuntimeException -> L48
            java.lang.String r8 = r4.e     // Catch: java.lang.RuntimeException -> L48
            boolean r7 = defpackage.fs1.b(r7, r8)     // Catch: java.lang.RuntimeException -> L48
            if (r7 != 0) goto Ldc
            net.safemoon.androidwallet.repository.ReflectionDataSource r7 = r4.b     // Catch: java.lang.RuntimeException -> L48
            java.lang.String r13 = r4.e     // Catch: java.lang.RuntimeException -> L48
            java.lang.Long r9 = r0.getId()     // Catch: java.lang.RuntimeException -> L48
            java.lang.String r10 = r0.getSymbolWithType()     // Catch: java.lang.RuntimeException -> L48
            java.lang.String r11 = r0.getSymbol()     // Catch: java.lang.RuntimeException -> L48
            java.lang.String r12 = r0.getName()     // Catch: java.lang.RuntimeException -> L48
            java.lang.String r14 = r0.getContractAddress()     // Catch: java.lang.RuntimeException -> L48
            int r15 = r0.getChainId()     // Catch: java.lang.RuntimeException -> L48
            int r16 = r0.getDecimals()     // Catch: java.lang.RuntimeException -> L48
            java.lang.String r17 = r0.getNativeBalance()     // Catch: java.lang.RuntimeException -> L48
            java.lang.Long r18 = r0.getFirstTimeStamp()     // Catch: java.lang.RuntimeException -> L48
            boolean r8 = r0.getEnableAdvanceMode()     // Catch: java.lang.RuntimeException -> L48
            java.lang.Long r20 = r0.getLatestBalance()     // Catch: java.lang.RuntimeException -> L48
            java.lang.Long r21 = r0.getLatestTimeStamp()     // Catch: java.lang.RuntimeException -> L48
            java.lang.Long r22 = r0.getCmcId()     // Catch: java.lang.RuntimeException -> L48
            java.lang.Double r23 = r0.getPriceUsd()     // Catch: java.lang.RuntimeException -> L48
            net.safemoon.androidwallet.model.reflections.RoomReflectionsToken r0 = new net.safemoon.androidwallet.model.reflections.RoomReflectionsToken     // Catch: java.lang.RuntimeException -> L48
            if (r8 == 0) goto Lb0
            goto Lb1
        Lb0:
            r6 = 0
        Lb1:
            r19 = r6
            r8 = r0
            r8.<init>(r9, r10, r11, r12, r13, r14, r15, r16, r17, r18, r19, r20, r21, r22, r23)     // Catch: java.lang.RuntimeException -> L48
            r2.L$0 = r4     // Catch: java.lang.RuntimeException -> L48
            r2.label = r5     // Catch: java.lang.RuntimeException -> L48
            java.lang.Object r0 = r7.m(r0, r2)     // Catch: java.lang.RuntimeException -> L48
            if (r0 != r3) goto Ldc
            return r3
        Lc2:
            r0 = move-exception
            r2 = r1
        Lc4:
            java.lang.String r0 = r0.getLocalizedMessage()
            if (r0 != 0) goto Ldc
            java.lang.Class r0 = r2.getClass()
            java.lang.String r0 = r0.getSimpleName()
            java.lang.String r2 = "this::class.java.simpleName"
            defpackage.fs1.e(r0, r2)
            java.lang.String r2 = ""
            defpackage.e30.c0(r2, r0)
        Ldc:
            te4 r0 = defpackage.te4.a
            return r0
        */
        throw new UnsupportedOperationException("Method not decompiled: net.safemoon.androidwallet.domain.useCase.startup.forceIcon.ForceTokenLogoUpdateUseCase.d(q70):java.lang.Object");
    }

    /* JADX WARN: Removed duplicated region for block: B:10:0x0023  */
    /* JADX WARN: Removed duplicated region for block: B:18:0x0037  */
    /* JADX WARN: Removed duplicated region for block: B:26:0x0053  */
    /*
        Code decompiled incorrectly, please refer to instructions dump.
        To view partially-correct code enable 'Show inconsistent code' option in preferences
    */
    public final java.lang.Object e(defpackage.q70<? super defpackage.te4> r6) {
        /*
            r5 = this;
            boolean r0 = r6 instanceof net.safemoon.androidwallet.domain.useCase.startup.forceIcon.ForceTokenLogoUpdateUseCase$updateInTokenRepo$1
            if (r0 == 0) goto L13
            r0 = r6
            net.safemoon.androidwallet.domain.useCase.startup.forceIcon.ForceTokenLogoUpdateUseCase$updateInTokenRepo$1 r0 = (net.safemoon.androidwallet.domain.useCase.startup.forceIcon.ForceTokenLogoUpdateUseCase$updateInTokenRepo$1) r0
            int r1 = r0.label
            r2 = -2147483648(0xffffffff80000000, float:-0.0)
            r3 = r1 & r2
            if (r3 == 0) goto L13
            int r1 = r1 - r2
            r0.label = r1
            goto L18
        L13:
            net.safemoon.androidwallet.domain.useCase.startup.forceIcon.ForceTokenLogoUpdateUseCase$updateInTokenRepo$1 r0 = new net.safemoon.androidwallet.domain.useCase.startup.forceIcon.ForceTokenLogoUpdateUseCase$updateInTokenRepo$1
            r0.<init>(r5, r6)
        L18:
            java.lang.Object r6 = r0.result
            java.lang.Object r1 = defpackage.gs1.d()
            int r2 = r0.label
            r3 = 1
            if (r2 == 0) goto L37
            if (r2 != r3) goto L2f
            java.lang.Object r0 = r0.L$0
            net.safemoon.androidwallet.domain.useCase.startup.forceIcon.ForceTokenLogoUpdateUseCase r0 = (net.safemoon.androidwallet.domain.useCase.startup.forceIcon.ForceTokenLogoUpdateUseCase) r0
            defpackage.o83.b(r6)     // Catch: java.lang.RuntimeException -> L2d
            goto L65
        L2d:
            r6 = move-exception
            goto L4d
        L2f:
            java.lang.IllegalStateException r6 = new java.lang.IllegalStateException
            java.lang.String r0 = "call to 'resume' before 'invoke' with coroutine"
            r6.<init>(r0)
            throw r6
        L37:
            defpackage.o83.b(r6)
            bn1 r6 = r5.a     // Catch: java.lang.RuntimeException -> L4b
            java.lang.String r2 = r5.d     // Catch: java.lang.RuntimeException -> L4b
            java.lang.String r4 = r5.e     // Catch: java.lang.RuntimeException -> L4b
            r0.L$0 = r5     // Catch: java.lang.RuntimeException -> L4b
            r0.label = r3     // Catch: java.lang.RuntimeException -> L4b
            java.lang.Object r6 = r6.b(r2, r4, r0)     // Catch: java.lang.RuntimeException -> L4b
            if (r6 != r1) goto L65
            return r1
        L4b:
            r6 = move-exception
            r0 = r5
        L4d:
            java.lang.String r6 = r6.getLocalizedMessage()
            if (r6 != 0) goto L65
            java.lang.Class r6 = r0.getClass()
            java.lang.String r6 = r6.getSimpleName()
            java.lang.String r0 = "this::class.java.simpleName"
            defpackage.fs1.e(r6, r0)
            java.lang.String r0 = ""
            defpackage.e30.c0(r0, r6)
        L65:
            te4 r6 = defpackage.te4.a
            return r6
        */
        throw new UnsupportedOperationException("Method not decompiled: net.safemoon.androidwallet.domain.useCase.startup.forceIcon.ForceTokenLogoUpdateUseCase.e(q70):java.lang.Object");
    }
}
