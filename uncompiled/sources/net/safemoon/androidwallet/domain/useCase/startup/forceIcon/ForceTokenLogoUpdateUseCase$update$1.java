package net.safemoon.androidwallet.domain.useCase.startup.forceIcon;

import kotlin.coroutines.jvm.internal.SuspendLambda;
import kotlin.coroutines.jvm.internal.a;

/* compiled from: ForceTokenLogoUpdateUseCase.kt */
@a(c = "net.safemoon.androidwallet.domain.useCase.startup.forceIcon.ForceTokenLogoUpdateUseCase$update$1", f = "ForceTokenLogoUpdateUseCase.kt", l = {20, 21}, m = "invokeSuspend")
/* loaded from: classes2.dex */
public final class ForceTokenLogoUpdateUseCase$update$1 extends SuspendLambda implements hd1<c90, q70<? super te4>, Object> {
    public int label;
    public final /* synthetic */ ForceTokenLogoUpdateUseCase this$0;

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public ForceTokenLogoUpdateUseCase$update$1(ForceTokenLogoUpdateUseCase forceTokenLogoUpdateUseCase, q70<? super ForceTokenLogoUpdateUseCase$update$1> q70Var) {
        super(2, q70Var);
        this.this$0 = forceTokenLogoUpdateUseCase;
    }

    @Override // kotlin.coroutines.jvm.internal.BaseContinuationImpl
    public final q70<te4> create(Object obj, q70<?> q70Var) {
        return new ForceTokenLogoUpdateUseCase$update$1(this.this$0, q70Var);
    }

    @Override // defpackage.hd1
    public final Object invoke(c90 c90Var, q70<? super te4> q70Var) {
        return ((ForceTokenLogoUpdateUseCase$update$1) create(c90Var, q70Var)).invokeSuspend(te4.a);
    }

    @Override // kotlin.coroutines.jvm.internal.BaseContinuationImpl
    public final Object invokeSuspend(Object obj) {
        Object e;
        Object d;
        Object d2 = gs1.d();
        int i = this.label;
        if (i == 0) {
            o83.b(obj);
            ForceTokenLogoUpdateUseCase forceTokenLogoUpdateUseCase = this.this$0;
            this.label = 1;
            e = forceTokenLogoUpdateUseCase.e(this);
            if (e == d2) {
                return d2;
            }
        } else if (i != 1) {
            if (i != 2) {
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
            o83.b(obj);
            return te4.a;
        } else {
            o83.b(obj);
        }
        ForceTokenLogoUpdateUseCase forceTokenLogoUpdateUseCase2 = this.this$0;
        this.label = 2;
        d = forceTokenLogoUpdateUseCase2.d(this);
        if (d == d2) {
            return d2;
        }
        return te4.a;
    }
}
