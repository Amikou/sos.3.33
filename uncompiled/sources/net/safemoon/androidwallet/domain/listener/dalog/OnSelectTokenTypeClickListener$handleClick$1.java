package net.safemoon.androidwallet.domain.listener.dalog;

import com.google.android.material.button.MaterialButton;
import kotlin.jvm.internal.Lambda;
import net.safemoon.androidwallet.R;

/* compiled from: OnSelectTokenTypeClickListener.kt */
/* loaded from: classes2.dex */
public final class OnSelectTokenTypeClickListener$handleClick$1 extends Lambda implements rc1<te4> {
    public final /* synthetic */ MaterialButton $btn;
    public final /* synthetic */ OnSelectTokenTypeClickListener this$0;

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public OnSelectTokenTypeClickListener$handleClick$1(OnSelectTokenTypeClickListener onSelectTokenTypeClickListener, MaterialButton materialButton) {
        super(0);
        this.this$0 = onSelectTokenTypeClickListener;
        this.$btn = materialButton;
    }

    @Override // defpackage.rc1
    public /* bridge */ /* synthetic */ te4 invoke() {
        invoke2();
        return te4.a;
    }

    @Override // defpackage.rc1
    /* renamed from: invoke  reason: avoid collision after fix types in other method */
    public final void invoke2() {
        this.this$0.e(this.$btn, R.drawable.ic_baseline_keyboard_arrow_down_24);
    }
}
