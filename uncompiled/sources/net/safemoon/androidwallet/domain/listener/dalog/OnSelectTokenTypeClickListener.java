package net.safemoon.androidwallet.domain.listener.dalog;

import android.app.Activity;
import android.content.res.Resources;
import android.view.View;
import android.widget.ListPopupWindow;
import com.google.android.material.button.MaterialButton;
import defpackage.lt2;
import java.lang.ref.WeakReference;
import java.util.Map;
import net.safemoon.androidwallet.R;
import net.safemoon.androidwallet.common.TokenType;

/* compiled from: OnSelectTokenTypeClickListener.kt */
/* loaded from: classes2.dex */
public abstract class OnSelectTokenTypeClickListener implements View.OnClickListener {
    public final lt2.a a;
    public final WeakReference<Activity> f0;
    public final Map<String, TokenType> g0;
    public final Resources h0;
    public ListPopupWindow i0;

    /* JADX WARN: Multi-variable type inference failed */
    public OnSelectTokenTypeClickListener(lt2.a aVar, WeakReference<Activity> weakReference, Map<String, ? extends TokenType> map) {
        fs1.f(aVar, "onTokenTypeSelectedListener");
        fs1.f(weakReference, "activity");
        fs1.f(map, "items");
        this.a = aVar;
        this.f0 = weakReference;
        this.g0 = map;
        Activity activity = weakReference.get();
        this.h0 = activity == null ? null : activity.getResources();
    }

    public final void b() {
        ListPopupWindow listPopupWindow = this.i0;
        if (listPopupWindow == null) {
            return;
        }
        listPopupWindow.dismiss();
    }

    public abstract TokenType c();

    public final void d(MaterialButton materialButton) {
        ListPopupWindow listPopupWindow = this.i0;
        if (listPopupWindow != null) {
            fs1.d(listPopupWindow);
            if (listPopupWindow.isShowing()) {
                ListPopupWindow listPopupWindow2 = this.i0;
                if (listPopupWindow2 != null) {
                    listPopupWindow2.dismiss();
                }
                this.i0 = null;
                return;
            }
        }
        e(materialButton, R.drawable.ic_baseline_keyboard_arrow_up_24);
        ListPopupWindow c = lt2.a.c(this.f0, materialButton, this.g0, c(), this.a, new OnSelectTokenTypeClickListener$handleClick$1(this, materialButton));
        this.i0 = c;
        if (c != null) {
            c.setVerticalOffset(materialButton.getHeight() * (-1));
        }
        int[] iArr = new int[2];
        materialButton.getLocationOnScreen(iArr);
        ListPopupWindow listPopupWindow3 = this.i0;
        if (listPopupWindow3 != null) {
            Activity activity = this.f0.get();
            int A = activity == null ? 0 : e30.A(activity);
            ListPopupWindow listPopupWindow4 = this.i0;
            fs1.d(listPopupWindow4);
            listPopupWindow3.setHorizontalOffset(((A - listPopupWindow4.getWidth()) / 2) - iArr[0]);
        }
        ListPopupWindow listPopupWindow5 = this.i0;
        if (listPopupWindow5 == null) {
            return;
        }
        listPopupWindow5.show();
    }

    public final void e(MaterialButton materialButton, int i) {
        Resources resources = this.h0;
        fs1.d(resources);
        materialButton.setIcon(g83.f(resources, i, null));
    }

    @Override // android.view.View.OnClickListener
    public void onClick(View view) {
        if (view instanceof MaterialButton) {
            d((MaterialButton) view);
            return;
        }
        throw new IllegalArgumentException("Only MaterialButton view is supported");
    }
}
