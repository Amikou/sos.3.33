package net.safemoon.androidwallet.database.room;

import android.database.Cursor;
import androidx.room.CoroutinesRoom;
import androidx.room.RoomDatabase;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.concurrent.Callable;
import net.safemoon.androidwallet.model.token.room.RoomCustomToken;
import org.web3j.ens.contracts.generated.PublicResolver;

/* compiled from: CustomTokenDao_Impl.java */
/* loaded from: classes2.dex */
public final class c implements xc0 {
    public final RoomDatabase a;
    public final zv0<RoomCustomToken> b;
    public final co3 c;
    public final co3 d;

    /* compiled from: CustomTokenDao_Impl.java */
    /* loaded from: classes2.dex */
    public class a implements Callable<List<RoomCustomToken>> {
        public final /* synthetic */ k93 a;

        public a(k93 k93Var) {
            this.a = k93Var;
        }

        @Override // java.util.concurrent.Callable
        /* renamed from: a */
        public List<RoomCustomToken> call() throws Exception {
            Cursor c = id0.c(c.this.a, this.a, false, null);
            try {
                int e = wb0.e(c, "symbolWithType");
                int e2 = wb0.e(c, "symbol");
                int e3 = wb0.e(c, PublicResolver.FUNC_NAME);
                int e4 = wb0.e(c, "iconPath");
                int e5 = wb0.e(c, "contractorAddress");
                int e6 = wb0.e(c, "tokenTypeChain");
                int e7 = wb0.e(c, "decimals");
                int e8 = wb0.e(c, "allowSwap");
                int e9 = wb0.e(c, "nativeBalance");
                int e10 = wb0.e(c, "priceInUsdt");
                int e11 = wb0.e(c, "percentChange1h");
                ArrayList arrayList = new ArrayList(c.getCount());
                while (c.moveToNext()) {
                    arrayList.add(new RoomCustomToken(c.isNull(e) ? null : c.getString(e), c.isNull(e2) ? null : c.getString(e2), c.isNull(e3) ? null : c.getString(e3), c.isNull(e4) ? null : c.getString(e4), c.isNull(e5) ? null : c.getString(e5), c.getInt(e6), c.getInt(e7), c.getInt(e8) != 0, c.getDouble(e9), c.getDouble(e10), c.getDouble(e11)));
                }
                return arrayList;
            } finally {
                c.close();
                this.a.f();
            }
        }
    }

    /* compiled from: CustomTokenDao_Impl.java */
    /* loaded from: classes2.dex */
    public class b implements Callable<RoomCustomToken> {
        public final /* synthetic */ k93 a;

        public b(k93 k93Var) {
            this.a = k93Var;
        }

        @Override // java.util.concurrent.Callable
        /* renamed from: a */
        public RoomCustomToken call() throws Exception {
            RoomCustomToken roomCustomToken = null;
            Cursor c = id0.c(c.this.a, this.a, false, null);
            try {
                int e = wb0.e(c, "symbolWithType");
                int e2 = wb0.e(c, "symbol");
                int e3 = wb0.e(c, PublicResolver.FUNC_NAME);
                int e4 = wb0.e(c, "iconPath");
                int e5 = wb0.e(c, "contractorAddress");
                int e6 = wb0.e(c, "tokenTypeChain");
                int e7 = wb0.e(c, "decimals");
                int e8 = wb0.e(c, "allowSwap");
                int e9 = wb0.e(c, "nativeBalance");
                int e10 = wb0.e(c, "priceInUsdt");
                int e11 = wb0.e(c, "percentChange1h");
                if (c.moveToFirst()) {
                    roomCustomToken = new RoomCustomToken(c.isNull(e) ? null : c.getString(e), c.isNull(e2) ? null : c.getString(e2), c.isNull(e3) ? null : c.getString(e3), c.isNull(e4) ? null : c.getString(e4), c.isNull(e5) ? null : c.getString(e5), c.getInt(e6), c.getInt(e7), c.getInt(e8) != 0, c.getDouble(e9), c.getDouble(e10), c.getDouble(e11));
                }
                return roomCustomToken;
            } finally {
                c.close();
                this.a.f();
            }
        }
    }

    /* compiled from: CustomTokenDao_Impl.java */
    /* renamed from: net.safemoon.androidwallet.database.room.c$c  reason: collision with other inner class name */
    /* loaded from: classes2.dex */
    public class C0205c extends zv0<RoomCustomToken> {
        public C0205c(c cVar, RoomDatabase roomDatabase) {
            super(roomDatabase);
        }

        @Override // defpackage.co3
        public String d() {
            return "INSERT OR REPLACE INTO `table_custom` (`symbolWithType`,`symbol`,`name`,`iconPath`,`contractorAddress`,`tokenTypeChain`,`decimals`,`allowSwap`,`nativeBalance`,`priceInUsdt`,`percentChange1h`) VALUES (?,?,?,?,?,?,?,?,?,?,?)";
        }

        @Override // defpackage.zv0
        /* renamed from: k */
        public void g(ww3 ww3Var, RoomCustomToken roomCustomToken) {
            if (roomCustomToken.getSymbolWithType() == null) {
                ww3Var.Y0(1);
            } else {
                ww3Var.L(1, roomCustomToken.getSymbolWithType());
            }
            if (roomCustomToken.getSymbol() == null) {
                ww3Var.Y0(2);
            } else {
                ww3Var.L(2, roomCustomToken.getSymbol());
            }
            if (roomCustomToken.getName() == null) {
                ww3Var.Y0(3);
            } else {
                ww3Var.L(3, roomCustomToken.getName());
            }
            if (roomCustomToken.getIconResName() == null) {
                ww3Var.Y0(4);
            } else {
                ww3Var.L(4, roomCustomToken.getIconResName());
            }
            if (roomCustomToken.getContractAddress() == null) {
                ww3Var.Y0(5);
            } else {
                ww3Var.L(5, roomCustomToken.getContractAddress());
            }
            ww3Var.q0(6, roomCustomToken.getChainId());
            ww3Var.q0(7, roomCustomToken.getDecimals());
            ww3Var.q0(8, roomCustomToken.getAllowSwap() ? 1L : 0L);
            ww3Var.Y(9, roomCustomToken.getNativeBalance());
            ww3Var.Y(10, roomCustomToken.getPriceInUsdt());
            ww3Var.Y(11, roomCustomToken.getPercentChange1h());
        }
    }

    /* compiled from: CustomTokenDao_Impl.java */
    /* loaded from: classes2.dex */
    public class d extends co3 {
        public d(c cVar, RoomDatabase roomDatabase) {
            super(roomDatabase);
        }

        @Override // defpackage.co3
        public String d() {
            return "UPDATE table_custom SET `iconPath` = ? WHERE symbolWithType=?";
        }
    }

    /* compiled from: CustomTokenDao_Impl.java */
    /* loaded from: classes2.dex */
    public class e extends co3 {
        public e(c cVar, RoomDatabase roomDatabase) {
            super(roomDatabase);
        }

        @Override // defpackage.co3
        public String d() {
            return "DELETE FROM table_custom WHERE symbolWithType=?";
        }
    }

    /* compiled from: CustomTokenDao_Impl.java */
    /* loaded from: classes2.dex */
    public class f implements Callable<te4> {
        public final /* synthetic */ RoomCustomToken a;

        public f(RoomCustomToken roomCustomToken) {
            this.a = roomCustomToken;
        }

        @Override // java.util.concurrent.Callable
        /* renamed from: a */
        public te4 call() throws Exception {
            c.this.a.e();
            try {
                c.this.b.h(this.a);
                c.this.a.E();
                return te4.a;
            } finally {
                c.this.a.j();
            }
        }
    }

    /* compiled from: CustomTokenDao_Impl.java */
    /* loaded from: classes2.dex */
    public class g implements Callable<te4> {
        public final /* synthetic */ String a;
        public final /* synthetic */ String b;

        public g(String str, String str2) {
            this.a = str;
            this.b = str2;
        }

        @Override // java.util.concurrent.Callable
        /* renamed from: a */
        public te4 call() throws Exception {
            ww3 a = c.this.c.a();
            String str = this.a;
            if (str == null) {
                a.Y0(1);
            } else {
                a.L(1, str);
            }
            String str2 = this.b;
            if (str2 == null) {
                a.Y0(2);
            } else {
                a.L(2, str2);
            }
            c.this.a.e();
            try {
                a.T();
                c.this.a.E();
                return te4.a;
            } finally {
                c.this.a.j();
                c.this.c.f(a);
            }
        }
    }

    /* compiled from: CustomTokenDao_Impl.java */
    /* loaded from: classes2.dex */
    public class h implements Callable<te4> {
        public final /* synthetic */ String a;

        public h(String str) {
            this.a = str;
        }

        @Override // java.util.concurrent.Callable
        /* renamed from: a */
        public te4 call() throws Exception {
            ww3 a = c.this.d.a();
            String str = this.a;
            if (str == null) {
                a.Y0(1);
            } else {
                a.L(1, str);
            }
            c.this.a.e();
            try {
                a.T();
                c.this.a.E();
                return te4.a;
            } finally {
                c.this.a.j();
                c.this.d.f(a);
            }
        }
    }

    /* compiled from: CustomTokenDao_Impl.java */
    /* loaded from: classes2.dex */
    public class i implements Callable<List<RoomCustomToken>> {
        public final /* synthetic */ k93 a;

        public i(k93 k93Var) {
            this.a = k93Var;
        }

        @Override // java.util.concurrent.Callable
        /* renamed from: a */
        public List<RoomCustomToken> call() throws Exception {
            Cursor c = id0.c(c.this.a, this.a, false, null);
            try {
                int e = wb0.e(c, "symbolWithType");
                int e2 = wb0.e(c, "symbol");
                int e3 = wb0.e(c, PublicResolver.FUNC_NAME);
                int e4 = wb0.e(c, "iconPath");
                int e5 = wb0.e(c, "contractorAddress");
                int e6 = wb0.e(c, "tokenTypeChain");
                int e7 = wb0.e(c, "decimals");
                int e8 = wb0.e(c, "allowSwap");
                int e9 = wb0.e(c, "nativeBalance");
                int e10 = wb0.e(c, "priceInUsdt");
                int e11 = wb0.e(c, "percentChange1h");
                ArrayList arrayList = new ArrayList(c.getCount());
                while (c.moveToNext()) {
                    arrayList.add(new RoomCustomToken(c.isNull(e) ? null : c.getString(e), c.isNull(e2) ? null : c.getString(e2), c.isNull(e3) ? null : c.getString(e3), c.isNull(e4) ? null : c.getString(e4), c.isNull(e5) ? null : c.getString(e5), c.getInt(e6), c.getInt(e7), c.getInt(e8) != 0, c.getDouble(e9), c.getDouble(e10), c.getDouble(e11)));
                }
                return arrayList;
            } finally {
                c.close();
                this.a.f();
            }
        }
    }

    public c(RoomDatabase roomDatabase) {
        this.a = roomDatabase;
        this.b = new C0205c(this, roomDatabase);
        this.c = new d(this, roomDatabase);
        this.d = new e(this, roomDatabase);
    }

    public static List<Class<?>> e() {
        return Collections.emptyList();
    }

    @Override // defpackage.xc0
    public Object g(int i2, q70<? super List<RoomCustomToken>> q70Var) {
        k93 c = k93.c("SELECT * FROM table_custom WHERE tokenTypeChain LIKE ?", 1);
        c.q0(1, i2);
        return CoroutinesRoom.a(this.a, false, id0.a(), new i(c), q70Var);
    }

    @Override // defpackage.xc0
    public Object h(String str, String str2, q70<? super te4> q70Var) {
        return CoroutinesRoom.b(this.a, true, new g(str2, str), q70Var);
    }

    @Override // defpackage.xc0
    public Object i(RoomCustomToken roomCustomToken, q70<? super te4> q70Var) {
        return CoroutinesRoom.b(this.a, true, new f(roomCustomToken), q70Var);
    }

    @Override // defpackage.xc0
    public Object j(String str, q70<? super RoomCustomToken> q70Var) {
        k93 c = k93.c("SELECT * FROM table_custom WHERE iconPath=?", 1);
        if (str == null) {
            c.Y0(1);
        } else {
            c.L(1, str);
        }
        return CoroutinesRoom.a(this.a, false, id0.a(), new b(c), q70Var);
    }

    @Override // defpackage.xc0
    public Object k(q70<? super List<RoomCustomToken>> q70Var) {
        k93 c = k93.c("SELECT * FROM table_custom ", 0);
        return CoroutinesRoom.a(this.a, false, id0.a(), new a(c), q70Var);
    }

    @Override // defpackage.xc0
    public Object l(String str, q70<? super te4> q70Var) {
        return CoroutinesRoom.b(this.a, true, new h(str), q70Var);
    }
}
