package net.safemoon.androidwallet.database.room;

import android.content.Context;
import androidx.room.RoomDatabase;
import androidx.room.l;
import java.util.List;
import kotlin.Pair;
import net.safemoon.androidwallet.common.TokenType;
import net.safemoon.androidwallet.model.wallets.Wallet;

/* compiled from: ApplicationRoomDatabase.kt */
/* loaded from: classes2.dex */
public abstract class ApplicationRoomDatabase extends RoomDatabase {
    public static ApplicationRoomDatabase o;
    public static final k n = new k(null);
    public static final b p = new b();
    public static final c q = new c();
    public static final d r = new d();
    public static final e s = new e();
    public static final f t = new f();
    public static final g u = new g();
    public static final h v = new h();
    public static final i w = new i();
    public static final j x = new j();
    public static final a y = new a();

    /* compiled from: ApplicationRoomDatabase.kt */
    /* loaded from: classes2.dex */
    public static final class a extends w82 {
        public a() {
            super(10, 11);
        }

        @Override // defpackage.w82
        public void a(sw3 sw3Var) {
            fs1.f(sw3Var, "database");
            sw3Var.K("ALTER TABLE `table_room_nft` ADD COLUMN `image_data` TEXT");
        }
    }

    /* compiled from: ApplicationRoomDatabase.kt */
    /* loaded from: classes2.dex */
    public static final class b extends w82 {
        public b() {
            super(1, 2);
        }

        @Override // defpackage.w82
        public void a(sw3 sw3Var) {
            fs1.f(sw3Var, "database");
            sw3Var.K("CREATE TABLE IF NOT EXISTS `table_fiat` (`symbol` TEXT PRIMARY KEY NOT NULL, `name` TEXT, `rate` REAL)");
        }
    }

    /* compiled from: ApplicationRoomDatabase.kt */
    /* loaded from: classes2.dex */
    public static final class c extends w82 {
        public c() {
            super(2, 3);
        }

        @Override // defpackage.w82
        public void a(sw3 sw3Var) {
            fs1.f(sw3Var, "database");
            sw3Var.K("CREATE TABLE IF NOT EXISTS `table_contact` (`_id` INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, `name` TEXT NOT NULL, `address` TEXT NOT NULL, `tokenTypeChain` INTEGER NOT NULL, `profilePath` TEXT NOT NULL, `lastSent` INTEGER, `contactCreate` INTEGER)");
        }
    }

    /* compiled from: ApplicationRoomDatabase.kt */
    /* loaded from: classes2.dex */
    public static final class d extends w82 {
        public d() {
            super(3, 4);
        }

        @Override // defpackage.w82
        public void a(sw3 sw3Var) {
            fs1.f(sw3Var, "database");
            sw3Var.K("CREATE TABLE IF NOT EXISTS `table_custom` (`symbolWithType` TEXT NOT NULL, `symbol` TEXT NOT NULL, `name` TEXT NOT NULL, `iconPath` TEXT NOT NULL, `contractorAddress` TEXT NOT NULL, `tokenTypeChain` INTEGER NOT NULL, `decimals` INTEGER NOT NULL, `allowSwap` INTEGER NOT NULL, `nativeBalance` REAL NOT NULL, `priceInUsdt` REAL NOT NULL, `percentChange1h` REAL NOT NULL, PRIMARY KEY(`symbolWithType`))");
        }
    }

    /* compiled from: ApplicationRoomDatabase.kt */
    /* loaded from: classes2.dex */
    public static final class e extends w82 {
        public e() {
            super(4, 5);
        }

        @Override // defpackage.w82
        public void a(sw3 sw3Var) {
            fs1.f(sw3Var, "database");
            sw3Var.K("ALTER TABLE table_token ADD COLUMN `order` INTEGER NOT NULL DEFAULT 0");
        }
    }

    /* compiled from: ApplicationRoomDatabase.kt */
    /* loaded from: classes2.dex */
    public static final class f extends w82 {
        public f() {
            super(5, 6);
        }

        @Override // defpackage.w82
        public void a(sw3 sw3Var) {
            fs1.f(sw3Var, "database");
        }
    }

    /* compiled from: ApplicationRoomDatabase.kt */
    /* loaded from: classes2.dex */
    public static final class g extends w82 {
        public g() {
            super(6, 7);
        }

        @Override // defpackage.w82
        public void a(sw3 sw3Var) {
            fs1.f(sw3Var, "database");
            sw3Var.K("CREATE TABLE IF NOT EXISTS `table_room_reflections_token_2` (`_id` INTEGER PRIMARY KEY AUTOINCREMENT, `symbolWithType` TEXT NOT NULL, `symbol` TEXT NOT NULL, `name` TEXT NOT NULL, `iconPath` TEXT NOT NULL, `contractorAddress` TEXT NOT NULL, `tokenTypeChain` INTEGER NOT NULL, `decimals` INTEGER NOT NULL, `nativeBalance` TEXT, `firstTimeStamp` INTEGER, `enableAdvanceMode` INTEGER NOT NULL DEFAULT 0, `latestBalance` INTEGER, `latestTimeStamp` INTEGER, `cmcId` INTEGER DEFAULT 0, `priceUsd` REAL DEFAULT 0)");
            sw3Var.K("CREATE TABLE IF NOT EXISTS `table_room_reflections_data_2` (`_id` INTEGER PRIMARY KEY AUTOINCREMENT, `symbolWithType` TEXT NOT NULL, `nativeBalance` TEXT NOT NULL, `blockBalance` TEXT NOT NULL, `block` TEXT NOT NULL, `timeStamp` INTEGER NOT NULL)");
        }
    }

    /* compiled from: ApplicationRoomDatabase.kt */
    /* loaded from: classes2.dex */
    public static final class h extends w82 {
        public h() {
            super(7, 8);
        }

        @Override // defpackage.w82
        public void a(sw3 sw3Var) {
            fs1.f(sw3Var, "database");
            sw3Var.K("CREATE TABLE IF NOT EXISTS `table_room_token_info` (`_id` INTEGER PRIMARY KEY AUTOINCREMENT, `symbolWithType` TEXT NOT NULL, `chainId` INTEGER NOT NULL, `slug` TEXT, `cmcId` INTEGER)");
        }
    }

    /* compiled from: ApplicationRoomDatabase.kt */
    /* loaded from: classes2.dex */
    public static final class i extends w82 {
        public i() {
            super(8, 9);
        }

        @Override // defpackage.w82
        public void a(sw3 sw3Var) {
            fs1.f(sw3Var, "database");
            sw3Var.K("CREATE TABLE IF NOT EXISTS `table_room_coin_price_alert` (`id` INTEGER NOT NULL, `symbol` TEXT NOT NULL, `name` TEXT NOT NULL, `slug` TEXT NOT NULL, `cmcData` TEXT NOT NULL, PRIMARY KEY(`id`))");
            sw3Var.K("CREATE TABLE IF NOT EXISTS `table_room_nft` (`_id` INTEGER PRIMARY KEY AUTOINCREMENT, `chain` INTEGER NOT NULL, `collectionId` INTEGER NOT NULL, `token_id` TEXT, `name` TEXT, `description` TEXT, `animation_url` TEXT, `image_preview_url` TEXT, `token_uri` TEXT, `attributes` TEXT, `fullData` TEXT, `permalink` TEXT, `openSeaUrl` TEXT, `updatedTime` INTEGER NOT NULL, `order` INTEGER NOT NULL)");
            sw3Var.K("CREATE TABLE IF NOT EXISTS `table_room_collections` (`_id` INTEGER PRIMARY KEY AUTOINCREMENT, `chain` INTEGER NOT NULL, `contractAddress` TEXT NOT NULL, `contract_type` TEXT NOT NULL, `name` TEXT, `description` TEXT, `imageUrl` TEXT, `symbol` TEXT, `marketPlace` TEXT, `slug` TEXT, `updatedTime` INTEGER NOT NULL, `order` INTEGER NOT NULL)");
        }
    }

    /* compiled from: ApplicationRoomDatabase.kt */
    /* loaded from: classes2.dex */
    public static final class j extends w82 {
        public j() {
            super(9, 10);
        }

        @Override // defpackage.w82
        public void a(sw3 sw3Var) {
            fs1.f(sw3Var, "database");
            sw3Var.K("ALTER TABLE `table_room_collections` ADD COLUMN `typeDeleteNft` INTEGER NOT NULL DEFAULT 0");
        }
    }

    /* compiled from: ApplicationRoomDatabase.kt */
    /* loaded from: classes2.dex */
    public static final class k {
        public k() {
        }

        public /* synthetic */ k(qi0 qi0Var) {
            this();
        }

        public static /* synthetic */ ApplicationRoomDatabase c(k kVar, Context context, String str, int i, Object obj) {
            if ((i & 2) != 0) {
                str = null;
            }
            return kVar.b(context, str);
        }

        public final void a(Context context) {
            fs1.f(context, "context");
            c(this, context, null, 2, null).f();
        }

        public final ApplicationRoomDatabase b(Context context, String str) {
            fs1.f(context, "context");
            if (ApplicationRoomDatabase.o == null) {
                synchronized (this) {
                    Wallet c = e30.c(context);
                    if (str == null) {
                        if (c != null) {
                            str = c.getPrefix();
                            if (str == null) {
                            }
                        }
                        str = "";
                    }
                    String l = fs1.l(str.length() == 0 ? "" : fs1.l(str, "_"), "safemoon");
                    sn4.a.f(l);
                    if (ApplicationRoomDatabase.o == null) {
                        k kVar = ApplicationRoomDatabase.n;
                        ApplicationRoomDatabase.o = (ApplicationRoomDatabase) l.a(context.getApplicationContext(), ApplicationRoomDatabase.class, l).b(ApplicationRoomDatabase.p).b(ApplicationRoomDatabase.q).b(ApplicationRoomDatabase.r).b(ApplicationRoomDatabase.s).b(ApplicationRoomDatabase.t).b(ApplicationRoomDatabase.u).b(ApplicationRoomDatabase.v).b(ApplicationRoomDatabase.w).b(ApplicationRoomDatabase.x).b(ApplicationRoomDatabase.y).d();
                    }
                    te4 te4Var = te4.a;
                }
            }
            ApplicationRoomDatabase applicationRoomDatabase = ApplicationRoomDatabase.o;
            fs1.d(applicationRoomDatabase);
            return applicationRoomDatabase;
        }

        public final List<Pair<TokenType, List<String>>> d() {
            return b20.j(qc4.a(TokenType.BEP_20, b20.j("BEP_BNB", "BEP_SAFEMOON_V2")), qc4.a(TokenType.ERC_20, b20.j("ERC_ETH", "ERC_PSAFEMOON")), qc4.a(TokenType.POLYGON, b20.j("POLYGON_MATIC", "POLYGON_USDT")), qc4.a(TokenType.AVALANCHE_C, b20.j("AVALANCHE_C_AVAX", "AVALANCHE_C_USDT")));
        }

        public final Pair<List<String>, List<String>> e() {
            return new Pair<>(a20.b("BEP_SAFEMOON_V2"), b20.g());
        }

        public final void f() {
            ApplicationRoomDatabase.o = null;
            za.a.a();
            gg4.a.a();
            e53.a.b();
            a70.a.b();
            zc0.a.b();
        }
    }

    public abstract l00 S();

    public abstract j10 T();

    public abstract xc0 U();

    public abstract a31 V();

    public abstract of2 W();

    public abstract t53 X();

    public abstract x64 Y();

    public abstract eg4 Z();
}
