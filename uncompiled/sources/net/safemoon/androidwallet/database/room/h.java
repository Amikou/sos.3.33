package net.safemoon.androidwallet.database.room;

import android.database.Cursor;
import androidx.lifecycle.LiveData;
import androidx.room.CoroutinesRoom;
import androidx.room.RoomDatabase;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.concurrent.Callable;
import net.safemoon.androidwallet.model.token.room.RoomToken;
import org.web3j.ens.contracts.generated.PublicResolver;

/* compiled from: UserTokenListDao_Impl.java */
/* loaded from: classes2.dex */
public final class h implements eg4 {
    public final RoomDatabase a;
    public final zv0<RoomToken> b;
    public final yv0<RoomToken> c;
    public final co3 d;
    public final co3 e;
    public final co3 f;
    public final co3 g;

    /* compiled from: UserTokenListDao_Impl.java */
    /* loaded from: classes2.dex */
    public class a implements Callable<List<RoomToken>> {
        public final /* synthetic */ k93 a;

        public a(k93 k93Var) {
            this.a = k93Var;
        }

        @Override // java.util.concurrent.Callable
        /* renamed from: a */
        public List<RoomToken> call() throws Exception {
            Cursor c = id0.c(h.this.a, this.a, false, null);
            try {
                int e = wb0.e(c, "symbolWithType");
                int e2 = wb0.e(c, "symbol");
                int e3 = wb0.e(c, PublicResolver.FUNC_NAME);
                int e4 = wb0.e(c, "iconName");
                int e5 = wb0.e(c, "contractorAddress");
                int e6 = wb0.e(c, "tokenTypeChain");
                int e7 = wb0.e(c, "decimals");
                int e8 = wb0.e(c, "allowSwap");
                int e9 = wb0.e(c, "nativeBalance");
                int e10 = wb0.e(c, "priceInUsdt");
                int e11 = wb0.e(c, "percentChange1h");
                int e12 = wb0.e(c, "order");
                ArrayList arrayList = new ArrayList(c.getCount());
                while (c.moveToNext()) {
                    arrayList.add(new RoomToken(c.isNull(e) ? null : c.getString(e), c.isNull(e2) ? null : c.getString(e2), c.isNull(e3) ? null : c.getString(e3), c.isNull(e4) ? null : c.getString(e4), c.isNull(e5) ? null : c.getString(e5), c.getInt(e6), c.getInt(e7), c.getInt(e8) != 0, c.getDouble(e9), c.getDouble(e10), c.getDouble(e11), c.getInt(e12)));
                }
                return arrayList;
            } finally {
                c.close();
            }
        }

        public void finalize() {
            this.a.f();
        }
    }

    /* compiled from: UserTokenListDao_Impl.java */
    /* loaded from: classes2.dex */
    public class b implements Callable<List<RoomToken>> {
        public final /* synthetic */ k93 a;

        public b(k93 k93Var) {
            this.a = k93Var;
        }

        @Override // java.util.concurrent.Callable
        /* renamed from: a */
        public List<RoomToken> call() throws Exception {
            Cursor c = id0.c(h.this.a, this.a, false, null);
            try {
                int e = wb0.e(c, "symbolWithType");
                int e2 = wb0.e(c, "symbol");
                int e3 = wb0.e(c, PublicResolver.FUNC_NAME);
                int e4 = wb0.e(c, "iconName");
                int e5 = wb0.e(c, "contractorAddress");
                int e6 = wb0.e(c, "tokenTypeChain");
                int e7 = wb0.e(c, "decimals");
                int e8 = wb0.e(c, "allowSwap");
                int e9 = wb0.e(c, "nativeBalance");
                int e10 = wb0.e(c, "priceInUsdt");
                int e11 = wb0.e(c, "percentChange1h");
                int e12 = wb0.e(c, "order");
                ArrayList arrayList = new ArrayList(c.getCount());
                while (c.moveToNext()) {
                    arrayList.add(new RoomToken(c.isNull(e) ? null : c.getString(e), c.isNull(e2) ? null : c.getString(e2), c.isNull(e3) ? null : c.getString(e3), c.isNull(e4) ? null : c.getString(e4), c.isNull(e5) ? null : c.getString(e5), c.getInt(e6), c.getInt(e7), c.getInt(e8) != 0, c.getDouble(e9), c.getDouble(e10), c.getDouble(e11), c.getInt(e12)));
                }
                return arrayList;
            } finally {
                c.close();
                this.a.f();
            }
        }
    }

    /* compiled from: UserTokenListDao_Impl.java */
    /* loaded from: classes2.dex */
    public class c implements Callable<List<RoomToken>> {
        public final /* synthetic */ k93 a;

        public c(k93 k93Var) {
            this.a = k93Var;
        }

        @Override // java.util.concurrent.Callable
        /* renamed from: a */
        public List<RoomToken> call() throws Exception {
            Cursor c = id0.c(h.this.a, this.a, false, null);
            try {
                int e = wb0.e(c, "symbolWithType");
                int e2 = wb0.e(c, "symbol");
                int e3 = wb0.e(c, PublicResolver.FUNC_NAME);
                int e4 = wb0.e(c, "iconName");
                int e5 = wb0.e(c, "contractorAddress");
                int e6 = wb0.e(c, "tokenTypeChain");
                int e7 = wb0.e(c, "decimals");
                int e8 = wb0.e(c, "allowSwap");
                int e9 = wb0.e(c, "nativeBalance");
                int e10 = wb0.e(c, "priceInUsdt");
                int e11 = wb0.e(c, "percentChange1h");
                int e12 = wb0.e(c, "order");
                ArrayList arrayList = new ArrayList(c.getCount());
                while (c.moveToNext()) {
                    arrayList.add(new RoomToken(c.isNull(e) ? null : c.getString(e), c.isNull(e2) ? null : c.getString(e2), c.isNull(e3) ? null : c.getString(e3), c.isNull(e4) ? null : c.getString(e4), c.isNull(e5) ? null : c.getString(e5), c.getInt(e6), c.getInt(e7), c.getInt(e8) != 0, c.getDouble(e9), c.getDouble(e10), c.getDouble(e11), c.getInt(e12)));
                }
                return arrayList;
            } finally {
                c.close();
            }
        }

        public void finalize() {
            this.a.f();
        }
    }

    /* compiled from: UserTokenListDao_Impl.java */
    /* loaded from: classes2.dex */
    public class d implements Callable<List<RoomToken>> {
        public final /* synthetic */ k93 a;

        public d(k93 k93Var) {
            this.a = k93Var;
        }

        @Override // java.util.concurrent.Callable
        /* renamed from: a */
        public List<RoomToken> call() throws Exception {
            Cursor c = id0.c(h.this.a, this.a, false, null);
            try {
                int e = wb0.e(c, "symbolWithType");
                int e2 = wb0.e(c, "symbol");
                int e3 = wb0.e(c, PublicResolver.FUNC_NAME);
                int e4 = wb0.e(c, "iconName");
                int e5 = wb0.e(c, "contractorAddress");
                int e6 = wb0.e(c, "tokenTypeChain");
                int e7 = wb0.e(c, "decimals");
                int e8 = wb0.e(c, "allowSwap");
                int e9 = wb0.e(c, "nativeBalance");
                int e10 = wb0.e(c, "priceInUsdt");
                int e11 = wb0.e(c, "percentChange1h");
                int e12 = wb0.e(c, "order");
                ArrayList arrayList = new ArrayList(c.getCount());
                while (c.moveToNext()) {
                    arrayList.add(new RoomToken(c.isNull(e) ? null : c.getString(e), c.isNull(e2) ? null : c.getString(e2), c.isNull(e3) ? null : c.getString(e3), c.isNull(e4) ? null : c.getString(e4), c.isNull(e5) ? null : c.getString(e5), c.getInt(e6), c.getInt(e7), c.getInt(e8) != 0, c.getDouble(e9), c.getDouble(e10), c.getDouble(e11), c.getInt(e12)));
                }
                return arrayList;
            } finally {
                c.close();
                this.a.f();
            }
        }
    }

    /* compiled from: UserTokenListDao_Impl.java */
    /* loaded from: classes2.dex */
    public class e implements Callable<RoomToken> {
        public final /* synthetic */ k93 a;

        public e(k93 k93Var) {
            this.a = k93Var;
        }

        @Override // java.util.concurrent.Callable
        /* renamed from: a */
        public RoomToken call() throws Exception {
            RoomToken roomToken = null;
            Cursor c = id0.c(h.this.a, this.a, false, null);
            try {
                int e = wb0.e(c, "symbolWithType");
                int e2 = wb0.e(c, "symbol");
                int e3 = wb0.e(c, PublicResolver.FUNC_NAME);
                int e4 = wb0.e(c, "iconName");
                int e5 = wb0.e(c, "contractorAddress");
                int e6 = wb0.e(c, "tokenTypeChain");
                int e7 = wb0.e(c, "decimals");
                int e8 = wb0.e(c, "allowSwap");
                int e9 = wb0.e(c, "nativeBalance");
                int e10 = wb0.e(c, "priceInUsdt");
                int e11 = wb0.e(c, "percentChange1h");
                int e12 = wb0.e(c, "order");
                if (c.moveToFirst()) {
                    roomToken = new RoomToken(c.isNull(e) ? null : c.getString(e), c.isNull(e2) ? null : c.getString(e2), c.isNull(e3) ? null : c.getString(e3), c.isNull(e4) ? null : c.getString(e4), c.isNull(e5) ? null : c.getString(e5), c.getInt(e6), c.getInt(e7), c.getInt(e8) != 0, c.getDouble(e9), c.getDouble(e10), c.getDouble(e11), c.getInt(e12));
                }
                return roomToken;
            } finally {
                c.close();
                this.a.f();
            }
        }
    }

    /* compiled from: UserTokenListDao_Impl.java */
    /* loaded from: classes2.dex */
    public class f implements Callable<RoomToken> {
        public final /* synthetic */ k93 a;

        public f(k93 k93Var) {
            this.a = k93Var;
        }

        @Override // java.util.concurrent.Callable
        /* renamed from: a */
        public RoomToken call() throws Exception {
            RoomToken roomToken = null;
            Cursor c = id0.c(h.this.a, this.a, false, null);
            try {
                int e = wb0.e(c, "symbolWithType");
                int e2 = wb0.e(c, "symbol");
                int e3 = wb0.e(c, PublicResolver.FUNC_NAME);
                int e4 = wb0.e(c, "iconName");
                int e5 = wb0.e(c, "contractorAddress");
                int e6 = wb0.e(c, "tokenTypeChain");
                int e7 = wb0.e(c, "decimals");
                int e8 = wb0.e(c, "allowSwap");
                int e9 = wb0.e(c, "nativeBalance");
                int e10 = wb0.e(c, "priceInUsdt");
                int e11 = wb0.e(c, "percentChange1h");
                int e12 = wb0.e(c, "order");
                if (c.moveToFirst()) {
                    roomToken = new RoomToken(c.isNull(e) ? null : c.getString(e), c.isNull(e2) ? null : c.getString(e2), c.isNull(e3) ? null : c.getString(e3), c.isNull(e4) ? null : c.getString(e4), c.isNull(e5) ? null : c.getString(e5), c.getInt(e6), c.getInt(e7), c.getInt(e8) != 0, c.getDouble(e9), c.getDouble(e10), c.getDouble(e11), c.getInt(e12));
                }
                return roomToken;
            } finally {
                c.close();
                this.a.f();
            }
        }
    }

    /* compiled from: UserTokenListDao_Impl.java */
    /* loaded from: classes2.dex */
    public class g implements Callable<RoomToken> {
        public final /* synthetic */ k93 a;

        public g(k93 k93Var) {
            this.a = k93Var;
        }

        @Override // java.util.concurrent.Callable
        /* renamed from: a */
        public RoomToken call() throws Exception {
            RoomToken roomToken = null;
            Cursor c = id0.c(h.this.a, this.a, false, null);
            try {
                int e = wb0.e(c, "symbolWithType");
                int e2 = wb0.e(c, "symbol");
                int e3 = wb0.e(c, PublicResolver.FUNC_NAME);
                int e4 = wb0.e(c, "iconName");
                int e5 = wb0.e(c, "contractorAddress");
                int e6 = wb0.e(c, "tokenTypeChain");
                int e7 = wb0.e(c, "decimals");
                int e8 = wb0.e(c, "allowSwap");
                int e9 = wb0.e(c, "nativeBalance");
                int e10 = wb0.e(c, "priceInUsdt");
                int e11 = wb0.e(c, "percentChange1h");
                int e12 = wb0.e(c, "order");
                if (c.moveToFirst()) {
                    roomToken = new RoomToken(c.isNull(e) ? null : c.getString(e), c.isNull(e2) ? null : c.getString(e2), c.isNull(e3) ? null : c.getString(e3), c.isNull(e4) ? null : c.getString(e4), c.isNull(e5) ? null : c.getString(e5), c.getInt(e6), c.getInt(e7), c.getInt(e8) != 0, c.getDouble(e9), c.getDouble(e10), c.getDouble(e11), c.getInt(e12));
                }
                return roomToken;
            } finally {
                c.close();
            }
        }

        public void finalize() {
            this.a.f();
        }
    }

    /* compiled from: UserTokenListDao_Impl.java */
    /* renamed from: net.safemoon.androidwallet.database.room.h$h  reason: collision with other inner class name */
    /* loaded from: classes2.dex */
    public class C0209h extends zv0<RoomToken> {
        public C0209h(h hVar, RoomDatabase roomDatabase) {
            super(roomDatabase);
        }

        @Override // defpackage.co3
        public String d() {
            return "INSERT OR REPLACE INTO `table_token` (`symbolWithType`,`symbol`,`name`,`iconName`,`contractorAddress`,`tokenTypeChain`,`decimals`,`allowSwap`,`nativeBalance`,`priceInUsdt`,`percentChange1h`,`order`) VALUES (?,?,?,?,?,?,?,?,?,?,?,?)";
        }

        @Override // defpackage.zv0
        /* renamed from: k */
        public void g(ww3 ww3Var, RoomToken roomToken) {
            if (roomToken.getSymbolWithType() == null) {
                ww3Var.Y0(1);
            } else {
                ww3Var.L(1, roomToken.getSymbolWithType());
            }
            if (roomToken.getSymbol() == null) {
                ww3Var.Y0(2);
            } else {
                ww3Var.L(2, roomToken.getSymbol());
            }
            if (roomToken.getName() == null) {
                ww3Var.Y0(3);
            } else {
                ww3Var.L(3, roomToken.getName());
            }
            if (roomToken.getIconResName() == null) {
                ww3Var.Y0(4);
            } else {
                ww3Var.L(4, roomToken.getIconResName());
            }
            if (roomToken.getContractAddress() == null) {
                ww3Var.Y0(5);
            } else {
                ww3Var.L(5, roomToken.getContractAddress());
            }
            ww3Var.q0(6, roomToken.getChainId());
            ww3Var.q0(7, roomToken.getDecimals());
            ww3Var.q0(8, roomToken.getAllowSwap() ? 1L : 0L);
            ww3Var.Y(9, roomToken.getNativeBalance());
            ww3Var.Y(10, roomToken.getPriceInUsdt());
            ww3Var.Y(11, roomToken.getPercentChange1h());
            ww3Var.q0(12, roomToken.getOrder());
        }
    }

    /* compiled from: UserTokenListDao_Impl.java */
    /* loaded from: classes2.dex */
    public class i extends yv0<RoomToken> {
        public i(h hVar, RoomDatabase roomDatabase) {
            super(roomDatabase);
        }

        @Override // defpackage.co3
        public String d() {
            return "DELETE FROM `table_token` WHERE `symbolWithType` = ?";
        }

        @Override // defpackage.yv0
        /* renamed from: i */
        public void g(ww3 ww3Var, RoomToken roomToken) {
            if (roomToken.getSymbolWithType() == null) {
                ww3Var.Y0(1);
            } else {
                ww3Var.L(1, roomToken.getSymbolWithType());
            }
        }
    }

    /* compiled from: UserTokenListDao_Impl.java */
    /* loaded from: classes2.dex */
    public class j extends co3 {
        public j(h hVar, RoomDatabase roomDatabase) {
            super(roomDatabase);
        }

        @Override // defpackage.co3
        public String d() {
            return "UPDATE table_token SET nativeBalance = ? WHERE symbolWithType=?";
        }
    }

    /* compiled from: UserTokenListDao_Impl.java */
    /* loaded from: classes2.dex */
    public class k extends co3 {
        public k(h hVar, RoomDatabase roomDatabase) {
            super(roomDatabase);
        }

        @Override // defpackage.co3
        public String d() {
            return "UPDATE table_token SET priceInUsdt = ?, percentChange1h = ? WHERE symbolWithType=?";
        }
    }

    /* compiled from: UserTokenListDao_Impl.java */
    /* loaded from: classes2.dex */
    public class l extends co3 {
        public l(h hVar, RoomDatabase roomDatabase) {
            super(roomDatabase);
        }

        @Override // defpackage.co3
        public String d() {
            return "UPDATE table_token SET `order` = ? WHERE symbolWithType=?";
        }
    }

    /* compiled from: UserTokenListDao_Impl.java */
    /* loaded from: classes2.dex */
    public class m extends co3 {
        public m(h hVar, RoomDatabase roomDatabase) {
            super(roomDatabase);
        }

        @Override // defpackage.co3
        public String d() {
            return "UPDATE table_token SET `iconName` = ? WHERE symbolWithType=?";
        }
    }

    /* compiled from: UserTokenListDao_Impl.java */
    /* loaded from: classes2.dex */
    public class n extends co3 {
        public n(h hVar, RoomDatabase roomDatabase) {
            super(roomDatabase);
        }

        @Override // defpackage.co3
        public String d() {
            return "DELETE FROM table_token";
        }
    }

    /* compiled from: UserTokenListDao_Impl.java */
    /* loaded from: classes2.dex */
    public class o implements Callable<te4> {
        public final /* synthetic */ int a;
        public final /* synthetic */ String b;

        public o(int i, String str) {
            this.a = i;
            this.b = str;
        }

        @Override // java.util.concurrent.Callable
        /* renamed from: a */
        public te4 call() throws Exception {
            ww3 a = h.this.f.a();
            a.q0(1, this.a);
            String str = this.b;
            if (str == null) {
                a.Y0(2);
            } else {
                a.L(2, str);
            }
            h.this.a.e();
            try {
                a.T();
                h.this.a.E();
                return te4.a;
            } finally {
                h.this.a.j();
                h.this.f.f(a);
            }
        }
    }

    /* compiled from: UserTokenListDao_Impl.java */
    /* loaded from: classes2.dex */
    public class p implements Callable<te4> {
        public final /* synthetic */ String a;
        public final /* synthetic */ String b;

        public p(String str, String str2) {
            this.a = str;
            this.b = str2;
        }

        @Override // java.util.concurrent.Callable
        /* renamed from: a */
        public te4 call() throws Exception {
            ww3 a = h.this.g.a();
            String str = this.a;
            if (str == null) {
                a.Y0(1);
            } else {
                a.L(1, str);
            }
            String str2 = this.b;
            if (str2 == null) {
                a.Y0(2);
            } else {
                a.L(2, str2);
            }
            h.this.a.e();
            try {
                a.T();
                h.this.a.E();
                return te4.a;
            } finally {
                h.this.a.j();
                h.this.g.f(a);
            }
        }
    }

    public h(RoomDatabase roomDatabase) {
        this.a = roomDatabase;
        this.b = new C0209h(this, roomDatabase);
        this.c = new i(this, roomDatabase);
        this.d = new j(this, roomDatabase);
        this.e = new k(this, roomDatabase);
        this.f = new l(this, roomDatabase);
        this.g = new m(this, roomDatabase);
        new n(this, roomDatabase);
    }

    public static List<Class<?>> r() {
        return Collections.emptyList();
    }

    @Override // defpackage.eg4
    public LiveData<List<RoomToken>> a() {
        return this.a.n().e(new String[]{"table_token"}, false, new c(k93.c("SELECT * FROM table_token ORDER BY `order` ASC", 0)));
    }

    @Override // defpackage.eg4
    public Object b(String str, String str2, q70<? super te4> q70Var) {
        return CoroutinesRoom.b(this.a, true, new p(str2, str), q70Var);
    }

    @Override // defpackage.eg4
    public void c(String str, double d2) {
        this.a.d();
        ww3 a2 = this.d.a();
        a2.Y(1, d2);
        if (str == null) {
            a2.Y0(2);
        } else {
            a2.L(2, str);
        }
        this.a.e();
        try {
            a2.T();
            this.a.E();
        } finally {
            this.a.j();
            this.d.f(a2);
        }
    }

    @Override // defpackage.eg4
    public LiveData<RoomToken> d(String str) {
        k93 c2 = k93.c("SELECT * FROM table_token WHERE symbolWithType=?", 1);
        if (str == null) {
            c2.Y0(1);
        } else {
            c2.L(1, str);
        }
        return this.a.n().e(new String[]{"table_token"}, false, new g(c2));
    }

    @Override // defpackage.eg4
    public Object e(String str, q70<? super RoomToken> q70Var) {
        k93 c2 = k93.c("SELECT * FROM table_token WHERE symbolWithType=?", 1);
        if (str == null) {
            c2.Y0(1);
        } else {
            c2.L(1, str);
        }
        return CoroutinesRoom.a(this.a, false, id0.a(), new e(c2), q70Var);
    }

    @Override // defpackage.eg4
    public void f(String str, double d2, double d3) {
        this.a.d();
        ww3 a2 = this.e.a();
        a2.Y(1, d2);
        a2.Y(2, d3);
        if (str == null) {
            a2.Y0(3);
        } else {
            a2.L(3, str);
        }
        this.a.e();
        try {
            a2.T();
            this.a.E();
        } finally {
            this.a.j();
            this.e.f(a2);
        }
    }

    @Override // defpackage.eg4
    public Object g(int i2, q70<? super List<RoomToken>> q70Var) {
        k93 c2 = k93.c("SELECT * FROM table_token WHERE tokenTypeChain LIKE ? ORDER BY `order` ASC", 1);
        c2.q0(1, i2);
        return CoroutinesRoom.a(this.a, false, id0.a(), new b(c2), q70Var);
    }

    @Override // defpackage.eg4
    public Object h(int i2, String str, q70<? super RoomToken> q70Var) {
        k93 c2 = k93.c("SELECT * FROM table_token WHERE tokenTypeChain=? AND contractorAddress=? COLLATE NOCASE", 2);
        c2.q0(1, i2);
        if (str == null) {
            c2.Y0(2);
        } else {
            c2.L(2, str);
        }
        return CoroutinesRoom.a(this.a, false, id0.a(), new f(c2), q70Var);
    }

    @Override // defpackage.eg4
    public Object i(String str, int i2, q70<? super te4> q70Var) {
        return CoroutinesRoom.b(this.a, true, new o(i2, str), q70Var);
    }

    @Override // defpackage.eg4
    public boolean j(String str, int i2) {
        k93 c2 = k93.c("SELECT EXISTS (SELECT 1 FROM table_token WHERE symbolWithType = ? AND tokenTypeChain =?)", 2);
        if (str == null) {
            c2.Y0(1);
        } else {
            c2.L(1, str);
        }
        c2.q0(2, i2);
        this.a.d();
        boolean z = false;
        Cursor c3 = id0.c(this.a, c2, false, null);
        try {
            if (c3.moveToFirst()) {
                z = c3.getInt(0) != 0;
            }
            return z;
        } finally {
            c3.close();
            c2.f();
        }
    }

    @Override // defpackage.eg4
    public void k(RoomToken roomToken) {
        this.a.d();
        this.a.e();
        try {
            this.c.h(roomToken);
            this.a.E();
        } finally {
            this.a.j();
        }
    }

    @Override // defpackage.eg4
    public Object l(q70<? super List<RoomToken>> q70Var) {
        k93 c2 = k93.c("SELECT * FROM table_token ORDER BY `order` ASC", 0);
        return CoroutinesRoom.a(this.a, false, id0.a(), new d(c2), q70Var);
    }

    @Override // defpackage.eg4
    public void m(RoomToken roomToken) {
        this.a.d();
        this.a.e();
        try {
            this.b.h(roomToken);
            this.a.E();
        } finally {
            this.a.j();
        }
    }

    @Override // defpackage.eg4
    public LiveData<List<RoomToken>> n(int i2) {
        k93 c2 = k93.c("SELECT * FROM table_token WHERE tokenTypeChain LIKE ? ORDER BY `order` ASC", 1);
        c2.q0(1, i2);
        return this.a.n().e(new String[]{"table_token"}, false, new a(c2));
    }
}
