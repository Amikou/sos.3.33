package net.safemoon.androidwallet.database.room;

import android.database.Cursor;
import androidx.lifecycle.LiveData;
import androidx.room.CoroutinesRoom;
import androidx.room.RoomDatabase;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.concurrent.Callable;
import net.safemoon.androidwallet.model.RoomCoinPriceAlert;
import org.web3j.ens.contracts.generated.PublicResolver;

/* compiled from: CoinPriceAlertDao_Impl.java */
/* loaded from: classes2.dex */
public final class a implements l00 {
    public final RoomDatabase a;
    public final zv0<RoomCoinPriceAlert> b;
    public final co3 c;

    /* compiled from: CoinPriceAlertDao_Impl.java */
    /* renamed from: net.safemoon.androidwallet.database.room.a$a  reason: collision with other inner class name */
    /* loaded from: classes2.dex */
    public class C0203a extends zv0<RoomCoinPriceAlert> {
        public C0203a(a aVar, RoomDatabase roomDatabase) {
            super(roomDatabase);
        }

        @Override // defpackage.co3
        public String d() {
            return "INSERT OR REPLACE INTO `table_room_coin_price_alert` (`id`,`symbol`,`name`,`slug`,`cmcData`) VALUES (?,?,?,?,?)";
        }

        @Override // defpackage.zv0
        /* renamed from: k */
        public void g(ww3 ww3Var, RoomCoinPriceAlert roomCoinPriceAlert) {
            ww3Var.q0(1, roomCoinPriceAlert.getCmcId());
            if (roomCoinPriceAlert.getSymbol() == null) {
                ww3Var.Y0(2);
            } else {
                ww3Var.L(2, roomCoinPriceAlert.getSymbol());
            }
            if (roomCoinPriceAlert.getName() == null) {
                ww3Var.Y0(3);
            } else {
                ww3Var.L(3, roomCoinPriceAlert.getName());
            }
            if (roomCoinPriceAlert.getSlug() == null) {
                ww3Var.Y0(4);
            } else {
                ww3Var.L(4, roomCoinPriceAlert.getSlug());
            }
            if (roomCoinPriceAlert.getCmcData() == null) {
                ww3Var.Y0(5);
            } else {
                ww3Var.L(5, roomCoinPriceAlert.getCmcData());
            }
        }
    }

    /* compiled from: CoinPriceAlertDao_Impl.java */
    /* loaded from: classes2.dex */
    public class b extends yv0<RoomCoinPriceAlert> {
        public b(a aVar, RoomDatabase roomDatabase) {
            super(roomDatabase);
        }

        @Override // defpackage.co3
        public String d() {
            return "DELETE FROM `table_room_coin_price_alert` WHERE `id` = ?";
        }

        @Override // defpackage.yv0
        /* renamed from: i */
        public void g(ww3 ww3Var, RoomCoinPriceAlert roomCoinPriceAlert) {
            ww3Var.q0(1, roomCoinPriceAlert.getCmcId());
        }
    }

    /* compiled from: CoinPriceAlertDao_Impl.java */
    /* loaded from: classes2.dex */
    public class c extends co3 {
        public c(a aVar, RoomDatabase roomDatabase) {
            super(roomDatabase);
        }

        @Override // defpackage.co3
        public String d() {
            return "DELETE FROM table_room_coin_price_alert where id=?";
        }
    }

    /* compiled from: CoinPriceAlertDao_Impl.java */
    /* loaded from: classes2.dex */
    public class d implements Callable<te4> {
        public final /* synthetic */ RoomCoinPriceAlert a;

        public d(RoomCoinPriceAlert roomCoinPriceAlert) {
            this.a = roomCoinPriceAlert;
        }

        @Override // java.util.concurrent.Callable
        /* renamed from: a */
        public te4 call() throws Exception {
            a.this.a.e();
            try {
                a.this.b.h(this.a);
                a.this.a.E();
                return te4.a;
            } finally {
                a.this.a.j();
            }
        }
    }

    /* compiled from: CoinPriceAlertDao_Impl.java */
    /* loaded from: classes2.dex */
    public class e implements Callable<te4> {
        public final /* synthetic */ int a;

        public e(int i) {
            this.a = i;
        }

        @Override // java.util.concurrent.Callable
        /* renamed from: a */
        public te4 call() throws Exception {
            ww3 a = a.this.c.a();
            a.q0(1, this.a);
            a.this.a.e();
            try {
                a.T();
                a.this.a.E();
                return te4.a;
            } finally {
                a.this.a.j();
                a.this.c.f(a);
            }
        }
    }

    /* compiled from: CoinPriceAlertDao_Impl.java */
    /* loaded from: classes2.dex */
    public class f implements Callable<List<RoomCoinPriceAlert>> {
        public final /* synthetic */ k93 a;

        public f(k93 k93Var) {
            this.a = k93Var;
        }

        @Override // java.util.concurrent.Callable
        /* renamed from: a */
        public List<RoomCoinPriceAlert> call() throws Exception {
            Cursor c = id0.c(a.this.a, this.a, false, null);
            try {
                int e = wb0.e(c, "id");
                int e2 = wb0.e(c, "symbol");
                int e3 = wb0.e(c, PublicResolver.FUNC_NAME);
                int e4 = wb0.e(c, "slug");
                int e5 = wb0.e(c, "cmcData");
                ArrayList arrayList = new ArrayList(c.getCount());
                while (c.moveToNext()) {
                    arrayList.add(new RoomCoinPriceAlert(c.getInt(e), c.isNull(e2) ? null : c.getString(e2), c.isNull(e3) ? null : c.getString(e3), c.isNull(e4) ? null : c.getString(e4), c.isNull(e5) ? null : c.getString(e5)));
                }
                return arrayList;
            } finally {
                c.close();
            }
        }

        public void finalize() {
            this.a.f();
        }
    }

    /* compiled from: CoinPriceAlertDao_Impl.java */
    /* loaded from: classes2.dex */
    public class g implements Callable<List<RoomCoinPriceAlert>> {
        public final /* synthetic */ k93 a;

        public g(k93 k93Var) {
            this.a = k93Var;
        }

        @Override // java.util.concurrent.Callable
        /* renamed from: a */
        public List<RoomCoinPriceAlert> call() throws Exception {
            Cursor c = id0.c(a.this.a, this.a, false, null);
            try {
                int e = wb0.e(c, "id");
                int e2 = wb0.e(c, "symbol");
                int e3 = wb0.e(c, PublicResolver.FUNC_NAME);
                int e4 = wb0.e(c, "slug");
                int e5 = wb0.e(c, "cmcData");
                ArrayList arrayList = new ArrayList(c.getCount());
                while (c.moveToNext()) {
                    arrayList.add(new RoomCoinPriceAlert(c.getInt(e), c.isNull(e2) ? null : c.getString(e2), c.isNull(e3) ? null : c.getString(e3), c.isNull(e4) ? null : c.getString(e4), c.isNull(e5) ? null : c.getString(e5)));
                }
                return arrayList;
            } finally {
                c.close();
                this.a.f();
            }
        }
    }

    public a(RoomDatabase roomDatabase) {
        this.a = roomDatabase;
        this.b = new C0203a(this, roomDatabase);
        new b(this, roomDatabase);
        this.c = new c(this, roomDatabase);
    }

    public static List<Class<?>> h() {
        return Collections.emptyList();
    }

    @Override // defpackage.l00
    public LiveData<List<RoomCoinPriceAlert>> a() {
        return this.a.n().e(new String[]{"table_room_coin_price_alert"}, false, new f(k93.c("SELECT * FROM table_room_coin_price_alert", 0)));
    }

    @Override // defpackage.l00
    public Object b(q70<? super List<RoomCoinPriceAlert>> q70Var) {
        k93 c2 = k93.c("SELECT * FROM table_room_coin_price_alert", 0);
        return CoroutinesRoom.a(this.a, false, id0.a(), new g(c2), q70Var);
    }

    @Override // defpackage.l00
    public Object c(int i, q70<? super te4> q70Var) {
        return CoroutinesRoom.b(this.a, true, new e(i), q70Var);
    }

    @Override // defpackage.l00
    public Object d(RoomCoinPriceAlert roomCoinPriceAlert, q70<? super te4> q70Var) {
        return CoroutinesRoom.b(this.a, true, new d(roomCoinPriceAlert), q70Var);
    }
}
