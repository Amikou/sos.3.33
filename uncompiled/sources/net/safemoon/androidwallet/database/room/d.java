package net.safemoon.androidwallet.database.room;

import android.database.Cursor;
import androidx.lifecycle.LiveData;
import androidx.room.RoomDatabase;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.concurrent.Callable;
import net.safemoon.androidwallet.model.fiat.room.RoomFiat;
import org.web3j.ens.contracts.generated.PublicResolver;

/* compiled from: FiatListDao_Impl.java */
/* loaded from: classes2.dex */
public final class d implements a31 {
    public final RoomDatabase a;
    public final zv0<RoomFiat> b;
    public final co3 c;
    public final co3 d;

    /* compiled from: FiatListDao_Impl.java */
    /* loaded from: classes2.dex */
    public class a extends zv0<RoomFiat> {
        public a(d dVar, RoomDatabase roomDatabase) {
            super(roomDatabase);
        }

        @Override // defpackage.co3
        public String d() {
            return "INSERT OR IGNORE INTO `table_fiat` (`symbol`,`name`,`rate`) VALUES (?,?,?)";
        }

        @Override // defpackage.zv0
        /* renamed from: k */
        public void g(ww3 ww3Var, RoomFiat roomFiat) {
            if (roomFiat.getSymbol() == null) {
                ww3Var.Y0(1);
            } else {
                ww3Var.L(1, roomFiat.getSymbol());
            }
            if (roomFiat.getName() == null) {
                ww3Var.Y0(2);
            } else {
                ww3Var.L(2, roomFiat.getName());
            }
            if (roomFiat.getRate() == null) {
                ww3Var.Y0(3);
            } else {
                ww3Var.Y(3, roomFiat.getRate().doubleValue());
            }
        }
    }

    /* compiled from: FiatListDao_Impl.java */
    /* loaded from: classes2.dex */
    public class b extends co3 {
        public b(d dVar, RoomDatabase roomDatabase) {
            super(roomDatabase);
        }

        @Override // defpackage.co3
        public String d() {
            return "UPDATE table_fiat SET rate = ? WHERE symbol=?";
        }
    }

    /* compiled from: FiatListDao_Impl.java */
    /* loaded from: classes2.dex */
    public class c extends co3 {
        public c(d dVar, RoomDatabase roomDatabase) {
            super(roomDatabase);
        }

        @Override // defpackage.co3
        public String d() {
            return "UPDATE table_fiat SET name = ? WHERE symbol=?";
        }
    }

    /* compiled from: FiatListDao_Impl.java */
    /* renamed from: net.safemoon.androidwallet.database.room.d$d  reason: collision with other inner class name */
    /* loaded from: classes2.dex */
    public class CallableC0206d implements Callable<List<RoomFiat>> {
        public final /* synthetic */ k93 a;

        public CallableC0206d(k93 k93Var) {
            this.a = k93Var;
        }

        @Override // java.util.concurrent.Callable
        /* renamed from: a */
        public List<RoomFiat> call() throws Exception {
            Cursor c = id0.c(d.this.a, this.a, false, null);
            try {
                int e = wb0.e(c, "symbol");
                int e2 = wb0.e(c, PublicResolver.FUNC_NAME);
                int e3 = wb0.e(c, "rate");
                ArrayList arrayList = new ArrayList(c.getCount());
                while (c.moveToNext()) {
                    arrayList.add(new RoomFiat(c.isNull(e) ? null : c.getString(e), c.isNull(e2) ? null : c.getString(e2), c.isNull(e3) ? null : Double.valueOf(c.getDouble(e3))));
                }
                return arrayList;
            } finally {
                c.close();
            }
        }

        public void finalize() {
            this.a.f();
        }
    }

    public d(RoomDatabase roomDatabase) {
        this.a = roomDatabase;
        this.b = new a(this, roomDatabase);
        this.c = new b(this, roomDatabase);
        this.d = new c(this, roomDatabase);
    }

    public static List<Class<?>> g() {
        return Collections.emptyList();
    }

    @Override // defpackage.a31
    public LiveData<List<RoomFiat>> a() {
        return this.a.n().e(new String[]{"table_fiat"}, false, new CallableC0206d(k93.c("SELECT * FROM table_fiat ", 0)));
    }

    @Override // defpackage.a31
    public void b(String str, double d) {
        this.a.d();
        ww3 a2 = this.c.a();
        a2.Y(1, d);
        if (str == null) {
            a2.Y0(2);
        } else {
            a2.L(2, str);
        }
        this.a.e();
        try {
            a2.T();
            this.a.E();
        } finally {
            this.a.j();
            this.c.f(a2);
        }
    }

    @Override // defpackage.a31
    public void c(RoomFiat... roomFiatArr) {
        this.a.d();
        this.a.e();
        try {
            this.b.i(roomFiatArr);
            this.a.E();
        } finally {
            this.a.j();
        }
    }

    @Override // defpackage.a31
    public void d(String str, String str2) {
        this.a.d();
        ww3 a2 = this.d.a();
        if (str2 == null) {
            a2.Y0(1);
        } else {
            a2.L(1, str2);
        }
        if (str == null) {
            a2.Y0(2);
        } else {
            a2.L(2, str);
        }
        this.a.e();
        try {
            a2.T();
            this.a.E();
        } finally {
            this.a.j();
            this.d.f(a2);
        }
    }

    @Override // defpackage.a31
    public boolean e(String str) {
        k93 c2 = k93.c("SELECT EXISTS (SELECT 1 FROM table_fiat WHERE symbol = ?)", 1);
        if (str == null) {
            c2.Y0(1);
        } else {
            c2.L(1, str);
        }
        this.a.d();
        boolean z = false;
        Cursor c3 = id0.c(this.a, c2, false, null);
        try {
            if (c3.moveToFirst()) {
                z = c3.getInt(0) != 0;
            }
            return z;
        } finally {
            c3.close();
            c2.f();
        }
    }
}
