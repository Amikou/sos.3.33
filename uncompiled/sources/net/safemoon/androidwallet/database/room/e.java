package net.safemoon.androidwallet.database.room;

import android.database.Cursor;
import androidx.lifecycle.LiveData;
import androidx.room.CoroutinesRoom;
import androidx.room.RoomDatabase;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.concurrent.Callable;
import net.safemoon.androidwallet.model.collectible.RoomNFT;
import org.web3j.ens.contracts.generated.PublicResolver;

/* compiled from: NftDao_Impl.java */
/* loaded from: classes2.dex */
public final class e implements of2 {
    public final RoomDatabase a;
    public final zv0<RoomNFT> b;
    public final yv0<RoomNFT> c;
    public final co3 d;
    public final co3 e;

    /* compiled from: NftDao_Impl.java */
    /* loaded from: classes2.dex */
    public class a implements Callable<List<RoomNFT>> {
        public final /* synthetic */ k93 a;

        public a(k93 k93Var) {
            this.a = k93Var;
        }

        @Override // java.util.concurrent.Callable
        /* renamed from: a */
        public List<RoomNFT> call() throws Exception {
            a aVar;
            int e;
            int e2;
            int e3;
            int e4;
            int e5;
            int e6;
            int e7;
            int e8;
            int e9;
            int e10;
            int e11;
            int e12;
            int e13;
            int e14;
            String string;
            int i;
            Cursor c = id0.c(e.this.a, this.a, false, null);
            try {
                e = wb0.e(c, "_id");
                e2 = wb0.e(c, "chain");
                e3 = wb0.e(c, "collectionId");
                e4 = wb0.e(c, "token_id");
                e5 = wb0.e(c, PublicResolver.FUNC_NAME);
                e6 = wb0.e(c, "description");
                e7 = wb0.e(c, "animation_url");
                e8 = wb0.e(c, "image_preview_url");
                e9 = wb0.e(c, "image_data");
                e10 = wb0.e(c, "token_uri");
                e11 = wb0.e(c, "attributes");
                e12 = wb0.e(c, "fullData");
                e13 = wb0.e(c, "permalink");
                e14 = wb0.e(c, "openSeaUrl");
            } catch (Throwable th) {
                th = th;
                aVar = this;
            }
            try {
                int e15 = wb0.e(c, "updatedTime");
                int e16 = wb0.e(c, "order");
                int i2 = e14;
                ArrayList arrayList = new ArrayList(c.getCount());
                while (c.moveToNext()) {
                    Long valueOf = c.isNull(e) ? null : Long.valueOf(c.getLong(e));
                    int i3 = c.getInt(e2);
                    long j = c.getLong(e3);
                    String string2 = c.isNull(e4) ? null : c.getString(e4);
                    String string3 = c.isNull(e5) ? null : c.getString(e5);
                    String string4 = c.isNull(e6) ? null : c.getString(e6);
                    String string5 = c.isNull(e7) ? null : c.getString(e7);
                    String string6 = c.isNull(e8) ? null : c.getString(e8);
                    String string7 = c.isNull(e9) ? null : c.getString(e9);
                    String string8 = c.isNull(e10) ? null : c.getString(e10);
                    String string9 = c.isNull(e11) ? null : c.getString(e11);
                    String string10 = c.isNull(e12) ? null : c.getString(e12);
                    if (c.isNull(e13)) {
                        i = i2;
                        string = null;
                    } else {
                        string = c.getString(e13);
                        i = i2;
                    }
                    int i4 = e15;
                    int i5 = e;
                    int i6 = e16;
                    e16 = i6;
                    arrayList.add(new RoomNFT(valueOf, i3, j, string2, string3, string4, string5, string6, string7, string8, string9, string10, string, c.isNull(i) ? null : c.getString(i), c.getLong(i4), c.getInt(i6)));
                    e = i5;
                    e15 = i4;
                    i2 = i;
                }
                c.close();
                this.a.f();
                return arrayList;
            } catch (Throwable th2) {
                th = th2;
                aVar = this;
                c.close();
                aVar.a.f();
                throw th;
            }
        }
    }

    /* compiled from: NftDao_Impl.java */
    /* loaded from: classes2.dex */
    public class b implements Callable<List<RoomNFT>> {
        public final /* synthetic */ k93 a;

        public b(k93 k93Var) {
            this.a = k93Var;
        }

        @Override // java.util.concurrent.Callable
        /* renamed from: a */
        public List<RoomNFT> call() throws Exception {
            b bVar;
            int e;
            int e2;
            int e3;
            int e4;
            int e5;
            int e6;
            int e7;
            int e8;
            int e9;
            int e10;
            int e11;
            int e12;
            int e13;
            int e14;
            String string;
            int i;
            Cursor c = id0.c(e.this.a, this.a, false, null);
            try {
                e = wb0.e(c, "_id");
                e2 = wb0.e(c, "chain");
                e3 = wb0.e(c, "collectionId");
                e4 = wb0.e(c, "token_id");
                e5 = wb0.e(c, PublicResolver.FUNC_NAME);
                e6 = wb0.e(c, "description");
                e7 = wb0.e(c, "animation_url");
                e8 = wb0.e(c, "image_preview_url");
                e9 = wb0.e(c, "image_data");
                e10 = wb0.e(c, "token_uri");
                e11 = wb0.e(c, "attributes");
                e12 = wb0.e(c, "fullData");
                e13 = wb0.e(c, "permalink");
                e14 = wb0.e(c, "openSeaUrl");
            } catch (Throwable th) {
                th = th;
                bVar = this;
            }
            try {
                int e15 = wb0.e(c, "updatedTime");
                int e16 = wb0.e(c, "order");
                int i2 = e14;
                ArrayList arrayList = new ArrayList(c.getCount());
                while (c.moveToNext()) {
                    Long valueOf = c.isNull(e) ? null : Long.valueOf(c.getLong(e));
                    int i3 = c.getInt(e2);
                    long j = c.getLong(e3);
                    String string2 = c.isNull(e4) ? null : c.getString(e4);
                    String string3 = c.isNull(e5) ? null : c.getString(e5);
                    String string4 = c.isNull(e6) ? null : c.getString(e6);
                    String string5 = c.isNull(e7) ? null : c.getString(e7);
                    String string6 = c.isNull(e8) ? null : c.getString(e8);
                    String string7 = c.isNull(e9) ? null : c.getString(e9);
                    String string8 = c.isNull(e10) ? null : c.getString(e10);
                    String string9 = c.isNull(e11) ? null : c.getString(e11);
                    String string10 = c.isNull(e12) ? null : c.getString(e12);
                    if (c.isNull(e13)) {
                        i = i2;
                        string = null;
                    } else {
                        string = c.getString(e13);
                        i = i2;
                    }
                    int i4 = e15;
                    int i5 = e;
                    int i6 = e16;
                    e16 = i6;
                    arrayList.add(new RoomNFT(valueOf, i3, j, string2, string3, string4, string5, string6, string7, string8, string9, string10, string, c.isNull(i) ? null : c.getString(i), c.getLong(i4), c.getInt(i6)));
                    e = i5;
                    e15 = i4;
                    i2 = i;
                }
                c.close();
                this.a.f();
                return arrayList;
            } catch (Throwable th2) {
                th = th2;
                bVar = this;
                c.close();
                bVar.a.f();
                throw th;
            }
        }
    }

    /* compiled from: NftDao_Impl.java */
    /* loaded from: classes2.dex */
    public class c implements Callable<RoomNFT> {
        public final /* synthetic */ k93 a;

        public c(k93 k93Var) {
            this.a = k93Var;
        }

        @Override // java.util.concurrent.Callable
        /* renamed from: a */
        public RoomNFT call() throws Exception {
            int e;
            int e2;
            int e3;
            int e4;
            int e5;
            int e6;
            int e7;
            int e8;
            int e9;
            int e10;
            int e11;
            int e12;
            int e13;
            int e14;
            RoomNFT roomNFT;
            String string;
            int i;
            c cVar = this;
            Cursor c = id0.c(e.this.a, cVar.a, false, null);
            try {
                e = wb0.e(c, "_id");
                e2 = wb0.e(c, "chain");
                e3 = wb0.e(c, "collectionId");
                e4 = wb0.e(c, "token_id");
                e5 = wb0.e(c, PublicResolver.FUNC_NAME);
                e6 = wb0.e(c, "description");
                e7 = wb0.e(c, "animation_url");
                e8 = wb0.e(c, "image_preview_url");
                e9 = wb0.e(c, "image_data");
                e10 = wb0.e(c, "token_uri");
                e11 = wb0.e(c, "attributes");
                e12 = wb0.e(c, "fullData");
                e13 = wb0.e(c, "permalink");
                e14 = wb0.e(c, "openSeaUrl");
            } catch (Throwable th) {
                th = th;
            }
            try {
                int e15 = wb0.e(c, "updatedTime");
                int e16 = wb0.e(c, "order");
                if (c.moveToFirst()) {
                    Long valueOf = c.isNull(e) ? null : Long.valueOf(c.getLong(e));
                    int i2 = c.getInt(e2);
                    long j = c.getLong(e3);
                    String string2 = c.isNull(e4) ? null : c.getString(e4);
                    String string3 = c.isNull(e5) ? null : c.getString(e5);
                    String string4 = c.isNull(e6) ? null : c.getString(e6);
                    String string5 = c.isNull(e7) ? null : c.getString(e7);
                    String string6 = c.isNull(e8) ? null : c.getString(e8);
                    String string7 = c.isNull(e9) ? null : c.getString(e9);
                    String string8 = c.isNull(e10) ? null : c.getString(e10);
                    String string9 = c.isNull(e11) ? null : c.getString(e11);
                    String string10 = c.isNull(e12) ? null : c.getString(e12);
                    String string11 = c.isNull(e13) ? null : c.getString(e13);
                    if (c.isNull(e14)) {
                        i = e15;
                        string = null;
                    } else {
                        string = c.getString(e14);
                        i = e15;
                    }
                    roomNFT = new RoomNFT(valueOf, i2, j, string2, string3, string4, string5, string6, string7, string8, string9, string10, string11, string, c.getLong(i), c.getInt(e16));
                } else {
                    roomNFT = null;
                }
                c.close();
                this.a.f();
                return roomNFT;
            } catch (Throwable th2) {
                th = th2;
                cVar = this;
                c.close();
                cVar.a.f();
                throw th;
            }
        }
    }

    /* compiled from: NftDao_Impl.java */
    /* loaded from: classes2.dex */
    public class d implements Callable<RoomNFT> {
        public final /* synthetic */ k93 a;

        public d(k93 k93Var) {
            this.a = k93Var;
        }

        @Override // java.util.concurrent.Callable
        /* renamed from: a */
        public RoomNFT call() throws Exception {
            int e;
            int e2;
            int e3;
            int e4;
            int e5;
            int e6;
            int e7;
            int e8;
            int e9;
            int e10;
            int e11;
            int e12;
            int e13;
            int e14;
            RoomNFT roomNFT;
            String string;
            int i;
            d dVar = this;
            Cursor c = id0.c(e.this.a, dVar.a, false, null);
            try {
                e = wb0.e(c, "_id");
                e2 = wb0.e(c, "chain");
                e3 = wb0.e(c, "collectionId");
                e4 = wb0.e(c, "token_id");
                e5 = wb0.e(c, PublicResolver.FUNC_NAME);
                e6 = wb0.e(c, "description");
                e7 = wb0.e(c, "animation_url");
                e8 = wb0.e(c, "image_preview_url");
                e9 = wb0.e(c, "image_data");
                e10 = wb0.e(c, "token_uri");
                e11 = wb0.e(c, "attributes");
                e12 = wb0.e(c, "fullData");
                e13 = wb0.e(c, "permalink");
                e14 = wb0.e(c, "openSeaUrl");
            } catch (Throwable th) {
                th = th;
            }
            try {
                int e15 = wb0.e(c, "updatedTime");
                int e16 = wb0.e(c, "order");
                if (c.moveToFirst()) {
                    Long valueOf = c.isNull(e) ? null : Long.valueOf(c.getLong(e));
                    int i2 = c.getInt(e2);
                    long j = c.getLong(e3);
                    String string2 = c.isNull(e4) ? null : c.getString(e4);
                    String string3 = c.isNull(e5) ? null : c.getString(e5);
                    String string4 = c.isNull(e6) ? null : c.getString(e6);
                    String string5 = c.isNull(e7) ? null : c.getString(e7);
                    String string6 = c.isNull(e8) ? null : c.getString(e8);
                    String string7 = c.isNull(e9) ? null : c.getString(e9);
                    String string8 = c.isNull(e10) ? null : c.getString(e10);
                    String string9 = c.isNull(e11) ? null : c.getString(e11);
                    String string10 = c.isNull(e12) ? null : c.getString(e12);
                    String string11 = c.isNull(e13) ? null : c.getString(e13);
                    if (c.isNull(e14)) {
                        i = e15;
                        string = null;
                    } else {
                        string = c.getString(e14);
                        i = e15;
                    }
                    roomNFT = new RoomNFT(valueOf, i2, j, string2, string3, string4, string5, string6, string7, string8, string9, string10, string11, string, c.getLong(i), c.getInt(e16));
                } else {
                    roomNFT = null;
                }
                c.close();
                this.a.f();
                return roomNFT;
            } catch (Throwable th2) {
                th = th2;
                dVar = this;
                c.close();
                dVar.a.f();
                throw th;
            }
        }
    }

    /* compiled from: NftDao_Impl.java */
    /* renamed from: net.safemoon.androidwallet.database.room.e$e  reason: collision with other inner class name */
    /* loaded from: classes2.dex */
    public class CallableC0207e implements Callable<Boolean> {
        public final /* synthetic */ k93 a;

        public CallableC0207e(k93 k93Var) {
            this.a = k93Var;
        }

        @Override // java.util.concurrent.Callable
        /* renamed from: a */
        public Boolean call() throws Exception {
            Boolean bool = null;
            Cursor c = id0.c(e.this.a, this.a, false, null);
            try {
                if (c.moveToFirst()) {
                    Integer valueOf = c.isNull(0) ? null : Integer.valueOf(c.getInt(0));
                    if (valueOf != null) {
                        bool = Boolean.valueOf(valueOf.intValue() != 0);
                    }
                }
                return bool;
            } finally {
                c.close();
                this.a.f();
            }
        }
    }

    /* compiled from: NftDao_Impl.java */
    /* loaded from: classes2.dex */
    public class f extends zv0<RoomNFT> {
        public f(e eVar, RoomDatabase roomDatabase) {
            super(roomDatabase);
        }

        @Override // defpackage.co3
        public String d() {
            return "INSERT OR REPLACE INTO `table_room_nft` (`_id`,`chain`,`collectionId`,`token_id`,`name`,`description`,`animation_url`,`image_preview_url`,`image_data`,`token_uri`,`attributes`,`fullData`,`permalink`,`openSeaUrl`,`updatedTime`,`order`) VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
        }

        @Override // defpackage.zv0
        /* renamed from: k */
        public void g(ww3 ww3Var, RoomNFT roomNFT) {
            if (roomNFT.getId() == null) {
                ww3Var.Y0(1);
            } else {
                ww3Var.q0(1, roomNFT.getId().longValue());
            }
            ww3Var.q0(2, roomNFT.getChain());
            ww3Var.q0(3, roomNFT.getCollectionId());
            if (roomNFT.getToken_id() == null) {
                ww3Var.Y0(4);
            } else {
                ww3Var.L(4, roomNFT.getToken_id());
            }
            if (roomNFT.getName() == null) {
                ww3Var.Y0(5);
            } else {
                ww3Var.L(5, roomNFT.getName());
            }
            if (roomNFT.getDescription() == null) {
                ww3Var.Y0(6);
            } else {
                ww3Var.L(6, roomNFT.getDescription());
            }
            if (roomNFT.getAnimation_url() == null) {
                ww3Var.Y0(7);
            } else {
                ww3Var.L(7, roomNFT.getAnimation_url());
            }
            if (roomNFT.getImage_preview_url() == null) {
                ww3Var.Y0(8);
            } else {
                ww3Var.L(8, roomNFT.getImage_preview_url());
            }
            if (roomNFT.getImage_data() == null) {
                ww3Var.Y0(9);
            } else {
                ww3Var.L(9, roomNFT.getImage_data());
            }
            if (roomNFT.getToken_uri() == null) {
                ww3Var.Y0(10);
            } else {
                ww3Var.L(10, roomNFT.getToken_uri());
            }
            if (roomNFT.getAttributes() == null) {
                ww3Var.Y0(11);
            } else {
                ww3Var.L(11, roomNFT.getAttributes());
            }
            if (roomNFT.getFullData() == null) {
                ww3Var.Y0(12);
            } else {
                ww3Var.L(12, roomNFT.getFullData());
            }
            if (roomNFT.getPermalink() == null) {
                ww3Var.Y0(13);
            } else {
                ww3Var.L(13, roomNFT.getPermalink());
            }
            if (roomNFT.getOpenSeaUrl() == null) {
                ww3Var.Y0(14);
            } else {
                ww3Var.L(14, roomNFT.getOpenSeaUrl());
            }
            ww3Var.q0(15, roomNFT.getUpdatedTime());
            ww3Var.q0(16, roomNFT.getOrder());
        }
    }

    /* compiled from: NftDao_Impl.java */
    /* loaded from: classes2.dex */
    public class g extends yv0<RoomNFT> {
        public g(e eVar, RoomDatabase roomDatabase) {
            super(roomDatabase);
        }

        @Override // defpackage.co3
        public String d() {
            return "DELETE FROM `table_room_nft` WHERE `_id` = ?";
        }

        @Override // defpackage.yv0
        /* renamed from: i */
        public void g(ww3 ww3Var, RoomNFT roomNFT) {
            if (roomNFT.getId() == null) {
                ww3Var.Y0(1);
            } else {
                ww3Var.q0(1, roomNFT.getId().longValue());
            }
        }
    }

    /* compiled from: NftDao_Impl.java */
    /* loaded from: classes2.dex */
    public class h extends co3 {
        public h(e eVar, RoomDatabase roomDatabase) {
            super(roomDatabase);
        }

        @Override // defpackage.co3
        public String d() {
            return "UPDATE table_room_nft SET `order` = ? WHERE _id=?";
        }
    }

    /* compiled from: NftDao_Impl.java */
    /* loaded from: classes2.dex */
    public class i extends co3 {
        public i(e eVar, RoomDatabase roomDatabase) {
            super(roomDatabase);
        }

        @Override // defpackage.co3
        public String d() {
            return "DELETE FROM table_room_nft WHERE collectionId=?";
        }
    }

    /* compiled from: NftDao_Impl.java */
    /* loaded from: classes2.dex */
    public class j implements Callable<Long> {
        public final /* synthetic */ RoomNFT a;

        public j(RoomNFT roomNFT) {
            this.a = roomNFT;
        }

        @Override // java.util.concurrent.Callable
        /* renamed from: a */
        public Long call() throws Exception {
            e.this.a.e();
            try {
                long j = e.this.b.j(this.a);
                e.this.a.E();
                return Long.valueOf(j);
            } finally {
                e.this.a.j();
            }
        }
    }

    /* compiled from: NftDao_Impl.java */
    /* loaded from: classes2.dex */
    public class k implements Callable<te4> {
        public final /* synthetic */ RoomNFT a;

        public k(RoomNFT roomNFT) {
            this.a = roomNFT;
        }

        @Override // java.util.concurrent.Callable
        /* renamed from: a */
        public te4 call() throws Exception {
            e.this.a.e();
            try {
                e.this.c.h(this.a);
                e.this.a.E();
                return te4.a;
            } finally {
                e.this.a.j();
            }
        }
    }

    /* compiled from: NftDao_Impl.java */
    /* loaded from: classes2.dex */
    public class l implements Callable<te4> {
        public final /* synthetic */ int a;
        public final /* synthetic */ long b;

        public l(int i, long j) {
            this.a = i;
            this.b = j;
        }

        @Override // java.util.concurrent.Callable
        /* renamed from: a */
        public te4 call() throws Exception {
            ww3 a = e.this.d.a();
            a.q0(1, this.a);
            a.q0(2, this.b);
            e.this.a.e();
            try {
                a.T();
                e.this.a.E();
                return te4.a;
            } finally {
                e.this.a.j();
                e.this.d.f(a);
            }
        }
    }

    /* compiled from: NftDao_Impl.java */
    /* loaded from: classes2.dex */
    public class m implements Callable<te4> {
        public final /* synthetic */ long a;

        public m(long j) {
            this.a = j;
        }

        @Override // java.util.concurrent.Callable
        /* renamed from: a */
        public te4 call() throws Exception {
            ww3 a = e.this.e.a();
            a.q0(1, this.a);
            e.this.a.e();
            try {
                a.T();
                e.this.a.E();
                return te4.a;
            } finally {
                e.this.a.j();
                e.this.e.f(a);
            }
        }
    }

    /* compiled from: NftDao_Impl.java */
    /* loaded from: classes2.dex */
    public class n implements Callable<List<RoomNFT>> {
        public final /* synthetic */ k93 a;

        public n(k93 k93Var) {
            this.a = k93Var;
        }

        @Override // java.util.concurrent.Callable
        /* renamed from: a */
        public List<RoomNFT> call() throws Exception {
            String string;
            int i;
            Cursor c = id0.c(e.this.a, this.a, false, null);
            try {
                int e = wb0.e(c, "_id");
                int e2 = wb0.e(c, "chain");
                int e3 = wb0.e(c, "collectionId");
                int e4 = wb0.e(c, "token_id");
                int e5 = wb0.e(c, PublicResolver.FUNC_NAME);
                int e6 = wb0.e(c, "description");
                int e7 = wb0.e(c, "animation_url");
                int e8 = wb0.e(c, "image_preview_url");
                int e9 = wb0.e(c, "image_data");
                int e10 = wb0.e(c, "token_uri");
                int e11 = wb0.e(c, "attributes");
                int e12 = wb0.e(c, "fullData");
                int e13 = wb0.e(c, "permalink");
                int e14 = wb0.e(c, "openSeaUrl");
                int e15 = wb0.e(c, "updatedTime");
                int e16 = wb0.e(c, "order");
                int i2 = e14;
                ArrayList arrayList = new ArrayList(c.getCount());
                while (c.moveToNext()) {
                    Long valueOf = c.isNull(e) ? null : Long.valueOf(c.getLong(e));
                    int i3 = c.getInt(e2);
                    long j = c.getLong(e3);
                    String string2 = c.isNull(e4) ? null : c.getString(e4);
                    String string3 = c.isNull(e5) ? null : c.getString(e5);
                    String string4 = c.isNull(e6) ? null : c.getString(e6);
                    String string5 = c.isNull(e7) ? null : c.getString(e7);
                    String string6 = c.isNull(e8) ? null : c.getString(e8);
                    String string7 = c.isNull(e9) ? null : c.getString(e9);
                    String string8 = c.isNull(e10) ? null : c.getString(e10);
                    String string9 = c.isNull(e11) ? null : c.getString(e11);
                    String string10 = c.isNull(e12) ? null : c.getString(e12);
                    if (c.isNull(e13)) {
                        i = i2;
                        string = null;
                    } else {
                        string = c.getString(e13);
                        i = i2;
                    }
                    int i4 = e15;
                    int i5 = e;
                    int i6 = e16;
                    e16 = i6;
                    arrayList.add(new RoomNFT(valueOf, i3, j, string2, string3, string4, string5, string6, string7, string8, string9, string10, string, c.isNull(i) ? null : c.getString(i), c.getLong(i4), c.getInt(i6)));
                    e = i5;
                    e15 = i4;
                    i2 = i;
                }
                return arrayList;
            } finally {
                c.close();
            }
        }

        public void finalize() {
            this.a.f();
        }
    }

    public e(RoomDatabase roomDatabase) {
        this.a = roomDatabase;
        this.b = new f(this, roomDatabase);
        this.c = new g(this, roomDatabase);
        this.d = new h(this, roomDatabase);
        this.e = new i(this, roomDatabase);
    }

    public static List<Class<?>> p() {
        return Collections.emptyList();
    }

    @Override // defpackage.of2
    public Object a(long j2, int i2, q70<? super te4> q70Var) {
        return CoroutinesRoom.b(this.a, true, new l(i2, j2), q70Var);
    }

    @Override // defpackage.of2
    public Object b(String str, long j2, q70<? super RoomNFT> q70Var) {
        k93 c2 = k93.c("SELECT * FROM table_room_nft WHERE token_id=? AND collectionId=?", 2);
        if (str == null) {
            c2.Y0(1);
        } else {
            c2.L(1, str);
        }
        c2.q0(2, j2);
        return CoroutinesRoom.a(this.a, false, id0.a(), new c(c2), q70Var);
    }

    @Override // defpackage.of2
    public Object c(String str, long j2, q70<? super Boolean> q70Var) {
        k93 c2 = k93.c("SELECT EXISTS (SELECT 1 FROM table_room_nft WHERE token_id=? AND collectionId=?)", 2);
        if (str == null) {
            c2.Y0(1);
        } else {
            c2.L(1, str);
        }
        c2.q0(2, j2);
        return CoroutinesRoom.a(this.a, false, id0.a(), new CallableC0207e(c2), q70Var);
    }

    @Override // defpackage.of2
    public Object d(long j2, q70<? super RoomNFT> q70Var) {
        k93 c2 = k93.c("SELECT * FROM table_room_nft WHERE _id=?", 1);
        c2.q0(1, j2);
        return CoroutinesRoom.a(this.a, false, id0.a(), new d(c2), q70Var);
    }

    @Override // defpackage.of2
    public Object e(long j2, q70<? super List<RoomNFT>> q70Var) {
        k93 c2 = k93.c("SELECT * FROM table_room_nft WHERE collectionId=? ORDER BY `order` ASC", 1);
        c2.q0(1, j2);
        return CoroutinesRoom.a(this.a, false, id0.a(), new a(c2), q70Var);
    }

    @Override // defpackage.of2
    public Object f(long j2, q70<? super te4> q70Var) {
        return CoroutinesRoom.b(this.a, true, new m(j2), q70Var);
    }

    @Override // defpackage.of2
    public Object g(RoomNFT roomNFT, q70<? super Long> q70Var) {
        return CoroutinesRoom.b(this.a, true, new j(roomNFT), q70Var);
    }

    @Override // defpackage.of2
    public Object h(int i2, q70<? super List<RoomNFT>> q70Var) {
        k93 c2 = k93.c("SELECT * FROM table_room_nft WHERE chain=? ORDER BY `order` ASC", 1);
        c2.q0(1, i2);
        return CoroutinesRoom.a(this.a, false, id0.a(), new b(c2), q70Var);
    }

    @Override // defpackage.of2
    public LiveData<List<RoomNFT>> i(long j2) {
        k93 c2 = k93.c("SELECT * FROM table_room_nft WHERE collectionId=? ORDER BY `order` ASC", 1);
        c2.q0(1, j2);
        return this.a.n().e(new String[]{"table_room_nft"}, false, new n(c2));
    }

    @Override // defpackage.of2
    public Object j(RoomNFT roomNFT, q70<? super te4> q70Var) {
        return CoroutinesRoom.b(this.a, true, new k(roomNFT), q70Var);
    }
}
