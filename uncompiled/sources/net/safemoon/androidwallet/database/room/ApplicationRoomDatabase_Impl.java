package net.safemoon.androidwallet.database.room;

import androidx.room.RoomDatabase;
import androidx.room.m;
import defpackage.f34;
import defpackage.tw3;
import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import org.web3j.abi.datatypes.Address;
import org.web3j.ens.contracts.generated.PublicResolver;

/* loaded from: classes2.dex */
public final class ApplicationRoomDatabase_Impl extends ApplicationRoomDatabase {
    public volatile a31 A;
    public volatile xc0 B;
    public volatile t53 C;
    public volatile x64 D;
    public volatile j10 E;
    public volatile of2 F;
    public volatile l00 G;
    public volatile eg4 z;

    /* loaded from: classes2.dex */
    public class a extends m.a {
        public a(int i) {
            super(i);
        }

        @Override // androidx.room.m.a
        public void a(sw3 sw3Var) {
            sw3Var.K("CREATE TABLE IF NOT EXISTS `table_token` (`symbolWithType` TEXT NOT NULL, `symbol` TEXT NOT NULL, `name` TEXT NOT NULL, `iconName` TEXT NOT NULL, `contractorAddress` TEXT NOT NULL, `tokenTypeChain` INTEGER NOT NULL, `decimals` INTEGER NOT NULL, `allowSwap` INTEGER NOT NULL, `nativeBalance` REAL NOT NULL, `priceInUsdt` REAL NOT NULL, `percentChange1h` REAL NOT NULL, `order` INTEGER NOT NULL DEFAULT 0, PRIMARY KEY(`symbolWithType`))");
            sw3Var.K("CREATE TABLE IF NOT EXISTS `table_fiat` (`symbol` TEXT NOT NULL, `name` TEXT, `rate` REAL, PRIMARY KEY(`symbol`))");
            sw3Var.K("CREATE TABLE IF NOT EXISTS `table_contact` (`_id` INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, `name` TEXT NOT NULL, `address` TEXT NOT NULL, `tokenTypeChain` INTEGER NOT NULL, `profilePath` TEXT NOT NULL, `lastSent` INTEGER, `contactCreate` INTEGER)");
            sw3Var.K("CREATE TABLE IF NOT EXISTS `table_custom` (`symbolWithType` TEXT NOT NULL, `symbol` TEXT NOT NULL, `name` TEXT NOT NULL, `iconPath` TEXT NOT NULL, `contractorAddress` TEXT NOT NULL, `tokenTypeChain` INTEGER NOT NULL, `decimals` INTEGER NOT NULL, `allowSwap` INTEGER NOT NULL, `nativeBalance` REAL NOT NULL, `priceInUsdt` REAL NOT NULL, `percentChange1h` REAL NOT NULL, PRIMARY KEY(`symbolWithType`))");
            sw3Var.K("CREATE TABLE IF NOT EXISTS `table_room_reflections_token_2` (`_id` INTEGER PRIMARY KEY AUTOINCREMENT, `symbolWithType` TEXT NOT NULL, `symbol` TEXT NOT NULL, `name` TEXT NOT NULL, `iconPath` TEXT NOT NULL, `contractorAddress` TEXT NOT NULL, `tokenTypeChain` INTEGER NOT NULL, `decimals` INTEGER NOT NULL, `nativeBalance` TEXT, `firstTimeStamp` INTEGER, `enableAdvanceMode` INTEGER NOT NULL DEFAULT 0, `latestBalance` INTEGER, `latestTimeStamp` INTEGER, `cmcId` INTEGER DEFAULT 0, `priceUsd` REAL DEFAULT 0)");
            sw3Var.K("CREATE TABLE IF NOT EXISTS `table_room_reflections_data_2` (`_id` INTEGER PRIMARY KEY AUTOINCREMENT, `symbolWithType` TEXT NOT NULL, `nativeBalance` TEXT NOT NULL, `blockBalance` TEXT NOT NULL, `block` TEXT NOT NULL, `timeStamp` INTEGER NOT NULL)");
            sw3Var.K("CREATE TABLE IF NOT EXISTS `table_room_token_info` (`_id` INTEGER PRIMARY KEY AUTOINCREMENT, `symbolWithType` TEXT NOT NULL, `chainId` INTEGER NOT NULL, `slug` TEXT, `cmcId` INTEGER)");
            sw3Var.K("CREATE TABLE IF NOT EXISTS `table_room_collections` (`_id` INTEGER PRIMARY KEY AUTOINCREMENT, `chain` INTEGER NOT NULL, `contractAddress` TEXT NOT NULL, `contract_type` TEXT NOT NULL, `name` TEXT, `description` TEXT, `imageUrl` TEXT, `symbol` TEXT, `marketPlace` TEXT, `slug` TEXT, `updatedTime` INTEGER NOT NULL, `order` INTEGER NOT NULL, `typeDeleteNft` INTEGER NOT NULL)");
            sw3Var.K("CREATE TABLE IF NOT EXISTS `table_room_nft` (`_id` INTEGER PRIMARY KEY AUTOINCREMENT, `chain` INTEGER NOT NULL, `collectionId` INTEGER NOT NULL, `token_id` TEXT, `name` TEXT, `description` TEXT, `animation_url` TEXT, `image_preview_url` TEXT, `image_data` TEXT, `token_uri` TEXT, `attributes` TEXT, `fullData` TEXT, `permalink` TEXT, `openSeaUrl` TEXT, `updatedTime` INTEGER NOT NULL, `order` INTEGER NOT NULL)");
            sw3Var.K("CREATE TABLE IF NOT EXISTS `table_room_coin_price_alert` (`id` INTEGER NOT NULL, `symbol` TEXT NOT NULL, `name` TEXT NOT NULL, `slug` TEXT NOT NULL, `cmcData` TEXT NOT NULL, PRIMARY KEY(`id`))");
            sw3Var.K("CREATE TABLE IF NOT EXISTS room_master_table (id INTEGER PRIMARY KEY,identity_hash TEXT)");
            sw3Var.K("INSERT OR REPLACE INTO room_master_table (id,identity_hash) VALUES(42, '5b12d6e35d989e14cd82f59a451a9829')");
        }

        @Override // androidx.room.m.a
        public void b(sw3 sw3Var) {
            sw3Var.K("DROP TABLE IF EXISTS `table_token`");
            sw3Var.K("DROP TABLE IF EXISTS `table_fiat`");
            sw3Var.K("DROP TABLE IF EXISTS `table_contact`");
            sw3Var.K("DROP TABLE IF EXISTS `table_custom`");
            sw3Var.K("DROP TABLE IF EXISTS `table_room_reflections_token_2`");
            sw3Var.K("DROP TABLE IF EXISTS `table_room_reflections_data_2`");
            sw3Var.K("DROP TABLE IF EXISTS `table_room_token_info`");
            sw3Var.K("DROP TABLE IF EXISTS `table_room_collections`");
            sw3Var.K("DROP TABLE IF EXISTS `table_room_nft`");
            sw3Var.K("DROP TABLE IF EXISTS `table_room_coin_price_alert`");
            if (ApplicationRoomDatabase_Impl.this.g != null) {
                int size = ApplicationRoomDatabase_Impl.this.g.size();
                for (int i = 0; i < size; i++) {
                    ((RoomDatabase.b) ApplicationRoomDatabase_Impl.this.g.get(i)).b(sw3Var);
                }
            }
        }

        @Override // androidx.room.m.a
        public void c(sw3 sw3Var) {
            if (ApplicationRoomDatabase_Impl.this.g != null) {
                int size = ApplicationRoomDatabase_Impl.this.g.size();
                for (int i = 0; i < size; i++) {
                    ((RoomDatabase.b) ApplicationRoomDatabase_Impl.this.g.get(i)).a(sw3Var);
                }
            }
        }

        @Override // androidx.room.m.a
        public void d(sw3 sw3Var) {
            ApplicationRoomDatabase_Impl.this.a = sw3Var;
            ApplicationRoomDatabase_Impl.this.x(sw3Var);
            if (ApplicationRoomDatabase_Impl.this.g != null) {
                int size = ApplicationRoomDatabase_Impl.this.g.size();
                for (int i = 0; i < size; i++) {
                    ((RoomDatabase.b) ApplicationRoomDatabase_Impl.this.g.get(i)).c(sw3Var);
                }
            }
        }

        @Override // androidx.room.m.a
        public void e(sw3 sw3Var) {
        }

        @Override // androidx.room.m.a
        public void f(sw3 sw3Var) {
            id0.b(sw3Var);
        }

        @Override // androidx.room.m.a
        public m.b g(sw3 sw3Var) {
            HashMap hashMap = new HashMap(12);
            hashMap.put("symbolWithType", new f34.a("symbolWithType", "TEXT", true, 1, null, 1));
            hashMap.put("symbol", new f34.a("symbol", "TEXT", true, 0, null, 1));
            hashMap.put(PublicResolver.FUNC_NAME, new f34.a(PublicResolver.FUNC_NAME, "TEXT", true, 0, null, 1));
            hashMap.put("iconName", new f34.a("iconName", "TEXT", true, 0, null, 1));
            hashMap.put("contractorAddress", new f34.a("contractorAddress", "TEXT", true, 0, null, 1));
            hashMap.put("tokenTypeChain", new f34.a("tokenTypeChain", "INTEGER", true, 0, null, 1));
            hashMap.put("decimals", new f34.a("decimals", "INTEGER", true, 0, null, 1));
            hashMap.put("allowSwap", new f34.a("allowSwap", "INTEGER", true, 0, null, 1));
            hashMap.put("nativeBalance", new f34.a("nativeBalance", "REAL", true, 0, null, 1));
            hashMap.put("priceInUsdt", new f34.a("priceInUsdt", "REAL", true, 0, null, 1));
            hashMap.put("percentChange1h", new f34.a("percentChange1h", "REAL", true, 0, null, 1));
            hashMap.put("order", new f34.a("order", "INTEGER", true, 0, "0", 1));
            f34 f34Var = new f34("table_token", hashMap, new HashSet(0), new HashSet(0));
            f34 a = f34.a(sw3Var, "table_token");
            if (!f34Var.equals(a)) {
                return new m.b(false, "table_token(net.safemoon.androidwallet.model.token.room.RoomToken).\n Expected:\n" + f34Var + "\n Found:\n" + a);
            }
            HashMap hashMap2 = new HashMap(3);
            hashMap2.put("symbol", new f34.a("symbol", "TEXT", true, 1, null, 1));
            hashMap2.put(PublicResolver.FUNC_NAME, new f34.a(PublicResolver.FUNC_NAME, "TEXT", false, 0, null, 1));
            hashMap2.put("rate", new f34.a("rate", "REAL", false, 0, null, 1));
            f34 f34Var2 = new f34("table_fiat", hashMap2, new HashSet(0), new HashSet(0));
            f34 a2 = f34.a(sw3Var, "table_fiat");
            if (!f34Var2.equals(a2)) {
                return new m.b(false, "table_fiat(net.safemoon.androidwallet.model.fiat.room.RoomFiat).\n Expected:\n" + f34Var2 + "\n Found:\n" + a2);
            }
            HashMap hashMap3 = new HashMap(7);
            hashMap3.put("_id", new f34.a("_id", "INTEGER", true, 1, null, 1));
            hashMap3.put(PublicResolver.FUNC_NAME, new f34.a(PublicResolver.FUNC_NAME, "TEXT", true, 0, null, 1));
            hashMap3.put(Address.TYPE_NAME, new f34.a(Address.TYPE_NAME, "TEXT", true, 0, null, 1));
            hashMap3.put("tokenTypeChain", new f34.a("tokenTypeChain", "INTEGER", true, 0, null, 1));
            hashMap3.put("profilePath", new f34.a("profilePath", "TEXT", true, 0, null, 1));
            hashMap3.put("lastSent", new f34.a("lastSent", "INTEGER", false, 0, null, 1));
            hashMap3.put("contactCreate", new f34.a("contactCreate", "INTEGER", false, 0, null, 1));
            f34 f34Var3 = new f34("table_contact", hashMap3, new HashSet(0), new HashSet(0));
            f34 a3 = f34.a(sw3Var, "table_contact");
            if (!f34Var3.equals(a3)) {
                return new m.b(false, "table_contact(net.safemoon.androidwallet.model.contact.room.RoomContact).\n Expected:\n" + f34Var3 + "\n Found:\n" + a3);
            }
            HashMap hashMap4 = new HashMap(11);
            hashMap4.put("symbolWithType", new f34.a("symbolWithType", "TEXT", true, 1, null, 1));
            hashMap4.put("symbol", new f34.a("symbol", "TEXT", true, 0, null, 1));
            hashMap4.put(PublicResolver.FUNC_NAME, new f34.a(PublicResolver.FUNC_NAME, "TEXT", true, 0, null, 1));
            hashMap4.put("iconPath", new f34.a("iconPath", "TEXT", true, 0, null, 1));
            hashMap4.put("contractorAddress", new f34.a("contractorAddress", "TEXT", true, 0, null, 1));
            hashMap4.put("tokenTypeChain", new f34.a("tokenTypeChain", "INTEGER", true, 0, null, 1));
            hashMap4.put("decimals", new f34.a("decimals", "INTEGER", true, 0, null, 1));
            hashMap4.put("allowSwap", new f34.a("allowSwap", "INTEGER", true, 0, null, 1));
            hashMap4.put("nativeBalance", new f34.a("nativeBalance", "REAL", true, 0, null, 1));
            hashMap4.put("priceInUsdt", new f34.a("priceInUsdt", "REAL", true, 0, null, 1));
            hashMap4.put("percentChange1h", new f34.a("percentChange1h", "REAL", true, 0, null, 1));
            f34 f34Var4 = new f34("table_custom", hashMap4, new HashSet(0), new HashSet(0));
            f34 a4 = f34.a(sw3Var, "table_custom");
            if (!f34Var4.equals(a4)) {
                return new m.b(false, "table_custom(net.safemoon.androidwallet.model.token.room.RoomCustomToken).\n Expected:\n" + f34Var4 + "\n Found:\n" + a4);
            }
            HashMap hashMap5 = new HashMap(15);
            hashMap5.put("_id", new f34.a("_id", "INTEGER", false, 1, null, 1));
            hashMap5.put("symbolWithType", new f34.a("symbolWithType", "TEXT", true, 0, null, 1));
            hashMap5.put("symbol", new f34.a("symbol", "TEXT", true, 0, null, 1));
            hashMap5.put(PublicResolver.FUNC_NAME, new f34.a(PublicResolver.FUNC_NAME, "TEXT", true, 0, null, 1));
            hashMap5.put("iconPath", new f34.a("iconPath", "TEXT", true, 0, null, 1));
            hashMap5.put("contractorAddress", new f34.a("contractorAddress", "TEXT", true, 0, null, 1));
            hashMap5.put("tokenTypeChain", new f34.a("tokenTypeChain", "INTEGER", true, 0, null, 1));
            hashMap5.put("decimals", new f34.a("decimals", "INTEGER", true, 0, null, 1));
            hashMap5.put("nativeBalance", new f34.a("nativeBalance", "TEXT", false, 0, null, 1));
            hashMap5.put("firstTimeStamp", new f34.a("firstTimeStamp", "INTEGER", false, 0, null, 1));
            hashMap5.put("enableAdvanceMode", new f34.a("enableAdvanceMode", "INTEGER", true, 0, "0", 1));
            hashMap5.put("latestBalance", new f34.a("latestBalance", "INTEGER", false, 0, null, 1));
            hashMap5.put("latestTimeStamp", new f34.a("latestTimeStamp", "INTEGER", false, 0, null, 1));
            hashMap5.put("cmcId", new f34.a("cmcId", "INTEGER", false, 0, "0", 1));
            hashMap5.put("priceUsd", new f34.a("priceUsd", "REAL", false, 0, "0", 1));
            f34 f34Var5 = new f34("table_room_reflections_token_2", hashMap5, new HashSet(0), new HashSet(0));
            f34 a5 = f34.a(sw3Var, "table_room_reflections_token_2");
            if (!f34Var5.equals(a5)) {
                return new m.b(false, "table_room_reflections_token_2(net.safemoon.androidwallet.model.reflections.RoomReflectionsToken).\n Expected:\n" + f34Var5 + "\n Found:\n" + a5);
            }
            HashMap hashMap6 = new HashMap(6);
            hashMap6.put("_id", new f34.a("_id", "INTEGER", false, 1, null, 1));
            hashMap6.put("symbolWithType", new f34.a("symbolWithType", "TEXT", true, 0, null, 1));
            hashMap6.put("nativeBalance", new f34.a("nativeBalance", "TEXT", true, 0, null, 1));
            hashMap6.put("blockBalance", new f34.a("blockBalance", "TEXT", true, 0, null, 1));
            hashMap6.put("block", new f34.a("block", "TEXT", true, 0, null, 1));
            hashMap6.put("timeStamp", new f34.a("timeStamp", "INTEGER", true, 0, null, 1));
            f34 f34Var6 = new f34("table_room_reflections_data_2", hashMap6, new HashSet(0), new HashSet(0));
            f34 a6 = f34.a(sw3Var, "table_room_reflections_data_2");
            if (!f34Var6.equals(a6)) {
                return new m.b(false, "table_room_reflections_data_2(net.safemoon.androidwallet.model.reflections.RoomReflectionsData).\n Expected:\n" + f34Var6 + "\n Found:\n" + a6);
            }
            HashMap hashMap7 = new HashMap(5);
            hashMap7.put("_id", new f34.a("_id", "INTEGER", false, 1, null, 1));
            hashMap7.put("symbolWithType", new f34.a("symbolWithType", "TEXT", true, 0, null, 1));
            hashMap7.put("chainId", new f34.a("chainId", "INTEGER", true, 0, null, 1));
            hashMap7.put("slug", new f34.a("slug", "TEXT", false, 0, null, 1));
            hashMap7.put("cmcId", new f34.a("cmcId", "INTEGER", false, 0, null, 1));
            f34 f34Var7 = new f34("table_room_token_info", hashMap7, new HashSet(0), new HashSet(0));
            f34 a7 = f34.a(sw3Var, "table_room_token_info");
            if (!f34Var7.equals(a7)) {
                return new m.b(false, "table_room_token_info(net.safemoon.androidwallet.model.RoomTokenInfo).\n Expected:\n" + f34Var7 + "\n Found:\n" + a7);
            }
            HashMap hashMap8 = new HashMap(13);
            hashMap8.put("_id", new f34.a("_id", "INTEGER", false, 1, null, 1));
            hashMap8.put("chain", new f34.a("chain", "INTEGER", true, 0, null, 1));
            hashMap8.put("contractAddress", new f34.a("contractAddress", "TEXT", true, 0, null, 1));
            hashMap8.put("contract_type", new f34.a("contract_type", "TEXT", true, 0, null, 1));
            hashMap8.put(PublicResolver.FUNC_NAME, new f34.a(PublicResolver.FUNC_NAME, "TEXT", false, 0, null, 1));
            hashMap8.put("description", new f34.a("description", "TEXT", false, 0, null, 1));
            hashMap8.put("imageUrl", new f34.a("imageUrl", "TEXT", false, 0, null, 1));
            hashMap8.put("symbol", new f34.a("symbol", "TEXT", false, 0, null, 1));
            hashMap8.put("marketPlace", new f34.a("marketPlace", "TEXT", false, 0, null, 1));
            hashMap8.put("slug", new f34.a("slug", "TEXT", false, 0, null, 1));
            hashMap8.put("updatedTime", new f34.a("updatedTime", "INTEGER", true, 0, null, 1));
            hashMap8.put("order", new f34.a("order", "INTEGER", true, 0, null, 1));
            hashMap8.put("typeDeleteNft", new f34.a("typeDeleteNft", "INTEGER", true, 0, null, 1));
            f34 f34Var8 = new f34("table_room_collections", hashMap8, new HashSet(0), new HashSet(0));
            f34 a8 = f34.a(sw3Var, "table_room_collections");
            if (!f34Var8.equals(a8)) {
                return new m.b(false, "table_room_collections(net.safemoon.androidwallet.model.collectible.RoomCollection).\n Expected:\n" + f34Var8 + "\n Found:\n" + a8);
            }
            HashMap hashMap9 = new HashMap(16);
            hashMap9.put("_id", new f34.a("_id", "INTEGER", false, 1, null, 1));
            hashMap9.put("chain", new f34.a("chain", "INTEGER", true, 0, null, 1));
            hashMap9.put("collectionId", new f34.a("collectionId", "INTEGER", true, 0, null, 1));
            hashMap9.put("token_id", new f34.a("token_id", "TEXT", false, 0, null, 1));
            hashMap9.put(PublicResolver.FUNC_NAME, new f34.a(PublicResolver.FUNC_NAME, "TEXT", false, 0, null, 1));
            hashMap9.put("description", new f34.a("description", "TEXT", false, 0, null, 1));
            hashMap9.put("animation_url", new f34.a("animation_url", "TEXT", false, 0, null, 1));
            hashMap9.put("image_preview_url", new f34.a("image_preview_url", "TEXT", false, 0, null, 1));
            hashMap9.put("image_data", new f34.a("image_data", "TEXT", false, 0, null, 1));
            hashMap9.put("token_uri", new f34.a("token_uri", "TEXT", false, 0, null, 1));
            hashMap9.put("attributes", new f34.a("attributes", "TEXT", false, 0, null, 1));
            hashMap9.put("fullData", new f34.a("fullData", "TEXT", false, 0, null, 1));
            hashMap9.put("permalink", new f34.a("permalink", "TEXT", false, 0, null, 1));
            hashMap9.put("openSeaUrl", new f34.a("openSeaUrl", "TEXT", false, 0, null, 1));
            hashMap9.put("updatedTime", new f34.a("updatedTime", "INTEGER", true, 0, null, 1));
            hashMap9.put("order", new f34.a("order", "INTEGER", true, 0, null, 1));
            f34 f34Var9 = new f34("table_room_nft", hashMap9, new HashSet(0), new HashSet(0));
            f34 a9 = f34.a(sw3Var, "table_room_nft");
            if (!f34Var9.equals(a9)) {
                return new m.b(false, "table_room_nft(net.safemoon.androidwallet.model.collectible.RoomNFT).\n Expected:\n" + f34Var9 + "\n Found:\n" + a9);
            }
            HashMap hashMap10 = new HashMap(5);
            hashMap10.put("id", new f34.a("id", "INTEGER", true, 1, null, 1));
            hashMap10.put("symbol", new f34.a("symbol", "TEXT", true, 0, null, 1));
            hashMap10.put(PublicResolver.FUNC_NAME, new f34.a(PublicResolver.FUNC_NAME, "TEXT", true, 0, null, 1));
            hashMap10.put("slug", new f34.a("slug", "TEXT", true, 0, null, 1));
            hashMap10.put("cmcData", new f34.a("cmcData", "TEXT", true, 0, null, 1));
            f34 f34Var10 = new f34("table_room_coin_price_alert", hashMap10, new HashSet(0), new HashSet(0));
            f34 a10 = f34.a(sw3Var, "table_room_coin_price_alert");
            if (!f34Var10.equals(a10)) {
                return new m.b(false, "table_room_coin_price_alert(net.safemoon.androidwallet.model.RoomCoinPriceAlert).\n Expected:\n" + f34Var10 + "\n Found:\n" + a10);
            }
            return new m.b(true, null);
        }
    }

    @Override // net.safemoon.androidwallet.database.room.ApplicationRoomDatabase
    public l00 S() {
        l00 l00Var;
        if (this.G != null) {
            return this.G;
        }
        synchronized (this) {
            if (this.G == null) {
                this.G = new net.safemoon.androidwallet.database.room.a(this);
            }
            l00Var = this.G;
        }
        return l00Var;
    }

    @Override // net.safemoon.androidwallet.database.room.ApplicationRoomDatabase
    public j10 T() {
        j10 j10Var;
        if (this.E != null) {
            return this.E;
        }
        synchronized (this) {
            if (this.E == null) {
                this.E = new b(this);
            }
            j10Var = this.E;
        }
        return j10Var;
    }

    @Override // net.safemoon.androidwallet.database.room.ApplicationRoomDatabase
    public xc0 U() {
        xc0 xc0Var;
        if (this.B != null) {
            return this.B;
        }
        synchronized (this) {
            if (this.B == null) {
                this.B = new c(this);
            }
            xc0Var = this.B;
        }
        return xc0Var;
    }

    @Override // net.safemoon.androidwallet.database.room.ApplicationRoomDatabase
    public a31 V() {
        a31 a31Var;
        if (this.A != null) {
            return this.A;
        }
        synchronized (this) {
            if (this.A == null) {
                this.A = new d(this);
            }
            a31Var = this.A;
        }
        return a31Var;
    }

    @Override // net.safemoon.androidwallet.database.room.ApplicationRoomDatabase
    public of2 W() {
        of2 of2Var;
        if (this.F != null) {
            return this.F;
        }
        synchronized (this) {
            if (this.F == null) {
                this.F = new e(this);
            }
            of2Var = this.F;
        }
        return of2Var;
    }

    @Override // net.safemoon.androidwallet.database.room.ApplicationRoomDatabase
    public t53 X() {
        t53 t53Var;
        if (this.C != null) {
            return this.C;
        }
        synchronized (this) {
            if (this.C == null) {
                this.C = new f(this);
            }
            t53Var = this.C;
        }
        return t53Var;
    }

    @Override // net.safemoon.androidwallet.database.room.ApplicationRoomDatabase
    public x64 Y() {
        x64 x64Var;
        if (this.D != null) {
            return this.D;
        }
        synchronized (this) {
            if (this.D == null) {
                this.D = new g(this);
            }
            x64Var = this.D;
        }
        return x64Var;
    }

    @Override // net.safemoon.androidwallet.database.room.ApplicationRoomDatabase
    public eg4 Z() {
        eg4 eg4Var;
        if (this.z != null) {
            return this.z;
        }
        synchronized (this) {
            if (this.z == null) {
                this.z = new h(this);
            }
            eg4Var = this.z;
        }
        return eg4Var;
    }

    @Override // androidx.room.RoomDatabase
    public void f() {
        super.c();
        sw3 D0 = super.o().D0();
        try {
            super.e();
            D0.K("DELETE FROM `table_token`");
            D0.K("DELETE FROM `table_fiat`");
            D0.K("DELETE FROM `table_contact`");
            D0.K("DELETE FROM `table_custom`");
            D0.K("DELETE FROM `table_room_reflections_token_2`");
            D0.K("DELETE FROM `table_room_reflections_data_2`");
            D0.K("DELETE FROM `table_room_token_info`");
            D0.K("DELETE FROM `table_room_collections`");
            D0.K("DELETE FROM `table_room_nft`");
            D0.K("DELETE FROM `table_room_coin_price_alert`");
            super.E();
        } finally {
            super.j();
            D0.E0("PRAGMA wal_checkpoint(FULL)").close();
            if (!D0.h1()) {
                D0.K("VACUUM");
            }
        }
    }

    @Override // androidx.room.RoomDatabase
    public androidx.room.f h() {
        return new androidx.room.f(this, new HashMap(0), new HashMap(0), "table_token", "table_fiat", "table_contact", "table_custom", "table_room_reflections_token_2", "table_room_reflections_data_2", "table_room_token_info", "table_room_collections", "table_room_nft", "table_room_coin_price_alert");
    }

    @Override // androidx.room.RoomDatabase
    public tw3 i(androidx.room.c cVar) {
        return cVar.a.a(tw3.b.a(cVar.b).c(cVar.c).b(new m(cVar, new a(11), "5b12d6e35d989e14cd82f59a451a9829", "347c75a0151258452fc805b1859b2c28")).a());
    }

    @Override // androidx.room.RoomDatabase
    public List<w82> k(Map<Class<? extends fk>, fk> map) {
        return Arrays.asList(new w82[0]);
    }

    @Override // androidx.room.RoomDatabase
    public Set<Class<? extends fk>> q() {
        return new HashSet();
    }

    @Override // androidx.room.RoomDatabase
    public Map<Class<?>, List<Class<?>>> r() {
        HashMap hashMap = new HashMap();
        hashMap.put(eg4.class, h.r());
        hashMap.put(t60.class, u60.g());
        hashMap.put(a31.class, d.g());
        hashMap.put(xc0.class, c.e());
        hashMap.put(t53.class, f.H());
        hashMap.put(x64.class, g.f());
        hashMap.put(j10.class, b.n());
        hashMap.put(of2.class, e.p());
        hashMap.put(l00.class, net.safemoon.androidwallet.database.room.a.h());
        return hashMap;
    }
}
