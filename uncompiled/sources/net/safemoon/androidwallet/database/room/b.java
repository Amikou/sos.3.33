package net.safemoon.androidwallet.database.room;

import android.database.Cursor;
import androidx.lifecycle.LiveData;
import androidx.room.CoroutinesRoom;
import androidx.room.RoomDatabase;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.concurrent.Callable;
import net.safemoon.androidwallet.model.collectible.RoomCollection;
import net.safemoon.androidwallet.model.collectible.RoomCollectionAndNft;
import net.safemoon.androidwallet.model.collectible.RoomNFT;
import org.web3j.ens.contracts.generated.PublicResolver;

/* compiled from: CollectionDao_Impl.java */
/* loaded from: classes2.dex */
public final class b implements j10 {
    public final RoomDatabase a;
    public final zv0<RoomCollection> b;
    public final yv0<RoomCollection> c;
    public final co3 d;
    public final co3 e;

    /* compiled from: CollectionDao_Impl.java */
    /* loaded from: classes2.dex */
    public class a implements Callable<List<RoomCollectionAndNft>> {
        public final /* synthetic */ k93 a;

        public a(k93 k93Var) {
            this.a = k93Var;
        }

        /* JADX WARN: Removed duplicated region for block: B:86:0x01c2 A[Catch: all -> 0x01f5, TryCatch #0 {all -> 0x01f5, blocks: (B:3:0x0010, B:4:0x0063, B:6:0x0069, B:8:0x006f, B:10:0x007f, B:13:0x0093, B:14:0x00ab, B:16:0x00b1, B:18:0x00b7, B:20:0x00bd, B:22:0x00c3, B:24:0x00c9, B:26:0x00cf, B:28:0x00d5, B:30:0x00db, B:32:0x00e1, B:34:0x00e7, B:36:0x00ed, B:38:0x00f5, B:40:0x00fd, B:47:0x0116, B:51:0x0129, B:55:0x013c, B:59:0x014b, B:63:0x015a, B:67:0x0169, B:71:0x0178, B:75:0x0187, B:79:0x0196, B:83:0x01a5, B:84:0x01bc, B:86:0x01c2, B:89:0x01d8, B:90:0x01dd, B:82:0x019f, B:78:0x0190, B:74:0x0181, B:70:0x0172, B:66:0x0163, B:62:0x0154, B:58:0x0145, B:54:0x0136, B:50:0x011f), top: B:97:0x0010 }] */
        /* JADX WARN: Removed duplicated region for block: B:87:0x01d1  */
        /* JADX WARN: Removed duplicated region for block: B:89:0x01d8 A[Catch: all -> 0x01f5, TryCatch #0 {all -> 0x01f5, blocks: (B:3:0x0010, B:4:0x0063, B:6:0x0069, B:8:0x006f, B:10:0x007f, B:13:0x0093, B:14:0x00ab, B:16:0x00b1, B:18:0x00b7, B:20:0x00bd, B:22:0x00c3, B:24:0x00c9, B:26:0x00cf, B:28:0x00d5, B:30:0x00db, B:32:0x00e1, B:34:0x00e7, B:36:0x00ed, B:38:0x00f5, B:40:0x00fd, B:47:0x0116, B:51:0x0129, B:55:0x013c, B:59:0x014b, B:63:0x015a, B:67:0x0169, B:71:0x0178, B:75:0x0187, B:79:0x0196, B:83:0x01a5, B:84:0x01bc, B:86:0x01c2, B:89:0x01d8, B:90:0x01dd, B:82:0x019f, B:78:0x0190, B:74:0x0181, B:70:0x0172, B:66:0x0163, B:62:0x0154, B:58:0x0145, B:54:0x0136, B:50:0x011f), top: B:97:0x0010 }] */
        @Override // java.util.concurrent.Callable
        /* renamed from: a */
        /*
            Code decompiled incorrectly, please refer to instructions dump.
            To view partially-correct code enable 'Show inconsistent code' option in preferences
        */
        public java.util.List<net.safemoon.androidwallet.model.collectible.RoomCollectionAndNft> call() throws java.lang.Exception {
            /*
                Method dump skipped, instructions count: 506
                To view this dump change 'Code comments level' option to 'DEBUG'
            */
            throw new UnsupportedOperationException("Method not decompiled: net.safemoon.androidwallet.database.room.b.a.call():java.util.List");
        }

        public void finalize() {
            this.a.f();
        }
    }

    /* compiled from: CollectionDao_Impl.java */
    /* renamed from: net.safemoon.androidwallet.database.room.b$b  reason: collision with other inner class name */
    /* loaded from: classes2.dex */
    public class CallableC0204b implements Callable<List<RoomCollection>> {
        public final /* synthetic */ k93 a;

        public CallableC0204b(k93 k93Var) {
            this.a = k93Var;
        }

        @Override // java.util.concurrent.Callable
        /* renamed from: a */
        public List<RoomCollection> call() throws Exception {
            CallableC0204b callableC0204b = this;
            Cursor c = id0.c(b.this.a, callableC0204b.a, false, null);
            try {
                int e = wb0.e(c, "_id");
                int e2 = wb0.e(c, "chain");
                int e3 = wb0.e(c, "contractAddress");
                int e4 = wb0.e(c, "contract_type");
                int e5 = wb0.e(c, PublicResolver.FUNC_NAME);
                int e6 = wb0.e(c, "description");
                int e7 = wb0.e(c, "imageUrl");
                int e8 = wb0.e(c, "symbol");
                int e9 = wb0.e(c, "marketPlace");
                int e10 = wb0.e(c, "slug");
                int e11 = wb0.e(c, "updatedTime");
                int e12 = wb0.e(c, "order");
                int e13 = wb0.e(c, "typeDeleteNft");
                try {
                    ArrayList arrayList = new ArrayList(c.getCount());
                    while (c.moveToNext()) {
                        arrayList.add(new RoomCollection(c.isNull(e) ? null : Long.valueOf(c.getLong(e)), c.getInt(e2), c.isNull(e3) ? null : c.getString(e3), c.isNull(e4) ? null : c.getString(e4), c.isNull(e5) ? null : c.getString(e5), c.isNull(e6) ? null : c.getString(e6), c.isNull(e7) ? null : c.getString(e7), c.isNull(e8) ? null : c.getString(e8), c.isNull(e9) ? null : c.getString(e9), c.isNull(e10) ? null : c.getString(e10), c.getLong(e11), c.getInt(e12), c.getInt(e13)));
                    }
                    c.close();
                    this.a.f();
                    return arrayList;
                } catch (Throwable th) {
                    th = th;
                    callableC0204b = this;
                    c.close();
                    callableC0204b.a.f();
                    throw th;
                }
            } catch (Throwable th2) {
                th = th2;
            }
        }
    }

    /* compiled from: CollectionDao_Impl.java */
    /* loaded from: classes2.dex */
    public class c extends zv0<RoomCollection> {
        public c(b bVar, RoomDatabase roomDatabase) {
            super(roomDatabase);
        }

        @Override // defpackage.co3
        public String d() {
            return "INSERT OR REPLACE INTO `table_room_collections` (`_id`,`chain`,`contractAddress`,`contract_type`,`name`,`description`,`imageUrl`,`symbol`,`marketPlace`,`slug`,`updatedTime`,`order`,`typeDeleteNft`) VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?)";
        }

        @Override // defpackage.zv0
        /* renamed from: k */
        public void g(ww3 ww3Var, RoomCollection roomCollection) {
            if (roomCollection.getId() == null) {
                ww3Var.Y0(1);
            } else {
                ww3Var.q0(1, roomCollection.getId().longValue());
            }
            ww3Var.q0(2, roomCollection.getChain());
            if (roomCollection.getContractAddress() == null) {
                ww3Var.Y0(3);
            } else {
                ww3Var.L(3, roomCollection.getContractAddress());
            }
            if (roomCollection.getContract_type() == null) {
                ww3Var.Y0(4);
            } else {
                ww3Var.L(4, roomCollection.getContract_type());
            }
            if (roomCollection.getName() == null) {
                ww3Var.Y0(5);
            } else {
                ww3Var.L(5, roomCollection.getName());
            }
            if (roomCollection.getDescription() == null) {
                ww3Var.Y0(6);
            } else {
                ww3Var.L(6, roomCollection.getDescription());
            }
            if (roomCollection.getImageUrl() == null) {
                ww3Var.Y0(7);
            } else {
                ww3Var.L(7, roomCollection.getImageUrl());
            }
            if (roomCollection.getSymbol() == null) {
                ww3Var.Y0(8);
            } else {
                ww3Var.L(8, roomCollection.getSymbol());
            }
            if (roomCollection.getMarketPlace() == null) {
                ww3Var.Y0(9);
            } else {
                ww3Var.L(9, roomCollection.getMarketPlace());
            }
            if (roomCollection.getSlug() == null) {
                ww3Var.Y0(10);
            } else {
                ww3Var.L(10, roomCollection.getSlug());
            }
            ww3Var.q0(11, roomCollection.getUpdatedTime());
            ww3Var.q0(12, roomCollection.getOrder());
            ww3Var.q0(13, roomCollection.getTypeDeleteNft());
        }
    }

    /* compiled from: CollectionDao_Impl.java */
    /* loaded from: classes2.dex */
    public class d extends yv0<RoomCollection> {
        public d(b bVar, RoomDatabase roomDatabase) {
            super(roomDatabase);
        }

        @Override // defpackage.co3
        public String d() {
            return "DELETE FROM `table_room_collections` WHERE `_id` = ?";
        }

        @Override // defpackage.yv0
        /* renamed from: i */
        public void g(ww3 ww3Var, RoomCollection roomCollection) {
            if (roomCollection.getId() == null) {
                ww3Var.Y0(1);
            } else {
                ww3Var.q0(1, roomCollection.getId().longValue());
            }
        }
    }

    /* compiled from: CollectionDao_Impl.java */
    /* loaded from: classes2.dex */
    public class e extends co3 {
        public e(b bVar, RoomDatabase roomDatabase) {
            super(roomDatabase);
        }

        @Override // defpackage.co3
        public String d() {
            return "UPDATE table_room_collections SET `order` = ? WHERE _id=?";
        }
    }

    /* compiled from: CollectionDao_Impl.java */
    /* loaded from: classes2.dex */
    public class f extends co3 {
        public f(b bVar, RoomDatabase roomDatabase) {
            super(roomDatabase);
        }

        @Override // defpackage.co3
        public String d() {
            return "UPDATE table_room_collections SET `typeDeleteNft` = ? WHERE _id=?";
        }
    }

    /* compiled from: CollectionDao_Impl.java */
    /* loaded from: classes2.dex */
    public class g implements Callable<Long> {
        public final /* synthetic */ RoomCollection a;

        public g(RoomCollection roomCollection) {
            this.a = roomCollection;
        }

        @Override // java.util.concurrent.Callable
        /* renamed from: a */
        public Long call() throws Exception {
            b.this.a.e();
            try {
                long j = b.this.b.j(this.a);
                b.this.a.E();
                return Long.valueOf(j);
            } finally {
                b.this.a.j();
            }
        }
    }

    /* compiled from: CollectionDao_Impl.java */
    /* loaded from: classes2.dex */
    public class h implements Callable<te4> {
        public final /* synthetic */ RoomCollection a;

        public h(RoomCollection roomCollection) {
            this.a = roomCollection;
        }

        @Override // java.util.concurrent.Callable
        /* renamed from: a */
        public te4 call() throws Exception {
            b.this.a.e();
            try {
                b.this.c.h(this.a);
                b.this.a.E();
                return te4.a;
            } finally {
                b.this.a.j();
            }
        }
    }

    /* compiled from: CollectionDao_Impl.java */
    /* loaded from: classes2.dex */
    public class i implements Callable<te4> {
        public final /* synthetic */ int a;
        public final /* synthetic */ long b;

        public i(int i, long j) {
            this.a = i;
            this.b = j;
        }

        @Override // java.util.concurrent.Callable
        /* renamed from: a */
        public te4 call() throws Exception {
            ww3 a = b.this.d.a();
            a.q0(1, this.a);
            a.q0(2, this.b);
            b.this.a.e();
            try {
                a.T();
                b.this.a.E();
                return te4.a;
            } finally {
                b.this.a.j();
                b.this.d.f(a);
            }
        }
    }

    /* compiled from: CollectionDao_Impl.java */
    /* loaded from: classes2.dex */
    public class j implements Callable<te4> {
        public final /* synthetic */ int a;
        public final /* synthetic */ long b;

        public j(int i, long j) {
            this.a = i;
            this.b = j;
        }

        @Override // java.util.concurrent.Callable
        /* renamed from: a */
        public te4 call() throws Exception {
            ww3 a = b.this.e.a();
            a.q0(1, this.a);
            a.q0(2, this.b);
            b.this.a.e();
            try {
                a.T();
                b.this.a.E();
                return te4.a;
            } finally {
                b.this.a.j();
                b.this.e.f(a);
            }
        }
    }

    public b(RoomDatabase roomDatabase) {
        this.a = roomDatabase;
        this.b = new c(this, roomDatabase);
        this.c = new d(this, roomDatabase);
        this.d = new e(this, roomDatabase);
        this.e = new f(this, roomDatabase);
    }

    public static List<Class<?>> n() {
        return Collections.emptyList();
    }

    @Override // defpackage.j10
    public Object a(long j2, int i2, q70<? super te4> q70Var) {
        return CoroutinesRoom.b(this.a, true, new i(i2, j2), q70Var);
    }

    @Override // defpackage.j10
    public Object b(int i2, q70<? super List<RoomCollection>> q70Var) {
        k93 c2 = k93.c("SELECT * FROM table_room_collections WHERE chain=? ORDER BY `order` ASC", 1);
        c2.q0(1, i2);
        return CoroutinesRoom.a(this.a, false, id0.a(), new CallableC0204b(c2), q70Var);
    }

    @Override // defpackage.j10
    public Object c(RoomCollection roomCollection, q70<? super te4> q70Var) {
        return CoroutinesRoom.b(this.a, true, new h(roomCollection), q70Var);
    }

    @Override // defpackage.j10
    public Object d(long j2, int i2, q70<? super te4> q70Var) {
        return CoroutinesRoom.b(this.a, true, new j(i2, j2), q70Var);
    }

    @Override // defpackage.j10
    public Object e(RoomCollection roomCollection, q70<? super Long> q70Var) {
        return CoroutinesRoom.b(this.a, true, new g(roomCollection), q70Var);
    }

    @Override // defpackage.j10
    public LiveData<List<RoomCollectionAndNft>> f(int i2) {
        k93 c2 = k93.c("SELECT * FROM table_room_collections WHERE chain=? ORDER BY `order` ASC", 1);
        c2.q0(1, i2);
        return this.a.n().e(new String[]{"table_room_nft", "table_room_collections"}, false, new a(c2));
    }

    public final void g(i22<ArrayList<RoomNFT>> i22Var) {
        int i2;
        String string;
        String string2;
        int i3;
        int i4;
        String string3;
        String string4;
        int i5;
        String string5;
        int i6;
        i22<ArrayList<RoomNFT>> i22Var2 = i22Var;
        if (i22Var.k()) {
            return;
        }
        if (i22Var.t() > 999) {
            i22<ArrayList<RoomNFT>> i22Var3 = new i22<>(999);
            int t = i22Var.t();
            int i7 = 0;
            int i8 = 0;
            while (i7 < t) {
                i22Var3.o(i22Var2.l(i7), i22Var2.u(i7));
                i7++;
                i8++;
                if (i8 == 999) {
                    g(i22Var3);
                    i22Var3 = new i22<>(999);
                    i8 = 0;
                }
            }
            if (i8 > 0) {
                g(i22Var3);
                return;
            }
            return;
        }
        StringBuilder b = qu3.b();
        b.append("SELECT `_id`,`chain`,`collectionId`,`token_id`,`name`,`description`,`animation_url`,`image_preview_url`,`image_data`,`token_uri`,`attributes`,`fullData`,`permalink`,`openSeaUrl`,`updatedTime`,`order` FROM `table_room_nft` WHERE `collectionId` IN (");
        int t2 = i22Var.t();
        qu3.a(b, t2);
        b.append(")");
        k93 c2 = k93.c(b.toString(), t2 + 0);
        int i9 = 1;
        for (int i10 = 0; i10 < i22Var.t(); i10++) {
            c2.q0(i9, i22Var2.l(i10));
            i9++;
        }
        Cursor c3 = id0.c(this.a, c2, false, null);
        try {
            int d2 = wb0.d(c3, "collectionId");
            if (d2 == -1) {
                return;
            }
            int e2 = wb0.e(c3, "_id");
            int e3 = wb0.e(c3, "chain");
            int e4 = wb0.e(c3, "collectionId");
            int e5 = wb0.e(c3, "token_id");
            int e6 = wb0.e(c3, PublicResolver.FUNC_NAME);
            int e7 = wb0.e(c3, "description");
            int e8 = wb0.e(c3, "animation_url");
            int e9 = wb0.e(c3, "image_preview_url");
            int e10 = wb0.e(c3, "image_data");
            int e11 = wb0.e(c3, "token_uri");
            int e12 = wb0.e(c3, "attributes");
            int e13 = wb0.e(c3, "fullData");
            int e14 = wb0.e(c3, "permalink");
            int e15 = wb0.e(c3, "openSeaUrl");
            int e16 = wb0.e(c3, "updatedTime");
            int e17 = wb0.e(c3, "order");
            while (c3.moveToNext()) {
                if (c3.isNull(d2)) {
                    i22Var2 = i22Var;
                } else {
                    int i11 = e11;
                    int i12 = e12;
                    ArrayList<RoomNFT> g2 = i22Var2.g(c3.getLong(d2));
                    if (g2 != null) {
                        Long valueOf = c3.isNull(e2) ? null : Long.valueOf(c3.getLong(e2));
                        int i13 = c3.getInt(e3);
                        long j2 = c3.getLong(e4);
                        String string6 = c3.isNull(e5) ? null : c3.getString(e5);
                        String string7 = c3.isNull(e6) ? null : c3.getString(e6);
                        String string8 = c3.isNull(e7) ? null : c3.getString(e7);
                        String string9 = c3.isNull(e8) ? null : c3.getString(e8);
                        String string10 = c3.isNull(e9) ? null : c3.getString(e9);
                        if (c3.isNull(e10)) {
                            i2 = i11;
                            string = null;
                        } else {
                            string = c3.getString(e10);
                            i2 = i11;
                        }
                        if (c3.isNull(i2)) {
                            i3 = i12;
                            string2 = null;
                        } else {
                            string2 = c3.getString(i2);
                            i3 = i12;
                        }
                        String string11 = c3.isNull(i3) ? null : c3.getString(i3);
                        if (c3.isNull(e13)) {
                            i12 = i3;
                            i4 = e14;
                            string3 = null;
                        } else {
                            i12 = i3;
                            i4 = e14;
                            string3 = c3.getString(e13);
                        }
                        if (c3.isNull(i4)) {
                            e14 = i4;
                            i5 = e15;
                            string4 = null;
                        } else {
                            string4 = c3.getString(i4);
                            e14 = i4;
                            i5 = e15;
                        }
                        if (c3.isNull(i5)) {
                            e15 = i5;
                            i6 = e16;
                            string5 = null;
                        } else {
                            string5 = c3.getString(i5);
                            e15 = i5;
                            i6 = e16;
                        }
                        e16 = i6;
                        g2.add(new RoomNFT(valueOf, i13, j2, string6, string7, string8, string9, string10, string, string2, string11, string3, string4, string5, c3.getLong(i6), c3.getInt(e17)));
                    } else {
                        i2 = i11;
                    }
                    i22Var2 = i22Var;
                    e11 = i2;
                    e12 = i12;
                }
            }
        } finally {
            c3.close();
        }
    }
}
