package net.safemoon.androidwallet.database.room;

import android.database.Cursor;
import androidx.lifecycle.LiveData;
import androidx.room.CoroutinesRoom;
import androidx.room.RoomDatabase;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Set;
import java.util.concurrent.Callable;
import net.safemoon.androidwallet.model.reflections.RoomReflectionsData;
import net.safemoon.androidwallet.model.reflections.RoomReflectionsDataAndToken;
import net.safemoon.androidwallet.model.reflections.RoomReflectionsToken;
import net.safemoon.androidwallet.model.reflections.RoomReflectionsTokenAndData;
import org.web3j.ens.contracts.generated.PublicResolver;

/* compiled from: ReflectionsDao_Impl.java */
/* loaded from: classes2.dex */
public final class f implements t53 {
    public final RoomDatabase a;
    public final zv0<RoomReflectionsToken> b;
    public final zv0<RoomReflectionsData> c;
    public final yv0<RoomReflectionsToken> d;
    public final co3 e;
    public final co3 f;
    public final co3 g;
    public final co3 h;
    public final co3 i;
    public final co3 j;

    /* compiled from: ReflectionsDao_Impl.java */
    /* loaded from: classes2.dex */
    public class a implements Callable<Long> {
        public final /* synthetic */ RoomReflectionsToken a;

        public a(RoomReflectionsToken roomReflectionsToken) {
            this.a = roomReflectionsToken;
        }

        @Override // java.util.concurrent.Callable
        /* renamed from: a */
        public Long call() throws Exception {
            f.this.a.e();
            try {
                long j = f.this.b.j(this.a);
                f.this.a.E();
                return Long.valueOf(j);
            } finally {
                f.this.a.j();
            }
        }
    }

    /* compiled from: ReflectionsDao_Impl.java */
    /* loaded from: classes2.dex */
    public class a0 extends co3 {
        public a0(f fVar, RoomDatabase roomDatabase) {
            super(roomDatabase);
        }

        @Override // defpackage.co3
        public String d() {
            return "DELETE FROM table_room_reflections_data_2 WHERE _ID NOT IN (SELECT MIN(_id) ROWIDS FROM table_room_reflections_data_2  GROUP BY symbolWithType)";
        }
    }

    /* compiled from: ReflectionsDao_Impl.java */
    /* loaded from: classes2.dex */
    public class b implements Callable<Long> {
        public final /* synthetic */ RoomReflectionsData a;

        public b(RoomReflectionsData roomReflectionsData) {
            this.a = roomReflectionsData;
        }

        @Override // java.util.concurrent.Callable
        /* renamed from: a */
        public Long call() throws Exception {
            f.this.a.e();
            try {
                long j = f.this.c.j(this.a);
                f.this.a.E();
                return Long.valueOf(j);
            } finally {
                f.this.a.j();
            }
        }
    }

    /* compiled from: ReflectionsDao_Impl.java */
    /* loaded from: classes2.dex */
    public class b0 extends co3 {
        public b0(f fVar, RoomDatabase roomDatabase) {
            super(roomDatabase);
        }

        @Override // defpackage.co3
        public String d() {
            return "DELETE FROM table_room_reflections_data_2 WHERE symbolWithType=? AND _ID NOT IN (SELECT MIN(_id) ROWIDS FROM table_room_reflections_data_2  GROUP BY symbolWithType)";
        }
    }

    /* compiled from: ReflectionsDao_Impl.java */
    /* loaded from: classes2.dex */
    public class c implements Callable<te4> {
        public final /* synthetic */ RoomReflectionsToken a;

        public c(RoomReflectionsToken roomReflectionsToken) {
            this.a = roomReflectionsToken;
        }

        @Override // java.util.concurrent.Callable
        /* renamed from: a */
        public te4 call() throws Exception {
            f.this.a.e();
            try {
                f.this.d.h(this.a);
                f.this.a.E();
                return te4.a;
            } finally {
                f.this.a.j();
            }
        }
    }

    /* compiled from: ReflectionsDao_Impl.java */
    /* loaded from: classes2.dex */
    public class d implements Callable<te4> {
        public final /* synthetic */ int a;
        public final /* synthetic */ String b;

        public d(int i, String str) {
            this.a = i;
            this.b = str;
        }

        @Override // java.util.concurrent.Callable
        /* renamed from: a */
        public te4 call() throws Exception {
            ww3 a = f.this.e.a();
            a.q0(1, this.a);
            String str = this.b;
            if (str == null) {
                a.Y0(2);
            } else {
                a.L(2, str);
            }
            f.this.a.e();
            try {
                a.T();
                f.this.a.E();
                return te4.a;
            } finally {
                f.this.a.j();
                f.this.e.f(a);
            }
        }
    }

    /* compiled from: ReflectionsDao_Impl.java */
    /* loaded from: classes2.dex */
    public class e implements Callable<te4> {
        public final /* synthetic */ double a;
        public final /* synthetic */ String b;

        public e(double d, String str) {
            this.a = d;
            this.b = str;
        }

        @Override // java.util.concurrent.Callable
        /* renamed from: a */
        public te4 call() throws Exception {
            ww3 a = f.this.f.a();
            a.Y(1, this.a);
            String str = this.b;
            if (str == null) {
                a.Y0(2);
            } else {
                a.L(2, str);
            }
            f.this.a.e();
            try {
                a.T();
                f.this.a.E();
                return te4.a;
            } finally {
                f.this.a.j();
                f.this.f.f(a);
            }
        }
    }

    /* compiled from: ReflectionsDao_Impl.java */
    /* renamed from: net.safemoon.androidwallet.database.room.f$f  reason: collision with other inner class name */
    /* loaded from: classes2.dex */
    public class CallableC0208f implements Callable<te4> {
        public final /* synthetic */ long a;
        public final /* synthetic */ long b;
        public final /* synthetic */ String c;

        public CallableC0208f(long j, long j2, String str) {
            this.a = j;
            this.b = j2;
            this.c = str;
        }

        @Override // java.util.concurrent.Callable
        /* renamed from: a */
        public te4 call() throws Exception {
            ww3 a = f.this.g.a();
            a.q0(1, this.a);
            a.q0(2, this.b);
            String str = this.c;
            if (str == null) {
                a.Y0(3);
            } else {
                a.L(3, str);
            }
            f.this.a.e();
            try {
                a.T();
                f.this.a.E();
                return te4.a;
            } finally {
                f.this.a.j();
                f.this.g.f(a);
            }
        }
    }

    /* compiled from: ReflectionsDao_Impl.java */
    /* loaded from: classes2.dex */
    public class g implements Callable<te4> {
        public final /* synthetic */ int a;
        public final /* synthetic */ long b;

        public g(int i, long j) {
            this.a = i;
            this.b = j;
        }

        @Override // java.util.concurrent.Callable
        /* renamed from: a */
        public te4 call() throws Exception {
            ww3 a = f.this.h.a();
            a.q0(1, this.a);
            a.q0(2, this.b);
            f.this.a.e();
            try {
                a.T();
                f.this.a.E();
                return te4.a;
            } finally {
                f.this.a.j();
                f.this.h.f(a);
            }
        }
    }

    /* compiled from: ReflectionsDao_Impl.java */
    /* loaded from: classes2.dex */
    public class h implements Callable<te4> {
        public h() {
        }

        @Override // java.util.concurrent.Callable
        /* renamed from: a */
        public te4 call() throws Exception {
            ww3 a = f.this.i.a();
            f.this.a.e();
            try {
                a.T();
                f.this.a.E();
                return te4.a;
            } finally {
                f.this.a.j();
                f.this.i.f(a);
            }
        }
    }

    /* compiled from: ReflectionsDao_Impl.java */
    /* loaded from: classes2.dex */
    public class i implements Callable<te4> {
        public final /* synthetic */ String a;

        public i(String str) {
            this.a = str;
        }

        @Override // java.util.concurrent.Callable
        /* renamed from: a */
        public te4 call() throws Exception {
            ww3 a = f.this.j.a();
            String str = this.a;
            if (str == null) {
                a.Y0(1);
            } else {
                a.L(1, str);
            }
            f.this.a.e();
            try {
                a.T();
                f.this.a.E();
                return te4.a;
            } finally {
                f.this.a.j();
                f.this.j.f(a);
            }
        }
    }

    /* compiled from: ReflectionsDao_Impl.java */
    /* loaded from: classes2.dex */
    public class j implements Callable<Boolean> {
        public final /* synthetic */ k93 a;

        public j(k93 k93Var) {
            this.a = k93Var;
        }

        @Override // java.util.concurrent.Callable
        /* renamed from: a */
        public Boolean call() throws Exception {
            Boolean bool = null;
            Cursor c = id0.c(f.this.a, this.a, false, null);
            try {
                if (c.moveToFirst()) {
                    Integer valueOf = c.isNull(0) ? null : Integer.valueOf(c.getInt(0));
                    if (valueOf != null) {
                        bool = Boolean.valueOf(valueOf.intValue() != 0);
                    }
                }
                return bool;
            } finally {
                c.close();
                this.a.f();
            }
        }
    }

    /* compiled from: ReflectionsDao_Impl.java */
    /* loaded from: classes2.dex */
    public class k extends zv0<RoomReflectionsToken> {
        public k(f fVar, RoomDatabase roomDatabase) {
            super(roomDatabase);
        }

        @Override // defpackage.co3
        public String d() {
            return "INSERT OR ABORT INTO `table_room_reflections_token_2` (`_id`,`symbolWithType`,`symbol`,`name`,`iconPath`,`contractorAddress`,`tokenTypeChain`,`decimals`,`nativeBalance`,`firstTimeStamp`,`enableAdvanceMode`,`latestBalance`,`latestTimeStamp`,`cmcId`,`priceUsd`) VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
        }

        @Override // defpackage.zv0
        /* renamed from: k */
        public void g(ww3 ww3Var, RoomReflectionsToken roomReflectionsToken) {
            if (roomReflectionsToken.getId() == null) {
                ww3Var.Y0(1);
            } else {
                ww3Var.q0(1, roomReflectionsToken.getId().longValue());
            }
            if (roomReflectionsToken.getSymbolWithType() == null) {
                ww3Var.Y0(2);
            } else {
                ww3Var.L(2, roomReflectionsToken.getSymbolWithType());
            }
            if (roomReflectionsToken.getSymbol() == null) {
                ww3Var.Y0(3);
            } else {
                ww3Var.L(3, roomReflectionsToken.getSymbol());
            }
            if (roomReflectionsToken.getName() == null) {
                ww3Var.Y0(4);
            } else {
                ww3Var.L(4, roomReflectionsToken.getName());
            }
            if (roomReflectionsToken.getIconResName() == null) {
                ww3Var.Y0(5);
            } else {
                ww3Var.L(5, roomReflectionsToken.getIconResName());
            }
            if (roomReflectionsToken.getContractAddress() == null) {
                ww3Var.Y0(6);
            } else {
                ww3Var.L(6, roomReflectionsToken.getContractAddress());
            }
            ww3Var.q0(7, roomReflectionsToken.getChainId());
            ww3Var.q0(8, roomReflectionsToken.getDecimals());
            if (roomReflectionsToken.getNativeBalance() == null) {
                ww3Var.Y0(9);
            } else {
                ww3Var.L(9, roomReflectionsToken.getNativeBalance());
            }
            if (roomReflectionsToken.getFirstTimeStamp() == null) {
                ww3Var.Y0(10);
            } else {
                ww3Var.q0(10, roomReflectionsToken.getFirstTimeStamp().longValue());
            }
            ww3Var.q0(11, roomReflectionsToken.getEnableAdvanceMode() ? 1L : 0L);
            if (roomReflectionsToken.getLatestBalance() == null) {
                ww3Var.Y0(12);
            } else {
                ww3Var.q0(12, roomReflectionsToken.getLatestBalance().longValue());
            }
            if (roomReflectionsToken.getLatestTimeStamp() == null) {
                ww3Var.Y0(13);
            } else {
                ww3Var.q0(13, roomReflectionsToken.getLatestTimeStamp().longValue());
            }
            if (roomReflectionsToken.getCmcId() == null) {
                ww3Var.Y0(14);
            } else {
                ww3Var.q0(14, roomReflectionsToken.getCmcId().longValue());
            }
            if (roomReflectionsToken.getPriceUsd() == null) {
                ww3Var.Y0(15);
            } else {
                ww3Var.Y(15, roomReflectionsToken.getPriceUsd().doubleValue());
            }
        }
    }

    /* compiled from: ReflectionsDao_Impl.java */
    /* loaded from: classes2.dex */
    public class l implements Callable<Boolean> {
        public final /* synthetic */ k93 a;

        public l(k93 k93Var) {
            this.a = k93Var;
        }

        @Override // java.util.concurrent.Callable
        /* renamed from: a */
        public Boolean call() throws Exception {
            Boolean bool = null;
            Cursor c = id0.c(f.this.a, this.a, false, null);
            try {
                if (c.moveToFirst()) {
                    Integer valueOf = c.isNull(0) ? null : Integer.valueOf(c.getInt(0));
                    if (valueOf != null) {
                        bool = Boolean.valueOf(valueOf.intValue() != 0);
                    }
                }
                return bool;
            } finally {
                c.close();
                this.a.f();
            }
        }
    }

    /* compiled from: ReflectionsDao_Impl.java */
    /* loaded from: classes2.dex */
    public class m implements Callable<List<RoomReflectionsToken>> {
        public final /* synthetic */ k93 a;

        public m(k93 k93Var) {
            this.a = k93Var;
        }

        @Override // java.util.concurrent.Callable
        /* renamed from: a */
        public List<RoomReflectionsToken> call() throws Exception {
            Long valueOf;
            int i;
            Double valueOf2;
            int i2;
            Cursor c = id0.c(f.this.a, this.a, false, null);
            try {
                int e = wb0.e(c, "_id");
                int e2 = wb0.e(c, "symbolWithType");
                int e3 = wb0.e(c, "symbol");
                int e4 = wb0.e(c, PublicResolver.FUNC_NAME);
                int e5 = wb0.e(c, "iconPath");
                int e6 = wb0.e(c, "contractorAddress");
                int e7 = wb0.e(c, "tokenTypeChain");
                int e8 = wb0.e(c, "decimals");
                int e9 = wb0.e(c, "nativeBalance");
                int e10 = wb0.e(c, "firstTimeStamp");
                int e11 = wb0.e(c, "enableAdvanceMode");
                int e12 = wb0.e(c, "latestBalance");
                int e13 = wb0.e(c, "latestTimeStamp");
                int e14 = wb0.e(c, "cmcId");
                int e15 = wb0.e(c, "priceUsd");
                int i3 = e14;
                ArrayList arrayList = new ArrayList(c.getCount());
                while (c.moveToNext()) {
                    Long valueOf3 = c.isNull(e) ? null : Long.valueOf(c.getLong(e));
                    String string = c.isNull(e2) ? null : c.getString(e2);
                    String string2 = c.isNull(e3) ? null : c.getString(e3);
                    String string3 = c.isNull(e4) ? null : c.getString(e4);
                    String string4 = c.isNull(e5) ? null : c.getString(e5);
                    String string5 = c.isNull(e6) ? null : c.getString(e6);
                    int i4 = c.getInt(e7);
                    int i5 = c.getInt(e8);
                    String string6 = c.isNull(e9) ? null : c.getString(e9);
                    Long valueOf4 = c.isNull(e10) ? null : Long.valueOf(c.getLong(e10));
                    boolean z = c.getInt(e11) != 0;
                    Long valueOf5 = c.isNull(e12) ? null : Long.valueOf(c.getLong(e12));
                    if (c.isNull(e13)) {
                        i = i3;
                        valueOf = null;
                    } else {
                        valueOf = Long.valueOf(c.getLong(e13));
                        i = i3;
                    }
                    Long valueOf6 = c.isNull(i) ? null : Long.valueOf(c.getLong(i));
                    int i6 = e15;
                    int i7 = e;
                    if (c.isNull(i6)) {
                        i2 = i6;
                        valueOf2 = null;
                    } else {
                        valueOf2 = Double.valueOf(c.getDouble(i6));
                        i2 = i6;
                    }
                    arrayList.add(new RoomReflectionsToken(valueOf3, string, string2, string3, string4, string5, i4, i5, string6, valueOf4, z, valueOf5, valueOf, valueOf6, valueOf2));
                    e = i7;
                    e15 = i2;
                    i3 = i;
                }
                return arrayList;
            } finally {
                c.close();
            }
        }

        public void finalize() {
            this.a.f();
        }
    }

    /* compiled from: ReflectionsDao_Impl.java */
    /* loaded from: classes2.dex */
    public class n implements Callable<List<RoomReflectionsTokenAndData>> {
        public final /* synthetic */ k93 a;

        public n(k93 k93Var) {
            this.a = k93Var;
        }

        /* JADX WARN: Removed duplicated region for block: B:101:0x020f  */
        /* JADX WARN: Removed duplicated region for block: B:102:0x0212 A[Catch: all -> 0x025b, TryCatch #1 {all -> 0x025b, blocks: (B:15:0x00be, B:17:0x00c4, B:19:0x00ca, B:21:0x00d0, B:23:0x00d6, B:25:0x00dc, B:27:0x00e2, B:29:0x00e8, B:31:0x00ee, B:33:0x00f4, B:35:0x00fa, B:37:0x0100, B:39:0x0108, B:41:0x0110, B:43:0x011a, B:51:0x013b, B:55:0x014e, B:59:0x015d, B:63:0x016c, B:67:0x017b, B:71:0x018a, B:75:0x0199, B:79:0x01b0, B:83:0x01c3, B:87:0x01d0, B:91:0x01e3, B:95:0x01f6, B:99:0x0209, B:103:0x021c, B:104:0x0229, B:106:0x0239, B:107:0x023e, B:102:0x0212, B:98:0x01ff, B:94:0x01ec, B:90:0x01d9, B:82:0x01b9, B:78:0x01aa, B:74:0x0193, B:70:0x0184, B:66:0x0175, B:62:0x0166, B:58:0x0157, B:54:0x0144), top: B:124:0x00be }] */
        /* JADX WARN: Removed duplicated region for block: B:106:0x0239 A[Catch: all -> 0x025b, TryCatch #1 {all -> 0x025b, blocks: (B:15:0x00be, B:17:0x00c4, B:19:0x00ca, B:21:0x00d0, B:23:0x00d6, B:25:0x00dc, B:27:0x00e2, B:29:0x00e8, B:31:0x00ee, B:33:0x00f4, B:35:0x00fa, B:37:0x0100, B:39:0x0108, B:41:0x0110, B:43:0x011a, B:51:0x013b, B:55:0x014e, B:59:0x015d, B:63:0x016c, B:67:0x017b, B:71:0x018a, B:75:0x0199, B:79:0x01b0, B:83:0x01c3, B:87:0x01d0, B:91:0x01e3, B:95:0x01f6, B:99:0x0209, B:103:0x021c, B:104:0x0229, B:106:0x0239, B:107:0x023e, B:102:0x0212, B:98:0x01ff, B:94:0x01ec, B:90:0x01d9, B:82:0x01b9, B:78:0x01aa, B:74:0x0193, B:70:0x0184, B:66:0x0175, B:62:0x0166, B:58:0x0157, B:54:0x0144), top: B:124:0x00be }] */
        /* JADX WARN: Removed duplicated region for block: B:53:0x0141  */
        /* JADX WARN: Removed duplicated region for block: B:54:0x0144 A[Catch: all -> 0x025b, TryCatch #1 {all -> 0x025b, blocks: (B:15:0x00be, B:17:0x00c4, B:19:0x00ca, B:21:0x00d0, B:23:0x00d6, B:25:0x00dc, B:27:0x00e2, B:29:0x00e8, B:31:0x00ee, B:33:0x00f4, B:35:0x00fa, B:37:0x0100, B:39:0x0108, B:41:0x0110, B:43:0x011a, B:51:0x013b, B:55:0x014e, B:59:0x015d, B:63:0x016c, B:67:0x017b, B:71:0x018a, B:75:0x0199, B:79:0x01b0, B:83:0x01c3, B:87:0x01d0, B:91:0x01e3, B:95:0x01f6, B:99:0x0209, B:103:0x021c, B:104:0x0229, B:106:0x0239, B:107:0x023e, B:102:0x0212, B:98:0x01ff, B:94:0x01ec, B:90:0x01d9, B:82:0x01b9, B:78:0x01aa, B:74:0x0193, B:70:0x0184, B:66:0x0175, B:62:0x0166, B:58:0x0157, B:54:0x0144), top: B:124:0x00be }] */
        /* JADX WARN: Removed duplicated region for block: B:57:0x0154  */
        /* JADX WARN: Removed duplicated region for block: B:58:0x0157 A[Catch: all -> 0x025b, TryCatch #1 {all -> 0x025b, blocks: (B:15:0x00be, B:17:0x00c4, B:19:0x00ca, B:21:0x00d0, B:23:0x00d6, B:25:0x00dc, B:27:0x00e2, B:29:0x00e8, B:31:0x00ee, B:33:0x00f4, B:35:0x00fa, B:37:0x0100, B:39:0x0108, B:41:0x0110, B:43:0x011a, B:51:0x013b, B:55:0x014e, B:59:0x015d, B:63:0x016c, B:67:0x017b, B:71:0x018a, B:75:0x0199, B:79:0x01b0, B:83:0x01c3, B:87:0x01d0, B:91:0x01e3, B:95:0x01f6, B:99:0x0209, B:103:0x021c, B:104:0x0229, B:106:0x0239, B:107:0x023e, B:102:0x0212, B:98:0x01ff, B:94:0x01ec, B:90:0x01d9, B:82:0x01b9, B:78:0x01aa, B:74:0x0193, B:70:0x0184, B:66:0x0175, B:62:0x0166, B:58:0x0157, B:54:0x0144), top: B:124:0x00be }] */
        /* JADX WARN: Removed duplicated region for block: B:61:0x0163  */
        /* JADX WARN: Removed duplicated region for block: B:62:0x0166 A[Catch: all -> 0x025b, TryCatch #1 {all -> 0x025b, blocks: (B:15:0x00be, B:17:0x00c4, B:19:0x00ca, B:21:0x00d0, B:23:0x00d6, B:25:0x00dc, B:27:0x00e2, B:29:0x00e8, B:31:0x00ee, B:33:0x00f4, B:35:0x00fa, B:37:0x0100, B:39:0x0108, B:41:0x0110, B:43:0x011a, B:51:0x013b, B:55:0x014e, B:59:0x015d, B:63:0x016c, B:67:0x017b, B:71:0x018a, B:75:0x0199, B:79:0x01b0, B:83:0x01c3, B:87:0x01d0, B:91:0x01e3, B:95:0x01f6, B:99:0x0209, B:103:0x021c, B:104:0x0229, B:106:0x0239, B:107:0x023e, B:102:0x0212, B:98:0x01ff, B:94:0x01ec, B:90:0x01d9, B:82:0x01b9, B:78:0x01aa, B:74:0x0193, B:70:0x0184, B:66:0x0175, B:62:0x0166, B:58:0x0157, B:54:0x0144), top: B:124:0x00be }] */
        /* JADX WARN: Removed duplicated region for block: B:65:0x0172  */
        /* JADX WARN: Removed duplicated region for block: B:66:0x0175 A[Catch: all -> 0x025b, TryCatch #1 {all -> 0x025b, blocks: (B:15:0x00be, B:17:0x00c4, B:19:0x00ca, B:21:0x00d0, B:23:0x00d6, B:25:0x00dc, B:27:0x00e2, B:29:0x00e8, B:31:0x00ee, B:33:0x00f4, B:35:0x00fa, B:37:0x0100, B:39:0x0108, B:41:0x0110, B:43:0x011a, B:51:0x013b, B:55:0x014e, B:59:0x015d, B:63:0x016c, B:67:0x017b, B:71:0x018a, B:75:0x0199, B:79:0x01b0, B:83:0x01c3, B:87:0x01d0, B:91:0x01e3, B:95:0x01f6, B:99:0x0209, B:103:0x021c, B:104:0x0229, B:106:0x0239, B:107:0x023e, B:102:0x0212, B:98:0x01ff, B:94:0x01ec, B:90:0x01d9, B:82:0x01b9, B:78:0x01aa, B:74:0x0193, B:70:0x0184, B:66:0x0175, B:62:0x0166, B:58:0x0157, B:54:0x0144), top: B:124:0x00be }] */
        /* JADX WARN: Removed duplicated region for block: B:69:0x0181  */
        /* JADX WARN: Removed duplicated region for block: B:70:0x0184 A[Catch: all -> 0x025b, TryCatch #1 {all -> 0x025b, blocks: (B:15:0x00be, B:17:0x00c4, B:19:0x00ca, B:21:0x00d0, B:23:0x00d6, B:25:0x00dc, B:27:0x00e2, B:29:0x00e8, B:31:0x00ee, B:33:0x00f4, B:35:0x00fa, B:37:0x0100, B:39:0x0108, B:41:0x0110, B:43:0x011a, B:51:0x013b, B:55:0x014e, B:59:0x015d, B:63:0x016c, B:67:0x017b, B:71:0x018a, B:75:0x0199, B:79:0x01b0, B:83:0x01c3, B:87:0x01d0, B:91:0x01e3, B:95:0x01f6, B:99:0x0209, B:103:0x021c, B:104:0x0229, B:106:0x0239, B:107:0x023e, B:102:0x0212, B:98:0x01ff, B:94:0x01ec, B:90:0x01d9, B:82:0x01b9, B:78:0x01aa, B:74:0x0193, B:70:0x0184, B:66:0x0175, B:62:0x0166, B:58:0x0157, B:54:0x0144), top: B:124:0x00be }] */
        /* JADX WARN: Removed duplicated region for block: B:73:0x0190  */
        /* JADX WARN: Removed duplicated region for block: B:74:0x0193 A[Catch: all -> 0x025b, TryCatch #1 {all -> 0x025b, blocks: (B:15:0x00be, B:17:0x00c4, B:19:0x00ca, B:21:0x00d0, B:23:0x00d6, B:25:0x00dc, B:27:0x00e2, B:29:0x00e8, B:31:0x00ee, B:33:0x00f4, B:35:0x00fa, B:37:0x0100, B:39:0x0108, B:41:0x0110, B:43:0x011a, B:51:0x013b, B:55:0x014e, B:59:0x015d, B:63:0x016c, B:67:0x017b, B:71:0x018a, B:75:0x0199, B:79:0x01b0, B:83:0x01c3, B:87:0x01d0, B:91:0x01e3, B:95:0x01f6, B:99:0x0209, B:103:0x021c, B:104:0x0229, B:106:0x0239, B:107:0x023e, B:102:0x0212, B:98:0x01ff, B:94:0x01ec, B:90:0x01d9, B:82:0x01b9, B:78:0x01aa, B:74:0x0193, B:70:0x0184, B:66:0x0175, B:62:0x0166, B:58:0x0157, B:54:0x0144), top: B:124:0x00be }] */
        /* JADX WARN: Removed duplicated region for block: B:77:0x01a7  */
        /* JADX WARN: Removed duplicated region for block: B:78:0x01aa A[Catch: all -> 0x025b, TryCatch #1 {all -> 0x025b, blocks: (B:15:0x00be, B:17:0x00c4, B:19:0x00ca, B:21:0x00d0, B:23:0x00d6, B:25:0x00dc, B:27:0x00e2, B:29:0x00e8, B:31:0x00ee, B:33:0x00f4, B:35:0x00fa, B:37:0x0100, B:39:0x0108, B:41:0x0110, B:43:0x011a, B:51:0x013b, B:55:0x014e, B:59:0x015d, B:63:0x016c, B:67:0x017b, B:71:0x018a, B:75:0x0199, B:79:0x01b0, B:83:0x01c3, B:87:0x01d0, B:91:0x01e3, B:95:0x01f6, B:99:0x0209, B:103:0x021c, B:104:0x0229, B:106:0x0239, B:107:0x023e, B:102:0x0212, B:98:0x01ff, B:94:0x01ec, B:90:0x01d9, B:82:0x01b9, B:78:0x01aa, B:74:0x0193, B:70:0x0184, B:66:0x0175, B:62:0x0166, B:58:0x0157, B:54:0x0144), top: B:124:0x00be }] */
        /* JADX WARN: Removed duplicated region for block: B:81:0x01b6  */
        /* JADX WARN: Removed duplicated region for block: B:82:0x01b9 A[Catch: all -> 0x025b, TryCatch #1 {all -> 0x025b, blocks: (B:15:0x00be, B:17:0x00c4, B:19:0x00ca, B:21:0x00d0, B:23:0x00d6, B:25:0x00dc, B:27:0x00e2, B:29:0x00e8, B:31:0x00ee, B:33:0x00f4, B:35:0x00fa, B:37:0x0100, B:39:0x0108, B:41:0x0110, B:43:0x011a, B:51:0x013b, B:55:0x014e, B:59:0x015d, B:63:0x016c, B:67:0x017b, B:71:0x018a, B:75:0x0199, B:79:0x01b0, B:83:0x01c3, B:87:0x01d0, B:91:0x01e3, B:95:0x01f6, B:99:0x0209, B:103:0x021c, B:104:0x0229, B:106:0x0239, B:107:0x023e, B:102:0x0212, B:98:0x01ff, B:94:0x01ec, B:90:0x01d9, B:82:0x01b9, B:78:0x01aa, B:74:0x0193, B:70:0x0184, B:66:0x0175, B:62:0x0166, B:58:0x0157, B:54:0x0144), top: B:124:0x00be }] */
        /* JADX WARN: Removed duplicated region for block: B:85:0x01c9  */
        /* JADX WARN: Removed duplicated region for block: B:86:0x01cc  */
        /* JADX WARN: Removed duplicated region for block: B:89:0x01d6  */
        /* JADX WARN: Removed duplicated region for block: B:90:0x01d9 A[Catch: all -> 0x025b, TryCatch #1 {all -> 0x025b, blocks: (B:15:0x00be, B:17:0x00c4, B:19:0x00ca, B:21:0x00d0, B:23:0x00d6, B:25:0x00dc, B:27:0x00e2, B:29:0x00e8, B:31:0x00ee, B:33:0x00f4, B:35:0x00fa, B:37:0x0100, B:39:0x0108, B:41:0x0110, B:43:0x011a, B:51:0x013b, B:55:0x014e, B:59:0x015d, B:63:0x016c, B:67:0x017b, B:71:0x018a, B:75:0x0199, B:79:0x01b0, B:83:0x01c3, B:87:0x01d0, B:91:0x01e3, B:95:0x01f6, B:99:0x0209, B:103:0x021c, B:104:0x0229, B:106:0x0239, B:107:0x023e, B:102:0x0212, B:98:0x01ff, B:94:0x01ec, B:90:0x01d9, B:82:0x01b9, B:78:0x01aa, B:74:0x0193, B:70:0x0184, B:66:0x0175, B:62:0x0166, B:58:0x0157, B:54:0x0144), top: B:124:0x00be }] */
        /* JADX WARN: Removed duplicated region for block: B:93:0x01e9  */
        /* JADX WARN: Removed duplicated region for block: B:94:0x01ec A[Catch: all -> 0x025b, TryCatch #1 {all -> 0x025b, blocks: (B:15:0x00be, B:17:0x00c4, B:19:0x00ca, B:21:0x00d0, B:23:0x00d6, B:25:0x00dc, B:27:0x00e2, B:29:0x00e8, B:31:0x00ee, B:33:0x00f4, B:35:0x00fa, B:37:0x0100, B:39:0x0108, B:41:0x0110, B:43:0x011a, B:51:0x013b, B:55:0x014e, B:59:0x015d, B:63:0x016c, B:67:0x017b, B:71:0x018a, B:75:0x0199, B:79:0x01b0, B:83:0x01c3, B:87:0x01d0, B:91:0x01e3, B:95:0x01f6, B:99:0x0209, B:103:0x021c, B:104:0x0229, B:106:0x0239, B:107:0x023e, B:102:0x0212, B:98:0x01ff, B:94:0x01ec, B:90:0x01d9, B:82:0x01b9, B:78:0x01aa, B:74:0x0193, B:70:0x0184, B:66:0x0175, B:62:0x0166, B:58:0x0157, B:54:0x0144), top: B:124:0x00be }] */
        /* JADX WARN: Removed duplicated region for block: B:97:0x01fc  */
        /* JADX WARN: Removed duplicated region for block: B:98:0x01ff A[Catch: all -> 0x025b, TryCatch #1 {all -> 0x025b, blocks: (B:15:0x00be, B:17:0x00c4, B:19:0x00ca, B:21:0x00d0, B:23:0x00d6, B:25:0x00dc, B:27:0x00e2, B:29:0x00e8, B:31:0x00ee, B:33:0x00f4, B:35:0x00fa, B:37:0x0100, B:39:0x0108, B:41:0x0110, B:43:0x011a, B:51:0x013b, B:55:0x014e, B:59:0x015d, B:63:0x016c, B:67:0x017b, B:71:0x018a, B:75:0x0199, B:79:0x01b0, B:83:0x01c3, B:87:0x01d0, B:91:0x01e3, B:95:0x01f6, B:99:0x0209, B:103:0x021c, B:104:0x0229, B:106:0x0239, B:107:0x023e, B:102:0x0212, B:98:0x01ff, B:94:0x01ec, B:90:0x01d9, B:82:0x01b9, B:78:0x01aa, B:74:0x0193, B:70:0x0184, B:66:0x0175, B:62:0x0166, B:58:0x0157, B:54:0x0144), top: B:124:0x00be }] */
        @Override // java.util.concurrent.Callable
        /* renamed from: a */
        /*
            Code decompiled incorrectly, please refer to instructions dump.
            To view partially-correct code enable 'Show inconsistent code' option in preferences
        */
        public java.util.List<net.safemoon.androidwallet.model.reflections.RoomReflectionsTokenAndData> call() throws java.lang.Exception {
            /*
                Method dump skipped, instructions count: 646
                To view this dump change 'Code comments level' option to 'DEBUG'
            */
            throw new UnsupportedOperationException("Method not decompiled: net.safemoon.androidwallet.database.room.f.n.call():java.util.List");
        }

        public void finalize() {
            this.a.f();
        }
    }

    /* compiled from: ReflectionsDao_Impl.java */
    /* loaded from: classes2.dex */
    public class o implements Callable<List<RoomReflectionsToken>> {
        public final /* synthetic */ k93 a;

        public o(k93 k93Var) {
            this.a = k93Var;
        }

        @Override // java.util.concurrent.Callable
        /* renamed from: a */
        public List<RoomReflectionsToken> call() throws Exception {
            o oVar;
            int e;
            int e2;
            int e3;
            int e4;
            int e5;
            int e6;
            int e7;
            int e8;
            int e9;
            int e10;
            int e11;
            int e12;
            int e13;
            int e14;
            Long valueOf;
            int i;
            Double valueOf2;
            int i2;
            Cursor c = id0.c(f.this.a, this.a, false, null);
            try {
                e = wb0.e(c, "_id");
                e2 = wb0.e(c, "symbolWithType");
                e3 = wb0.e(c, "symbol");
                e4 = wb0.e(c, PublicResolver.FUNC_NAME);
                e5 = wb0.e(c, "iconPath");
                e6 = wb0.e(c, "contractorAddress");
                e7 = wb0.e(c, "tokenTypeChain");
                e8 = wb0.e(c, "decimals");
                e9 = wb0.e(c, "nativeBalance");
                e10 = wb0.e(c, "firstTimeStamp");
                e11 = wb0.e(c, "enableAdvanceMode");
                e12 = wb0.e(c, "latestBalance");
                e13 = wb0.e(c, "latestTimeStamp");
                e14 = wb0.e(c, "cmcId");
            } catch (Throwable th) {
                th = th;
                oVar = this;
            }
            try {
                int e15 = wb0.e(c, "priceUsd");
                int i3 = e14;
                ArrayList arrayList = new ArrayList(c.getCount());
                while (c.moveToNext()) {
                    Long valueOf3 = c.isNull(e) ? null : Long.valueOf(c.getLong(e));
                    String string = c.isNull(e2) ? null : c.getString(e2);
                    String string2 = c.isNull(e3) ? null : c.getString(e3);
                    String string3 = c.isNull(e4) ? null : c.getString(e4);
                    String string4 = c.isNull(e5) ? null : c.getString(e5);
                    String string5 = c.isNull(e6) ? null : c.getString(e6);
                    int i4 = c.getInt(e7);
                    int i5 = c.getInt(e8);
                    String string6 = c.isNull(e9) ? null : c.getString(e9);
                    Long valueOf4 = c.isNull(e10) ? null : Long.valueOf(c.getLong(e10));
                    boolean z = c.getInt(e11) != 0;
                    Long valueOf5 = c.isNull(e12) ? null : Long.valueOf(c.getLong(e12));
                    if (c.isNull(e13)) {
                        i = i3;
                        valueOf = null;
                    } else {
                        valueOf = Long.valueOf(c.getLong(e13));
                        i = i3;
                    }
                    Long valueOf6 = c.isNull(i) ? null : Long.valueOf(c.getLong(i));
                    int i6 = e15;
                    int i7 = e;
                    if (c.isNull(i6)) {
                        i2 = i6;
                        valueOf2 = null;
                    } else {
                        valueOf2 = Double.valueOf(c.getDouble(i6));
                        i2 = i6;
                    }
                    arrayList.add(new RoomReflectionsToken(valueOf3, string, string2, string3, string4, string5, i4, i5, string6, valueOf4, z, valueOf5, valueOf, valueOf6, valueOf2));
                    e = i7;
                    e15 = i2;
                    i3 = i;
                }
                c.close();
                this.a.f();
                return arrayList;
            } catch (Throwable th2) {
                th = th2;
                oVar = this;
                c.close();
                oVar.a.f();
                throw th;
            }
        }
    }

    /* compiled from: ReflectionsDao_Impl.java */
    /* loaded from: classes2.dex */
    public class p implements Callable<List<RoomReflectionsDataAndToken>> {
        public final /* synthetic */ k93 a;

        public p(k93 k93Var) {
            this.a = k93Var;
        }

        @Override // java.util.concurrent.Callable
        /* renamed from: a */
        public List<RoomReflectionsDataAndToken> call() throws Exception {
            RoomReflectionsData roomReflectionsData;
            f.this.a.e();
            try {
                Cursor c = id0.c(f.this.a, this.a, true, null);
                int e = wb0.e(c, "_id");
                int e2 = wb0.e(c, "symbolWithType");
                int e3 = wb0.e(c, "nativeBalance");
                int e4 = wb0.e(c, "blockBalance");
                int e5 = wb0.e(c, "block");
                int e6 = wb0.e(c, "timeStamp");
                rh rhVar = new rh();
                while (c.moveToNext()) {
                    rhVar.put(c.getString(e2), null);
                }
                c.moveToPosition(-1);
                f.this.u(rhVar);
                ArrayList arrayList = new ArrayList(c.getCount());
                while (c.moveToNext()) {
                    if (c.isNull(e) && c.isNull(e2) && c.isNull(e3) && c.isNull(e4) && c.isNull(e5) && c.isNull(e6)) {
                        roomReflectionsData = null;
                        arrayList.add(new RoomReflectionsDataAndToken(roomReflectionsData, (RoomReflectionsToken) rhVar.get(c.getString(e2))));
                    }
                    roomReflectionsData = new RoomReflectionsData(c.isNull(e) ? null : Long.valueOf(c.getLong(e)), c.isNull(e2) ? null : c.getString(e2), c.isNull(e3) ? null : c.getString(e3), c.isNull(e4) ? null : c.getString(e4), c.isNull(e5) ? null : c.getString(e5), c.getLong(e6));
                    arrayList.add(new RoomReflectionsDataAndToken(roomReflectionsData, (RoomReflectionsToken) rhVar.get(c.getString(e2))));
                }
                f.this.a.E();
                c.close();
                return arrayList;
            } finally {
                f.this.a.j();
            }
        }

        public void finalize() {
            this.a.f();
        }
    }

    /* compiled from: ReflectionsDao_Impl.java */
    /* loaded from: classes2.dex */
    public class q implements Callable<List<RoomReflectionsDataAndToken>> {
        public final /* synthetic */ k93 a;

        public q(k93 k93Var) {
            this.a = k93Var;
        }

        @Override // java.util.concurrent.Callable
        /* renamed from: a */
        public List<RoomReflectionsDataAndToken> call() throws Exception {
            RoomReflectionsData roomReflectionsData;
            f.this.a.e();
            try {
                Cursor c = id0.c(f.this.a, this.a, true, null);
                int e = wb0.e(c, "_id");
                int e2 = wb0.e(c, "symbolWithType");
                int e3 = wb0.e(c, "nativeBalance");
                int e4 = wb0.e(c, "blockBalance");
                int e5 = wb0.e(c, "block");
                int e6 = wb0.e(c, "timeStamp");
                rh rhVar = new rh();
                while (c.moveToNext()) {
                    rhVar.put(c.getString(e2), null);
                }
                c.moveToPosition(-1);
                f.this.u(rhVar);
                ArrayList arrayList = new ArrayList(c.getCount());
                while (c.moveToNext()) {
                    if (c.isNull(e) && c.isNull(e2) && c.isNull(e3) && c.isNull(e4) && c.isNull(e5) && c.isNull(e6)) {
                        roomReflectionsData = null;
                        arrayList.add(new RoomReflectionsDataAndToken(roomReflectionsData, (RoomReflectionsToken) rhVar.get(c.getString(e2))));
                    }
                    roomReflectionsData = new RoomReflectionsData(c.isNull(e) ? null : Long.valueOf(c.getLong(e)), c.isNull(e2) ? null : c.getString(e2), c.isNull(e3) ? null : c.getString(e3), c.isNull(e4) ? null : c.getString(e4), c.isNull(e5) ? null : c.getString(e5), c.getLong(e6));
                    arrayList.add(new RoomReflectionsDataAndToken(roomReflectionsData, (RoomReflectionsToken) rhVar.get(c.getString(e2))));
                }
                f.this.a.E();
                c.close();
                return arrayList;
            } finally {
                f.this.a.j();
            }
        }

        public void finalize() {
            this.a.f();
        }
    }

    /* compiled from: ReflectionsDao_Impl.java */
    /* loaded from: classes2.dex */
    public class r implements Callable<RoomReflectionsData> {
        public final /* synthetic */ k93 a;

        public r(k93 k93Var) {
            this.a = k93Var;
        }

        @Override // java.util.concurrent.Callable
        /* renamed from: a */
        public RoomReflectionsData call() throws Exception {
            RoomReflectionsData roomReflectionsData = null;
            Cursor c = id0.c(f.this.a, this.a, false, null);
            try {
                int e = wb0.e(c, "_id");
                int e2 = wb0.e(c, "symbolWithType");
                int e3 = wb0.e(c, "nativeBalance");
                int e4 = wb0.e(c, "blockBalance");
                int e5 = wb0.e(c, "block");
                int e6 = wb0.e(c, "timeStamp");
                if (c.moveToFirst()) {
                    roomReflectionsData = new RoomReflectionsData(c.isNull(e) ? null : Long.valueOf(c.getLong(e)), c.isNull(e2) ? null : c.getString(e2), c.isNull(e3) ? null : c.getString(e3), c.isNull(e4) ? null : c.getString(e4), c.isNull(e5) ? null : c.getString(e5), c.getLong(e6));
                }
                return roomReflectionsData;
            } finally {
                c.close();
                this.a.f();
            }
        }
    }

    /* compiled from: ReflectionsDao_Impl.java */
    /* loaded from: classes2.dex */
    public class s implements Callable<RoomReflectionsTokenAndData> {
        public final /* synthetic */ k93 a;

        public s(k93 k93Var) {
            this.a = k93Var;
        }

        /* JADX WARN: Removed duplicated region for block: B:100:0x01fc A[Catch: all -> 0x022c, TryCatch #0 {all -> 0x022c, blocks: (B:3:0x0010, B:4:0x0073, B:6:0x0079, B:8:0x0087, B:11:0x0099, B:13:0x00ac, B:15:0x00b2, B:17:0x00b8, B:19:0x00be, B:21:0x00c4, B:23:0x00ca, B:25:0x00d0, B:27:0x00d6, B:29:0x00dc, B:31:0x00e2, B:33:0x00e8, B:35:0x00ee, B:37:0x00f6, B:39:0x00fe, B:41:0x0106, B:49:0x0126, B:53:0x0139, B:57:0x0148, B:61:0x0157, B:65:0x0166, B:69:0x0175, B:73:0x0184, B:77:0x019b, B:81:0x01ae, B:85:0x01ba, B:89:0x01cd, B:93:0x01e0, B:97:0x01f3, B:101:0x0206, B:102:0x020d, B:104:0x021b, B:105:0x0220, B:100:0x01fc, B:96:0x01e9, B:92:0x01d6, B:88:0x01c3, B:80:0x01a4, B:76:0x0195, B:72:0x017e, B:68:0x016f, B:64:0x0160, B:60:0x0151, B:56:0x0142, B:52:0x012f), top: B:113:0x0010 }] */
        /* JADX WARN: Removed duplicated region for block: B:104:0x021b A[Catch: all -> 0x022c, TryCatch #0 {all -> 0x022c, blocks: (B:3:0x0010, B:4:0x0073, B:6:0x0079, B:8:0x0087, B:11:0x0099, B:13:0x00ac, B:15:0x00b2, B:17:0x00b8, B:19:0x00be, B:21:0x00c4, B:23:0x00ca, B:25:0x00d0, B:27:0x00d6, B:29:0x00dc, B:31:0x00e2, B:33:0x00e8, B:35:0x00ee, B:37:0x00f6, B:39:0x00fe, B:41:0x0106, B:49:0x0126, B:53:0x0139, B:57:0x0148, B:61:0x0157, B:65:0x0166, B:69:0x0175, B:73:0x0184, B:77:0x019b, B:81:0x01ae, B:85:0x01ba, B:89:0x01cd, B:93:0x01e0, B:97:0x01f3, B:101:0x0206, B:102:0x020d, B:104:0x021b, B:105:0x0220, B:100:0x01fc, B:96:0x01e9, B:92:0x01d6, B:88:0x01c3, B:80:0x01a4, B:76:0x0195, B:72:0x017e, B:68:0x016f, B:64:0x0160, B:60:0x0151, B:56:0x0142, B:52:0x012f), top: B:113:0x0010 }] */
        /* JADX WARN: Removed duplicated region for block: B:51:0x012c  */
        /* JADX WARN: Removed duplicated region for block: B:52:0x012f A[Catch: all -> 0x022c, TryCatch #0 {all -> 0x022c, blocks: (B:3:0x0010, B:4:0x0073, B:6:0x0079, B:8:0x0087, B:11:0x0099, B:13:0x00ac, B:15:0x00b2, B:17:0x00b8, B:19:0x00be, B:21:0x00c4, B:23:0x00ca, B:25:0x00d0, B:27:0x00d6, B:29:0x00dc, B:31:0x00e2, B:33:0x00e8, B:35:0x00ee, B:37:0x00f6, B:39:0x00fe, B:41:0x0106, B:49:0x0126, B:53:0x0139, B:57:0x0148, B:61:0x0157, B:65:0x0166, B:69:0x0175, B:73:0x0184, B:77:0x019b, B:81:0x01ae, B:85:0x01ba, B:89:0x01cd, B:93:0x01e0, B:97:0x01f3, B:101:0x0206, B:102:0x020d, B:104:0x021b, B:105:0x0220, B:100:0x01fc, B:96:0x01e9, B:92:0x01d6, B:88:0x01c3, B:80:0x01a4, B:76:0x0195, B:72:0x017e, B:68:0x016f, B:64:0x0160, B:60:0x0151, B:56:0x0142, B:52:0x012f), top: B:113:0x0010 }] */
        /* JADX WARN: Removed duplicated region for block: B:55:0x013f  */
        /* JADX WARN: Removed duplicated region for block: B:56:0x0142 A[Catch: all -> 0x022c, TryCatch #0 {all -> 0x022c, blocks: (B:3:0x0010, B:4:0x0073, B:6:0x0079, B:8:0x0087, B:11:0x0099, B:13:0x00ac, B:15:0x00b2, B:17:0x00b8, B:19:0x00be, B:21:0x00c4, B:23:0x00ca, B:25:0x00d0, B:27:0x00d6, B:29:0x00dc, B:31:0x00e2, B:33:0x00e8, B:35:0x00ee, B:37:0x00f6, B:39:0x00fe, B:41:0x0106, B:49:0x0126, B:53:0x0139, B:57:0x0148, B:61:0x0157, B:65:0x0166, B:69:0x0175, B:73:0x0184, B:77:0x019b, B:81:0x01ae, B:85:0x01ba, B:89:0x01cd, B:93:0x01e0, B:97:0x01f3, B:101:0x0206, B:102:0x020d, B:104:0x021b, B:105:0x0220, B:100:0x01fc, B:96:0x01e9, B:92:0x01d6, B:88:0x01c3, B:80:0x01a4, B:76:0x0195, B:72:0x017e, B:68:0x016f, B:64:0x0160, B:60:0x0151, B:56:0x0142, B:52:0x012f), top: B:113:0x0010 }] */
        /* JADX WARN: Removed duplicated region for block: B:59:0x014e  */
        /* JADX WARN: Removed duplicated region for block: B:60:0x0151 A[Catch: all -> 0x022c, TryCatch #0 {all -> 0x022c, blocks: (B:3:0x0010, B:4:0x0073, B:6:0x0079, B:8:0x0087, B:11:0x0099, B:13:0x00ac, B:15:0x00b2, B:17:0x00b8, B:19:0x00be, B:21:0x00c4, B:23:0x00ca, B:25:0x00d0, B:27:0x00d6, B:29:0x00dc, B:31:0x00e2, B:33:0x00e8, B:35:0x00ee, B:37:0x00f6, B:39:0x00fe, B:41:0x0106, B:49:0x0126, B:53:0x0139, B:57:0x0148, B:61:0x0157, B:65:0x0166, B:69:0x0175, B:73:0x0184, B:77:0x019b, B:81:0x01ae, B:85:0x01ba, B:89:0x01cd, B:93:0x01e0, B:97:0x01f3, B:101:0x0206, B:102:0x020d, B:104:0x021b, B:105:0x0220, B:100:0x01fc, B:96:0x01e9, B:92:0x01d6, B:88:0x01c3, B:80:0x01a4, B:76:0x0195, B:72:0x017e, B:68:0x016f, B:64:0x0160, B:60:0x0151, B:56:0x0142, B:52:0x012f), top: B:113:0x0010 }] */
        /* JADX WARN: Removed duplicated region for block: B:63:0x015d  */
        /* JADX WARN: Removed duplicated region for block: B:64:0x0160 A[Catch: all -> 0x022c, TryCatch #0 {all -> 0x022c, blocks: (B:3:0x0010, B:4:0x0073, B:6:0x0079, B:8:0x0087, B:11:0x0099, B:13:0x00ac, B:15:0x00b2, B:17:0x00b8, B:19:0x00be, B:21:0x00c4, B:23:0x00ca, B:25:0x00d0, B:27:0x00d6, B:29:0x00dc, B:31:0x00e2, B:33:0x00e8, B:35:0x00ee, B:37:0x00f6, B:39:0x00fe, B:41:0x0106, B:49:0x0126, B:53:0x0139, B:57:0x0148, B:61:0x0157, B:65:0x0166, B:69:0x0175, B:73:0x0184, B:77:0x019b, B:81:0x01ae, B:85:0x01ba, B:89:0x01cd, B:93:0x01e0, B:97:0x01f3, B:101:0x0206, B:102:0x020d, B:104:0x021b, B:105:0x0220, B:100:0x01fc, B:96:0x01e9, B:92:0x01d6, B:88:0x01c3, B:80:0x01a4, B:76:0x0195, B:72:0x017e, B:68:0x016f, B:64:0x0160, B:60:0x0151, B:56:0x0142, B:52:0x012f), top: B:113:0x0010 }] */
        /* JADX WARN: Removed duplicated region for block: B:67:0x016c  */
        /* JADX WARN: Removed duplicated region for block: B:68:0x016f A[Catch: all -> 0x022c, TryCatch #0 {all -> 0x022c, blocks: (B:3:0x0010, B:4:0x0073, B:6:0x0079, B:8:0x0087, B:11:0x0099, B:13:0x00ac, B:15:0x00b2, B:17:0x00b8, B:19:0x00be, B:21:0x00c4, B:23:0x00ca, B:25:0x00d0, B:27:0x00d6, B:29:0x00dc, B:31:0x00e2, B:33:0x00e8, B:35:0x00ee, B:37:0x00f6, B:39:0x00fe, B:41:0x0106, B:49:0x0126, B:53:0x0139, B:57:0x0148, B:61:0x0157, B:65:0x0166, B:69:0x0175, B:73:0x0184, B:77:0x019b, B:81:0x01ae, B:85:0x01ba, B:89:0x01cd, B:93:0x01e0, B:97:0x01f3, B:101:0x0206, B:102:0x020d, B:104:0x021b, B:105:0x0220, B:100:0x01fc, B:96:0x01e9, B:92:0x01d6, B:88:0x01c3, B:80:0x01a4, B:76:0x0195, B:72:0x017e, B:68:0x016f, B:64:0x0160, B:60:0x0151, B:56:0x0142, B:52:0x012f), top: B:113:0x0010 }] */
        /* JADX WARN: Removed duplicated region for block: B:71:0x017b  */
        /* JADX WARN: Removed duplicated region for block: B:72:0x017e A[Catch: all -> 0x022c, TryCatch #0 {all -> 0x022c, blocks: (B:3:0x0010, B:4:0x0073, B:6:0x0079, B:8:0x0087, B:11:0x0099, B:13:0x00ac, B:15:0x00b2, B:17:0x00b8, B:19:0x00be, B:21:0x00c4, B:23:0x00ca, B:25:0x00d0, B:27:0x00d6, B:29:0x00dc, B:31:0x00e2, B:33:0x00e8, B:35:0x00ee, B:37:0x00f6, B:39:0x00fe, B:41:0x0106, B:49:0x0126, B:53:0x0139, B:57:0x0148, B:61:0x0157, B:65:0x0166, B:69:0x0175, B:73:0x0184, B:77:0x019b, B:81:0x01ae, B:85:0x01ba, B:89:0x01cd, B:93:0x01e0, B:97:0x01f3, B:101:0x0206, B:102:0x020d, B:104:0x021b, B:105:0x0220, B:100:0x01fc, B:96:0x01e9, B:92:0x01d6, B:88:0x01c3, B:80:0x01a4, B:76:0x0195, B:72:0x017e, B:68:0x016f, B:64:0x0160, B:60:0x0151, B:56:0x0142, B:52:0x012f), top: B:113:0x0010 }] */
        /* JADX WARN: Removed duplicated region for block: B:75:0x0192  */
        /* JADX WARN: Removed duplicated region for block: B:76:0x0195 A[Catch: all -> 0x022c, TryCatch #0 {all -> 0x022c, blocks: (B:3:0x0010, B:4:0x0073, B:6:0x0079, B:8:0x0087, B:11:0x0099, B:13:0x00ac, B:15:0x00b2, B:17:0x00b8, B:19:0x00be, B:21:0x00c4, B:23:0x00ca, B:25:0x00d0, B:27:0x00d6, B:29:0x00dc, B:31:0x00e2, B:33:0x00e8, B:35:0x00ee, B:37:0x00f6, B:39:0x00fe, B:41:0x0106, B:49:0x0126, B:53:0x0139, B:57:0x0148, B:61:0x0157, B:65:0x0166, B:69:0x0175, B:73:0x0184, B:77:0x019b, B:81:0x01ae, B:85:0x01ba, B:89:0x01cd, B:93:0x01e0, B:97:0x01f3, B:101:0x0206, B:102:0x020d, B:104:0x021b, B:105:0x0220, B:100:0x01fc, B:96:0x01e9, B:92:0x01d6, B:88:0x01c3, B:80:0x01a4, B:76:0x0195, B:72:0x017e, B:68:0x016f, B:64:0x0160, B:60:0x0151, B:56:0x0142, B:52:0x012f), top: B:113:0x0010 }] */
        /* JADX WARN: Removed duplicated region for block: B:79:0x01a1  */
        /* JADX WARN: Removed duplicated region for block: B:80:0x01a4 A[Catch: all -> 0x022c, TryCatch #0 {all -> 0x022c, blocks: (B:3:0x0010, B:4:0x0073, B:6:0x0079, B:8:0x0087, B:11:0x0099, B:13:0x00ac, B:15:0x00b2, B:17:0x00b8, B:19:0x00be, B:21:0x00c4, B:23:0x00ca, B:25:0x00d0, B:27:0x00d6, B:29:0x00dc, B:31:0x00e2, B:33:0x00e8, B:35:0x00ee, B:37:0x00f6, B:39:0x00fe, B:41:0x0106, B:49:0x0126, B:53:0x0139, B:57:0x0148, B:61:0x0157, B:65:0x0166, B:69:0x0175, B:73:0x0184, B:77:0x019b, B:81:0x01ae, B:85:0x01ba, B:89:0x01cd, B:93:0x01e0, B:97:0x01f3, B:101:0x0206, B:102:0x020d, B:104:0x021b, B:105:0x0220, B:100:0x01fc, B:96:0x01e9, B:92:0x01d6, B:88:0x01c3, B:80:0x01a4, B:76:0x0195, B:72:0x017e, B:68:0x016f, B:64:0x0160, B:60:0x0151, B:56:0x0142, B:52:0x012f), top: B:113:0x0010 }] */
        /* JADX WARN: Removed duplicated region for block: B:83:0x01b4  */
        /* JADX WARN: Removed duplicated region for block: B:84:0x01b7  */
        /* JADX WARN: Removed duplicated region for block: B:87:0x01c0  */
        /* JADX WARN: Removed duplicated region for block: B:88:0x01c3 A[Catch: all -> 0x022c, TryCatch #0 {all -> 0x022c, blocks: (B:3:0x0010, B:4:0x0073, B:6:0x0079, B:8:0x0087, B:11:0x0099, B:13:0x00ac, B:15:0x00b2, B:17:0x00b8, B:19:0x00be, B:21:0x00c4, B:23:0x00ca, B:25:0x00d0, B:27:0x00d6, B:29:0x00dc, B:31:0x00e2, B:33:0x00e8, B:35:0x00ee, B:37:0x00f6, B:39:0x00fe, B:41:0x0106, B:49:0x0126, B:53:0x0139, B:57:0x0148, B:61:0x0157, B:65:0x0166, B:69:0x0175, B:73:0x0184, B:77:0x019b, B:81:0x01ae, B:85:0x01ba, B:89:0x01cd, B:93:0x01e0, B:97:0x01f3, B:101:0x0206, B:102:0x020d, B:104:0x021b, B:105:0x0220, B:100:0x01fc, B:96:0x01e9, B:92:0x01d6, B:88:0x01c3, B:80:0x01a4, B:76:0x0195, B:72:0x017e, B:68:0x016f, B:64:0x0160, B:60:0x0151, B:56:0x0142, B:52:0x012f), top: B:113:0x0010 }] */
        /* JADX WARN: Removed duplicated region for block: B:91:0x01d3  */
        /* JADX WARN: Removed duplicated region for block: B:92:0x01d6 A[Catch: all -> 0x022c, TryCatch #0 {all -> 0x022c, blocks: (B:3:0x0010, B:4:0x0073, B:6:0x0079, B:8:0x0087, B:11:0x0099, B:13:0x00ac, B:15:0x00b2, B:17:0x00b8, B:19:0x00be, B:21:0x00c4, B:23:0x00ca, B:25:0x00d0, B:27:0x00d6, B:29:0x00dc, B:31:0x00e2, B:33:0x00e8, B:35:0x00ee, B:37:0x00f6, B:39:0x00fe, B:41:0x0106, B:49:0x0126, B:53:0x0139, B:57:0x0148, B:61:0x0157, B:65:0x0166, B:69:0x0175, B:73:0x0184, B:77:0x019b, B:81:0x01ae, B:85:0x01ba, B:89:0x01cd, B:93:0x01e0, B:97:0x01f3, B:101:0x0206, B:102:0x020d, B:104:0x021b, B:105:0x0220, B:100:0x01fc, B:96:0x01e9, B:92:0x01d6, B:88:0x01c3, B:80:0x01a4, B:76:0x0195, B:72:0x017e, B:68:0x016f, B:64:0x0160, B:60:0x0151, B:56:0x0142, B:52:0x012f), top: B:113:0x0010 }] */
        /* JADX WARN: Removed duplicated region for block: B:95:0x01e6  */
        /* JADX WARN: Removed duplicated region for block: B:96:0x01e9 A[Catch: all -> 0x022c, TryCatch #0 {all -> 0x022c, blocks: (B:3:0x0010, B:4:0x0073, B:6:0x0079, B:8:0x0087, B:11:0x0099, B:13:0x00ac, B:15:0x00b2, B:17:0x00b8, B:19:0x00be, B:21:0x00c4, B:23:0x00ca, B:25:0x00d0, B:27:0x00d6, B:29:0x00dc, B:31:0x00e2, B:33:0x00e8, B:35:0x00ee, B:37:0x00f6, B:39:0x00fe, B:41:0x0106, B:49:0x0126, B:53:0x0139, B:57:0x0148, B:61:0x0157, B:65:0x0166, B:69:0x0175, B:73:0x0184, B:77:0x019b, B:81:0x01ae, B:85:0x01ba, B:89:0x01cd, B:93:0x01e0, B:97:0x01f3, B:101:0x0206, B:102:0x020d, B:104:0x021b, B:105:0x0220, B:100:0x01fc, B:96:0x01e9, B:92:0x01d6, B:88:0x01c3, B:80:0x01a4, B:76:0x0195, B:72:0x017e, B:68:0x016f, B:64:0x0160, B:60:0x0151, B:56:0x0142, B:52:0x012f), top: B:113:0x0010 }] */
        /* JADX WARN: Removed duplicated region for block: B:99:0x01f9  */
        @Override // java.util.concurrent.Callable
        /* renamed from: a */
        /*
            Code decompiled incorrectly, please refer to instructions dump.
            To view partially-correct code enable 'Show inconsistent code' option in preferences
        */
        public net.safemoon.androidwallet.model.reflections.RoomReflectionsTokenAndData call() throws java.lang.Exception {
            /*
                Method dump skipped, instructions count: 561
                To view this dump change 'Code comments level' option to 'DEBUG'
            */
            throw new UnsupportedOperationException("Method not decompiled: net.safemoon.androidwallet.database.room.f.s.call():net.safemoon.androidwallet.model.reflections.RoomReflectionsTokenAndData");
        }

        public void finalize() {
            this.a.f();
        }
    }

    /* compiled from: ReflectionsDao_Impl.java */
    /* loaded from: classes2.dex */
    public class t extends zv0<RoomReflectionsData> {
        public t(f fVar, RoomDatabase roomDatabase) {
            super(roomDatabase);
        }

        @Override // defpackage.co3
        public String d() {
            return "INSERT OR ABORT INTO `table_room_reflections_data_2` (`_id`,`symbolWithType`,`nativeBalance`,`blockBalance`,`block`,`timeStamp`) VALUES (?,?,?,?,?,?)";
        }

        @Override // defpackage.zv0
        /* renamed from: k */
        public void g(ww3 ww3Var, RoomReflectionsData roomReflectionsData) {
            if (roomReflectionsData.getId() == null) {
                ww3Var.Y0(1);
            } else {
                ww3Var.q0(1, roomReflectionsData.getId().longValue());
            }
            if (roomReflectionsData.getSymbolWithType() == null) {
                ww3Var.Y0(2);
            } else {
                ww3Var.L(2, roomReflectionsData.getSymbolWithType());
            }
            if (roomReflectionsData.getNativeBalance() == null) {
                ww3Var.Y0(3);
            } else {
                ww3Var.L(3, roomReflectionsData.getNativeBalance());
            }
            if (roomReflectionsData.getBlockBalance() == null) {
                ww3Var.Y0(4);
            } else {
                ww3Var.L(4, roomReflectionsData.getBlockBalance());
            }
            if (roomReflectionsData.getBlock() == null) {
                ww3Var.Y0(5);
            } else {
                ww3Var.L(5, roomReflectionsData.getBlock());
            }
            ww3Var.q0(6, roomReflectionsData.getTimeStamp());
        }
    }

    /* compiled from: ReflectionsDao_Impl.java */
    /* loaded from: classes2.dex */
    public class u implements Callable<RoomReflectionsToken> {
        public final /* synthetic */ k93 a;

        public u(k93 k93Var) {
            this.a = k93Var;
        }

        @Override // java.util.concurrent.Callable
        /* renamed from: a */
        public RoomReflectionsToken call() throws Exception {
            RoomReflectionsToken roomReflectionsToken;
            u uVar = this;
            f.this.a.e();
            try {
                Cursor c = id0.c(f.this.a, uVar.a, false, null);
                try {
                    int e = wb0.e(c, "_id");
                    int e2 = wb0.e(c, "symbolWithType");
                    int e3 = wb0.e(c, "symbol");
                    int e4 = wb0.e(c, PublicResolver.FUNC_NAME);
                    int e5 = wb0.e(c, "iconPath");
                    int e6 = wb0.e(c, "contractorAddress");
                    int e7 = wb0.e(c, "tokenTypeChain");
                    int e8 = wb0.e(c, "decimals");
                    int e9 = wb0.e(c, "nativeBalance");
                    int e10 = wb0.e(c, "firstTimeStamp");
                    int e11 = wb0.e(c, "enableAdvanceMode");
                    int e12 = wb0.e(c, "latestBalance");
                    int e13 = wb0.e(c, "latestTimeStamp");
                    int e14 = wb0.e(c, "cmcId");
                    try {
                        int e15 = wb0.e(c, "priceUsd");
                        if (c.moveToFirst()) {
                            roomReflectionsToken = new RoomReflectionsToken(c.isNull(e) ? null : Long.valueOf(c.getLong(e)), c.isNull(e2) ? null : c.getString(e2), c.isNull(e3) ? null : c.getString(e3), c.isNull(e4) ? null : c.getString(e4), c.isNull(e5) ? null : c.getString(e5), c.isNull(e6) ? null : c.getString(e6), c.getInt(e7), c.getInt(e8), c.isNull(e9) ? null : c.getString(e9), c.isNull(e10) ? null : Long.valueOf(c.getLong(e10)), c.getInt(e11) != 0, c.isNull(e12) ? null : Long.valueOf(c.getLong(e12)), c.isNull(e13) ? null : Long.valueOf(c.getLong(e13)), c.isNull(e14) ? null : Long.valueOf(c.getLong(e14)), c.isNull(e15) ? null : Double.valueOf(c.getDouble(e15)));
                        } else {
                            roomReflectionsToken = null;
                        }
                        uVar = this;
                        f.this.a.E();
                        c.close();
                        uVar.a.f();
                        return roomReflectionsToken;
                    } catch (Throwable th) {
                        th = th;
                        uVar = this;
                        c.close();
                        uVar.a.f();
                        throw th;
                    }
                } catch (Throwable th2) {
                    th = th2;
                }
            } finally {
                f.this.a.j();
            }
        }
    }

    /* compiled from: ReflectionsDao_Impl.java */
    /* loaded from: classes2.dex */
    public class v extends yv0<RoomReflectionsToken> {
        public v(f fVar, RoomDatabase roomDatabase) {
            super(roomDatabase);
        }

        @Override // defpackage.co3
        public String d() {
            return "DELETE FROM `table_room_reflections_token_2` WHERE `_id` = ?";
        }

        @Override // defpackage.yv0
        /* renamed from: i */
        public void g(ww3 ww3Var, RoomReflectionsToken roomReflectionsToken) {
            if (roomReflectionsToken.getId() == null) {
                ww3Var.Y0(1);
            } else {
                ww3Var.q0(1, roomReflectionsToken.getId().longValue());
            }
        }
    }

    /* compiled from: ReflectionsDao_Impl.java */
    /* loaded from: classes2.dex */
    public class w extends co3 {
        public w(f fVar, RoomDatabase roomDatabase) {
            super(roomDatabase);
        }

        @Override // defpackage.co3
        public String d() {
            return "UPDATE table_room_reflections_token_2 SET cmcId = ? WHERE symbolWithType=?";
        }
    }

    /* compiled from: ReflectionsDao_Impl.java */
    /* loaded from: classes2.dex */
    public class x extends co3 {
        public x(f fVar, RoomDatabase roomDatabase) {
            super(roomDatabase);
        }

        @Override // defpackage.co3
        public String d() {
            return "UPDATE table_room_reflections_token_2 SET priceUsd = ? WHERE symbolWithType=?";
        }
    }

    /* compiled from: ReflectionsDao_Impl.java */
    /* loaded from: classes2.dex */
    public class y extends co3 {
        public y(f fVar, RoomDatabase roomDatabase) {
            super(roomDatabase);
        }

        @Override // defpackage.co3
        public String d() {
            return "UPDATE table_room_reflections_token_2 SET latestBalance = ?, latestTimeStamp = ? WHERE symbolWithType=?";
        }
    }

    /* compiled from: ReflectionsDao_Impl.java */
    /* loaded from: classes2.dex */
    public class z extends co3 {
        public z(f fVar, RoomDatabase roomDatabase) {
            super(roomDatabase);
        }

        @Override // defpackage.co3
        public String d() {
            return "UPDATE table_room_reflections_token_2 SET enableAdvanceMode = ? WHERE _id=?";
        }
    }

    public f(RoomDatabase roomDatabase) {
        this.a = roomDatabase;
        this.b = new k(this, roomDatabase);
        this.c = new t(this, roomDatabase);
        this.d = new v(this, roomDatabase);
        this.e = new w(this, roomDatabase);
        this.f = new x(this, roomDatabase);
        this.g = new y(this, roomDatabase);
        this.h = new z(this, roomDatabase);
        this.i = new a0(this, roomDatabase);
        this.j = new b0(this, roomDatabase);
    }

    public static List<Class<?>> H() {
        return Collections.emptyList();
    }

    @Override // defpackage.t53
    public Object a(double d2, String str, q70<? super te4> q70Var) {
        return CoroutinesRoom.b(this.a, true, new e(d2, str), q70Var);
    }

    @Override // defpackage.t53
    public Object b(RoomReflectionsToken roomReflectionsToken, q70<? super Long> q70Var) {
        return CoroutinesRoom.b(this.a, true, new a(roomReflectionsToken), q70Var);
    }

    @Override // defpackage.t53
    public LiveData<List<RoomReflectionsTokenAndData>> c(int i2) {
        k93 c2 = k93.c("SELECT * FROM table_room_reflections_token_2 WHERE tokenTypeChain=?", 1);
        c2.q0(1, i2);
        return this.a.n().e(new String[]{"table_room_reflections_data_2", "table_room_reflections_token_2"}, true, new n(c2));
    }

    @Override // defpackage.t53
    public LiveData<List<RoomReflectionsDataAndToken>> d(String str, long j2) {
        k93 c2 = k93.c("SELECT * FROM table_room_reflections_data_2 WHERE symbolWithType=?  AND ((timeStamp >= (SELECT timeStamp FROM table_room_reflections_data_2 WHERE symbolWithType=?  AND timeStamp < ? ORDER BY `timeStamp` DESC LIMIT 1)) OR timeStamp >= ?) ORDER BY `timeStamp` DESC", 4);
        if (str == null) {
            c2.Y0(1);
        } else {
            c2.L(1, str);
        }
        if (str == null) {
            c2.Y0(2);
        } else {
            c2.L(2, str);
        }
        c2.q0(3, j2);
        c2.q0(4, j2);
        return this.a.n().e(new String[]{"table_room_reflections_token_2", "table_room_reflections_data_2"}, true, new q(c2));
    }

    @Override // defpackage.t53
    public Object e(RoomReflectionsData roomReflectionsData, q70<? super Long> q70Var) {
        return CoroutinesRoom.b(this.a, true, new b(roomReflectionsData), q70Var);
    }

    @Override // defpackage.t53
    public LiveData<List<RoomReflectionsToken>> f(int i2) {
        k93 c2 = k93.c("SELECT * FROM table_room_reflections_token_2 WHERE tokenTypeChain=?", 1);
        c2.q0(1, i2);
        return this.a.n().e(new String[]{"table_room_reflections_token_2"}, false, new m(c2));
    }

    @Override // defpackage.t53
    public Object g(long j2, long j3, String str, q70<? super te4> q70Var) {
        return CoroutinesRoom.b(this.a, true, new CallableC0208f(j2, j3, str), q70Var);
    }

    @Override // defpackage.t53
    public Object h(String str, q70<? super RoomReflectionsData> q70Var) {
        k93 c2 = k93.c("SELECT * FROM table_room_reflections_data_2 WHERE symbolWithType=? ORDER BY `timeStamp` DESC LIMIT 1", 1);
        if (str == null) {
            c2.Y0(1);
        } else {
            c2.L(1, str);
        }
        return CoroutinesRoom.a(this.a, false, id0.a(), new r(c2), q70Var);
    }

    @Override // defpackage.t53
    public Object i(String str, q70<? super RoomReflectionsToken> q70Var) {
        k93 c2 = k93.c("SELECT * FROM table_room_reflections_token_2 WHERE symbolWithType=?", 1);
        if (str == null) {
            c2.Y0(1);
        } else {
            c2.L(1, str);
        }
        return CoroutinesRoom.a(this.a, true, id0.a(), new u(c2), q70Var);
    }

    @Override // defpackage.t53
    public Object j(String str, long j2, q70<? super Boolean> q70Var) {
        k93 c2 = k93.c("SELECT EXISTS (SELECT 1 FROM table_room_reflections_data_2 WHERE symbolWithType = ? AND timeStamp >=?)", 2);
        if (str == null) {
            c2.Y0(1);
        } else {
            c2.L(1, str);
        }
        c2.q0(2, j2);
        return CoroutinesRoom.a(this.a, false, id0.a(), new l(c2), q70Var);
    }

    @Override // defpackage.t53
    public Object k(q70<? super te4> q70Var) {
        return CoroutinesRoom.b(this.a, true, new h(), q70Var);
    }

    @Override // defpackage.t53
    public Object l(RoomReflectionsToken roomReflectionsToken, q70<? super te4> q70Var) {
        return CoroutinesRoom.b(this.a, true, new c(roomReflectionsToken), q70Var);
    }

    @Override // defpackage.t53
    public Object m(q70<? super List<RoomReflectionsToken>> q70Var) {
        k93 c2 = k93.c("SELECT * FROM table_room_reflections_token_2", 0);
        return CoroutinesRoom.a(this.a, false, id0.a(), new o(c2), q70Var);
    }

    @Override // defpackage.t53
    public LiveData<List<RoomReflectionsDataAndToken>> n(String str) {
        k93 c2 = k93.c("SELECT * FROM table_room_reflections_data_2 WHERE symbolWithType=? ORDER BY `timeStamp` DESC", 1);
        if (str == null) {
            c2.Y0(1);
        } else {
            c2.L(1, str);
        }
        return this.a.n().e(new String[]{"table_room_reflections_token_2", "table_room_reflections_data_2"}, true, new p(c2));
    }

    @Override // defpackage.t53
    public LiveData<RoomReflectionsTokenAndData> o(String str) {
        k93 c2 = k93.c("SELECT * FROM table_room_reflections_token_2 WHERE symbolWithType=?", 1);
        if (str == null) {
            c2.Y0(1);
        } else {
            c2.L(1, str);
        }
        return this.a.n().e(new String[]{"table_room_reflections_data_2", "table_room_reflections_token_2"}, false, new s(c2));
    }

    @Override // defpackage.t53
    public Object p(int i2, String str, q70<? super te4> q70Var) {
        return CoroutinesRoom.b(this.a, true, new d(i2, str), q70Var);
    }

    @Override // defpackage.t53
    public Object q(long j2, int i2, q70<? super te4> q70Var) {
        return CoroutinesRoom.b(this.a, true, new g(i2, j2), q70Var);
    }

    @Override // defpackage.t53
    public Object r(String str, q70<? super te4> q70Var) {
        return CoroutinesRoom.b(this.a, true, new i(str), q70Var);
    }

    @Override // defpackage.t53
    public Object s(String str, int i2, q70<? super Boolean> q70Var) {
        k93 c2 = k93.c("SELECT EXISTS (SELECT 1 FROM table_room_reflections_token_2 WHERE symbolWithType = ? AND tokenTypeChain =?)", 2);
        if (str == null) {
            c2.Y0(1);
        } else {
            c2.L(1, str);
        }
        c2.q0(2, i2);
        return CoroutinesRoom.a(this.a, false, id0.a(), new j(c2), q70Var);
    }

    public final void t(rh<String, ArrayList<RoomReflectionsData>> rhVar) {
        Set<String> keySet = rhVar.keySet();
        if (keySet.isEmpty()) {
            return;
        }
        if (rhVar.size() > 999) {
            rh<String, ArrayList<RoomReflectionsData>> rhVar2 = new rh<>(999);
            int size = rhVar.size();
            int i2 = 0;
            int i3 = 0;
            while (i2 < size) {
                rhVar2.put(rhVar.i(i2), rhVar.m(i2));
                i2++;
                i3++;
                if (i3 == 999) {
                    t(rhVar2);
                    rhVar2 = new rh<>(999);
                    i3 = 0;
                }
            }
            if (i3 > 0) {
                t(rhVar2);
                return;
            }
            return;
        }
        StringBuilder b2 = qu3.b();
        b2.append("SELECT `_id`,`symbolWithType`,`nativeBalance`,`blockBalance`,`block`,`timeStamp` FROM `table_room_reflections_data_2` WHERE `symbolWithType` IN (");
        int size2 = keySet.size();
        qu3.a(b2, size2);
        b2.append(")");
        k93 c2 = k93.c(b2.toString(), size2 + 0);
        int i4 = 1;
        for (String str : keySet) {
            if (str == null) {
                c2.Y0(i4);
            } else {
                c2.L(i4, str);
            }
            i4++;
        }
        Cursor c3 = id0.c(this.a, c2, false, null);
        try {
            int d2 = wb0.d(c3, "symbolWithType");
            if (d2 == -1) {
                return;
            }
            int e2 = wb0.e(c3, "_id");
            int e3 = wb0.e(c3, "symbolWithType");
            int e4 = wb0.e(c3, "nativeBalance");
            int e5 = wb0.e(c3, "blockBalance");
            int e6 = wb0.e(c3, "block");
            int e7 = wb0.e(c3, "timeStamp");
            while (c3.moveToNext()) {
                ArrayList<RoomReflectionsData> arrayList = rhVar.get(c3.getString(d2));
                if (arrayList != null) {
                    arrayList.add(new RoomReflectionsData(c3.isNull(e2) ? null : Long.valueOf(c3.getLong(e2)), c3.isNull(e3) ? null : c3.getString(e3), c3.isNull(e4) ? null : c3.getString(e4), c3.isNull(e5) ? null : c3.getString(e5), c3.isNull(e6) ? null : c3.getString(e6), c3.getLong(e7)));
                }
            }
        } finally {
            c3.close();
        }
    }

    public final void u(rh<String, RoomReflectionsToken> rhVar) {
        int i2;
        int i3;
        int i4;
        int i5;
        Double valueOf;
        Set<String> keySet = rhVar.keySet();
        if (keySet.isEmpty()) {
            return;
        }
        if (rhVar.size() > 999) {
            rh<String, RoomReflectionsToken> rhVar2 = new rh<>(999);
            int size = rhVar.size();
            int i6 = 0;
            int i7 = 0;
            while (i6 < size) {
                rhVar2.put(rhVar.i(i6), null);
                i6++;
                i7++;
                if (i7 == 999) {
                    u(rhVar2);
                    rhVar.putAll(rhVar2);
                    rhVar2 = new rh<>(999);
                    i7 = 0;
                }
            }
            if (i7 > 0) {
                u(rhVar2);
                rhVar.putAll(rhVar2);
                return;
            }
            return;
        }
        StringBuilder b2 = qu3.b();
        b2.append("SELECT `_id`,`symbolWithType`,`symbol`,`name`,`iconPath`,`contractorAddress`,`tokenTypeChain`,`decimals`,`nativeBalance`,`firstTimeStamp`,`enableAdvanceMode`,`latestBalance`,`latestTimeStamp`,`cmcId`,`priceUsd` FROM `table_room_reflections_token_2` WHERE `symbolWithType` IN (");
        int size2 = keySet.size();
        qu3.a(b2, size2);
        b2.append(")");
        k93 c2 = k93.c(b2.toString(), size2 + 0);
        int i8 = 1;
        for (String str : keySet) {
            if (str == null) {
                c2.Y0(i8);
            } else {
                c2.L(i8, str);
            }
            i8++;
        }
        Cursor c3 = id0.c(this.a, c2, false, null);
        try {
            int d2 = wb0.d(c3, "symbolWithType");
            if (d2 == -1) {
                return;
            }
            int e2 = wb0.e(c3, "_id");
            int e3 = wb0.e(c3, "symbolWithType");
            int e4 = wb0.e(c3, "symbol");
            int e5 = wb0.e(c3, PublicResolver.FUNC_NAME);
            int e6 = wb0.e(c3, "iconPath");
            int e7 = wb0.e(c3, "contractorAddress");
            int e8 = wb0.e(c3, "tokenTypeChain");
            int e9 = wb0.e(c3, "decimals");
            int e10 = wb0.e(c3, "nativeBalance");
            int e11 = wb0.e(c3, "firstTimeStamp");
            int e12 = wb0.e(c3, "enableAdvanceMode");
            int e13 = wb0.e(c3, "latestBalance");
            int e14 = wb0.e(c3, "latestTimeStamp");
            int e15 = wb0.e(c3, "cmcId");
            int e16 = wb0.e(c3, "priceUsd");
            while (c3.moveToNext()) {
                int i9 = e16;
                String string = c3.getString(d2);
                if (rhVar.containsKey(string)) {
                    Long valueOf2 = c3.isNull(e2) ? null : Long.valueOf(c3.getLong(e2));
                    String string2 = c3.isNull(e3) ? null : c3.getString(e3);
                    String string3 = c3.isNull(e4) ? null : c3.getString(e4);
                    String string4 = c3.isNull(e5) ? null : c3.getString(e5);
                    String string5 = c3.isNull(e6) ? null : c3.getString(e6);
                    String string6 = c3.isNull(e7) ? null : c3.getString(e7);
                    int i10 = c3.getInt(e8);
                    int i11 = c3.getInt(e9);
                    String string7 = c3.isNull(e10) ? null : c3.getString(e10);
                    Long valueOf3 = c3.isNull(e11) ? null : Long.valueOf(c3.getLong(e11));
                    boolean z2 = c3.getInt(e12) != 0;
                    Long valueOf4 = c3.isNull(e13) ? null : Long.valueOf(c3.getLong(e13));
                    int i12 = e14;
                    i5 = e3;
                    Long valueOf5 = c3.isNull(i12) ? null : Long.valueOf(c3.getLong(i12));
                    int i13 = e15;
                    i4 = i12;
                    Long valueOf6 = c3.isNull(i13) ? null : Long.valueOf(c3.getLong(i13));
                    i3 = i13;
                    if (c3.isNull(i9)) {
                        i2 = i9;
                        valueOf = null;
                    } else {
                        valueOf = Double.valueOf(c3.getDouble(i9));
                        i2 = i9;
                    }
                    rhVar.put(string, new RoomReflectionsToken(valueOf2, string2, string3, string4, string5, string6, i10, i11, string7, valueOf3, z2, valueOf4, valueOf5, valueOf6, valueOf));
                } else {
                    i2 = i9;
                    i3 = e15;
                    i4 = e14;
                    i5 = e3;
                }
                e3 = i5;
                e14 = i4;
                e15 = i3;
                e16 = i2;
            }
        } finally {
            c3.close();
        }
    }
}
