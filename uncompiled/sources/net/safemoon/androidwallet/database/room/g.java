package net.safemoon.androidwallet.database.room;

import android.database.Cursor;
import androidx.room.CoroutinesRoom;
import androidx.room.RoomDatabase;
import java.util.Collections;
import java.util.List;
import java.util.concurrent.Callable;
import net.safemoon.androidwallet.model.RoomTokenInfo;

/* compiled from: TokenInfoDao_Impl.java */
/* loaded from: classes2.dex */
public final class g implements x64 {
    public final RoomDatabase a;
    public final zv0<RoomTokenInfo> b;

    /* compiled from: TokenInfoDao_Impl.java */
    /* loaded from: classes2.dex */
    public class a extends zv0<RoomTokenInfo> {
        public a(g gVar, RoomDatabase roomDatabase) {
            super(roomDatabase);
        }

        @Override // defpackage.co3
        public String d() {
            return "INSERT OR REPLACE INTO `table_room_token_info` (`_id`,`symbolWithType`,`chainId`,`slug`,`cmcId`) VALUES (?,?,?,?,?)";
        }

        @Override // defpackage.zv0
        /* renamed from: k */
        public void g(ww3 ww3Var, RoomTokenInfo roomTokenInfo) {
            if (roomTokenInfo.getId() == null) {
                ww3Var.Y0(1);
            } else {
                ww3Var.q0(1, roomTokenInfo.getId().longValue());
            }
            if (roomTokenInfo.getSymbolWithType() == null) {
                ww3Var.Y0(2);
            } else {
                ww3Var.L(2, roomTokenInfo.getSymbolWithType());
            }
            ww3Var.q0(3, roomTokenInfo.getChainId());
            if (roomTokenInfo.getSlug() == null) {
                ww3Var.Y0(4);
            } else {
                ww3Var.L(4, roomTokenInfo.getSlug());
            }
            if (roomTokenInfo.getCmcId() == null) {
                ww3Var.Y0(5);
            } else {
                ww3Var.q0(5, roomTokenInfo.getCmcId().intValue());
            }
        }
    }

    /* compiled from: TokenInfoDao_Impl.java */
    /* loaded from: classes2.dex */
    public class b implements Callable<Long> {
        public final /* synthetic */ RoomTokenInfo a;

        public b(RoomTokenInfo roomTokenInfo) {
            this.a = roomTokenInfo;
        }

        @Override // java.util.concurrent.Callable
        /* renamed from: a */
        public Long call() throws Exception {
            g.this.a.e();
            try {
                long j = g.this.b.j(this.a);
                g.this.a.E();
                return Long.valueOf(j);
            } finally {
                g.this.a.j();
            }
        }
    }

    /* compiled from: TokenInfoDao_Impl.java */
    /* loaded from: classes2.dex */
    public class c implements Callable<Boolean> {
        public final /* synthetic */ k93 a;

        public c(k93 k93Var) {
            this.a = k93Var;
        }

        @Override // java.util.concurrent.Callable
        /* renamed from: a */
        public Boolean call() throws Exception {
            Boolean bool = null;
            Cursor c = id0.c(g.this.a, this.a, false, null);
            try {
                if (c.moveToFirst()) {
                    Integer valueOf = c.isNull(0) ? null : Integer.valueOf(c.getInt(0));
                    if (valueOf != null) {
                        bool = Boolean.valueOf(valueOf.intValue() != 0);
                    }
                }
                return bool;
            } finally {
                c.close();
                this.a.f();
            }
        }
    }

    /* compiled from: TokenInfoDao_Impl.java */
    /* loaded from: classes2.dex */
    public class d implements Callable<RoomTokenInfo> {
        public final /* synthetic */ k93 a;

        public d(k93 k93Var) {
            this.a = k93Var;
        }

        @Override // java.util.concurrent.Callable
        /* renamed from: a */
        public RoomTokenInfo call() throws Exception {
            RoomTokenInfo roomTokenInfo = null;
            Cursor c = id0.c(g.this.a, this.a, false, null);
            try {
                int e = wb0.e(c, "_id");
                int e2 = wb0.e(c, "symbolWithType");
                int e3 = wb0.e(c, "chainId");
                int e4 = wb0.e(c, "slug");
                int e5 = wb0.e(c, "cmcId");
                if (c.moveToFirst()) {
                    roomTokenInfo = new RoomTokenInfo(c.isNull(e) ? null : Long.valueOf(c.getLong(e)), c.isNull(e2) ? null : c.getString(e2), c.getInt(e3), c.isNull(e4) ? null : c.getString(e4), c.isNull(e5) ? null : Integer.valueOf(c.getInt(e5)));
                }
                return roomTokenInfo;
            } finally {
                c.close();
                this.a.f();
            }
        }
    }

    public g(RoomDatabase roomDatabase) {
        this.a = roomDatabase;
        this.b = new a(this, roomDatabase);
    }

    public static List<Class<?>> f() {
        return Collections.emptyList();
    }

    @Override // defpackage.x64
    public Object a(RoomTokenInfo roomTokenInfo, q70<? super Long> q70Var) {
        return CoroutinesRoom.b(this.a, true, new b(roomTokenInfo), q70Var);
    }

    @Override // defpackage.x64
    public Object b(String str, q70<? super Boolean> q70Var) {
        k93 c2 = k93.c("SELECT EXISTS (SELECT 1 FROM table_room_token_info WHERE symbolWithType = ?)", 1);
        if (str == null) {
            c2.Y0(1);
        } else {
            c2.L(1, str);
        }
        return CoroutinesRoom.a(this.a, false, id0.a(), new c(c2), q70Var);
    }

    @Override // defpackage.x64
    public Object c(int i, q70<? super RoomTokenInfo> q70Var) {
        k93 c2 = k93.c("SELECT * FROM table_room_token_info WHERE cmcId = ?", 1);
        c2.q0(1, i);
        return CoroutinesRoom.a(this.a, false, id0.a(), new d(c2), q70Var);
    }
}
