package net.safemoon.androidwallet.database.mainRoom;

import android.content.Context;
import androidx.room.RoomDatabase;
import androidx.room.l;

/* compiled from: MainRoomDatabase.kt */
/* loaded from: classes2.dex */
public abstract class MainRoomDatabase extends RoomDatabase {
    public static final f n = new f(null);
    public static final a o = new a();
    public static final b p = new b();
    public static final c q = new c();
    public static final d r = new d();
    public static final e s = new e();
    public static MainRoomDatabase t;

    /* compiled from: MainRoomDatabase.kt */
    /* loaded from: classes2.dex */
    public static final class a extends w82 {
        public a() {
            super(1, 2);
        }

        @Override // defpackage.w82
        public void a(sw3 sw3Var) {
            fs1.f(sw3Var, "database");
            sw3Var.K("CREATE TABLE IF NOT EXISTS `table_wallet_connect` (`session` TEXT NOT NULL, `peerId` TEXT NOT NULL, `remotePeerId` TEXT NOT NULL, `chainId` INTEGER NOT NULL, `peerMeta` TEXT NOT NULL, `connectedAt` INTEGER NOT NULL, `isAutoDisconnect` INTEGER NOT NULL, `walletId` INTEGER NOT NULL, PRIMARY KEY(`session`))");
        }
    }

    /* compiled from: MainRoomDatabase.kt */
    /* loaded from: classes2.dex */
    public static final class b extends w82 {
        public b() {
            super(2, 3);
        }

        @Override // defpackage.w82
        public void a(sw3 sw3Var) {
            fs1.f(sw3Var, "database");
            sw3Var.K("ALTER TABLE table_wallet ADD COLUMN `ka` TEXT");
            sw3Var.K("ALTER TABLE table_wallet ADD COLUMN `u5b64` TEXT");
        }
    }

    /* compiled from: MainRoomDatabase.kt */
    /* loaded from: classes2.dex */
    public static final class c extends w82 {
        public c() {
            super(3, 4);
        }

        @Override // defpackage.w82
        public void a(sw3 sw3Var) {
            fs1.f(sw3Var, "database");
            sw3Var.K("ALTER TABLE table_wallet ADD COLUMN `isLinked` INTEGER NOT NULL DEFAULT 0");
        }
    }

    /* compiled from: MainRoomDatabase.kt */
    /* loaded from: classes2.dex */
    public static final class d extends w82 {
        public d() {
            super(4, 5);
        }

        @Override // defpackage.w82
        public void a(sw3 sw3Var) {
            fs1.f(sw3Var, "database");
            sw3Var.K("ALTER TABLE table_wallet ADD COLUMN `order` INTEGER NOT NULL DEFAULT 0");
        }
    }

    /* compiled from: MainRoomDatabase.kt */
    /* loaded from: classes2.dex */
    public static final class e extends w82 {
        public e() {
            super(5, 6);
        }

        @Override // defpackage.w82
        public void a(sw3 sw3Var) {
            fs1.f(sw3Var, "database");
            sw3Var.K("ALTER TABLE table_wallet ADD COLUMN `isPrimaryWallet` INTEGER NOT NULL DEFAULT 0");
            sw3Var.K("UPDATE `table_wallet` SET `isPrimaryWallet` = 1 where `name`= 'SafeMoon Master Wallet' and `u5b64` is not null");
        }
    }

    /* compiled from: MainRoomDatabase.kt */
    /* loaded from: classes2.dex */
    public static final class f {
        public f() {
        }

        public /* synthetic */ f(qi0 qi0Var) {
            this();
        }

        public final void a(Context context) {
            fs1.f(context, "context");
            b(context).f();
        }

        public final MainRoomDatabase b(Context context) {
            fs1.f(context, "context");
            if (MainRoomDatabase.t == null) {
                synchronized (this) {
                    if (MainRoomDatabase.t == null) {
                        f fVar = MainRoomDatabase.n;
                        MainRoomDatabase.t = (MainRoomDatabase) l.a(context.getApplicationContext(), MainRoomDatabase.class, "mainSafeMoon").b(MainRoomDatabase.o, MainRoomDatabase.p, MainRoomDatabase.q, MainRoomDatabase.r, MainRoomDatabase.s).d();
                    }
                    te4 te4Var = te4.a;
                }
            }
            MainRoomDatabase mainRoomDatabase = MainRoomDatabase.t;
            fs1.d(mainRoomDatabase);
            return mainRoomDatabase;
        }
    }

    public abstract t50 N();

    public abstract t60 O();

    public abstract mm4 P();
}
