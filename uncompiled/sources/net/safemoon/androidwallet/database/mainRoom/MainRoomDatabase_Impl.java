package net.safemoon.androidwallet.database.mainRoom;

import androidx.room.RoomDatabase;
import androidx.room.f;
import androidx.room.m;
import defpackage.f34;
import defpackage.tw3;
import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import org.web3j.abi.datatypes.Address;
import org.web3j.ens.contracts.generated.PublicResolver;

/* loaded from: classes2.dex */
public final class MainRoomDatabase_Impl extends MainRoomDatabase {
    public volatile mm4 u;
    public volatile t60 v;
    public volatile t50 w;

    /* loaded from: classes2.dex */
    public class a extends m.a {
        public a(int i) {
            super(i);
        }

        @Override // androidx.room.m.a
        public void a(sw3 sw3Var) {
            sw3Var.K("CREATE TABLE IF NOT EXISTS `table_wallet` (`_id` INTEGER PRIMARY KEY AUTOINCREMENT, `name` TEXT NOT NULL, `prefix` TEXT NOT NULL, `privateKey` TEXT NOT NULL, `address` TEXT NOT NULL, `passPhrase` TEXT, `ka` TEXT, `u5b64` TEXT, `isLinked` INTEGER NOT NULL, `order` INTEGER NOT NULL DEFAULT 0, `isPrimaryWallet` INTEGER NOT NULL)");
            sw3Var.K("CREATE TABLE IF NOT EXISTS `table_contact` (`_id` INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, `name` TEXT NOT NULL, `address` TEXT NOT NULL, `tokenTypeChain` INTEGER NOT NULL, `profilePath` TEXT NOT NULL, `lastSent` INTEGER, `contactCreate` INTEGER)");
            sw3Var.K("CREATE TABLE IF NOT EXISTS `table_wallet_connect` (`session` TEXT NOT NULL, `peerId` TEXT NOT NULL, `remotePeerId` TEXT NOT NULL, `chainId` INTEGER NOT NULL, `peerMeta` TEXT NOT NULL, `connectedAt` INTEGER NOT NULL, `isAutoDisconnect` INTEGER NOT NULL, `walletId` INTEGER NOT NULL, PRIMARY KEY(`session`))");
            sw3Var.K("CREATE TABLE IF NOT EXISTS room_master_table (id INTEGER PRIMARY KEY,identity_hash TEXT)");
            sw3Var.K("INSERT OR REPLACE INTO room_master_table (id,identity_hash) VALUES(42, '27c0c004e38d0c449a96dd8dfd33bbae')");
        }

        @Override // androidx.room.m.a
        public void b(sw3 sw3Var) {
            sw3Var.K("DROP TABLE IF EXISTS `table_wallet`");
            sw3Var.K("DROP TABLE IF EXISTS `table_contact`");
            sw3Var.K("DROP TABLE IF EXISTS `table_wallet_connect`");
            if (MainRoomDatabase_Impl.this.g != null) {
                int size = MainRoomDatabase_Impl.this.g.size();
                for (int i = 0; i < size; i++) {
                    ((RoomDatabase.b) MainRoomDatabase_Impl.this.g.get(i)).b(sw3Var);
                }
            }
        }

        @Override // androidx.room.m.a
        public void c(sw3 sw3Var) {
            if (MainRoomDatabase_Impl.this.g != null) {
                int size = MainRoomDatabase_Impl.this.g.size();
                for (int i = 0; i < size; i++) {
                    ((RoomDatabase.b) MainRoomDatabase_Impl.this.g.get(i)).a(sw3Var);
                }
            }
        }

        @Override // androidx.room.m.a
        public void d(sw3 sw3Var) {
            MainRoomDatabase_Impl.this.a = sw3Var;
            MainRoomDatabase_Impl.this.x(sw3Var);
            if (MainRoomDatabase_Impl.this.g != null) {
                int size = MainRoomDatabase_Impl.this.g.size();
                for (int i = 0; i < size; i++) {
                    ((RoomDatabase.b) MainRoomDatabase_Impl.this.g.get(i)).c(sw3Var);
                }
            }
        }

        @Override // androidx.room.m.a
        public void e(sw3 sw3Var) {
        }

        @Override // androidx.room.m.a
        public void f(sw3 sw3Var) {
            id0.b(sw3Var);
        }

        @Override // androidx.room.m.a
        public m.b g(sw3 sw3Var) {
            HashMap hashMap = new HashMap(11);
            hashMap.put("_id", new f34.a("_id", "INTEGER", false, 1, null, 1));
            hashMap.put(PublicResolver.FUNC_NAME, new f34.a(PublicResolver.FUNC_NAME, "TEXT", true, 0, null, 1));
            hashMap.put("prefix", new f34.a("prefix", "TEXT", true, 0, null, 1));
            hashMap.put("privateKey", new f34.a("privateKey", "TEXT", true, 0, null, 1));
            hashMap.put(Address.TYPE_NAME, new f34.a(Address.TYPE_NAME, "TEXT", true, 0, null, 1));
            hashMap.put("passPhrase", new f34.a("passPhrase", "TEXT", false, 0, null, 1));
            hashMap.put("ka", new f34.a("ka", "TEXT", false, 0, null, 1));
            hashMap.put("u5b64", new f34.a("u5b64", "TEXT", false, 0, null, 1));
            hashMap.put("isLinked", new f34.a("isLinked", "INTEGER", true, 0, null, 1));
            hashMap.put("order", new f34.a("order", "INTEGER", true, 0, "0", 1));
            hashMap.put("isPrimaryWallet", new f34.a("isPrimaryWallet", "INTEGER", true, 0, null, 1));
            f34 f34Var = new f34("table_wallet", hashMap, new HashSet(0), new HashSet(0));
            f34 a = f34.a(sw3Var, "table_wallet");
            if (!f34Var.equals(a)) {
                return new m.b(false, "table_wallet(net.safemoon.androidwallet.model.wallets.Wallet).\n Expected:\n" + f34Var + "\n Found:\n" + a);
            }
            HashMap hashMap2 = new HashMap(7);
            hashMap2.put("_id", new f34.a("_id", "INTEGER", true, 1, null, 1));
            hashMap2.put(PublicResolver.FUNC_NAME, new f34.a(PublicResolver.FUNC_NAME, "TEXT", true, 0, null, 1));
            hashMap2.put(Address.TYPE_NAME, new f34.a(Address.TYPE_NAME, "TEXT", true, 0, null, 1));
            hashMap2.put("tokenTypeChain", new f34.a("tokenTypeChain", "INTEGER", true, 0, null, 1));
            hashMap2.put("profilePath", new f34.a("profilePath", "TEXT", true, 0, null, 1));
            hashMap2.put("lastSent", new f34.a("lastSent", "INTEGER", false, 0, null, 1));
            hashMap2.put("contactCreate", new f34.a("contactCreate", "INTEGER", false, 0, null, 1));
            f34 f34Var2 = new f34("table_contact", hashMap2, new HashSet(0), new HashSet(0));
            f34 a2 = f34.a(sw3Var, "table_contact");
            if (!f34Var2.equals(a2)) {
                return new m.b(false, "table_contact(net.safemoon.androidwallet.model.contact.room.RoomContact).\n Expected:\n" + f34Var2 + "\n Found:\n" + a2);
            }
            HashMap hashMap3 = new HashMap(8);
            hashMap3.put("session", new f34.a("session", "TEXT", true, 1, null, 1));
            hashMap3.put("peerId", new f34.a("peerId", "TEXT", true, 0, null, 1));
            hashMap3.put("remotePeerId", new f34.a("remotePeerId", "TEXT", true, 0, null, 1));
            hashMap3.put("chainId", new f34.a("chainId", "INTEGER", true, 0, null, 1));
            hashMap3.put("peerMeta", new f34.a("peerMeta", "TEXT", true, 0, null, 1));
            hashMap3.put("connectedAt", new f34.a("connectedAt", "INTEGER", true, 0, null, 1));
            hashMap3.put("isAutoDisconnect", new f34.a("isAutoDisconnect", "INTEGER", true, 0, null, 1));
            hashMap3.put("walletId", new f34.a("walletId", "INTEGER", true, 0, null, 1));
            f34 f34Var3 = new f34("table_wallet_connect", hashMap3, new HashSet(0), new HashSet(0));
            f34 a3 = f34.a(sw3Var, "table_wallet_connect");
            if (!f34Var3.equals(a3)) {
                return new m.b(false, "table_wallet_connect(net.safemoon.androidwallet.model.walletConnect.RoomConnectedInfo).\n Expected:\n" + f34Var3 + "\n Found:\n" + a3);
            }
            return new m.b(true, null);
        }
    }

    @Override // net.safemoon.androidwallet.database.mainRoom.MainRoomDatabase
    public t50 N() {
        t50 t50Var;
        if (this.w != null) {
            return this.w;
        }
        synchronized (this) {
            if (this.w == null) {
                this.w = new net.safemoon.androidwallet.database.mainRoom.a(this);
            }
            t50Var = this.w;
        }
        return t50Var;
    }

    @Override // net.safemoon.androidwallet.database.mainRoom.MainRoomDatabase
    public t60 O() {
        t60 t60Var;
        if (this.v != null) {
            return this.v;
        }
        synchronized (this) {
            if (this.v == null) {
                this.v = new b(this);
            }
            t60Var = this.v;
        }
        return t60Var;
    }

    @Override // net.safemoon.androidwallet.database.mainRoom.MainRoomDatabase
    public mm4 P() {
        mm4 mm4Var;
        if (this.u != null) {
            return this.u;
        }
        synchronized (this) {
            if (this.u == null) {
                this.u = new c(this);
            }
            mm4Var = this.u;
        }
        return mm4Var;
    }

    @Override // androidx.room.RoomDatabase
    public void f() {
        super.c();
        sw3 D0 = super.o().D0();
        try {
            super.e();
            D0.K("DELETE FROM `table_wallet`");
            D0.K("DELETE FROM `table_contact`");
            D0.K("DELETE FROM `table_wallet_connect`");
            super.E();
        } finally {
            super.j();
            D0.E0("PRAGMA wal_checkpoint(FULL)").close();
            if (!D0.h1()) {
                D0.K("VACUUM");
            }
        }
    }

    @Override // androidx.room.RoomDatabase
    public f h() {
        return new f(this, new HashMap(0), new HashMap(0), "table_wallet", "table_contact", "table_wallet_connect");
    }

    @Override // androidx.room.RoomDatabase
    public tw3 i(androidx.room.c cVar) {
        return cVar.a.a(tw3.b.a(cVar.b).c(cVar.c).b(new m(cVar, new a(6), "27c0c004e38d0c449a96dd8dfd33bbae", "06039b68ce9a852cc6c5fb10ee2181c6")).a());
    }

    @Override // androidx.room.RoomDatabase
    public List<w82> k(Map<Class<? extends fk>, fk> map) {
        return Arrays.asList(new w82[0]);
    }

    @Override // androidx.room.RoomDatabase
    public Set<Class<? extends fk>> q() {
        return new HashSet();
    }

    @Override // androidx.room.RoomDatabase
    public Map<Class<?>, List<Class<?>>> r() {
        HashMap hashMap = new HashMap();
        hashMap.put(mm4.class, c.u());
        hashMap.put(t60.class, b.i());
        hashMap.put(t50.class, net.safemoon.androidwallet.database.mainRoom.a.i());
        return hashMap;
    }
}
