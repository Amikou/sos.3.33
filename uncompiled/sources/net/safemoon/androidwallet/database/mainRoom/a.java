package net.safemoon.androidwallet.database.mainRoom;

import android.database.Cursor;
import androidx.lifecycle.LiveData;
import androidx.room.CoroutinesRoom;
import androidx.room.RoomDatabase;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.concurrent.Callable;
import net.safemoon.androidwallet.model.walletConnect.RoomConnectedInfo;
import net.safemoon.androidwallet.model.walletConnect.RoomConnectedInfoAndWallet;
import net.safemoon.androidwallet.model.wallets.Wallet;
import org.web3j.abi.datatypes.Address;
import org.web3j.ens.contracts.generated.PublicResolver;

/* compiled from: ConnectedInfoDao_Impl.java */
/* loaded from: classes2.dex */
public final class a implements t50 {
    public final RoomDatabase a;
    public final zv0<RoomConnectedInfo> b;
    public final co3 c;

    /* compiled from: ConnectedInfoDao_Impl.java */
    /* renamed from: net.safemoon.androidwallet.database.mainRoom.a$a  reason: collision with other inner class name */
    /* loaded from: classes2.dex */
    public class C0200a extends zv0<RoomConnectedInfo> {
        public C0200a(a aVar, RoomDatabase roomDatabase) {
            super(roomDatabase);
        }

        @Override // defpackage.co3
        public String d() {
            return "INSERT OR REPLACE INTO `table_wallet_connect` (`session`,`peerId`,`remotePeerId`,`chainId`,`peerMeta`,`connectedAt`,`isAutoDisconnect`,`walletId`) VALUES (?,?,?,?,?,?,?,?)";
        }

        @Override // defpackage.zv0
        /* renamed from: k */
        public void g(ww3 ww3Var, RoomConnectedInfo roomConnectedInfo) {
            if (roomConnectedInfo.getSession() == null) {
                ww3Var.Y0(1);
            } else {
                ww3Var.L(1, roomConnectedInfo.getSession());
            }
            if (roomConnectedInfo.getPeerId() == null) {
                ww3Var.Y0(2);
            } else {
                ww3Var.L(2, roomConnectedInfo.getPeerId());
            }
            if (roomConnectedInfo.getRemotePeerId() == null) {
                ww3Var.Y0(3);
            } else {
                ww3Var.L(3, roomConnectedInfo.getRemotePeerId());
            }
            ww3Var.q0(4, roomConnectedInfo.getChainId());
            if (roomConnectedInfo.getPeerMeta() == null) {
                ww3Var.Y0(5);
            } else {
                ww3Var.L(5, roomConnectedInfo.getPeerMeta());
            }
            ww3Var.q0(6, roomConnectedInfo.getConnectedAtUnix());
            ww3Var.q0(7, roomConnectedInfo.isAutoDisconnect() ? 1L : 0L);
            ww3Var.q0(8, roomConnectedInfo.getWalletId());
        }
    }

    /* compiled from: ConnectedInfoDao_Impl.java */
    /* loaded from: classes2.dex */
    public class b extends co3 {
        public b(a aVar, RoomDatabase roomDatabase) {
            super(roomDatabase);
        }

        @Override // defpackage.co3
        public String d() {
            return "DELETE FROM table_wallet_connect WHERE session=?";
        }
    }

    /* compiled from: ConnectedInfoDao_Impl.java */
    /* loaded from: classes2.dex */
    public class c implements Callable<te4> {
        public final /* synthetic */ RoomConnectedInfo a;

        public c(RoomConnectedInfo roomConnectedInfo) {
            this.a = roomConnectedInfo;
        }

        @Override // java.util.concurrent.Callable
        /* renamed from: a */
        public te4 call() throws Exception {
            a.this.a.e();
            try {
                a.this.b.h(this.a);
                a.this.a.E();
                return te4.a;
            } finally {
                a.this.a.j();
            }
        }
    }

    /* compiled from: ConnectedInfoDao_Impl.java */
    /* loaded from: classes2.dex */
    public class d implements Callable<te4> {
        public final /* synthetic */ String a;

        public d(String str) {
            this.a = str;
        }

        @Override // java.util.concurrent.Callable
        /* renamed from: a */
        public te4 call() throws Exception {
            ww3 a = a.this.c.a();
            String str = this.a;
            if (str == null) {
                a.Y0(1);
            } else {
                a.L(1, str);
            }
            a.this.a.e();
            try {
                a.T();
                a.this.a.E();
                return te4.a;
            } finally {
                a.this.a.j();
                a.this.c.f(a);
            }
        }
    }

    /* compiled from: ConnectedInfoDao_Impl.java */
    /* loaded from: classes2.dex */
    public class e implements Callable<List<RoomConnectedInfoAndWallet>> {
        public final /* synthetic */ k93 a;

        public e(k93 k93Var) {
            this.a = k93Var;
        }

        /* JADX WARN: Multi-variable type inference failed */
        @Override // java.util.concurrent.Callable
        /* renamed from: a */
        public List<RoomConnectedInfoAndWallet> call() throws Exception {
            RoomConnectedInfo roomConnectedInfo;
            boolean z = true;
            String str = null;
            Cursor c = id0.c(a.this.a, this.a, true, null);
            try {
                int e = wb0.e(c, "session");
                int e2 = wb0.e(c, "peerId");
                int e3 = wb0.e(c, "remotePeerId");
                int e4 = wb0.e(c, "chainId");
                int e5 = wb0.e(c, "peerMeta");
                int e6 = wb0.e(c, "connectedAt");
                int e7 = wb0.e(c, "isAutoDisconnect");
                int e8 = wb0.e(c, "walletId");
                i22 i22Var = new i22();
                while (c.moveToNext()) {
                    i22Var.o(c.getLong(e8), null);
                }
                c.moveToPosition(-1);
                a.this.d(i22Var);
                ArrayList arrayList = new ArrayList(c.getCount());
                while (c.moveToNext()) {
                    if (c.isNull(e) && c.isNull(e2) && c.isNull(e3) && c.isNull(e4) && c.isNull(e5) && c.isNull(e6) && c.isNull(e7) && c.isNull(e8)) {
                        roomConnectedInfo = str;
                        arrayList.add(new RoomConnectedInfoAndWallet(roomConnectedInfo, (Wallet) i22Var.g(c.getLong(e8))));
                        z = true;
                        str = null;
                    }
                    roomConnectedInfo = new RoomConnectedInfo(c.isNull(e) ? str : c.getString(e), c.isNull(e2) ? str : c.getString(e2), c.isNull(e3) ? str : c.getString(e3), c.getInt(e4), c.isNull(e5) ? str : c.getString(e5), c.getLong(e6), c.getInt(e7) != 0 ? z : false, c.getLong(e8));
                    arrayList.add(new RoomConnectedInfoAndWallet(roomConnectedInfo, (Wallet) i22Var.g(c.getLong(e8))));
                    z = true;
                    str = null;
                }
                return arrayList;
            } finally {
                c.close();
            }
        }

        public void finalize() {
            this.a.f();
        }
    }

    public a(RoomDatabase roomDatabase) {
        this.a = roomDatabase;
        this.b = new C0200a(this, roomDatabase);
        this.c = new b(this, roomDatabase);
    }

    public static List<Class<?>> i() {
        return Collections.emptyList();
    }

    @Override // defpackage.t50
    public Object a(RoomConnectedInfo roomConnectedInfo, q70<? super te4> q70Var) {
        return CoroutinesRoom.b(this.a, true, new c(roomConnectedInfo), q70Var);
    }

    @Override // defpackage.t50
    public Object b(String str, q70<? super te4> q70Var) {
        return CoroutinesRoom.b(this.a, true, new d(str), q70Var);
    }

    @Override // defpackage.t50
    public LiveData<List<RoomConnectedInfoAndWallet>> c() {
        return this.a.n().e(new String[]{"table_wallet", "table_wallet_connect"}, false, new e(k93.c("SELECT * FROM table_wallet_connect", 0)));
    }

    public final void d(i22<Wallet> i22Var) {
        boolean z;
        if (i22Var.k()) {
            return;
        }
        if (i22Var.t() > 999) {
            i22<? extends Wallet> i22Var2 = new i22<>(999);
            int t = i22Var.t();
            int i = 0;
            int i2 = 0;
            while (i < t) {
                i22Var2.o(i22Var.l(i), null);
                i++;
                i2++;
                if (i2 == 999) {
                    d(i22Var2);
                    i22Var.p(i22Var2);
                    i22Var2 = new i22<>(999);
                    i2 = 0;
                }
            }
            if (i2 > 0) {
                d(i22Var2);
                i22Var.p(i22Var2);
                return;
            }
            return;
        }
        StringBuilder b2 = qu3.b();
        b2.append("SELECT `_id`,`name`,`prefix`,`privateKey`,`address`,`passPhrase`,`ka`,`u5b64`,`isLinked`,`order`,`isPrimaryWallet` FROM `table_wallet` WHERE `_id` IN (");
        int t2 = i22Var.t();
        qu3.a(b2, t2);
        b2.append(")");
        k93 c2 = k93.c(b2.toString(), t2 + 0);
        int i3 = 1;
        for (int i4 = 0; i4 < i22Var.t(); i4++) {
            c2.q0(i3, i22Var.l(i4));
            i3++;
        }
        Cursor c3 = id0.c(this.a, c2, false, null);
        try {
            int d2 = wb0.d(c3, "_id");
            if (d2 == -1) {
                return;
            }
            int e2 = wb0.e(c3, "_id");
            int e3 = wb0.e(c3, PublicResolver.FUNC_NAME);
            int e4 = wb0.e(c3, "prefix");
            int e5 = wb0.e(c3, "privateKey");
            int e6 = wb0.e(c3, Address.TYPE_NAME);
            int e7 = wb0.e(c3, "passPhrase");
            int e8 = wb0.e(c3, "ka");
            int e9 = wb0.e(c3, "u5b64");
            int e10 = wb0.e(c3, "isLinked");
            int e11 = wb0.e(c3, "order");
            int e12 = wb0.e(c3, "isPrimaryWallet");
            while (c3.moveToNext()) {
                int i5 = e12;
                long j = c3.getLong(d2);
                if (i22Var.e(j)) {
                    Long valueOf = c3.isNull(e2) ? null : Long.valueOf(c3.getLong(e2));
                    String string = c3.isNull(e3) ? null : c3.getString(e3);
                    String string2 = c3.isNull(e4) ? null : c3.getString(e4);
                    String string3 = c3.isNull(e5) ? null : c3.getString(e5);
                    String string4 = c3.isNull(e6) ? null : c3.getString(e6);
                    String string5 = c3.isNull(e7) ? null : c3.getString(e7);
                    String string6 = c3.isNull(e8) ? null : c3.getString(e8);
                    String string7 = c3.isNull(e9) ? null : c3.getString(e9);
                    int i6 = c3.getInt(e10);
                    int i7 = c3.getInt(e11);
                    if (c3.getInt(i5) != 0) {
                        i5 = i5;
                        z = true;
                    } else {
                        i5 = i5;
                        z = false;
                    }
                    i22Var.o(j, new Wallet(valueOf, string, string2, string3, string4, string5, string6, string7, i6, i7, z));
                }
                e12 = i5;
            }
        } finally {
            c3.close();
        }
    }
}
