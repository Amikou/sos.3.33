package net.safemoon.androidwallet.database.mainRoom;

import android.database.Cursor;
import androidx.lifecycle.LiveData;
import androidx.room.CoroutinesRoom;
import androidx.room.RoomDatabase;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.concurrent.Callable;
import net.safemoon.androidwallet.model.contact.room.RoomContact;
import org.web3j.abi.datatypes.Address;
import org.web3j.ens.contracts.generated.PublicResolver;

/* compiled from: ContactDao_MainRoomDatabase_Impl.java */
/* loaded from: classes2.dex */
public final class b implements t60 {
    public final RoomDatabase a;
    public final zv0<RoomContact> b;
    public final yv0<RoomContact> c;
    public final co3 d;
    public final co3 e;

    /* compiled from: ContactDao_MainRoomDatabase_Impl.java */
    /* loaded from: classes2.dex */
    public class a extends zv0<RoomContact> {
        public a(b bVar, RoomDatabase roomDatabase) {
            super(roomDatabase);
        }

        @Override // defpackage.co3
        public String d() {
            return "INSERT OR REPLACE INTO `table_contact` (`_id`,`name`,`address`,`tokenTypeChain`,`profilePath`,`lastSent`,`contactCreate`) VALUES (nullif(?, 0),?,?,?,?,?,?)";
        }

        @Override // defpackage.zv0
        /* renamed from: k */
        public void g(ww3 ww3Var, RoomContact roomContact) {
            ww3Var.q0(1, roomContact.getId());
            if (roomContact.getName() == null) {
                ww3Var.Y0(2);
            } else {
                ww3Var.L(2, roomContact.getName());
            }
            if (roomContact.getAddress() == null) {
                ww3Var.Y0(3);
            } else {
                ww3Var.L(3, roomContact.getAddress());
            }
            ww3Var.q0(4, roomContact.getChainAddress());
            if (roomContact.getProfilePath() == null) {
                ww3Var.Y0(5);
            } else {
                ww3Var.L(5, roomContact.getProfilePath());
            }
            if (roomContact.getLastSent() == null) {
                ww3Var.Y0(6);
            } else {
                ww3Var.q0(6, roomContact.getLastSent().longValue());
            }
            if (roomContact.getContactCreate() == null) {
                ww3Var.Y0(7);
            } else {
                ww3Var.q0(7, roomContact.getContactCreate().longValue());
            }
        }
    }

    /* compiled from: ContactDao_MainRoomDatabase_Impl.java */
    /* renamed from: net.safemoon.androidwallet.database.mainRoom.b$b  reason: collision with other inner class name */
    /* loaded from: classes2.dex */
    public class C0201b extends yv0<RoomContact> {
        public C0201b(b bVar, RoomDatabase roomDatabase) {
            super(roomDatabase);
        }

        @Override // defpackage.co3
        public String d() {
            return "DELETE FROM `table_contact` WHERE `_id` = ?";
        }

        @Override // defpackage.yv0
        /* renamed from: i */
        public void g(ww3 ww3Var, RoomContact roomContact) {
            ww3Var.q0(1, roomContact.getId());
        }
    }

    /* compiled from: ContactDao_MainRoomDatabase_Impl.java */
    /* loaded from: classes2.dex */
    public class c extends co3 {
        public c(b bVar, RoomDatabase roomDatabase) {
            super(roomDatabase);
        }

        @Override // defpackage.co3
        public String d() {
            return "UPDATE table_contact SET lastSent = ? WHERE _id=?";
        }
    }

    /* compiled from: ContactDao_MainRoomDatabase_Impl.java */
    /* loaded from: classes2.dex */
    public class d extends co3 {
        public d(b bVar, RoomDatabase roomDatabase) {
            super(roomDatabase);
        }

        @Override // defpackage.co3
        public String d() {
            return "DELETE FROM table_contact where address=?";
        }
    }

    /* compiled from: ContactDao_MainRoomDatabase_Impl.java */
    /* loaded from: classes2.dex */
    public class e implements Callable<te4> {
        public final /* synthetic */ RoomContact a;

        public e(RoomContact roomContact) {
            this.a = roomContact;
        }

        @Override // java.util.concurrent.Callable
        /* renamed from: a */
        public te4 call() throws Exception {
            b.this.a.e();
            try {
                b.this.b.h(this.a);
                b.this.a.E();
                return te4.a;
            } finally {
                b.this.a.j();
            }
        }
    }

    /* compiled from: ContactDao_MainRoomDatabase_Impl.java */
    /* loaded from: classes2.dex */
    public class f implements Callable<List<RoomContact>> {
        public final /* synthetic */ k93 a;

        public f(k93 k93Var) {
            this.a = k93Var;
        }

        @Override // java.util.concurrent.Callable
        /* renamed from: a */
        public List<RoomContact> call() throws Exception {
            Cursor c = id0.c(b.this.a, this.a, false, null);
            try {
                int e = wb0.e(c, "_id");
                int e2 = wb0.e(c, PublicResolver.FUNC_NAME);
                int e3 = wb0.e(c, Address.TYPE_NAME);
                int e4 = wb0.e(c, "tokenTypeChain");
                int e5 = wb0.e(c, "profilePath");
                int e6 = wb0.e(c, "lastSent");
                int e7 = wb0.e(c, "contactCreate");
                ArrayList arrayList = new ArrayList(c.getCount());
                while (c.moveToNext()) {
                    arrayList.add(new RoomContact(c.getInt(e), c.isNull(e2) ? null : c.getString(e2), c.isNull(e3) ? null : c.getString(e3), c.getInt(e4), c.isNull(e5) ? null : c.getString(e5), c.isNull(e6) ? null : Long.valueOf(c.getLong(e6)), c.isNull(e7) ? null : Long.valueOf(c.getLong(e7))));
                }
                return arrayList;
            } finally {
                c.close();
                this.a.f();
            }
        }
    }

    /* compiled from: ContactDao_MainRoomDatabase_Impl.java */
    /* loaded from: classes2.dex */
    public class g implements Callable<List<RoomContact>> {
        public final /* synthetic */ k93 a;

        public g(k93 k93Var) {
            this.a = k93Var;
        }

        @Override // java.util.concurrent.Callable
        /* renamed from: a */
        public List<RoomContact> call() throws Exception {
            Cursor c = id0.c(b.this.a, this.a, false, null);
            try {
                int e = wb0.e(c, "_id");
                int e2 = wb0.e(c, PublicResolver.FUNC_NAME);
                int e3 = wb0.e(c, Address.TYPE_NAME);
                int e4 = wb0.e(c, "tokenTypeChain");
                int e5 = wb0.e(c, "profilePath");
                int e6 = wb0.e(c, "lastSent");
                int e7 = wb0.e(c, "contactCreate");
                ArrayList arrayList = new ArrayList(c.getCount());
                while (c.moveToNext()) {
                    arrayList.add(new RoomContact(c.getInt(e), c.isNull(e2) ? null : c.getString(e2), c.isNull(e3) ? null : c.getString(e3), c.getInt(e4), c.isNull(e5) ? null : c.getString(e5), c.isNull(e6) ? null : Long.valueOf(c.getLong(e6)), c.isNull(e7) ? null : Long.valueOf(c.getLong(e7))));
                }
                return arrayList;
            } finally {
                c.close();
            }
        }

        public void finalize() {
            this.a.f();
        }
    }

    public b(RoomDatabase roomDatabase) {
        this.a = roomDatabase;
        this.b = new a(this, roomDatabase);
        this.c = new C0201b(this, roomDatabase);
        this.d = new c(this, roomDatabase);
        this.e = new d(this, roomDatabase);
    }

    public static List<Class<?>> i() {
        return Collections.emptyList();
    }

    @Override // defpackage.t60
    public LiveData<List<RoomContact>> a() {
        return this.a.n().e(new String[]{"table_contact"}, false, new g(k93.c("SELECT * FROM table_contact ", 0)));
    }

    @Override // defpackage.t60
    public Object b(String str, int i, q70<? super List<RoomContact>> q70Var) {
        k93 c2 = k93.c("SELECT * FROM table_contact where address=? AND tokenTypeChain=?", 2);
        if (str == null) {
            c2.Y0(1);
        } else {
            c2.L(1, str);
        }
        c2.q0(2, i);
        return CoroutinesRoom.a(this.a, false, id0.a(), new f(c2), q70Var);
    }

    @Override // defpackage.t60
    public void c(String str) {
        this.a.d();
        ww3 a2 = this.e.a();
        if (str == null) {
            a2.Y0(1);
        } else {
            a2.L(1, str);
        }
        this.a.e();
        try {
            a2.T();
            this.a.E();
        } finally {
            this.a.j();
            this.e.f(a2);
        }
    }

    @Override // defpackage.t60
    public void d(int i, long j) {
        this.a.d();
        ww3 a2 = this.d.a();
        a2.q0(1, j);
        a2.q0(2, i);
        this.a.e();
        try {
            a2.T();
            this.a.E();
        } finally {
            this.a.j();
            this.d.f(a2);
        }
    }

    @Override // defpackage.t60
    public void e(RoomContact roomContact) {
        this.a.d();
        this.a.e();
        try {
            this.c.h(roomContact);
            this.a.E();
        } finally {
            this.a.j();
        }
    }

    @Override // defpackage.t60
    public Object f(RoomContact roomContact, q70<? super te4> q70Var) {
        return CoroutinesRoom.b(this.a, true, new e(roomContact), q70Var);
    }
}
