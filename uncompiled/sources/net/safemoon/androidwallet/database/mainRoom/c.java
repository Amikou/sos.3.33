package net.safemoon.androidwallet.database.mainRoom;

import android.database.Cursor;
import androidx.lifecycle.LiveData;
import androidx.room.CoroutinesRoom;
import androidx.room.RoomDatabase;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.concurrent.Callable;
import net.safemoon.androidwallet.model.wallets.Wallet;
import org.web3j.abi.datatypes.Address;
import org.web3j.ens.contracts.generated.PublicResolver;

/* compiled from: WalletDao_Impl.java */
/* loaded from: classes2.dex */
public final class c implements mm4 {
    public final RoomDatabase a;
    public final zv0<Wallet> b;
    public final yv0<Wallet> c;
    public final co3 d;
    public final co3 e;
    public final co3 f;
    public final co3 g;
    public final co3 h;

    /* compiled from: WalletDao_Impl.java */
    /* loaded from: classes2.dex */
    public class a implements Callable<te4> {
        public final /* synthetic */ String a;
        public final /* synthetic */ int b;
        public final /* synthetic */ long c;

        public a(String str, int i, long j) {
            this.a = str;
            this.b = i;
            this.c = j;
        }

        @Override // java.util.concurrent.Callable
        /* renamed from: a */
        public te4 call() throws Exception {
            ww3 a = c.this.d.a();
            String str = this.a;
            if (str == null) {
                a.Y0(1);
            } else {
                a.L(1, str);
            }
            a.q0(2, this.b);
            a.q0(3, this.c);
            c.this.a.e();
            try {
                a.T();
                c.this.a.E();
                return te4.a;
            } finally {
                c.this.a.j();
                c.this.d.f(a);
            }
        }
    }

    /* compiled from: WalletDao_Impl.java */
    /* loaded from: classes2.dex */
    public class b implements Callable<te4> {
        public final /* synthetic */ String a;
        public final /* synthetic */ long b;

        public b(String str, long j) {
            this.a = str;
            this.b = j;
        }

        @Override // java.util.concurrent.Callable
        /* renamed from: a */
        public te4 call() throws Exception {
            ww3 a = c.this.e.a();
            String str = this.a;
            if (str == null) {
                a.Y0(1);
            } else {
                a.L(1, str);
            }
            a.q0(2, this.b);
            c.this.a.e();
            try {
                a.T();
                c.this.a.E();
                return te4.a;
            } finally {
                c.this.a.j();
                c.this.e.f(a);
            }
        }
    }

    /* compiled from: WalletDao_Impl.java */
    /* renamed from: net.safemoon.androidwallet.database.mainRoom.c$c  reason: collision with other inner class name */
    /* loaded from: classes2.dex */
    public class CallableC0202c implements Callable<te4> {
        public final /* synthetic */ int a;
        public final /* synthetic */ long b;

        public CallableC0202c(int i, long j) {
            this.a = i;
            this.b = j;
        }

        @Override // java.util.concurrent.Callable
        /* renamed from: a */
        public te4 call() throws Exception {
            ww3 a = c.this.f.a();
            a.q0(1, this.a);
            a.q0(2, this.b);
            c.this.a.e();
            try {
                a.T();
                c.this.a.E();
                return te4.a;
            } finally {
                c.this.a.j();
                c.this.f.f(a);
            }
        }
    }

    /* compiled from: WalletDao_Impl.java */
    /* loaded from: classes2.dex */
    public class d implements Callable<te4> {
        public final /* synthetic */ String a;
        public final /* synthetic */ String b;
        public final /* synthetic */ String c;
        public final /* synthetic */ String d;
        public final /* synthetic */ int e;
        public final /* synthetic */ String f;
        public final /* synthetic */ long g;

        public d(String str, String str2, String str3, String str4, int i, String str5, long j) {
            this.a = str;
            this.b = str2;
            this.c = str3;
            this.d = str4;
            this.e = i;
            this.f = str5;
            this.g = j;
        }

        @Override // java.util.concurrent.Callable
        /* renamed from: a */
        public te4 call() throws Exception {
            ww3 a = c.this.g.a();
            String str = this.a;
            if (str == null) {
                a.Y0(1);
            } else {
                a.L(1, str);
            }
            String str2 = this.b;
            if (str2 == null) {
                a.Y0(2);
            } else {
                a.L(2, str2);
            }
            String str3 = this.c;
            if (str3 == null) {
                a.Y0(3);
            } else {
                a.L(3, str3);
            }
            String str4 = this.d;
            if (str4 == null) {
                a.Y0(4);
            } else {
                a.L(4, str4);
            }
            a.q0(5, this.e);
            String str5 = this.f;
            if (str5 == null) {
                a.Y0(6);
            } else {
                a.L(6, str5);
            }
            a.q0(7, this.g);
            c.this.a.e();
            try {
                a.T();
                c.this.a.E();
                return te4.a;
            } finally {
                c.this.a.j();
                c.this.g.f(a);
            }
        }
    }

    /* compiled from: WalletDao_Impl.java */
    /* loaded from: classes2.dex */
    public class e implements Callable<te4> {
        public final /* synthetic */ int a;
        public final /* synthetic */ long b;

        public e(int i, long j) {
            this.a = i;
            this.b = j;
        }

        @Override // java.util.concurrent.Callable
        /* renamed from: a */
        public te4 call() throws Exception {
            ww3 a = c.this.h.a();
            a.q0(1, this.a);
            a.q0(2, this.b);
            c.this.a.e();
            try {
                a.T();
                c.this.a.E();
                return te4.a;
            } finally {
                c.this.a.j();
                c.this.h.f(a);
            }
        }
    }

    /* compiled from: WalletDao_Impl.java */
    /* loaded from: classes2.dex */
    public class f implements Callable<List<Wallet>> {
        public final /* synthetic */ k93 a;

        public f(k93 k93Var) {
            this.a = k93Var;
        }

        @Override // java.util.concurrent.Callable
        /* renamed from: a */
        public List<Wallet> call() throws Exception {
            Cursor c = id0.c(c.this.a, this.a, false, null);
            try {
                int e = wb0.e(c, "_id");
                int e2 = wb0.e(c, PublicResolver.FUNC_NAME);
                int e3 = wb0.e(c, "prefix");
                int e4 = wb0.e(c, "privateKey");
                int e5 = wb0.e(c, Address.TYPE_NAME);
                int e6 = wb0.e(c, "passPhrase");
                int e7 = wb0.e(c, "ka");
                int e8 = wb0.e(c, "u5b64");
                int e9 = wb0.e(c, "isLinked");
                int e10 = wb0.e(c, "order");
                int e11 = wb0.e(c, "isPrimaryWallet");
                ArrayList arrayList = new ArrayList(c.getCount());
                while (c.moveToNext()) {
                    arrayList.add(new Wallet(c.isNull(e) ? null : Long.valueOf(c.getLong(e)), c.isNull(e2) ? null : c.getString(e2), c.isNull(e3) ? null : c.getString(e3), c.isNull(e4) ? null : c.getString(e4), c.isNull(e5) ? null : c.getString(e5), c.isNull(e6) ? null : c.getString(e6), c.isNull(e7) ? null : c.getString(e7), c.isNull(e8) ? null : c.getString(e8), c.getInt(e9), c.getInt(e10), c.getInt(e11) != 0));
                }
                return arrayList;
            } finally {
                c.close();
            }
        }

        public void finalize() {
            this.a.f();
        }
    }

    /* compiled from: WalletDao_Impl.java */
    /* loaded from: classes2.dex */
    public class g implements Callable<Integer> {
        public final /* synthetic */ k93 a;

        public g(k93 k93Var) {
            this.a = k93Var;
        }

        @Override // java.util.concurrent.Callable
        /* renamed from: a */
        public Integer call() throws Exception {
            Integer num = null;
            Cursor c = id0.c(c.this.a, this.a, false, null);
            try {
                if (c.moveToFirst() && !c.isNull(0)) {
                    num = Integer.valueOf(c.getInt(0));
                }
                return num;
            } finally {
                c.close();
            }
        }

        public void finalize() {
            this.a.f();
        }
    }

    /* compiled from: WalletDao_Impl.java */
    /* loaded from: classes2.dex */
    public class h implements Callable<List<Wallet>> {
        public final /* synthetic */ k93 a;

        public h(k93 k93Var) {
            this.a = k93Var;
        }

        @Override // java.util.concurrent.Callable
        /* renamed from: a */
        public List<Wallet> call() throws Exception {
            Cursor c = id0.c(c.this.a, this.a, false, null);
            try {
                int e = wb0.e(c, "_id");
                int e2 = wb0.e(c, PublicResolver.FUNC_NAME);
                int e3 = wb0.e(c, "prefix");
                int e4 = wb0.e(c, "privateKey");
                int e5 = wb0.e(c, Address.TYPE_NAME);
                int e6 = wb0.e(c, "passPhrase");
                int e7 = wb0.e(c, "ka");
                int e8 = wb0.e(c, "u5b64");
                int e9 = wb0.e(c, "isLinked");
                int e10 = wb0.e(c, "order");
                int e11 = wb0.e(c, "isPrimaryWallet");
                ArrayList arrayList = new ArrayList(c.getCount());
                while (c.moveToNext()) {
                    arrayList.add(new Wallet(c.isNull(e) ? null : Long.valueOf(c.getLong(e)), c.isNull(e2) ? null : c.getString(e2), c.isNull(e3) ? null : c.getString(e3), c.isNull(e4) ? null : c.getString(e4), c.isNull(e5) ? null : c.getString(e5), c.isNull(e6) ? null : c.getString(e6), c.isNull(e7) ? null : c.getString(e7), c.isNull(e8) ? null : c.getString(e8), c.getInt(e9), c.getInt(e10), c.getInt(e11) != 0));
                }
                return arrayList;
            } finally {
                c.close();
                this.a.f();
            }
        }
    }

    /* compiled from: WalletDao_Impl.java */
    /* loaded from: classes2.dex */
    public class i implements Callable<Wallet> {
        public final /* synthetic */ k93 a;

        public i(k93 k93Var) {
            this.a = k93Var;
        }

        @Override // java.util.concurrent.Callable
        /* renamed from: a */
        public Wallet call() throws Exception {
            Wallet wallet2 = null;
            Cursor c = id0.c(c.this.a, this.a, false, null);
            try {
                int e = wb0.e(c, "_id");
                int e2 = wb0.e(c, PublicResolver.FUNC_NAME);
                int e3 = wb0.e(c, "prefix");
                int e4 = wb0.e(c, "privateKey");
                int e5 = wb0.e(c, Address.TYPE_NAME);
                int e6 = wb0.e(c, "passPhrase");
                int e7 = wb0.e(c, "ka");
                int e8 = wb0.e(c, "u5b64");
                int e9 = wb0.e(c, "isLinked");
                int e10 = wb0.e(c, "order");
                int e11 = wb0.e(c, "isPrimaryWallet");
                if (c.moveToFirst()) {
                    wallet2 = new Wallet(c.isNull(e) ? null : Long.valueOf(c.getLong(e)), c.isNull(e2) ? null : c.getString(e2), c.isNull(e3) ? null : c.getString(e3), c.isNull(e4) ? null : c.getString(e4), c.isNull(e5) ? null : c.getString(e5), c.isNull(e6) ? null : c.getString(e6), c.isNull(e7) ? null : c.getString(e7), c.isNull(e8) ? null : c.getString(e8), c.getInt(e9), c.getInt(e10), c.getInt(e11) != 0);
                }
                return wallet2;
            } finally {
                c.close();
                this.a.f();
            }
        }
    }

    /* compiled from: WalletDao_Impl.java */
    /* loaded from: classes2.dex */
    public class j implements Callable<Boolean> {
        public final /* synthetic */ k93 a;

        public j(k93 k93Var) {
            this.a = k93Var;
        }

        @Override // java.util.concurrent.Callable
        /* renamed from: a */
        public Boolean call() throws Exception {
            Boolean bool = null;
            Cursor c = id0.c(c.this.a, this.a, false, null);
            try {
                if (c.moveToFirst()) {
                    Integer valueOf = c.isNull(0) ? null : Integer.valueOf(c.getInt(0));
                    if (valueOf != null) {
                        bool = Boolean.valueOf(valueOf.intValue() != 0);
                    }
                }
                return bool;
            } finally {
                c.close();
                this.a.f();
            }
        }
    }

    /* compiled from: WalletDao_Impl.java */
    /* loaded from: classes2.dex */
    public class k extends zv0<Wallet> {
        public k(c cVar, RoomDatabase roomDatabase) {
            super(roomDatabase);
        }

        @Override // defpackage.co3
        public String d() {
            return "INSERT OR REPLACE INTO `table_wallet` (`_id`,`name`,`prefix`,`privateKey`,`address`,`passPhrase`,`ka`,`u5b64`,`isLinked`,`order`,`isPrimaryWallet`) VALUES (?,?,?,?,?,?,?,?,?,?,?)";
        }

        @Override // defpackage.zv0
        /* renamed from: k */
        public void g(ww3 ww3Var, Wallet wallet2) {
            if (wallet2.getId() == null) {
                ww3Var.Y0(1);
            } else {
                ww3Var.q0(1, wallet2.getId().longValue());
            }
            if (wallet2.getName() == null) {
                ww3Var.Y0(2);
            } else {
                ww3Var.L(2, wallet2.getName());
            }
            if (wallet2.getPrefix() == null) {
                ww3Var.Y0(3);
            } else {
                ww3Var.L(3, wallet2.getPrefix());
            }
            if (wallet2.getPrivateKey() == null) {
                ww3Var.Y0(4);
            } else {
                ww3Var.L(4, wallet2.getPrivateKey());
            }
            if (wallet2.getAddress() == null) {
                ww3Var.Y0(5);
            } else {
                ww3Var.L(5, wallet2.getAddress());
            }
            if (wallet2.getPassPhrase() == null) {
                ww3Var.Y0(6);
            } else {
                ww3Var.L(6, wallet2.getPassPhrase());
            }
            if (wallet2.getKA() == null) {
                ww3Var.Y0(7);
            } else {
                ww3Var.L(7, wallet2.getKA());
            }
            if (wallet2.getU5B64() == null) {
                ww3Var.Y0(8);
            } else {
                ww3Var.L(8, wallet2.getU5B64());
            }
            ww3Var.q0(9, wallet2.getLinkedState());
            ww3Var.q0(10, wallet2.getOrder());
            ww3Var.q0(11, wallet2.isPrimaryWallet() ? 1L : 0L);
        }
    }

    /* compiled from: WalletDao_Impl.java */
    /* loaded from: classes2.dex */
    public class l extends yv0<Wallet> {
        public l(c cVar, RoomDatabase roomDatabase) {
            super(roomDatabase);
        }

        @Override // defpackage.co3
        public String d() {
            return "DELETE FROM `table_wallet` WHERE `_id` = ?";
        }

        @Override // defpackage.yv0
        /* renamed from: i */
        public void g(ww3 ww3Var, Wallet wallet2) {
            if (wallet2.getId() == null) {
                ww3Var.Y0(1);
            } else {
                ww3Var.q0(1, wallet2.getId().longValue());
            }
        }
    }

    /* compiled from: WalletDao_Impl.java */
    /* loaded from: classes2.dex */
    public class m extends co3 {
        public m(c cVar, RoomDatabase roomDatabase) {
            super(roomDatabase);
        }

        @Override // defpackage.co3
        public String d() {
            return "UPDATE table_wallet SET name = ?, isLinked = ? WHERE _id=?";
        }
    }

    /* compiled from: WalletDao_Impl.java */
    /* loaded from: classes2.dex */
    public class n extends co3 {
        public n(c cVar, RoomDatabase roomDatabase) {
            super(roomDatabase);
        }

        @Override // defpackage.co3
        public String d() {
            return "UPDATE table_wallet SET name = ? WHERE _id=?";
        }
    }

    /* compiled from: WalletDao_Impl.java */
    /* loaded from: classes2.dex */
    public class o extends co3 {
        public o(c cVar, RoomDatabase roomDatabase) {
            super(roomDatabase);
        }

        @Override // defpackage.co3
        public String d() {
            return "UPDATE table_wallet SET isLinked = ? WHERE _id=?";
        }
    }

    /* compiled from: WalletDao_Impl.java */
    /* loaded from: classes2.dex */
    public class p extends co3 {
        public p(c cVar, RoomDatabase roomDatabase) {
            super(roomDatabase);
        }

        @Override // defpackage.co3
        public String d() {
            return "UPDATE table_wallet SET ka = ?, u5b64= ?, privateKey=?, passPhrase=?, `order`=?, name=? WHERE _id=?";
        }
    }

    /* compiled from: WalletDao_Impl.java */
    /* loaded from: classes2.dex */
    public class q extends co3 {
        public q(c cVar, RoomDatabase roomDatabase) {
            super(roomDatabase);
        }

        @Override // defpackage.co3
        public String d() {
            return "UPDATE table_wallet SET `order` = ? WHERE _id=?";
        }
    }

    /* compiled from: WalletDao_Impl.java */
    /* loaded from: classes2.dex */
    public class r implements Callable<Long> {
        public final /* synthetic */ Wallet a;

        public r(Wallet wallet2) {
            this.a = wallet2;
        }

        @Override // java.util.concurrent.Callable
        /* renamed from: a */
        public Long call() throws Exception {
            c.this.a.e();
            try {
                long j = c.this.b.j(this.a);
                c.this.a.E();
                return Long.valueOf(j);
            } finally {
                c.this.a.j();
            }
        }
    }

    /* compiled from: WalletDao_Impl.java */
    /* loaded from: classes2.dex */
    public class s implements Callable<te4> {
        public final /* synthetic */ Wallet a;

        public s(Wallet wallet2) {
            this.a = wallet2;
        }

        @Override // java.util.concurrent.Callable
        /* renamed from: a */
        public te4 call() throws Exception {
            c.this.a.e();
            try {
                c.this.c.h(this.a);
                c.this.a.E();
                return te4.a;
            } finally {
                c.this.a.j();
            }
        }
    }

    public c(RoomDatabase roomDatabase) {
        this.a = roomDatabase;
        this.b = new k(this, roomDatabase);
        this.c = new l(this, roomDatabase);
        this.d = new m(this, roomDatabase);
        this.e = new n(this, roomDatabase);
        this.f = new o(this, roomDatabase);
        this.g = new p(this, roomDatabase);
        this.h = new q(this, roomDatabase);
    }

    public static List<Class<?>> u() {
        return Collections.emptyList();
    }

    @Override // defpackage.mm4
    public Object a(long j2, int i2, q70<? super te4> q70Var) {
        return CoroutinesRoom.b(this.a, true, new e(i2, j2), q70Var);
    }

    @Override // defpackage.mm4
    public LiveData<List<Wallet>> b() {
        return this.a.n().e(new String[]{"table_wallet"}, false, new f(k93.c("SELECT * FROM table_wallet ORDER BY `order` ASC", 0)));
    }

    @Override // defpackage.mm4
    public Object c(Wallet wallet2, q70<? super te4> q70Var) {
        return CoroutinesRoom.b(this.a, true, new s(wallet2), q70Var);
    }

    @Override // defpackage.mm4
    public Object d(long j2, String str, String str2, String str3, String str4, int i2, String str5, q70<? super te4> q70Var) {
        return CoroutinesRoom.b(this.a, true, new d(str3, str4, str, str2, i2, str5, j2), q70Var);
    }

    @Override // defpackage.mm4
    public Object e(long j2, int i2, q70<? super te4> q70Var) {
        return CoroutinesRoom.b(this.a, true, new CallableC0202c(i2, j2), q70Var);
    }

    @Override // defpackage.mm4
    public Object f(q70<? super List<Wallet>> q70Var) {
        k93 c = k93.c("SELECT * FROM table_wallet ORDER BY `order` ASC", 0);
        return CoroutinesRoom.a(this.a, false, id0.a(), new h(c), q70Var);
    }

    @Override // defpackage.mm4
    public Object g(long j2, String str, int i2, q70<? super te4> q70Var) {
        return CoroutinesRoom.b(this.a, true, new a(str, i2, j2), q70Var);
    }

    @Override // defpackage.mm4
    public Object h(long j2, q70<? super Wallet> q70Var) {
        k93 c = k93.c("SELECT * FROM table_wallet where _id=?", 1);
        c.q0(1, j2);
        return CoroutinesRoom.a(this.a, false, id0.a(), new i(c), q70Var);
    }

    @Override // defpackage.mm4
    public Object i(Wallet wallet2, q70<? super Long> q70Var) {
        return CoroutinesRoom.b(this.a, true, new r(wallet2), q70Var);
    }

    @Override // defpackage.mm4
    public Object j(String str, q70<? super Boolean> q70Var) {
        k93 c = k93.c("SELECT EXISTS (SELECT 1 FROM table_wallet WHERE UPPER(address) = UPPER(?))", 1);
        if (str == null) {
            c.Y0(1);
        } else {
            c.L(1, str);
        }
        return CoroutinesRoom.a(this.a, false, id0.a(), new j(c), q70Var);
    }

    @Override // defpackage.mm4
    public Object k(long j2, String str, q70<? super te4> q70Var) {
        return CoroutinesRoom.b(this.a, true, new b(str, j2), q70Var);
    }

    @Override // defpackage.mm4
    public LiveData<Integer> l() {
        return this.a.n().e(new String[]{"table_wallet"}, false, new g(k93.c("SELECT COUNT(*) FROM table_wallet", 0)));
    }
}
