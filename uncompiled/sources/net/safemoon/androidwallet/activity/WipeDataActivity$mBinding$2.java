package net.safemoon.androidwallet.activity;

import kotlin.jvm.internal.Lambda;

/* compiled from: WipeDataActivity.kt */
/* loaded from: classes2.dex */
public final class WipeDataActivity$mBinding$2 extends Lambda implements rc1<e8> {
    public final /* synthetic */ WipeDataActivity this$0;

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public WipeDataActivity$mBinding$2(WipeDataActivity wipeDataActivity) {
        super(0);
        this.this$0 = wipeDataActivity;
    }

    /* JADX WARN: Can't rename method to resolve collision */
    @Override // defpackage.rc1
    public final e8 invoke() {
        return e8.c(this.this$0.getLayoutInflater());
    }
}
