package net.safemoon.androidwallet.activity;

import android.content.Context;
import android.content.Intent;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import androidx.appcompat.app.ActionBar;
import androidx.fragment.app.FragmentManager;
import com.AKT.anonymouskey.ui.login.AKTServerFunctions;
import com.fasterxml.jackson.core.util.MinimalPrettyPrinter;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Objects;
import kotlin.text.StringsKt__StringsKt;
import net.safemoon.androidwallet.MyApplicationClass;
import net.safemoon.androidwallet.R;
import net.safemoon.androidwallet.activity.StartWalletActivity;
import net.safemoon.androidwallet.common.TokenType;
import net.safemoon.androidwallet.dialogs.ProgressLoading;
import net.safemoon.androidwallet.viewmodels.AKTWebSocketHandlingViewModel;
import net.safemoon.androidwallet.viewmodels.MultiWalletViewModel;
import wallet.core.jni.CoinType;
import wallet.core.jni.HDWallet;

/* compiled from: StartWalletActivity.kt */
/* loaded from: classes2.dex */
public final class StartWalletActivity extends AKTServerFunctions {
    public static final a y0 = new a(null);
    public boolean n0;
    public boolean o0;
    public boolean p0;
    public int v0;
    public final sy1 m0 = new fj4(d53.b(AKTWebSocketHandlingViewModel.class), new StartWalletActivity$special$$inlined$viewModels$default$2(this), new StartWalletActivity$special$$inlined$viewModels$default$1(this));
    public final sy1 q0 = zy1.a(new StartWalletActivity$isRegister$2(this));
    public final sy1 r0 = zy1.a(new StartWalletActivity$masterMnemonic$2(this));
    public final sy1 s0 = zy1.a(new StartWalletActivity$isImportNewWalletOnRegister$2(this));
    public final sy1 t0 = zy1.a(new StartWalletActivity$binding$2(this));
    public final List<String> u0 = new ArrayList();
    public final List<String> w0 = new ArrayList();
    public final Map<String, String> x0 = new LinkedHashMap();

    /* compiled from: StartWalletActivity.kt */
    /* loaded from: classes2.dex */
    public static final class a {
        public a() {
        }

        public /* synthetic */ a(qi0 qi0Var) {
            this();
        }

        public static /* synthetic */ void b(a aVar, Context context, boolean z, String str, boolean z2, int i, Object obj) {
            if ((i & 2) != 0) {
                z = false;
            }
            if ((i & 8) != 0) {
                z2 = false;
            }
            aVar.a(context, z, str, z2);
        }

        public final void a(Context context, boolean z, String str, boolean z2) {
            fs1.f(context, "context");
            fs1.f(str, "masterMnemonic");
            Intent intent = new Intent(context, StartWalletActivity.class);
            intent.putExtra("KEY_START_WHEN_REGISTER", z);
            intent.putExtra("KEY_MASTER_MNEMONIC", str);
            intent.putExtra("KEY_IS_IMPORT_NEW_WALLET", z2);
            context.startActivity(intent);
        }
    }

    public static /* synthetic */ void G0(StartWalletActivity startWalletActivity, boolean z, int i, Object obj) {
        if ((i & 1) != 0) {
            z = true;
        }
        startWalletActivity.F0(z);
    }

    public static final void J0(StartWalletActivity startWalletActivity) {
        fs1.f(startWalletActivity, "this$0");
        startWalletActivity.U0(AKTServerFunctions.T(startWalletActivity));
    }

    public static final void Q0(StartWalletActivity startWalletActivity, String str) {
        fs1.f(startWalletActivity, "this$0");
        if (str == null) {
            return;
        }
        qy4 qy4Var = startWalletActivity.l0;
        fs1.e(qy4Var, "safeMoonppp");
        startWalletActivity.X(qy4Var, str);
        startWalletActivity.Q();
    }

    /* JADX WARN: Multi-variable type inference failed */
    public final void B0(String str) {
        boolean z;
        String D;
        List w0 = StringsKt__StringsKt.w0(str, new String[]{","}, false, 0, 6, null);
        if (w0.size() >= 3) {
            String e = w.e(this, (String) w0.get(2));
            String str2 = null;
            if (e != null && (D = dv3.D(e, "|", MinimalPrettyPrinter.DEFAULT_ROOT_VALUE_SEPARATOR, false, 4, null)) != null) {
                str2 = StringsKt__StringsKt.K0(D).toString();
            }
            if (str2 != null) {
                if (str2.length() > 0) {
                    z = true;
                    if (z && HDWallet.isValid(str2)) {
                        HDWallet hDWallet = new HDWallet(str2, "");
                        CoinType coinType = CoinType.ETHEREUM;
                        String addressForCoin = hDWallet.getAddressForCoin(coinType);
                        byte[] data = hDWallet.getKeyForCoin(coinType).data();
                        fs1.e(data, "secretPrivateKey.data()");
                        String f0 = e30.f0(data, false);
                        List<String> list = this.w0;
                        fs1.e(addressForCoin, "addressEth");
                        list.add(addressForCoin);
                        this.x0.put(addressForCoin, w0.get(0));
                        MultiWalletViewModel multiWalletViewModel = this.k0;
                        fs1.e(multiWalletViewModel, "walletViewModel");
                        fs1.e(e, "mnemonic");
                        multiWalletViewModel.m(f0, addressForCoin, e, (r17 & 8) != 0 ? null : (String) w0.get(0), (r17 & 16) != 0, (r17 & 32) != 0 ? false : false, new StartWalletActivity$addWalletToDatabase$1(this));
                        return;
                    }
                }
            }
            z = false;
            if (z) {
                HDWallet hDWallet2 = new HDWallet(str2, "");
                CoinType coinType2 = CoinType.ETHEREUM;
                String addressForCoin2 = hDWallet2.getAddressForCoin(coinType2);
                byte[] data2 = hDWallet2.getKeyForCoin(coinType2).data();
                fs1.e(data2, "secretPrivateKey.data()");
                String f02 = e30.f0(data2, false);
                List<String> list2 = this.w0;
                fs1.e(addressForCoin2, "addressEth");
                list2.add(addressForCoin2);
                this.x0.put(addressForCoin2, w0.get(0));
                MultiWalletViewModel multiWalletViewModel2 = this.k0;
                fs1.e(multiWalletViewModel2, "walletViewModel");
                fs1.e(e, "mnemonic");
                multiWalletViewModel2.m(f02, addressForCoin2, e, (r17 & 8) != 0 ? null : (String) w0.get(0), (r17 & 16) != 0, (r17 & 32) != 0 ? false : false, new StartWalletActivity$addWalletToDatabase$1(this));
                return;
            }
        }
        if (w0.size() >= 2) {
            String str3 = (String) w0.get(1);
            ma0 create = ma0.create(str3);
            List<String> list3 = this.w0;
            String address = create.getAddress();
            fs1.e(address, "credential.address");
            list3.add(address);
            Map<String, String> map = this.x0;
            String address2 = create.getAddress();
            fs1.e(address2, "credential.address");
            map.put(address2, w0.get(0));
            MultiWalletViewModel multiWalletViewModel3 = this.k0;
            fs1.e(multiWalletViewModel3, "walletViewModel");
            String address3 = create.getAddress();
            fs1.e(address3, "credential.address");
            multiWalletViewModel3.m(str3, address3, "", (r17 & 8) != 0 ? null : (String) w0.get(0), (r17 & 16) != 0, (r17 & 32) != 0 ? false : false, new StartWalletActivity$addWalletToDatabase$2(this));
            return;
        }
        int i = this.v0 + 1;
        this.v0 = i;
        if (i >= this.u0.size()) {
            D0();
        } else {
            B0(this.u0.get(this.v0));
        }
    }

    public final void C0() {
        this.k0.p(new StartWalletActivity$checkActiveWalletBeforeGoHome$1(this));
    }

    public final void D0() {
        if (!this.p0 && !S0()) {
            G0(this, false, 1, null);
            return;
        }
        this.k0.p(new StartWalletActivity$checkSyncAllWalletsByDefault$1(this, new StringBuilder()));
    }

    public final String E0(String str) {
        String i = bo3.i(this, "U5");
        String Z = AKTServerFunctions.Z(this, ((Object) i) + '>' + str);
        fs1.e(Z, "putBlob(this, newBlob)");
        return Z;
    }

    public final void F0(boolean z) {
        this.k0.p(new StartWalletActivity$finishHandleBlob$1(e30.c(this), this, z));
    }

    public final a8 H0() {
        return (a8) this.t0.getValue();
    }

    public final void I0() {
        new Handler(Looper.getMainLooper()).post(new Runnable() { // from class: ss3
            @Override // java.lang.Runnable
            public final void run() {
                StartWalletActivity.J0(StartWalletActivity.this);
            }
        });
    }

    public final String K0() {
        return (String) this.r0.getValue();
    }

    public final AKTWebSocketHandlingViewModel L0() {
        return (AKTWebSocketHandlingViewModel) this.m0.getValue();
    }

    public final void M0() {
        if (this.n0) {
            ProgressLoading progressLoading = this.j0;
            if (progressLoading != null) {
                progressLoading.h();
            }
            if (R0()) {
                N0();
            } else {
                MyApplicationClass.c().m0 = true;
                AKTHomeActivity.R0(this);
                finish();
            }
        }
        this.o0 = true;
    }

    public final void N0() {
        startActivity(new Intent(this, AKTImportWalletOptionActivity.class));
        finish();
    }

    public final void O0(String str) {
        this.u0.addAll(StringsKt__StringsKt.w0(str, new String[]{"|"}, false, 0, 6, null));
        this.v0 = 0;
        e30.c(this);
        if (!this.u0.isEmpty()) {
            B0(this.u0.get(this.v0));
        } else {
            D0();
        }
    }

    public void P0() {
        L0().h().observe(this, new tl2() { // from class: rs3
            @Override // defpackage.tl2
            public final void onChanged(Object obj) {
                StartWalletActivity.Q0(StartWalletActivity.this, (String) obj);
            }
        });
    }

    @Override // com.AKT.anonymouskey.ui.login.AKTServerFunctions
    public void Q() {
        L0().f();
    }

    public final boolean R0() {
        return ((Boolean) this.s0.getValue()).booleanValue();
    }

    public final boolean S0() {
        return ((Boolean) this.q0.getValue()).booleanValue();
    }

    public final void T0(String str) {
        String K0 = K0();
        fs1.e(K0, "masterMnemonic");
        if (K0.length() == 0) {
            X0();
            return;
        }
        String D = dv3.D(str, "|", MinimalPrettyPrinter.DEFAULT_ROOT_VALUE_SEPARATOR, false, 4, null);
        Objects.requireNonNull(D, "null cannot be cast to non-null type kotlin.CharSequence");
        HDWallet hDWallet = new HDWallet(StringsKt__StringsKt.K0(D).toString(), "");
        CoinType coinType = CoinType.ETHEREUM;
        String addressForCoin = hDWallet.getAddressForCoin(coinType);
        byte[] data = hDWallet.getKeyForCoin(coinType).data();
        fs1.e(data, "secretPrivateKey.data()");
        String f0 = e30.f0(data, false);
        MultiWalletViewModel multiWalletViewModel = this.k0;
        fs1.e(addressForCoin, "addressEth");
        multiWalletViewModel.m(f0, addressForCoin, str, "Primary Wallet", false, true, new StartWalletActivity$proceedMasterWallet$1(this));
    }

    public void U0(String str) {
        if (str == null) {
            return;
        }
        L0().k(str);
    }

    public final void V0() {
        this.k0.p(new StartWalletActivity$updateImportedWalletAfterLogin$1(this));
    }

    public final void W0() {
        bo3.o(this, "DEFAULT_GATEWAY", TokenType.BEP_20.getName());
        if (e30.c(this) == null) {
            C0();
        } else {
            this.k0.p(new StartWalletActivity$updateImportedWalletAfterRegister$1(this));
        }
    }

    @Override // com.AKT.anonymouskey.ui.login.AKTServerFunctions
    public void X(qy4 qy4Var, String str) {
        fs1.f(qy4Var, "safeMoonppp");
        if (W(str)) {
            C0();
            return;
        }
        String[] v = i2.v(b30.a.v(this, str), "|");
        fs1.e(v, "parts");
        if (!(v.length == 0)) {
            String str2 = v[0];
            fs1.e(str2, "parts[0]");
            if (StringsKt__StringsKt.w0(str2, new String[]{"="}, false, 0, 6, null).size() >= 2) {
                String str3 = v[0];
                fs1.e(str3, "parts[0]");
                String str4 = (String) StringsKt__StringsKt.w0(str3, new String[]{"="}, false, 0, 6, null).get(1);
                Objects.requireNonNull(str4, "null cannot be cast to non-null type java.lang.String");
                String upperCase = str4.toUpperCase(Locale.ROOT);
                fs1.e(upperCase, "(this as java.lang.Strin….toUpperCase(Locale.ROOT)");
                Q();
                int hashCode = upperCase.hashCode();
                if (hashCode != -1549417970) {
                    if (hashCode != -282391275) {
                        if (hashCode == 325952731 && upperCase.equals("AKTSERVERERROR")) {
                            G0(this, false, 1, null);
                            return;
                        }
                    } else if (upperCase.equals("GETBLOB02")) {
                        Q();
                        this.u0.clear();
                        this.w0.clear();
                        this.x0.clear();
                        if (v.length > 2) {
                            String str5 = v[1];
                            fs1.e(str5, "u5EncryptBlob");
                            if (StringsKt__StringsKt.M(str5, "BLOB=", false, 2, null) && StringsKt__StringsKt.M(str5, ">", false, 2, null)) {
                                Object[] array = StringsKt__StringsKt.w0(str5, new String[]{">"}, false, 0, 6, null).toArray(new String[0]);
                                Objects.requireNonNull(array, "null cannot be cast to non-null type kotlin.Array<T>");
                                String a2 = w.a(this, ((String[]) array)[1]);
                                fs1.e(a2, "decryptedBlob");
                                O0(a2);
                                return;
                            }
                            D0();
                            return;
                        }
                        G0(this, false, 1, null);
                        return;
                    }
                } else if (upperCase.equals("PUTBLOB02")) {
                    this.k0.p(new StartWalletActivity$parseMessage$1(this));
                    return;
                }
                super.X(qy4Var, str);
            }
        }
    }

    public final void X0() {
        String j = bo3.j(this, "DEFAULT_GATEWAY", "");
        fs1.e(j, "getString(this, SharedPrefs.DEFAULT_GATEWAY, \"\")");
        if (j.length() == 0) {
            bo3.o(this, "DEFAULT_GATEWAY", TokenType.BEP_20.getName());
        }
        I0();
    }

    @Override // androidx.activity.ComponentActivity, android.app.Activity
    public void onBackPressed() {
    }

    @Override // com.AKT.anonymouskey.ui.login.AKTServerFunctions, net.safemoon.androidwallet.activity.common.BasicActivity, androidx.fragment.app.FragmentActivity, androidx.activity.ComponentActivity, androidx.core.app.ComponentActivity, android.app.Activity
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        setContentView(H0().b());
        L0().l();
        P0();
        ActionBar supportActionBar = getSupportActionBar();
        if (supportActionBar != null) {
            supportActionBar.l();
        }
        getWindow().addFlags(Integer.MIN_VALUE);
        getWindow().setStatusBarColor(m70.d(this, 17170445));
        getWindow().setBackgroundDrawable(new ColorDrawable(getColor(R.color.akt_night_background)));
        ProgressLoading.a aVar = ProgressLoading.y0;
        String string = getString(R.string.loading);
        fs1.e(string, "getString(R.string.loading)");
        ProgressLoading a2 = aVar.a(false, string, "");
        this.j0 = a2;
        FragmentManager supportFragmentManager = getSupportFragmentManager();
        fs1.e(supportFragmentManager, "supportFragmentManager");
        a2.A(supportFragmentManager);
        this.k0.p(new StartWalletActivity$onCreate$1(this));
        if (bo3.c(this, "TEMPKA")) {
            bo3.k(this, "TEMPKA");
        }
    }

    @Override // androidx.fragment.app.FragmentActivity, android.app.Activity
    public void onPause() {
        this.n0 = false;
        super.onPause();
    }

    @Override // androidx.fragment.app.FragmentActivity, android.app.Activity
    public void onResume() {
        super.onResume();
        this.n0 = true;
        if (this.o0) {
            M0();
        }
    }
}
