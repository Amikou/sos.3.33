package net.safemoon.androidwallet.activity;

import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.content.res.Configuration;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.IBinder;
import android.os.Looper;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import androidx.lifecycle.l;
import androidx.navigation.NavController;
import androidx.navigation.d;
import androidx.navigation.e;
import com.github.mikephil.charting.utils.Utils;
import com.google.android.material.bottomnavigation.BottomNavigationView;
import com.google.android.material.navigation.NavigationBarView;
import defpackage.af;
import java.lang.ref.WeakReference;
import java.util.List;
import java.util.Objects;
import net.safemoon.androidwallet.MyApplicationClass;
import net.safemoon.androidwallet.R;
import net.safemoon.androidwallet.activity.AKTHomeActivity;
import net.safemoon.androidwallet.activity.common.CloseByDoubleBackActivity;
import net.safemoon.androidwallet.common.TokenType;
import net.safemoon.androidwallet.model.navMap.NavTreeDirection;
import net.safemoon.androidwallet.model.request.RequestTransaction;
import net.safemoon.androidwallet.model.token.room.RoomToken;
import net.safemoon.androidwallet.model.wallets.Wallet;
import net.safemoon.androidwallet.service.MyFirebaseMessagingService;
import net.safemoon.androidwallet.service.TxnListService;
import net.safemoon.androidwallet.utils.StoragePermissionLauncher;
import net.safemoon.androidwallet.viewmodels.AKTWebSocketHandlingViewModel;
import net.safemoon.androidwallet.viewmodels.HomeViewModel;
import net.safemoon.androidwallet.viewmodels.MyTokensListViewModel;
import net.safemoon.androidwallet.viewmodels.ReflectionTrackerViewModel;
import net.safemoon.androidwallet.viewmodels.SwapViewModel;
import net.safemoon.androidwallet.viewmodels.factory.MyViewModelFactory;
import net.safemoon.androidwallet.viewmodels.wc.MultipleWalletConnect;
import net.safemoon.androidwallet.views.CustomKeyBoard;
import zendesk.core.AnonymousIdentity;
import zendesk.core.Zendesk;
import zendesk.support.Support;

/* loaded from: classes2.dex */
public class AKTHomeActivity extends CloseByDoubleBackActivity implements gm1, om2 {
    public static String A0 = "https://safemoon.com/buy/moonpay";
    public static String B0 = "safemoon";
    public static String z0 = "wc";
    public f7 n0;
    public MyTokensListViewModel o0;
    public HomeViewModel p0;
    public SwapViewModel q0;
    public Intent r0;
    public af s0;
    public NavController t0;
    public AKTWebSocketHandlingViewModel v0;
    public boolean w0;
    public TxnListService x0;
    public MultipleWalletConnect u0 = null;
    public ServiceConnection y0 = new b();

    /* loaded from: classes2.dex */
    public class a implements NavController.b {
        public a() {
        }

        @Override // androidx.navigation.NavController.b
        public void a(NavController navController, d dVar, Bundle bundle) {
            e30.G(AKTHomeActivity.this);
        }
    }

    /* loaded from: classes2.dex */
    public class b implements ServiceConnection {
        public b() {
        }

        @Override // android.content.ServiceConnection
        public void onServiceConnected(ComponentName componentName, IBinder iBinder) {
            AKTHomeActivity.this.x0 = ((TxnListService.b) iBinder).a();
            AKTHomeActivity.this.w0 = true;
        }

        @Override // android.content.ServiceConnection
        public void onServiceDisconnected(ComponentName componentName) {
            AKTHomeActivity.this.w0 = false;
        }
    }

    /* JADX INFO: Access modifiers changed from: private */
    public /* synthetic */ void C0() {
        Integer lastDestination = NavTreeDirection.INSTANCE.getLastDestination(this.t0, R.id.navigation_wallet);
        if (lastDestination != null) {
            this.t0.z(lastDestination.intValue(), false);
        } else {
            this.t0.p(R.id.navigation_wallet);
        }
        this.p0.k().postValue(Boolean.TRUE);
    }

    /* JADX INFO: Access modifiers changed from: private */
    public /* synthetic */ void D0(TokenType tokenType) {
        this.o0.J(tokenType);
    }

    /* JADX INFO: Access modifiers changed from: private */
    public /* synthetic */ void E0(String str) {
        if (str == null || str.isEmpty()) {
            return;
        }
        X(this.l0, str);
        Q();
    }

    /* JADX INFO: Access modifiers changed from: private */
    public /* synthetic */ boolean F0(MenuItem menuItem) {
        Integer lastDestination = NavTreeDirection.INSTANCE.getLastDestination(this.t0, menuItem.getItemId());
        if (lastDestination != null) {
            this.t0.z(lastDestination.intValue(), false);
            return true;
        }
        this.t0.p(menuItem.getItemId());
        return true;
    }

    /* JADX INFO: Access modifiers changed from: private */
    public /* synthetic */ void G0(MenuItem menuItem) {
        this.t0.z(menuItem.getItemId(), false);
        this.t0.y();
        this.t0.p(menuItem.getItemId());
    }

    /* JADX INFO: Access modifiers changed from: private */
    public /* synthetic */ void H0(int i) {
        this.t0.p(i);
    }

    /* JADX INFO: Access modifiers changed from: private */
    public /* synthetic */ void I0() {
        if (isFinishing() || isDestroyed()) {
            return;
        }
        this.n0.d.setVisibility(8);
    }

    /* JADX INFO: Access modifiers changed from: private */
    public /* synthetic */ te4 J0() {
        this.t0.t(wm4.f());
        return null;
    }

    /* JADX INFO: Access modifiers changed from: private */
    public /* synthetic */ te4 K0() {
        bo3.n(this, "V2_HELP_POP", Boolean.FALSE);
        return null;
    }

    /* JADX INFO: Access modifiers changed from: private */
    public /* synthetic */ void L0(Bundle bundle, RoomToken roomToken) {
        if (roomToken == null || roomToken.getNativeBalance() <= Utils.DOUBLE_EPSILON) {
            return;
        }
        if (bundle == null) {
            P0();
        }
        this.o0.x().removeObservers(this);
    }

    /* JADX INFO: Access modifiers changed from: private */
    public /* synthetic */ te4 M0(List list) {
        if (list.size() > 1 && ((Wallet) list.get(0)).getOrder() == 0 && ((Wallet) list.get(1)).getOrder() == 0) {
            this.k0.F(list);
            return null;
        }
        return null;
    }

    /* JADX INFO: Access modifiers changed from: private */
    public /* synthetic */ void N0() {
        this.p0.o().postValue(Boolean.FALSE);
    }

    public static void R0(Context context) {
        Intent intent = new Intent(context, AKTHomeActivity.class);
        intent.setFlags(268468224);
        intent.putExtra("ARG_START_NEW_HOME", true);
        context.startActivity(intent);
    }

    public static void S0(Context context, Integer num) {
        MyApplicationClass.c().i0 = true;
        Intent intent = new Intent(context, AKTHomeActivity.class);
        intent.setFlags(268468224);
        intent.putExtra("ACTION_ID", num);
        intent.putExtra("ARG_START_NEW_HOME", true);
        context.startActivity(intent);
    }

    public void A0() {
        this.v0.h().observe(this, new tl2() { // from class: j0
            @Override // defpackage.tl2
            public final void onChanged(Object obj) {
                AKTHomeActivity.this.E0((String) obj);
            }
        });
    }

    public final void B0() {
        Zendesk zendesk2 = Zendesk.INSTANCE;
        zendesk2.init(this, "https://safemoon.zendesk.com", a4.f, a4.g);
        zendesk2.setIdentity(new AnonymousIdentity());
        Support.INSTANCE.init(zendesk2);
    }

    public final void O0() {
        pg4.e(this);
    }

    public final void P0() {
        no0.x0.a(new rc1() { // from class: h0
            @Override // defpackage.rc1
            public final Object invoke() {
                te4 J0;
                J0 = AKTHomeActivity.this.J0();
                return J0;
            }
        }, new rc1() { // from class: e0
            @Override // defpackage.rc1
            public final Object invoke() {
                te4 K0;
                K0 = AKTHomeActivity.this.K0();
                return K0;
            }
        }).D(getSupportFragmentManager());
    }

    @Override // com.AKT.anonymouskey.ui.login.AKTServerFunctions
    public void Q() {
        this.v0.f();
    }

    public final void Q0(final Bundle bundle) {
        if (bo3.e(this, "V2_HELP_POP", true)) {
            this.o0.x().observe(this, new tl2() { // from class: l0
                @Override // defpackage.tl2
                public final void onChanged(Object obj) {
                    AKTHomeActivity.this.L0(bundle, (RoomToken) obj);
                }
            });
        }
    }

    public final void T0() {
        this.k0.p(new tc1() { // from class: i0
            @Override // defpackage.tc1
            public final Object invoke(Object obj) {
                te4 M0;
                M0 = AKTHomeActivity.this.M0((List) obj);
                return M0;
            }
        });
    }

    public final void U0() {
        this.p0.o().postValue(Boolean.TRUE);
        this.q0.U0();
        this.q0.X0();
        new Handler(Looper.getMainLooper()).postDelayed(new Runnable() { // from class: o0
            @Override // java.lang.Runnable
            public final void run() {
                AKTHomeActivity.this.N0();
            }
        }, 100L);
    }

    @Override // defpackage.gm1
    public hn2 a() {
        return D();
    }

    @Override // defpackage.gm1
    public void b() {
        MyTokensListViewModel myTokensListViewModel = this.o0;
        if (myTokensListViewModel != null) {
            myTokensListViewModel.K();
        }
    }

    @Override // defpackage.gm1
    public void c(RequestTransaction requestTransaction) {
        if (this.w0) {
            this.x0.q(requestTransaction);
        }
    }

    @Override // defpackage.gm1
    public StoragePermissionLauncher d() {
        return E();
    }

    @Override // net.safemoon.androidwallet.activity.common.CloseByDoubleBackActivity
    public ViewGroup g0() {
        return this.n0.c;
    }

    @Override // defpackage.gm1
    public void h(int i) {
        this.t0.z(i, false);
        this.t0.y();
        this.t0.p(i);
    }

    @Override // defpackage.om2
    public void i(boolean z) {
        BottomNavigationView bottomNavigationView = this.n0.e;
        if (bottomNavigationView != null) {
            bottomNavigationView.setVisibility(z ? 8 : 0);
        }
    }

    @Override // defpackage.gm1
    public Intent j() {
        return G();
    }

    @Override // defpackage.gm1
    public View l() {
        return findViewById(R.id.nav_host_fragment);
    }

    @Override // defpackage.gm1
    public MultipleWalletConnect m() {
        return this.u0;
    }

    @Override // defpackage.gm1
    public CustomKeyBoard n() {
        return (CustomKeyBoard) findViewById(R.id.keyWrapper);
    }

    @Override // net.safemoon.androidwallet.activity.common.CloseByDoubleBackActivity, androidx.activity.ComponentActivity, android.app.Activity
    public void onBackPressed() {
        if (getOnBackPressedDispatcher().c()) {
            getOnBackPressedDispatcher().d();
        } else if (this.t0.w()) {
        } else {
            super.onBackPressed();
        }
    }

    @Override // androidx.appcompat.app.AppCompatActivity, androidx.fragment.app.FragmentActivity, android.app.Activity, android.content.ComponentCallbacks
    public void onConfigurationChanged(Configuration configuration) {
        super.onConfigurationChanged(configuration);
    }

    @Override // com.AKT.anonymouskey.ui.login.AKTServerFunctions, net.safemoon.androidwallet.activity.common.BasicActivity, androidx.fragment.app.FragmentActivity, androidx.activity.ComponentActivity, androidx.core.app.ComponentActivity, android.app.Activity
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        if (y0()) {
            return;
        }
        this.u0 = new MultipleWalletConnect(this);
        this.r0 = new Intent(this, TxnListService.class);
        f7 c = f7.c(getLayoutInflater());
        this.n0 = c;
        setContentView(c.b());
        MyTokensListViewModel myTokensListViewModel = (MyTokensListViewModel) new l(this, new MyViewModelFactory(new WeakReference(this))).a(MyTokensListViewModel.class);
        this.o0 = myTokensListViewModel;
        myTokensListViewModel.n();
        this.p0 = (HomeViewModel) new l(this).a(HomeViewModel.class);
        this.q0 = (SwapViewModel) new l(this).a(SwapViewModel.class);
        AKTWebSocketHandlingViewModel aKTWebSocketHandlingViewModel = (AKTWebSocketHandlingViewModel) new l(this).a(AKTWebSocketHandlingViewModel.class);
        this.v0 = aKTWebSocketHandlingViewModel;
        aKTWebSocketHandlingViewModel.l();
        A0();
        z0(this.p0);
        new l(this).a(ReflectionTrackerViewModel.class);
        if (getSupportActionBar() != null) {
            getSupportActionBar().l();
        }
        getWindow().addFlags(Integer.MIN_VALUE);
        getWindow().setStatusBarColor(m70.d(this, 17170445));
        getWindow().setBackgroundDrawableResource(R.drawable.bottom_curve);
        this.n0.b.setVisibility(8);
        this.n0.e.setVisibility(0);
        this.s0 = new af.b(R.id.navigation_wallet, R.id.navigation_swap, R.id.navigation_collectibles, R.id.navigation_settings).a();
        this.t0 = he2.a(this, R.id.nav_host_fragment);
        if (MyApplicationClass.c().h0) {
            e c2 = this.t0.l().c(R.navigation.mobile_navigation);
            c2.O(do3.a.b(this));
            try {
                this.t0.G(c2);
            } catch (Exception unused) {
            }
            MyApplicationClass.c().h0 = false;
        }
        oe2.e(this, this.t0, this.s0);
        oe2.f(this.n0.e, this.t0);
        this.n0.e.setOnItemSelectedListener(new NavigationBarView.d() { // from class: n0
            @Override // com.google.android.material.navigation.NavigationBarView.d
            public final boolean a(MenuItem menuItem) {
                boolean F0;
                F0 = AKTHomeActivity.this.F0(menuItem);
                return F0;
            }
        });
        this.n0.e.setOnItemReselectedListener(new NavigationBarView.c() { // from class: m0
            @Override // com.google.android.material.navigation.NavigationBarView.c
            public final void a(MenuItem menuItem) {
                AKTHomeActivity.this.G0(menuItem);
            }
        });
        this.t0.a(new a());
        if (MyApplicationClass.c().i0) {
            MyApplicationClass.c().i0 = false;
            try {
                final int intExtra = getIntent().getIntExtra("ACTION_ID", 0);
                if (intExtra != 0) {
                    new Handler(Looper.getMainLooper()).post(new Runnable() { // from class: g0
                        @Override // java.lang.Runnable
                        public final void run() {
                            AKTHomeActivity.this.H0(intExtra);
                        }
                    });
                }
            } catch (Exception unused2) {
            }
        }
        w0(getIntent());
        x0(getIntent());
        v0(getIntent());
        new Handler(Looper.getMainLooper()).postDelayed(new Runnable() { // from class: p0
            @Override // java.lang.Runnable
            public final void run() {
                AKTHomeActivity.this.I0();
            }
        }, 500L);
        T0();
        B0();
        if (MyApplicationClass.c().m0) {
            return;
        }
        x();
        MyApplicationClass.c().m0 = true;
    }

    @Override // androidx.appcompat.app.AppCompatActivity, androidx.fragment.app.FragmentActivity, android.app.Activity
    public void onDestroy() {
        super.onDestroy();
        MyApplicationClass.c().h0 = true;
    }

    @Override // androidx.fragment.app.FragmentActivity, android.app.Activity
    public void onNewIntent(Intent intent) {
        super.onNewIntent(intent);
        try {
            if (intent.getData() != null && intent.getData().getScheme().startsWith(z0)) {
                x0(intent);
            } else if (intent.getData() != null && (intent.getData().toString().contains(A0) || intent.getData().getScheme().startsWith(B0))) {
                v0(intent);
            } else if (intent.hasExtra(MyFirebaseMessagingService.k0)) {
                w0(intent);
            } else if (intent.hasExtra(MyFirebaseMessagingService.l0)) {
                U0();
            } else {
                boolean booleanExtra = intent.getBooleanExtra("IS_BACK_FROM_CHANGE_EMAIL", false);
                if (intent.getBooleanExtra("ARG_START_NEW_HOME", false) || booleanExtra) {
                    return;
                }
                w();
            }
        } catch (Exception unused) {
            w();
        }
    }

    @Override // androidx.fragment.app.FragmentActivity, android.app.Activity
    public void onPause() {
        super.onPause();
    }

    @Override // net.safemoon.androidwallet.activity.common.BasicActivity, androidx.appcompat.app.AppCompatActivity, android.app.Activity
    public void onPostCreate(Bundle bundle) {
        super.onPostCreate(bundle);
        if (y0()) {
            return;
        }
        I(this);
        O0();
        C();
        if (MyApplicationClass.c().g0) {
            MyApplicationClass.c().g0 = false;
            Q0(bundle);
        }
    }

    @Override // androidx.appcompat.app.AppCompatActivity, androidx.fragment.app.FragmentActivity, android.app.Activity
    public void onPostResume() {
        super.onPostResume();
        MultipleWalletConnect multipleWalletConnect = this.u0;
        if (multipleWalletConnect != null) {
            multipleWalletConnect.v();
        }
    }

    @Override // androidx.fragment.app.FragmentActivity, android.app.Activity
    public void onResume() {
        super.onResume();
    }

    @Override // net.safemoon.androidwallet.activity.common.BasicActivity, androidx.appcompat.app.AppCompatActivity, androidx.fragment.app.FragmentActivity, android.app.Activity
    public void onStart() {
        super.onStart();
        if (y0()) {
            startActivity(new Intent(this, AKTActivity.class));
            finish();
            return;
        }
        if (Build.VERSION.SDK_INT >= 26) {
            startForegroundService(this.r0);
        } else {
            startService(this.r0);
        }
        bindService(this.r0, this.y0, 1);
    }

    @Override // net.safemoon.androidwallet.activity.common.BasicActivity, androidx.appcompat.app.AppCompatActivity, androidx.fragment.app.FragmentActivity, android.app.Activity
    public void onStop() {
        super.onStop();
        if (this.w0) {
            unbindService(this.y0);
            this.w0 = false;
            stopService(this.r0);
        }
    }

    @Override // defpackage.gm1
    public void p() {
        this.n0.e.setSelectedItemId(R.id.navigation_wallet);
    }

    public final void v0(Intent intent) {
        try {
            if (intent.getData() != null) {
                if (intent.getData().toString().contains(A0) || intent.getData().getScheme().startsWith(B0)) {
                    new Handler(Looper.getMainLooper()).post(new Runnable() { // from class: f0
                        @Override // java.lang.Runnable
                        public final void run() {
                            AKTHomeActivity.this.C0();
                        }
                    });
                }
            }
        } catch (Exception unused) {
        }
    }

    public final void w0(Intent intent) {
        try {
            if (intent.hasExtra(MyFirebaseMessagingService.k0)) {
                this.t0.t(wm4.i(intent.getStringExtra(MyFirebaseMessagingService.k0), true));
            }
        } catch (Exception unused) {
        }
    }

    public final void x0(Intent intent) {
        try {
            if (intent.getData() == null || !intent.getData().getScheme().startsWith(z0)) {
                return;
            }
            this.p0.n().postValue(intent.getData().toString());
            d i = this.t0.i();
            Objects.requireNonNull(i);
            if (i.s() != R.id.walletConnectFragment) {
                this.t0.t(wm4.j());
            }
        } catch (Exception unused) {
        }
    }

    public final boolean y0() {
        return e30.c(this) == null || bo3.i(this, "KA").isEmpty();
    }

    public final void z0(HomeViewModel homeViewModel) {
        homeViewModel.l().observe(this, new tl2() { // from class: k0
            @Override // defpackage.tl2
            public final void onChanged(Object obj) {
                AKTHomeActivity.this.D0((TokenType) obj);
            }
        });
    }
}
