package net.safemoon.androidwallet.activity;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.KeyEvent;
import android.view.View;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.TextView;
import androidx.appcompat.app.ActionBar;
import defpackage.qm1;
import net.safemoon.androidwallet.R;
import net.safemoon.androidwallet.activity.EnterPasswordActivity;
import net.safemoon.androidwallet.activity.common.BasicActivity;

/* compiled from: EnterPasswordActivity.kt */
@SuppressLint({"ClickableViewAccessibility"})
/* loaded from: classes2.dex */
public final class EnterPasswordActivity extends BasicActivity {
    public static final String l0;
    public final sy1 j0 = zy1.a(new EnterPasswordActivity$mBinding$2(this));
    public qm1 k0 = new w41();

    /* compiled from: EnterPasswordActivity.kt */
    /* loaded from: classes2.dex */
    public static final class a {
        public a() {
        }

        public /* synthetic */ a(qi0 qi0Var) {
            this();
        }
    }

    /* compiled from: EnterPasswordActivity.kt */
    /* loaded from: classes2.dex */
    public static final class b implements qm1.a {
        public final /* synthetic */ boolean b;

        public b(boolean z) {
            this.b = z;
        }

        @Override // defpackage.qm1.a
        public void a() {
            EnterPasswordActivity.this.b0(this.b);
            bo3.n(EnterPasswordActivity.this, "TWO_FACTOR", Boolean.valueOf(this.b));
        }

        @Override // defpackage.qm1.a
        public void b(int i) {
            EnterPasswordActivity.this.T().c.setChecked(!this.b);
            EnterPasswordActivity.this.b0(!this.b);
            bo3.n(EnterPasswordActivity.this, "TWO_FACTOR", Boolean.valueOf(!this.b));
        }
    }

    /* compiled from: EnterPasswordActivity.kt */
    /* loaded from: classes2.dex */
    public static final class c implements TextWatcher {
        public c() {
        }

        @Override // android.text.TextWatcher
        public void afterTextChanged(Editable editable) {
            fs1.f(editable, "s");
            if (editable.toString().length() > 0) {
                EnterPasswordActivity.this.T().e.setVisibility(8);
            }
        }

        @Override // android.text.TextWatcher
        public void beforeTextChanged(CharSequence charSequence, int i, int i2, int i3) {
            fs1.f(charSequence, "s");
        }

        @Override // android.text.TextWatcher
        public void onTextChanged(CharSequence charSequence, int i, int i2, int i3) {
            fs1.f(charSequence, "s");
        }
    }

    /* compiled from: EnterPasswordActivity.kt */
    /* loaded from: classes2.dex */
    public static final class d implements qm1.a {
        public d() {
        }

        @Override // defpackage.qm1.a
        public void a() {
            y54.a(EnterPasswordActivity.l0).a("OnBioAuthenticationCallback: onSucceed", new Object[0]);
            EnterPasswordActivity.this.Z();
        }

        @Override // defpackage.qm1.a
        public void b(int i) {
            y54.a(EnterPasswordActivity.l0).a(fs1.l("OnBioAuthenticationCallback: error", Integer.valueOf(i)), new Object[0]);
        }
    }

    static {
        new a(null);
        String simpleName = EnterPasswordActivity.class.getSimpleName();
        fs1.e(simpleName, "EnterPasswordActivity::class.java.simpleName");
        l0 = simpleName;
    }

    public static final void V(EnterPasswordActivity enterPasswordActivity, CompoundButton compoundButton, boolean z) {
        fs1.f(enterPasswordActivity, "this$0");
        enterPasswordActivity.k0.b(enterPasswordActivity, new b(z));
    }

    public static final void W(EnterPasswordActivity enterPasswordActivity, View view) {
        fs1.f(enterPasswordActivity, "this$0");
        enterPasswordActivity.a0();
    }

    public static final boolean X(EnterPasswordActivity enterPasswordActivity, TextView textView, int i, KeyEvent keyEvent) {
        fs1.f(enterPasswordActivity, "this$0");
        if ((keyEvent == null || keyEvent.getKeyCode() != 66) && i != 6) {
            return false;
        }
        enterPasswordActivity.S();
        return false;
    }

    public static final void Y(EnterPasswordActivity enterPasswordActivity, View view) {
        fs1.f(enterPasswordActivity, "this$0");
        enterPasswordActivity.startActivity(new Intent(enterPasswordActivity, WipeDataActivity.class));
    }

    public final void S() {
        String j = bo3.j(this, "SAFEMOON_PASSWORD", "");
        fs1.e(j, "getString(this, SharedPrefs.SAFEMOON_PASSWORD, \"\")");
        EditText editText = T().d.getEditText();
        if (fs1.b(String.valueOf(editText == null ? null : editText.getText()), j)) {
            Z();
            return;
        }
        T().e.setVisibility(0);
        EditText editText2 = T().d.getEditText();
        if (editText2 == null) {
            return;
        }
        editText2.setText("");
    }

    public final c7 T() {
        return (c7) this.j0.getValue();
    }

    public final void U() {
        boolean z = bo3.e(this, "TWO_FACTOR", false) && e30.c(this) != null;
        boolean e = bo3.e(this, "TWO_FACTOR", false);
        T().c.setChecked(e);
        b0(e);
        T().c.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() { // from class: wv0
            @Override // android.widget.CompoundButton.OnCheckedChangeListener
            public final void onCheckedChanged(CompoundButton compoundButton, boolean z2) {
                EnterPasswordActivity.V(EnterPasswordActivity.this, compoundButton, z2);
            }
        });
        T().b.setOnClickListener(new View.OnClickListener() { // from class: vv0
            @Override // android.view.View.OnClickListener
            public final void onClick(View view) {
                EnterPasswordActivity.W(EnterPasswordActivity.this, view);
            }
        });
        EditText editText = T().d.getEditText();
        if (editText != null) {
            editText.setOnEditorActionListener(new TextView.OnEditorActionListener() { // from class: xv0
                @Override // android.widget.TextView.OnEditorActionListener
                public final boolean onEditorAction(TextView textView, int i, KeyEvent keyEvent) {
                    boolean X;
                    X = EnterPasswordActivity.X(EnterPasswordActivity.this, textView, i, keyEvent);
                    return X;
                }
            });
        }
        EditText editText2 = T().d.getEditText();
        if (editText2 != null) {
            editText2.addTextChangedListener(new c());
        }
        if (z && pg4.d(this)) {
            a0();
        }
    }

    public final void Z() {
        startActivity(new Intent(this, AKTRegisterGuideActivity.class));
    }

    public final void a0() {
        this.k0.b(this, new d());
    }

    public final void b0(boolean z) {
        if (z) {
            T().b.setVisibility(0);
        } else {
            T().b.setVisibility(8);
        }
    }

    @Override // androidx.activity.ComponentActivity, android.app.Activity
    public void onBackPressed() {
    }

    @Override // net.safemoon.androidwallet.activity.common.BasicActivity, androidx.fragment.app.FragmentActivity, androidx.activity.ComponentActivity, androidx.core.app.ComponentActivity, android.app.Activity
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        setContentView(T().b());
        ActionBar supportActionBar = getSupportActionBar();
        if (supportActionBar != null) {
            supportActionBar.l();
        }
        getWindow().addFlags(Integer.MIN_VALUE);
        getWindow().setStatusBarColor(m70.d(this, 17170445));
        getWindow().setBackgroundDrawable(new ColorDrawable(getColor(R.color.akt_night_background)));
        U();
        T().f.setOnClickListener(new View.OnClickListener() { // from class: uv0
            @Override // android.view.View.OnClickListener
            public final void onClick(View view) {
                EnterPasswordActivity.Y(EnterPasswordActivity.this, view);
            }
        });
    }
}
