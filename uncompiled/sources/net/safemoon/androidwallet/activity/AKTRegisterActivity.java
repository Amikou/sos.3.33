package net.safemoon.androidwallet.activity;

import android.content.Context;
import android.content.Intent;
import android.content.res.ColorStateList;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.EditText;
import androidx.appcompat.app.ActionBar;
import androidx.fragment.app.FragmentManager;
import com.AKT.anonymouskey.ui.login.AKTServerFunctions;
import java.lang.ref.WeakReference;
import java.util.Locale;
import java.util.Objects;
import kotlin.text.StringsKt__StringsKt;
import net.safemoon.androidwallet.R;
import net.safemoon.androidwallet.activity.AKTRegisterActivity;
import net.safemoon.androidwallet.dialogs.ProgressLoading;
import net.safemoon.androidwallet.viewmodels.AKTWebSocketHandlingViewModel;

/* compiled from: AKTRegisterActivity.kt */
/* loaded from: classes2.dex */
public final class AKTRegisterActivity extends AKTServerFunctions {
    public static final a q0 = new a(null);
    public final sy1 m0 = zy1.a(new AKTRegisterActivity$binding$2(this));
    public final sy1 n0 = new fj4(d53.b(AKTWebSocketHandlingViewModel.class), new AKTRegisterActivity$special$$inlined$viewModels$default$2(this), new AKTRegisterActivity$special$$inlined$viewModels$default$1(this));
    public String o0 = "";
    public String p0 = "";

    /* compiled from: AKTRegisterActivity.kt */
    /* loaded from: classes2.dex */
    public static final class a {
        public a() {
        }

        public /* synthetic */ a(qi0 qi0Var) {
            this();
        }

        public final void a(Context context) {
            fs1.f(context, "context");
            context.startActivity(new Intent(context, AKTRegisterActivity.class));
        }
    }

    /* compiled from: AKTRegisterActivity.kt */
    /* loaded from: classes2.dex */
    public static final class b implements TextWatcher {
        public final /* synthetic */ q7 a;
        public final /* synthetic */ AKTRegisterActivity f0;

        public b(q7 q7Var, AKTRegisterActivity aKTRegisterActivity) {
            this.a = q7Var;
            this.f0 = aKTRegisterActivity;
        }

        @Override // android.text.TextWatcher
        public void afterTextChanged(Editable editable) {
            fs1.f(editable, "s");
            String obj = editable.toString();
            String b = b30.a.b(obj);
            if (fs1.b(obj, b)) {
                this.f0.o0();
                return;
            }
            EditText editText = this.a.f.getEditText();
            if (editText != null) {
                editText.setText(b);
            }
            EditText editText2 = this.a.f.getEditText();
            if (editText2 == null) {
                return;
            }
            editText2.setSelection(b.length());
        }

        @Override // android.text.TextWatcher
        public void beforeTextChanged(CharSequence charSequence, int i, int i2, int i3) {
            fs1.f(charSequence, "s");
        }

        @Override // android.text.TextWatcher
        public void onTextChanged(CharSequence charSequence, int i, int i2, int i3) {
            fs1.f(charSequence, "s");
        }
    }

    /* compiled from: AKTRegisterActivity.kt */
    /* loaded from: classes2.dex */
    public static final class c implements TextWatcher {
        public final /* synthetic */ q7 a;
        public final /* synthetic */ AKTRegisterActivity f0;

        public c(q7 q7Var, AKTRegisterActivity aKTRegisterActivity) {
            this.a = q7Var;
            this.f0 = aKTRegisterActivity;
        }

        @Override // android.text.TextWatcher
        public void afterTextChanged(Editable editable) {
            fs1.f(editable, "s");
            String obj = editable.toString();
            String b = b30.a.b(obj);
            if (fs1.b(obj, b)) {
                this.f0.o0();
                this.f0.p0(editable);
                return;
            }
            EditText editText = this.a.e.getEditText();
            if (editText != null) {
                editText.setText(b);
            }
            EditText editText2 = this.a.e.getEditText();
            if (editText2 == null) {
                return;
            }
            editText2.setSelection(b.length());
        }

        @Override // android.text.TextWatcher
        public void beforeTextChanged(CharSequence charSequence, int i, int i2, int i3) {
            fs1.f(charSequence, "s");
        }

        @Override // android.text.TextWatcher
        public void onTextChanged(CharSequence charSequence, int i, int i2, int i3) {
            fs1.f(charSequence, "s");
        }
    }

    /* compiled from: AKTRegisterActivity.kt */
    /* loaded from: classes2.dex */
    public static final class d implements TextWatcher {
        public final /* synthetic */ q7 a;
        public final /* synthetic */ AKTRegisterActivity f0;

        public d(q7 q7Var, AKTRegisterActivity aKTRegisterActivity) {
            this.a = q7Var;
            this.f0 = aKTRegisterActivity;
        }

        @Override // android.text.TextWatcher
        public void afterTextChanged(Editable editable) {
            fs1.f(editable, "s");
            String obj = editable.toString();
            String b = b30.a.b(obj);
            if (fs1.b(obj, b)) {
                this.f0.o0();
                return;
            }
            EditText editText = this.a.d.getEditText();
            if (editText != null) {
                editText.setText(b);
            }
            EditText editText2 = this.a.d.getEditText();
            if (editText2 == null) {
                return;
            }
            editText2.setSelection(b.length());
        }

        @Override // android.text.TextWatcher
        public void beforeTextChanged(CharSequence charSequence, int i, int i2, int i3) {
            fs1.f(charSequence, "s");
        }

        @Override // android.text.TextWatcher
        public void onTextChanged(CharSequence charSequence, int i, int i2, int i3) {
            fs1.f(charSequence, "s");
        }
    }

    public static final void n0(AKTRegisterActivity aKTRegisterActivity, View view) {
        fs1.f(aKTRegisterActivity, "this$0");
        ProgressLoading.a aVar = ProgressLoading.y0;
        String string = aKTRegisterActivity.getString(R.string.loading);
        fs1.e(string, "getString(R.string.loading)");
        ProgressLoading a2 = aVar.a(false, string, "");
        aKTRegisterActivity.j0 = a2;
        FragmentManager supportFragmentManager = aKTRegisterActivity.getSupportFragmentManager();
        fs1.e(supportFragmentManager, "supportFragmentManager");
        a2.A(supportFragmentManager);
        EditText editText = aKTRegisterActivity.q0().f.getEditText();
        aKTRegisterActivity.o0 = String.valueOf(editText == null ? null : editText.getText());
        EditText editText2 = aKTRegisterActivity.q0().e.getEditText();
        aKTRegisterActivity.p0 = String.valueOf(editText2 != null ? editText2.getText() : null);
        b30 b30Var = b30.a;
        aKTRegisterActivity.w0(aKTRegisterActivity.O(b30Var.n(aKTRegisterActivity.o0), b30Var.n(aKTRegisterActivity.p0)));
    }

    public static final void t0(AKTRegisterActivity aKTRegisterActivity, String str) {
        fs1.f(aKTRegisterActivity, "this$0");
        if (str == null) {
            return;
        }
        aKTRegisterActivity.X(aKTRegisterActivity.l0, str);
        aKTRegisterActivity.Q();
    }

    public static final void u0(AKTRegisterActivity aKTRegisterActivity, View view) {
        fs1.f(aKTRegisterActivity, "this$0");
        aKTRegisterActivity.onBackPressed();
    }

    public static final void v0(AKTRegisterActivity aKTRegisterActivity, View view) {
        fs1.f(aKTRegisterActivity, "this$0");
        pg4.e(aKTRegisterActivity);
    }

    @Override // com.AKT.anonymouskey.ui.login.AKTServerFunctions
    public void Q() {
        r0().f();
    }

    @Override // com.AKT.anonymouskey.ui.login.AKTServerFunctions
    public void X(qy4 qy4Var, String str) {
        if (W(str)) {
            return;
        }
        this.j0.h();
        b30 b30Var = b30.a;
        String[] v = i2.v(b30Var.v(this, str), "|");
        fs1.e(v, "parts");
        if (!(v.length == 0)) {
            String str2 = v[0];
            fs1.e(str2, "parts[0]");
            if (StringsKt__StringsKt.w0(str2, new String[]{"="}, false, 0, 6, null).size() >= 2) {
                String str3 = v[0];
                fs1.e(str3, "parts[0]");
                String str4 = (String) StringsKt__StringsKt.w0(str3, new String[]{"="}, false, 0, 6, null).get(1);
                Objects.requireNonNull(str4, "null cannot be cast to non-null type java.lang.String");
                String upperCase = str4.toUpperCase(Locale.ROOT);
                fs1.e(upperCase, "(this as java.lang.Strin….toUpperCase(Locale.ROOT)");
                Q();
                if (fs1.b(upperCase, "AKTSERVERERROR")) {
                    AKTSecurityQuestionsActivity.E0.a(this, b30Var.n(this.o0), b30Var.n(this.p0), this.o0);
                } else {
                    Y(v, null);
                }
            }
        }
    }

    @Override // com.AKT.anonymouskey.ui.login.AKTServerFunctions
    public void Y(String[] strArr, String str) {
        bh.V(new WeakReference(this), null, Integer.valueOf((int) R.string.akt_error_password_error_message), 0, null, 26, null);
    }

    public final void m0() {
        q0().b.setOnClickListener(new View.OnClickListener() { // from class: a2
            @Override // android.view.View.OnClickListener
            public final void onClick(View view) {
                AKTRegisterActivity.n0(AKTRegisterActivity.this, view);
            }
        });
    }

    /* JADX WARN: Removed duplicated region for block: B:41:0x0067  */
    /* JADX WARN: Removed duplicated region for block: B:42:0x0069  */
    /* JADX WARN: Removed duplicated region for block: B:45:0x0079  */
    /* JADX WARN: Removed duplicated region for block: B:46:0x007b  */
    /* JADX WARN: Removed duplicated region for block: B:54:0x0098  */
    /* JADX WARN: Removed duplicated region for block: B:64:0x00bc  */
    /* JADX WARN: Removed duplicated region for block: B:67:0x00d2  */
    /* JADX WARN: Removed duplicated region for block: B:68:0x00d4  */
    /* JADX WARN: Removed duplicated region for block: B:70:0x00d7  */
    /* JADX WARN: Removed duplicated region for block: B:77:0x00f4  */
    /* JADX WARN: Removed duplicated region for block: B:78:0x0112  */
    /*
        Code decompiled incorrectly, please refer to instructions dump.
        To view partially-correct code enable 'Show inconsistent code' option in preferences
    */
    public final void o0() {
        /*
            Method dump skipped, instructions count: 304
            To view this dump change 'Code comments level' option to 'DEBUG'
        */
        throw new UnsupportedOperationException("Method not decompiled: net.safemoon.androidwallet.activity.AKTRegisterActivity.o0():void");
    }

    @Override // com.AKT.anonymouskey.ui.login.AKTServerFunctions, net.safemoon.androidwallet.activity.common.BasicActivity, androidx.fragment.app.FragmentActivity, androidx.activity.ComponentActivity, androidx.core.app.ComponentActivity, android.app.Activity
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        setContentView(q0().b());
        ActionBar supportActionBar = getSupportActionBar();
        if (supportActionBar != null) {
            supportActionBar.l();
        }
        getWindow().addFlags(Integer.MIN_VALUE);
        getWindow().setStatusBarColor(m70.d(this, 17170445));
        getWindow().setBackgroundDrawable(new ColorDrawable(getColor(R.color.akt_night_background)));
        r0().l();
        s0();
        m0();
    }

    @Override // net.safemoon.androidwallet.activity.common.BasicActivity, androidx.appcompat.app.AppCompatActivity, android.app.Activity
    public void onPostCreate(Bundle bundle) {
        super.onPostCreate(bundle);
        String i = bo3.i(this, "EMAILRAW");
        q7 q02 = q0();
        q02.o.setText(i);
        o0();
        EditText editText = q02.f.getEditText();
        if (editText != null) {
            editText.addTextChangedListener(new b(q02, this));
        }
        EditText editText2 = q02.e.getEditText();
        if (editText2 != null) {
            editText2.addTextChangedListener(new c(q02, this));
        }
        EditText editText3 = q02.d.getEditText();
        if (editText3 != null) {
            editText3.addTextChangedListener(new d(q02, this));
        }
        EditText editText4 = q02.e.getEditText();
        fs1.d(editText4);
        Editable text = editText4.getText();
        fs1.e(text, "etPassword.editText!!.text");
        p0(text);
        q02.k.e.setText(getString(R.string.akt_activity_register_txt));
        q02.k.b.setVisibility(0);
        q02.k.b.setOnClickListener(new View.OnClickListener() { // from class: z1
            @Override // android.view.View.OnClickListener
            public final void onClick(View view) {
                AKTRegisterActivity.u0(AKTRegisterActivity.this, view);
            }
        });
        q02.c.setOnClickListener(new View.OnClickListener() { // from class: y1
            @Override // android.view.View.OnClickListener
            public final void onClick(View view) {
                AKTRegisterActivity.v0(AKTRegisterActivity.this, view);
            }
        });
    }

    public final void p0(Editable editable) {
        int d2 = m70.d(this, R.color.akt_sign_in_wipe);
        ColorStateList e = m70.e(this, R.color.akt_sign_in_wipe);
        int d3 = m70.d(this, R.color.akt_button_inactive);
        ColorStateList e2 = m70.e(this, R.color.akt_button_inactive);
        if (editable.toString().length() > 7) {
            q0().j.setTextColor(d2);
            q0().g.setImageTintList(e);
            q0().j.setTag("complete");
        } else {
            q0().j.setTextColor(d3);
            q0().g.setImageTintList(e2);
            q0().j.setTag("incomplete");
        }
        if (s44.h(editable.toString())) {
            q0().n.setTextColor(d2);
            q0().h.setImageTintList(e);
            q0().n.setTag("complete");
        } else {
            q0().n.setTextColor(d3);
            q0().h.setImageTintList(e2);
            q0().n.setTag("incomplete");
        }
        if (s44.e(editable.toString())) {
            q0().m.setTextColor(d2);
            q0().i.setImageTintList(e);
            q0().m.setTag("complete");
            return;
        }
        q0().m.setTextColor(d3);
        q0().i.setImageTintList(e2);
        q0().m.setTag("incomplete");
    }

    public final q7 q0() {
        return (q7) this.m0.getValue();
    }

    public final AKTWebSocketHandlingViewModel r0() {
        return (AKTWebSocketHandlingViewModel) this.n0.getValue();
    }

    public void s0() {
        r0().h().observe(this, new tl2() { // from class: x1
            @Override // defpackage.tl2
            public final void onChanged(Object obj) {
                AKTRegisterActivity.t0(AKTRegisterActivity.this, (String) obj);
            }
        });
    }

    public void w0(String str) {
        if (str == null) {
            return;
        }
        r0().k(str);
    }
}
