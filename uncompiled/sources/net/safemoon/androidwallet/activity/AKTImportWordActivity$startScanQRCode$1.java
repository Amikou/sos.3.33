package net.safemoon.androidwallet.activity;

import android.content.Intent;
import android.widget.EditText;
import com.fasterxml.jackson.core.util.MinimalPrettyPrinter;
import java.util.List;
import java.util.Objects;
import kotlin.jvm.internal.Lambda;
import kotlin.text.StringsKt__StringsKt;

/* compiled from: AKTImportWordActivity.kt */
/* loaded from: classes2.dex */
public final class AKTImportWordActivity$startScanQRCode$1 extends Lambda implements tc1<Intent, te4> {
    public final /* synthetic */ EditText[] $editTextView;
    public final /* synthetic */ AKTImportWordActivity this$0;

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public AKTImportWordActivity$startScanQRCode$1(AKTImportWordActivity aKTImportWordActivity, EditText[] editTextArr) {
        super(1);
        this.this$0 = aKTImportWordActivity;
        this.$editTextView = editTextArr;
    }

    @Override // defpackage.tc1
    public /* bridge */ /* synthetic */ te4 invoke(Intent intent) {
        invoke2(intent);
        return te4.a;
    }

    /* renamed from: invoke  reason: avoid collision after fix types in other method */
    public final void invoke2(Intent intent) {
        int B0;
        if (intent != null) {
            String stringExtra = intent.getStringExtra("result");
            if (stringExtra == null) {
                this.this$0.H0();
                return;
            }
            String D = dv3.D(stringExtra, "|", MinimalPrettyPrinter.DEFAULT_ROOT_VALUE_SEPARATOR, false, 4, null);
            Objects.requireNonNull(D, "null cannot be cast to non-null type kotlin.CharSequence");
            List w0 = StringsKt__StringsKt.w0(StringsKt__StringsKt.K0(D).toString(), new String[]{MinimalPrettyPrinter.DEFAULT_ROOT_VALUE_SEPARATOR}, false, 0, 6, null);
            int size = w0.size();
            B0 = this.this$0.B0();
            if (size != B0) {
                this.this$0.H0();
                return;
            }
            EditText[] editTextArr = this.$editTextView;
            int length = editTextArr.length;
            int i = 0;
            int i2 = 0;
            while (i < length) {
                EditText editText = editTextArr[i];
                int i3 = i2 + 1;
                if (editText != null && i2 < w0.size()) {
                    editText.setText((CharSequence) w0.get(i2));
                }
                i++;
                i2 = i3;
            }
        }
    }
}
