package net.safemoon.androidwallet.activity;

import java.util.List;
import kotlin.jvm.internal.Lambda;
import net.safemoon.androidwallet.model.wallets.Wallet;
import net.safemoon.androidwallet.viewmodels.MultiWalletViewModel;

/* compiled from: WalletManageActivity.kt */
/* loaded from: classes2.dex */
public final class WalletManageActivity$updateAllWalletsLinkedStatus$1 extends Lambda implements tc1<List<? extends Wallet>, te4> {
    public final /* synthetic */ int $linkedState;
    public final /* synthetic */ WalletManageActivity this$0;

    /* compiled from: WalletManageActivity.kt */
    /* renamed from: net.safemoon.androidwallet.activity.WalletManageActivity$updateAllWalletsLinkedStatus$1$1  reason: invalid class name */
    /* loaded from: classes2.dex */
    public static final class AnonymousClass1 extends Lambda implements rc1<te4> {
        public static final AnonymousClass1 INSTANCE = new AnonymousClass1();

        public AnonymousClass1() {
            super(0);
        }

        @Override // defpackage.rc1
        public /* bridge */ /* synthetic */ te4 invoke() {
            invoke2();
            return te4.a;
        }

        @Override // defpackage.rc1
        /* renamed from: invoke  reason: avoid collision after fix types in other method */
        public final void invoke2() {
        }
    }

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public WalletManageActivity$updateAllWalletsLinkedStatus$1(WalletManageActivity walletManageActivity, int i) {
        super(1);
        this.this$0 = walletManageActivity;
        this.$linkedState = i;
    }

    @Override // defpackage.tc1
    public /* bridge */ /* synthetic */ te4 invoke(List<? extends Wallet> list) {
        invoke2((List<Wallet>) list);
        return te4.a;
    }

    /* renamed from: invoke  reason: avoid collision after fix types in other method */
    public final void invoke2(List<Wallet> list) {
        MultiWalletViewModel multiWalletViewModel;
        fs1.f(list, "list");
        for (Wallet wallet2 : list) {
            multiWalletViewModel = this.this$0.k0;
            multiWalletViewModel.C(wallet2, this.$linkedState, AnonymousClass1.INSTANCE);
        }
    }
}
