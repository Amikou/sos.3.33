package net.safemoon.androidwallet.activity;

import java.util.List;
import kotlin.jvm.internal.Lambda;
import kotlin.jvm.internal.Ref$IntRef;
import net.safemoon.androidwallet.model.wallets.Wallet;
import net.safemoon.androidwallet.viewmodels.MultiWalletViewModel;

/* compiled from: StartWalletActivity.kt */
/* loaded from: classes2.dex */
public final class StartWalletActivity$updateImportedWalletAfterLogin$1 extends Lambda implements tc1<List<? extends Wallet>, te4> {
    public final /* synthetic */ StartWalletActivity this$0;

    /* compiled from: StartWalletActivity.kt */
    /* renamed from: net.safemoon.androidwallet.activity.StartWalletActivity$updateImportedWalletAfterLogin$1$1  reason: invalid class name */
    /* loaded from: classes2.dex */
    public static final class AnonymousClass1 extends Lambda implements rc1<te4> {
        public final /* synthetic */ int $count;
        public final /* synthetic */ Ref$IntRef $updatedCount;
        public final /* synthetic */ StartWalletActivity this$0;

        /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
        public AnonymousClass1(Ref$IntRef ref$IntRef, int i, StartWalletActivity startWalletActivity) {
            super(0);
            this.$updatedCount = ref$IntRef;
            this.$count = i;
            this.this$0 = startWalletActivity;
        }

        @Override // defpackage.rc1
        public /* bridge */ /* synthetic */ te4 invoke() {
            invoke2();
            return te4.a;
        }

        @Override // defpackage.rc1
        /* renamed from: invoke  reason: avoid collision after fix types in other method */
        public final void invoke2() {
            Ref$IntRef ref$IntRef = this.$updatedCount;
            int i = ref$IntRef.element + 1;
            ref$IntRef.element = i;
            if (i == this.$count) {
                this.this$0.I0();
            }
        }
    }

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public StartWalletActivity$updateImportedWalletAfterLogin$1(StartWalletActivity startWalletActivity) {
        super(1);
        this.this$0 = startWalletActivity;
    }

    @Override // defpackage.tc1
    public /* bridge */ /* synthetic */ te4 invoke(List<? extends Wallet> list) {
        invoke2((List<Wallet>) list);
        return te4.a;
    }

    /* renamed from: invoke  reason: avoid collision after fix types in other method */
    public final void invoke2(List<Wallet> list) {
        MultiWalletViewModel multiWalletViewModel;
        fs1.f(list, "list");
        int size = list.size();
        Ref$IntRef ref$IntRef = new Ref$IntRef();
        for (Wallet wallet2 : list) {
            String u5b64 = wallet2.getU5B64();
            if (u5b64 == null || u5b64.length() == 0) {
                multiWalletViewModel = this.this$0.k0;
                multiWalletViewModel.G(wallet2, ref$IntRef.element + 1, new AnonymousClass1(ref$IntRef, size, this.this$0));
            } else {
                ref$IntRef.element++;
            }
        }
        if (size == ref$IntRef.element) {
            this.this$0.I0();
        }
    }
}
