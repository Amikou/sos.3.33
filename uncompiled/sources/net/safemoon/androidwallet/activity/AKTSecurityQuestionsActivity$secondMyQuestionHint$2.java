package net.safemoon.androidwallet.activity;

import kotlin.jvm.internal.Lambda;
import net.safemoon.androidwallet.R;

/* compiled from: AKTSecurityQuestionsActivity.kt */
/* loaded from: classes2.dex */
public final class AKTSecurityQuestionsActivity$secondMyQuestionHint$2 extends Lambda implements rc1<String> {
    public final /* synthetic */ AKTSecurityQuestionsActivity this$0;

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public AKTSecurityQuestionsActivity$secondMyQuestionHint$2(AKTSecurityQuestionsActivity aKTSecurityQuestionsActivity) {
        super(0);
        this.this$0 = aKTSecurityQuestionsActivity;
    }

    @Override // defpackage.rc1
    public final String invoke() {
        return this.this$0.getString(R.string.akt_register_second_question_text_hint);
    }
}
