package net.safemoon.androidwallet.activity;

import android.content.Context;
import android.content.Intent;
import android.graphics.drawable.ColorDrawable;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.os.VibrationEffect;
import android.os.Vibrator;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.view.animation.AnimationUtils;
import android.widget.CompoundButton;
import android.widget.LinearLayout;
import androidx.appcompat.app.ActionBar;
import androidx.fragment.app.j;
import com.AKT.anonymouskey.ui.login.AKTServerFunctions;
import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import kotlin.text.StringsKt__StringsKt;
import net.safemoon.androidwallet.R;
import net.safemoon.androidwallet.activity.AKTImportExistingWalletsActivity;
import net.safemoon.androidwallet.activity.AKTSecurityQuestionsActivity;
import net.safemoon.androidwallet.fragments.FullScreenWebViewFragment;
import net.safemoon.androidwallet.viewmodels.AKTWebSocketHandlingViewModel;

/* compiled from: AKTSecurityQuestionsActivity.kt */
/* loaded from: classes2.dex */
public final class AKTSecurityQuestionsActivity extends AKTServerFunctions {
    public static final a E0 = new a(null);
    public oc A0;
    public boolean D0;
    public boolean r0;
    public List<String> t0;
    public boolean w0;
    public boolean x0;
    public boolean y0;
    public oc z0;
    public final sy1 m0 = new fj4(d53.b(AKTWebSocketHandlingViewModel.class), new AKTSecurityQuestionsActivity$special$$inlined$viewModels$default$2(this), new AKTSecurityQuestionsActivity$special$$inlined$viewModels$default$1(this));
    public final sy1 n0 = zy1.a(new AKTSecurityQuestionsActivity$binding$2(this));
    public final sy1 o0 = zy1.a(new AKTSecurityQuestionsActivity$displayUsername$2(this));
    public final sy1 p0 = zy1.a(new AKTSecurityQuestionsActivity$username$2(this));
    public final sy1 q0 = zy1.a(new AKTSecurityQuestionsActivity$password$2(this));
    public final gb2<Boolean> s0 = new gb2<>(Boolean.FALSE);
    public gb2<String> u0 = new gb2<>("");
    public gb2<String> v0 = new gb2<>("");
    public final sy1 B0 = zy1.a(new AKTSecurityQuestionsActivity$firstMyQuestionHint$2(this));
    public final sy1 C0 = zy1.a(new AKTSecurityQuestionsActivity$secondMyQuestionHint$2(this));

    /* compiled from: AKTSecurityQuestionsActivity.kt */
    /* loaded from: classes2.dex */
    public static final class a {
        public a() {
        }

        public /* synthetic */ a(qi0 qi0Var) {
            this();
        }

        public final void a(Context context, String str, String str2, String str3) {
            fs1.f(context, "context");
            fs1.f(str, "username");
            fs1.f(str2, "password");
            fs1.f(str3, "displayUsername");
            Intent intent = new Intent(context, AKTSecurityQuestionsActivity.class);
            intent.putExtra("KEY_USERNAME", str);
            intent.putExtra("KEY_DISPLAY_USERNAME", str3);
            intent.putExtra("KEY_PASSWORD", str2);
            context.startActivity(intent);
        }
    }

    /* compiled from: AKTSecurityQuestionsActivity.kt */
    /* loaded from: classes2.dex */
    public static final class b implements TextWatcher {
        public b() {
        }

        @Override // android.text.TextWatcher
        public void afterTextChanged(Editable editable) {
            fs1.f(editable, "s");
            AKTSecurityQuestionsActivity.this.N0();
        }

        @Override // android.text.TextWatcher
        public void beforeTextChanged(CharSequence charSequence, int i, int i2, int i3) {
            fs1.f(charSequence, "s");
        }

        @Override // android.text.TextWatcher
        public void onTextChanged(CharSequence charSequence, int i, int i2, int i3) {
            fs1.f(charSequence, "s");
        }
    }

    /* compiled from: AKTSecurityQuestionsActivity.kt */
    /* loaded from: classes2.dex */
    public static final class c implements TextWatcher {
        public c() {
        }

        @Override // android.text.TextWatcher
        public void afterTextChanged(Editable editable) {
            fs1.f(editable, "s");
            AKTSecurityQuestionsActivity.this.N0();
        }

        @Override // android.text.TextWatcher
        public void beforeTextChanged(CharSequence charSequence, int i, int i2, int i3) {
            fs1.f(charSequence, "s");
        }

        @Override // android.text.TextWatcher
        public void onTextChanged(CharSequence charSequence, int i, int i2, int i3) {
            fs1.f(charSequence, "s");
        }
    }

    /* compiled from: AKTSecurityQuestionsActivity.kt */
    /* loaded from: classes2.dex */
    public static final class d implements TextWatcher {
        public d() {
        }

        @Override // android.text.TextWatcher
        public void afterTextChanged(Editable editable) {
            fs1.f(editable, "s");
            AKTSecurityQuestionsActivity.this.N0();
            if (editable.toString().length() > 0) {
                AKTSecurityQuestionsActivity.this.y0 = true;
                AKTSecurityQuestionsActivity.this.o1();
            }
        }

        @Override // android.text.TextWatcher
        public void beforeTextChanged(CharSequence charSequence, int i, int i2, int i3) {
            fs1.f(charSequence, "s");
        }

        @Override // android.text.TextWatcher
        public void onTextChanged(CharSequence charSequence, int i, int i2, int i3) {
            fs1.f(charSequence, "s");
        }
    }

    /* compiled from: AKTSecurityQuestionsActivity.kt */
    /* loaded from: classes2.dex */
    public static final class e implements TextWatcher {
        public e() {
        }

        @Override // android.text.TextWatcher
        public void afterTextChanged(Editable editable) {
            fs1.f(editable, "s");
            AKTSecurityQuestionsActivity.this.N0();
        }

        @Override // android.text.TextWatcher
        public void beforeTextChanged(CharSequence charSequence, int i, int i2, int i3) {
            fs1.f(charSequence, "s");
        }

        @Override // android.text.TextWatcher
        public void onTextChanged(CharSequence charSequence, int i, int i2, int i3) {
            fs1.f(charSequence, "s");
        }
    }

    public static final void L0(AKTSecurityQuestionsActivity aKTSecurityQuestionsActivity, View view) {
        fs1.f(aKTSecurityQuestionsActivity, "this$0");
        if (aKTSecurityQuestionsActivity.t1()) {
            aKTSecurityQuestionsActivity.n1();
        }
    }

    public static final void M0(AKTSecurityQuestionsActivity aKTSecurityQuestionsActivity, View view) {
        fs1.f(aKTSecurityQuestionsActivity, "this$0");
        pg4.e(aKTSecurityQuestionsActivity);
        j n = aKTSecurityQuestionsActivity.getSupportFragmentManager().n();
        fs1.e(n, "supportFragmentManager.beginTransaction()");
        n.s(R.id.content_main, FullScreenWebViewFragment.j0.a("https://safemoon.com/legal/wallet/eula", R.string.terms_of_service, false));
        n.h(FullScreenWebViewFragment.class.getSimpleName());
        n.j();
    }

    public static final void Z0(AKTSecurityQuestionsActivity aKTSecurityQuestionsActivity, String str) {
        fs1.f(aKTSecurityQuestionsActivity, "this$0");
        if (str == null) {
            return;
        }
        aKTSecurityQuestionsActivity.X(aKTSecurityQuestionsActivity.l0, str);
        aKTSecurityQuestionsActivity.Q();
    }

    public static final void a1(w6 w6Var, AKTSecurityQuestionsActivity aKTSecurityQuestionsActivity, String str) {
        fs1.f(w6Var, "$this_apply");
        fs1.f(aKTSecurityQuestionsActivity, "this$0");
        fs1.e(str, "question");
        Object[] array = StringsKt__StringsKt.w0(str, new String[]{"="}, false, 0, 6, null).toArray(new String[0]);
        Objects.requireNonNull(array, "null cannot be cast to non-null type kotlin.Array<T>");
        String[] strArr = (String[]) array;
        if (strArr.length > 1) {
            w6Var.r.setText(strArr[1]);
        }
        aKTSecurityQuestionsActivity.s1();
    }

    public static final void b1(final AKTSecurityQuestionsActivity aKTSecurityQuestionsActivity, final w6 w6Var, View view) {
        fs1.f(aKTSecurityQuestionsActivity, "this$0");
        fs1.f(w6Var, "$this_apply");
        pg4.e(aKTSecurityQuestionsActivity);
        List l = b20.l(aKTSecurityQuestionsActivity.R0());
        List<String> list = aKTSecurityQuestionsActivity.t0;
        if (list == null) {
            fs1.r("questionList");
            list = null;
        }
        l.addAll(list);
        aKTSecurityQuestionsActivity.z0 = new oc(l);
        new Handler(Looper.getMainLooper()).postDelayed(new Runnable() { // from class: k2
            @Override // java.lang.Runnable
            public final void run() {
                AKTSecurityQuestionsActivity.c1(AKTSecurityQuestionsActivity.this, w6Var);
            }
        }, 200L);
    }

    public static final void c1(AKTSecurityQuestionsActivity aKTSecurityQuestionsActivity, w6 w6Var) {
        fs1.f(aKTSecurityQuestionsActivity, "this$0");
        fs1.f(w6Var, "$this_apply");
        oc ocVar = aKTSecurityQuestionsActivity.z0;
        if (ocVar == null) {
            fs1.r("firstQuestionsDialog");
            ocVar = null;
        }
        LinearLayout linearLayout = w6Var.l;
        fs1.e(linearLayout, "firstQuestionLayout");
        ocVar.g(aKTSecurityQuestionsActivity, linearLayout, w6Var.b(), new AKTSecurityQuestionsActivity$onPostCreate$1$2$1$1(aKTSecurityQuestionsActivity, w6Var));
    }

    public static final void d1(w6 w6Var, String str) {
        fs1.f(w6Var, "$this_apply");
        fs1.e(str, "question");
        Object[] array = StringsKt__StringsKt.w0(str, new String[]{"="}, false, 0, 6, null).toArray(new String[0]);
        Objects.requireNonNull(array, "null cannot be cast to non-null type kotlin.Array<T>");
        String[] strArr = (String[]) array;
        if (strArr.length > 1) {
            w6Var.s.setText(strArr[1]);
        }
    }

    public static final void e1(final AKTSecurityQuestionsActivity aKTSecurityQuestionsActivity, final w6 w6Var, View view) {
        fs1.f(aKTSecurityQuestionsActivity, "this$0");
        fs1.f(w6Var, "$this_apply");
        pg4.e(aKTSecurityQuestionsActivity);
        List<String> list = aKTSecurityQuestionsActivity.t0;
        if (list == null) {
            fs1.r("questionList");
            list = null;
        }
        ArrayList arrayList = new ArrayList();
        for (Object obj : list) {
            if (true ^ fs1.b((String) obj, aKTSecurityQuestionsActivity.u0.getValue())) {
                arrayList.add(obj);
            }
        }
        List l = b20.l(aKTSecurityQuestionsActivity.U0());
        l.addAll(arrayList);
        aKTSecurityQuestionsActivity.A0 = new oc(l);
        new Handler(Looper.getMainLooper()).postDelayed(new Runnable() { // from class: l2
            @Override // java.lang.Runnable
            public final void run() {
                AKTSecurityQuestionsActivity.f1(AKTSecurityQuestionsActivity.this, w6Var);
            }
        }, 200L);
    }

    public static final void f1(AKTSecurityQuestionsActivity aKTSecurityQuestionsActivity, w6 w6Var) {
        fs1.f(aKTSecurityQuestionsActivity, "this$0");
        fs1.f(w6Var, "$this_apply");
        oc ocVar = aKTSecurityQuestionsActivity.A0;
        if (ocVar == null) {
            fs1.r("secondQuestionsDialog");
            ocVar = null;
        }
        LinearLayout linearLayout = w6Var.m;
        fs1.e(linearLayout, "secondQuestionLayout");
        ocVar.g(aKTSecurityQuestionsActivity, linearLayout, w6Var.b(), new AKTSecurityQuestionsActivity$onPostCreate$1$4$1$1(aKTSecurityQuestionsActivity, w6Var));
    }

    public static final void g1(AKTSecurityQuestionsActivity aKTSecurityQuestionsActivity, CompoundButton compoundButton, boolean z) {
        fs1.f(aKTSecurityQuestionsActivity, "this$0");
        aKTSecurityQuestionsActivity.s0.setValue(Boolean.valueOf(z));
        aKTSecurityQuestionsActivity.N0();
    }

    public static final void h1(AKTSecurityQuestionsActivity aKTSecurityQuestionsActivity, View view) {
        fs1.f(aKTSecurityQuestionsActivity, "this$0");
        aKTSecurityQuestionsActivity.onBackPressed();
    }

    public static final void i1(AKTSecurityQuestionsActivity aKTSecurityQuestionsActivity, View view) {
        fs1.f(aKTSecurityQuestionsActivity, "this$0");
        pg4.e(aKTSecurityQuestionsActivity);
    }

    public final void K0() {
        P0().b.setOnClickListener(new View.OnClickListener() { // from class: o2
            @Override // android.view.View.OnClickListener
            public final void onClick(View view) {
                AKTSecurityQuestionsActivity.L0(AKTSecurityQuestionsActivity.this, view);
            }
        });
        P0().d.setOnClickListener(new View.OnClickListener() { // from class: q2
            @Override // android.view.View.OnClickListener
            public final void onClick(View view) {
                AKTSecurityQuestionsActivity.M0(AKTSecurityQuestionsActivity.this, view);
            }
        });
    }

    /* JADX WARN: Removed duplicated region for block: B:16:0x0025  */
    /* JADX WARN: Removed duplicated region for block: B:33:0x004e  */
    /* JADX WARN: Removed duplicated region for block: B:36:0x005d  */
    /* JADX WARN: Removed duplicated region for block: B:37:0x0064  */
    /* JADX WARN: Removed duplicated region for block: B:46:0x007d  */
    /* JADX WARN: Removed duplicated region for block: B:49:0x008a  */
    /* JADX WARN: Removed duplicated region for block: B:50:0x0091  */
    /* JADX WARN: Removed duplicated region for block: B:53:0x00a4  */
    /* JADX WARN: Removed duplicated region for block: B:55:0x00a7  */
    /* JADX WARN: Removed duplicated region for block: B:56:0x00c5  */
    /*
        Code decompiled incorrectly, please refer to instructions dump.
        To view partially-correct code enable 'Show inconsistent code' option in preferences
    */
    public final void N0() {
        /*
            Method dump skipped, instructions count: 227
            To view this dump change 'Code comments level' option to 'DEBUG'
        */
        throw new UnsupportedOperationException("Method not decompiled: net.safemoon.androidwallet.activity.AKTSecurityQuestionsActivity.N0():void");
    }

    public final String O0(String str) {
        String y;
        String[] v = i2.v(str, "|");
        fs1.e(v, "mySplit(registerStr, \"|\")");
        String u = i2.u(v[1], v[2]);
        if (s.b) {
            y = b0(v[0], v[1], v[2], v[3], v[4], v[5], v[6], v[7], v[8], u);
        } else {
            y = i2.y(this.l0, v[0], v[1], v[2], v[3], v[4], v[5], v[6], u);
        }
        fs1.e(y, "toSend");
        return y;
    }

    public final w6 P0() {
        return (w6) this.n0.getValue();
    }

    @Override // com.AKT.anonymouskey.ui.login.AKTServerFunctions
    public void Q() {
        W0().f();
    }

    public final String Q0() {
        return (String) this.o0.getValue();
    }

    public final String R0() {
        return (String) this.B0.getValue();
    }

    public final String S0() {
        return (String) this.q0.getValue();
    }

    public final String T0() {
        String str;
        String obj;
        String str2;
        String str3;
        String str4;
        String str5;
        String i = bo3.i(this, "EMAILRAW");
        boolean z = this.w0;
        if (this.x0) {
            z = !z ? true : true;
        }
        String str6 = "36";
        String str7 = "";
        if (!z) {
            String value = this.u0.getValue();
            fs1.d(value);
            fs1.e(value, "firstQuestion.value!!");
            Object[] array = StringsKt__StringsKt.w0(value, new String[]{"="}, false, 0, 6, null).toArray(new String[0]);
            Objects.requireNonNull(array, "null cannot be cast to non-null type kotlin.Array<T>");
            String[] strArr = (String[]) array;
            str6 = strArr[0];
            String str8 = strArr[1];
            String obj2 = P0().h.getText().toString();
            String value2 = this.v0.getValue();
            fs1.d(value2);
            fs1.e(value2, "secondQuestion.value!!");
            Object[] array2 = StringsKt__StringsKt.w0(value2, new String[]{"="}, false, 0, 6, null).toArray(new String[0]);
            Objects.requireNonNull(array2, "null cannot be cast to non-null type kotlin.Array<T>");
            String[] strArr2 = (String[]) array2;
            str = strArr2[0];
            String str9 = strArr2[1];
            obj = P0().j.getText().toString();
            str2 = str8;
            str7 = obj2;
            str3 = str9;
        } else {
            if (z) {
                String obj3 = P0().i.getText().toString();
                String obj4 = P0().h.getText().toString();
                String value3 = this.v0.getValue();
                fs1.d(value3);
                fs1.e(value3, "secondQuestion.value!!");
                Object[] array3 = StringsKt__StringsKt.w0(value3, new String[]{"="}, false, 0, 6, null).toArray(new String[0]);
                Objects.requireNonNull(array3, "null cannot be cast to non-null type kotlin.Array<T>");
                String[] strArr3 = (String[]) array3;
                String str10 = strArr3[0];
                str4 = strArr3[1];
                obj = obj4;
                str3 = obj3;
                str7 = P0().j.getText().toString();
                str5 = "36";
                str6 = str10;
            } else if (z) {
                String value4 = this.u0.getValue();
                fs1.d(value4);
                fs1.e(value4, "firstQuestion.value!!");
                Object[] array4 = StringsKt__StringsKt.w0(value4, new String[]{"="}, false, 0, 6, null).toArray(new String[0]);
                Objects.requireNonNull(array4, "null cannot be cast to non-null type kotlin.Array<T>");
                String[] strArr4 = (String[]) array4;
                String str11 = strArr4[0];
                String str12 = strArr4[1];
                String obj5 = P0().h.getText().toString();
                String obj6 = P0().k.getText().toString();
                obj = P0().j.getText().toString();
                str2 = str12;
                str7 = obj5;
                str3 = obj6;
                str = "36";
                str6 = str11;
            } else if (z) {
                String obj7 = P0().i.getText().toString();
                str7 = P0().h.getText().toString();
                str3 = P0().k.getText().toString();
                str4 = obj7;
                str5 = "37";
                obj = P0().j.getText().toString();
            } else {
                str3 = "";
                str4 = str3;
                str = str4;
                obj = str;
                str6 = obj;
                b30 b30Var = b30.a;
                String n = b30Var.n(str7);
                String n2 = b30Var.n(obj);
                return ((Object) i) + '|' + V0() + '|' + S0() + '|' + str6 + '|' + n + '|' + str + '|' + n2 + '|' + str4 + '|' + str3 + '|';
            }
            str = str5;
            b30 b30Var2 = b30.a;
            String n3 = b30Var2.n(str7);
            String n22 = b30Var2.n(obj);
            return ((Object) i) + '|' + V0() + '|' + S0() + '|' + str6 + '|' + n3 + '|' + str + '|' + n22 + '|' + str4 + '|' + str3 + '|';
        }
        str4 = str2;
        b30 b30Var22 = b30.a;
        String n32 = b30Var22.n(str7);
        String n222 = b30Var22.n(obj);
        return ((Object) i) + '|' + V0() + '|' + S0() + '|' + str6 + '|' + n32 + '|' + str + '|' + n222 + '|' + str4 + '|' + str3 + '|';
    }

    public final String U0() {
        return (String) this.C0.getValue();
    }

    public final String V0() {
        return (String) this.p0.getValue();
    }

    public final AKTWebSocketHandlingViewModel W0() {
        return (AKTWebSocketHandlingViewModel) this.m0.getValue();
    }

    /* JADX WARN: Code restructure failed: missing block: B:42:0x00f0, code lost:
        if (r4.equals("GR8REG02") == false) goto L52;
     */
    /* JADX WARN: Code restructure failed: missing block: B:53:0x010e, code lost:
        if (r4.equals("AKTSFMREG02") == false) goto L52;
     */
    /* JADX WARN: Code restructure failed: missing block: B:56:0x0117, code lost:
        if (r4.equals("SFMREG02") == false) goto L52;
     */
    /* JADX WARN: Code restructure failed: missing block: B:58:0x011a, code lost:
        if (r21 != null) goto L38;
     */
    /* JADX WARN: Code restructure failed: missing block: B:60:0x011d, code lost:
        r21.c("KA", defpackage.bo3.i(r20, "KA"));
     */
    /* JADX WARN: Code restructure failed: missing block: B:61:0x0124, code lost:
        defpackage.bo3.m(r20, "TIMENOW", defpackage.i2.A());
        p1();
     */
    /* JADX WARN: Code restructure failed: missing block: B:72:?, code lost:
        return;
     */
    /* JADX WARN: Removed duplicated region for block: B:65:0x0136  */
    @Override // com.AKT.anonymouskey.ui.login.AKTServerFunctions
    /*
        Code decompiled incorrectly, please refer to instructions dump.
        To view partially-correct code enable 'Show inconsistent code' option in preferences
    */
    public void X(defpackage.qy4 r21, java.lang.String r22) {
        /*
            Method dump skipped, instructions count: 340
            To view this dump change 'Code comments level' option to 'DEBUG'
        */
        throw new UnsupportedOperationException("Method not decompiled: net.safemoon.androidwallet.activity.AKTSecurityQuestionsActivity.X(qy4, java.lang.String):void");
    }

    public final void X0() {
        Object systemService = getSystemService("vibrator");
        Objects.requireNonNull(systemService, "null cannot be cast to non-null type android.os.Vibrator");
        Vibrator vibrator = (Vibrator) systemService;
        if (Build.VERSION.SDK_INT >= 26) {
            vibrator.vibrate(VibrationEffect.createOneShot(50L, 10));
        } else {
            vibrator.vibrate(50L);
        }
    }

    @Override // com.AKT.anonymouskey.ui.login.AKTServerFunctions
    public void Y(String[] strArr, String str) {
        this.j0.h();
        bh.V(new WeakReference(this), Integer.valueOf((int) R.string.akt_error_cannot_register_new_key_message), null, 0, null, 28, null);
    }

    public void Y0() {
        W0().h().observe(this, new tl2() { // from class: n2
            @Override // defpackage.tl2
            public final void onChanged(Object obj) {
                AKTSecurityQuestionsActivity.Z0(AKTSecurityQuestionsActivity.this, (String) obj);
            }
        });
    }

    public final void j1(String[] strArr) {
        int length = strArr.length;
        boolean z = false;
        String str = null;
        if (1 < length) {
            int i = 1;
            while (true) {
                int i2 = i + 1;
                Object[] array = StringsKt__StringsKt.w0(strArr[i], new String[]{"="}, false, 0, 6, null).toArray(new String[0]);
                Objects.requireNonNull(array, "null cannot be cast to non-null type kotlin.Array<T>");
                String[] strArr2 = (String[]) array;
                if (strArr2.length >= 2 && fs1.b(strArr2[0], "APIKEY")) {
                    str = strArr2[1];
                }
                if (i2 >= length) {
                    break;
                }
                i = i2;
            }
        }
        String j = bo3.j(this, "PB5K", "");
        dz4 dz4Var = new dz4();
        dz4Var.t(lm.a(j));
        String b2 = dz4Var.b(str);
        if (b2 != null) {
            if (b2.length() > 0) {
                z = true;
            }
        }
        if (z) {
            jv0.e(this, "API_KEY", b2);
        }
        b30.a.c(this);
    }

    public final void k1(String str) {
        l1(O0(str));
    }

    public void l1(String str) {
        if (str == null) {
            return;
        }
        W0().k(str);
    }

    public final void m1() {
        X0();
        P0().c.startAnimation(AnimationUtils.loadAnimation(getApplicationContext(), R.anim.shake));
    }

    public final void n1() {
        bh.e0(new WeakReference(this), Integer.valueOf((int) R.string.akt_reset_password_confirm_dialog_header), P0().h.getText().toString(), P0().j.getText().toString(), R.string.confirm, R.string.ca_alert_save_continue_editing, new AKTSecurityQuestionsActivity$showConfirmSecurityQuestionsDialog$1(this), AKTSecurityQuestionsActivity$showConfirmSecurityQuestionsDialog$2.INSTANCE);
    }

    public final void o1() {
        if (this.y0) {
            w6 P0 = P0();
            P0.t.setVisibility(0);
            P0.g.setVisibility(0);
            P0.j.setVisibility(0);
            P0.f.setVisibility(0);
            P0.m.setVisibility(0);
            if (this.x0) {
                P0.s.setVisibility(8);
                P0.k.setVisibility(0);
                return;
            }
            P0.s.setVisibility(0);
            P0.k.setVisibility(8);
        }
    }

    @Override // com.AKT.anonymouskey.ui.login.AKTServerFunctions, net.safemoon.androidwallet.activity.common.BasicActivity, androidx.fragment.app.FragmentActivity, androidx.activity.ComponentActivity, androidx.core.app.ComponentActivity, android.app.Activity
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        setContentView(P0().b());
        W0().l();
        Y0();
        ActionBar supportActionBar = getSupportActionBar();
        if (supportActionBar != null) {
            supportActionBar.l();
        }
        getWindow().addFlags(Integer.MIN_VALUE);
        getWindow().setStatusBarColor(m70.d(this, 17170445));
        getWindow().setBackgroundDrawable(new ColorDrawable(getColor(R.color.akt_night_background)));
        String[] v = i2.v(i2.o(this.l0), "|");
        fs1.e(v, "mySplit(questions, \"|\")");
        this.t0 = zh.c(v);
        K0();
        this.k0.p(new AKTSecurityQuestionsActivity$onCreate$1(this));
    }

    @Override // net.safemoon.androidwallet.activity.common.BasicActivity, androidx.appcompat.app.AppCompatActivity, android.app.Activity
    public void onPostCreate(Bundle bundle) {
        super.onPostCreate(bundle);
        String i = bo3.i(this, "EMAILRAW");
        final w6 P0 = P0();
        P0.q.setText(i);
        P0.u.setText(Q0());
        N0();
        this.u0.observe(this, new tl2() { // from class: m2
            @Override // defpackage.tl2
            public final void onChanged(Object obj) {
                AKTSecurityQuestionsActivity.a1(w6.this, this, (String) obj);
            }
        });
        List<String> list = this.t0;
        List<String> list2 = null;
        if (list == null) {
            fs1.r("questionList");
            list = null;
        }
        if (!list.isEmpty()) {
            gb2<String> gb2Var = this.u0;
            List<String> list3 = this.t0;
            if (list3 == null) {
                fs1.r("questionList");
            } else {
                list2 = list3;
            }
            gb2Var.postValue(list2.get(0));
        }
        P0.l.setOnClickListener(new View.OnClickListener() { // from class: s2
            @Override // android.view.View.OnClickListener
            public final void onClick(View view) {
                AKTSecurityQuestionsActivity.b1(AKTSecurityQuestionsActivity.this, P0, view);
            }
        });
        this.v0.observe(this, new tl2() { // from class: j2
            @Override // defpackage.tl2
            public final void onChanged(Object obj) {
                AKTSecurityQuestionsActivity.d1(w6.this, (String) obj);
            }
        });
        P0.m.setOnClickListener(new View.OnClickListener() { // from class: t2
            @Override // android.view.View.OnClickListener
            public final void onClick(View view) {
                AKTSecurityQuestionsActivity.e1(AKTSecurityQuestionsActivity.this, P0, view);
            }
        });
        P0.i.addTextChangedListener(new b());
        P0.k.addTextChangedListener(new c());
        P0.h.addTextChangedListener(new d());
        P0.j.addTextChangedListener(new e());
        P0.c.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() { // from class: u2
            @Override // android.widget.CompoundButton.OnCheckedChangeListener
            public final void onCheckedChanged(CompoundButton compoundButton, boolean z) {
                AKTSecurityQuestionsActivity.g1(AKTSecurityQuestionsActivity.this, compoundButton, z);
            }
        });
        P0.n.e.setText(getString(R.string.akt_activity_register_txt));
        P0.n.b.setVisibility(0);
        P0.n.b.setOnClickListener(new View.OnClickListener() { // from class: r2
            @Override // android.view.View.OnClickListener
            public final void onClick(View view) {
                AKTSecurityQuestionsActivity.h1(AKTSecurityQuestionsActivity.this, view);
            }
        });
        P0.e.setOnClickListener(new View.OnClickListener() { // from class: p2
            @Override // android.view.View.OnClickListener
            public final void onClick(View view) {
                AKTSecurityQuestionsActivity.i1(AKTSecurityQuestionsActivity.this, view);
            }
        });
    }

    public final void p1() {
        String M;
        if (s.b) {
            M = i2.e(this.l0, V0(), S0());
            fs1.e(M, "{\n            AKTSecpUti…d\n            )\n        }");
        } else {
            M = M(this.l0, V0(), S0());
            fs1.e(M, "{\n            anonymizeA…name, password)\n        }");
        }
        W0().k(M);
    }

    public final void q1(String str) {
        AKTImportExistingWalletsActivity.a aVar = AKTImportExistingWalletsActivity.v0;
        String V0 = V0();
        fs1.e(V0, "username");
        String S0 = S0();
        fs1.e(S0, "password");
        AKTImportExistingWalletsActivity.a.b(aVar, this, str, false, null, V0, S0, 12, null);
    }

    public final void r1(String str) {
        StartWalletActivity.y0.a(this, true, str, this.r0);
        finish();
    }

    public final void s1() {
        if (fs1.b(this.u0.getValue(), this.v0.getValue())) {
            List<String> list = this.t0;
            if (list == null) {
                fs1.r("questionList");
                list = null;
            }
            ArrayList arrayList = new ArrayList();
            for (Object obj : list) {
                if (!fs1.b((String) obj, this.u0.getValue())) {
                    arrayList.add(obj);
                }
            }
            if (!arrayList.isEmpty()) {
                this.v0.postValue(arrayList.get(0));
            }
        }
    }

    public final boolean t1() {
        if (fs1.b(this.s0.getValue(), Boolean.FALSE)) {
            m1();
            return false;
        }
        return true;
    }
}
