package net.safemoon.androidwallet.activity;

import android.os.Handler;
import android.os.Looper;
import java.util.List;
import kotlin.jvm.internal.Lambda;
import net.safemoon.androidwallet.activity.AKTImportExistingWalletsActivity;
import net.safemoon.androidwallet.activity.AKTImportExistingWalletsActivity$onCreate$1;
import net.safemoon.androidwallet.model.wallets.Wallet;

/* compiled from: AKTImportExistingWalletsActivity.kt */
/* loaded from: classes2.dex */
public final class AKTImportExistingWalletsActivity$onCreate$1 extends Lambda implements tc1<List<? extends Wallet>, te4> {
    public final /* synthetic */ AKTImportExistingWalletsActivity this$0;

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public AKTImportExistingWalletsActivity$onCreate$1(AKTImportExistingWalletsActivity aKTImportExistingWalletsActivity) {
        super(1);
        this.this$0 = aKTImportExistingWalletsActivity;
    }

    public static final void b(AKTImportExistingWalletsActivity aKTImportExistingWalletsActivity, List list) {
        fs1.f(aKTImportExistingWalletsActivity, "this$0");
        fs1.f(list, "$it");
        aKTImportExistingWalletsActivity.M0(list);
    }

    @Override // defpackage.tc1
    public /* bridge */ /* synthetic */ te4 invoke(List<? extends Wallet> list) {
        invoke2((List<Wallet>) list);
        return te4.a;
    }

    /* renamed from: invoke  reason: avoid collision after fix types in other method */
    public final void invoke2(final List<Wallet> list) {
        fs1.f(list, "it");
        Handler handler = new Handler(Looper.getMainLooper());
        final AKTImportExistingWalletsActivity aKTImportExistingWalletsActivity = this.this$0;
        handler.post(new Runnable() { // from class: w0
            @Override // java.lang.Runnable
            public final void run() {
                AKTImportExistingWalletsActivity$onCreate$1.b(AKTImportExistingWalletsActivity.this, list);
            }
        });
    }
}
