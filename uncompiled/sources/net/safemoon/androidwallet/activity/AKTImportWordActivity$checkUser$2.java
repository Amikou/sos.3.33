package net.safemoon.androidwallet.activity;

import android.content.DialogInterface;
import kotlin.jvm.internal.Lambda;
import net.safemoon.androidwallet.R;
import net.safemoon.androidwallet.dialogs.ProgressLoading;

/* compiled from: AKTImportWordActivity.kt */
/* loaded from: classes2.dex */
public final class AKTImportWordActivity$checkUser$2 extends Lambda implements md1<String, String, String, Boolean, te4> {
    public final /* synthetic */ String $aktMnemonic;
    public final /* synthetic */ AKTImportWordActivity this$0;

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public AKTImportWordActivity$checkUser$2(AKTImportWordActivity aKTImportWordActivity, String str) {
        super(4);
        this.this$0 = aKTImportWordActivity;
        this.$aktMnemonic = str;
    }

    public static final void b(DialogInterface dialogInterface) {
    }

    @Override // defpackage.md1
    public /* bridge */ /* synthetic */ te4 invoke(String str, String str2, String str3, Boolean bool) {
        invoke(str, str2, str3, bool.booleanValue());
        return te4.a;
    }

    public final void invoke(String str, String str2, String str3, boolean z) {
        ProgressLoading progressLoading;
        ProgressLoading progressLoading2;
        if (z) {
            progressLoading = this.this$0.j0;
            if (progressLoading != null) {
                progressLoading2 = this.this$0.j0;
                progressLoading2.h();
            }
            jc0.c(this.this$0, Integer.valueOf((int) R.string.warning_title), R.string.warning_black_list_address, true, n1.a);
            return;
        }
        this.this$0.m0(str, str2, this.$aktMnemonic);
    }
}
