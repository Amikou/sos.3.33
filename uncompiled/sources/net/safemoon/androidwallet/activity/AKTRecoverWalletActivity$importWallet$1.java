package net.safemoon.androidwallet.activity;

import java.lang.ref.WeakReference;
import kotlin.jvm.internal.Lambda;
import net.safemoon.androidwallet.R;
import net.safemoon.androidwallet.dialogs.ProgressLoading;

/* compiled from: AKTRecoverWalletActivity.kt */
/* loaded from: classes2.dex */
public final class AKTRecoverWalletActivity$importWallet$1 extends Lambda implements tc1<Long, te4> {
    public final /* synthetic */ AKTRecoverWalletActivity this$0;

    /* compiled from: AKTRecoverWalletActivity.kt */
    /* renamed from: net.safemoon.androidwallet.activity.AKTRecoverWalletActivity$importWallet$1$1  reason: invalid class name */
    /* loaded from: classes2.dex */
    public static final class AnonymousClass1 extends Lambda implements rc1<te4> {
        public static final AnonymousClass1 INSTANCE = new AnonymousClass1();

        public AnonymousClass1() {
            super(0);
        }

        @Override // defpackage.rc1
        public /* bridge */ /* synthetic */ te4 invoke() {
            invoke2();
            return te4.a;
        }

        @Override // defpackage.rc1
        /* renamed from: invoke  reason: avoid collision after fix types in other method */
        public final void invoke2() {
        }
    }

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public AKTRecoverWalletActivity$importWallet$1(AKTRecoverWalletActivity aKTRecoverWalletActivity) {
        super(1);
        this.this$0 = aKTRecoverWalletActivity;
    }

    @Override // defpackage.tc1
    public /* bridge */ /* synthetic */ te4 invoke(Long l) {
        invoke2(l);
        return te4.a;
    }

    /* renamed from: invoke  reason: avoid collision after fix types in other method */
    public final void invoke2(Long l) {
        ProgressLoading progressLoading;
        ProgressLoading progressLoading2;
        progressLoading = this.this$0.j0;
        if (progressLoading != null) {
            progressLoading2 = this.this$0.j0;
            progressLoading2.h();
        }
        if (l != null && l.longValue() > 0) {
            this.this$0.l0();
        } else {
            bh.U(new WeakReference(this.this$0), Integer.valueOf((int) R.string.warning_iw_failed_title), Integer.valueOf((int) R.string.warning_iw_failed_msg), R.string.action_ok, AnonymousClass1.INSTANCE);
        }
    }
}
