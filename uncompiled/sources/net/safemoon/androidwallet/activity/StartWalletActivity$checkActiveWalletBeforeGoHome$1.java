package net.safemoon.androidwallet.activity;

import java.util.List;
import kotlin.jvm.internal.Lambda;
import net.safemoon.androidwallet.model.wallets.Wallet;

/* compiled from: StartWalletActivity.kt */
/* loaded from: classes2.dex */
public final class StartWalletActivity$checkActiveWalletBeforeGoHome$1 extends Lambda implements tc1<List<? extends Wallet>, te4> {
    public final /* synthetic */ StartWalletActivity this$0;

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public StartWalletActivity$checkActiveWalletBeforeGoHome$1(StartWalletActivity startWalletActivity) {
        super(1);
        this.this$0 = startWalletActivity;
    }

    @Override // defpackage.tc1
    public /* bridge */ /* synthetic */ te4 invoke(List<? extends Wallet> list) {
        invoke2((List<Wallet>) list);
        return te4.a;
    }

    /* JADX WARN: Code restructure failed: missing block: B:33:0x007b, code lost:
        if (defpackage.fs1.b(r2 != null ? r2.getName() : null, r0.getName()) == false) goto L35;
     */
    /* renamed from: invoke  reason: avoid collision after fix types in other method */
    /*
        Code decompiled incorrectly, please refer to instructions dump.
        To view partially-correct code enable 'Show inconsistent code' option in preferences
    */
    public final void invoke2(java.util.List<net.safemoon.androidwallet.model.wallets.Wallet> r6) {
        /*
            r5 = this;
            java.lang.String r0 = "list"
            defpackage.fs1.f(r6, r0)
            net.safemoon.androidwallet.activity.StartWalletActivity r0 = r5.this$0
            net.safemoon.androidwallet.model.wallets.Wallet r0 = defpackage.e30.c(r0)
            r1 = 0
            if (r0 != 0) goto L36
            java.util.Iterator r6 = r6.iterator()
        L12:
            boolean r0 = r6.hasNext()
            if (r0 == 0) goto L26
            java.lang.Object r0 = r6.next()
            r2 = r0
            net.safemoon.androidwallet.model.wallets.Wallet r2 = (net.safemoon.androidwallet.model.wallets.Wallet) r2
            boolean r2 = r2.isPrimaryWallet()
            if (r2 == 0) goto L12
            r1 = r0
        L26:
            net.safemoon.androidwallet.model.wallets.Wallet r1 = (net.safemoon.androidwallet.model.wallets.Wallet) r1
            if (r1 != 0) goto L2c
            goto L8f
        L2c:
            net.safemoon.androidwallet.activity.StartWalletActivity r6 = r5.this$0
            net.safemoon.androidwallet.viewmodels.MultiWalletViewModel r6 = net.safemoon.androidwallet.activity.StartWalletActivity.t0(r6)
            r6.A(r1)
            goto L8f
        L36:
            java.util.Iterator r6 = r6.iterator()
        L3a:
            boolean r2 = r6.hasNext()
            if (r2 == 0) goto L56
            java.lang.Object r2 = r6.next()
            r3 = r2
            net.safemoon.androidwallet.model.wallets.Wallet r3 = (net.safemoon.androidwallet.model.wallets.Wallet) r3
            java.lang.String r3 = r3.getAddress()
            java.lang.String r4 = r0.getAddress()
            boolean r3 = defpackage.fs1.b(r3, r4)
            if (r3 == 0) goto L3a
            goto L57
        L56:
            r2 = r1
        L57:
            net.safemoon.androidwallet.model.wallets.Wallet r2 = (net.safemoon.androidwallet.model.wallets.Wallet) r2
            java.lang.String r6 = r0.getU5B64()
            r3 = 1
            if (r6 == 0) goto L69
            int r6 = r6.length()
            if (r6 != 0) goto L67
            goto L69
        L67:
            r6 = 0
            goto L6a
        L69:
            r6 = r3
        L6a:
            if (r6 != 0) goto L7d
            if (r2 != 0) goto L6f
            goto L73
        L6f:
            java.lang.String r1 = r2.getName()
        L73:
            java.lang.String r6 = r0.getName()
            boolean r6 = defpackage.fs1.b(r1, r6)
            if (r6 != 0) goto L83
        L7d:
            if (r2 != 0) goto L80
            goto L83
        L80:
            r2.setLinkedState(r3)
        L83:
            if (r2 != 0) goto L86
            goto L8f
        L86:
            net.safemoon.androidwallet.activity.StartWalletActivity r6 = r5.this$0
            net.safemoon.androidwallet.viewmodels.MultiWalletViewModel r6 = net.safemoon.androidwallet.activity.StartWalletActivity.t0(r6)
            r6.A(r2)
        L8f:
            net.safemoon.androidwallet.activity.StartWalletActivity r6 = r5.this$0
            net.safemoon.androidwallet.activity.StartWalletActivity.u0(r6)
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: net.safemoon.androidwallet.activity.StartWalletActivity$checkActiveWalletBeforeGoHome$1.invoke2(java.util.List):void");
    }
}
