package net.safemoon.androidwallet.activity;

import android.content.Context;
import android.content.Intent;
import android.content.res.ColorStateList;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.EditText;
import androidx.appcompat.app.ActionBar;
import androidx.fragment.app.FragmentManager;
import com.AKT.anonymouskey.ui.login.AKTServerFunctions;
import java.lang.ref.WeakReference;
import java.util.List;
import java.util.Locale;
import java.util.Objects;
import kotlin.text.StringsKt__StringsKt;
import net.safemoon.androidwallet.R;
import net.safemoon.androidwallet.activity.AKTAnswerQuestionsActivity;
import net.safemoon.androidwallet.activity.AKTGetEmailActivity;
import net.safemoon.androidwallet.dialogs.ProgressLoading;
import net.safemoon.androidwallet.viewmodels.AKTWebSocketHandlingViewModel;

/* compiled from: AKTGetEmailActivity.kt */
/* loaded from: classes2.dex */
public final class AKTGetEmailActivity extends AKTServerFunctions {
    public static final a s0 = new a(null);
    public final sy1 m0 = zy1.a(new AKTGetEmailActivity$isForgotPassword$2(this));
    public final sy1 n0 = zy1.a(new AKTGetEmailActivity$isRegister$2(this));
    public final sy1 o0 = zy1.a(new AKTGetEmailActivity$isChangeEmail$2(this));
    public final sy1 p0 = zy1.a(new AKTGetEmailActivity$binding$2(this));
    public final sy1 q0 = new fj4(d53.b(AKTWebSocketHandlingViewModel.class), new AKTGetEmailActivity$special$$inlined$viewModels$default$2(this), new AKTGetEmailActivity$special$$inlined$viewModels$default$1(this));
    public String r0 = "";

    /* compiled from: AKTGetEmailActivity.kt */
    /* loaded from: classes2.dex */
    public static final class a {
        public a() {
        }

        public /* synthetic */ a(qi0 qi0Var) {
            this();
        }

        public static /* synthetic */ void b(a aVar, Context context, boolean z, boolean z2, boolean z3, int i, Object obj) {
            if ((i & 2) != 0) {
                z = false;
            }
            if ((i & 4) != 0) {
                z2 = false;
            }
            if ((i & 8) != 0) {
                z3 = false;
            }
            aVar.a(context, z, z2, z3);
        }

        public final void a(Context context, boolean z, boolean z2, boolean z3) {
            fs1.f(context, "context");
            Intent intent = new Intent(context, AKTGetEmailActivity.class);
            intent.putExtra("IS_FORGOT_PASSWORD", z);
            intent.putExtra("IS_REGISTER_MASTER", z2);
            intent.putExtra("IS_CHANGE_MASTER_EMAIL", z3);
            context.startActivity(intent);
        }
    }

    /* compiled from: AKTGetEmailActivity.kt */
    /* loaded from: classes2.dex */
    public static final class b implements TextWatcher {
        public b() {
        }

        @Override // android.text.TextWatcher
        public void afterTextChanged(Editable editable) {
            fs1.f(editable, "s");
            AKTGetEmailActivity.this.F0(editable.toString());
        }

        @Override // android.text.TextWatcher
        public void beforeTextChanged(CharSequence charSequence, int i, int i2, int i3) {
            fs1.f(charSequence, "s");
        }

        @Override // android.text.TextWatcher
        public void onTextChanged(CharSequence charSequence, int i, int i2, int i3) {
            fs1.f(charSequence, "s");
        }
    }

    /* JADX WARN: Multi-variable type inference failed */
    public static /* synthetic */ void C0(AKTGetEmailActivity aKTGetEmailActivity, Integer num, int i, int i2, rc1 rc1Var, int i3, Object obj) {
        if ((i3 & 1) != 0) {
            num = null;
        }
        if ((i3 & 4) != 0) {
            i2 = R.string.action_ok;
        }
        if ((i3 & 8) != 0) {
            rc1Var = null;
        }
        aKTGetEmailActivity.B0(num, i, i2, rc1Var);
    }

    public static final void q0(AKTGetEmailActivity aKTGetEmailActivity, String str) {
        fs1.f(aKTGetEmailActivity, "this$0");
        if (str == null) {
            return;
        }
        aKTGetEmailActivity.j0.h();
        qy4 qy4Var = aKTGetEmailActivity.l0;
        fs1.e(qy4Var, "safeMoonppp");
        aKTGetEmailActivity.X(qy4Var, str);
    }

    public static final void u0(AKTGetEmailActivity aKTGetEmailActivity, View view) {
        fs1.f(aKTGetEmailActivity, "this$0");
        aKTGetEmailActivity.onBackPressed();
    }

    public static final void v0(AKTGetEmailActivity aKTGetEmailActivity, View view) {
        fs1.f(aKTGetEmailActivity, "this$0");
        aKTGetEmailActivity.onBackPressed();
    }

    public static final void w0(AKTGetEmailActivity aKTGetEmailActivity, View view) {
        fs1.f(aKTGetEmailActivity, "this$0");
        pg4.e(aKTGetEmailActivity);
    }

    public static final void x0(e7 e7Var, AKTGetEmailActivity aKTGetEmailActivity, View view) {
        String b2;
        fs1.f(e7Var, "$this_apply");
        fs1.f(aKTGetEmailActivity, "this$0");
        EditText editText = e7Var.e.getEditText();
        String obj = StringsKt__StringsKt.L0(String.valueOf(editText == null ? null : editText.getText())).toString();
        if (obj.length() == 0) {
            aKTGetEmailActivity.D0();
        } else if (!s44.d(obj)) {
            aKTGetEmailActivity.E0();
        } else {
            ProgressLoading.a aVar = ProgressLoading.y0;
            String string = aKTGetEmailActivity.getString(R.string.loading);
            fs1.e(string, "getString(R.string.loading)");
            ProgressLoading a2 = aVar.a(false, string, "");
            aKTGetEmailActivity.j0 = a2;
            FragmentManager supportFragmentManager = aKTGetEmailActivity.getSupportFragmentManager();
            fs1.e(supportFragmentManager, "supportFragmentManager");
            a2.A(supportFragmentManager);
            aKTGetEmailActivity.d0("EMAILRAW", obj);
            aKTGetEmailActivity.l0.c("EMAILRAW", obj);
            String L = AKTServerFunctions.L(obj);
            aKTGetEmailActivity.d0("EMAIL", L);
            aKTGetEmailActivity.l0.c("EMAIL", L);
            if (s.b) {
                b2 = aKTGetEmailActivity.K(aKTGetEmailActivity.l0, obj, aKTGetEmailActivity.s0());
                fs1.e(b2, "{\n                    SF…      )\n                }");
            } else {
                b2 = i2.b(aKTGetEmailActivity.l0, "AKTPPP03");
                fs1.e(b2, "{\n                    AK…PPP03\")\n                }");
            }
            aKTGetEmailActivity.A0(b2);
        }
    }

    public void A0(String str) {
        if (str == null) {
            return;
        }
        o0().k(str);
    }

    public final void B0(Integer num, int i, int i2, rc1<te4> rc1Var) {
        if (r0()) {
            bh.X(new WeakReference(this), num, Integer.valueOf(i), i2, rc1Var);
        } else {
            bh.U(new WeakReference(this), num, Integer.valueOf(i), i2, rc1Var);
        }
    }

    public final void D0() {
        C0(this, null, R.string.akt_error_email_empty_message, 0, new AKTGetEmailActivity$showEmailEmptyDialog$1(this), 5, null);
    }

    public final void E0() {
        C0(this, null, R.string.akt_error_invalid_email_format_message, 0, new AKTGetEmailActivity$showInvalidEmailFormatDialog$1(this), 5, null);
    }

    public final void F0(String str) {
        if (!(str.length() == 0) && s44.d(str)) {
            n0().b.setTextColor(getColor(R.color.black));
            n0().b.setEnabled(true);
            n0().b.setBackgroundTintList(getColorStateList(R.color.akt_button_active));
            return;
        }
        n0().b.setTextColor(getColor(R.color.white));
        n0().b.setEnabled(false);
        n0().b.setBackgroundTintList(getColorStateList(R.color.akt_button_inactive));
    }

    @Override // com.AKT.anonymouskey.ui.login.AKTServerFunctions
    public void Q() {
        o0().f();
    }

    @Override // com.AKT.anonymouskey.ui.login.AKTServerFunctions
    public void X(qy4 qy4Var, String str) {
        fs1.f(qy4Var, "safeMoonppp");
        if (W(str)) {
            return;
        }
        String[] v = i2.v(b30.a.v(this, str), "|");
        fs1.e(v, "parts");
        if (!(v.length == 0)) {
            String str2 = v[0];
            fs1.e(str2, "parts[0]");
            if (StringsKt__StringsKt.w0(str2, new String[]{"="}, false, 0, 6, null).size() >= 2) {
                String str3 = v[0];
                fs1.e(str3, "parts[0]");
                String str4 = (String) StringsKt__StringsKt.w0(str3, new String[]{"="}, false, 0, 6, null).get(1);
                Objects.requireNonNull(str4, "null cannot be cast to non-null type java.lang.String");
                String upperCase = str4.toUpperCase(Locale.ROOT);
                fs1.e(upperCase, "(this as java.lang.Strin….toUpperCase(Locale.ROOT)");
                Q();
                int hashCode = upperCase.hashCode();
                if (hashCode != -1299641089) {
                    if (hashCode != 325952731) {
                        if (hashCode == 1251582055 && upperCase.equals("FORGOT04")) {
                            z0(v);
                            return;
                        }
                    } else if (upperCase.equals("AKTSERVERERROR")) {
                        Y(v, null);
                        return;
                    }
                } else if (upperCase.equals("AKTSFMICE04")) {
                    y0(qy4Var, v);
                    AKTVerifyEmailActivity.x0.a(this, this.r0, r0());
                    return;
                }
                super.X(qy4Var, str);
            }
        }
    }

    @Override // com.AKT.anonymouskey.ui.login.AKTServerFunctions
    public void Y(String[] strArr, String str) {
        this.j0.h();
        if (s0()) {
            C0(this, null, R.string.akt_error_email_not_found_message, 0, null, 13, null);
        } else {
            C0(this, null, R.string.akt_error_email_already_exist_message, 0, null, 13, null);
        }
    }

    public final e7 n0() {
        return (e7) this.p0.getValue();
    }

    public final AKTWebSocketHandlingViewModel o0() {
        return (AKTWebSocketHandlingViewModel) this.q0.getValue();
    }

    @Override // com.AKT.anonymouskey.ui.login.AKTServerFunctions, net.safemoon.androidwallet.activity.common.BasicActivity, androidx.fragment.app.FragmentActivity, androidx.activity.ComponentActivity, androidx.core.app.ComponentActivity, android.app.Activity
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        setContentView(n0().b());
        o0().l();
        p0();
        ActionBar supportActionBar = getSupportActionBar();
        if (supportActionBar != null) {
            supportActionBar.l();
        }
        getWindow().addFlags(Integer.MIN_VALUE);
        getWindow().setStatusBarColor(m70.d(this, 17170445));
        if (!r0()) {
            getWindow().setBackgroundDrawable(new ColorDrawable(getColor(R.color.akt_night_background)));
        } else {
            getWindow().setBackgroundDrawableResource(R.drawable.bottom_curve);
        }
    }

    @Override // net.safemoon.androidwallet.activity.common.BasicActivity, androidx.appcompat.app.AppCompatActivity, android.app.Activity
    public void onPostCreate(Bundle bundle) {
        super.onPostCreate(bundle);
        final e7 n0 = n0();
        if (r0()) {
            n0.g.setText(getString(R.string.akt_get_email_change_email_address_label_title));
            int d = m70.d(this, R.color.t1);
            n0.h.c.setVisibility(8);
            n0.h.b.setVisibility(0);
            n0.g.setTextColor(d);
            n0.i.setTextColor(d);
            EditText editText = n0.e.getEditText();
            if (editText != null) {
                editText.setTextColor(d);
            }
            n0.e.setEndIconTintList(ColorStateList.valueOf(d));
            n0.f.setBackgroundColor(d);
            n0.d.setImageResource(R.drawable.ic_logo_dark);
            n0.h.b().setBackgroundColor(m70.d(this, R.color.toolbar_bg_color));
            n0.h.e.setVisibility(0);
            n0.h.e.setText(getString(R.string.screen_change_email_address_title));
            n0.c.setBackgroundColor(m70.d(this, R.color.p0));
        } else {
            n0.h.e.setVisibility(8);
            n0.h.b.setVisibility(0);
            n0.h.c.setVisibility(8);
            n0.h.c.setText(getString(R.string.cancel));
            if (t0()) {
                n0.h.e.setVisibility(0);
                n0.h.e.setText(getString(R.string.akt_activity_register_txt));
            } else {
                n0.h.e.setVisibility(0);
                n0.h.e.setText(getString(R.string.akt_reset_password_header_title));
            }
        }
        n0.h.c.setOnClickListener(new View.OnClickListener() { // from class: a0
            @Override // android.view.View.OnClickListener
            public final void onClick(View view) {
                AKTGetEmailActivity.u0(AKTGetEmailActivity.this, view);
            }
        });
        n0.h.b.setOnClickListener(new View.OnClickListener() { // from class: z
            @Override // android.view.View.OnClickListener
            public final void onClick(View view) {
                AKTGetEmailActivity.v0(AKTGetEmailActivity.this, view);
            }
        });
        n0.c.setOnClickListener(new View.OnClickListener() { // from class: b0
            @Override // android.view.View.OnClickListener
            public final void onClick(View view) {
                AKTGetEmailActivity.w0(AKTGetEmailActivity.this, view);
            }
        });
        EditText editText2 = n0.e.getEditText();
        if (editText2 != null) {
            editText2.addTextChangedListener(new b());
        }
        F0("");
        n0.b.setOnClickListener(new View.OnClickListener() { // from class: y
            @Override // android.view.View.OnClickListener
            public final void onClick(View view) {
                AKTGetEmailActivity.x0(e7.this, this, view);
            }
        });
        if (s0()) {
            n0.i.setVisibility(8);
        }
    }

    public void p0() {
        o0().h().observe(this, new tl2() { // from class: x
            @Override // defpackage.tl2
            public final void onChanged(Object obj) {
                AKTGetEmailActivity.q0(AKTGetEmailActivity.this, (String) obj);
            }
        });
    }

    public final boolean r0() {
        return ((Boolean) this.o0.getValue()).booleanValue();
    }

    public final boolean s0() {
        return ((Boolean) this.m0.getValue()).booleanValue();
    }

    public final boolean t0() {
        return ((Boolean) this.n0.getValue()).booleanValue();
    }

    public final void y0(qy4 qy4Var, String[] strArr) {
        int length = strArr.length;
        int i = 0;
        while (i < length) {
            String str = strArr[i];
            i++;
            List w0 = StringsKt__StringsKt.w0(str, new String[]{"="}, false, 0, 6, null);
            if (w0.size() > 1) {
                String str2 = (String) j20.L(w0);
                int hashCode = str2.hashCode();
                if (hashCode != -547471898) {
                    if (hashCode != 2313765) {
                        if (hashCode == 1004359981 && str2.equals("QUESTIONS")) {
                            String j = q.j(bo3.i(this, "ICEPRIVKEY"), s.g, (String) w0.get(1));
                            if (j == null) {
                                j = "";
                            }
                            int d0 = StringsKt__StringsKt.d0(j, '|', 0, false, 6, null);
                            if (d0 + 5 > j.length()) {
                                if (j.length() > 0) {
                                    j = j.substring(0, d0);
                                    fs1.e(j, "(this as java.lang.Strin…ing(startIndex, endIndex)");
                                }
                            }
                            qy4Var.c("questions", j);
                        }
                    } else if (str2.equals("KPPP")) {
                        qy4Var.c("KPP", (String) w0.get(1));
                    }
                } else if (str2.equals("VERIFYCODE")) {
                    this.r0 = (String) w0.get(1);
                }
            }
        }
    }

    public final void z0(String[] strArr) {
        int length = strArr.length;
        String str = "";
        int i = 0;
        while (i < length) {
            String str2 = strArr[i];
            i++;
            List w0 = StringsKt__StringsKt.w0(str2, new String[]{"="}, false, 0, 6, null);
            if (w0.size() > 1) {
                String str3 = (String) j20.L(w0);
                Objects.requireNonNull(str3, "null cannot be cast to non-null type java.lang.String");
                String upperCase = str3.toUpperCase(Locale.ROOT);
                fs1.e(upperCase, "(this as java.lang.Strin….toUpperCase(Locale.ROOT)");
                if (fs1.b(upperCase, "QUESTIONS")) {
                    str = (String) w0.get(1);
                }
            }
        }
        String V = V(str);
        fs1.e(V, "lson");
        String str4 = "";
        String str5 = str4;
        String str6 = str5;
        for (String str7 : StringsKt__StringsKt.w0(V, new String[]{"|"}, false, 0, 6, null)) {
            List w02 = StringsKt__StringsKt.w0(str7, new String[]{"="}, false, 0, 6, null);
            if (w02.size() > 1) {
                String str8 = (String) w02.get(0);
                int hashCode = str8.hashCode();
                if (hashCode != 2408) {
                    if (hashCode != 2560) {
                        if (hashCode == 2561 && str8.equals("Q2")) {
                            str5 = (String) w02.get(1);
                        }
                    } else if (str8.equals("Q1")) {
                        str4 = (String) w02.get(1);
                    }
                } else if (str8.equals("KS")) {
                    str6 = (String) w02.get(1);
                }
            }
        }
        if (!(str6.length() == 0)) {
            if (!(str4.length() == 0)) {
                if (!(str5.length() == 0)) {
                    AKTAnswerQuestionsActivity.a.b(AKTAnswerQuestionsActivity.u0, this, str4, str5, str6, true, false, 32, null);
                    return;
                }
            }
        }
        C0(this, null, R.string.akt_error_cannot_get_questions_message, R.string.action_ok, null, 9, null);
    }
}
