package net.safemoon.androidwallet.activity;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import androidx.appcompat.app.ActionBar;
import net.safemoon.androidwallet.R;
import net.safemoon.androidwallet.activity.ImportWalletOptionActivity;
import net.safemoon.androidwallet.activity.common.BasicActivity;

/* compiled from: ImportWalletOptionActivity.kt */
/* loaded from: classes2.dex */
public final class ImportWalletOptionActivity extends BasicActivity {
    public static final a k0 = new a(null);
    public final sy1 j0 = zy1.a(new ImportWalletOptionActivity$binding$2(this));

    /* compiled from: ImportWalletOptionActivity.kt */
    /* loaded from: classes2.dex */
    public static final class a {
        public a() {
        }

        public /* synthetic */ a(qi0 qi0Var) {
            this();
        }

        public final void a(Context context) {
            fs1.f(context, "context");
            context.startActivity(new Intent(context, ImportWalletOptionActivity.class));
        }
    }

    public static final void P(ImportWalletOptionActivity importWalletOptionActivity, View view) {
        fs1.f(importWalletOptionActivity, "this$0");
        ImportWordActivity.p0.a(importWalletOptionActivity, 12);
    }

    public static final void Q(ImportWalletOptionActivity importWalletOptionActivity, View view) {
        fs1.f(importWalletOptionActivity, "this$0");
        ImportWordActivity.p0.a(importWalletOptionActivity, 24);
    }

    public static final void R(ImportWalletOptionActivity importWalletOptionActivity, View view) {
        fs1.f(importWalletOptionActivity, "this$0");
        ImportPrivateKeyActivity.n0.a(importWalletOptionActivity);
    }

    public static final void S(ImportWalletOptionActivity importWalletOptionActivity, View view) {
        fs1.f(importWalletOptionActivity, "this$0");
        importWalletOptionActivity.onBackPressed();
    }

    public final h7 O() {
        return (h7) this.j0.getValue();
    }

    @Override // net.safemoon.androidwallet.activity.common.BasicActivity, androidx.fragment.app.FragmentActivity, androidx.activity.ComponentActivity, androidx.core.app.ComponentActivity, android.app.Activity
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        setContentView(O().b());
        ActionBar supportActionBar = getSupportActionBar();
        if (supportActionBar != null) {
            supportActionBar.l();
        }
        getWindow().addFlags(Integer.MIN_VALUE);
        getWindow().setStatusBarColor(m70.d(this, 17170445));
        getWindow().setBackgroundDrawableResource(R.drawable.ic_bg);
    }

    @Override // net.safemoon.androidwallet.activity.common.BasicActivity, androidx.appcompat.app.AppCompatActivity, android.app.Activity
    public void onPostCreate(Bundle bundle) {
        super.onPostCreate(bundle);
        h7 O = O();
        O.d.setOnClickListener(new View.OnClickListener() { // from class: op1
            @Override // android.view.View.OnClickListener
            public final void onClick(View view) {
                ImportWalletOptionActivity.P(ImportWalletOptionActivity.this, view);
            }
        });
        O.e.setOnClickListener(new View.OnClickListener() { // from class: pp1
            @Override // android.view.View.OnClickListener
            public final void onClick(View view) {
                ImportWalletOptionActivity.Q(ImportWalletOptionActivity.this, view);
            }
        });
        O.c.setOnClickListener(new View.OnClickListener() { // from class: qp1
            @Override // android.view.View.OnClickListener
            public final void onClick(View view) {
                ImportWalletOptionActivity.R(ImportWalletOptionActivity.this, view);
            }
        });
        O.b.setOnClickListener(new View.OnClickListener() { // from class: np1
            @Override // android.view.View.OnClickListener
            public final void onClick(View view) {
                ImportWalletOptionActivity.S(ImportWalletOptionActivity.this, view);
            }
        });
        C();
    }
}
