package net.safemoon.androidwallet.activity;

import android.content.Context;
import android.content.Intent;
import android.graphics.PorterDuff;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import androidx.appcompat.app.ActionBar;
import androidx.fragment.app.FragmentManager;
import com.AKT.anonymouskey.ui.login.AKTServerFunctions;
import java.lang.ref.WeakReference;
import java.util.Arrays;
import java.util.Locale;
import java.util.Objects;
import kotlin.text.StringsKt__StringsKt;
import net.safemoon.androidwallet.R;
import net.safemoon.androidwallet.activity.AKTChangePasswordActivity;
import net.safemoon.androidwallet.dialogs.ProgressLoading;
import net.safemoon.androidwallet.viewmodels.AKTWebSocketHandlingViewModel;

/* compiled from: AKTChangePasswordActivity.kt */
/* loaded from: classes2.dex */
public final class AKTChangePasswordActivity extends AKTServerFunctions {
    public static final a o0 = new a(null);
    public final sy1 m0 = zy1.a(new AKTChangePasswordActivity$binding$2(this));
    public final sy1 n0 = new fj4(d53.b(AKTWebSocketHandlingViewModel.class), new AKTChangePasswordActivity$special$$inlined$viewModels$default$2(this), new AKTChangePasswordActivity$special$$inlined$viewModels$default$1(this));

    /* compiled from: AKTChangePasswordActivity.kt */
    /* loaded from: classes2.dex */
    public static final class a {
        public a() {
        }

        public /* synthetic */ a(qi0 qi0Var) {
            this();
        }

        public final void a(Context context) {
            fs1.f(context, "context");
            context.startActivity(new Intent(context, AKTChangePasswordActivity.class));
        }
    }

    /* compiled from: AKTChangePasswordActivity.kt */
    /* loaded from: classes2.dex */
    public static final class b implements TextWatcher {
        public final /* synthetic */ q6 a;
        public final /* synthetic */ AKTChangePasswordActivity f0;

        public b(q6 q6Var, AKTChangePasswordActivity aKTChangePasswordActivity) {
            this.a = q6Var;
            this.f0 = aKTChangePasswordActivity;
        }

        @Override // android.text.TextWatcher
        public void afterTextChanged(Editable editable) {
            fs1.f(editable, "s");
            String obj = editable.toString();
            String b = b30.a.b(obj);
            if (fs1.b(obj, b)) {
                this.f0.n0();
                return;
            }
            this.a.f.setText(b);
            this.a.f.setSelection(b.length());
        }

        @Override // android.text.TextWatcher
        public void beforeTextChanged(CharSequence charSequence, int i, int i2, int i3) {
            fs1.f(charSequence, "s");
        }

        @Override // android.text.TextWatcher
        public void onTextChanged(CharSequence charSequence, int i, int i2, int i3) {
            fs1.f(charSequence, "s");
        }
    }

    /* compiled from: AKTChangePasswordActivity.kt */
    /* loaded from: classes2.dex */
    public static final class c implements TextWatcher {
        public final /* synthetic */ q6 a;
        public final /* synthetic */ AKTChangePasswordActivity f0;

        public c(q6 q6Var, AKTChangePasswordActivity aKTChangePasswordActivity) {
            this.a = q6Var;
            this.f0 = aKTChangePasswordActivity;
        }

        @Override // android.text.TextWatcher
        public void afterTextChanged(Editable editable) {
            fs1.f(editable, "s");
            String obj = editable.toString();
            String b = b30.a.b(obj);
            if (fs1.b(obj, b)) {
                this.f0.n0();
                this.f0.o0(editable);
                return;
            }
            this.a.e.setText(b);
            this.a.e.setSelection(b.length());
        }

        @Override // android.text.TextWatcher
        public void beforeTextChanged(CharSequence charSequence, int i, int i2, int i3) {
            fs1.f(charSequence, "s");
        }

        @Override // android.text.TextWatcher
        public void onTextChanged(CharSequence charSequence, int i, int i2, int i3) {
            fs1.f(charSequence, "s");
        }
    }

    /* compiled from: AKTChangePasswordActivity.kt */
    /* loaded from: classes2.dex */
    public static final class d implements TextWatcher {
        public final /* synthetic */ q6 a;
        public final /* synthetic */ AKTChangePasswordActivity f0;

        public d(q6 q6Var, AKTChangePasswordActivity aKTChangePasswordActivity) {
            this.a = q6Var;
            this.f0 = aKTChangePasswordActivity;
        }

        @Override // android.text.TextWatcher
        public void afterTextChanged(Editable editable) {
            fs1.f(editable, "s");
            String obj = editable.toString();
            String b = b30.a.b(obj);
            if (fs1.b(obj, b)) {
                this.f0.n0();
                return;
            }
            this.a.d.setText(b);
            this.a.d.setSelection(b.length());
        }

        @Override // android.text.TextWatcher
        public void beforeTextChanged(CharSequence charSequence, int i, int i2, int i3) {
            fs1.f(charSequence, "s");
        }

        @Override // android.text.TextWatcher
        public void onTextChanged(CharSequence charSequence, int i, int i2, int i3) {
            fs1.f(charSequence, "s");
        }
    }

    public static final void t0(AKTChangePasswordActivity aKTChangePasswordActivity, String str) {
        fs1.f(aKTChangePasswordActivity, "this$0");
        if (str == null) {
            return;
        }
        aKTChangePasswordActivity.X(aKTChangePasswordActivity.l0, str);
        aKTChangePasswordActivity.Q();
    }

    public static final void u0(AKTChangePasswordActivity aKTChangePasswordActivity, View view) {
        fs1.f(aKTChangePasswordActivity, "this$0");
        aKTChangePasswordActivity.onBackPressed();
    }

    public static final void v0(AKTChangePasswordActivity aKTChangePasswordActivity, View view) {
        fs1.f(aKTChangePasswordActivity, "this$0");
        if (view.getAlpha() == 1.0f) {
            aKTChangePasswordActivity.z0();
        }
    }

    public static final void w0(AKTChangePasswordActivity aKTChangePasswordActivity, View view) {
        fs1.f(aKTChangePasswordActivity, "this$0");
        pg4.e(aKTChangePasswordActivity);
    }

    @Override // com.AKT.anonymouskey.ui.login.AKTServerFunctions
    public void Q() {
        q0().f();
    }

    @Override // com.AKT.anonymouskey.ui.login.AKTServerFunctions
    public void X(qy4 qy4Var, String str) {
        if (W(str)) {
            return;
        }
        ProgressLoading progressLoading = this.j0;
        if (progressLoading != null) {
            progressLoading.h();
        }
        String[] v = i2.v(b30.a.v(this, str), "|");
        fs1.e(v, "parts");
        if (!(v.length == 0)) {
            String str2 = v[0];
            fs1.e(str2, "parts[0]");
            if (StringsKt__StringsKt.w0(str2, new String[]{"="}, false, 0, 6, null).size() >= 2) {
                String str3 = v[0];
                fs1.e(str3, "parts[0]");
                String str4 = (String) StringsKt__StringsKt.w0(str3, new String[]{"="}, false, 0, 6, null).get(1);
                Objects.requireNonNull(str4, "null cannot be cast to non-null type java.lang.String");
                String upperCase = str4.toUpperCase(Locale.ROOT);
                fs1.e(upperCase, "(this as java.lang.Strin….toUpperCase(Locale.ROOT)");
                Q();
                if (fs1.b(upperCase, "AKTSERVERERROR")) {
                    Y(v, null);
                    return;
                }
                String g = qy4Var == null ? null : qy4Var.g("tempU5");
                String g2 = qy4Var == null ? null : qy4Var.g("tempKA");
                String g3 = qy4Var == null ? null : qy4Var.g("tempUK5");
                String g4 = qy4Var != null ? qy4Var.g("tempK5") : null;
                if (g != null) {
                    bo3.o(this, "U5", g);
                    qy4Var.c("U5", g);
                }
                if (g2 != null) {
                    bo3.o(this, "KA", g2);
                    qy4Var.c("KA", g2);
                }
                if (g4 != null) {
                    jv0.e(this, "K5", g4);
                    qy4Var.c("K5", g4);
                }
                if (g3 != null) {
                    jv0.e(this, "UK5", g3);
                    qy4Var.c("UK5", g3);
                }
                bh.Y(new WeakReference(this), null, Integer.valueOf((int) R.string.akt_change_password_dialog_success_content), R.string.action_ok, new AKTChangePasswordActivity$parseMessage$5(this), 2, null);
            }
        }
    }

    @Override // com.AKT.anonymouskey.ui.login.AKTServerFunctions
    public void Y(String[] strArr, String str) {
        bh.Y(new WeakReference(this), null, Integer.valueOf((int) R.string.akt_error_change_password_message), 0, null, 26, null);
    }

    /* JADX WARN: Removed duplicated region for block: B:36:0x006f  */
    /* JADX WARN: Removed duplicated region for block: B:46:0x009f  */
    /* JADX WARN: Removed duplicated region for block: B:48:0x00a2  */
    /* JADX WARN: Removed duplicated region for block: B:55:0x00bf  */
    /* JADX WARN: Removed duplicated region for block: B:56:0x00c7  */
    /*
        Code decompiled incorrectly, please refer to instructions dump.
        To view partially-correct code enable 'Show inconsistent code' option in preferences
    */
    public final void n0() {
        /*
            r8 = this;
            q6 r0 = r8.p0()
            androidx.appcompat.widget.AppCompatEditText r1 = r0.f
            android.text.Editable r1 = r1.getText()
            r2 = 1
            r3 = 0
            if (r1 == 0) goto L17
            int r1 = r1.length()
            if (r1 != 0) goto L15
            goto L17
        L15:
            r1 = r3
            goto L18
        L17:
            r1 = r2
        L18:
            if (r1 != 0) goto L45
            androidx.appcompat.widget.AppCompatEditText r1 = r0.e
            android.text.Editable r1 = r1.getText()
            if (r1 == 0) goto L2b
            int r1 = r1.length()
            if (r1 != 0) goto L29
            goto L2b
        L29:
            r1 = r3
            goto L2c
        L2b:
            r1 = r2
        L2c:
            if (r1 != 0) goto L45
            androidx.appcompat.widget.AppCompatEditText r1 = r0.d
            android.text.Editable r1 = r1.getText()
            if (r1 == 0) goto L3f
            int r1 = r1.length()
            if (r1 != 0) goto L3d
            goto L3f
        L3d:
            r1 = r3
            goto L40
        L3f:
            r1 = r2
        L40:
            if (r1 == 0) goto L43
            goto L45
        L43:
            r1 = r2
            goto L46
        L45:
            r1 = r3
        L46:
            androidx.appcompat.widget.AppCompatEditText r4 = r0.e
            android.text.Editable r4 = r4.getText()
            java.lang.String r4 = java.lang.String.valueOf(r4)
            androidx.appcompat.widget.AppCompatEditText r5 = r0.d
            android.text.Editable r5 = r5.getText()
            java.lang.String r5 = java.lang.String.valueOf(r5)
            int r6 = r4.length()
            r7 = 8
            if (r6 < r7) goto L68
            boolean r6 = defpackage.s44.g(r4)
            if (r6 != 0) goto L69
        L68:
            r1 = r3
        L69:
            boolean r4 = defpackage.fs1.b(r4, r5)
            if (r4 != 0) goto L83
            int r4 = r5.length()
            if (r4 <= 0) goto L77
            r4 = r2
            goto L78
        L77:
            r4 = r3
        L78:
            if (r4 == 0) goto L83
            qy1 r1 = r0.g
            android.widget.TextView r1 = r1.g
            r1.setVisibility(r3)
            r1 = r3
            goto L8a
        L83:
            qy1 r4 = r0.g
            android.widget.TextView r4 = r4.g
            r4.setVisibility(r7)
        L8a:
            q6 r4 = r8.p0()
            androidx.appcompat.widget.AppCompatEditText r4 = r4.f
            android.text.Editable r4 = r4.getText()
            java.lang.String r4 = java.lang.String.valueOf(r4)
            int r5 = r4.length()
            if (r5 <= 0) goto L9f
            goto La0
        L9f:
            r2 = r3
        La0:
            if (r2 == 0) goto Lb7
            int r2 = r4.length()
            r5 = 7
            if (r2 < r5) goto Lb1
            int r2 = r4.length()
            r4 = 18
            if (r2 <= r4) goto Lb7
        Lb1:
            android.widget.TextView r1 = r0.i
            r1.setVisibility(r3)
            goto Lbd
        Lb7:
            android.widget.TextView r2 = r0.i
            r2.setVisibility(r7)
            r3 = r1
        Lbd:
            if (r3 == 0) goto Lc7
            androidx.appcompat.widget.AppCompatButton r1 = r0.b
            r2 = 1065353216(0x3f800000, float:1.0)
            r1.setAlpha(r2)
            goto Lce
        Lc7:
            androidx.appcompat.widget.AppCompatButton r1 = r0.b
            r2 = 1056964608(0x3f000000, float:0.5)
            r1.setAlpha(r2)
        Lce:
            androidx.appcompat.widget.AppCompatButton r0 = r0.b
            r0.setEnabled(r3)
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: net.safemoon.androidwallet.activity.AKTChangePasswordActivity.n0():void");
    }

    public final void o0(Editable editable) {
        if (editable.length() > 0) {
            qy1 qy1Var = p0().g;
            fs1.e(qy1Var, "binding.lPasswordRequirements");
            String obj = editable.toString();
            if (obj.length() > 7) {
                qy1Var.d.setTextColor(m70.d(this, R.color.btn_light_green));
                qy1Var.a.setColorFilter(m70.d(this, R.color.btn_light_green), PorterDuff.Mode.MULTIPLY);
                qy1Var.d.setTag("complete");
            } else {
                qy1Var.d.setTextColor(m70.d(this, R.color.color_edit));
                qy1Var.a.setColorFilter(m70.d(this, R.color.color_edit), PorterDuff.Mode.MULTIPLY);
                qy1Var.d.setTag("incomplete");
            }
            if (s44.h(obj)) {
                qy1Var.h.setTextColor(m70.d(this, R.color.btn_light_green));
                qy1Var.b.setColorFilter(m70.d(this, R.color.btn_light_green), PorterDuff.Mode.MULTIPLY);
                qy1Var.h.setTag("complete");
            } else {
                qy1Var.h.setTextColor(m70.d(this, R.color.color_edit));
                qy1Var.b.setColorFilter(m70.d(this, R.color.color_edit), PorterDuff.Mode.MULTIPLY);
                qy1Var.h.setTag("incomplete");
            }
            if (s44.e(obj)) {
                qy1Var.f.setTextColor(m70.d(this, R.color.btn_light_green));
                qy1Var.c.setColorFilter(m70.d(this, R.color.btn_light_green), PorterDuff.Mode.MULTIPLY);
                qy1Var.f.setTag("complete");
                return;
            }
            qy1Var.f.setTextColor(m70.d(this, R.color.color_edit));
            qy1Var.c.setColorFilter(m70.d(this, R.color.color_edit), PorterDuff.Mode.MULTIPLY);
            qy1Var.f.setTag("incomplete");
        }
    }

    @Override // com.AKT.anonymouskey.ui.login.AKTServerFunctions, net.safemoon.androidwallet.activity.common.BasicActivity, androidx.fragment.app.FragmentActivity, androidx.activity.ComponentActivity, androidx.core.app.ComponentActivity, android.app.Activity
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        setContentView(p0().b());
        ActionBar supportActionBar = getSupportActionBar();
        if (supportActionBar != null) {
            supportActionBar.l();
        }
        getWindow().addFlags(Integer.MIN_VALUE);
        getWindow().setStatusBarColor(m70.d(this, 17170445));
        getWindow().setBackgroundDrawableResource(R.drawable.bottom_curve);
        q0().l();
        s0();
    }

    @Override // net.safemoon.androidwallet.activity.common.BasicActivity, androidx.appcompat.app.AppCompatActivity, android.app.Activity
    public void onPostCreate(Bundle bundle) {
        super.onPostCreate(bundle);
        q6 p0 = p0();
        p0.h.a.setOnClickListener(new View.OnClickListener() { // from class: o
            @Override // android.view.View.OnClickListener
            public final void onClick(View view) {
                AKTChangePasswordActivity.u0(AKTChangePasswordActivity.this, view);
            }
        });
        p0.h.c.setText(R.string.screen_change_login_credential_title);
        p0.f.addTextChangedListener(new b(p0, this));
        p0.e.addTextChangedListener(new c(p0, this));
        p0.d.addTextChangedListener(new d(p0, this));
        p0.b.setOnClickListener(new View.OnClickListener() { // from class: p
            @Override // android.view.View.OnClickListener
            public final void onClick(View view) {
                AKTChangePasswordActivity.v0(AKTChangePasswordActivity.this, view);
            }
        });
        p0.c.setOnClickListener(new View.OnClickListener() { // from class: n
            @Override // android.view.View.OnClickListener
            public final void onClick(View view) {
                AKTChangePasswordActivity.w0(AKTChangePasswordActivity.this, view);
            }
        });
    }

    public final q6 p0() {
        return (q6) this.m0.getValue();
    }

    public final AKTWebSocketHandlingViewModel q0() {
        return (AKTWebSocketHandlingViewModel) this.n0.getValue();
    }

    public final void r0() {
        ProgressLoading.a aVar = ProgressLoading.y0;
        String string = getString(R.string.loading);
        fs1.e(string, "getString(R.string.loading)");
        ProgressLoading a2 = aVar.a(false, string, "");
        this.j0 = a2;
        FragmentManager supportFragmentManager = getSupportFragmentManager();
        fs1.e(supportFragmentManager, "supportFragmentManager");
        a2.A(supportFragmentManager);
        String valueOf = String.valueOf(p0().f.getText());
        String valueOf2 = String.valueOf(p0().e.getText());
        b30 b30Var = b30.a;
        y0(x0(b30Var.n(valueOf), b30Var.n(valueOf2)));
    }

    public void s0() {
        q0().h().observe(this, new tl2() { // from class: m
            @Override // defpackage.tl2
            public final void onChanged(Object obj) {
                AKTChangePasswordActivity.t0(AKTChangePasswordActivity.this, (String) obj);
            }
        });
    }

    public final String x0(String str, String str2) {
        String i = bo3.i(this, "KA");
        String c2 = jv0.c(this, "UK5");
        if (c2 == null || c2.length() == 0) {
            c2 = i2.p(this.l0, "UK5");
        }
        String f0 = f0(q.f(c2, i), str, str2);
        fs1.e(f0, "updateKAandU5(pbHex, username, password)");
        return f0;
    }

    public void y0(String str) {
        if (str == null) {
            return;
        }
        q0().k(str);
    }

    public final void z0() {
        lu3 lu3Var = lu3.a;
        String string = getString(R.string.akt_reset_password_confirm_dialog_content);
        fs1.e(string, "getString(R.string.akt_r…d_confirm_dialog_content)");
        String format = String.format(string, Arrays.copyOf(new Object[]{"<b>" + ((Object) p0().f.getText()) + "</b>"}, 1));
        fs1.e(format, "java.lang.String.format(format, *args)");
        bh.j0(new WeakReference(this), (r18 & 2) != 0 ? null : Integer.valueOf((int) R.string.akt_reset_password_confirm_dialog_header), (r18 & 4) != 0 ? null : null, (r18 & 8) != 0 ? null : format, (r18 & 16) != 0 ? R.string.action_ok : R.string.akt_reset_password_confirm_dialog_accept, (r18 & 32) != 0 ? R.string.cancel : R.string.ca_alert_save_continue_editing, new AKTChangePasswordActivity$showConfirmChangePasswordDialog$1(this), AKTChangePasswordActivity$showConfirmChangePasswordDialog$2.INSTANCE);
    }
}
