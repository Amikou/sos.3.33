package net.safemoon.androidwallet.activity;

import kotlin.jvm.internal.Lambda;

/* compiled from: AKTImportExistingWalletsActivity.kt */
/* loaded from: classes2.dex */
public final class AKTImportExistingWalletsActivity$showImportExistingWalletsSuccessWhenLogin$dialog$1 extends Lambda implements rc1<te4> {
    public final /* synthetic */ AKTImportExistingWalletsActivity this$0;

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public AKTImportExistingWalletsActivity$showImportExistingWalletsSuccessWhenLogin$dialog$1(AKTImportExistingWalletsActivity aKTImportExistingWalletsActivity) {
        super(0);
        this.this$0 = aKTImportExistingWalletsActivity;
    }

    @Override // defpackage.rc1
    public /* bridge */ /* synthetic */ te4 invoke() {
        invoke2();
        return te4.a;
    }

    @Override // defpackage.rc1
    /* renamed from: invoke  reason: avoid collision after fix types in other method */
    public final void invoke2() {
        String v0;
        AKTImportExistingWalletsActivity aKTImportExistingWalletsActivity = this.this$0;
        v0 = aKTImportExistingWalletsActivity.v0();
        fs1.e(v0, "masterMnemonic");
        aKTImportExistingWalletsActivity.P0(v0, false, true);
    }
}
