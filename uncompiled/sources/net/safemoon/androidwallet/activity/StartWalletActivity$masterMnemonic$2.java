package net.safemoon.androidwallet.activity;

import android.content.Intent;
import kotlin.jvm.internal.Lambda;

/* compiled from: StartWalletActivity.kt */
/* loaded from: classes2.dex */
public final class StartWalletActivity$masterMnemonic$2 extends Lambda implements rc1<String> {
    public final /* synthetic */ StartWalletActivity this$0;

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public StartWalletActivity$masterMnemonic$2(StartWalletActivity startWalletActivity) {
        super(0);
        this.this$0 = startWalletActivity;
    }

    @Override // defpackage.rc1
    public final String invoke() {
        String stringExtra;
        Intent intent = this.this$0.getIntent();
        return (intent == null || (stringExtra = intent.getStringExtra("KEY_MASTER_MNEMONIC")) == null) ? "" : stringExtra;
    }
}
