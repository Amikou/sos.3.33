package net.safemoon.androidwallet.activity;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.EditText;
import net.safemoon.androidwallet.R;
import net.safemoon.androidwallet.activity.AKTAnswerQuestionsActivity;

/* compiled from: AKTAnswerQuestionsActivity.kt */
/* loaded from: classes2.dex */
public final class AKTAnswerQuestionsActivity extends AKTBaseLoginFunctions {
    public static final a u0 = new a(null);
    public final sy1 o0 = zy1.a(new AKTAnswerQuestionsActivity$binding$2(this));
    public final sy1 p0 = zy1.a(new AKTAnswerQuestionsActivity$firstQuestion$2(this));
    public final sy1 q0 = zy1.a(new AKTAnswerQuestionsActivity$secondQuestion$2(this));
    public final sy1 r0 = zy1.a(new AKTAnswerQuestionsActivity$aktKs$2(this));
    public final sy1 s0 = zy1.a(new AKTAnswerQuestionsActivity$isForgotPassword$2(this));
    public final sy1 t0 = zy1.a(new AKTAnswerQuestionsActivity$isSwitchAccount$2(this));

    /* compiled from: AKTAnswerQuestionsActivity.kt */
    /* loaded from: classes2.dex */
    public static final class a {
        public a() {
        }

        public /* synthetic */ a(qi0 qi0Var) {
            this();
        }

        public static /* synthetic */ void b(a aVar, Context context, String str, String str2, String str3, boolean z, boolean z2, int i, Object obj) {
            if ((i & 16) != 0) {
                z = false;
            }
            boolean z3 = z;
            if ((i & 32) != 0) {
                z2 = true;
            }
            aVar.a(context, str, str2, str3, z3, z2);
        }

        public final void a(Context context, String str, String str2, String str3, boolean z, boolean z2) {
            fs1.f(context, "context");
            fs1.f(str, "firstQuestion");
            fs1.f(str2, "secondQuestion");
            fs1.f(str3, "aktKs");
            Intent intent = new Intent(context, AKTAnswerQuestionsActivity.class);
            intent.putExtra("FIRST_QUESTION", str);
            intent.putExtra("SECOND_QUESTION", str2);
            intent.putExtra("AKT_KS", str3);
            intent.putExtra("IS_FORGOT_PASSWORD", z);
            intent.putExtra("IS_SWITCH_ACCOUNT", z2);
            context.startActivity(intent);
        }
    }

    /* compiled from: AKTAnswerQuestionsActivity.kt */
    /* loaded from: classes2.dex */
    public static final class b implements TextWatcher {
        public b() {
            AKTAnswerQuestionsActivity.this = r1;
        }

        @Override // android.text.TextWatcher
        public void afterTextChanged(Editable editable) {
            fs1.f(editable, "s");
            AKTAnswerQuestionsActivity.this.J0();
        }

        @Override // android.text.TextWatcher
        public void beforeTextChanged(CharSequence charSequence, int i, int i2, int i3) {
            fs1.f(charSequence, "s");
        }

        @Override // android.text.TextWatcher
        public void onTextChanged(CharSequence charSequence, int i, int i2, int i3) {
            fs1.f(charSequence, "s");
        }
    }

    /* compiled from: AKTAnswerQuestionsActivity.kt */
    /* loaded from: classes2.dex */
    public static final class c implements TextWatcher {
        public c() {
            AKTAnswerQuestionsActivity.this = r1;
        }

        @Override // android.text.TextWatcher
        public void afterTextChanged(Editable editable) {
            fs1.f(editable, "s");
            AKTAnswerQuestionsActivity.this.J0();
        }

        @Override // android.text.TextWatcher
        public void beforeTextChanged(CharSequence charSequence, int i, int i2, int i3) {
            fs1.f(charSequence, "s");
        }

        @Override // android.text.TextWatcher
        public void onTextChanged(CharSequence charSequence, int i, int i2, int i3) {
            fs1.f(charSequence, "s");
        }
    }

    public static final void D0(AKTAnswerQuestionsActivity aKTAnswerQuestionsActivity, boolean z) {
        fs1.f(aKTAnswerQuestionsActivity, "this$0");
        qy4 qy4Var = aKTAnswerQuestionsActivity.l0;
        fs1.e(qy4Var, "safeMoonppp");
        aKTAnswerQuestionsActivity.i0(qy4Var, true);
        bo3.n(aKTAnswerQuestionsActivity, "TWO_FACTOR", Boolean.valueOf(z));
    }

    public static final void G0(AKTAnswerQuestionsActivity aKTAnswerQuestionsActivity, View view) {
        fs1.f(aKTAnswerQuestionsActivity, "this$0");
        aKTAnswerQuestionsActivity.C0();
    }

    public static final void H0(AKTAnswerQuestionsActivity aKTAnswerQuestionsActivity, View view) {
        fs1.f(aKTAnswerQuestionsActivity, "this$0");
        aKTAnswerQuestionsActivity.onBackPressed();
    }

    public static final void I0(AKTAnswerQuestionsActivity aKTAnswerQuestionsActivity, View view) {
        fs1.f(aKTAnswerQuestionsActivity, "this$0");
        pg4.e(aKTAnswerQuestionsActivity);
    }

    public final String A0() {
        return (String) this.p0.getValue();
    }

    public final String B0() {
        return (String) this.q0.getValue();
    }

    /* JADX WARN: Removed duplicated region for block: B:85:0x00a8 A[Catch: Exception -> 0x00fe, TRY_ENTER, TryCatch #0 {Exception -> 0x00fe, blocks: (B:68:0x006c, B:70:0x0072, B:71:0x0087, B:85:0x00a8, B:87:0x00d0, B:88:0x00ed, B:89:0x00f6, B:74:0x0091, B:77:0x0098), top: B:94:0x006c }] */
    /* JADX WARN: Removed duplicated region for block: B:89:0x00f6 A[Catch: Exception -> 0x00fe, TRY_LEAVE, TryCatch #0 {Exception -> 0x00fe, blocks: (B:68:0x006c, B:70:0x0072, B:71:0x0087, B:85:0x00a8, B:87:0x00d0, B:88:0x00ed, B:89:0x00f6, B:74:0x0091, B:77:0x0098), top: B:94:0x006c }] */
    /*
        Code decompiled incorrectly, please refer to instructions dump.
        To view partially-correct code enable 'Show inconsistent code' option in preferences
    */
    public final void C0() {
        /*
            Method dump skipped, instructions count: 262
            To view this dump change 'Code comments level' option to 'DEBUG'
        */
        throw new UnsupportedOperationException("Method not decompiled: net.safemoon.androidwallet.activity.AKTAnswerQuestionsActivity.C0():void");
    }

    public final boolean E0() {
        return ((Boolean) this.s0.getValue()).booleanValue();
    }

    public final boolean F0() {
        return ((Boolean) this.t0.getValue()).booleanValue();
    }

    /* JADX WARN: Removed duplicated region for block: B:78:0x005c  */
    /* JADX WARN: Removed duplicated region for block: B:88:0x006f  */
    /*
        Code decompiled incorrectly, please refer to instructions dump.
        To view partially-correct code enable 'Show inconsistent code' option in preferences
    */
    public final void J0() {
        /*
            r4 = this;
            o6 r0 = r4.z0()
            com.google.android.material.textfield.TextInputLayout r0 = r0.d
            android.widget.EditText r0 = r0.getEditText()
            r1 = 0
            if (r0 != 0) goto Lf
        Ld:
            r0 = r1
            goto L25
        Lf:
            android.text.Editable r0 = r0.getText()
            if (r0 != 0) goto L16
            goto Ld
        L16:
            java.lang.String r0 = r0.toString()
            if (r0 != 0) goto L1d
            goto Ld
        L1d:
            java.lang.CharSequence r0 = kotlin.text.StringsKt__StringsKt.K0(r0)
            java.lang.String r0 = r0.toString()
        L25:
            o6 r2 = r4.z0()
            com.google.android.material.textfield.TextInputLayout r2 = r2.e
            android.widget.EditText r2 = r2.getEditText()
            if (r2 != 0) goto L32
            goto L48
        L32:
            android.text.Editable r2 = r2.getText()
            if (r2 != 0) goto L39
            goto L48
        L39:
            java.lang.String r2 = r2.toString()
            if (r2 != 0) goto L40
            goto L48
        L40:
            java.lang.CharSequence r1 = kotlin.text.StringsKt__StringsKt.K0(r2)
            java.lang.String r1 = r1.toString()
        L48:
            r2 = 1
            r3 = 0
            if (r0 != 0) goto L4e
        L4c:
            r0 = r3
            goto L5a
        L4e:
            int r0 = r0.length()
            if (r0 != 0) goto L56
            r0 = r2
            goto L57
        L56:
            r0 = r3
        L57:
            if (r0 != r2) goto L4c
            r0 = r2
        L5a:
            if (r0 != 0) goto L99
            if (r1 != 0) goto L60
        L5e:
            r0 = r3
            goto L6c
        L60:
            int r0 = r1.length()
            if (r0 != 0) goto L68
            r0 = r2
            goto L69
        L68:
            r0 = r3
        L69:
            if (r0 != r2) goto L5e
            r0 = r2
        L6c:
            if (r0 == 0) goto L6f
            goto L99
        L6f:
            o6 r0 = r4.z0()
            com.google.android.material.button.MaterialButton r0 = r0.b
            r1 = 2131099707(0x7f06003b, float:1.7811775E38)
            int r1 = r4.getColor(r1)
            r0.setTextColor(r1)
            o6 r0 = r4.z0()
            com.google.android.material.button.MaterialButton r0 = r0.b
            r0.setEnabled(r2)
            o6 r0 = r4.z0()
            com.google.android.material.button.MaterialButton r0 = r0.b
            r1 = 2131099676(0x7f06001c, float:1.7811712E38)
            android.content.res.ColorStateList r1 = r4.getColorStateList(r1)
            r0.setBackgroundTintList(r1)
            goto Lc2
        L99:
            o6 r0 = r4.z0()
            com.google.android.material.button.MaterialButton r0 = r0.b
            r1 = 2131100010(0x7f06016a, float:1.781239E38)
            int r1 = r4.getColor(r1)
            r0.setTextColor(r1)
            o6 r0 = r4.z0()
            com.google.android.material.button.MaterialButton r0 = r0.b
            r0.setEnabled(r3)
            o6 r0 = r4.z0()
            com.google.android.material.button.MaterialButton r0 = r0.b
            r1 = 2131099677(0x7f06001d, float:1.7811714E38)
            android.content.res.ColorStateList r1 = r4.getColorStateList(r1)
            r0.setBackgroundTintList(r1)
        Lc2:
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: net.safemoon.androidwallet.activity.AKTAnswerQuestionsActivity.J0():void");
    }

    @Override // net.safemoon.androidwallet.activity.AKTBaseLoginFunctions, com.AKT.anonymouskey.ui.login.AKTServerFunctions, net.safemoon.androidwallet.activity.common.BasicActivity, androidx.fragment.app.FragmentActivity, androidx.activity.ComponentActivity, androidx.core.app.ComponentActivity, android.app.Activity
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        setContentView(z0().b());
        EditText editText = z0().d.getEditText();
        if (editText != null) {
            editText.addTextChangedListener(new b());
        }
        EditText editText2 = z0().e.getEditText();
        if (editText2 != null) {
            editText2.addTextChangedListener(new c());
        }
        J0();
    }

    @Override // net.safemoon.androidwallet.activity.common.BasicActivity, androidx.appcompat.app.AppCompatActivity, android.app.Activity
    public void onPostCreate(Bundle bundle) {
        super.onPostCreate(bundle);
        o6 z0 = z0();
        z0.g.setText(A0());
        z0.h.setText(B0());
        z0.b.setOnClickListener(new View.OnClickListener() { // from class: i
            @Override // android.view.View.OnClickListener
            public final void onClick(View view) {
                AKTAnswerQuestionsActivity.G0(AKTAnswerQuestionsActivity.this, view);
            }
        });
        z0.f.e.setText(R.string.akt_security_questions_screen_header_txt);
        z0.f.b.setVisibility(0);
        z0.f.c.setVisibility(8);
        z0.f.c.setText(getString(R.string.cancel));
        z0.f.b.setOnClickListener(new View.OnClickListener() { // from class: h
            @Override // android.view.View.OnClickListener
            public final void onClick(View view) {
                AKTAnswerQuestionsActivity.H0(AKTAnswerQuestionsActivity.this, view);
            }
        });
        z0.c.setOnClickListener(new View.OnClickListener() { // from class: j
            @Override // android.view.View.OnClickListener
            public final void onClick(View view) {
                AKTAnswerQuestionsActivity.I0(AKTAnswerQuestionsActivity.this, view);
            }
        });
    }

    public final String y0() {
        return (String) this.r0.getValue();
    }

    public final o6 z0() {
        return (o6) this.o0.getValue();
    }
}
