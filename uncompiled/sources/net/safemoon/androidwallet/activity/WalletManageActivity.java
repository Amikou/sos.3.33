package net.safemoon.androidwallet.activity;

import android.content.ClipData;
import android.content.ClipboardManager;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.TextView;
import androidx.activity.result.ActivityResult;
import androidx.appcompat.app.ActionBar;
import androidx.cardview.widget.CardView;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.fragment.app.FragmentManager;
import com.AKT.anonymouskey.ui.login.AKTServerFunctions;
import com.fasterxml.jackson.core.util.MinimalPrettyPrinter;
import defpackage.qm1;
import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.Locale;
import java.util.Objects;
import kotlin.text.StringsKt__StringsKt;
import net.safemoon.androidwallet.R;
import net.safemoon.androidwallet.activity.WalletManageActivity;
import net.safemoon.androidwallet.dialogs.G2FAVerfication;
import net.safemoon.androidwallet.dialogs.ProgressLoading;
import net.safemoon.androidwallet.model.wallets.Wallet;
import net.safemoon.androidwallet.viewmodels.AKTWebSocketHandlingViewModel;

/* compiled from: WalletManageActivity.kt */
/* loaded from: classes2.dex */
public final class WalletManageActivity extends AKTServerFunctions {
    public static final a B0 = new a(null);
    public final w41 A0;
    public final String m0 = "off";
    public final String n0 = "https://bscscan.com/address/";
    public final String o0 = "https://etherscan.io/address/";
    public final String p0 = "https://polygonscan.com/address/";
    public final String q0 = "https://snowtrace.io/address/";
    public final sy1 r0 = zy1.a(new WalletManageActivity$isBioAuth$2(this));
    public final sy1 s0 = zy1.a(new WalletManageActivity$manager$2(this));
    public final sy1 t0 = zy1.a(new WalletManageActivity$wallet$2(this));
    public final sy1 u0 = zy1.a(new WalletManageActivity$binding$2(this));
    public int v0;
    public final sy1 w0;
    public boolean x0;
    public boolean y0;
    public int z0;

    /* compiled from: WalletManageActivity.kt */
    /* loaded from: classes2.dex */
    public static final class a {
        public a() {
        }

        public /* synthetic */ a(qi0 qi0Var) {
            this();
        }

        public final void a(Context context, Wallet wallet2) {
            fs1.f(context, "context");
            fs1.f(wallet2, "wallet");
            Intent intent = new Intent(context, WalletManageActivity.class);
            intent.putExtra("argWallet", wallet2.toString());
            te4 te4Var = te4.a;
            context.startActivity(intent);
        }
    }

    /* compiled from: WalletManageActivity.kt */
    /* loaded from: classes2.dex */
    public static final class b implements G2FAVerfication.b {
        public final /* synthetic */ int b;

        public b(int i) {
            this.b = i;
        }

        @Override // net.safemoon.androidwallet.dialogs.G2FAVerfication.b
        public void a() {
            y54.a("Khang").a("G2FAVerificationCallback onError() called", new Object[0]);
        }

        @Override // net.safemoon.androidwallet.dialogs.G2FAVerfication.b
        public void onSuccess() {
            if (WalletManageActivity.this.i1()) {
                WalletManageActivity.this.S1(this.b);
                return;
            }
            int i = this.b;
            if (i == 3) {
                WalletManageActivity.this.z1();
            } else {
                WalletManageActivity.this.F1(i);
            }
        }
    }

    /* compiled from: WalletManageActivity.kt */
    /* loaded from: classes2.dex */
    public static final class c implements qm1.a {
        public c() {
        }

        @Override // defpackage.qm1.a
        public void a() {
            WalletManageActivity.this.J1(true);
        }

        @Override // defpackage.qm1.a
        public void b(int i) {
        }
    }

    /* compiled from: WalletManageActivity.kt */
    /* loaded from: classes2.dex */
    public static final class d implements qm1.a {
        public d() {
        }

        @Override // defpackage.qm1.a
        public void a() {
            WalletManageActivity.this.M1(true);
        }

        @Override // defpackage.qm1.a
        public void b(int i) {
        }
    }

    /* compiled from: WalletManageActivity.kt */
    /* loaded from: classes2.dex */
    public static final class e implements TextWatcher {
        public final /* synthetic */ d8 a;
        public final /* synthetic */ WalletManageActivity f0;

        public e(d8 d8Var, WalletManageActivity walletManageActivity) {
            this.a = d8Var;
            this.f0 = walletManageActivity;
        }

        @Override // android.text.TextWatcher
        public void afterTextChanged(Editable editable) {
            fs1.f(editable, "s");
            String obj = editable.toString();
            String D = dv3.D(dv3.D(obj, "|", "", false, 4, null), ",", "", false, 4, null);
            if (!fs1.b(obj, D)) {
                EditText editText = this.a.h.getEditText();
                if (editText != null) {
                    editText.setText(D);
                }
                EditText editText2 = this.a.h.getEditText();
                if (editText2 == null) {
                    return;
                }
                editText2.setSelection(D.length());
                return;
            }
            ArrayList<String> a = j60.a.a();
            Objects.requireNonNull(obj, "null cannot be cast to non-null type kotlin.CharSequence");
            if (a.contains(StringsKt__StringsKt.K0(obj).toString())) {
                Wallet e1 = this.f0.e1();
                if ((e1 == null || e1.isPrimaryWallet()) ? false : true) {
                    this.a.h.setError(this.f0.getString(R.string.akt_err_name_cant_be_same_master_wallet));
                    this.a.i.a.setEnabled(false);
                    this.a.i.a.setAlpha(0.5f);
                    return;
                }
            }
            this.a.h.setError(null);
            this.a.i.a.setEnabled(true);
            this.a.i.a.setAlpha(1.0f);
        }

        @Override // android.text.TextWatcher
        public void beforeTextChanged(CharSequence charSequence, int i, int i2, int i3) {
            fs1.f(charSequence, "s");
        }

        @Override // android.text.TextWatcher
        public void onTextChanged(CharSequence charSequence, int i, int i2, int i3) {
            fs1.f(charSequence, "s");
        }
    }

    /* compiled from: WalletManageActivity.kt */
    /* loaded from: classes2.dex */
    public static final class f implements qm1.a {
        public f() {
        }

        @Override // defpackage.qm1.a
        public void a() {
            WalletManageActivity.this.z1();
        }

        @Override // defpackage.qm1.a
        public void b(int i) {
        }
    }

    public WalletManageActivity() {
        fs1.e(registerForActivityResult(new v7(), new r7() { // from class: xm4
            @Override // defpackage.r7
            public final void a(Object obj) {
                WalletManageActivity.O1(WalletManageActivity.this, (ActivityResult) obj);
            }
        }), "registerForActivityResul…heckType)\n        }\n    }");
        this.v0 = 1;
        this.w0 = new fj4(d53.b(AKTWebSocketHandlingViewModel.class), new WalletManageActivity$special$$inlined$viewModels$default$2(this), new WalletManageActivity$special$$inlined$viewModels$default$1(this));
        this.z0 = 1;
        this.A0 = new w41();
    }

    public static final void G1(int i, WalletManageActivity walletManageActivity, View view) {
        fs1.f(walletManageActivity, "this$0");
        if (i == 1) {
            walletManageActivity.I1();
        } else {
            walletManageActivity.L1();
        }
    }

    public static final void H1(int i, WalletManageActivity walletManageActivity, View view) {
        fs1.f(walletManageActivity, "this$0");
        if (i == 1) {
            walletManageActivity.C1();
        } else {
            walletManageActivity.D1();
        }
    }

    public static final void K1(WalletManageActivity walletManageActivity, boolean z) {
        fs1.f(walletManageActivity, "this$0");
        walletManageActivity.W0();
        if (z) {
            walletManageActivity.F1(1);
        }
    }

    public static final void N1(WalletManageActivity walletManageActivity, boolean z) {
        fs1.f(walletManageActivity, "this$0");
        walletManageActivity.W0();
        if (z) {
            walletManageActivity.F1(2);
        }
    }

    public static final void O1(WalletManageActivity walletManageActivity, ActivityResult activityResult) {
        fs1.f(walletManageActivity, "this$0");
        fs1.f(activityResult, "result");
        if (activityResult.b() == -1) {
            walletManageActivity.F1(walletManageActivity.v0);
        }
    }

    public static final void h1(WalletManageActivity walletManageActivity, String str) {
        fs1.f(walletManageActivity, "this$0");
        if (str == null) {
            return;
        }
        qy4 qy4Var = walletManageActivity.l0;
        fs1.e(qy4Var, "safeMoonppp");
        walletManageActivity.X(qy4Var, str);
        walletManageActivity.Q();
    }

    public static final void l1(WalletManageActivity walletManageActivity, View view) {
        fs1.f(walletManageActivity, "this$0");
        walletManageActivity.R0();
    }

    public static final void m1(WalletManageActivity walletManageActivity, View view) {
        fs1.f(walletManageActivity, "this$0");
        if (bo3.d(walletManageActivity, "AUTH_2FA_IS_ENABLE")) {
            G2FAVerfication a2 = G2FAVerfication.C0.a(walletManageActivity.Y0(1), walletManageActivity.i1());
            FragmentManager supportFragmentManager = walletManageActivity.getSupportFragmentManager();
            fs1.e(supportFragmentManager, "supportFragmentManager");
            a2.H(supportFragmentManager);
        } else if (fs1.b(walletManageActivity.W0().k.getTag().toString(), "off")) {
            if (walletManageActivity.i1()) {
                walletManageActivity.A0.b(walletManageActivity, walletManageActivity.b1());
            } else {
                walletManageActivity.F1(1);
            }
        }
    }

    public static final void n1(WalletManageActivity walletManageActivity, View view) {
        fs1.f(walletManageActivity, "this$0");
        if (bo3.d(walletManageActivity, "AUTH_2FA_IS_ENABLE")) {
            G2FAVerfication a2 = G2FAVerfication.C0.a(walletManageActivity.Y0(2), walletManageActivity.i1());
            FragmentManager supportFragmentManager = walletManageActivity.getSupportFragmentManager();
            fs1.e(supportFragmentManager, "supportFragmentManager");
            a2.H(supportFragmentManager);
        } else if (fs1.b(walletManageActivity.W0().p.getTag().toString(), "off")) {
            if (walletManageActivity.i1()) {
                walletManageActivity.A0.b(walletManageActivity, walletManageActivity.c1());
                return;
            }
            walletManageActivity.F1(2);
        }
    }

    public static final void o1(WalletManageActivity walletManageActivity, View view) {
        fs1.f(walletManageActivity, "this$0");
        walletManageActivity.k1(walletManageActivity.W0().l.getText().toString());
    }

    public static final void p1(WalletManageActivity walletManageActivity, View view) {
        fs1.f(walletManageActivity, "this$0");
        walletManageActivity.k1(walletManageActivity.W0().n.getText().toString());
    }

    public static final void q1(WalletManageActivity walletManageActivity, View view) {
        String address;
        String obj;
        fs1.f(walletManageActivity, "this$0");
        String V0 = walletManageActivity.V0();
        Wallet e1 = walletManageActivity.e1();
        String str = null;
        if (e1 != null && (address = e1.getAddress()) != null && (obj = StringsKt__StringsKt.K0(address).toString()) != null) {
            str = obj.toLowerCase(Locale.ROOT);
            fs1.e(str, "(this as java.lang.Strin….toLowerCase(Locale.ROOT)");
        }
        walletManageActivity.y1(fs1.l(V0, str));
    }

    public static final void r1(WalletManageActivity walletManageActivity, View view) {
        String address;
        String obj;
        fs1.f(walletManageActivity, "this$0");
        String X0 = walletManageActivity.X0();
        Wallet e1 = walletManageActivity.e1();
        String str = null;
        if (e1 != null && (address = e1.getAddress()) != null && (obj = StringsKt__StringsKt.K0(address).toString()) != null) {
            str = obj.toLowerCase(Locale.ROOT);
            fs1.e(str, "(this as java.lang.Strin….toLowerCase(Locale.ROOT)");
        }
        walletManageActivity.y1(fs1.l(X0, str));
    }

    public static final void s1(WalletManageActivity walletManageActivity, View view) {
        String address;
        String obj;
        fs1.f(walletManageActivity, "this$0");
        String Z0 = walletManageActivity.Z0();
        Wallet e1 = walletManageActivity.e1();
        String str = null;
        if (e1 != null && (address = e1.getAddress()) != null && (obj = StringsKt__StringsKt.K0(address).toString()) != null) {
            str = obj.toLowerCase(Locale.ROOT);
            fs1.e(str, "(this as java.lang.Strin….toLowerCase(Locale.ROOT)");
        }
        walletManageActivity.y1(fs1.l(Z0, str));
    }

    public static final void t1(WalletManageActivity walletManageActivity, View view) {
        String address;
        String obj;
        fs1.f(walletManageActivity, "this$0");
        String U0 = walletManageActivity.U0();
        Wallet e1 = walletManageActivity.e1();
        String str = null;
        if (e1 != null && (address = e1.getAddress()) != null && (obj = StringsKt__StringsKt.K0(address).toString()) != null) {
            str = obj.toLowerCase(Locale.ROOT);
            fs1.e(str, "(this as java.lang.Strin….toLowerCase(Locale.ROOT)");
        }
        walletManageActivity.y1(fs1.l(U0, str));
    }

    public static final void u1(WalletManageActivity walletManageActivity, View view) {
        fs1.f(walletManageActivity, "this$0");
        walletManageActivity.onBackPressed();
    }

    public static final void v1(d8 d8Var, WalletManageActivity walletManageActivity, View view) {
        Editable text;
        fs1.f(d8Var, "$this_apply");
        fs1.f(walletManageActivity, "this$0");
        EditText editText = d8Var.h.getEditText();
        if (editText == null || (text = editText.getText()) == null) {
            return;
        }
        if (StringsKt__StringsKt.K0(text).length() == 0) {
            d8Var.h.setError(walletManageActivity.getString(R.string.err_required_field));
        } else if (j60.a.a().contains(StringsKt__StringsKt.K0(text).toString())) {
            d8Var.h.setError(walletManageActivity.getString(R.string.akt_err_name_cant_be_same_master_wallet));
        } else {
            ProgressLoading.a aVar = ProgressLoading.y0;
            String string = walletManageActivity.getString(R.string.loading);
            fs1.e(string, "getString(R.string.loading)");
            ProgressLoading a2 = aVar.a(false, string, "");
            walletManageActivity.j0 = a2;
            FragmentManager supportFragmentManager = walletManageActivity.getSupportFragmentManager();
            fs1.e(supportFragmentManager, "supportFragmentManager");
            a2.A(supportFragmentManager);
            walletManageActivity.k0.E(walletManageActivity.e1(), StringsKt__StringsKt.K0(text).toString(), new WalletManageActivity$onPostCreate$1$1$2$1$1(walletManageActivity, text));
        }
    }

    public static final void w1(WalletManageActivity walletManageActivity, View view) {
        fs1.f(walletManageActivity, "this$0");
        if (walletManageActivity.e1() == null) {
            return;
        }
        if (bo3.d(walletManageActivity, "AUTH_2FA_IS_ENABLE")) {
            G2FAVerfication a2 = G2FAVerfication.C0.a(walletManageActivity.Y0(3), walletManageActivity.i1());
            FragmentManager supportFragmentManager = walletManageActivity.getSupportFragmentManager();
            fs1.e(supportFragmentManager, "supportFragmentManager");
            a2.H(supportFragmentManager);
        } else if (walletManageActivity.i1()) {
            walletManageActivity.A0.b(walletManageActivity, walletManageActivity.P1());
        } else {
            walletManageActivity.z1();
        }
    }

    public static final void x1(WalletManageActivity walletManageActivity, View view) {
        fs1.f(walletManageActivity, "this$0");
        walletManageActivity.k1(walletManageActivity.W0().t.getText().toString());
    }

    public final void A1() {
        ProgressLoading progressLoading = this.j0;
        if (progressLoading != null) {
            progressLoading.h();
        }
        if (this.x0) {
            finish();
        } else if (this.y0) {
            onBackPressed();
        } else {
            bh.Y(new WeakReference(this), null, Integer.valueOf((int) R.string.akt_manage_wallets_link_wallet_error_message), 0, null, 24, null);
        }
    }

    public final void B1() {
        Wallet e1 = e1();
        if (e1 != null) {
            this.k0.o(e1);
        }
        Wallet e12 = e1();
        boolean z = false;
        if (e12 != null && e12.isLinked()) {
            z = true;
        }
        if (z) {
            this.x0 = true;
            Wallet e13 = e1();
            fs1.d(e13);
            j1(e13);
            return;
        }
        finish();
    }

    public final void C1() {
        d8 W0 = W0();
        W0.k.setTag(d1());
        W0.k.setVisibility(0);
        W0.e.setVisibility(0);
        W0.f.setVisibility(8);
        W0.m.setVisibility(0);
    }

    public final void D1() {
        d8 W0 = W0();
        W0.p.setTag(d1());
        W0.p.setVisibility(0);
        W0.d.setVisibility(8);
        W0.q.setVisibility(0);
    }

    public void E1(String str) {
        if (str == null) {
            return;
        }
        f1().k(str);
    }

    public final void F1(final int i) {
        bh.F(new WeakReference(this), new View.OnClickListener() { // from class: jn4
            @Override // android.view.View.OnClickListener
            public final void onClick(View view) {
                WalletManageActivity.G1(i, this, view);
            }
        }, new View.OnClickListener() { // from class: in4
            @Override // android.view.View.OnClickListener
            public final void onClick(View view) {
                WalletManageActivity.H1(i, this, view);
            }
        });
    }

    public final void I1() {
        String passPhrase;
        d8 W0 = W0();
        W0.m.setVisibility(8);
        W0.k.setVisibility(8);
        W0.f.setVisibility(0);
        W0.l.setVisibility(0);
        Wallet e1 = e1();
        if (e1 == null || (passPhrase = e1.getPassPhrase()) == null) {
            return;
        }
        String e2 = w.e(this, passPhrase);
        fs1.e(e2, "getDecryptedRecoveryPhra…WalletManageActivity, it)");
        String D = dv3.D(e2, "|", MinimalPrettyPrinter.DEFAULT_ROOT_VALUE_SEPARATOR, false, 4, null);
        Objects.requireNonNull(D, "null cannot be cast to non-null type kotlin.CharSequence");
        W0.l.setText(StringsKt__StringsKt.K0(D).toString());
    }

    public final void J1(final boolean z) {
        new Handler(Looper.getMainLooper()).post(new Runnable() { // from class: gn4
            @Override // java.lang.Runnable
            public final void run() {
                WalletManageActivity.K1(WalletManageActivity.this, z);
            }
        });
    }

    public final void L1() {
        String privateKey;
        d8 W0 = W0();
        W0.q.setVisibility(8);
        W0.p.setVisibility(8);
        W0.d.setVisibility(0);
        W0.n.setVisibility(0);
        Wallet e1 = e1();
        if (e1 == null || (privateKey = e1.getPrivateKey()) == null) {
            return;
        }
        W0.n.setText(w.d(this, privateKey));
    }

    public final void M1(final boolean z) {
        new Handler(Looper.getMainLooper()).post(new Runnable() { // from class: fn4
            @Override // java.lang.Runnable
            public final void run() {
                WalletManageActivity.N1(WalletManageActivity.this, z);
            }
        });
    }

    public final qm1.a P1() {
        return new f();
    }

    @Override // com.AKT.anonymouskey.ui.login.AKTServerFunctions
    public void Q() {
        f1().f();
    }

    public final void Q0() {
        this.k0.p(new WalletManageActivity$checkLinkedAllStatus$1(this));
    }

    public final void Q1() {
        this.k0.p(new WalletManageActivity$updateAllWalletsLinkedStatus$1(this, do3.a.d(this) ? 1 : 0));
    }

    public final void R0() {
        bh.P(new WeakReference(this), (r23 & 2) != 0 ? null : Integer.valueOf((int) R.string.confirm_remove_wallet), (r23 & 4) != 0 ? null : Integer.valueOf((int) R.string.confirm_remove_wallet_description), (r23 & 8) != 0 ? null : null, (r23 & 16) != 0 ? Integer.valueOf((int) R.string.action_ok) : Integer.valueOf((int) R.string.delete), (r23 & 32) != 0 ? Integer.valueOf((int) R.string.cancel) : Integer.valueOf((int) R.string.cancel), (r23 & 64) != 0 ? null : null, (r23 & 128) != 0 ? null : null, new WalletManageActivity$confirmRemoveWallet$1(this), WalletManageActivity$confirmRemoveWallet$2.INSTANCE);
    }

    public final void R1() {
        d8 W0 = W0();
        Wallet e1 = e1();
        if (e1 != null && e1.isPrimaryWallet()) {
            ViewGroup.LayoutParams layoutParams = W0.g.getLayoutParams();
            Objects.requireNonNull(layoutParams, "null cannot be cast to non-null type androidx.constraintlayout.widget.ConstraintLayout.LayoutParams");
            ConstraintLayout.LayoutParams layoutParams2 = (ConstraintLayout.LayoutParams) layoutParams;
            ((ViewGroup.MarginLayoutParams) layoutParams2).width = getResources().getDimensionPixelSize(R.dimen._22sdp);
            ((ViewGroup.MarginLayoutParams) layoutParams2).height = getResources().getDimensionPixelSize(R.dimen._29sdp);
            W0.g.setLayoutParams(layoutParams2);
            if (this.z0 > 1) {
                W0.g.setVisibility(0);
                if (do3.a.d(this)) {
                    W0.g.setImageResource(R.drawable.ic_wallets_link_all);
                    return;
                } else {
                    W0.g.setImageResource(R.drawable.ic_wallets_unlink_all);
                    return;
                }
            }
            W0.g.setVisibility(8);
            return;
        }
        W0.g.setVisibility(0);
        ViewGroup.LayoutParams layoutParams3 = W0.g.getLayoutParams();
        Objects.requireNonNull(layoutParams3, "null cannot be cast to non-null type androidx.constraintlayout.widget.ConstraintLayout.LayoutParams");
        ConstraintLayout.LayoutParams layoutParams4 = (ConstraintLayout.LayoutParams) layoutParams3;
        ((ViewGroup.MarginLayoutParams) layoutParams4).width = getResources().getDimensionPixelSize(R.dimen._22sdp);
        ((ViewGroup.MarginLayoutParams) layoutParams4).height = getResources().getDimensionPixelSize(R.dimen._22sdp);
        W0.g.setLayoutParams(layoutParams4);
        Wallet e12 = e1();
        if (e12 != null && e12.isLinked()) {
            W0.g.setImageResource(R.drawable.ic_wallet_linked);
        } else {
            W0.g.setImageResource(R.drawable.ic_wallet_unlinked);
        }
    }

    public final void S0(Wallet wallet2) {
        bh.w0(new WeakReference(this), R.string.akt_switch_wallet_unlink_all_wallets, R.string.akt_manage_wallets_unlink_all_wallets_content, R.string.action_no, R.string.action_yes, WalletManageActivity$confirmUnlinkAllWallets$1.INSTANCE, new WalletManageActivity$confirmUnlinkAllWallets$2(this, wallet2));
    }

    public final void S1(int i) {
        if (i == 1) {
            this.A0.b(this, b1());
        } else {
            this.A0.b(this, c1());
        }
    }

    public final String T0(String str) {
        String i = bo3.i(this, "U5");
        String Z = AKTServerFunctions.Z(this, ((Object) i) + '>' + str);
        fs1.e(Z, "putBlob(this, newBlob)");
        return Z;
    }

    public final String U0() {
        return this.q0;
    }

    public final String V0() {
        return this.n0;
    }

    public final d8 W0() {
        return (d8) this.u0.getValue();
    }

    @Override // com.AKT.anonymouskey.ui.login.AKTServerFunctions
    public void X(qy4 qy4Var, String str) {
        fs1.f(qy4Var, "safeMoonppp");
        if (W(str)) {
            if (this.x0) {
                finish();
            }
            if (this.y0) {
                onBackPressed();
                return;
            }
            return;
        }
        ProgressLoading progressLoading = this.j0;
        if (progressLoading != null) {
            progressLoading.h();
        }
        String[] v = i2.v(b30.a.v(this, str), "|");
        fs1.e(v, "parts");
        if (!(v.length == 0)) {
            String str2 = v[0];
            fs1.e(str2, "parts[0]");
            if (StringsKt__StringsKt.w0(str2, new String[]{"="}, false, 0, 6, null).size() >= 2) {
                String str3 = v[0];
                fs1.e(str3, "parts[0]");
                String str4 = (String) StringsKt__StringsKt.w0(str3, new String[]{"="}, false, 0, 6, null).get(1);
                Objects.requireNonNull(str4, "null cannot be cast to non-null type java.lang.String");
                String upperCase = str4.toUpperCase(Locale.ROOT);
                fs1.e(upperCase, "(this as java.lang.Strin….toUpperCase(Locale.ROOT)");
                Q();
                if (fs1.b(upperCase, "AKTSERVERERROR")) {
                    A1();
                } else if (fs1.b(upperCase, "PUTBLOB02")) {
                    if (this.x0) {
                        finish();
                    } else if (this.y0) {
                        onBackPressed();
                    } else {
                        Wallet e1 = e1();
                        if (e1 != null) {
                            if (e1.isPrimaryWallet()) {
                                do3 do3Var = do3.a;
                                do3Var.h(this, !do3Var.d(this));
                                Q1();
                            } else {
                                e1.updateLinkedState(!e1.isLinked());
                                this.k0.C(e1(), e1.getLinkedState(), new WalletManageActivity$parseMessage$1$1(this));
                            }
                        }
                        R1();
                    }
                } else {
                    super.X(qy4Var, str);
                }
            }
        }
    }

    public final String X0() {
        return this.o0;
    }

    public final G2FAVerfication.b Y0(int i) {
        return new b(i);
    }

    public final String Z0() {
        return this.p0;
    }

    public final ClipboardManager a1() {
        return (ClipboardManager) this.s0.getValue();
    }

    public final qm1.a b1() {
        return new c();
    }

    public final qm1.a c1() {
        return new d();
    }

    public final String d1() {
        return this.m0;
    }

    public final Wallet e1() {
        return (Wallet) this.t0.getValue();
    }

    public final AKTWebSocketHandlingViewModel f1() {
        return (AKTWebSocketHandlingViewModel) this.w0.getValue();
    }

    public void g1() {
        f1().h().observe(this, new tl2() { // from class: hn4
            @Override // defpackage.tl2
            public final void onChanged(Object obj) {
                WalletManageActivity.h1(WalletManageActivity.this, (String) obj);
            }
        });
    }

    public final boolean i1() {
        return ((Boolean) this.r0.getValue()).booleanValue();
    }

    public final void j1(Wallet wallet2) {
        ProgressLoading.a aVar = ProgressLoading.y0;
        String string = getString(R.string.loading);
        fs1.e(string, "getString(R.string.loading)");
        ProgressLoading a2 = aVar.a(false, string, "");
        this.j0 = a2;
        if (a2 != null) {
            FragmentManager supportFragmentManager = getSupportFragmentManager();
            fs1.e(supportFragmentManager, "supportFragmentManager");
            a2.A(supportFragmentManager);
        }
        this.k0.p(new WalletManageActivity$makeBlobFromWallets$1(wallet2, this, new StringBuilder()));
    }

    public final void k1(String str) {
        if (str.length() > 0) {
            a1().setPrimaryClip(ClipData.newPlainText("label", str));
            e30.Z(this, R.string.copied_to_clipboard);
        }
    }

    @Override // com.AKT.anonymouskey.ui.login.AKTServerFunctions, net.safemoon.androidwallet.activity.common.BasicActivity, androidx.fragment.app.FragmentActivity, androidx.activity.ComponentActivity, androidx.core.app.ComponentActivity, android.app.Activity
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        setContentView(W0().b());
        ActionBar supportActionBar = getSupportActionBar();
        if (supportActionBar != null) {
            supportActionBar.l();
        }
        getWindow().addFlags(Integer.MIN_VALUE);
        getWindow().setStatusBarColor(m70.d(this, 17170445));
        getWindow().setBackgroundDrawableResource(R.drawable.bottom_curve);
        if (!zr.a.booleanValue()) {
            H(true);
            m43.a(this, true);
        }
        f1().l();
        g1();
        this.k0.p(new WalletManageActivity$onCreate$1(this));
    }

    @Override // net.safemoon.androidwallet.activity.common.BasicActivity, androidx.appcompat.app.AppCompatActivity, android.app.Activity
    public void onPostCreate(Bundle bundle) {
        int i;
        String address;
        String obj;
        String lowerCase;
        String address2;
        String obj2;
        String lowerCase2;
        super.onPostCreate(bundle);
        final d8 W0 = W0();
        i74 i74Var = W0.i;
        i74Var.b.setOnClickListener(new View.OnClickListener() { // from class: on4
            @Override // android.view.View.OnClickListener
            public final void onClick(View view) {
                WalletManageActivity.u1(WalletManageActivity.this, view);
            }
        });
        i74Var.c.setText(R.string.manage_wallet_screen_title);
        i74Var.a.setOnClickListener(new View.OnClickListener() { // from class: kn4
            @Override // android.view.View.OnClickListener
            public final void onClick(View view) {
                WalletManageActivity.v1(d8.this, this, view);
            }
        });
        EditText editText = W0.h.getEditText();
        if (editText != null) {
            Wallet e1 = e1();
            editText.setText(e1 == null ? null : e1.getName());
            editText.setSelection(editText.getText().toString().length());
        }
        EditText editText2 = W0.h.getEditText();
        if (editText2 != null) {
            editText2.addTextChangedListener(new e(W0, this));
        }
        R1();
        W0.g.setOnClickListener(new View.OnClickListener() { // from class: cn4
            @Override // android.view.View.OnClickListener
            public final void onClick(View view) {
                WalletManageActivity.w1(WalletManageActivity.this, view);
            }
        });
        TextView textView = W0().t;
        Wallet e12 = e1();
        textView.setText(e12 == null ? null : e12.getAddress());
        W0().s.setOnClickListener(new View.OnClickListener() { // from class: ym4
            @Override // android.view.View.OnClickListener
            public final void onClick(View view) {
                WalletManageActivity.x1(WalletManageActivity.this, view);
            }
        });
        W0().r.setOnClickListener(new View.OnClickListener() { // from class: dn4
            @Override // android.view.View.OnClickListener
            public final void onClick(View view) {
                WalletManageActivity.l1(WalletManageActivity.this, view);
            }
        });
        CardView cardView = W0().b;
        Wallet e13 = e1();
        if ((e13 == null ? null : e13.getPassPhrase()) != null) {
            Wallet e14 = e1();
            String passPhrase = e14 == null ? null : e14.getPassPhrase();
            fs1.d(passPhrase);
            i = e30.m0(passPhrase.length() > 0);
        } else {
            i = 8;
        }
        cardView.setVisibility(i);
        W0().k.setOnClickListener(new View.OnClickListener() { // from class: ln4
            @Override // android.view.View.OnClickListener
            public final void onClick(View view) {
                WalletManageActivity.m1(WalletManageActivity.this, view);
            }
        });
        W0().p.setOnClickListener(new View.OnClickListener() { // from class: zm4
            @Override // android.view.View.OnClickListener
            public final void onClick(View view) {
                WalletManageActivity.n1(WalletManageActivity.this, view);
            }
        });
        W0().j.setOnClickListener(new View.OnClickListener() { // from class: pn4
            @Override // android.view.View.OnClickListener
            public final void onClick(View view) {
                WalletManageActivity.o1(WalletManageActivity.this, view);
            }
        });
        W0().o.setOnClickListener(new View.OnClickListener() { // from class: en4
            @Override // android.view.View.OnClickListener
            public final void onClick(View view) {
                WalletManageActivity.p1(WalletManageActivity.this, view);
            }
        });
        W0().v.setOnClickListener(new View.OnClickListener() { // from class: bn4
            @Override // android.view.View.OnClickListener
            public final void onClick(View view) {
                WalletManageActivity.q1(WalletManageActivity.this, view);
            }
        });
        W0().w.setOnClickListener(new View.OnClickListener() { // from class: mn4
            @Override // android.view.View.OnClickListener
            public final void onClick(View view) {
                WalletManageActivity.r1(WalletManageActivity.this, view);
            }
        });
        W0().x.setOnClickListener(new View.OnClickListener() { // from class: an4
            @Override // android.view.View.OnClickListener
            public final void onClick(View view) {
                WalletManageActivity.s1(WalletManageActivity.this, view);
            }
        });
        W0().u.setOnClickListener(new View.OnClickListener() { // from class: nn4
            @Override // android.view.View.OnClickListener
            public final void onClick(View view) {
                WalletManageActivity.t1(WalletManageActivity.this, view);
            }
        });
        Wallet c2 = e30.c(this);
        if (c2 == null || (address = c2.getAddress()) == null || (obj = StringsKt__StringsKt.K0(address).toString()) == null) {
            lowerCase = null;
        } else {
            lowerCase = obj.toLowerCase(Locale.ROOT);
            fs1.e(lowerCase, "(this as java.lang.Strin….toLowerCase(Locale.ROOT)");
        }
        Wallet e15 = e1();
        if (e15 == null || (address2 = e15.getAddress()) == null || (obj2 = StringsKt__StringsKt.K0(address2).toString()) == null) {
            lowerCase2 = null;
        } else {
            lowerCase2 = obj2.toLowerCase(Locale.ROOT);
            fs1.e(lowerCase2, "(this as java.lang.Strin….toLowerCase(Locale.ROOT)");
        }
        if (dv3.u(lowerCase, lowerCase2, false, 2, null)) {
            W0().c.setVisibility(8);
        } else {
            W0().c.setVisibility(0);
        }
        Wallet e16 = e1();
        if (e16 != null && e16.isPrimaryWallet()) {
            EditText editText3 = W0().h.getEditText();
            if (editText3 != null) {
                Wallet e17 = e1();
                editText3.setText(e17 != null ? e17.displayName() : null);
            }
            EditText editText4 = W0().h.getEditText();
            if (editText4 != null) {
                editText4.setInputType(0);
            }
            W0().c.setVisibility(4);
            W0().i.a.setVisibility(8);
            W0().h.setEndIconMode(0);
            W0().h.setCounterEnabled(false);
            EditText editText5 = W0().h.getEditText();
            if (editText5 != null) {
                editText5.setTextColor(getColor(R.color.t_all_dark));
            }
            ViewGroup.LayoutParams layoutParams = W0().g.getLayoutParams();
            Objects.requireNonNull(layoutParams, "null cannot be cast to non-null type androidx.constraintlayout.widget.ConstraintLayout.LayoutParams");
            ConstraintLayout.LayoutParams layoutParams2 = (ConstraintLayout.LayoutParams) layoutParams;
            layoutParams2.setMarginEnd(getResources().getDimensionPixelSize(R.dimen._15sdp));
            layoutParams2.E = 0.5f;
            W0().g.requestLayout();
        }
        C();
    }

    @Override // androidx.fragment.app.FragmentActivity, android.app.Activity
    public void onResume() {
        super.onResume();
        C1();
        D1();
    }

    public final void y1(String str) {
        startActivity(new Intent("android.intent.action.VIEW", Uri.parse(str)));
    }

    public final void z1() {
        Wallet e1 = e1();
        if (e1 == null) {
            return;
        }
        if (do3.a.d(this) && e1.isPrimaryWallet()) {
            S0(e1);
        } else {
            j1(e1);
        }
    }
}
