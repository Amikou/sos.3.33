package net.safemoon.androidwallet.activity;

import kotlin.jvm.internal.Lambda;

/* compiled from: WalletManageActivity.kt */
/* loaded from: classes2.dex */
public final class WalletManageActivity$isBioAuth$2 extends Lambda implements rc1<Boolean> {
    public final /* synthetic */ WalletManageActivity this$0;

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public WalletManageActivity$isBioAuth$2(WalletManageActivity walletManageActivity) {
        super(0);
        this.this$0 = walletManageActivity;
    }

    /* JADX WARN: Can't rename method to resolve collision */
    @Override // defpackage.rc1
    public final Boolean invoke() {
        return Boolean.valueOf(bo3.e(this.this$0, "TWO_FACTOR", false));
    }
}
