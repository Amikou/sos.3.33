package net.safemoon.androidwallet.activity;

import android.content.DialogInterface;
import java.util.List;
import kotlin.jvm.internal.Lambda;
import net.safemoon.androidwallet.model.wallets.Wallet;
import net.safemoon.androidwallet.viewmodels.MultiWalletViewModel;

/* compiled from: SwitchWalletActivity.kt */
/* loaded from: classes2.dex */
public final class SwitchWalletActivity$touchListener$2$1$1$1 extends Lambda implements tc1<DialogInterface, te4> {
    public final /* synthetic */ List<Wallet> $it;
    public final /* synthetic */ int $position;
    public final /* synthetic */ SwitchWalletActivity this$0;

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public SwitchWalletActivity$touchListener$2$1$1$1(SwitchWalletActivity switchWalletActivity, List<Wallet> list, int i) {
        super(1);
        this.this$0 = switchWalletActivity;
        this.$it = list;
        this.$position = i;
    }

    @Override // defpackage.tc1
    public /* bridge */ /* synthetic */ te4 invoke(DialogInterface dialogInterface) {
        invoke2(dialogInterface);
        return te4.a;
    }

    /* renamed from: invoke  reason: avoid collision after fix types in other method */
    public final void invoke2(DialogInterface dialogInterface) {
        MultiWalletViewModel Q;
        fs1.f(dialogInterface, "$noName_0");
        Q = this.this$0.Q();
        Q.o(this.$it.get(this.$position));
    }
}
