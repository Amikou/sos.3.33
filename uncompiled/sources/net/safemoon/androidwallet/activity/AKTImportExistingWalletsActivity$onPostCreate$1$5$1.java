package net.safemoon.androidwallet.activity;

import android.os.Handler;
import android.os.Looper;
import androidx.fragment.app.FragmentManager;
import kotlin.jvm.internal.Lambda;
import net.safemoon.androidwallet.R;
import net.safemoon.androidwallet.activity.AKTImportExistingWalletsActivity;
import net.safemoon.androidwallet.activity.AKTImportExistingWalletsActivity$onPostCreate$1$5$1;
import net.safemoon.androidwallet.dialogs.ProgressLoading;

/* compiled from: AKTImportExistingWalletsActivity.kt */
/* loaded from: classes2.dex */
public final class AKTImportExistingWalletsActivity$onPostCreate$1$5$1 extends Lambda implements rc1<te4> {
    public final /* synthetic */ AKTImportExistingWalletsActivity this$0;

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public AKTImportExistingWalletsActivity$onPostCreate$1$5$1(AKTImportExistingWalletsActivity aKTImportExistingWalletsActivity) {
        super(0);
        this.this$0 = aKTImportExistingWalletsActivity;
    }

    public static final void b(AKTImportExistingWalletsActivity aKTImportExistingWalletsActivity, boolean z, String str) {
        String v0;
        fs1.f(aKTImportExistingWalletsActivity, "this$0");
        aKTImportExistingWalletsActivity.K0();
        bo3.n(aKTImportExistingWalletsActivity, "TWO_FACTOR", Boolean.valueOf(z));
        aKTImportExistingWalletsActivity.c0("API_KEY", str);
        v0 = aKTImportExistingWalletsActivity.v0();
        fs1.e(v0, "masterMnemonic");
        aKTImportExistingWalletsActivity.P0(v0, false, false);
    }

    @Override // defpackage.rc1
    public /* bridge */ /* synthetic */ te4 invoke() {
        invoke2();
        return te4.a;
    }

    @Override // defpackage.rc1
    /* renamed from: invoke  reason: avoid collision after fix types in other method */
    public final void invoke2() {
        ProgressLoading progressLoading;
        AKTImportExistingWalletsActivity aKTImportExistingWalletsActivity = this.this$0;
        ProgressLoading.a aVar = ProgressLoading.y0;
        String string = aKTImportExistingWalletsActivity.getString(R.string.loading);
        fs1.e(string, "getString(R.string.loading)");
        aKTImportExistingWalletsActivity.j0 = aVar.a(false, string, "");
        progressLoading = this.this$0.j0;
        FragmentManager supportFragmentManager = this.this$0.getSupportFragmentManager();
        fs1.e(supportFragmentManager, "supportFragmentManager");
        progressLoading.A(supportFragmentManager);
        final boolean d = bo3.d(this.this$0, "TWO_FACTOR");
        final String d2 = jv0.d(this.this$0, "API_KEY", "");
        this.this$0.P();
        Handler handler = new Handler(Looper.getMainLooper());
        final AKTImportExistingWalletsActivity aKTImportExistingWalletsActivity2 = this.this$0;
        handler.postDelayed(new Runnable() { // from class: y0
            @Override // java.lang.Runnable
            public final void run() {
                AKTImportExistingWalletsActivity$onPostCreate$1$5$1.b(AKTImportExistingWalletsActivity.this, d, d2);
            }
        }, 500L);
    }
}
