package net.safemoon.androidwallet.activity;

import kotlin.jvm.internal.Lambda;

/* compiled from: AKTSplashActivity.kt */
/* loaded from: classes2.dex */
public final class AKTSplashActivity$binding$2 extends Lambda implements rc1<z7> {
    public final /* synthetic */ AKTSplashActivity this$0;

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public AKTSplashActivity$binding$2(AKTSplashActivity aKTSplashActivity) {
        super(0);
        this.this$0 = aKTSplashActivity;
    }

    @Override // defpackage.rc1
    public final z7 invoke() {
        return z7.c(this.this$0.getLayoutInflater());
    }
}
