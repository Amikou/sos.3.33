package net.safemoon.androidwallet.activity;

import android.content.Intent;
import kotlin.jvm.internal.Lambda;

/* compiled from: AKTLoginActivity.kt */
/* loaded from: classes2.dex */
public final class AKTLoginActivity$canGoBack$2 extends Lambda implements rc1<Boolean> {
    public final /* synthetic */ AKTLoginActivity this$0;

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public AKTLoginActivity$canGoBack$2(AKTLoginActivity aKTLoginActivity) {
        super(0);
        this.this$0 = aKTLoginActivity;
    }

    /* JADX WARN: Can't rename method to resolve collision */
    @Override // defpackage.rc1
    public final Boolean invoke() {
        Intent intent = this.this$0.getIntent();
        return Boolean.valueOf(intent != null ? intent.getBooleanExtra("CAN_GO_BACK", false) : false);
    }
}
