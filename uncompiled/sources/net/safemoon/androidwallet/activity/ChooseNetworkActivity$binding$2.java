package net.safemoon.androidwallet.activity;

import kotlin.jvm.internal.Lambda;

/* compiled from: ChooseNetworkActivity.kt */
/* loaded from: classes2.dex */
public final class ChooseNetworkActivity$binding$2 extends Lambda implements rc1<y6> {
    public final /* synthetic */ ChooseNetworkActivity this$0;

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public ChooseNetworkActivity$binding$2(ChooseNetworkActivity chooseNetworkActivity) {
        super(0);
        this.this$0 = chooseNetworkActivity;
    }

    @Override // defpackage.rc1
    public final y6 invoke() {
        return y6.c(this.this$0.getLayoutInflater());
    }
}
