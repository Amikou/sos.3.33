package net.safemoon.androidwallet.activity;

import android.content.Intent;
import kotlin.jvm.internal.Lambda;

/* compiled from: AKTImportExistingWalletsActivity.kt */
/* loaded from: classes2.dex */
public final class AKTImportExistingWalletsActivity$password$2 extends Lambda implements rc1<String> {
    public final /* synthetic */ AKTImportExistingWalletsActivity this$0;

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public AKTImportExistingWalletsActivity$password$2(AKTImportExistingWalletsActivity aKTImportExistingWalletsActivity) {
        super(0);
        this.this$0 = aKTImportExistingWalletsActivity;
    }

    @Override // defpackage.rc1
    public final String invoke() {
        String stringExtra;
        Intent intent = this.this$0.getIntent();
        return (intent == null || (stringExtra = intent.getStringExtra("KEY_PASSWORD")) == null) ? "" : stringExtra;
    }
}
