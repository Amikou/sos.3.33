package net.safemoon.androidwallet.activity;

import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.os.Handler;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import androidx.appcompat.app.AppCompatActivity;
import androidx.lifecycle.l;
import java.lang.ref.WeakReference;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import net.safemoon.androidwallet.MyApplicationClass;
import net.safemoon.androidwallet.R;
import net.safemoon.androidwallet.activity.ConfirmPassphraseActivity;
import net.safemoon.androidwallet.dialogs.ProgressLoading;
import net.safemoon.androidwallet.viewmodels.CreateWalletViewModel;
import net.safemoon.androidwallet.viewmodels.MultiWalletViewModel;

/* loaded from: classes2.dex */
public class ConfirmPassphraseActivity extends AppCompatActivity {
    public TextView A0;
    public TextView B0;
    public TextView C0;
    public TextView D0;
    public TextView E0;
    public TextView F0;
    public TextView[] G0;
    public TextView[] H0;
    public View[] I0;
    public ImageView[] J0;
    public String[] K0;
    public String[] L0;
    public String[] M0;
    public CreateWalletViewModel N0;
    public MultiWalletViewModel O0;
    public a7 a;
    public String f0;
    public int g0 = 0;
    public Button h0;
    public TextView i0;
    public TextView j0;
    public TextView k0;
    public TextView l0;
    public TextView m0;
    public TextView n0;
    public TextView o0;
    public TextView p0;
    public TextView q0;
    public TextView r0;
    public TextView s0;
    public TextView t0;
    public TextView u0;
    public TextView v0;
    public TextView w0;
    public TextView x0;
    public TextView y0;
    public TextView z0;

    public static boolean B(String[] strArr, String[] strArr2) {
        int length;
        if (strArr == strArr2) {
            return true;
        }
        if (strArr == null || strArr2 == null || (length = strArr.length) != strArr2.length) {
            return false;
        }
        for (int i = 0; i < length; i++) {
            if (!strArr[i].equals(strArr2[i])) {
                return false;
            }
        }
        return true;
    }

    /* JADX INFO: Access modifiers changed from: private */
    public /* synthetic */ void C(int i, List list, View view) {
        if (this.G0[i].getTag().toString().equals("complete")) {
            return;
        }
        this.M0[this.g0] = (String) list.get(i);
        this.H0[this.g0].setText(this.G0[i].getText().toString());
        this.H0[this.g0].setTextColor(m70.d(this, R.color.btn_light_green));
        this.G0[i].setTextColor(m70.d(this, R.color.dark_grey));
        this.G0[i].setTag("complete");
        this.I0[this.g0].setBackgroundColor(m70.d(this, R.color.btn_light_green));
        this.J0[this.g0].setVisibility(0);
        int i2 = this.g0 + 1;
        this.g0 = i2;
        if (i2 == this.K0.length) {
            this.a.c.setText(getResources().getString(R.string.confirm));
            this.a.c.setTag("confirm");
        }
        int i3 = this.g0;
        if (i3 < this.I0.length) {
            this.H0[i3].setTextColor(m70.d(this, R.color.btn_light_green));
            this.I0[this.g0].setBackgroundColor(m70.d(this, R.color.btn_light_green));
        }
    }

    /* JADX INFO: Access modifiers changed from: private */
    public /* synthetic */ void D(View view) {
        if (this.h0.getTag().toString().equals("reset")) {
            K();
            return;
        }
        int i = this.g0;
        String[] strArr = this.K0;
        if (i == strArr.length) {
            if (B(strArr, this.M0)) {
                L();
                return;
            } else {
                M();
                return;
            }
        }
        e30.a0(this, "Please fill all boxes");
    }

    /* JADX INFO: Access modifiers changed from: private */
    public /* synthetic */ void E(View view) {
        finish();
    }

    public static /* synthetic */ void F(DialogInterface dialogInterface) {
    }

    /* JADX INFO: Access modifiers changed from: private */
    public /* synthetic */ te4 G(ProgressLoading progressLoading, Long l) {
        progressLoading.h();
        if (l != null && l.longValue() > 0) {
            MyApplicationClass.c().m0 = true;
            AKTHomeActivity.R0(this);
        } else {
            bh.U(new WeakReference(this), Integer.valueOf((int) R.string.warning_iw_failed_title), Integer.valueOf((int) R.string.warning_iw_failed_msg), R.string.action_ok, null);
        }
        return null;
    }

    /* JADX INFO: Access modifiers changed from: private */
    public /* synthetic */ te4 H(final ProgressLoading progressLoading, String str, String str2, String str3, Boolean bool) {
        if (bool.booleanValue()) {
            progressLoading.h();
            jc0.c(this, Integer.valueOf((int) R.string.warning_title), R.string.warning_black_list_address, true, f50.a);
            return null;
        } else if (str != null) {
            this.O0.m(str2, str, str3, null, true, false, new tc1() { // from class: c50
                @Override // defpackage.tc1
                public final Object invoke(Object obj) {
                    te4 G;
                    G = ConfirmPassphraseActivity.this.G(progressLoading, (Long) obj);
                    return G;
                }
            });
            return null;
        } else {
            progressLoading.h();
            e30.a0(this, getString(R.string.nft_get_data_error));
            return null;
        }
    }

    /* JADX INFO: Access modifiers changed from: private */
    public /* synthetic */ void I(Dialog dialog) {
        dialog.dismiss();
        if (e30.c(this) != null) {
            final ProgressLoading a = ProgressLoading.y0.a(false, getString(R.string.loading), "");
            a.A(getSupportFragmentManager());
            this.N0.b(this.f0, CreateWalletViewModel.KEY_TYPE.PASSPHRASE, new md1() { // from class: d50
                @Override // defpackage.md1
                public final Object invoke(Object obj, Object obj2, Object obj3, Object obj4) {
                    te4 H;
                    H = ConfirmPassphraseActivity.this.H(a, (String) obj, (String) obj2, (String) obj3, (Boolean) obj4);
                    return H;
                }
            });
            return;
        }
        SetPasswordActivity.D(this, this.f0, CreateWalletViewModel.KEY_TYPE.PASSPHRASE);
        finish();
    }

    /* JADX INFO: Access modifiers changed from: private */
    public /* synthetic */ void J(DialogInterface dialogInterface, int i) {
        K();
        dialogInterface.dismiss();
    }

    @SuppressLint({"SetTextI18n"})
    public final void K() {
        this.g0 = 0;
        this.M0 = new String[]{"", "", "", "", "", "", "", "", "", "", "", ""};
        for (int i = 0; i < this.G0.length; i++) {
            this.H0[i].setText("");
            this.H0[i].setTextColor(m70.d(this, R.color.white));
            this.G0[i].setTextColor(m70.d(this, R.color.white));
            this.G0[i].setTag("incomplete");
            this.I0[i].setBackgroundColor(m70.d(this, R.color.white));
            this.J0[i].setVisibility(8);
        }
        this.I0[0].setBackgroundColor(m70.d(this, R.color.btn_light_green));
        this.H0[0].setTextColor(m70.d(this, R.color.btn_light_green));
        this.a.c.setText(getResources().getString(R.string.reset));
        this.a.c.setTag("reset");
    }

    public void L() {
        final Dialog dialog = new Dialog(this);
        dialog.requestWindowFeature(1);
        dialog.setCancelable(false);
        dialog.setContentView(R.layout.dialog_success);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(0));
        dialog.show();
        new Handler().postDelayed(new Runnable() { // from class: j50
            @Override // java.lang.Runnable
            public final void run() {
                ConfirmPassphraseActivity.this.I(dialog);
            }
        }, 1500L);
    }

    public final void M() {
        new AlertDialog.Builder(this).setTitle(getString(R.string.title_invalid_passphrase)).setMessage(getString(R.string.message_invalid_passphrase)).setPositiveButton("Ok", new DialogInterface.OnClickListener() { // from class: e50
            @Override // android.content.DialogInterface.OnClickListener
            public final void onClick(DialogInterface dialogInterface, int i) {
                ConfirmPassphraseActivity.this.J(dialogInterface, i);
            }
        }).show();
    }

    @Override // androidx.fragment.app.FragmentActivity, androidx.activity.ComponentActivity, androidx.core.app.ComponentActivity, android.app.Activity
    @SuppressLint({"SetTextI18n"})
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        a7 c = a7.c(getLayoutInflater());
        this.a = c;
        setContentView(c.b());
        this.N0 = (CreateWalletViewModel) new l(this).a(CreateWalletViewModel.class);
        this.O0 = (MultiWalletViewModel) new l(this).a(MultiWalletViewModel.class);
        if (!zr.a.booleanValue()) {
            getWindow().setFlags(8192, 8192);
        }
        if (getSupportActionBar() != null) {
            getSupportActionBar().l();
            getWindow().addFlags(Integer.MIN_VALUE);
            getWindow().setStatusBarColor(m70.d(this, 17170445));
            getWindow().setBackgroundDrawableResource(R.drawable.ic_bg);
        }
        this.h0 = (Button) findViewById(R.id.btnConfirm);
        this.i0 = (TextView) findViewById(R.id.word1);
        this.j0 = (TextView) findViewById(R.id.word2);
        this.k0 = (TextView) findViewById(R.id.word3);
        this.l0 = (TextView) findViewById(R.id.word4);
        this.m0 = (TextView) findViewById(R.id.word5);
        this.n0 = (TextView) findViewById(R.id.word6);
        this.o0 = (TextView) findViewById(R.id.word7);
        this.p0 = (TextView) findViewById(R.id.word8);
        this.q0 = (TextView) findViewById(R.id.word9);
        this.r0 = (TextView) findViewById(R.id.word10);
        this.s0 = (TextView) findViewById(R.id.word11);
        this.t0 = (TextView) findViewById(R.id.word12);
        this.u0 = (TextView) findViewById(R.id.sepratedword1);
        this.v0 = (TextView) findViewById(R.id.sepratedword2);
        this.w0 = (TextView) findViewById(R.id.sepratedword3);
        this.x0 = (TextView) findViewById(R.id.sepratedword4);
        this.y0 = (TextView) findViewById(R.id.sepratedword5);
        this.z0 = (TextView) findViewById(R.id.sepratedword6);
        this.A0 = (TextView) findViewById(R.id.sepratedword7);
        this.B0 = (TextView) findViewById(R.id.sepratedword8);
        this.C0 = (TextView) findViewById(R.id.sepratedword9);
        this.D0 = (TextView) findViewById(R.id.sepratedword10);
        this.E0 = (TextView) findViewById(R.id.sepratedword11);
        this.F0 = (TextView) findViewById(R.id.sepratedword12);
        String stringExtra = getIntent().getStringExtra("mnemonic");
        this.f0 = stringExtra;
        this.M0 = new String[]{"", "", "", "", "", "", "", "", "", "", "", ""};
        this.K0 = stringExtra.split("\\s+");
        String[] split = this.f0.split("\\s+");
        this.L0 = split;
        final List asList = Arrays.asList(split);
        Collections.shuffle(asList);
        this.H0 = new TextView[]{this.i0, this.j0, this.k0, this.l0, this.m0, this.n0, this.o0, this.p0, this.q0, this.r0, this.s0, this.t0};
        this.G0 = new TextView[]{this.u0, this.v0, this.w0, this.x0, this.y0, this.z0, this.A0, this.B0, this.C0, this.D0, this.E0, this.F0};
        a7 a7Var = this.a;
        this.I0 = new View[]{a7Var.p, a7Var.t, a7Var.u, a7Var.v, a7Var.w, a7Var.x, a7Var.y, a7Var.z, a7Var.A, a7Var.q, a7Var.r, a7Var.s};
        this.J0 = new ImageView[]{a7Var.d, a7Var.h, a7Var.i, a7Var.j, a7Var.k, a7Var.l, a7Var.m, a7Var.n, a7Var.o, a7Var.e, a7Var.f, a7Var.g};
        final int i = 0;
        while (true) {
            TextView[] textViewArr = this.G0;
            if (i < textViewArr.length) {
                textViewArr[i].setText((CharSequence) asList.get(i));
                this.G0[i].setTag("incomplete");
                this.G0[i].setOnClickListener(new View.OnClickListener() { // from class: i50
                    @Override // android.view.View.OnClickListener
                    public final void onClick(View view) {
                        ConfirmPassphraseActivity.this.C(i, asList, view);
                    }
                });
                i++;
            } else {
                this.I0[0].setBackgroundColor(m70.d(this, R.color.btn_light_green));
                this.H0[0].setTextColor(m70.d(this, R.color.btn_light_green));
                this.h0.setOnClickListener(new View.OnClickListener() { // from class: h50
                    @Override // android.view.View.OnClickListener
                    public final void onClick(View view) {
                        ConfirmPassphraseActivity.this.D(view);
                    }
                });
                this.a.b.setOnClickListener(new View.OnClickListener() { // from class: g50
                    @Override // android.view.View.OnClickListener
                    public final void onClick(View view) {
                        ConfirmPassphraseActivity.this.E(view);
                    }
                });
                return;
            }
        }
    }

    @Override // androidx.appcompat.app.AppCompatActivity, android.app.Activity
    public void onPostCreate(Bundle bundle) {
        super.onPostCreate(bundle);
    }
}
