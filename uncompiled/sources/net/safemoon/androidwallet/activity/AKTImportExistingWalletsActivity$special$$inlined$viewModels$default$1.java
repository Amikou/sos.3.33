package net.safemoon.androidwallet.activity;

import androidx.activity.ComponentActivity;
import androidx.lifecycle.l;
import kotlin.jvm.internal.Lambda;

/* compiled from: ActivityViewModelLazy.kt */
/* loaded from: classes2.dex */
public final class AKTImportExistingWalletsActivity$special$$inlined$viewModels$default$1 extends Lambda implements rc1<l.b> {
    public final /* synthetic */ ComponentActivity $this_viewModels;

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public AKTImportExistingWalletsActivity$special$$inlined$viewModels$default$1(ComponentActivity componentActivity) {
        super(0);
        this.$this_viewModels = componentActivity;
    }

    /* JADX WARN: Can't rename method to resolve collision */
    @Override // defpackage.rc1
    public final l.b invoke() {
        l.b defaultViewModelProviderFactory = this.$this_viewModels.getDefaultViewModelProviderFactory();
        fs1.c(defaultViewModelProviderFactory, "defaultViewModelProviderFactory");
        return defaultViewModelProviderFactory;
    }
}
