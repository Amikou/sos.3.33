package net.safemoon.androidwallet.activity;

import java.util.List;
import kotlin.jvm.internal.Lambda;

/* compiled from: StartWalletActivity.kt */
/* loaded from: classes2.dex */
public final class StartWalletActivity$addWalletToDatabase$2 extends Lambda implements tc1<Long, te4> {
    public final /* synthetic */ StartWalletActivity this$0;

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public StartWalletActivity$addWalletToDatabase$2(StartWalletActivity startWalletActivity) {
        super(1);
        this.this$0 = startWalletActivity;
    }

    @Override // defpackage.tc1
    public /* bridge */ /* synthetic */ te4 invoke(Long l) {
        invoke2(l);
        return te4.a;
    }

    /* renamed from: invoke  reason: avoid collision after fix types in other method */
    public final void invoke2(Long l) {
        int i;
        int i2;
        List list;
        List list2;
        int i3;
        StartWalletActivity startWalletActivity = this.this$0;
        i = startWalletActivity.v0;
        startWalletActivity.v0 = i + 1;
        i2 = this.this$0.v0;
        list = this.this$0.u0;
        if (i2 >= list.size()) {
            this.this$0.D0();
            return;
        }
        StartWalletActivity startWalletActivity2 = this.this$0;
        list2 = startWalletActivity2.u0;
        i3 = this.this$0.v0;
        startWalletActivity2.B0((String) list2.get(i3));
    }
}
