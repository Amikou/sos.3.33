package net.safemoon.androidwallet.activity;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.util.DisplayMetrics;
import android.util.SparseArray;
import android.view.SurfaceHolder;
import android.view.SurfaceView;
import android.view.View;
import android.widget.ImageView;
import com.google.android.gms.vision.barcode.Barcode;
import defpackage.gv;
import defpackage.hm;
import defpackage.rm0;
import defpackage.yb1;
import java.io.IOException;
import net.safemoon.androidwallet.R;
import net.safemoon.androidwallet.activity.ScannedCodeActivity;
import net.safemoon.androidwallet.activity.common.BasicActivity;
import net.safemoon.androidwallet.utils.CameraPermissionLauncher;

/* loaded from: classes2.dex */
public class ScannedCodeActivity extends BasicActivity {
    public SurfaceView j0;
    public hm k0;
    public gv l0;
    public String m0 = "";

    /* loaded from: classes2.dex */
    public class a extends wc0<Bitmap> {
        public a() {
        }

        @Override // defpackage.i34
        /* renamed from: d */
        public void j(Bitmap bitmap, hb4<? super Bitmap> hb4Var) {
            SparseArray<Barcode> a = ScannedCodeActivity.this.k0.a(new yb1.a().b(bitmap).a());
            if (a.size() == 0) {
                ScannedCodeActivity.this.S();
                return;
            }
            ScannedCodeActivity.this.m0 = a.valueAt(0).g0;
            Intent intent = new Intent();
            intent.putExtra("result", ScannedCodeActivity.this.m0);
            ScannedCodeActivity.this.setResult(-1, intent);
            ScannedCodeActivity.this.finish();
        }

        @Override // defpackage.i34
        public void m(Drawable drawable) {
        }
    }

    /* loaded from: classes2.dex */
    public class b extends e {
        public b() {
            super(ScannedCodeActivity.this);
        }

        @Override // net.safemoon.androidwallet.activity.ScannedCodeActivity.e, android.view.SurfaceHolder.Callback
        public void surfaceDestroyed(SurfaceHolder surfaceHolder) {
            super.surfaceDestroyed(surfaceHolder);
            ScannedCodeActivity.this.l0.c();
        }
    }

    /* loaded from: classes2.dex */
    public class c extends e {
        public c() {
            super(ScannedCodeActivity.this);
        }

        @Override // net.safemoon.androidwallet.activity.ScannedCodeActivity.e, android.view.SurfaceHolder.Callback
        public void surfaceCreated(SurfaceHolder surfaceHolder) {
            try {
                if (m70.a(ScannedCodeActivity.this, "android.permission.CAMERA") == 0) {
                    ScannedCodeActivity.this.l0.b(ScannedCodeActivity.this.j0.getHolder());
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        @Override // net.safemoon.androidwallet.activity.ScannedCodeActivity.e, android.view.SurfaceHolder.Callback
        public void surfaceDestroyed(SurfaceHolder surfaceHolder) {
            ScannedCodeActivity.this.l0.c();
        }
    }

    /* loaded from: classes2.dex */
    public class d implements rm0.b<Barcode> {
        public d() {
        }

        @Override // defpackage.rm0.b
        public void a() {
        }

        @Override // defpackage.rm0.b
        public void b(rm0.a<Barcode> aVar) {
            SparseArray<Barcode> a = aVar.a();
            if (a.size() != 0) {
                ScannedCodeActivity.this.m0 = a.valueAt(0).g0;
                Intent intent = new Intent();
                intent.putExtra("result", ScannedCodeActivity.this.m0);
                ScannedCodeActivity.this.setResult(-1, intent);
                ScannedCodeActivity.this.finish();
            }
        }
    }

    /* loaded from: classes2.dex */
    public class e implements SurfaceHolder.Callback {
        public e(ScannedCodeActivity scannedCodeActivity) {
        }

        @Override // android.view.SurfaceHolder.Callback
        public void surfaceChanged(SurfaceHolder surfaceHolder, int i, int i2, int i3) {
        }

        @Override // android.view.SurfaceHolder.Callback
        public void surfaceCreated(SurfaceHolder surfaceHolder) {
        }

        @Override // android.view.SurfaceHolder.Callback
        public void surfaceDestroyed(SurfaceHolder surfaceHolder) {
        }
    }

    /* JADX INFO: Access modifiers changed from: private */
    public /* synthetic */ Void V() {
        U();
        return null;
    }

    /* JADX INFO: Access modifiers changed from: private */
    public /* synthetic */ void W(View view) {
        finish();
    }

    /* JADX INFO: Access modifiers changed from: private */
    public /* synthetic */ void X(View view) {
        a0();
    }

    /* JADX INFO: Access modifiers changed from: private */
    public /* synthetic */ te4 Y(Intent intent) {
        pg4.b(this, Boolean.FALSE);
        if (intent.getData() != null) {
            com.bumptech.glide.a.w(this).i().N0(intent.getData()).D0(new a());
            return null;
        }
        return null;
    }

    /* JADX INFO: Access modifiers changed from: private */
    public /* synthetic */ te4 Z(Intent intent) {
        D().b(new tc1() { // from class: tc3
            @Override // defpackage.tc1
            public final Object invoke(Object obj) {
                te4 Y;
                Y = ScannedCodeActivity.this.Y((Intent) obj);
                return Y;
            }
        }).a(intent);
        return null;
    }

    public final void S() {
        e30.Z(this, R.string.scanned_code_invalid_qr_code);
    }

    public final void T() {
        View findViewById = findViewById(R.id.main_layout);
        new CameraPermissionLauncher(this, findViewById).c(findViewById, new rc1() { // from class: rc3
            @Override // defpackage.rc1
            public final Object invoke() {
                Void V;
                V = ScannedCodeActivity.this.V();
                return V;
            }
        });
    }

    @SuppressLint({"MissingPermission"})
    public final void U() {
        this.k0 = new hm.a(this).b(0).a();
        DisplayMetrics displayMetrics = new DisplayMetrics();
        getWindowManager().getDefaultDisplay().getMetrics(displayMetrics);
        this.l0 = new gv.a(this, this.k0).c(displayMetrics.widthPixels, displayMetrics.heightPixels).b(true).a();
        if (this.j0.getHolder().getSurface().isValid()) {
            try {
                if (m70.a(this, "android.permission.CAMERA") == 0) {
                    this.l0.b(this.j0.getHolder());
                }
                this.j0.getHolder().addCallback(new b());
            } catch (IOException e2) {
                e2.printStackTrace();
            }
        } else {
            this.j0.getHolder().addCallback(new c());
        }
        this.k0.e(new d());
    }

    public final void a0() {
        pg4.b(this, Boolean.TRUE);
        View findViewById = findViewById(R.id.main_layout);
        final Intent intent = new Intent("android.intent.action.GET_CONTENT");
        intent.setType("image/*");
        E().c(findViewById, new rc1() { // from class: sc3
            @Override // defpackage.rc1
            public final Object invoke() {
                te4 Z;
                Z = ScannedCodeActivity.this.Z(intent);
                return Z;
            }
        });
    }

    public final void initViews() {
        this.j0 = (SurfaceView) findViewById(R.id.surfaceView);
        ((ImageView) findViewById(R.id.iv_back)).setOnClickListener(new View.OnClickListener() { // from class: uc3
            @Override // android.view.View.OnClickListener
            public final void onClick(View view) {
                ScannedCodeActivity.this.W(view);
            }
        });
        ((ImageView) findViewById(R.id.iv_gallery)).setOnClickListener(new View.OnClickListener() { // from class: vc3
            @Override // android.view.View.OnClickListener
            public final void onClick(View view) {
                ScannedCodeActivity.this.X(view);
            }
        });
    }

    @Override // net.safemoon.androidwallet.activity.common.BasicActivity, androidx.fragment.app.FragmentActivity, androidx.activity.ComponentActivity, androidx.core.app.ComponentActivity, android.app.Activity
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        setContentView(R.layout.activity_scanned_code);
        if (getSupportActionBar() != null) {
            getSupportActionBar().l();
        }
        getWindow().addFlags(Integer.MIN_VALUE);
        getWindow().setStatusBarColor(m70.d(this, 17170445));
        getWindow().setBackgroundDrawableResource(R.drawable.bottom_curve);
        initViews();
        T();
    }

    @Override // androidx.fragment.app.FragmentActivity, android.app.Activity
    public void onPause() {
        super.onPause();
        gv gvVar = this.l0;
        if (gvVar != null) {
            gvVar.a();
        }
    }

    @Override // net.safemoon.androidwallet.activity.common.BasicActivity, androidx.appcompat.app.AppCompatActivity, android.app.Activity
    public void onPostCreate(Bundle bundle) {
        super.onPostCreate(bundle);
    }

    @Override // androidx.fragment.app.FragmentActivity, android.app.Activity
    public void onResume() {
        super.onResume();
        pg4.b(this, Boolean.FALSE);
    }
}
