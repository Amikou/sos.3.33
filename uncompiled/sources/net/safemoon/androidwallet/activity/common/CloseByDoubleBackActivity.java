package net.safemoon.androidwallet.activity.common;

import android.view.ViewGroup;
import androidx.media3.common.PlaybackException;
import com.AKT.anonymouskey.ui.login.AKTServerFunctions;
import com.google.android.material.snackbar.Snackbar;
import net.safemoon.androidwallet.R;

/* compiled from: CloseByDoubleBackActivity.kt */
/* loaded from: classes2.dex */
public abstract class CloseByDoubleBackActivity extends AKTServerFunctions {
    public long m0;

    /* compiled from: CloseByDoubleBackActivity.kt */
    /* loaded from: classes2.dex */
    public static final class a {
        public a() {
        }

        public /* synthetic */ a(qi0 qi0Var) {
            this();
        }
    }

    static {
        new a(null);
    }

    public abstract ViewGroup g0();

    @Override // androidx.activity.ComponentActivity, android.app.Activity
    public void onBackPressed() {
        if (this.m0 + ((long) PlaybackException.ERROR_CODE_IO_UNSPECIFIED) > System.currentTimeMillis()) {
            super.onBackPressed();
        } else if (g0() != null) {
            ViewGroup g0 = g0();
            fs1.d(g0);
            Snackbar.a0(g0, R.string.press_again_to_exit, 0).Q();
        }
        this.m0 = System.currentTimeMillis();
    }
}
