package net.safemoon.androidwallet.activity.common;

import android.app.Activity;
import android.app.Application;
import android.content.Context;
import android.content.Intent;
import android.graphics.Rect;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import android.widget.PopupWindow;
import androidx.appcompat.app.AppCompatActivity;
import androidx.lifecycle.Lifecycle;
import androidx.lifecycle.h;
import androidx.lifecycle.i;
import com.yariksoffice.lingver.Lingver;
import java.lang.ref.WeakReference;
import java.util.HashMap;
import java.util.List;
import java.util.Objects;
import kotlin.text.StringsKt__StringsKt;
import net.safemoon.androidwallet.MyApplicationClass;
import net.safemoon.androidwallet.R;
import net.safemoon.androidwallet.activity.AKTLoginActivity;
import net.safemoon.androidwallet.activity.common.BasicActivity;
import net.safemoon.androidwallet.receivers.ConnectionLiveData;
import net.safemoon.androidwallet.utils.StoragePermissionLauncher;

/* compiled from: BasicActivity.kt */
/* loaded from: classes2.dex */
public class BasicActivity extends AppCompatActivity implements qz1, p43 {
    public StoragePermissionLauncher a = new StoragePermissionLauncher(this);
    public hn2 f0 = new hn2(this);
    public boolean g0;
    public PopupWindow h0;
    public boolean i0;

    /* compiled from: BasicActivity.kt */
    /* loaded from: classes2.dex */
    public static final class a implements ViewTreeObserver.OnGlobalLayoutListener {
        public boolean a;
        public final int f0 = 100;
        public final int g0;
        public final Rect h0;
        public final /* synthetic */ View i0;
        public final /* synthetic */ om2 j0;

        public a(View view, om2 om2Var) {
            this.i0 = view;
            this.j0 = om2Var;
            this.g0 = 100 + (Build.VERSION.SDK_INT >= 21 ? 48 : 0);
            this.h0 = new Rect();
        }

        @Override // android.view.ViewTreeObserver.OnGlobalLayoutListener
        public void onGlobalLayout() {
            int applyDimension = (int) TypedValue.applyDimension(1, this.g0, this.i0.getResources().getDisplayMetrics());
            this.i0.getWindowVisibleDisplayFrame(this.h0);
            int height = this.i0.getRootView().getHeight();
            Rect rect = this.h0;
            boolean z = height - (rect.bottom - rect.top) >= applyDimension;
            if (z == this.a) {
                return;
            }
            this.a = z;
            this.j0.i(z);
        }
    }

    public static final void B(BasicActivity basicActivity) {
        fs1.f(basicActivity, "this$0");
        PopupWindow popupWindow = basicActivity.h0;
        if (popupWindow != null) {
            fs1.d(popupWindow);
            if (!popupWindow.isShowing() || basicActivity.isFinishing() || basicActivity.isDestroyed()) {
                return;
            }
            PopupWindow popupWindow2 = basicActivity.h0;
            fs1.d(popupWindow2);
            popupWindow2.dismiss();
        }
    }

    public static final void F(BasicActivity basicActivity, Boolean bool) {
        fs1.f(basicActivity, "this$0");
        if (bool == null) {
            return;
        }
        bool.booleanValue();
        fs1.e(bool, "isConnected");
        if (bool.booleanValue()) {
            basicActivity.A();
        } else {
            basicActivity.y();
        }
    }

    public static final void z(BasicActivity basicActivity) {
        fs1.f(basicActivity, "this$0");
        if (basicActivity.h0 == null) {
            basicActivity.h0 = new PopupWindow(LayoutInflater.from(basicActivity).inflate(R.layout.popup_network_change_receiver, (ViewGroup) null, false), -1, -2);
        }
        View findViewById = basicActivity.findViewById(R.id.popup_parent_for_alert);
        if (findViewById == null || basicActivity.isFinishing() || basicActivity.isDestroyed()) {
            return;
        }
        PopupWindow popupWindow = basicActivity.h0;
        fs1.d(popupWindow);
        popupWindow.showAtLocation(findViewById, 0, 0, 0);
    }

    public final void A() {
        new Handler(Looper.getMainLooper()).post(new Runnable() { // from class: go
            @Override // java.lang.Runnable
            public final void run() {
                BasicActivity.B(BasicActivity.this);
            }
        });
    }

    public final void C() {
        String simpleName = getClass().getSimpleName();
        HashMap<String, Boolean> hashMap = MyApplicationClass.c().k0;
        fs1.e(hashMap, "get().bioEnabledForTheActivityMap");
        hashMap.put(simpleName, Boolean.TRUE);
    }

    public final hn2 D() {
        return this.f0;
    }

    public final StoragePermissionLauncher E() {
        return this.a;
    }

    public final Intent G() {
        Intent intent = new Intent(this, AKTLoginActivity.class);
        intent.putExtra("bundle.KEY_SOURCE", "bundle.SOURCE_APP_FOR_RESULT");
        return intent;
    }

    public final void H(boolean z) {
        this.g0 = z;
    }

    public final void I(om2 om2Var) {
        fs1.f(om2Var, "onKeyboardVisibilityListener");
        View findViewById = findViewById(16908290);
        Objects.requireNonNull(findViewById, "null cannot be cast to non-null type android.view.ViewGroup");
        View childAt = ((ViewGroup) findViewById).getChildAt(0);
        childAt.getViewTreeObserver().addOnGlobalLayoutListener(new a(childAt, om2Var));
    }

    public final void J(boolean z) {
        Integer valueOf = Integer.valueOf((int) R.string.akt_connection_error_message);
        if (!z) {
            bh.Y(new WeakReference(this), null, valueOf, R.string.action_ok, null, 18, null);
        } else {
            bh.V(new WeakReference(this), null, valueOf, R.string.action_ok, null, 18, null);
        }
    }

    @Override // androidx.appcompat.app.AppCompatActivity, android.app.Activity, android.view.ContextThemeWrapper, android.content.ContextWrapper
    public void attachBaseContext(Context context) {
        fs1.f(context, "newBase");
        super.attachBaseContext(new mb2(context).e(context, do3.a.a(context)));
    }

    @Override // defpackage.p43
    public void o(Activity activity, boolean z) {
        fs1.f(activity, "activity");
        if (this.g0) {
            return;
        }
        if (z) {
            getWindow().addFlags(8192);
        } else {
            getWindow().clearFlags(8192);
        }
    }

    @h(Lifecycle.Event.ON_START)
    public final void onAppStart() {
        Boolean bool = MyApplicationClass.c().k0.get(getClass().getSimpleName());
        if (bool == null) {
            bool = Boolean.FALSE;
        }
        boolean booleanValue = bool.booleanValue();
        boolean d = pg4.d(this);
        boolean e = bo3.e(this, "TWO_FACTOR", false);
        long A = i2.A() - (bo3.c(this, "TIMENOW") ? bo3.g(this, "TIMENOW") : 0L);
        Long l = s.d;
        fs1.e(l, "loginTimeout");
        boolean z = A > l.longValue();
        if (booleanValue) {
            Application application = getApplication();
            Objects.requireNonNull(application, "null cannot be cast to non-null type net.safemoon.androidwallet.MyApplicationClass");
            if (((MyApplicationClass) application).f0 || !this.i0) {
                return;
            }
            if ((d && e) || z) {
                w();
            }
        }
    }

    @Override // androidx.fragment.app.FragmentActivity, androidx.activity.ComponentActivity, androidx.core.app.ComponentActivity, android.app.Activity
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        kc1.a(this);
        i.h().getLifecycle().a(this);
        do3 do3Var = do3.a;
        Context applicationContext = getApplicationContext();
        fs1.e(applicationContext, "applicationContext");
        String a2 = do3Var.a(applicationContext);
        if (StringsKt__StringsKt.M(a2, "-r", false, 2, null)) {
            List w0 = StringsKt__StringsKt.w0(a2, new String[]{"-r"}, false, 0, 6, null);
            Lingver b = Lingver.f.b();
            Context applicationContext2 = getApplicationContext();
            fs1.e(applicationContext2, "applicationContext");
            Lingver.m(b, applicationContext2, (String) w0.get(0), (String) w0.get(1), null, 8, null);
            return;
        }
        Lingver b2 = Lingver.f.b();
        Context applicationContext3 = getApplicationContext();
        fs1.e(applicationContext3, "applicationContext");
        Lingver.m(b2, applicationContext3, a2, null, null, 12, null);
    }

    @Override // androidx.appcompat.app.AppCompatActivity, android.app.Activity
    public void onPostCreate(Bundle bundle) {
        super.onPostCreate(bundle);
        new ConnectionLiveData(this).observe(this, new tl2() { // from class: fo
            @Override // defpackage.tl2
            public final void onChanged(Object obj) {
                BasicActivity.F(BasicActivity.this, (Boolean) obj);
            }
        });
    }

    @Override // androidx.appcompat.app.AppCompatActivity, androidx.fragment.app.FragmentActivity, android.app.Activity
    public void onStart() {
        super.onStart();
        this.i0 = true;
    }

    @Override // androidx.appcompat.app.AppCompatActivity, androidx.fragment.app.FragmentActivity, android.app.Activity
    public void onStop() {
        super.onStop();
        this.i0 = false;
    }

    public final void w() {
        Intent intent = new Intent(this, AKTLoginActivity.class);
        intent.putExtra("bundle.KEY_SOURCE", "bundle.SOURCE_APP");
        te4 te4Var = te4.a;
        startActivity(intent);
    }

    public final void x() {
        boolean d = pg4.d(this);
        boolean e = bo3.e(this, "TWO_FACTOR", false);
        long A = i2.A() - (bo3.c(this, "TIMENOW") ? bo3.g(this, "TIMENOW") : 0L);
        Long l = s.d;
        fs1.e(l, "loginTimeout");
        boolean z = A > l.longValue();
        if ((d && e) || z) {
            w();
        }
    }

    public final void y() {
        new Handler(Looper.getMainLooper()).post(new Runnable() { // from class: ho
            @Override // java.lang.Runnable
            public final void run() {
                BasicActivity.z(BasicActivity.this);
            }
        });
    }
}
