package net.safemoon.androidwallet.activity;

import java.util.Iterator;
import java.util.List;
import kotlin.jvm.internal.Lambda;
import net.safemoon.androidwallet.model.wallets.Wallet;
import net.safemoon.androidwallet.viewmodels.MultiWalletViewModel;

/* compiled from: StartWalletActivity.kt */
/* loaded from: classes2.dex */
public final class StartWalletActivity$parseMessage$1 extends Lambda implements tc1<List<? extends Wallet>, te4> {
    public final /* synthetic */ StartWalletActivity this$0;

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public StartWalletActivity$parseMessage$1(StartWalletActivity startWalletActivity) {
        super(1);
        this.this$0 = startWalletActivity;
    }

    @Override // defpackage.tc1
    public /* bridge */ /* synthetic */ te4 invoke(List<? extends Wallet> list) {
        invoke2((List<Wallet>) list);
        return te4.a;
    }

    /* renamed from: invoke  reason: avoid collision after fix types in other method */
    public final void invoke2(List<Wallet> list) {
        boolean S0;
        MultiWalletViewModel multiWalletViewModel;
        MultiWalletViewModel multiWalletViewModel2;
        fs1.f(list, "list");
        Iterator<Wallet> it = list.iterator();
        while (true) {
            if (!it.hasNext()) {
                break;
            }
            Wallet next = it.next();
            if (!next.isPrimaryWallet()) {
                multiWalletViewModel2 = this.this$0.k0;
                multiWalletViewModel2.C(next, 1, null);
            } else if (!next.isLinked()) {
                multiWalletViewModel = this.this$0.k0;
                multiWalletViewModel.C(next, 1, null);
            }
        }
        do3.a.h(this.this$0, list.size() != 1);
        S0 = this.this$0.S0();
        if (S0) {
            this.this$0.C0();
        } else {
            this.this$0.F0(false);
        }
    }
}
