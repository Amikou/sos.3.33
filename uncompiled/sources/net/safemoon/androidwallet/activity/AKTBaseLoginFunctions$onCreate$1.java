package net.safemoon.androidwallet.activity;

import java.util.Collection;
import java.util.List;
import kotlin.jvm.internal.Lambda;
import net.safemoon.androidwallet.model.wallets.Wallet;

/* compiled from: AKTBaseLoginFunctions.kt */
/* loaded from: classes2.dex */
public final class AKTBaseLoginFunctions$onCreate$1 extends Lambda implements tc1<List<? extends Wallet>, te4> {
    public final /* synthetic */ AKTBaseLoginFunctions this$0;

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public AKTBaseLoginFunctions$onCreate$1(AKTBaseLoginFunctions aKTBaseLoginFunctions) {
        super(1);
        this.this$0 = aKTBaseLoginFunctions;
    }

    @Override // defpackage.tc1
    public /* bridge */ /* synthetic */ te4 invoke(List<? extends Wallet> list) {
        invoke2((List<Wallet>) list);
        return te4.a;
    }

    /* renamed from: invoke  reason: avoid collision after fix types in other method */
    public final void invoke2(List<Wallet> list) {
        boolean z;
        fs1.f(list, "list");
        AKTBaseLoginFunctions aKTBaseLoginFunctions = this.this$0;
        boolean z2 = true;
        if (!(list instanceof Collection) || !list.isEmpty()) {
            for (Wallet wallet2 : list) {
                String u5b64 = wallet2.getU5B64();
                if (u5b64 == null || u5b64.length() == 0) {
                    z = true;
                    continue;
                } else {
                    z = false;
                    continue;
                }
                if (z) {
                    break;
                }
            }
        }
        z2 = false;
        aKTBaseLoginFunctions.n0 = z2;
    }
}
