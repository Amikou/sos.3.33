package net.safemoon.androidwallet.activity;

import android.content.Intent;
import kotlin.jvm.internal.Lambda;

/* compiled from: AKTAnswerQuestionsActivity.kt */
/* loaded from: classes2.dex */
public final class AKTAnswerQuestionsActivity$secondQuestion$2 extends Lambda implements rc1<String> {
    public final /* synthetic */ AKTAnswerQuestionsActivity this$0;

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public AKTAnswerQuestionsActivity$secondQuestion$2(AKTAnswerQuestionsActivity aKTAnswerQuestionsActivity) {
        super(0);
        this.this$0 = aKTAnswerQuestionsActivity;
    }

    @Override // defpackage.rc1
    public final String invoke() {
        String stringExtra;
        Intent intent = this.this$0.getIntent();
        return (intent == null || (stringExtra = intent.getStringExtra("SECOND_QUESTION")) == null) ? "" : stringExtra;
    }
}
