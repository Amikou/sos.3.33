package net.safemoon.androidwallet.activity;

import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.view.View;
import androidx.appcompat.app.ActionBar;
import net.safemoon.androidwallet.R;
import net.safemoon.androidwallet.activity.AKTGetEmailActivity;
import net.safemoon.androidwallet.activity.AKTRegisterGuideActivity;
import net.safemoon.androidwallet.activity.common.BasicActivity;

/* compiled from: AKTRegisterGuideActivity.kt */
/* loaded from: classes2.dex */
public final class AKTRegisterGuideActivity extends BasicActivity {
    public final sy1 j0 = zy1.a(new AKTRegisterGuideActivity$binding$2(this));

    public static final void O(AKTRegisterGuideActivity aKTRegisterGuideActivity, View view) {
        fs1.f(aKTRegisterGuideActivity, "this$0");
        aKTRegisterGuideActivity.onBackPressed();
    }

    public static final void P(AKTRegisterGuideActivity aKTRegisterGuideActivity, View view) {
        fs1.f(aKTRegisterGuideActivity, "this$0");
        AKTGetEmailActivity.a.b(AKTGetEmailActivity.s0, aKTRegisterGuideActivity, false, true, false, 10, null);
    }

    public static final void Q(AKTRegisterGuideActivity aKTRegisterGuideActivity, View view) {
        fs1.f(aKTRegisterGuideActivity, "this$0");
        AKTLoginActivity.r0.a(aKTRegisterGuideActivity, true);
    }

    public final u6 N() {
        return (u6) this.j0.getValue();
    }

    @Override // net.safemoon.androidwallet.activity.common.BasicActivity, androidx.fragment.app.FragmentActivity, androidx.activity.ComponentActivity, androidx.core.app.ComponentActivity, android.app.Activity
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        setContentView(N().b());
        ActionBar supportActionBar = getSupportActionBar();
        if (supportActionBar != null) {
            supportActionBar.l();
        }
        getWindow().addFlags(Integer.MIN_VALUE);
        getWindow().setStatusBarColor(m70.d(this, 17170445));
        getWindow().setBackgroundDrawable(new ColorDrawable(getColor(R.color.akt_night_background)));
    }

    @Override // net.safemoon.androidwallet.activity.common.BasicActivity, androidx.appcompat.app.AppCompatActivity, android.app.Activity
    public void onPostCreate(Bundle bundle) {
        super.onPostCreate(bundle);
        u6 N = N();
        N.c.e.setText(getString(R.string.akt_activity_register_txt));
        N.c.c.setVisibility(8);
        N.c.d.setVisibility(8);
        N.c.b.setOnClickListener(new View.OnClickListener() { // from class: c2
            @Override // android.view.View.OnClickListener
            public final void onClick(View view) {
                AKTRegisterGuideActivity.O(AKTRegisterGuideActivity.this, view);
            }
        });
        N.b.setOnClickListener(new View.OnClickListener() { // from class: d2
            @Override // android.view.View.OnClickListener
            public final void onClick(View view) {
                AKTRegisterGuideActivity.P(AKTRegisterGuideActivity.this, view);
            }
        });
        N.d.setOnClickListener(new View.OnClickListener() { // from class: b2
            @Override // android.view.View.OnClickListener
            public final void onClick(View view) {
                AKTRegisterGuideActivity.Q(AKTRegisterGuideActivity.this, view);
            }
        });
    }
}
