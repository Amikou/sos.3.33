package net.safemoon.androidwallet.activity;

import android.content.DialogInterface;
import androidx.fragment.app.FragmentManager;
import kotlin.jvm.internal.Lambda;
import net.safemoon.androidwallet.R;
import net.safemoon.androidwallet.dialogs.ProgressLoading;

/* compiled from: AKTSecurityQuestionsActivity.kt */
/* loaded from: classes2.dex */
public final class AKTSecurityQuestionsActivity$showConfirmSecurityQuestionsDialog$1 extends Lambda implements tc1<DialogInterface, te4> {
    public final /* synthetic */ AKTSecurityQuestionsActivity this$0;

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public AKTSecurityQuestionsActivity$showConfirmSecurityQuestionsDialog$1(AKTSecurityQuestionsActivity aKTSecurityQuestionsActivity) {
        super(1);
        this.this$0 = aKTSecurityQuestionsActivity;
    }

    @Override // defpackage.tc1
    public /* bridge */ /* synthetic */ te4 invoke(DialogInterface dialogInterface) {
        invoke2(dialogInterface);
        return te4.a;
    }

    /* renamed from: invoke  reason: avoid collision after fix types in other method */
    public final void invoke2(DialogInterface dialogInterface) {
        boolean z;
        ProgressLoading progressLoading;
        String T0;
        String T02;
        String O0;
        fs1.f(dialogInterface, "it");
        z = this.this$0.D0;
        if (z) {
            AKTSecurityQuestionsActivity aKTSecurityQuestionsActivity = this.this$0;
            T02 = aKTSecurityQuestionsActivity.T0();
            O0 = aKTSecurityQuestionsActivity.O0(T02);
            this.this$0.q1(O0);
            return;
        }
        AKTSecurityQuestionsActivity aKTSecurityQuestionsActivity2 = this.this$0;
        ProgressLoading.a aVar = ProgressLoading.y0;
        String string = aKTSecurityQuestionsActivity2.getString(R.string.loading);
        fs1.e(string, "getString(R.string.loading)");
        aKTSecurityQuestionsActivity2.j0 = aVar.a(false, string, "");
        progressLoading = this.this$0.j0;
        if (progressLoading != null) {
            FragmentManager supportFragmentManager = this.this$0.getSupportFragmentManager();
            fs1.e(supportFragmentManager, "supportFragmentManager");
            progressLoading.A(supportFragmentManager);
        }
        AKTSecurityQuestionsActivity aKTSecurityQuestionsActivity3 = this.this$0;
        T0 = aKTSecurityQuestionsActivity3.T0();
        aKTSecurityQuestionsActivity3.k1(T0);
    }
}
