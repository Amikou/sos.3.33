package net.safemoon.androidwallet.activity;

import android.content.ClipboardManager;
import java.util.Objects;
import kotlin.jvm.internal.Lambda;

/* compiled from: ImportWordActivity.kt */
/* loaded from: classes2.dex */
public final class ImportWordActivity$manager$2 extends Lambda implements rc1<ClipboardManager> {
    public final /* synthetic */ ImportWordActivity this$0;

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public ImportWordActivity$manager$2(ImportWordActivity importWordActivity) {
        super(0);
        this.this$0 = importWordActivity;
    }

    /* JADX WARN: Can't rename method to resolve collision */
    @Override // defpackage.rc1
    public final ClipboardManager invoke() {
        Object systemService = this.this$0.getSystemService("clipboard");
        Objects.requireNonNull(systemService, "null cannot be cast to non-null type android.content.ClipboardManager");
        return (ClipboardManager) systemService;
    }
}
