package net.safemoon.androidwallet.activity;

import defpackage.hy1;
import kotlin.jvm.internal.Lambda;
import net.safemoon.androidwallet.model.common.LanguageItem;

/* compiled from: AKTSelectLanguageActivity.kt */
/* loaded from: classes2.dex */
public final class AKTSelectLanguageActivity$defaultLanguageAdapter$2 extends Lambda implements rc1<a> {
    public final /* synthetic */ AKTSelectLanguageActivity this$0;

    /* compiled from: AKTSelectLanguageActivity.kt */
    /* loaded from: classes2.dex */
    public static final class a extends hy1 {
        public final /* synthetic */ AKTSelectLanguageActivity d;

        /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
        public a(AKTSelectLanguageActivity aKTSelectLanguageActivity, b bVar) {
            super(aKTSelectLanguageActivity, bVar);
            this.d = aKTSelectLanguageActivity;
        }

        @Override // defpackage.hy1
        public String f() {
            return do3.a.a(this.d);
        }
    }

    /* compiled from: AKTSelectLanguageActivity.kt */
    /* loaded from: classes2.dex */
    public static final class b implements hy1.a {
        public final /* synthetic */ AKTSelectLanguageActivity a;

        public b(AKTSelectLanguageActivity aKTSelectLanguageActivity) {
            this.a = aKTSelectLanguageActivity;
        }

        @Override // defpackage.hy1.a
        public void a(LanguageItem languageItem) {
            fs1.f(languageItem, "item");
            ly1.a.c(this.a, languageItem);
            AKTConfigurationsActivity.l0.b(this.a);
        }
    }

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public AKTSelectLanguageActivity$defaultLanguageAdapter$2(AKTSelectLanguageActivity aKTSelectLanguageActivity) {
        super(0);
        this.this$0 = aKTSelectLanguageActivity;
    }

    @Override // defpackage.rc1
    public final a invoke() {
        return new a(this.this$0, new b(this.this$0));
    }
}
