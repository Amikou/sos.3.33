package net.safemoon.androidwallet.activity;

import android.content.Intent;
import kotlin.jvm.internal.Lambda;

/* compiled from: AKTVerifyEmailActivity.kt */
/* loaded from: classes2.dex */
public final class AKTVerifyEmailActivity$isChangeEmail$2 extends Lambda implements rc1<Boolean> {
    public final /* synthetic */ AKTVerifyEmailActivity this$0;

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public AKTVerifyEmailActivity$isChangeEmail$2(AKTVerifyEmailActivity aKTVerifyEmailActivity) {
        super(0);
        this.this$0 = aKTVerifyEmailActivity;
    }

    /* JADX WARN: Can't rename method to resolve collision */
    @Override // defpackage.rc1
    public final Boolean invoke() {
        Intent intent = this.this$0.getIntent();
        return Boolean.valueOf(intent != null ? intent.getBooleanExtra("IS_CHANGE_EMAIL", false) : false);
    }
}
