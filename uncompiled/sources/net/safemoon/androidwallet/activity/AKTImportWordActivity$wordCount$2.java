package net.safemoon.androidwallet.activity;

import kotlin.jvm.internal.Lambda;

/* compiled from: AKTImportWordActivity.kt */
/* loaded from: classes2.dex */
public final class AKTImportWordActivity$wordCount$2 extends Lambda implements rc1<Integer> {
    public final /* synthetic */ AKTImportWordActivity this$0;

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public AKTImportWordActivity$wordCount$2(AKTImportWordActivity aKTImportWordActivity) {
        super(0);
        this.this$0 = aKTImportWordActivity;
    }

    /* JADX WARN: Can't rename method to resolve collision */
    @Override // defpackage.rc1
    public final Integer invoke() {
        return Integer.valueOf(this.this$0.getIntent().getIntExtra("ARG_WORD_COUNTS", 12));
    }
}
