package net.safemoon.androidwallet.activity;

import android.content.DialogInterface;
import kotlin.jvm.internal.Lambda;
import net.safemoon.androidwallet.R;
import net.safemoon.androidwallet.dialogs.ProgressLoading;

/* compiled from: AKTImportPrivateKeyActivity.kt */
/* loaded from: classes2.dex */
public final class AKTImportPrivateKeyActivity$checkUser$1 extends Lambda implements md1<String, String, String, Boolean, te4> {
    public final /* synthetic */ AKTImportPrivateKeyActivity this$0;

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public AKTImportPrivateKeyActivity$checkUser$1(AKTImportPrivateKeyActivity aKTImportPrivateKeyActivity) {
        super(4);
        this.this$0 = aKTImportPrivateKeyActivity;
    }

    public static final void b(DialogInterface dialogInterface) {
    }

    @Override // defpackage.md1
    public /* bridge */ /* synthetic */ te4 invoke(String str, String str2, String str3, Boolean bool) {
        invoke2(str, str2, str3, bool);
        return te4.a;
    }

    /* renamed from: invoke  reason: avoid collision after fix types in other method */
    public final void invoke2(String str, String str2, String str3, Boolean bool) {
        ProgressLoading progressLoading;
        ProgressLoading progressLoading2;
        if (fs1.b(bool, Boolean.TRUE)) {
            progressLoading = this.this$0.j0;
            if (progressLoading != null) {
                progressLoading2 = this.this$0.j0;
                progressLoading2.h();
            }
            jc0.c(this.this$0, Integer.valueOf((int) R.string.warning_title), R.string.warning_black_list_address, true, d1.a);
            return;
        }
        this.this$0.m0(str, str2, str3);
    }
}
