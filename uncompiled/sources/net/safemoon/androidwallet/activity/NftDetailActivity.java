package net.safemoon.androidwallet.activity;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import java.util.ArrayList;
import net.safemoon.androidwallet.R;
import net.safemoon.androidwallet.activity.NftDetailActivity;
import net.safemoon.androidwallet.model.nft.NFTData;

/* compiled from: NftDetailActivity.kt */
/* loaded from: classes2.dex */
public final class NftDetailActivity extends AppCompatActivity {
    public static NFTData a;

    /* compiled from: NftDetailActivity.kt */
    /* loaded from: classes2.dex */
    public static final class a {
        public a() {
        }

        public /* synthetic */ a(qi0 qi0Var) {
            this();
        }
    }

    static {
        new a(null);
    }

    public static final void v(NftDetailActivity nftDetailActivity, View view) {
        fs1.f(nftDetailActivity, "this$0");
        nftDetailActivity.finish();
    }

    public static final void w(NftDetailActivity nftDetailActivity, View view) {
        fs1.f(nftDetailActivity, "this$0");
        nftDetailActivity.x();
    }

    @Override // androidx.fragment.app.FragmentActivity, androidx.activity.ComponentActivity, androidx.core.app.ComponentActivity, android.app.Activity
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        setContentView(R.layout.activity_nft_detail);
        ActionBar supportActionBar = getSupportActionBar();
        if (supportActionBar != null) {
            supportActionBar.l();
        }
        RecyclerView recyclerView = (RecyclerView) findViewById(R.id.propertyRecycle);
        recyclerView.setLayoutManager(new GridLayoutManager(this, 3));
        NFTData nFTData = a;
        if ((nFTData == null ? null : nFTData.getProperties()) != null) {
            NFTData nFTData2 = a;
            ArrayList<NFTData.Property> properties = nFTData2 == null ? null : nFTData2.getProperties();
            fs1.d(properties);
            recyclerView.setAdapter(new pc2(properties));
        }
        ImageView imageView = (ImageView) findViewById(R.id.nftDetailImage);
        TextView textView = (TextView) findViewById(R.id.txtDescription);
        TextView textView2 = (TextView) findViewById(R.id.nftSource);
        TextView textView3 = (TextView) findViewById(R.id.title);
        if (imageView != null) {
            k73 u = com.bumptech.glide.a.u(imageView);
            NFTData nFTData3 = a;
            u.y(nFTData3 == null ? null : nFTData3.getImageUrl()).I0(imageView);
        }
        findViewById(R.id.btnBack).setOnClickListener(new View.OnClickListener() { // from class: pf2
            @Override // android.view.View.OnClickListener
            public final void onClick(View view) {
                NftDetailActivity.v(NftDetailActivity.this, view);
            }
        });
        findViewById(R.id.btnOpen).setOnClickListener(new View.OnClickListener() { // from class: qf2
            @Override // android.view.View.OnClickListener
            public final void onClick(View view) {
                NftDetailActivity.w(NftDetailActivity.this, view);
            }
        });
        if (textView != null) {
            NFTData nFTData4 = a;
            textView.setText(nFTData4 == null ? null : nFTData4.getDescription());
        }
        if (textView2 != null) {
            NFTData nFTData5 = a;
            textView2.setText(nFTData5 == null ? null : nFTData5.getPermalink());
        }
        if (textView3 == null) {
            return;
        }
        NFTData nFTData6 = a;
        textView3.setText(nFTData6 != null ? nFTData6.getName() : null);
    }

    public final void x() {
        NFTData nFTData = a;
        startActivity(new Intent("android.intent.action.VIEW", Uri.parse(nFTData == null ? null : nFTData.getPermalink())));
    }
}
