package net.safemoon.androidwallet.activity;

import kotlin.jvm.internal.Lambda;

/* compiled from: StartWalletActivity.kt */
/* loaded from: classes2.dex */
public final class StartWalletActivity$proceedMasterWallet$1 extends Lambda implements tc1<Long, te4> {
    public final /* synthetic */ StartWalletActivity this$0;

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public StartWalletActivity$proceedMasterWallet$1(StartWalletActivity startWalletActivity) {
        super(1);
        this.this$0 = startWalletActivity;
    }

    @Override // defpackage.tc1
    public /* bridge */ /* synthetic */ te4 invoke(Long l) {
        invoke2(l);
        return te4.a;
    }

    /* renamed from: invoke  reason: avoid collision after fix types in other method */
    public final void invoke2(Long l) {
        boolean S0;
        boolean S02;
        if (l == null || l.longValue() <= 0) {
            S0 = this.this$0.S0();
            if (S0) {
                return;
            }
            this.this$0.V0();
            return;
        }
        S02 = this.this$0.S0();
        if (S02) {
            this.this$0.W0();
        } else {
            this.this$0.V0();
        }
    }
}
