package net.safemoon.androidwallet.activity;

import android.content.Context;
import android.content.Intent;
import android.content.res.ColorStateList;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.EditText;
import androidx.appcompat.app.ActionBar;
import androidx.fragment.app.FragmentManager;
import com.AKT.anonymouskey.ui.login.AKTServerFunctions;
import java.lang.ref.WeakReference;
import java.util.Arrays;
import java.util.Locale;
import java.util.Objects;
import kotlin.text.StringsKt__StringsKt;
import net.safemoon.androidwallet.R;
import net.safemoon.androidwallet.activity.AKTResetPasswordActivity;
import net.safemoon.androidwallet.dialogs.ProgressLoading;
import net.safemoon.androidwallet.viewmodels.AKTWebSocketHandlingViewModel;

/* compiled from: AKTResetPasswordActivity.kt */
/* loaded from: classes2.dex */
public final class AKTResetPasswordActivity extends AKTServerFunctions {
    public static final a q0 = new a(null);
    public final sy1 m0 = zy1.a(new AKTResetPasswordActivity$aktKs$2(this));
    public final sy1 n0 = zy1.a(new AKTResetPasswordActivity$aktPBHex$2(this));
    public final sy1 o0 = zy1.a(new AKTResetPasswordActivity$binding$2(this));
    public final sy1 p0 = new fj4(d53.b(AKTWebSocketHandlingViewModel.class), new AKTResetPasswordActivity$special$$inlined$viewModels$default$2(this), new AKTResetPasswordActivity$special$$inlined$viewModels$default$1(this));

    /* compiled from: AKTResetPasswordActivity.kt */
    /* loaded from: classes2.dex */
    public static final class a {
        public a() {
        }

        public /* synthetic */ a(qi0 qi0Var) {
            this();
        }

        public final void a(Context context, String str, String str2) {
            fs1.f(context, "context");
            fs1.f(str, "aktKs");
            fs1.f(str2, "aktPBHex");
            Intent intent = new Intent(context, AKTResetPasswordActivity.class);
            intent.putExtra("AKT_KS", str);
            intent.putExtra("AKT_PB", str2);
            context.startActivity(intent);
        }
    }

    /* compiled from: AKTResetPasswordActivity.kt */
    /* loaded from: classes2.dex */
    public static final class b implements TextWatcher {
        public final /* synthetic */ v6 a;
        public final /* synthetic */ AKTResetPasswordActivity f0;

        public b(v6 v6Var, AKTResetPasswordActivity aKTResetPasswordActivity) {
            this.a = v6Var;
            this.f0 = aKTResetPasswordActivity;
        }

        @Override // android.text.TextWatcher
        public void afterTextChanged(Editable editable) {
            fs1.f(editable, "s");
            String obj = editable.toString();
            String b = b30.a.b(obj);
            if (fs1.b(obj, b)) {
                this.f0.n0();
                return;
            }
            EditText editText = this.a.f.getEditText();
            if (editText != null) {
                editText.setText(b);
            }
            EditText editText2 = this.a.f.getEditText();
            if (editText2 == null) {
                return;
            }
            editText2.setSelection(b.length());
        }

        @Override // android.text.TextWatcher
        public void beforeTextChanged(CharSequence charSequence, int i, int i2, int i3) {
            fs1.f(charSequence, "s");
        }

        @Override // android.text.TextWatcher
        public void onTextChanged(CharSequence charSequence, int i, int i2, int i3) {
            fs1.f(charSequence, "s");
        }
    }

    /* compiled from: AKTResetPasswordActivity.kt */
    /* loaded from: classes2.dex */
    public static final class c implements TextWatcher {
        public final /* synthetic */ v6 a;
        public final /* synthetic */ AKTResetPasswordActivity f0;

        public c(v6 v6Var, AKTResetPasswordActivity aKTResetPasswordActivity) {
            this.a = v6Var;
            this.f0 = aKTResetPasswordActivity;
        }

        @Override // android.text.TextWatcher
        public void afterTextChanged(Editable editable) {
            fs1.f(editable, "s");
            String obj = editable.toString();
            String b = b30.a.b(obj);
            if (fs1.b(obj, b)) {
                this.f0.n0();
                this.f0.o0(editable);
                return;
            }
            EditText editText = this.a.e.getEditText();
            if (editText != null) {
                editText.setText(b);
            }
            EditText editText2 = this.a.e.getEditText();
            if (editText2 == null) {
                return;
            }
            editText2.setSelection(b.length());
        }

        @Override // android.text.TextWatcher
        public void beforeTextChanged(CharSequence charSequence, int i, int i2, int i3) {
            fs1.f(charSequence, "s");
        }

        @Override // android.text.TextWatcher
        public void onTextChanged(CharSequence charSequence, int i, int i2, int i3) {
            fs1.f(charSequence, "s");
        }
    }

    /* compiled from: AKTResetPasswordActivity.kt */
    /* loaded from: classes2.dex */
    public static final class d implements TextWatcher {
        public final /* synthetic */ v6 a;
        public final /* synthetic */ AKTResetPasswordActivity f0;

        public d(v6 v6Var, AKTResetPasswordActivity aKTResetPasswordActivity) {
            this.a = v6Var;
            this.f0 = aKTResetPasswordActivity;
        }

        @Override // android.text.TextWatcher
        public void afterTextChanged(Editable editable) {
            fs1.f(editable, "s");
            String obj = editable.toString();
            String b = b30.a.b(obj);
            if (fs1.b(obj, b)) {
                this.f0.n0();
                return;
            }
            EditText editText = this.a.d.getEditText();
            if (editText != null) {
                editText.setText(b);
            }
            EditText editText2 = this.a.d.getEditText();
            if (editText2 == null) {
                return;
            }
            editText2.setSelection(b.length());
        }

        @Override // android.text.TextWatcher
        public void beforeTextChanged(CharSequence charSequence, int i, int i2, int i3) {
            fs1.f(charSequence, "s");
        }

        @Override // android.text.TextWatcher
        public void onTextChanged(CharSequence charSequence, int i, int i2, int i3) {
            fs1.f(charSequence, "s");
        }
    }

    public static final void u0(AKTResetPasswordActivity aKTResetPasswordActivity, String str) {
        fs1.f(aKTResetPasswordActivity, "this$0");
        if (str == null) {
            return;
        }
        aKTResetPasswordActivity.X(aKTResetPasswordActivity.l0, str);
        aKTResetPasswordActivity.Q();
    }

    public static final void v0(AKTResetPasswordActivity aKTResetPasswordActivity, View view) {
        fs1.f(aKTResetPasswordActivity, "this$0");
        aKTResetPasswordActivity.onBackPressed();
    }

    public static final void w0(AKTResetPasswordActivity aKTResetPasswordActivity, View view) {
        fs1.f(aKTResetPasswordActivity, "this$0");
        aKTResetPasswordActivity.z0();
    }

    public static final void x0(AKTResetPasswordActivity aKTResetPasswordActivity, View view) {
        fs1.f(aKTResetPasswordActivity, "this$0");
        pg4.e(aKTResetPasswordActivity);
    }

    @Override // com.AKT.anonymouskey.ui.login.AKTServerFunctions
    public void Q() {
        r0().f();
    }

    @Override // com.AKT.anonymouskey.ui.login.AKTServerFunctions
    public void X(qy4 qy4Var, String str) {
        if (W(str)) {
            return;
        }
        ProgressLoading progressLoading = this.j0;
        if (progressLoading != null) {
            progressLoading.h();
        }
        String[] v = i2.v(b30.a.v(this, str), "|");
        fs1.e(v, "parts");
        if (!(v.length == 0)) {
            String str2 = v[0];
            fs1.e(str2, "parts[0]");
            if (StringsKt__StringsKt.w0(str2, new String[]{"="}, false, 0, 6, null).size() >= 2) {
                String str3 = v[0];
                fs1.e(str3, "parts[0]");
                String str4 = (String) StringsKt__StringsKt.w0(str3, new String[]{"="}, false, 0, 6, null).get(1);
                Objects.requireNonNull(str4, "null cannot be cast to non-null type java.lang.String");
                String upperCase = str4.toUpperCase(Locale.ROOT);
                fs1.e(upperCase, "(this as java.lang.Strin….toUpperCase(Locale.ROOT)");
                Q();
                if (fs1.b(upperCase, "AKTSERVERERROR")) {
                    Y(v, null);
                } else {
                    bh.V(new WeakReference(this), Integer.valueOf((int) R.string.akt_reset_password_dialog_success_content), null, R.string.akt_reset_password_back_to_login, new AKTResetPasswordActivity$parseMessage$1(qy4Var, this), 4, null);
                }
            }
        }
    }

    @Override // com.AKT.anonymouskey.ui.login.AKTServerFunctions
    public void Y(String[] strArr, String str) {
        bh.V(new WeakReference(this), null, Integer.valueOf((int) R.string.akt_error_cannot_register_new_key_message), 0, null, 26, null);
    }

    /* JADX WARN: Removed duplicated region for block: B:41:0x0067  */
    /* JADX WARN: Removed duplicated region for block: B:42:0x0069  */
    /* JADX WARN: Removed duplicated region for block: B:45:0x0079  */
    /* JADX WARN: Removed duplicated region for block: B:46:0x007b  */
    /* JADX WARN: Removed duplicated region for block: B:54:0x0098  */
    /* JADX WARN: Removed duplicated region for block: B:64:0x00bc  */
    /* JADX WARN: Removed duplicated region for block: B:67:0x00ca  */
    /* JADX WARN: Removed duplicated region for block: B:68:0x00cc  */
    /* JADX WARN: Removed duplicated region for block: B:70:0x00cf  */
    /* JADX WARN: Removed duplicated region for block: B:77:0x00ec  */
    /* JADX WARN: Removed duplicated region for block: B:78:0x010a  */
    /*
        Code decompiled incorrectly, please refer to instructions dump.
        To view partially-correct code enable 'Show inconsistent code' option in preferences
    */
    public final void n0() {
        /*
            Method dump skipped, instructions count: 296
            To view this dump change 'Code comments level' option to 'DEBUG'
        */
        throw new UnsupportedOperationException("Method not decompiled: net.safemoon.androidwallet.activity.AKTResetPasswordActivity.n0():void");
    }

    public final void o0(Editable editable) {
        int d2 = m70.d(this, R.color.akt_sign_in_wipe);
        ColorStateList e = m70.e(this, R.color.akt_sign_in_wipe);
        int d3 = m70.d(this, R.color.akt_button_inactive);
        ColorStateList e2 = m70.e(this, R.color.akt_button_inactive);
        if (editable.toString().length() > 7) {
            q0().j.setTextColor(d2);
            q0().g.setImageTintList(e);
            q0().j.setTag("complete");
        } else {
            q0().j.setTextColor(d3);
            q0().g.setImageTintList(e2);
            q0().j.setTag("incomplete");
        }
        if (s44.h(editable.toString())) {
            q0().n.setTextColor(d2);
            q0().h.setImageTintList(e);
            q0().n.setTag("complete");
        } else {
            q0().n.setTextColor(d3);
            q0().h.setImageTintList(e2);
            q0().n.setTag("incomplete");
        }
        if (s44.e(editable.toString())) {
            q0().m.setTextColor(d2);
            q0().i.setImageTintList(e);
            q0().m.setTag("complete");
            return;
        }
        q0().m.setTextColor(d3);
        q0().i.setImageTintList(e2);
        q0().m.setTag("incomplete");
    }

    @Override // com.AKT.anonymouskey.ui.login.AKTServerFunctions, net.safemoon.androidwallet.activity.common.BasicActivity, androidx.fragment.app.FragmentActivity, androidx.activity.ComponentActivity, androidx.core.app.ComponentActivity, android.app.Activity
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        setContentView(q0().b());
        ActionBar supportActionBar = getSupportActionBar();
        if (supportActionBar != null) {
            supportActionBar.l();
        }
        getWindow().addFlags(Integer.MIN_VALUE);
        getWindow().setStatusBarColor(m70.d(this, 17170445));
        getWindow().setBackgroundDrawable(new ColorDrawable(getColor(R.color.akt_night_background)));
        r0().l();
        t0();
    }

    @Override // net.safemoon.androidwallet.activity.common.BasicActivity, androidx.appcompat.app.AppCompatActivity, android.app.Activity
    public void onPostCreate(Bundle bundle) {
        super.onPostCreate(bundle);
        v6 q02 = q0();
        q02.k.e.setText(getString(R.string.akt_reset_password_header_title));
        q02.k.b.setVisibility(0);
        q02.k.b.setOnClickListener(new View.OnClickListener() { // from class: h2
            @Override // android.view.View.OnClickListener
            public final void onClick(View view) {
                AKTResetPasswordActivity.v0(AKTResetPasswordActivity.this, view);
            }
        });
        EditText editText = q02.f.getEditText();
        if (editText != null) {
            editText.addTextChangedListener(new b(q02, this));
        }
        EditText editText2 = q02.e.getEditText();
        if (editText2 != null) {
            editText2.addTextChangedListener(new c(q02, this));
        }
        EditText editText3 = q02.d.getEditText();
        if (editText3 != null) {
            editText3.addTextChangedListener(new d(q02, this));
        }
        n0();
        EditText editText4 = q02.e.getEditText();
        fs1.d(editText4);
        Editable text = editText4.getText();
        fs1.e(text, "etPassword.editText!!.text");
        o0(text);
        q02.b.setOnClickListener(new View.OnClickListener() { // from class: g2
            @Override // android.view.View.OnClickListener
            public final void onClick(View view) {
                AKTResetPasswordActivity.w0(AKTResetPasswordActivity.this, view);
            }
        });
        q02.c.setOnClickListener(new View.OnClickListener() { // from class: f2
            @Override // android.view.View.OnClickListener
            public final void onClick(View view) {
                AKTResetPasswordActivity.x0(AKTResetPasswordActivity.this, view);
            }
        });
    }

    public final String p0() {
        return (String) this.n0.getValue();
    }

    public final v6 q0() {
        return (v6) this.o0.getValue();
    }

    public final AKTWebSocketHandlingViewModel r0() {
        return (AKTWebSocketHandlingViewModel) this.p0.getValue();
    }

    public final void s0() {
        ProgressLoading.a aVar = ProgressLoading.y0;
        String string = getString(R.string.loading);
        fs1.e(string, "getString(R.string.loading)");
        ProgressLoading a2 = aVar.a(false, string, "");
        this.j0 = a2;
        FragmentManager supportFragmentManager = getSupportFragmentManager();
        fs1.e(supportFragmentManager, "supportFragmentManager");
        a2.A(supportFragmentManager);
        EditText editText = q0().f.getEditText();
        String valueOf = String.valueOf(editText == null ? null : editText.getText());
        EditText editText2 = q0().d.getEditText();
        String valueOf2 = String.valueOf(editText2 != null ? editText2.getText() : null);
        b30 b30Var = b30.a;
        String n = b30Var.n(valueOf);
        String n2 = b30Var.n(valueOf2);
        String p0 = p0();
        fs1.e(p0, "aktPBHex");
        String f0 = f0(p0, n, n2);
        fs1.e(f0, "updateKAandU5(pbHex, newUsername, newPassword)");
        y0(f0);
    }

    public void t0() {
        r0().h().observe(this, new tl2() { // from class: e2
            @Override // defpackage.tl2
            public final void onChanged(Object obj) {
                AKTResetPasswordActivity.u0(AKTResetPasswordActivity.this, (String) obj);
            }
        });
    }

    public void y0(String str) {
        if (str == null) {
            return;
        }
        r0().k(str);
    }

    public final void z0() {
        StringBuilder sb = new StringBuilder();
        sb.append("<b>");
        EditText editText = q0().f.getEditText();
        sb.append((Object) (editText == null ? null : editText.getText()));
        sb.append("</b>");
        String sb2 = sb.toString();
        lu3 lu3Var = lu3.a;
        String string = getString(R.string.akt_reset_password_confirm_dialog_content);
        fs1.e(string, "getString(R.string.akt_r…d_confirm_dialog_content)");
        String format = String.format(string, Arrays.copyOf(new Object[]{sb2}, 1));
        fs1.e(format, "java.lang.String.format(format, *args)");
        bh.a0(new WeakReference(this), (r18 & 2) != 0 ? null : Integer.valueOf((int) R.string.akt_reset_password_confirm_dialog_header), (r18 & 4) != 0 ? null : null, (r18 & 8) != 0 ? null : format, (r18 & 16) != 0 ? R.string.action_ok : R.string.confirm, (r18 & 32) != 0 ? R.string.cancel : R.string.ca_alert_save_continue_editing, new AKTResetPasswordActivity$showConfirmResetPasswordDialog$1(this), AKTResetPasswordActivity$showConfirmResetPasswordDialog$2.INSTANCE);
    }
}
