package net.safemoon.androidwallet.activity;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.text.Editable;
import android.view.View;
import com.google.android.material.textfield.TextInputEditText;
import net.safemoon.androidwallet.R;
import net.safemoon.androidwallet.activity.ImportPrivateKeyActivity;

/* compiled from: ImportPrivateKeyActivity.kt */
/* loaded from: classes2.dex */
public final class ImportPrivateKeyActivity extends RecoverWalletActivity {
    public static final a n0 = new a(null);
    public final sy1 m0 = zy1.a(new ImportPrivateKeyActivity$binding$2(this));

    /* compiled from: ImportPrivateKeyActivity.kt */
    /* loaded from: classes2.dex */
    public static final class a {
        public a() {
        }

        public /* synthetic */ a(qi0 qi0Var) {
            this();
        }

        public final void a(Context context) {
            fs1.f(context, "context");
            Intent intent = new Intent(context, ImportPrivateKeyActivity.class);
            te4 te4Var = te4.a;
            context.startActivity(intent);
        }
    }

    public static final void l0(g7 g7Var, ImportPrivateKeyActivity importPrivateKeyActivity, View view) {
        fs1.f(g7Var, "$this_apply");
        fs1.f(importPrivateKeyActivity, "this$0");
        Editable text = g7Var.d.getText();
        if (importPrivateKeyActivity.p0(text == null ? null : text.toString())) {
            if (g7Var.d.getText() == null) {
                return;
            }
            importPrivateKeyActivity.S(String.valueOf(g7Var.d.getText()));
            return;
        }
        jc0.c(importPrivateKeyActivity, Integer.valueOf((int) R.string.invalid_private_key), R.string.invalid_private_key_description, true, kp1.a);
    }

    public static final void m0(DialogInterface dialogInterface) {
    }

    public static final void n0(ImportPrivateKeyActivity importPrivateKeyActivity, View view) {
        fs1.f(importPrivateKeyActivity, "this$0");
        importPrivateKeyActivity.onBackPressed();
    }

    public final g7 j0() {
        return (g7) this.m0.getValue();
    }

    public final void k0() {
        final g7 j0 = j0();
        zh4 zh4Var = j0.e;
        fs1.e(zh4Var, "viewBarcodeComponentLayout");
        TextInputEditText textInputEditText = j0.d;
        fs1.e(textInputEditText, "editPrivateKey");
        cj4.n(zh4Var, textInputEditText, new ImportPrivateKeyActivity$setView$1$1(this));
        j0.c.setOnClickListener(new View.OnClickListener() { // from class: lp1
            @Override // android.view.View.OnClickListener
            public final void onClick(View view) {
                ImportPrivateKeyActivity.l0(g7.this, this, view);
            }
        });
        j0.b.setOnClickListener(new View.OnClickListener() { // from class: mp1
            @Override // android.view.View.OnClickListener
            public final void onClick(View view) {
                ImportPrivateKeyActivity.n0(ImportPrivateKeyActivity.this, view);
            }
        });
    }

    public final void o0() {
        w7<Intent> b;
        hn2 D = D();
        if (D == null || (b = D.b(new ImportPrivateKeyActivity$startScanQRCode$1(this))) == null) {
            return;
        }
        b.a(new Intent(this, ScannedCodeActivity.class));
    }

    @Override // net.safemoon.androidwallet.activity.RecoverWalletActivity, net.safemoon.androidwallet.activity.common.BasicActivity, androidx.fragment.app.FragmentActivity, androidx.activity.ComponentActivity, androidx.core.app.ComponentActivity, android.app.Activity
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        setContentView(j0().b());
    }

    @Override // net.safemoon.androidwallet.activity.RecoverWalletActivity, net.safemoon.androidwallet.activity.common.BasicActivity, androidx.appcompat.app.AppCompatActivity, android.app.Activity
    public void onPostCreate(Bundle bundle) {
        super.onPostCreate(bundle);
        k0();
    }

    public final boolean p0(String str) {
        return str != null && str.length() >= 64;
    }
}
