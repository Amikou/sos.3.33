package net.safemoon.androidwallet.activity;

import android.content.Intent;
import kotlin.jvm.internal.Lambda;

/* compiled from: ImportPrivateKeyActivity.kt */
/* loaded from: classes2.dex */
public final class ImportPrivateKeyActivity$startScanQRCode$1 extends Lambda implements tc1<Intent, te4> {
    public final /* synthetic */ ImportPrivateKeyActivity this$0;

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public ImportPrivateKeyActivity$startScanQRCode$1(ImportPrivateKeyActivity importPrivateKeyActivity) {
        super(1);
        this.this$0 = importPrivateKeyActivity;
    }

    @Override // defpackage.tc1
    public /* bridge */ /* synthetic */ te4 invoke(Intent intent) {
        invoke2(intent);
        return te4.a;
    }

    /* renamed from: invoke  reason: avoid collision after fix types in other method */
    public final void invoke2(Intent intent) {
        String stringExtra;
        g7 j0;
        if (intent == null || (stringExtra = intent.getStringExtra("result")) == null) {
            return;
        }
        j0 = this.this$0.j0();
        j0.d.setText(stringExtra);
    }
}
