package net.safemoon.androidwallet.activity;

import defpackage.z21;
import kotlin.jvm.internal.Lambda;
import net.safemoon.androidwallet.model.fiat.gson.Fiat;
import net.safemoon.androidwallet.viewmodels.SelectFiatViewModel;

/* compiled from: AKTSelectCurrencyActivity.kt */
/* loaded from: classes2.dex */
public final class AKTSelectCurrencyActivity$defaultCurrencyAdapter$2 extends Lambda implements rc1<a> {
    public final /* synthetic */ AKTSelectCurrencyActivity this$0;

    /* compiled from: AKTSelectCurrencyActivity.kt */
    /* loaded from: classes2.dex */
    public static final class a extends z21 {
        public a(AKTSelectCurrencyActivity aKTSelectCurrencyActivity, b bVar) {
            super(aKTSelectCurrencyActivity, bVar);
        }

        @Override // defpackage.z21
        public Fiat c() {
            return u21.a.a();
        }
    }

    /* compiled from: AKTSelectCurrencyActivity.kt */
    /* loaded from: classes2.dex */
    public static final class b implements z21.a {
        public final /* synthetic */ AKTSelectCurrencyActivity a;

        public b(AKTSelectCurrencyActivity aKTSelectCurrencyActivity) {
            this.a = aKTSelectCurrencyActivity;
        }

        @Override // defpackage.z21.a
        public void a(Fiat fiat) {
            fs1.f(fiat, "item");
            SelectFiatViewModel.n(this.a.U(), fiat, false, null, 4, null);
            this.a.onBackPressed();
        }
    }

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public AKTSelectCurrencyActivity$defaultCurrencyAdapter$2(AKTSelectCurrencyActivity aKTSelectCurrencyActivity) {
        super(0);
        this.this$0 = aKTSelectCurrencyActivity;
    }

    @Override // defpackage.rc1
    public final a invoke() {
        return new a(this.this$0, new b(this.this$0));
    }
}
