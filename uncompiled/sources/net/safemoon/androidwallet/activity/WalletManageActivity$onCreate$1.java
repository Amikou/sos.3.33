package net.safemoon.androidwallet.activity;

import java.util.List;
import kotlin.jvm.internal.Lambda;
import net.safemoon.androidwallet.model.wallets.Wallet;

/* compiled from: WalletManageActivity.kt */
/* loaded from: classes2.dex */
public final class WalletManageActivity$onCreate$1 extends Lambda implements tc1<List<? extends Wallet>, te4> {
    public final /* synthetic */ WalletManageActivity this$0;

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public WalletManageActivity$onCreate$1(WalletManageActivity walletManageActivity) {
        super(1);
        this.this$0 = walletManageActivity;
    }

    @Override // defpackage.tc1
    public /* bridge */ /* synthetic */ te4 invoke(List<? extends Wallet> list) {
        invoke2((List<Wallet>) list);
        return te4.a;
    }

    /* renamed from: invoke  reason: avoid collision after fix types in other method */
    public final void invoke2(List<Wallet> list) {
        fs1.f(list, "list");
        this.this$0.z0 = list.size();
        this.this$0.R1();
    }
}
