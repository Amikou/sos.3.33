package net.safemoon.androidwallet.activity;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.view.View;
import android.widget.CompoundButton;
import android.widget.LinearLayout;
import androidx.appcompat.app.ActionBar;
import androidx.fragment.app.FragmentManager;
import com.AKT.anonymouskey.ui.login.AKTServerFunctions;
import java.lang.ref.WeakReference;
import java.util.List;
import java.util.Objects;
import kotlin.text.StringsKt__StringsKt;
import net.safemoon.androidwallet.R;
import net.safemoon.androidwallet.activity.AKTImportExistingWalletsActivity;
import net.safemoon.androidwallet.dialogs.ProgressLoading;
import net.safemoon.androidwallet.model.wallets.Wallet;
import net.safemoon.androidwallet.viewmodels.AKTWebSocketHandlingViewModel;
import net.safemoon.androidwallet.viewmodels.MultiWalletViewModel;
import net.safemoon.androidwallet.views.CustomRecoveryWalletLayout;

/* compiled from: AKTImportExistingWalletsActivity.kt */
/* loaded from: classes2.dex */
public final class AKTImportExistingWalletsActivity extends AKTServerFunctions {
    public static final a v0 = new a(null);
    public final sy1 m0 = zy1.a(new AKTImportExistingWalletsActivity$binding$2(this));
    public final gb2<Boolean> n0 = new gb2<>(Boolean.FALSE);
    public final sy1 o0 = new fj4(d53.b(MultiWalletViewModel.class), new AKTImportExistingWalletsActivity$special$$inlined$viewModels$default$2(this), new AKTImportExistingWalletsActivity$special$$inlined$viewModels$default$1(this));
    public final sy1 p0 = zy1.a(new AKTImportExistingWalletsActivity$registerParam$2(this));
    public final sy1 q0 = zy1.a(new AKTImportExistingWalletsActivity$isLoginCase$2(this));
    public final sy1 r0 = zy1.a(new AKTImportExistingWalletsActivity$masterMnemonic$2(this));
    public final sy1 s0 = zy1.a(new AKTImportExistingWalletsActivity$username$2(this));
    public final sy1 t0 = zy1.a(new AKTImportExistingWalletsActivity$password$2(this));
    public final sy1 u0 = new fj4(d53.b(AKTWebSocketHandlingViewModel.class), new AKTImportExistingWalletsActivity$special$$inlined$viewModels$default$4(this), new AKTImportExistingWalletsActivity$special$$inlined$viewModels$default$3(this));

    /* compiled from: AKTImportExistingWalletsActivity.kt */
    /* loaded from: classes2.dex */
    public static final class a {
        public a() {
        }

        public /* synthetic */ a(qi0 qi0Var) {
            this();
        }

        public static /* synthetic */ void b(a aVar, Context context, String str, boolean z, String str2, String str3, String str4, int i, Object obj) {
            aVar.a(context, (i & 2) != 0 ? "" : str, (i & 4) != 0 ? false : z, (i & 8) != 0 ? "" : str2, (i & 16) != 0 ? "" : str3, (i & 32) == 0 ? str4 : "");
        }

        public final void a(Context context, String str, boolean z, String str2, String str3, String str4) {
            fs1.f(context, "context");
            fs1.f(str, "registerParam");
            fs1.f(str2, "masterMnemonic");
            fs1.f(str3, "username");
            fs1.f(str4, "password");
            Intent intent = new Intent(context, AKTImportExistingWalletsActivity.class);
            intent.putExtra("KEY_REGISTER_PARAM", str);
            intent.putExtra("KEY_IS_LOGIN", z);
            intent.putExtra("KEY_MASTER_MNEMONIC", str2);
            intent.putExtra("KEY_USERNAME", str3);
            intent.putExtra("KEY_PASSWORD", str4);
            context.startActivity(intent);
        }
    }

    public static final void C0(AKTImportExistingWalletsActivity aKTImportExistingWalletsActivity, String str) {
        fs1.f(aKTImportExistingWalletsActivity, "this$0");
        if (str == null) {
            return;
        }
        aKTImportExistingWalletsActivity.X(aKTImportExistingWalletsActivity.l0, str);
        aKTImportExistingWalletsActivity.Q();
    }

    public static final void E0(AKTImportExistingWalletsActivity aKTImportExistingWalletsActivity, View view) {
        fs1.f(aKTImportExistingWalletsActivity, "this$0");
        aKTImportExistingWalletsActivity.onBackPressed();
    }

    public static final void F0(AKTImportExistingWalletsActivity aKTImportExistingWalletsActivity, CompoundButton compoundButton, boolean z) {
        fs1.f(aKTImportExistingWalletsActivity, "this$0");
        aKTImportExistingWalletsActivity.n0.setValue(Boolean.valueOf(z));
        aKTImportExistingWalletsActivity.R0();
    }

    public static final void G0(AKTImportExistingWalletsActivity aKTImportExistingWalletsActivity, View view) {
        fs1.f(aKTImportExistingWalletsActivity, "this$0");
        ProgressLoading.a aVar = ProgressLoading.y0;
        String string = aKTImportExistingWalletsActivity.getString(R.string.loading);
        fs1.e(string, "getString(R.string.loading)");
        ProgressLoading a2 = aVar.a(false, string, "");
        aKTImportExistingWalletsActivity.j0 = a2;
        if (a2 != null) {
            FragmentManager supportFragmentManager = aKTImportExistingWalletsActivity.getSupportFragmentManager();
            fs1.e(supportFragmentManager, "supportFragmentManager");
            a2.A(supportFragmentManager);
        }
        aKTImportExistingWalletsActivity.L0(aKTImportExistingWalletsActivity.y0());
    }

    public static final void H0(AKTImportExistingWalletsActivity aKTImportExistingWalletsActivity, View view) {
        fs1.f(aKTImportExistingWalletsActivity, "this$0");
        bh.M(new WeakReference(aKTImportExistingWalletsActivity), Integer.valueOf((int) R.string.akt_login_and_import_existing_wallets_confirm_dialog_header), R.string.akt_login_and_import_existing_wallets_confirm_dialog_content, R.string.confirm, R.string.cancel, new AKTImportExistingWalletsActivity$onPostCreate$1$4$1(aKTImportExistingWalletsActivity), AKTImportExistingWalletsActivity$onPostCreate$1$4$2.INSTANCE);
    }

    public static final void I0(AKTImportExistingWalletsActivity aKTImportExistingWalletsActivity, View view) {
        fs1.f(aKTImportExistingWalletsActivity, "this$0");
        bh.M(new WeakReference(aKTImportExistingWalletsActivity), Integer.valueOf((int) R.string.akt_login_and_not_import_existing_wallets_confirm_dialog_header), R.string.akt_login_and_not_import_existing_wallets_confirm_dialog_content, R.string.confirm, R.string.cancel, new AKTImportExistingWalletsActivity$onPostCreate$1$5$1(aKTImportExistingWalletsActivity), AKTImportExistingWalletsActivity$onPostCreate$1$5$2.INSTANCE);
    }

    public static /* synthetic */ void Q0(AKTImportExistingWalletsActivity aKTImportExistingWalletsActivity, String str, boolean z, boolean z2, int i, Object obj) {
        if ((i & 4) != 0) {
            z2 = false;
        }
        aKTImportExistingWalletsActivity.P0(str, z, z2);
    }

    public final AKTWebSocketHandlingViewModel A0() {
        return (AKTWebSocketHandlingViewModel) this.u0.getValue();
    }

    public void B0() {
        A0().h().observe(this, new tl2() { // from class: q0
            @Override // defpackage.tl2
            public final void onChanged(Object obj) {
                AKTImportExistingWalletsActivity.C0(AKTImportExistingWalletsActivity.this, (String) obj);
            }
        });
    }

    public final boolean D0() {
        return ((Boolean) this.q0.getValue()).booleanValue();
    }

    public final void J0(String[] strArr) {
        int length = strArr.length;
        boolean z = false;
        String str = null;
        if (1 < length) {
            int i = 1;
            while (true) {
                int i2 = i + 1;
                Object[] array = StringsKt__StringsKt.w0(strArr[i], new String[]{"="}, false, 0, 6, null).toArray(new String[0]);
                Objects.requireNonNull(array, "null cannot be cast to non-null type kotlin.Array<T>");
                String[] strArr2 = (String[]) array;
                if (strArr2.length >= 2 && fs1.b(strArr2[0], "APIKEY")) {
                    str = strArr2[1];
                }
                if (i2 >= length) {
                    break;
                }
                i = i2;
            }
        }
        String j = bo3.j(this, "PB5K", "");
        dz4 dz4Var = new dz4();
        dz4Var.t(lm.a(j));
        String b = dz4Var.b(str);
        if (b != null) {
            if (b.length() > 0) {
                z = true;
            }
        }
        if (z) {
            jv0.e(this, "API_KEY", b);
        }
        b30.a.c(this);
    }

    public final void K0() {
        bo3.m(this, "TIMENOW", i2.A());
        String g = this.l0.g("EMAIL");
        String g2 = this.l0.g("KA");
        String g3 = this.l0.g("PBU5");
        String g4 = this.l0.g("PB5K");
        String g5 = this.l0.g("U5");
        String g6 = this.l0.g("K5");
        String g7 = this.l0.g("UK5");
        this.l0.g("PBHEX");
        bo3.o(this, "EMAIL", g);
        bo3.o(this, "KA", g2);
        bo3.o(this, "PBU5", g3);
        bo3.o(this, "U5", g5);
        bo3.o(this, "PB5K", g4);
        if (bo3.c(this, "TEMPU5")) {
            bo3.k(this, "TEMPU5");
        }
        if (bo3.c(this, "TEMPK5")) {
            bo3.k(this, "TEMPK5");
        }
        c0("K5", g6);
        c0("UK5", g7);
    }

    public void L0(String str) {
        if (str == null) {
            return;
        }
        A0().k(str);
    }

    public final void M0(List<Wallet> list) {
        if (list.isEmpty()) {
            return;
        }
        s6 u0 = u0();
        for (Wallet wallet2 : list) {
            CustomRecoveryWalletLayout customRecoveryWalletLayout = new CustomRecoveryWalletLayout(this);
            customRecoveryWalletLayout.setLayoutParams(new LinearLayout.LayoutParams(-1, -2));
            u0.f.addView(customRecoveryWalletLayout);
            customRecoveryWalletLayout.setAttachedActivity(this);
            customRecoveryWalletLayout.setupRecoveryWallet(wallet2);
        }
    }

    public final void N0() {
        ProgressLoading progressLoading = this.j0;
        if (progressLoading != null) {
            progressLoading.h();
        }
        Dialog U = bh.U(new WeakReference(this), Integer.valueOf((int) R.string.akt_login_and_import_existing_wallets_success_dialog_header), Integer.valueOf((int) R.string.akt_login_and_import_existing_wallets_success_dialog_content), R.string.akt_register_success_dialog_button, new AKTImportExistingWalletsActivity$showImportExistingWalletsSuccessWhenLogin$dialog$1(this));
        if (U != null) {
            U.setCancelable(false);
        }
        if (U == null) {
            return;
        }
        U.setCanceledOnTouchOutside(false);
    }

    public final void O0() {
        String M;
        if (s.b) {
            M = i2.e(this.l0, z0(), x0());
            fs1.e(M, "{\n            AKTSecpUti…d\n            )\n        }");
        } else {
            M = M(this.l0, z0(), x0());
            fs1.e(M, "{\n            anonymizeA…name, password)\n        }");
        }
        A0().k(M);
    }

    public final void P0(String str, boolean z, boolean z2) {
        ProgressLoading progressLoading = this.j0;
        if (progressLoading != null) {
            progressLoading.h();
        }
        StartWalletActivity.y0.a(this, z, str, false);
        finish();
    }

    @Override // com.AKT.anonymouskey.ui.login.AKTServerFunctions
    public void Q() {
        A0().f();
    }

    public final void R0() {
        if (fs1.b(this.n0.getValue(), Boolean.FALSE)) {
            u0().b.setTextColor(getColor(R.color.white));
            u0().b.setEnabled(false);
            u0().b.setBackgroundTintList(getColorStateList(R.color.akt_button_inactive));
            return;
        }
        u0().b.setTextColor(getColor(R.color.black));
        u0().b.setEnabled(true);
        u0().b.setBackgroundTintList(getColorStateList(R.color.akt_button_active));
    }

    /* JADX WARN: Code restructure failed: missing block: B:116:0x00f8, code lost:
        if (r4.equals("GR8REG02") == false) goto L52;
     */
    /* JADX WARN: Code restructure failed: missing block: B:123:0x010e, code lost:
        if (r4.equals("AKTSFMREG02") == false) goto L52;
     */
    /* JADX WARN: Code restructure failed: missing block: B:126:0x0117, code lost:
        if (r4.equals("SFMREG02") == false) goto L52;
     */
    /* JADX WARN: Code restructure failed: missing block: B:128:0x011a, code lost:
        if (r21 != null) goto L41;
     */
    /* JADX WARN: Code restructure failed: missing block: B:130:0x011d, code lost:
        r21.c("KA", defpackage.bo3.i(r20, "KA"));
     */
    /* JADX WARN: Code restructure failed: missing block: B:131:0x0124, code lost:
        defpackage.bo3.m(r20, "TIMENOW", defpackage.i2.A());
        O0();
     */
    /* JADX WARN: Code restructure failed: missing block: B:138:?, code lost:
        return;
     */
    @Override // com.AKT.anonymouskey.ui.login.AKTServerFunctions
    /*
        Code decompiled incorrectly, please refer to instructions dump.
        To view partially-correct code enable 'Show inconsistent code' option in preferences
    */
    public void X(defpackage.qy4 r21, java.lang.String r22) {
        /*
            Method dump skipped, instructions count: 332
            To view this dump change 'Code comments level' option to 'DEBUG'
        */
        throw new UnsupportedOperationException("Method not decompiled: net.safemoon.androidwallet.activity.AKTImportExistingWalletsActivity.X(qy4, java.lang.String):void");
    }

    @Override // com.AKT.anonymouskey.ui.login.AKTServerFunctions
    public void Y(String[] strArr, String str) {
        this.j0.h();
        bh.V(new WeakReference(this), null, Integer.valueOf((int) R.string.akt_error_cannot_register_new_key_message), 0, null, 26, null);
    }

    @Override // com.AKT.anonymouskey.ui.login.AKTServerFunctions, net.safemoon.androidwallet.activity.common.BasicActivity, androidx.fragment.app.FragmentActivity, androidx.activity.ComponentActivity, androidx.core.app.ComponentActivity, android.app.Activity
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        setContentView(u0().b());
        A0().l();
        B0();
        if (!zr.a.booleanValue()) {
            H(true);
            m43.a(this, true);
        }
        ActionBar supportActionBar = getSupportActionBar();
        if (supportActionBar != null) {
            supportActionBar.l();
        }
        getWindow().addFlags(Integer.MIN_VALUE);
        getWindow().setStatusBarColor(m70.d(this, 17170445));
        getWindow().setBackgroundDrawable(new ColorDrawable(getColor(R.color.akt_night_background)));
        w0().p(new AKTImportExistingWalletsActivity$onCreate$1(this));
    }

    @Override // net.safemoon.androidwallet.activity.common.BasicActivity, androidx.appcompat.app.AppCompatActivity, android.app.Activity
    public void onPostCreate(Bundle bundle) {
        super.onPostCreate(bundle);
        s6 u0 = u0();
        u0.g.e.setText(R.string.akt_existing_wallets_title);
        u0.g.b.setOnClickListener(new View.OnClickListener() { // from class: u0
            @Override // android.view.View.OnClickListener
            public final void onClick(View view) {
                AKTImportExistingWalletsActivity.E0(AKTImportExistingWalletsActivity.this, view);
            }
        });
        u0.e.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() { // from class: v0
            @Override // android.widget.CompoundButton.OnCheckedChangeListener
            public final void onCheckedChanged(CompoundButton compoundButton, boolean z) {
                AKTImportExistingWalletsActivity.F0(AKTImportExistingWalletsActivity.this, compoundButton, z);
            }
        });
        R0();
        u0.b.setOnClickListener(new View.OnClickListener() { // from class: t0
            @Override // android.view.View.OnClickListener
            public final void onClick(View view) {
                AKTImportExistingWalletsActivity.G0(AKTImportExistingWalletsActivity.this, view);
            }
        });
        if (D0()) {
            u0.e.setVisibility(8);
            u0.b.setVisibility(8);
            u0.i.setVisibility(0);
            u0.j.setVisibility(0);
            u0.d.setVisibility(0);
            u0.c.setVisibility(0);
            u0.h.setText(R.string.akt_existing_wallets_on_login_header);
            u0.k.setText(R.string.akt_existing_wallets_on_login_notice_content);
            u0.d.setOnClickListener(new View.OnClickListener() { // from class: r0
                @Override // android.view.View.OnClickListener
                public final void onClick(View view) {
                    AKTImportExistingWalletsActivity.H0(AKTImportExistingWalletsActivity.this, view);
                }
            });
            u0.c.setOnClickListener(new View.OnClickListener() { // from class: s0
                @Override // android.view.View.OnClickListener
                public final void onClick(View view) {
                    AKTImportExistingWalletsActivity.I0(AKTImportExistingWalletsActivity.this, view);
                }
            });
        }
    }

    public final s6 u0() {
        return (s6) this.m0.getValue();
    }

    public final String v0() {
        return (String) this.r0.getValue();
    }

    public final MultiWalletViewModel w0() {
        return (MultiWalletViewModel) this.o0.getValue();
    }

    public final String x0() {
        return (String) this.t0.getValue();
    }

    public final String y0() {
        return (String) this.p0.getValue();
    }

    public final String z0() {
        return (String) this.s0.getValue();
    }
}
