package net.safemoon.androidwallet.activity;

import java.util.Iterator;
import java.util.List;
import kotlin.jvm.internal.Lambda;
import net.safemoon.androidwallet.model.wallets.Wallet;
import net.safemoon.androidwallet.viewmodels.MultiWalletViewModel;

/* compiled from: AKTSplashActivity.kt */
/* loaded from: classes2.dex */
public final class AKTSplashActivity$onCreate$1 extends Lambda implements tc1<List<? extends Wallet>, te4> {
    public final /* synthetic */ AKTSplashActivity this$0;

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public AKTSplashActivity$onCreate$1(AKTSplashActivity aKTSplashActivity) {
        super(1);
        this.this$0 = aKTSplashActivity;
    }

    @Override // defpackage.tc1
    public /* bridge */ /* synthetic */ te4 invoke(List<? extends Wallet> list) {
        invoke2((List<Wallet>) list);
        return te4.a;
    }

    /* renamed from: invoke  reason: avoid collision after fix types in other method */
    public final void invoke2(List<Wallet> list) {
        MultiWalletViewModel B;
        MultiWalletViewModel B2;
        fs1.f(list, "list");
        for (Wallet wallet2 : list) {
            if (wallet2.isPrimaryWallet() && fs1.b(wallet2.getName(), "SafeMoon Master Wallet")) {
                B2 = this.this$0.B();
                B2.E(wallet2, "Primary Wallet", null);
                Wallet c = e30.c(this.this$0);
                if (fs1.b(c != null ? c.getAddress() : null, wallet2.getAddress())) {
                    wallet2.setName("Primary Wallet");
                    bo3.o(this.this$0, "SAFEMOON_ACTIVE_WALLET", wallet2.toString());
                }
            } else if (!wallet2.isPrimaryWallet() && j60.a.a().contains(wallet2.getName())) {
                String l = fs1.l(wallet2.getName(), " 1");
                B = this.this$0.B();
                B.E(wallet2, l, null);
                Wallet c2 = e30.c(this.this$0);
                if (fs1.b(c2 != null ? c2.getAddress() : null, wallet2.getAddress())) {
                    wallet2.setName(l);
                    bo3.o(this.this$0, "SAFEMOON_ACTIVE_WALLET", wallet2.toString());
                }
            }
        }
        AKTSplashActivity aKTSplashActivity = this.this$0;
        boolean z = false;
        if (!list.isEmpty()) {
            Iterator<T> it = list.iterator();
            while (true) {
                if (it.hasNext()) {
                    if (((Wallet) it.next()).isPrimaryWallet()) {
                        z = true;
                        break;
                    }
                } else {
                    break;
                }
            }
        }
        aKTSplashActivity.g0 = z;
        this.this$0.H();
    }
}
