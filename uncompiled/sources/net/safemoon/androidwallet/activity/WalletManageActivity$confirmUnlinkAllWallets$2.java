package net.safemoon.androidwallet.activity;

import android.content.DialogInterface;
import kotlin.jvm.internal.Lambda;
import net.safemoon.androidwallet.model.wallets.Wallet;

/* compiled from: WalletManageActivity.kt */
/* loaded from: classes2.dex */
public final class WalletManageActivity$confirmUnlinkAllWallets$2 extends Lambda implements tc1<DialogInterface, te4> {
    public final /* synthetic */ Wallet $wallet;
    public final /* synthetic */ WalletManageActivity this$0;

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public WalletManageActivity$confirmUnlinkAllWallets$2(WalletManageActivity walletManageActivity, Wallet wallet2) {
        super(1);
        this.this$0 = walletManageActivity;
        this.$wallet = wallet2;
    }

    @Override // defpackage.tc1
    public /* bridge */ /* synthetic */ te4 invoke(DialogInterface dialogInterface) {
        invoke2(dialogInterface);
        return te4.a;
    }

    /* renamed from: invoke  reason: avoid collision after fix types in other method */
    public final void invoke2(DialogInterface dialogInterface) {
        fs1.f(dialogInterface, "it");
        this.this$0.j1(this.$wallet);
    }
}
