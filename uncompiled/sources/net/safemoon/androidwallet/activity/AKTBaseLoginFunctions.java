package net.safemoon.androidwallet.activity;

import android.content.Intent;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import androidx.appcompat.app.ActionBar;
import com.AKT.anonymouskey.ui.login.AKTServerFunctions;
import java.lang.ref.WeakReference;
import java.util.Locale;
import java.util.Objects;
import kotlin.text.StringsKt__StringsKt;
import net.safemoon.androidwallet.MyApplicationClass;
import net.safemoon.androidwallet.R;
import net.safemoon.androidwallet.activity.AKTBaseLoginFunctions;
import net.safemoon.androidwallet.activity.AKTImportExistingWalletsActivity;
import net.safemoon.androidwallet.activity.StartWalletActivity;
import net.safemoon.androidwallet.dialogs.ProgressLoading;
import net.safemoon.androidwallet.viewmodels.AKTWebSocketHandlingViewModel;

/* compiled from: AKTBaseLoginFunctions.kt */
/* loaded from: classes2.dex */
public abstract class AKTBaseLoginFunctions extends AKTServerFunctions {
    public final sy1 m0 = new fj4(d53.b(AKTWebSocketHandlingViewModel.class), new AKTBaseLoginFunctions$special$$inlined$viewModels$default$2(this), new AKTBaseLoginFunctions$special$$inlined$viewModels$default$1(this));
    public boolean n0;

    public static final void l0(AKTBaseLoginFunctions aKTBaseLoginFunctions, String str) {
        fs1.f(aKTBaseLoginFunctions, "this$0");
        if (str == null) {
            return;
        }
        qy4 qy4Var = aKTBaseLoginFunctions.l0;
        fs1.e(qy4Var, "safeMoonppp");
        aKTBaseLoginFunctions.X(qy4Var, str);
        aKTBaseLoginFunctions.Q();
    }

    @Override // com.AKT.anonymouskey.ui.login.AKTServerFunctions
    public void Q() {
        j0().f();
    }

    @Override // com.AKT.anonymouskey.ui.login.AKTServerFunctions
    public void X(qy4 qy4Var, String str) {
        fs1.f(qy4Var, "safeMoonppp");
        if (W(str)) {
            return;
        }
        String[] v = i2.v(b30.a.v(this, str), "|");
        fs1.e(v, "parts");
        if (!(v.length == 0)) {
            String str2 = v[0];
            fs1.e(str2, "parts[0]");
            if (StringsKt__StringsKt.w0(str2, new String[]{"="}, false, 0, 6, null).size() >= 2) {
                String str3 = v[0];
                fs1.e(str3, "parts[0]");
                String str4 = (String) StringsKt__StringsKt.w0(str3, new String[]{"="}, false, 0, 6, null).get(1);
                Objects.requireNonNull(str4, "null cannot be cast to non-null type java.lang.String");
                String upperCase = str4.toUpperCase(Locale.ROOT);
                fs1.e(upperCase, "(this as java.lang.Strin….toUpperCase(Locale.ROOT)");
                int hashCode = upperCase.hashCode();
                if (hashCode == -1977771811) {
                    if (upperCase.equals("AKTAUTHUSER")) {
                        String w = i2.w(qy4Var, v);
                        bo3.o(this, "EMAIL", i2.p(qy4Var, "EMAIL"));
                        bo3.o(this, "U5", i2.p(qy4Var, "U5"));
                        s0(w);
                    }
                    super.X(qy4Var, str);
                } else if (hashCode == 325952731) {
                    if (upperCase.equals("AKTSERVERERROR")) {
                        Y(v, null);
                    }
                    super.X(qy4Var, str);
                } else {
                    if (hashCode == 789732987 && upperCase.equals("AKTSFMUSER")) {
                        if (r0(qy4Var, v)) {
                            i0(qy4Var, false);
                        }
                    }
                    super.X(qy4Var, str);
                }
            }
        }
        Q();
    }

    @Override // com.AKT.anonymouskey.ui.login.AKTServerFunctions
    public void Y(String[] strArr, String str) {
        ProgressLoading progressLoading = this.j0;
        if (progressLoading != null) {
            progressLoading.h();
        }
        bh.V(new WeakReference(this), null, Integer.valueOf((int) R.string.akt_error_invalid_credentials_message), 0, null, 26, null);
    }

    public final void i0(qy4 qy4Var, boolean z) {
        fs1.f(qy4Var, "safeMoonppp");
        bo3.m(this, "TIMENOW", i2.A());
        String g = qy4Var.g("EMAIL");
        String g2 = qy4Var.g("KA");
        String g3 = qy4Var.g("PBU5");
        String g4 = qy4Var.g("PB5K");
        String g5 = qy4Var.g("U5");
        String g6 = qy4Var.g("K5");
        String g7 = qy4Var.g("UK5");
        String g8 = qy4Var.g("PBHEX");
        bo3.o(this, "EMAIL", g);
        bo3.o(this, "KA", g2);
        bo3.o(this, "PBU5", g3);
        bo3.o(this, "U5", g5);
        bo3.o(this, "PB5K", g4);
        if (bo3.c(this, "TEMPU5")) {
            bo3.k(this, "TEMPU5");
        }
        if (bo3.c(this, "TEMPK5")) {
            bo3.k(this, "TEMPK5");
        }
        c0("K5", g6);
        c0("UK5", g7);
        if (g8.length() == 64) {
            String l = i2.l(g8);
            fs1.e(l, "mnemonic");
            p0(l, z);
            return;
        }
        Y(null, null);
    }

    public final AKTWebSocketHandlingViewModel j0() {
        return (AKTWebSocketHandlingViewModel) this.m0.getValue();
    }

    public void k0() {
        j0().h().observe(this, new tl2() { // from class: l
            @Override // defpackage.tl2
            public final void onChanged(Object obj) {
                AKTBaseLoginFunctions.l0(AKTBaseLoginFunctions.this, (String) obj);
            }
        });
    }

    public final boolean m0() {
        Bundle extras;
        Intent intent = getIntent();
        String str = null;
        if (intent != null && (extras = intent.getExtras()) != null) {
            str = extras.getString("bundle.KEY_SOURCE");
        }
        return fs1.b(str, "bundle.SOURCE_APP");
    }

    public final boolean n0() {
        Bundle extras;
        Intent intent = getIntent();
        String str = null;
        if (intent != null && (extras = intent.getExtras()) != null) {
            str = extras.getString("bundle.KEY_SOURCE");
        }
        return fs1.b(str, "bundle.SOURCE_LOGOUT");
    }

    public final boolean o0() {
        Bundle extras;
        Intent intent = getIntent();
        String str = null;
        if (intent != null && (extras = intent.getExtras()) != null) {
            str = extras.getString("bundle.KEY_SOURCE");
        }
        return fs1.b(str, "bundle.SOURCE_APP_FOR_RESULT");
    }

    @Override // com.AKT.anonymouskey.ui.login.AKTServerFunctions, net.safemoon.androidwallet.activity.common.BasicActivity, androidx.fragment.app.FragmentActivity, androidx.activity.ComponentActivity, androidx.core.app.ComponentActivity, android.app.Activity
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        j0().l();
        k0();
        ActionBar supportActionBar = getSupportActionBar();
        if (supportActionBar != null) {
            supportActionBar.l();
        }
        getWindow().addFlags(Integer.MIN_VALUE);
        getWindow().setStatusBarColor(m70.d(this, 17170445));
        getWindow().setBackgroundDrawable(new ColorDrawable(getColor(R.color.akt_night_background)));
        this.k0.p(new AKTBaseLoginFunctions$onCreate$1(this));
    }

    public final void p0(String str, boolean z) {
        if (o0()) {
            setResult(-1, new Intent());
            finish();
        } else if (m0() && !z) {
            finish();
        } else {
            if (this.n0) {
                AKTImportExistingWalletsActivity.a.b(AKTImportExistingWalletsActivity.v0, this, null, true, str, null, null, 50, null);
            } else if (z) {
                StartWalletActivity.a.b(StartWalletActivity.y0, this, false, str, false, 10, null);
            } else {
                MyApplicationClass.c().m0 = true;
                AKTHomeActivity.R0(this);
            }
            finish();
        }
    }

    /* JADX WARN: Removed duplicated region for block: B:20:0x004b  */
    /* JADX WARN: Removed duplicated region for block: B:22:? A[RETURN, SYNTHETIC] */
    /*
        Code decompiled incorrectly, please refer to instructions dump.
        To view partially-correct code enable 'Show inconsistent code' option in preferences
    */
    public final void q0(defpackage.qy4 r6, java.lang.String r7) {
        /*
            r5 = this;
            java.lang.String r0 = "U"
            java.lang.String r0 = r6.g(r0)
            java.lang.String r1 = "P"
            java.lang.String r1 = r6.g(r1)
            java.lang.String r2 = "KA"
            r6.c(r2, r7)
            java.lang.String r2 = "username"
            defpackage.fs1.e(r0, r2)
            int r2 = r0.length()
            r3 = 1
            r4 = 0
            if (r2 <= 0) goto L20
            r2 = r3
            goto L21
        L20:
            r2 = r4
        L21:
            if (r2 == 0) goto L38
            java.lang.String r2 = "password"
            defpackage.fs1.e(r1, r2)
            int r2 = r1.length()
            if (r2 <= 0) goto L30
            r2 = r3
            goto L31
        L30:
            r2 = r4
        L31:
            if (r2 == 0) goto L38
            byte[] r7 = defpackage.i2.c(r7, r0, r1)
            goto L39
        L38:
            r7 = 0
        L39:
            java.lang.String r0 = "PBHEX"
            java.lang.String r1 = "ffff"
            r6.c(r0, r1)
            if (r7 != 0) goto L44
        L42:
            r3 = r4
            goto L49
        L44:
            int r1 = r7.length
            r2 = 32
            if (r1 != r2) goto L42
        L49:
            if (r3 == 0) goto L83
            byte[] r1 = defpackage.ez4.c(r7)
            byte[] r1 = defpackage.lm.e(r1)
            java.lang.String r1 = defpackage.ay1.f(r1)
            byte[] r2 = defpackage.i2.z(r7)
            java.lang.String r3 = defpackage.ay1.f(r2)
            defpackage.ay1.g(r3)
            java.lang.String r7 = defpackage.ay1.f(r7)
            java.lang.String r7 = defpackage.ay1.g(r7)
            byte[] r2 = defpackage.la3.a(r2)
            java.lang.String r2 = defpackage.ay1.f(r2)
            java.lang.String r2 = defpackage.lm.d(r2)
            java.lang.String r3 = "PBU5"
            r6.c(r3, r1)
            java.lang.String r1 = "PB5K"
            r6.c(r1, r2)
            r6.c(r0, r7)
        L83:
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: net.safemoon.androidwallet.activity.AKTBaseLoginFunctions.q0(qy4, java.lang.String):void");
    }

    /* JADX WARN: Removed duplicated region for block: B:102:0x00d5 A[EDGE_INSN: B:102:0x00d5->B:52:0x00d5 ?: BREAK  , SYNTHETIC] */
    /* JADX WARN: Removed duplicated region for block: B:53:0x00da A[LOOP:0: B:5:0x001c->B:53:0x00da, LOOP_END] */
    /* JADX WARN: Removed duplicated region for block: B:66:0x0125  */
    /* JADX WARN: Removed duplicated region for block: B:69:0x0141  */
    /* JADX WARN: Removed duplicated region for block: B:71:0x0144  */
    /* JADX WARN: Removed duplicated region for block: B:78:0x0154  */
    /* JADX WARN: Removed duplicated region for block: B:84:0x0160  */
    /* JADX WARN: Removed duplicated region for block: B:87:0x0169  */
    /* JADX WARN: Removed duplicated region for block: B:89:0x016f  */
    /*
        Code decompiled incorrectly, please refer to instructions dump.
        To view partially-correct code enable 'Show inconsistent code' option in preferences
    */
    public final boolean r0(defpackage.qy4 r25, java.lang.String[] r26) {
        /*
            Method dump skipped, instructions count: 434
            To view this dump change 'Code comments level' option to 'DEBUG'
        */
        throw new UnsupportedOperationException("Method not decompiled: net.safemoon.androidwallet.activity.AKTBaseLoginFunctions.r0(qy4, java.lang.String[]):boolean");
    }

    public void s0(String str) {
        if (str == null) {
            return;
        }
        j0().k(str);
    }
}
