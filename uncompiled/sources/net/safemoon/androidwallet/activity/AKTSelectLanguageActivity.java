package net.safemoon.androidwallet.activity;

import android.content.Context;
import android.content.Intent;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import androidx.appcompat.app.ActionBar;
import androidx.recyclerview.widget.LinearLayoutManager;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.Objects;
import kotlin.text.StringsKt__StringsKt;
import net.safemoon.androidwallet.R;
import net.safemoon.androidwallet.activity.AKTSelectLanguageActivity;
import net.safemoon.androidwallet.activity.AKTSelectLanguageActivity$defaultLanguageAdapter$2;
import net.safemoon.androidwallet.activity.common.BasicActivity;
import net.safemoon.androidwallet.model.common.LanguageItem;

/* compiled from: AKTSelectLanguageActivity.kt */
/* loaded from: classes2.dex */
public final class AKTSelectLanguageActivity extends BasicActivity {
    public static final a m0 = new a(null);
    public final sy1 j0 = zy1.a(new AKTSelectLanguageActivity$binding$2(this));
    public final sy1 k0 = zy1.a(new AKTSelectLanguageActivity$defaultLanguageAdapter$2(this));
    public String l0 = "";

    /* compiled from: AKTSelectLanguageActivity.kt */
    /* loaded from: classes2.dex */
    public static final class a {
        public a() {
        }

        public /* synthetic */ a(qi0 qi0Var) {
            this();
        }

        public final void a(Context context) {
            fs1.f(context, "context");
            context.startActivity(new Intent(context, AKTSelectLanguageActivity.class));
        }
    }

    /* compiled from: AKTSelectLanguageActivity.kt */
    /* loaded from: classes2.dex */
    public static final class b implements TextWatcher {
        public b() {
        }

        @Override // android.text.TextWatcher
        public void afterTextChanged(Editable editable) {
            AKTSelectLanguageActivity aKTSelectLanguageActivity = AKTSelectLanguageActivity.this;
            CharSequence charSequence = editable;
            if (editable == null) {
                charSequence = "";
            }
            aKTSelectLanguageActivity.l0 = charSequence.toString();
            AKTSelectLanguageActivity aKTSelectLanguageActivity2 = AKTSelectLanguageActivity.this;
            List<LanguageItem> S = aKTSelectLanguageActivity2.S(aKTSelectLanguageActivity2.l0);
            AKTSelectLanguageActivity.this.Q().e.setVisibility(S.size() == 0 ? 0 : 8);
            AKTSelectLanguageActivity.this.R().g(S);
        }

        @Override // android.text.TextWatcher
        public void beforeTextChanged(CharSequence charSequence, int i, int i2, int i3) {
        }

        @Override // android.text.TextWatcher
        public void onTextChanged(CharSequence charSequence, int i, int i2, int i3) {
        }
    }

    public static final void T(AKTSelectLanguageActivity aKTSelectLanguageActivity, View view) {
        fs1.f(aKTSelectLanguageActivity, "this$0");
        aKTSelectLanguageActivity.onBackPressed();
    }

    public final x6 Q() {
        return (x6) this.j0.getValue();
    }

    public final AKTSelectLanguageActivity$defaultLanguageAdapter$2.a R() {
        return (AKTSelectLanguageActivity$defaultLanguageAdapter$2.a) this.k0.getValue();
    }

    public final List<LanguageItem> S(String str) {
        List<LanguageItem> b2 = ly1.a.b();
        ArrayList arrayList = new ArrayList();
        for (Object obj : b2) {
            LanguageItem languageItem = (LanguageItem) obj;
            String string = getString(languageItem.getTitleResId());
            fs1.e(string, "getString(it.titleResId)");
            String string2 = getString(languageItem.getRegionResId());
            fs1.e(string2, "getString(it.regionResId)");
            Objects.requireNonNull(str, "null cannot be cast to non-null type kotlin.CharSequence");
            boolean z = true;
            if (!(StringsKt__StringsKt.K0(str).toString().length() == 0)) {
                Locale locale = Locale.ROOT;
                String lowerCase = string.toLowerCase(locale);
                fs1.e(lowerCase, "(this as java.lang.Strin….toLowerCase(Locale.ROOT)");
                String lowerCase2 = str.toLowerCase(locale);
                fs1.e(lowerCase2, "(this as java.lang.Strin….toLowerCase(Locale.ROOT)");
                Objects.requireNonNull(lowerCase2, "null cannot be cast to non-null type kotlin.CharSequence");
                if (!StringsKt__StringsKt.M(lowerCase, StringsKt__StringsKt.K0(lowerCase2).toString(), false, 2, null)) {
                    String languageCode = languageItem.getLanguageCode();
                    Objects.requireNonNull(languageCode, "null cannot be cast to non-null type java.lang.String");
                    String lowerCase3 = languageCode.toLowerCase(locale);
                    fs1.e(lowerCase3, "(this as java.lang.Strin….toLowerCase(Locale.ROOT)");
                    String lowerCase4 = str.toLowerCase(locale);
                    fs1.e(lowerCase4, "(this as java.lang.Strin….toLowerCase(Locale.ROOT)");
                    Objects.requireNonNull(lowerCase4, "null cannot be cast to non-null type kotlin.CharSequence");
                    if (!StringsKt__StringsKt.M(lowerCase3, StringsKt__StringsKt.K0(lowerCase4).toString(), false, 2, null)) {
                        String lowerCase5 = string2.toLowerCase(locale);
                        fs1.e(lowerCase5, "(this as java.lang.Strin….toLowerCase(Locale.ROOT)");
                        String lowerCase6 = str.toLowerCase(locale);
                        fs1.e(lowerCase6, "(this as java.lang.Strin….toLowerCase(Locale.ROOT)");
                        Objects.requireNonNull(lowerCase6, "null cannot be cast to non-null type kotlin.CharSequence");
                        if (!StringsKt__StringsKt.M(lowerCase5, StringsKt__StringsKt.K0(lowerCase6).toString(), false, 2, null)) {
                            z = false;
                        }
                    }
                }
            }
            if (z) {
                arrayList.add(obj);
            }
        }
        return j20.m0(arrayList);
    }

    @Override // net.safemoon.androidwallet.activity.common.BasicActivity, androidx.fragment.app.FragmentActivity, androidx.activity.ComponentActivity, androidx.core.app.ComponentActivity, android.app.Activity
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        setContentView(Q().b());
        ActionBar supportActionBar = getSupportActionBar();
        if (supportActionBar != null) {
            supportActionBar.l();
        }
        getWindow().addFlags(Integer.MIN_VALUE);
        getWindow().setStatusBarColor(m70.d(this, 17170445));
        getWindow().setBackgroundDrawable(new ColorDrawable(getColor(R.color.akt_night_background)));
    }

    @Override // net.safemoon.androidwallet.activity.common.BasicActivity, androidx.appcompat.app.AppCompatActivity, android.app.Activity
    public void onPostCreate(Bundle bundle) {
        super.onPostCreate(bundle);
        x6 Q = Q();
        Q.d.e.setText(R.string.default_language);
        Q.d.b.setOnClickListener(new View.OnClickListener() { // from class: b3
            @Override // android.view.View.OnClickListener
            public final void onClick(View view) {
                AKTSelectLanguageActivity.T(AKTSelectLanguageActivity.this, view);
            }
        });
        Q.c.setLayoutManager(new LinearLayoutManager(this, 1, false));
        Q.c.setAdapter(R());
        Q.b.addTextChangedListener(new b());
        R().g(S(this.l0));
    }
}
