package net.safemoon.androidwallet.activity;

import android.content.Intent;
import java.io.Serializable;
import java.util.Objects;
import kotlin.jvm.internal.Lambda;
import net.safemoon.androidwallet.common.TokenType;

/* compiled from: ChooseNetworkActivity.kt */
/* loaded from: classes2.dex */
public final class ChooseNetworkActivity$chainNetworkAdapter$2 extends Lambda implements rc1<ky> {
    public final /* synthetic */ ChooseNetworkActivity this$0;

    /* compiled from: ChooseNetworkActivity.kt */
    /* renamed from: net.safemoon.androidwallet.activity.ChooseNetworkActivity$chainNetworkAdapter$2$1  reason: invalid class name */
    /* loaded from: classes2.dex */
    public static final class AnonymousClass1 extends Lambda implements tc1<TokenType, te4> {
        public final /* synthetic */ ChooseNetworkActivity this$0;

        /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
        public AnonymousClass1(ChooseNetworkActivity chooseNetworkActivity) {
            super(1);
            this.this$0 = chooseNetworkActivity;
        }

        @Override // defpackage.tc1
        public /* bridge */ /* synthetic */ te4 invoke(TokenType tokenType) {
            invoke2(tokenType);
            return te4.a;
        }

        /* renamed from: invoke  reason: avoid collision after fix types in other method */
        public final void invoke2(TokenType tokenType) {
            fs1.f(tokenType, "it");
            ChooseNetworkActivity chooseNetworkActivity = this.this$0;
            Intent intent = new Intent();
            intent.putExtra("ARG_SELECTED_TOKEN_NETWORK", tokenType);
            te4 te4Var = te4.a;
            chooseNetworkActivity.setResult(-1, intent);
            this.this$0.finish();
        }
    }

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public ChooseNetworkActivity$chainNetworkAdapter$2(ChooseNetworkActivity chooseNetworkActivity) {
        super(0);
        this.this$0 = chooseNetworkActivity;
    }

    @Override // defpackage.rc1
    public final ky invoke() {
        Serializable serializableExtra = this.this$0.getIntent().getSerializableExtra("ARG_SELECTED_TOKEN_NETWORK");
        Objects.requireNonNull(serializableExtra, "null cannot be cast to non-null type net.safemoon.androidwallet.common.TokenType");
        return new ky((TokenType) serializableExtra, new AnonymousClass1(this.this$0));
    }
}
