package net.safemoon.androidwallet.activity;

import kotlin.jvm.internal.Lambda;

/* compiled from: StartWalletActivity.kt */
/* loaded from: classes2.dex */
public final class StartWalletActivity$binding$2 extends Lambda implements rc1<a8> {
    public final /* synthetic */ StartWalletActivity this$0;

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public StartWalletActivity$binding$2(StartWalletActivity startWalletActivity) {
        super(0);
        this.this$0 = startWalletActivity;
    }

    /* JADX WARN: Can't rename method to resolve collision */
    @Override // defpackage.rc1
    public final a8 invoke() {
        return a8.c(this.this$0.getLayoutInflater());
    }
}
