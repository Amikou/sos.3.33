package net.safemoon.androidwallet.activity;

import java.util.List;
import kotlin.jvm.internal.Lambda;
import net.safemoon.androidwallet.model.wallets.Wallet;

/* compiled from: StartWalletActivity.kt */
/* loaded from: classes2.dex */
public final class StartWalletActivity$checkSyncAllWalletsByDefault$1 extends Lambda implements tc1<List<? extends Wallet>, te4> {
    public final /* synthetic */ StringBuilder $blobBuilder;
    public final /* synthetic */ StartWalletActivity this$0;

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public StartWalletActivity$checkSyncAllWalletsByDefault$1(StartWalletActivity startWalletActivity, StringBuilder sb) {
        super(1);
        this.this$0 = startWalletActivity;
        this.$blobBuilder = sb;
    }

    @Override // defpackage.tc1
    public /* bridge */ /* synthetic */ te4 invoke(List<? extends Wallet> list) {
        invoke2((List<Wallet>) list);
        return te4.a;
    }

    /* JADX WARN: Removed duplicated region for block: B:28:0x00b2  */
    /* JADX WARN: Removed duplicated region for block: B:31:0x00c5  */
    /* renamed from: invoke  reason: avoid collision after fix types in other method */
    /*
        Code decompiled incorrectly, please refer to instructions dump.
        To view partially-correct code enable 'Show inconsistent code' option in preferences
    */
    public final void invoke2(java.util.List<net.safemoon.androidwallet.model.wallets.Wallet> r10) {
        /*
            r9 = this;
            java.lang.String r0 = "list"
            defpackage.fs1.f(r10, r0)
            int r0 = r10.size()
            r1 = 1
            if (r0 <= r1) goto Lf4
            int r0 = r10.size()
            int r0 = r0 + (-1)
            if (r0 < 0) goto Ld2
            r2 = 0
            r3 = r2
        L16:
            int r4 = r3 + 1
            java.lang.Object r5 = r10.get(r3)
            net.safemoon.androidwallet.model.wallets.Wallet r5 = (net.safemoon.androidwallet.model.wallets.Wallet) r5
            boolean r6 = r5.isPrimaryWallet()
            if (r6 == 0) goto L26
            goto Lcc
        L26:
            net.safemoon.androidwallet.activity.StartWalletActivity r6 = r9.this$0
            java.util.Map r6 = net.safemoon.androidwallet.activity.StartWalletActivity.r0(r6)
            java.lang.String r7 = r5.getAddress()
            boolean r6 = r6.containsKey(r7)
            if (r6 == 0) goto L81
            java.lang.String r6 = r5.getName()
            net.safemoon.androidwallet.activity.StartWalletActivity r7 = r9.this$0
            java.util.Map r7 = net.safemoon.androidwallet.activity.StartWalletActivity.r0(r7)
            java.lang.String r8 = r5.getAddress()
            java.lang.Object r7 = r7.get(r8)
            boolean r6 = defpackage.fs1.b(r6, r7)
            if (r6 != 0) goto L81
            net.safemoon.androidwallet.activity.StartWalletActivity r6 = r9.this$0
            net.safemoon.androidwallet.viewmodels.MultiWalletViewModel r6 = net.safemoon.androidwallet.activity.StartWalletActivity.t0(r6)
            net.safemoon.androidwallet.activity.StartWalletActivity r7 = r9.this$0
            java.util.Map r7 = net.safemoon.androidwallet.activity.StartWalletActivity.r0(r7)
            java.lang.String r8 = r5.getAddress()
            java.lang.Object r7 = r7.get(r8)
            defpackage.fs1.d(r7)
            java.lang.String r7 = (java.lang.String) r7
            r8 = 0
            r6.D(r5, r7, r1, r8)
            net.safemoon.androidwallet.activity.StartWalletActivity r6 = r9.this$0
            java.util.Map r6 = net.safemoon.androidwallet.activity.StartWalletActivity.r0(r6)
            java.lang.String r7 = r5.getAddress()
            java.lang.Object r6 = r6.get(r7)
            java.lang.String r6 = (java.lang.String) r6
            if (r6 != 0) goto L7e
            goto L81
        L7e:
            r5.setName(r6)
        L81:
            net.safemoon.androidwallet.activity.StartWalletActivity r6 = r9.this$0
            java.lang.String r7 = r5.getPrivateKey()
            java.lang.String r6 = defpackage.w.d(r6, r7)
            java.lang.StringBuilder r7 = r9.$blobBuilder
            java.lang.String r8 = r5.getName()
            r7.append(r8)
            java.lang.String r8 = ","
            r7.append(r8)
            r7.append(r6)
            java.lang.String r6 = r5.getKA()
            if (r6 != 0) goto La4
        La2:
            r6 = r2
            goto Lb0
        La4:
            int r6 = r6.length()
            if (r6 <= 0) goto Lac
            r6 = r1
            goto Lad
        Lac:
            r6 = r2
        Lad:
            if (r6 != r1) goto La2
            r6 = r1
        Lb0:
            if (r6 == 0) goto Lbe
            java.lang.StringBuilder r6 = r9.$blobBuilder
            r6.append(r8)
            java.lang.String r5 = r5.getKA()
            r6.append(r5)
        Lbe:
            int r5 = r10.size()
            int r5 = r5 - r1
            if (r3 >= r5) goto Lcc
            java.lang.StringBuilder r3 = r9.$blobBuilder
            java.lang.String r5 = "|"
            r3.append(r5)
        Lcc:
            if (r4 <= r0) goto Lcf
            goto Ld2
        Lcf:
            r3 = r4
            goto L16
        Ld2:
            java.lang.StringBuilder r10 = r9.$blobBuilder
            java.lang.String r10 = r10.toString()
            java.lang.String r0 = "blobBuilder.toString()"
            defpackage.fs1.e(r10, r0)
            net.safemoon.androidwallet.activity.StartWalletActivity r0 = r9.this$0
            java.lang.String r10 = defpackage.w.b(r0, r10)
            net.safemoon.androidwallet.activity.StartWalletActivity r0 = r9.this$0
            java.lang.String r1 = "encryptBlob"
            defpackage.fs1.e(r10, r1)
            java.lang.String r10 = net.safemoon.androidwallet.activity.StartWalletActivity.l0(r0, r10)
            net.safemoon.androidwallet.activity.StartWalletActivity r0 = r9.this$0
            r0.U0(r10)
            goto Lf9
        Lf4:
            net.safemoon.androidwallet.activity.StartWalletActivity r10 = r9.this$0
            net.safemoon.androidwallet.activity.StartWalletActivity.j0(r10)
        Lf9:
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: net.safemoon.androidwallet.activity.StartWalletActivity$checkSyncAllWalletsByDefault$1.invoke2(java.util.List):void");
    }
}
