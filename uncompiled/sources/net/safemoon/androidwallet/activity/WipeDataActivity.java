package net.safemoon.androidwallet.activity;

import android.annotation.SuppressLint;
import android.app.Dialog;
import android.content.Context;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.view.View;
import android.view.Window;
import android.widget.TextView;
import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity;
import net.safemoon.androidwallet.R;
import net.safemoon.androidwallet.activity.WipeDataActivity;

/* compiled from: WipeDataActivity.kt */
/* loaded from: classes2.dex */
public final class WipeDataActivity extends AppCompatActivity {
    public final sy1 a = zy1.a(new WipeDataActivity$mBinding$2(this));
    public int f0 = 5;

    /* compiled from: WipeDataActivity.kt */
    /* loaded from: classes2.dex */
    public static final class a extends CountDownTimer {
        public final /* synthetic */ TextView a;
        public final /* synthetic */ WipeDataActivity b;
        public final /* synthetic */ Dialog c;

        /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
        public a(TextView textView, WipeDataActivity wipeDataActivity, Dialog dialog) {
            super(5000L, 1000L);
            this.a = textView;
            this.b = wipeDataActivity;
            this.c = dialog;
        }

        @Override // android.os.CountDownTimer
        public void onFinish() {
            this.b.G(this.c);
        }

        @Override // android.os.CountDownTimer
        @SuppressLint({"SetTextI18n"})
        public void onTick(long j) {
            this.a.setText(fs1.l("", Integer.valueOf(this.b.z())));
            WipeDataActivity wipeDataActivity = this.b;
            wipeDataActivity.D(wipeDataActivity.z() - 1);
        }
    }

    public static final void A(WipeDataActivity wipeDataActivity, View view) {
        fs1.f(wipeDataActivity, "this$0");
        wipeDataActivity.onBackPressed();
    }

    public static final void B(WipeDataActivity wipeDataActivity, View view) {
        fs1.f(wipeDataActivity, "this$0");
        wipeDataActivity.onBackPressed();
    }

    public static final void C(WipeDataActivity wipeDataActivity, View view) {
        fs1.f(wipeDataActivity, "this$0");
        wipeDataActivity.E();
    }

    public static final void F(CountDownTimer countDownTimer, WipeDataActivity wipeDataActivity, Dialog dialog, View view) {
        fs1.f(countDownTimer, "$countDownTimer");
        fs1.f(wipeDataActivity, "this$0");
        fs1.f(dialog, "$dialog");
        countDownTimer.cancel();
        wipeDataActivity.D(5);
        dialog.dismiss();
    }

    public final void D(int i) {
        this.f0 = i;
    }

    public final void E() {
        final Dialog dialog = new Dialog(this);
        dialog.requestWindowFeature(1);
        dialog.setCancelable(false);
        dialog.setContentView(R.layout.dialog_wipe);
        Window window = dialog.getWindow();
        if (window != null) {
            window.setBackgroundDrawable(new ColorDrawable(0));
        }
        dialog.show();
        final a aVar = new a((TextView) dialog.findViewById(R.id.textView19), this, dialog);
        aVar.start();
        ((TextView) dialog.findViewById(R.id.tv_abort)).setOnClickListener(new View.OnClickListener() { // from class: mp4
            @Override // android.view.View.OnClickListener
            public final void onClick(View view) {
                WipeDataActivity.F(aVar, this, dialog, view);
            }
        });
    }

    public final void G(Dialog dialog) {
        Context applicationContext = getApplicationContext();
        fs1.e(applicationContext, "applicationContext");
        lp4.a(applicationContext).execute();
        bo3.a(this);
        jv0.a(this);
        if (dialog == null) {
            return;
        }
        dialog.dismiss();
        ly1 ly1Var = ly1.a;
        ly1Var.c(this, ly1Var.a());
        AKTConfigurationsActivity.l0.b(this);
        finish();
    }

    @Override // androidx.fragment.app.FragmentActivity, androidx.activity.ComponentActivity, androidx.core.app.ComponentActivity, android.app.Activity
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        setContentView(y().b());
        ActionBar supportActionBar = getSupportActionBar();
        if (supportActionBar != null) {
            supportActionBar.l();
        }
        getWindow().addFlags(Integer.MIN_VALUE);
        getWindow().setStatusBarColor(m70.d(this, 17170445));
        getWindow().setBackgroundDrawable(new ColorDrawable(getColor(R.color.akt_night_background)));
    }

    @Override // androidx.appcompat.app.AppCompatActivity, android.app.Activity
    public void onPostCreate(Bundle bundle) {
        super.onPostCreate(bundle);
        e8 y = y();
        y.d.e.setText(R.string.akt_reset_wallets_title);
        y.d.b.setVisibility(0);
        y.d.b.setOnClickListener(new View.OnClickListener() { // from class: np4
            @Override // android.view.View.OnClickListener
            public final void onClick(View view) {
                WipeDataActivity.A(WipeDataActivity.this, view);
            }
        });
        y.b.setOnClickListener(new View.OnClickListener() { // from class: pp4
            @Override // android.view.View.OnClickListener
            public final void onClick(View view) {
                WipeDataActivity.B(WipeDataActivity.this, view);
            }
        });
        y.c.setOnClickListener(new View.OnClickListener() { // from class: op4
            @Override // android.view.View.OnClickListener
            public final void onClick(View view) {
                WipeDataActivity.C(WipeDataActivity.this, view);
            }
        });
    }

    public final e8 y() {
        return (e8) this.a.getValue();
    }

    public final int z() {
        return this.f0;
    }
}
