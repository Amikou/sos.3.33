package net.safemoon.androidwallet.activity;

import android.os.Handler;
import android.os.Looper;
import androidx.fragment.app.FragmentManager;
import kotlin.jvm.internal.Lambda;
import net.safemoon.androidwallet.R;
import net.safemoon.androidwallet.activity.AKTImportExistingWalletsActivity;
import net.safemoon.androidwallet.activity.AKTImportExistingWalletsActivity$onPostCreate$1$4$1;
import net.safemoon.androidwallet.dialogs.ProgressLoading;

/* compiled from: AKTImportExistingWalletsActivity.kt */
/* loaded from: classes2.dex */
public final class AKTImportExistingWalletsActivity$onPostCreate$1$4$1 extends Lambda implements rc1<te4> {
    public final /* synthetic */ AKTImportExistingWalletsActivity this$0;

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public AKTImportExistingWalletsActivity$onPostCreate$1$4$1(AKTImportExistingWalletsActivity aKTImportExistingWalletsActivity) {
        super(0);
        this.this$0 = aKTImportExistingWalletsActivity;
    }

    public static final void b(AKTImportExistingWalletsActivity aKTImportExistingWalletsActivity) {
        fs1.f(aKTImportExistingWalletsActivity, "this$0");
        aKTImportExistingWalletsActivity.N0();
    }

    @Override // defpackage.rc1
    public /* bridge */ /* synthetic */ te4 invoke() {
        invoke2();
        return te4.a;
    }

    @Override // defpackage.rc1
    /* renamed from: invoke  reason: avoid collision after fix types in other method */
    public final void invoke2() {
        ProgressLoading progressLoading;
        AKTImportExistingWalletsActivity aKTImportExistingWalletsActivity = this.this$0;
        ProgressLoading.a aVar = ProgressLoading.y0;
        String string = aKTImportExistingWalletsActivity.getString(R.string.loading);
        fs1.e(string, "getString(R.string.loading)");
        aKTImportExistingWalletsActivity.j0 = aVar.a(false, string, "");
        progressLoading = this.this$0.j0;
        if (progressLoading != null) {
            FragmentManager supportFragmentManager = this.this$0.getSupportFragmentManager();
            fs1.e(supportFragmentManager, "supportFragmentManager");
            progressLoading.A(supportFragmentManager);
        }
        Handler handler = new Handler(Looper.getMainLooper());
        final AKTImportExistingWalletsActivity aKTImportExistingWalletsActivity2 = this.this$0;
        handler.postDelayed(new Runnable() { // from class: x0
            @Override // java.lang.Runnable
            public final void run() {
                AKTImportExistingWalletsActivity$onPostCreate$1$4$1.b(AKTImportExistingWalletsActivity.this);
            }
        }, 500L);
    }
}
