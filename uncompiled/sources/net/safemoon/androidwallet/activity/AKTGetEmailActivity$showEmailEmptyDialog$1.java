package net.safemoon.androidwallet.activity;

import android.os.Handler;
import android.os.Looper;
import android.widget.EditText;
import kotlin.jvm.internal.Lambda;
import net.safemoon.androidwallet.activity.AKTGetEmailActivity$showEmailEmptyDialog$1;

/* compiled from: AKTGetEmailActivity.kt */
/* loaded from: classes2.dex */
public final class AKTGetEmailActivity$showEmailEmptyDialog$1 extends Lambda implements rc1<te4> {
    public final /* synthetic */ AKTGetEmailActivity this$0;

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public AKTGetEmailActivity$showEmailEmptyDialog$1(AKTGetEmailActivity aKTGetEmailActivity) {
        super(0);
        this.this$0 = aKTGetEmailActivity;
    }

    public static final void b(EditText editText) {
        fs1.f(editText, "$it");
        pg4.g(editText);
    }

    @Override // defpackage.rc1
    public /* bridge */ /* synthetic */ te4 invoke() {
        invoke2();
        return te4.a;
    }

    @Override // defpackage.rc1
    /* renamed from: invoke  reason: avoid collision after fix types in other method */
    public final void invoke2() {
        e7 n0;
        n0 = this.this$0.n0();
        final EditText editText = n0.e.getEditText();
        if (editText == null) {
            return;
        }
        new Handler(Looper.getMainLooper()).postDelayed(new Runnable() { // from class: c0
            @Override // java.lang.Runnable
            public final void run() {
                AKTGetEmailActivity$showEmailEmptyDialog$1.b(editText);
            }
        }, 200L);
    }
}
