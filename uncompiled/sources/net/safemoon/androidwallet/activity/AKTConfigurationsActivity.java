package net.safemoon.androidwallet.activity;

import android.content.Context;
import android.content.Intent;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.view.View;
import androidx.appcompat.app.ActionBar;
import androidx.appcompat.widget.AppCompatTextView;
import java.util.Iterator;
import net.safemoon.androidwallet.R;
import net.safemoon.androidwallet.activity.AKTConfigurationsActivity;
import net.safemoon.androidwallet.activity.common.BasicActivity;
import net.safemoon.androidwallet.model.common.LanguageItem;
import net.safemoon.androidwallet.model.fiat.gson.Fiat;
import net.safemoon.androidwallet.viewmodels.SelectFiatViewModel;

/* compiled from: AKTConfigurationsActivity.kt */
/* loaded from: classes2.dex */
public final class AKTConfigurationsActivity extends BasicActivity {
    public static final a l0 = new a(null);
    public final sy1 j0 = zy1.a(new AKTConfigurationsActivity$binding$2(this));
    public final sy1 k0 = new fj4(d53.b(SelectFiatViewModel.class), new AKTConfigurationsActivity$special$$inlined$viewModels$2(this), new AKTConfigurationsActivity$selectFiatViewModel$2(this));

    /* compiled from: AKTConfigurationsActivity.kt */
    /* loaded from: classes2.dex */
    public static final class a {
        public a() {
        }

        public /* synthetic */ a(qi0 qi0Var) {
            this();
        }

        public final void a(Context context) {
            fs1.f(context, "context");
            context.startActivity(new Intent(context, AKTConfigurationsActivity.class));
        }

        public final void b(Context context) {
            fs1.f(context, "context");
            Intent intent = new Intent(context, AKTConfigurationsActivity.class);
            intent.setFlags(268468224);
            context.startActivity(intent);
        }
    }

    public static final void P(AKTConfigurationsActivity aKTConfigurationsActivity, View view) {
        fs1.f(aKTConfigurationsActivity, "this$0");
        AKTActivity.k0.a(aKTConfigurationsActivity);
    }

    public static final void Q(AKTConfigurationsActivity aKTConfigurationsActivity, View view) {
        fs1.f(aKTConfigurationsActivity, "this$0");
        AKTSelectLanguageActivity.m0.a(aKTConfigurationsActivity);
    }

    public static final void R(AKTConfigurationsActivity aKTConfigurationsActivity, View view) {
        fs1.f(aKTConfigurationsActivity, "this$0");
        AKTSelectCurrencyActivity.o0.a(aKTConfigurationsActivity);
    }

    public final r6 N() {
        return (r6) this.j0.getValue();
    }

    public final SelectFiatViewModel O() {
        return (SelectFiatViewModel) this.k0.getValue();
    }

    @Override // androidx.activity.ComponentActivity, android.app.Activity
    public void onBackPressed() {
    }

    @Override // net.safemoon.androidwallet.activity.common.BasicActivity, androidx.fragment.app.FragmentActivity, androidx.activity.ComponentActivity, androidx.core.app.ComponentActivity, android.app.Activity
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        setContentView(N().b());
        ActionBar supportActionBar = getSupportActionBar();
        if (supportActionBar != null) {
            supportActionBar.l();
        }
        getWindow().addFlags(Integer.MIN_VALUE);
        getWindow().setStatusBarColor(m70.d(this, 17170445));
        getWindow().setBackgroundDrawable(new ColorDrawable(getColor(R.color.akt_night_background)));
        Fiat.Companion companion = Fiat.Companion;
        String j = bo3.j(this, "DEFAULT_FIAT", companion.getDEFAULT_CURRENCY_STRING());
        fs1.e(j, "getString(\n            t…CURRENCY_STRING\n        )");
        SelectFiatViewModel.n(O(), companion.to(j), false, null, 6, null);
    }

    @Override // net.safemoon.androidwallet.activity.common.BasicActivity, androidx.appcompat.app.AppCompatActivity, android.app.Activity
    public void onPostCreate(Bundle bundle) {
        Object obj;
        super.onPostCreate(bundle);
        r6 N = N();
        N.b.setOnClickListener(new View.OnClickListener() { // from class: v
            @Override // android.view.View.OnClickListener
            public final void onClick(View view) {
                AKTConfigurationsActivity.P(AKTConfigurationsActivity.this, view);
            }
        });
        N.d.setOnClickListener(new View.OnClickListener() { // from class: t
            @Override // android.view.View.OnClickListener
            public final void onClick(View view) {
                AKTConfigurationsActivity.Q(AKTConfigurationsActivity.this, view);
            }
        });
        N.c.setOnClickListener(new View.OnClickListener() { // from class: u
            @Override // android.view.View.OnClickListener
            public final void onClick(View view) {
                AKTConfigurationsActivity.R(AKTConfigurationsActivity.this, view);
            }
        });
        String a2 = do3.a.a(this);
        Iterator<T> it = ly1.a.b().iterator();
        while (true) {
            if (!it.hasNext()) {
                obj = null;
                break;
            }
            obj = it.next();
            if (fs1.b(((LanguageItem) obj).getLanguageCode(), a2)) {
                break;
            }
        }
        LanguageItem languageItem = (LanguageItem) obj;
        if (languageItem == null) {
            languageItem = new LanguageItem("en", R.string.language_english, R.string.language_english_location);
        }
        N.f.setText(languageItem.getTitleResId());
    }

    @Override // androidx.fragment.app.FragmentActivity, android.app.Activity
    public void onResume() {
        super.onResume();
        Fiat.Companion companion = Fiat.Companion;
        String j = bo3.j(this, "DEFAULT_FIAT", companion.getDEFAULT_CURRENCY_STRING());
        fs1.e(j, "getString(\n             …ENCY_STRING\n            )");
        Fiat fiat = companion.to(j);
        AppCompatTextView appCompatTextView = N().e;
        appCompatTextView.setText(fiat.getSymbol() + " - " + ((Object) fiat.getName()));
    }
}
