package net.safemoon.androidwallet.activity;

import kotlin.jvm.internal.Lambda;
import net.safemoon.androidwallet.model.wallets.Wallet;
import net.safemoon.androidwallet.viewmodels.MultiWalletViewModel;

/* compiled from: SwitchWalletActivity.kt */
/* loaded from: classes2.dex */
public final class SwitchWalletActivity$walletAdapter$2 extends Lambda implements rc1<xn4> {
    public final /* synthetic */ SwitchWalletActivity this$0;

    /* compiled from: SwitchWalletActivity.kt */
    /* renamed from: net.safemoon.androidwallet.activity.SwitchWalletActivity$walletAdapter$2$1  reason: invalid class name */
    /* loaded from: classes2.dex */
    public static final class AnonymousClass1 extends Lambda implements tc1<Wallet, te4> {
        public final /* synthetic */ SwitchWalletActivity this$0;

        /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
        public AnonymousClass1(SwitchWalletActivity switchWalletActivity) {
            super(1);
            this.this$0 = switchWalletActivity;
        }

        @Override // defpackage.tc1
        public /* bridge */ /* synthetic */ te4 invoke(Wallet wallet2) {
            invoke2(wallet2);
            return te4.a;
        }

        /* renamed from: invoke  reason: avoid collision after fix types in other method */
        public final void invoke2(Wallet wallet2) {
            MultiWalletViewModel Q;
            fs1.f(wallet2, "wallet");
            Q = this.this$0.Q();
            Q.A(wallet2);
            AKTHomeActivity.R0(this.this$0);
        }
    }

    /* compiled from: SwitchWalletActivity.kt */
    /* renamed from: net.safemoon.androidwallet.activity.SwitchWalletActivity$walletAdapter$2$2  reason: invalid class name */
    /* loaded from: classes2.dex */
    public static final class AnonymousClass2 extends Lambda implements tc1<Wallet, te4> {
        public final /* synthetic */ SwitchWalletActivity this$0;

        /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
        public AnonymousClass2(SwitchWalletActivity switchWalletActivity) {
            super(1);
            this.this$0 = switchWalletActivity;
        }

        @Override // defpackage.tc1
        public /* bridge */ /* synthetic */ te4 invoke(Wallet wallet2) {
            invoke2(wallet2);
            return te4.a;
        }

        /* renamed from: invoke  reason: avoid collision after fix types in other method */
        public final void invoke2(Wallet wallet2) {
            fs1.f(wallet2, "wallet");
            WalletManageActivity.B0.a(this.this$0, wallet2);
        }
    }

    /* compiled from: SwitchWalletActivity.kt */
    /* renamed from: net.safemoon.androidwallet.activity.SwitchWalletActivity$walletAdapter$2$3  reason: invalid class name */
    /* loaded from: classes2.dex */
    public static final class AnonymousClass3 extends Lambda implements tc1<Wallet, te4> {
        public static final AnonymousClass3 INSTANCE = new AnonymousClass3();

        public AnonymousClass3() {
            super(1);
        }

        @Override // defpackage.tc1
        public /* bridge */ /* synthetic */ te4 invoke(Wallet wallet2) {
            invoke2(wallet2);
            return te4.a;
        }

        /* renamed from: invoke  reason: avoid collision after fix types in other method */
        public final void invoke2(Wallet wallet2) {
            fs1.f(wallet2, "it");
        }
    }

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public SwitchWalletActivity$walletAdapter$2(SwitchWalletActivity switchWalletActivity) {
        super(0);
        this.this$0 = switchWalletActivity;
    }

    @Override // defpackage.rc1
    public final xn4 invoke() {
        SwitchWalletActivity switchWalletActivity = this.this$0;
        return new xn4(switchWalletActivity, e30.c(switchWalletActivity), new AnonymousClass1(this.this$0), new AnonymousClass2(this.this$0), AnonymousClass3.INSTANCE);
    }
}
