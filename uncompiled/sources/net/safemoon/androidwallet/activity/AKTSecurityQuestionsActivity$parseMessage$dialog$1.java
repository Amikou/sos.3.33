package net.safemoon.androidwallet.activity;

import kotlin.jvm.internal.Lambda;

/* compiled from: AKTSecurityQuestionsActivity.kt */
/* loaded from: classes2.dex */
public final class AKTSecurityQuestionsActivity$parseMessage$dialog$1 extends Lambda implements rc1<te4> {
    public final /* synthetic */ String $mnemonic;
    public final /* synthetic */ AKTSecurityQuestionsActivity this$0;

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public AKTSecurityQuestionsActivity$parseMessage$dialog$1(AKTSecurityQuestionsActivity aKTSecurityQuestionsActivity, String str) {
        super(0);
        this.this$0 = aKTSecurityQuestionsActivity;
        this.$mnemonic = str;
    }

    @Override // defpackage.rc1
    public /* bridge */ /* synthetic */ te4 invoke() {
        invoke2();
        return te4.a;
    }

    @Override // defpackage.rc1
    /* renamed from: invoke  reason: avoid collision after fix types in other method */
    public final void invoke2() {
        AKTSecurityQuestionsActivity aKTSecurityQuestionsActivity = this.this$0;
        String str = this.$mnemonic;
        fs1.e(str, "mnemonic");
        aKTSecurityQuestionsActivity.r1(str);
    }
}
