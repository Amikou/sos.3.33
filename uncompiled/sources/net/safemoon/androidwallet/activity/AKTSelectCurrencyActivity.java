package net.safemoon.androidwallet.activity;

import android.content.Context;
import android.content.Intent;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import androidx.appcompat.app.ActionBar;
import androidx.fragment.app.FragmentManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import net.safemoon.androidwallet.R;
import net.safemoon.androidwallet.activity.AKTSelectCurrencyActivity;
import net.safemoon.androidwallet.activity.AKTSelectCurrencyActivity$defaultCurrencyAdapter$2;
import net.safemoon.androidwallet.activity.common.BasicActivity;
import net.safemoon.androidwallet.dialogs.ProgressLoading;
import net.safemoon.androidwallet.model.common.HistoryListItem;
import net.safemoon.androidwallet.model.fiat.room.RoomFiat;
import net.safemoon.androidwallet.viewmodels.SelectFiatViewModel;

/* compiled from: AKTSelectCurrencyActivity.kt */
/* loaded from: classes2.dex */
public final class AKTSelectCurrencyActivity extends BasicActivity {
    public static final a o0 = new a(null);
    public final sy1 j0 = zy1.a(new AKTSelectCurrencyActivity$binding$2(this));
    public final sy1 k0 = zy1.a(new AKTSelectCurrencyActivity$loader$2(this));
    public final sy1 l0 = new fj4(d53.b(SelectFiatViewModel.class), new AKTSelectCurrencyActivity$special$$inlined$viewModels$2(this), new AKTSelectCurrencyActivity$selectFiatViewModel$2(this));
    public String m0 = "";
    public final sy1 n0 = zy1.a(new AKTSelectCurrencyActivity$defaultCurrencyAdapter$2(this));

    /* compiled from: AKTSelectCurrencyActivity.kt */
    /* loaded from: classes2.dex */
    public static final class a {
        public a() {
        }

        public /* synthetic */ a(qi0 qi0Var) {
            this();
        }

        public final void a(Context context) {
            fs1.f(context, "context");
            context.startActivity(new Intent(context, AKTSelectCurrencyActivity.class));
        }
    }

    /* compiled from: AKTSelectCurrencyActivity.kt */
    /* loaded from: classes2.dex */
    public static final class b implements TextWatcher {
        public b() {
        }

        @Override // android.text.TextWatcher
        public void afterTextChanged(Editable editable) {
            AKTSelectCurrencyActivity aKTSelectCurrencyActivity = AKTSelectCurrencyActivity.this;
            CharSequence charSequence = editable;
            if (editable == null) {
                charSequence = "";
            }
            aKTSelectCurrencyActivity.m0 = charSequence.toString();
            List<HistoryListItem> k = AKTSelectCurrencyActivity.this.U().k(AKTSelectCurrencyActivity.this.m0);
            AKTSelectCurrencyActivity.this.R().e.setText(R.string.currency_not_found);
            AKTSelectCurrencyActivity.this.R().e.setVisibility(k.size() == 0 ? 0 : 8);
            AKTSelectCurrencyActivity.this.S().d(k);
        }

        @Override // android.text.TextWatcher
        public void beforeTextChanged(CharSequence charSequence, int i, int i2, int i3) {
        }

        @Override // android.text.TextWatcher
        public void onTextChanged(CharSequence charSequence, int i, int i2, int i3) {
        }
    }

    public static final void V(AKTSelectCurrencyActivity aKTSelectCurrencyActivity, View view) {
        fs1.f(aKTSelectCurrencyActivity, "this$0");
        aKTSelectCurrencyActivity.onBackPressed();
    }

    public static final void W(AKTSelectCurrencyActivity aKTSelectCurrencyActivity, List list) {
        boolean z;
        fs1.f(aKTSelectCurrencyActivity, "this$0");
        fs1.e(list, "listFiats");
        boolean z2 = true;
        if (!list.isEmpty()) {
            if (!(list instanceof Collection) || !list.isEmpty()) {
                Iterator it = list.iterator();
                while (it.hasNext()) {
                    if (((RoomFiat) it.next()).getName() == null) {
                        z = true;
                        continue;
                    } else {
                        z = false;
                        continue;
                    }
                    if (z) {
                        break;
                    }
                }
            }
            z2 = false;
            if (z2) {
                return;
            }
            aKTSelectCurrencyActivity.T().h();
            aKTSelectCurrencyActivity.S().d(aKTSelectCurrencyActivity.U().k(aKTSelectCurrencyActivity.m0));
        }
    }

    public final x6 R() {
        return (x6) this.j0.getValue();
    }

    public final AKTSelectCurrencyActivity$defaultCurrencyAdapter$2.a S() {
        return (AKTSelectCurrencyActivity$defaultCurrencyAdapter$2.a) this.n0.getValue();
    }

    public final ProgressLoading T() {
        return (ProgressLoading) this.k0.getValue();
    }

    public final SelectFiatViewModel U() {
        return (SelectFiatViewModel) this.l0.getValue();
    }

    @Override // net.safemoon.androidwallet.activity.common.BasicActivity, androidx.fragment.app.FragmentActivity, androidx.activity.ComponentActivity, androidx.core.app.ComponentActivity, android.app.Activity
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        setContentView(R().b());
        ActionBar supportActionBar = getSupportActionBar();
        if (supportActionBar != null) {
            supportActionBar.l();
        }
        getWindow().addFlags(Integer.MIN_VALUE);
        getWindow().setStatusBarColor(m70.d(this, 17170445));
        getWindow().setBackgroundDrawable(new ColorDrawable(getColor(R.color.akt_night_background)));
    }

    @Override // net.safemoon.androidwallet.activity.common.BasicActivity, androidx.appcompat.app.AppCompatActivity, android.app.Activity
    public void onPostCreate(Bundle bundle) {
        super.onPostCreate(bundle);
        ProgressLoading T = T();
        FragmentManager supportFragmentManager = getSupportFragmentManager();
        fs1.e(supportFragmentManager, "supportFragmentManager");
        T.A(supportFragmentManager);
        x6 R = R();
        R.d.e.setText(R.string.default_currency);
        R.d.b.setOnClickListener(new View.OnClickListener() { // from class: a3
            @Override // android.view.View.OnClickListener
            public final void onClick(View view) {
                AKTSelectCurrencyActivity.V(AKTSelectCurrencyActivity.this, view);
            }
        });
        R.c.setLayoutManager(new LinearLayoutManager(this, 1, false));
        R.c.setAdapter(S());
        R.b.addTextChangedListener(new b());
        U().h().observe(this, new tl2() { // from class: z2
            @Override // defpackage.tl2
            public final void onChanged(Object obj) {
                AKTSelectCurrencyActivity.W(AKTSelectCurrencyActivity.this, (List) obj);
            }
        });
    }
}
