package net.safemoon.androidwallet.activity;

import android.content.ClipData;
import android.content.ClipboardManager;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import androidx.appcompat.widget.AppCompatEditText;
import com.fasterxml.jackson.core.util.MinimalPrettyPrinter;
import java.util.Arrays;
import java.util.List;
import java.util.Objects;
import kotlin.text.StringsKt__StringsKt;
import net.safemoon.androidwallet.R;
import net.safemoon.androidwallet.activity.ImportWordActivity;

/* compiled from: ImportWordActivity.kt */
/* loaded from: classes2.dex */
public final class ImportWordActivity extends RecoverWalletActivity {
    public static final a p0 = new a(null);
    public final sy1 m0 = zy1.a(new ImportWordActivity$manager$2(this));
    public final sy1 n0 = zy1.a(new ImportWordActivity$binding$2(this));
    public final sy1 o0 = zy1.a(new ImportWordActivity$wordCount$2(this));

    /* compiled from: ImportWordActivity.kt */
    /* loaded from: classes2.dex */
    public static final class a {
        public a() {
        }

        public /* synthetic */ a(qi0 qi0Var) {
            this();
        }

        public final void a(Context context, int i) {
            fs1.f(context, "context");
            Intent intent = new Intent(context, ImportWordActivity.class);
            intent.putExtra("WORD_COUNT", i);
            te4 te4Var = te4.a;
            context.startActivity(intent);
        }
    }

    public static final void p0(ImportWordActivity importWordActivity, EditText[] editTextArr, View view) {
        fs1.f(importWordActivity, "this$0");
        fs1.f(editTextArr, "$editTextView");
        ClipData primaryClip = importWordActivity.m0().getPrimaryClip();
        if (primaryClip != null && primaryClip.getItemCount() > 0) {
            int i = 0;
            if (primaryClip.getItemAt(0).getText() != null) {
                String D = dv3.D(primaryClip.getItemAt(0).getText().toString(), "|", MinimalPrettyPrinter.DEFAULT_ROOT_VALUE_SEPARATOR, false, 4, null);
                Objects.requireNonNull(D, "null cannot be cast to non-null type kotlin.CharSequence");
                List w0 = StringsKt__StringsKt.w0(StringsKt__StringsKt.K0(D).toString(), new String[]{MinimalPrettyPrinter.DEFAULT_ROOT_VALUE_SEPARATOR}, false, 0, 6, null);
                if (w0.size() == importWordActivity.n0()) {
                    int length = editTextArr.length;
                    int i2 = 0;
                    while (i < length) {
                        EditText editText = editTextArr[i];
                        int i3 = i2 + 1;
                        if (editText != null && i2 < w0.size()) {
                            editText.setText((CharSequence) w0.get(i2));
                        }
                        i++;
                        i2 = i3;
                    }
                    return;
                }
                importWordActivity.t0();
                return;
            }
        }
        importWordActivity.t0();
    }

    public static final void q0(ImportWordActivity importWordActivity, EditText[] editTextArr, View view) {
        fs1.f(importWordActivity, "this$0");
        fs1.f(editTextArr, "$editTextView");
        importWordActivity.v0(editTextArr);
    }

    public static final void r0(ImportWordActivity importWordActivity, EditText[] editTextArr, View view) {
        fs1.f(importWordActivity, "this$0");
        fs1.f(editTextArr, "$editTextView");
        importWordActivity.T((EditText[]) Arrays.copyOf(editTextArr, editTextArr.length));
    }

    public static final void s0(ImportWordActivity importWordActivity, View view) {
        fs1.f(importWordActivity, "this$0");
        importWordActivity.onBackPressed();
    }

    public static final void u0(DialogInterface dialogInterface) {
    }

    public final i7 l0() {
        return (i7) this.n0.getValue();
    }

    public final ClipboardManager m0() {
        return (ClipboardManager) this.m0.getValue();
    }

    public final int n0() {
        return ((Number) this.o0.getValue()).intValue();
    }

    public final void o0() {
        i7 l0 = l0();
        int n0 = n0();
        Integer[] numArr = new Integer[n0];
        for (int i = 0; i < n0; i++) {
            numArr[i] = 1;
        }
        int n02 = n0();
        final EditText[] editTextArr = new EditText[n02];
        for (int i2 = 0; i2 < n02; i2++) {
            editTextArr[i2] = null;
        }
        int n03 = n0();
        if (n03 > 0) {
            int i3 = 0;
            while (true) {
                int i4 = i3 + 1;
                View inflate = LayoutInflater.from(this).inflate(R.layout.include_word, (ViewGroup) l0.f, false);
                inflate.setId(Math.abs(fs1.l("Item", Integer.valueOf(i4)).hashCode()));
                cq1 a2 = cq1.a(inflate);
                int abs = Math.abs(fs1.l("Word", Integer.valueOf(i4)).hashCode());
                int abs2 = Math.abs(fs1.l("Word", Integer.valueOf(i3 + 2)).hashCode());
                a2.a.setText("" + i4 + '.');
                AppCompatEditText appCompatEditText = (AppCompatEditText) inflate.findViewWithTag("word");
                appCompatEditText.setId(abs);
                appCompatEditText.setNextFocusDownId(abs2);
                editTextArr[i3] = appCompatEditText;
                numArr[i3] = Integer.valueOf(inflate.getId());
                l0.f.addView(inflate);
                if (i4 >= n03) {
                    break;
                }
                i3 = i4;
            }
        }
        l0.g.setReferencedIds(ai.I(numArr));
        l0.d.setOnClickListener(new View.OnClickListener() { // from class: vp1
            @Override // android.view.View.OnClickListener
            public final void onClick(View view) {
                ImportWordActivity.p0(ImportWordActivity.this, editTextArr, view);
            }
        });
        l0.e.setOnClickListener(new View.OnClickListener() { // from class: tp1
            @Override // android.view.View.OnClickListener
            public final void onClick(View view) {
                ImportWordActivity.q0(ImportWordActivity.this, editTextArr, view);
            }
        });
        l0.c.setOnClickListener(new View.OnClickListener() { // from class: up1
            @Override // android.view.View.OnClickListener
            public final void onClick(View view) {
                ImportWordActivity.r0(ImportWordActivity.this, editTextArr, view);
            }
        });
        l0.b.setOnClickListener(new View.OnClickListener() { // from class: sp1
            @Override // android.view.View.OnClickListener
            public final void onClick(View view) {
                ImportWordActivity.s0(ImportWordActivity.this, view);
            }
        });
    }

    @Override // net.safemoon.androidwallet.activity.RecoverWalletActivity, net.safemoon.androidwallet.activity.common.BasicActivity, androidx.fragment.app.FragmentActivity, androidx.activity.ComponentActivity, androidx.core.app.ComponentActivity, android.app.Activity
    public void onCreate(Bundle bundle) {
        setRequestedOrientation(1);
        super.onCreate(bundle);
        setContentView(l0().b());
    }

    @Override // net.safemoon.androidwallet.activity.RecoverWalletActivity, net.safemoon.androidwallet.activity.common.BasicActivity, androidx.appcompat.app.AppCompatActivity, android.app.Activity
    public void onPostCreate(Bundle bundle) {
        super.onPostCreate(bundle);
        o0();
    }

    public final void t0() {
        String string = getString(R.string.warning_wrong_phrase_title);
        String string2 = getString(R.string.warning_wrong_phrase_msg, new Object[]{Integer.valueOf(n0())});
        fs1.e(string2, "getString(R.string.warni…ng_phrase_msg, wordCount)");
        jc0.d(this, string, string2, true, rp1.a);
    }

    public final void v0(EditText[] editTextArr) {
        w7<Intent> b;
        hn2 D = D();
        if (D == null || (b = D.b(new ImportWordActivity$startScanQRCode$1(this, editTextArr))) == null) {
            return;
        }
        b.a(new Intent(this, ScannedCodeActivity.class));
    }
}
