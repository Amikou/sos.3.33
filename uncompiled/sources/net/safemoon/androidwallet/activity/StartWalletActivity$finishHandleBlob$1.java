package net.safemoon.androidwallet.activity;

import java.util.List;
import java.util.Map;
import kotlin.jvm.internal.Lambda;
import net.safemoon.androidwallet.model.wallets.Wallet;
import net.safemoon.androidwallet.viewmodels.MultiWalletViewModel;

/* compiled from: StartWalletActivity.kt */
/* loaded from: classes2.dex */
public final class StartWalletActivity$finishHandleBlob$1 extends Lambda implements tc1<List<? extends Wallet>, te4> {
    public final /* synthetic */ Wallet $activeWallet;
    public final /* synthetic */ boolean $updateWalletInfo;
    public final /* synthetic */ StartWalletActivity this$0;

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public StartWalletActivity$finishHandleBlob$1(Wallet wallet2, StartWalletActivity startWalletActivity, boolean z) {
        super(1);
        this.$activeWallet = wallet2;
        this.this$0 = startWalletActivity;
        this.$updateWalletInfo = z;
    }

    @Override // defpackage.tc1
    public /* bridge */ /* synthetic */ te4 invoke(List<? extends Wallet> list) {
        invoke2((List<Wallet>) list);
        return te4.a;
    }

    /* renamed from: invoke  reason: avoid collision after fix types in other method */
    public final void invoke2(List<Wallet> list) {
        MultiWalletViewModel multiWalletViewModel;
        MultiWalletViewModel multiWalletViewModel2;
        List list2;
        Map map;
        MultiWalletViewModel multiWalletViewModel3;
        Map map2;
        MultiWalletViewModel multiWalletViewModel4;
        Map map3;
        MultiWalletViewModel multiWalletViewModel5;
        fs1.f(list, "list");
        Wallet wallet2 = this.$activeWallet;
        if (((wallet2 != null && wallet2.isPrimaryWallet()) || this.$activeWallet == null) && list.size() > 1 && !do3.a.c(this.this$0)) {
            multiWalletViewModel = this.this$0.k0;
            multiWalletViewModel.A(list.get(1));
        }
        if (this.$updateWalletInfo) {
            boolean z = true;
            for (Wallet wallet3 : list) {
                if (!wallet3.isPrimaryWallet()) {
                    list2 = this.this$0.w0;
                    if (list2.contains(wallet3.getAddress())) {
                        map = this.this$0.x0;
                        if (map.containsKey(wallet3.getAddress())) {
                            String name = wallet3.getName();
                            map2 = this.this$0.x0;
                            if (!fs1.b(name, map2.get(wallet3.getAddress()))) {
                                multiWalletViewModel4 = this.this$0.k0;
                                map3 = this.this$0.x0;
                                Object obj = map3.get(wallet3.getAddress());
                                fs1.d(obj);
                                multiWalletViewModel4.D(wallet3, (String) obj, 1, null);
                            }
                        }
                        multiWalletViewModel3 = this.this$0.k0;
                        multiWalletViewModel3.C(wallet3, 1, null);
                    } else {
                        multiWalletViewModel5 = this.this$0.k0;
                        multiWalletViewModel5.C(wallet3, 0, null);
                        z = false;
                    }
                } else if (!wallet3.isLinked()) {
                    multiWalletViewModel2 = this.this$0.k0;
                    multiWalletViewModel2.C(wallet3, 1, null);
                }
            }
            do3.a.h(this.this$0, list.size() != 1 ? z : false);
        }
        this.this$0.C0();
    }
}
