package net.safemoon.androidwallet.activity;

import android.content.Context;
import android.content.Intent;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.view.View;
import androidx.appcompat.app.ActionBar;
import net.safemoon.androidwallet.R;
import net.safemoon.androidwallet.activity.AKTActivity;
import net.safemoon.androidwallet.activity.common.BasicActivity;

/* compiled from: AKTActivity.kt */
/* loaded from: classes2.dex */
public final class AKTActivity extends BasicActivity {
    public static final a k0 = new a(null);
    public final sy1 j0 = zy1.a(new AKTActivity$binding$2(this));

    /* compiled from: AKTActivity.kt */
    /* loaded from: classes2.dex */
    public static final class a {
        public a() {
        }

        public /* synthetic */ a(qi0 qi0Var) {
            this();
        }

        public final void a(Context context) {
            fs1.f(context, "context");
            context.startActivity(new Intent(context, AKTActivity.class));
        }
    }

    public static final void O(AKTActivity aKTActivity, View view) {
        fs1.f(aKTActivity, "this$0");
        aKTActivity.startActivity(new Intent(aKTActivity, AKTRegisterGuideActivity.class));
    }

    public static final void P(AKTActivity aKTActivity, View view) {
        fs1.f(aKTActivity, "this$0");
        AKTLoginActivity.r0.a(aKTActivity, true);
    }

    public static final void Q(AKTActivity aKTActivity, View view) {
        fs1.f(aKTActivity, "this$0");
        aKTActivity.onBackPressed();
    }

    public final p6 N() {
        return (p6) this.j0.getValue();
    }

    @Override // net.safemoon.androidwallet.activity.common.BasicActivity, androidx.fragment.app.FragmentActivity, androidx.activity.ComponentActivity, androidx.core.app.ComponentActivity, android.app.Activity
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        setContentView(N().b());
        ActionBar supportActionBar = getSupportActionBar();
        if (supportActionBar != null) {
            supportActionBar.l();
        }
        getWindow().addFlags(Integer.MIN_VALUE);
        getWindow().setStatusBarColor(m70.d(this, 17170445));
        getWindow().setBackgroundDrawable(new ColorDrawable(getColor(R.color.akt_night_background)));
    }

    @Override // net.safemoon.androidwallet.activity.common.BasicActivity, androidx.appcompat.app.AppCompatActivity, android.app.Activity
    public void onPostCreate(Bundle bundle) {
        super.onPostCreate(bundle);
        p6 N = N();
        N.c.setOnClickListener(new View.OnClickListener() { // from class: e
            @Override // android.view.View.OnClickListener
            public final void onClick(View view) {
                AKTActivity.O(AKTActivity.this, view);
            }
        });
        N.b.setOnClickListener(new View.OnClickListener() { // from class: f
            @Override // android.view.View.OnClickListener
            public final void onClick(View view) {
                AKTActivity.P(AKTActivity.this, view);
            }
        });
        N.d.setOnClickListener(new View.OnClickListener() { // from class: g
            @Override // android.view.View.OnClickListener
            public final void onClick(View view) {
                AKTActivity.Q(AKTActivity.this, view);
            }
        });
    }
}
