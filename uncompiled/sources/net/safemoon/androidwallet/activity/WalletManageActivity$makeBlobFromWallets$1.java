package net.safemoon.androidwallet.activity;

import java.util.List;
import kotlin.jvm.internal.Lambda;
import net.safemoon.androidwallet.model.wallets.Wallet;

/* compiled from: WalletManageActivity.kt */
/* loaded from: classes2.dex */
public final class WalletManageActivity$makeBlobFromWallets$1 extends Lambda implements tc1<List<? extends Wallet>, te4> {
    public final /* synthetic */ StringBuilder $blobBuilder;
    public final /* synthetic */ Wallet $selected;
    public final /* synthetic */ WalletManageActivity this$0;

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public WalletManageActivity$makeBlobFromWallets$1(Wallet wallet2, WalletManageActivity walletManageActivity, StringBuilder sb) {
        super(1);
        this.$selected = wallet2;
        this.this$0 = walletManageActivity;
        this.$blobBuilder = sb;
    }

    @Override // defpackage.tc1
    public /* bridge */ /* synthetic */ te4 invoke(List<? extends Wallet> list) {
        invoke2((List<Wallet>) list);
        return te4.a;
    }

    /* JADX WARN: Code restructure failed: missing block: B:40:0x00b9, code lost:
        if (r8 == false) goto L57;
     */
    /* JADX WARN: Code restructure failed: missing block: B:43:0x00c0, code lost:
        if (r7.isLinked() == false) goto L57;
     */
    /* JADX WARN: Removed duplicated region for block: B:22:0x0064  */
    /* JADX WARN: Removed duplicated region for block: B:25:0x0077  */
    /* JADX WARN: Removed duplicated region for block: B:55:0x00f2  */
    /* JADX WARN: Removed duplicated region for block: B:58:0x0105  */
    /* renamed from: invoke  reason: avoid collision after fix types in other method */
    /*
        Code decompiled incorrectly, please refer to instructions dump.
        To view partially-correct code enable 'Show inconsistent code' option in preferences
    */
    public final void invoke2(java.util.List<net.safemoon.androidwallet.model.wallets.Wallet> r12) {
        /*
            Method dump skipped, instructions count: 306
            To view this dump change 'Code comments level' option to 'DEBUG'
        */
        throw new UnsupportedOperationException("Method not decompiled: net.safemoon.androidwallet.activity.WalletManageActivity$makeBlobFromWallets$1.invoke2(java.util.List):void");
    }
}
