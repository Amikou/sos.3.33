package net.safemoon.androidwallet.activity;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.PorterDuff;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import androidx.appcompat.app.AppCompatActivity;
import androidx.lifecycle.l;
import java.lang.ref.WeakReference;
import net.safemoon.androidwallet.MyApplicationClass;
import net.safemoon.androidwallet.R;
import net.safemoon.androidwallet.activity.SetPasswordActivity;
import net.safemoon.androidwallet.common.TokenType;
import net.safemoon.androidwallet.dialogs.ProgressLoading;
import net.safemoon.androidwallet.viewmodels.CreateWalletViewModel;
import net.safemoon.androidwallet.viewmodels.MultiWalletViewModel;

/* loaded from: classes2.dex */
public class SetPasswordActivity extends AppCompatActivity {
    public y7 a;
    public CreateWalletViewModel f0;
    public MultiWalletViewModel g0;
    public String h0 = "";
    public CreateWalletViewModel.KEY_TYPE i0;

    /* loaded from: classes2.dex */
    public class a implements TextWatcher {
        public a() {
        }

        @Override // android.text.TextWatcher
        public void afterTextChanged(Editable editable) {
            String obj = SetPasswordActivity.this.a.c.getText().toString();
            if (editable.length() <= 0 || obj.length() <= 0) {
                return;
            }
            if (editable.toString().equals(obj)) {
                SetPasswordActivity.this.a.l.setVisibility(8);
                SetPasswordActivity.this.a.b.setAlpha(1.0f);
                return;
            }
            SetPasswordActivity.this.a.l.setVisibility(0);
            SetPasswordActivity.this.a.b.setAlpha(0.5f);
        }

        @Override // android.text.TextWatcher
        public void beforeTextChanged(CharSequence charSequence, int i, int i2, int i3) {
        }

        @Override // android.text.TextWatcher
        public void onTextChanged(CharSequence charSequence, int i, int i2, int i3) {
        }
    }

    /* loaded from: classes2.dex */
    public class b implements TextWatcher {
        public b() {
        }

        @Override // android.text.TextWatcher
        public void afterTextChanged(Editable editable) {
            String obj = SetPasswordActivity.this.a.d.getText().toString();
            if (editable.length() <= 0 || obj.length() <= 0) {
                return;
            }
            if (editable.toString().equals(obj)) {
                SetPasswordActivity.this.a.l.setVisibility(8);
                SetPasswordActivity.this.a.b.setAlpha(1.0f);
            } else {
                SetPasswordActivity.this.a.l.setVisibility(0);
                SetPasswordActivity.this.a.b.setAlpha(0.5f);
            }
            if (editable.toString().length() > 7) {
                SetPasswordActivity.this.a.j.setTextColor(m70.d(SetPasswordActivity.this, R.color.btn_light_green));
                SetPasswordActivity.this.a.f.setColorFilter(m70.d(SetPasswordActivity.this, R.color.btn_light_green), PorterDuff.Mode.MULTIPLY);
                SetPasswordActivity.this.a.j.setTag("complete");
            } else {
                SetPasswordActivity.this.a.j.setTextColor(m70.d(SetPasswordActivity.this, R.color.color_edit));
                SetPasswordActivity.this.a.f.setColorFilter(m70.d(SetPasswordActivity.this, R.color.color_edit), PorterDuff.Mode.MULTIPLY);
                SetPasswordActivity.this.a.j.setTag("incomplete");
            }
            if (s44.h(editable.toString())) {
                SetPasswordActivity.this.a.n.setTextColor(m70.d(SetPasswordActivity.this, R.color.btn_light_green));
                SetPasswordActivity.this.a.g.setColorFilter(m70.d(SetPasswordActivity.this, R.color.btn_light_green), PorterDuff.Mode.MULTIPLY);
                SetPasswordActivity.this.a.n.setTag("complete");
            } else {
                SetPasswordActivity.this.a.n.setTextColor(m70.d(SetPasswordActivity.this, R.color.color_edit));
                SetPasswordActivity.this.a.g.setColorFilter(m70.d(SetPasswordActivity.this, R.color.color_edit), PorterDuff.Mode.MULTIPLY);
                SetPasswordActivity.this.a.n.setTag("incomplete");
            }
            if (s44.b(editable.toString())) {
                SetPasswordActivity.this.a.k.setTextColor(m70.d(SetPasswordActivity.this, R.color.btn_light_green));
                SetPasswordActivity.this.a.h.setColorFilter(m70.d(SetPasswordActivity.this, R.color.btn_light_green), PorterDuff.Mode.MULTIPLY);
                SetPasswordActivity.this.a.k.setTag("complete");
            } else {
                SetPasswordActivity.this.a.k.setTextColor(m70.d(SetPasswordActivity.this, R.color.color_edit));
                SetPasswordActivity.this.a.h.setColorFilter(m70.d(SetPasswordActivity.this, R.color.color_edit), PorterDuff.Mode.MULTIPLY);
                SetPasswordActivity.this.a.k.setTag("incomplete");
            }
            if (s44.e(editable.toString())) {
                SetPasswordActivity.this.a.m.setTextColor(m70.d(SetPasswordActivity.this, R.color.btn_light_green));
                SetPasswordActivity.this.a.i.setColorFilter(m70.d(SetPasswordActivity.this, R.color.btn_light_green), PorterDuff.Mode.MULTIPLY);
                SetPasswordActivity.this.a.m.setTag("complete");
                return;
            }
            SetPasswordActivity.this.a.m.setTextColor(m70.d(SetPasswordActivity.this, R.color.color_edit));
            SetPasswordActivity.this.a.i.setColorFilter(m70.d(SetPasswordActivity.this, R.color.color_edit), PorterDuff.Mode.MULTIPLY);
            SetPasswordActivity.this.a.m.setTag("incomplete");
        }

        @Override // android.text.TextWatcher
        public void beforeTextChanged(CharSequence charSequence, int i, int i2, int i3) {
        }

        @Override // android.text.TextWatcher
        public void onTextChanged(CharSequence charSequence, int i, int i2, int i3) {
        }
    }

    /* JADX INFO: Access modifiers changed from: private */
    public /* synthetic */ te4 A(ProgressLoading progressLoading, Long l) {
        progressLoading.h();
        if (l != null && l.longValue() > 0) {
            MyApplicationClass.c().m0 = true;
            AKTHomeActivity.R0(this);
        } else {
            bh.U(new WeakReference(this), Integer.valueOf((int) R.string.warning_iw_failed_title), Integer.valueOf((int) R.string.warning_iw_failed_msg), R.string.action_ok, null);
        }
        return null;
    }

    /* JADX INFO: Access modifiers changed from: private */
    public /* synthetic */ te4 B(final ProgressLoading progressLoading, String str, String str2, String str3, String str4, Boolean bool) {
        if (bool.booleanValue()) {
            progressLoading.h();
            jc0.c(this, Integer.valueOf((int) R.string.warning_title), R.string.warning_black_list_address, true, pm3.a);
            return null;
        } else if (str2 != null) {
            bo3.o(this, "SAFEMOON_PASSWORD", str);
            bo3.o(this, "DEFAULT_GATEWAY", TokenType.BEP_20.getName());
            this.g0.m(str3, str2, str4, null, true, false, new tc1() { // from class: nm3
                @Override // defpackage.tc1
                public final Object invoke(Object obj) {
                    te4 A;
                    A = SetPasswordActivity.this.A(progressLoading, (Long) obj);
                    return A;
                }
            });
            return null;
        } else {
            progressLoading.h();
            e30.a0(this, getString(R.string.nft_get_data_error));
            return null;
        }
    }

    /* JADX INFO: Access modifiers changed from: private */
    public /* synthetic */ void C(View view) {
        if (this.a.b.getAlpha() == 1.0f) {
            if (this.a.d.getText().toString().length() > 7 && s44.g(this.a.d.getText().toString())) {
                final String trim = this.a.d.getText().toString().trim();
                final ProgressLoading a2 = ProgressLoading.y0.a(false, getString(R.string.loading), "");
                a2.A(getSupportFragmentManager());
                this.f0.b(this.h0, this.i0, new md1() { // from class: om3
                    @Override // defpackage.md1
                    public final Object invoke(Object obj, Object obj2, Object obj3, Object obj4) {
                        te4 B;
                        B = SetPasswordActivity.this.B(a2, trim, (String) obj, (String) obj2, (String) obj3, (Boolean) obj4);
                        return B;
                    }
                });
                return;
            }
            this.a.e.setVisibility(0);
            return;
        }
        e30.a0(this, "Password does not match");
    }

    public static void D(Context context, String str, CreateWalletViewModel.KEY_TYPE key_type) {
        Intent intent = new Intent(context, SetPasswordActivity.class);
        intent.putExtra("argKey", str);
        intent.putExtra("argKeyType", key_type);
        context.startActivity(intent);
    }

    public static /* synthetic */ void z(DialogInterface dialogInterface) {
    }

    public final void initViews() {
        this.a.d.addTextChangedListener(new a());
        this.a.c.addTextChangedListener(new b());
    }

    @Override // androidx.fragment.app.FragmentActivity, androidx.activity.ComponentActivity, androidx.core.app.ComponentActivity, android.app.Activity
    public void onCreate(Bundle bundle) {
        this.h0 = getIntent().getStringExtra("argKey");
        this.i0 = (CreateWalletViewModel.KEY_TYPE) getIntent().getSerializableExtra("argKeyType");
        super.onCreate(bundle);
        y7 c = y7.c(getLayoutInflater());
        this.a = c;
        setContentView(c.b());
        this.f0 = (CreateWalletViewModel) new l(this).a(CreateWalletViewModel.class);
        this.g0 = (MultiWalletViewModel) new l(this).a(MultiWalletViewModel.class);
        if (getSupportActionBar() != null) {
            getSupportActionBar().l();
        }
        getWindow().addFlags(Integer.MIN_VALUE);
        getWindow().setStatusBarColor(m70.d(this, 17170445));
        getWindow().setBackgroundDrawableResource(R.drawable.ic_bg);
        initViews();
        y();
    }

    public final void y() {
        this.a.b.setOnClickListener(new View.OnClickListener() { // from class: qm3
            @Override // android.view.View.OnClickListener
            public final void onClick(View view) {
                SetPasswordActivity.this.C(view);
            }
        });
    }
}
