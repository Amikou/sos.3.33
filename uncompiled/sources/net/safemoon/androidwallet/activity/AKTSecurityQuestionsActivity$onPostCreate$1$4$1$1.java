package net.safemoon.androidwallet.activity;

import android.os.Handler;
import android.os.Looper;
import kotlin.jvm.internal.Lambda;
import net.safemoon.androidwallet.activity.AKTSecurityQuestionsActivity$onPostCreate$1$4$1$1;

/* compiled from: AKTSecurityQuestionsActivity.kt */
/* loaded from: classes2.dex */
public final class AKTSecurityQuestionsActivity$onPostCreate$1$4$1$1 extends Lambda implements tc1<String, te4> {
    public final /* synthetic */ w6 $this_apply;
    public final /* synthetic */ AKTSecurityQuestionsActivity this$0;

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public AKTSecurityQuestionsActivity$onPostCreate$1$4$1$1(AKTSecurityQuestionsActivity aKTSecurityQuestionsActivity, w6 w6Var) {
        super(1);
        this.this$0 = aKTSecurityQuestionsActivity;
        this.$this_apply = w6Var;
    }

    public static final void c(w6 w6Var) {
        fs1.f(w6Var, "$this_apply");
        pg4.g(w6Var.k);
    }

    public static final void d(w6 w6Var) {
        fs1.f(w6Var, "$this_apply");
        pg4.g(w6Var.j);
    }

    @Override // defpackage.tc1
    public /* bridge */ /* synthetic */ te4 invoke(String str) {
        invoke2(str);
        return te4.a;
    }

    /* renamed from: invoke  reason: avoid collision after fix types in other method */
    public final void invoke2(String str) {
        String U0;
        gb2 gb2Var;
        fs1.f(str, "it");
        U0 = this.this$0.U0();
        if (fs1.b(str, U0)) {
            this.this$0.x0 = true;
            this.$this_apply.s.setVisibility(8);
            this.$this_apply.k.setVisibility(0);
            Handler handler = new Handler(Looper.getMainLooper());
            final w6 w6Var = this.$this_apply;
            handler.postDelayed(new Runnable() { // from class: y2
                @Override // java.lang.Runnable
                public final void run() {
                    AKTSecurityQuestionsActivity$onPostCreate$1$4$1$1.c(w6.this);
                }
            }, 200L);
        } else {
            this.this$0.x0 = false;
            this.$this_apply.s.setVisibility(0);
            this.$this_apply.k.setVisibility(8);
            gb2Var = this.this$0.v0;
            gb2Var.postValue(str);
            Handler handler2 = new Handler(Looper.getMainLooper());
            final w6 w6Var2 = this.$this_apply;
            handler2.postDelayed(new Runnable() { // from class: x2
                @Override // java.lang.Runnable
                public final void run() {
                    AKTSecurityQuestionsActivity$onPostCreate$1$4$1$1.d(w6.this);
                }
            }, 200L);
        }
        this.this$0.N0();
    }
}
