package net.safemoon.androidwallet.activity;

import kotlin.jvm.internal.Lambda;

/* compiled from: AKTResetPasswordActivity.kt */
/* loaded from: classes2.dex */
public final class AKTResetPasswordActivity$parseMessage$1 extends Lambda implements rc1<te4> {
    public final /* synthetic */ qy4 $safeMoonppp;
    public final /* synthetic */ AKTResetPasswordActivity this$0;

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public AKTResetPasswordActivity$parseMessage$1(qy4 qy4Var, AKTResetPasswordActivity aKTResetPasswordActivity) {
        super(0);
        this.$safeMoonppp = qy4Var;
        this.this$0 = aKTResetPasswordActivity;
    }

    @Override // defpackage.rc1
    public /* bridge */ /* synthetic */ te4 invoke() {
        invoke2();
        return te4.a;
    }

    @Override // defpackage.rc1
    /* renamed from: invoke  reason: avoid collision after fix types in other method */
    public final void invoke2() {
        qy4 qy4Var = this.$safeMoonppp;
        String g = qy4Var == null ? null : qy4Var.g("tempKA");
        if (g != null) {
            bo3.o(this.this$0, "TEMPKA", g);
        }
        AKTLoginActivity.r0.b(this.this$0, false);
        this.this$0.finish();
    }
}
