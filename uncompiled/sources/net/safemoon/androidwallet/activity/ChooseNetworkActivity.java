package net.safemoon.androidwallet.activity;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import androidx.appcompat.app.ActionBar;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import net.safemoon.androidwallet.R;
import net.safemoon.androidwallet.activity.ChooseNetworkActivity;
import net.safemoon.androidwallet.activity.common.BasicActivity;
import net.safemoon.androidwallet.common.TokenType;

/* compiled from: ChooseNetworkActivity.kt */
/* loaded from: classes2.dex */
public final class ChooseNetworkActivity extends BasicActivity {
    public static final a l0 = new a(null);
    public final sy1 j0 = zy1.a(new ChooseNetworkActivity$binding$2(this));
    public final sy1 k0 = zy1.a(new ChooseNetworkActivity$chainNetworkAdapter$2(this));

    /* compiled from: ChooseNetworkActivity.kt */
    /* loaded from: classes2.dex */
    public static final class a {
        public a() {
        }

        public /* synthetic */ a(qi0 qi0Var) {
            this();
        }

        public final Intent a(Context context, TokenType tokenType) {
            fs1.f(context, "context");
            fs1.f(tokenType, "tokenType");
            Intent intent = new Intent(context, ChooseNetworkActivity.class);
            intent.putExtra("ARG_SELECTED_TOKEN_NETWORK", tokenType);
            return intent;
        }
    }

    /* compiled from: ChooseNetworkActivity.kt */
    /* loaded from: classes2.dex */
    public static final class b implements TextWatcher {
        public b() {
        }

        @Override // android.text.TextWatcher
        public void afterTextChanged(Editable editable) {
            ChooseNetworkActivity.this.N().getFilter().filter(editable);
        }

        @Override // android.text.TextWatcher
        public void beforeTextChanged(CharSequence charSequence, int i, int i2, int i3) {
        }

        @Override // android.text.TextWatcher
        public void onTextChanged(CharSequence charSequence, int i, int i2, int i3) {
        }
    }

    public static final void O(ChooseNetworkActivity chooseNetworkActivity, View view) {
        fs1.f(chooseNetworkActivity, "this$0");
        chooseNetworkActivity.onBackPressed();
    }

    public final y6 M() {
        return (y6) this.j0.getValue();
    }

    public final ky N() {
        return (ky) this.k0.getValue();
    }

    @Override // net.safemoon.androidwallet.activity.common.BasicActivity, androidx.fragment.app.FragmentActivity, androidx.activity.ComponentActivity, androidx.core.app.ComponentActivity, android.app.Activity
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        setContentView(M().b());
        ActionBar supportActionBar = getSupportActionBar();
        if (supportActionBar == null) {
            return;
        }
        supportActionBar.l();
    }

    @Override // net.safemoon.androidwallet.activity.common.BasicActivity, androidx.appcompat.app.AppCompatActivity, android.app.Activity
    public void onPostCreate(Bundle bundle) {
        super.onPostCreate(bundle);
        y6 M = M();
        M.d.a.setOnClickListener(new View.OnClickListener() { // from class: jy
            @Override // android.view.View.OnClickListener
            public final void onClick(View view) {
                ChooseNetworkActivity.O(ChooseNetworkActivity.this, view);
            }
        });
        M.d.c.setText(R.string.screen_title_cc_network);
        RecyclerView recyclerView = M.b;
        recyclerView.setLayoutManager(new LinearLayoutManager(recyclerView.getContext(), 1, false));
        recyclerView.setAdapter(N());
        M().c.b.addTextChangedListener(new b());
    }
}
