package net.safemoon.androidwallet.activity;

import android.content.Intent;
import kotlin.jvm.internal.Lambda;

/* compiled from: StartWalletActivity.kt */
/* loaded from: classes2.dex */
public final class StartWalletActivity$isImportNewWalletOnRegister$2 extends Lambda implements rc1<Boolean> {
    public final /* synthetic */ StartWalletActivity this$0;

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public StartWalletActivity$isImportNewWalletOnRegister$2(StartWalletActivity startWalletActivity) {
        super(0);
        this.this$0 = startWalletActivity;
    }

    /* JADX WARN: Can't rename method to resolve collision */
    @Override // defpackage.rc1
    public final Boolean invoke() {
        Intent intent = this.this$0.getIntent();
        return Boolean.valueOf(intent != null ? intent.getBooleanExtra("KEY_IS_IMPORT_NEW_WALLET", false) : false);
    }
}
