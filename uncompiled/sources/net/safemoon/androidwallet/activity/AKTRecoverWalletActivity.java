package net.safemoon.androidwallet.activity;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.os.Bundle;
import androidx.appcompat.app.ActionBar;
import com.AKT.anonymouskey.ui.login.AKTServerFunctions;
import net.safemoon.androidwallet.MyApplicationClass;
import net.safemoon.androidwallet.R;
import net.safemoon.androidwallet.common.TokenType;
import net.safemoon.androidwallet.viewmodels.CreateWalletViewModel;

/* compiled from: AKTRecoverWalletActivity.kt */
/* loaded from: classes2.dex */
public class AKTRecoverWalletActivity extends AKTServerFunctions {
    public final sy1 m0 = new fj4(d53.b(CreateWalletViewModel.class), new AKTRecoverWalletActivity$special$$inlined$viewModels$default$2(this), new AKTRecoverWalletActivity$special$$inlined$viewModels$default$1(this));

    public static final void n0(DialogInterface dialogInterface) {
    }

    public static final void p0(DialogInterface dialogInterface, int i) {
        fs1.f(dialogInterface, "dialog");
        dialogInterface.dismiss();
    }

    @Override // com.AKT.anonymouskey.ui.login.AKTServerFunctions
    public void Q() {
    }

    public final CreateWalletViewModel k0() {
        return (CreateWalletViewModel) this.m0.getValue();
    }

    public final void l0() {
        MyApplicationClass.c().m0 = true;
        bo3.o(this, "DEFAULT_GATEWAY", TokenType.BEP_20.getName());
        AKTHomeActivity.R0(this);
        finish();
    }

    /* JADX WARN: Removed duplicated region for block: B:12:0x0014  */
    /* JADX WARN: Removed duplicated region for block: B:27:0x0046  */
    /*
        Code decompiled incorrectly, please refer to instructions dump.
        To view partially-correct code enable 'Show inconsistent code' option in preferences
    */
    public final void m0(java.lang.String r13, java.lang.String r14, java.lang.String r15) {
        /*
            r12 = this;
            r0 = 0
            r1 = 1
            if (r13 != 0) goto L6
        L4:
            r2 = r0
            goto L12
        L6:
            int r2 = r13.length()
            if (r2 <= 0) goto Le
            r2 = r1
            goto Lf
        Le:
            r2 = r0
        Lf:
            if (r2 != r1) goto L4
            r2 = r1
        L12:
            if (r2 == 0) goto L42
            if (r14 != 0) goto L17
            goto L23
        L17:
            int r2 = r14.length()
            if (r2 <= 0) goto L1f
            r2 = r1
            goto L20
        L1f:
            r2 = r0
        L20:
            if (r2 != r1) goto L23
            r0 = r1
        L23:
            if (r0 == 0) goto L42
            net.safemoon.androidwallet.viewmodels.MultiWalletViewModel r2 = r12.k0
            java.lang.String r0 = "walletViewModel"
            defpackage.fs1.e(r2, r0)
            if (r15 != 0) goto L30
            java.lang.String r15 = ""
        L30:
            r5 = r15
            r6 = 0
            r7 = 0
            r8 = 0
            net.safemoon.androidwallet.activity.AKTRecoverWalletActivity$importWallet$1 r9 = new net.safemoon.androidwallet.activity.AKTRecoverWalletActivity$importWallet$1
            r9.<init>(r12)
            r10 = 56
            r11 = 0
            r3 = r14
            r4 = r13
            net.safemoon.androidwallet.viewmodels.MultiWalletViewModel.n(r2, r3, r4, r5, r6, r7, r8, r9, r10, r11)
            goto L52
        L42:
            net.safemoon.androidwallet.dialogs.ProgressLoading r13 = r12.j0
            if (r13 == 0) goto L49
            r13.h()
        L49:
            r13 = 0
            r14 = 2131952339(0x7f1302d3, float:1.9541118E38)
            w1 r15 = defpackage.w1.a
            defpackage.jc0.c(r12, r13, r14, r1, r15)
        L52:
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: net.safemoon.androidwallet.activity.AKTRecoverWalletActivity.m0(java.lang.String, java.lang.String, java.lang.String):void");
    }

    public final void o0() {
        new AlertDialog.Builder(this).setTitle(getString(R.string.title_invalid_passphrase)).setMessage(getString(R.string.message_invalid_passphrase)).setPositiveButton("Ok", v1.a).show();
    }

    @Override // com.AKT.anonymouskey.ui.login.AKTServerFunctions, net.safemoon.androidwallet.activity.common.BasicActivity, androidx.fragment.app.FragmentActivity, androidx.activity.ComponentActivity, androidx.core.app.ComponentActivity, android.app.Activity
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        ActionBar supportActionBar = getSupportActionBar();
        if (supportActionBar != null) {
            supportActionBar.l();
        }
        getWindow().addFlags(Integer.MIN_VALUE);
        getWindow().setStatusBarColor(m70.d(this, 17170445));
        getWindow().setBackgroundDrawableResource(R.drawable.ic_bg);
    }
}
