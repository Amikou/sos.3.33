package net.safemoon.androidwallet.activity;

import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.TextView;
import androidx.lifecycle.l;
import com.fasterxml.jackson.core.util.MinimalPrettyPrinter;
import java.util.List;
import net.safemoon.androidwallet.R;
import net.safemoon.androidwallet.activity.CreateWalletActivity;
import net.safemoon.androidwallet.activity.common.BasicActivity;
import net.safemoon.androidwallet.viewmodels.CreateWalletViewModel;

/* loaded from: classes2.dex */
public class CreateWalletActivity extends BasicActivity {
    public b7 j0;
    public CreateWalletViewModel k0;

    /* JADX INFO: Access modifiers changed from: private */
    public /* synthetic */ void O(TextView[] textViewArr, List list) {
        if (list == null || list.size() < 12) {
            return;
        }
        for (int i = 0; i < list.size(); i++) {
            textViewArr[i].setText((String) list.get(i));
        }
        this.j0.c.setVisibility(0);
    }

    /* JADX INFO: Access modifiers changed from: private */
    public /* synthetic */ void P(View view) {
        Intent intent = new Intent(this, ConfirmPassphraseActivity.class);
        intent.putExtra("mnemonic", TextUtils.join(MinimalPrettyPrinter.DEFAULT_ROOT_VALUE_SEPARATOR, this.k0.e().getValue()));
        startActivity(intent);
        finish();
    }

    /* JADX INFO: Access modifiers changed from: private */
    public /* synthetic */ void Q(View view) {
        finish();
    }

    public final void N() {
        b7 b7Var = this.j0;
        final TextView[] textViewArr = {b7Var.d, b7Var.h, b7Var.i, b7Var.j, b7Var.k, b7Var.l, b7Var.m, b7Var.n, b7Var.o, b7Var.e, b7Var.f, b7Var.g};
        this.k0.c();
        this.k0.e().observe(this, new tl2() { // from class: ha0
            @Override // defpackage.tl2
            public final void onChanged(Object obj) {
                CreateWalletActivity.this.O(textViewArr, (List) obj);
            }
        });
    }

    @Override // net.safemoon.androidwallet.activity.common.BasicActivity, androidx.fragment.app.FragmentActivity, androidx.activity.ComponentActivity, androidx.core.app.ComponentActivity, android.app.Activity
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        b7 c = b7.c(getLayoutInflater());
        this.j0 = c;
        setContentView(c.b());
        if (getSupportActionBar() != null) {
            getSupportActionBar().l();
        }
        getWindow().addFlags(Integer.MIN_VALUE);
        getWindow().setStatusBarColor(m70.d(this, 17170445));
        getWindow().setBackgroundDrawableResource(R.drawable.ic_bg);
        this.k0 = (CreateWalletViewModel) new l(this).a(CreateWalletViewModel.class);
        N();
        this.j0.c.setOnClickListener(new View.OnClickListener() { // from class: ia0
            @Override // android.view.View.OnClickListener
            public final void onClick(View view) {
                CreateWalletActivity.this.P(view);
            }
        });
        this.j0.b.setOnClickListener(new View.OnClickListener() { // from class: ja0
            @Override // android.view.View.OnClickListener
            public final void onClick(View view) {
                CreateWalletActivity.this.Q(view);
            }
        });
    }

    @Override // androidx.fragment.app.FragmentActivity, android.app.Activity
    public void onPause() {
        super.onPause();
        if (zr.a.booleanValue()) {
            return;
        }
        getWindow().clearFlags(8192);
    }

    @Override // net.safemoon.androidwallet.activity.common.BasicActivity, androidx.appcompat.app.AppCompatActivity, android.app.Activity
    public void onPostCreate(Bundle bundle) {
        super.onPostCreate(bundle);
        C();
    }

    @Override // androidx.fragment.app.FragmentActivity, android.app.Activity
    public void onResume() {
        super.onResume();
        if (zr.a.booleanValue()) {
            return;
        }
        getWindow().addFlags(8192);
    }
}
