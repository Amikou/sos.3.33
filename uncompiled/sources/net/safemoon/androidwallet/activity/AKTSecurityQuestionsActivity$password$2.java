package net.safemoon.androidwallet.activity;

import android.content.Intent;
import kotlin.jvm.internal.Lambda;

/* compiled from: AKTSecurityQuestionsActivity.kt */
/* loaded from: classes2.dex */
public final class AKTSecurityQuestionsActivity$password$2 extends Lambda implements rc1<String> {
    public final /* synthetic */ AKTSecurityQuestionsActivity this$0;

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public AKTSecurityQuestionsActivity$password$2(AKTSecurityQuestionsActivity aKTSecurityQuestionsActivity) {
        super(0);
        this.this$0 = aKTSecurityQuestionsActivity;
    }

    @Override // defpackage.rc1
    public final String invoke() {
        String stringExtra;
        Intent intent = this.this$0.getIntent();
        return (intent == null || (stringExtra = intent.getStringExtra("KEY_PASSWORD")) == null) ? "" : stringExtra;
    }
}
