package net.safemoon.androidwallet.activity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import androidx.appcompat.app.ActionBar;
import androidx.recyclerview.widget.LinearLayoutManager;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Objects;
import net.safemoon.androidwallet.R;
import net.safemoon.androidwallet.activity.SwitchWalletActivity;
import net.safemoon.androidwallet.activity.common.BasicActivity;
import net.safemoon.androidwallet.adapter.touchHelper.RecyclerTouchListener;
import net.safemoon.androidwallet.model.wallets.Wallet;
import net.safemoon.androidwallet.viewmodels.MultiWalletViewModel;

/* compiled from: SwitchWalletActivity.kt */
/* loaded from: classes2.dex */
public final class SwitchWalletActivity extends BasicActivity {
    public final sy1 j0 = zy1.a(new SwitchWalletActivity$binding$2(this));
    public final sy1 k0 = new fj4(d53.b(MultiWalletViewModel.class), new SwitchWalletActivity$special$$inlined$viewModels$default$2(this), new SwitchWalletActivity$special$$inlined$viewModels$default$1(this));
    public final sy1 l0 = zy1.a(new SwitchWalletActivity$walletAdapter$2(this));
    public final sy1 m0 = zy1.a(new SwitchWalletActivity$touchListener$2(this));

    /* compiled from: SwitchWalletActivity.kt */
    /* loaded from: classes2.dex */
    public static final class a {
        public a() {
        }

        public /* synthetic */ a(qi0 qi0Var) {
            this();
        }
    }

    static {
        new a(null);
    }

    public static final void T(SwitchWalletActivity switchWalletActivity, View view) {
        fs1.f(switchWalletActivity, "this$0");
        switchWalletActivity.onBackPressed();
    }

    public static final void U(SwitchWalletActivity switchWalletActivity, View view) {
        fs1.f(switchWalletActivity, "this$0");
        switchWalletActivity.startActivity(new Intent(switchWalletActivity, MainActivity.class));
    }

    public static final void V(SwitchWalletActivity switchWalletActivity, List list) {
        fs1.f(switchWalletActivity, "this$0");
        switchWalletActivity.S().submitList(list);
        if (list == null) {
            return;
        }
        ArrayList arrayList = new ArrayList();
        int size = list.size() - 1;
        if (size >= 0) {
            int i = 0;
            while (true) {
                int i2 = i + 1;
                Long id = ((Wallet) list.get(i)).getId();
                Wallet c = e30.c(switchWalletActivity);
                if (fs1.b(id, c == null ? null : c.getId())) {
                    arrayList.add(Integer.valueOf(i));
                }
                if (i2 > size) {
                    break;
                }
                i = i2;
            }
        }
        if (arrayList.size() > 0) {
            RecyclerTouchListener R = switchWalletActivity.R();
            Object[] array = arrayList.toArray(new Integer[0]);
            Objects.requireNonNull(array, "null cannot be cast to non-null type kotlin.Array<T>");
            Integer[] numArr = (Integer[]) array;
            R.z((Integer[]) Arrays.copyOf(numArr, numArr.length));
        }
    }

    public final b8 P() {
        return (b8) this.j0.getValue();
    }

    public final MultiWalletViewModel Q() {
        return (MultiWalletViewModel) this.k0.getValue();
    }

    public final RecyclerTouchListener R() {
        return (RecyclerTouchListener) this.m0.getValue();
    }

    public final xn4 S() {
        return (xn4) this.l0.getValue();
    }

    @Override // net.safemoon.androidwallet.activity.common.BasicActivity, androidx.fragment.app.FragmentActivity, androidx.activity.ComponentActivity, androidx.core.app.ComponentActivity, android.app.Activity
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        setContentView(P().b());
        ActionBar supportActionBar = getSupportActionBar();
        if (supportActionBar == null) {
            return;
        }
        supportActionBar.l();
    }

    @Override // net.safemoon.androidwallet.activity.common.BasicActivity, androidx.appcompat.app.AppCompatActivity, android.app.Activity
    public void onPostCreate(Bundle bundle) {
        super.onPostCreate(bundle);
        b8 P = P();
        P.b.setLayoutManager(new LinearLayoutManager(this, 1, false));
        P.b.setAdapter(S());
        g74 g74Var = P.c;
        g74Var.c.setOnClickListener(new View.OnClickListener() { // from class: z14
            @Override // android.view.View.OnClickListener
            public final void onClick(View view) {
                SwitchWalletActivity.T(SwitchWalletActivity.this, view);
            }
        });
        g74Var.e.setText(R.string.switch_wallet_screen_title);
        g74Var.b.setOnClickListener(new View.OnClickListener() { // from class: y14
            @Override // android.view.View.OnClickListener
            public final void onClick(View view) {
                SwitchWalletActivity.U(SwitchWalletActivity.this, view);
            }
        });
        Q().t().observe(this, new tl2() { // from class: x14
            @Override // defpackage.tl2
            public final void onChanged(Object obj) {
                SwitchWalletActivity.V(SwitchWalletActivity.this, (List) obj);
            }
        });
        C();
    }
}
