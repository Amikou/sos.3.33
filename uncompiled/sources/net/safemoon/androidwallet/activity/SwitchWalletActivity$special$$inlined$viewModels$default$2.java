package net.safemoon.androidwallet.activity;

import androidx.activity.ComponentActivity;
import kotlin.jvm.internal.Lambda;

/* compiled from: ActivityViewModelLazy.kt */
/* loaded from: classes2.dex */
public final class SwitchWalletActivity$special$$inlined$viewModels$default$2 extends Lambda implements rc1<gj4> {
    public final /* synthetic */ ComponentActivity $this_viewModels;

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public SwitchWalletActivity$special$$inlined$viewModels$default$2(ComponentActivity componentActivity) {
        super(0);
        this.$this_viewModels = componentActivity;
    }

    /* JADX WARN: Can't rename method to resolve collision */
    @Override // defpackage.rc1
    public final gj4 invoke() {
        gj4 viewModelStore = this.$this_viewModels.getViewModelStore();
        fs1.c(viewModelStore, "viewModelStore");
        return viewModelStore;
    }
}
