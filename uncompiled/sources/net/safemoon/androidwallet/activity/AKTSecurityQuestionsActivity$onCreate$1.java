package net.safemoon.androidwallet.activity;

import java.util.Collection;
import java.util.List;
import kotlin.jvm.internal.Lambda;
import net.safemoon.androidwallet.model.wallets.Wallet;

/* compiled from: AKTSecurityQuestionsActivity.kt */
/* loaded from: classes2.dex */
public final class AKTSecurityQuestionsActivity$onCreate$1 extends Lambda implements tc1<List<? extends Wallet>, te4> {
    public final /* synthetic */ AKTSecurityQuestionsActivity this$0;

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public AKTSecurityQuestionsActivity$onCreate$1(AKTSecurityQuestionsActivity aKTSecurityQuestionsActivity) {
        super(1);
        this.this$0 = aKTSecurityQuestionsActivity;
    }

    @Override // defpackage.tc1
    public /* bridge */ /* synthetic */ te4 invoke(List<? extends Wallet> list) {
        invoke2((List<Wallet>) list);
        return te4.a;
    }

    /* renamed from: invoke  reason: avoid collision after fix types in other method */
    public final void invoke2(List<Wallet> list) {
        boolean z;
        fs1.f(list, "list");
        AKTSecurityQuestionsActivity aKTSecurityQuestionsActivity = this.this$0;
        boolean z2 = true;
        if (!(list instanceof Collection) || !list.isEmpty()) {
            for (Wallet wallet2 : list) {
                String u5b64 = wallet2.getU5B64();
                if (u5b64 == null || u5b64.length() == 0) {
                    z = true;
                    continue;
                } else {
                    z = false;
                    continue;
                }
                if (z) {
                    break;
                }
            }
        }
        z2 = false;
        aKTSecurityQuestionsActivity.D0 = z2;
    }
}
