package net.safemoon.androidwallet.activity;

import android.content.Context;
import android.content.Intent;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.view.animation.AlphaAnimation;
import android.view.animation.AnimationUtils;
import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity;
import com.github.mikephil.charting.utils.Utils;
import java.util.List;
import net.safemoon.androidwallet.R;
import net.safemoon.androidwallet.activity.AKTSplashActivity;
import net.safemoon.androidwallet.model.wallets.Wallet;
import net.safemoon.androidwallet.viewmodels.MultiWalletViewModel;

/* compiled from: AKTSplashActivity.kt */
/* loaded from: classes2.dex */
public final class AKTSplashActivity extends AppCompatActivity {
    public final sy1 a = zy1.a(new AKTSplashActivity$binding$2(this));
    public final sy1 f0 = new fj4(d53.b(MultiWalletViewModel.class), new AKTSplashActivity$special$$inlined$viewModels$default$2(this), new AKTSplashActivity$special$$inlined$viewModels$default$1(this));
    public boolean g0;

    public static final void D(AKTSplashActivity aKTSplashActivity) {
        fs1.f(aKTSplashActivity, "this$0");
        if (fs1.b("netMain", "netMainBeta")) {
            if (aKTSplashActivity.I(aKTSplashActivity)) {
                aKTSplashActivity.C();
                return;
            }
            return;
        }
        aKTSplashActivity.C();
    }

    public static final void E(AKTSplashActivity aKTSplashActivity) {
        fs1.f(aKTSplashActivity, "this$0");
        aKTSplashActivity.A().c.setVisibility(0);
        aKTSplashActivity.A().c.setAnimation(AnimationUtils.loadAnimation(aKTSplashActivity, R.anim.rotate_animation));
        aKTSplashActivity.A().d.setVisibility(0);
        aKTSplashActivity.A().e.setVisibility(0);
        aKTSplashActivity.A().f.setVisibility(0);
        aKTSplashActivity.A().g.setVisibility(0);
    }

    public static final void F(AKTSplashActivity aKTSplashActivity) {
        fs1.f(aKTSplashActivity, "this$0");
        AlphaAnimation alphaAnimation = new AlphaAnimation(1.0f, (float) Utils.FLOAT_EPSILON);
        alphaAnimation.setDuration(200L);
        aKTSplashActivity.A().b.setAnimation(alphaAnimation);
    }

    public static final void G(AKTSplashActivity aKTSplashActivity) {
        fs1.f(aKTSplashActivity, "this$0");
        aKTSplashActivity.A().b.setVisibility(4);
        aKTSplashActivity.A().c.setVisibility(4);
        aKTSplashActivity.A().d.setVisibility(4);
        aKTSplashActivity.A().e.setVisibility(4);
        aKTSplashActivity.A().f.setVisibility(4);
        aKTSplashActivity.A().g.setVisibility(4);
    }

    public final z7 A() {
        return (z7) this.a.getValue();
    }

    public final MultiWalletViewModel B() {
        return (MultiWalletViewModel) this.f0.getValue();
    }

    public final void C() {
        Wallet c = e30.c(this);
        if (this.g0) {
            String i = bo3.i(this, "KA");
            fs1.e(i, "getString(this, SharedPrefs.KA)");
            if (i.length() > 0) {
                AKTLoginActivity.r0.a(this, false);
                finish();
            }
        }
        if (c != null && c.getU5B64() == null) {
            startActivity(new Intent(this, EnterPasswordActivity.class));
        } else {
            AKTConfigurationsActivity.l0.a(this);
        }
        finish();
    }

    public final void H() {
        if (this.g0 || e30.c(this) != null) {
            return;
        }
        ly1 ly1Var = ly1.a;
        ly1Var.c(this, ly1Var.a());
    }

    public final boolean I(Context context) {
        List j = b20.j("com.android.vending", "com.google.android.feedback");
        String installerPackageName = context.getPackageManager().getInstallerPackageName(context.getPackageName());
        return installerPackageName != null && j.contains(installerPackageName);
    }

    @Override // androidx.fragment.app.FragmentActivity, androidx.activity.ComponentActivity, androidx.core.app.ComponentActivity, android.app.Activity
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        b30.a.c(this);
        setContentView(A().b());
        ActionBar supportActionBar = getSupportActionBar();
        if (supportActionBar != null) {
            supportActionBar.l();
        }
        getWindow().addFlags(Integer.MIN_VALUE);
        getWindow().setStatusBarColor(m70.d(this, 17170445));
        getWindow().setBackgroundDrawable(new ColorDrawable(getColor(R.color.akt_night_background)));
        A().g.setText("V3.33");
        B().p(new AKTSplashActivity$onCreate$1(this));
    }

    @Override // androidx.appcompat.app.AppCompatActivity, android.app.Activity
    public void onPostCreate(Bundle bundle) {
        super.onPostCreate(bundle);
        new Handler(Looper.getMainLooper()).postDelayed(new Runnable() { // from class: d3
            @Override // java.lang.Runnable
            public final void run() {
                AKTSplashActivity.D(AKTSplashActivity.this);
            }
        }, 2200L);
        A().b.setVisibility(0);
        AlphaAnimation alphaAnimation = new AlphaAnimation((float) Utils.FLOAT_EPSILON, 1.0f);
        alphaAnimation.setDuration(300L);
        A().b.setAnimation(alphaAnimation);
        new Handler(Looper.getMainLooper()).postDelayed(new Runnable() { // from class: f3
            @Override // java.lang.Runnable
            public final void run() {
                AKTSplashActivity.E(AKTSplashActivity.this);
            }
        }, 500L);
        new Handler(Looper.getMainLooper()).postDelayed(new Runnable() { // from class: e3
            @Override // java.lang.Runnable
            public final void run() {
                AKTSplashActivity.F(AKTSplashActivity.this);
            }
        }, 1785L);
        new Handler(Looper.getMainLooper()).postDelayed(new Runnable() { // from class: c3
            @Override // java.lang.Runnable
            public final void run() {
                AKTSplashActivity.G(AKTSplashActivity.this);
            }
        }, 2000L);
    }
}
