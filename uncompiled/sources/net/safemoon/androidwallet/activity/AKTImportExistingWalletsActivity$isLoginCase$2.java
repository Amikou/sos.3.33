package net.safemoon.androidwallet.activity;

import android.content.Intent;
import kotlin.jvm.internal.Lambda;

/* compiled from: AKTImportExistingWalletsActivity.kt */
/* loaded from: classes2.dex */
public final class AKTImportExistingWalletsActivity$isLoginCase$2 extends Lambda implements rc1<Boolean> {
    public final /* synthetic */ AKTImportExistingWalletsActivity this$0;

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public AKTImportExistingWalletsActivity$isLoginCase$2(AKTImportExistingWalletsActivity aKTImportExistingWalletsActivity) {
        super(0);
        this.this$0 = aKTImportExistingWalletsActivity;
    }

    /* JADX WARN: Can't rename method to resolve collision */
    @Override // defpackage.rc1
    public final Boolean invoke() {
        Intent intent = this.this$0.getIntent();
        return Boolean.valueOf(intent != null ? intent.getBooleanExtra("KEY_IS_LOGIN", false) : false);
    }
}
