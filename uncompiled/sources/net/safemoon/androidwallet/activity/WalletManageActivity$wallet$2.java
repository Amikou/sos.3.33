package net.safemoon.androidwallet.activity;

import kotlin.jvm.internal.Lambda;
import net.safemoon.androidwallet.model.wallets.Wallet;

/* compiled from: WalletManageActivity.kt */
/* loaded from: classes2.dex */
public final class WalletManageActivity$wallet$2 extends Lambda implements rc1<Wallet> {
    public final /* synthetic */ WalletManageActivity this$0;

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public WalletManageActivity$wallet$2(WalletManageActivity walletManageActivity) {
        super(0);
        this.this$0 = walletManageActivity;
    }

    @Override // defpackage.rc1
    public final Wallet invoke() {
        String stringExtra = this.this$0.getIntent().getStringExtra("argWallet");
        if (stringExtra == null) {
            return null;
        }
        return Wallet.Companion.toWallet(stringExtra);
    }
}
