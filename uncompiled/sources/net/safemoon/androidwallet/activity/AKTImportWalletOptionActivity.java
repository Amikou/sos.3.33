package net.safemoon.androidwallet.activity;

import android.os.Bundle;
import android.view.View;
import androidx.appcompat.app.ActionBar;
import net.safemoon.androidwallet.MyApplicationClass;
import net.safemoon.androidwallet.R;
import net.safemoon.androidwallet.activity.AKTImportWalletOptionActivity;
import net.safemoon.androidwallet.activity.common.BasicActivity;

/* compiled from: AKTImportWalletOptionActivity.kt */
/* loaded from: classes2.dex */
public final class AKTImportWalletOptionActivity extends BasicActivity {
    public final sy1 j0 = zy1.a(new AKTImportWalletOptionActivity$binding$2(this));

    /* compiled from: AKTImportWalletOptionActivity.kt */
    /* loaded from: classes2.dex */
    public static final class a {
        public a() {
        }

        public /* synthetic */ a(qi0 qi0Var) {
            this();
        }
    }

    static {
        new a(null);
    }

    public static final void P(AKTImportWalletOptionActivity aKTImportWalletOptionActivity, View view) {
        fs1.f(aKTImportWalletOptionActivity, "this$0");
        AKTImportWordActivity.q0.a(aKTImportWalletOptionActivity, 12);
    }

    public static final void Q(AKTImportWalletOptionActivity aKTImportWalletOptionActivity, View view) {
        fs1.f(aKTImportWalletOptionActivity, "this$0");
        AKTImportWordActivity.q0.a(aKTImportWalletOptionActivity, 24);
    }

    public static final void R(AKTImportWalletOptionActivity aKTImportWalletOptionActivity, View view) {
        fs1.f(aKTImportWalletOptionActivity, "this$0");
        AKTImportPrivateKeyActivity.o0.a(aKTImportWalletOptionActivity);
    }

    public static final void S(AKTImportWalletOptionActivity aKTImportWalletOptionActivity, View view) {
        fs1.f(aKTImportWalletOptionActivity, "this$0");
        MyApplicationClass.c().m0 = true;
        AKTHomeActivity.R0(aKTImportWalletOptionActivity);
        aKTImportWalletOptionActivity.finish();
    }

    public final t6 O() {
        return (t6) this.j0.getValue();
    }

    @Override // androidx.activity.ComponentActivity, android.app.Activity
    public void onBackPressed() {
        MyApplicationClass.c().m0 = true;
        AKTHomeActivity.R0(this);
        finish();
    }

    @Override // net.safemoon.androidwallet.activity.common.BasicActivity, androidx.fragment.app.FragmentActivity, androidx.activity.ComponentActivity, androidx.core.app.ComponentActivity, android.app.Activity
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        setContentView(O().b());
        ActionBar supportActionBar = getSupportActionBar();
        if (supportActionBar != null) {
            supportActionBar.l();
        }
        getWindow().addFlags(Integer.MIN_VALUE);
        getWindow().setStatusBarColor(m70.d(this, 17170445));
        getWindow().setBackgroundDrawableResource(R.drawable.ic_bg);
    }

    @Override // net.safemoon.androidwallet.activity.common.BasicActivity, androidx.appcompat.app.AppCompatActivity, android.app.Activity
    public void onPostCreate(Bundle bundle) {
        super.onPostCreate(bundle);
        t6 O = O();
        O.d.setOnClickListener(new View.OnClickListener() { // from class: g1
            @Override // android.view.View.OnClickListener
            public final void onClick(View view) {
                AKTImportWalletOptionActivity.P(AKTImportWalletOptionActivity.this, view);
            }
        });
        O.e.setOnClickListener(new View.OnClickListener() { // from class: f1
            @Override // android.view.View.OnClickListener
            public final void onClick(View view) {
                AKTImportWalletOptionActivity.Q(AKTImportWalletOptionActivity.this, view);
            }
        });
        O.c.setOnClickListener(new View.OnClickListener() { // from class: h1
            @Override // android.view.View.OnClickListener
            public final void onClick(View view) {
                AKTImportWalletOptionActivity.R(AKTImportWalletOptionActivity.this, view);
            }
        });
        O.b.setOnClickListener(new View.OnClickListener() { // from class: e1
            @Override // android.view.View.OnClickListener
            public final void onClick(View view) {
                AKTImportWalletOptionActivity.S(AKTImportWalletOptionActivity.this, view);
            }
        });
        C();
    }
}
