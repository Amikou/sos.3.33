package net.safemoon.androidwallet.activity;

import java.lang.ref.WeakReference;
import java.util.List;
import kotlin.jvm.internal.Lambda;
import net.safemoon.androidwallet.R;
import net.safemoon.androidwallet.activity.SwitchWalletActivity;
import net.safemoon.androidwallet.activity.SwitchWalletActivity$touchListener$2;
import net.safemoon.androidwallet.adapter.touchHelper.RecyclerTouchListener;
import net.safemoon.androidwallet.model.wallets.Wallet;
import net.safemoon.androidwallet.viewmodels.MultiWalletViewModel;

/* compiled from: SwitchWalletActivity.kt */
/* loaded from: classes2.dex */
public final class SwitchWalletActivity$touchListener$2 extends Lambda implements rc1<RecyclerTouchListener> {
    public final /* synthetic */ SwitchWalletActivity this$0;

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public SwitchWalletActivity$touchListener$2(SwitchWalletActivity switchWalletActivity) {
        super(0);
        this.this$0 = switchWalletActivity;
    }

    public static final void b(SwitchWalletActivity switchWalletActivity, int i, int i2) {
        MultiWalletViewModel Q;
        fs1.f(switchWalletActivity, "this$0");
        if (i == R.id.btnDelete) {
            Q = switchWalletActivity.Q();
            List<Wallet> value = Q.t().getValue();
            if (value == null) {
                return;
            }
            bh.P(new WeakReference(switchWalletActivity), (r23 & 2) != 0 ? null : Integer.valueOf((int) R.string.delete_wallet_title), (r23 & 4) != 0 ? null : Integer.valueOf((int) R.string.delete_wallet_msg), (r23 & 8) != 0 ? null : null, (r23 & 16) != 0 ? Integer.valueOf((int) R.string.action_ok) : Integer.valueOf((int) R.string.confirm), (r23 & 32) != 0 ? Integer.valueOf((int) R.string.cancel) : null, (r23 & 64) != 0 ? null : null, (r23 & 128) != 0 ? null : null, new SwitchWalletActivity$touchListener$2$1$1$1(switchWalletActivity, value, i2), SwitchWalletActivity$touchListener$2$1$1$2.INSTANCE);
        }
    }

    @Override // defpackage.rc1
    public final RecyclerTouchListener invoke() {
        b8 P;
        b8 P2;
        SwitchWalletActivity switchWalletActivity = this.this$0;
        P = switchWalletActivity.P();
        RecyclerTouchListener recyclerTouchListener = new RecyclerTouchListener(switchWalletActivity, P.b);
        RecyclerTouchListener x = recyclerTouchListener.x(Integer.valueOf((int) R.id.btnDelete));
        final SwitchWalletActivity switchWalletActivity2 = this.this$0;
        x.y(R.id.rowFG, R.id.rowBG, new RecyclerTouchListener.k() { // from class: a24
            @Override // net.safemoon.androidwallet.adapter.touchHelper.RecyclerTouchListener.k
            public final void a(int i, int i2) {
                SwitchWalletActivity$touchListener$2.b(SwitchWalletActivity.this, i, i2);
            }
        });
        P2 = this.this$0.P();
        P2.b.k(recyclerTouchListener);
        return recyclerTouchListener;
    }
}
