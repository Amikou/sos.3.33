package net.safemoon.androidwallet.activity;

import kotlin.jvm.internal.Lambda;

/* compiled from: ImportPrivateKeyActivity.kt */
/* loaded from: classes2.dex */
public final class ImportPrivateKeyActivity$binding$2 extends Lambda implements rc1<g7> {
    public final /* synthetic */ ImportPrivateKeyActivity this$0;

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public ImportPrivateKeyActivity$binding$2(ImportPrivateKeyActivity importPrivateKeyActivity) {
        super(0);
        this.this$0 = importPrivateKeyActivity;
    }

    /* JADX WARN: Can't rename method to resolve collision */
    @Override // defpackage.rc1
    public final g7 invoke() {
        return g7.c(this.this$0.getLayoutInflater());
    }
}
