package net.safemoon.androidwallet.activity;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.List;
import kotlin.coroutines.jvm.internal.SuspendLambda;
import kotlin.coroutines.jvm.internal.a;
import net.safemoon.androidwallet.ERC20;
import net.safemoon.androidwallet.SafeswapRouter;
import net.safemoon.androidwallet.ui.wallet.Convert;
import org.web3j.protocol.core.DefaultBlockParameterName;

/* compiled from: TestActivity.kt */
@a(c = "net.safemoon.androidwallet.activity.TestActivity$onCreate$1", f = "TestActivity.kt", l = {49}, m = "invokeSuspend")
/* loaded from: classes2.dex */
public final class TestActivity$onCreate$1 extends SuspendLambda implements hd1<c90, q70<? super te4>, Object> {
    public final /* synthetic */ ma0 $credentails;
    public final /* synthetic */ List<String> $path;
    public final /* synthetic */ SafeswapRouter $safeSwapRouter;
    public final /* synthetic */ ERC20 $safemoon;
    public final /* synthetic */ ko4 $web3;
    public int label;

    /* compiled from: TestActivity.kt */
    @a(c = "net.safemoon.androidwallet.activity.TestActivity$onCreate$1$1", f = "TestActivity.kt", l = {}, m = "invokeSuspend")
    /* renamed from: net.safemoon.androidwallet.activity.TestActivity$onCreate$1$1  reason: invalid class name */
    /* loaded from: classes2.dex */
    public static final class AnonymousClass1 extends SuspendLambda implements hd1<c90, q70<? super te4>, Object> {
        public final /* synthetic */ ma0 $credentails;
        public final /* synthetic */ List<String> $path;
        public final /* synthetic */ SafeswapRouter $safeSwapRouter;
        public final /* synthetic */ ERC20 $safemoon;
        public final /* synthetic */ ko4 $web3;
        public int label;

        /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
        public AnonymousClass1(ko4 ko4Var, ma0 ma0Var, ERC20 erc20, SafeswapRouter safeswapRouter, List<String> list, q70<? super AnonymousClass1> q70Var) {
            super(2, q70Var);
            this.$web3 = ko4Var;
            this.$credentails = ma0Var;
            this.$safemoon = erc20;
            this.$safeSwapRouter = safeswapRouter;
            this.$path = list;
        }

        @Override // kotlin.coroutines.jvm.internal.BaseContinuationImpl
        public final q70<te4> create(Object obj, q70<?> q70Var) {
            return new AnonymousClass1(this.$web3, this.$credentails, this.$safemoon, this.$safeSwapRouter, this.$path, q70Var);
        }

        @Override // defpackage.hd1
        public final Object invoke(c90 c90Var, q70<? super te4> q70Var) {
            return ((AnonymousClass1) create(c90Var, q70Var)).invokeSuspend(te4.a);
        }

        @Override // kotlin.coroutines.jvm.internal.BaseContinuationImpl
        public final Object invokeSuspend(Object obj) {
            gs1.d();
            if (this.label == 0) {
                o83.b(obj);
                try {
                    BigInteger balance = this.$web3.ethGetBalance(this.$credentails.getAddress(), DefaultBlockParameterName.LATEST).sendAsync().get().getBalance();
                    fs1.e(balance, "balance.balance");
                    String plainString = Convert.b(new BigDecimal(balance), Convert.Unit.ETHER).toPlainString();
                    fs1.e(plainString, "fromWei(balance.balance.…it.ETHER).toPlainString()");
                    e30.c0(plainString, "BNB Balance");
                } catch (Exception e) {
                    String localizedMessage = e.getLocalizedMessage();
                    fs1.e(localizedMessage, "e.localizedMessage");
                    e30.c0(localizedMessage, "ErrorBNB Balance");
                }
                try {
                    ERC20 erc20 = this.$safemoon;
                    BigInteger bigInteger = BigInteger.ONE;
                    String transactionHash = erc20.p("0xc04c3408EA62778072DBc8c14AA9d52FC77EeE8a", bigInteger.shiftLeft(256).subtract(bigInteger)).sendAsync().get().getTransactionHash();
                    fs1.e(transactionHash, "approval.transactionHash");
                    e30.c0(transactionHash, "Approval");
                } catch (Exception e2) {
                    String localizedMessage2 = e2.getLocalizedMessage();
                    fs1.e(localizedMessage2, "e.localizedMessage");
                    e30.c0(localizedMessage2, "ErrorApproval");
                }
                SafeswapRouter safeswapRouter = this.$safeSwapRouter;
                Convert.Unit unit = Convert.Unit.GWEI;
                List list = safeswapRouter.p(Convert.c("10", unit).toBigInteger(), this.$path).sendAsync().get();
                if (list != null) {
                    try {
                        BigDecimal a = Convert.a(String.valueOf(list.get(0)), unit);
                        String valueOf = String.valueOf(list.get(1));
                        Convert.Unit unit2 = Convert.Unit.ETHER;
                        BigDecimal a2 = Convert.a(valueOf, unit2);
                        String bigDecimal = a.toString();
                        fs1.e(bigDecimal, "estimateSafeMoon.toString()");
                        e30.c0(bigDecimal, "AmountIn Safemoon");
                        String bigDecimal2 = a2.toString();
                        fs1.e(bigDecimal2, "estimateWbnb.toString()");
                        e30.c0(bigDecimal2, "Amount WBNB:");
                        Float[] fArr = {hr.c(1.0f), hr.c(1.0f)};
                        Float[] fArr2 = {hr.c(1000.0f), hr.c(10000.0f)};
                        Float[] fArr3 = {hr.c((fArr2[0].floatValue() * fArr[1].floatValue()) + (fArr[0].floatValue() * fArr2[1].floatValue())), hr.c(fArr[1].floatValue() * fArr2[1].floatValue())};
                        Float[] fArr4 = {fArr3[1], fArr3[0]};
                        fArr4[0] = hr.c(fArr4[0].floatValue() * a2.floatValue());
                        float floatValue = fArr4[0].floatValue() / fArr4[1].floatValue();
                        String bigInteger2 = Convert.c(a.toString(), unit).toBigInteger().toString();
                        fs1.e(bigInteger2, "toWei(estimateSafeMoon.t…toBigInteger().toString()");
                        e30.c0(bigInteger2, "AmountIN");
                        String bigInteger3 = Convert.c(String.valueOf(floatValue), unit2).toBigInteger().toString();
                        fs1.e(bigInteger3, "toWei(amountOutMin.toStr…toBigInteger().toString()");
                        e30.c0(bigInteger3, "AmountOUT");
                        SafeswapRouter safeswapRouter2 = this.$safeSwapRouter;
                        BigInteger bigInteger4 = Convert.c(a.toString(), unit).toBigInteger();
                        BigInteger bigInteger5 = Convert.c(String.valueOf(floatValue), unit2).toBigInteger();
                        List<String> list2 = this.$path;
                        String address = this.$credentails.getAddress();
                        BigInteger valueOf2 = BigInteger.valueOf((System.currentTimeMillis() / 1000) + 60);
                        fs1.e(valueOf2, "BigInteger.valueOf(this)");
                        e30.c0(safeswapRouter2.x(bigInteger4, bigInteger5, list2, address, valueOf2).sendAsync().get().getTransactionHash().toString(), "ResultSwap");
                    } catch (Exception e3) {
                        String localizedMessage3 = e3.getLocalizedMessage();
                        fs1.e(localizedMessage3, "e.localizedMessage");
                        e30.c0(localizedMessage3, "ErrorResultSwap");
                    }
                }
                return te4.a;
            }
            throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        }
    }

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public TestActivity$onCreate$1(ko4 ko4Var, ma0 ma0Var, ERC20 erc20, SafeswapRouter safeswapRouter, List<String> list, q70<? super TestActivity$onCreate$1> q70Var) {
        super(2, q70Var);
        this.$web3 = ko4Var;
        this.$credentails = ma0Var;
        this.$safemoon = erc20;
        this.$safeSwapRouter = safeswapRouter;
        this.$path = list;
    }

    @Override // kotlin.coroutines.jvm.internal.BaseContinuationImpl
    public final q70<te4> create(Object obj, q70<?> q70Var) {
        return new TestActivity$onCreate$1(this.$web3, this.$credentails, this.$safemoon, this.$safeSwapRouter, this.$path, q70Var);
    }

    @Override // defpackage.hd1
    public final Object invoke(c90 c90Var, q70<? super te4> q70Var) {
        return ((TestActivity$onCreate$1) create(c90Var, q70Var)).invokeSuspend(te4.a);
    }

    @Override // kotlin.coroutines.jvm.internal.BaseContinuationImpl
    public final Object invokeSuspend(Object obj) {
        Object d = gs1.d();
        int i = this.label;
        if (i == 0) {
            o83.b(obj);
            AnonymousClass1 anonymousClass1 = new AnonymousClass1(this.$web3, this.$credentails, this.$safemoon, this.$safeSwapRouter, this.$path, null);
            this.label = 1;
            if (d90.b(anonymousClass1, this) == d) {
                return d;
            }
        } else if (i != 1) {
            throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        } else {
            o83.b(obj);
        }
        return te4.a;
    }
}
