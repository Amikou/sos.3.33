package net.safemoon.androidwallet.activity;

import android.content.DialogInterface;
import kotlin.jvm.internal.Lambda;

/* compiled from: WalletManageActivity.kt */
/* loaded from: classes2.dex */
public final class WalletManageActivity$confirmRemoveWallet$1 extends Lambda implements tc1<DialogInterface, te4> {
    public final /* synthetic */ WalletManageActivity this$0;

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public WalletManageActivity$confirmRemoveWallet$1(WalletManageActivity walletManageActivity) {
        super(1);
        this.this$0 = walletManageActivity;
    }

    @Override // defpackage.tc1
    public /* bridge */ /* synthetic */ te4 invoke(DialogInterface dialogInterface) {
        invoke2(dialogInterface);
        return te4.a;
    }

    /* renamed from: invoke  reason: avoid collision after fix types in other method */
    public final void invoke2(DialogInterface dialogInterface) {
        this.this$0.B1();
    }
}
