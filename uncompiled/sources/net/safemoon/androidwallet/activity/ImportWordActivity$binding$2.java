package net.safemoon.androidwallet.activity;

import kotlin.jvm.internal.Lambda;

/* compiled from: ImportWordActivity.kt */
/* loaded from: classes2.dex */
public final class ImportWordActivity$binding$2 extends Lambda implements rc1<i7> {
    public final /* synthetic */ ImportWordActivity this$0;

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public ImportWordActivity$binding$2(ImportWordActivity importWordActivity) {
        super(0);
        this.this$0 = importWordActivity;
    }

    /* JADX WARN: Can't rename method to resolve collision */
    @Override // defpackage.rc1
    public final i7 invoke() {
        return i7.c(this.this$0.getLayoutInflater());
    }
}
