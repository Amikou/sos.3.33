package net.safemoon.androidwallet.activity;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.CompoundButton;
import android.widget.EditText;
import androidx.fragment.app.FragmentManager;
import defpackage.qm1;
import java.lang.ref.WeakReference;
import kotlin.text.StringsKt__StringsKt;
import net.safemoon.androidwallet.MyApplicationClass;
import net.safemoon.androidwallet.R;
import net.safemoon.androidwallet.dialogs.ProgressLoading;
import net.safemoon.androidwallet.model.wallets.Wallet;

/* compiled from: AKTLoginActivity.kt */
/* loaded from: classes2.dex */
public class AKTLoginActivity extends AKTBaseLoginFunctions {
    public static final a r0 = new a(null);
    public final sy1 o0 = zy1.a(new AKTLoginActivity$binding$2(this));
    public final sy1 p0 = zy1.a(new AKTLoginActivity$canGoBack$2(this));
    public final qm1 q0 = new w41();

    /* compiled from: AKTLoginActivity.kt */
    /* loaded from: classes2.dex */
    public static final class a {
        public a() {
        }

        public /* synthetic */ a(qi0 qi0Var) {
            this();
        }

        public final void a(Context context, boolean z) {
            fs1.f(context, "context");
            Intent intent = new Intent(context, AKTLoginActivity.class);
            intent.setFlags(67108864);
            intent.putExtra("CAN_GO_BACK", z);
            context.startActivity(intent);
        }

        public final void b(Context context, boolean z) {
            fs1.f(context, "context");
            Intent intent = new Intent(context, AKTLoginActivity.class);
            intent.putExtra("bundle.KEY_SOURCE", "bundle.SOURCE_LOGOUT");
            intent.setFlags(268468224);
            intent.putExtra("CAN_GO_BACK", z);
            context.startActivity(intent);
        }
    }

    /* compiled from: AKTLoginActivity.kt */
    /* loaded from: classes2.dex */
    public static final class b implements qm1.a {
        public final /* synthetic */ boolean b;

        public b(boolean z) {
            this.b = z;
        }

        @Override // defpackage.qm1.a
        public void a() {
            AKTLoginActivity.this.R0(this.b);
            bo3.n(AKTLoginActivity.this, "TWO_FACTOR", Boolean.valueOf(this.b));
        }

        @Override // defpackage.qm1.a
        public void b(int i) {
            AKTLoginActivity.this.G0().e.setChecked(!this.b);
            AKTLoginActivity.this.R0(!this.b);
            bo3.n(AKTLoginActivity.this, "TWO_FACTOR", Boolean.valueOf(!this.b));
        }
    }

    /* compiled from: AKTLoginActivity.kt */
    /* loaded from: classes2.dex */
    public static final class c implements TextWatcher {
        public final /* synthetic */ l7 a;
        public final /* synthetic */ AKTLoginActivity f0;

        public c(l7 l7Var, AKTLoginActivity aKTLoginActivity) {
            this.a = l7Var;
            this.f0 = aKTLoginActivity;
        }

        @Override // android.text.TextWatcher
        public void afterTextChanged(Editable editable) {
            fs1.f(editable, "s");
            String obj = editable.toString();
            String b = b30.a.b(obj);
            if (fs1.b(obj, b)) {
                this.f0.S0();
                return;
            }
            EditText editText = this.a.g.getEditText();
            if (editText != null) {
                editText.setText(b);
            }
            EditText editText2 = this.a.g.getEditText();
            if (editText2 == null) {
                return;
            }
            editText2.setSelection(b.length());
        }

        @Override // android.text.TextWatcher
        public void beforeTextChanged(CharSequence charSequence, int i, int i2, int i3) {
            fs1.f(charSequence, "s");
        }

        @Override // android.text.TextWatcher
        public void onTextChanged(CharSequence charSequence, int i, int i2, int i3) {
            fs1.f(charSequence, "s");
        }
    }

    /* compiled from: AKTLoginActivity.kt */
    /* loaded from: classes2.dex */
    public static final class d implements TextWatcher {
        public final /* synthetic */ l7 a;
        public final /* synthetic */ AKTLoginActivity f0;

        public d(l7 l7Var, AKTLoginActivity aKTLoginActivity) {
            this.a = l7Var;
            this.f0 = aKTLoginActivity;
        }

        @Override // android.text.TextWatcher
        public void afterTextChanged(Editable editable) {
            fs1.f(editable, "s");
            String obj = editable.toString();
            String b = b30.a.b(obj);
            if (fs1.b(obj, b)) {
                this.f0.S0();
                return;
            }
            EditText editText = this.a.f.getEditText();
            if (editText != null) {
                editText.setText(b);
            }
            EditText editText2 = this.a.f.getEditText();
            if (editText2 == null) {
                return;
            }
            editText2.setSelection(b.length());
        }

        @Override // android.text.TextWatcher
        public void beforeTextChanged(CharSequence charSequence, int i, int i2, int i3) {
            fs1.f(charSequence, "s");
        }

        @Override // android.text.TextWatcher
        public void onTextChanged(CharSequence charSequence, int i, int i2, int i3) {
            fs1.f(charSequence, "s");
        }
    }

    /* compiled from: AKTLoginActivity.kt */
    /* loaded from: classes2.dex */
    public static final class e implements qm1.a {
        public e() {
        }

        @Override // defpackage.qm1.a
        public void a() {
            y54.a("Khang").a("OnBioAuthenticationCallback: onSucceed", new Object[0]);
            AKTLoginActivity.this.P0();
        }

        @Override // defpackage.qm1.a
        public void b(int i) {
            y54.a("Khang").a(fs1.l("OnBioAuthenticationCallback onError ", Integer.valueOf(i)), new Object[0]);
        }
    }

    public static final void F0(AKTLoginActivity aKTLoginActivity, CompoundButton compoundButton, boolean z) {
        fs1.f(aKTLoginActivity, "this$0");
        aKTLoginActivity.q0.b(aKTLoginActivity, new b(z));
    }

    public static final void J0(AKTLoginActivity aKTLoginActivity, View view) {
        fs1.f(aKTLoginActivity, "this$0");
        aKTLoginActivity.onBackPressed();
    }

    public static final void K0(l7 l7Var, AKTLoginActivity aKTLoginActivity, View view) {
        Editable text;
        String obj;
        String obj2;
        Editable text2;
        String obj3;
        fs1.f(l7Var, "$this_apply");
        fs1.f(aKTLoginActivity, "this$0");
        EditText editText = l7Var.g.getEditText();
        String str = "";
        if (editText == null || (text = editText.getText()) == null || (obj = text.toString()) == null || (obj2 = StringsKt__StringsKt.L0(obj).toString()) == null) {
            obj2 = "";
        }
        EditText editText2 = l7Var.f.getEditText();
        if (editText2 != null && (text2 = editText2.getText()) != null && (obj3 = text2.toString()) != null) {
            str = obj3;
        }
        aKTLoginActivity.I0(obj2, str);
    }

    public static final void L0(AKTLoginActivity aKTLoginActivity, View view) {
        fs1.f(aKTLoginActivity, "this$0");
        bh.U(new WeakReference(aKTLoginActivity), Integer.valueOf((int) R.string.acknowledgment_confirm_title), Integer.valueOf((int) R.string.akt_reset_password_dialog_remind_content), R.string.acknowledgment_confirm_button_text, new AKTLoginActivity$onPostCreate$1$3$1(aKTLoginActivity));
    }

    public static final void M0(AKTLoginActivity aKTLoginActivity, View view) {
        fs1.f(aKTLoginActivity, "this$0");
        aKTLoginActivity.startActivity(new Intent(aKTLoginActivity, WipeDataActivity.class));
    }

    public static final void N0(AKTLoginActivity aKTLoginActivity, View view) {
        fs1.f(aKTLoginActivity, "this$0");
        pg4.e(aKTLoginActivity);
    }

    public static final void O0(AKTLoginActivity aKTLoginActivity, View view) {
        fs1.f(aKTLoginActivity, "this$0");
        if (bo3.e(aKTLoginActivity, "TWO_FACTOR", false)) {
            aKTLoginActivity.Q0();
        }
    }

    /* JADX WARN: Code restructure failed: missing block: B:27:0x0054, code lost:
        if (r2 == false) goto L34;
     */
    /*
        Code decompiled incorrectly, please refer to instructions dump.
        To view partially-correct code enable 'Show inconsistent code' option in preferences
    */
    public final void E0() {
        /*
            r5 = this;
            boolean r0 = defpackage.pg4.d(r5)
            r1 = 0
            if (r0 == 0) goto L9
            r0 = r1
            goto Lb
        L9:
            r0 = 8
        Lb:
            l7 r2 = r5.G0()
            androidx.appcompat.widget.AppCompatImageView r2 = r2.b
            r2.setVisibility(r0)
            java.lang.String r0 = "TWO_FACTOR"
            boolean r2 = defpackage.bo3.e(r5, r0, r1)
            r3 = 1
            if (r2 == 0) goto L57
            boolean r2 = r5.n0()
            if (r2 != 0) goto L57
            java.lang.String r2 = "KA"
            java.lang.String r2 = defpackage.bo3.i(r5, r2)
            java.lang.String r4 = "getString(this@AKTLoginActivity, SharedPrefs.KA)"
            defpackage.fs1.e(r2, r4)
            int r2 = r2.length()
            if (r2 <= 0) goto L36
            r2 = r3
            goto L37
        L36:
            r2 = r1
        L37:
            if (r2 == 0) goto L57
            net.safemoon.androidwallet.model.wallets.Wallet r2 = defpackage.e30.c(r5)
            if (r2 != 0) goto L41
        L3f:
            r2 = r1
            goto L54
        L41:
            java.lang.String r2 = r2.getU5B64()
            if (r2 != 0) goto L48
            goto L3f
        L48:
            int r2 = r2.length()
            if (r2 <= 0) goto L50
            r2 = r3
            goto L51
        L50:
            r2 = r1
        L51:
            if (r2 != r3) goto L3f
            r2 = r3
        L54:
            if (r2 == 0) goto L57
            goto L58
        L57:
            r3 = r1
        L58:
            boolean r0 = defpackage.bo3.e(r5, r0, r1)
            l7 r1 = r5.G0()
            com.google.android.material.switchmaterial.SwitchMaterial r1 = r1.e
            r1.setChecked(r0)
            r5.R0(r0)
            l7 r0 = r5.G0()
            com.google.android.material.switchmaterial.SwitchMaterial r0 = r0.e
            u1 r1 = new u1
            r1.<init>()
            r0.setOnCheckedChangeListener(r1)
            if (r3 == 0) goto L81
            boolean r0 = defpackage.pg4.d(r5)
            if (r0 == 0) goto L81
            r5.Q0()
        L81:
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: net.safemoon.androidwallet.activity.AKTLoginActivity.E0():void");
    }

    public final l7 G0() {
        return (l7) this.o0.getValue();
    }

    public final boolean H0() {
        return ((Boolean) this.p0.getValue()).booleanValue();
    }

    public final void I0(String str, String str2) {
        String M;
        ProgressLoading.a aVar = ProgressLoading.y0;
        String string = getString(R.string.loading);
        fs1.e(string, "getString(R.string.loading)");
        boolean z = false;
        ProgressLoading a2 = aVar.a(false, string, "");
        this.j0 = a2;
        FragmentManager supportFragmentManager = getSupportFragmentManager();
        fs1.e(supportFragmentManager, "supportFragmentManager");
        a2.A(supportFragmentManager);
        b30 b30Var = b30.a;
        String n = b30Var.n(str);
        String n2 = b30Var.n(str2);
        if (s.b) {
            M = i2.e(this.l0, n, n2);
            fs1.e(M, "{\n            anonymize(…st if necessary\n        }");
        } else {
            M = M(this.l0, n, n2);
            fs1.e(M, "{\n            anonymizeA…st if necessary\n        }");
        }
        this.l0.c("U", n);
        this.l0.c("P", n2);
        dz4 c2 = q.c(n, n2);
        String e2 = c2.e();
        String g = c2.g();
        bo3.o(this, "TEMPU5", g);
        bo3.o(this, "TEMPK5", e2);
        this.l0.c("U5", g);
        this.l0.c("K5", e2);
        String d2 = lm.d(ay1.f(ay1.h(ay1.a(i2.q(n)), i2.q(n2))));
        this.l0.c("UK5", d2);
        Wallet c3 = e30.c(this);
        if (((c3 == null || c3.getU5B64() == null) ? false : true) && bo3.c(this, "KA")) {
            z = i2.s(bo3.i(this, "KA"), d2);
        }
        if (s.c) {
            s0(M);
        } else if (!z) {
            s0(M);
        } else {
            P0();
        }
    }

    public final void P0() {
        ProgressLoading progressLoading = this.j0;
        if (progressLoading != null) {
            progressLoading.h();
        }
        bo3.m(this, "TIMENOW", i2.A());
        if (o0()) {
            setResult(-1, new Intent());
        } else if (!m0()) {
            String c2 = jv0.c(this, "UK5");
            String c3 = jv0.c(this, "K5");
            String i = bo3.i(this, "U5");
            this.l0.c("K5", c3);
            this.l0.c("UK5", c2);
            this.l0.c("U5", i);
            MyApplicationClass.c().m0 = true;
            AKTHomeActivity.R0(this);
        }
        finish();
    }

    public final void Q0() {
        this.q0.b(this, new e());
    }

    public final void R0(boolean z) {
        if (z) {
            G0().b.setVisibility(0);
        } else {
            G0().b.setVisibility(8);
        }
    }

    /* JADX WARN: Removed duplicated region for block: B:29:0x0051  */
    /* JADX WARN: Removed duplicated region for block: B:39:0x0064  */
    /*
        Code decompiled incorrectly, please refer to instructions dump.
        To view partially-correct code enable 'Show inconsistent code' option in preferences
    */
    public final void S0() {
        /*
            r4 = this;
            l7 r0 = r4.G0()
            com.google.android.material.textfield.TextInputLayout r0 = r0.g
            android.widget.EditText r0 = r0.getEditText()
            r1 = 0
            if (r0 != 0) goto Lf
        Ld:
            r0 = r1
            goto L25
        Lf:
            android.text.Editable r0 = r0.getText()
            if (r0 != 0) goto L16
            goto Ld
        L16:
            java.lang.String r0 = r0.toString()
            if (r0 != 0) goto L1d
            goto Ld
        L1d:
            java.lang.CharSequence r0 = kotlin.text.StringsKt__StringsKt.K0(r0)
            java.lang.String r0 = r0.toString()
        L25:
            l7 r2 = r4.G0()
            com.google.android.material.textfield.TextInputLayout r2 = r2.f
            android.widget.EditText r2 = r2.getEditText()
            if (r2 != 0) goto L32
            goto L3d
        L32:
            android.text.Editable r2 = r2.getText()
            if (r2 != 0) goto L39
            goto L3d
        L39:
            java.lang.String r1 = r2.toString()
        L3d:
            r2 = 1
            r3 = 0
            if (r0 != 0) goto L43
        L41:
            r0 = r3
            goto L4f
        L43:
            int r0 = r0.length()
            if (r0 != 0) goto L4b
            r0 = r2
            goto L4c
        L4b:
            r0 = r3
        L4c:
            if (r0 != r2) goto L41
            r0 = r2
        L4f:
            if (r0 != 0) goto L8e
            if (r1 != 0) goto L55
        L53:
            r0 = r3
            goto L61
        L55:
            int r0 = r1.length()
            if (r0 != 0) goto L5d
            r0 = r2
            goto L5e
        L5d:
            r0 = r3
        L5e:
            if (r0 != r2) goto L53
            r0 = r2
        L61:
            if (r0 == 0) goto L64
            goto L8e
        L64:
            l7 r0 = r4.G0()
            com.google.android.material.button.MaterialButton r0 = r0.c
            r1 = 2131099707(0x7f06003b, float:1.7811775E38)
            int r1 = r4.getColor(r1)
            r0.setTextColor(r1)
            l7 r0 = r4.G0()
            com.google.android.material.button.MaterialButton r0 = r0.c
            r0.setEnabled(r2)
            l7 r0 = r4.G0()
            com.google.android.material.button.MaterialButton r0 = r0.c
            r1 = 2131099676(0x7f06001c, float:1.7811712E38)
            android.content.res.ColorStateList r1 = r4.getColorStateList(r1)
            r0.setBackgroundTintList(r1)
            goto Lb7
        L8e:
            l7 r0 = r4.G0()
            com.google.android.material.button.MaterialButton r0 = r0.c
            r1 = 2131100010(0x7f06016a, float:1.781239E38)
            int r1 = r4.getColor(r1)
            r0.setTextColor(r1)
            l7 r0 = r4.G0()
            com.google.android.material.button.MaterialButton r0 = r0.c
            r0.setEnabled(r3)
            l7 r0 = r4.G0()
            com.google.android.material.button.MaterialButton r0 = r0.c
            r1 = 2131099677(0x7f06001d, float:1.7811714E38)
            android.content.res.ColorStateList r1 = r4.getColorStateList(r1)
            r0.setBackgroundTintList(r1)
        Lb7:
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: net.safemoon.androidwallet.activity.AKTLoginActivity.S0():void");
    }

    @Override // androidx.activity.ComponentActivity, android.app.Activity
    public void onBackPressed() {
        if (o0() || H0()) {
            super.onBackPressed();
        }
    }

    @Override // net.safemoon.androidwallet.activity.AKTBaseLoginFunctions, com.AKT.anonymouskey.ui.login.AKTServerFunctions, net.safemoon.androidwallet.activity.common.BasicActivity, androidx.fragment.app.FragmentActivity, androidx.activity.ComponentActivity, androidx.core.app.ComponentActivity, android.app.Activity
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        setContentView(G0().b());
    }

    /* JADX WARN: Removed duplicated region for block: B:17:0x00c4  */
    /* JADX WARN: Removed duplicated region for block: B:21:0x00d5  */
    @Override // net.safemoon.androidwallet.activity.common.BasicActivity, androidx.appcompat.app.AppCompatActivity, android.app.Activity
    /*
        Code decompiled incorrectly, please refer to instructions dump.
        To view partially-correct code enable 'Show inconsistent code' option in preferences
    */
    public void onPostCreate(android.os.Bundle r5) {
        /*
            r4 = this;
            super.onPostCreate(r5)
            l7 r5 = r4.G0()
            sp3 r0 = r5.h
            androidx.appcompat.widget.Toolbar r0 = r0.b()
            r1 = 8
            r0.setVisibility(r1)
            sp3 r0 = r5.h
            android.widget.TextView r0 = r0.e
            r2 = 2131951678(0x7f13003e, float:1.9539777E38)
            java.lang.String r2 = r4.getString(r2)
            r0.setText(r2)
            sp3 r0 = r5.h
            android.widget.ImageView r0 = r0.b
            r2 = 0
            r0.setVisibility(r2)
            sp3 r0 = r5.h
            android.widget.TextView r0 = r0.c
            r0.setVisibility(r1)
            sp3 r0 = r5.h
            android.widget.TextView r0 = r0.c
            r3 = 2131951895(0x7f130117, float:1.9540217E38)
            java.lang.String r3 = r4.getString(r3)
            r0.setText(r3)
            sp3 r0 = r5.h
            android.widget.ImageView r0 = r0.b
            p1 r3 = new p1
            r3.<init>()
            r0.setOnClickListener(r3)
            com.google.android.material.button.MaterialButton r0 = r5.c
            o1 r3 = new o1
            r3.<init>()
            r0.setOnClickListener(r3)
            android.widget.TextView r0 = r5.i
            q1 r3 = new q1
            r3.<init>()
            r0.setOnClickListener(r3)
            android.widget.TextView r0 = r5.j
            s1 r3 = new s1
            r3.<init>()
            r0.setOnClickListener(r3)
            net.safemoon.androidwallet.model.wallets.Wallet r0 = defpackage.e30.c(r4)
            if (r0 == 0) goto L9b
            java.lang.String r0 = "KA"
            java.lang.String r0 = defpackage.bo3.i(r4, r0)
            java.lang.String r3 = "getString(this@AKTLoginActivity, SharedPrefs.KA)"
            defpackage.fs1.e(r0, r3)
            int r0 = r0.length()
            if (r0 != 0) goto L80
            r0 = 1
            goto L81
        L80:
            r0 = r2
        L81:
            if (r0 == 0) goto L84
            goto L9b
        L84:
            boolean r0 = r4.o0()
            if (r0 == 0) goto La7
            android.widget.TextView r0 = r5.i
            r2 = 4
            r0.setVisibility(r2)
            android.widget.TextView r0 = r5.k
            r0.setVisibility(r1)
            android.widget.TextView r0 = r5.j
            r0.setVisibility(r1)
            goto La7
        L9b:
            androidx.appcompat.widget.AppCompatImageView r0 = r5.b
            r0.setEnabled(r2)
            androidx.appcompat.widget.AppCompatImageView r0 = r5.b
            r1 = 1056964608(0x3f000000, float:0.5)
            r0.setAlpha(r1)
        La7:
            android.widget.LinearLayout r0 = r5.d
            t1 r1 = new t1
            r1.<init>()
            r0.setOnClickListener(r1)
            androidx.appcompat.widget.AppCompatImageView r0 = r5.b
            r1 r1 = new r1
            r1.<init>()
            r0.setOnClickListener(r1)
            com.google.android.material.textfield.TextInputLayout r0 = r5.g
            android.widget.EditText r0 = r0.getEditText()
            if (r0 != 0) goto Lc4
            goto Lcc
        Lc4:
            net.safemoon.androidwallet.activity.AKTLoginActivity$c r1 = new net.safemoon.androidwallet.activity.AKTLoginActivity$c
            r1.<init>(r5, r4)
            r0.addTextChangedListener(r1)
        Lcc:
            com.google.android.material.textfield.TextInputLayout r0 = r5.f
            android.widget.EditText r0 = r0.getEditText()
            if (r0 != 0) goto Ld5
            goto Ldd
        Ld5:
            net.safemoon.androidwallet.activity.AKTLoginActivity$d r1 = new net.safemoon.androidwallet.activity.AKTLoginActivity$d
            r1.<init>(r5, r4)
            r0.addTextChangedListener(r1)
        Ldd:
            r4.S0()
            r4.E0()
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: net.safemoon.androidwallet.activity.AKTLoginActivity.onPostCreate(android.os.Bundle):void");
    }
}
