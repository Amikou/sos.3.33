package net.safemoon.androidwallet.activity;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.text.TextUtils;
import android.widget.EditText;
import androidx.lifecycle.l;
import com.fasterxml.jackson.core.util.MinimalPrettyPrinter;
import java.lang.ref.WeakReference;
import java.util.ArrayList;
import net.safemoon.androidwallet.MyApplicationClass;
import net.safemoon.androidwallet.R;
import net.safemoon.androidwallet.activity.RecoverWalletActivity;
import net.safemoon.androidwallet.activity.common.BasicActivity;
import net.safemoon.androidwallet.dialogs.ProgressLoading;
import net.safemoon.androidwallet.viewmodels.CreateWalletViewModel;
import net.safemoon.androidwallet.viewmodels.MultiWalletViewModel;
import wallet.core.jni.HDWallet;

/* loaded from: classes2.dex */
public class RecoverWalletActivity extends BasicActivity {
    public CreateWalletViewModel j0;
    public MultiWalletViewModel k0;
    public b7 l0;

    public static /* synthetic */ void U(DialogInterface dialogInterface) {
    }

    /* JADX INFO: Access modifiers changed from: private */
    public /* synthetic */ te4 V(ProgressLoading progressLoading, Long l) {
        progressLoading.h();
        if (l != null && l.longValue() > 0) {
            MyApplicationClass.c().m0 = true;
            do3.a.h(this, false);
            AKTHomeActivity.R0(this);
        } else {
            bh.U(new WeakReference(this), Integer.valueOf((int) R.string.warning_iw_failed_title), Integer.valueOf((int) R.string.warning_iw_failed_msg), R.string.action_ok, null);
        }
        return null;
    }

    /* JADX INFO: Access modifiers changed from: private */
    public /* synthetic */ te4 W(final ProgressLoading progressLoading, String str, String str2, String str3, Boolean bool) {
        if (bool.booleanValue()) {
            progressLoading.h();
            jc0.c(this, Integer.valueOf((int) R.string.warning_title), R.string.warning_black_list_address, true, x43.a);
            return null;
        } else if (str != null) {
            this.k0.m(str2, str, str3, null, true, false, new tc1() { // from class: q43
                @Override // defpackage.tc1
                public final Object invoke(Object obj) {
                    te4 V;
                    V = RecoverWalletActivity.this.V(progressLoading, (Long) obj);
                    return V;
                }
            });
            return null;
        } else {
            progressLoading.h();
            e30.a0(this, getString(R.string.nft_get_data_error));
            return null;
        }
    }

    public static /* synthetic */ void X(DialogInterface dialogInterface) {
    }

    /* JADX INFO: Access modifiers changed from: private */
    public /* synthetic */ te4 Y(ProgressLoading progressLoading, Long l) {
        progressLoading.h();
        if (l != null && l.longValue() > 0) {
            MyApplicationClass.c().m0 = true;
            do3.a.h(this, false);
            AKTHomeActivity.R0(this);
        } else {
            bh.U(new WeakReference(this), Integer.valueOf((int) R.string.warning_iw_failed_title), Integer.valueOf((int) R.string.warning_iw_failed_msg), R.string.action_ok, null);
        }
        return null;
    }

    /* JADX INFO: Access modifiers changed from: private */
    public /* synthetic */ te4 Z(final ProgressLoading progressLoading, String str, String str2, String str3, Boolean bool) {
        if (bool.booleanValue()) {
            progressLoading.h();
            jc0.c(this, Integer.valueOf((int) R.string.warning_title), R.string.warning_black_list_address, true, w43.a);
            return null;
        } else if (str != null) {
            this.k0.m(str2, str, str3, null, true, false, new tc1() { // from class: r43
                @Override // defpackage.tc1
                public final Object invoke(Object obj) {
                    te4 Y;
                    Y = RecoverWalletActivity.this.Y(progressLoading, (Long) obj);
                    return Y;
                }
            });
            return null;
        } else {
            progressLoading.h();
            e30.a0(this, getString(R.string.nft_get_data_error));
            return null;
        }
    }

    public static /* synthetic */ void a0(DialogInterface dialogInterface) {
    }

    public void S(String str) {
        try {
            this.j0.d(str);
            if (e30.c(this) != null) {
                final ProgressLoading a = ProgressLoading.y0.a(false, getString(R.string.loading), "");
                a.A(getSupportFragmentManager());
                this.j0.b(str, CreateWalletViewModel.KEY_TYPE.PRIVATE_KEY, new md1() { // from class: t43
                    @Override // defpackage.md1
                    public final Object invoke(Object obj, Object obj2, Object obj3, Object obj4) {
                        te4 Z;
                        Z = RecoverWalletActivity.this.Z(a, (String) obj, (String) obj2, (String) obj3, (Boolean) obj4);
                        return Z;
                    }
                });
            } else {
                SetPasswordActivity.D(this, str, CreateWalletViewModel.KEY_TYPE.PRIVATE_KEY);
                finish();
            }
        } catch (Exception unused) {
            jc0.c(this, Integer.valueOf((int) R.string.error_iw_title), R.string.error_iw_msg, true, v43.a);
        }
    }

    public void T(EditText... editTextArr) {
        if (d0(new EditText[0])) {
            ArrayList arrayList = new ArrayList();
            for (EditText editText : editTextArr) {
                arrayList.add(editText.getText().toString().trim().toLowerCase());
            }
            String join = TextUtils.join(MinimalPrettyPrinter.DEFAULT_ROOT_VALUE_SEPARATOR, arrayList);
            if (!HDWallet.isValid(join)) {
                c0();
            } else if (e30.c(this) != null) {
                final ProgressLoading a = ProgressLoading.y0.a(false, getString(R.string.loading), "");
                a.A(getSupportFragmentManager());
                this.j0.b(join, CreateWalletViewModel.KEY_TYPE.PASSPHRASE, new md1() { // from class: s43
                    @Override // defpackage.md1
                    public final Object invoke(Object obj, Object obj2, Object obj3, Object obj4) {
                        te4 W;
                        W = RecoverWalletActivity.this.W(a, (String) obj, (String) obj2, (String) obj3, (Boolean) obj4);
                        return W;
                    }
                });
            } else {
                SetPasswordActivity.D(this, join, CreateWalletViewModel.KEY_TYPE.PASSPHRASE);
                finish();
            }
        }
    }

    public final void c0() {
        new AlertDialog.Builder(this).setTitle(getString(R.string.title_invalid_passphrase)).setMessage(getString(R.string.message_invalid_passphrase)).setPositiveButton("Ok", u43.a).show();
    }

    public final boolean d0(EditText... editTextArr) {
        boolean z = true;
        for (EditText editText : editTextArr) {
            if (TextUtils.isEmpty(editText.getText())) {
                editText.setError(getString(R.string.err_required_field));
                z = false;
            }
        }
        return z;
    }

    @Override // net.safemoon.androidwallet.activity.common.BasicActivity, androidx.fragment.app.FragmentActivity, androidx.activity.ComponentActivity, androidx.core.app.ComponentActivity, android.app.Activity
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        this.j0 = (CreateWalletViewModel) new l(this).a(CreateWalletViewModel.class);
        this.k0 = (MultiWalletViewModel) new l(this).a(MultiWalletViewModel.class);
        b7 c = b7.c(getLayoutInflater());
        this.l0 = c;
        setContentView(c.b());
        if (!zr.a.booleanValue()) {
            getWindow().setFlags(8192, 8192);
        }
        if (getSupportActionBar() != null) {
            getSupportActionBar().l();
        }
        getWindow().addFlags(Integer.MIN_VALUE);
        getWindow().setStatusBarColor(m70.d(this, 17170445));
        getWindow().setBackgroundDrawableResource(R.drawable.ic_bg);
    }

    @Override // net.safemoon.androidwallet.activity.common.BasicActivity, androidx.appcompat.app.AppCompatActivity, android.app.Activity
    public void onPostCreate(Bundle bundle) {
        super.onPostCreate(bundle);
    }
}
