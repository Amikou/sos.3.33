package net.safemoon.androidwallet.activity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.animation.AnimationUtils;
import android.widget.CompoundButton;
import androidx.fragment.app.j;
import net.safemoon.androidwallet.R;
import net.safemoon.androidwallet.activity.MainActivity;
import net.safemoon.androidwallet.activity.common.BasicActivity;
import net.safemoon.androidwallet.fragments.FullScreenWebViewFragment;

/* loaded from: classes2.dex */
public class MainActivity extends BasicActivity {
    public m7 j0;
    public gb2<Boolean> k0 = new gb2<>(Boolean.FALSE);

    /* JADX INFO: Access modifiers changed from: private */
    public /* synthetic */ void P(View view) {
        if (this.k0.getValue().booleanValue()) {
            startActivity(new Intent(this, CreateWalletActivity.class));
        } else {
            U();
        }
    }

    /* JADX INFO: Access modifiers changed from: private */
    public /* synthetic */ void Q(View view) {
        if (this.k0.getValue().booleanValue()) {
            ImportWalletOptionActivity.k0.a(this);
        } else {
            U();
        }
    }

    /* JADX INFO: Access modifiers changed from: private */
    public /* synthetic */ void R(View view) {
        pg4.e(this);
        j n = getSupportFragmentManager().n();
        n.s(R.id.content_main, FullScreenWebViewFragment.j0.a("https://safemoon.com/legal/wallet/eula ", R.string.terms_of_service, false));
        n.h(FullScreenWebViewFragment.class.getSimpleName());
        n.j();
    }

    /* JADX INFO: Access modifiers changed from: private */
    public /* synthetic */ void S(CompoundButton compoundButton, boolean z) {
        this.k0.setValue(Boolean.valueOf(z));
    }

    /* JADX INFO: Access modifiers changed from: private */
    public /* synthetic */ void T(View view) {
        onBackPressed();
    }

    public final void U() {
        e30.E(this);
        this.j0.b.startAnimation(AnimationUtils.loadAnimation(getApplicationContext(), R.anim.shake));
    }

    @Override // net.safemoon.androidwallet.activity.common.BasicActivity, androidx.fragment.app.FragmentActivity, androidx.activity.ComponentActivity, androidx.core.app.ComponentActivity, android.app.Activity
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        m7 c = m7.c(getLayoutInflater());
        this.j0 = c;
        setContentView(c.b());
        if (getSupportActionBar() != null) {
            getSupportActionBar().l();
        }
        getWindow().addFlags(Integer.MIN_VALUE);
        getWindow().setStatusBarColor(m70.d(this, 17170445));
        getWindow().setBackgroundDrawableResource(R.drawable.ic_bg);
        this.j0.d.setOnClickListener(new View.OnClickListener() { // from class: a32
            @Override // android.view.View.OnClickListener
            public final void onClick(View view) {
                MainActivity.this.P(view);
            }
        });
        this.j0.f.setOnClickListener(new View.OnClickListener() { // from class: z22
            @Override // android.view.View.OnClickListener
            public final void onClick(View view) {
                MainActivity.this.Q(view);
            }
        });
        this.j0.c.setOnClickListener(new View.OnClickListener() { // from class: c32
            @Override // android.view.View.OnClickListener
            public final void onClick(View view) {
                MainActivity.this.R(view);
            }
        });
        this.j0.b.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() { // from class: d32
            @Override // android.widget.CompoundButton.OnCheckedChangeListener
            public final void onCheckedChanged(CompoundButton compoundButton, boolean z) {
                MainActivity.this.S(compoundButton, z);
            }
        });
        this.j0.e.setOnClickListener(new View.OnClickListener() { // from class: b32
            @Override // android.view.View.OnClickListener
            public final void onClick(View view) {
                MainActivity.this.T(view);
            }
        });
    }

    @Override // net.safemoon.androidwallet.activity.common.BasicActivity, androidx.appcompat.app.AppCompatActivity, android.app.Activity
    public void onPostCreate(Bundle bundle) {
        super.onPostCreate(bundle);
        C();
    }
}
