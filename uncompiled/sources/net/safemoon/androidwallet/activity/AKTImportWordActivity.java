package net.safemoon.androidwallet.activity;

import android.content.ClipData;
import android.content.ClipboardManager;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import androidx.appcompat.widget.AppCompatEditText;
import androidx.fragment.app.FragmentManager;
import com.fasterxml.jackson.core.util.MinimalPrettyPrinter;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.Objects;
import kotlin.text.StringsKt__StringsKt;
import net.safemoon.androidwallet.R;
import net.safemoon.androidwallet.activity.AKTImportWordActivity;
import net.safemoon.androidwallet.dialogs.ProgressLoading;
import net.safemoon.androidwallet.viewmodels.CreateWalletViewModel;
import wallet.core.jni.HDWallet;

/* compiled from: AKTImportWordActivity.kt */
/* loaded from: classes2.dex */
public final class AKTImportWordActivity extends AKTRecoverWalletActivity {
    public static final a q0 = new a(null);
    public final sy1 n0 = zy1.a(new AKTImportWordActivity$manager$2(this));
    public final sy1 o0 = zy1.a(new AKTImportWordActivity$binding$2(this));
    public final sy1 p0 = zy1.a(new AKTImportWordActivity$wordCount$2(this));

    /* compiled from: AKTImportWordActivity.kt */
    /* loaded from: classes2.dex */
    public static final class a {
        public a() {
        }

        public /* synthetic */ a(qi0 qi0Var) {
            this();
        }

        public final void a(Context context, int i) {
            fs1.f(context, "context");
            Intent intent = new Intent(context, AKTImportWordActivity.class);
            intent.putExtra("ARG_WORD_COUNTS", i);
            te4 te4Var = te4.a;
            context.startActivity(intent);
        }
    }

    public static final void D0(AKTImportWordActivity aKTImportWordActivity, EditText[] editTextArr, View view) {
        fs1.f(aKTImportWordActivity, "this$0");
        fs1.f(editTextArr, "$editTextView");
        ClipData primaryClip = aKTImportWordActivity.A0().getPrimaryClip();
        if (primaryClip != null && primaryClip.getItemCount() > 0) {
            int i = 0;
            if (primaryClip.getItemAt(0).getText() != null) {
                String D = dv3.D(primaryClip.getItemAt(0).getText().toString(), "|", MinimalPrettyPrinter.DEFAULT_ROOT_VALUE_SEPARATOR, false, 4, null);
                Objects.requireNonNull(D, "null cannot be cast to non-null type kotlin.CharSequence");
                List w0 = StringsKt__StringsKt.w0(StringsKt__StringsKt.K0(D).toString(), new String[]{MinimalPrettyPrinter.DEFAULT_ROOT_VALUE_SEPARATOR}, false, 0, 6, null);
                if (w0.size() == aKTImportWordActivity.B0()) {
                    int length = editTextArr.length;
                    int i2 = 0;
                    while (i < length) {
                        EditText editText = editTextArr[i];
                        int i3 = i2 + 1;
                        if (editText != null && i2 < w0.size()) {
                            editText.setText((CharSequence) w0.get(i2));
                        }
                        i++;
                        i2 = i3;
                    }
                    return;
                }
                aKTImportWordActivity.H0();
                return;
            }
        }
        aKTImportWordActivity.H0();
    }

    public static final void E0(AKTImportWordActivity aKTImportWordActivity, EditText[] editTextArr, View view) {
        fs1.f(aKTImportWordActivity, "this$0");
        fs1.f(editTextArr, "$editTextView");
        aKTImportWordActivity.J0(editTextArr);
    }

    public static final void F0(AKTImportWordActivity aKTImportWordActivity, EditText[] editTextArr, View view) {
        fs1.f(aKTImportWordActivity, "this$0");
        fs1.f(editTextArr, "$editTextView");
        ProgressLoading.a aVar = ProgressLoading.y0;
        String string = aKTImportWordActivity.getString(R.string.loading);
        fs1.e(string, "getString(R.string.loading)");
        ProgressLoading a2 = aVar.a(false, string, "");
        aKTImportWordActivity.j0 = a2;
        FragmentManager supportFragmentManager = aKTImportWordActivity.getSupportFragmentManager();
        fs1.e(supportFragmentManager, "supportFragmentManager");
        a2.A(supportFragmentManager);
        aKTImportWordActivity.y0(editTextArr);
    }

    public static final void G0(AKTImportWordActivity aKTImportWordActivity, View view) {
        fs1.f(aKTImportWordActivity, "this$0");
        aKTImportWordActivity.onBackPressed();
    }

    public static final void I0(DialogInterface dialogInterface) {
    }

    public final ClipboardManager A0() {
        return (ClipboardManager) this.n0.getValue();
    }

    public final int B0() {
        return ((Number) this.p0.getValue()).intValue();
    }

    public final void C0() {
        i7 z0 = z0();
        int B0 = B0();
        Integer[] numArr = new Integer[B0];
        for (int i = 0; i < B0; i++) {
            numArr[i] = 1;
        }
        int B02 = B0();
        final EditText[] editTextArr = new EditText[B02];
        for (int i2 = 0; i2 < B02; i2++) {
            editTextArr[i2] = null;
        }
        int B03 = B0();
        if (B03 > 0) {
            int i3 = 0;
            while (true) {
                int i4 = i3 + 1;
                View inflate = LayoutInflater.from(this).inflate(R.layout.include_word, (ViewGroup) z0.f, false);
                inflate.setId(Math.abs(fs1.l("Item", Integer.valueOf(i4)).hashCode()));
                cq1 a2 = cq1.a(inflate);
                int abs = Math.abs(fs1.l("Word", Integer.valueOf(i4)).hashCode());
                int abs2 = Math.abs(fs1.l("Word", Integer.valueOf(i3 + 2)).hashCode());
                a2.a.setText("" + i4 + '.');
                AppCompatEditText appCompatEditText = (AppCompatEditText) inflate.findViewWithTag("word");
                appCompatEditText.setId(abs);
                appCompatEditText.setNextFocusDownId(abs2);
                editTextArr[i3] = appCompatEditText;
                numArr[i3] = Integer.valueOf(inflate.getId());
                z0.f.addView(inflate);
                if (i4 >= B03) {
                    break;
                }
                i3 = i4;
            }
        }
        z0.g.setReferencedIds(ai.I(numArr));
        z0.d.setOnClickListener(new View.OnClickListener() { // from class: k1
            @Override // android.view.View.OnClickListener
            public final void onClick(View view) {
                AKTImportWordActivity.D0(AKTImportWordActivity.this, editTextArr, view);
            }
        });
        z0.e.setOnClickListener(new View.OnClickListener() { // from class: l1
            @Override // android.view.View.OnClickListener
            public final void onClick(View view) {
                AKTImportWordActivity.E0(AKTImportWordActivity.this, editTextArr, view);
            }
        });
        z0.c.setOnClickListener(new View.OnClickListener() { // from class: m1
            @Override // android.view.View.OnClickListener
            public final void onClick(View view) {
                AKTImportWordActivity.F0(AKTImportWordActivity.this, editTextArr, view);
            }
        });
        z0.b.setOnClickListener(new View.OnClickListener() { // from class: j1
            @Override // android.view.View.OnClickListener
            public final void onClick(View view) {
                AKTImportWordActivity.G0(AKTImportWordActivity.this, view);
            }
        });
    }

    public final void H0() {
        String string = getString(R.string.warning_wrong_phrase_title);
        String string2 = getString(R.string.warning_wrong_phrase_msg, new Object[]{Integer.valueOf(B0())});
        fs1.e(string2, "getString(R.string.warni…ng_phrase_msg, wordCount)");
        jc0.d(this, string, string2, true, i1.a);
    }

    public final void J0(EditText[] editTextArr) {
        w7<Intent> b;
        hn2 D = D();
        if (D == null || (b = D.b(new AKTImportWordActivity$startScanQRCode$1(this, editTextArr))) == null) {
            return;
        }
        b.a(new Intent(this, ScannedCodeActivity.class));
    }

    @Override // net.safemoon.androidwallet.activity.AKTRecoverWalletActivity, com.AKT.anonymouskey.ui.login.AKTServerFunctions, net.safemoon.androidwallet.activity.common.BasicActivity, androidx.fragment.app.FragmentActivity, androidx.activity.ComponentActivity, androidx.core.app.ComponentActivity, android.app.Activity
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        setContentView(z0().b());
    }

    @Override // net.safemoon.androidwallet.activity.common.BasicActivity, androidx.appcompat.app.AppCompatActivity, android.app.Activity
    public void onPostCreate(Bundle bundle) {
        super.onPostCreate(bundle);
        C0();
    }

    public final void y0(EditText[] editTextArr) {
        ArrayList arrayList = new ArrayList();
        int length = editTextArr.length;
        int i = 0;
        while (i < length) {
            EditText editText = editTextArr[i];
            i++;
            if (editText != null) {
                String obj = editText.getText().toString();
                int length2 = obj.length() - 1;
                int i2 = 0;
                boolean z = false;
                while (i2 <= length2) {
                    boolean z2 = fs1.h(obj.charAt(!z ? i2 : length2), 32) <= 0;
                    if (z) {
                        if (!z2) {
                            break;
                        }
                        length2--;
                    } else if (z2) {
                        i2++;
                    } else {
                        z = true;
                    }
                }
                String obj2 = obj.subSequence(i2, length2 + 1).toString();
                Locale locale = Locale.getDefault();
                fs1.e(locale, "getDefault()");
                Objects.requireNonNull(obj2, "null cannot be cast to non-null type java.lang.String");
                String lowerCase = obj2.toLowerCase(locale);
                fs1.e(lowerCase, "(this as java.lang.String).toLowerCase(locale)");
                arrayList.add(lowerCase);
            }
        }
        String join = TextUtils.join(MinimalPrettyPrinter.DEFAULT_ROOT_VALUE_SEPARATOR, arrayList);
        String join2 = TextUtils.join("|", arrayList);
        if (!HDWallet.isValid(join)) {
            o0();
            return;
        }
        CreateWalletViewModel k0 = k0();
        fs1.e(join, "mnemonic");
        k0.b(join, CreateWalletViewModel.KEY_TYPE.PASSPHRASE, new AKTImportWordActivity$checkUser$2(this, join2));
    }

    public final i7 z0() {
        return (i7) this.o0.getValue();
    }
}
