package net.safemoon.androidwallet.activity;

import kotlin.jvm.internal.Lambda;

/* compiled from: ImportWalletOptionActivity.kt */
/* loaded from: classes2.dex */
public final class ImportWalletOptionActivity$binding$2 extends Lambda implements rc1<h7> {
    public final /* synthetic */ ImportWalletOptionActivity this$0;

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public ImportWalletOptionActivity$binding$2(ImportWalletOptionActivity importWalletOptionActivity) {
        super(0);
        this.this$0 = importWalletOptionActivity;
    }

    /* JADX WARN: Can't rename method to resolve collision */
    @Override // defpackage.rc1
    public final h7 invoke() {
        return h7.c(this.this$0.getLayoutInflater());
    }
}
