package net.safemoon.androidwallet.activity;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.content.res.ColorStateList;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.os.Handler;
import android.os.Looper;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.view.Window;
import android.widget.EditText;
import androidx.appcompat.app.ActionBar;
import androidx.fragment.app.FragmentManager;
import com.AKT.anonymouskey.ui.login.AKTServerFunctions;
import com.fasterxml.jackson.core.util.MinimalPrettyPrinter;
import defpackage.y54;
import java.lang.ref.WeakReference;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.Objects;
import kotlin.text.StringsKt__StringsKt;
import net.safemoon.androidwallet.R;
import net.safemoon.androidwallet.activity.AKTVerifyEmailActivity;
import net.safemoon.androidwallet.dialogs.ProgressLoading;
import net.safemoon.androidwallet.viewmodels.AKTWebSocketHandlingViewModel;

/* compiled from: AKTVerifyEmailActivity.kt */
/* loaded from: classes2.dex */
public final class AKTVerifyEmailActivity extends AKTServerFunctions {
    public static final a x0 = new a(null);
    public CountDownTimer r0;
    public long s0;
    public boolean w0;
    public final sy1 m0 = zy1.a(new AKTVerifyEmailActivity$binding$2(this));
    public final sy1 n0 = zy1.a(new AKTVerifyEmailActivity$isChangeEmail$2(this));
    public String o0 = "";
    public int p0 = 3;
    public final sy1 q0 = new fj4(d53.b(AKTWebSocketHandlingViewModel.class), new AKTVerifyEmailActivity$special$$inlined$viewModels$default$2(this), new AKTVerifyEmailActivity$special$$inlined$viewModels$default$1(this));
    public final int t0 = 900;
    public final gb2<Integer> u0 = new gb2<>(0);
    public final int v0 = 60;

    /* compiled from: AKTVerifyEmailActivity.kt */
    /* loaded from: classes2.dex */
    public static final class a {
        public a() {
        }

        public /* synthetic */ a(qi0 qi0Var) {
            this();
        }

        public final void a(Context context, String str, boolean z) {
            fs1.f(context, "context");
            fs1.f(str, "verifyCode");
            Intent intent = new Intent(context, AKTVerifyEmailActivity.class);
            intent.putExtra("IS_CHANGE_EMAIL", z);
            intent.putExtra("VERIFY_CODE", str);
            context.startActivity(intent);
        }
    }

    /* compiled from: AKTVerifyEmailActivity.kt */
    /* loaded from: classes2.dex */
    public static final class b implements TextWatcher {
        public final /* synthetic */ c8 f0;

        public b(c8 c8Var) {
            this.f0 = c8Var;
        }

        @Override // android.text.TextWatcher
        public void afterTextChanged(Editable editable) {
            fs1.f(editable, "s");
            String obj = editable.toString();
            AKTVerifyEmailActivity.this.S0(obj);
            Objects.requireNonNull(obj, "null cannot be cast to non-null type java.lang.String");
            Locale locale = Locale.ROOT;
            String upperCase = obj.toUpperCase(locale);
            fs1.e(upperCase, "(this as java.lang.Strin….toUpperCase(Locale.ROOT)");
            if (fs1.b(obj, upperCase)) {
                return;
            }
            EditText editText = this.f0.f.getEditText();
            if (editText != null) {
                String upperCase2 = obj.toUpperCase(locale);
                fs1.e(upperCase2, "(this as java.lang.Strin….toUpperCase(Locale.ROOT)");
                editText.setText(upperCase2);
            }
            EditText editText2 = this.f0.f.getEditText();
            if (editText2 == null) {
                return;
            }
            editText2.setSelection(obj.length());
        }

        @Override // android.text.TextWatcher
        public void beforeTextChanged(CharSequence charSequence, int i, int i2, int i3) {
            fs1.f(charSequence, "s");
        }

        @Override // android.text.TextWatcher
        public void onTextChanged(CharSequence charSequence, int i, int i2, int i3) {
            fs1.f(charSequence, "s");
        }
    }

    /* compiled from: AKTVerifyEmailActivity.kt */
    /* loaded from: classes2.dex */
    public static final class c extends CountDownTimer {
        public c(long j) {
            super(j, 1000L);
        }

        public static final void b(AKTVerifyEmailActivity aKTVerifyEmailActivity) {
            fs1.f(aKTVerifyEmailActivity, "this$0");
            Integer num = (Integer) aKTVerifyEmailActivity.u0.getValue();
            if (num == null) {
                num = 0;
            }
            aKTVerifyEmailActivity.T0(num.intValue());
            aKTVerifyEmailActivity.w0().e.setEnabled(true);
            aKTVerifyEmailActivity.w0().e.setTextColor(aKTVerifyEmailActivity.getColor(R.color.akt_sign_in_wipe));
        }

        @Override // android.os.CountDownTimer
        public void onFinish() {
            Handler handler = new Handler(Looper.getMainLooper());
            final AKTVerifyEmailActivity aKTVerifyEmailActivity = AKTVerifyEmailActivity.this;
            handler.post(new Runnable() { // from class: s3
                @Override // java.lang.Runnable
                public final void run() {
                    AKTVerifyEmailActivity.c.b(AKTVerifyEmailActivity.this);
                }
            });
        }

        @Override // android.os.CountDownTimer
        public void onTick(long j) {
            gb2 gb2Var = AKTVerifyEmailActivity.this.u0;
            Integer num = (Integer) AKTVerifyEmailActivity.this.u0.getValue();
            gb2Var.postValue(Integer.valueOf(num == null ? 0 : num.intValue() - 1));
        }
    }

    public static final void B0(AKTVerifyEmailActivity aKTVerifyEmailActivity, View view) {
        fs1.f(aKTVerifyEmailActivity, "this$0");
        aKTVerifyEmailActivity.onBackPressed();
    }

    public static final void C0(AKTVerifyEmailActivity aKTVerifyEmailActivity, View view) {
        fs1.f(aKTVerifyEmailActivity, "this$0");
        aKTVerifyEmailActivity.onBackPressed();
    }

    public static final void D0(AKTVerifyEmailActivity aKTVerifyEmailActivity, View view) {
        fs1.f(aKTVerifyEmailActivity, "this$0");
        pg4.e(aKTVerifyEmailActivity);
    }

    public static final void E0(AKTVerifyEmailActivity aKTVerifyEmailActivity, c8 c8Var, View view) {
        fs1.f(aKTVerifyEmailActivity, "this$0");
        fs1.f(c8Var, "$this_apply");
        if (new Date().getTime() - aKTVerifyEmailActivity.s0 > aKTVerifyEmailActivity.t0 * 1000) {
            aKTVerifyEmailActivity.o0 = "";
        }
        EditText editText = c8Var.f.getEditText();
        String obj = StringsKt__StringsKt.K0(String.valueOf(editText == null ? null : editText.getText())).toString();
        if (obj.length() == 0) {
            aKTVerifyEmailActivity.L0();
            return;
        }
        if ((aKTVerifyEmailActivity.o0.length() > 0) && fs1.b(obj, aKTVerifyEmailActivity.o0)) {
            aKTVerifyEmailActivity.U0();
            return;
        }
        int i = aKTVerifyEmailActivity.p0 - 1;
        aKTVerifyEmailActivity.p0 = i;
        if (i > 0) {
            String str = i > 1 ? "s" : "";
            lu3 lu3Var = lu3.a;
            String string = aKTVerifyEmailActivity.getString(R.string.akt_verify_email_invalid_code_text);
            fs1.e(string, "getString(R.string.akt_v…_email_invalid_code_text)");
            String format = String.format(string, Arrays.copyOf(new Object[]{Integer.valueOf(aKTVerifyEmailActivity.p0), str}, 2));
            fs1.e(format, "java.lang.String.format(format, *args)");
            e30.a0(aKTVerifyEmailActivity, format);
            return;
        }
        aKTVerifyEmailActivity.v0();
    }

    public static final void F0(AKTVerifyEmailActivity aKTVerifyEmailActivity, Integer num) {
        fs1.f(aKTVerifyEmailActivity, "this$0");
        fs1.e(num, "it");
        aKTVerifyEmailActivity.T0(num.intValue());
    }

    public static final void G0(AKTVerifyEmailActivity aKTVerifyEmailActivity, View view) {
        fs1.f(aKTVerifyEmailActivity, "this$0");
        aKTVerifyEmailActivity.J0();
    }

    public static final void H0(AKTVerifyEmailActivity aKTVerifyEmailActivity) {
        fs1.f(aKTVerifyEmailActivity, "this$0");
        aKTVerifyEmailActivity.M0();
    }

    public static final void N0(Dialog dialog, final AKTVerifyEmailActivity aKTVerifyEmailActivity) {
        fs1.f(dialog, "$dialog");
        fs1.f(aKTVerifyEmailActivity, "this$0");
        dialog.dismiss();
        new Handler(Looper.getMainLooper()).postDelayed(new Runnable() { // from class: q3
            @Override // java.lang.Runnable
            public final void run() {
                AKTVerifyEmailActivity.O0(AKTVerifyEmailActivity.this);
            }
        }, 200L);
    }

    public static final void O0(AKTVerifyEmailActivity aKTVerifyEmailActivity) {
        fs1.f(aKTVerifyEmailActivity, "this$0");
        EditText editText = aKTVerifyEmailActivity.w0().f.getEditText();
        pg4.g(editText);
        if (editText == null) {
            return;
        }
        editText.setSelection(editText.getText().length());
    }

    public static final void Q0(Dialog dialog, AKTVerifyEmailActivity aKTVerifyEmailActivity) {
        fs1.f(dialog, "$dialog");
        fs1.f(aKTVerifyEmailActivity, "this$0");
        dialog.dismiss();
        Intent intent = new Intent(aKTVerifyEmailActivity, AKTHomeActivity.class);
        intent.setFlags(67108864);
        intent.putExtra("IS_BACK_FROM_CHANGE_EMAIL", true);
        aKTVerifyEmailActivity.startActivity(intent);
        aKTVerifyEmailActivity.finish();
    }

    public static final void z0(AKTVerifyEmailActivity aKTVerifyEmailActivity, String str) {
        fs1.f(aKTVerifyEmailActivity, "this$0");
        if (str == null) {
            return;
        }
        aKTVerifyEmailActivity.j0.h();
        qy4 qy4Var = aKTVerifyEmailActivity.l0;
        fs1.e(qy4Var, "safeMoonppp");
        aKTVerifyEmailActivity.X(qy4Var, str);
    }

    public final boolean A0() {
        return ((Boolean) this.n0.getValue()).booleanValue();
    }

    public final void I0(qy4 qy4Var, String[] strArr) {
        int length = strArr.length;
        int i = 0;
        while (i < length) {
            String str = strArr[i];
            i++;
            List w0 = StringsKt__StringsKt.w0(str, new String[]{"="}, false, 0, 6, null);
            if (w0.size() > 1) {
                String str2 = (String) j20.L(w0);
                int hashCode = str2.hashCode();
                if (hashCode != -547471898) {
                    if (hashCode != 2313765) {
                        if (hashCode == 1004359981 && str2.equals("QUESTIONS")) {
                            String j = q.j(bo3.i(this, "ICEPRIVKEY"), s.g, (String) w0.get(1));
                            if (j == null) {
                                j = "";
                            }
                            int d0 = StringsKt__StringsKt.d0(j, '|', 0, false, 6, null);
                            if (d0 + 5 > j.length()) {
                                if (j.length() > 0) {
                                    j = j.substring(0, d0);
                                    fs1.e(j, "(this as java.lang.Strin…ing(startIndex, endIndex)");
                                }
                            }
                            qy4Var.c("questions", j);
                        }
                    } else if (str2.equals("KPPP")) {
                        qy4Var.c("KPP", (String) w0.get(1));
                    }
                } else if (str2.equals("VERIFYCODE")) {
                    this.o0 = (String) w0.get(1);
                }
            }
        }
    }

    public final void J0() {
        String b2;
        ProgressLoading.a aVar = ProgressLoading.y0;
        String string = getString(R.string.loading);
        fs1.e(string, "getString(R.string.loading)");
        ProgressLoading a2 = aVar.a(false, string, "");
        this.j0 = a2;
        FragmentManager supportFragmentManager = getSupportFragmentManager();
        fs1.e(supportFragmentManager, "supportFragmentManager");
        a2.A(supportFragmentManager);
        String i = bo3.i(this, "EMAILRAW");
        if (s.b) {
            b2 = K(this.l0, i, false);
            fs1.e(b2, "{\n            SFMICE03(\n…e\n            )\n        }");
        } else {
            b2 = i2.b(this.l0, "AKTPPP03");
            fs1.e(b2, "{\n            AKTSecpUti…pp, \"AKTPPP03\")\n        }");
        }
        K0(b2);
    }

    public void K0(String str) {
        if (str == null) {
            return;
        }
        x0().k(str);
    }

    public final void L0() {
        bh.V(new WeakReference(this), null, Integer.valueOf((int) R.string.akt_error_verify_code_empty_message), 0, new AKTVerifyEmailActivity$showCodeEmptyDialog$1(this), 10, null);
    }

    public final void M0() {
        final Dialog dialog = new Dialog(this);
        dialog.requestWindowFeature(1);
        dialog.setCancelable(false);
        dialog.setContentView(R.layout.dialog_email_sent);
        Window window = dialog.getWindow();
        fs1.d(window);
        window.setBackgroundDrawable(new ColorDrawable(0));
        dialog.show();
        new Handler(Looper.getMainLooper()).postDelayed(new Runnable() { // from class: o3
            @Override // java.lang.Runnable
            public final void run() {
                AKTVerifyEmailActivity.N0(dialog, this);
            }
        }, 1500L);
    }

    public final void P0() {
        final Dialog dialog = new Dialog(this);
        dialog.requestWindowFeature(1);
        dialog.setCancelable(false);
        dialog.setContentView(R.layout.dialog_success);
        Window window = dialog.getWindow();
        fs1.d(window);
        window.setBackgroundDrawable(new ColorDrawable(0));
        dialog.show();
        new Handler(Looper.getMainLooper()).postDelayed(new Runnable() { // from class: p3
            @Override // java.lang.Runnable
            public final void run() {
                AKTVerifyEmailActivity.Q0(dialog, this);
            }
        }, 1500L);
    }

    @Override // com.AKT.anonymouskey.ui.login.AKTServerFunctions
    public void Q() {
        x0().f();
    }

    public final void R0() {
        w0().e.setEnabled(false);
        w0().e.setTextColor(getColor(R.color.akt_button_inactive));
        this.u0.postValue(Integer.valueOf(this.v0));
        CountDownTimer countDownTimer = this.r0;
        if (countDownTimer != null) {
            countDownTimer.cancel();
        }
        c cVar = new c(this.v0 * 1000);
        this.r0 = cVar;
        cVar.start();
    }

    public final void S0(String str) {
        Objects.requireNonNull(str, "null cannot be cast to non-null type kotlin.CharSequence");
        if (StringsKt__StringsKt.K0(str).toString().length() == 0) {
            w0().b.setTextColor(getColor(R.color.white));
            w0().b.setEnabled(false);
            w0().b.setBackgroundTintList(getColorStateList(R.color.akt_button_inactive));
            return;
        }
        w0().b.setTextColor(getColor(R.color.black));
        w0().b.setEnabled(true);
        w0().b.setBackgroundTintList(getColorStateList(R.color.akt_button_active));
    }

    public final void T0(int i) {
        StringBuilder sb = new StringBuilder();
        sb.append(getString(R.string.akt_verify_email_resend_code_text));
        if (i > 0) {
            sb.append(MinimalPrettyPrinter.DEFAULT_ROOT_VALUE_SEPARATOR);
            StringBuilder sb2 = new StringBuilder();
            sb2.append('(');
            sb2.append(i);
            sb2.append(')');
            sb.append(sb2.toString());
        }
        w0().e.setText(sb.toString());
    }

    public final void U0() {
        if (A0()) {
            String N = AKTServerFunctions.N(this, bo3.i(this, "EMAILRAW"));
            this.w0 = true;
            K0(N);
            ProgressLoading.a aVar = ProgressLoading.y0;
            String string = getString(R.string.loading);
            fs1.e(string, "getString(R.string.loading)");
            ProgressLoading a2 = aVar.a(false, string, "");
            this.j0 = a2;
            FragmentManager supportFragmentManager = getSupportFragmentManager();
            fs1.e(supportFragmentManager, "supportFragmentManager");
            a2.A(supportFragmentManager);
            return;
        }
        AKTRegisterActivity.q0.a(this);
        finish();
    }

    @Override // com.AKT.anonymouskey.ui.login.AKTServerFunctions
    public void X(qy4 qy4Var, String str) {
        fs1.f(qy4Var, "safeMoonppp");
        if (W(str)) {
            return;
        }
        String[] v = i2.v(b30.a.v(this, str), "|");
        fs1.e(v, "parts");
        if (!(v.length == 0)) {
            String str2 = v[0];
            fs1.e(str2, "parts[0]");
            if (StringsKt__StringsKt.w0(str2, new String[]{"="}, false, 0, 6, null).size() >= 2) {
                String str3 = v[0];
                fs1.e(str3, "parts[0]");
                String str4 = (String) StringsKt__StringsKt.w0(str3, new String[]{"="}, false, 0, 6, null).get(1);
                Objects.requireNonNull(str4, "null cannot be cast to non-null type java.lang.String");
                String upperCase = str4.toUpperCase(Locale.ROOT);
                fs1.e(upperCase, "(this as java.lang.Strin….toUpperCase(Locale.ROOT)");
                Q();
                int hashCode = upperCase.hashCode();
                if (hashCode != -1299641089) {
                    if (hashCode != -1165468466) {
                        if (hashCode == 325952731 && upperCase.equals("AKTSERVERERROR")) {
                            Y(v, null);
                            return;
                        }
                    } else if (upperCase.equals("CHANGEEMAIL02")) {
                        ProgressLoading progressLoading = this.j0;
                        if (progressLoading != null) {
                            progressLoading.h();
                        }
                        this.w0 = false;
                        P0();
                        return;
                    }
                } else if (upperCase.equals("AKTSFMICE04")) {
                    I0(qy4Var, v);
                    ProgressLoading progressLoading2 = this.j0;
                    if (progressLoading2 != null) {
                        progressLoading2.h();
                    }
                    R0();
                    this.s0 = new Date().getTime();
                    M0();
                    return;
                }
                super.X(qy4Var, str);
            }
        }
    }

    @Override // com.AKT.anonymouskey.ui.login.AKTServerFunctions
    public void Y(String[] strArr, String str) {
        this.j0.h();
        if (this.w0) {
            bh.V(new WeakReference(this), null, Integer.valueOf((int) R.string.akt_error_change_email_message), 0, null, 26, null);
            this.w0 = false;
        }
    }

    @Override // com.AKT.anonymouskey.ui.login.AKTServerFunctions, net.safemoon.androidwallet.activity.common.BasicActivity, androidx.fragment.app.FragmentActivity, androidx.activity.ComponentActivity, androidx.core.app.ComponentActivity, android.app.Activity
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        setContentView(w0().b());
        x0().l();
        y0();
        ActionBar supportActionBar = getSupportActionBar();
        if (supportActionBar != null) {
            supportActionBar.l();
        }
        getWindow().addFlags(Integer.MIN_VALUE);
        getWindow().setStatusBarColor(m70.d(this, 17170445));
        if (!A0()) {
            getWindow().setBackgroundDrawable(new ColorDrawable(getColor(R.color.akt_night_background)));
        } else {
            getWindow().setBackgroundDrawableResource(R.drawable.bottom_curve);
        }
        Intent intent = getIntent();
        if (intent == null) {
            return;
        }
        String stringExtra = intent.getStringExtra("VERIFY_CODE");
        if (stringExtra == null) {
            stringExtra = "";
        }
        this.o0 = stringExtra;
        y54.b a2 = y54.a("Khang");
        String str = this.o0;
        Objects.requireNonNull(str, "null cannot be cast to non-null type java.lang.String");
        String upperCase = str.toUpperCase(Locale.ROOT);
        fs1.e(upperCase, "(this as java.lang.Strin….toUpperCase(Locale.ROOT)");
        a2.a(upperCase, new Object[0]);
    }

    @Override // androidx.appcompat.app.AppCompatActivity, androidx.fragment.app.FragmentActivity, android.app.Activity
    public void onDestroy() {
        super.onDestroy();
        CountDownTimer countDownTimer = this.r0;
        if (countDownTimer == null) {
            return;
        }
        countDownTimer.cancel();
    }

    @Override // net.safemoon.androidwallet.activity.common.BasicActivity, androidx.appcompat.app.AppCompatActivity, android.app.Activity
    public void onPostCreate(Bundle bundle) {
        super.onPostCreate(bundle);
        final c8 w0 = w0();
        if (A0()) {
            int d = m70.d(this, R.color.t1);
            w0.g.c.setVisibility(8);
            w0.g.b.setVisibility(0);
            EditText editText = w0.f.getEditText();
            if (editText != null) {
                editText.setTextColor(d);
            }
            w0.f.setEndIconTintList(ColorStateList.valueOf(d));
            w0.h.setBackgroundColor(d);
            w0.e.setTextColor(d);
            w0().d.setImageResource(R.drawable.ic_logo_dark);
            w0.g.b().setBackgroundColor(m70.d(this, R.color.toolbar_bg_color));
            w0.g.e.setVisibility(0);
            w0.g.e.setText(getString(R.string.akt_verify_email_header_title));
            w0.c.setBackgroundColor(m70.d(this, R.color.p0));
        } else {
            w0.g.e.setVisibility(0);
            w0.g.b.setVisibility(0);
            w0.g.c.setVisibility(8);
            w0.g.e.setText(getString(R.string.akt_activity_register_txt));
            w0.g.c.setText(getString(R.string.cancel));
        }
        w0.g.c.setOnClickListener(new View.OnClickListener() { // from class: m3
            @Override // android.view.View.OnClickListener
            public final void onClick(View view) {
                AKTVerifyEmailActivity.B0(AKTVerifyEmailActivity.this, view);
            }
        });
        w0.g.b.setOnClickListener(new View.OnClickListener() { // from class: k3
            @Override // android.view.View.OnClickListener
            public final void onClick(View view) {
                AKTVerifyEmailActivity.C0(AKTVerifyEmailActivity.this, view);
            }
        });
        w0.c.setOnClickListener(new View.OnClickListener() { // from class: j3
            @Override // android.view.View.OnClickListener
            public final void onClick(View view) {
                AKTVerifyEmailActivity.D0(AKTVerifyEmailActivity.this, view);
            }
        });
        w0.b.setOnClickListener(new View.OnClickListener() { // from class: n3
            @Override // android.view.View.OnClickListener
            public final void onClick(View view) {
                AKTVerifyEmailActivity.E0(AKTVerifyEmailActivity.this, w0, view);
            }
        });
        this.u0.observe(this, new tl2() { // from class: g3
            @Override // defpackage.tl2
            public final void onChanged(Object obj) {
                AKTVerifyEmailActivity.F0(AKTVerifyEmailActivity.this, (Integer) obj);
            }
        });
        R0();
        this.s0 = new Date().getTime();
        w0.e.setOnClickListener(new View.OnClickListener() { // from class: l3
            @Override // android.view.View.OnClickListener
            public final void onClick(View view) {
                AKTVerifyEmailActivity.G0(AKTVerifyEmailActivity.this, view);
            }
        });
        S0("");
        EditText editText2 = w0.f.getEditText();
        if (editText2 != null) {
            editText2.addTextChangedListener(new b(w0));
        }
        new Handler(Looper.getMainLooper()).postDelayed(new Runnable() { // from class: h3
            @Override // java.lang.Runnable
            public final void run() {
                AKTVerifyEmailActivity.H0(AKTVerifyEmailActivity.this);
            }
        }, 300L);
    }

    public final void v0() {
        Intent intent = new Intent(this, AKTActivity.class);
        intent.setFlags(67108864);
        startActivity(intent);
        finish();
    }

    public final c8 w0() {
        return (c8) this.m0.getValue();
    }

    public final AKTWebSocketHandlingViewModel x0() {
        return (AKTWebSocketHandlingViewModel) this.q0.getValue();
    }

    public void y0() {
        x0().h().observe(this, new tl2() { // from class: i3
            @Override // defpackage.tl2
            public final void onChanged(Object obj) {
                AKTVerifyEmailActivity.z0(AKTVerifyEmailActivity.this, (String) obj);
            }
        });
    }
}
