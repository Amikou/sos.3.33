package net.safemoon.androidwallet.activity;

import android.os.Bundle;
import android.view.View;
import androidx.appcompat.app.AppCompatActivity;
import kotlinx.coroutines.b;
import net.safemoon.androidwallet.ERC20;
import net.safemoon.androidwallet.SafeswapRouter;
import net.safemoon.androidwallet.ui.wallet.Convert;

/* compiled from: TestActivity.kt */
/* loaded from: classes2.dex */
public final class TestActivity extends AppCompatActivity {
    @Override // androidx.fragment.app.FragmentActivity, androidx.activity.ComponentActivity, androidx.core.app.ComponentActivity, android.app.Activity
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        setContentView(new View(this));
        ko4 a = jo4.a(new ml1("https://nourl.com"));
        ma0 create = ma0.create(w.c(getApplication()));
        String address = create.getAddress();
        fs1.e(address, "credentails.address");
        e30.c0(address, "Address");
        it3 it3Var = new it3(Convert.c("10", Convert.Unit.GWEI).toBigInteger(), new oj0().getGasLimit());
        b.b(null, new TestActivity$onCreate$1(a, create, ERC20.s("0xDD6999Ec25948811d7c671051f5B4E44B175239e", a, create, it3Var), SafeswapRouter.q("0xc04c3408EA62778072DBc8c14AA9d52FC77EeE8a", a, create, it3Var), b20.j("0xdd6999ec25948811d7c671051f5b4e44b175239e", "0xae13d989daC2f0dEbFf460aC112a837C89BAa7cd"), null), 1, null);
    }
}
