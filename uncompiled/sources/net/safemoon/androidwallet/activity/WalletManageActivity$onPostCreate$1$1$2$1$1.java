package net.safemoon.androidwallet.activity;

import android.text.Editable;
import kotlin.jvm.internal.Lambda;
import kotlin.text.StringsKt__StringsKt;
import net.safemoon.androidwallet.dialogs.ProgressLoading;
import net.safemoon.androidwallet.model.wallets.Wallet;
import net.safemoon.androidwallet.viewmodels.MultiWalletViewModel;

/* compiled from: WalletManageActivity.kt */
/* loaded from: classes2.dex */
public final class WalletManageActivity$onPostCreate$1$1$2$1$1 extends Lambda implements rc1<te4> {
    public final /* synthetic */ Editable $it;
    public final /* synthetic */ WalletManageActivity this$0;

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public WalletManageActivity$onPostCreate$1$1$2$1$1(WalletManageActivity walletManageActivity, Editable editable) {
        super(0);
        this.this$0 = walletManageActivity;
        this.$it = editable;
    }

    @Override // defpackage.rc1
    public /* bridge */ /* synthetic */ te4 invoke() {
        invoke2();
        return te4.a;
    }

    @Override // defpackage.rc1
    /* renamed from: invoke  reason: avoid collision after fix types in other method */
    public final void invoke2() {
        MultiWalletViewModel multiWalletViewModel;
        ProgressLoading progressLoading;
        Wallet e1 = this.this$0.e1();
        if (e1 == null) {
            return;
        }
        Editable editable = this.$it;
        WalletManageActivity walletManageActivity = this.this$0;
        e1.setName(StringsKt__StringsKt.K0(editable).toString());
        multiWalletViewModel = walletManageActivity.k0;
        multiWalletViewModel.B(e1);
        if (e1.isLinked()) {
            walletManageActivity.y0 = true;
            walletManageActivity.j1(e1);
            return;
        }
        progressLoading = walletManageActivity.j0;
        progressLoading.h();
        walletManageActivity.onBackPressed();
    }
}
