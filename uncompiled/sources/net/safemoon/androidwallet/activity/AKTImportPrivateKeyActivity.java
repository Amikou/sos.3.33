package net.safemoon.androidwallet.activity;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.text.Editable;
import android.view.View;
import androidx.fragment.app.FragmentManager;
import com.google.android.material.textfield.TextInputEditText;
import net.safemoon.androidwallet.R;
import net.safemoon.androidwallet.activity.AKTImportPrivateKeyActivity;
import net.safemoon.androidwallet.dialogs.ProgressLoading;
import net.safemoon.androidwallet.viewmodels.CreateWalletViewModel;

/* compiled from: AKTImportPrivateKeyActivity.kt */
/* loaded from: classes2.dex */
public final class AKTImportPrivateKeyActivity extends AKTRecoverWalletActivity {
    public static final a o0 = new a(null);
    public final sy1 n0 = zy1.a(new AKTImportPrivateKeyActivity$binding$2(this));

    /* compiled from: AKTImportPrivateKeyActivity.kt */
    /* loaded from: classes2.dex */
    public static final class a {
        public a() {
        }

        public /* synthetic */ a(qi0 qi0Var) {
            this();
        }

        public final void a(Context context) {
            fs1.f(context, "context");
            context.startActivity(new Intent(context, AKTImportPrivateKeyActivity.class));
        }
    }

    public static final void B0(g7 g7Var, AKTImportPrivateKeyActivity aKTImportPrivateKeyActivity, View view) {
        fs1.f(g7Var, "$this_apply");
        fs1.f(aKTImportPrivateKeyActivity, "this$0");
        Editable text = g7Var.d.getText();
        String obj = text == null ? null : text.toString();
        if (aKTImportPrivateKeyActivity.F0(obj)) {
            if (g7Var.d.getText() == null) {
                return;
            }
            ProgressLoading.a aVar = ProgressLoading.y0;
            String string = aKTImportPrivateKeyActivity.getString(R.string.loading);
            fs1.e(string, "getString(R.string.loading)");
            ProgressLoading a2 = aVar.a(false, string, "");
            aKTImportPrivateKeyActivity.j0 = a2;
            FragmentManager supportFragmentManager = aKTImportPrivateKeyActivity.getSupportFragmentManager();
            fs1.e(supportFragmentManager, "supportFragmentManager");
            a2.A(supportFragmentManager);
            aKTImportPrivateKeyActivity.x0(obj);
            return;
        }
        jc0.c(aKTImportPrivateKeyActivity, Integer.valueOf((int) R.string.invalid_private_key), R.string.invalid_private_key_description, true, a1.a);
    }

    public static final void C0(DialogInterface dialogInterface) {
    }

    public static final void D0(AKTImportPrivateKeyActivity aKTImportPrivateKeyActivity, View view) {
        fs1.f(aKTImportPrivateKeyActivity, "this$0");
        aKTImportPrivateKeyActivity.onBackPressed();
    }

    public static final void y0(DialogInterface dialogInterface) {
    }

    public final void A0() {
        final g7 z0 = z0();
        zh4 zh4Var = z0.e;
        fs1.e(zh4Var, "viewBarcodeComponentLayout");
        TextInputEditText textInputEditText = z0.d;
        fs1.e(textInputEditText, "editPrivateKey");
        cj4.n(zh4Var, textInputEditText, new AKTImportPrivateKeyActivity$setView$1$1(this));
        z0.c.setOnClickListener(new View.OnClickListener() { // from class: b1
            @Override // android.view.View.OnClickListener
            public final void onClick(View view) {
                AKTImportPrivateKeyActivity.B0(g7.this, this, view);
            }
        });
        z0.b.setOnClickListener(new View.OnClickListener() { // from class: c1
            @Override // android.view.View.OnClickListener
            public final void onClick(View view) {
                AKTImportPrivateKeyActivity.D0(AKTImportPrivateKeyActivity.this, view);
            }
        });
    }

    public final void E0() {
        w7<Intent> b;
        hn2 D = D();
        if (D == null || (b = D.b(new AKTImportPrivateKeyActivity$startScanQRCode$1(this))) == null) {
            return;
        }
        b.a(new Intent(this, ScannedCodeActivity.class));
    }

    public final boolean F0(String str) {
        return str != null && str.length() >= 64;
    }

    @Override // net.safemoon.androidwallet.activity.AKTRecoverWalletActivity, com.AKT.anonymouskey.ui.login.AKTServerFunctions, net.safemoon.androidwallet.activity.common.BasicActivity, androidx.fragment.app.FragmentActivity, androidx.activity.ComponentActivity, androidx.core.app.ComponentActivity, android.app.Activity
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        setContentView(z0().b());
    }

    @Override // net.safemoon.androidwallet.activity.common.BasicActivity, androidx.appcompat.app.AppCompatActivity, android.app.Activity
    public void onPostCreate(Bundle bundle) {
        super.onPostCreate(bundle);
        A0();
    }

    public final void x0(String str) {
        try {
            CreateWalletViewModel k0 = k0();
            fs1.d(str);
            k0.d(str);
            k0().b(str, CreateWalletViewModel.KEY_TYPE.PRIVATE_KEY, new AKTImportPrivateKeyActivity$checkUser$1(this));
        } catch (Exception unused) {
            jc0.c(this, Integer.valueOf((int) R.string.error_iw_title), R.string.error_iw_msg, true, z0.a);
        }
    }

    public final g7 z0() {
        return (g7) this.n0.getValue();
    }
}
