package net.safemoon.androidwallet.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import androidx.paging.PagingDataAdapter;
import androidx.recyclerview.widget.RecyclerView;
import androidx.recyclerview.widget.g;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;
import net.safemoon.androidwallet.R;
import net.safemoon.androidwallet.adapter.TransactionAdapter;
import net.safemoon.androidwallet.model.token.abstraction.IToken;
import net.safemoon.androidwallet.model.transaction.history.Result;
import net.safemoon.androidwallet.ui.displayModel.UserTokenItemDisplayModel;
import org.web3j.abi.datatypes.Address;

/* compiled from: TransactionAdapter.kt */
/* loaded from: classes2.dex */
public abstract class TransactionAdapter extends PagingDataAdapter<Result, b> {
    public final Context d;
    public final tc1<Result, te4> e;
    public final sy1 f;
    public final List<IToken> g;

    /* compiled from: TransactionAdapter.kt */
    /* loaded from: classes2.dex */
    public static final class a extends g.f<Result> {
        @Override // androidx.recyclerview.widget.g.f
        /* renamed from: a */
        public boolean areContentsTheSame(Result result, Result result2) {
            fs1.f(result, "oldItem");
            fs1.f(result2, "newItem");
            return fs1.b(result.toString(), result2.toString());
        }

        @Override // androidx.recyclerview.widget.g.f
        /* renamed from: b */
        public boolean areItemsTheSame(Result result, Result result2) {
            fs1.f(result, "oldItem");
            fs1.f(result2, "newItem");
            return fs1.b(result.toString(), result2.toString());
        }
    }

    /* compiled from: TransactionAdapter.kt */
    /* loaded from: classes2.dex */
    public final class b extends RecyclerView.a0 {
        public final ImageView a;
        public final TextView b;
        public final TextView c;
        public final TextView d;
        public final ViewGroup e;
        public final ViewGroup f;
        public final TextView g;
        public final /* synthetic */ TransactionAdapter h;

        /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
        public b(TransactionAdapter transactionAdapter, View view) {
            super(view);
            fs1.f(transactionAdapter, "this$0");
            fs1.f(view, "itemView");
            this.h = transactionAdapter;
            View findViewById = view.findViewById(R.id.iv_status);
            fs1.e(findViewById, "itemView.findViewById(R.id.iv_status)");
            this.a = (ImageView) findViewById;
            View findViewById2 = view.findViewById(R.id.iv_tran_name);
            fs1.e(findViewById2, "itemView.findViewById(R.id.iv_tran_name)");
            this.b = (TextView) findViewById2;
            View findViewById3 = view.findViewById(R.id.tv_amount);
            fs1.e(findViewById3, "itemView.findViewById(R.id.tv_amount)");
            this.c = (TextView) findViewById3;
            View findViewById4 = view.findViewById(R.id.tv_to_from);
            fs1.e(findViewById4, "itemView.findViewById(R.id.tv_to_from)");
            this.d = (TextView) findViewById4;
            View findViewById5 = view.findViewById(R.id.groupHeader);
            fs1.e(findViewById5, "itemView.findViewById(R.id.groupHeader)");
            this.e = (ViewGroup) findViewById5;
            View findViewById6 = view.findViewById(R.id.groupFooter);
            fs1.e(findViewById6, "itemView.findViewById(R.id.groupFooter)");
            this.f = (ViewGroup) findViewById6;
            View findViewById7 = view.findViewById(R.id.txtTimeStamp);
            fs1.e(findViewById7, "itemView.findViewById(R.id.txtTimeStamp)");
            this.g = (TextView) findViewById7;
        }

        public static final void d(TransactionAdapter transactionAdapter, Result result, View view) {
            fs1.f(transactionAdapter, "this$0");
            fs1.f(result, "$it");
            transactionAdapter.e.invoke(result);
        }

        public static final void e(TransactionAdapter transactionAdapter, Result result, View view) {
            fs1.f(transactionAdapter, "this$0");
            fs1.f(result, "$it");
            transactionAdapter.e.invoke(result);
        }

        public final void c(final Result result, int i) {
            IToken iToken;
            this.e.setVisibility(8);
            this.f.setVisibility(8);
            if (result == null) {
                return;
            }
            final TransactionAdapter transactionAdapter = this.h;
            int i2 = i - 1;
            Result h = i2 >= 0 ? TransactionAdapter.h(transactionAdapter, i2) : null;
            int i3 = i + 1;
            Result h2 = i3 < transactionAdapter.getItemCount() ? TransactionAdapter.h(transactionAdapter, i3) : null;
            String str = result.timeStamp;
            fs1.e(str, "current.timeStamp");
            Date m = transactionAdapter.m(str);
            f(m);
            if (h == null) {
                this.e.setVisibility(0);
                if (h2 != null) {
                    String str2 = h2.timeStamp;
                    fs1.e(str2, "next.timeStamp");
                    if (fs1.b(transactionAdapter.m(str2), m)) {
                        this.f.setVisibility(8);
                    } else {
                        this.f.setVisibility(0);
                    }
                }
            }
            if (h2 == null) {
                this.f.setVisibility(0);
                if (h != null) {
                    String str3 = h.timeStamp;
                    fs1.e(str3, "previous.timeStamp");
                    if (fs1.b(transactionAdapter.m(str3), m)) {
                        this.e.setVisibility(8);
                    } else {
                        this.e.setVisibility(0);
                    }
                }
            }
            if (h != null && h2 != null) {
                String str4 = h.timeStamp;
                fs1.e(str4, "previous.timeStamp");
                Date m2 = transactionAdapter.m(str4);
                String str5 = h2.timeStamp;
                fs1.e(str5, "next.timeStamp");
                Date m3 = transactionAdapter.m(str5);
                if (fs1.b(m2, m)) {
                    this.e.setVisibility(8);
                } else {
                    this.e.setVisibility(0);
                }
                if (fs1.b(m3, m)) {
                    this.f.setVisibility(8);
                } else {
                    this.f.setVisibility(0);
                }
            }
            String str6 = result.txreceiptStatus;
            String str7 = "0";
            if (fs1.b(str6, "1")) {
                this.a.setColorFilter(m70.d(transactionAdapter.d, R.color.green));
            } else if (fs1.b(str6, "0")) {
                this.a.setColorFilter(m70.d(transactionAdapter.d, R.color.red));
            } else {
                this.a.setColorFilter(m70.d(transactionAdapter.d, R.color.yellow));
            }
            if (result.offlinePending) {
                TextView textView = this.c;
                StringBuilder sb = new StringBuilder();
                sb.append((Object) result.value);
                sb.append(' ');
                sb.append((Object) result.tokenSymbol);
                textView.setText(sb.toString());
                String str8 = result.from;
                fs1.e(str8, "it.from");
                Locale locale = Locale.ROOT;
                String lowerCase = str8.toLowerCase(locale);
                fs1.e(lowerCase, "(this as java.lang.Strin….toLowerCase(Locale.ROOT)");
                String str9 = result.to;
                fs1.e(str9, "it.to");
                String lowerCase2 = str9.toLowerCase(locale);
                fs1.e(lowerCase2, "(this as java.lang.Strin….toLowerCase(Locale.ROOT)");
                if (!dv3.t(lowerCase, lowerCase2, true)) {
                    String k = transactionAdapter.k();
                    fs1.e(k, Address.TYPE_NAME);
                    String lowerCase3 = k.toLowerCase(locale);
                    fs1.e(lowerCase3, "(this as java.lang.Strin….toLowerCase(Locale.ROOT)");
                    String str10 = result.to;
                    fs1.e(str10, "it.to");
                    String lowerCase4 = str10.toLowerCase(locale);
                    fs1.e(lowerCase4, "(this as java.lang.Strin….toLowerCase(Locale.ROOT)");
                    if (fs1.b(lowerCase3, lowerCase4)) {
                        this.b.setText(transactionAdapter.l(R.string.tx_received));
                        this.d.setText(transactionAdapter.l(R.string.tx_from) + ' ' + ((Object) result.from));
                    } else {
                        this.b.setText(transactionAdapter.l(R.string.tx_sent));
                        this.d.setText(transactionAdapter.l(R.string.tx_to) + ' ' + ((Object) result.to));
                    }
                } else {
                    this.b.setText(transactionAdapter.l(R.string.tx_self));
                    this.d.setText("");
                }
                this.itemView.setOnClickListener(new View.OnClickListener() { // from class: q84
                    @Override // android.view.View.OnClickListener
                    public final void onClick(View view) {
                        TransactionAdapter.b.d(TransactionAdapter.this, result, view);
                    }
                });
                return;
            }
            String str11 = result.contractAddress;
            if (str11 != null) {
                fs1.e(str11, "it.contractAddress");
                iToken = transactionAdapter.n(str11);
            } else {
                iToken = null;
            }
            if (iToken == null) {
                iToken = transactionAdapter.o().toToken();
            }
            if (iToken != null) {
                TextView textView2 = this.c;
                StringBuilder sb2 = new StringBuilder();
                String str12 = result.value;
                fs1.e(str12, "it.value");
                sb2.append(e30.p(e30.q(str12, iToken.getDecimals()).doubleValue(), 0, null, false, 6, null));
                sb2.append(' ');
                sb2.append(iToken.getSymbol());
                textView2.setText(sb2.toString());
                String str13 = result.from;
                fs1.e(str13, "it.from");
                Locale locale2 = Locale.ROOT;
                String lowerCase5 = str13.toLowerCase(locale2);
                fs1.e(lowerCase5, "(this as java.lang.Strin….toLowerCase(Locale.ROOT)");
                String str14 = result.to;
                fs1.e(str14, "it.to");
                String lowerCase6 = str14.toLowerCase(locale2);
                fs1.e(lowerCase6, "(this as java.lang.Strin….toLowerCase(Locale.ROOT)");
                if (!dv3.t(lowerCase5, lowerCase6, true)) {
                    String k2 = transactionAdapter.k();
                    fs1.e(k2, Address.TYPE_NAME);
                    String lowerCase7 = k2.toLowerCase(locale2);
                    fs1.e(lowerCase7, "(this as java.lang.Strin….toLowerCase(Locale.ROOT)");
                    String str15 = result.to;
                    fs1.e(str15, "it.to");
                    String lowerCase8 = str15.toLowerCase(locale2);
                    fs1.e(lowerCase8, "(this as java.lang.Strin….toLowerCase(Locale.ROOT)");
                    if (fs1.b(lowerCase7, lowerCase8)) {
                        this.b.setText(transactionAdapter.l(R.string.tx_received));
                        this.d.setText(transactionAdapter.l(R.string.tx_from) + ' ' + ((Object) result.from));
                    } else if (!fs1.b(result.input, "0x")) {
                        this.b.setText(transactionAdapter.l(R.string.tx_smart_contract_call));
                        TextView textView3 = this.d;
                        StringBuilder sb3 = new StringBuilder();
                        sb3.append(transactionAdapter.l(R.string.tx_to));
                        sb3.append(' ');
                        String str16 = result.to;
                        if (str16.length() == 0) {
                            str16 = result.contractAddress;
                        }
                        sb3.append((Object) str16);
                        textView3.setText(sb3.toString());
                        String str17 = result.value;
                        fs1.e(str17, "it.value");
                        if (fs1.b(new BigDecimal(str17), BigDecimal.ZERO)) {
                            String str18 = result.gasUsed;
                            fs1.e(str18, "it.gasUsed");
                            BigInteger bigInteger = new BigInteger(str18);
                            String str19 = result.gasPrice;
                            fs1.e(str19, "it.gasPrice");
                            BigInteger multiply = bigInteger.multiply(new BigInteger(str19));
                            fs1.e(multiply, "it.gasUsed.toBigInteger(….gasPrice.toBigInteger())");
                            String p = e30.p(e30.r(multiply, 18).doubleValue(), 0, null, false, 6, null);
                            this.c.setText(p + ' ' + iToken.getSymbol());
                        }
                    } else {
                        this.b.setText(transactionAdapter.l(R.string.tx_sent));
                        this.d.setText(fs1.l(transactionAdapter.l(R.string.tx_to), result.to));
                    }
                } else {
                    this.b.setText(transactionAdapter.l(R.string.tx_self));
                    this.d.setText("");
                }
            } else {
                String str20 = result.from;
                fs1.e(str20, "it.from");
                Locale locale3 = Locale.ROOT;
                String lowerCase9 = str20.toLowerCase(locale3);
                fs1.e(lowerCase9, "(this as java.lang.Strin….toLowerCase(Locale.ROOT)");
                String str21 = result.to;
                fs1.e(str21, "it.to");
                String lowerCase10 = str21.toLowerCase(locale3);
                fs1.e(lowerCase10, "(this as java.lang.Strin….toLowerCase(Locale.ROOT)");
                if (!dv3.t(lowerCase9, lowerCase10, true)) {
                    String k3 = transactionAdapter.k();
                    fs1.e(k3, Address.TYPE_NAME);
                    String lowerCase11 = k3.toLowerCase(locale3);
                    fs1.e(lowerCase11, "(this as java.lang.Strin….toLowerCase(Locale.ROOT)");
                    String str22 = result.to;
                    fs1.e(str22, "it.to");
                    String lowerCase12 = str22.toLowerCase(locale3);
                    fs1.e(lowerCase12, "(this as java.lang.Strin….toLowerCase(Locale.ROOT)");
                    if (fs1.b(lowerCase11, lowerCase12)) {
                        this.b.setText(transactionAdapter.l(R.string.tx_received));
                        this.d.setText(transactionAdapter.l(R.string.tx_from) + ' ' + ((Object) result.from));
                    } else {
                        this.b.setText(transactionAdapter.l(R.string.tx_sent));
                        this.d.setText(transactionAdapter.l(R.string.tx_to) + ' ' + ((Object) result.to));
                    }
                } else {
                    this.b.setText(transactionAdapter.l(R.string.tx_self));
                    this.d.setText("");
                }
                TextView textView4 = this.c;
                if (result.tokenDecimal != null) {
                    StringBuilder sb4 = new StringBuilder();
                    String str23 = result.value;
                    fs1.e(str23, "it.value");
                    Integer num = result.tokenDecimal;
                    fs1.e(num, "it.tokenDecimal");
                    sb4.append(e30.p(e30.q(str23, num.intValue()).doubleValue(), 0, null, false, 6, null));
                    sb4.append(' ');
                    sb4.append((Object) result.tokenSymbol);
                    str7 = sb4.toString();
                }
                textView4.setText(str7);
            }
            this.itemView.setOnClickListener(new View.OnClickListener() { // from class: r84
                @Override // android.view.View.OnClickListener
                public final void onClick(View view) {
                    TransactionAdapter.b.e(TransactionAdapter.this, result, view);
                }
            });
        }

        public final void f(Date date) {
            SimpleDateFormat b = pe0.a.b(this.h.d);
            this.g.setText(fs1.b(b.format(date), b.format(new Date())) ? this.h.l(R.string.today) : b.format(date));
        }
    }

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    /* JADX WARN: Multi-variable type inference failed */
    public TransactionAdapter(Context context, tc1<? super Result, te4> tc1Var) {
        super(new a(), null, null, 6, null);
        fs1.f(context, "context");
        fs1.f(tc1Var, "onClick");
        this.d = context;
        this.e = tc1Var;
        this.f = zy1.a(new TransactionAdapter$address$2(this));
        this.g = new ArrayList();
    }

    public static final /* synthetic */ Result h(TransactionAdapter transactionAdapter, int i) {
        return transactionAdapter.getItem(i);
    }

    public final void j(List<? extends IToken> list) {
        fs1.f(list, "it");
        this.g.clear();
        this.g.addAll(list);
    }

    public final String k() {
        return (String) this.f.getValue();
    }

    public final String l(int i) {
        String string = this.d.getString(i);
        fs1.e(string, "context.getString(resId)");
        return string;
    }

    public final Date m(String str) {
        fs1.f(str, "timeStamp");
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(new Date(Long.parseLong(str) * 1000));
        calendar.set(11, 0);
        calendar.set(12, 0);
        calendar.set(13, 0);
        calendar.set(14, 0);
        Date time = calendar.getTime();
        fs1.e(time, "getInstance().apply { ti…LLISECOND, 0);\n    }.time");
        return time;
    }

    public final IToken n(String str) {
        Object obj;
        fs1.f(str, "contractAddress");
        Iterator<T> it = this.g.iterator();
        while (true) {
            if (!it.hasNext()) {
                obj = null;
                break;
            }
            obj = it.next();
            if (fs1.b(((IToken) obj).getContractAddress(), str)) {
                break;
            }
        }
        return (IToken) obj;
    }

    public abstract UserTokenItemDisplayModel o();

    @Override // androidx.recyclerview.widget.RecyclerView.Adapter
    /* renamed from: p */
    public void onBindViewHolder(b bVar, int i) {
        fs1.f(bVar, "holder");
        bVar.c(getItem(i), i);
    }

    @Override // androidx.recyclerview.widget.RecyclerView.Adapter
    /* renamed from: q */
    public b onCreateViewHolder(ViewGroup viewGroup, int i) {
        fs1.f(viewGroup, "parent");
        View inflate = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.item_transaction, viewGroup, false);
        fs1.e(inflate, "from(parent.context).inf…ansaction, parent, false)");
        return new b(this, inflate);
    }
}
