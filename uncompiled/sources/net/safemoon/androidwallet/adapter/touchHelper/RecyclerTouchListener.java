package net.safemoon.androidwallet.adapter.touchHelper;

import android.animation.Animator;
import android.animation.ObjectAnimator;
import android.app.Activity;
import android.graphics.Rect;
import android.os.Handler;
import android.util.DisplayMetrics;
import android.view.MotionEvent;
import android.view.VelocityTracker;
import android.view.View;
import android.view.ViewConfiguration;
import android.view.animation.DecelerateInterpolator;
import androidx.recyclerview.widget.RecyclerView;
import com.github.mikephil.charting.utils.Utils;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

/* loaded from: classes2.dex */
public class RecyclerTouchListener implements RecyclerView.q, dm2 {
    public View A;
    public boolean B;
    public int C;
    public boolean D;
    public View E;
    public View F;
    public int G;
    public int H;
    public ArrayList<Integer> I;
    public g J;
    public h K;
    public k L;
    public boolean Q;
    public j S;
    public i T;
    public Activity b;
    public List<Integer> c;
    public List<Integer> d;
    public List<Integer> e;
    public List<Integer> f;
    public Set<Integer> g;
    public int h;
    public int i;
    public int j;
    public RecyclerView m;
    public float p;
    public float q;
    public boolean r;
    public int s;
    public VelocityTracker t;
    public int u;
    public View v;
    public boolean w;
    public boolean x;
    public boolean y;
    public int z;
    public final Handler a = new Handler();
    public long k = 300;
    public long l = 150;
    public int n = 1;
    public int o = 0;
    public boolean M = false;
    public boolean N = false;
    public boolean O = false;
    public int P = 800;
    public Runnable R = new a();
    public int U = 0;

    /* loaded from: classes2.dex */
    public enum Animation {
        OPEN,
        CLOSE
    }

    /* loaded from: classes2.dex */
    public class a implements Runnable {
        public a() {
        }

        @Override // java.lang.Runnable
        public void run() {
            if (RecyclerTouchListener.this.N) {
                RecyclerTouchListener.this.D = true;
                if (RecyclerTouchListener.this.x || RecyclerTouchListener.this.u < 0) {
                    return;
                }
                RecyclerTouchListener recyclerTouchListener = RecyclerTouchListener.this;
                if (recyclerTouchListener.e.contains(Integer.valueOf(recyclerTouchListener.u)) || RecyclerTouchListener.this.B) {
                    return;
                }
                boolean unused = RecyclerTouchListener.this.Q;
                RecyclerTouchListener.this.K.a(RecyclerTouchListener.this.u);
            }
        }
    }

    /* loaded from: classes2.dex */
    public class b extends RecyclerView.r {
        public b() {
        }

        @Override // androidx.recyclerview.widget.RecyclerView.r
        public void onScrollStateChanged(RecyclerView recyclerView, int i) {
            RecyclerTouchListener.this.w(i != 1);
            RecyclerTouchListener.this.B = i != 0;
        }

        @Override // androidx.recyclerview.widget.RecyclerView.r
        public void onScrolled(RecyclerView recyclerView, int i, int i2) {
        }
    }

    /* loaded from: classes2.dex */
    public class c implements Animator.AnimatorListener {
        public final /* synthetic */ j a;
        public final /* synthetic */ ObjectAnimator f0;

        public c(RecyclerTouchListener recyclerTouchListener, j jVar, ObjectAnimator objectAnimator) {
            this.a = jVar;
            this.f0 = objectAnimator;
        }

        @Override // android.animation.Animator.AnimatorListener
        public void onAnimationCancel(Animator animator) {
        }

        @Override // android.animation.Animator.AnimatorListener
        public void onAnimationEnd(Animator animator) {
            j jVar = this.a;
            if (jVar != null) {
                jVar.b();
            }
            this.f0.removeAllListeners();
        }

        @Override // android.animation.Animator.AnimatorListener
        public void onAnimationRepeat(Animator animator) {
        }

        @Override // android.animation.Animator.AnimatorListener
        public void onAnimationStart(Animator animator) {
        }
    }

    /* loaded from: classes2.dex */
    public class d implements Animator.AnimatorListener {
        public final /* synthetic */ j a;
        public final /* synthetic */ Animation f0;
        public final /* synthetic */ ObjectAnimator g0;

        public d(RecyclerTouchListener recyclerTouchListener, j jVar, Animation animation, ObjectAnimator objectAnimator) {
            this.a = jVar;
            this.f0 = animation;
            this.g0 = objectAnimator;
        }

        @Override // android.animation.Animator.AnimatorListener
        public void onAnimationCancel(Animator animator) {
        }

        @Override // android.animation.Animator.AnimatorListener
        public void onAnimationEnd(Animator animator) {
            j jVar = this.a;
            if (jVar != null) {
                Animation animation = this.f0;
                if (animation == Animation.OPEN) {
                    jVar.a();
                } else if (animation == Animation.CLOSE) {
                    jVar.b();
                }
            }
            this.g0.removeAllListeners();
        }

        @Override // android.animation.Animator.AnimatorListener
        public void onAnimationRepeat(Animator animator) {
        }

        @Override // android.animation.Animator.AnimatorListener
        public void onAnimationStart(Animator animator) {
        }
    }

    /* loaded from: classes2.dex */
    public class e implements j {
        public final /* synthetic */ View a;

        public e(View view) {
            this.a = view;
        }

        @Override // net.safemoon.androidwallet.adapter.touchHelper.RecyclerTouchListener.j
        public void a() {
            if (RecyclerTouchListener.this.S != null) {
                RecyclerTouchListener.this.S.a();
            }
        }

        @Override // net.safemoon.androidwallet.adapter.touchHelper.RecyclerTouchListener.j
        public void b() {
            if (RecyclerTouchListener.this.S != null) {
                RecyclerTouchListener.this.S.b();
            }
            View view = this.a;
            if (view != null) {
                view.setVisibility(0);
            }
        }
    }

    /* loaded from: classes2.dex */
    public class f implements j {
        public final /* synthetic */ int a;
        public final /* synthetic */ int b;

        public f(int i, int i2) {
            this.a = i;
            this.b = i2;
        }

        @Override // net.safemoon.androidwallet.adapter.touchHelper.RecyclerTouchListener.j
        public void a() {
            if (RecyclerTouchListener.this.S != null) {
                RecyclerTouchListener.this.S.a();
            }
        }

        @Override // net.safemoon.androidwallet.adapter.touchHelper.RecyclerTouchListener.j
        public void b() {
            RecyclerTouchListener.this.L.a(this.a, this.b);
            if (RecyclerTouchListener.this.S != null) {
                RecyclerTouchListener.this.S.b();
            }
        }
    }

    /* loaded from: classes2.dex */
    public interface g {
        void a(int i);

        void b(int i, int i2);
    }

    /* loaded from: classes2.dex */
    public interface h {
        void a(int i);
    }

    /* loaded from: classes2.dex */
    public interface i {
        void a();

        void b();
    }

    /* loaded from: classes2.dex */
    public interface j {
        void a();

        void b();
    }

    /* loaded from: classes2.dex */
    public interface k {
        void a(int i, int i2);
    }

    /* loaded from: classes2.dex */
    public interface l {
        void a(dm2 dm2Var);
    }

    public RecyclerTouchListener(Activity activity, RecyclerView recyclerView) {
        this.b = activity;
        ViewConfiguration viewConfiguration = ViewConfiguration.get(recyclerView.getContext());
        this.h = viewConfiguration.getScaledTouchSlop();
        this.i = viewConfiguration.getScaledMinimumFlingVelocity() * 16;
        this.j = viewConfiguration.getScaledMaximumFlingVelocity();
        this.m = recyclerView;
        this.x = false;
        this.z = -1;
        this.A = null;
        this.y = false;
        this.c = new ArrayList();
        this.e = new ArrayList();
        this.g = new HashSet();
        this.d = new ArrayList();
        this.f = new ArrayList();
        this.I = new ArrayList<>();
        this.B = false;
        this.m.l(new b());
    }

    public final boolean A(int i2) {
        RecyclerView recyclerView = this.m;
        return recyclerView == null || this.g.contains(Integer.valueOf(recyclerView.getAdapter().getItemViewType(i2)));
    }

    @Override // androidx.recyclerview.widget.RecyclerView.q
    public void a(RecyclerView recyclerView, MotionEvent motionEvent) {
        u(motionEvent);
    }

    @Override // androidx.recyclerview.widget.RecyclerView.q
    public boolean c(RecyclerView recyclerView, MotionEvent motionEvent) {
        return u(motionEvent);
    }

    @Override // androidx.recyclerview.widget.RecyclerView.q
    public void e(boolean z) {
    }

    public final void n(View view, Animation animation, long j2) {
        if (animation == Animation.OPEN) {
            ObjectAnimator ofFloat = ObjectAnimator.ofFloat(this.E, View.TRANSLATION_X, -this.n);
            ofFloat.setDuration(j2);
            ofFloat.setInterpolator(new DecelerateInterpolator(1.5f));
            ofFloat.start();
            p(view, Utils.FLOAT_EPSILON, j2);
        } else if (animation == Animation.CLOSE) {
            ObjectAnimator ofFloat2 = ObjectAnimator.ofFloat(this.E, View.TRANSLATION_X, Utils.FLOAT_EPSILON);
            ofFloat2.setDuration(j2);
            ofFloat2.setInterpolator(new DecelerateInterpolator(1.5f));
            ofFloat2.start();
            p(view, 1.0f, j2);
        }
    }

    public final void o(View view, Animation animation, long j2, j jVar) {
        ObjectAnimator ofFloat;
        if (animation == Animation.OPEN) {
            ofFloat = ObjectAnimator.ofFloat(this.E, View.TRANSLATION_X, -this.n);
            ofFloat.setDuration(j2);
            ofFloat.setInterpolator(new DecelerateInterpolator(1.5f));
            ofFloat.start();
            p(view, Utils.FLOAT_EPSILON, j2);
        } else {
            ofFloat = ObjectAnimator.ofFloat(this.E, View.TRANSLATION_X, Utils.FLOAT_EPSILON);
            ofFloat.setDuration(j2);
            ofFloat.setInterpolator(new DecelerateInterpolator(1.5f));
            ofFloat.start();
            p(view, 1.0f, j2);
        }
        ofFloat.addListener(new d(this, jVar, animation, ofFloat));
    }

    public final void p(View view, float f2, long j2) {
        ArrayList<Integer> arrayList = this.I;
        if (arrayList != null) {
            Iterator<Integer> it = arrayList.iterator();
            while (it.hasNext()) {
                view.findViewById(it.next().intValue()).animate().alpha(f2).setDuration(j2);
            }
        }
    }

    @Deprecated
    public void q() {
        View view = this.A;
        if (view == null) {
            return;
        }
        view.animate().translationX(Utils.FLOAT_EPSILON).setDuration(this.l).setListener(null);
        p(this.A, 1.0f, this.l);
        this.x = false;
        this.A = null;
        this.z = -1;
    }

    public void r(j jVar) {
        View view = this.A;
        if (view == null) {
            return;
        }
        ObjectAnimator ofFloat = ObjectAnimator.ofFloat(view, View.TRANSLATION_X, Utils.FLOAT_EPSILON);
        ofFloat.setDuration(this.l);
        ofFloat.addListener(new c(this, jVar, ofFloat));
        ofFloat.start();
        p(this.A, 1.0f, this.l);
        this.x = false;
        this.A = null;
        this.z = -1;
    }

    public final int s(MotionEvent motionEvent) {
        for (int i2 = 0; i2 < this.d.size(); i2++) {
            if (this.v != null) {
                Rect rect = new Rect();
                this.v.findViewById(this.d.get(i2).intValue()).getGlobalVisibleRect(rect);
                if (rect.contains((int) motionEvent.getRawX(), (int) motionEvent.getRawY())) {
                    return this.d.get(i2).intValue();
                }
            }
        }
        return -1;
    }

    public final int t(MotionEvent motionEvent) {
        for (int i2 = 0; i2 < this.f.size(); i2++) {
            if (this.v != null) {
                Rect rect = new Rect();
                this.v.findViewById(this.f.get(i2).intValue()).getGlobalVisibleRect(rect);
                if (rect.contains((int) motionEvent.getRawX(), (int) motionEvent.getRawY())) {
                    return this.f.get(i2).intValue();
                }
            }
        }
        return -1;
    }

    public final boolean u(MotionEvent motionEvent) {
        boolean z;
        boolean z2;
        boolean z3;
        boolean z4;
        int t;
        int i2;
        int i3;
        int i4;
        int i5;
        int i6;
        VelocityTracker velocityTracker;
        boolean z5;
        int i7;
        i iVar;
        if (this.O && this.n < 2) {
            if (this.b.findViewById(this.H) != null) {
                this.n = this.b.findViewById(this.H).getWidth();
            }
            this.m.getHeight();
        }
        int actionMasked = motionEvent.getActionMasked();
        if (actionMasked != 0) {
            float f2 = Utils.FLOAT_EPSILON;
            if (actionMasked == 1) {
                i iVar2 = this.T;
                if (iVar2 != null) {
                    iVar2.b();
                }
                this.a.removeCallbacks(this.R);
                if (!this.D && ((this.t != null || !this.O) && this.u >= 0)) {
                    float rawX = motionEvent.getRawX() - this.p;
                    if (this.r) {
                        z = rawX < Utils.FLOAT_EPSILON;
                        z2 = rawX > Utils.FLOAT_EPSILON;
                    } else {
                        z = false;
                        z2 = false;
                    }
                    if (Math.abs(rawX) <= this.n / 2 || !this.r) {
                        if (this.O) {
                            this.t.addMovement(motionEvent);
                            this.t.computeCurrentVelocity(1000);
                            float xVelocity = this.t.getXVelocity();
                            float abs = Math.abs(xVelocity);
                            float abs2 = Math.abs(this.t.getYVelocity());
                            if (this.i <= abs && abs <= this.j && abs2 < abs && this.r) {
                                boolean z6 = ((xVelocity > Utils.FLOAT_EPSILON ? 1 : (xVelocity == Utils.FLOAT_EPSILON ? 0 : -1)) < 0) == ((rawX > Utils.FLOAT_EPSILON ? 1 : (rawX == Utils.FLOAT_EPSILON ? 0 : -1)) < 0);
                                z3 = ((xVelocity > Utils.FLOAT_EPSILON ? 1 : (xVelocity == Utils.FLOAT_EPSILON ? 0 : -1)) > 0) == ((rawX > Utils.FLOAT_EPSILON ? 1 : (rawX == Utils.FLOAT_EPSILON ? 0 : -1)) > 0);
                                z4 = z6;
                            }
                        }
                        z3 = false;
                        z4 = false;
                    } else {
                        z4 = rawX < Utils.FLOAT_EPSILON;
                        z3 = rawX > Utils.FLOAT_EPSILON;
                    }
                    if (this.O && !z2 && z4 && (i6 = this.u) != -1 && !this.c.contains(Integer.valueOf(i6)) && !this.x) {
                        View view = this.v;
                        int i8 = this.u;
                        this.o++;
                        n(view, Animation.OPEN, this.k);
                        this.x = true;
                        this.A = this.E;
                        this.z = i8;
                    } else if (this.O && !z && z3 && (i5 = this.u) != -1 && !this.c.contains(Integer.valueOf(i5)) && this.x) {
                        View view2 = this.v;
                        this.o++;
                        n(view2, Animation.CLOSE, this.k);
                        this.x = false;
                        this.A = null;
                        this.z = -1;
                    } else {
                        boolean z7 = this.O;
                        if (z7 && z && !this.x) {
                            o(this.v, Animation.CLOSE, this.k, new e(this.F));
                            this.x = false;
                            this.A = null;
                            this.z = -1;
                        } else if (z7 && z2 && this.x) {
                            n(this.v, Animation.OPEN, this.k);
                            this.x = true;
                            this.A = this.E;
                            this.z = this.u;
                        } else if (z7 && z2 && !this.x) {
                            n(this.v, Animation.CLOSE, this.k);
                            this.x = false;
                            this.A = null;
                            this.z = -1;
                        } else if (z7 && z && this.x) {
                            n(this.v, Animation.OPEN, this.k);
                            this.x = true;
                            this.A = this.E;
                            this.z = this.u;
                        } else if (!z2 && !z) {
                            if (z7 && this.y) {
                                n(this.v, Animation.CLOSE, this.k);
                                this.x = false;
                                this.A = null;
                                this.z = -1;
                            } else if (this.M && !this.x && (i4 = this.u) >= 0 && !this.e.contains(Integer.valueOf(i4)) && v(motionEvent) && !this.B) {
                                this.J.a(this.u);
                            } else if (this.M && !this.x && (i3 = this.u) >= 0 && !this.e.contains(Integer.valueOf(i3)) && !v(motionEvent) && !this.B) {
                                int s = s(motionEvent);
                                if (s >= 0) {
                                    this.J.b(s, this.u);
                                }
                            } else if (this.O && this.x && !this.y && (t = t(motionEvent)) >= 0 && (i2 = this.u) >= 0) {
                                r(new f(t, i2));
                            }
                        }
                    }
                    if (this.O) {
                        this.t.recycle();
                        this.t = null;
                    }
                    this.p = Utils.FLOAT_EPSILON;
                    this.q = Utils.FLOAT_EPSILON;
                    this.v = null;
                    this.u = -1;
                    this.r = false;
                    this.F = null;
                }
            } else if (actionMasked != 2) {
                if (actionMasked == 3) {
                    this.a.removeCallbacks(this.R);
                    if (!this.D && this.t != null) {
                        if (this.O) {
                            if (this.v != null && this.r && this.u != -1) {
                                if (motionEvent.getRawX() - this.p < Utils.FLOAT_EPSILON) {
                                    this.o++;
                                    n(this.v, Animation.OPEN, this.k);
                                    this.x = true;
                                    this.A = this.E;
                                    this.z = this.u;
                                }
                            }
                            View view3 = this.v;
                            if (view3 != null && this.r) {
                                n(view3, Animation.CLOSE, this.k);
                            }
                            this.t.recycle();
                            this.t = null;
                            this.r = false;
                            this.F = null;
                        }
                        this.p = Utils.FLOAT_EPSILON;
                        this.q = Utils.FLOAT_EPSILON;
                        this.v = null;
                        this.u = -1;
                    }
                }
            } else if (!this.D && (velocityTracker = this.t) != null && !this.w && this.O) {
                this.U++;
                velocityTracker.addMovement(motionEvent);
                float rawX2 = motionEvent.getRawX() - this.p;
                float rawY = motionEvent.getRawY() - this.q;
                if (!this.r && Math.abs(rawX2) > this.h && Math.abs(rawY) < Math.abs(rawX2) / 2.0f) {
                    this.a.removeCallbacks(this.R);
                    this.r = true;
                    this.s = rawX2 > Utils.FLOAT_EPSILON ? this.h : -this.h;
                }
                if (!this.r && this.U > 6 && (iVar = this.T) != null) {
                    iVar.b();
                }
                if (this.O && this.r && !this.c.contains(Integer.valueOf(this.u))) {
                    if (this.F == null) {
                        View findViewById = this.v.findViewById(this.H);
                        this.F = findViewById;
                        findViewById.setVisibility(0);
                    }
                    if (rawX2 < this.h && !this.x) {
                        float f3 = rawX2 - this.s;
                        this.E.setTranslationX(Math.abs(f3) > ((float) this.n) ? -i7 : f3);
                        if (this.E.getTranslationX() > Utils.FLOAT_EPSILON) {
                            this.E.setTranslationX(Utils.FLOAT_EPSILON);
                        }
                        ArrayList<Integer> arrayList = this.I;
                        if (arrayList != null) {
                            Iterator<Integer> it = arrayList.iterator();
                            while (it.hasNext()) {
                                this.v.findViewById(it.next().intValue()).setAlpha(1.0f - (Math.abs(f3) / this.n));
                            }
                        }
                    } else if (rawX2 > Utils.FLOAT_EPSILON && (z5 = this.x)) {
                        if (z5) {
                            float f4 = (rawX2 - this.s) - this.n;
                            View view4 = this.E;
                            if (f4 <= Utils.FLOAT_EPSILON) {
                                f2 = f4;
                            }
                            view4.setTranslationX(f2);
                            ArrayList<Integer> arrayList2 = this.I;
                            if (arrayList2 != null) {
                                Iterator<Integer> it2 = arrayList2.iterator();
                                while (it2.hasNext()) {
                                    this.v.findViewById(it2.next().intValue()).setAlpha(1.0f - (Math.abs(f4) / this.n));
                                }
                            }
                        } else {
                            float f5 = (rawX2 - this.s) - this.n;
                            View view5 = this.E;
                            if (f5 <= Utils.FLOAT_EPSILON) {
                                f2 = f5;
                            }
                            view5.setTranslationX(f2);
                            ArrayList<Integer> arrayList3 = this.I;
                            if (arrayList3 != null) {
                                Iterator<Integer> it3 = arrayList3.iterator();
                                while (it3.hasNext()) {
                                    this.v.findViewById(it3.next().intValue()).setAlpha(1.0f - (Math.abs(f5) / this.n));
                                }
                            }
                        }
                    }
                    return true;
                } else if (this.O && this.r && this.c.contains(Integer.valueOf(this.u))) {
                    i iVar3 = this.T;
                    if (iVar3 != null) {
                        iVar3.b();
                    }
                    if (rawX2 < this.h && !this.x) {
                        float f6 = rawX2 - this.s;
                        if (this.F == null) {
                            this.F = this.v.findViewById(this.H);
                        }
                        View view6 = this.F;
                        if (view6 != null) {
                            view6.setVisibility(8);
                        }
                        this.E.setTranslationX(f6 / 5.0f);
                        if (this.E.getTranslationX() > Utils.FLOAT_EPSILON) {
                            this.E.setTranslationX(Utils.FLOAT_EPSILON);
                        }
                    }
                    return true;
                }
            }
        } else {
            this.U = 0;
            if (!this.w) {
                Rect rect = new Rect();
                int childCount = this.m.getChildCount();
                int[] iArr = new int[2];
                this.m.getLocationOnScreen(iArr);
                int rawX3 = ((int) motionEvent.getRawX()) - iArr[0];
                int rawY2 = ((int) motionEvent.getRawY()) - iArr[1];
                int i9 = 0;
                while (true) {
                    if (i9 >= childCount) {
                        break;
                    }
                    View childAt = this.m.getChildAt(i9);
                    childAt.getHitRect(rect);
                    if (rect.contains(rawX3, rawY2)) {
                        this.v = childAt;
                        break;
                    }
                    i9++;
                }
                if (this.v != null) {
                    i iVar4 = this.T;
                    if (iVar4 != null) {
                        iVar4.a();
                    }
                    this.p = motionEvent.getRawX();
                    this.q = motionEvent.getRawY();
                    int f0 = this.m.f0(this.v);
                    this.u = f0;
                    if (A(f0)) {
                        this.u = -1;
                        return false;
                    }
                    if (this.N) {
                        this.D = false;
                        this.a.postDelayed(this.R, this.P);
                    }
                    if (this.O) {
                        VelocityTracker obtain = VelocityTracker.obtain();
                        this.t = obtain;
                        obtain.addMovement(motionEvent);
                        this.E = this.v.findViewById(this.G);
                        View findViewById2 = this.v.findViewById(this.H);
                        this.F = findViewById2;
                        View view7 = this.E;
                        if (view7 != null) {
                            findViewById2.setMinimumHeight(view7.getHeight());
                        }
                        if (this.x && this.E != null) {
                            this.a.removeCallbacks(this.R);
                            this.E.getGlobalVisibleRect(rect);
                            this.y = rect.contains((int) motionEvent.getRawX(), (int) motionEvent.getRawY());
                        } else {
                            this.y = false;
                        }
                    }
                }
                motionEvent.getRawX();
                motionEvent.getRawY();
                this.m.getHitRect(rect);
                if (this.O && this.x && this.u != this.z) {
                    this.a.removeCallbacks(this.R);
                    r(null);
                }
            }
        }
        return false;
    }

    public final boolean v(MotionEvent motionEvent) {
        for (int i2 = 0; i2 < this.d.size(); i2++) {
            if (this.v != null) {
                Rect rect = new Rect();
                this.v.findViewById(this.d.get(i2).intValue()).getGlobalVisibleRect(rect);
                if (rect.contains((int) motionEvent.getRawX(), (int) motionEvent.getRawY())) {
                    return false;
                }
            }
        }
        return true;
    }

    public void w(boolean z) {
        this.w = !z;
    }

    public RecyclerTouchListener x(Integer... numArr) {
        this.f = new ArrayList(Arrays.asList(numArr));
        return this;
    }

    public RecyclerTouchListener y(int i2, int i3, k kVar) {
        this.O = true;
        int i4 = this.G;
        if (i4 != 0 && i2 != i4) {
            throw new IllegalArgumentException("foregroundID does not match previously set ID");
        }
        this.G = i2;
        this.H = i3;
        this.L = kVar;
        Activity activity = this.b;
        if (activity instanceof l) {
            ((l) activity).a(this);
        }
        DisplayMetrics displayMetrics = new DisplayMetrics();
        this.b.getWindowManager().getDefaultDisplay().getMetrics(displayMetrics);
        this.C = displayMetrics.heightPixels;
        return this;
    }

    public RecyclerTouchListener z(Integer... numArr) {
        this.c = new ArrayList(Arrays.asList(numArr));
        return this;
    }
}
