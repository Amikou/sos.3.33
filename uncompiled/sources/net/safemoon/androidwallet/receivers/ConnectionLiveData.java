package net.safemoon.androidwallet.receivers;

import android.annotation.TargetApi;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.net.ConnectivityManager;
import android.net.Network;
import android.net.NetworkCapabilities;
import android.net.NetworkInfo;
import android.net.NetworkRequest;
import android.os.Build;
import androidx.lifecycle.LiveData;
import java.util.Objects;

/* compiled from: ConnectionLiveData.kt */
/* loaded from: classes2.dex */
public final class ConnectionLiveData extends LiveData<Boolean> {
    public final Context a;
    public ConnectivityManager b;
    public ConnectivityManager.NetworkCallback c;
    public final NetworkRequest.Builder d;
    public final ConnectionLiveData$networkReceiver$1 e;

    /* compiled from: ConnectionLiveData.kt */
    /* loaded from: classes2.dex */
    public static final class a extends ConnectivityManager.NetworkCallback {
        public a() {
        }

        @Override // android.net.ConnectivityManager.NetworkCallback
        public void onAvailable(Network network) {
            fs1.f(network, "network");
            ConnectionLiveData.this.postValue(Boolean.TRUE);
        }

        @Override // android.net.ConnectivityManager.NetworkCallback
        public void onLost(Network network) {
            fs1.f(network, "network");
            ConnectionLiveData.this.postValue(Boolean.FALSE);
        }
    }

    /* compiled from: ConnectionLiveData.kt */
    /* loaded from: classes2.dex */
    public static final class b extends ConnectivityManager.NetworkCallback {
        public b() {
        }

        @Override // android.net.ConnectivityManager.NetworkCallback
        public void onCapabilitiesChanged(Network network, NetworkCapabilities networkCapabilities) {
            fs1.f(network, "network");
            fs1.f(networkCapabilities, "networkCapabilities");
            ConnectionLiveData connectionLiveData = ConnectionLiveData.this;
            if (networkCapabilities.hasCapability(12) && networkCapabilities.hasCapability(16)) {
                connectionLiveData.postValue(Boolean.TRUE);
            }
        }

        @Override // android.net.ConnectivityManager.NetworkCallback
        public void onLost(Network network) {
            fs1.f(network, "network");
            ConnectionLiveData.this.postValue(Boolean.FALSE);
        }
    }

    /* JADX WARN: Type inference failed for: r2v6, types: [net.safemoon.androidwallet.receivers.ConnectionLiveData$networkReceiver$1] */
    public ConnectionLiveData(Context context) {
        fs1.f(context, "context");
        this.a = context;
        Object systemService = context.getSystemService("connectivity");
        Objects.requireNonNull(systemService, "null cannot be cast to non-null type android.net.ConnectivityManager");
        this.b = (ConnectivityManager) systemService;
        NetworkRequest.Builder addTransportType = new NetworkRequest.Builder().addTransportType(0).addTransportType(1);
        fs1.e(addTransportType, "Builder()\n        .addTr…abilities.TRANSPORT_WIFI)");
        this.d = addTransportType;
        this.e = new BroadcastReceiver() { // from class: net.safemoon.androidwallet.receivers.ConnectionLiveData$networkReceiver$1
            @Override // android.content.BroadcastReceiver
            public void onReceive(Context context2, Intent intent) {
                fs1.f(context2, "context");
                fs1.f(intent, "intent");
                ConnectionLiveData.this.g();
            }
        };
    }

    public final ConnectivityManager.NetworkCallback c() {
        if (Build.VERSION.SDK_INT >= 21) {
            a aVar = new a();
            this.c = aVar;
            return aVar;
        }
        throw new IllegalAccessError("Accessing wrong API version");
    }

    public final ConnectivityManager.NetworkCallback d() {
        if (Build.VERSION.SDK_INT >= 23) {
            b bVar = new b();
            this.c = bVar;
            return bVar;
        }
        throw new IllegalAccessError("Accessing wrong API version");
    }

    @TargetApi(21)
    public final void e() {
        this.b.registerNetworkCallback(this.d.build(), c());
    }

    @TargetApi(23)
    public final void f() {
        this.b.registerNetworkCallback(this.d.build(), d());
    }

    public final void g() {
        NetworkInfo activeNetworkInfo = this.b.getActiveNetworkInfo();
        boolean z = false;
        if (activeNetworkInfo != null && activeNetworkInfo.isConnected()) {
            z = true;
        }
        postValue(Boolean.valueOf(z));
    }

    @Override // androidx.lifecycle.LiveData
    public void onActive() {
        super.onActive();
        g();
        int i = Build.VERSION.SDK_INT;
        if (i >= 24) {
            this.b.registerDefaultNetworkCallback(d());
        } else if (i >= 23) {
            f();
        } else if (i >= 21) {
            e();
        } else if (i < 21) {
            this.a.registerReceiver(this.e, new IntentFilter("android.net.conn.CONNECTIVITY_CHANGE"));
        }
    }

    @Override // androidx.lifecycle.LiveData
    public void onInactive() {
        super.onInactive();
        if (Build.VERSION.SDK_INT >= 21) {
            ConnectivityManager connectivityManager = this.b;
            ConnectivityManager.NetworkCallback networkCallback = this.c;
            if (networkCallback == null) {
                fs1.r("connectivityManagerCallback");
                networkCallback = null;
            }
            connectivityManager.unregisterNetworkCallback(networkCallback);
            return;
        }
        this.a.unregisterReceiver(this.e);
    }
}
