package net.safemoon.androidwallet.mapper.token;

import android.content.Context;

/* compiled from: TokenDisplayModelMapper.kt */
/* loaded from: classes2.dex */
public final class TokenDisplayModelMapper implements sm1 {
    public final Context a;
    public final dm1 b;

    public TokenDisplayModelMapper(Context context, dm1 dm1Var) {
        fs1.f(context, "context");
        fs1.f(dm1Var, "getIsUserHasTokenUseCase");
        this.a = context;
        this.b = dm1Var;
    }

    /* JADX WARN: Multi-variable type inference failed */
    /* JADX WARN: Removed duplicated region for block: B:10:0x0029  */
    /* JADX WARN: Removed duplicated region for block: B:14:0x006d  */
    /* JADX WARN: Removed duplicated region for block: B:17:0x008d  */
    /* JADX WARN: Removed duplicated region for block: B:26:0x0119  */
    /* JADX WARN: Type inference failed for: r13v2 */
    /* JADX WARN: Type inference failed for: r13v3 */
    /* JADX WARN: Type inference failed for: r13v5, types: [java.util.List] */
    /* JADX WARN: Type inference failed for: r6v5, types: [java.util.List] */
    /* JADX WARN: Unsupported multi-entry loop pattern (BACK_EDGE: B:24:0x00e7 -> B:25:0x00f5). Please submit an issue!!! */
    @Override // defpackage.sm1
    /*
        Code decompiled incorrectly, please refer to instructions dump.
        To view partially-correct code enable 'Show inconsistent code' option in preferences
    */
    public java.lang.Object a(androidx.lifecycle.LiveData<java.util.List<net.safemoon.androidwallet.model.token.abstraction.IToken>> r27, net.safemoon.androidwallet.common.TokenType r28, defpackage.q70<? super androidx.lifecycle.LiveData<java.util.List<defpackage.q9>>> r29) {
        /*
            Method dump skipped, instructions count: 287
            To view this dump change 'Code comments level' option to 'DEBUG'
        */
        throw new UnsupportedOperationException("Method not decompiled: net.safemoon.androidwallet.mapper.token.TokenDisplayModelMapper.a(androidx.lifecycle.LiveData, net.safemoon.androidwallet.common.TokenType, q70):java.lang.Object");
    }
}
