package net.safemoon.androidwallet.mapper.token;

import kotlin.coroutines.jvm.internal.ContinuationImpl;
import kotlin.coroutines.jvm.internal.a;

/* compiled from: TokenDisplayModelMapper.kt */
@a(c = "net.safemoon.androidwallet.mapper.token.TokenDisplayModelMapper", f = "TokenDisplayModelMapper.kt", l = {38}, m = "map")
/* loaded from: classes2.dex */
public final class TokenDisplayModelMapper$map$1 extends ContinuationImpl {
    public int I$0;
    public Object L$0;
    public Object L$1;
    public Object L$2;
    public Object L$3;
    public Object L$4;
    public Object L$5;
    public Object L$6;
    public Object L$7;
    public Object L$8;
    public Object L$9;
    public int label;
    public /* synthetic */ Object result;
    public final /* synthetic */ TokenDisplayModelMapper this$0;

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public TokenDisplayModelMapper$map$1(TokenDisplayModelMapper tokenDisplayModelMapper, q70<? super TokenDisplayModelMapper$map$1> q70Var) {
        super(q70Var);
        this.this$0 = tokenDisplayModelMapper;
    }

    @Override // kotlin.coroutines.jvm.internal.BaseContinuationImpl
    public final Object invokeSuspend(Object obj) {
        this.result = obj;
        this.label |= Integer.MIN_VALUE;
        return this.this$0.a(null, null, this);
    }
}
