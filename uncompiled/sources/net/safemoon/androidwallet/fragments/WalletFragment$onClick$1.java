package net.safemoon.androidwallet.fragments;

import android.content.DialogInterface;
import defpackage.wm4;
import kotlin.jvm.internal.Lambda;

/* compiled from: WalletFragment.kt */
/* loaded from: classes2.dex */
public final class WalletFragment$onClick$1 extends Lambda implements tc1<DialogInterface, te4> {
    public final /* synthetic */ WalletFragment this$0;

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public WalletFragment$onClick$1(WalletFragment walletFragment) {
        super(1);
        this.this$0 = walletFragment;
    }

    @Override // defpackage.tc1
    public /* bridge */ /* synthetic */ te4 invoke(DialogInterface dialogInterface) {
        invoke2(dialogInterface);
        return te4.a;
    }

    /* renamed from: invoke  reason: avoid collision after fix types in other method */
    public final void invoke2(DialogInterface dialogInterface) {
        fs1.f(dialogInterface, "it");
        WalletFragment walletFragment = this.this$0;
        wm4.b g = wm4.g(false);
        fs1.e(g, "actionNavigationWalletToTokenListFragment(false)");
        walletFragment.g(g);
    }
}
