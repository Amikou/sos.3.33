package net.safemoon.androidwallet.fragments;

import kotlin.jvm.internal.Lambda;

/* compiled from: TransferDetailsFragmentStatus.kt */
/* loaded from: classes2.dex */
public final class TransferDetailsFragmentStatus$newTransaction$2 extends Lambda implements rc1<Boolean> {
    public final /* synthetic */ TransferDetailsFragmentStatus this$0;

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public TransferDetailsFragmentStatus$newTransaction$2(TransferDetailsFragmentStatus transferDetailsFragmentStatus) {
        super(0);
        this.this$0 = transferDetailsFragmentStatus;
    }

    /* JADX WARN: Can't rename method to resolve collision */
    @Override // defpackage.rc1
    public final Boolean invoke() {
        return Boolean.valueOf(this.this$0.requireArguments().getBoolean("isNewTransaction"));
    }
}
