package net.safemoon.androidwallet.fragments;

import android.content.Context;
import android.content.Intent;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.fragment.app.FragmentActivity;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentViewModelLazyKt;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;
import com.AKT.anonymouskey.ui.login.AKTServerFunctions;
import com.fasterxml.jackson.core.util.MinimalPrettyPrinter;
import defpackage.qm1;
import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.NoSuchElementException;
import java.util.Objects;
import kotlin.text.StringsKt__StringsKt;
import net.safemoon.androidwallet.R;
import net.safemoon.androidwallet.activity.MainActivity;
import net.safemoon.androidwallet.activity.common.BasicActivity;
import net.safemoon.androidwallet.dialogs.G2FAVerfication;
import net.safemoon.androidwallet.dialogs.ProgressLoading;
import net.safemoon.androidwallet.fragments.SwitchWalletFragment;
import net.safemoon.androidwallet.fragments.common.BaseMainFragment;
import net.safemoon.androidwallet.model.wallets.Wallet;
import net.safemoon.androidwallet.viewmodels.AKTWebSocketHandlingViewModel;
import net.safemoon.androidwallet.viewmodels.MultiWalletViewModel;
import net.safemoon.androidwallet.views.gesture.WalletRecyclerItemSwipeHelper;
import wallet.core.jni.CoinType;
import wallet.core.jni.HDWallet;

/* compiled from: SwitchWalletFragment.kt */
/* loaded from: classes2.dex */
public final class SwitchWalletFragment extends BaseMainFragment {
    public final sy1 i0 = FragmentViewModelLazyKt.a(this, d53.b(MultiWalletViewModel.class), new SwitchWalletFragment$special$$inlined$viewModels$default$2(new SwitchWalletFragment$special$$inlined$viewModels$default$1(this)), null);
    public final sy1 j0 = FragmentViewModelLazyKt.a(this, d53.b(MultiWalletViewModel.class), new SwitchWalletFragment$special$$inlined$viewModels$default$4(new SwitchWalletFragment$special$$inlined$viewModels$default$3(this)), null);
    public eb1 k0;
    public ProgressLoading l0;
    public final List<String> m0;
    public int n0;
    public final List<String> o0;
    public final Map<String, String> p0;
    public Wallet q0;
    public lc2 r0;
    public final sy1 s0;
    public final sy1 t0;
    public final w41 u0;

    /* compiled from: SwitchWalletFragment.kt */
    /* loaded from: classes2.dex */
    public enum RequestSocketState {
        NONE,
        GET_BLOB,
        PUT_BLOB
    }

    /* compiled from: SwitchWalletFragment.kt */
    /* loaded from: classes2.dex */
    public static final class a implements G2FAVerfication.b {
        public final /* synthetic */ Wallet b;

        public a(Wallet wallet2) {
            this.b = wallet2;
        }

        @Override // net.safemoon.androidwallet.dialogs.G2FAVerfication.b
        public void a() {
            y54.a("Khang").a("G2FAVerificationCallback onError() called", new Object[0]);
        }

        @Override // net.safemoon.androidwallet.dialogs.G2FAVerfication.b
        public void onSuccess() {
            if (SwitchWalletFragment.this.l0()) {
                w41 w41Var = SwitchWalletFragment.this.u0;
                SwitchWalletFragment switchWalletFragment = SwitchWalletFragment.this;
                w41Var.a(switchWalletFragment, switchWalletFragment.G0(this.b));
                return;
            }
            SwitchWalletFragment.this.x0(this.b);
        }
    }

    /* compiled from: SwitchWalletFragment.kt */
    /* loaded from: classes2.dex */
    public static final class b implements G2FAVerfication.b {
        public b() {
        }

        @Override // net.safemoon.androidwallet.dialogs.G2FAVerfication.b
        public void a() {
            y54.a("Khang").a("G2FAVerificationCallback onError() called", new Object[0]);
        }

        @Override // net.safemoon.androidwallet.dialogs.G2FAVerfication.b
        public void onSuccess() {
            if (SwitchWalletFragment.this.l0()) {
                w41 w41Var = SwitchWalletFragment.this.u0;
                SwitchWalletFragment switchWalletFragment = SwitchWalletFragment.this;
                w41Var.a(switchWalletFragment, switchWalletFragment.g0());
                return;
            }
            SwitchWalletFragment.this.b0();
        }
    }

    /* compiled from: SwitchWalletFragment.kt */
    /* loaded from: classes2.dex */
    public static final class c implements qm1.a {
        public c() {
        }

        @Override // defpackage.qm1.a
        public void a() {
            SwitchWalletFragment.this.b0();
        }

        @Override // defpackage.qm1.a
        public void b(int i) {
        }
    }

    /* compiled from: SwitchWalletFragment.kt */
    /* loaded from: classes2.dex */
    public static final class d extends WalletRecyclerItemSwipeHelper {
        /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
        public d(Context context, RecyclerView recyclerView, lc2 lc2Var, SwitchWalletFragment$setupItemActions$2 switchWalletFragment$setupItemActions$2) {
            super(context, switchWalletFragment$setupItemActions$2, recyclerView, lc2Var);
            fs1.e(context, "requireContext()");
            fs1.e(recyclerView, "!!");
        }

        @Override // net.safemoon.androidwallet.views.gesture.WalletRecyclerItemSwipeHelper
        public void n(RecyclerView.a0 a0Var, List<WalletRecyclerItemSwipeHelper.c> list) {
            fs1.d(list);
            list.add(new WalletRecyclerItemSwipeHelper.c(this, g83.f(SwitchWalletFragment.this.getResources(), R.drawable.ic_baseline_delete_24, null), new ColorDrawable(g83.d(SwitchWalletFragment.this.getResources(), R.color.red, null))));
        }
    }

    /* compiled from: SwitchWalletFragment.kt */
    /* loaded from: classes2.dex */
    public static final class e implements qm1.a {
        public final /* synthetic */ Wallet b;

        public e(Wallet wallet2) {
            this.b = wallet2;
        }

        @Override // defpackage.qm1.a
        public void a() {
            SwitchWalletFragment.this.x0(this.b);
        }

        @Override // defpackage.qm1.a
        public void b(int i) {
        }
    }

    public SwitchWalletFragment() {
        RequestSocketState requestSocketState = RequestSocketState.NONE;
        this.m0 = new ArrayList();
        this.o0 = new ArrayList();
        this.p0 = new LinkedHashMap();
        this.s0 = FragmentViewModelLazyKt.a(this, d53.b(AKTWebSocketHandlingViewModel.class), new SwitchWalletFragment$special$$inlined$viewModels$default$6(new SwitchWalletFragment$special$$inlined$viewModels$default$5(this)), null);
        this.t0 = zy1.a(new SwitchWalletFragment$isBioAuth$2(this));
        this.u0 = new w41();
    }

    public static final void k0(SwitchWalletFragment switchWalletFragment, String str) {
        fs1.f(switchWalletFragment, "this$0");
        if (str == null) {
            return;
        }
        switchWalletFragment.w0(str);
        switchWalletFragment.W();
    }

    public static final void o0(final SwitchWalletFragment switchWalletFragment, List list) {
        fs1.f(switchWalletFragment, "this$0");
        FragmentActivity activity = switchWalletFragment.getActivity();
        if (activity == null) {
            return;
        }
        lc2 lc2Var = 0;
        if (!do3.a.c(activity) && list.size() != 1) {
            lc2 lc2Var2 = switchWalletFragment.r0;
            if (lc2Var2 == null) {
                fs1.r("myAdapter");
            } else {
                lc2Var = lc2Var2;
            }
            fs1.e(list, "it");
            ArrayList arrayList = new ArrayList();
            for (Object obj : list) {
                if (!((Wallet) obj).isPrimaryWallet()) {
                    arrayList.add(obj);
                }
            }
            lc2Var.g(arrayList);
        } else {
            lc2 lc2Var3 = switchWalletFragment.r0;
            if (lc2Var3 == null) {
                fs1.r("myAdapter");
            } else {
                lc2Var = lc2Var3;
            }
            fs1.e(list, "it");
            lc2Var.g(list);
        }
        eb1 eb1Var = switchWalletFragment.k0;
        if (eb1Var == null) {
            return;
        }
        do3 do3Var = do3.a;
        if (!do3Var.c(activity) && list.size() > 1) {
            eb1Var.c.setVisibility(0);
            if (do3Var.d(activity)) {
                eb1Var.b.setImageResource(R.drawable.ic_wallets_link_all);
                eb1Var.g.setText(switchWalletFragment.getString(R.string.akt_switch_wallet_unlink_all_wallets));
            } else {
                eb1Var.b.setImageResource(R.drawable.ic_wallets_unlink_all);
                eb1Var.g.setText(switchWalletFragment.getString(R.string.akt_switch_wallet_link_all_wallets));
            }
            Iterator it = list.iterator();
            while (it.hasNext()) {
                final Wallet wallet2 = (Wallet) it.next();
                if (wallet2.isPrimaryWallet()) {
                    eb1Var.b.setOnClickListener(new View.OnClickListener() { // from class: i24
                        @Override // android.view.View.OnClickListener
                        public final void onClick(View view) {
                            SwitchWalletFragment.p0(SwitchWalletFragment.this, wallet2, view);
                        }
                    });
                    eb1Var.g.setOnClickListener(new View.OnClickListener() { // from class: h24
                        @Override // android.view.View.OnClickListener
                        public final void onClick(View view) {
                            SwitchWalletFragment.q0(SwitchWalletFragment.this, wallet2, view);
                        }
                    });
                    return;
                }
            }
            throw new NoSuchElementException("Collection contains no element matching the predicate.");
        }
        eb1Var.c.setVisibility(8);
    }

    public static final void p0(SwitchWalletFragment switchWalletFragment, Wallet wallet2, View view) {
        fs1.f(switchWalletFragment, "this$0");
        fs1.f(wallet2, "$masterWallet");
        switchWalletFragment.F0(wallet2);
    }

    public static final void q0(SwitchWalletFragment switchWalletFragment, Wallet wallet2, View view) {
        fs1.f(switchWalletFragment, "this$0");
        fs1.f(wallet2, "$masterWallet");
        switchWalletFragment.F0(wallet2);
    }

    public static final void r0(SwitchWalletFragment switchWalletFragment, View view) {
        fs1.f(switchWalletFragment, "this$0");
        switchWalletFragment.requireActivity().onBackPressed();
    }

    public static final void s0(SwitchWalletFragment switchWalletFragment, View view) {
        fs1.f(switchWalletFragment, "this$0");
        switchWalletFragment.startActivity(new Intent(switchWalletFragment.requireActivity(), MainActivity.class));
    }

    public static final void t0(SwitchWalletFragment switchWalletFragment, View view) {
        fs1.f(switchWalletFragment, "this$0");
        switchWalletFragment.D0();
    }

    public static final void u0(SwitchWalletFragment switchWalletFragment, View view) {
        fs1.f(switchWalletFragment, "this$0");
        switchWalletFragment.C0();
    }

    public static final void v0(eb1 eb1Var, SwitchWalletFragment switchWalletFragment) {
        fs1.f(eb1Var, "$this_apply");
        fs1.f(switchWalletFragment, "this$0");
        eb1Var.e.setRefreshing(false);
        switchWalletFragment.C0();
    }

    public final void A0() {
        Context requireContext = requireContext();
        fs1.e(requireContext, "requireContext()");
        FragmentActivity requireActivity = requireActivity();
        fs1.e(requireActivity, "requireActivity()");
        lc2 lc2Var = new lc2(requireContext, e30.c(requireActivity), new SwitchWalletFragment$setupAdapter$1(this), new SwitchWalletFragment$setupAdapter$2(this), new SwitchWalletFragment$setupAdapter$3(this));
        this.r0 = lc2Var;
        eb1 eb1Var = this.k0;
        RecyclerView recyclerView = eb1Var == null ? null : eb1Var.d;
        if (recyclerView != null) {
            recyclerView.setAdapter(lc2Var);
        }
        B0();
    }

    /* JADX WARN: Type inference failed for: r5v0, types: [net.safemoon.androidwallet.fragments.SwitchWalletFragment$setupItemActions$2] */
    public final void B0() {
        lc2 lc2Var;
        Context requireContext = requireContext();
        eb1 eb1Var = this.k0;
        RecyclerView recyclerView = eb1Var == null ? null : eb1Var.d;
        fs1.d(recyclerView);
        lc2 lc2Var2 = this.r0;
        if (lc2Var2 == null) {
            fs1.r("myAdapter");
            lc2Var = null;
        } else {
            lc2Var = lc2Var2;
        }
        new d(requireContext, recyclerView, lc2Var, new bt1() { // from class: net.safemoon.androidwallet.fragments.SwitchWalletFragment$setupItemActions$2
            @Override // defpackage.bt1
            public void a() {
                lc2 lc2Var3;
                MultiWalletViewModel e0;
                lc2 lc2Var4;
                lc2Var3 = SwitchWalletFragment.this.r0;
                if (lc2Var3 != null) {
                    e0 = SwitchWalletFragment.this.e0();
                    lc2Var4 = SwitchWalletFragment.this.r0;
                    if (lc2Var4 == null) {
                        fs1.r("myAdapter");
                        lc2Var4 = null;
                    }
                    e0.F(lc2Var4.a());
                }
            }

            @Override // defpackage.bt1
            public void b(int i) {
                lc2 lc2Var3;
                lc2Var3 = SwitchWalletFragment.this.r0;
                if (lc2Var3 == null) {
                    fs1.r("myAdapter");
                    lc2Var3 = null;
                }
                Wallet wallet2 = lc2Var3.a().get(i);
                SwitchWalletFragment switchWalletFragment = SwitchWalletFragment.this;
                bh.P(new WeakReference(switchWalletFragment.requireActivity()), (r23 & 2) != 0 ? null : Integer.valueOf((int) R.string.delete_wallet_title), (r23 & 4) != 0 ? null : Integer.valueOf((int) R.string.delete_wallet_msg), (r23 & 8) != 0 ? null : null, (r23 & 16) != 0 ? Integer.valueOf((int) R.string.action_ok) : Integer.valueOf((int) R.string.confirm), (r23 & 32) != 0 ? Integer.valueOf((int) R.string.cancel) : null, (r23 & 64) != 0 ? null : null, (r23 & 128) != 0 ? null : null, new SwitchWalletFragment$setupItemActions$2$onItemRemoved$1$1(switchWalletFragment, wallet2), SwitchWalletFragment$setupItemActions$2$onItemRemoved$1$2.INSTANCE);
            }
        });
    }

    public final void C0() {
        bh.J(new WeakReference(requireActivity()), R.string.akt_switch_wallet_get_wallets_notice_title, R.string.akt_switch_wallet_get_wallets_notice_content, R.string.confirm, R.string.cancel, new SwitchWalletFragment$showGetWalletsNoticeDialog$1(this), SwitchWalletFragment$showGetWalletsNoticeDialog$2.INSTANCE);
    }

    public final void D0() {
        bh.X(new WeakReference(getActivity()), Integer.valueOf((int) R.string.akt_manage_wallets_link_wallet_as_companions_dialog_header), Integer.valueOf((int) R.string.akt_manage_wallets_link_wallet_as_companions_dialog_content), R.string.acknowledgment_confirm_button_text, SwitchWalletFragment$showSafeMoonCompanionsInfoDialog$1.INSTANCE);
    }

    public final void E0() {
        if (bo3.d(requireContext(), "AUTH_2FA_IS_ENABLE")) {
            G2FAVerfication a2 = G2FAVerfication.C0.a(d0(), l0());
            FragmentManager childFragmentManager = getChildFragmentManager();
            fs1.e(childFragmentManager, "childFragmentManager");
            a2.H(childFragmentManager);
        } else if (l0()) {
            this.u0.a(this, g0());
        } else {
            b0();
        }
    }

    public final void F0(Wallet wallet2) {
        if (bo3.d(requireContext(), "AUTH_2FA_IS_ENABLE")) {
            G2FAVerfication a2 = G2FAVerfication.C0.a(c0(wallet2), l0());
            FragmentManager childFragmentManager = getChildFragmentManager();
            fs1.e(childFragmentManager, "childFragmentManager");
            a2.H(childFragmentManager);
        } else if (l0()) {
            this.u0.a(this, G0(wallet2));
        } else {
            x0(wallet2);
        }
    }

    public final qm1.a G0(Wallet wallet2) {
        return new e(wallet2);
    }

    public final void H0() {
        do3 do3Var = do3.a;
        Context requireContext = requireContext();
        fs1.e(requireContext, "requireContext()");
        f0().p(new SwitchWalletFragment$updateAllWalletsLinkedStatus$1(this, do3Var.d(requireContext) ? 1 : 0));
    }

    /* JADX WARN: Multi-variable type inference failed */
    public final void U(String str) {
        boolean z;
        String D;
        List w0 = StringsKt__StringsKt.w0(str, new String[]{","}, false, 0, 6, null);
        if (w0.size() >= 3) {
            String e2 = w.e(getActivity(), (String) w0.get(2));
            String str2 = null;
            if (e2 != null && (D = dv3.D(e2, "|", MinimalPrettyPrinter.DEFAULT_ROOT_VALUE_SEPARATOR, false, 4, null)) != null) {
                str2 = StringsKt__StringsKt.K0(D).toString();
            }
            if (str2 != null) {
                if (str2.length() > 0) {
                    z = true;
                    if (z && HDWallet.isValid(str2)) {
                        HDWallet hDWallet = new HDWallet(str2, "");
                        CoinType coinType = CoinType.ETHEREUM;
                        String addressForCoin = hDWallet.getAddressForCoin(coinType);
                        byte[] data = hDWallet.getKeyForCoin(coinType).data();
                        fs1.e(data, "secretPrivateKey.data()");
                        String f0 = e30.f0(data, false);
                        List<String> list = this.o0;
                        fs1.e(addressForCoin, "addressEth");
                        list.add(addressForCoin);
                        this.p0.put(addressForCoin, w0.get(0));
                        MultiWalletViewModel f02 = f0();
                        fs1.e(e2, "mnemonic");
                        f02.m(f0, addressForCoin, e2, (r17 & 8) != 0 ? null : (String) w0.get(0), (r17 & 16) != 0, (r17 & 32) != 0 ? false : false, new SwitchWalletFragment$addWalletToDatabase$1(this));
                        return;
                    }
                }
            }
            z = false;
            if (z) {
                HDWallet hDWallet2 = new HDWallet(str2, "");
                CoinType coinType2 = CoinType.ETHEREUM;
                String addressForCoin2 = hDWallet2.getAddressForCoin(coinType2);
                byte[] data2 = hDWallet2.getKeyForCoin(coinType2).data();
                fs1.e(data2, "secretPrivateKey.data()");
                String f03 = e30.f0(data2, false);
                List<String> list2 = this.o0;
                fs1.e(addressForCoin2, "addressEth");
                list2.add(addressForCoin2);
                this.p0.put(addressForCoin2, w0.get(0));
                MultiWalletViewModel f022 = f0();
                fs1.e(e2, "mnemonic");
                f022.m(f03, addressForCoin2, e2, (r17 & 8) != 0 ? null : (String) w0.get(0), (r17 & 16) != 0, (r17 & 32) != 0 ? false : false, new SwitchWalletFragment$addWalletToDatabase$1(this));
                return;
            }
        }
        if (w0.size() >= 2) {
            String str3 = (String) w0.get(1);
            ma0 create = ma0.create(str3);
            List<String> list3 = this.o0;
            String address = create.getAddress();
            fs1.e(address, "credential.address");
            list3.add(address);
            Map<String, String> map = this.p0;
            String address2 = create.getAddress();
            fs1.e(address2, "credential.address");
            map.put(address2, w0.get(0));
            MultiWalletViewModel f04 = f0();
            String address3 = create.getAddress();
            fs1.e(address3, "credential.address");
            f04.m(str3, address3, "", (r17 & 8) != 0 ? null : (String) w0.get(0), (r17 & 16) != 0, (r17 & 32) != 0 ? false : false, new SwitchWalletFragment$addWalletToDatabase$2(this));
            return;
        }
        int i = this.n0 + 1;
        this.n0 = i;
        if (i >= this.m0.size()) {
            a0();
        } else {
            U(this.m0.get(this.n0));
        }
    }

    public final void V() {
        f0().p(new SwitchWalletFragment$checkLinkedAllStatus$1(this));
    }

    public final void W() {
        h0().f();
    }

    public final void X(Wallet wallet2) {
        bh.J(new WeakReference(requireActivity()), R.string.akt_switch_wallet_unlink_all_wallets, R.string.akt_manage_wallets_unlink_all_wallets_content, R.string.confirm, R.string.cancel, new SwitchWalletFragment$confirmUnlinkAllWallets$1(this, wallet2), SwitchWalletFragment$confirmUnlinkAllWallets$2.INSTANCE);
    }

    public final String Y(String str) {
        String i = bo3.i(getActivity(), "U5");
        String Z = AKTServerFunctions.Z(getActivity(), ((Object) i) + '>' + str);
        fs1.e(Z, "putBlob(activity, newBlob)");
        return Z;
    }

    public final void Z(Wallet wallet2) {
        e0().o(wallet2);
        if (wallet2.isLinked()) {
            n0(wallet2);
        } else {
            V();
        }
    }

    public final void a0() {
        f0().p(new SwitchWalletFragment$finishHandleBlob$1(this));
        ProgressLoading progressLoading = this.l0;
        if (progressLoading == null) {
            return;
        }
        progressLoading.h();
    }

    public final void b0() {
        ProgressLoading progressLoading = this.l0;
        if (((progressLoading == null || progressLoading.isResumed()) ? false : true) || this.l0 == null) {
            ProgressLoading.a aVar = ProgressLoading.y0;
            String string = getString(R.string.loading);
            fs1.e(string, "getString(R.string.loading)");
            ProgressLoading a2 = aVar.a(false, string, "");
            this.l0 = a2;
            if (a2 != null) {
                FragmentManager childFragmentManager = getChildFragmentManager();
                fs1.e(childFragmentManager, "childFragmentManager");
                a2.A(childFragmentManager);
            }
        }
        RequestSocketState requestSocketState = RequestSocketState.GET_BLOB;
        z0(AKTServerFunctions.T(getActivity()));
    }

    public final G2FAVerfication.b c0(Wallet wallet2) {
        return new a(wallet2);
    }

    public final G2FAVerfication.b d0() {
        return new b();
    }

    public final MultiWalletViewModel e0() {
        return (MultiWalletViewModel) this.i0.getValue();
    }

    public final MultiWalletViewModel f0() {
        return (MultiWalletViewModel) this.j0.getValue();
    }

    public final qm1.a g0() {
        return new c();
    }

    public final AKTWebSocketHandlingViewModel h0() {
        return (AKTWebSocketHandlingViewModel) this.s0.getValue();
    }

    public final void i0(String str) {
        this.m0.clear();
        this.o0.clear();
        this.p0.clear();
        this.m0.addAll(StringsKt__StringsKt.w0(str, new String[]{"|"}, false, 0, 6, null));
        this.n0 = 0;
        if (!this.m0.isEmpty()) {
            U(this.m0.get(this.n0));
        }
    }

    public final void j0() {
        h0().h().observe(this, new tl2() { // from class: b24
            @Override // defpackage.tl2
            public final void onChanged(Object obj) {
                SwitchWalletFragment.k0(SwitchWalletFragment.this, (String) obj);
            }
        });
    }

    public final boolean l0() {
        return ((Boolean) this.t0.getValue()).booleanValue();
    }

    public final boolean m0(String str) {
        String upperCase;
        if (str == null) {
            upperCase = null;
        } else {
            Locale locale = Locale.ROOT;
            fs1.e(locale, "ROOT");
            upperCase = str.toUpperCase(locale);
            fs1.e(upperCase, "(this as java.lang.String).toUpperCase(locale)");
        }
        boolean b2 = fs1.b(upperCase, "CONNECTION_REFUSED");
        if (b2) {
            ProgressLoading progressLoading = this.l0;
            if (progressLoading != null) {
                progressLoading.h();
            }
            W();
            FragmentActivity activity = getActivity();
            BasicActivity basicActivity = activity instanceof BasicActivity ? (BasicActivity) activity : null;
            if (basicActivity != null) {
                basicActivity.J(false);
            }
        }
        return b2;
    }

    public final void n0(Wallet wallet2) {
        this.q0 = wallet2;
        ProgressLoading.a aVar = ProgressLoading.y0;
        String string = getString(R.string.loading);
        fs1.e(string, "getString(R.string.loading)");
        ProgressLoading a2 = aVar.a(false, string, "");
        this.l0 = a2;
        if (a2 != null) {
            FragmentManager childFragmentManager = getChildFragmentManager();
            fs1.e(childFragmentManager, "childFragmentManager");
            a2.A(childFragmentManager);
        }
        f0().p(new SwitchWalletFragment$makeBlobFromWallets$1(wallet2, this, new StringBuilder()));
    }

    @Override // androidx.fragment.app.Fragment
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        getArguments();
        ActionBar supportActionBar = ((AppCompatActivity) requireActivity()).getSupportActionBar();
        if (supportActionBar != null) {
            supportActionBar.l();
        }
        h0().l();
        j0();
    }

    @Override // androidx.fragment.app.Fragment
    public View onCreateView(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle) {
        fs1.f(layoutInflater, "inflater");
        eb1 a2 = eb1.a(layoutInflater.inflate(R.layout.fragment_switch_wallet, viewGroup, false));
        this.k0 = a2;
        if (a2 == null) {
            return null;
        }
        return a2.b();
    }

    @Override // androidx.fragment.app.Fragment
    public void onResume() {
        super.onResume();
        V();
    }

    @Override // net.safemoon.androidwallet.fragments.common.BaseMainFragment, defpackage.qn, androidx.fragment.app.Fragment
    public void onViewCreated(View view, Bundle bundle) {
        fs1.f(view, "view");
        super.onViewCreated(view, bundle);
        final eb1 eb1Var = this.k0;
        if (eb1Var != null) {
            eb1Var.d.setLayoutManager(new LinearLayoutManager(requireActivity(), 1, false));
            h74 h74Var = eb1Var.f;
            h74Var.c.setOnClickListener(new View.OnClickListener() { // from class: d24
                @Override // android.view.View.OnClickListener
                public final void onClick(View view2) {
                    SwitchWalletFragment.r0(SwitchWalletFragment.this, view2);
                }
            });
            h74Var.e.setText(R.string.switch_wallet_screen_title);
            h74Var.b.setOnClickListener(new View.OnClickListener() { // from class: g24
                @Override // android.view.View.OnClickListener
                public final void onClick(View view2) {
                    SwitchWalletFragment.s0(SwitchWalletFragment.this, view2);
                }
            });
            h74Var.a.setOnClickListener(new View.OnClickListener() { // from class: f24
                @Override // android.view.View.OnClickListener
                public final void onClick(View view2) {
                    SwitchWalletFragment.t0(SwitchWalletFragment.this, view2);
                }
            });
            h74Var.d.setOnClickListener(new View.OnClickListener() { // from class: e24
                @Override // android.view.View.OnClickListener
                public final void onClick(View view2) {
                    SwitchWalletFragment.u0(SwitchWalletFragment.this, view2);
                }
            });
            eb1Var.e.setOnRefreshListener(new SwipeRefreshLayout.j() { // from class: j24
                @Override // androidx.swiperefreshlayout.widget.SwipeRefreshLayout.j
                public final void onRefresh() {
                    SwitchWalletFragment.v0(eb1.this, this);
                }
            });
        }
        A0();
        eb1 eb1Var2 = this.k0;
        ConstraintLayout constraintLayout = eb1Var2 == null ? null : eb1Var2.c;
        if (constraintLayout != null) {
            constraintLayout.setVisibility(8);
        }
        e0().t().observe(requireActivity(), new qx2(new tl2() { // from class: c24
            @Override // defpackage.tl2
            public final void onChanged(Object obj) {
                SwitchWalletFragment.o0(SwitchWalletFragment.this, (List) obj);
            }
        }));
    }

    public final void w0(String str) {
        if (m0(str)) {
            return;
        }
        b30 b30Var = b30.a;
        Context requireContext = requireContext();
        fs1.e(requireContext, "requireContext()");
        String[] v = i2.v(b30Var.v(requireContext, str), "|");
        fs1.e(v, "parts");
        if (!(v.length == 0)) {
            String str2 = v[0];
            fs1.e(str2, "parts[0]");
            if (StringsKt__StringsKt.w0(str2, new String[]{"="}, false, 0, 6, null).size() >= 2) {
                String str3 = v[0];
                fs1.e(str3, "parts[0]");
                String str4 = (String) StringsKt__StringsKt.w0(str3, new String[]{"="}, false, 0, 6, null).get(1);
                Objects.requireNonNull(str4, "null cannot be cast to non-null type java.lang.String");
                String upperCase = str4.toUpperCase(Locale.ROOT);
                fs1.e(upperCase, "(this as java.lang.Strin….toUpperCase(Locale.ROOT)");
                W();
                if (fs1.b(upperCase, "PUTBLOB02")) {
                    ProgressLoading progressLoading = this.l0;
                    if (progressLoading != null) {
                        progressLoading.h();
                    }
                    Wallet wallet2 = this.q0;
                    if (wallet2 != null) {
                        if (wallet2.isPrimaryWallet()) {
                            do3 do3Var = do3.a;
                            Context requireContext2 = requireContext();
                            fs1.e(requireContext2, "requireContext()");
                            Context requireContext3 = requireContext();
                            fs1.e(requireContext3, "requireContext()");
                            do3Var.h(requireContext2, !do3Var.d(requireContext3));
                            H0();
                        } else {
                            wallet2.updateLinkedState(!wallet2.isLinked());
                            e0().C(wallet2, wallet2.getLinkedState(), new SwitchWalletFragment$parseMessage$1$1(this));
                        }
                    }
                } else if (fs1.b(upperCase, "GETBLOB02")) {
                    W();
                    if (v.length > 2) {
                        String str5 = v[1];
                        fs1.e(str5, "u5EncryptBlob");
                        if (StringsKt__StringsKt.M(str5, "BLOB=", false, 2, null) && StringsKt__StringsKt.M(str5, ">", false, 2, null)) {
                            Object[] array = StringsKt__StringsKt.w0(str5, new String[]{">"}, false, 0, 6, null).toArray(new String[0]);
                            Objects.requireNonNull(array, "null cannot be cast to non-null type kotlin.Array<T>");
                            String a2 = w.a(getActivity(), ((String[]) array)[1]);
                            fs1.e(a2, "decryptedBlob");
                            i0(a2);
                        } else {
                            ProgressLoading progressLoading2 = this.l0;
                            if (progressLoading2 != null) {
                                progressLoading2.h();
                            }
                        }
                    } else {
                        ProgressLoading progressLoading3 = this.l0;
                        if (progressLoading3 != null) {
                            progressLoading3.h();
                        }
                    }
                } else {
                    y0();
                }
            }
        }
        RequestSocketState requestSocketState = RequestSocketState.NONE;
    }

    public final void x0(Wallet wallet2) {
        do3 do3Var = do3.a;
        Context requireContext = requireContext();
        fs1.e(requireContext, "requireContext()");
        if (do3Var.d(requireContext) && wallet2.isPrimaryWallet()) {
            X(wallet2);
        } else {
            n0(wallet2);
        }
    }

    public final void y0() {
        ProgressLoading progressLoading = this.l0;
        if (progressLoading != null) {
            progressLoading.h();
        }
        bh.Y(new WeakReference(getActivity()), null, Integer.valueOf((int) R.string.akt_manage_wallets_link_wallet_error_message), 0, null, 24, null);
    }

    public final void z0(String str) {
        if (str == null) {
            return;
        }
        h0().k(str);
    }
}
