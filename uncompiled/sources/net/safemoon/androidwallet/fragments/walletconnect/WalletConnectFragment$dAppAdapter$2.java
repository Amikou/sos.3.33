package net.safemoon.androidwallet.fragments.walletconnect;

import android.content.Context;
import defpackage.bm4;
import kotlin.jvm.internal.Lambda;
import net.safemoon.androidwallet.model.walletConnect.RoomConnectedInfoAndWallet;

/* compiled from: WalletConnectFragment.kt */
/* loaded from: classes2.dex */
public final class WalletConnectFragment$dAppAdapter$2 extends Lambda implements rc1<gd0> {
    public final /* synthetic */ WalletConnectFragment this$0;

    /* compiled from: WalletConnectFragment.kt */
    /* renamed from: net.safemoon.androidwallet.fragments.walletconnect.WalletConnectFragment$dAppAdapter$2$1  reason: invalid class name */
    /* loaded from: classes2.dex */
    public static final class AnonymousClass1 extends Lambda implements tc1<RoomConnectedInfoAndWallet, te4> {
        public final /* synthetic */ WalletConnectFragment this$0;

        /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
        public AnonymousClass1(WalletConnectFragment walletConnectFragment) {
            super(1);
            this.this$0 = walletConnectFragment;
        }

        @Override // defpackage.tc1
        public /* bridge */ /* synthetic */ te4 invoke(RoomConnectedInfoAndWallet roomConnectedInfoAndWallet) {
            invoke2(roomConnectedInfoAndWallet);
            return te4.a;
        }

        /* renamed from: invoke  reason: avoid collision after fix types in other method */
        public final void invoke2(RoomConnectedInfoAndWallet roomConnectedInfoAndWallet) {
            fs1.f(roomConnectedInfoAndWallet, "it");
            WalletConnectFragment walletConnectFragment = this.this$0;
            bm4.b a = bm4.a(roomConnectedInfoAndWallet.getDApp());
            fs1.e(a, "actionToWalletConnectDetailFragment(it.dApp)");
            walletConnectFragment.h(a, true);
        }
    }

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public WalletConnectFragment$dAppAdapter$2(WalletConnectFragment walletConnectFragment) {
        super(0);
        this.this$0 = walletConnectFragment;
    }

    /* JADX WARN: Can't rename method to resolve collision */
    @Override // defpackage.rc1
    public final gd0 invoke() {
        Context applicationContext = this.this$0.requireActivity().getApplicationContext();
        fs1.e(applicationContext, "requireActivity().applicationContext");
        return new gd0(applicationContext, new AnonymousClass1(this.this$0));
    }
}
