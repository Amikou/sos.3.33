package net.safemoon.androidwallet.fragments.walletconnect;

import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import androidx.appcompat.widget.AppCompatImageView;
import androidx.fragment.app.FragmentViewModelLazyKt;
import com.trustwallet.walletconnect.models.WCPeerMeta;
import java.lang.ref.WeakReference;
import net.safemoon.androidwallet.R;
import net.safemoon.androidwallet.common.TokenType;
import net.safemoon.androidwallet.fragments.common.BaseMainFragment;
import net.safemoon.androidwallet.fragments.walletconnect.WalletConnectDetailFragment;
import net.safemoon.androidwallet.model.walletConnect.RoomConnectedInfo;
import net.safemoon.androidwallet.model.walletConnect.RoomExtensionKt;
import net.safemoon.androidwallet.model.wallets.Wallet;
import net.safemoon.androidwallet.viewmodels.WalletConnectViewModel;

/* compiled from: WalletConnectDetailFragment.kt */
/* loaded from: classes2.dex */
public final class WalletConnectDetailFragment extends BaseMainFragment {
    public final sy1 i0;
    public final sy1 j0;
    public rb1 k0;

    /* compiled from: WalletConnectDetailFragment.kt */
    /* loaded from: classes2.dex */
    public static final class a {
        public a() {
        }

        public /* synthetic */ a(qi0 qi0Var) {
            this();
        }
    }

    static {
        new a(null);
    }

    public WalletConnectDetailFragment() {
        sy1 a2 = zy1.a(new WalletConnectDetailFragment$special$$inlined$navGraphViewModels$default$1(this, R.id.mobile_navigation));
        this.i0 = FragmentViewModelLazyKt.a(this, d53.b(WalletConnectViewModel.class), new WalletConnectDetailFragment$special$$inlined$navGraphViewModels$default$2(a2, null), new WalletConnectDetailFragment$special$$inlined$navGraphViewModels$default$3(null, a2, null));
        this.j0 = zy1.a(new WalletConnectDetailFragment$roomConnectedInfo$2(this));
    }

    public static final void u(WalletConnectDetailFragment walletConnectDetailFragment, RoomConnectedInfo roomConnectedInfo, View view) {
        fs1.f(walletConnectDetailFragment, "this$0");
        fs1.f(roomConnectedInfo, "$rc");
        bh.P(new WeakReference(walletConnectDetailFragment.requireActivity()), (r23 & 2) != 0 ? null : Integer.valueOf((int) R.string.connection_title_disconnect), (r23 & 4) != 0 ? null : Integer.valueOf((int) R.string.connection_message_disconnect), (r23 & 8) != 0 ? null : null, (r23 & 16) != 0 ? Integer.valueOf((int) R.string.action_ok) : Integer.valueOf((int) R.string.confirm), (r23 & 32) != 0 ? Integer.valueOf((int) R.string.cancel) : Integer.valueOf((int) R.string.cancel), (r23 & 64) != 0 ? null : null, (r23 & 128) != 0 ? null : null, new WalletConnectDetailFragment$initView$2$1$3$2$1(walletConnectDetailFragment, roomConnectedInfo), WalletConnectDetailFragment$initView$2$1$3$2$2.INSTANCE);
    }

    public static final void v(WalletConnectDetailFragment walletConnectDetailFragment, View view) {
        fs1.f(walletConnectDetailFragment, "this$0");
        walletConnectDetailFragment.f();
    }

    @Override // androidx.fragment.app.Fragment
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        getArguments();
    }

    @Override // androidx.fragment.app.Fragment
    public View onCreateView(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle) {
        fs1.f(layoutInflater, "inflater");
        return layoutInflater.inflate(R.layout.fragment_wallet_connect_detail, viewGroup, false);
    }

    @Override // net.safemoon.androidwallet.fragments.common.BaseMainFragment, defpackage.qn, androidx.fragment.app.Fragment
    public void onViewCreated(View view, Bundle bundle) {
        fs1.f(view, "view");
        super.onViewCreated(view, bundle);
        this.k0 = rb1.a(view);
        t();
    }

    public final RoomConnectedInfo s() {
        return (RoomConnectedInfo) this.j0.getValue();
    }

    public final void t() {
        WCPeerMeta peerMeta;
        uk1 uk1Var;
        rp3 rp3Var;
        rb1 rb1Var = this.k0;
        if (rb1Var != null && (rp3Var = rb1Var.d) != null) {
            rp3Var.a.setOnClickListener(new View.OnClickListener() { // from class: tl4
                @Override // android.view.View.OnClickListener
                public final void onClick(View view) {
                    WalletConnectDetailFragment.v(WalletConnectDetailFragment.this, view);
                }
            });
            rp3Var.c.setText(getText(R.string.connection_screen_title));
        }
        rb1 rb1Var2 = this.k0;
        AppCompatImageView appCompatImageView = null;
        if (rb1Var2 != null && (uk1Var = rb1Var2.c) != null) {
            appCompatImageView = uk1Var.c;
        }
        if (appCompatImageView != null) {
            appCompatImageView.setVisibility(8);
        }
        final RoomConnectedInfo s = s();
        if (s == null || (peerMeta = RoomExtensionKt.toPeerMeta(s.getPeerMeta())) == null) {
            return;
        }
        rb1 rb1Var3 = this.k0;
        if (rb1Var3 != null) {
            TokenType b = TokenType.Companion.b(s.getChainId());
            if (b != null) {
                TextView textView = rb1Var3.f;
                textView.setText(b.getTitle() + " (" + b.getNativeToken() + ')');
                com.bumptech.glide.a.u(rb1Var3.b).w(Integer.valueOf(b.getIcon())).I0(rb1Var3.b);
            }
            uk1 uk1Var2 = rb1Var3.c;
            if (uk1Var2 != null) {
                if (!peerMeta.getIcons().isEmpty()) {
                    com.bumptech.glide.a.u(uk1Var2.d).y(peerMeta.getIcons().get(0)).d0(200, 200).a(n73.v0()).I0(uk1Var2.d);
                }
                uk1Var2.f.setText(peerMeta.getName());
                uk1Var2.e.setText(peerMeta.getUrl());
            }
        }
        rb1 rb1Var4 = this.k0;
        if (rb1Var4 == null) {
            return;
        }
        rb1Var4.e.setText(b30.a.j(s.getConnectedAtUnix() * 1000));
        Context requireContext = requireContext();
        fs1.e(requireContext, "requireContext()");
        Wallet c = e30.c(requireContext);
        if (c != null) {
            rb1Var4.h.setText(c.displayName());
            rb1Var4.g.setText(c.getAddress());
        }
        rb1Var4.a.setOnClickListener(new View.OnClickListener() { // from class: ul4
            @Override // android.view.View.OnClickListener
            public final void onClick(View view) {
                WalletConnectDetailFragment.u(WalletConnectDetailFragment.this, s, view);
            }
        });
    }
}
