package net.safemoon.androidwallet.fragments.walletconnect;

import android.content.Intent;
import java.io.Serializable;
import kotlin.jvm.internal.Lambda;
import net.safemoon.androidwallet.common.TokenType;
import net.safemoon.androidwallet.viewmodels.WalletConnectViewModel;

/* compiled from: WalletConnectFragment.kt */
/* loaded from: classes2.dex */
public final class WalletConnectFragment$onSelectNetwork$1 extends Lambda implements tc1<Intent, te4> {
    public final /* synthetic */ WalletConnectFragment this$0;

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public WalletConnectFragment$onSelectNetwork$1(WalletConnectFragment walletConnectFragment) {
        super(1);
        this.this$0 = walletConnectFragment;
    }

    @Override // defpackage.tc1
    public /* bridge */ /* synthetic */ te4 invoke(Intent intent) {
        invoke2(intent);
        return te4.a;
    }

    /* renamed from: invoke  reason: avoid collision after fix types in other method */
    public final void invoke2(Intent intent) {
        WalletConnectViewModel E;
        if (intent == null) {
            return;
        }
        WalletConnectFragment walletConnectFragment = this.this$0;
        Serializable serializableExtra = intent.getSerializableExtra("ARG_SELECTED_TOKEN_NETWORK");
        if (serializableExtra instanceof TokenType) {
            E = walletConnectFragment.E();
            E.e().postValue(serializableExtra);
        }
    }
}
