package net.safemoon.androidwallet.fragments.walletconnect;

import android.content.Context;
import android.content.Intent;
import kotlin.jvm.internal.Lambda;
import net.safemoon.androidwallet.R;
import net.safemoon.androidwallet.fragments.walletconnect.WalletConnectFragment$walletConnect$2;

/* compiled from: WalletConnectFragment.kt */
/* loaded from: classes2.dex */
public final class WalletConnectFragment$initView$2$1$1 extends Lambda implements tc1<Intent, te4> {
    public final /* synthetic */ qb1 $this_apply;
    public final /* synthetic */ WalletConnectFragment this$0;

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public WalletConnectFragment$initView$2$1$1(qb1 qb1Var, WalletConnectFragment walletConnectFragment) {
        super(1);
        this.$this_apply = qb1Var;
        this.this$0 = walletConnectFragment;
    }

    @Override // defpackage.tc1
    public /* bridge */ /* synthetic */ te4 invoke(Intent intent) {
        invoke2(intent);
        return te4.a;
    }

    /* renamed from: invoke  reason: avoid collision after fix types in other method */
    public final void invoke2(Intent intent) {
        String stringExtra;
        WalletConnectFragment$walletConnect$2.AnonymousClass1 D;
        if (intent == null || (stringExtra = intent.getStringExtra("result")) == null) {
            return;
        }
        try {
            this.$this_apply.a.setEnabled(false);
            D = this.this$0.D();
            D.d(stringExtra);
        } catch (Exception unused) {
            Context requireContext = this.this$0.requireContext();
            fs1.e(requireContext, "requireContext()");
            String string = this.this$0.getString(R.string.err_invalid_wc);
            fs1.e(string, "getString(R.string.err_invalid_wc)");
            e30.a0(requireContext, string);
        }
    }
}
