package net.safemoon.androidwallet.fragments.walletconnect;

import kotlin.jvm.internal.Lambda;

/* compiled from: NavGraphViewModelLazy.kt */
/* loaded from: classes2.dex */
public final class WalletConnectDetailFragment$special$$inlined$navGraphViewModels$default$2 extends Lambda implements rc1<gj4> {
    public final /* synthetic */ sy1 $backStackEntry;
    public final /* synthetic */ yw1 $backStackEntry$metadata;

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public WalletConnectDetailFragment$special$$inlined$navGraphViewModels$default$2(sy1 sy1Var, yw1 yw1Var) {
        super(0);
        this.$backStackEntry = sy1Var;
        this.$backStackEntry$metadata = yw1Var;
    }

    /* JADX WARN: Can't rename method to resolve collision */
    @Override // defpackage.rc1
    public final gj4 invoke() {
        xd2 xd2Var = (xd2) this.$backStackEntry.getValue();
        fs1.c(xd2Var, "backStackEntry");
        gj4 viewModelStore = xd2Var.getViewModelStore();
        fs1.c(viewModelStore, "backStackEntry.viewModelStore");
        return viewModelStore;
    }
}
