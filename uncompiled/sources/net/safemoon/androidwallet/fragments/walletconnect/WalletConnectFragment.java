package net.safemoon.androidwallet.fragments.walletconnect;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import androidx.fragment.app.FragmentActivity;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentViewModelLazyKt;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import com.google.android.material.button.MaterialButton;
import com.trustwallet.walletconnect.WCClient;
import com.trustwallet.walletconnect.models.WCPeerMeta;
import java.lang.ref.WeakReference;
import java.util.List;
import net.safemoon.androidwallet.R;
import net.safemoon.androidwallet.activity.ChooseNetworkActivity;
import net.safemoon.androidwallet.activity.ScannedCodeActivity;
import net.safemoon.androidwallet.adapter.touchHelper.RecyclerTouchListener;
import net.safemoon.androidwallet.common.TokenType;
import net.safemoon.androidwallet.dialogs.WalletConnectInterfaceFragment;
import net.safemoon.androidwallet.fragments.common.BaseMainFragment;
import net.safemoon.androidwallet.fragments.walletconnect.WalletConnectFragment;
import net.safemoon.androidwallet.fragments.walletconnect.WalletConnectFragment$walletConnect$2;
import net.safemoon.androidwallet.model.walletConnect.RoomConnectedInfo;
import net.safemoon.androidwallet.model.walletConnect.RoomConnectedInfoAndWallet;
import net.safemoon.androidwallet.viewmodels.HomeViewModel;
import net.safemoon.androidwallet.viewmodels.WalletConnectViewModel;

/* compiled from: WalletConnectFragment.kt */
/* loaded from: classes2.dex */
public final class WalletConnectFragment extends BaseMainFragment implements WalletConnectInterfaceFragment.b {
    public RecyclerTouchListener l0;
    public qb1 m0;
    public WalletConnectInterfaceFragment o0;
    public final c90 i0 = d90.a(tp0.c());
    public final sy1 j0 = FragmentViewModelLazyKt.a(this, d53.b(WalletConnectViewModel.class), new WalletConnectFragment$special$$inlined$viewModels$default$2(new WalletConnectFragment$special$$inlined$viewModels$default$1(this)), null);
    public final sy1 k0 = FragmentViewModelLazyKt.a(this, d53.b(HomeViewModel.class), new WalletConnectFragment$special$$inlined$activityViewModels$default$1(this), new WalletConnectFragment$special$$inlined$activityViewModels$default$2(this));
    public final gb2<WCPeerMeta> n0 = new gb2<>();
    public final sy1 p0 = zy1.a(new WalletConnectFragment$walletConnect$2(this));
    public final sy1 q0 = zy1.a(new WalletConnectFragment$dAppAdapter$2(this));

    public static final void G(WalletConnectFragment walletConnectFragment, View view) {
        fs1.f(walletConnectFragment, "this$0");
        walletConnectFragment.f();
    }

    public static final void H(WalletConnectFragment walletConnectFragment, String str) {
        fs1.f(walletConnectFragment, "this$0");
        if (str != null) {
            try {
                walletConnectFragment.C().n().postValue(null);
                walletConnectFragment.D().d(str);
            } catch (Exception unused) {
            }
        }
    }

    public static final void I(WalletConnectFragment walletConnectFragment, WCPeerMeta wCPeerMeta) {
        fs1.f(walletConnectFragment, "this$0");
        WalletConnectFragment$walletConnect$2.AnonymousClass1 D = walletConnectFragment.D();
        if (wCPeerMeta == null) {
            return;
        }
        walletConnectFragment.n0.postValue(null);
        if (D.o().getRemotePeerId() == null) {
            System.out.println((Object) "remotePeerId can't be null");
            return;
        }
        String chainId = D.o().getChainId();
        if (chainId != null) {
            walletConnectFragment.E().e().postValue(TokenType.Companion.b((int) e30.K(chainId)));
        }
        WalletConnectInterfaceFragment.a aVar = WalletConnectInterfaceFragment.B0;
        WCClient o = D.o();
        WalletConnectInterfaceFragment a = aVar.a(wCPeerMeta, o == null ? null : o.getChainId());
        walletConnectFragment.o0 = a;
        if (a != null) {
            FragmentManager childFragmentManager = walletConnectFragment.getChildFragmentManager();
            fs1.e(childFragmentManager, "childFragmentManager");
            a.S(childFragmentManager);
        }
        qb1 qb1Var = walletConnectFragment.m0;
        MaterialButton materialButton = qb1Var != null ? qb1Var.a : null;
        if (materialButton == null) {
            return;
        }
        materialButton.setEnabled(true);
    }

    public static final void J(WalletConnectFragment walletConnectFragment, qb1 qb1Var, View view) {
        hn2 a;
        w7<Intent> b;
        fs1.f(walletConnectFragment, "this$0");
        fs1.f(qb1Var, "$this_apply");
        gm1 j = walletConnectFragment.j();
        if (j == null || (a = j.a()) == null || (b = a.b(new WalletConnectFragment$initView$2$1$1(qb1Var, walletConnectFragment))) == null) {
            return;
        }
        b.a(new Intent(walletConnectFragment.requireActivity(), ScannedCodeActivity.class));
    }

    public static final void K(WalletConnectFragment walletConnectFragment, int i, int i2) {
        RoomConnectedInfo dApp;
        fs1.f(walletConnectFragment, "this$0");
        RoomConnectedInfoAndWallet c = walletConnectFragment.B().c(i2);
        if (c == null || (dApp = c.getDApp()) == null || i != R.id.btnDelete) {
            return;
        }
        bh.P(new WeakReference(walletConnectFragment.requireActivity()), (r23 & 2) != 0 ? null : Integer.valueOf((int) R.string.connection_title_disconnect), (r23 & 4) != 0 ? null : Integer.valueOf((int) R.string.connection_message_disconnect), (r23 & 8) != 0 ? null : null, (r23 & 16) != 0 ? Integer.valueOf((int) R.string.action_ok) : Integer.valueOf((int) R.string.confirm), (r23 & 32) != 0 ? Integer.valueOf((int) R.string.cancel) : Integer.valueOf((int) R.string.cancel), (r23 & 64) != 0 ? null : null, (r23 & 128) != 0 ? null : null, new WalletConnectFragment$initView$2$2$1$1$1(walletConnectFragment, dApp), WalletConnectFragment$initView$2$2$1$1$2.INSTANCE);
    }

    public static final void L(WalletConnectFragment walletConnectFragment, List list) {
        fs1.f(walletConnectFragment, "this$0");
        List<RoomConnectedInfoAndWallet> I = list == null ? null : j20.I(list);
        gd0 B = walletConnectFragment.B();
        fs1.d(I);
        B.e(I);
    }

    public final gd0 B() {
        return (gd0) this.q0.getValue();
    }

    public final HomeViewModel C() {
        return (HomeViewModel) this.k0.getValue();
    }

    public final WalletConnectFragment$walletConnect$2.AnonymousClass1 D() {
        return (WalletConnectFragment$walletConnect$2.AnonymousClass1) this.p0.getValue();
    }

    public final WalletConnectViewModel E() {
        return (WalletConnectViewModel) this.j0.getValue();
    }

    public final void F() {
        rp3 rp3Var;
        qb1 qb1Var = this.m0;
        if (qb1Var != null && (rp3Var = qb1Var.c) != null) {
            rp3Var.a.setOnClickListener(new View.OnClickListener() { // from class: yl4
                @Override // android.view.View.OnClickListener
                public final void onClick(View view) {
                    WalletConnectFragment.G(WalletConnectFragment.this, view);
                }
            });
            rp3Var.c.setText(getText(R.string.wc_screen_title));
        }
        final qb1 qb1Var2 = this.m0;
        if (qb1Var2 != null) {
            qb1Var2.a.setOnClickListener(new View.OnClickListener() { // from class: zl4
                @Override // android.view.View.OnClickListener
                public final void onClick(View view) {
                    WalletConnectFragment.J(WalletConnectFragment.this, qb1Var2, view);
                }
            });
            qb1Var2.b.setLayoutManager(new LinearLayoutManager(requireContext(), 1, false));
            qb1Var2.b.setAdapter(B());
            FragmentActivity requireActivity = requireActivity();
            qb1 qb1Var3 = this.m0;
            RecyclerTouchListener recyclerTouchListener = new RecyclerTouchListener(requireActivity, qb1Var3 == null ? null : qb1Var3.b);
            recyclerTouchListener.x(Integer.valueOf((int) R.id.btnDelete));
            recyclerTouchListener.y(R.id.rowFG, R.id.rowBG, new RecyclerTouchListener.k() { // from class: am4
                @Override // net.safemoon.androidwallet.adapter.touchHelper.RecyclerTouchListener.k
                public final void a(int i, int i2) {
                    WalletConnectFragment.K(WalletConnectFragment.this, i, i2);
                }
            });
            te4 te4Var = te4.a;
            this.l0 = recyclerTouchListener;
            RecyclerView recyclerView = qb1Var2.b;
            if (recyclerView != null) {
                fs1.d(recyclerTouchListener);
                recyclerView.k(recyclerTouchListener);
            }
        }
        E().c().observe(getViewLifecycleOwner(), new tl2() { // from class: xl4
            @Override // defpackage.tl2
            public final void onChanged(Object obj) {
                WalletConnectFragment.L(WalletConnectFragment.this, (List) obj);
            }
        });
        C().n().observe(getViewLifecycleOwner(), new tl2() { // from class: wl4
            @Override // defpackage.tl2
            public final void onChanged(Object obj) {
                WalletConnectFragment.H(WalletConnectFragment.this, (String) obj);
            }
        });
        this.n0.observe(getViewLifecycleOwner(), new tl2() { // from class: vl4
            @Override // defpackage.tl2
            public final void onChanged(Object obj) {
                WalletConnectFragment.I(WalletConnectFragment.this, (WCPeerMeta) obj);
            }
        });
    }

    @Override // net.safemoon.androidwallet.dialogs.WalletConnectInterfaceFragment.b
    public void c(TokenType tokenType) {
        hn2 a;
        w7<Intent> b;
        fs1.f(tokenType, "tokenType");
        gm1 j = j();
        if (j == null || (a = j.a()) == null || (b = a.b(new WalletConnectFragment$onSelectNetwork$1(this))) == null) {
            return;
        }
        ChooseNetworkActivity.a aVar = ChooseNetworkActivity.l0;
        Context requireContext = requireContext();
        fs1.e(requireContext, "requireContext()");
        b.a(aVar.a(requireContext, tokenType));
    }

    @Override // net.safemoon.androidwallet.dialogs.WalletConnectInterfaceFragment.b
    public void d() {
        WalletConnectFragment$walletConnect$2.AnonymousClass1 D = D();
        TokenType value = E().e().getValue();
        Boolean value2 = E().g().getValue();
        if (value2 == null) {
            value2 = Boolean.FALSE;
        }
        RoomConnectedInfo c = D.c(value, value2.booleanValue());
        if (c == null) {
            return;
        }
        D().f();
        E().h(c);
        WalletConnectInterfaceFragment walletConnectInterfaceFragment = this.o0;
        if (walletConnectInterfaceFragment == null) {
            return;
        }
        walletConnectInterfaceFragment.i();
    }

    @Override // androidx.fragment.app.Fragment
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
    }

    @Override // androidx.fragment.app.Fragment
    public View onCreateView(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle) {
        fs1.f(layoutInflater, "inflater");
        return layoutInflater.inflate(R.layout.fragment_wallet_connect, viewGroup, false);
    }

    @Override // net.safemoon.androidwallet.fragments.common.BaseMainFragment, defpackage.qn, androidx.fragment.app.Fragment
    public void onViewCreated(View view, Bundle bundle) {
        fs1.f(view, "view");
        super.onViewCreated(view, bundle);
        this.m0 = qb1.a(view);
        F();
    }
}
