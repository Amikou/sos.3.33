package net.safemoon.androidwallet.fragments.walletconnect;

import androidx.lifecycle.l;
import kotlin.jvm.internal.Lambda;

/* compiled from: NavGraphViewModelLazy.kt */
/* loaded from: classes2.dex */
public final class WalletConnectDetailFragment$special$$inlined$navGraphViewModels$default$3 extends Lambda implements rc1<l.b> {
    public final /* synthetic */ sy1 $backStackEntry;
    public final /* synthetic */ yw1 $backStackEntry$metadata;
    public final /* synthetic */ rc1 $factoryProducer;

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public WalletConnectDetailFragment$special$$inlined$navGraphViewModels$default$3(rc1 rc1Var, sy1 sy1Var, yw1 yw1Var) {
        super(0);
        this.$factoryProducer = rc1Var;
        this.$backStackEntry = sy1Var;
        this.$backStackEntry$metadata = yw1Var;
    }

    /* JADX WARN: Can't rename method to resolve collision */
    @Override // defpackage.rc1
    public final l.b invoke() {
        l.b bVar;
        rc1 rc1Var = this.$factoryProducer;
        if (rc1Var == null || (bVar = (l.b) rc1Var.invoke()) == null) {
            xd2 xd2Var = (xd2) this.$backStackEntry.getValue();
            fs1.c(xd2Var, "backStackEntry");
            l.b defaultViewModelProviderFactory = xd2Var.getDefaultViewModelProviderFactory();
            fs1.c(defaultViewModelProviderFactory, "backStackEntry.defaultViewModelProviderFactory");
            return defaultViewModelProviderFactory;
        }
        return bVar;
    }
}
