package net.safemoon.androidwallet.fragments.walletconnect;

import com.trustwallet.walletconnect.models.WCPeerMeta;
import kotlin.coroutines.jvm.internal.SuspendLambda;
import kotlin.coroutines.jvm.internal.a;

/* compiled from: WalletConnectFragment.kt */
@a(c = "net.safemoon.androidwallet.fragments.walletconnect.WalletConnectFragment$walletConnect$2$1$onSessionRequest$1", f = "WalletConnectFragment.kt", l = {}, m = "invokeSuspend")
/* loaded from: classes2.dex */
public final class WalletConnectFragment$walletConnect$2$1$onSessionRequest$1 extends SuspendLambda implements hd1<c90, q70<? super te4>, Object> {
    public final /* synthetic */ WCPeerMeta $peer;
    public int label;
    public final /* synthetic */ WalletConnectFragment this$0;

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public WalletConnectFragment$walletConnect$2$1$onSessionRequest$1(WalletConnectFragment walletConnectFragment, WCPeerMeta wCPeerMeta, q70<? super WalletConnectFragment$walletConnect$2$1$onSessionRequest$1> q70Var) {
        super(2, q70Var);
        this.this$0 = walletConnectFragment;
        this.$peer = wCPeerMeta;
    }

    @Override // kotlin.coroutines.jvm.internal.BaseContinuationImpl
    public final q70<te4> create(Object obj, q70<?> q70Var) {
        return new WalletConnectFragment$walletConnect$2$1$onSessionRequest$1(this.this$0, this.$peer, q70Var);
    }

    @Override // defpackage.hd1
    public final Object invoke(c90 c90Var, q70<? super te4> q70Var) {
        return ((WalletConnectFragment$walletConnect$2$1$onSessionRequest$1) create(c90Var, q70Var)).invokeSuspend(te4.a);
    }

    @Override // kotlin.coroutines.jvm.internal.BaseContinuationImpl
    public final Object invokeSuspend(Object obj) {
        gb2 gb2Var;
        gs1.d();
        if (this.label == 0) {
            o83.b(obj);
            gb2Var = this.this$0.n0;
            gb2Var.setValue(this.$peer);
            return te4.a;
        }
        throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
    }
}
