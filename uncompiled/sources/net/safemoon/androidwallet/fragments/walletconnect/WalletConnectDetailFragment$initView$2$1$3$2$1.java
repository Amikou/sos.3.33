package net.safemoon.androidwallet.fragments.walletconnect;

import android.content.DialogInterface;
import kotlin.jvm.internal.Lambda;
import net.safemoon.androidwallet.model.walletConnect.RoomConnectedInfo;
import net.safemoon.androidwallet.viewmodels.wc.MultipleWalletConnect;

/* compiled from: WalletConnectDetailFragment.kt */
/* loaded from: classes2.dex */
public final class WalletConnectDetailFragment$initView$2$1$3$2$1 extends Lambda implements tc1<DialogInterface, te4> {
    public final /* synthetic */ RoomConnectedInfo $rc;
    public final /* synthetic */ WalletConnectDetailFragment this$0;

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public WalletConnectDetailFragment$initView$2$1$3$2$1(WalletConnectDetailFragment walletConnectDetailFragment, RoomConnectedInfo roomConnectedInfo) {
        super(1);
        this.this$0 = walletConnectDetailFragment;
        this.$rc = roomConnectedInfo;
    }

    @Override // defpackage.tc1
    public /* bridge */ /* synthetic */ te4 invoke(DialogInterface dialogInterface) {
        invoke2(dialogInterface);
        return te4.a;
    }

    /* renamed from: invoke  reason: avoid collision after fix types in other method */
    public final void invoke2(DialogInterface dialogInterface) {
        gm1 j;
        MultipleWalletConnect m;
        fs1.f(dialogInterface, "$noName_0");
        j = this.this$0.j();
        if (j != null && (m = j.m()) != null) {
            m.q(this.$rc);
        }
        this.this$0.f();
    }
}
