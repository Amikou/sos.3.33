package net.safemoon.androidwallet.fragments.walletconnect;

import androidx.fragment.app.Fragment;
import kotlin.jvm.internal.Lambda;

/* compiled from: NavGraphViewModelLazy.kt */
/* loaded from: classes2.dex */
public final class WalletConnectDetailFragment$special$$inlined$navGraphViewModels$default$1 extends Lambda implements rc1<xd2> {
    public final /* synthetic */ int $navGraphId;
    public final /* synthetic */ Fragment $this_navGraphViewModels;

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public WalletConnectDetailFragment$special$$inlined$navGraphViewModels$default$1(Fragment fragment, int i) {
        super(0);
        this.$this_navGraphViewModels = fragment;
        this.$navGraphId = i;
    }

    @Override // defpackage.rc1
    public final xd2 invoke() {
        return ka1.a(this.$this_navGraphViewModels).f(this.$navGraphId);
    }
}
