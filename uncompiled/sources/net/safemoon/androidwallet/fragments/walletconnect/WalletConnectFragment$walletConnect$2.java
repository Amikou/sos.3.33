package net.safemoon.androidwallet.fragments.walletconnect;

import androidx.fragment.app.FragmentActivity;
import com.trustwallet.walletconnect.models.WCPeerMeta;
import kotlin.jvm.internal.Lambda;
import net.safemoon.androidwallet.model.wallets.Wallet;

/* compiled from: WalletConnectFragment.kt */
/* loaded from: classes2.dex */
public final class WalletConnectFragment$walletConnect$2 extends Lambda implements rc1<AnonymousClass1> {
    public final /* synthetic */ WalletConnectFragment this$0;

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public WalletConnectFragment$walletConnect$2(WalletConnectFragment walletConnectFragment) {
        super(0);
        this.this$0 = walletConnectFragment;
    }

    /* JADX WARN: Type inference failed for: r2v0, types: [net.safemoon.androidwallet.fragments.walletconnect.WalletConnectFragment$walletConnect$2$1] */
    @Override // defpackage.rc1
    public final AnonymousClass1 invoke() {
        FragmentActivity requireActivity = this.this$0.requireActivity();
        FragmentActivity activity = this.this$0.getActivity();
        Wallet c = activity == null ? null : e30.c(activity);
        fs1.d(c);
        return new el4(requireActivity, c) { // from class: net.safemoon.androidwallet.fragments.walletconnect.WalletConnectFragment$walletConnect$2.1
            /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
            {
                super(requireActivity, c);
                fs1.e(requireActivity, "requireActivity()");
            }

            @Override // defpackage.el4, net.safemoon.androidwallet.viewmodels.wc.WalletConnect
            public void t(WCPeerMeta wCPeerMeta) {
                c90 c90Var;
                fs1.f(wCPeerMeta, "peer");
                c90Var = WalletConnectFragment.this.i0;
                as.b(c90Var, null, null, new WalletConnectFragment$walletConnect$2$1$onSessionRequest$1(WalletConnectFragment.this, wCPeerMeta, null), 3, null);
            }
        };
    }
}
