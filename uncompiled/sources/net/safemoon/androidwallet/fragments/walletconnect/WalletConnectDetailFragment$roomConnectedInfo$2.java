package net.safemoon.androidwallet.fragments.walletconnect;

import java.io.Serializable;
import java.util.Objects;
import kotlin.jvm.internal.Lambda;
import net.safemoon.androidwallet.model.walletConnect.RoomConnectedInfo;

/* compiled from: WalletConnectDetailFragment.kt */
/* loaded from: classes2.dex */
public final class WalletConnectDetailFragment$roomConnectedInfo$2 extends Lambda implements rc1<RoomConnectedInfo> {
    public final /* synthetic */ WalletConnectDetailFragment this$0;

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public WalletConnectDetailFragment$roomConnectedInfo$2(WalletConnectDetailFragment walletConnectDetailFragment) {
        super(0);
        this.this$0 = walletConnectDetailFragment;
    }

    @Override // defpackage.rc1
    public final RoomConnectedInfo invoke() {
        if (this.this$0.requireArguments().getSerializable("roomConnectedInfo") instanceof RoomConnectedInfo) {
            Serializable serializable = this.this$0.requireArguments().getSerializable("roomConnectedInfo");
            Objects.requireNonNull(serializable, "null cannot be cast to non-null type net.safemoon.androidwallet.model.walletConnect.RoomConnectedInfo");
            return (RoomConnectedInfo) serializable;
        }
        return null;
    }
}
