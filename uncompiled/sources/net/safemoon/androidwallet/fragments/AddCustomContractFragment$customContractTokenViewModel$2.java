package net.safemoon.androidwallet.fragments;

import androidx.lifecycle.l;
import java.lang.ref.WeakReference;
import kotlin.jvm.internal.Lambda;
import net.safemoon.androidwallet.viewmodels.factory.MyViewModelFactory;

/* compiled from: AddCustomContractFragment.kt */
/* loaded from: classes2.dex */
public final class AddCustomContractFragment$customContractTokenViewModel$2 extends Lambda implements rc1<l.b> {
    public final /* synthetic */ AddCustomContractFragment this$0;

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public AddCustomContractFragment$customContractTokenViewModel$2(AddCustomContractFragment addCustomContractFragment) {
        super(0);
        this.this$0 = addCustomContractFragment;
    }

    /* JADX WARN: Can't rename method to resolve collision */
    @Override // defpackage.rc1
    public final l.b invoke() {
        return new MyViewModelFactory(new WeakReference(this.this$0.requireActivity()));
    }
}
