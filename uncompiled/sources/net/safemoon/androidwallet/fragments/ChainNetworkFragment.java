package net.safemoon.androidwallet.fragments;

import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import net.safemoon.androidwallet.R;
import net.safemoon.androidwallet.fragments.ChainNetworkFragment;
import net.safemoon.androidwallet.fragments.common.BaseMainFragment;

/* compiled from: ChainNetworkFragment.kt */
/* loaded from: classes2.dex */
public final class ChainNetworkFragment extends BaseMainFragment {
    public s91 i0;
    public final sy1 j0 = zy1.a(new ChainNetworkFragment$chainNetworkAdapter$2(this));

    /* compiled from: ChainNetworkFragment.kt */
    /* loaded from: classes2.dex */
    public static final class a {
        public a() {
        }

        public /* synthetic */ a(qi0 qi0Var) {
            this();
        }
    }

    /* compiled from: ChainNetworkFragment.kt */
    /* loaded from: classes2.dex */
    public static final class b implements TextWatcher {
        public b() {
        }

        @Override // android.text.TextWatcher
        public void afterTextChanged(Editable editable) {
            ChainNetworkFragment.this.s().getFilter().filter(editable);
        }

        @Override // android.text.TextWatcher
        public void beforeTextChanged(CharSequence charSequence, int i, int i2, int i3) {
        }

        @Override // android.text.TextWatcher
        public void onTextChanged(CharSequence charSequence, int i, int i2, int i3) {
        }
    }

    static {
        new a(null);
    }

    public static final void t(ChainNetworkFragment chainNetworkFragment, View view) {
        fs1.f(chainNetworkFragment, "this$0");
        chainNetworkFragment.f();
    }

    @Override // androidx.fragment.app.Fragment
    public View onCreateView(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle) {
        fs1.f(layoutInflater, "inflater");
        return layoutInflater.inflate(R.layout.fragment_chain_network, viewGroup, false);
    }

    @Override // net.safemoon.androidwallet.fragments.common.BaseMainFragment, defpackage.qn, androidx.fragment.app.Fragment
    public void onViewCreated(View view, Bundle bundle) {
        fs1.f(view, "view");
        super.onViewCreated(view, bundle);
        s91 a2 = s91.a(view);
        fs1.e(a2, "bind(view)");
        this.i0 = a2;
        s91 s91Var = null;
        if (a2 == null) {
            fs1.r("binding");
            a2 = null;
        }
        a2.c.a.setOnClickListener(new View.OnClickListener() { // from class: ex
            @Override // android.view.View.OnClickListener
            public final void onClick(View view2) {
                ChainNetworkFragment.t(ChainNetworkFragment.this, view2);
            }
        });
        a2.c.c.setText(R.string.screen_title_cc_network);
        RecyclerView recyclerView = a2.a;
        recyclerView.setLayoutManager(new LinearLayoutManager(recyclerView.getContext(), 1, false));
        recyclerView.setAdapter(s());
        s91 s91Var2 = this.i0;
        if (s91Var2 == null) {
            fs1.r("binding");
        } else {
            s91Var = s91Var2;
        }
        s91Var.b.b.addTextChangedListener(new b());
    }

    public final cx s() {
        return (cx) this.j0.getValue();
    }
}
