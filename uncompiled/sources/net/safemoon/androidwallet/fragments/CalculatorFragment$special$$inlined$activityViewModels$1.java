package net.safemoon.androidwallet.fragments;

import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentActivity;
import kotlin.jvm.internal.Lambda;

/* compiled from: FragmentViewModelLazy.kt */
/* loaded from: classes2.dex */
public final class CalculatorFragment$special$$inlined$activityViewModels$1 extends Lambda implements rc1<gj4> {
    public final /* synthetic */ Fragment $this_activityViewModels;

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public CalculatorFragment$special$$inlined$activityViewModels$1(Fragment fragment) {
        super(0);
        this.$this_activityViewModels = fragment;
    }

    /* JADX WARN: Can't rename method to resolve collision */
    @Override // defpackage.rc1
    public final gj4 invoke() {
        FragmentActivity requireActivity = this.$this_activityViewModels.requireActivity();
        fs1.c(requireActivity, "requireActivity()");
        gj4 viewModelStore = requireActivity.getViewModelStore();
        fs1.c(viewModelStore, "requireActivity().viewModelStore");
        return viewModelStore;
    }
}
