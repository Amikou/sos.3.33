package net.safemoon.androidwallet.fragments;

import java.lang.ref.WeakReference;
import kotlin.jvm.internal.Lambda;
import net.safemoon.androidwallet.R;

/* compiled from: SendtoFragment.kt */
/* loaded from: classes2.dex */
public final class SendtoFragment$bind$1 extends Lambda implements tc1<Boolean, te4> {
    public final /* synthetic */ SendtoFragment this$0;

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public SendtoFragment$bind$1(SendtoFragment sendtoFragment) {
        super(1);
        this.this$0 = sendtoFragment;
    }

    @Override // defpackage.tc1
    public /* bridge */ /* synthetic */ te4 invoke(Boolean bool) {
        invoke(bool.booleanValue());
        return te4.a;
    }

    public final void invoke(boolean z) {
        if (z) {
            bh.Y(new WeakReference(this.this$0.requireActivity()), null, Integer.valueOf((int) R.string.send_full_amount_recipient_warning), R.string.dialog_alert_slip_button, null, 16, null);
        }
        this.this$0.p0();
    }
}
