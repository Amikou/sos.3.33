package net.safemoon.androidwallet.fragments;

import kotlin.coroutines.jvm.internal.SuspendLambda;
import net.safemoon.androidwallet.fragments.TransferHistoryFragment$transactionAdapter$2;
import net.safemoon.androidwallet.model.transaction.history.Result;

/* compiled from: TransferHistoryFragment.kt */
@kotlin.coroutines.jvm.internal.a(c = "net.safemoon.androidwallet.fragments.TransferHistoryFragment$initRecyclerView$2", f = "TransferHistoryFragment.kt", l = {674}, m = "invokeSuspend")
/* loaded from: classes2.dex */
public final class TransferHistoryFragment$initRecyclerView$2 extends SuspendLambda implements hd1<c90, q70<? super te4>, Object> {
    public int label;
    public final /* synthetic */ TransferHistoryFragment this$0;

    /* compiled from: Collect.kt */
    /* loaded from: classes2.dex */
    public static final class a implements k71<fp2<Result>> {
        public final /* synthetic */ TransferHistoryFragment a;

        public a(TransferHistoryFragment transferHistoryFragment) {
            this.a = transferHistoryFragment;
        }

        @Override // defpackage.k71
        public Object emit(fp2<Result> fp2Var, q70<? super te4> q70Var) {
            TransferHistoryFragment$transactionAdapter$2.a f0;
            f0 = this.a.f0();
            Object e = f0.e(fp2Var, q70Var);
            return e == gs1.d() ? e : te4.a;
        }
    }

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public TransferHistoryFragment$initRecyclerView$2(TransferHistoryFragment transferHistoryFragment, q70<? super TransferHistoryFragment$initRecyclerView$2> q70Var) {
        super(2, q70Var);
        this.this$0 = transferHistoryFragment;
    }

    @Override // kotlin.coroutines.jvm.internal.BaseContinuationImpl
    public final q70<te4> create(Object obj, q70<?> q70Var) {
        return new TransferHistoryFragment$initRecyclerView$2(this.this$0, q70Var);
    }

    @Override // defpackage.hd1
    public final Object invoke(c90 c90Var, q70<? super te4> q70Var) {
        return ((TransferHistoryFragment$initRecyclerView$2) create(c90Var, q70Var)).invokeSuspend(te4.a);
    }

    @Override // kotlin.coroutines.jvm.internal.BaseContinuationImpl
    public final Object invokeSuspend(Object obj) {
        j71 h0;
        Object d = gs1.d();
        int i = this.label;
        if (i == 0) {
            o83.b(obj);
            h0 = this.this$0.h0();
            a aVar = new a(this.this$0);
            this.label = 1;
            if (h0.a(aVar, this) == d) {
                return d;
            }
        } else if (i != 1) {
            throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        } else {
            o83.b(obj);
        }
        return te4.a;
    }
}
