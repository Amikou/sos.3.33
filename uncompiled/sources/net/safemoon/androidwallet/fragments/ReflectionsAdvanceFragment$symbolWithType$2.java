package net.safemoon.androidwallet.fragments;

import kotlin.jvm.internal.Lambda;

/* compiled from: ReflectionsAdvanceFragment.kt */
/* loaded from: classes2.dex */
public final class ReflectionsAdvanceFragment$symbolWithType$2 extends Lambda implements rc1<String> {
    public final /* synthetic */ ReflectionsAdvanceFragment this$0;

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public ReflectionsAdvanceFragment$symbolWithType$2(ReflectionsAdvanceFragment reflectionsAdvanceFragment) {
        super(0);
        this.this$0 = reflectionsAdvanceFragment;
    }

    @Override // defpackage.rc1
    public final String invoke() {
        return this.this$0.requireArguments().getString("symbolWithType", "");
    }
}
