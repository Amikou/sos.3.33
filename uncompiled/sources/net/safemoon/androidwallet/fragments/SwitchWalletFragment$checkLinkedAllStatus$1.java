package net.safemoon.androidwallet.fragments;

import android.content.Context;
import java.util.Iterator;
import java.util.List;
import kotlin.jvm.internal.Lambda;
import net.safemoon.androidwallet.model.wallets.Wallet;
import net.safemoon.androidwallet.viewmodels.MultiWalletViewModel;

/* compiled from: SwitchWalletFragment.kt */
/* loaded from: classes2.dex */
public final class SwitchWalletFragment$checkLinkedAllStatus$1 extends Lambda implements tc1<List<? extends Wallet>, te4> {
    public final /* synthetic */ SwitchWalletFragment this$0;

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public SwitchWalletFragment$checkLinkedAllStatus$1(SwitchWalletFragment switchWalletFragment) {
        super(1);
        this.this$0 = switchWalletFragment;
    }

    @Override // defpackage.tc1
    public /* bridge */ /* synthetic */ te4 invoke(List<? extends Wallet> list) {
        invoke2((List<Wallet>) list);
        return te4.a;
    }

    /* renamed from: invoke  reason: avoid collision after fix types in other method */
    public final void invoke2(List<Wallet> list) {
        MultiWalletViewModel f0;
        boolean z;
        lc2 lc2Var;
        fs1.f(list, "list");
        f0 = this.this$0.f0();
        f0.F(list);
        Iterator<Wallet> it = list.iterator();
        while (true) {
            if (!it.hasNext()) {
                z = true;
                break;
            } else if (!it.next().isLinked()) {
                z = false;
                break;
            }
        }
        boolean z2 = list.size() != 1 ? z : false;
        do3 do3Var = do3.a;
        Context requireContext = this.this$0.requireContext();
        fs1.e(requireContext, "requireContext()");
        do3Var.h(requireContext, z2);
        lc2Var = this.this$0.r0;
        if (lc2Var == null) {
            fs1.r("myAdapter");
            lc2Var = null;
        }
        lc2Var.notifyDataSetChanged();
    }
}
