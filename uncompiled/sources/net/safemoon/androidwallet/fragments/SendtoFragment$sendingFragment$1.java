package net.safemoon.androidwallet.fragments;

import defpackage.nl3;
import kotlin.jvm.internal.Lambda;
import net.safemoon.androidwallet.ui.displayModel.UserTokenItemDisplayModel;
import net.safemoon.androidwallet.viewmodels.ContactViewModel;

/* compiled from: SendtoFragment.kt */
/* loaded from: classes2.dex */
public final class SendtoFragment$sendingFragment$1 extends Lambda implements rc1<te4> {
    public final /* synthetic */ String $sendingAmount;
    public final /* synthetic */ SendtoFragment this$0;

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public SendtoFragment$sendingFragment$1(SendtoFragment sendtoFragment, String str) {
        super(0);
        this.this$0 = sendtoFragment;
        this.$sendingAmount = str;
    }

    @Override // defpackage.rc1
    public /* bridge */ /* synthetic */ te4 invoke() {
        invoke2();
        return te4.a;
    }

    @Override // defpackage.rc1
    /* renamed from: invoke  reason: avoid collision after fix types in other method */
    public final void invoke2() {
        UserTokenItemDisplayModel userTokenItemDisplayModel;
        ContactViewModel contactViewModel = this.this$0.q0;
        fs1.d(contactViewModel);
        contactViewModel.s();
        SendtoFragment sendtoFragment = this.this$0;
        bb1 bb1Var = sendtoFragment.m0;
        fs1.d(bb1Var);
        String obj = bb1Var.i.getText().toString();
        String str = this.$sendingAmount;
        userTokenItemDisplayModel = this.this$0.n0;
        fs1.d(userTokenItemDisplayModel);
        nl3.b b = nl3.b(obj, str, userTokenItemDisplayModel);
        fs1.e(b, "actionSendtoFragmentToSe…Token!!\n                )");
        sendtoFragment.g(b);
    }
}
