package net.safemoon.androidwallet.fragments;

import kotlin.jvm.internal.Lambda;
import net.safemoon.androidwallet.common.TokenType;

/* compiled from: TransferDetailsFragmentStatus.kt */
/* loaded from: classes2.dex */
public final class TransferDetailsFragmentStatus$chainType$2 extends Lambda implements rc1<TokenType> {
    public final /* synthetic */ TransferDetailsFragmentStatus this$0;

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public TransferDetailsFragmentStatus$chainType$2(TransferDetailsFragmentStatus transferDetailsFragmentStatus) {
        super(0);
        this.this$0 = transferDetailsFragmentStatus;
    }

    @Override // defpackage.rc1
    public final TokenType invoke() {
        return TokenType.Companion.b(this.this$0.requireArguments().getInt("tokenChainId"));
    }
}
