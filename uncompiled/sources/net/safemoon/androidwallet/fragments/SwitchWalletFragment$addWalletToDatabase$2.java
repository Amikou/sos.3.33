package net.safemoon.androidwallet.fragments;

import java.util.List;
import kotlin.jvm.internal.Lambda;

/* compiled from: SwitchWalletFragment.kt */
/* loaded from: classes2.dex */
public final class SwitchWalletFragment$addWalletToDatabase$2 extends Lambda implements tc1<Long, te4> {
    public final /* synthetic */ SwitchWalletFragment this$0;

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public SwitchWalletFragment$addWalletToDatabase$2(SwitchWalletFragment switchWalletFragment) {
        super(1);
        this.this$0 = switchWalletFragment;
    }

    @Override // defpackage.tc1
    public /* bridge */ /* synthetic */ te4 invoke(Long l) {
        invoke2(l);
        return te4.a;
    }

    /* renamed from: invoke  reason: avoid collision after fix types in other method */
    public final void invoke2(Long l) {
        int i;
        int i2;
        List list;
        List list2;
        int i3;
        SwitchWalletFragment switchWalletFragment = this.this$0;
        i = switchWalletFragment.n0;
        switchWalletFragment.n0 = i + 1;
        i2 = this.this$0.n0;
        list = this.this$0.m0;
        if (i2 >= list.size()) {
            this.this$0.a0();
            return;
        }
        SwitchWalletFragment switchWalletFragment2 = this.this$0;
        list2 = switchWalletFragment2.m0;
        i3 = this.this$0.n0;
        switchWalletFragment2.U((String) list2.get(i3));
    }
}
