package net.safemoon.androidwallet.fragments;

import defpackage.w21;
import kotlin.jvm.internal.Lambda;
import net.safemoon.androidwallet.model.fiat.gson.Fiat;

/* compiled from: CalculatorFragment.kt */
/* loaded from: classes2.dex */
public final class CalculatorFragment$defaultCurrencyAdapter$2 extends Lambda implements rc1<a> {
    public final /* synthetic */ CalculatorFragment this$0;

    /* compiled from: CalculatorFragment.kt */
    /* loaded from: classes2.dex */
    public static final class a extends w21 {
        public final /* synthetic */ CalculatorFragment c;

        /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
        public a(CalculatorFragment calculatorFragment, b bVar) {
            super(bVar);
            this.c = calculatorFragment;
        }

        @Override // defpackage.w21
        public Fiat c() {
            gb2 gb2Var;
            gb2Var = this.c.u0;
            T value = gb2Var.getValue();
            fs1.d(value);
            fs1.e(value, "defaultFiat.value!!");
            return (Fiat) value;
        }
    }

    /* compiled from: CalculatorFragment.kt */
    /* loaded from: classes2.dex */
    public static final class b implements w21.a {
        public final /* synthetic */ CalculatorFragment a;

        public b(CalculatorFragment calculatorFragment) {
            this.a = calculatorFragment;
        }

        @Override // defpackage.w21.a
        public void a(Fiat fiat) {
            gb2 gb2Var;
            fs1.f(fiat, "item");
            gb2Var = this.a.u0;
            gb2Var.postValue(fiat);
            this.a.V();
        }
    }

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public CalculatorFragment$defaultCurrencyAdapter$2(CalculatorFragment calculatorFragment) {
        super(0);
        this.this$0 = calculatorFragment;
    }

    @Override // defpackage.rc1
    public final a invoke() {
        return new a(this.this$0, new b(this.this$0));
    }
}
