package net.safemoon.androidwallet.fragments;

import com.github.mikephil.charting.utils.Utils;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import kotlin.coroutines.jvm.internal.SuspendLambda;
import net.safemoon.androidwallet.model.reflections.RoomReflectionsDataAndToken;
import net.safemoon.androidwallet.viewmodels.ReflectionTrackerViewModel;

/* compiled from: ReflectionsAdvanceFragment.kt */
@kotlin.coroutines.jvm.internal.a(c = "net.safemoon.androidwallet.fragments.ReflectionsAdvanceFragment$onViewCreated$2", f = "ReflectionsAdvanceFragment.kt", l = {349}, m = "invokeSuspend")
/* loaded from: classes2.dex */
public final class ReflectionsAdvanceFragment$onViewCreated$2 extends SuspendLambda implements hd1<c90, q70<? super te4>, Object> {
    public int label;
    public final /* synthetic */ ReflectionsAdvanceFragment this$0;

    /* compiled from: Collect.kt */
    /* loaded from: classes2.dex */
    public static final class a implements k71<List<RoomReflectionsDataAndToken>> {
        public final /* synthetic */ ReflectionsAdvanceFragment a;

        public a(ReflectionsAdvanceFragment reflectionsAdvanceFragment) {
            this.a = reflectionsAdvanceFragment;
        }

        @Override // defpackage.k71
        public Object emit(List<RoomReflectionsDataAndToken> list, q70<? super te4> q70Var) {
            j53 K;
            j53 K2;
            List<RoomReflectionsDataAndToken> list2 = list;
            K = this.a.K();
            K.submitList(null);
            K2 = this.a.K();
            ArrayList arrayList = new ArrayList();
            Iterator<T> it = list2.iterator();
            while (true) {
                if (!it.hasNext()) {
                    break;
                }
                Object next = it.next();
                Float diffBalance = ((RoomReflectionsDataAndToken) next).getDiffBalance();
                fs1.d(diffBalance);
                if (diffBalance.floatValue() > Utils.FLOAT_EPSILON) {
                    arrayList.add(next);
                }
            }
            K2.submitList(arrayList);
            ReflectionsAdvanceFragment reflectionsAdvanceFragment = this.a;
            ArrayList arrayList2 = new ArrayList();
            for (Object obj : list2) {
                Float diffBalance2 = ((RoomReflectionsDataAndToken) obj).getDiffBalance();
                fs1.d(diffBalance2);
                if (diffBalance2.floatValue() > Utils.FLOAT_EPSILON) {
                    arrayList2.add(obj);
                }
            }
            reflectionsAdvanceFragment.I(arrayList2);
            return te4.a;
        }
    }

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public ReflectionsAdvanceFragment$onViewCreated$2(ReflectionsAdvanceFragment reflectionsAdvanceFragment, q70<? super ReflectionsAdvanceFragment$onViewCreated$2> q70Var) {
        super(2, q70Var);
        this.this$0 = reflectionsAdvanceFragment;
    }

    @Override // kotlin.coroutines.jvm.internal.BaseContinuationImpl
    public final q70<te4> create(Object obj, q70<?> q70Var) {
        return new ReflectionsAdvanceFragment$onViewCreated$2(this.this$0, q70Var);
    }

    @Override // defpackage.hd1
    public final Object invoke(c90 c90Var, q70<? super te4> q70Var) {
        return ((ReflectionsAdvanceFragment$onViewCreated$2) create(c90Var, q70Var)).invokeSuspend(te4.a);
    }

    @Override // kotlin.coroutines.jvm.internal.BaseContinuationImpl
    public final Object invokeSuspend(Object obj) {
        ReflectionTrackerViewModel L;
        Object d = gs1.d();
        int i = this.label;
        if (i == 0) {
            o83.b(obj);
            L = this.this$0.L();
            j71<List<RoomReflectionsDataAndToken>> l = L.l();
            a aVar = new a(this.this$0);
            this.label = 1;
            if (l.a(aVar, this) == d) {
                return d;
            }
        } else if (i != 1) {
            throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        } else {
            o83.b(obj);
        }
        return te4.a;
    }
}
