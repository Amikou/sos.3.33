package net.safemoon.androidwallet.fragments;

import android.content.Context;
import defpackage.ea4;
import kotlin.jvm.internal.Lambda;
import net.safemoon.androidwallet.adapter.TransactionAdapter;
import net.safemoon.androidwallet.model.transaction.history.Result;
import net.safemoon.androidwallet.ui.displayModel.UserTokenItemDisplayModel;
import net.safemoon.androidwallet.viewmodels.TransactionHistoryViewModel;

/* compiled from: TransferHistoryFragment.kt */
/* loaded from: classes2.dex */
public final class TransferHistoryFragment$transactionAdapter$2 extends Lambda implements rc1<a> {
    public final /* synthetic */ TransferHistoryFragment this$0;

    /* compiled from: TransferHistoryFragment.kt */
    /* renamed from: net.safemoon.androidwallet.fragments.TransferHistoryFragment$transactionAdapter$2$2  reason: invalid class name */
    /* loaded from: classes2.dex */
    public static final class AnonymousClass2 extends Lambda implements tc1<Result, te4> {
        public final /* synthetic */ TransferHistoryFragment this$0;

        /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
        public AnonymousClass2(TransferHistoryFragment transferHistoryFragment) {
            super(1);
            this.this$0 = transferHistoryFragment;
        }

        @Override // defpackage.tc1
        public /* bridge */ /* synthetic */ te4 invoke(Result result) {
            invoke2(result);
            return te4.a;
        }

        /* renamed from: invoke  reason: avoid collision after fix types in other method */
        public final void invoke2(Result result) {
            TransactionHistoryViewModel g0;
            UserTokenItemDisplayModel i0;
            fs1.f(result, "result");
            TransferHistoryFragment transferHistoryFragment = this.this$0;
            if (result.tokenDecimal == null) {
                i0 = transferHistoryFragment.i0();
                result.tokenDecimal = Integer.valueOf(i0.getDecimals());
            }
            TransferHistoryFragment transferHistoryFragment2 = this.this$0;
            g0 = transferHistoryFragment2.g0();
            ea4.f e = ea4.e(result, g0.g().getChainId(), result.offlinePending);
            fs1.e(e, "actionTransferHistoryFra…d, result.offlinePending)");
            transferHistoryFragment2.g(e);
        }
    }

    /* compiled from: TransferHistoryFragment.kt */
    /* loaded from: classes2.dex */
    public static final class a extends TransactionAdapter {
        public final /* synthetic */ TransferHistoryFragment h;

        /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
        public a(TransferHistoryFragment transferHistoryFragment, Context context, AnonymousClass2 anonymousClass2) {
            super(context, anonymousClass2);
            this.h = transferHistoryFragment;
            fs1.e(context, "requireContext()");
        }

        @Override // net.safemoon.androidwallet.adapter.TransactionAdapter
        public UserTokenItemDisplayModel o() {
            UserTokenItemDisplayModel i0;
            i0 = this.h.i0();
            return i0;
        }
    }

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public TransferHistoryFragment$transactionAdapter$2(TransferHistoryFragment transferHistoryFragment) {
        super(0);
        this.this$0 = transferHistoryFragment;
    }

    @Override // defpackage.rc1
    public final a invoke() {
        return new a(this.this$0, this.this$0.requireContext(), new AnonymousClass2(this.this$0));
    }
}
