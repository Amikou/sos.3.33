package net.safemoon.androidwallet.fragments;

import kotlin.jvm.internal.Lambda;
import net.safemoon.androidwallet.model.transaction.history.Result;
import net.safemoon.androidwallet.ui.displayModel.UserTokenItemDisplayModel;
import net.safemoon.androidwallet.viewmodels.TransactionHistoryViewModel;

/* compiled from: TransferHistoryFragment.kt */
/* loaded from: classes2.dex */
public final class TransferHistoryFragment$transferList$2 extends Lambda implements rc1<j71<? extends fp2<Result>>> {
    public final /* synthetic */ TransferHistoryFragment this$0;

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public TransferHistoryFragment$transferList$2(TransferHistoryFragment transferHistoryFragment) {
        super(0);
        this.this$0 = transferHistoryFragment;
    }

    @Override // defpackage.rc1
    /* renamed from: invoke */
    public final j71<? extends fp2<Result>> invoke2() {
        TransactionHistoryViewModel g0;
        UserTokenItemDisplayModel i0;
        g0 = this.this$0.g0();
        i0 = this.this$0.i0();
        return g0.q(i0);
    }
}
