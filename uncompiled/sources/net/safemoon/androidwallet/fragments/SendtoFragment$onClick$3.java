package net.safemoon.androidwallet.fragments;

import android.content.Intent;
import kotlin.jvm.internal.Lambda;

/* compiled from: SendtoFragment.kt */
/* loaded from: classes2.dex */
public final class SendtoFragment$onClick$3 extends Lambda implements tc1<Intent, te4> {
    public final /* synthetic */ SendtoFragment this$0;

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public SendtoFragment$onClick$3(SendtoFragment sendtoFragment) {
        super(1);
        this.this$0 = sendtoFragment;
    }

    @Override // defpackage.tc1
    public /* bridge */ /* synthetic */ te4 invoke(Intent intent) {
        invoke2(intent);
        return te4.a;
    }

    /* JADX WARN: Code restructure failed: missing block: B:3:0x0002, code lost:
        r5 = r5.getStringExtra("result");
     */
    /* renamed from: invoke  reason: avoid collision after fix types in other method */
    /*
        Code decompiled incorrectly, please refer to instructions dump.
        To view partially-correct code enable 'Show inconsistent code' option in preferences
    */
    public final void invoke2(android.content.Intent r5) {
        /*
            r4 = this;
            if (r5 == 0) goto L43
            java.lang.String r0 = "result"
            java.lang.String r5 = r5.getStringExtra(r0)
            if (r5 == 0) goto L43
            b30 r0 = defpackage.b30.a     // Catch: java.lang.Exception -> L11
            java.lang.String r5 = r0.q(r5)     // Catch: java.lang.Exception -> L11
            goto L15
        L11:
            r0 = move-exception
            r0.printStackTrace()
        L15:
            defpackage.fs1.d(r5)
            r0 = 0
            r1 = 2
            r2 = 0
            java.lang.String r3 = "0x"
            boolean r0 = defpackage.dv3.H(r5, r3, r0, r1, r2)
            if (r0 == 0) goto L32
            net.safemoon.androidwallet.fragments.SendtoFragment r0 = r4.this$0
            bb1 r0 = net.safemoon.androidwallet.fragments.SendtoFragment.C(r0)
            defpackage.fs1.d(r0)
            android.widget.EditText r0 = r0.i
            r0.setText(r5)
            goto L43
        L32:
            net.safemoon.androidwallet.fragments.SendtoFragment r5 = r4.this$0
            androidx.fragment.app.FragmentActivity r5 = r5.requireActivity()
            java.lang.String r0 = "requireActivity()"
            defpackage.fs1.e(r5, r0)
            r0 = 2131952345(0x7f1302d9, float:1.954113E38)
            defpackage.e30.Z(r5, r0)
        L43:
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: net.safemoon.androidwallet.fragments.SendtoFragment$onClick$3.invoke2(android.content.Intent):void");
    }
}
