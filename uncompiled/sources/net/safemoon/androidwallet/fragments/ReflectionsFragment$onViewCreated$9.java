package net.safemoon.androidwallet.fragments;

import java.util.List;
import kotlin.coroutines.jvm.internal.SuspendLambda;
import net.safemoon.androidwallet.adapter.touchHelper.RecyclerTouchListener;
import net.safemoon.androidwallet.model.reflections.RoomReflectionsToken;

/* compiled from: ReflectionsFragment.kt */
@kotlin.coroutines.jvm.internal.a(c = "net.safemoon.androidwallet.fragments.ReflectionsFragment$onViewCreated$9", f = "ReflectionsFragment.kt", l = {284}, m = "invokeSuspend")
/* loaded from: classes2.dex */
public final class ReflectionsFragment$onViewCreated$9 extends SuspendLambda implements hd1<c90, q70<? super te4>, Object> {
    public final /* synthetic */ RecyclerTouchListener $touchListener;
    public int label;
    public final /* synthetic */ ReflectionsFragment this$0;

    /* compiled from: Collect.kt */
    /* loaded from: classes2.dex */
    public static final class a implements k71<List<RoomReflectionsToken>> {
        public final /* synthetic */ ReflectionsFragment a;
        public final /* synthetic */ RecyclerTouchListener f0;

        public a(ReflectionsFragment reflectionsFragment, RecyclerTouchListener recyclerTouchListener) {
            this.a = reflectionsFragment;
            this.f0 = recyclerTouchListener;
        }

        @Override // defpackage.k71
        public Object emit(List<RoomReflectionsToken> list, q70<? super te4> q70Var) {
            te4 te4Var;
            List<RoomReflectionsToken> list2 = list;
            if (list2 == null) {
                te4Var = null;
            } else {
                this.a.E(this.f0, list2);
                te4Var = te4.a;
            }
            return te4Var == gs1.d() ? te4Var : te4.a;
        }
    }

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public ReflectionsFragment$onViewCreated$9(ReflectionsFragment reflectionsFragment, RecyclerTouchListener recyclerTouchListener, q70<? super ReflectionsFragment$onViewCreated$9> q70Var) {
        super(2, q70Var);
        this.this$0 = reflectionsFragment;
        this.$touchListener = recyclerTouchListener;
    }

    @Override // kotlin.coroutines.jvm.internal.BaseContinuationImpl
    public final q70<te4> create(Object obj, q70<?> q70Var) {
        return new ReflectionsFragment$onViewCreated$9(this.this$0, this.$touchListener, q70Var);
    }

    @Override // defpackage.hd1
    public final Object invoke(c90 c90Var, q70<? super te4> q70Var) {
        return ((ReflectionsFragment$onViewCreated$9) create(c90Var, q70Var)).invokeSuspend(te4.a);
    }

    @Override // kotlin.coroutines.jvm.internal.BaseContinuationImpl
    public final Object invokeSuspend(Object obj) {
        Object d = gs1.d();
        int i = this.label;
        if (i == 0) {
            o83.b(obj);
            this.this$0.D().G();
            j71<List<RoomReflectionsToken>> k = this.this$0.D().k(this.this$0.D().t());
            a aVar = new a(this.this$0, this.$touchListener);
            this.label = 1;
            if (k.a(aVar, this) == d) {
                return d;
            }
        } else if (i != 1) {
            throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        } else {
            o83.b(obj);
        }
        return te4.a;
    }
}
