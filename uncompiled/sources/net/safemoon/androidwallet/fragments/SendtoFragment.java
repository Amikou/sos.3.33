package net.safemoon.androidwallet.fragments;

import android.content.ClipData;
import android.content.ClipboardManager;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.text.Editable;
import android.text.InputFilter;
import android.text.TextWatcher;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.PopupWindow;
import android.widget.ScrollView;
import android.widget.TextView;
import androidx.appcompat.widget.AppCompatEditText;
import androidx.appcompat.widget.AppCompatImageView;
import androidx.fragment.app.FragmentActivity;
import androidx.fragment.app.FragmentManager;
import androidx.lifecycle.l;
import com.github.mikephil.charting.utils.Utils;
import com.google.android.material.checkbox.MaterialCheckBox;
import com.google.android.material.textfield.TextInputEditText;
import defpackage.u21;
import java.lang.ref.WeakReference;
import java.math.RoundingMode;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Objects;
import net.safemoon.androidwallet.R;
import net.safemoon.androidwallet.activity.ScannedCodeActivity;
import net.safemoon.androidwallet.dialogs.G2FAVerfication;
import net.safemoon.androidwallet.fragments.SendtoFragment;
import net.safemoon.androidwallet.fragments.common.BaseMainFragment;
import net.safemoon.androidwallet.model.contact.abstraction.IContact;
import net.safemoon.androidwallet.model.tokensInfo.CurrencyTokenInfo;
import net.safemoon.androidwallet.model.tokensInfo.CurrencyTokenInfoResult;
import net.safemoon.androidwallet.provider.AskAuthorizeProvider;
import net.safemoon.androidwallet.ui.displayModel.UserTokenItemDisplayModel;
import net.safemoon.androidwallet.viewmodels.ContactViewModel;
import net.safemoon.androidwallet.viewmodels.MultiWalletViewModel;
import net.safemoon.androidwallet.viewmodels.MyTokensListViewModel;
import net.safemoon.androidwallet.viewmodels.factory.MyViewModelFactory;
import net.safemoon.androidwallet.views.CustomKeyBoard;
import net.safemoon.androidwallet.views.carousel.ContactCarouselView;
import retrofit2.n;

/* compiled from: SendtoFragment.kt */
/* loaded from: classes2.dex */
public final class SendtoFragment extends BaseMainFragment implements View.OnClickListener {
    public static String x0;
    public MyTokensListViewModel i0;
    public double j0;
    public double k0;
    public ClipboardManager l0;
    public bb1 m0;
    public UserTokenItemDisplayModel n0;
    public q60 o0;
    public qc p0;
    public ContactViewModel q0;
    public MultiWalletViewModel r0;
    public final List<IContact> s0 = new ArrayList();
    public final gb2<Boolean> t0 = new gb2<>(Boolean.TRUE);
    public final Map<String, Double> u0;
    public final sy1 v0;
    public long w0;

    /* compiled from: SendtoFragment.kt */
    /* loaded from: classes2.dex */
    public static final class a {
        public a() {
        }

        public /* synthetic */ a(qi0 qi0Var) {
            this();
        }
    }

    /* compiled from: SendtoFragment.kt */
    /* loaded from: classes2.dex */
    public static final class b implements ContactCarouselView.b {
        public b() {
        }

        @Override // net.safemoon.androidwallet.views.carousel.ContactCarouselView.b
        public void a(boolean z, int i, IContact iContact) {
            fs1.f(iContact, "contact");
            if (SendtoFragment.this.p0 != null) {
                qc qcVar = SendtoFragment.this.p0;
                fs1.d(qcVar);
                if (qcVar.e()) {
                    qc qcVar2 = SendtoFragment.this.p0;
                    fs1.d(qcVar2);
                    qcVar2.d();
                }
            }
            if (SendtoFragment.this.o0 != null) {
                q60 q60Var = SendtoFragment.this.o0;
                fs1.d(q60Var);
                if (q60Var.h()) {
                    q60 q60Var2 = SendtoFragment.this.o0;
                    fs1.d(q60Var2);
                    q60Var2.g();
                }
            }
            if (z) {
                ContactViewModel contactViewModel = SendtoFragment.this.q0;
                fs1.d(contactViewModel);
                contactViewModel.r(iContact);
                return;
            }
            ContactViewModel contactViewModel2 = SendtoFragment.this.q0;
            fs1.d(contactViewModel2);
            contactViewModel2.p();
            SendtoFragment.this.Z();
        }
    }

    /* compiled from: SendtoFragment.kt */
    /* loaded from: classes2.dex */
    public static final class c implements TextWatcher {
        public c() {
        }

        @Override // android.text.TextWatcher
        public void afterTextChanged(Editable editable) {
            fs1.f(editable, "s");
            bb1 bb1Var = SendtoFragment.this.m0;
            fs1.d(bb1Var);
            bb1Var.c.setVisibility(editable.toString().length() > 0 ? 0 : 8);
        }

        @Override // android.text.TextWatcher
        public void beforeTextChanged(CharSequence charSequence, int i, int i2, int i3) {
            fs1.f(charSequence, "s");
        }

        @Override // android.text.TextWatcher
        public void onTextChanged(CharSequence charSequence, int i, int i2, int i3) {
            fs1.f(charSequence, "s");
        }
    }

    /* compiled from: SendtoFragment.kt */
    /* loaded from: classes2.dex */
    public static final class d implements G2FAVerfication.b {
        public d() {
        }

        @Override // net.safemoon.androidwallet.dialogs.G2FAVerfication.b
        public void a() {
        }

        @Override // net.safemoon.androidwallet.dialogs.G2FAVerfication.b
        public void onSuccess() {
            SendtoFragment.this.n0();
        }
    }

    /* compiled from: SendtoFragment.kt */
    /* loaded from: classes2.dex */
    public static final class e implements sl1 {
        public e() {
        }

        @Override // defpackage.sl1
        public void a(IContact iContact) {
            fs1.f(iContact, "item");
            bb1 bb1Var = SendtoFragment.this.m0;
            fs1.d(bb1Var);
            bb1Var.h.setSelectedItem(iContact);
            ContactViewModel contactViewModel = SendtoFragment.this.q0;
            fs1.d(contactViewModel);
            contactViewModel.n().postValue(iContact);
            if (SendtoFragment.this.o0 != null) {
                q60 q60Var = SendtoFragment.this.o0;
                fs1.d(q60Var);
                q60Var.g();
            }
        }
    }

    /* compiled from: SendtoFragment.kt */
    /* loaded from: classes2.dex */
    public static final class f extends cj2 {
        /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
        public f(TextInputEditText textInputEditText) {
            super(textInputEditText);
            fs1.e(textInputEditText, "etAmount");
        }

        @Override // defpackage.cj2, android.text.TextWatcher
        public void onTextChanged(CharSequence charSequence, int i, int i2, int i3) {
            fs1.f(charSequence, "s");
            super.onTextChanged(charSequence, i, i2, i3);
            SendtoFragment.this.p0();
        }
    }

    /* compiled from: SendtoFragment.kt */
    /* loaded from: classes2.dex */
    public static final class g implements wu<CurrencyTokenInfoResult> {
        public g() {
        }

        @Override // defpackage.wu
        public void a(retrofit2.b<CurrencyTokenInfoResult> bVar, Throwable th) {
            fs1.f(bVar, "call");
            fs1.f(th, "t");
        }

        @Override // defpackage.wu
        public void b(retrofit2.b<CurrencyTokenInfoResult> bVar, n<CurrencyTokenInfoResult> nVar) {
            String priceUsd;
            fs1.f(bVar, "call");
            fs1.f(nVar, "response");
            if (nVar.a() != null) {
                CurrencyTokenInfoResult a = nVar.a();
                fs1.d(a);
                if (a.getData() != null) {
                    CurrencyTokenInfoResult a2 = nVar.a();
                    CurrencyTokenInfo data = a2 == null ? null : a2.getData();
                    if (data == null || (priceUsd = data.getPriceUsd()) == null) {
                        return;
                    }
                    SendtoFragment.this.o0(Double.parseDouble(priceUsd));
                }
            }
        }
    }

    static {
        new a(null);
        x0 = "userTokenData";
    }

    public SendtoFragment() {
        String lowerCase = "0x42981d0bfbAf196529376EE702F2a9Eb9092fcB5".toLowerCase(Locale.ROOT);
        fs1.e(lowerCase, "(this as java.lang.Strin….toLowerCase(Locale.ROOT)");
        this.u0 = y32.b(qc4.a(lowerCase, Double.valueOf(2.0d)));
        this.v0 = zy1.a(new SendtoFragment$isFullAllowToRecipient$2(this));
    }

    public static final void P(SendtoFragment sendtoFragment, View view) {
        fs1.f(sendtoFragment, "this$0");
        bb1 bb1Var = sendtoFragment.m0;
        fs1.d(bb1Var);
        bb1Var.i.setText("");
    }

    public static final boolean Q(SendtoFragment sendtoFragment, TextView textView, int i, KeyEvent keyEvent) {
        fs1.f(sendtoFragment, "this$0");
        if (i == 6) {
            pg4.e(sendtoFragment.requireActivity());
            return true;
        }
        return false;
    }

    public static final void R(SendtoFragment sendtoFragment, View view, boolean z) {
        fs1.f(sendtoFragment, "this$0");
        if (z) {
            pg4.e(sendtoFragment.requireActivity());
        }
    }

    public static final void S(SendtoFragment sendtoFragment, View view) {
        fs1.f(sendtoFragment, "this$0");
        gb2<Boolean> gb2Var = sendtoFragment.t0;
        Boolean value = gb2Var.getValue();
        fs1.d(value);
        gb2Var.postValue(Boolean.valueOf(!value.booleanValue()));
    }

    public static final void b0(SendtoFragment sendtoFragment) {
        fs1.f(sendtoFragment, "this$0");
        pg4.e(sendtoFragment.requireActivity());
        bb1 bb1Var = sendtoFragment.m0;
        fs1.d(bb1Var);
        bb1Var.i.setVisibility(0);
        bb1 bb1Var2 = sendtoFragment.m0;
        fs1.d(bb1Var2);
        if (!(bb1Var2.i.getText().toString().length() == 0)) {
            bb1 bb1Var3 = sendtoFragment.m0;
            fs1.d(bb1Var3);
            bb1Var3.c.setVisibility(0);
        }
        bb1 bb1Var4 = sendtoFragment.m0;
        fs1.d(bb1Var4);
        bb1Var4.k.setText("");
        bb1 bb1Var5 = sendtoFragment.m0;
        fs1.d(bb1Var5);
        bb1Var5.k.setVisibility(8);
        sendtoFragment.o0 = null;
        sendtoFragment.w0 = System.currentTimeMillis();
    }

    public static final void e0(SendtoFragment sendtoFragment, Boolean bool) {
        fs1.f(sendtoFragment, "this$0");
        bb1 bb1Var = sendtoFragment.m0;
        fs1.d(bb1Var);
        TextView textView = bb1Var.w;
        fs1.e(bool, "it");
        textView.setSelected(bool.booleanValue());
        bb1 bb1Var2 = sendtoFragment.m0;
        fs1.d(bb1Var2);
        bb1Var2.s.setSelected(bool.booleanValue());
        bb1 bb1Var3 = sendtoFragment.m0;
        fs1.d(bb1Var3);
        bb1Var3.t.setSelected(bool.booleanValue());
        bb1 bb1Var4 = sendtoFragment.m0;
        fs1.d(bb1Var4);
        bb1Var4.r.setSelected(!bool.booleanValue());
        sendtoFragment.p0();
    }

    public static final void f0(SendtoFragment sendtoFragment, s60 s60Var) {
        fs1.f(sendtoFragment, "this$0");
        bb1 bb1Var = sendtoFragment.m0;
        AppCompatImageView appCompatImageView = bb1Var == null ? null : bb1Var.l;
        boolean z = true;
        if (appCompatImageView != null) {
            appCompatImageView.setVisibility(s60Var != null && !s60Var.a().isEmpty() ? 0 : 8);
        }
        bb1 bb1Var2 = sendtoFragment.m0;
        ContactCarouselView contactCarouselView = bb1Var2 != null ? bb1Var2.h : null;
        if (contactCarouselView != null) {
            if (s60Var == null || s60Var.a().isEmpty()) {
                z = false;
            }
            contactCarouselView.setVisibility(z ? 0 : 8);
        }
        if (s60Var == null || s60Var.a().isEmpty()) {
            return;
        }
        sendtoFragment.s0.clear();
        List<IContact> list = sendtoFragment.s0;
        List<IContact> a2 = s60Var.a();
        if (a2 == null) {
            a2 = b20.g();
        }
        list.addAll(a2);
        bb1 bb1Var3 = sendtoFragment.m0;
        fs1.d(bb1Var3);
        ContactCarouselView contactCarouselView2 = bb1Var3.h;
        List<IContact> a3 = s60Var.a();
        if (a3 == null) {
            a3 = b20.g();
        }
        contactCarouselView2.setItems(a3, s60Var.b());
        ContactViewModel contactViewModel = sendtoFragment.q0;
        fs1.d(contactViewModel);
        if (contactViewModel.n().getValue() != null) {
            bb1 bb1Var4 = sendtoFragment.m0;
            fs1.d(bb1Var4);
            ContactCarouselView contactCarouselView3 = bb1Var4.h;
            ContactViewModel contactViewModel2 = sendtoFragment.q0;
            fs1.d(contactViewModel2);
            IContact value = contactViewModel2.n().getValue();
            fs1.d(value);
            fs1.e(value, "contactViewModel!!.selectedContact.value!!");
            contactCarouselView3.setSelectedItem(value);
        }
        bb1 bb1Var5 = sendtoFragment.m0;
        fs1.d(bb1Var5);
        bb1Var5.h.p();
    }

    public static final void g0(SendtoFragment sendtoFragment, List list) {
        fs1.f(sendtoFragment, "this$0");
        ContactViewModel contactViewModel = sendtoFragment.q0;
        fs1.d(contactViewModel);
        contactViewModel.o();
    }

    public static final void h0(SendtoFragment sendtoFragment, IContact iContact) {
        fs1.f(sendtoFragment, "this$0");
        if (iContact != null) {
            bb1 bb1Var = sendtoFragment.m0;
            fs1.d(bb1Var);
            bb1Var.i.setText(iContact.getAddress());
        }
    }

    public static final void i0(SendtoFragment sendtoFragment, View view) {
        fs1.f(sendtoFragment, "this$0");
        if (bo3.d(sendtoFragment.requireContext(), "AUTH_2FA_IS_ENABLE")) {
            G2FAVerfication a2 = G2FAVerfication.C0.a(sendtoFragment.V(), false);
            FragmentManager childFragmentManager = sendtoFragment.getChildFragmentManager();
            fs1.e(childFragmentManager, "childFragmentManager");
            a2.H(childFragmentManager);
            return;
        }
        sendtoFragment.n0();
    }

    public static final void j0(View view) {
    }

    public static final void k0(SendtoFragment sendtoFragment, View view, int i, int i2, int i3, int i4, int i5, int i6, int i7, int i8) {
        fs1.f(sendtoFragment, "this$0");
        if (sendtoFragment.isVisible()) {
            bb1 bb1Var = sendtoFragment.m0;
            fs1.d(bb1Var);
            ScrollView scrollView = bb1Var.p;
            bb1 bb1Var2 = sendtoFragment.m0;
            fs1.d(bb1Var2);
            scrollView.smoothScrollTo(0, bb1Var2.p.getBottom());
        }
    }

    public static final void l0(SendtoFragment sendtoFragment, View view) {
        fs1.f(sendtoFragment, "this$0");
        ClipboardManager W = sendtoFragment.W();
        fs1.d(W);
        ClipData primaryClip = W.getPrimaryClip();
        if (primaryClip == null || primaryClip.getItemCount() <= 0 || primaryClip.getItemAt(0).getText() == null) {
            return;
        }
        String obj = primaryClip.getItemAt(0).getText().toString();
        bb1 bb1Var = sendtoFragment.m0;
        fs1.d(bb1Var);
        bb1Var.i.setText(obj);
    }

    public static final void m0(SendtoFragment sendtoFragment, Double d2) {
        fs1.f(sendtoFragment, "this$0");
        if (d2 != null) {
            bb1 bb1Var = sendtoFragment.m0;
            fs1.d(bb1Var);
            TextView textView = bb1Var.q;
            fs1.e(textView, "binding!!.tvBalance");
            UserTokenItemDisplayModel userTokenItemDisplayModel = sendtoFragment.n0;
            fs1.d(userTokenItemDisplayModel);
            double nativeBalance = userTokenItemDisplayModel.getNativeBalance();
            UserTokenItemDisplayModel userTokenItemDisplayModel2 = sendtoFragment.n0;
            fs1.d(userTokenItemDisplayModel2);
            e30.T(textView, nativeBalance, userTokenItemDisplayModel2.getSymbol());
        }
    }

    public final void O() {
        bb1 bb1Var = this.m0;
        if (bb1Var == null) {
            return;
        }
        fs1.d(bb1Var);
        MaterialCheckBox materialCheckBox = bb1Var.f;
        fs1.e(materialCheckBox, "binding!!.chkFullAmountRecipient");
        materialCheckBox.setVisibility(c0() ? 0 : 8);
        bb1 bb1Var2 = this.m0;
        fs1.d(bb1Var2);
        MaterialCheckBox materialCheckBox2 = bb1Var2.f;
        fs1.e(materialCheckBox2, "binding!!.chkFullAmountRecipient");
        cj4.j(materialCheckBox2, new SendtoFragment$bind$1(this));
        bb1 bb1Var3 = this.m0;
        fs1.d(bb1Var3);
        bb1Var3.h.setOnItemSelectedChangedListener(new b());
        bb1 bb1Var4 = this.m0;
        fs1.d(bb1Var4);
        bb1Var4.c.setOnClickListener(new View.OnClickListener() { // from class: ll3
            @Override // android.view.View.OnClickListener
            public final void onClick(View view) {
                SendtoFragment.P(SendtoFragment.this, view);
            }
        });
        bb1 bb1Var5 = this.m0;
        fs1.d(bb1Var5);
        bb1Var5.i.addTextChangedListener(new c());
        bb1 bb1Var6 = this.m0;
        fs1.d(bb1Var6);
        bb1Var6.k.setOnEditorActionListener(new TextView.OnEditorActionListener() { // from class: dl3
            @Override // android.widget.TextView.OnEditorActionListener
            public final boolean onEditorAction(TextView textView, int i, KeyEvent keyEvent) {
                boolean Q;
                Q = SendtoFragment.Q(SendtoFragment.this, textView, i, keyEvent);
                return Q;
            }
        });
        bb1 bb1Var7 = this.m0;
        fs1.d(bb1Var7);
        bb1Var7.g.setOnFocusChangeListener(new View.OnFocusChangeListener() { // from class: al3
            @Override // android.view.View.OnFocusChangeListener
            public final void onFocusChange(View view, boolean z) {
                SendtoFragment.R(SendtoFragment.this, view, z);
            }
        });
        bb1 bb1Var8 = this.m0;
        fs1.d(bb1Var8);
        bb1Var8.r.setText(u21.a.b());
        bb1 bb1Var9 = this.m0;
        fs1.d(bb1Var9);
        bb1Var9.r.setOnClickListener(new View.OnClickListener() { // from class: kl3
            @Override // android.view.View.OnClickListener
            public final void onClick(View view) {
                SendtoFragment.S(SendtoFragment.this, view);
            }
        });
    }

    public final void T() {
        CustomKeyBoard n;
        try {
            gm1 j = j();
            if (j != null && (n = j.n()) != null) {
                bb1 bb1Var = this.m0;
                fs1.d(bb1Var);
                TextInputEditText textInputEditText = bb1Var.j;
                fs1.e(textInputEditText, "binding!!.etAmount");
                n.v(textInputEditText);
            }
        } catch (Exception e2) {
            e2.printStackTrace();
        }
    }

    public final double U() {
        double c2;
        String obj;
        bb1 bb1Var = this.m0;
        fs1.d(bb1Var);
        Editable text = bb1Var.j.getText();
        String str = "";
        if (text != null && (obj = text.toString()) != null) {
            str = obj;
        }
        if (fs1.b(this.t0.getValue(), Boolean.TRUE)) {
            c2 = e30.K(str);
        } else {
            c2 = v21.c(e30.K(str)) / this.k0;
        }
        return X() - c2;
    }

    public final G2FAVerfication.b V() {
        return new d();
    }

    public final ClipboardManager W() {
        return this.l0;
    }

    public final double X() {
        double c2;
        String contractAddress;
        String obj;
        bb1 bb1Var = this.m0;
        fs1.d(bb1Var);
        Editable text = bb1Var.j.getText();
        String str = "";
        if (text != null && (obj = text.toString()) != null) {
            str = obj;
        }
        bb1 bb1Var2 = this.m0;
        fs1.d(bb1Var2);
        boolean isChecked = bb1Var2.f.isChecked();
        if (fs1.b(this.t0.getValue(), Boolean.TRUE)) {
            c2 = e30.K(str);
        } else {
            c2 = v21.c(e30.K(str)) / this.k0;
        }
        if (isChecked) {
            Map<String, Double> map = this.u0;
            UserTokenItemDisplayModel userTokenItemDisplayModel = this.n0;
            String str2 = null;
            if (userTokenItemDisplayModel != null && (contractAddress = userTokenItemDisplayModel.getContractAddress()) != null) {
                str2 = contractAddress.toLowerCase(Locale.ROOT);
                fs1.e(str2, "(this as java.lang.Strin….toLowerCase(Locale.ROOT)");
            }
            Double d2 = map.get(str2);
            return (c2 * 100.0d) / (100 - (d2 == null ? Utils.DOUBLE_EPSILON : d2.doubleValue()));
        }
        return c2;
    }

    /* JADX WARN: Code restructure failed: missing block: B:5:0x0029, code lost:
        if (defpackage.fs1.b(r1.getSymbol(), "ETH") != false) goto L8;
     */
    /*
        Code decompiled incorrectly, please refer to instructions dump.
        To view partially-correct code enable 'Show inconsistent code' option in preferences
    */
    public final defpackage.te4 Y() {
        /*
            r6 = this;
            net.safemoon.androidwallet.ui.displayModel.UserTokenItemDisplayModel r0 = r6.n0
            defpackage.fs1.d(r0)
            java.lang.String r0 = r0.getContractAddress()
            net.safemoon.androidwallet.ui.displayModel.UserTokenItemDisplayModel r1 = r6.n0
            defpackage.fs1.d(r1)
            java.lang.String r1 = r1.getSymbol()
            java.lang.String r2 = "BNB"
            boolean r1 = defpackage.fs1.b(r1, r2)
            if (r1 != 0) goto L2b
            net.safemoon.androidwallet.ui.displayModel.UserTokenItemDisplayModel r1 = r6.n0
            defpackage.fs1.d(r1)
            java.lang.String r1 = r1.getSymbol()
            java.lang.String r2 = "ETH"
            boolean r1 = defpackage.fs1.b(r1, r2)
            if (r1 == 0) goto Lbb
        L2b:
            net.safemoon.androidwallet.common.TokenType$a r1 = net.safemoon.androidwallet.common.TokenType.Companion
            androidx.fragment.app.FragmentActivity r2 = r6.requireActivity()
            android.app.Application r2 = r2.getApplication()
            net.safemoon.androidwallet.common.TokenType r3 = net.safemoon.androidwallet.common.TokenType.BEP_20
            java.lang.String r4 = r3.getName()
            java.lang.String r5 = "DEFAULT_GATEWAY"
            java.lang.String r2 = defpackage.bo3.j(r2, r5, r4)
            java.lang.String r4 = "getString(\n             …kenType.BEP_20.getName())"
            defpackage.fs1.e(r2, r4)
            net.safemoon.androidwallet.common.TokenType r1 = r1.c(r2)
            java.lang.String r2 = r1.getName()
            java.lang.String r3 = r3.getName()
            boolean r2 = defpackage.fs1.b(r2, r3)
            java.lang.String r3 = "swap.address"
            if (r2 == 0) goto L83
            net.safemoon.androidwallet.viewmodels.MyTokensListViewModel r1 = r6.i0
            defpackage.fs1.d(r1)
            java.util.ArrayList r1 = r1.y()
            java.util.Iterator r1 = r1.iterator()
        L67:
            boolean r2 = r1.hasNext()
            if (r2 == 0) goto Lbb
            java.lang.Object r2 = r1.next()
            net.safemoon.androidwallet.model.swap.Swap r2 = (net.safemoon.androidwallet.model.swap.Swap) r2
            java.lang.String r4 = r2.symbol
            java.lang.String r5 = "WBNB"
            boolean r4 = defpackage.fs1.b(r4, r5)
            if (r4 == 0) goto L67
            java.lang.String r0 = r2.address
            defpackage.fs1.e(r0, r3)
            goto Lbb
        L83:
            java.lang.String r1 = r1.getName()
            net.safemoon.androidwallet.common.TokenType r2 = net.safemoon.androidwallet.common.TokenType.ERC_20
            java.lang.String r2 = r2.getName()
            boolean r1 = defpackage.fs1.b(r1, r2)
            if (r1 == 0) goto Lbb
            net.safemoon.androidwallet.viewmodels.MyTokensListViewModel r1 = r6.i0
            defpackage.fs1.d(r1)
            java.util.ArrayList r1 = r1.y()
            java.util.Iterator r1 = r1.iterator()
        La0:
            boolean r2 = r1.hasNext()
            if (r2 == 0) goto Lbb
            java.lang.Object r2 = r1.next()
            net.safemoon.androidwallet.model.swap.Swap r2 = (net.safemoon.androidwallet.model.swap.Swap) r2
            java.lang.String r4 = r2.symbol
            java.lang.String r5 = "WETH"
            boolean r4 = defpackage.fs1.b(r4, r5)
            if (r4 == 0) goto La0
            java.lang.String r0 = r2.address
            defpackage.fs1.e(r0, r3)
        Lbb:
            e42 r1 = defpackage.a4.k()
            b30 r2 = defpackage.b30.a
            java.lang.String r2 = r2.m(r0)
            retrofit2.b r0 = r1.p(r0, r2)
            net.safemoon.androidwallet.fragments.SendtoFragment$g r1 = new net.safemoon.androidwallet.fragments.SendtoFragment$g
            r1.<init>()
            r0.n(r1)
            te4 r0 = defpackage.te4.a
            return r0
        */
        throw new UnsupportedOperationException("Method not decompiled: net.safemoon.androidwallet.fragments.SendtoFragment.Y():te4");
    }

    public final void Z() {
        bb1 bb1Var = this.m0;
        fs1.d(bb1Var);
        bb1Var.i.setText("");
    }

    public final void a0() {
        if (System.currentTimeMillis() - this.w0 >= 1000 && requireActivity().getApplicationContext() != null) {
            bb1 bb1Var = this.m0;
            fs1.d(bb1Var);
            bb1Var.i.setVisibility(8);
            bb1 bb1Var2 = this.m0;
            fs1.d(bb1Var2);
            bb1Var2.k.setVisibility(0);
            q60 q60Var = this.o0;
            if (q60Var != null) {
                fs1.d(q60Var);
                if (q60Var.h()) {
                    q60 q60Var2 = this.o0;
                    fs1.d(q60Var2);
                    q60Var2.g();
                    return;
                }
            }
            bb1 bb1Var3 = this.m0;
            fs1.d(bb1Var3);
            bb1Var3.c.setVisibility(8);
            q60 q60Var3 = q60.a;
            Context applicationContext = requireActivity().getApplicationContext();
            fs1.e(applicationContext, "requireActivity().applicationContext");
            List<IContact> list = this.s0;
            bb1 bb1Var4 = this.m0;
            fs1.d(bb1Var4);
            AppCompatEditText appCompatEditText = bb1Var4.k;
            fs1.e(appCompatEditText, "binding!!.etContactSearch");
            this.o0 = q60Var3.i(applicationContext, list, appCompatEditText, new e(), new PopupWindow.OnDismissListener() { // from class: cl3
                @Override // android.widget.PopupWindow.OnDismissListener
                public final void onDismiss() {
                    SendtoFragment.b0(SendtoFragment.this);
                }
            });
            bb1 bb1Var5 = this.m0;
            fs1.d(bb1Var5);
            pg4.g(bb1Var5.k);
        }
    }

    public final boolean c0() {
        return ((Boolean) this.v0.getValue()).booleanValue();
    }

    public final void d0() {
        if (this.q0 == null) {
            return;
        }
        this.t0.observe(getViewLifecycleOwner(), new tl2() { // from class: zk3
            @Override // defpackage.tl2
            public final void onChanged(Object obj) {
                SendtoFragment.e0(SendtoFragment.this, (Boolean) obj);
            }
        });
        ContactViewModel contactViewModel = this.q0;
        fs1.d(contactViewModel);
        contactViewModel.h().observe(getViewLifecycleOwner(), new tl2() { // from class: hl3
            @Override // defpackage.tl2
            public final void onChanged(Object obj) {
                SendtoFragment.f0(SendtoFragment.this, (s60) obj);
            }
        });
        ContactViewModel contactViewModel2 = this.q0;
        fs1.d(contactViewModel2);
        contactViewModel2.j().observe(getViewLifecycleOwner(), new tl2() { // from class: fl3
            @Override // defpackage.tl2
            public final void onChanged(Object obj) {
                SendtoFragment.g0(SendtoFragment.this, (List) obj);
            }
        });
        ContactViewModel contactViewModel3 = this.q0;
        fs1.d(contactViewModel3);
        contactViewModel3.n().observe(getViewLifecycleOwner(), new tl2() { // from class: gl3
            @Override // defpackage.tl2
            public final void onChanged(Object obj) {
                SendtoFragment.h0(SendtoFragment.this, (IContact) obj);
            }
        });
    }

    public final void n0() {
        String o;
        if (fs1.b(this.t0.getValue(), Boolean.TRUE)) {
            bb1 bb1Var = this.m0;
            fs1.d(bb1Var);
            if (!bb1Var.f.isChecked()) {
                bb1 bb1Var2 = this.m0;
                fs1.d(bb1Var2);
                o = String.valueOf(bb1Var2.j.getText());
                Context requireContext = requireContext();
                fs1.e(requireContext, "requireContext()");
                new AskAuthorizeProvider(requireContext, j()).a(new SendtoFragment$sendingFragment$1(this, o), SendtoFragment$sendingFragment$2.INSTANCE);
            }
        }
        o = e30.o(X(), 18, RoundingMode.HALF_UP, true);
        Context requireContext2 = requireContext();
        fs1.e(requireContext2, "requireContext()");
        new AskAuthorizeProvider(requireContext2, j()).a(new SendtoFragment$sendingFragment$1(this, o), SendtoFragment$sendingFragment$2.INSTANCE);
    }

    public final void o0(double d2) {
        this.k0 = d2;
    }

    @Override // android.view.View.OnClickListener
    public void onClick(View view) {
        hn2 a2;
        w7<Intent> b2;
        fs1.f(view, "v");
        switch (view.getId()) {
            case R.id.btnAddContact /* 2131362033 */:
                ce2 a3 = nl3.a();
                fs1.e(a3, "actionSendtoFragmentToManageContactsFragment()");
                g(a3);
                return;
            case R.id.btnSend /* 2131362092 */:
                bb1 bb1Var = this.m0;
                fs1.d(bb1Var);
                Editable text = bb1Var.j.getText();
                if (text != null) {
                    text.toString();
                }
                double X = X();
                String j = bo3.j(getActivity(), "SAFEMOON_ADDRESS", "");
                bb1 bb1Var2 = this.m0;
                fs1.d(bb1Var2);
                Editable text2 = bb1Var2.i.getText();
                if ((text2 != null ? text2 : "").length() == 0) {
                    Context requireContext = requireContext();
                    fs1.e(requireContext, "requireContext()");
                    e30.E(requireContext);
                    FragmentActivity requireActivity = requireActivity();
                    fs1.e(requireActivity, "requireActivity()");
                    e30.Z(requireActivity, R.string.fill_the_form);
                    return;
                }
                bb1 bb1Var3 = this.m0;
                fs1.d(bb1Var3);
                if (fs1.b(bb1Var3.i.getText().toString(), j)) {
                    bh.Y(new WeakReference(requireActivity()), Integer.valueOf((int) R.string.send_to_same_wallet_address_dialog_title), Integer.valueOf((int) R.string.send_to_same_wallet_address_dialog_content), R.string.send_to_same_wallet_address_dialog_action_ok, null, 16, null);
                    return;
                }
                bb1 bb1Var4 = this.m0;
                fs1.d(bb1Var4);
                if (!e30.I(bb1Var4.i.getText().toString())) {
                    Context requireContext2 = requireContext();
                    fs1.e(requireContext2, "requireContext()");
                    e30.E(requireContext2);
                    Context requireContext3 = requireContext();
                    fs1.e(requireContext3, "requireContext()");
                    e30.Z(requireContext3, R.string.send_to_invalid_address);
                    return;
                } else if (X > this.j0) {
                    Context requireContext4 = requireContext();
                    fs1.e(requireContext4, "requireContext()");
                    e30.E(requireContext4);
                    Context requireContext5 = requireContext();
                    fs1.e(requireContext5, "requireContext()");
                    e30.Z(requireContext5, R.string.send_to_amount_less_then_available);
                    return;
                } else if (X <= Utils.DOUBLE_EPSILON) {
                    Context requireContext6 = requireContext();
                    fs1.e(requireContext6, "requireContext()");
                    e30.Z(requireContext6, R.string.send_to_amount_more_then_zero);
                    return;
                } else {
                    FragmentActivity requireActivity2 = requireActivity();
                    fs1.e(requireActivity2, "requireActivity()");
                    bb1 bb1Var5 = this.m0;
                    fs1.d(bb1Var5);
                    bh.n0(requireActivity2, bb1Var5.i.getText().toString(), new View.OnClickListener() { // from class: il3
                        @Override // android.view.View.OnClickListener
                        public final void onClick(View view2) {
                            SendtoFragment.i0(SendtoFragment.this, view2);
                        }
                    }, ml3.a);
                    return;
                }
            case R.id.camera /* 2131362145 */:
                gm1 j2 = j();
                if (j2 == null || (a2 = j2.a()) == null || (b2 = a2.b(new SendtoFragment$onClick$3(this))) == null) {
                    return;
                }
                b2.a(new Intent(getActivity(), ScannedCodeActivity.class));
                return;
            case R.id.imgContacts /* 2131362589 */:
                a0();
                return;
            case R.id.imgWallets /* 2131362617 */:
                MultiWalletViewModel multiWalletViewModel = this.r0;
                if (multiWalletViewModel == null) {
                    return;
                }
                multiWalletViewModel.p(new SendtoFragment$onClick$4(this));
                return;
            case R.id.iv_back /* 2131362710 */:
                f();
                return;
            case R.id.tvHalf /* 2131363399 */:
                if (this.j0 <= Utils.DOUBLE_EPSILON || !fs1.b(this.t0.getValue(), Boolean.TRUE)) {
                    return;
                }
                bb1 bb1Var6 = this.m0;
                fs1.d(bb1Var6);
                bb1Var6.j.setText(e30.o(this.j0 * 0.5d, 0, RoundingMode.DOWN, false));
                bb1 bb1Var7 = this.m0;
                fs1.d(bb1Var7);
                TextInputEditText textInputEditText = bb1Var7.j;
                bb1 bb1Var8 = this.m0;
                fs1.d(bb1Var8);
                textInputEditText.setSelection(bb1Var8.j.length());
                pg4.f(requireContext());
                return;
            case R.id.tvMax /* 2131363410 */:
                if (this.j0 <= Utils.DOUBLE_EPSILON || !fs1.b(this.t0.getValue(), Boolean.TRUE)) {
                    return;
                }
                bb1 bb1Var9 = this.m0;
                fs1.d(bb1Var9);
                bb1Var9.j.setText(e30.o(this.j0, 0, RoundingMode.DOWN, false));
                bb1 bb1Var10 = this.m0;
                fs1.d(bb1Var10);
                TextInputEditText textInputEditText2 = bb1Var10.j;
                bb1 bb1Var11 = this.m0;
                fs1.d(bb1Var11);
                textInputEditText2.setSelection(bb1Var11.j.length());
                pg4.f(requireContext());
                return;
            case R.id.tvQuarter /* 2131363437 */:
                if (this.j0 <= Utils.DOUBLE_EPSILON || !fs1.b(this.t0.getValue(), Boolean.TRUE)) {
                    return;
                }
                bb1 bb1Var12 = this.m0;
                fs1.d(bb1Var12);
                bb1Var12.j.setText(e30.o(this.j0 * 0.25d, 0, RoundingMode.DOWN, false));
                bb1 bb1Var13 = this.m0;
                fs1.d(bb1Var13);
                TextInputEditText textInputEditText3 = bb1Var13.j;
                bb1 bb1Var14 = this.m0;
                fs1.d(bb1Var14);
                textInputEditText3.setSelection(bb1Var14.j.length());
                pg4.f(requireContext());
                return;
            default:
                return;
        }
    }

    @Override // androidx.fragment.app.Fragment
    public View onCreateView(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle) {
        fs1.f(layoutInflater, "inflater");
        View inflate = layoutInflater.inflate(R.layout.fragment_sendto, viewGroup, false);
        Object systemService = requireActivity().getSystemService("clipboard");
        Objects.requireNonNull(systemService, "null cannot be cast to non-null type android.content.ClipboardManager");
        this.l0 = (ClipboardManager) systemService;
        this.q0 = (ContactViewModel) new l(this, new MyViewModelFactory(new WeakReference(requireActivity()))).a(ContactViewModel.class);
        this.r0 = (MultiWalletViewModel) new l(this).a(MultiWalletViewModel.class);
        this.m0 = bb1.a(inflate);
        T();
        fs1.d(viewGroup);
        viewGroup.addOnLayoutChangeListener(new View.OnLayoutChangeListener() { // from class: bl3
            @Override // android.view.View.OnLayoutChangeListener
            public final void onLayoutChange(View view, int i, int i2, int i3, int i4, int i5, int i6, int i7, int i8) {
                SendtoFragment.k0(SendtoFragment.this, view, i, i2, i3, i4, i5, i6, i7, i8);
            }
        });
        Bundle arguments = getArguments();
        if (arguments != null) {
            UserTokenItemDisplayModel userTokenItemDisplayModel = (UserTokenItemDisplayModel) arguments.getSerializable(x0);
            this.n0 = userTokenItemDisplayModel;
            fs1.d(userTokenItemDisplayModel);
            this.j0 = userTokenItemDisplayModel.getNativeBalance();
            UserTokenItemDisplayModel userTokenItemDisplayModel2 = this.n0;
            fs1.d(userTokenItemDisplayModel2);
            this.k0 = userTokenItemDisplayModel2.getPriceInUsdt();
            bb1 bb1Var = this.m0;
            fs1.d(bb1Var);
            TextView textView = bb1Var.q;
            fs1.e(textView, "binding!!.tvBalance");
            UserTokenItemDisplayModel userTokenItemDisplayModel3 = this.n0;
            fs1.d(userTokenItemDisplayModel3);
            double nativeBalance = userTokenItemDisplayModel3.getNativeBalance();
            UserTokenItemDisplayModel userTokenItemDisplayModel4 = this.n0;
            fs1.d(userTokenItemDisplayModel4);
            e30.T(textView, nativeBalance, userTokenItemDisplayModel4.getSymbol());
        }
        bb1 bb1Var2 = this.m0;
        fs1.d(bb1Var2);
        bb1Var2.j.setFilters(new InputFilter[]{new xq1(Double.valueOf((double) Utils.DOUBLE_EPSILON), Double.valueOf(Double.MAX_VALUE))});
        bb1 bb1Var3 = this.m0;
        fs1.d(bb1Var3);
        TextInputEditText textInputEditText = bb1Var3.j;
        bb1 bb1Var4 = this.m0;
        fs1.d(bb1Var4);
        textInputEditText.addTextChangedListener(new f(bb1Var4.j));
        bb1 bb1Var5 = this.m0;
        fs1.d(bb1Var5);
        bb1Var5.j.setText("");
        bb1 bb1Var6 = this.m0;
        fs1.d(bb1Var6);
        bb1Var6.u.setOnClickListener(new View.OnClickListener() { // from class: jl3
            @Override // android.view.View.OnClickListener
            public final void onClick(View view) {
                SendtoFragment.l0(SendtoFragment.this, view);
            }
        });
        bb1 bb1Var7 = this.m0;
        fs1.d(bb1Var7);
        bb1Var7.e.setOnClickListener(this);
        bb1 bb1Var8 = this.m0;
        fs1.d(bb1Var8);
        bb1Var8.w.setOnClickListener(this);
        bb1 bb1Var9 = this.m0;
        fs1.d(bb1Var9);
        bb1Var9.s.setOnClickListener(this);
        bb1 bb1Var10 = this.m0;
        fs1.d(bb1Var10);
        bb1Var10.t.setOnClickListener(this);
        bb1 bb1Var11 = this.m0;
        fs1.d(bb1Var11);
        bb1Var11.d.setOnClickListener(this);
        bb1 bb1Var12 = this.m0;
        fs1.d(bb1Var12);
        bb1Var12.o.setOnClickListener(this);
        bb1 bb1Var13 = this.m0;
        fs1.d(bb1Var13);
        bb1Var13.b.setOnClickListener(this);
        bb1 bb1Var14 = this.m0;
        fs1.d(bb1Var14);
        bb1Var14.l.setOnClickListener(this);
        bb1 bb1Var15 = this.m0;
        fs1.d(bb1Var15);
        bb1Var15.n.setOnClickListener(this);
        bb1 bb1Var16 = this.m0;
        fs1.d(bb1Var16);
        bb1Var16.g.setOnClickListener(this);
        this.i0 = (MyTokensListViewModel) new l(requireActivity(), new MyViewModelFactory(new WeakReference(requireActivity()))).a(MyTokensListViewModel.class);
        bb1 bb1Var17 = this.m0;
        fs1.d(bb1Var17);
        TextView textView2 = bb1Var17.q;
        fs1.e(textView2, "binding!!.tvBalance");
        e30.X(textView2, new SendtoFragment$onCreateView$4(this));
        MyTokensListViewModel myTokensListViewModel = this.i0;
        fs1.d(myTokensListViewModel);
        myTokensListViewModel.B().observe(getViewLifecycleOwner(), new tl2() { // from class: el3
            @Override // defpackage.tl2
            public final void onChanged(Object obj) {
                SendtoFragment.m0(SendtoFragment.this, (Double) obj);
            }
        });
        Y();
        bb1 bb1Var18 = this.m0;
        fs1.d(bb1Var18);
        AppCompatImageView appCompatImageView = bb1Var18.m;
        fs1.e(appCompatImageView, "binding!!.imgSymbol");
        UserTokenItemDisplayModel userTokenItemDisplayModel5 = this.n0;
        fs1.d(userTokenItemDisplayModel5);
        int iconResId = userTokenItemDisplayModel5.getIconResId();
        UserTokenItemDisplayModel userTokenItemDisplayModel6 = this.n0;
        fs1.d(userTokenItemDisplayModel6);
        String iconFile = userTokenItemDisplayModel6.getIconFile();
        UserTokenItemDisplayModel userTokenItemDisplayModel7 = this.n0;
        fs1.d(userTokenItemDisplayModel7);
        e30.Q(appCompatImageView, iconResId, iconFile, userTokenItemDisplayModel7.getSymbol());
        UserTokenItemDisplayModel userTokenItemDisplayModel8 = this.n0;
        fs1.d(userTokenItemDisplayModel8);
        if (e30.H(userTokenItemDisplayModel8.getSymbol())) {
            bb1 bb1Var19 = this.m0;
            fs1.d(bb1Var19);
            AppCompatImageView appCompatImageView2 = bb1Var19.m;
            fs1.e(appCompatImageView2, "binding!!.imgSymbol");
            UserTokenItemDisplayModel userTokenItemDisplayModel9 = this.n0;
            fs1.d(userTokenItemDisplayModel9);
            String cmcId = userTokenItemDisplayModel9.getCmcId();
            UserTokenItemDisplayModel userTokenItemDisplayModel10 = this.n0;
            fs1.d(userTokenItemDisplayModel10);
            e30.P(appCompatImageView2, cmcId, userTokenItemDisplayModel10.getSymbol());
        }
        bb1 bb1Var20 = this.m0;
        fs1.d(bb1Var20);
        TextView textView3 = bb1Var20.x;
        UserTokenItemDisplayModel userTokenItemDisplayModel11 = this.n0;
        fs1.d(userTokenItemDisplayModel11);
        textView3.setText(userTokenItemDisplayModel11.getName());
        bb1 bb1Var21 = this.m0;
        fs1.d(bb1Var21);
        ScrollView b2 = bb1Var21.b();
        fs1.e(b2, "binding!!.root");
        return b2;
    }

    @Override // androidx.fragment.app.Fragment
    public void onPause() {
        super.onPause();
        q60 q60Var = this.o0;
        if (q60Var != null) {
            fs1.d(q60Var);
            if (q60Var.h()) {
                q60 q60Var2 = this.o0;
                fs1.d(q60Var2);
                q60Var2.g();
            }
        }
        qc qcVar = this.p0;
        if (qcVar != null) {
            fs1.d(qcVar);
            if (qcVar.e()) {
                qc qcVar2 = this.p0;
                fs1.d(qcVar2);
                qcVar2.d();
            }
        }
    }

    @Override // androidx.fragment.app.Fragment
    public void onResume() {
        super.onResume();
        bb1 bb1Var = this.m0;
        TextView textView = bb1Var == null ? null : bb1Var.r;
        if (textView == null) {
            return;
        }
        textView.setText(u21.a.b());
    }

    @Override // net.safemoon.androidwallet.fragments.common.BaseMainFragment, defpackage.qn, androidx.fragment.app.Fragment
    public void onViewCreated(View view, Bundle bundle) {
        fs1.f(view, "view");
        super.onViewCreated(view, bundle);
        d0();
        O();
    }

    public final void p0() {
        u21.a aVar;
        bb1 bb1Var = this.m0;
        fs1.d(bb1Var);
        String valueOf = String.valueOf(bb1Var.j.getText());
        if (valueOf.length() > 0) {
            try {
                bb1 bb1Var2 = this.m0;
                fs1.d(bb1Var2);
                bb1Var2.v.setVisibility(0);
                if (fs1.b(this.t0.getValue(), Boolean.TRUE)) {
                    String o = e30.o(v21.a(this.k0 * e30.K(valueOf)), 0, RoundingMode.DOWN, false);
                    String o2 = e30.o(v21.a(this.k0 * U()), 0, RoundingMode.DOWN, false);
                    StringBuilder sb = new StringBuilder();
                    sb.append("~ ");
                    sb.append(u21.a.b());
                    sb.append(' ');
                    sb.append(o);
                    String sb2 = sb.toString();
                    bb1 bb1Var3 = this.m0;
                    fs1.d(bb1Var3);
                    TextView textView = bb1Var3.v;
                    if (U() > Utils.DOUBLE_EPSILON) {
                        sb2 = sb2 + " + " + aVar.b() + ' ' + o2;
                    }
                    textView.setText(sb2);
                } else {
                    String o3 = e30.o(v21.c(e30.K(valueOf)) / this.k0, 18, RoundingMode.HALF_UP, true);
                    String o4 = e30.o(U(), 18, RoundingMode.HALF_UP, true);
                    StringBuilder sb3 = new StringBuilder();
                    sb3.append("~ ");
                    sb3.append(o3);
                    sb3.append(' ');
                    UserTokenItemDisplayModel userTokenItemDisplayModel = this.n0;
                    fs1.d(userTokenItemDisplayModel);
                    sb3.append(userTokenItemDisplayModel.getSymbol());
                    String sb4 = sb3.toString();
                    bb1 bb1Var4 = this.m0;
                    fs1.d(bb1Var4);
                    TextView textView2 = bb1Var4.v;
                    if (U() > Utils.DOUBLE_EPSILON) {
                        StringBuilder sb5 = new StringBuilder();
                        sb5.append(sb4);
                        sb5.append(" + ");
                        sb5.append(o4);
                        sb5.append(' ');
                        UserTokenItemDisplayModel userTokenItemDisplayModel2 = this.n0;
                        fs1.d(userTokenItemDisplayModel2);
                        sb5.append(userTokenItemDisplayModel2.getSymbol());
                        sb4 = sb5.toString();
                    }
                    textView2.setText(sb4);
                }
            } catch (Exception unused) {
            }
        } else {
            bb1 bb1Var5 = this.m0;
            fs1.d(bb1Var5);
            bb1Var5.v.setVisibility(8);
            bb1 bb1Var6 = this.m0;
            fs1.d(bb1Var6);
            bb1Var6.v.setText("~ " + u21.a.b() + ' ' + getString(R.string.send_to_0, Character.valueOf(b30.a.y())));
        }
        if (fs1.b(this.t0.getValue(), Boolean.TRUE)) {
            bb1 bb1Var7 = this.m0;
            fs1.d(bb1Var7);
            TextInputEditText textInputEditText = bb1Var7.j;
            lu3 lu3Var = lu3.a;
            Locale locale = Locale.getDefault();
            String string = getString(R.string.send_to_fragment_amount_format);
            fs1.e(string, "getString(R.string.send_to_fragment_amount_format)");
            UserTokenItemDisplayModel userTokenItemDisplayModel3 = this.n0;
            fs1.d(userTokenItemDisplayModel3);
            String format = String.format(locale, string, Arrays.copyOf(new Object[]{userTokenItemDisplayModel3.getName()}, 1));
            fs1.e(format, "java.lang.String.format(locale, format, *args)");
            textInputEditText.setHint(format);
            return;
        }
        bb1 bb1Var8 = this.m0;
        fs1.d(bb1Var8);
        TextInputEditText textInputEditText2 = bb1Var8.j;
        lu3 lu3Var2 = lu3.a;
        Locale locale2 = Locale.getDefault();
        String string2 = getString(R.string.send_to_fragment_amount_format);
        fs1.e(string2, "getString(R.string.send_to_fragment_amount_format)");
        String format2 = String.format(locale2, string2, Arrays.copyOf(new Object[]{u21.a.b()}, 1));
        fs1.e(format2, "java.lang.String.format(locale, format, *args)");
        textInputEditText2.setHint(format2);
    }
}
