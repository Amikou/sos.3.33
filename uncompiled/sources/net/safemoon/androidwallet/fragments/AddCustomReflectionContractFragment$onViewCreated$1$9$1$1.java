package net.safemoon.androidwallet.fragments;

import android.content.DialogInterface;
import androidx.fragment.app.FragmentActivity;
import kotlin.jvm.internal.Lambda;
import net.safemoon.androidwallet.R;
import net.safemoon.androidwallet.viewmodels.CustomReflectionContractTokenViewModel;

/* compiled from: AddCustomReflectionContractFragment.kt */
/* loaded from: classes2.dex */
public final class AddCustomReflectionContractFragment$onViewCreated$1$9$1$1 extends Lambda implements tc1<CustomReflectionContractTokenViewModel.SaveReturnCode, te4> {
    public final /* synthetic */ n91 $this_apply;
    public final /* synthetic */ AddCustomReflectionContractFragment this$0;

    /* compiled from: AddCustomReflectionContractFragment.kt */
    /* loaded from: classes2.dex */
    public /* synthetic */ class a {
        public static final /* synthetic */ int[] a;

        static {
            int[] iArr = new int[CustomReflectionContractTokenViewModel.SaveReturnCode.values().length];
            iArr[CustomReflectionContractTokenViewModel.SaveReturnCode.ERROR.ordinal()] = 1;
            a = iArr;
        }
    }

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public AddCustomReflectionContractFragment$onViewCreated$1$9$1$1(n91 n91Var, AddCustomReflectionContractFragment addCustomReflectionContractFragment) {
        super(1);
        this.$this_apply = n91Var;
        this.this$0 = addCustomReflectionContractFragment;
    }

    public static final void b(DialogInterface dialogInterface) {
    }

    @Override // defpackage.tc1
    public /* bridge */ /* synthetic */ te4 invoke(CustomReflectionContractTokenViewModel.SaveReturnCode saveReturnCode) {
        invoke2(saveReturnCode);
        return te4.a;
    }

    /* renamed from: invoke  reason: avoid collision after fix types in other method */
    public final void invoke2(CustomReflectionContractTokenViewModel.SaveReturnCode saveReturnCode) {
        this.$this_apply.b.setEnabled(true);
        if (saveReturnCode != null) {
            if (a.a[saveReturnCode.ordinal()] == 1) {
                FragmentActivity requireActivity = this.this$0.requireActivity();
                fs1.e(requireActivity, "requireActivity()");
                jc0.e(requireActivity, Integer.valueOf((int) R.string.warning_title), R.string.mr_error_initiate_wallet, false, e9.a, 8, null);
            }
        } else if (this.this$0.isAdded() && this.this$0.isVisible()) {
            this.this$0.f();
        }
    }
}
