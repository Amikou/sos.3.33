package net.safemoon.androidwallet.fragments;

import defpackage.bk3;
import kotlin.jvm.internal.Lambda;
import net.safemoon.androidwallet.ui.displayModel.UserTokenItemDisplayModel;

/* compiled from: SendFragment.kt */
/* loaded from: classes2.dex */
public final class SendFragment$onViewCreated$2 extends Lambda implements tc1<UserTokenItemDisplayModel, te4> {
    public final /* synthetic */ SendFragment this$0;

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public SendFragment$onViewCreated$2(SendFragment sendFragment) {
        super(1);
        this.this$0 = sendFragment;
    }

    @Override // defpackage.tc1
    public /* bridge */ /* synthetic */ te4 invoke(UserTokenItemDisplayModel userTokenItemDisplayModel) {
        invoke2(userTokenItemDisplayModel);
        return te4.a;
    }

    /* renamed from: invoke  reason: avoid collision after fix types in other method */
    public final void invoke2(UserTokenItemDisplayModel userTokenItemDisplayModel) {
        fs1.f(userTokenItemDisplayModel, "userTokenItemDisplayModel");
        if (um1.a(userTokenItemDisplayModel.getSymbolWithType()).b()) {
            SendFragment sendFragment = this.this$0;
            bk3.b a = bk3.a(userTokenItemDisplayModel);
            fs1.e(a, "actionSendFragmentToSend…serTokenItemDisplayModel)");
            sendFragment.g(a);
        }
    }
}
