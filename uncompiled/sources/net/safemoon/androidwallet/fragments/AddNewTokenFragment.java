package net.safemoon.androidwallet.fragments;

import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import androidx.fragment.app.FragmentActivity;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentViewModelLazyKt;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;
import com.google.android.material.textfield.TextInputEditText;
import defpackage.t9;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Comparator;
import java.util.List;
import java.util.Locale;
import java.util.Objects;
import net.safemoon.androidwallet.R;
import net.safemoon.androidwallet.adapter.touchHelper.RecyclerTouchListener;
import net.safemoon.androidwallet.common.TokenType;
import net.safemoon.androidwallet.dialogs.ProgressLoading;
import net.safemoon.androidwallet.fragments.AddNewTokenFragment;
import net.safemoon.androidwallet.fragments.common.BaseMainFragment;
import net.safemoon.androidwallet.viewmodels.AddNewTokensViewModel;
import net.safemoon.androidwallet.viewmodels.CustomContractTokenViewModel;
import net.safemoon.androidwallet.viewmodels.HomeViewModel;

/* compiled from: AddNewTokenFragment.kt */
/* loaded from: classes2.dex */
public final class AddNewTokenFragment extends BaseMainFragment {
    public final int i0;
    public p91 j0;
    public t9 l0;
    public RecyclerTouchListener m0;
    public ProgressLoading n0;
    public final sy1 k0 = FragmentViewModelLazyKt.a(this, d53.b(HomeViewModel.class), new AddNewTokenFragment$special$$inlined$activityViewModels$default$1(this), new AddNewTokenFragment$special$$inlined$activityViewModels$default$2(this));
    public final sy1 o0 = FragmentViewModelLazyKt.a(this, d53.b(AddNewTokensViewModel.class), new AddNewTokenFragment$special$$inlined$viewModels$default$2(new AddNewTokenFragment$special$$inlined$viewModels$default$1(this)), new AddNewTokenFragment$addNewTokensViewModel$2(this));
    public final sy1 p0 = FragmentViewModelLazyKt.a(this, d53.b(CustomContractTokenViewModel.class), new AddNewTokenFragment$special$$inlined$viewModels$default$4(new AddNewTokenFragment$special$$inlined$viewModels$default$3(this)), new AddNewTokenFragment$customContractTokenViewModel$2(this));

    /* compiled from: AddNewTokenFragment.kt */
    /* loaded from: classes2.dex */
    public static final class a implements t9.b {
        public a() {
        }

        @Override // defpackage.t9.b
        public void a(q9 q9Var, boolean z) {
            fs1.f(q9Var, "item");
            AddNewTokenFragment.this.y().m(q9Var, z);
        }
    }

    /* compiled from: Comparisons.kt */
    /* loaded from: classes2.dex */
    public static final class b<T> implements Comparator {
        @Override // java.util.Comparator
        public final int compare(T t, T t2) {
            String f = ((q9) t).f();
            Objects.requireNonNull(f, "null cannot be cast to non-null type java.lang.String");
            Locale locale = Locale.ROOT;
            String lowerCase = f.toLowerCase(locale);
            fs1.e(lowerCase, "(this as java.lang.Strin….toLowerCase(Locale.ROOT)");
            String f2 = ((q9) t2).f();
            Objects.requireNonNull(f2, "null cannot be cast to non-null type java.lang.String");
            String lowerCase2 = f2.toLowerCase(locale);
            fs1.e(lowerCase2, "(this as java.lang.Strin….toLowerCase(Locale.ROOT)");
            return l30.a(lowerCase, lowerCase2);
        }
    }

    /* compiled from: TextView.kt */
    /* loaded from: classes2.dex */
    public static final class c implements TextWatcher {
        public c() {
        }

        @Override // android.text.TextWatcher
        public void afterTextChanged(Editable editable) {
            AddNewTokenFragment.this.y().n(String.valueOf(editable));
        }

        @Override // android.text.TextWatcher
        public void beforeTextChanged(CharSequence charSequence, int i, int i2, int i3) {
        }

        @Override // android.text.TextWatcher
        public void onTextChanged(CharSequence charSequence, int i, int i2, int i3) {
        }
    }

    public static final void D(AddNewTokenFragment addNewTokenFragment, Boolean bool) {
        fs1.f(addNewTokenFragment, "this$0");
        fs1.e(bool, "it");
        if (bool.booleanValue()) {
            addNewTokenFragment.y().o();
        }
    }

    public static final void E(AddNewTokenFragment addNewTokenFragment, List list) {
        fs1.f(addNewTokenFragment, "this$0");
        ProgressLoading progressLoading = addNewTokenFragment.n0;
        if (progressLoading != null) {
            progressLoading.h();
        }
        fs1.e(list, "it");
        List<q9> e0 = j20.e0(list, new b());
        RecyclerTouchListener recyclerTouchListener = addNewTokenFragment.m0;
        if (recyclerTouchListener != null) {
            fs1.d(recyclerTouchListener);
            addNewTokenFragment.B(recyclerTouchListener, e0);
        }
        addNewTokenFragment.l0 = new t9(new ArrayList(e0), new a());
        p91 p91Var = addNewTokenFragment.j0;
        p91 p91Var2 = null;
        if (p91Var == null) {
            fs1.r("binding");
            p91Var = null;
        }
        p91Var.b.setAdapter(addNewTokenFragment.l0);
        if (e0.size() == addNewTokenFragment.i0) {
            p91 p91Var3 = addNewTokenFragment.j0;
            if (p91Var3 == null) {
                fs1.r("binding");
                p91Var3 = null;
            }
            Editable text = p91Var3.c.b.getText();
            if (!(text == null || text.length() == 0)) {
                p91 p91Var4 = addNewTokenFragment.j0;
                if (p91Var4 == null) {
                    fs1.r("binding");
                } else {
                    p91Var2 = p91Var4;
                }
                p91Var2.f.setVisibility(0);
                return;
            }
        }
        p91 p91Var5 = addNewTokenFragment.j0;
        if (p91Var5 == null) {
            fs1.r("binding");
        } else {
            p91Var2 = p91Var5;
        }
        p91Var2.f.setVisibility(8);
    }

    public static final void F(AddNewTokenFragment addNewTokenFragment, View view) {
        fs1.f(addNewTokenFragment, "this$0");
        ce2 a2 = p9.a();
        fs1.e(a2, "actionAddNewTokenFragmen…dCustomContractFragment()");
        addNewTokenFragment.g(a2);
    }

    public static final void G(AddNewTokenFragment addNewTokenFragment, TokenType tokenType) {
        fs1.f(addNewTokenFragment, "this$0");
        addNewTokenFragment.C();
        AddNewTokensViewModel y = addNewTokenFragment.y();
        fs1.e(tokenType, "it");
        y.l(tokenType);
        addNewTokenFragment.N();
        addNewTokenFragment.I();
    }

    public static final void J(AddNewTokenFragment addNewTokenFragment, int i, int i2) {
        ArrayList<q9> d;
        RecyclerTouchListener recyclerTouchListener;
        fs1.f(addNewTokenFragment, "this$0");
        if (i == R.id.btnDelete) {
            t9 t9Var = addNewTokenFragment.l0;
            q9 c2 = t9Var == null ? null : t9Var.c(i2);
            if (c2 != null && b30.a.s(c2.h())) {
                addNewTokenFragment.y().p(c2);
                addNewTokenFragment.y().m(c2, false);
                addNewTokenFragment.z().j(c2.h());
                t9 t9Var2 = addNewTokenFragment.l0;
                if (t9Var2 != null) {
                    t9Var2.removeItem(i2);
                }
                t9 t9Var3 = addNewTokenFragment.l0;
                if (t9Var3 == null || (d = t9Var3.d()) == null || (recyclerTouchListener = addNewTokenFragment.m0) == null) {
                    return;
                }
                fs1.d(recyclerTouchListener);
                addNewTokenFragment.B(recyclerTouchListener, d);
            }
        }
    }

    public static final void K(p91 p91Var, AddNewTokenFragment addNewTokenFragment) {
        fs1.f(p91Var, "$this_apply");
        fs1.f(addNewTokenFragment, "this$0");
        p91Var.d.setRefreshing(false);
        addNewTokenFragment.H();
    }

    public static final void L(AddNewTokenFragment addNewTokenFragment, View view) {
        fs1.f(addNewTokenFragment, "this$0");
        addNewTokenFragment.H();
    }

    public static final void M(AddNewTokenFragment addNewTokenFragment, View view) {
        fs1.f(addNewTokenFragment, "this$0");
        addNewTokenFragment.f();
    }

    public static final void O(AddNewTokenFragment addNewTokenFragment) {
        fs1.f(addNewTokenFragment, "this$0");
        ProgressLoading progressLoading = addNewTokenFragment.n0;
        if (progressLoading == null) {
            return;
        }
        progressLoading.h();
    }

    public final HomeViewModel A() {
        return (HomeViewModel) this.k0.getValue();
    }

    public final void B(RecyclerTouchListener recyclerTouchListener, List<q9> list) {
        ArrayList arrayList = new ArrayList();
        int size = list.size() - 1;
        if (size >= 0) {
            int i = 0;
            while (true) {
                int i2 = i + 1;
                if (!b30.a.s(list.get(i).h())) {
                    arrayList.add(Integer.valueOf(i));
                }
                if (i2 > size) {
                    break;
                }
                i = i2;
            }
        }
        if (arrayList.size() > this.i0) {
            Object[] array = arrayList.toArray(new Integer[0]);
            Objects.requireNonNull(array, "null cannot be cast to non-null type kotlin.Array<T>");
            Integer[] numArr = (Integer[]) array;
            recyclerTouchListener.z((Integer[]) Arrays.copyOf(numArr, numArr.length));
            return;
        }
        recyclerTouchListener.z(new Integer[0]);
    }

    public final void C() {
        A().o().observe(getViewLifecycleOwner(), new tl2() { // from class: g9
            @Override // defpackage.tl2
            public final void onChanged(Object obj) {
                AddNewTokenFragment.D(AddNewTokenFragment.this, (Boolean) obj);
            }
        });
        y().j().observe(getViewLifecycleOwner(), new tl2() { // from class: h9
            @Override // defpackage.tl2
            public final void onChanged(Object obj) {
                AddNewTokenFragment.E(AddNewTokenFragment.this, (List) obj);
            }
        });
    }

    public final void H() {
        y().o();
        N();
    }

    public final void I() {
        final p91 p91Var = this.j0;
        p91 p91Var2 = null;
        if (p91Var == null) {
            fs1.r("binding");
            p91Var = null;
        }
        p91Var.d.setOnRefreshListener(new SwipeRefreshLayout.j() { // from class: m9
            @Override // androidx.swiperefreshlayout.widget.SwipeRefreshLayout.j
            public final void onRefresh() {
                AddNewTokenFragment.K(p91.this, this);
            }
        });
        p91Var.e.d.setVisibility(0);
        p91Var.e.d.setOnClickListener(new View.OnClickListener() { // from class: l9
            @Override // android.view.View.OnClickListener
            public final void onClick(View view) {
                AddNewTokenFragment.L(AddNewTokenFragment.this, view);
            }
        });
        p91 p91Var3 = this.j0;
        if (p91Var3 == null) {
            fs1.r("binding");
            p91Var3 = null;
        }
        p91Var3.e.c.setOnClickListener(new View.OnClickListener() { // from class: k9
            @Override // android.view.View.OnClickListener
            public final void onClick(View view) {
                AddNewTokenFragment.M(AddNewTokenFragment.this, view);
            }
        });
        p91 p91Var4 = this.j0;
        if (p91Var4 == null) {
            fs1.r("binding");
            p91Var4 = null;
        }
        p91Var4.e.e.setText(getResources().getText(R.string.add_new_tokens_screen_title));
        p91 p91Var5 = this.j0;
        if (p91Var5 == null) {
            fs1.r("binding");
            p91Var5 = null;
        }
        p91Var5.c.b.setText("");
        p91 p91Var6 = this.j0;
        if (p91Var6 == null) {
            fs1.r("binding");
            p91Var6 = null;
        }
        TextInputEditText textInputEditText = p91Var6.c.b;
        fs1.e(textInputEditText, "binding.searchBar.etSearch");
        textInputEditText.addTextChangedListener(new c());
        FragmentActivity requireActivity = requireActivity();
        p91 p91Var7 = this.j0;
        if (p91Var7 == null) {
            fs1.r("binding");
            p91Var7 = null;
        }
        RecyclerTouchListener recyclerTouchListener = new RecyclerTouchListener(requireActivity, p91Var7.b);
        this.m0 = recyclerTouchListener;
        fs1.d(recyclerTouchListener);
        recyclerTouchListener.x(Integer.valueOf((int) R.id.btnDelete)).y(R.id.rowFG, R.id.rowBG, new RecyclerTouchListener.k() { // from class: o9
            @Override // net.safemoon.androidwallet.adapter.touchHelper.RecyclerTouchListener.k
            public final void a(int i, int i2) {
                AddNewTokenFragment.J(AddNewTokenFragment.this, i, i2);
            }
        });
        p91 p91Var8 = this.j0;
        if (p91Var8 == null) {
            fs1.r("binding");
        } else {
            p91Var2 = p91Var8;
        }
        RecyclerView recyclerView = p91Var2.b;
        RecyclerTouchListener recyclerTouchListener2 = this.m0;
        fs1.d(recyclerTouchListener2);
        recyclerView.k(recyclerTouchListener2);
    }

    public final void N() {
        ProgressLoading.a aVar = ProgressLoading.y0;
        String string = getString(R.string.loading);
        fs1.e(string, "getString(R.string.loading)");
        ProgressLoading a2 = aVar.a(false, string, "");
        this.n0 = a2;
        if (a2 != null) {
            FragmentManager childFragmentManager = getChildFragmentManager();
            fs1.e(childFragmentManager, "childFragmentManager");
            a2.A(childFragmentManager);
        }
        new Handler(Looper.getMainLooper()).postDelayed(new Runnable() { // from class: n9
            @Override // java.lang.Runnable
            public final void run() {
                AddNewTokenFragment.O(AddNewTokenFragment.this);
            }
        }, 2000L);
    }

    @Override // androidx.fragment.app.Fragment
    public View onCreateView(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle) {
        fs1.f(layoutInflater, "inflater");
        p91 a2 = p91.a(layoutInflater.inflate(R.layout.fragment_add_new_token, viewGroup, false));
        fs1.e(a2, "bind(\n            inflat…ntainer, false)\n        )");
        this.j0 = a2;
        if (a2 == null) {
            fs1.r("binding");
            a2 = null;
        }
        return a2.b();
    }

    @Override // net.safemoon.androidwallet.fragments.common.BaseMainFragment, defpackage.qn, androidx.fragment.app.Fragment
    public void onViewCreated(View view, Bundle bundle) {
        fs1.f(view, "view");
        super.onViewCreated(view, bundle);
        p91 p91Var = this.j0;
        if (p91Var == null) {
            fs1.r("binding");
            p91Var = null;
        }
        p91Var.e.b.setOnClickListener(new View.OnClickListener() { // from class: j9
            @Override // android.view.View.OnClickListener
            public final void onClick(View view2) {
                AddNewTokenFragment.F(AddNewTokenFragment.this, view2);
            }
        });
        A().l().observe(getViewLifecycleOwner(), new tl2() { // from class: i9
            @Override // defpackage.tl2
            public final void onChanged(Object obj) {
                AddNewTokenFragment.G(AddNewTokenFragment.this, (TokenType) obj);
            }
        });
    }

    public final AddNewTokensViewModel y() {
        return (AddNewTokensViewModel) this.o0.getValue();
    }

    public final CustomContractTokenViewModel z() {
        return (CustomContractTokenViewModel) this.p0.getValue();
    }
}
