package net.safemoon.androidwallet.fragments;

import java.util.ArrayList;
import java.util.List;
import kotlin.jvm.internal.Lambda;
import net.safemoon.androidwallet.model.MyTokenType;

/* compiled from: SelectChainFragment.kt */
/* loaded from: classes2.dex */
public final class SelectChainFragment$initView$1$2 extends Lambda implements rc1<te4> {
    public final /* synthetic */ ec3 $savedStateHandle;
    public final /* synthetic */ SelectChainFragment this$0;

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public SelectChainFragment$initView$1$2(SelectChainFragment selectChainFragment, ec3 ec3Var) {
        super(0);
        this.this$0 = selectChainFragment;
        this.$savedStateHandle = ec3Var;
    }

    @Override // defpackage.rc1
    public /* bridge */ /* synthetic */ te4 invoke() {
        invoke2();
        return te4.a;
    }

    @Override // defpackage.rc1
    /* renamed from: invoke  reason: avoid collision after fix types in other method */
    public final void invoke2() {
        ec3 ec3Var;
        List<MyTokenType> value = this.this$0.r().g().getValue();
        if (value == null || (ec3Var = this.$savedStateHandle) == null) {
            return;
        }
        ArrayList<MyTokenType> arrayList = new ArrayList();
        for (Object obj : value) {
            if (((MyTokenType) obj).isSelect()) {
                arrayList.add(obj);
            }
        }
        ArrayList arrayList2 = new ArrayList(c20.q(arrayList, 10));
        for (MyTokenType myTokenType : arrayList) {
            arrayList2.add(myTokenType.getTokenType());
        }
        ec3Var.e("RESULT_SELECTED_CHAIN", arrayList2);
    }
}
