package net.safemoon.androidwallet.fragments;

import android.os.Bundle;
import android.os.Parcelable;
import java.io.Serializable;
import java.util.HashMap;
import net.safemoon.androidwallet.R;
import net.safemoon.androidwallet.model.transaction.history.Result;

/* compiled from: SendingFragmentDirections.java */
/* loaded from: classes2.dex */
public class b {

    /* compiled from: SendingFragmentDirections.java */
    /* renamed from: net.safemoon.androidwallet.fragments.b$b  reason: collision with other inner class name */
    /* loaded from: classes2.dex */
    public static class C0212b implements ce2 {
        public final HashMap a;

        @Override // defpackage.ce2
        public Bundle a() {
            Bundle bundle = new Bundle();
            if (this.a.containsKey("result")) {
                Result result = (Result) this.a.get("result");
                if (!Parcelable.class.isAssignableFrom(Result.class) && result != null) {
                    if (Serializable.class.isAssignableFrom(Result.class)) {
                        bundle.putSerializable("result", (Serializable) Serializable.class.cast(result));
                    } else {
                        throw new UnsupportedOperationException(Result.class.getName() + " must implement Parcelable or Serializable or must be an Enum.");
                    }
                } else {
                    bundle.putParcelable("result", (Parcelable) Parcelable.class.cast(result));
                }
            }
            if (this.a.containsKey("tokenChainId")) {
                bundle.putInt("tokenChainId", ((Integer) this.a.get("tokenChainId")).intValue());
            }
            if (this.a.containsKey("isNewTransaction")) {
                bundle.putBoolean("isNewTransaction", ((Boolean) this.a.get("isNewTransaction")).booleanValue());
            }
            return bundle;
        }

        @Override // defpackage.ce2
        public int b() {
            return R.id.action_sendingFragment_to_transferDetailsFragmentStatus;
        }

        public boolean c() {
            return ((Boolean) this.a.get("isNewTransaction")).booleanValue();
        }

        public Result d() {
            return (Result) this.a.get("result");
        }

        public int e() {
            return ((Integer) this.a.get("tokenChainId")).intValue();
        }

        public boolean equals(Object obj) {
            if (this == obj) {
                return true;
            }
            if (obj == null || C0212b.class != obj.getClass()) {
                return false;
            }
            C0212b c0212b = (C0212b) obj;
            if (this.a.containsKey("result") != c0212b.a.containsKey("result")) {
                return false;
            }
            if (d() == null ? c0212b.d() == null : d().equals(c0212b.d())) {
                return this.a.containsKey("tokenChainId") == c0212b.a.containsKey("tokenChainId") && e() == c0212b.e() && this.a.containsKey("isNewTransaction") == c0212b.a.containsKey("isNewTransaction") && c() == c0212b.c() && b() == c0212b.b();
            }
            return false;
        }

        public int hashCode() {
            return (((((((d() != null ? d().hashCode() : 0) + 31) * 31) + e()) * 31) + (c() ? 1 : 0)) * 31) + b();
        }

        public String toString() {
            return "ActionSendingFragmentToTransferDetailsFragmentStatus(actionId=" + b() + "){result=" + d() + ", tokenChainId=" + e() + ", isNewTransaction=" + c() + "}";
        }

        public C0212b(Result result, int i, boolean z) {
            HashMap hashMap = new HashMap();
            this.a = hashMap;
            if (result != null) {
                hashMap.put("result", result);
                hashMap.put("tokenChainId", Integer.valueOf(i));
                hashMap.put("isNewTransaction", Boolean.valueOf(z));
                return;
            }
            throw new IllegalArgumentException("Argument \"result\" is marked as non-null but was passed a null value.");
        }
    }

    public static C0212b a(Result result, int i, boolean z) {
        return new C0212b(result, i, z);
    }
}
