package net.safemoon.androidwallet.fragments;

import androidx.navigation.NavController;
import kotlin.jvm.internal.Lambda;
import net.safemoon.androidwallet.common.TokenType;

/* compiled from: ChainNetworkFragment.kt */
/* loaded from: classes2.dex */
public final class ChainNetworkFragment$chainNetworkAdapter$2 extends Lambda implements rc1<cx> {
    public final /* synthetic */ ChainNetworkFragment this$0;

    /* compiled from: ChainNetworkFragment.kt */
    /* renamed from: net.safemoon.androidwallet.fragments.ChainNetworkFragment$chainNetworkAdapter$2$1  reason: invalid class name */
    /* loaded from: classes2.dex */
    public static final class AnonymousClass1 extends Lambda implements tc1<TokenType, te4> {
        public final /* synthetic */ ChainNetworkFragment this$0;

        /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
        public AnonymousClass1(ChainNetworkFragment chainNetworkFragment) {
            super(1);
            this.this$0 = chainNetworkFragment;
        }

        @Override // defpackage.tc1
        public /* bridge */ /* synthetic */ te4 invoke(TokenType tokenType) {
            invoke2(tokenType);
            return te4.a;
        }

        /* renamed from: invoke  reason: avoid collision after fix types in other method */
        public final void invoke2(TokenType tokenType) {
            NavController e;
            xd2 n;
            ec3 d;
            fs1.f(tokenType, "it");
            e = this.this$0.e();
            if (e != null && (n = e.n()) != null && (d = n.d()) != null) {
                d.e("bundle.TOKEN_CHAIN_KEY", Integer.valueOf(tokenType.getChainId()));
            }
            this.this$0.f();
        }
    }

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public ChainNetworkFragment$chainNetworkAdapter$2(ChainNetworkFragment chainNetworkFragment) {
        super(0);
        this.this$0 = chainNetworkFragment;
    }

    /* JADX WARN: Can't rename method to resolve collision */
    @Override // defpackage.rc1
    public final cx invoke() {
        return new cx(new AnonymousClass1(this.this$0));
    }
}
