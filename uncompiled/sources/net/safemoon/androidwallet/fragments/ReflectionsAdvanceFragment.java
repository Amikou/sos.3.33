package net.safemoon.androidwallet.fragments;

import android.content.Context;
import android.content.DialogInterface;
import android.content.res.Resources;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RadioGroup;
import android.widget.TextView;
import androidx.appcompat.widget.AppCompatImageView;
import androidx.appcompat.widget.AppCompatTextView;
import androidx.coordinatorlayout.widget.CoordinatorLayout;
import androidx.fragment.app.FragmentActivity;
import androidx.fragment.app.FragmentViewModelLazyKt;
import androidx.lifecycle.LiveData;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import com.github.mikephil.charting.components.AxisBase;
import com.github.mikephil.charting.components.XAxis;
import com.github.mikephil.charting.formatter.ValueFormatter;
import com.github.mikephil.charting.utils.Utils;
import com.google.android.material.appbar.AppBarLayout;
import defpackage.pe0;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import net.safemoon.androidwallet.R;
import net.safemoon.androidwallet.fragments.ReflectionsAdvanceFragment;
import net.safemoon.androidwallet.fragments.common.BaseMainFragment;
import net.safemoon.androidwallet.model.reflections.RoomReflectionsDataAndToken;
import net.safemoon.androidwallet.model.reflections.RoomReflectionsToken;
import net.safemoon.androidwallet.viewmodels.ReflectionTrackerViewModel;
import net.safemoon.androidwallet.views.TouchControlLineChart;
import net.safemoon.androidwallet.views.marker.ReflectionLineGraphMarkerView;

/* compiled from: ReflectionsAdvanceFragment.kt */
/* loaded from: classes2.dex */
public final class ReflectionsAdvanceFragment extends BaseMainFragment {
    public final float i0 = 2.0f;
    public final String j0 = "tokenName";
    public final String k0 = "MMMdd";
    public final String l0 = "MM/dd";
    public final String m0 = "hh:mmaa";
    public final String n0 = "yyyy";
    public final int o0 = 4;
    public final sy1 p0 = FragmentViewModelLazyKt.a(this, d53.b(ReflectionTrackerViewModel.class), new ReflectionsAdvanceFragment$special$$inlined$viewModels$default$2(new ReflectionsAdvanceFragment$special$$inlined$viewModels$default$1(this)), null);
    public final sy1 q0 = zy1.a(new ReflectionsAdvanceFragment$symbolWithType$2(this));
    public final sy1 r0 = zy1.a(new ReflectionsAdvanceFragment$binding$2(this));
    public final sy1 s0 = zy1.a(ReflectionsAdvanceFragment$reflectionDataAdapter$2.INSTANCE);

    /* compiled from: ReflectionsAdvanceFragment.kt */
    /* loaded from: classes2.dex */
    public final class a extends ValueFormatter {
        public final gb2<ReflectionTrackerViewModel.a> a;
        public final /* synthetic */ ReflectionsAdvanceFragment b;

        /* compiled from: ReflectionsAdvanceFragment.kt */
        /* renamed from: net.safemoon.androidwallet.fragments.ReflectionsAdvanceFragment$a$a  reason: collision with other inner class name */
        /* loaded from: classes2.dex */
        public /* synthetic */ class C0210a {
            public static final /* synthetic */ int[] a;

            static {
                int[] iArr = new int[ReflectionTrackerViewModel.TimeMode.values().length];
                iArr[ReflectionTrackerViewModel.TimeMode.DAILY.ordinal()] = 1;
                iArr[ReflectionTrackerViewModel.TimeMode.WEEKLY.ordinal()] = 2;
                iArr[ReflectionTrackerViewModel.TimeMode.MONTHLY.ordinal()] = 3;
                iArr[ReflectionTrackerViewModel.TimeMode.YEARLY.ordinal()] = 4;
                a = iArr;
            }
        }

        public a(ReflectionsAdvanceFragment reflectionsAdvanceFragment, gb2<ReflectionTrackerViewModel.a> gb2Var) {
            fs1.f(reflectionsAdvanceFragment, "this$0");
            fs1.f(gb2Var, "relfectionParam");
            this.b = reflectionsAdvanceFragment;
            this.a = gb2Var;
        }

        @Override // com.github.mikephil.charting.formatter.ValueFormatter
        public String getAxisLabel(float f, AxisBase axisBase) {
            String format;
            if (axisBase != null) {
                axisBase.setLabelCount(6);
            }
            Calendar calendar = Calendar.getInstance();
            calendar.setTime(new Date(f * 1000));
            if (this.a.getValue() != null) {
                ReflectionTrackerViewModel.a value = this.a.getValue();
                fs1.d(value);
                ReflectionsAdvanceFragment reflectionsAdvanceFragment = this.b;
                ReflectionTrackerViewModel.a aVar = value;
                int i = C0210a.a[aVar.b().ordinal()];
                if (i != 1) {
                    if (i == 2) {
                        StringBuilder sb = new StringBuilder();
                        sb.append(calendar.get(3));
                        sb.append('/');
                        sb.append(calendar.get(1));
                        format = sb.toString();
                    } else if (i == 3) {
                        StringBuilder sb2 = new StringBuilder();
                        sb2.append((Object) calendar.getDisplayName(2, 1, Locale.getDefault()));
                        sb2.append('/');
                        sb2.append(calendar.get(1));
                        format = sb2.toString();
                    } else if (i != 4) {
                        format = new SimpleDateFormat(reflectionsAdvanceFragment.k0, Locale.getDefault()).format(calendar.getTime());
                    } else {
                        format = new SimpleDateFormat(reflectionsAdvanceFragment.n0, Locale.getDefault()).format(calendar.getTime());
                    }
                } else if (aVar.c() == ReflectionTrackerViewModel.TimeSpan.LOW) {
                    if (axisBase != null) {
                        axisBase.setLabelCount(reflectionsAdvanceFragment.o0);
                    }
                    format = new SimpleDateFormat(reflectionsAdvanceFragment.m0, Locale.getDefault()).format(calendar.getTime());
                } else {
                    format = new SimpleDateFormat(reflectionsAdvanceFragment.l0, Locale.getDefault()).format(calendar.getTime());
                }
                fs1.e(format, "{\n                relfec…          }\n            }");
                return format;
            }
            String format2 = new SimpleDateFormat(this.b.k0, Locale.getDefault()).format(calendar.getTime());
            fs1.e(format2, "{\n                Simple…endar.time)\n            }");
            return format2;
        }
    }

    /* compiled from: ReflectionsAdvanceFragment.kt */
    /* loaded from: classes2.dex */
    public /* synthetic */ class b {
        public static final /* synthetic */ int[] a;

        static {
            int[] iArr = new int[ReflectionTrackerViewModel.TimeMode.values().length];
            iArr[ReflectionTrackerViewModel.TimeMode.DAILY.ordinal()] = 1;
            iArr[ReflectionTrackerViewModel.TimeMode.WEEKLY.ordinal()] = 2;
            iArr[ReflectionTrackerViewModel.TimeMode.MONTHLY.ordinal()] = 3;
            iArr[ReflectionTrackerViewModel.TimeMode.YEARLY.ordinal()] = 4;
            a = iArr;
        }
    }

    /* compiled from: ReflectionsAdvanceFragment.kt */
    /* loaded from: classes2.dex */
    public static final class c extends AppBarLayout.Behavior.a {
        public final /* synthetic */ boolean a;

        public c(boolean z) {
            this.a = z;
        }

        @Override // com.google.android.material.appbar.AppBarLayout.BaseBehavior.d
        public boolean a(AppBarLayout appBarLayout) {
            fs1.f(appBarLayout, "appBarLayout");
            return this.a;
        }
    }

    public static final void O(ReflectionsAdvanceFragment reflectionsAdvanceFragment, View view) {
        fs1.f(reflectionsAdvanceFragment, "this$0");
        reflectionsAdvanceFragment.f();
    }

    public static final void P(ReflectionsAdvanceFragment reflectionsAdvanceFragment, RadioGroup radioGroup, int i) {
        fs1.f(reflectionsAdvanceFragment, "this$0");
        switch (i) {
            case R.id.tm_daily /* 2131363327 */:
                reflectionsAdvanceFragment.L().E(ReflectionTrackerViewModel.TimeMode.DAILY);
                break;
            case R.id.tm_monthly /* 2131363328 */:
                reflectionsAdvanceFragment.L().E(ReflectionTrackerViewModel.TimeMode.MONTHLY);
                break;
            case R.id.tm_weekly /* 2131363329 */:
                reflectionsAdvanceFragment.L().E(ReflectionTrackerViewModel.TimeMode.WEEKLY);
                break;
            case R.id.tm_yearly /* 2131363330 */:
                reflectionsAdvanceFragment.L().E(ReflectionTrackerViewModel.TimeMode.YEARLY);
                break;
        }
        reflectionsAdvanceFragment.N();
    }

    public static final void Q(ReflectionsAdvanceFragment reflectionsAdvanceFragment, RadioGroup radioGroup, int i) {
        fs1.f(reflectionsAdvanceFragment, "this$0");
        switch (i) {
            case R.id.ts_max /* 2131363362 */:
                reflectionsAdvanceFragment.L().F(ReflectionTrackerViewModel.TimeSpan.HIGH);
                return;
            case R.id.ts_med /* 2131363363 */:
                reflectionsAdvanceFragment.L().F(ReflectionTrackerViewModel.TimeSpan.MEDIUM);
                return;
            case R.id.ts_min /* 2131363364 */:
                reflectionsAdvanceFragment.L().F(ReflectionTrackerViewModel.TimeSpan.LOW);
                return;
            default:
                return;
        }
    }

    public static final void R(ReflectionsAdvanceFragment reflectionsAdvanceFragment, View view) {
        fs1.f(reflectionsAdvanceFragment, "this$0");
        ReflectionTrackerViewModel L = reflectionsAdvanceFragment.L();
        String M = reflectionsAdvanceFragment.M();
        fs1.e(M, "symbolWithType");
        L.y(M);
    }

    public static final void S(ReflectionsAdvanceFragment reflectionsAdvanceFragment, View view) {
        fs1.f(reflectionsAdvanceFragment, "this$0");
        FragmentActivity requireActivity = reflectionsAdvanceFragment.requireActivity();
        fs1.e(requireActivity, "requireActivity()");
        jc0.e(requireActivity, Integer.valueOf((int) R.string.reflection_tracker_title), R.string.reflection_tracker_detail, false, n53.a, 8, null);
    }

    public static final void T(DialogInterface dialogInterface) {
    }

    public static final void U(sa1 sa1Var, RoomReflectionsToken roomReflectionsToken) {
        fs1.f(sa1Var, "$this_apply");
        if (roomReflectionsToken == null) {
            return;
        }
        yk1 yk1Var = sa1Var.b;
        ImageView imageView = yk1Var.c;
        fs1.e(imageView, "ivTokenIcon");
        Integer iconResId = roomReflectionsToken.getIconResId();
        e30.Q(imageView, iconResId == null ? 0 : iconResId.intValue(), roomReflectionsToken.getIconResName(), roomReflectionsToken.getSymbol());
        yk1Var.e.setText(roomReflectionsToken.getName());
        yk1Var.h.setText(roomReflectionsToken.getSymbol());
        double doubleValue = roomReflectionsToken.getDifferenceBalance().doubleValue();
        TextView textView = yk1Var.f;
        fs1.e(textView, "tvTokenNativeBalance");
        e30.R(textView, doubleValue);
        Double priceUsd = roomReflectionsToken.getPriceUsd();
        double doubleValue2 = priceUsd == null ? 0.0d : priceUsd.doubleValue();
        double d = doubleValue * doubleValue2;
        if (doubleValue2 > Utils.DOUBLE_EPSILON) {
            TextView textView2 = yk1Var.g;
            fs1.e(textView2, "tvTokenNativeBalanceInFiat");
            e30.N(textView2, d, true);
        } else {
            TextView textView3 = yk1Var.g;
            fs1.e(textView3, "tvTokenNativeBalanceInFiat");
            e30.R(textView3, doubleValue);
        }
        String displayDate = roomReflectionsToken.getDisplayDate();
        if (displayDate != null) {
            yk1Var.d.setText(displayDate);
        }
        yk1Var.b.setIconResource(!roomReflectionsToken.getEnableAdvanceMode() ? R.drawable.ic_baseline_add_24 : R.drawable.ic_baseline_remove_24);
        yk1Var.b.setVisibility(8);
    }

    public static final void V(ReflectionsAdvanceFragment reflectionsAdvanceFragment, Date date) {
        fs1.f(reflectionsAdvanceFragment, "this$0");
        sa1 J = reflectionsAdvanceFragment.J();
        if (J == null) {
            return;
        }
        AppCompatTextView appCompatTextView = J.l;
        fs1.e(appCompatTextView, "txtLoading");
        appCompatTextView.setVisibility(date != null ? 0 : 8);
        AppCompatImageView appCompatImageView = J.g.b;
        fs1.e(appCompatImageView, "toolbar.ivToolbarAction");
        appCompatImageView.setVisibility(date == null ? 0 : 8);
        if (date != null) {
            AppCompatTextView appCompatTextView2 = J.l;
            Resources resources = reflectionsAdvanceFragment.getResources();
            pe0.a aVar = pe0.a;
            Context requireContext = reflectionsAdvanceFragment.requireContext();
            fs1.e(requireContext, "requireContext()");
            appCompatTextView2.setText(resources.getString(R.string.reflection_data_for_date, aVar.b(requireContext).format(date)));
        }
    }

    public final void I(List<RoomReflectionsDataAndToken> list) {
        as.b(sz1.a(this), null, null, new ReflectionsAdvanceFragment$bindChart$1(this, list, null), 3, null);
    }

    public final sa1 J() {
        return (sa1) this.r0.getValue();
    }

    public final j53 K() {
        return (j53) this.s0.getValue();
    }

    public final ReflectionTrackerViewModel L() {
        return (ReflectionTrackerViewModel) this.p0.getValue();
    }

    public final String M() {
        return (String) this.q0.getValue();
    }

    public final void N() {
        sa1 J = J();
        if (J == null) {
            return;
        }
        ReflectionTrackerViewModel.a value = L().n().getValue();
        ReflectionTrackerViewModel.TimeMode b2 = value == null ? null : value.b();
        int i = b2 == null ? -1 : b.a[b2.ordinal()];
        if (i == 1) {
            J.k.setText(R.string.mr_seg_time_low_daily);
            J.j.setText(R.string.mr_seg_time_med_daily);
            J.i.setText(R.string.mr_seg_time_high_daily);
            J.f.setVisibility(0);
        } else if (i == 2) {
            J.k.setText(R.string.mr_seg_time_low_weekly);
            J.j.setText(R.string.mr_seg_time_med_weekly);
            J.i.setText(R.string.mr_seg_time_high_weekly);
            J.f.setVisibility(0);
        } else if (i != 3) {
            if (i != 4) {
                return;
            }
            J.f.setVisibility(8);
        } else {
            J.k.setText(R.string.mr_seg_time_low_monthly);
            J.j.setText(R.string.mr_seg_time_med_monthly);
            J.i.setText(R.string.mr_seg_time_high_monthly);
            J.f.setVisibility(0);
        }
    }

    public final void W(boolean z) {
        ViewGroup.LayoutParams layoutParams;
        CoordinatorLayout.Behavior f;
        sa1 J = J();
        if (J == null || (layoutParams = J.a.getLayoutParams()) == null || !(layoutParams instanceof CoordinatorLayout.e) || (f = ((CoordinatorLayout.e) layoutParams).f()) == null || !(f instanceof AppBarLayout.Behavior)) {
            return;
        }
        ((AppBarLayout.Behavior) f).H(new c(z));
    }

    @Override // androidx.fragment.app.Fragment
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        getArguments();
    }

    @Override // androidx.fragment.app.Fragment
    public View onCreateView(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle) {
        fs1.f(layoutInflater, "inflater");
        return layoutInflater.inflate(R.layout.fragment_reflections_advance, viewGroup, false);
    }

    @Override // net.safemoon.androidwallet.fragments.common.BaseMainFragment, defpackage.qn, androidx.fragment.app.Fragment
    public void onViewCreated(View view, Bundle bundle) {
        fs1.f(view, "view");
        super.onViewCreated(view, bundle);
        sn4 sn4Var = sn4.a;
        Context requireContext = requireContext();
        fs1.e(requireContext, "requireContext()");
        sn4Var.d(requireContext, "REFLECTION_INTRO", Boolean.FALSE);
        final sa1 J = J();
        if (J != null) {
            J.g.c.setOnClickListener(new View.OnClickListener() { // from class: o53
                @Override // android.view.View.OnClickListener
                public final void onClick(View view2) {
                    ReflectionsAdvanceFragment.O(ReflectionsAdvanceFragment.this, view2);
                }
            });
            J.g.b.setImageResource(R.drawable.ic_baseline_refresh_24);
            J.g.b.setOnClickListener(new View.OnClickListener() { // from class: q53
                @Override // android.view.View.OnClickListener
                public final void onClick(View view2) {
                    ReflectionsAdvanceFragment.R(ReflectionsAdvanceFragment.this, view2);
                }
            });
            J.g.e.setText(R.string.screen_title_my_reflections);
            AppCompatImageView appCompatImageView = J.g.a;
            fs1.e(appCompatImageView, "toolbar.imgInfo");
            appCompatImageView.setVisibility(0);
            J.g.a.setOnClickListener(new View.OnClickListener() { // from class: p53
                @Override // android.view.View.OnClickListener
                public final void onClick(View view2) {
                    ReflectionsAdvanceFragment.S(ReflectionsAdvanceFragment.this, view2);
                }
            });
            RecyclerView recyclerView = J.d;
            recyclerView.setLayoutManager(new LinearLayoutManager(requireContext(), 1, false));
            recyclerView.setAdapter(K());
            ReflectionTrackerViewModel L = L();
            String M = M();
            fs1.e(M, "symbolWithType");
            LiveData<RoomReflectionsToken> m = L.m(M);
            if (m != null) {
                m.observe(getViewLifecycleOwner(), new tl2() { // from class: l53
                    @Override // defpackage.tl2
                    public final void onChanged(Object obj) {
                        ReflectionsAdvanceFragment.U(sa1.this, (RoomReflectionsToken) obj);
                    }
                });
            }
            J.e.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() { // from class: s53
                @Override // android.widget.RadioGroup.OnCheckedChangeListener
                public final void onCheckedChanged(RadioGroup radioGroup, int i) {
                    ReflectionsAdvanceFragment.P(ReflectionsAdvanceFragment.this, radioGroup, i);
                }
            });
            J.f.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() { // from class: r53
                @Override // android.widget.RadioGroup.OnCheckedChangeListener
                public final void onCheckedChanged(RadioGroup radioGroup, int i) {
                    ReflectionsAdvanceFragment.Q(ReflectionsAdvanceFragment.this, radioGroup, i);
                }
            });
            J.e.check(R.id.tm_daily);
            J.f.check(R.id.ts_min);
            TouchControlLineChart touchControlLineChart = J.c;
            touchControlLineChart.setTouchEnabled(true);
            touchControlLineChart.setPinchZoom(false);
            touchControlLineChart.setDrawBorders(false);
            touchControlLineChart.setNoDataText(getString(R.string.chart_loading));
            ReflectionLineGraphMarkerView reflectionLineGraphMarkerView = new ReflectionLineGraphMarkerView(requireContext(), R.layout.marker_graph_view, "");
            reflectionLineGraphMarkerView.setChartView(J.c);
            te4 te4Var = te4.a;
            touchControlLineChart.setMarker(reflectionLineGraphMarkerView);
            touchControlLineChart.setNoDataTextColor(m70.d(requireContext(), R.color.white));
            touchControlLineChart.getLegend().setEnabled(false);
            touchControlLineChart.getXAxis().setPosition(XAxis.XAxisPosition.BOTTOM);
            touchControlLineChart.a(new ReflectionsAdvanceFragment$onViewCreated$1$8$2(this));
            xk1 xk1Var = J.h;
            xk1Var.c.setText("");
            xk1Var.d.setText("");
        }
        ReflectionTrackerViewModel L2 = L();
        String M2 = M();
        fs1.e(M2, "symbolWithType");
        L2.D(M2);
        sz1.a(this).b(new ReflectionsAdvanceFragment$onViewCreated$2(this, null));
        L().p().observe(getViewLifecycleOwner(), new tl2() { // from class: m53
            @Override // defpackage.tl2
            public final void onChanged(Object obj) {
                ReflectionsAdvanceFragment.V(ReflectionsAdvanceFragment.this, (Date) obj);
            }
        });
    }
}
