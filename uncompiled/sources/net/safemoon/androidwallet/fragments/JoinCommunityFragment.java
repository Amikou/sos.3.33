package net.safemoon.androidwallet.fragments;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import net.safemoon.androidwallet.R;
import net.safemoon.androidwallet.fragments.JoinCommunityFragment;
import net.safemoon.androidwallet.fragments.common.BaseMainFragment;

/* compiled from: JoinCommunityFragment.kt */
/* loaded from: classes2.dex */
public final class JoinCommunityFragment extends BaseMainFragment {
    public final String i0 = "https://safemoon.com/";
    public final String j0 = "https://twitter.com/safemoon";
    public final String k0 = "https://facebook.com/safemoonofficial";
    public final String l0 = "https://discord.gg/safemoon";
    public final String m0 = "https://www.reddit.com/r/SafeMoon/";
    public final String n0 = "https://www.instagram.com/safemoonhq/";
    public final String o0 = "https://www.linkedin.com/company/safemoon/";
    public final String p0 = "https://www.youtube.com/safemoonhq";
    public final String q0 = "com.twitter.android";
    public final String r0 = "com.facebook.katana";
    public final String s0 = "com.discord";
    public final String t0 = "com.reddit.frontpage";
    public final String u0 = "com.instagram.android";
    public final String v0 = "com.linkedin.android";
    public final String w0 = "com.google.android.youtube";
    public ja1 x0;

    public static final void N(JoinCommunityFragment joinCommunityFragment, View view) {
        fs1.f(joinCommunityFragment, "this$0");
        joinCommunityFragment.requireActivity().onBackPressed();
    }

    public static final void O(JoinCommunityFragment joinCommunityFragment, View view) {
        fs1.f(joinCommunityFragment, "this$0");
        joinCommunityFragment.Y(joinCommunityFragment.K());
    }

    public static final void P(JoinCommunityFragment joinCommunityFragment, View view) {
        fs1.f(joinCommunityFragment, "this$0");
        joinCommunityFragment.X(joinCommunityFragment.C(), joinCommunityFragment.J());
    }

    public static final void Q(JoinCommunityFragment joinCommunityFragment, View view) {
        fs1.f(joinCommunityFragment, "this$0");
        joinCommunityFragment.X(joinCommunityFragment.x(), joinCommunityFragment.E());
    }

    public static final void R(JoinCommunityFragment joinCommunityFragment, View view) {
        fs1.f(joinCommunityFragment, "this$0");
        joinCommunityFragment.X(joinCommunityFragment.y(), joinCommunityFragment.F());
    }

    public static final void S(JoinCommunityFragment joinCommunityFragment, View view) {
        fs1.f(joinCommunityFragment, "this$0");
        joinCommunityFragment.X(joinCommunityFragment.B(), joinCommunityFragment.I());
    }

    public static final void T(JoinCommunityFragment joinCommunityFragment, View view) {
        fs1.f(joinCommunityFragment, "this$0");
        joinCommunityFragment.X(joinCommunityFragment.z(), joinCommunityFragment.G());
    }

    public static final void U(JoinCommunityFragment joinCommunityFragment, View view) {
        fs1.f(joinCommunityFragment, "this$0");
        joinCommunityFragment.X(joinCommunityFragment.A(), joinCommunityFragment.H());
    }

    public static final void V(JoinCommunityFragment joinCommunityFragment, View view) {
        fs1.f(joinCommunityFragment, "this$0");
        joinCommunityFragment.X(joinCommunityFragment.D(), joinCommunityFragment.L());
    }

    public final String A() {
        return this.v0;
    }

    public final String B() {
        return this.t0;
    }

    public final String C() {
        return this.q0;
    }

    public final String D() {
        return this.w0;
    }

    public final String E() {
        return this.l0;
    }

    public final String F() {
        return this.k0;
    }

    public final String G() {
        return this.n0;
    }

    public final String H() {
        return this.o0;
    }

    public final String I() {
        return this.m0;
    }

    public final String J() {
        return this.j0;
    }

    public final String K() {
        return this.i0;
    }

    public final String L() {
        return this.p0;
    }

    public final boolean M(String str, Context context) {
        try {
            context.getPackageManager().getPackageInfo(str, 0);
            return true;
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
    }

    public final void W(String str) {
        Intent intent = new Intent("android.intent.action.VIEW", Uri.parse(str));
        intent.addFlags(268435456);
        startActivity(intent);
    }

    public final void X(String str, String str2) {
        Context requireContext = requireContext();
        fs1.e(requireContext, "requireContext()");
        if (M(str, requireContext)) {
            W(str2);
        } else {
            Y(str2);
        }
    }

    public final void Y(String str) {
        startActivity(new Intent("android.intent.action.VIEW", Uri.parse(str)));
    }

    @Override // androidx.fragment.app.Fragment
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
    }

    @Override // androidx.fragment.app.Fragment
    public View onCreateView(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle) {
        fs1.f(layoutInflater, "inflater");
        ja1 a = ja1.a(layoutInflater.inflate(R.layout.fragment_join_community, viewGroup, false));
        this.x0 = a;
        if (a == null) {
            return null;
        }
        return a.b();
    }

    @Override // net.safemoon.androidwallet.fragments.common.BaseMainFragment, defpackage.qn, androidx.fragment.app.Fragment
    public void onViewCreated(View view, Bundle bundle) {
        fs1.f(view, "view");
        super.onViewCreated(view, bundle);
        ja1 ja1Var = this.x0;
        if (ja1Var == null) {
            return;
        }
        rp3 rp3Var = ja1Var.j;
        rp3Var.a.setOnClickListener(new View.OnClickListener() { // from class: gu1
            @Override // android.view.View.OnClickListener
            public final void onClick(View view2) {
                JoinCommunityFragment.N(JoinCommunityFragment.this, view2);
            }
        });
        rp3Var.c.setText(R.string.join_community);
        ja1Var.h.setOnClickListener(new View.OnClickListener() { // from class: lu1
            @Override // android.view.View.OnClickListener
            public final void onClick(View view2) {
                JoinCommunityFragment.O(JoinCommunityFragment.this, view2);
            }
        });
        ja1Var.g.setOnClickListener(new View.OnClickListener() { // from class: mu1
            @Override // android.view.View.OnClickListener
            public final void onClick(View view2) {
                JoinCommunityFragment.P(JoinCommunityFragment.this, view2);
            }
        });
        ja1Var.b.setOnClickListener(new View.OnClickListener() { // from class: eu1
            @Override // android.view.View.OnClickListener
            public final void onClick(View view2) {
                JoinCommunityFragment.Q(JoinCommunityFragment.this, view2);
            }
        });
        ja1Var.c.setOnClickListener(new View.OnClickListener() { // from class: ku1
            @Override // android.view.View.OnClickListener
            public final void onClick(View view2) {
                JoinCommunityFragment.R(JoinCommunityFragment.this, view2);
            }
        });
        ja1Var.f.setOnClickListener(new View.OnClickListener() { // from class: fu1
            @Override // android.view.View.OnClickListener
            public final void onClick(View view2) {
                JoinCommunityFragment.S(JoinCommunityFragment.this, view2);
            }
        });
        ja1Var.d.setOnClickListener(new View.OnClickListener() { // from class: hu1
            @Override // android.view.View.OnClickListener
            public final void onClick(View view2) {
                JoinCommunityFragment.T(JoinCommunityFragment.this, view2);
            }
        });
        ja1Var.e.setOnClickListener(new View.OnClickListener() { // from class: ju1
            @Override // android.view.View.OnClickListener
            public final void onClick(View view2) {
                JoinCommunityFragment.U(JoinCommunityFragment.this, view2);
            }
        });
        ja1Var.i.setOnClickListener(new View.OnClickListener() { // from class: iu1
            @Override // android.view.View.OnClickListener
            public final void onClick(View view2) {
                JoinCommunityFragment.V(JoinCommunityFragment.this, view2);
            }
        });
    }

    public final String x() {
        return this.s0;
    }

    public final String y() {
        return this.r0;
    }

    public final String z() {
        return this.u0;
    }
}
