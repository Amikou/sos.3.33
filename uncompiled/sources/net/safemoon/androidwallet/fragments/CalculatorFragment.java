package net.safemoon.androidwallet.fragments;

import android.content.ClipData;
import android.content.ClipDescription;
import android.content.ClipboardManager;
import android.content.Context;
import android.content.res.Resources;
import android.graphics.Rect;
import android.os.Bundle;
import android.util.DisplayMetrics;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.PopupMenu;
import android.widget.TextView;
import androidx.appcompat.widget.AppCompatTextView;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.fragment.app.FragmentActivity;
import androidx.fragment.app.FragmentViewModelLazyKt;
import com.creageek.segmentedbutton.SegmentedButton;
import com.github.mikephil.charting.utils.Utils;
import com.google.android.material.button.MaterialButton;
import com.google.android.material.imageview.ShapeableImageView;
import java.math.BigDecimal;
import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.text.NumberFormat;
import java.text.ParseException;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;
import java.util.Objects;
import kotlin.Pair;
import kotlin.text.StringsKt__StringsKt;
import net.safemoon.androidwallet.R;
import net.safemoon.androidwallet.common.TokenType;
import net.safemoon.androidwallet.database.room.ApplicationRoomDatabase;
import net.safemoon.androidwallet.dialogs.AnchorCurrencyConverter;
import net.safemoon.androidwallet.dialogs.AnchorPopUpMyTokens;
import net.safemoon.androidwallet.fragments.CalculatorFragment;
import net.safemoon.androidwallet.fragments.CalculatorFragment$anchorFiatCurrency$2;
import net.safemoon.androidwallet.fragments.CalculatorFragment$defaultCurrencyAdapter$2;
import net.safemoon.androidwallet.fragments.common.BaseMainFragment;
import net.safemoon.androidwallet.model.fiat.gson.Fiat;
import net.safemoon.androidwallet.viewmodels.AddNewTokensViewModel;
import net.safemoon.androidwallet.viewmodels.CalculatorViewModel;
import net.safemoon.androidwallet.viewmodels.HomeViewModel;
import net.safemoon.androidwallet.viewmodels.SelectFiatViewModel;

/* compiled from: CalculatorFragment.kt */
/* loaded from: classes2.dex */
public final class CalculatorFragment extends BaseMainFragment implements View.OnClickListener {
    public r91 m0;
    public boolean y0;
    public final char i0 = '.';
    public final char j0 = ',';
    public final String k0 = "#,###.#####################";
    public final sy1 l0 = FragmentViewModelLazyKt.a(this, d53.b(CalculatorViewModel.class), new CalculatorFragment$special$$inlined$activityViewModels$default$1(this), new CalculatorFragment$special$$inlined$activityViewModels$default$2(this));
    public final DecimalFormatSymbols n0 = new DecimalFormatSymbols(Locale.getDefault());
    public AnchorPopUpMyTokens o0 = new AnchorPopUpMyTokens();
    public final sy1 p0 = zy1.a(new CalculatorFragment$anchorMoneyBag$2(this));
    public final sy1 q0 = zy1.a(new CalculatorFragment$anchorCurrencyConverter$2(this));
    public final sy1 r0 = FragmentViewModelLazyKt.a(this, d53.b(HomeViewModel.class), new CalculatorFragment$special$$inlined$activityViewModels$default$3(this), new CalculatorFragment$special$$inlined$activityViewModels$default$4(this));
    public final sy1 s0 = FragmentViewModelLazyKt.a(this, d53.b(AddNewTokensViewModel.class), new CalculatorFragment$special$$inlined$viewModels$default$2(new CalculatorFragment$special$$inlined$viewModels$default$1(this)), new CalculatorFragment$addNewTokensViewModel$2(this));
    public final sy1 t0 = FragmentViewModelLazyKt.a(this, d53.b(SelectFiatViewModel.class), new CalculatorFragment$special$$inlined$activityViewModels$1(this), new CalculatorFragment$selectFiatViewModel$2(this));
    public gb2<Fiat> u0 = new gb2<>(u21.a.a());
    public final sy1 v0 = zy1.a(new CalculatorFragment$defaultCurrencyAdapter$2(this));
    public final sy1 w0 = zy1.a(new CalculatorFragment$anchorFiatCurrency$2(this));
    public final DecimalFormat x0 = new DecimalFormat("#,###.#####################");
    public final View.OnLongClickListener z0 = new View.OnLongClickListener() { // from class: cu
        @Override // android.view.View.OnLongClickListener
        public final boolean onLongClick(View view) {
            boolean v0;
            v0 = CalculatorFragment.v0(CalculatorFragment.this, view);
            return v0;
        }
    };
    public final View.OnLongClickListener A0 = new View.OnLongClickListener() { // from class: du
        @Override // android.view.View.OnLongClickListener
        public final boolean onLongClick(View view) {
            boolean e0;
            e0 = CalculatorFragment.e0(CalculatorFragment.this, view);
            return e0;
        }
    };

    /* compiled from: CalculatorFragment.kt */
    /* loaded from: classes2.dex */
    public /* synthetic */ class a {
        public static final /* synthetic */ int[] a;

        static {
            int[] iArr = new int[TokenType.values().length];
            iArr[TokenType.BEP_20.ordinal()] = 1;
            iArr[TokenType.BEP_20_TEST.ordinal()] = 2;
            iArr[TokenType.ERC_20.ordinal()] = 3;
            iArr[TokenType.ERC_20_TEST.ordinal()] = 4;
            a = iArr;
        }
    }

    /* compiled from: View.kt */
    /* loaded from: classes2.dex */
    public static final class b implements Runnable {
        public final /* synthetic */ View a;
        public final /* synthetic */ CalculatorFragment f0;
        public final /* synthetic */ r91 g0;

        public b(View view, CalculatorFragment calculatorFragment, r91 r91Var) {
            this.a = view;
            this.f0 = calculatorFragment;
            this.g0 = r91Var;
        }

        @Override // java.lang.Runnable
        public final void run() {
            View l;
            View view = this.a;
            Rect rect = new Rect();
            gm1 j = this.f0.j();
            if (j != null && (l = j.l()) != null) {
                l.getLocalVisibleRect(rect);
            }
            ConstraintLayout constraintLayout = this.g0.z;
            ViewGroup.LayoutParams layoutParams = constraintLayout.getLayoutParams();
            layoutParams.height = (rect.bottom - rect.top) - view.getHeight();
            te4 te4Var = te4.a;
            constraintLayout.setLayoutParams(layoutParams);
        }
    }

    public static final void R(CalculatorFragment calculatorFragment, View view) {
        fs1.f(calculatorFragment, "this$0");
        AnchorCurrencyConverter X = calculatorFragment.X();
        fs1.e(view, "it");
        X.D(view);
        calculatorFragment.X().A(calculatorFragment.d0().k(""));
    }

    public static final void S(r91 r91Var, Fiat fiat) {
        fs1.f(r91Var, "$this_run");
        if (fiat == null) {
            return;
        }
        r91Var.D.d.setText(u21.a.c(fiat.getSymbol()));
    }

    public static final void T(CalculatorFragment calculatorFragment, View view) {
        fs1.f(calculatorFragment, "this$0");
        CalculatorFragment$anchorFiatCurrency$2.a Y = calculatorFragment.Y();
        fs1.e(view, "it");
        Y.i(view);
    }

    public static final boolean e0(CalculatorFragment calculatorFragment, View view) {
        fs1.f(calculatorFragment, "this$0");
        lc Z = calculatorFragment.Z();
        Context requireContext = calculatorFragment.requireContext();
        fs1.e(requireContext, "requireContext()");
        fs1.e(view, "v");
        r91 r91Var = calculatorFragment.m0;
        Z.k(requireContext, view, r91Var == null ? null : r91Var.b());
        return true;
    }

    public static final void f0(CalculatorFragment calculatorFragment, BigDecimal bigDecimal) {
        fs1.f(calculatorFragment, "this$0");
        if (bigDecimal == null || !calculatorFragment.P(bigDecimal)) {
            return;
        }
        r91 r91Var = calculatorFragment.m0;
        fs1.d(r91Var);
        r91Var.B.setText(calculatorFragment.x0.format(bigDecimal));
    }

    public static final void g0(CalculatorFragment calculatorFragment, TokenType tokenType) {
        String str;
        Object obj;
        boolean z;
        fs1.f(calculatorFragment, "this$0");
        Iterator<T> it = ApplicationRoomDatabase.n.d().iterator();
        while (true) {
            str = null;
            if (!it.hasNext()) {
                obj = null;
                break;
            }
            obj = it.next();
            if (((Pair) obj).getFirst() == tokenType) {
                z = true;
                continue;
            } else {
                z = false;
                continue;
            }
            if (z) {
                break;
            }
        }
        Pair pair = (Pair) obj;
        List list = pair == null ? null : (List) pair.getSecond();
        gb2<String> t = calculatorFragment.a0().t();
        int i = tokenType == null ? -1 : a.a[tokenType.ordinal()];
        if (i == 1 || i == 2 || i == 3 || i == 4) {
            if (list != null) {
                str = (String) list.get(1);
            }
        } else if (list != null) {
            str = (String) list.get(0);
        }
        t.postValue(str);
        tokenType.getSymbolWithType();
        AddNewTokensViewModel W = calculatorFragment.W();
        fs1.e(tokenType, "it");
        W.l(tokenType);
    }

    public static final void h0(CalculatorFragment calculatorFragment, String str) {
        fs1.f(calculatorFragment, "this$0");
        calculatorFragment.r0();
    }

    public static final void i0(CalculatorFragment calculatorFragment, List list) {
        fs1.f(calculatorFragment, "this$0");
        if (list != null) {
            calculatorFragment.o0.g(list);
        }
        calculatorFragment.r0();
    }

    public static final void j0(CalculatorFragment calculatorFragment, Integer num) {
        r91 r91Var;
        MaterialButton materialButton;
        MaterialButton materialButton2;
        fs1.f(calculatorFragment, "this$0");
        if (num != null && num.intValue() == R.id.btnBag) {
            r91 r91Var2 = calculatorFragment.m0;
            if (r91Var2 == null || (materialButton2 = r91Var2.m) == null) {
                return;
            }
            materialButton2.setIcon(g83.f(materialButton2.getResources(), R.drawable.ic_money_bag, null));
            materialButton2.setText("");
        } else if (num == null || num.intValue() != R.id.btnPercentageOfTraditional || (r91Var = calculatorFragment.m0) == null || (materialButton = r91Var.m) == null) {
        } else {
            materialButton.setIconResource(0);
            materialButton.setText("%");
        }
    }

    public static final void k0(CalculatorFragment calculatorFragment, List list) {
        fs1.f(calculatorFragment, "this$0");
        if (list == null) {
            return;
        }
        calculatorFragment.b0().d(calculatorFragment.d0().k(""));
    }

    public static final boolean l0(CalculatorFragment calculatorFragment, View view) {
        fs1.f(calculatorFragment, "this$0");
        fs1.f(view, "v");
        calculatorFragment.t0(view);
        return true;
    }

    public static final boolean m0(View view) {
        fs1.f(view, "v");
        return true;
    }

    public static final void n0(CalculatorFragment calculatorFragment, List list) {
        fs1.f(calculatorFragment, "this$0");
        fs1.f(list, "calcs");
        calculatorFragment.s0();
    }

    public static final void o0(CalculatorFragment calculatorFragment, String str) {
        fs1.f(calculatorFragment, "this$0");
        fs1.f(str, "charSequence");
        calculatorFragment.s0();
        try {
            if (str.length() == 0) {
                r91 r91Var = calculatorFragment.m0;
                fs1.d(r91Var);
                r91Var.B.setText("");
            } else {
                String v = e30.v(str);
                if (calculatorFragment.P(e30.L(v))) {
                    r91 r91Var2 = calculatorFragment.m0;
                    fs1.d(r91Var2);
                    r91Var2.B.setText(v);
                    r91 r91Var3 = calculatorFragment.m0;
                    fs1.d(r91Var3);
                    r91Var3.b.fullScroll(66);
                }
            }
        } catch (Exception unused) {
        }
    }

    public static final boolean u0(CharSequence charSequence, CalculatorFragment calculatorFragment, ClipboardManager clipboardManager, MenuItem menuItem) {
        fs1.f(charSequence, "$pasteData");
        fs1.f(calculatorFragment, "this$0");
        fs1.f(clipboardManager, "$clipboard");
        fs1.f(menuItem, "item");
        if (menuItem.getItemId() == R.id.item_paste) {
            try {
                Number parse = NumberFormat.getInstance().parse(charSequence.toString());
                CalculatorViewModel a0 = calculatorFragment.a0();
                fs1.d(a0);
                BigDecimal valueOf = BigDecimal.valueOf(parse.doubleValue());
                fs1.e(valueOf, "valueOf(number.toDouble())");
                a0.c(valueOf);
            } catch (ParseException e) {
                e.printStackTrace();
            }
        } else if (menuItem.getItemId() == R.id.item_copy) {
            r91 r91Var = calculatorFragment.m0;
            fs1.d(r91Var);
            CharSequence text = r91Var.B.getText();
            fs1.e(text, "binding!!.e2.text");
            if (StringsKt__StringsKt.K0(text).length() > 0) {
                r91 r91Var2 = calculatorFragment.m0;
                fs1.d(r91Var2);
                CharSequence text2 = r91Var2.B.getText();
                fs1.e(text2, "binding!!.e2.text");
                clipboardManager.setPrimaryClip(ClipData.newPlainText("label", StringsKt__StringsKt.K0(text2)));
            }
        }
        return false;
    }

    public static final boolean v0(CalculatorFragment calculatorFragment, View view) {
        fs1.f(calculatorFragment, "this$0");
        AnchorPopUpMyTokens anchorPopUpMyTokens = calculatorFragment.o0;
        Context requireContext = calculatorFragment.requireContext();
        fs1.e(requireContext, "requireContext()");
        fs1.e(view, "v");
        r91 r91Var = calculatorFragment.m0;
        anchorPopUpMyTokens.f(requireContext, view, r91Var == null ? null : r91Var.b(), new CalculatorFragment$switchTokenListener$1$1(calculatorFragment), calculatorFragment.a0().t().getValue(), hu.a);
        return true;
    }

    public static final void w0() {
    }

    public final boolean P(BigDecimal bigDecimal) {
        if (bigDecimal == null || bigDecimal.compareTo(new BigDecimal(999000000000000L)) > 0) {
            Context context = getContext();
            if (context != null) {
                e30.Z(context, R.string.dialog_alert_max_999);
            }
            return false;
        }
        return true;
    }

    public final void Q() {
        final r91 r91Var = this.m0;
        if (r91Var == null) {
            return;
        }
        r91Var.D.e.setChecked(true);
        r91Var.D.a.setVisibility(4);
        r91Var.D.i.setVisibility(4);
        r91Var.D.b.setVisibility(0);
        r91Var.D.b.setImageResource(R.drawable.convert_currency_icon);
        r91Var.D.b.setColorFilter(m70.d(requireContext(), R.color.white));
        r91Var.D.b.setOnClickListener(new View.OnClickListener() { // from class: bu
            @Override // android.view.View.OnClickListener
            public final void onClick(View view) {
                CalculatorFragment.R(CalculatorFragment.this, view);
            }
        });
        this.u0.observe(getViewLifecycleOwner(), new tl2() { // from class: au
            @Override // defpackage.tl2
            public final void onChanged(Object obj) {
                CalculatorFragment.S(r91.this, (Fiat) obj);
            }
        });
        r91Var.D.d.setTextColor(m70.d(requireContext(), R.color.white));
        TextView textView = r91Var.D.d;
        fs1.e(textView, "toolbar.leftText");
        textView.setVisibility(0);
        r91Var.D.d.setOnClickListener(new View.OnClickListener() { // from class: qu
            @Override // android.view.View.OnClickListener
            public final void onClick(View view) {
                CalculatorFragment.T(CalculatorFragment.this, view);
            }
        });
        r91Var.c.setOnClickListener(this);
        r91Var.d.setOnClickListener(this);
        r91Var.e.setOnClickListener(this);
        r91Var.f.setOnClickListener(this);
        r91Var.g.setOnClickListener(this);
        r91Var.h.setOnClickListener(this);
        r91Var.i.setOnClickListener(this);
        r91Var.j.setOnClickListener(this);
        r91Var.k.setOnClickListener(this);
        r91Var.l.setOnClickListener(this);
        r91Var.x.setOnClickListener(this);
        r91Var.v.setOnClickListener(this);
        r91Var.w.setOnClickListener(this);
        r91Var.q.setOnClickListener(this);
        r91Var.u.setOnClickListener(this);
        r91Var.n.setOnClickListener(this);
        r91Var.y.setOnClickListener(this);
        r91Var.t.setOnClickListener(this);
        r91Var.t.setOnLongClickListener(this.z0);
        r91Var.m.setOnClickListener(this);
        r91Var.m.setOnLongClickListener(this.A0);
        r91Var.o.setOnClickListener(this);
        r91Var.p.setOnClickListener(this);
        r91Var.r.setText(String.valueOf(b30.a.y()));
        r91Var.r.setOnClickListener(this);
        r91Var.s.setOnClickListener(this);
    }

    public final void U() {
        Resources resources;
        DisplayMetrics displayMetrics;
        wf wfVar;
        if (r44.b(Locale.getDefault()) == 1) {
            int i = Resources.getSystem().getDisplayMetrics().widthPixels;
            FragmentActivity activity = getActivity();
            Float valueOf = (activity == null || (resources = activity.getResources()) == null || (displayMetrics = resources.getDisplayMetrics()) == null) ? null : Float.valueOf(displayMetrics.density);
            fs1.d(valueOf);
            int b2 = i - t42.b(85 * valueOf.floatValue());
            r91 r91Var = this.m0;
            SegmentedButton segmentedButton = (r91Var == null || (wfVar = r91Var.D) == null) ? null : wfVar.h;
            ViewGroup.LayoutParams layoutParams = segmentedButton != null ? segmentedButton.getLayoutParams() : null;
            if (layoutParams != null) {
                layoutParams.width = b2;
            }
            if (segmentedButton == null) {
                return;
            }
            segmentedButton.setLayoutParams(layoutParams);
        }
    }

    public final void V() {
        Y().d();
    }

    public final AddNewTokensViewModel W() {
        return (AddNewTokensViewModel) this.s0.getValue();
    }

    public final AnchorCurrencyConverter X() {
        return (AnchorCurrencyConverter) this.q0.getValue();
    }

    public final CalculatorFragment$anchorFiatCurrency$2.a Y() {
        return (CalculatorFragment$anchorFiatCurrency$2.a) this.w0.getValue();
    }

    public final lc Z() {
        return (lc) this.p0.getValue();
    }

    public final CalculatorViewModel a0() {
        return (CalculatorViewModel) this.l0.getValue();
    }

    public final CalculatorFragment$defaultCurrencyAdapter$2.a b0() {
        return (CalculatorFragment$defaultCurrencyAdapter$2.a) this.v0.getValue();
    }

    public final HomeViewModel c0() {
        return (HomeViewModel) this.r0.getValue();
    }

    public final SelectFiatViewModel d0() {
        return (SelectFiatViewModel) this.t0.getValue();
    }

    @Override // android.view.View.OnClickListener
    public void onClick(View view) {
        fs1.f(view, "v");
        int id = view.getId();
        if (id == R.id.btnLatestPrice) {
            try {
                q0();
            } catch (Exception e) {
                e.printStackTrace();
            }
        } else if (id == R.id.btn_del) {
            a0().j();
        } else if (id != R.id.btn_divide) {
            switch (id) {
                case R.id.btn_0 /* 2131362104 */:
                    a0().B(0);
                    return;
                case R.id.btn_1 /* 2131362105 */:
                    a0().B(1);
                    return;
                case R.id.btn_2 /* 2131362106 */:
                    a0().B(2);
                    return;
                case R.id.btn_3 /* 2131362107 */:
                    a0().B(3);
                    return;
                case R.id.btn_4 /* 2131362108 */:
                    a0().B(4);
                    return;
                case R.id.btn_5 /* 2131362109 */:
                    a0().B(5);
                    return;
                case R.id.btn_6 /* 2131362110 */:
                    a0().B(6);
                    return;
                case R.id.btn_7 /* 2131362111 */:
                    a0().B(7);
                    return;
                case R.id.btn_8 /* 2131362112 */:
                    a0().B(8);
                    return;
                case R.id.btn_9 /* 2131362113 */:
                    a0().B(9);
                    return;
                case R.id.btn_bag /* 2131362114 */:
                    try {
                        Integer value = a0().s().getValue();
                        if (value != null) {
                            int intValue = value.intValue();
                            if (intValue == R.id.btnBag) {
                                p0();
                            } else if (intValue == R.id.btnPercentageOfTraditional) {
                                a0().g();
                            }
                        }
                        return;
                    } catch (Exception e2) {
                        e2.printStackTrace();
                        return;
                    }
                case R.id.btn_bil /* 2131362115 */:
                    CalculatorViewModel a0 = a0();
                    BigDecimal valueOf = BigDecimal.valueOf(1000000000L);
                    fs1.e(valueOf, "valueOf(1000000000L)");
                    a0.w(valueOf);
                    return;
                case R.id.btn_c /* 2131362116 */:
                    a0().i();
                    r91 r91Var = this.m0;
                    AppCompatTextView appCompatTextView = r91Var == null ? null : r91Var.B;
                    if (appCompatTextView == null) {
                        return;
                    }
                    appCompatTextView.setText("");
                    return;
                default:
                    switch (id) {
                        case R.id.btn_dot /* 2131362122 */:
                            a0().C();
                            return;
                        case R.id.btn_equal /* 2131362123 */:
                            a0().e();
                            return;
                        default:
                            switch (id) {
                                case R.id.btn_mil /* 2131362126 */:
                                    CalculatorViewModel a02 = a0();
                                    BigDecimal valueOf2 = BigDecimal.valueOf(1000000L);
                                    fs1.e(valueOf2, "valueOf(1000000L)");
                                    a02.w(valueOf2);
                                    return;
                                case R.id.btn_minus /* 2131362127 */:
                                    r91 r91Var2 = this.m0;
                                    fs1.d(r91Var2);
                                    r91Var2.B.setText("");
                                    a0().y(CalculatorViewModel.Operation.SUBTRACT);
                                    return;
                                default:
                                    switch (id) {
                                        case R.id.btn_multi /* 2131362129 */:
                                            r91 r91Var3 = this.m0;
                                            fs1.d(r91Var3);
                                            r91Var3.B.setText("");
                                            a0().y(CalculatorViewModel.Operation.MULTIPLY);
                                            return;
                                        case R.id.btn_plus /* 2131362130 */:
                                            r91 r91Var4 = this.m0;
                                            fs1.d(r91Var4);
                                            r91Var4.B.setText("");
                                            a0().y(CalculatorViewModel.Operation.ADD);
                                            return;
                                        case R.id.btn_ths /* 2131362131 */:
                                            CalculatorViewModel a03 = a0();
                                            BigDecimal valueOf3 = BigDecimal.valueOf(1000L);
                                            fs1.e(valueOf3, "valueOf(1000L)");
                                            a03.w(valueOf3);
                                            return;
                                        default:
                                            return;
                                    }
                            }
                    }
            }
        } else {
            r91 r91Var5 = this.m0;
            fs1.d(r91Var5);
            r91Var5.B.setText("");
            a0().y(CalculatorViewModel.Operation.DIVIDE);
        }
    }

    @Override // androidx.fragment.app.Fragment
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        this.n0.setDecimalSeparator(this.i0);
        this.n0.setGroupingSeparator(this.j0);
    }

    @Override // androidx.fragment.app.Fragment
    public View onCreateView(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle) {
        fs1.f(layoutInflater, "inflater");
        this.m0 = r91.a(layoutInflater.inflate(R.layout.fragment_calculator, viewGroup, false));
        requireActivity().getWindow().setBackgroundDrawableResource(R.color.light_grey);
        Q();
        U();
        r91 r91Var = this.m0;
        fs1.d(r91Var);
        return r91Var.b();
    }

    @Override // androidx.fragment.app.Fragment
    public void onPause() {
        super.onPause();
        AnchorPopUpMyTokens anchorPopUpMyTokens = this.o0;
        if (anchorPopUpMyTokens != null) {
            anchorPopUpMyTokens.d();
        }
    }

    @Override // net.safemoon.androidwallet.fragments.common.BaseMainFragment, defpackage.qn, androidx.fragment.app.Fragment
    public void onViewCreated(View view, Bundle bundle) {
        ShapeableImageView shapeableImageView;
        fs1.f(view, "view");
        super.onViewCreated(view, bundle);
        r91 r91Var = this.m0;
        fs1.d(r91Var);
        r91Var.B.setOnLongClickListener(new View.OnLongClickListener() { // from class: eu
            @Override // android.view.View.OnLongClickListener
            public final boolean onLongClick(View view2) {
                boolean l0;
                l0 = CalculatorFragment.l0(CalculatorFragment.this, view2);
                return l0;
            }
        });
        r91 r91Var2 = this.m0;
        fs1.d(r91Var2);
        r91Var2.A.setOnLongClickListener(fu.a);
        CalculatorViewModel a0 = a0();
        fs1.d(a0);
        h11<List<CalculatorViewModel.a>> n = a0.n();
        rz1 viewLifecycleOwner = getViewLifecycleOwner();
        fs1.e(viewLifecycleOwner, "viewLifecycleOwner");
        n.observe(viewLifecycleOwner, new tl2() { // from class: ou
            @Override // defpackage.tl2
            public final void onChanged(Object obj) {
                CalculatorFragment.n0(CalculatorFragment.this, (List) obj);
            }
        });
        a0().m().observe(getViewLifecycleOwner(), new tl2() { // from class: ju
            @Override // defpackage.tl2
            public final void onChanged(Object obj) {
                CalculatorFragment.o0(CalculatorFragment.this, (String) obj);
            }
        });
        a0().o().observe(getViewLifecycleOwner(), new tl2() { // from class: lu
            @Override // defpackage.tl2
            public final void onChanged(Object obj) {
                CalculatorFragment.f0(CalculatorFragment.this, (BigDecimal) obj);
            }
        });
        r91 r91Var3 = this.m0;
        if (r91Var3 != null && (shapeableImageView = r91Var3.t) != null) {
            shapeableImageView.setImageResource(c0().l().getValue() == TokenType.BEP_20 ? R.drawable.safemoon : R.drawable.p_safemoon);
        }
        c0().l().observe(getViewLifecycleOwner(), new tl2() { // from class: pu
            @Override // defpackage.tl2
            public final void onChanged(Object obj) {
                CalculatorFragment.g0(CalculatorFragment.this, (TokenType) obj);
            }
        });
        a0().t().observe(getViewLifecycleOwner(), new tl2() { // from class: ku
            @Override // defpackage.tl2
            public final void onChanged(Object obj) {
                CalculatorFragment.h0(CalculatorFragment.this, (String) obj);
            }
        });
        W().j().observe(getViewLifecycleOwner(), new tl2() { // from class: nu
            @Override // defpackage.tl2
            public final void onChanged(Object obj) {
                CalculatorFragment.i0(CalculatorFragment.this, (List) obj);
            }
        });
        a0().s().observe(getViewLifecycleOwner(), new tl2() { // from class: iu
            @Override // defpackage.tl2
            public final void onChanged(Object obj) {
                CalculatorFragment.j0(CalculatorFragment.this, (Integer) obj);
            }
        });
        r91 r91Var4 = this.m0;
        if (r91Var4 != null) {
            LinearLayout linearLayout = r91Var4.C;
            fs1.e(linearLayout, "editWrapper");
            fs1.e(vm2.a(linearLayout, new b(linearLayout, this, r91Var4)), "View.doOnPreDraw(\n    crossinline action: (view: View) -> Unit\n): OneShotPreDrawListener = OneShotPreDrawListener.add(this) { action(this) }");
        }
        d0().h().observe(getViewLifecycleOwner(), new tl2() { // from class: mu
            @Override // defpackage.tl2
            public final void onChanged(Object obj) {
                CalculatorFragment.k0(CalculatorFragment.this, (List) obj);
            }
        });
        r91 r91Var5 = this.m0;
        fs1.d(r91Var5);
        SegmentedButton segmentedButton = r91Var5.D.h;
        fs1.e(segmentedButton, "binding!!.toolbar.segmentedGroup");
        n(segmentedButton);
    }

    public final void p0() {
        a0().d(a0().q());
    }

    public final void q0() {
        double b2;
        CalculatorViewModel a0 = a0();
        Double r = a0().r();
        if (r == null) {
            b2 = Utils.DOUBLE_EPSILON;
        } else {
            double doubleValue = r.doubleValue();
            Fiat value = this.u0.getValue();
            fs1.d(value);
            fs1.e(value, "defaultFiat.value!!");
            b2 = v21.b(doubleValue, value);
        }
        BigDecimal valueOf = BigDecimal.valueOf(b2);
        fs1.e(valueOf, "valueOf(\n               …e!!) ?: 0.0\n            )");
        a0.d(valueOf);
    }

    public final void r0() {
        Object obj;
        q9 q9Var;
        ShapeableImageView shapeableImageView;
        ShapeableImageView shapeableImageView2;
        List<q9> value = W().j().getValue();
        if (value == null) {
            q9Var = null;
        } else {
            Iterator<T> it = value.iterator();
            while (true) {
                if (!it.hasNext()) {
                    obj = null;
                    break;
                }
                obj = it.next();
                if (fs1.b(((q9) obj).h(), a0().t().getValue())) {
                    break;
                }
            }
            q9Var = (q9) obj;
        }
        if (q9Var == null) {
            return;
        }
        r91 r91Var = this.m0;
        if (r91Var != null && (shapeableImageView2 = r91Var.t) != null) {
            e30.Q(shapeableImageView2, q9Var.e(), q9Var.d(), q9Var.g());
        }
        if (e30.H(q9Var.g())) {
            String c = kt.c(q9Var.a(), q9Var.g(), null, 2, null);
            r91 r91Var2 = this.m0;
            if (r91Var2 != null && (shapeableImageView = r91Var2.t) != null) {
                e30.P(shapeableImageView, c, q9Var.g());
            }
        }
        a0().l(q9Var, new CalculatorFragment$setSelectedTokenIcon$1$1(this));
        a0().k(q9Var);
    }

    public final void s0() {
        h11<List<CalculatorViewModel.a>> n;
        List<CalculatorViewModel.a> value;
        CalculatorViewModel a0 = a0();
        if (a0 != null && (n = a0.n()) != null && (value = n.getValue()) != null) {
            try {
                StringBuilder sb = new StringBuilder();
                for (CalculatorViewModel.a aVar : value) {
                    CalculatorViewModel.Type a2 = aVar.a();
                    CalculatorViewModel.Operation b2 = aVar.b();
                    BigDecimal c = aVar.c();
                    if (a2 == CalculatorViewModel.Type.Digit) {
                        sb.append(this.x0.format(c));
                    }
                    if (a2 == CalculatorViewModel.Type.Operation) {
                        fs1.d(b2);
                        sb.append(b2.getEquation());
                    }
                }
                r91 r91Var = this.m0;
                fs1.d(r91Var);
                r91Var.A.setText(sb.toString());
            } catch (Exception unused) {
            }
        }
    }

    public final void t0(View view) {
        Object systemService = requireContext().getSystemService("clipboard");
        Objects.requireNonNull(systemService, "null cannot be cast to non-null type android.content.ClipboardManager");
        final ClipboardManager clipboardManager = (ClipboardManager) systemService;
        ClipData primaryClip = clipboardManager.getPrimaryClip();
        ClipData.Item itemAt = primaryClip == null ? null : primaryClip.getItemAt(0);
        final CharSequence text = itemAt != null ? itemAt.getText() : null;
        if (text == null) {
            return;
        }
        PopupMenu popupMenu = new PopupMenu(requireContext(), view);
        popupMenu.getMenuInflater().inflate(R.menu.popup_paste_menu, popupMenu.getMenu());
        MenuItem findItem = popupMenu.getMenu().findItem(R.id.item_paste);
        popupMenu.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() { // from class: gu
            @Override // android.widget.PopupMenu.OnMenuItemClickListener
            public final boolean onMenuItemClick(MenuItem menuItem) {
                boolean u0;
                u0 = CalculatorFragment.u0(text, this, clipboardManager, menuItem);
                return u0;
            }
        });
        if (!clipboardManager.hasPrimaryClip()) {
            findItem.setEnabled(false);
        } else {
            ClipDescription primaryClipDescription = clipboardManager.getPrimaryClipDescription();
            fs1.d(primaryClipDescription);
            findItem.setEnabled(primaryClipDescription.hasMimeType("text/plain"));
        }
        popupMenu.show();
    }
}
