package net.safemoon.androidwallet.fragments;

import defpackage.th2;
import kotlin.jvm.internal.Lambda;
import net.safemoon.androidwallet.model.priceAlert.PAToken;

/* compiled from: NotificationFragment.kt */
/* loaded from: classes2.dex */
public final class NotificationFragment$paTokenAdapter$2 extends Lambda implements rc1<ho2> {
    public final /* synthetic */ NotificationFragment this$0;

    /* compiled from: NotificationFragment.kt */
    /* renamed from: net.safemoon.androidwallet.fragments.NotificationFragment$paTokenAdapter$2$1  reason: invalid class name */
    /* loaded from: classes2.dex */
    public static final class AnonymousClass1 extends Lambda implements tc1<PAToken, te4> {
        public final /* synthetic */ NotificationFragment this$0;

        /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
        public AnonymousClass1(NotificationFragment notificationFragment) {
            super(1);
            this.this$0 = notificationFragment;
        }

        @Override // defpackage.tc1
        public /* bridge */ /* synthetic */ te4 invoke(PAToken pAToken) {
            invoke2(pAToken);
            return te4.a;
        }

        /* renamed from: invoke  reason: avoid collision after fix types in other method */
        public final void invoke2(PAToken pAToken) {
            fs1.f(pAToken, "it");
            NotificationFragment notificationFragment = this.this$0;
            th2.b a = th2.a(pAToken);
            fs1.e(a, "actionToCryptoPriceAlertFragment(it)");
            notificationFragment.g(a);
        }
    }

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public NotificationFragment$paTokenAdapter$2(NotificationFragment notificationFragment) {
        super(0);
        this.this$0 = notificationFragment;
    }

    /* JADX WARN: Can't rename method to resolve collision */
    @Override // defpackage.rc1
    public final ho2 invoke() {
        return new ho2(new AnonymousClass1(this.this$0));
    }
}
