package net.safemoon.androidwallet.fragments;

import androidx.fragment.app.FragmentActivity;
import java.util.Locale;
import java.util.Objects;
import kotlin.jvm.internal.Lambda;
import net.safemoon.androidwallet.activity.AKTHomeActivity;
import net.safemoon.androidwallet.model.wallets.Wallet;
import net.safemoon.androidwallet.viewmodels.MultiWalletViewModel;

/* compiled from: SwitchWalletFragment.kt */
/* loaded from: classes2.dex */
public final class SwitchWalletFragment$setupAdapter$1 extends Lambda implements tc1<Wallet, te4> {
    public final /* synthetic */ SwitchWalletFragment this$0;

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public SwitchWalletFragment$setupAdapter$1(SwitchWalletFragment switchWalletFragment) {
        super(1);
        this.this$0 = switchWalletFragment;
    }

    @Override // defpackage.tc1
    public /* bridge */ /* synthetic */ te4 invoke(Wallet wallet2) {
        invoke2(wallet2);
        return te4.a;
    }

    /* renamed from: invoke  reason: avoid collision after fix types in other method */
    public final void invoke2(Wallet wallet2) {
        MultiWalletViewModel e0;
        String address;
        fs1.f(wallet2, "wallet");
        String address2 = wallet2.getAddress();
        Objects.requireNonNull(address2, "null cannot be cast to non-null type java.lang.String");
        Locale locale = Locale.ROOT;
        String lowerCase = address2.toLowerCase(locale);
        fs1.e(lowerCase, "(this as java.lang.Strin….toLowerCase(Locale.ROOT)");
        FragmentActivity requireActivity = this.this$0.requireActivity();
        fs1.e(requireActivity, "requireActivity()");
        Wallet c = e30.c(requireActivity);
        String str = null;
        if (c != null && (address = c.getAddress()) != null) {
            str = address.toLowerCase(locale);
            fs1.e(str, "(this as java.lang.Strin….toLowerCase(Locale.ROOT)");
        }
        if (fs1.b(lowerCase, str)) {
            return;
        }
        e0 = this.this$0.e0();
        e0.A(wallet2);
        AKTHomeActivity.R0(this.this$0.requireActivity());
    }
}
