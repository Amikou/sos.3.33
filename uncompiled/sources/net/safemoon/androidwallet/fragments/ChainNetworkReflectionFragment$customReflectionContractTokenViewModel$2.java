package net.safemoon.androidwallet.fragments;

import androidx.fragment.app.Fragment;
import kotlin.jvm.internal.Lambda;

/* compiled from: ChainNetworkReflectionFragment.kt */
/* loaded from: classes2.dex */
public final class ChainNetworkReflectionFragment$customReflectionContractTokenViewModel$2 extends Lambda implements rc1<hj4> {
    public final /* synthetic */ ChainNetworkReflectionFragment this$0;

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public ChainNetworkReflectionFragment$customReflectionContractTokenViewModel$2(ChainNetworkReflectionFragment chainNetworkReflectionFragment) {
        super(0);
        this.this$0 = chainNetworkReflectionFragment;
    }

    /* JADX WARN: Can't rename method to resolve collision */
    @Override // defpackage.rc1
    public final hj4 invoke() {
        Fragment requireParentFragment = this.this$0.requireParentFragment();
        fs1.e(requireParentFragment, "requireParentFragment()");
        return requireParentFragment;
    }
}
