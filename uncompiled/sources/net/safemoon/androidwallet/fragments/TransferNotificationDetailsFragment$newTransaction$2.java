package net.safemoon.androidwallet.fragments;

import kotlin.jvm.internal.Lambda;

/* compiled from: TransferNotificationDetailsFragment.kt */
/* loaded from: classes2.dex */
public final class TransferNotificationDetailsFragment$newTransaction$2 extends Lambda implements rc1<Boolean> {
    public final /* synthetic */ TransferNotificationDetailsFragment this$0;

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public TransferNotificationDetailsFragment$newTransaction$2(TransferNotificationDetailsFragment transferNotificationDetailsFragment) {
        super(0);
        this.this$0 = transferNotificationDetailsFragment;
    }

    /* JADX WARN: Can't rename method to resolve collision */
    @Override // defpackage.rc1
    public final Boolean invoke() {
        return Boolean.valueOf(this.this$0.requireArguments().getBoolean("isNewTransaction"));
    }
}
