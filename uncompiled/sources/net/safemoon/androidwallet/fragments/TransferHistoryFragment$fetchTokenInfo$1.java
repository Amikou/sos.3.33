package net.safemoon.androidwallet.fragments;

import kotlin.coroutines.jvm.internal.SuspendLambda;
import net.safemoon.androidwallet.ui.displayModel.UserTokenItemDisplayModel;

/* compiled from: TransferHistoryFragment.kt */
@kotlin.coroutines.jvm.internal.a(c = "net.safemoon.androidwallet.fragments.TransferHistoryFragment$fetchTokenInfo$1", f = "TransferHistoryFragment.kt", l = {238}, m = "invokeSuspend")
/* loaded from: classes2.dex */
public final class TransferHistoryFragment$fetchTokenInfo$1 extends SuspendLambda implements hd1<c90, q70<? super te4>, Object> {
    public final /* synthetic */ UserTokenItemDisplayModel $token;
    public Object L$0;
    public Object L$1;
    public int label;
    public final /* synthetic */ TransferHistoryFragment this$0;

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public TransferHistoryFragment$fetchTokenInfo$1(UserTokenItemDisplayModel userTokenItemDisplayModel, TransferHistoryFragment transferHistoryFragment, q70<? super TransferHistoryFragment$fetchTokenInfo$1> q70Var) {
        super(2, q70Var);
        this.$token = userTokenItemDisplayModel;
        this.this$0 = transferHistoryFragment;
    }

    @Override // kotlin.coroutines.jvm.internal.BaseContinuationImpl
    public final q70<te4> create(Object obj, q70<?> q70Var) {
        return new TransferHistoryFragment$fetchTokenInfo$1(this.$token, this.this$0, q70Var);
    }

    @Override // defpackage.hd1
    public final Object invoke(c90 c90Var, q70<? super te4> q70Var) {
        return ((TransferHistoryFragment$fetchTokenInfo$1) create(c90Var, q70Var)).invokeSuspend(te4.a);
    }

    /* JADX WARN: Removed duplicated region for block: B:32:0x007b A[Catch: Exception -> 0x00cc, TryCatch #2 {Exception -> 0x00cc, blocks: (B:30:0x0071, B:32:0x007b, B:36:0x00a6, B:35:0x0092, B:37:0x00c4, B:38:0x00cb, B:11:0x0022, B:14:0x002c), top: B:46:0x0022 }] */
    /* JADX WARN: Removed duplicated region for block: B:37:0x00c4 A[Catch: Exception -> 0x00cc, TryCatch #2 {Exception -> 0x00cc, blocks: (B:30:0x0071, B:32:0x007b, B:36:0x00a6, B:35:0x0092, B:37:0x00c4, B:38:0x00cb, B:11:0x0022, B:14:0x002c), top: B:46:0x0022 }] */
    @Override // kotlin.coroutines.jvm.internal.BaseContinuationImpl
    /*
        Code decompiled incorrectly, please refer to instructions dump.
        To view partially-correct code enable 'Show inconsistent code' option in preferences
    */
    public final java.lang.Object invokeSuspend(java.lang.Object r6) {
        /*
            r5 = this;
            java.lang.Object r0 = defpackage.gs1.d()
            int r1 = r5.label
            r2 = 1
            if (r1 == 0) goto L1f
            if (r1 != r2) goto L17
            java.lang.Object r0 = r5.L$1
            net.safemoon.androidwallet.fragments.TransferHistoryFragment r0 = (net.safemoon.androidwallet.fragments.TransferHistoryFragment) r0
            java.lang.Object r1 = r5.L$0
            net.safemoon.androidwallet.ui.displayModel.UserTokenItemDisplayModel r1 = (net.safemoon.androidwallet.ui.displayModel.UserTokenItemDisplayModel) r1
            defpackage.o83.b(r6)     // Catch: java.lang.Exception -> L6f
            goto L51
        L17:
            java.lang.IllegalStateException r6 = new java.lang.IllegalStateException
            java.lang.String r0 = "call to 'resume' before 'invoke' with coroutine"
            r6.<init>(r0)
            throw r6
        L1f:
            defpackage.o83.b(r6)
            net.safemoon.androidwallet.ui.displayModel.UserTokenItemDisplayModel r6 = r5.$token     // Catch: java.lang.Exception -> Lcc
            java.lang.String r6 = r6.getCmcId()     // Catch: java.lang.Exception -> Lcc
            if (r6 != 0) goto L2c
            goto Lcc
        L2c:
            net.safemoon.androidwallet.ui.displayModel.UserTokenItemDisplayModel r1 = r5.$token     // Catch: java.lang.Exception -> Lcc
            net.safemoon.androidwallet.fragments.TransferHistoryFragment r3 = r5.this$0     // Catch: java.lang.Exception -> Lcc
            int r4 = r6.length()     // Catch: java.lang.Exception -> L6e
            if (r4 != 0) goto L38
            r4 = r2
            goto L39
        L38:
            r4 = 0
        L39:
            if (r4 != 0) goto L66
            e42 r4 = defpackage.a4.k()     // Catch: java.lang.Exception -> L6e
            retrofit2.b r6 = r4.e(r6)     // Catch: java.lang.Exception -> L6e
            r5.L$0 = r1     // Catch: java.lang.Exception -> L6e
            r5.L$1 = r3     // Catch: java.lang.Exception -> L6e
            r5.label = r2     // Catch: java.lang.Exception -> L6e
            java.lang.Object r6 = retrofit2.KotlinExtensions.c(r6, r5)     // Catch: java.lang.Exception -> L6e
            if (r6 != r0) goto L50
            return r0
        L50:
            r0 = r3
        L51:
            retrofit2.n r6 = (retrofit2.n) r6     // Catch: java.lang.Exception -> L6f
            java.lang.Object r6 = r6.a()     // Catch: java.lang.Exception -> L6f
            defpackage.fs1.d(r6)     // Catch: java.lang.Exception -> L6f
            com.google.gson.JsonObject r6 = (com.google.gson.JsonObject) r6     // Catch: java.lang.Exception -> L6f
            java.lang.String r6 = r6.toString()     // Catch: java.lang.Exception -> L6f
            java.lang.String r2 = "{\n                      …g()\n                    }"
            defpackage.fs1.e(r6, r2)     // Catch: java.lang.Exception -> L6f
            goto L71
        L66:
            java.lang.Exception r6 = new java.lang.Exception     // Catch: java.lang.Exception -> L6e
            java.lang.String r0 = ""
            r6.<init>(r0)     // Catch: java.lang.Exception -> L6e
            throw r6     // Catch: java.lang.Exception -> L6e
        L6e:
            r0 = r3
        L6f:
            java.lang.String r6 = "{}"
        L71:
            java.lang.String r1 = r1.getSymbol()     // Catch: java.lang.Exception -> Lcc
            java.lang.String r1 = defpackage.kt.e(r1)     // Catch: java.lang.Exception -> Lcc
            if (r1 == 0) goto Lc4
            java.util.Locale r2 = java.util.Locale.ROOT     // Catch: java.lang.Exception -> Lcc
            java.lang.String r1 = r1.toUpperCase(r2)     // Catch: java.lang.Exception -> Lcc
            java.lang.String r2 = "(this as java.lang.Strin….toUpperCase(Locale.ROOT)"
            defpackage.fs1.e(r1, r2)     // Catch: java.lang.Exception -> Lcc
            org.json.JSONObject r2 = new org.json.JSONObject     // Catch: java.lang.Exception -> Lcc
            r2.<init>(r6)     // Catch: java.lang.Exception -> Lcc
            boolean r2 = r2.has(r1)     // Catch: java.lang.Exception -> Lcc
            if (r2 == 0) goto L92
            goto La6
        L92:
            org.json.JSONObject r1 = new org.json.JSONObject     // Catch: java.lang.Exception -> Lcc
            r1.<init>(r6)     // Catch: java.lang.Exception -> Lcc
            java.util.Iterator r1 = r1.keys()     // Catch: java.lang.Exception -> Lcc
            java.lang.Object r1 = r1.next()     // Catch: java.lang.Exception -> Lcc
            java.lang.String r2 = "{\n                      …                        }"
            defpackage.fs1.e(r1, r2)     // Catch: java.lang.Exception -> Lcc
            java.lang.String r1 = (java.lang.String) r1     // Catch: java.lang.Exception -> Lcc
        La6:
            com.google.gson.Gson r2 = new com.google.gson.Gson     // Catch: java.lang.Exception -> Lcc
            r2.<init>()     // Catch: java.lang.Exception -> Lcc
            org.json.JSONObject r3 = new org.json.JSONObject     // Catch: java.lang.Exception -> Lcc
            r3.<init>(r6)     // Catch: java.lang.Exception -> Lcc
            java.lang.String r6 = r3.getString(r1)     // Catch: java.lang.Exception -> Lcc
            java.lang.Class<net.safemoon.androidwallet.model.Coin> r1 = net.safemoon.androidwallet.model.Coin.class
            java.lang.Object r6 = r2.fromJson(r6, r1)     // Catch: java.lang.Exception -> Lcc
            net.safemoon.androidwallet.model.Coin r6 = (net.safemoon.androidwallet.model.Coin) r6     // Catch: java.lang.Exception -> Lcc
            gb2 r0 = r0.c0()     // Catch: java.lang.Exception -> Lcc
            r0.postValue(r6)     // Catch: java.lang.Exception -> Lcc
            goto Lcc
        Lc4:
            java.lang.NullPointerException r6 = new java.lang.NullPointerException     // Catch: java.lang.Exception -> Lcc
            java.lang.String r0 = "null cannot be cast to non-null type java.lang.String"
            r6.<init>(r0)     // Catch: java.lang.Exception -> Lcc
            throw r6     // Catch: java.lang.Exception -> Lcc
        Lcc:
            te4 r6 = defpackage.te4.a
            return r6
        */
        throw new UnsupportedOperationException("Method not decompiled: net.safemoon.androidwallet.fragments.TransferHistoryFragment$fetchTokenInfo$1.invokeSuspend(java.lang.Object):java.lang.Object");
    }
}
