package net.safemoon.androidwallet.fragments.collectibles;

import android.content.Context;
import android.content.DialogInterface;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.os.Handler;
import android.os.Looper;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentViewModelLazyKt;
import com.bumptech.glide.load.DataSource;
import com.bumptech.glide.load.engine.GlideException;
import com.github.mikephil.charting.utils.Utils;
import com.google.android.material.button.MaterialButton;
import com.google.android.material.textview.MaterialTextView;
import java.lang.ref.WeakReference;
import net.safemoon.androidwallet.R;
import net.safemoon.androidwallet.common.TokenType;
import net.safemoon.androidwallet.dialogs.ProgressLoading;
import net.safemoon.androidwallet.fragments.collectibles.TransferNftFragment;
import net.safemoon.androidwallet.fragments.common.BaseMainFragment;
import net.safemoon.androidwallet.model.collectible.AssetContract;
import net.safemoon.androidwallet.model.common.LoadingState;
import net.safemoon.androidwallet.model.contact.abstraction.IContact;
import net.safemoon.androidwallet.model.nft.NFTData;
import net.safemoon.androidwallet.model.nft.NFTTransferResponse;
import net.safemoon.androidwallet.model.wallets.Wallet;
import net.safemoon.androidwallet.viewmodels.CollectibleViewModel;

/* compiled from: TransferNftFragment.kt */
/* loaded from: classes2.dex */
public final class TransferNftFragment extends BaseMainFragment {
    public kb1 n0;
    public gb2<NFTTransferResponse> o0;
    public im1 p0;
    public CountDownTimer s0;
    public final sy1 i0 = zy1.a(new TransferNftFragment$nftData$2(this));
    public final sy1 j0 = zy1.a(new TransferNftFragment$toAddress$2(this));
    public final sy1 k0 = zy1.a(new TransferNftFragment$wallet$2(this));
    public final sy1 l0 = zy1.a(new TransferNftFragment$iContact$2(this));
    public final sy1 m0 = FragmentViewModelLazyKt.a(this, d53.b(CollectibleViewModel.class), new TransferNftFragment$special$$inlined$viewModels$default$2(new TransferNftFragment$special$$inlined$viewModels$default$1(this)), null);
    public final long q0 = 5000;
    public final gb2<Long> r0 = new gb2<>(0L);

    /* compiled from: TransferNftFragment.kt */
    /* loaded from: classes2.dex */
    public static final class a implements j73<Drawable> {
        public final /* synthetic */ kb1 f0;

        public a(kb1 kb1Var) {
            this.f0 = kb1Var;
        }

        public static final void b(TransferNftFragment transferNftFragment, kb1 kb1Var) {
            fs1.f(transferNftFragment, "this$0");
            fs1.f(kb1Var, "$this_run");
            if (transferNftFragment.isVisible()) {
                k73 u = com.bumptech.glide.a.u(kb1Var.e);
                String imageData = transferNftFragment.G().getImageData();
                u.z(imageData == null ? null : mu3.a(imageData)).I0(kb1Var.e);
            }
        }

        @Override // defpackage.j73
        /* renamed from: c */
        public boolean n(Drawable drawable, Object obj, i34<Drawable> i34Var, DataSource dataSource, boolean z) {
            return false;
        }

        @Override // defpackage.j73
        public boolean i(GlideException glideException, Object obj, i34<Drawable> i34Var, boolean z) {
            Handler handler = new Handler(Looper.getMainLooper());
            final TransferNftFragment transferNftFragment = TransferNftFragment.this;
            final kb1 kb1Var = this.f0;
            handler.post(new Runnable() { // from class: pa4
                @Override // java.lang.Runnable
                public final void run() {
                    TransferNftFragment.a.b(TransferNftFragment.this, kb1Var);
                }
            });
            return true;
        }
    }

    public static final void K(TransferNftFragment transferNftFragment, View view) {
        fs1.f(transferNftFragment, "this$0");
        transferNftFragment.f();
    }

    public static final void L(kb1 kb1Var, String str) {
        fs1.f(kb1Var, "$this_run");
        if (str == null) {
            return;
        }
        kb1Var.i.setText(str);
    }

    public static final void M(TransferNftFragment transferNftFragment, ProgressLoading progressLoading, LoadingState loadingState) {
        boolean z;
        fs1.f(transferNftFragment, "this$0");
        fs1.f(progressLoading, "$loading");
        if (loadingState == null) {
            return;
        }
        kb1 kb1Var = transferNftFragment.n0;
        MaterialButton materialButton = kb1Var == null ? null : kb1Var.b;
        if (materialButton != null) {
            if (loadingState == LoadingState.Normal) {
                Double A = transferNftFragment.E().A();
                double doubleValue = A == null ? Utils.DOUBLE_EPSILON : A.doubleValue();
                Double v = transferNftFragment.E().v();
                if (doubleValue <= (v == null ? -1.0d : v.doubleValue())) {
                    z = true;
                    materialButton.setEnabled(z);
                }
            }
            z = false;
            materialButton.setEnabled(z);
        }
        try {
            if (loadingState == LoadingState.Loading) {
                FragmentManager childFragmentManager = transferNftFragment.getChildFragmentManager();
                fs1.e(childFragmentManager, "childFragmentManager");
                progressLoading.A(childFragmentManager);
            } else if (progressLoading.isVisible()) {
                progressLoading.h();
            }
        } catch (Exception unused) {
        }
    }

    public static final boolean N(TransferNftFragment transferNftFragment, kb1 kb1Var, View view, MotionEvent motionEvent) {
        fs1.f(transferNftFragment, "this$0");
        fs1.f(kb1Var, "$this_run");
        if (motionEvent.getAction() == 0) {
            Context requireContext = transferNftFragment.requireContext();
            fs1.e(requireContext, "requireContext()");
            e30.E(requireContext);
            Context requireContext2 = transferNftFragment.requireContext();
            fs1.e(requireContext2, "requireContext()");
            e30.Z(requireContext2, R.string.alert_hold_to_send);
            kb1Var.c.requestDisallowInterceptTouchEvent(true);
            transferNftFragment.T();
        }
        if (motionEvent.getAction() == 1 || motionEvent.getAction() == 3) {
            kb1Var.c.requestDisallowInterceptTouchEvent(false);
            if (transferNftFragment.s0 != null) {
                transferNftFragment.r0.setValue(0L);
                CountDownTimer countDownTimer = transferNftFragment.s0;
                if (countDownTimer != null) {
                    countDownTimer.cancel();
                }
            }
        }
        return false;
    }

    public static final void O(kb1 kb1Var, String str) {
        fs1.f(kb1Var, "$this_run");
        if (str == null) {
            return;
        }
        kb1Var.j.setText(str);
    }

    public static final void P(TransferNftFragment transferNftFragment, View view) {
        fs1.f(transferNftFragment, "this$0");
        oc2 oc2Var = new oc2();
        FragmentManager parentFragmentManager = transferNftFragment.getParentFragmentManager();
        fs1.e(parentFragmentManager, "parentFragmentManager");
        oc2Var.x(parentFragmentManager);
    }

    public static final void Q(final TransferNftFragment transferNftFragment, NFTTransferResponse nFTTransferResponse) {
        fs1.f(transferNftFragment, "this$0");
        if (nFTTransferResponse == null) {
            return;
        }
        if (nFTTransferResponse.getError() == null) {
            if (nFTTransferResponse.getHash() != null) {
                b30 b30Var = b30.a;
                TokenType.a aVar = TokenType.Companion;
                Integer chainID = transferNftFragment.G().getChainID();
                fs1.d(chainID);
                fo0.e(new WeakReference(transferNftFragment.requireActivity()), fs1.l(b30Var.o(aVar.b(chainID.intValue())), nFTTransferResponse.getHash()), new DialogInterface.OnDismissListener() { // from class: la4
                    @Override // android.content.DialogInterface.OnDismissListener
                    public final void onDismiss(DialogInterface dialogInterface) {
                        TransferNftFragment.R(TransferNftFragment.this, dialogInterface);
                    }
                });
                return;
            }
            return;
        }
        Context requireContext = transferNftFragment.requireContext();
        fs1.e(requireContext, "requireContext()");
        e30.a0(requireContext, nFTTransferResponse.getError());
    }

    public static final void R(TransferNftFragment transferNftFragment, DialogInterface dialogInterface) {
        fs1.f(transferNftFragment, "this$0");
        gm1 j = transferNftFragment.j();
        if (j == null) {
            return;
        }
        j.h(R.id.navigation_collectibles);
    }

    public static final void S(TransferNftFragment transferNftFragment, long j) {
        MaterialButton materialButton;
        fs1.f(transferNftFragment, "this$0");
        int i = (int) ((100 * j) / transferNftFragment.q0);
        kb1 kb1Var = transferNftFragment.n0;
        ProgressBar progressBar = kb1Var == null ? null : kb1Var.f;
        if (progressBar != null) {
            progressBar.setProgress(i);
        }
        if (j > 0) {
            kb1 kb1Var2 = transferNftFragment.n0;
            ProgressBar progressBar2 = kb1Var2 == null ? null : kb1Var2.f;
            if (progressBar2 != null) {
                progressBar2.setVisibility(0);
            }
            int b = t42.b((100 - i) / 20.0f);
            kb1 kb1Var3 = transferNftFragment.n0;
            MaterialButton materialButton2 = kb1Var3 != null ? kb1Var3.b : null;
            if (materialButton2 == null) {
                return;
            }
            materialButton2.setText(fs1.l("", Integer.valueOf(Math.max(1, b))));
            return;
        }
        kb1 kb1Var4 = transferNftFragment.n0;
        ProgressBar progressBar3 = kb1Var4 != null ? kb1Var4.f : null;
        if (progressBar3 != null) {
            progressBar3.setVisibility(8);
        }
        kb1 kb1Var5 = transferNftFragment.n0;
        if (kb1Var5 == null || (materialButton = kb1Var5.b) == null) {
            return;
        }
        materialButton.setText(R.string.send);
    }

    public final CollectibleViewModel E() {
        return (CollectibleViewModel) this.m0.getValue();
    }

    public final IContact F() {
        return (IContact) this.l0.getValue();
    }

    public final NFTData G() {
        return (NFTData) this.i0.getValue();
    }

    public final gb2<NFTTransferResponse> H() {
        return this.o0;
    }

    public final String I() {
        return (String) this.j0.getValue();
    }

    public final Wallet J() {
        return (Wallet) this.k0.getValue();
    }

    public final void T() {
        CountDownTimer countDownTimer = this.s0;
        if (countDownTimer != null) {
            countDownTimer.cancel();
        }
        final long j = this.q0;
        final long j2 = j / 100;
        CountDownTimer countDownTimer2 = new CountDownTimer(j, j2) { // from class: net.safemoon.androidwallet.fragments.collectibles.TransferNftFragment$startTimer$1
            @Override // android.os.CountDownTimer
            public void onFinish() {
                gb2 gb2Var;
                CountDownTimer countDownTimer3;
                CollectibleViewModel E;
                String I;
                im1 im1Var;
                gb2Var = TransferNftFragment.this.r0;
                gb2Var.setValue(0L);
                countDownTimer3 = TransferNftFragment.this.s0;
                if (countDownTimer3 != null) {
                    countDownTimer3.cancel();
                }
                try {
                    Context requireContext = TransferNftFragment.this.requireContext();
                    fs1.e(requireContext, "requireContext()");
                    e30.E(requireContext);
                    gb2<NFTTransferResponse> H = TransferNftFragment.this.H();
                    if (H == null) {
                        return;
                    }
                    TransferNftFragment transferNftFragment = TransferNftFragment.this;
                    E = transferNftFragment.E();
                    I = transferNftFragment.I();
                    String tokenId = transferNftFragment.G().getTokenId();
                    im1Var = transferNftFragment.p0;
                    E.d0(I, tokenId, im1Var, H, new TransferNftFragment$startTimer$1$onFinish$1$1(transferNftFragment));
                } catch (Exception e) {
                    String message = e.getMessage();
                    if (message == null) {
                        return;
                    }
                    Context requireContext2 = TransferNftFragment.this.requireContext();
                    fs1.e(requireContext2, "requireContext()");
                    e30.a0(requireContext2, message);
                }
            }

            @Override // android.os.CountDownTimer
            public void onTick(long j3) {
                gb2 gb2Var;
                long j4;
                gb2Var = TransferNftFragment.this.r0;
                j4 = TransferNftFragment.this.q0;
                gb2Var.setValue(Long.valueOf((50 + j4) - j3));
            }
        };
        this.s0 = countDownTimer2;
        countDownTimer2.start();
    }

    @Override // androidx.fragment.app.Fragment
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
    }

    @Override // androidx.fragment.app.Fragment
    public View onCreateView(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle) {
        fs1.f(layoutInflater, "inflater");
        kb1 c = kb1.c(layoutInflater, viewGroup, false);
        this.n0 = c;
        if (c == null) {
            return null;
        }
        return c.b();
    }

    @Override // androidx.fragment.app.Fragment
    public void onDetach() {
        super.onDetach();
        this.n0 = null;
    }

    @Override // net.safemoon.androidwallet.fragments.common.BaseMainFragment, defpackage.qn, androidx.fragment.app.Fragment
    public void onViewCreated(View view, Bundle bundle) {
        MaterialTextView materialTextView;
        rp3 rp3Var;
        fs1.f(view, "view");
        super.onViewCreated(view, bundle);
        CollectibleViewModel E = E();
        Wallet J = J();
        AssetContract assetContract = G().getAssetContract();
        String address = assetContract == null ? null : assetContract.getAddress();
        AssetContract assetContract2 = G().getAssetContract();
        this.p0 = E.O(J, address, assetContract2 == null ? null : assetContract2.getSchema_name(), G().getChainID());
        kb1 kb1Var = this.n0;
        if (kb1Var != null && (rp3Var = kb1Var.g) != null) {
            rp3Var.a.setOnClickListener(new View.OnClickListener() { // from class: na4
                @Override // android.view.View.OnClickListener
                public final void onClick(View view2) {
                    TransferNftFragment.K(TransferNftFragment.this, view2);
                }
            });
            rp3Var.c.setText(getString(R.string.collectible_screen_transfer));
        }
        gb2<NFTTransferResponse> gb2Var = new gb2<>();
        this.o0 = gb2Var;
        gb2Var.observe(getViewLifecycleOwner(), new tl2() { // from class: ia4
            @Override // defpackage.tl2
            public final void onChanged(Object obj) {
                TransferNftFragment.Q(TransferNftFragment.this, (NFTTransferResponse) obj);
            }
        });
        this.r0.observe(getViewLifecycleOwner(), new tl2() { // from class: ja4
            @Override // defpackage.tl2
            public final void onChanged(Object obj) {
                TransferNftFragment.S(TransferNftFragment.this, ((Long) obj).longValue());
            }
        });
        final kb1 kb1Var2 = this.n0;
        if (kb1Var2 != null) {
            kb1Var2.h.setText(G().getName());
            Wallet J2 = J();
            if (J2 != null) {
                MaterialTextView materialTextView2 = kb1Var2.k;
                materialTextView2.setText(J2.displayName() + "\n(" + e30.d(J2.getAddress()) + ')');
                MaterialTextView materialTextView3 = kb1Var2.l;
                IContact F = F();
                String name = F == null ? null : F.getName();
                if (name == null) {
                    name = I();
                }
                materialTextView3.setText(name != null ? e30.d(name) : null);
                kb1Var2.b.setOnTouchListener(new View.OnTouchListener() { // from class: oa4
                    @Override // android.view.View.OnTouchListener
                    public final boolean onTouch(View view2, MotionEvent motionEvent) {
                        boolean N;
                        N = TransferNftFragment.N(TransferNftFragment.this, kb1Var2, view2, motionEvent);
                        return N;
                    }
                });
            }
            E().y().observe(getViewLifecycleOwner(), new tl2() { // from class: ga4
                @Override // defpackage.tl2
                public final void onChanged(Object obj) {
                    TransferNftFragment.O(kb1.this, (String) obj);
                }
            });
            E().F().observe(getViewLifecycleOwner(), new tl2() { // from class: ha4
                @Override // defpackage.tl2
                public final void onChanged(Object obj) {
                    TransferNftFragment.L(kb1.this, (String) obj);
                }
            });
            com.bumptech.glide.a.u(kb1Var2.e).y(G().getImageUrl()).L0(new a(kb1Var2)).I0(kb1Var2.e);
            ProgressLoading.a aVar = ProgressLoading.y0;
            String string = getString(R.string.loading);
            fs1.e(string, "getString(R.string.loading)");
            final ProgressLoading a2 = aVar.a(false, string, "");
            E().z().observe(getViewLifecycleOwner(), new tl2() { // from class: ka4
                @Override // defpackage.tl2
                public final void onChanged(Object obj) {
                    TransferNftFragment.M(TransferNftFragment.this, a2, (LoadingState) obj);
                }
            });
        }
        try {
            CollectibleViewModel E2 = E();
            String I = I();
            fs1.d(I);
            fs1.e(I, "toAddress!!");
            String tokenId = G().getTokenId();
            fs1.d(tokenId);
            E2.p(I, tokenId, this.p0);
        } catch (Exception unused) {
        }
        kb1 kb1Var3 = this.n0;
        if (kb1Var3 == null || (materialTextView = kb1Var3.d) == null) {
            return;
        }
        materialTextView.setOnClickListener(new View.OnClickListener() { // from class: ma4
            @Override // android.view.View.OnClickListener
            public final void onClick(View view2) {
                TransferNftFragment.P(TransferNftFragment.this, view2);
            }
        });
    }
}
