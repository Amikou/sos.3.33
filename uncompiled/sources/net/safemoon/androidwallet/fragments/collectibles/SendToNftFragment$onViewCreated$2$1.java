package net.safemoon.androidwallet.fragments.collectibles;

import android.content.Context;
import android.content.Intent;
import kotlin.jvm.internal.Lambda;
import net.safemoon.androidwallet.R;
import net.safemoon.androidwallet.activity.ScannedCodeActivity;

/* compiled from: SendToNftFragment.kt */
/* loaded from: classes2.dex */
public final class SendToNftFragment$onViewCreated$2$1 extends Lambda implements rc1<te4> {
    public final /* synthetic */ za1 $this_run;
    public final /* synthetic */ SendToNftFragment this$0;

    /* compiled from: SendToNftFragment.kt */
    /* renamed from: net.safemoon.androidwallet.fragments.collectibles.SendToNftFragment$onViewCreated$2$1$1  reason: invalid class name */
    /* loaded from: classes2.dex */
    public static final class AnonymousClass1 extends Lambda implements tc1<Intent, te4> {
        public final /* synthetic */ za1 $this_run;
        public final /* synthetic */ SendToNftFragment this$0;

        /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
        public AnonymousClass1(za1 za1Var, SendToNftFragment sendToNftFragment) {
            super(1);
            this.$this_run = za1Var;
            this.this$0 = sendToNftFragment;
        }

        @Override // defpackage.tc1
        public /* bridge */ /* synthetic */ te4 invoke(Intent intent) {
            invoke2(intent);
            return te4.a;
        }

        /* renamed from: invoke  reason: avoid collision after fix types in other method */
        public final void invoke2(Intent intent) {
            String stringExtra;
            if (intent == null || (stringExtra = intent.getStringExtra("result")) == null) {
                return;
            }
            try {
                this.$this_run.f.setText(b30.a.q(stringExtra));
            } catch (Exception unused) {
                Context requireContext = this.this$0.requireContext();
                fs1.e(requireContext, "requireContext()");
                String string = this.this$0.getString(R.string.err_invalid_wc);
                fs1.e(string, "getString(R.string.err_invalid_wc)");
                e30.a0(requireContext, string);
            }
        }
    }

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public SendToNftFragment$onViewCreated$2$1(SendToNftFragment sendToNftFragment, za1 za1Var) {
        super(0);
        this.this$0 = sendToNftFragment;
        this.$this_run = za1Var;
    }

    @Override // defpackage.rc1
    public /* bridge */ /* synthetic */ te4 invoke() {
        invoke2();
        return te4.a;
    }

    @Override // defpackage.rc1
    /* renamed from: invoke  reason: avoid collision after fix types in other method */
    public final void invoke2() {
        gm1 j;
        hn2 a;
        w7<Intent> b;
        j = this.this$0.j();
        if (j == null || (a = j.a()) == null || (b = a.b(new AnonymousClass1(this.$this_run, this.this$0))) == null) {
            return;
        }
        b.a(new Intent(this.this$0.requireActivity(), ScannedCodeActivity.class));
    }
}
