package net.safemoon.androidwallet.fragments.collectibles;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.ScrollView;
import androidx.appcompat.widget.AppCompatEditText;
import androidx.appcompat.widget.AppCompatTextView;
import androidx.fragment.app.FragmentActivity;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentViewModelLazyKt;
import androidx.lifecycle.l;
import androidx.recyclerview.widget.RecyclerView;
import com.bumptech.glide.load.DataSource;
import com.bumptech.glide.load.engine.GlideException;
import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.Objects;
import kotlin.text.StringsKt__StringsKt;
import net.safemoon.androidwallet.R;
import net.safemoon.androidwallet.dialogs.G2FAVerfication;
import net.safemoon.androidwallet.fragments.collectibles.SendToNftFragment;
import net.safemoon.androidwallet.fragments.common.BaseMainFragment;
import net.safemoon.androidwallet.model.contact.abstraction.IContact;
import net.safemoon.androidwallet.model.nft.NFTData;
import net.safemoon.androidwallet.model.wallets.Wallet;
import net.safemoon.androidwallet.provider.AskAuthorizeProvider;
import net.safemoon.androidwallet.viewmodels.ContactViewModel;
import net.safemoon.androidwallet.viewmodels.MultiWalletViewModel;
import net.safemoon.androidwallet.viewmodels.factory.MyViewModelFactory;
import net.safemoon.androidwallet.views.carousel.ContactCarouselView;

/* compiled from: SendToNftFragment.kt */
/* loaded from: classes2.dex */
public final class SendToNftFragment extends BaseMainFragment {
    public ContactViewModel i0;
    public qc k0;
    public za1 m0;
    public final sy1 j0 = FragmentViewModelLazyKt.a(this, d53.b(MultiWalletViewModel.class), new SendToNftFragment$special$$inlined$viewModels$default$2(new SendToNftFragment$special$$inlined$viewModels$default$1(this)), null);
    public final sy1 l0 = zy1.a(new SendToNftFragment$nftData$2(this));

    /* compiled from: SendToNftFragment.kt */
    /* loaded from: classes2.dex */
    public static final class a implements sl1 {
        public a() {
        }

        @Override // defpackage.sl1
        public void a(IContact iContact) {
            AppCompatEditText appCompatEditText;
            fs1.f(iContact, "item");
            za1 za1Var = SendToNftFragment.this.m0;
            if (za1Var != null && (appCompatEditText = za1Var.f) != null) {
                appCompatEditText.setText(iContact.getAddress());
            }
            za1 za1Var2 = SendToNftFragment.this.m0;
            RecyclerView recyclerView = za1Var2 == null ? null : za1Var2.d;
            if (recyclerView != null) {
                recyclerView.setAdapter(null);
            }
            za1 za1Var3 = SendToNftFragment.this.m0;
            LinearLayout linearLayout = za1Var3 == null ? null : za1Var3.j;
            if (linearLayout != null) {
                linearLayout.setVisibility(8);
            }
            za1 za1Var4 = SendToNftFragment.this.m0;
            AppCompatEditText appCompatEditText2 = za1Var4 != null ? za1Var4.f : null;
            if (appCompatEditText2 == null) {
                return;
            }
            appCompatEditText2.setVisibility(0);
        }
    }

    /* compiled from: SendToNftFragment.kt */
    /* loaded from: classes2.dex */
    public static final class b implements G2FAVerfication.b {
        public b() {
        }

        @Override // net.safemoon.androidwallet.dialogs.G2FAVerfication.b
        public void a() {
        }

        @Override // net.safemoon.androidwallet.dialogs.G2FAVerfication.b
        public void onSuccess() {
            SendToNftFragment.this.i0();
        }
    }

    /* compiled from: SendToNftFragment.kt */
    /* loaded from: classes2.dex */
    public static final class c implements j73<Drawable> {
        public final /* synthetic */ za1 f0;

        public c(za1 za1Var) {
            this.f0 = za1Var;
        }

        public static final void b(SendToNftFragment sendToNftFragment, za1 za1Var) {
            fs1.f(sendToNftFragment, "this$0");
            fs1.f(za1Var, "$this_run");
            if (sendToNftFragment.isVisible()) {
                k73 u = com.bumptech.glide.a.u(za1Var.h);
                String imageData = sendToNftFragment.Q().getImageData();
                u.z(imageData == null ? null : mu3.a(imageData)).I0(za1Var.h);
            }
        }

        @Override // defpackage.j73
        /* renamed from: c */
        public boolean n(Drawable drawable, Object obj, i34<Drawable> i34Var, DataSource dataSource, boolean z) {
            return false;
        }

        @Override // defpackage.j73
        public boolean i(GlideException glideException, Object obj, i34<Drawable> i34Var, boolean z) {
            Handler handler = new Handler(Looper.getMainLooper());
            final SendToNftFragment sendToNftFragment = SendToNftFragment.this;
            final za1 za1Var = this.f0;
            handler.post(new Runnable() { // from class: ok3
                @Override // java.lang.Runnable
                public final void run() {
                    SendToNftFragment.c.b(SendToNftFragment.this, za1Var);
                }
            });
            return true;
        }
    }

    /* compiled from: SendToNftFragment.kt */
    /* loaded from: classes2.dex */
    public static final class d implements ContactCarouselView.b {
        public d() {
        }

        @Override // net.safemoon.androidwallet.views.carousel.ContactCarouselView.b
        public void a(boolean z, int i, IContact iContact) {
            fs1.f(iContact, "contact");
            if (SendToNftFragment.this.k0 != null) {
                qc qcVar = SendToNftFragment.this.k0;
                fs1.d(qcVar);
                if (qcVar.e()) {
                    qc qcVar2 = SendToNftFragment.this.k0;
                    fs1.d(qcVar2);
                    qcVar2.d();
                }
            }
            if (z) {
                SendToNftFragment.this.X(iContact);
                return;
            }
            ContactViewModel contactViewModel = SendToNftFragment.this.i0;
            if (contactViewModel == null) {
                return;
            }
            contactViewModel.p();
        }
    }

    /* compiled from: SendToNftFragment.kt */
    /* loaded from: classes2.dex */
    public static final class e implements TextWatcher {
        public e() {
        }

        @Override // android.text.TextWatcher
        public void afterTextChanged(Editable editable) {
        }

        @Override // android.text.TextWatcher
        public void beforeTextChanged(CharSequence charSequence, int i, int i2, int i3) {
        }

        @Override // android.text.TextWatcher
        public void onTextChanged(CharSequence charSequence, int i, int i2, int i3) {
            SendToNftFragment.this.N(charSequence);
        }
    }

    /* compiled from: SendToNftFragment.kt */
    /* loaded from: classes2.dex */
    public static final class f implements TextWatcher {
        public f() {
        }

        @Override // android.text.TextWatcher
        public void afterTextChanged(Editable editable) {
        }

        @Override // android.text.TextWatcher
        public void beforeTextChanged(CharSequence charSequence, int i, int i2, int i3) {
        }

        @Override // android.text.TextWatcher
        public void onTextChanged(CharSequence charSequence, int i, int i2, int i3) {
            SendToNftFragment.this.M(charSequence);
        }
    }

    /* compiled from: SendToNftFragment.kt */
    /* loaded from: classes2.dex */
    public static final class g implements sl1 {
        public g() {
        }

        @Override // defpackage.sl1
        public void a(IContact iContact) {
            AppCompatEditText appCompatEditText;
            fs1.f(iContact, "item");
            za1 za1Var = SendToNftFragment.this.m0;
            if (za1Var != null && (appCompatEditText = za1Var.f) != null) {
                appCompatEditText.setText(iContact.getAddress());
            }
            za1 za1Var2 = SendToNftFragment.this.m0;
            LinearLayout linearLayout = za1Var2 == null ? null : za1Var2.j;
            if (linearLayout != null) {
                linearLayout.setVisibility(8);
            }
            za1 za1Var3 = SendToNftFragment.this.m0;
            AppCompatEditText appCompatEditText2 = za1Var3 == null ? null : za1Var3.f;
            if (appCompatEditText2 != null) {
                appCompatEditText2.setVisibility(0);
            }
            za1 za1Var4 = SendToNftFragment.this.m0;
            RecyclerView recyclerView = za1Var4 == null ? null : za1Var4.d;
            if (recyclerView == null) {
                return;
            }
            recyclerView.setAdapter(null);
        }
    }

    public static final void T(SendToNftFragment sendToNftFragment, s60 s60Var) {
        fs1.f(sendToNftFragment, "this$0");
        if (s60Var == null || s60Var.a().isEmpty()) {
            return;
        }
        za1 za1Var = sendToNftFragment.m0;
        fs1.d(za1Var);
        za1Var.e.setItems(s60Var.a(), s60Var.b());
        ContactViewModel contactViewModel = sendToNftFragment.i0;
        fs1.d(contactViewModel);
        if (contactViewModel.n().getValue() != null) {
            za1 za1Var2 = sendToNftFragment.m0;
            fs1.d(za1Var2);
            ContactCarouselView contactCarouselView = za1Var2.e;
            ContactViewModel contactViewModel2 = sendToNftFragment.i0;
            fs1.d(contactViewModel2);
            IContact value = contactViewModel2.n().getValue();
            fs1.d(value);
            fs1.e(value, "contactViewModel!!.selectedContact.value!!");
            contactCarouselView.setSelectedItem(value);
        }
        za1 za1Var3 = sendToNftFragment.m0;
        fs1.d(za1Var3);
        za1Var3.e.p();
    }

    public static final void U(SendToNftFragment sendToNftFragment, List list) {
        fs1.f(sendToNftFragment, "this$0");
        ContactViewModel contactViewModel = sendToNftFragment.i0;
        fs1.d(contactViewModel);
        contactViewModel.o();
    }

    public static final void V(SendToNftFragment sendToNftFragment, IContact iContact) {
        za1 za1Var;
        AppCompatEditText appCompatEditText;
        fs1.f(sendToNftFragment, "this$0");
        if (iContact == null || (za1Var = sendToNftFragment.m0) == null || (appCompatEditText = za1Var.f) == null) {
            return;
        }
        appCompatEditText.setText(iContact.getAddress());
    }

    public static final void W(SendToNftFragment sendToNftFragment, View view, int i, int i2, int i3, int i4, int i5, int i6, int i7, int i8) {
        za1 za1Var;
        ScrollView scrollView;
        fs1.f(sendToNftFragment, "this$0");
        if (!sendToNftFragment.isVisible() || (za1Var = sendToNftFragment.m0) == null || (scrollView = za1Var.i) == null) {
            return;
        }
        fs1.d(za1Var);
        scrollView.smoothScrollTo(0, za1Var.i.getBottom());
    }

    public static final void Y(SendToNftFragment sendToNftFragment, View view) {
        fs1.f(sendToNftFragment, "this$0");
        sendToNftFragment.f();
    }

    public static final void Z(za1 za1Var, SendToNftFragment sendToNftFragment, View view) {
        fs1.f(za1Var, "$this_run");
        fs1.f(sendToNftFragment, "this$0");
        if (e30.I(String.valueOf(za1Var.f.getText()))) {
            String obj = za1Var.f.getEditableText().toString();
            Context requireContext = sendToNftFragment.requireContext();
            fs1.e(requireContext, "requireContext()");
            Wallet c2 = e30.c(requireContext);
            if (c2 == null) {
                return;
            }
            String address = c2.getAddress();
            Objects.requireNonNull(address, "null cannot be cast to non-null type kotlin.CharSequence");
            String obj2 = StringsKt__StringsKt.K0(address).toString();
            Objects.requireNonNull(obj2, "null cannot be cast to non-null type java.lang.String");
            Locale locale = Locale.ROOT;
            String lowerCase = obj2.toLowerCase(locale);
            fs1.e(lowerCase, "(this as java.lang.Strin….toLowerCase(Locale.ROOT)");
            Objects.requireNonNull(obj, "null cannot be cast to non-null type kotlin.CharSequence");
            String obj3 = StringsKt__StringsKt.K0(obj).toString();
            Objects.requireNonNull(obj3, "null cannot be cast to non-null type java.lang.String");
            String lowerCase2 = obj3.toLowerCase(locale);
            fs1.e(lowerCase2, "(this as java.lang.Strin….toLowerCase(Locale.ROOT)");
            if (fs1.b(lowerCase, lowerCase2)) {
                new AlertDialog.Builder(sendToNftFragment.getContext()).setTitle(sendToNftFragment.getResources().getString(R.string.oops)).setMessage(sendToNftFragment.getResources().getString(R.string.cant_send_same_wallet)).setPositiveButton("Ok", hk3.a).show();
                return;
            } else {
                sendToNftFragment.f0();
                return;
            }
        }
        Context requireContext2 = sendToNftFragment.requireContext();
        fs1.e(requireContext2, "requireContext()");
        e30.E(requireContext2);
        Context requireContext3 = sendToNftFragment.requireContext();
        fs1.e(requireContext3, "requireContext()");
        e30.Z(requireContext3, R.string.send_to_invalid_address);
    }

    public static final void a0(DialogInterface dialogInterface, int i) {
        fs1.f(dialogInterface, "dialog");
        dialogInterface.dismiss();
    }

    public static final void b0(SendToNftFragment sendToNftFragment, View view) {
        fs1.f(sendToNftFragment, "this$0");
        ce2 b2 = pk3.b();
        fs1.e(b2, "actionSendtoNFTFragmentToManageContactsFragment()");
        sendToNftFragment.g(b2);
        sendToNftFragment.R();
    }

    public static final void c0(SendToNftFragment sendToNftFragment, View view) {
        fs1.f(sendToNftFragment, "this$0");
        sendToNftFragment.P().p(new SendToNftFragment$onViewCreated$2$7$1(sendToNftFragment));
    }

    public static final void g0(SendToNftFragment sendToNftFragment, View view) {
        fs1.f(sendToNftFragment, "this$0");
        if (bo3.d(sendToNftFragment.requireContext(), "AUTH_2FA_IS_ENABLE")) {
            G2FAVerfication b2 = G2FAVerfication.a.b(G2FAVerfication.C0, sendToNftFragment.O(), false, 2, null);
            FragmentManager childFragmentManager = sendToNftFragment.getChildFragmentManager();
            fs1.e(childFragmentManager, "childFragmentManager");
            b2.H(childFragmentManager);
            return;
        }
        sendToNftFragment.i0();
    }

    public static final void h0(View view) {
    }

    /* JADX WARN: Code restructure failed: missing block: B:9:0x000f, code lost:
        if ((r3.length() == 0) == true) goto L4;
     */
    /*
        Code decompiled incorrectly, please refer to instructions dump.
        To view partially-correct code enable 'Show inconsistent code' option in preferences
    */
    public final void M(java.lang.CharSequence r3) {
        /*
            r2 = this;
            r0 = 1
            r1 = 0
            if (r3 != 0) goto L6
        L4:
            r0 = r1
            goto L11
        L6:
            int r3 = r3.length()
            if (r3 != 0) goto Le
            r3 = r0
            goto Lf
        Le:
            r3 = r1
        Lf:
            if (r3 != r0) goto L4
        L11:
            if (r0 == 0) goto L2c
            za1 r3 = r2.m0
            if (r3 != 0) goto L18
            goto L44
        L18:
            com.google.android.material.button.MaterialButton r3 = r3.c
            if (r3 != 0) goto L1d
            goto L44
        L1d:
            android.content.Context r0 = r2.requireContext()
            r1 = 2131099719(0x7f060047, float:1.78118E38)
            android.content.res.ColorStateList r0 = defpackage.m70.e(r0, r1)
            r3.setBackgroundTintList(r0)
            goto L44
        L2c:
            za1 r3 = r2.m0
            if (r3 != 0) goto L31
            goto L44
        L31:
            com.google.android.material.button.MaterialButton r3 = r3.c
            if (r3 != 0) goto L36
            goto L44
        L36:
            android.content.Context r0 = r2.requireContext()
            r1 = 2131099754(0x7f06006a, float:1.781187E38)
            android.content.res.ColorStateList r0 = defpackage.m70.e(r0, r1)
            r3.setBackgroundTintList(r0)
        L44:
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: net.safemoon.androidwallet.fragments.collectibles.SendToNftFragment.M(java.lang.CharSequence):void");
    }

    public final void N(CharSequence charSequence) {
        ContactViewModel contactViewModel;
        List<IContact> l;
        String name;
        LinearLayout linearLayout;
        za1 za1Var = this.m0;
        boolean z = false;
        if (za1Var != null && (linearLayout = za1Var.j) != null) {
            if (!(linearLayout.getVisibility() == 0)) {
                z = true;
            }
        }
        if (z) {
            return;
        }
        fs1.l("keyword : ", charSequence);
        ArrayList arrayList = new ArrayList();
        if (charSequence != null && (contactViewModel = this.i0) != null && (l = contactViewModel.l()) != null) {
            for (IContact iContact : l) {
                if (!StringsKt__StringsKt.K(iContact == null ? null : iContact.getAddress(), charSequence, true)) {
                    if (((iContact == null || (name = iContact.getName()) == null) ? null : Boolean.valueOf(StringsKt__StringsKt.K(name, charSequence, true))).booleanValue()) {
                    }
                }
                arrayList.add(iContact);
            }
        }
        za1 za1Var2 = this.m0;
        RecyclerView recyclerView = za1Var2 != null ? za1Var2.d : null;
        if (recyclerView == null) {
            return;
        }
        recyclerView.setAdapter(new x60(arrayList, new a()));
    }

    public final G2FAVerfication.b O() {
        return new b();
    }

    public final MultiWalletViewModel P() {
        return (MultiWalletViewModel) this.j0.getValue();
    }

    public final NFTData Q() {
        return (NFTData) this.l0.getValue();
    }

    public final void R() {
        za1 za1Var = this.m0;
        RecyclerView recyclerView = za1Var == null ? null : za1Var.d;
        if (recyclerView != null) {
            recyclerView.setAdapter(null);
        }
        za1 za1Var2 = this.m0;
        LinearLayout linearLayout = za1Var2 == null ? null : za1Var2.j;
        if (linearLayout != null) {
            linearLayout.setVisibility(8);
        }
        za1 za1Var3 = this.m0;
        AppCompatEditText appCompatEditText = za1Var3 != null ? za1Var3.f : null;
        if (appCompatEditText == null) {
            return;
        }
        appCompatEditText.setVisibility(0);
    }

    public final void S() {
        ContactViewModel contactViewModel = this.i0;
        if (contactViewModel == null) {
            return;
        }
        fs1.d(contactViewModel);
        contactViewModel.h().observe(getViewLifecycleOwner(), new tl2() { // from class: gk3
            @Override // defpackage.tl2
            public final void onChanged(Object obj) {
                SendToNftFragment.T(SendToNftFragment.this, (s60) obj);
            }
        });
        ContactViewModel contactViewModel2 = this.i0;
        fs1.d(contactViewModel2);
        contactViewModel2.j().observe(getViewLifecycleOwner(), new tl2() { // from class: dk3
            @Override // defpackage.tl2
            public final void onChanged(Object obj) {
                SendToNftFragment.U(SendToNftFragment.this, (List) obj);
            }
        });
        ContactViewModel contactViewModel3 = this.i0;
        fs1.d(contactViewModel3);
        contactViewModel3.n().observe(getViewLifecycleOwner(), new tl2() { // from class: fk3
            @Override // defpackage.tl2
            public final void onChanged(Object obj) {
                SendToNftFragment.V(SendToNftFragment.this, (IContact) obj);
            }
        });
    }

    public final void X(IContact iContact) {
        ContactViewModel contactViewModel = this.i0;
        if (contactViewModel != null) {
            contactViewModel.r(iContact);
        }
        R();
    }

    public final void d0() {
        za1 za1Var = this.m0;
        RecyclerView recyclerView = za1Var == null ? null : za1Var.d;
        if (recyclerView != null) {
            recyclerView.setAdapter(null);
        }
        za1 za1Var2 = this.m0;
        LinearLayout linearLayout = za1Var2 == null ? null : za1Var2.j;
        if (linearLayout != null) {
            linearLayout.setVisibility(8);
        }
        za1 za1Var3 = this.m0;
        AppCompatEditText appCompatEditText = za1Var3 != null ? za1Var3.f : null;
        if (appCompatEditText == null) {
            return;
        }
        appCompatEditText.setVisibility(0);
    }

    public final void e0() {
        RecyclerView recyclerView;
        AppCompatEditText appCompatEditText;
        za1 za1Var = this.m0;
        if (((za1Var == null || (recyclerView = za1Var.d) == null) ? null : recyclerView.getAdapter()) != null) {
            za1 za1Var2 = this.m0;
            RecyclerView recyclerView2 = za1Var2 == null ? null : za1Var2.d;
            if (recyclerView2 != null) {
                recyclerView2.setAdapter(null);
            }
            za1 za1Var3 = this.m0;
            LinearLayout linearLayout = za1Var3 == null ? null : za1Var3.j;
            if (linearLayout != null) {
                linearLayout.setVisibility(8);
            }
            za1 za1Var4 = this.m0;
            AppCompatEditText appCompatEditText2 = za1Var4 != null ? za1Var4.f : null;
            if (appCompatEditText2 == null) {
                return;
            }
            appCompatEditText2.setVisibility(0);
            return;
        }
        za1 za1Var5 = this.m0;
        LinearLayout linearLayout2 = za1Var5 == null ? null : za1Var5.j;
        if (linearLayout2 != null) {
            linearLayout2.setVisibility(0);
        }
        za1 za1Var6 = this.m0;
        AppCompatEditText appCompatEditText3 = za1Var6 == null ? null : za1Var6.f;
        if (appCompatEditText3 != null) {
            appCompatEditText3.setVisibility(8);
        }
        za1 za1Var7 = this.m0;
        if (za1Var7 != null && (appCompatEditText = za1Var7.k) != null) {
            appCompatEditText.setText("");
        }
        za1 za1Var8 = this.m0;
        RecyclerView recyclerView3 = za1Var8 == null ? null : za1Var8.d;
        if (recyclerView3 == null) {
            return;
        }
        ContactViewModel contactViewModel = this.i0;
        List<IContact> l = contactViewModel != null ? contactViewModel.l() : null;
        fs1.d(l);
        recyclerView3.setAdapter(new x60(l, new g()));
    }

    public final void f0() {
        AppCompatEditText appCompatEditText;
        za1 za1Var = this.m0;
        Editable editable = null;
        if (za1Var != null && (appCompatEditText = za1Var.f) != null) {
            editable = appCompatEditText.getEditableText();
        }
        String valueOf = String.valueOf(editable);
        FragmentActivity requireActivity = requireActivity();
        fs1.e(requireActivity, "requireActivity()");
        bh.n0(requireActivity, valueOf, new View.OnClickListener() { // from class: lk3
            @Override // android.view.View.OnClickListener
            public final void onClick(View view) {
                SendToNftFragment.g0(SendToNftFragment.this, view);
            }
        }, nk3.a);
    }

    public final void i0() {
        AppCompatEditText appCompatEditText;
        za1 za1Var = this.m0;
        Editable editable = null;
        if (za1Var != null && (appCompatEditText = za1Var.f) != null) {
            editable = appCompatEditText.getEditableText();
        }
        String valueOf = String.valueOf(editable);
        Context requireContext = requireContext();
        fs1.e(requireContext, "requireContext()");
        new AskAuthorizeProvider(requireContext, j()).a(new SendToNftFragment$transferNft$1(this, valueOf), SendToNftFragment$transferNft$2.INSTANCE);
    }

    @Override // androidx.fragment.app.Fragment
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        getArguments();
    }

    @Override // androidx.fragment.app.Fragment
    public View onCreateView(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle) {
        fs1.f(layoutInflater, "inflater");
        this.m0 = za1.c(layoutInflater, viewGroup, false);
        this.i0 = (ContactViewModel) new l(this, new MyViewModelFactory(new WeakReference(requireActivity()))).a(ContactViewModel.class);
        fs1.d(viewGroup);
        viewGroup.addOnLayoutChangeListener(new View.OnLayoutChangeListener() { // from class: ek3
            @Override // android.view.View.OnLayoutChangeListener
            public final void onLayoutChange(View view, int i, int i2, int i3, int i4, int i5, int i6, int i7, int i8) {
                SendToNftFragment.W(SendToNftFragment.this, view, i, i2, i3, i4, i5, i6, i7, i8);
            }
        });
        za1 za1Var = this.m0;
        if (za1Var == null) {
            return null;
        }
        return za1Var.b();
    }

    @Override // androidx.fragment.app.Fragment
    public void onDetach() {
        super.onDetach();
        this.m0 = null;
    }

    @Override // androidx.fragment.app.Fragment
    public void onPause() {
        super.onPause();
        qc qcVar = this.k0;
        if (qcVar != null) {
            fs1.d(qcVar);
            if (qcVar.e()) {
                qc qcVar2 = this.k0;
                fs1.d(qcVar2);
                qcVar2.d();
            }
        }
    }

    @Override // net.safemoon.androidwallet.fragments.common.BaseMainFragment, defpackage.qn, androidx.fragment.app.Fragment
    public void onViewCreated(View view, Bundle bundle) {
        AppCompatEditText appCompatEditText;
        AppCompatEditText appCompatEditText2;
        AppCompatEditText appCompatEditText3;
        ContactCarouselView contactCarouselView;
        rp3 rp3Var;
        fs1.f(view, "view");
        super.onViewCreated(view, bundle);
        za1 za1Var = this.m0;
        if (za1Var != null && (rp3Var = za1Var.l) != null) {
            rp3Var.a.setOnClickListener(new View.OnClickListener() { // from class: kk3
                @Override // android.view.View.OnClickListener
                public final void onClick(View view2) {
                    SendToNftFragment.Y(SendToNftFragment.this, view2);
                }
            });
            AppCompatTextView appCompatTextView = rp3Var.c;
            appCompatTextView.setText(getString(R.string.send) + ' ' + ((Object) Q().getName()));
        }
        final za1 za1Var2 = this.m0;
        if (za1Var2 != null) {
            ij4 ij4Var = za1Var2.m;
            fs1.e(ij4Var, "viewTilEndLayout");
            AppCompatEditText appCompatEditText4 = za1Var2.f;
            fs1.e(appCompatEditText4, "etContactAddress");
            cj4.o(ij4Var, appCompatEditText4, new SendToNftFragment$onViewCreated$2$1(this, za1Var2), new SendToNftFragment$onViewCreated$2$2(this), new SendToNftFragment$onViewCreated$2$3(this));
            com.bumptech.glide.a.u(za1Var2.h).y(Q().getImageUrl()).L0(new c(za1Var2)).I0(za1Var2.h);
            za1Var2.c.setOnClickListener(new View.OnClickListener() { // from class: ik3
                @Override // android.view.View.OnClickListener
                public final void onClick(View view2) {
                    SendToNftFragment.Z(za1.this, this, view2);
                }
            });
            za1Var2.b.setOnClickListener(new View.OnClickListener() { // from class: mk3
                @Override // android.view.View.OnClickListener
                public final void onClick(View view2) {
                    SendToNftFragment.b0(SendToNftFragment.this, view2);
                }
            });
            za1Var2.g.setOnClickListener(new View.OnClickListener() { // from class: jk3
                @Override // android.view.View.OnClickListener
                public final void onClick(View view2) {
                    SendToNftFragment.c0(SendToNftFragment.this, view2);
                }
            });
        }
        za1 za1Var3 = this.m0;
        if (za1Var3 != null && (contactCarouselView = za1Var3.e) != null) {
            contactCarouselView.setOnItemSelectedChangedListener(new d());
        }
        za1 za1Var4 = this.m0;
        if (za1Var4 != null && (appCompatEditText3 = za1Var4.k) != null) {
            appCompatEditText3.addTextChangedListener(new e());
        }
        za1 za1Var5 = this.m0;
        if (za1Var5 != null && (appCompatEditText2 = za1Var5.f) != null) {
            appCompatEditText2.addTextChangedListener(new f());
        }
        za1 za1Var6 = this.m0;
        Editable editable = null;
        if (za1Var6 != null && (appCompatEditText = za1Var6.f) != null) {
            editable = appCompatEditText.getText();
        }
        M(editable);
        S();
    }
}
