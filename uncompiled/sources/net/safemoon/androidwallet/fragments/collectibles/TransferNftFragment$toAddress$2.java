package net.safemoon.androidwallet.fragments.collectibles;

import kotlin.jvm.internal.Lambda;
import org.web3j.abi.datatypes.Address;

/* compiled from: TransferNftFragment.kt */
/* loaded from: classes2.dex */
public final class TransferNftFragment$toAddress$2 extends Lambda implements rc1<String> {
    public final /* synthetic */ TransferNftFragment this$0;

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public TransferNftFragment$toAddress$2(TransferNftFragment transferNftFragment) {
        super(0);
        this.this$0 = transferNftFragment;
    }

    @Override // defpackage.rc1
    public final String invoke() {
        return this.this$0.requireArguments().getString(Address.TYPE_NAME);
    }
}
