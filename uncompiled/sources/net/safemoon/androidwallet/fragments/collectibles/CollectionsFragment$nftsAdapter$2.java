package net.safemoon.androidwallet.fragments.collectibles;

import kotlin.jvm.internal.Lambda;
import net.safemoon.androidwallet.model.collectible.RoomCollection;
import net.safemoon.androidwallet.model.collectible.RoomNFT;

/* compiled from: CollectionsFragment.kt */
/* loaded from: classes2.dex */
public final class CollectionsFragment$nftsAdapter$2 extends Lambda implements rc1<fg2> {
    public final /* synthetic */ CollectionsFragment this$0;

    /* compiled from: CollectionsFragment.kt */
    /* renamed from: net.safemoon.androidwallet.fragments.collectibles.CollectionsFragment$nftsAdapter$2$1  reason: invalid class name */
    /* loaded from: classes2.dex */
    public static final class AnonymousClass1 extends Lambda implements tc1<RoomNFT, te4> {
        public final /* synthetic */ CollectionsFragment this$0;

        /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
        public AnonymousClass1(CollectionsFragment collectionsFragment) {
            super(1);
            this.this$0 = collectionsFragment;
        }

        @Override // defpackage.tc1
        public /* bridge */ /* synthetic */ te4 invoke(RoomNFT roomNFT) {
            invoke2(roomNFT);
            return te4.a;
        }

        /* renamed from: invoke  reason: avoid collision after fix types in other method */
        public final void invoke2(RoomNFT roomNFT) {
            RoomCollection x;
            fs1.f(roomNFT, "it");
            CollectionsFragment collectionsFragment = this.this$0;
            x = collectionsFragment.x();
            collectionsFragment.J(x, roomNFT);
        }
    }

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public CollectionsFragment$nftsAdapter$2(CollectionsFragment collectionsFragment) {
        super(0);
        this.this$0 = collectionsFragment;
    }

    /* JADX WARN: Can't rename method to resolve collision */
    @Override // defpackage.rc1
    public final fg2 invoke() {
        return new fg2(new AnonymousClass1(this.this$0));
    }
}
