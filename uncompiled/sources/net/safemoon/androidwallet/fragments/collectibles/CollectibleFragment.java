package net.safemoon.androidwallet.fragments.collectibles;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.res.ColorStateList;
import android.net.Uri;
import android.os.Bundle;
import android.text.TextPaint;
import android.text.style.ClickableSpan;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import androidx.appcompat.widget.AppCompatImageView;
import androidx.fragment.app.FragmentViewModelLazyKt;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.recyclerview.widget.k;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;
import com.google.android.material.button.MaterialButton;
import com.google.android.material.textview.MaterialTextView;
import defpackage.c10;
import defpackage.lt2;
import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import kotlin.Pair;
import net.safemoon.androidwallet.R;
import net.safemoon.androidwallet.common.TokenType;
import net.safemoon.androidwallet.dialogs.AnchorSwitchWallet;
import net.safemoon.androidwallet.dialogs.ProgressLoading;
import net.safemoon.androidwallet.domain.listener.dalog.OnSelectTokenTypeClickListener;
import net.safemoon.androidwallet.fragments.collectibles.CollectibleFragment;
import net.safemoon.androidwallet.fragments.common.BaseMainFragment;
import net.safemoon.androidwallet.model.collectible.RoomCollection;
import net.safemoon.androidwallet.model.collectible.RoomCollectionAndNft;
import net.safemoon.androidwallet.model.collectible.TYPE_DELETE_NFT;
import net.safemoon.androidwallet.model.wallets.Wallet;
import net.safemoon.androidwallet.viewmodels.CollectibleViewModel;
import net.safemoon.androidwallet.viewmodels.MultiWalletViewModel;

/* compiled from: CollectibleFragment.kt */
/* loaded from: classes2.dex */
public final class CollectibleFragment extends BaseMainFragment {
    public static final a p0 = new a(null);
    public static String q0 = "https://www.safemoon.education/sfm-wallet-nfts";
    public u91 i0;
    public final sy1 j0 = zy1.a(new CollectibleFragment$address$2(this));
    public final sy1 k0 = zy1.a(new CollectibleFragment$collectionsAdapter$2(this));
    public final sy1 l0 = FragmentViewModelLazyKt.a(this, d53.b(CollectibleViewModel.class), new CollectibleFragment$special$$inlined$viewModels$default$2(new CollectibleFragment$special$$inlined$viewModels$default$1(this)), null);
    public final sy1 m0 = FragmentViewModelLazyKt.a(this, d53.b(MultiWalletViewModel.class), new CollectibleFragment$special$$inlined$viewModels$default$4(new CollectibleFragment$special$$inlined$viewModels$default$3(this)), null);
    public final sy1 n0 = zy1.a(new CollectibleFragment$anchorCollectionsOption$2(this));
    public OnSelectTokenTypeClickListener o0;

    /* compiled from: CollectibleFragment.kt */
    /* loaded from: classes2.dex */
    public static final class a {
        public a() {
        }

        public /* synthetic */ a(qi0 qi0Var) {
            this();
        }

        public final String a() {
            return CollectibleFragment.q0;
        }
    }

    /* compiled from: CollectibleFragment.kt */
    /* loaded from: classes2.dex */
    public /* synthetic */ class b {
        public static final /* synthetic */ int[] a;

        static {
            int[] iArr = new int[TYPE_DELETE_NFT.values().length];
            iArr[TYPE_DELETE_NFT.VISIBLE.ordinal()] = 1;
            iArr[TYPE_DELETE_NFT.HIDE.ordinal()] = 2;
            a = iArr;
        }
    }

    /* compiled from: CollectibleFragment.kt */
    /* loaded from: classes2.dex */
    public static final class c extends fm2 {
        public c() {
            super(true);
        }

        @Override // defpackage.fm2
        public void b() {
            TYPE_DELETE_NFT value = CollectibleFragment.this.I().J().getValue();
            if (value != null && value.getValue() == 0) {
                CollectibleFragment.this.f();
            } else {
                CollectibleFragment.this.I().c0(false);
            }
        }
    }

    /* compiled from: CollectibleFragment.kt */
    /* loaded from: classes2.dex */
    public static final class d implements RecyclerView.q {
        public d() {
        }

        @Override // androidx.recyclerview.widget.RecyclerView.q
        public void a(RecyclerView recyclerView, MotionEvent motionEvent) {
            fs1.f(recyclerView, "rv");
            fs1.f(motionEvent, "e");
        }

        @Override // androidx.recyclerview.widget.RecyclerView.q
        public boolean c(RecyclerView recyclerView, MotionEvent motionEvent) {
            fs1.f(recyclerView, "rv");
            fs1.f(motionEvent, "e");
            View S = recyclerView.S(motionEvent.getX(), motionEvent.getY());
            if (CollectibleFragment.this.I().J().getValue() == TYPE_DELETE_NFT.VISIBLE && fs1.b(CollectibleFragment.this.I().I().getValue(), Boolean.TRUE) && S == null) {
                CollectibleFragment.this.I().I().postValue(Boolean.FALSE);
                return false;
            }
            return false;
        }

        @Override // androidx.recyclerview.widget.RecyclerView.q
        public void e(boolean z) {
        }
    }

    /* compiled from: CollectibleFragment.kt */
    /* loaded from: classes2.dex */
    public static final class e extends k.f {
        public e() {
        }

        @Override // androidx.recyclerview.widget.k.f
        public void A(RecyclerView.a0 a0Var, int i) {
            super.A(a0Var, i);
            if (i == 2) {
                View view = a0Var == null ? null : a0Var.itemView;
                if (view == null) {
                    return;
                }
                view.setAlpha(0.5f);
            }
        }

        @Override // androidx.recyclerview.widget.k.f
        public void B(RecyclerView.a0 a0Var, int i) {
            fs1.f(a0Var, "viewHolder");
        }

        @Override // androidx.recyclerview.widget.k.f
        public void c(RecyclerView recyclerView, RecyclerView.a0 a0Var) {
            fs1.f(recyclerView, "recyclerView");
            fs1.f(a0Var, "viewHolder");
            super.c(recyclerView, a0Var);
            a0Var.itemView.setAlpha(1.0f);
            CollectibleViewModel I = CollectibleFragment.this.I();
            List<RoomCollectionAndNft> currentList = CollectibleFragment.this.J().getCurrentList();
            fs1.e(currentList, "collectionsAdapter.currentList");
            ArrayList arrayList = new ArrayList(c20.q(currentList, 10));
            int i = 0;
            for (Object obj : currentList) {
                int i2 = i + 1;
                if (i < 0) {
                    b20.p();
                }
                arrayList.add(new Pair<>(((RoomCollectionAndNft) obj).getCollection().getId(), Integer.valueOf(i)));
                i = i2;
            }
            I.h0(arrayList);
        }

        @Override // androidx.recyclerview.widget.k.f
        public int k(RecyclerView recyclerView, RecyclerView.a0 a0Var) {
            fs1.f(recyclerView, "recyclerView");
            fs1.f(a0Var, "viewHolder");
            return k.f.s(2, 51);
        }

        @Override // androidx.recyclerview.widget.k.f
        public boolean y(RecyclerView recyclerView, RecyclerView.a0 a0Var, RecyclerView.a0 a0Var2) {
            fs1.f(recyclerView, "recyclerView");
            fs1.f(a0Var, "viewHolder");
            fs1.f(a0Var2, "target");
            CollectibleFragment.this.N(a0Var.getAbsoluteAdapterPosition(), a0Var2.getAbsoluteAdapterPosition());
            return true;
        }
    }

    /* compiled from: CollectibleFragment.kt */
    /* loaded from: classes2.dex */
    public static final class f extends OnSelectTokenTypeClickListener {
        public f(g gVar, WeakReference<Activity> weakReference, Map<String, ? extends TokenType> map) {
            super(gVar, weakReference, map);
        }

        @Override // net.safemoon.androidwallet.domain.listener.dalog.OnSelectTokenTypeClickListener
        public TokenType c() {
            TokenType value = CollectibleFragment.this.I().H().getValue();
            if (value == null) {
                Context context = CollectibleFragment.this.getContext();
                value = context == null ? null : e30.f(context);
                fs1.d(value);
            }
            return value;
        }
    }

    /* compiled from: CollectibleFragment.kt */
    /* loaded from: classes2.dex */
    public static final class g implements lt2.a {
        public g() {
        }

        @Override // defpackage.lt2.a
        public void a(TokenType tokenType) {
            fs1.f(tokenType, "token");
            CollectibleFragment.this.I().c0(false);
            CollectibleFragment.this.I().H().postValue(tokenType);
            bo3.o(CollectibleFragment.this.requireContext(), "DEFAULT_GATEWAY_COLLECTIONS", tokenType.name());
        }
    }

    /* compiled from: CollectibleFragment.kt */
    /* loaded from: classes2.dex */
    public static final class h extends ClickableSpan {
        public h() {
        }

        @Override // android.text.style.ClickableSpan
        public void onClick(View view) {
            fs1.f(view, "widget");
            CollectibleFragment.this.I().b0();
        }

        @Override // android.text.style.ClickableSpan, android.text.style.CharacterStyle
        public void updateDrawState(TextPaint textPaint) {
            fs1.f(textPaint, "ds");
            super.updateDrawState(textPaint);
            textPaint.setUnderlineText(false);
        }
    }

    public static final void O(CollectibleFragment collectibleFragment, Boolean bool) {
        fs1.f(collectibleFragment, "this$0");
        Z(collectibleFragment);
    }

    public static final void P(final CollectibleFragment collectibleFragment, List list) {
        fs1.f(collectibleFragment, "this$0");
        collectibleFragment.J().submitList(list == null ? null : j20.m0(list), new Runnable() { // from class: s00
            @Override // java.lang.Runnable
            public final void run() {
                CollectibleFragment.Q(CollectibleFragment.this);
            }
        });
    }

    public static final void Q(CollectibleFragment collectibleFragment) {
        fs1.f(collectibleFragment, "this$0");
        Z(collectibleFragment);
    }

    public static final void R(CollectibleFragment collectibleFragment, TYPE_DELETE_NFT type_delete_nft) {
        Integer valueOf;
        TextView textView;
        AppCompatImageView appCompatImageView;
        fs1.f(collectibleFragment, "this$0");
        u91 H = collectibleFragment.H();
        if (H != null && (appCompatImageView = H.b) != null) {
            appCompatImageView.setImageResource(type_delete_nft.getValue() == 0 ? R.drawable.custom_eye : R.drawable.ic_back);
        }
        int i = type_delete_nft == null ? -1 : b.a[type_delete_nft.ordinal()];
        if (i != 1) {
            valueOf = i != 2 ? null : Integer.valueOf((int) R.string.title_hidden_collections);
        } else {
            valueOf = Integer.valueOf((int) R.string.title_collections);
        }
        if (valueOf == null) {
            return;
        }
        int intValue = valueOf.intValue();
        u91 H2 = collectibleFragment.H();
        if (H2 == null || (textView = H2.k) == null) {
            return;
        }
        textView.setText(intValue);
    }

    public static final void S(CollectibleFragment collectibleFragment, TokenType tokenType) {
        ry1 ry1Var;
        ImageView imageView;
        ry1 ry1Var2;
        fs1.f(collectibleFragment, "this$0");
        if (tokenType == null) {
            return;
        }
        CollectibleViewModel.Q(collectibleFragment.I(), tokenType.getChainId(), 0, 0, 6, null);
        u91 H = collectibleFragment.H();
        MaterialTextView materialTextView = null;
        if (H != null && (ry1Var2 = H.g) != null) {
            materialTextView = ry1Var2.d;
        }
        if (materialTextView != null) {
            materialTextView.setText(tokenType.getPlaneName());
        }
        u91 H2 = collectibleFragment.H();
        if (H2 == null || (ry1Var = H2.g) == null || (imageView = ry1Var.c) == null) {
            return;
        }
        imageView.setImageResource(tokenType.getIcon());
    }

    public static final void T(u91 u91Var) {
        fs1.f(u91Var, "$it");
        u91Var.j.setRefreshing(false);
    }

    public static final void U(CollectibleFragment collectibleFragment, View view) {
        fs1.f(collectibleFragment, "this$0");
        collectibleFragment.b0();
    }

    public static final void V(CollectibleFragment collectibleFragment, View view) {
        fs1.f(collectibleFragment, "this$0");
        TYPE_DELETE_NFT value = collectibleFragment.I().J().getValue();
        boolean z = false;
        if (value != null && value.getValue() == 0) {
            z = true;
        }
        if (z) {
            zb G = collectibleFragment.G();
            Context requireContext = collectibleFragment.requireContext();
            fs1.e(requireContext, "requireContext()");
            fs1.e(view, "btn");
            G.k(requireContext, view, new CollectibleFragment$onViewCreated$2$7$1(collectibleFragment));
            return;
        }
        collectibleFragment.requireActivity().onBackPressed();
    }

    public static final void W(CollectibleFragment collectibleFragment, u91 u91Var, View view) {
        fs1.f(collectibleFragment, "this$0");
        fs1.f(u91Var, "$it");
        AnchorSwitchWallet anchorSwitchWallet = new AnchorSwitchWallet(collectibleFragment.K(), R.id.navigation_collectibles);
        Context requireContext = collectibleFragment.requireContext();
        fs1.e(requireContext, "requireContext()");
        fs1.e(view, "view");
        anchorSwitchWallet.h(requireContext, view, u91Var.f);
    }

    public static final void X(CollectibleFragment collectibleFragment, List list) {
        fs1.f(collectibleFragment, "this$0");
        collectibleFragment.M();
    }

    public static final void Y(CollectibleFragment collectibleFragment, Boolean bool) {
        fs1.f(collectibleFragment, "this$0");
        collectibleFragment.M();
    }

    /* JADX WARN: Code restructure failed: missing block: B:32:0x0058, code lost:
        if ((r0.getVisibility() == 0) == true) goto L21;
     */
    /*
        Code decompiled incorrectly, please refer to instructions dump.
        To view partially-correct code enable 'Show inconsistent code' option in preferences
    */
    public static final void Z(final net.safemoon.androidwallet.fragments.collectibles.CollectibleFragment r10) {
        /*
            Method dump skipped, instructions count: 504
            To view this dump change 'Code comments level' option to 'DEBUG'
        */
        throw new UnsupportedOperationException("Method not decompiled: net.safemoon.androidwallet.fragments.collectibles.CollectibleFragment.Z(net.safemoon.androidwallet.fragments.collectibles.CollectibleFragment):void");
    }

    public static final void a0(CollectibleFragment collectibleFragment, View view) {
        fs1.f(collectibleFragment, "this$0");
        collectibleFragment.b0();
    }

    public final zb G() {
        return (zb) this.n0.getValue();
    }

    public final u91 H() {
        return this.i0;
    }

    public final CollectibleViewModel I() {
        return (CollectibleViewModel) this.l0.getValue();
    }

    public final r10 J() {
        return (r10) this.k0.getValue();
    }

    public final MultiWalletViewModel K() {
        return (MultiWalletViewModel) this.m0.getValue();
    }

    public final void L(RoomCollection roomCollection) {
        c10.b a2 = c10.a(roomCollection);
        fs1.e(a2, "actionNavigationCollecti…              collection)");
        g(a2);
    }

    public final void M() {
        List<RoomCollectionAndNft> value;
        int d2;
        u91 u91Var = this.i0;
        AppCompatImageView appCompatImageView = u91Var == null ? null : u91Var.b;
        if (appCompatImageView == null) {
            return;
        }
        boolean z = false;
        if (I().D().getValue() != null && (!value.isEmpty())) {
            z = true;
        }
        if (!z && !G().f() && !fs1.b(I().I().getValue(), Boolean.TRUE)) {
            d2 = m70.d(requireContext(), R.color.t_all_light);
        } else {
            d2 = m70.d(requireContext(), R.color.wallet_name_color);
        }
        appCompatImageView.setImageTintList(ColorStateList.valueOf(d2));
    }

    public final void N(int i, int i2) {
        try {
            J().e(i, i2);
        } catch (Exception unused) {
        }
    }

    public final void b0() {
        startActivity(new Intent("android.intent.action.VIEW", Uri.parse(q0)));
    }

    @Override // androidx.fragment.app.Fragment
    public View onCreateView(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle) {
        fs1.f(layoutInflater, "inflater");
        u91 a2 = u91.a(LayoutInflater.from(requireContext()).inflate(R.layout.fragment_collectibles, viewGroup, false));
        this.i0 = a2;
        if (a2 == null) {
            return null;
        }
        return a2.b();
    }

    @Override // androidx.fragment.app.Fragment
    public void onPause() {
        super.onPause();
        OnSelectTokenTypeClickListener onSelectTokenTypeClickListener = this.o0;
        if (onSelectTokenTypeClickListener == null) {
            return;
        }
        onSelectTokenTypeClickListener.b();
    }

    @Override // net.safemoon.androidwallet.fragments.common.BaseMainFragment, defpackage.qn, androidx.fragment.app.Fragment
    public void onViewCreated(View view, Bundle bundle) {
        ry1 ry1Var;
        MaterialButton materialButton;
        fs1.f(view, "view");
        super.onViewCreated(view, bundle);
        if (bundle == null) {
            requireActivity().getOnBackPressedDispatcher().a(getViewLifecycleOwner(), new c());
        }
        ProgressLoading.a aVar = ProgressLoading.y0;
        String string = getString(R.string.loading);
        fs1.e(string, "getString(R.string.loading)");
        aVar.a(false, string, "");
        final u91 u91Var = this.i0;
        if (u91Var != null) {
            u91Var.i.setLayoutManager(new GridLayoutManager(getContext(), 2));
            u91Var.i.setAdapter(J());
            u91Var.i.k(new d());
            new k(new e()).g(u91Var.i);
            g gVar = new g();
            WeakReference weakReference = new WeakReference(requireActivity());
            Context applicationContext = requireContext().getApplicationContext();
            fs1.e(applicationContext, "requireContext().applicationContext");
            this.o0 = new f(gVar, weakReference, new mf1(applicationContext).get());
            u91 H = H();
            if (H != null && (ry1Var = H.g) != null && (materialButton = ry1Var.a) != null) {
                materialButton.setOnClickListener(this.o0);
            }
            u91Var.j.setOnRefreshListener(new SwipeRefreshLayout.j() { // from class: r00
                @Override // androidx.swiperefreshlayout.widget.SwipeRefreshLayout.j
                public final void onRefresh() {
                    CollectibleFragment.T(u91.this);
                }
            });
            u91Var.c.setOnClickListener(new View.OnClickListener() { // from class: y00
                @Override // android.view.View.OnClickListener
                public final void onClick(View view2) {
                    CollectibleFragment.U(CollectibleFragment.this, view2);
                }
            });
            u91Var.b.setOnClickListener(new View.OnClickListener() { // from class: a10
                @Override // android.view.View.OnClickListener
                public final void onClick(View view2) {
                    CollectibleFragment.V(CollectibleFragment.this, view2);
                }
            });
            Wallet.Companion companion = Wallet.Companion;
            String j = bo3.j(getContext(), "SAFEMOON_ACTIVE_WALLET", "");
            fs1.e(j, "getString(\n             …     \"\"\n                )");
            Wallet wallet2 = companion.toWallet(j);
            u91Var.l.setText(wallet2 == null ? null : wallet2.displayName());
            u91Var.l.setOnClickListener(new View.OnClickListener() { // from class: b10
                @Override // android.view.View.OnClickListener
                public final void onClick(View view2) {
                    CollectibleFragment.W(CollectibleFragment.this, u91Var, view2);
                }
            });
        }
        I().D().observe(getViewLifecycleOwner(), new tl2() { // from class: u00
            @Override // defpackage.tl2
            public final void onChanged(Object obj) {
                CollectibleFragment.X(CollectibleFragment.this, (List) obj);
            }
        });
        I().I().observe(getViewLifecycleOwner(), new tl2() { // from class: t00
            @Override // defpackage.tl2
            public final void onChanged(Object obj) {
                CollectibleFragment.Y(CollectibleFragment.this, (Boolean) obj);
            }
        });
        I().N().observe(getViewLifecycleOwner(), new tl2() { // from class: q00
            @Override // defpackage.tl2
            public final void onChanged(Object obj) {
                CollectibleFragment.O(CollectibleFragment.this, (Boolean) obj);
            }
        });
        I().C().observe(getViewLifecycleOwner(), new tl2() { // from class: v00
            @Override // defpackage.tl2
            public final void onChanged(Object obj) {
                CollectibleFragment.P(CollectibleFragment.this, (List) obj);
            }
        });
        I().J().observe(getViewLifecycleOwner(), new tl2() { // from class: x00
            @Override // defpackage.tl2
            public final void onChanged(Object obj) {
                CollectibleFragment.R(CollectibleFragment.this, (TYPE_DELETE_NFT) obj);
            }
        });
        I().H().observe(getViewLifecycleOwner(), new tl2() { // from class: w00
            @Override // defpackage.tl2
            public final void onChanged(Object obj) {
                CollectibleFragment.S(CollectibleFragment.this, (TokenType) obj);
            }
        });
    }
}
