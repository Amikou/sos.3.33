package net.safemoon.androidwallet.fragments.collectibles;

import kotlin.jvm.internal.Lambda;
import net.safemoon.androidwallet.R;

/* compiled from: CollectibleFragment.kt */
/* loaded from: classes2.dex */
public final class CollectibleFragment$onViewCreated$2$7$1 extends Lambda implements tc1<Integer, te4> {
    public final /* synthetic */ CollectibleFragment this$0;

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public CollectibleFragment$onViewCreated$2$7$1(CollectibleFragment collectibleFragment) {
        super(1);
        this.this$0 = collectibleFragment;
    }

    @Override // defpackage.tc1
    public /* bridge */ /* synthetic */ te4 invoke(Integer num) {
        invoke(num.intValue());
        return te4.a;
    }

    public final void invoke(int i) {
        switch (i) {
            case R.id.btnHiddenCollections /* 2131362067 */:
                this.this$0.I().b0();
                return;
            case R.id.btnHideCollections /* 2131362068 */:
                this.this$0.I().I().postValue(Boolean.valueOf(!fs1.b(this.this$0.I().I().getValue(), Boolean.TRUE)));
                return;
            default:
                return;
        }
    }
}
