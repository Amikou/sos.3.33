package net.safemoon.androidwallet.fragments.collectibles;

import android.content.Context;
import kotlin.jvm.internal.Lambda;
import net.safemoon.androidwallet.model.wallets.Wallet;

/* compiled from: CollectibleFragment.kt */
/* loaded from: classes2.dex */
public final class CollectibleFragment$address$2 extends Lambda implements rc1<String> {
    public final /* synthetic */ CollectibleFragment this$0;

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public CollectibleFragment$address$2(CollectibleFragment collectibleFragment) {
        super(0);
        this.this$0 = collectibleFragment;
    }

    @Override // defpackage.rc1
    public final String invoke() {
        String address;
        Context requireContext = this.this$0.requireContext();
        fs1.e(requireContext, "requireContext()");
        Wallet c = e30.c(requireContext);
        return (c == null || (address = c.getAddress()) == null) ? "" : address;
    }
}
