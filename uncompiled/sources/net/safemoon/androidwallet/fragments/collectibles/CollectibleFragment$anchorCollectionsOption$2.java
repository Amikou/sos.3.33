package net.safemoon.androidwallet.fragments.collectibles;

import kotlin.jvm.internal.Lambda;

/* compiled from: CollectibleFragment.kt */
/* loaded from: classes2.dex */
public final class CollectibleFragment$anchorCollectionsOption$2 extends Lambda implements rc1<zb> {
    public final /* synthetic */ CollectibleFragment this$0;

    /* compiled from: CollectibleFragment.kt */
    /* renamed from: net.safemoon.androidwallet.fragments.collectibles.CollectibleFragment$anchorCollectionsOption$2$1  reason: invalid class name */
    /* loaded from: classes2.dex */
    public static final class AnonymousClass1 extends Lambda implements rc1<te4> {
        public final /* synthetic */ CollectibleFragment this$0;

        /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
        public AnonymousClass1(CollectibleFragment collectibleFragment) {
            super(0);
            this.this$0 = collectibleFragment;
        }

        @Override // defpackage.rc1
        public /* bridge */ /* synthetic */ te4 invoke() {
            invoke2();
            return te4.a;
        }

        @Override // defpackage.rc1
        /* renamed from: invoke  reason: avoid collision after fix types in other method */
        public final void invoke2() {
            this.this$0.M();
        }
    }

    /* compiled from: CollectibleFragment.kt */
    /* renamed from: net.safemoon.androidwallet.fragments.collectibles.CollectibleFragment$anchorCollectionsOption$2$2  reason: invalid class name */
    /* loaded from: classes2.dex */
    public static final class AnonymousClass2 extends Lambda implements rc1<te4> {
        public final /* synthetic */ CollectibleFragment this$0;

        /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
        public AnonymousClass2(CollectibleFragment collectibleFragment) {
            super(0);
            this.this$0 = collectibleFragment;
        }

        @Override // defpackage.rc1
        public /* bridge */ /* synthetic */ te4 invoke() {
            invoke2();
            return te4.a;
        }

        @Override // defpackage.rc1
        /* renamed from: invoke  reason: avoid collision after fix types in other method */
        public final void invoke2() {
            this.this$0.M();
        }
    }

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public CollectibleFragment$anchorCollectionsOption$2(CollectibleFragment collectibleFragment) {
        super(0);
        this.this$0 = collectibleFragment;
    }

    @Override // defpackage.rc1
    public final zb invoke() {
        return new zb(new AnonymousClass1(this.this$0), new AnonymousClass2(this.this$0));
    }
}
