package net.safemoon.androidwallet.fragments.collectibles;

import android.content.Context;
import kotlin.jvm.internal.Lambda;
import net.safemoon.androidwallet.model.wallets.Wallet;

/* compiled from: TransferNftFragment.kt */
/* loaded from: classes2.dex */
public final class TransferNftFragment$wallet$2 extends Lambda implements rc1<Wallet> {
    public final /* synthetic */ TransferNftFragment this$0;

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public TransferNftFragment$wallet$2(TransferNftFragment transferNftFragment) {
        super(0);
        this.this$0 = transferNftFragment;
    }

    @Override // defpackage.rc1
    public final Wallet invoke() {
        Context requireContext = this.this$0.requireContext();
        fs1.e(requireContext, "requireContext()");
        return e30.c(requireContext);
    }
}
