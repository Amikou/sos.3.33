package net.safemoon.androidwallet.fragments.collectibles;

import java.io.Serializable;
import java.util.Objects;
import kotlin.jvm.internal.Lambda;
import net.safemoon.androidwallet.model.nft.NFTData;

/* compiled from: SendToNftFragment.kt */
/* loaded from: classes2.dex */
public final class SendToNftFragment$nftData$2 extends Lambda implements rc1<NFTData> {
    public final /* synthetic */ SendToNftFragment this$0;

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public SendToNftFragment$nftData$2(SendToNftFragment sendToNftFragment) {
        super(0);
        this.this$0 = sendToNftFragment;
    }

    @Override // defpackage.rc1
    public final NFTData invoke() {
        Serializable serializable = this.this$0.requireArguments().getSerializable("nftData");
        Objects.requireNonNull(serializable, "null cannot be cast to non-null type net.safemoon.androidwallet.model.nft.NFTData");
        return (NFTData) serializable;
    }
}
