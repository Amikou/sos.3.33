package net.safemoon.androidwallet.fragments.collectibles;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import androidx.appcompat.widget.AppCompatImageView;
import androidx.appcompat.widget.AppCompatTextView;
import androidx.fragment.app.FragmentActivity;
import androidx.fragment.app.FragmentViewModelLazyKt;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.recyclerview.widget.k;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;
import defpackage.z10;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import kotlin.Pair;
import net.safemoon.androidwallet.R;
import net.safemoon.androidwallet.fragments.collectibles.CollectionsFragment;
import net.safemoon.androidwallet.fragments.common.BaseMainFragment;
import net.safemoon.androidwallet.model.collectible.RoomCollection;
import net.safemoon.androidwallet.model.collectible.RoomNFT;
import net.safemoon.androidwallet.viewmodels.CollectibleViewModel;

/* compiled from: CollectionsFragment.kt */
/* loaded from: classes2.dex */
public final class CollectionsFragment extends BaseMainFragment {
    public v91 i0;
    public SwipeRefreshLayout j0;
    public final sy1 k0 = zy1.a(new CollectionsFragment$collection$2(this));
    public final sy1 l0 = FragmentViewModelLazyKt.a(this, d53.b(CollectibleViewModel.class), new CollectionsFragment$special$$inlined$viewModels$default$2(new CollectionsFragment$special$$inlined$viewModels$default$1(this)), null);
    public final sy1 m0 = zy1.a(new CollectionsFragment$nftsAdapter$2(this));

    /* compiled from: CollectionsFragment.kt */
    /* loaded from: classes2.dex */
    public static final class a extends k.f {
        public a() {
        }

        @Override // androidx.recyclerview.widget.k.f
        public void A(RecyclerView.a0 a0Var, int i) {
            super.A(a0Var, i);
            if (i == 2) {
                View view = a0Var == null ? null : a0Var.itemView;
                if (view == null) {
                    return;
                }
                view.setAlpha(0.5f);
            }
        }

        @Override // androidx.recyclerview.widget.k.f
        public void B(RecyclerView.a0 a0Var, int i) {
            fs1.f(a0Var, "viewHolder");
        }

        @Override // androidx.recyclerview.widget.k.f
        public void c(RecyclerView recyclerView, RecyclerView.a0 a0Var) {
            fs1.f(recyclerView, "recyclerView");
            fs1.f(a0Var, "viewHolder");
            super.c(recyclerView, a0Var);
            a0Var.itemView.setAlpha(1.0f);
        }

        @Override // androidx.recyclerview.widget.k.f
        public int k(RecyclerView recyclerView, RecyclerView.a0 a0Var) {
            fs1.f(recyclerView, "recyclerView");
            fs1.f(a0Var, "viewHolder");
            return k.f.s(2, 51);
        }

        @Override // androidx.recyclerview.widget.k.f
        public boolean y(RecyclerView recyclerView, RecyclerView.a0 a0Var, RecyclerView.a0 a0Var2) {
            fs1.f(recyclerView, "recyclerView");
            fs1.f(a0Var, "viewHolder");
            fs1.f(a0Var2, "target");
            CollectionsFragment.this.A(a0Var.getAbsoluteAdapterPosition(), a0Var2.getAbsoluteAdapterPosition());
            return true;
        }
    }

    public static final void B(CollectionsFragment collectionsFragment, View view) {
        fs1.f(collectionsFragment, "this$0");
        FragmentActivity activity = collectionsFragment.getActivity();
        if (activity == null) {
            return;
        }
        activity.onBackPressed();
    }

    public static final void C(CollectionsFragment collectionsFragment, View view) {
        fs1.f(collectionsFragment, "this$0");
        collectionsFragment.I();
    }

    public static final void D(CollectionsFragment collectionsFragment, View view) {
        fs1.f(collectionsFragment, "this$0");
        collectionsFragment.I();
    }

    public static final void E(CollectionsFragment collectionsFragment) {
        fs1.f(collectionsFragment, "this$0");
        SwipeRefreshLayout z = collectionsFragment.z();
        if (z == null) {
            return;
        }
        z.setRefreshing(false);
    }

    public static final void F(CollectionsFragment collectionsFragment, Boolean bool) {
        fs1.f(collectionsFragment, "this$0");
        H(collectionsFragment);
    }

    public static final void G(CollectionsFragment collectionsFragment, List list) {
        fs1.f(collectionsFragment, "this$0");
        collectionsFragment.y().b().e(list);
        H(collectionsFragment);
    }

    public static final void H(CollectionsFragment collectionsFragment) {
        v91 v91Var = collectionsFragment.i0;
        RecyclerView recyclerView = v91Var == null ? null : v91Var.e;
        if (recyclerView == null) {
            return;
        }
        recyclerView.setVisibility(collectionsFragment.y().b().b().size() <= 0 && fs1.b(collectionsFragment.w().N().getValue(), Boolean.FALSE) ? 8 : 0);
    }

    public final void A(int i, int i2) {
        List<RoomNFT> b = y().b().b();
        fs1.e(b, "differ.currentList");
        List m0 = j20.m0(b);
        Collections.swap(m0, i, i2);
        CollectibleViewModel w = w();
        ArrayList arrayList = new ArrayList(c20.q(m0, 10));
        int i3 = 0;
        for (Object obj : m0) {
            int i4 = i3 + 1;
            if (i3 < 0) {
                b20.p();
            }
            arrayList.add(new Pair<>(((RoomNFT) obj).getId(), Integer.valueOf(i3)));
            i3 = i4;
        }
        w.i0(arrayList);
    }

    public final void I() {
        startActivity(new Intent("android.intent.action.VIEW", Uri.parse(CollectibleFragment.p0.a())));
    }

    public final void J(RoomCollection roomCollection, RoomNFT roomNFT) {
        z10.b a2 = z10.a(roomCollection, roomNFT);
        fs1.e(a2, "actionNavigationCollecti…(roomCollection, nftData)");
        g(a2);
    }

    @Override // androidx.fragment.app.Fragment
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
    }

    @Override // androidx.fragment.app.Fragment
    public View onCreateView(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle) {
        fs1.f(layoutInflater, "inflater");
        v91 a2 = v91.a(LayoutInflater.from(requireContext()).inflate(R.layout.fragment_collections, viewGroup, false));
        this.i0 = a2;
        if (a2 == null) {
            return null;
        }
        return a2.b();
    }

    @Override // net.safemoon.androidwallet.fragments.common.BaseMainFragment, defpackage.qn, androidx.fragment.app.Fragment
    public void onViewCreated(View view, Bundle bundle) {
        TextView textView;
        AppCompatImageView appCompatImageView;
        AppCompatImageView appCompatImageView2;
        AppCompatTextView appCompatTextView;
        fs1.f(view, "view");
        super.onViewCreated(view, bundle);
        this.j0 = (SwipeRefreshLayout) view.findViewById(R.id.swRefresh);
        v91 v91Var = this.i0;
        AppCompatImageView appCompatImageView3 = v91Var == null ? null : v91Var.b;
        if (appCompatImageView3 != null) {
            appCompatImageView3.setVisibility(0);
        }
        v91 v91Var2 = this.i0;
        if (v91Var2 != null && (appCompatTextView = v91Var2.f) != null) {
            appCompatTextView.setText(x().getName());
        }
        v91 v91Var3 = this.i0;
        if (v91Var3 != null && (appCompatImageView2 = v91Var3.b) != null) {
            appCompatImageView2.setOnClickListener(new View.OnClickListener() { // from class: w10
                @Override // android.view.View.OnClickListener
                public final void onClick(View view2) {
                    CollectionsFragment.B(CollectionsFragment.this, view2);
                }
            });
        }
        v91 v91Var4 = this.i0;
        if (v91Var4 != null && (appCompatImageView = v91Var4.c) != null) {
            appCompatImageView.setOnClickListener(new View.OnClickListener() { // from class: v10
                @Override // android.view.View.OnClickListener
                public final void onClick(View view2) {
                    CollectionsFragment.C(CollectionsFragment.this, view2);
                }
            });
        }
        v91 v91Var5 = this.i0;
        if (v91Var5 != null && (textView = v91Var5.d) != null) {
            textView.setOnClickListener(new View.OnClickListener() { // from class: x10
                @Override // android.view.View.OnClickListener
                public final void onClick(View view2) {
                    CollectionsFragment.D(CollectionsFragment.this, view2);
                }
            });
        }
        SwipeRefreshLayout swipeRefreshLayout = this.j0;
        if (swipeRefreshLayout != null) {
            swipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.j() { // from class: y10
                @Override // androidx.swiperefreshlayout.widget.SwipeRefreshLayout.j
                public final void onRefresh() {
                    CollectionsFragment.E(CollectionsFragment.this);
                }
            });
        }
        v91 v91Var6 = this.i0;
        if (v91Var6 == null) {
            return;
        }
        v91Var6.e.setLayoutManager(new GridLayoutManager(getContext(), 2));
        v91Var6.e.setAdapter(y());
        new k(new a()).g(v91Var6.e);
        w().N().observe(getViewLifecycleOwner(), new tl2() { // from class: t10
            @Override // defpackage.tl2
            public final void onChanged(Object obj) {
                CollectionsFragment.F(CollectionsFragment.this, (Boolean) obj);
            }
        });
        CollectibleViewModel.t(w(), x(), 0, 0, 6, null).observe(getViewLifecycleOwner(), new tl2() { // from class: u10
            @Override // defpackage.tl2
            public final void onChanged(Object obj) {
                CollectionsFragment.G(CollectionsFragment.this, (List) obj);
            }
        });
    }

    public final CollectibleViewModel w() {
        return (CollectibleViewModel) this.l0.getValue();
    }

    public final RoomCollection x() {
        return (RoomCollection) this.k0.getValue();
    }

    public final fg2 y() {
        return (fg2) this.m0.getValue();
    }

    public final SwipeRefreshLayout z() {
        return this.j0;
    }
}
