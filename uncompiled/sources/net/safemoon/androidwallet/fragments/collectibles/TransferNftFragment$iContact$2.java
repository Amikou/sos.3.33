package net.safemoon.androidwallet.fragments.collectibles;

import java.io.Serializable;
import java.util.Locale;
import java.util.Objects;
import kotlin.jvm.internal.Lambda;
import net.safemoon.androidwallet.model.contact.abstraction.IContact;

/* compiled from: TransferNftFragment.kt */
/* loaded from: classes2.dex */
public final class TransferNftFragment$iContact$2 extends Lambda implements rc1<IContact> {
    public final /* synthetic */ TransferNftFragment this$0;

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public TransferNftFragment$iContact$2(TransferNftFragment transferNftFragment) {
        super(0);
        this.this$0 = transferNftFragment;
    }

    @Override // defpackage.rc1
    public final IContact invoke() {
        String I;
        String lowerCase;
        Serializable serializable = this.this$0.requireArguments().getSerializable("iContact");
        if (serializable != null) {
            IContact iContact = (IContact) serializable;
            I = this.this$0.I();
            if (I == null) {
                lowerCase = null;
            } else {
                lowerCase = I.toLowerCase(Locale.ROOT);
                fs1.e(lowerCase, "(this as java.lang.Strin….toLowerCase(Locale.ROOT)");
            }
            String address = iContact.getAddress();
            Objects.requireNonNull(address, "null cannot be cast to non-null type java.lang.String");
            String lowerCase2 = address.toLowerCase(Locale.ROOT);
            fs1.e(lowerCase2, "(this as java.lang.Strin….toLowerCase(Locale.ROOT)");
            if (fs1.b(lowerCase, lowerCase2)) {
                return iContact;
            }
            return null;
        }
        return null;
    }
}
