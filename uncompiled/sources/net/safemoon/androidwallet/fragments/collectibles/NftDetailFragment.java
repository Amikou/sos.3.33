package net.safemoon.androidwallet.fragments.collectibles;

import android.annotation.SuppressLint;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Matrix;
import android.graphics.RectF;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewParent;
import android.view.Window;
import android.view.WindowManager;
import android.webkit.URLUtil;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.TextView;
import androidx.appcompat.widget.AppCompatImageView;
import androidx.fragment.app.FragmentActivity;
import androidx.fragment.app.FragmentViewModelLazyKt;
import androidx.media3.common.q;
import androidx.media3.ui.PlayerView;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import com.bumptech.glide.load.DataSource;
import com.bumptech.glide.load.engine.GlideException;
import com.github.mikephil.charting.utils.Utils;
import com.google.android.material.button.MaterialButton;
import com.google.android.material.chip.Chip;
import defpackage.dg2;
import java.util.ArrayList;
import net.safemoon.androidwallet.R;
import net.safemoon.androidwallet.common.TokenType;
import net.safemoon.androidwallet.fragments.collectibles.NftDetailFragment;
import net.safemoon.androidwallet.fragments.common.BaseMainFragment;
import net.safemoon.androidwallet.model.collectible.AssetContract;
import net.safemoon.androidwallet.model.collectible.RoomCollection;
import net.safemoon.androidwallet.model.collectible.RoomNFT;
import net.safemoon.androidwallet.model.nft.NFTBalance;
import net.safemoon.androidwallet.model.nft.NFTData;
import net.safemoon.androidwallet.model.nft.NFTType;
import net.safemoon.androidwallet.model.wallets.Wallet;
import net.safemoon.androidwallet.viewmodels.CollectibleViewModel;
import net.safemoon.androidwallet.views.CustomVideoPlayer;
import net.safemoon.androidwallet.views.zoomImage.ZoomageView;

/* compiled from: NftDetailFragment.kt */
/* loaded from: classes2.dex */
public final class NftDetailFragment extends BaseMainFragment {
    public boolean k0;
    public n7 p0;
    public CustomVideoPlayer r0;
    public Dialog s0;
    public im1 t0;
    public final String i0 = "ERC";
    public final String j0 = "ERC-";
    public final sy1 l0 = zy1.a(new NftDetailFragment$collection$2(this));
    public final sy1 m0 = zy1.a(new NftDetailFragment$roomNFT$2(this));
    public final sy1 n0 = zy1.a(new NftDetailFragment$nftData$2(this));
    public final sy1 o0 = zy1.a(new NftDetailFragment$wallet$2(this));
    public final sy1 q0 = FragmentViewModelLazyKt.a(this, d53.b(CollectibleViewModel.class), new NftDetailFragment$special$$inlined$viewModels$default$2(new NftDetailFragment$special$$inlined$viewModels$default$1(this)), null);

    /* compiled from: NftDetailFragment.kt */
    /* loaded from: classes2.dex */
    public static final class a extends Dialog {
        public a(Context context) {
            super(context, 16973834);
        }

        @Override // android.app.Dialog
        public void onBackPressed() {
            if (NftDetailFragment.this.k0) {
                NftDetailFragment.this.D();
            }
            super.onBackPressed();
        }
    }

    /* compiled from: NftDetailFragment.kt */
    /* loaded from: classes2.dex */
    public static final class b implements j73<Drawable> {
        public final /* synthetic */ ZoomageView a;
        public final /* synthetic */ NftDetailFragment f0;

        public b(ZoomageView zoomageView, NftDetailFragment nftDetailFragment) {
            this.a = zoomageView;
            this.f0 = nftDetailFragment;
        }

        public static final void c(NftDetailFragment nftDetailFragment, ZoomageView zoomageView, Drawable drawable) {
            fs1.f(nftDetailFragment, "this$0");
            if (nftDetailFragment.isVisible()) {
                zoomageView.setScaleType(ImageView.ScaleType.MATRIX);
                Matrix matrix = new Matrix();
                matrix.setRectToRect(new RectF(Utils.FLOAT_EPSILON, Utils.FLOAT_EPSILON, drawable == null ? 0.0f : drawable.getIntrinsicWidth(), drawable == null ? 0.0f : drawable.getIntrinsicHeight()), new RectF(Utils.FLOAT_EPSILON, Utils.FLOAT_EPSILON, zoomageView.getWidth(), zoomageView.getHeight()), Matrix.ScaleToFit.CENTER);
                te4 te4Var = te4.a;
                zoomageView.setImageMatrix(matrix);
            }
        }

        @Override // defpackage.j73
        /* renamed from: b */
        public boolean n(final Drawable drawable, Object obj, i34<Drawable> i34Var, DataSource dataSource, boolean z) {
            final ZoomageView zoomageView = this.a;
            final NftDetailFragment nftDetailFragment = this.f0;
            zoomageView.postDelayed(new Runnable() { // from class: cg2
                @Override // java.lang.Runnable
                public final void run() {
                    NftDetailFragment.b.c(NftDetailFragment.this, zoomageView, drawable);
                }
            }, 100L);
            return false;
        }

        @Override // defpackage.j73
        public boolean i(GlideException glideException, Object obj, i34<Drawable> i34Var, boolean z) {
            return false;
        }
    }

    /* compiled from: NftDetailFragment.kt */
    /* loaded from: classes2.dex */
    public static final class c extends CustomVideoPlayer.b {
        public final /* synthetic */ CustomVideoPlayer a;
        public final /* synthetic */ NftDetailFragment b;

        public c(CustomVideoPlayer customVideoPlayer, NftDetailFragment nftDetailFragment) {
            this.a = customVideoPlayer;
            this.b = nftDetailFragment;
        }

        @Override // net.safemoon.androidwallet.views.CustomVideoPlayer.a
        public void i(int i) {
            n7 n7Var;
            ii4 ii4Var;
            if (i != 3) {
                return;
            }
            q player = this.a.getPlayer();
            if ((player == null ? 0L : player.R()) <= 0 || (n7Var = this.b.p0) == null || (ii4Var = n7Var.d) == null) {
                return;
            }
            ZoomageView zoomageView = ii4Var.e;
            fs1.e(zoomageView, "it.zoomImage");
            zoomageView.setVisibility(8);
            CustomVideoPlayer customVideoPlayer = ii4Var.d;
            fs1.e(customVideoPlayer, "it.videoView");
            customVideoPlayer.setVisibility(0);
        }
    }

    public static final void K(NftDetailFragment nftDetailFragment, DialogInterface dialogInterface) {
        fs1.f(nftDetailFragment, "this$0");
        FragmentActivity activity = nftDetailFragment.getActivity();
        if (activity == null) {
            return;
        }
        activity.setRequestedOrientation(2);
    }

    public static final void L(NftDetailFragment nftDetailFragment, DialogInterface dialogInterface) {
        fs1.f(nftDetailFragment, "this$0");
        FragmentActivity activity = nftDetailFragment.getActivity();
        if (activity == null) {
            return;
        }
        activity.setRequestedOrientation(1);
    }

    public static final void M(NftDetailFragment nftDetailFragment, NFTBalance nFTBalance) {
        ii4 ii4Var;
        ii4 ii4Var2;
        String string;
        fs1.f(nftDetailFragment, "this$0");
        if (nFTBalance != null) {
            n7 n7Var = nftDetailFragment.p0;
            if (n7Var == null || (ii4Var2 = n7Var.d) == null) {
                return;
            }
            ii4Var2.a.setEnabled(nFTBalance.getBalance() > 0);
            Chip chip = ii4Var2.c;
            fs1.e(chip, "txtError");
            chip.setVisibility(nFTBalance.getBalance() <= 0 ? 0 : 8);
            if (nFTBalance.getBalance() <= 0) {
                Chip chip2 = ii4Var2.c;
                if (nFTBalance.getType() == NFTType.ERC721) {
                    string = nftDetailFragment.getString(R.string.nft_transfer_balance_error_erc721);
                } else {
                    string = nftDetailFragment.getString(R.string.nft_transfer_balance_error_erc1155);
                }
                chip2.setText(string);
                return;
            }
            return;
        }
        n7 n7Var2 = nftDetailFragment.p0;
        MaterialButton materialButton = null;
        if (n7Var2 != null && (ii4Var = n7Var2.d) != null) {
            materialButton = ii4Var.a;
        }
        if (materialButton == null) {
            return;
        }
        materialButton.setEnabled(false);
    }

    public static final void N(NftDetailFragment nftDetailFragment, TextView textView, View view) {
        fs1.f(nftDetailFragment, "this$0");
        nftDetailFragment.V(textView.getText().toString());
    }

    public static final void O(NftDetailFragment nftDetailFragment, boolean z) {
        fs1.f(nftDetailFragment, "this$0");
        if (z) {
            nftDetailFragment.U();
        } else {
            nftDetailFragment.D();
        }
    }

    public static final void P(NftDetailFragment nftDetailFragment, View view) {
        fs1.f(nftDetailFragment, "this$0");
        FragmentActivity activity = nftDetailFragment.getActivity();
        if (activity == null) {
            return;
        }
        activity.onBackPressed();
    }

    public static final void Q(NftDetailFragment nftDetailFragment, View view) {
        fs1.f(nftDetailFragment, "this$0");
        nftDetailFragment.T();
    }

    public static final void R(NftDetailFragment nftDetailFragment, View view) {
        fs1.f(nftDetailFragment, "this$0");
        FragmentActivity requireActivity = nftDetailFragment.requireActivity();
        fs1.e(requireActivity, "requireActivity()");
        StringBuilder sb = new StringBuilder();
        sb.append((Object) nftDetailFragment.G().getImageUrl());
        sb.append('\n');
        sb.append((Object) nftDetailFragment.G().getName());
        j7.e(requireActivity, null, sb.toString(), null);
    }

    public static final void S(NftDetailFragment nftDetailFragment, View view) {
        fs1.f(nftDetailFragment, "this$0");
        dg2.b a2 = dg2.a(nftDetailFragment.G());
        fs1.e(a2, "actionNftDetailToSendto(nftData)");
        nftDetailFragment.g(a2);
    }

    public final void D() {
        ii4 ii4Var;
        FrameLayout frameLayout;
        if (this.r0 != null) {
            Dialog dialog = this.s0;
            if (dialog != null && dialog.isShowing()) {
                CustomVideoPlayer customVideoPlayer = this.r0;
                ViewParent parent = customVideoPlayer == null ? null : customVideoPlayer.getParent();
                ViewGroup viewGroup = parent instanceof ViewGroup ? (ViewGroup) parent : null;
                if (viewGroup != null) {
                    viewGroup.removeView(this.r0);
                }
                n7 n7Var = this.p0;
                if (n7Var != null && (ii4Var = n7Var.d) != null && (frameLayout = ii4Var.b) != null) {
                    frameLayout.addView(this.r0);
                }
                this.k0 = false;
                Dialog dialog2 = this.s0;
                if (dialog2 == null) {
                    return;
                }
                dialog2.dismiss();
            }
        }
    }

    public final CollectibleViewModel E() {
        return (CollectibleViewModel) this.q0.getValue();
    }

    public final RoomCollection F() {
        return (RoomCollection) this.l0.getValue();
    }

    public final NFTData G() {
        return (NFTData) this.n0.getValue();
    }

    public final RoomNFT H() {
        return (RoomNFT) this.m0.getValue();
    }

    public final Wallet I() {
        return (Wallet) this.o0.getValue();
    }

    public final void J() {
        a aVar = new a(requireContext());
        aVar.requestWindowFeature(1);
        if (Build.VERSION.SDK_INT >= 28) {
            Window window = aVar.getWindow();
            if (window != null) {
                window.setFlags(256, 256);
            }
            Window window2 = aVar.getWindow();
            WindowManager.LayoutParams attributes = window2 == null ? null : window2.getAttributes();
            if (attributes != null) {
                attributes.layoutInDisplayCutoutMode = 1;
            }
        } else {
            Window window3 = aVar.getWindow();
            if (window3 != null) {
                window3.setLayout(-1, -1);
            }
        }
        aVar.setOnShowListener(new DialogInterface.OnShowListener() { // from class: tf2
            @Override // android.content.DialogInterface.OnShowListener
            public final void onShow(DialogInterface dialogInterface) {
                NftDetailFragment.K(NftDetailFragment.this, dialogInterface);
            }
        });
        aVar.setOnDismissListener(new DialogInterface.OnDismissListener() { // from class: sf2
            @Override // android.content.DialogInterface.OnDismissListener
            public final void onDismiss(DialogInterface dialogInterface) {
                NftDetailFragment.L(NftDetailFragment.this, dialogInterface);
            }
        });
        te4 te4Var = te4.a;
        this.s0 = aVar;
    }

    public final void T() {
        String openSeaUrl = G().getOpenSeaUrl();
        if (openSeaUrl == null) {
            return;
        }
        startActivity(new Intent("android.intent.action.VIEW", Uri.parse(openSeaUrl)));
    }

    @SuppressLint({"SourceLockedOrientationActivity"})
    public final void U() {
        CustomVideoPlayer customVideoPlayer = this.r0;
        if (customVideoPlayer != null) {
            ViewParent parent = customVideoPlayer == null ? null : customVideoPlayer.getParent();
            ViewGroup viewGroup = parent instanceof ViewGroup ? (ViewGroup) parent : null;
            if (viewGroup != null) {
                viewGroup.removeView(this.r0);
            }
            Dialog dialog = this.s0;
            if (dialog != null) {
                CustomVideoPlayer customVideoPlayer2 = this.r0;
                fs1.d(customVideoPlayer2);
                dialog.addContentView(customVideoPlayer2, new ViewGroup.LayoutParams(-1, -1));
            }
            this.k0 = true;
            Dialog dialog2 = this.s0;
            if (dialog2 == null) {
                return;
            }
            dialog2.show();
        }
    }

    public final void V(String str) {
        if (str == null) {
            return;
        }
        startActivity(new Intent("android.intent.action.VIEW", Uri.parse(str)));
    }

    @Override // androidx.fragment.app.Fragment
    public View onCreateView(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle) {
        fs1.f(layoutInflater, "inflater");
        n7 c2 = n7.c(layoutInflater, viewGroup, false);
        this.p0 = c2;
        if (c2 == null) {
            return null;
        }
        return c2.b();
    }

    @Override // androidx.fragment.app.Fragment
    public void onDetach() {
        super.onDetach();
        this.p0 = null;
    }

    @Override // net.safemoon.androidwallet.fragments.common.BaseMainFragment, defpackage.qn, androidx.fragment.app.Fragment
    public void onViewCreated(View view, Bundle bundle) {
        ii4 ii4Var;
        CustomVideoPlayer customVideoPlayer;
        ii4 ii4Var2;
        MaterialButton materialButton;
        AppCompatImageView appCompatImageView;
        fs1.f(view, "view");
        super.onViewCreated(view, bundle);
        CollectibleViewModel E = E();
        Wallet I = I();
        AssetContract assetContract = G().getAssetContract();
        String address = assetContract == null ? null : assetContract.getAddress();
        AssetContract assetContract2 = G().getAssetContract();
        this.t0 = E.O(I, address, assetContract2 == null ? null : assetContract2.getSchema_name(), G().getChainID());
        J();
        RecyclerView recyclerView = (RecyclerView) view.findViewById(R.id.propertyRecycle);
        recyclerView.setLayoutManager(new GridLayoutManager(getContext(), 3));
        if (G().getProperties() != null) {
            ArrayList<NFTData.Property> properties = G().getProperties();
            fs1.d(properties);
            recyclerView.setAdapter(new pc2(properties));
        }
        ZoomageView zoomageView = (ZoomageView) view.findViewById(R.id.zoom_image);
        TextView textView = (TextView) view.findViewById(R.id.txtDescription);
        final TextView textView2 = (TextView) view.findViewById(R.id.nftSource);
        TextView textView3 = (TextView) view.findViewById(R.id.title);
        String imageUrl = G().getImageUrl();
        int i = R.drawable.safemoon;
        if (imageUrl != null) {
            Integer chainID = G().getChainID();
            if (chainID != null) {
                i = TokenType.Companion.b(chainID.intValue()).getIcon();
            }
            com.bumptech.glide.a.u(zoomageView).y(G().getImageUrl()).e0(i).u0(new NftDetailFragment$onViewCreated$1(zoomageView, this)).I0(zoomageView);
        } else if (G().getImageData() != null) {
            Integer chainID2 = G().getChainID();
            if (chainID2 != null) {
                i = TokenType.Companion.b(chainID2.intValue()).getIcon();
            }
            k73 u = com.bumptech.glide.a.u(zoomageView);
            String imageData = G().getImageData();
            fs1.d(imageData);
            u.z(mu3.a(imageData)).e0(i).u0(new b(zoomageView, this)).I0(zoomageView);
        }
        view.findViewById(R.id.btnBack).setOnClickListener(new View.OnClickListener() { // from class: xf2
            @Override // android.view.View.OnClickListener
            public final void onClick(View view2) {
                NftDetailFragment.P(NftDetailFragment.this, view2);
            }
        });
        if (URLUtil.isValidUrl(G().getOpenSeaUrl())) {
            n7 n7Var = this.p0;
            AppCompatImageView appCompatImageView2 = n7Var == null ? null : n7Var.b;
            if (appCompatImageView2 != null) {
                appCompatImageView2.setVisibility(0);
            }
            view.findViewById(R.id.btnOpen).setOnClickListener(new View.OnClickListener() { // from class: uf2
                @Override // android.view.View.OnClickListener
                public final void onClick(View view2) {
                    NftDetailFragment.Q(NftDetailFragment.this, view2);
                }
            });
        } else {
            n7 n7Var2 = this.p0;
            AppCompatImageView appCompatImageView3 = n7Var2 == null ? null : n7Var2.b;
            if (appCompatImageView3 != null) {
                appCompatImageView3.setVisibility(8);
            }
        }
        n7 n7Var3 = this.p0;
        if (n7Var3 != null && (appCompatImageView = n7Var3.c) != null) {
            appCompatImageView.setOnClickListener(new View.OnClickListener() { // from class: vf2
                @Override // android.view.View.OnClickListener
                public final void onClick(View view2) {
                    NftDetailFragment.R(NftDetailFragment.this, view2);
                }
            });
        }
        n7 n7Var4 = this.p0;
        if (n7Var4 != null && (ii4Var2 = n7Var4.d) != null && (materialButton = ii4Var2.a) != null) {
            materialButton.setOnClickListener(new View.OnClickListener() { // from class: wf2
                @Override // android.view.View.OnClickListener
                public final void onClick(View view2) {
                    NftDetailFragment.S(NftDetailFragment.this, view2);
                }
            });
        }
        TextView textView4 = (TextView) view.findViewById(R.id.contract_address);
        AssetContract assetContract3 = G().getAssetContract();
        textView4.setText(assetContract3 == null ? null : assetContract3.getAddress());
        ((TextView) view.findViewById(R.id.token_id)).setText(G().getTokenId());
        AssetContract assetContract4 = G().getAssetContract();
        String schema_name = assetContract4 == null ? null : assetContract4.getSchema_name();
        ((TextView) view.findViewById(R.id.token_standard)).setText(schema_name == null ? null : dv3.D(schema_name, this.i0, this.j0, false, 4, null));
        Integer chainID3 = G().getChainID();
        ((TextView) view.findViewById(R.id.network)).setText(chainID3 != null ? TokenType.Companion.b(chainID3.intValue()).getTitle() : null);
        gb2<NFTBalance> gb2Var = new gb2<>();
        E().o(G().getTokenId(), this.t0, gb2Var);
        gb2Var.observe(getViewLifecycleOwner(), new tl2() { // from class: rf2
            @Override // defpackage.tl2
            public final void onChanged(Object obj) {
                NftDetailFragment.M(NftDetailFragment.this, (NFTBalance) obj);
            }
        });
        if (textView != null) {
            textView.setText(G().getDescription());
        }
        if (textView2 != null) {
            textView2.setText(G().getOpenSeaUrl());
        }
        if (textView3 != null) {
            textView3.setText(G().getName());
        }
        if (textView2 != null) {
            textView2.setOnClickListener(new View.OnClickListener() { // from class: yf2
                @Override // android.view.View.OnClickListener
                public final void onClick(View view2) {
                    NftDetailFragment.N(NftDetailFragment.this, textView2, view2);
                }
            });
        }
        n7 n7Var5 = this.p0;
        if (n7Var5 == null || (ii4Var = n7Var5.d) == null || (customVideoPlayer = ii4Var.d) == null) {
            return;
        }
        this.r0 = customVideoPlayer;
        customVideoPlayer.setFullscreenButtonClickListener(new PlayerView.c() { // from class: zf2
            @Override // androidx.media3.ui.PlayerView.c
            public final void a(boolean z) {
                NftDetailFragment.O(NftDetailFragment.this, z);
            }
        });
        customVideoPlayer.setIPlayerListener(new c(customVideoPlayer, this));
        customVideoPlayer.setPlayURL(G().getAnimationUrl());
    }
}
