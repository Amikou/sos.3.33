package net.safemoon.androidwallet.fragments.collectibles;

import android.app.Dialog;
import android.content.DialogInterface;
import java.lang.ref.WeakReference;
import kotlin.jvm.internal.Lambda;
import net.safemoon.androidwallet.R;
import net.safemoon.androidwallet.viewmodels.CollectibleViewModel;

/* compiled from: TransferNftFragment.kt */
/* loaded from: classes2.dex */
public final class TransferNftFragment$startTimer$1$onFinish$1$1 extends Lambda implements rc1<te4> {
    public final /* synthetic */ TransferNftFragment this$0;

    /* compiled from: TransferNftFragment.kt */
    /* renamed from: net.safemoon.androidwallet.fragments.collectibles.TransferNftFragment$startTimer$1$onFinish$1$1$1  reason: invalid class name */
    /* loaded from: classes2.dex */
    public static final class AnonymousClass1 extends Lambda implements tc1<DialogInterface, te4> {
        public final /* synthetic */ TransferNftFragment this$0;

        /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
        public AnonymousClass1(TransferNftFragment transferNftFragment) {
            super(1);
            this.this$0 = transferNftFragment;
        }

        @Override // defpackage.tc1
        public /* bridge */ /* synthetic */ te4 invoke(DialogInterface dialogInterface) {
            invoke2(dialogInterface);
            return te4.a;
        }

        /* renamed from: invoke  reason: avoid collision after fix types in other method */
        public final void invoke2(DialogInterface dialogInterface) {
            CollectibleViewModel E;
            String I;
            im1 im1Var;
            fs1.f(dialogInterface, "it");
            try {
                E = this.this$0.E();
                I = this.this$0.I();
                fs1.d(I);
                fs1.e(I, "toAddress!!");
                String tokenId = this.this$0.G().getTokenId();
                fs1.d(tokenId);
                im1Var = this.this$0.p0;
                E.p(I, tokenId, im1Var);
            } catch (Exception unused) {
            }
        }
    }

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public TransferNftFragment$startTimer$1$onFinish$1$1(TransferNftFragment transferNftFragment) {
        super(0);
        this.this$0 = transferNftFragment;
    }

    @Override // defpackage.rc1
    public /* bridge */ /* synthetic */ te4 invoke() {
        invoke2();
        return te4.a;
    }

    @Override // defpackage.rc1
    /* renamed from: invoke  reason: avoid collision after fix types in other method */
    public final void invoke2() {
        if (this.this$0.isVisible()) {
            Dialog P = bh.P(new WeakReference(this.this$0.requireActivity()), Integer.valueOf((int) R.string.gas_price_updated), null, this.this$0.getString(R.string.gas_price_updated_content), Integer.valueOf((int) R.string.action_ok), null, null, null, new AnonymousClass1(this.this$0), null);
            fs1.d(P);
            P.show();
        }
    }
}
