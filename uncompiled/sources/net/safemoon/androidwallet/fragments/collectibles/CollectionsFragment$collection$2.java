package net.safemoon.androidwallet.fragments.collectibles;

import java.io.Serializable;
import java.util.Objects;
import kotlin.jvm.internal.Lambda;
import net.safemoon.androidwallet.model.collectible.RoomCollection;

/* compiled from: CollectionsFragment.kt */
/* loaded from: classes2.dex */
public final class CollectionsFragment$collection$2 extends Lambda implements rc1<RoomCollection> {
    public final /* synthetic */ CollectionsFragment this$0;

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public CollectionsFragment$collection$2(CollectionsFragment collectionsFragment) {
        super(0);
        this.this$0 = collectionsFragment;
    }

    @Override // defpackage.rc1
    public final RoomCollection invoke() {
        Serializable serializable = this.this$0.requireArguments().getSerializable("collection");
        Objects.requireNonNull(serializable, "null cannot be cast to non-null type net.safemoon.androidwallet.model.collectible.RoomCollection");
        return (RoomCollection) serializable;
    }
}
