package net.safemoon.androidwallet.fragments.collectibles;

import androidx.appcompat.widget.AppCompatEditText;
import androidx.fragment.app.FragmentActivity;
import java.util.List;
import kotlin.jvm.internal.Lambda;
import net.safemoon.androidwallet.model.wallets.Wallet;

/* compiled from: SendToNftFragment.kt */
/* loaded from: classes2.dex */
public final class SendToNftFragment$onViewCreated$2$7$1 extends Lambda implements tc1<List<? extends Wallet>, te4> {
    public final /* synthetic */ SendToNftFragment this$0;

    /* compiled from: SendToNftFragment.kt */
    /* loaded from: classes2.dex */
    public static final class a implements dn1 {
        public final /* synthetic */ SendToNftFragment a;

        public a(SendToNftFragment sendToNftFragment) {
            this.a = sendToNftFragment;
        }

        @Override // defpackage.dn1
        public void a(Wallet wallet2) {
            AppCompatEditText appCompatEditText;
            fs1.f(wallet2, "item");
            za1 za1Var = this.a.m0;
            if (za1Var != null && (appCompatEditText = za1Var.f) != null) {
                appCompatEditText.setText(wallet2.getAddress());
            }
            qc qcVar = this.a.k0;
            fs1.d(qcVar);
            qcVar.d();
        }
    }

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public SendToNftFragment$onViewCreated$2$7$1(SendToNftFragment sendToNftFragment) {
        super(1);
        this.this$0 = sendToNftFragment;
    }

    @Override // defpackage.tc1
    public /* bridge */ /* synthetic */ te4 invoke(List<? extends Wallet> list) {
        invoke2((List<Wallet>) list);
        return te4.a;
    }

    /* renamed from: invoke  reason: avoid collision after fix types in other method */
    public final void invoke2(List<Wallet> list) {
        fs1.f(list, "list");
        if (list.size() > 1) {
            this.this$0.k0 = new qc(list);
            qc qcVar = this.this$0.k0;
            if (qcVar == null) {
                return;
            }
            FragmentActivity requireActivity = this.this$0.requireActivity();
            fs1.e(requireActivity, "requireActivity()");
            za1 za1Var = this.this$0.m0;
            AppCompatEditText appCompatEditText = za1Var == null ? null : za1Var.f;
            fs1.d(appCompatEditText);
            fs1.e(appCompatEditText, "binding?.etContactAddress!!");
            za1 za1Var2 = this.this$0.m0;
            qcVar.f(requireActivity, appCompatEditText, za1Var2 != null ? za1Var2.i : null, new a(this.this$0));
        }
    }
}
