package net.safemoon.androidwallet.fragments.collectibles;

import android.content.Context;
import kotlin.jvm.internal.Lambda;
import net.safemoon.androidwallet.model.wallets.Wallet;

/* compiled from: NftDetailFragment.kt */
/* loaded from: classes2.dex */
public final class NftDetailFragment$wallet$2 extends Lambda implements rc1<Wallet> {
    public final /* synthetic */ NftDetailFragment this$0;

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public NftDetailFragment$wallet$2(NftDetailFragment nftDetailFragment) {
        super(0);
        this.this$0 = nftDetailFragment;
    }

    @Override // defpackage.rc1
    public final Wallet invoke() {
        Context requireContext = this.this$0.requireContext();
        fs1.e(requireContext, "requireContext()");
        return e30.c(requireContext);
    }
}
