package net.safemoon.androidwallet.fragments.collectibles;

import com.google.gson.reflect.TypeToken;
import java.util.List;
import kotlin.jvm.internal.Lambda;
import net.safemoon.androidwallet.model.collectible.AssetTrait;
import net.safemoon.androidwallet.model.nft.NFTData;

/* compiled from: NftDetailFragment.kt */
/* loaded from: classes2.dex */
public final class NftDetailFragment$nftData$2 extends Lambda implements rc1<NFTData> {
    public final /* synthetic */ NftDetailFragment this$0;

    /* compiled from: NftDetailFragment.kt */
    /* loaded from: classes2.dex */
    public static final class a extends TypeToken<List<? extends AssetTrait>> {
    }

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public NftDetailFragment$nftData$2(NftDetailFragment nftDetailFragment) {
        super(0);
        this.this$0 = nftDetailFragment;
    }

    /* JADX WARN: Code restructure failed: missing block: B:22:0x005a, code lost:
        if (r0 == false) goto L26;
     */
    /* JADX WARN: Removed duplicated region for block: B:14:0x0041  */
    /* JADX WARN: Removed duplicated region for block: B:24:0x005e  */
    /* JADX WARN: Removed duplicated region for block: B:25:0x0060  */
    /* JADX WARN: Removed duplicated region for block: B:27:0x006e  */
    /* JADX WARN: Removed duplicated region for block: B:28:0x0070  */
    @Override // defpackage.rc1
    /*
        Code decompiled incorrectly, please refer to instructions dump.
        To view partially-correct code enable 'Show inconsistent code' option in preferences
    */
    public final net.safemoon.androidwallet.model.nft.NFTData invoke() {
        /*
            Method dump skipped, instructions count: 502
            To view this dump change 'Code comments level' option to 'DEBUG'
        */
        throw new UnsupportedOperationException("Method not decompiled: net.safemoon.androidwallet.fragments.collectibles.NftDetailFragment$nftData$2.invoke():net.safemoon.androidwallet.model.nft.NFTData");
    }
}
