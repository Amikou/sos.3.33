package net.safemoon.androidwallet.fragments.collectibles;

import android.graphics.Matrix;
import android.graphics.RectF;
import android.graphics.drawable.Drawable;
import android.view.View;
import android.widget.ImageView;
import com.bumptech.glide.load.DataSource;
import com.bumptech.glide.load.engine.GlideException;
import com.github.mikephil.charting.utils.Utils;
import net.safemoon.androidwallet.fragments.collectibles.NftDetailFragment;
import net.safemoon.androidwallet.fragments.collectibles.NftDetailFragment$onViewCreated$1;
import net.safemoon.androidwallet.views.zoomImage.ZoomageView;

/* compiled from: NftDetailFragment.kt */
/* loaded from: classes2.dex */
public final class NftDetailFragment$onViewCreated$1 implements j73<Drawable> {
    public final /* synthetic */ ZoomageView a;
    public final /* synthetic */ NftDetailFragment f0;

    public NftDetailFragment$onViewCreated$1(ZoomageView zoomageView, NftDetailFragment nftDetailFragment) {
        this.a = zoomageView;
        this.f0 = nftDetailFragment;
    }

    public static final void d(NftDetailFragment nftDetailFragment, ZoomageView zoomageView, Drawable drawable) {
        fs1.f(nftDetailFragment, "this$0");
        if (nftDetailFragment.isVisible()) {
            zoomageView.setScaleType(ImageView.ScaleType.MATRIX);
            Matrix matrix = new Matrix();
            matrix.setRectToRect(new RectF(Utils.FLOAT_EPSILON, Utils.FLOAT_EPSILON, drawable == null ? 0.0f : drawable.getIntrinsicWidth(), drawable == null ? 0.0f : drawable.getIntrinsicHeight()), new RectF(Utils.FLOAT_EPSILON, Utils.FLOAT_EPSILON, zoomageView.getWidth(), zoomageView.getHeight()), Matrix.ScaleToFit.CENTER);
            te4 te4Var = te4.a;
            zoomageView.setImageMatrix(matrix);
        }
    }

    public static final boolean e(NftDetailFragment nftDetailFragment, View view) {
        fs1.f(nftDetailFragment, "this$0");
        as.b(sz1.a(nftDetailFragment), null, null, new NftDetailFragment$onViewCreated$1$onResourceReady$2$1(nftDetailFragment, null), 3, null);
        return false;
    }

    @Override // defpackage.j73
    /* renamed from: c */
    public boolean n(final Drawable drawable, Object obj, i34<Drawable> i34Var, DataSource dataSource, boolean z) {
        final ZoomageView zoomageView = this.a;
        final NftDetailFragment nftDetailFragment = this.f0;
        zoomageView.postDelayed(new Runnable() { // from class: bg2
            @Override // java.lang.Runnable
            public final void run() {
                NftDetailFragment$onViewCreated$1.d(NftDetailFragment.this, zoomageView, drawable);
            }
        }, 100L);
        ZoomageView zoomageView2 = this.a;
        final NftDetailFragment nftDetailFragment2 = this.f0;
        zoomageView2.setOnLongClickListener(new View.OnLongClickListener() { // from class: ag2
            @Override // android.view.View.OnLongClickListener
            public final boolean onLongClick(View view) {
                boolean e;
                e = NftDetailFragment$onViewCreated$1.e(NftDetailFragment.this, view);
                return e;
            }
        });
        return false;
    }

    @Override // defpackage.j73
    public boolean i(GlideException glideException, Object obj, i34<Drawable> i34Var, boolean z) {
        return false;
    }
}
