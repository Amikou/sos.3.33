package net.safemoon.androidwallet.fragments.collectibles;

import android.content.DialogInterface;
import java.lang.ref.WeakReference;
import kotlin.jvm.internal.Lambda;
import net.safemoon.androidwallet.R;
import net.safemoon.androidwallet.model.collectible.RoomCollectionAndNft;
import net.safemoon.androidwallet.model.collectible.TYPE_DELETE_NFT;

/* compiled from: CollectibleFragment.kt */
/* loaded from: classes2.dex */
public final class CollectibleFragment$collectionsAdapter$2 extends Lambda implements rc1<r10> {
    public final /* synthetic */ CollectibleFragment this$0;

    /* compiled from: CollectibleFragment.kt */
    /* renamed from: net.safemoon.androidwallet.fragments.collectibles.CollectibleFragment$collectionsAdapter$2$1  reason: invalid class name */
    /* loaded from: classes2.dex */
    public static final class AnonymousClass1 extends Lambda implements tc1<RoomCollectionAndNft, te4> {
        public final /* synthetic */ CollectibleFragment this$0;

        /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
        public AnonymousClass1(CollectibleFragment collectibleFragment) {
            super(1);
            this.this$0 = collectibleFragment;
        }

        @Override // defpackage.tc1
        public /* bridge */ /* synthetic */ te4 invoke(RoomCollectionAndNft roomCollectionAndNft) {
            invoke2(roomCollectionAndNft);
            return te4.a;
        }

        /* renamed from: invoke  reason: avoid collision after fix types in other method */
        public final void invoke2(RoomCollectionAndNft roomCollectionAndNft) {
            fs1.f(roomCollectionAndNft, "it");
            this.this$0.L(roomCollectionAndNft.getCollection());
        }
    }

    /* compiled from: CollectibleFragment.kt */
    /* renamed from: net.safemoon.androidwallet.fragments.collectibles.CollectibleFragment$collectionsAdapter$2$2  reason: invalid class name */
    /* loaded from: classes2.dex */
    public static final class AnonymousClass2 extends Lambda implements hd1<RoomCollectionAndNft, TYPE_DELETE_NFT, te4> {
        public final /* synthetic */ CollectibleFragment this$0;

        /* compiled from: CollectibleFragment.kt */
        /* renamed from: net.safemoon.androidwallet.fragments.collectibles.CollectibleFragment$collectionsAdapter$2$2$1  reason: invalid class name */
        /* loaded from: classes2.dex */
        public static final class AnonymousClass1 extends Lambda implements tc1<DialogInterface, te4> {
            public final /* synthetic */ RoomCollectionAndNft $it;
            public final /* synthetic */ CollectibleFragment this$0;

            /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
            public AnonymousClass1(CollectibleFragment collectibleFragment, RoomCollectionAndNft roomCollectionAndNft) {
                super(1);
                this.this$0 = collectibleFragment;
                this.$it = roomCollectionAndNft;
            }

            @Override // defpackage.tc1
            public /* bridge */ /* synthetic */ te4 invoke(DialogInterface dialogInterface) {
                invoke2(dialogInterface);
                return te4.a;
            }

            /* renamed from: invoke  reason: avoid collision after fix types in other method */
            public final void invoke2(DialogInterface dialogInterface) {
                fs1.f(dialogInterface, "$noName_0");
                this.this$0.I().L(this.$it, true);
            }
        }

        /* compiled from: CollectibleFragment.kt */
        /* renamed from: net.safemoon.androidwallet.fragments.collectibles.CollectibleFragment$collectionsAdapter$2$2$2  reason: invalid class name and collision with other inner class name */
        /* loaded from: classes2.dex */
        public static final class C02132 extends Lambda implements tc1<DialogInterface, te4> {
            public static final C02132 INSTANCE = new C02132();

            public C02132() {
                super(1);
            }

            @Override // defpackage.tc1
            public /* bridge */ /* synthetic */ te4 invoke(DialogInterface dialogInterface) {
                invoke2(dialogInterface);
                return te4.a;
            }

            /* renamed from: invoke  reason: avoid collision after fix types in other method */
            public final void invoke2(DialogInterface dialogInterface) {
                fs1.f(dialogInterface, "it");
            }
        }

        /* compiled from: CollectibleFragment.kt */
        /* renamed from: net.safemoon.androidwallet.fragments.collectibles.CollectibleFragment$collectionsAdapter$2$2$3  reason: invalid class name */
        /* loaded from: classes2.dex */
        public static final class AnonymousClass3 extends Lambda implements tc1<DialogInterface, te4> {
            public final /* synthetic */ RoomCollectionAndNft $it;
            public final /* synthetic */ CollectibleFragment this$0;

            /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
            public AnonymousClass3(CollectibleFragment collectibleFragment, RoomCollectionAndNft roomCollectionAndNft) {
                super(1);
                this.this$0 = collectibleFragment;
                this.$it = roomCollectionAndNft;
            }

            @Override // defpackage.tc1
            public /* bridge */ /* synthetic */ te4 invoke(DialogInterface dialogInterface) {
                invoke2(dialogInterface);
                return te4.a;
            }

            /* renamed from: invoke  reason: avoid collision after fix types in other method */
            public final void invoke2(DialogInterface dialogInterface) {
                fs1.f(dialogInterface, "$noName_0");
                this.this$0.I().L(this.$it, false);
            }
        }

        /* compiled from: CollectibleFragment.kt */
        /* renamed from: net.safemoon.androidwallet.fragments.collectibles.CollectibleFragment$collectionsAdapter$2$2$4  reason: invalid class name */
        /* loaded from: classes2.dex */
        public static final class AnonymousClass4 extends Lambda implements tc1<DialogInterface, te4> {
            public static final AnonymousClass4 INSTANCE = new AnonymousClass4();

            public AnonymousClass4() {
                super(1);
            }

            @Override // defpackage.tc1
            public /* bridge */ /* synthetic */ te4 invoke(DialogInterface dialogInterface) {
                invoke2(dialogInterface);
                return te4.a;
            }

            /* renamed from: invoke  reason: avoid collision after fix types in other method */
            public final void invoke2(DialogInterface dialogInterface) {
                fs1.f(dialogInterface, "it");
            }
        }

        /* compiled from: CollectibleFragment.kt */
        /* renamed from: net.safemoon.androidwallet.fragments.collectibles.CollectibleFragment$collectionsAdapter$2$2$5  reason: invalid class name */
        /* loaded from: classes2.dex */
        public static final class AnonymousClass5 extends Lambda implements tc1<DialogInterface, te4> {
            public final /* synthetic */ RoomCollectionAndNft $it;
            public final /* synthetic */ CollectibleFragment this$0;

            /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
            public AnonymousClass5(CollectibleFragment collectibleFragment, RoomCollectionAndNft roomCollectionAndNft) {
                super(1);
                this.this$0 = collectibleFragment;
                this.$it = roomCollectionAndNft;
            }

            @Override // defpackage.tc1
            public /* bridge */ /* synthetic */ te4 invoke(DialogInterface dialogInterface) {
                invoke2(dialogInterface);
                return te4.a;
            }

            /* renamed from: invoke  reason: avoid collision after fix types in other method */
            public final void invoke2(DialogInterface dialogInterface) {
                fs1.f(dialogInterface, "$noName_0");
                this.this$0.I().M(this.$it);
            }
        }

        /* compiled from: CollectibleFragment.kt */
        /* renamed from: net.safemoon.androidwallet.fragments.collectibles.CollectibleFragment$collectionsAdapter$2$2$6  reason: invalid class name */
        /* loaded from: classes2.dex */
        public static final class AnonymousClass6 extends Lambda implements tc1<DialogInterface, te4> {
            public static final AnonymousClass6 INSTANCE = new AnonymousClass6();

            public AnonymousClass6() {
                super(1);
            }

            @Override // defpackage.tc1
            public /* bridge */ /* synthetic */ te4 invoke(DialogInterface dialogInterface) {
                invoke2(dialogInterface);
                return te4.a;
            }

            /* renamed from: invoke  reason: avoid collision after fix types in other method */
            public final void invoke2(DialogInterface dialogInterface) {
                fs1.f(dialogInterface, "it");
            }
        }

        /* compiled from: CollectibleFragment.kt */
        /* renamed from: net.safemoon.androidwallet.fragments.collectibles.CollectibleFragment$collectionsAdapter$2$2$a */
        /* loaded from: classes2.dex */
        public /* synthetic */ class a {
            public static final /* synthetic */ int[] a;

            static {
                int[] iArr = new int[TYPE_DELETE_NFT.values().length];
                iArr[TYPE_DELETE_NFT.HIDE.ordinal()] = 1;
                iArr[TYPE_DELETE_NFT.VISIBLE.ordinal()] = 2;
                iArr[TYPE_DELETE_NFT.DELETE_FOREVER.ordinal()] = 3;
                a = iArr;
            }
        }

        /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
        public AnonymousClass2(CollectibleFragment collectibleFragment) {
            super(2);
            this.this$0 = collectibleFragment;
        }

        @Override // defpackage.hd1
        public /* bridge */ /* synthetic */ te4 invoke(RoomCollectionAndNft roomCollectionAndNft, TYPE_DELETE_NFT type_delete_nft) {
            invoke2(roomCollectionAndNft, type_delete_nft);
            return te4.a;
        }

        /* renamed from: invoke  reason: avoid collision after fix types in other method */
        public final void invoke2(RoomCollectionAndNft roomCollectionAndNft, TYPE_DELETE_NFT type_delete_nft) {
            fs1.f(roomCollectionAndNft, "it");
            fs1.f(type_delete_nft, "type");
            int i = a.a[type_delete_nft.ordinal()];
            if (i == 1) {
                bh.P(new WeakReference(this.this$0.requireActivity()), (r23 & 2) != 0 ? null : null, (r23 & 4) != 0 ? null : Integer.valueOf((int) R.string.alert_remove_this_collection), (r23 & 8) != 0 ? null : null, (r23 & 16) != 0 ? Integer.valueOf((int) R.string.action_ok) : Integer.valueOf((int) R.string.action_yes), (r23 & 32) != 0 ? Integer.valueOf((int) R.string.cancel) : Integer.valueOf((int) R.string.action_no), (r23 & 64) != 0 ? null : null, (r23 & 128) != 0 ? null : null, new AnonymousClass1(this.this$0, roomCollectionAndNft), C02132.INSTANCE);
            } else if (i == 2) {
                bh.P(new WeakReference(this.this$0.requireActivity()), (r23 & 2) != 0 ? null : null, (r23 & 4) != 0 ? null : Integer.valueOf((int) R.string.alert_back_this_collection), (r23 & 8) != 0 ? null : null, (r23 & 16) != 0 ? Integer.valueOf((int) R.string.action_ok) : Integer.valueOf((int) R.string.action_yes), (r23 & 32) != 0 ? Integer.valueOf((int) R.string.cancel) : Integer.valueOf((int) R.string.action_no), (r23 & 64) != 0 ? null : null, (r23 & 128) != 0 ? null : null, new AnonymousClass3(this.this$0, roomCollectionAndNft), AnonymousClass4.INSTANCE);
            } else if (i != 3) {
            } else {
                bh.P(new WeakReference(this.this$0.requireActivity()), (r23 & 2) != 0 ? null : null, (r23 & 4) != 0 ? null : Integer.valueOf((int) R.string.alert_delete_this_collection), (r23 & 8) != 0 ? null : null, (r23 & 16) != 0 ? Integer.valueOf((int) R.string.action_ok) : Integer.valueOf((int) R.string.action_yes), (r23 & 32) != 0 ? Integer.valueOf((int) R.string.cancel) : Integer.valueOf((int) R.string.action_no), (r23 & 64) != 0 ? null : null, (r23 & 128) != 0 ? null : null, new AnonymousClass5(this.this$0, roomCollectionAndNft), AnonymousClass6.INSTANCE);
            }
        }
    }

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public CollectibleFragment$collectionsAdapter$2(CollectibleFragment collectibleFragment) {
        super(0);
        this.this$0 = collectibleFragment;
    }

    @Override // defpackage.rc1
    public final r10 invoke() {
        return new r10(new AnonymousClass1(this.this$0), new AnonymousClass2(this.this$0));
    }
}
