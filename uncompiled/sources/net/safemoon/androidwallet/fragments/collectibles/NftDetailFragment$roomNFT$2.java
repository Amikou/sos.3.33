package net.safemoon.androidwallet.fragments.collectibles;

import java.io.Serializable;
import java.util.Objects;
import kotlin.jvm.internal.Lambda;
import net.safemoon.androidwallet.model.collectible.RoomNFT;

/* compiled from: NftDetailFragment.kt */
/* loaded from: classes2.dex */
public final class NftDetailFragment$roomNFT$2 extends Lambda implements rc1<RoomNFT> {
    public final /* synthetic */ NftDetailFragment this$0;

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public NftDetailFragment$roomNFT$2(NftDetailFragment nftDetailFragment) {
        super(0);
        this.this$0 = nftDetailFragment;
    }

    @Override // defpackage.rc1
    public final RoomNFT invoke() {
        Serializable serializable = this.this$0.requireArguments().getSerializable("nftData");
        Objects.requireNonNull(serializable, "null cannot be cast to non-null type net.safemoon.androidwallet.model.collectible.RoomNFT");
        return (RoomNFT) serializable;
    }
}
