package net.safemoon.androidwallet.fragments.collectibles;

import android.webkit.MimeTypeMap;
import androidx.fragment.app.FragmentActivity;
import com.bumptech.glide.e;
import java.io.File;
import java.net.URLConnection;
import kotlin.coroutines.jvm.internal.SuspendLambda;
import kotlin.coroutines.jvm.internal.a;
import kotlinx.coroutines.CoroutineDispatcher;
import net.safemoon.androidwallet.model.nft.NFTData;

/* compiled from: NftDetailFragment.kt */
@a(c = "net.safemoon.androidwallet.fragments.collectibles.NftDetailFragment$onViewCreated$1$onResourceReady$2$1", f = "NftDetailFragment.kt", l = {239}, m = "invokeSuspend")
/* loaded from: classes2.dex */
public final class NftDetailFragment$onViewCreated$1$onResourceReady$2$1 extends SuspendLambda implements hd1<c90, q70<? super te4>, Object> {
    public int label;
    public final /* synthetic */ NftDetailFragment this$0;

    /* compiled from: NftDetailFragment.kt */
    @a(c = "net.safemoon.androidwallet.fragments.collectibles.NftDetailFragment$onViewCreated$1$onResourceReady$2$1$1", f = "NftDetailFragment.kt", l = {241, 250}, m = "invokeSuspend")
    /* renamed from: net.safemoon.androidwallet.fragments.collectibles.NftDetailFragment$onViewCreated$1$onResourceReady$2$1$1  reason: invalid class name */
    /* loaded from: classes2.dex */
    public static final class AnonymousClass1 extends SuspendLambda implements hd1<c90, q70<? super te4>, Object> {
        public Object L$0;
        public Object L$1;
        public int label;
        public final /* synthetic */ NftDetailFragment this$0;

        /* compiled from: NftDetailFragment.kt */
        @a(c = "net.safemoon.androidwallet.fragments.collectibles.NftDetailFragment$onViewCreated$1$onResourceReady$2$1$1$1", f = "NftDetailFragment.kt", l = {}, m = "invokeSuspend")
        /* renamed from: net.safemoon.androidwallet.fragments.collectibles.NftDetailFragment$onViewCreated$1$onResourceReady$2$1$1$1  reason: invalid class name and collision with other inner class name */
        /* loaded from: classes2.dex */
        public static final class C02141 extends SuspendLambda implements hd1<c90, q70<? super te4>, Object> {
            public final /* synthetic */ String $mimeType;
            public final /* synthetic */ File $originalFile;
            public final /* synthetic */ File $sharingFile;
            public int label;
            public final /* synthetic */ NftDetailFragment this$0;

            /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
            public C02141(NftDetailFragment nftDetailFragment, File file, File file2, String str, q70<? super C02141> q70Var) {
                super(2, q70Var);
                this.this$0 = nftDetailFragment;
                this.$sharingFile = file;
                this.$originalFile = file2;
                this.$mimeType = str;
            }

            @Override // kotlin.coroutines.jvm.internal.BaseContinuationImpl
            public final q70<te4> create(Object obj, q70<?> q70Var) {
                return new C02141(this.this$0, this.$sharingFile, this.$originalFile, this.$mimeType, q70Var);
            }

            @Override // defpackage.hd1
            public final Object invoke(c90 c90Var, q70<? super te4> q70Var) {
                return ((C02141) create(c90Var, q70Var)).invokeSuspend(te4.a);
            }

            @Override // kotlin.coroutines.jvm.internal.BaseContinuationImpl
            public final Object invokeSuspend(Object obj) {
                NFTData G;
                gs1.d();
                if (this.label == 0) {
                    o83.b(obj);
                    if (this.this$0.isVisible()) {
                        FragmentActivity requireActivity = this.this$0.requireActivity();
                        fs1.e(requireActivity, "requireActivity()");
                        File file = this.$sharingFile;
                        if (file == null) {
                            file = this.$originalFile;
                        }
                        G = this.this$0.G();
                        j7.e(requireActivity, file, G.getName(), this.$mimeType);
                    }
                    return te4.a;
                }
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
        }

        /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
        public AnonymousClass1(NftDetailFragment nftDetailFragment, q70<? super AnonymousClass1> q70Var) {
            super(2, q70Var);
            this.this$0 = nftDetailFragment;
        }

        @Override // kotlin.coroutines.jvm.internal.BaseContinuationImpl
        public final q70<te4> create(Object obj, q70<?> q70Var) {
            return new AnonymousClass1(this.this$0, q70Var);
        }

        @Override // defpackage.hd1
        public final Object invoke(c90 c90Var, q70<? super te4> q70Var) {
            return ((AnonymousClass1) create(c90Var, q70Var)).invokeSuspend(te4.a);
        }

        @Override // kotlin.coroutines.jvm.internal.BaseContinuationImpl
        public final Object invokeSuspend(Object obj) {
            NFTData G;
            String headerField;
            NFTData G2;
            NFTData G3;
            File file;
            Object d = gs1.d();
            int i = this.label;
            if (i == 0) {
                o83.b(obj);
                FragmentActivity requireActivity = this.this$0.requireActivity();
                fs1.e(requireActivity, "requireActivity()");
                G = this.this$0.G();
                String imageUrl = G.getImageUrl();
                this.label = 1;
                obj = j7.a(requireActivity, imageUrl, this);
                if (obj == d) {
                    return d;
                }
            } else if (i != 1) {
                if (i == 2) {
                    headerField = (String) this.L$0;
                    o83.b(obj);
                    file = (File) this.L$1;
                    as.b(sz1.a(this.this$0), null, null, new C02141(this.this$0, (File) obj, file, headerField, null), 3, null);
                    return te4.a;
                }
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            } else {
                o83.b(obj);
            }
            URLConnection uRLConnection = (URLConnection) obj;
            headerField = uRLConnection == null ? null : uRLConnection.getHeaderField("Content-Type");
            String extensionFromMimeType = MimeTypeMap.getSingleton().getExtensionFromMimeType(headerField);
            if (extensionFromMimeType == null) {
                extensionFromMimeType = "";
            }
            e<File> p = com.bumptech.glide.a.t(this.this$0.requireContext()).p();
            G2 = this.this$0.G();
            File file2 = p.R0(G2.getImageUrl()).V0().get();
            FragmentActivity requireActivity2 = this.this$0.requireActivity();
            fs1.e(requireActivity2, "requireActivity()");
            fs1.e(file2, "originalFile");
            StringBuilder sb = new StringBuilder();
            G3 = this.this$0.G();
            String imageUrl2 = G3.getImageUrl();
            sb.append(imageUrl2 != null ? imageUrl2.hashCode() : 0);
            sb.append('.');
            sb.append(extensionFromMimeType);
            String sb2 = sb.toString();
            this.L$0 = headerField;
            this.L$1 = file2;
            this.label = 2;
            obj = j7.d(requireActivity2, file2, sb2, this);
            if (obj == d) {
                return d;
            }
            file = file2;
            as.b(sz1.a(this.this$0), null, null, new C02141(this.this$0, (File) obj, file, headerField, null), 3, null);
            return te4.a;
        }
    }

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public NftDetailFragment$onViewCreated$1$onResourceReady$2$1(NftDetailFragment nftDetailFragment, q70<? super NftDetailFragment$onViewCreated$1$onResourceReady$2$1> q70Var) {
        super(2, q70Var);
        this.this$0 = nftDetailFragment;
    }

    @Override // kotlin.coroutines.jvm.internal.BaseContinuationImpl
    public final q70<te4> create(Object obj, q70<?> q70Var) {
        return new NftDetailFragment$onViewCreated$1$onResourceReady$2$1(this.this$0, q70Var);
    }

    @Override // defpackage.hd1
    public final Object invoke(c90 c90Var, q70<? super te4> q70Var) {
        return ((NftDetailFragment$onViewCreated$1$onResourceReady$2$1) create(c90Var, q70Var)).invokeSuspend(te4.a);
    }

    @Override // kotlin.coroutines.jvm.internal.BaseContinuationImpl
    public final Object invokeSuspend(Object obj) {
        Object d = gs1.d();
        int i = this.label;
        if (i == 0) {
            o83.b(obj);
            CoroutineDispatcher b = tp0.b();
            AnonymousClass1 anonymousClass1 = new AnonymousClass1(this.this$0, null);
            this.label = 1;
            if (kotlinx.coroutines.a.e(b, anonymousClass1, this) == d) {
                return d;
            }
        } else if (i != 1) {
            throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        } else {
            o83.b(obj);
        }
        return te4.a;
    }
}
