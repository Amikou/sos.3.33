package net.safemoon.androidwallet.fragments.collectibles;

import defpackage.pk3;
import kotlin.jvm.internal.Lambda;
import net.safemoon.androidwallet.model.contact.abstraction.IContact;
import net.safemoon.androidwallet.model.nft.NFTData;
import net.safemoon.androidwallet.viewmodels.ContactViewModel;

/* compiled from: SendToNftFragment.kt */
/* loaded from: classes2.dex */
public final class SendToNftFragment$transferNft$1 extends Lambda implements rc1<te4> {
    public final /* synthetic */ String $address;
    public final /* synthetic */ SendToNftFragment this$0;

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public SendToNftFragment$transferNft$1(SendToNftFragment sendToNftFragment, String str) {
        super(0);
        this.this$0 = sendToNftFragment;
        this.$address = str;
    }

    @Override // defpackage.rc1
    public /* bridge */ /* synthetic */ te4 invoke() {
        invoke2();
        return te4.a;
    }

    @Override // defpackage.rc1
    /* renamed from: invoke  reason: avoid collision after fix types in other method */
    public final void invoke2() {
        gb2<IContact> n;
        SendToNftFragment sendToNftFragment = this.this$0;
        String str = this.$address;
        NFTData Q = sendToNftFragment.Q();
        ContactViewModel contactViewModel = this.this$0.i0;
        IContact iContact = null;
        if (contactViewModel != null && (n = contactViewModel.n()) != null) {
            iContact = n.getValue();
        }
        pk3.b a = pk3.a(str, Q, iContact);
        fs1.e(a, "actionNftTransfer(addres…?.selectedContact?.value)");
        sendToNftFragment.g(a);
    }
}
