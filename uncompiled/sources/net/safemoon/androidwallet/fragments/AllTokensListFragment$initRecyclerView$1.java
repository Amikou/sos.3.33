package net.safemoon.androidwallet.fragments;

import androidx.fragment.app.FragmentManager;
import kotlin.jvm.internal.Lambda;
import net.safemoon.androidwallet.R;
import net.safemoon.androidwallet.common.ActiveTokenListMode;
import net.safemoon.androidwallet.model.Coin;
import net.safemoon.androidwallet.utils.ChartParameter;

/* compiled from: AllTokensListFragment.kt */
/* loaded from: classes2.dex */
public final class AllTokensListFragment$initRecyclerView$1 extends Lambda implements tc1<Coin, te4> {
    public final /* synthetic */ AllTokensListFragment this$0;

    /* compiled from: AllTokensListFragment.kt */
    /* renamed from: net.safemoon.androidwallet.fragments.AllTokensListFragment$initRecyclerView$1$1  reason: invalid class name */
    /* loaded from: classes2.dex */
    public static final class AnonymousClass1 extends Lambda implements rc1<te4> {
        public static final AnonymousClass1 INSTANCE = new AnonymousClass1();

        public AnonymousClass1() {
            super(0);
        }

        @Override // defpackage.rc1
        public /* bridge */ /* synthetic */ te4 invoke() {
            invoke2();
            return te4.a;
        }

        @Override // defpackage.rc1
        /* renamed from: invoke  reason: avoid collision after fix types in other method */
        public final void invoke2() {
        }
    }

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public AllTokensListFragment$initRecyclerView$1(AllTokensListFragment allTokensListFragment) {
        super(1);
        this.this$0 = allTokensListFragment;
    }

    @Override // defpackage.tc1
    public /* bridge */ /* synthetic */ te4 invoke(Coin coin) {
        invoke2(coin);
        return te4.a;
    }

    /* renamed from: invoke  reason: avoid collision after fix types in other method */
    public final void invoke2(Coin coin) {
        ActiveTokenListMode activeTokenListMode;
        ChartParameter chartParameter;
        String str;
        String str2;
        String str3;
        int i;
        ActiveTokenListMode activeTokenListMode2;
        fs1.f(coin, "it");
        activeTokenListMode = this.this$0.z0;
        if (activeTokenListMode != ActiveTokenListMode.GAINER) {
            activeTokenListMode2 = this.this$0.z0;
            if (activeTokenListMode2 != ActiveTokenListMode.LOSER) {
                chartParameter = null;
                FragmentManager childFragmentManager = this.this$0.getChildFragmentManager();
                fs1.e(childFragmentManager, "childFragmentManager");
                new dy(coin, childFragmentManager).g(chartParameter, AnonymousClass1.INSTANCE);
            }
        }
        chartParameter = new ChartParameter();
        AllTokensListFragment allTokensListFragment = this.this$0;
        String C = allTokensListFragment.C();
        str = allTokensListFragment.i0;
        if (fs1.b(C, str)) {
            i = R.id.btn12Hours;
        } else {
            str2 = allTokensListFragment.j0;
            if (fs1.b(C, str2)) {
                i = R.id.btn1Day;
            } else {
                str3 = allTokensListFragment.k0;
                i = fs1.b(C, str3) ? R.id.btn1Week : 0;
            }
        }
        chartParameter.setDefaultInterval(i);
        FragmentManager childFragmentManager2 = this.this$0.getChildFragmentManager();
        fs1.e(childFragmentManager2, "childFragmentManager");
        new dy(coin, childFragmentManager2).g(chartParameter, AnonymousClass1.INSTANCE);
    }
}
