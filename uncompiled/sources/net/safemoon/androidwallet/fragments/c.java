package net.safemoon.androidwallet.fragments;

import android.os.Bundle;
import java.util.HashMap;
import net.safemoon.androidwallet.R;

/* compiled from: SettingsFragmentDirections.java */
/* loaded from: classes2.dex */
public class c {

    /* compiled from: SettingsFragmentDirections.java */
    /* loaded from: classes2.dex */
    public static class b implements ce2 {
        public final HashMap a;

        @Override // defpackage.ce2
        public Bundle a() {
            Bundle bundle = new Bundle();
            if (this.a.containsKey("url")) {
                bundle.putString("url", (String) this.a.get("url"));
            }
            if (this.a.containsKey("withBottomPadding")) {
                bundle.putBoolean("withBottomPadding", ((Boolean) this.a.get("withBottomPadding")).booleanValue());
            } else {
                bundle.putBoolean("withBottomPadding", true);
            }
            if (this.a.containsKey("titleResId")) {
                bundle.putInt("titleResId", ((Integer) this.a.get("titleResId")).intValue());
            }
            return bundle;
        }

        @Override // defpackage.ce2
        public int b() {
            return R.id.action_navigation_settings_to_termScreenWebViewFragment;
        }

        public int c() {
            return ((Integer) this.a.get("titleResId")).intValue();
        }

        public String d() {
            return (String) this.a.get("url");
        }

        public boolean e() {
            return ((Boolean) this.a.get("withBottomPadding")).booleanValue();
        }

        public boolean equals(Object obj) {
            if (this == obj) {
                return true;
            }
            if (obj == null || b.class != obj.getClass()) {
                return false;
            }
            b bVar = (b) obj;
            if (this.a.containsKey("url") != bVar.a.containsKey("url")) {
                return false;
            }
            if (d() == null ? bVar.d() == null : d().equals(bVar.d())) {
                return this.a.containsKey("withBottomPadding") == bVar.a.containsKey("withBottomPadding") && e() == bVar.e() && this.a.containsKey("titleResId") == bVar.a.containsKey("titleResId") && c() == bVar.c() && b() == bVar.b();
            }
            return false;
        }

        public int hashCode() {
            return (((((((d() != null ? d().hashCode() : 0) + 31) * 31) + (e() ? 1 : 0)) * 31) + c()) * 31) + b();
        }

        public String toString() {
            return "ActionNavigationSettingsToTermScreenWebViewFragment(actionId=" + b() + "){url=" + d() + ", withBottomPadding=" + e() + ", titleResId=" + c() + "}";
        }

        public b(String str, int i) {
            HashMap hashMap = new HashMap();
            this.a = hashMap;
            if (str != null) {
                hashMap.put("url", str);
                hashMap.put("titleResId", Integer.valueOf(i));
                return;
            }
            throw new IllegalArgumentException("Argument \"url\" is marked as non-null but was passed a null value.");
        }
    }

    public static ce2 a() {
        return new l6(R.id.action_navigation_settings_to_dateFormatFragment);
    }

    public static ce2 b() {
        return new l6(R.id.action_navigation_settings_to_defaultLanguageFragment);
    }

    public static ce2 c() {
        return new l6(R.id.action_navigation_settings_to_defaultScreenFragment);
    }

    public static ce2 d() {
        return new l6(R.id.action_navigation_settings_to_fiatListFragment);
    }

    public static ce2 e() {
        return new l6(R.id.action_navigation_settings_to_joinCommunityFragment);
    }

    public static ce2 f() {
        return new l6(R.id.action_navigation_settings_to_manageContactsFragment);
    }

    public static ce2 g() {
        return new l6(R.id.action_navigation_settings_to_notificationFragment);
    }

    public static ce2 h() {
        return new l6(R.id.action_navigation_settings_to_securityFragment);
    }

    public static ce2 i() {
        return new l6(R.id.action_navigation_settings_to_switchWalletFragment);
    }

    public static b j(String str, int i) {
        return new b(str, i);
    }

    public static ce2 k() {
        return new l6(R.id.action_navigation_settings_to_walletConnectFragment);
    }
}
