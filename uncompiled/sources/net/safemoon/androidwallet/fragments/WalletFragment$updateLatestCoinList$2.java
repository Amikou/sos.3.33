package net.safemoon.androidwallet.fragments;

import kotlin.coroutines.jvm.internal.SuspendLambda;
import net.safemoon.androidwallet.model.Coin;

/* compiled from: WalletFragment.kt */
@kotlin.coroutines.jvm.internal.a(c = "net.safemoon.androidwallet.fragments.WalletFragment$updateLatestCoinList$2", f = "WalletFragment.kt", l = {253}, m = "invokeSuspend")
/* loaded from: classes2.dex */
public final class WalletFragment$updateLatestCoinList$2 extends SuspendLambda implements hd1<fp2<Coin>, q70<? super te4>, Object> {
    public /* synthetic */ Object L$0;
    public int label;
    public final /* synthetic */ WalletFragment this$0;

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public WalletFragment$updateLatestCoinList$2(WalletFragment walletFragment, q70<? super WalletFragment$updateLatestCoinList$2> q70Var) {
        super(2, q70Var);
        this.this$0 = walletFragment;
    }

    @Override // kotlin.coroutines.jvm.internal.BaseContinuationImpl
    public final q70<te4> create(Object obj, q70<?> q70Var) {
        WalletFragment$updateLatestCoinList$2 walletFragment$updateLatestCoinList$2 = new WalletFragment$updateLatestCoinList$2(this.this$0, q70Var);
        walletFragment$updateLatestCoinList$2.L$0 = obj;
        return walletFragment$updateLatestCoinList$2;
    }

    @Override // defpackage.hd1
    public final Object invoke(fp2<Coin> fp2Var, q70<? super te4> q70Var) {
        return ((WalletFragment$updateLatestCoinList$2) create(fp2Var, q70Var)).invokeSuspend(te4.a);
    }

    @Override // kotlin.coroutines.jvm.internal.BaseContinuationImpl
    public final Object invokeSuspend(Object obj) {
        k00 k00Var;
        Object d = gs1.d();
        int i = this.label;
        if (i == 0) {
            o83.b(obj);
            fp2 fp2Var = (fp2) this.L$0;
            k00Var = this.this$0.o0;
            if (k00Var != null) {
                this.label = 1;
                if (k00Var.e(fp2Var, this) == d) {
                    return d;
                }
            }
        } else if (i != 1) {
            throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        } else {
            o83.b(obj);
        }
        return te4.a;
    }
}
