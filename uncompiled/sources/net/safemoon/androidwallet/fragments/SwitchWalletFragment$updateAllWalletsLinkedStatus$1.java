package net.safemoon.androidwallet.fragments;

import java.util.List;
import kotlin.jvm.internal.Lambda;
import net.safemoon.androidwallet.model.wallets.Wallet;
import net.safemoon.androidwallet.viewmodels.MultiWalletViewModel;

/* compiled from: SwitchWalletFragment.kt */
/* loaded from: classes2.dex */
public final class SwitchWalletFragment$updateAllWalletsLinkedStatus$1 extends Lambda implements tc1<List<? extends Wallet>, te4> {
    public final /* synthetic */ int $linkedState;
    public final /* synthetic */ SwitchWalletFragment this$0;

    /* compiled from: SwitchWalletFragment.kt */
    /* renamed from: net.safemoon.androidwallet.fragments.SwitchWalletFragment$updateAllWalletsLinkedStatus$1$1  reason: invalid class name */
    /* loaded from: classes2.dex */
    public static final class AnonymousClass1 extends Lambda implements rc1<te4> {
        public static final AnonymousClass1 INSTANCE = new AnonymousClass1();

        public AnonymousClass1() {
            super(0);
        }

        @Override // defpackage.rc1
        public /* bridge */ /* synthetic */ te4 invoke() {
            invoke2();
            return te4.a;
        }

        @Override // defpackage.rc1
        /* renamed from: invoke  reason: avoid collision after fix types in other method */
        public final void invoke2() {
        }
    }

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public SwitchWalletFragment$updateAllWalletsLinkedStatus$1(SwitchWalletFragment switchWalletFragment, int i) {
        super(1);
        this.this$0 = switchWalletFragment;
        this.$linkedState = i;
    }

    @Override // defpackage.tc1
    public /* bridge */ /* synthetic */ te4 invoke(List<? extends Wallet> list) {
        invoke2((List<Wallet>) list);
        return te4.a;
    }

    /* renamed from: invoke  reason: avoid collision after fix types in other method */
    public final void invoke2(List<Wallet> list) {
        MultiWalletViewModel e0;
        fs1.f(list, "list");
        for (Wallet wallet2 : list) {
            e0 = this.this$0.e0();
            e0.C(wallet2, this.$linkedState, AnonymousClass1.INSTANCE);
        }
    }
}
