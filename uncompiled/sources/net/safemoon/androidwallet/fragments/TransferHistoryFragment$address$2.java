package net.safemoon.androidwallet.fragments;

import kotlin.jvm.internal.Lambda;

/* compiled from: TransferHistoryFragment.kt */
/* loaded from: classes2.dex */
public final class TransferHistoryFragment$address$2 extends Lambda implements rc1<String> {
    public final /* synthetic */ TransferHistoryFragment this$0;

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public TransferHistoryFragment$address$2(TransferHistoryFragment transferHistoryFragment) {
        super(0);
        this.this$0 = transferHistoryFragment;
    }

    @Override // defpackage.rc1
    public final String invoke() {
        return bo3.i(this.this$0.requireActivity(), "SAFEMOON_ADDRESS");
    }
}
