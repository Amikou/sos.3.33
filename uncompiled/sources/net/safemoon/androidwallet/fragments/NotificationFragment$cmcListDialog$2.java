package net.safemoon.androidwallet.fragments;

import androidx.lifecycle.LiveData;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Iterator;
import java.util.List;
import kotlin.jvm.internal.Lambda;
import net.safemoon.androidwallet.R;
import net.safemoon.androidwallet.dialogs.CMCListCheckable;
import net.safemoon.androidwallet.fragments.NotificationFragment$cmcListDialog$2;
import net.safemoon.androidwallet.model.Coin;
import net.safemoon.androidwallet.model.RoomCoinPriceAlert;
import net.safemoon.androidwallet.model.priceAlert.PAToken;
import net.safemoon.androidwallet.viewmodels.SettingNotificationViewModel;

/* compiled from: NotificationFragment.kt */
/* loaded from: classes2.dex */
public final class NotificationFragment$cmcListDialog$2 extends Lambda implements rc1<CMCListCheckable> {
    public final /* synthetic */ NotificationFragment this$0;

    /* compiled from: NotificationFragment.kt */
    /* loaded from: classes2.dex */
    public static final class a implements rl1 {
        public final /* synthetic */ NotificationFragment a;
        public final /* synthetic */ CMCListCheckable b;

        public a(NotificationFragment notificationFragment, CMCListCheckable cMCListCheckable) {
            this.a = notificationFragment;
            this.b = cMCListCheckable;
        }

        public static final void e(CMCListCheckable cMCListCheckable, List list) {
            fs1.f(cMCListCheckable, "$this_apply");
            if (list == null) {
                return;
            }
            ArrayList arrayList = new ArrayList(c20.q(list, 10));
            Iterator it = list.iterator();
            while (it.hasNext()) {
                arrayList.add(((RoomCoinPriceAlert) it.next()).getCoinData());
            }
            ArrayList<Coin> arrayList2 = new ArrayList();
            Iterator it2 = arrayList.iterator();
            while (true) {
                if (!it2.hasNext()) {
                    break;
                }
                Object next = it2.next();
                Coin coin = (Coin) next;
                if ((coin != null ? coin.getId() : null) != null) {
                    arrayList2.add(next);
                }
            }
            ArrayList arrayList3 = new ArrayList(c20.q(arrayList2, 10));
            for (Coin coin2 : arrayList2) {
                Integer id = coin2 == null ? null : coin2.getId();
                fs1.d(id);
                arrayList3.add(Integer.valueOf(id.intValue()));
            }
            int[] j0 = j20.j0(arrayList3);
            cMCListCheckable.O(Arrays.copyOf(j0, j0.length));
        }

        @Override // defpackage.rl1
        public void a(Coin coin) {
            ho2 J;
            Object obj;
            SettingNotificationViewModel H;
            fs1.f(coin, "coin");
            J = this.a.J();
            List<PAToken> currentList = J.getCurrentList();
            fs1.e(currentList, "paTokenAdapter.currentList");
            Iterator<T> it = currentList.iterator();
            while (true) {
                if (!it.hasNext()) {
                    obj = null;
                    break;
                }
                obj = it.next();
                if (fs1.b(((PAToken) obj).getCmcId(), coin.getId())) {
                    break;
                }
            }
            PAToken pAToken = (PAToken) obj;
            if (pAToken == null) {
                return;
            }
            H = this.a.H();
            H.o(pAToken);
        }

        @Override // defpackage.rl1
        public void b(Coin coin) {
            SettingNotificationViewModel H;
            fs1.f(coin, "coin");
            H = this.a.H();
            H.n(coin);
        }

        @Override // defpackage.rl1
        public void c() {
            SettingNotificationViewModel H;
            H = this.a.H();
            LiveData<List<RoomCoinPriceAlert>> r = H.r();
            rz1 viewLifecycleOwner = this.a.getViewLifecycleOwner();
            fs1.e(viewLifecycleOwner, "this@NotificationFragment.viewLifecycleOwner");
            final CMCListCheckable cMCListCheckable = this.b;
            q02.a(r, viewLifecycleOwner, new tl2() { // from class: sh2
                @Override // defpackage.tl2
                public final void onChanged(Object obj) {
                    NotificationFragment$cmcListDialog$2.a.e(CMCListCheckable.this, (List) obj);
                }
            });
        }
    }

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public NotificationFragment$cmcListDialog$2(NotificationFragment notificationFragment) {
        super(0);
        this.this$0 = notificationFragment;
    }

    @Override // defpackage.rc1
    public final CMCListCheckable invoke() {
        CMCListCheckable.a aVar = CMCListCheckable.C0;
        String string = this.this$0.getString(R.string.notifications);
        fs1.e(string, "getString(R.string.notifications)");
        CMCListCheckable a2 = aVar.a(string, true);
        a2.N(new a(this.this$0, a2));
        return a2;
    }
}
