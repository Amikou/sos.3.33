package net.safemoon.androidwallet.fragments;

import kotlin.jvm.internal.Lambda;
import net.safemoon.androidwallet.viewmodels.MyTokensListViewModel;

/* compiled from: SendtoFragment.kt */
/* loaded from: classes2.dex */
public final class SendtoFragment$onCreateView$4 extends Lambda implements rc1<te4> {
    public final /* synthetic */ SendtoFragment this$0;

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public SendtoFragment$onCreateView$4(SendtoFragment sendtoFragment) {
        super(0);
        this.this$0 = sendtoFragment;
    }

    @Override // defpackage.rc1
    public /* bridge */ /* synthetic */ te4 invoke() {
        invoke2();
        return te4.a;
    }

    @Override // defpackage.rc1
    /* renamed from: invoke  reason: avoid collision after fix types in other method */
    public final void invoke2() {
        MyTokensListViewModel myTokensListViewModel;
        MyTokensListViewModel myTokensListViewModel2;
        myTokensListViewModel = this.this$0.i0;
        fs1.d(myTokensListViewModel);
        myTokensListViewModel.Q();
        myTokensListViewModel2 = this.this$0.i0;
        fs1.d(myTokensListViewModel2);
        myTokensListViewModel2.P();
    }
}
