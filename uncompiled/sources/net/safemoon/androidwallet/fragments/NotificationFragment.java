package net.safemoon.androidwallet.fragments;

import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.ImageView;
import androidx.appcompat.widget.LinearLayoutCompat;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.fragment.app.FragmentActivity;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentViewModelLazyKt;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import com.google.android.material.button.MaterialButton;
import com.google.android.material.switchmaterial.SwitchMaterial;
import com.google.android.material.textfield.TextInputEditText;
import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Iterator;
import java.util.List;
import net.safemoon.androidwallet.R;
import net.safemoon.androidwallet.adapter.touchHelper.RecyclerTouchListener;
import net.safemoon.androidwallet.dialogs.CMCListCheckable;
import net.safemoon.androidwallet.fragments.NotificationFragment;
import net.safemoon.androidwallet.fragments.common.BaseMainFragment;
import net.safemoon.androidwallet.model.Coin;
import net.safemoon.androidwallet.model.RoomCoinPriceAlert;
import net.safemoon.androidwallet.model.priceAlert.PAToken;
import net.safemoon.androidwallet.viewmodels.SettingNotificationViewModel;

/* compiled from: NotificationFragment.kt */
/* loaded from: classes2.dex */
public final class NotificationFragment extends BaseMainFragment {
    public oa1 i0;
    public final sy1 j0 = FragmentViewModelLazyKt.a(this, d53.b(SettingNotificationViewModel.class), new NotificationFragment$special$$inlined$viewModels$default$2(new NotificationFragment$special$$inlined$viewModels$default$1(this)), null);
    public final sy1 k0 = zy1.a(new NotificationFragment$paTokenAdapter$2(this));
    public final sy1 l0 = zy1.a(new NotificationFragment$cmcListDialog$2(this));
    public final View.OnTouchListener m0 = new View.OnTouchListener() { // from class: rh2
        @Override // android.view.View.OnTouchListener
        public final boolean onTouch(View view, MotionEvent motionEvent) {
            boolean V;
            V = NotificationFragment.V(NotificationFragment.this, view, motionEvent);
            return V;
        }
    };
    public RecyclerTouchListener n0;

    /* compiled from: NotificationFragment.kt */
    /* loaded from: classes2.dex */
    public static final class a extends fm2 {
        public a() {
            super(true);
        }

        /* JADX WARN: Code restructure failed: missing block: B:12:0x001a, code lost:
            if ((r0.getVisibility() == 0) == true) goto L4;
         */
        @Override // defpackage.fm2
        /*
            Code decompiled incorrectly, please refer to instructions dump.
            To view partially-correct code enable 'Show inconsistent code' option in preferences
        */
        public void b() {
            /*
                r3 = this;
                net.safemoon.androidwallet.fragments.NotificationFragment r0 = net.safemoon.androidwallet.fragments.NotificationFragment.this
                oa1 r0 = net.safemoon.androidwallet.fragments.NotificationFragment.A(r0)
                r1 = 1
                r2 = 0
                if (r0 != 0) goto Lc
            La:
                r1 = r2
                goto L1c
            Lc:
                androidx.appcompat.widget.LinearLayoutCompat r0 = r0.b
                if (r0 != 0) goto L11
                goto La
            L11:
                int r0 = r0.getVisibility()
                if (r0 != 0) goto L19
                r0 = r1
                goto L1a
            L19:
                r0 = r2
            L1a:
                if (r0 != r1) goto La
            L1c:
                if (r1 == 0) goto L24
                net.safemoon.androidwallet.fragments.NotificationFragment r0 = net.safemoon.androidwallet.fragments.NotificationFragment.this
                net.safemoon.androidwallet.fragments.NotificationFragment.D(r0)
                goto L29
            L24:
                net.safemoon.androidwallet.fragments.NotificationFragment r0 = net.safemoon.androidwallet.fragments.NotificationFragment.this
                net.safemoon.androidwallet.fragments.NotificationFragment.F(r0)
            L29:
                return
            */
            throw new UnsupportedOperationException("Method not decompiled: net.safemoon.androidwallet.fragments.NotificationFragment.a.b():void");
        }
    }

    public static final void L(SwitchMaterial switchMaterial, Boolean bool) {
        fs1.f(switchMaterial, "$this_with");
        fs1.e(bool, "it");
        switchMaterial.setChecked(bool.booleanValue());
    }

    public static final void N(NotificationFragment notificationFragment, List list) {
        fs1.f(notificationFragment, "this$0");
        if (list == null) {
            return;
        }
        ArrayList arrayList = new ArrayList(c20.q(list, 10));
        Iterator it = list.iterator();
        while (it.hasNext()) {
            arrayList.add(((RoomCoinPriceAlert) it.next()).getCoinData());
        }
        if (notificationFragment.G().isVisible()) {
            CMCListCheckable G = notificationFragment.G();
            ArrayList<Coin> arrayList2 = new ArrayList();
            Iterator it2 = arrayList.iterator();
            while (true) {
                if (!it2.hasNext()) {
                    break;
                }
                Object next = it2.next();
                Coin coin = (Coin) next;
                if ((coin != null ? coin.getId() : null) != null) {
                    arrayList2.add(next);
                }
            }
            ArrayList arrayList3 = new ArrayList(c20.q(arrayList2, 10));
            for (Coin coin2 : arrayList2) {
                Integer id = coin2 == null ? null : coin2.getId();
                fs1.d(id);
                arrayList3.add(Integer.valueOf(id.intValue()));
            }
            int[] j0 = j20.j0(arrayList3);
            G.O(Arrays.copyOf(j0, j0.length));
        }
    }

    public static final void O(NotificationFragment notificationFragment, View view) {
        fs1.f(notificationFragment, "this$0");
        notificationFragment.Y();
    }

    public static final void P(final oa1 oa1Var, final NotificationFragment notificationFragment, List list) {
        fs1.f(oa1Var, "$this_with");
        fs1.f(notificationFragment, "this$0");
        if (list == null) {
            return;
        }
        oa1Var.b.removeAllViews();
        Iterator it = list.iterator();
        while (it.hasNext()) {
            final PAToken pAToken = (PAToken) it.next();
            View inflate = LayoutInflater.from(notificationFragment.requireContext()).inflate(R.layout.item_arrow_down_button, (ViewGroup) null);
            if (inflate instanceof Button) {
                ((Button) inflate).setText(pAToken.getName());
                oa1Var.b.addView(inflate);
                inflate.setOnClickListener(new View.OnClickListener() { // from class: ph2
                    @Override // android.view.View.OnClickListener
                    public final void onClick(View view) {
                        NotificationFragment.Q(NotificationFragment.this, pAToken, oa1Var, view);
                    }
                });
            }
        }
    }

    public static final void Q(NotificationFragment notificationFragment, PAToken pAToken, oa1 oa1Var, View view) {
        fs1.f(notificationFragment, "this$0");
        fs1.f(pAToken, "$pt");
        fs1.f(oa1Var, "$this_with");
        notificationFragment.a0(true);
        notificationFragment.H().O(Integer.valueOf(pAToken.getChainId()));
        notificationFragment.H().p(pAToken.getChainId());
        if (pAToken.getChainId() == -1) {
            notificationFragment.H().C().postValue("");
            TextInputEditText textInputEditText = oa1Var.i.b;
            fs1.e(textInputEditText, "stickySearchBar.etSearch");
            cj4.w(textInputEditText, "");
            TextInputEditText textInputEditText2 = oa1Var.h.b;
            fs1.e(textInputEditText2, "searchBar.etSearch");
            cj4.w(textInputEditText2, "");
            oa1Var.i.b.setOnTouchListener(notificationFragment.I());
            oa1Var.h.b.setOnTouchListener(notificationFragment.I());
            return;
        }
        notificationFragment.H().C().postValue(notificationFragment.H().D());
        TextInputEditText textInputEditText3 = oa1Var.i.b;
        fs1.e(textInputEditText3, "stickySearchBar.etSearch");
        cj4.w(textInputEditText3, notificationFragment.H().D());
        TextInputEditText textInputEditText4 = oa1Var.h.b;
        fs1.e(textInputEditText4, "searchBar.etSearch");
        cj4.w(textInputEditText4, notificationFragment.H().D());
        oa1Var.i.b.setOnTouchListener(null);
        oa1Var.h.b.setOnTouchListener(null);
    }

    public static final void R(NotificationFragment notificationFragment, int i, int i2) {
        fs1.f(notificationFragment, "this$0");
        if (i == R.id.btnDelete) {
            bh.P(new WeakReference(notificationFragment.requireActivity()), Integer.valueOf((int) R.string.confirm), Integer.valueOf((int) R.string.delete_any_confirm), null, Integer.valueOf((int) R.string.delete), Integer.valueOf((int) R.string.cancel), Integer.valueOf(m70.d(notificationFragment.requireContext(), R.color.red)), Integer.valueOf(m70.d(notificationFragment.requireContext(), R.color.t1)), new NotificationFragment$initPriceAlert$1$1$6$1$1$1(notificationFragment, notificationFragment.J().getCurrentList().get(i2)), NotificationFragment$initPriceAlert$1$1$6$1$1$2.INSTANCE);
        }
    }

    public static final void S(NotificationFragment notificationFragment, List list) {
        PAToken pAToken;
        RecyclerView recyclerView;
        RecyclerView recyclerView2;
        fs1.f(notificationFragment, "this$0");
        ho2 J = notificationFragment.J();
        ArrayList arrayList = null;
        if (list != null) {
            ArrayList arrayList2 = new ArrayList(c20.q(list, 10));
            Iterator it = list.iterator();
            while (it.hasNext()) {
                arrayList2.add(PAToken.copy$default((PAToken) it.next(), 0, null, null, null, null, null, false, 127, null));
            }
            ArrayList arrayList3 = new ArrayList();
            for (Object obj : arrayList2) {
                int chainId = ((PAToken) obj).getChainId();
                Integer w = notificationFragment.H().w();
                if (w != null && chainId == w.intValue()) {
                    arrayList3.add(obj);
                }
            }
            if (!arrayList3.isEmpty()) {
                arrayList = arrayList3;
            }
        }
        J.submitList(arrayList);
        RecyclerTouchListener recyclerTouchListener = notificationFragment.n0;
        if (recyclerTouchListener != null) {
            recyclerTouchListener.q();
            oa1 oa1Var = notificationFragment.i0;
            if (oa1Var != null && (recyclerView2 = oa1Var.f) != null) {
                recyclerView2.e1(recyclerTouchListener);
            }
        }
        if ((list == null || (pAToken = (PAToken) j20.M(list)) == null || pAToken.getChainId() != -1) ? false : true) {
            RecyclerTouchListener recyclerTouchListener2 = notificationFragment.n0;
            if (recyclerTouchListener2 != null) {
                recyclerTouchListener2.z((Integer[]) Arrays.copyOf(new Integer[0], 0));
            }
            oa1 oa1Var2 = notificationFragment.i0;
            if (oa1Var2 == null || (recyclerView = oa1Var2.f) == null) {
                return;
            }
            RecyclerTouchListener recyclerTouchListener3 = notificationFragment.n0;
            fs1.d(recyclerTouchListener3);
            recyclerView.k(recyclerTouchListener3);
        }
    }

    public static final void U(NotificationFragment notificationFragment, Boolean bool) {
        fs1.f(notificationFragment, "this$0");
        fs1.e(bool, "result");
        if (bool.booleanValue()) {
            notificationFragment.H().R();
        }
    }

    public static final boolean V(NotificationFragment notificationFragment, View view, MotionEvent motionEvent) {
        fs1.f(notificationFragment, "this$0");
        if (motionEvent.getAction() == 1) {
            if (notificationFragment.G().isVisible() || notificationFragment.G().isAdded()) {
                notificationFragment.G().h();
            }
            CMCListCheckable G = notificationFragment.G();
            FragmentManager childFragmentManager = notificationFragment.getChildFragmentManager();
            fs1.e(childFragmentManager, "childFragmentManager");
            G.P(childFragmentManager);
            return true;
        }
        return false;
    }

    public static final void W(NotificationFragment notificationFragment, View view) {
        fs1.f(notificationFragment, "this$0");
        notificationFragment.requireActivity().onBackPressed();
    }

    /* JADX WARN: Removed duplicated region for block: B:15:0x002f  */
    /* JADX WARN: Removed duplicated region for block: B:43:? A[RETURN, SYNTHETIC] */
    /*
        Code decompiled incorrectly, please refer to instructions dump.
        To view partially-correct code enable 'Show inconsistent code' option in preferences
    */
    public static final void X(net.safemoon.androidwallet.fragments.NotificationFragment r0, defpackage.oa1 r1, int[] r2, int[] r3, android.view.View r4, int r5, int r6, int r7, int r8) {
        /*
            java.lang.String r4 = "this$0"
            defpackage.fs1.f(r0, r4)
            java.lang.String r4 = "$it"
            defpackage.fs1.f(r1, r4)
            java.lang.String r4 = "$mRect"
            defpackage.fs1.f(r2, r4)
            java.lang.String r4 = "$toolbarRect"
            defpackage.fs1.f(r3, r4)
            oa1 r4 = r0.i0
            r5 = 1
            r6 = 0
            if (r4 != 0) goto L1c
        L1a:
            r4 = r6
            goto L2d
        L1c:
            androidx.appcompat.widget.LinearLayoutCompat r4 = r4.b
            if (r4 != 0) goto L21
            goto L1a
        L21:
            int r4 = r4.getVisibility()
            if (r4 != 0) goto L29
            r4 = r5
            goto L2a
        L29:
            r4 = r6
        L2a:
            if (r4 != r5) goto L1a
            r4 = r5
        L2d:
            if (r4 != 0) goto La4
            zd3 r4 = r1.h
            android.widget.LinearLayout r4 = r4.b()
            r4.getLocationOnScreen(r2)
            r2 = r2[r5]
            r3 = r3[r5]
            android.view.View r4 = r1.c
            int r4 = r4.getBottom()
            int r3 = r3 + r4
            android.content.res.Resources r0 = r0.getResources()
            r4 = 2131165219(0x7f070023, float:1.7944649E38)
            int r0 = r0.getDimensionPixelSize(r4)
            int r3 = r3 + r0
            int r2 = r2 - r3
            if (r2 > 0) goto L53
            goto L54
        L53:
            r5 = r6
        L54:
            zd3 r0 = r1.h
            com.google.android.material.textfield.TextInputEditText r0 = r0.b
            boolean r0 = r0.hasFocus()
            r2 = -99
            if (r0 == 0) goto L69
            zd3 r3 = r1.h
            com.google.android.material.textfield.TextInputEditText r3 = r3.b
            int r3 = r3.getSelectionEnd()
            goto L6a
        L69:
            r3 = r2
        L6a:
            zd3 r4 = r1.i
            com.google.android.material.textfield.TextInputEditText r4 = r4.b
            boolean r4 = r4.hasFocus()
            if (r4 == 0) goto L7c
            zd3 r2 = r1.i
            com.google.android.material.textfield.TextInputEditText r2 = r2.b
            int r2 = r2.getSelectionEnd()
        L7c:
            android.widget.FrameLayout r7 = r1.j
            java.lang.String r8 = "it.stickySearchWrapper"
            defpackage.fs1.e(r7, r8)
            if (r5 == 0) goto L86
            goto L88
        L86:
            r6 = 8
        L88:
            r7.setVisibility(r6)
            if (r0 == 0) goto L94
            zd3 r0 = r1.i     // Catch: java.lang.Exception -> L94
            com.google.android.material.textfield.TextInputEditText r0 = r0.b     // Catch: java.lang.Exception -> L94
            r0.setSelection(r3)     // Catch: java.lang.Exception -> L94
        L94:
            if (r4 == 0) goto La4
            zd3 r0 = r1.h
            com.google.android.material.textfield.TextInputEditText r0 = r0.b
            r0.requestFocus()
            zd3 r0 = r1.h     // Catch: java.lang.Exception -> La4
            com.google.android.material.textfield.TextInputEditText r0 = r0.b     // Catch: java.lang.Exception -> La4
            r0.setSelection(r2)     // Catch: java.lang.Exception -> La4
        La4:
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: net.safemoon.androidwallet.fragments.NotificationFragment.X(net.safemoon.androidwallet.fragments.NotificationFragment, oa1, int[], int[], android.view.View, int, int, int, int):void");
    }

    public static final void Z(NotificationFragment notificationFragment) {
        fs1.f(notificationFragment, "this$0");
        if (notificationFragment.isVisible()) {
            notificationFragment.a0(false);
        }
    }

    public final CMCListCheckable G() {
        return (CMCListCheckable) this.l0.getValue();
    }

    public final SettingNotificationViewModel H() {
        return (SettingNotificationViewModel) this.j0.getValue();
    }

    public final View.OnTouchListener I() {
        return this.m0;
    }

    public final ho2 J() {
        return (ho2) this.k0.getValue();
    }

    public final void K() {
        oa1 oa1Var = this.i0;
        if (oa1Var == null) {
            return;
        }
        final SwitchMaterial switchMaterial = oa1Var.k;
        fs1.e(switchMaterial, "");
        cj4.j(switchMaterial, new NotificationFragment$initEnableNotification$1$1$1(this));
        H().F().observe(getViewLifecycleOwner(), new tl2() { // from class: jh2
            @Override // defpackage.tl2
            public final void onChanged(Object obj) {
                NotificationFragment.L(SwitchMaterial.this, (Boolean) obj);
            }
        });
    }

    public final void M() {
        final oa1 oa1Var = this.i0;
        if (oa1Var == null) {
            return;
        }
        H().r().observe(getViewLifecycleOwner(), new tl2() { // from class: lh2
            @Override // defpackage.tl2
            public final void onChanged(Object obj) {
                NotificationFragment.N(NotificationFragment.this, (List) obj);
            }
        });
        TextInputEditText textInputEditText = oa1Var.i.b;
        fs1.e(textInputEditText, "stickySearchBar.etSearch");
        cj4.i(textInputEditText, new NotificationFragment$initPriceAlert$1$1$2(oa1Var, this));
        TextInputEditText textInputEditText2 = oa1Var.h.b;
        fs1.e(textInputEditText2, "searchBar.etSearch");
        cj4.i(textInputEditText2, new NotificationFragment$initPriceAlert$1$1$3(oa1Var, this));
        oa1Var.l.setOnClickListener(new View.OnClickListener() { // from class: nh2
            @Override // android.view.View.OnClickListener
            public final void onClick(View view) {
                NotificationFragment.O(NotificationFragment.this, view);
            }
        });
        H().v().observe(getViewLifecycleOwner(), new tl2() { // from class: gh2
            @Override // defpackage.tl2
            public final void onChanged(Object obj) {
                NotificationFragment.P(oa1.this, this, (List) obj);
            }
        });
        oa1Var.f.setLayoutManager(new LinearLayoutManager(requireContext(), 1, false));
        oa1Var.f.setAdapter(J());
        FragmentActivity requireActivity = requireActivity();
        oa1 oa1Var2 = this.i0;
        RecyclerTouchListener recyclerTouchListener = new RecyclerTouchListener(requireActivity, oa1Var2 == null ? null : oa1Var2.f);
        recyclerTouchListener.x(Integer.valueOf((int) R.id.btnDelete));
        recyclerTouchListener.y(R.id.rowFG, R.id.rowBG, new RecyclerTouchListener.k() { // from class: ih2
            @Override // net.safemoon.androidwallet.adapter.touchHelper.RecyclerTouchListener.k
            public final void a(int i, int i2) {
                NotificationFragment.R(NotificationFragment.this, i, i2);
            }
        });
        te4 te4Var = te4.a;
        this.n0 = recyclerTouchListener;
        H().A().observe(getViewLifecycleOwner(), new tl2() { // from class: mh2
            @Override // defpackage.tl2
            public final void onChanged(Object obj) {
                NotificationFragment.S(NotificationFragment.this, (List) obj);
            }
        });
    }

    public final void T() {
        gb2 b;
        xd2 h = ka1.a(this).h();
        ec3 d = h == null ? null : h.d();
        if (d == null || (b = d.b("RESULT_FROM_CRYPTO_PRICE_ALERT")) == null) {
            return;
        }
        b.observe(h, new tl2() { // from class: kh2
            @Override // defpackage.tl2
            public final void onChanged(Object obj) {
                NotificationFragment.U(NotificationFragment.this, (Boolean) obj);
            }
        });
    }

    /* JADX WARN: Code restructure failed: missing block: B:12:0x0016, code lost:
        if ((r0.getVisibility() == 0) == false) goto L4;
     */
    /*
        Code decompiled incorrectly, please refer to instructions dump.
        To view partially-correct code enable 'Show inconsistent code' option in preferences
    */
    public final void Y() {
        /*
            r4 = this;
            oa1 r0 = r4.i0
            r1 = 1
            r2 = 0
            if (r0 != 0) goto L8
        L6:
            r1 = r2
            goto L18
        L8:
            androidx.appcompat.widget.LinearLayoutCompat r0 = r0.b
            if (r0 != 0) goto Ld
            goto L6
        Ld:
            int r0 = r0.getVisibility()
            if (r0 != 0) goto L15
            r0 = r1
            goto L16
        L15:
            r0 = r2
        L16:
            if (r0 != 0) goto L6
        L18:
            if (r1 == 0) goto L7f
            androidx.fragment.app.FragmentActivity r0 = r4.requireActivity()
            defpackage.pg4.e(r0)
            net.safemoon.androidwallet.viewmodels.SettingNotificationViewModel r0 = r4.H()
            gb2 r0 = r0.z()
            java.util.List r1 = defpackage.b20.g()
            r0.postValue(r1)
            net.safemoon.androidwallet.viewmodels.SettingNotificationViewModel r0 = r4.H()
            java.lang.Integer r0 = r0.w()
            r1 = -1
            if (r0 != 0) goto L3c
            goto L42
        L3c:
            int r0 = r0.intValue()
            if (r0 == r1) goto L57
        L42:
            net.safemoon.androidwallet.viewmodels.SettingNotificationViewModel r0 = r4.H()
            net.safemoon.androidwallet.viewmodels.SettingNotificationViewModel r1 = r4.H()
            gb2 r1 = r1.C()
            java.lang.Object r1 = r1.getValue()
            java.lang.String r1 = (java.lang.String) r1
            r0.P(r1)
        L57:
            net.safemoon.androidwallet.viewmodels.SettingNotificationViewModel r0 = r4.H()
            gb2 r0 = r0.C()
            java.lang.String r1 = ""
            r0.postValue(r1)
            net.safemoon.androidwallet.viewmodels.SettingNotificationViewModel r0 = r4.H()
            r1 = 0
            r0.O(r1)
            android.os.Handler r0 = new android.os.Handler
            android.os.Looper r1 = android.os.Looper.getMainLooper()
            r0.<init>(r1)
            hh2 r1 = new hh2
            r1.<init>()
            r2 = 50
            r0.postDelayed(r1, r2)
        L7f:
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: net.safemoon.androidwallet.fragments.NotificationFragment.Y():void");
    }

    public final void a0(boolean z) {
        Drawable f;
        oa1 oa1Var = this.i0;
        if (oa1Var == null) {
            return;
        }
        ConstraintLayout constraintLayout = oa1Var.d;
        fs1.e(constraintLayout, "it.cvTokenWrapper");
        constraintLayout.setVisibility(z ? 0 : 8);
        LinearLayoutCompat linearLayoutCompat = oa1Var.b;
        fs1.e(linearLayoutCompat, "it.chainWrapper");
        linearLayoutCompat.setVisibility(z ^ true ? 0 : 8);
        if (!z) {
            FrameLayout frameLayout = oa1Var.j;
            fs1.e(frameLayout, "it.stickySearchWrapper");
            frameLayout.setVisibility(8);
        }
        MaterialButton materialButton = oa1Var.l;
        Drawable drawable = null;
        if (z && (f = m70.f(requireContext(), R.drawable.ic_baseline_keyboard_arrow_down_24)) != null) {
            f.setTint(m70.d(requireContext(), R.color.setting_color));
            te4 te4Var = te4.a;
            drawable = f;
        }
        materialButton.setIcon(drawable);
    }

    @Override // androidx.fragment.app.Fragment
    public View onCreateView(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle) {
        fs1.f(layoutInflater, "inflater");
        oa1 a2 = oa1.a(layoutInflater.inflate(R.layout.fragment_notification, viewGroup, false));
        this.i0 = a2;
        fs1.d(a2);
        ConstraintLayout b = a2.b();
        fs1.e(b, "binding!!.root");
        return b;
    }

    @Override // net.safemoon.androidwallet.fragments.common.BaseMainFragment, defpackage.qn, androidx.fragment.app.Fragment
    public void onViewCreated(View view, Bundle bundle) {
        ImageView imageView;
        fs1.f(view, "view");
        super.onViewCreated(view, bundle);
        H().G();
        oa1 oa1Var = this.i0;
        if (oa1Var != null && (imageView = oa1Var.e) != null) {
            imageView.setOnClickListener(new View.OnClickListener() { // from class: oh2
                @Override // android.view.View.OnClickListener
                public final void onClick(View view2) {
                    NotificationFragment.W(NotificationFragment.this, view2);
                }
            });
        }
        if (bundle == null) {
            requireActivity().getOnBackPressedDispatcher().a(getViewLifecycleOwner(), new a());
        }
        K();
        M();
        T();
        final oa1 oa1Var2 = this.i0;
        if (oa1Var2 == null) {
            return;
        }
        final int[] iArr = new int[2];
        final int[] iArr2 = new int[2];
        oa1Var2.c.getLocationOnScreen(iArr);
        oa1Var2.g.setOnScrollChangeListener(new View.OnScrollChangeListener() { // from class: qh2
            @Override // android.view.View.OnScrollChangeListener
            public final void onScrollChange(View view2, int i, int i2, int i3, int i4) {
                NotificationFragment.X(NotificationFragment.this, oa1Var2, iArr2, iArr, view2, i, i2, i3, i4);
            }
        });
    }
}
