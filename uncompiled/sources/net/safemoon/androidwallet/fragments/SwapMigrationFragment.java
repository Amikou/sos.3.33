package net.safemoon.androidwallet.fragments;

import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.text.Editable;
import android.text.InputFilter;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import androidx.appcompat.widget.AppCompatTextView;
import androidx.fragment.app.FragmentActivity;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentViewModelLazyKt;
import androidx.lifecycle.LiveData;
import com.github.mikephil.charting.utils.Utils;
import com.google.android.material.button.MaterialButton;
import com.google.android.material.chip.Chip;
import com.google.android.material.textview.MaterialTextView;
import java.lang.ref.WeakReference;
import java.math.BigDecimal;
import java.util.Arrays;
import java.util.List;
import java.util.Locale;
import kotlin.text.StringsKt__StringsKt;
import net.safemoon.androidwallet.R;
import net.safemoon.androidwallet.common.TokenType;
import net.safemoon.androidwallet.dialogs.AnchorSwitchWallet;
import net.safemoon.androidwallet.dialogs.BestMaxFragment;
import net.safemoon.androidwallet.dialogs.ProgressLoading;
import net.safemoon.androidwallet.dialogs.SwapTransactionSpeed;
import net.safemoon.androidwallet.dialogs.SwapTransactionTimeLimit;
import net.safemoon.androidwallet.fragments.SwapMigrationFragment;
import net.safemoon.androidwallet.fragments.common.BaseMainFragment;
import net.safemoon.androidwallet.model.common.Gas;
import net.safemoon.androidwallet.model.common.LoadingState;
import net.safemoon.androidwallet.model.swap.Swap;
import net.safemoon.androidwallet.model.wallets.Wallet;
import net.safemoon.androidwallet.provider.AskAuthorizeProvider;
import net.safemoon.androidwallet.ui.displayModel.UserTokenItemDisplayModel;
import net.safemoon.androidwallet.viewmodels.HomeViewModel;
import net.safemoon.androidwallet.viewmodels.MultiWalletViewModel;
import net.safemoon.androidwallet.viewmodels.MyTokensListViewModel;
import net.safemoon.androidwallet.viewmodels.SwapMigrationViewModel;
import net.safemoon.androidwallet.views.editText.autoSize.AutofitEdittext;

/* compiled from: SwapMigrationFragment.kt */
/* loaded from: classes2.dex */
public final class SwapMigrationFragment extends BaseMainFragment {
    public db1 q0;
    public ck4 t0;
    public ck4 u0;
    public ProgressLoading z0;
    public final String i0 = "0.01";
    public final String j0 = "BNB";
    public final String k0 = "ETH";
    public final String l0 = "SFM";
    public final String m0 = "SAFEMOON";
    public final int n0 = 100;
    public final int o0 = 200;
    public final int p0 = 200;
    public final sy1 r0 = FragmentViewModelLazyKt.a(this, d53.b(HomeViewModel.class), new SwapMigrationFragment$special$$inlined$activityViewModels$default$1(this), new SwapMigrationFragment$special$$inlined$activityViewModels$default$2(this));
    public final sy1 s0 = FragmentViewModelLazyKt.a(this, d53.b(SwapMigrationViewModel.class), new SwapMigrationFragment$special$$inlined$viewModels$default$2(new SwapMigrationFragment$special$$inlined$viewModels$default$1(this)), null);
    public final sy1 v0 = FragmentViewModelLazyKt.a(this, d53.b(MyTokensListViewModel.class), new SwapMigrationFragment$special$$inlined$activityViewModels$1(this), new SwapMigrationFragment$myTokenListViewModel$2(this));
    public final sy1 w0 = zy1.a(new SwapMigrationFragment$argDefaultSwapParam$2(this));
    public final sy1 x0 = FragmentViewModelLazyKt.a(this, d53.b(MultiWalletViewModel.class), new SwapMigrationFragment$special$$inlined$viewModels$default$4(new SwapMigrationFragment$special$$inlined$viewModels$default$3(this)), null);
    public final sy1 y0 = zy1.a(new SwapMigrationFragment$iHomeActivity$2(this));

    /* compiled from: SwapMigrationFragment.kt */
    /* loaded from: classes2.dex */
    public static final class a {
        public a() {
        }

        public /* synthetic */ a(qi0 qi0Var) {
            this();
        }
    }

    /* compiled from: SwapMigrationFragment.kt */
    /* loaded from: classes2.dex */
    public /* synthetic */ class b {
        public static final /* synthetic */ int[] a;

        static {
            int[] iArr = new int[Gas.values().length];
            iArr[Gas.Standard.ordinal()] = 1;
            iArr[Gas.Fast.ordinal()] = 2;
            iArr[Gas.Lightning.ordinal()] = 3;
            a = iArr;
        }
    }

    /* compiled from: SwapMigrationFragment.kt */
    /* loaded from: classes2.dex */
    public static final class c extends jq0 {
        /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
        public c(AutofitEdittext autofitEdittext) {
            super(autofitEdittext);
            fs1.e(autofitEdittext, "newValue");
        }

        @Override // defpackage.jq0, android.text.TextWatcher
        public void afterTextChanged(Editable editable) {
            BigDecimal bigDecimal;
            super.afterTextChanged(editable);
            ck4 ck4Var = SwapMigrationFragment.this.t0;
            if (ck4Var == null) {
                fs1.r("bindingSource");
                ck4Var = null;
            }
            if (ck4Var.j.isFocused()) {
                return;
            }
            gb2<SwapMigrationViewModel.a> c0 = SwapMigrationFragment.this.u0().c0();
            if (editable != null) {
                if (editable.length() > 0) {
                    bigDecimal = new BigDecimal(String.valueOf(e30.K(editable.toString())));
                    c0.postValue(new SwapMigrationViewModel.a(false, bigDecimal));
                }
            }
            bigDecimal = new BigDecimal(0);
            c0.postValue(new SwapMigrationViewModel.a(false, bigDecimal));
        }
    }

    /* compiled from: SwapMigrationFragment.kt */
    /* loaded from: classes2.dex */
    public static final class d extends jq0 {
        /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
        public d(AutofitEdittext autofitEdittext) {
            super(autofitEdittext);
            fs1.e(autofitEdittext, "newValue");
        }

        @Override // defpackage.jq0, android.text.TextWatcher
        public void afterTextChanged(Editable editable) {
            BigDecimal bigDecimal;
            super.afterTextChanged(editable);
            try {
                ck4 ck4Var = SwapMigrationFragment.this.t0;
                if (ck4Var == null) {
                    fs1.r("bindingSource");
                    ck4Var = null;
                }
                if (ck4Var.j.isFocused()) {
                    gb2<SwapMigrationViewModel.a> c0 = SwapMigrationFragment.this.u0().c0();
                    if (editable != null) {
                        if (editable.length() > 0) {
                            bigDecimal = new BigDecimal(String.valueOf(e30.K(editable.toString())));
                            c0.postValue(new SwapMigrationViewModel.a(true, bigDecimal));
                        }
                    }
                    bigDecimal = new BigDecimal(0);
                    c0.postValue(new SwapMigrationViewModel.a(true, bigDecimal));
                }
            } catch (Exception unused) {
            }
        }

        @Override // defpackage.jq0, android.text.TextWatcher
        public void onTextChanged(CharSequence charSequence, int i, int i2, int i3) {
            String obj;
            fs1.f(charSequence, "s");
            super.onTextChanged(charSequence, i, i2, i3);
            db1 db1Var = SwapMigrationFragment.this.q0;
            db1 db1Var2 = null;
            if (db1Var == null) {
                fs1.r("binding");
                db1Var = null;
            }
            db1Var.b.setVisibility(8);
            ck4 ck4Var = SwapMigrationFragment.this.t0;
            if (ck4Var == null) {
                fs1.r("bindingSource");
                ck4Var = null;
            }
            Editable text = ck4Var.j.getText();
            String obj2 = (text == null || (obj = text.toString()) == null) ? null : StringsKt__StringsKt.K0(obj).toString();
            if (obj2 == null || obj2.length() == 0) {
                db1 db1Var3 = SwapMigrationFragment.this.q0;
                if (db1Var3 == null) {
                    fs1.r("binding");
                    db1Var3 = null;
                }
                db1Var3.c.setVisibility(0);
                db1 db1Var4 = SwapMigrationFragment.this.q0;
                if (db1Var4 == null) {
                    fs1.r("binding");
                } else {
                    db1Var2 = db1Var4;
                }
                db1Var2.d.setVisibility(8);
                return;
            }
            db1 db1Var5 = SwapMigrationFragment.this.q0;
            if (db1Var5 == null) {
                fs1.r("binding");
                db1Var5 = null;
            }
            db1Var5.c.setVisibility(8);
            db1 db1Var6 = SwapMigrationFragment.this.q0;
            if (db1Var6 == null) {
                fs1.r("binding");
            } else {
                db1Var2 = db1Var6;
            }
            db1Var2.d.setVisibility(0);
        }
    }

    static {
        new a(null);
    }

    public static final void B0(ck4 ck4Var, boolean z) {
        fs1.f(ck4Var, "$this_run");
        MaterialButton materialButton = ck4Var.e;
        fs1.e(materialButton, "btnClearText");
        materialButton.setVisibility(z ? 0 : 8);
    }

    public static final void C0(SwapMigrationFragment swapMigrationFragment, Swap swap) {
        fs1.f(swapMigrationFragment, "this$0");
        if (swap == null) {
            return;
        }
        ck4 ck4Var = swapMigrationFragment.t0;
        if (ck4Var == null) {
            fs1.r("bindingSource");
            ck4Var = null;
        }
        String str = swap.logoURI;
        fs1.e(str, "it.logoURI");
        if ((str.length() > 0) || swap.imageResource > 0) {
            String str2 = swap.logoURI;
            String str3 = swap.symbol;
            fs1.e(str3, "it.symbol");
            com.bumptech.glide.a.u(ck4Var.i).x(kt.f(new i00(str2, str3, Integer.valueOf(swap.imageResource)))).d0(swapMigrationFragment.o0, swapMigrationFragment.p0).a(n73.v0()).I0(ck4Var.i);
        }
        MaterialTextView materialTextView = ck4Var.n;
        materialTextView.setText(((Object) swap.name) + " (" + ((Object) swap.symbol) + ')');
        SwapMigrationViewModel.a value = swapMigrationFragment.u0().c0().getValue();
        if (value != null && value.b()) {
            ck4Var.j.setText("");
            gb2<SwapMigrationViewModel.a> c0 = swapMigrationFragment.u0().c0();
            BigDecimal bigDecimal = BigDecimal.ZERO;
            fs1.e(bigDecimal, "ZERO");
            c0.postValue(new SwapMigrationViewModel.a(true, bigDecimal));
        }
    }

    public static final void D0(SwapMigrationFragment swapMigrationFragment, BigDecimal bigDecimal) {
        fs1.f(swapMigrationFragment, "this$0");
        ck4 ck4Var = swapMigrationFragment.t0;
        if (ck4Var == null) {
            fs1.r("bindingSource");
            ck4Var = null;
        }
        AppCompatTextView appCompatTextView = ck4Var.g;
        lu3 lu3Var = lu3.a;
        Locale locale = Locale.getDefault();
        String string = swapMigrationFragment.getString(R.string.text_swap_coin_bal);
        fs1.e(string, "getString(R.string.text_swap_coin_bal)");
        Object[] objArr = new Object[1];
        Context context = swapMigrationFragment.getContext();
        objArr[0] = context != null ? e30.y(bigDecimal.doubleValue(), context) : null;
        String format = String.format(locale, string, Arrays.copyOf(objArr, 1));
        fs1.e(format, "java.lang.String.format(locale, format, *args)");
        appCompatTextView.setText(format);
    }

    public static final void E0(SwapMigrationFragment swapMigrationFragment, Double d2) {
        fs1.f(swapMigrationFragment, "this$0");
        ck4 ck4Var = swapMigrationFragment.t0;
        if (ck4Var == null) {
            fs1.r("bindingSource");
            ck4Var = null;
        }
        double d3 = Utils.DOUBLE_EPSILON;
        if (fs1.a(d2, Utils.DOUBLE_EPSILON)) {
            ck4Var.l.setVisibility(4);
        } else {
            ck4Var.l.setVisibility(0);
        }
        MaterialTextView materialTextView = ck4Var.l;
        Object[] objArr = new Object[2];
        objArr[0] = u21.a.b();
        if (d2 != null) {
            d3 = d2.doubleValue();
        }
        objArr[1] = e30.p(v21.a(d3), 0, null, false, 7, null);
        materialTextView.setText(swapMigrationFragment.getString(R.string.text_swap_current_price, objArr));
    }

    public static final void H0(SwapMigrationFragment swapMigrationFragment, Double d2) {
        fs1.f(swapMigrationFragment, "this$0");
        if (d2 != null) {
            db1 db1Var = swapMigrationFragment.q0;
            db1 db1Var2 = null;
            if (db1Var == null) {
                fs1.r("binding");
                db1Var = null;
            }
            TextView textView = db1Var.t;
            fs1.e(textView, "binding.txtSymbol");
            e30.W(textView);
            db1 db1Var3 = swapMigrationFragment.q0;
            if (db1Var3 == null) {
                fs1.r("binding");
            } else {
                db1Var2 = db1Var3;
            }
            MaterialTextView materialTextView = db1Var2.r;
            fs1.e(materialTextView, "binding.tvTotal");
            e30.N(materialTextView, d2.doubleValue(), false);
        }
    }

    public static final void I0(SwapMigrationFragment swapMigrationFragment, Gas gas) {
        fs1.f(swapMigrationFragment, "this$0");
        int i = gas == null ? -1 : b.a[gas.ordinal()];
        db1 db1Var = null;
        if (i == 1) {
            db1 db1Var2 = swapMigrationFragment.q0;
            if (db1Var2 == null) {
                fs1.r("binding");
            } else {
                db1Var = db1Var2;
            }
            db1Var.j.setText(swapMigrationFragment.getString(R.string.swap_speed_standard));
        } else if (i == 2) {
            db1 db1Var3 = swapMigrationFragment.q0;
            if (db1Var3 == null) {
                fs1.r("binding");
            } else {
                db1Var = db1Var3;
            }
            db1Var.j.setText(swapMigrationFragment.getString(R.string.swap_speed_fast));
        } else if (i != 3) {
        } else {
            db1 db1Var4 = swapMigrationFragment.q0;
            if (db1Var4 == null) {
                fs1.r("binding");
            } else {
                db1Var = db1Var4;
            }
            db1Var.j.setText(swapMigrationFragment.getString(R.string.swap_speed_lightning));
        }
    }

    public static final void J0(SwapMigrationFragment swapMigrationFragment, Integer num) {
        fs1.f(swapMigrationFragment, "this$0");
        db1 db1Var = swapMigrationFragment.q0;
        if (db1Var == null) {
            fs1.r("binding");
            db1Var = null;
        }
        Chip chip = db1Var.h;
        lu3 lu3Var = lu3.a;
        Locale locale = Locale.getDefault();
        String string = swapMigrationFragment.getString(R.string.text_swap_min_txn);
        fs1.e(string, "getString(R.string.text_swap_min_txn)");
        String format = String.format(locale, string, Arrays.copyOf(new Object[]{num}, 1));
        fs1.e(format, "java.lang.String.format(locale, format, *args)");
        chip.setText(format);
    }

    public static final void K0(SwapMigrationFragment swapMigrationFragment, SwapMigrationViewModel.c cVar) {
        fs1.f(swapMigrationFragment, "this$0");
        db1 db1Var = swapMigrationFragment.q0;
        if (db1Var == null) {
            fs1.r("binding");
            db1Var = null;
        }
        db1Var.d.setEnabled((cVar == null || fs1.b(cVar.a(), "ERROR")) ? false : true);
    }

    public static final void L0(SwapMigrationFragment swapMigrationFragment, String str) {
        fs1.f(swapMigrationFragment, "this$0");
        db1 db1Var = swapMigrationFragment.q0;
        db1 db1Var2 = null;
        if (db1Var == null) {
            fs1.r("binding");
            db1Var = null;
        }
        MaterialTextView materialTextView = db1Var.s;
        fs1.e(str, "it");
        materialTextView.setVisibility(str.length() == 0 ? 8 : 0);
        db1 db1Var3 = swapMigrationFragment.q0;
        if (db1Var3 == null) {
            fs1.r("binding");
        } else {
            db1Var2 = db1Var3;
        }
        db1Var2.s.setText(str);
    }

    public static final void M0(SwapMigrationFragment swapMigrationFragment, View view) {
        fs1.f(swapMigrationFragment, "this$0");
        SwapTransactionSpeed a2 = SwapTransactionSpeed.x0.a();
        FragmentManager childFragmentManager = swapMigrationFragment.getChildFragmentManager();
        fs1.e(childFragmentManager, "childFragmentManager");
        a2.F(childFragmentManager);
    }

    public static final void N0(SwapMigrationFragment swapMigrationFragment, View view) {
        fs1.f(swapMigrationFragment, "this$0");
        SwapTransactionTimeLimit a2 = SwapTransactionTimeLimit.w0.a();
        FragmentManager childFragmentManager = swapMigrationFragment.getChildFragmentManager();
        fs1.e(childFragmentManager, "childFragmentManager");
        a2.E(childFragmentManager);
    }

    public static final void O0(SwapMigrationFragment swapMigrationFragment, View view) {
        fs1.f(swapMigrationFragment, "this$0");
        swapMigrationFragment.u0().w();
    }

    public static final void P0(SwapMigrationFragment swapMigrationFragment, View view) {
        fs1.f(swapMigrationFragment, "this$0");
        no0 a2 = no0.x0.a(new SwapMigrationFragment$onViewCreated$15$1(swapMigrationFragment), null);
        FragmentManager childFragmentManager = swapMigrationFragment.getChildFragmentManager();
        fs1.e(childFragmentManager, "childFragmentManager");
        a2.D(childFragmentManager);
    }

    public static final void Q0(SwapMigrationFragment swapMigrationFragment, LoadingState loadingState) {
        fs1.f(swapMigrationFragment, "this$0");
        if (loadingState == null) {
            return;
        }
        if (loadingState == LoadingState.Loading) {
            String string = swapMigrationFragment.getString(R.string.swap_approving_title);
            fs1.e(string, "getString(R.string.swap_approving_title)");
            String string2 = swapMigrationFragment.getString(R.string.swap_approving_msg);
            fs1.e(string2, "getString(R.string.swap_approving_msg)");
            swapMigrationFragment.l1(false, string, string2);
            return;
        }
        swapMigrationFragment.p0();
    }

    public static final void R0(SwapMigrationFragment swapMigrationFragment, View view) {
        fs1.f(swapMigrationFragment, "$this_run");
        swapMigrationFragment.k1(25.0f);
    }

    public static final void S0(SwapMigrationFragment swapMigrationFragment, View view) {
        fs1.f(swapMigrationFragment, "$this_run");
        swapMigrationFragment.k1(50.0f);
    }

    public static final void T0(SwapMigrationFragment swapMigrationFragment, View view) {
        fs1.f(swapMigrationFragment, "$this_run");
        swapMigrationFragment.k1(75.0f);
    }

    public static final void U0(SwapMigrationFragment swapMigrationFragment, View view) {
        fs1.f(swapMigrationFragment, "$this_run");
        swapMigrationFragment.k1(100.0f);
    }

    public static final void V0(SwapMigrationFragment swapMigrationFragment, SwapMigrationViewModel.a aVar) {
        fs1.f(swapMigrationFragment, "this$0");
        swapMigrationFragment.u0().m0();
    }

    /* JADX WARN: Removed duplicated region for block: B:22:0x0053  */
    /* JADX WARN: Removed duplicated region for block: B:39:0x008e  */
    /* JADX WARN: Removed duplicated region for block: B:40:0x0092  */
    /*
        Code decompiled incorrectly, please refer to instructions dump.
        To view partially-correct code enable 'Show inconsistent code' option in preferences
    */
    public static final void W0(net.safemoon.androidwallet.fragments.SwapMigrationFragment r8, java.lang.Double r9) {
        /*
            java.lang.String r0 = "this$0"
            defpackage.fs1.f(r8, r0)
            if (r9 == 0) goto La7
            net.safemoon.androidwallet.viewmodels.SwapMigrationViewModel r0 = r8.u0()
            gb2 r0 = r0.c0()
            java.lang.Object r0 = r0.getValue()
            if (r0 == 0) goto La7
            net.safemoon.androidwallet.viewmodels.SwapMigrationViewModel r0 = r8.u0()
            gb2 r0 = r0.c0()
            java.lang.Object r0 = r0.getValue()
            defpackage.fs1.d(r0)
            net.safemoon.androidwallet.viewmodels.SwapMigrationViewModel$a r0 = (net.safemoon.androidwallet.viewmodels.SwapMigrationViewModel.a) r0
            boolean r0 = r0.b()
            if (r0 != 0) goto La7
            ck4 r0 = r8.u0
            java.lang.String r1 = "bindingDestination"
            r2 = 0
            if (r0 != 0) goto L37
            defpackage.fs1.r(r1)
            r0 = r2
        L37:
            net.safemoon.androidwallet.views.editText.autoSize.AutofitEdittext r0 = r0.j
            android.text.Editable r0 = r0.getText()
            r3 = 1
            r4 = 0
            if (r0 != 0) goto L43
        L41:
            r0 = r4
            goto L4f
        L43:
            int r0 = r0.length()
            if (r0 != 0) goto L4b
            r0 = r3
            goto L4c
        L4b:
            r0 = r4
        L4c:
            if (r0 != r3) goto L41
            r0 = r3
        L4f:
            java.lang.String r5 = "bindingSource"
            if (r0 != 0) goto L8a
            ck4 r0 = r8.u0
            if (r0 != 0) goto L5b
            defpackage.fs1.r(r1)
            r0 = r2
        L5b:
            net.safemoon.androidwallet.views.editText.autoSize.AutofitEdittext r0 = r0.j
            android.text.Editable r0 = r0.getText()
            if (r0 != 0) goto L65
        L63:
            r3 = r4
            goto L6b
        L65:
            boolean r0 = defpackage.dv3.w(r0)
            if (r0 != r3) goto L63
        L6b:
            if (r3 == 0) goto L6e
            goto L8a
        L6e:
            ck4 r0 = r8.t0
            if (r0 != 0) goto L76
            defpackage.fs1.r(r5)
            goto L77
        L76:
            r2 = r0
        L77:
            net.safemoon.androidwallet.views.editText.autoSize.AutofitEdittext r0 = r2.j
            double r1 = r9.doubleValue()
            r3 = 0
            r4 = 0
            r5 = 1
            r6 = 3
            r7 = 0
            java.lang.String r9 = defpackage.e30.p(r1, r3, r4, r5, r6, r7)
            r0.setText(r9)
            goto L9a
        L8a:
            ck4 r9 = r8.t0
            if (r9 != 0) goto L92
            defpackage.fs1.r(r5)
            goto L93
        L92:
            r2 = r9
        L93:
            net.safemoon.androidwallet.views.editText.autoSize.AutofitEdittext r9 = r2.j
            java.lang.String r0 = ""
            r9.setText(r0)
        L9a:
            net.safemoon.androidwallet.viewmodels.SwapMigrationViewModel r8 = r8.u0()
            gb2 r8 = r8.G()
            java.lang.Boolean r9 = java.lang.Boolean.FALSE
            r8.setValue(r9)
        La7:
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: net.safemoon.androidwallet.fragments.SwapMigrationFragment.W0(net.safemoon.androidwallet.fragments.SwapMigrationFragment, java.lang.Double):void");
    }

    /* JADX WARN: Removed duplicated region for block: B:22:0x0053  */
    /* JADX WARN: Removed duplicated region for block: B:39:0x008e  */
    /* JADX WARN: Removed duplicated region for block: B:40:0x0092  */
    /*
        Code decompiled incorrectly, please refer to instructions dump.
        To view partially-correct code enable 'Show inconsistent code' option in preferences
    */
    public static final void X0(net.safemoon.androidwallet.fragments.SwapMigrationFragment r8, java.lang.Double r9) {
        /*
            java.lang.String r0 = "this$0"
            defpackage.fs1.f(r8, r0)
            if (r9 == 0) goto La7
            net.safemoon.androidwallet.viewmodels.SwapMigrationViewModel r0 = r8.u0()
            gb2 r0 = r0.c0()
            java.lang.Object r0 = r0.getValue()
            if (r0 == 0) goto La7
            net.safemoon.androidwallet.viewmodels.SwapMigrationViewModel r0 = r8.u0()
            gb2 r0 = r0.c0()
            java.lang.Object r0 = r0.getValue()
            defpackage.fs1.d(r0)
            net.safemoon.androidwallet.viewmodels.SwapMigrationViewModel$a r0 = (net.safemoon.androidwallet.viewmodels.SwapMigrationViewModel.a) r0
            boolean r0 = r0.b()
            if (r0 == 0) goto La7
            ck4 r0 = r8.t0
            java.lang.String r1 = "bindingSource"
            r2 = 0
            if (r0 != 0) goto L37
            defpackage.fs1.r(r1)
            r0 = r2
        L37:
            net.safemoon.androidwallet.views.editText.autoSize.AutofitEdittext r0 = r0.j
            android.text.Editable r0 = r0.getText()
            r3 = 1
            r4 = 0
            if (r0 != 0) goto L43
        L41:
            r0 = r4
            goto L4f
        L43:
            int r0 = r0.length()
            if (r0 != 0) goto L4b
            r0 = r3
            goto L4c
        L4b:
            r0 = r4
        L4c:
            if (r0 != r3) goto L41
            r0 = r3
        L4f:
            java.lang.String r5 = "bindingDestination"
            if (r0 != 0) goto L8a
            ck4 r0 = r8.t0
            if (r0 != 0) goto L5b
            defpackage.fs1.r(r1)
            r0 = r2
        L5b:
            net.safemoon.androidwallet.views.editText.autoSize.AutofitEdittext r0 = r0.j
            android.text.Editable r0 = r0.getText()
            if (r0 != 0) goto L65
        L63:
            r3 = r4
            goto L6b
        L65:
            boolean r0 = defpackage.dv3.w(r0)
            if (r0 != r3) goto L63
        L6b:
            if (r3 == 0) goto L6e
            goto L8a
        L6e:
            ck4 r0 = r8.u0
            if (r0 != 0) goto L76
            defpackage.fs1.r(r5)
            goto L77
        L76:
            r2 = r0
        L77:
            net.safemoon.androidwallet.views.editText.autoSize.AutofitEdittext r0 = r2.j
            double r1 = r9.doubleValue()
            r3 = 0
            r4 = 0
            r5 = 1
            r6 = 3
            r7 = 0
            java.lang.String r9 = defpackage.e30.p(r1, r3, r4, r5, r6, r7)
            r0.setText(r9)
            goto L9a
        L8a:
            ck4 r9 = r8.u0
            if (r9 != 0) goto L92
            defpackage.fs1.r(r5)
            goto L93
        L92:
            r2 = r9
        L93:
            net.safemoon.androidwallet.views.editText.autoSize.AutofitEdittext r9 = r2.j
            java.lang.String r0 = ""
            r9.setText(r0)
        L9a:
            net.safemoon.androidwallet.viewmodels.SwapMigrationViewModel r8 = r8.u0()
            gb2 r8 = r8.G()
            java.lang.Boolean r9 = java.lang.Boolean.FALSE
            r8.setValue(r9)
        La7:
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: net.safemoon.androidwallet.fragments.SwapMigrationFragment.X0(net.safemoon.androidwallet.fragments.SwapMigrationFragment, java.lang.Double):void");
    }

    public static final void Y0(SwapMigrationFragment swapMigrationFragment, Boolean bool) {
        fs1.f(swapMigrationFragment, "this$0");
        fs1.e(bool, "it");
        ck4 ck4Var = null;
        if (bool.booleanValue() && swapMigrationFragment.u0().c0().getValue() != null) {
            SwapMigrationViewModel.a value = swapMigrationFragment.u0().c0().getValue();
            fs1.d(value);
            if (value.b()) {
                ck4 ck4Var2 = swapMigrationFragment.t0;
                if (ck4Var2 == null) {
                    fs1.r("bindingSource");
                    ck4Var2 = null;
                }
                ck4Var2.m.setVisibility(8);
                ck4 ck4Var3 = swapMigrationFragment.u0;
                if (ck4Var3 == null) {
                    fs1.r("bindingDestination");
                    ck4Var3 = null;
                }
                ck4Var3.m.setVisibility(0);
                ck4 ck4Var4 = swapMigrationFragment.t0;
                if (ck4Var4 == null) {
                    fs1.r("bindingSource");
                    ck4Var4 = null;
                }
                ck4Var4.k.setEnabled(true);
                ck4 ck4Var5 = swapMigrationFragment.t0;
                if (ck4Var5 == null) {
                    fs1.r("bindingSource");
                    ck4Var5 = null;
                }
                ck4Var5.j.setEnabled(true);
                ck4 ck4Var6 = swapMigrationFragment.u0;
                if (ck4Var6 == null) {
                    fs1.r("bindingDestination");
                    ck4Var6 = null;
                }
                ck4Var6.k.setEnabled(false);
                ck4 ck4Var7 = swapMigrationFragment.u0;
                if (ck4Var7 == null) {
                    fs1.r("bindingDestination");
                } else {
                    ck4Var = ck4Var7;
                }
                ck4Var.j.setEnabled(false);
            } else {
                ck4 ck4Var8 = swapMigrationFragment.t0;
                if (ck4Var8 == null) {
                    fs1.r("bindingSource");
                    ck4Var8 = null;
                }
                ck4Var8.m.setVisibility(0);
                ck4 ck4Var9 = swapMigrationFragment.u0;
                if (ck4Var9 == null) {
                    fs1.r("bindingDestination");
                    ck4Var9 = null;
                }
                ck4Var9.m.setVisibility(8);
                ck4 ck4Var10 = swapMigrationFragment.t0;
                if (ck4Var10 == null) {
                    fs1.r("bindingSource");
                    ck4Var10 = null;
                }
                ck4Var10.k.setEnabled(false);
                ck4 ck4Var11 = swapMigrationFragment.t0;
                if (ck4Var11 == null) {
                    fs1.r("bindingSource");
                    ck4Var11 = null;
                }
                ck4Var11.j.setEnabled(false);
                ck4 ck4Var12 = swapMigrationFragment.u0;
                if (ck4Var12 == null) {
                    fs1.r("bindingDestination");
                    ck4Var12 = null;
                }
                ck4Var12.k.setEnabled(true);
                ck4 ck4Var13 = swapMigrationFragment.u0;
                if (ck4Var13 == null) {
                    fs1.r("bindingDestination");
                } else {
                    ck4Var = ck4Var13;
                }
                ck4Var.j.setEnabled(false);
            }
            swapMigrationFragment.F0(true);
            return;
        }
        swapMigrationFragment.F0(false);
        ck4 ck4Var14 = swapMigrationFragment.t0;
        if (ck4Var14 == null) {
            fs1.r("bindingSource");
            ck4Var14 = null;
        }
        ck4Var14.m.setVisibility(8);
        ck4 ck4Var15 = swapMigrationFragment.u0;
        if (ck4Var15 == null) {
            fs1.r("bindingDestination");
            ck4Var15 = null;
        }
        ck4Var15.m.setVisibility(8);
        ck4 ck4Var16 = swapMigrationFragment.t0;
        if (ck4Var16 == null) {
            fs1.r("bindingSource");
            ck4Var16 = null;
        }
        ck4Var16.k.setEnabled(true);
        ck4 ck4Var17 = swapMigrationFragment.t0;
        if (ck4Var17 == null) {
            fs1.r("bindingSource");
            ck4Var17 = null;
        }
        ck4Var17.j.setEnabled(true);
        ck4 ck4Var18 = swapMigrationFragment.u0;
        if (ck4Var18 == null) {
            fs1.r("bindingDestination");
            ck4Var18 = null;
        }
        ck4Var18.k.setEnabled(true);
        ck4 ck4Var19 = swapMigrationFragment.u0;
        if (ck4Var19 == null) {
            fs1.r("bindingDestination");
        } else {
            ck4Var = ck4Var19;
        }
        ck4Var.j.setEnabled(false);
    }

    public static final void Z0(SwapMigrationFragment swapMigrationFragment, LoadingState loadingState) {
        fs1.f(swapMigrationFragment, "this$0");
        if (loadingState == null) {
            return;
        }
        LoadingState loadingState2 = LoadingState.Loading;
        swapMigrationFragment.G0(loadingState == loadingState2);
        db1 db1Var = swapMigrationFragment.q0;
        if (db1Var == null) {
            fs1.r("binding");
            db1Var = null;
        }
        db1Var.d.setVisibility(loadingState == LoadingState.Normal ? 0 : 8);
        if (loadingState == loadingState2) {
            String string = swapMigrationFragment.getString(R.string.swap_loading_title);
            fs1.e(string, "getString(R.string.swap_loading_title)");
            String string2 = swapMigrationFragment.getString(R.string.swap_loading_msg);
            fs1.e(string2, "getString(R.string.swap_loading_msg)");
            swapMigrationFragment.l1(false, string, string2);
            return;
        }
        swapMigrationFragment.p0();
    }

    public static final void a1(SwapMigrationFragment swapMigrationFragment, View view) {
        fs1.f(swapMigrationFragment, "this$0");
        Context requireContext = swapMigrationFragment.requireContext();
        fs1.e(requireContext, "requireContext()");
        new AskAuthorizeProvider(requireContext, swapMigrationFragment.j()).a(new SwapMigrationFragment$onViewCreated$25$1(swapMigrationFragment), SwapMigrationFragment$onViewCreated$25$2.INSTANCE);
    }

    public static final void b1(final SwapMigrationFragment swapMigrationFragment, SwapMigrationViewModel.d dVar) {
        fs1.f(swapMigrationFragment, "this$0");
        if (dVar != null) {
            if (dVar.b()) {
                b30 b30Var = b30.a;
                TokenType.a aVar = TokenType.Companion;
                Swap value = swapMigrationFragment.u0().X().getValue();
                fs1.d(value);
                Integer num = value.chainId;
                fs1.e(num, "swapViewModel.sourceLiveData.value!!.chainId");
                String l = fs1.l(b30Var.o(aVar.b(num.intValue())), dVar.a());
                swapMigrationFragment.u0().f0().postValue(null);
                fo0.e(new WeakReference(swapMigrationFragment.requireActivity()), l, new DialogInterface.OnDismissListener() { // from class: yz3
                    @Override // android.content.DialogInterface.OnDismissListener
                    public final void onDismiss(DialogInterface dialogInterface) {
                        SwapMigrationFragment.c1(SwapMigrationFragment.this, dialogInterface);
                    }
                });
                gm1 r0 = swapMigrationFragment.r0();
                if (r0 == null) {
                    return;
                }
                r0.b();
                return;
            }
            Context context = swapMigrationFragment.getContext();
            if (context != null) {
                e30.a0(context, dVar.a());
            }
            swapMigrationFragment.u0().f0().postValue(null);
        }
    }

    public static final void c1(SwapMigrationFragment swapMigrationFragment, DialogInterface dialogInterface) {
        fs1.f(swapMigrationFragment, "this$0");
        gm1 r0 = swapMigrationFragment.r0();
        if (r0 == null) {
            return;
        }
        r0.p();
    }

    public static final void d1(SwapMigrationFragment swapMigrationFragment, Integer num) {
        String obj;
        fs1.f(swapMigrationFragment, "this$0");
        db1 db1Var = swapMigrationFragment.q0;
        db1 db1Var2 = null;
        if (db1Var == null) {
            fs1.r("binding");
            db1Var = null;
        }
        boolean z = true;
        db1Var.b.setVisibility(e30.m0(num != null && num.intValue() == 1));
        if (num == null || num.intValue() != 1) {
            ck4 ck4Var = swapMigrationFragment.t0;
            if (ck4Var == null) {
                fs1.r("bindingSource");
                ck4Var = null;
            }
            Editable text = ck4Var.j.getText();
            String obj2 = (text == null || (obj = text.toString()) == null) ? null : StringsKt__StringsKt.K0(obj).toString();
            if (obj2 != null && obj2.length() != 0) {
                z = false;
            }
            if (z) {
                db1 db1Var3 = swapMigrationFragment.q0;
                if (db1Var3 == null) {
                    fs1.r("binding");
                    db1Var3 = null;
                }
                db1Var3.c.setVisibility(0);
                db1 db1Var4 = swapMigrationFragment.q0;
                if (db1Var4 == null) {
                    fs1.r("binding");
                } else {
                    db1Var2 = db1Var4;
                }
                db1Var2.d.setVisibility(8);
            } else {
                db1 db1Var5 = swapMigrationFragment.q0;
                if (db1Var5 == null) {
                    fs1.r("binding");
                    db1Var5 = null;
                }
                db1Var5.c.setVisibility(8);
                db1 db1Var6 = swapMigrationFragment.q0;
                if (db1Var6 == null) {
                    fs1.r("binding");
                } else {
                    db1Var2 = db1Var6;
                }
                db1Var2.d.setVisibility(0);
            }
        } else {
            db1 db1Var7 = swapMigrationFragment.q0;
            if (db1Var7 == null) {
                fs1.r("binding");
                db1Var7 = null;
            }
            db1Var7.c.setVisibility(8);
            db1 db1Var8 = swapMigrationFragment.q0;
            if (db1Var8 == null) {
                fs1.r("binding");
            } else {
                db1Var2 = db1Var8;
            }
            db1Var2.d.setVisibility(8);
        }
        if (num != null && num.intValue() == 0) {
            pg4.c();
        }
    }

    /* JADX WARN: Code restructure failed: missing block: B:30:0x0058, code lost:
        if (defpackage.fs1.b(r0 == null ? null : r0.getSymbol(), "WETH") != false) goto L20;
     */
    /* JADX WARN: Removed duplicated region for block: B:19:0x0032  */
    /* JADX WARN: Removed duplicated region for block: B:59:0x00bf  */
    /* JADX WARN: Removed duplicated region for block: B:60:0x00c1  */
    /* JADX WARN: Removed duplicated region for block: B:62:0x00c4  */
    /* JADX WARN: Removed duplicated region for block: B:64:0x00c7  */
    /* JADX WARN: Removed duplicated region for block: B:81:0x0112  */
    /* JADX WARN: Removed duplicated region for block: B:84:0x0129  */
    /* JADX WARN: Removed duplicated region for block: B:91:? A[RETURN, SYNTHETIC] */
    /*
        Code decompiled incorrectly, please refer to instructions dump.
        To view partially-correct code enable 'Show inconsistent code' option in preferences
    */
    public static final void e1(net.safemoon.androidwallet.fragments.SwapMigrationFragment r11, java.util.List r12) {
        /*
            Method dump skipped, instructions count: 315
            To view this dump change 'Code comments level' option to 'DEBUG'
        */
        throw new UnsupportedOperationException("Method not decompiled: net.safemoon.androidwallet.fragments.SwapMigrationFragment.e1(net.safemoon.androidwallet.fragments.SwapMigrationFragment, java.util.List):void");
    }

    public static final void f1(SwapMigrationFragment swapMigrationFragment, LoadingState loadingState) {
        fs1.f(swapMigrationFragment, "this$0");
        if (loadingState != null && loadingState == LoadingState.Loading) {
            BestMaxFragment a2 = BestMaxFragment.w0.a();
            FragmentManager childFragmentManager = swapMigrationFragment.getChildFragmentManager();
            fs1.e(childFragmentManager, "childFragmentManager");
            a2.E(childFragmentManager);
        }
    }

    public static final void g1(SwapMigrationFragment swapMigrationFragment, View view) {
        fs1.f(swapMigrationFragment, "this$0");
        swapMigrationFragment.requireActivity().onBackPressed();
    }

    public static final void h1(SwapMigrationFragment swapMigrationFragment, View view) {
        fs1.f(swapMigrationFragment, "this$0");
        FragmentActivity requireActivity = swapMigrationFragment.requireActivity();
        fs1.e(requireActivity, "requireActivity()");
        Object[] objArr = new Object[1];
        Swap value = swapMigrationFragment.u0().K().getValue();
        objArr[0] = value == null ? null : value.name;
        String string = swapMigrationFragment.getString(R.string.warning_cant_swap, objArr);
        fs1.e(string, "getString(R.string.warni…tionLiveData.value?.name)");
        jc0.d(requireActivity, null, string, true, null);
    }

    public static final void i1(SwapMigrationFragment swapMigrationFragment, BigDecimal bigDecimal) {
        fs1.f(swapMigrationFragment, "this$0");
        if (bigDecimal == null) {
            return;
        }
        swapMigrationFragment.u0().U().postValue(null);
        ck4 ck4Var = swapMigrationFragment.t0;
        if (ck4Var == null) {
            fs1.r("bindingSource");
            ck4Var = null;
        }
        ck4Var.j.requestFocus();
        ck4 ck4Var2 = swapMigrationFragment.t0;
        if (ck4Var2 == null) {
            fs1.r("bindingSource");
            ck4Var2 = null;
        }
        ck4Var2.j.setText(e30.h0(bigDecimal, 0, 1, null));
    }

    public static final void j1(SwapMigrationFragment swapMigrationFragment, Double d2) {
        fs1.f(swapMigrationFragment, "this$0");
        db1 db1Var = swapMigrationFragment.q0;
        if (db1Var == null) {
            fs1.r("binding");
            db1Var = null;
        }
        Chip chip = db1Var.i;
        lu3 lu3Var = lu3.a;
        Locale locale = Locale.getDefault();
        String string = swapMigrationFragment.getString(R.string.text_swap_slippage);
        fs1.e(string, "getString(R.string.text_swap_slippage)");
        fs1.e(d2, "it");
        String format = String.format(locale, string, Arrays.copyOf(new Object[]{e30.m(d2.doubleValue())}, 1));
        fs1.e(format, "java.lang.String.format(locale, format, *args)");
        chip.setText(format);
    }

    public static final void k0(SwapMigrationFragment swapMigrationFragment, View view) {
        fs1.f(swapMigrationFragment, "this$0");
        AnchorSwitchWallet anchorSwitchWallet = new AnchorSwitchWallet(swapMigrationFragment.s0(), R.id.navigation_swap);
        Context requireContext = swapMigrationFragment.requireContext();
        fs1.e(requireContext, "requireContext()");
        fs1.e(view, "it");
        db1 db1Var = swapMigrationFragment.q0;
        if (db1Var == null) {
            fs1.r("binding");
            db1Var = null;
        }
        anchorSwitchWallet.h(requireContext, view, db1Var.o);
    }

    public static final boolean l0(SwapMigrationFragment swapMigrationFragment, View view) {
        fs1.f(swapMigrationFragment, "this$0");
        wb wbVar = new wb(swapMigrationFragment.t0());
        Context requireContext = swapMigrationFragment.requireContext();
        fs1.e(requireContext, "requireContext()");
        fs1.e(view, "it");
        db1 db1Var = swapMigrationFragment.q0;
        if (db1Var == null) {
            fs1.r("binding");
            db1Var = null;
        }
        wbVar.g(requireContext, view, db1Var.o);
        return true;
    }

    public static final void n0(ck4 ck4Var, View view) {
        fs1.f(ck4Var, "$this_run");
        ck4Var.j.setText("");
    }

    public static final void o0(ck4 ck4Var, View view) {
        fs1.f(ck4Var, "$this_run");
        ck4Var.j.setText("");
    }

    public static final void w0(ck4 ck4Var, boolean z) {
        fs1.f(ck4Var, "$this_run");
        MaterialButton materialButton = ck4Var.e;
        fs1.e(materialButton, "btnClearText");
        materialButton.setVisibility(z ? 0 : 8);
    }

    public static final void x0(SwapMigrationFragment swapMigrationFragment, Swap swap) {
        fs1.f(swapMigrationFragment, "this$0");
        if (swap == null) {
            return;
        }
        ck4 ck4Var = swapMigrationFragment.u0;
        if (ck4Var == null) {
            fs1.r("bindingDestination");
            ck4Var = null;
        }
        String str = swap.logoURI;
        fs1.e(str, "it.logoURI");
        if ((str.length() > 0) || swap.imageResource > 0) {
            String str2 = swap.logoURI;
            String str3 = swap.symbol;
            fs1.e(str3, "it.symbol");
            com.bumptech.glide.a.u(ck4Var.i).x(kt.f(new i00(str2, str3, Integer.valueOf(swap.imageResource)))).d0(200, 200).a(n73.v0()).I0(ck4Var.i);
        }
        MaterialTextView materialTextView = ck4Var.n;
        materialTextView.setText(((Object) swap.name) + " (" + ((Object) swap.symbol) + ')');
    }

    public static final void y0(SwapMigrationFragment swapMigrationFragment, BigDecimal bigDecimal) {
        fs1.f(swapMigrationFragment, "this$0");
        ck4 ck4Var = swapMigrationFragment.u0;
        if (ck4Var == null) {
            fs1.r("bindingDestination");
            ck4Var = null;
        }
        AppCompatTextView appCompatTextView = ck4Var.g;
        lu3 lu3Var = lu3.a;
        Locale locale = Locale.getDefault();
        String string = swapMigrationFragment.getString(R.string.text_swap_coin_bal);
        fs1.e(string, "getString(R.string.text_swap_coin_bal)");
        Object[] objArr = new Object[1];
        Context context = swapMigrationFragment.getContext();
        objArr[0] = context != null ? e30.y(bigDecimal.doubleValue(), context) : null;
        String format = String.format(locale, string, Arrays.copyOf(objArr, 1));
        fs1.e(format, "java.lang.String.format(locale, format, *args)");
        appCompatTextView.setText(format);
    }

    public static final void z0(SwapMigrationFragment swapMigrationFragment, Double d2) {
        fs1.f(swapMigrationFragment, "this$0");
        ck4 ck4Var = swapMigrationFragment.u0;
        if (ck4Var == null) {
            fs1.r("bindingDestination");
            ck4Var = null;
        }
        double d3 = Utils.DOUBLE_EPSILON;
        if (fs1.a(d2, Utils.DOUBLE_EPSILON)) {
            ck4Var.l.setVisibility(4);
        } else {
            ck4Var.l.setVisibility(0);
        }
        MaterialTextView materialTextView = ck4Var.l;
        Object[] objArr = new Object[2];
        objArr[0] = u21.a.b();
        if (d2 != null) {
            d3 = d2.doubleValue();
        }
        objArr[1] = e30.p(v21.a(d3), 0, null, false, 7, null);
        materialTextView.setText(swapMigrationFragment.getString(R.string.text_swap_current_price, objArr));
    }

    public final void A0() {
        final ck4 ck4Var = this.t0;
        if (ck4Var == null) {
            fs1.r("bindingSource");
            ck4Var = null;
        }
        ck4Var.j.c(new AutofitEdittext.a() { // from class: p04
            @Override // net.safemoon.androidwallet.views.editText.autoSize.AutofitEdittext.a
            public final void a(boolean z) {
                SwapMigrationFragment.B0(ck4.this, z);
            }
        });
        LiveData a2 = cb4.a(u0().X());
        fs1.e(a2, "Transformations.distinctUntilChanged(this)");
        a2.observe(getViewLifecycleOwner(), new tl2() { // from class: sz3
            @Override // defpackage.tl2
            public final void onChanged(Object obj) {
                SwapMigrationFragment.C0(SwapMigrationFragment.this, (Swap) obj);
            }
        });
        u0().W().observe(getViewLifecycleOwner(), new tl2() { // from class: kz3
            @Override // defpackage.tl2
            public final void onChanged(Object obj) {
                SwapMigrationFragment.D0(SwapMigrationFragment.this, (BigDecimal) obj);
            }
        });
        u0().Y().observe(getViewLifecycleOwner(), new tl2() { // from class: s04
            @Override // defpackage.tl2
            public final void onChanged(Object obj) {
                SwapMigrationFragment.E0(SwapMigrationFragment.this, (Double) obj);
            }
        });
    }

    public final void F0(boolean z) {
        boolean z2 = !z;
        db1 db1Var = this.q0;
        ck4 ck4Var = null;
        if (db1Var == null) {
            fs1.r("binding");
            db1Var = null;
        }
        db1Var.l.setEnabled(z2);
        db1 db1Var2 = this.q0;
        if (db1Var2 == null) {
            fs1.r("binding");
            db1Var2 = null;
        }
        db1Var2.i.setEnabled(z2);
        db1 db1Var3 = this.q0;
        if (db1Var3 == null) {
            fs1.r("binding");
            db1Var3 = null;
        }
        db1Var3.j.setEnabled(z2);
        db1 db1Var4 = this.q0;
        if (db1Var4 == null) {
            fs1.r("binding");
            db1Var4 = null;
        }
        db1Var4.h.setEnabled(z2);
        ck4 ck4Var2 = this.t0;
        if (ck4Var2 == null) {
            fs1.r("bindingSource");
            ck4Var2 = null;
        }
        ck4Var2.f.setEnabled(z2);
        ck4 ck4Var3 = this.u0;
        if (ck4Var3 == null) {
            fs1.r("bindingDestination");
        } else {
            ck4Var = ck4Var3;
        }
        ck4Var.f.setEnabled(z2);
    }

    public final void G0(boolean z) {
        boolean z2 = !z;
        F0(z);
        ck4 ck4Var = this.t0;
        ck4 ck4Var2 = null;
        if (ck4Var == null) {
            fs1.r("bindingSource");
            ck4Var = null;
        }
        ck4Var.j.setEnabled(z2);
        ck4Var.a.setEnabled(z2);
        ck4Var.b.setEnabled(z2);
        ck4Var.c.setEnabled(z2);
        ck4Var.d.setEnabled(z2);
        ck4 ck4Var3 = this.u0;
        if (ck4Var3 == null) {
            fs1.r("bindingDestination");
        } else {
            ck4Var2 = ck4Var3;
        }
        ck4Var2.j.setEnabled(z2);
    }

    public final void j0() {
        if (this.q0 == null) {
            fs1.r("binding");
        }
        db1 db1Var = this.q0;
        db1 db1Var2 = null;
        if (db1Var == null) {
            fs1.r("binding");
            db1Var = null;
        }
        MaterialTextView materialTextView = db1Var.r;
        fs1.e(materialTextView, "binding?.tvTotal");
        e30.X(materialTextView, new SwapMigrationFragment$bind$2(this));
        db1 db1Var3 = this.q0;
        if (db1Var3 == null) {
            fs1.r("binding");
            db1Var3 = null;
        }
        MaterialTextView materialTextView2 = db1Var3.q;
        if (materialTextView2 != null) {
            materialTextView2.setOnClickListener(new View.OnClickListener() { // from class: l04
                @Override // android.view.View.OnClickListener
                public final void onClick(View view) {
                    SwapMigrationFragment.k0(SwapMigrationFragment.this, view);
                }
            });
        }
        db1 db1Var4 = this.q0;
        if (db1Var4 == null) {
            fs1.r("binding");
        } else {
            db1Var2 = db1Var4;
        }
        MaterialTextView materialTextView3 = db1Var2.r;
        if (materialTextView3 == null) {
            return;
        }
        materialTextView3.setOnLongClickListener(new View.OnLongClickListener() { // from class: o04
            @Override // android.view.View.OnLongClickListener
            public final boolean onLongClick(View view) {
                boolean l0;
                l0 = SwapMigrationFragment.l0(SwapMigrationFragment.this, view);
                return l0;
            }
        });
    }

    public final void k1(float f) {
        BigDecimal value = u0().W().getValue();
        if (value == null) {
            return;
        }
        ck4 ck4Var = this.t0;
        ck4 ck4Var2 = null;
        if (ck4Var == null) {
            fs1.r("bindingSource");
            ck4Var = null;
        }
        ck4Var.j.requestFocus();
        pg4.f(requireContext());
        Swap value2 = u0().X().getValue();
        fs1.d(value2);
        fs1.e(value2, "swapViewModel.sourceLiveData.value!!");
        Swap swap = value2;
        BigDecimal multiply = value.multiply(new BigDecimal(String.valueOf(f / this.n0)));
        if (!swap.symbol.equals(this.j0) && !swap.symbol.equals(this.k0)) {
            String j = e30.j(multiply.doubleValue());
            fs1.e(j, "balance.toDouble().decimal8()");
            if (e30.K(j) > Utils.DOUBLE_EPSILON) {
                ck4 ck4Var3 = this.t0;
                if (ck4Var3 == null) {
                    fs1.r("bindingSource");
                } else {
                    ck4Var2 = ck4Var3;
                }
                ck4Var2.j.setText(e30.j(multiply.doubleValue()));
                return;
            }
            FragmentActivity activity = getActivity();
            if (activity == null) {
                return;
            }
            String string = getResources().getString(R.string.swap_error_insufficient_balance);
            fs1.e(string, "resources.getString(R.st…ror_insufficient_balance)");
            e30.b0(activity, string);
            return;
        }
        BigDecimal multiply2 = value.subtract(new BigDecimal(this.i0)).multiply(new BigDecimal(String.valueOf(f / this.n0)));
        BigDecimal subtract = value.subtract(new BigDecimal(String.valueOf(0.01d)));
        fs1.e(subtract, "this.subtract(other)");
        if (multiply.compareTo(subtract) > 0) {
            multiply = multiply2;
        }
        String j2 = e30.j(multiply.doubleValue());
        fs1.e(j2, "safetyMargin.toDouble().decimal8()");
        if (e30.K(j2) > Utils.DOUBLE_EPSILON) {
            ck4 ck4Var4 = this.t0;
            if (ck4Var4 == null) {
                fs1.r("bindingSource");
            } else {
                ck4Var2 = ck4Var4;
            }
            ck4Var2.j.setText(e30.j(multiply.doubleValue()));
            return;
        }
        FragmentActivity activity2 = getActivity();
        if (activity2 == null) {
            return;
        }
        String string2 = getResources().getString(R.string.swap_error_insufficient_balance);
        fs1.e(string2, "resources.getString(R.st…ror_insufficient_balance)");
        e30.b0(activity2, string2);
    }

    public final void l1(boolean z, String str, String str2) {
        ProgressLoading progressLoading;
        ProgressLoading progressLoading2 = this.z0;
        boolean z2 = false;
        if (progressLoading2 != null && progressLoading2.isVisible()) {
            z2 = true;
        }
        if (z2 && (progressLoading = this.z0) != null) {
            progressLoading.h();
        }
        ProgressLoading a2 = ProgressLoading.y0.a(z, str, str2);
        this.z0 = a2;
        if (a2 == null) {
            return;
        }
        FragmentManager childFragmentManager = getChildFragmentManager();
        fs1.e(childFragmentManager, "childFragmentManager");
        a2.A(childFragmentManager);
    }

    public final void m0() {
        final ck4 ck4Var = this.t0;
        final ck4 ck4Var2 = null;
        if (ck4Var == null) {
            fs1.r("bindingSource");
            ck4Var = null;
        }
        ck4Var.e.setOnClickListener(new View.OnClickListener() { // from class: zz3
            @Override // android.view.View.OnClickListener
            public final void onClick(View view) {
                SwapMigrationFragment.n0(ck4.this, view);
            }
        });
        ck4 ck4Var3 = this.u0;
        if (ck4Var3 == null) {
            fs1.r("bindingDestination");
        } else {
            ck4Var2 = ck4Var3;
        }
        ck4Var2.e.setOnClickListener(new View.OnClickListener() { // from class: a04
            @Override // android.view.View.OnClickListener
            public final void onClick(View view) {
                SwapMigrationFragment.o0(ck4.this, view);
            }
        });
    }

    @Override // androidx.fragment.app.Fragment
    public View onCreateView(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle) {
        fs1.f(layoutInflater, "inflater");
        return LayoutInflater.from(requireContext()).inflate(R.layout.fragment_swap, viewGroup, false);
    }

    @Override // androidx.fragment.app.Fragment
    public void onDetach() {
        super.onDetach();
        if (u0().R().getValue() == LoadingState.Normal) {
            u0().x();
        }
    }

    @Override // androidx.fragment.app.Fragment
    public void onHiddenChanged(boolean z) {
        super.onHiddenChanged(z);
    }

    @Override // net.safemoon.androidwallet.fragments.common.BaseMainFragment, defpackage.qn, androidx.fragment.app.Fragment
    public void onViewCreated(View view, Bundle bundle) {
        fs1.f(view, "view");
        super.onViewCreated(view, bundle);
        db1 a2 = db1.a(view);
        fs1.e(a2, "bind(view)");
        this.q0 = a2;
        SwapMigrationViewModel u0 = u0();
        rz1 viewLifecycleOwner = getViewLifecycleOwner();
        fs1.e(viewLifecycleOwner, "viewLifecycleOwner");
        u0.o0(viewLifecycleOwner);
        db1 db1Var = null;
        u0().f0().postValue(null);
        u0().c0().postValue(null);
        gb2<Double> D = u0().D();
        Double valueOf = Double.valueOf((double) Utils.DOUBLE_EPSILON);
        D.postValue(valueOf);
        u0().B().postValue(valueOf);
        db1 db1Var2 = this.q0;
        if (db1Var2 == null) {
            fs1.r("binding");
            db1Var2 = null;
        }
        db1Var2.k.setPadding(0, 0, 0, 0);
        db1 db1Var3 = this.q0;
        if (db1Var3 == null) {
            fs1.r("binding");
            db1Var3 = null;
        }
        db1Var3.n.b.setVisibility(8);
        FragmentActivity requireActivity = requireActivity();
        requireActivity.getWindow().addFlags(Integer.MIN_VALUE);
        requireActivity.getWindow().setStatusBarColor(m70.d(requireActivity, 17170445));
        requireActivity.getWindow().setBackgroundDrawableResource(R.drawable.bottom_curve);
        te4 te4Var = te4.a;
        db1 db1Var4 = this.q0;
        if (db1Var4 == null) {
            fs1.r("binding");
            db1Var4 = null;
        }
        ck4 a3 = ck4.a(db1Var4.g.findViewById(R.id.parent));
        fs1.e(a3, "bind(binding.ccSource.findViewById(R.id.parent))");
        this.t0 = a3;
        db1 db1Var5 = this.q0;
        if (db1Var5 == null) {
            fs1.r("binding");
            db1Var5 = null;
        }
        ck4 a4 = ck4.a(db1Var5.f.findViewById(R.id.parent));
        fs1.e(a4, "bind(binding.ccDestinati…indViewById(R.id.parent))");
        this.u0 = a4;
        ck4 ck4Var = this.t0;
        if (ck4Var == null) {
            fs1.r("bindingSource");
            ck4Var = null;
        }
        ck4Var.j.setText("");
        ck4 ck4Var2 = this.u0;
        if (ck4Var2 == null) {
            fs1.r("bindingDestination");
            ck4Var2 = null;
        }
        ck4Var2.j.setText("");
        Wallet.Companion companion = Wallet.Companion;
        String j = bo3.j(getContext(), "SAFEMOON_ACTIVE_WALLET", "");
        fs1.e(j, "getString(context, Share…FEMOON_ACTIVE_WALLET, \"\")");
        Wallet wallet2 = companion.toWallet(j);
        String displayName = wallet2 == null ? null : wallet2.displayName();
        db1 db1Var6 = this.q0;
        if (db1Var6 == null) {
            fs1.r("binding");
            db1Var6 = null;
        }
        db1Var6.q.setText(displayName);
        j0();
        t0().B().observe(getViewLifecycleOwner(), new tl2() { // from class: q04
            @Override // defpackage.tl2
            public final void onChanged(Object obj) {
                SwapMigrationFragment.H0(SwapMigrationFragment.this, (Double) obj);
            }
        });
        u0().b0().observe(getViewLifecycleOwner(), new tl2() { // from class: nz3
            @Override // defpackage.tl2
            public final void onChanged(Object obj) {
                SwapMigrationFragment.e1(SwapMigrationFragment.this, (List) obj);
            }
        });
        db1 db1Var7 = this.q0;
        if (db1Var7 == null) {
            fs1.r("binding");
            db1Var7 = null;
        }
        db1Var7.m.setOnClickListener(new View.OnClickListener() { // from class: c04
            @Override // android.view.View.OnClickListener
            public final void onClick(View view2) {
                SwapMigrationFragment.g1(SwapMigrationFragment.this, view2);
            }
        });
        A0();
        v0();
        db1 db1Var8 = this.q0;
        if (db1Var8 == null) {
            fs1.r("binding");
            db1Var8 = null;
        }
        db1Var8.l.setOnClickListener(new View.OnClickListener() { // from class: b04
            @Override // android.view.View.OnClickListener
            public final void onClick(View view2) {
                SwapMigrationFragment.h1(SwapMigrationFragment.this, view2);
            }
        });
        u0().U().observe(getViewLifecycleOwner(), new tl2() { // from class: lz3
            @Override // defpackage.tl2
            public final void onChanged(Object obj) {
                SwapMigrationFragment.i1(SwapMigrationFragment.this, (BigDecimal) obj);
            }
        });
        db1 db1Var9 = this.q0;
        if (db1Var9 == null) {
            fs1.r("binding");
            db1Var9 = null;
        }
        TextView textView = (TextView) db1Var9.b().findViewById(R.id.textScreenName);
        if (textView != null) {
            e30.V(textView);
        }
        u0().V().postValue(valueOf);
        u0().V().observe(getViewLifecycleOwner(), new tl2() { // from class: u04
            @Override // defpackage.tl2
            public final void onChanged(Object obj) {
                SwapMigrationFragment.j1(SwapMigrationFragment.this, (Double) obj);
            }
        });
        db1 db1Var10 = this.q0;
        if (db1Var10 == null) {
            fs1.r("binding");
            db1Var10 = null;
        }
        db1Var10.i.setVisibility(8);
        db1 db1Var11 = this.q0;
        if (db1Var11 == null) {
            fs1.r("binding");
            db1Var11 = null;
        }
        db1Var11.h.setVisibility(8);
        db1 db1Var12 = this.q0;
        if (db1Var12 == null) {
            fs1.r("binding");
            db1Var12 = null;
        }
        db1Var12.e.setVisibility(8);
        u0().O().observe(getViewLifecycleOwner(), new tl2() { // from class: oz3
            @Override // defpackage.tl2
            public final void onChanged(Object obj) {
                SwapMigrationFragment.I0(SwapMigrationFragment.this, (Gas) obj);
            }
        });
        u0().I().observe(getViewLifecycleOwner(), new tl2() { // from class: v04
            @Override // defpackage.tl2
            public final void onChanged(Object obj) {
                SwapMigrationFragment.J0(SwapMigrationFragment.this, (Integer) obj);
            }
        });
        u0().e0().observe(getViewLifecycleOwner(), new tl2() { // from class: wz3
            @Override // defpackage.tl2
            public final void onChanged(Object obj) {
                SwapMigrationFragment.K0(SwapMigrationFragment.this, (SwapMigrationViewModel.c) obj);
            }
        });
        u0().d0().observe(getViewLifecycleOwner(), new tl2() { // from class: x04
            @Override // defpackage.tl2
            public final void onChanged(Object obj) {
                SwapMigrationFragment.L0(SwapMigrationFragment.this, (String) obj);
            }
        });
        db1 db1Var13 = this.q0;
        if (db1Var13 == null) {
            fs1.r("binding");
            db1Var13 = null;
        }
        db1Var13.j.setOnClickListener(new View.OnClickListener() { // from class: k04
            @Override // android.view.View.OnClickListener
            public final void onClick(View view2) {
                SwapMigrationFragment.M0(SwapMigrationFragment.this, view2);
            }
        });
        db1 db1Var14 = this.q0;
        if (db1Var14 == null) {
            fs1.r("binding");
            db1Var14 = null;
        }
        db1Var14.h.setOnClickListener(new View.OnClickListener() { // from class: n04
            @Override // android.view.View.OnClickListener
            public final void onClick(View view2) {
                SwapMigrationFragment.N0(SwapMigrationFragment.this, view2);
            }
        });
        db1 db1Var15 = this.q0;
        if (db1Var15 == null) {
            fs1.r("binding");
            db1Var15 = null;
        }
        db1Var15.b.setOnClickListener(new View.OnClickListener() { // from class: e04
            @Override // android.view.View.OnClickListener
            public final void onClick(View view2) {
                SwapMigrationFragment.O0(SwapMigrationFragment.this, view2);
            }
        });
        db1 db1Var16 = this.q0;
        if (db1Var16 == null) {
            fs1.r("binding");
            db1Var16 = null;
        }
        db1Var16.e.setOnClickListener(new View.OnClickListener() { // from class: d04
            @Override // android.view.View.OnClickListener
            public final void onClick(View view2) {
                SwapMigrationFragment.P0(SwapMigrationFragment.this, view2);
            }
        });
        u0().E().observe(getViewLifecycleOwner(), new tl2() { // from class: qz3
            @Override // defpackage.tl2
            public final void onChanged(Object obj) {
                SwapMigrationFragment.Q0(SwapMigrationFragment.this, (LoadingState) obj);
            }
        });
        ck4 ck4Var3 = this.t0;
        if (ck4Var3 == null) {
            fs1.r("bindingSource");
            ck4Var3 = null;
        }
        ck4Var3.a.setOnClickListener(new View.OnClickListener() { // from class: h04
            @Override // android.view.View.OnClickListener
            public final void onClick(View view2) {
                SwapMigrationFragment.R0(SwapMigrationFragment.this, view2);
            }
        });
        ck4Var3.b.setOnClickListener(new View.OnClickListener() { // from class: j04
            @Override // android.view.View.OnClickListener
            public final void onClick(View view2) {
                SwapMigrationFragment.S0(SwapMigrationFragment.this, view2);
            }
        });
        ck4Var3.c.setOnClickListener(new View.OnClickListener() { // from class: m04
            @Override // android.view.View.OnClickListener
            public final void onClick(View view2) {
                SwapMigrationFragment.T0(SwapMigrationFragment.this, view2);
            }
        });
        ck4Var3.d.setOnClickListener(new View.OnClickListener() { // from class: i04
            @Override // android.view.View.OnClickListener
            public final void onClick(View view2) {
                SwapMigrationFragment.U0(SwapMigrationFragment.this, view2);
            }
        });
        ck4Var3.j.setFilters(new InputFilter[]{new xq1(valueOf, Double.valueOf(Double.MAX_VALUE))});
        AutofitEdittext autofitEdittext = ck4Var3.j;
        b30 b30Var = b30.a;
        autofitEdittext.setHint(getString(R.string.hint_0, Character.valueOf(b30Var.y())));
        ck4 ck4Var4 = this.u0;
        if (ck4Var4 == null) {
            fs1.r("bindingDestination");
            ck4Var4 = null;
        }
        ck4Var4.a.setVisibility(4);
        ck4Var4.b.setVisibility(4);
        ck4Var4.c.setVisibility(4);
        ck4Var4.d.setVisibility(4);
        ck4Var4.j.setFilters(new InputFilter[]{new xq1(valueOf, Double.valueOf(Double.MAX_VALUE))});
        ck4Var4.j.setHint(getString(R.string.hint_0, Character.valueOf(b30Var.y())));
        u0().c0().observe(getViewLifecycleOwner(), new tl2() { // from class: vz3
            @Override // defpackage.tl2
            public final void onChanged(Object obj) {
                SwapMigrationFragment.V0(SwapMigrationFragment.this, (SwapMigrationViewModel.a) obj);
            }
        });
        u0().D().observe(getViewLifecycleOwner(), new tl2() { // from class: uz3
            @Override // defpackage.tl2
            public final void onChanged(Object obj) {
                SwapMigrationFragment.W0(SwapMigrationFragment.this, (Double) obj);
            }
        });
        u0().B().observe(getViewLifecycleOwner(), new tl2() { // from class: f04
            @Override // defpackage.tl2
            public final void onChanged(Object obj) {
                SwapMigrationFragment.X0(SwapMigrationFragment.this, (Double) obj);
            }
        });
        u0().G().observe(getViewLifecycleOwner(), new tl2() { // from class: jz3
            @Override // defpackage.tl2
            public final void onChanged(Object obj) {
                SwapMigrationFragment.Y0(SwapMigrationFragment.this, (Boolean) obj);
            }
        });
        u0().R().observe(getViewLifecycleOwner(), new tl2() { // from class: rz3
            @Override // defpackage.tl2
            public final void onChanged(Object obj) {
                SwapMigrationFragment.Z0(SwapMigrationFragment.this, (LoadingState) obj);
            }
        });
        m0();
        ck4 ck4Var5 = this.t0;
        if (ck4Var5 == null) {
            fs1.r("bindingSource");
            ck4Var5 = null;
        }
        d dVar = new d(ck4Var5.j);
        ck4 ck4Var6 = this.u0;
        if (ck4Var6 == null) {
            fs1.r("bindingDestination");
            ck4Var6 = null;
        }
        c cVar = new c(ck4Var6.j);
        ck4 ck4Var7 = this.t0;
        if (ck4Var7 == null) {
            fs1.r("bindingSource");
            ck4Var7 = null;
        }
        AutofitEdittext autofitEdittext2 = ck4Var7.j;
        autofitEdittext2.setEnabled(true);
        autofitEdittext2.setFocusableInTouchMode(true);
        autofitEdittext2.setFocusable(true);
        ck4 ck4Var8 = this.t0;
        if (ck4Var8 == null) {
            fs1.r("bindingSource");
            ck4Var8 = null;
        }
        AutofitEdittext autofitEdittext3 = ck4Var8.j;
        ck4 ck4Var9 = this.t0;
        if (ck4Var9 == null) {
            fs1.r("bindingSource");
            ck4Var9 = null;
        }
        AutofitEdittext autofitEdittext4 = ck4Var9.j;
        fs1.e(autofitEdittext4, "bindingSource.newValue");
        autofitEdittext3.addTextChangedListener(new cj2(autofitEdittext4));
        ck4 ck4Var10 = this.t0;
        if (ck4Var10 == null) {
            fs1.r("bindingSource");
            ck4Var10 = null;
        }
        ck4Var10.j.addTextChangedListener(dVar);
        ck4 ck4Var11 = this.u0;
        if (ck4Var11 == null) {
            fs1.r("bindingDestination");
            ck4Var11 = null;
        }
        AutofitEdittext autofitEdittext5 = ck4Var11.j;
        ck4 ck4Var12 = this.u0;
        if (ck4Var12 == null) {
            fs1.r("bindingDestination");
            ck4Var12 = null;
        }
        AutofitEdittext autofitEdittext6 = ck4Var12.j;
        fs1.e(autofitEdittext6, "bindingDestination.newValue");
        autofitEdittext5.addTextChangedListener(new cj2(autofitEdittext6));
        ck4 ck4Var13 = this.u0;
        if (ck4Var13 == null) {
            fs1.r("bindingDestination");
            ck4Var13 = null;
        }
        ck4Var13.j.addTextChangedListener(cVar);
        db1 db1Var17 = this.q0;
        if (db1Var17 == null) {
            fs1.r("binding");
        } else {
            db1Var = db1Var17;
        }
        db1Var.d.setOnClickListener(new View.OnClickListener() { // from class: g04
            @Override // android.view.View.OnClickListener
            public final void onClick(View view2) {
                SwapMigrationFragment.a1(SwapMigrationFragment.this, view2);
            }
        });
        u0().f0().observe(getViewLifecycleOwner(), new tl2() { // from class: xz3
            @Override // defpackage.tl2
            public final void onChanged(Object obj) {
                SwapMigrationFragment.b1(SwapMigrationFragment.this, (SwapMigrationViewModel.d) obj);
            }
        });
        u0().F().observe(getViewLifecycleOwner(), new tl2() { // from class: w04
            @Override // defpackage.tl2
            public final void onChanged(Object obj) {
                SwapMigrationFragment.d1(SwapMigrationFragment.this, (Integer) obj);
            }
        });
        t40.b(u0().N()).observe(getViewLifecycleOwner(), new tl2() { // from class: pz3
            @Override // defpackage.tl2
            public final void onChanged(Object obj) {
                SwapMigrationFragment.f1(SwapMigrationFragment.this, (LoadingState) obj);
            }
        });
    }

    public final void p0() {
        ProgressLoading progressLoading = this.z0;
        if (progressLoading == null) {
            return;
        }
        progressLoading.h();
    }

    public final UserTokenItemDisplayModel q0() {
        return (UserTokenItemDisplayModel) this.w0.getValue();
    }

    public final gm1 r0() {
        return (gm1) this.y0.getValue();
    }

    public final MultiWalletViewModel s0() {
        return (MultiWalletViewModel) this.x0.getValue();
    }

    public final MyTokensListViewModel t0() {
        return (MyTokensListViewModel) this.v0.getValue();
    }

    public final SwapMigrationViewModel u0() {
        return (SwapMigrationViewModel) this.s0.getValue();
    }

    public final void v0() {
        final ck4 ck4Var = this.u0;
        if (ck4Var == null) {
            fs1.r("bindingDestination");
            ck4Var = null;
        }
        ck4Var.j.c(new AutofitEdittext.a() { // from class: r04
            @Override // net.safemoon.androidwallet.views.editText.autoSize.AutofitEdittext.a
            public final void a(boolean z) {
                SwapMigrationFragment.w0(ck4.this, z);
            }
        });
        LiveData a2 = cb4.a(u0().K());
        fs1.e(a2, "Transformations.distinctUntilChanged(this)");
        a2.observe(getViewLifecycleOwner(), new tl2() { // from class: tz3
            @Override // defpackage.tl2
            public final void onChanged(Object obj) {
                SwapMigrationFragment.x0(SwapMigrationFragment.this, (Swap) obj);
            }
        });
        u0().J().observe(getViewLifecycleOwner(), new tl2() { // from class: mz3
            @Override // defpackage.tl2
            public final void onChanged(Object obj) {
                SwapMigrationFragment.y0(SwapMigrationFragment.this, (BigDecimal) obj);
            }
        });
        u0().L().observe(getViewLifecycleOwner(), new tl2() { // from class: t04
            @Override // defpackage.tl2
            public final void onChanged(Object obj) {
                SwapMigrationFragment.z0(SwapMigrationFragment.this, (Double) obj);
            }
        });
    }
}
