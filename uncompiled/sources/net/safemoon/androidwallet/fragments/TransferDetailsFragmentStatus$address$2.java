package net.safemoon.androidwallet.fragments;

import kotlin.jvm.internal.Lambda;

/* compiled from: TransferDetailsFragmentStatus.kt */
/* loaded from: classes2.dex */
public final class TransferDetailsFragmentStatus$address$2 extends Lambda implements rc1<String> {
    public final /* synthetic */ TransferDetailsFragmentStatus this$0;

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public TransferDetailsFragmentStatus$address$2(TransferDetailsFragmentStatus transferDetailsFragmentStatus) {
        super(0);
        this.this$0 = transferDetailsFragmentStatus;
    }

    @Override // defpackage.rc1
    public final String invoke() {
        return bo3.j(this.this$0.getContext(), "SAFEMOON_ADDRESS", "");
    }
}
