package net.safemoon.androidwallet.fragments;

import androidx.lifecycle.l;
import java.lang.ref.WeakReference;
import kotlin.jvm.internal.Lambda;

/* compiled from: AddCustomContractFragment.kt */
/* loaded from: classes2.dex */
public final class AddCustomContractFragment$addNewTokensViewModel$2 extends Lambda implements rc1<l.b> {
    public final /* synthetic */ AddCustomContractFragment this$0;

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public AddCustomContractFragment$addNewTokensViewModel$2(AddCustomContractFragment addCustomContractFragment) {
        super(0);
        this.this$0 = addCustomContractFragment;
    }

    /* JADX WARN: Can't rename method to resolve collision */
    @Override // defpackage.rc1
    public final l.b invoke() {
        return new u9(new WeakReference(this.this$0.requireContext()));
    }
}
