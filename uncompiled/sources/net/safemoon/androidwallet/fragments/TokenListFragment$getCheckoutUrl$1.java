package net.safemoon.androidwallet.fragments;

import android.net.Uri;
import defpackage.rc0;
import kotlin.jvm.internal.Lambda;
import net.safemoon.androidwallet.model.wyre.CheckoutPage;

/* compiled from: TokenListFragment.kt */
/* loaded from: classes2.dex */
public final class TokenListFragment$getCheckoutUrl$1 extends Lambda implements tc1<CheckoutPage, te4> {
    public final /* synthetic */ TokenListFragment this$0;

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public TokenListFragment$getCheckoutUrl$1(TokenListFragment tokenListFragment) {
        super(1);
        this.this$0 = tokenListFragment;
    }

    @Override // defpackage.tc1
    public /* bridge */ /* synthetic */ te4 invoke(CheckoutPage checkoutPage) {
        invoke2(checkoutPage);
        return te4.a;
    }

    /* renamed from: invoke  reason: avoid collision after fix types in other method */
    public final void invoke2(CheckoutPage checkoutPage) {
        this.this$0.m0 = false;
        if ((checkoutPage == null ? null : checkoutPage.getUrl()) != null) {
            rc0 a = new rc0.a().a();
            fs1.e(a, "builder.build()");
            a.a(this.this$0.requireContext(), Uri.parse(checkoutPage.getUrl()));
            pg4.b(this.this$0.requireActivity(), Boolean.TRUE);
            this.this$0.l0 = true;
        }
    }
}
