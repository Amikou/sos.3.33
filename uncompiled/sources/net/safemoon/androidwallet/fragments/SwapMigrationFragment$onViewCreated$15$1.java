package net.safemoon.androidwallet.fragments;

import java.util.Iterator;
import java.util.List;
import kotlin.jvm.internal.Lambda;
import net.safemoon.androidwallet.model.swap.Swap;

/* compiled from: SwapMigrationFragment.kt */
/* loaded from: classes2.dex */
public final class SwapMigrationFragment$onViewCreated$15$1 extends Lambda implements rc1<te4> {
    public final /* synthetic */ SwapMigrationFragment this$0;

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public SwapMigrationFragment$onViewCreated$15$1(SwapMigrationFragment swapMigrationFragment) {
        super(0);
        this.this$0 = swapMigrationFragment;
    }

    @Override // defpackage.rc1
    public /* bridge */ /* synthetic */ te4 invoke() {
        invoke2();
        return te4.a;
    }

    /* JADX WARN: Multi-variable type inference failed */
    @Override // defpackage.rc1
    /* renamed from: invoke  reason: avoid collision after fix types in other method */
    public final void invoke2() {
        Object obj;
        Swap swap;
        String str;
        String str2;
        List<Swap> value = this.this$0.u0().b0().getValue();
        Swap swap2 = null;
        if (value == null) {
            swap = null;
        } else {
            SwapMigrationFragment swapMigrationFragment = this.this$0;
            Iterator<T> it = value.iterator();
            while (true) {
                if (!it.hasNext()) {
                    obj = null;
                    break;
                }
                obj = it.next();
                String str3 = ((Swap) obj).symbol;
                str = swapMigrationFragment.m0;
                if (fs1.b(str3, str)) {
                    break;
                }
            }
            swap = (Swap) obj;
        }
        List<Swap> value2 = this.this$0.u0().b0().getValue();
        if (value2 != null) {
            SwapMigrationFragment swapMigrationFragment2 = this.this$0;
            Iterator<T> it2 = value2.iterator();
            while (true) {
                if (!it2.hasNext()) {
                    break;
                }
                Object next = it2.next();
                String str4 = ((Swap) next).symbol;
                str2 = swapMigrationFragment2.l0;
                if (fs1.b(str4, str2)) {
                    swap2 = next;
                    break;
                }
            }
            swap2 = swap2;
        }
        if (swap != null) {
            this.this$0.u0().X().setValue(swap);
        }
        if (swap2 != null) {
            this.this$0.u0().K().setValue(swap2);
        }
        this.this$0.u0().x();
    }
}
