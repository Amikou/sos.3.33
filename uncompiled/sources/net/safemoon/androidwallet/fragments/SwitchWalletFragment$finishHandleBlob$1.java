package net.safemoon.androidwallet.fragments;

import android.content.Context;
import androidx.fragment.app.FragmentActivity;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import kotlin.jvm.internal.Lambda;
import net.safemoon.androidwallet.model.wallets.Wallet;
import net.safemoon.androidwallet.viewmodels.MultiWalletViewModel;

/* compiled from: SwitchWalletFragment.kt */
/* loaded from: classes2.dex */
public final class SwitchWalletFragment$finishHandleBlob$1 extends Lambda implements tc1<List<? extends Wallet>, te4> {
    public final /* synthetic */ SwitchWalletFragment this$0;

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public SwitchWalletFragment$finishHandleBlob$1(SwitchWalletFragment switchWalletFragment) {
        super(1);
        this.this$0 = switchWalletFragment;
    }

    @Override // defpackage.tc1
    public /* bridge */ /* synthetic */ te4 invoke(List<? extends Wallet> list) {
        invoke2((List<Wallet>) list);
        return te4.a;
    }

    /* renamed from: invoke  reason: avoid collision after fix types in other method */
    public final void invoke2(List<Wallet> list) {
        Object obj;
        List list2;
        MultiWalletViewModel f0;
        Map map;
        Map map2;
        MultiWalletViewModel f02;
        List list3;
        Map map3;
        MultiWalletViewModel f03;
        Map map4;
        MultiWalletViewModel f04;
        Map map5;
        MultiWalletViewModel f05;
        fs1.f(list, "list");
        Iterator<Wallet> it = list.iterator();
        boolean z = true;
        while (true) {
            obj = null;
            if (!it.hasNext()) {
                break;
            }
            Wallet next = it.next();
            if (!next.isPrimaryWallet()) {
                list3 = this.this$0.o0;
                if (list3.contains(next.getAddress())) {
                    map3 = this.this$0.p0;
                    if (map3.containsKey(next.getAddress())) {
                        String name = next.getName();
                        map4 = this.this$0.p0;
                        if (!fs1.b(name, map4.get(next.getAddress()))) {
                            f04 = this.this$0.f0();
                            map5 = this.this$0.p0;
                            Object obj2 = map5.get(next.getAddress());
                            fs1.d(obj2);
                            f04.D(next, (String) obj2, 1, null);
                        }
                    }
                    f03 = this.this$0.f0();
                    f03.C(next, 1, null);
                } else {
                    f05 = this.this$0.f0();
                    f05.C(next, 0, null);
                    z = false;
                }
            } else if (!next.isLinked()) {
                f02 = this.this$0.f0();
                f02.C(next, 1, null);
            }
        }
        do3 do3Var = do3.a;
        Context requireContext = this.this$0.requireContext();
        fs1.e(requireContext, "requireContext()");
        do3Var.h(requireContext, z);
        FragmentActivity activity = this.this$0.getActivity();
        Wallet c = activity == null ? null : e30.c(activity);
        Iterator<T> it2 = list.iterator();
        while (true) {
            if (!it2.hasNext()) {
                break;
            }
            Object next2 = it2.next();
            if (fs1.b(((Wallet) next2).getAddress(), c == null ? null : c.getAddress())) {
                obj = next2;
                break;
            }
        }
        Wallet wallet2 = (Wallet) obj;
        if (wallet2 == null) {
            return;
        }
        SwitchWalletFragment switchWalletFragment = this.this$0;
        list2 = switchWalletFragment.o0;
        if (list2.contains(wallet2.getAddress())) {
            wallet2.setLinkedState(1);
            map = switchWalletFragment.p0;
            if (map.containsKey(wallet2.getAddress())) {
                map2 = switchWalletFragment.p0;
                Object obj3 = map2.get(wallet2.getAddress());
                fs1.d(obj3);
                wallet2.setName((String) obj3);
            }
        }
        f0 = switchWalletFragment.f0();
        f0.A(wallet2);
    }
}
