package net.safemoon.androidwallet.fragments;

import java.io.Serializable;
import java.util.Objects;
import kotlin.jvm.internal.Lambda;
import net.safemoon.androidwallet.model.transaction.history.Result;

/* compiled from: TransferDetailsFragmentStatus.kt */
/* loaded from: classes2.dex */
public final class TransferDetailsFragmentStatus$requestTransaction$2 extends Lambda implements rc1<Result> {
    public final /* synthetic */ TransferDetailsFragmentStatus this$0;

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public TransferDetailsFragmentStatus$requestTransaction$2(TransferDetailsFragmentStatus transferDetailsFragmentStatus) {
        super(0);
        this.this$0 = transferDetailsFragmentStatus;
    }

    @Override // defpackage.rc1
    public final Result invoke() {
        Serializable serializable = this.this$0.requireArguments().getSerializable("result");
        Objects.requireNonNull(serializable, "null cannot be cast to non-null type net.safemoon.androidwallet.model.transaction.history.Result");
        return (Result) serializable;
    }
}
