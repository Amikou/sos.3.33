package net.safemoon.androidwallet.fragments;

import android.widget.TextView;
import com.github.mikephil.charting.components.XAxis;
import com.github.mikephil.charting.data.Entry;
import com.github.mikephil.charting.data.LineData;
import com.github.mikephil.charting.data.LineDataSet;
import com.github.mikephil.charting.utils.Utils;
import java.util.ArrayList;
import java.util.List;
import kotlin.coroutines.jvm.internal.SuspendLambda;
import net.safemoon.androidwallet.R;
import net.safemoon.androidwallet.fragments.ReflectionsAdvanceFragment;
import net.safemoon.androidwallet.model.reflections.RoomReflectionsDataAndToken;
import net.safemoon.androidwallet.model.reflections.RoomReflectionsToken;
import net.safemoon.androidwallet.viewmodels.ReflectionTrackerViewModel;
import net.safemoon.androidwallet.views.TouchControlLineChart;

/* compiled from: ReflectionsAdvanceFragment.kt */
@kotlin.coroutines.jvm.internal.a(c = "net.safemoon.androidwallet.fragments.ReflectionsAdvanceFragment$bindChart$1", f = "ReflectionsAdvanceFragment.kt", l = {}, m = "invokeSuspend")
/* loaded from: classes2.dex */
public final class ReflectionsAdvanceFragment$bindChart$1 extends SuspendLambda implements hd1<c90, q70<? super te4>, Object> {
    public final /* synthetic */ List<RoomReflectionsDataAndToken> $list;
    public int label;
    public final /* synthetic */ ReflectionsAdvanceFragment this$0;

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public ReflectionsAdvanceFragment$bindChart$1(ReflectionsAdvanceFragment reflectionsAdvanceFragment, List<RoomReflectionsDataAndToken> list, q70<? super ReflectionsAdvanceFragment$bindChart$1> q70Var) {
        super(2, q70Var);
        this.this$0 = reflectionsAdvanceFragment;
        this.$list = list;
    }

    @Override // kotlin.coroutines.jvm.internal.BaseContinuationImpl
    public final q70<te4> create(Object obj, q70<?> q70Var) {
        return new ReflectionsAdvanceFragment$bindChart$1(this.this$0, this.$list, q70Var);
    }

    @Override // defpackage.hd1
    public final Object invoke(c90 c90Var, q70<? super te4> q70Var) {
        return ((ReflectionsAdvanceFragment$bindChart$1) create(c90Var, q70Var)).invokeSuspend(te4.a);
    }

    @Override // kotlin.coroutines.jvm.internal.BaseContinuationImpl
    public final Object invokeSuspend(Object obj) {
        sa1 J;
        String str;
        float f;
        sa1 J2;
        TouchControlLineChart touchControlLineChart;
        ReflectionTrackerViewModel L;
        Float diffBalance;
        xk1 xk1Var;
        RoomReflectionsDataAndToken roomReflectionsDataAndToken;
        RoomReflectionsToken token;
        Double priceUsd;
        Float diffBalance2;
        gs1.d();
        if (this.label == 0) {
            o83.b(obj);
            J = this.this$0.J();
            if (J != null && (xk1Var = J.h) != null) {
                List<RoomReflectionsDataAndToken> list = this.$list;
                xk1Var.b.setText(R.string.mr_seg_time_total);
                xk1Var.e.setVisibility(4);
                if (!list.isEmpty()) {
                    roomReflectionsDataAndToken = new RoomReflectionsDataAndToken(((RoomReflectionsDataAndToken) j20.L(list)).getData(), ((RoomReflectionsDataAndToken) j20.L(list)).getToken());
                    float f2 = 0.0f;
                    for (RoomReflectionsDataAndToken roomReflectionsDataAndToken2 : list) {
                        Float diffBalance3 = roomReflectionsDataAndToken2.getDiffBalance();
                        f2 += diffBalance3 == null ? 0.0f : diffBalance3.floatValue();
                    }
                    roomReflectionsDataAndToken.setDiffBalance(hr.c(f2));
                } else {
                    roomReflectionsDataAndToken = null;
                }
                double doubleValue = (roomReflectionsDataAndToken == null || (token = roomReflectionsDataAndToken.getToken()) == null || (priceUsd = token.getPriceUsd()) == null) ? 0.0d : priceUsd.doubleValue();
                Double b = (roomReflectionsDataAndToken == null || (diffBalance2 = roomReflectionsDataAndToken.getDiffBalance()) == null) ? null : hr.b(diffBalance2.floatValue());
                Double b2 = b == null ? null : hr.b(b.doubleValue() * doubleValue);
                if (b != null) {
                    TextView textView = xk1Var.d;
                    fs1.e(textView, "tvTokenNativeBalance");
                    e30.R(textView, b.doubleValue());
                }
                if (doubleValue > Utils.DOUBLE_EPSILON && b2 != null) {
                    TextView textView2 = xk1Var.c;
                    fs1.e(textView2, "tvTokenBalance");
                    e30.N(textView2, b2.doubleValue(), true);
                } else {
                    xk1Var.c.setText("");
                }
            }
            ArrayList arrayList = new ArrayList();
            int size = this.$list.size() - 1;
            if (size >= 0) {
                while (true) {
                    int i = size - 1;
                    try {
                        RoomReflectionsDataAndToken roomReflectionsDataAndToken3 = this.$list.get(size);
                        Double priceUsd2 = roomReflectionsDataAndToken3.getToken().getPriceUsd();
                        if (priceUsd2 != null) {
                            priceUsd2.doubleValue();
                        }
                        Double b3 = roomReflectionsDataAndToken3.getDiffBalance() == null ? null : hr.b(diffBalance.floatValue());
                        if (this.$list.size() == 1) {
                            arrayList.add(new Entry((float) roomReflectionsDataAndToken3.getData().getStartTime(), Utils.FLOAT_EPSILON));
                            float timeStamp = (float) roomReflectionsDataAndToken3.getData().getTimeStamp();
                            Float c = b3 == null ? null : hr.c((float) b3.doubleValue());
                            fs1.d(c);
                            arrayList.add(new Entry(timeStamp, c.floatValue()));
                        } else {
                            float timeStamp2 = (float) roomReflectionsDataAndToken3.getData().getTimeStamp();
                            Float c2 = b3 == null ? null : hr.c((float) b3.doubleValue());
                            fs1.d(c2);
                            arrayList.add(new Entry(timeStamp2, c2.floatValue()));
                        }
                    } catch (Exception unused) {
                    }
                    if (i < 0) {
                        break;
                    }
                    size = i;
                }
            }
            str = this.this$0.j0;
            LineDataSet lineDataSet = new LineDataSet(arrayList, str);
            ReflectionsAdvanceFragment reflectionsAdvanceFragment = this.this$0;
            f = reflectionsAdvanceFragment.i0;
            lineDataSet.setLineWidth(f);
            lineDataSet.setColor(m70.d(reflectionsAdvanceFragment.requireContext(), R.color.white));
            lineDataSet.setHighLightColor(m70.d(reflectionsAdvanceFragment.requireContext(), R.color.white));
            LineData lineData = new LineData(lineDataSet);
            lineData.setValueTextColor(m70.d(this.this$0.requireContext(), R.color.white));
            lineData.setDrawValues(false);
            lineData.setHighlightEnabled(true);
            J2 = this.this$0.J();
            if (J2 != null && (touchControlLineChart = J2.c) != null) {
                ReflectionsAdvanceFragment reflectionsAdvanceFragment2 = this.this$0;
                touchControlLineChart.clear();
                touchControlLineChart.setClipValuesToContent(true);
                touchControlLineChart.setData(lineData);
                touchControlLineChart.getDescription().setEnabled(false);
                touchControlLineChart.getXAxis().setEnabled(true);
                XAxis xAxis = touchControlLineChart.getXAxis();
                L = reflectionsAdvanceFragment2.L();
                xAxis.setValueFormatter(new ReflectionsAdvanceFragment.a(reflectionsAdvanceFragment2, L.n()));
                touchControlLineChart.getXAxis().setAxisLineColor(m70.d(reflectionsAdvanceFragment2.requireContext(), R.color.white));
                touchControlLineChart.getXAxis().setTextColor(m70.d(reflectionsAdvanceFragment2.requireContext(), R.color.white));
                touchControlLineChart.getAxisLeft().setEnabled(false);
                touchControlLineChart.getAxisLeft().setDrawLabels(false);
                touchControlLineChart.getAxisRight().setDrawLabels(false);
                touchControlLineChart.getAxisRight().setEnabled(false);
                touchControlLineChart.notifyDataSetChanged();
            }
            return te4.a;
        }
        throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
    }
}
