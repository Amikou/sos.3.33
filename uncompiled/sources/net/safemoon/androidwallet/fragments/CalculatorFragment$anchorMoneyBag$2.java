package net.safemoon.androidwallet.fragments;

import kotlin.jvm.internal.Lambda;
import net.safemoon.androidwallet.viewmodels.CalculatorViewModel;

/* compiled from: CalculatorFragment.kt */
/* loaded from: classes2.dex */
public final class CalculatorFragment$anchorMoneyBag$2 extends Lambda implements rc1<lc> {
    public final /* synthetic */ CalculatorFragment this$0;

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public CalculatorFragment$anchorMoneyBag$2(CalculatorFragment calculatorFragment) {
        super(0);
        this.this$0 = calculatorFragment;
    }

    @Override // defpackage.rc1
    public final lc invoke() {
        CalculatorViewModel a0;
        a0 = this.this$0.a0();
        return new lc(a0);
    }
}
