package net.safemoon.androidwallet.fragments;

import androidx.fragment.app.Fragment;
import kotlin.jvm.internal.Lambda;

/* compiled from: FragmentViewModelLazy.kt */
/* loaded from: classes2.dex */
public final class AddCustomReflectionContractFragment$special$$inlined$viewModels$default$1 extends Lambda implements rc1<Fragment> {
    public final /* synthetic */ Fragment $this_viewModels;

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public AddCustomReflectionContractFragment$special$$inlined$viewModels$default$1(Fragment fragment) {
        super(0);
        this.$this_viewModels = fragment;
    }

    /* JADX WARN: Can't rename method to resolve collision */
    @Override // defpackage.rc1
    public final Fragment invoke() {
        return this.$this_viewModels;
    }
}
