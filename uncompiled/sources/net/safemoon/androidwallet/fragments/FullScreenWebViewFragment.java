package net.safemoon.androidwallet.fragments;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebResourceError;
import android.webkit.WebResourceRequest;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import androidx.constraintlayout.widget.ConstraintLayout;
import net.safemoon.androidwallet.R;
import net.safemoon.androidwallet.fragments.FullScreenWebViewFragment;

/* compiled from: FullScreenWebViewFragment.kt */
/* loaded from: classes2.dex */
public final class FullScreenWebViewFragment extends qn {
    public static final a j0 = new a(null);
    public ea1 f0;
    public int g0;
    public String h0 = "";
    public boolean i0;

    /* compiled from: FullScreenWebViewFragment.kt */
    /* loaded from: classes2.dex */
    public static final class a {
        public a() {
        }

        public /* synthetic */ a(qi0 qi0Var) {
            this();
        }

        public final FullScreenWebViewFragment a(String str, int i, boolean z) {
            fs1.f(str, "url");
            FullScreenWebViewFragment fullScreenWebViewFragment = new FullScreenWebViewFragment();
            Bundle bundle = new Bundle();
            bundle.putInt("titleResId", i);
            bundle.putString("url", str);
            bundle.putBoolean("withBottomPadding", z);
            fullScreenWebViewFragment.setArguments(bundle);
            return fullScreenWebViewFragment;
        }
    }

    /* compiled from: FullScreenWebViewFragment.kt */
    /* loaded from: classes2.dex */
    public static final class b extends WebViewClient {
        public b() {
        }

        @Override // android.webkit.WebViewClient
        public void onPageCommitVisible(WebView webView, String str) {
            super.onPageCommitVisible(webView, str);
            FullScreenWebViewFragment.this.i0 = true;
            super.onPageFinished(webView, str);
            ea1 ea1Var = FullScreenWebViewFragment.this.f0;
            ProgressBar progressBar = ea1Var == null ? null : ea1Var.d;
            if (progressBar != null) {
                progressBar.setVisibility(8);
            }
            ea1 ea1Var2 = FullScreenWebViewFragment.this.f0;
            LinearLayout linearLayout = ea1Var2 == null ? null : ea1Var2.c;
            if (linearLayout != null) {
                linearLayout.setVisibility(8);
            }
            ea1 ea1Var3 = FullScreenWebViewFragment.this.f0;
            WebView webView2 = ea1Var3 != null ? ea1Var3.f : null;
            if (webView2 == null) {
                return;
            }
            webView2.setVisibility(0);
        }

        @Override // android.webkit.WebViewClient
        public void onReceivedError(WebView webView, WebResourceRequest webResourceRequest, WebResourceError webResourceError) {
            super.onReceivedError(webView, webResourceRequest, webResourceError);
            if (FullScreenWebViewFragment.this.i0) {
                return;
            }
            ea1 ea1Var = FullScreenWebViewFragment.this.f0;
            LinearLayout linearLayout = ea1Var == null ? null : ea1Var.c;
            if (linearLayout != null) {
                linearLayout.setVisibility(0);
            }
            ea1 ea1Var2 = FullScreenWebViewFragment.this.f0;
            ProgressBar progressBar = ea1Var2 == null ? null : ea1Var2.d;
            if (progressBar != null) {
                progressBar.setVisibility(8);
            }
            ea1 ea1Var3 = FullScreenWebViewFragment.this.f0;
            WebView webView2 = ea1Var3 != null ? ea1Var3.f : null;
            if (webView2 == null) {
                return;
            }
            webView2.setVisibility(8);
        }

        @Override // android.webkit.WebViewClient
        public boolean shouldOverrideUrlLoading(WebView webView, WebResourceRequest webResourceRequest) {
            return true;
        }
    }

    public static final void p(FullScreenWebViewFragment fullScreenWebViewFragment, View view) {
        fs1.f(fullScreenWebViewFragment, "this$0");
        fullScreenWebViewFragment.f();
    }

    public static final void r(FullScreenWebViewFragment fullScreenWebViewFragment, View view) {
        fs1.f(fullScreenWebViewFragment, "this$0");
        ea1 ea1Var = fullScreenWebViewFragment.f0;
        LinearLayout linearLayout = ea1Var == null ? null : ea1Var.c;
        if (linearLayout != null) {
            linearLayout.setVisibility(8);
        }
        ea1 ea1Var2 = fullScreenWebViewFragment.f0;
        ProgressBar progressBar = ea1Var2 != null ? ea1Var2.d : null;
        if (progressBar != null) {
            progressBar.setVisibility(0);
        }
        fullScreenWebViewFragment.n();
    }

    public final void n() {
        WebView webView;
        WebView webView2;
        ea1 ea1Var = this.f0;
        if (ea1Var != null && (webView2 = ea1Var.f) != null) {
            webView2.loadUrl(this.h0);
        }
        try {
            ea1 ea1Var2 = this.f0;
            if (ea1Var2 != null && (webView = ea1Var2.f) != null) {
                webView.loadUrl(this.h0);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public final void o() {
        ea1 ea1Var = this.f0;
        fs1.d(ea1Var);
        ea1Var.e.a.setOnClickListener(new View.OnClickListener() { // from class: pc1
            @Override // android.view.View.OnClickListener
            public final void onClick(View view) {
                FullScreenWebViewFragment.p(FullScreenWebViewFragment.this, view);
            }
        });
        ea1 ea1Var2 = this.f0;
        fs1.d(ea1Var2);
        ea1Var2.e.c.setText(getResources().getText(this.g0));
    }

    @Override // androidx.fragment.app.Fragment
    public View onCreateView(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle) {
        fs1.f(layoutInflater, "inflater");
        this.f0 = ea1.a(layoutInflater.inflate(R.layout.fragment_full_screen_web_view, viewGroup, false));
        if (getArguments() != null && requireArguments().get("url") != null && requireArguments().get("titleResId") != null && requireArguments().get("withBottomPadding") != null) {
            this.g0 = requireArguments().getInt("titleResId");
            String string = requireArguments().getString("url");
            fs1.d(string);
            fs1.e(string, "requireArguments().getString(BUNDLE_KEY_URL)!!");
            this.h0 = string;
            requireArguments().getBoolean("withBottomPadding");
            ea1 ea1Var = this.f0;
            fs1.d(ea1Var);
            ConstraintLayout b2 = ea1Var.b();
            fs1.e(b2, "binding!!.root");
            return b2;
        }
        throw new NullPointerException("Arguments shouldn't be null");
    }

    @Override // defpackage.qn, androidx.fragment.app.Fragment
    public void onViewCreated(View view, Bundle bundle) {
        fs1.f(view, "view");
        try {
            super.onViewCreated(view, bundle);
        } catch (Exception e) {
            e.printStackTrace();
        }
        o();
        q();
        n();
    }

    public final void q() {
        ea1 ea1Var = this.f0;
        fs1.d(ea1Var);
        WebView webView = ea1Var.f;
        webView.getSettings().setJavaScriptEnabled(true);
        webView.getSettings().setSupportZoom(false);
        webView.getSettings().setJavaScriptCanOpenWindowsAutomatically(false);
        webView.getSettings().setLoadWithOverviewMode(true);
        webView.getSettings().setUseWideViewPort(true);
        webView.setWebViewClient(new b());
        ea1 ea1Var2 = this.f0;
        fs1.d(ea1Var2);
        ea1Var2.b.setOnClickListener(new View.OnClickListener() { // from class: qc1
            @Override // android.view.View.OnClickListener
            public final void onClick(View view) {
                FullScreenWebViewFragment.r(FullScreenWebViewFragment.this, view);
            }
        });
    }
}
