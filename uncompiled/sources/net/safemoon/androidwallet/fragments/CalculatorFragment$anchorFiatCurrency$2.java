package net.safemoon.androidwallet.fragments;

import android.content.Context;
import android.widget.ScrollView;
import kotlin.jvm.internal.Lambda;
import net.safemoon.androidwallet.fragments.CalculatorFragment$defaultCurrencyAdapter$2;
import net.safemoon.androidwallet.viewmodels.SelectFiatViewModel;

/* compiled from: CalculatorFragment.kt */
/* loaded from: classes2.dex */
public final class CalculatorFragment$anchorFiatCurrency$2 extends Lambda implements rc1<a> {
    public final /* synthetic */ CalculatorFragment this$0;

    /* compiled from: CalculatorFragment.kt */
    /* renamed from: net.safemoon.androidwallet.fragments.CalculatorFragment$anchorFiatCurrency$2$2  reason: invalid class name */
    /* loaded from: classes2.dex */
    public static final class AnonymousClass2 extends Lambda implements rc1<te4> {
        public static final AnonymousClass2 INSTANCE = new AnonymousClass2();

        public AnonymousClass2() {
            super(0);
        }

        @Override // defpackage.rc1
        public /* bridge */ /* synthetic */ te4 invoke() {
            invoke2();
            return te4.a;
        }

        @Override // defpackage.rc1
        /* renamed from: invoke  reason: avoid collision after fix types in other method */
        public final void invoke2() {
        }
    }

    /* compiled from: CalculatorFragment.kt */
    /* loaded from: classes2.dex */
    public static final class a extends ec {
        public final /* synthetic */ CalculatorFragment f;

        /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
        public a(CalculatorFragment calculatorFragment, Context context, CalculatorFragment$defaultCurrencyAdapter$2.a aVar, ScrollView scrollView, AnonymousClass2 anonymousClass2) {
            super(context, aVar, scrollView, anonymousClass2);
            this.f = calculatorFragment;
            fs1.e(context, "requireContext()");
        }

        @Override // defpackage.ec
        public void h(String str) {
            CalculatorFragment$defaultCurrencyAdapter$2.a b0;
            SelectFiatViewModel d0;
            if (str == null) {
                return;
            }
            CalculatorFragment calculatorFragment = this.f;
            b0 = calculatorFragment.b0();
            d0 = calculatorFragment.d0();
            b0.d(d0.k(str));
        }
    }

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public CalculatorFragment$anchorFiatCurrency$2(CalculatorFragment calculatorFragment) {
        super(0);
        this.this$0 = calculatorFragment;
    }

    @Override // defpackage.rc1
    public final a invoke() {
        CalculatorFragment$defaultCurrencyAdapter$2.a b0;
        r91 r91Var;
        Context requireContext = this.this$0.requireContext();
        b0 = this.this$0.b0();
        r91Var = this.this$0.m0;
        return new a(this.this$0, requireContext, b0, r91Var == null ? null : r91Var.b(), AnonymousClass2.INSTANCE);
    }
}
