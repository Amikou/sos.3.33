package net.safemoon.androidwallet.fragments;

import android.app.Activity;
import android.content.Context;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.os.Bundle;
import android.util.DisplayMetrics;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RadioButton;
import android.widget.TextView;
import androidx.fragment.app.FragmentActivity;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentViewModelLazyKt;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;
import com.creageek.segmentedbutton.SegmentedButton;
import com.google.android.material.button.MaterialButton;
import com.google.android.material.textview.MaterialTextView;
import defpackage.k00;
import defpackage.lt2;
import defpackage.qn4;
import defpackage.wm4;
import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import net.safemoon.androidwallet.R;
import net.safemoon.androidwallet.common.TokenType;
import net.safemoon.androidwallet.dialogs.AnchorSwitchWallet;
import net.safemoon.androidwallet.domain.listener.dalog.OnSelectTokenTypeClickListener;
import net.safemoon.androidwallet.fragments.WalletFragment;
import net.safemoon.androidwallet.fragments.common.BaseMainFragment;
import net.safemoon.androidwallet.model.Coin;
import net.safemoon.androidwallet.model.fiat.gson.Fiat;
import net.safemoon.androidwallet.model.notificationHistory.NotificationHistory;
import net.safemoon.androidwallet.model.notificationHistory.NotificationHistoryData;
import net.safemoon.androidwallet.model.notificationHistory.NotificationHistoryResult;
import net.safemoon.androidwallet.model.wallets.Wallet;
import net.safemoon.androidwallet.viewmodels.HomeViewModel;
import net.safemoon.androidwallet.viewmodels.MultiWalletViewModel;
import net.safemoon.androidwallet.viewmodels.MyTokensListViewModel;
import net.safemoon.androidwallet.viewmodels.SelectFiatViewModel;

/* compiled from: WalletFragment.kt */
/* loaded from: classes2.dex */
public final class WalletFragment extends BaseMainFragment implements View.OnClickListener {
    public pb1 i0;
    public k00 o0;
    public qn4 p0;
    public OnSelectTokenTypeClickListener q0;
    public final sy1 j0 = FragmentViewModelLazyKt.a(this, d53.b(HomeViewModel.class), new WalletFragment$special$$inlined$activityViewModels$default$1(this), new WalletFragment$special$$inlined$activityViewModels$default$2(this));
    public final sy1 k0 = FragmentViewModelLazyKt.a(this, d53.b(MyTokensListViewModel.class), new WalletFragment$special$$inlined$activityViewModels$1(this), new WalletFragment$myTokenListViewModel$2(this));
    public final sy1 l0 = FragmentViewModelLazyKt.a(this, d53.b(MultiWalletViewModel.class), new WalletFragment$special$$inlined$viewModels$default$2(new WalletFragment$special$$inlined$viewModels$default$1(this)), null);
    public final sy1 m0 = zy1.a(new WalletFragment$iHomeActivity$2(this));
    public final sy1 n0 = FragmentViewModelLazyKt.a(this, d53.b(qi2.class), new WalletFragment$special$$inlined$activityViewModels$default$3(this), new WalletFragment$special$$inlined$activityViewModels$default$4(this));
    public final sy1 r0 = FragmentViewModelLazyKt.a(this, d53.b(SelectFiatViewModel.class), new WalletFragment$special$$inlined$activityViewModels$3(this), new WalletFragment$selectFiatViewModel$2(this));

    /* compiled from: WalletFragment.kt */
    /* loaded from: classes2.dex */
    public static final class a implements qn4.a {
        public a() {
        }

        /* JADX WARN: Removed duplicated region for block: B:23:0x0064 A[EDGE_INSN: B:23:0x0064->B:17:0x0064 ?: BREAK  , SYNTHETIC] */
        @Override // defpackage.qn4.a
        /*
            Code decompiled incorrectly, please refer to instructions dump.
            To view partially-correct code enable 'Show inconsistent code' option in preferences
        */
        public void a(net.safemoon.androidwallet.ui.displayModel.UserTokenItemDisplayModel r10) {
            /*
                r9 = this;
                java.lang.String r0 = "item"
                defpackage.fs1.f(r10, r0)
                net.safemoon.androidwallet.fragments.WalletFragment r0 = net.safemoon.androidwallet.fragments.WalletFragment.this
                net.safemoon.androidwallet.viewmodels.MyTokensListViewModel r0 = net.safemoon.androidwallet.fragments.WalletFragment.z(r0)
                java.util.ArrayList r0 = r0.y()
                int r1 = r0.size()
                java.util.ListIterator r0 = r0.listIterator(r1)
            L17:
                boolean r1 = r0.hasPrevious()
                r2 = 1
                r3 = 0
                if (r1 == 0) goto L63
                java.lang.Object r1 = r0.previous()
                r4 = r1
                net.safemoon.androidwallet.model.swap.Swap r4 = (net.safemoon.androidwallet.model.swap.Swap) r4
                java.lang.Integer r5 = r4.chainId
                int r6 = r10.getChainId()
                if (r5 != 0) goto L2f
                goto L5f
            L2f:
                int r5 = r5.intValue()
                if (r5 != r6) goto L5f
                java.lang.String r4 = r4.address
                java.lang.String r5 = "swap.address"
                defpackage.fs1.e(r4, r5)
                java.util.Locale r5 = java.util.Locale.ROOT
                java.lang.String r4 = r4.toLowerCase(r5)
                java.lang.String r6 = "(this as java.lang.Strin….toLowerCase(Locale.ROOT)"
                defpackage.fs1.e(r4, r6)
                java.lang.String r7 = r10.getContractAddress()
                java.lang.String r8 = "null cannot be cast to non-null type java.lang.String"
                java.util.Objects.requireNonNull(r7, r8)
                java.lang.String r5 = r7.toLowerCase(r5)
                defpackage.fs1.e(r5, r6)
                boolean r4 = defpackage.fs1.b(r4, r5)
                if (r4 == 0) goto L5f
                r4 = r2
                goto L60
            L5f:
                r4 = r3
            L60:
                if (r4 == 0) goto L17
                goto L64
            L63:
                r1 = 0
            L64:
                net.safemoon.androidwallet.model.swap.Swap r1 = (net.safemoon.androidwallet.model.swap.Swap) r1
                if (r1 == 0) goto L69
                goto L6a
            L69:
                r2 = r3
            L6a:
                r10.setAllowSwap(r2)
                net.safemoon.androidwallet.fragments.WalletFragment r0 = net.safemoon.androidwallet.fragments.WalletFragment.this
                wm4$c r10 = defpackage.wm4.h(r10)
                java.lang.String r1 = "actionNavigationWalletTo…sferHistoryFragment(item)"
                defpackage.fs1.e(r10, r1)
                net.safemoon.androidwallet.fragments.WalletFragment.A(r0, r10)
                return
            */
            throw new UnsupportedOperationException("Method not decompiled: net.safemoon.androidwallet.fragments.WalletFragment.a.a(net.safemoon.androidwallet.ui.displayModel.UserTokenItemDisplayModel):void");
        }
    }

    /* compiled from: WalletFragment.kt */
    /* loaded from: classes2.dex */
    public static final class b extends OnSelectTokenTypeClickListener {
        public b(c cVar, WeakReference<Activity> weakReference, Map<String, ? extends TokenType> map) {
            super(cVar, weakReference, map);
        }

        @Override // net.safemoon.androidwallet.domain.listener.dalog.OnSelectTokenTypeClickListener
        public TokenType c() {
            Context requireContext = WalletFragment.this.requireContext();
            fs1.e(requireContext, "requireContext()");
            return e30.e(requireContext);
        }
    }

    /* compiled from: WalletFragment.kt */
    /* loaded from: classes2.dex */
    public static final class c implements lt2.a {
        public c() {
        }

        @Override // defpackage.lt2.a
        public void a(TokenType tokenType) {
            fs1.f(tokenType, "token");
            WalletFragment.this.H().J(tokenType);
            WalletFragment.this.E().p(tokenType);
        }
    }

    public static final void L(WalletFragment walletFragment, View view) {
        fs1.f(walletFragment, "this$0");
        AnchorSwitchWallet anchorSwitchWallet = new AnchorSwitchWallet(walletFragment.G(), 0);
        Context requireContext = walletFragment.requireContext();
        fs1.e(requireContext, "requireContext()");
        fs1.e(view, "it");
        pb1 pb1Var = walletFragment.i0;
        anchorSwitchWallet.h(requireContext, view, pb1Var == null ? null : pb1Var.e);
    }

    public static final boolean M(WalletFragment walletFragment, View view) {
        fs1.f(walletFragment, "this$0");
        wb wbVar = new wb(walletFragment.H());
        Context requireContext = walletFragment.requireContext();
        fs1.e(requireContext, "requireContext()");
        fs1.e(view, "it");
        pb1 pb1Var = walletFragment.i0;
        wbVar.g(requireContext, view, pb1Var == null ? null : pb1Var.e);
        return true;
    }

    public static final void N(WalletFragment walletFragment, Double d) {
        fs1.f(walletFragment, "this$0");
        if (d != null) {
            pb1 pb1Var = walletFragment.i0;
            fs1.d(pb1Var);
            TextView textView = pb1Var.b.g;
            fs1.e(textView, "binding!!.lMain.txtSymbol");
            e30.W(textView);
            pb1 pb1Var2 = walletFragment.i0;
            fs1.d(pb1Var2);
            TextView textView2 = pb1Var2.b.f;
            fs1.e(textView2, "binding!!.lMain.tvWalletBlnc");
            e30.N(textView2, d.doubleValue(), false);
        }
    }

    public static final void O(WalletFragment walletFragment, Boolean bool) {
        fs1.f(walletFragment, "this$0");
        fs1.e(bool, "isOpen");
        if (bool.booleanValue()) {
            wm4.b g = wm4.g(true);
            fs1.e(g, "actionNavigationWalletToTokenListFragment(true)");
            walletFragment.g(g);
            walletFragment.E().k().postValue(Boolean.FALSE);
        }
    }

    public static final void P(WalletFragment walletFragment, NotificationHistory notificationHistory) {
        NotificationHistoryData data;
        ArrayList<NotificationHistoryResult> result;
        wf wfVar;
        MaterialTextView materialTextView;
        fs1.f(walletFragment, "this$0");
        if (notificationHistory == null || (data = notificationHistory.getData()) == null || (result = data.getResult()) == null) {
            return;
        }
        ArrayList arrayList = new ArrayList();
        Iterator<T> it = result.iterator();
        while (true) {
            if (!it.hasNext()) {
                break;
            }
            Object next = it.next();
            if (true ^ ((NotificationHistoryResult) next).read) {
                arrayList.add(next);
            }
        }
        int size = arrayList.size();
        pb1 pb1Var = walletFragment.i0;
        if (pb1Var == null || (wfVar = pb1Var.a) == null || (materialTextView = wfVar.i) == null) {
            return;
        }
        materialTextView.setVisibility(e30.m0(size > 0));
        materialTextView.setText(size < 100 ? String.valueOf(size) : "99+");
    }

    public static final void Q(pb1 pb1Var, WalletFragment walletFragment) {
        fs1.f(pb1Var, "$this_apply");
        fs1.f(walletFragment, "this$0");
        pb1Var.e.setRefreshing(false);
        TokenType value = walletFragment.E().l().getValue();
        if (value == null) {
            return;
        }
        walletFragment.H().K();
        walletFragment.H().J(value);
        k00 k00Var = walletFragment.o0;
        if (k00Var == null) {
            return;
        }
        k00Var.c();
    }

    public static final void R(WalletFragment walletFragment, TokenType tokenType) {
        fs1.f(walletFragment, "this$0");
        MyTokensListViewModel H = walletFragment.H();
        fs1.e(tokenType, "it");
        H.J(tokenType);
    }

    public static final void S(WalletFragment walletFragment, List list) {
        fs1.f(walletFragment, "this$0");
        if (list != null) {
            qn4 qn4Var = walletFragment.p0;
            fs1.d(qn4Var);
            qn4Var.e(list);
        }
    }

    public static final void T(WalletFragment walletFragment, TokenType tokenType) {
        wn4 wn4Var;
        ry1 ry1Var;
        ImageView imageView;
        wn4 wn4Var2;
        ry1 ry1Var2;
        fs1.f(walletFragment, "this$0");
        pb1 pb1Var = walletFragment.i0;
        MaterialTextView materialTextView = null;
        if (pb1Var != null && (wn4Var2 = pb1Var.b) != null && (ry1Var2 = wn4Var2.d) != null) {
            materialTextView = ry1Var2.d;
        }
        if (materialTextView != null) {
            materialTextView.setText(tokenType.getPlaneName());
        }
        pb1 pb1Var2 = walletFragment.i0;
        if (pb1Var2 == null || (wn4Var = pb1Var2.b) == null || (ry1Var = wn4Var.d) == null || (imageView = ry1Var.c) == null) {
            return;
        }
        imageView.setImageResource(tokenType.getIcon());
    }

    public final void C(View view) {
        TextView textView;
        TextView textView2;
        wf wfVar;
        ImageView imageView;
        wf wfVar2;
        ImageView imageView2;
        wn4 wn4Var;
        MaterialButton materialButton;
        wn4 wn4Var2;
        MaterialButton materialButton2;
        wn4 wn4Var3;
        MaterialButton materialButton3;
        wf wfVar3;
        pb1 a2 = pb1.a(view);
        this.i0 = a2;
        RadioButton radioButton = null;
        if (a2 != null && (wfVar3 = a2.a) != null) {
            radioButton = wfVar3.g;
        }
        if (radioButton != null) {
            radioButton.setChecked(true);
        }
        pb1 pb1Var = this.i0;
        if (pb1Var != null && (wn4Var3 = pb1Var.b) != null && (materialButton3 = wn4Var3.c) != null) {
            materialButton3.setOnClickListener(this);
        }
        pb1 pb1Var2 = this.i0;
        if (pb1Var2 != null && (wn4Var2 = pb1Var2.b) != null && (materialButton2 = wn4Var2.b) != null) {
            materialButton2.setOnClickListener(this);
        }
        pb1 pb1Var3 = this.i0;
        if (pb1Var3 != null && (wn4Var = pb1Var3.b) != null && (materialButton = wn4Var.a) != null) {
            materialButton.setOnClickListener(this);
        }
        pb1 pb1Var4 = this.i0;
        if (pb1Var4 != null && (wfVar2 = pb1Var4.a) != null && (imageView2 = wfVar2.a) != null) {
            imageView2.setOnClickListener(this);
        }
        pb1 pb1Var5 = this.i0;
        if (pb1Var5 != null && (wfVar = pb1Var5.a) != null && (imageView = wfVar.b) != null) {
            imageView.setOnClickListener(this);
        }
        pb1 pb1Var6 = this.i0;
        if (pb1Var6 != null && (textView2 = pb1Var6.f) != null) {
            textView2.setOnClickListener(this);
        }
        pb1 pb1Var7 = this.i0;
        if (pb1Var7 == null || (textView = pb1Var7.g) == null) {
            return;
        }
        textView.setOnClickListener(this);
    }

    public final void D() {
        Resources resources;
        DisplayMetrics displayMetrics;
        wf wfVar;
        if (r44.b(Locale.getDefault()) == 1) {
            int i = Resources.getSystem().getDisplayMetrics().widthPixels;
            FragmentActivity activity = getActivity();
            Float valueOf = (activity == null || (resources = activity.getResources()) == null || (displayMetrics = resources.getDisplayMetrics()) == null) ? null : Float.valueOf(displayMetrics.density);
            fs1.d(valueOf);
            int b2 = i - t42.b(85 * valueOf.floatValue());
            pb1 pb1Var = this.i0;
            SegmentedButton segmentedButton = (pb1Var == null || (wfVar = pb1Var.a) == null) ? null : wfVar.h;
            ViewGroup.LayoutParams layoutParams = segmentedButton != null ? segmentedButton.getLayoutParams() : null;
            if (layoutParams != null) {
                layoutParams.width = b2;
            }
            if (segmentedButton == null) {
                return;
            }
            segmentedButton.setLayoutParams(layoutParams);
        }
    }

    public final HomeViewModel E() {
        return (HomeViewModel) this.j0.getValue();
    }

    public final gm1 F() {
        return (gm1) this.m0.getValue();
    }

    public final MultiWalletViewModel G() {
        return (MultiWalletViewModel) this.l0.getValue();
    }

    public final MyTokensListViewModel H() {
        return (MyTokensListViewModel) this.k0.getValue();
    }

    public final qi2 I() {
        return (qi2) this.n0.getValue();
    }

    public final SelectFiatViewModel J() {
        return (SelectFiatViewModel) this.r0.getValue();
    }

    public final void K() {
        wn4 wn4Var;
        TextView textView;
        wn4 wn4Var2;
        TextView textView2;
        wn4 wn4Var3;
        TextView textView3;
        pb1 pb1Var = this.i0;
        if (pb1Var != null && (wn4Var3 = pb1Var.b) != null && (textView3 = wn4Var3.f) != null) {
            e30.X(textView3, new WalletFragment$observeViewModel$1(this));
        }
        pb1 pb1Var2 = this.i0;
        if (pb1Var2 != null && (wn4Var2 = pb1Var2.b) != null && (textView2 = wn4Var2.e) != null) {
            textView2.setOnClickListener(new View.OnClickListener() { // from class: tm4
                @Override // android.view.View.OnClickListener
                public final void onClick(View view) {
                    WalletFragment.L(WalletFragment.this, view);
                }
            });
        }
        pb1 pb1Var3 = this.i0;
        if (pb1Var3 != null && (wn4Var = pb1Var3.b) != null && (textView = wn4Var.f) != null) {
            textView.setOnLongClickListener(new View.OnLongClickListener() { // from class: um4
                @Override // android.view.View.OnLongClickListener
                public final boolean onLongClick(View view) {
                    boolean M;
                    M = WalletFragment.M(WalletFragment.this, view);
                    return M;
                }
            });
        }
        H().B().observe(getViewLifecycleOwner(), new tl2() { // from class: om4
            @Override // defpackage.tl2
            public final void onChanged(Object obj) {
                WalletFragment.N(WalletFragment.this, (Double) obj);
            }
        });
    }

    public final Object U(q70<? super te4> q70Var) {
        Object f = n71.f(E().i(), new WalletFragment$updateLatestCoinList$2(this, null), q70Var);
        return f == gs1.d() ? f : te4.a;
    }

    public final void V(View view) {
        Wallet.Companion companion = Wallet.Companion;
        String j = bo3.j(getContext(), "SAFEMOON_ACTIVE_WALLET", "");
        fs1.e(j, "getString(context, Share…FEMOON_ACTIVE_WALLET, \"\")");
        Wallet wallet2 = companion.toWallet(j);
        String displayName = wallet2 == null ? null : wallet2.displayName();
        TextView textView = (TextView) view.findViewById(R.id.tvMainWallet);
        if (textView == null) {
            return;
        }
        textView.setText(displayName);
    }

    @Override // androidx.fragment.app.Fragment
    public void onAttach(Context context) {
        fs1.f(context, "context");
        super.onAttach(context);
        gm1 F = F();
        if (F == null) {
            return;
        }
        F.b();
    }

    @Override // android.view.View.OnClickListener
    public void onClick(View view) {
        fs1.f(view, "v");
        switch (view.getId()) {
            case R.id.btnBuy /* 2131362040 */:
                rw2.a.d(new WeakReference<>(requireActivity()), new WalletFragment$onClick$1(this), WalletFragment$onClick$2.INSTANCE);
                return;
            case R.id.btnReceive /* 2131362084 */:
                ce2 d = wm4.d();
                fs1.e(d, "actionNavigationWalletToReceiveFragment()");
                g(d);
                return;
            case R.id.btnSend /* 2131362092 */:
                ce2 e = wm4.e();
                fs1.e(e, "actionNavigationWalletToSendFragment()");
                g(e);
                return;
            case R.id.ivNotification /* 2131362696 */:
                ce2 c2 = wm4.c();
                fs1.e(c2, "actionNavigationWalletTo…ficationHistoryFragment()");
                g(c2);
                return;
            case R.id.ivTokenList /* 2131362702 */:
            case R.id.tvSeeAll /* 2131363446 */:
                ce2 b2 = wm4.b();
                fs1.e(b2, "actionNavigationWalletToMyTokensListFragment()");
                g(b2);
                return;
            case R.id.tvSeeAllTokens /* 2131363447 */:
                ce2 a2 = wm4.a();
                fs1.e(a2, "actionNavigationWalletToAllTokensListFragment()");
                g(a2);
                return;
            default:
                return;
        }
    }

    @Override // androidx.fragment.app.Fragment, android.content.ComponentCallbacks
    public void onConfigurationChanged(Configuration configuration) {
        fs1.f(configuration, "newConfig");
        super.onConfigurationChanged(configuration);
    }

    @Override // androidx.fragment.app.Fragment
    public View onCreateView(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle) {
        fs1.f(layoutInflater, "inflater");
        View inflate = layoutInflater.inflate(R.layout.fragment_wallet, viewGroup, false);
        fs1.e(inflate, "root");
        C(inflate);
        return inflate;
    }

    @Override // androidx.fragment.app.Fragment
    public void onPause() {
        super.onPause();
        OnSelectTokenTypeClickListener onSelectTokenTypeClickListener = this.q0;
        if (onSelectTokenTypeClickListener == null) {
            return;
        }
        onSelectTokenTypeClickListener.b();
    }

    @Override // androidx.fragment.app.Fragment
    public void onResume() {
        super.onResume();
        requireActivity().getWindow().addFlags(Integer.MIN_VALUE);
        requireActivity().getWindow().setStatusBarColor(m70.d(requireActivity(), R.color.p1));
    }

    @Override // net.safemoon.androidwallet.fragments.common.BaseMainFragment, defpackage.qn, androidx.fragment.app.Fragment
    public void onViewCreated(View view, Bundle bundle) {
        fs1.f(view, "view");
        super.onViewCreated(view, bundle);
        pb1 pb1Var = this.i0;
        fs1.d(pb1Var);
        SegmentedButton segmentedButton = pb1Var.a.h;
        fs1.e(segmentedButton, "binding!!.lHeader.segmentedGroup");
        n(segmentedButton);
        I().e().observe(getViewLifecycleOwner(), new tl2() { // from class: sm4
            @Override // defpackage.tl2
            public final void onChanged(Object obj) {
                WalletFragment.P(WalletFragment.this, (NotificationHistory) obj);
            }
        });
        final pb1 pb1Var2 = this.i0;
        if (pb1Var2 != null) {
            this.p0 = new qn4(new a());
            k00 k00Var = new k00(new k00.c() { // from class: net.safemoon.androidwallet.fragments.WalletFragment$onViewCreated$2$2
                @Override // defpackage.k00.c
                public void a(Coin coin) {
                    fs1.f(coin, "coin");
                    FragmentManager childFragmentManager = WalletFragment.this.getChildFragmentManager();
                    fs1.e(childFragmentManager, "childFragmentManager");
                    dy.h(new dy(coin, childFragmentManager), null, WalletFragment$onViewCreated$2$2$onCoinItemClick$1.INSTANCE, 1, null);
                }
            });
            this.o0 = k00Var;
            pb1Var2.c.setAdapter(k00Var);
            pb1Var2.c.setHasFixedSize(true);
            pb1Var2.c.setLayoutManager(new LinearLayoutManager(getActivity(), 0, false));
            pb1Var2.d.setAdapter(this.p0);
            b bVar = new b(new c(), new WeakReference(requireActivity()), E().m());
            this.q0 = bVar;
            pb1Var2.b.d.a.setOnClickListener(bVar);
            pb1Var2.e.setOnRefreshListener(new SwipeRefreshLayout.j() { // from class: vm4
                @Override // androidx.swiperefreshlayout.widget.SwipeRefreshLayout.j
                public final void onRefresh() {
                    WalletFragment.Q(pb1.this, this);
                }
            });
        }
        K();
        D();
        rz1 viewLifecycleOwner = getViewLifecycleOwner();
        fs1.e(viewLifecycleOwner, "viewLifecycleOwner");
        sz1.a(viewLifecycleOwner).b(new WalletFragment$onViewCreated$3(this, null));
        E().l().observe(getViewLifecycleOwner(), new tl2() { // from class: rm4
            @Override // defpackage.tl2
            public final void onChanged(Object obj) {
                WalletFragment.R(WalletFragment.this, (TokenType) obj);
            }
        });
        H().A().observe(getViewLifecycleOwner(), new tl2() { // from class: pm4
            @Override // defpackage.tl2
            public final void onChanged(Object obj) {
                WalletFragment.S(WalletFragment.this, (List) obj);
            }
        });
        E().l().observe(getViewLifecycleOwner(), new tl2() { // from class: qm4
            @Override // defpackage.tl2
            public final void onChanged(Object obj) {
                WalletFragment.T(WalletFragment.this, (TokenType) obj);
            }
        });
        E().k().observe(getViewLifecycleOwner(), new tl2() { // from class: nm4
            @Override // defpackage.tl2
            public final void onChanged(Object obj) {
                WalletFragment.O(WalletFragment.this, (Boolean) obj);
            }
        });
        V(view);
        Fiat.Companion companion = Fiat.Companion;
        String j = bo3.j(requireContext(), "DEFAULT_FIAT", companion.getDEFAULT_CURRENCY_STRING());
        fs1.e(j, "getString(\n            r…CURRENCY_STRING\n        )");
        SelectFiatViewModel.n(J(), companion.to(j), false, null, 6, null);
    }
}
