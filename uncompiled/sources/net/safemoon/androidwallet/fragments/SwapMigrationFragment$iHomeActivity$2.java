package net.safemoon.androidwallet.fragments;

import kotlin.jvm.internal.Lambda;

/* compiled from: SwapMigrationFragment.kt */
/* loaded from: classes2.dex */
public final class SwapMigrationFragment$iHomeActivity$2 extends Lambda implements rc1<gm1> {
    public final /* synthetic */ SwapMigrationFragment this$0;

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public SwapMigrationFragment$iHomeActivity$2(SwapMigrationFragment swapMigrationFragment) {
        super(0);
        this.this$0 = swapMigrationFragment;
    }

    /* JADX WARN: Can't rename method to resolve collision */
    @Override // defpackage.rc1
    public final gm1 invoke() {
        if (this.this$0.requireActivity() instanceof gm1) {
            return (gm1) this.this$0.requireActivity();
        }
        return null;
    }
}
