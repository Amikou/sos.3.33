package net.safemoon.androidwallet.fragments;

import android.content.DialogInterface;
import androidx.fragment.app.FragmentActivity;
import kotlin.jvm.internal.Lambda;
import net.safemoon.androidwallet.R;
import net.safemoon.androidwallet.viewmodels.CustomContractTokenViewModel;

/* compiled from: AddCustomContractFragment.kt */
/* loaded from: classes2.dex */
public final class AddCustomContractFragment$onViewCreated$2$8$1$1 extends Lambda implements tc1<CustomContractTokenViewModel.SaveReturnCode, te4> {
    public final /* synthetic */ AddCustomContractFragment this$0;

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public AddCustomContractFragment$onViewCreated$2$8$1$1(AddCustomContractFragment addCustomContractFragment) {
        super(1);
        this.this$0 = addCustomContractFragment;
    }

    public static final void b(DialogInterface dialogInterface) {
    }

    @Override // defpackage.tc1
    public /* bridge */ /* synthetic */ te4 invoke(CustomContractTokenViewModel.SaveReturnCode saveReturnCode) {
        invoke2(saveReturnCode);
        return te4.a;
    }

    /* renamed from: invoke  reason: avoid collision after fix types in other method */
    public final void invoke2(CustomContractTokenViewModel.SaveReturnCode saveReturnCode) {
        fs1.f(saveReturnCode, "ctr");
        if (saveReturnCode == CustomContractTokenViewModel.SaveReturnCode.BLACKLIST) {
            FragmentActivity requireActivity = this.this$0.requireActivity();
            fs1.e(requireActivity, "requireActivity()");
            jc0.c(requireActivity, Integer.valueOf((int) R.string.warning_title), R.string.warning_black_list_cc, true, u8.a);
        } else if (this.this$0.isAdded() && this.this$0.isVisible()) {
            this.this$0.f();
        }
    }
}
