package net.safemoon.androidwallet.fragments;

import kotlin.jvm.internal.Lambda;
import net.safemoon.androidwallet.viewmodels.SettingNotificationViewModel;

/* compiled from: NotificationFragment.kt */
/* loaded from: classes2.dex */
public final class NotificationFragment$initEnableNotification$1$1$1 extends Lambda implements tc1<Boolean, te4> {
    public final /* synthetic */ NotificationFragment this$0;

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public NotificationFragment$initEnableNotification$1$1$1(NotificationFragment notificationFragment) {
        super(1);
        this.this$0 = notificationFragment;
    }

    @Override // defpackage.tc1
    public /* bridge */ /* synthetic */ te4 invoke(Boolean bool) {
        invoke(bool.booleanValue());
        return te4.a;
    }

    public final void invoke(boolean z) {
        SettingNotificationViewModel H;
        SettingNotificationViewModel H2;
        if (z) {
            H2 = this.this$0.H();
            H2.M();
            return;
        }
        H = this.this$0.H();
        H.Q();
    }
}
