package net.safemoon.androidwallet.fragments;

import android.app.Activity;
import defpackage.lt2;
import java.lang.ref.WeakReference;
import java.util.Map;
import kotlin.jvm.internal.Lambda;
import net.safemoon.androidwallet.common.TokenType;
import net.safemoon.androidwallet.domain.listener.dalog.OnSelectTokenTypeClickListener;
import net.safemoon.androidwallet.viewmodels.SwapViewModel;

/* compiled from: SwapFragment.kt */
/* loaded from: classes2.dex */
public final class SwapFragment$onSelectTokenTypeClickListener$2 extends Lambda implements rc1<a> {
    public final /* synthetic */ SwapFragment this$0;

    /* compiled from: SwapFragment.kt */
    /* loaded from: classes2.dex */
    public static final class a extends OnSelectTokenTypeClickListener {
        public final /* synthetic */ SwapFragment j0;

        /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
        public a(SwapFragment swapFragment, b bVar, WeakReference<Activity> weakReference, Map<String, ? extends TokenType> map) {
            super(bVar, weakReference, map);
            this.j0 = swapFragment;
        }

        @Override // net.safemoon.androidwallet.domain.listener.dalog.OnSelectTokenTypeClickListener
        public TokenType c() {
            TokenType L0;
            L0 = this.j0.L0();
            return L0;
        }
    }

    /* compiled from: SwapFragment.kt */
    /* loaded from: classes2.dex */
    public static final class b implements lt2.a {
        public final /* synthetic */ SwapFragment a;

        public b(SwapFragment swapFragment) {
            this.a = swapFragment;
        }

        @Override // defpackage.lt2.a
        public void a(TokenType tokenType) {
            fs1.f(tokenType, "token");
            try {
                this.a.u0 = true;
                pg4.f(this.a.requireContext());
            } catch (Exception e) {
                e.printStackTrace();
            }
            gb2<SwapViewModel.c> Z = this.a.N0().Z();
            SwapViewModel.c value = Z.getValue();
            if (value == null) {
                value = null;
            } else {
                value.b(tokenType);
                te4 te4Var = te4.a;
            }
            Z.setValue(value);
        }
    }

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public SwapFragment$onSelectTokenTypeClickListener$2(SwapFragment swapFragment) {
        super(0);
        this.this$0 = swapFragment;
    }

    @Override // defpackage.rc1
    public final a invoke() {
        Map M0;
        b bVar = new b(this.this$0);
        WeakReference weakReference = new WeakReference(this.this$0.requireActivity());
        M0 = this.this$0.M0();
        return new a(this.this$0, bVar, weakReference, M0);
    }
}
