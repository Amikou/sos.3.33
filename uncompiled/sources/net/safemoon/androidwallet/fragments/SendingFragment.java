package net.safemoon.androidwallet.fragments;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import androidx.lifecycle.l;
import com.fasterxml.jackson.core.util.MinimalPrettyPrinter;
import com.github.mikephil.charting.utils.Utils;
import com.google.android.material.button.MaterialButton;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import java.lang.ref.WeakReference;
import java.math.RoundingMode;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;
import java.util.Objects;
import net.safemoon.androidwallet.R;
import net.safemoon.androidwallet.common.TokenType;
import net.safemoon.androidwallet.dialogs.ProgressLoading;
import net.safemoon.androidwallet.fragments.SendingFragment;
import net.safemoon.androidwallet.fragments.common.BaseMainFragment;
import net.safemoon.androidwallet.model.contact.abstraction.IContact;
import net.safemoon.androidwallet.model.request.RequestTransaction;
import net.safemoon.androidwallet.ui.displayModel.UserTokenItemDisplayModel;
import net.safemoon.androidwallet.viewmodels.ContactViewModel;
import net.safemoon.androidwallet.viewmodels.TransferViewModel;
import net.safemoon.androidwallet.viewmodels.factory.MyViewModelFactory;

/* loaded from: classes2.dex */
public class SendingFragment extends BaseMainFragment {
    public ab1 l0;
    public UserTokenItemDisplayModel m0;
    public TransferViewModel n0;
    public ContactViewModel o0;
    public IContact p0;
    public CountDownTimer s0;
    public ProgressLoading t0;
    public gm1 u0;
    public String i0 = "";
    public Double j0 = Double.valueOf((double) Utils.DOUBLE_EPSILON);
    public ArrayList<String> k0 = new ArrayList<>();
    public final Long q0 = 5000L;
    public final gb2<Long> r0 = new gb2<>(0L);

    /* loaded from: classes2.dex */
    public class a extends CountDownTimer {
        public a(long j, long j2) {
            super(j, j2);
        }

        @Override // android.os.CountDownTimer
        public void onFinish() {
            try {
                e30.E(SendingFragment.this.requireContext());
                SendingFragment.this.M();
            } catch (Exception e) {
                Context requireContext = SendingFragment.this.requireContext();
                String message = e.getMessage();
                Objects.requireNonNull(message);
                e30.a0(requireContext, message);
            }
        }

        @Override // android.os.CountDownTimer
        public void onTick(long j) {
            SendingFragment.this.r0.setValue(Long.valueOf((SendingFragment.this.q0.longValue() + 50) - j));
        }
    }

    /* loaded from: classes2.dex */
    public class b extends TypeToken<ArrayList<String>> {
        public b(SendingFragment sendingFragment) {
        }
    }

    /* JADX INFO: Access modifiers changed from: private */
    public /* synthetic */ void C(TransferViewModel.a aVar) {
        if (aVar != null) {
            if (aVar.b() && aVar.a() != null) {
                this.l0.b.setVisibility(e30.m0(true));
                this.l0.k.setText(String.format(Locale.US, "Max Fee %s%s", e30.o(aVar.a().doubleValue(), 5, RoundingMode.HALF_UP, false), TokenType.fromValue(this.m0.getChainId()).getNativeToken()));
            } else {
                this.l0.b.setVisibility(e30.m0(false));
                String p = this.n0.j().p();
                if (this.n0.j().s()) {
                    TextView textView = this.l0.k;
                    textView.setText("Insufficient " + p + " balance for this transaction");
                } else {
                    TextView textView2 = this.l0.k;
                    textView2.setText("Not enough " + p + " available as gas for transaction");
                }
            }
            this.l0.k.setVisibility(0);
            return;
        }
        this.l0.b.setVisibility(e30.m0(false));
        this.l0.k.setVisibility(4);
    }

    /* JADX INFO: Access modifiers changed from: private */
    public /* synthetic */ void D(List list) {
        Iterator it = list.iterator();
        while (it.hasNext()) {
            IContact iContact = (IContact) it.next();
            if (iContact.getAddress().toLowerCase().equals(this.i0.toLowerCase(Locale.ROOT))) {
                this.p0 = iContact;
                this.l0.j.setText(iContact.getName());
                com.bumptech.glide.a.u(this.l0.d).y(iContact.getProfilePath()).e0(R.drawable.contact_no_icon).l(R.drawable.contact_no_icon).a(n73.v0()).I0(this.l0.d);
                this.l0.e.setVisibility(0);
                return;
            }
        }
    }

    /* JADX INFO: Access modifiers changed from: private */
    public /* synthetic */ void E(View view) {
        f();
    }

    /* JADX INFO: Access modifiers changed from: private */
    public /* synthetic */ void F(Long l) {
        int longValue = (int) ((l.longValue() * 100) / this.q0.longValue());
        this.l0.f.setProgress(longValue);
        if (l.longValue() > 0) {
            this.l0.f.setVisibility(0);
            int round = Math.round((100 - longValue) / 20.0f);
            MaterialButton materialButton = this.l0.a;
            materialButton.setText("" + Math.max(1, round));
            return;
        }
        this.l0.f.setVisibility(8);
        this.l0.a.setText(R.string.send);
    }

    /* JADX INFO: Access modifiers changed from: private */
    public /* synthetic */ boolean G(View view, MotionEvent motionEvent) {
        if (motionEvent.getAction() == 0) {
            e30.E(requireContext());
            e30.Z(requireContext(), R.string.alert_hold_to_send);
            L();
        }
        if (motionEvent.getAction() != 1 || this.s0 == null) {
            return false;
        }
        this.r0.setValue(0L);
        this.s0.cancel();
        return false;
    }

    /* JADX INFO: Access modifiers changed from: private */
    public /* synthetic */ te4 H(DialogInterface dialogInterface) {
        this.n0.f().setValue(null);
        this.n0.e(this.m0.toToken(), this.i0, this.j0.doubleValue());
        return null;
    }

    /* JADX INFO: Access modifiers changed from: private */
    public /* synthetic */ te4 I(Double d) {
        if (isVisible()) {
            z();
            bh bhVar = bh.a;
            bh.P(new WeakReference(requireContext()), Integer.valueOf((int) R.string.gas_price_updated), null, getString(R.string.gas_price_updated_content), Integer.valueOf((int) R.string.action_ok), null, null, null, new tc1() { // from class: rk3
                @Override // defpackage.tc1
                public final Object invoke(Object obj) {
                    te4 H;
                    H = SendingFragment.this.H((DialogInterface) obj);
                    return H;
                }
            }, null).show();
            return null;
        }
        return null;
    }

    /* JADX INFO: Access modifiers changed from: private */
    public /* synthetic */ void J(TransferViewModel.b bVar) {
        if (bVar != null) {
            z();
            if (bVar.a()) {
                gm1 gm1Var = this.u0;
                if (gm1Var != null) {
                    gm1Var.b();
                    if (!bVar.c().booleanValue()) {
                        gm1 gm1Var2 = this.u0;
                        RequestTransaction l = this.n0.l(this.m0.toToken(), bVar.b());
                        Objects.requireNonNull(l);
                        gm1Var2.c(l);
                    }
                }
                g(net.safemoon.androidwallet.fragments.b.a(this.n0.h(this.m0.toToken()), this.m0.getChainId(), true));
                return;
            }
            this.l0.k.setText(R.string.transfer_refuse);
            this.l0.a.setVisibility(8);
        }
    }

    public ArrayList<String> A() {
        return (ArrayList) new Gson().fromJson(bo3.j(getActivity(), "localTransaction", ""), new b(this).getType());
    }

    public final void B() {
        this.n0.f().setValue(null);
        this.n0.e(this.m0.toToken(), this.i0, this.j0.doubleValue());
        this.n0.f().observe(getViewLifecycleOwner(), new tl2() { // from class: vk3
            @Override // defpackage.tl2
            public final void onChanged(Object obj) {
                SendingFragment.this.C((TransferViewModel.a) obj);
            }
        });
    }

    public final void K(Boolean bool, String str, String str2) {
        ProgressLoading progressLoading = this.t0;
        if (progressLoading != null && progressLoading.isVisible()) {
            this.t0.h();
        }
        ProgressLoading a2 = ProgressLoading.y0.a(bool.booleanValue(), str, str2);
        this.t0 = a2;
        a2.A(getChildFragmentManager());
    }

    public final void L() {
        CountDownTimer countDownTimer = this.s0;
        if (countDownTimer != null) {
            countDownTimer.cancel();
        }
        a aVar = new a(this.q0.longValue(), this.q0.longValue() / 100);
        this.s0 = aVar;
        aVar.start();
    }

    public final void M() {
        K(Boolean.FALSE, getString(R.string.transfer_loading_title), getString(R.string.transfer_loading_msg));
        this.n0.m(this.m0.toToken(), new tc1() { // from class: sk3
            @Override // defpackage.tc1
            public final Object invoke(Object obj) {
                te4 I;
                I = SendingFragment.this.I((Double) obj);
                return I;
            }
        });
    }

    public final void N() {
        this.n0.k().setValue(null);
        this.n0.k().observe(getViewLifecycleOwner(), new tl2() { // from class: wk3
            @Override // defpackage.tl2
            public final void onChanged(Object obj) {
                SendingFragment.this.J((TransferViewModel.b) obj);
            }
        });
    }

    @Override // androidx.fragment.app.Fragment
    public void onAttach(Context context) {
        super.onAttach(context);
        if (requireActivity() instanceof gm1) {
            this.u0 = (gm1) requireActivity();
        }
    }

    @Override // androidx.fragment.app.Fragment
    @SuppressLint({"SetTextI18n"})
    public View onCreateView(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle) {
        View inflate = layoutInflater.inflate(R.layout.fragment_sending, viewGroup, false);
        this.l0 = ab1.a(inflate);
        this.n0 = (TransferViewModel) new l(requireActivity()).a(TransferViewModel.class);
        this.o0 = (ContactViewModel) new l(this, new MyViewModelFactory(new WeakReference(requireActivity()))).a(ContactViewModel.class);
        if (getActivity() != null) {
            getActivity().getWindow().addFlags(Integer.MIN_VALUE);
            getActivity().getWindow().setBackgroundDrawableResource(R.drawable.bottom_curve);
        }
        Bundle arguments = getArguments();
        if (arguments != null) {
            this.m0 = (UserTokenItemDisplayModel) arguments.getSerializable(SendtoFragment.x0);
            if (arguments.containsKey("balance")) {
                Double valueOf = Double.valueOf(e30.K(arguments.getString("balance")));
                this.j0 = valueOf;
                this.l0.h.setText(e30.o(valueOf.doubleValue(), 18, RoundingMode.HALF_UP, true));
                e30.U(this.l0.h, this.j0.doubleValue(), this.m0.getSymbol());
                String o = e30.o(v21.a(this.j0.doubleValue() * this.m0.getPriceInUsdt()), 0, RoundingMode.DOWN, false);
                TextView textView = this.l0.i;
                textView.setText("~ " + u21.a.b() + MinimalPrettyPrinter.DEFAULT_ROOT_VALUE_SEPARATOR + o);
            }
            if (arguments.containsKey("etAddress")) {
                String string = arguments.getString("etAddress");
                this.i0 = string;
                this.l0.g.setText(e30.d(string));
                this.o0.j().observe(getViewLifecycleOwner(), new tl2() { // from class: uk3
                    @Override // defpackage.tl2
                    public final void onChanged(Object obj) {
                        SendingFragment.this.D((List) obj);
                    }
                });
            }
        }
        ArrayList<String> A = A();
        this.k0 = A;
        if (A == null) {
            this.k0 = new ArrayList<>();
        }
        this.l0.c.setOnClickListener(new View.OnClickListener() { // from class: xk3
            @Override // android.view.View.OnClickListener
            public final void onClick(View view) {
                SendingFragment.this.E(view);
            }
        });
        return inflate;
    }

    @Override // androidx.fragment.app.Fragment
    public void onDetach() {
        super.onDetach();
        this.u0 = null;
    }

    @Override // net.safemoon.androidwallet.fragments.common.BaseMainFragment, defpackage.qn, androidx.fragment.app.Fragment
    public void onViewCreated(View view, Bundle bundle) {
        super.onViewCreated(view, bundle);
        B();
        N();
        this.r0.observe(getViewLifecycleOwner(), new tl2() { // from class: tk3
            @Override // defpackage.tl2
            public final void onChanged(Object obj) {
                SendingFragment.this.F((Long) obj);
            }
        });
        this.l0.a.setOnTouchListener(new View.OnTouchListener() { // from class: yk3
            @Override // android.view.View.OnTouchListener
            public final boolean onTouch(View view2, MotionEvent motionEvent) {
                boolean G;
                G = SendingFragment.this.G(view2, motionEvent);
                return G;
            }
        });
    }

    public final void z() {
        ProgressLoading progressLoading = this.t0;
        if (progressLoading != null) {
            progressLoading.h();
        }
    }
}
