package net.safemoon.androidwallet.fragments;

import kotlin.jvm.internal.Lambda;
import net.safemoon.androidwallet.ui.displayModel.UserTokenItemDisplayModel;

/* compiled from: MyTokensListFragment.kt */
/* loaded from: classes2.dex */
public final class MyTokensListFragment$setupAdapter$1 extends Lambda implements tc1<UserTokenItemDisplayModel, te4> {
    public final /* synthetic */ MyTokensListFragment this$0;

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public MyTokensListFragment$setupAdapter$1(MyTokensListFragment myTokensListFragment) {
        super(1);
        this.this$0 = myTokensListFragment;
    }

    @Override // defpackage.tc1
    public /* bridge */ /* synthetic */ te4 invoke(UserTokenItemDisplayModel userTokenItemDisplayModel) {
        invoke2(userTokenItemDisplayModel);
        return te4.a;
    }

    /* JADX WARN: Removed duplicated region for block: B:23:0x0064 A[EDGE_INSN: B:23:0x0064->B:17:0x0064 ?: BREAK  , SYNTHETIC] */
    /* renamed from: invoke  reason: avoid collision after fix types in other method */
    /*
        Code decompiled incorrectly, please refer to instructions dump.
        To view partially-correct code enable 'Show inconsistent code' option in preferences
    */
    public final void invoke2(net.safemoon.androidwallet.ui.displayModel.UserTokenItemDisplayModel r10) {
        /*
            r9 = this;
            java.lang.String r0 = "it"
            defpackage.fs1.f(r10, r0)
            net.safemoon.androidwallet.fragments.MyTokensListFragment r0 = r9.this$0
            net.safemoon.androidwallet.viewmodels.MyTokensListViewModel r0 = net.safemoon.androidwallet.fragments.MyTokensListFragment.B(r0)
            java.util.ArrayList r0 = r0.y()
            int r1 = r0.size()
            java.util.ListIterator r0 = r0.listIterator(r1)
        L17:
            boolean r1 = r0.hasPrevious()
            r2 = 1
            r3 = 0
            if (r1 == 0) goto L63
            java.lang.Object r1 = r0.previous()
            r4 = r1
            net.safemoon.androidwallet.model.swap.Swap r4 = (net.safemoon.androidwallet.model.swap.Swap) r4
            java.lang.Integer r5 = r4.chainId
            int r6 = r10.getChainId()
            if (r5 != 0) goto L2f
            goto L5f
        L2f:
            int r5 = r5.intValue()
            if (r5 != r6) goto L5f
            java.lang.String r4 = r4.address
            java.lang.String r5 = "swap.address"
            defpackage.fs1.e(r4, r5)
            java.util.Locale r5 = java.util.Locale.ROOT
            java.lang.String r4 = r4.toLowerCase(r5)
            java.lang.String r6 = "(this as java.lang.Strin….toLowerCase(Locale.ROOT)"
            defpackage.fs1.e(r4, r6)
            java.lang.String r7 = r10.getContractAddress()
            java.lang.String r8 = "null cannot be cast to non-null type java.lang.String"
            java.util.Objects.requireNonNull(r7, r8)
            java.lang.String r5 = r7.toLowerCase(r5)
            defpackage.fs1.e(r5, r6)
            boolean r4 = defpackage.fs1.b(r4, r5)
            if (r4 == 0) goto L5f
            r4 = r2
            goto L60
        L5f:
            r4 = r3
        L60:
            if (r4 == 0) goto L17
            goto L64
        L63:
            r1 = 0
        L64:
            net.safemoon.androidwallet.model.swap.Swap r1 = (net.safemoon.androidwallet.model.swap.Swap) r1
            if (r1 == 0) goto L69
            goto L6a
        L69:
            r2 = r3
        L6a:
            r10.setAllowSwap(r2)
            net.safemoon.androidwallet.fragments.MyTokensListFragment r0 = r9.this$0
            cc2$b r10 = defpackage.cc2.d(r10)
            java.lang.String r1 = "actionMyTokensListFragme…     it\n                )"
            defpackage.fs1.e(r10, r1)
            net.safemoon.androidwallet.fragments.MyTokensListFragment.C(r0, r10)
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: net.safemoon.androidwallet.fragments.MyTokensListFragment$setupAdapter$1.invoke2(net.safemoon.androidwallet.ui.displayModel.UserTokenItemDisplayModel):void");
    }
}
