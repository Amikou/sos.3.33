package net.safemoon.androidwallet.fragments;

import defpackage.n32;
import kotlin.jvm.internal.Lambda;
import net.safemoon.androidwallet.model.contact.abstraction.IContact;

/* compiled from: ManageContactsFragment.kt */
/* loaded from: classes2.dex */
public final class ManageContactsFragment$adapter$2 extends Lambda implements rc1<i32> {
    public final /* synthetic */ ManageContactsFragment this$0;

    /* compiled from: ManageContactsFragment.kt */
    /* loaded from: classes2.dex */
    public static final class a implements sl1 {
        public final /* synthetic */ ManageContactsFragment a;

        public a(ManageContactsFragment manageContactsFragment) {
            this.a = manageContactsFragment;
        }

        @Override // defpackage.sl1
        public void a(IContact iContact) {
            fs1.f(iContact, "item");
            ManageContactsFragment manageContactsFragment = this.a;
            n32.b b = n32.b(iContact);
            fs1.e(b, "actionManageContactsFrag…                        )");
            manageContactsFragment.g(b);
        }
    }

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public ManageContactsFragment$adapter$2(ManageContactsFragment manageContactsFragment) {
        super(0);
        this.this$0 = manageContactsFragment;
    }

    /* JADX WARN: Can't rename method to resolve collision */
    @Override // defpackage.rc1
    public final i32 invoke() {
        return new i32(new a(this.this$0));
    }
}
