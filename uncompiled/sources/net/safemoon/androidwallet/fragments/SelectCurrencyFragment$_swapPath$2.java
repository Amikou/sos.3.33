package net.safemoon.androidwallet.fragments;

import android.os.Bundle;
import java.io.Serializable;
import java.util.Objects;
import kotlin.jvm.internal.Lambda;
import net.safemoon.androidwallet.fragments.SelectCurrencyFragment;

/* compiled from: SelectCurrencyFragment.kt */
/* loaded from: classes2.dex */
public final class SelectCurrencyFragment$_swapPath$2 extends Lambda implements rc1<SelectCurrencyFragment.SWAPPATH> {
    public final /* synthetic */ SelectCurrencyFragment this$0;

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public SelectCurrencyFragment$_swapPath$2(SelectCurrencyFragment selectCurrencyFragment) {
        super(0);
        this.this$0 = selectCurrencyFragment;
    }

    @Override // defpackage.rc1
    public final SelectCurrencyFragment.SWAPPATH invoke() {
        Bundle arguments = this.this$0.getArguments();
        Serializable serializable = arguments == null ? null : arguments.getSerializable("SWAPPATH");
        Objects.requireNonNull(serializable, "null cannot be cast to non-null type net.safemoon.androidwallet.fragments.SelectCurrencyFragment.SWAPPATH");
        return (SelectCurrencyFragment.SWAPPATH) serializable;
    }
}
