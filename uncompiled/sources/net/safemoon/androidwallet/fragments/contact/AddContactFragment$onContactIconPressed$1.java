package net.safemoon.androidwallet.fragments.contact;

import android.content.Intent;
import android.net.Uri;
import com.bumptech.glide.a;
import kotlin.jvm.internal.Lambda;
import net.safemoon.androidwallet.R;
import net.safemoon.androidwallet.model.contact.RequestContact;

/* compiled from: AddContactFragment.kt */
/* loaded from: classes2.dex */
public final class AddContactFragment$onContactIconPressed$1 extends Lambda implements rc1<te4> {
    public final /* synthetic */ AddContactFragment this$0;

    /* compiled from: AddContactFragment.kt */
    /* renamed from: net.safemoon.androidwallet.fragments.contact.AddContactFragment$onContactIconPressed$1$1  reason: invalid class name */
    /* loaded from: classes2.dex */
    public static final class AnonymousClass1 extends Lambda implements tc1<Intent, te4> {
        public final /* synthetic */ AddContactFragment this$0;

        /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
        public AnonymousClass1(AddContactFragment addContactFragment) {
            super(1);
            this.this$0 = addContactFragment;
        }

        @Override // defpackage.tc1
        public /* bridge */ /* synthetic */ te4 invoke(Intent intent) {
            invoke2(intent);
            return te4.a;
        }

        /* renamed from: invoke  reason: avoid collision after fix types in other method */
        public final void invoke2(Intent intent) {
            Uri data;
            gb2 gb2Var;
            pg4.b(this.this$0.requireActivity(), Boolean.FALSE);
            if (intent == null || (data = intent.getData()) == null) {
                return;
            }
            AddContactFragment addContactFragment = this.this$0;
            gb2Var = addContactFragment.n0;
            RequestContact requestContact = (RequestContact) gb2Var.getValue();
            if (requestContact != null) {
                requestContact.setProfilePath(data);
            }
            a.u(addContactFragment.C().i).u(data).e0(R.drawable.contact_no_icon).l(R.drawable.contact_no_icon).d0(addContactFragment.E(), addContactFragment.E()).a(n73.v0()).I0(addContactFragment.C().i);
        }
    }

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public AddContactFragment$onContactIconPressed$1(AddContactFragment addContactFragment) {
        super(0);
        this.this$0 = addContactFragment;
    }

    @Override // defpackage.rc1
    public /* bridge */ /* synthetic */ te4 invoke() {
        invoke2();
        return te4.a;
    }

    @Override // defpackage.rc1
    /* renamed from: invoke  reason: avoid collision after fix types in other method */
    public final void invoke2() {
        hn2 l;
        w7<Intent> b;
        String str;
        l = this.this$0.l();
        if (l == null || (b = l.b(new AnonymousClass1(this.this$0))) == null) {
            return;
        }
        Intent intent = new Intent();
        AddContactFragment addContactFragment = this.this$0;
        pg4.b(addContactFragment.requireActivity(), Boolean.TRUE);
        str = addContactFragment.m0;
        intent.setType(str);
        intent.setAction("android.intent.action.GET_CONTENT");
        te4 te4Var = te4.a;
        b.a(intent);
    }
}
