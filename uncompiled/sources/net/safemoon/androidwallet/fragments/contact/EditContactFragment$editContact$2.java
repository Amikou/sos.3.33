package net.safemoon.androidwallet.fragments.contact;

import android.os.Bundle;
import java.io.Serializable;
import java.util.Objects;
import kotlin.jvm.internal.Lambda;
import net.safemoon.androidwallet.model.contact.abstraction.IContact;

/* compiled from: EditContactFragment.kt */
/* loaded from: classes2.dex */
public final class EditContactFragment$editContact$2 extends Lambda implements rc1<IContact> {
    public final /* synthetic */ EditContactFragment this$0;

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public EditContactFragment$editContact$2(EditContactFragment editContactFragment) {
        super(0);
        this.this$0 = editContactFragment;
    }

    @Override // defpackage.rc1
    public final IContact invoke() {
        Bundle arguments = this.this$0.getArguments();
        Serializable serializable = arguments == null ? null : arguments.getSerializable("contact");
        Objects.requireNonNull(serializable, "null cannot be cast to non-null type net.safemoon.androidwallet.model.contact.abstraction.IContact");
        return (IContact) serializable;
    }
}
