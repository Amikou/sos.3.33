package net.safemoon.androidwallet.fragments.contact;

import android.content.Context;
import android.os.Bundle;
import android.view.View;
import androidx.constraintlayout.widget.ConstraintLayout;
import com.bumptech.glide.a;
import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.Objects;
import kotlin.text.StringsKt__StringsKt;
import net.safemoon.androidwallet.R;
import net.safemoon.androidwallet.common.TokenType;
import net.safemoon.androidwallet.fragments.contact.EditContactFragment;
import net.safemoon.androidwallet.model.contact.RequestContact;
import net.safemoon.androidwallet.model.contact.abstraction.IContact;
import net.safemoon.androidwallet.utils.StoragePermissionLauncher;

/* compiled from: EditContactFragment.kt */
/* loaded from: classes2.dex */
public final class EditContactFragment extends BaseContactFragment {
    public String p0;
    public final int m0 = 250;
    public final String n0 = "image/*";
    public final sy1 o0 = zy1.a(new EditContactFragment$editContact$2(this));
    public final gb2<RequestContact> q0 = new gb2<>(new RequestContact());

    public static final void r0(EditContactFragment editContactFragment, RequestContact requestContact) {
        fs1.f(editContactFragment, "this$0");
        if (requestContact == null) {
            return;
        }
        o91 C = editContactFragment.C();
        String name = requestContact.getName();
        if (name != null) {
            if (name.length() == 0) {
                name = null;
            }
            if (name != null) {
                C.g.setText(name);
            }
        }
        String address = requestContact.getAddress();
        if (address != null) {
            String str = address.length() == 0 ? null : address;
            if (str != null) {
                C.f.setText(str);
            }
        }
        Object profilePath = requestContact.getProfilePath();
        if (profilePath == null) {
            profilePath = requestContact.getOldProfilePath();
        }
        if (profilePath != null) {
            a.u(editContactFragment.C().i).x(profilePath).e0(R.drawable.contact_no_icon).l(R.drawable.contact_no_icon).d0(editContactFragment.E(), editContactFragment.E()).a(n73.v0()).I0(editContactFragment.C().i);
        }
    }

    public static final void s0(EditContactFragment editContactFragment, List list) {
        fs1.f(editContactFragment, "this$0");
        if (list == null) {
            return;
        }
        ArrayList arrayList = new ArrayList();
        arrayList.clear();
        arrayList.addAll(list);
        ArrayList arrayList2 = new ArrayList();
        ArrayList<IContact> arrayList3 = new ArrayList();
        for (Object obj : arrayList) {
            String address = ((IContact) obj).getAddress();
            Objects.requireNonNull(address, "null cannot be cast to non-null type java.lang.String");
            Locale locale = Locale.ROOT;
            String lowerCase = address.toLowerCase(locale);
            fs1.e(lowerCase, "(this as java.lang.Strin….toLowerCase(Locale.ROOT)");
            String address2 = editContactFragment.q0().getAddress();
            Objects.requireNonNull(address2, "null cannot be cast to non-null type java.lang.String");
            String lowerCase2 = address2.toLowerCase(locale);
            fs1.e(lowerCase2, "(this as java.lang.Strin….toLowerCase(Locale.ROOT)");
            if (fs1.b(lowerCase, lowerCase2)) {
                arrayList3.add(obj);
            }
        }
        for (IContact iContact : arrayList3) {
            arrayList2.add(TokenType.Companion.b(iContact.getChainAddress()));
        }
        editContactFragment.D().m().postValue(arrayList2);
        editContactFragment.C().l.setChecked(arrayList2.size() == b30.a.a().size());
    }

    public static final void t0(EditContactFragment editContactFragment, View view) {
        fs1.f(editContactFragment, "this$0");
        editContactFragment.u0();
    }

    @Override // net.safemoon.androidwallet.fragments.contact.BaseContactFragment
    public void K(String str) {
        fs1.f(str, "data");
        RequestContact value = this.q0.getValue();
        if (value == null) {
            return;
        }
        value.setAddress(str);
    }

    @Override // net.safemoon.androidwallet.fragments.contact.BaseContactFragment
    public void L() {
        StoragePermissionLauncher k = k();
        if (k == null) {
            return;
        }
        ConstraintLayout constraintLayout = C().k;
        fs1.e(constraintLayout, "binding.mainLayout");
        k.c(constraintLayout, new EditContactFragment$onContactIconPressed$1(this));
    }

    @Override // net.safemoon.androidwallet.fragments.contact.BaseContactFragment
    public void M(String str) {
        fs1.f(str, "data");
        RequestContact value = this.q0.getValue();
        if (value == null) {
            return;
        }
        value.setName(str);
    }

    @Override // net.safemoon.androidwallet.fragments.contact.BaseContactFragment
    public void R() {
        ArrayList arrayList;
        RequestContact value = this.q0.getValue();
        if (value == null) {
            return;
        }
        List<TokenType> value2 = D().m().getValue();
        if (value2 == null) {
            arrayList = null;
        } else {
            ArrayList arrayList2 = new ArrayList(c20.q(value2, 10));
            for (TokenType tokenType : value2) {
                arrayList2.add(Integer.valueOf(tokenType.getChainId()));
            }
            arrayList = arrayList2;
        }
        if (arrayList == null) {
            arrayList = b20.g();
        }
        value.setNetworks(arrayList);
        if (!arrayList.isEmpty()) {
            D().q(value, new EditContactFragment$save$1$1(this, value));
            return;
        }
        Context context = getContext();
        if (context == null) {
            return;
        }
        e30.Z(context, R.string.select_one_chain);
    }

    @Override // net.safemoon.androidwallet.fragments.contact.BaseContactFragment
    public void V() {
        C().n.e.setText(getText(R.string.edit_contact_screen_title));
        C().n.b.setImageResource(R.drawable.ic_baseline_delete_24);
        C().n.b.setOnClickListener(new View.OnClickListener() { // from class: fu0
            @Override // android.view.View.OnClickListener
            public final void onClick(View view) {
                EditContactFragment.t0(EditContactFragment.this, view);
            }
        });
    }

    @Override // androidx.fragment.app.Fragment
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        IContact q0 = q0();
        RequestContact value = this.q0.getValue();
        if (value == null) {
            return;
        }
        value.setId(q0.getId());
        value.setOldProfilePath(q0.getProfilePath());
        value.setName(q0.getName());
        value.setAddress(q0.getAddress());
        value.setContactCreate(q0.getContactCreate());
        value.setLastSent(q0.getLastSent());
        this.p0 = q0.getAddress();
    }

    @Override // net.safemoon.androidwallet.fragments.contact.BaseContactFragment, net.safemoon.androidwallet.fragments.common.BaseMainFragment, defpackage.qn, androidx.fragment.app.Fragment
    public void onViewCreated(View view, Bundle bundle) {
        gb2 b;
        fs1.f(view, "view");
        super.onViewCreated(view, bundle);
        xd2 h = ka1.a(this).h();
        List list = null;
        ec3 d = h == null ? null : h.d();
        if (d != null && (b = d.b("RESULT_SELECTED_CHAIN")) != null) {
            list = (List) b.getValue();
        }
        if (list == null) {
            g72<List<IContact>> j = D().j();
            rz1 viewLifecycleOwner = getViewLifecycleOwner();
            fs1.e(viewLifecycleOwner, "viewLifecycleOwner");
            q02.a(j, viewLifecycleOwner, new tl2() { // from class: du0
                @Override // defpackage.tl2
                public final void onChanged(Object obj) {
                    EditContactFragment.s0(EditContactFragment.this, (List) obj);
                }
            });
        }
        gb2<RequestContact> gb2Var = this.q0;
        rz1 viewLifecycleOwner2 = getViewLifecycleOwner();
        fs1.e(viewLifecycleOwner2, "viewLifecycleOwner");
        q02.a(gb2Var, viewLifecycleOwner2, new tl2() { // from class: eu0
            @Override // defpackage.tl2
            public final void onChanged(Object obj) {
                EditContactFragment.r0(EditContactFragment.this, (RequestContact) obj);
            }
        });
    }

    public final void p0(String str) {
        String obj;
        String lowerCase;
        String obj2;
        String lowerCase2;
        String str2;
        if (str == null || (obj = StringsKt__StringsKt.K0(str).toString()) == null) {
            lowerCase = null;
        } else {
            lowerCase = obj.toLowerCase(Locale.ROOT);
            fs1.e(lowerCase, "(this as java.lang.Strin….toLowerCase(Locale.ROOT)");
        }
        String str3 = this.p0;
        if (str3 == null || (obj2 = StringsKt__StringsKt.K0(str3).toString()) == null) {
            lowerCase2 = null;
        } else {
            lowerCase2 = obj2.toLowerCase(Locale.ROOT);
            fs1.e(lowerCase2, "(this as java.lang.Strin….toLowerCase(Locale.ROOT)");
        }
        if (dv3.u(lowerCase, lowerCase2, false, 2, null) || (str2 = this.p0) == null) {
            return;
        }
        D().e(str2, EditContactFragment$checkEditedAddress$1$1.INSTANCE);
    }

    public final IContact q0() {
        return (IContact) this.o0.getValue();
    }

    public final void u0() {
        bh.r0(new WeakReference(requireActivity()), R.string.dialog_remove_contact_text, R.string.action_delete, new EditContactFragment$showRemoveContactConfirmationDialog$1(this));
    }
}
