package net.safemoon.androidwallet.fragments.contact;

import android.content.Context;
import android.content.Intent;
import kotlin.jvm.internal.Lambda;
import net.safemoon.androidwallet.R;

/* compiled from: BaseContactFragment.kt */
/* loaded from: classes2.dex */
public final class BaseContactFragment$setupView$6$1 extends Lambda implements tc1<Intent, te4> {
    public final /* synthetic */ BaseContactFragment this$0;

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public BaseContactFragment$setupView$6$1(BaseContactFragment baseContactFragment) {
        super(1);
        this.this$0 = baseContactFragment;
    }

    @Override // defpackage.tc1
    public /* bridge */ /* synthetic */ te4 invoke(Intent intent) {
        invoke2(intent);
        return te4.a;
    }

    /* renamed from: invoke  reason: avoid collision after fix types in other method */
    public final void invoke2(Intent intent) {
        if (intent != null) {
            String stringExtra = intent.getStringExtra("result");
            if (stringExtra != null) {
                try {
                    stringExtra = b30.a.q(stringExtra);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
            if (stringExtra == null) {
                return;
            }
            BaseContactFragment baseContactFragment = this.this$0;
            try {
                baseContactFragment.C().f.setText(stringExtra);
            } catch (Exception unused) {
                Context requireContext = baseContactFragment.requireContext();
                fs1.e(requireContext, "requireContext()");
                String string = baseContactFragment.getString(R.string.err_invalid_wc);
                fs1.e(string, "getString(R.string.err_invalid_wc)");
                e30.a0(requireContext, string);
            }
        }
    }
}
