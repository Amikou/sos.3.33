package net.safemoon.androidwallet.fragments.contact;

import android.content.Context;
import android.os.Bundle;
import android.view.View;
import androidx.appcompat.widget.AppCompatImageView;
import androidx.constraintlayout.widget.ConstraintLayout;
import com.bumptech.glide.a;
import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.List;
import java.util.ListIterator;
import java.util.Locale;
import java.util.Objects;
import net.safemoon.androidwallet.R;
import net.safemoon.androidwallet.common.TokenType;
import net.safemoon.androidwallet.fragments.contact.AddContactFragment;
import net.safemoon.androidwallet.model.contact.RequestContact;
import net.safemoon.androidwallet.model.contact.abstraction.IContact;
import net.safemoon.androidwallet.utils.StoragePermissionLauncher;

/* compiled from: AddContactFragment.kt */
/* loaded from: classes2.dex */
public final class AddContactFragment extends BaseContactFragment {
    public final String m0 = "image/*";
    public final gb2<RequestContact> n0 = new gb2<>(new RequestContact());
    public final List<IContact> o0 = new ArrayList();

    public static final void l0(AddContactFragment addContactFragment, List list) {
        fs1.f(addContactFragment, "this$0");
        if (list == null) {
            return;
        }
        addContactFragment.o0.addAll(list);
    }

    public static final void m0(AddContactFragment addContactFragment, RequestContact requestContact) {
        fs1.f(addContactFragment, "this$0");
        if (requestContact == null) {
            return;
        }
        o91 C = addContactFragment.C();
        String name = requestContact.getName();
        if (name != null) {
            if (name.length() == 0) {
                name = null;
            }
            if (name != null) {
                C.g.setText(name);
            }
        }
        String address = requestContact.getAddress();
        if (address != null) {
            String str = address.length() == 0 ? null : address;
            if (str != null) {
                C.f.setText(str);
            }
        }
        if (requestContact.getProfilePath() != null) {
            a.u(addContactFragment.C().i).u(requestContact.getProfilePath()).e0(R.drawable.contact_no_icon).l(R.drawable.contact_no_icon).d0(addContactFragment.E(), addContactFragment.E()).a(n73.v0()).I0(addContactFragment.C().i);
        }
    }

    @Override // net.safemoon.androidwallet.fragments.contact.BaseContactFragment
    public void K(String str) {
        fs1.f(str, "data");
        RequestContact value = this.n0.getValue();
        if (value == null) {
            return;
        }
        value.setAddress(str);
    }

    @Override // net.safemoon.androidwallet.fragments.contact.BaseContactFragment
    public void L() {
        StoragePermissionLauncher k = k();
        if (k == null) {
            return;
        }
        ConstraintLayout constraintLayout = C().k;
        fs1.e(constraintLayout, "binding.mainLayout");
        k.c(constraintLayout, new AddContactFragment$onContactIconPressed$1(this));
    }

    @Override // net.safemoon.androidwallet.fragments.contact.BaseContactFragment
    public void M(String str) {
        fs1.f(str, "data");
        RequestContact value = this.n0.getValue();
        if (value == null) {
            return;
        }
        value.setName(str);
    }

    @Override // net.safemoon.androidwallet.fragments.contact.BaseContactFragment
    public void R() {
        String str;
        RequestContact value = this.n0.getValue();
        if (value == null) {
            return;
        }
        List<IContact> list = this.o0;
        ListIterator<IContact> listIterator = list.listIterator(list.size());
        while (true) {
            str = null;
            if (!listIterator.hasPrevious()) {
                break;
            }
            IContact previous = listIterator.previous();
            IContact iContact = previous;
            String address = value.getAddress();
            if (address != null) {
                str = address.toLowerCase(Locale.ROOT);
                fs1.e(str, "(this as java.lang.Strin….toLowerCase(Locale.ROOT)");
            }
            String address2 = iContact.getAddress();
            Objects.requireNonNull(address2, "null cannot be cast to non-null type java.lang.String");
            String lowerCase = address2.toLowerCase(Locale.ROOT);
            fs1.e(lowerCase, "(this as java.lang.Strin….toLowerCase(Locale.ROOT)");
            if (fs1.b(str, lowerCase)) {
                str = previous;
                break;
            }
        }
        if (str != null) {
            bh.P(new WeakReference(requireActivity()), (r23 & 2) != 0 ? null : null, (r23 & 4) != 0 ? null : Integer.valueOf((int) R.string.contact_exist_title), (r23 & 8) != 0 ? null : null, (r23 & 16) != 0 ? Integer.valueOf((int) R.string.action_ok) : null, (r23 & 32) != 0 ? Integer.valueOf((int) R.string.cancel) : Integer.valueOf((int) R.string.action_ok), (r23 & 64) != 0 ? null : null, (r23 & 128) != 0 ? null : null, AddContactFragment$save$1$2.INSTANCE, AddContactFragment$save$1$3.INSTANCE);
            return;
        }
        n0(value);
    }

    @Override // net.safemoon.androidwallet.fragments.contact.BaseContactFragment
    public void V() {
        C().n.e.setText(getText(R.string.add_contact_screen_title));
        AppCompatImageView appCompatImageView = C().n.b;
        fs1.e(appCompatImageView, "binding.toolbar.ivToolbarAction");
        appCompatImageView.setVisibility(8);
    }

    public final void n0(RequestContact requestContact) {
        ArrayList arrayList;
        List<TokenType> value = D().m().getValue();
        if (value == null) {
            arrayList = null;
        } else {
            ArrayList arrayList2 = new ArrayList(c20.q(value, 10));
            for (TokenType tokenType : value) {
                arrayList2.add(Integer.valueOf(tokenType.getChainId()));
            }
            arrayList = arrayList2;
        }
        if (arrayList == null) {
            arrayList = b20.g();
        }
        requestContact.setNetworks(arrayList);
        if (!arrayList.isEmpty()) {
            D().q(requestContact, new AddContactFragment$save$2(this));
            return;
        }
        Context context = getContext();
        if (context == null) {
            return;
        }
        e30.Z(context, R.string.select_one_chain);
    }

    @Override // androidx.fragment.app.Fragment
    public void onResume() {
        super.onResume();
        pg4.b(requireActivity(), Boolean.FALSE);
    }

    @Override // net.safemoon.androidwallet.fragments.contact.BaseContactFragment, net.safemoon.androidwallet.fragments.common.BaseMainFragment, defpackage.qn, androidx.fragment.app.Fragment
    public void onViewCreated(View view, Bundle bundle) {
        fs1.f(view, "view");
        super.onViewCreated(view, bundle);
        D().j().observe(getViewLifecycleOwner(), new tl2() { // from class: j8
            @Override // defpackage.tl2
            public final void onChanged(Object obj) {
                AddContactFragment.l0(AddContactFragment.this, (List) obj);
            }
        });
        gb2<RequestContact> gb2Var = this.n0;
        rz1 viewLifecycleOwner = getViewLifecycleOwner();
        fs1.e(viewLifecycleOwner, "viewLifecycleOwner");
        q02.a(gb2Var, viewLifecycleOwner, new tl2() { // from class: k8
            @Override // defpackage.tl2
            public final void onChanged(Object obj) {
                AddContactFragment.m0(AddContactFragment.this, (RequestContact) obj);
            }
        });
    }
}
