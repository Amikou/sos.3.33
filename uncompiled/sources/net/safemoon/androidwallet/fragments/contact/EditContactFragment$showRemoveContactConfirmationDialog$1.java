package net.safemoon.androidwallet.fragments.contact;

import kotlin.jvm.internal.Lambda;
import net.safemoon.androidwallet.model.contact.abstraction.IContact;
import net.safemoon.androidwallet.viewmodels.ContactViewModel;

/* compiled from: EditContactFragment.kt */
/* loaded from: classes2.dex */
public final class EditContactFragment$showRemoveContactConfirmationDialog$1 extends Lambda implements rc1<te4> {
    public final /* synthetic */ EditContactFragment this$0;

    /* compiled from: EditContactFragment.kt */
    /* renamed from: net.safemoon.androidwallet.fragments.contact.EditContactFragment$showRemoveContactConfirmationDialog$1$1  reason: invalid class name */
    /* loaded from: classes2.dex */
    public static final class AnonymousClass1 extends Lambda implements rc1<te4> {
        public final /* synthetic */ EditContactFragment this$0;

        /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
        public AnonymousClass1(EditContactFragment editContactFragment) {
            super(0);
            this.this$0 = editContactFragment;
        }

        @Override // defpackage.rc1
        public /* bridge */ /* synthetic */ te4 invoke() {
            invoke2();
            return te4.a;
        }

        @Override // defpackage.rc1
        /* renamed from: invoke  reason: avoid collision after fix types in other method */
        public final void invoke2() {
            this.this$0.f();
        }
    }

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public EditContactFragment$showRemoveContactConfirmationDialog$1(EditContactFragment editContactFragment) {
        super(0);
        this.this$0 = editContactFragment;
    }

    @Override // defpackage.rc1
    public /* bridge */ /* synthetic */ te4 invoke() {
        invoke2();
        return te4.a;
    }

    @Override // defpackage.rc1
    /* renamed from: invoke  reason: avoid collision after fix types in other method */
    public final void invoke2() {
        IContact q0;
        ContactViewModel D = this.this$0.D();
        q0 = this.this$0.q0();
        D.f(q0, new AnonymousClass1(this.this$0));
    }
}
