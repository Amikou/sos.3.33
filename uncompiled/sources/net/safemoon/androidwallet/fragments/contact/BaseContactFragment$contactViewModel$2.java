package net.safemoon.androidwallet.fragments.contact;

import androidx.lifecycle.l;
import java.lang.ref.WeakReference;
import kotlin.jvm.internal.Lambda;
import net.safemoon.androidwallet.viewmodels.factory.MyViewModelFactory;

/* compiled from: BaseContactFragment.kt */
/* loaded from: classes2.dex */
public final class BaseContactFragment$contactViewModel$2 extends Lambda implements rc1<l.b> {
    public final /* synthetic */ BaseContactFragment this$0;

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public BaseContactFragment$contactViewModel$2(BaseContactFragment baseContactFragment) {
        super(0);
        this.this$0 = baseContactFragment;
    }

    /* JADX WARN: Can't rename method to resolve collision */
    @Override // defpackage.rc1
    public final l.b invoke() {
        return new MyViewModelFactory(new WeakReference(this.this$0.requireActivity()));
    }
}
