package net.safemoon.androidwallet.fragments.contact;

import kotlin.jvm.internal.Lambda;
import net.safemoon.androidwallet.model.contact.RequestContact;

/* compiled from: EditContactFragment.kt */
/* loaded from: classes2.dex */
public final class EditContactFragment$save$1$1 extends Lambda implements rc1<te4> {
    public final /* synthetic */ RequestContact $it;
    public final /* synthetic */ EditContactFragment this$0;

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public EditContactFragment$save$1$1(EditContactFragment editContactFragment, RequestContact requestContact) {
        super(0);
        this.this$0 = editContactFragment;
        this.$it = requestContact;
    }

    @Override // defpackage.rc1
    public /* bridge */ /* synthetic */ te4 invoke() {
        invoke2();
        return te4.a;
    }

    @Override // defpackage.rc1
    /* renamed from: invoke  reason: avoid collision after fix types in other method */
    public final void invoke2() {
        this.this$0.p0(this.$it.getAddress());
        this.this$0.f();
    }
}
