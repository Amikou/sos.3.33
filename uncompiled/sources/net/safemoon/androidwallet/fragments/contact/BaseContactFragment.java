package net.safemoon.androidwallet.fragments.contact;

import android.content.ClipData;
import android.content.ClipboardManager;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.text.Editable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CompoundButton;
import androidx.appcompat.widget.AppCompatImageView;
import androidx.fragment.app.FragmentViewModelLazyKt;
import com.google.android.material.button.MaterialButton;
import defpackage.gu0;
import defpackage.l8;
import java.util.Iterator;
import java.util.List;
import net.safemoon.androidwallet.R;
import net.safemoon.androidwallet.activity.ScannedCodeActivity;
import net.safemoon.androidwallet.common.TokenType;
import net.safemoon.androidwallet.fragments.common.BaseMainFragment;
import net.safemoon.androidwallet.fragments.contact.BaseContactFragment;
import net.safemoon.androidwallet.model.arguments.TokenTypes;
import net.safemoon.androidwallet.utils.ImageUtility;
import net.safemoon.androidwallet.viewmodels.ContactViewModel;

/* compiled from: BaseContactFragment.kt */
/* loaded from: classes2.dex */
public abstract class BaseContactFragment extends BaseMainFragment {
    public o91 i0;
    public final int j0 = 250;
    public final sy1 k0 = zy1.a(new BaseContactFragment$manager$2(this));
    public final sy1 l0 = FragmentViewModelLazyKt.a(this, d53.b(ContactViewModel.class), new BaseContactFragment$special$$inlined$viewModels$default$2(new BaseContactFragment$special$$inlined$viewModels$default$1(this)), new BaseContactFragment$contactViewModel$2(this));

    /* compiled from: BaseContactFragment.kt */
    /* loaded from: classes2.dex */
    public static final class a extends vu0 {
        public a() {
        }

        @Override // defpackage.vu0, android.text.TextWatcher
        public void afterTextChanged(Editable editable) {
            String obj;
            if (BaseContactFragment.this.isVisible()) {
                String str = "";
                if (editable != null && (obj = editable.toString()) != null) {
                    str = obj;
                }
                if (s44.a.c(str)) {
                    BaseContactFragment.this.M(str);
                } else {
                    BaseContactFragment.this.C().g.setError(BaseContactFragment.this.getResources().getString(R.string.error_wrong_contact_name));
                }
                BaseContactFragment.this.e0();
            }
        }

        @Override // defpackage.vu0, android.text.TextWatcher
        public void beforeTextChanged(CharSequence charSequence, int i, int i2, int i3) {
            if (BaseContactFragment.this.isVisible()) {
                BaseContactFragment.this.C().g.setError(null);
            }
        }
    }

    /* compiled from: BaseContactFragment.kt */
    /* loaded from: classes2.dex */
    public static final class b extends vu0 {
        public b() {
        }

        @Override // defpackage.vu0, android.text.TextWatcher
        public void afterTextChanged(Editable editable) {
            String obj;
            if (BaseContactFragment.this.isVisible()) {
                String str = "";
                if (editable != null && (obj = editable.toString()) != null) {
                    str = obj;
                }
                s44 s44Var = s44.a;
                if (s44Var.f(str)) {
                    if (s44Var.i(str)) {
                        BaseContactFragment.this.K(str);
                    } else if (str.length() > 42) {
                        BaseContactFragment.this.C().f.setError(BaseContactFragment.this.getResources().getString(R.string.error_wrong_address));
                    }
                } else {
                    if (str.length() > 0) {
                        BaseContactFragment.this.C().f.setError(BaseContactFragment.this.getResources().getString(R.string.error_wrong_address));
                    }
                }
                BaseContactFragment.this.e0();
                BaseContactFragment.this.G((editable == null ? 0 : editable.length()) == 0);
            }
        }

        @Override // defpackage.vu0, android.text.TextWatcher
        public void beforeTextChanged(CharSequence charSequence, int i, int i2, int i3) {
            if (BaseContactFragment.this.isVisible()) {
                BaseContactFragment.this.C().f.setError(null);
            }
        }
    }

    public static final void I(final BaseContactFragment baseContactFragment, List list) {
        fs1.f(baseContactFragment, "this$0");
        if ((list == null ? b20.g() : list).size() == b30.a.a().size()) {
            new Handler(Looper.getMainLooper()).post(new Runnable() { // from class: tm
                @Override // java.lang.Runnable
                public final void run() {
                    BaseContactFragment.J(BaseContactFragment.this);
                }
            });
            return;
        }
        gb2<List<TokenType>> m = baseContactFragment.D().m();
        if (list == null) {
            list = b20.g();
        }
        m.postValue(list);
    }

    public static final void J(BaseContactFragment baseContactFragment) {
        fs1.f(baseContactFragment, "this$0");
        if (baseContactFragment.isVisible()) {
            baseContactFragment.C().l.setChecked(true);
        }
    }

    public static final void P(BaseContactFragment baseContactFragment, View view) {
        fs1.f(baseContactFragment, "this$0");
        baseContactFragment.f();
    }

    public static final void Q(BaseContactFragment baseContactFragment, View view) {
        fs1.f(baseContactFragment, "this$0");
        baseContactFragment.R();
    }

    public static final void U(BaseContactFragment baseContactFragment, List list) {
        fs1.f(baseContactFragment, "this$0");
        baseContactFragment.C().e.removeAllViews();
        if (list == null) {
            return;
        }
        Iterator it = list.iterator();
        while (it.hasNext()) {
            TokenType tokenType = (TokenType) it.next();
            us1 a2 = us1.a(LayoutInflater.from(baseContactFragment.requireContext()).inflate(R.layout.item_chain_network_vertical, (ViewGroup) null, false));
            fs1.e(a2, "bind(network)");
            a2.b().setTag(Integer.valueOf(tokenType.getChainId()));
            com.bumptech.glide.a.t(baseContactFragment.requireContext()).w(Integer.valueOf(tokenType.getIcon())).I0(a2.b);
            baseContactFragment.C().e.addView(a2.b());
        }
    }

    public static final void X(BaseContactFragment baseContactFragment, View view) {
        fs1.f(baseContactFragment, "this$0");
        if (baseContactFragment instanceof AddContactFragment) {
            TokenTypes tokenTypes = new TokenTypes();
            List<TokenType> value = baseContactFragment.D().m().getValue();
            if (value == null) {
                value = b20.g();
            }
            tokenTypes.addAll(value);
            te4 te4Var = te4.a;
            l8.b a2 = l8.a(tokenTypes);
            fs1.e(a2, "actionAddContactFragment…value?: emptyList() ) } )");
            baseContactFragment.g(a2);
        }
        if (baseContactFragment instanceof EditContactFragment) {
            TokenTypes tokenTypes2 = new TokenTypes();
            List<TokenType> value2 = baseContactFragment.D().m().getValue();
            if (value2 == null) {
                value2 = b20.g();
            }
            tokenTypes2.addAll(value2);
            te4 te4Var2 = te4.a;
            gu0.b a3 = gu0.a(tokenTypes2);
            fs1.e(a3, "actionEditContactFragmen….value?: emptyList() ) })");
            baseContactFragment.g(a3);
        }
    }

    public static final void Y(BaseContactFragment baseContactFragment, CompoundButton compoundButton, boolean z) {
        fs1.f(baseContactFragment, "this$0");
        baseContactFragment.C().m.setEnabled(!z);
        if (z) {
            baseContactFragment.D().m().postValue(b30.a.a());
        } else {
            baseContactFragment.D().m().postValue(b20.g());
        }
    }

    public static final void Z(BaseContactFragment baseContactFragment, View view) {
        fs1.f(baseContactFragment, "this$0");
        baseContactFragment.L();
    }

    public static final void a0(BaseContactFragment baseContactFragment, View view) {
        fs1.f(baseContactFragment, "this$0");
        baseContactFragment.N();
    }

    public static final void b0(BaseContactFragment baseContactFragment, View view) {
        fs1.f(baseContactFragment, "this$0");
        baseContactFragment.O();
    }

    public static final void c0(BaseContactFragment baseContactFragment, View view) {
        hn2 a2;
        w7<Intent> b2;
        fs1.f(baseContactFragment, "this$0");
        gm1 j = baseContactFragment.j();
        if (j == null || (a2 = j.a()) == null || (b2 = a2.b(new BaseContactFragment$setupView$6$1(baseContactFragment))) == null) {
            return;
        }
        b2.a(new Intent(baseContactFragment.requireActivity(), ScannedCodeActivity.class));
    }

    public static final void d0(BaseContactFragment baseContactFragment, View view) {
        fs1.f(baseContactFragment, "this$0");
        baseContactFragment.C().f.setText("");
    }

    public final o91 C() {
        o91 o91Var = this.i0;
        if (o91Var != null) {
            return o91Var;
        }
        fs1.r("binding");
        return null;
    }

    public final ContactViewModel D() {
        return (ContactViewModel) this.l0.getValue();
    }

    public final int E() {
        return this.j0;
    }

    public final ClipboardManager F() {
        return (ClipboardManager) this.k0.getValue();
    }

    public final void G(boolean z) {
        C().b.setVisibility(e30.m0(z));
        C().c.setVisibility(e30.m0(z));
        C().j.setVisibility(e30.k0(z));
        C().h.setVisibility(e30.k0(z));
    }

    public final void H() {
        gb2 b2;
        xd2 h = ka1.a(this).h();
        ec3 d = h == null ? null : h.d();
        if (d == null || (b2 = d.b("RESULT_SELECTED_CHAIN")) == null) {
            return;
        }
        b2.observe(h, new tl2() { // from class: um
            @Override // defpackage.tl2
            public final void onChanged(Object obj) {
                BaseContactFragment.I(BaseContactFragment.this, (List) obj);
            }
        });
    }

    public abstract void K(String str);

    public abstract void L();

    public abstract void M(String str);

    public final void N() {
        Editable text = C().f.getText();
        fs1.e(text, "binding.etContactAddress.text");
        if (text.length() > 0) {
            F().setPrimaryClip(ClipData.newPlainText("label", C().f.getText()));
            Context requireContext = requireContext();
            fs1.e(requireContext, "requireContext()");
            e30.Z(requireContext, R.string.copied_to_clipboard);
        }
    }

    public final void O() {
        ClipData primaryClip = F().getPrimaryClip();
        if (primaryClip == null || primaryClip.getItemCount() <= 0 || primaryClip.getItemAt(0).getText() == null) {
            return;
        }
        C().f.setText(primaryClip.getItemAt(0).getText().toString());
    }

    public abstract void R();

    public final void S(o91 o91Var) {
        fs1.f(o91Var, "<set-?>");
        this.i0 = o91Var;
    }

    public final void T() {
        C().e.removeAllViews();
        D().m().observe(getViewLifecycleOwner(), new tl2() { // from class: rm
            @Override // defpackage.tl2
            public final void onChanged(Object obj) {
                BaseContactFragment.U(BaseContactFragment.this, (List) obj);
            }
        });
    }

    public abstract void V();

    public final void W() {
        C().d.setEnabled(false);
        C().g.addTextChangedListener(new z44(new a(), 0L, 2, null));
        C().f.addTextChangedListener(new z44(new b(), 0L, 2, null));
        C().i.setOnClickListener(new View.OnClickListener() { // from class: vm
            @Override // android.view.View.OnClickListener
            public final void onClick(View view) {
                BaseContactFragment.Z(BaseContactFragment.this, view);
            }
        });
        C().j.setOnClickListener(new View.OnClickListener() { // from class: ym
            @Override // android.view.View.OnClickListener
            public final void onClick(View view) {
                BaseContactFragment.a0(BaseContactFragment.this, view);
            }
        });
        C().b.setOnClickListener(new View.OnClickListener() { // from class: an
            @Override // android.view.View.OnClickListener
            public final void onClick(View view) {
                BaseContactFragment.b0(BaseContactFragment.this, view);
            }
        });
        C().c.setOnClickListener(new View.OnClickListener() { // from class: wm
            @Override // android.view.View.OnClickListener
            public final void onClick(View view) {
                BaseContactFragment.c0(BaseContactFragment.this, view);
            }
        });
        C().h.setOnClickListener(new View.OnClickListener() { // from class: bn
            @Override // android.view.View.OnClickListener
            public final void onClick(View view) {
                BaseContactFragment.d0(BaseContactFragment.this, view);
            }
        });
        AppCompatImageView appCompatImageView = C().j;
        Context requireContext = requireContext();
        fs1.e(requireContext, "requireContext()");
        appCompatImageView.setImageDrawable(new ImageUtility(requireContext).c(g83.f(getResources(), R.drawable.ic_baseline_file_copy_24, null), R.color.dark_grey));
        C().m.setOnClickListener(new View.OnClickListener() { // from class: cn
            @Override // android.view.View.OnClickListener
            public final void onClick(View view) {
                BaseContactFragment.X(BaseContactFragment.this, view);
            }
        });
        C().l.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() { // from class: sm
            @Override // android.widget.CompoundButton.OnCheckedChangeListener
            public final void onCheckedChanged(CompoundButton compoundButton, boolean z) {
                BaseContactFragment.Y(BaseContactFragment.this, compoundButton, z);
            }
        });
        T();
    }

    public final void e0() {
        MaterialButton materialButton = C().d;
        s44 s44Var = s44.a;
        materialButton.setEnabled(s44Var.c(C().g.getText().toString()) && s44Var.i(C().f.getText().toString()));
    }

    @Override // androidx.fragment.app.Fragment
    public View onCreateView(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle) {
        fs1.f(layoutInflater, "inflater");
        o91 a2 = o91.a(layoutInflater.inflate(R.layout.fragment_add_edit_contact, viewGroup, false));
        fs1.e(a2, "bind(\n            inflat…e\n            )\n        )");
        S(a2);
        return C().b();
    }

    @Override // net.safemoon.androidwallet.fragments.common.BaseMainFragment, defpackage.qn, androidx.fragment.app.Fragment
    public void onViewCreated(View view, Bundle bundle) {
        fs1.f(view, "view");
        super.onViewCreated(view, bundle);
        V();
        W();
        H();
        C().n.c.setOnClickListener(new View.OnClickListener() { // from class: xm
            @Override // android.view.View.OnClickListener
            public final void onClick(View view2) {
                BaseContactFragment.P(BaseContactFragment.this, view2);
            }
        });
        C().d.setOnClickListener(new View.OnClickListener() { // from class: zm
            @Override // android.view.View.OnClickListener
            public final void onClick(View view2) {
                BaseContactFragment.Q(BaseContactFragment.this, view2);
            }
        });
    }
}
