package net.safemoon.androidwallet.fragments;

import android.content.DialogInterface;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CompoundButton;
import androidx.lifecycle.l;
import java.util.List;
import java.util.Locale;
import net.safemoon.androidwallet.MyApplicationClass;
import net.safemoon.androidwallet.R;
import net.safemoon.androidwallet.activity.AKTHomeActivity;
import net.safemoon.androidwallet.activity.AKTLoginActivity;
import net.safemoon.androidwallet.database.mainRoom.MainRoomDatabase;
import net.safemoon.androidwallet.fragments.SettingsFragment;
import net.safemoon.androidwallet.fragments.common.BaseMainFragment;
import net.safemoon.androidwallet.model.wallets.Wallet;
import net.safemoon.androidwallet.viewmodels.MultiWalletViewModel;
import zendesk.configurations.Configuration;
import zendesk.support.requestlist.RequestListActivity;

/* loaded from: classes2.dex */
public class SettingsFragment extends BaseMainFragment implements View.OnClickListener {
    public cb1 i0;
    public MultiWalletViewModel j0;
    public Wallet k0;

    /* loaded from: classes2.dex */
    public class a implements CompoundButton.OnCheckedChangeListener {
        public a() {
        }

        @Override // android.widget.CompoundButton.OnCheckedChangeListener
        public void onCheckedChanged(CompoundButton compoundButton, boolean z) {
            if (z) {
                androidx.appcompat.app.b.G(2);
                bo3.l(SettingsFragment.this.getContext(), "MODE_NIGHT", 2);
                return;
            }
            androidx.appcompat.app.b.G(1);
            bo3.l(SettingsFragment.this.getContext(), "MODE_NIGHT", 1);
        }
    }

    /* JADX INFO: Access modifiers changed from: private */
    public /* synthetic */ void u(DialogInterface dialogInterface, int i) {
        AKTLoginActivity.r0.b(requireContext(), false);
        MyApplicationClass.c().h0 = true;
        if (getActivity() != null) {
            getActivity().finish();
        }
    }

    /* JADX INFO: Access modifiers changed from: private */
    public /* synthetic */ void w(View view) {
        cz0.c(requireContext(), new DialogInterface.OnClickListener() { // from class: hn3
            @Override // android.content.DialogInterface.OnClickListener
            public final void onClick(DialogInterface dialogInterface, int i) {
                SettingsFragment.this.u(dialogInterface, i);
            }
        }, in3.a).show();
    }

    /* JADX INFO: Access modifiers changed from: private */
    public /* synthetic */ void x(Wallet wallet2, CompoundButton compoundButton, boolean z) {
        Wallet wallet3;
        do3.a.g(requireActivity(), z);
        if (z || wallet2 == null || !wallet2.isPrimaryWallet() || (wallet3 = this.k0) == null) {
            return;
        }
        this.j0.A(wallet3);
        AKTHomeActivity.R0(requireActivity());
    }

    /* JADX INFO: Access modifiers changed from: private */
    public /* synthetic */ void y(final Wallet wallet2, List list) {
        this.k0 = null;
        if (list.size() > 1) {
            this.i0.f.setVisibility(0);
            this.i0.l.setVisibility(0);
            this.i0.l.setChecked(do3.a.c(requireActivity()));
            this.k0 = (Wallet) list.get(1);
            this.i0.l.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() { // from class: kn3
                @Override // android.widget.CompoundButton.OnCheckedChangeListener
                public final void onCheckedChanged(CompoundButton compoundButton, boolean z) {
                    SettingsFragment.this.x(wallet2, compoundButton, z);
                }
            });
            return;
        }
        this.i0.f.setVisibility(8);
        this.i0.l.setVisibility(8);
    }

    /* JADX INFO: Access modifiers changed from: private */
    public /* synthetic */ void z(List list) {
        this.i0.h.setVisibility(e30.m0(list != null && list.size() > 0));
    }

    public final void A() {
        this.i0.p.setText("V3.33");
    }

    public final void B() {
        this.i0.k.setOnClickListener(this);
        this.i0.n.setOnClickListener(this);
        this.i0.v.setOnClickListener(this);
        this.i0.s.setOnClickListener(this);
        this.i0.r.setOnClickListener(this);
        this.i0.r.setOnClickListener(this);
        this.i0.u.setOnClickListener(this);
        this.i0.b.setOnClickListener(this);
        this.i0.c.setOnClickListener(this);
        this.i0.e.setOnClickListener(this);
        this.i0.d.setOnClickListener(this);
        this.i0.t.setOnClickListener(this);
        this.i0.p.setOnClickListener(this);
        this.i0.q.setOnClickListener(this);
        this.i0.g.setOnClickListener(this);
        this.i0.i.setOnClickListener(this);
        this.i0.o.c.setText(getText(R.string.setting));
        this.i0.o.a.setVisibility(8);
        this.i0.m.setChecked(androidx.appcompat.app.b.l() == 2);
        this.i0.m.setOnCheckedChangeListener(new a());
        final Wallet c = e30.c(requireContext());
        this.j0.t().observe(getViewLifecycleOwner(), new tl2() { // from class: gn3
            @Override // defpackage.tl2
            public final void onChanged(Object obj) {
                SettingsFragment.this.y(c, (List) obj);
            }
        });
        MainRoomDatabase.n.b(requireContext()).N().c().observe(getViewLifecycleOwner(), new tl2() { // from class: fn3
            @Override // defpackage.tl2
            public final void onChanged(Object obj) {
                SettingsFragment.this.z((List) obj);
            }
        });
        if (getArguments() != null && getArguments().containsKey("isNotification") && getArguments().getBoolean("isNotification")) {
            this.i0.r.performClick();
        }
        A();
        if (r44.b(Locale.getDefault()) == 1) {
            this.i0.g.setGravity(8388629);
        }
    }

    @Override // android.view.View.OnClickListener
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.btnDefaultCurrency /* 2131362053 */:
                g(c.d());
                return;
            case R.id.btnDefaultDateFormat /* 2131362054 */:
                g(c.a());
                return;
            case R.id.btnDefaultLanguage /* 2131362055 */:
                g(c.b());
                return;
            case R.id.btnDefaultScreen /* 2131362056 */:
                g(c.c());
                return;
            case R.id.btn_wallet_connect /* 2131362132 */:
                g(c.k());
                return;
            case R.id.joinCommunityButton /* 2131362715 */:
                g(c.e());
                return;
            case R.id.privacy /* 2131362971 */:
                g(c.j("https://safemoon.com/legal/wallet/privacy", R.string.privacy_policy));
                return;
            case R.id.terms /* 2131363246 */:
                g(c.j("https://safemoon.com/legal/wallet/eula", R.string.terms_of_service));
                return;
            case R.id.tv_my_contacts /* 2131363484 */:
                g(c.f());
                return;
            case R.id.tv_notification /* 2131363490 */:
                g(c.g());
                return;
            case R.id.tv_security /* 2131363501 */:
                g(c.h());
                return;
            case R.id.tv_switch_wallet /* 2131363507 */:
                g(c.i());
                return;
            case R.id.zendeskSupport /* 2131363787 */:
                RequestListActivity.builder().withContactUsButtonVisible(true).show(requireActivity(), new Configuration[0]);
                return;
            default:
                return;
        }
    }

    @Override // androidx.fragment.app.Fragment
    public View onCreateView(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle) {
        this.i0 = cb1.a(layoutInflater.inflate(R.layout.fragment_setting, viewGroup, false));
        this.j0 = (MultiWalletViewModel) new l(requireActivity()).a(MultiWalletViewModel.class);
        return this.i0.b();
    }

    @Override // androidx.fragment.app.Fragment
    public void onStart() {
        super.onStart();
    }

    @Override // net.safemoon.androidwallet.fragments.common.BaseMainFragment, defpackage.qn, androidx.fragment.app.Fragment
    public void onViewCreated(View view, Bundle bundle) {
        super.onViewCreated(view, bundle);
        this.i0.j.setOnClickListener(new View.OnClickListener() { // from class: jn3
            @Override // android.view.View.OnClickListener
            public final void onClick(View view2) {
                SettingsFragment.this.w(view2);
            }
        });
        B();
    }
}
