package net.safemoon.androidwallet.fragments;

import kotlin.jvm.internal.Lambda;
import net.safemoon.androidwallet.viewmodels.MyTokensListViewModel;

/* compiled from: SwapMigrationFragment.kt */
/* loaded from: classes2.dex */
public final class SwapMigrationFragment$bind$2 extends Lambda implements rc1<te4> {
    public final /* synthetic */ SwapMigrationFragment this$0;

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public SwapMigrationFragment$bind$2(SwapMigrationFragment swapMigrationFragment) {
        super(0);
        this.this$0 = swapMigrationFragment;
    }

    @Override // defpackage.rc1
    public /* bridge */ /* synthetic */ te4 invoke() {
        invoke2();
        return te4.a;
    }

    @Override // defpackage.rc1
    /* renamed from: invoke  reason: avoid collision after fix types in other method */
    public final void invoke2() {
        MyTokensListViewModel t0;
        MyTokensListViewModel t02;
        t0 = this.this$0.t0();
        t0.Q();
        t02 = this.this$0.t0();
        t02.P();
        this.this$0.u0().v0();
    }
}
