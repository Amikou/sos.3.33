package net.safemoon.androidwallet.fragments;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebResourceError;
import android.webkit.WebResourceRequest;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import androidx.constraintlayout.widget.ConstraintLayout;
import net.safemoon.androidwallet.R;
import net.safemoon.androidwallet.fragments.TermScreenWebViewFragment;

/* compiled from: TermScreenWebViewFragment.kt */
/* loaded from: classes2.dex */
public final class TermScreenWebViewFragment extends qn {
    public fb1 f0;
    public int g0;
    public String h0 = "";
    public boolean i0;

    /* compiled from: TermScreenWebViewFragment.kt */
    /* loaded from: classes2.dex */
    public static final class a {
        public a() {
        }

        public /* synthetic */ a(qi0 qi0Var) {
            this();
        }
    }

    /* compiled from: TermScreenWebViewFragment.kt */
    /* loaded from: classes2.dex */
    public static final class b extends WebViewClient {
        public b() {
        }

        @Override // android.webkit.WebViewClient
        public void onPageCommitVisible(WebView webView, String str) {
            super.onPageCommitVisible(webView, str);
            TermScreenWebViewFragment.this.i0 = true;
            super.onPageFinished(webView, str);
            fb1 fb1Var = TermScreenWebViewFragment.this.f0;
            ProgressBar progressBar = fb1Var == null ? null : fb1Var.d;
            if (progressBar != null) {
                progressBar.setVisibility(8);
            }
            fb1 fb1Var2 = TermScreenWebViewFragment.this.f0;
            LinearLayout linearLayout = fb1Var2 == null ? null : fb1Var2.c;
            if (linearLayout != null) {
                linearLayout.setVisibility(8);
            }
            fb1 fb1Var3 = TermScreenWebViewFragment.this.f0;
            WebView webView2 = fb1Var3 != null ? fb1Var3.f : null;
            if (webView2 == null) {
                return;
            }
            webView2.setVisibility(0);
        }

        @Override // android.webkit.WebViewClient
        public void onReceivedError(WebView webView, WebResourceRequest webResourceRequest, WebResourceError webResourceError) {
            super.onReceivedError(webView, webResourceRequest, webResourceError);
            if (TermScreenWebViewFragment.this.i0) {
                return;
            }
            fb1 fb1Var = TermScreenWebViewFragment.this.f0;
            LinearLayout linearLayout = fb1Var == null ? null : fb1Var.c;
            if (linearLayout != null) {
                linearLayout.setVisibility(0);
            }
            fb1 fb1Var2 = TermScreenWebViewFragment.this.f0;
            ProgressBar progressBar = fb1Var2 == null ? null : fb1Var2.d;
            if (progressBar != null) {
                progressBar.setVisibility(8);
            }
            fb1 fb1Var3 = TermScreenWebViewFragment.this.f0;
            WebView webView2 = fb1Var3 != null ? fb1Var3.f : null;
            if (webView2 == null) {
                return;
            }
            webView2.setVisibility(8);
        }

        @Override // android.webkit.WebViewClient
        public boolean shouldOverrideUrlLoading(WebView webView, WebResourceRequest webResourceRequest) {
            return true;
        }
    }

    static {
        new a(null);
    }

    public static final void p(TermScreenWebViewFragment termScreenWebViewFragment, View view) {
        fs1.f(termScreenWebViewFragment, "this$0");
        termScreenWebViewFragment.f();
    }

    public static final void r(TermScreenWebViewFragment termScreenWebViewFragment, View view) {
        fs1.f(termScreenWebViewFragment, "this$0");
        fb1 fb1Var = termScreenWebViewFragment.f0;
        LinearLayout linearLayout = fb1Var == null ? null : fb1Var.c;
        if (linearLayout != null) {
            linearLayout.setVisibility(8);
        }
        fb1 fb1Var2 = termScreenWebViewFragment.f0;
        ProgressBar progressBar = fb1Var2 != null ? fb1Var2.d : null;
        if (progressBar != null) {
            progressBar.setVisibility(0);
        }
        termScreenWebViewFragment.n();
    }

    public final void n() {
        WebView webView;
        WebView webView2;
        fb1 fb1Var = this.f0;
        if (fb1Var != null && (webView2 = fb1Var.f) != null) {
            webView2.loadUrl(this.h0);
        }
        try {
            fb1 fb1Var2 = this.f0;
            if (fb1Var2 != null && (webView = fb1Var2.f) != null) {
                webView.loadUrl(this.h0);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public final void o() {
        fb1 fb1Var = this.f0;
        fs1.d(fb1Var);
        fb1Var.e.a.setOnClickListener(new View.OnClickListener() { // from class: z34
            @Override // android.view.View.OnClickListener
            public final void onClick(View view) {
                TermScreenWebViewFragment.p(TermScreenWebViewFragment.this, view);
            }
        });
        fb1 fb1Var2 = this.f0;
        fs1.d(fb1Var2);
        fb1Var2.e.c.setText(getResources().getText(this.g0));
    }

    @Override // androidx.fragment.app.Fragment
    public View onCreateView(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle) {
        fs1.f(layoutInflater, "inflater");
        this.f0 = fb1.a(layoutInflater.inflate(R.layout.fragment_term_screen_web_view, viewGroup, false));
        if (getArguments() != null && requireArguments().get("url") != null && requireArguments().get("titleResId") != null && requireArguments().get("withBottomPadding") != null) {
            this.g0 = requireArguments().getInt("titleResId");
            String string = requireArguments().getString("url");
            fs1.d(string);
            fs1.e(string, "requireArguments().getString(BUNDLE_KEY_URL)!!");
            this.h0 = string;
            requireArguments().getBoolean("withBottomPadding");
            fb1 fb1Var = this.f0;
            fs1.d(fb1Var);
            ConstraintLayout b2 = fb1Var.b();
            fs1.e(b2, "binding!!.root");
            return b2;
        }
        throw new NullPointerException("Arguments shouldn't be null");
    }

    @Override // defpackage.qn, androidx.fragment.app.Fragment
    public void onViewCreated(View view, Bundle bundle) {
        fs1.f(view, "view");
        try {
            super.onViewCreated(view, bundle);
        } catch (Exception e) {
            e.printStackTrace();
        }
        o();
        q();
        n();
    }

    public final void q() {
        fb1 fb1Var = this.f0;
        fs1.d(fb1Var);
        WebView webView = fb1Var.f;
        webView.getSettings().setJavaScriptEnabled(true);
        webView.getSettings().setSupportZoom(false);
        webView.getSettings().setJavaScriptCanOpenWindowsAutomatically(false);
        webView.getSettings().setLoadWithOverviewMode(true);
        webView.getSettings().setUseWideViewPort(true);
        webView.setWebViewClient(new b());
        fb1 fb1Var2 = this.f0;
        fs1.d(fb1Var2);
        fb1Var2.b.setOnClickListener(new View.OnClickListener() { // from class: a44
            @Override // android.view.View.OnClickListener
            public final void onClick(View view) {
                TermScreenWebViewFragment.r(TermScreenWebViewFragment.this, view);
            }
        });
    }
}
