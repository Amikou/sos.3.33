package net.safemoon.androidwallet.fragments;

import android.graphics.Bitmap;
import android.os.Bundle;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import androidx.fragment.app.FragmentViewModelLazyKt;
import androidx.navigation.NavController;
import net.safemoon.androidwallet.R;
import net.safemoon.androidwallet.common.TokenType;
import net.safemoon.androidwallet.fragments.AddCustomContractFragment;
import net.safemoon.androidwallet.fragments.common.BaseMainFragment;
import net.safemoon.androidwallet.model.cmcTokenInfo.TokenDetail;
import net.safemoon.androidwallet.model.transaction.history.Result;
import net.safemoon.androidwallet.model.transaction.history.TransactionHistoryModel;
import net.safemoon.androidwallet.viewmodels.AddNewTokensViewModel;
import net.safemoon.androidwallet.viewmodels.CustomContractTokenViewModel;

/* compiled from: AddCustomContractFragment.kt */
/* loaded from: classes2.dex */
public final class AddCustomContractFragment extends BaseMainFragment {
    public final sy1 i0 = FragmentViewModelLazyKt.a(this, d53.b(CustomContractTokenViewModel.class), new AddCustomContractFragment$special$$inlined$viewModels$default$2(new AddCustomContractFragment$special$$inlined$viewModels$default$1(this)), new AddCustomContractFragment$customContractTokenViewModel$2(this));
    public final sy1 j0 = FragmentViewModelLazyKt.a(this, d53.b(AddNewTokensViewModel.class), new AddCustomContractFragment$special$$inlined$viewModels$default$4(new AddCustomContractFragment$special$$inlined$viewModels$default$3(this)), new AddCustomContractFragment$addNewTokensViewModel$2(this));
    public n91 k0;

    /* compiled from: TextView.kt */
    /* loaded from: classes2.dex */
    public static final class a implements TextWatcher {
        public final /* synthetic */ n91 f0;

        public a(n91 n91Var) {
            this.f0 = n91Var;
        }

        /* JADX WARN: Removed duplicated region for block: B:17:0x0035  */
        /* JADX WARN: Removed duplicated region for block: B:26:0x004f  */
        /* JADX WARN: Removed duplicated region for block: B:27:0x0059  */
        @Override // android.text.TextWatcher
        /*
            Code decompiled incorrectly, please refer to instructions dump.
            To view partially-correct code enable 'Show inconsistent code' option in preferences
        */
        public void afterTextChanged(android.text.Editable r6) {
            /*
                r5 = this;
                java.lang.String r6 = java.lang.String.valueOf(r6)
                net.safemoon.androidwallet.fragments.AddCustomContractFragment r0 = net.safemoon.androidwallet.fragments.AddCustomContractFragment.this
                net.safemoon.androidwallet.viewmodels.CustomContractTokenViewModel r0 = net.safemoon.androidwallet.fragments.AddCustomContractFragment.w(r0)
                net.safemoon.androidwallet.common.TokenType r0 = r0.r()
                if (r0 != 0) goto L11
                goto L62
            L11:
                boolean r0 = defpackage.hx.a(r6, r0)
                n91 r1 = r5.f0
                net.safemoon.androidwallet.views.MyTextInputLayout r1 = r1.f
                r2 = 0
                r3 = 1
                if (r0 == 0) goto L2b
                int r4 = r6.length()
                if (r4 != 0) goto L25
                r4 = r3
                goto L26
            L25:
                r4 = r2
            L26:
                if (r4 == 0) goto L29
                goto L2b
            L29:
                r4 = r2
                goto L2c
            L2b:
                r4 = r3
            L2c:
                r1.setErrorEnabled(r4)
                n91 r1 = r5.f0
                net.safemoon.androidwallet.views.MyTextInputLayout r1 = r1.f
                if (r0 != 0) goto L49
                int r4 = r6.length()
                if (r4 != 0) goto L3c
                r2 = r3
            L3c:
                if (r2 == 0) goto L3f
                goto L49
            L3f:
                net.safemoon.androidwallet.fragments.AddCustomContractFragment r2 = net.safemoon.androidwallet.fragments.AddCustomContractFragment.this
                r3 = 2131951986(0x7f130172, float:1.9540402E38)
                java.lang.String r2 = r2.getString(r3)
                goto L4a
            L49:
                r2 = 0
            L4a:
                r1.setError(r2)
                if (r0 == 0) goto L59
                net.safemoon.androidwallet.fragments.AddCustomContractFragment r0 = net.safemoon.androidwallet.fragments.AddCustomContractFragment.this
                net.safemoon.androidwallet.viewmodels.CustomContractTokenViewModel r0 = net.safemoon.androidwallet.fragments.AddCustomContractFragment.w(r0)
                r0.l(r6)
                goto L62
            L59:
                net.safemoon.androidwallet.fragments.AddCustomContractFragment r6 = net.safemoon.androidwallet.fragments.AddCustomContractFragment.this
                net.safemoon.androidwallet.viewmodels.CustomContractTokenViewModel r6 = net.safemoon.androidwallet.fragments.AddCustomContractFragment.w(r6)
                r6.h()
            L62:
                return
            */
            throw new UnsupportedOperationException("Method not decompiled: net.safemoon.androidwallet.fragments.AddCustomContractFragment.a.afterTextChanged(android.text.Editable):void");
        }

        @Override // android.text.TextWatcher
        public void beforeTextChanged(CharSequence charSequence, int i, int i2, int i3) {
        }

        @Override // android.text.TextWatcher
        public void onTextChanged(CharSequence charSequence, int i, int i2, int i3) {
        }
    }

    public static final void A(AddCustomContractFragment addCustomContractFragment, Integer num) {
        fs1.f(addCustomContractFragment, "this$0");
        if (num != null) {
            addCustomContractFragment.z().q().postValue(TokenType.Companion.b(num.intValue()));
        }
    }

    public static final void B(AddCustomContractFragment addCustomContractFragment, View view) {
        fs1.f(addCustomContractFragment, "this$0");
        addCustomContractFragment.f();
    }

    public static final void C(n91 n91Var, AddCustomContractFragment addCustomContractFragment, TokenDetail tokenDetail) {
        fs1.f(n91Var, "$this_apply");
        fs1.f(addCustomContractFragment, "this$0");
        if (tokenDetail != null) {
            k73 u = com.bumptech.glide.a.u(n91Var.d);
            Integer num = tokenDetail.id;
            fs1.e(num, "it.id");
            u.x(a4.g(num.intValue(), tokenDetail.symbol, addCustomContractFragment.z().r())).d0(200, 200).a(n73.v0()).I0(n91Var.d);
            return;
        }
        n91Var.d.setImageResource(R.drawable.ic_default_empty_coin);
    }

    public static final void D(n91 n91Var, Bitmap bitmap) {
        fs1.f(n91Var, "$this_apply");
        if (bitmap == null) {
            return;
        }
        com.bumptech.glide.a.u(n91Var.d).t(bitmap).e().I0(n91Var.d);
    }

    /* JADX WARN: Code restructure failed: missing block: B:76:0x0132, code lost:
        if ((r9.length() == 0) == true) goto L32;
     */
    /* JADX WARN: Removed duplicated region for block: B:48:0x00d6  */
    /* JADX WARN: Removed duplicated region for block: B:49:0x00e4  */
    /* JADX WARN: Removed duplicated region for block: B:65:0x010a  */
    /* JADX WARN: Removed duplicated region for block: B:66:0x0118  */
    /*
        Code decompiled incorrectly, please refer to instructions dump.
        To view partially-correct code enable 'Show inconsistent code' option in preferences
    */
    public static final void E(net.safemoon.androidwallet.fragments.AddCustomContractFragment r7, defpackage.n91 r8, android.view.View r9) {
        /*
            Method dump skipped, instructions count: 422
            To view this dump change 'Code comments level' option to 'DEBUG'
        */
        throw new UnsupportedOperationException("Method not decompiled: net.safemoon.androidwallet.fragments.AddCustomContractFragment.E(net.safemoon.androidwallet.fragments.AddCustomContractFragment, n91, android.view.View):void");
    }

    public static final void F(n91 n91Var, AddCustomContractFragment addCustomContractFragment, TokenType tokenType) {
        fs1.f(n91Var, "$this_apply");
        fs1.f(addCustomContractFragment, "this$0");
        if (tokenType == null) {
            return;
        }
        n91Var.a.setText(tokenType.getTitle());
        com.bumptech.glide.a.u(n91Var.c).w(Integer.valueOf(tokenType.getIcon())).d0(200, 200).a(n73.v0()).I0(n91Var.c);
        addCustomContractFragment.y().l(tokenType);
        EditText editText = n91Var.f.getEditText();
        if (editText == null) {
            return;
        }
        editText.setText("");
    }

    public static final void G(AddCustomContractFragment addCustomContractFragment, View view) {
        fs1.f(addCustomContractFragment, "this$0");
        ce2 a2 = v8.a();
        fs1.e(a2, "actionAddCustomContractF…tToChainNetworkFragment()");
        addCustomContractFragment.g(a2);
    }

    public static final void H(AddCustomContractFragment addCustomContractFragment, n91 n91Var, TransactionHistoryModel transactionHistoryModel) {
        fs1.f(addCustomContractFragment, "this$0");
        fs1.f(n91Var, "$this_apply");
        if (transactionHistoryModel != null) {
            Result m = addCustomContractFragment.z().m();
            if (m == null) {
                return;
            }
            EditText editText = n91Var.h.getEditText();
            if (editText != null) {
                editText.setEnabled(false);
            }
            EditText editText2 = n91Var.i.getEditText();
            if (editText2 != null) {
                editText2.setEnabled(false);
            }
            EditText editText3 = n91Var.g.getEditText();
            if (editText3 != null) {
                editText3.setEnabled(false);
            }
            EditText editText4 = n91Var.h.getEditText();
            if (editText4 != null) {
                editText4.setText(m.tokenName);
            }
            EditText editText5 = n91Var.i.getEditText();
            if (editText5 != null) {
                editText5.setText(m.tokenSymbol);
            }
            EditText editText6 = n91Var.g.getEditText();
            if (editText6 == null) {
                return;
            }
            editText6.setText(String.valueOf(m.tokenDecimal));
            return;
        }
        n91Var.h.setEnabled(true);
        n91Var.i.setEnabled(true);
        n91Var.g.setEnabled(true);
        EditText editText7 = n91Var.h.getEditText();
        if (editText7 != null) {
            editText7.setText("");
        }
        EditText editText8 = n91Var.i.getEditText();
        if (editText8 != null) {
            editText8.setText("");
        }
        EditText editText9 = n91Var.g.getEditText();
        if (editText9 == null) {
            return;
        }
        editText9.setText("");
    }

    @Override // androidx.fragment.app.Fragment
    public View onCreateView(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle) {
        fs1.f(layoutInflater, "inflater");
        return layoutInflater.inflate(R.layout.fragment_add_custom_contract, viewGroup, false);
    }

    @Override // net.safemoon.androidwallet.fragments.common.BaseMainFragment, defpackage.qn, androidx.fragment.app.Fragment
    public void onViewCreated(View view, Bundle bundle) {
        xd2 h;
        ec3 d;
        gb2 b;
        fs1.f(view, "view");
        super.onViewCreated(view, bundle);
        n91 a2 = n91.a(view);
        fs1.e(a2, "bind(view)");
        this.k0 = a2;
        NavController e = e();
        if (e != null && (h = e.h()) != null && (d = h.d()) != null && (b = d.b("bundle.TOKEN_CHAIN_KEY")) != null) {
            b.observe(getViewLifecycleOwner(), new tl2() { // from class: p8
                @Override // defpackage.tl2
                public final void onChanged(Object obj) {
                    AddCustomContractFragment.A(AddCustomContractFragment.this, (Integer) obj);
                }
            });
        }
        final n91 n91Var = this.k0;
        if (n91Var == null) {
            fs1.r("binding");
            n91Var = null;
        }
        n91Var.j.a.setOnClickListener(new View.OnClickListener() { // from class: r8
            @Override // android.view.View.OnClickListener
            public final void onClick(View view2) {
                AddCustomContractFragment.B(AddCustomContractFragment.this, view2);
            }
        });
        n91Var.j.c.setText(R.string.screen_title_cs_contract);
        z().q().observe(getViewLifecycleOwner(), new tl2() { // from class: n8
            @Override // defpackage.tl2
            public final void onChanged(Object obj) {
                AddCustomContractFragment.F(n91.this, this, (TokenType) obj);
            }
        });
        n91Var.e.setOnClickListener(new View.OnClickListener() { // from class: s8
            @Override // android.view.View.OnClickListener
            public final void onClick(View view2) {
                AddCustomContractFragment.G(AddCustomContractFragment.this, view2);
            }
        });
        n91Var.f.setUpDefaultView();
        EditText editText = n91Var.f.getEditText();
        fs1.d(editText);
        fs1.e(editText, "tilContractAddress.editText!!");
        editText.addTextChangedListener(new a(n91Var));
        z().s().observe(getViewLifecycleOwner(), new tl2() { // from class: q8
            @Override // defpackage.tl2
            public final void onChanged(Object obj) {
                AddCustomContractFragment.H(AddCustomContractFragment.this, n91Var, (TransactionHistoryModel) obj);
            }
        });
        z().t().observe(getViewLifecycleOwner(), new tl2() { // from class: o8
            @Override // defpackage.tl2
            public final void onChanged(Object obj) {
                AddCustomContractFragment.C(n91.this, this, (TokenDetail) obj);
            }
        });
        z().o().observe(getViewLifecycleOwner(), new tl2() { // from class: m8
            @Override // defpackage.tl2
            public final void onChanged(Object obj) {
                AddCustomContractFragment.D(n91.this, (Bitmap) obj);
            }
        });
        n91Var.b.setOnClickListener(new View.OnClickListener() { // from class: t8
            @Override // android.view.View.OnClickListener
            public final void onClick(View view2) {
                AddCustomContractFragment.E(AddCustomContractFragment.this, n91Var, view2);
            }
        });
    }

    public final AddNewTokensViewModel y() {
        return (AddNewTokensViewModel) this.j0.getValue();
    }

    public final CustomContractTokenViewModel z() {
        return (CustomContractTokenViewModel) this.i0.getValue();
    }
}
