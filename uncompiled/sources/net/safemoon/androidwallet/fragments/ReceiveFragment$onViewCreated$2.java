package net.safemoon.androidwallet.fragments;

import defpackage.k43;
import kotlin.jvm.internal.Lambda;
import net.safemoon.androidwallet.ui.displayModel.UserTokenItemDisplayModel;

/* compiled from: ReceiveFragment.kt */
/* loaded from: classes2.dex */
public final class ReceiveFragment$onViewCreated$2 extends Lambda implements tc1<UserTokenItemDisplayModel, te4> {
    public final /* synthetic */ ReceiveFragment this$0;

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public ReceiveFragment$onViewCreated$2(ReceiveFragment receiveFragment) {
        super(1);
        this.this$0 = receiveFragment;
    }

    @Override // defpackage.tc1
    public /* bridge */ /* synthetic */ te4 invoke(UserTokenItemDisplayModel userTokenItemDisplayModel) {
        invoke2(userTokenItemDisplayModel);
        return te4.a;
    }

    /* renamed from: invoke  reason: avoid collision after fix types in other method */
    public final void invoke2(UserTokenItemDisplayModel userTokenItemDisplayModel) {
        fs1.f(userTokenItemDisplayModel, "$dstr$symbolWithType$_u24__u24$_u24__u24$_u24__u24$_u24__u24$_u24__u24$chainId");
        String component1 = userTokenItemDisplayModel.component1();
        int component7 = userTokenItemDisplayModel.component7();
        if (um1.a(component1).b()) {
            ReceiveFragment receiveFragment = this.this$0;
            k43.b a = k43.a(component7, bo3.i(receiveFragment.requireActivity(), "SAFEMOON_ADDRESS"));
            fs1.e(a, "actionReceiveFragmentToQ…SS)\n                    )");
            receiveFragment.g(a);
        }
    }
}
