package net.safemoon.androidwallet.fragments;

import android.annotation.SuppressLint;
import android.content.ClipData;
import android.content.ClipboardManager;
import android.graphics.Bitmap;
import android.graphics.Point;
import android.os.Bundle;
import android.view.Display;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import com.google.zxing.WriterException;
import net.safemoon.androidwallet.R;
import net.safemoon.androidwallet.common.TokenType;
import net.safemoon.androidwallet.fragments.QrFragment;
import net.safemoon.androidwallet.fragments.common.BaseMainFragment;
import org.web3j.abi.datatypes.Address;

/* loaded from: classes2.dex */
public class QrFragment extends BaseMainFragment {
    public ImageView i0;
    public ImageView j0;
    public TextView k0;
    public TextView l0;
    public String m0;
    public TokenType n0;
    public uw2 o0;
    public Bitmap p0;
    public Button q0;

    /* JADX INFO: Access modifiers changed from: private */
    public /* synthetic */ void q(View view) {
        f();
    }

    /* JADX INFO: Access modifiers changed from: private */
    public /* synthetic */ void r(View view) {
        ((ClipboardManager) getActivity().getSystemService("clipboard")).setPrimaryClip(ClipData.newPlainText("label", this.m0));
        e30.Z(requireActivity(), R.string.copied);
    }

    @Override // androidx.fragment.app.Fragment
    @SuppressLint({"SetTextI18n"})
    public View onCreateView(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle) {
        View inflate = layoutInflater.inflate(R.layout.fragment_qr, viewGroup, false);
        this.l0 = (TextView) inflate.findViewById(R.id.tvWarningText);
        this.j0 = (ImageView) inflate.findViewById(R.id.iv_back);
        this.k0 = (TextView) inflate.findViewById(R.id.tvPublicAddress);
        this.i0 = (ImageView) inflate.findViewById(R.id.ivQr);
        TextView textView = (TextView) inflate.findViewById(R.id.tvQr);
        this.q0 = (Button) inflate.findViewById(R.id.btnCopy);
        if (getArguments() != null) {
            this.m0 = getArguments().getString(Address.TYPE_NAME);
            this.n0 = TokenType.fromValue(getArguments().getInt("tokenChainId", TokenType.BEP_20.getChainId()));
        }
        this.k0.setText(this.m0);
        this.l0.setText(getResources().getString(R.string.qr_wallet_alert_text, this.n0.getDisplayName()));
        if (this.k0.getText().toString().length() > 0) {
            Display defaultDisplay = ((WindowManager) requireActivity().getSystemService("window")).getDefaultDisplay();
            Point point = new Point();
            defaultDisplay.getSize(point);
            uw2 uw2Var = new uw2(this.k0.getText().toString(), null, "TEXT_TYPE", (Math.min(point.x, point.y) * 3) / 4);
            this.o0 = uw2Var;
            try {
                Bitmap a = uw2Var.a();
                this.p0 = a;
                this.i0.setImageBitmap(a);
            } catch (WriterException e) {
                StringBuilder sb = new StringBuilder();
                sb.append("onCreateView:--> ");
                sb.append(e.getMessage());
            }
        } else {
            this.k0.setError("Required");
        }
        this.j0.setOnClickListener(new View.OnClickListener() { // from class: vw2
            @Override // android.view.View.OnClickListener
            public final void onClick(View view) {
                QrFragment.this.q(view);
            }
        });
        this.q0.setOnClickListener(new View.OnClickListener() { // from class: ww2
            @Override // android.view.View.OnClickListener
            public final void onClick(View view) {
                QrFragment.this.r(view);
            }
        });
        return inflate;
    }
}
