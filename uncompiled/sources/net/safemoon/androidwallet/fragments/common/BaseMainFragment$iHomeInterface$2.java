package net.safemoon.androidwallet.fragments.common;

import kotlin.jvm.internal.Lambda;

/* compiled from: BaseMainFragment.kt */
/* loaded from: classes2.dex */
public final class BaseMainFragment$iHomeInterface$2 extends Lambda implements rc1<gm1> {
    public final /* synthetic */ BaseMainFragment this$0;

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public BaseMainFragment$iHomeInterface$2(BaseMainFragment baseMainFragment) {
        super(0);
        this.this$0 = baseMainFragment;
    }

    /* JADX WARN: Can't rename method to resolve collision */
    @Override // defpackage.rc1
    public final gm1 invoke() {
        if (this.this$0.requireActivity() instanceof gm1) {
            return (gm1) this.this$0.requireActivity();
        }
        return null;
    }
}
