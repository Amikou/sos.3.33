package net.safemoon.androidwallet.fragments.common;

import android.os.Bundle;
import android.view.View;
import android.widget.RadioGroup;
import com.creageek.segmentedbutton.SegmentedButton;
import net.safemoon.androidwallet.R;
import net.safemoon.androidwallet.utils.StoragePermissionLauncher;

/* compiled from: BaseMainFragment.kt */
/* loaded from: classes2.dex */
public abstract class BaseMainFragment extends qn {
    public final sy1 f0 = zy1.a(new BaseMainFragment$iHomeInterface$2(this));
    public int g0 = R.id.navigation_wallet;
    public final a h0 = new a();

    /* compiled from: BaseMainFragment.kt */
    /* loaded from: classes2.dex */
    public static final class a implements RadioGroup.OnCheckedChangeListener {
        public a() {
        }

        @Override // android.widget.RadioGroup.OnCheckedChangeListener
        public void onCheckedChanged(RadioGroup radioGroup, int i) {
            if (radioGroup != null) {
                radioGroup.setOnCheckedChangeListener(null);
            }
            if (radioGroup != null) {
                radioGroup.check(BaseMainFragment.this.g0);
            }
            if (radioGroup != null) {
                radioGroup.setOnCheckedChangeListener(this);
            }
            switch (i) {
                case R.id.rbCalculate /* 2131362987 */:
                    BaseMainFragment baseMainFragment = BaseMainFragment.this;
                    ce2 c = e92.c();
                    fs1.e(c, "actionToCalculator()");
                    baseMainFragment.h(c, true);
                    return;
                case R.id.rbNftS /* 2131362988 */:
                    throw new IllegalStateException("Unsupported operation");
                case R.id.rbReflections /* 2131362989 */:
                    BaseMainFragment baseMainFragment2 = BaseMainFragment.this;
                    ce2 d = e92.d();
                    fs1.e(d, "actionToReflection()");
                    baseMainFragment2.h(d, true);
                    return;
                case R.id.rbTokens /* 2131362990 */:
                    BaseMainFragment baseMainFragment3 = BaseMainFragment.this;
                    ce2 e = e92.e();
                    fs1.e(e, "actionToWallet()");
                    baseMainFragment3.h(e, true);
                    return;
                default:
                    return;
            }
        }
    }

    public final gm1 j() {
        return (gm1) this.f0.getValue();
    }

    public final StoragePermissionLauncher k() {
        gm1 j = j();
        if (j == null) {
            return null;
        }
        return j.d();
    }

    public final hn2 l() {
        gm1 j = j();
        if (j == null) {
            return null;
        }
        return j.a();
    }

    public final void m() {
        if (getActivity() != null) {
            requireActivity().getWindow().addFlags(Integer.MIN_VALUE);
            requireActivity().getWindow().setBackgroundDrawableResource(R.drawable.bottom_curve);
        }
    }

    public final void n(SegmentedButton segmentedButton) {
        fs1.f(segmentedButton, "segmentedGroup");
        this.g0 = segmentedButton.getCheckedRadioButtonId();
        segmentedButton.setOnCheckedChangeListener(this.h0);
    }

    @Override // defpackage.qn, androidx.fragment.app.Fragment
    public void onViewCreated(View view, Bundle bundle) {
        fs1.f(view, "view");
        super.onViewCreated(view, bundle);
        m();
    }
}
