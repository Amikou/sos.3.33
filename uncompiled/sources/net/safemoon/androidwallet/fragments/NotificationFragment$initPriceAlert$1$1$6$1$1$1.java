package net.safemoon.androidwallet.fragments;

import android.content.DialogInterface;
import kotlin.jvm.internal.Lambda;
import net.safemoon.androidwallet.model.priceAlert.PAToken;
import net.safemoon.androidwallet.viewmodels.SettingNotificationViewModel;

/* compiled from: NotificationFragment.kt */
/* loaded from: classes2.dex */
public final class NotificationFragment$initPriceAlert$1$1$6$1$1$1 extends Lambda implements tc1<DialogInterface, te4> {
    public final /* synthetic */ PAToken $it;
    public final /* synthetic */ NotificationFragment this$0;

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public NotificationFragment$initPriceAlert$1$1$6$1$1$1(NotificationFragment notificationFragment, PAToken pAToken) {
        super(1);
        this.this$0 = notificationFragment;
        this.$it = pAToken;
    }

    @Override // defpackage.tc1
    public /* bridge */ /* synthetic */ te4 invoke(DialogInterface dialogInterface) {
        invoke2(dialogInterface);
        return te4.a;
    }

    /* renamed from: invoke  reason: avoid collision after fix types in other method */
    public final void invoke2(DialogInterface dialogInterface) {
        SettingNotificationViewModel H;
        fs1.f(dialogInterface, "$noName_0");
        H = this.this$0.H();
        PAToken pAToken = this.$it;
        fs1.e(pAToken, "it");
        H.o(pAToken);
    }
}
