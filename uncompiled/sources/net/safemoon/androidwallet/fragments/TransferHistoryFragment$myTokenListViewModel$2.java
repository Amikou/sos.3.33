package net.safemoon.androidwallet.fragments;

import androidx.lifecycle.l;
import java.lang.ref.WeakReference;
import kotlin.jvm.internal.Lambda;
import net.safemoon.androidwallet.viewmodels.factory.MyViewModelFactory;

/* compiled from: TransferHistoryFragment.kt */
/* loaded from: classes2.dex */
public final class TransferHistoryFragment$myTokenListViewModel$2 extends Lambda implements rc1<l.b> {
    public final /* synthetic */ TransferHistoryFragment this$0;

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public TransferHistoryFragment$myTokenListViewModel$2(TransferHistoryFragment transferHistoryFragment) {
        super(0);
        this.this$0 = transferHistoryFragment;
    }

    /* JADX WARN: Can't rename method to resolve collision */
    @Override // defpackage.rc1
    public final l.b invoke() {
        return new MyViewModelFactory(new WeakReference(this.this$0.requireActivity()));
    }
}
