package net.safemoon.androidwallet.fragments;

import android.widget.EditText;
import androidx.fragment.app.FragmentActivity;
import java.util.List;
import kotlin.jvm.internal.Lambda;
import net.safemoon.androidwallet.model.wallets.Wallet;

/* compiled from: SendtoFragment.kt */
/* loaded from: classes2.dex */
public final class SendtoFragment$onClick$4 extends Lambda implements tc1<List<? extends Wallet>, te4> {
    public final /* synthetic */ SendtoFragment this$0;

    /* compiled from: SendtoFragment.kt */
    /* loaded from: classes2.dex */
    public static final class a implements dn1 {
        public final /* synthetic */ SendtoFragment a;

        public a(SendtoFragment sendtoFragment) {
            this.a = sendtoFragment;
        }

        @Override // defpackage.dn1
        public void a(Wallet wallet2) {
            fs1.f(wallet2, "item");
            bb1 bb1Var = this.a.m0;
            fs1.d(bb1Var);
            bb1Var.i.setText(wallet2.getAddress());
            qc qcVar = this.a.p0;
            fs1.d(qcVar);
            qcVar.d();
        }
    }

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public SendtoFragment$onClick$4(SendtoFragment sendtoFragment) {
        super(1);
        this.this$0 = sendtoFragment;
    }

    @Override // defpackage.tc1
    public /* bridge */ /* synthetic */ te4 invoke(List<? extends Wallet> list) {
        invoke2((List<Wallet>) list);
        return te4.a;
    }

    /* renamed from: invoke  reason: avoid collision after fix types in other method */
    public final void invoke2(List<Wallet> list) {
        fs1.f(list, "list");
        if (list.size() > 1) {
            this.this$0.p0 = new qc(list);
            qc qcVar = this.this$0.p0;
            fs1.d(qcVar);
            FragmentActivity requireActivity = this.this$0.requireActivity();
            fs1.e(requireActivity, "requireActivity()");
            bb1 bb1Var = this.this$0.m0;
            fs1.d(bb1Var);
            EditText editText = bb1Var.i;
            fs1.e(editText, "binding!!.etAddress");
            bb1 bb1Var2 = this.this$0.m0;
            fs1.d(bb1Var2);
            qcVar.f(requireActivity, editText, bb1Var2.p, new a(this.this$0));
        }
    }
}
