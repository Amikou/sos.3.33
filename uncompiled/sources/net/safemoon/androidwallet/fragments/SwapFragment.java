package net.safemoon.androidwallet.fragments;

import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.text.Editable;
import android.text.InputFilter;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import androidx.appcompat.widget.AppCompatImageView;
import androidx.appcompat.widget.AppCompatTextView;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentActivity;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentViewModelLazyKt;
import androidx.fragment.app.j;
import androidx.lifecycle.LiveData;
import com.github.mikephil.charting.utils.Utils;
import com.google.android.material.button.MaterialButton;
import com.google.android.material.chip.Chip;
import com.google.android.material.textview.MaterialTextView;
import java.lang.ref.WeakReference;
import java.math.BigDecimal;
import java.util.Arrays;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import kotlin.Pair;
import kotlin.text.StringsKt__StringsKt;
import net.safemoon.androidwallet.R;
import net.safemoon.androidwallet.common.TokenType;
import net.safemoon.androidwallet.dialogs.AnchorSwitchWallet;
import net.safemoon.androidwallet.dialogs.BestMaxFragment;
import net.safemoon.androidwallet.dialogs.ConfirmSwapTransactionFragment;
import net.safemoon.androidwallet.dialogs.ProgressLoading;
import net.safemoon.androidwallet.dialogs.SwapAutoSlippage;
import net.safemoon.androidwallet.dialogs.SwapSlipPage;
import net.safemoon.androidwallet.dialogs.SwapTransactionSpeed;
import net.safemoon.androidwallet.dialogs.SwapTransactionTimeLimit;
import net.safemoon.androidwallet.fragments.SelectCurrencyFragment;
import net.safemoon.androidwallet.fragments.SwapFragment;
import net.safemoon.androidwallet.fragments.SwapFragment$onSelectTokenTypeClickListener$2;
import net.safemoon.androidwallet.fragments.common.BaseMainFragment;
import net.safemoon.androidwallet.model.common.Gas;
import net.safemoon.androidwallet.model.common.LoadingState;
import net.safemoon.androidwallet.model.swap.Swap;
import net.safemoon.androidwallet.model.token.room.RoomToken;
import net.safemoon.androidwallet.model.wallets.Wallet;
import net.safemoon.androidwallet.ui.displayModel.UserTokenItemDisplayModel;
import net.safemoon.androidwallet.viewmodels.HomeViewModel;
import net.safemoon.androidwallet.viewmodels.MultiWalletViewModel;
import net.safemoon.androidwallet.viewmodels.MyTokensListViewModel;
import net.safemoon.androidwallet.viewmodels.SwapViewModel;
import net.safemoon.androidwallet.views.editText.autoSize.AutofitEdittext;

/* compiled from: SwapFragment.kt */
/* loaded from: classes2.dex */
public final class SwapFragment extends BaseMainFragment {
    public static final double M0;
    public ck4 H0;
    public ck4 I0;
    public ProgressLoading L0;
    public db1 x0;
    public final long i0 = 2500;
    public final int j0 = 100;
    public final String k0 = "0.01";
    public final int l0 = 200;
    public final int m0 = 200;
    public final String n0 = "WBNB";
    public final String o0 = "WETH";
    public final String p0 = "SAFEMOON";
    public final float q0 = 25.0f;
    public final float r0 = 50.0f;
    public final float s0 = 75.0f;
    public final float t0 = 100.0f;
    public boolean u0 = true;
    public int v0 = -1;
    public int w0 = -1;
    public final sy1 y0 = FragmentViewModelLazyKt.a(this, d53.b(HomeViewModel.class), new SwapFragment$special$$inlined$activityViewModels$default$1(this), new SwapFragment$special$$inlined$activityViewModels$default$2(this));
    public final sy1 z0 = FragmentViewModelLazyKt.a(this, d53.b(MultiWalletViewModel.class), new SwapFragment$special$$inlined$viewModels$default$2(new SwapFragment$special$$inlined$viewModels$default$1(this)), null);
    public final sy1 A0 = FragmentViewModelLazyKt.a(this, d53.b(SwapViewModel.class), new SwapFragment$special$$inlined$activityViewModels$default$3(this), new SwapFragment$special$$inlined$activityViewModels$default$4(this));
    public final sy1 B0 = FragmentViewModelLazyKt.a(this, d53.b(MyTokensListViewModel.class), new SwapFragment$special$$inlined$activityViewModels$1(this), new SwapFragment$myTokenListViewModel$2(this));
    public final List<TokenType> C0 = b20.j(TokenType.POLYGON, TokenType.AVALANCHE_C);
    public final sy1 D0 = zy1.a(new SwapFragment$swapChains$2(this));
    public final sy1 E0 = zy1.a(new SwapFragment$onSelectTokenTypeClickListener$2(this));
    public final sy1 F0 = zy1.a(new SwapFragment$argDefaultSwapParam$2(this));
    public final sy1 G0 = zy1.a(new SwapFragment$iHomeActivity$2(this));
    public final List<Pair<String, Double>> J0 = b20.g();
    public final List<String> K0 = a20.b("SAFEMOON");

    /* compiled from: SwapFragment.kt */
    /* loaded from: classes2.dex */
    public static final class a {
        public a() {
        }

        public /* synthetic */ a(qi0 qi0Var) {
            this();
        }
    }

    /* compiled from: SwapFragment.kt */
    /* loaded from: classes2.dex */
    public /* synthetic */ class b {
        public static final /* synthetic */ int[] a;

        static {
            int[] iArr = new int[Gas.values().length];
            iArr[Gas.Standard.ordinal()] = 1;
            iArr[Gas.Fast.ordinal()] = 2;
            iArr[Gas.Lightning.ordinal()] = 3;
            a = iArr;
        }
    }

    /* compiled from: SwapFragment.kt */
    /* loaded from: classes2.dex */
    public static final class c {
        public RoomToken a;
        public SwapViewModel.c b;

        public c(RoomToken roomToken, SwapViewModel.c cVar) {
            this.a = roomToken;
            this.b = cVar;
        }

        public final RoomToken a() {
            return this.a;
        }

        public final SwapViewModel.c b() {
            return this.b;
        }

        public final void c(RoomToken roomToken) {
            this.a = roomToken;
        }

        public final void d(SwapViewModel.c cVar) {
            this.b = cVar;
        }

        public boolean equals(Object obj) {
            if (this == obj) {
                return true;
            }
            if (obj instanceof c) {
                c cVar = (c) obj;
                return fs1.b(this.a, cVar.a) && fs1.b(this.b, cVar.b);
            }
            return false;
        }

        public int hashCode() {
            RoomToken roomToken = this.a;
            int hashCode = (roomToken == null ? 0 : roomToken.hashCode()) * 31;
            SwapViewModel.c cVar = this.b;
            return hashCode + (cVar != null ? cVar.hashCode() : 0);
        }

        public String toString() {
            return "btnV2(roomToken=" + this.a + ", tokenType=" + this.b + ')';
        }
    }

    /* compiled from: SwapFragment.kt */
    /* loaded from: classes2.dex */
    public static final class d extends vu0 {
        public d() {
        }

        /* JADX WARN: Code restructure failed: missing block: B:30:0x0070, code lost:
            r8 = r7.a.I0;
         */
        /* JADX WARN: Code restructure failed: missing block: B:31:0x0076, code lost:
            if (r8 != null) goto L38;
         */
        /* JADX WARN: Code restructure failed: missing block: B:32:0x0078, code lost:
            defpackage.fs1.r("bindingDestination");
         */
        /* JADX WARN: Code restructure failed: missing block: B:33:0x007c, code lost:
            r1 = r8;
         */
        /* JADX WARN: Code restructure failed: missing block: B:35:0x008f, code lost:
            if (defpackage.e30.K(java.lang.String.valueOf(r1.j.getText())) <= com.github.mikephil.charting.utils.Utils.DOUBLE_EPSILON) goto L37;
         */
        /* JADX WARN: Code restructure failed: missing block: B:36:0x0091, code lost:
            defpackage.pg4.f(r7.a.requireContext());
         */
        /* JADX WARN: Code restructure failed: missing block: B:45:?, code lost:
            return;
         */
        /* JADX WARN: Code restructure failed: missing block: B:46:?, code lost:
            return;
         */
        /* JADX WARN: Removed duplicated region for block: B:22:0x005a A[Catch: Exception -> 0x009b, TryCatch #0 {Exception -> 0x009b, blocks: (B:3:0x0003, B:6:0x000e, B:7:0x0012, B:9:0x001a, B:11:0x002a, B:16:0x0035, B:18:0x003f, B:20:0x0047, B:22:0x005a, B:23:0x005e, B:25:0x0066, B:30:0x0070, B:32:0x0078, B:34:0x007d, B:36:0x0091, B:19:0x0042), top: B:41:0x0003 }] */
        @Override // defpackage.vu0, android.text.TextWatcher
        /*
            Code decompiled incorrectly, please refer to instructions dump.
            To view partially-correct code enable 'Show inconsistent code' option in preferences
        */
        public void afterTextChanged(android.text.Editable r8) {
            /*
                r7 = this;
                super.afterTextChanged(r8)
                net.safemoon.androidwallet.fragments.SwapFragment r0 = net.safemoon.androidwallet.fragments.SwapFragment.this     // Catch: java.lang.Exception -> L9b
                ck4 r0 = net.safemoon.androidwallet.fragments.SwapFragment.r0(r0)     // Catch: java.lang.Exception -> L9b
                r1 = 0
                java.lang.String r2 = "bindingDestination"
                if (r0 != 0) goto L12
                defpackage.fs1.r(r2)     // Catch: java.lang.Exception -> L9b
                r0 = r1
            L12:
                net.safemoon.androidwallet.views.editText.autoSize.AutofitEdittext r0 = r0.j     // Catch: java.lang.Exception -> L9b
                boolean r0 = r0.isFocused()     // Catch: java.lang.Exception -> L9b
                if (r0 == 0) goto L9f
                net.safemoon.androidwallet.fragments.SwapFragment r0 = net.safemoon.androidwallet.fragments.SwapFragment.this     // Catch: java.lang.Exception -> L9b
                net.safemoon.androidwallet.viewmodels.SwapViewModel r0 = net.safemoon.androidwallet.fragments.SwapFragment.y0(r0)     // Catch: java.lang.Exception -> L9b
                gb2 r0 = r0.F0()     // Catch: java.lang.Exception -> L9b
                net.safemoon.androidwallet.viewmodels.SwapViewModel$a r3 = new net.safemoon.androidwallet.viewmodels.SwapViewModel$a     // Catch: java.lang.Exception -> L9b
                r4 = 1
                r5 = 0
                if (r8 == 0) goto L42
                int r6 = r8.length()     // Catch: java.lang.Exception -> L9b
                if (r6 <= 0) goto L32
                r6 = r4
                goto L33
            L32:
                r6 = r5
            L33:
                if (r6 == 0) goto L42
                java.lang.String r8 = r8.toString()     // Catch: java.lang.Exception -> L9b
                java.math.BigDecimal r8 = defpackage.e30.L(r8)     // Catch: java.lang.Exception -> L9b
                if (r8 != 0) goto L47
                java.math.BigDecimal r8 = java.math.BigDecimal.ZERO     // Catch: java.lang.Exception -> L9b
                goto L47
            L42:
                java.math.BigDecimal r8 = new java.math.BigDecimal     // Catch: java.lang.Exception -> L9b
                r8.<init>(r5)     // Catch: java.lang.Exception -> L9b
            L47:
                java.lang.String r6 = "if (s != null && s.isNot…                        }"
                defpackage.fs1.e(r8, r6)     // Catch: java.lang.Exception -> L9b
                r3.<init>(r5, r8)     // Catch: java.lang.Exception -> L9b
                r0.postValue(r3)     // Catch: java.lang.Exception -> L9b
                net.safemoon.androidwallet.fragments.SwapFragment r8 = net.safemoon.androidwallet.fragments.SwapFragment.this     // Catch: java.lang.Exception -> L9b
                ck4 r8 = net.safemoon.androidwallet.fragments.SwapFragment.r0(r8)     // Catch: java.lang.Exception -> L9b
                if (r8 != 0) goto L5e
                defpackage.fs1.r(r2)     // Catch: java.lang.Exception -> L9b
                r8 = r1
            L5e:
                net.safemoon.androidwallet.views.editText.autoSize.AutofitEdittext r8 = r8.j     // Catch: java.lang.Exception -> L9b
                android.text.Editable r8 = r8.getText()     // Catch: java.lang.Exception -> L9b
                if (r8 == 0) goto L6e
                boolean r8 = defpackage.dv3.w(r8)     // Catch: java.lang.Exception -> L9b
                if (r8 == 0) goto L6d
                goto L6e
            L6d:
                r4 = r5
            L6e:
                if (r4 != 0) goto L9f
                net.safemoon.androidwallet.fragments.SwapFragment r8 = net.safemoon.androidwallet.fragments.SwapFragment.this     // Catch: java.lang.Exception -> L9b
                ck4 r8 = net.safemoon.androidwallet.fragments.SwapFragment.r0(r8)     // Catch: java.lang.Exception -> L9b
                if (r8 != 0) goto L7c
                defpackage.fs1.r(r2)     // Catch: java.lang.Exception -> L9b
                goto L7d
            L7c:
                r1 = r8
            L7d:
                net.safemoon.androidwallet.views.editText.autoSize.AutofitEdittext r8 = r1.j     // Catch: java.lang.Exception -> L9b
                android.text.Editable r8 = r8.getText()     // Catch: java.lang.Exception -> L9b
                java.lang.String r8 = java.lang.String.valueOf(r8)     // Catch: java.lang.Exception -> L9b
                double r0 = defpackage.e30.K(r8)     // Catch: java.lang.Exception -> L9b
                r2 = 0
                int r8 = (r0 > r2 ? 1 : (r0 == r2 ? 0 : -1))
                if (r8 <= 0) goto L9f
                net.safemoon.androidwallet.fragments.SwapFragment r8 = net.safemoon.androidwallet.fragments.SwapFragment.this     // Catch: java.lang.Exception -> L9b
                android.content.Context r8 = r8.requireContext()     // Catch: java.lang.Exception -> L9b
                defpackage.pg4.f(r8)     // Catch: java.lang.Exception -> L9b
                goto L9f
            L9b:
                r8 = move-exception
                r8.printStackTrace()
            L9f:
                return
            */
            throw new UnsupportedOperationException("Method not decompiled: net.safemoon.androidwallet.fragments.SwapFragment.d.afterTextChanged(android.text.Editable):void");
        }

        @Override // defpackage.vu0, android.text.TextWatcher
        public void onTextChanged(CharSequence charSequence, int i, int i2, int i3) {
            String obj;
            super.onTextChanged(charSequence, i, i2, i3);
            db1 db1Var = SwapFragment.this.x0;
            db1 db1Var2 = null;
            if (db1Var == null) {
                fs1.r("binding");
                db1Var = null;
            }
            db1Var.b.setVisibility(8);
            ck4 ck4Var = SwapFragment.this.I0;
            if (ck4Var == null) {
                fs1.r("bindingDestination");
                ck4Var = null;
            }
            Editable text = ck4Var.j.getText();
            String obj2 = (text == null || (obj = text.toString()) == null) ? null : StringsKt__StringsKt.K0(obj).toString();
            if (obj2 == null || obj2.length() == 0) {
                db1 db1Var3 = SwapFragment.this.x0;
                if (db1Var3 == null) {
                    fs1.r("binding");
                    db1Var3 = null;
                }
                db1Var3.c.setVisibility(0);
                db1 db1Var4 = SwapFragment.this.x0;
                if (db1Var4 == null) {
                    fs1.r("binding");
                    db1Var4 = null;
                }
                db1Var4.d.setVisibility(8);
                db1 db1Var5 = SwapFragment.this.x0;
                if (db1Var5 == null) {
                    fs1.r("binding");
                } else {
                    db1Var2 = db1Var5;
                }
                db1Var2.p.setVisibility(8);
                return;
            }
            db1 db1Var6 = SwapFragment.this.x0;
            if (db1Var6 == null) {
                fs1.r("binding");
                db1Var6 = null;
            }
            db1Var6.c.setVisibility(8);
            db1 db1Var7 = SwapFragment.this.x0;
            if (db1Var7 == null) {
                fs1.r("binding");
                db1Var7 = null;
            }
            db1Var7.d.setVisibility(0);
            db1 db1Var8 = SwapFragment.this.x0;
            if (db1Var8 == null) {
                fs1.r("binding");
            } else {
                db1Var2 = db1Var8;
            }
            db1Var2.p.setVisibility(8);
        }
    }

    /* compiled from: SwapFragment.kt */
    /* loaded from: classes2.dex */
    public static final class e extends vu0 {
        public e() {
        }

        /* JADX WARN: Removed duplicated region for block: B:22:0x005a A[Catch: Exception -> 0x009a, TryCatch #0 {Exception -> 0x009a, blocks: (B:3:0x0003, B:6:0x000e, B:7:0x0012, B:9:0x001a, B:11:0x002a, B:16:0x0035, B:18:0x003f, B:20:0x0047, B:22:0x005a, B:23:0x005e, B:25:0x0066, B:29:0x006f, B:31:0x0077, B:33:0x007c, B:35:0x0090, B:19:0x0042), top: B:40:0x0003 }] */
        /* JADX WARN: Removed duplicated region for block: B:29:0x006f A[Catch: Exception -> 0x009a, TryCatch #0 {Exception -> 0x009a, blocks: (B:3:0x0003, B:6:0x000e, B:7:0x0012, B:9:0x001a, B:11:0x002a, B:16:0x0035, B:18:0x003f, B:20:0x0047, B:22:0x005a, B:23:0x005e, B:25:0x0066, B:29:0x006f, B:31:0x0077, B:33:0x007c, B:35:0x0090, B:19:0x0042), top: B:40:0x0003 }] */
        /* JADX WARN: Removed duplicated region for block: B:43:? A[RETURN, SYNTHETIC] */
        @Override // defpackage.vu0, android.text.TextWatcher
        /*
            Code decompiled incorrectly, please refer to instructions dump.
            To view partially-correct code enable 'Show inconsistent code' option in preferences
        */
        public void afterTextChanged(android.text.Editable r8) {
            /*
                r7 = this;
                super.afterTextChanged(r8)
                net.safemoon.androidwallet.fragments.SwapFragment r0 = net.safemoon.androidwallet.fragments.SwapFragment.this     // Catch: java.lang.Exception -> L9a
                ck4 r0 = net.safemoon.androidwallet.fragments.SwapFragment.s0(r0)     // Catch: java.lang.Exception -> L9a
                r1 = 0
                java.lang.String r2 = "bindingSource"
                if (r0 != 0) goto L12
                defpackage.fs1.r(r2)     // Catch: java.lang.Exception -> L9a
                r0 = r1
            L12:
                net.safemoon.androidwallet.views.editText.autoSize.AutofitEdittext r0 = r0.j     // Catch: java.lang.Exception -> L9a
                boolean r0 = r0.isFocused()     // Catch: java.lang.Exception -> L9a
                if (r0 == 0) goto L9e
                net.safemoon.androidwallet.fragments.SwapFragment r0 = net.safemoon.androidwallet.fragments.SwapFragment.this     // Catch: java.lang.Exception -> L9a
                net.safemoon.androidwallet.viewmodels.SwapViewModel r0 = net.safemoon.androidwallet.fragments.SwapFragment.y0(r0)     // Catch: java.lang.Exception -> L9a
                gb2 r0 = r0.F0()     // Catch: java.lang.Exception -> L9a
                net.safemoon.androidwallet.viewmodels.SwapViewModel$a r3 = new net.safemoon.androidwallet.viewmodels.SwapViewModel$a     // Catch: java.lang.Exception -> L9a
                r4 = 0
                r5 = 1
                if (r8 == 0) goto L42
                int r6 = r8.length()     // Catch: java.lang.Exception -> L9a
                if (r6 <= 0) goto L32
                r6 = r5
                goto L33
            L32:
                r6 = r4
            L33:
                if (r6 == 0) goto L42
                java.lang.String r8 = r8.toString()     // Catch: java.lang.Exception -> L9a
                java.math.BigDecimal r8 = defpackage.e30.L(r8)     // Catch: java.lang.Exception -> L9a
                if (r8 != 0) goto L47
                java.math.BigDecimal r8 = java.math.BigDecimal.ZERO     // Catch: java.lang.Exception -> L9a
                goto L47
            L42:
                java.math.BigDecimal r8 = new java.math.BigDecimal     // Catch: java.lang.Exception -> L9a
                r8.<init>(r4)     // Catch: java.lang.Exception -> L9a
            L47:
                java.lang.String r6 = "if (s != null && s.isNot…                        }"
                defpackage.fs1.e(r8, r6)     // Catch: java.lang.Exception -> L9a
                r3.<init>(r5, r8)     // Catch: java.lang.Exception -> L9a
                r0.postValue(r3)     // Catch: java.lang.Exception -> L9a
                net.safemoon.androidwallet.fragments.SwapFragment r8 = net.safemoon.androidwallet.fragments.SwapFragment.this     // Catch: java.lang.Exception -> L9a
                ck4 r8 = net.safemoon.androidwallet.fragments.SwapFragment.s0(r8)     // Catch: java.lang.Exception -> L9a
                if (r8 != 0) goto L5e
                defpackage.fs1.r(r2)     // Catch: java.lang.Exception -> L9a
                r8 = r1
            L5e:
                net.safemoon.androidwallet.views.editText.autoSize.AutofitEdittext r8 = r8.j     // Catch: java.lang.Exception -> L9a
                android.text.Editable r8 = r8.getText()     // Catch: java.lang.Exception -> L9a
                if (r8 == 0) goto L6c
                boolean r8 = defpackage.dv3.w(r8)     // Catch: java.lang.Exception -> L9a
                if (r8 == 0) goto L6d
            L6c:
                r4 = r5
            L6d:
                if (r4 != 0) goto L9e
                net.safemoon.androidwallet.fragments.SwapFragment r8 = net.safemoon.androidwallet.fragments.SwapFragment.this     // Catch: java.lang.Exception -> L9a
                ck4 r8 = net.safemoon.androidwallet.fragments.SwapFragment.s0(r8)     // Catch: java.lang.Exception -> L9a
                if (r8 != 0) goto L7b
                defpackage.fs1.r(r2)     // Catch: java.lang.Exception -> L9a
                goto L7c
            L7b:
                r1 = r8
            L7c:
                net.safemoon.androidwallet.views.editText.autoSize.AutofitEdittext r8 = r1.j     // Catch: java.lang.Exception -> L9a
                android.text.Editable r8 = r8.getText()     // Catch: java.lang.Exception -> L9a
                java.lang.String r8 = java.lang.String.valueOf(r8)     // Catch: java.lang.Exception -> L9a
                double r0 = defpackage.e30.K(r8)     // Catch: java.lang.Exception -> L9a
                r2 = 0
                int r8 = (r0 > r2 ? 1 : (r0 == r2 ? 0 : -1))
                if (r8 <= 0) goto L9e
                net.safemoon.androidwallet.fragments.SwapFragment r8 = net.safemoon.androidwallet.fragments.SwapFragment.this     // Catch: java.lang.Exception -> L9a
                android.content.Context r8 = r8.requireContext()     // Catch: java.lang.Exception -> L9a
                defpackage.pg4.f(r8)     // Catch: java.lang.Exception -> L9a
                goto L9e
            L9a:
                r8 = move-exception
                r8.printStackTrace()
            L9e:
                return
            */
            throw new UnsupportedOperationException("Method not decompiled: net.safemoon.androidwallet.fragments.SwapFragment.e.afterTextChanged(android.text.Editable):void");
        }

        @Override // defpackage.vu0, android.text.TextWatcher
        public void onTextChanged(CharSequence charSequence, int i, int i2, int i3) {
            String obj;
            super.onTextChanged(charSequence, i, i2, i3);
            db1 db1Var = SwapFragment.this.x0;
            db1 db1Var2 = null;
            if (db1Var == null) {
                fs1.r("binding");
                db1Var = null;
            }
            db1Var.b.setVisibility(8);
            ck4 ck4Var = SwapFragment.this.H0;
            if (ck4Var == null) {
                fs1.r("bindingSource");
                ck4Var = null;
            }
            Editable text = ck4Var.j.getText();
            String obj2 = (text == null || (obj = text.toString()) == null) ? null : StringsKt__StringsKt.K0(obj).toString();
            if (obj2 == null || obj2.length() == 0) {
                db1 db1Var3 = SwapFragment.this.x0;
                if (db1Var3 == null) {
                    fs1.r("binding");
                    db1Var3 = null;
                }
                db1Var3.c.setVisibility(0);
                db1 db1Var4 = SwapFragment.this.x0;
                if (db1Var4 == null) {
                    fs1.r("binding");
                    db1Var4 = null;
                }
                db1Var4.d.setVisibility(8);
                db1 db1Var5 = SwapFragment.this.x0;
                if (db1Var5 == null) {
                    fs1.r("binding");
                } else {
                    db1Var2 = db1Var5;
                }
                db1Var2.p.setVisibility(8);
                return;
            }
            db1 db1Var6 = SwapFragment.this.x0;
            if (db1Var6 == null) {
                fs1.r("binding");
                db1Var6 = null;
            }
            db1Var6.c.setVisibility(8);
            db1 db1Var7 = SwapFragment.this.x0;
            if (db1Var7 == null) {
                fs1.r("binding");
                db1Var7 = null;
            }
            db1Var7.d.setVisibility(0);
            db1 db1Var8 = SwapFragment.this.x0;
            if (db1Var8 == null) {
                fs1.r("binding");
            } else {
                db1Var2 = db1Var8;
            }
            db1Var2.p.setVisibility(8);
        }
    }

    static {
        new a(null);
        M0 = 0.5d;
    }

    public static final void A1(SwapFragment swapFragment, View view) {
        fs1.f(swapFragment, "$this_run");
        bh.h0(new WeakReference(swapFragment.requireActivity()), R.string.hint_from_swap_estimate, R.string.ok);
    }

    public static final void B1(SwapFragment swapFragment, View view) {
        fs1.f(swapFragment, "$this_run");
        pg4.e(swapFragment.requireActivity());
        swapFragment.S1(SelectCurrencyFragment.n0.a(SelectCurrencyFragment.SWAPPATH.Destination), true);
    }

    public static final void C0(ck4 ck4Var, View view) {
        fs1.f(ck4Var, "$this_run");
        ck4Var.j.setText("");
    }

    public static final void C1(SwapFragment swapFragment, View view) {
        fs1.f(swapFragment, "$this_run");
        bh.h0(new WeakReference(swapFragment.requireActivity()), R.string.hint_to_swap_estimate, R.string.ok);
    }

    public static final void D0(ck4 ck4Var, View view) {
        fs1.f(ck4Var, "$this_run");
        ck4Var.j.setText("");
    }

    public static final void D1(SwapFragment swapFragment, SwapViewModel.a aVar) {
        fs1.f(swapFragment, "this$0");
        swapFragment.N0().V0();
    }

    public static final void E1(gb2 gb2Var, SwapFragment swapFragment, SwapViewModel.c cVar) {
        fs1.f(gb2Var, "$btnV2LiveData");
        fs1.f(swapFragment, "this$0");
        c cVar2 = (c) gb2Var.getValue();
        db1 db1Var = null;
        if (cVar2 == null) {
            cVar2 = new c(null, null);
        }
        cVar2.d(cVar);
        te4 te4Var = te4.a;
        gb2Var.setValue(cVar2);
        SwapViewModel N0 = swapFragment.N0();
        fs1.e(cVar, "param");
        N0.J(cVar);
        TokenType a2 = cVar.a();
        db1 db1Var2 = swapFragment.x0;
        if (db1Var2 == null) {
            fs1.r("binding");
            db1Var2 = null;
        }
        db1Var2.n.d.setText(a2.getPlaneName());
        db1 db1Var3 = swapFragment.x0;
        if (db1Var3 == null) {
            fs1.r("binding");
        } else {
            db1Var = db1Var3;
        }
        db1Var.n.c.setImageResource(a2.getIcon());
        if (swapFragment.u0) {
            swapFragment.J0().J(a2);
            swapFragment.G0().p(a2);
        }
    }

    /* JADX WARN: Removed duplicated region for block: B:22:0x0059  */
    /* JADX WARN: Removed duplicated region for block: B:45:0x00b6  */
    /* JADX WARN: Removed duplicated region for block: B:48:0x00c5  */
    /* JADX WARN: Removed duplicated region for block: B:49:0x00c9  */
    /*
        Code decompiled incorrectly, please refer to instructions dump.
        To view partially-correct code enable 'Show inconsistent code' option in preferences
    */
    public static final void F1(net.safemoon.androidwallet.fragments.SwapFragment r16, java.lang.Double r17) {
        /*
            Method dump skipped, instructions count: 224
            To view this dump change 'Code comments level' option to 'DEBUG'
        */
        throw new UnsupportedOperationException("Method not decompiled: net.safemoon.androidwallet.fragments.SwapFragment.F1(net.safemoon.androidwallet.fragments.SwapFragment, java.lang.Double):void");
    }

    public static final void G1(SwapFragment swapFragment, Boolean bool) {
        SwapViewModel.a value;
        fs1.f(swapFragment, "this$0");
        fs1.e(bool, "it");
        if (!bool.booleanValue() || (value = swapFragment.N0().F0().getValue()) == null) {
            return;
        }
        ck4 ck4Var = null;
        if (value.b()) {
            ck4 ck4Var2 = swapFragment.H0;
            if (ck4Var2 == null) {
                fs1.r("bindingSource");
                ck4Var2 = null;
            }
            AppCompatTextView appCompatTextView = ck4Var2.h;
            fs1.e(appCompatTextView, "bindingSource.hintNewValue");
            appCompatTextView.setVisibility(8);
            ck4 ck4Var3 = swapFragment.H0;
            if (ck4Var3 == null) {
                fs1.r("bindingSource");
                ck4Var3 = null;
            }
            ck4Var3.j.requestFocus();
            ck4 ck4Var4 = swapFragment.H0;
            if (ck4Var4 == null) {
                fs1.r("bindingSource");
            } else {
                ck4Var = ck4Var4;
            }
            ck4Var.j.setText(e30.p(value.a().doubleValue(), 0, null, false, 6, null));
            return;
        }
        ck4 ck4Var5 = swapFragment.I0;
        if (ck4Var5 == null) {
            fs1.r("bindingDestination");
            ck4Var5 = null;
        }
        AppCompatTextView appCompatTextView2 = ck4Var5.h;
        fs1.e(appCompatTextView2, "bindingDestination.hintNewValue");
        appCompatTextView2.setVisibility(8);
        ck4 ck4Var6 = swapFragment.I0;
        if (ck4Var6 == null) {
            fs1.r("bindingDestination");
            ck4Var6 = null;
        }
        ck4Var6.j.requestFocus();
        ck4 ck4Var7 = swapFragment.I0;
        if (ck4Var7 == null) {
            fs1.r("bindingDestination");
        } else {
            ck4Var = ck4Var7;
        }
        ck4Var.j.setText(e30.p(value.a().doubleValue(), 0, null, false, 6, null));
    }

    /* JADX WARN: Removed duplicated region for block: B:22:0x0059  */
    /* JADX WARN: Removed duplicated region for block: B:45:0x00b6  */
    /* JADX WARN: Removed duplicated region for block: B:48:0x00c5  */
    /* JADX WARN: Removed duplicated region for block: B:49:0x00c9  */
    /*
        Code decompiled incorrectly, please refer to instructions dump.
        To view partially-correct code enable 'Show inconsistent code' option in preferences
    */
    public static final void H1(net.safemoon.androidwallet.fragments.SwapFragment r16, java.lang.Double r17) {
        /*
            Method dump skipped, instructions count: 224
            To view this dump change 'Code comments level' option to 'DEBUG'
        */
        throw new UnsupportedOperationException("Method not decompiled: net.safemoon.androidwallet.fragments.SwapFragment.H1(net.safemoon.androidwallet.fragments.SwapFragment, java.lang.Double):void");
    }

    public static final void I1(SwapFragment swapFragment, Boolean bool) {
        fs1.f(swapFragment, "this$0");
        fs1.e(bool, "it");
        ck4 ck4Var = null;
        if (bool.booleanValue() && swapFragment.N0().F0().getValue() != null) {
            SwapViewModel.a value = swapFragment.N0().F0().getValue();
            fs1.d(value);
            if (value.b()) {
                ck4 ck4Var2 = swapFragment.H0;
                if (ck4Var2 == null) {
                    fs1.r("bindingSource");
                    ck4Var2 = null;
                }
                ck4Var2.m.setVisibility(8);
                ck4 ck4Var3 = swapFragment.I0;
                if (ck4Var3 == null) {
                    fs1.r("bindingDestination");
                    ck4Var3 = null;
                }
                ck4Var3.m.setVisibility(0);
                ck4 ck4Var4 = swapFragment.H0;
                if (ck4Var4 == null) {
                    fs1.r("bindingSource");
                    ck4Var4 = null;
                }
                ck4Var4.k.setEnabled(true);
                ck4 ck4Var5 = swapFragment.H0;
                if (ck4Var5 == null) {
                    fs1.r("bindingSource");
                    ck4Var5 = null;
                }
                ck4Var5.j.setEnabled(true);
                ck4 ck4Var6 = swapFragment.I0;
                if (ck4Var6 == null) {
                    fs1.r("bindingDestination");
                } else {
                    ck4Var = ck4Var6;
                }
                ck4Var.k.setEnabled(false);
            } else {
                ck4 ck4Var7 = swapFragment.H0;
                if (ck4Var7 == null) {
                    fs1.r("bindingSource");
                    ck4Var7 = null;
                }
                ck4Var7.m.setVisibility(0);
                ck4 ck4Var8 = swapFragment.I0;
                if (ck4Var8 == null) {
                    fs1.r("bindingDestination");
                    ck4Var8 = null;
                }
                ck4Var8.m.setVisibility(8);
                ck4 ck4Var9 = swapFragment.H0;
                if (ck4Var9 == null) {
                    fs1.r("bindingSource");
                    ck4Var9 = null;
                }
                ck4Var9.k.setEnabled(false);
                ck4 ck4Var10 = swapFragment.H0;
                if (ck4Var10 == null) {
                    fs1.r("bindingSource");
                    ck4Var10 = null;
                }
                ck4Var10.j.setEnabled(false);
                ck4 ck4Var11 = swapFragment.I0;
                if (ck4Var11 == null) {
                    fs1.r("bindingDestination");
                } else {
                    ck4Var = ck4Var11;
                }
                ck4Var.k.setEnabled(true);
            }
            swapFragment.Y0(true);
            return;
        }
        swapFragment.Y0(false);
        ck4 ck4Var12 = swapFragment.H0;
        if (ck4Var12 == null) {
            fs1.r("bindingSource");
            ck4Var12 = null;
        }
        ck4Var12.m.setVisibility(8);
        ck4 ck4Var13 = swapFragment.I0;
        if (ck4Var13 == null) {
            fs1.r("bindingDestination");
            ck4Var13 = null;
        }
        ck4Var13.m.setVisibility(8);
        ck4 ck4Var14 = swapFragment.H0;
        if (ck4Var14 == null) {
            fs1.r("bindingSource");
            ck4Var14 = null;
        }
        ck4Var14.k.setEnabled(true);
        ck4 ck4Var15 = swapFragment.H0;
        if (ck4Var15 == null) {
            fs1.r("bindingSource");
            ck4Var15 = null;
        }
        ck4Var15.j.setEnabled(true);
        ck4 ck4Var16 = swapFragment.I0;
        if (ck4Var16 == null) {
            fs1.r("bindingDestination");
        } else {
            ck4Var = ck4Var16;
        }
        ck4Var.k.setEnabled(true);
    }

    public static final void J1(SwapFragment swapFragment, LoadingState loadingState) {
        fs1.f(swapFragment, "this$0");
        if (loadingState == null) {
            return;
        }
        LoadingState loadingState2 = LoadingState.Loading;
        swapFragment.Z0(loadingState == loadingState2);
        db1 db1Var = swapFragment.x0;
        if (db1Var == null) {
            fs1.r("binding");
            db1Var = null;
        }
        Chip chip = db1Var.p;
        fs1.e(chip, "requireGasFee");
        chip.setVisibility(8);
        db1Var.d.setVisibility(loadingState == LoadingState.Normal ? 0 : 8);
        if (loadingState == loadingState2) {
            String string = swapFragment.getString(R.string.swap_loading_title);
            fs1.e(string, "getString(R.string.swap_loading_title)");
            String string2 = swapFragment.getString(R.string.swap_loading_msg);
            fs1.e(string2, "getString(R.string.swap_loading_msg)");
            swapFragment.V1(false, string, string2);
            return;
        }
        swapFragment.E0();
    }

    public static final void K1(SwapFragment swapFragment, View view) {
        Object obj;
        Object obj2;
        fs1.f(swapFragment, "this$0");
        Iterator<T> it = swapFragment.J0.iterator();
        while (true) {
            obj = null;
            if (!it.hasNext()) {
                obj2 = null;
                break;
            }
            obj2 = it.next();
            Object first = ((Pair) obj2).getFirst();
            Swap value = swapFragment.N0().A0().getValue();
            if (fs1.b(first, value == null ? null : value.symbol)) {
                break;
            }
        }
        Pair pair = (Pair) obj2;
        Iterator<T> it2 = swapFragment.J0.iterator();
        while (true) {
            if (!it2.hasNext()) {
                break;
            }
            Object next = it2.next();
            Object first2 = ((Pair) next).getFirst();
            Swap value2 = swapFragment.N0().b0().getValue();
            if (fs1.b(first2, value2 == null ? null : value2.symbol)) {
                obj = next;
                break;
            }
        }
        Pair pair2 = (Pair) obj;
        if (pair == null) {
            pair = pair2;
        }
        if (pair != null) {
            Double value3 = swapFragment.N0().w0().getValue();
            fs1.d(value3);
            fs1.e(value3, "swapViewModel.slipPage.value!!");
            if (value3.doubleValue() < ((Number) pair.getSecond()).doubleValue()) {
                WeakReference weakReference = new WeakReference(swapFragment.requireActivity());
                String string = swapFragment.getString(R.string.dialog_alert_slip_title);
                fs1.e(string, "getString(R.string.dialog_alert_slip_title)");
                String string2 = swapFragment.getString(R.string.dialog_alert_slip_desc);
                fs1.e(string2, "getString(R.string.dialog_alert_slip_desc)");
                String string3 = swapFragment.getString(R.string.dialog_alert_slip_button);
                fs1.e(string3, "getString(R.string.dialog_alert_slip_button)");
                lu3 lu3Var = lu3.a;
                String format = String.format("DO_NOT_SHOW_ALERT_%s", Arrays.copyOf(new Object[]{pair.getFirst()}, 1));
                fs1.e(format, "java.lang.String.format(format, *args)");
                bh.z0(weakReference, string, string2, string3, format, xy3.a);
                return;
            }
        }
        ConfirmSwapTransactionFragment a2 = ConfirmSwapTransactionFragment.x0.a();
        FragmentManager childFragmentManager = swapFragment.getChildFragmentManager();
        fs1.e(childFragmentManager, "childFragmentManager");
        a2.N(childFragmentManager);
    }

    public static final void L1(View view) {
    }

    public static final void M1(final SwapFragment swapFragment, SwapViewModel.e eVar) {
        fs1.f(swapFragment, "this$0");
        if (eVar != null) {
            if (eVar.b()) {
                b30 b30Var = b30.a;
                TokenType.a aVar = TokenType.Companion;
                Swap value = swapFragment.N0().A0().getValue();
                fs1.d(value);
                Integer num = value.chainId;
                fs1.e(num, "swapViewModel.sourceLiveData.value!!.chainId");
                String l = fs1.l(b30Var.o(aVar.b(num.intValue())), eVar.a());
                swapFragment.N0().L0().postValue(null);
                fo0.e(new WeakReference(swapFragment.requireActivity()), l, new DialogInterface.OnDismissListener() { // from class: by3
                    @Override // android.content.DialogInterface.OnDismissListener
                    public final void onDismiss(DialogInterface dialogInterface) {
                        SwapFragment.N1(SwapFragment.this, dialogInterface);
                    }
                });
                gm1 H0 = swapFragment.H0();
                if (H0 == null) {
                    return;
                }
                H0.b();
                return;
            }
            Context context = swapFragment.getContext();
            if (context != null) {
                e30.a0(context, eVar.a());
            }
            swapFragment.N0().L0().postValue(null);
        }
    }

    public static final void N1(SwapFragment swapFragment, DialogInterface dialogInterface) {
        fs1.f(swapFragment, "this$0");
        swapFragment.N0().W0();
    }

    public static final void O1(SwapFragment swapFragment, Integer num) {
        String obj;
        fs1.f(swapFragment, "this$0");
        db1 db1Var = swapFragment.x0;
        db1 db1Var2 = null;
        if (db1Var == null) {
            fs1.r("binding");
            db1Var = null;
        }
        boolean z = true;
        db1Var.b.setVisibility(e30.m0(num != null && num.intValue() == 1));
        db1 db1Var3 = swapFragment.x0;
        if (db1Var3 == null) {
            fs1.r("binding");
            db1Var3 = null;
        }
        Chip chip = db1Var3.p;
        fs1.e(chip, "binding.requireGasFee");
        chip.setVisibility(8);
        if (num == null || num.intValue() != 1) {
            ck4 ck4Var = swapFragment.H0;
            if (ck4Var == null) {
                fs1.r("bindingSource");
                ck4Var = null;
            }
            Editable text = ck4Var.j.getText();
            String obj2 = (text == null || (obj = text.toString()) == null) ? null : StringsKt__StringsKt.K0(obj).toString();
            if (obj2 != null && obj2.length() != 0) {
                z = false;
            }
            if (z) {
                db1 db1Var4 = swapFragment.x0;
                if (db1Var4 == null) {
                    fs1.r("binding");
                    db1Var4 = null;
                }
                db1Var4.c.setVisibility(0);
                db1 db1Var5 = swapFragment.x0;
                if (db1Var5 == null) {
                    fs1.r("binding");
                    db1Var5 = null;
                }
                db1Var5.d.setVisibility(8);
                db1 db1Var6 = swapFragment.x0;
                if (db1Var6 == null) {
                    fs1.r("binding");
                } else {
                    db1Var2 = db1Var6;
                }
                db1Var2.b.setVisibility(8);
            } else {
                db1 db1Var7 = swapFragment.x0;
                if (db1Var7 == null) {
                    fs1.r("binding");
                    db1Var7 = null;
                }
                db1Var7.c.setVisibility(8);
                db1 db1Var8 = swapFragment.x0;
                if (db1Var8 == null) {
                    fs1.r("binding");
                    db1Var8 = null;
                }
                db1Var8.d.setVisibility(0);
                db1 db1Var9 = swapFragment.x0;
                if (db1Var9 == null) {
                    fs1.r("binding");
                } else {
                    db1Var2 = db1Var9;
                }
                db1Var2.b.setVisibility(8);
            }
        } else {
            db1 db1Var10 = swapFragment.x0;
            if (db1Var10 == null) {
                fs1.r("binding");
                db1Var10 = null;
            }
            db1Var10.c.setVisibility(8);
            db1 db1Var11 = swapFragment.x0;
            if (db1Var11 == null) {
                fs1.r("binding");
            } else {
                db1Var2 = db1Var11;
            }
            db1Var2.d.setVisibility(8);
        }
        if (num != null && num.intValue() == 0) {
            pg4.c();
        }
    }

    public static final void P0(ck4 ck4Var, boolean z) {
        fs1.f(ck4Var, "$this_run");
        MaterialButton materialButton = ck4Var.e;
        fs1.e(materialButton, "btnClearText");
        materialButton.setVisibility(z ? 0 : 8);
    }

    public static final void P1(SwapFragment swapFragment, LoadingState loadingState) {
        fs1.f(swapFragment, "this$0");
        if (loadingState != null && loadingState == LoadingState.Loading) {
            BestMaxFragment a2 = BestMaxFragment.w0.a();
            FragmentManager childFragmentManager = swapFragment.getChildFragmentManager();
            fs1.e(childFragmentManager, "childFragmentManager");
            a2.E(childFragmentManager);
        }
    }

    public static final void Q0(SwapFragment swapFragment, Swap swap) {
        fs1.f(swapFragment, "this$0");
        if (swap == null) {
            return;
        }
        swapFragment.a1();
        ck4 ck4Var = swapFragment.I0;
        if (ck4Var == null) {
            fs1.r("bindingDestination");
            ck4Var = null;
        }
        String str = swap.logoURI;
        fs1.e(str, "it.logoURI");
        if ((str.length() > 0) || swap.imageResource > 0) {
            String str2 = swap.logoURI;
            String str3 = swap.symbol;
            fs1.e(str3, "it.symbol");
            com.bumptech.glide.a.u(ck4Var.i).x(kt.f(new i00(str2, str3, Integer.valueOf(swap.imageResource)))).d0(swapFragment.l0, swapFragment.m0).a(n73.v0()).I0(ck4Var.i);
        }
        MaterialTextView materialTextView = ck4Var.n;
        materialTextView.setText(((Object) swap.name) + " (" + ((Object) swap.symbol) + ')');
        SwapViewModel.a value = swapFragment.N0().F0().getValue();
        if (value == null || value.b()) {
            return;
        }
        ck4Var.j.setText("");
        gb2<SwapViewModel.a> F0 = swapFragment.N0().F0();
        BigDecimal bigDecimal = BigDecimal.ZERO;
        fs1.e(bigDecimal, "ZERO");
        F0.postValue(new SwapViewModel.a(false, bigDecimal));
    }

    public static final void Q1(SwapFragment swapFragment, c cVar) {
        boolean z;
        fs1.f(swapFragment, "this$0");
        if (cVar == null) {
            return;
        }
        db1 db1Var = swapFragment.x0;
        if (db1Var == null) {
            fs1.r("binding");
            db1Var = null;
        }
        MaterialButton materialButton = db1Var.e;
        if (cVar.a() != null) {
            RoomToken a2 = cVar.a();
            fs1.d(a2);
            if (a2.getNativeBalance() > Utils.DOUBLE_EPSILON && cVar.b() != null) {
                SwapViewModel.c b2 = cVar.b();
                fs1.d(b2);
                if (b2.a() == TokenType.BEP_20) {
                    z = true;
                    materialButton.setVisibility(e30.m0(z));
                }
            }
        }
        z = false;
        materialButton.setVisibility(e30.m0(z));
    }

    public static final void R0(SwapFragment swapFragment, BigDecimal bigDecimal) {
        fs1.f(swapFragment, "this$0");
        ck4 ck4Var = swapFragment.I0;
        if (ck4Var == null) {
            fs1.r("bindingDestination");
            ck4Var = null;
        }
        if (bigDecimal != null) {
            AppCompatTextView appCompatTextView = ck4Var.g;
            String string = swapFragment.getString(R.string.text_swap_coin_bal);
            fs1.e(string, "getString(R.string.text_swap_coin_bal)");
            double doubleValue = bigDecimal.doubleValue();
            Context requireContext = swapFragment.requireContext();
            fs1.e(requireContext, "requireContext()");
            String format = String.format(string, Arrays.copyOf(new Object[]{e30.y(doubleValue, requireContext)}, 1));
            fs1.e(format, "java.lang.String.format(this, *args)");
            appCompatTextView.setText(format);
            return;
        }
        ck4Var.g.setText(swapFragment.getString(R.string.loading));
    }

    public static final void R1(SwapFragment swapFragment) {
        SwapViewModel.a value;
        fs1.f(swapFragment, "this$0");
        if (swapFragment.getChildFragmentManager().j0(R.id.content_main) != null || (value = swapFragment.N0().F0().getValue()) == null) {
            return;
        }
        ck4 ck4Var = null;
        if (value.b()) {
            ck4 ck4Var2 = swapFragment.H0;
            if (ck4Var2 == null) {
                fs1.r("bindingSource");
            } else {
                ck4Var = ck4Var2;
            }
            ck4Var.j.requestFocus();
            return;
        }
        ck4 ck4Var3 = swapFragment.I0;
        if (ck4Var3 == null) {
            fs1.r("bindingDestination");
        } else {
            ck4Var = ck4Var3;
        }
        ck4Var.j.requestFocus();
    }

    public static final void S0(SwapFragment swapFragment, Double d2) {
        fs1.f(swapFragment, "this$0");
        ck4 ck4Var = swapFragment.I0;
        if (ck4Var == null) {
            fs1.r("bindingDestination");
            ck4Var = null;
        }
        double d3 = Utils.DOUBLE_EPSILON;
        if (fs1.a(d2, Utils.DOUBLE_EPSILON)) {
            ck4Var.l.setVisibility(4);
        } else {
            ck4Var.l.setVisibility(0);
        }
        MaterialTextView materialTextView = ck4Var.l;
        Object[] objArr = new Object[2];
        objArr[0] = u21.a.b();
        if (d2 != null) {
            d3 = d2.doubleValue();
        }
        objArr[1] = e30.p(v21.a(d3), 5, null, false, 6, null);
        materialTextView.setText(swapFragment.getString(R.string.text_swap_current_price, objArr));
    }

    public static final void U0(ck4 ck4Var, boolean z) {
        fs1.f(ck4Var, "$this_run");
        MaterialButton materialButton = ck4Var.e;
        fs1.e(materialButton, "btnClearText");
        materialButton.setVisibility(z ? 0 : 8);
    }

    public static final void U1(BigDecimal bigDecimal, SwapFragment swapFragment, BigDecimal bigDecimal2) {
        fs1.f(swapFragment, "this$0");
        fs1.e(bigDecimal, "balance");
        if (bigDecimal2 == null) {
            bigDecimal2 = BigDecimal.ZERO;
        }
        fs1.e(bigDecimal2, "it?: BigDecimal.ZERO");
        BigDecimal subtract = bigDecimal.subtract(bigDecimal2);
        fs1.e(subtract, "this.subtract(other)");
        double doubleValue = subtract.doubleValue();
        if (doubleValue > Utils.DOUBLE_EPSILON) {
            ck4 ck4Var = swapFragment.H0;
            if (ck4Var == null) {
                fs1.r("bindingSource");
                ck4Var = null;
            }
            ck4Var.j.setText(e30.j(doubleValue));
        } else {
            FragmentActivity activity = swapFragment.getActivity();
            if (activity != null) {
                String string = swapFragment.getResources().getString(R.string.swap_error_insufficient_balance);
                fs1.e(string, "resources.getString(R.st…ror_insufficient_balance)");
                e30.b0(activity, string);
            }
        }
        swapFragment.E0();
    }

    public static final void V0(SwapFragment swapFragment, Swap swap) {
        fs1.f(swapFragment, "this$0");
        if (swap == null) {
            return;
        }
        swapFragment.a1();
        ck4 ck4Var = swapFragment.H0;
        if (ck4Var == null) {
            fs1.r("bindingSource");
            ck4Var = null;
        }
        String str = swap.logoURI;
        fs1.e(str, "it.logoURI");
        if ((str.length() > 0) || swap.imageResource > 0) {
            String str2 = swap.logoURI;
            String str3 = swap.symbol;
            fs1.e(str3, "it.symbol");
            com.bumptech.glide.a.u(ck4Var.i).x(kt.f(new i00(str2, str3, Integer.valueOf(swap.imageResource)))).d0(swapFragment.l0, swapFragment.m0).a(n73.v0()).I0(ck4Var.i);
        }
        MaterialTextView materialTextView = ck4Var.n;
        materialTextView.setText(((Object) swap.name) + " (" + ((Object) swap.symbol) + ')');
        SwapViewModel.a value = swapFragment.N0().F0().getValue();
        if (value != null && value.b()) {
            ck4Var.j.setText("");
            gb2<SwapViewModel.a> F0 = swapFragment.N0().F0();
            BigDecimal bigDecimal = BigDecimal.ZERO;
            fs1.e(bigDecimal, "ZERO");
            F0.postValue(new SwapViewModel.a(true, bigDecimal));
        }
    }

    public static final void W0(SwapFragment swapFragment, BigDecimal bigDecimal) {
        fs1.f(swapFragment, "this$0");
        ck4 ck4Var = swapFragment.H0;
        if (ck4Var == null) {
            fs1.r("bindingSource");
            ck4Var = null;
        }
        if (bigDecimal != null) {
            AppCompatTextView appCompatTextView = ck4Var.g;
            String string = swapFragment.getString(R.string.text_swap_coin_bal);
            fs1.e(string, "getString(R.string.text_swap_coin_bal)");
            double doubleValue = bigDecimal.doubleValue();
            Context requireContext = swapFragment.requireContext();
            fs1.e(requireContext, "requireContext()");
            String format = String.format(string, Arrays.copyOf(new Object[]{e30.y(doubleValue, requireContext)}, 1));
            fs1.e(format, "java.lang.String.format(this, *args)");
            appCompatTextView.setText(format);
            return;
        }
        ck4Var.g.setText(swapFragment.getString(R.string.loading));
    }

    public static final void X0(SwapFragment swapFragment, Double d2) {
        fs1.f(swapFragment, "this$0");
        ck4 ck4Var = swapFragment.H0;
        if (ck4Var == null) {
            fs1.r("bindingSource");
            ck4Var = null;
        }
        double d3 = Utils.DOUBLE_EPSILON;
        if (fs1.a(d2, Utils.DOUBLE_EPSILON)) {
            ck4Var.l.setVisibility(4);
        } else {
            ck4Var.l.setVisibility(0);
        }
        MaterialTextView materialTextView = ck4Var.l;
        Object[] objArr = new Object[2];
        objArr[0] = u21.a.b();
        if (d2 != null) {
            d3 = d2.doubleValue();
        }
        objArr[1] = e30.p(v21.a(d3), 5, null, false, 6, null);
        materialTextView.setText(swapFragment.getString(R.string.text_swap_current_price, objArr));
    }

    public static final boolean b1(SwapFragment swapFragment, View view) {
        fs1.f(swapFragment, "this$0");
        wb wbVar = new wb(swapFragment.J0());
        Context requireContext = swapFragment.requireContext();
        fs1.e(requireContext, "requireContext()");
        fs1.e(view, "it");
        db1 db1Var = swapFragment.x0;
        if (db1Var == null) {
            fs1.r("binding");
            db1Var = null;
        }
        wbVar.g(requireContext, view, db1Var.o);
        return true;
    }

    public static final void c1(SwapFragment swapFragment, View view) {
        fs1.f(swapFragment, "this$0");
        AnchorSwitchWallet anchorSwitchWallet = new AnchorSwitchWallet(swapFragment.I0(), R.id.navigation_swap);
        Context requireContext = swapFragment.requireContext();
        fs1.e(requireContext, "requireContext()");
        fs1.e(view, "it");
        db1 db1Var = swapFragment.x0;
        if (db1Var == null) {
            fs1.r("binding");
            db1Var = null;
        }
        anchorSwitchWallet.h(requireContext, view, db1Var.o);
    }

    public static final void d1(SwapFragment swapFragment, Double d2) {
        fs1.f(swapFragment, "this$0");
        if (d2 != null) {
            db1 db1Var = swapFragment.x0;
            db1 db1Var2 = null;
            if (db1Var == null) {
                fs1.r("binding");
                db1Var = null;
            }
            TextView textView = db1Var.t;
            fs1.e(textView, "binding.txtSymbol");
            e30.W(textView);
            db1 db1Var3 = swapFragment.x0;
            if (db1Var3 == null) {
                fs1.r("binding");
            } else {
                db1Var2 = db1Var3;
            }
            MaterialTextView materialTextView = db1Var2.r;
            fs1.e(materialTextView, "binding.tvTotal");
            e30.N(materialTextView, d2.doubleValue(), false);
        }
    }

    /* JADX WARN: Code restructure failed: missing block: B:47:0x0097, code lost:
        if (defpackage.fs1.b(r0 == null ? null : r0.getSymbol(), r13.p0) != false) goto L31;
     */
    /* JADX WARN: Removed duplicated region for block: B:100:0x015b  */
    /* JADX WARN: Removed duplicated region for block: B:103:0x0174  */
    /* JADX WARN: Removed duplicated region for block: B:179:0x0215 A[EDGE_INSN: B:179:0x0215->B:139:0x0215 ?: BREAK  , SYNTHETIC] */
    /* JADX WARN: Removed duplicated region for block: B:182:0x028f A[SYNTHETIC] */
    /* JADX WARN: Removed duplicated region for block: B:186:? A[RETURN, SYNTHETIC] */
    /* JADX WARN: Removed duplicated region for block: B:30:0x005d  */
    /* JADX WARN: Removed duplicated region for block: B:67:0x00e9  */
    /* JADX WARN: Removed duplicated region for block: B:80:0x0106  */
    /*
        Code decompiled incorrectly, please refer to instructions dump.
        To view partially-correct code enable 'Show inconsistent code' option in preferences
    */
    public static final void e1(net.safemoon.androidwallet.fragments.SwapFragment r13, java.util.List r14) {
        /*
            Method dump skipped, instructions count: 717
            To view this dump change 'Code comments level' option to 'DEBUG'
        */
        throw new UnsupportedOperationException("Method not decompiled: net.safemoon.androidwallet.fragments.SwapFragment.e1(net.safemoon.androidwallet.fragments.SwapFragment, java.util.List):void");
    }

    public static final void f1(SwapFragment swapFragment, View view) {
        fs1.f(swapFragment, "this$0");
        swapFragment.requireActivity().onBackPressed();
    }

    public static final void g1(gb2 gb2Var, RoomToken roomToken) {
        fs1.f(gb2Var, "$btnV2LiveData");
        c cVar = (c) gb2Var.getValue();
        if (cVar == null) {
            cVar = new c(null, null);
        }
        cVar.c(roomToken);
        te4 te4Var = te4.a;
        gb2Var.setValue(cVar);
    }

    public static final void h1(SwapFragment swapFragment, View view) {
        ck4 ck4Var;
        Object obj;
        fs1.f(swapFragment, "this$0");
        Iterator<T> it = swapFragment.K0.iterator();
        while (true) {
            ck4Var = null;
            if (!it.hasNext()) {
                obj = null;
                break;
            }
            obj = it.next();
            String str = (String) obj;
            Swap value = swapFragment.N0().A0().getValue();
            if (fs1.b(str, value == null ? null : value.symbol)) {
                break;
            }
        }
        if (((String) obj) == null) {
            SwapViewModel N0 = swapFragment.N0();
            ck4 ck4Var2 = swapFragment.H0;
            if (ck4Var2 == null) {
                fs1.r("bindingSource");
                ck4Var2 = null;
            }
            ck4 ck4Var3 = swapFragment.I0;
            if (ck4Var3 == null) {
                fs1.r("bindingDestination");
            } else {
                ck4Var = ck4Var3;
            }
            N0.Z0(ck4Var2, ck4Var);
            return;
        }
        FragmentActivity requireActivity = swapFragment.requireActivity();
        fs1.e(requireActivity, "requireActivity()");
        String string = swapFragment.getString(R.string.warning_cant_swap);
        fs1.e(string, "getString(R.string.warning_cant_swap)");
        jc0.d(requireActivity, null, string, true, null);
    }

    public static final void i1(SwapFragment swapFragment, BigDecimal bigDecimal) {
        fs1.f(swapFragment, "this$0");
        if (bigDecimal == null) {
            return;
        }
        swapFragment.N0().v0().postValue(null);
        ck4 ck4Var = swapFragment.H0;
        if (ck4Var == null) {
            fs1.r("bindingSource");
            ck4Var = null;
        }
        ck4Var.j.requestFocus();
        ck4 ck4Var2 = swapFragment.H0;
        if (ck4Var2 == null) {
            fs1.r("bindingSource");
            ck4Var2 = null;
        }
        ck4Var2.j.setText(e30.h0(bigDecimal, 0, 1, null));
    }

    public static final void j1(SwapFragment swapFragment, Double d2) {
        fs1.f(swapFragment, "this$0");
        db1 db1Var = swapFragment.x0;
        if (db1Var == null) {
            fs1.r("binding");
            db1Var = null;
        }
        Chip chip = db1Var.i;
        lu3 lu3Var = lu3.a;
        Locale locale = Locale.getDefault();
        String string = swapFragment.getString(R.string.text_swap_slippage);
        fs1.e(string, "getString(R.string.text_swap_slippage)");
        fs1.e(d2, "it");
        String format = String.format(locale, string, Arrays.copyOf(new Object[]{e30.m(d2.doubleValue())}, 1));
        fs1.e(format, "java.lang.String.format(locale, format, *args)");
        chip.setText(format);
    }

    public static final void k1(SwapFragment swapFragment, Gas gas) {
        fs1.f(swapFragment, "this$0");
        int i = gas == null ? -1 : b.a[gas.ordinal()];
        db1 db1Var = null;
        if (i == 1) {
            db1 db1Var2 = swapFragment.x0;
            if (db1Var2 == null) {
                fs1.r("binding");
            } else {
                db1Var = db1Var2;
            }
            db1Var.j.setText(swapFragment.getString(R.string.swap_speed_standard));
        } else if (i == 2) {
            db1 db1Var3 = swapFragment.x0;
            if (db1Var3 == null) {
                fs1.r("binding");
            } else {
                db1Var = db1Var3;
            }
            db1Var.j.setText(swapFragment.getString(R.string.swap_speed_fast));
        } else if (i != 3) {
        } else {
            db1 db1Var4 = swapFragment.x0;
            if (db1Var4 == null) {
                fs1.r("binding");
            } else {
                db1Var = db1Var4;
            }
            db1Var.j.setText(swapFragment.getString(R.string.swap_speed_lightning));
        }
    }

    public static final void l1(SwapFragment swapFragment, Integer num) {
        fs1.f(swapFragment, "this$0");
        db1 db1Var = swapFragment.x0;
        if (db1Var == null) {
            fs1.r("binding");
            db1Var = null;
        }
        Chip chip = db1Var.h;
        lu3 lu3Var = lu3.a;
        Locale locale = Locale.getDefault();
        String string = swapFragment.getString(R.string.text_swap_min_txn);
        fs1.e(string, "getString(R.string.text_swap_min_txn)");
        String format = String.format(locale, string, Arrays.copyOf(new Object[]{num}, 1));
        fs1.e(format, "java.lang.String.format(locale, format, *args)");
        chip.setText(format);
    }

    public static final void m1(SwapFragment swapFragment, SwapViewModel.d dVar) {
        fs1.f(swapFragment, "this$0");
        db1 db1Var = swapFragment.x0;
        if (db1Var == null) {
            fs1.r("binding");
            db1Var = null;
        }
        db1Var.d.setEnabled((dVar == null || fs1.b(dVar.b(), "ERROR")) ? false : true);
    }

    public static final void n1(SwapFragment swapFragment, String str) {
        fs1.f(swapFragment, "this$0");
        db1 db1Var = swapFragment.x0;
        db1 db1Var2 = null;
        if (db1Var == null) {
            fs1.r("binding");
            db1Var = null;
        }
        MaterialTextView materialTextView = db1Var.s;
        fs1.e(str, "it");
        materialTextView.setVisibility(str.length() == 0 ? 8 : 0);
        db1 db1Var3 = swapFragment.x0;
        if (db1Var3 == null) {
            fs1.r("binding");
        } else {
            db1Var2 = db1Var3;
        }
        db1Var2.s.setText(str);
    }

    public static final void o1(SwapFragment swapFragment, View view) {
        fs1.f(swapFragment, "this$0");
        SwapSlipPage a2 = SwapSlipPage.w0.a();
        FragmentManager childFragmentManager = swapFragment.getChildFragmentManager();
        fs1.e(childFragmentManager, "childFragmentManager");
        a2.O(childFragmentManager);
    }

    public static final void p1(SwapFragment swapFragment, View view) {
        fs1.f(swapFragment, "this$0");
        SwapTransactionSpeed a2 = SwapTransactionSpeed.x0.a();
        FragmentManager childFragmentManager = swapFragment.getChildFragmentManager();
        fs1.e(childFragmentManager, "childFragmentManager");
        a2.F(childFragmentManager);
    }

    public static final void q1(SwapFragment swapFragment, View view) {
        fs1.f(swapFragment, "this$0");
        SwapTransactionTimeLimit a2 = SwapTransactionTimeLimit.w0.a();
        FragmentManager childFragmentManager = swapFragment.getChildFragmentManager();
        fs1.e(childFragmentManager, "childFragmentManager");
        a2.E(childFragmentManager);
    }

    public static final void r1(SwapFragment swapFragment, View view) {
        fs1.f(swapFragment, "this$0");
        swapFragment.N0().F();
    }

    public static final void s1(SwapFragment swapFragment, View view) {
        fs1.f(swapFragment, "this$0");
        no0 a2 = no0.x0.a(new SwapFragment$onViewCreated$24$1(swapFragment), null);
        FragmentManager childFragmentManager = swapFragment.getChildFragmentManager();
        fs1.e(childFragmentManager, "childFragmentManager");
        a2.D(childFragmentManager);
    }

    public static final void t1(SwapFragment swapFragment, String str) {
        fs1.f(swapFragment, "this$0");
        db1 db1Var = null;
        if (str != null) {
            db1 db1Var2 = swapFragment.x0;
            if (db1Var2 == null) {
                fs1.r("binding");
                db1Var2 = null;
            }
            MaterialButton materialButton = db1Var2.d;
            fs1.e(materialButton, "binding.btnSwap");
            materialButton.setVisibility(8);
            db1 db1Var3 = swapFragment.x0;
            if (db1Var3 == null) {
                fs1.r("binding");
                db1Var3 = null;
            }
            Chip chip = db1Var3.p;
            fs1.e(chip, "binding.requireGasFee");
            chip.setVisibility(0);
            db1 db1Var4 = swapFragment.x0;
            if (db1Var4 == null) {
                fs1.r("binding");
            } else {
                db1Var = db1Var4;
            }
            db1Var.p.setText(str);
            return;
        }
        db1 db1Var5 = swapFragment.x0;
        if (db1Var5 == null) {
            fs1.r("binding");
            db1Var5 = null;
        }
        MaterialButton materialButton2 = db1Var5.d;
        fs1.e(materialButton2, "binding.btnSwap");
        materialButton2.setVisibility(0);
        db1 db1Var6 = swapFragment.x0;
        if (db1Var6 == null) {
            fs1.r("binding");
        } else {
            db1Var = db1Var6;
        }
        Chip chip2 = db1Var.p;
        fs1.e(chip2, "binding.requireGasFee");
        chip2.setVisibility(8);
    }

    public static final void u1(SwapFragment swapFragment, LoadingState loadingState) {
        fs1.f(swapFragment, "this$0");
        if (loadingState == null) {
            return;
        }
        if (loadingState == LoadingState.Loading) {
            String string = swapFragment.getResources().getString(R.string.approving);
            fs1.e(string, "resources.getString(R.string.approving)");
            String string2 = swapFragment.getResources().getString(R.string.please_wait);
            fs1.e(string2, "resources.getString(R.string.please_wait)");
            swapFragment.V1(false, string, string2);
            return;
        }
        swapFragment.E0();
    }

    public static final void v1(SwapFragment swapFragment, View view) {
        fs1.f(swapFragment, "$this_run");
        swapFragment.T1(swapFragment.q0);
    }

    public static final void w1(SwapFragment swapFragment, View view) {
        fs1.f(swapFragment, "$this_run");
        swapFragment.T1(swapFragment.r0);
    }

    public static final void x1(SwapFragment swapFragment, View view) {
        fs1.f(swapFragment, "$this_run");
        swapFragment.T1(swapFragment.s0);
    }

    public static final void y1(SwapFragment swapFragment, View view) {
        fs1.f(swapFragment, "$this_run");
        swapFragment.T1(swapFragment.t0);
    }

    public static final void z1(SwapFragment swapFragment, View view) {
        fs1.f(swapFragment, "$this_run");
        pg4.e(swapFragment.requireActivity());
        swapFragment.S1(SelectCurrencyFragment.n0.a(SelectCurrencyFragment.SWAPPATH.Source), true);
    }

    public final void B0() {
        final ck4 ck4Var = this.H0;
        final ck4 ck4Var2 = null;
        if (ck4Var == null) {
            fs1.r("bindingSource");
            ck4Var = null;
        }
        ck4Var.e.setOnClickListener(new View.OnClickListener() { // from class: dy3
            @Override // android.view.View.OnClickListener
            public final void onClick(View view) {
                SwapFragment.C0(ck4.this, view);
            }
        });
        ck4 ck4Var3 = this.I0;
        if (ck4Var3 == null) {
            fs1.r("bindingDestination");
        } else {
            ck4Var2 = ck4Var3;
        }
        ck4Var2.e.setOnClickListener(new View.OnClickListener() { // from class: ey3
            @Override // android.view.View.OnClickListener
            public final void onClick(View view) {
                SwapFragment.D0(ck4.this, view);
            }
        });
    }

    public final void E0() {
        ProgressLoading progressLoading = this.L0;
        if (progressLoading == null) {
            return;
        }
        progressLoading.h();
    }

    public final UserTokenItemDisplayModel F0() {
        return (UserTokenItemDisplayModel) this.F0.getValue();
    }

    public final HomeViewModel G0() {
        return (HomeViewModel) this.y0.getValue();
    }

    public final gm1 H0() {
        return (gm1) this.G0.getValue();
    }

    public final MultiWalletViewModel I0() {
        return (MultiWalletViewModel) this.z0.getValue();
    }

    public final MyTokensListViewModel J0() {
        return (MyTokensListViewModel) this.B0.getValue();
    }

    public final SwapFragment$onSelectTokenTypeClickListener$2.a K0() {
        return (SwapFragment$onSelectTokenTypeClickListener$2.a) this.E0.getValue();
    }

    public final TokenType L0() {
        boolean z;
        Map<String, TokenType> M02 = M0();
        boolean z2 = true;
        if (!M02.isEmpty()) {
            for (Map.Entry<String, TokenType> entry : M02.entrySet()) {
                TokenType value = entry.getValue();
                Context requireContext = requireContext();
                fs1.e(requireContext, "requireContext()");
                if (value == e30.e(requireContext)) {
                    z = true;
                    continue;
                } else {
                    z = false;
                    continue;
                }
                if (z) {
                    break;
                }
            }
        }
        z2 = false;
        if (z2) {
            Context requireContext2 = requireContext();
            fs1.e(requireContext2, "requireContext()");
            return e30.e(requireContext2);
        }
        return TokenType.Companion.a("BNB");
    }

    public final Map<String, TokenType> M0() {
        return (Map) this.D0.getValue();
    }

    public final SwapViewModel N0() {
        return (SwapViewModel) this.A0.getValue();
    }

    public final void O0() {
        final ck4 ck4Var = this.I0;
        if (ck4Var == null) {
            fs1.r("bindingDestination");
            ck4Var = null;
        }
        ck4Var.j.c(new AutofitEdittext.a() { // from class: cz3
            @Override // net.safemoon.androidwallet.views.editText.autoSize.AutofitEdittext.a
            public final void a(boolean z) {
                SwapFragment.P0(ck4.this, z);
            }
        });
        LiveData a2 = cb4.a(N0().b0());
        fs1.e(a2, "Transformations.distinctUntilChanged(this)");
        a2.observe(getViewLifecycleOwner(), new tl2() { // from class: wx3
            @Override // defpackage.tl2
            public final void onChanged(Object obj) {
                SwapFragment.Q0(SwapFragment.this, (Swap) obj);
            }
        });
        N0().a0().observe(getViewLifecycleOwner(), new tl2() { // from class: ox3
            @Override // defpackage.tl2
            public final void onChanged(Object obj) {
                SwapFragment.R0(SwapFragment.this, (BigDecimal) obj);
            }
        });
        N0().c0().observe(getViewLifecycleOwner(), new tl2() { // from class: hx3
            @Override // defpackage.tl2
            public final void onChanged(Object obj) {
                SwapFragment.S0(SwapFragment.this, (Double) obj);
            }
        });
    }

    public final void S1(Fragment fragment, boolean z) {
        j n = getChildFragmentManager().n();
        fs1.e(n, "childFragmentManager.beginTransaction()");
        fs1.d(fragment);
        n.s(R.id.content_main, fragment);
        if (z) {
            n.h(null);
        }
        n.j();
    }

    public final void T0() {
        final ck4 ck4Var = this.H0;
        if (ck4Var == null) {
            fs1.r("bindingSource");
            ck4Var = null;
        }
        ck4Var.j.c(new AutofitEdittext.a() { // from class: bz3
            @Override // net.safemoon.androidwallet.views.editText.autoSize.AutofitEdittext.a
            public final void a(boolean z) {
                SwapFragment.U0(ck4.this, z);
            }
        });
        LiveData a2 = cb4.a(N0().A0());
        fs1.e(a2, "Transformations.distinctUntilChanged(this)");
        a2.observe(getViewLifecycleOwner(), new tl2() { // from class: xx3
            @Override // defpackage.tl2
            public final void onChanged(Object obj) {
                SwapFragment.V0(SwapFragment.this, (Swap) obj);
            }
        });
        N0().z0().observe(getViewLifecycleOwner(), new tl2() { // from class: nx3
            @Override // defpackage.tl2
            public final void onChanged(Object obj) {
                SwapFragment.W0(SwapFragment.this, (BigDecimal) obj);
            }
        });
        N0().B0().observe(getViewLifecycleOwner(), new tl2() { // from class: dz3
            @Override // defpackage.tl2
            public final void onChanged(Object obj) {
                SwapFragment.X0(SwapFragment.this, (Double) obj);
            }
        });
    }

    public final void T1(float f) {
        BigDecimal value = N0().z0().getValue();
        if (value == null) {
            return;
        }
        ck4 ck4Var = this.H0;
        ck4 ck4Var2 = null;
        if (ck4Var == null) {
            fs1.r("bindingSource");
            ck4Var = null;
        }
        ck4Var.j.requestFocus();
        pg4.f(requireContext());
        Swap value2 = N0().A0().getValue();
        fs1.d(value2);
        fs1.e(value2, "swapViewModel.sourceLiveData.value!!");
        Swap swap = value2;
        final BigDecimal multiply = value.multiply(new BigDecimal(String.valueOf(f / this.j0)));
        if (!swap.symbol.equals("BNB") && !swap.symbol.equals("ETH")) {
            String j = e30.j(multiply.doubleValue());
            fs1.e(j, "balance.toDouble().decimal8()");
            if (e30.K(j) > Utils.DOUBLE_EPSILON) {
                ck4 ck4Var3 = this.H0;
                if (ck4Var3 == null) {
                    fs1.r("bindingSource");
                } else {
                    ck4Var2 = ck4Var3;
                }
                ck4Var2.j.setText(e30.j(multiply.doubleValue()));
                return;
            }
            FragmentActivity activity = getActivity();
            if (activity == null) {
                return;
            }
            String string = getResources().getString(R.string.swap_error_insufficient_balance);
            fs1.e(string, "resources.getString(R.st…ror_insufficient_balance)");
            e30.b0(activity, string);
            return;
        }
        if (f == this.t0) {
            String string2 = getResources().getString(R.string.loading);
            fs1.e(string2, "resources.getString(R.string.loading)");
            String string3 = getResources().getString(R.string.please_wait);
            fs1.e(string3, "resources.getString(R.string.please_wait)");
            V1(false, string2, string3);
            SwapViewModel N0 = N0();
            fs1.e(multiply, "balance");
            up3<BigDecimal> up3Var = new up3<>();
            rz1 viewLifecycleOwner = getViewLifecycleOwner();
            fs1.e(viewLifecycleOwner, "viewLifecycleOwner");
            up3Var.observe(viewLifecycleOwner, new tl2() { // from class: cy3
                @Override // defpackage.tl2
                public final void onChanged(Object obj) {
                    SwapFragment.U1(multiply, this, (BigDecimal) obj);
                }
            });
            te4 te4Var = te4.a;
            N0.K(multiply, up3Var);
            return;
        }
        BigDecimal multiply2 = value.subtract(new BigDecimal(this.k0)).multiply(new BigDecimal(String.valueOf(f / this.j0)));
        if (multiply.compareTo(value) > 0) {
            multiply = multiply2;
        }
        String j2 = e30.j(multiply.doubleValue());
        fs1.e(j2, "safetyMargin.toDouble().decimal8()");
        if (e30.K(j2) > Utils.DOUBLE_EPSILON) {
            ck4 ck4Var4 = this.H0;
            if (ck4Var4 == null) {
                fs1.r("bindingSource");
            } else {
                ck4Var2 = ck4Var4;
            }
            ck4Var2.j.setText(e30.j(multiply.doubleValue()));
            return;
        }
        FragmentActivity activity2 = getActivity();
        if (activity2 == null) {
            return;
        }
        String string4 = getResources().getString(R.string.swap_error_insufficient_balance);
        fs1.e(string4, "resources.getString(R.st…ror_insufficient_balance)");
        e30.b0(activity2, string4);
    }

    public final void V1(boolean z, String str, String str2) {
        ProgressLoading progressLoading;
        ProgressLoading progressLoading2 = this.L0;
        boolean z2 = false;
        if (progressLoading2 != null && progressLoading2.isVisible()) {
            z2 = true;
        }
        if (z2 && (progressLoading = this.L0) != null) {
            progressLoading.h();
        }
        ProgressLoading a2 = ProgressLoading.y0.a(z, str, str2);
        this.L0 = a2;
        if (a2 == null) {
            return;
        }
        FragmentManager childFragmentManager = getChildFragmentManager();
        fs1.e(childFragmentManager, "childFragmentManager");
        a2.A(childFragmentManager);
    }

    public final void Y0(boolean z) {
        boolean z2 = !z;
        db1 db1Var = this.x0;
        ck4 ck4Var = null;
        if (db1Var == null) {
            fs1.r("binding");
            db1Var = null;
        }
        db1Var.l.setEnabled(z2);
        db1 db1Var2 = this.x0;
        if (db1Var2 == null) {
            fs1.r("binding");
            db1Var2 = null;
        }
        db1Var2.i.setEnabled(z2);
        db1 db1Var3 = this.x0;
        if (db1Var3 == null) {
            fs1.r("binding");
            db1Var3 = null;
        }
        db1Var3.j.setEnabled(z2);
        db1 db1Var4 = this.x0;
        if (db1Var4 == null) {
            fs1.r("binding");
            db1Var4 = null;
        }
        db1Var4.h.setEnabled(z2);
        ck4 ck4Var2 = this.H0;
        if (ck4Var2 == null) {
            fs1.r("bindingSource");
            ck4Var2 = null;
        }
        ck4Var2.f.setEnabled(z2);
        ck4 ck4Var3 = this.I0;
        if (ck4Var3 == null) {
            fs1.r("bindingDestination");
        } else {
            ck4Var = ck4Var3;
        }
        ck4Var.f.setEnabled(z2);
    }

    public final void Z0(boolean z) {
        boolean z2 = !z;
        Y0(z);
        ck4 ck4Var = this.H0;
        if (ck4Var == null) {
            fs1.r("bindingSource");
            ck4Var = null;
        }
        ck4Var.j.setEnabled(z2);
        ck4Var.a.setEnabled(z2);
        ck4Var.b.setEnabled(z2);
        ck4Var.c.setEnabled(z2);
        ck4Var.d.setEnabled(z2);
    }

    public final void a1() {
        TokenType a2;
        Swap value = N0().A0().getValue();
        Swap value2 = N0().b0().getValue();
        SwapViewModel.c value3 = N0().Z().getValue();
        Integer num = null;
        if (value3 != null && (a2 = value3.a()) != null) {
            num = Integer.valueOf(a2.getChainId());
        }
        if (value != null && value2 != null && fs1.b(value.chainId, num) && fs1.b(value2.chainId, num) && !fs1.b(value.symbol, value2.symbol)) {
            double B = e30.B(qc4.a(value, value2));
            double d2 = M0;
            if (B > d2) {
                N0().w0().setValue(Double.valueOf(B));
                if (bo3.e(requireContext(), "SWAP_PAIR_DONT_SHOW_ME", true)) {
                    try {
                        if (!isVisible()) {
                            return;
                        }
                    } catch (Exception e2) {
                        e2.printStackTrace();
                    }
                    SwapAutoSlippage a3 = SwapAutoSlippage.w0.a();
                    FragmentManager childFragmentManager = getChildFragmentManager();
                    fs1.e(childFragmentManager, "childFragmentManager");
                    a3.D(childFragmentManager);
                    return;
                }
                return;
            }
            N0().w0().setValue(Double.valueOf(d2));
            return;
        }
        N0().w0().setValue(Double.valueOf(M0));
    }

    @Override // androidx.fragment.app.Fragment
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        List<TokenType> list = this.C0;
        Context requireContext = requireContext();
        fs1.e(requireContext, "requireContext()");
        if (list.contains(e30.e(requireContext))) {
            this.u0 = false;
        }
    }

    @Override // androidx.fragment.app.Fragment
    public View onCreateView(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle) {
        fs1.f(layoutInflater, "inflater");
        return LayoutInflater.from(requireContext()).inflate(R.layout.fragment_swap, viewGroup, false);
    }

    @Override // androidx.fragment.app.Fragment
    public void onDetach() {
        super.onDetach();
        try {
            N0().k1();
            if (N0().l0().getValue() == LoadingState.Normal) {
                N0().G();
            }
        } catch (Exception e2) {
            e2.printStackTrace();
        }
    }

    @Override // androidx.fragment.app.Fragment
    public void onPause() {
        super.onPause();
    }

    @Override // androidx.fragment.app.Fragment
    public void onResume() {
        super.onResume();
    }

    @Override // net.safemoon.androidwallet.fragments.common.BaseMainFragment, defpackage.qn, androidx.fragment.app.Fragment
    public void onViewCreated(View view, Bundle bundle) {
        fs1.f(view, "view");
        super.onViewCreated(view, bundle);
        db1 a2 = db1.a(view);
        fs1.e(a2, "bind(view)");
        this.x0 = a2;
        SwapViewModel N0 = N0();
        rz1 viewLifecycleOwner = getViewLifecycleOwner();
        fs1.e(viewLifecycleOwner, "viewLifecycleOwner");
        N0.b1(viewLifecycleOwner);
        db1 db1Var = null;
        N0().L0().postValue(null);
        N0().F0().postValue(null);
        gb2<Double> N = N0().N();
        Double valueOf = Double.valueOf((double) Utils.DOUBLE_EPSILON);
        N.postValue(valueOf);
        N0().M().postValue(valueOf);
        Wallet.Companion companion = Wallet.Companion;
        String j = bo3.j(getContext(), "SAFEMOON_ACTIVE_WALLET", "");
        fs1.e(j, "getString(\n             …         \"\"\n            )");
        Wallet wallet2 = companion.toWallet(j);
        String displayName = wallet2 == null ? null : wallet2.displayName();
        db1 db1Var2 = this.x0;
        if (db1Var2 == null) {
            fs1.r("binding");
            db1Var2 = null;
        }
        db1Var2.q.setText(displayName);
        final gb2 gb2Var = new gb2();
        J0().x().observe(getViewLifecycleOwner(), new tl2() { // from class: gx3
            @Override // defpackage.tl2
            public final void onChanged(Object obj) {
                SwapFragment.g1(gb2.this, (RoomToken) obj);
            }
        });
        N0().Z().observe(getViewLifecycleOwner(), new tl2() { // from class: rx3
            @Override // defpackage.tl2
            public final void onChanged(Object obj) {
                SwapFragment.E1(gb2.this, this, (SwapViewModel.c) obj);
            }
        });
        gb2Var.observe(getViewLifecycleOwner(), new tl2() { // from class: qx3
            @Override // defpackage.tl2
            public final void onChanged(Object obj) {
                SwapFragment.Q1(SwapFragment.this, (SwapFragment.c) obj);
            }
        });
        gb2<SwapViewModel.c> Z = N0().Z();
        SwapViewModel.c value = N0().Z().getValue();
        if (value == null) {
            value = null;
        } else {
            value.b(L0());
            te4 te4Var = te4.a;
        }
        Z.postValue(value);
        FragmentActivity requireActivity = requireActivity();
        requireActivity.getWindow().addFlags(Integer.MIN_VALUE);
        requireActivity.getWindow().setStatusBarColor(m70.d(requireActivity, 17170445));
        requireActivity.getWindow().setBackgroundDrawableResource(R.drawable.bottom_curve);
        te4 te4Var2 = te4.a;
        db1 db1Var3 = this.x0;
        if (db1Var3 == null) {
            fs1.r("binding");
            db1Var3 = null;
        }
        ck4 a3 = ck4.a(db1Var3.g.findViewById(R.id.parent));
        fs1.e(a3, "bind(binding.ccSource.findViewById(R.id.parent))");
        this.H0 = a3;
        db1 db1Var4 = this.x0;
        if (db1Var4 == null) {
            fs1.r("binding");
            db1Var4 = null;
        }
        ck4 a4 = ck4.a(db1Var4.f.findViewById(R.id.parent));
        fs1.e(a4, "bind(binding.ccDestinati…indViewById(R.id.parent))");
        this.I0 = a4;
        ck4 ck4Var = this.H0;
        if (ck4Var == null) {
            fs1.r("bindingSource");
            ck4Var = null;
        }
        ck4Var.j.setText("");
        ck4 ck4Var2 = this.I0;
        if (ck4Var2 == null) {
            fs1.r("bindingDestination");
            ck4Var2 = null;
        }
        ck4Var2.j.setText("");
        db1 db1Var5 = this.x0;
        if (db1Var5 == null) {
            fs1.r("binding");
            db1Var5 = null;
        }
        db1Var5.n.a.setOnClickListener(K0());
        db1 db1Var6 = this.x0;
        if (db1Var6 == null) {
            fs1.r("binding");
            db1Var6 = null;
        }
        MaterialTextView materialTextView = db1Var6.r;
        fs1.e(materialTextView, "binding.tvTotal");
        e30.X(materialTextView, new SwapFragment$onViewCreated$7(this));
        db1 db1Var7 = this.x0;
        if (db1Var7 == null) {
            fs1.r("binding");
            db1Var7 = null;
        }
        db1Var7.r.setOnLongClickListener(new View.OnLongClickListener() { // from class: zy3
            @Override // android.view.View.OnLongClickListener
            public final boolean onLongClick(View view2) {
                boolean b1;
                b1 = SwapFragment.b1(SwapFragment.this, view2);
                return b1;
            }
        });
        db1 db1Var8 = this.x0;
        if (db1Var8 == null) {
            fs1.r("binding");
            db1Var8 = null;
        }
        db1Var8.q.setOnClickListener(new View.OnClickListener() { // from class: uy3
            @Override // android.view.View.OnClickListener
            public final void onClick(View view2) {
                SwapFragment.c1(SwapFragment.this, view2);
            }
        });
        J0().B().observe(getViewLifecycleOwner(), new tl2() { // from class: gz3
            @Override // defpackage.tl2
            public final void onChanged(Object obj) {
                SwapFragment.d1(SwapFragment.this, (Double) obj);
            }
        });
        N0().E0().observe(getViewLifecycleOwner(), new tl2() { // from class: px3
            @Override // defpackage.tl2
            public final void onChanged(Object obj) {
                SwapFragment.e1(SwapFragment.this, (List) obj);
            }
        });
        db1 db1Var9 = this.x0;
        if (db1Var9 == null) {
            fs1.r("binding");
            db1Var9 = null;
        }
        AppCompatImageView appCompatImageView = db1Var9.m;
        fs1.e(appCompatImageView, "binding.ivBack");
        do3 do3Var = do3.a;
        Context requireContext = requireContext();
        fs1.e(requireContext, "requireContext()");
        appCompatImageView.setVisibility(do3Var.b(requireContext) != R.id.navigation_swap ? 0 : 8);
        db1 db1Var10 = this.x0;
        if (db1Var10 == null) {
            fs1.r("binding");
            db1Var10 = null;
        }
        db1Var10.m.setOnClickListener(new View.OnClickListener() { // from class: sy3
            @Override // android.view.View.OnClickListener
            public final void onClick(View view2) {
                SwapFragment.f1(SwapFragment.this, view2);
            }
        });
        T0();
        O0();
        db1 db1Var11 = this.x0;
        if (db1Var11 == null) {
            fs1.r("binding");
            db1Var11 = null;
        }
        db1Var11.l.setOnClickListener(new View.OnClickListener() { // from class: vy3
            @Override // android.view.View.OnClickListener
            public final void onClick(View view2) {
                SwapFragment.h1(SwapFragment.this, view2);
            }
        });
        N0().v0().observe(getViewLifecycleOwner(), new tl2() { // from class: mx3
            @Override // defpackage.tl2
            public final void onChanged(Object obj) {
                SwapFragment.i1(SwapFragment.this, (BigDecimal) obj);
            }
        });
        db1 db1Var12 = this.x0;
        if (db1Var12 == null) {
            fs1.r("binding");
            db1Var12 = null;
        }
        TextView textView = (TextView) db1Var12.b().findViewById(R.id.textScreenName);
        if (textView != null) {
            e30.V(textView);
        }
        N0().w0().observe(getViewLifecycleOwner(), new tl2() { // from class: hz3
            @Override // defpackage.tl2
            public final void onChanged(Object obj) {
                SwapFragment.j1(SwapFragment.this, (Double) obj);
            }
        });
        N0().i0().observe(getViewLifecycleOwner(), new tl2() { // from class: sx3
            @Override // defpackage.tl2
            public final void onChanged(Object obj) {
                SwapFragment.k1(SwapFragment.this, (Gas) obj);
            }
        });
        N0().Y().observe(getViewLifecycleOwner(), new tl2() { // from class: jx3
            @Override // defpackage.tl2
            public final void onChanged(Object obj) {
                SwapFragment.l1(SwapFragment.this, (Integer) obj);
            }
        });
        N0().K0().observe(getViewLifecycleOwner(), new tl2() { // from class: zx3
            @Override // defpackage.tl2
            public final void onChanged(Object obj) {
                SwapFragment.m1(SwapFragment.this, (SwapViewModel.d) obj);
            }
        });
        N0().J0().observe(getViewLifecycleOwner(), new tl2() { // from class: lx3
            @Override // defpackage.tl2
            public final void onChanged(Object obj) {
                SwapFragment.n1(SwapFragment.this, (String) obj);
            }
        });
        db1 db1Var13 = this.x0;
        if (db1Var13 == null) {
            fs1.r("binding");
            db1Var13 = null;
        }
        db1Var13.i.setOnClickListener(new View.OnClickListener() { // from class: py3
            @Override // android.view.View.OnClickListener
            public final void onClick(View view2) {
                SwapFragment.o1(SwapFragment.this, view2);
            }
        });
        db1 db1Var14 = this.x0;
        if (db1Var14 == null) {
            fs1.r("binding");
            db1Var14 = null;
        }
        db1Var14.j.setOnClickListener(new View.OnClickListener() { // from class: oy3
            @Override // android.view.View.OnClickListener
            public final void onClick(View view2) {
                SwapFragment.p1(SwapFragment.this, view2);
            }
        });
        db1 db1Var15 = this.x0;
        if (db1Var15 == null) {
            fs1.r("binding");
            db1Var15 = null;
        }
        db1Var15.h.setOnClickListener(new View.OnClickListener() { // from class: ry3
            @Override // android.view.View.OnClickListener
            public final void onClick(View view2) {
                SwapFragment.q1(SwapFragment.this, view2);
            }
        });
        db1 db1Var16 = this.x0;
        if (db1Var16 == null) {
            fs1.r("binding");
            db1Var16 = null;
        }
        db1Var16.b.setOnClickListener(new View.OnClickListener() { // from class: qy3
            @Override // android.view.View.OnClickListener
            public final void onClick(View view2) {
                SwapFragment.r1(SwapFragment.this, view2);
            }
        });
        db1 db1Var17 = this.x0;
        if (db1Var17 == null) {
            fs1.r("binding");
            db1Var17 = null;
        }
        db1Var17.e.setOnClickListener(new View.OnClickListener() { // from class: ty3
            @Override // android.view.View.OnClickListener
            public final void onClick(View view2) {
                SwapFragment.s1(SwapFragment.this, view2);
            }
        });
        N0().s0().observe(getViewLifecycleOwner(), new tl2() { // from class: kx3
            @Override // defpackage.tl2
            public final void onChanged(Object obj) {
                SwapFragment.t1(SwapFragment.this, (String) obj);
            }
        });
        N0().O().observe(getViewLifecycleOwner(), new tl2() { // from class: ux3
            @Override // defpackage.tl2
            public final void onChanged(Object obj) {
                SwapFragment.u1(SwapFragment.this, (LoadingState) obj);
            }
        });
        ck4 ck4Var3 = this.H0;
        if (ck4Var3 == null) {
            fs1.r("bindingSource");
            ck4Var3 = null;
        }
        ck4Var3.a.setOnClickListener(new View.OnClickListener() { // from class: fy3
            @Override // android.view.View.OnClickListener
            public final void onClick(View view2) {
                SwapFragment.v1(SwapFragment.this, view2);
            }
        });
        ck4Var3.b.setOnClickListener(new View.OnClickListener() { // from class: iy3
            @Override // android.view.View.OnClickListener
            public final void onClick(View view2) {
                SwapFragment.w1(SwapFragment.this, view2);
            }
        });
        ck4Var3.c.setOnClickListener(new View.OnClickListener() { // from class: wy3
            @Override // android.view.View.OnClickListener
            public final void onClick(View view2) {
                SwapFragment.x1(SwapFragment.this, view2);
            }
        });
        ck4Var3.d.setOnClickListener(new View.OnClickListener() { // from class: hy3
            @Override // android.view.View.OnClickListener
            public final void onClick(View view2) {
                SwapFragment.y1(SwapFragment.this, view2);
            }
        });
        ck4Var3.f.setOnClickListener(new View.OnClickListener() { // from class: gy3
            @Override // android.view.View.OnClickListener
            public final void onClick(View view2) {
                SwapFragment.z1(SwapFragment.this, view2);
            }
        });
        ck4Var3.j.setFilters(new InputFilter[]{new xq1(valueOf, Double.valueOf(Double.MAX_VALUE))});
        AutofitEdittext autofitEdittext = ck4Var3.j;
        b30 b30Var = b30.a;
        autofitEdittext.setHint(getString(R.string.hint_0, Character.valueOf(b30Var.y())));
        ck4Var3.h.setOnClickListener(new View.OnClickListener() { // from class: ly3
            @Override // android.view.View.OnClickListener
            public final void onClick(View view2) {
                SwapFragment.A1(SwapFragment.this, view2);
            }
        });
        ck4 ck4Var4 = this.I0;
        if (ck4Var4 == null) {
            fs1.r("bindingDestination");
            ck4Var4 = null;
        }
        ck4Var4.a.setVisibility(4);
        ck4Var4.b.setVisibility(4);
        ck4Var4.c.setVisibility(4);
        ck4Var4.d.setVisibility(4);
        ck4Var4.f.setOnClickListener(new View.OnClickListener() { // from class: ky3
            @Override // android.view.View.OnClickListener
            public final void onClick(View view2) {
                SwapFragment.B1(SwapFragment.this, view2);
            }
        });
        ck4Var4.j.setFilters(new InputFilter[]{new xq1(valueOf, Double.valueOf(Double.MAX_VALUE))});
        ck4Var4.j.setHint(getString(R.string.hint_0, Character.valueOf(b30Var.y())));
        ck4Var4.h.setOnClickListener(new View.OnClickListener() { // from class: my3
            @Override // android.view.View.OnClickListener
            public final void onClick(View view2) {
                SwapFragment.C1(SwapFragment.this, view2);
            }
        });
        N0().F0().observe(getViewLifecycleOwner(), new tl2() { // from class: yx3
            @Override // defpackage.tl2
            public final void onChanged(Object obj) {
                SwapFragment.D1(SwapFragment.this, (SwapViewModel.a) obj);
            }
        });
        N0().N().observe(getViewLifecycleOwner(), new tl2() { // from class: ez3
            @Override // defpackage.tl2
            public final void onChanged(Object obj) {
                SwapFragment.F1(SwapFragment.this, (Double) obj);
            }
        });
        N0().t0().observe(getViewLifecycleOwner(), new tl2() { // from class: ny3
            @Override // defpackage.tl2
            public final void onChanged(Object obj) {
                SwapFragment.G1(SwapFragment.this, (Boolean) obj);
            }
        });
        N0().M().observe(getViewLifecycleOwner(), new tl2() { // from class: fz3
            @Override // defpackage.tl2
            public final void onChanged(Object obj) {
                SwapFragment.H1(SwapFragment.this, (Double) obj);
            }
        });
        N0().T().observe(getViewLifecycleOwner(), new tl2() { // from class: yy3
            @Override // defpackage.tl2
            public final void onChanged(Object obj) {
                SwapFragment.I1(SwapFragment.this, (Boolean) obj);
            }
        });
        N0().l0().observe(getViewLifecycleOwner(), new tl2() { // from class: vx3
            @Override // defpackage.tl2
            public final void onChanged(Object obj) {
                SwapFragment.J1(SwapFragment.this, (LoadingState) obj);
            }
        });
        B0();
        z44 z44Var = new z44(new e(), this.i0);
        z44 z44Var2 = new z44(new d(), this.i0);
        ck4 ck4Var5 = this.H0;
        if (ck4Var5 == null) {
            fs1.r("bindingSource");
            ck4Var5 = null;
        }
        AutofitEdittext autofitEdittext2 = ck4Var5.j;
        autofitEdittext2.setEnabled(true);
        autofitEdittext2.setFocusableInTouchMode(true);
        autofitEdittext2.setFocusable(true);
        ck4 ck4Var6 = this.H0;
        if (ck4Var6 == null) {
            fs1.r("bindingSource");
            ck4Var6 = null;
        }
        AutofitEdittext autofitEdittext3 = ck4Var6.j;
        ck4 ck4Var7 = this.H0;
        if (ck4Var7 == null) {
            fs1.r("bindingSource");
            ck4Var7 = null;
        }
        AutofitEdittext autofitEdittext4 = ck4Var7.j;
        fs1.e(autofitEdittext4, "bindingSource.newValue");
        autofitEdittext3.addTextChangedListener(new cj2(autofitEdittext4));
        ck4 ck4Var8 = this.H0;
        if (ck4Var8 == null) {
            fs1.r("bindingSource");
            ck4Var8 = null;
        }
        ck4Var8.j.addTextChangedListener(z44Var);
        ck4 ck4Var9 = this.I0;
        if (ck4Var9 == null) {
            fs1.r("bindingDestination");
            ck4Var9 = null;
        }
        AutofitEdittext autofitEdittext5 = ck4Var9.j;
        ck4 ck4Var10 = this.I0;
        if (ck4Var10 == null) {
            fs1.r("bindingDestination");
            ck4Var10 = null;
        }
        AutofitEdittext autofitEdittext6 = ck4Var10.j;
        fs1.e(autofitEdittext6, "bindingDestination.newValue");
        autofitEdittext5.addTextChangedListener(new cj2(autofitEdittext6));
        ck4 ck4Var11 = this.I0;
        if (ck4Var11 == null) {
            fs1.r("bindingDestination");
            ck4Var11 = null;
        }
        ck4Var11.j.addTextChangedListener(z44Var2);
        db1 db1Var18 = this.x0;
        if (db1Var18 == null) {
            fs1.r("binding");
        } else {
            db1Var = db1Var18;
        }
        db1Var.d.setOnClickListener(new View.OnClickListener() { // from class: jy3
            @Override // android.view.View.OnClickListener
            public final void onClick(View view2) {
                SwapFragment.K1(SwapFragment.this, view2);
            }
        });
        N0().L0().observe(getViewLifecycleOwner(), new tl2() { // from class: ay3
            @Override // defpackage.tl2
            public final void onChanged(Object obj) {
                SwapFragment.M1(SwapFragment.this, (SwapViewModel.e) obj);
            }
        });
        N0().Q().observe(getViewLifecycleOwner(), new tl2() { // from class: ix3
            @Override // defpackage.tl2
            public final void onChanged(Object obj) {
                SwapFragment.O1(SwapFragment.this, (Integer) obj);
            }
        });
        t40.b(N0().g0()).observe(getViewLifecycleOwner(), new tl2() { // from class: tx3
            @Override // defpackage.tl2
            public final void onChanged(Object obj) {
                SwapFragment.P1(SwapFragment.this, (LoadingState) obj);
            }
        });
        getChildFragmentManager().i(new FragmentManager.m() { // from class: az3
            @Override // androidx.fragment.app.FragmentManager.m
            public final void onBackStackChanged() {
                SwapFragment.R1(SwapFragment.this);
            }
        });
    }
}
