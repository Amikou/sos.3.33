package net.safemoon.androidwallet.fragments;

import android.os.Bundle;
import android.text.Editable;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.fragment.app.FragmentViewModelLazyKt;
import androidx.recyclerview.widget.RecyclerView;
import java.util.List;
import net.safemoon.androidwallet.R;
import net.safemoon.androidwallet.fragments.ManageContactsFragment;
import net.safemoon.androidwallet.fragments.common.BaseMainFragment;
import net.safemoon.androidwallet.model.contact.abstraction.IContact;
import net.safemoon.androidwallet.viewmodels.ContactViewModel;

/* compiled from: ManageContactsFragment.kt */
/* loaded from: classes2.dex */
public final class ManageContactsFragment extends BaseMainFragment {
    public la1 i0;
    public final sy1 j0 = FragmentViewModelLazyKt.a(this, d53.b(ContactViewModel.class), new ManageContactsFragment$special$$inlined$viewModels$default$2(new ManageContactsFragment$special$$inlined$viewModels$default$1(this)), new ManageContactsFragment$contactViewModel$2(this));
    public final sy1 k0 = zy1.a(new ManageContactsFragment$adapter$2(this));

    /* compiled from: ManageContactsFragment.kt */
    /* loaded from: classes2.dex */
    public static final class a extends vu0 {
        public a() {
        }

        @Override // defpackage.vu0, android.text.TextWatcher
        public void afterTextChanged(Editable editable) {
            ManageContactsFragment.this.v().g(String.valueOf(editable));
        }
    }

    /* compiled from: ManageContactsFragment.kt */
    /* loaded from: classes2.dex */
    public static final class b extends RecyclerView.i {
        public b() {
        }

        @Override // androidx.recyclerview.widget.RecyclerView.i
        public void onChanged() {
            super.onChanged();
            la1 la1Var = null;
            if (ManageContactsFragment.this.v().getItemCount() == 0) {
                la1 la1Var2 = ManageContactsFragment.this.i0;
                if (la1Var2 == null) {
                    fs1.r("binding");
                    la1Var2 = null;
                }
                TextView textView = la1Var2.e;
                fs1.e(textView, "binding.tvNoItemsMsg");
                textView.setVisibility(0);
                la1 la1Var3 = ManageContactsFragment.this.i0;
                if (la1Var3 == null) {
                    fs1.r("binding");
                    la1Var3 = null;
                }
                if (String.valueOf(la1Var3.c.b.getText()).length() == 0) {
                    la1 la1Var4 = ManageContactsFragment.this.i0;
                    if (la1Var4 == null) {
                        fs1.r("binding");
                    } else {
                        la1Var = la1Var4;
                    }
                    la1Var.e.setText(ManageContactsFragment.this.getText(R.string.contact_list_no_data));
                    return;
                }
                la1 la1Var5 = ManageContactsFragment.this.i0;
                if (la1Var5 == null) {
                    fs1.r("binding");
                } else {
                    la1Var = la1Var5;
                }
                la1Var.e.setText(ManageContactsFragment.this.getText(R.string.list_no_data));
                return;
            }
            la1 la1Var6 = ManageContactsFragment.this.i0;
            if (la1Var6 == null) {
                fs1.r("binding");
            } else {
                la1Var = la1Var6;
            }
            TextView textView2 = la1Var.e;
            fs1.e(textView2, "binding.tvNoItemsMsg");
            textView2.setVisibility(8);
        }
    }

    public static final void A(ManageContactsFragment manageContactsFragment, View view) {
        fs1.f(manageContactsFragment, "this$0");
        ce2 a2 = n32.a();
        fs1.e(a2, "actionManageContactsFragmentToAddContactFragment()");
        manageContactsFragment.g(a2);
    }

    public static final boolean C(ManageContactsFragment manageContactsFragment, TextView textView, int i, KeyEvent keyEvent) {
        fs1.f(manageContactsFragment, "this$0");
        if (i == 6) {
            pg4.e(manageContactsFragment.requireActivity());
            return true;
        }
        return false;
    }

    public static final void x(ManageContactsFragment manageContactsFragment, List list) {
        List<IContact> l;
        fs1.f(manageContactsFragment, "this$0");
        if (list == null || (l = manageContactsFragment.w().l()) == null) {
            return;
        }
        manageContactsFragment.v().f(l);
    }

    public static final void z(ManageContactsFragment manageContactsFragment, View view) {
        fs1.f(manageContactsFragment, "this$0");
        manageContactsFragment.f();
    }

    public final void B() {
        la1 la1Var = this.i0;
        la1 la1Var2 = null;
        if (la1Var == null) {
            fs1.r("binding");
            la1Var = null;
        }
        la1Var.c.b.setOnEditorActionListener(new TextView.OnEditorActionListener() { // from class: m32
            @Override // android.widget.TextView.OnEditorActionListener
            public final boolean onEditorAction(TextView textView, int i, KeyEvent keyEvent) {
                boolean C;
                C = ManageContactsFragment.C(ManageContactsFragment.this, textView, i, keyEvent);
                return C;
            }
        });
        la1 la1Var3 = this.i0;
        if (la1Var3 == null) {
            fs1.r("binding");
            la1Var3 = null;
        }
        la1Var3.c.b.addTextChangedListener(new z44(new a(), 0L, 2, null));
        la1 la1Var4 = this.i0;
        if (la1Var4 == null) {
            fs1.r("binding");
        } else {
            la1Var2 = la1Var4;
        }
        la1Var2.b.setAdapter(v());
        v().registerAdapterDataObserver(new b());
    }

    @Override // androidx.fragment.app.Fragment
    public View onCreateView(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle) {
        fs1.f(layoutInflater, "inflater");
        la1 a2 = la1.a(layoutInflater.inflate(R.layout.fragment_manage_contacts, viewGroup, false));
        fs1.e(a2, "bind(\n            inflat…e\n            )\n        )");
        this.i0 = a2;
        if (a2 == null) {
            fs1.r("binding");
            a2 = null;
        }
        ConstraintLayout b2 = a2.b();
        fs1.e(b2, "binding.root");
        return b2;
    }

    @Override // net.safemoon.androidwallet.fragments.common.BaseMainFragment, defpackage.qn, androidx.fragment.app.Fragment
    public void onViewCreated(View view, Bundle bundle) {
        fs1.f(view, "view");
        super.onViewCreated(view, bundle);
        y();
        B();
        w().j().observe(getViewLifecycleOwner(), new tl2() { // from class: j32
            @Override // defpackage.tl2
            public final void onChanged(Object obj) {
                ManageContactsFragment.x(ManageContactsFragment.this, (List) obj);
            }
        });
    }

    public final i32 v() {
        return (i32) this.k0.getValue();
    }

    public final ContactViewModel w() {
        return (ContactViewModel) this.j0.getValue();
    }

    public final void y() {
        la1 la1Var = this.i0;
        la1 la1Var2 = null;
        if (la1Var == null) {
            fs1.r("binding");
            la1Var = null;
        }
        la1Var.d.c.setOnClickListener(new View.OnClickListener() { // from class: k32
            @Override // android.view.View.OnClickListener
            public final void onClick(View view) {
                ManageContactsFragment.z(ManageContactsFragment.this, view);
            }
        });
        la1 la1Var3 = this.i0;
        if (la1Var3 == null) {
            fs1.r("binding");
            la1Var3 = null;
        }
        la1Var3.d.e.setText(getText(R.string.manage_contacts_title));
        la1 la1Var4 = this.i0;
        if (la1Var4 == null) {
            fs1.r("binding");
        } else {
            la1Var2 = la1Var4;
        }
        la1Var2.d.b.setOnClickListener(new View.OnClickListener() { // from class: l32
            @Override // android.view.View.OnClickListener
            public final void onClick(View view) {
                ManageContactsFragment.A(ManageContactsFragment.this, view);
            }
        });
    }
}
