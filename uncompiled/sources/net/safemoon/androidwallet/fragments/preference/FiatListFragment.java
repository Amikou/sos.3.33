package net.safemoon.androidwallet.fragments.preference;

import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.fragment.app.FragmentViewModelLazyKt;
import androidx.recyclerview.widget.LinearLayoutManager;
import java.util.List;
import net.safemoon.androidwallet.R;
import net.safemoon.androidwallet.dialogs.ProgressLoading;
import net.safemoon.androidwallet.fragments.common.BaseMainFragment;
import net.safemoon.androidwallet.fragments.preference.FiatListFragment;
import net.safemoon.androidwallet.fragments.preference.FiatListFragment$defaultCurrencyAdapter$2;
import net.safemoon.androidwallet.model.common.HistoryListItem;
import net.safemoon.androidwallet.viewmodels.SelectFiatViewModel;

/* compiled from: FiatListFragment.kt */
/* loaded from: classes2.dex */
public final class FiatListFragment extends BaseMainFragment {
    public da1 i0;
    public final sy1 j0 = FragmentViewModelLazyKt.a(this, d53.b(SelectFiatViewModel.class), new FiatListFragment$special$$inlined$activityViewModels$1(this), new FiatListFragment$selectFiatViewModel$2(this));
    public final sy1 k0 = zy1.a(new FiatListFragment$loader$2(this));
    public final sy1 l0 = zy1.a(new FiatListFragment$defaultCurrencyAdapter$2(this));
    public String m0 = "";

    /* compiled from: FiatListFragment.kt */
    /* loaded from: classes2.dex */
    public static final class a implements TextWatcher {
        public a() {
        }

        @Override // android.text.TextWatcher
        public void afterTextChanged(Editable editable) {
            FiatListFragment fiatListFragment = FiatListFragment.this;
            CharSequence charSequence = editable;
            if (editable == null) {
                charSequence = "";
            }
            fiatListFragment.C(charSequence.toString());
            List<HistoryListItem> k = FiatListFragment.this.y().k(FiatListFragment.this.x());
            da1 da1Var = FiatListFragment.this.i0;
            if (da1Var == null) {
                fs1.r("binding");
                da1Var = null;
            }
            da1Var.e.setVisibility(k.size() == 0 ? 0 : 8);
            FiatListFragment.this.v().d(k);
        }

        @Override // android.text.TextWatcher
        public void beforeTextChanged(CharSequence charSequence, int i, int i2, int i3) {
        }

        @Override // android.text.TextWatcher
        public void onTextChanged(CharSequence charSequence, int i, int i2, int i3) {
        }
    }

    public static final void A(FiatListFragment fiatListFragment, View view) {
        fs1.f(fiatListFragment, "this$0");
        fiatListFragment.f();
    }

    public static final void B(FiatListFragment fiatListFragment, List list) {
        fs1.f(fiatListFragment, "this$0");
        if (list == null) {
            return;
        }
        fiatListFragment.v().d(fiatListFragment.y().k(fiatListFragment.x()));
    }

    public final void C(String str) {
        fs1.f(str, "<set-?>");
        this.m0 = str;
    }

    @Override // androidx.fragment.app.Fragment
    public View onCreateView(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle) {
        fs1.f(layoutInflater, "inflater");
        da1 a2 = da1.a(layoutInflater.inflate(R.layout.fragment_fiat_list, viewGroup, false));
        fs1.e(a2, "bind(inflater.inflate(R.…_list, container, false))");
        this.i0 = a2;
        if (a2 == null) {
            fs1.r("binding");
            a2 = null;
        }
        ConstraintLayout b = a2.b();
        fs1.e(b, "binding.root");
        return b;
    }

    @Override // net.safemoon.androidwallet.fragments.common.BaseMainFragment, defpackage.qn, androidx.fragment.app.Fragment
    public void onViewCreated(View view, Bundle bundle) {
        fs1.f(view, "view");
        super.onViewCreated(view, bundle);
        da1 da1Var = this.i0;
        if (da1Var == null) {
            fs1.r("binding");
            da1Var = null;
        }
        da1Var.d.c.setText(R.string.default_currency);
        da1Var.d.a.setOnClickListener(new View.OnClickListener() { // from class: c31
            @Override // android.view.View.OnClickListener
            public final void onClick(View view2) {
                FiatListFragment.A(FiatListFragment.this, view2);
            }
        });
        da1Var.c.setLayoutManager(new LinearLayoutManager(getContext(), 1, false));
        da1Var.c.setAdapter(v());
        da1Var.b.addTextChangedListener(new a());
        y().h().observe(getViewLifecycleOwner(), new tl2() { // from class: b31
            @Override // defpackage.tl2
            public final void onChanged(Object obj) {
                FiatListFragment.B(FiatListFragment.this, (List) obj);
            }
        });
    }

    public final FiatListFragment$defaultCurrencyAdapter$2.a v() {
        return (FiatListFragment$defaultCurrencyAdapter$2.a) this.l0.getValue();
    }

    public final ProgressLoading w() {
        return (ProgressLoading) this.k0.getValue();
    }

    public final String x() {
        return this.m0;
    }

    public final SelectFiatViewModel y() {
        return (SelectFiatViewModel) this.j0.getValue();
    }

    public final void z() {
        v().b();
    }
}
