package net.safemoon.androidwallet.fragments.preference;

import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.fragment.app.FragmentViewModelLazyKt;
import androidx.recyclerview.widget.LinearLayoutManager;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.Objects;
import kotlin.text.StringsKt__StringsKt;
import net.safemoon.androidwallet.R;
import net.safemoon.androidwallet.dialogs.ProgressLoading;
import net.safemoon.androidwallet.fragments.common.BaseMainFragment;
import net.safemoon.androidwallet.fragments.preference.DefaultLanguageFragment;
import net.safemoon.androidwallet.fragments.preference.DefaultLanguageFragment$defaultLanguageAdapter$2;
import net.safemoon.androidwallet.model.common.LanguageItem;
import net.safemoon.androidwallet.repository.FcmDataSource;
import net.safemoon.androidwallet.viewmodels.MultiWalletViewModel;

/* compiled from: DefaultLanguageFragment.kt */
/* loaded from: classes2.dex */
public final class DefaultLanguageFragment extends BaseMainFragment {
    public ba1 i0;
    public final sy1 j0 = FragmentViewModelLazyKt.a(this, d53.b(MultiWalletViewModel.class), new DefaultLanguageFragment$special$$inlined$viewModels$default$2(new DefaultLanguageFragment$special$$inlined$viewModels$default$1(this)), null);
    public final sy1 k0 = zy1.a(new DefaultLanguageFragment$fcmDataSource$2(this));
    public final sy1 l0 = zy1.a(new DefaultLanguageFragment$loader$2(this));
    public final sy1 m0 = zy1.a(new DefaultLanguageFragment$defaultLanguageAdapter$2(this));
    public String n0 = "";

    /* compiled from: DefaultLanguageFragment.kt */
    /* loaded from: classes2.dex */
    public static final class a implements TextWatcher {
        public a() {
        }

        @Override // android.text.TextWatcher
        public void afterTextChanged(Editable editable) {
            DefaultLanguageFragment defaultLanguageFragment = DefaultLanguageFragment.this;
            CharSequence charSequence = editable;
            if (editable == null) {
                charSequence = "";
            }
            defaultLanguageFragment.A(charSequence.toString());
            DefaultLanguageFragment defaultLanguageFragment2 = DefaultLanguageFragment.this;
            List<LanguageItem> w = defaultLanguageFragment2.w(defaultLanguageFragment2.y());
            ba1 ba1Var = DefaultLanguageFragment.this.i0;
            if (ba1Var == null) {
                fs1.r("binding");
                ba1Var = null;
            }
            ba1Var.e.setVisibility(w.size() == 0 ? 0 : 8);
            DefaultLanguageFragment.this.u().f(w);
        }

        @Override // android.text.TextWatcher
        public void beforeTextChanged(CharSequence charSequence, int i, int i2, int i3) {
        }

        @Override // android.text.TextWatcher
        public void onTextChanged(CharSequence charSequence, int i, int i2, int i3) {
        }
    }

    public static final void z(DefaultLanguageFragment defaultLanguageFragment, View view) {
        fs1.f(defaultLanguageFragment, "this$0");
        defaultLanguageFragment.f();
    }

    public final void A(String str) {
        fs1.f(str, "<set-?>");
        this.n0 = str;
    }

    @Override // androidx.fragment.app.Fragment
    public View onCreateView(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle) {
        fs1.f(layoutInflater, "inflater");
        ba1 a2 = ba1.a(layoutInflater.inflate(R.layout.fragment_default_language, viewGroup, false));
        fs1.e(a2, "bind(inflater.inflate(R.…guage, container, false))");
        this.i0 = a2;
        if (a2 == null) {
            fs1.r("binding");
            a2 = null;
        }
        ConstraintLayout b = a2.b();
        fs1.e(b, "binding.root");
        return b;
    }

    @Override // net.safemoon.androidwallet.fragments.common.BaseMainFragment, defpackage.qn, androidx.fragment.app.Fragment
    public void onViewCreated(View view, Bundle bundle) {
        fs1.f(view, "view");
        super.onViewCreated(view, bundle);
        ba1 ba1Var = this.i0;
        if (ba1Var == null) {
            fs1.r("binding");
            ba1Var = null;
        }
        ba1Var.d.c.setText(R.string.default_language);
        ba1Var.d.a.setOnClickListener(new View.OnClickListener() { // from class: xj0
            @Override // android.view.View.OnClickListener
            public final void onClick(View view2) {
                DefaultLanguageFragment.z(DefaultLanguageFragment.this, view2);
            }
        });
        ba1Var.c.setLayoutManager(new LinearLayoutManager(getContext(), 1, false));
        ba1Var.c.setAdapter(u());
        ba1Var.b.addTextChangedListener(new a());
        u().f(w(y()));
    }

    public final DefaultLanguageFragment$defaultLanguageAdapter$2.a u() {
        return (DefaultLanguageFragment$defaultLanguageAdapter$2.a) this.m0.getValue();
    }

    public final FcmDataSource v() {
        return (FcmDataSource) this.k0.getValue();
    }

    public final List<LanguageItem> w(String str) {
        List<LanguageItem> b = ly1.a.b();
        ArrayList arrayList = new ArrayList();
        for (Object obj : b) {
            LanguageItem languageItem = (LanguageItem) obj;
            String string = requireActivity().getString(languageItem.getTitleResId());
            fs1.e(string, "requireActivity().getString(it.titleResId)");
            String string2 = requireActivity().getString(languageItem.getRegionResId());
            fs1.e(string2, "requireActivity().getString(it.regionResId)");
            Objects.requireNonNull(str, "null cannot be cast to non-null type kotlin.CharSequence");
            boolean z = true;
            if (!(StringsKt__StringsKt.K0(str).toString().length() == 0)) {
                Locale locale = Locale.ROOT;
                String lowerCase = string.toLowerCase(locale);
                fs1.e(lowerCase, "(this as java.lang.Strin….toLowerCase(Locale.ROOT)");
                String lowerCase2 = str.toLowerCase(locale);
                fs1.e(lowerCase2, "(this as java.lang.Strin….toLowerCase(Locale.ROOT)");
                Objects.requireNonNull(lowerCase2, "null cannot be cast to non-null type kotlin.CharSequence");
                if (!StringsKt__StringsKt.M(lowerCase, StringsKt__StringsKt.K0(lowerCase2).toString(), false, 2, null)) {
                    String languageCode = languageItem.getLanguageCode();
                    Objects.requireNonNull(languageCode, "null cannot be cast to non-null type java.lang.String");
                    String lowerCase3 = languageCode.toLowerCase(locale);
                    fs1.e(lowerCase3, "(this as java.lang.Strin….toLowerCase(Locale.ROOT)");
                    String lowerCase4 = str.toLowerCase(locale);
                    fs1.e(lowerCase4, "(this as java.lang.Strin….toLowerCase(Locale.ROOT)");
                    Objects.requireNonNull(lowerCase4, "null cannot be cast to non-null type kotlin.CharSequence");
                    if (!StringsKt__StringsKt.M(lowerCase3, StringsKt__StringsKt.K0(lowerCase4).toString(), false, 2, null)) {
                        String lowerCase5 = string2.toLowerCase(locale);
                        fs1.e(lowerCase5, "(this as java.lang.Strin….toLowerCase(Locale.ROOT)");
                        String lowerCase6 = str.toLowerCase(locale);
                        fs1.e(lowerCase6, "(this as java.lang.Strin….toLowerCase(Locale.ROOT)");
                        Objects.requireNonNull(lowerCase6, "null cannot be cast to non-null type kotlin.CharSequence");
                        if (!StringsKt__StringsKt.M(lowerCase5, StringsKt__StringsKt.K0(lowerCase6).toString(), false, 2, null)) {
                            z = false;
                        }
                    }
                }
            }
            if (z) {
                arrayList.add(obj);
            }
        }
        return j20.m0(arrayList);
    }

    public final ProgressLoading x() {
        return (ProgressLoading) this.l0.getValue();
    }

    public final String y() {
        return this.n0;
    }
}
