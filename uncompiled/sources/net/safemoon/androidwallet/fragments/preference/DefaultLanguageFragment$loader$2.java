package net.safemoon.androidwallet.fragments.preference;

import kotlin.jvm.internal.Lambda;
import net.safemoon.androidwallet.R;
import net.safemoon.androidwallet.dialogs.ProgressLoading;

/* compiled from: DefaultLanguageFragment.kt */
/* loaded from: classes2.dex */
public final class DefaultLanguageFragment$loader$2 extends Lambda implements rc1<ProgressLoading> {
    public final /* synthetic */ DefaultLanguageFragment this$0;

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public DefaultLanguageFragment$loader$2(DefaultLanguageFragment defaultLanguageFragment) {
        super(0);
        this.this$0 = defaultLanguageFragment;
    }

    @Override // defpackage.rc1
    public final ProgressLoading invoke() {
        ProgressLoading.a aVar = ProgressLoading.y0;
        String string = this.this$0.getString(R.string.loading);
        fs1.e(string, "getString(R.string.loading)");
        return aVar.a(false, string, "");
    }
}
