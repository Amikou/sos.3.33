package net.safemoon.androidwallet.fragments.preference;

import kotlin.jvm.internal.Lambda;
import net.safemoon.androidwallet.dialogs.ProgressLoading;

/* compiled from: FiatListFragment.kt */
/* loaded from: classes2.dex */
public final class FiatListFragment$defaultCurrencyAdapter$2$2$onClick$1 extends Lambda implements rc1<te4> {
    public final /* synthetic */ FiatListFragment this$0;

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public FiatListFragment$defaultCurrencyAdapter$2$2$onClick$1(FiatListFragment fiatListFragment) {
        super(0);
        this.this$0 = fiatListFragment;
    }

    @Override // defpackage.rc1
    public /* bridge */ /* synthetic */ te4 invoke() {
        invoke2();
        return te4.a;
    }

    @Override // defpackage.rc1
    /* renamed from: invoke  reason: avoid collision after fix types in other method */
    public final void invoke2() {
        ProgressLoading w;
        w = this.this$0.w();
        w.h();
        if (this.this$0.isVisible()) {
            this.this$0.z();
        }
    }
}
