package net.safemoon.androidwallet.fragments.preference;

import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import com.google.android.material.checkbox.MaterialCheckBox;
import defpackage.pe0;
import net.safemoon.androidwallet.R;
import net.safemoon.androidwallet.fragments.common.BaseMainFragment;
import net.safemoon.androidwallet.fragments.preference.DefaultDateFormatFragment;

/* compiled from: DefaultDateFormatFragment.kt */
/* loaded from: classes2.dex */
public final class DefaultDateFormatFragment extends BaseMainFragment {
    public aa1 i0;

    public static final void q(DefaultDateFormatFragment defaultDateFormatFragment, View view) {
        fs1.f(defaultDateFormatFragment, "this$0");
        defaultDateFormatFragment.f();
    }

    public static final void s(LinearLayout linearLayout, gt1 gt1Var, DefaultDateFormatFragment defaultDateFormatFragment, String str, View view) {
        fs1.f(linearLayout, "$this_with");
        fs1.f(gt1Var, "$this_run");
        fs1.f(defaultDateFormatFragment, "this$0");
        fs1.f(str, "$dateFormat");
        for (View view2 : li4.a(linearLayout)) {
            ((MaterialCheckBox) view2.findViewById(R.id.cbSelect)).setChecked(false);
        }
        gt1Var.b.setChecked(true);
        pe0.a aVar = pe0.a;
        Context requireContext = defaultDateFormatFragment.requireContext();
        fs1.e(requireContext, "requireContext()");
        aVar.d(requireContext, str);
    }

    @Override // androidx.fragment.app.Fragment
    public View onCreateView(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle) {
        fs1.f(layoutInflater, "inflater");
        aa1 a = aa1.a(layoutInflater.inflate(R.layout.fragment_default_date_format, viewGroup, false));
        fs1.e(a, "bind(inflater.inflate(R.…ormat, container, false))");
        this.i0 = a;
        if (a == null) {
            fs1.r("binding");
            a = null;
        }
        return a.b();
    }

    @Override // androidx.fragment.app.Fragment
    public void onResume() {
        super.onResume();
        r();
    }

    @Override // net.safemoon.androidwallet.fragments.common.BaseMainFragment, defpackage.qn, androidx.fragment.app.Fragment
    public void onViewCreated(View view, Bundle bundle) {
        fs1.f(view, "view");
        super.onViewCreated(view, bundle);
        aa1 aa1Var = this.i0;
        if (aa1Var == null) {
            fs1.r("binding");
            aa1Var = null;
        }
        aa1Var.b.c.setText(R.string.screen_title_default_date_format);
        aa1Var.b.a.setOnClickListener(new View.OnClickListener() { // from class: si0
            @Override // android.view.View.OnClickListener
            public final void onClick(View view2) {
                DefaultDateFormatFragment.q(DefaultDateFormatFragment.this, view2);
            }
        });
        r();
    }

    public final void r() {
        aa1 aa1Var = this.i0;
        if (aa1Var == null) {
            fs1.r("binding");
            aa1Var = null;
        }
        final LinearLayout linearLayout = aa1Var.c;
        linearLayout.removeAllViews();
        for (final String str : b20.c(pe0.a.a(), "MM/dd/yyyy", "dd/MM/yyyy", "yyyy/MM/dd")) {
            final gt1 c = gt1.c(getLayoutInflater(), linearLayout, false);
            c.c.setText(str);
            linearLayout.addView(c.b());
            MaterialCheckBox materialCheckBox = c.b;
            pe0.a aVar = pe0.a;
            Context requireContext = requireContext();
            fs1.e(requireContext, "requireContext()");
            materialCheckBox.setChecked(fs1.b(str, aVar.c(requireContext)));
            c.b().setOnClickListener(new View.OnClickListener() { // from class: ri0
                @Override // android.view.View.OnClickListener
                public final void onClick(View view) {
                    DefaultDateFormatFragment.s(linearLayout, c, this, str, view);
                }
            });
        }
    }
}
