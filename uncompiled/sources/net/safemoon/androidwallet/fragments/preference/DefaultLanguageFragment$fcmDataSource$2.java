package net.safemoon.androidwallet.fragments.preference;

import android.app.Application;
import kotlin.jvm.internal.Lambda;
import net.safemoon.androidwallet.repository.FcmDataSource;

/* compiled from: DefaultLanguageFragment.kt */
/* loaded from: classes2.dex */
public final class DefaultLanguageFragment$fcmDataSource$2 extends Lambda implements rc1<FcmDataSource> {
    public final /* synthetic */ DefaultLanguageFragment this$0;

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public DefaultLanguageFragment$fcmDataSource$2(DefaultLanguageFragment defaultLanguageFragment) {
        super(0);
        this.this$0 = defaultLanguageFragment;
    }

    @Override // defpackage.rc1
    public final FcmDataSource invoke() {
        Application application = this.this$0.requireActivity().getApplication();
        fs1.e(application, "requireActivity().application");
        return new FcmDataSource(application);
    }
}
