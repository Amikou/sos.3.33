package net.safemoon.androidwallet.fragments.preference;

import androidx.fragment.app.FragmentManager;
import defpackage.w21;
import kotlin.jvm.internal.Lambda;
import net.safemoon.androidwallet.dialogs.ProgressLoading;
import net.safemoon.androidwallet.model.fiat.gson.Fiat;

/* compiled from: FiatListFragment.kt */
/* loaded from: classes2.dex */
public final class FiatListFragment$defaultCurrencyAdapter$2 extends Lambda implements rc1<a> {
    public final /* synthetic */ FiatListFragment this$0;

    /* compiled from: FiatListFragment.kt */
    /* loaded from: classes2.dex */
    public static final class a extends w21 {
        public a(AnonymousClass2 anonymousClass2) {
            super(anonymousClass2);
        }

        @Override // defpackage.w21
        public Fiat c() {
            return u21.a.a();
        }
    }

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public FiatListFragment$defaultCurrencyAdapter$2(FiatListFragment fiatListFragment) {
        super(0);
        this.this$0 = fiatListFragment;
    }

    /* JADX WARN: Type inference failed for: r0v0, types: [net.safemoon.androidwallet.fragments.preference.FiatListFragment$defaultCurrencyAdapter$2$2] */
    @Override // defpackage.rc1
    public final a invoke() {
        final FiatListFragment fiatListFragment = this.this$0;
        return new a(new w21.a() { // from class: net.safemoon.androidwallet.fragments.preference.FiatListFragment$defaultCurrencyAdapter$2.2
            @Override // defpackage.w21.a
            public void a(Fiat fiat) {
                ProgressLoading w;
                fs1.f(fiat, "item");
                w = FiatListFragment.this.w();
                FragmentManager childFragmentManager = FiatListFragment.this.getChildFragmentManager();
                fs1.e(childFragmentManager, "childFragmentManager");
                w.A(childFragmentManager);
                FiatListFragment.this.y().m(fiat, true, new FiatListFragment$defaultCurrencyAdapter$2$2$onClick$1(FiatListFragment.this));
            }
        });
    }
}
