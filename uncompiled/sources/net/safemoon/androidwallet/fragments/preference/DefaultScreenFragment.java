package net.safemoon.androidwallet.fragments.preference;

import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import com.google.android.material.checkbox.MaterialCheckBox;
import java.util.Iterator;
import kotlin.Pair;
import net.safemoon.androidwallet.MyApplicationClass;
import net.safemoon.androidwallet.R;
import net.safemoon.androidwallet.activity.AKTHomeActivity;
import net.safemoon.androidwallet.fragments.common.BaseMainFragment;
import net.safemoon.androidwallet.fragments.preference.DefaultScreenFragment;

/* compiled from: DefaultScreenFragment.kt */
/* loaded from: classes2.dex */
public final class DefaultScreenFragment extends BaseMainFragment {
    public ca1 i0;

    public static final void q(DefaultScreenFragment defaultScreenFragment, View view) {
        fs1.f(defaultScreenFragment, "this$0");
        defaultScreenFragment.f();
    }

    public static final void s(LinearLayout linearLayout, gt1 gt1Var, Pair pair, View view) {
        fs1.f(linearLayout, "$this_with");
        fs1.f(gt1Var, "$this_run");
        fs1.f(pair, "$pair");
        for (View view2 : li4.a(linearLayout)) {
            ((MaterialCheckBox) view2.findViewById(R.id.cbSelect)).setChecked(false);
        }
        gt1Var.b.setChecked(true);
        do3 do3Var = do3.a;
        Context context = linearLayout.getContext();
        fs1.e(context, "context");
        do3Var.f(context, ((Number) pair.getFirst()).intValue());
        MyApplicationClass.c().h0 = true;
        AKTHomeActivity.S0(linearLayout.getContext(), (Integer) pair.getFirst());
    }

    @Override // androidx.fragment.app.Fragment
    public View onCreateView(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle) {
        fs1.f(layoutInflater, "inflater");
        ca1 a = ca1.a(layoutInflater.inflate(R.layout.fragment_default_screen, viewGroup, false));
        fs1.e(a, "bind(inflater.inflate(R.…creen, container, false))");
        this.i0 = a;
        if (a == null) {
            fs1.r("binding");
            a = null;
        }
        return a.b();
    }

    @Override // androidx.fragment.app.Fragment
    public void onResume() {
        super.onResume();
        r();
    }

    @Override // net.safemoon.androidwallet.fragments.common.BaseMainFragment, defpackage.qn, androidx.fragment.app.Fragment
    public void onViewCreated(View view, Bundle bundle) {
        fs1.f(view, "view");
        super.onViewCreated(view, bundle);
        ca1 ca1Var = this.i0;
        if (ca1Var == null) {
            fs1.r("binding");
            ca1Var = null;
        }
        ca1Var.b.c.setText(R.string.screen_title_default_screen);
        ca1Var.b.a.setOnClickListener(new View.OnClickListener() { // from class: tk0
            @Override // android.view.View.OnClickListener
            public final void onClick(View view2) {
                DefaultScreenFragment.q(DefaultScreenFragment.this, view2);
            }
        });
        r();
    }

    public final void r() {
        ca1 ca1Var = this.i0;
        if (ca1Var == null) {
            fs1.r("binding");
            ca1Var = null;
        }
        final LinearLayout linearLayout = ca1Var.c;
        linearLayout.removeAllViews();
        Iterator<T> it = jd0.a.a().iterator();
        while (it.hasNext()) {
            final Pair pair = (Pair) it.next();
            boolean z = false;
            final gt1 c = gt1.c(getLayoutInflater(), linearLayout, false);
            c.c.setText(((Number) pair.getSecond()).intValue());
            linearLayout.addView(c.b());
            MaterialCheckBox materialCheckBox = c.b;
            int intValue = ((Number) pair.getFirst()).intValue();
            do3 do3Var = do3.a;
            Context context = linearLayout.getContext();
            fs1.e(context, "context");
            if (intValue == do3Var.b(context)) {
                z = true;
            }
            materialCheckBox.setChecked(z);
            c.b().setOnClickListener(new View.OnClickListener() { // from class: sk0
                @Override // android.view.View.OnClickListener
                public final void onClick(View view) {
                    DefaultScreenFragment.s(linearLayout, c, pair, view);
                }
            });
        }
    }
}
