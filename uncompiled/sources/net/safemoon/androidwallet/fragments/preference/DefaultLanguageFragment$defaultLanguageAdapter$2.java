package net.safemoon.androidwallet.fragments.preference;

import android.content.Context;
import androidx.fragment.app.FragmentManager;
import defpackage.ey1;
import kotlin.jvm.internal.Lambda;
import net.safemoon.androidwallet.dialogs.ProgressLoading;
import net.safemoon.androidwallet.model.common.LanguageItem;
import net.safemoon.androidwallet.repository.FcmDataSource;

/* compiled from: DefaultLanguageFragment.kt */
/* loaded from: classes2.dex */
public final class DefaultLanguageFragment$defaultLanguageAdapter$2 extends Lambda implements rc1<a> {
    public final /* synthetic */ DefaultLanguageFragment this$0;

    /* compiled from: DefaultLanguageFragment.kt */
    /* loaded from: classes2.dex */
    public static final class a extends ey1 {
        public final /* synthetic */ DefaultLanguageFragment c;

        /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
        public a(DefaultLanguageFragment defaultLanguageFragment, AnonymousClass2 anonymousClass2) {
            super(anonymousClass2);
            this.c = defaultLanguageFragment;
        }

        @Override // defpackage.ey1
        public String e() {
            do3 do3Var = do3.a;
            Context requireContext = this.c.requireContext();
            fs1.e(requireContext, "requireContext()");
            return do3Var.a(requireContext);
        }
    }

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public DefaultLanguageFragment$defaultLanguageAdapter$2(DefaultLanguageFragment defaultLanguageFragment) {
        super(0);
        this.this$0 = defaultLanguageFragment;
    }

    /* JADX WARN: Type inference failed for: r0v0, types: [net.safemoon.androidwallet.fragments.preference.DefaultLanguageFragment$defaultLanguageAdapter$2$2] */
    @Override // defpackage.rc1
    public final a invoke() {
        final DefaultLanguageFragment defaultLanguageFragment = this.this$0;
        return new a(this.this$0, new ey1.a() { // from class: net.safemoon.androidwallet.fragments.preference.DefaultLanguageFragment$defaultLanguageAdapter$2.2
            @Override // defpackage.ey1.a
            public void a(LanguageItem languageItem) {
                ProgressLoading x;
                FcmDataSource v;
                fs1.f(languageItem, "item");
                x = DefaultLanguageFragment.this.x();
                FragmentManager childFragmentManager = DefaultLanguageFragment.this.getChildFragmentManager();
                fs1.e(childFragmentManager, "childFragmentManager");
                x.A(childFragmentManager);
                v = DefaultLanguageFragment.this.v();
                b30 b30Var = b30.a;
                Context requireContext = DefaultLanguageFragment.this.requireContext();
                fs1.e(requireContext, "requireContext()");
                FcmDataSource.h(v, b30Var.f(requireContext, languageItem.getLanguageCode()), null, false, new DefaultLanguageFragment$defaultLanguageAdapter$2$2$onClick$1(DefaultLanguageFragment.this, languageItem), 6, null);
            }
        });
    }
}
