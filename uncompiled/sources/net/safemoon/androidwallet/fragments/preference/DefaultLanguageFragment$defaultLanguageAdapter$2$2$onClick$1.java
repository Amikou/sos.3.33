package net.safemoon.androidwallet.fragments.preference;

import androidx.fragment.app.FragmentActivity;
import kotlin.jvm.internal.Lambda;
import net.safemoon.androidwallet.R;
import net.safemoon.androidwallet.activity.AKTHomeActivity;
import net.safemoon.androidwallet.dialogs.ProgressLoading;
import net.safemoon.androidwallet.model.common.LanguageItem;

/* compiled from: DefaultLanguageFragment.kt */
/* loaded from: classes2.dex */
public final class DefaultLanguageFragment$defaultLanguageAdapter$2$2$onClick$1 extends Lambda implements tc1<Boolean, te4> {
    public final /* synthetic */ LanguageItem $item;
    public final /* synthetic */ DefaultLanguageFragment this$0;

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public DefaultLanguageFragment$defaultLanguageAdapter$2$2$onClick$1(DefaultLanguageFragment defaultLanguageFragment, LanguageItem languageItem) {
        super(1);
        this.this$0 = defaultLanguageFragment;
        this.$item = languageItem;
    }

    @Override // defpackage.tc1
    public /* bridge */ /* synthetic */ te4 invoke(Boolean bool) {
        invoke(bool.booleanValue());
        return te4.a;
    }

    public final void invoke(boolean z) {
        ProgressLoading x;
        x = this.this$0.x();
        x.h();
        if (z) {
            ly1 ly1Var = ly1.a;
            FragmentActivity requireActivity = this.this$0.requireActivity();
            fs1.e(requireActivity, "requireActivity()");
            ly1Var.c(requireActivity, this.$item);
            AKTHomeActivity.S0(this.this$0.requireContext(), Integer.valueOf((int) R.id.navigation_settings));
        }
    }
}
