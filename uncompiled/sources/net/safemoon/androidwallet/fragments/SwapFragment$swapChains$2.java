package net.safemoon.androidwallet.fragments;

import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import kotlin.jvm.internal.Lambda;
import net.safemoon.androidwallet.common.TokenType;
import net.safemoon.androidwallet.viewmodels.HomeViewModel;

/* compiled from: SwapFragment.kt */
/* loaded from: classes2.dex */
public final class SwapFragment$swapChains$2 extends Lambda implements rc1<Map<String, ? extends TokenType>> {
    public final /* synthetic */ SwapFragment this$0;

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public SwapFragment$swapChains$2(SwapFragment swapFragment) {
        super(0);
        this.this$0 = swapFragment;
    }

    @Override // defpackage.rc1
    public final Map<String, ? extends TokenType> invoke() {
        HomeViewModel G0;
        List list;
        G0 = this.this$0.G0();
        Map<String, TokenType> m = G0.m();
        SwapFragment swapFragment = this.this$0;
        LinkedHashMap linkedHashMap = new LinkedHashMap();
        for (Map.Entry<String, TokenType> entry : m.entrySet()) {
            list = swapFragment.C0;
            if (!list.contains(entry.getValue())) {
                linkedHashMap.put(entry.getKey(), entry.getValue());
            }
        }
        return linkedHashMap;
    }
}
