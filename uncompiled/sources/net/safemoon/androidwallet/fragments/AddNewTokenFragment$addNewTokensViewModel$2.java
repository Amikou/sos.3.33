package net.safemoon.androidwallet.fragments;

import androidx.lifecycle.l;
import java.lang.ref.WeakReference;
import kotlin.jvm.internal.Lambda;

/* compiled from: AddNewTokenFragment.kt */
/* loaded from: classes2.dex */
public final class AddNewTokenFragment$addNewTokensViewModel$2 extends Lambda implements rc1<l.b> {
    public final /* synthetic */ AddNewTokenFragment this$0;

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public AddNewTokenFragment$addNewTokensViewModel$2(AddNewTokenFragment addNewTokenFragment) {
        super(0);
        this.this$0 = addNewTokenFragment;
    }

    /* JADX WARN: Can't rename method to resolve collision */
    @Override // defpackage.rc1
    public final l.b invoke() {
        return new u9(new WeakReference(this.this$0.getContext()));
    }
}
