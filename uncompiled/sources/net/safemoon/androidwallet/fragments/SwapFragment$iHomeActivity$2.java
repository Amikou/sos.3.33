package net.safemoon.androidwallet.fragments;

import kotlin.jvm.internal.Lambda;

/* compiled from: SwapFragment.kt */
/* loaded from: classes2.dex */
public final class SwapFragment$iHomeActivity$2 extends Lambda implements rc1<gm1> {
    public final /* synthetic */ SwapFragment this$0;

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public SwapFragment$iHomeActivity$2(SwapFragment swapFragment) {
        super(0);
        this.this$0 = swapFragment;
    }

    /* JADX WARN: Can't rename method to resolve collision */
    @Override // defpackage.rc1
    public final gm1 invoke() {
        if (this.this$0.requireActivity() instanceof gm1) {
            return (gm1) this.this$0.requireActivity();
        }
        return null;
    }
}
