package net.safemoon.androidwallet.fragments;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.res.Resources;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.PopupWindow;
import android.widget.TextView;
import androidx.appcompat.widget.AppCompatImageView;
import androidx.appcompat.widget.AppCompatTextView;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.fragment.app.FragmentActivity;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentViewModelLazyKt;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;
import com.github.mikephil.charting.utils.Utils;
import com.google.android.material.textview.MaterialTextView;
import defpackage.ea4;
import defpackage.u21;
import java.lang.ref.WeakReference;
import java.util.Arrays;
import java.util.List;
import java.util.Locale;
import kotlin.jvm.internal.Ref$IntRef;
import net.safemoon.androidwallet.R;
import net.safemoon.androidwallet.common.TokenType;
import net.safemoon.androidwallet.dialogs.GraphFragment;
import net.safemoon.androidwallet.fragments.TransferHistoryFragment;
import net.safemoon.androidwallet.fragments.TransferHistoryFragment$transactionAdapter$2;
import net.safemoon.androidwallet.fragments.common.BaseMainFragment;
import net.safemoon.androidwallet.model.Coin;
import net.safemoon.androidwallet.model.common.LoadingState;
import net.safemoon.androidwallet.model.common.PaymentMethod;
import net.safemoon.androidwallet.model.priceAlert.PAToken;
import net.safemoon.androidwallet.model.tokensInfo.CurrencyTokenInfo;
import net.safemoon.androidwallet.model.transaction.history.Result;
import net.safemoon.androidwallet.ui.displayModel.UserTokenItemDisplayModel;
import net.safemoon.androidwallet.viewmodels.HomeViewModel;
import net.safemoon.androidwallet.viewmodels.MyTokensListViewModel;
import net.safemoon.androidwallet.viewmodels.TransactionHistoryViewModel;

/* compiled from: TransferHistoryFragment.kt */
/* loaded from: classes2.dex */
public final class TransferHistoryFragment extends BaseMainFragment {
    public final int i0;
    public jb1 k0;
    public CurrencyTokenInfo m0;
    public boolean s0;
    public boolean t0;
    public final long j0 = 100;
    public final sy1 l0 = FragmentViewModelLazyKt.a(this, d53.b(TransactionHistoryViewModel.class), new TransferHistoryFragment$special$$inlined$viewModels$default$2(new TransferHistoryFragment$special$$inlined$viewModels$default$1(this)), null);
    public final sy1 n0 = zy1.a(new TransferHistoryFragment$userTokenItemDisplayModel$2(this));
    public final sy1 o0 = zy1.a(new TransferHistoryFragment$transferList$2(this));
    public final sy1 p0 = zy1.a(new TransferHistoryFragment$address$2(this));
    public final sy1 q0 = FragmentViewModelLazyKt.a(this, d53.b(MyTokensListViewModel.class), new TransferHistoryFragment$special$$inlined$activityViewModels$1(this), new TransferHistoryFragment$myTokenListViewModel$2(this));
    public final sy1 r0 = FragmentViewModelLazyKt.a(this, d53.b(HomeViewModel.class), new TransferHistoryFragment$special$$inlined$activityViewModels$default$1(this), new TransferHistoryFragment$special$$inlined$activityViewModels$default$2(this));
    public final gb2<Coin> u0 = new gb2<>(null);
    public final sy1 v0 = zy1.a(new TransferHistoryFragment$transactionAdapter$2(this));

    /* compiled from: TransferHistoryFragment.kt */
    /* loaded from: classes2.dex */
    public static final class a {
        public a() {
        }

        public /* synthetic */ a(qi0 qi0Var) {
            this();
        }
    }

    static {
        new a(null);
    }

    public static final void A0(TransferHistoryFragment transferHistoryFragment, View view) {
        fs1.f(transferHistoryFragment, "this$0");
        ea4.e d = ea4.d(transferHistoryFragment.i0());
        fs1.e(d, "actionTransferHistoryFra…serTokenItemDisplayModel)");
        transferHistoryFragment.g(d);
    }

    public static final void B0(TransferHistoryFragment transferHistoryFragment, View view) {
        fs1.f(transferHistoryFragment, "this$0");
        ea4.d c = ea4.c(transferHistoryFragment.i0().getChainId(), transferHistoryFragment.a0());
        fs1.e(c, "actionTransferHistoryFra…ess\n                    )");
        transferHistoryFragment.g(c);
    }

    public static final void C0(TransferHistoryFragment transferHistoryFragment, View view) {
        fs1.f(transferHistoryFragment, "this$0");
        ea4.c b = ea4.b(transferHistoryFragment.i0());
        fs1.e(b, "actionTransferHistoryFra…serTokenItemDisplayModel)");
        transferHistoryFragment.g(b);
    }

    public static final void D0(TransferHistoryFragment transferHistoryFragment, Double d) {
        jb1 jb1Var;
        AppCompatTextView appCompatTextView;
        fs1.f(transferHistoryFragment, "this$0");
        if (d == null || (jb1Var = transferHistoryFragment.k0) == null || (appCompatTextView = jb1Var.q) == null) {
            return;
        }
        e30.T(appCompatTextView, transferHistoryFragment.i0().getNativeBalance(), transferHistoryFragment.i0().getSymbol());
    }

    public static final void H0(PopupWindow popupWindow, TransferHistoryFragment transferHistoryFragment, View view, View view2) {
        fs1.f(popupWindow, "$popupWindow");
        fs1.f(transferHistoryFragment, "this$0");
        fs1.f(view, "$anchor");
        popupWindow.dismiss();
        transferHistoryFragment.O0(view);
    }

    public static final void I0(PopupWindow popupWindow, TransferHistoryFragment transferHistoryFragment, View view, View view2) {
        fs1.f(popupWindow, "$popupWindow");
        fs1.f(transferHistoryFragment, "this$0");
        fs1.f(view, "$anchor");
        popupWindow.dismiss();
        transferHistoryFragment.J0(view);
    }

    public static final void K0(Ref$IntRef ref$IntRef, TransferHistoryFragment transferHistoryFragment, List list, List list2, View view) {
        fs1.f(ref$IntRef, "$selectedIndex");
        fs1.f(transferHistoryFragment, "this$0");
        fs1.f(list, "$listHeader");
        fs1.f(list2, "$listValueTxt");
        ref$IntRef.element = 0;
        transferHistoryFragment.S0(0, list);
        transferHistoryFragment.R0(0, list2);
    }

    public static final void L0(Ref$IntRef ref$IntRef, TransferHistoryFragment transferHistoryFragment, List list, List list2, View view) {
        fs1.f(ref$IntRef, "$selectedIndex");
        fs1.f(transferHistoryFragment, "this$0");
        fs1.f(list, "$listHeader");
        fs1.f(list2, "$listValueTxt");
        ref$IntRef.element = 1;
        transferHistoryFragment.S0(1, list);
        transferHistoryFragment.R0(1, list2);
    }

    public static final void M0(Ref$IntRef ref$IntRef, TransferHistoryFragment transferHistoryFragment, List list, List list2, View view) {
        fs1.f(ref$IntRef, "$selectedIndex");
        fs1.f(transferHistoryFragment, "this$0");
        fs1.f(list, "$listHeader");
        fs1.f(list2, "$listValueTxt");
        ref$IntRef.element = 2;
        transferHistoryFragment.S0(2, list);
        transferHistoryFragment.R0(2, list2);
    }

    public static final void N0(Ref$IntRef ref$IntRef, TransferHistoryFragment transferHistoryFragment, List list, List list2, View view) {
        fs1.f(ref$IntRef, "$selectedIndex");
        fs1.f(transferHistoryFragment, "this$0");
        fs1.f(list, "$listHeader");
        fs1.f(list2, "$listValueTxt");
        ref$IntRef.element = 3;
        transferHistoryFragment.S0(3, list);
        transferHistoryFragment.R0(3, list2);
    }

    public static final void P0(TransferHistoryFragment transferHistoryFragment, String str, View view) {
        fs1.f(transferHistoryFragment, "this$0");
        fs1.f(str, "$contractAddress");
        Context requireContext = transferHistoryFragment.requireContext();
        fs1.e(requireContext, "requireContext()");
        e30.h(requireContext, str);
    }

    public static final boolean Q0(PopupWindow popupWindow, View view, MotionEvent motionEvent) {
        fs1.f(popupWindow, "$popupWindow");
        popupWindow.dismiss();
        return true;
    }

    public static final void T0(List list, int i, TransferHistoryFragment transferHistoryFragment) {
        fs1.f(list, "$listHeader");
        fs1.f(transferHistoryFragment, "this$0");
        int size = list.size();
        if (size <= 0) {
            return;
        }
        int i2 = 0;
        while (true) {
            int i3 = i2 + 1;
            if (i2 == i) {
                ((AppCompatTextView) list.get(i2)).setTextColor(m70.d(transferHistoryFragment.requireContext(), R.color.curve_green));
                ((AppCompatTextView) list.get(i2)).setBackgroundColor(m70.d(transferHistoryFragment.requireContext(), R.color.light_green_wrapper));
            } else {
                ((AppCompatTextView) list.get(i2)).setTextColor(m70.d(transferHistoryFragment.requireContext(), R.color.white));
                ((AppCompatTextView) list.get(i2)).setBackgroundColor(m70.d(transferHistoryFragment.requireContext(), R.color.p1));
            }
            if (i3 >= size) {
                return;
            }
            i2 = i3;
        }
    }

    public static final void k0(TransferHistoryFragment transferHistoryFragment, Boolean bool) {
        fs1.f(transferHistoryFragment, "this$0");
        fs1.e(bool, "result");
        if (bool.booleanValue()) {
            UserTokenItemDisplayModel i0 = transferHistoryFragment.i0();
            TransactionHistoryViewModel g0 = transferHistoryFragment.g0();
            String contractAddress = i0.getContractAddress();
            if (contractAddress.length() == 0) {
                contractAddress = null;
            }
            g0.m(contractAddress, i0.getSymbol());
        }
    }

    public static final void m0(final TransferHistoryFragment transferHistoryFragment, final LoadingState loadingState) {
        fs1.f(transferHistoryFragment, "this$0");
        if (loadingState == null) {
            return;
        }
        new Handler(Looper.getMainLooper()).postDelayed(new Runnable() { // from class: v94
            @Override // java.lang.Runnable
            public final void run() {
                TransferHistoryFragment.n0(TransferHistoryFragment.this, loadingState);
            }
        }, transferHistoryFragment.j0);
    }

    public static final void n0(TransferHistoryFragment transferHistoryFragment, LoadingState loadingState) {
        TextView textView;
        fs1.f(transferHistoryFragment, "this$0");
        fs1.f(loadingState, "$it");
        jb1 jb1Var = transferHistoryFragment.k0;
        if (jb1Var != null && (textView = jb1Var.p) != null) {
            textView.setText(loadingState == LoadingState.Loading ? R.string.loading : R.string.transactions_not_found);
        }
        jb1 jb1Var2 = transferHistoryFragment.k0;
        SwipeRefreshLayout swipeRefreshLayout = jb1Var2 == null ? null : jb1Var2.l;
        if (swipeRefreshLayout != null) {
            swipeRefreshLayout.setRefreshing(false);
        }
        jb1 jb1Var3 = transferHistoryFragment.k0;
        TextView textView2 = jb1Var3 != null ? jb1Var3.p : null;
        if (textView2 == null) {
            return;
        }
        textView2.setVisibility(e30.m0(transferHistoryFragment.f0().getItemCount() == transferHistoryFragment.i0));
    }

    public static final void o0(TransferHistoryFragment transferHistoryFragment, List list) {
        fs1.f(transferHistoryFragment, "this$0");
        if (list == null) {
            return;
        }
        transferHistoryFragment.f0().j(list);
    }

    public static final void p0(TransferHistoryFragment transferHistoryFragment, CurrencyTokenInfo currencyTokenInfo) {
        fs1.f(transferHistoryFragment, "this$0");
        if (currencyTokenInfo == null) {
            return;
        }
        transferHistoryFragment.m0 = currencyTokenInfo;
        jb1 jb1Var = transferHistoryFragment.k0;
        AppCompatImageView appCompatImageView = jb1Var == null ? null : jb1Var.g;
        if (appCompatImageView == null) {
            return;
        }
        appCompatImageView.setVisibility(0);
    }

    public static final void r0(TransferHistoryFragment transferHistoryFragment, View view) {
        fs1.f(transferHistoryFragment, "this$0");
        transferHistoryFragment.f();
    }

    public static final void s0(TransferHistoryFragment transferHistoryFragment) {
        fs1.f(transferHistoryFragment, "this$0");
        transferHistoryFragment.f0().c();
    }

    public static final void t0(TransferHistoryFragment transferHistoryFragment, View view) {
        String cmcId;
        GraphFragment a2;
        fs1.f(transferHistoryFragment, "this$0");
        UserTokenItemDisplayModel i0 = transferHistoryFragment.i0();
        if (dv3.H(i0.getSymbolWithType(), "CUSTOM_", false, 2, null)) {
            Integer l = cv3.l(i0.getIconFile());
            cmcId = String.valueOf(l == null ? transferHistoryFragment.i0 : l.intValue());
        } else {
            cmcId = fs1.b(i0.getSymbol(), "SAFEMOON") ? "8757" : i0.getCmcId();
        }
        String str = cmcId;
        rt a3 = kt.a(i0.getContractAddress(), i0.getSymbol());
        String d = a3 != null ? a3.d() : null;
        if (str != null) {
            if ((str.length() > 0) && transferHistoryFragment.c0().getValue() != null) {
                a2 = GraphFragment.L0.a(i0.getName(), i0.getSymbol(), i0.getNativeBalance(), str, d, (r29 & 32) != 0 ? null : i0.getContractAddress(), (r29 & 64) != 0 ? 0 : 0, (r29 & 128) != 0 ? null : i0.getIconFile(), (r29 & 256) != 0 ? 0 : i0.getIconResId(), (r29 & RecyclerView.a0.FLAG_ADAPTER_POSITION_UNKNOWN) != 0 ? false : false, (r29 & RecyclerView.a0.FLAG_ADAPTER_FULLUPDATE) != 0 ? null : null);
                FragmentManager childFragmentManager = transferHistoryFragment.getChildFragmentManager();
                fs1.e(childFragmentManager, "childFragmentManager");
                a2.C0(childFragmentManager);
                return;
            }
        }
        bh.Y(new WeakReference(transferHistoryFragment.requireActivity()), null, Integer.valueOf((int) R.string.token_not_listed_on_cmc), R.string.action_ok, null, 16, null);
    }

    public static final void u0(TransferHistoryFragment transferHistoryFragment, View view) {
        GraphFragment a2;
        fs1.f(transferHistoryFragment, "this$0");
        UserTokenItemDisplayModel i0 = transferHistoryFragment.i0();
        rt a3 = kt.a(i0.getContractAddress(), i0.getSymbol());
        String d = a3 == null ? null : a3.d();
        GraphFragment.b bVar = GraphFragment.L0;
        String name = i0.getName();
        String symbol = i0.getSymbol();
        double nativeBalance = i0.getNativeBalance();
        String contractAddress = i0.getContractAddress();
        String iconFile = i0.getIconFile();
        int iconResId = i0.getIconResId();
        CurrencyTokenInfo currencyTokenInfo = transferHistoryFragment.m0;
        a2 = bVar.a(name, symbol, nativeBalance, null, d, (r29 & 32) != 0 ? null : contractAddress, (r29 & 64) != 0 ? 0 : 0, (r29 & 128) != 0 ? null : iconFile, (r29 & 256) != 0 ? 0 : iconResId, (r29 & RecyclerView.a0.FLAG_ADAPTER_POSITION_UNKNOWN) != 0 ? false : true, (r29 & RecyclerView.a0.FLAG_ADAPTER_FULLUPDATE) != 0 ? null : currencyTokenInfo != null ? currencyTokenInfo.getUrl() : null);
        FragmentManager childFragmentManager = transferHistoryFragment.getChildFragmentManager();
        fs1.e(childFragmentManager, "childFragmentManager");
        a2.C0(childFragmentManager);
    }

    /* JADX WARN: Removed duplicated region for block: B:17:0x0050  */
    /*
        Code decompiled incorrectly, please refer to instructions dump.
        To view partially-correct code enable 'Show inconsistent code' option in preferences
    */
    public static final void v0(net.safemoon.androidwallet.fragments.TransferHistoryFragment r22, android.view.View r23) {
        /*
            r0 = r22
            java.lang.String r1 = "this$0"
            defpackage.fs1.f(r0, r1)
            net.safemoon.androidwallet.ui.displayModel.UserTokenItemDisplayModel r1 = r22.i0()
            java.lang.String r2 = r1.getSymbolWithType()
            java.lang.String r3 = "CUSTOM_"
            r4 = 0
            r5 = 2
            r6 = 0
            boolean r2 = defpackage.dv3.H(r2, r3, r4, r5, r6)
            if (r2 == 0) goto L30
            java.lang.String r2 = r1.getIconFile()
            java.lang.Integer r2 = defpackage.cv3.l(r2)
            if (r2 != 0) goto L27
            int r2 = r0.i0
            goto L2b
        L27:
            int r2 = r2.intValue()
        L2b:
            java.lang.String r2 = java.lang.String.valueOf(r2)
            goto L3e
        L30:
            java.lang.String r2 = r1.getSymbol()
            java.lang.String r3 = "SAFEMOON"
            boolean r2 = defpackage.fs1.b(r2, r3)
            if (r2 == 0) goto L40
            java.lang.String r2 = "8757"
        L3e:
            r12 = r2
            goto L41
        L40:
            r12 = r6
        L41:
            java.lang.String r2 = r1.getContractAddress()
            java.lang.String r3 = r1.getSymbol()
            rt r2 = defpackage.kt.a(r2, r3)
            if (r2 != 0) goto L50
            goto L54
        L50:
            java.lang.String r6 = r2.d()
        L54:
            r13 = r6
            net.safemoon.androidwallet.dialogs.GraphFragment$b r7 = net.safemoon.androidwallet.dialogs.GraphFragment.L0
            java.lang.String r8 = r1.getName()
            java.lang.String r9 = r1.getSymbol()
            double r10 = r1.getNativeBalance()
            java.lang.String r14 = r1.getContractAddress()
            r15 = 0
            java.lang.String r16 = r1.getIconFile()
            r17 = 0
            r18 = 0
            r19 = 0
            r20 = 1856(0x740, float:2.601E-42)
            r21 = 0
            net.safemoon.androidwallet.dialogs.GraphFragment r1 = net.safemoon.androidwallet.dialogs.GraphFragment.b.d(r7, r8, r9, r10, r12, r13, r14, r15, r16, r17, r18, r19, r20, r21)
            androidx.fragment.app.FragmentManager r0 = r22.getChildFragmentManager()
            java.lang.String r2 = "childFragmentManager"
            defpackage.fs1.e(r0, r2)
            r1.C0(r0)
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: net.safemoon.androidwallet.fragments.TransferHistoryFragment.v0(net.safemoon.androidwallet.fragments.TransferHistoryFragment, android.view.View):void");
    }

    public static final void w0(TransferHistoryFragment transferHistoryFragment, UserTokenItemDisplayModel userTokenItemDisplayModel, View view) {
        fs1.f(transferHistoryFragment, "this$0");
        fs1.f(userTokenItemDisplayModel, "$it");
        rw2.a.d(new WeakReference<>(transferHistoryFragment.requireActivity()), new TransferHistoryFragment$onViewCreated$1$1$2$1(transferHistoryFragment, userTokenItemDisplayModel), TransferHistoryFragment$onViewCreated$1$1$2$2.INSTANCE);
    }

    public static final void x0(TransferHistoryFragment transferHistoryFragment, View view) {
        fs1.f(transferHistoryFragment, "this$0");
        if (transferHistoryFragment.m0 != null) {
            transferHistoryFragment.E0();
            fs1.e(view, "it");
            transferHistoryFragment.G0(view);
            return;
        }
        fs1.e(view, "it");
        transferHistoryFragment.O0(view);
    }

    public static final void y0(TransferHistoryFragment transferHistoryFragment, UserTokenItemDisplayModel userTokenItemDisplayModel, View view) {
        fs1.f(transferHistoryFragment, "this$0");
        fs1.f(userTokenItemDisplayModel, "$it");
        int chainId = userTokenItemDisplayModel.getChainId();
        String name = userTokenItemDisplayModel.getName();
        Context requireContext = transferHistoryFragment.requireContext();
        fs1.e(requireContext, "requireContext()");
        Object w = e30.w(requireContext, userTokenItemDisplayModel.getIconResId(), userTokenItemDisplayModel.getIconFile(), userTokenItemDisplayModel.getSymbol(), userTokenItemDisplayModel.getCmcId());
        String cmcId = userTokenItemDisplayModel.getCmcId();
        ea4.b a2 = ea4.a(new PAToken(chainId, name, userTokenItemDisplayModel.getSymbol(), userTokenItemDisplayModel.getContractAddress(), cmcId == null ? null : cv3.l(cmcId), w, false, 64, null));
        fs1.e(a2, "actionTransferHistoryFra…ss)\n                    )");
        transferHistoryFragment.g(a2);
    }

    /* JADX WARN: Code restructure failed: missing block: B:14:0x002f, code lost:
        if (r5 == true) goto L4;
     */
    /*
        Code decompiled incorrectly, please refer to instructions dump.
        To view partially-correct code enable 'Show inconsistent code' option in preferences
    */
    public static final void z0(defpackage.jb1 r3, net.safemoon.androidwallet.fragments.TransferHistoryFragment r4, java.util.List r5) {
        /*
            java.lang.String r0 = "$this_apply"
            defpackage.fs1.f(r3, r0)
            java.lang.String r0 = "this$0"
            defpackage.fs1.f(r4, r0)
            r0 = 1
            r1 = 0
            if (r5 != 0) goto L10
        Le:
            r0 = r1
            goto L31
        L10:
            boolean r2 = r5.isEmpty()
            if (r2 == 0) goto L18
        L16:
            r5 = r1
            goto L2f
        L18:
            java.util.Iterator r5 = r5.iterator()
        L1c:
            boolean r2 = r5.hasNext()
            if (r2 == 0) goto L16
            java.lang.Object r2 = r5.next()
            net.safemoon.androidwallet.model.priceAlert.PriceAlertToken r2 = (net.safemoon.androidwallet.model.priceAlert.PriceAlertToken) r2
            boolean r2 = r2.isStatus()
            if (r2 == 0) goto L1c
            r5 = r0
        L2f:
            if (r5 != r0) goto Le
        L31:
            if (r0 == 0) goto L48
            androidx.appcompat.widget.AppCompatImageView r3 = r3.h
            android.content.Context r4 = r4.requireContext()
            r5 = 2131099754(0x7f06006a, float:1.781187E38)
            int r4 = defpackage.m70.d(r4, r5)
            android.content.res.ColorStateList r4 = android.content.res.ColorStateList.valueOf(r4)
            r3.setImageTintList(r4)
            goto L5c
        L48:
            androidx.appcompat.widget.AppCompatImageView r3 = r3.h
            android.content.Context r4 = r4.requireContext()
            r5 = 2131099983(0x7f06014f, float:1.7812335E38)
            int r4 = defpackage.m70.d(r4, r5)
            android.content.res.ColorStateList r4 = android.content.res.ColorStateList.valueOf(r4)
            r3.setImageTintList(r4)
        L5c:
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: net.safemoon.androidwallet.fragments.TransferHistoryFragment.z0(jb1, net.safemoon.androidwallet.fragments.TransferHistoryFragment, java.util.List):void");
    }

    public final void E0() {
        UserTokenItemDisplayModel i0 = i0();
        String contractAddress = i0.getContractAddress();
        if (i0.getContractAddress().length() == 0) {
            contractAddress = TokenType.Companion.b(i0.getChainId()).getWrapAddress();
        }
        g0().j(contractAddress);
    }

    public final void F0() {
        jb1 jb1Var = this.k0;
        if (jb1Var == null) {
            return;
        }
        UserTokenItemDisplayModel i0 = i0();
        TextView textView = jb1Var.o;
        fs1.e(textView, "tvFiatBalance");
        e30.N(textView, i0.getBalanceInUSDT(), true);
    }

    public final void G0(final View view) {
        LayoutInflater layoutInflater;
        FragmentActivity activity = getActivity();
        View view2 = null;
        if (activity != null && (layoutInflater = activity.getLayoutInflater()) != null) {
            view2 = layoutInflater.inflate(R.layout.view_choose_stats, (ViewGroup) null);
        }
        final PopupWindow popupWindow = new PopupWindow(view2, (int) (requireView().getWidth() * 0.8d), -2, true);
        popupWindow.showAsDropDown(view);
        fs1.d(view2);
        ci4 a2 = ci4.a(view2);
        a2.a.setOnClickListener(new View.OnClickListener() { // from class: ba4
            @Override // android.view.View.OnClickListener
            public final void onClick(View view3) {
                TransferHistoryFragment.H0(popupWindow, this, view, view3);
            }
        });
        a2.b.setOnClickListener(new View.OnClickListener() { // from class: aa4
            @Override // android.view.View.OnClickListener
            public final void onClick(View view3) {
                TransferHistoryFragment.I0(popupWindow, this, view, view3);
            }
        });
    }

    @SuppressLint({"SetTextI18n"})
    public final void J0(View view) {
        LayoutInflater layoutInflater;
        Resources resources;
        Integer valueOf;
        Resources resources2;
        Double h24;
        Double h6;
        Double h1;
        Double m5;
        Double usd;
        Resources resources3;
        FragmentActivity activity = getActivity();
        View inflate = (activity == null || (layoutInflater = activity.getLayoutInflater()) == null) ? null : layoutInflater.inflate(R.layout.view_swap_stats_info, (ViewGroup) null);
        new PopupWindow(inflate, (int) (requireView().getWidth() * 0.9d), -2, true).showAsDropDown(view);
        fs1.d(inflate);
        dk4 a2 = dk4.a(inflate);
        CurrencyTokenInfo currencyTokenInfo = this.m0;
        if (currencyTokenInfo == null) {
            return;
        }
        Integer version = currencyTokenInfo.getVersion();
        if (version != null) {
            a2.i.setText(fs1.l("V", Integer.valueOf(version.intValue())));
            te4 te4Var = te4.a;
        }
        final Ref$IntRef ref$IntRef = new Ref$IntRef();
        TokenType.a aVar = TokenType.Companion;
        FragmentActivity requireActivity = requireActivity();
        TokenType tokenType = TokenType.BEP_20;
        String j = bo3.j(requireActivity, "DEFAULT_GATEWAY", tokenType.getName());
        fs1.e(j, "getString(requireActivit…kenType.BEP_20.getName())");
        TokenType c = aVar.c(j);
        if (c == tokenType) {
            Context context = getContext();
            if (context != null && (resources3 = context.getResources()) != null) {
                valueOf = Integer.valueOf(resources3.getIdentifier("wrapped_bnb_2", "drawable", requireContext().getPackageName()));
            }
            valueOf = null;
        } else {
            Context context2 = getContext();
            if (context2 != null && (resources = context2.getResources()) != null) {
                valueOf = Integer.valueOf(resources.getIdentifier("ethereum", "drawable", requireContext().getPackageName()));
            }
            valueOf = null;
        }
        if (valueOf == null) {
            valueOf = 0;
        }
        Context context3 = getContext();
        Integer valueOf2 = (context3 == null || (resources2 = context3.getResources()) == null) ? null : Integer.valueOf(resources2.getIdentifier("safemoon", "drawable", requireContext().getPackageName()));
        if (valueOf2 == null) {
            valueOf2 = 0;
        }
        if (c == tokenType) {
            AppCompatImageView appCompatImageView = a2.a;
            fs1.e(appCompatImageView, "fromPairTokenIcon");
            e30.Q(appCompatImageView, valueOf.intValue(), "wrapped_bnb_2", null);
            a2.n.setText(getString(R.string.swap_stats_dialog_bsc_txt));
        } else {
            AppCompatImageView appCompatImageView2 = a2.a;
            fs1.e(appCompatImageView2, "fromPairTokenIcon");
            e30.Q(appCompatImageView2, valueOf.intValue(), "ethereum", null);
            a2.n.setText(getString(R.string.swap_stats_dialog_ethereum_txt));
        }
        AppCompatImageView appCompatImageView3 = a2.b;
        fs1.e(appCompatImageView3, "toPairTokenIcon");
        e30.Q(appCompatImageView3, valueOf2.intValue(), "safemoon", null);
        a2.v.setText(getString(R.string.swap_stats_dialog_sfm_swap_txt));
        a2.j.setText(currencyTokenInfo.getBaseToken().getSymbol() + '/' + currencyTokenInfo.getQuoteToken().getSymbol());
        a2.k.setText(currencyTokenInfo.getBaseToken().getName());
        a2.r.setText(currencyTokenInfo.getPriceUsd());
        a2.s.setText(((Object) currencyTokenInfo.getPriceNative()) + ' ' + currencyTokenInfo.getQuoteToken().getSymbol());
        a2.o.setText("$0");
        CurrencyTokenInfo.Liquidity liquidity = currencyTokenInfo.getLiquidity();
        if (liquidity != null && (usd = liquidity.getUsd()) != null) {
            double doubleValue = usd.doubleValue();
            a2.o.setText(fs1.l("$", e30.M(doubleValue)));
            a2.p.setText(fs1.l("$", e30.M(doubleValue)));
            te4 te4Var2 = te4.a;
        }
        a2.l.setText("$0");
        Double fdv = currencyTokenInfo.getFdv();
        if (fdv != null) {
            double doubleValue2 = fdv.doubleValue();
            a2.l.setText(fs1.l("$", e30.M(doubleValue2)));
            a2.p.setText(fs1.l("$", e30.M(doubleValue2)));
            te4 te4Var3 = te4.a;
        }
        CurrencyTokenInfo.PriceChange priceChange = currencyTokenInfo.getPriceChange();
        if (priceChange != null && (m5 = priceChange.getM5()) != null) {
            double doubleValue3 = m5.doubleValue();
            AppCompatTextView appCompatTextView = a2.m;
            StringBuilder sb = new StringBuilder();
            sb.append(doubleValue3);
            sb.append('%');
            appCompatTextView.setText(sb.toString());
            a2.m.setTextColor(m70.d(requireContext(), R.color.t1));
            if (doubleValue3 > Utils.DOUBLE_EPSILON) {
                a2.m.setTextColor(m70.d(requireContext(), R.color.curve_green));
            } else if (doubleValue3 < Utils.DOUBLE_EPSILON) {
                a2.m.setTextColor(m70.d(requireContext(), R.color.candle_red));
            }
            te4 te4Var4 = te4.a;
        }
        CurrencyTokenInfo.PriceChange priceChange2 = currencyTokenInfo.getPriceChange();
        if (priceChange2 != null && (h1 = priceChange2.getH1()) != null) {
            double doubleValue4 = h1.doubleValue();
            AppCompatTextView appCompatTextView2 = a2.q;
            StringBuilder sb2 = new StringBuilder();
            sb2.append(doubleValue4);
            sb2.append('%');
            appCompatTextView2.setText(sb2.toString());
            a2.q.setTextColor(m70.d(requireContext(), R.color.t1));
            if (doubleValue4 > Utils.DOUBLE_EPSILON) {
                a2.q.setTextColor(m70.d(requireContext(), R.color.curve_green));
            } else if (doubleValue4 < Utils.DOUBLE_EPSILON) {
                a2.q.setTextColor(m70.d(requireContext(), R.color.candle_red));
            }
            te4 te4Var5 = te4.a;
        }
        CurrencyTokenInfo.PriceChange priceChange3 = currencyTokenInfo.getPriceChange();
        if (priceChange3 != null && (h6 = priceChange3.getH6()) != null) {
            double doubleValue5 = h6.doubleValue();
            AppCompatTextView appCompatTextView3 = a2.u;
            StringBuilder sb3 = new StringBuilder();
            sb3.append(doubleValue5);
            sb3.append('%');
            appCompatTextView3.setText(sb3.toString());
            a2.u.setTextColor(m70.d(requireContext(), R.color.t1));
            if (doubleValue5 > Utils.DOUBLE_EPSILON) {
                a2.u.setTextColor(m70.d(requireContext(), R.color.curve_green));
            } else if (doubleValue5 < Utils.DOUBLE_EPSILON) {
                a2.u.setTextColor(m70.d(requireContext(), R.color.candle_red));
            }
            te4 te4Var6 = te4.a;
        }
        CurrencyTokenInfo.PriceChange priceChange4 = currencyTokenInfo.getPriceChange();
        if (priceChange4 != null && (h24 = priceChange4.getH24()) != null) {
            double doubleValue6 = h24.doubleValue();
            AppCompatTextView appCompatTextView4 = a2.d;
            StringBuilder sb4 = new StringBuilder();
            sb4.append(doubleValue6);
            sb4.append('%');
            appCompatTextView4.setText(sb4.toString());
            a2.d.setTextColor(m70.d(requireContext(), R.color.t1));
            if (doubleValue6 > Utils.DOUBLE_EPSILON) {
                a2.d.setTextColor(m70.d(requireContext(), R.color.curve_green));
            } else if (doubleValue6 < Utils.DOUBLE_EPSILON) {
                a2.d.setTextColor(m70.d(requireContext(), R.color.candle_red));
            }
            te4 te4Var7 = te4.a;
        }
        final List<AppCompatTextView> l = b20.l(a2.f, a2.c, a2.g, a2.e);
        final List<AppCompatTextView> l2 = b20.l(a2.w, a2.h, a2.t, a2.x);
        S0(ref$IntRef.element, l);
        R0(0, l2);
        a2.f.setOnClickListener(new View.OnClickListener() { // from class: ca4
            @Override // android.view.View.OnClickListener
            public final void onClick(View view2) {
                TransferHistoryFragment.K0(Ref$IntRef.this, this, l, l2, view2);
            }
        });
        a2.c.setOnClickListener(new View.OnClickListener() { // from class: da4
            @Override // android.view.View.OnClickListener
            public final void onClick(View view2) {
                TransferHistoryFragment.L0(Ref$IntRef.this, this, l, l2, view2);
            }
        });
        a2.g.setOnClickListener(new View.OnClickListener() { // from class: f94
            @Override // android.view.View.OnClickListener
            public final void onClick(View view2) {
                TransferHistoryFragment.M0(Ref$IntRef.this, this, l, l2, view2);
            }
        });
        a2.e.setOnClickListener(new View.OnClickListener() { // from class: e94
            @Override // android.view.View.OnClickListener
            public final void onClick(View view2) {
                TransferHistoryFragment.N0(Ref$IntRef.this, this, l, l2, view2);
            }
        });
        te4 te4Var8 = te4.a;
    }

    public final void O0(View view) {
        LayoutInflater layoutInflater;
        int i;
        Double selfReportedMarketCap;
        Double selfReportedCirculatingSupply;
        String p;
        char c;
        Coin value = this.u0.getValue();
        FragmentActivity activity = getActivity();
        View inflate = (activity == null || (layoutInflater = activity.getLayoutInflater()) == null) ? null : layoutInflater.inflate(R.layout.view_token_details, (ViewGroup) null);
        int width = (int) (requireView().getWidth() * 0.8d);
        fs1.d(inflate);
        hk4 a2 = hk4.a(inflate);
        u21.a aVar = u21.a;
        String t = e30.t(aVar.b());
        if (value == null) {
            AppCompatTextView appCompatTextView = a2.c;
            lu3 lu3Var = lu3.a;
            String format = String.format(Locale.getDefault(), t, Arrays.copyOf(new Object[]{aVar.b(), "0.00"}, 2));
            fs1.e(format, "java.lang.String.format(locale, format, *args)");
            appCompatTextView.setText(format);
            AppCompatTextView appCompatTextView2 = a2.e;
            String format2 = String.format(Locale.getDefault(), t, Arrays.copyOf(new Object[]{aVar.b(), "0.00"}, 2));
            fs1.e(format2, "java.lang.String.format(locale, format, *args)");
            appCompatTextView2.setText(format2);
            a2.a.setText("0");
            a2.d.setText("0");
            MaterialTextView materialTextView = a2.b;
            fs1.e(materialTextView, "txtContractAddress");
            materialTextView.setVisibility(8);
            i = width;
        } else {
            i = width;
            if (value.getQuote().getUSD().getMarketCap() != null && !fs1.a(value.getQuote().getUSD().getMarketCap(), Utils.DOUBLE_EPSILON)) {
                selfReportedMarketCap = value.getQuote().getUSD().getMarketCap();
            } else {
                selfReportedMarketCap = value.getSelfReportedMarketCap();
            }
            if (value.getCirculatingSupply() != null && !fs1.a(value.getCirculatingSupply(), Utils.DOUBLE_EPSILON)) {
                selfReportedCirculatingSupply = value.getCirculatingSupply();
            } else {
                selfReportedCirculatingSupply = value.getSelfReportedCirculatingSupply();
            }
            AppCompatTextView appCompatTextView3 = a2.c;
            lu3 lu3Var2 = lu3.a;
            Locale locale = Locale.getDefault();
            Object[] objArr = new Object[2];
            objArr[0] = aVar.b();
            objArr[1] = e30.p(selfReportedMarketCap == null ? Utils.DOUBLE_EPSILON : selfReportedMarketCap.doubleValue(), 0, null, false, 7, null);
            String format3 = String.format(locale, t, Arrays.copyOf(objArr, 2));
            fs1.e(format3, "java.lang.String.format(locale, format, *args)");
            appCompatTextView3.setText(format3);
            AppCompatTextView appCompatTextView4 = a2.e;
            Locale locale2 = Locale.getDefault();
            Object[] objArr2 = new Object[2];
            objArr2[0] = aVar.b();
            Double volume24h = value.getQuote().getUSD().getVolume24h();
            if (volume24h == null) {
                c = 1;
                p = null;
            } else {
                p = e30.p(volume24h.doubleValue(), 0, null, false, 7, null);
                c = 1;
            }
            objArr2[c] = p;
            String format4 = String.format(locale2, t, Arrays.copyOf(objArr2, 2));
            fs1.e(format4, "java.lang.String.format(locale, format, *args)");
            appCompatTextView4.setText(format4);
            AppCompatTextView appCompatTextView5 = a2.a;
            StringBuilder sb = new StringBuilder();
            sb.append(e30.p(selfReportedCirculatingSupply == null ? Utils.DOUBLE_EPSILON : selfReportedCirculatingSupply.doubleValue(), 0, null, false, 7, null));
            sb.append(' ');
            sb.append((Object) value.getSymbol());
            appCompatTextView5.setText(sb.toString());
            AppCompatTextView appCompatTextView6 = a2.d;
            StringBuilder sb2 = new StringBuilder();
            Double totalSupply = value.getTotalSupply();
            sb2.append(e30.p(totalSupply == null ? Utils.DOUBLE_EPSILON : totalSupply.doubleValue(), 0, null, false, 7, null));
            sb2.append(' ');
            sb2.append((Object) value.getSymbol());
            appCompatTextView6.setText(sb2.toString());
            final String contractAddress = i0().getContractAddress();
            a2.b.setText(e30.d(contractAddress));
            MaterialTextView materialTextView2 = a2.b;
            fs1.e(materialTextView2, "txtContractAddress");
            materialTextView2.setVisibility(0);
            a2.b.setOnClickListener(new View.OnClickListener() { // from class: p94
                @Override // android.view.View.OnClickListener
                public final void onClick(View view2) {
                    TransferHistoryFragment.P0(TransferHistoryFragment.this, contractAddress, view2);
                }
            });
        }
        final PopupWindow popupWindow = new PopupWindow(inflate, i, -2, true);
        popupWindow.showAsDropDown(view);
        inflate.setOnTouchListener(new View.OnTouchListener() { // from class: s94
            @Override // android.view.View.OnTouchListener
            public final boolean onTouch(View view2, MotionEvent motionEvent) {
                boolean Q0;
                Q0 = TransferHistoryFragment.Q0(popupWindow, view2, motionEvent);
                return Q0;
            }
        });
    }

    /* JADX WARN: Multi-variable type inference failed */
    /* JADX WARN: Type inference failed for: r9v23 */
    /* JADX WARN: Type inference failed for: r9v6 */
    /* JADX WARN: Type inference failed for: r9v8, types: [java.lang.Number] */
    @SuppressLint({"SetTextI18n"})
    public final void R0(int i, List<AppCompatTextView> list) {
        CurrencyTokenInfo.TxnsUnit m5;
        Double m52;
        ?? r9;
        CurrencyTokenInfo currencyTokenInfo = this.m0;
        if (currencyTokenInfo == null) {
            return;
        }
        CurrencyTokenInfo.TxnsUnit txnsUnit = null;
        if (i == 0) {
            CurrencyTokenInfo.Txns txns = currencyTokenInfo.getTxns();
            m5 = txns == null ? null : txns.getM5();
            CurrencyTokenInfo.Volume volume = currencyTokenInfo.getVolume();
            if (volume != null) {
                m52 = volume.getM5();
                txnsUnit = m52;
            }
            CurrencyTokenInfo.TxnsUnit txnsUnit2 = txnsUnit;
            txnsUnit = m5;
            r9 = txnsUnit2;
        } else if (i == 1) {
            CurrencyTokenInfo.Txns txns2 = currencyTokenInfo.getTxns();
            m5 = txns2 == null ? null : txns2.getH1();
            CurrencyTokenInfo.Volume volume2 = currencyTokenInfo.getVolume();
            if (volume2 != null) {
                m52 = volume2.getH1();
                txnsUnit = m52;
            }
            CurrencyTokenInfo.TxnsUnit txnsUnit22 = txnsUnit;
            txnsUnit = m5;
            r9 = txnsUnit22;
        } else if (i == 2) {
            CurrencyTokenInfo.Txns txns3 = currencyTokenInfo.getTxns();
            m5 = txns3 == null ? null : txns3.getH6();
            CurrencyTokenInfo.Volume volume3 = currencyTokenInfo.getVolume();
            if (volume3 != null) {
                m52 = volume3.getH6();
                txnsUnit = m52;
            }
            CurrencyTokenInfo.TxnsUnit txnsUnit222 = txnsUnit;
            txnsUnit = m5;
            r9 = txnsUnit222;
        } else if (i != 3) {
            r9 = 0;
        } else {
            CurrencyTokenInfo.Txns txns4 = currencyTokenInfo.getTxns();
            m5 = txns4 == null ? null : txns4.getH24();
            CurrencyTokenInfo.Volume volume4 = currencyTokenInfo.getVolume();
            if (volume4 != null) {
                m52 = volume4.getH24();
                txnsUnit = m52;
            }
            CurrencyTokenInfo.TxnsUnit txnsUnit2222 = txnsUnit;
            txnsUnit = m5;
            r9 = txnsUnit2222;
        }
        for (AppCompatTextView appCompatTextView : list) {
            appCompatTextView.setText(org.web3j.utils.b.MISSING_REASON);
        }
        if (txnsUnit != null) {
            Integer buys = txnsUnit.getBuys();
            if (buys != null) {
                list.get(1).setText(String.valueOf(buys.intValue()));
            }
            Integer sells = txnsUnit.getSells();
            if (sells != null) {
                list.get(2).setText(String.valueOf(sells.intValue()));
            }
            if (txnsUnit.getSells() != null && txnsUnit.getBuys() != null) {
                list.get(0).setText(String.valueOf(txnsUnit.getSells().intValue() + txnsUnit.getBuys().intValue()));
            }
        }
        if (r9 == 0) {
            return;
        }
        list.get(3).setText(fs1.l("$", e30.M(r9.doubleValue())));
    }

    public final void S0(final int i, final List<AppCompatTextView> list) {
        requireActivity().runOnUiThread(new Runnable() { // from class: u94
            @Override // java.lang.Runnable
            public final void run() {
                TransferHistoryFragment.T0(list, i, this);
            }
        });
    }

    public final void Z(UserTokenItemDisplayModel userTokenItemDisplayModel) {
        rz1 viewLifecycleOwner = getViewLifecycleOwner();
        fs1.e(viewLifecycleOwner, "viewLifecycleOwner");
        as.b(sz1.a(viewLifecycleOwner), null, null, new TransferHistoryFragment$fetchTokenInfo$1(userTokenItemDisplayModel, this, null), 3, null);
    }

    public final String a0() {
        return (String) this.p0.getValue();
    }

    public final void b0(PaymentMethod paymentMethod, String str) {
        if (this.t0) {
            return;
        }
        this.t0 = true;
        d0().h(paymentMethod, str, new TransferHistoryFragment$getCheckoutUrl$1(this), new TransferHistoryFragment$getCheckoutUrl$2(this));
    }

    public final gb2<Coin> c0() {
        return this.u0;
    }

    public final HomeViewModel d0() {
        return (HomeViewModel) this.r0.getValue();
    }

    public final MyTokensListViewModel e0() {
        return (MyTokensListViewModel) this.q0.getValue();
    }

    public final TransferHistoryFragment$transactionAdapter$2.a f0() {
        return (TransferHistoryFragment$transactionAdapter$2.a) this.v0.getValue();
    }

    public final TransactionHistoryViewModel g0() {
        return (TransactionHistoryViewModel) this.l0.getValue();
    }

    public final j71<fp2<Result>> h0() {
        return (j71) this.o0.getValue();
    }

    public final UserTokenItemDisplayModel i0() {
        return (UserTokenItemDisplayModel) this.n0.getValue();
    }

    public final void j0() {
        gb2 b;
        xd2 h = ka1.a(this).h();
        ec3 d = h == null ? null : h.d();
        if (d == null || (b = d.b("RESULT_FROM_CRYPTO_PRICE_ALERT")) == null) {
            return;
        }
        b.observe(h, new tl2() { // from class: o94
            @Override // defpackage.tl2
            public final void onChanged(Object obj) {
                TransferHistoryFragment.k0(TransferHistoryFragment.this, (Boolean) obj);
            }
        });
    }

    public final void l0() {
        jb1 jb1Var = this.k0;
        if (jb1Var != null) {
            jb1Var.k.setLayoutManager(new LinearLayoutManager(getActivity(), 1, false));
            jb1Var.k.setAdapter(f0());
            jb1Var.k.setItemAnimator(null);
        }
        sz1.a(this).b(new TransferHistoryFragment$initRecyclerView$2(this, null));
        g0().o().observe(getViewLifecycleOwner(), new tl2() { // from class: y94
            @Override // defpackage.tl2
            public final void onChanged(Object obj) {
                TransferHistoryFragment.m0(TransferHistoryFragment.this, (LoadingState) obj);
            }
        });
        g0().h().observe(getViewLifecycleOwner(), new tl2() { // from class: x94
            @Override // defpackage.tl2
            public final void onChanged(Object obj) {
                TransferHistoryFragment.o0(TransferHistoryFragment.this, (List) obj);
            }
        });
        g0().i().observe(getViewLifecycleOwner(), new tl2() { // from class: z94
            @Override // defpackage.tl2
            public final void onChanged(Object obj) {
                TransferHistoryFragment.p0(TransferHistoryFragment.this, (CurrencyTokenInfo) obj);
            }
        });
        E0();
    }

    @Override // androidx.fragment.app.Fragment
    public View onCreateView(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle) {
        fs1.f(layoutInflater, "inflater");
        this.k0 = jb1.a(layoutInflater.inflate(R.layout.fragment_transfer_history, viewGroup, false));
        q0();
        l0();
        jb1 jb1Var = this.k0;
        fs1.d(jb1Var);
        ConstraintLayout b = jb1Var.b();
        fs1.e(b, "binding!!.root");
        return b;
    }

    @Override // androidx.fragment.app.Fragment
    public void onResume() {
        super.onResume();
        if (this.s0) {
            this.s0 = false;
            pg4.b(requireActivity(), Boolean.FALSE);
        }
    }

    @Override // net.safemoon.androidwallet.fragments.common.BaseMainFragment, defpackage.qn, androidx.fragment.app.Fragment
    public void onViewCreated(View view, Bundle bundle) {
        AppCompatTextView appCompatTextView;
        fs1.f(view, "view");
        super.onViewCreated(view, bundle);
        final jb1 jb1Var = this.k0;
        if (jb1Var != null) {
            final UserTokenItemDisplayModel i0 = i0();
            TransactionHistoryViewModel g0 = g0();
            String contractAddress = i0.getContractAddress();
            if (contractAddress.length() == 0) {
                contractAddress = null;
            }
            g0.m(contractAddress, i0.getSymbol());
            jb1Var.n.setText(TokenType.Companion.b(i0.getChainId()).getDisplayName());
            AppCompatImageView appCompatImageView = jb1Var.i;
            fs1.e(appCompatImageView, "imgSymbol");
            e30.Q(appCompatImageView, i0.getIconResId(), i0.getIconFile(), i0.getSymbol());
            if (e30.H(i0.getSymbol())) {
                AppCompatImageView appCompatImageView2 = jb1Var.i;
                fs1.e(appCompatImageView2, "imgSymbol");
                e30.P(appCompatImageView2, i0.getCmcId(), i0.getSymbol());
            }
            F0();
            jb1Var.e.setVisibility(e30.m0(i0.getAllowSwap()));
            jb1Var.b.setVisibility(e30.m0(um1.a(i0.getSymbolWithType()).d()));
            jb1Var.b.setOnClickListener(new View.OnClickListener() { // from class: r94
                @Override // android.view.View.OnClickListener
                public final void onClick(View view2) {
                    TransferHistoryFragment.w0(TransferHistoryFragment.this, i0, view2);
                }
            });
            jb1Var.d.setVisibility(e30.m0(um1.a(i0.getSymbolWithType()).b()));
            jb1Var.c.setVisibility(e30.m0(um1.a(i0.getSymbolWithType()).a()));
            jb1Var.j.setOnClickListener(new View.OnClickListener() { // from class: i94
                @Override // android.view.View.OnClickListener
                public final void onClick(View view2) {
                    TransferHistoryFragment.x0(TransferHistoryFragment.this, view2);
                }
            });
            jb1Var.h.setOnClickListener(new View.OnClickListener() { // from class: q94
                @Override // android.view.View.OnClickListener
                public final void onClick(View view2) {
                    TransferHistoryFragment.y0(TransferHistoryFragment.this, i0, view2);
                }
            });
            g0().n().observe(getViewLifecycleOwner(), new tl2() { // from class: d94
                @Override // defpackage.tl2
                public final void onChanged(Object obj) {
                    TransferHistoryFragment.z0(jb1.this, this, (List) obj);
                }
            });
            Z(i0);
            jb1Var.d.setOnClickListener(new View.OnClickListener() { // from class: g94
                @Override // android.view.View.OnClickListener
                public final void onClick(View view2) {
                    TransferHistoryFragment.A0(TransferHistoryFragment.this, view2);
                }
            });
            jb1Var.c.setOnClickListener(new View.OnClickListener() { // from class: k94
                @Override // android.view.View.OnClickListener
                public final void onClick(View view2) {
                    TransferHistoryFragment.B0(TransferHistoryFragment.this, view2);
                }
            });
            jb1Var.e.setOnClickListener(new View.OnClickListener() { // from class: n94
                @Override // android.view.View.OnClickListener
                public final void onClick(View view2) {
                    TransferHistoryFragment.C0(TransferHistoryFragment.this, view2);
                }
            });
        }
        jb1 jb1Var2 = this.k0;
        if (jb1Var2 != null && (appCompatTextView = jb1Var2.q) != null) {
            e30.X(appCompatTextView, new TransferHistoryFragment$onViewCreated$2(this));
        }
        e0().B().observe(getViewLifecycleOwner(), new tl2() { // from class: w94
            @Override // defpackage.tl2
            public final void onChanged(Object obj) {
                TransferHistoryFragment.D0(TransferHistoryFragment.this, (Double) obj);
            }
        });
        j0();
    }

    public final void q0() {
        jb1 jb1Var = this.k0;
        if (jb1Var == null) {
            return;
        }
        jb1Var.m.c.setOnClickListener(new View.OnClickListener() { // from class: m94
            @Override // android.view.View.OnClickListener
            public final void onClick(View view) {
                TransferHistoryFragment.r0(TransferHistoryFragment.this, view);
            }
        });
        jb1Var.m.e.setText(getText(R.string.transaction_history));
        jb1Var.m.b.setVisibility(4);
        jb1Var.l.setOnRefreshListener(new SwipeRefreshLayout.j() { // from class: t94
            @Override // androidx.swiperefreshlayout.widget.SwipeRefreshLayout.j
            public final void onRefresh() {
                TransferHistoryFragment.s0(TransferHistoryFragment.this);
            }
        });
        jb1Var.f.setOnClickListener(new View.OnClickListener() { // from class: h94
            @Override // android.view.View.OnClickListener
            public final void onClick(View view) {
                TransferHistoryFragment.t0(TransferHistoryFragment.this, view);
            }
        });
        jb1Var.g.setOnClickListener(new View.OnClickListener() { // from class: j94
            @Override // android.view.View.OnClickListener
            public final void onClick(View view) {
                TransferHistoryFragment.u0(TransferHistoryFragment.this, view);
            }
        });
        jb1Var.g.setVisibility(8);
        jb1Var.m.b.setImageResource(R.drawable.ic_top_nav_chart);
        jb1Var.m.b.setOnClickListener(new View.OnClickListener() { // from class: l94
            @Override // android.view.View.OnClickListener
            public final void onClick(View view) {
                TransferHistoryFragment.v0(TransferHistoryFragment.this, view);
            }
        });
    }
}
