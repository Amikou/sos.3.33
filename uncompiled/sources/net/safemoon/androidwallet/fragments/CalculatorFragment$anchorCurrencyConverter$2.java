package net.safemoon.androidwallet.fragments;

import android.content.Context;
import kotlin.jvm.internal.Lambda;
import net.safemoon.androidwallet.dialogs.AnchorCurrencyConverter;
import net.safemoon.androidwallet.viewmodels.SelectFiatViewModel;

/* compiled from: CalculatorFragment.kt */
/* loaded from: classes2.dex */
public final class CalculatorFragment$anchorCurrencyConverter$2 extends Lambda implements rc1<AnchorCurrencyConverter> {
    public final /* synthetic */ CalculatorFragment this$0;

    /* compiled from: CalculatorFragment.kt */
    /* renamed from: net.safemoon.androidwallet.fragments.CalculatorFragment$anchorCurrencyConverter$2$1  reason: invalid class name */
    /* loaded from: classes2.dex */
    public static final class AnonymousClass1 extends Lambda implements rc1<te4> {
        public static final AnonymousClass1 INSTANCE = new AnonymousClass1();

        public AnonymousClass1() {
            super(0);
        }

        @Override // defpackage.rc1
        public /* bridge */ /* synthetic */ te4 invoke() {
            invoke2();
            return te4.a;
        }

        @Override // defpackage.rc1
        /* renamed from: invoke  reason: avoid collision after fix types in other method */
        public final void invoke2() {
        }
    }

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public CalculatorFragment$anchorCurrencyConverter$2(CalculatorFragment calculatorFragment) {
        super(0);
        this.this$0 = calculatorFragment;
    }

    @Override // defpackage.rc1
    public final AnchorCurrencyConverter invoke() {
        SelectFiatViewModel d0;
        r91 r91Var;
        Context requireContext = this.this$0.requireContext();
        fs1.e(requireContext, "requireContext()");
        d0 = this.this$0.d0();
        r91Var = this.this$0.m0;
        return new AnchorCurrencyConverter(requireContext, d0, r91Var == null ? null : r91Var.b(), AnonymousClass1.INSTANCE);
    }
}
