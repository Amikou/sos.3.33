package net.safemoon.androidwallet.fragments;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import androidx.fragment.app.FragmentViewModelLazyKt;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;
import java.util.Objects;
import net.safemoon.androidwallet.R;
import net.safemoon.androidwallet.common.TokenType;
import net.safemoon.androidwallet.fragments.TransferDetailsFragmentStatus;
import net.safemoon.androidwallet.fragments.common.BaseMainFragment;
import net.safemoon.androidwallet.model.ReceiptStatus;
import net.safemoon.androidwallet.model.token.abstraction.IToken;
import net.safemoon.androidwallet.model.transaction.history.Result;
import net.safemoon.androidwallet.viewmodels.TransferViewModel;

/* compiled from: TransferDetailsFragmentStatus.kt */
/* loaded from: classes2.dex */
public final class TransferDetailsFragmentStatus extends BaseMainFragment {
    public ib1 k0;
    public final int i0 = 18;
    public final long j0 = 1000;
    public final sy1 l0 = FragmentViewModelLazyKt.a(this, d53.b(TransferViewModel.class), new TransferDetailsFragmentStatus$special$$inlined$activityViewModels$default$1(this), new TransferDetailsFragmentStatus$special$$inlined$activityViewModels$default$2(this));
    public final sy1 m0 = zy1.a(new TransferDetailsFragmentStatus$requestTransaction$2(this));
    public final sy1 n0 = zy1.a(new TransferDetailsFragmentStatus$chainType$2(this));
    public final sy1 o0 = zy1.a(new TransferDetailsFragmentStatus$address$2(this));
    public final sy1 p0 = zy1.a(new TransferDetailsFragmentStatus$newTransaction$2(this));

    /* compiled from: TransferDetailsFragmentStatus.kt */
    /* loaded from: classes2.dex */
    public static final class a {
        public a() {
        }

        public /* synthetic */ a(qi0 qi0Var) {
            this();
        }
    }

    static {
        new a(null);
    }

    public static final void D(TransferDetailsFragmentStatus transferDetailsFragmentStatus, String str, Result result, View view) {
        fs1.f(transferDetailsFragmentStatus, "this$0");
        fs1.f(str, "$link");
        fs1.f(result, "$details");
        transferDetailsFragmentStatus.B(false);
        transferDetailsFragmentStatus.startActivity(new Intent("android.intent.action.VIEW", Uri.parse(fs1.l(str, result.hash))));
    }

    public static final void E(TransferDetailsFragmentStatus transferDetailsFragmentStatus, View view) {
        fs1.f(transferDetailsFragmentStatus, "this$0");
        transferDetailsFragmentStatus.f();
    }

    public static final void y(TransferDetailsFragmentStatus transferDetailsFragmentStatus, List list) {
        fs1.f(transferDetailsFragmentStatus, "this$0");
        if (list != null && (!list.isEmpty())) {
            transferDetailsFragmentStatus.C(transferDetailsFragmentStatus.v());
            TransferViewModel x = transferDetailsFragmentStatus.x();
            String str = transferDetailsFragmentStatus.v().hash;
            fs1.e(str, "requestTransaction.hash");
            x.i(str, transferDetailsFragmentStatus.t());
        }
    }

    public static final void z(TransferDetailsFragmentStatus transferDetailsFragmentStatus, ReceiptStatus receiptStatus) {
        fs1.f(transferDetailsFragmentStatus, "this$0");
        if (transferDetailsFragmentStatus.u()) {
            String str = receiptStatus == null ? "" : receiptStatus.result.status;
            fs1.e(str, "if(it == null) \"\" else it.result.status");
            transferDetailsFragmentStatus.A(str);
        }
    }

    public final void A(String str) {
        ib1 ib1Var = this.k0;
        if (ib1Var == null) {
            return;
        }
        if (fs1.b(str, "1")) {
            ib1Var.d.setColorFilter(m70.d(requireContext(), R.color.green));
            ib1Var.m.setText(R.string.transfer_confirmed);
        } else if (fs1.b(str, "0")) {
            ib1Var.d.setColorFilter(m70.d(requireContext(), R.color.red));
            ib1Var.m.setText(R.string.transfer_failed);
        } else {
            ib1Var.d.setColorFilter(m70.d(requireContext(), R.color.yellow));
            ib1Var.m.setText(R.string.transfer_pending);
        }
    }

    public final void B(boolean z) {
        ib1 ib1Var = this.k0;
        if (ib1Var != null) {
            fs1.d(ib1Var);
            ib1Var.g.setClickable(z);
        }
    }

    /* JADX WARN: Code restructure failed: missing block: B:24:0x0179, code lost:
        if (r7 != null) goto L28;
     */
    /* JADX WARN: Removed duplicated region for block: B:62:0x0321  */
    /* JADX WARN: Removed duplicated region for block: B:64:0x0333  */
    /*
        Code decompiled incorrectly, please refer to instructions dump.
        To view partially-correct code enable 'Show inconsistent code' option in preferences
    */
    public final void C(final net.safemoon.androidwallet.model.transaction.history.Result r18) {
        /*
            Method dump skipped, instructions count: 1170
            To view this dump change 'Code comments level' option to 'DEBUG'
        */
        throw new UnsupportedOperationException("Method not decompiled: net.safemoon.androidwallet.fragments.TransferDetailsFragmentStatus.C(net.safemoon.androidwallet.model.transaction.history.Result):void");
    }

    @Override // androidx.fragment.app.Fragment
    public View onCreateView(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle) {
        fs1.f(layoutInflater, "inflater");
        ib1 a2 = ib1.a(layoutInflater.inflate(R.layout.fragment_transaction_notification_details, viewGroup, false));
        this.k0 = a2;
        fs1.d(a2);
        a2.f.c.setText(getText(R.string.Transfer_detail));
        ib1 ib1Var = this.k0;
        fs1.d(ib1Var);
        return ib1Var.b();
    }

    @Override // androidx.fragment.app.Fragment
    public void onResume() {
        super.onResume();
        B(true);
    }

    @Override // net.safemoon.androidwallet.fragments.common.BaseMainFragment, defpackage.qn, androidx.fragment.app.Fragment
    public void onViewCreated(View view, Bundle bundle) {
        fs1.f(view, "view");
        super.onViewCreated(view, bundle);
        x().g().postValue(null);
        x().c(t());
        x().d().observe(getViewLifecycleOwner(), new tl2() { // from class: z84
            @Override // defpackage.tl2
            public final void onChanged(Object obj) {
                TransferDetailsFragmentStatus.y(TransferDetailsFragmentStatus.this, (List) obj);
            }
        });
        x().g().observe(getViewLifecycleOwner(), new tl2() { // from class: a94
            @Override // defpackage.tl2
            public final void onChanged(Object obj) {
                TransferDetailsFragmentStatus.z(TransferDetailsFragmentStatus.this, (ReceiptStatus) obj);
            }
        });
    }

    public final String s() {
        return (String) this.o0.getValue();
    }

    public final TokenType t() {
        return (TokenType) this.n0.getValue();
    }

    public final boolean u() {
        return ((Boolean) this.p0.getValue()).booleanValue();
    }

    public final Result v() {
        return (Result) this.m0.getValue();
    }

    public final IToken w(String str) {
        String lowerCase;
        List<IToken> value = x().d().getValue();
        Object obj = null;
        if (value == null) {
            return null;
        }
        Iterator<T> it = value.iterator();
        while (true) {
            if (!it.hasNext()) {
                break;
            }
            Object next = it.next();
            String contractAddress = ((IToken) next).getContractAddress();
            Objects.requireNonNull(contractAddress, "null cannot be cast to non-null type java.lang.String");
            Locale locale = Locale.ROOT;
            String lowerCase2 = contractAddress.toLowerCase(locale);
            fs1.e(lowerCase2, "(this as java.lang.Strin….toLowerCase(Locale.ROOT)");
            if (str == null) {
                lowerCase = null;
            } else {
                lowerCase = str.toLowerCase(locale);
                fs1.e(lowerCase, "(this as java.lang.Strin….toLowerCase(Locale.ROOT)");
            }
            if (fs1.b(lowerCase2, lowerCase)) {
                obj = next;
                break;
            }
        }
        return (IToken) obj;
    }

    public final TransferViewModel x() {
        return (TransferViewModel) this.l0.getValue();
    }
}
