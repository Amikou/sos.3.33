package net.safemoon.androidwallet.fragments;

import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import androidx.fragment.app.FragmentViewModelLazyKt;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import com.google.android.material.textfield.TextInputEditText;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import net.safemoon.androidwallet.R;
import net.safemoon.androidwallet.common.TokenType;
import net.safemoon.androidwallet.fragments.TokenListFragment;
import net.safemoon.androidwallet.fragments.common.BaseMainFragment;
import net.safemoon.androidwallet.model.common.PaymentMethod;
import net.safemoon.androidwallet.viewmodels.AddNewTokensViewModel;
import net.safemoon.androidwallet.viewmodels.HomeViewModel;

/* compiled from: TokenListFragment.kt */
/* loaded from: classes2.dex */
public final class TokenListFragment extends BaseMainFragment {
    public final sy1 i0 = FragmentViewModelLazyKt.a(this, d53.b(AddNewTokensViewModel.class), new TokenListFragment$special$$inlined$viewModels$default$2(new TokenListFragment$special$$inlined$viewModels$default$1(this)), new TokenListFragment$addNewTokensViewModel$2(this));
    public final sy1 j0 = FragmentViewModelLazyKt.a(this, d53.b(HomeViewModel.class), new TokenListFragment$special$$inlined$activityViewModels$default$1(this), new TokenListFragment$special$$inlined$activityViewModels$default$2(this));
    public gb1 k0;
    public boolean l0;
    public boolean m0;

    /* compiled from: TokenListFragment.kt */
    /* loaded from: classes2.dex */
    public static final class a {
        public a() {
        }

        public /* synthetic */ a(qi0 qi0Var) {
            this();
        }
    }

    /* compiled from: Comparisons.kt */
    /* loaded from: classes2.dex */
    public static final class b<T> implements Comparator {
        @Override // java.util.Comparator
        public final int compare(T t, T t2) {
            return l30.a(((q9) t).f(), ((q9) t2).f());
        }
    }

    /* compiled from: Comparisons.kt */
    /* loaded from: classes2.dex */
    public static final class c<T> implements Comparator {
        @Override // java.util.Comparator
        public final int compare(T t, T t2) {
            q9 q9Var = (q9) t2;
            String g = q9Var.g();
            TokenType.a aVar = TokenType.Companion;
            q9 q9Var2 = (q9) t;
            return l30.a(Boolean.valueOf(fs1.b(g, aVar.b(q9Var.b()).getNativeToken())), Boolean.valueOf(fs1.b(q9Var2.g(), aVar.b(q9Var2.b()).getNativeToken())));
        }
    }

    /* compiled from: TextView.kt */
    /* loaded from: classes2.dex */
    public static final class d implements TextWatcher {
        public d() {
        }

        @Override // android.text.TextWatcher
        public void afterTextChanged(Editable editable) {
            TokenListFragment.this.w().n(String.valueOf(editable));
        }

        @Override // android.text.TextWatcher
        public void beforeTextChanged(CharSequence charSequence, int i, int i2, int i3) {
        }

        @Override // android.text.TextWatcher
        public void onTextChanged(CharSequence charSequence, int i, int i2, int i3) {
        }
    }

    static {
        new a(null);
    }

    public static final void A(gb1 gb1Var, TokenListFragment tokenListFragment, List list) {
        fs1.f(gb1Var, "$this_apply");
        fs1.f(tokenListFragment, "this$0");
        new qb2();
        RecyclerView recyclerView = gb1Var.a;
        fs1.e(list, "it");
        ArrayList arrayList = new ArrayList();
        for (Object obj : list) {
            if (um1.a(((q9) obj).h()).d()) {
                arrayList.add(obj);
            }
        }
        recyclerView.setAdapter(new z64(j20.e0(j20.e0(arrayList, new b()), new c()), new TokenListFragment$loadTokens$1$1$4(tokenListFragment)));
    }

    public static final void B(TokenListFragment tokenListFragment, View view) {
        fs1.f(tokenListFragment, "this$0");
        tokenListFragment.f();
    }

    public static final boolean C(gb1 gb1Var, TextView textView, int i, KeyEvent keyEvent) {
        fs1.f(gb1Var, "$this_apply");
        if (i == 6) {
            gb1Var.b.a.clearFocus();
        }
        return false;
    }

    public static final void D(TokenListFragment tokenListFragment, TokenType tokenType) {
        fs1.f(tokenListFragment, "this$0");
        AddNewTokensViewModel w = tokenListFragment.w();
        fs1.e(tokenType, "it");
        w.l(tokenType);
        tokenListFragment.z();
    }

    @Override // androidx.fragment.app.Fragment
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
    }

    @Override // androidx.fragment.app.Fragment
    public View onCreateView(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle) {
        fs1.f(layoutInflater, "inflater");
        return layoutInflater.inflate(R.layout.fragment_token_list, viewGroup, false);
    }

    @Override // androidx.fragment.app.Fragment
    public void onResume() {
        super.onResume();
        if (this.l0) {
            this.l0 = false;
            pg4.b(requireActivity(), Boolean.FALSE);
        }
    }

    @Override // net.safemoon.androidwallet.fragments.common.BaseMainFragment, defpackage.qn, androidx.fragment.app.Fragment
    public void onViewCreated(View view, Bundle bundle) {
        yd3 yd3Var;
        TextInputEditText textInputEditText;
        fs1.f(view, "view");
        super.onViewCreated(view, bundle);
        final gb1 a2 = gb1.a(view);
        this.k0 = a2;
        if (a2 != null) {
            a2.c.a.setOnClickListener(new View.OnClickListener() { // from class: c74
                @Override // android.view.View.OnClickListener
                public final void onClick(View view2) {
                    TokenListFragment.B(TokenListFragment.this, view2);
                }
            });
            a2.c.c.setText(getResources().getText(R.string.screen_title_purchase_tokens));
            a2.a.setLayoutManager(new LinearLayoutManager(requireContext(), 1, false));
            TextInputEditText textInputEditText2 = a2.b.a;
            fs1.e(textInputEditText2, "searchBar.etSearch");
            textInputEditText2.addTextChangedListener(new d());
            a2.b.a.setOnEditorActionListener(new TextView.OnEditorActionListener() { // from class: d74
                @Override // android.widget.TextView.OnEditorActionListener
                public final boolean onEditorAction(TextView textView, int i, KeyEvent keyEvent) {
                    boolean C;
                    C = TokenListFragment.C(gb1.this, textView, i, keyEvent);
                    return C;
                }
            });
            a2.b.a.clearFocus();
        }
        y().l().observe(getViewLifecycleOwner(), new tl2() { // from class: b74
            @Override // defpackage.tl2
            public final void onChanged(Object obj) {
                TokenListFragment.D(TokenListFragment.this, (TokenType) obj);
            }
        });
        gb1 gb1Var = this.k0;
        if (gb1Var == null || (yd3Var = gb1Var.b) == null || (textInputEditText = yd3Var.a) == null) {
            return;
        }
        textInputEditText.clearFocus();
    }

    public final AddNewTokensViewModel w() {
        return (AddNewTokensViewModel) this.i0.getValue();
    }

    public final void x(PaymentMethod paymentMethod, String str) {
        if (this.m0) {
            return;
        }
        this.m0 = true;
        y().h(paymentMethod, str, new TokenListFragment$getCheckoutUrl$1(this), new TokenListFragment$getCheckoutUrl$2(this));
    }

    public final HomeViewModel y() {
        return (HomeViewModel) this.j0.getValue();
    }

    public final void z() {
        w().n("");
        final gb1 gb1Var = this.k0;
        if (gb1Var == null) {
            return;
        }
        w().j().observe(getViewLifecycleOwner(), new tl2() { // from class: a74
            @Override // defpackage.tl2
            public final void onChanged(Object obj) {
                TokenListFragment.A(gb1.this, this, (List) obj);
            }
        });
    }
}
