package net.safemoon.androidwallet.fragments;

import kotlin.jvm.internal.Lambda;
import net.safemoon.androidwallet.model.common.PaymentMethod;

/* compiled from: TokenListFragment.kt */
/* loaded from: classes2.dex */
public final class TokenListFragment$loadTokens$1$1$4 extends Lambda implements tc1<q9, te4> {
    public final /* synthetic */ TokenListFragment this$0;

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public TokenListFragment$loadTokens$1$1$4(TokenListFragment tokenListFragment) {
        super(1);
        this.this$0 = tokenListFragment;
    }

    @Override // defpackage.tc1
    public /* bridge */ /* synthetic */ te4 invoke(q9 q9Var) {
        invoke2(q9Var);
        return te4.a;
    }

    /* renamed from: invoke  reason: avoid collision after fix types in other method */
    public final void invoke2(q9 q9Var) {
        fs1.f(q9Var, "it");
        if (!q9Var.i()) {
            this.this$0.w().m(q9Var, true);
        }
        this.this$0.x(PaymentMethod.MOONPAY, um1.a(q9Var.h()).c());
    }
}
