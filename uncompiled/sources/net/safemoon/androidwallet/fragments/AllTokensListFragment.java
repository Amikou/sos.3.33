package net.safemoon.androidwallet.fragments;

import android.os.Bundle;
import android.text.Editable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;
import androidx.fragment.app.FragmentViewModelLazyKt;
import androidx.lifecycle.l;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;
import com.creageek.segmentedbutton.SegmentedButton;
import com.google.android.material.textview.MaterialTextView;
import java.util.ArrayList;
import java.util.Iterator;
import net.safemoon.androidwallet.R;
import net.safemoon.androidwallet.common.ActiveTokenListMode;
import net.safemoon.androidwallet.fragments.AllTokensListFragment;
import net.safemoon.androidwallet.fragments.common.BaseMainFragment;
import net.safemoon.androidwallet.model.notificationHistory.NotificationHistory;
import net.safemoon.androidwallet.model.notificationHistory.NotificationHistoryData;
import net.safemoon.androidwallet.model.notificationHistory.NotificationHistoryResult;
import net.safemoon.androidwallet.viewmodels.HomeViewModel;

/* compiled from: AllTokensListFragment.kt */
/* loaded from: classes2.dex */
public final class AllTokensListFragment extends BaseMainFragment implements View.OnClickListener {
    public TextView l0;
    public TextView m0;
    public TextView n0;
    public TextView o0;
    public TextView p0;
    public TextView q0;
    public TextView r0;
    public HomeViewModel s0;
    public q91 t0;
    public RecyclerView u0;
    public pa0 v0;
    public final String i0 = "1h";
    public final String j0 = "24h";
    public final String k0 = "7d";
    public String w0 = "24h";
    public String x0 = "desc";
    public String y0 = "asc";
    public ActiveTokenListMode z0 = ActiveTokenListMode.GAINER;
    public final sy1 A0 = FragmentViewModelLazyKt.a(this, d53.b(qi2.class), new AllTokensListFragment$special$$inlined$activityViewModels$default$1(this), new AllTokensListFragment$special$$inlined$activityViewModels$default$2(this));
    public String B0 = "24h";

    /* compiled from: AllTokensListFragment.kt */
    /* loaded from: classes2.dex */
    public static final class a extends vu0 {
        public a() {
        }

        @Override // defpackage.vu0, android.text.TextWatcher
        public void afterTextChanged(Editable editable) {
            String obj;
            HomeViewModel homeViewModel = AllTokensListFragment.this.s0;
            if (homeViewModel == null) {
                return;
            }
            String str = "";
            if (editable != null && (obj = editable.toString()) != null) {
                str = obj;
            }
            homeViewModel.r(str);
        }
    }

    /* compiled from: AllTokensListFragment.kt */
    /* loaded from: classes2.dex */
    public static final class b extends RecyclerView.i {
        public b() {
        }

        @Override // androidx.recyclerview.widget.RecyclerView.i
        public void onItemRangeInserted(int i, int i2) {
            RecyclerView recyclerView;
            RecyclerView.LayoutManager layoutManager;
            super.onItemRangeInserted(i, i2);
            if (i != 0 || (recyclerView = AllTokensListFragment.this.u0) == null || (layoutManager = recyclerView.getLayoutManager()) == null) {
                return;
            }
            layoutManager.G1(0);
        }
    }

    public static final void F(AllTokensListFragment allTokensListFragment) {
        fs1.f(allTokensListFragment, "this$0");
        allTokensListFragment.P();
    }

    public static final void H(AllTokensListFragment allTokensListFragment, ActiveTokenListMode activeTokenListMode) {
        fs1.f(allTokensListFragment, "this$0");
        fs1.f(activeTokenListMode, "mode");
        pa0 pa0Var = allTokensListFragment.v0;
        fs1.d(pa0Var);
        pa0Var.c();
    }

    public static final void I(AllTokensListFragment allTokensListFragment, String str) {
        fs1.f(allTokensListFragment, "this$0");
        pa0 pa0Var = allTokensListFragment.v0;
        fs1.d(pa0Var);
        pa0Var.c();
    }

    public static final void J(AllTokensListFragment allTokensListFragment, NotificationHistory notificationHistory) {
        NotificationHistoryData data;
        ArrayList<NotificationHistoryResult> result;
        wf wfVar;
        MaterialTextView materialTextView;
        fs1.f(allTokensListFragment, "this$0");
        if (notificationHistory == null || (data = notificationHistory.getData()) == null || (result = data.getResult()) == null) {
            return;
        }
        ArrayList arrayList = new ArrayList();
        Iterator<T> it = result.iterator();
        while (true) {
            if (!it.hasNext()) {
                break;
            }
            Object next = it.next();
            if (true ^ ((NotificationHistoryResult) next).read) {
                arrayList.add(next);
            }
        }
        int size = arrayList.size();
        q91 q91Var = allTokensListFragment.t0;
        if (q91Var == null || (wfVar = q91Var.d) == null || (materialTextView = wfVar.i) == null) {
            return;
        }
        materialTextView.setVisibility(e30.m0(size > 0));
        materialTextView.setText(size < 100 ? String.valueOf(size) : "99+");
    }

    public final qi2 B() {
        return (qi2) this.A0.getValue();
    }

    public final String C() {
        return this.B0;
    }

    public final void D() {
        q91 q91Var = this.t0;
        LinearLayout linearLayout = q91Var == null ? null : q91Var.c;
        if (linearLayout == null) {
            return;
        }
        linearLayout.setVisibility(8);
    }

    public final void E() {
        this.v0 = new pa0(new AllTokensListFragment$initRecyclerView$1(this));
        RecyclerView recyclerView = this.u0;
        fs1.d(recyclerView);
        recyclerView.setLayoutManager(new LinearLayoutManager(getActivity(), 1, false));
        RecyclerView recyclerView2 = this.u0;
        fs1.d(recyclerView2);
        recyclerView2.setAdapter(this.v0);
        q91 q91Var = this.t0;
        fs1.d(q91Var);
        q91Var.b.setOnRefreshListener(new SwipeRefreshLayout.j() { // from class: db
            @Override // androidx.swiperefreshlayout.widget.SwipeRefreshLayout.j
            public final void onRefresh() {
                AllTokensListFragment.F(AllTokensListFragment.this);
            }
        });
        pa0 pa0Var = this.v0;
        fs1.d(pa0Var);
        pa0Var.b(new AllTokensListFragment$initRecyclerView$3(this));
    }

    public final void G() {
        HomeViewModel homeViewModel = this.s0;
        fs1.d(homeViewModel);
        homeViewModel.e().observe(getViewLifecycleOwner(), new tl2() { // from class: bb
            @Override // defpackage.tl2
            public final void onChanged(Object obj) {
                AllTokensListFragment.H(AllTokensListFragment.this, (ActiveTokenListMode) obj);
            }
        });
        HomeViewModel homeViewModel2 = this.s0;
        fs1.d(homeViewModel2);
        homeViewModel2.d().observe(getViewLifecycleOwner(), new tl2() { // from class: ab
            @Override // defpackage.tl2
            public final void onChanged(Object obj) {
                AllTokensListFragment.I(AllTokensListFragment.this, (String) obj);
            }
        });
        K();
        rz1 viewLifecycleOwner = getViewLifecycleOwner();
        fs1.e(viewLifecycleOwner, "viewLifecycleOwner");
        sz1.a(viewLifecycleOwner).b(new AllTokensListFragment$observeViewModel$3(this, null));
    }

    public final void K() {
        pa0 pa0Var = this.v0;
        fs1.d(pa0Var);
        pa0Var.registerAdapterDataObserver(new b());
    }

    public final void L(TextView textView, String str) {
        if (getActivity() != null) {
            TextView textView2 = this.l0;
            fs1.d(textView2);
            textView2.setBackgroundResource(R.drawable.white_bg_badge);
            TextView textView3 = this.n0;
            fs1.d(textView3);
            textView3.setBackgroundResource(R.drawable.white_bg_badge);
            TextView textView4 = this.m0;
            fs1.d(textView4);
            textView4.setBackgroundResource(R.drawable.white_bg_badge);
            fs1.d(textView);
            textView.setBackgroundResource(R.drawable.primary_bg_baged);
            TextView textView5 = this.r0;
            fs1.d(textView5);
            textView5.setText(str);
        }
    }

    public final void M(TextView textView) {
        if (getActivity() != null) {
            TextView textView2 = this.o0;
            fs1.d(textView2);
            textView2.setBackgroundResource(R.drawable.white_bg_badge);
            TextView textView3 = this.p0;
            fs1.d(textView3);
            textView3.setBackgroundResource(R.drawable.white_bg_badge);
            TextView textView4 = this.q0;
            fs1.d(textView4);
            textView4.setBackgroundResource(R.drawable.white_bg_badge);
            fs1.d(textView);
            textView.setBackgroundResource(R.drawable.primary_bg_baged);
        }
    }

    public final void N(TextView textView) {
        TextView textView2 = this.o0;
        fs1.d(textView2);
        textView2.setTextColor(m70.d(requireContext(), R.color.t1));
        TextView textView3 = this.p0;
        fs1.d(textView3);
        textView3.setTextColor(m70.d(requireContext(), R.color.t1));
        TextView textView4 = this.q0;
        fs1.d(textView4);
        textView4.setTextColor(m70.d(requireContext(), R.color.t1));
        fs1.d(textView);
        textView.setTextColor(m70.d(requireContext(), R.color.white));
    }

    public final void O(TextView textView) {
        TextView textView2 = this.l0;
        fs1.d(textView2);
        textView2.setTextColor(m70.d(requireContext(), R.color.t1));
        TextView textView3 = this.m0;
        fs1.d(textView3);
        textView3.setTextColor(m70.d(requireContext(), R.color.t1));
        TextView textView4 = this.n0;
        fs1.d(textView4);
        textView4.setTextColor(m70.d(requireContext(), R.color.t1));
        fs1.d(textView);
        textView.setTextColor(m70.d(requireContext(), R.color.white));
    }

    public final void P() {
        HomeViewModel homeViewModel = this.s0;
        fs1.d(homeViewModel);
        homeViewModel.q(this.x0, this.w0, this.z0);
    }

    public final void Q() {
        q91 q91Var = this.t0;
        LinearLayout linearLayout = q91Var == null ? null : q91Var.c;
        if (linearLayout == null) {
            return;
        }
        linearLayout.setVisibility(0);
    }

    @Override // android.view.View.OnClickListener
    public void onClick(View view) {
        fs1.f(view, "v");
        switch (view.getId()) {
            case R.id.btn1h /* 2131362026 */:
                M(this.o0);
                N(this.o0);
                String str = this.i0;
                this.B0 = str;
                this.w0 = str;
                P();
                return;
            case R.id.btn24h /* 2131362027 */:
                M(this.p0);
                N(this.p0);
                String str2 = this.j0;
                this.B0 = str2;
                this.w0 = str2;
                P();
                return;
            case R.id.btn7d /* 2131362030 */:
                M(this.q0);
                N(this.q0);
                String str3 = this.k0;
                this.B0 = str3;
                this.w0 = str3;
                P();
                return;
            case R.id.btnRecent /* 2131362085 */:
                L(this.l0, getResources().getString(R.string.recently_added));
                O(this.l0);
                D();
                this.x0 = this.x0;
                this.w0 = "date_added";
                this.z0 = ActiveTokenListMode.RECENT;
                P();
                return;
            case R.id.btnTopGainers /* 2131362097 */:
                L(this.m0, getResources().getString(R.string.top_gainers));
                O(this.m0);
                Q();
                this.x0 = this.x0;
                this.w0 = this.B0;
                this.z0 = ActiveTokenListMode.GAINER;
                P();
                return;
            case R.id.btnTopLosers /* 2131362098 */:
                L(this.n0, getResources().getString(R.string.top_losers));
                O(this.n0);
                Q();
                this.x0 = this.y0;
                this.w0 = this.B0;
                this.z0 = ActiveTokenListMode.LOSER;
                P();
                return;
            case R.id.ivNotification /* 2131362696 */:
                ce2 b2 = eb.b();
                fs1.e(b2, "actionAllTokensListFragm…ficationHistoryFragment()");
                g(b2);
                return;
            case R.id.ivTokenList /* 2131362702 */:
                ce2 a2 = eb.a();
                fs1.e(a2, "actionAllTokensListFragm…tToMyTokensListFragment()");
                g(a2);
                return;
            default:
                return;
        }
    }

    @Override // androidx.fragment.app.Fragment
    public View onCreateView(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle) {
        fs1.f(layoutInflater, "inflater");
        View inflate = layoutInflater.inflate(R.layout.fragment_alltokens_list, viewGroup, false);
        this.s0 = (HomeViewModel) new l(requireActivity()).a(HomeViewModel.class);
        q91 a2 = q91.a(inflate);
        this.t0 = a2;
        fs1.d(a2);
        a2.d.c.setVisibility(4);
        q91 q91Var = this.t0;
        fs1.d(q91Var);
        q91Var.d.i.setVisibility(4);
        q91 q91Var2 = this.t0;
        fs1.d(q91Var2);
        q91Var2.d.g.setChecked(false);
        this.r0 = (TextView) inflate.findViewById(R.id.tv_title);
        this.u0 = (RecyclerView) inflate.findViewById(R.id.rView);
        this.l0 = (TextView) inflate.findViewById(R.id.btnRecent);
        this.m0 = (TextView) inflate.findViewById(R.id.btnTopGainers);
        this.n0 = (TextView) inflate.findViewById(R.id.btnTopLosers);
        this.o0 = (TextView) inflate.findViewById(R.id.btn1h);
        this.p0 = (TextView) inflate.findViewById(R.id.btn24h);
        this.q0 = (TextView) inflate.findViewById(R.id.btn7d);
        TextView textView = this.l0;
        fs1.d(textView);
        textView.setOnClickListener(this);
        TextView textView2 = this.m0;
        fs1.d(textView2);
        textView2.setOnClickListener(this);
        TextView textView3 = this.n0;
        fs1.d(textView3);
        textView3.setOnClickListener(this);
        TextView textView4 = this.o0;
        fs1.d(textView4);
        textView4.setOnClickListener(this);
        TextView textView5 = this.p0;
        fs1.d(textView5);
        textView5.setOnClickListener(this);
        TextView textView6 = this.q0;
        fs1.d(textView6);
        textView6.setOnClickListener(this);
        View findViewById = inflate.findViewById(R.id.ivTokenList);
        if (findViewById != null) {
            findViewById.setOnClickListener(this);
        }
        View findViewById2 = inflate.findViewById(R.id.ivNotification);
        if (findViewById2 != null) {
            findViewById2.setOnClickListener(this);
        }
        E();
        P();
        HomeViewModel homeViewModel = this.s0;
        if (homeViewModel != null) {
            homeViewModel.r("");
        }
        q91 q91Var3 = this.t0;
        fs1.d(q91Var3);
        q91Var3.a.b.addTextChangedListener(new z44(new a(), 0L, 2, null));
        B().e().observe(getViewLifecycleOwner(), new tl2() { // from class: cb
            @Override // defpackage.tl2
            public final void onChanged(Object obj) {
                AllTokensListFragment.J(AllTokensListFragment.this, (NotificationHistory) obj);
            }
        });
        return inflate;
    }

    @Override // net.safemoon.androidwallet.fragments.common.BaseMainFragment, defpackage.qn, androidx.fragment.app.Fragment
    public void onViewCreated(View view, Bundle bundle) {
        fs1.f(view, "view");
        super.onViewCreated(view, bundle);
        q91 q91Var = this.t0;
        fs1.d(q91Var);
        SegmentedButton segmentedButton = q91Var.d.h;
        fs1.e(segmentedButton, "binding!!.topBar.segmentedGroup");
        n(segmentedButton);
        G();
    }
}
