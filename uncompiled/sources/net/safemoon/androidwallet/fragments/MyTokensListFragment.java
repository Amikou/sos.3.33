package net.safemoon.androidwallet.fragments;

import android.app.Activity;
import android.content.Context;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.fragment.app.FragmentViewModelLazyKt;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;
import com.creageek.segmentedbutton.SegmentedButton;
import com.google.android.material.textview.MaterialTextView;
import defpackage.lt2;
import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import net.safemoon.androidwallet.R;
import net.safemoon.androidwallet.adapter.touchHelper.RecyclerTouchListener;
import net.safemoon.androidwallet.common.TokenType;
import net.safemoon.androidwallet.dialogs.AnchorSwitchWallet;
import net.safemoon.androidwallet.domain.listener.dalog.OnSelectTokenTypeClickListener;
import net.safemoon.androidwallet.fragments.MyTokensListFragment;
import net.safemoon.androidwallet.fragments.common.BaseMainFragment;
import net.safemoon.androidwallet.model.notificationHistory.NotificationHistory;
import net.safemoon.androidwallet.model.notificationHistory.NotificationHistoryData;
import net.safemoon.androidwallet.model.notificationHistory.NotificationHistoryResult;
import net.safemoon.androidwallet.model.wallets.Wallet;
import net.safemoon.androidwallet.ui.displayModel.UserTokenItemDisplayModel;
import net.safemoon.androidwallet.viewmodels.HomeViewModel;
import net.safemoon.androidwallet.viewmodels.MultiWalletViewModel;
import net.safemoon.androidwallet.viewmodels.MyTokensListViewModel;
import net.safemoon.androidwallet.views.FixedForAppBarLayoutManager;
import net.safemoon.androidwallet.views.gesture.RecyclerItemSwipeHelper;

/* compiled from: MyTokensListFragment.kt */
/* loaded from: classes2.dex */
public final class MyTokensListFragment extends BaseMainFragment {
    public hb1 i0;
    public qb2 m0;
    public FixedForAppBarLayoutManager n0;
    public OnSelectTokenTypeClickListener o0;
    public RecyclerTouchListener p0;
    public final sy1 j0 = FragmentViewModelLazyKt.a(this, d53.b(MyTokensListViewModel.class), new MyTokensListFragment$special$$inlined$activityViewModels$1(this), new MyTokensListFragment$myTokenListViewModel$2(this));
    public final sy1 k0 = FragmentViewModelLazyKt.a(this, d53.b(HomeViewModel.class), new MyTokensListFragment$special$$inlined$activityViewModels$default$1(this), new MyTokensListFragment$special$$inlined$activityViewModels$default$2(this));
    public final sy1 l0 = FragmentViewModelLazyKt.a(this, d53.b(MultiWalletViewModel.class), new MyTokensListFragment$special$$inlined$viewModels$default$2(new MyTokensListFragment$special$$inlined$viewModels$default$1(this)), null);
    public final sy1 q0 = FragmentViewModelLazyKt.a(this, d53.b(qi2.class), new MyTokensListFragment$special$$inlined$activityViewModels$default$3(this), new MyTokensListFragment$special$$inlined$activityViewModels$default$4(this));

    /* compiled from: MyTokensListFragment.kt */
    /* loaded from: classes2.dex */
    public static final class a extends RecyclerItemSwipeHelper {
        /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
        public a(Context context, RecyclerView recyclerView, qb2 qb2Var, b bVar) {
            super(context, bVar, recyclerView, qb2Var);
            fs1.e(context, "requireContext()");
            fs1.e(recyclerView, "rvMyTokenList");
        }

        @Override // net.safemoon.androidwallet.views.gesture.RecyclerItemSwipeHelper
        public void n(RecyclerView.a0 a0Var, List<RecyclerItemSwipeHelper.c> list) {
            fs1.d(list);
            list.add(new RecyclerItemSwipeHelper.c(this, g83.f(MyTokensListFragment.this.getResources(), R.drawable.ic_baseline_delete_24, null), new ColorDrawable(g83.d(MyTokensListFragment.this.getResources(), R.color.red, null))));
        }
    }

    /* compiled from: MyTokensListFragment.kt */
    /* loaded from: classes2.dex */
    public static final class b implements bt1 {
        public b() {
        }

        @Override // defpackage.bt1
        public void a() {
            if (MyTokensListFragment.this.m0 != null) {
                MyTokensListViewModel F = MyTokensListFragment.this.F();
                qb2 qb2Var = MyTokensListFragment.this.m0;
                if (qb2Var == null) {
                    fs1.r("adapter");
                    qb2Var = null;
                }
                F.N(qb2Var.b());
            }
        }

        @Override // defpackage.bt1
        public void b(int i) {
            qb2 qb2Var = MyTokensListFragment.this.m0;
            qb2 qb2Var2 = null;
            if (qb2Var == null) {
                fs1.r("adapter");
                qb2Var = null;
            }
            UserTokenItemDisplayModel userTokenItemDisplayModel = qb2Var.b().get(i);
            MyTokensListFragment myTokensListFragment = MyTokensListFragment.this;
            UserTokenItemDisplayModel userTokenItemDisplayModel2 = userTokenItemDisplayModel;
            qb2 qb2Var3 = myTokensListFragment.m0;
            if (qb2Var3 == null) {
                fs1.r("adapter");
            } else {
                qb2Var2 = qb2Var3;
            }
            qb2Var2.h(i);
            myTokensListFragment.F().I(userTokenItemDisplayModel2);
        }
    }

    /* compiled from: MyTokensListFragment.kt */
    /* loaded from: classes2.dex */
    public static final class c extends OnSelectTokenTypeClickListener {
        public c(d dVar, WeakReference<Activity> weakReference, Map<String, ? extends TokenType> map) {
            super(dVar, weakReference, map);
        }

        @Override // net.safemoon.androidwallet.domain.listener.dalog.OnSelectTokenTypeClickListener
        public TokenType c() {
            Context requireContext = MyTokensListFragment.this.requireContext();
            fs1.e(requireContext, "requireContext()");
            return e30.e(requireContext);
        }
    }

    /* compiled from: MyTokensListFragment.kt */
    /* loaded from: classes2.dex */
    public static final class d implements lt2.a {
        public d() {
        }

        @Override // defpackage.lt2.a
        public void a(TokenType tokenType) {
            fs1.f(tokenType, "token");
            MyTokensListFragment.this.F().J(tokenType);
            MyTokensListFragment.this.D().p(tokenType);
        }
    }

    public static final void I(MyTokensListFragment myTokensListFragment, TokenType tokenType) {
        fs1.f(myTokensListFragment, "this$0");
        hb1 hb1Var = myTokensListFragment.i0;
        hb1 hb1Var2 = null;
        if (hb1Var == null) {
            fs1.r("binding");
            hb1Var = null;
        }
        hb1Var.e.d.setText(tokenType.getPlaneName());
        hb1 hb1Var3 = myTokensListFragment.i0;
        if (hb1Var3 == null) {
            fs1.r("binding");
        } else {
            hb1Var2 = hb1Var3;
        }
        hb1Var2.e.c.setImageResource(tokenType.getIcon());
    }

    public static final void J(MyTokensListFragment myTokensListFragment, List list) {
        RecyclerTouchListener recyclerTouchListener;
        fs1.f(myTokensListFragment, "this$0");
        ArrayList arrayList = new ArrayList();
        int size = list.size() - 1;
        if (size >= 0) {
            int i = 0;
            while (true) {
                int i2 = i + 1;
                if (myTokensListFragment.F().v().contains(((UserTokenItemDisplayModel) list.get(i)).getSymbol())) {
                    arrayList.add(Integer.valueOf(i));
                }
                if (i2 > size) {
                    break;
                }
                i = i2;
            }
        }
        if (arrayList.size() > 0 && (recyclerTouchListener = myTokensListFragment.p0) != null) {
            Object[] array = arrayList.toArray(new Integer[0]);
            Objects.requireNonNull(array, "null cannot be cast to non-null type kotlin.Array<T>");
            Integer[] numArr = (Integer[]) array;
            recyclerTouchListener.z((Integer[]) Arrays.copyOf(numArr, numArr.length));
        }
        qb2 qb2Var = myTokensListFragment.m0;
        if (qb2Var == null) {
            fs1.r("adapter");
            qb2Var = null;
        }
        fs1.e(list, "items");
        qb2Var.l(list);
    }

    public static final void L(MyTokensListFragment myTokensListFragment, NotificationHistory notificationHistory) {
        NotificationHistoryData data;
        ArrayList<NotificationHistoryResult> result;
        MaterialTextView materialTextView;
        fs1.f(myTokensListFragment, "this$0");
        if (notificationHistory == null || (data = notificationHistory.getData()) == null || (result = data.getResult()) == null) {
            return;
        }
        ArrayList arrayList = new ArrayList();
        Iterator<T> it = result.iterator();
        while (true) {
            if (!it.hasNext()) {
                break;
            }
            Object next = it.next();
            if (true ^ ((NotificationHistoryResult) next).read) {
                arrayList.add(next);
            }
        }
        int size = arrayList.size();
        hb1 hb1Var = myTokensListFragment.i0;
        if (hb1Var == null) {
            fs1.r("binding");
            hb1Var = null;
        }
        wf wfVar = hb1Var.h;
        if (wfVar == null || (materialTextView = wfVar.i) == null) {
            return;
        }
        materialTextView.setVisibility(e30.m0(size > 0));
        materialTextView.setText(size < 100 ? String.valueOf(size) : "99+");
    }

    public static final void M(MyTokensListFragment myTokensListFragment, View view) {
        fs1.f(myTokensListFragment, "this$0");
        ce2 e = cc2.e();
        fs1.e(e, "actionNavigationMyTokens…ficationHistoryFragment()");
        myTokensListFragment.g(e);
    }

    public static final void Q(MyTokensListFragment myTokensListFragment, View view) {
        fs1.f(myTokensListFragment, "this$0");
        ce2 a2 = cc2.a();
        fs1.e(a2, "actionMyTokensListFragmentToAddNewTokenFragment()");
        myTokensListFragment.g(a2);
    }

    public static final void S(MyTokensListFragment myTokensListFragment, View view) {
        fs1.f(myTokensListFragment, "this$0");
        ce2 b2 = cc2.b();
        fs1.e(b2, "actionMyTokensListFragmentToReceiveFragment()");
        myTokensListFragment.g(b2);
    }

    public static final void T(MyTokensListFragment myTokensListFragment, View view) {
        fs1.f(myTokensListFragment, "this$0");
        ce2 c2 = cc2.c();
        fs1.e(c2, "actionMyTokensListFragmentToSendFragment()");
        myTokensListFragment.g(c2);
    }

    public static final void U(hb1 hb1Var, MyTokensListFragment myTokensListFragment) {
        fs1.f(hb1Var, "$this_apply");
        fs1.f(myTokensListFragment, "this$0");
        hb1Var.g.setRefreshing(false);
        if (myTokensListFragment.D().l().getValue() == null) {
            return;
        }
        myTokensListFragment.F().K();
    }

    public static final boolean V(MyTokensListFragment myTokensListFragment, View view) {
        fs1.f(myTokensListFragment, "this$0");
        wb wbVar = new wb(myTokensListFragment.F());
        Context requireContext = myTokensListFragment.requireContext();
        fs1.e(requireContext, "requireContext()");
        fs1.e(view, "v");
        hb1 hb1Var = myTokensListFragment.i0;
        if (hb1Var == null) {
            fs1.r("binding");
            hb1Var = null;
        }
        wbVar.g(requireContext, view, hb1Var.b);
        return true;
    }

    public static final void W(MyTokensListFragment myTokensListFragment, View view) {
        fs1.f(myTokensListFragment, "this$0");
        AnchorSwitchWallet anchorSwitchWallet = new AnchorSwitchWallet(myTokensListFragment.E(), R.id.myTokensListFragment);
        Context requireContext = myTokensListFragment.requireContext();
        fs1.e(requireContext, "requireContext()");
        fs1.e(view, "v");
        hb1 hb1Var = myTokensListFragment.i0;
        if (hb1Var == null) {
            fs1.r("binding");
            hb1Var = null;
        }
        anchorSwitchWallet.h(requireContext, view, hb1Var.b);
    }

    public static final void X(MyTokensListFragment myTokensListFragment, Double d2) {
        fs1.f(myTokensListFragment, "this$0");
        if (d2 != null) {
            hb1 hb1Var = myTokensListFragment.i0;
            hb1 hb1Var2 = null;
            if (hb1Var == null) {
                fs1.r("binding");
                hb1Var = null;
            }
            TextView textView = hb1Var.k;
            fs1.e(textView, "binding.txtSymbol");
            e30.W(textView);
            hb1 hb1Var3 = myTokensListFragment.i0;
            if (hb1Var3 == null) {
                fs1.r("binding");
            } else {
                hb1Var2 = hb1Var3;
            }
            TextView textView2 = hb1Var2.j;
            fs1.e(textView2, "binding.tvWalletBlnc");
            e30.N(textView2, d2.doubleValue(), false);
        }
    }

    public final HomeViewModel D() {
        return (HomeViewModel) this.k0.getValue();
    }

    public final MultiWalletViewModel E() {
        return (MultiWalletViewModel) this.l0.getValue();
    }

    public final MyTokensListViewModel F() {
        return (MyTokensListViewModel) this.j0.getValue();
    }

    public final qi2 G() {
        return (qi2) this.q0.getValue();
    }

    public final void H() {
        D().l().observe(getViewLifecycleOwner(), new tl2() { // from class: ub2
            @Override // defpackage.tl2
            public final void onChanged(Object obj) {
                MyTokensListFragment.I(MyTokensListFragment.this, (TokenType) obj);
            }
        });
        F().A().observe(getViewLifecycleOwner(), new qx2(new tl2() { // from class: tb2
            @Override // defpackage.tl2
            public final void onChanged(Object obj) {
                MyTokensListFragment.J(MyTokensListFragment.this, (List) obj);
            }
        }));
    }

    public final void K() {
        hb1 hb1Var = this.i0;
        hb1 hb1Var2 = null;
        if (hb1Var == null) {
            fs1.r("binding");
            hb1Var = null;
        }
        hb1Var.h.a.setVisibility(0);
        hb1 hb1Var3 = this.i0;
        if (hb1Var3 == null) {
            fs1.r("binding");
        } else {
            hb1Var2 = hb1Var3;
        }
        hb1Var2.h.a.setOnClickListener(new View.OnClickListener() { // from class: xb2
            @Override // android.view.View.OnClickListener
            public final void onClick(View view) {
                MyTokensListFragment.M(MyTokensListFragment.this, view);
            }
        });
        G().e().observe(getViewLifecycleOwner(), new tl2() { // from class: vb2
            @Override // defpackage.tl2
            public final void onChanged(Object obj) {
                MyTokensListFragment.L(MyTokensListFragment.this, (NotificationHistory) obj);
            }
        });
    }

    public final void N() {
        qb2 qb2Var = new qb2();
        this.m0 = qb2Var;
        qb2Var.k(new MyTokensListFragment$setupAdapter$1(this));
        qb2 qb2Var2 = this.m0;
        qb2 qb2Var3 = null;
        if (qb2Var2 == null) {
            fs1.r("adapter");
            qb2Var2 = null;
        }
        FixedForAppBarLayoutManager fixedForAppBarLayoutManager = this.n0;
        if (fixedForAppBarLayoutManager == null) {
            fs1.r("layoutManager");
            fixedForAppBarLayoutManager = null;
        }
        qb2Var2.j(fixedForAppBarLayoutManager);
        hb1 hb1Var = this.i0;
        if (hb1Var == null) {
            fs1.r("binding");
            hb1Var = null;
        }
        RecyclerView recyclerView = hb1Var.f;
        qb2 qb2Var4 = this.m0;
        if (qb2Var4 == null) {
            fs1.r("adapter");
        } else {
            qb2Var3 = qb2Var4;
        }
        recyclerView.setAdapter(qb2Var3);
        O();
    }

    public final void O() {
        qb2 qb2Var;
        Context requireContext = requireContext();
        hb1 hb1Var = this.i0;
        if (hb1Var == null) {
            fs1.r("binding");
            hb1Var = null;
        }
        RecyclerView recyclerView = hb1Var.f;
        qb2 qb2Var2 = this.m0;
        if (qb2Var2 == null) {
            fs1.r("adapter");
            qb2Var = null;
        } else {
            qb2Var = qb2Var2;
        }
        new a(requireContext, recyclerView, qb2Var, new b());
    }

    public final void P() {
        hb1 hb1Var = this.i0;
        hb1 hb1Var2 = null;
        if (hb1Var == null) {
            fs1.r("binding");
            hb1Var = null;
        }
        hb1Var.h.i.setVisibility(4);
        hb1 hb1Var3 = this.i0;
        if (hb1Var3 == null) {
            fs1.r("binding");
            hb1Var3 = null;
        }
        hb1Var3.h.a.setVisibility(4);
        hb1 hb1Var4 = this.i0;
        if (hb1Var4 == null) {
            fs1.r("binding");
            hb1Var4 = null;
        }
        hb1Var4.h.b.setVisibility(4);
        hb1 hb1Var5 = this.i0;
        if (hb1Var5 == null) {
            fs1.r("binding");
            hb1Var5 = null;
        }
        hb1Var5.h.c.setVisibility(0);
        hb1 hb1Var6 = this.i0;
        if (hb1Var6 == null) {
            fs1.r("binding");
            hb1Var6 = null;
        }
        hb1Var6.h.g.setChecked(true);
        hb1 hb1Var7 = this.i0;
        if (hb1Var7 == null) {
            fs1.r("binding");
            hb1Var7 = null;
        }
        SegmentedButton segmentedButton = hb1Var7.h.h;
        fs1.e(segmentedButton, "binding.topBar.segmentedGroup");
        n(segmentedButton);
        hb1 hb1Var8 = this.i0;
        if (hb1Var8 == null) {
            fs1.r("binding");
        } else {
            hb1Var2 = hb1Var8;
        }
        hb1Var2.h.c.setOnClickListener(new View.OnClickListener() { // from class: yb2
            @Override // android.view.View.OnClickListener
            public final void onClick(View view) {
                MyTokensListFragment.Q(MyTokensListFragment.this, view);
            }
        });
    }

    public final void R() {
        P();
        this.n0 = new FixedForAppBarLayoutManager(requireContext());
        hb1 hb1Var = this.i0;
        hb1 hb1Var2 = null;
        if (hb1Var == null) {
            fs1.r("binding");
            hb1Var = null;
        }
        RecyclerView recyclerView = hb1Var.f;
        FixedForAppBarLayoutManager fixedForAppBarLayoutManager = this.n0;
        if (fixedForAppBarLayoutManager == null) {
            fs1.r("layoutManager");
            fixedForAppBarLayoutManager = null;
        }
        recyclerView.setLayoutManager(fixedForAppBarLayoutManager);
        this.o0 = new c(new d(), new WeakReference(requireActivity()), D().m());
        hb1 hb1Var3 = this.i0;
        if (hb1Var3 == null) {
            fs1.r("binding");
            hb1Var3 = null;
        }
        hb1Var3.e.a.setOnClickListener(this.o0);
        hb1 hb1Var4 = this.i0;
        if (hb1Var4 == null) {
            fs1.r("binding");
            hb1Var4 = null;
        }
        hb1Var4.c.setOnClickListener(new View.OnClickListener() { // from class: zb2
            @Override // android.view.View.OnClickListener
            public final void onClick(View view) {
                MyTokensListFragment.S(MyTokensListFragment.this, view);
            }
        });
        hb1 hb1Var5 = this.i0;
        if (hb1Var5 == null) {
            fs1.r("binding");
            hb1Var5 = null;
        }
        hb1Var5.d.setOnClickListener(new View.OnClickListener() { // from class: ac2
            @Override // android.view.View.OnClickListener
            public final void onClick(View view) {
                MyTokensListFragment.T(MyTokensListFragment.this, view);
            }
        });
        final hb1 hb1Var6 = this.i0;
        if (hb1Var6 == null) {
            fs1.r("binding");
            hb1Var6 = null;
        }
        hb1Var6.g.setOnRefreshListener(new SwipeRefreshLayout.j() { // from class: sb2
            @Override // androidx.swiperefreshlayout.widget.SwipeRefreshLayout.j
            public final void onRefresh() {
                MyTokensListFragment.U(hb1.this, this);
            }
        });
        hb1 hb1Var7 = this.i0;
        if (hb1Var7 == null) {
            fs1.r("binding");
            hb1Var7 = null;
        }
        TextView textView = hb1Var7.j;
        fs1.e(textView, "binding.tvWalletBlnc");
        e30.X(textView, new MyTokensListFragment$setupView$6(this));
        hb1 hb1Var8 = this.i0;
        if (hb1Var8 == null) {
            fs1.r("binding");
            hb1Var8 = null;
        }
        hb1Var8.j.setOnLongClickListener(new View.OnLongClickListener() { // from class: bc2
            @Override // android.view.View.OnLongClickListener
            public final boolean onLongClick(View view) {
                boolean V;
                V = MyTokensListFragment.V(MyTokensListFragment.this, view);
                return V;
            }
        });
        hb1 hb1Var9 = this.i0;
        if (hb1Var9 == null) {
            fs1.r("binding");
        } else {
            hb1Var2 = hb1Var9;
        }
        hb1Var2.i.setOnClickListener(new View.OnClickListener() { // from class: wb2
            @Override // android.view.View.OnClickListener
            public final void onClick(View view) {
                MyTokensListFragment.W(MyTokensListFragment.this, view);
            }
        });
        F().B().observe(getViewLifecycleOwner(), new tl2() { // from class: rb2
            @Override // defpackage.tl2
            public final void onChanged(Object obj) {
                MyTokensListFragment.X(MyTokensListFragment.this, (Double) obj);
            }
        });
    }

    public final void Y(View view) {
        Wallet.Companion companion = Wallet.Companion;
        String j = bo3.j(getContext(), "SAFEMOON_ACTIVE_WALLET", "");
        fs1.e(j, "getString(context, Share…FEMOON_ACTIVE_WALLET, \"\")");
        Wallet wallet2 = companion.toWallet(j);
        String displayName = wallet2 == null ? null : wallet2.displayName();
        TextView textView = (TextView) view.findViewById(R.id.tvMainWallet);
        if (textView == null) {
            return;
        }
        textView.setText(displayName);
    }

    @Override // androidx.fragment.app.Fragment
    public View onCreateView(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle) {
        fs1.f(layoutInflater, "inflater");
        hb1 a2 = hb1.a(layoutInflater.inflate(R.layout.fragment_tokenslist, viewGroup, false));
        fs1.e(a2, "bind(\n            inflat…e\n            )\n        )");
        this.i0 = a2;
        if (a2 == null) {
            fs1.r("binding");
            a2 = null;
        }
        ConstraintLayout b2 = a2.b();
        fs1.e(b2, "binding.root");
        return b2;
    }

    @Override // androidx.fragment.app.Fragment
    public void onPause() {
        super.onPause();
        OnSelectTokenTypeClickListener onSelectTokenTypeClickListener = this.o0;
        if (onSelectTokenTypeClickListener == null) {
            return;
        }
        onSelectTokenTypeClickListener.b();
    }

    @Override // net.safemoon.androidwallet.fragments.common.BaseMainFragment, defpackage.qn, androidx.fragment.app.Fragment
    public void onViewCreated(View view, Bundle bundle) {
        fs1.f(view, "view");
        super.onViewCreated(view, bundle);
        R();
        N();
        H();
        K();
        Y(view);
    }
}
