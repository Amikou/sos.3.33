package net.safemoon.androidwallet.fragments;

import android.view.MotionEvent;
import kotlin.jvm.internal.Lambda;

/* compiled from: ReflectionsAdvanceFragment.kt */
/* loaded from: classes2.dex */
public final class ReflectionsAdvanceFragment$onViewCreated$1$8$2 extends Lambda implements tc1<MotionEvent, te4> {
    public final /* synthetic */ ReflectionsAdvanceFragment this$0;

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public ReflectionsAdvanceFragment$onViewCreated$1$8$2(ReflectionsAdvanceFragment reflectionsAdvanceFragment) {
        super(1);
        this.this$0 = reflectionsAdvanceFragment;
    }

    @Override // defpackage.tc1
    public /* bridge */ /* synthetic */ te4 invoke(MotionEvent motionEvent) {
        invoke2(motionEvent);
        return te4.a;
    }

    /* renamed from: invoke  reason: avoid collision after fix types in other method */
    public final void invoke2(MotionEvent motionEvent) {
        if (motionEvent == null) {
            return;
        }
        ReflectionsAdvanceFragment reflectionsAdvanceFragment = this.this$0;
        int actionMasked = motionEvent.getActionMasked();
        if (actionMasked == 0) {
            reflectionsAdvanceFragment.W(false);
        } else if (actionMasked != 1) {
        } else {
            reflectionsAdvanceFragment.W(true);
        }
    }
}
