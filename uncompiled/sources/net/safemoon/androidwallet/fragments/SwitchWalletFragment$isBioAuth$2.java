package net.safemoon.androidwallet.fragments;

import kotlin.jvm.internal.Lambda;

/* compiled from: SwitchWalletFragment.kt */
/* loaded from: classes2.dex */
public final class SwitchWalletFragment$isBioAuth$2 extends Lambda implements rc1<Boolean> {
    public final /* synthetic */ SwitchWalletFragment this$0;

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public SwitchWalletFragment$isBioAuth$2(SwitchWalletFragment switchWalletFragment) {
        super(0);
        this.this$0 = switchWalletFragment;
    }

    /* JADX WARN: Can't rename method to resolve collision */
    @Override // defpackage.rc1
    public final Boolean invoke() {
        return Boolean.valueOf(bo3.e(this.this$0.requireContext(), "TWO_FACTOR", false));
    }
}
