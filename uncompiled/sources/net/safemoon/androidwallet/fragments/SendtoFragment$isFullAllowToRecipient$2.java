package net.safemoon.androidwallet.fragments;

import java.util.Iterator;
import java.util.Locale;
import java.util.Map;
import java.util.Objects;
import kotlin.jvm.internal.Lambda;
import net.safemoon.androidwallet.ui.displayModel.UserTokenItemDisplayModel;

/* compiled from: SendtoFragment.kt */
/* loaded from: classes2.dex */
public final class SendtoFragment$isFullAllowToRecipient$2 extends Lambda implements rc1<Boolean> {
    public final /* synthetic */ SendtoFragment this$0;

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public SendtoFragment$isFullAllowToRecipient$2(SendtoFragment sendtoFragment) {
        super(0);
        this.this$0 = sendtoFragment;
    }

    /* JADX WARN: Can't rename method to resolve collision */
    @Override // defpackage.rc1
    public final Boolean invoke() {
        Map map;
        UserTokenItemDisplayModel userTokenItemDisplayModel;
        String contractAddress;
        map = this.this$0.u0;
        SendtoFragment sendtoFragment = this.this$0;
        boolean z = false;
        if (!map.isEmpty()) {
            Iterator it = map.entrySet().iterator();
            while (true) {
                if (!it.hasNext()) {
                    break;
                }
                String str = (String) ((Map.Entry) it.next()).getKey();
                Objects.requireNonNull(str, "null cannot be cast to non-null type java.lang.String");
                Locale locale = Locale.ROOT;
                String lowerCase = str.toLowerCase(locale);
                fs1.e(lowerCase, "(this as java.lang.Strin….toLowerCase(Locale.ROOT)");
                userTokenItemDisplayModel = sendtoFragment.n0;
                String str2 = null;
                if (userTokenItemDisplayModel != null && (contractAddress = userTokenItemDisplayModel.getContractAddress()) != null) {
                    str2 = contractAddress.toLowerCase(locale);
                    fs1.e(str2, "(this as java.lang.Strin….toLowerCase(Locale.ROOT)");
                }
                if (fs1.b(lowerCase, str2)) {
                    z = true;
                    break;
                }
            }
        }
        return Boolean.valueOf(z);
    }
}
