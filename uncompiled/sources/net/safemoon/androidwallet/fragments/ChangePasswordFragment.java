package net.safemoon.androidwallet.fragments;

import android.content.Context;
import android.graphics.PorterDuff;
import android.os.Bundle;
import android.text.Editable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import androidx.fragment.app.FragmentActivity;
import com.google.android.material.snackbar.Snackbar;
import net.safemoon.androidwallet.R;
import net.safemoon.androidwallet.fragments.ChangePasswordFragment;
import net.safemoon.androidwallet.fragments.common.BaseMainFragment;

/* compiled from: ChangePasswordFragment.kt */
/* loaded from: classes2.dex */
public final class ChangePasswordFragment extends BaseMainFragment {
    public final int i0 = 7;
    public final float j0 = 1.0f;
    public final float k0 = 0.5f;
    public t91 l0;

    /* compiled from: ChangePasswordFragment.kt */
    /* loaded from: classes2.dex */
    public static final class a extends fm2 {
        public a() {
            super(true);
        }

        @Override // defpackage.fm2
        public void b() {
            ChangePasswordFragment.this.f();
        }
    }

    /* compiled from: ChangePasswordFragment.kt */
    /* loaded from: classes2.dex */
    public static final class b extends vu0 {
        public final /* synthetic */ qy1 f0;

        public b(qy1 qy1Var) {
            this.f0 = qy1Var;
        }

        @Override // defpackage.vu0, android.text.TextWatcher
        public void afterTextChanged(Editable editable) {
            super.afterTextChanged(editable);
            t91 t91Var = ChangePasswordFragment.this.l0;
            if (t91Var == null) {
                fs1.r("binding");
                t91Var = null;
            }
            String valueOf = String.valueOf(t91Var.e.getText());
            String valueOf2 = String.valueOf(editable);
            if (valueOf2.length() > 0) {
                if (valueOf.length() > 0) {
                    TextView textView = this.f0.g;
                    fs1.e(textView, "passwordView.tvPassNotMatch");
                    textView.setVisibility(true ^ fs1.b(valueOf2, valueOf) ? 0 : 8);
                    ChangePasswordFragment.this.u(fs1.b(valueOf2, valueOf));
                    if (valueOf2.length() > ChangePasswordFragment.this.i0) {
                        this.f0.d.setTextColor(m70.d(ChangePasswordFragment.this.requireContext(), R.color.btn_light_green));
                        this.f0.a.setColorFilter(m70.d(ChangePasswordFragment.this.requireContext(), R.color.btn_light_green), PorterDuff.Mode.MULTIPLY);
                        this.f0.d.setTag("complete");
                    } else {
                        this.f0.d.setTextColor(m70.d(ChangePasswordFragment.this.requireContext(), R.color.color_edit));
                        this.f0.a.setColorFilter(m70.d(ChangePasswordFragment.this.requireContext(), R.color.color_edit), PorterDuff.Mode.MULTIPLY);
                        this.f0.d.setTag("incomplete");
                    }
                    if (s44.h(valueOf2)) {
                        this.f0.h.setTextColor(m70.d(ChangePasswordFragment.this.requireContext(), R.color.btn_light_green));
                        this.f0.b.setColorFilter(m70.d(ChangePasswordFragment.this.requireContext(), R.color.btn_light_green), PorterDuff.Mode.MULTIPLY);
                        this.f0.h.setTag("complete");
                    } else {
                        this.f0.h.setTextColor(m70.d(ChangePasswordFragment.this.requireContext(), R.color.color_edit));
                        this.f0.b.setColorFilter(m70.d(ChangePasswordFragment.this.requireContext(), R.color.color_edit), PorterDuff.Mode.MULTIPLY);
                        this.f0.h.setTag("incomplete");
                    }
                    if (s44.e(valueOf2)) {
                        this.f0.f.setTextColor(m70.d(ChangePasswordFragment.this.requireContext(), R.color.btn_light_green));
                        this.f0.c.setColorFilter(m70.d(ChangePasswordFragment.this.requireContext(), R.color.btn_light_green), PorterDuff.Mode.MULTIPLY);
                        this.f0.f.setTag("complete");
                        return;
                    }
                    this.f0.f.setTextColor(m70.d(ChangePasswordFragment.this.requireContext(), R.color.color_edit));
                    this.f0.c.setColorFilter(m70.d(ChangePasswordFragment.this.requireContext(), R.color.color_edit), PorterDuff.Mode.MULTIPLY);
                    this.f0.f.setTag("incomplete");
                }
            }
        }
    }

    /* compiled from: ChangePasswordFragment.kt */
    /* loaded from: classes2.dex */
    public static final class c extends vu0 {
        public final /* synthetic */ qy1 f0;

        public c(qy1 qy1Var) {
            this.f0 = qy1Var;
        }

        @Override // defpackage.vu0, android.text.TextWatcher
        public void afterTextChanged(Editable editable) {
            super.afterTextChanged(editable);
            t91 t91Var = ChangePasswordFragment.this.l0;
            if (t91Var == null) {
                fs1.r("binding");
                t91Var = null;
            }
            String valueOf = String.valueOf(t91Var.c.getText());
            if (String.valueOf(editable).length() > 0) {
                if (valueOf.length() > 0) {
                    TextView textView = this.f0.g;
                    fs1.e(textView, "passwordView.tvPassNotMatch");
                    textView.setVisibility(true ^ fs1.b(String.valueOf(editable), valueOf) ? 0 : 8);
                    ChangePasswordFragment.this.u(fs1.b(String.valueOf(editable), valueOf));
                }
            }
        }
    }

    /* compiled from: ChangePasswordFragment.kt */
    /* loaded from: classes2.dex */
    public static final class d extends vu0 {
        public d() {
        }

        @Override // defpackage.vu0, android.text.TextWatcher
        public void afterTextChanged(Editable editable) {
            super.afterTextChanged(editable);
            boolean z = true;
            if (String.valueOf(editable).length() == 0) {
                ChangePasswordFragment.this.u(false);
                return;
            }
            t91 t91Var = ChangePasswordFragment.this.l0;
            t91 t91Var2 = null;
            if (t91Var == null) {
                fs1.r("binding");
                t91Var = null;
            }
            String valueOf = String.valueOf(t91Var.e.getText());
            t91 t91Var3 = ChangePasswordFragment.this.l0;
            if (t91Var3 == null) {
                fs1.r("binding");
            } else {
                t91Var2 = t91Var3;
            }
            String valueOf2 = String.valueOf(t91Var2.c.getText());
            if (!fs1.b(valueOf, valueOf2) || !s44.g(valueOf2)) {
                z = false;
            }
            ChangePasswordFragment.this.u(z);
        }
    }

    public static final void w(ChangePasswordFragment changePasswordFragment, View view) {
        fs1.f(changePasswordFragment, "this$0");
        changePasswordFragment.f();
    }

    public static final void y(ChangePasswordFragment changePasswordFragment, View view) {
        fs1.f(changePasswordFragment, "this$0");
        t91 t91Var = changePasswordFragment.l0;
        t91 t91Var2 = null;
        if (t91Var == null) {
            fs1.r("binding");
            t91Var = null;
        }
        boolean z = !fs1.b(String.valueOf(t91Var.d.getText()), bo3.i(changePasswordFragment.requireContext(), "SAFEMOON_PASSWORD"));
        t91 t91Var3 = changePasswordFragment.l0;
        if (t91Var3 == null) {
            fs1.r("binding");
            t91Var3 = null;
        }
        if (!(String.valueOf(t91Var3.d.getText()).length() == 0) && !z) {
            t91 t91Var4 = changePasswordFragment.l0;
            if (t91Var4 == null) {
                fs1.r("binding");
                t91Var4 = null;
            }
            if (s44.g(String.valueOf(t91Var4.e.getText()))) {
                Context requireContext = changePasswordFragment.requireContext();
                t91 t91Var5 = changePasswordFragment.l0;
                if (t91Var5 == null) {
                    fs1.r("binding");
                    t91Var5 = null;
                }
                bo3.o(requireContext, "SAFEMOON_PASSWORD", String.valueOf(t91Var5.e.getText()));
                t91 t91Var6 = changePasswordFragment.l0;
                if (t91Var6 == null) {
                    fs1.r("binding");
                } else {
                    t91Var2 = t91Var6;
                }
                Snackbar.a0(t91Var2.b(), R.string.screen_change_password_changed, -1).Q();
                changePasswordFragment.f();
                return;
            }
            return;
        }
        FragmentActivity requireActivity = changePasswordFragment.requireActivity();
        fs1.e(requireActivity, "requireActivity()");
        jc0.c(requireActivity, Integer.valueOf((int) R.string.warning_title), R.string.screen_change_password_current_error, true, null);
    }

    @Override // androidx.fragment.app.Fragment
    public View onCreateView(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle) {
        fs1.f(layoutInflater, "inflater");
        return layoutInflater.inflate(R.layout.fragment_change_password, viewGroup, false);
    }

    @Override // net.safemoon.androidwallet.fragments.common.BaseMainFragment, defpackage.qn, androidx.fragment.app.Fragment
    public void onViewCreated(View view, Bundle bundle) {
        fs1.f(view, "view");
        super.onViewCreated(view, bundle);
        t91 a2 = t91.a(view);
        fs1.e(a2, "bind(view)");
        this.l0 = a2;
        v();
        x();
        if (bundle == null) {
            requireActivity().getOnBackPressedDispatcher().a(getViewLifecycleOwner(), new a());
        }
    }

    public final void u(boolean z) {
        t91 t91Var = this.l0;
        t91 t91Var2 = null;
        if (t91Var == null) {
            fs1.r("binding");
            t91Var = null;
        }
        t91Var.b.setEnabled(z);
        t91 t91Var3 = this.l0;
        if (t91Var3 == null) {
            fs1.r("binding");
        } else {
            t91Var2 = t91Var3;
        }
        t91Var2.b.setAlpha(z ? this.j0 : this.k0);
    }

    public final void v() {
        t91 t91Var = this.l0;
        t91 t91Var2 = null;
        if (t91Var == null) {
            fs1.r("binding");
            t91Var = null;
        }
        t91Var.g.a.setOnClickListener(new View.OnClickListener() { // from class: ix
            @Override // android.view.View.OnClickListener
            public final void onClick(View view) {
                ChangePasswordFragment.w(ChangePasswordFragment.this, view);
            }
        });
        t91 t91Var3 = this.l0;
        if (t91Var3 == null) {
            fs1.r("binding");
        } else {
            t91Var2 = t91Var3;
        }
        t91Var2.g.c.setText(R.string.screen_change_password_title);
    }

    public final void x() {
        t91 t91Var = this.l0;
        t91 t91Var2 = null;
        if (t91Var == null) {
            fs1.r("binding");
            t91Var = null;
        }
        qy1 qy1Var = t91Var.f;
        fs1.e(qy1Var, "binding.lPasswordRequirements");
        qy1Var.e.setTextColor(m70.d(requireContext(), R.color.t1));
        u(false);
        t91 t91Var3 = this.l0;
        if (t91Var3 == null) {
            fs1.r("binding");
            t91Var3 = null;
        }
        t91Var3.c.addTextChangedListener(new b(qy1Var));
        t91 t91Var4 = this.l0;
        if (t91Var4 == null) {
            fs1.r("binding");
            t91Var4 = null;
        }
        t91Var4.e.addTextChangedListener(new c(qy1Var));
        t91 t91Var5 = this.l0;
        if (t91Var5 == null) {
            fs1.r("binding");
            t91Var5 = null;
        }
        t91Var5.d.addTextChangedListener(new d());
        t91 t91Var6 = this.l0;
        if (t91Var6 == null) {
            fs1.r("binding");
        } else {
            t91Var2 = t91Var6;
        }
        t91Var2.b.setOnClickListener(new View.OnClickListener() { // from class: jx
            @Override // android.view.View.OnClickListener
            public final void onClick(View view) {
                ChangePasswordFragment.y(ChangePasswordFragment.this, view);
            }
        });
    }
}
