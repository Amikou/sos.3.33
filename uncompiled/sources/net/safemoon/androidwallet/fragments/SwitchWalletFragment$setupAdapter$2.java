package net.safemoon.androidwallet.fragments;

import android.content.Context;
import kotlin.jvm.internal.Lambda;
import net.safemoon.androidwallet.activity.WalletManageActivity;
import net.safemoon.androidwallet.model.wallets.Wallet;

/* compiled from: SwitchWalletFragment.kt */
/* loaded from: classes2.dex */
public final class SwitchWalletFragment$setupAdapter$2 extends Lambda implements tc1<Wallet, te4> {
    public final /* synthetic */ SwitchWalletFragment this$0;

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public SwitchWalletFragment$setupAdapter$2(SwitchWalletFragment switchWalletFragment) {
        super(1);
        this.this$0 = switchWalletFragment;
    }

    @Override // defpackage.tc1
    public /* bridge */ /* synthetic */ te4 invoke(Wallet wallet2) {
        invoke2(wallet2);
        return te4.a;
    }

    /* renamed from: invoke  reason: avoid collision after fix types in other method */
    public final void invoke2(Wallet wallet2) {
        fs1.f(wallet2, "wallet");
        WalletManageActivity.a aVar = WalletManageActivity.B0;
        Context requireContext = this.this$0.requireContext();
        fs1.e(requireContext, "requireContext()");
        aVar.a(requireContext, wallet2);
    }
}
