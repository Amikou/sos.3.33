package net.safemoon.androidwallet.fragments;

import android.annotation.SuppressLint;
import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import androidx.lifecycle.l;
import androidx.recyclerview.widget.LinearLayoutManager;
import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.List;
import net.safemoon.androidwallet.R;
import net.safemoon.androidwallet.dialogs.AnchorSwitchWallet;
import net.safemoon.androidwallet.fragments.SendFragment;
import net.safemoon.androidwallet.fragments.common.BaseMainFragment;
import net.safemoon.androidwallet.model.wallets.Wallet;
import net.safemoon.androidwallet.ui.displayModel.UserTokenItemDisplayModel;
import net.safemoon.androidwallet.viewmodels.MultiWalletViewModel;
import net.safemoon.androidwallet.viewmodels.MyTokensListViewModel;
import net.safemoon.androidwallet.viewmodels.factory.MyViewModelFactory;

/* compiled from: SendFragment.kt */
/* loaded from: classes2.dex */
public final class SendFragment extends BaseMainFragment implements View.OnClickListener {
    public MyTokensListViewModel i0;
    public ya1 j0;
    public final qb2 k0 = new qb2();
    public MultiWalletViewModel l0;

    public static final boolean u(SendFragment sendFragment, View view) {
        fs1.f(sendFragment, "this$0");
        MyTokensListViewModel myTokensListViewModel = sendFragment.i0;
        fs1.d(myTokensListViewModel);
        wb wbVar = new wb(myTokensListViewModel);
        Context requireContext = sendFragment.requireContext();
        fs1.e(requireContext, "requireContext()");
        fs1.d(view);
        ya1 ya1Var = sendFragment.j0;
        fs1.d(ya1Var);
        wbVar.g(requireContext, view, ya1Var.a);
        return true;
    }

    public static final void v(SendFragment sendFragment, View view) {
        fs1.f(sendFragment, "this$0");
        MultiWalletViewModel multiWalletViewModel = sendFragment.l0;
        fs1.d(multiWalletViewModel);
        AnchorSwitchWallet anchorSwitchWallet = new AnchorSwitchWallet(multiWalletViewModel, R.id.sendFragment);
        Context requireContext = sendFragment.requireContext();
        fs1.e(requireContext, "requireContext()");
        fs1.d(view);
        ya1 ya1Var = sendFragment.j0;
        fs1.d(ya1Var);
        anchorSwitchWallet.h(requireContext, view, ya1Var.c);
    }

    public static final void w(SendFragment sendFragment, Double d) {
        fs1.f(sendFragment, "this$0");
        if (d != null) {
            ya1 ya1Var = sendFragment.j0;
            fs1.d(ya1Var);
            TextView textView = ya1Var.g;
            fs1.e(textView, "binding!!.txtSymbol");
            e30.W(textView);
            ya1 ya1Var2 = sendFragment.j0;
            fs1.d(ya1Var2);
            TextView textView2 = ya1Var2.f;
            fs1.e(textView2, "binding!!.tvWalletBlnc");
            e30.N(textView2, d.doubleValue(), false);
        }
    }

    public static final void x(SendFragment sendFragment, List list) {
        fs1.f(sendFragment, "this$0");
        fs1.f(list, "list");
        qb2 qb2Var = sendFragment.k0;
        ArrayList arrayList = new ArrayList();
        for (Object obj : list) {
            if (um1.a(((UserTokenItemDisplayModel) obj).getSymbolWithType()).b()) {
                arrayList.add(obj);
            }
        }
        qb2Var.l(arrayList);
    }

    @Override // android.view.View.OnClickListener
    public void onClick(View view) {
        fs1.f(view, "v");
        if (view.getId() == R.id.iv_back) {
            f();
        }
    }

    @Override // androidx.fragment.app.Fragment
    @SuppressLint({"UseCompatLoadingForDrawables"})
    public View onCreateView(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle) {
        fs1.f(layoutInflater, "inflater");
        View inflate = layoutInflater.inflate(R.layout.fragment_send, viewGroup, false);
        this.i0 = (MyTokensListViewModel) new l(requireActivity(), new MyViewModelFactory(new WeakReference(requireActivity()))).a(MyTokensListViewModel.class);
        this.l0 = (MultiWalletViewModel) new l(this).a(MultiWalletViewModel.class);
        ya1 a = ya1.a(inflate);
        this.j0 = a;
        fs1.d(a);
        a.b.setOnClickListener(this);
        ya1 ya1Var = this.j0;
        fs1.d(ya1Var);
        TextView textView = ya1Var.f;
        fs1.e(textView, "binding!!.tvWalletBlnc");
        e30.X(textView, new SendFragment$onCreateView$1(this));
        ya1 ya1Var2 = this.j0;
        fs1.d(ya1Var2);
        ya1Var2.f.setOnLongClickListener(new View.OnLongClickListener() { // from class: ak3
            @Override // android.view.View.OnLongClickListener
            public final boolean onLongClick(View view) {
                boolean u;
                u = SendFragment.u(SendFragment.this, view);
                return u;
            }
        });
        ya1 ya1Var3 = this.j0;
        fs1.d(ya1Var3);
        ya1Var3.e.setOnClickListener(new View.OnClickListener() { // from class: zj3
            @Override // android.view.View.OnClickListener
            public final void onClick(View view) {
                SendFragment.v(SendFragment.this, view);
            }
        });
        MyTokensListViewModel myTokensListViewModel = this.i0;
        fs1.d(myTokensListViewModel);
        myTokensListViewModel.B().observe(getViewLifecycleOwner(), new tl2() { // from class: xj3
            @Override // defpackage.tl2
            public final void onChanged(Object obj) {
                SendFragment.w(SendFragment.this, (Double) obj);
            }
        });
        return inflate;
    }

    @Override // net.safemoon.androidwallet.fragments.common.BaseMainFragment, defpackage.qn, androidx.fragment.app.Fragment
    public void onViewCreated(View view, Bundle bundle) {
        fs1.f(view, "view");
        super.onViewCreated(view, bundle);
        ya1 ya1Var = this.j0;
        fs1.d(ya1Var);
        ya1Var.d.setLayoutManager(new LinearLayoutManager(getContext(), 1, false));
        ya1 ya1Var2 = this.j0;
        fs1.d(ya1Var2);
        ya1Var2.d.setAdapter(this.k0);
        MyTokensListViewModel myTokensListViewModel = this.i0;
        fs1.d(myTokensListViewModel);
        myTokensListViewModel.A().observe(getViewLifecycleOwner(), new tl2() { // from class: yj3
            @Override // defpackage.tl2
            public final void onChanged(Object obj) {
                SendFragment.x(SendFragment.this, (List) obj);
            }
        });
        this.k0.k(new SendFragment$onViewCreated$2(this));
        Wallet.Companion companion = Wallet.Companion;
        String j = bo3.j(getContext(), "SAFEMOON_ACTIVE_WALLET", "");
        fs1.e(j, "getString(\n            c…FEMOON_ACTIVE_WALLET, \"\")");
        Wallet wallet2 = companion.toWallet(j);
        String displayName = wallet2 == null ? null : wallet2.displayName();
        ya1 ya1Var3 = this.j0;
        fs1.d(ya1Var3);
        ya1Var3.e.setText(displayName);
    }
}
