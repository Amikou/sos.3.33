package net.safemoon.androidwallet.fragments;

import android.content.DialogInterface;
import android.widget.Button;
import androidx.fragment.app.FragmentActivity;
import defpackage.e63;
import kotlin.jvm.internal.Lambda;
import net.safemoon.androidwallet.R;
import net.safemoon.androidwallet.fragments.ReflectionsFragment;
import net.safemoon.androidwallet.fragments.ReflectionsFragment$onViewCreated$6;
import net.safemoon.androidwallet.model.reflections.RoomReflectionsToken;

/* compiled from: ReflectionsFragment.kt */
/* loaded from: classes2.dex */
public final class ReflectionsFragment$onViewCreated$6 extends Lambda implements hd1<RoomReflectionsToken, Button, te4> {
    public final /* synthetic */ ReflectionsFragment this$0;

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public ReflectionsFragment$onViewCreated$6(ReflectionsFragment reflectionsFragment) {
        super(2);
        this.this$0 = reflectionsFragment;
    }

    public static final void b(ReflectionsFragment reflectionsFragment, RoomReflectionsToken roomReflectionsToken, Button button, DialogInterface dialogInterface) {
        fs1.f(reflectionsFragment, "this$0");
        fs1.f(roomReflectionsToken, "$model");
        fs1.f(button, "$button");
        e63.c c = e63.c(roomReflectionsToken.getSymbolWithType());
        fs1.e(c, "actionReflectionsFragmen…                        )");
        reflectionsFragment.g(c);
        button.setEnabled(true);
    }

    @Override // defpackage.hd1
    public /* bridge */ /* synthetic */ te4 invoke(RoomReflectionsToken roomReflectionsToken, Button button) {
        invoke2(roomReflectionsToken, button);
        return te4.a;
    }

    /* renamed from: invoke  reason: avoid collision after fix types in other method */
    public final void invoke2(final RoomReflectionsToken roomReflectionsToken, final Button button) {
        fs1.f(roomReflectionsToken, "model");
        fs1.f(button, "button");
        this.this$0.D().w(roomReflectionsToken);
        if (!roomReflectionsToken.getEnableAdvanceMode()) {
            FragmentActivity requireActivity = this.this$0.requireActivity();
            fs1.e(requireActivity, "requireActivity()");
            Integer valueOf = Integer.valueOf((int) R.string.warning_title);
            final ReflectionsFragment reflectionsFragment = this.this$0;
            jc0.c(requireActivity, valueOf, R.string.reflection_data_loading, false, new DialogInterface.OnDismissListener() { // from class: d63
                @Override // android.content.DialogInterface.OnDismissListener
                public final void onDismiss(DialogInterface dialogInterface) {
                    ReflectionsFragment$onViewCreated$6.b(ReflectionsFragment.this, roomReflectionsToken, button, dialogInterface);
                }
            });
        }
        button.setEnabled(true);
    }
}
