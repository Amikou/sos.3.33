package net.safemoon.androidwallet.fragments;

import java.io.Serializable;
import java.util.Objects;
import kotlin.jvm.internal.Lambda;
import net.safemoon.androidwallet.ui.displayModel.UserTokenItemDisplayModel;

/* compiled from: TransferHistoryFragment.kt */
/* loaded from: classes2.dex */
public final class TransferHistoryFragment$userTokenItemDisplayModel$2 extends Lambda implements rc1<UserTokenItemDisplayModel> {
    public final /* synthetic */ TransferHistoryFragment this$0;

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public TransferHistoryFragment$userTokenItemDisplayModel$2(TransferHistoryFragment transferHistoryFragment) {
        super(0);
        this.this$0 = transferHistoryFragment;
    }

    @Override // defpackage.rc1
    public final UserTokenItemDisplayModel invoke() {
        Serializable serializable = this.this$0.requireArguments().getSerializable("userTokenData");
        Objects.requireNonNull(serializable, "null cannot be cast to non-null type net.safemoon.androidwallet.ui.displayModel.UserTokenItemDisplayModel");
        return (UserTokenItemDisplayModel) serializable;
    }
}
