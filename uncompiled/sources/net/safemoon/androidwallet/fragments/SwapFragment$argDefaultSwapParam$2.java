package net.safemoon.androidwallet.fragments;

import java.io.Serializable;
import kotlin.jvm.internal.Lambda;
import net.safemoon.androidwallet.ui.displayModel.UserTokenItemDisplayModel;

/* compiled from: SwapFragment.kt */
/* loaded from: classes2.dex */
public final class SwapFragment$argDefaultSwapParam$2 extends Lambda implements rc1<UserTokenItemDisplayModel> {
    public final /* synthetic */ SwapFragment this$0;

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public SwapFragment$argDefaultSwapParam$2(SwapFragment swapFragment) {
        super(0);
        this.this$0 = swapFragment;
    }

    @Override // defpackage.rc1
    public final UserTokenItemDisplayModel invoke() {
        try {
            Serializable serializable = this.this$0.requireArguments().getSerializable("userTokenData");
            if (serializable != null) {
                return (UserTokenItemDisplayModel) serializable;
            }
            throw new NullPointerException("null cannot be cast to non-null type net.safemoon.androidwallet.ui.displayModel.UserTokenItemDisplayModel");
        } catch (Exception unused) {
            return null;
        }
    }
}
