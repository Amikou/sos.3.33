package net.safemoon.androidwallet.fragments;

import kotlin.jvm.internal.Lambda;
import net.safemoon.androidwallet.viewmodels.MyTokensListViewModel;

/* compiled from: TransferHistoryFragment.kt */
/* loaded from: classes2.dex */
public final class TransferHistoryFragment$onViewCreated$2 extends Lambda implements rc1<te4> {
    public final /* synthetic */ TransferHistoryFragment this$0;

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public TransferHistoryFragment$onViewCreated$2(TransferHistoryFragment transferHistoryFragment) {
        super(0);
        this.this$0 = transferHistoryFragment;
    }

    @Override // defpackage.rc1
    public /* bridge */ /* synthetic */ te4 invoke() {
        invoke2();
        return te4.a;
    }

    @Override // defpackage.rc1
    /* renamed from: invoke  reason: avoid collision after fix types in other method */
    public final void invoke2() {
        MyTokensListViewModel e0;
        e0 = this.this$0.e0();
        e0.Q();
        this.this$0.F0();
    }
}
