package net.safemoon.androidwallet.fragments;

import android.view.View;
import kotlin.jvm.internal.Lambda;

/* compiled from: SelectCurrencyFragment.kt */
/* loaded from: classes2.dex */
public final class SelectCurrencyFragment$binding$2 extends Lambda implements rc1<xa1> {
    public final /* synthetic */ SelectCurrencyFragment this$0;

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public SelectCurrencyFragment$binding$2(SelectCurrencyFragment selectCurrencyFragment) {
        super(0);
        this.this$0 = selectCurrencyFragment;
    }

    @Override // defpackage.rc1
    public final xa1 invoke() {
        View view = this.this$0.getView();
        if (view == null) {
            return null;
        }
        return xa1.a(view);
    }
}
