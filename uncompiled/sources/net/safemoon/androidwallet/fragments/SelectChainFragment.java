package net.safemoon.androidwallet.fragments;

import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import androidx.fragment.app.FragmentViewModelLazyKt;
import androidx.recyclerview.widget.LinearLayoutManager;
import com.google.android.material.textfield.TextInputEditText;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import kotlin.text.StringsKt__StringsKt;
import net.safemoon.androidwallet.R;
import net.safemoon.androidwallet.common.TokenType;
import net.safemoon.androidwallet.fragments.SelectChainFragment;
import net.safemoon.androidwallet.fragments.common.BaseMainFragment;
import net.safemoon.androidwallet.model.MyTokenType;
import net.safemoon.androidwallet.model.arguments.TokenTypes;

/* compiled from: SelectChainFragment.kt */
/* loaded from: classes2.dex */
public class SelectChainFragment extends BaseMainFragment {
    public wa1 i0;
    public gj3 j0;
    public final sy1 k0 = FragmentViewModelLazyKt.a(this, d53.b(sj3.class), new SelectChainFragment$special$$inlined$viewModels$default$2(new SelectChainFragment$special$$inlined$viewModels$default$1(this)), null);

    /* compiled from: TextView.kt */
    /* loaded from: classes2.dex */
    public static final class a implements TextWatcher {
        public a() {
        }

        @Override // android.text.TextWatcher
        public void afterTextChanged(Editable editable) {
        }

        @Override // android.text.TextWatcher
        public void beforeTextChanged(CharSequence charSequence, int i, int i2, int i3) {
        }

        @Override // android.text.TextWatcher
        public void onTextChanged(CharSequence charSequence, int i, int i2, int i3) {
            String obj;
            gb2<String> i4 = SelectChainFragment.this.r().i();
            String str = null;
            if (charSequence != null && (obj = charSequence.toString()) != null) {
                str = StringsKt__StringsKt.K0(obj).toString();
            }
            i4.postValue(str);
        }
    }

    public static final void t(SelectChainFragment selectChainFragment, View view) {
        fs1.f(selectChainFragment, "this$0");
        selectChainFragment.f();
    }

    public static final void u(SelectChainFragment selectChainFragment, List list) {
        fs1.f(selectChainFragment, "this$0");
        gj3 gj3Var = selectChainFragment.j0;
        if (gj3Var == null) {
            return;
        }
        gj3Var.submitList(list);
    }

    @Override // androidx.fragment.app.Fragment
    public View onCreateView(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle) {
        fs1.f(layoutInflater, "inflater");
        wa1 a2 = wa1.a(layoutInflater.inflate(R.layout.fragment_select_chain, viewGroup, false));
        this.i0 = a2;
        if (a2 == null) {
            return null;
        }
        return a2.b();
    }

    @Override // net.safemoon.androidwallet.fragments.common.BaseMainFragment, defpackage.qn, androidx.fragment.app.Fragment
    public void onViewCreated(View view, Bundle bundle) {
        TokenTypes<TokenType> tokenTypes;
        boolean z;
        boolean z2;
        fs1.f(view, "view");
        super.onViewCreated(view, bundle);
        s();
        if (requireArguments().getSerializable("selectedChain") instanceof TokenTypes) {
            Serializable serializable = requireArguments().getSerializable("selectedChain");
            Objects.requireNonNull(serializable, "null cannot be cast to non-null type net.safemoon.androidwallet.model.arguments.TokenTypes");
            tokenTypes = (TokenTypes) serializable;
        } else {
            tokenTypes = null;
        }
        gb2<List<MyTokenType>> g = r().g();
        List<TokenType> a2 = b30.a.a();
        ArrayList arrayList = new ArrayList(c20.q(a2, 10));
        for (TokenType tokenType : a2) {
            boolean z3 = true;
            if (tokenTypes != null) {
                if (!tokenTypes.isEmpty()) {
                    for (TokenType tokenType2 : tokenTypes) {
                        if (tokenType == tokenType2) {
                            z = true;
                            continue;
                        } else {
                            z = false;
                            continue;
                        }
                        if (z) {
                            z2 = true;
                            break;
                        }
                    }
                }
                z2 = false;
                if (z2) {
                    arrayList.add(new MyTokenType(tokenType, z3));
                }
            }
            z3 = false;
            arrayList.add(new MyTokenType(tokenType, z3));
        }
        g.postValue(j20.m0(arrayList));
        r().h().observe(getViewLifecycleOwner(), new tl2() { // from class: ij3
            @Override // defpackage.tl2
            public final void onChanged(Object obj) {
                SelectChainFragment.u(SelectChainFragment.this, (List) obj);
            }
        });
    }

    public final sj3 r() {
        return (sj3) this.k0.getValue();
    }

    public final void s() {
        xd2 n = ka1.a(this).n();
        ec3 d = n == null ? null : n.d();
        wa1 wa1Var = this.i0;
        if (wa1Var == null) {
            return;
        }
        wa1Var.d.a.setOnClickListener(new View.OnClickListener() { // from class: jj3
            @Override // android.view.View.OnClickListener
            public final void onClick(View view) {
                SelectChainFragment.t(SelectChainFragment.this, view);
            }
        });
        wa1Var.d.c.setText(getText(R.string.select_chain));
        this.j0 = new gj3(new SelectChainFragment$initView$1$2(this, d));
        TextInputEditText textInputEditText = wa1Var.c.b;
        fs1.e(textInputEditText, "it.searchBar.etSearch");
        textInputEditText.addTextChangedListener(new a());
        wa1Var.b.setLayoutManager(new LinearLayoutManager(requireContext(), 1, false));
        wa1Var.b.setAdapter(this.j0);
    }
}
