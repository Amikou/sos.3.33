package net.safemoon.androidwallet.fragments;

import androidx.lifecycle.l;
import java.lang.ref.WeakReference;
import kotlin.jvm.internal.Lambda;

/* compiled from: TokenListFragment.kt */
/* loaded from: classes2.dex */
public final class TokenListFragment$addNewTokensViewModel$2 extends Lambda implements rc1<l.b> {
    public final /* synthetic */ TokenListFragment this$0;

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public TokenListFragment$addNewTokensViewModel$2(TokenListFragment tokenListFragment) {
        super(0);
        this.this$0 = tokenListFragment;
    }

    /* JADX WARN: Can't rename method to resolve collision */
    @Override // defpackage.rc1
    public final l.b invoke() {
        return new u9(new WeakReference(this.this$0.getContext()));
    }
}
