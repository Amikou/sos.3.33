package net.safemoon.androidwallet.fragments;

import kotlin.jvm.internal.Lambda;

/* compiled from: AllTokensListFragment.kt */
/* loaded from: classes2.dex */
public final class AllTokensListFragment$initRecyclerView$3 extends Lambda implements tc1<a30, te4> {
    public final /* synthetic */ AllTokensListFragment this$0;

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public AllTokensListFragment$initRecyclerView$3(AllTokensListFragment allTokensListFragment) {
        super(1);
        this.this$0 = allTokensListFragment;
    }

    @Override // defpackage.tc1
    public /* bridge */ /* synthetic */ te4 invoke(a30 a30Var) {
        invoke2(a30Var);
        return te4.a;
    }

    /* JADX WARN: Removed duplicated region for block: B:19:0x0047  */
    /* renamed from: invoke  reason: avoid collision after fix types in other method */
    /*
        Code decompiled incorrectly, please refer to instructions dump.
        To view partially-correct code enable 'Show inconsistent code' option in preferences
    */
    public final void invoke2(defpackage.a30 r3) {
        /*
            r2 = this;
            java.lang.String r0 = "it"
            defpackage.fs1.f(r3, r0)
            net.safemoon.androidwallet.fragments.AllTokensListFragment r0 = r2.this$0
            q91 r0 = net.safemoon.androidwallet.fragments.AllTokensListFragment.t(r0)
            defpackage.fs1.d(r0)
            androidx.swiperefreshlayout.widget.SwipeRefreshLayout r0 = r0.b
            w02 r1 = r3.e()
            boolean r1 = r1 instanceof defpackage.w02.b
            r0.setRefreshing(r1)
            net.safemoon.androidwallet.fragments.AllTokensListFragment r0 = r2.this$0
            q91 r0 = net.safemoon.androidwallet.fragments.AllTokensListFragment.t(r0)
            if (r0 != 0) goto L23
            r0 = 0
            goto L25
        L23:
            android.widget.TextView r0 = r0.e
        L25:
            if (r0 != 0) goto L28
            goto L4c
        L28:
            w02 r3 = r3.e()
            boolean r3 = r3 instanceof defpackage.w02.b
            r1 = 0
            if (r3 != 0) goto L43
            net.safemoon.androidwallet.fragments.AllTokensListFragment r3 = r2.this$0
            pa0 r3 = net.safemoon.androidwallet.fragments.AllTokensListFragment.u(r3)
            if (r3 != 0) goto L3b
            r3 = r1
            goto L3f
        L3b:
            int r3 = r3.getItemCount()
        L3f:
            if (r3 != 0) goto L43
            r3 = 1
            goto L44
        L43:
            r3 = r1
        L44:
            if (r3 == 0) goto L47
            goto L49
        L47:
            r1 = 8
        L49:
            r0.setVisibility(r1)
        L4c:
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: net.safemoon.androidwallet.fragments.AllTokensListFragment$initRecyclerView$3.invoke2(a30):void");
    }
}
