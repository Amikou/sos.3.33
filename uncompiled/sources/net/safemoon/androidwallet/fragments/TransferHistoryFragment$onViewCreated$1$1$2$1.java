package net.safemoon.androidwallet.fragments;

import android.content.DialogInterface;
import kotlin.jvm.internal.Lambda;
import net.safemoon.androidwallet.model.common.PaymentMethod;
import net.safemoon.androidwallet.ui.displayModel.UserTokenItemDisplayModel;

/* compiled from: TransferHistoryFragment.kt */
/* loaded from: classes2.dex */
public final class TransferHistoryFragment$onViewCreated$1$1$2$1 extends Lambda implements tc1<DialogInterface, te4> {
    public final /* synthetic */ UserTokenItemDisplayModel $it;
    public final /* synthetic */ TransferHistoryFragment this$0;

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public TransferHistoryFragment$onViewCreated$1$1$2$1(TransferHistoryFragment transferHistoryFragment, UserTokenItemDisplayModel userTokenItemDisplayModel) {
        super(1);
        this.this$0 = transferHistoryFragment;
        this.$it = userTokenItemDisplayModel;
    }

    @Override // defpackage.tc1
    public /* bridge */ /* synthetic */ te4 invoke(DialogInterface dialogInterface) {
        invoke2(dialogInterface);
        return te4.a;
    }

    /* renamed from: invoke  reason: avoid collision after fix types in other method */
    public final void invoke2(DialogInterface dialogInterface) {
        fs1.f(dialogInterface, "$noName_0");
        this.this$0.b0(PaymentMethod.MOONPAY, um1.a(this.$it.getSymbolWithType()).c());
    }
}
