package net.safemoon.androidwallet.fragments;

import android.content.ClipData;
import android.content.ClipboardManager;
import android.content.pm.PackageManager;
import android.hardware.fingerprint.FingerprintManager;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.text.method.LinkMovementMethod;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CompoundButton;
import androidx.activity.result.ActivityResult;
import androidx.lifecycle.l;
import com.fasterxml.jackson.core.util.MinimalPrettyPrinter;
import com.google.android.material.switchmaterial.SwitchMaterial;
import defpackage.qm1;
import java.lang.ref.WeakReference;
import net.safemoon.androidwallet.R;
import net.safemoon.androidwallet.activity.AKTChangePasswordActivity;
import net.safemoon.androidwallet.activity.AKTGetEmailActivity;
import net.safemoon.androidwallet.dialogs.G2FAVerfication;
import net.safemoon.androidwallet.dialogs.Google2FAVerfiedAuthFragment;
import net.safemoon.androidwallet.dialogs.GoogleAuthPairFragment;
import net.safemoon.androidwallet.fragments.SecurityFragment;
import net.safemoon.androidwallet.fragments.common.BaseMainFragment;
import net.safemoon.androidwallet.model.wallets.Wallet;
import net.safemoon.androidwallet.provider.RequiredAuthorizeProvider;
import net.safemoon.androidwallet.viewmodels.GoogleAuthViewModel;

/* loaded from: classes2.dex */
public class SecurityFragment extends BaseMainFragment implements fm1, cn1 {
    public va1 i0;
    public GoogleAuthViewModel j0;
    public qm1 k0;
    public int l0 = 1;

    /* loaded from: classes2.dex */
    public class a implements qm1.a {
        public a() {
        }

        @Override // defpackage.qm1.a
        public void a() {
            SecurityFragment.this.d0();
        }

        @Override // defpackage.qm1.a
        public void b(int i) {
        }
    }

    /* loaded from: classes2.dex */
    public class b implements View.OnClickListener {
        public b() {
        }

        /* JADX INFO: Access modifiers changed from: private */
        public /* synthetic */ te4 c(boolean z) {
            SecurityFragment.this.i0.j.setChecked(z);
            bo3.n(SecurityFragment.this.requireContext(), "ASK_AUTH_TRANSACTION_SIGN", Boolean.valueOf(z));
            return null;
        }

        public static /* synthetic */ te4 d() {
            return null;
        }

        @Override // android.view.View.OnClickListener
        public void onClick(View view) {
            final boolean isChecked = SecurityFragment.this.i0.j.isChecked();
            SecurityFragment.this.i0.j.setChecked(!isChecked);
            new RequiredAuthorizeProvider(SecurityFragment.this.requireContext(), SecurityFragment.this.j()).a(new rc1() { // from class: ui3
                @Override // defpackage.rc1
                public final Object invoke() {
                    te4 c;
                    c = SecurityFragment.b.this.c(isChecked);
                    return c;
                }
            }, vi3.a);
        }
    }

    /* loaded from: classes2.dex */
    public class c implements G2FAVerfication.b {
        public final /* synthetic */ int a;

        public c(int i) {
            this.a = i;
        }

        @Override // net.safemoon.androidwallet.dialogs.G2FAVerfication.b
        public void a() {
            y54.a("Khang").a("G2FAVerificationCallback onError() called", new Object[0]);
        }

        @Override // net.safemoon.androidwallet.dialogs.G2FAVerfication.b
        public void onSuccess() {
            switch (this.a) {
                case 1:
                case 2:
                    if (SecurityFragment.this.h0()) {
                        SecurityFragment.this.P0(this.a);
                        return;
                    } else {
                        SecurityFragment.this.E0(this.a);
                        return;
                    }
                case 3:
                    SecurityFragment.this.N0();
                    return;
                case 4:
                    SecurityFragment.this.O0();
                    return;
                case 5:
                    SecurityFragment.this.C0();
                    return;
                case 6:
                    SecurityFragment.this.M0();
                    return;
                default:
                    return;
            }
        }
    }

    /* loaded from: classes2.dex */
    public class d implements qm1.a {
        public final /* synthetic */ boolean a;

        public d(boolean z) {
            this.a = z;
        }

        @Override // defpackage.qm1.a
        public void a() {
            bo3.n(SecurityFragment.this.getActivity(), "TWO_FACTOR", Boolean.valueOf(this.a));
        }

        @Override // defpackage.qm1.a
        public void b(int i) {
            SecurityFragment.this.K0(!this.a);
        }
    }

    /* loaded from: classes2.dex */
    public class e implements Runnable {
        public final /* synthetic */ boolean a;

        public e(boolean z) {
            this.a = z;
        }

        @Override // java.lang.Runnable
        public void run() {
            if (this.a) {
                SecurityFragment.this.E0(1);
            }
        }
    }

    /* loaded from: classes2.dex */
    public class f implements Runnable {
        public final /* synthetic */ boolean a;

        public f(boolean z) {
            this.a = z;
        }

        @Override // java.lang.Runnable
        public void run() {
            if (this.a) {
                SecurityFragment.this.E0(2);
            } else {
                SecurityFragment.this.i0.s.setVisibility(8);
            }
        }
    }

    /* loaded from: classes2.dex */
    public class g implements qm1.a {
        public g() {
        }

        @Override // defpackage.qm1.a
        public void a() {
            SecurityFragment.this.L0();
        }

        @Override // defpackage.qm1.a
        public void b(int i) {
        }
    }

    /* loaded from: classes2.dex */
    public class h implements qm1.a {
        public h() {
        }

        @Override // defpackage.qm1.a
        public void a() {
            SecurityFragment.this.J0(true);
        }

        @Override // defpackage.qm1.a
        public void b(int i) {
        }
    }

    /* loaded from: classes2.dex */
    public class i implements qm1.a {
        public i() {
        }

        @Override // defpackage.qm1.a
        public void a() {
            SecurityFragment.this.H0(true);
        }

        @Override // defpackage.qm1.a
        public void b(int i) {
        }
    }

    /* loaded from: classes2.dex */
    public class j implements qm1.a {
        public j() {
        }

        @Override // defpackage.qm1.a
        public void a() {
            SecurityFragment.this.F0();
        }

        @Override // defpackage.qm1.a
        public void b(int i) {
        }
    }

    /* JADX INFO: Access modifiers changed from: private */
    public /* synthetic */ te4 A0() {
        boolean z = !this.j0.e().getValue().booleanValue();
        bo3.p(requireContext(), "AUTH_2FA_IS_ENABLE", z);
        this.j0.e().setValue(Boolean.valueOf(z));
        return null;
    }

    public static /* synthetic */ te4 B0() {
        return null;
    }

    /* JADX INFO: Access modifiers changed from: private */
    public /* synthetic */ void i0(CompoundButton compoundButton, boolean z) {
        if (getActivity() != null) {
            D0(z);
        }
    }

    /* JADX INFO: Access modifiers changed from: private */
    public /* synthetic */ void j0(Boolean bool) {
        this.i0.b.setVisibility(bool.booleanValue() ? 0 : 8);
        this.i0.o.setVisibility(bool.booleanValue() ? 8 : 0);
        this.i0.i.setChecked(bool.booleanValue());
    }

    /* JADX INFO: Access modifiers changed from: private */
    public /* synthetic */ void k0(View view) {
        ((ClipboardManager) getActivity().getSystemService("clipboard")).setPrimaryClip(ClipData.newPlainText("label", this.i0.s.getText().toString()));
        e30.Z(requireActivity(), R.string.copied_to_clipboard);
    }

    /* JADX INFO: Access modifiers changed from: private */
    public /* synthetic */ void l0(View view) {
        f();
    }

    /* JADX INFO: Access modifiers changed from: private */
    public /* synthetic */ void m0(View view) {
        if (bo3.d(requireContext(), "AUTH_2FA_IS_ENABLE")) {
            G2FAVerfication.C0.a(Z(3), h0()).H(getChildFragmentManager());
        } else if (h0()) {
            this.k0.a(this, Y());
        } else {
            F0();
        }
    }

    /* JADX INFO: Access modifiers changed from: private */
    public /* synthetic */ void n0(View view) {
        if (bo3.d(requireContext(), "AUTH_2FA_IS_ENABLE")) {
            G2FAVerfication.C0.a(Z(6), h0()).H(getChildFragmentManager());
        } else if (h0()) {
            this.k0.a(this, X());
        } else {
            d0();
        }
    }

    /* JADX INFO: Access modifiers changed from: private */
    public /* synthetic */ void o0(CompoundButton compoundButton, boolean z) {
        if (this.j0.e().getValue().booleanValue() != z) {
            this.j0.e().setValue(this.j0.e().getValue());
        }
    }

    /* JADX INFO: Access modifiers changed from: private */
    public /* synthetic */ te4 p0(String str) {
        if (str.isEmpty()) {
            GoogleAuthPairFragment.x0.a().P(getChildFragmentManager());
            return null;
        }
        boolean z = !this.j0.e().getValue().booleanValue();
        bo3.p(requireContext(), "AUTH_2FA_IS_ENABLE", z);
        this.j0.e().setValue(Boolean.valueOf(z));
        return null;
    }

    public static /* synthetic */ te4 q0() {
        return null;
    }

    /* JADX INFO: Access modifiers changed from: private */
    public /* synthetic */ void r0(View view) {
        final String j2 = bo3.j(requireContext(), "AUTH_2FA_KEY", "");
        boolean booleanValue = this.j0.e().getValue().booleanValue();
        if (!j2.isEmpty() && booleanValue) {
            G2FAVerfication.C0.a(Z(4), false).H(getChildFragmentManager());
        } else {
            new RequiredAuthorizeProvider(requireContext(), j()).a(new rc1() { // from class: ni3
                @Override // defpackage.rc1
                public final Object invoke() {
                    te4 p0;
                    p0 = SecurityFragment.this.p0(j2);
                    return p0;
                }
            }, oi3.a);
        }
    }

    /* JADX INFO: Access modifiers changed from: private */
    public /* synthetic */ void s0(View view) {
        G2FAVerfication.C0.a(Z(5), h0()).H(getChildFragmentManager());
    }

    /* JADX INFO: Access modifiers changed from: private */
    public /* synthetic */ void t0(View view) {
        if (bo3.d(requireContext(), "AUTH_2FA_IS_ENABLE")) {
            G2FAVerfication.C0.a(Z(1), h0()).H(getChildFragmentManager());
        } else if (this.i0.q.getTag().toString().equals("off")) {
            if (h0()) {
                this.k0.a(this, a0());
            } else {
                E0(1);
            }
        }
    }

    /* JADX INFO: Access modifiers changed from: private */
    public /* synthetic */ void u0(View view) {
        if (bo3.d(requireContext(), "AUTH_2FA_IS_ENABLE")) {
            G2FAVerfication.C0.a(Z(2), h0()).H(getChildFragmentManager());
        } else if (this.i0.u.getTag().toString().equals("off")) {
            if (h0()) {
                this.k0.a(this, b0());
            } else {
                E0(2);
            }
        }
    }

    /* JADX INFO: Access modifiers changed from: private */
    public /* synthetic */ void v0(View view) {
        ((ClipboardManager) getActivity().getSystemService("clipboard")).setPrimaryClip(ClipData.newPlainText("label", this.i0.r.getText().toString()));
        e30.Z(requireActivity(), R.string.copied_to_clipboard);
    }

    /* JADX INFO: Access modifiers changed from: private */
    public /* synthetic */ void w0(ActivityResult activityResult) {
        if (activityResult.b() == -1) {
            int i2 = this.l0;
            if (i2 == 3) {
                F0();
            } else {
                E0(i2);
            }
        }
    }

    /* JADX INFO: Access modifiers changed from: private */
    public /* synthetic */ void x0(int i2, View view) {
        if (i2 == 1) {
            I0();
        } else {
            G0();
        }
    }

    /* JADX INFO: Access modifiers changed from: private */
    public /* synthetic */ void y0(int i2, View view) {
        if (i2 == 1) {
            e0();
        } else {
            f0();
        }
    }

    /* JADX INFO: Access modifiers changed from: private */
    public /* synthetic */ te4 z0() {
        AKTChangePasswordActivity.o0.a(getActivity());
        return null;
    }

    public final void C0() {
        if (h0()) {
            this.k0.a(this, new g());
        } else {
            L0();
        }
    }

    public final void D0(boolean z) {
        this.k0.a(this, new d(z));
    }

    public final void E0(final int i2) {
        bh.F(new WeakReference(requireActivity()), new View.OnClickListener() { // from class: hi3
            @Override // android.view.View.OnClickListener
            public final void onClick(View view) {
                SecurityFragment.this.x0(i2, view);
            }
        }, new View.OnClickListener() { // from class: ii3
            @Override // android.view.View.OnClickListener
            public final void onClick(View view) {
                SecurityFragment.this.y0(i2, view);
            }
        });
    }

    public final void F0() {
        bh.X(new WeakReference(getActivity()), null, Integer.valueOf((int) R.string.akt_reset_password_dialog_remind_content), R.string.acknowledgment_confirm_button_text, new rc1() { // from class: li3
            @Override // defpackage.rc1
            public final Object invoke() {
                te4 z0;
                z0 = SecurityFragment.this.z0();
                return z0;
            }
        });
    }

    public final void G0() {
        this.i0.u.setVisibility(8);
        this.i0.g.setVisibility(0);
        this.i0.s.setVisibility(0);
        this.i0.s.setText(w.c(getActivity()));
        this.i0.m.setVisibility(8);
    }

    public final void H0(boolean z) {
        new Handler(Looper.getMainLooper()).post(new f(z));
    }

    public final void I0() {
        Wallet c2;
        this.i0.q.setVisibility(8);
        this.i0.l.setVisibility(8);
        this.i0.h.setVisibility(0);
        this.i0.r.setVisibility(0);
        if (getActivity() == null || (c2 = e30.c(getActivity())) == null || c2.getPassPhrase() == null) {
            return;
        }
        this.i0.r.setText(w.e(getActivity(), bo3.i(getActivity(), "SAFEMOON_RECOVERY_PHRASE")).replace("|", MinimalPrettyPrinter.DEFAULT_ROOT_VALUE_SEPARATOR).trim());
    }

    public final void J0(boolean z) {
        new Handler(Looper.getMainLooper()).post(new e(z));
    }

    public final void K0(boolean z) {
        this.i0.k.setOnCheckedChangeListener(null);
        this.i0.k.setChecked(z);
        this.i0.k.setOnCheckedChangeListener(c0());
    }

    public final void L0() {
        bo3.k(requireContext(), "AUTH_2FA_KEY");
        bo3.k(requireContext(), "AUTH_2FA_IS_ENABLE");
        this.j0.e().setValue(Boolean.FALSE);
    }

    public final void M0() {
        if (h0()) {
            this.k0.a(this, X());
        } else {
            d0();
        }
    }

    public final void N0() {
        if (h0()) {
            this.k0.a(this, Y());
        } else {
            F0();
        }
    }

    public final void O0() {
        new RequiredAuthorizeProvider(requireContext(), j()).a(new rc1() { // from class: mi3
            @Override // defpackage.rc1
            public final Object invoke() {
                te4 A0;
                A0 = SecurityFragment.this.A0();
                return A0;
            }
        }, pi3.a);
    }

    public final void P0(int i2) {
        if (i2 == 1) {
            this.k0.a(this, a0());
        } else {
            this.k0.a(this, b0());
        }
    }

    public final qm1.a X() {
        return new a();
    }

    public final qm1.a Y() {
        return new j();
    }

    public final G2FAVerfication.b Z(int i2) {
        return new c(i2);
    }

    @Override // defpackage.fm1
    public void a() {
        Google2FAVerfiedAuthFragment.y0.a().H(getChildFragmentManager());
    }

    public final qm1.a a0() {
        return new h();
    }

    @Override // defpackage.cn1
    public void b() {
    }

    public final qm1.a b0() {
        return new i();
    }

    public final CompoundButton.OnCheckedChangeListener c0() {
        return new CompoundButton.OnCheckedChangeListener() { // from class: ki3
            @Override // android.widget.CompoundButton.OnCheckedChangeListener
            public final void onCheckedChanged(CompoundButton compoundButton, boolean z) {
                SecurityFragment.this.i0(compoundButton, z);
            }
        };
    }

    public final void d0() {
        AKTGetEmailActivity.s0.a(getActivity(), false, false, true);
    }

    public final void e0() {
        this.i0.r.setVisibility(8);
        this.i0.p.setVisibility(8);
        this.i0.h.setVisibility(8);
        this.i0.l.setVisibility(0);
        this.i0.q.setVisibility(0);
        this.i0.q.setTag("off");
    }

    public final void f0() {
        this.i0.s.setVisibility(8);
        this.i0.g.setVisibility(8);
        this.i0.u.setVisibility(0);
        this.i0.u.setTag("off");
        this.i0.m.setVisibility(0);
    }

    public final void g0() {
        if (getActivity() != null) {
            this.i0.k.setChecked(bo3.e(getActivity(), "TWO_FACTOR", false));
            this.i0.j.setChecked(bo3.e(requireActivity(), "ASK_AUTH_TRANSACTION_SIGN", true));
        }
        this.i0.k.setOnCheckedChangeListener(c0());
        this.j0.e().observe(getViewLifecycleOwner(), new tl2() { // from class: qi3
            @Override // defpackage.tl2
            public final void onChanged(Object obj) {
                SecurityFragment.this.j0((Boolean) obj);
            }
        });
        this.i0.i.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() { // from class: ji3
            @Override // android.widget.CompoundButton.OnCheckedChangeListener
            public final void onCheckedChanged(CompoundButton compoundButton, boolean z) {
                SecurityFragment.this.o0(compoundButton, z);
            }
        });
        this.i0.i.setOnClickListener(new View.OnClickListener() { // from class: fi3
            @Override // android.view.View.OnClickListener
            public final void onClick(View view) {
                SecurityFragment.this.r0(view);
            }
        });
        this.i0.j.setOnClickListener(new b());
        this.i0.b.setOnClickListener(new View.OnClickListener() { // from class: bi3
            @Override // android.view.View.OnClickListener
            public final void onClick(View view) {
                SecurityFragment.this.s0(view);
            }
        });
        this.i0.c.setVisibility(e30.m0(!bo3.j(getActivity(), "SAFEMOON_RECOVERY_PHRASE", "").isEmpty()));
        this.i0.q.setOnClickListener(new View.OnClickListener() { // from class: ti3
            @Override // android.view.View.OnClickListener
            public final void onClick(View view) {
                SecurityFragment.this.t0(view);
            }
        });
        this.i0.u.setOnClickListener(new View.OnClickListener() { // from class: ei3
            @Override // android.view.View.OnClickListener
            public final void onClick(View view) {
                SecurityFragment.this.u0(view);
            }
        });
        this.i0.p.setOnClickListener(new View.OnClickListener() { // from class: di3
            @Override // android.view.View.OnClickListener
            public final void onClick(View view) {
                SecurityFragment.this.v0(view);
            }
        });
        this.i0.t.setOnClickListener(new View.OnClickListener() { // from class: si3
            @Override // android.view.View.OnClickListener
            public final void onClick(View view) {
                SecurityFragment.this.k0(view);
            }
        });
        this.i0.n.a.setOnClickListener(new View.OnClickListener() { // from class: ci3
            @Override // android.view.View.OnClickListener
            public final void onClick(View view) {
                SecurityFragment.this.l0(view);
            }
        });
        this.i0.n.c.setText(getText(R.string.security));
        this.i0.f.setOnClickListener(new View.OnClickListener() { // from class: ri3
            @Override // android.view.View.OnClickListener
            public final void onClick(View view) {
                SecurityFragment.this.m0(view);
            }
        });
        this.i0.e.setOnClickListener(new View.OnClickListener() { // from class: gi3
            @Override // android.view.View.OnClickListener
            public final void onClick(View view) {
                SecurityFragment.this.n0(view);
            }
        });
    }

    public final boolean h0() {
        return bo3.e(requireContext(), "TWO_FACTOR", false);
    }

    @Override // androidx.fragment.app.Fragment
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        registerForActivityResult(new v7(), new r7() { // from class: ai3
            @Override // defpackage.r7
            public final void a(Object obj) {
                SecurityFragment.this.w0((ActivityResult) obj);
            }
        });
    }

    @Override // androidx.fragment.app.Fragment
    public View onCreateView(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle) {
        this.i0 = va1.a(layoutInflater.inflate(R.layout.fragment_security, viewGroup, false));
        this.j0 = (GoogleAuthViewModel) new l(requireActivity()).a(GoogleAuthViewModel.class);
        this.k0 = new w41();
        return this.i0.b();
    }

    @Override // androidx.fragment.app.Fragment
    public void onPause() {
        super.onPause();
        if (zr.a.booleanValue()) {
            return;
        }
        requireActivity().getWindow().clearFlags(8192);
    }

    @Override // androidx.fragment.app.Fragment
    public void onResume() {
        super.onResume();
        if (!zr.a.booleanValue()) {
            requireActivity().getWindow().addFlags(8192);
        }
        f0();
        e0();
    }

    @Override // androidx.fragment.app.Fragment
    public void onStart() {
        super.onStart();
        if (zr.a.booleanValue()) {
            return;
        }
        requireActivity().getWindow().addFlags(8192);
    }

    @Override // androidx.fragment.app.Fragment
    public void onStop() {
        super.onStop();
        if (zr.a.booleanValue()) {
            return;
        }
        requireActivity().getWindow().clearFlags(8192);
    }

    @Override // net.safemoon.androidwallet.fragments.common.BaseMainFragment, defpackage.qn, androidx.fragment.app.Fragment
    public void onViewCreated(View view, Bundle bundle) {
        super.onViewCreated(view, bundle);
        g0();
        this.i0.d.setVisibility(pg4.d(requireContext()) ? 0 : 8);
        this.i0.o.setMovementMethod(LinkMovementMethod.getInstance());
        PackageManager packageManager = getActivity().getPackageManager();
        SwitchMaterial switchMaterial = (SwitchMaterial) this.i0.d.findViewById(R.id.switchTwoLayerConfirmation);
        if (packageManager.hasSystemFeature("android.hardware.fingerprint")) {
            if (((FingerprintManager) getContext().getSystemService("fingerprint")).hasEnrolledFingerprints()) {
                switchMaterial.setText(getResources().getString(R.string.require_fingerprint_or_device_passcode));
            }
        } else if (packageManager.hasSystemFeature("android.hardware.biometrics.face")) {
            switchMaterial.setText(getResources().getString(R.string.require_face_id_or_device_passcode));
        }
    }
}
