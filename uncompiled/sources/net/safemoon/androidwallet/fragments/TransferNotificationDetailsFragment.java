package net.safemoon.androidwallet.fragments;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.fragment.app.FragmentViewModelLazyKt;
import java.util.Arrays;
import java.util.Date;
import java.util.Locale;
import java.util.Objects;
import net.safemoon.androidwallet.R;
import net.safemoon.androidwallet.common.TokenType;
import net.safemoon.androidwallet.fragments.TransferNotificationDetailsFragment;
import net.safemoon.androidwallet.fragments.common.BaseMainFragment;
import net.safemoon.androidwallet.model.ReceiptStatus;
import net.safemoon.androidwallet.model.transaction.details.TransactionDetailsData;
import net.safemoon.androidwallet.viewmodels.TransferViewModel;
import org.web3j.abi.datatypes.Address;

/* compiled from: TransferNotificationDetailsFragment.kt */
/* loaded from: classes2.dex */
public final class TransferNotificationDetailsFragment extends BaseMainFragment {
    public final String i0 = "0";
    public final String j0 = "1";
    public final sy1 k0 = zy1.a(new TransferNotificationDetailsFragment$address$2(this));
    public final sy1 l0 = zy1.a(new TransferNotificationDetailsFragment$newTransaction$2(this));
    public final sy1 m0 = FragmentViewModelLazyKt.a(this, d53.b(TransferViewModel.class), new TransferNotificationDetailsFragment$special$$inlined$activityViewModels$default$1(this), new TransferNotificationDetailsFragment$special$$inlined$activityViewModels$default$2(this));
    public ib1 n0;
    public ua4 o0;

    /* compiled from: TransferNotificationDetailsFragment.kt */
    /* loaded from: classes2.dex */
    public static final class a {
        public a() {
        }

        public /* synthetic */ a(qi0 qi0Var) {
            this();
        }
    }

    static {
        new a(null);
    }

    public static final void B(TransferNotificationDetailsFragment transferNotificationDetailsFragment, String str, TransactionDetailsData transactionDetailsData, View view) {
        fs1.f(transferNotificationDetailsFragment, "this$0");
        fs1.f(str, "$link");
        fs1.f(transactionDetailsData, "$details");
        transferNotificationDetailsFragment.z(false);
        transferNotificationDetailsFragment.startActivity(new Intent("android.intent.action.VIEW", Uri.parse(fs1.l(str, transactionDetailsData.getHash()))));
    }

    public static final void C(TransferNotificationDetailsFragment transferNotificationDetailsFragment, View view) {
        fs1.f(transferNotificationDetailsFragment, "this$0");
        transferNotificationDetailsFragment.f();
    }

    public static final void w(TransferNotificationDetailsFragment transferNotificationDetailsFragment, TransactionDetailsData transactionDetailsData) {
        fs1.f(transferNotificationDetailsFragment, "this$0");
        if (transactionDetailsData == null) {
            return;
        }
        ib1 ib1Var = transferNotificationDetailsFragment.n0;
        fs1.d(ib1Var);
        ib1Var.c.setVisibility(0);
        ib1 ib1Var2 = transferNotificationDetailsFragment.n0;
        fs1.d(ib1Var2);
        ib1Var2.e.setVisibility(8);
        transferNotificationDetailsFragment.A(transactionDetailsData);
        transferNotificationDetailsFragment.u().i(transactionDetailsData.getHash(), TokenType.Companion.c(transactionDetailsData.getChain()));
    }

    public static final void x(TransferNotificationDetailsFragment transferNotificationDetailsFragment, ReceiptStatus receiptStatus) {
        fs1.f(transferNotificationDetailsFragment, "this$0");
        if (transferNotificationDetailsFragment.t()) {
            String str = receiptStatus == null ? "" : receiptStatus.result.status;
            fs1.e(str, "if(it == null) \"\" else it.result.status");
            transferNotificationDetailsFragment.y(str);
        }
    }

    public final void A(final TransactionDetailsData transactionDetailsData) {
        String string;
        if (getActivity() != null) {
            if (!t()) {
                y(transactionDetailsData.getStatus() ? this.j0 : this.i0);
            }
            String s = s();
            fs1.e(s, Address.TYPE_NAME);
            Locale locale = Locale.ROOT;
            fs1.e(locale, "ROOT");
            String lowerCase = s.toLowerCase(locale);
            fs1.e(lowerCase, "(this as java.lang.String).toLowerCase(locale)");
            String to = transactionDetailsData.getTo();
            fs1.e(locale, "ROOT");
            Objects.requireNonNull(to, "null cannot be cast to non-null type java.lang.String");
            String lowerCase2 = to.toLowerCase(locale);
            fs1.e(lowerCase2, "(this as java.lang.String).toLowerCase(locale)");
            if (fs1.b(lowerCase, lowerCase2)) {
                string = getString(R.string.tx_received);
                fs1.e(string, "getString(R.string.tx_received)");
                ib1 ib1Var = this.n0;
                fs1.d(ib1Var);
                TextView textView = ib1Var.k;
                textView.setText(getString(R.string.tx_from) + ' ' + transactionDetailsData.getFrom());
            } else {
                string = getString(R.string.tx_sent);
                fs1.e(string, "getString(R.string.tx_sent)");
                ib1 ib1Var2 = this.n0;
                fs1.d(ib1Var2);
                TextView textView2 = ib1Var2.k;
                textView2.setText(getString(R.string.tx_to) + ' ' + transactionDetailsData.getTo());
            }
            ib1 ib1Var3 = this.n0;
            fs1.d(ib1Var3);
            ib1Var3.j.setText(string);
            ib1 ib1Var4 = this.n0;
            fs1.d(ib1Var4);
            TextView textView3 = ib1Var4.l;
            textView3.setText(getString(R.string.tx_total) + ' ' + e30.p(transactionDetailsData.getAmount(), 0, null, false, 6, null) + ' ' + transactionDetailsData.getTokenSymbol());
            TokenType c = TokenType.Companion.c(transactionDetailsData.getChain());
            ib1 ib1Var5 = this.n0;
            fs1.d(ib1Var5);
            TextView textView4 = ib1Var5.i;
            textView4.setText(getString(R.string.tx_network_fee) + ' ' + transactionDetailsData.getGasUsed() + ' ' + c.getNativeToken());
            b30 b30Var = b30.a;
            Date w = b30Var.w(transactionDetailsData.getCreatedAt());
            if (w != null) {
                ib1 ib1Var6 = this.n0;
                fs1.d(ib1Var6);
                TextView textView5 = ib1Var6.h;
                Context requireContext = requireContext();
                fs1.e(requireContext, "requireContext()");
                textView5.setText(b30Var.d(w, requireContext));
            }
            final String o = b30Var.o(c);
            ib1 ib1Var7 = this.n0;
            fs1.d(ib1Var7);
            TextView textView6 = ib1Var7.g;
            lu3 lu3Var = lu3.a;
            Locale locale2 = Locale.getDefault();
            String string2 = getString(R.string.view_transaction_on_bsc);
            fs1.e(string2, "getString(R.string.view_transaction_on_bsc)");
            String format = String.format(locale2, string2, Arrays.copyOf(new Object[]{c.getPlaneName()}, 1));
            fs1.e(format, "java.lang.String.format(locale, format, *args)");
            textView6.setText(format);
            ib1 ib1Var8 = this.n0;
            fs1.d(ib1Var8);
            ib1Var8.g.setOnClickListener(new View.OnClickListener() { // from class: ta4
                @Override // android.view.View.OnClickListener
                public final void onClick(View view) {
                    TransferNotificationDetailsFragment.B(TransferNotificationDetailsFragment.this, o, transactionDetailsData, view);
                }
            });
            ib1 ib1Var9 = this.n0;
            fs1.d(ib1Var9);
            ib1Var9.f.a.setOnClickListener(new View.OnClickListener() { // from class: sa4
                @Override // android.view.View.OnClickListener
                public final void onClick(View view) {
                    TransferNotificationDetailsFragment.C(TransferNotificationDetailsFragment.this, view);
                }
            });
        }
    }

    @Override // androidx.fragment.app.Fragment
    public View onCreateView(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle) {
        fs1.f(layoutInflater, "inflater");
        this.n0 = ib1.a(layoutInflater.inflate(R.layout.fragment_transaction_notification_details, viewGroup, false));
        ua4 ua4Var = (ua4) new v84().a(ua4.class);
        this.o0 = ua4Var;
        fs1.d(ua4Var);
        String string = requireArguments().getString("transactionHash");
        fs1.d(string);
        fs1.e(string, "requireArguments().getSt…EXTRA_TRANSACTION_HASH)!!");
        ua4Var.a(string);
        ib1 ib1Var = this.n0;
        fs1.d(ib1Var);
        ib1Var.f.c.setText(getText(R.string.Transfer_detail));
        v();
        ib1 ib1Var2 = this.n0;
        fs1.d(ib1Var2);
        ConstraintLayout b = ib1Var2.b();
        fs1.e(b, "mBinding!!.root");
        return b;
    }

    @Override // androidx.fragment.app.Fragment
    public void onResume() {
        super.onResume();
        z(true);
    }

    @Override // net.safemoon.androidwallet.fragments.common.BaseMainFragment, defpackage.qn, androidx.fragment.app.Fragment
    public void onViewCreated(View view, Bundle bundle) {
        fs1.f(view, "view");
        super.onViewCreated(view, bundle);
        u().g().postValue(null);
        u().g().observe(getViewLifecycleOwner(), new tl2() { // from class: qa4
            @Override // defpackage.tl2
            public final void onChanged(Object obj) {
                TransferNotificationDetailsFragment.x(TransferNotificationDetailsFragment.this, (ReceiptStatus) obj);
            }
        });
    }

    public final String s() {
        return (String) this.k0.getValue();
    }

    public final boolean t() {
        return ((Boolean) this.l0.getValue()).booleanValue();
    }

    public final TransferViewModel u() {
        return (TransferViewModel) this.m0.getValue();
    }

    @SuppressLint({"SetTextI18n"})
    public final void v() {
        ua4 ua4Var = this.o0;
        fs1.d(ua4Var);
        ua4Var.b().observe(getViewLifecycleOwner(), new tl2() { // from class: ra4
            @Override // defpackage.tl2
            public final void onChanged(Object obj) {
                TransferNotificationDetailsFragment.w(TransferNotificationDetailsFragment.this, (TransactionDetailsData) obj);
            }
        });
    }

    public final void y(String str) {
        ib1 ib1Var = this.n0;
        if (ib1Var == null) {
            return;
        }
        if (fs1.b(str, this.j0)) {
            ib1Var.d.setColorFilter(m70.d(requireContext(), R.color.green));
            ib1Var.m.setText(R.string.transfer_confirmed);
        } else if (fs1.b(str, this.i0)) {
            ib1Var.d.setColorFilter(m70.d(requireContext(), R.color.red));
            ib1Var.m.setText(R.string.transfer_failed);
        } else {
            ib1Var.d.setColorFilter(m70.d(requireContext(), R.color.yellow));
            ib1Var.m.setText(R.string.transfer_pending);
        }
    }

    public final void z(boolean z) {
        ib1 ib1Var = this.n0;
        if (ib1Var != null) {
            fs1.d(ib1Var);
            ib1Var.g.setClickable(z);
        }
    }
}
