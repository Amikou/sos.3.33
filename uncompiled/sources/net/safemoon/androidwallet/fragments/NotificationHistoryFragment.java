package net.safemoon.androidwallet.fragments;

import android.content.DialogInterface;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import androidx.lifecycle.l;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;
import defpackage.uh2;
import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.List;
import net.safemoon.androidwallet.R;
import net.safemoon.androidwallet.fragments.NotificationHistoryFragment;
import net.safemoon.androidwallet.fragments.common.BaseMainFragment;
import net.safemoon.androidwallet.model.notificationHistory.NotificationHistory;
import net.safemoon.androidwallet.model.notificationHistory.NotificationHistoryResult;

/* loaded from: classes2.dex */
public class NotificationHistoryFragment extends BaseMainFragment {
    public static boolean m0 = false;
    public List<String> i0 = new ArrayList();
    public pa1 j0;
    public uh2 k0;
    public qi2 l0;

    /* loaded from: classes2.dex */
    public class a implements View.OnClickListener {
        public a() {
            NotificationHistoryFragment.this = r1;
        }

        @Override // android.view.View.OnClickListener
        public void onClick(View view) {
            NotificationHistoryFragment.this.s0();
        }
    }

    /* loaded from: classes2.dex */
    public class b implements View.OnClickListener {
        public b() {
            NotificationHistoryFragment.this = r1;
        }

        @Override // android.view.View.OnClickListener
        public void onClick(View view) {
            WeakReference weakReference = new WeakReference(NotificationHistoryFragment.this.getContext());
            Integer valueOf = Integer.valueOf((int) R.string.confirm);
            Integer valueOf2 = Integer.valueOf((int) R.string.mark_as_read_confirm);
            Integer valueOf3 = Integer.valueOf((int) R.string.mark);
            Integer valueOf4 = Integer.valueOf((int) R.string.cancel);
            final NotificationHistoryFragment notificationHistoryFragment = NotificationHistoryFragment.this;
            tc1 tc1Var = new tc1() { // from class: hi2
                @Override // defpackage.tc1
                public final Object invoke(Object obj) {
                    te4 C;
                    C = NotificationHistoryFragment.C(NotificationHistoryFragment.this, (DialogInterface) obj);
                    return C;
                }
            };
            final NotificationHistoryFragment notificationHistoryFragment2 = NotificationHistoryFragment.this;
            bh.P(weakReference, valueOf, valueOf2, null, valueOf3, valueOf4, null, null, tc1Var, new tc1() { // from class: ii2
                @Override // defpackage.tc1
                public final Object invoke(Object obj) {
                    te4 B;
                    B = NotificationHistoryFragment.B(NotificationHistoryFragment.this, (DialogInterface) obj);
                    return B;
                }
            });
        }
    }

    /* loaded from: classes2.dex */
    public class c implements View.OnClickListener {
        public c() {
            NotificationHistoryFragment.this = r1;
        }

        @Override // android.view.View.OnClickListener
        public void onClick(View view) {
            WeakReference weakReference = new WeakReference(NotificationHistoryFragment.this.getContext());
            Integer valueOf = Integer.valueOf((int) R.string.confirm);
            Integer valueOf2 = Integer.valueOf((int) R.string.mark_all_confirm);
            Integer valueOf3 = Integer.valueOf((int) R.string.ok);
            Integer valueOf4 = Integer.valueOf((int) R.string.cancel);
            final NotificationHistoryFragment notificationHistoryFragment = NotificationHistoryFragment.this;
            tc1 tc1Var = new tc1() { // from class: ji2
                @Override // defpackage.tc1
                public final Object invoke(Object obj) {
                    te4 E;
                    E = NotificationHistoryFragment.E(NotificationHistoryFragment.this, (DialogInterface) obj);
                    return E;
                }
            };
            final NotificationHistoryFragment notificationHistoryFragment2 = NotificationHistoryFragment.this;
            bh.P(weakReference, valueOf, valueOf2, null, valueOf3, valueOf4, null, null, tc1Var, new tc1() { // from class: ki2
                @Override // defpackage.tc1
                public final Object invoke(Object obj) {
                    te4 D;
                    D = NotificationHistoryFragment.D(NotificationHistoryFragment.this, (DialogInterface) obj);
                    return D;
                }
            });
        }
    }

    /* loaded from: classes2.dex */
    public class d implements View.OnClickListener {
        public d() {
            NotificationHistoryFragment.this = r1;
        }

        @Override // android.view.View.OnClickListener
        public void onClick(View view) {
            WeakReference weakReference = new WeakReference(NotificationHistoryFragment.this.getContext());
            Integer valueOf = Integer.valueOf((int) R.string.confirm);
            Integer valueOf2 = Integer.valueOf((int) R.string.delete_all_confirm);
            Integer valueOf3 = Integer.valueOf((int) R.string.ok);
            Integer valueOf4 = Integer.valueOf((int) R.string.cancel);
            final NotificationHistoryFragment notificationHistoryFragment = NotificationHistoryFragment.this;
            tc1 tc1Var = new tc1() { // from class: li2
                @Override // defpackage.tc1
                public final Object invoke(Object obj) {
                    te4 G;
                    G = NotificationHistoryFragment.G(NotificationHistoryFragment.this, (DialogInterface) obj);
                    return G;
                }
            };
            final NotificationHistoryFragment notificationHistoryFragment2 = NotificationHistoryFragment.this;
            bh.P(weakReference, valueOf, valueOf2, null, valueOf3, valueOf4, null, null, tc1Var, new tc1() { // from class: mi2
                @Override // defpackage.tc1
                public final Object invoke(Object obj) {
                    te4 F;
                    F = NotificationHistoryFragment.F(NotificationHistoryFragment.this, (DialogInterface) obj);
                    return F;
                }
            });
        }
    }

    /* loaded from: classes2.dex */
    public class e implements View.OnClickListener {
        public e() {
            NotificationHistoryFragment.this = r1;
        }

        @Override // android.view.View.OnClickListener
        public void onClick(View view) {
            StringBuilder sb = new StringBuilder();
            sb.append("delete: ");
            sb.append(NotificationHistoryFragment.this.i0.toString());
            WeakReference weakReference = new WeakReference(NotificationHistoryFragment.this.getContext());
            Integer valueOf = Integer.valueOf((int) R.string.confirm);
            Integer valueOf2 = Integer.valueOf((int) R.string.delete_any_confirm);
            Integer valueOf3 = Integer.valueOf((int) R.string.ok);
            Integer valueOf4 = Integer.valueOf((int) R.string.cancel);
            final NotificationHistoryFragment notificationHistoryFragment = NotificationHistoryFragment.this;
            tc1 tc1Var = new tc1() { // from class: oi2
                @Override // defpackage.tc1
                public final Object invoke(Object obj) {
                    te4 J;
                    J = NotificationHistoryFragment.J(NotificationHistoryFragment.this, (DialogInterface) obj);
                    return J;
                }
            };
            final NotificationHistoryFragment notificationHistoryFragment2 = NotificationHistoryFragment.this;
            bh.P(weakReference, valueOf, valueOf2, null, valueOf3, valueOf4, null, null, tc1Var, new tc1() { // from class: ni2
                @Override // defpackage.tc1
                public final Object invoke(Object obj) {
                    te4 I;
                    I = NotificationHistoryFragment.I(NotificationHistoryFragment.this, (DialogInterface) obj);
                    return I;
                }
            });
        }
    }

    public static /* synthetic */ te4 B(NotificationHistoryFragment notificationHistoryFragment, DialogInterface dialogInterface) {
        return notificationHistoryFragment.R(dialogInterface);
    }

    public static /* synthetic */ te4 C(NotificationHistoryFragment notificationHistoryFragment, DialogInterface dialogInterface) {
        return notificationHistoryFragment.S(dialogInterface);
    }

    public static /* synthetic */ te4 D(NotificationHistoryFragment notificationHistoryFragment, DialogInterface dialogInterface) {
        return notificationHistoryFragment.P(dialogInterface);
    }

    public static /* synthetic */ te4 E(NotificationHistoryFragment notificationHistoryFragment, DialogInterface dialogInterface) {
        return notificationHistoryFragment.Q(dialogInterface);
    }

    public static /* synthetic */ te4 F(NotificationHistoryFragment notificationHistoryFragment, DialogInterface dialogInterface) {
        return notificationHistoryFragment.L(dialogInterface);
    }

    public static /* synthetic */ te4 G(NotificationHistoryFragment notificationHistoryFragment, DialogInterface dialogInterface) {
        return notificationHistoryFragment.M(dialogInterface);
    }

    public static /* synthetic */ te4 I(NotificationHistoryFragment notificationHistoryFragment, DialogInterface dialogInterface) {
        return notificationHistoryFragment.N(dialogInterface);
    }

    public static /* synthetic */ te4 J(NotificationHistoryFragment notificationHistoryFragment, DialogInterface dialogInterface) {
        return notificationHistoryFragment.O(dialogInterface);
    }

    public /* synthetic */ void a0(NotificationHistoryResult notificationHistoryResult) {
        if (!notificationHistoryResult.read) {
            this.l0.j(notificationHistoryResult.id);
        }
        g(net.safemoon.androidwallet.fragments.a.a(notificationHistoryResult.data.transactionHash, false));
    }

    public /* synthetic */ void c0(View view) {
        f();
    }

    public /* synthetic */ void d0(NotificationHistory notificationHistory) {
        if (notificationHistory != null && notificationHistory.getData() != null && notificationHistory.getData().getResult() != null && notificationHistory.getData().getResult().size() > 0) {
            this.k0.h(this.l0.g());
            this.j0.j.setVisibility(8);
            this.j0.i.b.setVisibility(0);
        } else {
            this.j0.j.setVisibility(0);
        }
        this.j0.g.setRefreshing(false);
    }

    public /* synthetic */ void e0(String str) {
        if (str != null) {
            this.j0.g.setRefreshing(false);
        }
    }

    public final void K() {
        this.j0.i.b.setText(R.string.edit);
        this.j0.h.setVisibility(8);
        this.i0.clear();
    }

    public final te4 L(DialogInterface dialogInterface) {
        return te4.a;
    }

    public final te4 M(DialogInterface dialogInterface) {
        T();
        return te4.a;
    }

    public final te4 N(DialogInterface dialogInterface) {
        return te4.a;
    }

    public final te4 O(DialogInterface dialogInterface) {
        U();
        return te4.a;
    }

    public final te4 P(DialogInterface dialogInterface) {
        return te4.a;
    }

    public final te4 Q(DialogInterface dialogInterface) {
        f0();
        return te4.a;
    }

    public final te4 R(DialogInterface dialogInterface) {
        return te4.a;
    }

    public final te4 S(DialogInterface dialogInterface) {
        g0();
        StringBuilder sb = new StringBuilder();
        sb.append("markAsRead: ");
        sb.append(this.i0.toString());
        return te4.a;
    }

    public final void T() {
        this.l0.b(new rc1() { // from class: yh2
            @Override // defpackage.rc1
            public final Object invoke() {
                te4 j0;
                j0 = NotificationHistoryFragment.this.j0();
                return j0;
            }
        }, new rc1() { // from class: vh2
            @Override // defpackage.rc1
            public final Object invoke() {
                te4 i0;
                i0 = NotificationHistoryFragment.this.i0();
                return i0;
            }
        });
    }

    public final void U() {
        this.l0.c(this.i0, new rc1() { // from class: ai2
            @Override // defpackage.rc1
            public final Object invoke() {
                te4 l0;
                l0 = NotificationHistoryFragment.this.l0();
                return l0;
            }
        }, new rc1() { // from class: ci2
            @Override // defpackage.rc1
            public final Object invoke() {
                te4 k0;
                k0 = NotificationHistoryFragment.this.k0();
                return k0;
            }
        });
    }

    public final void V() {
        this.j0.i.b.setText(R.string.cancel);
        this.j0.e.setVisibility(8);
        this.j0.d.setVisibility(0);
        this.j0.b.setVisibility(8);
        this.j0.c.setVisibility(0);
        this.j0.h.setVisibility(0);
    }

    public final void W() {
        this.j0.g.setRefreshing(true);
        this.l0.f();
    }

    public final void X() {
        this.j0.i.b.setVisibility(8);
        this.j0.h.setVisibility(8);
    }

    public final void Y() {
        uh2 uh2Var = new uh2(new ArrayList(), new uh2.f() { // from class: ei2
            @Override // defpackage.uh2.f
            public final void a(NotificationHistoryResult notificationHistoryResult) {
                NotificationHistoryFragment.this.a0(notificationHistoryResult);
            }
        }, new uh2.b() { // from class: di2
            @Override // defpackage.uh2.b
            public final void a(List list) {
                NotificationHistoryFragment.this.b0(list);
            }
        });
        this.k0 = uh2Var;
        this.j0.f.setAdapter(uh2Var);
    }

    public final void Z() {
        this.j0.g.setOnRefreshListener(new SwipeRefreshLayout.j() { // from class: xh2
            @Override // androidx.swiperefreshlayout.widget.SwipeRefreshLayout.j
            public final void onRefresh() {
                NotificationHistoryFragment.this.W();
            }
        });
        this.j0.i.a.setOnClickListener(new View.OnClickListener() { // from class: wh2
            @Override // android.view.View.OnClickListener
            public final void onClick(View view) {
                NotificationHistoryFragment.this.c0(view);
            }
        });
        this.j0.i.c.setText(getText(R.string.notification_history_screen_title));
        this.j0.i.b.setVisibility(8);
        this.j0.i.b.setOnClickListener(new a());
    }

    public final void f0() {
        this.l0.h(new bi2(this), new zh2(this));
    }

    public final void g0() {
        if (this.i0.size() > 0) {
            this.l0.i(this.i0, new bi2(this), new zh2(this));
        }
    }

    public final void h0() {
        this.l0.e().observe(getViewLifecycleOwner(), new tl2() { // from class: gi2
            @Override // defpackage.tl2
            public final void onChanged(Object obj) {
                NotificationHistoryFragment.this.d0((NotificationHistory) obj);
            }
        });
        this.l0.d().observe(getViewLifecycleOwner(), new tl2() { // from class: fi2
            @Override // defpackage.tl2
            public final void onChanged(Object obj) {
                NotificationHistoryFragment.this.e0((String) obj);
            }
        });
    }

    public final te4 i0() {
        return te4.a;
    }

    public final te4 j0() {
        this.i0.clear();
        p0();
        W();
        this.j0.f.setVisibility(8);
        this.j0.j.setVisibility(0);
        X();
        return te4.a;
    }

    public final te4 k0() {
        return te4.a;
    }

    public final te4 l0() {
        this.i0.clear();
        p0();
        W();
        return te4.a;
    }

    public final te4 m0() {
        return te4.a;
    }

    public final te4 n0() {
        this.i0.clear();
        q0();
        W();
        return te4.a;
    }

    /* renamed from: o0 */
    public final void b0(List<String> list) {
        this.i0 = list;
        StringBuilder sb = new StringBuilder();
        sb.append("selectedNotificationIds: ");
        sb.append(list.toString());
        p0();
    }

    @Override // androidx.fragment.app.Fragment
    public View onCreateView(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle) {
        pa1 a2 = pa1.a(layoutInflater.inflate(R.layout.fragment_notification_history, viewGroup, false));
        this.j0 = a2;
        return a2.b();
    }

    @Override // androidx.fragment.app.Fragment
    public void onResume() {
        super.onResume();
        m0 = false;
    }

    @Override // net.safemoon.androidwallet.fragments.common.BaseMainFragment, defpackage.qn, androidx.fragment.app.Fragment
    public void onViewCreated(View view, Bundle bundle) {
        super.onViewCreated(view, bundle);
        this.l0 = (qi2) new l(requireActivity()).a(qi2.class);
        Z();
        Y();
        h0();
        W();
    }

    public final void p0() {
        if (this.i0.size() > 0) {
            this.j0.e.setVisibility(0);
            this.j0.d.setVisibility(8);
            this.j0.b.setVisibility(0);
            this.j0.c.setVisibility(8);
            return;
        }
        this.j0.e.setVisibility(8);
        this.j0.d.setVisibility(0);
        this.j0.b.setVisibility(8);
        this.j0.c.setVisibility(0);
    }

    public final void q0() {
        r0();
    }

    public final void r0() {
        boolean z = !m0;
        m0 = z;
        if (z) {
            V();
        } else {
            K();
        }
        this.k0.i(m0);
    }

    public final void s0() {
        r0();
        this.j0.e.setOnClickListener(new b());
        this.j0.d.setOnClickListener(new c());
        this.j0.c.setOnClickListener(new d());
        this.j0.b.setOnClickListener(new e());
    }
}
