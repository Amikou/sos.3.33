package net.safemoon.androidwallet.fragments;

import defpackage.e63;
import kotlin.jvm.internal.Lambda;
import net.safemoon.androidwallet.model.reflections.RoomReflectionsToken;

/* compiled from: ReflectionsFragment.kt */
/* loaded from: classes2.dex */
public final class ReflectionsFragment$onViewCreated$7 extends Lambda implements tc1<RoomReflectionsToken, te4> {
    public final /* synthetic */ ReflectionsFragment this$0;

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public ReflectionsFragment$onViewCreated$7(ReflectionsFragment reflectionsFragment) {
        super(1);
        this.this$0 = reflectionsFragment;
    }

    @Override // defpackage.tc1
    public /* bridge */ /* synthetic */ te4 invoke(RoomReflectionsToken roomReflectionsToken) {
        invoke2(roomReflectionsToken);
        return te4.a;
    }

    /* renamed from: invoke  reason: avoid collision after fix types in other method */
    public final void invoke2(RoomReflectionsToken roomReflectionsToken) {
        fs1.f(roomReflectionsToken, "it");
        if (roomReflectionsToken.getEnableAdvanceMode()) {
            ReflectionsFragment reflectionsFragment = this.this$0;
            e63.c c = e63.c(roomReflectionsToken.getSymbolWithType());
            fs1.e(c, "actionReflectionsFragmen…ype\n                    )");
            reflectionsFragment.g(c);
        }
    }
}
