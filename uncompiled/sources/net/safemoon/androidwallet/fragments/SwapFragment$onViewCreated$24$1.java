package net.safemoon.androidwallet.fragments;

import kotlin.jvm.internal.Lambda;

/* compiled from: SwapFragment.kt */
/* loaded from: classes2.dex */
public final class SwapFragment$onViewCreated$24$1 extends Lambda implements rc1<te4> {
    public final /* synthetic */ SwapFragment this$0;

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public SwapFragment$onViewCreated$24$1(SwapFragment swapFragment) {
        super(0);
        this.this$0 = swapFragment;
    }

    @Override // defpackage.rc1
    public /* bridge */ /* synthetic */ te4 invoke() {
        invoke2();
        return te4.a;
    }

    @Override // defpackage.rc1
    /* renamed from: invoke  reason: avoid collision after fix types in other method */
    public final void invoke2() {
        SwapFragment swapFragment = this.this$0;
        ce2 a = iz3.a();
        fs1.e(a, "actionFragmentSwapToSwapMigrationFragment()");
        swapFragment.g(a);
    }
}
