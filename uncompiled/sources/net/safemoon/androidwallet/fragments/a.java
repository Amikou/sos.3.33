package net.safemoon.androidwallet.fragments;

import android.os.Bundle;
import java.util.HashMap;
import net.safemoon.androidwallet.R;

/* compiled from: NotificationHistoryFragmentDirections.java */
/* loaded from: classes2.dex */
public class a {

    /* compiled from: NotificationHistoryFragmentDirections.java */
    /* loaded from: classes2.dex */
    public static class b implements ce2 {
        public final HashMap a;

        @Override // defpackage.ce2
        public Bundle a() {
            Bundle bundle = new Bundle();
            if (this.a.containsKey("transactionHash")) {
                bundle.putString("transactionHash", (String) this.a.get("transactionHash"));
            }
            if (this.a.containsKey("isNewTransaction")) {
                bundle.putBoolean("isNewTransaction", ((Boolean) this.a.get("isNewTransaction")).booleanValue());
            }
            return bundle;
        }

        @Override // defpackage.ce2
        public int b() {
            return R.id.action_notificationHistoryFragment_to_transferNotificationDetailsFragment;
        }

        public boolean c() {
            return ((Boolean) this.a.get("isNewTransaction")).booleanValue();
        }

        public String d() {
            return (String) this.a.get("transactionHash");
        }

        public boolean equals(Object obj) {
            if (this == obj) {
                return true;
            }
            if (obj == null || b.class != obj.getClass()) {
                return false;
            }
            b bVar = (b) obj;
            if (this.a.containsKey("transactionHash") != bVar.a.containsKey("transactionHash")) {
                return false;
            }
            if (d() == null ? bVar.d() == null : d().equals(bVar.d())) {
                return this.a.containsKey("isNewTransaction") == bVar.a.containsKey("isNewTransaction") && c() == bVar.c() && b() == bVar.b();
            }
            return false;
        }

        public int hashCode() {
            return (((((d() != null ? d().hashCode() : 0) + 31) * 31) + (c() ? 1 : 0)) * 31) + b();
        }

        public String toString() {
            return "ActionNotificationHistoryFragmentToTransferNotificationDetailsFragment(actionId=" + b() + "){transactionHash=" + d() + ", isNewTransaction=" + c() + "}";
        }

        public b(String str, boolean z) {
            HashMap hashMap = new HashMap();
            this.a = hashMap;
            if (str != null) {
                hashMap.put("transactionHash", str);
                hashMap.put("isNewTransaction", Boolean.valueOf(z));
                return;
            }
            throw new IllegalArgumentException("Argument \"transactionHash\" is marked as non-null but was passed a null value.");
        }
    }

    public static b a(String str, boolean z) {
        return new b(str, z);
    }
}
