package net.safemoon.androidwallet.fragments;

import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import androidx.fragment.app.FragmentViewModelLazyKt;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import net.safemoon.androidwallet.R;
import net.safemoon.androidwallet.fragments.ChainNetworkReflectionFragment;
import net.safemoon.androidwallet.fragments.common.BaseMainFragment;
import net.safemoon.androidwallet.viewmodels.CustomReflectionContractTokenViewModel;

/* compiled from: ChainNetworkReflectionFragment.kt */
/* loaded from: classes2.dex */
public final class ChainNetworkReflectionFragment extends BaseMainFragment {
    public s91 j0;
    public final sy1 i0 = FragmentViewModelLazyKt.a(this, d53.b(CustomReflectionContractTokenViewModel.class), new ChainNetworkReflectionFragment$special$$inlined$viewModels$default$1(new ChainNetworkReflectionFragment$customReflectionContractTokenViewModel$2(this)), null);
    public final sy1 k0 = zy1.a(new ChainNetworkReflectionFragment$chainNetworkAdapter$2(this));

    /* compiled from: ChainNetworkReflectionFragment.kt */
    /* loaded from: classes2.dex */
    public static final class a implements TextWatcher {
        public a() {
        }

        @Override // android.text.TextWatcher
        public void afterTextChanged(Editable editable) {
            ChainNetworkReflectionFragment.this.r().getFilter().filter(editable);
        }

        @Override // android.text.TextWatcher
        public void beforeTextChanged(CharSequence charSequence, int i, int i2, int i3) {
        }

        @Override // android.text.TextWatcher
        public void onTextChanged(CharSequence charSequence, int i, int i2, int i3) {
        }
    }

    public static final void t(ChainNetworkReflectionFragment chainNetworkReflectionFragment, View view) {
        fs1.f(chainNetworkReflectionFragment, "this$0");
        chainNetworkReflectionFragment.requireActivity().onBackPressed();
    }

    @Override // androidx.fragment.app.Fragment
    public View onCreateView(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle) {
        fs1.f(layoutInflater, "inflater");
        return layoutInflater.inflate(R.layout.fragment_chain_network, viewGroup, false);
    }

    @Override // net.safemoon.androidwallet.fragments.common.BaseMainFragment, defpackage.qn, androidx.fragment.app.Fragment
    public void onViewCreated(View view, Bundle bundle) {
        fs1.f(view, "view");
        super.onViewCreated(view, bundle);
        s91 a2 = s91.a(view);
        fs1.e(a2, "bind(view)");
        this.j0 = a2;
        s91 s91Var = null;
        if (a2 == null) {
            fs1.r("binding");
            a2 = null;
        }
        a2.c.a.setOnClickListener(new View.OnClickListener() { // from class: fx
            @Override // android.view.View.OnClickListener
            public final void onClick(View view2) {
                ChainNetworkReflectionFragment.t(ChainNetworkReflectionFragment.this, view2);
            }
        });
        a2.c.c.setText(R.string.screen_title_cc_network);
        RecyclerView recyclerView = a2.a;
        recyclerView.setLayoutManager(new LinearLayoutManager(recyclerView.getContext(), 1, false));
        recyclerView.setAdapter(r());
        s91 s91Var2 = this.j0;
        if (s91Var2 == null) {
            fs1.r("binding");
        } else {
            s91Var = s91Var2;
        }
        s91Var.b.b.addTextChangedListener(new a());
    }

    public final cx r() {
        return (cx) this.k0.getValue();
    }

    public final CustomReflectionContractTokenViewModel s() {
        return (CustomReflectionContractTokenViewModel) this.i0.getValue();
    }
}
