package net.safemoon.androidwallet.fragments;

import kotlin.jvm.internal.Lambda;
import net.safemoon.androidwallet.common.TokenType;
import net.safemoon.androidwallet.viewmodels.CustomReflectionContractTokenViewModel;

/* compiled from: ChainNetworkReflectionFragment.kt */
/* loaded from: classes2.dex */
public final class ChainNetworkReflectionFragment$chainNetworkAdapter$2 extends Lambda implements rc1<cx> {
    public final /* synthetic */ ChainNetworkReflectionFragment this$0;

    /* compiled from: ChainNetworkReflectionFragment.kt */
    /* renamed from: net.safemoon.androidwallet.fragments.ChainNetworkReflectionFragment$chainNetworkAdapter$2$1  reason: invalid class name */
    /* loaded from: classes2.dex */
    public static final class AnonymousClass1 extends Lambda implements tc1<TokenType, te4> {
        public final /* synthetic */ ChainNetworkReflectionFragment this$0;

        /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
        public AnonymousClass1(ChainNetworkReflectionFragment chainNetworkReflectionFragment) {
            super(1);
            this.this$0 = chainNetworkReflectionFragment;
        }

        @Override // defpackage.tc1
        public /* bridge */ /* synthetic */ te4 invoke(TokenType tokenType) {
            invoke2(tokenType);
            return te4.a;
        }

        /* renamed from: invoke  reason: avoid collision after fix types in other method */
        public final void invoke2(TokenType tokenType) {
            CustomReflectionContractTokenViewModel s;
            fs1.f(tokenType, "it");
            s = this.this$0.s();
            s.o().postValue(tokenType);
            this.this$0.requireActivity().onBackPressed();
        }
    }

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public ChainNetworkReflectionFragment$chainNetworkAdapter$2(ChainNetworkReflectionFragment chainNetworkReflectionFragment) {
        super(0);
        this.this$0 = chainNetworkReflectionFragment;
    }

    /* JADX WARN: Can't rename method to resolve collision */
    @Override // defpackage.rc1
    public final cx invoke() {
        return new cx(new AnonymousClass1(this.this$0));
    }
}
