package net.safemoon.androidwallet.fragments;

import kotlin.jvm.internal.Lambda;
import net.safemoon.androidwallet.viewmodels.CalculatorViewModel;

/* compiled from: CalculatorFragment.kt */
/* loaded from: classes2.dex */
public final class CalculatorFragment$switchTokenListener$1$1 extends Lambda implements tc1<q9, te4> {
    public final /* synthetic */ CalculatorFragment this$0;

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public CalculatorFragment$switchTokenListener$1$1(CalculatorFragment calculatorFragment) {
        super(1);
        this.this$0 = calculatorFragment;
    }

    @Override // defpackage.tc1
    public /* bridge */ /* synthetic */ te4 invoke(q9 q9Var) {
        invoke2(q9Var);
        return te4.a;
    }

    /* renamed from: invoke  reason: avoid collision after fix types in other method */
    public final void invoke2(q9 q9Var) {
        CalculatorViewModel a0;
        fs1.f(q9Var, "it");
        this.this$0.y0 = true;
        a0 = this.this$0.a0();
        a0.t().postValue(q9Var.h());
    }
}
