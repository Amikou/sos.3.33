package net.safemoon.androidwallet.fragments;

import android.content.Context;
import kotlin.jvm.internal.Lambda;

/* compiled from: TokenListFragment.kt */
/* loaded from: classes2.dex */
public final class TokenListFragment$getCheckoutUrl$2 extends Lambda implements tc1<String, te4> {
    public final /* synthetic */ TokenListFragment this$0;

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public TokenListFragment$getCheckoutUrl$2(TokenListFragment tokenListFragment) {
        super(1);
        this.this$0 = tokenListFragment;
    }

    @Override // defpackage.tc1
    public /* bridge */ /* synthetic */ te4 invoke(String str) {
        invoke2(str);
        return te4.a;
    }

    /* renamed from: invoke  reason: avoid collision after fix types in other method */
    public final void invoke2(String str) {
        this.this$0.m0 = false;
        if (str != null) {
            Context requireContext = this.this$0.requireContext();
            fs1.e(requireContext, "requireContext()");
            e30.a0(requireContext, str);
        }
    }
}
