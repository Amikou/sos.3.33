package net.safemoon.androidwallet.fragments;

import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.content.res.Resources;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.util.DisplayMetrics;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import androidx.coordinatorlayout.widget.CoordinatorLayout;
import androidx.fragment.app.FragmentActivity;
import androidx.fragment.app.FragmentViewModelLazyKt;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import com.creageek.segmentedbutton.SegmentedButton;
import com.google.android.material.appbar.AppBarLayout;
import com.google.android.material.textview.MaterialTextView;
import defpackage.e63;
import defpackage.lt2;
import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Objects;
import net.safemoon.androidwallet.R;
import net.safemoon.androidwallet.adapter.touchHelper.RecyclerTouchListener;
import net.safemoon.androidwallet.common.TokenType;
import net.safemoon.androidwallet.domain.listener.dalog.OnSelectTokenTypeClickListener;
import net.safemoon.androidwallet.fragments.ReflectionsFragment;
import net.safemoon.androidwallet.fragments.common.BaseMainFragment;
import net.safemoon.androidwallet.model.notificationHistory.NotificationHistory;
import net.safemoon.androidwallet.model.notificationHistory.NotificationHistoryData;
import net.safemoon.androidwallet.model.notificationHistory.NotificationHistoryResult;
import net.safemoon.androidwallet.model.reflections.RoomReflectionsToken;
import net.safemoon.androidwallet.viewmodels.ReflectionTrackerViewModel;

/* compiled from: ReflectionsFragment.kt */
/* loaded from: classes2.dex */
public final class ReflectionsFragment extends BaseMainFragment {
    public OnSelectTokenTypeClickListener k0;
    public ta1 l0;
    public final sy1 i0 = FragmentViewModelLazyKt.a(this, d53.b(qi2.class), new ReflectionsFragment$special$$inlined$activityViewModels$default$1(this), new ReflectionsFragment$special$$inlined$activityViewModels$default$2(this));
    public final sy1 j0 = FragmentViewModelLazyKt.a(this, d53.b(ReflectionTrackerViewModel.class), new ReflectionsFragment$special$$inlined$activityViewModels$default$3(this), new ReflectionsFragment$special$$inlined$activityViewModels$default$4(this));
    public final sy1 m0 = zy1.a(ReflectionsFragment$reflectionTokenAdapter$2.INSTANCE);
    public final Handler n0 = new Handler(Looper.getMainLooper());

    /* compiled from: ReflectionsFragment.kt */
    /* loaded from: classes2.dex */
    public static final class a extends OnSelectTokenTypeClickListener {
        public a(b bVar, WeakReference<Activity> weakReference, Map<String, ? extends TokenType> map) {
            super(bVar, weakReference, map);
        }

        @Override // net.safemoon.androidwallet.domain.listener.dalog.OnSelectTokenTypeClickListener
        public TokenType c() {
            TokenType value = ReflectionsFragment.this.D().t().getValue();
            if (value == null) {
                Context requireContext = ReflectionsFragment.this.requireContext();
                fs1.e(requireContext, "requireContext()");
                value = e30.e(requireContext);
            }
            fs1.e(value, "reflectionViewModel.sele…:requireContext().chain()");
            return value;
        }
    }

    /* compiled from: ReflectionsFragment.kt */
    /* loaded from: classes2.dex */
    public static final class b implements lt2.a {
        public b() {
        }

        @Override // defpackage.lt2.a
        public void a(TokenType tokenType) {
            fs1.f(tokenType, "token");
            ReflectionsFragment.this.D().t().postValue(tokenType);
        }
    }

    public static final void F(ReflectionsFragment reflectionsFragment, List list, RecyclerTouchListener recyclerTouchListener) {
        fs1.f(reflectionsFragment, "this$0");
        fs1.f(list, "$list");
        fs1.f(recyclerTouchListener, "$touchListener");
        if (reflectionsFragment.l0 == null) {
            fs1.r("binding");
        }
        reflectionsFragment.O(!list.isEmpty());
        reflectionsFragment.C().submitList(null);
        reflectionsFragment.C().submitList(list);
        reflectionsFragment.C().notifyDataSetChanged();
        ArrayList arrayList = new ArrayList();
        if (arrayList.size() > 0) {
            Object[] array = arrayList.toArray(new Integer[0]);
            Objects.requireNonNull(array, "null cannot be cast to non-null type kotlin.Array<T>");
            Integer[] numArr = (Integer[]) array;
            recyclerTouchListener.z((Integer[]) Arrays.copyOf(numArr, numArr.length));
            return;
        }
        recyclerTouchListener.z(new Integer[0]);
    }

    public static final void G(ReflectionsFragment reflectionsFragment, TokenType tokenType) {
        fs1.f(reflectionsFragment, "this$0");
        if (tokenType == null) {
            return;
        }
        ta1 ta1Var = reflectionsFragment.l0;
        ta1 ta1Var2 = null;
        if (ta1Var == null) {
            fs1.r("binding");
            ta1Var = null;
        }
        ta1Var.d.d.setText(tokenType.getPlaneName());
        ta1 ta1Var3 = reflectionsFragment.l0;
        if (ta1Var3 == null) {
            fs1.r("binding");
        } else {
            ta1Var2 = ta1Var3;
        }
        ta1Var2.d.c.setImageResource(tokenType.getIcon());
    }

    public static final void H(ReflectionsFragment reflectionsFragment, int i, int i2) {
        fs1.f(reflectionsFragment, "this$0");
        if (i == R.id.btnDelete) {
            RoomReflectionsToken roomReflectionsToken = reflectionsFragment.C().getCurrentList().get(i2);
            ReflectionTrackerViewModel D = reflectionsFragment.D();
            fs1.e(roomReflectionsToken, "item");
            D.j(roomReflectionsToken);
        }
    }

    public static final void I(DialogInterface dialogInterface) {
    }

    public static final void J(ReflectionsFragment reflectionsFragment, NotificationHistory notificationHistory) {
        NotificationHistoryData data;
        ArrayList<NotificationHistoryResult> result;
        fs1.f(reflectionsFragment, "this$0");
        if (notificationHistory == null || (data = notificationHistory.getData()) == null || (result = data.getResult()) == null) {
            return;
        }
        ArrayList arrayList = new ArrayList();
        Iterator<T> it = result.iterator();
        while (true) {
            if (!it.hasNext()) {
                break;
            }
            Object next = it.next();
            if (true ^ ((NotificationHistoryResult) next).read) {
                arrayList.add(next);
            }
        }
        int size = arrayList.size();
        ta1 ta1Var = reflectionsFragment.l0;
        if (ta1Var == null) {
            fs1.r("binding");
            ta1Var = null;
        }
        MaterialTextView materialTextView = ta1Var.f.i;
        materialTextView.setVisibility(e30.m0(size > 0));
        materialTextView.setText(size < 100 ? String.valueOf(size) : "99+");
    }

    public static final void K(ReflectionsFragment reflectionsFragment, View view) {
        fs1.f(reflectionsFragment, "this$0");
        if (reflectionsFragment.C().getCurrentList().size() >= 3) {
            bh.h0(new WeakReference(reflectionsFragment.requireActivity()), R.string.reflection_limit_message, R.string.ok);
            return;
        }
        TokenType value = reflectionsFragment.D().t().getValue();
        fs1.d(value);
        e63.b a2 = e63.a(value.getChainId());
        fs1.e(a2, "actionReflectionsFragmen…                        )");
        reflectionsFragment.g(a2);
    }

    public static final void L(ReflectionsFragment reflectionsFragment, View view) {
        fs1.f(reflectionsFragment, "this$0");
        ce2 b2 = e63.b();
        fs1.e(b2, "actionReflectionsFragmen…ficationHistoryFragment()");
        reflectionsFragment.g(b2);
    }

    public static final void M(ReflectionsFragment reflectionsFragment, View view) {
        fs1.f(reflectionsFragment, "this$0");
        FragmentActivity requireActivity = reflectionsFragment.requireActivity();
        fs1.e(requireActivity, "requireActivity()");
        jc0.e(requireActivity, Integer.valueOf((int) R.string.reflection_tracker_title), R.string.reflection_tracker_detail, false, x53.a, 8, null);
    }

    public static final void N(DialogInterface dialogInterface) {
    }

    public final void A() {
        Resources resources;
        DisplayMetrics displayMetrics;
        if (r44.b(Locale.getDefault()) == 1) {
            int i = Resources.getSystem().getDisplayMetrics().widthPixels;
            FragmentActivity activity = getActivity();
            Float valueOf = (activity == null || (resources = activity.getResources()) == null || (displayMetrics = resources.getDisplayMetrics()) == null) ? null : Float.valueOf(displayMetrics.density);
            fs1.d(valueOf);
            int b2 = i - t42.b(85 * valueOf.floatValue());
            ta1 ta1Var = this.l0;
            if (ta1Var == null) {
                fs1.r("binding");
                ta1Var = null;
            }
            wf wfVar = ta1Var.f;
            SegmentedButton segmentedButton = wfVar == null ? null : wfVar.h;
            ViewGroup.LayoutParams layoutParams = segmentedButton != null ? segmentedButton.getLayoutParams() : null;
            if (layoutParams != null) {
                layoutParams.width = b2;
            }
            if (segmentedButton == null) {
                return;
            }
            segmentedButton.setLayoutParams(layoutParams);
        }
    }

    public final qi2 B() {
        return (qi2) this.i0.getValue();
    }

    public final g53 C() {
        return (g53) this.m0.getValue();
    }

    public final ReflectionTrackerViewModel D() {
        return (ReflectionTrackerViewModel) this.j0.getValue();
    }

    public final void E(final RecyclerTouchListener recyclerTouchListener, final List<RoomReflectionsToken> list) {
        this.n0.removeCallbacksAndMessages(null);
        this.n0.postDelayed(new Runnable() { // from class: b63
            @Override // java.lang.Runnable
            public final void run() {
                ReflectionsFragment.F(ReflectionsFragment.this, list, recyclerTouchListener);
            }
        }, 500L);
    }

    public final void O(boolean z) {
        if (z) {
            P();
        } else {
            Q();
        }
    }

    public final void P() {
        ta1 ta1Var = this.l0;
        if (ta1Var == null) {
            fs1.r("binding");
            ta1Var = null;
        }
        ViewGroup.LayoutParams layoutParams = ta1Var.b.getLayoutParams();
        Objects.requireNonNull(layoutParams, "null cannot be cast to non-null type com.google.android.material.appbar.AppBarLayout.LayoutParams");
        AppBarLayout.LayoutParams layoutParams2 = (AppBarLayout.LayoutParams) layoutParams;
        layoutParams2.d(5);
        ta1Var.b.setLayoutParams(layoutParams2);
        ViewGroup.LayoutParams layoutParams3 = ta1Var.a.getLayoutParams();
        Objects.requireNonNull(layoutParams3, "null cannot be cast to non-null type androidx.coordinatorlayout.widget.CoordinatorLayout.LayoutParams");
        CoordinatorLayout.e eVar = (CoordinatorLayout.e) layoutParams3;
        eVar.o(new AppBarLayout.Behavior());
        ta1Var.a.setLayoutParams(eVar);
    }

    public final void Q() {
        ta1 ta1Var = this.l0;
        if (ta1Var == null) {
            fs1.r("binding");
            ta1Var = null;
        }
        ViewGroup.LayoutParams layoutParams = ta1Var.b.getLayoutParams();
        Objects.requireNonNull(layoutParams, "null cannot be cast to non-null type com.google.android.material.appbar.AppBarLayout.LayoutParams");
        AppBarLayout.LayoutParams layoutParams2 = (AppBarLayout.LayoutParams) layoutParams;
        layoutParams2.d(0);
        ta1Var.b.setLayoutParams(layoutParams2);
        ViewGroup.LayoutParams layoutParams3 = ta1Var.a.getLayoutParams();
        Objects.requireNonNull(layoutParams3, "null cannot be cast to non-null type androidx.coordinatorlayout.widget.CoordinatorLayout.LayoutParams");
        CoordinatorLayout.e eVar = (CoordinatorLayout.e) layoutParams3;
        eVar.o(null);
        ta1Var.a.setLayoutParams(eVar);
    }

    @Override // androidx.fragment.app.Fragment
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        D().t().postValue(D().s());
    }

    @Override // androidx.fragment.app.Fragment
    public View onCreateView(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle) {
        fs1.f(layoutInflater, "inflater");
        return layoutInflater.inflate(R.layout.fragment_reflections, viewGroup, false);
    }

    @Override // androidx.fragment.app.Fragment
    public void onPause() {
        super.onPause();
        OnSelectTokenTypeClickListener onSelectTokenTypeClickListener = this.k0;
        if (onSelectTokenTypeClickListener == null) {
            return;
        }
        onSelectTokenTypeClickListener.b();
    }

    @Override // net.safemoon.androidwallet.fragments.common.BaseMainFragment, defpackage.qn, androidx.fragment.app.Fragment
    public void onViewCreated(View view, Bundle bundle) {
        fs1.f(view, "view");
        super.onViewCreated(view, bundle);
        ta1 a2 = ta1.a(view);
        fs1.e(a2, "bind(view)");
        this.l0 = a2;
        B().e().observe(getViewLifecycleOwner(), new tl2() { // from class: v53
            @Override // defpackage.tl2
            public final void onChanged(Object obj) {
                ReflectionsFragment.J(ReflectionsFragment.this, (NotificationHistory) obj);
            }
        });
        ta1 ta1Var = this.l0;
        if (ta1Var == null) {
            fs1.r("binding");
            ta1Var = null;
        }
        ta1Var.f.f.setChecked(true);
        RecyclerView recyclerView = ta1Var.e;
        recyclerView.setLayoutManager(new LinearLayoutManager(requireContext(), 1, false));
        recyclerView.setAdapter(C());
        ta1Var.f.b.setVisibility(4);
        ta1Var.f.c.setVisibility(0);
        ta1Var.f.c.setOnClickListener(new View.OnClickListener() { // from class: z53
            @Override // android.view.View.OnClickListener
            public final void onClick(View view2) {
                ReflectionsFragment.K(ReflectionsFragment.this, view2);
            }
        });
        ta1Var.f.a.setOnClickListener(new View.OnClickListener() { // from class: y53
            @Override // android.view.View.OnClickListener
            public final void onClick(View view2) {
                ReflectionsFragment.L(ReflectionsFragment.this, view2);
            }
        });
        SegmentedButton segmentedButton = ta1Var.f.h;
        fs1.e(segmentedButton, "it.topBar.segmentedGroup");
        n(segmentedButton);
        ta1Var.c.setOnClickListener(new View.OnClickListener() { // from class: a63
            @Override // android.view.View.OnClickListener
            public final void onClick(View view2) {
                ReflectionsFragment.M(ReflectionsFragment.this, view2);
            }
        });
        A();
        this.k0 = new a(new b(), new WeakReference(requireActivity()), D().u());
        ta1 ta1Var2 = this.l0;
        if (ta1Var2 == null) {
            fs1.r("binding");
            ta1Var2 = null;
        }
        ta1Var2.d.a.setOnClickListener(this.k0);
        D().t().observe(getViewLifecycleOwner(), new tl2() { // from class: u53
            @Override // defpackage.tl2
            public final void onChanged(Object obj) {
                ReflectionsFragment.G(ReflectionsFragment.this, (TokenType) obj);
            }
        });
        C().g(new ReflectionsFragment$onViewCreated$6(this));
        C().e(new ReflectionsFragment$onViewCreated$7(this));
        FragmentActivity requireActivity = requireActivity();
        ta1 ta1Var3 = this.l0;
        if (ta1Var3 == null) {
            fs1.r("binding");
            ta1Var3 = null;
        }
        RecyclerTouchListener recyclerTouchListener = new RecyclerTouchListener(requireActivity, ta1Var3.e);
        recyclerTouchListener.x(Integer.valueOf((int) R.id.btnDelete)).y(R.id.rowFG, R.id.rowBG, new RecyclerTouchListener.k() { // from class: c63
            @Override // net.safemoon.androidwallet.adapter.touchHelper.RecyclerTouchListener.k
            public final void a(int i, int i2) {
                ReflectionsFragment.H(ReflectionsFragment.this, i, i2);
            }
        });
        ta1 ta1Var4 = this.l0;
        if (ta1Var4 == null) {
            fs1.r("binding");
            ta1Var4 = null;
        }
        ta1Var4.e.k(recyclerTouchListener);
        sz1.a(this).b(new ReflectionsFragment$onViewCreated$9(this, recyclerTouchListener, null));
        sn4 sn4Var = sn4.a;
        Context requireContext = requireContext();
        fs1.e(requireContext, "requireContext()");
        if (sn4Var.a(requireContext, "REFLECTION_INTRO", true)) {
            FragmentActivity requireActivity2 = requireActivity();
            fs1.e(requireActivity2, "requireActivity()");
            jc0.e(requireActivity2, Integer.valueOf((int) R.string.reflection_tracker_title), R.string.reflection_tracker_detail, false, w53.a, 8, null);
        }
    }
}
