package net.safemoon.androidwallet.fragments;

import kotlin.jvm.internal.Lambda;
import net.safemoon.androidwallet.fragments.SelectCurrencyFragment;
import net.safemoon.androidwallet.model.swap.Swap;
import net.safemoon.androidwallet.viewmodels.SwapViewModel;

/* compiled from: SelectCurrencyFragment.kt */
/* loaded from: classes2.dex */
public final class SelectCurrencyFragment$onViewCreated$2 extends Lambda implements tc1<Swap, te4> {
    public final /* synthetic */ SelectCurrencyFragment this$0;

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public SelectCurrencyFragment$onViewCreated$2(SelectCurrencyFragment selectCurrencyFragment) {
        super(1);
        this.this$0 = selectCurrencyFragment;
    }

    @Override // defpackage.tc1
    public /* bridge */ /* synthetic */ te4 invoke(Swap swap) {
        invoke2(swap);
        return te4.a;
    }

    /* renamed from: invoke  reason: avoid collision after fix types in other method */
    public final void invoke2(Swap swap) {
        SelectCurrencyFragment.SWAPPATH y;
        SelectCurrencyFragment.SWAPPATH y2;
        SelectCurrencyFragment.SWAPPATH y3;
        SwapViewModel x;
        SwapViewModel x2;
        SwapViewModel x3;
        SwapViewModel x4;
        SwapViewModel x5;
        SwapViewModel x6;
        fs1.f(swap, "it");
        y = this.this$0.y();
        SelectCurrencyFragment.SWAPPATH swappath = SelectCurrencyFragment.SWAPPATH.Source;
        if (y == swappath) {
            x5 = this.this$0.x();
            if (x5.b0().getValue() != null) {
                x6 = this.this$0.x();
                Swap value = x6.b0().getValue();
                fs1.d(value);
                if (fs1.b(value.address, swap.address)) {
                    return;
                }
            }
        }
        y2 = this.this$0.y();
        if (y2 == SelectCurrencyFragment.SWAPPATH.Destination) {
            x3 = this.this$0.x();
            if (x3.A0().getValue() != null) {
                x4 = this.this$0.x();
                Swap value2 = x4.A0().getValue();
                fs1.d(value2);
                if (fs1.b(value2.address, swap.address)) {
                    return;
                }
            }
        }
        y3 = this.this$0.y();
        if (y3 == swappath) {
            x2 = this.this$0.x();
            x2.A0().setValue(swap);
        } else {
            x = this.this$0.x();
            x.b0().setValue(swap);
        }
        pg4.e(this.this$0.requireActivity());
        this.this$0.getParentFragmentManager().Y0();
    }
}
