package net.safemoon.androidwallet.fragments;

import kotlin.coroutines.jvm.internal.SuspendLambda;
import net.safemoon.androidwallet.model.Coin;
import net.safemoon.androidwallet.viewmodels.HomeViewModel;

/* compiled from: AllTokensListFragment.kt */
@kotlin.coroutines.jvm.internal.a(c = "net.safemoon.androidwallet.fragments.AllTokensListFragment$observeViewModel$3", f = "AllTokensListFragment.kt", l = {220}, m = "invokeSuspend")
/* loaded from: classes2.dex */
public final class AllTokensListFragment$observeViewModel$3 extends SuspendLambda implements hd1<c90, q70<? super te4>, Object> {
    public int label;
    public final /* synthetic */ AllTokensListFragment this$0;

    /* compiled from: AllTokensListFragment.kt */
    @kotlin.coroutines.jvm.internal.a(c = "net.safemoon.androidwallet.fragments.AllTokensListFragment$observeViewModel$3$1", f = "AllTokensListFragment.kt", l = {222}, m = "invokeSuspend")
    /* renamed from: net.safemoon.androidwallet.fragments.AllTokensListFragment$observeViewModel$3$1  reason: invalid class name */
    /* loaded from: classes2.dex */
    public static final class AnonymousClass1 extends SuspendLambda implements hd1<fp2<Coin>, q70<? super te4>, Object> {
        public /* synthetic */ Object L$0;
        public int label;
        public final /* synthetic */ AllTokensListFragment this$0;

        /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
        public AnonymousClass1(AllTokensListFragment allTokensListFragment, q70<? super AnonymousClass1> q70Var) {
            super(2, q70Var);
            this.this$0 = allTokensListFragment;
        }

        @Override // kotlin.coroutines.jvm.internal.BaseContinuationImpl
        public final q70<te4> create(Object obj, q70<?> q70Var) {
            AnonymousClass1 anonymousClass1 = new AnonymousClass1(this.this$0, q70Var);
            anonymousClass1.L$0 = obj;
            return anonymousClass1;
        }

        @Override // defpackage.hd1
        public final Object invoke(fp2<Coin> fp2Var, q70<? super te4> q70Var) {
            return ((AnonymousClass1) create(fp2Var, q70Var)).invokeSuspend(te4.a);
        }

        @Override // kotlin.coroutines.jvm.internal.BaseContinuationImpl
        public final Object invokeSuspend(Object obj) {
            pa0 pa0Var;
            String str;
            pa0 pa0Var2;
            Object d = gs1.d();
            int i = this.label;
            if (i == 0) {
                o83.b(obj);
                pa0Var = this.this$0.v0;
                fs1.d(pa0Var);
                str = this.this$0.w0;
                pa0Var.j(str);
                pa0Var2 = this.this$0.v0;
                fs1.d(pa0Var2);
                this.label = 1;
                if (pa0Var2.e((fp2) this.L$0, this) == d) {
                    return d;
                }
            } else if (i != 1) {
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            } else {
                o83.b(obj);
            }
            return te4.a;
        }
    }

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public AllTokensListFragment$observeViewModel$3(AllTokensListFragment allTokensListFragment, q70<? super AllTokensListFragment$observeViewModel$3> q70Var) {
        super(2, q70Var);
        this.this$0 = allTokensListFragment;
    }

    @Override // kotlin.coroutines.jvm.internal.BaseContinuationImpl
    public final q70<te4> create(Object obj, q70<?> q70Var) {
        return new AllTokensListFragment$observeViewModel$3(this.this$0, q70Var);
    }

    @Override // defpackage.hd1
    public final Object invoke(c90 c90Var, q70<? super te4> q70Var) {
        return ((AllTokensListFragment$observeViewModel$3) create(c90Var, q70Var)).invokeSuspend(te4.a);
    }

    @Override // kotlin.coroutines.jvm.internal.BaseContinuationImpl
    public final Object invokeSuspend(Object obj) {
        Object d = gs1.d();
        int i = this.label;
        if (i == 0) {
            o83.b(obj);
            HomeViewModel homeViewModel = this.this$0.s0;
            fs1.d(homeViewModel);
            j71<fp2<Coin>> f = homeViewModel.f();
            AnonymousClass1 anonymousClass1 = new AnonymousClass1(this.this$0, null);
            this.label = 1;
            if (n71.f(f, anonymousClass1, this) == d) {
                return d;
            }
        } else if (i != 1) {
            throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        } else {
            o83.b(obj);
        }
        return te4.a;
    }
}
