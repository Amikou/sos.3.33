package net.safemoon.androidwallet.fragments;

import android.view.View;
import kotlin.jvm.internal.Lambda;

/* compiled from: ReflectionsAdvanceFragment.kt */
/* loaded from: classes2.dex */
public final class ReflectionsAdvanceFragment$binding$2 extends Lambda implements rc1<sa1> {
    public final /* synthetic */ ReflectionsAdvanceFragment this$0;

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public ReflectionsAdvanceFragment$binding$2(ReflectionsAdvanceFragment reflectionsAdvanceFragment) {
        super(0);
        this.this$0 = reflectionsAdvanceFragment;
    }

    @Override // defpackage.rc1
    public final sa1 invoke() {
        View view = this.this$0.getView();
        if (view == null) {
            return null;
        }
        return sa1.a(view);
    }
}
