package net.safemoon.androidwallet.fragments;

import android.text.Editable;
import com.google.android.material.textfield.TextInputEditText;
import kotlin.jvm.internal.Lambda;
import net.safemoon.androidwallet.viewmodels.SettingNotificationViewModel;

/* compiled from: NotificationFragment.kt */
/* loaded from: classes2.dex */
public final class NotificationFragment$initPriceAlert$1$1$2 extends Lambda implements tc1<Editable, te4> {
    public final /* synthetic */ oa1 $this_with;
    public final /* synthetic */ NotificationFragment this$0;

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public NotificationFragment$initPriceAlert$1$1$2(oa1 oa1Var, NotificationFragment notificationFragment) {
        super(1);
        this.$this_with = oa1Var;
        this.this$0 = notificationFragment;
    }

    @Override // defpackage.tc1
    public /* bridge */ /* synthetic */ te4 invoke(Editable editable) {
        invoke2(editable);
        return te4.a;
    }

    /* renamed from: invoke  reason: avoid collision after fix types in other method */
    public final void invoke2(Editable editable) {
        SettingNotificationViewModel H;
        TextInputEditText textInputEditText = this.$this_with.h.b;
        fs1.e(textInputEditText, "searchBar.etSearch");
        cj4.w(textInputEditText, editable);
        H = this.this$0.H();
        H.C().setValue(editable == null ? null : editable.toString());
    }
}
