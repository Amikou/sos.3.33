package net.safemoon.androidwallet.fragments;

import java.util.List;
import kotlin.jvm.internal.Lambda;
import net.safemoon.androidwallet.model.wallets.Wallet;

/* compiled from: SwitchWalletFragment.kt */
/* loaded from: classes2.dex */
public final class SwitchWalletFragment$makeBlobFromWallets$1 extends Lambda implements tc1<List<? extends Wallet>, te4> {
    public final /* synthetic */ StringBuilder $blobBuilder;
    public final /* synthetic */ Wallet $selected;
    public final /* synthetic */ SwitchWalletFragment this$0;

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public SwitchWalletFragment$makeBlobFromWallets$1(Wallet wallet2, SwitchWalletFragment switchWalletFragment, StringBuilder sb) {
        super(1);
        this.$selected = wallet2;
        this.this$0 = switchWalletFragment;
        this.$blobBuilder = sb;
    }

    @Override // defpackage.tc1
    public /* bridge */ /* synthetic */ te4 invoke(List<? extends Wallet> list) {
        invoke2((List<Wallet>) list);
        return te4.a;
    }

    /* JADX WARN: Removed duplicated region for block: B:22:0x0071  */
    /* JADX WARN: Removed duplicated region for block: B:25:0x0084  */
    /* JADX WARN: Removed duplicated region for block: B:53:0x00fb  */
    /* JADX WARN: Removed duplicated region for block: B:56:0x010e  */
    /* renamed from: invoke  reason: avoid collision after fix types in other method */
    /*
        Code decompiled incorrectly, please refer to instructions dump.
        To view partially-correct code enable 'Show inconsistent code' option in preferences
    */
    public final void invoke2(java.util.List<net.safemoon.androidwallet.model.wallets.Wallet> r12) {
        /*
            Method dump skipped, instructions count: 318
            To view this dump change 'Code comments level' option to 'DEBUG'
        */
        throw new UnsupportedOperationException("Method not decompiled: net.safemoon.androidwallet.fragments.SwitchWalletFragment$makeBlobFromWallets$1.invoke2(java.util.List):void");
    }
}
