package net.safemoon.androidwallet.viewmodels;

import android.app.Application;
import defpackage.st1;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.List;
import net.safemoon.androidwallet.common.TokenType;
import net.safemoon.androidwallet.model.ReceiptStatus;
import net.safemoon.androidwallet.model.request.RequestTransaction;
import net.safemoon.androidwallet.model.token.abstraction.IToken;
import net.safemoon.androidwallet.model.transaction.history.Result;
import net.safemoon.androidwallet.viewmodels.blockChainClass.MyWeb3;
import net.safemoon.androidwallet.viewmodels.blockChainClass.Transfer;

/* compiled from: TransferViewModel.kt */
/* loaded from: classes2.dex */
public final class TransferViewModel extends gd {
    public final gb2<List<IToken>> b;
    public final gb2<ReceiptStatus> c;
    public Transfer d;
    public final gb2<a> e;
    public final gb2<b> f;
    public st1 g;

    /* compiled from: TransferViewModel.kt */
    /* loaded from: classes2.dex */
    public static final class a {
        public final boolean a;
        public final Double b;

        public a(boolean z, Double d) {
            this.a = z;
            this.b = d;
        }

        public final Double a() {
            return this.b;
        }

        public final boolean b() {
            return this.a;
        }

        public boolean equals(Object obj) {
            if (this == obj) {
                return true;
            }
            if (obj instanceof a) {
                a aVar = (a) obj;
                return this.a == aVar.a && fs1.b(this.b, aVar.b);
            }
            return false;
        }

        /* JADX WARN: Multi-variable type inference failed */
        /* JADX WARN: Type inference failed for: r0v1, types: [int] */
        /* JADX WARN: Type inference failed for: r0v4 */
        /* JADX WARN: Type inference failed for: r0v5 */
        public int hashCode() {
            boolean z = this.a;
            ?? r0 = z;
            if (z) {
                r0 = 1;
            }
            int i = r0 * 31;
            Double d = this.b;
            return i + (d == null ? 0 : d.hashCode());
        }

        public String toString() {
            return "GasFee(hasSuccess=" + this.a + ", fee=" + this.b + ')';
        }
    }

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public TransferViewModel(Application application) {
        super(application);
        fs1.f(application, "application");
        this.b = new gb2<>(b20.g());
        this.c = new gb2<>();
        this.e = new gb2<>();
        this.f = new gb2<>();
    }

    public final void c(TokenType tokenType) {
        fs1.f(tokenType, "tokenType");
        as.b(qg1.a, null, null, new TransferViewModel$fetchAllTokens$1(this, tokenType, null), 3, null);
    }

    public final gb2<List<IToken>> d() {
        return this.b;
    }

    public final void e(IToken iToken, String str, double d) {
        st1 b2;
        fs1.f(iToken, "iToken");
        fs1.f(str, "receiver");
        Application a2 = a();
        fs1.e(a2, "getApplication()");
        this.d = new Transfer(a2, iToken, str, d);
        st1 st1Var = this.g;
        if (st1Var != null) {
            st1.a.a(st1Var, null, 1, null);
        }
        b2 = as.b(qg1.a, null, null, new TransferViewModel$getEstimateGas$1(this, null), 3, null);
        this.g = b2;
    }

    public final gb2<a> f() {
        return this.e;
    }

    public final gb2<ReceiptStatus> g() {
        return this.c;
    }

    public final Result h(IToken iToken) {
        String b2;
        BigInteger E;
        BigDecimal scale;
        BigInteger l;
        BigDecimal scale2;
        BigInteger y;
        BigDecimal scale3;
        fs1.f(iToken, "iToken");
        b value = this.f.getValue();
        String str = null;
        if (value == null || (b2 = value.b()) == null) {
            return null;
        }
        Result result = new Result();
        result.timeStamp = String.valueOf(b30.a.p());
        result.hash = b2;
        Transfer j = j();
        fs1.d(j);
        result.from = j.d();
        result.contractAddress = iToken.getContractAddress();
        Transfer j2 = j();
        fs1.d(j2);
        result.to = j2.D();
        Transfer j3 = j();
        result.value = (j3 == null || (E = j3.E()) == null || (scale = new BigDecimal(E).setScale(0)) == null) ? null : e30.h0(scale, 0, 1, null);
        result.tokenName = iToken.getName();
        result.tokenSymbol = iToken.getSymbol();
        result.tokenDecimal = Integer.valueOf(iToken.getDecimals());
        Transfer j4 = j();
        result.gasPrice = (j4 == null || (l = MyWeb3.l(j4, null, 1, null)) == null || (scale2 = new BigDecimal(l).setScale(0)) == null) ? null : e30.h0(scale2, 0, 1, null);
        Transfer j5 = j();
        if (j5 != null && (y = j5.y()) != null && (scale3 = new BigDecimal(y).setScale(0)) != null) {
            str = e30.h0(scale3, 0, 1, null);
        }
        result.gasUsed = str;
        result.input = iToken.getContractAddress().length() == 0 ? "0x" : "00000";
        return result;
    }

    public final void i(String str, TokenType tokenType) {
        fs1.f(str, "hash");
        fs1.f(tokenType, "tokenType");
        as.b(ej4.a(this), null, null, new TransferViewModel$getTransactionStatus$1(tokenType, str, this, null), 3, null);
    }

    public final Transfer j() {
        return this.d;
    }

    public final gb2<b> k() {
        return this.f;
    }

    public final RequestTransaction l(IToken iToken, String str) {
        BigInteger y;
        BigDecimal scale;
        fs1.f(iToken, "iToken");
        String str2 = null;
        if (str == null) {
            return null;
        }
        Transfer j = j();
        fs1.d(j);
        String d = j.d();
        fs1.e(d, "transfer!!.address");
        Transfer j2 = j();
        fs1.d(j2);
        String D = j2.D();
        Transfer j3 = j();
        Double valueOf = j3 == null ? null : Double.valueOf(j3.v());
        fs1.d(valueOf);
        double doubleValue = valueOf.doubleValue();
        String symbol = iToken.getSymbol();
        Transfer j4 = j();
        Double valueOf2 = j4 == null ? null : Double.valueOf(j4.m());
        fs1.d(valueOf2);
        RequestTransaction requestTransaction = new RequestTransaction(str, d, D, doubleValue, symbol, (long) valueOf2.doubleValue(), TokenType.Companion.b(iToken.getChainId()).getOptionalName(), b30.a.h());
        Transfer j5 = j();
        if (j5 != null && (y = j5.y()) != null && (scale = new BigDecimal(y).setScale(0)) != null) {
            str2 = e30.h0(scale, 0, 1, null);
        }
        requestTransaction.setGasUsed(str2);
        return requestTransaction;
    }

    public final void m(IToken iToken, tc1<? super Double, te4> tc1Var) {
        fs1.f(iToken, "iToken");
        fs1.f(tc1Var, "errorCallBack");
        as.b(qg1.a, null, null, new TransferViewModel$sendTransfer$1(this, iToken, tc1Var, null), 3, null);
    }

    /* JADX WARN: Removed duplicated region for block: B:10:0x002d  */
    /* JADX WARN: Removed duplicated region for block: B:23:0x0055  */
    /* JADX WARN: Removed duplicated region for block: B:43:0x00b0  */
    /* JADX WARN: Removed duplicated region for block: B:44:0x00b2  */
    /* JADX WARN: Removed duplicated region for block: B:46:0x00b5 A[Catch: Exception -> 0x0183, TryCatch #0 {Exception -> 0x0183, blocks: (B:13:0x0033, B:60:0x0164, B:62:0x016c, B:64:0x0171, B:65:0x0176, B:18:0x0044, B:49:0x010e, B:51:0x0112, B:53:0x0118, B:55:0x0139, B:57:0x0142, B:56:0x013e, B:66:0x0177, B:67:0x017c, B:21:0x0051, B:31:0x0091, B:41:0x00a7, B:46:0x00b5, B:68:0x017d, B:69:0x0182, B:34:0x0097, B:37:0x00a0, B:40:0x00a5, B:24:0x0058, B:27:0x0083), top: B:72:0x002b }] */
    /* JADX WARN: Removed duplicated region for block: B:62:0x016c A[Catch: Exception -> 0x0183, TryCatch #0 {Exception -> 0x0183, blocks: (B:13:0x0033, B:60:0x0164, B:62:0x016c, B:64:0x0171, B:65:0x0176, B:18:0x0044, B:49:0x010e, B:51:0x0112, B:53:0x0118, B:55:0x0139, B:57:0x0142, B:56:0x013e, B:66:0x0177, B:67:0x017c, B:21:0x0051, B:31:0x0091, B:41:0x00a7, B:46:0x00b5, B:68:0x017d, B:69:0x0182, B:34:0x0097, B:37:0x00a0, B:40:0x00a5, B:24:0x0058, B:27:0x0083), top: B:72:0x002b }] */
    /* JADX WARN: Removed duplicated region for block: B:64:0x0171 A[Catch: Exception -> 0x0183, TryCatch #0 {Exception -> 0x0183, blocks: (B:13:0x0033, B:60:0x0164, B:62:0x016c, B:64:0x0171, B:65:0x0176, B:18:0x0044, B:49:0x010e, B:51:0x0112, B:53:0x0118, B:55:0x0139, B:57:0x0142, B:56:0x013e, B:66:0x0177, B:67:0x017c, B:21:0x0051, B:31:0x0091, B:41:0x00a7, B:46:0x00b5, B:68:0x017d, B:69:0x0182, B:34:0x0097, B:37:0x00a0, B:40:0x00a5, B:24:0x0058, B:27:0x0083), top: B:72:0x002b }] */
    /* JADX WARN: Removed duplicated region for block: B:68:0x017d A[Catch: Exception -> 0x0183, TryCatch #0 {Exception -> 0x0183, blocks: (B:13:0x0033, B:60:0x0164, B:62:0x016c, B:64:0x0171, B:65:0x0176, B:18:0x0044, B:49:0x010e, B:51:0x0112, B:53:0x0118, B:55:0x0139, B:57:0x0142, B:56:0x013e, B:66:0x0177, B:67:0x017c, B:21:0x0051, B:31:0x0091, B:41:0x00a7, B:46:0x00b5, B:68:0x017d, B:69:0x0182, B:34:0x0097, B:37:0x00a0, B:40:0x00a5, B:24:0x0058, B:27:0x0083), top: B:72:0x002b }] */
    /*
        Code decompiled incorrectly, please refer to instructions dump.
        To view partially-correct code enable 'Show inconsistent code' option in preferences
    */
    public final java.lang.Object n(net.safemoon.androidwallet.model.token.abstraction.IToken r34, net.safemoon.androidwallet.viewmodels.TransferViewModel.b r35, defpackage.q70<? super java.lang.Boolean> r36) {
        /*
            Method dump skipped, instructions count: 392
            To view this dump change 'Code comments level' option to 'DEBUG'
        */
        throw new UnsupportedOperationException("Method not decompiled: net.safemoon.androidwallet.viewmodels.TransferViewModel.n(net.safemoon.androidwallet.model.token.abstraction.IToken, net.safemoon.androidwallet.viewmodels.TransferViewModel$b, q70):java.lang.Object");
    }

    /* compiled from: TransferViewModel.kt */
    /* loaded from: classes2.dex */
    public static final class b {
        public final boolean a;
        public final String b;
        public Boolean c;

        public b(boolean z, String str, Boolean bool) {
            this.a = z;
            this.b = str;
            this.c = bool;
        }

        public final boolean a() {
            return this.a;
        }

        public final String b() {
            return this.b;
        }

        public final Boolean c() {
            return this.c;
        }

        public final void d(Boolean bool) {
            this.c = bool;
        }

        public boolean equals(Object obj) {
            if (this == obj) {
                return true;
            }
            if (obj instanceof b) {
                b bVar = (b) obj;
                return this.a == bVar.a && fs1.b(this.b, bVar.b) && fs1.b(this.c, bVar.c);
            }
            return false;
        }

        /* JADX WARN: Multi-variable type inference failed */
        /* JADX WARN: Type inference failed for: r0v1, types: [int] */
        /* JADX WARN: Type inference failed for: r0v6 */
        /* JADX WARN: Type inference failed for: r0v7 */
        public int hashCode() {
            boolean z = this.a;
            ?? r0 = z;
            if (z) {
                r0 = 1;
            }
            int i = r0 * 31;
            String str = this.b;
            int hashCode = (i + (str == null ? 0 : str.hashCode())) * 31;
            Boolean bool = this.c;
            return hashCode + (bool != null ? bool.hashCode() : 0);
        }

        public String toString() {
            return "TransferResponse(hasSuccess=" + this.a + ", transactionReceipt=" + ((Object) this.b) + ", isSubmittedToServer=" + this.c + ')';
        }

        public /* synthetic */ b(boolean z, String str, Boolean bool, int i, qi0 qi0Var) {
            this(z, str, (i & 4) != 0 ? null : bool);
        }
    }
}
