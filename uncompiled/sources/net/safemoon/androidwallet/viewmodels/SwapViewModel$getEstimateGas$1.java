package net.safemoon.androidwallet.viewmodels;

import kotlin.coroutines.jvm.internal.SuspendLambda;
import kotlin.coroutines.jvm.internal.a;
import kotlinx.coroutines.CoroutineDispatcher;
import net.safemoon.androidwallet.viewmodels.SwapViewModel;

/* compiled from: SwapViewModel.kt */
@a(c = "net.safemoon.androidwallet.viewmodels.SwapViewModel$getEstimateGas$1", f = "SwapViewModel.kt", l = {539}, m = "invokeSuspend")
/* loaded from: classes2.dex */
public final class SwapViewModel$getEstimateGas$1 extends SuspendLambda implements hd1<c90, q70<? super te4>, Object> {
    public int label;
    public final /* synthetic */ SwapViewModel this$0;

    /* compiled from: SwapViewModel.kt */
    @kotlin.coroutines.jvm.internal.a(c = "net.safemoon.androidwallet.viewmodels.SwapViewModel$getEstimateGas$1$1", f = "SwapViewModel.kt", l = {2166}, m = "invokeSuspend")
    /* renamed from: net.safemoon.androidwallet.viewmodels.SwapViewModel$getEstimateGas$1$1  reason: invalid class name */
    /* loaded from: classes2.dex */
    public static final class AnonymousClass1 extends SuspendLambda implements hd1<c90, q70<? super te4>, Object> {
        public int label;
        public final /* synthetic */ SwapViewModel this$0;

        /* compiled from: SwapViewModel.kt */
        @kotlin.coroutines.jvm.internal.a(c = "net.safemoon.androidwallet.viewmodels.SwapViewModel$getEstimateGas$1$1$1", f = "SwapViewModel.kt", l = {544, 597, 609, 616, 620, 658, 665, 671, 727, 744, 751, 755, 798, 805, 811, 870, 887, 894, 898, 936, 943, 949}, m = "invokeSuspend")
        /* renamed from: net.safemoon.androidwallet.viewmodels.SwapViewModel$getEstimateGas$1$1$1  reason: invalid class name and collision with other inner class name */
        /* loaded from: classes2.dex */
        public static final class C02431 extends SuspendLambda implements hd1<k71<? super SwapViewModel.d>, q70<? super te4>, Object> {
            private /* synthetic */ Object L$0;
            public Object L$1;
            public Object L$2;
            public Object L$3;
            public int label;
            public final /* synthetic */ SwapViewModel this$0;

            /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
            public C02431(SwapViewModel swapViewModel, q70<? super C02431> q70Var) {
                super(2, q70Var);
                this.this$0 = swapViewModel;
            }

            @Override // kotlin.coroutines.jvm.internal.BaseContinuationImpl
            public final q70<te4> create(Object obj, q70<?> q70Var) {
                C02431 c02431 = new C02431(this.this$0, q70Var);
                c02431.L$0 = obj;
                return c02431;
            }

            @Override // defpackage.hd1
            public final Object invoke(k71<? super SwapViewModel.d> k71Var, q70<? super te4> q70Var) {
                return ((C02431) create(k71Var, q70Var)).invokeSuspend(te4.a);
            }

            /* JADX WARN: Removed duplicated region for block: B:112:0x063b A[RETURN] */
            /* JADX WARN: Removed duplicated region for block: B:156:0x086b A[RETURN] */
            /* JADX WARN: Removed duplicated region for block: B:189:0x09f9 A[RETURN] */
            /* JADX WARN: Removed duplicated region for block: B:192:0x0a11  */
            /* JADX WARN: Removed duplicated region for block: B:195:0x0a1e  */
            /* JADX WARN: Removed duplicated region for block: B:229:0x0bf2 A[RETURN] */
            /* JADX WARN: Removed duplicated region for block: B:230:0x0bf3  */
            /* JADX WARN: Removed duplicated region for block: B:255:0x0d44 A[RETURN] */
            /* JADX WARN: Removed duplicated region for block: B:29:0x0266  */
            /* JADX WARN: Removed duplicated region for block: B:78:0x04a7 A[RETURN] */
            @Override // kotlin.coroutines.jvm.internal.BaseContinuationImpl
            /*
                Code decompiled incorrectly, please refer to instructions dump.
                To view partially-correct code enable 'Show inconsistent code' option in preferences
            */
            public final java.lang.Object invokeSuspend(java.lang.Object r31) {
                /*
                    Method dump skipped, instructions count: 3450
                    To view this dump change 'Code comments level' option to 'DEBUG'
                */
                throw new UnsupportedOperationException("Method not decompiled: net.safemoon.androidwallet.viewmodels.SwapViewModel$getEstimateGas$1.AnonymousClass1.C02431.invokeSuspend(java.lang.Object):java.lang.Object");
            }
        }

        /* compiled from: SwapViewModel.kt */
        @kotlin.coroutines.jvm.internal.a(c = "net.safemoon.androidwallet.viewmodels.SwapViewModel$getEstimateGas$1$1$2", f = "SwapViewModel.kt", l = {}, m = "invokeSuspend")
        /* renamed from: net.safemoon.androidwallet.viewmodels.SwapViewModel$getEstimateGas$1$1$2  reason: invalid class name */
        /* loaded from: classes2.dex */
        public static final class AnonymousClass2 extends SuspendLambda implements kd1<k71<? super SwapViewModel.d>, Throwable, q70<? super te4>, Object> {
            public /* synthetic */ Object L$0;
            public int label;
            public final /* synthetic */ SwapViewModel this$0;

            /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
            public AnonymousClass2(SwapViewModel swapViewModel, q70<? super AnonymousClass2> q70Var) {
                super(3, q70Var);
                this.this$0 = swapViewModel;
            }

            @Override // defpackage.kd1
            public final Object invoke(k71<? super SwapViewModel.d> k71Var, Throwable th, q70<? super te4> q70Var) {
                AnonymousClass2 anonymousClass2 = new AnonymousClass2(this.this$0, q70Var);
                anonymousClass2.L$0 = th;
                return anonymousClass2.invokeSuspend(te4.a);
            }

            @Override // kotlin.coroutines.jvm.internal.BaseContinuationImpl
            public final Object invokeSuspend(Object obj) {
                gs1.d();
                if (this.label == 0) {
                    o83.b(obj);
                    this.this$0.K0().postValue(null);
                    this.this$0.J0().postValue(((Throwable) this.L$0).getMessage());
                    return te4.a;
                }
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
        }

        /* compiled from: Collect.kt */
        /* renamed from: net.safemoon.androidwallet.viewmodels.SwapViewModel$getEstimateGas$1$1$a */
        /* loaded from: classes2.dex */
        public static final class a implements k71<SwapViewModel.d> {
            public final /* synthetic */ SwapViewModel a;

            public a(SwapViewModel swapViewModel) {
                this.a = swapViewModel;
            }

            @Override // defpackage.k71
            public Object emit(SwapViewModel.d dVar, q70<? super te4> q70Var) {
                st1 b;
                b = as.b(ej4.a(this.a), null, null, new SwapViewModel$getEstimateGas$1$1$3$1(dVar, this.a, null), 3, null);
                return b == gs1.d() ? b : te4.a;
            }
        }

        /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
        public AnonymousClass1(SwapViewModel swapViewModel, q70<? super AnonymousClass1> q70Var) {
            super(2, q70Var);
            this.this$0 = swapViewModel;
        }

        @Override // kotlin.coroutines.jvm.internal.BaseContinuationImpl
        public final q70<te4> create(Object obj, q70<?> q70Var) {
            return new AnonymousClass1(this.this$0, q70Var);
        }

        @Override // defpackage.hd1
        public final Object invoke(c90 c90Var, q70<? super te4> q70Var) {
            return ((AnonymousClass1) create(c90Var, q70Var)).invokeSuspend(te4.a);
        }

        @Override // kotlin.coroutines.jvm.internal.BaseContinuationImpl
        public final Object invokeSuspend(Object obj) {
            Object d = gs1.d();
            int i = this.label;
            if (i == 0) {
                o83.b(obj);
                j71 c = n71.c(n71.n(new C02431(this.this$0, null)), new AnonymousClass2(this.this$0, null));
                a aVar = new a(this.this$0);
                this.label = 1;
                if (c.a(aVar, this) == d) {
                    return d;
                }
            } else if (i != 1) {
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            } else {
                o83.b(obj);
            }
            return te4.a;
        }
    }

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public SwapViewModel$getEstimateGas$1(SwapViewModel swapViewModel, q70<? super SwapViewModel$getEstimateGas$1> q70Var) {
        super(2, q70Var);
        this.this$0 = swapViewModel;
    }

    @Override // kotlin.coroutines.jvm.internal.BaseContinuationImpl
    public final q70<te4> create(Object obj, q70<?> q70Var) {
        return new SwapViewModel$getEstimateGas$1(this.this$0, q70Var);
    }

    @Override // defpackage.hd1
    public final Object invoke(c90 c90Var, q70<? super te4> q70Var) {
        return ((SwapViewModel$getEstimateGas$1) create(c90Var, q70Var)).invokeSuspend(te4.a);
    }

    @Override // kotlin.coroutines.jvm.internal.BaseContinuationImpl
    public final Object invokeSuspend(Object obj) {
        Object d = gs1.d();
        int i = this.label;
        if (i == 0) {
            o83.b(obj);
            CoroutineDispatcher b = tp0.b();
            AnonymousClass1 anonymousClass1 = new AnonymousClass1(this.this$0, null);
            this.label = 1;
            if (kotlinx.coroutines.a.e(b, anonymousClass1, this) == d) {
                return d;
            }
        } else if (i != 1) {
            throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        } else {
            o83.b(obj);
        }
        return te4.a;
    }
}
