package net.safemoon.androidwallet.viewmodels;

import java.util.List;
import kotlin.coroutines.jvm.internal.SuspendLambda;
import kotlin.coroutines.jvm.internal.a;
import net.safemoon.androidwallet.model.priceAlert.PAToken;
import net.safemoon.androidwallet.model.priceAlert.PriceAlertToken;

/* compiled from: CryptoPriceAlertViewModel.kt */
@a(c = "net.safemoon.androidwallet.viewmodels.CryptoPriceAlertViewModel$addBlankPriceAlert$1", f = "CryptoPriceAlertViewModel.kt", l = {89}, m = "invokeSuspend")
/* loaded from: classes2.dex */
public final class CryptoPriceAlertViewModel$addBlankPriceAlert$1 extends SuspendLambda implements hd1<c90, q70<? super te4>, Object> {
    public final /* synthetic */ PAToken $paToken;
    public int label;
    public final /* synthetic */ CryptoPriceAlertViewModel this$0;

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public CryptoPriceAlertViewModel$addBlankPriceAlert$1(CryptoPriceAlertViewModel cryptoPriceAlertViewModel, PAToken pAToken, q70<? super CryptoPriceAlertViewModel$addBlankPriceAlert$1> q70Var) {
        super(2, q70Var);
        this.this$0 = cryptoPriceAlertViewModel;
        this.$paToken = pAToken;
    }

    @Override // kotlin.coroutines.jvm.internal.BaseContinuationImpl
    public final q70<te4> create(Object obj, q70<?> q70Var) {
        return new CryptoPriceAlertViewModel$addBlankPriceAlert$1(this.this$0, this.$paToken, q70Var);
    }

    @Override // defpackage.hd1
    public final Object invoke(c90 c90Var, q70<? super te4> q70Var) {
        return ((CryptoPriceAlertViewModel$addBlankPriceAlert$1) create(c90Var, q70Var)).invokeSuspend(te4.a);
    }

    @Override // kotlin.coroutines.jvm.internal.BaseContinuationImpl
    public final Object invokeSuspend(Object obj) {
        String str;
        Object d = gs1.d();
        int i = this.label;
        if (i == 0) {
            o83.b(obj);
            this.this$0.x();
            String symbol = this.$paToken.getSymbol();
            if (symbol == null) {
                symbol = "";
            }
            String str2 = symbol;
            String name = this.$paToken.getName();
            String contractAddress = this.$paToken.getContractAddress();
            if (contractAddress == null) {
                str = null;
            } else {
                if (contractAddress.length() == 0) {
                    contractAddress = null;
                }
                str = contractAddress;
            }
            int chainId = this.$paToken.getChainId();
            CryptoPriceAlertViewModel cryptoPriceAlertViewModel = this.this$0;
            Integer cmcId = this.$paToken.getCmcId();
            this.label = 1;
            obj = cryptoPriceAlertViewModel.q(str2, name, str, chainId, cmcId, this);
            if (obj == d) {
                return d;
            }
        } else if (i != 1) {
            throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        } else {
            o83.b(obj);
        }
        PriceAlertToken priceAlertToken = (PriceAlertToken) obj;
        if (priceAlertToken != null) {
            CryptoPriceAlertViewModel cryptoPriceAlertViewModel2 = this.this$0;
            List<PriceAlertToken> value = cryptoPriceAlertViewModel2.r().getValue();
            if (value == null) {
                value = b20.g();
            }
            List<PriceAlertToken> m0 = j20.m0(value);
            m0.add(priceAlertToken);
            cryptoPriceAlertViewModel2.r().postValue(m0);
            cryptoPriceAlertViewModel2.t().postValue(hr.d(m0.size() - 1));
        }
        this.this$0.y();
        return te4.a;
    }
}
