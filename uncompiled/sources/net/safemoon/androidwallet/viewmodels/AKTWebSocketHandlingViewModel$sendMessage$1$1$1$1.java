package net.safemoon.androidwallet.viewmodels;

import kotlin.jvm.internal.Lambda;

/* compiled from: AKTWebSocketHandlingViewModel.kt */
/* loaded from: classes2.dex */
public final class AKTWebSocketHandlingViewModel$sendMessage$1$1$1$1 extends Lambda implements tc1<bm3, te4> {
    public final /* synthetic */ String $LSON;
    public final /* synthetic */ mo4 $webClient;
    public final /* synthetic */ AKTWebSocketHandlingViewModel this$0;

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public AKTWebSocketHandlingViewModel$sendMessage$1$1$1$1(AKTWebSocketHandlingViewModel aKTWebSocketHandlingViewModel, mo4 mo4Var, String str) {
        super(1);
        this.this$0 = aKTWebSocketHandlingViewModel;
        this.$webClient = mo4Var;
        this.$LSON = str;
    }

    @Override // defpackage.tc1
    public /* bridge */ /* synthetic */ te4 invoke(bm3 bm3Var) {
        invoke2(bm3Var);
        return te4.a;
    }

    /* renamed from: invoke  reason: avoid collision after fix types in other method */
    public final void invoke2(bm3 bm3Var) {
        this.this$0.b = null;
        this.$webClient.send(this.$LSON);
    }
}
