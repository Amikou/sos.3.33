package net.safemoon.androidwallet.viewmodels;

import android.app.Application;
import java.util.List;
import net.safemoon.androidwallet.model.Coin;
import net.safemoon.androidwallet.repository.TokenListDataSource;
import net.safemoon.androidwallet.viewmodels.CMCListViewModel;

/* compiled from: CMCListViewModel.kt */
/* loaded from: classes2.dex */
public final class CMCListViewModel extends gd {
    public final sy1 b;
    public final sy1 c;
    public final gb2<Boolean> d;
    public final gb2<String> e;
    public final gb2<List<Integer>> f;
    public final gb2<List<Coin>> g;
    public final g72<List<d11>> h;

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public CMCListViewModel(Application application) {
        super(application);
        fs1.f(application, "application");
        this.b = zy1.a(CMCListViewModel$marketClient$2.INSTANCE);
        this.c = zy1.a(new CMCListViewModel$tokenListDataSource$2(application));
        this.d = new gb2<>(Boolean.TRUE);
        this.e = new gb2<>("");
        this.f = new gb2<>(b20.g());
        gb2<List<Coin>> gb2Var = new gb2<>(b20.g());
        this.g = gb2Var;
        final g72<List<d11>> g72Var = new g72<>();
        g72Var.a(q(), new tl2() { // from class: ot
            @Override // defpackage.tl2
            public final void onChanged(Object obj) {
                CMCListViewModel.g(CMCListViewModel.this, g72Var, (List) obj);
            }
        });
        g72Var.a(gb2Var, new tl2() { // from class: pt
            @Override // defpackage.tl2
            public final void onChanged(Object obj) {
                CMCListViewModel.h(CMCListViewModel.this, g72Var, (List) obj);
            }
        });
        g72Var.a(p(), new tl2() { // from class: nt
            @Override // defpackage.tl2
            public final void onChanged(Object obj) {
                CMCListViewModel.i(CMCListViewModel.this, g72Var, (String) obj);
            }
        });
        j(this, g72Var);
        te4 te4Var = te4.a;
        this.h = g72Var;
    }

    public static final void g(CMCListViewModel cMCListViewModel, g72 g72Var, List list) {
        fs1.f(cMCListViewModel, "this$0");
        fs1.f(g72Var, "$this_apply");
        j(cMCListViewModel, g72Var);
    }

    public static final void h(CMCListViewModel cMCListViewModel, g72 g72Var, List list) {
        fs1.f(cMCListViewModel, "this$0");
        fs1.f(g72Var, "$this_apply");
        j(cMCListViewModel, g72Var);
    }

    public static final void i(CMCListViewModel cMCListViewModel, g72 g72Var, String str) {
        fs1.f(cMCListViewModel, "this$0");
        fs1.f(g72Var, "$this_apply");
        j(cMCListViewModel, g72Var);
    }

    /* JADX WARN: Removed duplicated region for block: B:28:0x0091  */
    /* JADX WARN: Removed duplicated region for block: B:41:0x00b1 A[SYNTHETIC] */
    /* JADX WARN: Removed duplicated region for block: B:43:0x005e A[SYNTHETIC] */
    /*
        Code decompiled incorrectly, please refer to instructions dump.
        To view partially-correct code enable 'Show inconsistent code' option in preferences
    */
    public static final void j(net.safemoon.androidwallet.viewmodels.CMCListViewModel r11, defpackage.g72<java.util.List<defpackage.d11>> r12) {
        /*
            gb2<java.lang.String> r0 = r11.e
            java.lang.Object r0 = r0.getValue()
            java.lang.String r0 = (java.lang.String) r0
            if (r0 != 0) goto Lc
            java.lang.String r0 = ""
        Lc:
            gb2<java.util.List<net.safemoon.androidwallet.model.Coin>> r1 = r11.g
            java.lang.Object r1 = r1.getValue()
            java.util.List r1 = (java.util.List) r1
            r2 = 0
            if (r1 != 0) goto L19
            goto Lb6
        L19:
            java.util.ArrayList r3 = new java.util.ArrayList
            r4 = 10
            int r4 = defpackage.c20.q(r1, r4)
            r3.<init>(r4)
            java.util.Iterator r1 = r1.iterator()
        L28:
            boolean r4 = r1.hasNext()
            if (r4 == 0) goto L55
            java.lang.Object r4 = r1.next()
            net.safemoon.androidwallet.model.Coin r4 = (net.safemoon.androidwallet.model.Coin) r4
            d11 r5 = new d11
            gb2 r6 = r11.q()
            java.lang.Object r6 = r6.getValue()
            java.util.List r6 = (java.util.List) r6
            if (r6 != 0) goto L46
            java.util.List r6 = defpackage.b20.g()
        L46:
            java.lang.Integer r7 = r4.getId()
            boolean r6 = r6.contains(r7)
            r5.<init>(r4, r6)
            r3.add(r5)
            goto L28
        L55:
            java.util.ArrayList r11 = new java.util.ArrayList
            r11.<init>()
            java.util.Iterator r1 = r3.iterator()
        L5e:
            boolean r3 = r1.hasNext()
            if (r3 == 0) goto Lb5
            java.lang.Object r3 = r1.next()
            r4 = r3
            d11 r4 = (defpackage.d11) r4
            net.safemoon.androidwallet.model.Coin r5 = r4.c()
            java.lang.String r5 = r5.getSymbol()
            r6 = 2
            java.lang.String r7 = "(this as java.lang.Strin….toLowerCase(Locale.ROOT)"
            r8 = 1
            r9 = 0
            if (r5 != 0) goto L7c
        L7a:
            r5 = r9
            goto L8f
        L7c:
            java.util.Locale r10 = java.util.Locale.ROOT
            java.lang.String r5 = r5.toLowerCase(r10)
            defpackage.fs1.e(r5, r7)
            if (r5 != 0) goto L88
            goto L7a
        L88:
            boolean r5 = kotlin.text.StringsKt__StringsKt.M(r5, r0, r9, r6, r2)
            if (r5 != r8) goto L7a
            r5 = r8
        L8f:
            if (r5 != 0) goto Laf
            net.safemoon.androidwallet.model.Coin r4 = r4.c()
            java.lang.String r4 = r4.getName()
            java.lang.String r5 = "it.coin.name"
            defpackage.fs1.e(r4, r5)
            java.util.Locale r5 = java.util.Locale.ROOT
            java.lang.String r4 = r4.toLowerCase(r5)
            defpackage.fs1.e(r4, r7)
            boolean r4 = kotlin.text.StringsKt__StringsKt.M(r4, r0, r9, r6, r2)
            if (r4 == 0) goto Lae
            goto Laf
        Lae:
            r8 = r9
        Laf:
            if (r8 == 0) goto L5e
            r11.add(r3)
            goto L5e
        Lb5:
            r2 = r11
        Lb6:
            r12.postValue(r2)
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: net.safemoon.androidwallet.viewmodels.CMCListViewModel.j(net.safemoon.androidwallet.viewmodels.CMCListViewModel, g72):void");
    }

    public final void k() {
        as.b(qg1.a, tp0.b(), null, new CMCListViewModel$fetchCmcToken$1(this, null), 2, null);
    }

    /* JADX WARN: Code restructure failed: missing block: B:33:0x0085, code lost:
        if (r11 != null) goto L78;
     */
    /* JADX WARN: Removed duplicated region for block: B:10:0x0025  */
    /* JADX WARN: Removed duplicated region for block: B:20:0x0042  */
    /* JADX WARN: Removed duplicated region for block: B:31:0x007f  */
    /* JADX WARN: Removed duplicated region for block: B:32:0x0081 A[Catch: Exception -> 0x013b, TryCatch #0 {Exception -> 0x013b, blocks: (B:13:0x002d, B:41:0x00af, B:42:0x00ba, B:44:0x00c0, B:52:0x00df, B:47:0x00ce, B:50:0x00d9, B:55:0x00e9, B:56:0x00f2, B:58:0x00f8, B:60:0x0104, B:75:0x0132, B:77:0x0136, B:63:0x010b, B:64:0x010f, B:66:0x0115, B:69:0x0126, B:18:0x003e, B:25:0x0069, B:27:0x0071, B:29:0x0077, B:35:0x008b, B:37:0x009d, B:54:0x00e5, B:32:0x0081, B:34:0x0087, B:21:0x0045), top: B:82:0x0023 }] */
    /* JADX WARN: Removed duplicated region for block: B:37:0x009d A[Catch: Exception -> 0x013b, TryCatch #0 {Exception -> 0x013b, blocks: (B:13:0x002d, B:41:0x00af, B:42:0x00ba, B:44:0x00c0, B:52:0x00df, B:47:0x00ce, B:50:0x00d9, B:55:0x00e9, B:56:0x00f2, B:58:0x00f8, B:60:0x0104, B:75:0x0132, B:77:0x0136, B:63:0x010b, B:64:0x010f, B:66:0x0115, B:69:0x0126, B:18:0x003e, B:25:0x0069, B:27:0x0071, B:29:0x0077, B:35:0x008b, B:37:0x009d, B:54:0x00e5, B:32:0x0081, B:34:0x0087, B:21:0x0045), top: B:82:0x0023 }] */
    /* JADX WARN: Removed duplicated region for block: B:44:0x00c0 A[Catch: Exception -> 0x013b, TryCatch #0 {Exception -> 0x013b, blocks: (B:13:0x002d, B:41:0x00af, B:42:0x00ba, B:44:0x00c0, B:52:0x00df, B:47:0x00ce, B:50:0x00d9, B:55:0x00e9, B:56:0x00f2, B:58:0x00f8, B:60:0x0104, B:75:0x0132, B:77:0x0136, B:63:0x010b, B:64:0x010f, B:66:0x0115, B:69:0x0126, B:18:0x003e, B:25:0x0069, B:27:0x0071, B:29:0x0077, B:35:0x008b, B:37:0x009d, B:54:0x00e5, B:32:0x0081, B:34:0x0087, B:21:0x0045), top: B:82:0x0023 }] */
    /* JADX WARN: Removed duplicated region for block: B:54:0x00e5 A[Catch: Exception -> 0x013b, TryCatch #0 {Exception -> 0x013b, blocks: (B:13:0x002d, B:41:0x00af, B:42:0x00ba, B:44:0x00c0, B:52:0x00df, B:47:0x00ce, B:50:0x00d9, B:55:0x00e9, B:56:0x00f2, B:58:0x00f8, B:60:0x0104, B:75:0x0132, B:77:0x0136, B:63:0x010b, B:64:0x010f, B:66:0x0115, B:69:0x0126, B:18:0x003e, B:25:0x0069, B:27:0x0071, B:29:0x0077, B:35:0x008b, B:37:0x009d, B:54:0x00e5, B:32:0x0081, B:34:0x0087, B:21:0x0045), top: B:82:0x0023 }] */
    /* JADX WARN: Removed duplicated region for block: B:58:0x00f8 A[Catch: Exception -> 0x013b, TryCatch #0 {Exception -> 0x013b, blocks: (B:13:0x002d, B:41:0x00af, B:42:0x00ba, B:44:0x00c0, B:52:0x00df, B:47:0x00ce, B:50:0x00d9, B:55:0x00e9, B:56:0x00f2, B:58:0x00f8, B:60:0x0104, B:75:0x0132, B:77:0x0136, B:63:0x010b, B:64:0x010f, B:66:0x0115, B:69:0x0126, B:18:0x003e, B:25:0x0069, B:27:0x0071, B:29:0x0077, B:35:0x008b, B:37:0x009d, B:54:0x00e5, B:32:0x0081, B:34:0x0087, B:21:0x0045), top: B:82:0x0023 }] */
    /* JADX WARN: Removed duplicated region for block: B:84:0x00df A[SYNTHETIC] */
    /* JADX WARN: Removed duplicated region for block: B:87:0x00ba A[SYNTHETIC] */
    /* JADX WARN: Removed duplicated region for block: B:95:0x0131 A[SYNTHETIC] */
    /*
        Code decompiled incorrectly, please refer to instructions dump.
        To view partially-correct code enable 'Show inconsistent code' option in preferences
    */
    public final java.lang.Object l(defpackage.q70<? super java.util.List<? extends net.safemoon.androidwallet.model.Coin>> r11) {
        /*
            Method dump skipped, instructions count: 320
            To view this dump change 'Code comments level' option to 'DEBUG'
        */
        throw new UnsupportedOperationException("Method not decompiled: net.safemoon.androidwallet.viewmodels.CMCListViewModel.l(q70):java.lang.Object");
    }

    public final g72<List<d11>> m() {
        return this.h;
    }

    public final gb2<Boolean> n() {
        return this.d;
    }

    public final e42 o() {
        return (e42) this.b.getValue();
    }

    public final gb2<String> p() {
        return this.e;
    }

    public final gb2<List<Integer>> q() {
        return this.f;
    }

    public final TokenListDataSource r() {
        return (TokenListDataSource) this.c.getValue();
    }
}
