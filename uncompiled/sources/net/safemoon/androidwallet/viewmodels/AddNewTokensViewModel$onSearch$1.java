package net.safemoon.androidwallet.viewmodels;

import kotlin.coroutines.jvm.internal.SuspendLambda;
import kotlin.coroutines.jvm.internal.a;
import kotlinx.coroutines.CoroutineDispatcher;

/* compiled from: AddNewTokensViewModel.kt */
@a(c = "net.safemoon.androidwallet.viewmodels.AddNewTokensViewModel$onSearch$1", f = "AddNewTokensViewModel.kt", l = {92}, m = "invokeSuspend")
/* loaded from: classes2.dex */
public final class AddNewTokensViewModel$onSearch$1 extends SuspendLambda implements hd1<c90, q70<? super te4>, Object> {
    public int label;
    public final /* synthetic */ AddNewTokensViewModel this$0;

    /* compiled from: AddNewTokensViewModel.kt */
    @a(c = "net.safemoon.androidwallet.viewmodels.AddNewTokensViewModel$onSearch$1$1", f = "AddNewTokensViewModel.kt", l = {95}, m = "invokeSuspend")
    /* renamed from: net.safemoon.androidwallet.viewmodels.AddNewTokensViewModel$onSearch$1$1  reason: invalid class name */
    /* loaded from: classes2.dex */
    public static final class AnonymousClass1 extends SuspendLambda implements hd1<c90, q70<? super te4>, Object> {
        public Object L$0;
        public int label;
        public final /* synthetic */ AddNewTokensViewModel this$0;

        /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
        public AnonymousClass1(AddNewTokensViewModel addNewTokensViewModel, q70<? super AnonymousClass1> q70Var) {
            super(2, q70Var);
            this.this$0 = addNewTokensViewModel;
        }

        @Override // kotlin.coroutines.jvm.internal.BaseContinuationImpl
        public final q70<te4> create(Object obj, q70<?> q70Var) {
            return new AnonymousClass1(this.this$0, q70Var);
        }

        @Override // defpackage.hd1
        public final Object invoke(c90 c90Var, q70<? super te4> q70Var) {
            return ((AnonymousClass1) create(c90Var, q70Var)).invokeSuspend(te4.a);
        }

        /* JADX WARN: Code restructure failed: missing block: B:14:0x009a, code lost:
            if (kotlin.text.StringsKt__StringsKt.M(r9, r10, false, 2, null) != false) goto L22;
         */
        /* JADX WARN: Multi-variable type inference failed */
        /* JADX WARN: Type inference failed for: r1v2, types: [gb2] */
        @Override // kotlin.coroutines.jvm.internal.BaseContinuationImpl
        /*
            Code decompiled incorrectly, please refer to instructions dump.
            To view partially-correct code enable 'Show inconsistent code' option in preferences
        */
        public final java.lang.Object invokeSuspend(java.lang.Object r17) {
            /*
                r16 = this;
                r0 = r16
                java.lang.Object r1 = defpackage.gs1.d()
                int r2 = r0.label
                r3 = 1
                if (r2 == 0) goto L20
                if (r2 != r3) goto L18
                java.lang.Object r1 = r0.L$0
                gb2 r1 = (defpackage.gb2) r1
                defpackage.o83.b(r17)
                r3 = r17
                goto Ld6
            L18:
                java.lang.IllegalStateException r1 = new java.lang.IllegalStateException
                java.lang.String r2 = "call to 'resume' before 'invoke' with coroutine"
                r1.<init>(r2)
                throw r1
            L20:
                defpackage.o83.b(r17)
                net.safemoon.androidwallet.viewmodels.AddNewTokensViewModel r2 = r0.this$0
                gb2 r2 = r2.j()
                net.safemoon.androidwallet.viewmodels.AddNewTokensViewModel r4 = r0.this$0
                sm1 r4 = net.safemoon.androidwallet.viewmodels.AddNewTokensViewModel.e(r4)
                net.safemoon.androidwallet.viewmodels.AddNewTokensViewModel r5 = r0.this$0
                java.util.List r5 = net.safemoon.androidwallet.viewmodels.AddNewTokensViewModel.b(r5)
                net.safemoon.androidwallet.viewmodels.AddNewTokensViewModel r6 = r0.this$0
                java.util.ArrayList r7 = new java.util.ArrayList
                r7.<init>()
                java.util.Iterator r5 = r5.iterator()
            L40:
                boolean r8 = r5.hasNext()
                if (r8 == 0) goto La6
                java.lang.Object r8 = r5.next()
                r9 = r8
                net.safemoon.androidwallet.model.token.abstraction.IToken r9 = (net.safemoon.androidwallet.model.token.abstraction.IToken) r9
                java.lang.String r10 = r9.getSymbol()
                java.lang.String r11 = "null cannot be cast to non-null type java.lang.String"
                java.util.Objects.requireNonNull(r10, r11)
                java.util.Locale r12 = java.util.Locale.ROOT
                java.lang.String r10 = r10.toLowerCase(r12)
                java.lang.String r13 = "(this as java.lang.Strin….toLowerCase(Locale.ROOT)"
                defpackage.fs1.e(r10, r13)
                java.lang.String r14 = net.safemoon.androidwallet.viewmodels.AddNewTokensViewModel.d(r6)
                java.util.Objects.requireNonNull(r14, r11)
                java.lang.String r14 = r14.toLowerCase(r12)
                defpackage.fs1.e(r14, r13)
                r15 = 0
                r3 = 2
                r17 = r5
                r5 = 0
                boolean r10 = kotlin.text.StringsKt__StringsKt.M(r10, r14, r15, r3, r5)
                if (r10 != 0) goto L9c
                java.lang.String r9 = r9.getName()
                java.util.Objects.requireNonNull(r9, r11)
                java.lang.String r9 = r9.toLowerCase(r12)
                defpackage.fs1.e(r9, r13)
                java.lang.String r10 = net.safemoon.androidwallet.viewmodels.AddNewTokensViewModel.d(r6)
                java.util.Objects.requireNonNull(r10, r11)
                java.lang.String r10 = r10.toLowerCase(r12)
                defpackage.fs1.e(r10, r13)
                boolean r3 = kotlin.text.StringsKt__StringsKt.M(r9, r10, r15, r3, r5)
                if (r3 == 0) goto L9d
            L9c:
                r15 = 1
            L9d:
                if (r15 == 0) goto La2
                r7.add(r8)
            La2:
                r5 = r17
                r3 = 1
                goto L40
            La6:
                java.util.List r3 = defpackage.j20.k0(r7)
                gb2 r5 = new gb2
                r5.<init>(r3)
                net.safemoon.androidwallet.viewmodels.AddNewTokensViewModel r3 = r0.this$0
                java.lang.Object r6 = r5.getValue()
                java.util.List r6 = (java.util.List) r6
                if (r6 != 0) goto Lbe
                java.util.ArrayList r6 = new java.util.ArrayList
                r6.<init>()
            Lbe:
                net.safemoon.androidwallet.viewmodels.AddNewTokensViewModel.h(r3, r6)
                te4 r3 = defpackage.te4.a
                net.safemoon.androidwallet.viewmodels.AddNewTokensViewModel r3 = r0.this$0
                net.safemoon.androidwallet.common.TokenType r3 = net.safemoon.androidwallet.viewmodels.AddNewTokensViewModel.f(r3)
                r0.L$0 = r2
                r6 = 1
                r0.label = r6
                java.lang.Object r3 = r4.a(r5, r3, r0)
                if (r3 != r1) goto Ld5
                return r1
            Ld5:
                r1 = r2
            Ld6:
                androidx.lifecycle.LiveData r3 = (androidx.lifecycle.LiveData) r3
                java.lang.Object r2 = r3.getValue()
                r1.postValue(r2)
                te4 r1 = defpackage.te4.a
                return r1
            */
            throw new UnsupportedOperationException("Method not decompiled: net.safemoon.androidwallet.viewmodels.AddNewTokensViewModel$onSearch$1.AnonymousClass1.invokeSuspend(java.lang.Object):java.lang.Object");
        }
    }

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public AddNewTokensViewModel$onSearch$1(AddNewTokensViewModel addNewTokensViewModel, q70<? super AddNewTokensViewModel$onSearch$1> q70Var) {
        super(2, q70Var);
        this.this$0 = addNewTokensViewModel;
    }

    @Override // kotlin.coroutines.jvm.internal.BaseContinuationImpl
    public final q70<te4> create(Object obj, q70<?> q70Var) {
        return new AddNewTokensViewModel$onSearch$1(this.this$0, q70Var);
    }

    @Override // defpackage.hd1
    public final Object invoke(c90 c90Var, q70<? super te4> q70Var) {
        return ((AddNewTokensViewModel$onSearch$1) create(c90Var, q70Var)).invokeSuspend(te4.a);
    }

    @Override // kotlin.coroutines.jvm.internal.BaseContinuationImpl
    public final Object invokeSuspend(Object obj) {
        Object d = gs1.d();
        int i = this.label;
        if (i == 0) {
            o83.b(obj);
            CoroutineDispatcher b = tp0.b();
            AnonymousClass1 anonymousClass1 = new AnonymousClass1(this.this$0, null);
            this.label = 1;
            if (kotlinx.coroutines.a.e(b, anonymousClass1, this) == d) {
                return d;
            }
        } else if (i != 1) {
            throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        } else {
            o83.b(obj);
        }
        return te4.a;
    }
}
