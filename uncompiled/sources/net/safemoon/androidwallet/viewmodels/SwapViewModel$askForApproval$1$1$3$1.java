package net.safemoon.androidwallet.viewmodels;

import kotlin.coroutines.jvm.internal.SuspendLambda;
import kotlin.coroutines.jvm.internal.a;
import net.safemoon.androidwallet.model.common.LoadingState;

/* compiled from: SwapViewModel.kt */
@a(c = "net.safemoon.androidwallet.viewmodels.SwapViewModel$askForApproval$1$1$3$1", f = "SwapViewModel.kt", l = {}, m = "invokeSuspend")
/* loaded from: classes2.dex */
public final class SwapViewModel$askForApproval$1$1$3$1 extends SuspendLambda implements hd1<c90, q70<? super te4>, Object> {
    public final /* synthetic */ hx0 $it;
    public int label;
    public final /* synthetic */ SwapViewModel this$0;

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public SwapViewModel$askForApproval$1$1$3$1(hx0 hx0Var, SwapViewModel swapViewModel, q70<? super SwapViewModel$askForApproval$1$1$3$1> q70Var) {
        super(2, q70Var);
        this.$it = hx0Var;
        this.this$0 = swapViewModel;
    }

    @Override // kotlin.coroutines.jvm.internal.BaseContinuationImpl
    public final q70<te4> create(Object obj, q70<?> q70Var) {
        return new SwapViewModel$askForApproval$1$1$3$1(this.$it, this.this$0, q70Var);
    }

    @Override // defpackage.hd1
    public final Object invoke(c90 c90Var, q70<? super te4> q70Var) {
        return ((SwapViewModel$askForApproval$1$1$3$1) create(c90Var, q70Var)).invokeSuspend(te4.a);
    }

    @Override // kotlin.coroutines.jvm.internal.BaseContinuationImpl
    public final Object invokeSuspend(Object obj) {
        gs1.d();
        if (this.label == 0) {
            o83.b(obj);
            hx0 hx0Var = this.$it;
            if (hx0Var != null) {
                if (hx0Var.getError() != null) {
                    this.this$0.O().postValue(LoadingState.Normal);
                    SwapViewModel.i1(this.this$0, null, 1, null);
                } else {
                    SwapViewModel swapViewModel = this.this$0;
                    String transactionHash = this.$it.getTransactionHash();
                    fs1.e(transactionHash, "it.transactionHash");
                    swapViewModel.H(transactionHash);
                }
            } else {
                this.this$0.O().postValue(LoadingState.Normal);
            }
            return te4.a;
        }
        throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
    }
}
