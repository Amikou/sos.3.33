package net.safemoon.androidwallet.viewmodels;

import kotlin.coroutines.jvm.internal.SuspendLambda;
import kotlin.coroutines.jvm.internal.a;
import kotlinx.coroutines.CoroutineDispatcher;
import net.safemoon.androidwallet.R;
import net.safemoon.androidwallet.model.common.LoadingState;
import net.safemoon.androidwallet.viewmodels.SwapViewModel;

/* compiled from: SwapViewModel.kt */
@a(c = "net.safemoon.androidwallet.viewmodels.SwapViewModel$swapTokens$1", f = "SwapViewModel.kt", l = {1610}, m = "invokeSuspend")
/* loaded from: classes2.dex */
public final class SwapViewModel$swapTokens$1 extends SuspendLambda implements hd1<c90, q70<? super te4>, Object> {
    public int label;
    public final /* synthetic */ SwapViewModel this$0;

    /* compiled from: SwapViewModel.kt */
    @kotlin.coroutines.jvm.internal.a(c = "net.safemoon.androidwallet.viewmodels.SwapViewModel$swapTokens$1$1", f = "SwapViewModel.kt", l = {2166}, m = "invokeSuspend")
    /* renamed from: net.safemoon.androidwallet.viewmodels.SwapViewModel$swapTokens$1$1  reason: invalid class name */
    /* loaded from: classes2.dex */
    public static final class AnonymousClass1 extends SuspendLambda implements hd1<c90, q70<? super te4>, Object> {
        public int label;
        public final /* synthetic */ SwapViewModel this$0;

        /* compiled from: SwapViewModel.kt */
        @kotlin.coroutines.jvm.internal.a(c = "net.safemoon.androidwallet.viewmodels.SwapViewModel$swapTokens$1$1$1", f = "SwapViewModel.kt", l = {1694, 1696, 1737, 1801, 1803, 1842, 1844, 1901, 1903, 1948, 1950}, m = "invokeSuspend")
        /* renamed from: net.safemoon.androidwallet.viewmodels.SwapViewModel$swapTokens$1$1$1  reason: invalid class name and collision with other inner class name */
        /* loaded from: classes2.dex */
        public static final class C02511 extends SuspendLambda implements hd1<k71<? super Object>, q70<? super te4>, Object> {
            private /* synthetic */ Object L$0;
            public int label;
            public final /* synthetic */ SwapViewModel this$0;

            /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
            public C02511(SwapViewModel swapViewModel, q70<? super C02511> q70Var) {
                super(2, q70Var);
                this.this$0 = swapViewModel;
            }

            @Override // kotlin.coroutines.jvm.internal.BaseContinuationImpl
            public final q70<te4> create(Object obj, q70<?> q70Var) {
                C02511 c02511 = new C02511(this.this$0, q70Var);
                c02511.L$0 = obj;
                return c02511;
            }

            @Override // defpackage.hd1
            public /* bridge */ /* synthetic */ Object invoke(k71<? super Object> k71Var, q70<? super te4> q70Var) {
                return invoke2((k71<Object>) k71Var, q70Var);
            }

            /* renamed from: invoke  reason: avoid collision after fix types in other method */
            public final Object invoke2(k71<Object> k71Var, q70<? super te4> q70Var) {
                return ((C02511) create(k71Var, q70Var)).invokeSuspend(te4.a);
            }

            /* JADX WARN: Removed duplicated region for block: B:173:0x04f1  */
            /* JADX WARN: Removed duplicated region for block: B:176:0x050e  */
            /* JADX WARN: Removed duplicated region for block: B:271:0x07b8  */
            /* JADX WARN: Removed duplicated region for block: B:274:0x07d5  */
            /* JADX WARN: Removed duplicated region for block: B:69:0x0200  */
            /* JADX WARN: Removed duplicated region for block: B:72:0x021e  */
            @Override // kotlin.coroutines.jvm.internal.BaseContinuationImpl
            /*
                Code decompiled incorrectly, please refer to instructions dump.
                To view partially-correct code enable 'Show inconsistent code' option in preferences
            */
            public final java.lang.Object invokeSuspend(java.lang.Object r28) {
                /*
                    Method dump skipped, instructions count: 2346
                    To view this dump change 'Code comments level' option to 'DEBUG'
                */
                throw new UnsupportedOperationException("Method not decompiled: net.safemoon.androidwallet.viewmodels.SwapViewModel$swapTokens$1.AnonymousClass1.C02511.invokeSuspend(java.lang.Object):java.lang.Object");
            }
        }

        /* compiled from: SwapViewModel.kt */
        @kotlin.coroutines.jvm.internal.a(c = "net.safemoon.androidwallet.viewmodels.SwapViewModel$swapTokens$1$1$2", f = "SwapViewModel.kt", l = {}, m = "invokeSuspend")
        /* renamed from: net.safemoon.androidwallet.viewmodels.SwapViewModel$swapTokens$1$1$2  reason: invalid class name */
        /* loaded from: classes2.dex */
        public static final class AnonymousClass2 extends SuspendLambda implements kd1<k71<? super Object>, Throwable, q70<? super te4>, Object> {
            public /* synthetic */ Object L$0;
            public int label;
            public final /* synthetic */ SwapViewModel this$0;

            /* compiled from: SwapViewModel.kt */
            @kotlin.coroutines.jvm.internal.a(c = "net.safemoon.androidwallet.viewmodels.SwapViewModel$swapTokens$1$1$2$1", f = "SwapViewModel.kt", l = {}, m = "invokeSuspend")
            /* renamed from: net.safemoon.androidwallet.viewmodels.SwapViewModel$swapTokens$1$1$2$1  reason: invalid class name and collision with other inner class name */
            /* loaded from: classes2.dex */
            public static final class C02521 extends SuspendLambda implements hd1<c90, q70<? super te4>, Object> {
                public final /* synthetic */ Throwable $it;
                public int label;
                public final /* synthetic */ SwapViewModel this$0;

                /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
                public C02521(SwapViewModel swapViewModel, Throwable th, q70<? super C02521> q70Var) {
                    super(2, q70Var);
                    this.this$0 = swapViewModel;
                    this.$it = th;
                }

                @Override // kotlin.coroutines.jvm.internal.BaseContinuationImpl
                public final q70<te4> create(Object obj, q70<?> q70Var) {
                    return new C02521(this.this$0, this.$it, q70Var);
                }

                @Override // defpackage.hd1
                public final Object invoke(c90 c90Var, q70<? super te4> q70Var) {
                    return ((C02521) create(c90Var, q70Var)).invokeSuspend(te4.a);
                }

                @Override // kotlin.coroutines.jvm.internal.BaseContinuationImpl
                public final Object invokeSuspend(Object obj) {
                    gs1.d();
                    if (this.label == 0) {
                        o83.b(obj);
                        gb2<SwapViewModel.e> L0 = this.this$0.L0();
                        String localizedMessage = this.$it.getLocalizedMessage();
                        if (localizedMessage == null && (localizedMessage = this.$it.getMessage()) == null) {
                            localizedMessage = this.this$0.D0(R.string.swap_could_not);
                        }
                        L0.setValue(new SwapViewModel.e(false, localizedMessage));
                        this.this$0.l0().setValue(LoadingState.Normal);
                        this.$it.printStackTrace();
                        return te4.a;
                    }
                    throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
                }
            }

            /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
            public AnonymousClass2(SwapViewModel swapViewModel, q70<? super AnonymousClass2> q70Var) {
                super(3, q70Var);
                this.this$0 = swapViewModel;
            }

            @Override // defpackage.kd1
            public /* bridge */ /* synthetic */ Object invoke(k71<? super Object> k71Var, Throwable th, q70<? super te4> q70Var) {
                return invoke2((k71<Object>) k71Var, th, q70Var);
            }

            /* renamed from: invoke  reason: avoid collision after fix types in other method */
            public final Object invoke2(k71<Object> k71Var, Throwable th, q70<? super te4> q70Var) {
                AnonymousClass2 anonymousClass2 = new AnonymousClass2(this.this$0, q70Var);
                anonymousClass2.L$0 = th;
                return anonymousClass2.invokeSuspend(te4.a);
            }

            @Override // kotlin.coroutines.jvm.internal.BaseContinuationImpl
            public final Object invokeSuspend(Object obj) {
                gs1.d();
                if (this.label == 0) {
                    o83.b(obj);
                    as.b(ej4.a(this.this$0), null, null, new C02521(this.this$0, (Throwable) this.L$0, null), 3, null);
                    return te4.a;
                }
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
        }

        /* compiled from: Collect.kt */
        /* renamed from: net.safemoon.androidwallet.viewmodels.SwapViewModel$swapTokens$1$1$a */
        /* loaded from: classes2.dex */
        public static final class a implements k71<Object> {
            public final /* synthetic */ SwapViewModel a;

            public a(SwapViewModel swapViewModel) {
                this.a = swapViewModel;
            }

            @Override // defpackage.k71
            public Object emit(Object obj, q70<? super te4> q70Var) {
                st1 b;
                b = as.b(ej4.a(this.a), null, null, new SwapViewModel$swapTokens$1$1$3$1(this.a, obj, null), 3, null);
                return b == gs1.d() ? b : te4.a;
            }
        }

        /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
        public AnonymousClass1(SwapViewModel swapViewModel, q70<? super AnonymousClass1> q70Var) {
            super(2, q70Var);
            this.this$0 = swapViewModel;
        }

        @Override // kotlin.coroutines.jvm.internal.BaseContinuationImpl
        public final q70<te4> create(Object obj, q70<?> q70Var) {
            return new AnonymousClass1(this.this$0, q70Var);
        }

        @Override // defpackage.hd1
        public final Object invoke(c90 c90Var, q70<? super te4> q70Var) {
            return ((AnonymousClass1) create(c90Var, q70Var)).invokeSuspend(te4.a);
        }

        @Override // kotlin.coroutines.jvm.internal.BaseContinuationImpl
        public final Object invokeSuspend(Object obj) {
            Object d = gs1.d();
            int i = this.label;
            if (i == 0) {
                o83.b(obj);
                j71 c = n71.c(n71.n(new C02511(this.this$0, null)), new AnonymousClass2(this.this$0, null));
                a aVar = new a(this.this$0);
                this.label = 1;
                if (c.a(aVar, this) == d) {
                    return d;
                }
            } else if (i != 1) {
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            } else {
                o83.b(obj);
            }
            return te4.a;
        }
    }

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public SwapViewModel$swapTokens$1(SwapViewModel swapViewModel, q70<? super SwapViewModel$swapTokens$1> q70Var) {
        super(2, q70Var);
        this.this$0 = swapViewModel;
    }

    @Override // kotlin.coroutines.jvm.internal.BaseContinuationImpl
    public final q70<te4> create(Object obj, q70<?> q70Var) {
        return new SwapViewModel$swapTokens$1(this.this$0, q70Var);
    }

    @Override // defpackage.hd1
    public final Object invoke(c90 c90Var, q70<? super te4> q70Var) {
        return ((SwapViewModel$swapTokens$1) create(c90Var, q70Var)).invokeSuspend(te4.a);
    }

    @Override // kotlin.coroutines.jvm.internal.BaseContinuationImpl
    public final Object invokeSuspend(Object obj) {
        Object d = gs1.d();
        int i = this.label;
        if (i == 0) {
            o83.b(obj);
            this.this$0.l0().setValue(LoadingState.Loading);
            CoroutineDispatcher b = tp0.b();
            AnonymousClass1 anonymousClass1 = new AnonymousClass1(this.this$0, null);
            this.label = 1;
            if (kotlinx.coroutines.a.e(b, anonymousClass1, this) == d) {
                return d;
            }
        } else if (i != 1) {
            throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        } else {
            o83.b(obj);
        }
        return te4.a;
    }
}
