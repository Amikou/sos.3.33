package net.safemoon.androidwallet.viewmodels;

import android.app.Application;
import kotlin.jvm.internal.Lambda;
import net.safemoon.androidwallet.database.room.ApplicationRoomDatabase;
import net.safemoon.androidwallet.repository.dataSource.token.user.LocalUserTokenDataSource;

/* compiled from: ReflectionTrackerViewModel.kt */
/* loaded from: classes2.dex */
public final class ReflectionTrackerViewModel$userDataSource$2 extends Lambda implements rc1<LocalUserTokenDataSource> {
    public final /* synthetic */ Application $application;

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public ReflectionTrackerViewModel$userDataSource$2(Application application) {
        super(0);
        this.$application = application;
    }

    @Override // defpackage.rc1
    public final LocalUserTokenDataSource invoke() {
        return new LocalUserTokenDataSource(ApplicationRoomDatabase.k.c(ApplicationRoomDatabase.n, this.$application, null, 2, null).Z());
    }
}
