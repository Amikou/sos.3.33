package net.safemoon.androidwallet.viewmodels;

import android.app.Application;
import java.util.Arrays;
import java.util.List;
import java.util.ListIterator;
import java.util.Objects;
import kotlin.Pair;
import kotlin.text.Regex;
import net.safemoon.androidwallet.model.blackListAddress.BlackListAddress;
import net.safemoon.androidwallet.model.blackListAddress.Request;
import retrofit2.n;
import wallet.core.jni.CoinType;
import wallet.core.jni.HDWallet;

/* compiled from: CreateWalletViewModel.kt */
/* loaded from: classes2.dex */
public final class CreateWalletViewModel extends gd {
    public final gb2<List<String>> b;

    /* compiled from: CreateWalletViewModel.kt */
    /* loaded from: classes2.dex */
    public enum KEY_TYPE {
        PRIVATE_KEY,
        PASSPHRASE
    }

    /* compiled from: CreateWalletViewModel.kt */
    /* loaded from: classes2.dex */
    public static final class a {
        public a() {
        }

        public /* synthetic */ a(qi0 qi0Var) {
            this();
        }
    }

    /* compiled from: CreateWalletViewModel.kt */
    /* loaded from: classes2.dex */
    public static final class b implements wu<BlackListAddress> {
        public final /* synthetic */ md1<String, String, String, Boolean, te4> a;
        public final /* synthetic */ String b;
        public final /* synthetic */ String c;
        public final /* synthetic */ KEY_TYPE d;
        public final /* synthetic */ String e;

        /* JADX WARN: Multi-variable type inference failed */
        public b(md1<? super String, ? super String, ? super String, ? super Boolean, te4> md1Var, String str, String str2, KEY_TYPE key_type, String str3) {
            this.a = md1Var;
            this.b = str;
            this.c = str2;
            this.d = key_type;
            this.e = str3;
        }

        @Override // defpackage.wu
        public void a(retrofit2.b<BlackListAddress> bVar, Throwable th) {
            fs1.f(bVar, "call");
            fs1.f(th, "t");
            this.a.invoke(null, null, null, Boolean.FALSE);
        }

        @Override // defpackage.wu
        public void b(retrofit2.b<BlackListAddress> bVar, n<BlackListAddress> nVar) {
            List<BlackListAddress.Data> list;
            BlackListAddress.Data data;
            BlackListAddress.Data data2;
            fs1.f(bVar, "call");
            fs1.f(nVar, "response");
            BlackListAddress a = nVar.a();
            if (a == null || (list = a.data) == null) {
                data2 = null;
            } else {
                String str = this.b;
                ListIterator<BlackListAddress.Data> listIterator = list.listIterator(list.size());
                while (true) {
                    if (!listIterator.hasPrevious()) {
                        data = null;
                        break;
                    }
                    data = listIterator.previous();
                    if (fs1.b(data.walletAddress, str)) {
                        break;
                    }
                }
                data2 = data;
            }
            if (data2 != null) {
                md1<String, String, String, Boolean, te4> md1Var = this.a;
                String str2 = this.b;
                String str3 = this.c;
                String str4 = this.d == KEY_TYPE.PASSPHRASE ? this.e : "";
                Boolean bool = data2.isBlacklist;
                fs1.e(bool, "statusToken.isBlacklist");
                md1Var.invoke(str2, str3, str4, bool);
                return;
            }
            this.a.invoke(null, null, null, Boolean.FALSE);
        }
    }

    static {
        new a(null);
        System.loadLibrary("TrustWalletCore");
    }

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public CreateWalletViewModel(Application application) {
        super(application);
        fs1.f(application, "application");
        this.b = new gb2<>(b20.g());
    }

    public final void b(String str, KEY_TYPE key_type, md1<? super String, ? super String, ? super String, ? super Boolean, te4> md1Var) {
        Pair pair;
        fs1.f(str, "enterKey");
        fs1.f(key_type, "keyType");
        fs1.f(md1Var, "callBack");
        if (key_type == KEY_TYPE.PASSPHRASE) {
            CoinType coinType = CoinType.ETHEREUM;
            HDWallet hDWallet = new HDWallet(str, "");
            String addressForCoin = hDWallet.getAddressForCoin(coinType);
            byte[] data = hDWallet.getKeyForCoin(coinType).data();
            fs1.e(data, "hdWallet.getKeyForCoin(coinEth).data()");
            pair = new Pair(addressForCoin, e30.f0(data, false));
        } else {
            pair = new Pair(d(str).getAddress(), str);
        }
        String str2 = (String) pair.component1();
        a4.k().o(new Request(a20.b(str2))).n(new b(md1Var, str2, (String) pair.component2(), key_type, str));
    }

    public final void c() {
        int i = 0;
        do {
            String mnemonic = new HDWallet(128, "").mnemonic();
            fs1.e(mnemonic, "passPhrase");
            Object[] array = new Regex("\\s+").split(mnemonic, 0).toArray(new String[0]);
            Objects.requireNonNull(array, "null cannot be cast to non-null type kotlin.Array<T>");
            String[] strArr = (String[]) array;
            if (ai.q(strArr).size() == strArr.length) {
                this.b.setValue(b20.j(Arrays.copyOf(strArr, strArr.length)));
                return;
            }
            i++;
        } while (i < 10);
    }

    public final ma0 d(String str) {
        fs1.f(str, "privateKey");
        return ma0.create(str);
    }

    public final gb2<List<String>> e() {
        return this.b;
    }
}
