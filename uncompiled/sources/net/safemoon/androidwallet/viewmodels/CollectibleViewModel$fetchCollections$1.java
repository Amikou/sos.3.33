package net.safemoon.androidwallet.viewmodels;

import kotlin.coroutines.jvm.internal.ContinuationImpl;
import kotlin.coroutines.jvm.internal.a;

/* compiled from: CollectibleViewModel.kt */
@a(c = "net.safemoon.androidwallet.viewmodels.CollectibleViewModel", f = "CollectibleViewModel.kt", l = {311, 330, 344, 350, 355, 361, 377, 391, 399, 401, 405, 407, 417, 418, 424}, m = "fetchCollections")
/* loaded from: classes2.dex */
public final class CollectibleViewModel$fetchCollections$1 extends ContinuationImpl {
    public int I$0;
    public long J$0;
    public Object L$0;
    public Object L$1;
    public Object L$2;
    public Object L$3;
    public Object L$4;
    public int label;
    public /* synthetic */ Object result;
    public final /* synthetic */ CollectibleViewModel this$0;

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public CollectibleViewModel$fetchCollections$1(CollectibleViewModel collectibleViewModel, q70<? super CollectibleViewModel$fetchCollections$1> q70Var) {
        super(q70Var);
        this.this$0 = collectibleViewModel;
    }

    @Override // kotlin.coroutines.jvm.internal.BaseContinuationImpl
    public final Object invokeSuspend(Object obj) {
        Object q;
        this.result = obj;
        this.label |= Integer.MIN_VALUE;
        q = this.this$0.q(0, 0, 0, this);
        return q;
    }
}
