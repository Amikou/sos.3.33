package net.safemoon.androidwallet.viewmodels;

import kotlin.coroutines.jvm.internal.ContinuationImpl;
import kotlin.coroutines.jvm.internal.a;

/* compiled from: CustomContractTokenViewModel.kt */
@a(c = "net.safemoon.androidwallet.viewmodels.CustomContractTokenViewModel", f = "CustomContractTokenViewModel.kt", l = {185}, m = "isBlackListed")
/* loaded from: classes2.dex */
public final class CustomContractTokenViewModel$isBlackListed$1 extends ContinuationImpl {
    public Object L$0;
    public int label;
    public /* synthetic */ Object result;
    public final /* synthetic */ CustomContractTokenViewModel this$0;

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public CustomContractTokenViewModel$isBlackListed$1(CustomContractTokenViewModel customContractTokenViewModel, q70<? super CustomContractTokenViewModel$isBlackListed$1> q70Var) {
        super(q70Var);
        this.this$0 = customContractTokenViewModel;
    }

    @Override // kotlin.coroutines.jvm.internal.BaseContinuationImpl
    public final Object invokeSuspend(Object obj) {
        Object v;
        this.result = obj;
        this.label |= Integer.MIN_VALUE;
        v = this.this$0.v(null, this);
        return v;
    }
}
