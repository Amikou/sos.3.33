package net.safemoon.androidwallet.viewmodels;

import android.app.Application;
import kotlin.jvm.internal.Lambda;

/* compiled from: CustomContractTokenViewModel.kt */
/* loaded from: classes2.dex */
public final class CustomContractTokenViewModel$localUserTokenDataSource$2 extends Lambda implements rc1<bn1> {
    public final /* synthetic */ CustomContractTokenViewModel this$0;

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public CustomContractTokenViewModel$localUserTokenDataSource$2(CustomContractTokenViewModel customContractTokenViewModel) {
        super(0);
        this.this$0 = customContractTokenViewModel;
    }

    /* JADX WARN: Can't rename method to resolve collision */
    @Override // defpackage.rc1
    public final bn1 invoke() {
        gg4 gg4Var = gg4.a;
        Application a = this.this$0.a();
        fs1.e(a, "getApplication()");
        return gg4Var.b(a);
    }
}
