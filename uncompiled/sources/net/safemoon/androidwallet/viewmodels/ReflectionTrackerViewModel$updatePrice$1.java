package net.safemoon.androidwallet.viewmodels;

import kotlin.coroutines.jvm.internal.SuspendLambda;
import kotlin.coroutines.jvm.internal.a;

/* compiled from: ReflectionTrackerViewModel.kt */
@a(c = "net.safemoon.androidwallet.viewmodels.ReflectionTrackerViewModel$updatePrice$1", f = "ReflectionTrackerViewModel.kt", l = {289, 290, 291}, m = "invokeSuspend")
/* loaded from: classes2.dex */
public final class ReflectionTrackerViewModel$updatePrice$1 extends SuspendLambda implements hd1<c90, q70<? super te4>, Object> {
    public Object L$0;
    public int label;
    public final /* synthetic */ ReflectionTrackerViewModel this$0;

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public ReflectionTrackerViewModel$updatePrice$1(ReflectionTrackerViewModel reflectionTrackerViewModel, q70<? super ReflectionTrackerViewModel$updatePrice$1> q70Var) {
        super(2, q70Var);
        this.this$0 = reflectionTrackerViewModel;
    }

    @Override // kotlin.coroutines.jvm.internal.BaseContinuationImpl
    public final q70<te4> create(Object obj, q70<?> q70Var) {
        return new ReflectionTrackerViewModel$updatePrice$1(this.this$0, q70Var);
    }

    @Override // defpackage.hd1
    public final Object invoke(c90 c90Var, q70<? super te4> q70Var) {
        return ((ReflectionTrackerViewModel$updatePrice$1) create(c90Var, q70Var)).invokeSuspend(te4.a);
    }

    /* JADX WARN: Removed duplicated region for block: B:20:0x0060 A[RETURN] */
    @Override // kotlin.coroutines.jvm.internal.BaseContinuationImpl
    /*
        Code decompiled incorrectly, please refer to instructions dump.
        To view partially-correct code enable 'Show inconsistent code' option in preferences
    */
    public final java.lang.Object invokeSuspend(java.lang.Object r6) {
        /*
            r5 = this;
            java.lang.Object r0 = defpackage.gs1.d()
            int r1 = r5.label
            r2 = 3
            r3 = 2
            r4 = 1
            if (r1 == 0) goto L29
            if (r1 == r4) goto L25
            if (r1 == r3) goto L1d
            if (r1 != r2) goto L15
            defpackage.o83.b(r6)
            goto L61
        L15:
            java.lang.IllegalStateException r6 = new java.lang.IllegalStateException
            java.lang.String r0 = "call to 'resume' before 'invoke' with coroutine"
            r6.<init>(r0)
            throw r6
        L1d:
            java.lang.Object r1 = r5.L$0
            java.util.List r1 = (java.util.List) r1
            defpackage.o83.b(r6)
            goto L4f
        L25:
            defpackage.o83.b(r6)
            goto L3b
        L29:
            defpackage.o83.b(r6)
            net.safemoon.androidwallet.viewmodels.ReflectionTrackerViewModel r6 = r5.this$0
            net.safemoon.androidwallet.repository.ReflectionDataSource r6 = net.safemoon.androidwallet.viewmodels.ReflectionTrackerViewModel.d(r6)
            r5.label = r4
            java.lang.Object r6 = r6.g(r5)
            if (r6 != r0) goto L3b
            return r0
        L3b:
            r1 = r6
            java.util.List r1 = (java.util.List) r1
            net.safemoon.androidwallet.viewmodels.ReflectionTrackerViewModel r6 = r5.this$0
            net.safemoon.androidwallet.utils.ReflectionCustomContract r6 = net.safemoon.androidwallet.viewmodels.ReflectionTrackerViewModel.c(r6)
            r5.L$0 = r1
            r5.label = r3
            java.lang.Object r6 = r6.i(r1, r5)
            if (r6 != r0) goto L4f
            return r0
        L4f:
            net.safemoon.androidwallet.viewmodels.ReflectionTrackerViewModel r6 = r5.this$0
            net.safemoon.androidwallet.utils.ReflectionCustomContract r6 = net.safemoon.androidwallet.viewmodels.ReflectionTrackerViewModel.c(r6)
            r3 = 0
            r5.L$0 = r3
            r5.label = r2
            java.lang.Object r6 = r6.j(r1, r5)
            if (r6 != r0) goto L61
            return r0
        L61:
            te4 r6 = defpackage.te4.a
            return r6
        */
        throw new UnsupportedOperationException("Method not decompiled: net.safemoon.androidwallet.viewmodels.ReflectionTrackerViewModel$updatePrice$1.invokeSuspend(java.lang.Object):java.lang.Object");
    }
}
