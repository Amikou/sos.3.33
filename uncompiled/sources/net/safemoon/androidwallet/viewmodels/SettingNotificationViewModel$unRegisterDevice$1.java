package net.safemoon.androidwallet.viewmodels;

import android.app.Application;
import kotlin.jvm.internal.Lambda;

/* compiled from: SettingNotificationViewModel.kt */
/* loaded from: classes2.dex */
public final class SettingNotificationViewModel$unRegisterDevice$1 extends Lambda implements tc1<Boolean, te4> {
    public final /* synthetic */ SettingNotificationViewModel this$0;

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public SettingNotificationViewModel$unRegisterDevice$1(SettingNotificationViewModel settingNotificationViewModel) {
        super(1);
        this.this$0 = settingNotificationViewModel;
    }

    @Override // defpackage.tc1
    public /* bridge */ /* synthetic */ te4 invoke(Boolean bool) {
        invoke(bool.booleanValue());
        return te4.a;
    }

    public final void invoke(boolean z) {
        if (z) {
            Application a = this.this$0.a();
            Boolean bool = Boolean.FALSE;
            bo3.n(a, "FCM_TOKEN", bool);
            this.this$0.F().postValue(bool);
            return;
        }
        Application a2 = this.this$0.a();
        Boolean bool2 = Boolean.TRUE;
        bo3.n(a2, "FCM_TOKEN", bool2);
        this.this$0.F().postValue(bool2);
    }
}
