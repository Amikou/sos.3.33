package net.safemoon.androidwallet.viewmodels;

import kotlin.coroutines.jvm.internal.SuspendLambda;
import kotlin.coroutines.jvm.internal.a;
import net.safemoon.androidwallet.model.reflections.RoomReflectionsToken;
import net.safemoon.androidwallet.repository.ReflectionDataSource;

/* compiled from: ReflectionTrackerViewModel.kt */
@a(c = "net.safemoon.androidwallet.viewmodels.ReflectionTrackerViewModel$onChangeReflectionEnable$1", f = "ReflectionTrackerViewModel.kt", l = {446, 447}, m = "invokeSuspend")
/* loaded from: classes2.dex */
public final class ReflectionTrackerViewModel$onChangeReflectionEnable$1 extends SuspendLambda implements hd1<c90, q70<? super te4>, Object> {
    public final /* synthetic */ RoomReflectionsToken $rrt;
    public int label;
    public final /* synthetic */ ReflectionTrackerViewModel this$0;

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public ReflectionTrackerViewModel$onChangeReflectionEnable$1(ReflectionTrackerViewModel reflectionTrackerViewModel, RoomReflectionsToken roomReflectionsToken, q70<? super ReflectionTrackerViewModel$onChangeReflectionEnable$1> q70Var) {
        super(2, q70Var);
        this.this$0 = reflectionTrackerViewModel;
        this.$rrt = roomReflectionsToken;
    }

    @Override // kotlin.coroutines.jvm.internal.BaseContinuationImpl
    public final q70<te4> create(Object obj, q70<?> q70Var) {
        return new ReflectionTrackerViewModel$onChangeReflectionEnable$1(this.this$0, this.$rrt, q70Var);
    }

    @Override // defpackage.hd1
    public final Object invoke(c90 c90Var, q70<? super te4> q70Var) {
        return ((ReflectionTrackerViewModel$onChangeReflectionEnable$1) create(c90Var, q70Var)).invokeSuspend(te4.a);
    }

    @Override // kotlin.coroutines.jvm.internal.BaseContinuationImpl
    public final Object invokeSuspend(Object obj) {
        Object A;
        Object d = gs1.d();
        int i = this.label;
        if (i == 0) {
            o83.b(obj);
            ReflectionDataSource r = this.this$0.r();
            RoomReflectionsToken roomReflectionsToken = this.$rrt;
            this.label = 1;
            if (r.r(roomReflectionsToken, this) == d) {
                return d;
            }
        } else if (i != 1) {
            if (i != 2) {
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
            o83.b(obj);
            return te4.a;
        } else {
            o83.b(obj);
        }
        ReflectionTrackerViewModel reflectionTrackerViewModel = this.this$0;
        RoomReflectionsToken roomReflectionsToken2 = this.$rrt;
        this.label = 2;
        A = reflectionTrackerViewModel.A(roomReflectionsToken2, this);
        if (A == d) {
            return d;
        }
        return te4.a;
    }
}
