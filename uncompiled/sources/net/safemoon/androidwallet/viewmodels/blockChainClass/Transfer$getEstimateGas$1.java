package net.safemoon.androidwallet.viewmodels.blockChainClass;

import kotlin.coroutines.jvm.internal.ContinuationImpl;
import kotlin.coroutines.jvm.internal.a;

/* compiled from: Transfer.kt */
@a(c = "net.safemoon.androidwallet.viewmodels.blockChainClass.Transfer", f = "Transfer.kt", l = {109, 117, 126, 136}, m = "getEstimateGas")
/* loaded from: classes2.dex */
public final class Transfer$getEstimateGas$1 extends ContinuationImpl {
    public double D$0;
    public double D$1;
    public Object L$0;
    public Object L$1;
    public Object L$2;
    public Object L$3;
    public Object L$4;
    public int label;
    public /* synthetic */ Object result;
    public final /* synthetic */ Transfer this$0;

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public Transfer$getEstimateGas$1(Transfer transfer, q70<? super Transfer$getEstimateGas$1> q70Var) {
        super(q70Var);
        this.this$0 = transfer;
    }

    @Override // kotlin.coroutines.jvm.internal.BaseContinuationImpl
    public final Object invokeSuspend(Object obj) {
        this.result = obj;
        this.label |= Integer.MIN_VALUE;
        return this.this$0.x(this);
    }
}
