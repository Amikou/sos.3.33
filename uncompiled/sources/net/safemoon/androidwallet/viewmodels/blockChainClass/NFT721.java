package net.safemoon.androidwallet.viewmodels.blockChainClass;

import java.math.BigInteger;
import java.util.Locale;
import net.safemoon.androidwallet.ERC721;
import net.safemoon.androidwallet.model.wallets.Wallet;

/* compiled from: NFT721.kt */
/* loaded from: classes2.dex */
public final class NFT721 extends WalletWeb3 implements im1 {
    public final String i;
    public final int j;
    public final Wallet k;
    public final sy1 l;

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public NFT721(String str, int i, Wallet wallet2) {
        super(i, wallet2);
        fs1.f(str, "contractAddress");
        fs1.f(wallet2, "wallet");
        this.i = str;
        this.j = i;
        this.k = wallet2;
        this.l = zy1.a(new NFT721$erc721$2(this));
    }

    public final ERC721 A() {
        return (ERC721) this.l.getValue();
    }

    public final Object B(BigInteger bigInteger, q70<? super Boolean> q70Var) {
        String send = A().o(bigInteger).send();
        fs1.e(send, "erc721.ownerOf(tokenId).send()");
        Locale locale = Locale.ROOT;
        String lowerCase = send.toLowerCase(locale);
        fs1.e(lowerCase, "(this as java.lang.Strin….toLowerCase(Locale.ROOT)");
        String address = j().getAddress();
        fs1.e(address, "credentails.address");
        String lowerCase2 = address.toLowerCase(locale);
        fs1.e(lowerCase2, "(this as java.lang.Strin….toLowerCase(Locale.ROOT)");
        return hr.a(fs1.b(lowerCase, lowerCase2));
    }

    @Override // defpackage.im1
    public int a() {
        return i();
    }

    /* JADX WARN: Removed duplicated region for block: B:10:0x0029  */
    /* JADX WARN: Removed duplicated region for block: B:16:0x0064  */
    @Override // defpackage.im1
    /*
        Code decompiled incorrectly, please refer to instructions dump.
        To view partially-correct code enable 'Show inconsistent code' option in preferences
    */
    public java.lang.Object b(java.lang.String r17, java.math.BigInteger r18, java.math.BigInteger r19, defpackage.q70<? super defpackage.hx0> r20) {
        /*
            Method dump skipped, instructions count: 229
            To view this dump change 'Code comments level' option to 'DEBUG'
        */
        throw new UnsupportedOperationException("Method not decompiled: net.safemoon.androidwallet.viewmodels.blockChainClass.NFT721.b(java.lang.String, java.math.BigInteger, java.math.BigInteger, q70):java.lang.Object");
    }

    /* JADX WARN: Removed duplicated region for block: B:10:0x0023  */
    /* JADX WARN: Removed duplicated region for block: B:14:0x0040  */
    @Override // defpackage.im1
    /*
        Code decompiled incorrectly, please refer to instructions dump.
        To view partially-correct code enable 'Show inconsistent code' option in preferences
    */
    public java.lang.Object c(java.lang.String r9, java.math.BigInteger r10, java.math.BigInteger r11, boolean r12, defpackage.q70<? super defpackage.ow0> r13) {
        /*
            r8 = this;
            boolean r11 = r13 instanceof net.safemoon.androidwallet.viewmodels.blockChainClass.NFT721$estimateGas$1
            if (r11 == 0) goto L13
            r11 = r13
            net.safemoon.androidwallet.viewmodels.blockChainClass.NFT721$estimateGas$1 r11 = (net.safemoon.androidwallet.viewmodels.blockChainClass.NFT721$estimateGas$1) r11
            int r0 = r11.label
            r1 = -2147483648(0xffffffff80000000, float:-0.0)
            r2 = r0 & r1
            if (r2 == 0) goto L13
            int r0 = r0 - r1
            r11.label = r0
            goto L18
        L13:
            net.safemoon.androidwallet.viewmodels.blockChainClass.NFT721$estimateGas$1 r11 = new net.safemoon.androidwallet.viewmodels.blockChainClass.NFT721$estimateGas$1
            r11.<init>(r8, r13)
        L18:
            java.lang.Object r13 = r11.result
            java.lang.Object r0 = defpackage.gs1.d()
            int r1 = r11.label
            r2 = 1
            if (r1 == 0) goto L40
            if (r1 != r2) goto L38
            java.lang.Object r9 = r11.L$2
            java.math.BigInteger r9 = (java.math.BigInteger) r9
            java.lang.Object r10 = r11.L$1
            java.lang.String r10 = (java.lang.String) r10
            java.lang.Object r11 = r11.L$0
            net.safemoon.androidwallet.viewmodels.blockChainClass.NFT721 r11 = (net.safemoon.androidwallet.viewmodels.blockChainClass.NFT721) r11
            defpackage.o83.b(r13)
            r7 = r10
            r10 = r9
            r9 = r7
            goto L55
        L38:
            java.lang.IllegalStateException r9 = new java.lang.IllegalStateException
            java.lang.String r10 = "call to 'resume' before 'invoke' with coroutine"
            r9.<init>(r10)
            throw r9
        L40:
            defpackage.o83.b(r13)
            if (r12 == 0) goto L54
            r11.L$0 = r8
            r11.L$1 = r9
            r11.L$2 = r10
            r11.label = r2
            java.lang.Object r11 = r8.g(r11)
            if (r11 != r0) goto L54
            return r0
        L54:
            r11 = r8
        L55:
            r2 = r9
            r3 = r10
            net.safemoon.androidwallet.ERC721 r0 = r11.A()
            ma0 r9 = r11.j()
            java.lang.String r1 = r9.getAddress()
            org.web3j.protocol.core.DefaultBlockParameterName r9 = org.web3j.protocol.core.DefaultBlockParameterName.LATEST
            java.math.BigInteger r4 = r11.q(r9)
            java.math.BigInteger r5 = r11.getGasPrice()
            java.math.BigInteger r6 = r11.l()
            p84 r9 = r0.q(r1, r2, r3, r4, r5, r6)
            ko4 r10 = r11.y()
            org.web3j.protocol.core.c r9 = r10.ethEstimateGas(r9)
            i83 r9 = r9.send()
            ow0 r9 = (defpackage.ow0) r9
            r11.w(r9)
            ow0 r9 = r11.k()
            defpackage.fs1.d(r9)
            return r9
        */
        throw new UnsupportedOperationException("Method not decompiled: net.safemoon.androidwallet.viewmodels.blockChainClass.NFT721.c(java.lang.String, java.math.BigInteger, java.math.BigInteger, boolean, q70):java.lang.Object");
    }

    @Override // net.safemoon.androidwallet.viewmodels.blockChainClass.WalletWeb3
    public int i() {
        return this.j;
    }

    @Override // net.safemoon.androidwallet.viewmodels.blockChainClass.WalletWeb3
    public Wallet u() {
        return this.k;
    }

    public final String z() {
        return this.i;
    }
}
