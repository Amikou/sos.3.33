package net.safemoon.androidwallet.viewmodels.blockChainClass;

import java.math.BigDecimal;
import java.math.BigInteger;
import net.safemoon.androidwallet.model.common.Gas;
import net.safemoon.androidwallet.model.common.GasPrice;
import net.safemoon.androidwallet.model.wallets.Wallet;
import org.web3j.protocol.core.DefaultBlockParameterName;

/* compiled from: WalletWeb3.kt */
/* loaded from: classes2.dex */
public class WalletWeb3 {
    public final int a;
    public final Wallet b;
    public final gb2<Gas> c;
    public final GasPrice d;
    public final BigInteger e;
    public ow0 f;
    public final sy1 g;
    public final sy1 h;

    public WalletWeb3(int i, Wallet wallet2) {
        fs1.f(wallet2, "wallet");
        this.a = i;
        this.b = wallet2;
        this.c = new gb2<>(Gas.Standard);
        this.d = new GasPrice();
        this.e = BigInteger.valueOf(9000000L);
        this.g = zy1.a(new WalletWeb3$privateKey$2(this));
        this.h = zy1.a(new WalletWeb3$credentails$2(this));
    }

    public static /* synthetic */ BigInteger r(WalletWeb3 walletWeb3, DefaultBlockParameterName defaultBlockParameterName, int i, Object obj) {
        if (obj == null) {
            if ((i & 1) != 0) {
                defaultBlockParameterName = DefaultBlockParameterName.PENDING;
            }
            return walletWeb3.q(defaultBlockParameterName);
        }
        throw new UnsupportedOperationException("Super calls with default arguments not supported in this target, function: getNonce");
    }

    public final void e(double d) {
        GasPrice gasPrice = this.d;
        int i = i();
        Gas value = this.c.getValue();
        fs1.d(value);
        fs1.e(value, "gas.value!!");
        gasPrice.applyNewGas(i, d, value);
    }

    public final Object f(q70<? super Double> q70Var) {
        return n().fetchGasSuspended(i(), m().getValue(), q70Var);
    }

    /* JADX WARN: Removed duplicated region for block: B:10:0x0024  */
    /* JADX WARN: Removed duplicated region for block: B:14:0x0036  */
    /*
        Code decompiled incorrectly, please refer to instructions dump.
        To view partially-correct code enable 'Show inconsistent code' option in preferences
    */
    public final java.lang.Object g(defpackage.q70<? super java.math.BigInteger> r8) {
        /*
            r7 = this;
            boolean r0 = r8 instanceof net.safemoon.androidwallet.viewmodels.blockChainClass.WalletWeb3$fetchGasSuspended$1
            if (r0 == 0) goto L13
            r0 = r8
            net.safemoon.androidwallet.viewmodels.blockChainClass.WalletWeb3$fetchGasSuspended$1 r0 = (net.safemoon.androidwallet.viewmodels.blockChainClass.WalletWeb3$fetchGasSuspended$1) r0
            int r1 = r0.label
            r2 = -2147483648(0xffffffff80000000, float:-0.0)
            r3 = r1 & r2
            if (r3 == 0) goto L13
            int r1 = r1 - r2
            r0.label = r1
            goto L18
        L13:
            net.safemoon.androidwallet.viewmodels.blockChainClass.WalletWeb3$fetchGasSuspended$1 r0 = new net.safemoon.androidwallet.viewmodels.blockChainClass.WalletWeb3$fetchGasSuspended$1
            r0.<init>(r7, r8)
        L18:
            r4 = r0
            java.lang.Object r8 = r4.result
            java.lang.Object r0 = defpackage.gs1.d()
            int r1 = r4.label
            r2 = 1
            if (r1 == 0) goto L36
            if (r1 != r2) goto L2e
            java.lang.Object r0 = r4.L$0
            net.safemoon.androidwallet.viewmodels.blockChainClass.WalletWeb3 r0 = (net.safemoon.androidwallet.viewmodels.blockChainClass.WalletWeb3) r0
            defpackage.o83.b(r8)
            goto L51
        L2e:
            java.lang.IllegalStateException r8 = new java.lang.IllegalStateException
            java.lang.String r0 = "call to 'resume' before 'invoke' with coroutine"
            r8.<init>(r0)
            throw r8
        L36:
            defpackage.o83.b(r8)
            net.safemoon.androidwallet.model.common.GasPrice r1 = r7.n()
            int r8 = r7.i()
            r3 = 0
            r5 = 2
            r6 = 0
            r4.L$0 = r7
            r4.label = r2
            r2 = r8
            java.lang.Object r8 = net.safemoon.androidwallet.model.common.GasPrice.fetchGasSuspended$default(r1, r2, r3, r4, r5, r6)
            if (r8 != r0) goto L50
            return r0
        L50:
            r0 = r7
        L51:
            java.math.BigInteger r8 = r0.getGasPrice()
            return r8
        */
        throw new UnsupportedOperationException("Method not decompiled: net.safemoon.androidwallet.viewmodels.blockChainClass.WalletWeb3.g(q70):java.lang.Object");
    }

    public final BigInteger getGasPrice() {
        GasPrice gasPrice = this.d;
        Gas value = this.c.getValue();
        fs1.d(value);
        fs1.e(value, "gas.value!!");
        BigInteger bigInteger = new BigDecimal(gasPrice.getPrice(value, i())).multiply(BigDecimal.TEN.pow(9)).toBigInteger();
        fs1.e(bigInteger, "BigDecimal(gasPrice.getP…EN.pow(9)).toBigInteger()");
        return bigInteger;
    }

    /* JADX WARN: Removed duplicated region for block: B:10:0x0023  */
    /* JADX WARN: Removed duplicated region for block: B:15:0x0031  */
    /*
        Code decompiled incorrectly, please refer to instructions dump.
        To view partially-correct code enable 'Show inconsistent code' option in preferences
    */
    public final java.lang.Object h(java.lang.String r5, int r6, defpackage.q70<? super java.math.BigDecimal> r7) {
        /*
            r4 = this;
            boolean r0 = r7 instanceof net.safemoon.androidwallet.viewmodels.blockChainClass.WalletWeb3$getBalance$1
            if (r0 == 0) goto L13
            r0 = r7
            net.safemoon.androidwallet.viewmodels.blockChainClass.WalletWeb3$getBalance$1 r0 = (net.safemoon.androidwallet.viewmodels.blockChainClass.WalletWeb3$getBalance$1) r0
            int r1 = r0.label
            r2 = -2147483648(0xffffffff80000000, float:-0.0)
            r3 = r1 & r2
            if (r3 == 0) goto L13
            int r1 = r1 - r2
            r0.label = r1
            goto L18
        L13:
            net.safemoon.androidwallet.viewmodels.blockChainClass.WalletWeb3$getBalance$1 r0 = new net.safemoon.androidwallet.viewmodels.blockChainClass.WalletWeb3$getBalance$1
            r0.<init>(r4, r7)
        L18:
            java.lang.Object r7 = r0.result
            java.lang.Object r1 = defpackage.gs1.d()
            int r2 = r0.label
            r3 = 1
            if (r2 == 0) goto L31
            if (r2 != r3) goto L29
            defpackage.o83.b(r7)     // Catch: java.lang.Exception -> L85
            goto L48
        L29:
            java.lang.IllegalStateException r5 = new java.lang.IllegalStateException
            java.lang.String r6 = "call to 'resume' before 'invoke' with coroutine"
            r5.<init>(r6)
            throw r5
        L31:
            defpackage.o83.b(r7)
            int r7 = r5.length()     // Catch: java.lang.Exception -> L85
            if (r7 != 0) goto L3c
            r7 = r3
            goto L3d
        L3c:
            r7 = 0
        L3d:
            if (r7 == 0) goto L4b
            r0.label = r3     // Catch: java.lang.Exception -> L85
            java.lang.Object r7 = r4.v(r0)     // Catch: java.lang.Exception -> L85
            if (r7 != r1) goto L48
            return r1
        L48:
            java.math.BigDecimal r7 = (java.math.BigDecimal) r7     // Catch: java.lang.Exception -> L85
            goto L7f
        L4b:
            ko4 r7 = r4.y()     // Catch: java.lang.Exception -> L85
            ma0 r0 = r4.j()     // Catch: java.lang.Exception -> L85
            java.math.BigInteger r1 = r4.l()     // Catch: java.lang.Exception -> L85
            java.lang.String r2 = "GAS_LIMIT"
            defpackage.fs1.e(r1, r2)     // Catch: java.lang.Exception -> L85
            j80 r1 = r4.p(r1)     // Catch: java.lang.Exception -> L85
            net.safemoon.androidwallet.ERC20 r5 = net.safemoon.androidwallet.ERC20.s(r5, r7, r0, r1)     // Catch: java.lang.Exception -> L85
            ma0 r7 = r4.j()     // Catch: java.lang.Exception -> L85
            java.lang.String r7 = r7.getAddress()     // Catch: java.lang.Exception -> L85
            m63 r5 = r5.q(r7)     // Catch: java.lang.Exception -> L85
            java.lang.Object r5 = r5.send()     // Catch: java.lang.Exception -> L85
            java.lang.String r7 = "load(contractAddress, we…edentails.address).send()"
            defpackage.fs1.e(r5, r7)     // Catch: java.lang.Exception -> L85
            java.math.BigInteger r5 = (java.math.BigInteger) r5     // Catch: java.lang.Exception -> L85
            java.math.BigDecimal r7 = defpackage.e30.r(r5, r6)     // Catch: java.lang.Exception -> L85
        L7f:
            java.lang.String r5 = "{\n            if (contra…)\n            }\n        }"
            defpackage.fs1.e(r7, r5)     // Catch: java.lang.Exception -> L85
            goto L8c
        L85:
            java.math.BigDecimal r7 = java.math.BigDecimal.ZERO
            java.lang.String r5 = "{\n            BigDecimal.ZERO\n        }"
            defpackage.fs1.e(r7, r5)
        L8c:
            return r7
        */
        throw new UnsupportedOperationException("Method not decompiled: net.safemoon.androidwallet.viewmodels.blockChainClass.WalletWeb3.h(java.lang.String, int, q70):java.lang.Object");
    }

    public int i() {
        return this.a;
    }

    public final ma0 j() {
        return (ma0) this.h.getValue();
    }

    public final ow0 k() {
        return this.f;
    }

    public final BigInteger l() {
        return this.e;
    }

    public final gb2<Gas> m() {
        return this.c;
    }

    public final GasPrice n() {
        return this.d;
    }

    public final double o() {
        GasPrice gasPrice = this.d;
        Gas value = this.c.getValue();
        fs1.d(value);
        fs1.e(value, "gas.value!!");
        return gasPrice.getPrice(value, i());
    }

    public final j80 p(BigInteger bigInteger) {
        fs1.f(bigInteger, "gasLimit");
        return new it3(getGasPrice(), bigInteger);
    }

    public final BigInteger q(DefaultBlockParameterName defaultBlockParameterName) {
        fs1.f(defaultBlockParameterName, "defaultBlockParameterName");
        BigInteger transactionCount = y().ethGetTransactionCount(j().getAddress(), defaultBlockParameterName).send().getTransactionCount();
        fs1.e(transactionCount, "ethGetTransactionCount.transactionCount");
        return transactionCount;
    }

    public final String s() {
        return (String) this.g.getValue();
    }

    public final String t() {
        return e30.z(i());
    }

    public Wallet u() {
        return this.b;
    }

    public final Object v(q70<? super BigDecimal> q70Var) {
        try {
            BigInteger balance = y().ethGetBalance(j().getAddress(), DefaultBlockParameterName.LATEST).send().getBalance();
            fs1.e(balance, "web3().ethGetBalance(cre…          .send().balance");
            BigDecimal r = e30.r(balance, 18);
            fs1.e(r, "{\n            web3().eth…nce.fromWEI(18)\n        }");
            return r;
        } catch (Exception unused) {
            BigDecimal bigDecimal = BigDecimal.ZERO;
            fs1.e(bigDecimal, "{\n            BigDecimal.ZERO\n        }");
            return bigDecimal;
        }
    }

    public final void w(ow0 ow0Var) {
        this.f = ow0Var;
    }

    public final z33 x() {
        return new z33(y(), j(), i());
    }

    public final ko4 y() {
        return jo4.a(new ml1(t()));
    }
}
