package net.safemoon.androidwallet.viewmodels.blockChainClass;

import kotlin.coroutines.jvm.internal.ContinuationImpl;
import kotlin.coroutines.jvm.internal.a;

/* compiled from: BlockTime.kt */
@a(c = "net.safemoon.androidwallet.viewmodels.blockChainClass.BlockTime", f = "BlockTime.kt", l = {71, 76, 76, 78}, m = "findBetter")
/* loaded from: classes2.dex */
public final class BlockTime$findBetter$1 extends ContinuationImpl {
    public long J$0;
    public Object L$0;
    public Object L$1;
    public Object L$2;
    public Object L$3;
    public boolean Z$0;
    public int label;
    public /* synthetic */ Object result;
    public final /* synthetic */ BlockTime this$0;

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public BlockTime$findBetter$1(BlockTime blockTime, q70<? super BlockTime$findBetter$1> q70Var) {
        super(q70Var);
        this.this$0 = blockTime;
    }

    @Override // kotlin.coroutines.jvm.internal.BaseContinuationImpl
    public final Object invokeSuspend(Object obj) {
        this.result = obj;
        this.label |= Integer.MIN_VALUE;
        return this.this$0.u(null, null, false, 0L, this);
    }
}
