package net.safemoon.androidwallet.viewmodels.blockChainClass;

import android.app.Application;
import java.math.BigDecimal;
import java.math.BigInteger;
import net.safemoon.androidwallet.ERC20;
import net.safemoon.androidwallet.common.TokenType;
import net.safemoon.androidwallet.model.common.Gas;
import net.safemoon.androidwallet.model.common.GasPrice;
import net.safemoon.androidwallet.model.token.abstraction.IToken;
import org.web3j.protocol.core.DefaultBlockParameterName;

/* compiled from: MyWeb3.kt */
/* loaded from: classes2.dex */
public class MyWeb3 {
    public final Application a;
    public final IToken b;
    public final sy1 c;
    public final sy1 d;
    public final sy1 e;
    public final GasPrice f;
    public final Gas g;
    public final BigInteger h;

    public MyWeb3(Application application, IToken iToken) {
        fs1.f(application, "application");
        fs1.f(iToken, "iToken");
        this.a = application;
        this.b = iToken;
        this.c = zy1.a(new MyWeb3$privateKey$2(this));
        this.d = zy1.a(new MyWeb3$address$2(this));
        this.e = zy1.a(new MyWeb3$credentials$2(this));
        this.f = new GasPrice();
        this.g = Gas.Standard;
        this.h = BigInteger.valueOf(9000000L);
    }

    public static /* synthetic */ BigInteger l(MyWeb3 myWeb3, Double d, int i, Object obj) {
        if (obj == null) {
            if ((i & 1) != 0) {
                d = null;
            }
            return myWeb3.j(d);
        }
        throw new UnsupportedOperationException("Super calls with default arguments not supported in this target, function: getGasPrice");
    }

    public final void a(double d) {
        this.f.applyNewGas(o().getChainId(), d, this.g);
    }

    public final ERC20 b(BigInteger bigInteger) {
        fs1.f(bigInteger, "gasLimit");
        return ERC20.s(o().getContractAddress(), t(), g(), n(bigInteger));
    }

    public final Object c(q70<? super Double> q70Var) {
        return k().fetchGasSuspended(o().getChainId(), i(), q70Var);
    }

    public final String d() {
        return (String) this.d.getValue();
    }

    public Application e() {
        return this.a;
    }

    public final Object f(q70<? super BigInteger> q70Var) {
        BigInteger send;
        try {
            if (s()) {
                send = t().ethGetBalance(d(), DefaultBlockParameterName.LATEST).send().getBalance();
            } else {
                BigInteger h = h();
                fs1.e(h, "GAS_LIMIT");
                send = b(h).q(d()).send();
            }
            return send;
        } catch (Exception unused) {
            return null;
        }
    }

    public final ma0 g() {
        return (ma0) this.e.getValue();
    }

    public final BigInteger h() {
        return this.h;
    }

    public final Gas i() {
        return this.g;
    }

    public final BigInteger j(Double d) {
        BigInteger bigInteger = new BigDecimal(d == null ? m() : d.doubleValue()).multiply(BigDecimal.TEN.pow(9)).toBigInteger();
        fs1.e(bigInteger, "BigDecimal(gasPriceInGWE…EN.pow(9)).toBigInteger()");
        return bigInteger;
    }

    public final GasPrice k() {
        return this.f;
    }

    public final double m() {
        return this.f.getPrice(this.g, o().getChainId());
    }

    public final j80 n(BigInteger bigInteger) {
        fs1.f(bigInteger, "gasLimit");
        return new it3(l(this, null, 1, null), bigInteger);
    }

    public IToken o() {
        return this.b;
    }

    public final String p() {
        return TokenType.Companion.b(o().getChainId()).getNativeToken();
    }

    public final String q() {
        return (String) this.c.getValue();
    }

    public final String r() {
        return e30.z(o().getChainId());
    }

    public final boolean s() {
        return o().getContractAddress().length() == 0;
    }

    public final ko4 t() {
        return jo4.a(new ml1(r()));
    }
}
