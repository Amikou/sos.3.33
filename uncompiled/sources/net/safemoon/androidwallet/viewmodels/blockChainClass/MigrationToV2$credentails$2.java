package net.safemoon.androidwallet.viewmodels.blockChainClass;

import kotlin.jvm.internal.Lambda;

/* compiled from: MigrationToV2.kt */
/* loaded from: classes2.dex */
public final class MigrationToV2$credentails$2 extends Lambda implements rc1<ma0> {
    public final /* synthetic */ MigrationToV2 this$0;

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public MigrationToV2$credentails$2(MigrationToV2 migrationToV2) {
        super(0);
        this.this$0 = migrationToV2;
    }

    @Override // defpackage.rc1
    public final ma0 invoke() {
        String o;
        o = this.this$0.o();
        return ma0.create(o);
    }
}
