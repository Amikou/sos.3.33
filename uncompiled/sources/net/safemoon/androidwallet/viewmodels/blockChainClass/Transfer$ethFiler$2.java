package net.safemoon.androidwallet.viewmodels.blockChainClass;

import kotlin.jvm.internal.Lambda;
import org.web3j.protocol.core.DefaultBlockParameterName;

/* compiled from: Transfer.kt */
/* loaded from: classes2.dex */
public final class Transfer$ethFiler$2 extends Lambda implements rc1<qw0> {
    public static final Transfer$ethFiler$2 INSTANCE = new Transfer$ethFiler$2();

    public Transfer$ethFiler$2() {
        super(0);
    }

    @Override // defpackage.rc1
    public final qw0 invoke() {
        return new qw0(DefaultBlockParameterName.EARLIEST, DefaultBlockParameterName.LATEST, b20.c("0x8076c74c5e3f5852037f31ff0093eeb8c8add8d3"));
    }
}
