package net.safemoon.androidwallet.viewmodels.blockChainClass;

import java.math.BigInteger;
import kotlin.jvm.internal.Lambda;
import net.safemoon.androidwallet.ERC20;

/* compiled from: Transfer.kt */
/* loaded from: classes2.dex */
public final class Transfer$ercTransferEvent$2 extends Lambda implements rc1<q71<ERC20.b>> {
    public final /* synthetic */ Transfer this$0;

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public Transfer$ercTransferEvent$2(Transfer transfer) {
        super(0);
        this.this$0 = transfer;
    }

    @Override // defpackage.rc1
    public final q71<ERC20.b> invoke() {
        qw0 A;
        Transfer transfer = this.this$0;
        BigInteger h = transfer.h();
        fs1.e(h, "GAS_LIMIT");
        ERC20 b = transfer.b(h);
        A = this.this$0.A();
        return b.transferEventFlowable(A);
    }
}
