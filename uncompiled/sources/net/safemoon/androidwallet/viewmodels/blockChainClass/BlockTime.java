package net.safemoon.androidwallet.viewmodels.blockChainClass;

import android.app.Application;
import defpackage.gw0;
import java.math.BigInteger;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import net.safemoon.androidwallet.model.token.abstraction.IToken;
import org.web3j.protocol.core.DefaultBlockParameterName;

/* compiled from: BlockTime.kt */
/* loaded from: classes2.dex */
public final class BlockTime extends MyWeb3 {
    public final Application i;
    public final IToken j;
    public final HashMap<Long, ArrayList<BigInteger>> k;
    public final HashMap<String, dc3> l;
    public int m;
    public Double n;
    public Date o;

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public BlockTime(Application application, IToken iToken) {
        super(application, iToken);
        fs1.f(application, "application");
        fs1.f(iToken, "iToken");
        this.i = application;
        this.j = iToken;
        this.k = new HashMap<>();
        this.l = new HashMap<>();
    }

    public final Object A(Date date, BigInteger bigInteger, BigInteger bigInteger2, q70<? super BigInteger> q70Var) {
        BigInteger subtract;
        BigInteger add = bigInteger.add(bigInteger2);
        fs1.e(add, "this.add(other)");
        ArrayList<BigInteger> arrayList = this.k.get(hr.e(F(date)));
        boolean z = false;
        if (arrayList != null && arrayList.contains(add)) {
            z = true;
        }
        if (z) {
            if (bigInteger2.compareTo(BigInteger.ZERO) < 0) {
                BigInteger bigInteger3 = BigInteger.ONE;
                fs1.e(bigInteger3, "ONE");
                subtract = bigInteger3.add(bigInteger2);
                fs1.e(subtract, "this.add(other)");
            } else {
                BigInteger bigInteger4 = BigInteger.ONE;
                fs1.e(bigInteger4, "ONE");
                subtract = bigInteger4.subtract(bigInteger2);
                fs1.e(subtract, "this.subtract(other)");
            }
            return A(date, bigInteger, subtract, q70Var);
        }
        ArrayList<BigInteger> arrayList2 = this.k.get(hr.e(F(date)));
        if (arrayList2 != null) {
            hr.a(arrayList2.add(add));
        }
        return add;
    }

    public final boolean B(Date date, Date date2) {
        return date.compareTo(date2) < 0;
    }

    /* JADX WARN: Removed duplicated region for block: B:10:0x0025  */
    /* JADX WARN: Removed duplicated region for block: B:16:0x0052  */
    /*
        Code decompiled incorrectly, please refer to instructions dump.
        To view partially-correct code enable 'Show inconsistent code' option in preferences
    */
    public final java.lang.Object C(java.util.Date r9, defpackage.dc3 r10, boolean r11, defpackage.q70<? super java.lang.Boolean> r12) {
        /*
            Method dump skipped, instructions count: 280
            To view this dump change 'Code comments level' option to 'DEBUG'
        */
        throw new UnsupportedOperationException("Method not decompiled: net.safemoon.androidwallet.viewmodels.blockChainClass.BlockTime.C(java.util.Date, dc3, boolean, q70):java.lang.Object");
    }

    public final boolean D(Date date, Date date2) {
        return date.compareTo(date2) >= 0;
    }

    public final Date E(BigInteger bigInteger) {
        return new Date(bigInteger.longValue() * 1000);
    }

    public final long F(Date date) {
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(date);
        return calendar.getTimeInMillis() / 1000;
    }

    @Override // net.safemoon.androidwallet.viewmodels.blockChainClass.MyWeb3
    public Application e() {
        return this.i;
    }

    @Override // net.safemoon.androidwallet.viewmodels.blockChainClass.MyWeb3
    public IToken o() {
        return this.j;
    }

    /* JADX WARN: Multi-variable type inference failed */
    /* JADX WARN: Removed duplicated region for block: B:10:0x0033  */
    /* JADX WARN: Removed duplicated region for block: B:28:0x009c  */
    /* JADX WARN: Removed duplicated region for block: B:34:0x00bf  */
    /* JADX WARN: Removed duplicated region for block: B:36:0x00c4  */
    /* JADX WARN: Removed duplicated region for block: B:50:0x013d A[RETURN] */
    /* JADX WARN: Removed duplicated region for block: B:51:0x013e  */
    /* JADX WARN: Removed duplicated region for block: B:54:0x017d  */
    /* JADX WARN: Removed duplicated region for block: B:55:0x017f  */
    /* JADX WARN: Removed duplicated region for block: B:58:0x0192 A[RETURN] */
    /* JADX WARN: Removed duplicated region for block: B:59:0x0193 A[PHI: r4 
      PHI: (r4v23 java.lang.Object) = (r4v22 java.lang.Object), (r4v1 java.lang.Object) binds: [B:57:0x0190, B:15:0x003f] A[DONT_GENERATE, DONT_INLINE], RETURN] */
    /* JADX WARN: Type inference failed for: r1v0, types: [java.util.Date, java.lang.Object] */
    /* JADX WARN: Type inference failed for: r1v1 */
    /* JADX WARN: Type inference failed for: r1v21 */
    /*
        Code decompiled incorrectly, please refer to instructions dump.
        To view partially-correct code enable 'Show inconsistent code' option in preferences
    */
    public final java.lang.Object u(java.util.Date r22, defpackage.dc3 r23, boolean r24, long r25, defpackage.q70<? super java.lang.String> r27) {
        /*
            Method dump skipped, instructions count: 413
            To view this dump change 'Code comments level' option to 'DEBUG'
        */
        throw new UnsupportedOperationException("Method not decompiled: net.safemoon.androidwallet.viewmodels.blockChainClass.BlockTime.u(java.util.Date, dc3, boolean, long, q70):java.lang.Object");
    }

    public final Object v(String str, q70<? super gw0.a> q70Var) {
        gi0 b;
        try {
            if (bv3.i(str) == null) {
                b = fi0.a(str);
            } else {
                b = fi0.b(new BigInteger(str));
            }
            gw0 send = t().ethGetBlockByNumber(b, false).send();
            if (send.getError() == null) {
                return send.getBlock();
            }
            return null;
        } catch (Exception unused) {
            return null;
        }
    }

    public final Object w(q70<? super gw0.a> q70Var) {
        try {
            gw0 send = t().ethGetBlockByNumber(DefaultBlockParameterName.LATEST, false).send();
            if (send.getError() == null) {
                return send.getBlock();
            }
            return null;
        } catch (Exception unused) {
            return null;
        }
    }

    /* JADX WARN: Removed duplicated region for block: B:10:0x0024  */
    /* JADX WARN: Removed duplicated region for block: B:16:0x0044  */
    /* JADX WARN: Removed duplicated region for block: B:22:0x006d A[RETURN] */
    /* JADX WARN: Removed duplicated region for block: B:23:0x006e  */
    /*
        Code decompiled incorrectly, please refer to instructions dump.
        To view partially-correct code enable 'Show inconsistent code' option in preferences
    */
    public final java.lang.Object x(defpackage.q70<? super defpackage.te4> r9) {
        /*
            r8 = this;
            boolean r0 = r9 instanceof net.safemoon.androidwallet.viewmodels.blockChainClass.BlockTime$getBlockTime$1
            if (r0 == 0) goto L13
            r0 = r9
            net.safemoon.androidwallet.viewmodels.blockChainClass.BlockTime$getBlockTime$1 r0 = (net.safemoon.androidwallet.viewmodels.blockChainClass.BlockTime$getBlockTime$1) r0
            int r1 = r0.label
            r2 = -2147483648(0xffffffff80000000, float:-0.0)
            r3 = r1 & r2
            if (r3 == 0) goto L13
            int r1 = r1 - r2
            r0.label = r1
            goto L18
        L13:
            net.safemoon.androidwallet.viewmodels.blockChainClass.BlockTime$getBlockTime$1 r0 = new net.safemoon.androidwallet.viewmodels.blockChainClass.BlockTime$getBlockTime$1
            r0.<init>(r8, r9)
        L18:
            java.lang.Object r9 = r0.result
            java.lang.Object r1 = defpackage.gs1.d()
            int r2 = r0.label
            r3 = 2
            r4 = 1
            if (r2 == 0) goto L44
            if (r2 == r4) goto L3c
            if (r2 != r3) goto L34
            java.lang.Object r1 = r0.L$1
            dc3 r1 = (defpackage.dc3) r1
            java.lang.Object r0 = r0.L$0
            net.safemoon.androidwallet.viewmodels.blockChainClass.BlockTime r0 = (net.safemoon.androidwallet.viewmodels.blockChainClass.BlockTime) r0
            defpackage.o83.b(r9)
            goto L71
        L34:
            java.lang.IllegalStateException r9 = new java.lang.IllegalStateException
            java.lang.String r0 = "call to 'resume' before 'invoke' with coroutine"
            r9.<init>(r0)
            throw r9
        L3c:
            java.lang.Object r2 = r0.L$0
            net.safemoon.androidwallet.viewmodels.blockChainClass.BlockTime r2 = (net.safemoon.androidwallet.viewmodels.blockChainClass.BlockTime) r2
            defpackage.o83.b(r9)
            goto L59
        L44:
            defpackage.o83.b(r9)
            org.web3j.protocol.core.DefaultBlockParameterName r9 = org.web3j.protocol.core.DefaultBlockParameterName.LATEST
            java.lang.String r9 = r9.name()
            r0.L$0 = r8
            r0.label = r4
            java.lang.Object r9 = r8.y(r9, r0)
            if (r9 != r1) goto L58
            return r1
        L58:
            r2 = r8
        L59:
            dc3 r9 = (defpackage.dc3) r9
            org.web3j.protocol.core.DefaultBlockParameterName r4 = org.web3j.protocol.core.DefaultBlockParameterName.EARLIEST
            java.lang.String r4 = r4.name()
            r0.L$0 = r2
            r0.L$1 = r9
            r0.label = r3
            java.lang.Object r0 = r2.y(r4, r0)
            if (r0 != r1) goto L6e
            return r1
        L6e:
            r1 = r9
            r9 = r0
            r0 = r2
        L71:
            dc3 r9 = (defpackage.dc3) r9
            java.math.BigInteger r2 = r1.b()
            double r2 = r2.doubleValue()
            java.math.BigInteger r4 = r9.b()
            double r4 = r4.doubleValue()
            double r2 = r2 - r4
            java.lang.String r1 = r1.a()
            int r1 = java.lang.Integer.parseInt(r1)
            double r4 = (double) r1
            r6 = 4607182418800017408(0x3ff0000000000000, double:1.0)
            double r4 = r4 - r6
            double r2 = r2 / r4
            java.lang.Double r1 = defpackage.hr.b(r2)
            r0.n = r1
            java.math.BigInteger r9 = r9.b()
            java.util.Date r9 = r0.E(r9)
            r0.o = r9
            te4 r9 = defpackage.te4.a
            return r9
        */
        throw new UnsupportedOperationException("Method not decompiled: net.safemoon.androidwallet.viewmodels.blockChainClass.BlockTime.x(q70):java.lang.Object");
    }

    /* JADX WARN: Multi-variable type inference failed */
    /* JADX WARN: Removed duplicated region for block: B:10:0x0026  */
    /* JADX WARN: Removed duplicated region for block: B:16:0x0053  */
    /* JADX WARN: Removed duplicated region for block: B:26:0x0095  */
    /* JADX WARN: Removed duplicated region for block: B:32:0x00b0  */
    /* JADX WARN: Removed duplicated region for block: B:33:0x00b2  */
    /* JADX WARN: Removed duplicated region for block: B:35:0x00bf  */
    /* JADX WARN: Type inference failed for: r1v5, types: [java.util.Map] */
    /*
        Code decompiled incorrectly, please refer to instructions dump.
        To view partially-correct code enable 'Show inconsistent code' option in preferences
    */
    public final java.lang.Object y(java.lang.String r10, defpackage.q70<? super defpackage.dc3> r11) {
        /*
            Method dump skipped, instructions count: 219
            To view this dump change 'Code comments level' option to 'DEBUG'
        */
        throw new UnsupportedOperationException("Method not decompiled: net.safemoon.androidwallet.viewmodels.blockChainClass.BlockTime.y(java.lang.String, q70):java.lang.Object");
    }

    /* JADX WARN: Can't wrap try/catch for region: R(9:1|(2:3|(7:5|6|7|(1:(2:56|(1:(1:(1:(3:61|52|53)(2:62|63))(5:64|65|66|40|41))(8:67|68|69|70|33|(1:35)(1:42)|36|(1:38)(3:39|40|41)))(5:73|74|75|28|29))(4:10|11|12|13))(10:77|78|79|(2:84|85)|86|87|88|89|(1:91)|85)|14|15|(2:17|18)(3:20|(1:45)(1:22)|(2:24|(1:26)(3:27|28|29))(6:30|(1:32)|33|(0)(0)|36|(0)(0)))))|98|6|7|(0)(0)|14|15|(0)(0)) */
    /* JADX WARN: Multi-variable type inference failed */
    /* JADX WARN: Removed duplicated region for block: B:10:0x002d  */
    /* JADX WARN: Removed duplicated region for block: B:34:0x00a5  */
    /* JADX WARN: Removed duplicated region for block: B:49:0x00d5 A[Catch: Exception -> 0x00a2, TryCatch #5 {Exception -> 0x00a2, blocks: (B:67:0x0173, B:71:0x017a, B:47:0x00ca, B:49:0x00d5, B:51:0x00e2, B:58:0x0105, B:64:0x0128, B:54:0x00f4), top: B:96:0x00ca }] */
    /* JADX WARN: Removed duplicated region for block: B:51:0x00e2 A[Catch: Exception -> 0x00a2, TryCatch #5 {Exception -> 0x00a2, blocks: (B:67:0x0173, B:71:0x017a, B:47:0x00ca, B:49:0x00d5, B:51:0x00e2, B:58:0x0105, B:64:0x0128, B:54:0x00f4), top: B:96:0x00ca }] */
    /* JADX WARN: Removed duplicated region for block: B:69:0x0177  */
    /* JADX WARN: Removed duplicated region for block: B:70:0x0179  */
    /* JADX WARN: Removed duplicated region for block: B:73:0x0195 A[RETURN] */
    /* JADX WARN: Removed duplicated region for block: B:74:0x0196  */
    /* JADX WARN: Removed duplicated region for block: B:81:0x01b7 A[RETURN] */
    /* JADX WARN: Removed duplicated region for block: B:82:0x01b8  */
    /* JADX WARN: Type inference failed for: r1v10, types: [java.lang.Object] */
    /* JADX WARN: Type inference failed for: r1v14 */
    /* JADX WARN: Type inference failed for: r1v16, types: [java.util.Date, java.lang.Object] */
    /* JADX WARN: Type inference failed for: r1v17 */
    /* JADX WARN: Type inference failed for: r1v19 */
    /* JADX WARN: Type inference failed for: r1v23 */
    /* JADX WARN: Type inference failed for: r1v24 */
    /* JADX WARN: Type inference failed for: r1v25 */
    /* JADX WARN: Type inference failed for: r1v5, types: [java.lang.Object] */
    /* JADX WARN: Type inference failed for: r1v8 */
    /* JADX WARN: Type inference failed for: r5v0 */
    /* JADX WARN: Type inference failed for: r5v1, types: [net.safemoon.androidwallet.viewmodels.blockChainClass.BlockTime] */
    /* JADX WARN: Type inference failed for: r5v14 */
    /* JADX WARN: Type inference failed for: r5v19 */
    /* JADX WARN: Type inference failed for: r5v2 */
    /* JADX WARN: Type inference failed for: r5v3 */
    /*
        Code decompiled incorrectly, please refer to instructions dump.
        To view partially-correct code enable 'Show inconsistent code' option in preferences
    */
    public final java.lang.Object z(java.util.Date r20, boolean r21, defpackage.q70<? super defpackage.k83> r22) {
        /*
            Method dump skipped, instructions count: 463
            To view this dump change 'Code comments level' option to 'DEBUG'
        */
        throw new UnsupportedOperationException("Method not decompiled: net.safemoon.androidwallet.viewmodels.blockChainClass.BlockTime.z(java.util.Date, boolean, q70):java.lang.Object");
    }
}
