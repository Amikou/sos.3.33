package net.safemoon.androidwallet.viewmodels.blockChainClass;

import android.app.Application;
import java.math.BigDecimal;
import java.math.BigInteger;
import kotlin.coroutines.jvm.internal.SuspendLambda;
import kotlin.coroutines.jvm.internal.a;
import net.safemoon.androidwallet.model.token.abstraction.IToken;
import net.safemoon.androidwallet.model.token.gson.GsonToken;
import org.web3j.protocol.core.DefaultBlockParameterName;

/* compiled from: Transfer.kt */
/* loaded from: classes2.dex */
public final class Transfer extends MyWeb3 {
    public final Application i;
    public final IToken j;
    public final String k;
    public final double l;
    public final sy1 m;
    public final sy1 n;
    public final sy1 o;
    public ow0 p;

    /* compiled from: Transfer.kt */
    @a(c = "net.safemoon.androidwallet.viewmodels.blockChainClass.Transfer$1", f = "Transfer.kt", l = {}, m = "invokeSuspend")
    /* renamed from: net.safemoon.androidwallet.viewmodels.blockChainClass.Transfer$1  reason: invalid class name */
    /* loaded from: classes2.dex */
    public static final class AnonymousClass1 extends SuspendLambda implements hd1<c90, q70<? super te4>, Object> {
        public int label;

        public AnonymousClass1(q70<? super AnonymousClass1> q70Var) {
            super(2, q70Var);
        }

        @Override // kotlin.coroutines.jvm.internal.BaseContinuationImpl
        public final q70<te4> create(Object obj, q70<?> q70Var) {
            return new AnonymousClass1(q70Var);
        }

        @Override // defpackage.hd1
        public final Object invoke(c90 c90Var, q70<? super te4> q70Var) {
            return ((AnonymousClass1) create(c90Var, q70Var)).invokeSuspend(te4.a);
        }

        @Override // kotlin.coroutines.jvm.internal.BaseContinuationImpl
        public final Object invokeSuspend(Object obj) {
            gs1.d();
            if (this.label == 0) {
                o83.b(obj);
                return te4.a;
            }
            throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        }
    }

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public Transfer(Application application, IToken iToken, String str, double d) {
        super(application, iToken);
        fs1.f(application, "application");
        fs1.f(iToken, "iToken");
        fs1.f(str, "receiver");
        this.i = application;
        this.j = iToken;
        this.k = str;
        this.l = d;
        this.m = zy1.a(Transfer$chainGasToken$2.INSTANCE);
        this.n = zy1.a(Transfer$ethFiler$2.INSTANCE);
        this.o = zy1.a(new Transfer$ercTransferEvent$2(this));
        as.b(qg1.a, null, null, new AnonymousClass1(null), 3, null);
    }

    public final qw0 A() {
        return (qw0) this.n.getValue();
    }

    public final Object B(q70<? super BigInteger> q70Var) {
        try {
            return t().ethGetTransactionCount(g().getAddress(), DefaultBlockParameterName.PENDING).send().getTransactionCount();
        } catch (Exception unused) {
            return null;
        }
    }

    public final Object C(String str, q70<? super zw0> q70Var) {
        try {
            return t().ethGetTransactionReceipt(str).send();
        } catch (Exception unused) {
            return null;
        }
    }

    public final String D() {
        return this.k;
    }

    public final BigInteger E() {
        return e30.i0(new BigDecimal(String.valueOf(this.l)), o().getDecimals());
    }

    /* JADX WARN: Removed duplicated region for block: B:10:0x0026  */
    /* JADX WARN: Removed duplicated region for block: B:24:0x0063  */
    /* JADX WARN: Removed duplicated region for block: B:33:0x00a3 A[Catch: Exception -> 0x019c, TryCatch #0 {Exception -> 0x019c, blocks: (B:14:0x003c, B:46:0x0163, B:48:0x0197, B:19:0x0054, B:39:0x00da, B:41:0x011d, B:22:0x005f, B:31:0x008a, B:33:0x00a3, B:35:0x00ca, B:34:0x00bd, B:25:0x0066, B:27:0x006c, B:42:0x0123), top: B:52:0x0024 }] */
    /* JADX WARN: Removed duplicated region for block: B:34:0x00bd A[Catch: Exception -> 0x019c, TryCatch #0 {Exception -> 0x019c, blocks: (B:14:0x003c, B:46:0x0163, B:48:0x0197, B:19:0x0054, B:39:0x00da, B:41:0x011d, B:22:0x005f, B:31:0x008a, B:33:0x00a3, B:35:0x00ca, B:34:0x00bd, B:25:0x0066, B:27:0x006c, B:42:0x0123), top: B:52:0x0024 }] */
    /* JADX WARN: Removed duplicated region for block: B:37:0x00d6 A[RETURN] */
    /* JADX WARN: Removed duplicated region for block: B:38:0x00d7  */
    /* JADX WARN: Removed duplicated region for block: B:41:0x011d A[Catch: Exception -> 0x019c, TryCatch #0 {Exception -> 0x019c, blocks: (B:14:0x003c, B:46:0x0163, B:48:0x0197, B:19:0x0054, B:39:0x00da, B:41:0x011d, B:22:0x005f, B:31:0x008a, B:33:0x00a3, B:35:0x00ca, B:34:0x00bd, B:25:0x0066, B:27:0x006c, B:42:0x0123), top: B:52:0x0024 }] */
    /* JADX WARN: Removed duplicated region for block: B:48:0x0197 A[Catch: Exception -> 0x019c, TRY_LEAVE, TryCatch #0 {Exception -> 0x019c, blocks: (B:14:0x003c, B:46:0x0163, B:48:0x0197, B:19:0x0054, B:39:0x00da, B:41:0x011d, B:22:0x005f, B:31:0x008a, B:33:0x00a3, B:35:0x00ca, B:34:0x00bd, B:25:0x0066, B:27:0x006c, B:42:0x0123), top: B:52:0x0024 }] */
    /* JADX WARN: Removed duplicated region for block: B:53:? A[RETURN, SYNTHETIC] */
    /* JADX WARN: Removed duplicated region for block: B:54:? A[RETURN, SYNTHETIC] */
    /*
        Code decompiled incorrectly, please refer to instructions dump.
        To view partially-correct code enable 'Show inconsistent code' option in preferences
    */
    public final java.lang.Object F(defpackage.q70<? super java.lang.String> r14) {
        /*
            Method dump skipped, instructions count: 413
            To view this dump change 'Code comments level' option to 'DEBUG'
        */
        throw new UnsupportedOperationException("Method not decompiled: net.safemoon.androidwallet.viewmodels.blockChainClass.Transfer.F(q70):java.lang.Object");
    }

    @Override // net.safemoon.androidwallet.viewmodels.blockChainClass.MyWeb3
    public Application e() {
        return this.i;
    }

    @Override // net.safemoon.androidwallet.viewmodels.blockChainClass.MyWeb3
    public IToken o() {
        return this.j;
    }

    public final double v() {
        return this.l;
    }

    public final GsonToken w() {
        return (GsonToken) this.m.getValue();
    }

    /* JADX WARN: Removed duplicated region for block: B:10:0x002b  */
    /* JADX WARN: Removed duplicated region for block: B:29:0x0088  */
    /* JADX WARN: Removed duplicated region for block: B:36:0x00ad A[Catch: Exception -> 0x01c1, TryCatch #0 {Exception -> 0x01c1, blocks: (B:15:0x0047, B:52:0x01a1, B:53:0x01b0, B:21:0x006a, B:47:0x0139, B:24:0x0077, B:39:0x00ea, B:41:0x0103, B:43:0x011f, B:42:0x0112, B:27:0x0084, B:34:0x00a7, B:36:0x00ad, B:48:0x0159, B:30:0x008b), top: B:57:0x0029 }] */
    /* JADX WARN: Removed duplicated region for block: B:41:0x0103 A[Catch: Exception -> 0x01c1, TryCatch #0 {Exception -> 0x01c1, blocks: (B:15:0x0047, B:52:0x01a1, B:53:0x01b0, B:21:0x006a, B:47:0x0139, B:24:0x0077, B:39:0x00ea, B:41:0x0103, B:43:0x011f, B:42:0x0112, B:27:0x0084, B:34:0x00a7, B:36:0x00ad, B:48:0x0159, B:30:0x008b), top: B:57:0x0029 }] */
    /* JADX WARN: Removed duplicated region for block: B:42:0x0112 A[Catch: Exception -> 0x01c1, TryCatch #0 {Exception -> 0x01c1, blocks: (B:15:0x0047, B:52:0x01a1, B:53:0x01b0, B:21:0x006a, B:47:0x0139, B:24:0x0077, B:39:0x00ea, B:41:0x0103, B:43:0x011f, B:42:0x0112, B:27:0x0084, B:34:0x00a7, B:36:0x00ad, B:48:0x0159, B:30:0x008b), top: B:57:0x0029 }] */
    /* JADX WARN: Removed duplicated region for block: B:45:0x0132 A[RETURN] */
    /* JADX WARN: Removed duplicated region for block: B:46:0x0133  */
    /* JADX WARN: Removed duplicated region for block: B:48:0x0159 A[Catch: Exception -> 0x01c1, TryCatch #0 {Exception -> 0x01c1, blocks: (B:15:0x0047, B:52:0x01a1, B:53:0x01b0, B:21:0x006a, B:47:0x0139, B:24:0x0077, B:39:0x00ea, B:41:0x0103, B:43:0x011f, B:42:0x0112, B:27:0x0084, B:34:0x00a7, B:36:0x00ad, B:48:0x0159, B:30:0x008b), top: B:57:0x0029 }] */
    /*
        Code decompiled incorrectly, please refer to instructions dump.
        To view partially-correct code enable 'Show inconsistent code' option in preferences
    */
    public final java.lang.Object x(defpackage.q70<? super defpackage.ow0> r20) {
        /*
            Method dump skipped, instructions count: 450
            To view this dump change 'Code comments level' option to 'DEBUG'
        */
        throw new UnsupportedOperationException("Method not decompiled: net.safemoon.androidwallet.viewmodels.blockChainClass.Transfer.x(q70):java.lang.Object");
    }

    public final BigInteger y() {
        ow0 ow0Var = this.p;
        if (ow0Var == null) {
            return null;
        }
        return e30.J(ow0Var);
    }

    public final Double z() {
        BigInteger J;
        BigInteger multiply;
        BigDecimal r;
        ow0 ow0Var = this.p;
        if (ow0Var == null || (J = e30.J(ow0Var)) == null || (multiply = J.multiply(MyWeb3.l(this, null, 1, null))) == null || (r = e30.r(multiply, w().getDecimals())) == null) {
            return null;
        }
        return Double.valueOf(r.doubleValue());
    }
}
