package net.safemoon.androidwallet.viewmodels.blockChainClass;

import kotlin.coroutines.jvm.internal.ContinuationImpl;
import kotlin.coroutines.jvm.internal.a;

/* compiled from: NFT721.kt */
@a(c = "net.safemoon.androidwallet.viewmodels.blockChainClass.NFT721", f = "NFT721.kt", l = {50, 55}, m = "safeTransferFrom")
/* loaded from: classes2.dex */
public final class NFT721$safeTransferFrom$1 extends ContinuationImpl {
    public double D$0;
    public Object L$0;
    public Object L$1;
    public Object L$2;
    public Object L$3;
    public int label;
    public /* synthetic */ Object result;
    public final /* synthetic */ NFT721 this$0;

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public NFT721$safeTransferFrom$1(NFT721 nft721, q70<? super NFT721$safeTransferFrom$1> q70Var) {
        super(q70Var);
        this.this$0 = nft721;
    }

    @Override // kotlin.coroutines.jvm.internal.BaseContinuationImpl
    public final Object invokeSuspend(Object obj) {
        this.result = obj;
        this.label |= Integer.MIN_VALUE;
        return this.this$0.b(null, null, null, this);
    }
}
