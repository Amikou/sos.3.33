package net.safemoon.androidwallet.viewmodels.blockChainClass;

import java.math.BigInteger;
import kotlin.jvm.internal.Lambda;
import net.safemoon.androidwallet.ERC721;

/* compiled from: NFT721.kt */
/* loaded from: classes2.dex */
public final class NFT721$erc721$2 extends Lambda implements rc1<ERC721> {
    public final /* synthetic */ NFT721 this$0;

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public NFT721$erc721$2(NFT721 nft721) {
        super(0);
        this.this$0 = nft721;
    }

    @Override // defpackage.rc1
    public final ERC721 invoke() {
        String z = this.this$0.z();
        ko4 y = this.this$0.y();
        ma0 j = this.this$0.j();
        NFT721 nft721 = this.this$0;
        BigInteger l = nft721.l();
        fs1.e(l, "GAS_LIMIT");
        return ERC721.n(z, y, j, nft721.p(l));
    }
}
