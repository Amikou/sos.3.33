package net.safemoon.androidwallet.viewmodels.blockChainClass;

import com.github.mikephil.charting.utils.Utils;
import kotlin.jvm.internal.Lambda;
import net.safemoon.androidwallet.model.token.gson.GsonToken;

/* compiled from: Transfer.kt */
/* loaded from: classes2.dex */
public final class Transfer$chainGasToken$2 extends Lambda implements rc1<GsonToken> {
    public static final Transfer$chainGasToken$2 INSTANCE = new Transfer$chainGasToken$2();

    public Transfer$chainGasToken$2() {
        super(0);
    }

    @Override // defpackage.rc1
    public final GsonToken invoke() {
        return new GsonToken("", "", "iconResName", "", 0, 18, false, "", 0, null, Utils.DOUBLE_EPSILON, Utils.DOUBLE_EPSILON, Utils.DOUBLE_EPSILON, 768, null);
    }
}
