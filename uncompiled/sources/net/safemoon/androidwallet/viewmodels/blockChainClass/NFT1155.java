package net.safemoon.androidwallet.viewmodels.blockChainClass;

import java.math.BigInteger;
import net.safemoon.androidwallet.ERC1155;
import net.safemoon.androidwallet.model.wallets.Wallet;

/* compiled from: NFT1155.kt */
/* loaded from: classes2.dex */
public final class NFT1155 extends WalletWeb3 implements im1 {
    public final String i;
    public final int j;
    public final Wallet k;
    public final sy1 l;

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public NFT1155(String str, int i, Wallet wallet2) {
        super(i, wallet2);
        fs1.f(str, "contractAddress");
        fs1.f(wallet2, "wallet");
        this.i = str;
        this.j = i;
        this.k = wallet2;
        this.l = zy1.a(new NFT1155$erc1155$2(this));
    }

    public final String A() {
        return this.i;
    }

    public final ERC1155 B() {
        return (ERC1155) this.l.getValue();
    }

    @Override // defpackage.im1
    public int a() {
        return i();
    }

    /* JADX WARN: Removed duplicated region for block: B:10:0x0029  */
    /* JADX WARN: Removed duplicated region for block: B:16:0x0069  */
    @Override // defpackage.im1
    /*
        Code decompiled incorrectly, please refer to instructions dump.
        To view partially-correct code enable 'Show inconsistent code' option in preferences
    */
    public java.lang.Object b(java.lang.String r17, java.math.BigInteger r18, java.math.BigInteger r19, defpackage.q70<? super defpackage.hx0> r20) {
        /*
            Method dump skipped, instructions count: 239
            To view this dump change 'Code comments level' option to 'DEBUG'
        */
        throw new UnsupportedOperationException("Method not decompiled: net.safemoon.androidwallet.viewmodels.blockChainClass.NFT1155.b(java.lang.String, java.math.BigInteger, java.math.BigInteger, q70):java.lang.Object");
    }

    /* JADX WARN: Removed duplicated region for block: B:10:0x0023  */
    /* JADX WARN: Removed duplicated region for block: B:14:0x0044  */
    @Override // defpackage.im1
    /*
        Code decompiled incorrectly, please refer to instructions dump.
        To view partially-correct code enable 'Show inconsistent code' option in preferences
    */
    public java.lang.Object c(java.lang.String r11, java.math.BigInteger r12, java.math.BigInteger r13, boolean r14, defpackage.q70<? super defpackage.ow0> r15) {
        /*
            r10 = this;
            boolean r0 = r15 instanceof net.safemoon.androidwallet.viewmodels.blockChainClass.NFT1155$estimateGas$1
            if (r0 == 0) goto L13
            r0 = r15
            net.safemoon.androidwallet.viewmodels.blockChainClass.NFT1155$estimateGas$1 r0 = (net.safemoon.androidwallet.viewmodels.blockChainClass.NFT1155$estimateGas$1) r0
            int r1 = r0.label
            r2 = -2147483648(0xffffffff80000000, float:-0.0)
            r3 = r1 & r2
            if (r3 == 0) goto L13
            int r1 = r1 - r2
            r0.label = r1
            goto L18
        L13:
            net.safemoon.androidwallet.viewmodels.blockChainClass.NFT1155$estimateGas$1 r0 = new net.safemoon.androidwallet.viewmodels.blockChainClass.NFT1155$estimateGas$1
            r0.<init>(r10, r15)
        L18:
            java.lang.Object r15 = r0.result
            java.lang.Object r1 = defpackage.gs1.d()
            int r2 = r0.label
            r3 = 1
            if (r2 == 0) goto L44
            if (r2 != r3) goto L3c
            java.lang.Object r11 = r0.L$3
            java.math.BigInteger r11 = (java.math.BigInteger) r11
            java.lang.Object r12 = r0.L$2
            java.math.BigInteger r12 = (java.math.BigInteger) r12
            java.lang.Object r13 = r0.L$1
            java.lang.String r13 = (java.lang.String) r13
            java.lang.Object r14 = r0.L$0
            net.safemoon.androidwallet.viewmodels.blockChainClass.NFT1155 r14 = (net.safemoon.androidwallet.viewmodels.blockChainClass.NFT1155) r14
            defpackage.o83.b(r15)
            r9 = r13
            r13 = r11
            r11 = r9
            goto L5b
        L3c:
            java.lang.IllegalStateException r11 = new java.lang.IllegalStateException
            java.lang.String r12 = "call to 'resume' before 'invoke' with coroutine"
            r11.<init>(r12)
            throw r11
        L44:
            defpackage.o83.b(r15)
            if (r14 == 0) goto L5a
            r0.L$0 = r10
            r0.L$1 = r11
            r0.L$2 = r12
            r0.L$3 = r13
            r0.label = r3
            java.lang.Object r14 = r10.g(r0)
            if (r14 != r1) goto L5a
            return r1
        L5a:
            r14 = r10
        L5b:
            r2 = r11
            r3 = r12
            r4 = r13
            net.safemoon.androidwallet.ERC1155 r0 = r14.B()
            ma0 r11 = r14.j()
            java.lang.String r1 = r11.getAddress()
            java.lang.String r11 = "0x01"
            byte[] r5 = defpackage.ej2.hexStringToByteArray(r11)
            org.web3j.protocol.core.DefaultBlockParameterName r11 = org.web3j.protocol.core.DefaultBlockParameterName.LATEST
            java.math.BigInteger r6 = r14.q(r11)
            java.math.BigInteger r7 = r14.getGasPrice()
            java.math.BigInteger r8 = r14.l()
            p84 r11 = r0.r(r1, r2, r3, r4, r5, r6, r7, r8)
            ko4 r12 = r14.y()
            org.web3j.protocol.core.c r11 = r12.ethEstimateGas(r11)
            i83 r11 = r11.send()
            ow0 r11 = (defpackage.ow0) r11
            r14.w(r11)
            ow0 r11 = r14.k()
            defpackage.fs1.d(r11)
            return r11
        */
        throw new UnsupportedOperationException("Method not decompiled: net.safemoon.androidwallet.viewmodels.blockChainClass.NFT1155.c(java.lang.String, java.math.BigInteger, java.math.BigInteger, boolean, q70):java.lang.Object");
    }

    @Override // net.safemoon.androidwallet.viewmodels.blockChainClass.WalletWeb3
    public int i() {
        return this.j;
    }

    @Override // net.safemoon.androidwallet.viewmodels.blockChainClass.WalletWeb3
    public Wallet u() {
        return this.k;
    }

    public final Object z(BigInteger bigInteger, q70<? super BigInteger> q70Var) {
        BigInteger send = B().n(j().getAddress(), bigInteger).send();
        fs1.e(send, "erc1155.balanceOf(creden….address, tokenId).send()");
        return send;
    }
}
