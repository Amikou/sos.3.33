package net.safemoon.androidwallet.viewmodels.blockChainClass;

import kotlin.jvm.internal.Lambda;

/* compiled from: MyWeb3.kt */
/* loaded from: classes2.dex */
public final class MyWeb3$credentials$2 extends Lambda implements rc1<ma0> {
    public final /* synthetic */ MyWeb3 this$0;

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public MyWeb3$credentials$2(MyWeb3 myWeb3) {
        super(0);
        this.this$0 = myWeb3;
    }

    @Override // defpackage.rc1
    public final ma0 invoke() {
        return ma0.create(this.this$0.q());
    }
}
