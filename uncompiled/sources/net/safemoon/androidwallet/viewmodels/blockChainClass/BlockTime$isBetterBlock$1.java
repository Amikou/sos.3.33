package net.safemoon.androidwallet.viewmodels.blockChainClass;

import kotlin.coroutines.jvm.internal.ContinuationImpl;
import kotlin.coroutines.jvm.internal.a;

/* compiled from: BlockTime.kt */
@a(c = "net.safemoon.androidwallet.viewmodels.blockChainClass.BlockTime", f = "BlockTime.kt", l = {88, 92}, m = "isBetterBlock")
/* loaded from: classes2.dex */
public final class BlockTime$isBetterBlock$1 extends ContinuationImpl {
    public Object L$0;
    public Object L$1;
    public Object L$2;
    public int label;
    public /* synthetic */ Object result;
    public final /* synthetic */ BlockTime this$0;

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public BlockTime$isBetterBlock$1(BlockTime blockTime, q70<? super BlockTime$isBetterBlock$1> q70Var) {
        super(q70Var);
        this.this$0 = blockTime;
    }

    @Override // kotlin.coroutines.jvm.internal.BaseContinuationImpl
    public final Object invokeSuspend(Object obj) {
        this.result = obj;
        this.label |= Integer.MIN_VALUE;
        return this.this$0.C(null, null, false, this);
    }
}
