package net.safemoon.androidwallet.viewmodels.blockChainClass;

import kotlin.coroutines.jvm.internal.ContinuationImpl;
import kotlin.coroutines.jvm.internal.a;

/* compiled from: NFT1155.kt */
@a(c = "net.safemoon.androidwallet.viewmodels.blockChainClass.NFT1155", f = "NFT1155.kt", l = {26}, m = "estimateGas")
/* loaded from: classes2.dex */
public final class NFT1155$estimateGas$1 extends ContinuationImpl {
    public Object L$0;
    public Object L$1;
    public Object L$2;
    public Object L$3;
    public int label;
    public /* synthetic */ Object result;
    public final /* synthetic */ NFT1155 this$0;

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public NFT1155$estimateGas$1(NFT1155 nft1155, q70<? super NFT1155$estimateGas$1> q70Var) {
        super(q70Var);
        this.this$0 = nft1155;
    }

    @Override // kotlin.coroutines.jvm.internal.BaseContinuationImpl
    public final Object invokeSuspend(Object obj) {
        this.result = obj;
        this.label |= Integer.MIN_VALUE;
        return this.this$0.c(null, null, null, false, this);
    }
}
