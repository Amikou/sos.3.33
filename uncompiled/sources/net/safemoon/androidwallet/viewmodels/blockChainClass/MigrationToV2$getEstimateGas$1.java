package net.safemoon.androidwallet.viewmodels.blockChainClass;

import kotlin.coroutines.jvm.internal.ContinuationImpl;
import kotlin.coroutines.jvm.internal.a;

/* compiled from: MigrationToV2.kt */
@a(c = "net.safemoon.androidwallet.viewmodels.blockChainClass.MigrationToV2", f = "MigrationToV2.kt", l = {186, 189}, m = "getEstimateGas")
/* loaded from: classes2.dex */
public final class MigrationToV2$getEstimateGas$1 extends ContinuationImpl {
    public Object L$0;
    public Object L$1;
    public Object L$2;
    public Object L$3;
    public Object L$4;
    public Object L$5;
    public int label;
    public /* synthetic */ Object result;
    public final /* synthetic */ MigrationToV2 this$0;

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public MigrationToV2$getEstimateGas$1(MigrationToV2 migrationToV2, q70<? super MigrationToV2$getEstimateGas$1> q70Var) {
        super(q70Var);
        this.this$0 = migrationToV2;
    }

    @Override // kotlin.coroutines.jvm.internal.BaseContinuationImpl
    public final Object invokeSuspend(Object obj) {
        this.result = obj;
        this.label |= Integer.MIN_VALUE;
        return this.this$0.k(this);
    }
}
