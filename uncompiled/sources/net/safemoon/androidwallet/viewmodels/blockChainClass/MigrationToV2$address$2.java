package net.safemoon.androidwallet.viewmodels.blockChainClass;

import android.app.Application;
import kotlin.jvm.internal.Lambda;

/* compiled from: MigrationToV2.kt */
/* loaded from: classes2.dex */
public final class MigrationToV2$address$2 extends Lambda implements rc1<String> {
    public final /* synthetic */ MigrationToV2 this$0;

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public MigrationToV2$address$2(MigrationToV2 migrationToV2) {
        super(0);
        this.this$0 = migrationToV2;
    }

    @Override // defpackage.rc1
    public final String invoke() {
        Application application;
        application = this.this$0.a;
        return bo3.i(application, "SAFEMOON_ADDRESS");
    }
}
