package net.safemoon.androidwallet.viewmodels.blockChainClass;

import java.math.BigInteger;
import kotlin.jvm.internal.Lambda;
import net.safemoon.androidwallet.ERC1155;

/* compiled from: NFT1155.kt */
/* loaded from: classes2.dex */
public final class NFT1155$erc1155$2 extends Lambda implements rc1<ERC1155> {
    public final /* synthetic */ NFT1155 this$0;

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public NFT1155$erc1155$2(NFT1155 nft1155) {
        super(0);
        this.this$0 = nft1155;
    }

    @Override // defpackage.rc1
    public final ERC1155 invoke() {
        String A = this.this$0.A();
        ko4 y = this.this$0.y();
        ma0 j = this.this$0.j();
        NFT1155 nft1155 = this.this$0;
        BigInteger l = nft1155.l();
        fs1.e(l, "GAS_LIMIT");
        return ERC1155.p(A, y, j, nft1155.p(l));
    }
}
