package net.safemoon.androidwallet.viewmodels.blockChainClass;

import kotlin.coroutines.jvm.internal.ContinuationImpl;
import kotlin.coroutines.jvm.internal.a;

/* compiled from: BlockTime.kt */
@a(c = "net.safemoon.androidwallet.viewmodels.blockChainClass.BlockTime", f = "BlockTime.kt", l = {23, 24}, m = "getBlockTime")
/* loaded from: classes2.dex */
public final class BlockTime$getBlockTime$1 extends ContinuationImpl {
    public Object L$0;
    public Object L$1;
    public int label;
    public /* synthetic */ Object result;
    public final /* synthetic */ BlockTime this$0;

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public BlockTime$getBlockTime$1(BlockTime blockTime, q70<? super BlockTime$getBlockTime$1> q70Var) {
        super(q70Var);
        this.this$0 = blockTime;
    }

    @Override // kotlin.coroutines.jvm.internal.BaseContinuationImpl
    public final Object invokeSuspend(Object obj) {
        this.result = obj;
        this.label |= Integer.MIN_VALUE;
        return this.this$0.x(this);
    }
}
