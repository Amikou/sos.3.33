package net.safemoon.androidwallet.viewmodels.blockChainClass;

import kotlin.jvm.internal.Lambda;

/* compiled from: WalletWeb3.kt */
/* loaded from: classes2.dex */
public final class WalletWeb3$credentails$2 extends Lambda implements rc1<ma0> {
    public final /* synthetic */ WalletWeb3 this$0;

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public WalletWeb3$credentails$2(WalletWeb3 walletWeb3) {
        super(0);
        this.this$0 = walletWeb3;
    }

    @Override // defpackage.rc1
    public final ma0 invoke() {
        String s;
        s = this.this$0.s();
        return ma0.create(s);
    }
}
