package net.safemoon.androidwallet.viewmodels.blockChainClass;

import android.app.Application;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.Locale;
import net.safemoon.androidwallet.ERC20;
import net.safemoon.androidwallet.Migration;
import net.safemoon.androidwallet.Safemoon;
import net.safemoon.androidwallet.model.common.Gas;
import net.safemoon.androidwallet.model.common.GasPrice;
import net.safemoon.androidwallet.model.swap.Swap;
import net.safemoon.androidwallet.viewmodels.SwapMigrationViewModel;
import org.web3j.protocol.core.DefaultBlockParameterName;

/* compiled from: MigrationToV2.kt */
/* loaded from: classes2.dex */
public final class MigrationToV2 {
    public static final a m = new a(null);
    public static final String n;
    public static final String o;
    public static final String p;
    public static final String q;
    public final Application a;
    public final Swap b;
    public final Swap c;
    public final String d;
    public final sy1 e;
    public final sy1 f;
    public final sy1 g;
    public final GasPrice h;
    public final BigInteger i;
    public final gb2<Gas> j;
    public SwapMigrationViewModel.a k;
    public ow0 l;

    /* compiled from: MigrationToV2.kt */
    /* loaded from: classes2.dex */
    public static final class a {
        public a() {
        }

        public /* synthetic */ a(qi0 qi0Var) {
            this();
        }

        public final String a() {
            return MigrationToV2.p;
        }

        public final String b() {
            return MigrationToV2.q;
        }
    }

    static {
        Boolean bool = zr.b;
        fs1.e(bool, "IS_TEST_NET");
        String str = bool.booleanValue() ? "0xDD6999Ec25948811d7c671051f5B4E44B175239e" : "0x8076c74c5e3f5852037f31ff0093eeb8c8add8d3";
        Locale locale = Locale.ROOT;
        String lowerCase = str.toLowerCase(locale);
        fs1.e(lowerCase, "(this as java.lang.Strin….toLowerCase(Locale.ROOT)");
        n = lowerCase;
        fs1.e(bool, "IS_TEST_NET");
        String lowerCase2 = (bool.booleanValue() ? "0x409b3718F4090EBf7dAc8185a6e8336407d9d3a3" : "0x42981d0bfbAf196529376EE702F2a9Eb9092fcB5").toLowerCase(locale);
        fs1.e(lowerCase2, "(this as java.lang.Strin….toLowerCase(Locale.ROOT)");
        o = lowerCase2;
        StringBuilder sb = new StringBuilder();
        sb.append("{ \"name\": \"SafeMoon\", \"symbolWithType\": \"BEP_SAFEMOON\", \"symbol\": \"SAFEMOON\", \"address\": \"");
        sb.append(lowerCase);
        sb.append("\", \"icon\": \"safemoon\", \"chainId\": ");
        fs1.e(bool, "IS_TEST_NET");
        sb.append(bool.booleanValue() ? 97 : 56);
        sb.append(", \"decimals\": 9, \"allowSwap\": true, \"logoURI\":\"https://swap.safemoon.net/images/SAFEMOON.png\" }");
        p = sb.toString();
        StringBuilder sb2 = new StringBuilder();
        sb2.append("{ \"name\": \"SafeMoonV2\", \"symbolWithType\": \"BEP_SAFEMOON_V2\", \"symbol\": \"SFM\", \"address\": \"");
        sb2.append(lowerCase2);
        sb2.append("\", \"icon\": \"safemoon\", \"chainId\": ");
        fs1.e(bool, "IS_TEST_NET");
        sb2.append(bool.booleanValue() ? 97 : 56);
        sb2.append(", \"decimals\": 9, \"allowSwap\": true, \"logoURI\":\"https://swap.safemoon.net/images/SAFEMOON.png\" }");
        q = sb2.toString();
    }

    public MigrationToV2(Application application, Swap swap, Swap swap2) {
        fs1.f(application, "application");
        fs1.f(swap, "inToken");
        fs1.f(swap2, "outToken");
        this.a = application;
        this.b = swap;
        this.c = swap2;
        Boolean bool = zr.b;
        fs1.e(bool, "IS_TEST_NET");
        String lowerCase = (bool.booleanValue() ? "0x1f3E730542610ac6c16b00b9931535A2DD39844F" : "0x9d50518de14f89836f2b9b9ac05f177de7bf521a").toLowerCase(Locale.ROOT);
        fs1.e(lowerCase, "(this as java.lang.Strin….toLowerCase(Locale.ROOT)");
        this.d = lowerCase;
        this.e = zy1.a(new MigrationToV2$privateKey$2(this));
        this.f = zy1.a(new MigrationToV2$address$2(this));
        this.g = zy1.a(new MigrationToV2$credentails$2(this));
        this.h = new GasPrice();
        this.i = BigInteger.valueOf(9000000L);
        this.j = new gb2<>(Gas.Standard);
        new gb2(20);
        f();
    }

    /* JADX WARN: Removed duplicated region for block: B:10:0x0024  */
    /* JADX WARN: Removed duplicated region for block: B:16:0x0054  */
    /* JADX WARN: Removed duplicated region for block: B:22:0x00b8 A[RETURN] */
    /* JADX WARN: Removed duplicated region for block: B:23:0x00b9  */
    /*
        Code decompiled incorrectly, please refer to instructions dump.
        To view partially-correct code enable 'Show inconsistent code' option in preferences
    */
    public final java.lang.Object e(defpackage.q70<? super defpackage.hx0> r14) {
        /*
            Method dump skipped, instructions count: 237
            To view this dump change 'Code comments level' option to 'DEBUG'
        */
        throw new UnsupportedOperationException("Method not decompiled: net.safemoon.androidwallet.viewmodels.blockChainClass.MigrationToV2.e(q70):java.lang.Object");
    }

    public final boolean f() {
        String str = this.b.address;
        fs1.e(str, "inToken.address");
        Locale locale = Locale.ROOT;
        String lowerCase = str.toLowerCase(locale);
        fs1.e(lowerCase, "(this as java.lang.Strin….toLowerCase(Locale.ROOT)");
        String str2 = this.b.address;
        fs1.e(str2, "inToken.address");
        String lowerCase2 = str2.toLowerCase(locale);
        fs1.e(lowerCase2, "(this as java.lang.Strin….toLowerCase(Locale.ROOT)");
        if (fs1.b(lowerCase, lowerCase2)) {
            String str3 = this.c.address;
            fs1.e(str3, "outToken.address");
            String lowerCase3 = str3.toLowerCase(locale);
            fs1.e(lowerCase3, "(this as java.lang.Strin….toLowerCase(Locale.ROOT)");
            String str4 = this.c.address;
            fs1.e(str4, "outToken.address");
            String lowerCase4 = str4.toLowerCase(locale);
            fs1.e(lowerCase4, "(this as java.lang.Strin….toLowerCase(Locale.ROOT)");
            if (fs1.b(lowerCase3, lowerCase4)) {
                return true;
            }
        }
        return false;
    }

    public final Object g(q70<? super w84> q70Var) {
        ow0 ow0Var;
        SwapMigrationViewModel.a aVar = this.k;
        if (aVar == null || (ow0Var = this.l) == null) {
            return null;
        }
        BigInteger amountUsed = ow0Var.getAmountUsed();
        fs1.e(amountUsed, "it1.amountUsed");
        Migration u = u(amountUsed);
        BigDecimal a2 = aVar.a();
        Integer num = this.b.decimals;
        fs1.e(num, "inToken.decimals");
        return u.p(e30.i0(a2, num.intValue())).send();
    }

    public final ERC20 h() {
        String str = this.b.address;
        fs1.e(str, "inToken.address");
        String lowerCase = str.toLowerCase(Locale.ROOT);
        fs1.e(lowerCase, "(this as java.lang.Strin….toLowerCase(Locale.ROOT)");
        ko4 w = w();
        ma0 j = j();
        BigInteger bigInteger = this.i;
        fs1.e(bigInteger, "GAS_LIMIT");
        return ERC20.s(lowerCase, w, j, m(bigInteger));
    }

    public final Object i(SwapMigrationViewModel.a aVar, q70<? super BigDecimal> q70Var) {
        return aVar.a().divide(new BigDecimal(1000));
    }

    public final ma0 j() {
        return (ma0) this.g.getValue();
    }

    /* JADX WARN: Removed duplicated region for block: B:10:0x0024  */
    /* JADX WARN: Removed duplicated region for block: B:16:0x005b  */
    /* JADX WARN: Removed duplicated region for block: B:25:0x00c8 A[RETURN] */
    /* JADX WARN: Removed duplicated region for block: B:26:0x00c9  */
    /*
        Code decompiled incorrectly, please refer to instructions dump.
        To view partially-correct code enable 'Show inconsistent code' option in preferences
    */
    public final java.lang.Object k(defpackage.q70<? super defpackage.ow0> r10) {
        /*
            Method dump skipped, instructions count: 230
            To view this dump change 'Code comments level' option to 'DEBUG'
        */
        throw new UnsupportedOperationException("Method not decompiled: net.safemoon.androidwallet.viewmodels.blockChainClass.MigrationToV2.k(q70):java.lang.Object");
    }

    public final BigInteger l() {
        if (r()) {
            GasPrice gasPrice = this.h;
            Gas value = this.j.getValue();
            fs1.d(value);
            fs1.e(value, "gas.value!!");
            Integer num = this.b.chainId;
            fs1.e(num, "inToken.chainId");
            return new BigDecimal(gasPrice.getPrice(value, num.intValue())).multiply(BigDecimal.TEN.pow(9)).toBigInteger();
        }
        GasPrice gasPrice2 = this.h;
        Gas value2 = this.j.getValue();
        fs1.d(value2);
        fs1.e(value2, "gas.value!!");
        Integer num2 = this.b.chainId;
        fs1.e(num2, "inToken.chainId");
        return new BigDecimal(gasPrice2.getPrice(value2, num2.intValue())).multiply(BigDecimal.TEN.pow(9)).toBigInteger();
    }

    public final j80 m(BigInteger bigInteger) {
        return new it3(l(), bigInteger);
    }

    public final Object n(q70<? super BigInteger> q70Var) {
        BigInteger transactionCount = w().ethGetTransactionCount(j().getAddress(), DefaultBlockParameterName.PENDING).send().getTransactionCount();
        fs1.e(transactionCount, "ethGetTransactionCount.transactionCount");
        return transactionCount;
    }

    public final String o() {
        return (String) this.e.getValue();
    }

    public final String p() {
        Integer num = this.b.chainId;
        fs1.e(num, "inToken.chainId");
        return e30.z(num.intValue());
    }

    public final Object q(SwapMigrationViewModel.a aVar, q70<? super Boolean> q70Var) {
        this.k = aVar;
        BigDecimal a2 = aVar.a();
        Integer num = this.b.decimals;
        fs1.e(num, "inToken.decimals");
        return hr.a(h().o(j().getAddress(), this.d).send().compareTo(e30.i0(a2, num.intValue())) >= 0);
    }

    public final boolean r() {
        Integer num = this.b.chainId;
        if (num != null && num.intValue() == 1) {
            return true;
        }
        return num != null && num.intValue() == 3;
    }

    public final Object s(q70<? super Boolean> q70Var) {
        BigInteger bigInteger = this.i;
        fs1.e(bigInteger, "GAS_LIMIT");
        return u(bigInteger).n().send();
    }

    public final Object t(q70<? super Boolean> q70Var) {
        BigInteger bigInteger = this.i;
        fs1.e(bigInteger, "GAS_LIMIT");
        return v(bigInteger).n().send();
    }

    public final Migration u(BigInteger bigInteger) {
        return Migration.o(this.d, w(), j(), m(bigInteger));
    }

    public final Safemoon v(BigInteger bigInteger) {
        String str = this.c.address;
        fs1.e(str, "outToken.address");
        String lowerCase = str.toLowerCase(Locale.ROOT);
        fs1.e(lowerCase, "(this as java.lang.Strin….toLowerCase(Locale.ROOT)");
        return Safemoon.o(lowerCase, w(), j(), m(bigInteger));
    }

    public final ko4 w() {
        return jo4.a(new ml1(p()));
    }
}
