package net.safemoon.androidwallet.viewmodels.blockChainClass;

import kotlin.coroutines.jvm.internal.ContinuationImpl;
import kotlin.coroutines.jvm.internal.a;

/* compiled from: WalletWeb3.kt */
@a(c = "net.safemoon.androidwallet.viewmodels.blockChainClass.WalletWeb3", f = "WalletWeb3.kt", l = {78}, m = "getBalance")
/* loaded from: classes2.dex */
public final class WalletWeb3$getBalance$1 extends ContinuationImpl {
    public int label;
    public /* synthetic */ Object result;
    public final /* synthetic */ WalletWeb3 this$0;

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public WalletWeb3$getBalance$1(WalletWeb3 walletWeb3, q70<? super WalletWeb3$getBalance$1> q70Var) {
        super(q70Var);
        this.this$0 = walletWeb3;
    }

    @Override // kotlin.coroutines.jvm.internal.BaseContinuationImpl
    public final Object invokeSuspend(Object obj) {
        this.result = obj;
        this.label |= Integer.MIN_VALUE;
        return this.this$0.h(null, 0, this);
    }
}
