package net.safemoon.androidwallet.viewmodels;

import kotlin.jvm.internal.Lambda;

/* compiled from: SwapViewModel.kt */
/* loaded from: classes2.dex */
public final class SwapViewModel$address$2 extends Lambda implements rc1<String> {
    public final /* synthetic */ SwapViewModel this$0;

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public SwapViewModel$address$2(SwapViewModel swapViewModel) {
        super(0);
        this.this$0 = swapViewModel;
    }

    @Override // defpackage.rc1
    public final String invoke() {
        return bo3.i(this.this$0.a(), "SAFEMOON_ADDRESS");
    }
}
