package net.safemoon.androidwallet.viewmodels;

import kotlin.coroutines.jvm.internal.SuspendLambda;
import kotlin.coroutines.jvm.internal.a;
import kotlinx.coroutines.CoroutineDispatcher;

/* compiled from: SwapMigrationViewModel.kt */
@a(c = "net.safemoon.androidwallet.viewmodels.SwapMigrationViewModel$getAmountOut$1", f = "SwapMigrationViewModel.kt", l = {134}, m = "invokeSuspend")
/* loaded from: classes2.dex */
public final class SwapMigrationViewModel$getAmountOut$1 extends SuspendLambda implements hd1<c90, q70<? super te4>, Object> {
    public int label;
    public final /* synthetic */ SwapMigrationViewModel this$0;

    /* compiled from: SwapMigrationViewModel.kt */
    @a(c = "net.safemoon.androidwallet.viewmodels.SwapMigrationViewModel$getAmountOut$1$1", f = "SwapMigrationViewModel.kt", l = {136, 142, 148, 150, 156}, m = "invokeSuspend")
    /* renamed from: net.safemoon.androidwallet.viewmodels.SwapMigrationViewModel$getAmountOut$1$1  reason: invalid class name */
    /* loaded from: classes2.dex */
    public static final class AnonymousClass1 extends SuspendLambda implements hd1<c90, q70<? super te4>, Object> {
        public int label;
        public final /* synthetic */ SwapMigrationViewModel this$0;

        /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
        public AnonymousClass1(SwapMigrationViewModel swapMigrationViewModel, q70<? super AnonymousClass1> q70Var) {
            super(2, q70Var);
            this.this$0 = swapMigrationViewModel;
        }

        @Override // kotlin.coroutines.jvm.internal.BaseContinuationImpl
        public final q70<te4> create(Object obj, q70<?> q70Var) {
            return new AnonymousClass1(this.this$0, q70Var);
        }

        @Override // defpackage.hd1
        public final Object invoke(c90 c90Var, q70<? super te4> q70Var) {
            return ((AnonymousClass1) create(c90Var, q70Var)).invokeSuspend(te4.a);
        }

        /* JADX WARN: Removed duplicated region for block: B:34:0x009b A[Catch: all -> 0x003c, Exception -> 0x003f, TryCatch #0 {Exception -> 0x003f, blocks: (B:9:0x001d, B:51:0x0170, B:53:0x0174, B:56:0x017c, B:60:0x01d2, B:61:0x01f4, B:13:0x002a, B:45:0x013a, B:47:0x0142, B:48:0x0151, B:14:0x002f, B:42:0x0103, B:15:0x0034, B:32:0x008e, B:34:0x009b, B:35:0x00b8, B:37:0x00c4, B:39:0x00df, B:62:0x0215, B:16:0x0038, B:26:0x0057, B:28:0x005f, B:29:0x007c, B:23:0x0045), top: B:69:0x0011, outer: #1 }] */
        /* JADX WARN: Removed duplicated region for block: B:35:0x00b8 A[Catch: all -> 0x003c, Exception -> 0x003f, TryCatch #0 {Exception -> 0x003f, blocks: (B:9:0x001d, B:51:0x0170, B:53:0x0174, B:56:0x017c, B:60:0x01d2, B:61:0x01f4, B:13:0x002a, B:45:0x013a, B:47:0x0142, B:48:0x0151, B:14:0x002f, B:42:0x0103, B:15:0x0034, B:32:0x008e, B:34:0x009b, B:35:0x00b8, B:37:0x00c4, B:39:0x00df, B:62:0x0215, B:16:0x0038, B:26:0x0057, B:28:0x005f, B:29:0x007c, B:23:0x0045), top: B:69:0x0011, outer: #1 }] */
        /* JADX WARN: Removed duplicated region for block: B:44:0x0139 A[RETURN] */
        /* JADX WARN: Removed duplicated region for block: B:47:0x0142 A[Catch: all -> 0x003c, Exception -> 0x003f, TryCatch #0 {Exception -> 0x003f, blocks: (B:9:0x001d, B:51:0x0170, B:53:0x0174, B:56:0x017c, B:60:0x01d2, B:61:0x01f4, B:13:0x002a, B:45:0x013a, B:47:0x0142, B:48:0x0151, B:14:0x002f, B:42:0x0103, B:15:0x0034, B:32:0x008e, B:34:0x009b, B:35:0x00b8, B:37:0x00c4, B:39:0x00df, B:62:0x0215, B:16:0x0038, B:26:0x0057, B:28:0x005f, B:29:0x007c, B:23:0x0045), top: B:69:0x0011, outer: #1 }] */
        /* JADX WARN: Removed duplicated region for block: B:48:0x0151 A[Catch: all -> 0x003c, Exception -> 0x003f, TryCatch #0 {Exception -> 0x003f, blocks: (B:9:0x001d, B:51:0x0170, B:53:0x0174, B:56:0x017c, B:60:0x01d2, B:61:0x01f4, B:13:0x002a, B:45:0x013a, B:47:0x0142, B:48:0x0151, B:14:0x002f, B:42:0x0103, B:15:0x0034, B:32:0x008e, B:34:0x009b, B:35:0x00b8, B:37:0x00c4, B:39:0x00df, B:62:0x0215, B:16:0x0038, B:26:0x0057, B:28:0x005f, B:29:0x007c, B:23:0x0045), top: B:69:0x0011, outer: #1 }] */
        /* JADX WARN: Removed duplicated region for block: B:58:0x01cd  */
        /* JADX WARN: Removed duplicated region for block: B:59:0x01d0  */
        @Override // kotlin.coroutines.jvm.internal.BaseContinuationImpl
        /*
            Code decompiled incorrectly, please refer to instructions dump.
            To view partially-correct code enable 'Show inconsistent code' option in preferences
        */
        public final java.lang.Object invokeSuspend(java.lang.Object r12) {
            /*
                Method dump skipped, instructions count: 588
                To view this dump change 'Code comments level' option to 'DEBUG'
            */
            throw new UnsupportedOperationException("Method not decompiled: net.safemoon.androidwallet.viewmodels.SwapMigrationViewModel$getAmountOut$1.AnonymousClass1.invokeSuspend(java.lang.Object):java.lang.Object");
        }
    }

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public SwapMigrationViewModel$getAmountOut$1(SwapMigrationViewModel swapMigrationViewModel, q70<? super SwapMigrationViewModel$getAmountOut$1> q70Var) {
        super(2, q70Var);
        this.this$0 = swapMigrationViewModel;
    }

    @Override // kotlin.coroutines.jvm.internal.BaseContinuationImpl
    public final q70<te4> create(Object obj, q70<?> q70Var) {
        return new SwapMigrationViewModel$getAmountOut$1(this.this$0, q70Var);
    }

    @Override // defpackage.hd1
    public final Object invoke(c90 c90Var, q70<? super te4> q70Var) {
        return ((SwapMigrationViewModel$getAmountOut$1) create(c90Var, q70Var)).invokeSuspend(te4.a);
    }

    @Override // kotlin.coroutines.jvm.internal.BaseContinuationImpl
    public final Object invokeSuspend(Object obj) {
        Object d = gs1.d();
        int i = this.label;
        if (i == 0) {
            o83.b(obj);
            CoroutineDispatcher b = tp0.b();
            AnonymousClass1 anonymousClass1 = new AnonymousClass1(this.this$0, null);
            this.label = 1;
            if (kotlinx.coroutines.a.e(b, anonymousClass1, this) == d) {
                return d;
            }
        } else if (i != 1) {
            throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        } else {
            o83.b(obj);
        }
        return te4.a;
    }
}
