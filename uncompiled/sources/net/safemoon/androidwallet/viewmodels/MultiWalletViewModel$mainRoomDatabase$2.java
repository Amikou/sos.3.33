package net.safemoon.androidwallet.viewmodels;

import android.app.Application;
import kotlin.jvm.internal.Lambda;
import net.safemoon.androidwallet.database.mainRoom.MainRoomDatabase;

/* compiled from: MultiWalletViewModel.kt */
/* loaded from: classes2.dex */
public final class MultiWalletViewModel$mainRoomDatabase$2 extends Lambda implements rc1<MainRoomDatabase> {
    public final /* synthetic */ Application $application;

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public MultiWalletViewModel$mainRoomDatabase$2(Application application) {
        super(0);
        this.$application = application;
    }

    @Override // defpackage.rc1
    public final MainRoomDatabase invoke() {
        return MainRoomDatabase.n.b(this.$application);
    }
}
