package net.safemoon.androidwallet.viewmodels;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import kotlin.coroutines.jvm.internal.SuspendLambda;
import kotlin.coroutines.jvm.internal.a;
import net.safemoon.androidwallet.model.swap.AllSwapTokens;
import net.safemoon.androidwallet.model.swap.Data;
import net.safemoon.androidwallet.model.swap.Swap;
import net.safemoon.androidwallet.viewmodels.SwapViewModel;
import retrofit2.n;

/* compiled from: SwapViewModel.kt */
@a(c = "net.safemoon.androidwallet.viewmodels.SwapViewModel$loadTokens$1$onResponse$1", f = "SwapViewModel.kt", l = {}, m = "invokeSuspend")
/* loaded from: classes2.dex */
public final class SwapViewModel$loadTokens$1$onResponse$1 extends SuspendLambda implements hd1<c90, q70<? super te4>, Object> {
    public final /* synthetic */ n<AllSwapTokens> $response;
    public int label;
    public final /* synthetic */ SwapViewModel this$0;

    /* compiled from: SwapViewModel.kt */
    @a(c = "net.safemoon.androidwallet.viewmodels.SwapViewModel$loadTokens$1$onResponse$1$1", f = "SwapViewModel.kt", l = {}, m = "invokeSuspend")
    /* renamed from: net.safemoon.androidwallet.viewmodels.SwapViewModel$loadTokens$1$onResponse$1$1  reason: invalid class name */
    /* loaded from: classes2.dex */
    public static final class AnonymousClass1 extends SuspendLambda implements hd1<c90, q70<? super te4>, Object> {
        public int label;
        public final /* synthetic */ SwapViewModel this$0;

        /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
        public AnonymousClass1(SwapViewModel swapViewModel, q70<? super AnonymousClass1> q70Var) {
            super(2, q70Var);
            this.this$0 = swapViewModel;
        }

        @Override // kotlin.coroutines.jvm.internal.BaseContinuationImpl
        public final q70<te4> create(Object obj, q70<?> q70Var) {
            return new AnonymousClass1(this.this$0, q70Var);
        }

        @Override // defpackage.hd1
        public final Object invoke(c90 c90Var, q70<? super te4> q70Var) {
            return ((AnonymousClass1) create(c90Var, q70Var)).invokeSuspend(te4.a);
        }

        @Override // kotlin.coroutines.jvm.internal.BaseContinuationImpl
        public final Object invokeSuspend(Object obj) {
            gs1.d();
            if (this.label == 0) {
                o83.b(obj);
                SwapViewModel.c value = this.this$0.Z().getValue();
                if (value != null) {
                    this.this$0.J(value);
                }
                return te4.a;
            }
            throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        }
    }

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public SwapViewModel$loadTokens$1$onResponse$1(SwapViewModel swapViewModel, n<AllSwapTokens> nVar, q70<? super SwapViewModel$loadTokens$1$onResponse$1> q70Var) {
        super(2, q70Var);
        this.this$0 = swapViewModel;
        this.$response = nVar;
    }

    @Override // kotlin.coroutines.jvm.internal.BaseContinuationImpl
    public final q70<te4> create(Object obj, q70<?> q70Var) {
        return new SwapViewModel$loadTokens$1$onResponse$1(this.this$0, this.$response, q70Var);
    }

    @Override // defpackage.hd1
    public final Object invoke(c90 c90Var, q70<? super te4> q70Var) {
        return ((SwapViewModel$loadTokens$1$onResponse$1) create(c90Var, q70Var)).invokeSuspend(te4.a);
    }

    @Override // kotlin.coroutines.jvm.internal.BaseContinuationImpl
    public final Object invokeSuspend(Object obj) {
        Data data;
        List<Swap> tokens;
        gs1.d();
        if (this.label == 0) {
            o83.b(obj);
            ArrayList arrayList = new ArrayList();
            AllSwapTokens a = this.$response.a();
            if (a != null && (data = a.getData()) != null && (tokens = data.getTokens()) != null) {
                arrayList.addAll(b30.a.l());
                ArrayList arrayList2 = new ArrayList(c20.q(tokens, 10));
                for (Swap swap : tokens) {
                    String str = swap.address;
                    fs1.e(str, "it.address");
                    String lowerCase = str.toLowerCase(Locale.ROOT);
                    fs1.e(lowerCase, "(this as java.lang.Strin….toLowerCase(Locale.ROOT)");
                    if (fs1.b(lowerCase, "0x16631e53c20fd2670027c6d53efe2642929b285c")) {
                        swap.name = "pSafeMoon";
                    }
                    arrayList2.add(swap);
                }
                hr.a(arrayList.addAll(arrayList2));
            }
            Boolean bool = zr.b;
            fs1.e(bool, "IS_TEST_NET");
            if (bool.booleanValue()) {
                Swap swap2 = new Swap();
                swap2.name = "Smart Chain";
                swap2.decimals = hr.d(18);
                swap2.address = "";
                swap2.symbol = "BNB";
                swap2.chainId = hr.d(97);
                swap2.logoURI = "https://s2.coinmarketcap.com/static/img/coins/64x64/1839.png";
                te4 te4Var = te4.a;
                arrayList.add(swap2);
                Swap swap3 = new Swap();
                swap3.name = "Enhance Token";
                swap3.decimals = hr.d(9);
                swap3.address = "0xfdB829e3f1916633d8Ea0C5bA8FdFcF7f2D7A40b";
                swap3.symbol = "ENHANCE";
                swap3.chainId = hr.d(97);
                swap3.logoURI = "https://s2.coinmarketcap.com/static/img/coins/64x64/5994.png";
                arrayList.add(swap3);
                Swap swap4 = new Swap();
                swap4.name = "SUSHI";
                swap4.decimals = hr.d(18);
                swap4.address = "0x0fc943031CdB3C8393b8fC43328755c21CF3fC31";
                swap4.symbol = "SUSHI";
                swap4.chainId = hr.d(97);
                swap4.logoURI = "https://s2.coinmarketcap.com/static/img/coins/64x64/6758.png";
                arrayList.add(swap4);
            }
            this.this$0.W = arrayList;
            as.b(ej4.a(this.this$0), null, null, new AnonymousClass1(this.this$0, null), 3, null);
            return te4.a;
        }
        throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
    }
}
