package net.safemoon.androidwallet.viewmodels;

import kotlin.coroutines.jvm.internal.SuspendLambda;
import kotlin.coroutines.jvm.internal.a;
import net.safemoon.androidwallet.viewmodels.TransferViewModel;

/* compiled from: TransferViewModel.kt */
@a(c = "net.safemoon.androidwallet.viewmodels.TransferViewModel$sendTransfer$1$1$1$1", f = "TransferViewModel.kt", l = {}, m = "invokeSuspend")
/* loaded from: classes2.dex */
public final class TransferViewModel$sendTransfer$1$1$1$1 extends SuspendLambda implements hd1<c90, q70<? super te4>, Object> {
    public final /* synthetic */ TransferViewModel.b $transferResponse;
    public int label;
    public final /* synthetic */ TransferViewModel this$0;

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public TransferViewModel$sendTransfer$1$1$1$1(TransferViewModel transferViewModel, TransferViewModel.b bVar, q70<? super TransferViewModel$sendTransfer$1$1$1$1> q70Var) {
        super(2, q70Var);
        this.this$0 = transferViewModel;
        this.$transferResponse = bVar;
    }

    @Override // kotlin.coroutines.jvm.internal.BaseContinuationImpl
    public final q70<te4> create(Object obj, q70<?> q70Var) {
        return new TransferViewModel$sendTransfer$1$1$1$1(this.this$0, this.$transferResponse, q70Var);
    }

    @Override // defpackage.hd1
    public final Object invoke(c90 c90Var, q70<? super te4> q70Var) {
        return ((TransferViewModel$sendTransfer$1$1$1$1) create(c90Var, q70Var)).invokeSuspend(te4.a);
    }

    @Override // kotlin.coroutines.jvm.internal.BaseContinuationImpl
    public final Object invokeSuspend(Object obj) {
        gs1.d();
        if (this.label == 0) {
            o83.b(obj);
            this.this$0.k().postValue(this.$transferResponse);
            return te4.a;
        }
        throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
    }
}
