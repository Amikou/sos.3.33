package net.safemoon.androidwallet.viewmodels;

import android.app.Application;
import kotlin.coroutines.jvm.internal.SuspendLambda;
import kotlin.coroutines.jvm.internal.a;
import net.safemoon.androidwallet.R;
import net.safemoon.androidwallet.model.common.LoadingState;
import net.safemoon.androidwallet.viewmodels.SwapViewModel;

/* compiled from: SwapViewModel.kt */
@a(c = "net.safemoon.androidwallet.viewmodels.SwapViewModel$swapTokens$1$1$3$1", f = "SwapViewModel.kt", l = {}, m = "invokeSuspend")
/* loaded from: classes2.dex */
public final class SwapViewModel$swapTokens$1$1$3$1 extends SuspendLambda implements hd1<c90, q70<? super te4>, Object> {
    public final /* synthetic */ Object $it;
    public int label;
    public final /* synthetic */ SwapViewModel this$0;

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public SwapViewModel$swapTokens$1$1$3$1(SwapViewModel swapViewModel, Object obj, q70<? super SwapViewModel$swapTokens$1$1$3$1> q70Var) {
        super(2, q70Var);
        this.this$0 = swapViewModel;
        this.$it = obj;
    }

    @Override // kotlin.coroutines.jvm.internal.BaseContinuationImpl
    public final q70<te4> create(Object obj, q70<?> q70Var) {
        return new SwapViewModel$swapTokens$1$1$3$1(this.this$0, this.$it, q70Var);
    }

    @Override // defpackage.hd1
    public final Object invoke(c90 c90Var, q70<? super te4> q70Var) {
        return ((SwapViewModel$swapTokens$1$1$3$1) create(c90Var, q70Var)).invokeSuspend(te4.a);
    }

    @Override // kotlin.coroutines.jvm.internal.BaseContinuationImpl
    public final Object invokeSuspend(Object obj) {
        gs1.d();
        if (this.label == 0) {
            o83.b(obj);
            this.this$0.l0().setValue(LoadingState.Normal);
            Object obj2 = this.$it;
            if (obj2 != null && (obj2 instanceof w84)) {
                SwapViewModel swapViewModel = this.this$0;
                String transactionHash = ((w84) obj2).getTransactionHash();
                fs1.e(transactionHash, "it.transactionHash");
                swapViewModel.Y0(transactionHash);
            } else if (obj2 != null && (obj2 instanceof hx0)) {
                if (((hx0) obj2).getError() != null) {
                    gb2<SwapViewModel.e> L0 = this.this$0.L0();
                    String message = ((hx0) this.$it).getError().getMessage();
                    fs1.e(message, "it.error.message");
                    L0.setValue(new SwapViewModel.e(false, message));
                } else {
                    SwapViewModel swapViewModel2 = this.this$0;
                    String transactionHash2 = ((hx0) this.$it).getTransactionHash();
                    fs1.e(transactionHash2, "it.transactionHash");
                    swapViewModel2.Y0(transactionHash2);
                }
            } else {
                Application a = this.this$0.a();
                fs1.e(a, "getApplication<Application>()");
                e30.a0(a, this.this$0.D0(R.string.swap_could_not));
            }
            return te4.a;
        }
        throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
    }
}
