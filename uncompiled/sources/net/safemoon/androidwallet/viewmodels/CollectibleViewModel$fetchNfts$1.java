package net.safemoon.androidwallet.viewmodels;

import kotlin.coroutines.jvm.internal.SuspendLambda;
import kotlin.coroutines.jvm.internal.a;
import net.safemoon.androidwallet.model.collectible.MarketPalace;
import net.safemoon.androidwallet.model.collectible.RoomCollection;

/* compiled from: CollectibleViewModel.kt */
@a(c = "net.safemoon.androidwallet.viewmodels.CollectibleViewModel$fetchNfts$1", f = "CollectibleViewModel.kt", l = {433}, m = "invokeSuspend")
/* loaded from: classes2.dex */
public final class CollectibleViewModel$fetchNfts$1 extends SuspendLambda implements hd1<c90, q70<? super te4>, Object> {
    public final /* synthetic */ RoomCollection $collection;
    public int label;
    public final /* synthetic */ CollectibleViewModel this$0;

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public CollectibleViewModel$fetchNfts$1(CollectibleViewModel collectibleViewModel, RoomCollection roomCollection, q70<? super CollectibleViewModel$fetchNfts$1> q70Var) {
        super(2, q70Var);
        this.this$0 = collectibleViewModel;
        this.$collection = roomCollection;
    }

    @Override // kotlin.coroutines.jvm.internal.BaseContinuationImpl
    public final q70<te4> create(Object obj, q70<?> q70Var) {
        return new CollectibleViewModel$fetchNfts$1(this.this$0, this.$collection, q70Var);
    }

    @Override // defpackage.hd1
    public final Object invoke(c90 c90Var, q70<? super te4> q70Var) {
        return ((CollectibleViewModel$fetchNfts$1) create(c90Var, q70Var)).invokeSuspend(te4.a);
    }

    @Override // kotlin.coroutines.jvm.internal.BaseContinuationImpl
    public final Object invokeSuspend(Object obj) {
        Object r;
        Object d = gs1.d();
        int i = this.label;
        if (i == 0) {
            o83.b(obj);
            this.this$0.N().postValue(hr.a(true));
            String marketPlace = this.$collection.getMarketPlace();
            if (fs1.b(marketPlace, MarketPalace.OPEN_SEA.name())) {
                if (this.$collection.getId() != null && this.$collection.getSlug() != null) {
                    CollectibleViewModel collectibleViewModel = this.this$0;
                    int chain = this.$collection.getChain();
                    Long id = this.$collection.getId();
                    fs1.d(id);
                    long longValue = id.longValue();
                    String slug = this.$collection.getSlug();
                    fs1.d(slug);
                    this.label = 1;
                    r = collectibleViewModel.r(chain, longValue, slug, 100, 0, this);
                    if (r == d) {
                        return d;
                    }
                }
            } else {
                fs1.b(marketPlace, MarketPalace.MORALIS.name());
            }
        } else if (i != 1) {
            throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        } else {
            o83.b(obj);
        }
        this.this$0.N().postValue(hr.a(false));
        return te4.a;
    }
}
