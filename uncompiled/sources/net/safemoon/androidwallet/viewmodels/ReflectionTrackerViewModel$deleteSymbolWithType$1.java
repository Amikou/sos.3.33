package net.safemoon.androidwallet.viewmodels;

import android.app.Application;
import kotlin.coroutines.jvm.internal.SuspendLambda;
import kotlin.coroutines.jvm.internal.a;
import kotlinx.coroutines.CoroutineDispatcher;
import net.safemoon.androidwallet.database.room.ApplicationRoomDatabase;
import net.safemoon.androidwallet.model.reflections.RoomReflectionsToken;
import net.safemoon.androidwallet.repository.ReflectionDataSource;

/* compiled from: ReflectionTrackerViewModel.kt */
@a(c = "net.safemoon.androidwallet.viewmodels.ReflectionTrackerViewModel$deleteSymbolWithType$1", f = "ReflectionTrackerViewModel.kt", l = {432}, m = "invokeSuspend")
/* loaded from: classes2.dex */
public final class ReflectionTrackerViewModel$deleteSymbolWithType$1 extends SuspendLambda implements hd1<c90, q70<? super te4>, Object> {
    public final /* synthetic */ RoomReflectionsToken $rrt;
    public int label;
    public final /* synthetic */ ReflectionTrackerViewModel this$0;

    /* compiled from: ReflectionTrackerViewModel.kt */
    @a(c = "net.safemoon.androidwallet.viewmodels.ReflectionTrackerViewModel$deleteSymbolWithType$1$1", f = "ReflectionTrackerViewModel.kt", l = {436}, m = "invokeSuspend")
    /* renamed from: net.safemoon.androidwallet.viewmodels.ReflectionTrackerViewModel$deleteSymbolWithType$1$1  reason: invalid class name */
    /* loaded from: classes2.dex */
    public static final class AnonymousClass1 extends SuspendLambda implements hd1<c90, q70<? super te4>, Object> {
        public final /* synthetic */ RoomReflectionsToken $rrt;
        public int label;
        public final /* synthetic */ ReflectionTrackerViewModel this$0;

        /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
        public AnonymousClass1(RoomReflectionsToken roomReflectionsToken, ReflectionTrackerViewModel reflectionTrackerViewModel, q70<? super AnonymousClass1> q70Var) {
            super(2, q70Var);
            this.$rrt = roomReflectionsToken;
            this.this$0 = reflectionTrackerViewModel;
        }

        @Override // kotlin.coroutines.jvm.internal.BaseContinuationImpl
        public final q70<te4> create(Object obj, q70<?> q70Var) {
            return new AnonymousClass1(this.$rrt, this.this$0, q70Var);
        }

        @Override // defpackage.hd1
        public final Object invoke(c90 c90Var, q70<? super te4> q70Var) {
            return ((AnonymousClass1) create(c90Var, q70Var)).invokeSuspend(te4.a);
        }

        @Override // kotlin.coroutines.jvm.internal.BaseContinuationImpl
        public final Object invokeSuspend(Object obj) {
            Object d = gs1.d();
            int i = this.label;
            if (i == 0) {
                o83.b(obj);
                if (ApplicationRoomDatabase.n.e().getFirst().contains(this.$rrt.getSymbolWithType())) {
                    sn4 sn4Var = sn4.a;
                    Application a = this.this$0.a();
                    fs1.e(a, "getApplication()");
                    sn4Var.e(a, "DEFAULT_REFLECTION_DELETE_MAP", this.$rrt.getSymbolWithType());
                }
                ReflectionDataSource r = this.this$0.r();
                RoomReflectionsToken roomReflectionsToken = this.$rrt;
                this.label = 1;
                if (r.a(roomReflectionsToken, this) == d) {
                    return d;
                }
            } else if (i != 1) {
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            } else {
                o83.b(obj);
            }
            return te4.a;
        }
    }

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public ReflectionTrackerViewModel$deleteSymbolWithType$1(RoomReflectionsToken roomReflectionsToken, ReflectionTrackerViewModel reflectionTrackerViewModel, q70<? super ReflectionTrackerViewModel$deleteSymbolWithType$1> q70Var) {
        super(2, q70Var);
        this.$rrt = roomReflectionsToken;
        this.this$0 = reflectionTrackerViewModel;
    }

    @Override // kotlin.coroutines.jvm.internal.BaseContinuationImpl
    public final q70<te4> create(Object obj, q70<?> q70Var) {
        return new ReflectionTrackerViewModel$deleteSymbolWithType$1(this.$rrt, this.this$0, q70Var);
    }

    @Override // defpackage.hd1
    public final Object invoke(c90 c90Var, q70<? super te4> q70Var) {
        return ((ReflectionTrackerViewModel$deleteSymbolWithType$1) create(c90Var, q70Var)).invokeSuspend(te4.a);
    }

    @Override // kotlin.coroutines.jvm.internal.BaseContinuationImpl
    public final Object invokeSuspend(Object obj) {
        Object d = gs1.d();
        int i = this.label;
        if (i == 0) {
            o83.b(obj);
            CoroutineDispatcher b = tp0.b();
            AnonymousClass1 anonymousClass1 = new AnonymousClass1(this.$rrt, this.this$0, null);
            this.label = 1;
            if (kotlinx.coroutines.a.e(b, anonymousClass1, this) == d) {
                return d;
            }
        } else if (i != 1) {
            throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        } else {
            o83.b(obj);
        }
        return te4.a;
    }
}
