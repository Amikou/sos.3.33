package net.safemoon.androidwallet.viewmodels;

import kotlin.jvm.internal.Lambda;
import net.safemoon.androidwallet.common.ActiveTokenListMode;
import net.safemoon.androidwallet.model.Coin;
import net.safemoon.androidwallet.repository.AllTokensDataSource;

/* compiled from: HomeViewModel.kt */
/* loaded from: classes2.dex */
public final class HomeViewModel$allCoinListFlow$1 extends Lambda implements rc1<gp2<Integer, Coin>> {
    public final /* synthetic */ HomeViewModel this$0;

    /* compiled from: HomeViewModel.kt */
    /* loaded from: classes2.dex */
    public static final class a implements cm1 {
        public final /* synthetic */ HomeViewModel a;

        public a(HomeViewModel homeViewModel) {
            this.a = homeViewModel;
        }

        @Override // defpackage.cm1
        public String get() {
            String str;
            str = this.a.c;
            return str;
        }
    }

    /* compiled from: HomeViewModel.kt */
    /* loaded from: classes2.dex */
    public static final class b implements cm1 {
        public final /* synthetic */ HomeViewModel a;

        public b(HomeViewModel homeViewModel) {
            this.a = homeViewModel;
        }

        @Override // defpackage.cm1
        public String get() {
            String str;
            str = this.a.d;
            return str;
        }
    }

    /* compiled from: HomeViewModel.kt */
    /* loaded from: classes2.dex */
    public static final class c implements cm1 {
        public final /* synthetic */ HomeViewModel a;

        public c(HomeViewModel homeViewModel) {
            this.a = homeViewModel;
        }

        @Override // defpackage.cm1
        public String get() {
            String value;
            ActiveTokenListMode value2 = this.a.e().getValue();
            return (value2 == null || (value = value2.getValue()) == null) ? "" : value;
        }
    }

    /* compiled from: HomeViewModel.kt */
    /* loaded from: classes2.dex */
    public static final class d implements cm1 {
        public final /* synthetic */ HomeViewModel a;

        public d(HomeViewModel homeViewModel) {
            this.a = homeViewModel;
        }

        @Override // defpackage.cm1
        public String get() {
            String value = this.a.d().getValue();
            return value == null ? "" : value;
        }
    }

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public HomeViewModel$allCoinListFlow$1(HomeViewModel homeViewModel) {
        super(0);
        this.this$0 = homeViewModel;
    }

    /* JADX WARN: Can't rename method to resolve collision */
    @Override // defpackage.rc1
    public final gp2<Integer, Coin> invoke() {
        e42 k = a4.k();
        fs1.e(k, "getMarketClient()");
        return new AllTokensDataSource(k, new a(this.this$0), new b(this.this$0), new c(this.this$0), new d(this.this$0));
    }
}
