package net.safemoon.androidwallet.viewmodels;

import kotlin.coroutines.jvm.internal.SuspendLambda;
import kotlin.coroutines.jvm.internal.a;
import net.safemoon.androidwallet.R;
import net.safemoon.androidwallet.common.TokenType;
import net.safemoon.androidwallet.model.common.LoadingState;
import net.safemoon.androidwallet.model.swap.Swap;
import net.safemoon.androidwallet.viewmodels.SwapViewModel;

/* compiled from: SwapViewModel.kt */
@a(c = "net.safemoon.androidwallet.viewmodels.SwapViewModel$checkApproveStatus$1$1$3$1", f = "SwapViewModel.kt", l = {}, m = "invokeSuspend")
/* loaded from: classes2.dex */
public final class SwapViewModel$checkApproveStatus$1$1$3$1 extends SuspendLambda implements hd1<c90, q70<? super te4>, Object> {
    public final /* synthetic */ SwapViewModel.ApproveStatus $it;
    public int label;
    public final /* synthetic */ SwapViewModel this$0;

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public SwapViewModel$checkApproveStatus$1$1$3$1(SwapViewModel swapViewModel, SwapViewModel.ApproveStatus approveStatus, q70<? super SwapViewModel$checkApproveStatus$1$1$3$1> q70Var) {
        super(2, q70Var);
        this.this$0 = swapViewModel;
        this.$it = approveStatus;
    }

    @Override // kotlin.coroutines.jvm.internal.BaseContinuationImpl
    public final q70<te4> create(Object obj, q70<?> q70Var) {
        return new SwapViewModel$checkApproveStatus$1$1$3$1(this.this$0, this.$it, q70Var);
    }

    @Override // defpackage.hd1
    public final Object invoke(c90 c90Var, q70<? super te4> q70Var) {
        return ((SwapViewModel$checkApproveStatus$1$1$3$1) create(c90Var, q70Var)).invokeSuspend(te4.a);
    }

    @Override // kotlin.coroutines.jvm.internal.BaseContinuationImpl
    public final Object invokeSuspend(Object obj) {
        String L;
        String l;
        String L2;
        gs1.d();
        if (this.label == 0) {
            o83.b(obj);
            this.this$0.O().postValue(LoadingState.Normal);
            SwapViewModel.ApproveStatus approveStatus = this.$it;
            if (approveStatus == SwapViewModel.ApproveStatus.ALLOWED) {
                this.this$0.Q().postValue(hr.d(0));
                this.this$0.e0();
            } else if (approveStatus == SwapViewModel.ApproveStatus.FAILED) {
                this.this$0.Q().postValue(hr.d(1));
                this.this$0.J0().postValue(this.this$0.D0(R.string.swap_approval_declined));
            } else {
                this.this$0.Q().postValue(hr.d(1));
                Swap value = this.this$0.A0().getValue();
                if (value != null) {
                    SwapViewModel swapViewModel = this.this$0;
                    TokenType.a aVar = TokenType.Companion;
                    Integer num = value.chainId;
                    fs1.e(num, "it.chainId");
                    if (aVar.b(num.intValue()) == TokenType.BEP_20) {
                        L2 = swapViewModel.L();
                        l = fs1.l("https://bscscan.com/address/", L2);
                    } else {
                        L = swapViewModel.L();
                        l = fs1.l("https://etherscan.io/address/", L);
                    }
                    gb2<String> J0 = swapViewModel.J0();
                    J0.postValue(swapViewModel.D0(R.string.swap_approval_pending_1) + l + swapViewModel.D0(R.string.swap_approval_pending_2));
                }
            }
            return te4.a;
        }
        throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
    }
}
