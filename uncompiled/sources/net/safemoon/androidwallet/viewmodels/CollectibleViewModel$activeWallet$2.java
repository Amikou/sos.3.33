package net.safemoon.androidwallet.viewmodels;

import android.app.Application;
import kotlin.jvm.internal.Lambda;
import net.safemoon.androidwallet.model.wallets.Wallet;

/* compiled from: CollectibleViewModel.kt */
/* loaded from: classes2.dex */
public final class CollectibleViewModel$activeWallet$2 extends Lambda implements rc1<Wallet> {
    public final /* synthetic */ Application $application;

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public CollectibleViewModel$activeWallet$2(Application application) {
        super(0);
        this.$application = application;
    }

    @Override // defpackage.rc1
    public final Wallet invoke() {
        return e30.c(this.$application);
    }
}
