package net.safemoon.androidwallet.viewmodels;

import kotlin.coroutines.jvm.internal.ContinuationImpl;
import kotlin.coroutines.jvm.internal.a;

/* compiled from: SettingNotificationViewModel.kt */
@a(c = "net.safemoon.androidwallet.viewmodels.SettingNotificationViewModel", f = "SettingNotificationViewModel.kt", l = {171, 171}, m = "getAllPriceAlert")
/* loaded from: classes2.dex */
public final class SettingNotificationViewModel$getAllPriceAlert$1 extends ContinuationImpl {
    public Object L$0;
    public Object L$1;
    public int label;
    public /* synthetic */ Object result;
    public final /* synthetic */ SettingNotificationViewModel this$0;

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public SettingNotificationViewModel$getAllPriceAlert$1(SettingNotificationViewModel settingNotificationViewModel, q70<? super SettingNotificationViewModel$getAllPriceAlert$1> q70Var) {
        super(q70Var);
        this.this$0 = settingNotificationViewModel;
    }

    @Override // kotlin.coroutines.jvm.internal.BaseContinuationImpl
    public final Object invokeSuspend(Object obj) {
        Object s;
        this.result = obj;
        this.label |= Integer.MIN_VALUE;
        s = this.this$0.s(this);
        return s;
    }
}
