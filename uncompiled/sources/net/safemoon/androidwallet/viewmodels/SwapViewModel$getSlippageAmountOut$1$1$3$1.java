package net.safemoon.androidwallet.viewmodels;

import com.github.mikephil.charting.utils.Utils;
import java.math.BigInteger;
import kotlin.coroutines.jvm.internal.SuspendLambda;
import kotlin.coroutines.jvm.internal.a;
import net.safemoon.androidwallet.model.swap.Swap;

/* compiled from: SwapViewModel.kt */
@a(c = "net.safemoon.androidwallet.viewmodels.SwapViewModel$getSlippageAmountOut$1$1$3$1", f = "SwapViewModel.kt", l = {}, m = "invokeSuspend")
/* loaded from: classes2.dex */
public final class SwapViewModel$getSlippageAmountOut$1$1$3$1 extends SuspendLambda implements hd1<c90, q70<? super te4>, Object> {
    public final /* synthetic */ l84 $it;
    public int label;
    public final /* synthetic */ SwapViewModel this$0;

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public SwapViewModel$getSlippageAmountOut$1$1$3$1(SwapViewModel swapViewModel, l84 l84Var, q70<? super SwapViewModel$getSlippageAmountOut$1$1$3$1> q70Var) {
        super(2, q70Var);
        this.this$0 = swapViewModel;
        this.$it = l84Var;
    }

    @Override // kotlin.coroutines.jvm.internal.BaseContinuationImpl
    public final q70<te4> create(Object obj, q70<?> q70Var) {
        return new SwapViewModel$getSlippageAmountOut$1$1$3$1(this.this$0, this.$it, q70Var);
    }

    @Override // defpackage.hd1
    public final Object invoke(c90 c90Var, q70<? super te4> q70Var) {
        return ((SwapViewModel$getSlippageAmountOut$1$1$3$1) create(c90Var, q70Var)).invokeSuspend(te4.a);
    }

    @Override // kotlin.coroutines.jvm.internal.BaseContinuationImpl
    public final Object invokeSuspend(Object obj) {
        gs1.d();
        if (this.label == 0) {
            o83.b(obj);
            Swap value = this.this$0.b0().getValue();
            if (this.$it != null && value != null) {
                this.this$0.e0();
                gb2<Double> M = this.this$0.M();
                BigInteger b = this.$it.b();
                Integer num = value.decimals;
                fs1.e(num, "swap.decimals");
                M.postValue(hr.b(e30.r(b, num.intValue()).doubleValue()));
            } else {
                this.this$0.M().postValue(hr.b(Utils.DOUBLE_EPSILON));
            }
            return te4.a;
        }
        throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
    }
}
