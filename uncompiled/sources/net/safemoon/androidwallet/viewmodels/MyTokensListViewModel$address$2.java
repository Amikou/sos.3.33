package net.safemoon.androidwallet.viewmodels;

import kotlin.jvm.internal.Lambda;

/* compiled from: MyTokensListViewModel.kt */
/* loaded from: classes2.dex */
public final class MyTokensListViewModel$address$2 extends Lambda implements rc1<String> {
    public final /* synthetic */ MyTokensListViewModel this$0;

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public MyTokensListViewModel$address$2(MyTokensListViewModel myTokensListViewModel) {
        super(0);
        this.this$0 = myTokensListViewModel;
    }

    @Override // defpackage.rc1
    public final String invoke() {
        return bo3.i(this.this$0.a(), "SAFEMOON_ADDRESS");
    }
}
