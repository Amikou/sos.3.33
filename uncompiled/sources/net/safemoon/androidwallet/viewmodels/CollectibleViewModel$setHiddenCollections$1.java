package net.safemoon.androidwallet.viewmodels;

import kotlin.coroutines.jvm.internal.ContinuationImpl;
import kotlin.coroutines.jvm.internal.a;

/* compiled from: CollectibleViewModel.kt */
@a(c = "net.safemoon.androidwallet.viewmodels.CollectibleViewModel", f = "CollectibleViewModel.kt", l = {529, 530, 556}, m = "setHiddenCollections")
/* loaded from: classes2.dex */
public final class CollectibleViewModel$setHiddenCollections$1 extends ContinuationImpl {
    public int I$0;
    public Object L$0;
    public Object L$1;
    public Object L$2;
    public int label;
    public /* synthetic */ Object result;
    public final /* synthetic */ CollectibleViewModel this$0;

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public CollectibleViewModel$setHiddenCollections$1(CollectibleViewModel collectibleViewModel, q70<? super CollectibleViewModel$setHiddenCollections$1> q70Var) {
        super(q70Var);
        this.this$0 = collectibleViewModel;
    }

    @Override // kotlin.coroutines.jvm.internal.BaseContinuationImpl
    public final Object invokeSuspend(Object obj) {
        this.result = obj;
        this.label |= Integer.MIN_VALUE;
        return this.this$0.a0(0, this);
    }
}
