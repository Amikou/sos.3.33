package net.safemoon.androidwallet.viewmodels;

import kotlin.coroutines.jvm.internal.SuspendLambda;
import kotlin.coroutines.jvm.internal.a;
import net.safemoon.androidwallet.model.priceAlert.PriceAlertToken;

/* compiled from: CryptoPriceAlertViewModel.kt */
@a(c = "net.safemoon.androidwallet.viewmodels.CryptoPriceAlertViewModel$createUpdatePriceAlert$1", f = "CryptoPriceAlertViewModel.kt", l = {110, 112}, m = "invokeSuspend")
/* loaded from: classes2.dex */
public final class CryptoPriceAlertViewModel$createUpdatePriceAlert$1 extends SuspendLambda implements hd1<c90, q70<? super te4>, Object> {
    public final /* synthetic */ int $position;
    public final /* synthetic */ PriceAlertToken $priceAlertData;
    public int label;
    public final /* synthetic */ CryptoPriceAlertViewModel this$0;

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public CryptoPriceAlertViewModel$createUpdatePriceAlert$1(CryptoPriceAlertViewModel cryptoPriceAlertViewModel, PriceAlertToken priceAlertToken, int i, q70<? super CryptoPriceAlertViewModel$createUpdatePriceAlert$1> q70Var) {
        super(2, q70Var);
        this.this$0 = cryptoPriceAlertViewModel;
        this.$priceAlertData = priceAlertToken;
        this.$position = i;
    }

    @Override // kotlin.coroutines.jvm.internal.BaseContinuationImpl
    public final q70<te4> create(Object obj, q70<?> q70Var) {
        return new CryptoPriceAlertViewModel$createUpdatePriceAlert$1(this.this$0, this.$priceAlertData, this.$position, q70Var);
    }

    @Override // defpackage.hd1
    public final Object invoke(c90 c90Var, q70<? super te4> q70Var) {
        return ((CryptoPriceAlertViewModel$createUpdatePriceAlert$1) create(c90Var, q70Var)).invokeSuspend(te4.a);
    }

    /* JADX WARN: Removed duplicated region for block: B:38:0x0095 A[Catch: Exception -> 0x00b4, TryCatch #0 {Exception -> 0x00b4, blocks: (B:6:0x000e, B:22:0x0065, B:23:0x0067, B:25:0x006d, B:30:0x007e, B:38:0x0095, B:44:0x00a9, B:39:0x0098, B:43:0x00a5, B:42:0x00a1, B:33:0x0088, B:36:0x008f, B:28:0x0078, B:10:0x001a, B:18:0x0048, B:13:0x0026, B:15:0x002e, B:19:0x004b), top: B:48:0x0008 }] */
    /* JADX WARN: Removed duplicated region for block: B:39:0x0098 A[Catch: Exception -> 0x00b4, TryCatch #0 {Exception -> 0x00b4, blocks: (B:6:0x000e, B:22:0x0065, B:23:0x0067, B:25:0x006d, B:30:0x007e, B:38:0x0095, B:44:0x00a9, B:39:0x0098, B:43:0x00a5, B:42:0x00a1, B:33:0x0088, B:36:0x008f, B:28:0x0078, B:10:0x001a, B:18:0x0048, B:13:0x0026, B:15:0x002e, B:19:0x004b), top: B:48:0x0008 }] */
    @Override // kotlin.coroutines.jvm.internal.BaseContinuationImpl
    /*
        Code decompiled incorrectly, please refer to instructions dump.
        To view partially-correct code enable 'Show inconsistent code' option in preferences
    */
    public final java.lang.Object invokeSuspend(java.lang.Object r5) {
        /*
            r4 = this;
            java.lang.Object r0 = defpackage.gs1.d()
            int r1 = r4.label
            r2 = 2
            r3 = 1
            if (r1 == 0) goto L1e
            if (r1 == r3) goto L1a
            if (r1 != r2) goto L12
            defpackage.o83.b(r5)     // Catch: java.lang.Exception -> Lb4
            goto L65
        L12:
            java.lang.IllegalStateException r5 = new java.lang.IllegalStateException
            java.lang.String r0 = "call to 'resume' before 'invoke' with coroutine"
            r5.<init>(r0)
            throw r5
        L1a:
            defpackage.o83.b(r5)     // Catch: java.lang.Exception -> Lb4
            goto L48
        L1e:
            defpackage.o83.b(r5)
            net.safemoon.androidwallet.viewmodels.CryptoPriceAlertViewModel r5 = r4.this$0
            r5.x()
            net.safemoon.androidwallet.model.priceAlert.PriceAlertToken r5 = r4.$priceAlertData     // Catch: java.lang.Exception -> Lb4
            java.lang.Integer r5 = r5.getId()     // Catch: java.lang.Exception -> Lb4
            if (r5 != 0) goto L4b
            net.safemoon.androidwallet.viewmodels.CryptoPriceAlertViewModel r5 = r4.this$0     // Catch: java.lang.Exception -> Lb4
            ac3 r5 = net.safemoon.androidwallet.viewmodels.CryptoPriceAlertViewModel.e(r5)     // Catch: java.lang.Exception -> Lb4
            net.safemoon.androidwallet.model.priceAlert.PriceAlertToken r1 = r4.$priceAlertData     // Catch: java.lang.Exception -> Lb4
            retrofit2.b r5 = r5.j(r1)     // Catch: java.lang.Exception -> Lb4
            java.lang.String r1 = "mainNetApiInterface.crea…riceAlert(priceAlertData)"
            defpackage.fs1.e(r5, r1)     // Catch: java.lang.Exception -> Lb4
            r4.label = r3     // Catch: java.lang.Exception -> Lb4
            java.lang.Object r5 = retrofit2.KotlinExtensions.c(r5, r4)     // Catch: java.lang.Exception -> Lb4
            if (r5 != r0) goto L48
            return r0
        L48:
            retrofit2.n r5 = (retrofit2.n) r5     // Catch: java.lang.Exception -> Lb4
            goto L67
        L4b:
            net.safemoon.androidwallet.viewmodels.CryptoPriceAlertViewModel r5 = r4.this$0     // Catch: java.lang.Exception -> Lb4
            ac3 r5 = net.safemoon.androidwallet.viewmodels.CryptoPriceAlertViewModel.e(r5)     // Catch: java.lang.Exception -> Lb4
            net.safemoon.androidwallet.model.priceAlert.PriceAlertToken r1 = r4.$priceAlertData     // Catch: java.lang.Exception -> Lb4
            retrofit2.b r5 = r5.h(r1)     // Catch: java.lang.Exception -> Lb4
            java.lang.String r1 = "mainNetApiInterface.upda…riceAlert(priceAlertData)"
            defpackage.fs1.e(r5, r1)     // Catch: java.lang.Exception -> Lb4
            r4.label = r2     // Catch: java.lang.Exception -> Lb4
            java.lang.Object r5 = retrofit2.KotlinExtensions.c(r5, r4)     // Catch: java.lang.Exception -> Lb4
            if (r5 != r0) goto L65
            return r0
        L65:
            retrofit2.n r5 = (retrofit2.n) r5     // Catch: java.lang.Exception -> Lb4
        L67:
            boolean r0 = r5.e()     // Catch: java.lang.Exception -> Lb4
            if (r0 == 0) goto Lb4
            java.lang.Object r0 = r5.a()     // Catch: java.lang.Exception -> Lb4
            net.safemoon.androidwallet.model.priceAlert.PriceAlertTokenData r0 = (net.safemoon.androidwallet.model.priceAlert.PriceAlertTokenData) r0     // Catch: java.lang.Exception -> Lb4
            r1 = 0
            if (r0 != 0) goto L78
            r0 = r1
            goto L7c
        L78:
            net.safemoon.androidwallet.model.priceAlert.PriceAlertToken r0 = r0.getResult()     // Catch: java.lang.Exception -> Lb4
        L7c:
            if (r0 == 0) goto Lb4
            java.lang.Object r0 = r5.a()     // Catch: java.lang.Exception -> Lb4
            net.safemoon.androidwallet.model.priceAlert.PriceAlertTokenData r0 = (net.safemoon.androidwallet.model.priceAlert.PriceAlertTokenData) r0     // Catch: java.lang.Exception -> Lb4
            if (r0 != 0) goto L88
        L86:
            r0 = r1
            goto L93
        L88:
            net.safemoon.androidwallet.model.priceAlert.PriceAlertToken r0 = r0.getResult()     // Catch: java.lang.Exception -> Lb4
            if (r0 != 0) goto L8f
            goto L86
        L8f:
            java.lang.Integer r0 = r0.getId()     // Catch: java.lang.Exception -> Lb4
        L93:
            if (r0 != 0) goto L98
            net.safemoon.androidwallet.model.priceAlert.PriceAlertToken r5 = r4.$priceAlertData     // Catch: java.lang.Exception -> Lb4
            goto La9
        L98:
            java.lang.Object r5 = r5.a()     // Catch: java.lang.Exception -> Lb4
            net.safemoon.androidwallet.model.priceAlert.PriceAlertTokenData r5 = (net.safemoon.androidwallet.model.priceAlert.PriceAlertTokenData) r5     // Catch: java.lang.Exception -> Lb4
            if (r5 != 0) goto La1
            goto La5
        La1:
            net.safemoon.androidwallet.model.priceAlert.PriceAlertToken r1 = r5.getResult()     // Catch: java.lang.Exception -> Lb4
        La5:
            defpackage.fs1.d(r1)     // Catch: java.lang.Exception -> Lb4
            r5 = r1
        La9:
            net.safemoon.androidwallet.viewmodels.CryptoPriceAlertViewModel r0 = r4.this$0     // Catch: java.lang.Exception -> Lb4
            int r1 = r4.$position     // Catch: java.lang.Exception -> Lb4
            java.lang.Integer r1 = defpackage.hr.d(r1)     // Catch: java.lang.Exception -> Lb4
            r0.z(r5, r1)     // Catch: java.lang.Exception -> Lb4
        Lb4:
            net.safemoon.androidwallet.viewmodels.CryptoPriceAlertViewModel r5 = r4.this$0
            r5.y()
            te4 r5 = defpackage.te4.a
            return r5
        */
        throw new UnsupportedOperationException("Method not decompiled: net.safemoon.androidwallet.viewmodels.CryptoPriceAlertViewModel$createUpdatePriceAlert$1.invokeSuspend(java.lang.Object):java.lang.Object");
    }
}
