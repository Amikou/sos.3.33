package net.safemoon.androidwallet.viewmodels;

import com.google.gson.JsonObject;
import kotlin.coroutines.jvm.internal.SuspendLambda;
import kotlin.coroutines.jvm.internal.a;
import kotlinx.coroutines.CoroutineDispatcher;
import net.safemoon.androidwallet.model.swap.Swap;
import retrofit2.KotlinExtensions;
import retrofit2.b;
import retrofit2.n;

/* compiled from: SwapMigrationViewModel.kt */
@a(c = "net.safemoon.androidwallet.viewmodels.SwapMigrationViewModel$getDestinationPriceFromCMC$1", f = "SwapMigrationViewModel.kt", l = {398}, m = "invokeSuspend")
/* loaded from: classes2.dex */
public final class SwapMigrationViewModel$getDestinationPriceFromCMC$1 extends SuspendLambda implements hd1<c90, q70<? super te4>, Object> {
    public final /* synthetic */ Swap $swap;
    public int label;
    public final /* synthetic */ SwapMigrationViewModel this$0;

    /* compiled from: SwapMigrationViewModel.kt */
    @kotlin.coroutines.jvm.internal.a(c = "net.safemoon.androidwallet.viewmodels.SwapMigrationViewModel$getDestinationPriceFromCMC$1$1", f = "SwapMigrationViewModel.kt", l = {1458}, m = "invokeSuspend")
    /* renamed from: net.safemoon.androidwallet.viewmodels.SwapMigrationViewModel$getDestinationPriceFromCMC$1$1  reason: invalid class name */
    /* loaded from: classes2.dex */
    public static final class AnonymousClass1 extends SuspendLambda implements hd1<c90, q70<? super te4>, Object> {
        public final /* synthetic */ Swap $swap;
        public int label;
        public final /* synthetic */ SwapMigrationViewModel this$0;

        /* compiled from: SwapMigrationViewModel.kt */
        @kotlin.coroutines.jvm.internal.a(c = "net.safemoon.androidwallet.viewmodels.SwapMigrationViewModel$getDestinationPriceFromCMC$1$1$1", f = "SwapMigrationViewModel.kt", l = {402, 400}, m = "invokeSuspend")
        /* renamed from: net.safemoon.androidwallet.viewmodels.SwapMigrationViewModel$getDestinationPriceFromCMC$1$1$1  reason: invalid class name and collision with other inner class name */
        /* loaded from: classes2.dex */
        public static final class C02301 extends SuspendLambda implements hd1<k71<? super JsonObject>, q70<? super te4>, Object> {
            public final /* synthetic */ Swap $swap;
            private /* synthetic */ Object L$0;
            public int label;

            /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
            public C02301(Swap swap, q70<? super C02301> q70Var) {
                super(2, q70Var);
                this.$swap = swap;
            }

            @Override // kotlin.coroutines.jvm.internal.BaseContinuationImpl
            public final q70<te4> create(Object obj, q70<?> q70Var) {
                C02301 c02301 = new C02301(this.$swap, q70Var);
                c02301.L$0 = obj;
                return c02301;
            }

            @Override // defpackage.hd1
            public final Object invoke(k71<? super JsonObject> k71Var, q70<? super te4> q70Var) {
                return ((C02301) create(k71Var, q70Var)).invokeSuspend(te4.a);
            }

            @Override // kotlin.coroutines.jvm.internal.BaseContinuationImpl
            public final Object invokeSuspend(Object obj) {
                k71 k71Var;
                Object d = gs1.d();
                int i = this.label;
                if (i == 0) {
                    o83.b(obj);
                    k71Var = (k71) this.L$0;
                    b<JsonObject> e = a4.k().e(this.$swap.cmcId);
                    this.L$0 = k71Var;
                    this.label = 1;
                    obj = KotlinExtensions.c(e, this);
                    if (obj == d) {
                        return d;
                    }
                } else if (i != 1) {
                    if (i != 2) {
                        throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
                    }
                    o83.b(obj);
                    return te4.a;
                } else {
                    k71Var = (k71) this.L$0;
                    o83.b(obj);
                }
                Object a = ((n) obj).a();
                this.L$0 = null;
                this.label = 2;
                if (k71Var.emit(a, this) == d) {
                    return d;
                }
                return te4.a;
            }
        }

        /* compiled from: SwapMigrationViewModel.kt */
        @kotlin.coroutines.jvm.internal.a(c = "net.safemoon.androidwallet.viewmodels.SwapMigrationViewModel$getDestinationPriceFromCMC$1$1$2", f = "SwapMigrationViewModel.kt", l = {}, m = "invokeSuspend")
        /* renamed from: net.safemoon.androidwallet.viewmodels.SwapMigrationViewModel$getDestinationPriceFromCMC$1$1$2  reason: invalid class name */
        /* loaded from: classes2.dex */
        public static final class AnonymousClass2 extends SuspendLambda implements kd1<k71<? super JsonObject>, Throwable, q70<? super te4>, Object> {
            public /* synthetic */ Object L$0;
            public int label;

            public AnonymousClass2(q70<? super AnonymousClass2> q70Var) {
                super(3, q70Var);
            }

            @Override // defpackage.kd1
            public final Object invoke(k71<? super JsonObject> k71Var, Throwable th, q70<? super te4> q70Var) {
                AnonymousClass2 anonymousClass2 = new AnonymousClass2(q70Var);
                anonymousClass2.L$0 = th;
                return anonymousClass2.invokeSuspend(te4.a);
            }

            @Override // kotlin.coroutines.jvm.internal.BaseContinuationImpl
            public final Object invokeSuspend(Object obj) {
                gs1.d();
                if (this.label == 0) {
                    o83.b(obj);
                    ((Throwable) this.L$0).printStackTrace();
                    return te4.a;
                }
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
        }

        /* compiled from: Collect.kt */
        /* renamed from: net.safemoon.androidwallet.viewmodels.SwapMigrationViewModel$getDestinationPriceFromCMC$1$1$a */
        /* loaded from: classes2.dex */
        public static final class a implements k71<JsonObject> {
            public final /* synthetic */ SwapMigrationViewModel a;
            public final /* synthetic */ Swap f0;

            public a(SwapMigrationViewModel swapMigrationViewModel, Swap swap) {
                this.a = swapMigrationViewModel;
                this.f0 = swap;
            }

            @Override // defpackage.k71
            public Object emit(JsonObject jsonObject, q70<? super te4> q70Var) {
                st1 b;
                b = as.b(ej4.a(this.a), null, null, new SwapMigrationViewModel$getDestinationPriceFromCMC$1$1$3$1(this.a, jsonObject, this.f0, null), 3, null);
                return b == gs1.d() ? b : te4.a;
            }
        }

        /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
        public AnonymousClass1(Swap swap, SwapMigrationViewModel swapMigrationViewModel, q70<? super AnonymousClass1> q70Var) {
            super(2, q70Var);
            this.$swap = swap;
            this.this$0 = swapMigrationViewModel;
        }

        @Override // kotlin.coroutines.jvm.internal.BaseContinuationImpl
        public final q70<te4> create(Object obj, q70<?> q70Var) {
            return new AnonymousClass1(this.$swap, this.this$0, q70Var);
        }

        @Override // defpackage.hd1
        public final Object invoke(c90 c90Var, q70<? super te4> q70Var) {
            return ((AnonymousClass1) create(c90Var, q70Var)).invokeSuspend(te4.a);
        }

        @Override // kotlin.coroutines.jvm.internal.BaseContinuationImpl
        public final Object invokeSuspend(Object obj) {
            Object d = gs1.d();
            int i = this.label;
            if (i == 0) {
                o83.b(obj);
                j71 c = n71.c(n71.n(new C02301(this.$swap, null)), new AnonymousClass2(null));
                a aVar = new a(this.this$0, this.$swap);
                this.label = 1;
                if (c.a(aVar, this) == d) {
                    return d;
                }
            } else if (i != 1) {
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            } else {
                o83.b(obj);
            }
            return te4.a;
        }
    }

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public SwapMigrationViewModel$getDestinationPriceFromCMC$1(Swap swap, SwapMigrationViewModel swapMigrationViewModel, q70<? super SwapMigrationViewModel$getDestinationPriceFromCMC$1> q70Var) {
        super(2, q70Var);
        this.$swap = swap;
        this.this$0 = swapMigrationViewModel;
    }

    @Override // kotlin.coroutines.jvm.internal.BaseContinuationImpl
    public final q70<te4> create(Object obj, q70<?> q70Var) {
        return new SwapMigrationViewModel$getDestinationPriceFromCMC$1(this.$swap, this.this$0, q70Var);
    }

    @Override // defpackage.hd1
    public final Object invoke(c90 c90Var, q70<? super te4> q70Var) {
        return ((SwapMigrationViewModel$getDestinationPriceFromCMC$1) create(c90Var, q70Var)).invokeSuspend(te4.a);
    }

    @Override // kotlin.coroutines.jvm.internal.BaseContinuationImpl
    public final Object invokeSuspend(Object obj) {
        Object d = gs1.d();
        int i = this.label;
        if (i == 0) {
            o83.b(obj);
            CoroutineDispatcher b = tp0.b();
            AnonymousClass1 anonymousClass1 = new AnonymousClass1(this.$swap, this.this$0, null);
            this.label = 1;
            if (kotlinx.coroutines.a.e(b, anonymousClass1, this) == d) {
                return d;
            }
        } else if (i != 1) {
            throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        } else {
            o83.b(obj);
        }
        return te4.a;
    }
}
