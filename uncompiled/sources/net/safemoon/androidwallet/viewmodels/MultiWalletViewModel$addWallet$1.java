package net.safemoon.androidwallet.viewmodels;

import kotlin.coroutines.jvm.internal.SuspendLambda;
import kotlin.coroutines.jvm.internal.a;
import kotlin.jvm.internal.Ref$ObjectRef;
import kotlinx.coroutines.CoroutineDispatcher;

/* compiled from: MultiWalletViewModel.kt */
@a(c = "net.safemoon.androidwallet.viewmodels.MultiWalletViewModel$addWallet$1", f = "MultiWalletViewModel.kt", l = {197}, m = "invokeSuspend")
/* loaded from: classes2.dex */
public final class MultiWalletViewModel$addWallet$1 extends SuspendLambda implements hd1<c90, q70<? super te4>, Object> {
    public final /* synthetic */ String $address;
    public final /* synthetic */ tc1<Long, te4> $callBack;
    public final /* synthetic */ Ref$ObjectRef<String> $encryptedMnemonic;
    public final /* synthetic */ String $encryptedPrivateKey;
    public final /* synthetic */ boolean $isPrimaryWallet;
    public final /* synthetic */ boolean $isSetActiveWallet;
    public final /* synthetic */ String $privateKey;
    public final /* synthetic */ String $u5B64;
    public final /* synthetic */ String $walletName;
    public int label;
    public final /* synthetic */ MultiWalletViewModel this$0;

    /* compiled from: MultiWalletViewModel.kt */
    @a(c = "net.safemoon.androidwallet.viewmodels.MultiWalletViewModel$addWallet$1$1", f = "MultiWalletViewModel.kt", l = {198, 207, 215, 236, 238, 241, 245, 260}, m = "invokeSuspend")
    /* renamed from: net.safemoon.androidwallet.viewmodels.MultiWalletViewModel$addWallet$1$1  reason: invalid class name */
    /* loaded from: classes2.dex */
    public static final class AnonymousClass1 extends SuspendLambda implements hd1<c90, q70<? super te4>, Object> {
        public final /* synthetic */ String $address;
        public final /* synthetic */ tc1<Long, te4> $callBack;
        public final /* synthetic */ Ref$ObjectRef<String> $encryptedMnemonic;
        public final /* synthetic */ String $encryptedPrivateKey;
        public final /* synthetic */ boolean $isPrimaryWallet;
        public final /* synthetic */ boolean $isSetActiveWallet;
        public final /* synthetic */ String $privateKey;
        public final /* synthetic */ String $u5B64;
        public final /* synthetic */ String $walletName;
        public Object L$0;
        public Object L$1;
        public int label;
        public final /* synthetic */ MultiWalletViewModel this$0;

        /* compiled from: MultiWalletViewModel.kt */
        @a(c = "net.safemoon.androidwallet.viewmodels.MultiWalletViewModel$addWallet$1$1$1", f = "MultiWalletViewModel.kt", l = {}, m = "invokeSuspend")
        /* renamed from: net.safemoon.androidwallet.viewmodels.MultiWalletViewModel$addWallet$1$1$1  reason: invalid class name and collision with other inner class name */
        /* loaded from: classes2.dex */
        public static final class C02261 extends SuspendLambda implements hd1<c90, q70<? super te4>, Object> {
            public final /* synthetic */ tc1<Long, te4> $callBack;
            public int label;

            /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
            /* JADX WARN: Multi-variable type inference failed */
            public C02261(tc1<? super Long, te4> tc1Var, q70<? super C02261> q70Var) {
                super(2, q70Var);
                this.$callBack = tc1Var;
            }

            @Override // kotlin.coroutines.jvm.internal.BaseContinuationImpl
            public final q70<te4> create(Object obj, q70<?> q70Var) {
                return new C02261(this.$callBack, q70Var);
            }

            @Override // defpackage.hd1
            public final Object invoke(c90 c90Var, q70<? super te4> q70Var) {
                return ((C02261) create(c90Var, q70Var)).invokeSuspend(te4.a);
            }

            @Override // kotlin.coroutines.jvm.internal.BaseContinuationImpl
            public final Object invokeSuspend(Object obj) {
                gs1.d();
                if (this.label == 0) {
                    o83.b(obj);
                    tc1<Long, te4> tc1Var = this.$callBack;
                    if (tc1Var != null) {
                        tc1Var.invoke(hr.e(0L));
                    }
                    return te4.a;
                }
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
        }

        /* compiled from: MultiWalletViewModel.kt */
        @a(c = "net.safemoon.androidwallet.viewmodels.MultiWalletViewModel$addWallet$1$1$2", f = "MultiWalletViewModel.kt", l = {}, m = "invokeSuspend")
        /* renamed from: net.safemoon.androidwallet.viewmodels.MultiWalletViewModel$addWallet$1$1$2  reason: invalid class name */
        /* loaded from: classes2.dex */
        public static final class AnonymousClass2 extends SuspendLambda implements hd1<c90, q70<? super te4>, Object> {
            public final /* synthetic */ tc1<Long, te4> $callBack;
            public final /* synthetic */ Long $savedId;
            public int label;

            /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
            /* JADX WARN: Multi-variable type inference failed */
            public AnonymousClass2(tc1<? super Long, te4> tc1Var, Long l, q70<? super AnonymousClass2> q70Var) {
                super(2, q70Var);
                this.$callBack = tc1Var;
                this.$savedId = l;
            }

            @Override // kotlin.coroutines.jvm.internal.BaseContinuationImpl
            public final q70<te4> create(Object obj, q70<?> q70Var) {
                return new AnonymousClass2(this.$callBack, this.$savedId, q70Var);
            }

            @Override // defpackage.hd1
            public final Object invoke(c90 c90Var, q70<? super te4> q70Var) {
                return ((AnonymousClass2) create(c90Var, q70Var)).invokeSuspend(te4.a);
            }

            @Override // kotlin.coroutines.jvm.internal.BaseContinuationImpl
            public final Object invokeSuspend(Object obj) {
                gs1.d();
                if (this.label == 0) {
                    o83.b(obj);
                    tc1<Long, te4> tc1Var = this.$callBack;
                    if (tc1Var != null) {
                        tc1Var.invoke(this.$savedId);
                    }
                    return te4.a;
                }
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
        }

        /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
        /* JADX WARN: Multi-variable type inference failed */
        public AnonymousClass1(MultiWalletViewModel multiWalletViewModel, String str, String str2, String str3, Ref$ObjectRef<String> ref$ObjectRef, String str4, boolean z, boolean z2, String str5, tc1<? super Long, te4> tc1Var, q70<? super AnonymousClass1> q70Var) {
            super(2, q70Var);
            this.this$0 = multiWalletViewModel;
            this.$address = str;
            this.$walletName = str2;
            this.$encryptedPrivateKey = str3;
            this.$encryptedMnemonic = ref$ObjectRef;
            this.$u5B64 = str4;
            this.$isPrimaryWallet = z;
            this.$isSetActiveWallet = z2;
            this.$privateKey = str5;
            this.$callBack = tc1Var;
        }

        @Override // kotlin.coroutines.jvm.internal.BaseContinuationImpl
        public final q70<te4> create(Object obj, q70<?> q70Var) {
            return new AnonymousClass1(this.this$0, this.$address, this.$walletName, this.$encryptedPrivateKey, this.$encryptedMnemonic, this.$u5B64, this.$isPrimaryWallet, this.$isSetActiveWallet, this.$privateKey, this.$callBack, q70Var);
        }

        @Override // defpackage.hd1
        public final Object invoke(c90 c90Var, q70<? super te4> q70Var) {
            return ((AnonymousClass1) create(c90Var, q70Var)).invokeSuspend(te4.a);
        }

        /* JADX WARN: Removed duplicated region for block: B:18:0x0078  */
        /* JADX WARN: Removed duplicated region for block: B:20:0x008f  */
        /* JADX WARN: Removed duplicated region for block: B:25:0x00af  */
        /* JADX WARN: Removed duplicated region for block: B:26:0x00b2  */
        /* JADX WARN: Removed duplicated region for block: B:30:0x00d3  */
        /* JADX WARN: Removed duplicated region for block: B:33:0x00ef  */
        /* JADX WARN: Removed duplicated region for block: B:34:0x00f1  */
        /* JADX WARN: Removed duplicated region for block: B:37:0x0112 A[RETURN] */
        /* JADX WARN: Removed duplicated region for block: B:38:0x0113  */
        /* JADX WARN: Removed duplicated region for block: B:46:0x0132  */
        /* JADX WARN: Removed duplicated region for block: B:49:0x013d  */
        /* JADX WARN: Removed duplicated region for block: B:54:0x016b A[RETURN] */
        /* JADX WARN: Removed duplicated region for block: B:55:0x016c  */
        /* JADX WARN: Removed duplicated region for block: B:60:0x01b0 A[RETURN] */
        /* JADX WARN: Removed duplicated region for block: B:64:0x01e3  */
        /* JADX WARN: Removed duplicated region for block: B:73:0x01fd  */
        /* JADX WARN: Removed duplicated region for block: B:76:0x020a A[RETURN] */
        @Override // kotlin.coroutines.jvm.internal.BaseContinuationImpl
        /*
            Code decompiled incorrectly, please refer to instructions dump.
            To view partially-correct code enable 'Show inconsistent code' option in preferences
        */
        public final java.lang.Object invokeSuspend(java.lang.Object r18) {
            /*
                Method dump skipped, instructions count: 568
                To view this dump change 'Code comments level' option to 'DEBUG'
            */
            throw new UnsupportedOperationException("Method not decompiled: net.safemoon.androidwallet.viewmodels.MultiWalletViewModel$addWallet$1.AnonymousClass1.invokeSuspend(java.lang.Object):java.lang.Object");
        }
    }

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    /* JADX WARN: Multi-variable type inference failed */
    public MultiWalletViewModel$addWallet$1(MultiWalletViewModel multiWalletViewModel, String str, String str2, String str3, Ref$ObjectRef<String> ref$ObjectRef, String str4, boolean z, boolean z2, String str5, tc1<? super Long, te4> tc1Var, q70<? super MultiWalletViewModel$addWallet$1> q70Var) {
        super(2, q70Var);
        this.this$0 = multiWalletViewModel;
        this.$address = str;
        this.$walletName = str2;
        this.$encryptedPrivateKey = str3;
        this.$encryptedMnemonic = ref$ObjectRef;
        this.$u5B64 = str4;
        this.$isPrimaryWallet = z;
        this.$isSetActiveWallet = z2;
        this.$privateKey = str5;
        this.$callBack = tc1Var;
    }

    @Override // kotlin.coroutines.jvm.internal.BaseContinuationImpl
    public final q70<te4> create(Object obj, q70<?> q70Var) {
        return new MultiWalletViewModel$addWallet$1(this.this$0, this.$address, this.$walletName, this.$encryptedPrivateKey, this.$encryptedMnemonic, this.$u5B64, this.$isPrimaryWallet, this.$isSetActiveWallet, this.$privateKey, this.$callBack, q70Var);
    }

    @Override // defpackage.hd1
    public final Object invoke(c90 c90Var, q70<? super te4> q70Var) {
        return ((MultiWalletViewModel$addWallet$1) create(c90Var, q70Var)).invokeSuspend(te4.a);
    }

    @Override // kotlin.coroutines.jvm.internal.BaseContinuationImpl
    public final Object invokeSuspend(Object obj) {
        Object d = gs1.d();
        int i = this.label;
        if (i == 0) {
            o83.b(obj);
            CoroutineDispatcher b = tp0.b();
            AnonymousClass1 anonymousClass1 = new AnonymousClass1(this.this$0, this.$address, this.$walletName, this.$encryptedPrivateKey, this.$encryptedMnemonic, this.$u5B64, this.$isPrimaryWallet, this.$isSetActiveWallet, this.$privateKey, this.$callBack, null);
            this.label = 1;
            if (kotlinx.coroutines.a.e(b, anonymousClass1, this) == d) {
                return d;
            }
        } else if (i != 1) {
            throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        } else {
            o83.b(obj);
        }
        return te4.a;
    }
}
