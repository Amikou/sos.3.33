package net.safemoon.androidwallet.viewmodels;

import kotlin.jvm.internal.Lambda;

/* compiled from: SwapMigrationViewModel.kt */
/* loaded from: classes2.dex */
public final class SwapMigrationViewModel$address$2 extends Lambda implements rc1<String> {
    public final /* synthetic */ SwapMigrationViewModel this$0;

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public SwapMigrationViewModel$address$2(SwapMigrationViewModel swapMigrationViewModel) {
        super(0);
        this.this$0 = swapMigrationViewModel;
    }

    @Override // defpackage.rc1
    public final String invoke() {
        return bo3.i(this.this$0.a(), "SAFEMOON_ADDRESS");
    }
}
