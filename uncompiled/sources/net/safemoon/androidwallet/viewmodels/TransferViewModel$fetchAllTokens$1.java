package net.safemoon.androidwallet.viewmodels;

import android.app.Application;
import java.util.List;
import kotlin.coroutines.jvm.internal.SuspendLambda;
import kotlin.coroutines.jvm.internal.a;
import kotlinx.coroutines.CoroutineDispatcher;
import net.safemoon.androidwallet.common.TokenType;
import net.safemoon.androidwallet.model.token.abstraction.IToken;

/* compiled from: TransferViewModel.kt */
@a(c = "net.safemoon.androidwallet.viewmodels.TransferViewModel$fetchAllTokens$1", f = "TransferViewModel.kt", l = {35}, m = "invokeSuspend")
/* loaded from: classes2.dex */
public final class TransferViewModel$fetchAllTokens$1 extends SuspendLambda implements hd1<c90, q70<? super te4>, Object> {
    public final /* synthetic */ TokenType $tokenType;
    public int label;
    public final /* synthetic */ TransferViewModel this$0;

    /* compiled from: TransferViewModel.kt */
    @a(c = "net.safemoon.androidwallet.viewmodels.TransferViewModel$fetchAllTokens$1$1", f = "TransferViewModel.kt", l = {}, m = "invokeSuspend")
    /* renamed from: net.safemoon.androidwallet.viewmodels.TransferViewModel$fetchAllTokens$1$1  reason: invalid class name */
    /* loaded from: classes2.dex */
    public static final class AnonymousClass1 extends SuspendLambda implements hd1<c90, q70<? super te4>, Object> {
        public final /* synthetic */ TokenType $tokenType;
        public int label;
        public final /* synthetic */ TransferViewModel this$0;

        /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
        public AnonymousClass1(TransferViewModel transferViewModel, TokenType tokenType, q70<? super AnonymousClass1> q70Var) {
            super(2, q70Var);
            this.this$0 = transferViewModel;
            this.$tokenType = tokenType;
        }

        @Override // kotlin.coroutines.jvm.internal.BaseContinuationImpl
        public final q70<te4> create(Object obj, q70<?> q70Var) {
            return new AnonymousClass1(this.this$0, this.$tokenType, q70Var);
        }

        @Override // defpackage.hd1
        public final Object invoke(c90 c90Var, q70<? super te4> q70Var) {
            return ((AnonymousClass1) create(c90Var, q70Var)).invokeSuspend(te4.a);
        }

        @Override // kotlin.coroutines.jvm.internal.BaseContinuationImpl
        public final Object invokeSuspend(Object obj) {
            gs1.d();
            if (this.label == 0) {
                o83.b(obj);
                gb2<List<IToken>> d = this.this$0.d();
                za zaVar = za.a;
                Application a = this.this$0.a();
                fs1.e(a, "getApplication()");
                d.postValue(zaVar.b(a).a(this.$tokenType));
                return te4.a;
            }
            throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        }
    }

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public TransferViewModel$fetchAllTokens$1(TransferViewModel transferViewModel, TokenType tokenType, q70<? super TransferViewModel$fetchAllTokens$1> q70Var) {
        super(2, q70Var);
        this.this$0 = transferViewModel;
        this.$tokenType = tokenType;
    }

    @Override // kotlin.coroutines.jvm.internal.BaseContinuationImpl
    public final q70<te4> create(Object obj, q70<?> q70Var) {
        return new TransferViewModel$fetchAllTokens$1(this.this$0, this.$tokenType, q70Var);
    }

    @Override // defpackage.hd1
    public final Object invoke(c90 c90Var, q70<? super te4> q70Var) {
        return ((TransferViewModel$fetchAllTokens$1) create(c90Var, q70Var)).invokeSuspend(te4.a);
    }

    @Override // kotlin.coroutines.jvm.internal.BaseContinuationImpl
    public final Object invokeSuspend(Object obj) {
        Object d = gs1.d();
        int i = this.label;
        if (i == 0) {
            o83.b(obj);
            CoroutineDispatcher b = tp0.b();
            AnonymousClass1 anonymousClass1 = new AnonymousClass1(this.this$0, this.$tokenType, null);
            this.label = 1;
            if (kotlinx.coroutines.a.e(b, anonymousClass1, this) == d) {
                return d;
            }
        } else if (i != 1) {
            throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        } else {
            o83.b(obj);
        }
        return te4.a;
    }
}
