package net.safemoon.androidwallet.viewmodels;

import android.app.Application;
import android.graphics.Bitmap;
import com.google.zxing.BarcodeFormat;
import com.google.zxing.MultiFormatWriter;
import com.google.zxing.common.BitMatrix;

/* compiled from: GoogleAuthViewModel.kt */
/* loaded from: classes2.dex */
public final class GoogleAuthViewModel extends gd {
    public final gb2<String> b;
    public final gb2<Bitmap> c;
    public final gb2<Boolean> d;

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public GoogleAuthViewModel(Application application) {
        super(application);
        fs1.f(application, "application");
        this.b = new gb2<>("");
        this.c = new gb2<>();
        gb2<Boolean> gb2Var = new gb2<>(Boolean.FALSE);
        this.d = gb2Var;
        gb2Var.setValue(Boolean.valueOf(bo3.d(application, "AUTH_2FA_IS_ENABLE")));
    }

    public final void c() {
        as.b(ej4.a(this), null, null, new GoogleAuthViewModel$generateAuthKey$1(this, null), 3, null);
    }

    public final Bitmap d(e34 e34Var) {
        BitMatrix encode = new MultiFormatWriter().encode(e34Var.c(), BarcodeFormat.QR_CODE, 300, 300);
        int width = encode.getWidth();
        int height = encode.getHeight();
        int[] iArr = new int[width * height];
        if (height > 0) {
            int i = 0;
            while (true) {
                int i2 = i + 1;
                int i3 = i * width;
                if (width > 0) {
                    int i4 = 0;
                    while (true) {
                        int i5 = i4 + 1;
                        iArr[i3 + i4] = encode.get(i4, i) ? -16777216 : -1;
                        if (i5 >= width) {
                            break;
                        }
                        i4 = i5;
                    }
                }
                if (i2 >= height) {
                    break;
                }
                i = i2;
            }
        }
        Bitmap createBitmap = Bitmap.createBitmap(width, height, Bitmap.Config.ARGB_8888);
        createBitmap.setPixels(iArr, 0, width, 0, 0, width, height);
        fs1.e(createBitmap, "bitmap");
        return createBitmap;
    }

    public final gb2<Boolean> e() {
        return this.d;
    }

    public final gb2<Bitmap> f() {
        return this.c;
    }

    public final gb2<String> g() {
        return this.b;
    }

    public final String h(String str) {
        return d34.b(str);
    }
}
