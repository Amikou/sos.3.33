package net.safemoon.androidwallet.viewmodels.factory;

import android.app.Activity;
import android.app.Application;
import android.content.Context;
import androidx.lifecycle.l;
import java.lang.ref.WeakReference;
import net.safemoon.androidwallet.viewmodels.ContactViewModel;
import net.safemoon.androidwallet.viewmodels.CustomContractTokenViewModel;
import net.safemoon.androidwallet.viewmodels.MyTokensListViewModel;
import net.safemoon.androidwallet.viewmodels.SelectFiatViewModel;

/* compiled from: MyViewModelFactory.kt */
/* loaded from: classes2.dex */
public final class MyViewModelFactory implements l.b {
    public final WeakReference<Activity> a;

    public MyViewModelFactory(WeakReference<Activity> weakReference) {
        fs1.f(weakReference, "activity");
        this.a = weakReference;
    }

    @Override // androidx.lifecycle.l.b
    public <T extends dj4> T a(Class<T> cls) {
        T contactViewModel;
        fs1.f(cls, "modelClass");
        Activity activity = this.a.get();
        fs1.d(activity);
        Context applicationContext = activity.getApplicationContext();
        fs1.e(applicationContext, "activity.get()!!.applicationContext");
        if (fs1.b(cls, MyTokensListViewModel.class)) {
            Activity activity2 = this.a.get();
            fs1.d(activity2);
            Application application = activity2.getApplication();
            bn1 b = gg4.a.b(applicationContext);
            dg4 dg4Var = new dg4(applicationContext);
            fs1.e(application, "application");
            return new MyTokensListViewModel(application, dg4Var, b);
        }
        if (fs1.b(cls, oj3.class)) {
            Activity activity3 = this.a.get();
            fs1.d(activity3);
            Application application2 = activity3.getApplication();
            fs1.e(application2, "activity.get()!!.application");
            contactViewModel = new oj3(application2, za.a.b(applicationContext));
        } else if (fs1.b(cls, SelectFiatViewModel.class)) {
            Activity activity4 = this.a.get();
            fs1.d(activity4);
            Application application3 = activity4.getApplication();
            fs1.e(application3, "activity.get()!!.application");
            contactViewModel = new SelectFiatViewModel(application3, e31.a.b(applicationContext));
        } else if (fs1.b(cls, ContactViewModel.class)) {
            Activity activity5 = this.a.get();
            fs1.d(activity5);
            Application application4 = activity5.getApplication();
            fs1.e(application4, "activity.get()!!.application");
            contactViewModel = new ContactViewModel(application4, a70.a.a(applicationContext), new if1());
        } else if (fs1.b(cls, CustomContractTokenViewModel.class)) {
            Activity activity6 = this.a.get();
            fs1.d(activity6);
            Application application5 = activity6.getApplication();
            fs1.e(application5, "application");
            return new CustomContractTokenViewModel(application5, new MyViewModelFactory$create$1(this), new MyViewModelFactory$create$2(this));
        } else {
            return null;
        }
        return contactViewModel;
    }
}
