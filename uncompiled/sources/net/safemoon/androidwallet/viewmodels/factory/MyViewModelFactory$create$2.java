package net.safemoon.androidwallet.viewmodels.factory;

import java.lang.ref.WeakReference;
import kotlin.jvm.internal.Lambda;
import net.safemoon.androidwallet.R;

/* compiled from: MyViewModelFactory.kt */
/* loaded from: classes2.dex */
public final class MyViewModelFactory$create$2 extends Lambda implements rc1<te4> {
    public final /* synthetic */ MyViewModelFactory this$0;

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public MyViewModelFactory$create$2(MyViewModelFactory myViewModelFactory) {
        super(0);
        this.this$0 = myViewModelFactory;
    }

    @Override // defpackage.rc1
    public /* bridge */ /* synthetic */ te4 invoke() {
        invoke2();
        return te4.a;
    }

    @Override // defpackage.rc1
    /* renamed from: invoke  reason: avoid collision after fix types in other method */
    public final void invoke2() {
        WeakReference weakReference;
        weakReference = this.this$0.a;
        bh.u0(weakReference, R.string.cs_contract_wrong_bsc, R.string.action_close);
    }
}
