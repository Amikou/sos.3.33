package net.safemoon.androidwallet.viewmodels;

import android.app.Application;
import androidx.lifecycle.FlowLiveDataConversions;
import androidx.lifecycle.LiveData;
import com.github.mikephil.charting.utils.Utils;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import net.safemoon.androidwallet.R;
import net.safemoon.androidwallet.common.TokenType;
import net.safemoon.androidwallet.model.reflections.RoomReflectionsData;
import net.safemoon.androidwallet.model.reflections.RoomReflectionsDataAndToken;
import net.safemoon.androidwallet.model.reflections.RoomReflectionsToken;
import net.safemoon.androidwallet.model.reflections.RoomReflectionsTokenAndData;
import net.safemoon.androidwallet.repository.ReflectionDataSource;
import net.safemoon.androidwallet.repository.dataSource.token.user.LocalUserTokenDataSource;
import net.safemoon.androidwallet.utils.ReflectionCustomContract;
import net.safemoon.androidwallet.viewmodels.ReflectionTrackerViewModel;

/* compiled from: ReflectionTrackerViewModel.kt */
/* loaded from: classes2.dex */
public final class ReflectionTrackerViewModel extends gd {
    public final gb2<TokenType> b;
    public final gb2<Date> c;
    public final sy1 d;
    public final sy1 e;
    public final sy1 f;
    public final gb2<a> g;
    public final LiveData<List<RoomReflectionsDataAndToken>> h;
    public final sy1 i;
    public Map<String, ? extends TokenType> j;

    /* compiled from: ReflectionTrackerViewModel.kt */
    /* loaded from: classes2.dex */
    public enum TimeMode {
        DAILY,
        WEEKLY,
        MONTHLY,
        YEARLY
    }

    /* compiled from: ReflectionTrackerViewModel.kt */
    /* loaded from: classes2.dex */
    public enum TimeSpan {
        LOW,
        MEDIUM,
        HIGH
    }

    /* compiled from: ReflectionTrackerViewModel.kt */
    /* loaded from: classes2.dex */
    public /* synthetic */ class b {
        public static final /* synthetic */ int[] a;

        static {
            int[] iArr = new int[TimeMode.values().length];
            iArr[TimeMode.WEEKLY.ordinal()] = 1;
            iArr[TimeMode.MONTHLY.ordinal()] = 2;
            iArr[TimeMode.YEARLY.ordinal()] = 3;
            a = iArr;
        }
    }

    /* compiled from: Transformations.kt */
    /* loaded from: classes2.dex */
    public static final class c<I, O> implements ud1<List<? extends RoomReflectionsTokenAndData>, List<RoomReflectionsToken>> {
        public c() {
        }

        @Override // defpackage.ud1
        public final List<RoomReflectionsToken> apply(List<? extends RoomReflectionsTokenAndData> list) {
            int i;
            String e;
            String packageName = ReflectionTrackerViewModel.this.a().getPackageName();
            ArrayList arrayList = new ArrayList();
            for (RoomReflectionsTokenAndData roomReflectionsTokenAndData : list) {
                RoomReflectionsToken token = roomReflectionsTokenAndData.getToken();
                if (token != null) {
                    if (cv3.l(token.getIconResName()) == null) {
                        i = Integer.valueOf(ReflectionTrackerViewModel.this.a().getResources().getIdentifier(token.getIconResName(), "drawable", packageName));
                    } else {
                        i = 0;
                    }
                    token.setIconResId(i);
                    Long firstTimeStamp = token.getFirstTimeStamp();
                    if (firstTimeStamp == null) {
                        e = null;
                    } else {
                        long longValue = firstTimeStamp.longValue();
                        b30 b30Var = b30.a;
                        Date date = new Date(longValue * 1000);
                        Application a = ReflectionTrackerViewModel.this.a();
                        fs1.e(a, "getApplication()");
                        e = b30Var.e(date, a);
                    }
                    token.setDisplayDate(e);
                    BigInteger bigInteger = BigInteger.ZERO;
                    List b0 = j20.b0(roomReflectionsTokenAndData.getData());
                    HashSet hashSet = new HashSet();
                    ArrayList<RoomReflectionsData> arrayList2 = new ArrayList();
                    for (Object obj : b0) {
                        if (hashSet.add(Long.valueOf(((RoomReflectionsData) obj).getTimeStamp()))) {
                            arrayList2.add(obj);
                        }
                    }
                    for (RoomReflectionsData roomReflectionsData : arrayList2) {
                        try {
                            BigInteger i2 = bv3.i(roomReflectionsData.getNativeBalance());
                            fs1.d(i2);
                            BigInteger i3 = bv3.i(roomReflectionsData.getBlockBalance());
                            fs1.d(i3);
                            BigInteger subtract = i2.subtract(i3);
                            fs1.e(subtract, "this.subtract(other)");
                            if (subtract.compareTo(BigInteger.ZERO) > 0) {
                                fs1.e(bigInteger, "total");
                                BigInteger add = bigInteger.add(subtract);
                                fs1.e(add, "this.add(other)");
                                bigInteger = add;
                            }
                        } catch (Exception unused) {
                        }
                    }
                    fs1.e(bigInteger, "total");
                    BigDecimal r = e30.r(bigInteger, token.getDecimals());
                    fs1.e(r, "total.fromWEI(it.decimals)");
                    token.setDifferenceBalance(r);
                }
                arrayList.add(roomReflectionsTokenAndData.getToken());
            }
            return arrayList;
        }
    }

    /* compiled from: Transformations.kt */
    /* loaded from: classes2.dex */
    public static final class d<I, O> implements ud1<TokenType, LiveData<List<? extends RoomReflectionsTokenAndData>>> {
        public d() {
        }

        @Override // defpackage.ud1
        /* renamed from: a */
        public final LiveData<List<? extends RoomReflectionsTokenAndData>> apply(TokenType tokenType) {
            return ReflectionTrackerViewModel.this.r().e(tokenType);
        }
    }

    /* compiled from: Transformations.kt */
    /* loaded from: classes2.dex */
    public static final class e<I, O> implements ud1<List<? extends RoomReflectionsDataAndToken>, List<RoomReflectionsDataAndToken>> {
        public e() {
        }

        @Override // defpackage.ud1
        public final List<RoomReflectionsDataAndToken> apply(List<? extends RoomReflectionsDataAndToken> list) {
            String sb;
            String str;
            RoomReflectionsData data;
            List<? extends RoomReflectionsDataAndToken> list2 = list;
            ArrayList arrayList = new ArrayList();
            fs1.e(list2, "_list");
            List b0 = j20.b0(list2);
            HashSet hashSet = new HashSet();
            ArrayList arrayList2 = new ArrayList();
            for (Object obj : b0) {
                if (hashSet.add(Long.valueOf(((RoomReflectionsDataAndToken) obj).getData().getTimeStamp()))) {
                    arrayList2.add(obj);
                }
            }
            arrayList.addAll(j20.b0(arrayList2));
            te4 te4Var = te4.a;
            int i = 0;
            Iterator it = arrayList.iterator();
            while (true) {
                Long l = null;
                if (!it.hasNext()) {
                    break;
                }
                Object next = it.next();
                int i2 = i + 1;
                if (i < 0) {
                    b20.p();
                }
                RoomReflectionsDataAndToken roomReflectionsDataAndToken = (RoomReflectionsDataAndToken) next;
                RoomReflectionsDataAndToken roomReflectionsDataAndToken2 = i2 < arrayList.size() ? (RoomReflectionsDataAndToken) arrayList.get(i2) : null;
                RoomReflectionsData data2 = roomReflectionsDataAndToken.getData();
                if (roomReflectionsDataAndToken2 != null && (data = roomReflectionsDataAndToken2.getData()) != null) {
                    l = Long.valueOf(data.getTimeStamp());
                }
                data2.setStartTime(l == null ? roomReflectionsDataAndToken.getData().getTimeStamp() : l.longValue());
                i = i2;
            }
            ArrayList arrayList3 = new ArrayList();
            RoomReflectionsDataAndToken roomReflectionsDataAndToken3 = (RoomReflectionsDataAndToken) j20.V(arrayList);
            if (roomReflectionsDataAndToken3 != null) {
                RoomReflectionsData data3 = roomReflectionsDataAndToken3.getData();
                RoomReflectionsToken token = roomReflectionsDataAndToken3.getToken();
                Calendar calendar = Calendar.getInstance();
                calendar.setTime(new Date(1000 * roomReflectionsDataAndToken3.getData().getTimeStamp()));
                te4 te4Var2 = te4.a;
                a value = ReflectionTrackerViewModel.this.n().getValue();
                TimeMode b = value == null ? null : value.b();
                int i3 = b == null ? -1 : b.a[b.ordinal()];
                if (i3 == 1) {
                    calendar.add(3, -1);
                } else if (i3 == 2) {
                    calendar.add(2, -1);
                } else if (i3 != 3) {
                    calendar.add(5, -1);
                } else {
                    calendar.add(1, -1);
                }
                if (token.getFirstTimeStamp() != null) {
                    b30 b30Var = b30.a;
                    Date time = calendar.getTime();
                    fs1.e(time, "calendar.time");
                    long z = b30Var.z(time);
                    Long firstTimeStamp = token.getFirstTimeStamp();
                    fs1.d(firstTimeStamp);
                    if (z < firstTimeStamp.longValue()) {
                        String symbolWithType = data3.getSymbolWithType();
                        Date time2 = calendar.getTime();
                        fs1.e(time2, "calendar.time");
                        arrayList.add(new RoomReflectionsDataAndToken(new RoomReflectionsData(Long.MAX_VALUE, symbolWithType, "0", "0", "-9223372036854775808", b30Var.z(time2)), token));
                    }
                }
            }
            LinkedHashMap linkedHashMap = new LinkedHashMap();
            for (Object obj2 : arrayList) {
                Calendar calendar2 = Calendar.getInstance();
                calendar2.setTime(new Date(1000 * ((RoomReflectionsDataAndToken) obj2).getData().getTimeStamp()));
                te4 te4Var3 = te4.a;
                a value2 = ReflectionTrackerViewModel.this.n().getValue();
                TimeMode b2 = value2 == null ? null : value2.b();
                int i4 = b2 == null ? -1 : b.a[b2.ordinal()];
                if (i4 == 1) {
                    str = "Week " + calendar2.get(3) + ' ' + calendar2.get(1);
                } else if (i4 == 2) {
                    StringBuilder sb2 = new StringBuilder();
                    sb2.append((Object) calendar2.getDisplayName(2, 2, Locale.ENGLISH));
                    sb2.append(' ');
                    sb2.append(calendar2.get(1));
                    str = sb2.toString();
                } else if (i4 != 3) {
                    b30 b30Var2 = b30.a;
                    Date time3 = calendar2.getTime();
                    fs1.e(time3, "calendar.time");
                    Application a = ReflectionTrackerViewModel.this.a();
                    fs1.e(a, "getApplication()");
                    str = String.valueOf(b30Var2.e(time3, a));
                } else {
                    str = String.valueOf(calendar2.get(1));
                }
                Object obj3 = linkedHashMap.get(str);
                if (obj3 == null) {
                    obj3 = new ArrayList();
                    linkedHashMap.put(str, obj3);
                }
                ((List) obj3).add(obj2);
            }
            for (Map.Entry entry : linkedHashMap.entrySet()) {
                RoomReflectionsDataAndToken roomReflectionsDataAndToken4 = (RoomReflectionsDataAndToken) j20.L((List) entry.getValue());
                RoomReflectionsDataAndToken roomReflectionsDataAndToken5 = (RoomReflectionsDataAndToken) j20.U((List) entry.getValue());
                float f = 0.0f;
                for (RoomReflectionsDataAndToken roomReflectionsDataAndToken6 : (Iterable) entry.getValue()) {
                    float floatValue = e30.r(new BigInteger(roomReflectionsDataAndToken6.getData().getNativeBalance()), roomReflectionsDataAndToken6.getToken().getDecimals()).floatValue() - e30.r(new BigInteger(roomReflectionsDataAndToken6.getData().getBlockBalance()), roomReflectionsDataAndToken6.getToken().getDecimals()).floatValue();
                    if (floatValue > Utils.FLOAT_EPSILON) {
                        f += floatValue;
                    }
                    roomReflectionsDataAndToken4.setDiffBalance(Float.valueOf(f));
                }
                long j = 1000;
                Date date = new Date(roomReflectionsDataAndToken4.getData().getTimeStamp() * j);
                Date date2 = new Date(roomReflectionsDataAndToken5.getData().getStartTime() * j);
                if (fs1.b(date, date2)) {
                    b30 b30Var3 = b30.a;
                    Application a2 = ReflectionTrackerViewModel.this.a();
                    fs1.e(a2, "getApplication()");
                    sb = b30Var3.d(date, a2);
                } else {
                    StringBuilder sb3 = new StringBuilder();
                    b30 b30Var4 = b30.a;
                    Application a3 = ReflectionTrackerViewModel.this.a();
                    fs1.e(a3, "getApplication()");
                    sb3.append(b30Var4.d(date, a3));
                    sb3.append(' ');
                    sb3.append(ReflectionTrackerViewModel.this.a().getString(R.string.reflection_tracker_display_date_to));
                    sb3.append('\n');
                    Application a4 = ReflectionTrackerViewModel.this.a();
                    fs1.e(a4, "getApplication()");
                    sb3.append(b30Var4.d(date2, a4));
                    sb = sb3.toString();
                }
                roomReflectionsDataAndToken4.setDisplayDate(sb);
                if (!fs1.b(date, date2)) {
                    arrayList3.add(roomReflectionsDataAndToken4);
                }
            }
            return arrayList3;
        }
    }

    /* compiled from: Transformations.kt */
    /* loaded from: classes2.dex */
    public static final class f<I, O> implements ud1<RoomReflectionsTokenAndData, RoomReflectionsToken> {
        public f() {
        }

        @Override // defpackage.ud1
        public final RoomReflectionsToken apply(RoomReflectionsTokenAndData roomReflectionsTokenAndData) {
            int i;
            String e;
            RoomReflectionsTokenAndData roomReflectionsTokenAndData2 = roomReflectionsTokenAndData;
            RoomReflectionsToken token = roomReflectionsTokenAndData2.getToken();
            String packageName = ReflectionTrackerViewModel.this.a().getPackageName();
            if (cv3.l(token.getIconResName()) == null) {
                i = Integer.valueOf(ReflectionTrackerViewModel.this.a().getResources().getIdentifier(token.getIconResName(), "drawable", packageName));
            } else {
                i = 0;
            }
            token.setIconResId(i);
            Long firstTimeStamp = token.getFirstTimeStamp();
            if (firstTimeStamp == null) {
                e = null;
            } else {
                long longValue = firstTimeStamp.longValue();
                b30 b30Var = b30.a;
                Date date = new Date(longValue * 1000);
                Application a = ReflectionTrackerViewModel.this.a();
                fs1.e(a, "getApplication()");
                e = b30Var.e(date, a);
            }
            token.setDisplayDate(e);
            BigInteger bigInteger = BigInteger.ZERO;
            List b0 = j20.b0(roomReflectionsTokenAndData2.getData());
            HashSet hashSet = new HashSet();
            ArrayList<RoomReflectionsData> arrayList = new ArrayList();
            for (Object obj : b0) {
                if (hashSet.add(Long.valueOf(((RoomReflectionsData) obj).getTimeStamp()))) {
                    arrayList.add(obj);
                }
            }
            for (RoomReflectionsData roomReflectionsData : arrayList) {
                try {
                    BigInteger i2 = bv3.i(roomReflectionsData.getNativeBalance());
                    fs1.d(i2);
                    BigInteger i3 = bv3.i(roomReflectionsData.getBlockBalance());
                    fs1.d(i3);
                    BigInteger subtract = i2.subtract(i3);
                    fs1.e(subtract, "this.subtract(other)");
                    if (subtract.compareTo(BigInteger.ZERO) > 0) {
                        fs1.e(bigInteger, "total");
                        BigInteger add = bigInteger.add(subtract);
                        fs1.e(add, "this.add(other)");
                        bigInteger = add;
                    }
                } catch (Exception unused) {
                }
            }
            fs1.e(bigInteger, "total");
            BigDecimal r = e30.r(bigInteger, token.getDecimals());
            fs1.e(r, "total.fromWEI(it.decimals)");
            token.setDifferenceBalance(r);
            return roomReflectionsTokenAndData2.getToken();
        }
    }

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public ReflectionTrackerViewModel(Application application) {
        super(application);
        fs1.f(application, "application");
        this.b = new gb2<>(s());
        this.c = new gb2<>();
        this.d = zy1.a(new ReflectionTrackerViewModel$reflectionDataSource$2(application));
        this.e = zy1.a(new ReflectionTrackerViewModel$userDataSource$2(application));
        this.f = zy1.a(new ReflectionTrackerViewModel$reflectionCustomContract$2(this));
        gb2<a> gb2Var = new gb2<>(new a("", null, null, 6, null));
        this.g = gb2Var;
        LiveData<List<RoomReflectionsDataAndToken>> c2 = cb4.c(gb2Var, new ud1() { // from class: k53
            @Override // defpackage.ud1
            public final Object apply(Object obj) {
                LiveData x;
                x = ReflectionTrackerViewModel.x(ReflectionTrackerViewModel.this, (ReflectionTrackerViewModel.a) obj);
                return x;
            }
        });
        fs1.e(c2, "switchMap(filterReflecti…ReflectionsData(param)  }");
        this.h = c2;
        C();
        B();
        this.i = zy1.a(new ReflectionTrackerViewModel$getTokenTypeMapUseCase$2(application));
    }

    public static final LiveData x(ReflectionTrackerViewModel reflectionTrackerViewModel, a aVar) {
        fs1.f(reflectionTrackerViewModel, "this$0");
        ReflectionDataSource r = reflectionTrackerViewModel.r();
        fs1.e(aVar, "param");
        return r.f(aVar);
    }

    /* JADX WARN: Can't wrap try/catch for region: R(16:49|50|51|(1:53)(6:154|(8:157|158|159|160|161|162|(8:164|165|166|167|168|169|170|(7:172|173|174|175|176|(2:178|179)(2:181|182)|180)(3:185|186|187))(3:195|196|197)|155)|202|203|204|205)|54|55|56|57|58|59|60|61|62|63|64|(1:66)(4:67|68|69|(5:71|72|73|74|(1:76)(5:77|78|79|80|(4:82|83|84|(1:86)(4:87|88|89|(4:91|92|93|(1:95)(4:96|97|98|(6:118|119|120|121|122|(1:124)(6:125|126|127|101|(1:103)|(7:(1:115)(4:110|111|112|(1:114))|16|17|18|19|20|(1:223)(0))(6:105|106|18|19|20|(0)(0))))(4:100|101|(0)|(0)(0))))(2:133|(0)(0))))(2:134|(0)(0))))(2:140|(0)(0)))) */
    /* JADX WARN: Can't wrap try/catch for region: R(5:71|72|73|74|(1:76)(5:77|78|79|80|(4:82|83|84|(1:86)(4:87|88|89|(4:91|92|93|(1:95)(4:96|97|98|(6:118|119|120|121|122|(1:124)(6:125|126|127|101|(1:103)|(7:(1:115)(4:110|111|112|(1:114))|16|17|18|19|20|(1:223)(0))(6:105|106|18|19|20|(0)(0))))(4:100|101|(0)|(0)(0))))(2:133|(0)(0))))(2:134|(0)(0)))) */
    /* JADX WARN: Can't wrap try/catch for region: R(6:118|119|120|121|122|(1:124)(6:125|126|127|101|(1:103)|(7:(1:115)(4:110|111|112|(1:114))|16|17|18|19|20|(1:223)(0))(6:105|106|18|19|20|(0)(0)))) */
    /* JADX WARN: Can't wrap try/catch for region: R(6:154|(8:157|158|159|160|161|162|(8:164|165|166|167|168|169|170|(7:172|173|174|175|176|(2:178|179)(2:181|182)|180)(3:185|186|187))(3:195|196|197)|155)|202|203|204|205) */
    /* JADX WARN: Can't wrap try/catch for region: R(6:1|(2:3|(4:5|6|7|8))|296|6|7|8) */
    /* JADX WARN: Can't wrap try/catch for region: R(6:24|25|26|27|28|(1:30)(4:31|32|33|(1:35)(9:36|37|38|(8:40|41|42|43|44|45|46|(1:48)(16:49|50|51|(1:53)(6:154|(8:157|158|159|160|161|162|(8:164|165|166|167|168|169|170|(7:172|173|174|175|176|(2:178|179)(2:181|182)|180)(3:185|186|187))(3:195|196|197)|155)|202|203|204|205)|54|55|56|57|58|59|60|61|62|63|64|(1:66)(4:67|68|69|(5:71|72|73|74|(1:76)(5:77|78|79|80|(4:82|83|84|(1:86)(4:87|88|89|(4:91|92|93|(1:95)(4:96|97|98|(6:118|119|120|121|122|(1:124)(6:125|126|127|101|(1:103)|(7:(1:115)(4:110|111|112|(1:114))|16|17|18|19|20|(1:223)(0))(6:105|106|18|19|20|(0)(0))))(4:100|101|(0)|(0)(0))))(2:133|(0)(0))))(2:134|(0)(0))))(2:140|(0)(0)))))(1:213)|210|18|19|20|(0)(0)))) */
    /* JADX WARN: Can't wrap try/catch for region: R(7:(1:115)(4:110|111|112|(1:114))|16|17|18|19|20|(1:223)(0)) */
    /* JADX WARN: Can't wrap try/catch for region: R(8:40|41|42|43|44|45|46|(1:48)(16:49|50|51|(1:53)(6:154|(8:157|158|159|160|161|162|(8:164|165|166|167|168|169|170|(7:172|173|174|175|176|(2:178|179)(2:181|182)|180)(3:185|186|187))(3:195|196|197)|155)|202|203|204|205)|54|55|56|57|58|59|60|61|62|63|64|(1:66)(4:67|68|69|(5:71|72|73|74|(1:76)(5:77|78|79|80|(4:82|83|84|(1:86)(4:87|88|89|(4:91|92|93|(1:95)(4:96|97|98|(6:118|119|120|121|122|(1:124)(6:125|126|127|101|(1:103)|(7:(1:115)(4:110|111|112|(1:114))|16|17|18|19|20|(1:223)(0))(6:105|106|18|19|20|(0)(0))))(4:100|101|(0)|(0)(0))))(2:133|(0)(0))))(2:134|(0)(0))))(2:140|(0)(0))))) */
    /* JADX WARN: Code restructure failed: missing block: B:194:0x05e5, code lost:
        r12 = r2;
        r15 = r19;
        r13 = r22;
     */
    /* JADX WARN: Code restructure failed: missing block: B:213:0x069f, code lost:
        r15 = r19;
        r13 = r21;
        r12 = r22;
     */
    /* JADX WARN: Code restructure failed: missing block: B:214:0x06a6, code lost:
        r20 = r2;
     */
    /* JADX WARN: Code restructure failed: missing block: B:215:0x06a8, code lost:
        r5 = r17;
        r15 = r19;
     */
    /* JADX WARN: Code restructure failed: missing block: B:216:0x06ad, code lost:
        r20 = r2;
        r5 = r17;
        r15 = r19;
        r12 = r21;
     */
    /* JADX WARN: Code restructure failed: missing block: B:217:0x06b6, code lost:
        r5 = r17;
        r15 = r19;
        r6 = r20;
        r12 = r21;
        r20 = r2;
     */
    /* JADX WARN: Code restructure failed: missing block: B:218:0x06c0, code lost:
        r4 = r6;
     */
    /* JADX WARN: Code restructure failed: missing block: B:220:0x06ca, code lost:
        r20 = r2;
        r15 = r19;
        r13 = r3;
     */
    /* JADX WARN: Code restructure failed: missing block: B:221:0x06d0, code lost:
        r20 = r2;
        r17 = r14;
        r15 = r19;
     */
    /* JADX WARN: Code restructure failed: missing block: B:227:0x06f0, code lost:
        r20 = r2;
     */
    /* JADX WARN: Code restructure failed: missing block: B:31:0x0152, code lost:
        r20 = "(this as java.lang.Strin….toLowerCase(Locale.ROOT)";
        r13 = r6;
        r6 = r14;
        r12 = r15;
     */
    /* JADX WARN: Code restructure failed: missing block: B:32:0x0158, code lost:
        r15 = r3;
     */
    /* JADX WARN: Not initialized variable reg: 14, insn: 0x0155: MOVE  (r6 I:??[OBJECT, ARRAY]) = (r14 I:??[OBJECT, ARRAY]), block:B:31:0x0152 */
    /* JADX WARN: Not initialized variable reg: 15, insn: 0x0156: MOVE  (r12 I:??[OBJECT, ARRAY]) = (r15 I:??[OBJECT, ARRAY]), block:B:31:0x0152 */
    /* JADX WARN: Removed duplicated region for block: B:103:0x034c  */
    /* JADX WARN: Removed duplicated region for block: B:104:0x0357 A[Catch: Exception -> 0x06c3, TryCatch #27 {Exception -> 0x06c3, blocks: (B:101:0x032f, B:104:0x0357, B:105:0x035b), top: B:291:0x032f }] */
    /* JADX WARN: Removed duplicated region for block: B:10:0x0031  */
    /* JADX WARN: Removed duplicated region for block: B:12:0x0039  */
    /* JADX WARN: Removed duplicated region for block: B:150:0x049e A[RETURN] */
    /* JADX WARN: Removed duplicated region for block: B:151:0x049f  */
    /* JADX WARN: Removed duplicated region for block: B:154:0x04ad A[Catch: Exception -> 0x069b, TRY_LEAVE, TryCatch #13 {Exception -> 0x069b, blocks: (B:152:0x04a9, B:154:0x04ad, B:165:0x04fd, B:175:0x0547), top: B:264:0x04a9 }] */
    /* JADX WARN: Removed duplicated region for block: B:15:0x004d  */
    /* JADX WARN: Removed duplicated region for block: B:163:0x04f3  */
    /* JADX WARN: Removed duplicated region for block: B:165:0x04fd A[Catch: Exception -> 0x069b, TRY_ENTER, TRY_LEAVE, TryCatch #13 {Exception -> 0x069b, blocks: (B:152:0x04a9, B:154:0x04ad, B:165:0x04fd, B:175:0x0547), top: B:264:0x04a9 }] */
    /* JADX WARN: Removed duplicated region for block: B:173:0x053d  */
    /* JADX WARN: Removed duplicated region for block: B:175:0x0547 A[Catch: Exception -> 0x069b, TRY_ENTER, TRY_LEAVE, TryCatch #13 {Exception -> 0x069b, blocks: (B:152:0x04a9, B:154:0x04ad, B:165:0x04fd, B:175:0x0547), top: B:264:0x04a9 }] */
    /* JADX WARN: Removed duplicated region for block: B:183:0x058a  */
    /* JADX WARN: Removed duplicated region for block: B:195:0x05ec  */
    /* JADX WARN: Removed duplicated region for block: B:198:0x0608 A[Catch: Exception -> 0x05df, TryCatch #9 {Exception -> 0x05df, blocks: (B:191:0x05d9, B:196:0x05fb, B:198:0x0608, B:202:0x0614, B:204:0x061c), top: B:256:0x05d9 }] */
    /* JADX WARN: Removed duplicated region for block: B:19:0x0087  */
    /* JADX WARN: Removed duplicated region for block: B:200:0x0611  */
    /* JADX WARN: Removed duplicated region for block: B:201:0x0612  */
    /* JADX WARN: Removed duplicated region for block: B:222:0x06d8  */
    /* JADX WARN: Removed duplicated region for block: B:22:0x00ba  */
    /* JADX WARN: Removed duplicated region for block: B:230:0x06fe  */
    /* JADX WARN: Removed duplicated region for block: B:25:0x00ed  */
    /* JADX WARN: Removed duplicated region for block: B:272:0x059e A[EXC_TOP_SPLITTER, SYNTHETIC] */
    /* JADX WARN: Removed duplicated region for block: B:28:0x0120  */
    /* JADX WARN: Removed duplicated region for block: B:33:0x015b  */
    /* JADX WARN: Removed duplicated region for block: B:38:0x0182  */
    /* JADX WARN: Removed duplicated region for block: B:43:0x01a5  */
    /* JADX WARN: Removed duplicated region for block: B:47:0x01c2  */
    /* JADX WARN: Removed duplicated region for block: B:48:0x01ce  */
    /* JADX WARN: Removed duplicated region for block: B:57:0x01f7  */
    /* JADX WARN: Removed duplicated region for block: B:78:0x0270  */
    /* JADX WARN: Removed duplicated region for block: B:89:0x02d2 A[RETURN] */
    /* JADX WARN: Removed duplicated region for block: B:90:0x02d3  */
    /* JADX WARN: Removed duplicated region for block: B:93:0x02df A[Catch: Exception -> 0x06e1, TRY_LEAVE, TryCatch #18 {Exception -> 0x06e1, blocks: (B:91:0x02db, B:93:0x02df), top: B:274:0x02db }] */
    /* JADX WARN: Type inference failed for: r1v43, types: [T, java.math.BigInteger, java.lang.Object] */
    /* JADX WARN: Type inference failed for: r1v48, types: [T, java.math.BigInteger, java.lang.Object] */
    /* JADX WARN: Type inference failed for: r6v0, types: [int] */
    /* JADX WARN: Type inference failed for: r6v19, types: [T, java.math.BigInteger] */
    /* JADX WARN: Unsupported multi-entry loop pattern (BACK_EDGE: B:138:0x044b -> B:229:0x06f9). Please submit an issue!!! */
    /* JADX WARN: Unsupported multi-entry loop pattern (BACK_EDGE: B:162:0x04ed -> B:223:0x06df). Please submit an issue!!! */
    /* JADX WARN: Unsupported multi-entry loop pattern (BACK_EDGE: B:192:0x05df -> B:223:0x06df). Please submit an issue!!! */
    /* JADX WARN: Unsupported multi-entry loop pattern (BACK_EDGE: B:194:0x05e5 -> B:223:0x06df). Please submit an issue!!! */
    /* JADX WARN: Unsupported multi-entry loop pattern (BACK_EDGE: B:207:0x0693 -> B:260:0x0698). Please submit an issue!!! */
    /* JADX WARN: Unsupported multi-entry loop pattern (BACK_EDGE: B:209:0x0696 -> B:260:0x0698). Please submit an issue!!! */
    /* JADX WARN: Unsupported multi-entry loop pattern (BACK_EDGE: B:213:0x069f -> B:223:0x06df). Please submit an issue!!! */
    /* JADX WARN: Unsupported multi-entry loop pattern (BACK_EDGE: B:225:0x06e7 -> B:223:0x06df). Please submit an issue!!! */
    /* JADX WARN: Unsupported multi-entry loop pattern (BACK_EDGE: B:226:0x06ea -> B:229:0x06f9). Please submit an issue!!! */
    /* JADX WARN: Unsupported multi-entry loop pattern (BACK_EDGE: B:228:0x06f5 -> B:223:0x06df). Please submit an issue!!! */
    /* JADX WARN: Unsupported multi-entry loop pattern (BACK_EDGE: B:32:0x0158 -> B:229:0x06f9). Please submit an issue!!! */
    /* JADX WARN: Unsupported multi-entry loop pattern (BACK_EDGE: B:37:0x017d -> B:223:0x06df). Please submit an issue!!! */
    /*
        Code decompiled incorrectly, please refer to instructions dump.
        To view partially-correct code enable 'Show inconsistent code' option in preferences
    */
    public final java.lang.Object A(net.safemoon.androidwallet.model.reflections.RoomReflectionsToken r33, defpackage.q70<? super defpackage.te4> r34) {
        /*
            Method dump skipped, instructions count: 1836
            To view this dump change 'Code comments level' option to 'DEBUG'
        */
        throw new UnsupportedOperationException("Method not decompiled: net.safemoon.androidwallet.viewmodels.ReflectionTrackerViewModel.A(net.safemoon.androidwallet.model.reflections.RoomReflectionsToken, q70):java.lang.Object");
    }

    public final void B() {
        as.b(i(), null, null, new ReflectionTrackerViewModel$setDailyData$1(this, null), 3, null);
    }

    public final void C() {
        as.b(i(), null, null, new ReflectionTrackerViewModel$setDefaultReflectionToken$1(this, null), 3, null);
    }

    public final void D(String str) {
        fs1.f(str, "symbolWithType");
        gb2<a> gb2Var = this.g;
        a value = gb2Var.getValue();
        a aVar = value;
        if (aVar != null) {
            aVar.d(str);
        }
        te4 te4Var = te4.a;
        gb2Var.postValue(value);
    }

    public final void E(TimeMode timeMode) {
        fs1.f(timeMode, "timeMode");
        gb2<a> gb2Var = this.g;
        a value = gb2Var.getValue();
        a aVar = value;
        if (aVar != null) {
            aVar.e(timeMode);
        }
        te4 te4Var = te4.a;
        gb2Var.postValue(value);
    }

    public final void F(TimeSpan timeSpan) {
        fs1.f(timeSpan, "timeSpan");
        gb2<a> gb2Var = this.g;
        a value = gb2Var.getValue();
        a aVar = value;
        if (aVar != null) {
            aVar.f(timeSpan);
        }
        te4 te4Var = te4.a;
        gb2Var.postValue(value);
    }

    public final void G() {
        as.b(i(), null, null, new ReflectionTrackerViewModel$updatePrice$1(this, null), 3, null);
    }

    public final c90 i() {
        return d90.a(tp0.b());
    }

    public final void j(RoomReflectionsToken roomReflectionsToken) {
        fs1.f(roomReflectionsToken, "rrt");
        as.b(i(), null, null, new ReflectionTrackerViewModel$deleteSymbolWithType$1(roomReflectionsToken, this, null), 3, null);
    }

    public final j71<List<RoomReflectionsToken>> k(gb2<TokenType> gb2Var) {
        fs1.f(gb2Var, "chainTokenType");
        LiveData c2 = cb4.c(gb2Var, new d());
        fs1.e(c2, "Transformations.switchMap(this) { transform(it) }");
        LiveData b2 = cb4.b(c2, new c());
        fs1.e(b2, "Transformations.map(this) { transform(it) }");
        return n71.c(n71.i(FlowLiveDataConversions.a(b2)), new ReflectionTrackerViewModel$fetchReflectionCached$3(this, null));
    }

    public final j71<List<RoomReflectionsDataAndToken>> l() {
        j71 a2;
        j71 i;
        LiveData<List<RoomReflectionsDataAndToken>> liveData = this.h;
        if (liveData != null) {
            LiveData b2 = cb4.b(liveData, new e());
            fs1.e(b2, "Transformations.map(this) { transform(it) }");
            if (b2 != null && (a2 = FlowLiveDataConversions.a(b2)) != null) {
                i = n71.i(a2);
                return n71.c(i, new ReflectionTrackerViewModel$fetchReflectionData$2(this, null));
            }
        }
        i = null;
        return n71.c(i, new ReflectionTrackerViewModel$fetchReflectionData$2(this, null));
    }

    public final LiveData<RoomReflectionsToken> m(String str) {
        fs1.f(str, "symbolWithType");
        LiveData<RoomReflectionsTokenAndData> b2 = r().b(str);
        if (b2 == null) {
            return null;
        }
        LiveData a2 = cb4.a(b2);
        fs1.e(a2, "Transformations.distinctUntilChanged(this)");
        if (a2 == null) {
            return null;
        }
        LiveData<RoomReflectionsToken> b3 = cb4.b(a2, new f());
        fs1.e(b3, "Transformations.map(this) { transform(it) }");
        return b3;
    }

    public final gb2<a> n() {
        return this.g;
    }

    public final em1 o() {
        return (em1) this.i.getValue();
    }

    public final gb2<Date> p() {
        return this.c;
    }

    public final ReflectionCustomContract q() {
        return (ReflectionCustomContract) this.f.getValue();
    }

    public final ReflectionDataSource r() {
        return (ReflectionDataSource) this.d.getValue();
    }

    public final TokenType s() {
        TokenType.a aVar = TokenType.Companion;
        String j = bo3.j(a(), "DEFAULT_GATEWAY", TokenType.BEP_20.getName());
        fs1.e(j, "getString(getApplication…kenType.BEP_20.getName())");
        return aVar.c(j);
    }

    public final gb2<TokenType> t() {
        return this.b;
    }

    public final Map<String, TokenType> u() {
        if (this.j == null) {
            this.j = o().get();
        }
        Map map = this.j;
        fs1.d(map);
        return map;
    }

    public final LocalUserTokenDataSource v() {
        return (LocalUserTokenDataSource) this.e.getValue();
    }

    public final void w(RoomReflectionsToken roomReflectionsToken) {
        fs1.f(roomReflectionsToken, "rrt");
        as.b(i(), null, null, new ReflectionTrackerViewModel$onChangeReflectionEnable$1(this, roomReflectionsToken, null), 3, null);
    }

    public final void y(String str) {
        fs1.f(str, "symbolWithType");
        as.b(i(), null, null, new ReflectionTrackerViewModel$removeAndSeed$1(this, str, null), 3, null);
    }

    /* JADX WARN: Removed duplicated region for block: B:10:0x0023  */
    /* JADX WARN: Removed duplicated region for block: B:14:0x0031  */
    /*
        Code decompiled incorrectly, please refer to instructions dump.
        To view partially-correct code enable 'Show inconsistent code' option in preferences
    */
    public final java.lang.Object z(long r5, net.safemoon.androidwallet.model.reflections.RoomReflectionsToken r7, defpackage.k83 r8, defpackage.q70<? super java.math.BigInteger> r9) {
        /*
            r4 = this;
            boolean r0 = r9 instanceof net.safemoon.androidwallet.viewmodels.ReflectionTrackerViewModel$retryGetBalance$1
            if (r0 == 0) goto L13
            r0 = r9
            net.safemoon.androidwallet.viewmodels.ReflectionTrackerViewModel$retryGetBalance$1 r0 = (net.safemoon.androidwallet.viewmodels.ReflectionTrackerViewModel$retryGetBalance$1) r0
            int r1 = r0.label
            r2 = -2147483648(0xffffffff80000000, float:-0.0)
            r3 = r1 & r2
            if (r3 == 0) goto L13
            int r1 = r1 - r2
            r0.label = r1
            goto L18
        L13:
            net.safemoon.androidwallet.viewmodels.ReflectionTrackerViewModel$retryGetBalance$1 r0 = new net.safemoon.androidwallet.viewmodels.ReflectionTrackerViewModel$retryGetBalance$1
            r0.<init>(r4, r9)
        L18:
            java.lang.Object r9 = r0.result
            java.lang.Object r1 = defpackage.gs1.d()
            int r2 = r0.label
            r3 = 1
            if (r2 == 0) goto L31
            if (r2 != r3) goto L29
            defpackage.o83.b(r9)
            goto L5f
        L29:
            java.lang.IllegalStateException r5 = new java.lang.IllegalStateException
            java.lang.String r6 = "call to 'resume' before 'invoke' with coroutine"
            r5.<init>(r6)
            throw r5
        L31:
            defpackage.o83.b(r9)
            java.lang.Thread.sleep(r5)
            net.safemoon.androidwallet.utils.ReflectionCustomContract r5 = r4.q()
            net.safemoon.androidwallet.common.TokenType$a r6 = net.safemoon.androidwallet.common.TokenType.Companion
            int r9 = r7.getChainId()
            net.safemoon.androidwallet.common.TokenType r6 = r6.b(r9)
            java.math.BigInteger r8 = r8.a()
            java.lang.String r8 = r8.toString()
            java.lang.String r9 = "blockOfDate.block.toString()"
            defpackage.fs1.e(r8, r9)
            java.lang.String r7 = r7.getContractAddress()
            r0.label = r3
            java.lang.Object r9 = r5.e(r6, r8, r7, r0)
            if (r9 != r1) goto L5f
            return r1
        L5f:
            java.math.BigInteger r9 = (java.math.BigInteger) r9
            return r9
        */
        throw new UnsupportedOperationException("Method not decompiled: net.safemoon.androidwallet.viewmodels.ReflectionTrackerViewModel.z(long, net.safemoon.androidwallet.model.reflections.RoomReflectionsToken, k83, q70):java.lang.Object");
    }

    /* compiled from: ReflectionTrackerViewModel.kt */
    /* loaded from: classes2.dex */
    public static final class a {
        public String a;
        public TimeMode b;
        public TimeSpan c;

        public a(String str, TimeMode timeMode, TimeSpan timeSpan) {
            fs1.f(str, "symbolWithType");
            fs1.f(timeMode, "timeMode");
            fs1.f(timeSpan, "timeSpan");
            this.a = str;
            this.b = timeMode;
            this.c = timeSpan;
        }

        public final String a() {
            return this.a;
        }

        public final TimeMode b() {
            return this.b;
        }

        public final TimeSpan c() {
            return this.c;
        }

        public final void d(String str) {
            fs1.f(str, "<set-?>");
            this.a = str;
        }

        public final void e(TimeMode timeMode) {
            fs1.f(timeMode, "<set-?>");
            this.b = timeMode;
        }

        public boolean equals(Object obj) {
            if (this == obj) {
                return true;
            }
            if (obj instanceof a) {
                a aVar = (a) obj;
                return fs1.b(this.a, aVar.a) && this.b == aVar.b && this.c == aVar.c;
            }
            return false;
        }

        public final void f(TimeSpan timeSpan) {
            fs1.f(timeSpan, "<set-?>");
            this.c = timeSpan;
        }

        public int hashCode() {
            return (((this.a.hashCode() * 31) + this.b.hashCode()) * 31) + this.c.hashCode();
        }

        public String toString() {
            return "ReflectionParam(symbolWithType=" + this.a + ", timeMode=" + this.b + ", timeSpan=" + this.c + ')';
        }

        public /* synthetic */ a(String str, TimeMode timeMode, TimeSpan timeSpan, int i, qi0 qi0Var) {
            this(str, (i & 2) != 0 ? TimeMode.DAILY : timeMode, (i & 4) != 0 ? TimeSpan.LOW : timeSpan);
        }
    }
}
