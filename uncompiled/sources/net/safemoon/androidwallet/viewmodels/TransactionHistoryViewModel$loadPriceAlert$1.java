package net.safemoon.androidwallet.viewmodels;

import kotlin.coroutines.jvm.internal.ContinuationImpl;
import kotlin.coroutines.jvm.internal.a;

/* compiled from: TransactionHistoryViewModel.kt */
@a(c = "net.safemoon.androidwallet.viewmodels.TransactionHistoryViewModel", f = "TransactionHistoryViewModel.kt", l = {111, 111}, m = "loadPriceAlert")
/* loaded from: classes2.dex */
public final class TransactionHistoryViewModel$loadPriceAlert$1 extends ContinuationImpl {
    public Object L$0;
    public Object L$1;
    public Object L$2;
    public Object L$3;
    public int label;
    public /* synthetic */ Object result;
    public final /* synthetic */ TransactionHistoryViewModel this$0;

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public TransactionHistoryViewModel$loadPriceAlert$1(TransactionHistoryViewModel transactionHistoryViewModel, q70<? super TransactionHistoryViewModel$loadPriceAlert$1> q70Var) {
        super(q70Var);
        this.this$0 = transactionHistoryViewModel;
    }

    @Override // kotlin.coroutines.jvm.internal.BaseContinuationImpl
    public final Object invokeSuspend(Object obj) {
        Object p;
        this.result = obj;
        this.label |= Integer.MIN_VALUE;
        p = this.this$0.p(null, null, this);
        return p;
    }
}
