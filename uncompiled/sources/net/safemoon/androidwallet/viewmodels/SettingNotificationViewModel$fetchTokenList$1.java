package net.safemoon.androidwallet.viewmodels;

import java.util.List;
import kotlin.coroutines.jvm.internal.SuspendLambda;
import kotlin.coroutines.jvm.internal.a;

/* compiled from: SettingNotificationViewModel.kt */
@a(c = "net.safemoon.androidwallet.viewmodels.SettingNotificationViewModel$fetchTokenList$1", f = "SettingNotificationViewModel.kt", l = {124}, m = "invokeSuspend")
/* loaded from: classes2.dex */
public final class SettingNotificationViewModel$fetchTokenList$1 extends SuspendLambda implements hd1<c90, q70<? super te4>, Object> {
    public int label;
    public final /* synthetic */ SettingNotificationViewModel this$0;

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public SettingNotificationViewModel$fetchTokenList$1(SettingNotificationViewModel settingNotificationViewModel, q70<? super SettingNotificationViewModel$fetchTokenList$1> q70Var) {
        super(2, q70Var);
        this.this$0 = settingNotificationViewModel;
    }

    @Override // kotlin.coroutines.jvm.internal.BaseContinuationImpl
    public final q70<te4> create(Object obj, q70<?> q70Var) {
        return new SettingNotificationViewModel$fetchTokenList$1(this.this$0, q70Var);
    }

    @Override // defpackage.hd1
    public final Object invoke(c90 c90Var, q70<? super te4> q70Var) {
        return ((SettingNotificationViewModel$fetchTokenList$1) create(c90Var, q70Var)).invokeSuspend(te4.a);
    }

    @Override // kotlin.coroutines.jvm.internal.BaseContinuationImpl
    public final Object invokeSuspend(Object obj) {
        gb2 gb2Var;
        Object d = gs1.d();
        int i = this.label;
        if (i == 0) {
            o83.b(obj);
            SettingNotificationViewModel settingNotificationViewModel = this.this$0;
            this.label = 1;
            obj = settingNotificationViewModel.s(this);
            if (obj == d) {
                return d;
            }
        } else if (i != 1) {
            throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        } else {
            o83.b(obj);
        }
        gb2Var = this.this$0.k;
        gb2Var.postValue((List) obj);
        return te4.a;
    }
}
