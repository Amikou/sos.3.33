package net.safemoon.androidwallet.viewmodels;

import kotlin.coroutines.jvm.internal.SuspendLambda;
import kotlin.coroutines.jvm.internal.a;
import kotlinx.coroutines.CoroutineDispatcher;
import net.safemoon.androidwallet.model.token.abstraction.IToken;

/* compiled from: TransferViewModel.kt */
@a(c = "net.safemoon.androidwallet.viewmodels.TransferViewModel$sendTransfer$1", f = "TransferViewModel.kt", l = {92}, m = "invokeSuspend")
/* loaded from: classes2.dex */
public final class TransferViewModel$sendTransfer$1 extends SuspendLambda implements hd1<c90, q70<? super te4>, Object> {
    public final /* synthetic */ tc1<Double, te4> $errorCallBack;
    public final /* synthetic */ IToken $iToken;
    public int label;
    public final /* synthetic */ TransferViewModel this$0;

    /* compiled from: TransferViewModel.kt */
    @a(c = "net.safemoon.androidwallet.viewmodels.TransferViewModel$sendTransfer$1$1", f = "TransferViewModel.kt", l = {96, 100, 101, 103}, m = "invokeSuspend")
    /* renamed from: net.safemoon.androidwallet.viewmodels.TransferViewModel$sendTransfer$1$1  reason: invalid class name */
    /* loaded from: classes2.dex */
    public static final class AnonymousClass1 extends SuspendLambda implements hd1<c90, q70<? super te4>, Object> {
        public final /* synthetic */ tc1<Double, te4> $errorCallBack;
        public final /* synthetic */ IToken $iToken;
        public Object L$0;
        public Object L$1;
        public Object L$2;
        public int label;
        public final /* synthetic */ TransferViewModel this$0;

        /* compiled from: TransferViewModel.kt */
        @a(c = "net.safemoon.androidwallet.viewmodels.TransferViewModel$sendTransfer$1$1$2", f = "TransferViewModel.kt", l = {}, m = "invokeSuspend")
        /* renamed from: net.safemoon.androidwallet.viewmodels.TransferViewModel$sendTransfer$1$1$2  reason: invalid class name */
        /* loaded from: classes2.dex */
        public static final class AnonymousClass2 extends SuspendLambda implements hd1<c90, q70<? super te4>, Object> {
            public final /* synthetic */ tc1<Double, te4> $errorCallBack;
            public final /* synthetic */ Double $newGasPriceInGWEI;
            public int label;

            /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
            /* JADX WARN: Multi-variable type inference failed */
            public AnonymousClass2(tc1<? super Double, te4> tc1Var, Double d, q70<? super AnonymousClass2> q70Var) {
                super(2, q70Var);
                this.$errorCallBack = tc1Var;
                this.$newGasPriceInGWEI = d;
            }

            @Override // kotlin.coroutines.jvm.internal.BaseContinuationImpl
            public final q70<te4> create(Object obj, q70<?> q70Var) {
                return new AnonymousClass2(this.$errorCallBack, this.$newGasPriceInGWEI, q70Var);
            }

            @Override // defpackage.hd1
            public final Object invoke(c90 c90Var, q70<? super te4> q70Var) {
                return ((AnonymousClass2) create(c90Var, q70Var)).invokeSuspend(te4.a);
            }

            @Override // kotlin.coroutines.jvm.internal.BaseContinuationImpl
            public final Object invokeSuspend(Object obj) {
                gs1.d();
                if (this.label == 0) {
                    o83.b(obj);
                    this.$errorCallBack.invoke(this.$newGasPriceInGWEI);
                    return te4.a;
                }
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
        }

        /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
        /* JADX WARN: Multi-variable type inference failed */
        public AnonymousClass1(TransferViewModel transferViewModel, IToken iToken, tc1<? super Double, te4> tc1Var, q70<? super AnonymousClass1> q70Var) {
            super(2, q70Var);
            this.this$0 = transferViewModel;
            this.$iToken = iToken;
            this.$errorCallBack = tc1Var;
        }

        @Override // kotlin.coroutines.jvm.internal.BaseContinuationImpl
        public final q70<te4> create(Object obj, q70<?> q70Var) {
            return new AnonymousClass1(this.this$0, this.$iToken, this.$errorCallBack, q70Var);
        }

        @Override // defpackage.hd1
        public final Object invoke(c90 c90Var, q70<? super te4> q70Var) {
            return ((AnonymousClass1) create(c90Var, q70Var)).invokeSuspend(te4.a);
        }

        /* JADX WARN: Removed duplicated region for block: B:49:0x00c6 A[RETURN] */
        /* JADX WARN: Removed duplicated region for block: B:50:0x00c7  */
        /* JADX WARN: Removed duplicated region for block: B:53:0x00e2 A[RETURN] */
        /* JADX WARN: Removed duplicated region for block: B:54:0x00e3  */
        /* JADX WARN: Removed duplicated region for block: B:57:0x00ed  */
        /* JADX WARN: Removed duplicated region for block: B:58:0x00ee  */
        @Override // kotlin.coroutines.jvm.internal.BaseContinuationImpl
        /*
            Code decompiled incorrectly, please refer to instructions dump.
            To view partially-correct code enable 'Show inconsistent code' option in preferences
        */
        public final java.lang.Object invokeSuspend(java.lang.Object r14) {
            /*
                Method dump skipped, instructions count: 307
                To view this dump change 'Code comments level' option to 'DEBUG'
            */
            throw new UnsupportedOperationException("Method not decompiled: net.safemoon.androidwallet.viewmodels.TransferViewModel$sendTransfer$1.AnonymousClass1.invokeSuspend(java.lang.Object):java.lang.Object");
        }
    }

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    /* JADX WARN: Multi-variable type inference failed */
    public TransferViewModel$sendTransfer$1(TransferViewModel transferViewModel, IToken iToken, tc1<? super Double, te4> tc1Var, q70<? super TransferViewModel$sendTransfer$1> q70Var) {
        super(2, q70Var);
        this.this$0 = transferViewModel;
        this.$iToken = iToken;
        this.$errorCallBack = tc1Var;
    }

    @Override // kotlin.coroutines.jvm.internal.BaseContinuationImpl
    public final q70<te4> create(Object obj, q70<?> q70Var) {
        return new TransferViewModel$sendTransfer$1(this.this$0, this.$iToken, this.$errorCallBack, q70Var);
    }

    @Override // defpackage.hd1
    public final Object invoke(c90 c90Var, q70<? super te4> q70Var) {
        return ((TransferViewModel$sendTransfer$1) create(c90Var, q70Var)).invokeSuspend(te4.a);
    }

    @Override // kotlin.coroutines.jvm.internal.BaseContinuationImpl
    public final Object invokeSuspend(Object obj) {
        Object d = gs1.d();
        int i = this.label;
        if (i == 0) {
            o83.b(obj);
            CoroutineDispatcher b = tp0.b();
            AnonymousClass1 anonymousClass1 = new AnonymousClass1(this.this$0, this.$iToken, this.$errorCallBack, null);
            this.label = 1;
            if (kotlinx.coroutines.a.e(b, anonymousClass1, this) == d) {
                return d;
            }
        } else if (i != 1) {
            throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        } else {
            o83.b(obj);
        }
        return te4.a;
    }
}
