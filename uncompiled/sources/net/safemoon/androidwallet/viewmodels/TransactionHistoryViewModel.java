package net.safemoon.androidwallet.viewmodels;

import android.app.Application;
import androidx.paging.CachedPagingDataKt;
import androidx.paging.Pager;
import java.util.List;
import net.safemoon.androidwallet.common.TokenType;
import net.safemoon.androidwallet.model.common.LoadingState;
import net.safemoon.androidwallet.model.priceAlert.PriceAlertToken;
import net.safemoon.androidwallet.model.token.abstraction.IToken;
import net.safemoon.androidwallet.model.tokensInfo.CurrencyTokenInfo;
import net.safemoon.androidwallet.model.transaction.history.Result;
import net.safemoon.androidwallet.repository.FcmDataSource;
import net.safemoon.androidwallet.ui.displayModel.UserTokenItemDisplayModel;

/* compiled from: TransactionHistoryViewModel.kt */
/* loaded from: classes2.dex */
public final class TransactionHistoryViewModel extends gd {
    public final sy1 b;
    public final sy1 c;
    public final gb2<List<IToken>> d;
    public final gb2<LoadingState> e;
    public final gb2<CurrencyTokenInfo> f;
    public final gb2<List<PriceAlertToken>> g;

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public TransactionHistoryViewModel(Application application) {
        super(application);
        fs1.f(application, "application");
        this.b = zy1.a(TransactionHistoryViewModel$mainNetApiInterface$2.INSTANCE);
        this.c = zy1.a(new TransactionHistoryViewModel$fcmDataSource$2(application));
        this.d = new gb2<>(b20.g());
        this.e = new gb2<>(LoadingState.Normal);
        new gb2(null);
        this.f = new gb2<>(null);
        this.g = new gb2<>(b20.g());
    }

    public final String e() {
        return bo3.i(a(), "SAFEMOON_ADDRESS");
    }

    public final String f() {
        return bo3.i(a(), "DEFAULT_GATEWAY");
    }

    public final TokenType g() {
        TokenType.a aVar = TokenType.Companion;
        String f = f();
        fs1.e(f, "chain()");
        return aVar.c(f);
    }

    public final gb2<List<IToken>> h() {
        return this.d;
    }

    public final gb2<CurrencyTokenInfo> i() {
        return this.f;
    }

    public final void j(String str) {
        fs1.f(str, "tokenAddress");
        as.b(ej4.a(this), null, null, new TransactionHistoryViewModel$getCurrencyTokenInfo$1(str, this, null), 3, null);
    }

    public final FcmDataSource k() {
        return (FcmDataSource) this.c.getValue();
    }

    public final ac3 l() {
        return (ac3) this.b.getValue();
    }

    public final void m(String str, String str2) {
        as.b(ej4.a(this), tp0.b(), null, new TransactionHistoryViewModel$getNotificationPriceAlert$1(this, str, str2, null), 2, null);
    }

    public final gb2<List<PriceAlertToken>> n() {
        return this.g;
    }

    public final gb2<LoadingState> o() {
        return this.e;
    }

    /* JADX WARN: Can't wrap try/catch for region: R(13:1|(2:3|(11:5|6|7|(1:(1:(6:11|12|13|(3:19|(1:23)|(2:25|(2:27|28)(1:30)))|31|32)(2:34|35))(3:36|37|38))(5:42|43|(1:45)(1:50)|46|(1:48)(1:49))|39|(1:41)|12|13|(5:15|17|19|(2:21|23)|(0))|31|32))|52|6|7|(0)(0)|39|(0)|12|13|(0)|31|32) */
    /* JADX WARN: Removed duplicated region for block: B:10:0x0025  */
    /* JADX WARN: Removed duplicated region for block: B:19:0x004c  */
    /* JADX WARN: Removed duplicated region for block: B:30:0x008e A[RETURN] */
    /* JADX WARN: Removed duplicated region for block: B:34:0x0094  */
    /* JADX WARN: Removed duplicated region for block: B:46:0x00b7  */
    /*
        Code decompiled incorrectly, please refer to instructions dump.
        To view partially-correct code enable 'Show inconsistent code' option in preferences
    */
    public final java.lang.Object p(java.lang.String r9, java.lang.String r10, defpackage.q70<? super java.util.ArrayList<net.safemoon.androidwallet.model.priceAlert.PriceAlertToken>> r11) {
        /*
            r8 = this;
            boolean r0 = r11 instanceof net.safemoon.androidwallet.viewmodels.TransactionHistoryViewModel$loadPriceAlert$1
            if (r0 == 0) goto L13
            r0 = r11
            net.safemoon.androidwallet.viewmodels.TransactionHistoryViewModel$loadPriceAlert$1 r0 = (net.safemoon.androidwallet.viewmodels.TransactionHistoryViewModel$loadPriceAlert$1) r0
            int r1 = r0.label
            r2 = -2147483648(0xffffffff80000000, float:-0.0)
            r3 = r1 & r2
            if (r3 == 0) goto L13
            int r1 = r1 - r2
            r0.label = r1
            goto L18
        L13:
            net.safemoon.androidwallet.viewmodels.TransactionHistoryViewModel$loadPriceAlert$1 r0 = new net.safemoon.androidwallet.viewmodels.TransactionHistoryViewModel$loadPriceAlert$1
            r0.<init>(r8, r11)
        L18:
            java.lang.Object r11 = r0.result
            java.lang.Object r1 = defpackage.gs1.d()
            int r2 = r0.label
            r3 = 2
            r4 = 1
            r5 = 0
            if (r2 == 0) goto L4c
            if (r2 == r4) goto L35
            if (r2 != r3) goto L2d
            defpackage.o83.b(r11)     // Catch: java.lang.Exception -> L92
            goto L8f
        L2d:
            java.lang.IllegalStateException r9 = new java.lang.IllegalStateException
            java.lang.String r10 = "call to 'resume' before 'invoke' with coroutine"
            r9.<init>(r10)
            throw r9
        L35:
            java.lang.Object r9 = r0.L$3
            java.lang.String r9 = (java.lang.String) r9
            java.lang.Object r10 = r0.L$2
            java.lang.String r10 = (java.lang.String) r10
            java.lang.Object r2 = r0.L$1
            java.lang.String r2 = (java.lang.String) r2
            java.lang.Object r4 = r0.L$0
            ac3 r4 = (defpackage.ac3) r4
            defpackage.o83.b(r11)     // Catch: java.lang.Exception -> L92
            r7 = r10
            r10 = r9
            r9 = r7
            goto L73
        L4c:
            defpackage.o83.b(r11)
            java.lang.String r2 = r8.e()
            ac3 r11 = r8.l()     // Catch: java.lang.Exception -> L92
            if (r9 != 0) goto L5a
            goto L5b
        L5a:
            r10 = r5
        L5b:
            net.safemoon.androidwallet.repository.FcmDataSource r6 = r8.k()     // Catch: java.lang.Exception -> L92
            r0.L$0 = r11     // Catch: java.lang.Exception -> L92
            r0.L$1 = r2     // Catch: java.lang.Exception -> L92
            r0.L$2 = r9     // Catch: java.lang.Exception -> L92
            r0.L$3 = r10     // Catch: java.lang.Exception -> L92
            r0.label = r4     // Catch: java.lang.Exception -> L92
            java.lang.Object r4 = r6.d(r0)     // Catch: java.lang.Exception -> L92
            if (r4 != r1) goto L70
            return r1
        L70:
            r7 = r4
            r4 = r11
            r11 = r7
        L73:
            java.lang.String r11 = (java.lang.String) r11     // Catch: java.lang.Exception -> L92
            retrofit2.b r9 = r4.f(r2, r9, r10, r11)     // Catch: java.lang.Exception -> L92
            java.lang.String r10 = "mainNetApiInterface.getP…DataSource.getFCMToken())"
            defpackage.fs1.e(r9, r10)     // Catch: java.lang.Exception -> L92
            r0.L$0 = r5     // Catch: java.lang.Exception -> L92
            r0.L$1 = r5     // Catch: java.lang.Exception -> L92
            r0.L$2 = r5     // Catch: java.lang.Exception -> L92
            r0.L$3 = r5     // Catch: java.lang.Exception -> L92
            r0.label = r3     // Catch: java.lang.Exception -> L92
            java.lang.Object r11 = retrofit2.KotlinExtensions.c(r9, r0)     // Catch: java.lang.Exception -> L92
            if (r11 != r1) goto L8f
            return r1
        L8f:
            retrofit2.n r11 = (retrofit2.n) r11     // Catch: java.lang.Exception -> L92
            r5 = r11
        L92:
            if (r5 == 0) goto Lcc
            boolean r9 = r5.e()
            if (r9 == 0) goto Lcc
            java.lang.Object r9 = r5.a()
            if (r9 == 0) goto Lcc
            java.lang.Object r9 = r5.a()
            net.safemoon.androidwallet.model.priceAlert.PriceAlertTokenListData r9 = (net.safemoon.androidwallet.model.priceAlert.PriceAlertTokenListData) r9
            r10 = 0
            if (r9 != 0) goto Laa
            goto Lb5
        Laa:
            java.util.ArrayList r9 = r9.getResult()
            if (r9 != 0) goto Lb1
            goto Lb5
        Lb1:
            int r10 = r9.size()
        Lb5:
            if (r10 <= 0) goto Lcc
            java.lang.Object r9 = r5.a()
            defpackage.fs1.d(r9)
            net.safemoon.androidwallet.model.priceAlert.PriceAlertTokenListData r9 = (net.safemoon.androidwallet.model.priceAlert.PriceAlertTokenListData) r9
            java.util.ArrayList r9 = r9.getResult()
            if (r9 != 0) goto Ld1
            java.util.ArrayList r9 = new java.util.ArrayList
            r9.<init>()
            goto Ld1
        Lcc:
            java.util.ArrayList r9 = new java.util.ArrayList
            r9.<init>()
        Ld1:
            return r9
        */
        throw new UnsupportedOperationException("Method not decompiled: net.safemoon.androidwallet.viewmodels.TransactionHistoryViewModel.p(java.lang.String, java.lang.String, q70):java.lang.Object");
    }

    public final j71<fp2<Result>> q(UserTokenItemDisplayModel userTokenItemDisplayModel) {
        fs1.f(userTokenItemDisplayModel, "userTokenItemDisplayModel");
        as.b(qg1.a, null, null, new TransactionHistoryViewModel$transactionListFlow$1(this, null), 3, null);
        return CachedPagingDataKt.a(new Pager(new ep2(100, 2, false, 100, 0, 0, 48, null), null, new TransactionHistoryViewModel$transactionListFlow$2(this, userTokenItemDisplayModel), 2, null).a(), ej4.a(this));
    }
}
