package net.safemoon.androidwallet.viewmodels;

import java.math.BigInteger;
import kotlin.coroutines.jvm.internal.SuspendLambda;
import kotlin.coroutines.jvm.internal.a;
import kotlinx.coroutines.CoroutineDispatcher;
import net.safemoon.androidwallet.model.nft.NFTTransferResponse;
import net.safemoon.androidwallet.viewmodels.blockChainClass.NFT721;

/* compiled from: CollectibleViewModel.kt */
@a(c = "net.safemoon.androidwallet.viewmodels.CollectibleViewModel$transfer711From$1", f = "CollectibleViewModel.kt", l = {95}, m = "invokeSuspend")
/* loaded from: classes2.dex */
public final class CollectibleViewModel$transfer711From$1 extends SuspendLambda implements hd1<c90, q70<? super te4>, Object> {
    public final /* synthetic */ gb2<NFTTransferResponse> $callBack;
    public final /* synthetic */ rc1<te4> $errorCallBack;
    public final /* synthetic */ NFT721 $nft721;
    public final /* synthetic */ String $toAddress;
    public final /* synthetic */ BigInteger $tokenId;
    public int label;
    public final /* synthetic */ CollectibleViewModel this$0;

    /* compiled from: CollectibleViewModel.kt */
    @a(c = "net.safemoon.androidwallet.viewmodels.CollectibleViewModel$transfer711From$1$1", f = "CollectibleViewModel.kt", l = {97, 99}, m = "invokeSuspend")
    /* renamed from: net.safemoon.androidwallet.viewmodels.CollectibleViewModel$transfer711From$1$1  reason: invalid class name */
    /* loaded from: classes2.dex */
    public static final class AnonymousClass1 extends SuspendLambda implements hd1<c90, q70<? super te4>, Object> {
        public final /* synthetic */ gb2<NFTTransferResponse> $callBack;
        public final /* synthetic */ rc1<te4> $errorCallBack;
        public final /* synthetic */ NFT721 $nft721;
        public final /* synthetic */ String $toAddress;
        public final /* synthetic */ BigInteger $tokenId;
        public int label;
        public final /* synthetic */ CollectibleViewModel this$0;

        /* compiled from: CollectibleViewModel.kt */
        @a(c = "net.safemoon.androidwallet.viewmodels.CollectibleViewModel$transfer711From$1$1$1", f = "CollectibleViewModel.kt", l = {}, m = "invokeSuspend")
        /* renamed from: net.safemoon.androidwallet.viewmodels.CollectibleViewModel$transfer711From$1$1$1  reason: invalid class name and collision with other inner class name */
        /* loaded from: classes2.dex */
        public static final class C02191 extends SuspendLambda implements hd1<c90, q70<? super te4>, Object> {
            public final /* synthetic */ rc1<te4> $errorCallBack;
            public int label;

            /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
            public C02191(rc1<te4> rc1Var, q70<? super C02191> q70Var) {
                super(2, q70Var);
                this.$errorCallBack = rc1Var;
            }

            @Override // kotlin.coroutines.jvm.internal.BaseContinuationImpl
            public final q70<te4> create(Object obj, q70<?> q70Var) {
                return new C02191(this.$errorCallBack, q70Var);
            }

            @Override // defpackage.hd1
            public final Object invoke(c90 c90Var, q70<? super te4> q70Var) {
                return ((C02191) create(c90Var, q70Var)).invokeSuspend(te4.a);
            }

            @Override // kotlin.coroutines.jvm.internal.BaseContinuationImpl
            public final Object invokeSuspend(Object obj) {
                gs1.d();
                if (this.label == 0) {
                    o83.b(obj);
                    this.$errorCallBack.invoke();
                    return te4.a;
                }
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
        }

        /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
        public AnonymousClass1(NFT721 nft721, BigInteger bigInteger, String str, gb2<NFTTransferResponse> gb2Var, CollectibleViewModel collectibleViewModel, rc1<te4> rc1Var, q70<? super AnonymousClass1> q70Var) {
            super(2, q70Var);
            this.$nft721 = nft721;
            this.$tokenId = bigInteger;
            this.$toAddress = str;
            this.$callBack = gb2Var;
            this.this$0 = collectibleViewModel;
            this.$errorCallBack = rc1Var;
        }

        @Override // kotlin.coroutines.jvm.internal.BaseContinuationImpl
        public final q70<te4> create(Object obj, q70<?> q70Var) {
            return new AnonymousClass1(this.$nft721, this.$tokenId, this.$toAddress, this.$callBack, this.this$0, this.$errorCallBack, q70Var);
        }

        @Override // defpackage.hd1
        public final Object invoke(c90 c90Var, q70<? super te4> q70Var) {
            return ((AnonymousClass1) create(c90Var, q70Var)).invokeSuspend(te4.a);
        }

        /* JADX WARN: Removed duplicated region for block: B:27:0x0054 A[Catch: all -> 0x001f, Exception -> 0x0022, TryCatch #0 {Exception -> 0x0022, blocks: (B:6:0x000f, B:25:0x0050, B:27:0x0054, B:29:0x005a, B:30:0x006b, B:31:0x007e, B:10:0x001b, B:20:0x0035, B:22:0x003d, B:32:0x009d, B:17:0x0028), top: B:44:0x0009, outer: #1 }] */
        /* JADX WARN: Removed duplicated region for block: B:31:0x007e A[Catch: all -> 0x001f, Exception -> 0x0022, TryCatch #0 {Exception -> 0x0022, blocks: (B:6:0x000f, B:25:0x0050, B:27:0x0054, B:29:0x005a, B:30:0x006b, B:31:0x007e, B:10:0x001b, B:20:0x0035, B:22:0x003d, B:32:0x009d, B:17:0x0028), top: B:44:0x0009, outer: #1 }] */
        @Override // kotlin.coroutines.jvm.internal.BaseContinuationImpl
        /*
            Code decompiled incorrectly, please refer to instructions dump.
            To view partially-correct code enable 'Show inconsistent code' option in preferences
        */
        public final java.lang.Object invokeSuspend(java.lang.Object r13) {
            /*
                Method dump skipped, instructions count: 235
                To view this dump change 'Code comments level' option to 'DEBUG'
            */
            throw new UnsupportedOperationException("Method not decompiled: net.safemoon.androidwallet.viewmodels.CollectibleViewModel$transfer711From$1.AnonymousClass1.invokeSuspend(java.lang.Object):java.lang.Object");
        }
    }

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public CollectibleViewModel$transfer711From$1(NFT721 nft721, BigInteger bigInteger, String str, gb2<NFTTransferResponse> gb2Var, CollectibleViewModel collectibleViewModel, rc1<te4> rc1Var, q70<? super CollectibleViewModel$transfer711From$1> q70Var) {
        super(2, q70Var);
        this.$nft721 = nft721;
        this.$tokenId = bigInteger;
        this.$toAddress = str;
        this.$callBack = gb2Var;
        this.this$0 = collectibleViewModel;
        this.$errorCallBack = rc1Var;
    }

    @Override // kotlin.coroutines.jvm.internal.BaseContinuationImpl
    public final q70<te4> create(Object obj, q70<?> q70Var) {
        return new CollectibleViewModel$transfer711From$1(this.$nft721, this.$tokenId, this.$toAddress, this.$callBack, this.this$0, this.$errorCallBack, q70Var);
    }

    @Override // defpackage.hd1
    public final Object invoke(c90 c90Var, q70<? super te4> q70Var) {
        return ((CollectibleViewModel$transfer711From$1) create(c90Var, q70Var)).invokeSuspend(te4.a);
    }

    @Override // kotlin.coroutines.jvm.internal.BaseContinuationImpl
    public final Object invokeSuspend(Object obj) {
        Object d = gs1.d();
        int i = this.label;
        if (i == 0) {
            o83.b(obj);
            CoroutineDispatcher b = tp0.b();
            AnonymousClass1 anonymousClass1 = new AnonymousClass1(this.$nft721, this.$tokenId, this.$toAddress, this.$callBack, this.this$0, this.$errorCallBack, null);
            this.label = 1;
            if (kotlinx.coroutines.a.e(b, anonymousClass1, this) == d) {
                return d;
            }
        } else if (i != 1) {
            throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        } else {
            o83.b(obj);
        }
        return te4.a;
    }
}
