package net.safemoon.androidwallet.viewmodels;

import android.app.Application;
import android.os.Handler;
import android.os.Looper;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import kotlin.jvm.internal.Ref$ObjectRef;
import kotlin.text.StringsKt__StringsKt;
import net.safemoon.androidwallet.R;
import net.safemoon.androidwallet.common.TokenType;
import net.safemoon.androidwallet.model.swap.AllSwapTokens;
import net.safemoon.androidwallet.model.swap.Data;
import net.safemoon.androidwallet.model.swap.Swap;
import net.safemoon.androidwallet.viewmodels.CalculatorViewModel;
import retrofit2.n;

/* compiled from: CalculatorViewModel.kt */
/* loaded from: classes2.dex */
public final class CalculatorViewModel extends gd {
    public final gb2<BigDecimal> b;
    public final h11<List<a>> c;
    public final gb2<String> d;
    public final gb2<String> e;
    public Double f;
    public BigDecimal g;
    public final gb2<Integer> h;
    public final ArrayList<Swap> i;

    /* compiled from: CalculatorViewModel.kt */
    /* loaded from: classes2.dex */
    public enum Operation {
        ADD(" + "),
        SUBTRACT(" - "),
        MULTIPLY(" x "),
        DIVIDE(" / "),
        PERCENTAGE(" % "),
        EQUAL_TO(" = ");
        
        private final String equation;

        Operation(String str) {
            this.equation = str;
        }

        public final String getEquation() {
            return this.equation;
        }
    }

    /* compiled from: CalculatorViewModel.kt */
    /* loaded from: classes2.dex */
    public enum Type {
        Operation,
        Digit
    }

    /* compiled from: CalculatorViewModel.kt */
    /* loaded from: classes2.dex */
    public static final class a {
        public final Type a;
        public final Operation b;
        public final BigDecimal c;

        public a(Type type, Operation operation, BigDecimal bigDecimal) {
            fs1.f(type, "type");
            this.a = type;
            this.b = operation;
            this.c = bigDecimal;
        }

        public final Type a() {
            return this.a;
        }

        public final Operation b() {
            return this.b;
        }

        public final BigDecimal c() {
            return this.c;
        }

        public final BigDecimal d() {
            return this.c;
        }

        public final Operation e() {
            return this.b;
        }

        public boolean equals(Object obj) {
            if (this == obj) {
                return true;
            }
            if (obj instanceof a) {
                a aVar = (a) obj;
                return this.a == aVar.a && this.b == aVar.b && fs1.b(this.c, aVar.c);
            }
            return false;
        }

        public final Type f() {
            return this.a;
        }

        public int hashCode() {
            int hashCode = this.a.hashCode() * 31;
            Operation operation = this.b;
            int hashCode2 = (hashCode + (operation == null ? 0 : operation.hashCode())) * 31;
            BigDecimal bigDecimal = this.c;
            return hashCode2 + (bigDecimal != null ? bigDecimal.hashCode() : 0);
        }

        public String toString() {
            return "Calc(type=" + this.a + ", operation=" + this.b + ", digit=" + this.c + ')';
        }
    }

    /* compiled from: CalculatorViewModel.kt */
    /* loaded from: classes2.dex */
    public /* synthetic */ class b {
        public static final /* synthetic */ int[] a;

        static {
            int[] iArr = new int[Operation.values().length];
            iArr[Operation.ADD.ordinal()] = 1;
            iArr[Operation.SUBTRACT.ordinal()] = 2;
            iArr[Operation.MULTIPLY.ordinal()] = 3;
            iArr[Operation.DIVIDE.ordinal()] = 4;
            iArr[Operation.PERCENTAGE.ordinal()] = 5;
            a = iArr;
        }
    }

    /* compiled from: CalculatorViewModel.kt */
    /* loaded from: classes2.dex */
    public static final class c implements wu<AllSwapTokens> {
        public c() {
        }

        @Override // defpackage.wu
        public void a(retrofit2.b<AllSwapTokens> bVar, Throwable th) {
            fs1.f(bVar, "call");
            fs1.f(th, "t");
        }

        @Override // defpackage.wu
        public void b(retrofit2.b<AllSwapTokens> bVar, n<AllSwapTokens> nVar) {
            Data data;
            fs1.f(bVar, "call");
            fs1.f(nVar, "response");
            if (!nVar.e() || nVar.a() == null) {
                return;
            }
            CalculatorViewModel.this.u().clear();
            ArrayList<Swap> u = CalculatorViewModel.this.u();
            AllSwapTokens a = nVar.a();
            List<Swap> list = null;
            if (a != null && (data = a.getData()) != null) {
                list = data.getTokens();
            }
            fs1.d(list);
            u.addAll(list);
            CalculatorViewModel.this.u().addAll(b30.a.l());
        }
    }

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public CalculatorViewModel(Application application) {
        super(application);
        fs1.f(application, "application");
        v();
        this.b = new gb2<>();
        h11<List<a>> h11Var = new h11<>();
        h11Var.setValue(new ArrayList());
        te4 te4Var = te4.a;
        this.c = h11Var;
        this.d = new gb2<>();
        this.e = new gb2<>("");
        BigDecimal bigDecimal = BigDecimal.ZERO;
        fs1.e(bigDecimal, "ZERO");
        this.g = bigDecimal;
        this.h = new gb2<>(Integer.valueOf((int) R.id.btnBag));
        this.i = new ArrayList<>();
    }

    public static final void f(CalculatorViewModel calculatorViewModel) {
        fs1.f(calculatorViewModel, "this$0");
        calculatorViewModel.n().c(j20.m0(b20.g()));
    }

    public final void A(Double d) {
        this.f = d;
    }

    public final void B(int i) {
        gb2<String> gb2Var = this.e;
        gb2Var.setValue(fs1.l(gb2Var.getValue(), Integer.valueOf(i)));
    }

    public final void C() {
        String value = this.e.getValue();
        fs1.d(value);
        String str = value;
        fs1.e(str, "");
        if (str.length() > 0) {
            b30 b30Var = b30.a;
            if (!StringsKt__StringsKt.L(str, b30Var.y(), false, 2, null)) {
                m().setValue(fs1.l(m().getValue(), Character.valueOf(b30Var.y())));
                return;
            }
        }
        if (str.length() == 0) {
            m().setValue(fs1.l("0", Character.valueOf(b30.a.y())));
        }
    }

    /* JADX WARN: Code restructure failed: missing block: B:9:0x001c, code lost:
        if ((r0.length() == 0) == true) goto L4;
     */
    /*
        Code decompiled incorrectly, please refer to instructions dump.
        To view partially-correct code enable 'Show inconsistent code' option in preferences
    */
    public final void c(java.math.BigDecimal r9) {
        /*
            r8 = this;
            java.lang.String r0 = "n"
            defpackage.fs1.f(r9, r0)
            gb2<java.lang.String> r0 = r8.e
            java.lang.Object r0 = r0.getValue()
            java.lang.String r0 = (java.lang.String) r0
            r1 = 1
            r2 = 0
            if (r0 != 0) goto L13
        L11:
            r1 = r2
            goto L1e
        L13:
            int r0 = r0.length()
            if (r0 != 0) goto L1b
            r0 = r1
            goto L1c
        L1b:
            r0 = r2
        L1c:
            if (r0 != r1) goto L11
        L1e:
            if (r1 == 0) goto L33
            gb2<java.lang.String> r0 = r8.e
            double r1 = r9.doubleValue()
            r3 = 8
            r4 = 0
            r5 = 0
            r6 = 6
            r7 = 0
            java.lang.String r9 = defpackage.e30.p(r1, r3, r4, r5, r6, r7)
            r0.setValue(r9)
        L33:
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: net.safemoon.androidwallet.viewmodels.CalculatorViewModel.c(java.math.BigDecimal):void");
    }

    public final void d(BigDecimal bigDecimal) {
        fs1.f(bigDecimal, "n");
        this.e.setValue(e30.p(bigDecimal.doubleValue(), 8, null, false, 6, null));
    }

    public final void e() {
        String h0;
        List<a> value;
        try {
            String value2 = this.e.getValue();
            BigDecimal L = value2 == null ? null : e30.L(value2);
            if (L != null && (value = this.c.getValue()) != null) {
                value.add(new a(Type.Digit, null, L));
            }
            BigDecimal h = h();
            if (h != null && (h0 = e30.h0(h, 0, 1, null)) != null) {
                m().postValue(h0);
            }
            List<a> value3 = this.c.getValue();
            if (value3 != null) {
                value3.add(new a(Type.Operation, Operation.EQUAL_TO, null));
            }
            new Handler(Looper.getMainLooper()).post(new Runnable() { // from class: ru
                @Override // java.lang.Runnable
                public final void run() {
                    CalculatorViewModel.f(CalculatorViewModel.this);
                }
            });
        } catch (Exception unused) {
        }
    }

    public final void g() {
        String value = this.e.getValue();
        BigDecimal L = value == null ? null : e30.L(value);
        if (L != null) {
            gb2<String> gb2Var = this.e;
            BigDecimal divide = L.divide(new BigDecimal(100));
            fs1.e(divide, "inputValue.divide(BigDecimal(100))");
            gb2Var.postValue(e30.h0(divide, 0, 1, null));
        }
    }

    public final BigDecimal h() {
        ArrayList arrayList;
        BigDecimal bigDecimal;
        BigDecimal add;
        BigDecimal multiply;
        BigDecimal d;
        BigDecimal d2;
        int i;
        String value = this.e.getValue();
        BigDecimal bigDecimal2 = null;
        BigDecimal L = value == null ? null : e30.L(value);
        List<a> value2 = this.c.getValue();
        if (value2 == null) {
            arrayList = null;
        } else {
            arrayList = new ArrayList();
            arrayList.addAll(value2);
        }
        if (arrayList != null && arrayList.size() >= 2) {
            if (L != null) {
                arrayList.add(new a(Type.Digit, null, L));
            }
            Iterator it = arrayList.iterator();
            fs1.e(it, "it.iterator()");
            loop0: while (true) {
                bigDecimal = null;
                while (it.hasNext()) {
                    if (bigDecimal == null) {
                        try {
                            d = ((a) it.next()).d();
                            fs1.d(d);
                            Operation e = ((a) it.next()).e();
                            fs1.d(e);
                            d2 = ((a) it.next()).d();
                            fs1.d(d2);
                            i = b.a[e.ordinal()];
                        } catch (Exception e2) {
                            e2.printStackTrace();
                        }
                        if (i == 1) {
                            add = d.add(d2);
                            fs1.e(add, "this.add(other)");
                        } else if (i == 2) {
                            add = d.subtract(d2);
                            fs1.e(add, "this.subtract(other)");
                        } else if (i == 3) {
                            multiply = d.multiply(d2);
                            fs1.e(multiply, "this.multiply(other)");
                            bigDecimal = multiply;
                        } else if (i == 4) {
                            add = d.divide(d2, 8, RoundingMode.HALF_UP);
                        } else if (i != 5) {
                            break;
                        } else {
                            add = d.remainder(d2);
                        }
                        bigDecimal = add;
                    } else {
                        Operation e3 = ((a) it.next()).e();
                        fs1.d(e3);
                        BigDecimal d3 = ((a) it.next()).d();
                        fs1.d(d3);
                        int i2 = b.a[e3.ordinal()];
                        if (i2 == 1) {
                            add = bigDecimal.add(d3);
                            fs1.e(add, "this.add(other)");
                        } else if (i2 == 2) {
                            add = bigDecimal.subtract(d3);
                            fs1.e(add, "this.subtract(other)");
                        } else if (i2 == 3) {
                            multiply = bigDecimal.multiply(d3);
                            fs1.e(multiply, "this.multiply(other)");
                            bigDecimal = multiply;
                        } else if (i2 == 4) {
                            add = bigDecimal.divide(d3, 8, RoundingMode.HALF_UP);
                        } else if (i2 != 5) {
                            break;
                        } else {
                            add = bigDecimal.remainder(d3);
                        }
                        bigDecimal = add;
                    }
                }
            }
            bigDecimal2 = bigDecimal;
        }
        this.b.postValue(bigDecimal2);
        return bigDecimal2;
    }

    public final void i() {
        this.e.setValue("");
        List<a> value = this.c.getValue();
        fs1.d(value);
        value.clear();
        this.b.postValue(null);
        x();
    }

    /* JADX WARN: Removed duplicated region for block: B:12:0x001c A[Catch: NumberFormatException -> 0x00b6, TryCatch #0 {NumberFormatException -> 0x00b6, blocks: (B:2:0x0000, B:12:0x001c, B:17:0x002d, B:35:0x009c, B:20:0x003b, B:23:0x0044, B:25:0x004c, B:27:0x0059, B:29:0x0063, B:30:0x006a, B:31:0x006b, B:33:0x0080, B:34:0x008c, B:15:0x0027, B:36:0x009f, B:5:0x000e), top: B:39:0x0000 }] */
    /*
        Code decompiled incorrectly, please refer to instructions dump.
        To view partially-correct code enable 'Show inconsistent code' option in preferences
    */
    public final void j() {
        /*
            r5 = this;
            gb2<java.lang.String> r0 = r5.e     // Catch: java.lang.NumberFormatException -> Lb6
            java.lang.Object r0 = r0.getValue()     // Catch: java.lang.NumberFormatException -> Lb6
            java.lang.String r0 = (java.lang.String) r0     // Catch: java.lang.NumberFormatException -> Lb6
            r1 = 1
            r2 = 0
            if (r0 != 0) goto Le
        Lc:
            r0 = r2
            goto L1a
        Le:
            int r0 = r0.length()     // Catch: java.lang.NumberFormatException -> Lb6
            if (r0 != 0) goto L16
            r0 = r1
            goto L17
        L16:
            r0 = r2
        L17:
            if (r0 != r1) goto Lc
            r0 = r1
        L1a:
            if (r0 == 0) goto L9f
            h11<java.util.List<net.safemoon.androidwallet.viewmodels.CalculatorViewModel$a>> r0 = r5.c     // Catch: java.lang.NumberFormatException -> Lb6
            java.lang.Object r0 = r0.getValue()     // Catch: java.lang.NumberFormatException -> Lb6
            java.util.List r0 = (java.util.List) r0     // Catch: java.lang.NumberFormatException -> Lb6
            if (r0 != 0) goto L27
            goto L2b
        L27:
            int r2 = r0.size()     // Catch: java.lang.NumberFormatException -> Lb6
        L2b:
            if (r2 <= 0) goto L9f
            gb2<java.lang.String> r0 = r5.e     // Catch: java.lang.NumberFormatException -> Lb6
            h11<java.util.List<net.safemoon.androidwallet.viewmodels.CalculatorViewModel$a>> r2 = r5.c     // Catch: java.lang.NumberFormatException -> Lb6
            java.lang.Object r2 = r2.getValue()     // Catch: java.lang.NumberFormatException -> Lb6
            java.util.List r2 = (java.util.List) r2     // Catch: java.lang.NumberFormatException -> Lb6
            r3 = 0
            if (r2 != 0) goto L3b
            goto L9c
        L3b:
            java.lang.Object r2 = defpackage.g20.A(r2)     // Catch: java.lang.NumberFormatException -> Lb6
            net.safemoon.androidwallet.viewmodels.CalculatorViewModel$a r2 = (net.safemoon.androidwallet.viewmodels.CalculatorViewModel.a) r2     // Catch: java.lang.NumberFormatException -> Lb6
            if (r2 != 0) goto L44
            goto L9c
        L44:
            net.safemoon.androidwallet.viewmodels.CalculatorViewModel$Type r3 = r2.f()     // Catch: java.lang.NumberFormatException -> Lb6
            net.safemoon.androidwallet.viewmodels.CalculatorViewModel$Type r4 = net.safemoon.androidwallet.viewmodels.CalculatorViewModel.Type.Operation     // Catch: java.lang.NumberFormatException -> Lb6
            if (r3 != r4) goto L6b
            net.safemoon.androidwallet.viewmodels.CalculatorViewModel$Operation r2 = r2.e()     // Catch: java.lang.NumberFormatException -> Lb6
            defpackage.fs1.d(r2)     // Catch: java.lang.NumberFormatException -> Lb6
            java.lang.String r2 = r2.getEquation()     // Catch: java.lang.NumberFormatException -> Lb6
            if (r2 == 0) goto L63
            java.lang.CharSequence r2 = kotlin.text.StringsKt__StringsKt.K0(r2)     // Catch: java.lang.NumberFormatException -> Lb6
            java.lang.String r2 = r2.toString()     // Catch: java.lang.NumberFormatException -> Lb6
        L61:
            r3 = r2
            goto L9c
        L63:
            java.lang.NullPointerException r0 = new java.lang.NullPointerException     // Catch: java.lang.NumberFormatException -> Lb6
            java.lang.String r1 = "null cannot be cast to non-null type kotlin.CharSequence"
            r0.<init>(r1)     // Catch: java.lang.NumberFormatException -> Lb6
            throw r0     // Catch: java.lang.NumberFormatException -> Lb6
        L6b:
            b30 r3 = defpackage.b30.a     // Catch: java.lang.NumberFormatException -> Lb6
            java.math.BigDecimal r4 = r2.d()     // Catch: java.lang.NumberFormatException -> Lb6
            defpackage.fs1.d(r4)     // Catch: java.lang.NumberFormatException -> Lb6
            java.math.BigDecimal r3 = r3.k(r4)     // Catch: java.lang.NumberFormatException -> Lb6
            java.math.BigDecimal r4 = java.math.BigDecimal.ZERO     // Catch: java.lang.NumberFormatException -> Lb6
            int r3 = r3.compareTo(r4)     // Catch: java.lang.NumberFormatException -> Lb6
            if (r3 <= 0) goto L8c
            java.math.BigDecimal r2 = r2.d()     // Catch: java.lang.NumberFormatException -> Lb6
            defpackage.fs1.d(r2)     // Catch: java.lang.NumberFormatException -> Lb6
            java.lang.String r2 = r2.toPlainString()     // Catch: java.lang.NumberFormatException -> Lb6
            goto L61
        L8c:
            java.math.BigDecimal r2 = r2.d()     // Catch: java.lang.NumberFormatException -> Lb6
            defpackage.fs1.d(r2)     // Catch: java.lang.NumberFormatException -> Lb6
            long r2 = r2.longValueExact()     // Catch: java.lang.NumberFormatException -> Lb6
            java.lang.String r2 = java.lang.String.valueOf(r2)     // Catch: java.lang.NumberFormatException -> Lb6
            goto L61
        L9c:
            r0.setValue(r3)     // Catch: java.lang.NumberFormatException -> Lb6
        L9f:
            gb2<java.lang.String> r0 = r5.e     // Catch: java.lang.NumberFormatException -> Lb6
            java.lang.Object r2 = r0.getValue()     // Catch: java.lang.NumberFormatException -> Lb6
            defpackage.fs1.d(r2)     // Catch: java.lang.NumberFormatException -> Lb6
            java.lang.String r3 = "e2Text.value!!"
            defpackage.fs1.e(r2, r3)     // Catch: java.lang.NumberFormatException -> Lb6
            java.lang.String r2 = (java.lang.String) r2     // Catch: java.lang.NumberFormatException -> Lb6
            java.lang.String r1 = kotlin.text.StringsKt___StringsKt.O0(r2, r1)     // Catch: java.lang.NumberFormatException -> Lb6
            r0.setValue(r1)     // Catch: java.lang.NumberFormatException -> Lb6
        Lb6:
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: net.safemoon.androidwallet.viewmodels.CalculatorViewModel.j():void");
    }

    public final void k(q9 q9Var) {
        fs1.f(q9Var, "model");
        as.b(ej4.a(this), null, null, new CalculatorViewModel$fetchTokenBalance$1$1(this, q9Var, null), 3, null);
    }

    /* JADX WARN: Type inference failed for: r0v10, types: [T, java.lang.String] */
    /* JADX WARN: Type inference failed for: r0v2, types: [T, java.lang.String] */
    public final void l(q9 q9Var, rc1<te4> rc1Var) {
        fs1.f(q9Var, "model");
        fs1.f(rc1Var, "callBack");
        Ref$ObjectRef ref$ObjectRef = new Ref$ObjectRef();
        ?? a2 = q9Var.a();
        ref$ObjectRef.element = a2;
        if (((CharSequence) a2).length() == 0) {
            ref$ObjectRef.element = p().getWrapAddress();
        }
        as.b(ej4.a(this), null, null, new CalculatorViewModel$fetchTokenPrice$1(this, ref$ObjectRef, kt.b(q9Var.a(), q9Var.g(), cv3.l(q9Var.d())), rc1Var, null), 3, null);
    }

    public final gb2<String> m() {
        return this.e;
    }

    public final h11<List<a>> n() {
        return this.c;
    }

    public final gb2<BigDecimal> o() {
        return this.b;
    }

    public final TokenType p() {
        TokenType.a aVar = TokenType.Companion;
        String j = bo3.j(a(), "DEFAULT_GATEWAY", TokenType.BEP_20.getName());
        fs1.e(j, "getString(\n             …0.getName()\n            )");
        return aVar.c(j);
    }

    public final BigDecimal q() {
        return this.g;
    }

    public final Double r() {
        return this.f;
    }

    public final gb2<Integer> s() {
        return this.h;
    }

    public final gb2<String> t() {
        return this.d;
    }

    public final ArrayList<Swap> u() {
        return this.i;
    }

    public final void v() {
        a4.k().m().n(new c());
    }

    public final void w(BigDecimal bigDecimal) {
        fs1.f(bigDecimal, "n");
        try {
            String value = this.e.getValue();
            fs1.d(value);
            fs1.e(value, "e2Text.value!!");
            if (value.length() > 0) {
                String value2 = this.e.getValue();
                fs1.d(value2);
                fs1.e(value2, "e2Text.value!!");
                BigDecimal bigDecimal2 = new BigDecimal(String.valueOf(e30.K(value2)));
                gb2<String> gb2Var = this.e;
                BigDecimal multiply = bigDecimal2.multiply(bigDecimal);
                fs1.e(multiply, "this.multiply(other)");
                gb2Var.setValue(e30.h0(multiply, 0, 1, null));
            } else {
                this.e.setValue(e30.h0(bigDecimal, 0, 1, null));
            }
        } catch (NumberFormatException unused) {
        }
    }

    public final void x() {
        List<a> value = this.c.getValue();
        if (value == null) {
            return;
        }
        n().setValue(value);
    }

    public final void y(Operation operation) {
        List<a> value;
        fs1.f(operation, "s");
        try {
            String value2 = this.e.getValue();
            BigDecimal L = value2 == null ? null : e30.L(value2);
            if (L == null) {
                List<a> value3 = this.c.getValue();
                a aVar = value3 == null ? null : (a) j20.V(value3);
                if (aVar != null) {
                    Type f = aVar.f();
                    Type type = Type.Operation;
                    if (f == type) {
                        List<a> value4 = this.c.getValue();
                        if (value4 != null) {
                            a aVar2 = (a) g20.A(value4);
                        }
                        List<a> value5 = this.c.getValue();
                        if (value5 != null) {
                            value5.add(new a(type, operation, null));
                        }
                    }
                }
                List<a> value6 = this.c.getValue();
                fs1.d(value6);
                if (value6.size() > 0 && (value = this.c.getValue()) != null) {
                    value.add(new a(Type.Operation, operation, null));
                }
            } else {
                List<a> value7 = this.c.getValue();
                if (value7 != null) {
                    value7.add(new a(Type.Digit, null, L));
                }
                List<a> value8 = this.c.getValue();
                if (value8 != null) {
                    value8.add(new a(Type.Operation, operation, null));
                }
            }
            this.e.setValue("");
            h();
        } catch (NumberFormatException unused) {
        }
    }

    public final void z(BigDecimal bigDecimal) {
        fs1.f(bigDecimal, "<set-?>");
        this.g = bigDecimal;
    }
}
