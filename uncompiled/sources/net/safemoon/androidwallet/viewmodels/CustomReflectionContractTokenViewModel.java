package net.safemoon.androidwallet.viewmodels;

import android.app.Application;
import android.graphics.Bitmap;
import android.util.Log;
import androidx.lifecycle.LiveData;
import java.util.ArrayList;
import java.util.List;
import net.safemoon.androidwallet.common.TokenType;
import net.safemoon.androidwallet.model.cmcTokenInfo.CmcTokenInfo;
import net.safemoon.androidwallet.model.cmcTokenInfo.TokenDetail;
import net.safemoon.androidwallet.model.reflections.RoomReflectionsToken;
import net.safemoon.androidwallet.model.transaction.history.Result;
import net.safemoon.androidwallet.model.transaction.history.TransactionHistoryModel;
import net.safemoon.androidwallet.repository.ReflectionDataSource;
import net.safemoon.androidwallet.utils.ReflectionCustomContract;
import net.safemoon.androidwallet.viewmodels.CustomReflectionContractTokenViewModel;
import retrofit2.n;

/* compiled from: CustomReflectionContractTokenViewModel.kt */
/* loaded from: classes2.dex */
public final class CustomReflectionContractTokenViewModel extends gd {
    public final sy1 b;
    public final sy1 c;
    public final gb2<TokenType> d;
    public final LiveData<List<RoomReflectionsToken>> e;
    public final gb2<TransactionHistoryModel> f;
    public final gb2<TokenDetail> g;
    public final gb2<Bitmap> h;
    public retrofit2.b<TransactionHistoryModel> i;
    public retrofit2.b<CmcTokenInfo> j;

    /* compiled from: CustomReflectionContractTokenViewModel.kt */
    /* loaded from: classes2.dex */
    public enum SaveReturnCode {
        ERROR,
        SUCCESS,
        PROGRESS,
        BLACKLIST,
        DUPLICATE
    }

    /* compiled from: CustomReflectionContractTokenViewModel.kt */
    /* loaded from: classes2.dex */
    public static final class a {
        public a() {
        }

        public /* synthetic */ a(qi0 qi0Var) {
            this();
        }
    }

    /* compiled from: CustomReflectionContractTokenViewModel.kt */
    /* loaded from: classes2.dex */
    public static final class b implements wu<TransactionHistoryModel> {
        public b() {
        }

        @Override // defpackage.wu
        public void a(retrofit2.b<TransactionHistoryModel> bVar, Throwable th) {
            fs1.f(bVar, "call");
            fs1.f(th, "t");
        }

        @Override // defpackage.wu
        public void b(retrofit2.b<TransactionHistoryModel> bVar, n<TransactionHistoryModel> nVar) {
            fs1.f(bVar, "call");
            fs1.f(nVar, "response");
            TransactionHistoryModel a = nVar.a();
            if (a == null) {
                return;
            }
            CustomReflectionContractTokenViewModel customReflectionContractTokenViewModel = CustomReflectionContractTokenViewModel.this;
            if (a.result.size() > 0) {
                customReflectionContractTokenViewModel.q().postValue(a);
            } else {
                customReflectionContractTokenViewModel.q().postValue(null);
            }
        }
    }

    /* compiled from: CustomReflectionContractTokenViewModel.kt */
    /* loaded from: classes2.dex */
    public static final class c implements wu<CmcTokenInfo> {
        public final /* synthetic */ String b;

        public c(String str) {
            this.b = str;
        }

        @Override // defpackage.wu
        public void a(retrofit2.b<CmcTokenInfo> bVar, Throwable th) {
            fs1.f(bVar, "call");
            fs1.f(th, "t");
            Log.println(7, "Error", String.valueOf(th));
        }

        @Override // defpackage.wu
        public void b(retrofit2.b<CmcTokenInfo> bVar, n<CmcTokenInfo> nVar) {
            fs1.f(bVar, "call");
            fs1.f(nVar, "response");
            if (nVar.e()) {
                CmcTokenInfo a = nVar.a();
                if (a == null) {
                    return;
                }
                CustomReflectionContractTokenViewModel customReflectionContractTokenViewModel = CustomReflectionContractTokenViewModel.this;
                String str = this.b;
                try {
                    customReflectionContractTokenViewModel.r().postValue(a.getTokenDetails().get(0));
                    customReflectionContractTokenViewModel.l().postValue(null);
                    return;
                } catch (Exception unused) {
                    customReflectionContractTokenViewModel.h(str);
                    return;
                }
            }
            CustomReflectionContractTokenViewModel.this.h(this.b);
        }
    }

    static {
        new a(null);
    }

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public CustomReflectionContractTokenViewModel(Application application) {
        super(application);
        fs1.f(application, "application");
        this.b = zy1.a(new CustomReflectionContractTokenViewModel$reflectionDataSource$2(application));
        this.c = zy1.a(new CustomReflectionContractTokenViewModel$reflectionCustomContract$2(this));
        gb2<TokenType> gb2Var = new gb2<>(g());
        this.d = gb2Var;
        LiveData<List<RoomReflectionsToken>> c2 = cb4.c(gb2Var, new ud1() { // from class: oc0
            @Override // defpackage.ud1
            public final Object apply(Object obj) {
                LiveData e;
                e = CustomReflectionContractTokenViewModel.e(CustomReflectionContractTokenViewModel.this, (TokenType) obj);
                return e;
            }
        });
        fs1.e(c2, "switchMap(selectedNetwor…iveData()\n        }\n    }");
        this.e = c2;
        this.f = new gb2<>();
        this.g = new gb2<>();
        this.h = new gb2<>();
    }

    public static final LiveData e(CustomReflectionContractTokenViewModel customReflectionContractTokenViewModel, TokenType tokenType) {
        fs1.f(customReflectionContractTokenViewModel, "this$0");
        if (tokenType != null) {
            return customReflectionContractTokenViewModel.n().d(tokenType);
        }
        return new gb2();
    }

    public final void f() {
        retrofit2.b<CmcTokenInfo> bVar = this.j;
        if (bVar != null) {
            bVar.cancel();
        }
        retrofit2.b<TransactionHistoryModel> bVar2 = this.i;
        if (bVar2 != null) {
            bVar2.cancel();
        }
        this.f.postValue(null);
        this.g.postValue(null);
        this.h.postValue(null);
    }

    public final TokenType g() {
        TokenType.a aVar = TokenType.Companion;
        String i = bo3.i(a(), "DEFAULT_GATEWAY");
        fs1.e(i, "getString(getApplication…redPrefs.DEFAULT_GATEWAY)");
        return aVar.c(i);
    }

    public final void h(String str) {
        fs1.f(str, "contractAddress");
        kotlinx.coroutines.a.b(ej4.a(this), null, null, new CustomReflectionContractTokenViewModel$generateIdentiIcon$1(this, str, null), 3, null);
    }

    public final LiveData<List<RoomReflectionsToken>> i() {
        return this.e;
    }

    public final void j(String str) {
        fs1.f(str, "contractAddress");
        TokenType p = p();
        if (p != null) {
            retrofit2.b<TransactionHistoryModel> bVar = this.i;
            if (bVar != null) {
                bVar.cancel();
            }
            retrofit2.b<TransactionHistoryModel> g = e30.s(p).g(str, e30.x(p));
            this.i = g;
            if (g != null) {
                g.n(new b());
            }
        }
        retrofit2.b<CmcTokenInfo> bVar2 = this.j;
        if (bVar2 != null) {
            bVar2.cancel();
        }
        jt e = a4.e();
        retrofit2.b<CmcTokenInfo> d = e == null ? null : e.d(str);
        this.j = d;
        if (d == null) {
            return;
        }
        d.n(new c(str));
    }

    public final Result k() {
        ArrayList<Result> arrayList;
        TransactionHistoryModel value = this.f.getValue();
        if (value == null || (arrayList = value.result) == null) {
            return null;
        }
        return arrayList.get(0);
    }

    public final gb2<Bitmap> l() {
        return this.h;
    }

    public final ReflectionCustomContract m() {
        return (ReflectionCustomContract) this.c.getValue();
    }

    public final ReflectionDataSource n() {
        return (ReflectionDataSource) this.b.getValue();
    }

    public final gb2<TokenType> o() {
        return this.d;
    }

    public final TokenType p() {
        return this.d.getValue();
    }

    public final gb2<TransactionHistoryModel> q() {
        return this.f;
    }

    public final gb2<TokenDetail> r() {
        return this.g;
    }

    public final void s(TokenType tokenType) {
        if (tokenType != null) {
            this.d.setValue(tokenType);
        }
    }

    public final void t(String str, String str2, String str3, String str4, tc1<? super SaveReturnCode, te4> tc1Var) {
        fs1.f(tc1Var, "callBack");
        if (str == null || str2 == null || str3 == null || str4 == null || cv3.l(str4) == null) {
            return;
        }
        TokenType p = p();
        if ((p == null ? null : Integer.valueOf(p.getChainId())) != null) {
            kotlinx.coroutines.a.b(ej4.a(this), null, null, new CustomReflectionContractTokenViewModel$save$1(this, str, str2, str3, str4, tc1Var, null), 3, null);
        }
    }
}
