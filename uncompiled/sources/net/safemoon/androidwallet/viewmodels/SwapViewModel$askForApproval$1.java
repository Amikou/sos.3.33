package net.safemoon.androidwallet.viewmodels;

import java.math.BigInteger;
import kotlin.coroutines.jvm.internal.SuspendLambda;
import kotlin.coroutines.jvm.internal.a;
import kotlinx.coroutines.CoroutineDispatcher;
import net.safemoon.androidwallet.ERC20;
import net.safemoon.androidwallet.model.common.LoadingState;
import net.safemoon.androidwallet.model.swap.Swap;
import net.safemoon.androidwallet.viewmodels.SwapViewModel;

/* compiled from: SwapViewModel.kt */
@a(c = "net.safemoon.androidwallet.viewmodels.SwapViewModel$askForApproval$1", f = "SwapViewModel.kt", l = {1011}, m = "invokeSuspend")
/* loaded from: classes2.dex */
public final class SwapViewModel$askForApproval$1 extends SuspendLambda implements hd1<c90, q70<? super te4>, Object> {
    public int label;
    public final /* synthetic */ SwapViewModel this$0;

    /* compiled from: SwapViewModel.kt */
    @kotlin.coroutines.jvm.internal.a(c = "net.safemoon.androidwallet.viewmodels.SwapViewModel$askForApproval$1$1", f = "SwapViewModel.kt", l = {2166}, m = "invokeSuspend")
    /* renamed from: net.safemoon.androidwallet.viewmodels.SwapViewModel$askForApproval$1$1  reason: invalid class name */
    /* loaded from: classes2.dex */
    public static final class AnonymousClass1 extends SuspendLambda implements hd1<c90, q70<? super te4>, Object> {
        public int label;
        public final /* synthetic */ SwapViewModel this$0;

        /* compiled from: SwapViewModel.kt */
        @kotlin.coroutines.jvm.internal.a(c = "net.safemoon.androidwallet.viewmodels.SwapViewModel$askForApproval$1$1$1", f = "SwapViewModel.kt", l = {1056, 1096, 1138, 1179}, m = "invokeSuspend")
        /* renamed from: net.safemoon.androidwallet.viewmodels.SwapViewModel$askForApproval$1$1$1  reason: invalid class name and collision with other inner class name */
        /* loaded from: classes2.dex */
        public static final class C02381 extends SuspendLambda implements hd1<k71<? super hx0>, q70<? super te4>, Object> {
            private /* synthetic */ Object L$0;
            public int label;
            public final /* synthetic */ SwapViewModel this$0;

            /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
            public C02381(SwapViewModel swapViewModel, q70<? super C02381> q70Var) {
                super(2, q70Var);
                this.this$0 = swapViewModel;
            }

            @Override // kotlin.coroutines.jvm.internal.BaseContinuationImpl
            public final q70<te4> create(Object obj, q70<?> q70Var) {
                C02381 c02381 = new C02381(this.this$0, q70Var);
                c02381.L$0 = obj;
                return c02381;
            }

            @Override // defpackage.hd1
            public final Object invoke(k71<? super hx0> k71Var, q70<? super te4> q70Var) {
                return ((C02381) create(k71Var, q70Var)).invokeSuspend(te4.a);
            }

            @Override // kotlin.coroutines.jvm.internal.BaseContinuationImpl
            public final Object invokeSuspend(Object obj) {
                BigInteger bigInteger;
                ERC20 erc20;
                ma0 W;
                ERC20 erc202;
                ma0 W2;
                BigInteger j0;
                BigInteger bigInteger2;
                ko4 n1;
                BigInteger j02;
                ERC20 erc203;
                ko4 n12;
                ma0 W3;
                ERC20 erc204;
                ma0 W4;
                ERC20 erc205;
                ma0 W5;
                BigInteger j03;
                BigInteger bigInteger3;
                ko4 n13;
                BigInteger j04;
                ERC20 erc206;
                ko4 n14;
                ma0 W6;
                ERC20 erc207;
                ma0 W7;
                ERC20 erc208;
                ma0 W8;
                BigInteger j05;
                BigInteger bigInteger4;
                ko4 n15;
                BigInteger j06;
                ERC20 erc209;
                ko4 n16;
                ma0 W9;
                ERC20 erc2010;
                ma0 W10;
                ERC20 erc2011;
                ma0 W11;
                BigInteger j07;
                BigInteger bigInteger5;
                ko4 n17;
                BigInteger j08;
                ERC20 erc2012;
                ko4 n18;
                ma0 W12;
                Object d = gs1.d();
                int i = this.label;
                if (i == 0) {
                    o83.b(obj);
                    k71 k71Var = (k71) this.L$0;
                    Swap value = this.this$0.b0().getValue();
                    fs1.d(value);
                    fs1.e(value, "destinationLiveData.value!!");
                    Swap swap = value;
                    SwapViewModel.a value2 = this.this$0.F0().getValue();
                    fs1.d(value2);
                    fs1.e(value2, "tilEnterAmount.value!!");
                    SwapViewModel.a aVar = value2;
                    SwapViewModel swapViewModel = this.this$0;
                    bigInteger = swapViewModel.x;
                    fs1.e(bigInteger, "GAS_LIMIT");
                    swapViewModel.o0(bigInteger);
                    ERC20 erc2013 = null;
                    if (!dv3.t(swap.symbol, "BNB", true) && !dv3.t(swap.symbol, "ETH", true)) {
                        if (aVar.b()) {
                            l84 I0 = this.this$0.I0();
                            fs1.d(I0);
                            SwapViewModel swapViewModel2 = this.this$0;
                            BigInteger a = I0.a();
                            erc2010 = swapViewModel2.y;
                            if (erc2010 == null) {
                                fs1.r("erc20Source");
                                erc2010 = null;
                            }
                            W10 = swapViewModel2.W();
                            if (erc2010.o(W10.getAddress(), swapViewModel2.u0()).send().compareTo(a) < 0) {
                                erc2011 = swapViewModel2.y;
                                if (erc2011 == null) {
                                    fs1.r("erc20Source");
                                    erc2011 = null;
                                }
                                String u0 = swapViewModel2.u0();
                                BigInteger bigInteger6 = BigInteger.ONE;
                                String r = erc2011.r(u0, bigInteger6.shiftLeft(256).subtract(bigInteger6));
                                W11 = swapViewModel2.W();
                                String address = W11.getAddress();
                                BigInteger p0 = swapViewModel2.p0();
                                j07 = swapViewModel2.j0();
                                bigInteger5 = swapViewModel2.x;
                                Swap value3 = swapViewModel2.A0().getValue();
                                fs1.d(value3);
                                String str = value3.address;
                                BigInteger bigInteger7 = BigInteger.ZERO;
                                p84 createFunctionCallTransaction = p84.createFunctionCallTransaction(address, p0, j07, bigInteger5, str, bigInteger7, r);
                                n17 = swapViewModel2.n1();
                                BigInteger p02 = swapViewModel2.p0();
                                j08 = swapViewModel2.j0();
                                BigInteger amountUsed = n17.ethEstimateGas(createFunctionCallTransaction).send().getAmountUsed();
                                erc2012 = swapViewModel2.y;
                                if (erc2012 == null) {
                                    fs1.r("erc20Source");
                                } else {
                                    erc2013 = erc2012;
                                }
                                y33 createTransaction = y33.createTransaction(p02, j08, amountUsed, erc2013.getContractAddress(), bigInteger7, r);
                                n18 = swapViewModel2.n1();
                                W12 = swapViewModel2.W();
                                hx0 signAndSend = new z33(n18, W12).signAndSend(createTransaction);
                                this.label = 3;
                                if (k71Var.emit(signAndSend, this) == d) {
                                    return d;
                                }
                            } else {
                                swapViewModel2.Q().postValue(hr.d(0));
                                swapViewModel2.O().postValue(LoadingState.Normal);
                            }
                        } else {
                            l84 I02 = this.this$0.I0();
                            fs1.d(I02);
                            SwapViewModel swapViewModel3 = this.this$0;
                            BigInteger D = swapViewModel3.D(I02.a());
                            erc207 = swapViewModel3.y;
                            if (erc207 == null) {
                                fs1.r("erc20Source");
                                erc207 = null;
                            }
                            W7 = swapViewModel3.W();
                            if (erc207.o(W7.getAddress(), swapViewModel3.u0()).send().compareTo(D) < 0) {
                                erc208 = swapViewModel3.y;
                                if (erc208 == null) {
                                    fs1.r("erc20Source");
                                    erc208 = null;
                                }
                                String u02 = swapViewModel3.u0();
                                BigInteger bigInteger8 = BigInteger.ONE;
                                String r2 = erc208.r(u02, bigInteger8.shiftLeft(256).subtract(bigInteger8));
                                W8 = swapViewModel3.W();
                                String address2 = W8.getAddress();
                                BigInteger p03 = swapViewModel3.p0();
                                j05 = swapViewModel3.j0();
                                bigInteger4 = swapViewModel3.x;
                                Swap value4 = swapViewModel3.A0().getValue();
                                fs1.d(value4);
                                String str2 = value4.address;
                                BigInteger bigInteger9 = BigInteger.ZERO;
                                p84 createFunctionCallTransaction2 = p84.createFunctionCallTransaction(address2, p03, j05, bigInteger4, str2, bigInteger9, r2);
                                n15 = swapViewModel3.n1();
                                BigInteger p04 = swapViewModel3.p0();
                                j06 = swapViewModel3.j0();
                                BigInteger amountUsed2 = n15.ethEstimateGas(createFunctionCallTransaction2).send().getAmountUsed();
                                erc209 = swapViewModel3.y;
                                if (erc209 == null) {
                                    fs1.r("erc20Source");
                                } else {
                                    erc2013 = erc209;
                                }
                                y33 createTransaction2 = y33.createTransaction(p04, j06, amountUsed2, erc2013.getContractAddress(), bigInteger9, r2);
                                n16 = swapViewModel3.n1();
                                W9 = swapViewModel3.W();
                                hx0 signAndSend2 = new z33(n16, W9).signAndSend(createTransaction2);
                                this.label = 4;
                                if (k71Var.emit(signAndSend2, this) == d) {
                                    return d;
                                }
                            } else {
                                swapViewModel3.Q().postValue(hr.d(0));
                                swapViewModel3.O().postValue(LoadingState.Normal);
                            }
                        }
                    } else if (aVar.b()) {
                        l84 I03 = this.this$0.I0();
                        fs1.d(I03);
                        SwapViewModel swapViewModel4 = this.this$0;
                        BigInteger a2 = I03.a();
                        erc204 = swapViewModel4.y;
                        if (erc204 == null) {
                            fs1.r("erc20Source");
                            erc204 = null;
                        }
                        W4 = swapViewModel4.W();
                        if (erc204.o(W4.getAddress(), swapViewModel4.u0()).send().compareTo(a2) < 0) {
                            erc205 = swapViewModel4.y;
                            if (erc205 == null) {
                                fs1.r("erc20Source");
                                erc205 = null;
                            }
                            String u03 = swapViewModel4.u0();
                            BigInteger bigInteger10 = BigInteger.ONE;
                            String r3 = erc205.r(u03, bigInteger10.shiftLeft(256).subtract(bigInteger10));
                            W5 = swapViewModel4.W();
                            String address3 = W5.getAddress();
                            BigInteger p05 = swapViewModel4.p0();
                            j03 = swapViewModel4.j0();
                            bigInteger3 = swapViewModel4.x;
                            Swap value5 = swapViewModel4.A0().getValue();
                            fs1.d(value5);
                            String str3 = value5.address;
                            BigInteger bigInteger11 = BigInteger.ZERO;
                            p84 createFunctionCallTransaction3 = p84.createFunctionCallTransaction(address3, p05, j03, bigInteger3, str3, bigInteger11, r3);
                            n13 = swapViewModel4.n1();
                            BigInteger p06 = swapViewModel4.p0();
                            j04 = swapViewModel4.j0();
                            BigInteger amountUsed3 = n13.ethEstimateGas(createFunctionCallTransaction3).send().getAmountUsed();
                            erc206 = swapViewModel4.y;
                            if (erc206 == null) {
                                fs1.r("erc20Source");
                            } else {
                                erc2013 = erc206;
                            }
                            y33 createTransaction3 = y33.createTransaction(p06, j04, amountUsed3, erc2013.getContractAddress(), bigInteger11, r3);
                            n14 = swapViewModel4.n1();
                            W6 = swapViewModel4.W();
                            hx0 signAndSend3 = new z33(n14, W6).signAndSend(createTransaction3);
                            this.label = 1;
                            if (k71Var.emit(signAndSend3, this) == d) {
                                return d;
                            }
                        } else {
                            swapViewModel4.Q().postValue(hr.d(0));
                            swapViewModel4.O().postValue(LoadingState.Normal);
                        }
                    } else {
                        l84 I04 = this.this$0.I0();
                        fs1.d(I04);
                        SwapViewModel swapViewModel5 = this.this$0;
                        BigInteger D2 = swapViewModel5.D(I04.a());
                        erc20 = swapViewModel5.y;
                        if (erc20 == null) {
                            fs1.r("erc20Source");
                            erc20 = null;
                        }
                        W = swapViewModel5.W();
                        if (erc20.o(W.getAddress(), swapViewModel5.u0()).send().compareTo(D2) < 0) {
                            erc202 = swapViewModel5.y;
                            if (erc202 == null) {
                                fs1.r("erc20Source");
                                erc202 = null;
                            }
                            String u04 = swapViewModel5.u0();
                            BigInteger bigInteger12 = BigInteger.ONE;
                            String r4 = erc202.r(u04, bigInteger12.shiftLeft(256).subtract(bigInteger12));
                            W2 = swapViewModel5.W();
                            String address4 = W2.getAddress();
                            BigInteger p07 = swapViewModel5.p0();
                            j0 = swapViewModel5.j0();
                            bigInteger2 = swapViewModel5.x;
                            Swap value6 = swapViewModel5.A0().getValue();
                            fs1.d(value6);
                            String str4 = value6.address;
                            BigInteger bigInteger13 = BigInteger.ZERO;
                            p84 createFunctionCallTransaction4 = p84.createFunctionCallTransaction(address4, p07, j0, bigInteger2, str4, bigInteger13, r4);
                            n1 = swapViewModel5.n1();
                            BigInteger p08 = swapViewModel5.p0();
                            j02 = swapViewModel5.j0();
                            BigInteger amountUsed4 = n1.ethEstimateGas(createFunctionCallTransaction4).send().getAmountUsed();
                            erc203 = swapViewModel5.y;
                            if (erc203 == null) {
                                fs1.r("erc20Source");
                            } else {
                                erc2013 = erc203;
                            }
                            y33 createTransaction4 = y33.createTransaction(p08, j02, amountUsed4, erc2013.getContractAddress(), bigInteger13, r4);
                            n12 = swapViewModel5.n1();
                            W3 = swapViewModel5.W();
                            hx0 signAndSend4 = new z33(n12, W3).signAndSend(createTransaction4);
                            this.label = 2;
                            if (k71Var.emit(signAndSend4, this) == d) {
                                return d;
                            }
                        } else {
                            swapViewModel5.Q().postValue(hr.d(0));
                            swapViewModel5.O().postValue(LoadingState.Normal);
                        }
                    }
                } else if (i != 1 && i != 2 && i != 3 && i != 4) {
                    throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
                } else {
                    o83.b(obj);
                }
                return te4.a;
            }
        }

        /* compiled from: SwapViewModel.kt */
        @kotlin.coroutines.jvm.internal.a(c = "net.safemoon.androidwallet.viewmodels.SwapViewModel$askForApproval$1$1$2", f = "SwapViewModel.kt", l = {}, m = "invokeSuspend")
        /* renamed from: net.safemoon.androidwallet.viewmodels.SwapViewModel$askForApproval$1$1$2  reason: invalid class name */
        /* loaded from: classes2.dex */
        public static final class AnonymousClass2 extends SuspendLambda implements kd1<k71<? super hx0>, Throwable, q70<? super te4>, Object> {
            public int label;
            public final /* synthetic */ SwapViewModel this$0;

            /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
            public AnonymousClass2(SwapViewModel swapViewModel, q70<? super AnonymousClass2> q70Var) {
                super(3, q70Var);
                this.this$0 = swapViewModel;
            }

            @Override // defpackage.kd1
            public final Object invoke(k71<? super hx0> k71Var, Throwable th, q70<? super te4> q70Var) {
                return new AnonymousClass2(this.this$0, q70Var).invokeSuspend(te4.a);
            }

            @Override // kotlin.coroutines.jvm.internal.BaseContinuationImpl
            public final Object invokeSuspend(Object obj) {
                gs1.d();
                if (this.label == 0) {
                    o83.b(obj);
                    this.this$0.K0().postValue(null);
                    this.this$0.O().postValue(LoadingState.Normal);
                    SwapViewModel.i1(this.this$0, null, 1, null);
                    return te4.a;
                }
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
        }

        /* compiled from: Collect.kt */
        /* renamed from: net.safemoon.androidwallet.viewmodels.SwapViewModel$askForApproval$1$1$a */
        /* loaded from: classes2.dex */
        public static final class a implements k71<hx0> {
            public final /* synthetic */ SwapViewModel a;

            public a(SwapViewModel swapViewModel) {
                this.a = swapViewModel;
            }

            @Override // defpackage.k71
            public Object emit(hx0 hx0Var, q70<? super te4> q70Var) {
                st1 b;
                b = as.b(ej4.a(this.a), null, null, new SwapViewModel$askForApproval$1$1$3$1(hx0Var, this.a, null), 3, null);
                return b == gs1.d() ? b : te4.a;
            }
        }

        /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
        public AnonymousClass1(SwapViewModel swapViewModel, q70<? super AnonymousClass1> q70Var) {
            super(2, q70Var);
            this.this$0 = swapViewModel;
        }

        @Override // kotlin.coroutines.jvm.internal.BaseContinuationImpl
        public final q70<te4> create(Object obj, q70<?> q70Var) {
            return new AnonymousClass1(this.this$0, q70Var);
        }

        @Override // defpackage.hd1
        public final Object invoke(c90 c90Var, q70<? super te4> q70Var) {
            return ((AnonymousClass1) create(c90Var, q70Var)).invokeSuspend(te4.a);
        }

        @Override // kotlin.coroutines.jvm.internal.BaseContinuationImpl
        public final Object invokeSuspend(Object obj) {
            Object d = gs1.d();
            int i = this.label;
            if (i == 0) {
                o83.b(obj);
                j71 c = n71.c(n71.n(new C02381(this.this$0, null)), new AnonymousClass2(this.this$0, null));
                a aVar = new a(this.this$0);
                this.label = 1;
                if (c.a(aVar, this) == d) {
                    return d;
                }
            } else if (i != 1) {
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            } else {
                o83.b(obj);
            }
            return te4.a;
        }
    }

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public SwapViewModel$askForApproval$1(SwapViewModel swapViewModel, q70<? super SwapViewModel$askForApproval$1> q70Var) {
        super(2, q70Var);
        this.this$0 = swapViewModel;
    }

    @Override // kotlin.coroutines.jvm.internal.BaseContinuationImpl
    public final q70<te4> create(Object obj, q70<?> q70Var) {
        return new SwapViewModel$askForApproval$1(this.this$0, q70Var);
    }

    @Override // defpackage.hd1
    public final Object invoke(c90 c90Var, q70<? super te4> q70Var) {
        return ((SwapViewModel$askForApproval$1) create(c90Var, q70Var)).invokeSuspend(te4.a);
    }

    @Override // kotlin.coroutines.jvm.internal.BaseContinuationImpl
    public final Object invokeSuspend(Object obj) {
        Object d = gs1.d();
        int i = this.label;
        if (i == 0) {
            o83.b(obj);
            CoroutineDispatcher b = tp0.b();
            AnonymousClass1 anonymousClass1 = new AnonymousClass1(this.this$0, null);
            this.label = 1;
            if (kotlinx.coroutines.a.e(b, anonymousClass1, this) == d) {
                return d;
            }
        } else if (i != 1) {
            throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        } else {
            o83.b(obj);
        }
        return te4.a;
    }
}
