package net.safemoon.androidwallet.viewmodels;

import kotlin.coroutines.jvm.internal.SuspendLambda;
import kotlin.coroutines.jvm.internal.a;
import kotlinx.coroutines.CoroutineDispatcher;
import net.safemoon.androidwallet.viewmodels.TransferViewModel;
import net.safemoon.androidwallet.viewmodels.blockChainClass.Transfer;

/* compiled from: TransferViewModel.kt */
@a(c = "net.safemoon.androidwallet.viewmodels.TransferViewModel$getEstimateGas$1", f = "TransferViewModel.kt", l = {56}, m = "invokeSuspend")
/* loaded from: classes2.dex */
public final class TransferViewModel$getEstimateGas$1 extends SuspendLambda implements hd1<c90, q70<? super te4>, Object> {
    public int label;
    public final /* synthetic */ TransferViewModel this$0;

    /* compiled from: TransferViewModel.kt */
    @a(c = "net.safemoon.androidwallet.viewmodels.TransferViewModel$getEstimateGas$1$1", f = "TransferViewModel.kt", l = {59}, m = "invokeSuspend")
    /* renamed from: net.safemoon.androidwallet.viewmodels.TransferViewModel$getEstimateGas$1$1  reason: invalid class name */
    /* loaded from: classes2.dex */
    public static final class AnonymousClass1 extends SuspendLambda implements hd1<c90, q70<? super te4>, Object> {
        public Object L$0;
        public Object L$1;
        public int label;
        public final /* synthetic */ TransferViewModel this$0;

        /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
        public AnonymousClass1(TransferViewModel transferViewModel, q70<? super AnonymousClass1> q70Var) {
            super(2, q70Var);
            this.this$0 = transferViewModel;
        }

        @Override // kotlin.coroutines.jvm.internal.BaseContinuationImpl
        public final q70<te4> create(Object obj, q70<?> q70Var) {
            return new AnonymousClass1(this.this$0, q70Var);
        }

        @Override // defpackage.hd1
        public final Object invoke(c90 c90Var, q70<? super te4> q70Var) {
            return ((AnonymousClass1) create(c90Var, q70Var)).invokeSuspend(te4.a);
        }

        @Override // kotlin.coroutines.jvm.internal.BaseContinuationImpl
        public final Object invokeSuspend(Object obj) {
            TransferViewModel transferViewModel;
            Transfer transfer;
            Object d = gs1.d();
            int i = this.label;
            try {
                if (i == 0) {
                    o83.b(obj);
                    Transfer j = this.this$0.j();
                    fs1.d(j);
                    transferViewModel = this.this$0;
                    this.L$0 = transferViewModel;
                    this.L$1 = j;
                    this.label = 1;
                    if (j.x(this) == d) {
                        return d;
                    }
                    transfer = j;
                } else if (i != 1) {
                    throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
                } else {
                    transfer = (Transfer) this.L$1;
                    transferViewModel = (TransferViewModel) this.L$0;
                    o83.b(obj);
                }
                transferViewModel.f().postValue(new TransferViewModel.a(true, transfer.z()));
            } catch (Exception unused) {
                this.this$0.f().postValue(new TransferViewModel.a(false, null));
            }
            return te4.a;
        }
    }

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public TransferViewModel$getEstimateGas$1(TransferViewModel transferViewModel, q70<? super TransferViewModel$getEstimateGas$1> q70Var) {
        super(2, q70Var);
        this.this$0 = transferViewModel;
    }

    @Override // kotlin.coroutines.jvm.internal.BaseContinuationImpl
    public final q70<te4> create(Object obj, q70<?> q70Var) {
        return new TransferViewModel$getEstimateGas$1(this.this$0, q70Var);
    }

    @Override // defpackage.hd1
    public final Object invoke(c90 c90Var, q70<? super te4> q70Var) {
        return ((TransferViewModel$getEstimateGas$1) create(c90Var, q70Var)).invokeSuspend(te4.a);
    }

    @Override // kotlin.coroutines.jvm.internal.BaseContinuationImpl
    public final Object invokeSuspend(Object obj) {
        Object d = gs1.d();
        int i = this.label;
        if (i == 0) {
            o83.b(obj);
            CoroutineDispatcher b = tp0.b();
            AnonymousClass1 anonymousClass1 = new AnonymousClass1(this.this$0, null);
            this.label = 1;
            if (kotlinx.coroutines.a.e(b, anonymousClass1, this) == d) {
                return d;
            }
        } else if (i != 1) {
            throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        } else {
            o83.b(obj);
        }
        return te4.a;
    }
}
