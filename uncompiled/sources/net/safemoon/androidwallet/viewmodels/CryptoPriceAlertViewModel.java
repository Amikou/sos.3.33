package net.safemoon.androidwallet.viewmodels;

import android.app.Application;
import androidx.lifecycle.LiveData;
import java.util.ArrayList;
import java.util.List;
import net.safemoon.androidwallet.model.common.LoadingState;
import net.safemoon.androidwallet.model.priceAlert.PAToken;
import net.safemoon.androidwallet.model.priceAlert.PriceAlertToken;
import net.safemoon.androidwallet.model.wallets.Wallet;
import net.safemoon.androidwallet.repository.FcmDataSource;
import net.safemoon.androidwallet.repository.PriceFetchDataSource;
import net.safemoon.androidwallet.viewmodels.CryptoPriceAlertViewModel;

/* compiled from: CryptoPriceAlertViewModel.kt */
/* loaded from: classes2.dex */
public final class CryptoPriceAlertViewModel extends gd {
    public final sy1 b;
    public final sy1 c;
    public final sy1 d;
    public final sy1 e;
    public final gb2<LoadingState> f;
    public final gb2<List<PriceAlertToken>> g;
    public final gb2<Integer> h;
    public final LiveData<PriceAlertToken> i;

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public CryptoPriceAlertViewModel(Application application) {
        super(application);
        fs1.f(application, "application");
        this.b = zy1.a(CryptoPriceAlertViewModel$mainNetApiInterface$2.INSTANCE);
        this.c = zy1.a(new CryptoPriceAlertViewModel$activeWallet$2(application));
        this.d = zy1.a(new CryptoPriceAlertViewModel$priceFetch$2(application));
        this.e = zy1.a(new CryptoPriceAlertViewModel$fcmDataSource$2(application));
        this.f = new gb2<>(LoadingState.Loading);
        this.g = new gb2<>();
        gb2<Integer> gb2Var = new gb2<>(null);
        this.h = gb2Var;
        LiveData<PriceAlertToken> c = cb4.c(gb2Var, new ud1() { // from class: gb0
            @Override // defpackage.ud1
            public final Object apply(Object obj) {
                LiveData w;
                w = CryptoPriceAlertViewModel.w(CryptoPriceAlertViewModel.this, (Integer) obj);
                return w;
            }
        });
        fs1.e(c, "switchMap(selectedIndex)…e?.getOrNull(it) })\n    }");
        this.i = c;
    }

    public static /* synthetic */ void A(CryptoPriceAlertViewModel cryptoPriceAlertViewModel, PriceAlertToken priceAlertToken, Integer num, int i, Object obj) {
        if ((i & 2) != 0) {
            num = cryptoPriceAlertViewModel.h.getValue();
        }
        cryptoPriceAlertViewModel.z(priceAlertToken, num);
    }

    public static final LiveData w(CryptoPriceAlertViewModel cryptoPriceAlertViewModel, Integer num) {
        fs1.f(cryptoPriceAlertViewModel, "this$0");
        PriceAlertToken priceAlertToken = null;
        if (num != null) {
            int intValue = num.intValue();
            List<PriceAlertToken> value = cryptoPriceAlertViewModel.r().getValue();
            if (value != null) {
                priceAlertToken = (PriceAlertToken) j20.N(value, intValue);
            }
        }
        return new gb2(priceAlertToken);
    }

    public final void h(PAToken pAToken) {
        fs1.f(pAToken, "paToken");
        as.b(ej4.a(this), tp0.b(), null, new CryptoPriceAlertViewModel$addBlankPriceAlert$1(this, pAToken, null), 2, null);
    }

    public final void i(PriceAlertToken priceAlertToken, int i) {
        fs1.f(priceAlertToken, "priceAlertData");
        as.b(qg1.a, tp0.b(), null, new CryptoPriceAlertViewModel$createUpdatePriceAlert$1(this, priceAlertToken, i, null), 2, null);
    }

    public final void j(PriceAlertToken priceAlertToken) {
        fs1.f(priceAlertToken, "paToken");
        if (m() == null) {
            return;
        }
        as.b(ej4.a(this), tp0.b(), null, new CryptoPriceAlertViewModel$deleteAllPriceAlert$1(this, priceAlertToken, null), 2, null);
    }

    public final void k(PriceAlertToken priceAlertToken, int i) {
        fs1.f(priceAlertToken, "priceAlertData");
        List list = null;
        if (priceAlertToken.getId() != null) {
            as.b(qg1.a, tp0.b(), null, new CryptoPriceAlertViewModel$deletePriceAlert$1(this, priceAlertToken, i, null), 2, null);
            return;
        }
        List<PriceAlertToken> value = this.g.getValue();
        if (value != null) {
            list = new ArrayList();
            int i2 = 0;
            for (Object obj : value) {
                int i3 = i2 + 1;
                if (i2 < 0) {
                    b20.p();
                }
                PriceAlertToken priceAlertToken2 = (PriceAlertToken) obj;
                if (i2 != i) {
                    list.add(obj);
                }
                i2 = i3;
            }
        }
        if (list == null) {
            list = b20.g();
        }
        this.g.postValue(list);
        if (i >= list.size()) {
            this.h.postValue(Integer.valueOf(list.size() - 1));
        } else {
            this.h.postValue(Integer.valueOf(i));
        }
    }

    public final void l(PAToken pAToken) {
        fs1.f(pAToken, "paToken");
        as.b(ej4.a(this), tp0.b(), null, new CryptoPriceAlertViewModel$fetchPriceAlert$1(this, pAToken, null), 2, null);
    }

    public final Wallet m() {
        return (Wallet) this.c.getValue();
    }

    public final FcmDataSource n() {
        return (FcmDataSource) this.e.getValue();
    }

    public final gb2<LoadingState> o() {
        return this.f;
    }

    public final ac3 p() {
        return (ac3) this.b.getValue();
    }

    /* JADX WARN: Removed duplicated region for block: B:10:0x0030  */
    /* JADX WARN: Removed duplicated region for block: B:18:0x0090  */
    /* JADX WARN: Removed duplicated region for block: B:33:0x00d6  */
    /* JADX WARN: Removed duplicated region for block: B:38:0x00e3  */
    /* JADX WARN: Removed duplicated region for block: B:55:0x018a A[RETURN] */
    /* JADX WARN: Removed duplicated region for block: B:56:0x018b  */
    /* JADX WARN: Removed duplicated region for block: B:59:0x0198  */
    /* JADX WARN: Removed duplicated region for block: B:60:0x019a  */
    /* JADX WARN: Removed duplicated region for block: B:71:0x01e5  */
    /*
        Code decompiled incorrectly, please refer to instructions dump.
        To view partially-correct code enable 'Show inconsistent code' option in preferences
    */
    public final java.lang.Object q(java.lang.String r40, java.lang.String r41, java.lang.String r42, int r43, java.lang.Integer r44, defpackage.q70<? super net.safemoon.androidwallet.model.priceAlert.PriceAlertToken> r45) {
        /*
            Method dump skipped, instructions count: 517
            To view this dump change 'Code comments level' option to 'DEBUG'
        */
        throw new UnsupportedOperationException("Method not decompiled: net.safemoon.androidwallet.viewmodels.CryptoPriceAlertViewModel.q(java.lang.String, java.lang.String, java.lang.String, int, java.lang.Integer, q70):java.lang.Object");
    }

    public final gb2<List<PriceAlertToken>> r() {
        return this.g;
    }

    public final PriceFetchDataSource s() {
        return (PriceFetchDataSource) this.d.getValue();
    }

    public final gb2<Integer> t() {
        return this.h;
    }

    public final LiveData<PriceAlertToken> u() {
        return this.i;
    }

    /* JADX WARN: Can't wrap try/catch for region: R(12:1|(2:3|(10:5|6|7|(1:(1:(5:11|12|(3:18|(1:22)|(3:24|(1:26)|(2:28|29)(1:31)))|32|33)(2:35|36))(3:37|38|39))(2:43|(2:45|46)(5:47|48|(1:50)(1:55)|51|(1:53)(1:54)))|40|(1:42)|12|(5:14|16|18|(2:20|22)|(0))|32|33))|57|6|7|(0)(0)|40|(0)|12|(0)|32|33) */
    /* JADX WARN: Code restructure failed: missing block: B:37:0x00a6, code lost:
        r11 = null;
     */
    /* JADX WARN: Removed duplicated region for block: B:10:0x0025  */
    /* JADX WARN: Removed duplicated region for block: B:19:0x004d  */
    /* JADX WARN: Removed duplicated region for block: B:34:0x00a2 A[RETURN] */
    /* JADX WARN: Removed duplicated region for block: B:39:0x00a9  */
    /* JADX WARN: Removed duplicated region for block: B:51:0x00cc  */
    /*
        Code decompiled incorrectly, please refer to instructions dump.
        To view partially-correct code enable 'Show inconsistent code' option in preferences
    */
    public final java.lang.Object v(java.lang.String r9, java.lang.String r10, defpackage.q70<? super java.util.ArrayList<net.safemoon.androidwallet.model.priceAlert.PriceAlertToken>> r11) {
        /*
            Method dump skipped, instructions count: 231
            To view this dump change 'Code comments level' option to 'DEBUG'
        */
        throw new UnsupportedOperationException("Method not decompiled: net.safemoon.androidwallet.viewmodels.CryptoPriceAlertViewModel.v(java.lang.String, java.lang.String, q70):java.lang.Object");
    }

    public final void x() {
        this.f.postValue(LoadingState.Loading);
    }

    public final void y() {
        this.f.postValue(LoadingState.Normal);
    }

    public final void z(PriceAlertToken priceAlertToken, Integer num) {
        List<PriceAlertToken> m0;
        fs1.f(priceAlertToken, "priceToken");
        int intValue = num == null ? 0 : num.intValue();
        List<PriceAlertToken> value = this.g.getValue();
        if (value == null || (m0 = j20.m0(value)) == null) {
            return;
        }
        m0.set(intValue, priceAlertToken);
        r().postValue(m0);
        t().postValue(Integer.valueOf(intValue));
    }
}
