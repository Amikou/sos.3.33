package net.safemoon.androidwallet.viewmodels;

import android.app.Application;
import kotlin.jvm.internal.Lambda;
import net.safemoon.androidwallet.repository.FcmDataSource;

/* compiled from: SelectFiatViewModel.kt */
/* loaded from: classes2.dex */
public final class SelectFiatViewModel$fcmDataSource$2 extends Lambda implements rc1<FcmDataSource> {
    public final /* synthetic */ Application $application;

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public SelectFiatViewModel$fcmDataSource$2(Application application) {
        super(0);
        this.$application = application;
    }

    @Override // defpackage.rc1
    public final FcmDataSource invoke() {
        return new FcmDataSource(this.$application);
    }
}
