package net.safemoon.androidwallet.viewmodels;

import android.app.Application;
import android.os.CountDownTimer;
import androidx.lifecycle.LiveData;
import com.fasterxml.jackson.databind.deser.std.ThrowableDeserializer;
import com.github.mikephil.charting.utils.Utils;
import defpackage.st1;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.math.MathContext;
import java.math.RoundingMode;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.ListIterator;
import java.util.Locale;
import java.util.Objects;
import java.util.concurrent.TimeUnit;
import kotlin.jvm.internal.Ref$ObjectRef;
import net.safemoon.androidwallet.ERC20;
import net.safemoon.androidwallet.R;
import net.safemoon.androidwallet.SafeswapFactory;
import net.safemoon.androidwallet.SafeswapPair;
import net.safemoon.androidwallet.SafeswapRouter;
import net.safemoon.androidwallet.SafeswapTradeRouter;
import net.safemoon.androidwallet.common.TokenType;
import net.safemoon.androidwallet.model.common.Gas;
import net.safemoon.androidwallet.model.common.GasPrice;
import net.safemoon.androidwallet.model.common.LoadingState;
import net.safemoon.androidwallet.model.swap.AllSwapTokens;
import net.safemoon.androidwallet.model.swap.BaseTokens;
import net.safemoon.androidwallet.model.swap.Pairs;
import net.safemoon.androidwallet.model.swap.Swap;
import net.safemoon.androidwallet.utils.PreFetchData;
import net.safemoon.androidwallet.viewmodels.SwapViewModel;
import org.web3j.protocol.core.DefaultBlockParameterName;
import retrofit2.b;
import retrofit2.n;

/* compiled from: SwapViewModel.kt */
/* loaded from: classes2.dex */
public final class SwapViewModel extends gd {
    public st1 A;
    public st1 B;
    public st1 C;
    public st1 D;
    public st1 E;
    public final long F;
    public final gb2<Long> G;
    public final gb2<Boolean> H;
    public CountDownTimer I;
    public st1 J;
    public final gb2<String> K;
    public final gb2<Double> L;
    public final gb2<Double> M;
    public final gb2<Boolean> N;
    public st1 O;
    public HashMap<Integer, Pairs> P;
    public HashMap<Integer, BaseTokens> Q;
    public l84 R;
    public ex3 S;
    public final gb2<LoadingState> T;
    public final gb2<e> U;
    public final gb2<c> V;
    public List<? extends Swap> W;
    public final gb2<List<Swap>> X;
    public final Application b;
    public final gb2<a> c;
    public final gb2<Double> d;
    public final gb2<Gas> e;
    public final gb2<Integer> f;
    public gb2<Swap> g;
    public gb2<Swap> h;
    public final gb2<BigDecimal> i;
    public final gb2<BigDecimal> j;
    public final gb2<Double> k;
    public final gb2<Double> l;
    public final gb2<String> m;
    public final gb2<d> n;
    public final gb2<LoadingState> o;
    public final gb2<BigDecimal> p;
    public final gb2<BigDecimal> q;
    public final gb2<Integer> r;
    public final gb2<LoadingState> s;
    public final GasPrice t;
    public final sy1 u;
    public final sy1 v;
    public final sy1 w;
    public final BigInteger x;
    public ERC20 y;
    public ERC20 z;

    /* compiled from: SwapViewModel.kt */
    /* loaded from: classes2.dex */
    public enum ApproveStatus {
        ALLOWED,
        FAILED,
        CONFUSE
    }

    /* compiled from: SwapViewModel.kt */
    /* loaded from: classes2.dex */
    public static final class a {
        public final boolean a;
        public final BigDecimal b;

        public a(boolean z, BigDecimal bigDecimal) {
            fs1.f(bigDecimal, "amount");
            this.a = z;
            this.b = bigDecimal;
        }

        public final BigDecimal a() {
            return this.b;
        }

        public final boolean b() {
            return this.a;
        }

        public boolean equals(Object obj) {
            if (this == obj) {
                return true;
            }
            if (obj instanceof a) {
                a aVar = (a) obj;
                return this.a == aVar.a && fs1.b(this.b, aVar.b);
            }
            return false;
        }

        /* JADX WARN: Multi-variable type inference failed */
        /* JADX WARN: Type inference failed for: r0v1, types: [int] */
        /* JADX WARN: Type inference failed for: r0v4 */
        /* JADX WARN: Type inference failed for: r0v5 */
        public int hashCode() {
            boolean z = this.a;
            ?? r0 = z;
            if (z) {
                r0 = 1;
            }
            return (r0 * 31) + this.b.hashCode();
        }

        public String toString() {
            return "AMOUNT(isSource=" + this.a + ", amount=" + this.b + ')';
        }
    }

    /* compiled from: SwapViewModel.kt */
    /* loaded from: classes2.dex */
    public static final class b {
        public b() {
        }

        public /* synthetic */ b(qi0 qi0Var) {
            this();
        }
    }

    /* compiled from: SwapViewModel.kt */
    /* loaded from: classes2.dex */
    public static final class c {
        public TokenType a;

        public c() {
            this(null, 1, null);
        }

        public c(TokenType tokenType) {
            fs1.f(tokenType, "tokenType");
            this.a = tokenType;
        }

        public final TokenType a() {
            return this.a;
        }

        public final void b(TokenType tokenType) {
            fs1.f(tokenType, "<set-?>");
            this.a = tokenType;
        }

        public boolean equals(Object obj) {
            if (this == obj) {
                return true;
            }
            return (obj instanceof c) && this.a == ((c) obj).a;
        }

        public int hashCode() {
            return this.a.hashCode();
        }

        public String toString() {
            return "DefaultParam(tokenType=" + this.a + ')';
        }

        public /* synthetic */ c(TokenType tokenType, int i, qi0 qi0Var) {
            this((i & 1) != 0 ? TokenType.BEP_20 : tokenType);
        }
    }

    /* compiled from: SwapViewModel.kt */
    /* loaded from: classes2.dex */
    public static final class d {
        public final BigInteger a;
        public final BigInteger b;
        public final String c;
        public final BigDecimal d;

        public d(BigInteger bigInteger, BigInteger bigInteger2, String str, BigDecimal bigDecimal) {
            fs1.f(bigInteger, "amount");
            fs1.f(bigInteger2, "tradeFee");
            fs1.f(str, "method");
            fs1.f(bigDecimal, "nativeBalance");
            this.a = bigInteger;
            this.b = bigInteger2;
            this.c = str;
            this.d = bigDecimal;
        }

        public final BigInteger a() {
            return this.a;
        }

        public final String b() {
            return this.c;
        }

        public final BigDecimal c() {
            return this.d;
        }

        public final BigInteger d() {
            return this.b;
        }

        public boolean equals(Object obj) {
            if (this == obj) {
                return true;
            }
            if (obj instanceof d) {
                d dVar = (d) obj;
                return fs1.b(this.a, dVar.a) && fs1.b(this.b, dVar.b) && fs1.b(this.c, dVar.c) && fs1.b(this.d, dVar.d);
            }
            return false;
        }

        public int hashCode() {
            return (((((this.a.hashCode() * 31) + this.b.hashCode()) * 31) + this.c.hashCode()) * 31) + this.d.hashCode();
        }

        public String toString() {
            return "GasLimit(amount=" + this.a + ", tradeFee=" + this.b + ", method=" + this.c + ", nativeBalance=" + this.d + ')';
        }
    }

    /* compiled from: SwapViewModel.kt */
    /* loaded from: classes2.dex */
    public static final class e {
        public final boolean a;
        public final String b;

        public e(boolean z, String str) {
            fs1.f(str, ThrowableDeserializer.PROP_NAME_MESSAGE);
            this.a = z;
            this.b = str;
        }

        public final String a() {
            return this.b;
        }

        public final boolean b() {
            return this.a;
        }

        public boolean equals(Object obj) {
            if (this == obj) {
                return true;
            }
            if (obj instanceof e) {
                e eVar = (e) obj;
                return this.a == eVar.a && fs1.b(this.b, eVar.b);
            }
            return false;
        }

        /* JADX WARN: Multi-variable type inference failed */
        /* JADX WARN: Type inference failed for: r0v1, types: [int] */
        /* JADX WARN: Type inference failed for: r0v4 */
        /* JADX WARN: Type inference failed for: r0v5 */
        public int hashCode() {
            boolean z = this.a;
            ?? r0 = z;
            if (z) {
                r0 = 1;
            }
            return (r0 * 31) + this.b.hashCode();
        }

        public String toString() {
            return "TransactionResult(isSuccess=" + this.a + ", message=" + this.b + ')';
        }
    }

    /* compiled from: SwapViewModel.kt */
    /* loaded from: classes2.dex */
    public static final class f extends ex3 {
        /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
        public f(SafeswapRouter safeswapRouter, Swap swap) {
            super(safeswapRouter, swap);
            fs1.e(safeswapRouter, "getMainRouter(GAS_LIMIT)");
            fs1.e(swap, "!!");
        }

        @Override // defpackage.ex3
        public SafeswapPair n(String str) {
            fs1.f(str, "pair");
            return SwapViewModel.this.n0(str);
        }
    }

    /* compiled from: SwapViewModel.kt */
    /* loaded from: classes2.dex */
    public static final class g extends CountDownTimer {
        public g(long j, long j2) {
            super(j, j2);
        }

        @Override // android.os.CountDownTimer
        public void onFinish() {
            if (fs1.b(SwapViewModel.this.T().getValue(), Boolean.FALSE)) {
                SwapViewModel.this.t0().postValue(Boolean.TRUE);
            }
        }

        @Override // android.os.CountDownTimer
        public void onTick(long j) {
            SwapViewModel.this.V().postValue(Long.valueOf(j));
        }
    }

    static {
        new b(null);
    }

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public SwapViewModel(Application application) {
        super(application);
        fs1.f(application, "application");
        this.b = application;
        U0();
        this.c = new gb2<>();
        this.d = new gb2<>(Double.valueOf(12.0d));
        this.e = new gb2<>(Gas.Standard);
        this.f = new gb2<>(20);
        this.g = new gb2<>();
        this.h = new gb2<>();
        this.i = new gb2<>();
        this.j = new gb2<>();
        this.k = new gb2<>();
        this.l = new gb2<>();
        this.m = new gb2<>("");
        this.n = new gb2<>();
        LoadingState loadingState = LoadingState.Normal;
        this.o = new gb2<>(loadingState);
        this.p = new gb2<>();
        this.q = new gb2<>();
        this.r = new gb2<>(0);
        this.s = new gb2<>(loadingState);
        this.t = new GasPrice();
        this.u = zy1.a(new SwapViewModel$privateKey$2(this));
        this.v = zy1.a(new SwapViewModel$address$2(this));
        this.w = zy1.a(new SwapViewModel$credentails$2(this));
        this.x = BigInteger.valueOf(9000000L);
        long millis = TimeUnit.SECONDS.toMillis(10L);
        this.F = millis;
        this.G = new gb2<>(Long.valueOf(millis));
        Boolean bool = Boolean.FALSE;
        this.H = new gb2<>(bool);
        this.K = new gb2<>(null);
        Double valueOf = Double.valueOf((double) Utils.DOUBLE_EPSILON);
        this.L = new gb2<>(valueOf);
        this.M = new gb2<>(valueOf);
        this.N = new gb2<>(bool);
        this.P = new HashMap<>();
        this.Q = new HashMap<>();
        this.T = new gb2<>(loadingState);
        this.U = new gb2<>();
        this.V = new gb2<>(new c(null, 1, null));
        this.W = b20.g();
        this.X = new gb2<>(b20.g());
    }

    public static final void c1(SwapViewModel swapViewModel, Swap swap) {
        fs1.f(swapViewModel, "this$0");
        if (swap != null) {
            swapViewModel.S0();
            swapViewModel.V0();
        }
    }

    public static final void d1(SwapViewModel swapViewModel, Swap swap) {
        fs1.f(swapViewModel, "this$0");
        if (swap != null) {
            swapViewModel.Q0();
            swapViewModel.V0();
        }
    }

    public static final void e1(SwapViewModel swapViewModel, Double d2) {
        fs1.f(swapViewModel, "this$0");
        swapViewModel.V0();
    }

    public static final void f1(SwapViewModel swapViewModel, Gas gas) {
        fs1.f(swapViewModel, "this$0");
        swapViewModel.V0();
    }

    public static /* synthetic */ void i1(SwapViewModel swapViewModel, BigDecimal bigDecimal, int i, Object obj) {
        if ((i & 1) != 0) {
            bigDecimal = null;
        }
        swapViewModel.h1(bigDecimal);
    }

    public final gb2<Swap> A0() {
        return this.g;
    }

    public final gb2<Double> B0() {
        return this.k;
    }

    public final void C0(Swap swap) {
        st1 b2;
        st1 st1Var = this.B;
        if (st1Var != null) {
            st1.a.a(st1Var, null, 1, null);
        }
        b2 = as.b(ej4.a(this), null, null, new SwapViewModel$getSourcePriceFromCMC$1(swap, this, null), 3, null);
        this.B = b2;
    }

    public final BigInteger D(BigInteger bigInteger) {
        fs1.f(bigInteger, "amountIn");
        Double value = this.d.getValue();
        fs1.d(value);
        fs1.e(value, "slipPage.value!!");
        double doubleValue = value.doubleValue();
        BigDecimal[] bigDecimalArr = {new BigDecimal(String.valueOf(1.0d)), new BigDecimal(String.valueOf(1.0d))};
        BigDecimal multiply = new BigDecimal(String.valueOf(doubleValue)).multiply(new BigDecimal(String.valueOf(100.0d)));
        fs1.e(multiply, "this.multiply(other)");
        BigDecimal[] bigDecimalArr2 = {multiply, new BigDecimal(String.valueOf(10000.0d))};
        BigDecimal multiply2 = bigDecimalArr2[0].multiply(bigDecimalArr[1]);
        fs1.e(multiply2, "this.multiply(other)");
        BigDecimal multiply3 = bigDecimalArr[0].multiply(bigDecimalArr2[1]);
        fs1.e(multiply3, "this.multiply(other)");
        BigDecimal add = multiply2.add(multiply3);
        fs1.e(add, "this.add(other)");
        BigDecimal multiply4 = bigDecimalArr[1].multiply(bigDecimalArr2[1]);
        fs1.e(multiply4, "this.multiply(other)");
        BigDecimal[] bigDecimalArr3 = {add, multiply4};
        BigDecimal multiply5 = bigDecimalArr3[0].multiply(new BigDecimal(bigInteger));
        fs1.e(multiply5, "outputamount[0].multiply(amountIn.toBigDecimal())");
        bigDecimalArr3[0] = multiply5;
        BigDecimal divide = bigDecimalArr3[0].divide(bigDecimalArr3[1], RoundingMode.HALF_EVEN);
        fs1.e(divide, "this.divide(other, RoundingMode.HALF_EVEN)");
        BigInteger bigInteger2 = divide.setScale(0, RoundingMode.DOWN).toBigInteger();
        fs1.e(bigInteger2, "outputamount[0].div(outp…Mode.DOWN).toBigInteger()");
        return bigInteger2;
    }

    public final String D0(int i) {
        String string = a().getString(i);
        fs1.e(string, "getApplication<Application>().getString(resId)");
        return string;
    }

    public final BigInteger E(BigInteger bigInteger) {
        fs1.f(bigInteger, "amountOut");
        Double value = this.d.getValue();
        fs1.d(value);
        fs1.e(value, "slipPage.value!!");
        double doubleValue = value.doubleValue();
        BigDecimal[] bigDecimalArr = {new BigDecimal(String.valueOf(1.0d)), new BigDecimal(String.valueOf(1.0d))};
        BigDecimal multiply = new BigDecimal(String.valueOf(doubleValue)).multiply(new BigDecimal(String.valueOf(100.0d)));
        fs1.e(multiply, "this.multiply(other)");
        BigDecimal[] bigDecimalArr2 = {multiply, new BigDecimal(String.valueOf(10000.0d))};
        BigDecimal multiply2 = bigDecimalArr2[0].multiply(bigDecimalArr[1]);
        fs1.e(multiply2, "this.multiply(other)");
        BigDecimal multiply3 = bigDecimalArr[0].multiply(bigDecimalArr2[1]);
        fs1.e(multiply3, "this.multiply(other)");
        BigDecimal add = multiply2.add(multiply3);
        fs1.e(add, "this.add(other)");
        BigDecimal multiply4 = bigDecimalArr[1].multiply(bigDecimalArr2[1]);
        fs1.e(multiply4, "this.multiply(other)");
        BigDecimal[] bigDecimalArr3 = {add, multiply4};
        BigDecimal[] bigDecimalArr4 = {bigDecimalArr3[1], bigDecimalArr3[0]};
        BigDecimal multiply5 = bigDecimalArr4[0].multiply(new BigDecimal(bigInteger));
        fs1.e(multiply5, "outputamount[0].multiply(amountOut.toBigDecimal())");
        bigDecimalArr4[0] = multiply5;
        BigDecimal divide = bigDecimalArr4[0].divide(bigDecimalArr4[1], RoundingMode.HALF_EVEN);
        fs1.e(divide, "this.divide(other, RoundingMode.HALF_EVEN)");
        BigInteger bigInteger2 = divide.setScale(0, RoundingMode.DOWN).toBigInteger();
        fs1.e(bigInteger2, "outputamount[0].div(outp…Mode.DOWN).toBigInteger()");
        return bigInteger2;
    }

    public final gb2<List<Swap>> E0() {
        return this.X;
    }

    public final void F() {
        this.s.postValue(LoadingState.Loading);
        as.b(ej4.a(this), null, null, new SwapViewModel$askForApproval$1(this, null), 3, null);
    }

    public final gb2<a> F0() {
        return this.c;
    }

    public final void G() {
        st1 st1Var = this.B;
        if (st1Var != null) {
            st1.a.a(st1Var, null, 1, null);
        }
        st1 st1Var2 = this.A;
        if (st1Var2 != null) {
            st1.a.a(st1Var2, null, 1, null);
        }
        st1 st1Var3 = this.D;
        if (st1Var3 != null) {
            st1.a.a(st1Var3, null, 1, null);
        }
        st1 st1Var4 = this.C;
        if (st1Var4 != null) {
            st1.a.a(st1Var4, null, 1, null);
        }
        st1 st1Var5 = this.E;
        if (st1Var5 != null) {
            st1.a.a(st1Var5, null, 1, null);
        }
        st1 st1Var6 = this.O;
        if (st1Var6 != null) {
            st1.a.a(st1Var6, null, 1, null);
        }
        this.c.postValue(null);
        this.i.postValue(BigDecimal.ZERO);
        this.j.postValue(BigDecimal.ZERO);
        gb2<Swap> gb2Var = this.g;
        gb2Var.postValue(gb2Var.getValue());
        gb2<Swap> gb2Var2 = this.h;
        gb2Var2.postValue(gb2Var2.getValue());
        this.s.postValue(LoadingState.Normal);
        this.r.postValue(0);
    }

    public final String G0() {
        Swap value = this.g.getValue();
        Integer num = value == null ? null : value.chainId;
        return (num != null && num.intValue() == 56) ? "0x524BC73fCb4fB70E2E84dC08EFE255252A3b026E" : "0x0000000000000000000000000000000000000000";
    }

    public final void H(String str) {
        st1 b2;
        st1 st1Var = this.J;
        if (st1Var != null) {
            st1.a.a(st1Var, null, 1, null);
        }
        b2 = as.b(ej4.a(this), null, null, new SwapViewModel$checkApproveStatus$1(this, str, null), 3, null);
        this.J = b2;
    }

    public final SafeswapTradeRouter H0(BigInteger bigInteger) {
        return SafeswapTradeRouter.u(G0(), n1(), W(), k0(bigInteger));
    }

    public final Object I(String str, q70<? super zw0> q70Var) {
        zw0 send = n1().ethGetTransactionReceipt(str).send();
        fs1.e(send, "web3().ethGetTransaction…t(transactionHash).send()");
        return send;
    }

    public final l84 I0() {
        return this.R;
    }

    public final void J(c cVar) {
        int i;
        fs1.f(cVar, "stl");
        ArrayList arrayList = new ArrayList();
        List<? extends Swap> list = this.W;
        ArrayList arrayList2 = new ArrayList();
        Iterator<T> it = list.iterator();
        while (true) {
            boolean z = true;
            if (!it.hasNext()) {
                break;
            }
            Object next = it.next();
            int chainId = cVar.a().getChainId();
            Integer num = ((Swap) next).chainId;
            if ((num == null || chainId != num.intValue()) ? false : false) {
                arrayList2.add(next);
            }
        }
        HashSet hashSet = new HashSet();
        ArrayList arrayList3 = new ArrayList();
        for (Object obj : arrayList2) {
            if (hashSet.add(((Swap) obj).address)) {
                arrayList3.add(obj);
            }
        }
        arrayList.addAll(arrayList3);
        ListIterator listIterator = arrayList.listIterator(arrayList.size());
        while (true) {
            if (listIterator.hasPrevious()) {
                if (fs1.b(((Swap) listIterator.previous()).symbol, "SFM")) {
                    i = listIterator.nextIndex();
                    break;
                }
            } else {
                i = -1;
                break;
            }
        }
        if (i > 2) {
            arrayList.add(1, (Swap) arrayList.remove(i));
        }
        this.X.postValue(arrayList);
    }

    public final gb2<String> J0() {
        return this.m;
    }

    public final void K(BigDecimal bigDecimal, up3<BigDecimal> up3Var) {
        fs1.f(bigDecimal, "amountIn");
        fs1.f(up3Var, "callBack");
        this.n.postValue(null);
        this.m.setValue("");
        st1 st1Var = this.O;
        if (st1Var != null) {
            zt1.e(st1Var, "New Job", null, 2, null);
        }
        k1();
        this.r.postValue(0);
        as.b(ej4.a(this), null, null, new SwapViewModel$findMaximumAmountInForNativeToken$1(this, bigDecimal, up3Var, null), 3, null);
    }

    public final gb2<d> K0() {
        return this.n;
    }

    public final String L() {
        return (String) this.v.getValue();
    }

    public final gb2<e> L0() {
        return this.U;
    }

    public final gb2<Double> M() {
        return this.L;
    }

    public final boolean M0() {
        Swap value = this.g.getValue();
        Integer num = value == null ? null : value.chainId;
        if (num != null && num.intValue() == 1) {
            return true;
        }
        return num != null && num.intValue() == 3;
    }

    public final gb2<Double> N() {
        return this.M;
    }

    public final String N0() {
        String str = M0() ? "ETH" : "BNB";
        Swap value = this.g.getValue();
        if (dv3.t(value == null ? null : value.symbol, str, true)) {
            return str;
        }
        return null;
    }

    public final gb2<LoadingState> O() {
        return this.s;
    }

    public final boolean O0(Swap swap) {
        return fs1.b(swap.symbol, "BNB") || fs1.b(swap.symbol, "ETH");
    }

    public final String P() {
        if (M0()) {
            return D0(R.string.swap_infura_approval);
        }
        return D0(R.string.swap_bsc_approval);
    }

    public final boolean P0() {
        return !M0();
    }

    public final gb2<Integer> Q() {
        return this.r;
    }

    public final void Q0() {
        Object obj = null;
        this.j.postValue(null);
        Swap value = this.h.getValue();
        fs1.d(value);
        fs1.e(value, "destinationLiveData.value!!");
        Swap swap = value;
        Iterator<T> it = PreFetchData.a.a().iterator();
        while (true) {
            if (!it.hasNext()) {
                break;
            }
            Object next = it.next();
            String a2 = ((rt) next).a();
            Objects.requireNonNull(a2, "null cannot be cast to non-null type java.lang.String");
            Locale locale = Locale.ROOT;
            String lowerCase = a2.toLowerCase(locale);
            fs1.e(lowerCase, "(this as java.lang.Strin….toLowerCase(Locale.ROOT)");
            String str = swap.address;
            fs1.e(str, "swap.address");
            String lowerCase2 = str.toLowerCase(locale);
            fs1.e(lowerCase2, "(this as java.lang.Strin….toLowerCase(Locale.ROOT)");
            if (fs1.b(lowerCase, lowerCase2)) {
                obj = next;
                break;
            }
        }
        rt rtVar = (rt) obj;
        if (rtVar != null) {
            double g2 = rtVar.g();
            Integer num = swap.decimals;
            fs1.e(num, "swap.decimals");
            a0().postValue(new BigDecimal(g2, new MathContext(num.intValue())));
        }
        String D = e30.D(swap);
        ko4 n1 = n1();
        ma0 W = W();
        BigInteger bigInteger = this.x;
        fs1.e(bigInteger, "GAS_LIMIT");
        ERC20 s = ERC20.s(D, n1, W, k0(bigInteger));
        fs1.e(s, "load(swap.getWrapAddress…etGasProvider(GAS_LIMIT))");
        this.z = s;
        R0();
    }

    public final ex3 R() {
        return this.S;
    }

    /* JADX WARN: Type inference failed for: r4v1, types: [T, java.lang.String] */
    /* JADX WARN: Type inference failed for: r4v4, types: [T, java.lang.String] */
    public final void R0() {
        st1 b2;
        st1 b3;
        Swap value = this.h.getValue();
        fs1.d(value);
        fs1.e(value, "destinationLiveData.value!!");
        Swap swap = value;
        st1 st1Var = this.C;
        if (st1Var != null) {
            st1.a.a(st1Var, null, 1, null);
        }
        b2 = as.b(ej4.a(this), null, null, new SwapViewModel$loadDestinationBalance$1(swap, this, null), 3, null);
        this.C = b2;
        Ref$ObjectRef ref$ObjectRef = new Ref$ObjectRef();
        ref$ObjectRef.element = swap.address;
        if (O0(swap)) {
            ref$ObjectRef.element = U();
        }
        st1 st1Var2 = this.D;
        if (st1Var2 != null) {
            st1.a.a(st1Var2, null, 1, null);
        }
        b3 = as.b(ej4.a(this), null, null, new SwapViewModel$loadDestinationBalance$2(ref$ObjectRef, this, swap, null), 3, null);
        this.D = b3;
    }

    /* JADX WARN: Code restructure failed: missing block: B:110:0x0267, code lost:
        if (defpackage.fs1.b(r3, r14) != false) goto L56;
     */
    /* JADX WARN: Code restructure failed: missing block: B:111:0x0269, code lost:
        r3 = true;
     */
    /* JADX WARN: Code restructure failed: missing block: B:98:0x022d, code lost:
        if (defpackage.fs1.b(r3, r13) == false) goto L62;
     */
    /* JADX WARN: Removed duplicated region for block: B:10:0x002b  */
    /* JADX WARN: Removed duplicated region for block: B:127:0x028d A[LOOP:2: B:125:0x0287->B:127:0x028d, LOOP_END] */
    /* JADX WARN: Removed duplicated region for block: B:131:0x02b1  */
    /* JADX WARN: Removed duplicated region for block: B:151:0x036f  */
    /* JADX WARN: Removed duplicated region for block: B:165:0x03a2  */
    /* JADX WARN: Removed duplicated region for block: B:170:0x03b0  */
    /* JADX WARN: Removed duplicated region for block: B:171:0x03f5  */
    /* JADX WARN: Removed duplicated region for block: B:175:0x0447  */
    /* JADX WARN: Removed duplicated region for block: B:181:0x0455  */
    /* JADX WARN: Removed duplicated region for block: B:183:0x045a  */
    /* JADX WARN: Removed duplicated region for block: B:196:0x011c A[EXC_TOP_SPLITTER, SYNTHETIC] */
    /* JADX WARN: Removed duplicated region for block: B:20:0x0065  */
    /* JADX WARN: Removed duplicated region for block: B:35:0x00fd  */
    /* JADX WARN: Removed duplicated region for block: B:38:0x0111  */
    /* JADX WARN: Removed duplicated region for block: B:49:0x015d  */
    /* JADX WARN: Removed duplicated region for block: B:52:0x016b  */
    /* JADX WARN: Removed duplicated region for block: B:62:0x018f  */
    /* JADX WARN: Removed duplicated region for block: B:68:0x01a6  */
    /*
        Code decompiled incorrectly, please refer to instructions dump.
        To view partially-correct code enable 'Show inconsistent code' option in preferences
    */
    public final java.lang.Object S(net.safemoon.androidwallet.viewmodels.SwapViewModel.a r20, defpackage.q70<? super defpackage.l84> r21) {
        /*
            Method dump skipped, instructions count: 1122
            To view this dump change 'Code comments level' option to 'DEBUG'
        */
        throw new UnsupportedOperationException("Method not decompiled: net.safemoon.androidwallet.viewmodels.SwapViewModel.S(net.safemoon.androidwallet.viewmodels.SwapViewModel$a, q70):java.lang.Object");
    }

    /* JADX WARN: Removed duplicated region for block: B:20:0x007c A[SYNTHETIC] */
    /*
        Code decompiled incorrectly, please refer to instructions dump.
        To view partially-correct code enable 'Show inconsistent code' option in preferences
    */
    public final void S0() {
        /*
            r11 = this;
            gb2<java.math.BigDecimal> r0 = r11.i
            r1 = 0
            r0.postValue(r1)
            gb2<net.safemoon.androidwallet.model.swap.Swap> r0 = r11.g
            java.lang.Object r0 = r0.getValue()
            defpackage.fs1.d(r0)
            java.lang.String r2 = "sourceLiveData.value!!"
            defpackage.fs1.e(r0, r2)
            net.safemoon.androidwallet.model.swap.Swap r0 = (net.safemoon.androidwallet.model.swap.Swap) r0
            net.safemoon.androidwallet.utils.PreFetchData r2 = net.safemoon.androidwallet.utils.PreFetchData.a
            java.util.ArrayList r2 = r2.a()
            java.util.Iterator r2 = r2.iterator()
        L20:
            boolean r3 = r2.hasNext()
            if (r3 == 0) goto L7d
            java.lang.Object r3 = r2.next()
            r4 = r3
            rt r4 = (defpackage.rt) r4
            java.lang.String r5 = r4.a()
            java.lang.String r6 = "null cannot be cast to non-null type java.lang.String"
            java.util.Objects.requireNonNull(r5, r6)
            java.util.Locale r7 = java.util.Locale.ROOT
            java.lang.String r5 = r5.toLowerCase(r7)
            java.lang.String r8 = "(this as java.lang.Strin….toLowerCase(Locale.ROOT)"
            defpackage.fs1.e(r5, r8)
            java.lang.String r9 = r0.address
            java.lang.String r10 = "swap.address"
            defpackage.fs1.e(r9, r10)
            java.lang.String r9 = r9.toLowerCase(r7)
            defpackage.fs1.e(r9, r8)
            boolean r5 = defpackage.fs1.b(r5, r9)
            if (r5 == 0) goto L79
            java.lang.String r4 = r4.h()
            java.util.Objects.requireNonNull(r4, r6)
            java.lang.String r4 = r4.toLowerCase(r7)
            defpackage.fs1.e(r4, r8)
            java.lang.String r5 = r0.symbol
            java.lang.String r6 = "swap.symbol"
            defpackage.fs1.e(r5, r6)
            java.lang.String r5 = r5.toLowerCase(r7)
            defpackage.fs1.e(r5, r8)
            boolean r4 = defpackage.fs1.b(r4, r5)
            if (r4 == 0) goto L79
            r4 = 1
            goto L7a
        L79:
            r4 = 0
        L7a:
            if (r4 == 0) goto L20
            r1 = r3
        L7d:
            rt r1 = (defpackage.rt) r1
            if (r1 != 0) goto L82
            goto La2
        L82:
            double r1 = r1.g()
            java.math.BigDecimal r3 = new java.math.BigDecimal
            java.math.MathContext r4 = new java.math.MathContext
            java.lang.Integer r5 = r0.decimals
            java.lang.String r6 = "swap.decimals"
            defpackage.fs1.e(r5, r6)
            int r5 = r5.intValue()
            r4.<init>(r5)
            r3.<init>(r1, r4)
            gb2 r1 = r11.z0()
            r1.postValue(r3)
        La2:
            java.lang.String r0 = defpackage.e30.D(r0)
            ko4 r1 = r11.n1()
            ma0 r2 = r11.W()
            java.math.BigInteger r3 = r11.x
            java.lang.String r4 = "GAS_LIMIT"
            defpackage.fs1.e(r3, r4)
            j80 r3 = r11.k0(r3)
            net.safemoon.androidwallet.ERC20 r0 = net.safemoon.androidwallet.ERC20.s(r0, r1, r2, r3)
            java.lang.String r1 = "load(swap.getWrapAddress…etGasProvider(GAS_LIMIT))"
            defpackage.fs1.e(r0, r1)
            r11.y = r0
            r11.T0()
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: net.safemoon.androidwallet.viewmodels.SwapViewModel.S0():void");
    }

    public final gb2<Boolean> T() {
        return this.N;
    }

    /* JADX WARN: Type inference failed for: r4v1, types: [T, java.lang.String] */
    /* JADX WARN: Type inference failed for: r4v4, types: [T, java.lang.String] */
    public final void T0() {
        st1 b2;
        st1 b3;
        Swap value = this.g.getValue();
        fs1.d(value);
        fs1.e(value, "sourceLiveData.value!!");
        Swap swap = value;
        st1 st1Var = this.A;
        if (st1Var != null) {
            st1.a.a(st1Var, null, 1, null);
        }
        b2 = as.b(ej4.a(this), null, null, new SwapViewModel$loadSourceBalance$1(swap, this, null), 3, null);
        this.A = b2;
        Ref$ObjectRef ref$ObjectRef = new Ref$ObjectRef();
        ref$ObjectRef.element = swap.address;
        if (O0(swap)) {
            ref$ObjectRef.element = U();
        }
        st1 st1Var2 = this.B;
        if (st1Var2 != null) {
            st1.a.a(st1Var2, null, 1, null);
        }
        b3 = as.b(ej4.a(this), null, null, new SwapViewModel$loadSourceBalance$2(ref$ObjectRef, this, swap, null), 3, null);
        this.B = b3;
    }

    public final String U() {
        c value = this.V.getValue();
        Object obj = null;
        if ((value == null ? null : value.a()) == TokenType.BEP_20) {
            Iterator<T> it = this.W.iterator();
            while (true) {
                if (!it.hasNext()) {
                    break;
                }
                Object next = it.next();
                if (fs1.b(((Swap) next).symbol, "WBNB")) {
                    obj = next;
                    break;
                }
            }
            Swap swap = (Swap) obj;
            if (swap == null) {
                return "";
            }
            String str = swap.address;
            fs1.e(str, "it.address");
            return str;
        }
        c value2 = this.V.getValue();
        if ((value2 == null ? null : value2.a()) == TokenType.ERC_20) {
            Iterator<T> it2 = this.W.iterator();
            while (true) {
                if (!it2.hasNext()) {
                    break;
                }
                Object next2 = it2.next();
                if (fs1.b(((Swap) next2).symbol, "WETH")) {
                    obj = next2;
                    break;
                }
            }
            Swap swap2 = (Swap) obj;
            if (swap2 == null) {
                return "";
            }
            String str2 = swap2.address;
            fs1.e(str2, "it.address");
            return str2;
        }
        return "";
    }

    public final void U0() {
        a4.k().m().n(new wu<AllSwapTokens>() { // from class: net.safemoon.androidwallet.viewmodels.SwapViewModel$loadTokens$1
            @Override // defpackage.wu
            public void a(b<AllSwapTokens> bVar, Throwable th) {
                fs1.f(bVar, "call");
                fs1.f(th, "t");
            }

            @Override // defpackage.wu
            public void b(b<AllSwapTokens> bVar, n<AllSwapTokens> nVar) {
                fs1.f(bVar, "call");
                fs1.f(nVar, "response");
                if (!nVar.e() || nVar.a() == null) {
                    return;
                }
                as.b(ej4.a(SwapViewModel.this), null, null, new SwapViewModel$loadTokens$1$onResponse$1(SwapViewModel.this, nVar, null), 3, null);
            }
        });
    }

    public final gb2<Long> V() {
        return this.G;
    }

    public final void V0() {
        this.n.postValue(null);
        this.m.setValue("");
        this.N.setValue(Boolean.TRUE);
        st1 st1Var = this.O;
        if (st1Var != null) {
            zt1.e(st1Var, "New Job", null, 2, null);
        }
        k1();
        a value = this.c.getValue();
        if (value == null) {
            return;
        }
        if (value.a().compareTo(BigDecimal.ZERO) > 0) {
            Q().postValue(0);
            if (value.b()) {
                y0(F0().getValue());
                return;
            } else {
                x0(F0().getValue());
                return;
            }
        }
        M().postValue(Double.valueOf((double) Utils.DOUBLE_EPSILON));
        N().postValue(Double.valueOf((double) Utils.DOUBLE_EPSILON));
        T().setValue(Boolean.FALSE);
    }

    public final ma0 W() {
        return (ma0) this.w.getValue();
    }

    public final void W0() {
        T0();
        R0();
    }

    public final BigInteger X() {
        Integer value = this.f.getValue();
        fs1.d(value);
        BigInteger valueOf = BigInteger.valueOf((System.currentTimeMillis() / 1000) + (value.intValue() * 60));
        fs1.e(valueOf, "BigInteger.valueOf(this)");
        return valueOf;
    }

    public final void X0() {
        this.P.clear();
        this.Q.clear();
    }

    public final gb2<Integer> Y() {
        return this.f;
    }

    public final void Y0(String str) {
        this.U.setValue(new e(true, str));
        gb2<a> gb2Var = this.c;
        BigDecimal bigDecimal = BigDecimal.ZERO;
        fs1.e(bigDecimal, "ZERO");
        gb2Var.setValue(new a(true, bigDecimal));
    }

    public final gb2<c> Z() {
        return this.V;
    }

    public final void Z0(ck4 ck4Var, ck4 ck4Var2) {
        fs1.f(ck4Var, "bindingSource");
        fs1.f(ck4Var2, "bindingDestination");
        try {
            st1 st1Var = this.E;
            if (st1Var != null) {
                st1.a.a(st1Var, null, 1, null);
            }
            this.c.postValue(null);
            ck4Var.j.setText("");
            ck4Var2.j.setText("");
        } catch (Exception unused) {
        }
        gb2<Swap> gb2Var = this.h;
        Swap value = this.g.getValue();
        A0().setValue(b0().getValue());
        te4 te4Var = te4.a;
        gb2Var.setValue(value);
    }

    public final gb2<BigDecimal> a0() {
        return this.j;
    }

    public final void a1(ex3 ex3Var) {
        this.S = ex3Var;
    }

    public final gb2<Swap> b0() {
        return this.h;
    }

    public final void b1(rz1 rz1Var) {
        fs1.f(rz1Var, "lifecycle");
        this.d.postValue(Double.valueOf(12.0d));
        this.e.postValue(Gas.Standard);
        this.f.postValue(20);
        this.g = new gb2<>();
        this.h = new gb2<>();
        this.n.postValue(null);
        this.m.setValue("");
        this.g.observe(rz1Var, new tl2() { // from class: v14
            @Override // defpackage.tl2
            public final void onChanged(Object obj) {
                SwapViewModel.c1(SwapViewModel.this, (Swap) obj);
            }
        });
        this.h.observe(rz1Var, new tl2() { // from class: w14
            @Override // defpackage.tl2
            public final void onChanged(Object obj) {
                SwapViewModel.d1(SwapViewModel.this, (Swap) obj);
            }
        });
        LiveData a2 = cb4.a(this.d);
        fs1.e(a2, "Transformations.distinctUntilChanged(this)");
        a2.observe(rz1Var, new tl2() { // from class: t14
            @Override // defpackage.tl2
            public final void onChanged(Object obj) {
                SwapViewModel.e1(SwapViewModel.this, (Double) obj);
            }
        });
        LiveData a3 = cb4.a(this.e);
        fs1.e(a3, "Transformations.distinctUntilChanged(this)");
        a3.observe(rz1Var, new tl2() { // from class: u14
            @Override // defpackage.tl2
            public final void onChanged(Object obj) {
                SwapViewModel.f1(SwapViewModel.this, (Gas) obj);
            }
        });
    }

    public final gb2<Double> c0() {
        return this.l;
    }

    public final void d0(Swap swap) {
        st1 b2;
        st1 st1Var = this.D;
        if (st1Var != null) {
            st1.a.a(st1Var, null, 1, null);
        }
        b2 = as.b(ej4.a(this), null, null, new SwapViewModel$getDestinationPriceFromCMC$1(swap, this, null), 3, null);
        this.D = b2;
    }

    public final void e0() {
        st1 b2;
        st1 st1Var = this.E;
        if (st1Var != null) {
            st1.a.a(st1Var, null, 1, null);
        }
        b2 = as.b(ej4.a(this), null, null, new SwapViewModel$getEstimateGas$1(this, null), 3, null);
        this.E = b2;
    }

    public final String f0() {
        Swap value = this.g.getValue();
        Integer num = value == null ? null : value.chainId;
        return (num != null && num.intValue() == 3) ? "0xDfD8bbA37423950bD8050C65E698610C57E55cea" : (num != null && num.intValue() == 97) ? "0xa7cc5b4cb33bc630e934Be7C140A289dEd2B23e8" : (num != null && num.intValue() == 1) ? "0x9Cf2f35E3656D4C68474525d67D9459Da3A000CD" : (num != null && num.intValue() == 56) ? "0x505751023083BfC7D2AFB0588717dCC6182e54B2" : "";
    }

    public final gb2<LoadingState> g0() {
        return this.o;
    }

    public final void g1(l84 l84Var) {
        this.R = l84Var;
    }

    public final gb2<BigDecimal> h0() {
        return this.p;
    }

    public final void h1(BigDecimal bigDecimal) {
        double doubleValue;
        if (bigDecimal != null) {
            gb2<String> gb2Var = this.K;
            StringBuilder sb = new StringBuilder();
            sb.append(D0(R.string.swap_max_fee));
            sb.append(' ');
            sb.append(e30.p(bigDecimal.doubleValue(), 8, RoundingMode.HALF_UP, false, 4, null));
            sb.append(M0() ? " ETH" : " BNB");
            gb2Var.postValue(sb.toString());
        } else {
            this.K.postValue(null);
        }
        Swap value = this.g.getValue();
        Swap value2 = this.h.getValue();
        if (value != null) {
            try {
                a value3 = this.c.getValue();
                fs1.d(value3);
                if (value3.b()) {
                    a value4 = this.c.getValue();
                    fs1.d(value4);
                    doubleValue = value4.a().doubleValue();
                } else {
                    Double value5 = this.M.getValue();
                    fs1.d(value5);
                    fs1.e(value5, "{\n                    am…value!!\n                }");
                    doubleValue = value5.doubleValue();
                }
                BigDecimal value6 = this.i.getValue();
                fs1.d(value6);
                if (doubleValue > value6.doubleValue()) {
                    this.m.postValue(D0(R.string.swap_amount_less_available_balance));
                    return;
                }
                throw new Exception("For Throw to show bottom msg");
            } catch (Exception unused) {
                Double value7 = this.d.getValue();
                if (value7 == null) {
                    value7 = Double.valueOf((double) Utils.DOUBLE_EPSILON);
                }
                if (value7.doubleValue() < e30.B(qc4.a(value, value2))) {
                    this.m.postValue(D0(R.string.swap_pls_increase_slippage));
                    return;
                }
                String str = M0() ? "ETH" : "BNB";
                if (N0() != null) {
                    gb2<String> gb2Var2 = this.m;
                    gb2Var2.postValue(D0(R.string.swap_transactionFee_insufficent1) + str + D0(R.string.swap_transactionFee_insufficent2));
                    return;
                }
                gb2<String> gb2Var3 = this.m;
                gb2Var3.postValue(D0(R.string.swap_transactionFee_not_enough1) + str + D0(R.string.swap_transactionFee_not_enough2));
            }
        }
    }

    public final gb2<Gas> i0() {
        return this.e;
    }

    public final BigInteger j0() {
        Swap value = this.g.getValue();
        if (value == null) {
            return BigInteger.ZERO;
        }
        if (M0()) {
            GasPrice gasPrice = this.t;
            Gas value2 = i0().getValue();
            fs1.d(value2);
            fs1.e(value2, "gas.value!!");
            Integer num = value.chainId;
            fs1.e(num, "it.chainId");
            return new BigDecimal(gasPrice.getPrice(value2, num.intValue())).multiply(BigDecimal.TEN.pow(9)).toBigInteger();
        }
        GasPrice gasPrice2 = this.t;
        Gas value3 = i0().getValue();
        fs1.d(value3);
        fs1.e(value3, "gas.value!!");
        Integer num2 = value.chainId;
        fs1.e(num2, "it.chainId");
        return new BigDecimal(gasPrice2.getPrice(value3, num2.intValue())).multiply(BigDecimal.TEN.pow(9)).toBigInteger();
    }

    public final void j1() {
        this.H.postValue(Boolean.FALSE);
        CountDownTimer countDownTimer = this.I;
        if (countDownTimer != null) {
            countDownTimer.cancel();
        }
        this.G.postValue(Long.valueOf(this.F));
        g gVar = new g(this.F, TimeUnit.SECONDS.toMillis(1L));
        gVar.start();
        te4 te4Var = te4.a;
        this.I = gVar;
    }

    public final j80 k0(BigInteger bigInteger) {
        return new it3(j0(), bigInteger);
    }

    public final void k1() {
        CountDownTimer countDownTimer = this.I;
        if (countDownTimer == null) {
            return;
        }
        countDownTimer.cancel();
    }

    public final gb2<LoadingState> l0() {
        return this.T;
    }

    public final void l1() {
        as.b(ej4.a(this), null, null, new SwapViewModel$swapTokens$1(this, null), 3, null);
    }

    public final SafeswapFactory m0() {
        String f0 = f0();
        ko4 n1 = n1();
        ma0 W = W();
        BigInteger bigInteger = this.x;
        fs1.e(bigInteger, "GAS_LIMIT");
        return SafeswapFactory.p(f0, n1, W, k0(bigInteger));
    }

    public final void m1() {
        gb2<BigDecimal> gb2Var = this.i;
        gb2Var.postValue(gb2Var.getValue());
        gb2<BigDecimal> gb2Var2 = this.j;
        gb2Var2.postValue(gb2Var2.getValue());
    }

    public final SafeswapPair n0(String str) {
        ko4 n1 = n1();
        ma0 W = W();
        BigInteger bigInteger = this.x;
        fs1.e(bigInteger, "GAS_LIMIT");
        return SafeswapPair.p(str, n1, W, k0(bigInteger));
    }

    public final ko4 n1() {
        return jo4.a(new ml1(r0()));
    }

    public final SafeswapRouter o0(BigInteger bigInteger) {
        return SafeswapRouter.q(u0(), n1(), W(), k0(bigInteger));
    }

    public final BigInteger p0() {
        BigInteger transactionCount = n1().ethGetTransactionCount(W().getAddress(), DefaultBlockParameterName.PENDING).send().getTransactionCount();
        fs1.e(transactionCount, "ethGetTransactionCount.transactionCount");
        return transactionCount;
    }

    public final String q0() {
        return (String) this.u.getValue();
    }

    public final String r0() {
        Integer num;
        String z;
        Swap value = this.g.getValue();
        return (value == null || (num = value.chainId) == null || (z = e30.z(num.intValue())) == null) ? "https://noRPC" : z;
    }

    public final gb2<String> s0() {
        return this.K;
    }

    public final gb2<Boolean> t0() {
        return this.H;
    }

    public final String u0() {
        Swap value = this.g.getValue();
        Integer num = value == null ? null : value.chainId;
        return (num != null && num.intValue() == 3) ? "0x713702D3fb45BC9765d3A00e0B37c33f9CE9Ec91" : (num != null && num.intValue() == 97) ? "0x73422d047085CC52c79C6182c67Abb275F72B9a2" : (num != null && num.intValue() == 1) ? "0x1fdD76e18dD21046b7e7D54C8254Bf08B239e4D9" : (num != null && num.intValue() == 56) ? "0x6ac68913d8fccd52d196b09e6bc0205735a4be5f" : "";
    }

    public final gb2<BigDecimal> v0() {
        return this.q;
    }

    public final gb2<Double> w0() {
        return this.d;
    }

    public final void x0(a aVar) {
        st1 b2;
        b2 = as.b(ej4.a(this), null, null, new SwapViewModel$getSlippageAmountIn$1(this, aVar, null), 3, null);
        this.O = b2;
    }

    public final void y0(a aVar) {
        st1 b2;
        b2 = as.b(ej4.a(this), null, null, new SwapViewModel$getSlippageAmountOut$1(this, aVar, null), 3, null);
        this.O = b2;
    }

    public final gb2<BigDecimal> z0() {
        return this.i;
    }
}
