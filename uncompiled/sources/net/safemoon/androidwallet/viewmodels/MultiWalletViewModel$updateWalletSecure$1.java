package net.safemoon.androidwallet.viewmodels;

import kotlin.coroutines.jvm.internal.SuspendLambda;
import kotlin.coroutines.jvm.internal.a;
import kotlin.jvm.internal.Ref$ObjectRef;
import kotlinx.coroutines.CoroutineDispatcher;
import net.safemoon.androidwallet.model.wallets.Wallet;
import net.safemoon.androidwallet.repository.WalletDataSource;

/* compiled from: MultiWalletViewModel.kt */
@a(c = "net.safemoon.androidwallet.viewmodels.MultiWalletViewModel$updateWalletSecure$1", f = "MultiWalletViewModel.kt", l = {156}, m = "invokeSuspend")
/* loaded from: classes2.dex */
public final class MultiWalletViewModel$updateWalletSecure$1 extends SuspendLambda implements hd1<c90, q70<? super te4>, Object> {
    public final /* synthetic */ rc1<te4> $callBack;
    public final /* synthetic */ Ref$ObjectRef<String> $encryptedMnemonic;
    public final /* synthetic */ String $encryptedPrivateKey;
    public final /* synthetic */ int $order;
    public final /* synthetic */ String $u5B64;
    public final /* synthetic */ Wallet $wallet;
    public int label;
    public final /* synthetic */ MultiWalletViewModel this$0;

    /* compiled from: MultiWalletViewModel.kt */
    @a(c = "net.safemoon.androidwallet.viewmodels.MultiWalletViewModel$updateWalletSecure$1$1", f = "MultiWalletViewModel.kt", l = {159}, m = "invokeSuspend")
    /* renamed from: net.safemoon.androidwallet.viewmodels.MultiWalletViewModel$updateWalletSecure$1$1  reason: invalid class name */
    /* loaded from: classes2.dex */
    public static final class AnonymousClass1 extends SuspendLambda implements hd1<c90, q70<? super te4>, Object> {
        public final /* synthetic */ rc1<te4> $callBack;
        public final /* synthetic */ Ref$ObjectRef<String> $encryptedMnemonic;
        public final /* synthetic */ String $encryptedPrivateKey;
        public final /* synthetic */ int $order;
        public final /* synthetic */ String $u5B64;
        public final /* synthetic */ Wallet $wallet;
        public Object L$0;
        public Object L$1;
        public int label;
        public final /* synthetic */ MultiWalletViewModel this$0;

        /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
        public AnonymousClass1(Wallet wallet2, MultiWalletViewModel multiWalletViewModel, String str, Ref$ObjectRef<String> ref$ObjectRef, String str2, int i, rc1<te4> rc1Var, q70<? super AnonymousClass1> q70Var) {
            super(2, q70Var);
            this.$wallet = wallet2;
            this.this$0 = multiWalletViewModel;
            this.$encryptedPrivateKey = str;
            this.$encryptedMnemonic = ref$ObjectRef;
            this.$u5B64 = str2;
            this.$order = i;
            this.$callBack = rc1Var;
        }

        @Override // kotlin.coroutines.jvm.internal.BaseContinuationImpl
        public final q70<te4> create(Object obj, q70<?> q70Var) {
            return new AnonymousClass1(this.$wallet, this.this$0, this.$encryptedPrivateKey, this.$encryptedMnemonic, this.$u5B64, this.$order, this.$callBack, q70Var);
        }

        @Override // defpackage.hd1
        public final Object invoke(c90 c90Var, q70<? super te4> q70Var) {
            return ((AnonymousClass1) create(c90Var, q70Var)).invokeSuspend(te4.a);
        }

        @Override // kotlin.coroutines.jvm.internal.BaseContinuationImpl
        public final Object invokeSuspend(Object obj) {
            Long id;
            WalletDataSource s;
            MultiWalletViewModel multiWalletViewModel;
            rc1<te4> rc1Var;
            Object d = gs1.d();
            int i = this.label;
            if (i == 0) {
                o83.b(obj);
                Wallet wallet2 = this.$wallet;
                if (wallet2 != null && (id = wallet2.getId()) != null) {
                    Wallet wallet3 = this.$wallet;
                    MultiWalletViewModel multiWalletViewModel2 = this.this$0;
                    String str = this.$encryptedPrivateKey;
                    Ref$ObjectRef<String> ref$ObjectRef = this.$encryptedMnemonic;
                    String str2 = this.$u5B64;
                    int i2 = this.$order;
                    rc1<te4> rc1Var2 = this.$callBack;
                    long longValue = id.longValue();
                    String D = dv3.D(dv3.D(wallet3.getName(), "|", "", false, 4, null), ",", "", false, 4, null);
                    s = multiWalletViewModel2.s();
                    fs1.e(str, "encryptedPrivateKey");
                    String str3 = ref$ObjectRef.element;
                    fs1.e(str2, "u5B64");
                    this.L$0 = multiWalletViewModel2;
                    this.L$1 = rc1Var2;
                    this.label = 1;
                    if (s.m(longValue, str, str3, str3, str2, i2, D, this) == d) {
                        return d;
                    }
                    multiWalletViewModel = multiWalletViewModel2;
                    rc1Var = rc1Var2;
                }
                return te4.a;
            } else if (i != 1) {
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            } else {
                rc1Var = (rc1) this.L$1;
                multiWalletViewModel = (MultiWalletViewModel) this.L$0;
                o83.b(obj);
            }
            as.b(ej4.a(multiWalletViewModel), null, null, new MultiWalletViewModel$updateWalletSecure$1$1$1$1(rc1Var, null), 3, null);
            return te4.a;
        }
    }

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public MultiWalletViewModel$updateWalletSecure$1(Wallet wallet2, MultiWalletViewModel multiWalletViewModel, String str, Ref$ObjectRef<String> ref$ObjectRef, String str2, int i, rc1<te4> rc1Var, q70<? super MultiWalletViewModel$updateWalletSecure$1> q70Var) {
        super(2, q70Var);
        this.$wallet = wallet2;
        this.this$0 = multiWalletViewModel;
        this.$encryptedPrivateKey = str;
        this.$encryptedMnemonic = ref$ObjectRef;
        this.$u5B64 = str2;
        this.$order = i;
        this.$callBack = rc1Var;
    }

    @Override // kotlin.coroutines.jvm.internal.BaseContinuationImpl
    public final q70<te4> create(Object obj, q70<?> q70Var) {
        return new MultiWalletViewModel$updateWalletSecure$1(this.$wallet, this.this$0, this.$encryptedPrivateKey, this.$encryptedMnemonic, this.$u5B64, this.$order, this.$callBack, q70Var);
    }

    @Override // defpackage.hd1
    public final Object invoke(c90 c90Var, q70<? super te4> q70Var) {
        return ((MultiWalletViewModel$updateWalletSecure$1) create(c90Var, q70Var)).invokeSuspend(te4.a);
    }

    @Override // kotlin.coroutines.jvm.internal.BaseContinuationImpl
    public final Object invokeSuspend(Object obj) {
        Object d = gs1.d();
        int i = this.label;
        if (i == 0) {
            o83.b(obj);
            CoroutineDispatcher b = tp0.b();
            AnonymousClass1 anonymousClass1 = new AnonymousClass1(this.$wallet, this.this$0, this.$encryptedPrivateKey, this.$encryptedMnemonic, this.$u5B64, this.$order, this.$callBack, null);
            this.label = 1;
            if (kotlinx.coroutines.a.e(b, anonymousClass1, this) == d) {
                return d;
            }
        } else if (i != 1) {
            throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        } else {
            o83.b(obj);
        }
        return te4.a;
    }
}
