package net.safemoon.androidwallet.viewmodels;

import kotlin.coroutines.jvm.internal.SuspendLambda;
import kotlin.coroutines.jvm.internal.a;
import kotlin.jvm.internal.Ref$ObjectRef;
import kotlinx.coroutines.CoroutineDispatcher;
import net.safemoon.androidwallet.model.swap.Swap;
import net.safemoon.androidwallet.model.tokensInfo.CurrencyTokenInfoResult;
import retrofit2.KotlinExtensions;
import retrofit2.b;
import retrofit2.n;

/* compiled from: SwapViewModel.kt */
@a(c = "net.safemoon.androidwallet.viewmodels.SwapViewModel$loadDestinationBalance$2", f = "SwapViewModel.kt", l = {459}, m = "invokeSuspend")
/* loaded from: classes2.dex */
public final class SwapViewModel$loadDestinationBalance$2 extends SuspendLambda implements hd1<c90, q70<? super te4>, Object> {
    public final /* synthetic */ Swap $swap;
    public final /* synthetic */ Ref$ObjectRef<String> $tokenAddress;
    public int label;
    public final /* synthetic */ SwapViewModel this$0;

    /* compiled from: SwapViewModel.kt */
    @kotlin.coroutines.jvm.internal.a(c = "net.safemoon.androidwallet.viewmodels.SwapViewModel$loadDestinationBalance$2$1", f = "SwapViewModel.kt", l = {2166}, m = "invokeSuspend")
    /* renamed from: net.safemoon.androidwallet.viewmodels.SwapViewModel$loadDestinationBalance$2$1  reason: invalid class name */
    /* loaded from: classes2.dex */
    public static final class AnonymousClass1 extends SuspendLambda implements hd1<c90, q70<? super te4>, Object> {
        public final /* synthetic */ Swap $swap;
        public final /* synthetic */ Ref$ObjectRef<String> $tokenAddress;
        public int label;
        public final /* synthetic */ SwapViewModel this$0;

        /* compiled from: SwapViewModel.kt */
        @kotlin.coroutines.jvm.internal.a(c = "net.safemoon.androidwallet.viewmodels.SwapViewModel$loadDestinationBalance$2$1$1", f = "SwapViewModel.kt", l = {463, 461}, m = "invokeSuspend")
        /* renamed from: net.safemoon.androidwallet.viewmodels.SwapViewModel$loadDestinationBalance$2$1$1  reason: invalid class name and collision with other inner class name */
        /* loaded from: classes2.dex */
        public static final class C02481 extends SuspendLambda implements hd1<k71<? super CurrencyTokenInfoResult>, q70<? super te4>, Object> {
            public final /* synthetic */ Ref$ObjectRef<String> $tokenAddress;
            private /* synthetic */ Object L$0;
            public int label;

            /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
            public C02481(Ref$ObjectRef<String> ref$ObjectRef, q70<? super C02481> q70Var) {
                super(2, q70Var);
                this.$tokenAddress = ref$ObjectRef;
            }

            @Override // kotlin.coroutines.jvm.internal.BaseContinuationImpl
            public final q70<te4> create(Object obj, q70<?> q70Var) {
                C02481 c02481 = new C02481(this.$tokenAddress, q70Var);
                c02481.L$0 = obj;
                return c02481;
            }

            @Override // defpackage.hd1
            public final Object invoke(k71<? super CurrencyTokenInfoResult> k71Var, q70<? super te4> q70Var) {
                return ((C02481) create(k71Var, q70Var)).invokeSuspend(te4.a);
            }

            @Override // kotlin.coroutines.jvm.internal.BaseContinuationImpl
            public final Object invokeSuspend(Object obj) {
                k71 k71Var;
                Object d = gs1.d();
                int i = this.label;
                if (i == 0) {
                    o83.b(obj);
                    k71Var = (k71) this.L$0;
                    e42 k = a4.k();
                    String str = this.$tokenAddress.element;
                    fs1.e(str, "tokenAddress");
                    b30 b30Var = b30.a;
                    String str2 = this.$tokenAddress.element;
                    fs1.e(str2, "tokenAddress");
                    b<CurrencyTokenInfoResult> p = k.p(str, b30Var.m(str2));
                    this.L$0 = k71Var;
                    this.label = 1;
                    obj = KotlinExtensions.c(p, this);
                    if (obj == d) {
                        return d;
                    }
                } else if (i != 1) {
                    if (i != 2) {
                        throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
                    }
                    o83.b(obj);
                    return te4.a;
                } else {
                    k71Var = (k71) this.L$0;
                    o83.b(obj);
                }
                Object a = ((n) obj).a();
                this.L$0 = null;
                this.label = 2;
                if (k71Var.emit(a, this) == d) {
                    return d;
                }
                return te4.a;
            }
        }

        /* compiled from: SwapViewModel.kt */
        @kotlin.coroutines.jvm.internal.a(c = "net.safemoon.androidwallet.viewmodels.SwapViewModel$loadDestinationBalance$2$1$2", f = "SwapViewModel.kt", l = {}, m = "invokeSuspend")
        /* renamed from: net.safemoon.androidwallet.viewmodels.SwapViewModel$loadDestinationBalance$2$1$2  reason: invalid class name */
        /* loaded from: classes2.dex */
        public static final class AnonymousClass2 extends SuspendLambda implements kd1<k71<? super CurrencyTokenInfoResult>, Throwable, q70<? super te4>, Object> {
            public /* synthetic */ Object L$0;
            public int label;

            public AnonymousClass2(q70<? super AnonymousClass2> q70Var) {
                super(3, q70Var);
            }

            @Override // defpackage.kd1
            public final Object invoke(k71<? super CurrencyTokenInfoResult> k71Var, Throwable th, q70<? super te4> q70Var) {
                AnonymousClass2 anonymousClass2 = new AnonymousClass2(q70Var);
                anonymousClass2.L$0 = th;
                return anonymousClass2.invokeSuspend(te4.a);
            }

            @Override // kotlin.coroutines.jvm.internal.BaseContinuationImpl
            public final Object invokeSuspend(Object obj) {
                gs1.d();
                if (this.label == 0) {
                    o83.b(obj);
                    ((Throwable) this.L$0).printStackTrace();
                    return te4.a;
                }
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
        }

        /* compiled from: Collect.kt */
        /* renamed from: net.safemoon.androidwallet.viewmodels.SwapViewModel$loadDestinationBalance$2$1$a */
        /* loaded from: classes2.dex */
        public static final class a implements k71<CurrencyTokenInfoResult> {
            public final /* synthetic */ SwapViewModel a;
            public final /* synthetic */ Swap f0;

            public a(SwapViewModel swapViewModel, Swap swap) {
                this.a = swapViewModel;
                this.f0 = swap;
            }

            @Override // defpackage.k71
            public Object emit(CurrencyTokenInfoResult currencyTokenInfoResult, q70<? super te4> q70Var) {
                st1 b;
                b = as.b(ej4.a(this.a), null, null, new SwapViewModel$loadDestinationBalance$2$1$3$1(this.a, currencyTokenInfoResult, this.f0, null), 3, null);
                return b == gs1.d() ? b : te4.a;
            }
        }

        /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
        public AnonymousClass1(Ref$ObjectRef<String> ref$ObjectRef, SwapViewModel swapViewModel, Swap swap, q70<? super AnonymousClass1> q70Var) {
            super(2, q70Var);
            this.$tokenAddress = ref$ObjectRef;
            this.this$0 = swapViewModel;
            this.$swap = swap;
        }

        @Override // kotlin.coroutines.jvm.internal.BaseContinuationImpl
        public final q70<te4> create(Object obj, q70<?> q70Var) {
            return new AnonymousClass1(this.$tokenAddress, this.this$0, this.$swap, q70Var);
        }

        @Override // defpackage.hd1
        public final Object invoke(c90 c90Var, q70<? super te4> q70Var) {
            return ((AnonymousClass1) create(c90Var, q70Var)).invokeSuspend(te4.a);
        }

        @Override // kotlin.coroutines.jvm.internal.BaseContinuationImpl
        public final Object invokeSuspend(Object obj) {
            Object d = gs1.d();
            int i = this.label;
            if (i == 0) {
                o83.b(obj);
                j71 c = n71.c(n71.n(new C02481(this.$tokenAddress, null)), new AnonymousClass2(null));
                a aVar = new a(this.this$0, this.$swap);
                this.label = 1;
                if (c.a(aVar, this) == d) {
                    return d;
                }
            } else if (i != 1) {
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            } else {
                o83.b(obj);
            }
            return te4.a;
        }
    }

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public SwapViewModel$loadDestinationBalance$2(Ref$ObjectRef<String> ref$ObjectRef, SwapViewModel swapViewModel, Swap swap, q70<? super SwapViewModel$loadDestinationBalance$2> q70Var) {
        super(2, q70Var);
        this.$tokenAddress = ref$ObjectRef;
        this.this$0 = swapViewModel;
        this.$swap = swap;
    }

    @Override // kotlin.coroutines.jvm.internal.BaseContinuationImpl
    public final q70<te4> create(Object obj, q70<?> q70Var) {
        return new SwapViewModel$loadDestinationBalance$2(this.$tokenAddress, this.this$0, this.$swap, q70Var);
    }

    @Override // defpackage.hd1
    public final Object invoke(c90 c90Var, q70<? super te4> q70Var) {
        return ((SwapViewModel$loadDestinationBalance$2) create(c90Var, q70Var)).invokeSuspend(te4.a);
    }

    @Override // kotlin.coroutines.jvm.internal.BaseContinuationImpl
    public final Object invokeSuspend(Object obj) {
        Object d = gs1.d();
        int i = this.label;
        if (i == 0) {
            o83.b(obj);
            CoroutineDispatcher b = tp0.b();
            AnonymousClass1 anonymousClass1 = new AnonymousClass1(this.$tokenAddress, this.this$0, this.$swap, null);
            this.label = 1;
            if (kotlinx.coroutines.a.e(b, anonymousClass1, this) == d) {
                return d;
            }
        } else if (i != 1) {
            throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        } else {
            o83.b(obj);
        }
        return te4.a;
    }
}
