package net.safemoon.androidwallet.viewmodels;

import kotlin.coroutines.jvm.internal.SuspendLambda;
import kotlin.coroutines.jvm.internal.a;

/* compiled from: ReflectionTrackerViewModel.kt */
@a(c = "net.safemoon.androidwallet.viewmodels.ReflectionTrackerViewModel$removeAndSeed$1", f = "ReflectionTrackerViewModel.kt", l = {262, 263, 266}, m = "invokeSuspend")
/* loaded from: classes2.dex */
public final class ReflectionTrackerViewModel$removeAndSeed$1 extends SuspendLambda implements hd1<c90, q70<? super te4>, Object> {
    public final /* synthetic */ String $symbolWithType;
    public int label;
    public final /* synthetic */ ReflectionTrackerViewModel this$0;

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public ReflectionTrackerViewModel$removeAndSeed$1(ReflectionTrackerViewModel reflectionTrackerViewModel, String str, q70<? super ReflectionTrackerViewModel$removeAndSeed$1> q70Var) {
        super(2, q70Var);
        this.this$0 = reflectionTrackerViewModel;
        this.$symbolWithType = str;
    }

    @Override // kotlin.coroutines.jvm.internal.BaseContinuationImpl
    public final q70<te4> create(Object obj, q70<?> q70Var) {
        return new ReflectionTrackerViewModel$removeAndSeed$1(this.this$0, this.$symbolWithType, q70Var);
    }

    @Override // defpackage.hd1
    public final Object invoke(c90 c90Var, q70<? super te4> q70Var) {
        return ((ReflectionTrackerViewModel$removeAndSeed$1) create(c90Var, q70Var)).invokeSuspend(te4.a);
    }

    /* JADX WARN: Removed duplicated region for block: B:21:0x0056  */
    /* JADX WARN: Removed duplicated region for block: B:28:0x006e  */
    /* JADX WARN: Removed duplicated region for block: B:33:0x0068 A[SYNTHETIC] */
    @Override // kotlin.coroutines.jvm.internal.BaseContinuationImpl
    /*
        Code decompiled incorrectly, please refer to instructions dump.
        To view partially-correct code enable 'Show inconsistent code' option in preferences
    */
    public final java.lang.Object invokeSuspend(java.lang.Object r6) {
        /*
            r5 = this;
            java.lang.Object r0 = defpackage.gs1.d()
            int r1 = r5.label
            r2 = 3
            r3 = 2
            r4 = 1
            if (r1 == 0) goto L25
            if (r1 == r4) goto L21
            if (r1 == r3) goto L1d
            if (r1 != r2) goto L15
            defpackage.o83.b(r6)
            goto L79
        L15:
            java.lang.IllegalStateException r6 = new java.lang.IllegalStateException
            java.lang.String r0 = "call to 'resume' before 'invoke' with coroutine"
            r6.<init>(r0)
            throw r6
        L1d:
            defpackage.o83.b(r6)
            goto L48
        L21:
            defpackage.o83.b(r6)
            goto L39
        L25:
            defpackage.o83.b(r6)
            net.safemoon.androidwallet.viewmodels.ReflectionTrackerViewModel r6 = r5.this$0
            net.safemoon.androidwallet.repository.ReflectionDataSource r6 = net.safemoon.androidwallet.viewmodels.ReflectionTrackerViewModel.d(r6)
            java.lang.String r1 = r5.$symbolWithType
            r5.label = r4
            java.lang.Object r6 = r6.j(r1, r5)
            if (r6 != r0) goto L39
            return r0
        L39:
            net.safemoon.androidwallet.viewmodels.ReflectionTrackerViewModel r6 = r5.this$0
            net.safemoon.androidwallet.repository.ReflectionDataSource r6 = net.safemoon.androidwallet.viewmodels.ReflectionTrackerViewModel.d(r6)
            r5.label = r3
            java.lang.Object r6 = r6.g(r5)
            if (r6 != r0) goto L48
            return r0
        L48:
            java.lang.Iterable r6 = (java.lang.Iterable) r6
            java.lang.String r1 = r5.$symbolWithType
            java.util.Iterator r6 = r6.iterator()
        L50:
            boolean r3 = r6.hasNext()
            if (r3 == 0) goto L68
            java.lang.Object r3 = r6.next()
            r4 = r3
            net.safemoon.androidwallet.model.reflections.RoomReflectionsToken r4 = (net.safemoon.androidwallet.model.reflections.RoomReflectionsToken) r4
            java.lang.String r4 = r4.getSymbolWithType()
            boolean r4 = defpackage.fs1.b(r4, r1)
            if (r4 == 0) goto L50
            goto L69
        L68:
            r3 = 0
        L69:
            net.safemoon.androidwallet.model.reflections.RoomReflectionsToken r3 = (net.safemoon.androidwallet.model.reflections.RoomReflectionsToken) r3
            if (r3 != 0) goto L6e
            goto L79
        L6e:
            net.safemoon.androidwallet.viewmodels.ReflectionTrackerViewModel r6 = r5.this$0
            r5.label = r2
            java.lang.Object r6 = net.safemoon.androidwallet.viewmodels.ReflectionTrackerViewModel.g(r6, r3, r5)
            if (r6 != r0) goto L79
            return r0
        L79:
            te4 r6 = defpackage.te4.a
            return r6
        */
        throw new UnsupportedOperationException("Method not decompiled: net.safemoon.androidwallet.viewmodels.ReflectionTrackerViewModel$removeAndSeed$1.invokeSuspend(java.lang.Object):java.lang.Object");
    }
}
