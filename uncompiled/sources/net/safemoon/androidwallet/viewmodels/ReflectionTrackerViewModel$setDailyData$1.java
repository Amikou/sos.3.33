package net.safemoon.androidwallet.viewmodels;

import kotlin.coroutines.jvm.internal.SuspendLambda;
import kotlin.coroutines.jvm.internal.a;

/* compiled from: ReflectionTrackerViewModel.kt */
@a(c = "net.safemoon.androidwallet.viewmodels.ReflectionTrackerViewModel$setDailyData$1", f = "ReflectionTrackerViewModel.kt", l = {275, 279, 281}, m = "invokeSuspend")
/* loaded from: classes2.dex */
public final class ReflectionTrackerViewModel$setDailyData$1 extends SuspendLambda implements hd1<c90, q70<? super te4>, Object> {
    public Object L$0;
    public Object L$1;
    public int label;
    public final /* synthetic */ ReflectionTrackerViewModel this$0;

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public ReflectionTrackerViewModel$setDailyData$1(ReflectionTrackerViewModel reflectionTrackerViewModel, q70<? super ReflectionTrackerViewModel$setDailyData$1> q70Var) {
        super(2, q70Var);
        this.this$0 = reflectionTrackerViewModel;
    }

    @Override // kotlin.coroutines.jvm.internal.BaseContinuationImpl
    public final q70<te4> create(Object obj, q70<?> q70Var) {
        return new ReflectionTrackerViewModel$setDailyData$1(this.this$0, q70Var);
    }

    @Override // defpackage.hd1
    public final Object invoke(c90 c90Var, q70<? super te4> q70Var) {
        return ((ReflectionTrackerViewModel$setDailyData$1) create(c90Var, q70Var)).invokeSuspend(te4.a);
    }

    /* JADX WARN: Removed duplicated region for block: B:25:0x008d  */
    @Override // kotlin.coroutines.jvm.internal.BaseContinuationImpl
    /*
        Code decompiled incorrectly, please refer to instructions dump.
        To view partially-correct code enable 'Show inconsistent code' option in preferences
    */
    public final java.lang.Object invokeSuspend(java.lang.Object r8) {
        /*
            r7 = this;
            java.lang.Object r0 = defpackage.gs1.d()
            int r1 = r7.label
            java.lang.String r2 = "RESET_REFLECTION_DATA_V31"
            java.lang.String r3 = "getApplication()"
            r4 = 3
            r5 = 2
            r6 = 1
            if (r1 == 0) goto L31
            if (r1 == r6) goto L2d
            if (r1 == r5) goto L29
            if (r1 != r4) goto L21
            java.lang.Object r1 = r7.L$1
            java.util.Iterator r1 = (java.util.Iterator) r1
            java.lang.Object r2 = r7.L$0
            net.safemoon.androidwallet.viewmodels.ReflectionTrackerViewModel r2 = (net.safemoon.androidwallet.viewmodels.ReflectionTrackerViewModel) r2
            defpackage.o83.b(r8)
            goto L86
        L21:
            java.lang.IllegalStateException r8 = new java.lang.IllegalStateException
            java.lang.String r0 = "call to 'resume' before 'invoke' with coroutine"
            r8.<init>(r0)
            throw r8
        L29:
            defpackage.o83.b(r8)
            goto L7c
        L2d:
            defpackage.o83.b(r8)
            goto L54
        L31:
            defpackage.o83.b(r8)
            sn4 r8 = defpackage.sn4.a
            net.safemoon.androidwallet.viewmodels.ReflectionTrackerViewModel r1 = r7.this$0
            android.app.Application r1 = r1.a()
            defpackage.fs1.e(r1, r3)
            boolean r8 = r8.a(r1, r2, r6)
            if (r8 == 0) goto L6d
            net.safemoon.androidwallet.viewmodels.ReflectionTrackerViewModel r8 = r7.this$0
            net.safemoon.androidwallet.repository.ReflectionDataSource r8 = net.safemoon.androidwallet.viewmodels.ReflectionTrackerViewModel.d(r8)
            r7.label = r6
            java.lang.Object r8 = r8.k(r7)
            if (r8 != r0) goto L54
            return r0
        L54:
            sn4 r8 = defpackage.sn4.a
            net.safemoon.androidwallet.viewmodels.ReflectionTrackerViewModel r0 = r7.this$0
            android.app.Application r0 = r0.a()
            defpackage.fs1.e(r0, r3)
            r1 = 0
            java.lang.Boolean r1 = defpackage.hr.a(r1)
            r8.d(r0, r2, r1)
            net.safemoon.androidwallet.viewmodels.ReflectionTrackerViewModel r8 = r7.this$0
            net.safemoon.androidwallet.viewmodels.ReflectionTrackerViewModel.h(r8)
            goto La0
        L6d:
            net.safemoon.androidwallet.viewmodels.ReflectionTrackerViewModel r8 = r7.this$0
            net.safemoon.androidwallet.repository.ReflectionDataSource r8 = net.safemoon.androidwallet.viewmodels.ReflectionTrackerViewModel.d(r8)
            r7.label = r5
            java.lang.Object r8 = r8.g(r7)
            if (r8 != r0) goto L7c
            return r0
        L7c:
            java.util.List r8 = (java.util.List) r8
            net.safemoon.androidwallet.viewmodels.ReflectionTrackerViewModel r1 = r7.this$0
            java.util.Iterator r8 = r8.iterator()
            r2 = r1
            r1 = r8
        L86:
            r8 = r7
        L87:
            boolean r3 = r1.hasNext()
            if (r3 == 0) goto La0
            java.lang.Object r3 = r1.next()
            net.safemoon.androidwallet.model.reflections.RoomReflectionsToken r3 = (net.safemoon.androidwallet.model.reflections.RoomReflectionsToken) r3
            r8.L$0 = r2
            r8.L$1 = r1
            r8.label = r4
            java.lang.Object r3 = net.safemoon.androidwallet.viewmodels.ReflectionTrackerViewModel.g(r2, r3, r8)
            if (r3 != r0) goto L87
            return r0
        La0:
            te4 r8 = defpackage.te4.a
            return r8
        */
        throw new UnsupportedOperationException("Method not decompiled: net.safemoon.androidwallet.viewmodels.ReflectionTrackerViewModel$setDailyData$1.invokeSuspend(java.lang.Object):java.lang.Object");
    }
}
