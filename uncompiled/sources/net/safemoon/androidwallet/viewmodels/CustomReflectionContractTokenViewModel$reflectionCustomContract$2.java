package net.safemoon.androidwallet.viewmodels;

import android.app.Application;
import kotlin.jvm.internal.Lambda;
import net.safemoon.androidwallet.repository.ReflectionDataSource;
import net.safemoon.androidwallet.utils.ReflectionCustomContract;

/* compiled from: CustomReflectionContractTokenViewModel.kt */
/* loaded from: classes2.dex */
public final class CustomReflectionContractTokenViewModel$reflectionCustomContract$2 extends Lambda implements rc1<ReflectionCustomContract> {
    public final /* synthetic */ CustomReflectionContractTokenViewModel this$0;

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public CustomReflectionContractTokenViewModel$reflectionCustomContract$2(CustomReflectionContractTokenViewModel customReflectionContractTokenViewModel) {
        super(0);
        this.this$0 = customReflectionContractTokenViewModel;
    }

    @Override // defpackage.rc1
    public final ReflectionCustomContract invoke() {
        ReflectionDataSource n;
        Application a = this.this$0.a();
        fs1.e(a, "getApplication()");
        n = this.this$0.n();
        return new ReflectionCustomContract(a, n);
    }
}
