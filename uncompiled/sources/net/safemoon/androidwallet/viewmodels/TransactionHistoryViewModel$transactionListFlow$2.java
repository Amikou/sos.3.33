package net.safemoon.androidwallet.viewmodels;

import android.app.Application;
import kotlin.jvm.internal.Lambda;
import net.safemoon.androidwallet.common.TokenType;
import net.safemoon.androidwallet.model.transaction.history.Result;
import net.safemoon.androidwallet.repository.TransactionDataSource;
import net.safemoon.androidwallet.ui.displayModel.UserTokenItemDisplayModel;

/* compiled from: TransactionHistoryViewModel.kt */
/* loaded from: classes2.dex */
public final class TransactionHistoryViewModel$transactionListFlow$2 extends Lambda implements rc1<gp2<Integer, Result>> {
    public final /* synthetic */ UserTokenItemDisplayModel $userTokenItemDisplayModel;
    public final /* synthetic */ TransactionHistoryViewModel this$0;

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public TransactionHistoryViewModel$transactionListFlow$2(TransactionHistoryViewModel transactionHistoryViewModel, UserTokenItemDisplayModel userTokenItemDisplayModel) {
        super(0);
        this.this$0 = transactionHistoryViewModel;
        this.$userTokenItemDisplayModel = userTokenItemDisplayModel;
    }

    /* JADX WARN: Can't rename method to resolve collision */
    @Override // defpackage.rc1
    public final gp2<Integer, Result> invoke() {
        String e;
        String f;
        Application a = this.this$0.a();
        fs1.e(a, "getApplication()");
        e = this.this$0.e();
        fs1.e(e, "address()");
        UserTokenItemDisplayModel userTokenItemDisplayModel = this.$userTokenItemDisplayModel;
        TokenType.a aVar = TokenType.Companion;
        f = this.this$0.f();
        fs1.e(f, "chain()");
        return new TransactionDataSource(a, e, userTokenItemDisplayModel, aVar.c(f), this.this$0.o());
    }
}
