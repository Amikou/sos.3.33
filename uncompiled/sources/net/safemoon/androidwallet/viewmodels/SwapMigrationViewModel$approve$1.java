package net.safemoon.androidwallet.viewmodels;

import kotlin.coroutines.jvm.internal.SuspendLambda;
import kotlin.coroutines.jvm.internal.a;
import kotlinx.coroutines.CoroutineDispatcher;
import net.safemoon.androidwallet.model.common.LoadingState;
import net.safemoon.androidwallet.viewmodels.blockChainClass.MigrationToV2;

/* compiled from: SwapMigrationViewModel.kt */
@a(c = "net.safemoon.androidwallet.viewmodels.SwapMigrationViewModel$approve$1", f = "SwapMigrationViewModel.kt", l = {186}, m = "invokeSuspend")
/* loaded from: classes2.dex */
public final class SwapMigrationViewModel$approve$1 extends SuspendLambda implements hd1<c90, q70<? super te4>, Object> {
    public int label;
    public final /* synthetic */ SwapMigrationViewModel this$0;

    /* compiled from: SwapMigrationViewModel.kt */
    @a(c = "net.safemoon.androidwallet.viewmodels.SwapMigrationViewModel$approve$1$1", f = "SwapMigrationViewModel.kt", l = {189}, m = "invokeSuspend")
    /* renamed from: net.safemoon.androidwallet.viewmodels.SwapMigrationViewModel$approve$1$1  reason: invalid class name */
    /* loaded from: classes2.dex */
    public static final class AnonymousClass1 extends SuspendLambda implements hd1<c90, q70<? super te4>, Object> {
        public int label;
        public final /* synthetic */ SwapMigrationViewModel this$0;

        /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
        public AnonymousClass1(SwapMigrationViewModel swapMigrationViewModel, q70<? super AnonymousClass1> q70Var) {
            super(2, q70Var);
            this.this$0 = swapMigrationViewModel;
        }

        @Override // kotlin.coroutines.jvm.internal.BaseContinuationImpl
        public final q70<te4> create(Object obj, q70<?> q70Var) {
            return new AnonymousClass1(this.this$0, q70Var);
        }

        @Override // defpackage.hd1
        public final Object invoke(c90 c90Var, q70<? super te4> q70Var) {
            return ((AnonymousClass1) create(c90Var, q70Var)).invokeSuspend(te4.a);
        }

        @Override // kotlin.coroutines.jvm.internal.BaseContinuationImpl
        public final Object invokeSuspend(Object obj) {
            MigrationToV2 migrationToV2;
            Object d = gs1.d();
            int i = this.label;
            try {
                try {
                    if (i == 0) {
                        o83.b(obj);
                        this.this$0.E().postValue(LoadingState.Loading);
                        migrationToV2 = this.this$0.w;
                        fs1.d(migrationToV2);
                        this.label = 1;
                        obj = migrationToV2.e(this);
                        if (obj == d) {
                            return d;
                        }
                    } else if (i != 1) {
                        throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
                    } else {
                        o83.b(obj);
                    }
                    SwapMigrationViewModel swapMigrationViewModel = this.this$0;
                    String transactionHash = ((hx0) obj).getTransactionHash();
                    fs1.d(transactionHash);
                    swapMigrationViewModel.y(transactionHash);
                } catch (Exception unused) {
                    this.this$0.e0().postValue(null);
                    this.this$0.t0();
                }
                this.this$0.E().postValue(LoadingState.Normal);
                return te4.a;
            } catch (Throwable th) {
                this.this$0.E().postValue(LoadingState.Normal);
                throw th;
            }
        }
    }

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public SwapMigrationViewModel$approve$1(SwapMigrationViewModel swapMigrationViewModel, q70<? super SwapMigrationViewModel$approve$1> q70Var) {
        super(2, q70Var);
        this.this$0 = swapMigrationViewModel;
    }

    @Override // kotlin.coroutines.jvm.internal.BaseContinuationImpl
    public final q70<te4> create(Object obj, q70<?> q70Var) {
        return new SwapMigrationViewModel$approve$1(this.this$0, q70Var);
    }

    @Override // defpackage.hd1
    public final Object invoke(c90 c90Var, q70<? super te4> q70Var) {
        return ((SwapMigrationViewModel$approve$1) create(c90Var, q70Var)).invokeSuspend(te4.a);
    }

    @Override // kotlin.coroutines.jvm.internal.BaseContinuationImpl
    public final Object invokeSuspend(Object obj) {
        Object d = gs1.d();
        int i = this.label;
        if (i == 0) {
            o83.b(obj);
            CoroutineDispatcher b = tp0.b();
            AnonymousClass1 anonymousClass1 = new AnonymousClass1(this.this$0, null);
            this.label = 1;
            if (kotlinx.coroutines.a.e(b, anonymousClass1, this) == d) {
                return d;
            }
        } else if (i != 1) {
            throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        } else {
            o83.b(obj);
        }
        return te4.a;
    }
}
