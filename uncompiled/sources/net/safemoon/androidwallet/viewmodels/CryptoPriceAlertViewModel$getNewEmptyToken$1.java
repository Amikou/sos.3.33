package net.safemoon.androidwallet.viewmodels;

import kotlin.coroutines.jvm.internal.ContinuationImpl;
import kotlin.coroutines.jvm.internal.a;

/* compiled from: CryptoPriceAlertViewModel.kt */
@a(c = "net.safemoon.androidwallet.viewmodels.CryptoPriceAlertViewModel", f = "CryptoPriceAlertViewModel.kt", l = {234, 243, 251}, m = "getNewEmptyToken")
/* loaded from: classes2.dex */
public final class CryptoPriceAlertViewModel$getNewEmptyToken$1 extends ContinuationImpl {
    public int I$0;
    public Object L$0;
    public Object L$1;
    public Object L$2;
    public Object L$3;
    public Object L$4;
    public int label;
    public /* synthetic */ Object result;
    public final /* synthetic */ CryptoPriceAlertViewModel this$0;

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public CryptoPriceAlertViewModel$getNewEmptyToken$1(CryptoPriceAlertViewModel cryptoPriceAlertViewModel, q70<? super CryptoPriceAlertViewModel$getNewEmptyToken$1> q70Var) {
        super(q70Var);
        this.this$0 = cryptoPriceAlertViewModel;
    }

    @Override // kotlin.coroutines.jvm.internal.BaseContinuationImpl
    public final Object invokeSuspend(Object obj) {
        Object q;
        this.result = obj;
        this.label |= Integer.MIN_VALUE;
        q = this.this$0.q(null, null, null, 0, null, this);
        return q;
    }
}
