package net.safemoon.androidwallet.viewmodels;

import kotlin.coroutines.jvm.internal.SuspendLambda;
import kotlin.coroutines.jvm.internal.a;
import net.safemoon.androidwallet.model.collectible.MarketPalace;
import net.safemoon.androidwallet.model.collectible.RoomCollectionAndNft;
import net.safemoon.androidwallet.model.nft.DeleteNftIdList;
import net.safemoon.androidwallet.model.nft.DeleteNfts;
import net.safemoon.androidwallet.model.wallets.Wallet;
import retrofit2.KotlinExtensions;
import retrofit2.b;

/* compiled from: CollectibleViewModel.kt */
@a(c = "net.safemoon.androidwallet.viewmodels.CollectibleViewModel$hideCollectionsForever$1", f = "CollectibleViewModel.kt", l = {517, 518}, m = "invokeSuspend")
/* loaded from: classes2.dex */
public final class CollectibleViewModel$hideCollectionsForever$1 extends SuspendLambda implements hd1<c90, q70<? super te4>, Object> {
    public final /* synthetic */ RoomCollectionAndNft $roomCollectionAndNft;
    public Object L$0;
    public Object L$1;
    public int label;
    public final /* synthetic */ CollectibleViewModel this$0;

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public CollectibleViewModel$hideCollectionsForever$1(CollectibleViewModel collectibleViewModel, RoomCollectionAndNft roomCollectionAndNft, q70<? super CollectibleViewModel$hideCollectionsForever$1> q70Var) {
        super(2, q70Var);
        this.this$0 = collectibleViewModel;
        this.$roomCollectionAndNft = roomCollectionAndNft;
    }

    @Override // kotlin.coroutines.jvm.internal.BaseContinuationImpl
    public final q70<te4> create(Object obj, q70<?> q70Var) {
        return new CollectibleViewModel$hideCollectionsForever$1(this.this$0, this.$roomCollectionAndNft, q70Var);
    }

    @Override // defpackage.hd1
    public final Object invoke(c90 c90Var, q70<? super te4> q70Var) {
        return ((CollectibleViewModel$hideCollectionsForever$1) create(c90Var, q70Var)).invokeSuspend(te4.a);
    }

    @Override // kotlin.coroutines.jvm.internal.BaseContinuationImpl
    public final Object invokeSuspend(Object obj) {
        Wallet u;
        String contractAddress;
        e42 E;
        RoomCollectionAndNft roomCollectionAndNft;
        CollectibleViewModel collectibleViewModel;
        Object d = gs1.d();
        int i = this.label;
        if (i == 0) {
            o83.b(obj);
            u = this.this$0.u();
            if (u != null) {
                RoomCollectionAndNft roomCollectionAndNft2 = this.$roomCollectionAndNft;
                CollectibleViewModel collectibleViewModel2 = this.this$0;
                String marketPlace = roomCollectionAndNft2.getCollection().getMarketPlace();
                if (fs1.b(marketPlace, MarketPalace.OPEN_SEA.name())) {
                    contractAddress = roomCollectionAndNft2.getCollection().getSlug();
                } else {
                    contractAddress = fs1.b(marketPlace, MarketPalace.MORALIS.name()) ? roomCollectionAndNft2.getCollection().getContractAddress() : null;
                }
                if (contractAddress != null) {
                    E = collectibleViewModel2.E();
                    b<DeleteNfts> a = E.a(new DeleteNftIdList(u.getAddress(), null, b20.c(contractAddress), 2, null));
                    this.L$0 = roomCollectionAndNft2;
                    this.L$1 = collectibleViewModel2;
                    this.label = 1;
                    if (KotlinExtensions.c(a, this) == d) {
                        return d;
                    }
                    roomCollectionAndNft = roomCollectionAndNft2;
                    collectibleViewModel = collectibleViewModel2;
                }
            }
            return te4.a;
        } else if (i != 1) {
            if (i != 2) {
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
            o83.b(obj);
            return te4.a;
        } else {
            collectibleViewModel = (CollectibleViewModel) this.L$1;
            roomCollectionAndNft = (RoomCollectionAndNft) this.L$0;
            o83.b(obj);
        }
        int chain = roomCollectionAndNft.getCollection().getChain();
        this.L$0 = null;
        this.L$1 = null;
        this.label = 2;
        if (collectibleViewModel.a0(chain, this) == d) {
            return d;
        }
        return te4.a;
    }
}
