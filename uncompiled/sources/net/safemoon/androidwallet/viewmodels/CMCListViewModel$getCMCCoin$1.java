package net.safemoon.androidwallet.viewmodels;

import kotlin.coroutines.jvm.internal.ContinuationImpl;
import kotlin.coroutines.jvm.internal.a;

/* compiled from: CMCListViewModel.kt */
@a(c = "net.safemoon.androidwallet.viewmodels.CMCListViewModel", f = "CMCListViewModel.kt", l = {47, 51}, m = "getCMCCoin")
/* loaded from: classes2.dex */
public final class CMCListViewModel$getCMCCoin$1 extends ContinuationImpl {
    public Object L$0;
    public int label;
    public /* synthetic */ Object result;
    public final /* synthetic */ CMCListViewModel this$0;

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public CMCListViewModel$getCMCCoin$1(CMCListViewModel cMCListViewModel, q70<? super CMCListViewModel$getCMCCoin$1> q70Var) {
        super(q70Var);
        this.this$0 = cMCListViewModel;
    }

    @Override // kotlin.coroutines.jvm.internal.BaseContinuationImpl
    public final Object invokeSuspend(Object obj) {
        Object l;
        this.result = obj;
        this.label |= Integer.MIN_VALUE;
        l = this.this$0.l(this);
        return l;
    }
}
