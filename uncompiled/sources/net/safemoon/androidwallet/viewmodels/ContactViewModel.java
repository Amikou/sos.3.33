package net.safemoon.androidwallet.viewmodels;

import android.app.Application;
import androidx.lifecycle.LiveData;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import net.safemoon.androidwallet.common.TokenType;
import net.safemoon.androidwallet.model.contact.RequestContact;
import net.safemoon.androidwallet.model.contact.abstraction.IContact;
import net.safemoon.androidwallet.model.contact.room.RoomContact;
import net.safemoon.androidwallet.viewmodels.ContactViewModel;
import org.web3j.abi.datatypes.Address;

/* compiled from: ContactViewModel.kt */
/* loaded from: classes2.dex */
public final class ContactViewModel extends gd {
    public final v60 b;
    public final bm1 c;
    public final gb2<IContact> d;
    public final gb2<s60> e;
    public final g72<List<IContact>> f;
    public final gb2<List<TokenType>> g;

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public ContactViewModel(Application application, v60 v60Var, bm1 bm1Var) {
        super(application);
        fs1.f(application, "application");
        fs1.f(v60Var, "contactDataSource");
        fs1.f(bm1Var, "getConcatCarouselDataUseCase");
        this.b = v60Var;
        this.c = bm1Var;
        this.d = new gb2<>();
        this.e = new gb2<>(new s60(0, b20.g()));
        g72<List<IContact>> g72Var = new g72<>();
        new ArrayList();
        te4 te4Var = te4.a;
        this.f = g72Var;
        this.g = new gb2<>(b20.g());
        LiveData<S> a = cb4.a(v60Var.d());
        fs1.e(a, "Transformations.distinctUntilChanged(this)");
        g72Var.a(a, new tl2() { // from class: b70
            @Override // defpackage.tl2
            public final void onChanged(Object obj) {
                ContactViewModel.c(ContactViewModel.this, (List) obj);
            }
        });
    }

    public static final void c(ContactViewModel contactViewModel, List list) {
        fs1.f(contactViewModel, "this$0");
        if (list == null) {
            return;
        }
        g72<List<IContact>> j = contactViewModel.j();
        ArrayList arrayList = new ArrayList();
        Iterator it = list.iterator();
        while (it.hasNext()) {
            final RoomContact roomContact = (RoomContact) it.next();
            arrayList.add(new IContact() { // from class: net.safemoon.androidwallet.viewmodels.ContactViewModel$1$1$1$1$1
                private String address;
                private int chainAddress;
                private Long contactCreate;
                private int id;
                private Long lastSent;
                private String name;
                private String profilePath;

                {
                    this.id = RoomContact.this.getId();
                    this.name = RoomContact.this.getName();
                    this.address = RoomContact.this.getAddress();
                    this.chainAddress = RoomContact.this.getChainAddress();
                    this.profilePath = RoomContact.this.getProfilePath();
                    this.lastSent = RoomContact.this.getLastSent();
                    this.contactCreate = RoomContact.this.getContactCreate();
                }

                @Override // net.safemoon.androidwallet.model.contact.abstraction.IContact
                public String getAddress() {
                    return this.address;
                }

                @Override // net.safemoon.androidwallet.model.contact.abstraction.IContact
                public int getChainAddress() {
                    return this.chainAddress;
                }

                @Override // net.safemoon.androidwallet.model.contact.abstraction.IContact
                public Long getContactCreate() {
                    return this.contactCreate;
                }

                @Override // net.safemoon.androidwallet.model.contact.abstraction.IContact
                public int getId() {
                    return this.id;
                }

                @Override // net.safemoon.androidwallet.model.contact.abstraction.IContact
                public Long getLastSent() {
                    return this.lastSent;
                }

                @Override // net.safemoon.androidwallet.model.contact.abstraction.IContact
                public String getName() {
                    return this.name;
                }

                @Override // net.safemoon.androidwallet.model.contact.abstraction.IContact
                public String getProfilePath() {
                    return this.profilePath;
                }

                @Override // net.safemoon.androidwallet.model.contact.abstraction.IContact
                public void setAddress(String str) {
                    fs1.f(str, "<set-?>");
                    this.address = str;
                }

                @Override // net.safemoon.androidwallet.model.contact.abstraction.IContact
                public void setChainAddress(int i) {
                    this.chainAddress = i;
                }

                @Override // net.safemoon.androidwallet.model.contact.abstraction.IContact
                public void setContactCreate(Long l) {
                    this.contactCreate = l;
                }

                @Override // net.safemoon.androidwallet.model.contact.abstraction.IContact
                public void setId(int i) {
                    this.id = i;
                }

                @Override // net.safemoon.androidwallet.model.contact.abstraction.IContact
                public void setLastSent(Long l) {
                    this.lastSent = l;
                }

                @Override // net.safemoon.androidwallet.model.contact.abstraction.IContact
                public void setName(String str) {
                    fs1.f(str, "<set-?>");
                    this.name = str;
                }

                @Override // net.safemoon.androidwallet.model.contact.abstraction.IContact
                public void setProfilePath(String str) {
                    fs1.f(str, "<set-?>");
                    this.profilePath = str;
                }
            });
        }
        te4 te4Var = te4.a;
        j.postValue(arrayList);
    }

    public final TokenType d() {
        TokenType.a aVar = TokenType.Companion;
        String i = bo3.i(a(), "DEFAULT_GATEWAY");
        fs1.e(i, "getString(getApplication…redPrefs.DEFAULT_GATEWAY)");
        return aVar.c(i);
    }

    public final void e(String str, rc1<te4> rc1Var) {
        fs1.f(str, Address.TYPE_NAME);
        fs1.f(rc1Var, "callBack");
        as.b(qg1.a, null, null, new ContactViewModel$deleteContact$2(this, str, rc1Var, null), 3, null);
    }

    public final void f(IContact iContact, rc1<te4> rc1Var) {
        fs1.f(iContact, "editContact");
        fs1.f(rc1Var, "callBack");
        as.b(qg1.a, null, null, new ContactViewModel$deleteContact$1(this, iContact, rc1Var, null), 3, null);
    }

    public final IContact g(final RequestContact requestContact, final int i, final String str) {
        fs1.f(requestContact, "requestContact");
        fs1.f(str, "profilePath");
        return new IContact(i, str) { // from class: net.safemoon.androidwallet.viewmodels.ContactViewModel$getContact$1
            public final /* synthetic */ int $chainAddress;
            public final /* synthetic */ String $profilePath;
            private String address;
            private int chainAddress;
            private Long contactCreate;
            private int id;
            private Long lastSent;
            private String name;
            private String profilePath;

            {
                this.$chainAddress = i;
                this.$profilePath = str;
                this.id = RequestContact.this.getId();
                String name = RequestContact.this.getName();
                fs1.d(name);
                this.name = name;
                String address = RequestContact.this.getAddress();
                fs1.d(address);
                this.address = address;
                this.chainAddress = i;
                this.profilePath = str;
                this.contactCreate = Long.valueOf(System.currentTimeMillis());
            }

            @Override // net.safemoon.androidwallet.model.contact.abstraction.IContact
            public String getAddress() {
                return this.address;
            }

            @Override // net.safemoon.androidwallet.model.contact.abstraction.IContact
            public int getChainAddress() {
                return this.chainAddress;
            }

            @Override // net.safemoon.androidwallet.model.contact.abstraction.IContact
            public Long getContactCreate() {
                return this.contactCreate;
            }

            @Override // net.safemoon.androidwallet.model.contact.abstraction.IContact
            public int getId() {
                return this.id;
            }

            @Override // net.safemoon.androidwallet.model.contact.abstraction.IContact
            public Long getLastSent() {
                return this.lastSent;
            }

            @Override // net.safemoon.androidwallet.model.contact.abstraction.IContact
            public String getName() {
                return this.name;
            }

            @Override // net.safemoon.androidwallet.model.contact.abstraction.IContact
            public String getProfilePath() {
                return this.profilePath;
            }

            @Override // net.safemoon.androidwallet.model.contact.abstraction.IContact
            public void setAddress(String str2) {
                fs1.f(str2, "<set-?>");
                this.address = str2;
            }

            @Override // net.safemoon.androidwallet.model.contact.abstraction.IContact
            public void setChainAddress(int i2) {
                this.chainAddress = i2;
            }

            @Override // net.safemoon.androidwallet.model.contact.abstraction.IContact
            public void setContactCreate(Long l) {
                this.contactCreate = l;
            }

            @Override // net.safemoon.androidwallet.model.contact.abstraction.IContact
            public void setId(int i2) {
                this.id = i2;
            }

            @Override // net.safemoon.androidwallet.model.contact.abstraction.IContact
            public void setLastSent(Long l) {
                this.lastSent = l;
            }

            @Override // net.safemoon.androidwallet.model.contact.abstraction.IContact
            public void setName(String str2) {
                fs1.f(str2, "<set-?>");
                this.name = str2;
            }

            @Override // net.safemoon.androidwallet.model.contact.abstraction.IContact
            public void setProfilePath(String str2) {
                fs1.f(str2, "<set-?>");
                this.profilePath = str2;
            }
        };
    }

    public final gb2<s60> h() {
        return this.e;
    }

    public final v60 i() {
        return this.b;
    }

    public final g72<List<IContact>> j() {
        return this.f;
    }

    public final List<IContact> k() {
        TokenType d = d();
        List<IContact> value = this.f.getValue();
        if (value == null) {
            return null;
        }
        ArrayList arrayList = new ArrayList();
        for (Object obj : value) {
            if (((IContact) obj).getChainAddress() == d.getChainId()) {
                arrayList.add(obj);
            }
        }
        return arrayList;
    }

    public final List<IContact> l() {
        List<IContact> value = this.f.getValue();
        if (value == null) {
            return null;
        }
        HashSet hashSet = new HashSet();
        ArrayList arrayList = new ArrayList();
        for (Object obj : value) {
            if (hashSet.add(((IContact) obj).getAddress())) {
                arrayList.add(obj);
            }
        }
        return arrayList;
    }

    public final gb2<List<TokenType>> m() {
        return this.g;
    }

    public final gb2<IContact> n() {
        return this.d;
    }

    public final void o() {
        gb2<s60> gb2Var = this.e;
        bm1 bm1Var = this.c;
        List<IContact> k = k();
        if (k == null) {
            k = b20.g();
        }
        gb2Var.setValue(bm1Var.a(k));
    }

    public final void p() {
        this.d.setValue(null);
    }

    public final void q(RequestContact requestContact, rc1<te4> rc1Var) {
        fs1.f(requestContact, "requestContact");
        fs1.f(rc1Var, "callBack");
        as.b(qg1.a, null, null, new ContactViewModel$saveContact$1(requestContact, this, rc1Var, null), 3, null);
    }

    public final void r(IContact iContact) {
        fs1.f(iContact, "contact");
        this.d.setValue(iContact);
    }

    public final void s() {
        if (this.d.getValue() != null) {
            as.b(qg1.a, null, null, new ContactViewModel$updateRecentContact$1(this, null), 3, null);
        }
    }
}
