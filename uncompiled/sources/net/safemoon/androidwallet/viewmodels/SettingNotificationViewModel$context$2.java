package net.safemoon.androidwallet.viewmodels;

import android.app.Application;
import kotlin.jvm.internal.Lambda;

/* compiled from: SettingNotificationViewModel.kt */
/* loaded from: classes2.dex */
public final class SettingNotificationViewModel$context$2 extends Lambda implements rc1<Application> {
    public final /* synthetic */ SettingNotificationViewModel this$0;

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public SettingNotificationViewModel$context$2(SettingNotificationViewModel settingNotificationViewModel) {
        super(0);
        this.this$0 = settingNotificationViewModel;
    }

    /* JADX WARN: Can't rename method to resolve collision */
    @Override // defpackage.rc1
    public final Application invoke() {
        return this.this$0.a();
    }
}
