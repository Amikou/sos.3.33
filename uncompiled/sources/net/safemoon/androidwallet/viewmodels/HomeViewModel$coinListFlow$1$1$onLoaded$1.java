package net.safemoon.androidwallet.viewmodels;

import kotlin.coroutines.jvm.internal.SuspendLambda;
import kotlin.coroutines.jvm.internal.a;
import net.safemoon.androidwallet.model.AllCryptoList;

/* compiled from: HomeViewModel.kt */
@a(c = "net.safemoon.androidwallet.viewmodels.HomeViewModel$coinListFlow$1$1$onLoaded$1", f = "HomeViewModel.kt", l = {}, m = "invokeSuspend")
/* loaded from: classes2.dex */
public final class HomeViewModel$coinListFlow$1$1$onLoaded$1 extends SuspendLambda implements hd1<c90, q70<? super te4>, Object> {
    public final /* synthetic */ AllCryptoList $newData;
    public int label;
    public final /* synthetic */ HomeViewModel this$0;

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public HomeViewModel$coinListFlow$1$1$onLoaded$1(HomeViewModel homeViewModel, AllCryptoList allCryptoList, q70<? super HomeViewModel$coinListFlow$1$1$onLoaded$1> q70Var) {
        super(2, q70Var);
        this.this$0 = homeViewModel;
        this.$newData = allCryptoList;
    }

    @Override // kotlin.coroutines.jvm.internal.BaseContinuationImpl
    public final q70<te4> create(Object obj, q70<?> q70Var) {
        return new HomeViewModel$coinListFlow$1$1$onLoaded$1(this.this$0, this.$newData, q70Var);
    }

    @Override // defpackage.hd1
    public final Object invoke(c90 c90Var, q70<? super te4> q70Var) {
        return ((HomeViewModel$coinListFlow$1$1$onLoaded$1) create(c90Var, q70Var)).invokeSuspend(te4.a);
    }

    @Override // kotlin.coroutines.jvm.internal.BaseContinuationImpl
    public final Object invokeSuspend(Object obj) {
        gs1.d();
        if (this.label == 0) {
            o83.b(obj);
            this.this$0.g().setValue(this.$newData);
            return te4.a;
        }
        throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
    }
}
