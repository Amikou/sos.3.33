package net.safemoon.androidwallet.viewmodels;

import kotlin.coroutines.jvm.internal.SuspendLambda;
import kotlin.coroutines.jvm.internal.a;
import kotlinx.coroutines.CoroutineDispatcher;
import net.safemoon.androidwallet.viewmodels.CustomContractTokenViewModel;

/* compiled from: CustomContractTokenViewModel.kt */
@a(c = "net.safemoon.androidwallet.viewmodels.CustomContractTokenViewModel$save$1", f = "CustomContractTokenViewModel.kt", l = {128}, m = "invokeSuspend")
/* loaded from: classes2.dex */
public final class CustomContractTokenViewModel$save$1 extends SuspendLambda implements hd1<c90, q70<? super te4>, Object> {
    public final /* synthetic */ tc1<CustomContractTokenViewModel.SaveReturnCode, te4> $callBack;
    public final /* synthetic */ String $contractAddress;
    public final /* synthetic */ String $contractDecimal;
    public final /* synthetic */ String $contractName;
    public final /* synthetic */ String $contractSymbol;
    public int label;
    public final /* synthetic */ CustomContractTokenViewModel this$0;

    /* compiled from: CustomContractTokenViewModel.kt */
    @a(c = "net.safemoon.androidwallet.viewmodels.CustomContractTokenViewModel$save$1$1", f = "CustomContractTokenViewModel.kt", l = {129, 139, 166, 167, 168}, m = "invokeSuspend")
    /* renamed from: net.safemoon.androidwallet.viewmodels.CustomContractTokenViewModel$save$1$1  reason: invalid class name */
    /* loaded from: classes2.dex */
    public static final class AnonymousClass1 extends SuspendLambda implements hd1<c90, q70<? super te4>, Object> {
        public final /* synthetic */ tc1<CustomContractTokenViewModel.SaveReturnCode, te4> $callBack;
        public final /* synthetic */ String $contractAddress;
        public final /* synthetic */ String $contractDecimal;
        public final /* synthetic */ String $contractName;
        public final /* synthetic */ String $contractSymbol;
        public Object L$0;
        public Object L$1;
        public Object L$2;
        public Object L$3;
        public int label;
        public final /* synthetic */ CustomContractTokenViewModel this$0;

        /* compiled from: CustomContractTokenViewModel.kt */
        @a(c = "net.safemoon.androidwallet.viewmodels.CustomContractTokenViewModel$save$1$1$1", f = "CustomContractTokenViewModel.kt", l = {}, m = "invokeSuspend")
        /* renamed from: net.safemoon.androidwallet.viewmodels.CustomContractTokenViewModel$save$1$1$1  reason: invalid class name and collision with other inner class name */
        /* loaded from: classes2.dex */
        public static final class C02221 extends SuspendLambda implements hd1<c90, q70<? super te4>, Object> {
            public final /* synthetic */ tc1<CustomContractTokenViewModel.SaveReturnCode, te4> $callBack;
            public int label;

            /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
            /* JADX WARN: Multi-variable type inference failed */
            public C02221(tc1<? super CustomContractTokenViewModel.SaveReturnCode, te4> tc1Var, q70<? super C02221> q70Var) {
                super(2, q70Var);
                this.$callBack = tc1Var;
            }

            @Override // kotlin.coroutines.jvm.internal.BaseContinuationImpl
            public final q70<te4> create(Object obj, q70<?> q70Var) {
                return new C02221(this.$callBack, q70Var);
            }

            @Override // defpackage.hd1
            public final Object invoke(c90 c90Var, q70<? super te4> q70Var) {
                return ((C02221) create(c90Var, q70Var)).invokeSuspend(te4.a);
            }

            @Override // kotlin.coroutines.jvm.internal.BaseContinuationImpl
            public final Object invokeSuspend(Object obj) {
                gs1.d();
                if (this.label == 0) {
                    o83.b(obj);
                    this.$callBack.invoke(CustomContractTokenViewModel.SaveReturnCode.BLACKLIST);
                    return te4.a;
                }
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
        }

        /* compiled from: CustomContractTokenViewModel.kt */
        @a(c = "net.safemoon.androidwallet.viewmodels.CustomContractTokenViewModel$save$1$1$2", f = "CustomContractTokenViewModel.kt", l = {}, m = "invokeSuspend")
        /* renamed from: net.safemoon.androidwallet.viewmodels.CustomContractTokenViewModel$save$1$1$2  reason: invalid class name */
        /* loaded from: classes2.dex */
        public static final class AnonymousClass2 extends SuspendLambda implements hd1<c90, q70<? super te4>, Object> {
            public final /* synthetic */ tc1<CustomContractTokenViewModel.SaveReturnCode, te4> $callBack;
            public int label;

            /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
            /* JADX WARN: Multi-variable type inference failed */
            public AnonymousClass2(tc1<? super CustomContractTokenViewModel.SaveReturnCode, te4> tc1Var, q70<? super AnonymousClass2> q70Var) {
                super(2, q70Var);
                this.$callBack = tc1Var;
            }

            @Override // kotlin.coroutines.jvm.internal.BaseContinuationImpl
            public final q70<te4> create(Object obj, q70<?> q70Var) {
                return new AnonymousClass2(this.$callBack, q70Var);
            }

            @Override // defpackage.hd1
            public final Object invoke(c90 c90Var, q70<? super te4> q70Var) {
                return ((AnonymousClass2) create(c90Var, q70Var)).invokeSuspend(te4.a);
            }

            @Override // kotlin.coroutines.jvm.internal.BaseContinuationImpl
            public final Object invokeSuspend(Object obj) {
                gs1.d();
                if (this.label == 0) {
                    o83.b(obj);
                    this.$callBack.invoke(CustomContractTokenViewModel.SaveReturnCode.SUCCESS);
                    return te4.a;
                }
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
        }

        /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
        /* JADX WARN: Multi-variable type inference failed */
        public AnonymousClass1(CustomContractTokenViewModel customContractTokenViewModel, String str, tc1<? super CustomContractTokenViewModel.SaveReturnCode, te4> tc1Var, String str2, String str3, String str4, q70<? super AnonymousClass1> q70Var) {
            super(2, q70Var);
            this.this$0 = customContractTokenViewModel;
            this.$contractAddress = str;
            this.$callBack = tc1Var;
            this.$contractName = str2;
            this.$contractSymbol = str3;
            this.$contractDecimal = str4;
        }

        @Override // kotlin.coroutines.jvm.internal.BaseContinuationImpl
        public final q70<te4> create(Object obj, q70<?> q70Var) {
            return new AnonymousClass1(this.this$0, this.$contractAddress, this.$callBack, this.$contractName, this.$contractSymbol, this.$contractDecimal, q70Var);
        }

        @Override // defpackage.hd1
        public final Object invoke(c90 c90Var, q70<? super te4> q70Var) {
            return ((AnonymousClass1) create(c90Var, q70Var)).invokeSuspend(te4.a);
        }

        /* JADX WARN: Removed duplicated region for block: B:32:0x00f6  */
        /* JADX WARN: Removed duplicated region for block: B:33:0x0108  */
        /* JADX WARN: Removed duplicated region for block: B:36:0x0115  */
        /* JADX WARN: Removed duplicated region for block: B:37:0x0127  */
        /* JADX WARN: Removed duplicated region for block: B:40:0x015e A[RETURN] */
        /* JADX WARN: Removed duplicated region for block: B:41:0x015f  */
        /* JADX WARN: Removed duplicated region for block: B:44:0x0178 A[RETURN] */
        /* JADX WARN: Removed duplicated region for block: B:45:0x0179  */
        /* JADX WARN: Removed duplicated region for block: B:48:0x0184  */
        @Override // kotlin.coroutines.jvm.internal.BaseContinuationImpl
        /*
            Code decompiled incorrectly, please refer to instructions dump.
            To view partially-correct code enable 'Show inconsistent code' option in preferences
        */
        public final java.lang.Object invokeSuspend(java.lang.Object r19) {
            /*
                Method dump skipped, instructions count: 469
                To view this dump change 'Code comments level' option to 'DEBUG'
            */
            throw new UnsupportedOperationException("Method not decompiled: net.safemoon.androidwallet.viewmodels.CustomContractTokenViewModel$save$1.AnonymousClass1.invokeSuspend(java.lang.Object):java.lang.Object");
        }
    }

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    /* JADX WARN: Multi-variable type inference failed */
    public CustomContractTokenViewModel$save$1(CustomContractTokenViewModel customContractTokenViewModel, String str, tc1<? super CustomContractTokenViewModel.SaveReturnCode, te4> tc1Var, String str2, String str3, String str4, q70<? super CustomContractTokenViewModel$save$1> q70Var) {
        super(2, q70Var);
        this.this$0 = customContractTokenViewModel;
        this.$contractAddress = str;
        this.$callBack = tc1Var;
        this.$contractName = str2;
        this.$contractSymbol = str3;
        this.$contractDecimal = str4;
    }

    @Override // kotlin.coroutines.jvm.internal.BaseContinuationImpl
    public final q70<te4> create(Object obj, q70<?> q70Var) {
        return new CustomContractTokenViewModel$save$1(this.this$0, this.$contractAddress, this.$callBack, this.$contractName, this.$contractSymbol, this.$contractDecimal, q70Var);
    }

    @Override // defpackage.hd1
    public final Object invoke(c90 c90Var, q70<? super te4> q70Var) {
        return ((CustomContractTokenViewModel$save$1) create(c90Var, q70Var)).invokeSuspend(te4.a);
    }

    @Override // kotlin.coroutines.jvm.internal.BaseContinuationImpl
    public final Object invokeSuspend(Object obj) {
        Object d = gs1.d();
        int i = this.label;
        if (i == 0) {
            o83.b(obj);
            CoroutineDispatcher b = tp0.b();
            AnonymousClass1 anonymousClass1 = new AnonymousClass1(this.this$0, this.$contractAddress, this.$callBack, this.$contractName, this.$contractSymbol, this.$contractDecimal, null);
            this.label = 1;
            if (kotlinx.coroutines.a.e(b, anonymousClass1, this) == d) {
                return d;
            }
        } else if (i != 1) {
            throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        } else {
            o83.b(obj);
        }
        return te4.a;
    }
}
