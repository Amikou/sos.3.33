package net.safemoon.androidwallet.viewmodels;

import android.app.Application;
import kotlin.jvm.internal.Lambda;
import net.safemoon.androidwallet.database.room.ApplicationRoomDatabase;

/* compiled from: CollectibleViewModel.kt */
/* loaded from: classes2.dex */
public final class CollectibleViewModel$userTokenListDao$2 extends Lambda implements rc1<eg4> {
    public final /* synthetic */ Application $application;

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public CollectibleViewModel$userTokenListDao$2(Application application) {
        super(0);
        this.$application = application;
    }

    /* JADX WARN: Can't rename method to resolve collision */
    @Override // defpackage.rc1
    public final eg4 invoke() {
        return ApplicationRoomDatabase.k.c(ApplicationRoomDatabase.n, this.$application, null, 2, null).Z();
    }
}
