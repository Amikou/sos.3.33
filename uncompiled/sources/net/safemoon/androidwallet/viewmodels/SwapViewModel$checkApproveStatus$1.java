package net.safemoon.androidwallet.viewmodels;

import kotlin.coroutines.jvm.internal.SuspendLambda;
import kotlin.coroutines.jvm.internal.a;
import kotlinx.coroutines.CoroutineDispatcher;
import net.safemoon.androidwallet.model.common.LoadingState;
import net.safemoon.androidwallet.viewmodels.SwapViewModel;

/* compiled from: SwapViewModel.kt */
@a(c = "net.safemoon.androidwallet.viewmodels.SwapViewModel$checkApproveStatus$1", f = "SwapViewModel.kt", l = {1252}, m = "invokeSuspend")
/* loaded from: classes2.dex */
public final class SwapViewModel$checkApproveStatus$1 extends SuspendLambda implements hd1<c90, q70<? super te4>, Object> {
    public final /* synthetic */ String $trxHash;
    public int label;
    public final /* synthetic */ SwapViewModel this$0;

    /* compiled from: SwapViewModel.kt */
    @kotlin.coroutines.jvm.internal.a(c = "net.safemoon.androidwallet.viewmodels.SwapViewModel$checkApproveStatus$1$1", f = "SwapViewModel.kt", l = {2166}, m = "invokeSuspend")
    /* renamed from: net.safemoon.androidwallet.viewmodels.SwapViewModel$checkApproveStatus$1$1  reason: invalid class name */
    /* loaded from: classes2.dex */
    public static final class AnonymousClass1 extends SuspendLambda implements hd1<c90, q70<? super te4>, Object> {
        public final /* synthetic */ String $trxHash;
        public int label;
        public final /* synthetic */ SwapViewModel this$0;

        /* compiled from: SwapViewModel.kt */
        @kotlin.coroutines.jvm.internal.a(c = "net.safemoon.androidwallet.viewmodels.SwapViewModel$checkApproveStatus$1$1$1", f = "SwapViewModel.kt", l = {1257, 1258, 1263, 1266, 1271, 1281}, m = "invokeSuspend")
        /* renamed from: net.safemoon.androidwallet.viewmodels.SwapViewModel$checkApproveStatus$1$1$1  reason: invalid class name and collision with other inner class name */
        /* loaded from: classes2.dex */
        public static final class C02391 extends SuspendLambda implements hd1<k71<? super SwapViewModel.ApproveStatus>, q70<? super te4>, Object> {
            public final /* synthetic */ String $trxHash;
            public int I$0;
            private /* synthetic */ Object L$0;
            public Object L$1;
            public int label;
            public final /* synthetic */ SwapViewModel this$0;

            /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
            public C02391(SwapViewModel swapViewModel, String str, q70<? super C02391> q70Var) {
                super(2, q70Var);
                this.this$0 = swapViewModel;
                this.$trxHash = str;
            }

            @Override // kotlin.coroutines.jvm.internal.BaseContinuationImpl
            public final q70<te4> create(Object obj, q70<?> q70Var) {
                C02391 c02391 = new C02391(this.this$0, this.$trxHash, q70Var);
                c02391.L$0 = obj;
                return c02391;
            }

            @Override // defpackage.hd1
            public final Object invoke(k71<? super SwapViewModel.ApproveStatus> k71Var, q70<? super te4> q70Var) {
                return ((C02391) create(k71Var, q70Var)).invokeSuspend(te4.a);
            }

            /* JADX WARN: Removed duplicated region for block: B:13:0x006b A[RETURN] */
            /* JADX WARN: Removed duplicated region for block: B:16:0x007f A[RETURN] */
            /* JADX WARN: Removed duplicated region for block: B:17:0x0080  */
            /* JADX WARN: Removed duplicated region for block: B:20:0x0095  */
            /* JADX WARN: Removed duplicated region for block: B:30:0x00c2  */
            /* JADX WARN: Removed duplicated region for block: B:38:0x00dd  */
            /* JADX WARN: Unsupported multi-entry loop pattern (BACK_EDGE: B:17:0x0080 -> B:18:0x0087). Please submit an issue!!! */
            @Override // kotlin.coroutines.jvm.internal.BaseContinuationImpl
            /*
                Code decompiled incorrectly, please refer to instructions dump.
                To view partially-correct code enable 'Show inconsistent code' option in preferences
            */
            public final java.lang.Object invokeSuspend(java.lang.Object r11) {
                /*
                    Method dump skipped, instructions count: 266
                    To view this dump change 'Code comments level' option to 'DEBUG'
                */
                throw new UnsupportedOperationException("Method not decompiled: net.safemoon.androidwallet.viewmodels.SwapViewModel$checkApproveStatus$1.AnonymousClass1.C02391.invokeSuspend(java.lang.Object):java.lang.Object");
            }
        }

        /* compiled from: SwapViewModel.kt */
        @kotlin.coroutines.jvm.internal.a(c = "net.safemoon.androidwallet.viewmodels.SwapViewModel$checkApproveStatus$1$1$2", f = "SwapViewModel.kt", l = {}, m = "invokeSuspend")
        /* renamed from: net.safemoon.androidwallet.viewmodels.SwapViewModel$checkApproveStatus$1$1$2  reason: invalid class name */
        /* loaded from: classes2.dex */
        public static final class AnonymousClass2 extends SuspendLambda implements kd1<k71<? super SwapViewModel.ApproveStatus>, Throwable, q70<? super te4>, Object> {
            public int label;
            public final /* synthetic */ SwapViewModel this$0;

            /* compiled from: SwapViewModel.kt */
            @kotlin.coroutines.jvm.internal.a(c = "net.safemoon.androidwallet.viewmodels.SwapViewModel$checkApproveStatus$1$1$2$1", f = "SwapViewModel.kt", l = {}, m = "invokeSuspend")
            /* renamed from: net.safemoon.androidwallet.viewmodels.SwapViewModel$checkApproveStatus$1$1$2$1  reason: invalid class name and collision with other inner class name */
            /* loaded from: classes2.dex */
            public static final class C02401 extends SuspendLambda implements hd1<c90, q70<? super te4>, Object> {
                public int label;
                public final /* synthetic */ SwapViewModel this$0;

                /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
                public C02401(SwapViewModel swapViewModel, q70<? super C02401> q70Var) {
                    super(2, q70Var);
                    this.this$0 = swapViewModel;
                }

                @Override // kotlin.coroutines.jvm.internal.BaseContinuationImpl
                public final q70<te4> create(Object obj, q70<?> q70Var) {
                    return new C02401(this.this$0, q70Var);
                }

                @Override // defpackage.hd1
                public final Object invoke(c90 c90Var, q70<? super te4> q70Var) {
                    return ((C02401) create(c90Var, q70Var)).invokeSuspend(te4.a);
                }

                @Override // kotlin.coroutines.jvm.internal.BaseContinuationImpl
                public final Object invokeSuspend(Object obj) {
                    gs1.d();
                    if (this.label == 0) {
                        o83.b(obj);
                        this.this$0.O().postValue(LoadingState.Normal);
                        return te4.a;
                    }
                    throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
                }
            }

            /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
            public AnonymousClass2(SwapViewModel swapViewModel, q70<? super AnonymousClass2> q70Var) {
                super(3, q70Var);
                this.this$0 = swapViewModel;
            }

            @Override // defpackage.kd1
            public final Object invoke(k71<? super SwapViewModel.ApproveStatus> k71Var, Throwable th, q70<? super te4> q70Var) {
                return new AnonymousClass2(this.this$0, q70Var).invokeSuspend(te4.a);
            }

            @Override // kotlin.coroutines.jvm.internal.BaseContinuationImpl
            public final Object invokeSuspend(Object obj) {
                gs1.d();
                if (this.label == 0) {
                    o83.b(obj);
                    as.b(ej4.a(this.this$0), null, null, new C02401(this.this$0, null), 3, null);
                    return te4.a;
                }
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
        }

        /* compiled from: Collect.kt */
        /* renamed from: net.safemoon.androidwallet.viewmodels.SwapViewModel$checkApproveStatus$1$1$a */
        /* loaded from: classes2.dex */
        public static final class a implements k71<SwapViewModel.ApproveStatus> {
            public final /* synthetic */ SwapViewModel a;

            public a(SwapViewModel swapViewModel) {
                this.a = swapViewModel;
            }

            @Override // defpackage.k71
            public Object emit(SwapViewModel.ApproveStatus approveStatus, q70<? super te4> q70Var) {
                st1 b;
                b = as.b(ej4.a(this.a), null, null, new SwapViewModel$checkApproveStatus$1$1$3$1(this.a, approveStatus, null), 3, null);
                return b == gs1.d() ? b : te4.a;
            }
        }

        /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
        public AnonymousClass1(SwapViewModel swapViewModel, String str, q70<? super AnonymousClass1> q70Var) {
            super(2, q70Var);
            this.this$0 = swapViewModel;
            this.$trxHash = str;
        }

        @Override // kotlin.coroutines.jvm.internal.BaseContinuationImpl
        public final q70<te4> create(Object obj, q70<?> q70Var) {
            return new AnonymousClass1(this.this$0, this.$trxHash, q70Var);
        }

        @Override // defpackage.hd1
        public final Object invoke(c90 c90Var, q70<? super te4> q70Var) {
            return ((AnonymousClass1) create(c90Var, q70Var)).invokeSuspend(te4.a);
        }

        @Override // kotlin.coroutines.jvm.internal.BaseContinuationImpl
        public final Object invokeSuspend(Object obj) {
            Object d = gs1.d();
            int i = this.label;
            if (i == 0) {
                o83.b(obj);
                j71 c = n71.c(n71.n(new C02391(this.this$0, this.$trxHash, null)), new AnonymousClass2(this.this$0, null));
                a aVar = new a(this.this$0);
                this.label = 1;
                if (c.a(aVar, this) == d) {
                    return d;
                }
            } else if (i != 1) {
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            } else {
                o83.b(obj);
            }
            return te4.a;
        }
    }

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public SwapViewModel$checkApproveStatus$1(SwapViewModel swapViewModel, String str, q70<? super SwapViewModel$checkApproveStatus$1> q70Var) {
        super(2, q70Var);
        this.this$0 = swapViewModel;
        this.$trxHash = str;
    }

    @Override // kotlin.coroutines.jvm.internal.BaseContinuationImpl
    public final q70<te4> create(Object obj, q70<?> q70Var) {
        return new SwapViewModel$checkApproveStatus$1(this.this$0, this.$trxHash, q70Var);
    }

    @Override // defpackage.hd1
    public final Object invoke(c90 c90Var, q70<? super te4> q70Var) {
        return ((SwapViewModel$checkApproveStatus$1) create(c90Var, q70Var)).invokeSuspend(te4.a);
    }

    @Override // kotlin.coroutines.jvm.internal.BaseContinuationImpl
    public final Object invokeSuspend(Object obj) {
        Object d = gs1.d();
        int i = this.label;
        if (i == 0) {
            o83.b(obj);
            CoroutineDispatcher b = tp0.b();
            AnonymousClass1 anonymousClass1 = new AnonymousClass1(this.this$0, this.$trxHash, null);
            this.label = 1;
            if (kotlinx.coroutines.a.e(b, anonymousClass1, this) == d) {
                return d;
            }
        } else if (i != 1) {
            throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        } else {
            o83.b(obj);
        }
        return te4.a;
    }
}
