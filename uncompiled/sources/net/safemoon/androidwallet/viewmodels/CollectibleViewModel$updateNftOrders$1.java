package net.safemoon.androidwallet.viewmodels;

import java.util.List;
import kotlin.Pair;
import kotlin.coroutines.jvm.internal.SuspendLambda;
import kotlin.coroutines.jvm.internal.a;
import net.safemoon.androidwallet.repository.dataSource.collection.NftDataSource;

/* compiled from: CollectibleViewModel.kt */
@a(c = "net.safemoon.androidwallet.viewmodels.CollectibleViewModel$updateNftOrders$1", f = "CollectibleViewModel.kt", l = {481}, m = "invokeSuspend")
/* loaded from: classes2.dex */
public final class CollectibleViewModel$updateNftOrders$1 extends SuspendLambda implements hd1<c90, q70<? super te4>, Object> {
    public final /* synthetic */ List<Pair<Long, Integer>> $mapIndexed;
    public int label;
    public final /* synthetic */ CollectibleViewModel this$0;

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public CollectibleViewModel$updateNftOrders$1(CollectibleViewModel collectibleViewModel, List<Pair<Long, Integer>> list, q70<? super CollectibleViewModel$updateNftOrders$1> q70Var) {
        super(2, q70Var);
        this.this$0 = collectibleViewModel;
        this.$mapIndexed = list;
    }

    @Override // kotlin.coroutines.jvm.internal.BaseContinuationImpl
    public final q70<te4> create(Object obj, q70<?> q70Var) {
        return new CollectibleViewModel$updateNftOrders$1(this.this$0, this.$mapIndexed, q70Var);
    }

    @Override // defpackage.hd1
    public final Object invoke(c90 c90Var, q70<? super te4> q70Var) {
        return ((CollectibleViewModel$updateNftOrders$1) create(c90Var, q70Var)).invokeSuspend(te4.a);
    }

    @Override // kotlin.coroutines.jvm.internal.BaseContinuationImpl
    public final Object invokeSuspend(Object obj) {
        NftDataSource G;
        Object d = gs1.d();
        int i = this.label;
        if (i == 0) {
            o83.b(obj);
            G = this.this$0.G();
            List<Pair<Long, Integer>> list = this.$mapIndexed;
            this.label = 1;
            if (G.l(list, this) == d) {
                return d;
            }
        } else if (i != 1) {
            throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        } else {
            o83.b(obj);
        }
        return te4.a;
    }
}
