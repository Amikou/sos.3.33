package net.safemoon.androidwallet.viewmodels;

import kotlin.jvm.internal.Lambda;
import net.safemoon.androidwallet.database.room.ApplicationRoomDatabase;
import net.safemoon.androidwallet.repository.dataSource.collection.CollectionDataSource;

/* compiled from: CollectibleViewModel.kt */
/* loaded from: classes2.dex */
public final class CollectibleViewModel$collectionDataSource$2 extends Lambda implements rc1<CollectionDataSource> {
    public final /* synthetic */ CollectibleViewModel this$0;

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public CollectibleViewModel$collectionDataSource$2(CollectibleViewModel collectibleViewModel) {
        super(0);
        this.this$0 = collectibleViewModel;
    }

    @Override // defpackage.rc1
    public final CollectionDataSource invoke() {
        ApplicationRoomDatabase x;
        ApplicationRoomDatabase x2;
        x = this.this$0.x();
        j10 T = x.T();
        x2 = this.this$0.x();
        return new CollectionDataSource(T, x2.W());
    }
}
