package net.safemoon.androidwallet.viewmodels;

import kotlin.coroutines.jvm.internal.SuspendLambda;
import kotlin.coroutines.jvm.internal.a;
import kotlinx.coroutines.CoroutineDispatcher;

/* compiled from: CustomReflectionContractTokenViewModel.kt */
@a(c = "net.safemoon.androidwallet.viewmodels.CustomReflectionContractTokenViewModel$generateIdentiIcon$1", f = "CustomReflectionContractTokenViewModel.kt", l = {112}, m = "invokeSuspend")
/* loaded from: classes2.dex */
public final class CustomReflectionContractTokenViewModel$generateIdentiIcon$1 extends SuspendLambda implements hd1<c90, q70<? super te4>, Object> {
    public final /* synthetic */ String $contractAddress;
    public int label;
    public final /* synthetic */ CustomReflectionContractTokenViewModel this$0;

    /* compiled from: CustomReflectionContractTokenViewModel.kt */
    @a(c = "net.safemoon.androidwallet.viewmodels.CustomReflectionContractTokenViewModel$generateIdentiIcon$1$1", f = "CustomReflectionContractTokenViewModel.kt", l = {}, m = "invokeSuspend")
    /* renamed from: net.safemoon.androidwallet.viewmodels.CustomReflectionContractTokenViewModel$generateIdentiIcon$1$1  reason: invalid class name */
    /* loaded from: classes2.dex */
    public static final class AnonymousClass1 extends SuspendLambda implements hd1<c90, q70<? super te4>, Object> {
        public final /* synthetic */ String $contractAddress;
        public int label;
        public final /* synthetic */ CustomReflectionContractTokenViewModel this$0;

        /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
        public AnonymousClass1(CustomReflectionContractTokenViewModel customReflectionContractTokenViewModel, String str, q70<? super AnonymousClass1> q70Var) {
            super(2, q70Var);
            this.this$0 = customReflectionContractTokenViewModel;
            this.$contractAddress = str;
        }

        @Override // kotlin.coroutines.jvm.internal.BaseContinuationImpl
        public final q70<te4> create(Object obj, q70<?> q70Var) {
            return new AnonymousClass1(this.this$0, this.$contractAddress, q70Var);
        }

        @Override // defpackage.hd1
        public final Object invoke(c90 c90Var, q70<? super te4> q70Var) {
            return ((AnonymousClass1) create(c90Var, q70Var)).invokeSuspend(te4.a);
        }

        @Override // kotlin.coroutines.jvm.internal.BaseContinuationImpl
        public final Object invokeSuspend(Object obj) {
            gs1.d();
            if (this.label == 0) {
                o83.b(obj);
                this.this$0.l().postValue(nn1.b(nn1.a, 0, 0, this.$contractAddress.hashCode(), 3, null));
                return te4.a;
            }
            throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        }
    }

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public CustomReflectionContractTokenViewModel$generateIdentiIcon$1(CustomReflectionContractTokenViewModel customReflectionContractTokenViewModel, String str, q70<? super CustomReflectionContractTokenViewModel$generateIdentiIcon$1> q70Var) {
        super(2, q70Var);
        this.this$0 = customReflectionContractTokenViewModel;
        this.$contractAddress = str;
    }

    @Override // kotlin.coroutines.jvm.internal.BaseContinuationImpl
    public final q70<te4> create(Object obj, q70<?> q70Var) {
        return new CustomReflectionContractTokenViewModel$generateIdentiIcon$1(this.this$0, this.$contractAddress, q70Var);
    }

    @Override // defpackage.hd1
    public final Object invoke(c90 c90Var, q70<? super te4> q70Var) {
        return ((CustomReflectionContractTokenViewModel$generateIdentiIcon$1) create(c90Var, q70Var)).invokeSuspend(te4.a);
    }

    @Override // kotlin.coroutines.jvm.internal.BaseContinuationImpl
    public final Object invokeSuspend(Object obj) {
        Object d = gs1.d();
        int i = this.label;
        if (i == 0) {
            o83.b(obj);
            CoroutineDispatcher b = tp0.b();
            AnonymousClass1 anonymousClass1 = new AnonymousClass1(this.this$0, this.$contractAddress, null);
            this.label = 1;
            if (kotlinx.coroutines.a.e(b, anonymousClass1, this) == d) {
                return d;
            }
        } else if (i != 1) {
            throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        } else {
            o83.b(obj);
        }
        return te4.a;
    }
}
