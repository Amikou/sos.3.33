package net.safemoon.androidwallet.viewmodels;

import android.app.Application;
import kotlin.jvm.internal.Lambda;
import net.safemoon.androidwallet.repository.ReflectionDataSource;

/* compiled from: ReflectionTrackerViewModel.kt */
/* loaded from: classes2.dex */
public final class ReflectionTrackerViewModel$reflectionDataSource$2 extends Lambda implements rc1<ReflectionDataSource> {
    public final /* synthetic */ Application $application;

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public ReflectionTrackerViewModel$reflectionDataSource$2(Application application) {
        super(0);
        this.$application = application;
    }

    @Override // defpackage.rc1
    public final ReflectionDataSource invoke() {
        return e53.a.a(this.$application);
    }
}
