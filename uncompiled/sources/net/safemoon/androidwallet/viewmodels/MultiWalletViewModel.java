package net.safemoon.androidwallet.viewmodels;

import android.app.Application;
import androidx.lifecycle.LiveData;
import com.google.android.gms.tasks.c;
import com.google.firebase.messaging.FirebaseMessaging;
import defpackage.pm1;
import java.util.ArrayList;
import java.util.List;
import kotlin.Pair;
import kotlin.jvm.internal.Ref$ObjectRef;
import net.safemoon.androidwallet.database.mainRoom.MainRoomDatabase;
import net.safemoon.androidwallet.database.room.ApplicationRoomDatabase;
import net.safemoon.androidwallet.model.wallets.Wallet;
import net.safemoon.androidwallet.repository.WalletDataSource;
import net.safemoon.androidwallet.utils.PreFetchData;
import net.safemoon.androidwallet.viewmodels.MultiWalletViewModel;
import org.web3j.abi.datatypes.Address;

/* compiled from: MultiWalletViewModel.kt */
/* loaded from: classes2.dex */
public final class MultiWalletViewModel extends gd {
    public final sy1 b;
    public final sy1 c;
    public final sy1 d;
    public final LiveData<List<Wallet>> e;
    public final sy1 f;
    public List<Pair<Long, Integer>> g;

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public MultiWalletViewModel(Application application) {
        super(application);
        fs1.f(application, "application");
        this.b = zy1.a(new MultiWalletViewModel$mainRoomDatabase$2(application));
        this.c = zy1.a(new MultiWalletViewModel$walletDataSource$2(this));
        this.d = zy1.a(new MultiWalletViewModel$registerDeviceTokenUseCase$2(this));
        this.e = s().e();
        s().h();
        this.f = zy1.a(new MultiWalletViewModel$fcmDataSource$2(application));
        this.g = new ArrayList();
    }

    public static final void l(MultiWalletViewModel multiWalletViewModel, c cVar) {
        fs1.f(multiWalletViewModel, "this$0");
        fs1.f(cVar, "task");
        cVar.p();
    }

    public static final void v(MultiWalletViewModel multiWalletViewModel, String str, String str2) {
        fs1.f(multiWalletViewModel, "this$0");
        fs1.f(str, "$xAddress");
        g63 r = multiWalletViewModel.r();
        fs1.e(str2, "it");
        pm1.a.a(r, str2, new String[]{str}, null, 4, null);
    }

    public static final void w(Exception exc) {
        fs1.f(exc, "it");
        System.out.print((Object) fs1.l("Firecase-addOnFailureListener ", exc.getMessage()));
    }

    public static final void y(MultiWalletViewModel multiWalletViewModel, c cVar) {
        fs1.f(multiWalletViewModel, "this$0");
        fs1.f(cVar, "task");
        fs1.l("238 MWVM removeToken: ", Boolean.valueOf(cVar.p()));
        if (cVar.p()) {
            multiWalletViewModel.k();
        }
    }

    public final void A(Wallet wallet2) {
        fs1.f(wallet2, "wallet");
        bo3.o(a(), "SAFEMOON_ADDRESS", wallet2.getAddress());
        bo3.o(a(), "SAFEMOON_PRIVATE_KEY", wallet2.getPrivateKey());
        Application a = a();
        String passPhrase = wallet2.getPassPhrase();
        if (passPhrase == null) {
            passPhrase = "";
        }
        bo3.o(a, "SAFEMOON_RECOVERY_PHRASE", passPhrase);
        bo3.o(a(), "SAFEMOON_ACTIVE_WALLET", wallet2.toString());
        ApplicationRoomDatabase.n.f();
        PreFetchData preFetchData = PreFetchData.a;
        Application a2 = a();
        fs1.e(a2, "getApplication()");
        preFetchData.b(a2);
    }

    public final void B(Wallet wallet2) {
        fs1.f(wallet2, "wallet");
        Application a = a();
        fs1.e(a, "getApplication<Application>()");
        Wallet c = e30.c(a);
        if (fs1.b(c == null ? null : c.getId(), wallet2.getId())) {
            bo3.o(a(), "SAFEMOON_ACTIVE_WALLET", wallet2.toString());
        }
    }

    public final void C(Wallet wallet2, int i, rc1<te4> rc1Var) {
        as.b(ej4.a(this), null, null, new MultiWalletViewModel$updateWallet$3(wallet2, this, i, rc1Var, null), 3, null);
    }

    public final void D(Wallet wallet2, String str, int i, rc1<te4> rc1Var) {
        fs1.f(str, "newName");
        as.b(ej4.a(this), null, null, new MultiWalletViewModel$updateWallet$1(wallet2, this, str, i, rc1Var, null), 3, null);
    }

    public final void E(Wallet wallet2, String str, rc1<te4> rc1Var) {
        fs1.f(str, "newName");
        as.b(ej4.a(this), null, null, new MultiWalletViewModel$updateWallet$2(wallet2, this, str, rc1Var, null), 3, null);
    }

    public final void F(List<Wallet> list) {
        fs1.f(list, "orders");
        ArrayList arrayList = new ArrayList();
        int i = 0;
        for (Object obj : list) {
            int i2 = i + 1;
            if (i < 0) {
                b20.p();
            }
            Long id = ((Wallet) obj).getId();
            if (id != null) {
                arrayList.add(new Pair(Long.valueOf(id.longValue()), Integer.valueOf(i)));
            }
            i = i2;
        }
        if (fs1.b(this.g, arrayList)) {
            return;
        }
        as.b(ej4.a(this), null, null, new MultiWalletViewModel$updateWalletOrders$2(this, arrayList, null), 3, null);
    }

    /* JADX WARN: Code restructure failed: missing block: B:36:0x001f, code lost:
        if ((r3.length() > 0) == true) goto L4;
     */
    /* JADX WARN: Type inference failed for: r1v7, types: [T, java.lang.Object, java.lang.String] */
    /*
        Code decompiled incorrectly, please refer to instructions dump.
        To view partially-correct code enable 'Show inconsistent code' option in preferences
    */
    public final void G(net.safemoon.androidwallet.model.wallets.Wallet r14, int r15, defpackage.rc1<defpackage.te4> r16) {
        /*
            r13 = this;
            kotlin.jvm.internal.Ref$ObjectRef r4 = new kotlin.jvm.internal.Ref$ObjectRef
            r4.<init>()
            java.lang.String r0 = ""
            r4.element = r0
            r1 = 1
            r2 = 0
            if (r14 != 0) goto Lf
        Ld:
            r1 = r2
            goto L21
        Lf:
            java.lang.String r3 = r14.getPassPhrase()
            if (r3 != 0) goto L16
            goto Ld
        L16:
            int r3 = r3.length()
            if (r3 <= 0) goto L1e
            r3 = r1
            goto L1f
        L1e:
            r3 = r2
        L1f:
            if (r3 != r1) goto Ld
        L21:
            if (r1 == 0) goto L36
            android.app.Application r1 = r13.a()
            java.lang.String r2 = r14.getPassPhrase()
            java.lang.String r1 = defpackage.w.g(r1, r2)
            java.lang.String r2 = "getEncryptedRecoveryPhra….passPhrase\n            )"
            defpackage.fs1.e(r1, r2)
            r4.element = r1
        L36:
            android.app.Application r1 = r13.a()
            if (r14 != 0) goto L3d
            goto L45
        L3d:
            java.lang.String r2 = r14.getPrivateKey()
            if (r2 != 0) goto L44
            goto L45
        L44:
            r0 = r2
        L45:
            java.lang.String r3 = defpackage.w.f(r1, r0)
            android.app.Application r0 = r13.a()
            java.lang.String r1 = "U5"
            java.lang.String r5 = defpackage.bo3.i(r0, r1)
            c90 r9 = defpackage.ej4.a(r13)
            r10 = 0
            r11 = 0
            net.safemoon.androidwallet.viewmodels.MultiWalletViewModel$updateWalletSecure$1 r12 = new net.safemoon.androidwallet.viewmodels.MultiWalletViewModel$updateWalletSecure$1
            r8 = 0
            r0 = r12
            r1 = r14
            r2 = r13
            r6 = r15
            r7 = r16
            r0.<init>(r1, r2, r3, r4, r5, r6, r7, r8)
            r0 = 3
            r1 = 0
            r6 = r9
            r7 = r10
            r8 = r11
            r9 = r12
            r10 = r0
            r11 = r1
            kotlinx.coroutines.a.b(r6, r7, r8, r9, r10, r11)
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: net.safemoon.androidwallet.viewmodels.MultiWalletViewModel.G(net.safemoon.androidwallet.model.wallets.Wallet, int, rc1):void");
    }

    /* JADX WARN: Removed duplicated region for block: B:32:0x0025  */
    /* JADX WARN: Removed duplicated region for block: B:36:0x003d  */
    /* JADX WARN: Removed duplicated region for block: B:41:0x006d  */
    /*
        Code decompiled incorrectly, please refer to instructions dump.
        To view partially-correct code enable 'Show inconsistent code' option in preferences
    */
    public final java.lang.Object j(boolean r7, defpackage.q70<? super defpackage.te4> r8) {
        /*
            r6 = this;
            boolean r0 = r8 instanceof net.safemoon.androidwallet.viewmodels.MultiWalletViewModel$addDeleteAbleDefaultToken$1
            if (r0 == 0) goto L13
            r0 = r8
            net.safemoon.androidwallet.viewmodels.MultiWalletViewModel$addDeleteAbleDefaultToken$1 r0 = (net.safemoon.androidwallet.viewmodels.MultiWalletViewModel$addDeleteAbleDefaultToken$1) r0
            int r1 = r0.label
            r2 = -2147483648(0xffffffff80000000, float:-0.0)
            r3 = r1 & r2
            if (r3 == 0) goto L13
            int r1 = r1 - r2
            r0.label = r1
            goto L18
        L13:
            net.safemoon.androidwallet.viewmodels.MultiWalletViewModel$addDeleteAbleDefaultToken$1 r0 = new net.safemoon.androidwallet.viewmodels.MultiWalletViewModel$addDeleteAbleDefaultToken$1
            r0.<init>(r6, r8)
        L18:
            java.lang.Object r8 = r0.result
            java.lang.Object r1 = defpackage.gs1.d()
            int r2 = r0.label
            java.lang.String r3 = "getApplication()"
            r4 = 1
            if (r2 == 0) goto L3d
            if (r2 != r4) goto L35
            boolean r7 = r0.Z$0
            java.lang.Object r1 = r0.L$1
            net.safemoon.androidwallet.viewmodels.MultiWalletViewModel$addDeleteAbleDefaultToken$iToken$1 r1 = (net.safemoon.androidwallet.viewmodels.MultiWalletViewModel$addDeleteAbleDefaultToken$iToken$1) r1
            java.lang.Object r0 = r0.L$0
            net.safemoon.androidwallet.viewmodels.MultiWalletViewModel r0 = (net.safemoon.androidwallet.viewmodels.MultiWalletViewModel) r0
            defpackage.o83.b(r8)
            goto L6b
        L35:
            java.lang.IllegalStateException r7 = new java.lang.IllegalStateException
            java.lang.String r8 = "call to 'resume' before 'invoke' with coroutine"
            r7.<init>(r8)
            throw r7
        L3d:
            defpackage.o83.b(r8)
            net.safemoon.androidwallet.viewmodels.MultiWalletViewModel$addDeleteAbleDefaultToken$iToken$1 r8 = new net.safemoon.androidwallet.viewmodels.MultiWalletViewModel$addDeleteAbleDefaultToken$iToken$1
            r8.<init>()
            zc0 r2 = defpackage.zc0.a
            r2.b()
            gg4 r5 = defpackage.gg4.a
            r5.a()
            android.app.Application r5 = r6.a()
            defpackage.fs1.e(r5, r3)
            yc0 r2 = r2.a(r5)
            r0.L$0 = r6
            r0.L$1 = r8
            r0.Z$0 = r7
            r0.label = r4
            java.lang.Object r0 = r2.e(r8, r0)
            if (r0 != r1) goto L69
            return r1
        L69:
            r0 = r6
            r1 = r8
        L6b:
            if (r7 == 0) goto L7d
            gg4 r7 = defpackage.gg4.a
            android.app.Application r8 = r0.a()
            defpackage.fs1.e(r8, r3)
            bn1 r7 = r7.b(r8)
            r7.i(r1)
        L7d:
            te4 r7 = defpackage.te4.a
            return r7
        */
        throw new UnsupportedOperationException("Method not decompiled: net.safemoon.androidwallet.viewmodels.MultiWalletViewModel.j(boolean, q70):java.lang.Object");
    }

    public final void k() {
        FirebaseMessaging.g().i().b(new im2() { // from class: ta2
            @Override // defpackage.im2
            public final void a(c cVar) {
                MultiWalletViewModel.l(MultiWalletViewModel.this, cVar);
            }
        });
    }

    /* JADX WARN: Type inference failed for: r0v5, types: [T, java.lang.Object, java.lang.String] */
    public final void m(String str, String str2, String str3, String str4, boolean z, boolean z2, tc1<? super Long, te4> tc1Var) {
        fs1.f(str, "privateKey");
        fs1.f(str2, Address.TYPE_NAME);
        fs1.f(str3, "passPhrase");
        Ref$ObjectRef ref$ObjectRef = new Ref$ObjectRef();
        ref$ObjectRef.element = "";
        if (str3.length() > 0) {
            ?? g = w.g(a(), str3);
            fs1.e(g, "getEncryptedRecoveryPhra…pplication(), passPhrase)");
            ref$ObjectRef.element = g;
        }
        as.b(ej4.a(this), null, null, new MultiWalletViewModel$addWallet$1(this, str2, str4, w.f(a(), str), ref$ObjectRef, bo3.i(a(), "U5"), z2, z, str, tc1Var, null), 3, null);
    }

    public final void o(Wallet wallet2) {
        fs1.f(wallet2, "wallet");
        as.b(ej4.a(this), null, null, new MultiWalletViewModel$deleteWallet$1(this, wallet2, null), 3, null);
    }

    public final void p(tc1<? super List<Wallet>, te4> tc1Var) {
        fs1.f(tc1Var, "list");
        as.b(qg1.a, null, null, new MultiWalletViewModel$getAllWallets$1(this, tc1Var, null), 3, null);
    }

    public final MainRoomDatabase q() {
        return (MainRoomDatabase) this.b.getValue();
    }

    public final g63 r() {
        return (g63) this.d.getValue();
    }

    public final WalletDataSource s() {
        return (WalletDataSource) this.c.getValue();
    }

    public final LiveData<List<Wallet>> t() {
        return this.e;
    }

    public final void u(final String str) {
        fs1.f(str, "xAddress");
        if (bo3.e(a(), "FCM_TOKEN", true)) {
            try {
                c<String> i = FirebaseMessaging.g().i();
                fs1.e(i, "getInstance().token");
                i.f(new um2() { // from class: va2
                    @Override // defpackage.um2
                    public final void a(Object obj) {
                        MultiWalletViewModel.v(MultiWalletViewModel.this, str, (String) obj);
                    }
                });
                i.d(ua2.a);
            } catch (Exception e) {
                System.out.print((Object) fs1.l("Firecase-Exception ", e.getMessage()));
                String localizedMessage = e.getLocalizedMessage();
                if (localizedMessage == null) {
                    localizedMessage = e.getMessage();
                }
                if (localizedMessage == null) {
                    return;
                }
                e30.c0(localizedMessage, "RegisterDevice3");
            }
        }
    }

    public final void x() {
        FirebaseMessaging.g().d().b(new im2() { // from class: sa2
            @Override // defpackage.im2
            public final void a(c cVar) {
                MultiWalletViewModel.y(MultiWalletViewModel.this, cVar);
            }
        });
    }

    public final void z(List<Pair<Long, Integer>> list) {
        fs1.f(list, "<set-?>");
        this.g = list;
    }
}
