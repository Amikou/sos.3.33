package net.safemoon.androidwallet.viewmodels;

import android.app.Application;
import com.google.firebase.analytics.FirebaseAnalytics;
import kotlin.jvm.internal.Lambda;

/* compiled from: MyTokensListViewModel.kt */
/* loaded from: classes2.dex */
public final class MyTokensListViewModel$analytics$2 extends Lambda implements rc1<FirebaseAnalytics> {
    public final /* synthetic */ Application $application;

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public MyTokensListViewModel$analytics$2(Application application) {
        super(0);
        this.$application = application;
    }

    /* JADX WARN: Can't rename method to resolve collision */
    @Override // defpackage.rc1
    public final FirebaseAnalytics invoke() {
        return FirebaseAnalytics.getInstance(this.$application);
    }
}
