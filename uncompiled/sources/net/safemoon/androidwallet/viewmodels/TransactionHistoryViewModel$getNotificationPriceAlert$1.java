package net.safemoon.androidwallet.viewmodels;

import java.util.List;
import kotlin.coroutines.jvm.internal.SuspendLambda;
import kotlin.coroutines.jvm.internal.a;
import net.safemoon.androidwallet.model.priceAlert.PriceAlertToken;

/* compiled from: TransactionHistoryViewModel.kt */
@a(c = "net.safemoon.androidwallet.viewmodels.TransactionHistoryViewModel$getNotificationPriceAlert$1", f = "TransactionHistoryViewModel.kt", l = {100}, m = "invokeSuspend")
/* loaded from: classes2.dex */
public final class TransactionHistoryViewModel$getNotificationPriceAlert$1 extends SuspendLambda implements hd1<c90, q70<? super te4>, Object> {
    public final /* synthetic */ String $tokenAddress;
    public final /* synthetic */ String $tokenSymbol;
    public Object L$0;
    public int label;
    public final /* synthetic */ TransactionHistoryViewModel this$0;

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public TransactionHistoryViewModel$getNotificationPriceAlert$1(TransactionHistoryViewModel transactionHistoryViewModel, String str, String str2, q70<? super TransactionHistoryViewModel$getNotificationPriceAlert$1> q70Var) {
        super(2, q70Var);
        this.this$0 = transactionHistoryViewModel;
        this.$tokenAddress = str;
        this.$tokenSymbol = str2;
    }

    @Override // kotlin.coroutines.jvm.internal.BaseContinuationImpl
    public final q70<te4> create(Object obj, q70<?> q70Var) {
        return new TransactionHistoryViewModel$getNotificationPriceAlert$1(this.this$0, this.$tokenAddress, this.$tokenSymbol, q70Var);
    }

    @Override // defpackage.hd1
    public final Object invoke(c90 c90Var, q70<? super te4> q70Var) {
        return ((TransactionHistoryViewModel$getNotificationPriceAlert$1) create(c90Var, q70Var)).invokeSuspend(te4.a);
    }

    /* JADX WARN: Multi-variable type inference failed */
    /* JADX WARN: Type inference failed for: r0v2, types: [gb2] */
    @Override // kotlin.coroutines.jvm.internal.BaseContinuationImpl
    public final Object invokeSuspend(Object obj) {
        Object p;
        gb2<List<PriceAlertToken>> gb2Var;
        Object d = gs1.d();
        int i = this.label;
        if (i == 0) {
            o83.b(obj);
            gb2<List<PriceAlertToken>> n = this.this$0.n();
            TransactionHistoryViewModel transactionHistoryViewModel = this.this$0;
            String str = this.$tokenAddress;
            String str2 = this.$tokenSymbol;
            this.L$0 = n;
            this.label = 1;
            p = transactionHistoryViewModel.p(str, str2, this);
            if (p == d) {
                return d;
            }
            gb2Var = n;
            obj = p;
        } else if (i != 1) {
            throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        } else {
            gb2Var = (gb2) this.L$0;
            o83.b(obj);
        }
        gb2Var.postValue(obj);
        return te4.a;
    }
}
