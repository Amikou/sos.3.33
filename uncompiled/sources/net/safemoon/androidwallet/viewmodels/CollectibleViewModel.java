package net.safemoon.androidwallet.viewmodels;

import android.app.Application;
import androidx.lifecycle.LiveData;
import java.math.BigInteger;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import kotlin.Pair;
import net.safemoon.androidwallet.common.TokenType;
import net.safemoon.androidwallet.database.room.ApplicationRoomDatabase;
import net.safemoon.androidwallet.model.collectible.RoomCollection;
import net.safemoon.androidwallet.model.collectible.RoomCollectionAndNft;
import net.safemoon.androidwallet.model.collectible.RoomNFT;
import net.safemoon.androidwallet.model.collectible.TYPE_DELETE_NFT;
import net.safemoon.androidwallet.model.common.LoadingState;
import net.safemoon.androidwallet.model.nft.NFTBalance;
import net.safemoon.androidwallet.model.nft.NFTTransferResponse;
import net.safemoon.androidwallet.model.nft.NFTType;
import net.safemoon.androidwallet.model.nft.NFTTypeKt;
import net.safemoon.androidwallet.model.wallets.Wallet;
import net.safemoon.androidwallet.repository.dataSource.collection.CollectionDataSource;
import net.safemoon.androidwallet.repository.dataSource.collection.NftDataSource;
import net.safemoon.androidwallet.viewmodels.CollectibleViewModel;
import net.safemoon.androidwallet.viewmodels.blockChainClass.NFT1155;
import net.safemoon.androidwallet.viewmodels.blockChainClass.NFT721;

/* compiled from: CollectibleViewModel.kt */
/* loaded from: classes2.dex */
public final class CollectibleViewModel extends gd {
    public final sy1 b;
    public final sy1 c;
    public final sy1 d;
    public final sy1 e;
    public final sy1 f;
    public final gb2<TokenType> g;
    public final LiveData<List<RoomCollectionAndNft>> h;
    public final gb2<Boolean> i;
    public final gb2<TYPE_DELETE_NFT> j;
    public final g72<List<RoomCollectionAndNft>> k;
    public final g72<List<RoomCollectionAndNft>> l;
    public final sy1 m;
    public final gb2<LoadingState> n;
    public final gb2<String> o;
    public final gb2<String> p;
    public Double q;
    public Double r;
    public gb2<Boolean> s;

    /* compiled from: CollectibleViewModel.kt */
    /* loaded from: classes2.dex */
    public /* synthetic */ class a {
        public static final /* synthetic */ int[] a;

        static {
            int[] iArr = new int[NFTType.values().length];
            iArr[NFTType.ERC1155.ordinal()] = 1;
            iArr[NFTType.ERC721.ordinal()] = 2;
            a = iArr;
        }
    }

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public CollectibleViewModel(Application application) {
        super(application);
        fs1.f(application, "application");
        this.b = zy1.a(new CollectibleViewModel$db$2(application));
        this.c = zy1.a(new CollectibleViewModel$activeWallet$2(application));
        this.d = zy1.a(new CollectibleViewModel$collectionDataSource$2(this));
        this.e = zy1.a(new CollectibleViewModel$nftDataSource$2(this));
        this.f = zy1.a(CollectibleViewModel$marketDataApiInterface$2.INSTANCE);
        gb2<TokenType> gb2Var = new gb2<>(e30.f(application));
        this.g = gb2Var;
        LiveData<List<RoomCollectionAndNft>> c = cb4.c(gb2Var, new ud1() { // from class: d10
            @Override // defpackage.ud1
            public final Object apply(Object obj) {
                LiveData V;
                V = CollectibleViewModel.V(CollectibleViewModel.this, (TokenType) obj);
                return V;
            }
        });
        fs1.e(c, "switchMap(selectChain) {…ections(it.chainId)\n    }");
        this.h = c;
        this.i = new gb2<>(Boolean.FALSE);
        this.j = new gb2<>(TYPE_DELETE_NFT.VISIBLE);
        final g72<List<RoomCollectionAndNft>> g72Var = new g72<>();
        g72Var.a(c, new tl2() { // from class: g10
            @Override // defpackage.tl2
            public final void onChanged(Object obj) {
                CollectibleViewModel.R(g72.this, this, (List) obj);
            }
        });
        g72Var.a(I(), new tl2() { // from class: e10
            @Override // defpackage.tl2
            public final void onChanged(Object obj) {
                CollectibleViewModel.S(g72.this, this, (Boolean) obj);
            }
        });
        g72Var.a(J(), new tl2() { // from class: h10
            @Override // defpackage.tl2
            public final void onChanged(Object obj) {
                CollectibleViewModel.T(g72.this, this, (TYPE_DELETE_NFT) obj);
            }
        });
        te4 te4Var = te4.a;
        this.k = g72Var;
        final g72<List<RoomCollectionAndNft>> g72Var2 = new g72<>();
        g72Var2.a(c, new tl2() { // from class: f10
            @Override // defpackage.tl2
            public final void onChanged(Object obj) {
                CollectibleViewModel.W(g72.this, this, (List) obj);
            }
        });
        this.l = g72Var2;
        this.m = zy1.a(new CollectibleViewModel$userTokenListDao$2(application));
        this.n = new gb2<>(LoadingState.Normal);
        this.o = new gb2<>();
        this.p = new gb2<>();
        this.s = new gb2<>();
    }

    public static /* synthetic */ void Q(CollectibleViewModel collectibleViewModel, int i, int i2, int i3, int i4, Object obj) {
        if ((i4 & 2) != 0) {
            i2 = 100;
        }
        if ((i4 & 4) != 0) {
            i3 = 0;
        }
        collectibleViewModel.P(i, i2, i3);
    }

    public static final void R(g72 g72Var, CollectibleViewModel collectibleViewModel, List list) {
        fs1.f(g72Var, "$this_apply");
        fs1.f(collectibleViewModel, "this$0");
        U(g72Var, collectibleViewModel);
    }

    public static final void S(g72 g72Var, CollectibleViewModel collectibleViewModel, Boolean bool) {
        fs1.f(g72Var, "$this_apply");
        fs1.f(collectibleViewModel, "this$0");
        U(g72Var, collectibleViewModel);
    }

    public static final void T(g72 g72Var, CollectibleViewModel collectibleViewModel, TYPE_DELETE_NFT type_delete_nft) {
        fs1.f(g72Var, "$this_apply");
        fs1.f(collectibleViewModel, "this$0");
        U(g72Var, collectibleViewModel);
    }

    public static final void U(g72<List<RoomCollectionAndNft>> g72Var, CollectibleViewModel collectibleViewModel) {
        List<RoomCollectionAndNft> value = collectibleViewModel.h.getValue();
        ArrayList arrayList = null;
        if (value != null) {
            ArrayList<RoomCollectionAndNft> arrayList2 = new ArrayList();
            Iterator<T> it = value.iterator();
            while (true) {
                boolean z = false;
                if (!it.hasNext()) {
                    break;
                }
                Object next = it.next();
                int typeDeleteNft = ((RoomCollectionAndNft) next).getCollection().getTypeDeleteNft();
                TYPE_DELETE_NFT value2 = collectibleViewModel.J().getValue();
                if (value2 != null && typeDeleteNft == value2.getValue()) {
                    z = true;
                }
                if (z) {
                    arrayList2.add(next);
                }
            }
            ArrayList arrayList3 = new ArrayList(c20.q(arrayList2, 10));
            for (RoomCollectionAndNft roomCollectionAndNft : arrayList2) {
                RoomCollectionAndNft copy$default = RoomCollectionAndNft.copy$default(roomCollectionAndNft, null, null, 3, null);
                Boolean value3 = collectibleViewModel.I().getValue();
                copy$default.setShowHideIcon(value3 == null ? false : value3.booleanValue());
                arrayList3.add(copy$default);
            }
            arrayList = arrayList3;
        }
        g72Var.postValue(arrayList);
    }

    public static final LiveData V(CollectibleViewModel collectibleViewModel, TokenType tokenType) {
        fs1.f(collectibleViewModel, "this$0");
        return collectibleViewModel.w().d(tokenType.getChainId());
    }

    public static final void W(g72 g72Var, CollectibleViewModel collectibleViewModel, List list) {
        fs1.f(g72Var, "$this_apply");
        fs1.f(collectibleViewModel, "this$0");
        X(g72Var, collectibleViewModel);
    }

    public static final void X(g72<List<RoomCollectionAndNft>> g72Var, CollectibleViewModel collectibleViewModel) {
        g72Var.postValue(collectibleViewModel.B());
    }

    public static /* synthetic */ void f0(CollectibleViewModel collectibleViewModel, NFT1155 nft1155, String str, BigInteger bigInteger, BigInteger bigInteger2, gb2 gb2Var, rc1 rc1Var, int i, Object obj) {
        if ((i & 8) != 0) {
            bigInteger2 = BigInteger.ONE;
            fs1.e(bigInteger2, "ONE");
        }
        collectibleViewModel.e0(nft1155, str, bigInteger, bigInteger2, gb2Var, rc1Var);
    }

    public static /* synthetic */ LiveData t(CollectibleViewModel collectibleViewModel, RoomCollection roomCollection, int i, int i2, int i3, Object obj) {
        if ((i3 & 2) != 0) {
            i = 100;
        }
        if ((i3 & 4) != 0) {
            i2 = 0;
        }
        return collectibleViewModel.s(roomCollection, i, i2);
    }

    public final Double A() {
        return this.q;
    }

    public final List<RoomCollectionAndNft> B() {
        List<RoomCollectionAndNft> value = this.h.getValue();
        if (value == null) {
            return null;
        }
        ArrayList arrayList = new ArrayList();
        for (Object obj : value) {
            if (((RoomCollectionAndNft) obj).getCollection().getTypeDeleteNft() == TYPE_DELETE_NFT.HIDE.getValue()) {
                arrayList.add(obj);
            }
        }
        return arrayList;
    }

    public final g72<List<RoomCollectionAndNft>> C() {
        return this.k;
    }

    public final g72<List<RoomCollectionAndNft>> D() {
        return this.l;
    }

    public final e42 E() {
        return (e42) this.f.getValue();
    }

    public final gb2<String> F() {
        return this.p;
    }

    public final NftDataSource G() {
        return (NftDataSource) this.e.getValue();
    }

    public final gb2<TokenType> H() {
        return this.g;
    }

    public final gb2<Boolean> I() {
        return this.i;
    }

    public final gb2<TYPE_DELETE_NFT> J() {
        return this.j;
    }

    public final eg4 K() {
        return (eg4) this.m.getValue();
    }

    public final void L(RoomCollectionAndNft roomCollectionAndNft, boolean z) {
        fs1.f(roomCollectionAndNft, "roomCollectionAndNft");
        as.b(ej4.a(this), tp0.b(), null, new CollectibleViewModel$hideCollections$1(this, roomCollectionAndNft, z, null), 2, null);
    }

    public final void M(RoomCollectionAndNft roomCollectionAndNft) {
        fs1.f(roomCollectionAndNft, "roomCollectionAndNft");
        as.b(ej4.a(this), tp0.b(), null, new CollectibleViewModel$hideCollectionsForever$1(this, roomCollectionAndNft, null), 2, null);
    }

    public final gb2<Boolean> N() {
        return this.s;
    }

    public final im1 O(Wallet wallet2, String str, String str2, Integer num) {
        NFTType nFTType;
        im1 nft1155;
        if (num == null) {
            return null;
        }
        if (str2 == null) {
            nFTType = null;
        } else {
            try {
                nFTType = NFTTypeKt.toNFTType(str2);
            } catch (Exception unused) {
                return null;
            }
        }
        int i = nFTType == null ? -1 : a.a[nFTType.ordinal()];
        if (i == 1) {
            fs1.d(str);
            int intValue = num.intValue();
            fs1.d(wallet2);
            nft1155 = new NFT1155(str, intValue, wallet2);
        } else if (i != 2) {
            return null;
        } else {
            fs1.d(str);
            int intValue2 = num.intValue();
            fs1.d(wallet2);
            nft1155 = new NFT721(str, intValue2, wallet2);
        }
        return nft1155;
    }

    public final void P(int i, int i2, int i3) {
        as.b(ej4.a(this), tp0.b(), null, new CollectibleViewModel$loadCollections$2(this, i, i2, i3, null), 2, null);
    }

    public final void Y(Double d) {
        this.r = d;
    }

    public final void Z(Double d) {
        this.q = d;
    }

    /* JADX WARN: Can't wrap try/catch for region: R(10:1|(2:3|(8:5|6|7|(1:(1:(1:(3:12|13|14)(2:16|17))(9:18|19|20|(4:23|(4:79|(4:115|(3:120|(3:123|(2:125|126)|121)|127)|117|(2:119|(3:109|110|111)(3:83|(4:96|(3:101|(3:104|(2:106|107)|102)|108)|98|(1:100))|(3:90|91|92)(3:87|88|89))))|81|(0)(0))(3:25|26|(1:78)(4:28|(4:65|(3:70|(3:73|(2:75|76)|71)|77)|67|(2:69|(3:59|60|61)(3:32|(4:46|(3:51|(3:54|(2:56|57)|52)|58)|48|(1:50))|(3:40|41|42)(3:36|37|38))))|30|(0)(0)))|39|21)|128|129|(1:131)|13|14))(2:132|133))(4:142|(2:144|(1:146)(1:147))|13|14)|134|(1:136)(1:141)|137|(1:139)(8:140|20|(1:21)|128|129|(0)|13|14)))|149|6|7|(0)(0)|134|(0)(0)|137|(0)(0)) */
    /* JADX WARN: Removed duplicated region for block: B:10:0x0026  */
    /* JADX WARN: Removed duplicated region for block: B:121:0x0259 A[RETURN] */
    /* JADX WARN: Removed duplicated region for block: B:126:0x011d A[SYNTHETIC] */
    /* JADX WARN: Removed duplicated region for block: B:129:0x0103 A[SYNTHETIC] */
    /* JADX WARN: Removed duplicated region for block: B:134:0x01df A[SYNTHETIC] */
    /* JADX WARN: Removed duplicated region for block: B:135:0x01c4 A[SYNTHETIC] */
    /* JADX WARN: Removed duplicated region for block: B:23:0x0057  */
    /* JADX WARN: Removed duplicated region for block: B:32:0x0090  */
    /* JADX WARN: Removed duplicated region for block: B:33:0x0092 A[Catch: Exception -> 0x025a, TryCatch #0 {Exception -> 0x025a, blocks: (B:13:0x002c, B:18:0x0045, B:38:0x00ad, B:39:0x00b3, B:41:0x00b9, B:60:0x0103, B:78:0x014f, B:79:0x016a, B:63:0x0120, B:66:0x0127, B:69:0x012f, B:70:0x0133, B:72:0x0139, B:45:0x00d4, B:48:0x00db, B:51:0x00e3, B:52:0x00e7, B:54:0x00ed, B:80:0x0185, B:99:0x01c4, B:117:0x0211, B:118:0x022c, B:102:0x01e2, B:105:0x01e9, B:108:0x01f1, B:109:0x01f5, B:111:0x01fb, B:84:0x0195, B:87:0x019c, B:90:0x01a4, B:91:0x01a8, B:93:0x01ae, B:119:0x0247, B:21:0x0053, B:30:0x0086, B:34:0x0096, B:33:0x0092, B:26:0x0062), top: B:125:0x0024 }] */
    /* JADX WARN: Removed duplicated region for block: B:36:0x00a8 A[RETURN] */
    /* JADX WARN: Removed duplicated region for block: B:37:0x00a9  */
    /* JADX WARN: Removed duplicated region for block: B:41:0x00b9 A[Catch: Exception -> 0x025a, TryCatch #0 {Exception -> 0x025a, blocks: (B:13:0x002c, B:18:0x0045, B:38:0x00ad, B:39:0x00b3, B:41:0x00b9, B:60:0x0103, B:78:0x014f, B:79:0x016a, B:63:0x0120, B:66:0x0127, B:69:0x012f, B:70:0x0133, B:72:0x0139, B:45:0x00d4, B:48:0x00db, B:51:0x00e3, B:52:0x00e7, B:54:0x00ed, B:80:0x0185, B:99:0x01c4, B:117:0x0211, B:118:0x022c, B:102:0x01e2, B:105:0x01e9, B:108:0x01f1, B:109:0x01f5, B:111:0x01fb, B:84:0x0195, B:87:0x019c, B:90:0x01a4, B:91:0x01a8, B:93:0x01ae, B:119:0x0247, B:21:0x0053, B:30:0x0086, B:34:0x0096, B:33:0x0092, B:26:0x0062), top: B:125:0x0024 }] */
    /*
        Code decompiled incorrectly, please refer to instructions dump.
        To view partially-correct code enable 'Show inconsistent code' option in preferences
    */
    public final java.lang.Object a0(int r14, defpackage.q70<? super defpackage.te4> r15) {
        /*
            Method dump skipped, instructions count: 605
            To view this dump change 'Code comments level' option to 'DEBUG'
        */
        throw new UnsupportedOperationException("Method not decompiled: net.safemoon.androidwallet.viewmodels.CollectibleViewModel.a0(int, q70):java.lang.Object");
    }

    public final void b0() {
        this.j.postValue(TYPE_DELETE_NFT.HIDE);
        this.i.postValue(Boolean.FALSE);
    }

    public final void c0(boolean z) {
        this.j.postValue(TYPE_DELETE_NFT.VISIBLE);
        this.i.postValue(Boolean.valueOf(z));
    }

    public final void d0(String str, String str2, im1 im1Var, gb2<NFTTransferResponse> gb2Var, rc1<te4> rc1Var) {
        fs1.f(gb2Var, "callBack");
        fs1.f(rc1Var, "errorCallBack");
        try {
            if (im1Var instanceof NFT721) {
                fs1.d(str);
                fs1.d(str2);
                g0((NFT721) im1Var, str, new BigInteger(str2), gb2Var, rc1Var);
            } else if (im1Var instanceof NFT1155) {
                fs1.d(str);
                fs1.d(str2);
                f0(this, (NFT1155) im1Var, str, new BigInteger(str2), null, gb2Var, rc1Var, 8, null);
            } else {
                new Exception("Unknown NFT Type");
            }
        } catch (Exception e) {
            String message = e.getMessage();
            if (message == null) {
                message = "Error";
            }
            gb2Var.postValue(new NFTTransferResponse(null, null, message));
        }
    }

    public final void e0(NFT1155 nft1155, String str, BigInteger bigInteger, BigInteger bigInteger2, gb2<NFTTransferResponse> gb2Var, rc1<te4> rc1Var) {
        fs1.f(nft1155, "nft1155");
        fs1.f(str, "toAddress");
        fs1.f(bigInteger, "tokenId");
        fs1.f(bigInteger2, "amount");
        fs1.f(gb2Var, "callBack");
        fs1.f(rc1Var, "errorCallBack");
        this.n.postValue(LoadingState.Loading);
        as.b(ej4.a(this), null, null, new CollectibleViewModel$transfer1155From$1(nft1155, bigInteger, bigInteger2, str, gb2Var, this, rc1Var, null), 3, null);
    }

    public final void g0(NFT721 nft721, String str, BigInteger bigInteger, gb2<NFTTransferResponse> gb2Var, rc1<te4> rc1Var) {
        fs1.f(nft721, "nft721");
        fs1.f(str, "toAddress");
        fs1.f(bigInteger, "tokenId");
        fs1.f(gb2Var, "callBack");
        fs1.f(rc1Var, "errorCallBack");
        this.n.postValue(LoadingState.Loading);
        as.b(ej4.a(this), null, null, new CollectibleViewModel$transfer711From$1(nft721, bigInteger, str, gb2Var, this, rc1Var, null), 3, null);
    }

    public final void h0(List<Pair<Long, Integer>> list) {
        fs1.f(list, "mapIndexed");
        as.b(ej4.a(this), tp0.b(), null, new CollectibleViewModel$updateCollectionOrders$1(this, list, null), 2, null);
    }

    public final void i0(List<Pair<Long, Integer>> list) {
        fs1.f(list, "mapIndexed");
        as.b(ej4.a(this), tp0.b(), null, new CollectibleViewModel$updateNftOrders$1(this, list, null), 2, null);
    }

    public final void o(String str, im1 im1Var, gb2<NFTBalance> gb2Var) {
        fs1.f(gb2Var, "callBack");
        as.b(ej4.a(this), null, null, new CollectibleViewModel$balance$1(im1Var, str, gb2Var, null), 3, null);
    }

    public final void p(String str, String str2, im1 im1Var) {
        fs1.f(str, "toAddress");
        fs1.f(str2, "tokenId");
        as.b(ej4.a(this), null, null, new CollectibleViewModel$estimateGas$1(im1Var, str, str2, this, null), 3, null);
    }

    /* JADX WARN: Can't wrap try/catch for region: R(11:80|81|82|83|84|85|86|87|88|89|(1:91)(6:92|93|94|58|59|(0)(0))) */
    /* JADX WARN: Can't wrap try/catch for region: R(8:47|(1:49)(1:115)|50|(1:52)(1:114)|53|54|55|(4:57|58|59|(1:61)(3:62|63|(2:65|(1:67)(5:68|69|25|26|(0)(0)))(2:70|(1:72)(7:73|74|(2:76|(1:78)(2:79|23))|24|25|26|(0)(0)))))(11:80|81|82|83|84|85|86|87|88|89|(1:91)(6:92|93|94|58|59|(0)(0)))) */
    /* JADX WARN: Code restructure failed: missing block: B:196:0x04ac, code lost:
        r0 = e;
     */
    /* JADX WARN: Code restructure failed: missing block: B:198:0x04ae, code lost:
        r0 = e;
     */
    /* JADX WARN: Code restructure failed: missing block: B:199:0x04af, code lost:
        r12 = r10;
     */
    /* JADX WARN: Code restructure failed: missing block: B:200:0x04b2, code lost:
        r0 = e;
     */
    /* JADX WARN: Code restructure failed: missing block: B:201:0x04b3, code lost:
        r12 = r10;
     */
    /* JADX WARN: Code restructure failed: missing block: B:202:0x04b6, code lost:
        r0 = e;
     */
    /* JADX WARN: Code restructure failed: missing block: B:203:0x04b7, code lost:
        r12 = r10;
        r11 = r32;
     */
    /* JADX WARN: Code restructure failed: missing block: B:204:0x04bb, code lost:
        r10 = r33;
     */
    /* JADX WARN: Code restructure failed: missing block: B:205:0x04bd, code lost:
        r14 = r10;
        r15 = r11;
        r11 = r2;
        r10 = r5;
        r34 = r12;
        r13 = r1;
        r12 = r6;
        r5 = r34;
     */
    /* JADX WARN: Removed duplicated region for block: B:10:0x0030  */
    /* JADX WARN: Removed duplicated region for block: B:121:0x02ec  */
    /* JADX WARN: Removed duplicated region for block: B:123:0x02f3  */
    /* JADX WARN: Removed duplicated region for block: B:12:0x0038  */
    /* JADX WARN: Removed duplicated region for block: B:135:0x035e A[RETURN] */
    /* JADX WARN: Removed duplicated region for block: B:136:0x035f  */
    /* JADX WARN: Removed duplicated region for block: B:13:0x003d  */
    /* JADX WARN: Removed duplicated region for block: B:142:0x0367  */
    /* JADX WARN: Removed duplicated region for block: B:149:0x038d  */
    /* JADX WARN: Removed duplicated region for block: B:14:0x004f  */
    /* JADX WARN: Removed duplicated region for block: B:15:0x0066  */
    /* JADX WARN: Removed duplicated region for block: B:16:0x007d  */
    /* JADX WARN: Removed duplicated region for block: B:171:0x0417  */
    /* JADX WARN: Removed duplicated region for block: B:17:0x0090  */
    /* JADX WARN: Removed duplicated region for block: B:18:0x00ad  */
    /* JADX WARN: Removed duplicated region for block: B:209:0x04ce  */
    /* JADX WARN: Removed duplicated region for block: B:213:0x04fa A[RETURN] */
    /* JADX WARN: Removed duplicated region for block: B:214:0x04fb  */
    /* JADX WARN: Removed duplicated region for block: B:217:0x050e  */
    /* JADX WARN: Removed duplicated region for block: B:222:0x0547  */
    /* JADX WARN: Removed duplicated region for block: B:228:0x0586  */
    /* JADX WARN: Removed duplicated region for block: B:233:0x05bb  */
    /* JADX WARN: Removed duplicated region for block: B:236:0x05c6  */
    /* JADX WARN: Removed duplicated region for block: B:23:0x00d2  */
    /* JADX WARN: Removed duplicated region for block: B:241:0x05f4 A[RETURN] */
    /* JADX WARN: Removed duplicated region for block: B:245:0x0606 A[RETURN] */
    /* JADX WARN: Removed duplicated region for block: B:25:0x00ee  */
    /* JADX WARN: Removed duplicated region for block: B:28:0x00f9  */
    /* JADX WARN: Removed duplicated region for block: B:29:0x0108  */
    /* JADX WARN: Removed duplicated region for block: B:30:0x0113  */
    /* JADX WARN: Removed duplicated region for block: B:31:0x012f  */
    /* JADX WARN: Removed duplicated region for block: B:32:0x0142  */
    /* JADX WARN: Removed duplicated region for block: B:35:0x014c  */
    /* JADX WARN: Removed duplicated region for block: B:58:0x019f  */
    /* JADX WARN: Removed duplicated region for block: B:65:0x01c7  */
    /* JADX WARN: Removed duplicated region for block: B:93:0x0260  */
    /* JADX WARN: Unsupported multi-entry loop pattern (BACK_EDGE: B:118:0x02e6 -> B:120:0x02e9). Please submit an issue!!! */
    /* JADX WARN: Unsupported multi-entry loop pattern (BACK_EDGE: B:221:0x052d -> B:235:0x05c2). Please submit an issue!!! */
    /* JADX WARN: Unsupported multi-entry loop pattern (BACK_EDGE: B:227:0x0584 -> B:234:0x05c0). Please submit an issue!!! */
    /* JADX WARN: Unsupported multi-entry loop pattern (BACK_EDGE: B:231:0x059f -> B:232:0x05a0). Please submit an issue!!! */
    /* JADX WARN: Unsupported multi-entry loop pattern (BACK_EDGE: B:233:0x05bb -> B:234:0x05c0). Please submit an issue!!! */
    /* JADX WARN: Unsupported multi-entry loop pattern (BACK_EDGE: B:86:0x0244 -> B:122:0x02f0). Please submit an issue!!! */
    /* JADX WARN: Unsupported multi-entry loop pattern (BACK_EDGE: B:88:0x024b -> B:122:0x02f0). Please submit an issue!!! */
    /* JADX WARN: Unsupported multi-entry loop pattern (BACK_EDGE: B:90:0x024f -> B:91:0x025a). Please submit an issue!!! */
    /*
        Code decompiled incorrectly, please refer to instructions dump.
        To view partially-correct code enable 'Show inconsistent code' option in preferences
    */
    public final java.lang.Object q(int r37, int r38, int r39, defpackage.q70<? super defpackage.te4> r40) {
        /*
            Method dump skipped, instructions count: 1582
            To view this dump change 'Code comments level' option to 'DEBUG'
        */
        throw new UnsupportedOperationException("Method not decompiled: net.safemoon.androidwallet.viewmodels.CollectibleViewModel.q(int, int, int, q70):java.lang.Object");
    }

    /* JADX WARN: Removed duplicated region for block: B:10:0x002a  */
    /* JADX WARN: Removed duplicated region for block: B:20:0x0060  */
    /* JADX WARN: Removed duplicated region for block: B:38:0x00a5  */
    /* JADX WARN: Removed duplicated region for block: B:49:0x00ca  */
    /* JADX WARN: Removed duplicated region for block: B:80:0x0177  */
    /* JADX WARN: Removed duplicated region for block: B:83:0x018b A[RETURN] */
    /* JADX WARN: Unsupported multi-entry loop pattern (BACK_EDGE: B:74:0x015d -> B:75:0x015f). Please submit an issue!!! */
    /*
        Code decompiled incorrectly, please refer to instructions dump.
        To view partially-correct code enable 'Show inconsistent code' option in preferences
    */
    public final java.lang.Object r(int r36, long r37, java.lang.String r39, int r40, int r41, defpackage.q70<? super defpackage.te4> r42) {
        /*
            Method dump skipped, instructions count: 399
            To view this dump change 'Code comments level' option to 'DEBUG'
        */
        throw new UnsupportedOperationException("Method not decompiled: net.safemoon.androidwallet.viewmodels.CollectibleViewModel.r(int, long, java.lang.String, int, int, q70):java.lang.Object");
    }

    public final LiveData<List<RoomNFT>> s(RoomCollection roomCollection, int i, int i2) {
        fs1.f(roomCollection, "collection");
        as.b(ej4.a(this), tp0.b(), null, new CollectibleViewModel$fetchNfts$1(this, roomCollection, null), 2, null);
        NftDataSource G = G();
        Long id = roomCollection.getId();
        return G.i(id == null ? 0L : id.longValue());
    }

    public final Wallet u() {
        return (Wallet) this.c.getValue();
    }

    public final Double v() {
        return this.r;
    }

    public final CollectionDataSource w() {
        return (CollectionDataSource) this.d.getValue();
    }

    public final ApplicationRoomDatabase x() {
        return (ApplicationRoomDatabase) this.b.getValue();
    }

    public final gb2<String> y() {
        return this.o;
    }

    public final gb2<LoadingState> z() {
        return this.n;
    }
}
