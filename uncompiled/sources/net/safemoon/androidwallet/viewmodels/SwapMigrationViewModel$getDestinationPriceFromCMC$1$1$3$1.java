package net.safemoon.androidwallet.viewmodels;

import com.github.mikephil.charting.utils.Utils;
import com.google.gson.Gson;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import kotlin.coroutines.jvm.internal.SuspendLambda;
import kotlin.coroutines.jvm.internal.a;
import net.safemoon.androidwallet.model.Coin;
import net.safemoon.androidwallet.model.swap.Swap;

/* compiled from: SwapMigrationViewModel.kt */
@a(c = "net.safemoon.androidwallet.viewmodels.SwapMigrationViewModel$getDestinationPriceFromCMC$1$1$3$1", f = "SwapMigrationViewModel.kt", l = {}, m = "invokeSuspend")
/* loaded from: classes2.dex */
public final class SwapMigrationViewModel$getDestinationPriceFromCMC$1$1$3$1 extends SuspendLambda implements hd1<c90, q70<? super te4>, Object> {
    public final /* synthetic */ JsonObject $it;
    public final /* synthetic */ Swap $swap;
    public int label;
    public final /* synthetic */ SwapMigrationViewModel this$0;

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public SwapMigrationViewModel$getDestinationPriceFromCMC$1$1$3$1(SwapMigrationViewModel swapMigrationViewModel, JsonObject jsonObject, Swap swap, q70<? super SwapMigrationViewModel$getDestinationPriceFromCMC$1$1$3$1> q70Var) {
        super(2, q70Var);
        this.this$0 = swapMigrationViewModel;
        this.$it = jsonObject;
        this.$swap = swap;
    }

    @Override // kotlin.coroutines.jvm.internal.BaseContinuationImpl
    public final q70<te4> create(Object obj, q70<?> q70Var) {
        return new SwapMigrationViewModel$getDestinationPriceFromCMC$1$1$3$1(this.this$0, this.$it, this.$swap, q70Var);
    }

    @Override // defpackage.hd1
    public final Object invoke(c90 c90Var, q70<? super te4> q70Var) {
        return ((SwapMigrationViewModel$getDestinationPriceFromCMC$1$1$3$1) create(c90Var, q70Var)).invokeSuspend(te4.a);
    }

    @Override // kotlin.coroutines.jvm.internal.BaseContinuationImpl
    public final Object invokeSuspend(Object obj) {
        JsonObject asJsonObject;
        gs1.d();
        if (this.label == 0) {
            o83.b(obj);
            try {
                this.this$0.L().setValue(hr.b(Utils.DOUBLE_EPSILON));
                gb2<Double> L = this.this$0.L();
                Gson gson = new Gson();
                JsonObject jsonObject = this.$it;
                if (jsonObject == null) {
                    asJsonObject = null;
                } else {
                    String str = this.$swap.symbol;
                    fs1.e(str, "swap.symbol");
                    asJsonObject = jsonObject.getAsJsonObject(kt.e(str));
                }
                L.setValue(((Coin) gson.fromJson((JsonElement) asJsonObject, (Class<Object>) Coin.class)).getQuote().getUSD().getPrice());
            } catch (Exception unused) {
                this.this$0.L().setValue(hr.b(Utils.DOUBLE_EPSILON));
            }
            return te4.a;
        }
        throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
    }
}
