package net.safemoon.androidwallet.viewmodels;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import kotlin.Pair;
import net.safemoon.androidwallet.common.TokenType;
import net.safemoon.androidwallet.database.room.ApplicationRoomDatabase;
import net.safemoon.androidwallet.model.token.abstraction.IToken;

/* compiled from: AddNewTokensViewModel.kt */
/* loaded from: classes2.dex */
public final class AddNewTokensViewModel extends dj4 {
    public final pl1 a;
    public final sm1 b;
    public final bn1 c;
    public final yc0 d;
    public gb2<List<q9>> e;
    public String f;
    public TokenType g;
    public List<? extends IToken> h;
    public final List<IToken> i;
    public final List<String> j;

    public AddNewTokensViewModel(pl1 pl1Var, sm1 sm1Var, bn1 bn1Var, yc0 yc0Var) {
        fs1.f(pl1Var, "allTokenListRepository");
        fs1.f(sm1Var, "tokenDisplayModelMapper");
        fs1.f(bn1Var, "userTokenListRepository");
        fs1.f(yc0Var, "customTokenDataSource");
        this.a = pl1Var;
        this.b = sm1Var;
        this.c = bn1Var;
        this.d = yc0Var;
        this.e = new gb2<>(b20.g());
        this.f = "";
        this.g = TokenType.BEP_20;
        this.i = new ArrayList();
        ArrayList arrayList = new ArrayList();
        for (Pair<TokenType, List<String>> pair : ApplicationRoomDatabase.n.d()) {
            arrayList.addAll(pair.getSecond());
        }
        te4 te4Var = te4.a;
        this.j = arrayList;
    }

    public final IToken i(String str) {
        List<? extends IToken> list = this.h;
        Object obj = null;
        if (list == null) {
            fs1.r("selectedTokenList");
            list = null;
        }
        Iterator<T> it = list.iterator();
        while (true) {
            if (!it.hasNext()) {
                break;
            }
            Object next = it.next();
            if (fs1.b(((IToken) next).getSymbolWithType(), str)) {
                obj = next;
                break;
            }
        }
        return (IToken) obj;
    }

    public final gb2<List<q9>> j() {
        return this.e;
    }

    public final void k(q9 q9Var, boolean z) {
        IToken i = i(q9Var.h());
        if (i == null) {
            return;
        }
        if (z) {
            this.c.i(i);
        } else if (this.j.contains(q9Var.h())) {
        } else {
            this.c.h(i);
        }
    }

    public final void l(TokenType tokenType) {
        fs1.f(tokenType, "type");
        this.g = tokenType;
        as.b(ej4.a(this), null, null, new AddNewTokensViewModel$loadTokens$1(this, null), 3, null);
    }

    public final void m(q9 q9Var, boolean z) {
        fs1.f(q9Var, "item");
        as.b(ej4.a(this), null, null, new AddNewTokensViewModel$onItemChanged$1(this, q9Var, z, null), 3, null);
    }

    public final void n(String str) {
        fs1.f(str, "phrase");
        this.f = str;
        as.b(ej4.a(this), null, null, new AddNewTokensViewModel$onSearch$1(this, null), 3, null);
    }

    public final void o() {
        l(this.g);
    }

    public final void p(q9 q9Var) {
        fs1.f(q9Var, "item");
        g20.z(this.i, new AddNewTokensViewModel$removeDeletableItem$1(q9Var));
    }
}
