package net.safemoon.androidwallet.viewmodels;

import kotlin.coroutines.jvm.internal.SuspendLambda;
import kotlin.coroutines.jvm.internal.a;
import net.safemoon.androidwallet.model.priceAlert.PriceAlertToken;

/* compiled from: CryptoPriceAlertViewModel.kt */
@a(c = "net.safemoon.androidwallet.viewmodels.CryptoPriceAlertViewModel$deletePriceAlert$1", f = "CryptoPriceAlertViewModel.kt", l = {128, 141}, m = "invokeSuspend")
/* loaded from: classes2.dex */
public final class CryptoPriceAlertViewModel$deletePriceAlert$1 extends SuspendLambda implements hd1<c90, q70<? super te4>, Object> {
    public final /* synthetic */ int $position;
    public final /* synthetic */ PriceAlertToken $priceAlertData;
    public int label;
    public final /* synthetic */ CryptoPriceAlertViewModel this$0;

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public CryptoPriceAlertViewModel$deletePriceAlert$1(CryptoPriceAlertViewModel cryptoPriceAlertViewModel, PriceAlertToken priceAlertToken, int i, q70<? super CryptoPriceAlertViewModel$deletePriceAlert$1> q70Var) {
        super(2, q70Var);
        this.this$0 = cryptoPriceAlertViewModel;
        this.$priceAlertData = priceAlertToken;
        this.$position = i;
    }

    @Override // kotlin.coroutines.jvm.internal.BaseContinuationImpl
    public final q70<te4> create(Object obj, q70<?> q70Var) {
        return new CryptoPriceAlertViewModel$deletePriceAlert$1(this.this$0, this.$priceAlertData, this.$position, q70Var);
    }

    @Override // defpackage.hd1
    public final Object invoke(c90 c90Var, q70<? super te4> q70Var) {
        return ((CryptoPriceAlertViewModel$deletePriceAlert$1) create(c90Var, q70Var)).invokeSuspend(te4.a);
    }

    /* JADX WARN: Removed duplicated region for block: B:56:0x00e7  */
    /* JADX WARN: Removed duplicated region for block: B:57:0x00e8 A[Catch: Exception -> 0x012c, TryCatch #0 {Exception -> 0x012c, blocks: (B:6:0x0010, B:54:0x00e3, B:59:0x00f2, B:61:0x00f9, B:63:0x010a, B:64:0x011d, B:57:0x00e8, B:10:0x001d, B:16:0x0043, B:18:0x004b, B:20:0x0051, B:30:0x008e, B:31:0x0092, B:33:0x009c, B:43:0x00bf, B:47:0x00cb, B:51:0x00d9, B:50:0x00d4, B:46:0x00c7, B:36:0x00b2, B:23:0x0061, B:24:0x006c, B:26:0x0072, B:28:0x0088, B:13:0x0029), top: B:68:0x000a }] */
    /* JADX WARN: Removed duplicated region for block: B:59:0x00f2 A[Catch: Exception -> 0x012c, TryCatch #0 {Exception -> 0x012c, blocks: (B:6:0x0010, B:54:0x00e3, B:59:0x00f2, B:61:0x00f9, B:63:0x010a, B:64:0x011d, B:57:0x00e8, B:10:0x001d, B:16:0x0043, B:18:0x004b, B:20:0x0051, B:30:0x008e, B:31:0x0092, B:33:0x009c, B:43:0x00bf, B:47:0x00cb, B:51:0x00d9, B:50:0x00d4, B:46:0x00c7, B:36:0x00b2, B:23:0x0061, B:24:0x006c, B:26:0x0072, B:28:0x0088, B:13:0x0029), top: B:68:0x000a }] */
    /* JADX WARN: Removed duplicated region for block: B:60:0x00f8  */
    /* JADX WARN: Removed duplicated region for block: B:63:0x010a A[Catch: Exception -> 0x012c, TryCatch #0 {Exception -> 0x012c, blocks: (B:6:0x0010, B:54:0x00e3, B:59:0x00f2, B:61:0x00f9, B:63:0x010a, B:64:0x011d, B:57:0x00e8, B:10:0x001d, B:16:0x0043, B:18:0x004b, B:20:0x0051, B:30:0x008e, B:31:0x0092, B:33:0x009c, B:43:0x00bf, B:47:0x00cb, B:51:0x00d9, B:50:0x00d4, B:46:0x00c7, B:36:0x00b2, B:23:0x0061, B:24:0x006c, B:26:0x0072, B:28:0x0088, B:13:0x0029), top: B:68:0x000a }] */
    /* JADX WARN: Removed duplicated region for block: B:64:0x011d A[Catch: Exception -> 0x012c, TRY_LEAVE, TryCatch #0 {Exception -> 0x012c, blocks: (B:6:0x0010, B:54:0x00e3, B:59:0x00f2, B:61:0x00f9, B:63:0x010a, B:64:0x011d, B:57:0x00e8, B:10:0x001d, B:16:0x0043, B:18:0x004b, B:20:0x0051, B:30:0x008e, B:31:0x0092, B:33:0x009c, B:43:0x00bf, B:47:0x00cb, B:51:0x00d9, B:50:0x00d4, B:46:0x00c7, B:36:0x00b2, B:23:0x0061, B:24:0x006c, B:26:0x0072, B:28:0x0088, B:13:0x0029), top: B:68:0x000a }] */
    @Override // kotlin.coroutines.jvm.internal.BaseContinuationImpl
    /*
        Code decompiled incorrectly, please refer to instructions dump.
        To view partially-correct code enable 'Show inconsistent code' option in preferences
    */
    public final java.lang.Object invokeSuspend(java.lang.Object r15) {
        /*
            Method dump skipped, instructions count: 308
            To view this dump change 'Code comments level' option to 'DEBUG'
        */
        throw new UnsupportedOperationException("Method not decompiled: net.safemoon.androidwallet.viewmodels.CryptoPriceAlertViewModel$deletePriceAlert$1.invokeSuspend(java.lang.Object):java.lang.Object");
    }
}
