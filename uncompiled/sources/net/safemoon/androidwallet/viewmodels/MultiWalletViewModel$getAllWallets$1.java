package net.safemoon.androidwallet.viewmodels;

import java.util.List;
import kotlin.coroutines.jvm.internal.SuspendLambda;
import kotlin.coroutines.jvm.internal.a;
import net.safemoon.androidwallet.model.wallets.Wallet;
import net.safemoon.androidwallet.repository.WalletDataSource;

/* compiled from: MultiWalletViewModel.kt */
@a(c = "net.safemoon.androidwallet.viewmodels.MultiWalletViewModel$getAllWallets$1", f = "MultiWalletViewModel.kt", l = {}, m = "invokeSuspend")
/* loaded from: classes2.dex */
public final class MultiWalletViewModel$getAllWallets$1 extends SuspendLambda implements hd1<c90, q70<? super te4>, Object> {
    public final /* synthetic */ tc1<List<Wallet>, te4> $list;
    public int label;
    public final /* synthetic */ MultiWalletViewModel this$0;

    /* compiled from: MultiWalletViewModel.kt */
    @a(c = "net.safemoon.androidwallet.viewmodels.MultiWalletViewModel$getAllWallets$1$1", f = "MultiWalletViewModel.kt", l = {56}, m = "invokeSuspend")
    /* renamed from: net.safemoon.androidwallet.viewmodels.MultiWalletViewModel$getAllWallets$1$1  reason: invalid class name */
    /* loaded from: classes2.dex */
    public static final class AnonymousClass1 extends SuspendLambda implements hd1<c90, q70<? super te4>, Object> {
        public final /* synthetic */ tc1<List<Wallet>, te4> $list;
        public Object L$0;
        public int label;
        public final /* synthetic */ MultiWalletViewModel this$0;

        /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
        /* JADX WARN: Multi-variable type inference failed */
        public AnonymousClass1(tc1<? super List<Wallet>, te4> tc1Var, MultiWalletViewModel multiWalletViewModel, q70<? super AnonymousClass1> q70Var) {
            super(2, q70Var);
            this.$list = tc1Var;
            this.this$0 = multiWalletViewModel;
        }

        @Override // kotlin.coroutines.jvm.internal.BaseContinuationImpl
        public final q70<te4> create(Object obj, q70<?> q70Var) {
            return new AnonymousClass1(this.$list, this.this$0, q70Var);
        }

        @Override // defpackage.hd1
        public final Object invoke(c90 c90Var, q70<? super te4> q70Var) {
            return ((AnonymousClass1) create(c90Var, q70Var)).invokeSuspend(te4.a);
        }

        /* JADX WARN: Multi-variable type inference failed */
        /* JADX WARN: Type inference failed for: r0v2, types: [tc1] */
        @Override // kotlin.coroutines.jvm.internal.BaseContinuationImpl
        public final Object invokeSuspend(Object obj) {
            WalletDataSource s;
            tc1<List<Wallet>, te4> tc1Var;
            Object d = gs1.d();
            int i = this.label;
            if (i == 0) {
                o83.b(obj);
                tc1<List<Wallet>, te4> tc1Var2 = this.$list;
                s = this.this$0.s();
                this.L$0 = tc1Var2;
                this.label = 1;
                Object c = s.c(this);
                if (c == d) {
                    return d;
                }
                tc1Var = tc1Var2;
                obj = c;
            } else if (i != 1) {
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            } else {
                tc1Var = (tc1) this.L$0;
                o83.b(obj);
            }
            tc1Var.invoke(obj);
            return te4.a;
        }
    }

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    /* JADX WARN: Multi-variable type inference failed */
    public MultiWalletViewModel$getAllWallets$1(MultiWalletViewModel multiWalletViewModel, tc1<? super List<Wallet>, te4> tc1Var, q70<? super MultiWalletViewModel$getAllWallets$1> q70Var) {
        super(2, q70Var);
        this.this$0 = multiWalletViewModel;
        this.$list = tc1Var;
    }

    @Override // kotlin.coroutines.jvm.internal.BaseContinuationImpl
    public final q70<te4> create(Object obj, q70<?> q70Var) {
        return new MultiWalletViewModel$getAllWallets$1(this.this$0, this.$list, q70Var);
    }

    @Override // defpackage.hd1
    public final Object invoke(c90 c90Var, q70<? super te4> q70Var) {
        return ((MultiWalletViewModel$getAllWallets$1) create(c90Var, q70Var)).invokeSuspend(te4.a);
    }

    @Override // kotlin.coroutines.jvm.internal.BaseContinuationImpl
    public final Object invokeSuspend(Object obj) {
        gs1.d();
        if (this.label == 0) {
            o83.b(obj);
            as.b(ej4.a(this.this$0), null, null, new AnonymousClass1(this.$list, this.this$0, null), 3, null);
            return te4.a;
        }
        throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
    }
}
