package net.safemoon.androidwallet.viewmodels;

import kotlin.coroutines.jvm.internal.ContinuationImpl;
import kotlin.coroutines.jvm.internal.a;

/* compiled from: CollectibleViewModel.kt */
@a(c = "net.safemoon.androidwallet.viewmodels.CollectibleViewModel", f = "CollectibleViewModel.kt", l = {447, 465, 468}, m = "fetchNFTsBySlug")
/* loaded from: classes2.dex */
public final class CollectibleViewModel$fetchNFTsBySlug$1 extends ContinuationImpl {
    public int I$0;
    public long J$0;
    public Object L$0;
    public Object L$1;
    public Object L$2;
    public int label;
    public /* synthetic */ Object result;
    public final /* synthetic */ CollectibleViewModel this$0;

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public CollectibleViewModel$fetchNFTsBySlug$1(CollectibleViewModel collectibleViewModel, q70<? super CollectibleViewModel$fetchNFTsBySlug$1> q70Var) {
        super(q70Var);
        this.this$0 = collectibleViewModel;
    }

    @Override // kotlin.coroutines.jvm.internal.BaseContinuationImpl
    public final Object invokeSuspend(Object obj) {
        Object r;
        this.result = obj;
        this.label |= Integer.MIN_VALUE;
        r = this.this$0.r(0, 0L, null, 0, 0, this);
        return r;
    }
}
