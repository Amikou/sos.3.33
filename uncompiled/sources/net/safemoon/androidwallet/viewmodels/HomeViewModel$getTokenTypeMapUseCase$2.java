package net.safemoon.androidwallet.viewmodels;

import android.app.Application;
import android.content.Context;
import kotlin.jvm.internal.Lambda;

/* compiled from: HomeViewModel.kt */
/* loaded from: classes2.dex */
public final class HomeViewModel$getTokenTypeMapUseCase$2 extends Lambda implements rc1<mf1> {
    public final /* synthetic */ Application $application;

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public HomeViewModel$getTokenTypeMapUseCase$2(Application application) {
        super(0);
        this.$application = application;
    }

    @Override // defpackage.rc1
    public final mf1 invoke() {
        Context applicationContext = this.$application.getApplicationContext();
        fs1.e(applicationContext, "application.applicationContext");
        return new mf1(applicationContext);
    }
}
