package net.safemoon.androidwallet.viewmodels;

import kotlin.coroutines.jvm.internal.SuspendLambda;
import kotlin.coroutines.jvm.internal.a;
import kotlinx.coroutines.CoroutineDispatcher;
import net.safemoon.androidwallet.R;
import net.safemoon.androidwallet.model.common.LoadingState;
import net.safemoon.androidwallet.viewmodels.SwapMigrationViewModel;
import net.safemoon.androidwallet.viewmodels.blockChainClass.MigrationToV2;

/* compiled from: SwapMigrationViewModel.kt */
@a(c = "net.safemoon.androidwallet.viewmodels.SwapMigrationViewModel$swapTokens$1", f = "SwapMigrationViewModel.kt", l = {1364}, m = "invokeSuspend")
/* loaded from: classes2.dex */
public final class SwapMigrationViewModel$swapTokens$1 extends SuspendLambda implements hd1<c90, q70<? super te4>, Object> {
    public int label;
    public final /* synthetic */ SwapMigrationViewModel this$0;

    /* compiled from: SwapMigrationViewModel.kt */
    @kotlin.coroutines.jvm.internal.a(c = "net.safemoon.androidwallet.viewmodels.SwapMigrationViewModel$swapTokens$1$1", f = "SwapMigrationViewModel.kt", l = {1458}, m = "invokeSuspend")
    /* renamed from: net.safemoon.androidwallet.viewmodels.SwapMigrationViewModel$swapTokens$1$1  reason: invalid class name */
    /* loaded from: classes2.dex */
    public static final class AnonymousClass1 extends SuspendLambda implements hd1<c90, q70<? super te4>, Object> {
        public int label;
        public final /* synthetic */ SwapMigrationViewModel this$0;

        /* compiled from: SwapMigrationViewModel.kt */
        @kotlin.coroutines.jvm.internal.a(c = "net.safemoon.androidwallet.viewmodels.SwapMigrationViewModel$swapTokens$1$1$1", f = "SwapMigrationViewModel.kt", l = {1366, 1366}, m = "invokeSuspend")
        /* renamed from: net.safemoon.androidwallet.viewmodels.SwapMigrationViewModel$swapTokens$1$1$1  reason: invalid class name and collision with other inner class name */
        /* loaded from: classes2.dex */
        public static final class C02361 extends SuspendLambda implements hd1<k71<? super w84>, q70<? super te4>, Object> {
            private /* synthetic */ Object L$0;
            public int label;
            public final /* synthetic */ SwapMigrationViewModel this$0;

            /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
            public C02361(SwapMigrationViewModel swapMigrationViewModel, q70<? super C02361> q70Var) {
                super(2, q70Var);
                this.this$0 = swapMigrationViewModel;
            }

            @Override // kotlin.coroutines.jvm.internal.BaseContinuationImpl
            public final q70<te4> create(Object obj, q70<?> q70Var) {
                C02361 c02361 = new C02361(this.this$0, q70Var);
                c02361.L$0 = obj;
                return c02361;
            }

            @Override // defpackage.hd1
            public final Object invoke(k71<? super w84> k71Var, q70<? super te4> q70Var) {
                return ((C02361) create(k71Var, q70Var)).invokeSuspend(te4.a);
            }

            @Override // kotlin.coroutines.jvm.internal.BaseContinuationImpl
            public final Object invokeSuspend(Object obj) {
                k71 k71Var;
                MigrationToV2 migrationToV2;
                Object d = gs1.d();
                int i = this.label;
                if (i == 0) {
                    o83.b(obj);
                    k71Var = (k71) this.L$0;
                    migrationToV2 = this.this$0.w;
                    fs1.d(migrationToV2);
                    this.L$0 = k71Var;
                    this.label = 1;
                    obj = migrationToV2.g(this);
                    if (obj == d) {
                        return d;
                    }
                } else if (i != 1) {
                    if (i != 2) {
                        throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
                    }
                    o83.b(obj);
                    return te4.a;
                } else {
                    k71Var = (k71) this.L$0;
                    o83.b(obj);
                }
                this.L$0 = null;
                this.label = 2;
                if (k71Var.emit(obj, this) == d) {
                    return d;
                }
                return te4.a;
            }
        }

        /* compiled from: SwapMigrationViewModel.kt */
        @kotlin.coroutines.jvm.internal.a(c = "net.safemoon.androidwallet.viewmodels.SwapMigrationViewModel$swapTokens$1$1$2", f = "SwapMigrationViewModel.kt", l = {}, m = "invokeSuspend")
        /* renamed from: net.safemoon.androidwallet.viewmodels.SwapMigrationViewModel$swapTokens$1$1$2  reason: invalid class name */
        /* loaded from: classes2.dex */
        public static final class AnonymousClass2 extends SuspendLambda implements kd1<k71<? super w84>, Throwable, q70<? super te4>, Object> {
            public /* synthetic */ Object L$0;
            public int label;
            public final /* synthetic */ SwapMigrationViewModel this$0;

            /* compiled from: SwapMigrationViewModel.kt */
            @kotlin.coroutines.jvm.internal.a(c = "net.safemoon.androidwallet.viewmodels.SwapMigrationViewModel$swapTokens$1$1$2$1", f = "SwapMigrationViewModel.kt", l = {}, m = "invokeSuspend")
            /* renamed from: net.safemoon.androidwallet.viewmodels.SwapMigrationViewModel$swapTokens$1$1$2$1  reason: invalid class name and collision with other inner class name */
            /* loaded from: classes2.dex */
            public static final class C02371 extends SuspendLambda implements hd1<c90, q70<? super te4>, Object> {
                public final /* synthetic */ Throwable $it;
                public int label;
                public final /* synthetic */ SwapMigrationViewModel this$0;

                /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
                public C02371(SwapMigrationViewModel swapMigrationViewModel, Throwable th, q70<? super C02371> q70Var) {
                    super(2, q70Var);
                    this.this$0 = swapMigrationViewModel;
                    this.$it = th;
                }

                @Override // kotlin.coroutines.jvm.internal.BaseContinuationImpl
                public final q70<te4> create(Object obj, q70<?> q70Var) {
                    return new C02371(this.this$0, this.$it, q70Var);
                }

                @Override // defpackage.hd1
                public final Object invoke(c90 c90Var, q70<? super te4> q70Var) {
                    return ((C02371) create(c90Var, q70Var)).invokeSuspend(te4.a);
                }

                @Override // kotlin.coroutines.jvm.internal.BaseContinuationImpl
                public final Object invokeSuspend(Object obj) {
                    gs1.d();
                    if (this.label == 0) {
                        o83.b(obj);
                        gb2<SwapMigrationViewModel.d> f0 = this.this$0.f0();
                        String localizedMessage = this.$it.getLocalizedMessage();
                        if (localizedMessage == null && (localizedMessage = this.$it.getMessage()) == null) {
                            localizedMessage = this.this$0.a0(R.string.swap_could_not);
                        }
                        f0.setValue(new SwapMigrationViewModel.d(false, localizedMessage));
                        this.this$0.R().setValue(LoadingState.Normal);
                        this.$it.printStackTrace();
                        return te4.a;
                    }
                    throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
                }
            }

            /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
            public AnonymousClass2(SwapMigrationViewModel swapMigrationViewModel, q70<? super AnonymousClass2> q70Var) {
                super(3, q70Var);
                this.this$0 = swapMigrationViewModel;
            }

            @Override // defpackage.kd1
            public final Object invoke(k71<? super w84> k71Var, Throwable th, q70<? super te4> q70Var) {
                AnonymousClass2 anonymousClass2 = new AnonymousClass2(this.this$0, q70Var);
                anonymousClass2.L$0 = th;
                return anonymousClass2.invokeSuspend(te4.a);
            }

            @Override // kotlin.coroutines.jvm.internal.BaseContinuationImpl
            public final Object invokeSuspend(Object obj) {
                gs1.d();
                if (this.label == 0) {
                    o83.b(obj);
                    as.b(ej4.a(this.this$0), null, null, new C02371(this.this$0, (Throwable) this.L$0, null), 3, null);
                    return te4.a;
                }
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
        }

        /* compiled from: Collect.kt */
        /* renamed from: net.safemoon.androidwallet.viewmodels.SwapMigrationViewModel$swapTokens$1$1$a */
        /* loaded from: classes2.dex */
        public static final class a implements k71<w84> {
            public final /* synthetic */ SwapMigrationViewModel a;

            public a(SwapMigrationViewModel swapMigrationViewModel) {
                this.a = swapMigrationViewModel;
            }

            @Override // defpackage.k71
            public Object emit(w84 w84Var, q70<? super te4> q70Var) {
                st1 b;
                b = as.b(ej4.a(this.a), null, null, new SwapMigrationViewModel$swapTokens$1$1$3$1(this.a, w84Var, null), 3, null);
                return b == gs1.d() ? b : te4.a;
            }
        }

        /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
        public AnonymousClass1(SwapMigrationViewModel swapMigrationViewModel, q70<? super AnonymousClass1> q70Var) {
            super(2, q70Var);
            this.this$0 = swapMigrationViewModel;
        }

        @Override // kotlin.coroutines.jvm.internal.BaseContinuationImpl
        public final q70<te4> create(Object obj, q70<?> q70Var) {
            return new AnonymousClass1(this.this$0, q70Var);
        }

        @Override // defpackage.hd1
        public final Object invoke(c90 c90Var, q70<? super te4> q70Var) {
            return ((AnonymousClass1) create(c90Var, q70Var)).invokeSuspend(te4.a);
        }

        @Override // kotlin.coroutines.jvm.internal.BaseContinuationImpl
        public final Object invokeSuspend(Object obj) {
            Object d = gs1.d();
            int i = this.label;
            if (i == 0) {
                o83.b(obj);
                j71 c = n71.c(n71.n(new C02361(this.this$0, null)), new AnonymousClass2(this.this$0, null));
                a aVar = new a(this.this$0);
                this.label = 1;
                if (c.a(aVar, this) == d) {
                    return d;
                }
            } else if (i != 1) {
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            } else {
                o83.b(obj);
            }
            return te4.a;
        }
    }

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public SwapMigrationViewModel$swapTokens$1(SwapMigrationViewModel swapMigrationViewModel, q70<? super SwapMigrationViewModel$swapTokens$1> q70Var) {
        super(2, q70Var);
        this.this$0 = swapMigrationViewModel;
    }

    @Override // kotlin.coroutines.jvm.internal.BaseContinuationImpl
    public final q70<te4> create(Object obj, q70<?> q70Var) {
        return new SwapMigrationViewModel$swapTokens$1(this.this$0, q70Var);
    }

    @Override // defpackage.hd1
    public final Object invoke(c90 c90Var, q70<? super te4> q70Var) {
        return ((SwapMigrationViewModel$swapTokens$1) create(c90Var, q70Var)).invokeSuspend(te4.a);
    }

    @Override // kotlin.coroutines.jvm.internal.BaseContinuationImpl
    public final Object invokeSuspend(Object obj) {
        Object d = gs1.d();
        int i = this.label;
        if (i == 0) {
            o83.b(obj);
            this.this$0.R().setValue(LoadingState.Loading);
            CoroutineDispatcher b = tp0.b();
            AnonymousClass1 anonymousClass1 = new AnonymousClass1(this.this$0, null);
            this.label = 1;
            if (kotlinx.coroutines.a.e(b, anonymousClass1, this) == d) {
                return d;
            }
        } else if (i != 1) {
            throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        } else {
            o83.b(obj);
        }
        return te4.a;
    }
}
