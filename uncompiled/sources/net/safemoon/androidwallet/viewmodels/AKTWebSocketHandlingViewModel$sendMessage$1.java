package net.safemoon.androidwallet.viewmodels;

import kotlin.coroutines.jvm.internal.SuspendLambda;
import kotlin.coroutines.jvm.internal.a;
import kotlinx.coroutines.CoroutineDispatcher;

/* compiled from: AKTWebSocketHandlingViewModel.kt */
@a(c = "net.safemoon.androidwallet.viewmodels.AKTWebSocketHandlingViewModel$sendMessage$1", f = "AKTWebSocketHandlingViewModel.kt", l = {49}, m = "invokeSuspend")
/* loaded from: classes2.dex */
public final class AKTWebSocketHandlingViewModel$sendMessage$1 extends SuspendLambda implements hd1<c90, q70<? super te4>, Object> {
    public final /* synthetic */ String $LSON;
    public final /* synthetic */ mo4 $webClient;
    public int label;
    public final /* synthetic */ AKTWebSocketHandlingViewModel this$0;

    /* compiled from: AKTWebSocketHandlingViewModel.kt */
    @a(c = "net.safemoon.androidwallet.viewmodels.AKTWebSocketHandlingViewModel$sendMessage$1$1", f = "AKTWebSocketHandlingViewModel.kt", l = {}, m = "invokeSuspend")
    /* renamed from: net.safemoon.androidwallet.viewmodels.AKTWebSocketHandlingViewModel$sendMessage$1$1  reason: invalid class name */
    /* loaded from: classes2.dex */
    public static final class AnonymousClass1 extends SuspendLambda implements hd1<c90, q70<? super te4>, Object> {
        public final /* synthetic */ String $LSON;
        public final /* synthetic */ mo4 $webClient;
        public int label;
        public final /* synthetic */ AKTWebSocketHandlingViewModel this$0;

        /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
        public AnonymousClass1(mo4 mo4Var, String str, AKTWebSocketHandlingViewModel aKTWebSocketHandlingViewModel, q70<? super AnonymousClass1> q70Var) {
            super(2, q70Var);
            this.$webClient = mo4Var;
            this.$LSON = str;
            this.this$0 = aKTWebSocketHandlingViewModel;
        }

        @Override // kotlin.coroutines.jvm.internal.BaseContinuationImpl
        public final q70<te4> create(Object obj, q70<?> q70Var) {
            return new AnonymousClass1(this.$webClient, this.$LSON, this.this$0, q70Var);
        }

        @Override // defpackage.hd1
        public final Object invoke(c90 c90Var, q70<? super te4> q70Var) {
            return ((AnonymousClass1) create(c90Var, q70Var)).invokeSuspend(te4.a);
        }

        @Override // kotlin.coroutines.jvm.internal.BaseContinuationImpl
        public final Object invokeSuspend(Object obj) {
            gs1.d();
            if (this.label == 0) {
                o83.b(obj);
                mo4 mo4Var = this.$webClient;
                String str = this.$LSON;
                AKTWebSocketHandlingViewModel aKTWebSocketHandlingViewModel = this.this$0;
                if (mo4Var.isOpen()) {
                    mo4Var.send(str);
                } else {
                    aKTWebSocketHandlingViewModel.j();
                    aKTWebSocketHandlingViewModel.b = new AKTWebSocketHandlingViewModel$sendMessage$1$1$1$1(aKTWebSocketHandlingViewModel, mo4Var, str);
                }
                return te4.a;
            }
            throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        }
    }

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public AKTWebSocketHandlingViewModel$sendMessage$1(mo4 mo4Var, String str, AKTWebSocketHandlingViewModel aKTWebSocketHandlingViewModel, q70<? super AKTWebSocketHandlingViewModel$sendMessage$1> q70Var) {
        super(2, q70Var);
        this.$webClient = mo4Var;
        this.$LSON = str;
        this.this$0 = aKTWebSocketHandlingViewModel;
    }

    @Override // kotlin.coroutines.jvm.internal.BaseContinuationImpl
    public final q70<te4> create(Object obj, q70<?> q70Var) {
        return new AKTWebSocketHandlingViewModel$sendMessage$1(this.$webClient, this.$LSON, this.this$0, q70Var);
    }

    @Override // defpackage.hd1
    public final Object invoke(c90 c90Var, q70<? super te4> q70Var) {
        return ((AKTWebSocketHandlingViewModel$sendMessage$1) create(c90Var, q70Var)).invokeSuspend(te4.a);
    }

    @Override // kotlin.coroutines.jvm.internal.BaseContinuationImpl
    public final Object invokeSuspend(Object obj) {
        Object d = gs1.d();
        int i = this.label;
        if (i == 0) {
            o83.b(obj);
            CoroutineDispatcher b = tp0.b();
            AnonymousClass1 anonymousClass1 = new AnonymousClass1(this.$webClient, this.$LSON, this.this$0, null);
            this.label = 1;
            if (kotlinx.coroutines.a.e(b, anonymousClass1, this) == d) {
                return d;
            }
        } else if (i != 1) {
            throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        } else {
            o83.b(obj);
        }
        return te4.a;
    }
}
