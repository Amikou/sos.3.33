package net.safemoon.androidwallet.viewmodels;

import java.math.BigDecimal;
import java.math.BigInteger;
import kotlin.coroutines.jvm.internal.SuspendLambda;
import kotlin.coroutines.jvm.internal.a;
import kotlin.jvm.internal.Ref$ObjectRef;
import kotlinx.coroutines.CoroutineDispatcher;

/* compiled from: SwapViewModel.kt */
@a(c = "net.safemoon.androidwallet.viewmodels.SwapViewModel$findMaximumAmountInForNativeToken$1", f = "SwapViewModel.kt", l = {165}, m = "invokeSuspend")
/* loaded from: classes2.dex */
public final class SwapViewModel$findMaximumAmountInForNativeToken$1 extends SuspendLambda implements hd1<c90, q70<? super te4>, Object> {
    public final /* synthetic */ BigDecimal $amountIn;
    public final /* synthetic */ up3<BigDecimal> $callBack;
    public int label;
    public final /* synthetic */ SwapViewModel this$0;

    /* compiled from: SwapViewModel.kt */
    @kotlin.coroutines.jvm.internal.a(c = "net.safemoon.androidwallet.viewmodels.SwapViewModel$findMaximumAmountInForNativeToken$1$1", f = "SwapViewModel.kt", l = {2166}, m = "invokeSuspend")
    /* renamed from: net.safemoon.androidwallet.viewmodels.SwapViewModel$findMaximumAmountInForNativeToken$1$1  reason: invalid class name */
    /* loaded from: classes2.dex */
    public static final class AnonymousClass1 extends SuspendLambda implements hd1<c90, q70<? super te4>, Object> {
        public final /* synthetic */ BigDecimal $amountIn;
        public final /* synthetic */ up3<BigDecimal> $callBack;
        public int label;
        public final /* synthetic */ SwapViewModel this$0;

        /* compiled from: SwapViewModel.kt */
        @kotlin.coroutines.jvm.internal.a(c = "net.safemoon.androidwallet.viewmodels.SwapViewModel$findMaximumAmountInForNativeToken$1$1$1", f = "SwapViewModel.kt", l = {168, 170, 220, 223}, m = "invokeSuspend")
        /* renamed from: net.safemoon.androidwallet.viewmodels.SwapViewModel$findMaximumAmountInForNativeToken$1$1$1  reason: invalid class name and collision with other inner class name */
        /* loaded from: classes2.dex */
        public static final class C02411 extends SuspendLambda implements hd1<k71<? super BigInteger>, q70<? super te4>, Object> {
            public final /* synthetic */ BigDecimal $amountIn;
            public final /* synthetic */ Ref$ObjectRef<BigInteger> $tradeFee;
            private /* synthetic */ Object L$0;
            public Object L$1;
            public Object L$2;
            public Object L$3;
            public Object L$4;
            public int label;
            public final /* synthetic */ SwapViewModel this$0;

            /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
            public C02411(SwapViewModel swapViewModel, BigDecimal bigDecimal, Ref$ObjectRef<BigInteger> ref$ObjectRef, q70<? super C02411> q70Var) {
                super(2, q70Var);
                this.this$0 = swapViewModel;
                this.$amountIn = bigDecimal;
                this.$tradeFee = ref$ObjectRef;
            }

            @Override // kotlin.coroutines.jvm.internal.BaseContinuationImpl
            public final q70<te4> create(Object obj, q70<?> q70Var) {
                C02411 c02411 = new C02411(this.this$0, this.$amountIn, this.$tradeFee, q70Var);
                c02411.L$0 = obj;
                return c02411;
            }

            @Override // defpackage.hd1
            public final Object invoke(k71<? super BigInteger> k71Var, q70<? super te4> q70Var) {
                return ((C02411) create(k71Var, q70Var)).invokeSuspend(te4.a);
            }

            /* JADX WARN: Removed duplicated region for block: B:27:0x010c A[LOOP:0: B:25:0x0106->B:27:0x010c, LOOP_END] */
            /* JADX WARN: Removed duplicated region for block: B:30:0x0121  */
            /* JADX WARN: Removed duplicated region for block: B:31:0x014d  */
            /* JADX WARN: Removed duplicated region for block: B:34:0x0185  */
            /* JADX WARN: Removed duplicated region for block: B:40:0x01c8  */
            /* JADX WARN: Removed duplicated region for block: B:43:0x01de A[RETURN] */
            /* JADX WARN: Removed duplicated region for block: B:44:0x01df  */
            /* JADX WARN: Removed duplicated region for block: B:47:0x01e5  */
            /* JADX WARN: Type inference failed for: r12v12, types: [T, java.lang.Object] */
            @Override // kotlin.coroutines.jvm.internal.BaseContinuationImpl
            /*
                Code decompiled incorrectly, please refer to instructions dump.
                To view partially-correct code enable 'Show inconsistent code' option in preferences
            */
            public final java.lang.Object invokeSuspend(java.lang.Object r24) {
                /*
                    Method dump skipped, instructions count: 499
                    To view this dump change 'Code comments level' option to 'DEBUG'
                */
                throw new UnsupportedOperationException("Method not decompiled: net.safemoon.androidwallet.viewmodels.SwapViewModel$findMaximumAmountInForNativeToken$1.AnonymousClass1.C02411.invokeSuspend(java.lang.Object):java.lang.Object");
            }
        }

        /* compiled from: SwapViewModel.kt */
        @kotlin.coroutines.jvm.internal.a(c = "net.safemoon.androidwallet.viewmodels.SwapViewModel$findMaximumAmountInForNativeToken$1$1$2", f = "SwapViewModel.kt", l = {}, m = "invokeSuspend")
        /* renamed from: net.safemoon.androidwallet.viewmodels.SwapViewModel$findMaximumAmountInForNativeToken$1$1$2  reason: invalid class name */
        /* loaded from: classes2.dex */
        public static final class AnonymousClass2 extends SuspendLambda implements kd1<k71<? super BigInteger>, Throwable, q70<? super te4>, Object> {
            public final /* synthetic */ up3<BigDecimal> $callBack;
            public int label;

            /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
            public AnonymousClass2(up3<BigDecimal> up3Var, q70<? super AnonymousClass2> q70Var) {
                super(3, q70Var);
                this.$callBack = up3Var;
            }

            @Override // defpackage.kd1
            public final Object invoke(k71<? super BigInteger> k71Var, Throwable th, q70<? super te4> q70Var) {
                return new AnonymousClass2(this.$callBack, q70Var).invokeSuspend(te4.a);
            }

            @Override // kotlin.coroutines.jvm.internal.BaseContinuationImpl
            public final Object invokeSuspend(Object obj) {
                gs1.d();
                if (this.label == 0) {
                    o83.b(obj);
                    this.$callBack.postValue(null);
                    return te4.a;
                }
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
        }

        /* compiled from: Collect.kt */
        /* renamed from: net.safemoon.androidwallet.viewmodels.SwapViewModel$findMaximumAmountInForNativeToken$1$1$a */
        /* loaded from: classes2.dex */
        public static final class a implements k71<BigInteger> {
            public final /* synthetic */ SwapViewModel a;
            public final /* synthetic */ Ref$ObjectRef f0;
            public final /* synthetic */ up3 g0;

            public a(SwapViewModel swapViewModel, Ref$ObjectRef ref$ObjectRef, up3 up3Var) {
                this.a = swapViewModel;
                this.f0 = ref$ObjectRef;
                this.g0 = up3Var;
            }

            @Override // defpackage.k71
            public Object emit(BigInteger bigInteger, q70<? super te4> q70Var) {
                BigInteger j0;
                BigInteger bigInteger2 = bigInteger;
                if (bigInteger2 != null) {
                    BigDecimal multiply = new BigDecimal(bigInteger2).multiply(new BigDecimal(1.1d));
                    fs1.e(multiply, "this.multiply(other)");
                    BigInteger bigInteger3 = multiply.toBigInteger();
                    j0 = this.a.j0();
                    fs1.e(j0, "getGasPrice()");
                    fs1.e(bigInteger3, "newEstimateGas");
                    BigInteger multiply2 = j0.multiply(bigInteger3);
                    fs1.e(multiply2, "this.multiply(other)");
                    BigInteger add = multiply2.add((BigInteger) this.f0.element);
                    fs1.e(add, "getGasPrice() * newEstimateGas).add(tradeFee)");
                    BigDecimal r = e30.r(add, 18);
                    up3 up3Var = this.g0;
                    fs1.e(r, "gasLimitAmount");
                    BigDecimal valueOf = BigDecimal.valueOf(0.001d);
                    fs1.e(valueOf, "valueOf(0.001)");
                    BigDecimal add2 = r.add(valueOf);
                    fs1.e(add2, "this.add(other)");
                    up3Var.postValue(add2);
                } else {
                    this.g0.postValue(null);
                }
                return te4.a;
            }
        }

        /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
        public AnonymousClass1(SwapViewModel swapViewModel, BigDecimal bigDecimal, up3<BigDecimal> up3Var, q70<? super AnonymousClass1> q70Var) {
            super(2, q70Var);
            this.this$0 = swapViewModel;
            this.$amountIn = bigDecimal;
            this.$callBack = up3Var;
        }

        @Override // kotlin.coroutines.jvm.internal.BaseContinuationImpl
        public final q70<te4> create(Object obj, q70<?> q70Var) {
            return new AnonymousClass1(this.this$0, this.$amountIn, this.$callBack, q70Var);
        }

        @Override // defpackage.hd1
        public final Object invoke(c90 c90Var, q70<? super te4> q70Var) {
            return ((AnonymousClass1) create(c90Var, q70Var)).invokeSuspend(te4.a);
        }

        /* JADX WARN: Type inference failed for: r1v1, types: [T, java.math.BigInteger] */
        @Override // kotlin.coroutines.jvm.internal.BaseContinuationImpl
        public final Object invokeSuspend(Object obj) {
            Object d = gs1.d();
            int i = this.label;
            if (i == 0) {
                o83.b(obj);
                Ref$ObjectRef ref$ObjectRef = new Ref$ObjectRef();
                ref$ObjectRef.element = BigInteger.ZERO;
                j71 c = n71.c(n71.n(new C02411(this.this$0, this.$amountIn, ref$ObjectRef, null)), new AnonymousClass2(this.$callBack, null));
                a aVar = new a(this.this$0, ref$ObjectRef, this.$callBack);
                this.label = 1;
                if (c.a(aVar, this) == d) {
                    return d;
                }
            } else if (i != 1) {
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            } else {
                o83.b(obj);
            }
            return te4.a;
        }
    }

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public SwapViewModel$findMaximumAmountInForNativeToken$1(SwapViewModel swapViewModel, BigDecimal bigDecimal, up3<BigDecimal> up3Var, q70<? super SwapViewModel$findMaximumAmountInForNativeToken$1> q70Var) {
        super(2, q70Var);
        this.this$0 = swapViewModel;
        this.$amountIn = bigDecimal;
        this.$callBack = up3Var;
    }

    @Override // kotlin.coroutines.jvm.internal.BaseContinuationImpl
    public final q70<te4> create(Object obj, q70<?> q70Var) {
        return new SwapViewModel$findMaximumAmountInForNativeToken$1(this.this$0, this.$amountIn, this.$callBack, q70Var);
    }

    @Override // defpackage.hd1
    public final Object invoke(c90 c90Var, q70<? super te4> q70Var) {
        return ((SwapViewModel$findMaximumAmountInForNativeToken$1) create(c90Var, q70Var)).invokeSuspend(te4.a);
    }

    @Override // kotlin.coroutines.jvm.internal.BaseContinuationImpl
    public final Object invokeSuspend(Object obj) {
        Object d = gs1.d();
        int i = this.label;
        if (i == 0) {
            o83.b(obj);
            CoroutineDispatcher b = tp0.b();
            AnonymousClass1 anonymousClass1 = new AnonymousClass1(this.this$0, this.$amountIn, this.$callBack, null);
            this.label = 1;
            if (kotlinx.coroutines.a.e(b, anonymousClass1, this) == d) {
                return d;
            }
        } else if (i != 1) {
            throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        } else {
            o83.b(obj);
        }
        return te4.a;
    }
}
