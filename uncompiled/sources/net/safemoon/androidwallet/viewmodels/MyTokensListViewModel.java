package net.safemoon.androidwallet.viewmodels;

import android.app.Application;
import androidx.lifecycle.LiveData;
import com.github.mikephil.charting.utils.Utils;
import com.google.firebase.analytics.FirebaseAnalytics;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Iterator;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Objects;
import java.util.Set;
import kotlin.Pair;
import net.safemoon.androidwallet.common.TokenType;
import net.safemoon.androidwallet.database.room.ApplicationRoomDatabase;
import net.safemoon.androidwallet.model.swap.AllSwapTokens;
import net.safemoon.androidwallet.model.swap.Data;
import net.safemoon.androidwallet.model.swap.Swap;
import net.safemoon.androidwallet.model.token.abstraction.IToken;
import net.safemoon.androidwallet.model.token.room.RoomToken;
import net.safemoon.androidwallet.ui.displayModel.UserTokenItemDisplayModel;
import net.safemoon.androidwallet.viewmodels.MyTokensListViewModel;
import retrofit2.b;
import retrofit2.n;

/* compiled from: MyTokensListViewModel.kt */
/* loaded from: classes2.dex */
public final class MyTokensListViewModel extends gd {
    public final an1 b;
    public final bn1 c;
    public final LiveData<RoomToken> d;
    public final ArrayList<Swap> e;
    public final sy1 f;
    public final sy1 g;
    public final sy1 h;
    public final gb2<List<UserTokenItemDisplayModel>> i;
    public final LiveData<List<RoomToken>> j;
    public final Set<IToken> k;
    public final sy1 l;
    public final g72<Double> m;
    public final sy1 n;
    public final tl2<List<RoomToken>> o;
    public final tl2<List<UserTokenItemDisplayModel>> p;
    public final tl2<List<RoomToken>> q;
    public LiveData<List<RoomToken>> r;
    public final List<String> s;
    public List<Pair<String, Integer>> t;

    /* compiled from: MyTokensListViewModel.kt */
    /* loaded from: classes2.dex */
    public static final class a implements wu<AllSwapTokens> {
        public a() {
        }

        @Override // defpackage.wu
        public void a(b<AllSwapTokens> bVar, Throwable th) {
            fs1.f(bVar, "call");
            fs1.f(th, "t");
        }

        @Override // defpackage.wu
        public void b(b<AllSwapTokens> bVar, n<AllSwapTokens> nVar) {
            Data data;
            fs1.f(bVar, "call");
            fs1.f(nVar, "response");
            if (!nVar.e() || nVar.a() == null) {
                return;
            }
            MyTokensListViewModel.this.y().clear();
            ArrayList<Swap> y = MyTokensListViewModel.this.y();
            AllSwapTokens a = nVar.a();
            Object obj = null;
            List<Swap> tokens = (a == null || (data = a.getData()) == null) ? null : data.getTokens();
            fs1.d(tokens);
            y.addAll(tokens);
            MyTokensListViewModel.this.y().addAll(b30.a.l());
            Iterator<T> it = MyTokensListViewModel.this.y().iterator();
            while (true) {
                if (!it.hasNext()) {
                    break;
                }
                Object next = it.next();
                if (fs1.b(((Swap) next).symbol, "WETH")) {
                    obj = next;
                    break;
                }
            }
            b30.a.A((Swap) obj);
        }
    }

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public MyTokensListViewModel(Application application, an1 an1Var, bn1 bn1Var) {
        super(application);
        fs1.f(application, "application");
        fs1.f(an1Var, "userTokenDisplayModelMapper");
        fs1.f(bn1Var, "userTokenListRepository");
        this.b = an1Var;
        this.c = bn1Var;
        G();
        this.d = bn1Var.d("BEP_SAFEMOON");
        bn1Var.d("BEP_SAFEMOON_V2");
        this.e = new ArrayList<>();
        this.f = zy1.a(new MyTokensListViewModel$address$2(this));
        this.g = zy1.a(new MyTokensListViewModel$credentails$2(this));
        this.h = zy1.a(new MyTokensListViewModel$customTokenDataSource$2(this));
        this.i = new gb2<>(new ArrayList());
        LiveData<List<RoomToken>> k = bn1Var.k();
        this.j = k;
        this.k = new LinkedHashSet();
        this.l = zy1.a(new MyTokensListViewModel$totalBalanceForDefault$2(this));
        g72<Double> g72Var = new g72<>();
        this.m = g72Var;
        this.n = zy1.a(new MyTokensListViewModel$analytics$2(application));
        this.o = new tl2() { // from class: hc2
            @Override // defpackage.tl2
            public final void onChanged(Object obj) {
                MyTokensListViewModel.L(MyTokensListViewModel.this, (List) obj);
            }
        };
        this.p = new tl2() { // from class: gc2
            @Override // defpackage.tl2
            public final void onChanged(Object obj) {
                MyTokensListViewModel.o(MyTokensListViewModel.this, (List) obj);
            }
        };
        g72Var.setValue(Double.valueOf((double) Utils.DOUBLE_EPSILON));
        C().observeForever(new tl2() { // from class: dc2
            @Override // defpackage.tl2
            public final void onChanged(Object obj) {
                MyTokensListViewModel.h(MyTokensListViewModel.this, (Boolean) obj);
            }
        });
        k.observeForever(new tl2() { // from class: ec2
            @Override // defpackage.tl2
            public final void onChanged(Object obj) {
                MyTokensListViewModel.g(MyTokensListViewModel.this, (List) obj);
            }
        });
        this.q = new tl2() { // from class: fc2
            @Override // defpackage.tl2
            public final void onChanged(Object obj) {
                MyTokensListViewModel.F(MyTokensListViewModel.this, (List) obj);
            }
        };
        ArrayList arrayList = new ArrayList();
        for (Pair<TokenType, List<String>> pair : ApplicationRoomDatabase.n.d()) {
            arrayList.addAll(pair.getSecond());
        }
        te4 te4Var = te4.a;
        this.s = arrayList;
        this.t = new ArrayList();
    }

    public static final void F(MyTokensListViewModel myTokensListViewModel, List list) {
        fs1.f(myTokensListViewModel, "this$0");
        if (list != null) {
            myTokensListViewModel.A().postValue(myTokensListViewModel.b.a(list));
        }
    }

    public static final void L(MyTokensListViewModel myTokensListViewModel, List list) {
        fs1.f(myTokensListViewModel, "this$0");
        if (list != null) {
            double d = Utils.DOUBLE_EPSILON;
            Iterator it = list.iterator();
            while (it.hasNext()) {
                RoomToken roomToken = (RoomToken) it.next();
                try {
                    d += roomToken.getNativeBalance() * roomToken.getPriceInUsdt();
                } catch (Exception unused) {
                }
            }
            myTokensListViewModel.B().setValue(Double.valueOf(d));
        }
    }

    public static final void g(MyTokensListViewModel myTokensListViewModel, List list) {
        Object obj;
        fs1.f(myTokensListViewModel, "this$0");
        if (list != null) {
            ArrayList arrayList = new ArrayList();
            Iterator it = list.iterator();
            while (true) {
                Object obj2 = null;
                if (!it.hasNext()) {
                    break;
                }
                Object next = it.next();
                RoomToken roomToken = (RoomToken) next;
                Iterator<T> it2 = myTokensListViewModel.k.iterator();
                while (true) {
                    if (!it2.hasNext()) {
                        break;
                    }
                    Object next2 = it2.next();
                    if (fs1.b(((IToken) next2).getSymbolWithType(), roomToken.getSymbolWithType())) {
                        obj2 = next2;
                        break;
                    }
                }
                if (obj2 == null) {
                    arrayList.add(next);
                }
            }
            Set<IToken> set = myTokensListViewModel.k;
            ArrayList<IToken> arrayList2 = new ArrayList();
            for (Object obj3 : set) {
                IToken iToken = (IToken) obj3;
                Iterator it3 = list.iterator();
                while (true) {
                    if (!it3.hasNext()) {
                        obj = null;
                        break;
                    }
                    obj = it3.next();
                    if (fs1.b(iToken.getSymbolWithType(), ((RoomToken) obj).getSymbolWithType())) {
                        break;
                    }
                }
                if (obj == null) {
                    arrayList2.add(obj3);
                }
            }
            for (IToken iToken2 : arrayList2) {
                myTokensListViewModel.k.remove(iToken2);
            }
            myTokensListViewModel.k.addAll(arrayList);
            myTokensListViewModel.O(arrayList);
            myTokensListViewModel.M(arrayList);
            myTokensListViewModel.p(list);
        }
    }

    public static final void h(MyTokensListViewModel myTokensListViewModel, Boolean bool) {
        fs1.f(myTokensListViewModel, "this$0");
        fs1.e(bool, "it");
        if (bool.booleanValue()) {
            myTokensListViewModel.r().removeObserver(myTokensListViewModel.D());
            myTokensListViewModel.A().observeForever(myTokensListViewModel.w());
            return;
        }
        myTokensListViewModel.r().observeForever(myTokensListViewModel.D());
        myTokensListViewModel.A().removeObserver(myTokensListViewModel.w());
    }

    public static final void o(MyTokensListViewModel myTokensListViewModel, List list) {
        fs1.f(myTokensListViewModel, "this$0");
        if (list != null) {
            double d = Utils.DOUBLE_EPSILON;
            Iterator it = list.iterator();
            while (it.hasNext()) {
                UserTokenItemDisplayModel userTokenItemDisplayModel = (UserTokenItemDisplayModel) it.next();
                try {
                    d += userTokenItemDisplayModel.getNativeBalance() * userTokenItemDisplayModel.getPriceInUsdt();
                } catch (Exception unused) {
                }
            }
            myTokensListViewModel.B().setValue(Double.valueOf(d));
        }
    }

    public final gb2<List<UserTokenItemDisplayModel>> A() {
        return this.i;
    }

    public final g72<Double> B() {
        return this.m;
    }

    public final gb2<Boolean> C() {
        return (gb2) this.l.getValue();
    }

    public final tl2<List<RoomToken>> D() {
        return this.o;
    }

    public final void E(TokenType tokenType) {
        LiveData<List<RoomToken>> liveData = this.r;
        if (liveData != null) {
            liveData.removeObserver(this.q);
        }
        LiveData<List<RoomToken>> a2 = this.c.a(tokenType);
        this.r = a2;
        fs1.d(a2);
        a2.observeForever(this.q);
    }

    public final void G() {
        a4.k().m().n(new a());
    }

    public final void H(boolean z) {
        C().postValue(Boolean.valueOf(z));
        bo3.n(a(), "DEFAULT_CHAIN_BALANCE", Boolean.valueOf(z));
    }

    public final void I(UserTokenItemDisplayModel userTokenItemDisplayModel) {
        fs1.f(userTokenItemDisplayModel, "item");
        if (this.s.contains(userTokenItemDisplayModel.getSymbolWithType())) {
            return;
        }
        as.b(ej4.a(this), tp0.b(), null, new MyTokensListViewModel$onItemRemove$1(this, z(userTokenItemDisplayModel.getSymbol()), null), 2, null);
    }

    public final void J(TokenType tokenType) {
        fs1.f(tokenType, "tokenType");
        bo3.o(a(), "DEFAULT_GATEWAY", tokenType.name());
        E(tokenType);
    }

    public final void K() {
        O(j20.k0(this.k));
        M(j20.k0(this.k));
    }

    public final void M(List<? extends IToken> list) {
        if (list.isEmpty()) {
            return;
        }
        for (IToken iToken : list) {
            as.b(qg1.a, null, null, new MyTokensListViewModel$updateNativeBalances$1$1(iToken, this, null), 3, null);
        }
    }

    public final void N(List<UserTokenItemDisplayModel> list) {
        fs1.f(list, "orders");
        ArrayList arrayList = new ArrayList();
        int i = 0;
        for (Object obj : list) {
            int i2 = i + 1;
            if (i < 0) {
                b20.p();
            }
            arrayList.add(new Pair(((UserTokenItemDisplayModel) obj).getSymbolWithType(), Integer.valueOf(i)));
            i = i2;
        }
        if (fs1.b(this.t, arrayList)) {
            return;
        }
        bn1 bn1Var = this.c;
        Object[] array = arrayList.toArray(new Pair[0]);
        Objects.requireNonNull(array, "null cannot be cast to non-null type kotlin.Array<T>");
        Pair[] pairArr = (Pair[]) array;
        bn1Var.j((Pair[]) Arrays.copyOf(pairArr, pairArr.length));
        this.t = arrayList;
    }

    public final void O(List<? extends IToken> list) {
        if (list.isEmpty()) {
            return;
        }
        as.b(qg1.a, null, null, new MyTokensListViewModel$updatePrice$1(list, this, null), 3, null);
    }

    public final void P() {
        gb2<List<UserTokenItemDisplayModel>> gb2Var = this.i;
        gb2Var.postValue(gb2Var.getValue());
    }

    public final void Q() {
        g72<Double> g72Var = this.m;
        g72Var.postValue(g72Var.getValue());
    }

    public final void n() {
        this.k.clear();
    }

    public final void p(List<RoomToken> list) {
        Object obj;
        fs1.f(list, "tokens");
        String[] strArr = {"BEP_USDT", "BEP_AQUAGOAT"};
        for (int i = 0; i < 2; i++) {
            String str = strArr[i];
            Iterator<T> it = list.iterator();
            while (true) {
                if (!it.hasNext()) {
                    obj = null;
                    break;
                }
                obj = it.next();
                if (fs1.b(str, ((RoomToken) obj).getSymbolWithType())) {
                    break;
                }
            }
            RoomToken roomToken = (RoomToken) obj;
            if (roomToken != null) {
                I(new UserTokenItemDisplayModel(roomToken.getSymbolWithType(), 0, roomToken.getName(), roomToken.getSymbol(), roomToken.getContractAddress(), roomToken.getDecimals(), roomToken.getChainId(), roomToken.getAllowSwap(), roomToken.getPriceInUsdt(), roomToken.getNativeBalance(), roomToken.getPercentChange1h(), Utils.DOUBLE_EPSILON, "", null));
            }
        }
    }

    public final String q() {
        return (String) this.f.getValue();
    }

    public final LiveData<List<RoomToken>> r() {
        return this.j;
    }

    public final FirebaseAnalytics s() {
        return (FirebaseAnalytics) this.n.getValue();
    }

    public final ma0 t() {
        return (ma0) this.g.getValue();
    }

    public final yc0 u() {
        return (yc0) this.h.getValue();
    }

    public final List<String> v() {
        return this.s;
    }

    public final tl2<List<UserTokenItemDisplayModel>> w() {
        return this.p;
    }

    public final LiveData<RoomToken> x() {
        return this.d;
    }

    public final ArrayList<Swap> y() {
        return this.e;
    }

    /* JADX WARN: Multi-variable type inference failed */
    public final IToken z(String str) {
        List<RoomToken> value = this.j.getValue();
        RoomToken roomToken = null;
        if (value != null) {
            Iterator<T> it = value.iterator();
            while (true) {
                if (!it.hasNext()) {
                    break;
                }
                Object next = it.next();
                if (fs1.b(((RoomToken) next).getSymbol(), str)) {
                    roomToken = next;
                    break;
                }
            }
            roomToken = roomToken;
        }
        if (roomToken != null) {
            return roomToken;
        }
        throw new NullPointerException("Token for symbol=" + str + " nit found");
    }
}
