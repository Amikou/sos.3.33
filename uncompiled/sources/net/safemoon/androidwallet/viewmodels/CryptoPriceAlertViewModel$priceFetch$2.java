package net.safemoon.androidwallet.viewmodels;

import android.app.Application;
import kotlin.jvm.internal.Lambda;
import net.safemoon.androidwallet.repository.PriceFetchDataSource;

/* compiled from: CryptoPriceAlertViewModel.kt */
/* loaded from: classes2.dex */
public final class CryptoPriceAlertViewModel$priceFetch$2 extends Lambda implements rc1<PriceFetchDataSource> {
    public final /* synthetic */ Application $application;

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public CryptoPriceAlertViewModel$priceFetch$2(Application application) {
        super(0);
        this.$application = application;
    }

    @Override // defpackage.rc1
    public final PriceFetchDataSource invoke() {
        return new PriceFetchDataSource(this.$application);
    }
}
