package net.safemoon.androidwallet.viewmodels;

import kotlin.coroutines.jvm.internal.ContinuationImpl;
import kotlin.coroutines.jvm.internal.a;

/* compiled from: ReflectionTrackerViewModel.kt */
@a(c = "net.safemoon.androidwallet.viewmodels.ReflectionTrackerViewModel", f = "ReflectionTrackerViewModel.kt", l = {409}, m = "retryGetBalance")
/* loaded from: classes2.dex */
public final class ReflectionTrackerViewModel$retryGetBalance$1 extends ContinuationImpl {
    public int label;
    public /* synthetic */ Object result;
    public final /* synthetic */ ReflectionTrackerViewModel this$0;

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public ReflectionTrackerViewModel$retryGetBalance$1(ReflectionTrackerViewModel reflectionTrackerViewModel, q70<? super ReflectionTrackerViewModel$retryGetBalance$1> q70Var) {
        super(q70Var);
        this.this$0 = reflectionTrackerViewModel;
    }

    @Override // kotlin.coroutines.jvm.internal.BaseContinuationImpl
    public final Object invokeSuspend(Object obj) {
        Object z;
        this.result = obj;
        this.label |= Integer.MIN_VALUE;
        z = this.this$0.z(0L, null, null, this);
        return z;
    }
}
