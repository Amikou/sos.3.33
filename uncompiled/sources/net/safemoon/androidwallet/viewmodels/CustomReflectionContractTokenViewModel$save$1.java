package net.safemoon.androidwallet.viewmodels;

import kotlin.coroutines.jvm.internal.SuspendLambda;
import kotlin.coroutines.jvm.internal.a;
import kotlinx.coroutines.CoroutineDispatcher;
import net.safemoon.androidwallet.viewmodels.CustomReflectionContractTokenViewModel;

/* compiled from: CustomReflectionContractTokenViewModel.kt */
@a(c = "net.safemoon.androidwallet.viewmodels.CustomReflectionContractTokenViewModel$save$1", f = "CustomReflectionContractTokenViewModel.kt", l = {135}, m = "invokeSuspend")
/* loaded from: classes2.dex */
public final class CustomReflectionContractTokenViewModel$save$1 extends SuspendLambda implements hd1<c90, q70<? super te4>, Object> {
    public final /* synthetic */ tc1<CustomReflectionContractTokenViewModel.SaveReturnCode, te4> $callBack;
    public final /* synthetic */ String $contractAddress;
    public final /* synthetic */ String $contractDecimal;
    public final /* synthetic */ String $contractName;
    public final /* synthetic */ String $contractSymbol;
    public int label;
    public final /* synthetic */ CustomReflectionContractTokenViewModel this$0;

    /* compiled from: CustomReflectionContractTokenViewModel.kt */
    @a(c = "net.safemoon.androidwallet.viewmodels.CustomReflectionContractTokenViewModel$save$1$1", f = "CustomReflectionContractTokenViewModel.kt", l = {139, 160}, m = "invokeSuspend")
    /* renamed from: net.safemoon.androidwallet.viewmodels.CustomReflectionContractTokenViewModel$save$1$1  reason: invalid class name */
    /* loaded from: classes2.dex */
    public static final class AnonymousClass1 extends SuspendLambda implements hd1<c90, q70<? super te4>, Object> {
        public final /* synthetic */ tc1<CustomReflectionContractTokenViewModel.SaveReturnCode, te4> $callBack;
        public final /* synthetic */ String $contractAddress;
        public final /* synthetic */ String $contractDecimal;
        public final /* synthetic */ String $contractName;
        public final /* synthetic */ String $contractSymbol;
        public int label;
        public final /* synthetic */ CustomReflectionContractTokenViewModel this$0;

        /* compiled from: CustomReflectionContractTokenViewModel.kt */
        @a(c = "net.safemoon.androidwallet.viewmodels.CustomReflectionContractTokenViewModel$save$1$1$1", f = "CustomReflectionContractTokenViewModel.kt", l = {}, m = "invokeSuspend")
        /* renamed from: net.safemoon.androidwallet.viewmodels.CustomReflectionContractTokenViewModel$save$1$1$1  reason: invalid class name and collision with other inner class name */
        /* loaded from: classes2.dex */
        public static final class C02231 extends SuspendLambda implements hd1<c90, q70<? super te4>, Object> {
            public final /* synthetic */ tc1<CustomReflectionContractTokenViewModel.SaveReturnCode, te4> $callBack;
            public final /* synthetic */ Long $savedId;
            public int label;

            /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
            /* JADX WARN: Multi-variable type inference failed */
            public C02231(Long l, tc1<? super CustomReflectionContractTokenViewModel.SaveReturnCode, te4> tc1Var, q70<? super C02231> q70Var) {
                super(2, q70Var);
                this.$savedId = l;
                this.$callBack = tc1Var;
            }

            @Override // kotlin.coroutines.jvm.internal.BaseContinuationImpl
            public final q70<te4> create(Object obj, q70<?> q70Var) {
                return new C02231(this.$savedId, this.$callBack, q70Var);
            }

            @Override // defpackage.hd1
            public final Object invoke(c90 c90Var, q70<? super te4> q70Var) {
                return ((C02231) create(c90Var, q70Var)).invokeSuspend(te4.a);
            }

            @Override // kotlin.coroutines.jvm.internal.BaseContinuationImpl
            public final Object invokeSuspend(Object obj) {
                gs1.d();
                if (this.label == 0) {
                    o83.b(obj);
                    Long l = this.$savedId;
                    if (l != null && (l == null || l.longValue() != 0)) {
                        this.$callBack.invoke(null);
                    } else {
                        this.$callBack.invoke(CustomReflectionContractTokenViewModel.SaveReturnCode.ERROR);
                    }
                    return te4.a;
                }
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
        }

        /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
        /* JADX WARN: Multi-variable type inference failed */
        public AnonymousClass1(CustomReflectionContractTokenViewModel customReflectionContractTokenViewModel, String str, String str2, String str3, String str4, tc1<? super CustomReflectionContractTokenViewModel.SaveReturnCode, te4> tc1Var, q70<? super AnonymousClass1> q70Var) {
            super(2, q70Var);
            this.this$0 = customReflectionContractTokenViewModel;
            this.$contractAddress = str;
            this.$contractName = str2;
            this.$contractSymbol = str3;
            this.$contractDecimal = str4;
            this.$callBack = tc1Var;
        }

        @Override // kotlin.coroutines.jvm.internal.BaseContinuationImpl
        public final q70<te4> create(Object obj, q70<?> q70Var) {
            return new AnonymousClass1(this.this$0, this.$contractAddress, this.$contractName, this.$contractSymbol, this.$contractDecimal, this.$callBack, q70Var);
        }

        @Override // defpackage.hd1
        public final Object invoke(c90 c90Var, q70<? super te4> q70Var) {
            return ((AnonymousClass1) create(c90Var, q70Var)).invokeSuspend(te4.a);
        }

        /* JADX WARN: Removed duplicated region for block: B:20:0x0095 A[RETURN] */
        @Override // kotlin.coroutines.jvm.internal.BaseContinuationImpl
        /*
            Code decompiled incorrectly, please refer to instructions dump.
            To view partially-correct code enable 'Show inconsistent code' option in preferences
        */
        public final java.lang.Object invokeSuspend(java.lang.Object r12) {
            /*
                r11 = this;
                java.lang.Object r0 = defpackage.gs1.d()
                int r1 = r11.label
                r2 = 2
                r3 = 1
                if (r1 == 0) goto L1f
                if (r1 == r3) goto L1b
                if (r1 != r2) goto L13
                defpackage.o83.b(r12)
                goto L96
            L13:
                java.lang.IllegalStateException r12 = new java.lang.IllegalStateException
                java.lang.String r0 = "call to 'resume' before 'invoke' with coroutine"
                r12.<init>(r0)
                throw r12
            L1b:
                defpackage.o83.b(r12)
                goto L70
            L1f:
                defpackage.o83.b(r12)
                net.safemoon.androidwallet.viewmodels.CustomReflectionContractTokenViewModel r12 = r11.this$0
                gb2 r12 = r12.r()
                java.lang.Object r12 = r12.getValue()
                if (r12 == 0) goto L45
                net.safemoon.androidwallet.viewmodels.CustomReflectionContractTokenViewModel r12 = r11.this$0
                gb2 r12 = r12.r()
                java.lang.Object r12 = r12.getValue()
                defpackage.fs1.d(r12)
                net.safemoon.androidwallet.model.cmcTokenInfo.TokenDetail r12 = (net.safemoon.androidwallet.model.cmcTokenInfo.TokenDetail) r12
                java.lang.Integer r12 = r12.id
                java.lang.String r12 = java.lang.String.valueOf(r12)
            L43:
                r8 = r12
                goto L77
            L45:
                net.safemoon.androidwallet.utils.ImageUtility r12 = new net.safemoon.androidwallet.utils.ImageUtility
                net.safemoon.androidwallet.viewmodels.CustomReflectionContractTokenViewModel r1 = r11.this$0
                android.app.Application r1 = r1.a()
                java.lang.String r4 = "getApplication()"
                defpackage.fs1.e(r1, r4)
                r12.<init>(r1)
                nn1 r5 = defpackage.nn1.a
                r6 = 0
                r7 = 0
                java.lang.String r1 = r11.$contractAddress
                int r8 = r1.hashCode()
                r9 = 3
                r10 = 0
                android.graphics.Bitmap r1 = defpackage.nn1.b(r5, r6, r7, r8, r9, r10)
                r11.label = r3
                java.lang.String r3 = "Token"
                java.lang.Object r12 = r12.a(r1, r3, r11)
                if (r12 != r0) goto L70
                return r0
            L70:
                java.io.File r12 = (java.io.File) r12
                java.lang.String r12 = r12.getAbsolutePath()
                goto L43
            L77:
                net.safemoon.androidwallet.viewmodels.CustomReflectionContractTokenViewModel$save$1$1$iToken$1 r12 = new net.safemoon.androidwallet.viewmodels.CustomReflectionContractTokenViewModel$save$1$1$iToken$1
                net.safemoon.androidwallet.viewmodels.CustomReflectionContractTokenViewModel r4 = r11.this$0
                java.lang.String r5 = r11.$contractAddress
                java.lang.String r6 = r11.$contractName
                java.lang.String r7 = r11.$contractSymbol
                java.lang.String r9 = r11.$contractDecimal
                r3 = r12
                r3.<init>(r5, r6, r7, r8, r9)
                net.safemoon.androidwallet.viewmodels.CustomReflectionContractTokenViewModel r1 = r11.this$0
                net.safemoon.androidwallet.utils.ReflectionCustomContract r1 = net.safemoon.androidwallet.viewmodels.CustomReflectionContractTokenViewModel.c(r1)
                r11.label = r2
                java.lang.Object r12 = r1.k(r12, r11)
                if (r12 != r0) goto L96
                return r0
            L96:
                java.lang.Long r12 = (java.lang.Long) r12
                net.safemoon.androidwallet.viewmodels.CustomReflectionContractTokenViewModel r0 = r11.this$0
                c90 r1 = defpackage.ej4.a(r0)
                r2 = 0
                r3 = 0
                net.safemoon.androidwallet.viewmodels.CustomReflectionContractTokenViewModel$save$1$1$1 r4 = new net.safemoon.androidwallet.viewmodels.CustomReflectionContractTokenViewModel$save$1$1$1
                tc1<net.safemoon.androidwallet.viewmodels.CustomReflectionContractTokenViewModel$SaveReturnCode, te4> r0 = r11.$callBack
                r5 = 0
                r4.<init>(r12, r0, r5)
                r5 = 3
                r6 = 0
                kotlinx.coroutines.a.b(r1, r2, r3, r4, r5, r6)
                te4 r12 = defpackage.te4.a
                return r12
            */
            throw new UnsupportedOperationException("Method not decompiled: net.safemoon.androidwallet.viewmodels.CustomReflectionContractTokenViewModel$save$1.AnonymousClass1.invokeSuspend(java.lang.Object):java.lang.Object");
        }
    }

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    /* JADX WARN: Multi-variable type inference failed */
    public CustomReflectionContractTokenViewModel$save$1(CustomReflectionContractTokenViewModel customReflectionContractTokenViewModel, String str, String str2, String str3, String str4, tc1<? super CustomReflectionContractTokenViewModel.SaveReturnCode, te4> tc1Var, q70<? super CustomReflectionContractTokenViewModel$save$1> q70Var) {
        super(2, q70Var);
        this.this$0 = customReflectionContractTokenViewModel;
        this.$contractAddress = str;
        this.$contractName = str2;
        this.$contractSymbol = str3;
        this.$contractDecimal = str4;
        this.$callBack = tc1Var;
    }

    @Override // kotlin.coroutines.jvm.internal.BaseContinuationImpl
    public final q70<te4> create(Object obj, q70<?> q70Var) {
        return new CustomReflectionContractTokenViewModel$save$1(this.this$0, this.$contractAddress, this.$contractName, this.$contractSymbol, this.$contractDecimal, this.$callBack, q70Var);
    }

    @Override // defpackage.hd1
    public final Object invoke(c90 c90Var, q70<? super te4> q70Var) {
        return ((CustomReflectionContractTokenViewModel$save$1) create(c90Var, q70Var)).invokeSuspend(te4.a);
    }

    @Override // kotlin.coroutines.jvm.internal.BaseContinuationImpl
    public final Object invokeSuspend(Object obj) {
        Object d = gs1.d();
        int i = this.label;
        if (i == 0) {
            o83.b(obj);
            CoroutineDispatcher b = tp0.b();
            AnonymousClass1 anonymousClass1 = new AnonymousClass1(this.this$0, this.$contractAddress, this.$contractName, this.$contractSymbol, this.$contractDecimal, this.$callBack, null);
            this.label = 1;
            if (kotlinx.coroutines.a.e(b, anonymousClass1, this) == d) {
                return d;
            }
        } else if (i != 1) {
            throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        } else {
            o83.b(obj);
        }
        return te4.a;
    }
}
