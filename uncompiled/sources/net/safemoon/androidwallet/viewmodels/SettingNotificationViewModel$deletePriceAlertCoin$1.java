package net.safemoon.androidwallet.viewmodels;

import kotlin.coroutines.jvm.internal.SuspendLambda;
import kotlin.coroutines.jvm.internal.a;
import net.safemoon.androidwallet.model.priceAlert.PAToken;

/* compiled from: SettingNotificationViewModel.kt */
@a(c = "net.safemoon.androidwallet.viewmodels.SettingNotificationViewModel$deletePriceAlertCoin$1", f = "SettingNotificationViewModel.kt", l = {199, 201, 203, 203, 203}, m = "invokeSuspend")
/* loaded from: classes2.dex */
public final class SettingNotificationViewModel$deletePriceAlertCoin$1 extends SuspendLambda implements hd1<c90, q70<? super te4>, Object> {
    public final /* synthetic */ PAToken $paToken;
    public int I$0;
    public Object L$0;
    public Object L$1;
    public Object L$2;
    public Object L$3;
    public int label;
    public final /* synthetic */ SettingNotificationViewModel this$0;

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public SettingNotificationViewModel$deletePriceAlertCoin$1(SettingNotificationViewModel settingNotificationViewModel, PAToken pAToken, q70<? super SettingNotificationViewModel$deletePriceAlertCoin$1> q70Var) {
        super(2, q70Var);
        this.this$0 = settingNotificationViewModel;
        this.$paToken = pAToken;
    }

    @Override // kotlin.coroutines.jvm.internal.BaseContinuationImpl
    public final q70<te4> create(Object obj, q70<?> q70Var) {
        return new SettingNotificationViewModel$deletePriceAlertCoin$1(this.this$0, this.$paToken, q70Var);
    }

    @Override // defpackage.hd1
    public final Object invoke(c90 c90Var, q70<? super te4> q70Var) {
        return ((SettingNotificationViewModel$deletePriceAlertCoin$1) create(c90Var, q70Var)).invokeSuspend(te4.a);
    }

    /* JADX WARN: Can't wrap try/catch for region: R(9:1|(1:(1:(1:(3:13|14|15)(1:(2:8|9)(2:11|12)))(4:16|17|18|19))(13:39|40|41|42|43|44|45|46|47|48|49|50|(1:52)(1:53)))(2:71|(2:73|74)(4:75|(4:77|78|79|(1:81)(10:82|43|44|45|46|47|48|49|50|(0)(0)))|14|15))|20|21|22|(1:24)|14|15|(1:(0))) */
    /* JADX WARN: Code restructure failed: missing block: B:48:0x0131, code lost:
        r0 = th;
     */
    /* JADX WARN: Code restructure failed: missing block: B:49:0x0132, code lost:
        r6 = r4;
     */
    /* JADX WARN: Code restructure failed: missing block: B:50:0x0134, code lost:
        r6 = r4;
     */
    /* JADX WARN: Removed duplicated region for block: B:42:0x011e A[RETURN] */
    /* JADX WARN: Removed duplicated region for block: B:43:0x011f  */
    /* JADX WARN: Removed duplicated region for block: B:65:0x0161 A[RETURN] */
    /* JADX WARN: Removed duplicated region for block: B:70:0x017a A[RETURN] */
    /* JADX WARN: Removed duplicated region for block: B:89:? A[RETURN, SYNTHETIC] */
    @Override // kotlin.coroutines.jvm.internal.BaseContinuationImpl
    /*
        Code decompiled incorrectly, please refer to instructions dump.
        To view partially-correct code enable 'Show inconsistent code' option in preferences
    */
    public final java.lang.Object invokeSuspend(java.lang.Object r35) {
        /*
            Method dump skipped, instructions count: 382
            To view this dump change 'Code comments level' option to 'DEBUG'
        */
        throw new UnsupportedOperationException("Method not decompiled: net.safemoon.androidwallet.viewmodels.SettingNotificationViewModel$deletePriceAlertCoin$1.invokeSuspend(java.lang.Object):java.lang.Object");
    }
}
