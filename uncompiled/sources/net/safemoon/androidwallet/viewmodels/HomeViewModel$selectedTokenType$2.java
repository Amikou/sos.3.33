package net.safemoon.androidwallet.viewmodels;

import android.app.Application;
import kotlin.jvm.internal.Lambda;
import net.safemoon.androidwallet.common.TokenType;

/* compiled from: HomeViewModel.kt */
/* loaded from: classes2.dex */
public final class HomeViewModel$selectedTokenType$2 extends Lambda implements rc1<gb2<TokenType>> {
    public final /* synthetic */ Application $application;

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public HomeViewModel$selectedTokenType$2(Application application) {
        super(0);
        this.$application = application;
    }

    /* JADX WARN: Can't rename method to resolve collision */
    @Override // defpackage.rc1
    public final gb2<TokenType> invoke() {
        TokenType.a aVar = TokenType.Companion;
        String j = bo3.j(this.$application, "DEFAULT_GATEWAY", TokenType.BEP_20.name());
        fs1.e(j, "getString(\n             …20.name\n                )");
        return new gb2<>(aVar.c(j));
    }
}
