package net.safemoon.androidwallet.viewmodels;

import android.app.Application;
import kotlin.jvm.internal.Lambda;
import net.safemoon.androidwallet.database.room.ApplicationRoomDatabase;

/* compiled from: SettingNotificationViewModel.kt */
/* loaded from: classes2.dex */
public final class SettingNotificationViewModel$coinPriceAlertDataSource$2 extends Lambda implements rc1<m00> {
    public final /* synthetic */ SettingNotificationViewModel this$0;

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public SettingNotificationViewModel$coinPriceAlertDataSource$2(SettingNotificationViewModel settingNotificationViewModel) {
        super(0);
        this.this$0 = settingNotificationViewModel;
    }

    @Override // defpackage.rc1
    public final m00 invoke() {
        ApplicationRoomDatabase.k kVar = ApplicationRoomDatabase.n;
        Application a = this.this$0.a();
        fs1.e(a, "getApplication()");
        return new m00(ApplicationRoomDatabase.k.c(kVar, a, null, 2, null).S());
    }
}
