package net.safemoon.androidwallet.viewmodels;

import kotlin.coroutines.jvm.internal.SuspendLambda;
import kotlin.coroutines.jvm.internal.a;
import kotlinx.coroutines.CoroutineDispatcher;

/* compiled from: AddNewTokensViewModel.kt */
@a(c = "net.safemoon.androidwallet.viewmodels.AddNewTokensViewModel$loadTokens$1", f = "AddNewTokensViewModel.kt", l = {43}, m = "invokeSuspend")
/* loaded from: classes2.dex */
public final class AddNewTokensViewModel$loadTokens$1 extends SuspendLambda implements hd1<c90, q70<? super te4>, Object> {
    public int label;
    public final /* synthetic */ AddNewTokensViewModel this$0;

    /* compiled from: AddNewTokensViewModel.kt */
    @a(c = "net.safemoon.androidwallet.viewmodels.AddNewTokensViewModel$loadTokens$1$1", f = "AddNewTokensViewModel.kt", l = {45, 52, 66}, m = "invokeSuspend")
    /* renamed from: net.safemoon.androidwallet.viewmodels.AddNewTokensViewModel$loadTokens$1$1  reason: invalid class name */
    /* loaded from: classes2.dex */
    public static final class AnonymousClass1 extends SuspendLambda implements hd1<c90, q70<? super te4>, Object> {
        public Object L$0;
        public int label;
        public final /* synthetic */ AddNewTokensViewModel this$0;

        /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
        public AnonymousClass1(AddNewTokensViewModel addNewTokensViewModel, q70<? super AnonymousClass1> q70Var) {
            super(2, q70Var);
            this.this$0 = addNewTokensViewModel;
        }

        @Override // kotlin.coroutines.jvm.internal.BaseContinuationImpl
        public final q70<te4> create(Object obj, q70<?> q70Var) {
            return new AnonymousClass1(this.this$0, q70Var);
        }

        @Override // defpackage.hd1
        public final Object invoke(c90 c90Var, q70<? super te4> q70Var) {
            return ((AnonymousClass1) create(c90Var, q70Var)).invokeSuspend(te4.a);
        }

        /* JADX WARN: Can't wrap try/catch for region: R(19:1|(1:(1:(1:(3:6|7|8)(2:10|11))(16:12|13|14|15|16|(6:71|(6:74|(3:85|(2:86|(2:88|(4:90|(1:92)(1:100)|93|(2:96|97)(1:95))(3:101|102|103))(2:104|105))|(3:99|(2:79|80)(1:82)|81))|77|(0)(0)|81|72)|106|107|(2:110|108)|111)|19|(6:22|(3:24|(2:25|(2:27|(1:29))(2:38|39))|(3:31|(3:33|34|35)(1:37)|36))|40|(0)(0)|36|20)|41|42|(6:45|(2:47|(3:49|(2:51|52)(1:54)|53))(1:56)|55|(0)(0)|53|43)|57|58|(1:60)|61|(1:63)(3:64|7|8)))(1:115))(2:124|(1:126))|116|(1:118)|119|120|(1:122)|16|(1:18)(8:65|68|71|(1:72)|106|107|(1:108)|111)|19|(1:20)|41|42|(1:43)|57|58|(0)|61|(0)(0)) */
        /* JADX WARN: Code restructure failed: missing block: B:68:0x015b, code lost:
            r2 = r10;
         */
        /* JADX WARN: Multi-variable type inference failed */
        /* JADX WARN: Removed duplicated region for block: B:103:0x0269  */
        /* JADX WARN: Removed duplicated region for block: B:106:0x0284 A[RETURN] */
        /* JADX WARN: Removed duplicated region for block: B:107:0x0285  */
        /* JADX WARN: Removed duplicated region for block: B:114:0x01c5 A[SYNTHETIC] */
        /* JADX WARN: Removed duplicated region for block: B:117:0x0168 A[SYNTHETIC] */
        /* JADX WARN: Removed duplicated region for block: B:123:0x0253 A[SYNTHETIC] */
        /* JADX WARN: Removed duplicated region for block: B:127:0x0134 A[SYNTHETIC] */
        /* JADX WARN: Removed duplicated region for block: B:36:0x00cd A[Catch: Exception -> 0x015b, TryCatch #1 {Exception -> 0x015b, blocks: (B:24:0x00a0, B:27:0x00ac, B:30:0x00b4, B:33:0x00bc, B:34:0x00c7, B:36:0x00cd, B:61:0x0131, B:39:0x00e1, B:41:0x00e7, B:42:0x00ef, B:44:0x00f5, B:46:0x0103, B:50:0x0119, B:49:0x0112, B:54:0x0123, B:55:0x0128, B:63:0x0138, B:64:0x013e, B:66:0x0144, B:21:0x008d), top: B:113:0x008d }] */
        /* JADX WARN: Removed duplicated region for block: B:61:0x0131 A[Catch: Exception -> 0x015b, TryCatch #1 {Exception -> 0x015b, blocks: (B:24:0x00a0, B:27:0x00ac, B:30:0x00b4, B:33:0x00bc, B:34:0x00c7, B:36:0x00cd, B:61:0x0131, B:39:0x00e1, B:41:0x00e7, B:42:0x00ef, B:44:0x00f5, B:46:0x0103, B:50:0x0119, B:49:0x0112, B:54:0x0123, B:55:0x0128, B:63:0x0138, B:64:0x013e, B:66:0x0144, B:21:0x008d), top: B:113:0x008d }] */
        /* JADX WARN: Removed duplicated region for block: B:66:0x0144 A[Catch: Exception -> 0x015b, LOOP:5: B:64:0x013e->B:66:0x0144, LOOP_END, TRY_LEAVE, TryCatch #1 {Exception -> 0x015b, blocks: (B:24:0x00a0, B:27:0x00ac, B:30:0x00b4, B:33:0x00bc, B:34:0x00c7, B:36:0x00cd, B:61:0x0131, B:39:0x00e1, B:41:0x00e7, B:42:0x00ef, B:44:0x00f5, B:46:0x0103, B:50:0x0119, B:49:0x0112, B:54:0x0123, B:55:0x0128, B:63:0x0138, B:64:0x013e, B:66:0x0144, B:21:0x008d), top: B:113:0x008d }] */
        /* JADX WARN: Removed duplicated region for block: B:73:0x016e  */
        /* JADX WARN: Removed duplicated region for block: B:90:0x01f5  */
        /* JADX WARN: Removed duplicated region for block: B:99:0x0250  */
        /* JADX WARN: Type inference failed for: r1v2, types: [gb2] */
        @Override // kotlin.coroutines.jvm.internal.BaseContinuationImpl
        /*
            Code decompiled incorrectly, please refer to instructions dump.
            To view partially-correct code enable 'Show inconsistent code' option in preferences
        */
        public final java.lang.Object invokeSuspend(java.lang.Object r18) {
            /*
                Method dump skipped, instructions count: 658
                To view this dump change 'Code comments level' option to 'DEBUG'
            */
            throw new UnsupportedOperationException("Method not decompiled: net.safemoon.androidwallet.viewmodels.AddNewTokensViewModel$loadTokens$1.AnonymousClass1.invokeSuspend(java.lang.Object):java.lang.Object");
        }
    }

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public AddNewTokensViewModel$loadTokens$1(AddNewTokensViewModel addNewTokensViewModel, q70<? super AddNewTokensViewModel$loadTokens$1> q70Var) {
        super(2, q70Var);
        this.this$0 = addNewTokensViewModel;
    }

    @Override // kotlin.coroutines.jvm.internal.BaseContinuationImpl
    public final q70<te4> create(Object obj, q70<?> q70Var) {
        return new AddNewTokensViewModel$loadTokens$1(this.this$0, q70Var);
    }

    @Override // defpackage.hd1
    public final Object invoke(c90 c90Var, q70<? super te4> q70Var) {
        return ((AddNewTokensViewModel$loadTokens$1) create(c90Var, q70Var)).invokeSuspend(te4.a);
    }

    @Override // kotlin.coroutines.jvm.internal.BaseContinuationImpl
    public final Object invokeSuspend(Object obj) {
        Object d = gs1.d();
        int i = this.label;
        if (i == 0) {
            o83.b(obj);
            CoroutineDispatcher b = tp0.b();
            AnonymousClass1 anonymousClass1 = new AnonymousClass1(this.this$0, null);
            this.label = 1;
            if (kotlinx.coroutines.a.e(b, anonymousClass1, this) == d) {
                return d;
            }
        } else if (i != 1) {
            throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        } else {
            o83.b(obj);
        }
        return te4.a;
    }
}
