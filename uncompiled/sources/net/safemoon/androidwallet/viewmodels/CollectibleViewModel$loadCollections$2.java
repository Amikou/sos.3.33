package net.safemoon.androidwallet.viewmodels;

import kotlin.coroutines.jvm.internal.SuspendLambda;
import kotlin.coroutines.jvm.internal.a;

/* compiled from: CollectibleViewModel.kt */
@a(c = "net.safemoon.androidwallet.viewmodels.CollectibleViewModel$loadCollections$2", f = "CollectibleViewModel.kt", l = {300}, m = "invokeSuspend")
/* loaded from: classes2.dex */
public final class CollectibleViewModel$loadCollections$2 extends SuspendLambda implements hd1<c90, q70<? super te4>, Object> {
    public final /* synthetic */ int $chainId;
    public final /* synthetic */ int $limit;
    public final /* synthetic */ int $offset;
    public int label;
    public final /* synthetic */ CollectibleViewModel this$0;

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public CollectibleViewModel$loadCollections$2(CollectibleViewModel collectibleViewModel, int i, int i2, int i3, q70<? super CollectibleViewModel$loadCollections$2> q70Var) {
        super(2, q70Var);
        this.this$0 = collectibleViewModel;
        this.$chainId = i;
        this.$limit = i2;
        this.$offset = i3;
    }

    @Override // kotlin.coroutines.jvm.internal.BaseContinuationImpl
    public final q70<te4> create(Object obj, q70<?> q70Var) {
        return new CollectibleViewModel$loadCollections$2(this.this$0, this.$chainId, this.$limit, this.$offset, q70Var);
    }

    @Override // defpackage.hd1
    public final Object invoke(c90 c90Var, q70<? super te4> q70Var) {
        return ((CollectibleViewModel$loadCollections$2) create(c90Var, q70Var)).invokeSuspend(te4.a);
    }

    @Override // kotlin.coroutines.jvm.internal.BaseContinuationImpl
    public final Object invokeSuspend(Object obj) {
        Object q;
        Object d = gs1.d();
        int i = this.label;
        if (i == 0) {
            o83.b(obj);
            this.this$0.N().postValue(hr.a(true));
            CollectibleViewModel collectibleViewModel = this.this$0;
            int i2 = this.$chainId;
            int i3 = this.$limit;
            int i4 = this.$offset;
            this.label = 1;
            q = collectibleViewModel.q(i2, i3, i4, this);
            if (q == d) {
                return d;
            }
        } else if (i != 1) {
            throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        } else {
            o83.b(obj);
        }
        this.this$0.N().postValue(hr.a(false));
        return te4.a;
    }
}
