package net.safemoon.androidwallet.viewmodels;

import kotlin.jvm.internal.Lambda;

/* compiled from: SwapViewModel.kt */
/* loaded from: classes2.dex */
public final class SwapViewModel$credentails$2 extends Lambda implements rc1<ma0> {
    public final /* synthetic */ SwapViewModel this$0;

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public SwapViewModel$credentails$2(SwapViewModel swapViewModel) {
        super(0);
        this.this$0 = swapViewModel;
    }

    @Override // defpackage.rc1
    public final ma0 invoke() {
        String q0;
        q0 = this.this$0.q0();
        return ma0.create(q0);
    }
}
