package net.safemoon.androidwallet.viewmodels;

import com.github.mikephil.charting.utils.Utils;
import kotlin.coroutines.jvm.internal.SuspendLambda;
import kotlin.coroutines.jvm.internal.a;
import kotlinx.coroutines.CoroutineDispatcher;
import net.safemoon.androidwallet.viewmodels.SwapViewModel;

/* compiled from: SwapViewModel.kt */
@a(c = "net.safemoon.androidwallet.viewmodels.SwapViewModel$getSlippageAmountIn$1", f = "SwapViewModel.kt", l = {1430}, m = "invokeSuspend")
/* loaded from: classes2.dex */
public final class SwapViewModel$getSlippageAmountIn$1 extends SuspendLambda implements hd1<c90, q70<? super te4>, Object> {
    public final /* synthetic */ SwapViewModel.a $enterAmount;
    public int label;
    public final /* synthetic */ SwapViewModel this$0;

    /* compiled from: SwapViewModel.kt */
    @kotlin.coroutines.jvm.internal.a(c = "net.safemoon.androidwallet.viewmodels.SwapViewModel$getSlippageAmountIn$1$1", f = "SwapViewModel.kt", l = {2166}, m = "invokeSuspend")
    /* renamed from: net.safemoon.androidwallet.viewmodels.SwapViewModel$getSlippageAmountIn$1$1  reason: invalid class name */
    /* loaded from: classes2.dex */
    public static final class AnonymousClass1 extends SuspendLambda implements hd1<c90, q70<? super te4>, Object> {
        public final /* synthetic */ SwapViewModel.a $enterAmount;
        public int label;
        public final /* synthetic */ SwapViewModel this$0;

        /* compiled from: SwapViewModel.kt */
        @kotlin.coroutines.jvm.internal.a(c = "net.safemoon.androidwallet.viewmodels.SwapViewModel$getSlippageAmountIn$1$1$1", f = "SwapViewModel.kt", l = {1432, 1432}, m = "invokeSuspend")
        /* renamed from: net.safemoon.androidwallet.viewmodels.SwapViewModel$getSlippageAmountIn$1$1$1  reason: invalid class name and collision with other inner class name */
        /* loaded from: classes2.dex */
        public static final class C02441 extends SuspendLambda implements hd1<k71<? super l84>, q70<? super te4>, Object> {
            public final /* synthetic */ SwapViewModel.a $enterAmount;
            private /* synthetic */ Object L$0;
            public int label;
            public final /* synthetic */ SwapViewModel this$0;

            /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
            public C02441(SwapViewModel swapViewModel, SwapViewModel.a aVar, q70<? super C02441> q70Var) {
                super(2, q70Var);
                this.this$0 = swapViewModel;
                this.$enterAmount = aVar;
            }

            @Override // kotlin.coroutines.jvm.internal.BaseContinuationImpl
            public final q70<te4> create(Object obj, q70<?> q70Var) {
                C02441 c02441 = new C02441(this.this$0, this.$enterAmount, q70Var);
                c02441.L$0 = obj;
                return c02441;
            }

            @Override // defpackage.hd1
            public final Object invoke(k71<? super l84> k71Var, q70<? super te4> q70Var) {
                return ((C02441) create(k71Var, q70Var)).invokeSuspend(te4.a);
            }

            @Override // kotlin.coroutines.jvm.internal.BaseContinuationImpl
            public final Object invokeSuspend(Object obj) {
                k71 k71Var;
                Object d = gs1.d();
                int i = this.label;
                if (i == 0) {
                    o83.b(obj);
                    k71Var = (k71) this.L$0;
                    SwapViewModel swapViewModel = this.this$0;
                    SwapViewModel.a aVar = this.$enterAmount;
                    this.L$0 = k71Var;
                    this.label = 1;
                    obj = swapViewModel.S(aVar, this);
                    if (obj == d) {
                        return d;
                    }
                } else if (i != 1) {
                    if (i != 2) {
                        throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
                    }
                    o83.b(obj);
                    return te4.a;
                } else {
                    k71Var = (k71) this.L$0;
                    o83.b(obj);
                }
                this.L$0 = null;
                this.label = 2;
                if (k71Var.emit(obj, this) == d) {
                    return d;
                }
                return te4.a;
            }
        }

        /* compiled from: SwapViewModel.kt */
        @kotlin.coroutines.jvm.internal.a(c = "net.safemoon.androidwallet.viewmodels.SwapViewModel$getSlippageAmountIn$1$1$2", f = "SwapViewModel.kt", l = {}, m = "invokeSuspend")
        /* renamed from: net.safemoon.androidwallet.viewmodels.SwapViewModel$getSlippageAmountIn$1$1$2  reason: invalid class name */
        /* loaded from: classes2.dex */
        public static final class AnonymousClass2 extends SuspendLambda implements kd1<k71<? super l84>, Throwable, q70<? super te4>, Object> {
            public /* synthetic */ Object L$0;
            public int label;
            public final /* synthetic */ SwapViewModel this$0;

            /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
            public AnonymousClass2(SwapViewModel swapViewModel, q70<? super AnonymousClass2> q70Var) {
                super(3, q70Var);
                this.this$0 = swapViewModel;
            }

            @Override // defpackage.kd1
            public final Object invoke(k71<? super l84> k71Var, Throwable th, q70<? super te4> q70Var) {
                AnonymousClass2 anonymousClass2 = new AnonymousClass2(this.this$0, q70Var);
                anonymousClass2.L$0 = th;
                return anonymousClass2.invokeSuspend(te4.a);
            }

            @Override // kotlin.coroutines.jvm.internal.BaseContinuationImpl
            public final Object invokeSuspend(Object obj) {
                gs1.d();
                if (this.label == 0) {
                    o83.b(obj);
                    ((Throwable) this.L$0).printStackTrace();
                    this.this$0.N().postValue(hr.b(Utils.DOUBLE_EPSILON));
                    return te4.a;
                }
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
        }

        /* compiled from: Collect.kt */
        /* renamed from: net.safemoon.androidwallet.viewmodels.SwapViewModel$getSlippageAmountIn$1$1$a */
        /* loaded from: classes2.dex */
        public static final class a implements k71<l84> {
            public final /* synthetic */ SwapViewModel a;

            public a(SwapViewModel swapViewModel) {
                this.a = swapViewModel;
            }

            @Override // defpackage.k71
            public Object emit(l84 l84Var, q70<? super te4> q70Var) {
                st1 b;
                b = as.b(ej4.a(this.a), null, null, new SwapViewModel$getSlippageAmountIn$1$1$3$1(this.a, l84Var, null), 3, null);
                return b == gs1.d() ? b : te4.a;
            }
        }

        /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
        public AnonymousClass1(SwapViewModel swapViewModel, SwapViewModel.a aVar, q70<? super AnonymousClass1> q70Var) {
            super(2, q70Var);
            this.this$0 = swapViewModel;
            this.$enterAmount = aVar;
        }

        @Override // kotlin.coroutines.jvm.internal.BaseContinuationImpl
        public final q70<te4> create(Object obj, q70<?> q70Var) {
            return new AnonymousClass1(this.this$0, this.$enterAmount, q70Var);
        }

        @Override // defpackage.hd1
        public final Object invoke(c90 c90Var, q70<? super te4> q70Var) {
            return ((AnonymousClass1) create(c90Var, q70Var)).invokeSuspend(te4.a);
        }

        @Override // kotlin.coroutines.jvm.internal.BaseContinuationImpl
        public final Object invokeSuspend(Object obj) {
            Object d = gs1.d();
            int i = this.label;
            if (i == 0) {
                o83.b(obj);
                j71 c = n71.c(n71.n(new C02441(this.this$0, this.$enterAmount, null)), new AnonymousClass2(this.this$0, null));
                a aVar = new a(this.this$0);
                this.label = 1;
                if (c.a(aVar, this) == d) {
                    return d;
                }
            } else if (i != 1) {
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            } else {
                o83.b(obj);
            }
            return te4.a;
        }
    }

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public SwapViewModel$getSlippageAmountIn$1(SwapViewModel swapViewModel, SwapViewModel.a aVar, q70<? super SwapViewModel$getSlippageAmountIn$1> q70Var) {
        super(2, q70Var);
        this.this$0 = swapViewModel;
        this.$enterAmount = aVar;
    }

    @Override // kotlin.coroutines.jvm.internal.BaseContinuationImpl
    public final q70<te4> create(Object obj, q70<?> q70Var) {
        return new SwapViewModel$getSlippageAmountIn$1(this.this$0, this.$enterAmount, q70Var);
    }

    @Override // defpackage.hd1
    public final Object invoke(c90 c90Var, q70<? super te4> q70Var) {
        return ((SwapViewModel$getSlippageAmountIn$1) create(c90Var, q70Var)).invokeSuspend(te4.a);
    }

    @Override // kotlin.coroutines.jvm.internal.BaseContinuationImpl
    public final Object invokeSuspend(Object obj) {
        Object d = gs1.d();
        int i = this.label;
        if (i == 0) {
            o83.b(obj);
            CoroutineDispatcher b = tp0.b();
            AnonymousClass1 anonymousClass1 = new AnonymousClass1(this.this$0, this.$enterAmount, null);
            this.label = 1;
            if (kotlinx.coroutines.a.e(b, anonymousClass1, this) == d) {
                return d;
            }
        } else if (i != 1) {
            throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        } else {
            o83.b(obj);
        }
        return te4.a;
    }
}
