package net.safemoon.androidwallet.viewmodels;

import kotlin.jvm.internal.Lambda;
import net.safemoon.androidwallet.model.token.abstraction.IToken;

/* compiled from: AddNewTokensViewModel.kt */
/* loaded from: classes2.dex */
public final class AddNewTokensViewModel$removeDeletableItem$1 extends Lambda implements tc1<IToken, Boolean> {
    public final /* synthetic */ q9 $item;

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public AddNewTokensViewModel$removeDeletableItem$1(q9 q9Var) {
        super(1);
        this.$item = q9Var;
    }

    @Override // defpackage.tc1
    public final Boolean invoke(IToken iToken) {
        fs1.f(iToken, "it");
        return Boolean.valueOf(fs1.b(iToken.getContractAddress(), this.$item.a()) && fs1.b(iToken.getSymbol(), this.$item.g()));
    }
}
