package net.safemoon.androidwallet.viewmodels;

import java.math.BigDecimal;
import java.math.BigInteger;
import kotlin.coroutines.jvm.internal.SuspendLambda;
import kotlin.coroutines.jvm.internal.a;
import kotlinx.coroutines.CoroutineDispatcher;
import net.safemoon.androidwallet.model.swap.Swap;

/* compiled from: SwapViewModel.kt */
@a(c = "net.safemoon.androidwallet.viewmodels.SwapViewModel$loadSourceBalance$1", f = "SwapViewModel.kt", l = {313}, m = "invokeSuspend")
/* loaded from: classes2.dex */
public final class SwapViewModel$loadSourceBalance$1 extends SuspendLambda implements hd1<c90, q70<? super te4>, Object> {
    public final /* synthetic */ Swap $swap;
    public int label;
    public final /* synthetic */ SwapViewModel this$0;

    /* compiled from: SwapViewModel.kt */
    @kotlin.coroutines.jvm.internal.a(c = "net.safemoon.androidwallet.viewmodels.SwapViewModel$loadSourceBalance$1$1", f = "SwapViewModel.kt", l = {2166}, m = "invokeSuspend")
    /* renamed from: net.safemoon.androidwallet.viewmodels.SwapViewModel$loadSourceBalance$1$1  reason: invalid class name */
    /* loaded from: classes2.dex */
    public static final class AnonymousClass1 extends SuspendLambda implements hd1<c90, q70<? super te4>, Object> {
        public final /* synthetic */ Swap $swap;
        public int label;
        public final /* synthetic */ SwapViewModel this$0;

        /* compiled from: SwapViewModel.kt */
        @kotlin.coroutines.jvm.internal.a(c = "net.safemoon.androidwallet.viewmodels.SwapViewModel$loadSourceBalance$1$1$1", f = "SwapViewModel.kt", l = {315}, m = "invokeSuspend")
        /* renamed from: net.safemoon.androidwallet.viewmodels.SwapViewModel$loadSourceBalance$1$1$1  reason: invalid class name and collision with other inner class name */
        /* loaded from: classes2.dex */
        public static final class C02491 extends SuspendLambda implements hd1<k71<? super BigInteger>, q70<? super te4>, Object> {
            public final /* synthetic */ Swap $swap;
            private /* synthetic */ Object L$0;
            public int label;
            public final /* synthetic */ SwapViewModel this$0;

            /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
            public C02491(Swap swap, SwapViewModel swapViewModel, q70<? super C02491> q70Var) {
                super(2, q70Var);
                this.$swap = swap;
                this.this$0 = swapViewModel;
            }

            @Override // kotlin.coroutines.jvm.internal.BaseContinuationImpl
            public final q70<te4> create(Object obj, q70<?> q70Var) {
                C02491 c02491 = new C02491(this.$swap, this.this$0, q70Var);
                c02491.L$0 = obj;
                return c02491;
            }

            @Override // defpackage.hd1
            public final Object invoke(k71<? super BigInteger> k71Var, q70<? super te4> q70Var) {
                return ((C02491) create(k71Var, q70Var)).invokeSuspend(te4.a);
            }

            /* JADX WARN: Removed duplicated region for block: B:23:0x00a0 A[RETURN] */
            @Override // kotlin.coroutines.jvm.internal.BaseContinuationImpl
            /*
                Code decompiled incorrectly, please refer to instructions dump.
                To view partially-correct code enable 'Show inconsistent code' option in preferences
            */
            public final java.lang.Object invokeSuspend(java.lang.Object r9) {
                /*
                    r8 = this;
                    java.lang.Object r0 = defpackage.gs1.d()
                    int r1 = r8.label
                    r2 = 1
                    if (r1 == 0) goto L18
                    if (r1 != r2) goto L10
                    defpackage.o83.b(r9)
                    goto La1
                L10:
                    java.lang.IllegalStateException r9 = new java.lang.IllegalStateException
                    java.lang.String r0 = "call to 'resume' before 'invoke' with coroutine"
                    r9.<init>(r0)
                    throw r9
                L18:
                    defpackage.o83.b(r9)
                    java.lang.Object r9 = r8.L$0
                    k71 r9 = (defpackage.k71) r9
                    net.safemoon.androidwallet.model.swap.Swap r1 = r8.$swap
                    java.lang.String r1 = r1.address
                    if (r1 == 0) goto L78
                    java.lang.String r3 = "swap.address"
                    defpackage.fs1.e(r1, r3)
                    int r1 = r1.length()
                    if (r1 != 0) goto L32
                    r1 = r2
                    goto L33
                L32:
                    r1 = 0
                L33:
                    if (r1 == 0) goto L36
                    goto L78
                L36:
                    net.safemoon.androidwallet.viewmodels.SwapViewModel r1 = r8.this$0
                    net.safemoon.androidwallet.ERC20 r1 = net.safemoon.androidwallet.viewmodels.SwapViewModel.n(r1)
                    if (r1 != 0) goto L44
                    java.lang.String r1 = "erc20Source"
                    defpackage.fs1.r(r1)
                    r1 = 0
                L44:
                    java.lang.String r1 = r1.getContractAddress()
                    net.safemoon.androidwallet.viewmodels.SwapViewModel r3 = r8.this$0
                    ko4 r3 = net.safemoon.androidwallet.viewmodels.SwapViewModel.C(r3)
                    net.safemoon.androidwallet.viewmodels.SwapViewModel r4 = r8.this$0
                    ma0 r4 = net.safemoon.androidwallet.viewmodels.SwapViewModel.k(r4)
                    net.safemoon.androidwallet.viewmodels.SwapViewModel r5 = r8.this$0
                    java.math.BigInteger r6 = net.safemoon.androidwallet.viewmodels.SwapViewModel.p(r5)
                    java.lang.String r7 = "GAS_LIMIT"
                    defpackage.fs1.e(r6, r7)
                    j80 r5 = net.safemoon.androidwallet.viewmodels.SwapViewModel.s(r5, r6)
                    net.safemoon.androidwallet.ERC20 r1 = net.safemoon.androidwallet.ERC20.s(r1, r3, r4, r5)
                    net.safemoon.androidwallet.viewmodels.SwapViewModel r3 = r8.this$0
                    java.lang.String r3 = net.safemoon.androidwallet.viewmodels.SwapViewModel.h(r3)
                    m63 r1 = r1.q(r3)
                    java.lang.Object r1 = r1.send()
                    java.math.BigInteger r1 = (java.math.BigInteger) r1
                    goto L98
                L78:
                    net.safemoon.androidwallet.viewmodels.SwapViewModel r1 = r8.this$0
                    ko4 r1 = net.safemoon.androidwallet.viewmodels.SwapViewModel.C(r1)
                    net.safemoon.androidwallet.viewmodels.SwapViewModel r3 = r8.this$0
                    ma0 r3 = net.safemoon.androidwallet.viewmodels.SwapViewModel.k(r3)
                    java.lang.String r3 = r3.getAddress()
                    org.web3j.protocol.core.DefaultBlockParameterName r4 = org.web3j.protocol.core.DefaultBlockParameterName.LATEST
                    org.web3j.protocol.core.c r1 = r1.ethGetBalance(r3, r4)
                    i83 r1 = r1.send()
                    sw0 r1 = (defpackage.sw0) r1
                    java.math.BigInteger r1 = r1.getBalance()
                L98:
                    r8.label = r2
                    java.lang.Object r9 = r9.emit(r1, r8)
                    if (r9 != r0) goto La1
                    return r0
                La1:
                    te4 r9 = defpackage.te4.a
                    return r9
                */
                throw new UnsupportedOperationException("Method not decompiled: net.safemoon.androidwallet.viewmodels.SwapViewModel$loadSourceBalance$1.AnonymousClass1.C02491.invokeSuspend(java.lang.Object):java.lang.Object");
            }
        }

        /* compiled from: SwapViewModel.kt */
        @kotlin.coroutines.jvm.internal.a(c = "net.safemoon.androidwallet.viewmodels.SwapViewModel$loadSourceBalance$1$1$2", f = "SwapViewModel.kt", l = {}, m = "invokeSuspend")
        /* renamed from: net.safemoon.androidwallet.viewmodels.SwapViewModel$loadSourceBalance$1$1$2  reason: invalid class name */
        /* loaded from: classes2.dex */
        public static final class AnonymousClass2 extends SuspendLambda implements kd1<k71<? super BigInteger>, Throwable, q70<? super te4>, Object> {
            public /* synthetic */ Object L$0;
            public int label;
            public final /* synthetic */ SwapViewModel this$0;

            /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
            public AnonymousClass2(SwapViewModel swapViewModel, q70<? super AnonymousClass2> q70Var) {
                super(3, q70Var);
                this.this$0 = swapViewModel;
            }

            @Override // defpackage.kd1
            public final Object invoke(k71<? super BigInteger> k71Var, Throwable th, q70<? super te4> q70Var) {
                AnonymousClass2 anonymousClass2 = new AnonymousClass2(this.this$0, q70Var);
                anonymousClass2.L$0 = th;
                return anonymousClass2.invokeSuspend(te4.a);
            }

            @Override // kotlin.coroutines.jvm.internal.BaseContinuationImpl
            public final Object invokeSuspend(Object obj) {
                gs1.d();
                if (this.label == 0) {
                    o83.b(obj);
                    this.this$0.z0().postValue(BigDecimal.ZERO);
                    ((Throwable) this.L$0).printStackTrace();
                    return te4.a;
                }
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
        }

        /* compiled from: Collect.kt */
        /* renamed from: net.safemoon.androidwallet.viewmodels.SwapViewModel$loadSourceBalance$1$1$a */
        /* loaded from: classes2.dex */
        public static final class a implements k71<BigInteger> {
            public final /* synthetic */ SwapViewModel a;
            public final /* synthetic */ Swap f0;

            public a(SwapViewModel swapViewModel, Swap swap) {
                this.a = swapViewModel;
                this.f0 = swap;
            }

            @Override // defpackage.k71
            public Object emit(BigInteger bigInteger, q70<? super te4> q70Var) {
                st1 b;
                b = as.b(ej4.a(this.a), null, null, new SwapViewModel$loadSourceBalance$1$1$3$1(this.a, bigInteger, this.f0, null), 3, null);
                return b == gs1.d() ? b : te4.a;
            }
        }

        /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
        public AnonymousClass1(Swap swap, SwapViewModel swapViewModel, q70<? super AnonymousClass1> q70Var) {
            super(2, q70Var);
            this.$swap = swap;
            this.this$0 = swapViewModel;
        }

        @Override // kotlin.coroutines.jvm.internal.BaseContinuationImpl
        public final q70<te4> create(Object obj, q70<?> q70Var) {
            return new AnonymousClass1(this.$swap, this.this$0, q70Var);
        }

        @Override // defpackage.hd1
        public final Object invoke(c90 c90Var, q70<? super te4> q70Var) {
            return ((AnonymousClass1) create(c90Var, q70Var)).invokeSuspend(te4.a);
        }

        @Override // kotlin.coroutines.jvm.internal.BaseContinuationImpl
        public final Object invokeSuspend(Object obj) {
            Object d = gs1.d();
            int i = this.label;
            if (i == 0) {
                o83.b(obj);
                j71 c = n71.c(n71.n(new C02491(this.$swap, this.this$0, null)), new AnonymousClass2(this.this$0, null));
                a aVar = new a(this.this$0, this.$swap);
                this.label = 1;
                if (c.a(aVar, this) == d) {
                    return d;
                }
            } else if (i != 1) {
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            } else {
                o83.b(obj);
            }
            return te4.a;
        }
    }

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public SwapViewModel$loadSourceBalance$1(Swap swap, SwapViewModel swapViewModel, q70<? super SwapViewModel$loadSourceBalance$1> q70Var) {
        super(2, q70Var);
        this.$swap = swap;
        this.this$0 = swapViewModel;
    }

    @Override // kotlin.coroutines.jvm.internal.BaseContinuationImpl
    public final q70<te4> create(Object obj, q70<?> q70Var) {
        return new SwapViewModel$loadSourceBalance$1(this.$swap, this.this$0, q70Var);
    }

    @Override // defpackage.hd1
    public final Object invoke(c90 c90Var, q70<? super te4> q70Var) {
        return ((SwapViewModel$loadSourceBalance$1) create(c90Var, q70Var)).invokeSuspend(te4.a);
    }

    @Override // kotlin.coroutines.jvm.internal.BaseContinuationImpl
    public final Object invokeSuspend(Object obj) {
        Object d = gs1.d();
        int i = this.label;
        if (i == 0) {
            o83.b(obj);
            CoroutineDispatcher b = tp0.b();
            AnonymousClass1 anonymousClass1 = new AnonymousClass1(this.$swap, this.this$0, null);
            this.label = 1;
            if (kotlinx.coroutines.a.e(b, anonymousClass1, this) == d) {
                return d;
            }
        } else if (i != 1) {
            throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        } else {
            o83.b(obj);
        }
        return te4.a;
    }
}
