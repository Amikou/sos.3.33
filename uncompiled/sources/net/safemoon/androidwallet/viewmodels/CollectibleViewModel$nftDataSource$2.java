package net.safemoon.androidwallet.viewmodels;

import kotlin.jvm.internal.Lambda;
import net.safemoon.androidwallet.database.room.ApplicationRoomDatabase;
import net.safemoon.androidwallet.repository.dataSource.collection.NftDataSource;

/* compiled from: CollectibleViewModel.kt */
/* loaded from: classes2.dex */
public final class CollectibleViewModel$nftDataSource$2 extends Lambda implements rc1<NftDataSource> {
    public final /* synthetic */ CollectibleViewModel this$0;

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public CollectibleViewModel$nftDataSource$2(CollectibleViewModel collectibleViewModel) {
        super(0);
        this.this$0 = collectibleViewModel;
    }

    @Override // defpackage.rc1
    public final NftDataSource invoke() {
        ApplicationRoomDatabase x;
        x = this.this$0.x();
        return new NftDataSource(x.W());
    }
}
