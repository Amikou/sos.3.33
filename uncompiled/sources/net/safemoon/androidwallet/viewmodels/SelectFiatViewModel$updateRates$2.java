package net.safemoon.androidwallet.viewmodels;

import java.util.Arrays;
import java.util.List;
import kotlin.coroutines.jvm.internal.SuspendLambda;
import kotlin.coroutines.jvm.internal.a;
import kotlinx.coroutines.CoroutineDispatcher;
import net.safemoon.androidwallet.model.fiat.gson.Fiat;
import net.safemoon.androidwallet.model.fiat.gson.FiatLatest;
import retrofit2.KotlinExtensions;
import retrofit2.b;
import retrofit2.n;

/* compiled from: SelectFiatViewModel.kt */
@a(c = "net.safemoon.androidwallet.viewmodels.SelectFiatViewModel$updateRates$2", f = "SelectFiatViewModel.kt", l = {89}, m = "invokeSuspend")
/* loaded from: classes2.dex */
public final class SelectFiatViewModel$updateRates$2 extends SuspendLambda implements hd1<c90, q70<? super te4>, Object> {
    public int label;
    public final /* synthetic */ SelectFiatViewModel this$0;

    /* compiled from: SelectFiatViewModel.kt */
    @a(c = "net.safemoon.androidwallet.viewmodels.SelectFiatViewModel$updateRates$2$1", f = "SelectFiatViewModel.kt", l = {92, 94}, m = "invokeSuspend")
    /* renamed from: net.safemoon.androidwallet.viewmodels.SelectFiatViewModel$updateRates$2$1  reason: invalid class name */
    /* loaded from: classes2.dex */
    public static final class AnonymousClass1 extends SuspendLambda implements hd1<c90, q70<? super te4>, Object> {
        public int label;
        public final /* synthetic */ SelectFiatViewModel this$0;

        /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
        public AnonymousClass1(SelectFiatViewModel selectFiatViewModel, q70<? super AnonymousClass1> q70Var) {
            super(2, q70Var);
            this.this$0 = selectFiatViewModel;
        }

        @Override // kotlin.coroutines.jvm.internal.BaseContinuationImpl
        public final q70<te4> create(Object obj, q70<?> q70Var) {
            return new AnonymousClass1(this.this$0, q70Var);
        }

        @Override // defpackage.hd1
        public final Object invoke(c90 c90Var, q70<? super te4> q70Var) {
            return ((AnonymousClass1) create(c90Var, q70Var)).invokeSuspend(te4.a);
        }

        @Override // kotlin.coroutines.jvm.internal.BaseContinuationImpl
        public final Object invokeSuspend(Object obj) {
            yl1 yl1Var;
            Object d = gs1.d();
            int i = this.label;
            if (i == 0) {
                o83.b(obj);
                b<FiatLatest> b = a4.k().b("ALLDATA");
                this.label = 1;
                obj = KotlinExtensions.c(b, this);
                if (obj == d) {
                    return d;
                }
            } else if (i != 1) {
                if (i != 2) {
                    throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
                }
                o83.b(obj);
                return te4.a;
            } else {
                o83.b(obj);
            }
            FiatLatest fiatLatest = (FiatLatest) ((n) obj).a();
            List<Fiat> allFiatRates = fiatLatest == null ? null : fiatLatest.allFiatRates();
            if (allFiatRates != null) {
                yl1Var = this.this$0.b;
                Object[] array = allFiatRates.toArray(new Fiat[0]);
                if (array != null) {
                    Fiat[] fiatArr = (Fiat[]) array;
                    this.label = 2;
                    if (yl1Var.b((Fiat[]) Arrays.copyOf(fiatArr, fiatArr.length), this) == d) {
                        return d;
                    }
                } else {
                    throw new NullPointerException("null cannot be cast to non-null type kotlin.Array<T>");
                }
            }
            return te4.a;
        }
    }

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public SelectFiatViewModel$updateRates$2(SelectFiatViewModel selectFiatViewModel, q70<? super SelectFiatViewModel$updateRates$2> q70Var) {
        super(2, q70Var);
        this.this$0 = selectFiatViewModel;
    }

    @Override // kotlin.coroutines.jvm.internal.BaseContinuationImpl
    public final q70<te4> create(Object obj, q70<?> q70Var) {
        return new SelectFiatViewModel$updateRates$2(this.this$0, q70Var);
    }

    @Override // defpackage.hd1
    public final Object invoke(c90 c90Var, q70<? super te4> q70Var) {
        return ((SelectFiatViewModel$updateRates$2) create(c90Var, q70Var)).invokeSuspend(te4.a);
    }

    @Override // kotlin.coroutines.jvm.internal.BaseContinuationImpl
    public final Object invokeSuspend(Object obj) {
        Object d = gs1.d();
        int i = this.label;
        if (i == 0) {
            o83.b(obj);
            CoroutineDispatcher b = tp0.b();
            AnonymousClass1 anonymousClass1 = new AnonymousClass1(this.this$0, null);
            this.label = 1;
            if (kotlinx.coroutines.a.e(b, anonymousClass1, this) == d) {
                return d;
            }
        } else if (i != 1) {
            throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        } else {
            o83.b(obj);
        }
        return te4.a;
    }
}
