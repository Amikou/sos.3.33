package net.safemoon.androidwallet.viewmodels;

import android.app.Application;
import kotlin.jvm.internal.Lambda;
import net.safemoon.androidwallet.common.TokenType;

/* compiled from: WalletConnectViewModel.kt */
/* loaded from: classes2.dex */
public final class WalletConnectViewModel$defaultGateWay$2 extends Lambda implements rc1<TokenType> {
    public final /* synthetic */ Application $application;

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public WalletConnectViewModel$defaultGateWay$2(Application application) {
        super(0);
        this.$application = application;
    }

    @Override // defpackage.rc1
    public final TokenType invoke() {
        TokenType.a aVar = TokenType.Companion;
        String j = bo3.j(this.$application, "DEFAULT_GATEWAY", TokenType.BEP_20.getName());
        fs1.e(j, "getString(application, S…kenType.BEP_20.getName())");
        return aVar.c(j);
    }
}
