package net.safemoon.androidwallet.viewmodels;

import java.util.List;
import kotlin.coroutines.jvm.internal.SuspendLambda;
import kotlin.coroutines.jvm.internal.a;

/* compiled from: CMCListViewModel.kt */
@a(c = "net.safemoon.androidwallet.viewmodels.CMCListViewModel$fetchCmcToken$1", f = "CMCListViewModel.kt", l = {67}, m = "invokeSuspend")
/* loaded from: classes2.dex */
public final class CMCListViewModel$fetchCmcToken$1 extends SuspendLambda implements hd1<c90, q70<? super te4>, Object> {
    public int label;
    public final /* synthetic */ CMCListViewModel this$0;

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public CMCListViewModel$fetchCmcToken$1(CMCListViewModel cMCListViewModel, q70<? super CMCListViewModel$fetchCmcToken$1> q70Var) {
        super(2, q70Var);
        this.this$0 = cMCListViewModel;
    }

    @Override // kotlin.coroutines.jvm.internal.BaseContinuationImpl
    public final q70<te4> create(Object obj, q70<?> q70Var) {
        return new CMCListViewModel$fetchCmcToken$1(this.this$0, q70Var);
    }

    @Override // defpackage.hd1
    public final Object invoke(c90 c90Var, q70<? super te4> q70Var) {
        return ((CMCListViewModel$fetchCmcToken$1) create(c90Var, q70Var)).invokeSuspend(te4.a);
    }

    @Override // kotlin.coroutines.jvm.internal.BaseContinuationImpl
    public final Object invokeSuspend(Object obj) {
        gb2 gb2Var;
        gb2 gb2Var2;
        Object d = gs1.d();
        int i = this.label;
        if (i == 0) {
            o83.b(obj);
            CMCListViewModel cMCListViewModel = this.this$0;
            this.label = 1;
            obj = cMCListViewModel.l(this);
            if (obj == d) {
                return d;
            }
        } else if (i != 1) {
            throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        } else {
            o83.b(obj);
        }
        List list = (List) obj;
        if (!list.isEmpty()) {
            gb2Var2 = this.this$0.g;
            gb2Var2.postValue(null);
        }
        gb2Var = this.this$0.g;
        gb2Var.postValue(list);
        return te4.a;
    }
}
