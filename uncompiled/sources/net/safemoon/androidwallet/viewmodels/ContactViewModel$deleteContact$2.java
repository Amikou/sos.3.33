package net.safemoon.androidwallet.viewmodels;

import kotlin.coroutines.jvm.internal.SuspendLambda;
import kotlin.coroutines.jvm.internal.a;
import kotlinx.coroutines.CoroutineDispatcher;

/* compiled from: ContactViewModel.kt */
@a(c = "net.safemoon.androidwallet.viewmodels.ContactViewModel$deleteContact$2", f = "ContactViewModel.kt", l = {182}, m = "invokeSuspend")
/* loaded from: classes2.dex */
public final class ContactViewModel$deleteContact$2 extends SuspendLambda implements hd1<c90, q70<? super te4>, Object> {
    public final /* synthetic */ String $address;
    public final /* synthetic */ rc1<te4> $callBack;
    public int label;
    public final /* synthetic */ ContactViewModel this$0;

    /* compiled from: ContactViewModel.kt */
    @a(c = "net.safemoon.androidwallet.viewmodels.ContactViewModel$deleteContact$2$1", f = "ContactViewModel.kt", l = {183}, m = "invokeSuspend")
    /* renamed from: net.safemoon.androidwallet.viewmodels.ContactViewModel$deleteContact$2$1  reason: invalid class name */
    /* loaded from: classes2.dex */
    public static final class AnonymousClass1 extends SuspendLambda implements hd1<c90, q70<? super te4>, Object> {
        public final /* synthetic */ String $address;
        public final /* synthetic */ rc1<te4> $callBack;
        public int label;
        public final /* synthetic */ ContactViewModel this$0;

        /* compiled from: ContactViewModel.kt */
        @a(c = "net.safemoon.androidwallet.viewmodels.ContactViewModel$deleteContact$2$1$1", f = "ContactViewModel.kt", l = {}, m = "invokeSuspend")
        /* renamed from: net.safemoon.androidwallet.viewmodels.ContactViewModel$deleteContact$2$1$1  reason: invalid class name and collision with other inner class name */
        /* loaded from: classes2.dex */
        public static final class C02211 extends SuspendLambda implements hd1<c90, q70<? super te4>, Object> {
            public final /* synthetic */ rc1<te4> $callBack;
            public int label;

            /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
            public C02211(rc1<te4> rc1Var, q70<? super C02211> q70Var) {
                super(2, q70Var);
                this.$callBack = rc1Var;
            }

            @Override // kotlin.coroutines.jvm.internal.BaseContinuationImpl
            public final q70<te4> create(Object obj, q70<?> q70Var) {
                return new C02211(this.$callBack, q70Var);
            }

            @Override // defpackage.hd1
            public final Object invoke(c90 c90Var, q70<? super te4> q70Var) {
                return ((C02211) create(c90Var, q70Var)).invokeSuspend(te4.a);
            }

            @Override // kotlin.coroutines.jvm.internal.BaseContinuationImpl
            public final Object invokeSuspend(Object obj) {
                gs1.d();
                if (this.label == 0) {
                    o83.b(obj);
                    this.$callBack.invoke();
                    return te4.a;
                }
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
        }

        /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
        public AnonymousClass1(ContactViewModel contactViewModel, String str, rc1<te4> rc1Var, q70<? super AnonymousClass1> q70Var) {
            super(2, q70Var);
            this.this$0 = contactViewModel;
            this.$address = str;
            this.$callBack = rc1Var;
        }

        @Override // kotlin.coroutines.jvm.internal.BaseContinuationImpl
        public final q70<te4> create(Object obj, q70<?> q70Var) {
            return new AnonymousClass1(this.this$0, this.$address, this.$callBack, q70Var);
        }

        @Override // defpackage.hd1
        public final Object invoke(c90 c90Var, q70<? super te4> q70Var) {
            return ((AnonymousClass1) create(c90Var, q70Var)).invokeSuspend(te4.a);
        }

        @Override // kotlin.coroutines.jvm.internal.BaseContinuationImpl
        public final Object invokeSuspend(Object obj) {
            Object d = gs1.d();
            int i = this.label;
            if (i == 0) {
                o83.b(obj);
                v60 i2 = this.this$0.i();
                String str = this.$address;
                this.label = 1;
                if (i2.b(str, this) == d) {
                    return d;
                }
            } else if (i != 1) {
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            } else {
                o83.b(obj);
            }
            as.b(ej4.a(this.this$0), null, null, new C02211(this.$callBack, null), 3, null);
            return te4.a;
        }
    }

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public ContactViewModel$deleteContact$2(ContactViewModel contactViewModel, String str, rc1<te4> rc1Var, q70<? super ContactViewModel$deleteContact$2> q70Var) {
        super(2, q70Var);
        this.this$0 = contactViewModel;
        this.$address = str;
        this.$callBack = rc1Var;
    }

    @Override // kotlin.coroutines.jvm.internal.BaseContinuationImpl
    public final q70<te4> create(Object obj, q70<?> q70Var) {
        return new ContactViewModel$deleteContact$2(this.this$0, this.$address, this.$callBack, q70Var);
    }

    @Override // defpackage.hd1
    public final Object invoke(c90 c90Var, q70<? super te4> q70Var) {
        return ((ContactViewModel$deleteContact$2) create(c90Var, q70Var)).invokeSuspend(te4.a);
    }

    @Override // kotlin.coroutines.jvm.internal.BaseContinuationImpl
    public final Object invokeSuspend(Object obj) {
        Object d = gs1.d();
        int i = this.label;
        if (i == 0) {
            o83.b(obj);
            CoroutineDispatcher b = tp0.b();
            AnonymousClass1 anonymousClass1 = new AnonymousClass1(this.this$0, this.$address, this.$callBack, null);
            this.label = 1;
            if (kotlinx.coroutines.a.e(b, anonymousClass1, this) == d) {
                return d;
            }
        } else if (i != 1) {
            throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        } else {
            o83.b(obj);
        }
        return te4.a;
    }
}
