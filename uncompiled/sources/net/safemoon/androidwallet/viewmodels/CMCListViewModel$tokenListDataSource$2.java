package net.safemoon.androidwallet.viewmodels;

import android.app.Application;
import kotlin.jvm.internal.Lambda;
import net.safemoon.androidwallet.repository.TokenListDataSource;

/* compiled from: CMCListViewModel.kt */
/* loaded from: classes2.dex */
public final class CMCListViewModel$tokenListDataSource$2 extends Lambda implements rc1<TokenListDataSource> {
    public final /* synthetic */ Application $application;

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public CMCListViewModel$tokenListDataSource$2(Application application) {
        super(0);
        this.$application = application;
    }

    @Override // defpackage.rc1
    public final TokenListDataSource invoke() {
        return new TokenListDataSource(this.$application);
    }
}
