package net.safemoon.androidwallet.viewmodels;

import kotlin.coroutines.jvm.internal.ContinuationImpl;
import kotlin.coroutines.jvm.internal.a;

/* compiled from: CryptoPriceAlertViewModel.kt */
@a(c = "net.safemoon.androidwallet.viewmodels.CryptoPriceAlertViewModel", f = "CryptoPriceAlertViewModel.kt", l = {212, 212}, m = "loadPriceAlert")
/* loaded from: classes2.dex */
public final class CryptoPriceAlertViewModel$loadPriceAlert$1 extends ContinuationImpl {
    public Object L$0;
    public Object L$1;
    public Object L$2;
    public Object L$3;
    public int label;
    public /* synthetic */ Object result;
    public final /* synthetic */ CryptoPriceAlertViewModel this$0;

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public CryptoPriceAlertViewModel$loadPriceAlert$1(CryptoPriceAlertViewModel cryptoPriceAlertViewModel, q70<? super CryptoPriceAlertViewModel$loadPriceAlert$1> q70Var) {
        super(q70Var);
        this.this$0 = cryptoPriceAlertViewModel;
    }

    @Override // kotlin.coroutines.jvm.internal.BaseContinuationImpl
    public final Object invokeSuspend(Object obj) {
        Object v;
        this.result = obj;
        this.label |= Integer.MIN_VALUE;
        v = this.this$0.v(null, null, this);
        return v;
    }
}
