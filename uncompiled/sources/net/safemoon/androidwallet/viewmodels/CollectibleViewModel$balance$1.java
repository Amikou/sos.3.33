package net.safemoon.androidwallet.viewmodels;

import kotlin.coroutines.jvm.internal.SuspendLambda;
import kotlin.coroutines.jvm.internal.a;
import kotlinx.coroutines.CoroutineDispatcher;
import net.safemoon.androidwallet.model.nft.NFTBalance;

/* compiled from: CollectibleViewModel.kt */
@a(c = "net.safemoon.androidwallet.viewmodels.CollectibleViewModel$balance$1", f = "CollectibleViewModel.kt", l = {181}, m = "invokeSuspend")
/* loaded from: classes2.dex */
public final class CollectibleViewModel$balance$1 extends SuspendLambda implements hd1<c90, q70<? super te4>, Object> {
    public final /* synthetic */ gb2<NFTBalance> $callBack;
    public final /* synthetic */ im1 $iNFTWeb;
    public final /* synthetic */ String $tokenId;
    public int label;

    /* compiled from: CollectibleViewModel.kt */
    @a(c = "net.safemoon.androidwallet.viewmodels.CollectibleViewModel$balance$1$1", f = "CollectibleViewModel.kt", l = {185, 191}, m = "invokeSuspend")
    /* renamed from: net.safemoon.androidwallet.viewmodels.CollectibleViewModel$balance$1$1  reason: invalid class name */
    /* loaded from: classes2.dex */
    public static final class AnonymousClass1 extends SuspendLambda implements hd1<c90, q70<? super te4>, Object> {
        public final /* synthetic */ gb2<NFTBalance> $callBack;
        public final /* synthetic */ im1 $iNFTWeb;
        public final /* synthetic */ String $tokenId;
        public int label;

        /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
        public AnonymousClass1(im1 im1Var, String str, gb2<NFTBalance> gb2Var, q70<? super AnonymousClass1> q70Var) {
            super(2, q70Var);
            this.$iNFTWeb = im1Var;
            this.$tokenId = str;
            this.$callBack = gb2Var;
        }

        @Override // kotlin.coroutines.jvm.internal.BaseContinuationImpl
        public final q70<te4> create(Object obj, q70<?> q70Var) {
            return new AnonymousClass1(this.$iNFTWeb, this.$tokenId, this.$callBack, q70Var);
        }

        @Override // defpackage.hd1
        public final Object invoke(c90 c90Var, q70<? super te4> q70Var) {
            return ((AnonymousClass1) create(c90Var, q70Var)).invokeSuspend(te4.a);
        }

        /* JADX WARN: Removed duplicated region for block: B:28:0x0063  */
        @Override // kotlin.coroutines.jvm.internal.BaseContinuationImpl
        /*
            Code decompiled incorrectly, please refer to instructions dump.
            To view partially-correct code enable 'Show inconsistent code' option in preferences
        */
        public final java.lang.Object invokeSuspend(java.lang.Object r7) {
            /*
                r6 = this;
                java.lang.Object r0 = defpackage.gs1.d()
                int r1 = r6.label
                r2 = 2
                r3 = 1
                if (r1 == 0) goto L1e
                if (r1 == r3) goto L1a
                if (r1 != r2) goto L12
                defpackage.o83.b(r7)     // Catch: java.lang.Exception -> L95
                goto L7a
            L12:
                java.lang.IllegalStateException r7 = new java.lang.IllegalStateException
                java.lang.String r0 = "call to 'resume' before 'invoke' with coroutine"
                r7.<init>(r0)
                throw r7
            L1a:
                defpackage.o83.b(r7)     // Catch: java.lang.Exception -> L95
                goto L3e
            L1e:
                defpackage.o83.b(r7)
                im1 r7 = r6.$iNFTWeb     // Catch: java.lang.Exception -> L95
                boolean r1 = r7 instanceof net.safemoon.androidwallet.viewmodels.blockChainClass.NFT721     // Catch: java.lang.Exception -> L95
                if (r1 == 0) goto L5d
                if (r7 == 0) goto L55
                net.safemoon.androidwallet.viewmodels.blockChainClass.NFT721 r7 = (net.safemoon.androidwallet.viewmodels.blockChainClass.NFT721) r7     // Catch: java.lang.Exception -> L95
                java.lang.String r1 = r6.$tokenId     // Catch: java.lang.Exception -> L95
                defpackage.fs1.d(r1)     // Catch: java.lang.Exception -> L95
                java.math.BigInteger r4 = new java.math.BigInteger     // Catch: java.lang.Exception -> L95
                r4.<init>(r1)     // Catch: java.lang.Exception -> L95
                r6.label = r3     // Catch: java.lang.Exception -> L95
                java.lang.Object r7 = r7.B(r4, r6)     // Catch: java.lang.Exception -> L95
                if (r7 != r0) goto L3e
                return r0
            L3e:
                java.lang.Boolean r7 = (java.lang.Boolean) r7     // Catch: java.lang.Exception -> L95
                boolean r7 = r7.booleanValue()     // Catch: java.lang.Exception -> L95
                gb2<net.safemoon.androidwallet.model.nft.NFTBalance> r1 = r6.$callBack     // Catch: java.lang.Exception -> L95
                net.safemoon.androidwallet.model.nft.NFTBalance r4 = new net.safemoon.androidwallet.model.nft.NFTBalance     // Catch: java.lang.Exception -> L95
                net.safemoon.androidwallet.model.nft.NFTType r5 = net.safemoon.androidwallet.model.nft.NFTType.ERC721     // Catch: java.lang.Exception -> L95
                if (r7 == 0) goto L4d
                goto L4e
            L4d:
                r3 = 0
            L4e:
                r4.<init>(r5, r3)     // Catch: java.lang.Exception -> L95
                r1.postValue(r4)     // Catch: java.lang.Exception -> L95
                goto L5d
            L55:
                java.lang.NullPointerException r7 = new java.lang.NullPointerException     // Catch: java.lang.Exception -> L95
                java.lang.String r0 = "null cannot be cast to non-null type net.safemoon.androidwallet.viewmodels.blockChainClass.NFT721"
                r7.<init>(r0)     // Catch: java.lang.Exception -> L95
                throw r7     // Catch: java.lang.Exception -> L95
            L5d:
                im1 r7 = r6.$iNFTWeb     // Catch: java.lang.Exception -> L95
                boolean r1 = r7 instanceof net.safemoon.androidwallet.viewmodels.blockChainClass.NFT1155     // Catch: java.lang.Exception -> L95
                if (r1 == 0) goto L9b
                if (r7 == 0) goto L8d
                net.safemoon.androidwallet.viewmodels.blockChainClass.NFT1155 r7 = (net.safemoon.androidwallet.viewmodels.blockChainClass.NFT1155) r7     // Catch: java.lang.Exception -> L95
                java.lang.String r1 = r6.$tokenId     // Catch: java.lang.Exception -> L95
                defpackage.fs1.d(r1)     // Catch: java.lang.Exception -> L95
                java.math.BigInteger r3 = new java.math.BigInteger     // Catch: java.lang.Exception -> L95
                r3.<init>(r1)     // Catch: java.lang.Exception -> L95
                r6.label = r2     // Catch: java.lang.Exception -> L95
                java.lang.Object r7 = r7.z(r3, r6)     // Catch: java.lang.Exception -> L95
                if (r7 != r0) goto L7a
                return r0
            L7a:
                java.math.BigInteger r7 = (java.math.BigInteger) r7     // Catch: java.lang.Exception -> L95
                gb2<net.safemoon.androidwallet.model.nft.NFTBalance> r0 = r6.$callBack     // Catch: java.lang.Exception -> L95
                net.safemoon.androidwallet.model.nft.NFTBalance r1 = new net.safemoon.androidwallet.model.nft.NFTBalance     // Catch: java.lang.Exception -> L95
                net.safemoon.androidwallet.model.nft.NFTType r2 = net.safemoon.androidwallet.model.nft.NFTType.ERC1155     // Catch: java.lang.Exception -> L95
                int r7 = r7.intValue()     // Catch: java.lang.Exception -> L95
                r1.<init>(r2, r7)     // Catch: java.lang.Exception -> L95
                r0.postValue(r1)     // Catch: java.lang.Exception -> L95
                goto L9b
            L8d:
                java.lang.NullPointerException r7 = new java.lang.NullPointerException     // Catch: java.lang.Exception -> L95
                java.lang.String r0 = "null cannot be cast to non-null type net.safemoon.androidwallet.viewmodels.blockChainClass.NFT1155"
                r7.<init>(r0)     // Catch: java.lang.Exception -> L95
                throw r7     // Catch: java.lang.Exception -> L95
            L95:
                gb2<net.safemoon.androidwallet.model.nft.NFTBalance> r7 = r6.$callBack
                r0 = 0
                r7.postValue(r0)
            L9b:
                te4 r7 = defpackage.te4.a
                return r7
            */
            throw new UnsupportedOperationException("Method not decompiled: net.safemoon.androidwallet.viewmodels.CollectibleViewModel$balance$1.AnonymousClass1.invokeSuspend(java.lang.Object):java.lang.Object");
        }
    }

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public CollectibleViewModel$balance$1(im1 im1Var, String str, gb2<NFTBalance> gb2Var, q70<? super CollectibleViewModel$balance$1> q70Var) {
        super(2, q70Var);
        this.$iNFTWeb = im1Var;
        this.$tokenId = str;
        this.$callBack = gb2Var;
    }

    @Override // kotlin.coroutines.jvm.internal.BaseContinuationImpl
    public final q70<te4> create(Object obj, q70<?> q70Var) {
        return new CollectibleViewModel$balance$1(this.$iNFTWeb, this.$tokenId, this.$callBack, q70Var);
    }

    @Override // defpackage.hd1
    public final Object invoke(c90 c90Var, q70<? super te4> q70Var) {
        return ((CollectibleViewModel$balance$1) create(c90Var, q70Var)).invokeSuspend(te4.a);
    }

    @Override // kotlin.coroutines.jvm.internal.BaseContinuationImpl
    public final Object invokeSuspend(Object obj) {
        Object d = gs1.d();
        int i = this.label;
        if (i == 0) {
            o83.b(obj);
            CoroutineDispatcher b = tp0.b();
            AnonymousClass1 anonymousClass1 = new AnonymousClass1(this.$iNFTWeb, this.$tokenId, this.$callBack, null);
            this.label = 1;
            if (kotlinx.coroutines.a.e(b, anonymousClass1, this) == d) {
                return d;
            }
        } else if (i != 1) {
            throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        } else {
            o83.b(obj);
        }
        return te4.a;
    }
}
