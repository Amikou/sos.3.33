package net.safemoon.androidwallet.viewmodels;

import kotlin.jvm.internal.Lambda;

/* compiled from: MyTokensListViewModel.kt */
/* loaded from: classes2.dex */
public final class MyTokensListViewModel$totalBalanceForDefault$2 extends Lambda implements rc1<gb2<Boolean>> {
    public final /* synthetic */ MyTokensListViewModel this$0;

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public MyTokensListViewModel$totalBalanceForDefault$2(MyTokensListViewModel myTokensListViewModel) {
        super(0);
        this.this$0 = myTokensListViewModel;
    }

    /* JADX WARN: Can't rename method to resolve collision */
    @Override // defpackage.rc1
    public final gb2<Boolean> invoke() {
        return new gb2<>(Boolean.valueOf(bo3.e(this.this$0.a(), "DEFAULT_CHAIN_BALANCE", true)));
    }
}
