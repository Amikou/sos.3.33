package net.safemoon.androidwallet.viewmodels;

import android.app.Application;
import android.net.Uri;
import androidx.paging.CachedPagingDataKt;
import androidx.paging.Pager;
import java.util.Map;
import kotlin.coroutines.jvm.internal.SuspendLambda;
import kotlinx.coroutines.CoroutineDispatcher;
import net.safemoon.androidwallet.R;
import net.safemoon.androidwallet.common.ActiveTokenListMode;
import net.safemoon.androidwallet.common.TokenType;
import net.safemoon.androidwallet.model.AllCryptoList;
import net.safemoon.androidwallet.model.Coin;
import net.safemoon.androidwallet.model.MoonPaySignURL;
import net.safemoon.androidwallet.model.common.PaymentMethod;
import net.safemoon.androidwallet.model.wyre.CheckoutPage;
import org.web3j.abi.datatypes.Address;
import retrofit2.b;
import retrofit2.n;

/* compiled from: HomeViewModel.kt */
/* loaded from: classes2.dex */
public final class HomeViewModel extends gd {
    public final sy1 b;
    public String c;
    public String d;
    public Map<String, ? extends TokenType> e;
    public final sy1 f;
    public final gb2<Boolean> g;
    public final gb2<String> h;
    public final gb2<ActiveTokenListMode> i;
    public final gb2<String> j;
    public final gb2<AllCryptoList> k;
    public final j71<fp2<Coin>> l;
    public final gb2<Boolean> m;

    /* compiled from: HomeViewModel.kt */
    @kotlin.coroutines.jvm.internal.a(c = "net.safemoon.androidwallet.viewmodels.HomeViewModel$1", f = "HomeViewModel.kt", l = {63}, m = "invokeSuspend")
    /* renamed from: net.safemoon.androidwallet.viewmodels.HomeViewModel$1  reason: invalid class name */
    /* loaded from: classes2.dex */
    public static final class AnonymousClass1 extends SuspendLambda implements hd1<c90, q70<? super te4>, Object> {
        public int label;

        /* compiled from: HomeViewModel.kt */
        @kotlin.coroutines.jvm.internal.a(c = "net.safemoon.androidwallet.viewmodels.HomeViewModel$1$1", f = "HomeViewModel.kt", l = {64}, m = "invokeSuspend")
        /* renamed from: net.safemoon.androidwallet.viewmodels.HomeViewModel$1$1  reason: invalid class name and collision with other inner class name */
        /* loaded from: classes2.dex */
        public static final class C02251 extends SuspendLambda implements hd1<c90, q70<? super te4>, Object> {
            public int label;
            public final /* synthetic */ HomeViewModel this$0;

            /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
            public C02251(HomeViewModel homeViewModel, q70<? super C02251> q70Var) {
                super(2, q70Var);
                this.this$0 = homeViewModel;
            }

            @Override // kotlin.coroutines.jvm.internal.BaseContinuationImpl
            public final q70<te4> create(Object obj, q70<?> q70Var) {
                return new C02251(this.this$0, q70Var);
            }

            @Override // defpackage.hd1
            public final Object invoke(c90 c90Var, q70<? super te4> q70Var) {
                return ((C02251) create(c90Var, q70Var)).invokeSuspend(te4.a);
            }

            @Override // kotlin.coroutines.jvm.internal.BaseContinuationImpl
            public final Object invokeSuspend(Object obj) {
                Object d = gs1.d();
                int i = this.label;
                if (i == 0) {
                    o83.b(obj);
                    mu2 mu2Var = mu2.a;
                    Application a = this.this$0.a();
                    fs1.e(a, "getApplication()");
                    nm1 b = mu2Var.b(a);
                    this.label = 1;
                    if (b.a(this) == d) {
                        return d;
                    }
                } else if (i != 1) {
                    throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
                } else {
                    o83.b(obj);
                }
                return te4.a;
            }
        }

        public AnonymousClass1(q70<? super AnonymousClass1> q70Var) {
            super(2, q70Var);
        }

        @Override // kotlin.coroutines.jvm.internal.BaseContinuationImpl
        public final q70<te4> create(Object obj, q70<?> q70Var) {
            return new AnonymousClass1(q70Var);
        }

        @Override // defpackage.hd1
        public final Object invoke(c90 c90Var, q70<? super te4> q70Var) {
            return ((AnonymousClass1) create(c90Var, q70Var)).invokeSuspend(te4.a);
        }

        @Override // kotlin.coroutines.jvm.internal.BaseContinuationImpl
        public final Object invokeSuspend(Object obj) {
            Object d = gs1.d();
            int i = this.label;
            if (i == 0) {
                o83.b(obj);
                CoroutineDispatcher b = tp0.b();
                C02251 c02251 = new C02251(HomeViewModel.this, null);
                this.label = 1;
                if (kotlinx.coroutines.a.e(b, c02251, this) == d) {
                    return d;
                }
            } else if (i != 1) {
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            } else {
                o83.b(obj);
            }
            return te4.a;
        }
    }

    /* compiled from: HomeViewModel.kt */
    /* loaded from: classes2.dex */
    public static final class a implements wu<MoonPaySignURL> {
        public final /* synthetic */ tc1<CheckoutPage, te4> a;
        public final /* synthetic */ tc1<String, te4> b;
        public final /* synthetic */ HomeViewModel c;

        /* JADX WARN: Multi-variable type inference failed */
        public a(tc1<? super CheckoutPage, te4> tc1Var, tc1<? super String, te4> tc1Var2, HomeViewModel homeViewModel) {
            this.a = tc1Var;
            this.b = tc1Var2;
            this.c = homeViewModel;
        }

        @Override // defpackage.wu
        public void a(b<MoonPaySignURL> bVar, Throwable th) {
            fs1.f(bVar, "call");
            fs1.f(th, "t");
            this.b.invoke(this.c.a().getString(R.string.error_text));
        }

        @Override // defpackage.wu
        public void b(b<MoonPaySignURL> bVar, n<MoonPaySignURL> nVar) {
            fs1.f(bVar, "call");
            fs1.f(nVar, "response");
            if (nVar.e()) {
                tc1<CheckoutPage, te4> tc1Var = this.a;
                CheckoutPage checkoutPage = new CheckoutPage();
                MoonPaySignURL a = nVar.a();
                checkoutPage.setUrl(a == null ? null : a.urlWithSignature);
                te4 te4Var = te4.a;
                tc1Var.invoke(checkoutPage);
                return;
            }
            this.b.invoke(this.c.a().getString(R.string.error_text));
        }
    }

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public HomeViewModel(Application application) {
        super(application);
        fs1.f(application, "application");
        this.b = zy1.a(new HomeViewModel$getTokenTypeMapUseCase$2(application));
        this.c = "";
        this.d = "";
        this.f = zy1.a(new HomeViewModel$selectedTokenType$2(application));
        Boolean bool = Boolean.FALSE;
        this.g = new gb2<>(bool);
        as.b(qg1.a, null, null, new AnonymousClass1(null), 3, null);
        this.h = new gb2<>(null);
        this.i = new gb2<>();
        this.j = new gb2<>("");
        this.k = new gb2<>();
        this.l = CachedPagingDataKt.a(new Pager(new ep2(100, 2, false, 100, 0, 0, 48, null), null, new HomeViewModel$coinListFlow$1(this), 2, null).a(), ej4.a(this));
        this.m = new gb2<>(bool);
    }

    public final gb2<String> d() {
        return this.j;
    }

    public final gb2<ActiveTokenListMode> e() {
        return this.i;
    }

    public final j71<fp2<Coin>> f() {
        return CachedPagingDataKt.a(new Pager(new ep2(100, 2, false, 100, 0, 0, 48, null), null, new HomeViewModel$allCoinListFlow$1(this), 2, null).a(), ej4.a(this));
    }

    public final gb2<AllCryptoList> g() {
        return this.k;
    }

    public final void h(PaymentMethod paymentMethod, String str, tc1<? super CheckoutPage, te4> tc1Var, tc1<? super String, te4> tc1Var2) {
        Uri build;
        String uri;
        fs1.f(paymentMethod, "paymentMethod");
        fs1.f(str, "currencyCode");
        fs1.f(tc1Var, "responseCall");
        fs1.f(tc1Var2, "errorCall");
        String i = bo3.i(a(), "SAFEMOON_ADDRESS");
        if (paymentMethod == PaymentMethod.MOONPAY) {
            q92 q92Var = q92.a;
            fs1.e(i, Address.TYPE_NAME);
            Uri.Builder a2 = q92Var.a(i, str);
            if (a2 == null || (build = a2.build()) == null || (uri = build.toString()) == null) {
                return;
            }
            a4.k().c(uri).n(new a(tc1Var, tc1Var2, this));
        }
    }

    public final j71<fp2<Coin>> i() {
        return this.l;
    }

    public final em1 j() {
        return (em1) this.b.getValue();
    }

    public final gb2<Boolean> k() {
        return this.g;
    }

    public final gb2<TokenType> l() {
        return (gb2) this.f.getValue();
    }

    public final Map<String, TokenType> m() {
        if (this.e == null) {
            this.e = j().get();
        }
        Map map = this.e;
        fs1.d(map);
        return map;
    }

    public final gb2<String> n() {
        return this.h;
    }

    public final gb2<Boolean> o() {
        return this.m;
    }

    public final void p(TokenType tokenType) {
        fs1.f(tokenType, "tokenType");
        l().setValue(tokenType);
    }

    public final void q(String str, String str2, ActiveTokenListMode activeTokenListMode) {
        fs1.f(str, "valueSortDir");
        fs1.f(str2, "valueSort");
        fs1.f(activeTokenListMode, "activeTokenListMode");
        this.c = str2;
        this.d = str;
        this.i.setValue(activeTokenListMode);
    }

    public final void r(String str) {
        fs1.f(str, "filter");
        this.j.setValue(str);
    }
}
