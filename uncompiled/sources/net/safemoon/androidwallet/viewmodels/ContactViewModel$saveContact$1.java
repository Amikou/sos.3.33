package net.safemoon.androidwallet.viewmodels;

import kotlin.coroutines.jvm.internal.SuspendLambda;
import kotlin.coroutines.jvm.internal.a;
import kotlinx.coroutines.CoroutineDispatcher;
import net.safemoon.androidwallet.model.contact.RequestContact;

/* compiled from: ContactViewModel.kt */
@a(c = "net.safemoon.androidwallet.viewmodels.ContactViewModel$saveContact$1", f = "ContactViewModel.kt", l = {100}, m = "invokeSuspend")
/* loaded from: classes2.dex */
public final class ContactViewModel$saveContact$1 extends SuspendLambda implements hd1<c90, q70<? super te4>, Object> {
    public final /* synthetic */ rc1<te4> $callBack;
    public final /* synthetic */ RequestContact $requestContact;
    public int label;
    public final /* synthetic */ ContactViewModel this$0;

    /* compiled from: ContactViewModel.kt */
    @a(c = "net.safemoon.androidwallet.viewmodels.ContactViewModel$saveContact$1$1", f = "ContactViewModel.kt", l = {103, 108, 112, 118, 115, 126, 137, 138}, m = "invokeSuspend")
    /* renamed from: net.safemoon.androidwallet.viewmodels.ContactViewModel$saveContact$1$1  reason: invalid class name */
    /* loaded from: classes2.dex */
    public static final class AnonymousClass1 extends SuspendLambda implements hd1<c90, q70<? super te4>, Object> {
        public final /* synthetic */ rc1<te4> $callBack;
        public final /* synthetic */ RequestContact $requestContact;
        public int I$0;
        public Object L$0;
        public Object L$1;
        public Object L$2;
        public Object L$3;
        public Object L$4;
        public Object L$5;
        public Object L$6;
        public Object L$7;
        public int label;
        public final /* synthetic */ ContactViewModel this$0;

        /* compiled from: ContactViewModel.kt */
        @a(c = "net.safemoon.androidwallet.viewmodels.ContactViewModel$saveContact$1$1$3", f = "ContactViewModel.kt", l = {}, m = "invokeSuspend")
        /* renamed from: net.safemoon.androidwallet.viewmodels.ContactViewModel$saveContact$1$1$3  reason: invalid class name */
        /* loaded from: classes2.dex */
        public static final class AnonymousClass3 extends SuspendLambda implements hd1<c90, q70<? super te4>, Object> {
            public final /* synthetic */ rc1<te4> $callBack;
            public int label;

            /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
            public AnonymousClass3(rc1<te4> rc1Var, q70<? super AnonymousClass3> q70Var) {
                super(2, q70Var);
                this.$callBack = rc1Var;
            }

            @Override // kotlin.coroutines.jvm.internal.BaseContinuationImpl
            public final q70<te4> create(Object obj, q70<?> q70Var) {
                return new AnonymousClass3(this.$callBack, q70Var);
            }

            @Override // defpackage.hd1
            public final Object invoke(c90 c90Var, q70<? super te4> q70Var) {
                return ((AnonymousClass3) create(c90Var, q70Var)).invokeSuspend(te4.a);
            }

            @Override // kotlin.coroutines.jvm.internal.BaseContinuationImpl
            public final Object invokeSuspend(Object obj) {
                gs1.d();
                if (this.label == 0) {
                    o83.b(obj);
                    this.$callBack.invoke();
                    return te4.a;
                }
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
        }

        /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
        public AnonymousClass1(RequestContact requestContact, ContactViewModel contactViewModel, rc1<te4> rc1Var, q70<? super AnonymousClass1> q70Var) {
            super(2, q70Var);
            this.$requestContact = requestContact;
            this.this$0 = contactViewModel;
            this.$callBack = rc1Var;
        }

        @Override // kotlin.coroutines.jvm.internal.BaseContinuationImpl
        public final q70<te4> create(Object obj, q70<?> q70Var) {
            return new AnonymousClass1(this.$requestContact, this.this$0, this.$callBack, q70Var);
        }

        @Override // defpackage.hd1
        public final Object invoke(c90 c90Var, q70<? super te4> q70Var) {
            return ((AnonymousClass1) create(c90Var, q70Var)).invokeSuspend(te4.a);
        }

        /* JADX WARN: Removed duplicated region for block: B:100:0x0319 A[Catch: Exception -> 0x0394, TryCatch #0 {Exception -> 0x0394, blocks: (B:7:0x0024, B:113:0x0376, B:101:0x0321, B:103:0x0327, B:107:0x0356, B:110:0x0361, B:115:0x037e, B:10:0x0036, B:13:0x0052, B:46:0x0169, B:48:0x016f, B:52:0x01a3, B:54:0x01ab, B:55:0x01be, B:57:0x01c4, B:58:0x01d6, B:59:0x01ef, B:61:0x01f5, B:65:0x0228, B:67:0x022c, B:75:0x0242, B:87:0x02a7, B:78:0x0257, B:82:0x0284, B:86:0x029f, B:85:0x028f, B:92:0x02d4, B:96:0x02fd, B:95:0x02e6, B:100:0x0319, B:16:0x006a, B:19:0x009c, B:22:0x00c2, B:25:0x00e0, B:26:0x00e8, B:37:0x011b, B:41:0x0125, B:42:0x0138, B:44:0x013e, B:45:0x0150, B:29:0x00f1, B:31:0x00f9, B:34:0x0102), top: B:124:0x000d }] */
        /* JADX WARN: Removed duplicated region for block: B:103:0x0327 A[Catch: Exception -> 0x0394, TryCatch #0 {Exception -> 0x0394, blocks: (B:7:0x0024, B:113:0x0376, B:101:0x0321, B:103:0x0327, B:107:0x0356, B:110:0x0361, B:115:0x037e, B:10:0x0036, B:13:0x0052, B:46:0x0169, B:48:0x016f, B:52:0x01a3, B:54:0x01ab, B:55:0x01be, B:57:0x01c4, B:58:0x01d6, B:59:0x01ef, B:61:0x01f5, B:65:0x0228, B:67:0x022c, B:75:0x0242, B:87:0x02a7, B:78:0x0257, B:82:0x0284, B:86:0x029f, B:85:0x028f, B:92:0x02d4, B:96:0x02fd, B:95:0x02e6, B:100:0x0319, B:16:0x006a, B:19:0x009c, B:22:0x00c2, B:25:0x00e0, B:26:0x00e8, B:37:0x011b, B:41:0x0125, B:42:0x0138, B:44:0x013e, B:45:0x0150, B:29:0x00f1, B:31:0x00f9, B:34:0x0102), top: B:124:0x000d }] */
        /* JADX WARN: Removed duplicated region for block: B:109:0x0360  */
        /* JADX WARN: Removed duplicated region for block: B:110:0x0361 A[Catch: Exception -> 0x0394, TryCatch #0 {Exception -> 0x0394, blocks: (B:7:0x0024, B:113:0x0376, B:101:0x0321, B:103:0x0327, B:107:0x0356, B:110:0x0361, B:115:0x037e, B:10:0x0036, B:13:0x0052, B:46:0x0169, B:48:0x016f, B:52:0x01a3, B:54:0x01ab, B:55:0x01be, B:57:0x01c4, B:58:0x01d6, B:59:0x01ef, B:61:0x01f5, B:65:0x0228, B:67:0x022c, B:75:0x0242, B:87:0x02a7, B:78:0x0257, B:82:0x0284, B:86:0x029f, B:85:0x028f, B:92:0x02d4, B:96:0x02fd, B:95:0x02e6, B:100:0x0319, B:16:0x006a, B:19:0x009c, B:22:0x00c2, B:25:0x00e0, B:26:0x00e8, B:37:0x011b, B:41:0x0125, B:42:0x0138, B:44:0x013e, B:45:0x0150, B:29:0x00f1, B:31:0x00f9, B:34:0x0102), top: B:124:0x000d }] */
        /* JADX WARN: Removed duplicated region for block: B:115:0x037e A[Catch: Exception -> 0x0394, TRY_LEAVE, TryCatch #0 {Exception -> 0x0394, blocks: (B:7:0x0024, B:113:0x0376, B:101:0x0321, B:103:0x0327, B:107:0x0356, B:110:0x0361, B:115:0x037e, B:10:0x0036, B:13:0x0052, B:46:0x0169, B:48:0x016f, B:52:0x01a3, B:54:0x01ab, B:55:0x01be, B:57:0x01c4, B:58:0x01d6, B:59:0x01ef, B:61:0x01f5, B:65:0x0228, B:67:0x022c, B:75:0x0242, B:87:0x02a7, B:78:0x0257, B:82:0x0284, B:86:0x029f, B:85:0x028f, B:92:0x02d4, B:96:0x02fd, B:95:0x02e6, B:100:0x0319, B:16:0x006a, B:19:0x009c, B:22:0x00c2, B:25:0x00e0, B:26:0x00e8, B:37:0x011b, B:41:0x0125, B:42:0x0138, B:44:0x013e, B:45:0x0150, B:29:0x00f1, B:31:0x00f9, B:34:0x0102), top: B:124:0x000d }] */
        /* JADX WARN: Removed duplicated region for block: B:39:0x0123  */
        /* JADX WARN: Removed duplicated region for block: B:44:0x013e A[Catch: Exception -> 0x0394, LOOP:1: B:42:0x0138->B:44:0x013e, LOOP_END, TryCatch #0 {Exception -> 0x0394, blocks: (B:7:0x0024, B:113:0x0376, B:101:0x0321, B:103:0x0327, B:107:0x0356, B:110:0x0361, B:115:0x037e, B:10:0x0036, B:13:0x0052, B:46:0x0169, B:48:0x016f, B:52:0x01a3, B:54:0x01ab, B:55:0x01be, B:57:0x01c4, B:58:0x01d6, B:59:0x01ef, B:61:0x01f5, B:65:0x0228, B:67:0x022c, B:75:0x0242, B:87:0x02a7, B:78:0x0257, B:82:0x0284, B:86:0x029f, B:85:0x028f, B:92:0x02d4, B:96:0x02fd, B:95:0x02e6, B:100:0x0319, B:16:0x006a, B:19:0x009c, B:22:0x00c2, B:25:0x00e0, B:26:0x00e8, B:37:0x011b, B:41:0x0125, B:42:0x0138, B:44:0x013e, B:45:0x0150, B:29:0x00f1, B:31:0x00f9, B:34:0x0102), top: B:124:0x000d }] */
        /* JADX WARN: Removed duplicated region for block: B:48:0x016f A[Catch: Exception -> 0x0394, TryCatch #0 {Exception -> 0x0394, blocks: (B:7:0x0024, B:113:0x0376, B:101:0x0321, B:103:0x0327, B:107:0x0356, B:110:0x0361, B:115:0x037e, B:10:0x0036, B:13:0x0052, B:46:0x0169, B:48:0x016f, B:52:0x01a3, B:54:0x01ab, B:55:0x01be, B:57:0x01c4, B:58:0x01d6, B:59:0x01ef, B:61:0x01f5, B:65:0x0228, B:67:0x022c, B:75:0x0242, B:87:0x02a7, B:78:0x0257, B:82:0x0284, B:86:0x029f, B:85:0x028f, B:92:0x02d4, B:96:0x02fd, B:95:0x02e6, B:100:0x0319, B:16:0x006a, B:19:0x009c, B:22:0x00c2, B:25:0x00e0, B:26:0x00e8, B:37:0x011b, B:41:0x0125, B:42:0x0138, B:44:0x013e, B:45:0x0150, B:29:0x00f1, B:31:0x00f9, B:34:0x0102), top: B:124:0x000d }] */
        /* JADX WARN: Removed duplicated region for block: B:54:0x01ab A[Catch: Exception -> 0x0394, TryCatch #0 {Exception -> 0x0394, blocks: (B:7:0x0024, B:113:0x0376, B:101:0x0321, B:103:0x0327, B:107:0x0356, B:110:0x0361, B:115:0x037e, B:10:0x0036, B:13:0x0052, B:46:0x0169, B:48:0x016f, B:52:0x01a3, B:54:0x01ab, B:55:0x01be, B:57:0x01c4, B:58:0x01d6, B:59:0x01ef, B:61:0x01f5, B:65:0x0228, B:67:0x022c, B:75:0x0242, B:87:0x02a7, B:78:0x0257, B:82:0x0284, B:86:0x029f, B:85:0x028f, B:92:0x02d4, B:96:0x02fd, B:95:0x02e6, B:100:0x0319, B:16:0x006a, B:19:0x009c, B:22:0x00c2, B:25:0x00e0, B:26:0x00e8, B:37:0x011b, B:41:0x0125, B:42:0x0138, B:44:0x013e, B:45:0x0150, B:29:0x00f1, B:31:0x00f9, B:34:0x0102), top: B:124:0x000d }] */
        /* JADX WARN: Removed duplicated region for block: B:61:0x01f5 A[Catch: Exception -> 0x0394, TryCatch #0 {Exception -> 0x0394, blocks: (B:7:0x0024, B:113:0x0376, B:101:0x0321, B:103:0x0327, B:107:0x0356, B:110:0x0361, B:115:0x037e, B:10:0x0036, B:13:0x0052, B:46:0x0169, B:48:0x016f, B:52:0x01a3, B:54:0x01ab, B:55:0x01be, B:57:0x01c4, B:58:0x01d6, B:59:0x01ef, B:61:0x01f5, B:65:0x0228, B:67:0x022c, B:75:0x0242, B:87:0x02a7, B:78:0x0257, B:82:0x0284, B:86:0x029f, B:85:0x028f, B:92:0x02d4, B:96:0x02fd, B:95:0x02e6, B:100:0x0319, B:16:0x006a, B:19:0x009c, B:22:0x00c2, B:25:0x00e0, B:26:0x00e8, B:37:0x011b, B:41:0x0125, B:42:0x0138, B:44:0x013e, B:45:0x0150, B:29:0x00f1, B:31:0x00f9, B:34:0x0102), top: B:124:0x000d }] */
        /* JADX WARN: Removed duplicated region for block: B:72:0x0238  */
        /* JADX WARN: Removed duplicated region for block: B:73:0x023c  */
        /* JADX WARN: Removed duplicated region for block: B:74:0x0241  */
        /* JADX WARN: Removed duplicated region for block: B:77:0x0252  */
        /* JADX WARN: Removed duplicated region for block: B:78:0x0257 A[Catch: Exception -> 0x0394, TryCatch #0 {Exception -> 0x0394, blocks: (B:7:0x0024, B:113:0x0376, B:101:0x0321, B:103:0x0327, B:107:0x0356, B:110:0x0361, B:115:0x037e, B:10:0x0036, B:13:0x0052, B:46:0x0169, B:48:0x016f, B:52:0x01a3, B:54:0x01ab, B:55:0x01be, B:57:0x01c4, B:58:0x01d6, B:59:0x01ef, B:61:0x01f5, B:65:0x0228, B:67:0x022c, B:75:0x0242, B:87:0x02a7, B:78:0x0257, B:82:0x0284, B:86:0x029f, B:85:0x028f, B:92:0x02d4, B:96:0x02fd, B:95:0x02e6, B:100:0x0319, B:16:0x006a, B:19:0x009c, B:22:0x00c2, B:25:0x00e0, B:26:0x00e8, B:37:0x011b, B:41:0x0125, B:42:0x0138, B:44:0x013e, B:45:0x0150, B:29:0x00f1, B:31:0x00f9, B:34:0x0102), top: B:124:0x000d }] */
        /* JADX WARN: Removed duplicated region for block: B:84:0x028e  */
        /* JADX WARN: Removed duplicated region for block: B:85:0x028f A[Catch: Exception -> 0x0394, TryCatch #0 {Exception -> 0x0394, blocks: (B:7:0x0024, B:113:0x0376, B:101:0x0321, B:103:0x0327, B:107:0x0356, B:110:0x0361, B:115:0x037e, B:10:0x0036, B:13:0x0052, B:46:0x0169, B:48:0x016f, B:52:0x01a3, B:54:0x01ab, B:55:0x01be, B:57:0x01c4, B:58:0x01d6, B:59:0x01ef, B:61:0x01f5, B:65:0x0228, B:67:0x022c, B:75:0x0242, B:87:0x02a7, B:78:0x0257, B:82:0x0284, B:86:0x029f, B:85:0x028f, B:92:0x02d4, B:96:0x02fd, B:95:0x02e6, B:100:0x0319, B:16:0x006a, B:19:0x009c, B:22:0x00c2, B:25:0x00e0, B:26:0x00e8, B:37:0x011b, B:41:0x0125, B:42:0x0138, B:44:0x013e, B:45:0x0150, B:29:0x00f1, B:31:0x00f9, B:34:0x0102), top: B:124:0x000d }] */
        /* JADX WARN: Removed duplicated region for block: B:89:0x02c3 A[RETURN] */
        /* JADX WARN: Removed duplicated region for block: B:90:0x02c4  */
        /* JADX WARN: Removed duplicated region for block: B:92:0x02d4 A[Catch: Exception -> 0x0394, TryCatch #0 {Exception -> 0x0394, blocks: (B:7:0x0024, B:113:0x0376, B:101:0x0321, B:103:0x0327, B:107:0x0356, B:110:0x0361, B:115:0x037e, B:10:0x0036, B:13:0x0052, B:46:0x0169, B:48:0x016f, B:52:0x01a3, B:54:0x01ab, B:55:0x01be, B:57:0x01c4, B:58:0x01d6, B:59:0x01ef, B:61:0x01f5, B:65:0x0228, B:67:0x022c, B:75:0x0242, B:87:0x02a7, B:78:0x0257, B:82:0x0284, B:86:0x029f, B:85:0x028f, B:92:0x02d4, B:96:0x02fd, B:95:0x02e6, B:100:0x0319, B:16:0x006a, B:19:0x009c, B:22:0x00c2, B:25:0x00e0, B:26:0x00e8, B:37:0x011b, B:41:0x0125, B:42:0x0138, B:44:0x013e, B:45:0x0150, B:29:0x00f1, B:31:0x00f9, B:34:0x0102), top: B:124:0x000d }] */
        /* JADX WARN: Unsupported multi-entry loop pattern (BACK_EDGE: B:109:0x0360 -> B:114:0x0378). Please submit an issue!!! */
        /* JADX WARN: Unsupported multi-entry loop pattern (BACK_EDGE: B:111:0x0373 -> B:113:0x0376). Please submit an issue!!! */
        /* JADX WARN: Unsupported multi-entry loop pattern (BACK_EDGE: B:73:0x023c -> B:59:0x01ef). Please submit an issue!!! */
        /* JADX WARN: Unsupported multi-entry loop pattern (BACK_EDGE: B:90:0x02c4 -> B:91:0x02d0). Please submit an issue!!! */
        /* JADX WARN: Unsupported multi-entry loop pattern (BACK_EDGE: B:99:0x0313 -> B:91:0x02d0). Please submit an issue!!! */
        @Override // kotlin.coroutines.jvm.internal.BaseContinuationImpl
        /*
            Code decompiled incorrectly, please refer to instructions dump.
            To view partially-correct code enable 'Show inconsistent code' option in preferences
        */
        public final java.lang.Object invokeSuspend(java.lang.Object r19) {
            /*
                Method dump skipped, instructions count: 956
                To view this dump change 'Code comments level' option to 'DEBUG'
            */
            throw new UnsupportedOperationException("Method not decompiled: net.safemoon.androidwallet.viewmodels.ContactViewModel$saveContact$1.AnonymousClass1.invokeSuspend(java.lang.Object):java.lang.Object");
        }
    }

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public ContactViewModel$saveContact$1(RequestContact requestContact, ContactViewModel contactViewModel, rc1<te4> rc1Var, q70<? super ContactViewModel$saveContact$1> q70Var) {
        super(2, q70Var);
        this.$requestContact = requestContact;
        this.this$0 = contactViewModel;
        this.$callBack = rc1Var;
    }

    @Override // kotlin.coroutines.jvm.internal.BaseContinuationImpl
    public final q70<te4> create(Object obj, q70<?> q70Var) {
        return new ContactViewModel$saveContact$1(this.$requestContact, this.this$0, this.$callBack, q70Var);
    }

    @Override // defpackage.hd1
    public final Object invoke(c90 c90Var, q70<? super te4> q70Var) {
        return ((ContactViewModel$saveContact$1) create(c90Var, q70Var)).invokeSuspend(te4.a);
    }

    @Override // kotlin.coroutines.jvm.internal.BaseContinuationImpl
    public final Object invokeSuspend(Object obj) {
        Object d = gs1.d();
        int i = this.label;
        if (i == 0) {
            o83.b(obj);
            CoroutineDispatcher b = tp0.b();
            AnonymousClass1 anonymousClass1 = new AnonymousClass1(this.$requestContact, this.this$0, this.$callBack, null);
            this.label = 1;
            if (kotlinx.coroutines.a.e(b, anonymousClass1, this) == d) {
                return d;
            }
        } else if (i != 1) {
            throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        } else {
            o83.b(obj);
        }
        return te4.a;
    }
}
