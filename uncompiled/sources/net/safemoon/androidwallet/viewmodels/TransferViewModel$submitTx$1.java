package net.safemoon.androidwallet.viewmodels;

import kotlin.coroutines.jvm.internal.ContinuationImpl;
import kotlin.coroutines.jvm.internal.a;

/* compiled from: TransferViewModel.kt */
@a(c = "net.safemoon.androidwallet.viewmodels.TransferViewModel", f = "TransferViewModel.kt", l = {126, 141, 153}, m = "submitTx")
/* loaded from: classes2.dex */
public final class TransferViewModel$submitTx$1 extends ContinuationImpl {
    public Object L$0;
    public Object L$1;
    public int label;
    public /* synthetic */ Object result;
    public final /* synthetic */ TransferViewModel this$0;

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public TransferViewModel$submitTx$1(TransferViewModel transferViewModel, q70<? super TransferViewModel$submitTx$1> q70Var) {
        super(q70Var);
        this.this$0 = transferViewModel;
    }

    @Override // kotlin.coroutines.jvm.internal.BaseContinuationImpl
    public final Object invokeSuspend(Object obj) {
        Object n;
        this.result = obj;
        this.label |= Integer.MIN_VALUE;
        n = this.this$0.n(null, null, this);
        return n;
    }
}
