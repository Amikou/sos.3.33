package net.safemoon.androidwallet.viewmodels;

import kotlin.coroutines.jvm.internal.SuspendLambda;
import kotlin.coroutines.jvm.internal.a;

/* compiled from: AKTWebSocketHandlingViewModel.kt */
@a(c = "net.safemoon.androidwallet.viewmodels.AKTWebSocketHandlingViewModel$myWebInterface$1$onError$1", f = "AKTWebSocketHandlingViewModel.kt", l = {}, m = "invokeSuspend")
/* loaded from: classes2.dex */
public final class AKTWebSocketHandlingViewModel$myWebInterface$1$onError$1 extends SuspendLambda implements hd1<c90, q70<? super te4>, Object> {
    public int label;
    public final /* synthetic */ AKTWebSocketHandlingViewModel this$0;

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public AKTWebSocketHandlingViewModel$myWebInterface$1$onError$1(AKTWebSocketHandlingViewModel aKTWebSocketHandlingViewModel, q70<? super AKTWebSocketHandlingViewModel$myWebInterface$1$onError$1> q70Var) {
        super(2, q70Var);
        this.this$0 = aKTWebSocketHandlingViewModel;
    }

    @Override // kotlin.coroutines.jvm.internal.BaseContinuationImpl
    public final q70<te4> create(Object obj, q70<?> q70Var) {
        return new AKTWebSocketHandlingViewModel$myWebInterface$1$onError$1(this.this$0, q70Var);
    }

    @Override // defpackage.hd1
    public final Object invoke(c90 c90Var, q70<? super te4> q70Var) {
        return ((AKTWebSocketHandlingViewModel$myWebInterface$1$onError$1) create(c90Var, q70Var)).invokeSuspend(te4.a);
    }

    @Override // kotlin.coroutines.jvm.internal.BaseContinuationImpl
    public final Object invokeSuspend(Object obj) {
        gs1.d();
        if (this.label == 0) {
            o83.b(obj);
            this.this$0.g("CONNECTION_REFUSED");
            return te4.a;
        }
        throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
    }
}
