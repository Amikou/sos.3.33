package net.safemoon.androidwallet.viewmodels;

import java.util.HashMap;
import java.util.List;
import net.safemoon.androidwallet.model.Data;
import net.safemoon.androidwallet.model.HistoricalDatum;
import net.safemoon.androidwallet.model.HistoricalList;
import net.safemoon.androidwallet.model.USDT;
import net.safemoon.androidwallet.model.cmc.coinPrice.CoinPriceStats;
import net.safemoon.androidwallet.model.cmc.coinPrice.CoinPriceStatsData;
import net.safemoon.androidwallet.model.cmc.coinPrice.CoinPriceStatsDataQuoteList;
import net.safemoon.androidwallet.model.cmc.coinPrice.CoinPriceStatsUsd;
import net.safemoon.androidwallet.model.tokensInfo.CurrencyTokenInfo;
import org.web3j.abi.datatypes.Address;
import retrofit2.n;

/* compiled from: GraphViewModel.kt */
/* loaded from: classes2.dex */
public final class GraphViewModel extends dj4 {
    public final jt a;
    public final e42 b;
    public final gb2<Boolean> c;
    public final gb2<Boolean> d;
    public final gb2<CoinPriceStats> e;
    public final gb2<HistoricalList> f;
    public final gb2<CurrencyTokenInfo> g;
    public final HashMap<String, String> h;

    /* compiled from: GraphViewModel.kt */
    /* loaded from: classes2.dex */
    public static final class a extends p83<CoinPriceStats> {
        public final /* synthetic */ String b;

        public a(String str) {
            this.b = str;
        }

        @Override // defpackage.p83, defpackage.wu
        public void a(retrofit2.b<CoinPriceStats> bVar, Throwable th) {
            fs1.f(bVar, "call");
            fs1.f(th, "t");
            super.a(bVar, th);
            GraphViewModel.this.b().setValue(null);
        }

        @Override // defpackage.wu
        public void b(retrofit2.b<CoinPriceStats> bVar, n<CoinPriceStats> nVar) {
            List<CoinPriceStatsDataQuoteList> quote;
            CoinPriceStatsData data;
            fs1.f(bVar, "call");
            fs1.f(nVar, "response");
            CoinPriceStats a = nVar.a();
            if (a != null && (data = a.getData()) != null) {
                data.setSymbol(kt.g(data.getSymbol(), this.b));
            }
            gb2<CoinPriceStats> b = GraphViewModel.this.b();
            if (a == null) {
                a = null;
            } else {
                CoinPriceStatsData data2 = a.getData();
                if (data2 != null && (quote = data2.getQuote()) != null) {
                    for (CoinPriceStatsDataQuoteList coinPriceStatsDataQuoteList : quote) {
                        CoinPriceStatsUsd usd = coinPriceStatsDataQuoteList.getQuote().getUsd();
                        usd.setOpenValue((float) v21.a(usd.getOpenValue()));
                        usd.setCloseValue((float) v21.a(usd.getCloseValue()));
                        usd.setHighValue((float) v21.a(usd.getHighValue()));
                        usd.setLowValue((float) v21.a(usd.getLowValue()));
                    }
                }
                te4 te4Var = te4.a;
            }
            b.setValue(a);
        }
    }

    /* compiled from: GraphViewModel.kt */
    /* loaded from: classes2.dex */
    public static final class b extends p83<HistoricalList> {
        public final /* synthetic */ String b;

        public b(String str) {
            this.b = str;
        }

        @Override // defpackage.p83, defpackage.wu
        public void a(retrofit2.b<HistoricalList> bVar, Throwable th) {
            fs1.f(bVar, "call");
            fs1.f(th, "t");
            super.a(bVar, th);
            GraphViewModel.this.c().setValue(null);
        }

        @Override // defpackage.wu
        public void b(retrofit2.b<HistoricalList> bVar, n<HistoricalList> nVar) {
            List<HistoricalDatum> quotes;
            Data data;
            fs1.f(bVar, "call");
            fs1.f(nVar, "response");
            HistoricalList a = nVar.a();
            if (a != null && (data = a.getData()) != null) {
                String str = this.b;
                String symbol = data.getSymbol();
                fs1.e(symbol, "this.symbol");
                data.setSymbol(kt.g(symbol, str));
            }
            gb2<HistoricalList> c = GraphViewModel.this.c();
            if (a == null) {
                a = null;
            } else {
                Data data2 = a.getData();
                if (data2 != null && (quotes = data2.getQuotes()) != null) {
                    for (HistoricalDatum historicalDatum : quotes) {
                        USDT usd = historicalDatum.getQuote().getUSD();
                        Double price = usd.getPrice();
                        fs1.e(price, "price");
                        usd.setPrice(Double.valueOf(v21.a(price.doubleValue())));
                    }
                }
                te4 te4Var = te4.a;
            }
            c.setValue(a);
        }
    }

    public GraphViewModel(jt jtVar, e42 e42Var) {
        fs1.f(jtVar, "cmcApiInterface");
        fs1.f(e42Var, "marketApiInterface");
        this.a = jtVar;
        this.b = e42Var;
        Boolean bool = Boolean.FALSE;
        this.c = new gb2<>(bool);
        this.d = new gb2<>(bool);
        this.e = new gb2<>();
        this.f = new gb2<>();
        this.g = new gb2<>(null);
        this.h = new HashMap<>();
    }

    public final gb2<CoinPriceStats> b() {
        return this.e;
    }

    public final gb2<HistoricalList> c() {
        return this.f;
    }

    public final gb2<CurrencyTokenInfo> d() {
        return this.g;
    }

    public final void e(String str, String str2, String str3, String str4, String str5) {
        retrofit2.b<CoinPriceStats> e;
        fs1.f(str, "symbol");
        fs1.f(str3, "startDate");
        fs1.f(str4, "_endDate");
        fs1.f(str5, "timeperiod");
        String str6 = str + '-' + ((Object) str2) + '-' + str3 + '-' + str5;
        if (this.h.containsKey(str6)) {
            str4 = this.h.get(str6);
        } else {
            this.h.put(str6, str4);
        }
        String str7 = str4;
        if (str2 == null) {
            e = this.a.g(kt.e(str), str3, str7, "USD", str5, "1h");
        } else {
            e = this.a.e(str2, str3, str7, "USD", str5, "1h");
        }
        e.n(new a(str));
    }

    public final void f(String str, String str2, String str3, String str4, String str5) {
        retrofit2.b<HistoricalList> c;
        fs1.f(str, "symbol");
        fs1.f(str3, "startDate");
        fs1.f(str4, "_endDate");
        fs1.f(str5, "interval");
        String str6 = str + '-' + ((Object) str2) + '-' + str3 + '-' + str5;
        if (this.h.containsKey(str6)) {
            str4 = this.h.get(str6);
        } else {
            this.h.put(str6, str4);
        }
        String str7 = str4;
        if (str2 != null) {
            Integer l = cv3.l(str2);
            if ((l == null ? 0 : l.intValue()) != 0) {
                c = this.a.h(str2, str5, str3, str7, "USD");
                c.n(new b(str));
            }
        }
        c = this.a.c(kt.e(str), str5, str3, str7, "USD");
        c.n(new b(str));
    }

    public final void g(String str) {
        fs1.f(str, Address.TYPE_NAME);
        kotlinx.coroutines.a.b(ej4.a(this), null, null, new GraphViewModel$getPriceByAddress$1(this, str, null), 3, null);
    }

    public final gb2<Boolean> h() {
        return this.d;
    }

    public final gb2<Boolean> i() {
        return this.c;
    }
}
