package net.safemoon.androidwallet.viewmodels;

import kotlin.coroutines.jvm.internal.SuspendLambda;
import kotlin.coroutines.jvm.internal.a;
import kotlinx.coroutines.CoroutineDispatcher;
import okhttp3.internal.http.StatusLine;

/* compiled from: CalculatorViewModel.kt */
@a(c = "net.safemoon.androidwallet.viewmodels.CalculatorViewModel$fetchTokenBalance$1$1", f = "CalculatorViewModel.kt", l = {304}, m = "invokeSuspend")
/* loaded from: classes2.dex */
public final class CalculatorViewModel$fetchTokenBalance$1$1 extends SuspendLambda implements hd1<c90, q70<? super te4>, Object> {
    public final /* synthetic */ q9 $model;
    public int label;
    public final /* synthetic */ CalculatorViewModel this$0;

    /* compiled from: CalculatorViewModel.kt */
    @a(c = "net.safemoon.androidwallet.viewmodels.CalculatorViewModel$fetchTokenBalance$1$1$1", f = "CalculatorViewModel.kt", l = {StatusLine.HTTP_TEMP_REDIRECT}, m = "invokeSuspend")
    /* renamed from: net.safemoon.androidwallet.viewmodels.CalculatorViewModel$fetchTokenBalance$1$1$1  reason: invalid class name */
    /* loaded from: classes2.dex */
    public static final class AnonymousClass1 extends SuspendLambda implements hd1<c90, q70<? super te4>, Object> {
        public final /* synthetic */ q9 $model;
        public Object L$0;
        public Object L$1;
        public int label;
        public final /* synthetic */ CalculatorViewModel this$0;

        /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
        public AnonymousClass1(CalculatorViewModel calculatorViewModel, q9 q9Var, q70<? super AnonymousClass1> q70Var) {
            super(2, q70Var);
            this.this$0 = calculatorViewModel;
            this.$model = q9Var;
        }

        @Override // kotlin.coroutines.jvm.internal.BaseContinuationImpl
        public final q70<te4> create(Object obj, q70<?> q70Var) {
            return new AnonymousClass1(this.this$0, this.$model, q70Var);
        }

        @Override // defpackage.hd1
        public final Object invoke(c90 c90Var, q70<? super te4> q70Var) {
            return ((AnonymousClass1) create(c90Var, q70Var)).invokeSuspend(te4.a);
        }

        /* JADX WARN: Removed duplicated region for block: B:20:0x0062 A[Catch: Exception -> 0x0069, TryCatch #0 {Exception -> 0x0069, blocks: (B:6:0x0015, B:18:0x005b, B:20:0x0062, B:21:0x0064), top: B:28:0x0015 }] */
        @Override // kotlin.coroutines.jvm.internal.BaseContinuationImpl
        /*
            Code decompiled incorrectly, please refer to instructions dump.
            To view partially-correct code enable 'Show inconsistent code' option in preferences
        */
        public final java.lang.Object invokeSuspend(java.lang.Object r9) {
            /*
                r8 = this;
                java.lang.Object r0 = defpackage.gs1.d()
                int r1 = r8.label
                java.lang.String r2 = "{\n                      …ERO\n                    }"
                r3 = 1
                if (r1 == 0) goto L21
                if (r1 != r3) goto L19
                java.lang.Object r0 = r8.L$1
                net.safemoon.androidwallet.viewmodels.CalculatorViewModel r0 = (net.safemoon.androidwallet.viewmodels.CalculatorViewModel) r0
                java.lang.Object r1 = r8.L$0
                net.safemoon.androidwallet.viewmodels.CalculatorViewModel r1 = (net.safemoon.androidwallet.viewmodels.CalculatorViewModel) r1
                defpackage.o83.b(r9)     // Catch: java.lang.Exception -> L69
                goto L5b
            L19:
                java.lang.IllegalStateException r9 = new java.lang.IllegalStateException
                java.lang.String r0 = "call to 'resume' before 'invoke' with coroutine"
                r9.<init>(r0)
                throw r9
            L21:
                defpackage.o83.b(r9)
                net.safemoon.androidwallet.viewmodels.CalculatorViewModel r9 = r8.this$0
                android.app.Application r1 = r9.a()     // Catch: java.lang.Exception -> L68
                java.lang.String r4 = "getApplication<Application>()"
                defpackage.fs1.e(r1, r4)     // Catch: java.lang.Exception -> L68
                net.safemoon.androidwallet.model.wallets.Wallet r1 = defpackage.e30.c(r1)     // Catch: java.lang.Exception -> L68
                if (r1 != 0) goto L38
                r0 = 0
                r1 = r9
                goto L60
            L38:
                q9 r4 = r8.$model     // Catch: java.lang.Exception -> L68
                net.safemoon.androidwallet.viewmodels.blockChainClass.WalletWeb3 r5 = new net.safemoon.androidwallet.viewmodels.blockChainClass.WalletWeb3     // Catch: java.lang.Exception -> L68
                int r6 = r4.b()     // Catch: java.lang.Exception -> L68
                r5.<init>(r6, r1)     // Catch: java.lang.Exception -> L68
                java.lang.String r1 = r4.a()     // Catch: java.lang.Exception -> L68
                int r4 = r4.c()     // Catch: java.lang.Exception -> L68
                r8.L$0 = r9     // Catch: java.lang.Exception -> L68
                r8.L$1 = r9     // Catch: java.lang.Exception -> L68
                r8.label = r3     // Catch: java.lang.Exception -> L68
                java.lang.Object r1 = r5.h(r1, r4, r8)     // Catch: java.lang.Exception -> L68
                if (r1 != r0) goto L58
                return r0
            L58:
                r0 = r9
                r9 = r1
                r1 = r0
            L5b:
                java.math.BigDecimal r9 = (java.math.BigDecimal) r9     // Catch: java.lang.Exception -> L69
                r7 = r0
                r0 = r9
                r9 = r7
            L60:
                if (r0 != 0) goto L64
                java.math.BigDecimal r0 = java.math.BigDecimal.ZERO     // Catch: java.lang.Exception -> L69
            L64:
                defpackage.fs1.e(r0, r2)     // Catch: java.lang.Exception -> L69
                goto L6f
            L68:
                r1 = r9
            L69:
                java.math.BigDecimal r0 = java.math.BigDecimal.ZERO
                defpackage.fs1.e(r0, r2)
                r9 = r1
            L6f:
                r9.z(r0)
                te4 r9 = defpackage.te4.a
                return r9
            */
            throw new UnsupportedOperationException("Method not decompiled: net.safemoon.androidwallet.viewmodels.CalculatorViewModel$fetchTokenBalance$1$1.AnonymousClass1.invokeSuspend(java.lang.Object):java.lang.Object");
        }
    }

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public CalculatorViewModel$fetchTokenBalance$1$1(CalculatorViewModel calculatorViewModel, q9 q9Var, q70<? super CalculatorViewModel$fetchTokenBalance$1$1> q70Var) {
        super(2, q70Var);
        this.this$0 = calculatorViewModel;
        this.$model = q9Var;
    }

    @Override // kotlin.coroutines.jvm.internal.BaseContinuationImpl
    public final q70<te4> create(Object obj, q70<?> q70Var) {
        return new CalculatorViewModel$fetchTokenBalance$1$1(this.this$0, this.$model, q70Var);
    }

    @Override // defpackage.hd1
    public final Object invoke(c90 c90Var, q70<? super te4> q70Var) {
        return ((CalculatorViewModel$fetchTokenBalance$1$1) create(c90Var, q70Var)).invokeSuspend(te4.a);
    }

    @Override // kotlin.coroutines.jvm.internal.BaseContinuationImpl
    public final Object invokeSuspend(Object obj) {
        Object d = gs1.d();
        int i = this.label;
        if (i == 0) {
            o83.b(obj);
            CoroutineDispatcher b = tp0.b();
            AnonymousClass1 anonymousClass1 = new AnonymousClass1(this.this$0, this.$model, null);
            this.label = 1;
            if (kotlinx.coroutines.a.e(b, anonymousClass1, this) == d) {
                return d;
            }
        } else if (i != 1) {
            throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        } else {
            o83.b(obj);
        }
        return te4.a;
    }
}
