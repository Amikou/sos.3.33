package net.safemoon.androidwallet.viewmodels;

import kotlin.coroutines.jvm.internal.SuspendLambda;
import kotlin.coroutines.jvm.internal.a;
import kotlin.jvm.internal.Lambda;
import net.safemoon.androidwallet.model.fiat.gson.Fiat;

/* compiled from: SelectFiatViewModel.kt */
/* loaded from: classes2.dex */
public final class SelectFiatViewModel$setDefault$1 extends Lambda implements tc1<Boolean, te4> {
    public final /* synthetic */ rc1<te4> $callBack;
    public final /* synthetic */ Fiat $item;
    public final /* synthetic */ SelectFiatViewModel this$0;

    /* compiled from: SelectFiatViewModel.kt */
    @a(c = "net.safemoon.androidwallet.viewmodels.SelectFiatViewModel$setDefault$1$1", f = "SelectFiatViewModel.kt", l = {}, m = "invokeSuspend")
    /* renamed from: net.safemoon.androidwallet.viewmodels.SelectFiatViewModel$setDefault$1$1  reason: invalid class name */
    /* loaded from: classes2.dex */
    public static final class AnonymousClass1 extends SuspendLambda implements hd1<c90, q70<? super te4>, Object> {
        public final /* synthetic */ rc1<te4> $callBack;
        public int label;
        public final /* synthetic */ SelectFiatViewModel this$0;

        /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
        public AnonymousClass1(SelectFiatViewModel selectFiatViewModel, rc1<te4> rc1Var, q70<? super AnonymousClass1> q70Var) {
            super(2, q70Var);
            this.this$0 = selectFiatViewModel;
            this.$callBack = rc1Var;
        }

        @Override // kotlin.coroutines.jvm.internal.BaseContinuationImpl
        public final q70<te4> create(Object obj, q70<?> q70Var) {
            return new AnonymousClass1(this.this$0, this.$callBack, q70Var);
        }

        @Override // defpackage.hd1
        public final Object invoke(c90 c90Var, q70<? super te4> q70Var) {
            return ((AnonymousClass1) create(c90Var, q70Var)).invokeSuspend(te4.a);
        }

        @Override // kotlin.coroutines.jvm.internal.BaseContinuationImpl
        public final Object invokeSuspend(Object obj) {
            gs1.d();
            if (this.label == 0) {
                o83.b(obj);
                this.this$0.l();
                rc1<te4> rc1Var = this.$callBack;
                if (rc1Var != null) {
                    rc1Var.invoke();
                }
                return te4.a;
            }
            throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        }
    }

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public SelectFiatViewModel$setDefault$1(SelectFiatViewModel selectFiatViewModel, Fiat fiat, rc1<te4> rc1Var) {
        super(1);
        this.this$0 = selectFiatViewModel;
        this.$item = fiat;
        this.$callBack = rc1Var;
    }

    @Override // defpackage.tc1
    public /* bridge */ /* synthetic */ te4 invoke(Boolean bool) {
        invoke(bool.booleanValue());
        return te4.a;
    }

    public final void invoke(boolean z) {
        if (z) {
            bo3.b(this.this$0.a(), "DEFAULT_FIAT", this.$item.toString());
            as.b(ej4.a(this.this$0), null, null, new AnonymousClass1(this.this$0, this.$callBack, null), 3, null);
        }
    }
}
