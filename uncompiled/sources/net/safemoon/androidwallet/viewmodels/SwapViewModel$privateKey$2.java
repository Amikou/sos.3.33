package net.safemoon.androidwallet.viewmodels;

import android.app.Application;
import kotlin.jvm.internal.Lambda;

/* compiled from: SwapViewModel.kt */
/* loaded from: classes2.dex */
public final class SwapViewModel$privateKey$2 extends Lambda implements rc1<String> {
    public final /* synthetic */ SwapViewModel this$0;

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public SwapViewModel$privateKey$2(SwapViewModel swapViewModel) {
        super(0);
        this.this$0 = swapViewModel;
    }

    @Override // defpackage.rc1
    public final String invoke() {
        Application application;
        application = this.this$0.b;
        return w.c(application);
    }
}
