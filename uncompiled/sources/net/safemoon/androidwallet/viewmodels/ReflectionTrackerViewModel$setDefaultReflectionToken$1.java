package net.safemoon.androidwallet.viewmodels;

import kotlin.coroutines.jvm.internal.SuspendLambda;
import kotlin.coroutines.jvm.internal.a;

/* compiled from: ReflectionTrackerViewModel.kt */
@a(c = "net.safemoon.androidwallet.viewmodels.ReflectionTrackerViewModel$setDefaultReflectionToken$1", f = "ReflectionTrackerViewModel.kt", l = {244, 252, 254}, m = "invokeSuspend")
/* loaded from: classes2.dex */
public final class ReflectionTrackerViewModel$setDefaultReflectionToken$1 extends SuspendLambda implements hd1<c90, q70<? super te4>, Object> {
    public Object L$0;
    public Object L$1;
    public int label;
    public final /* synthetic */ ReflectionTrackerViewModel this$0;

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public ReflectionTrackerViewModel$setDefaultReflectionToken$1(ReflectionTrackerViewModel reflectionTrackerViewModel, q70<? super ReflectionTrackerViewModel$setDefaultReflectionToken$1> q70Var) {
        super(2, q70Var);
        this.this$0 = reflectionTrackerViewModel;
    }

    @Override // kotlin.coroutines.jvm.internal.BaseContinuationImpl
    public final q70<te4> create(Object obj, q70<?> q70Var) {
        return new ReflectionTrackerViewModel$setDefaultReflectionToken$1(this.this$0, q70Var);
    }

    @Override // defpackage.hd1
    public final Object invoke(c90 c90Var, q70<? super te4> q70Var) {
        return ((ReflectionTrackerViewModel$setDefaultReflectionToken$1) create(c90Var, q70Var)).invokeSuspend(te4.a);
    }

    /* JADX WARN: Removed duplicated region for block: B:41:0x0105  */
    /* JADX WARN: Removed duplicated region for block: B:46:0x0121  */
    /* JADX WARN: Removed duplicated region for block: B:47:0x0123  */
    /* JADX WARN: Removed duplicated region for block: B:52:0x0138  */
    /* JADX WARN: Unsupported multi-entry loop pattern (BACK_EDGE: B:46:0x0121 -> B:39:0x00ff). Please submit an issue!!! */
    /* JADX WARN: Unsupported multi-entry loop pattern (BACK_EDGE: B:50:0x0135 -> B:51:0x0136). Please submit an issue!!! */
    @Override // kotlin.coroutines.jvm.internal.BaseContinuationImpl
    /*
        Code decompiled incorrectly, please refer to instructions dump.
        To view partially-correct code enable 'Show inconsistent code' option in preferences
    */
    public final java.lang.Object invokeSuspend(java.lang.Object r17) {
        /*
            Method dump skipped, instructions count: 315
            To view this dump change 'Code comments level' option to 'DEBUG'
        */
        throw new UnsupportedOperationException("Method not decompiled: net.safemoon.androidwallet.viewmodels.ReflectionTrackerViewModel$setDefaultReflectionToken$1.invokeSuspend(java.lang.Object):java.lang.Object");
    }
}
