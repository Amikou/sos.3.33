package net.safemoon.androidwallet.viewmodels;

import android.graphics.Bitmap;
import kotlin.coroutines.jvm.internal.SuspendLambda;
import kotlin.coroutines.jvm.internal.a;

/* compiled from: GoogleAuthViewModel.kt */
@a(c = "net.safemoon.androidwallet.viewmodels.GoogleAuthViewModel$generateAuthKey$1$1$3$1", f = "GoogleAuthViewModel.kt", l = {}, m = "invokeSuspend")
/* loaded from: classes2.dex */
public final class GoogleAuthViewModel$generateAuthKey$1$1$3$1 extends SuspendLambda implements hd1<c90, q70<? super te4>, Object> {
    public final /* synthetic */ Bitmap $it;
    public final /* synthetic */ e34 $totpData;
    public int label;
    public final /* synthetic */ GoogleAuthViewModel this$0;

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public GoogleAuthViewModel$generateAuthKey$1$1$3$1(GoogleAuthViewModel googleAuthViewModel, e34 e34Var, Bitmap bitmap, q70<? super GoogleAuthViewModel$generateAuthKey$1$1$3$1> q70Var) {
        super(2, q70Var);
        this.this$0 = googleAuthViewModel;
        this.$totpData = e34Var;
        this.$it = bitmap;
    }

    @Override // kotlin.coroutines.jvm.internal.BaseContinuationImpl
    public final q70<te4> create(Object obj, q70<?> q70Var) {
        return new GoogleAuthViewModel$generateAuthKey$1$1$3$1(this.this$0, this.$totpData, this.$it, q70Var);
    }

    @Override // defpackage.hd1
    public final Object invoke(c90 c90Var, q70<? super te4> q70Var) {
        return ((GoogleAuthViewModel$generateAuthKey$1$1$3$1) create(c90Var, q70Var)).invokeSuspend(te4.a);
    }

    @Override // kotlin.coroutines.jvm.internal.BaseContinuationImpl
    public final Object invokeSuspend(Object obj) {
        gs1.d();
        if (this.label == 0) {
            o83.b(obj);
            this.this$0.g().setValue(this.$totpData.b());
            this.this$0.f().setValue(this.$it);
            return te4.a;
        }
        throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
    }
}
