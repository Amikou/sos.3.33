package net.safemoon.androidwallet.viewmodels;

import android.app.Application;
import com.AKT.anonymouskey.ui.login.AKTServerFunctions;

/* compiled from: AKTWebSocketHandlingViewModel.kt */
/* loaded from: classes2.dex */
public final class AKTWebSocketHandlingViewModel extends gd {
    public tc1<? super bm3, te4> b;
    public final AKTWebSocketHandlingViewModel$myWebInterface$1 c;
    public final sy1 d;
    public final gb2<String> e;

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    /* JADX WARN: Type inference failed for: r2v1, types: [net.safemoon.androidwallet.viewmodels.AKTWebSocketHandlingViewModel$myWebInterface$1] */
    public AKTWebSocketHandlingViewModel(Application application) {
        super(application);
        fs1.f(application, "application");
        this.c = new mc2() { // from class: net.safemoon.androidwallet.viewmodels.AKTWebSocketHandlingViewModel$myWebInterface$1
            @Override // defpackage.mc2
            public void b(Exception exc) {
                super.b(exc);
                as.b(ej4.a(AKTWebSocketHandlingViewModel.this), null, null, new AKTWebSocketHandlingViewModel$myWebInterface$1$onError$1(AKTWebSocketHandlingViewModel.this, null), 3, null);
            }

            @Override // defpackage.mc2
            public void c(String str) {
                as.b(ej4.a(AKTWebSocketHandlingViewModel.this), null, null, new AKTWebSocketHandlingViewModel$myWebInterface$1$onMessage$1(AKTWebSocketHandlingViewModel.this, str, null), 3, null);
            }

            @Override // defpackage.mc2
            public void d(bm3 bm3Var) {
                tc1 tc1Var;
                super.d(bm3Var);
                tc1Var = AKTWebSocketHandlingViewModel.this.b;
                if (tc1Var == null) {
                    return;
                }
                tc1Var.invoke(bm3Var);
            }
        };
        this.d = zy1.a(new AKTWebSocketHandlingViewModel$webSocket$2(this));
        this.e = new gb2<>();
    }

    public final void f() {
        i().a();
    }

    public final void g(String str) {
        this.e.postValue(str);
    }

    public final gb2<String> h() {
        return this.e;
    }

    public final no4 i() {
        return (no4) this.d.getValue();
    }

    public final void j() {
        i().d();
    }

    public final void k(String str) {
        fs1.f(str, "request");
        as.b(ej4.a(this), null, null, new AKTWebSocketHandlingViewModel$sendMessage$1(i().b(), AKTServerFunctions.e0(a(), str), this, null), 3, null);
    }

    public final void l() {
    }
}
