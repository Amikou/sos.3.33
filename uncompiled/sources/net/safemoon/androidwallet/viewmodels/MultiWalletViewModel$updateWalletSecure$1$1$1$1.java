package net.safemoon.androidwallet.viewmodels;

import kotlin.coroutines.jvm.internal.SuspendLambda;
import kotlin.coroutines.jvm.internal.a;

/* compiled from: MultiWalletViewModel.kt */
@a(c = "net.safemoon.androidwallet.viewmodels.MultiWalletViewModel$updateWalletSecure$1$1$1$1", f = "MultiWalletViewModel.kt", l = {}, m = "invokeSuspend")
/* loaded from: classes2.dex */
public final class MultiWalletViewModel$updateWalletSecure$1$1$1$1 extends SuspendLambda implements hd1<c90, q70<? super te4>, Object> {
    public final /* synthetic */ rc1<te4> $callBack;
    public int label;

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public MultiWalletViewModel$updateWalletSecure$1$1$1$1(rc1<te4> rc1Var, q70<? super MultiWalletViewModel$updateWalletSecure$1$1$1$1> q70Var) {
        super(2, q70Var);
        this.$callBack = rc1Var;
    }

    @Override // kotlin.coroutines.jvm.internal.BaseContinuationImpl
    public final q70<te4> create(Object obj, q70<?> q70Var) {
        return new MultiWalletViewModel$updateWalletSecure$1$1$1$1(this.$callBack, q70Var);
    }

    @Override // defpackage.hd1
    public final Object invoke(c90 c90Var, q70<? super te4> q70Var) {
        return ((MultiWalletViewModel$updateWalletSecure$1$1$1$1) create(c90Var, q70Var)).invokeSuspend(te4.a);
    }

    @Override // kotlin.coroutines.jvm.internal.BaseContinuationImpl
    public final Object invokeSuspend(Object obj) {
        gs1.d();
        if (this.label == 0) {
            o83.b(obj);
            rc1<te4> rc1Var = this.$callBack;
            if (rc1Var != null) {
                rc1Var.invoke();
            }
            return te4.a;
        }
        throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
    }
}
