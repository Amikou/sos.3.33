package net.safemoon.androidwallet.viewmodels;

import kotlin.jvm.internal.Lambda;
import net.safemoon.androidwallet.model.AllCryptoList;
import net.safemoon.androidwallet.model.Coin;
import net.safemoon.androidwallet.repository.LatestCoinDataSource;

/* compiled from: HomeViewModel.kt */
/* loaded from: classes2.dex */
public final class HomeViewModel$coinListFlow$1 extends Lambda implements rc1<gp2<Integer, Coin>> {
    public final /* synthetic */ HomeViewModel this$0;

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public HomeViewModel$coinListFlow$1(HomeViewModel homeViewModel) {
        super(0);
        this.this$0 = homeViewModel;
    }

    /* JADX WARN: Can't rename method to resolve collision */
    @Override // defpackage.rc1
    public final gp2<Integer, Coin> invoke() {
        e42 k = a4.k();
        fs1.e(k, "getMarketClient()");
        final HomeViewModel homeViewModel = this.this$0;
        return new LatestCoinDataSource(k, new LatestCoinDataSource.a() { // from class: net.safemoon.androidwallet.viewmodels.HomeViewModel$coinListFlow$1.1
            @Override // net.safemoon.androidwallet.repository.LatestCoinDataSource.a
            public void a(AllCryptoList allCryptoList) {
                fs1.f(allCryptoList, "newData");
                as.b(ej4.a(HomeViewModel.this), tp0.c(), null, new HomeViewModel$coinListFlow$1$1$onLoaded$1(HomeViewModel.this, allCryptoList, null), 2, null);
            }
        });
    }
}
