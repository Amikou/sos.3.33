package net.safemoon.androidwallet.viewmodels;

import kotlin.coroutines.jvm.internal.SuspendLambda;
import kotlin.coroutines.jvm.internal.a;
import kotlinx.coroutines.CoroutineDispatcher;
import net.safemoon.androidwallet.model.tokensInfo.CurrencyTokenInfo;
import net.safemoon.androidwallet.model.tokensInfo.CurrencyTokenInfoResult;
import retrofit2.KotlinExtensions;
import retrofit2.b;
import retrofit2.n;

/* compiled from: TransactionHistoryViewModel.kt */
@a(c = "net.safemoon.androidwallet.viewmodels.TransactionHistoryViewModel$getCurrencyTokenInfo$1", f = "TransactionHistoryViewModel.kt", l = {82}, m = "invokeSuspend")
/* loaded from: classes2.dex */
public final class TransactionHistoryViewModel$getCurrencyTokenInfo$1 extends SuspendLambda implements hd1<c90, q70<? super te4>, Object> {
    public final /* synthetic */ String $tokenAddress;
    public int label;
    public final /* synthetic */ TransactionHistoryViewModel this$0;

    /* compiled from: TransactionHistoryViewModel.kt */
    @a(c = "net.safemoon.androidwallet.viewmodels.TransactionHistoryViewModel$getCurrencyTokenInfo$1$1", f = "TransactionHistoryViewModel.kt", l = {85}, m = "invokeSuspend")
    /* renamed from: net.safemoon.androidwallet.viewmodels.TransactionHistoryViewModel$getCurrencyTokenInfo$1$1  reason: invalid class name */
    /* loaded from: classes2.dex */
    public static final class AnonymousClass1 extends SuspendLambda implements hd1<c90, q70<? super te4>, Object> {
        public final /* synthetic */ String $tokenAddress;
        public int label;
        public final /* synthetic */ TransactionHistoryViewModel this$0;

        /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
        public AnonymousClass1(String str, TransactionHistoryViewModel transactionHistoryViewModel, q70<? super AnonymousClass1> q70Var) {
            super(2, q70Var);
            this.$tokenAddress = str;
            this.this$0 = transactionHistoryViewModel;
        }

        @Override // kotlin.coroutines.jvm.internal.BaseContinuationImpl
        public final q70<te4> create(Object obj, q70<?> q70Var) {
            return new AnonymousClass1(this.$tokenAddress, this.this$0, q70Var);
        }

        @Override // defpackage.hd1
        public final Object invoke(c90 c90Var, q70<? super te4> q70Var) {
            return ((AnonymousClass1) create(c90Var, q70Var)).invokeSuspend(te4.a);
        }

        @Override // kotlin.coroutines.jvm.internal.BaseContinuationImpl
        public final Object invokeSuspend(Object obj) {
            CurrencyTokenInfo data;
            Object d = gs1.d();
            int i = this.label;
            try {
                if (i == 0) {
                    o83.b(obj);
                    e42 k = a4.k();
                    String str = this.$tokenAddress;
                    b<CurrencyTokenInfoResult> p = k.p(str, b30.a.m(str));
                    this.label = 1;
                    obj = KotlinExtensions.c(p, this);
                    if (obj == d) {
                        return d;
                    }
                } else if (i != 1) {
                    throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
                } else {
                    o83.b(obj);
                }
                CurrencyTokenInfoResult currencyTokenInfoResult = (CurrencyTokenInfoResult) ((n) obj).a();
                if (currencyTokenInfoResult != null && (data = currencyTokenInfoResult.getData()) != null) {
                    this.this$0.i().postValue(data);
                }
            } catch (Exception unused) {
            }
            return te4.a;
        }
    }

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public TransactionHistoryViewModel$getCurrencyTokenInfo$1(String str, TransactionHistoryViewModel transactionHistoryViewModel, q70<? super TransactionHistoryViewModel$getCurrencyTokenInfo$1> q70Var) {
        super(2, q70Var);
        this.$tokenAddress = str;
        this.this$0 = transactionHistoryViewModel;
    }

    @Override // kotlin.coroutines.jvm.internal.BaseContinuationImpl
    public final q70<te4> create(Object obj, q70<?> q70Var) {
        return new TransactionHistoryViewModel$getCurrencyTokenInfo$1(this.$tokenAddress, this.this$0, q70Var);
    }

    @Override // defpackage.hd1
    public final Object invoke(c90 c90Var, q70<? super te4> q70Var) {
        return ((TransactionHistoryViewModel$getCurrencyTokenInfo$1) create(c90Var, q70Var)).invokeSuspend(te4.a);
    }

    @Override // kotlin.coroutines.jvm.internal.BaseContinuationImpl
    public final Object invokeSuspend(Object obj) {
        Object d = gs1.d();
        int i = this.label;
        if (i == 0) {
            o83.b(obj);
            CoroutineDispatcher b = tp0.b();
            AnonymousClass1 anonymousClass1 = new AnonymousClass1(this.$tokenAddress, this.this$0, null);
            this.label = 1;
            if (kotlinx.coroutines.a.e(b, anonymousClass1, this) == d) {
                return d;
            }
        } else if (i != 1) {
            throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        } else {
            o83.b(obj);
        }
        return te4.a;
    }
}
