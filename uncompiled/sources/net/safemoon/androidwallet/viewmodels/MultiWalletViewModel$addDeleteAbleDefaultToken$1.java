package net.safemoon.androidwallet.viewmodels;

import kotlin.coroutines.jvm.internal.ContinuationImpl;
import kotlin.coroutines.jvm.internal.a;

/* compiled from: MultiWalletViewModel.kt */
@a(c = "net.safemoon.androidwallet.viewmodels.MultiWalletViewModel", f = "MultiWalletViewModel.kt", l = {288}, m = "addDeleteAbleDefaultToken")
/* loaded from: classes2.dex */
public final class MultiWalletViewModel$addDeleteAbleDefaultToken$1 extends ContinuationImpl {
    public Object L$0;
    public Object L$1;
    public boolean Z$0;
    public int label;
    public /* synthetic */ Object result;
    public final /* synthetic */ MultiWalletViewModel this$0;

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public MultiWalletViewModel$addDeleteAbleDefaultToken$1(MultiWalletViewModel multiWalletViewModel, q70<? super MultiWalletViewModel$addDeleteAbleDefaultToken$1> q70Var) {
        super(q70Var);
        this.this$0 = multiWalletViewModel;
    }

    @Override // kotlin.coroutines.jvm.internal.BaseContinuationImpl
    public final Object invokeSuspend(Object obj) {
        Object j;
        this.result = obj;
        this.label |= Integer.MIN_VALUE;
        j = this.this$0.j(false, this);
        return j;
    }
}
