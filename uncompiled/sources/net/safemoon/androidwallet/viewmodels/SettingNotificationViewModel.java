package net.safemoon.androidwallet.viewmodels;

import android.app.Application;
import androidx.lifecycle.LiveData;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.Locale;
import java.util.Objects;
import net.safemoon.androidwallet.R;
import net.safemoon.androidwallet.common.TokenType;
import net.safemoon.androidwallet.model.Coin;
import net.safemoon.androidwallet.model.RoomCoinPriceAlert;
import net.safemoon.androidwallet.model.priceAlert.PAToken;
import net.safemoon.androidwallet.model.priceAlert.PriceAlertToken;
import net.safemoon.androidwallet.model.wallets.Wallet;
import net.safemoon.androidwallet.repository.FcmDataSource;
import net.safemoon.androidwallet.repository.TokenListDataSource;
import net.safemoon.androidwallet.viewmodels.SettingNotificationViewModel;

/* compiled from: SettingNotificationViewModel.kt */
/* loaded from: classes2.dex */
public final class SettingNotificationViewModel extends gd {
    public final sy1 b;
    public final sy1 c;
    public final sy1 d;
    public final sy1 e;
    public final sy1 f;
    public Integer g;
    public final sy1 h;
    public final gb2<Boolean> i;
    public final gb2<List<PAToken>> j;
    public final gb2<List<PriceAlertToken>> k;
    public final gb2<List<PAToken>> l;
    public final gb2<String> m;
    public String n;
    public final LiveData<List<RoomCoinPriceAlert>> o;
    public final LiveData<List<PAToken>> p;
    public final sy1 q;
    public final List<TokenType> r;

    /* compiled from: Comparisons.kt */
    /* loaded from: classes2.dex */
    public static final class a<T> implements Comparator {
        @Override // java.util.Comparator
        public final int compare(T t, T t2) {
            String name = ((PAToken) t).getName();
            Objects.requireNonNull(name, "null cannot be cast to non-null type java.lang.String");
            Locale locale = Locale.ROOT;
            String lowerCase = name.toLowerCase(locale);
            fs1.e(lowerCase, "(this as java.lang.Strin….toLowerCase(Locale.ROOT)");
            String name2 = ((PAToken) t2).getName();
            Objects.requireNonNull(name2, "null cannot be cast to non-null type java.lang.String");
            String lowerCase2 = name2.toLowerCase(locale);
            fs1.e(lowerCase2, "(this as java.lang.Strin….toLowerCase(Locale.ROOT)");
            return l30.a(lowerCase, lowerCase2);
        }
    }

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public SettingNotificationViewModel(Application application) {
        super(application);
        fs1.f(application, "application");
        this.b = zy1.a(new SettingNotificationViewModel$activeWallet$2(application));
        this.c = zy1.a(new SettingNotificationViewModel$fcmDataSource$2(application));
        this.d = zy1.a(new SettingNotificationViewModel$coinPriceAlertDataSource$2(this));
        this.e = zy1.a(new SettingNotificationViewModel$tokenListDataSource$2(application));
        this.f = zy1.a(SettingNotificationViewModel$mainNetApiInterface$2.INSTANCE);
        this.h = zy1.a(new SettingNotificationViewModel$context$2(this));
        this.i = new gb2<>(Boolean.valueOf(bo3.e(a(), "FCM_TOKEN", true)));
        this.j = new gb2<>(b20.g());
        gb2<List<PriceAlertToken>> gb2Var = new gb2<>(b20.g());
        this.k = gb2Var;
        this.l = new gb2<>(b20.g());
        this.m = new gb2<>("");
        this.n = "";
        this.o = t().c();
        final g72 g72Var = new g72();
        g72Var.a(z(), new tl2() { // from class: bn3
            @Override // defpackage.tl2
            public final void onChanged(Object obj) {
                SettingNotificationViewModel.J(SettingNotificationViewModel.this, g72Var, (List) obj);
            }
        });
        g72Var.a(gb2Var, new tl2() { // from class: an3
            @Override // defpackage.tl2
            public final void onChanged(Object obj) {
                SettingNotificationViewModel.K(SettingNotificationViewModel.this, g72Var, (List) obj);
            }
        });
        g72Var.a(C(), new tl2() { // from class: zm3
            @Override // defpackage.tl2
            public final void onChanged(Object obj) {
                SettingNotificationViewModel.H(SettingNotificationViewModel.this, g72Var, (String) obj);
            }
        });
        g72Var.a(r(), new tl2() { // from class: ym3
            @Override // defpackage.tl2
            public final void onChanged(Object obj) {
                SettingNotificationViewModel.I(SettingNotificationViewModel.this, (List) obj);
            }
        });
        L(this, g72Var);
        te4 te4Var = te4.a;
        this.p = g72Var;
        this.q = zy1.a(SettingNotificationViewModel$safeMoonClient$2.INSTANCE);
        this.r = b30.a.a();
    }

    public static final void H(SettingNotificationViewModel settingNotificationViewModel, g72 g72Var, String str) {
        fs1.f(settingNotificationViewModel, "this$0");
        fs1.f(g72Var, "$this_apply");
        L(settingNotificationViewModel, g72Var);
    }

    public static final void I(SettingNotificationViewModel settingNotificationViewModel, List list) {
        fs1.f(settingNotificationViewModel, "this$0");
        Integer w = settingNotificationViewModel.w();
        if (w != null && w.intValue() == -1) {
            settingNotificationViewModel.p(-1);
        }
    }

    public static final void J(SettingNotificationViewModel settingNotificationViewModel, g72 g72Var, List list) {
        fs1.f(settingNotificationViewModel, "this$0");
        fs1.f(g72Var, "$this_apply");
        L(settingNotificationViewModel, g72Var);
    }

    public static final void K(SettingNotificationViewModel settingNotificationViewModel, g72 g72Var, List list) {
        fs1.f(settingNotificationViewModel, "this$0");
        fs1.f(g72Var, "$this_apply");
        L(settingNotificationViewModel, g72Var);
    }

    /* JADX WARN: Code restructure failed: missing block: B:27:0x007c, code lost:
        if ((r14.length() == 0) != false) goto L18;
     */
    /* JADX WARN: Code restructure failed: missing block: B:39:0x009c, code lost:
        if ((r7.length() == 0) != false) goto L21;
     */
    /* JADX WARN: Removed duplicated region for block: B:105:0x00d2 A[SYNTHETIC] */
    /* JADX WARN: Removed duplicated region for block: B:108:0x0052 A[SYNTHETIC] */
    /* JADX WARN: Removed duplicated region for block: B:117:0x0184 A[SYNTHETIC] */
    /* JADX WARN: Removed duplicated region for block: B:120:0x0137 A[SYNTHETIC] */
    /* JADX WARN: Removed duplicated region for block: B:93:0x0166  */
    /*
        Code decompiled incorrectly, please refer to instructions dump.
        To view partially-correct code enable 'Show inconsistent code' option in preferences
    */
    public static final void L(net.safemoon.androidwallet.viewmodels.SettingNotificationViewModel r16, defpackage.g72<java.util.List<net.safemoon.androidwallet.model.priceAlert.PAToken>> r17) {
        /*
            Method dump skipped, instructions count: 407
            To view this dump change 'Code comments level' option to 'DEBUG'
        */
        throw new UnsupportedOperationException("Method not decompiled: net.safemoon.androidwallet.viewmodels.SettingNotificationViewModel.L(net.safemoon.androidwallet.viewmodels.SettingNotificationViewModel, g72):void");
    }

    public final LiveData<List<PAToken>> A() {
        return this.p;
    }

    public final ac3 B() {
        return (ac3) this.q.getValue();
    }

    public final gb2<String> C() {
        return this.m;
    }

    public final String D() {
        return this.n;
    }

    public final TokenListDataSource E() {
        return (TokenListDataSource) this.e.getValue();
    }

    public final gb2<Boolean> F() {
        return this.i;
    }

    public final void G() {
        this.g = null;
        N();
    }

    public final void M() {
        FcmDataSource.h(x(), null, null, true, new SettingNotificationViewModel$registerDevice$1(this), 3, null);
    }

    public final void N() {
        ArrayList arrayList = new ArrayList();
        for (TokenType tokenType : this.r) {
            arrayList.add(new PAToken(tokenType.getChainId(), tokenType.getTitle(), tokenType.getNativeToken(), null, null, null, false, 88, null));
        }
        String string = u().getString(R.string.cpa_cmc_token_type_title);
        fs1.e(string, "context.getString(R.stri…cpa_cmc_token_type_title)");
        arrayList.add(new PAToken(-1, string, null, null, null, null, false, 120, null));
        this.j.postValue(arrayList);
    }

    public final void O(Integer num) {
        this.g = num;
    }

    public final void P(String str) {
        this.n = str;
    }

    public final void Q() {
        FcmDataSource.h(x(), null, null, false, new SettingNotificationViewModel$unRegisterDevice$1(this), 3, null);
    }

    public final void R() {
        as.b(ej4.a(this), tp0.b(), null, new SettingNotificationViewModel$updateAllPriceAlert$1(this, null), 2, null);
    }

    public final void n(Coin coin) {
        fs1.f(coin, "coin");
        as.b(ej4.a(this), tp0.b(), null, new SettingNotificationViewModel$addPriceAlertCoin$1(this, coin, null), 2, null);
    }

    public final void o(PAToken pAToken) {
        fs1.f(pAToken, "paToken");
        as.b(ej4.a(this), tp0.b(), null, new SettingNotificationViewModel$deletePriceAlertCoin$1(this, pAToken, null), 2, null);
    }

    public final void p(int i) {
        as.b(ej4.a(this), tp0.b(), null, new SettingNotificationViewModel$fetchTokenList$1(this, null), 2, null);
        as.b(ej4.a(this), tp0.b(), null, new SettingNotificationViewModel$fetchTokenList$2(this, i, null), 2, null);
    }

    public final Wallet q() {
        return (Wallet) this.b.getValue();
    }

    public final LiveData<List<RoomCoinPriceAlert>> r() {
        return this.o;
    }

    /* JADX WARN: Can't wrap try/catch for region: R(12:1|(2:3|(10:5|6|7|(1:(1:(5:11|12|(3:18|(1:20)|(1:22))|24|25)(2:27|28))(2:29|30))(2:34|(2:36|37)(2:38|(1:40)(1:41)))|31|(1:33)|12|(5:14|16|18|(0)|(0))|24|25))|43|6|7|(0)(0)|31|(0)|12|(0)|24|25) */
    /* JADX WARN: Code restructure failed: missing block: B:32:0x008d, code lost:
        r9 = null;
     */
    /* JADX WARN: Removed duplicated region for block: B:10:0x0025  */
    /* JADX WARN: Removed duplicated region for block: B:19:0x0041  */
    /* JADX WARN: Removed duplicated region for block: B:29:0x0089 A[RETURN] */
    /* JADX WARN: Removed duplicated region for block: B:34:0x0090  */
    /* JADX WARN: Removed duplicated region for block: B:41:0x00a5  */
    /* JADX WARN: Removed duplicated region for block: B:47:? A[RETURN, SYNTHETIC] */
    /*
        Code decompiled incorrectly, please refer to instructions dump.
        To view partially-correct code enable 'Show inconsistent code' option in preferences
    */
    public final java.lang.Object s(defpackage.q70<? super java.util.List<net.safemoon.androidwallet.model.priceAlert.PriceAlertToken>> r9) {
        /*
            r8 = this;
            boolean r0 = r9 instanceof net.safemoon.androidwallet.viewmodels.SettingNotificationViewModel$getAllPriceAlert$1
            if (r0 == 0) goto L13
            r0 = r9
            net.safemoon.androidwallet.viewmodels.SettingNotificationViewModel$getAllPriceAlert$1 r0 = (net.safemoon.androidwallet.viewmodels.SettingNotificationViewModel$getAllPriceAlert$1) r0
            int r1 = r0.label
            r2 = -2147483648(0xffffffff80000000, float:-0.0)
            r3 = r1 & r2
            if (r3 == 0) goto L13
            int r1 = r1 - r2
            r0.label = r1
            goto L18
        L13:
            net.safemoon.androidwallet.viewmodels.SettingNotificationViewModel$getAllPriceAlert$1 r0 = new net.safemoon.androidwallet.viewmodels.SettingNotificationViewModel$getAllPriceAlert$1
            r0.<init>(r8, r9)
        L18:
            java.lang.Object r9 = r0.result
            java.lang.Object r1 = defpackage.gs1.d()
            int r2 = r0.label
            r3 = 2
            r4 = 1
            r5 = 0
            if (r2 == 0) goto L41
            if (r2 == r4) goto L35
            if (r2 != r3) goto L2d
            defpackage.o83.b(r9)     // Catch: java.lang.Exception -> L8d
            goto L8a
        L2d:
            java.lang.IllegalStateException r9 = new java.lang.IllegalStateException
            java.lang.String r0 = "call to 'resume' before 'invoke' with coroutine"
            r9.<init>(r0)
            throw r9
        L35:
            java.lang.Object r2 = r0.L$1
            java.lang.String r2 = (java.lang.String) r2
            java.lang.Object r4 = r0.L$0
            ac3 r4 = (defpackage.ac3) r4
            defpackage.o83.b(r9)     // Catch: java.lang.Exception -> L8d
            goto L72
        L41:
            defpackage.o83.b(r9)
            net.safemoon.androidwallet.model.wallets.Wallet r9 = r8.q()
            if (r9 != 0) goto L4f
            java.util.List r9 = defpackage.b20.g()
            return r9
        L4f:
            ac3 r9 = r8.B()     // Catch: java.lang.Exception -> L8d
            net.safemoon.androidwallet.model.wallets.Wallet r2 = r8.q()     // Catch: java.lang.Exception -> L8d
            defpackage.fs1.d(r2)     // Catch: java.lang.Exception -> L8d
            java.lang.String r2 = r2.getAddress()     // Catch: java.lang.Exception -> L8d
            net.safemoon.androidwallet.repository.FcmDataSource r6 = r8.x()     // Catch: java.lang.Exception -> L8d
            r0.L$0 = r9     // Catch: java.lang.Exception -> L8d
            r0.L$1 = r2     // Catch: java.lang.Exception -> L8d
            r0.label = r4     // Catch: java.lang.Exception -> L8d
            java.lang.Object r4 = r6.d(r0)     // Catch: java.lang.Exception -> L8d
            if (r4 != r1) goto L6f
            return r1
        L6f:
            r7 = r4
            r4 = r9
            r9 = r7
        L72:
            java.lang.String r9 = (java.lang.String) r9     // Catch: java.lang.Exception -> L8d
            retrofit2.b r9 = r4.f(r2, r5, r5, r9)     // Catch: java.lang.Exception -> L8d
            java.lang.String r2 = "safeMoonClient.getPriceA…DataSource.getFCMToken())"
            defpackage.fs1.e(r9, r2)     // Catch: java.lang.Exception -> L8d
            r0.L$0 = r5     // Catch: java.lang.Exception -> L8d
            r0.L$1 = r5     // Catch: java.lang.Exception -> L8d
            r0.label = r3     // Catch: java.lang.Exception -> L8d
            java.lang.Object r9 = retrofit2.KotlinExtensions.c(r9, r0)     // Catch: java.lang.Exception -> L8d
            if (r9 != r1) goto L8a
            return r1
        L8a:
            retrofit2.n r9 = (retrofit2.n) r9     // Catch: java.lang.Exception -> L8d
            goto L8e
        L8d:
            r9 = r5
        L8e:
            if (r9 == 0) goto Lab
            boolean r0 = r9.e()
            if (r0 == 0) goto Lab
            java.lang.Object r0 = r9.a()
            if (r0 == 0) goto Lab
            java.lang.Object r9 = r9.a()
            net.safemoon.androidwallet.model.priceAlert.PriceAlertTokenListData r9 = (net.safemoon.androidwallet.model.priceAlert.PriceAlertTokenListData) r9
            if (r9 != 0) goto La5
            goto La9
        La5:
            java.util.ArrayList r5 = r9.getResult()
        La9:
            if (r5 != 0) goto Laf
        Lab:
            java.util.List r5 = defpackage.b20.g()
        Laf:
            return r5
        */
        throw new UnsupportedOperationException("Method not decompiled: net.safemoon.androidwallet.viewmodels.SettingNotificationViewModel.s(q70):java.lang.Object");
    }

    public final m00 t() {
        return (m00) this.d.getValue();
    }

    public final Application u() {
        return (Application) this.h.getValue();
    }

    public final gb2<List<PAToken>> v() {
        return this.j;
    }

    public final Integer w() {
        return this.g;
    }

    public final FcmDataSource x() {
        return (FcmDataSource) this.c.getValue();
    }

    public final ac3 y() {
        return (ac3) this.f.getValue();
    }

    public final gb2<List<PAToken>> z() {
        return this.l;
    }
}
