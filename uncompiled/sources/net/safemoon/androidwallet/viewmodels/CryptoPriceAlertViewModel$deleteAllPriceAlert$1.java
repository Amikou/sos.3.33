package net.safemoon.androidwallet.viewmodels;

import kotlin.coroutines.jvm.internal.SuspendLambda;
import kotlin.coroutines.jvm.internal.a;
import net.safemoon.androidwallet.model.priceAlert.PriceAlertToken;

/* compiled from: CryptoPriceAlertViewModel.kt */
@a(c = "net.safemoon.androidwallet.viewmodels.CryptoPriceAlertViewModel$deleteAllPriceAlert$1", f = "CryptoPriceAlertViewModel.kt", l = {182, 185, 188}, m = "invokeSuspend")
/* loaded from: classes2.dex */
public final class CryptoPriceAlertViewModel$deleteAllPriceAlert$1 extends SuspendLambda implements hd1<c90, q70<? super te4>, Object> {
    public final /* synthetic */ PriceAlertToken $paToken;
    public Object L$0;
    public Object L$1;
    public Object L$2;
    public int label;
    public final /* synthetic */ CryptoPriceAlertViewModel this$0;

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public CryptoPriceAlertViewModel$deleteAllPriceAlert$1(CryptoPriceAlertViewModel cryptoPriceAlertViewModel, PriceAlertToken priceAlertToken, q70<? super CryptoPriceAlertViewModel$deleteAllPriceAlert$1> q70Var) {
        super(2, q70Var);
        this.this$0 = cryptoPriceAlertViewModel;
        this.$paToken = priceAlertToken;
    }

    @Override // kotlin.coroutines.jvm.internal.BaseContinuationImpl
    public final q70<te4> create(Object obj, q70<?> q70Var) {
        return new CryptoPriceAlertViewModel$deleteAllPriceAlert$1(this.this$0, this.$paToken, q70Var);
    }

    @Override // defpackage.hd1
    public final Object invoke(c90 c90Var, q70<? super te4> q70Var) {
        return ((CryptoPriceAlertViewModel$deleteAllPriceAlert$1) create(c90Var, q70Var)).invokeSuspend(te4.a);
    }

    /* JADX WARN: Removed duplicated region for block: B:24:0x00d8 A[Catch: Exception -> 0x013c, TryCatch #0 {Exception -> 0x013c, blocks: (B:7:0x0014, B:35:0x011e, B:38:0x0123, B:11:0x0023, B:22:0x00d0, B:24:0x00d8, B:28:0x0100, B:32:0x010d, B:31:0x0109, B:27:0x00fc, B:19:0x00ab), top: B:42:0x000c }] */
    /* JADX WARN: Removed duplicated region for block: B:37:0x0122  */
    /* JADX WARN: Removed duplicated region for block: B:38:0x0123 A[Catch: Exception -> 0x013c, TRY_LEAVE, TryCatch #0 {Exception -> 0x013c, blocks: (B:7:0x0014, B:35:0x011e, B:38:0x0123, B:11:0x0023, B:22:0x00d0, B:24:0x00d8, B:28:0x0100, B:32:0x010d, B:31:0x0109, B:27:0x00fc, B:19:0x00ab), top: B:42:0x000c }] */
    @Override // kotlin.coroutines.jvm.internal.BaseContinuationImpl
    /*
        Code decompiled incorrectly, please refer to instructions dump.
        To view partially-correct code enable 'Show inconsistent code' option in preferences
    */
    public final java.lang.Object invokeSuspend(java.lang.Object r34) {
        /*
            Method dump skipped, instructions count: 324
            To view this dump change 'Code comments level' option to 'DEBUG'
        */
        throw new UnsupportedOperationException("Method not decompiled: net.safemoon.androidwallet.viewmodels.CryptoPriceAlertViewModel$deleteAllPriceAlert$1.invokeSuspend(java.lang.Object):java.lang.Object");
    }
}
