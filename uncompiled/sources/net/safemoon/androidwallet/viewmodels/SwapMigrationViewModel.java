package net.safemoon.androidwallet.viewmodels;

import android.app.Application;
import androidx.lifecycle.LiveData;
import com.fasterxml.jackson.databind.deser.std.ThrowableDeserializer;
import com.github.mikephil.charting.utils.Utils;
import com.google.gson.Gson;
import defpackage.st1;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.List;
import net.safemoon.androidwallet.ERC20;
import net.safemoon.androidwallet.R;
import net.safemoon.androidwallet.model.common.Gas;
import net.safemoon.androidwallet.model.common.GasPrice;
import net.safemoon.androidwallet.model.common.LoadingState;
import net.safemoon.androidwallet.model.swap.Swap;
import net.safemoon.androidwallet.viewmodels.SwapMigrationViewModel;
import net.safemoon.androidwallet.viewmodels.blockChainClass.MigrationToV2;

/* compiled from: SwapMigrationViewModel.kt */
/* loaded from: classes2.dex */
public final class SwapMigrationViewModel extends gd {
    public ERC20 A;
    public st1 B;
    public st1 C;
    public st1 D;
    public st1 E;
    public st1 F;
    public st1 G;
    public final gb2<Double> H;
    public final gb2<Double> I;
    public final gb2<Boolean> J;
    public st1 K;
    public final gb2<LoadingState> L;
    public final gb2<d> M;
    public final gb2<List<Swap>> b;
    public final gb2<a> c;
    public final gb2<Double> d;
    public final gb2<Gas> e;
    public final gb2<Integer> f;
    public final gb2<Swap> g;
    public final gb2<Swap> h;
    public final gb2<BigDecimal> i;
    public final gb2<BigDecimal> j;
    public final gb2<Double> k;
    public final gb2<Double> l;
    public final gb2<String> m;
    public final gb2<c> n;
    public final gb2<LoadingState> o;
    public final gb2<BigDecimal> p;
    public final gb2<Integer> q;
    public final gb2<LoadingState> r;
    public final GasPrice s;
    public final sy1 t;
    public final sy1 u;
    public final sy1 v;
    public MigrationToV2 w;
    public st1 x;
    public final BigInteger y;
    public ERC20 z;

    /* compiled from: SwapMigrationViewModel.kt */
    /* loaded from: classes2.dex */
    public enum ApproveStatus {
        ALLOWED,
        FAILED,
        CONFUSE
    }

    /* compiled from: SwapMigrationViewModel.kt */
    /* loaded from: classes2.dex */
    public static final class a {
        public final boolean a;
        public final BigDecimal b;

        public a(boolean z, BigDecimal bigDecimal) {
            fs1.f(bigDecimal, "amount");
            this.a = z;
            this.b = bigDecimal;
        }

        public final BigDecimal a() {
            return this.b;
        }

        public final boolean b() {
            return this.a;
        }

        public boolean equals(Object obj) {
            if (this == obj) {
                return true;
            }
            if (obj instanceof a) {
                a aVar = (a) obj;
                return this.a == aVar.a && fs1.b(this.b, aVar.b);
            }
            return false;
        }

        /* JADX WARN: Multi-variable type inference failed */
        /* JADX WARN: Type inference failed for: r0v1, types: [int] */
        /* JADX WARN: Type inference failed for: r0v4 */
        /* JADX WARN: Type inference failed for: r0v5 */
        public int hashCode() {
            boolean z = this.a;
            ?? r0 = z;
            if (z) {
                r0 = 1;
            }
            return (r0 * 31) + this.b.hashCode();
        }

        public String toString() {
            return "AMOUNT(isSource=" + this.a + ", amount=" + this.b + ')';
        }
    }

    /* compiled from: SwapMigrationViewModel.kt */
    /* loaded from: classes2.dex */
    public static final class b {
        public b() {
        }

        public /* synthetic */ b(qi0 qi0Var) {
            this();
        }
    }

    /* compiled from: SwapMigrationViewModel.kt */
    /* loaded from: classes2.dex */
    public static final class c {
        public final BigInteger a;
        public final String b;

        public c(BigInteger bigInteger, String str) {
            fs1.f(bigInteger, "amount");
            fs1.f(str, "method");
            this.a = bigInteger;
            this.b = str;
        }

        public final String a() {
            return this.b;
        }

        public boolean equals(Object obj) {
            if (this == obj) {
                return true;
            }
            if (obj instanceof c) {
                c cVar = (c) obj;
                return fs1.b(this.a, cVar.a) && fs1.b(this.b, cVar.b);
            }
            return false;
        }

        public int hashCode() {
            return (this.a.hashCode() * 31) + this.b.hashCode();
        }

        public String toString() {
            return "GasLimit(amount=" + this.a + ", method=" + this.b + ')';
        }
    }

    /* compiled from: SwapMigrationViewModel.kt */
    /* loaded from: classes2.dex */
    public static final class d {
        public final boolean a;
        public final String b;

        public d(boolean z, String str) {
            fs1.f(str, ThrowableDeserializer.PROP_NAME_MESSAGE);
            this.a = z;
            this.b = str;
        }

        public final String a() {
            return this.b;
        }

        public final boolean b() {
            return this.a;
        }

        public boolean equals(Object obj) {
            if (this == obj) {
                return true;
            }
            if (obj instanceof d) {
                d dVar = (d) obj;
                return this.a == dVar.a && fs1.b(this.b, dVar.b);
            }
            return false;
        }

        /* JADX WARN: Multi-variable type inference failed */
        /* JADX WARN: Type inference failed for: r0v1, types: [int] */
        /* JADX WARN: Type inference failed for: r0v4 */
        /* JADX WARN: Type inference failed for: r0v5 */
        public int hashCode() {
            boolean z = this.a;
            ?? r0 = z;
            if (z) {
                r0 = 1;
            }
            return (r0 * 31) + this.b.hashCode();
        }

        public String toString() {
            return "TransactionResult(isSuccess=" + this.a + ", message=" + this.b + ')';
        }
    }

    static {
        new b(null);
    }

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public SwapMigrationViewModel(Application application) {
        super(application);
        fs1.f(application, "application");
        this.b = new gb2<>(b20.g());
        l0();
        this.c = new gb2<>();
        this.d = new gb2<>(Double.valueOf(12.0d));
        this.e = new gb2<>(Gas.Standard);
        this.f = new gb2<>(20);
        this.g = new gb2<>();
        this.h = new gb2<>();
        this.i = new gb2<>();
        this.j = new gb2<>();
        this.k = new gb2<>();
        this.l = new gb2<>();
        this.m = new gb2<>("");
        this.n = new gb2<>();
        LoadingState loadingState = LoadingState.Normal;
        this.o = new gb2<>(loadingState);
        this.p = new gb2<>();
        this.q = new gb2<>(0);
        this.r = new gb2<>(loadingState);
        this.s = new GasPrice();
        this.t = zy1.a(new SwapMigrationViewModel$privateKey$2(this));
        this.u = zy1.a(new SwapMigrationViewModel$address$2(this));
        this.v = zy1.a(new SwapMigrationViewModel$credentails$2(this));
        this.y = BigInteger.valueOf(9000000L);
        Double valueOf = Double.valueOf((double) Utils.DOUBLE_EPSILON);
        this.H = new gb2<>(valueOf);
        this.I = new gb2<>(valueOf);
        this.J = new gb2<>(Boolean.FALSE);
        this.L = new gb2<>(loadingState);
        this.M = new gb2<>();
    }

    public static final void p0(SwapMigrationViewModel swapMigrationViewModel, Swap swap) {
        fs1.f(swapMigrationViewModel, "this$0");
        if (swap != null) {
            swapMigrationViewModel.j0();
            swapMigrationViewModel.m0();
        }
    }

    public static final void q0(SwapMigrationViewModel swapMigrationViewModel, Swap swap) {
        fs1.f(swapMigrationViewModel, "this$0");
        if (swap != null) {
            swapMigrationViewModel.h0();
            swapMigrationViewModel.m0();
        }
    }

    public static final void r0(SwapMigrationViewModel swapMigrationViewModel, Double d2) {
        fs1.f(swapMigrationViewModel, "this$0");
        swapMigrationViewModel.m0();
    }

    public static final void s0(SwapMigrationViewModel swapMigrationViewModel, Gas gas) {
        fs1.f(swapMigrationViewModel, "this$0");
        swapMigrationViewModel.m0();
    }

    public final String A() {
        return (String) this.u.getValue();
    }

    public final gb2<Double> B() {
        return this.H;
    }

    public final void C() {
        st1 b2;
        b2 = as.b(ej4.a(this), null, null, new SwapMigrationViewModel$getAmountOut$1(this, null), 3, null);
        this.x = b2;
    }

    public final gb2<Double> D() {
        return this.I;
    }

    public final gb2<LoadingState> E() {
        return this.r;
    }

    public final gb2<Integer> F() {
        return this.q;
    }

    public final gb2<Boolean> G() {
        return this.J;
    }

    public final ma0 H() {
        return (ma0) this.v.getValue();
    }

    public final gb2<Integer> I() {
        return this.f;
    }

    public final gb2<BigDecimal> J() {
        return this.j;
    }

    public final gb2<Swap> K() {
        return this.h;
    }

    public final gb2<Double> L() {
        return this.l;
    }

    public final void M(Swap swap) {
        st1 b2;
        st1 st1Var = this.E;
        if (st1Var != null) {
            st1.a.a(st1Var, null, 1, null);
        }
        b2 = as.b(ej4.a(this), null, null, new SwapMigrationViewModel$getDestinationPriceFromCMC$1(swap, this, null), 3, null);
        this.E = b2;
    }

    public final gb2<LoadingState> N() {
        return this.o;
    }

    public final gb2<Gas> O() {
        return this.e;
    }

    public final BigInteger P() {
        Swap value = this.g.getValue();
        if (value == null) {
            return BigInteger.ZERO;
        }
        if (g0()) {
            GasPrice gasPrice = this.s;
            Gas value2 = O().getValue();
            fs1.d(value2);
            fs1.e(value2, "gas.value!!");
            Integer num = value.chainId;
            fs1.e(num, "it.chainId");
            return new BigDecimal(gasPrice.getPrice(value2, num.intValue())).multiply(BigDecimal.TEN.pow(9)).toBigInteger();
        }
        GasPrice gasPrice2 = this.s;
        Gas value3 = O().getValue();
        fs1.d(value3);
        fs1.e(value3, "gas.value!!");
        Integer num2 = value.chainId;
        fs1.e(num2, "it.chainId");
        return new BigDecimal(gasPrice2.getPrice(value3, num2.intValue())).multiply(BigDecimal.TEN.pow(9)).toBigInteger();
    }

    public final j80 Q(BigInteger bigInteger) {
        return new it3(P(), bigInteger);
    }

    public final gb2<LoadingState> R() {
        return this.L;
    }

    public final String S() {
        return (String) this.t.getValue();
    }

    public final String T() {
        Integer num;
        String z;
        Swap value = this.g.getValue();
        return (value == null || (num = value.chainId) == null || (z = e30.z(num.intValue())) == null) ? "https://noRPC" : z;
    }

    public final gb2<BigDecimal> U() {
        return this.p;
    }

    public final gb2<Double> V() {
        return this.d;
    }

    public final gb2<BigDecimal> W() {
        return this.i;
    }

    public final gb2<Swap> X() {
        return this.g;
    }

    public final gb2<Double> Y() {
        return this.k;
    }

    public final void Z(Swap swap) {
        st1 b2;
        st1 st1Var = this.C;
        if (st1Var != null) {
            st1.a.a(st1Var, null, 1, null);
        }
        b2 = as.b(ej4.a(this), null, null, new SwapMigrationViewModel$getSourcePriceFromCMC$1(swap, this, null), 3, null);
        this.C = b2;
    }

    public final String a0(int i) {
        String string = a().getString(i);
        fs1.e(string, "getApplication<Application>().getString(resId)");
        return string;
    }

    public final gb2<List<Swap>> b0() {
        return this.b;
    }

    public final gb2<a> c0() {
        return this.c;
    }

    public final gb2<String> d0() {
        return this.m;
    }

    public final gb2<c> e0() {
        return this.n;
    }

    public final gb2<d> f0() {
        return this.M;
    }

    public final boolean g0() {
        Swap value = this.g.getValue();
        Integer num = value == null ? null : value.chainId;
        if (num != null && num.intValue() == 1) {
            return true;
        }
        return num != null && num.intValue() == 3;
    }

    public final void h0() {
        this.j.postValue(BigDecimal.ZERO);
        Swap value = this.h.getValue();
        fs1.d(value);
        fs1.e(value, "destinationLiveData.value!!");
        String D = e30.D(value);
        ko4 w0 = w0();
        ma0 H = H();
        BigInteger bigInteger = this.y;
        fs1.e(bigInteger, "GAS_LIMIT");
        ERC20 s = ERC20.s(D, w0, H, Q(bigInteger));
        fs1.e(s, "load(swap.getWrapAddress…etGasProvider(GAS_LIMIT))");
        this.A = s;
        i0();
    }

    public final void i0() {
        st1 b2;
        st1 b3;
        Swap value = this.h.getValue();
        fs1.d(value);
        fs1.e(value, "destinationLiveData.value!!");
        Swap swap = value;
        st1 st1Var = this.D;
        if (st1Var != null) {
            st1.a.a(st1Var, null, 1, null);
        }
        b2 = as.b(ej4.a(this), null, null, new SwapMigrationViewModel$loadDestinationBalance$1(swap, this, null), 3, null);
        this.D = b2;
        st1 st1Var2 = this.E;
        if (st1Var2 != null) {
            st1.a.a(st1Var2, null, 1, null);
        }
        b3 = as.b(ej4.a(this), null, null, new SwapMigrationViewModel$loadDestinationBalance$2(swap, this, null), 3, null);
        this.E = b3;
    }

    public final void j0() {
        this.i.postValue(BigDecimal.ZERO);
        Swap value = this.g.getValue();
        fs1.d(value);
        fs1.e(value, "sourceLiveData.value!!");
        String D = e30.D(value);
        ko4 w0 = w0();
        ma0 H = H();
        BigInteger bigInteger = this.y;
        fs1.e(bigInteger, "GAS_LIMIT");
        ERC20 s = ERC20.s(D, w0, H, Q(bigInteger));
        fs1.e(s, "load(swap.getWrapAddress…etGasProvider(GAS_LIMIT))");
        this.z = s;
        k0();
    }

    public final void k0() {
        st1 b2;
        st1 b3;
        Swap value = this.g.getValue();
        fs1.d(value);
        fs1.e(value, "sourceLiveData.value!!");
        Swap swap = value;
        st1 st1Var = this.B;
        if (st1Var != null) {
            st1.a.a(st1Var, null, 1, null);
        }
        b2 = as.b(ej4.a(this), null, null, new SwapMigrationViewModel$loadSourceBalance$1(swap, this, null), 3, null);
        this.B = b2;
        st1 st1Var2 = this.C;
        if (st1Var2 != null) {
            st1.a.a(st1Var2, null, 1, null);
        }
        b3 = as.b(ej4.a(this), null, null, new SwapMigrationViewModel$loadSourceBalance$2(swap, this, null), 3, null);
        this.C = b3;
    }

    public final void l0() {
        gb2<List<Swap>> gb2Var = this.b;
        Gson gson = new Gson();
        MigrationToV2.a aVar = MigrationToV2.m;
        gb2Var.postValue(b20.j((Swap) gson.fromJson(aVar.a(), Swap.class), (Swap) new Gson().fromJson(aVar.b(), Swap.class)));
    }

    public final void m0() {
        this.n.postValue(null);
        this.m.setValue("");
        if (this.g.getValue() == null || this.h.getValue() == null) {
            return;
        }
        this.J.setValue(Boolean.TRUE);
        Application a2 = a();
        fs1.e(a2, "getApplication()");
        Swap value = this.g.getValue();
        fs1.d(value);
        fs1.e(value, "sourceLiveData.value!!");
        Swap value2 = this.h.getValue();
        fs1.d(value2);
        fs1.e(value2, "destinationLiveData.value!!");
        this.w = new MigrationToV2(a2, value, value2);
        st1 st1Var = this.x;
        if (st1Var != null) {
            st1.a.a(st1Var, null, 1, null);
        }
        C();
    }

    public final void n0(String str) {
        this.M.setValue(new d(true, str));
        gb2<a> gb2Var = this.c;
        BigDecimal bigDecimal = BigDecimal.ZERO;
        fs1.e(bigDecimal, "ZERO");
        gb2Var.setValue(new a(true, bigDecimal));
    }

    public final void o0(rz1 rz1Var) {
        fs1.f(rz1Var, "lifecycle");
        this.n.postValue(null);
        this.m.setValue("");
        this.g.observe(rz1Var, new tl2() { // from class: b14
            @Override // defpackage.tl2
            public final void onChanged(Object obj) {
                SwapMigrationViewModel.p0(SwapMigrationViewModel.this, (Swap) obj);
            }
        });
        this.h.observe(rz1Var, new tl2() { // from class: a14
            @Override // defpackage.tl2
            public final void onChanged(Object obj) {
                SwapMigrationViewModel.q0(SwapMigrationViewModel.this, (Swap) obj);
            }
        });
        LiveData a2 = cb4.a(this.d);
        fs1.e(a2, "Transformations.distinctUntilChanged(this)");
        a2.observe(rz1Var, new tl2() { // from class: y04
            @Override // defpackage.tl2
            public final void onChanged(Object obj) {
                SwapMigrationViewModel.r0(SwapMigrationViewModel.this, (Double) obj);
            }
        });
        LiveData a3 = cb4.a(this.e);
        fs1.e(a3, "Transformations.distinctUntilChanged(this)");
        a3.observe(rz1Var, new tl2() { // from class: z04
            @Override // defpackage.tl2
            public final void onChanged(Object obj) {
                SwapMigrationViewModel.s0(SwapMigrationViewModel.this, (Gas) obj);
            }
        });
    }

    public final void t0() {
        double doubleValue;
        Swap value = this.g.getValue();
        if (value != null) {
            try {
                a value2 = this.c.getValue();
                fs1.d(value2);
                if (value2.b()) {
                    a value3 = this.c.getValue();
                    fs1.d(value3);
                    doubleValue = value3.a().doubleValue();
                } else {
                    Double value4 = this.I.getValue();
                    fs1.d(value4);
                    fs1.e(value4, "{\n                    am…value!!\n                }");
                    doubleValue = value4.doubleValue();
                }
                BigDecimal value5 = this.i.getValue();
                fs1.d(value5);
                if (doubleValue > value5.doubleValue()) {
                    this.m.postValue(a0(R.string.swap_amount_less_available_balance));
                    return;
                }
                throw new Exception("For Throw to show bottom msg");
            } catch (Exception unused) {
                String str = g0() ? "ETH" : "BNB";
                if (dv3.t(value.symbol, str, true)) {
                    gb2<String> gb2Var = this.m;
                    gb2Var.postValue(a0(R.string.swap_transactionFee_insufficent1) + str + a0(R.string.swap_transactionFee_insufficent2));
                    return;
                }
                gb2<String> gb2Var2 = this.m;
                gb2Var2.postValue(a0(R.string.swap_transactionFee_not_enough1) + str + a0(R.string.swap_transactionFee_not_enough2));
            }
        }
    }

    public final void u0() {
        as.b(ej4.a(this), null, null, new SwapMigrationViewModel$swapTokens$1(this, null), 3, null);
    }

    public final void v0() {
        gb2<BigDecimal> gb2Var = this.i;
        gb2Var.postValue(gb2Var.getValue());
        gb2<BigDecimal> gb2Var2 = this.j;
        gb2Var2.postValue(gb2Var2.getValue());
    }

    public final void w() {
        as.b(ej4.a(this), null, null, new SwapMigrationViewModel$approve$1(this, null), 3, null);
    }

    public final ko4 w0() {
        return jo4.a(new ml1(T()));
    }

    public final void x() {
        st1 st1Var = this.C;
        if (st1Var != null) {
            st1.a.a(st1Var, null, 1, null);
        }
        st1 st1Var2 = this.B;
        if (st1Var2 != null) {
            st1.a.a(st1Var2, null, 1, null);
        }
        st1 st1Var3 = this.E;
        if (st1Var3 != null) {
            st1.a.a(st1Var3, null, 1, null);
        }
        st1 st1Var4 = this.D;
        if (st1Var4 != null) {
            st1.a.a(st1Var4, null, 1, null);
        }
        st1 st1Var5 = this.F;
        if (st1Var5 != null) {
            st1.a.a(st1Var5, null, 1, null);
        }
        st1 st1Var6 = this.K;
        if (st1Var6 != null) {
            st1.a.a(st1Var6, null, 1, null);
        }
        this.c.postValue(null);
        this.i.postValue(BigDecimal.ZERO);
        this.j.postValue(BigDecimal.ZERO);
        gb2<Swap> gb2Var = this.g;
        gb2Var.postValue(gb2Var.getValue());
        gb2<Swap> gb2Var2 = this.h;
        gb2Var2.postValue(gb2Var2.getValue());
        this.r.postValue(LoadingState.Normal);
        this.q.postValue(0);
    }

    public final void y(String str) {
        st1 b2;
        st1 st1Var = this.G;
        if (st1Var != null) {
            st1.a.a(st1Var, null, 1, null);
        }
        b2 = as.b(ej4.a(this), null, null, new SwapMigrationViewModel$checkApproveStatus$1(this, str, null), 3, null);
        this.G = b2;
    }

    public final Object z(String str, q70<? super zw0> q70Var) {
        zw0 send = w0().ethGetTransactionReceipt(str).send();
        fs1.e(send, "web3().ethGetTransaction…t(transactionHash).send()");
        return send;
    }
}
