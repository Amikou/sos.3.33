package net.safemoon.androidwallet.viewmodels;

import android.app.Application;
import androidx.lifecycle.LiveData;
import java.util.List;
import net.safemoon.androidwallet.common.TokenType;
import net.safemoon.androidwallet.model.walletConnect.RoomConnectedInfo;
import net.safemoon.androidwallet.model.walletConnect.RoomConnectedInfoAndWallet;

/* compiled from: WalletConnectViewModel.kt */
/* loaded from: classes2.dex */
public final class WalletConnectViewModel extends gd {
    public final sy1 b;
    public final sy1 c;
    public final gb2<TokenType> d;
    public final gb2<Boolean> e;
    public final LiveData<List<RoomConnectedInfoAndWallet>> f;

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public WalletConnectViewModel(Application application) {
        super(application);
        fs1.f(application, "application");
        this.b = zy1.a(new WalletConnectViewModel$WalletConnectDao$2(application));
        this.c = zy1.a(new WalletConnectViewModel$defaultGateWay$2(application));
        gb2<TokenType> gb2Var = new gb2<>();
        this.d = gb2Var;
        this.e = new gb2<>(Boolean.TRUE);
        gb2Var.postValue(d());
        this.f = f().c();
    }

    public final LiveData<List<RoomConnectedInfoAndWallet>> c() {
        return this.f;
    }

    public final TokenType d() {
        return (TokenType) this.c.getValue();
    }

    public final gb2<TokenType> e() {
        return this.d;
    }

    public final t50 f() {
        return (t50) this.b.getValue();
    }

    public final gb2<Boolean> g() {
        return this.e;
    }

    public final void h(RoomConnectedInfo roomConnectedInfo) {
        fs1.f(roomConnectedInfo, "rci");
        as.b(ej4.a(this), null, null, new WalletConnectViewModel$saveConnectedInfo$1(this, roomConnectedInfo, null), 3, null);
    }
}
