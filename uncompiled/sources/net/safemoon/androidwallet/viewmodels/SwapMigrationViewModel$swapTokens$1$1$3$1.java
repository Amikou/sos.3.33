package net.safemoon.androidwallet.viewmodels;

import android.app.Application;
import kotlin.coroutines.jvm.internal.SuspendLambda;
import kotlin.coroutines.jvm.internal.a;
import net.safemoon.androidwallet.R;
import net.safemoon.androidwallet.model.common.LoadingState;

/* compiled from: SwapMigrationViewModel.kt */
@a(c = "net.safemoon.androidwallet.viewmodels.SwapMigrationViewModel$swapTokens$1$1$3$1", f = "SwapMigrationViewModel.kt", l = {}, m = "invokeSuspend")
/* loaded from: classes2.dex */
public final class SwapMigrationViewModel$swapTokens$1$1$3$1 extends SuspendLambda implements hd1<c90, q70<? super te4>, Object> {
    public final /* synthetic */ w84 $it;
    public int label;
    public final /* synthetic */ SwapMigrationViewModel this$0;

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public SwapMigrationViewModel$swapTokens$1$1$3$1(SwapMigrationViewModel swapMigrationViewModel, w84 w84Var, q70<? super SwapMigrationViewModel$swapTokens$1$1$3$1> q70Var) {
        super(2, q70Var);
        this.this$0 = swapMigrationViewModel;
        this.$it = w84Var;
    }

    @Override // kotlin.coroutines.jvm.internal.BaseContinuationImpl
    public final q70<te4> create(Object obj, q70<?> q70Var) {
        return new SwapMigrationViewModel$swapTokens$1$1$3$1(this.this$0, this.$it, q70Var);
    }

    @Override // defpackage.hd1
    public final Object invoke(c90 c90Var, q70<? super te4> q70Var) {
        return ((SwapMigrationViewModel$swapTokens$1$1$3$1) create(c90Var, q70Var)).invokeSuspend(te4.a);
    }

    @Override // kotlin.coroutines.jvm.internal.BaseContinuationImpl
    public final Object invokeSuspend(Object obj) {
        gs1.d();
        if (this.label == 0) {
            o83.b(obj);
            this.this$0.R().setValue(LoadingState.Normal);
            w84 w84Var = this.$it;
            if (w84Var != null) {
                SwapMigrationViewModel swapMigrationViewModel = this.this$0;
                String transactionHash = w84Var.getTransactionHash();
                fs1.e(transactionHash, "it.transactionHash");
                swapMigrationViewModel.n0(transactionHash);
            } else {
                Application a = this.this$0.a();
                fs1.e(a, "getApplication<Application>()");
                e30.a0(a, this.this$0.a0(R.string.swap_could_not));
            }
            return te4.a;
        }
        throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
    }
}
