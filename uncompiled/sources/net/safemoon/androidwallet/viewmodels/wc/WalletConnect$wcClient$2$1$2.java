package net.safemoon.androidwallet.viewmodels.wc;

import com.fasterxml.jackson.databind.deser.std.ThrowableDeserializer;
import kotlin.jvm.internal.Lambda;

/* compiled from: WalletConnect.kt */
/* loaded from: classes2.dex */
public final class WalletConnect$wcClient$2$1$2 extends Lambda implements hd1<Integer, String, te4> {
    public final /* synthetic */ WalletConnect this$0;

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public WalletConnect$wcClient$2$1$2(WalletConnect walletConnect) {
        super(2);
        this.this$0 = walletConnect;
    }

    @Override // defpackage.hd1
    public /* bridge */ /* synthetic */ te4 invoke(Integer num, String str) {
        invoke(num.intValue(), str);
        return te4.a;
    }

    public final void invoke(int i, String str) {
        fs1.f(str, ThrowableDeserializer.PROP_NAME_MESSAGE);
        this.this$0.q(i, str);
    }
}
