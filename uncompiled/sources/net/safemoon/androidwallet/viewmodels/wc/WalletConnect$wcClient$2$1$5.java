package net.safemoon.androidwallet.viewmodels.wc;

import com.fasterxml.jackson.databind.deser.std.ThrowableDeserializer;
import com.trustwallet.walletconnect.models.ethereum.WCEthereumSignMessage;
import kotlin.jvm.internal.Lambda;

/* compiled from: WalletConnect.kt */
/* loaded from: classes2.dex */
public final class WalletConnect$wcClient$2$1$5 extends Lambda implements hd1<Long, WCEthereumSignMessage, te4> {
    public final /* synthetic */ WalletConnect this$0;

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public WalletConnect$wcClient$2$1$5(WalletConnect walletConnect) {
        super(2);
        this.this$0 = walletConnect;
    }

    @Override // defpackage.hd1
    public /* bridge */ /* synthetic */ te4 invoke(Long l, WCEthereumSignMessage wCEthereumSignMessage) {
        invoke(l.longValue(), wCEthereumSignMessage);
        return te4.a;
    }

    public final void invoke(long j, WCEthereumSignMessage wCEthereumSignMessage) {
        fs1.f(wCEthereumSignMessage, ThrowableDeserializer.PROP_NAME_MESSAGE);
        this.this$0.r(j, wCEthereumSignMessage);
    }
}
