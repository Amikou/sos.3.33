package net.safemoon.androidwallet.viewmodels.wc;

import com.trustwallet.walletconnect.models.ethereum.WCEthereumTransaction;
import kotlin.jvm.internal.Lambda;

/* compiled from: WalletConnect.kt */
/* loaded from: classes2.dex */
public final class WalletConnect$wcClient$2$1$7 extends Lambda implements hd1<Long, WCEthereumTransaction, te4> {
    public final /* synthetic */ WalletConnect this$0;

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public WalletConnect$wcClient$2$1$7(WalletConnect walletConnect) {
        super(2);
        this.this$0 = walletConnect;
    }

    @Override // defpackage.hd1
    public /* bridge */ /* synthetic */ te4 invoke(Long l, WCEthereumTransaction wCEthereumTransaction) {
        invoke(l.longValue(), wCEthereumTransaction);
        return te4.a;
    }

    public final void invoke(long j, WCEthereumTransaction wCEthereumTransaction) {
        fs1.f(wCEthereumTransaction, "transaction");
        this.this$0.s(j, wCEthereumTransaction);
    }
}
