package net.safemoon.androidwallet.viewmodels.wc;

import com.google.gson.GsonBuilder;
import com.trustwallet.walletconnect.WCClient;
import kotlin.jvm.internal.Lambda;
import okhttp3.OkHttpClient;

/* compiled from: WalletConnect.kt */
/* loaded from: classes2.dex */
public final class WalletConnect$wcClient$2 extends Lambda implements rc1<WCClient> {
    public final /* synthetic */ WalletConnect this$0;

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public WalletConnect$wcClient$2(WalletConnect walletConnect) {
        super(0);
        this.this$0 = walletConnect;
    }

    /* JADX WARN: Can't rename method to resolve collision */
    @Override // defpackage.rc1
    public final WCClient invoke() {
        WCClient wCClient = new WCClient(new GsonBuilder(), new OkHttpClient());
        WalletConnect walletConnect = this.this$0;
        wCClient.setOnSessionRequest(new WalletConnect$wcClient$2$1$1(walletConnect));
        wCClient.setOnDisconnect(new WalletConnect$wcClient$2$1$2(walletConnect));
        wCClient.setOnFailure(WalletConnect$wcClient$2$1$3.INSTANCE);
        wCClient.setOnGetAccounts(WalletConnect$wcClient$2$1$4.INSTANCE);
        wCClient.setOnEthSign(new WalletConnect$wcClient$2$1$5(walletConnect));
        wCClient.setOnEthSignTransaction(new WalletConnect$wcClient$2$1$6(walletConnect));
        wCClient.setOnEthSendTransaction(new WalletConnect$wcClient$2$1$7(walletConnect));
        wCClient.setOnSignTransaction(WalletConnect$wcClient$2$1$8.INSTANCE);
        return wCClient;
    }
}
