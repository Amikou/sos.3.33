package net.safemoon.androidwallet.viewmodels.wc;

import android.app.Activity;
import com.trustwallet.walletconnect.WCClient;
import com.trustwallet.walletconnect.exceptions.InvalidSessionException;
import com.trustwallet.walletconnect.models.WCPeerMeta;
import com.trustwallet.walletconnect.models.ethereum.WCEthereumSignMessage;
import com.trustwallet.walletconnect.models.ethereum.WCEthereumTransaction;
import com.trustwallet.walletconnect.models.session.WCSession;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Objects;
import kotlin.text.StringsKt__StringsKt;
import kotlin.text.StringsKt___StringsKt;
import net.safemoon.androidwallet.common.TokenType;
import net.safemoon.androidwallet.model.walletConnect.RoomConnectedInfo;
import net.safemoon.androidwallet.model.walletConnect.RoomExtensionKt;
import net.safemoon.androidwallet.model.wallets.Wallet;
import org.web3j.crypto.c;
import wallet.core.jni.CoinType;
import wallet.core.jni.PrivateKey;

/* compiled from: WalletConnect.kt */
/* loaded from: classes2.dex */
public abstract class WalletConnect {
    public final Activity a;
    public final Wallet b;
    public final sy1 c;
    public final sy1 d;
    public final sy1 e;
    public final String f;
    public final WCPeerMeta g;
    public WCSession h;
    public WCPeerMeta i;

    /* compiled from: WalletConnect.kt */
    /* loaded from: classes2.dex */
    public static final class a {
        public a() {
        }

        public /* synthetic */ a(qi0 qi0Var) {
            this();
        }
    }

    static {
        new a(null);
        System.loadLibrary("TrustWalletCore");
    }

    public WalletConnect(Activity activity, Wallet wallet2) {
        fs1.f(activity, "activity");
        fs1.f(wallet2, "wallet");
        this.a = activity;
        this.b = wallet2;
        this.c = zy1.a(new WalletConnect$wcClient$2(this));
        this.d = zy1.a(new WalletConnect$privateKey$2(this));
        this.e = zy1.a(new WalletConnect$credentails$2(this));
        this.f = CoinType.ETHEREUM.deriveAddress(m());
        this.g = new WCPeerMeta("Safemoon", "https://safemoon.net/", null, null, 12, null);
    }

    public static /* synthetic */ Object j(WalletConnect walletConnect, RoomConnectedInfo roomConnectedInfo, long j, WCEthereumTransaction wCEthereumTransaction, boolean z, q70 q70Var, int i, Object obj) {
        if (obj == null) {
            if ((i & 8) != 0) {
                z = false;
            }
            return walletConnect.i(roomConnectedInfo, j, wCEthereumTransaction, z, q70Var);
        }
        throw new UnsupportedOperationException("Super calls with default arguments not supported in this target, function: ethTransaction");
    }

    public final RoomConnectedInfo c(TokenType tokenType, boolean z) {
        if (tokenType != null) {
            if (o().approveSession(a20.b(this.f), tokenType.getChainId())) {
                WCSession session = o().getSession();
                String uri = session != null ? session.toUri() : null;
                fs1.d(uri);
                String peerId = o().getPeerId();
                fs1.d(peerId);
                String remotePeerId = o().getRemotePeerId();
                fs1.d(remotePeerId);
                int chainId = tokenType.getChainId();
                long z2 = b30.a.z(new Date());
                String changeToString = RoomExtensionKt.changeToString(this.i);
                Long id = this.b.getId();
                fs1.d(id);
                long longValue = id.longValue();
                fs1.e(changeToString, "changeToString()");
                return new RoomConnectedInfo(uri, peerId, remotePeerId, chainId, changeToString, z2, z, longValue);
            }
            return null;
        }
        return null;
    }

    public final void d(String str) {
        fs1.f(str, "uri");
        p();
        WCSession from = WCSession.Companion.from(str);
        if (from != null) {
            this.h = from;
            WCClient o = o();
            WCSession wCSession = this.h;
            if (wCSession == null) {
                fs1.r("wcSession");
                wCSession = null;
            }
            WCClient.connect$default(o, wCSession, this.g, null, null, 12, null);
            return;
        }
        throw new InvalidSessionException();
    }

    public final byte[] e(String str) {
        if (str.length() % 2 == 0) {
            List<String> M0 = StringsKt___StringsKt.M0(StringsKt__StringsKt.n0(str, "0x"), 2);
            ArrayList arrayList = new ArrayList(c20.q(M0, 10));
            for (String str2 : M0) {
                arrayList.add(Byte.valueOf((byte) Integer.parseInt(str2, xx.a(16))));
            }
            return j20.g0(arrayList);
        }
        throw new IllegalStateException("Must have an even length".toString());
    }

    public final void f() {
        o().disconnect();
    }

    public final void g(long j, WCEthereumSignMessage wCEthereumSignMessage) {
        fs1.f(wCEthereumSignMessage, "_message");
        if (wCEthereumSignMessage.getType() == WCEthereumSignMessage.WCSignType.ETH_SIGN_TYPE_DATA_V4) {
            h(j, wCEthereumSignMessage);
            return;
        }
        ma0 l = l();
        boolean H = dv3.H(wCEthereumSignMessage.getData(), "0x", false, 2, null);
        String data = wCEthereumSignMessage.getData();
        if (H) {
            Objects.requireNonNull(data, "null cannot be cast to non-null type java.lang.String");
            data = data.substring(2);
            fs1.e(data, "(this as java.lang.String).substring(startIndex)");
        }
        c.a signPrefixedMessage = c.signPrefixedMessage(e30.F(data), l.getEcKeyPair());
        byte[] bArr = new byte[65];
        System.arraycopy(signPrefixedMessage.getR(), 0, bArr, 0, 32);
        System.arraycopy(signPrefixedMessage.getS(), 0, bArr, 32, 32);
        System.arraycopy(signPrefixedMessage.getV(), 0, bArr, 64, 1);
        o().approveRequest(j, ej2.toHexString(bArr));
    }

    public final void h(long j, WCEthereumSignMessage wCEthereumSignMessage) {
        fs1.f(wCEthereumSignMessage, "_message");
        ma0 l = l();
        boolean H = dv3.H(wCEthereumSignMessage.getData(), "0x", false, 2, null);
        String data = wCEthereumSignMessage.getData();
        if (H) {
            Objects.requireNonNull(data, "null cannot be cast to non-null type java.lang.String");
            data = data.substring(2);
            fs1.e(data, "(this as java.lang.String).substring(startIndex)");
        }
        c.a signPrefixedMessage = c.signPrefixedMessage(e30.F(data), l.getEcKeyPair());
        byte[] bArr = new byte[258];
        System.arraycopy(signPrefixedMessage.getR(), 0, bArr, 0, 64);
        System.arraycopy(signPrefixedMessage.getS(), 0, bArr, 64, 128);
        System.arraycopy(signPrefixedMessage.getV(), 0, bArr, 128, 130);
        o().approveRequest(j, ej2.toHexString(bArr));
    }

    /* JADX WARN: Removed duplicated region for block: B:35:0x0023  */
    /* JADX WARN: Removed duplicated region for block: B:39:0x0037  */
    /*
        Code decompiled incorrectly, please refer to instructions dump.
        To view partially-correct code enable 'Show inconsistent code' option in preferences
    */
    public final java.lang.Object i(net.safemoon.androidwallet.model.walletConnect.RoomConnectedInfo r5, long r6, com.trustwallet.walletconnect.models.ethereum.WCEthereumTransaction r8, boolean r9, defpackage.q70<? super defpackage.hx0> r10) {
        /*
            r4 = this;
            boolean r9 = r10 instanceof net.safemoon.androidwallet.viewmodels.wc.WalletConnect$ethTransaction$1
            if (r9 == 0) goto L13
            r9 = r10
            net.safemoon.androidwallet.viewmodels.wc.WalletConnect$ethTransaction$1 r9 = (net.safemoon.androidwallet.viewmodels.wc.WalletConnect$ethTransaction$1) r9
            int r0 = r9.label
            r1 = -2147483648(0xffffffff80000000, float:-0.0)
            r2 = r0 & r1
            if (r2 == 0) goto L13
            int r0 = r0 - r1
            r9.label = r0
            goto L18
        L13:
            net.safemoon.androidwallet.viewmodels.wc.WalletConnect$ethTransaction$1 r9 = new net.safemoon.androidwallet.viewmodels.wc.WalletConnect$ethTransaction$1
            r9.<init>(r4, r10)
        L18:
            java.lang.Object r10 = r9.result
            java.lang.Object r0 = defpackage.gs1.d()
            int r1 = r9.label
            r2 = 1
            if (r1 == 0) goto L37
            if (r1 != r2) goto L2f
            long r6 = r9.J$0
            java.lang.Object r5 = r9.L$0
            net.safemoon.androidwallet.viewmodels.wc.WalletConnect r5 = (net.safemoon.androidwallet.viewmodels.wc.WalletConnect) r5
            defpackage.o83.b(r10)
            goto L59
        L2f:
            java.lang.IllegalStateException r5 = new java.lang.IllegalStateException
            java.lang.String r6 = "call to 'resume' before 'invoke' with coroutine"
            r5.<init>(r6)
            throw r5
        L37:
            defpackage.o83.b(r10)
            int r5 = r5.getChainId()
            net.safemoon.androidwallet.viewmodels.wc.WcWeb3 r10 = new net.safemoon.androidwallet.viewmodels.wc.WcWeb3
            android.app.Activity r1 = r4.k()
            net.safemoon.androidwallet.model.wallets.Wallet r3 = r4.n()
            r10.<init>(r1, r5, r3)
            r9.L$0 = r4
            r9.J$0 = r6
            r9.label = r2
            java.lang.Object r10 = r10.B(r8, r9)
            if (r10 != r0) goto L58
            return r0
        L58:
            r5 = r4
        L59:
            hx0 r10 = (defpackage.hx0) r10
            i83$a r8 = r10.getError()
            if (r8 != 0) goto L73
            java.lang.String r8 = r10.getTransactionHash()
            if (r8 == 0) goto L73
            com.trustwallet.walletconnect.WCClient r5 = r5.o()
            java.lang.String r8 = r10.getTransactionHash()
            r5.approveRequest(r6, r8)
            goto L87
        L73:
            com.trustwallet.walletconnect.WCClient r5 = r5.o()
            i83$a r8 = r10.getError()
            java.lang.String r8 = r8.getMessage()
            java.lang.String r9 = "result.error.message"
            defpackage.fs1.e(r8, r9)
            r5.rejectRequest(r6, r8)
        L87:
            return r10
        */
        throw new UnsupportedOperationException("Method not decompiled: net.safemoon.androidwallet.viewmodels.wc.WalletConnect.i(net.safemoon.androidwallet.model.walletConnect.RoomConnectedInfo, long, com.trustwallet.walletconnect.models.ethereum.WCEthereumTransaction, boolean, q70):java.lang.Object");
    }

    public final Activity k() {
        return this.a;
    }

    public final ma0 l() {
        return (ma0) this.e.getValue();
    }

    public final PrivateKey m() {
        return (PrivateKey) this.d.getValue();
    }

    public final Wallet n() {
        return this.b;
    }

    public final WCClient o() {
        return (WCClient) this.c.getValue();
    }

    public final void p() {
        if (o().getSession() != null) {
            o().killSession();
        } else {
            o().disconnect();
        }
    }

    public abstract void q(int i, String str);

    public abstract void r(long j, WCEthereumSignMessage wCEthereumSignMessage);

    public abstract void s(long j, WCEthereumTransaction wCEthereumTransaction);

    public abstract void t(WCPeerMeta wCPeerMeta);

    public final void u(RoomConnectedInfo roomConnectedInfo) {
        fs1.f(roomConnectedInfo, "roomConnectedInfo");
        WCSession from = WCSession.Companion.from(roomConnectedInfo.getSession());
        if (from != null) {
            this.h = from;
            String peerId = roomConnectedInfo.getPeerId();
            String remotePeerId = roomConnectedInfo.getRemotePeerId();
            WCClient o = o();
            WCSession wCSession = this.h;
            if (wCSession == null) {
                fs1.r("wcSession");
                wCSession = null;
            }
            o.connect(wCSession, this.g, peerId, remotePeerId);
            return;
        }
        throw new InvalidSessionException();
    }

    public final void v(long j) {
        o().rejectRequest(j, "User canceled");
    }
}
