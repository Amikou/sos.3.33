package net.safemoon.androidwallet.viewmodels.wc;

import kotlin.jvm.internal.Lambda;

/* compiled from: WalletConnect.kt */
/* loaded from: classes2.dex */
public final class WalletConnect$credentails$2 extends Lambda implements rc1<ma0> {
    public final /* synthetic */ WalletConnect this$0;

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public WalletConnect$credentails$2(WalletConnect walletConnect) {
        super(0);
        this.this$0 = walletConnect;
    }

    @Override // defpackage.rc1
    public final ma0 invoke() {
        return ma0.create(w.d(this.this$0.k(), this.this$0.n().getPrivateKey()));
    }
}
