package net.safemoon.androidwallet.viewmodels.wc;

import android.content.DialogInterface;
import java.lang.ref.WeakReference;
import kotlin.coroutines.jvm.internal.SuspendLambda;
import kotlin.coroutines.jvm.internal.a;
import kotlinx.coroutines.CoroutineDispatcher;
import net.safemoon.androidwallet.common.TokenType;
import net.safemoon.androidwallet.dialogs.WalletConnectSmartContractCall;
import net.safemoon.androidwallet.viewmodels.wc.MultipleWalletConnect;

/* compiled from: MultipleWalletConnect.kt */
@a(c = "net.safemoon.androidwallet.viewmodels.wc.MultipleWalletConnect$4$1$1$1$onApprove$1", f = "MultipleWalletConnect.kt", l = {88, 91}, m = "invokeSuspend")
/* loaded from: classes2.dex */
public final class MultipleWalletConnect$4$1$1$1$onApprove$1 extends SuspendLambda implements hd1<c90, q70<? super te4>, Object> {
    public final /* synthetic */ MultipleWalletConnect.c $it;
    public final /* synthetic */ WalletConnectSmartContractCall $this_apply;
    public int label;
    public final /* synthetic */ MultipleWalletConnect this$0;

    /* compiled from: MultipleWalletConnect.kt */
    @a(c = "net.safemoon.androidwallet.viewmodels.wc.MultipleWalletConnect$4$1$1$1$onApprove$1$1", f = "MultipleWalletConnect.kt", l = {}, m = "invokeSuspend")
    /* renamed from: net.safemoon.androidwallet.viewmodels.wc.MultipleWalletConnect$4$1$1$1$onApprove$1$1  reason: invalid class name */
    /* loaded from: classes2.dex */
    public static final class AnonymousClass1 extends SuspendLambda implements hd1<c90, q70<? super te4>, Object> {
        public final /* synthetic */ hx0 $ethTransaction;
        public final /* synthetic */ MultipleWalletConnect.c $it;
        public final /* synthetic */ WalletConnectSmartContractCall $this_apply;
        public int label;

        /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
        public AnonymousClass1(hx0 hx0Var, WalletConnectSmartContractCall walletConnectSmartContractCall, MultipleWalletConnect.c cVar, q70<? super AnonymousClass1> q70Var) {
            super(2, q70Var);
            this.$ethTransaction = hx0Var;
            this.$this_apply = walletConnectSmartContractCall;
            this.$it = cVar;
        }

        public static final void e(DialogInterface dialogInterface) {
        }

        @Override // kotlin.coroutines.jvm.internal.BaseContinuationImpl
        public final q70<te4> create(Object obj, q70<?> q70Var) {
            return new AnonymousClass1(this.$ethTransaction, this.$this_apply, this.$it, q70Var);
        }

        @Override // defpackage.hd1
        public final Object invoke(c90 c90Var, q70<? super te4> q70Var) {
            return ((AnonymousClass1) create(c90Var, q70Var)).invokeSuspend(te4.a);
        }

        @Override // kotlin.coroutines.jvm.internal.BaseContinuationImpl
        public final Object invokeSuspend(Object obj) {
            gs1.d();
            if (this.label == 0) {
                o83.b(obj);
                hx0 hx0Var = this.$ethTransaction;
                if (hx0Var != null) {
                    MultipleWalletConnect.c cVar = this.$it;
                    WalletConnectSmartContractCall walletConnectSmartContractCall = this.$this_apply;
                    if (hx0Var.getError() == null && hx0Var.getTransactionHash() != null) {
                        fo0.e(new WeakReference(walletConnectSmartContractCall.requireActivity()), fs1.l(b30.a.o(TokenType.Companion.b(cVar.c().getChainId())), hx0Var.getTransactionHash()), db2.a);
                    }
                }
                this.$this_apply.h();
                return te4.a;
            }
            throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        }
    }

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public MultipleWalletConnect$4$1$1$1$onApprove$1(MultipleWalletConnect multipleWalletConnect, MultipleWalletConnect.c cVar, WalletConnectSmartContractCall walletConnectSmartContractCall, q70<? super MultipleWalletConnect$4$1$1$1$onApprove$1> q70Var) {
        super(2, q70Var);
        this.this$0 = multipleWalletConnect;
        this.$it = cVar;
        this.$this_apply = walletConnectSmartContractCall;
    }

    @Override // kotlin.coroutines.jvm.internal.BaseContinuationImpl
    public final q70<te4> create(Object obj, q70<?> q70Var) {
        return new MultipleWalletConnect$4$1$1$1$onApprove$1(this.this$0, this.$it, this.$this_apply, q70Var);
    }

    @Override // defpackage.hd1
    public final Object invoke(c90 c90Var, q70<? super te4> q70Var) {
        return ((MultipleWalletConnect$4$1$1$1$onApprove$1) create(c90Var, q70Var)).invokeSuspend(te4.a);
    }

    @Override // kotlin.coroutines.jvm.internal.BaseContinuationImpl
    public final Object invokeSuspend(Object obj) {
        Object d = gs1.d();
        int i = this.label;
        if (i == 0) {
            o83.b(obj);
            CoroutineDispatcher b = tp0.b();
            MultipleWalletConnect$4$1$1$1$onApprove$1$ethTransaction$1 multipleWalletConnect$4$1$1$1$onApprove$1$ethTransaction$1 = new MultipleWalletConnect$4$1$1$1$onApprove$1$ethTransaction$1(this.this$0, this.$it, null);
            this.label = 1;
            obj = kotlinx.coroutines.a.e(b, multipleWalletConnect$4$1$1$1$onApprove$1$ethTransaction$1, this);
            if (obj == d) {
                return d;
            }
        } else if (i != 1) {
            if (i != 2) {
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
            o83.b(obj);
            return te4.a;
        } else {
            o83.b(obj);
        }
        e32 c = tp0.c();
        AnonymousClass1 anonymousClass1 = new AnonymousClass1((hx0) obj, this.$this_apply, this.$it, null);
        this.label = 2;
        if (kotlinx.coroutines.a.e(c, anonymousClass1, this) == d) {
            return d;
        }
        return te4.a;
    }
}
