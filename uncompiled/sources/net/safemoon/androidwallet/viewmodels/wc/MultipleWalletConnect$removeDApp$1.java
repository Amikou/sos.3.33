package net.safemoon.androidwallet.viewmodels.wc;

import kotlin.coroutines.jvm.internal.SuspendLambda;
import kotlin.coroutines.jvm.internal.a;
import net.safemoon.androidwallet.model.walletConnect.RoomConnectedInfo;

/* compiled from: MultipleWalletConnect.kt */
@a(c = "net.safemoon.androidwallet.viewmodels.wc.MultipleWalletConnect$removeDApp$1", f = "MultipleWalletConnect.kt", l = {176}, m = "invokeSuspend")
/* loaded from: classes2.dex */
public final class MultipleWalletConnect$removeDApp$1 extends SuspendLambda implements hd1<c90, q70<? super te4>, Object> {
    public final /* synthetic */ RoomConnectedInfo $rc;
    public int label;
    public final /* synthetic */ MultipleWalletConnect this$0;

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public MultipleWalletConnect$removeDApp$1(MultipleWalletConnect multipleWalletConnect, RoomConnectedInfo roomConnectedInfo, q70<? super MultipleWalletConnect$removeDApp$1> q70Var) {
        super(2, q70Var);
        this.this$0 = multipleWalletConnect;
        this.$rc = roomConnectedInfo;
    }

    @Override // kotlin.coroutines.jvm.internal.BaseContinuationImpl
    public final q70<te4> create(Object obj, q70<?> q70Var) {
        return new MultipleWalletConnect$removeDApp$1(this.this$0, this.$rc, q70Var);
    }

    @Override // defpackage.hd1
    public final Object invoke(c90 c90Var, q70<? super te4> q70Var) {
        return ((MultipleWalletConnect$removeDApp$1) create(c90Var, q70Var)).invokeSuspend(te4.a);
    }

    @Override // kotlin.coroutines.jvm.internal.BaseContinuationImpl
    public final Object invokeSuspend(Object obj) {
        t50 s;
        Object d = gs1.d();
        int i = this.label;
        try {
            if (i == 0) {
                o83.b(obj);
                s = this.this$0.s();
                String session = this.$rc.getSession();
                this.label = 1;
                if (s.b(session, this) == d) {
                    return d;
                }
            } else if (i != 1) {
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            } else {
                o83.b(obj);
            }
        } catch (Exception unused) {
        }
        return te4.a;
    }
}
