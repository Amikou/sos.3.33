package net.safemoon.androidwallet.viewmodels.wc;

import kotlin.jvm.internal.Lambda;
import net.safemoon.androidwallet.database.mainRoom.MainRoomDatabase;

/* compiled from: MultipleWalletConnect.kt */
/* loaded from: classes2.dex */
public final class MultipleWalletConnect$walletConnectDao$2 extends Lambda implements rc1<t50> {
    public final /* synthetic */ MultipleWalletConnect this$0;

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public MultipleWalletConnect$walletConnectDao$2(MultipleWalletConnect multipleWalletConnect) {
        super(0);
        this.this$0 = multipleWalletConnect;
    }

    @Override // defpackage.rc1
    public final t50 invoke() {
        return MainRoomDatabase.n.b(this.this$0.r()).N();
    }
}
