package net.safemoon.androidwallet.viewmodels.wc;

import android.app.Activity;
import com.trustwallet.walletconnect.models.ethereum.WCEthereumTransaction;
import java.math.BigInteger;
import net.safemoon.androidwallet.model.wallets.Wallet;
import net.safemoon.androidwallet.viewmodels.blockChainClass.WalletWeb3;

/* compiled from: WcWeb3.kt */
/* loaded from: classes2.dex */
public final class WcWeb3 extends WalletWeb3 {
    public final int i;
    public final Wallet j;

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public WcWeb3(Activity activity, int i, Wallet wallet2) {
        super(i, wallet2);
        fs1.f(activity, "activity");
        fs1.f(wallet2, "wallet");
        this.i = i;
        this.j = wallet2;
    }

    /* JADX WARN: Removed duplicated region for block: B:10:0x0024  */
    /* JADX WARN: Removed duplicated region for block: B:14:0x0036  */
    /*
        Code decompiled incorrectly, please refer to instructions dump.
        To view partially-correct code enable 'Show inconsistent code' option in preferences
    */
    public final java.lang.Object A(com.trustwallet.walletconnect.models.ethereum.WCEthereumTransaction r8, defpackage.q70<? super java.math.BigInteger> r9) {
        /*
            r7 = this;
            boolean r0 = r9 instanceof net.safemoon.androidwallet.viewmodels.wc.WcWeb3$fetchGasPrice$1
            if (r0 == 0) goto L13
            r0 = r9
            net.safemoon.androidwallet.viewmodels.wc.WcWeb3$fetchGasPrice$1 r0 = (net.safemoon.androidwallet.viewmodels.wc.WcWeb3$fetchGasPrice$1) r0
            int r1 = r0.label
            r2 = -2147483648(0xffffffff80000000, float:-0.0)
            r3 = r1 & r2
            if (r3 == 0) goto L13
            int r1 = r1 - r2
            r0.label = r1
            goto L18
        L13:
            net.safemoon.androidwallet.viewmodels.wc.WcWeb3$fetchGasPrice$1 r0 = new net.safemoon.androidwallet.viewmodels.wc.WcWeb3$fetchGasPrice$1
            r0.<init>(r7, r9)
        L18:
            r4 = r0
            java.lang.Object r9 = r4.result
            java.lang.Object r0 = defpackage.gs1.d()
            int r1 = r4.label
            r2 = 1
            if (r1 == 0) goto L36
            if (r1 != r2) goto L2e
            java.lang.Object r8 = r4.L$0
            net.safemoon.androidwallet.viewmodels.wc.WcWeb3 r8 = (net.safemoon.androidwallet.viewmodels.wc.WcWeb3) r8
            defpackage.o83.b(r9)
            goto L60
        L2e:
            java.lang.IllegalStateException r8 = new java.lang.IllegalStateException
            java.lang.String r9 = "call to 'resume' before 'invoke' with coroutine"
            r8.<init>(r9)
            throw r8
        L36:
            defpackage.o83.b(r9)
            java.lang.String r9 = r8.getGasPrice()
            if (r9 == 0) goto L48
            java.lang.String r8 = r8.getGasPrice()
            java.math.BigInteger r8 = defpackage.ej2.decodeQuantity(r8)
            goto L64
        L48:
            net.safemoon.androidwallet.model.common.GasPrice r1 = r7.n()
            int r8 = r7.i()
            r3 = 0
            r5 = 2
            r6 = 0
            r4.L$0 = r7
            r4.label = r2
            r2 = r8
            java.lang.Object r8 = net.safemoon.androidwallet.model.common.GasPrice.fetchGasSuspended$default(r1, r2, r3, r4, r5, r6)
            if (r8 != r0) goto L5f
            return r0
        L5f:
            r8 = r7
        L60:
            java.math.BigInteger r8 = r8.getGasPrice()
        L64:
            java.lang.String r9 = "payload.run {\n          …getGasPrice() }\n        }"
            defpackage.fs1.e(r8, r9)
            return r8
        */
        throw new UnsupportedOperationException("Method not decompiled: net.safemoon.androidwallet.viewmodels.wc.WcWeb3.A(com.trustwallet.walletconnect.models.ethereum.WCEthereumTransaction, q70):java.lang.Object");
    }

    /* JADX WARN: Removed duplicated region for block: B:10:0x0024  */
    /* JADX WARN: Removed duplicated region for block: B:16:0x005f  */
    /* JADX WARN: Removed duplicated region for block: B:26:0x0091  */
    /* JADX WARN: Removed duplicated region for block: B:27:0x009a  */
    /* JADX WARN: Removed duplicated region for block: B:30:0x00ae A[RETURN] */
    /* JADX WARN: Removed duplicated region for block: B:31:0x00af  */
    /*
        Code decompiled incorrectly, please refer to instructions dump.
        To view partially-correct code enable 'Show inconsistent code' option in preferences
    */
    public final java.lang.Object B(com.trustwallet.walletconnect.models.ethereum.WCEthereumTransaction r14, defpackage.q70<? super defpackage.hx0> r15) {
        /*
            Method dump skipped, instructions count: 233
            To view this dump change 'Code comments level' option to 'DEBUG'
        */
        throw new UnsupportedOperationException("Method not decompiled: net.safemoon.androidwallet.viewmodels.wc.WcWeb3.B(com.trustwallet.walletconnect.models.ethereum.WCEthereumTransaction, q70):java.lang.Object");
    }

    @Override // net.safemoon.androidwallet.viewmodels.blockChainClass.WalletWeb3
    public int i() {
        return this.i;
    }

    @Override // net.safemoon.androidwallet.viewmodels.blockChainClass.WalletWeb3
    public Wallet u() {
        return this.j;
    }

    public final Object z(WCEthereumTransaction wCEthereumTransaction, q70<? super BigInteger> q70Var) {
        BigInteger valueOf;
        if (wCEthereumTransaction.getGasLimit() != null) {
            valueOf = ej2.decodeQuantity(wCEthereumTransaction.getGasLimit());
        } else if (wCEthereumTransaction.getGas() != null) {
            valueOf = ej2.decodeQuantity(wCEthereumTransaction.getGas());
        } else {
            ow0 send = y().ethEstimateGas(p84.createEthCallTransaction(wCEthereumTransaction.getFrom(), wCEthereumTransaction.getTo(), wCEthereumTransaction.getData())).send();
            if (send.getError() == null) {
                valueOf = send.getAmountUsed();
            } else {
                valueOf = BigInteger.valueOf(90000L);
            }
        }
        fs1.e(valueOf, "payload.run {\n          …)\n            }\n        }");
        return valueOf;
    }
}
