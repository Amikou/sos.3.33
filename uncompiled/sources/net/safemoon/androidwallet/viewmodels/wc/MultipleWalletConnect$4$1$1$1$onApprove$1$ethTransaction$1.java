package net.safemoon.androidwallet.viewmodels.wc;

import com.trustwallet.walletconnect.models.ethereum.WCEthereumTransaction;
import kotlin.coroutines.jvm.internal.SuspendLambda;
import kotlin.coroutines.jvm.internal.a;
import net.safemoon.androidwallet.model.walletConnect.RoomConnectedInfo;
import net.safemoon.androidwallet.viewmodels.wc.MultipleWalletConnect;

/* compiled from: MultipleWalletConnect.kt */
@a(c = "net.safemoon.androidwallet.viewmodels.wc.MultipleWalletConnect$4$1$1$1$onApprove$1$ethTransaction$1", f = "MultipleWalletConnect.kt", l = {89}, m = "invokeSuspend")
/* loaded from: classes2.dex */
public final class MultipleWalletConnect$4$1$1$1$onApprove$1$ethTransaction$1 extends SuspendLambda implements hd1<c90, q70<? super hx0>, Object> {
    public final /* synthetic */ MultipleWalletConnect.c $it;
    public int label;
    public final /* synthetic */ MultipleWalletConnect this$0;

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public MultipleWalletConnect$4$1$1$1$onApprove$1$ethTransaction$1(MultipleWalletConnect multipleWalletConnect, MultipleWalletConnect.c cVar, q70<? super MultipleWalletConnect$4$1$1$1$onApprove$1$ethTransaction$1> q70Var) {
        super(2, q70Var);
        this.this$0 = multipleWalletConnect;
        this.$it = cVar;
    }

    @Override // kotlin.coroutines.jvm.internal.BaseContinuationImpl
    public final q70<te4> create(Object obj, q70<?> q70Var) {
        return new MultipleWalletConnect$4$1$1$1$onApprove$1$ethTransaction$1(this.this$0, this.$it, q70Var);
    }

    @Override // defpackage.hd1
    public final Object invoke(c90 c90Var, q70<? super hx0> q70Var) {
        return ((MultipleWalletConnect$4$1$1$1$onApprove$1$ethTransaction$1) create(c90Var, q70Var)).invokeSuspend(te4.a);
    }

    @Override // kotlin.coroutines.jvm.internal.BaseContinuationImpl
    public final Object invokeSuspend(Object obj) {
        Object d = gs1.d();
        int i = this.label;
        if (i == 0) {
            o83.b(obj);
            WalletConnect walletConnect = (WalletConnect) this.this$0.c.get(this.$it.c().getPeerId());
            if (walletConnect == null) {
                return null;
            }
            RoomConnectedInfo c = this.$it.c();
            long a = this.$it.a();
            WCEthereumTransaction b = this.$it.b();
            this.label = 1;
            obj = WalletConnect.j(walletConnect, c, a, b, false, this, 8, null);
            if (obj == d) {
                return d;
            }
        } else if (i != 1) {
            throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        } else {
            o83.b(obj);
        }
        return (hx0) obj;
    }
}
