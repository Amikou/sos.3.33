package net.safemoon.androidwallet.viewmodels.wc;

import com.trustwallet.walletconnect.models.WCPeerMeta;
import kotlin.jvm.internal.Lambda;

/* compiled from: WalletConnect.kt */
/* loaded from: classes2.dex */
public final class WalletConnect$wcClient$2$1$1 extends Lambda implements hd1<Long, WCPeerMeta, te4> {
    public final /* synthetic */ WalletConnect this$0;

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public WalletConnect$wcClient$2$1$1(WalletConnect walletConnect) {
        super(2);
        this.this$0 = walletConnect;
    }

    @Override // defpackage.hd1
    public /* bridge */ /* synthetic */ te4 invoke(Long l, WCPeerMeta wCPeerMeta) {
        invoke(l.longValue(), wCPeerMeta);
        return te4.a;
    }

    public final void invoke(long j, WCPeerMeta wCPeerMeta) {
        fs1.f(wCPeerMeta, "peer");
        this.this$0.i = wCPeerMeta;
        this.this$0.t(wCPeerMeta);
    }
}
