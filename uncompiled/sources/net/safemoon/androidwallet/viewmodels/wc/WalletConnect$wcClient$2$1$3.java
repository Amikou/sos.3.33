package net.safemoon.androidwallet.viewmodels.wc;

import kotlin.jvm.internal.Lambda;

/* compiled from: WalletConnect.kt */
/* loaded from: classes2.dex */
public final class WalletConnect$wcClient$2$1$3 extends Lambda implements tc1<Throwable, te4> {
    public static final WalletConnect$wcClient$2$1$3 INSTANCE = new WalletConnect$wcClient$2$1$3();

    public WalletConnect$wcClient$2$1$3() {
        super(1);
    }

    @Override // defpackage.tc1
    public /* bridge */ /* synthetic */ te4 invoke(Throwable th) {
        invoke2(th);
        return te4.a;
    }

    /* renamed from: invoke  reason: avoid collision after fix types in other method */
    public final void invoke2(Throwable th) {
        fs1.f(th, "t");
        String th2 = th.toString();
        String canonicalName = WalletConnect.class.getCanonicalName();
        fs1.e(canonicalName, "WalletConnect::class.java.canonicalName");
        e30.c0(th2, canonicalName);
    }
}
