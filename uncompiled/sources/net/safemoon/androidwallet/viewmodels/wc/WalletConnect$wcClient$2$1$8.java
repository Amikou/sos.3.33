package net.safemoon.androidwallet.viewmodels.wc;

import com.trustwallet.walletconnect.models.WCSignTransaction;
import kotlin.jvm.internal.Lambda;

/* compiled from: WalletConnect.kt */
/* loaded from: classes2.dex */
public final class WalletConnect$wcClient$2$1$8 extends Lambda implements hd1<Long, WCSignTransaction, te4> {
    public static final WalletConnect$wcClient$2$1$8 INSTANCE = new WalletConnect$wcClient$2$1$8();

    public WalletConnect$wcClient$2$1$8() {
        super(2);
    }

    @Override // defpackage.hd1
    public /* bridge */ /* synthetic */ te4 invoke(Long l, WCSignTransaction wCSignTransaction) {
        invoke(l.longValue(), wCSignTransaction);
        return te4.a;
    }

    public final void invoke(long j, WCSignTransaction wCSignTransaction) {
        fs1.f(wCSignTransaction, "transaction");
        String valueOf = String.valueOf(j);
        String canonicalName = WalletConnect.class.getCanonicalName();
        fs1.e(canonicalName, "WalletConnect::class.java.canonicalName");
        e30.c0(valueOf, canonicalName);
    }
}
