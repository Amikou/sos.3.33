package net.safemoon.androidwallet.viewmodels.wc;

import kotlin.coroutines.jvm.internal.ContinuationImpl;
import kotlin.coroutines.jvm.internal.a;

/* compiled from: WcWeb3.kt */
@a(c = "net.safemoon.androidwallet.viewmodels.wc.WcWeb3", f = "WcWeb3.kt", l = {29, 32}, m = "sendTransaction")
/* loaded from: classes2.dex */
public final class WcWeb3$sendTransaction$1 extends ContinuationImpl {
    public Object L$0;
    public Object L$1;
    public Object L$2;
    public Object L$3;
    public Object L$4;
    public int label;
    public /* synthetic */ Object result;
    public final /* synthetic */ WcWeb3 this$0;

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public WcWeb3$sendTransaction$1(WcWeb3 wcWeb3, q70<? super WcWeb3$sendTransaction$1> q70Var) {
        super(q70Var);
        this.this$0 = wcWeb3;
    }

    @Override // kotlin.coroutines.jvm.internal.BaseContinuationImpl
    public final Object invokeSuspend(Object obj) {
        this.result = obj;
        this.label |= Integer.MIN_VALUE;
        return this.this$0.B(null, this);
    }
}
