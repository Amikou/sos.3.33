package net.safemoon.androidwallet.viewmodels.wc;

import kotlin.coroutines.jvm.internal.ContinuationImpl;
import kotlin.coroutines.jvm.internal.a;

/* compiled from: WalletConnect.kt */
@a(c = "net.safemoon.androidwallet.viewmodels.wc.WalletConnect", f = "WalletConnect.kt", l = {194}, m = "ethTransaction")
/* loaded from: classes2.dex */
public final class WalletConnect$ethTransaction$1 extends ContinuationImpl {
    public long J$0;
    public Object L$0;
    public int label;
    public /* synthetic */ Object result;
    public final /* synthetic */ WalletConnect this$0;

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public WalletConnect$ethTransaction$1(WalletConnect walletConnect, q70<? super WalletConnect$ethTransaction$1> q70Var) {
        super(q70Var);
        this.this$0 = walletConnect;
    }

    @Override // kotlin.coroutines.jvm.internal.BaseContinuationImpl
    public final Object invokeSuspend(Object obj) {
        this.result = obj;
        this.label |= Integer.MIN_VALUE;
        return this.this$0.i(null, 0L, null, false, this);
    }
}
