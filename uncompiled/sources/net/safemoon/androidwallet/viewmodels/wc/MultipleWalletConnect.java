package net.safemoon.androidwallet.viewmodels.wc;

import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.FragmentManager;
import androidx.lifecycle.LiveData;
import com.fasterxml.jackson.databind.deser.std.ThrowableDeserializer;
import com.trustwallet.walletconnect.models.ethereum.WCEthereumSignMessage;
import com.trustwallet.walletconnect.models.ethereum.WCEthereumTransaction;
import defpackage.jm4;
import java.util.ArrayList;
import java.util.Date;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.TimeUnit;
import net.safemoon.androidwallet.dialogs.WalletConnectSmartContractCall;
import net.safemoon.androidwallet.model.walletConnect.RoomConnectedInfo;
import net.safemoon.androidwallet.model.walletConnect.RoomConnectedInfoAndWallet;
import net.safemoon.androidwallet.model.wallets.Wallet;
import net.safemoon.androidwallet.receivers.ConnectionLiveData;
import net.safemoon.androidwallet.viewmodels.wc.MultipleWalletConnect;

/* compiled from: MultipleWalletConnect.kt */
/* loaded from: classes2.dex */
public final class MultipleWalletConnect {
    public final AppCompatActivity a;
    public final sy1 b;
    public final Map<String, WalletConnect> c;
    public final c90 d;
    public final c90 e;
    public List<RoomConnectedInfoAndWallet> f;
    public final gb2<b> g;
    public final gb2<c> h;

    /* compiled from: MultipleWalletConnect.kt */
    /* loaded from: classes2.dex */
    public static final class a implements jm4.b {
        public final /* synthetic */ b b;
        public final /* synthetic */ jm4 c;

        public a(b bVar, jm4 jm4Var) {
            this.b = bVar;
            this.c = jm4Var;
        }

        @Override // defpackage.jm4.b
        public void a() {
            WalletConnect walletConnect = (WalletConnect) MultipleWalletConnect.this.c.get(this.b.c().getPeerId());
            if (walletConnect == null) {
                return;
            }
            walletConnect.v(this.b.a());
        }

        @Override // defpackage.jm4.b
        public void b() {
            try {
                WalletConnect walletConnect = (WalletConnect) MultipleWalletConnect.this.c.get(this.b.c().getPeerId());
                if (walletConnect != null) {
                    walletConnect.g(this.b.a(), this.b.b());
                }
            } catch (Exception unused) {
            } catch (Throwable th) {
                this.c.h();
                throw th;
            }
            this.c.h();
        }
    }

    /* compiled from: MultipleWalletConnect.kt */
    /* loaded from: classes2.dex */
    public static final class b {
        public final RoomConnectedInfo a;
        public final long b;
        public final WCEthereumSignMessage c;
        public final Wallet d;

        public b(RoomConnectedInfo roomConnectedInfo, long j, WCEthereumSignMessage wCEthereumSignMessage, Wallet wallet2) {
            fs1.f(roomConnectedInfo, "rc");
            fs1.f(wCEthereumSignMessage, ThrowableDeserializer.PROP_NAME_MESSAGE);
            fs1.f(wallet2, "wallet");
            this.a = roomConnectedInfo;
            this.b = j;
            this.c = wCEthereumSignMessage;
            this.d = wallet2;
        }

        public final long a() {
            return this.b;
        }

        public final WCEthereumSignMessage b() {
            return this.c;
        }

        public final RoomConnectedInfo c() {
            return this.a;
        }

        public final Wallet d() {
            return this.d;
        }

        public boolean equals(Object obj) {
            if (this == obj) {
                return true;
            }
            if (obj instanceof b) {
                b bVar = (b) obj;
                return fs1.b(this.a, bVar.a) && this.b == bVar.b && fs1.b(this.c, bVar.c) && fs1.b(this.d, bVar.d);
            }
            return false;
        }

        public int hashCode() {
            return (((((this.a.hashCode() * 31) + p80.a(this.b)) * 31) + this.c.hashCode()) * 31) + this.d.hashCode();
        }

        public String toString() {
            return "EthSign(rc=" + this.a + ", id=" + this.b + ", message=" + this.c + ", wallet=" + this.d + ')';
        }
    }

    /* compiled from: MultipleWalletConnect.kt */
    /* loaded from: classes2.dex */
    public static final class c {
        public final RoomConnectedInfo a;
        public final long b;
        public final WCEthereumTransaction c;
        public final Wallet d;

        public c(RoomConnectedInfo roomConnectedInfo, long j, WCEthereumTransaction wCEthereumTransaction, Wallet wallet2) {
            fs1.f(roomConnectedInfo, "rc");
            fs1.f(wCEthereumTransaction, ThrowableDeserializer.PROP_NAME_MESSAGE);
            fs1.f(wallet2, "wallet");
            this.a = roomConnectedInfo;
            this.b = j;
            this.c = wCEthereumTransaction;
            this.d = wallet2;
        }

        public final long a() {
            return this.b;
        }

        public final WCEthereumTransaction b() {
            return this.c;
        }

        public final RoomConnectedInfo c() {
            return this.a;
        }

        public final Wallet d() {
            return this.d;
        }

        public boolean equals(Object obj) {
            if (this == obj) {
                return true;
            }
            if (obj instanceof c) {
                c cVar = (c) obj;
                return fs1.b(this.a, cVar.a) && this.b == cVar.b && fs1.b(this.c, cVar.c) && fs1.b(this.d, cVar.d);
            }
            return false;
        }

        public int hashCode() {
            return (((((this.a.hashCode() * 31) + p80.a(this.b)) * 31) + this.c.hashCode()) * 31) + this.d.hashCode();
        }

        public String toString() {
            return "EthSmartContract(rc=" + this.a + ", id=" + this.b + ", message=" + this.c + ", wallet=" + this.d + ')';
        }
    }

    /* compiled from: MultipleWalletConnect.kt */
    /* loaded from: classes2.dex */
    public static final class d extends el4 {
        public final /* synthetic */ RoomConnectedInfo k;
        public final /* synthetic */ RoomConnectedInfoAndWallet l;

        /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
        public d(RoomConnectedInfo roomConnectedInfo, RoomConnectedInfoAndWallet roomConnectedInfoAndWallet, AppCompatActivity appCompatActivity, Wallet wallet2) {
            super(appCompatActivity, wallet2);
            this.k = roomConnectedInfo;
            this.l = roomConnectedInfoAndWallet;
        }

        @Override // defpackage.el4, net.safemoon.androidwallet.viewmodels.wc.WalletConnect
        public void q(int i, String str) {
            fs1.f(str, ThrowableDeserializer.PROP_NAME_MESSAGE);
            super.q(i, str);
            MultipleWalletConnect.this.w(this.k);
            MultipleWalletConnect.this.c.remove(this.k.getPeerId());
        }

        @Override // defpackage.el4, net.safemoon.androidwallet.viewmodels.wc.WalletConnect
        public void r(long j, WCEthereumSignMessage wCEthereumSignMessage) {
            fs1.f(wCEthereumSignMessage, ThrowableDeserializer.PROP_NAME_MESSAGE);
            super.r(j, wCEthereumSignMessage);
            MultipleWalletConnect.this.t(this.k, j, wCEthereumSignMessage, this.l.getWallet());
        }

        @Override // defpackage.el4, net.safemoon.androidwallet.viewmodels.wc.WalletConnect
        public void s(long j, WCEthereumTransaction wCEthereumTransaction) {
            fs1.f(wCEthereumTransaction, "transaction");
            super.s(j, wCEthereumTransaction);
            MultipleWalletConnect.this.u(this.k, j, wCEthereumTransaction, this.l.getWallet());
        }
    }

    public MultipleWalletConnect(AppCompatActivity appCompatActivity) {
        fs1.f(appCompatActivity, "activity");
        this.a = appCompatActivity;
        this.b = zy1.a(new MultipleWalletConnect$walletConnectDao$2(this));
        this.c = new LinkedHashMap();
        this.d = d90.a(tp0.c());
        this.e = d90.a(tp0.b());
        this.f = new ArrayList();
        gb2<b> gb2Var = new gb2<>(null);
        this.g = gb2Var;
        gb2<c> gb2Var2 = new gb2<>(null);
        this.h = gb2Var2;
        LiveData a2 = cb4.a(s().c());
        fs1.e(a2, "Transformations.distinctUntilChanged(this)");
        a2.observe(appCompatActivity, new tl2() { // from class: za2
            @Override // defpackage.tl2
            public final void onChanged(Object obj) {
                MultipleWalletConnect.f(MultipleWalletConnect.this, (List) obj);
            }
        });
        new ConnectionLiveData(appCompatActivity).observe(appCompatActivity, new tl2() { // from class: ya2
            @Override // defpackage.tl2
            public final void onChanged(Object obj) {
                MultipleWalletConnect.g(MultipleWalletConnect.this, (Boolean) obj);
            }
        });
        gb2Var.observe(appCompatActivity, new tl2() { // from class: ab2
            @Override // defpackage.tl2
            public final void onChanged(Object obj) {
                MultipleWalletConnect.h(MultipleWalletConnect.this, (MultipleWalletConnect.b) obj);
            }
        });
        gb2Var2.observe(appCompatActivity, new tl2() { // from class: bb2
            @Override // defpackage.tl2
            public final void onChanged(Object obj) {
                MultipleWalletConnect.i(MultipleWalletConnect.this, (MultipleWalletConnect.c) obj);
            }
        });
    }

    public static final void f(MultipleWalletConnect multipleWalletConnect, List list) {
        fs1.f(multipleWalletConnect, "this$0");
        List I = list == null ? null : j20.I(list);
        multipleWalletConnect.f.clear();
        List<RoomConnectedInfoAndWallet> list2 = multipleWalletConnect.f;
        fs1.d(I);
        list2.addAll(I);
        multipleWalletConnect.o();
    }

    public static final void g(MultipleWalletConnect multipleWalletConnect, Boolean bool) {
        fs1.f(multipleWalletConnect, "this$0");
        fs1.e(bool, "it");
        if (bool.booleanValue()) {
            multipleWalletConnect.o();
            return;
        }
        for (Map.Entry<String, WalletConnect> entry : multipleWalletConnect.c.entrySet()) {
            entry.getValue().f();
        }
        multipleWalletConnect.c.clear();
    }

    public static final void h(MultipleWalletConnect multipleWalletConnect, b bVar) {
        fs1.f(multipleWalletConnect, "this$0");
        if (bVar == null) {
            return;
        }
        multipleWalletConnect.g.postValue(null);
        jm4 a2 = jm4.B0.a();
        a2.J(bVar.c(), bVar.a(), bVar.b(), bVar.d(), new a(bVar, a2));
        FragmentManager supportFragmentManager = multipleWalletConnect.r().getSupportFragmentManager();
        fs1.e(supportFragmentManager, "activity.supportFragmentManager");
        a2.K(supportFragmentManager);
    }

    public static final void i(final MultipleWalletConnect multipleWalletConnect, final c cVar) {
        fs1.f(multipleWalletConnect, "this$0");
        if (cVar == null) {
            return;
        }
        multipleWalletConnect.h.postValue(null);
        final WalletConnectSmartContractCall a2 = WalletConnectSmartContractCall.C0.a();
        a2.R(cVar.c(), cVar.a(), cVar.b(), cVar.d(), new WalletConnectSmartContractCall.b() { // from class: net.safemoon.androidwallet.viewmodels.wc.MultipleWalletConnect$4$1$1$1
            @Override // net.safemoon.androidwallet.dialogs.WalletConnectSmartContractCall.b
            public void a() {
                WalletConnect walletConnect = (WalletConnect) MultipleWalletConnect.this.c.get(cVar.c().getPeerId());
                if (walletConnect == null) {
                    return;
                }
                walletConnect.v(cVar.a());
            }

            @Override // net.safemoon.androidwallet.dialogs.WalletConnectSmartContractCall.b
            public void b() {
                as.b(qg1.a, null, null, new MultipleWalletConnect$4$1$1$1$onApprove$1(MultipleWalletConnect.this, cVar, a2, null), 3, null);
            }
        });
        FragmentManager supportFragmentManager = multipleWalletConnect.r().getSupportFragmentManager();
        fs1.e(supportFragmentManager, "activity.supportFragmentManager");
        a2.S(supportFragmentManager);
    }

    public static final void p(d dVar, MultipleWalletConnect multipleWalletConnect, RoomConnectedInfo roomConnectedInfo) {
        fs1.f(dVar, "$this_apply");
        fs1.f(multipleWalletConnect, "this$0");
        fs1.f(roomConnectedInfo, "$it");
        try {
            dVar.o().killSession();
            multipleWalletConnect.w(roomConnectedInfo);
            multipleWalletConnect.c.remove(roomConnectedInfo.getPeerId());
        } catch (Exception unused) {
        }
    }

    /* JADX WARN: Removed duplicated region for block: B:40:0x004d A[SYNTHETIC] */
    /* JADX WARN: Removed duplicated region for block: B:43:0x000b A[SYNTHETIC] */
    /*
        Code decompiled incorrectly, please refer to instructions dump.
        To view partially-correct code enable 'Show inconsistent code' option in preferences
    */
    public final void o() {
        /*
            r14 = this;
            java.util.List<net.safemoon.androidwallet.model.walletConnect.RoomConnectedInfoAndWallet> r0 = r14.f
            java.util.ArrayList r1 = new java.util.ArrayList
            r1.<init>()
            java.util.Iterator r0 = r0.iterator()
        Lb:
            boolean r2 = r0.hasNext()
            if (r2 == 0) goto L51
            java.lang.Object r2 = r0.next()
            r3 = r2
            net.safemoon.androidwallet.model.walletConnect.RoomConnectedInfoAndWallet r3 = (net.safemoon.androidwallet.model.walletConnect.RoomConnectedInfoAndWallet) r3
            net.safemoon.androidwallet.model.walletConnect.RoomConnectedInfo r4 = r3.getDApp()
            net.safemoon.androidwallet.model.wallets.Wallet r3 = r3.getWallet()
            if (r3 == 0) goto L4a
            java.util.Map<java.lang.String, net.safemoon.androidwallet.viewmodels.wc.WalletConnect> r3 = r14.c
            java.util.Set r3 = r3.keySet()
            java.util.Iterator r3 = r3.iterator()
        L2c:
            boolean r5 = r3.hasNext()
            if (r5 == 0) goto L44
            java.lang.Object r5 = r3.next()
            r6 = r5
            java.lang.String r6 = (java.lang.String) r6
            java.lang.String r7 = r4.getPeerId()
            boolean r6 = defpackage.fs1.b(r6, r7)
            if (r6 == 0) goto L2c
            goto L45
        L44:
            r5 = 0
        L45:
            if (r5 != 0) goto L48
            goto L4a
        L48:
            r3 = 0
            goto L4b
        L4a:
            r3 = 1
        L4b:
            if (r3 == 0) goto Lb
            r1.add(r2)
            goto Lb
        L51:
            java.util.Iterator r0 = r1.iterator()
        L55:
            boolean r1 = r0.hasNext()
            if (r1 == 0) goto Lf0
            java.lang.Object r1 = r0.next()
            net.safemoon.androidwallet.model.walletConnect.RoomConnectedInfoAndWallet r1 = (net.safemoon.androidwallet.model.walletConnect.RoomConnectedInfoAndWallet) r1
            net.safemoon.androidwallet.model.walletConnect.RoomConnectedInfo r8 = r1.getDApp()
            net.safemoon.androidwallet.model.wallets.Wallet r2 = r1.getWallet()
            if (r2 != 0) goto L6d
            goto Le5
        L6d:
            java.lang.String r2 = r8.getPeerMeta()
            java.lang.String r2 = java.lang.String.valueOf(r2)
            java.lang.String r3 = "RoomConnectedInfo"
            defpackage.e30.c0(r2, r3)
            java.util.Map<java.lang.String, net.safemoon.androidwallet.viewmodels.wc.WalletConnect> r9 = r14.c
            java.lang.String r10 = r8.getPeerId()
            androidx.appcompat.app.AppCompatActivity r6 = r14.r()
            net.safemoon.androidwallet.model.wallets.Wallet r7 = r1.getWallet()
            net.safemoon.androidwallet.viewmodels.wc.MultipleWalletConnect$d r11 = new net.safemoon.androidwallet.viewmodels.wc.MultipleWalletConnect$d
            r2 = r11
            r3 = r14
            r4 = r8
            r5 = r1
            r2.<init>(r4, r5, r6, r7)
            r11.u(r8)
            boolean r2 = r8.isAutoDisconnect()
            if (r2 == 0) goto Le2
            b30 r2 = defpackage.b30.a
            java.util.Date r3 = new java.util.Date
            r3.<init>()
            long r2 = r2.z(r3)
            long r4 = r8.getConnectedAtUnix()
            java.util.concurrent.TimeUnit r6 = java.util.concurrent.TimeUnit.MINUTES
            r12 = 30
            long r6 = r6.toSeconds(r12)
            long r4 = r4 + r6
            int r6 = (r4 > r2 ? 1 : (r4 == r2 ? 0 : -1))
            if (r6 <= 0) goto Lcf
            android.os.Handler r6 = new android.os.Handler
            android.os.Looper r7 = android.os.Looper.getMainLooper()
            r6.<init>(r7)
            cb2 r7 = new cb2
            r7.<init>()
            java.util.concurrent.TimeUnit r12 = java.util.concurrent.TimeUnit.SECONDS
            long r4 = r4 - r2
            long r2 = r12.toMillis(r4)
            r6.postDelayed(r7, r2)
            goto Le2
        Lcf:
            r14.w(r8)     // Catch: java.lang.Exception -> Le2
            com.trustwallet.walletconnect.WCClient r2 = r11.o()     // Catch: java.lang.Exception -> Le2
            r2.killSession()     // Catch: java.lang.Exception -> Le2
            java.util.Map<java.lang.String, net.safemoon.androidwallet.viewmodels.wc.WalletConnect> r2 = r14.c     // Catch: java.lang.Exception -> Le2
            java.lang.String r3 = r8.getPeerId()     // Catch: java.lang.Exception -> Le2
            r2.remove(r3)     // Catch: java.lang.Exception -> Le2
        Le2:
            r9.put(r10, r11)
        Le5:
            net.safemoon.androidwallet.model.wallets.Wallet r1 = r1.getWallet()
            if (r1 != 0) goto L55
            r14.w(r8)
            goto L55
        Lf0:
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: net.safemoon.androidwallet.viewmodels.wc.MultipleWalletConnect.o():void");
    }

    public final void q(RoomConnectedInfo roomConnectedInfo) {
        fs1.f(roomConnectedInfo, "roomConnectedInfo");
        WalletConnect walletConnect = this.c.get(roomConnectedInfo.getPeerId());
        if (walletConnect == null) {
            return;
        }
        walletConnect.p();
    }

    public final AppCompatActivity r() {
        return this.a;
    }

    public final t50 s() {
        return (t50) this.b.getValue();
    }

    public final void t(RoomConnectedInfo roomConnectedInfo, long j, WCEthereumSignMessage wCEthereumSignMessage, Wallet wallet2) {
        this.g.postValue(new b(roomConnectedInfo, j, wCEthereumSignMessage, wallet2));
    }

    public final void u(RoomConnectedInfo roomConnectedInfo, long j, WCEthereumTransaction wCEthereumTransaction, Wallet wallet2) {
        this.h.postValue(new c(roomConnectedInfo, j, wCEthereumTransaction, wallet2));
    }

    public final void v() {
        WalletConnect walletConnect;
        for (RoomConnectedInfoAndWallet roomConnectedInfoAndWallet : this.f) {
            try {
                RoomConnectedInfo dApp = roomConnectedInfoAndWallet.getDApp();
                if (dApp.isAutoDisconnect()) {
                    if (dApp.getConnectedAtUnix() + TimeUnit.MINUTES.toSeconds(30L) <= b30.a.z(new Date())) {
                        w(dApp);
                        if (this.c.containsKey(dApp.getPeerId()) && (walletConnect = this.c.get(dApp.getPeerId())) != null) {
                            walletConnect.p();
                        }
                    }
                }
            } catch (Exception unused) {
            }
        }
    }

    public final void w(RoomConnectedInfo roomConnectedInfo) {
        as.b(this.e, null, null, new MultipleWalletConnect$removeDApp$1(this, roomConnectedInfo, null), 3, null);
    }
}
