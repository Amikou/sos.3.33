package net.safemoon.androidwallet.viewmodels.wc;

import kotlin.jvm.internal.Lambda;
import wallet.core.jni.PrivateKey;

/* compiled from: WalletConnect.kt */
/* loaded from: classes2.dex */
public final class WalletConnect$privateKey$2 extends Lambda implements rc1<PrivateKey> {
    public final /* synthetic */ WalletConnect this$0;

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public WalletConnect$privateKey$2(WalletConnect walletConnect) {
        super(0);
        this.this$0 = walletConnect;
    }

    @Override // defpackage.rc1
    public final PrivateKey invoke() {
        byte[] e;
        WalletConnect walletConnect = this.this$0;
        String d = w.d(walletConnect.k(), this.this$0.n().getPrivateKey());
        fs1.e(d, "getDecryptedPrivateKey(a…ivity, wallet.privateKey)");
        e = walletConnect.e(d);
        return new PrivateKey(e);
    }
}
