package net.safemoon.androidwallet.viewmodels.wc;

import defpackage.zc4;
import org.web3j.abi.datatypes.DynamicArray;

/* loaded from: classes2.dex */
public class DynamicStructArray<T extends zc4> extends DynamicArray<T> {
    @Override // org.web3j.abi.datatypes.DynamicArray, org.web3j.abi.datatypes.Array, defpackage.zc4
    public String getTypeAsString() {
        return "(address,bytes)[]";
    }
}
