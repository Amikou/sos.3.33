package net.safemoon.androidwallet.viewmodels.wc;

import kotlin.jvm.internal.Lambda;

/* compiled from: WalletConnect.kt */
/* loaded from: classes2.dex */
public final class WalletConnect$wcClient$2$1$4 extends Lambda implements tc1<Long, te4> {
    public static final WalletConnect$wcClient$2$1$4 INSTANCE = new WalletConnect$wcClient$2$1$4();

    public WalletConnect$wcClient$2$1$4() {
        super(1);
    }

    @Override // defpackage.tc1
    public /* bridge */ /* synthetic */ te4 invoke(Long l) {
        invoke(l.longValue());
        return te4.a;
    }

    public final void invoke(long j) {
        String valueOf = String.valueOf(j);
        String canonicalName = WalletConnect.class.getCanonicalName();
        fs1.e(canonicalName, "WalletConnect::class.java.canonicalName");
        e30.c0(valueOf, canonicalName);
    }
}
