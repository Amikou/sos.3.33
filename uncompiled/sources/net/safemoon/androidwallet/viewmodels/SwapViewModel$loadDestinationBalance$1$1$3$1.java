package net.safemoon.androidwallet.viewmodels;

import java.math.BigDecimal;
import java.math.BigInteger;
import kotlin.coroutines.jvm.internal.SuspendLambda;
import kotlin.coroutines.jvm.internal.a;
import net.safemoon.androidwallet.model.swap.Swap;

/* compiled from: SwapViewModel.kt */
@a(c = "net.safemoon.androidwallet.viewmodels.SwapViewModel$loadDestinationBalance$1$1$3$1", f = "SwapViewModel.kt", l = {}, m = "invokeSuspend")
/* loaded from: classes2.dex */
public final class SwapViewModel$loadDestinationBalance$1$1$3$1 extends SuspendLambda implements hd1<c90, q70<? super te4>, Object> {
    public final /* synthetic */ BigInteger $it;
    public final /* synthetic */ Swap $swap;
    public int label;
    public final /* synthetic */ SwapViewModel this$0;

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public SwapViewModel$loadDestinationBalance$1$1$3$1(SwapViewModel swapViewModel, BigInteger bigInteger, Swap swap, q70<? super SwapViewModel$loadDestinationBalance$1$1$3$1> q70Var) {
        super(2, q70Var);
        this.this$0 = swapViewModel;
        this.$it = bigInteger;
        this.$swap = swap;
    }

    @Override // kotlin.coroutines.jvm.internal.BaseContinuationImpl
    public final q70<te4> create(Object obj, q70<?> q70Var) {
        return new SwapViewModel$loadDestinationBalance$1$1$3$1(this.this$0, this.$it, this.$swap, q70Var);
    }

    @Override // defpackage.hd1
    public final Object invoke(c90 c90Var, q70<? super te4> q70Var) {
        return ((SwapViewModel$loadDestinationBalance$1$1$3$1) create(c90Var, q70Var)).invokeSuspend(te4.a);
    }

    @Override // kotlin.coroutines.jvm.internal.BaseContinuationImpl
    public final Object invokeSuspend(Object obj) {
        gs1.d();
        if (this.label == 0) {
            o83.b(obj);
            gb2<BigDecimal> a0 = this.this$0.a0();
            String bigInteger = this.$it.toString();
            fs1.e(bigInteger, "it.toString()");
            Integer num = this.$swap.decimals;
            fs1.e(num, "swap.decimals");
            a0.postValue(e30.q(bigInteger, num.intValue()));
            return te4.a;
        }
        throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
    }
}
