package net.safemoon.androidwallet.viewmodels;

import android.app.Application;
import androidx.lifecycle.LiveData;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.Iterator;
import java.util.List;
import java.util.ListIterator;
import net.safemoon.androidwallet.model.fiat.gson.Fiat;
import net.safemoon.androidwallet.model.fiat.room.RoomFiat;
import net.safemoon.androidwallet.repository.FcmDataSource;
import net.safemoon.androidwallet.viewmodels.SelectFiatViewModel;

/* compiled from: SelectFiatViewModel.kt */
/* loaded from: classes2.dex */
public final class SelectFiatViewModel extends gd {
    public static final a e = new a(null);
    public static final List<String> f = b20.j("AUD", "EUR", "GBP", "USD");
    public final yl1 b;
    public final sy1 c;
    public final LiveData<List<RoomFiat>> d;

    /* compiled from: SelectFiatViewModel.kt */
    /* loaded from: classes2.dex */
    public static final class a {
        public a() {
        }

        public /* synthetic */ a(qi0 qi0Var) {
            this();
        }

        public final List<String> a() {
            return SelectFiatViewModel.f;
        }
    }

    /* compiled from: Comparisons.kt */
    /* loaded from: classes2.dex */
    public static final class b<T> implements Comparator {
        @Override // java.util.Comparator
        public final int compare(T t, T t2) {
            return l30.a(((RoomFiat) t).getSymbol(), ((RoomFiat) t2).getSymbol());
        }
    }

    /* compiled from: Comparisons.kt */
    /* loaded from: classes2.dex */
    public static final class c<T> implements Comparator {
        @Override // java.util.Comparator
        public final int compare(T t, T t2) {
            return l30.a(((RoomFiat) t).getSymbol(), ((RoomFiat) t2).getSymbol());
        }
    }

    /* compiled from: Comparisons.kt */
    /* loaded from: classes2.dex */
    public static final class d<T> implements Comparator {
        @Override // java.util.Comparator
        public final int compare(T t, T t2) {
            return l30.a(((RoomFiat) t).getSymbol(), ((RoomFiat) t2).getSymbol());
        }
    }

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public SelectFiatViewModel(Application application, yl1 yl1Var) {
        super(application);
        fs1.f(application, "application");
        fs1.f(yl1Var, "iFiatTokenDataSource");
        this.b = yl1Var;
        this.c = zy1.a(new SelectFiatViewModel$fcmDataSource$2(application));
        LiveData<List<RoomFiat>> a2 = cb4.a(yl1Var.a());
        fs1.e(a2, "Transformations.distinctUntilChanged(this)");
        this.d = a2;
        o();
        a2.observeForever(new tl2() { // from class: pj3
            @Override // defpackage.tl2
            public final void onChanged(Object obj) {
                SelectFiatViewModel.c(SelectFiatViewModel.this, (List) obj);
            }
        });
    }

    public static final void c(SelectFiatViewModel selectFiatViewModel, List list) {
        fs1.f(selectFiatViewModel, "this$0");
        selectFiatViewModel.l();
    }

    /* JADX WARN: Multi-variable type inference failed */
    public static /* synthetic */ void n(SelectFiatViewModel selectFiatViewModel, Fiat fiat, boolean z, rc1 rc1Var, int i, Object obj) {
        if ((i & 2) != 0) {
            z = false;
        }
        if ((i & 4) != 0) {
            rc1Var = null;
        }
        selectFiatViewModel.m(fiat, z, rc1Var);
    }

    public final Fiat g() {
        Fiat.Companion companion = Fiat.Companion;
        String j = bo3.j(a(), "DEFAULT_FIAT", companion.getDEFAULT_CURRENCY_STRING());
        fs1.e(j, "getString(\n            g…CURRENCY_STRING\n        )");
        return companion.to(j);
    }

    public final LiveData<List<RoomFiat>> h() {
        return this.d;
    }

    public final FcmDataSource i() {
        return (FcmDataSource) this.c.getValue();
    }

    public final RoomFiat j() {
        Object obj;
        List<RoomFiat> value = this.d.getValue();
        Object obj2 = null;
        if (value == null) {
            return null;
        }
        List e0 = j20.e0(value, new b());
        ArrayList arrayList = new ArrayList();
        Iterator it = e0.iterator();
        while (true) {
            if (!it.hasNext()) {
                break;
            }
            Object next = it.next();
            RoomFiat roomFiat = (RoomFiat) next;
            Iterator<T> it2 = f.iterator();
            while (true) {
                if (!it2.hasNext()) {
                    obj = null;
                    break;
                }
                obj = it2.next();
                if (dv3.t((String) obj, roomFiat.getSymbol(), true)) {
                    break;
                }
            }
            if (obj != null) {
                arrayList.add(next);
            }
        }
        if (!arrayList.isEmpty()) {
            Iterator it3 = arrayList.iterator();
            while (true) {
                if (!it3.hasNext()) {
                    break;
                }
                Object next2 = it3.next();
                if (fs1.b(((RoomFiat) next2).getSymbol(), "EUR")) {
                    obj2 = next2;
                    break;
                }
            }
            return (RoomFiat) obj2;
        }
        return null;
    }

    /* JADX WARN: Code restructure failed: missing block: B:118:0x009c, code lost:
        if (r4 == false) goto L16;
     */
    /*
        Code decompiled incorrectly, please refer to instructions dump.
        To view partially-correct code enable 'Show inconsistent code' option in preferences
    */
    public final java.util.List<net.safemoon.androidwallet.model.common.HistoryListItem> k(java.lang.String r15) {
        /*
            Method dump skipped, instructions count: 447
            To view this dump change 'Code comments level' option to 'DEBUG'
        */
        throw new UnsupportedOperationException("Method not decompiled: net.safemoon.androidwallet.viewmodels.SelectFiatViewModel.k(java.lang.String):java.util.List");
    }

    public final void l() {
        RoomFiat roomFiat;
        Fiat g = g();
        u21.a.e(g);
        List<RoomFiat> value = this.d.getValue();
        if (value == null) {
            return;
        }
        ListIterator<RoomFiat> listIterator = value.listIterator(value.size());
        while (true) {
            if (!listIterator.hasPrevious()) {
                roomFiat = null;
                break;
            }
            roomFiat = listIterator.previous();
            if (fs1.b(roomFiat.getSymbol(), g.getSymbol())) {
                break;
            }
        }
        RoomFiat roomFiat2 = roomFiat;
        if (roomFiat2 == null) {
            return;
        }
        bo3.o(a(), "DEFAULT_FIAT", new Fiat(roomFiat2.getSymbol(), roomFiat2.getName(), roomFiat2.getRate()).toString());
    }

    public final void m(Fiat fiat, boolean z, rc1<te4> rc1Var) {
        fs1.f(fiat, "item");
        if (z) {
            FcmDataSource.h(i(), null, fiat.getSymbol(), false, new SelectFiatViewModel$setDefault$1(this, fiat, rc1Var), 5, null);
            return;
        }
        bo3.o(a(), "DEFAULT_FIAT", fiat.toString());
        l();
    }

    public final void o() {
        qg1 qg1Var = qg1.a;
        as.b(qg1Var, null, null, new SelectFiatViewModel$updateRates$1(this, null), 3, null);
        as.b(qg1Var, null, null, new SelectFiatViewModel$updateRates$2(this, null), 3, null);
    }
}
