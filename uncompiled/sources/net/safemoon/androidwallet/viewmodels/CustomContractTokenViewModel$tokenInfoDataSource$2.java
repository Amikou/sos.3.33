package net.safemoon.androidwallet.viewmodels;

import android.app.Application;
import kotlin.jvm.internal.Lambda;
import net.safemoon.androidwallet.database.room.ApplicationRoomDatabase;

/* compiled from: CustomContractTokenViewModel.kt */
/* loaded from: classes2.dex */
public final class CustomContractTokenViewModel$tokenInfoDataSource$2 extends Lambda implements rc1<x64> {
    public final /* synthetic */ CustomContractTokenViewModel this$0;

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public CustomContractTokenViewModel$tokenInfoDataSource$2(CustomContractTokenViewModel customContractTokenViewModel) {
        super(0);
        this.this$0 = customContractTokenViewModel;
    }

    @Override // defpackage.rc1
    public final x64 invoke() {
        ApplicationRoomDatabase.k kVar = ApplicationRoomDatabase.n;
        Application a = this.this$0.a();
        fs1.e(a, "getApplication()");
        return ApplicationRoomDatabase.k.c(kVar, a, null, 2, null).Y();
    }
}
