package net.safemoon.androidwallet.viewmodels;

import java.util.List;
import kotlin.coroutines.jvm.internal.SuspendLambda;
import kotlin.coroutines.jvm.internal.a;
import kotlinx.coroutines.CoroutineDispatcher;
import net.safemoon.androidwallet.model.token.abstraction.IToken;

/* compiled from: MyTokensListViewModel.kt */
@a(c = "net.safemoon.androidwallet.viewmodels.MyTokensListViewModel$updatePrice$1", f = "MyTokensListViewModel.kt", l = {265}, m = "invokeSuspend")
/* loaded from: classes2.dex */
public final class MyTokensListViewModel$updatePrice$1 extends SuspendLambda implements hd1<c90, q70<? super te4>, Object> {
    public final /* synthetic */ List<IToken> $iTokens;
    public int label;
    public final /* synthetic */ MyTokensListViewModel this$0;

    /* compiled from: MyTokensListViewModel.kt */
    @a(c = "net.safemoon.androidwallet.viewmodels.MyTokensListViewModel$updatePrice$1$1", f = "MyTokensListViewModel.kt", l = {295, 302, 310, 356, 357, 361, 387, 403}, m = "invokeSuspend")
    /* renamed from: net.safemoon.androidwallet.viewmodels.MyTokensListViewModel$updatePrice$1$1  reason: invalid class name */
    /* loaded from: classes2.dex */
    public static final class AnonymousClass1 extends SuspendLambda implements hd1<c90, q70<? super te4>, Object> {
        public final /* synthetic */ List<IToken> $iTokens;
        public Object L$0;
        public Object L$1;
        public Object L$2;
        public Object L$3;
        public Object L$4;
        public Object L$5;
        public Object L$6;
        public Object L$7;
        public int label;
        public final /* synthetic */ MyTokensListViewModel this$0;

        /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
        /* JADX WARN: Multi-variable type inference failed */
        public AnonymousClass1(List<? extends IToken> list, MyTokensListViewModel myTokensListViewModel, q70<? super AnonymousClass1> q70Var) {
            super(2, q70Var);
            this.$iTokens = list;
            this.this$0 = myTokensListViewModel;
        }

        @Override // kotlin.coroutines.jvm.internal.BaseContinuationImpl
        public final q70<te4> create(Object obj, q70<?> q70Var) {
            return new AnonymousClass1(this.$iTokens, this.this$0, q70Var);
        }

        @Override // defpackage.hd1
        public final Object invoke(c90 c90Var, q70<? super te4> q70Var) {
            return ((AnonymousClass1) create(c90Var, q70Var)).invokeSuspend(te4.a);
        }

        /* JADX WARN: Can't wrap try/catch for region: R(11:30|31|(1:33)|11|12|14|15|(4:18|(3:24|25|26)(3:20|21|22)|23|16)|27|28|(6:34|35|(15:38|39|40|41|42|43|44|(2:170|(1:172)(3:173|174|175))(1:48)|49|(1:51)(1:169)|(1:53)(3:57|58|(2:60|(7:62|(6:98|(1:100)|101|(2:102|(2:104|(3:106|(2:129|130)(2:110|111)|(2:113|114))(3:131|132|133))(2:134|135))|115|(4:118|(1:120)(1:128)|121|(1:127))(1:117))|64|65|(2:66|(2:68|(2:70|(2:72|(2:74|75))(3:90|91|92))(3:93|94|95))(2:96|97))|76|(4:79|(1:81)(1:89)|82|(1:88))(1:78))(6:136|(6:138|64|65|(3:66|(0)(0)|73)|76|(0)(0))|101|(3:102|(0)(0)|112)|115|(0)(0)))(6:139|(4:141|(2:142|(2:144|(2:146|(2:148|149))(3:164|165|166))(2:167|168))|150|(4:153|(1:155)(1:163)|156|(1:162))(1:152))|65|(3:66|(0)(0)|73)|76|(0)(0)))|54|55|56|36)|182|183|184)(0)) */
        /* JADX WARN: Can't wrap try/catch for region: R(18:336|(5:337|338|(1:340)|313|314)|315|316|317|(1:319)|278|279|(2:282|280)|283|284|(2:287|285)|288|289|(4:292|(3:294|295|296)(1:298)|297|290)|299|300|(3:302|232|(5:234|(1:253)(2:236|(1:238)(3:239|240|(1:242)(3:243|244|(1:246)(8:247|248|(1:250)|218|219|(4:222|(3:224|225|226)(1:228)|227|220)|229|230))))|231|232|(5:254|(3:256|257|(1:259)(7:260|195|196|197|198|(4:201|(1:207)(3:203|204|205)|206|199)|208))|209|28|(11:30|31|(1:33)|11|12|14|15|(4:18|(3:24|25|26)(3:20|21|22)|23|16)|27|28|(6:34|35|(15:38|39|40|41|42|43|44|(2:170|(1:172)(3:173|174|175))(1:48)|49|(1:51)(1:169)|(1:53)(3:57|58|(2:60|(7:62|(6:98|(1:100)|101|(2:102|(2:104|(3:106|(2:129|130)(2:110|111)|(2:113|114))(3:131|132|133))(2:134|135))|115|(4:118|(1:120)(1:128)|121|(1:127))(1:117))|64|65|(2:66|(2:68|(2:70|(2:72|(2:74|75))(3:90|91|92))(3:93|94|95))(2:96|97))|76|(4:79|(1:81)(1:89)|82|(1:88))(1:78))(6:136|(6:138|64|65|(3:66|(0)(0)|73)|76|(0)(0))|101|(3:102|(0)(0)|112)|115|(0)(0)))(6:139|(4:141|(2:142|(2:144|(2:146|(2:148|149))(3:164|165|166))(2:167|168))|150|(4:153|(1:155)(1:163)|156|(1:162))(1:152))|65|(3:66|(0)(0)|73)|76|(0)(0)))|54|55|56|36)|182|183|184)(0))(0))(0))(0))(6:303|35|(1:36)|182|183|184)) */
        /* JADX WARN: Can't wrap try/catch for region: R(8:247|248|(1:250)|218|219|(4:222|(3:224|225|226)(1:228)|227|220)|229|230) */
        /* JADX WARN: Removed duplicated region for block: B:102:0x033f  */
        /* JADX WARN: Removed duplicated region for block: B:105:0x035b  */
        /* JADX WARN: Removed duplicated region for block: B:116:0x03ed A[RETURN] */
        /* JADX WARN: Removed duplicated region for block: B:117:0x03ee  */
        /* JADX WARN: Removed duplicated region for block: B:120:0x0418 A[RETURN] */
        /* JADX WARN: Removed duplicated region for block: B:124:0x0427 A[Catch: Exception -> 0x0448, TryCatch #11 {Exception -> 0x0448, blocks: (B:121:0x0419, B:122:0x0421, B:124:0x0427, B:126:0x043b, B:114:0x03ae, B:118:0x03f0), top: B:328:0x0419 }] */
        /* JADX WARN: Removed duplicated region for block: B:132:0x0459  */
        /* JADX WARN: Removed duplicated region for block: B:145:0x04c8  */
        /* JADX WARN: Removed duplicated region for block: B:152:0x04e7  */
        /* JADX WARN: Removed duplicated region for block: B:162:0x053b A[Catch: Exception -> 0x04e1, TryCatch #17 {Exception -> 0x04e1, blocks: (B:159:0x0527, B:160:0x0535, B:162:0x053b, B:165:0x0544), top: B:339:0x0527 }] */
        /* JADX WARN: Removed duplicated region for block: B:167:0x054e  */
        /* JADX WARN: Removed duplicated region for block: B:168:0x0550  */
        /* JADX WARN: Removed duplicated region for block: B:172:0x056a  */
        /* JADX WARN: Removed duplicated region for block: B:207:0x063f A[Catch: Exception -> 0x07a2, TryCatch #16 {Exception -> 0x07a2, blocks: (B:175:0x0577, B:178:0x0581, B:180:0x058b, B:184:0x05a3, B:186:0x05bf, B:290:0x0787, B:190:0x0608, B:236:0x06b7, B:237:0x06bb, B:239:0x06c1, B:241:0x06d2, B:243:0x06e1, B:251:0x06fc, B:254:0x0702, B:258:0x0711, B:261:0x0719, B:264:0x0720, B:257:0x0709, B:246:0x06ef, B:247:0x06f4, B:248:0x06f5, B:249:0x06fa, B:198:0x0625, B:204:0x0635, B:205:0x0639, B:207:0x063f, B:209:0x0650, B:211:0x065f, B:220:0x067e, B:223:0x0684, B:227:0x0693, B:230:0x069b, B:233:0x06a3, B:226:0x068b, B:217:0x0675, B:218:0x067c, B:201:0x062c, B:234:0x06ad, B:265:0x0729, B:266:0x072d, B:268:0x0733, B:270:0x0744, B:276:0x075d, B:279:0x0762, B:283:0x0771, B:286:0x0778, B:289:0x077f, B:282:0x0769, B:273:0x0756, B:274:0x075b, B:181:0x0590, B:183:0x059a, B:291:0x079b, B:292:0x07a1), top: B:338:0x0577 }] */
        /* JADX WARN: Removed duplicated region for block: B:222:0x0682  */
        /* JADX WARN: Removed duplicated region for block: B:223:0x0684 A[Catch: Exception -> 0x07a2, TryCatch #16 {Exception -> 0x07a2, blocks: (B:175:0x0577, B:178:0x0581, B:180:0x058b, B:184:0x05a3, B:186:0x05bf, B:290:0x0787, B:190:0x0608, B:236:0x06b7, B:237:0x06bb, B:239:0x06c1, B:241:0x06d2, B:243:0x06e1, B:251:0x06fc, B:254:0x0702, B:258:0x0711, B:261:0x0719, B:264:0x0720, B:257:0x0709, B:246:0x06ef, B:247:0x06f4, B:248:0x06f5, B:249:0x06fa, B:198:0x0625, B:204:0x0635, B:205:0x0639, B:207:0x063f, B:209:0x0650, B:211:0x065f, B:220:0x067e, B:223:0x0684, B:227:0x0693, B:230:0x069b, B:233:0x06a3, B:226:0x068b, B:217:0x0675, B:218:0x067c, B:201:0x062c, B:234:0x06ad, B:265:0x0729, B:266:0x072d, B:268:0x0733, B:270:0x0744, B:276:0x075d, B:279:0x0762, B:283:0x0771, B:286:0x0778, B:289:0x077f, B:282:0x0769, B:273:0x0756, B:274:0x075b, B:181:0x0590, B:183:0x059a, B:291:0x079b, B:292:0x07a1), top: B:338:0x0577 }] */
        /* JADX WARN: Removed duplicated region for block: B:239:0x06c1 A[Catch: Exception -> 0x07a2, TryCatch #16 {Exception -> 0x07a2, blocks: (B:175:0x0577, B:178:0x0581, B:180:0x058b, B:184:0x05a3, B:186:0x05bf, B:290:0x0787, B:190:0x0608, B:236:0x06b7, B:237:0x06bb, B:239:0x06c1, B:241:0x06d2, B:243:0x06e1, B:251:0x06fc, B:254:0x0702, B:258:0x0711, B:261:0x0719, B:264:0x0720, B:257:0x0709, B:246:0x06ef, B:247:0x06f4, B:248:0x06f5, B:249:0x06fa, B:198:0x0625, B:204:0x0635, B:205:0x0639, B:207:0x063f, B:209:0x0650, B:211:0x065f, B:220:0x067e, B:223:0x0684, B:227:0x0693, B:230:0x069b, B:233:0x06a3, B:226:0x068b, B:217:0x0675, B:218:0x067c, B:201:0x062c, B:234:0x06ad, B:265:0x0729, B:266:0x072d, B:268:0x0733, B:270:0x0744, B:276:0x075d, B:279:0x0762, B:283:0x0771, B:286:0x0778, B:289:0x077f, B:282:0x0769, B:273:0x0756, B:274:0x075b, B:181:0x0590, B:183:0x059a, B:291:0x079b, B:292:0x07a1), top: B:338:0x0577 }] */
        /* JADX WARN: Removed duplicated region for block: B:253:0x0700  */
        /* JADX WARN: Removed duplicated region for block: B:254:0x0702 A[Catch: Exception -> 0x07a2, TryCatch #16 {Exception -> 0x07a2, blocks: (B:175:0x0577, B:178:0x0581, B:180:0x058b, B:184:0x05a3, B:186:0x05bf, B:290:0x0787, B:190:0x0608, B:236:0x06b7, B:237:0x06bb, B:239:0x06c1, B:241:0x06d2, B:243:0x06e1, B:251:0x06fc, B:254:0x0702, B:258:0x0711, B:261:0x0719, B:264:0x0720, B:257:0x0709, B:246:0x06ef, B:247:0x06f4, B:248:0x06f5, B:249:0x06fa, B:198:0x0625, B:204:0x0635, B:205:0x0639, B:207:0x063f, B:209:0x0650, B:211:0x065f, B:220:0x067e, B:223:0x0684, B:227:0x0693, B:230:0x069b, B:233:0x06a3, B:226:0x068b, B:217:0x0675, B:218:0x067c, B:201:0x062c, B:234:0x06ad, B:265:0x0729, B:266:0x072d, B:268:0x0733, B:270:0x0744, B:276:0x075d, B:279:0x0762, B:283:0x0771, B:286:0x0778, B:289:0x077f, B:282:0x0769, B:273:0x0756, B:274:0x075b, B:181:0x0590, B:183:0x059a, B:291:0x079b, B:292:0x07a1), top: B:338:0x0577 }] */
        /* JADX WARN: Removed duplicated region for block: B:356:0x06fb A[SYNTHETIC] */
        /* JADX WARN: Removed duplicated region for block: B:358:0x067d A[SYNTHETIC] */
        /* JADX WARN: Removed duplicated region for block: B:64:0x01f6  */
        /* JADX WARN: Removed duplicated region for block: B:65:0x01f8 A[Catch: Exception -> 0x0202, TRY_LEAVE, TryCatch #15 {Exception -> 0x0202, blocks: (B:62:0x01ec, B:65:0x01f8, B:59:0x01cf), top: B:336:0x01cf }] */
        /* JADX WARN: Removed duplicated region for block: B:69:0x0206  */
        /* JADX WARN: Removed duplicated region for block: B:71:0x0209  */
        /* JADX WARN: Removed duplicated region for block: B:82:0x0285 A[RETURN] */
        /* JADX WARN: Removed duplicated region for block: B:89:0x02b6 A[LOOP:7: B:87:0x02ae->B:89:0x02b6, LOOP_END] */
        /* JADX WARN: Removed duplicated region for block: B:93:0x02e8 A[LOOP:8: B:91:0x02e2->B:93:0x02e8, LOOP_END] */
        /* JADX WARN: Removed duplicated region for block: B:97:0x0312  */
        /* JADX WARN: Type inference failed for: r4v0, types: [java.lang.String] */
        /* JADX WARN: Type inference failed for: r4v10 */
        /* JADX WARN: Type inference failed for: r4v11 */
        /* JADX WARN: Type inference failed for: r4v22 */
        /* JADX WARN: Type inference failed for: r4v27 */
        /* JADX WARN: Type inference failed for: r4v3, types: [java.util.Iterator, java.lang.Object] */
        /* JADX WARN: Type inference failed for: r4v35 */
        /* JADX WARN: Type inference failed for: r4v36 */
        /* JADX WARN: Type inference failed for: r4v40 */
        /* JADX WARN: Unsupported multi-entry loop pattern (BACK_EDGE: B:109:0x0378 -> B:131:0x0455). Please submit an issue!!! */
        /* JADX WARN: Unsupported multi-entry loop pattern (BACK_EDGE: B:119:0x0416 -> B:328:0x0419). Please submit an issue!!! */
        /* JADX WARN: Unsupported multi-entry loop pattern (BACK_EDGE: B:128:0x0443 -> B:129:0x0448). Please submit an issue!!! */
        /* JADX WARN: Unsupported multi-entry loop pattern (BACK_EDGE: B:130:0x044e -> B:131:0x0455). Please submit an issue!!! */
        /* JADX WARN: Unsupported multi-entry loop pattern (BACK_EDGE: B:154:0x050e -> B:330:0x0511). Please submit an issue!!! */
        @Override // kotlin.coroutines.jvm.internal.BaseContinuationImpl
        /*
            Code decompiled incorrectly, please refer to instructions dump.
            To view partially-correct code enable 'Show inconsistent code' option in preferences
        */
        public final java.lang.Object invokeSuspend(java.lang.Object r29) {
            /*
                Method dump skipped, instructions count: 2010
                To view this dump change 'Code comments level' option to 'DEBUG'
            */
            throw new UnsupportedOperationException("Method not decompiled: net.safemoon.androidwallet.viewmodels.MyTokensListViewModel$updatePrice$1.AnonymousClass1.invokeSuspend(java.lang.Object):java.lang.Object");
        }
    }

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    /* JADX WARN: Multi-variable type inference failed */
    public MyTokensListViewModel$updatePrice$1(List<? extends IToken> list, MyTokensListViewModel myTokensListViewModel, q70<? super MyTokensListViewModel$updatePrice$1> q70Var) {
        super(2, q70Var);
        this.$iTokens = list;
        this.this$0 = myTokensListViewModel;
    }

    @Override // kotlin.coroutines.jvm.internal.BaseContinuationImpl
    public final q70<te4> create(Object obj, q70<?> q70Var) {
        return new MyTokensListViewModel$updatePrice$1(this.$iTokens, this.this$0, q70Var);
    }

    @Override // defpackage.hd1
    public final Object invoke(c90 c90Var, q70<? super te4> q70Var) {
        return ((MyTokensListViewModel$updatePrice$1) create(c90Var, q70Var)).invokeSuspend(te4.a);
    }

    @Override // kotlin.coroutines.jvm.internal.BaseContinuationImpl
    public final Object invokeSuspend(Object obj) {
        Object d = gs1.d();
        int i = this.label;
        if (i == 0) {
            o83.b(obj);
            CoroutineDispatcher b = tp0.b();
            AnonymousClass1 anonymousClass1 = new AnonymousClass1(this.$iTokens, this.this$0, null);
            this.label = 1;
            if (kotlinx.coroutines.a.e(b, anonymousClass1, this) == d) {
                return d;
            }
        } else if (i != 1) {
            throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        } else {
            o83.b(obj);
        }
        return te4.a;
    }
}
