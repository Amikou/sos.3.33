package net.safemoon.androidwallet.viewmodels;

import java.util.List;
import kotlin.coroutines.jvm.internal.SuspendLambda;
import kotlin.coroutines.jvm.internal.a;
import net.safemoon.androidwallet.model.reflections.RoomReflectionsToken;

/* compiled from: ReflectionTrackerViewModel.kt */
@a(c = "net.safemoon.androidwallet.viewmodels.ReflectionTrackerViewModel$fetchReflectionCached$3", f = "ReflectionTrackerViewModel.kt", l = {}, m = "invokeSuspend")
/* loaded from: classes2.dex */
public final class ReflectionTrackerViewModel$fetchReflectionCached$3 extends SuspendLambda implements kd1<k71<? super List<RoomReflectionsToken>>, Throwable, q70<? super te4>, Object> {
    public int label;
    public final /* synthetic */ ReflectionTrackerViewModel this$0;

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public ReflectionTrackerViewModel$fetchReflectionCached$3(ReflectionTrackerViewModel reflectionTrackerViewModel, q70<? super ReflectionTrackerViewModel$fetchReflectionCached$3> q70Var) {
        super(3, q70Var);
        this.this$0 = reflectionTrackerViewModel;
    }

    @Override // defpackage.kd1
    public final Object invoke(k71<? super List<RoomReflectionsToken>> k71Var, Throwable th, q70<? super te4> q70Var) {
        return new ReflectionTrackerViewModel$fetchReflectionCached$3(this.this$0, q70Var).invokeSuspend(te4.a);
    }

    @Override // kotlin.coroutines.jvm.internal.BaseContinuationImpl
    public final Object invokeSuspend(Object obj) {
        gs1.d();
        if (this.label == 0) {
            o83.b(obj);
            ej4.a(this.this$0);
            return te4.a;
        }
        throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
    }
}
