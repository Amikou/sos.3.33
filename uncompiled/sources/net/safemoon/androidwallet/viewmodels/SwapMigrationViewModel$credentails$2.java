package net.safemoon.androidwallet.viewmodels;

import kotlin.jvm.internal.Lambda;

/* compiled from: SwapMigrationViewModel.kt */
/* loaded from: classes2.dex */
public final class SwapMigrationViewModel$credentails$2 extends Lambda implements rc1<ma0> {
    public final /* synthetic */ SwapMigrationViewModel this$0;

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public SwapMigrationViewModel$credentails$2(SwapMigrationViewModel swapMigrationViewModel) {
        super(0);
        this.this$0 = swapMigrationViewModel;
    }

    @Override // defpackage.rc1
    public final ma0 invoke() {
        String S;
        S = this.this$0.S();
        return ma0.create(S);
    }
}
