package net.safemoon.androidwallet.viewmodels;

import android.app.Application;
import kotlin.jvm.internal.Lambda;
import net.safemoon.androidwallet.utils.ReflectionCustomContract;

/* compiled from: ReflectionTrackerViewModel.kt */
/* loaded from: classes2.dex */
public final class ReflectionTrackerViewModel$reflectionCustomContract$2 extends Lambda implements rc1<ReflectionCustomContract> {
    public final /* synthetic */ ReflectionTrackerViewModel this$0;

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public ReflectionTrackerViewModel$reflectionCustomContract$2(ReflectionTrackerViewModel reflectionTrackerViewModel) {
        super(0);
        this.this$0 = reflectionTrackerViewModel;
    }

    @Override // defpackage.rc1
    public final ReflectionCustomContract invoke() {
        Application a = this.this$0.a();
        fs1.e(a, "getApplication()");
        return new ReflectionCustomContract(a, this.this$0.r());
    }
}
