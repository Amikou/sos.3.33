package net.safemoon.androidwallet.viewmodels;

import android.app.Application;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.math.RoundingMode;
import kotlin.coroutines.jvm.internal.SuspendLambda;
import kotlin.coroutines.jvm.internal.a;
import net.safemoon.androidwallet.R;
import net.safemoon.androidwallet.viewmodels.SwapViewModel;

/* compiled from: SwapViewModel.kt */
@a(c = "net.safemoon.androidwallet.viewmodels.SwapViewModel$getEstimateGas$1$1$3$1", f = "SwapViewModel.kt", l = {}, m = "invokeSuspend")
/* loaded from: classes2.dex */
public final class SwapViewModel$getEstimateGas$1$1$3$1 extends SuspendLambda implements hd1<c90, q70<? super te4>, Object> {
    public final /* synthetic */ SwapViewModel.d $it;
    public int label;
    public final /* synthetic */ SwapViewModel this$0;

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public SwapViewModel$getEstimateGas$1$1$3$1(SwapViewModel.d dVar, SwapViewModel swapViewModel, q70<? super SwapViewModel$getEstimateGas$1$1$3$1> q70Var) {
        super(2, q70Var);
        this.$it = dVar;
        this.this$0 = swapViewModel;
    }

    @Override // kotlin.coroutines.jvm.internal.BaseContinuationImpl
    public final q70<te4> create(Object obj, q70<?> q70Var) {
        return new SwapViewModel$getEstimateGas$1$1$3$1(this.$it, this.this$0, q70Var);
    }

    @Override // defpackage.hd1
    public final Object invoke(c90 c90Var, q70<? super te4> q70Var) {
        return ((SwapViewModel$getEstimateGas$1$1$3$1) create(c90Var, q70Var)).invokeSuspend(te4.a);
    }

    @Override // kotlin.coroutines.jvm.internal.BaseContinuationImpl
    public final Object invokeSuspend(Object obj) {
        BigInteger j0;
        String N0;
        BigDecimal bigDecimal;
        Application application;
        Application application2;
        gs1.d();
        if (this.label == 0) {
            o83.b(obj);
            if (fs1.b(this.$it.b(), "ERROR")) {
                this.this$0.K0().postValue(null);
                gb2<String> J0 = this.this$0.J0();
                application2 = this.this$0.b;
                J0.postValue(application2.getString(R.string.swap_error_insufficient_balance));
                SwapViewModel.i1(this.this$0, null, 1, null);
            } else {
                BigDecimal multiply = new BigDecimal(this.$it.a()).multiply(new BigDecimal(1.1d));
                fs1.e(multiply, "this.multiply(other)");
                BigInteger bigInteger = multiply.toBigInteger();
                fs1.e(bigInteger, "it.amount.toBigDecimal()…imal(1.1)).toBigInteger()");
                SwapViewModel.d dVar = new SwapViewModel.d(bigInteger, this.$it.d(), this.$it.b(), this.$it.c());
                j0 = this.this$0.j0();
                fs1.e(j0, "getGasPrice()");
                BigInteger multiply2 = j0.multiply(dVar.a());
                fs1.e(multiply2, "this.multiply(other)");
                BigInteger add = multiply2.add(this.$it.d());
                fs1.e(add, "getGasPrice() * newGasLi….amount).add(it.tradeFee)");
                BigDecimal r = e30.r(add, 18);
                N0 = this.this$0.N0();
                if (N0 != null) {
                    l84 I0 = this.this$0.I0();
                    BigInteger a = I0 == null ? null : I0.a();
                    if (a == null) {
                        a = BigInteger.ZERO;
                    }
                    fs1.e(a, "tradeStructure?.amountIn ?: BigInteger.ZERO");
                    BigDecimal r2 = e30.r(a, 18);
                    fs1.e(r2, "tradeStructure?.amountIn…er.ZERO).fromWEI(decimal)");
                    fs1.e(r, "gasLimitAmount");
                    bigDecimal = r2.add(r);
                    fs1.e(bigDecimal, "this.add(other)");
                } else {
                    bigDecimal = r;
                }
                if (dVar.c().compareTo(bigDecimal) >= 0) {
                    this.this$0.j1();
                    this.this$0.K0().postValue(dVar);
                    gb2<String> J02 = this.this$0.J0();
                    StringBuilder sb = new StringBuilder();
                    sb.append(this.this$0.D0(R.string.swap_max_fee));
                    sb.append(' ');
                    sb.append(e30.p(r.doubleValue(), 8, RoundingMode.HALF_UP, false, 4, null));
                    sb.append(this.this$0.M0() ? " ETH" : " BNB");
                    J02.setValue(sb.toString());
                } else {
                    this.this$0.K0().postValue(null);
                    gb2<String> J03 = this.this$0.J0();
                    application = this.this$0.b;
                    J03.postValue(application.getString(R.string.swap_error_insufficient_balance));
                    this.this$0.h1(r);
                }
            }
            return te4.a;
        }
        throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
    }
}
