package net.safemoon.androidwallet.viewmodels;

import kotlin.coroutines.jvm.internal.SuspendLambda;
import kotlin.coroutines.jvm.internal.a;
import kotlinx.coroutines.CoroutineDispatcher;
import net.safemoon.androidwallet.model.walletConnect.RoomConnectedInfo;

/* compiled from: WalletConnectViewModel.kt */
@a(c = "net.safemoon.androidwallet.viewmodels.WalletConnectViewModel$saveConnectedInfo$1", f = "WalletConnectViewModel.kt", l = {36}, m = "invokeSuspend")
/* loaded from: classes2.dex */
public final class WalletConnectViewModel$saveConnectedInfo$1 extends SuspendLambda implements hd1<c90, q70<? super te4>, Object> {
    public final /* synthetic */ RoomConnectedInfo $rci;
    public int label;
    public final /* synthetic */ WalletConnectViewModel this$0;

    /* compiled from: WalletConnectViewModel.kt */
    @a(c = "net.safemoon.androidwallet.viewmodels.WalletConnectViewModel$saveConnectedInfo$1$1", f = "WalletConnectViewModel.kt", l = {37}, m = "invokeSuspend")
    /* renamed from: net.safemoon.androidwallet.viewmodels.WalletConnectViewModel$saveConnectedInfo$1$1  reason: invalid class name */
    /* loaded from: classes2.dex */
    public static final class AnonymousClass1 extends SuspendLambda implements hd1<c90, q70<? super te4>, Object> {
        public final /* synthetic */ RoomConnectedInfo $rci;
        public int label;
        public final /* synthetic */ WalletConnectViewModel this$0;

        /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
        public AnonymousClass1(WalletConnectViewModel walletConnectViewModel, RoomConnectedInfo roomConnectedInfo, q70<? super AnonymousClass1> q70Var) {
            super(2, q70Var);
            this.this$0 = walletConnectViewModel;
            this.$rci = roomConnectedInfo;
        }

        @Override // kotlin.coroutines.jvm.internal.BaseContinuationImpl
        public final q70<te4> create(Object obj, q70<?> q70Var) {
            return new AnonymousClass1(this.this$0, this.$rci, q70Var);
        }

        @Override // defpackage.hd1
        public final Object invoke(c90 c90Var, q70<? super te4> q70Var) {
            return ((AnonymousClass1) create(c90Var, q70Var)).invokeSuspend(te4.a);
        }

        @Override // kotlin.coroutines.jvm.internal.BaseContinuationImpl
        public final Object invokeSuspend(Object obj) {
            t50 f;
            Object d = gs1.d();
            int i = this.label;
            if (i == 0) {
                o83.b(obj);
                f = this.this$0.f();
                RoomConnectedInfo roomConnectedInfo = this.$rci;
                this.label = 1;
                if (f.a(roomConnectedInfo, this) == d) {
                    return d;
                }
            } else if (i != 1) {
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            } else {
                o83.b(obj);
            }
            return te4.a;
        }
    }

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public WalletConnectViewModel$saveConnectedInfo$1(WalletConnectViewModel walletConnectViewModel, RoomConnectedInfo roomConnectedInfo, q70<? super WalletConnectViewModel$saveConnectedInfo$1> q70Var) {
        super(2, q70Var);
        this.this$0 = walletConnectViewModel;
        this.$rci = roomConnectedInfo;
    }

    @Override // kotlin.coroutines.jvm.internal.BaseContinuationImpl
    public final q70<te4> create(Object obj, q70<?> q70Var) {
        return new WalletConnectViewModel$saveConnectedInfo$1(this.this$0, this.$rci, q70Var);
    }

    @Override // defpackage.hd1
    public final Object invoke(c90 c90Var, q70<? super te4> q70Var) {
        return ((WalletConnectViewModel$saveConnectedInfo$1) create(c90Var, q70Var)).invokeSuspend(te4.a);
    }

    @Override // kotlin.coroutines.jvm.internal.BaseContinuationImpl
    public final Object invokeSuspend(Object obj) {
        Object d = gs1.d();
        int i = this.label;
        if (i == 0) {
            o83.b(obj);
            CoroutineDispatcher b = tp0.b();
            AnonymousClass1 anonymousClass1 = new AnonymousClass1(this.this$0, this.$rci, null);
            this.label = 1;
            if (kotlinx.coroutines.a.e(b, anonymousClass1, this) == d) {
                return d;
            }
        } else if (i != 1) {
            throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        } else {
            o83.b(obj);
        }
        return te4.a;
    }
}
