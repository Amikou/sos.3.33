package net.safemoon.androidwallet.viewmodels;

import kotlin.coroutines.jvm.internal.SuspendLambda;
import kotlin.coroutines.jvm.internal.a;

/* compiled from: SettingNotificationViewModel.kt */
@a(c = "net.safemoon.androidwallet.viewmodels.SettingNotificationViewModel$updateAllPriceAlert$1", f = "SettingNotificationViewModel.kt", l = {160}, m = "invokeSuspend")
/* loaded from: classes2.dex */
public final class SettingNotificationViewModel$updateAllPriceAlert$1 extends SuspendLambda implements hd1<c90, q70<? super te4>, Object> {
    public Object L$0;
    public int label;
    public final /* synthetic */ SettingNotificationViewModel this$0;

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public SettingNotificationViewModel$updateAllPriceAlert$1(SettingNotificationViewModel settingNotificationViewModel, q70<? super SettingNotificationViewModel$updateAllPriceAlert$1> q70Var) {
        super(2, q70Var);
        this.this$0 = settingNotificationViewModel;
    }

    @Override // kotlin.coroutines.jvm.internal.BaseContinuationImpl
    public final q70<te4> create(Object obj, q70<?> q70Var) {
        return new SettingNotificationViewModel$updateAllPriceAlert$1(this.this$0, q70Var);
    }

    @Override // defpackage.hd1
    public final Object invoke(c90 c90Var, q70<? super te4> q70Var) {
        return ((SettingNotificationViewModel$updateAllPriceAlert$1) create(c90Var, q70Var)).invokeSuspend(te4.a);
    }

    @Override // kotlin.coroutines.jvm.internal.BaseContinuationImpl
    public final Object invokeSuspend(Object obj) {
        gb2 gb2Var;
        Object s;
        gb2 gb2Var2;
        Object d = gs1.d();
        int i = this.label;
        if (i == 0) {
            o83.b(obj);
            gb2Var = this.this$0.k;
            SettingNotificationViewModel settingNotificationViewModel = this.this$0;
            this.L$0 = gb2Var;
            this.label = 1;
            s = settingNotificationViewModel.s(this);
            if (s == d) {
                return d;
            }
            gb2Var2 = gb2Var;
            obj = s;
        } else if (i != 1) {
            throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        } else {
            gb2Var2 = (gb2) this.L$0;
            o83.b(obj);
        }
        gb2Var2.postValue(obj);
        return te4.a;
    }
}
