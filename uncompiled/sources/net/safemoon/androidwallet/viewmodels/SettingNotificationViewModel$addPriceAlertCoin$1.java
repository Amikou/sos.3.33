package net.safemoon.androidwallet.viewmodels;

import kotlin.coroutines.jvm.internal.SuspendLambda;
import kotlin.coroutines.jvm.internal.a;
import net.safemoon.androidwallet.model.Coin;
import net.safemoon.androidwallet.model.RoomCoinPriceAlert;
import org.web3j.ens.contracts.generated.PublicResolver;

/* compiled from: SettingNotificationViewModel.kt */
@a(c = "net.safemoon.androidwallet.viewmodels.SettingNotificationViewModel$addPriceAlertCoin$1", f = "SettingNotificationViewModel.kt", l = {178}, m = "invokeSuspend")
/* loaded from: classes2.dex */
public final class SettingNotificationViewModel$addPriceAlertCoin$1 extends SuspendLambda implements hd1<c90, q70<? super te4>, Object> {
    public final /* synthetic */ Coin $coin;
    public int label;
    public final /* synthetic */ SettingNotificationViewModel this$0;

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public SettingNotificationViewModel$addPriceAlertCoin$1(SettingNotificationViewModel settingNotificationViewModel, Coin coin, q70<? super SettingNotificationViewModel$addPriceAlertCoin$1> q70Var) {
        super(2, q70Var);
        this.this$0 = settingNotificationViewModel;
        this.$coin = coin;
    }

    @Override // kotlin.coroutines.jvm.internal.BaseContinuationImpl
    public final q70<te4> create(Object obj, q70<?> q70Var) {
        return new SettingNotificationViewModel$addPriceAlertCoin$1(this.this$0, this.$coin, q70Var);
    }

    @Override // defpackage.hd1
    public final Object invoke(c90 c90Var, q70<? super te4> q70Var) {
        return ((SettingNotificationViewModel$addPriceAlertCoin$1) create(c90Var, q70Var)).invokeSuspend(te4.a);
    }

    @Override // kotlin.coroutines.jvm.internal.BaseContinuationImpl
    public final Object invokeSuspend(Object obj) {
        m00 t;
        Object d = gs1.d();
        int i = this.label;
        if (i == 0) {
            o83.b(obj);
            t = this.this$0.t();
            Integer id = this.$coin.getId();
            String name = this.$coin.getName();
            String symbol = this.$coin.getSymbol();
            String slug = this.$coin.getSlug();
            String coin = this.$coin.toString();
            fs1.e(id, "id");
            int intValue = id.intValue();
            fs1.e(symbol, "symbol");
            fs1.e(name, PublicResolver.FUNC_NAME);
            fs1.e(slug, "slug");
            fs1.e(coin, "toString()");
            RoomCoinPriceAlert roomCoinPriceAlert = new RoomCoinPriceAlert(intValue, symbol, name, slug, coin);
            this.label = 1;
            if (t.d(roomCoinPriceAlert, this) == d) {
                return d;
            }
        } else if (i != 1) {
            throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        } else {
            o83.b(obj);
        }
        return te4.a;
    }
}
