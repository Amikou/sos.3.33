package net.safemoon.androidwallet.viewmodels;

import kotlin.coroutines.jvm.internal.SuspendLambda;
import kotlin.coroutines.jvm.internal.a;
import net.safemoon.androidwallet.R;
import net.safemoon.androidwallet.common.TokenType;
import net.safemoon.androidwallet.model.common.LoadingState;
import net.safemoon.androidwallet.model.swap.Swap;
import net.safemoon.androidwallet.viewmodels.SwapMigrationViewModel;

/* compiled from: SwapMigrationViewModel.kt */
@a(c = "net.safemoon.androidwallet.viewmodels.SwapMigrationViewModel$checkApproveStatus$1$1$3$1", f = "SwapMigrationViewModel.kt", l = {}, m = "invokeSuspend")
/* loaded from: classes2.dex */
public final class SwapMigrationViewModel$checkApproveStatus$1$1$3$1 extends SuspendLambda implements hd1<c90, q70<? super te4>, Object> {
    public final /* synthetic */ SwapMigrationViewModel.ApproveStatus $it;
    public int label;
    public final /* synthetic */ SwapMigrationViewModel this$0;

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public SwapMigrationViewModel$checkApproveStatus$1$1$3$1(SwapMigrationViewModel swapMigrationViewModel, SwapMigrationViewModel.ApproveStatus approveStatus, q70<? super SwapMigrationViewModel$checkApproveStatus$1$1$3$1> q70Var) {
        super(2, q70Var);
        this.this$0 = swapMigrationViewModel;
        this.$it = approveStatus;
    }

    @Override // kotlin.coroutines.jvm.internal.BaseContinuationImpl
    public final q70<te4> create(Object obj, q70<?> q70Var) {
        return new SwapMigrationViewModel$checkApproveStatus$1$1$3$1(this.this$0, this.$it, q70Var);
    }

    @Override // defpackage.hd1
    public final Object invoke(c90 c90Var, q70<? super te4> q70Var) {
        return ((SwapMigrationViewModel$checkApproveStatus$1$1$3$1) create(c90Var, q70Var)).invokeSuspend(te4.a);
    }

    @Override // kotlin.coroutines.jvm.internal.BaseContinuationImpl
    public final Object invokeSuspend(Object obj) {
        String A;
        String l;
        String A2;
        gs1.d();
        if (this.label == 0) {
            o83.b(obj);
            this.this$0.E().postValue(LoadingState.Normal);
            SwapMigrationViewModel.ApproveStatus approveStatus = this.$it;
            if (approveStatus == SwapMigrationViewModel.ApproveStatus.ALLOWED) {
                this.this$0.F().postValue(hr.d(0));
                this.this$0.C();
            } else if (approveStatus == SwapMigrationViewModel.ApproveStatus.FAILED) {
                this.this$0.F().postValue(hr.d(1));
                this.this$0.d0().postValue(this.this$0.a0(R.string.swap_approval_declined));
            } else {
                this.this$0.F().postValue(hr.d(1));
                Swap value = this.this$0.X().getValue();
                if (value != null) {
                    SwapMigrationViewModel swapMigrationViewModel = this.this$0;
                    TokenType.a aVar = TokenType.Companion;
                    Integer num = value.chainId;
                    fs1.e(num, "it.chainId");
                    if (aVar.b(num.intValue()) == TokenType.BEP_20) {
                        A2 = swapMigrationViewModel.A();
                        l = fs1.l("https://bscscan.com/address/", A2);
                    } else {
                        A = swapMigrationViewModel.A();
                        l = fs1.l("https://etherscan.io/address/", A);
                    }
                    gb2<String> d0 = swapMigrationViewModel.d0();
                    d0.postValue(swapMigrationViewModel.a0(R.string.swap_approval_pending_1) + l + swapMigrationViewModel.a0(R.string.swap_approval_pending_2));
                }
            }
            return te4.a;
        }
        throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
    }
}
