package net.safemoon.androidwallet.viewmodels;

import java.util.Arrays;
import java.util.List;
import java.util.Objects;
import kotlin.Pair;
import kotlin.coroutines.jvm.internal.SuspendLambda;
import kotlin.coroutines.jvm.internal.a;
import kotlinx.coroutines.CoroutineDispatcher;
import net.safemoon.androidwallet.repository.WalletDataSource;

/* compiled from: MultiWalletViewModel.kt */
@a(c = "net.safemoon.androidwallet.viewmodels.MultiWalletViewModel$updateWalletOrders$2", f = "MultiWalletViewModel.kt", l = {138}, m = "invokeSuspend")
/* loaded from: classes2.dex */
public final class MultiWalletViewModel$updateWalletOrders$2 extends SuspendLambda implements hd1<c90, q70<? super te4>, Object> {
    public final /* synthetic */ List<Pair<Long, Integer>> $orderOfWallet;
    public int label;
    public final /* synthetic */ MultiWalletViewModel this$0;

    /* compiled from: MultiWalletViewModel.kt */
    @a(c = "net.safemoon.androidwallet.viewmodels.MultiWalletViewModel$updateWalletOrders$2$1", f = "MultiWalletViewModel.kt", l = {139}, m = "invokeSuspend")
    /* renamed from: net.safemoon.androidwallet.viewmodels.MultiWalletViewModel$updateWalletOrders$2$1  reason: invalid class name */
    /* loaded from: classes2.dex */
    public static final class AnonymousClass1 extends SuspendLambda implements hd1<c90, q70<? super te4>, Object> {
        public final /* synthetic */ List<Pair<Long, Integer>> $orderOfWallet;
        public int label;
        public final /* synthetic */ MultiWalletViewModel this$0;

        /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
        public AnonymousClass1(MultiWalletViewModel multiWalletViewModel, List<Pair<Long, Integer>> list, q70<? super AnonymousClass1> q70Var) {
            super(2, q70Var);
            this.this$0 = multiWalletViewModel;
            this.$orderOfWallet = list;
        }

        @Override // kotlin.coroutines.jvm.internal.BaseContinuationImpl
        public final q70<te4> create(Object obj, q70<?> q70Var) {
            return new AnonymousClass1(this.this$0, this.$orderOfWallet, q70Var);
        }

        @Override // defpackage.hd1
        public final Object invoke(c90 c90Var, q70<? super te4> q70Var) {
            return ((AnonymousClass1) create(c90Var, q70Var)).invokeSuspend(te4.a);
        }

        @Override // kotlin.coroutines.jvm.internal.BaseContinuationImpl
        public final Object invokeSuspend(Object obj) {
            WalletDataSource s;
            Object d = gs1.d();
            int i = this.label;
            if (i == 0) {
                o83.b(obj);
                s = this.this$0.s();
                Object[] array = this.$orderOfWallet.toArray(new Pair[0]);
                Objects.requireNonNull(array, "null cannot be cast to non-null type kotlin.Array<T>");
                Pair[] pairArr = (Pair[]) array;
                this.label = 1;
                if (s.l((Pair[]) Arrays.copyOf(pairArr, pairArr.length), this) == d) {
                    return d;
                }
            } else if (i != 1) {
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            } else {
                o83.b(obj);
            }
            this.this$0.z(this.$orderOfWallet);
            return te4.a;
        }
    }

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public MultiWalletViewModel$updateWalletOrders$2(MultiWalletViewModel multiWalletViewModel, List<Pair<Long, Integer>> list, q70<? super MultiWalletViewModel$updateWalletOrders$2> q70Var) {
        super(2, q70Var);
        this.this$0 = multiWalletViewModel;
        this.$orderOfWallet = list;
    }

    @Override // kotlin.coroutines.jvm.internal.BaseContinuationImpl
    public final q70<te4> create(Object obj, q70<?> q70Var) {
        return new MultiWalletViewModel$updateWalletOrders$2(this.this$0, this.$orderOfWallet, q70Var);
    }

    @Override // defpackage.hd1
    public final Object invoke(c90 c90Var, q70<? super te4> q70Var) {
        return ((MultiWalletViewModel$updateWalletOrders$2) create(c90Var, q70Var)).invokeSuspend(te4.a);
    }

    @Override // kotlin.coroutines.jvm.internal.BaseContinuationImpl
    public final Object invokeSuspend(Object obj) {
        Object d = gs1.d();
        int i = this.label;
        if (i == 0) {
            o83.b(obj);
            CoroutineDispatcher b = tp0.b();
            AnonymousClass1 anonymousClass1 = new AnonymousClass1(this.this$0, this.$orderOfWallet, null);
            this.label = 1;
            if (kotlinx.coroutines.a.e(b, anonymousClass1, this) == d) {
                return d;
            }
        } else if (i != 1) {
            throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        } else {
            o83.b(obj);
        }
        return te4.a;
    }
}
