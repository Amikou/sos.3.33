package net.safemoon.androidwallet.viewmodels;

import kotlin.coroutines.jvm.internal.SuspendLambda;
import kotlin.coroutines.jvm.internal.a;
import kotlin.jvm.internal.Ref$ObjectRef;
import kotlinx.coroutines.CoroutineDispatcher;

/* compiled from: CalculatorViewModel.kt */
@a(c = "net.safemoon.androidwallet.viewmodels.CalculatorViewModel$fetchTokenPrice$1", f = "CalculatorViewModel.kt", l = {272}, m = "invokeSuspend")
/* loaded from: classes2.dex */
public final class CalculatorViewModel$fetchTokenPrice$1 extends SuspendLambda implements hd1<c90, q70<? super te4>, Object> {
    public final /* synthetic */ rc1<te4> $callBack;
    public final /* synthetic */ Ref$ObjectRef<String> $tokenAddress;
    public final /* synthetic */ String $tokenId;
    public int label;
    public final /* synthetic */ CalculatorViewModel this$0;

    /* compiled from: CalculatorViewModel.kt */
    @a(c = "net.safemoon.androidwallet.viewmodels.CalculatorViewModel$fetchTokenPrice$1$1", f = "CalculatorViewModel.kt", l = {278, 287}, m = "invokeSuspend")
    /* renamed from: net.safemoon.androidwallet.viewmodels.CalculatorViewModel$fetchTokenPrice$1$1  reason: invalid class name */
    /* loaded from: classes2.dex */
    public static final class AnonymousClass1 extends SuspendLambda implements hd1<c90, q70<? super te4>, Object> {
        public final /* synthetic */ rc1<te4> $callBack;
        public final /* synthetic */ Ref$ObjectRef<String> $tokenAddress;
        public final /* synthetic */ String $tokenId;
        public Object L$0;
        public Object L$1;
        public int label;
        public final /* synthetic */ CalculatorViewModel this$0;

        /* compiled from: CalculatorViewModel.kt */
        @a(c = "net.safemoon.androidwallet.viewmodels.CalculatorViewModel$fetchTokenPrice$1$1$1", f = "CalculatorViewModel.kt", l = {}, m = "invokeSuspend")
        /* renamed from: net.safemoon.androidwallet.viewmodels.CalculatorViewModel$fetchTokenPrice$1$1$1  reason: invalid class name and collision with other inner class name */
        /* loaded from: classes2.dex */
        public static final class C02171 extends SuspendLambda implements hd1<c90, q70<? super te4>, Object> {
            public final /* synthetic */ rc1<te4> $callBack;
            public int label;

            /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
            public C02171(rc1<te4> rc1Var, q70<? super C02171> q70Var) {
                super(2, q70Var);
                this.$callBack = rc1Var;
            }

            @Override // kotlin.coroutines.jvm.internal.BaseContinuationImpl
            public final q70<te4> create(Object obj, q70<?> q70Var) {
                return new C02171(this.$callBack, q70Var);
            }

            @Override // defpackage.hd1
            public final Object invoke(c90 c90Var, q70<? super te4> q70Var) {
                return ((C02171) create(c90Var, q70Var)).invokeSuspend(te4.a);
            }

            @Override // kotlin.coroutines.jvm.internal.BaseContinuationImpl
            public final Object invokeSuspend(Object obj) {
                gs1.d();
                if (this.label == 0) {
                    o83.b(obj);
                    rc1<te4> rc1Var = this.$callBack;
                    if (rc1Var != null) {
                        rc1Var.invoke();
                    }
                    return te4.a;
                }
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
        }

        /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
        public AnonymousClass1(CalculatorViewModel calculatorViewModel, Ref$ObjectRef<String> ref$ObjectRef, String str, rc1<te4> rc1Var, q70<? super AnonymousClass1> q70Var) {
            super(2, q70Var);
            this.this$0 = calculatorViewModel;
            this.$tokenAddress = ref$ObjectRef;
            this.$tokenId = str;
            this.$callBack = rc1Var;
        }

        @Override // kotlin.coroutines.jvm.internal.BaseContinuationImpl
        public final q70<te4> create(Object obj, q70<?> q70Var) {
            return new AnonymousClass1(this.this$0, this.$tokenAddress, this.$tokenId, this.$callBack, q70Var);
        }

        @Override // defpackage.hd1
        public final Object invoke(c90 c90Var, q70<? super te4> q70Var) {
            return ((AnonymousClass1) create(c90Var, q70Var)).invokeSuspend(te4.a);
        }

        /* JADX WARN: Removed duplicated region for block: B:35:0x008f  */
        /* JADX WARN: Removed duplicated region for block: B:50:0x00dc A[Catch: Exception -> 0x00e9, TryCatch #0 {Exception -> 0x00e9, blocks: (B:7:0x0017, B:40:0x00ad, B:47:0x00c8, B:50:0x00dc, B:53:0x00e3, B:43:0x00b9, B:46:0x00c2), top: B:63:0x0017 }] */
        @Override // kotlin.coroutines.jvm.internal.BaseContinuationImpl
        /*
            Code decompiled incorrectly, please refer to instructions dump.
            To view partially-correct code enable 'Show inconsistent code' option in preferences
        */
        public final java.lang.Object invokeSuspend(java.lang.Object r12) {
            /*
                Method dump skipped, instructions count: 261
                To view this dump change 'Code comments level' option to 'DEBUG'
            */
            throw new UnsupportedOperationException("Method not decompiled: net.safemoon.androidwallet.viewmodels.CalculatorViewModel$fetchTokenPrice$1.AnonymousClass1.invokeSuspend(java.lang.Object):java.lang.Object");
        }
    }

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public CalculatorViewModel$fetchTokenPrice$1(CalculatorViewModel calculatorViewModel, Ref$ObjectRef<String> ref$ObjectRef, String str, rc1<te4> rc1Var, q70<? super CalculatorViewModel$fetchTokenPrice$1> q70Var) {
        super(2, q70Var);
        this.this$0 = calculatorViewModel;
        this.$tokenAddress = ref$ObjectRef;
        this.$tokenId = str;
        this.$callBack = rc1Var;
    }

    @Override // kotlin.coroutines.jvm.internal.BaseContinuationImpl
    public final q70<te4> create(Object obj, q70<?> q70Var) {
        return new CalculatorViewModel$fetchTokenPrice$1(this.this$0, this.$tokenAddress, this.$tokenId, this.$callBack, q70Var);
    }

    @Override // defpackage.hd1
    public final Object invoke(c90 c90Var, q70<? super te4> q70Var) {
        return ((CalculatorViewModel$fetchTokenPrice$1) create(c90Var, q70Var)).invokeSuspend(te4.a);
    }

    @Override // kotlin.coroutines.jvm.internal.BaseContinuationImpl
    public final Object invokeSuspend(Object obj) {
        Object d = gs1.d();
        int i = this.label;
        if (i == 0) {
            o83.b(obj);
            CoroutineDispatcher b = tp0.b();
            AnonymousClass1 anonymousClass1 = new AnonymousClass1(this.this$0, this.$tokenAddress, this.$tokenId, this.$callBack, null);
            this.label = 1;
            if (kotlinx.coroutines.a.e(b, anonymousClass1, this) == d) {
                return d;
            }
        } else if (i != 1) {
            throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        } else {
            o83.b(obj);
        }
        return te4.a;
    }
}
