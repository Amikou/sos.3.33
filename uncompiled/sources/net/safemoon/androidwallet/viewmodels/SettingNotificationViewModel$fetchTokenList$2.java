package net.safemoon.androidwallet.viewmodels;

import kotlin.coroutines.jvm.internal.SuspendLambda;
import kotlin.coroutines.jvm.internal.a;

/* compiled from: SettingNotificationViewModel.kt */
@a(c = "net.safemoon.androidwallet.viewmodels.SettingNotificationViewModel$fetchTokenList$2", f = "SettingNotificationViewModel.kt", l = {130, 138}, m = "invokeSuspend")
/* loaded from: classes2.dex */
public final class SettingNotificationViewModel$fetchTokenList$2 extends SuspendLambda implements hd1<c90, q70<? super te4>, Object> {
    public final /* synthetic */ int $chainId;
    public int label;
    public final /* synthetic */ SettingNotificationViewModel this$0;

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public SettingNotificationViewModel$fetchTokenList$2(SettingNotificationViewModel settingNotificationViewModel, int i, q70<? super SettingNotificationViewModel$fetchTokenList$2> q70Var) {
        super(2, q70Var);
        this.this$0 = settingNotificationViewModel;
        this.$chainId = i;
    }

    @Override // kotlin.coroutines.jvm.internal.BaseContinuationImpl
    public final q70<te4> create(Object obj, q70<?> q70Var) {
        return new SettingNotificationViewModel$fetchTokenList$2(this.this$0, this.$chainId, q70Var);
    }

    @Override // defpackage.hd1
    public final Object invoke(c90 c90Var, q70<? super te4> q70Var) {
        return ((SettingNotificationViewModel$fetchTokenList$2) create(c90Var, q70Var)).invokeSuspend(te4.a);
    }

    /* JADX WARN: Removed duplicated region for block: B:30:0x0079  */
    /* JADX WARN: Removed duplicated region for block: B:40:0x00ba A[LOOP:0: B:38:0x00b4->B:40:0x00ba, LOOP_END] */
    /* JADX WARN: Removed duplicated region for block: B:43:0x00f8  */
    @Override // kotlin.coroutines.jvm.internal.BaseContinuationImpl
    /*
        Code decompiled incorrectly, please refer to instructions dump.
        To view partially-correct code enable 'Show inconsistent code' option in preferences
    */
    public final java.lang.Object invokeSuspend(java.lang.Object r18) {
        /*
            Method dump skipped, instructions count: 270
            To view this dump change 'Code comments level' option to 'DEBUG'
        */
        throw new UnsupportedOperationException("Method not decompiled: net.safemoon.androidwallet.viewmodels.SettingNotificationViewModel$fetchTokenList$2.invokeSuspend(java.lang.Object):java.lang.Object");
    }
}
