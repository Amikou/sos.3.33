package net.safemoon.androidwallet.viewmodels;

import android.app.Application;
import kotlin.jvm.internal.Lambda;

/* compiled from: CustomContractTokenViewModel.kt */
/* loaded from: classes2.dex */
public final class CustomContractTokenViewModel$customTokenDataSource$2 extends Lambda implements rc1<yc0> {
    public final /* synthetic */ CustomContractTokenViewModel this$0;

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public CustomContractTokenViewModel$customTokenDataSource$2(CustomContractTokenViewModel customContractTokenViewModel) {
        super(0);
        this.this$0 = customContractTokenViewModel;
    }

    @Override // defpackage.rc1
    public final yc0 invoke() {
        zc0 zc0Var = zc0.a;
        Application a = this.this$0.a();
        fs1.e(a, "getApplication()");
        return zc0Var.a(a);
    }
}
