package net.safemoon.androidwallet.viewmodels;

import android.graphics.Bitmap;
import kotlin.coroutines.jvm.internal.SuspendLambda;
import kotlin.coroutines.jvm.internal.a;
import kotlinx.coroutines.CoroutineDispatcher;

/* compiled from: GoogleAuthViewModel.kt */
@a(c = "net.safemoon.androidwallet.viewmodels.GoogleAuthViewModel$generateAuthKey$1", f = "GoogleAuthViewModel.kt", l = {36}, m = "invokeSuspend")
/* loaded from: classes2.dex */
public final class GoogleAuthViewModel$generateAuthKey$1 extends SuspendLambda implements hd1<c90, q70<? super te4>, Object> {
    public int label;
    public final /* synthetic */ GoogleAuthViewModel this$0;

    /* compiled from: GoogleAuthViewModel.kt */
    @kotlin.coroutines.jvm.internal.a(c = "net.safemoon.androidwallet.viewmodels.GoogleAuthViewModel$generateAuthKey$1$1", f = "GoogleAuthViewModel.kt", l = {72}, m = "invokeSuspend")
    /* renamed from: net.safemoon.androidwallet.viewmodels.GoogleAuthViewModel$generateAuthKey$1$1  reason: invalid class name */
    /* loaded from: classes2.dex */
    public static final class AnonymousClass1 extends SuspendLambda implements hd1<c90, q70<? super te4>, Object> {
        public final /* synthetic */ e34 $totpData;
        public int label;
        public final /* synthetic */ GoogleAuthViewModel this$0;

        /* compiled from: GoogleAuthViewModel.kt */
        @kotlin.coroutines.jvm.internal.a(c = "net.safemoon.androidwallet.viewmodels.GoogleAuthViewModel$generateAuthKey$1$1$1", f = "GoogleAuthViewModel.kt", l = {38}, m = "invokeSuspend")
        /* renamed from: net.safemoon.androidwallet.viewmodels.GoogleAuthViewModel$generateAuthKey$1$1$1  reason: invalid class name and collision with other inner class name */
        /* loaded from: classes2.dex */
        public static final class C02241 extends SuspendLambda implements hd1<k71<? super Bitmap>, q70<? super te4>, Object> {
            public final /* synthetic */ e34 $totpData;
            private /* synthetic */ Object L$0;
            public int label;
            public final /* synthetic */ GoogleAuthViewModel this$0;

            /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
            public C02241(GoogleAuthViewModel googleAuthViewModel, e34 e34Var, q70<? super C02241> q70Var) {
                super(2, q70Var);
                this.this$0 = googleAuthViewModel;
                this.$totpData = e34Var;
            }

            @Override // kotlin.coroutines.jvm.internal.BaseContinuationImpl
            public final q70<te4> create(Object obj, q70<?> q70Var) {
                C02241 c02241 = new C02241(this.this$0, this.$totpData, q70Var);
                c02241.L$0 = obj;
                return c02241;
            }

            @Override // defpackage.hd1
            public final Object invoke(k71<? super Bitmap> k71Var, q70<? super te4> q70Var) {
                return ((C02241) create(k71Var, q70Var)).invokeSuspend(te4.a);
            }

            @Override // kotlin.coroutines.jvm.internal.BaseContinuationImpl
            public final Object invokeSuspend(Object obj) {
                Bitmap d;
                Object d2 = gs1.d();
                int i = this.label;
                if (i == 0) {
                    o83.b(obj);
                    d = this.this$0.d(this.$totpData);
                    this.label = 1;
                    if (((k71) this.L$0).emit(d, this) == d2) {
                        return d2;
                    }
                } else if (i != 1) {
                    throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
                } else {
                    o83.b(obj);
                }
                return te4.a;
            }
        }

        /* compiled from: GoogleAuthViewModel.kt */
        @kotlin.coroutines.jvm.internal.a(c = "net.safemoon.androidwallet.viewmodels.GoogleAuthViewModel$generateAuthKey$1$1$2", f = "GoogleAuthViewModel.kt", l = {}, m = "invokeSuspend")
        /* renamed from: net.safemoon.androidwallet.viewmodels.GoogleAuthViewModel$generateAuthKey$1$1$2  reason: invalid class name */
        /* loaded from: classes2.dex */
        public static final class AnonymousClass2 extends SuspendLambda implements kd1<k71<? super Bitmap>, Throwable, q70<? super te4>, Object> {
            public int label;

            public AnonymousClass2(q70<? super AnonymousClass2> q70Var) {
                super(3, q70Var);
            }

            @Override // defpackage.kd1
            public final Object invoke(k71<? super Bitmap> k71Var, Throwable th, q70<? super te4> q70Var) {
                return new AnonymousClass2(q70Var).invokeSuspend(te4.a);
            }

            @Override // kotlin.coroutines.jvm.internal.BaseContinuationImpl
            public final Object invokeSuspend(Object obj) {
                gs1.d();
                if (this.label == 0) {
                    o83.b(obj);
                    return te4.a;
                }
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
        }

        /* compiled from: Collect.kt */
        /* renamed from: net.safemoon.androidwallet.viewmodels.GoogleAuthViewModel$generateAuthKey$1$1$a */
        /* loaded from: classes2.dex */
        public static final class a implements k71<Bitmap> {
            public final /* synthetic */ GoogleAuthViewModel a;
            public final /* synthetic */ e34 f0;

            public a(GoogleAuthViewModel googleAuthViewModel, e34 e34Var) {
                this.a = googleAuthViewModel;
                this.f0 = e34Var;
            }

            @Override // defpackage.k71
            public Object emit(Bitmap bitmap, q70<? super te4> q70Var) {
                st1 b;
                b = as.b(ej4.a(this.a), null, null, new GoogleAuthViewModel$generateAuthKey$1$1$3$1(this.a, this.f0, bitmap, null), 3, null);
                return b == gs1.d() ? b : te4.a;
            }
        }

        /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
        public AnonymousClass1(GoogleAuthViewModel googleAuthViewModel, e34 e34Var, q70<? super AnonymousClass1> q70Var) {
            super(2, q70Var);
            this.this$0 = googleAuthViewModel;
            this.$totpData = e34Var;
        }

        @Override // kotlin.coroutines.jvm.internal.BaseContinuationImpl
        public final q70<te4> create(Object obj, q70<?> q70Var) {
            return new AnonymousClass1(this.this$0, this.$totpData, q70Var);
        }

        @Override // defpackage.hd1
        public final Object invoke(c90 c90Var, q70<? super te4> q70Var) {
            return ((AnonymousClass1) create(c90Var, q70Var)).invokeSuspend(te4.a);
        }

        @Override // kotlin.coroutines.jvm.internal.BaseContinuationImpl
        public final Object invokeSuspend(Object obj) {
            Object d = gs1.d();
            int i = this.label;
            if (i == 0) {
                o83.b(obj);
                j71 c = n71.c(n71.n(new C02241(this.this$0, this.$totpData, null)), new AnonymousClass2(null));
                a aVar = new a(this.this$0, this.$totpData);
                this.label = 1;
                if (c.a(aVar, this) == d) {
                    return d;
                }
            } else if (i != 1) {
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            } else {
                o83.b(obj);
            }
            return te4.a;
        }
    }

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public GoogleAuthViewModel$generateAuthKey$1(GoogleAuthViewModel googleAuthViewModel, q70<? super GoogleAuthViewModel$generateAuthKey$1> q70Var) {
        super(2, q70Var);
        this.this$0 = googleAuthViewModel;
    }

    @Override // kotlin.coroutines.jvm.internal.BaseContinuationImpl
    public final q70<te4> create(Object obj, q70<?> q70Var) {
        return new GoogleAuthViewModel$generateAuthKey$1(this.this$0, q70Var);
    }

    @Override // defpackage.hd1
    public final Object invoke(c90 c90Var, q70<? super te4> q70Var) {
        return ((GoogleAuthViewModel$generateAuthKey$1) create(c90Var, q70Var)).invokeSuspend(te4.a);
    }

    @Override // kotlin.coroutines.jvm.internal.BaseContinuationImpl
    public final Object invokeSuspend(Object obj) {
        Object d = gs1.d();
        int i = this.label;
        if (i == 0) {
            o83.b(obj);
            e34 e34Var = new e34("Safemoon", "Android", e34.a());
            CoroutineDispatcher b = tp0.b();
            AnonymousClass1 anonymousClass1 = new AnonymousClass1(this.this$0, e34Var, null);
            this.label = 1;
            if (kotlinx.coroutines.a.e(b, anonymousClass1, this) == d) {
                return d;
            }
        } else if (i != 1) {
            throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        } else {
            o83.b(obj);
        }
        return te4.a;
    }
}
