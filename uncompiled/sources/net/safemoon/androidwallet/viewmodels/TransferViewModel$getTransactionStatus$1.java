package net.safemoon.androidwallet.viewmodels;

import kotlin.coroutines.jvm.internal.SuspendLambda;
import kotlin.coroutines.jvm.internal.a;
import kotlinx.coroutines.CoroutineDispatcher;
import net.safemoon.androidwallet.common.TokenType;
import net.safemoon.androidwallet.model.ReceiptStatus;
import retrofit2.b;
import retrofit2.n;

/* compiled from: TransferViewModel.kt */
@a(c = "net.safemoon.androidwallet.viewmodels.TransferViewModel$getTransactionStatus$1", f = "TransferViewModel.kt", l = {71}, m = "invokeSuspend")
/* loaded from: classes2.dex */
public final class TransferViewModel$getTransactionStatus$1 extends SuspendLambda implements hd1<c90, q70<? super te4>, Object> {
    public final /* synthetic */ String $hash;
    public final /* synthetic */ TokenType $tokenType;
    public int label;
    public final /* synthetic */ TransferViewModel this$0;

    /* compiled from: TransferViewModel.kt */
    @kotlin.coroutines.jvm.internal.a(c = "net.safemoon.androidwallet.viewmodels.TransferViewModel$getTransactionStatus$1$1", f = "TransferViewModel.kt", l = {}, m = "invokeSuspend")
    /* renamed from: net.safemoon.androidwallet.viewmodels.TransferViewModel$getTransactionStatus$1$1  reason: invalid class name */
    /* loaded from: classes2.dex */
    public static final class AnonymousClass1 extends SuspendLambda implements hd1<c90, q70<? super te4>, Object> {
        public final /* synthetic */ String $hash;
        public final /* synthetic */ TokenType $tokenType;
        public int label;
        public final /* synthetic */ TransferViewModel this$0;

        /* compiled from: TransferViewModel.kt */
        /* renamed from: net.safemoon.androidwallet.viewmodels.TransferViewModel$getTransactionStatus$1$1$a */
        /* loaded from: classes2.dex */
        public static final class a implements wu<ReceiptStatus> {
            public final /* synthetic */ TransferViewModel a;

            public a(TransferViewModel transferViewModel) {
                this.a = transferViewModel;
            }

            @Override // defpackage.wu
            public void a(b<ReceiptStatus> bVar, Throwable th) {
                fs1.f(bVar, "call");
                fs1.f(th, "t");
            }

            @Override // defpackage.wu
            public void b(b<ReceiptStatus> bVar, n<ReceiptStatus> nVar) {
                fs1.f(bVar, "call");
                fs1.f(nVar, "response");
                if (nVar.e()) {
                    this.a.g().postValue(nVar.a());
                }
            }
        }

        /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
        public AnonymousClass1(TokenType tokenType, String str, TransferViewModel transferViewModel, q70<? super AnonymousClass1> q70Var) {
            super(2, q70Var);
            this.$tokenType = tokenType;
            this.$hash = str;
            this.this$0 = transferViewModel;
        }

        @Override // kotlin.coroutines.jvm.internal.BaseContinuationImpl
        public final q70<te4> create(Object obj, q70<?> q70Var) {
            return new AnonymousClass1(this.$tokenType, this.$hash, this.this$0, q70Var);
        }

        @Override // defpackage.hd1
        public final Object invoke(c90 c90Var, q70<? super te4> q70Var) {
            return ((AnonymousClass1) create(c90Var, q70Var)).invokeSuspend(te4.a);
        }

        @Override // kotlin.coroutines.jvm.internal.BaseContinuationImpl
        public final Object invokeSuspend(Object obj) {
            gs1.d();
            if (this.label == 0) {
                o83.b(obj);
                b<ReceiptStatus> a2 = e30.s(this.$tokenType).a(this.$hash, e30.x(this.$tokenType));
                if (a2 != null) {
                    a2.n(new a(this.this$0));
                }
                return te4.a;
            }
            throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        }
    }

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public TransferViewModel$getTransactionStatus$1(TokenType tokenType, String str, TransferViewModel transferViewModel, q70<? super TransferViewModel$getTransactionStatus$1> q70Var) {
        super(2, q70Var);
        this.$tokenType = tokenType;
        this.$hash = str;
        this.this$0 = transferViewModel;
    }

    @Override // kotlin.coroutines.jvm.internal.BaseContinuationImpl
    public final q70<te4> create(Object obj, q70<?> q70Var) {
        return new TransferViewModel$getTransactionStatus$1(this.$tokenType, this.$hash, this.this$0, q70Var);
    }

    @Override // defpackage.hd1
    public final Object invoke(c90 c90Var, q70<? super te4> q70Var) {
        return ((TransferViewModel$getTransactionStatus$1) create(c90Var, q70Var)).invokeSuspend(te4.a);
    }

    @Override // kotlin.coroutines.jvm.internal.BaseContinuationImpl
    public final Object invokeSuspend(Object obj) {
        Object d = gs1.d();
        int i = this.label;
        if (i == 0) {
            o83.b(obj);
            CoroutineDispatcher b = tp0.b();
            AnonymousClass1 anonymousClass1 = new AnonymousClass1(this.$tokenType, this.$hash, this.this$0, null);
            this.label = 1;
            if (kotlinx.coroutines.a.e(b, anonymousClass1, this) == d) {
                return d;
            }
        } else if (i != 1) {
            throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        } else {
            o83.b(obj);
        }
        return te4.a;
    }
}
