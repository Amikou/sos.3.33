package net.safemoon.androidwallet.viewmodels;

import kotlin.coroutines.jvm.internal.SuspendLambda;
import kotlin.coroutines.jvm.internal.a;
import net.safemoon.androidwallet.model.priceAlert.PAToken;

/* compiled from: CryptoPriceAlertViewModel.kt */
@a(c = "net.safemoon.androidwallet.viewmodels.CryptoPriceAlertViewModel$fetchPriceAlert$1", f = "CryptoPriceAlertViewModel.kt", l = {63, 68}, m = "invokeSuspend")
/* loaded from: classes2.dex */
public final class CryptoPriceAlertViewModel$fetchPriceAlert$1 extends SuspendLambda implements hd1<c90, q70<? super te4>, Object> {
    public final /* synthetic */ PAToken $paToken;
    public int I$0;
    public Object L$0;
    public Object L$1;
    public Object L$2;
    public int label;
    public final /* synthetic */ CryptoPriceAlertViewModel this$0;

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public CryptoPriceAlertViewModel$fetchPriceAlert$1(CryptoPriceAlertViewModel cryptoPriceAlertViewModel, PAToken pAToken, q70<? super CryptoPriceAlertViewModel$fetchPriceAlert$1> q70Var) {
        super(2, q70Var);
        this.this$0 = cryptoPriceAlertViewModel;
        this.$paToken = pAToken;
    }

    @Override // kotlin.coroutines.jvm.internal.BaseContinuationImpl
    public final q70<te4> create(Object obj, q70<?> q70Var) {
        return new CryptoPriceAlertViewModel$fetchPriceAlert$1(this.this$0, this.$paToken, q70Var);
    }

    @Override // defpackage.hd1
    public final Object invoke(c90 c90Var, q70<? super te4> q70Var) {
        return ((CryptoPriceAlertViewModel$fetchPriceAlert$1) create(c90Var, q70Var)).invokeSuspend(te4.a);
    }

    /* JADX WARN: Removed duplicated region for block: B:36:0x00ba  */
    @Override // kotlin.coroutines.jvm.internal.BaseContinuationImpl
    /*
        Code decompiled incorrectly, please refer to instructions dump.
        To view partially-correct code enable 'Show inconsistent code' option in preferences
    */
    public final java.lang.Object invokeSuspend(java.lang.Object r15) {
        /*
            r14 = this;
            java.lang.Object r0 = defpackage.gs1.d()
            int r1 = r14.label
            r2 = 2
            r3 = 0
            r4 = 1
            r5 = 0
            if (r1 == 0) goto L34
            if (r1 == r4) goto L1d
            if (r1 != r2) goto L15
            defpackage.o83.b(r15)
            goto Lb5
        L15:
            java.lang.IllegalStateException r15 = new java.lang.IllegalStateException
            java.lang.String r0 = "call to 'resume' before 'invoke' with coroutine"
            r15.<init>(r0)
            throw r15
        L1d:
            int r1 = r14.I$0
            java.lang.Object r6 = r14.L$2
            java.lang.String r6 = (java.lang.String) r6
            java.lang.Object r7 = r14.L$1
            java.lang.String r7 = (java.lang.String) r7
            java.lang.Object r8 = r14.L$0
            java.lang.String r8 = (java.lang.String) r8
            defpackage.o83.b(r15)
        L2e:
            r10 = r1
            r9 = r6
            r13 = r8
            r8 = r7
            r7 = r13
            goto L7d
        L34:
            defpackage.o83.b(r15)
            net.safemoon.androidwallet.viewmodels.CryptoPriceAlertViewModel r15 = r14.this$0
            r15.x()
            net.safemoon.androidwallet.model.priceAlert.PAToken r15 = r14.$paToken
            java.lang.String r15 = r15.getSymbol()
            if (r15 != 0) goto L46
            java.lang.String r15 = ""
        L46:
            r8 = r15
            net.safemoon.androidwallet.model.priceAlert.PAToken r15 = r14.$paToken
            java.lang.String r7 = r15.getName()
            net.safemoon.androidwallet.model.priceAlert.PAToken r15 = r14.$paToken
            java.lang.String r15 = r15.getContractAddress()
            if (r15 != 0) goto L57
            r6 = r5
            goto L64
        L57:
            int r1 = r15.length()
            if (r1 != 0) goto L5f
            r1 = r4
            goto L60
        L5f:
            r1 = r3
        L60:
            if (r1 == 0) goto L63
            r15 = r5
        L63:
            r6 = r15
        L64:
            net.safemoon.androidwallet.model.priceAlert.PAToken r15 = r14.$paToken
            int r1 = r15.getChainId()
            net.safemoon.androidwallet.viewmodels.CryptoPriceAlertViewModel r15 = r14.this$0
            r14.L$0 = r8
            r14.L$1 = r7
            r14.L$2 = r6
            r14.I$0 = r1
            r14.label = r4
            java.lang.Object r15 = net.safemoon.androidwallet.viewmodels.CryptoPriceAlertViewModel.g(r15, r6, r8, r14)
            if (r15 != r0) goto L2e
            return r0
        L7d:
            java.util.ArrayList r15 = (java.util.ArrayList) r15
            boolean r1 = r15.isEmpty()
            r1 = r1 ^ r4
            if (r1 == 0) goto L9d
            net.safemoon.androidwallet.viewmodels.CryptoPriceAlertViewModel r0 = r14.this$0
            gb2 r0 = r0.r()
            r0.postValue(r15)
            net.safemoon.androidwallet.viewmodels.CryptoPriceAlertViewModel r15 = r14.this$0
            gb2 r15 = r15.t()
            java.lang.Integer r0 = defpackage.hr.d(r3)
            r15.postValue(r0)
            goto Ld2
        L9d:
            net.safemoon.androidwallet.viewmodels.CryptoPriceAlertViewModel r6 = r14.this$0
            net.safemoon.androidwallet.model.priceAlert.PAToken r15 = r14.$paToken
            java.lang.Integer r11 = r15.getCmcId()
            r14.L$0 = r5
            r14.L$1 = r5
            r14.L$2 = r5
            r14.label = r2
            r12 = r14
            java.lang.Object r15 = net.safemoon.androidwallet.viewmodels.CryptoPriceAlertViewModel.f(r6, r7, r8, r9, r10, r11, r12)
            if (r15 != r0) goto Lb5
            return r0
        Lb5:
            net.safemoon.androidwallet.model.priceAlert.PriceAlertToken r15 = (net.safemoon.androidwallet.model.priceAlert.PriceAlertToken) r15
            if (r15 != 0) goto Lba
            goto Ld2
        Lba:
            net.safemoon.androidwallet.viewmodels.CryptoPriceAlertViewModel r0 = r14.this$0
            gb2 r1 = r0.r()
            java.util.List r15 = defpackage.a20.b(r15)
            r1.postValue(r15)
            gb2 r15 = r0.t()
            java.lang.Integer r0 = defpackage.hr.d(r3)
            r15.postValue(r0)
        Ld2:
            net.safemoon.androidwallet.viewmodels.CryptoPriceAlertViewModel r15 = r14.this$0
            r15.y()
            te4 r15 = defpackage.te4.a
            return r15
        */
        throw new UnsupportedOperationException("Method not decompiled: net.safemoon.androidwallet.viewmodels.CryptoPriceAlertViewModel$fetchPriceAlert$1.invokeSuspend(java.lang.Object):java.lang.Object");
    }
}
