package net.safemoon.androidwallet.viewmodels;

import android.app.Application;
import kotlin.jvm.internal.Lambda;

/* compiled from: MultiWalletViewModel.kt */
/* loaded from: classes2.dex */
public final class MultiWalletViewModel$registerDeviceTokenUseCase$2 extends Lambda implements rc1<g63> {
    public final /* synthetic */ MultiWalletViewModel this$0;

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public MultiWalletViewModel$registerDeviceTokenUseCase$2(MultiWalletViewModel multiWalletViewModel) {
        super(0);
        this.this$0 = multiWalletViewModel;
    }

    /* JADX WARN: Can't rename method to resolve collision */
    @Override // defpackage.rc1
    public final g63 invoke() {
        ac3 m = a4.m();
        fs1.e(m, "getSafeMoonClient()");
        Application a = this.this$0.a();
        fs1.e(a, "getApplication()");
        return new g63(m, a);
    }
}
