package net.safemoon.androidwallet.viewmodels;

import kotlin.jvm.internal.Lambda;
import net.safemoon.androidwallet.database.mainRoom.MainRoomDatabase;
import net.safemoon.androidwallet.repository.WalletDataSource;

/* compiled from: MultiWalletViewModel.kt */
/* loaded from: classes2.dex */
public final class MultiWalletViewModel$walletDataSource$2 extends Lambda implements rc1<WalletDataSource> {
    public final /* synthetic */ MultiWalletViewModel this$0;

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public MultiWalletViewModel$walletDataSource$2(MultiWalletViewModel multiWalletViewModel) {
        super(0);
        this.this$0 = multiWalletViewModel;
    }

    @Override // defpackage.rc1
    public final WalletDataSource invoke() {
        MainRoomDatabase q;
        q = this.this$0.q();
        return new WalletDataSource(q.P());
    }
}
