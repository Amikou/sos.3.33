package net.safemoon.androidwallet.viewmodels;

import android.app.Application;
import android.graphics.Bitmap;
import java.util.ArrayList;
import net.safemoon.androidwallet.common.TokenType;
import net.safemoon.androidwallet.model.cmcTokenInfo.CmcTokenInfo;
import net.safemoon.androidwallet.model.cmcTokenInfo.TokenDetail;
import net.safemoon.androidwallet.model.transaction.history.Result;
import net.safemoon.androidwallet.model.transaction.history.TransactionHistoryModel;
import retrofit2.n;

/* compiled from: CustomContractTokenViewModel.kt */
/* loaded from: classes2.dex */
public final class CustomContractTokenViewModel extends gd {
    public final rc1<te4> b;
    public final rc1<te4> c;
    public final sy1 d;
    public final sy1 e;
    public final sy1 f;
    public final gb2<TokenType> g;
    public final gb2<TransactionHistoryModel> h;
    public final gb2<TokenDetail> i;
    public final gb2<Bitmap> j;
    public retrofit2.b<TransactionHistoryModel> k;
    public retrofit2.b<CmcTokenInfo> l;

    /* compiled from: CustomContractTokenViewModel.kt */
    /* loaded from: classes2.dex */
    public enum SaveReturnCode {
        ERROR,
        SUCCESS,
        PROGRESS,
        BLACKLIST
    }

    /* compiled from: CustomContractTokenViewModel.kt */
    /* loaded from: classes2.dex */
    public static final class a {
        public a() {
        }

        public /* synthetic */ a(qi0 qi0Var) {
            this();
        }
    }

    /* compiled from: CustomContractTokenViewModel.kt */
    /* loaded from: classes2.dex */
    public static final class b implements wu<TransactionHistoryModel> {

        /* compiled from: CustomContractTokenViewModel.kt */
        /* loaded from: classes2.dex */
        public /* synthetic */ class a {
            public static final /* synthetic */ int[] a;

            static {
                int[] iArr = new int[TokenType.values().length];
                iArr[TokenType.BEP_20.ordinal()] = 1;
                iArr[TokenType.ERC_20.ordinal()] = 2;
                a = iArr;
            }
        }

        public b() {
        }

        @Override // defpackage.wu
        public void a(retrofit2.b<TransactionHistoryModel> bVar, Throwable th) {
            fs1.f(bVar, "call");
            fs1.f(th, "t");
            fs1.l("onFailure: ", th.getMessage());
        }

        @Override // defpackage.wu
        public void b(retrofit2.b<TransactionHistoryModel> bVar, n<TransactionHistoryModel> nVar) {
            fs1.f(bVar, "call");
            fs1.f(nVar, "response");
            TransactionHistoryModel a2 = nVar.a();
            if (a2 == null) {
                return;
            }
            CustomContractTokenViewModel customContractTokenViewModel = CustomContractTokenViewModel.this;
            if (a2.result.size() > 0) {
                customContractTokenViewModel.s().postValue(a2);
                return;
            }
            customContractTokenViewModel.s().postValue(null);
            TokenType value = customContractTokenViewModel.q().getValue();
            int i = value == null ? -1 : a.a[value.ordinal()];
            if (i == 1) {
                customContractTokenViewModel.c.invoke();
            } else if (i != 2) {
            } else {
                customContractTokenViewModel.b.invoke();
            }
        }
    }

    /* compiled from: CustomContractTokenViewModel.kt */
    /* loaded from: classes2.dex */
    public static final class c implements wu<CmcTokenInfo> {
        public final /* synthetic */ String b;

        public c(String str) {
            this.b = str;
        }

        @Override // defpackage.wu
        public void a(retrofit2.b<CmcTokenInfo> bVar, Throwable th) {
            fs1.f(bVar, "call");
            fs1.f(th, "t");
            fs1.l("onFailure: ", th.getMessage());
        }

        @Override // defpackage.wu
        public void b(retrofit2.b<CmcTokenInfo> bVar, n<CmcTokenInfo> nVar) {
            fs1.f(bVar, "call");
            fs1.f(nVar, "response");
            if (nVar.e()) {
                CmcTokenInfo a = nVar.a();
                if (a == null) {
                    return;
                }
                CustomContractTokenViewModel customContractTokenViewModel = CustomContractTokenViewModel.this;
                String str = this.b;
                try {
                    customContractTokenViewModel.t().postValue(a.getTokenDetails().get(0));
                    customContractTokenViewModel.o().postValue(null);
                    return;
                } catch (Exception unused) {
                    customContractTokenViewModel.k(str);
                    return;
                }
            }
            CustomContractTokenViewModel.this.k(this.b);
        }
    }

    static {
        new a(null);
    }

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public CustomContractTokenViewModel(Application application, rc1<te4> rc1Var, rc1<te4> rc1Var2) {
        super(application);
        fs1.f(application, "application");
        fs1.f(rc1Var, "showWrongERCAddressPopUp");
        fs1.f(rc1Var2, "showWrongBepAddressPopUp");
        this.b = rc1Var;
        this.c = rc1Var2;
        this.d = zy1.a(new CustomContractTokenViewModel$customTokenDataSource$2(this));
        this.e = zy1.a(new CustomContractTokenViewModel$localUserTokenDataSource$2(this));
        this.f = zy1.a(new CustomContractTokenViewModel$tokenInfoDataSource$2(this));
        this.g = new gb2<>(i());
        this.h = new gb2<>();
        this.i = new gb2<>();
        this.j = new gb2<>();
    }

    public final void h() {
        retrofit2.b<CmcTokenInfo> bVar = this.l;
        if (bVar != null) {
            bVar.cancel();
        }
        retrofit2.b<TransactionHistoryModel> bVar2 = this.k;
        if (bVar2 != null) {
            bVar2.cancel();
        }
        this.h.postValue(null);
        this.i.postValue(null);
        this.j.postValue(null);
    }

    public final TokenType i() {
        TokenType.a aVar = TokenType.Companion;
        String i = bo3.i(a(), "DEFAULT_GATEWAY");
        fs1.e(i, "getString(getApplication…redPrefs.DEFAULT_GATEWAY)");
        return aVar.c(i);
    }

    public final void j(String str) {
        fs1.f(str, "symbolWithType");
        kotlinx.coroutines.a.b(qg1.a, null, null, new CustomContractTokenViewModel$deleteSymbolWithType$1(this, str, null), 3, null);
    }

    public final void k(String str) {
        fs1.f(str, "contractAddress");
        kotlinx.coroutines.a.b(ej4.a(this), null, null, new CustomContractTokenViewModel$generateIdentiIcon$1(this, str, null), 3, null);
    }

    public final void l(String str) {
        fs1.f(str, "contractAddress");
        TokenType r = r();
        if (r != null) {
            retrofit2.b<TransactionHistoryModel> bVar = this.k;
            if (bVar != null) {
                bVar.cancel();
            }
            retrofit2.b<TransactionHistoryModel> g = e30.s(r).g(str, e30.x(r));
            this.k = g;
            if (g != null) {
                g.n(new b());
            }
        }
        retrofit2.b<CmcTokenInfo> bVar2 = this.l;
        if (bVar2 != null) {
            bVar2.cancel();
        }
        jt e = a4.e();
        retrofit2.b<CmcTokenInfo> d = e == null ? null : e.d(str);
        this.l = d;
        if (d == null) {
            return;
        }
        d.n(new c(str));
    }

    public final Result m() {
        ArrayList<Result> arrayList;
        TransactionHistoryModel value = this.h.getValue();
        if (value == null || (arrayList = value.result) == null) {
            return null;
        }
        return arrayList.get(0);
    }

    public final yc0 n() {
        return (yc0) this.d.getValue();
    }

    public final gb2<Bitmap> o() {
        return this.j;
    }

    public final bn1 p() {
        return (bn1) this.e.getValue();
    }

    public final gb2<TokenType> q() {
        return this.g;
    }

    public final TokenType r() {
        return this.g.getValue();
    }

    public final gb2<TransactionHistoryModel> s() {
        return this.h;
    }

    public final gb2<TokenDetail> t() {
        return this.i;
    }

    public final x64 u() {
        return (x64) this.f.getValue();
    }

    /* JADX WARN: Can't wrap try/catch for region: R(12:1|(2:3|(10:5|6|7|(1:(2:10|11)(2:38|39))(3:40|41|(1:43))|12|(1:14)(1:37)|(3:28|(2:29|(2:31|(2:33|34))(1:36))|35)|(2:18|(1:20))|22|23))|48|6|7|(0)(0)|12|(0)(0)|(1:16)(4:25|28|(3:29|(0)(0)|32)|35)|(0)|22|23) */
    /* JADX WARN: Code restructure failed: missing block: B:40:0x009d, code lost:
        r7 = move-exception;
     */
    /* JADX WARN: Code restructure failed: missing block: B:41:0x009e, code lost:
        r7 = r7.getMessage();
     */
    /* JADX WARN: Code restructure failed: missing block: B:42:0x00a2, code lost:
        if (r7 != null) goto L47;
     */
    /* JADX WARN: Code restructure failed: missing block: B:44:0x00a5, code lost:
        defpackage.e30.c0(r7, "Err-BlackListed");
     */
    /* JADX WARN: Removed duplicated region for block: B:10:0x0024  */
    /* JADX WARN: Removed duplicated region for block: B:16:0x0036  */
    /* JADX WARN: Removed duplicated region for block: B:22:0x005e A[Catch: Exception -> 0x009d, TryCatch #0 {Exception -> 0x009d, blocks: (B:12:0x002a, B:20:0x0055, B:22:0x005e, B:37:0x0093, B:26:0x006c, B:29:0x0071, B:30:0x0079, B:32:0x007f, B:35:0x008f, B:23:0x0065, B:17:0x0039), top: B:47:0x0022 }] */
    /* JADX WARN: Removed duplicated region for block: B:23:0x0065 A[Catch: Exception -> 0x009d, TryCatch #0 {Exception -> 0x009d, blocks: (B:12:0x002a, B:20:0x0055, B:22:0x005e, B:37:0x0093, B:26:0x006c, B:29:0x0071, B:30:0x0079, B:32:0x007f, B:35:0x008f, B:23:0x0065, B:17:0x0039), top: B:47:0x0022 }] */
    /* JADX WARN: Removed duplicated region for block: B:32:0x007f A[Catch: Exception -> 0x009d, TryCatch #0 {Exception -> 0x009d, blocks: (B:12:0x002a, B:20:0x0055, B:22:0x005e, B:37:0x0093, B:26:0x006c, B:29:0x0071, B:30:0x0079, B:32:0x007f, B:35:0x008f, B:23:0x0065, B:17:0x0039), top: B:47:0x0022 }] */
    /* JADX WARN: Removed duplicated region for block: B:37:0x0093 A[Catch: Exception -> 0x009d, TRY_LEAVE, TryCatch #0 {Exception -> 0x009d, blocks: (B:12:0x002a, B:20:0x0055, B:22:0x005e, B:37:0x0093, B:26:0x006c, B:29:0x0071, B:30:0x0079, B:32:0x007f, B:35:0x008f, B:23:0x0065, B:17:0x0039), top: B:47:0x0022 }] */
    /* JADX WARN: Removed duplicated region for block: B:50:0x008f A[EDGE_INSN: B:50:0x008f->B:35:0x008f ?: BREAK  , SYNTHETIC] */
    /*
        Code decompiled incorrectly, please refer to instructions dump.
        To view partially-correct code enable 'Show inconsistent code' option in preferences
    */
    public final java.lang.Object v(java.lang.String r7, defpackage.q70<? super java.lang.Boolean> r8) {
        /*
            r6 = this;
            boolean r0 = r8 instanceof net.safemoon.androidwallet.viewmodels.CustomContractTokenViewModel$isBlackListed$1
            if (r0 == 0) goto L13
            r0 = r8
            net.safemoon.androidwallet.viewmodels.CustomContractTokenViewModel$isBlackListed$1 r0 = (net.safemoon.androidwallet.viewmodels.CustomContractTokenViewModel$isBlackListed$1) r0
            int r1 = r0.label
            r2 = -2147483648(0xffffffff80000000, float:-0.0)
            r3 = r1 & r2
            if (r3 == 0) goto L13
            int r1 = r1 - r2
            r0.label = r1
            goto L18
        L13:
            net.safemoon.androidwallet.viewmodels.CustomContractTokenViewModel$isBlackListed$1 r0 = new net.safemoon.androidwallet.viewmodels.CustomContractTokenViewModel$isBlackListed$1
            r0.<init>(r6, r8)
        L18:
            java.lang.Object r8 = r0.result
            java.lang.Object r1 = defpackage.gs1.d()
            int r2 = r0.label
            r3 = 0
            r4 = 1
            if (r2 == 0) goto L36
            if (r2 != r4) goto L2e
            java.lang.Object r7 = r0.L$0
            java.lang.String r7 = (java.lang.String) r7
            defpackage.o83.b(r8)     // Catch: java.lang.Exception -> L9d
            goto L55
        L2e:
            java.lang.IllegalStateException r7 = new java.lang.IllegalStateException
            java.lang.String r8 = "call to 'resume' before 'invoke' with coroutine"
            r7.<init>(r8)
            throw r7
        L36:
            defpackage.o83.b(r8)
            e42 r8 = defpackage.a4.k()     // Catch: java.lang.Exception -> L9d
            net.safemoon.androidwallet.model.blackListTokens.Request r2 = new net.safemoon.androidwallet.model.blackListTokens.Request     // Catch: java.lang.Exception -> L9d
            java.util.List r5 = defpackage.a20.b(r7)     // Catch: java.lang.Exception -> L9d
            r2.<init>(r5)     // Catch: java.lang.Exception -> L9d
            retrofit2.b r8 = r8.j(r2)     // Catch: java.lang.Exception -> L9d
            r0.L$0 = r7     // Catch: java.lang.Exception -> L9d
            r0.label = r4     // Catch: java.lang.Exception -> L9d
            java.lang.Object r8 = retrofit2.KotlinExtensions.c(r8, r0)     // Catch: java.lang.Exception -> L9d
            if (r8 != r1) goto L55
            return r1
        L55:
            retrofit2.n r8 = (retrofit2.n) r8     // Catch: java.lang.Exception -> L9d
            boolean r0 = r8.e()     // Catch: java.lang.Exception -> L9d
            r1 = 0
            if (r0 == 0) goto L65
            java.lang.Object r8 = r8.a()     // Catch: java.lang.Exception -> L9d
            net.safemoon.androidwallet.model.blackListTokens.BlackListTokens r8 = (net.safemoon.androidwallet.model.blackListTokens.BlackListTokens) r8     // Catch: java.lang.Exception -> L9d
            goto L69
        L65:
            r8.d()     // Catch: java.lang.Exception -> L9d
            r8 = r1
        L69:
            if (r8 != 0) goto L6c
            goto L91
        L6c:
            java.util.List<net.safemoon.androidwallet.model.blackListTokens.BlackListTokens$Data> r8 = r8.data     // Catch: java.lang.Exception -> L9d
            if (r8 != 0) goto L71
            goto L91
        L71:
            int r0 = r8.size()     // Catch: java.lang.Exception -> L9d
            java.util.ListIterator r8 = r8.listIterator(r0)     // Catch: java.lang.Exception -> L9d
        L79:
            boolean r0 = r8.hasPrevious()     // Catch: java.lang.Exception -> L9d
            if (r0 == 0) goto L8f
            java.lang.Object r0 = r8.previous()     // Catch: java.lang.Exception -> L9d
            r2 = r0
            net.safemoon.androidwallet.model.blackListTokens.BlackListTokens$Data r2 = (net.safemoon.androidwallet.model.blackListTokens.BlackListTokens.Data) r2     // Catch: java.lang.Exception -> L9d
            java.lang.String r2 = r2.tokenContractAddress     // Catch: java.lang.Exception -> L9d
            boolean r2 = defpackage.fs1.b(r2, r7)     // Catch: java.lang.Exception -> L9d
            if (r2 == 0) goto L79
            r1 = r0
        L8f:
            net.safemoon.androidwallet.model.blackListTokens.BlackListTokens$Data r1 = (net.safemoon.androidwallet.model.blackListTokens.BlackListTokens.Data) r1     // Catch: java.lang.Exception -> L9d
        L91:
            if (r1 == 0) goto Laa
            java.lang.Boolean r7 = r1.isBlacklist     // Catch: java.lang.Exception -> L9d
            boolean r7 = r7.booleanValue()     // Catch: java.lang.Exception -> L9d
            if (r7 == 0) goto Laa
            r3 = r4
            goto Laa
        L9d:
            r7 = move-exception
            java.lang.String r7 = r7.getMessage()
            if (r7 != 0) goto La5
            goto Laa
        La5:
            java.lang.String r8 = "Err-BlackListed"
            defpackage.e30.c0(r7, r8)
        Laa:
            java.lang.Boolean r7 = defpackage.hr.a(r3)
            return r7
        */
        throw new UnsupportedOperationException("Method not decompiled: net.safemoon.androidwallet.viewmodels.CustomContractTokenViewModel.v(java.lang.String, q70):java.lang.Object");
    }

    public final void w(String str, String str2, String str3, String str4, tc1<? super SaveReturnCode, te4> tc1Var) {
        fs1.f(tc1Var, "callBack");
        if (str == null || str2 == null || str3 == null || str4 == null) {
            return;
        }
        TokenType r = r();
        if ((r == null ? null : Integer.valueOf(r.getChainId())) != null) {
            kotlinx.coroutines.a.b(ej4.a(this), null, null, new CustomContractTokenViewModel$save$1(this, str, tc1Var, str2, str3, str4, null), 3, null);
        }
    }
}
