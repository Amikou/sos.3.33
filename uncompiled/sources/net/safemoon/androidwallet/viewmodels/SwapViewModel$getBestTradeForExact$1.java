package net.safemoon.androidwallet.viewmodels;

import kotlin.coroutines.jvm.internal.ContinuationImpl;
import kotlin.coroutines.jvm.internal.a;

/* compiled from: SwapViewModel.kt */
@a(c = "net.safemoon.androidwallet.viewmodels.SwapViewModel", f = "SwapViewModel.kt", l = {1476, 1488}, m = "getBestTradeForExact")
/* loaded from: classes2.dex */
public final class SwapViewModel$getBestTradeForExact$1 extends ContinuationImpl {
    public Object L$0;
    public Object L$1;
    public Object L$2;
    public Object L$3;
    public Object L$4;
    public int label;
    public /* synthetic */ Object result;
    public final /* synthetic */ SwapViewModel this$0;

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public SwapViewModel$getBestTradeForExact$1(SwapViewModel swapViewModel, q70<? super SwapViewModel$getBestTradeForExact$1> q70Var) {
        super(q70Var);
        this.this$0 = swapViewModel;
    }

    @Override // kotlin.coroutines.jvm.internal.BaseContinuationImpl
    public final Object invokeSuspend(Object obj) {
        Object S;
        this.result = obj;
        this.label |= Integer.MIN_VALUE;
        S = this.this$0.S(null, this);
        return S;
    }
}
