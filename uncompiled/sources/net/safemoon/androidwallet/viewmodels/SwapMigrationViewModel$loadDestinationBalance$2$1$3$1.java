package net.safemoon.androidwallet.viewmodels;

import com.github.mikephil.charting.utils.Utils;
import kotlin.coroutines.jvm.internal.SuspendLambda;
import kotlin.coroutines.jvm.internal.a;
import net.safemoon.androidwallet.model.swap.Swap;
import net.safemoon.androidwallet.model.tokensInfo.CurrencyTokenInfo;
import net.safemoon.androidwallet.model.tokensInfo.CurrencyTokenInfoResult;

/* compiled from: SwapMigrationViewModel.kt */
@a(c = "net.safemoon.androidwallet.viewmodels.SwapMigrationViewModel$loadDestinationBalance$2$1$3$1", f = "SwapMigrationViewModel.kt", l = {}, m = "invokeSuspend")
/* loaded from: classes2.dex */
public final class SwapMigrationViewModel$loadDestinationBalance$2$1$3$1 extends SuspendLambda implements hd1<c90, q70<? super te4>, Object> {
    public final /* synthetic */ CurrencyTokenInfoResult $it;
    public final /* synthetic */ Swap $swap;
    public int label;
    public final /* synthetic */ SwapMigrationViewModel this$0;

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public SwapMigrationViewModel$loadDestinationBalance$2$1$3$1(SwapMigrationViewModel swapMigrationViewModel, CurrencyTokenInfoResult currencyTokenInfoResult, Swap swap, q70<? super SwapMigrationViewModel$loadDestinationBalance$2$1$3$1> q70Var) {
        super(2, q70Var);
        this.this$0 = swapMigrationViewModel;
        this.$it = currencyTokenInfoResult;
        this.$swap = swap;
    }

    @Override // kotlin.coroutines.jvm.internal.BaseContinuationImpl
    public final q70<te4> create(Object obj, q70<?> q70Var) {
        return new SwapMigrationViewModel$loadDestinationBalance$2$1$3$1(this.this$0, this.$it, this.$swap, q70Var);
    }

    @Override // defpackage.hd1
    public final Object invoke(c90 c90Var, q70<? super te4> q70Var) {
        return ((SwapMigrationViewModel$loadDestinationBalance$2$1$3$1) create(c90Var, q70Var)).invokeSuspend(te4.a);
    }

    @Override // kotlin.coroutines.jvm.internal.BaseContinuationImpl
    public final Object invokeSuspend(Object obj) {
        CurrencyTokenInfo data;
        gs1.d();
        if (this.label == 0) {
            o83.b(obj);
            try {
                this.this$0.L().setValue(hr.b(Utils.DOUBLE_EPSILON));
                CurrencyTokenInfoResult currencyTokenInfoResult = this.$it;
                CurrencyTokenInfo currencyTokenInfo = null;
                if (currencyTokenInfoResult != null && (data = currencyTokenInfoResult.getData()) != null) {
                    gb2<Double> L = this.this$0.L();
                    String priceUsd = data.getPriceUsd();
                    L.setValue(priceUsd == null ? null : hr.b(Double.parseDouble(priceUsd)));
                }
                CurrencyTokenInfoResult currencyTokenInfoResult2 = this.$it;
                if (currencyTokenInfoResult2 != null) {
                    currencyTokenInfo = currencyTokenInfoResult2.getData();
                }
                if (currencyTokenInfo == null) {
                    this.this$0.M(this.$swap);
                }
            } catch (Exception unused) {
                this.this$0.L().setValue(hr.b(Utils.DOUBLE_EPSILON));
            }
            return te4.a;
        }
        throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
    }
}
