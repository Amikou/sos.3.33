package net.safemoon.androidwallet.viewmodels;

import kotlin.coroutines.jvm.internal.SuspendLambda;
import kotlin.coroutines.jvm.internal.a;
import kotlinx.coroutines.CoroutineDispatcher;

/* compiled from: CollectibleViewModel.kt */
@a(c = "net.safemoon.androidwallet.viewmodels.CollectibleViewModel$estimateGas$1", f = "CollectibleViewModel.kt", l = {241}, m = "invokeSuspend")
/* loaded from: classes2.dex */
public final class CollectibleViewModel$estimateGas$1 extends SuspendLambda implements hd1<c90, q70<? super te4>, Object> {
    public final /* synthetic */ im1 $iNFTWeb;
    public final /* synthetic */ String $toAddress;
    public final /* synthetic */ String $tokenId;
    public int label;
    public final /* synthetic */ CollectibleViewModel this$0;

    /* compiled from: CollectibleViewModel.kt */
    @a(c = "net.safemoon.androidwallet.viewmodels.CollectibleViewModel$estimateGas$1$1", f = "CollectibleViewModel.kt", l = {244, 250}, m = "invokeSuspend")
    /* renamed from: net.safemoon.androidwallet.viewmodels.CollectibleViewModel$estimateGas$1$1  reason: invalid class name */
    /* loaded from: classes2.dex */
    public static final class AnonymousClass1 extends SuspendLambda implements hd1<c90, q70<? super te4>, Object> {
        public final /* synthetic */ im1 $iNFTWeb;
        public final /* synthetic */ String $toAddress;
        public final /* synthetic */ String $tokenId;
        public Object L$0;
        public Object L$1;
        public Object L$2;
        public int label;
        public final /* synthetic */ CollectibleViewModel this$0;

        /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
        public AnonymousClass1(im1 im1Var, String str, String str2, CollectibleViewModel collectibleViewModel, q70<? super AnonymousClass1> q70Var) {
            super(2, q70Var);
            this.$iNFTWeb = im1Var;
            this.$toAddress = str;
            this.$tokenId = str2;
            this.this$0 = collectibleViewModel;
        }

        @Override // kotlin.coroutines.jvm.internal.BaseContinuationImpl
        public final q70<te4> create(Object obj, q70<?> q70Var) {
            return new AnonymousClass1(this.$iNFTWeb, this.$toAddress, this.$tokenId, this.this$0, q70Var);
        }

        @Override // defpackage.hd1
        public final Object invoke(c90 c90Var, q70<? super te4> q70Var) {
            return ((AnonymousClass1) create(c90Var, q70Var)).invokeSuspend(te4.a);
        }

        /* JADX WARN: Removed duplicated region for block: B:29:0x0099  */
        /* JADX WARN: Removed duplicated region for block: B:30:0x009b A[Catch: Exception -> 0x015f, TryCatch #0 {Exception -> 0x015f, blocks: (B:7:0x001a, B:27:0x0095, B:30:0x009b, B:32:0x00f1, B:33:0x00f7, B:12:0x002b, B:23:0x006b, B:15:0x0032, B:19:0x0042, B:18:0x003a), top: B:37:0x0008 }] */
        @Override // kotlin.coroutines.jvm.internal.BaseContinuationImpl
        /*
            Code decompiled incorrectly, please refer to instructions dump.
            To view partially-correct code enable 'Show inconsistent code' option in preferences
        */
        public final java.lang.Object invokeSuspend(java.lang.Object r15) {
            /*
                Method dump skipped, instructions count: 354
                To view this dump change 'Code comments level' option to 'DEBUG'
            */
            throw new UnsupportedOperationException("Method not decompiled: net.safemoon.androidwallet.viewmodels.CollectibleViewModel$estimateGas$1.AnonymousClass1.invokeSuspend(java.lang.Object):java.lang.Object");
        }
    }

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public CollectibleViewModel$estimateGas$1(im1 im1Var, String str, String str2, CollectibleViewModel collectibleViewModel, q70<? super CollectibleViewModel$estimateGas$1> q70Var) {
        super(2, q70Var);
        this.$iNFTWeb = im1Var;
        this.$toAddress = str;
        this.$tokenId = str2;
        this.this$0 = collectibleViewModel;
    }

    @Override // kotlin.coroutines.jvm.internal.BaseContinuationImpl
    public final q70<te4> create(Object obj, q70<?> q70Var) {
        return new CollectibleViewModel$estimateGas$1(this.$iNFTWeb, this.$toAddress, this.$tokenId, this.this$0, q70Var);
    }

    @Override // defpackage.hd1
    public final Object invoke(c90 c90Var, q70<? super te4> q70Var) {
        return ((CollectibleViewModel$estimateGas$1) create(c90Var, q70Var)).invokeSuspend(te4.a);
    }

    @Override // kotlin.coroutines.jvm.internal.BaseContinuationImpl
    public final Object invokeSuspend(Object obj) {
        Object d = gs1.d();
        int i = this.label;
        if (i == 0) {
            o83.b(obj);
            CoroutineDispatcher b = tp0.b();
            AnonymousClass1 anonymousClass1 = new AnonymousClass1(this.$iNFTWeb, this.$toAddress, this.$tokenId, this.this$0, null);
            this.label = 1;
            if (kotlinx.coroutines.a.e(b, anonymousClass1, this) == d) {
                return d;
            }
        } else if (i != 1) {
            throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        } else {
            o83.b(obj);
        }
        return te4.a;
    }
}
