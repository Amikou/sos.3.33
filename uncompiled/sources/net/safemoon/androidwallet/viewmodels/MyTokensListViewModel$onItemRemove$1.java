package net.safemoon.androidwallet.viewmodels;

import kotlin.coroutines.jvm.internal.SuspendLambda;
import kotlin.coroutines.jvm.internal.a;
import net.safemoon.androidwallet.model.token.abstraction.IToken;

/* compiled from: MyTokensListViewModel.kt */
@a(c = "net.safemoon.androidwallet.viewmodels.MyTokensListViewModel$onItemRemove$1", f = "MyTokensListViewModel.kt", l = {}, m = "invokeSuspend")
/* loaded from: classes2.dex */
public final class MyTokensListViewModel$onItemRemove$1 extends SuspendLambda implements hd1<c90, q70<? super te4>, Object> {
    public final /* synthetic */ IToken $token;
    public int label;
    public final /* synthetic */ MyTokensListViewModel this$0;

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public MyTokensListViewModel$onItemRemove$1(MyTokensListViewModel myTokensListViewModel, IToken iToken, q70<? super MyTokensListViewModel$onItemRemove$1> q70Var) {
        super(2, q70Var);
        this.this$0 = myTokensListViewModel;
        this.$token = iToken;
    }

    @Override // kotlin.coroutines.jvm.internal.BaseContinuationImpl
    public final q70<te4> create(Object obj, q70<?> q70Var) {
        return new MyTokensListViewModel$onItemRemove$1(this.this$0, this.$token, q70Var);
    }

    @Override // defpackage.hd1
    public final Object invoke(c90 c90Var, q70<? super te4> q70Var) {
        return ((MyTokensListViewModel$onItemRemove$1) create(c90Var, q70Var)).invokeSuspend(te4.a);
    }

    @Override // kotlin.coroutines.jvm.internal.BaseContinuationImpl
    public final Object invokeSuspend(Object obj) {
        bn1 bn1Var;
        gs1.d();
        if (this.label == 0) {
            o83.b(obj);
            bn1Var = this.this$0.c;
            bn1Var.h(this.$token);
            return te4.a;
        }
        throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
    }
}
