package net.safemoon.androidwallet.viewmodels;

import android.os.Bundle;
import java.math.BigDecimal;
import java.math.BigInteger;
import kotlin.coroutines.jvm.internal.SuspendLambda;
import kotlin.coroutines.jvm.internal.a;
import kotlinx.coroutines.CoroutineDispatcher;
import net.safemoon.androidwallet.ERC20;
import net.safemoon.androidwallet.model.token.abstraction.IToken;
import org.web3j.protocol.core.DefaultBlockParameterName;

/* compiled from: MyTokensListViewModel.kt */
@a(c = "net.safemoon.androidwallet.viewmodels.MyTokensListViewModel$updateNativeBalances$1$1", f = "MyTokensListViewModel.kt", l = {497}, m = "invokeSuspend")
/* loaded from: classes2.dex */
public final class MyTokensListViewModel$updateNativeBalances$1$1 extends SuspendLambda implements hd1<c90, q70<? super te4>, Object> {
    public final /* synthetic */ IToken $it;
    public int label;
    public final /* synthetic */ MyTokensListViewModel this$0;

    /* compiled from: MyTokensListViewModel.kt */
    @a(c = "net.safemoon.androidwallet.viewmodels.MyTokensListViewModel$updateNativeBalances$1$1$1", f = "MyTokensListViewModel.kt", l = {}, m = "invokeSuspend")
    /* renamed from: net.safemoon.androidwallet.viewmodels.MyTokensListViewModel$updateNativeBalances$1$1$1  reason: invalid class name */
    /* loaded from: classes2.dex */
    public static final class AnonymousClass1 extends SuspendLambda implements hd1<c90, q70<? super te4>, Object> {
        public final /* synthetic */ IToken $it;
        public int label;
        public final /* synthetic */ MyTokensListViewModel this$0;

        /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
        public AnonymousClass1(IToken iToken, MyTokensListViewModel myTokensListViewModel, q70<? super AnonymousClass1> q70Var) {
            super(2, q70Var);
            this.$it = iToken;
            this.this$0 = myTokensListViewModel;
        }

        @Override // kotlin.coroutines.jvm.internal.BaseContinuationImpl
        public final q70<te4> create(Object obj, q70<?> q70Var) {
            return new AnonymousClass1(this.$it, this.this$0, q70Var);
        }

        @Override // defpackage.hd1
        public final Object invoke(c90 c90Var, q70<? super te4> q70Var) {
            return ((AnonymousClass1) create(c90Var, q70Var)).invokeSuspend(te4.a);
        }

        @Override // kotlin.coroutines.jvm.internal.BaseContinuationImpl
        public final Object invokeSuspend(Object obj) {
            ko4 a;
            String q;
            BigInteger balance;
            BigDecimal bigDecimal;
            String bigInteger;
            bn1 bn1Var;
            ma0 t;
            String q2;
            gs1.d();
            if (this.label == 0) {
                o83.b(obj);
                try {
                    a = jo4.a(new ml1(e30.z(this.$it.getChainId())));
                } catch (Exception e) {
                    Bundle bundle = new Bundle();
                    bundle.putString("updateNativeBalances", "UpdateNativeBalanceError");
                    bundle.putSerializable("Key", e);
                    this.this$0.s().a("select_item", bundle);
                    String localizedMessage = e.getLocalizedMessage();
                    if (localizedMessage == null) {
                        localizedMessage = e.getMessage();
                    }
                    if (localizedMessage != null) {
                        e30.c0(localizedMessage, "ErrorFetch");
                    }
                }
                if (this.$it.getContractAddress() != null) {
                    if (!(this.$it.getContractAddress().length() == 0)) {
                        String contractAddress = this.$it.getContractAddress();
                        t = this.this$0.t();
                        ERC20 s = ERC20.s(contractAddress, a, t, new oj0());
                        q2 = this.this$0.q();
                        balance = s.q(q2).send();
                        bigDecimal = null;
                        if (balance != null && (bigInteger = balance.toString()) != null) {
                            bigDecimal = e30.q(bigInteger, this.$it.getDecimals());
                        }
                        fs1.d(bigDecimal);
                        double doubleValue = bigDecimal.doubleValue();
                        bn1Var = this.this$0.c;
                        bn1Var.c(this.$it.getSymbolWithType(), doubleValue);
                        return te4.a;
                    }
                }
                q = this.this$0.q();
                balance = a.ethGetBalance(q, DefaultBlockParameterName.LATEST).send().getBalance();
                bigDecimal = null;
                if (balance != null) {
                    bigDecimal = e30.q(bigInteger, this.$it.getDecimals());
                }
                fs1.d(bigDecimal);
                double doubleValue2 = bigDecimal.doubleValue();
                bn1Var = this.this$0.c;
                bn1Var.c(this.$it.getSymbolWithType(), doubleValue2);
                return te4.a;
            }
            throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        }
    }

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public MyTokensListViewModel$updateNativeBalances$1$1(IToken iToken, MyTokensListViewModel myTokensListViewModel, q70<? super MyTokensListViewModel$updateNativeBalances$1$1> q70Var) {
        super(2, q70Var);
        this.$it = iToken;
        this.this$0 = myTokensListViewModel;
    }

    @Override // kotlin.coroutines.jvm.internal.BaseContinuationImpl
    public final q70<te4> create(Object obj, q70<?> q70Var) {
        return new MyTokensListViewModel$updateNativeBalances$1$1(this.$it, this.this$0, q70Var);
    }

    @Override // defpackage.hd1
    public final Object invoke(c90 c90Var, q70<? super te4> q70Var) {
        return ((MyTokensListViewModel$updateNativeBalances$1$1) create(c90Var, q70Var)).invokeSuspend(te4.a);
    }

    @Override // kotlin.coroutines.jvm.internal.BaseContinuationImpl
    public final Object invokeSuspend(Object obj) {
        Object d = gs1.d();
        int i = this.label;
        if (i == 0) {
            o83.b(obj);
            CoroutineDispatcher b = tp0.b();
            AnonymousClass1 anonymousClass1 = new AnonymousClass1(this.$it, this.this$0, null);
            this.label = 1;
            if (kotlinx.coroutines.a.e(b, anonymousClass1, this) == d) {
                return d;
            }
        } else if (i != 1) {
            throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        } else {
            o83.b(obj);
        }
        return te4.a;
    }
}
