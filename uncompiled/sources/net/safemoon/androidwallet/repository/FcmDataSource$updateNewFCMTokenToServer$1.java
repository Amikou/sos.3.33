package net.safemoon.androidwallet.repository;

import kotlin.coroutines.jvm.internal.ContinuationImpl;
import kotlin.coroutines.jvm.internal.a;

/* compiled from: FcmDataSource.kt */
@a(c = "net.safemoon.androidwallet.repository.FcmDataSource", f = "FcmDataSource.kt", l = {88, 93}, m = "updateNewFCMTokenToServer")
/* loaded from: classes2.dex */
public final class FcmDataSource$updateNewFCMTokenToServer$1 extends ContinuationImpl {
    public Object L$0;
    public Object L$1;
    public Object L$2;
    public int label;
    public /* synthetic */ Object result;
    public final /* synthetic */ FcmDataSource this$0;

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public FcmDataSource$updateNewFCMTokenToServer$1(FcmDataSource fcmDataSource, q70<? super FcmDataSource$updateNewFCMTokenToServer$1> q70Var) {
        super(q70Var);
        this.this$0 = fcmDataSource;
    }

    @Override // kotlin.coroutines.jvm.internal.BaseContinuationImpl
    public final Object invokeSuspend(Object obj) {
        Object j;
        this.result = obj;
        this.label |= Integer.MIN_VALUE;
        j = this.this$0.j(null, this);
        return j;
    }
}
