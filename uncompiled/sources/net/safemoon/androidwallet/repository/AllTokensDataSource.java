package net.safemoon.androidwallet.repository;

import java.util.ArrayList;
import java.util.List;
import kotlin.text.StringsKt__StringsKt;
import net.safemoon.androidwallet.model.AllCryptoList;
import net.safemoon.androidwallet.model.Coin;
import retrofit2.b;

/* compiled from: AllTokensDataSource.kt */
/* loaded from: classes2.dex */
public final class AllTokensDataSource extends gp2<Integer, Coin> {
    public final e42 c;
    public final cm1 d;
    public final cm1 e;
    public final cm1 f;
    public final cm1 g;
    public String h;
    public b<AllCryptoList> i;

    public AllTokensDataSource(e42 e42Var, cm1 cm1Var, cm1 cm1Var2, cm1 cm1Var3, cm1 cm1Var4) {
        fs1.f(e42Var, "apiService");
        fs1.f(cm1Var, "getValueSortParamUseCase");
        fs1.f(cm1Var2, "getValueSortDirParamUseCase");
        fs1.f(cm1Var3, "getActiveTabParamUseCase");
        fs1.f(cm1Var4, "getActiveTabSearchParamUseCase");
        this.c = e42Var;
        this.d = cm1Var;
        this.e = cm1Var2;
        this.f = cm1Var3;
        this.g = cm1Var4;
        this.h = cm1Var3.get();
    }

    /* JADX WARN: Removed duplicated region for block: B:10:0x0024  */
    /* JADX WARN: Removed duplicated region for block: B:20:0x0043  */
    /* JADX WARN: Removed duplicated region for block: B:47:0x012c A[Catch: HttpException -> 0x0035, IOException -> 0x0038, TryCatch #2 {IOException -> 0x0038, HttpException -> 0x0035, blocks: (B:12:0x0030, B:45:0x0122, B:47:0x012c, B:48:0x0131, B:52:0x0140, B:53:0x0144, B:55:0x014a, B:56:0x0166, B:60:0x0177, B:59:0x0172, B:51:0x013c, B:31:0x006d, B:33:0x007f, B:41:0x0107, B:34:0x00a4, B:36:0x00b0, B:37:0x00d5, B:39:0x00e1), top: B:65:0x0022 }] */
    /* JADX WARN: Removed duplicated region for block: B:50:0x013b  */
    /* JADX WARN: Removed duplicated region for block: B:51:0x013c A[Catch: HttpException -> 0x0035, IOException -> 0x0038, TryCatch #2 {IOException -> 0x0038, HttpException -> 0x0035, blocks: (B:12:0x0030, B:45:0x0122, B:47:0x012c, B:48:0x0131, B:52:0x0140, B:53:0x0144, B:55:0x014a, B:56:0x0166, B:60:0x0177, B:59:0x0172, B:51:0x013c, B:31:0x006d, B:33:0x007f, B:41:0x0107, B:34:0x00a4, B:36:0x00b0, B:37:0x00d5, B:39:0x00e1), top: B:65:0x0022 }] */
    /* JADX WARN: Removed duplicated region for block: B:55:0x014a A[Catch: HttpException -> 0x0035, IOException -> 0x0038, LOOP:0: B:53:0x0144->B:55:0x014a, LOOP_END, TryCatch #2 {IOException -> 0x0038, HttpException -> 0x0035, blocks: (B:12:0x0030, B:45:0x0122, B:47:0x012c, B:48:0x0131, B:52:0x0140, B:53:0x0144, B:55:0x014a, B:56:0x0166, B:60:0x0177, B:59:0x0172, B:51:0x013c, B:31:0x006d, B:33:0x007f, B:41:0x0107, B:34:0x00a4, B:36:0x00b0, B:37:0x00d5, B:39:0x00e1), top: B:65:0x0022 }] */
    /* JADX WARN: Removed duplicated region for block: B:58:0x0170  */
    /* JADX WARN: Removed duplicated region for block: B:59:0x0172 A[Catch: HttpException -> 0x0035, IOException -> 0x0038, TryCatch #2 {IOException -> 0x0038, HttpException -> 0x0035, blocks: (B:12:0x0030, B:45:0x0122, B:47:0x012c, B:48:0x0131, B:52:0x0140, B:53:0x0144, B:55:0x014a, B:56:0x0166, B:60:0x0177, B:59:0x0172, B:51:0x013c, B:31:0x006d, B:33:0x007f, B:41:0x0107, B:34:0x00a4, B:36:0x00b0, B:37:0x00d5, B:39:0x00e1), top: B:65:0x0022 }] */
    @Override // defpackage.gp2
    /*
        Code decompiled incorrectly, please refer to instructions dump.
        To view partially-correct code enable 'Show inconsistent code' option in preferences
    */
    public java.lang.Object f(defpackage.gp2.a<java.lang.Integer> r12, defpackage.q70<? super defpackage.gp2.b<java.lang.Integer, net.safemoon.androidwallet.model.Coin>> r13) {
        /*
            Method dump skipped, instructions count: 399
            To view this dump change 'Code comments level' option to 'DEBUG'
        */
        throw new UnsupportedOperationException("Method not decompiled: net.safemoon.androidwallet.repository.AllTokensDataSource.f(gp2$a, q70):java.lang.Object");
    }

    public final AllCryptoList i(AllCryptoList allCryptoList) {
        String name;
        String str = this.g.get();
        boolean w = dv3.w(str);
        List<Coin> data = allCryptoList.getData();
        if (w || (data == null || data.isEmpty())) {
            return allCryptoList;
        }
        ArrayList arrayList = new ArrayList();
        for (Coin coin : allCryptoList.getData()) {
            String str2 = "";
            if (coin != null && (name = coin.getName()) != null) {
                str2 = name;
            }
            if (StringsKt__StringsKt.K(str2, str, true)) {
                fs1.e(coin, "coin");
                arrayList.add(coin);
            }
        }
        return new AllCryptoList(arrayList);
    }

    public final b<AllCryptoList> j() {
        return this.i;
    }

    public final int k() {
        return dv3.w(this.g.get()) ? 100 : 1000;
    }

    @Override // defpackage.gp2
    /* renamed from: l */
    public Integer d(ip2<Integer, Coin> ip2Var) {
        fs1.f(ip2Var, "state");
        return 1;
    }

    public final void m(b<AllCryptoList> bVar) {
        this.i = bVar;
    }
}
