package net.safemoon.androidwallet.repository;

import kotlin.coroutines.jvm.internal.ContinuationImpl;
import kotlin.coroutines.jvm.internal.a;

/* compiled from: TransactionDataSource.kt */
@a(c = "net.safemoon.androidwallet.repository.TransactionDataSource", f = "TransactionDataSource.kt", l = {61}, m = "load")
/* loaded from: classes2.dex */
public final class TransactionDataSource$load$1 extends ContinuationImpl {
    public int I$0;
    public Object L$0;
    public int label;
    public /* synthetic */ Object result;
    public final /* synthetic */ TransactionDataSource this$0;

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public TransactionDataSource$load$1(TransactionDataSource transactionDataSource, q70<? super TransactionDataSource$load$1> q70Var) {
        super(q70Var);
        this.this$0 = transactionDataSource;
    }

    @Override // kotlin.coroutines.jvm.internal.BaseContinuationImpl
    public final Object invokeSuspend(Object obj) {
        this.result = obj;
        this.label |= Integer.MIN_VALUE;
        return this.this$0.f(null, this);
    }
}
