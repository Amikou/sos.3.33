package net.safemoon.androidwallet.repository;

import kotlin.coroutines.jvm.internal.ContinuationImpl;
import kotlin.coroutines.jvm.internal.a;

/* compiled from: TokenListDataSource.kt */
@a(c = "net.safemoon.androidwallet.repository.TokenListDataSource", f = "TokenListDataSource.kt", l = {129}, m = "getSwapTokens")
/* loaded from: classes2.dex */
public final class TokenListDataSource$getSwapTokens$1 extends ContinuationImpl {
    public Object L$0;
    public Object L$1;
    public int label;
    public /* synthetic */ Object result;
    public final /* synthetic */ TokenListDataSource this$0;

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public TokenListDataSource$getSwapTokens$1(TokenListDataSource tokenListDataSource, q70<? super TokenListDataSource$getSwapTokens$1> q70Var) {
        super(q70Var);
        this.this$0 = tokenListDataSource;
    }

    @Override // kotlin.coroutines.jvm.internal.BaseContinuationImpl
    public final Object invokeSuspend(Object obj) {
        this.result = obj;
        this.label |= Integer.MIN_VALUE;
        return this.this$0.f(this);
    }
}
