package net.safemoon.androidwallet.repository;

import net.safemoon.androidwallet.model.AllCryptoList;
import net.safemoon.androidwallet.model.Coin;
import retrofit2.b;

/* compiled from: LatestCoinDataSource.kt */
/* loaded from: classes2.dex */
public final class LatestCoinDataSource extends gp2<Integer, Coin> {
    public final e42 c;
    public final a d;
    public final int e;
    public final int f;
    public final int g;
    public b<AllCryptoList> h;

    /* compiled from: LatestCoinDataSource.kt */
    /* loaded from: classes2.dex */
    public interface a {
        void a(AllCryptoList allCryptoList);
    }

    public LatestCoinDataSource(e42 e42Var, a aVar) {
        fs1.f(e42Var, "apiService");
        fs1.f(aVar, "onCoinDataLoadedListener");
        this.c = e42Var;
        this.d = aVar;
        this.e = 1;
        this.f = 1;
        this.g = 10;
    }

    /* JADX WARN: Removed duplicated region for block: B:10:0x0024  */
    /* JADX WARN: Removed duplicated region for block: B:16:0x0038  */
    /* JADX WARN: Removed duplicated region for block: B:36:0x0084 A[Catch: HttpException -> 0x00d7, IOException -> 0x00e1, TryCatch #2 {IOException -> 0x00e1, HttpException -> 0x00d7, blocks: (B:12:0x002c, B:33:0x007e, B:38:0x008c, B:39:0x0091, B:43:0x00a1, B:44:0x00a5, B:46:0x00ab, B:47:0x00be, B:51:0x00d1, B:50:0x00ca, B:42:0x009d, B:36:0x0084, B:17:0x003b, B:19:0x0043, B:21:0x004a, B:25:0x0051, B:26:0x0054, B:29:0x0070, B:20:0x0046), top: B:58:0x0022 }] */
    /* JADX WARN: Removed duplicated region for block: B:38:0x008c A[Catch: HttpException -> 0x00d7, IOException -> 0x00e1, TryCatch #2 {IOException -> 0x00e1, HttpException -> 0x00d7, blocks: (B:12:0x002c, B:33:0x007e, B:38:0x008c, B:39:0x0091, B:43:0x00a1, B:44:0x00a5, B:46:0x00ab, B:47:0x00be, B:51:0x00d1, B:50:0x00ca, B:42:0x009d, B:36:0x0084, B:17:0x003b, B:19:0x0043, B:21:0x004a, B:25:0x0051, B:26:0x0054, B:29:0x0070, B:20:0x0046), top: B:58:0x0022 }] */
    /* JADX WARN: Removed duplicated region for block: B:41:0x009c  */
    /* JADX WARN: Removed duplicated region for block: B:42:0x009d A[Catch: HttpException -> 0x00d7, IOException -> 0x00e1, TryCatch #2 {IOException -> 0x00e1, HttpException -> 0x00d7, blocks: (B:12:0x002c, B:33:0x007e, B:38:0x008c, B:39:0x0091, B:43:0x00a1, B:44:0x00a5, B:46:0x00ab, B:47:0x00be, B:51:0x00d1, B:50:0x00ca, B:42:0x009d, B:36:0x0084, B:17:0x003b, B:19:0x0043, B:21:0x004a, B:25:0x0051, B:26:0x0054, B:29:0x0070, B:20:0x0046), top: B:58:0x0022 }] */
    /* JADX WARN: Removed duplicated region for block: B:46:0x00ab A[Catch: HttpException -> 0x00d7, IOException -> 0x00e1, LOOP:0: B:44:0x00a5->B:46:0x00ab, LOOP_END, TryCatch #2 {IOException -> 0x00e1, HttpException -> 0x00d7, blocks: (B:12:0x002c, B:33:0x007e, B:38:0x008c, B:39:0x0091, B:43:0x00a1, B:44:0x00a5, B:46:0x00ab, B:47:0x00be, B:51:0x00d1, B:50:0x00ca, B:42:0x009d, B:36:0x0084, B:17:0x003b, B:19:0x0043, B:21:0x004a, B:25:0x0051, B:26:0x0054, B:29:0x0070, B:20:0x0046), top: B:58:0x0022 }] */
    /* JADX WARN: Removed duplicated region for block: B:49:0x00c8  */
    /* JADX WARN: Removed duplicated region for block: B:50:0x00ca A[Catch: HttpException -> 0x00d7, IOException -> 0x00e1, TryCatch #2 {IOException -> 0x00e1, HttpException -> 0x00d7, blocks: (B:12:0x002c, B:33:0x007e, B:38:0x008c, B:39:0x0091, B:43:0x00a1, B:44:0x00a5, B:46:0x00ab, B:47:0x00be, B:51:0x00d1, B:50:0x00ca, B:42:0x009d, B:36:0x0084, B:17:0x003b, B:19:0x0043, B:21:0x004a, B:25:0x0051, B:26:0x0054, B:29:0x0070, B:20:0x0046), top: B:58:0x0022 }] */
    @Override // defpackage.gp2
    /*
        Code decompiled incorrectly, please refer to instructions dump.
        To view partially-correct code enable 'Show inconsistent code' option in preferences
    */
    public java.lang.Object f(defpackage.gp2.a<java.lang.Integer> r8, defpackage.q70<? super defpackage.gp2.b<java.lang.Integer, net.safemoon.androidwallet.model.Coin>> r9) {
        /*
            Method dump skipped, instructions count: 235
            To view this dump change 'Code comments level' option to 'DEBUG'
        */
        throw new UnsupportedOperationException("Method not decompiled: net.safemoon.androidwallet.repository.LatestCoinDataSource.f(gp2$a, q70):java.lang.Object");
    }

    @Override // defpackage.gp2
    /* renamed from: i */
    public Integer d(ip2<Integer, Coin> ip2Var) {
        fs1.f(ip2Var, "state");
        return 1;
    }
}
