package net.safemoon.androidwallet.repository;

import java.util.concurrent.TimeUnit;
import kotlin.coroutines.jvm.internal.SuspendLambda;
import kotlin.coroutines.jvm.internal.a;

/* compiled from: FcmDataSource.kt */
@a(c = "net.safemoon.androidwallet.repository.FcmDataSource$updateFCMTokenConfiguration$1", f = "FcmDataSource.kt", l = {51, 52}, m = "invokeSuspend")
/* loaded from: classes2.dex */
public final class FcmDataSource$updateFCMTokenConfiguration$1 extends SuspendLambda implements hd1<c90, q70<? super te4>, Object> {
    public final /* synthetic */ tc1<Boolean, te4> $callBack;
    public final /* synthetic */ String $currency;
    public final /* synthetic */ boolean $isSendPushPriceAlert;
    public final /* synthetic */ String $lang;
    public int label;
    public final /* synthetic */ FcmDataSource this$0;

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    /* JADX WARN: Multi-variable type inference failed */
    public FcmDataSource$updateFCMTokenConfiguration$1(FcmDataSource fcmDataSource, String str, String str2, boolean z, tc1<? super Boolean, te4> tc1Var, q70<? super FcmDataSource$updateFCMTokenConfiguration$1> q70Var) {
        super(2, q70Var);
        this.this$0 = fcmDataSource;
        this.$lang = str;
        this.$currency = str2;
        this.$isSendPushPriceAlert = z;
        this.$callBack = tc1Var;
    }

    @Override // kotlin.coroutines.jvm.internal.BaseContinuationImpl
    public final q70<te4> create(Object obj, q70<?> q70Var) {
        return new FcmDataSource$updateFCMTokenConfiguration$1(this.this$0, this.$lang, this.$currency, this.$isSendPushPriceAlert, this.$callBack, q70Var);
    }

    @Override // defpackage.hd1
    public final Object invoke(c90 c90Var, q70<? super te4> q70Var) {
        return ((FcmDataSource$updateFCMTokenConfiguration$1) create(c90Var, q70Var)).invokeSuspend(te4.a);
    }

    @Override // kotlin.coroutines.jvm.internal.BaseContinuationImpl
    public final Object invokeSuspend(Object obj) {
        Object f;
        Object d = gs1.d();
        int i = this.label;
        if (i == 0) {
            o83.b(obj);
            long millis = TimeUnit.SECONDS.toMillis(1L);
            this.label = 1;
            if (xl0.a(millis, this) == d) {
                return d;
            }
        } else if (i != 1) {
            if (i != 2) {
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
            o83.b(obj);
            return te4.a;
        } else {
            o83.b(obj);
        }
        FcmDataSource fcmDataSource = this.this$0;
        String str = this.$lang;
        String str2 = this.$currency;
        boolean z = this.$isSendPushPriceAlert;
        tc1<Boolean, te4> tc1Var = this.$callBack;
        this.label = 2;
        f = fcmDataSource.f(str, str2, z, tc1Var, this);
        if (f == d) {
            return d;
        }
        return te4.a;
    }
}
