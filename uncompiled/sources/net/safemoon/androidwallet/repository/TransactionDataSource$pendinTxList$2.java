package net.safemoon.androidwallet.repository;

import android.app.Application;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import kotlin.jvm.internal.Lambda;
import net.safemoon.androidwallet.model.transaction.history.Result;
import net.safemoon.androidwallet.service.TxnListService;
import net.safemoon.androidwallet.ui.displayModel.UserTokenItemDisplayModel;

/* compiled from: TransactionDataSource.kt */
/* loaded from: classes2.dex */
public final class TransactionDataSource$pendinTxList$2 extends Lambda implements rc1<List<? extends Result>> {
    public final /* synthetic */ TransactionDataSource this$0;

    /* compiled from: Comparisons.kt */
    /* loaded from: classes2.dex */
    public static final class a<T> implements Comparator {
        @Override // java.util.Comparator
        public final int compare(T t, T t2) {
            String str = ((Result) t2).timeStamp;
            fs1.e(str, "it.timeStamp");
            Long valueOf = Long.valueOf(Long.parseLong(str));
            String str2 = ((Result) t).timeStamp;
            fs1.e(str2, "it.timeStamp");
            return l30.a(valueOf, Long.valueOf(Long.parseLong(str2)));
        }
    }

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public TransactionDataSource$pendinTxList$2(TransactionDataSource transactionDataSource) {
        super(0);
        this.this$0 = transactionDataSource;
    }

    @Override // defpackage.rc1
    public final List<? extends Result> invoke() {
        Application application;
        UserTokenItemDisplayModel userTokenItemDisplayModel;
        TxnListService.a aVar = TxnListService.o0;
        application = this.this$0.c;
        List<Result> a2 = aVar.a(application);
        TransactionDataSource transactionDataSource = this.this$0;
        ArrayList arrayList = new ArrayList();
        for (Object obj : a2) {
            String str = ((Result) obj).tokenSymbol;
            userTokenItemDisplayModel = transactionDataSource.e;
            if (fs1.b(str, userTokenItemDisplayModel.getSymbol())) {
                arrayList.add(obj);
            }
        }
        return j20.e0(arrayList, new a());
    }
}
