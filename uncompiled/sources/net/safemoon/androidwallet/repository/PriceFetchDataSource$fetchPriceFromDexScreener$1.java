package net.safemoon.androidwallet.repository;

import kotlin.coroutines.jvm.internal.ContinuationImpl;
import kotlin.coroutines.jvm.internal.a;

/* compiled from: PriceFetchDataSource.kt */
@a(c = "net.safemoon.androidwallet.repository.PriceFetchDataSource", f = "PriceFetchDataSource.kt", l = {112}, m = "fetchPriceFromDexScreener")
/* loaded from: classes2.dex */
public final class PriceFetchDataSource$fetchPriceFromDexScreener$1 extends ContinuationImpl {
    public int label;
    public /* synthetic */ Object result;
    public final /* synthetic */ PriceFetchDataSource this$0;

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public PriceFetchDataSource$fetchPriceFromDexScreener$1(PriceFetchDataSource priceFetchDataSource, q70<? super PriceFetchDataSource$fetchPriceFromDexScreener$1> q70Var) {
        super(q70Var);
        this.this$0 = priceFetchDataSource;
    }

    @Override // kotlin.coroutines.jvm.internal.BaseContinuationImpl
    public final Object invokeSuspend(Object obj) {
        this.result = obj;
        this.label |= Integer.MIN_VALUE;
        return this.this$0.b(null, this);
    }
}
