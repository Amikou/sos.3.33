package net.safemoon.androidwallet.repository;

import kotlin.coroutines.jvm.internal.ContinuationImpl;
import kotlin.coroutines.jvm.internal.a;

/* compiled from: AllTokensDataSource.kt */
@a(c = "net.safemoon.androidwallet.repository.AllTokensDataSource", f = "AllTokensDataSource.kt", l = {72}, m = "load")
/* loaded from: classes2.dex */
public final class AllTokensDataSource$load$1 extends ContinuationImpl {
    public int I$0;
    public Object L$0;
    public Object L$1;
    public int label;
    public /* synthetic */ Object result;
    public final /* synthetic */ AllTokensDataSource this$0;

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public AllTokensDataSource$load$1(AllTokensDataSource allTokensDataSource, q70<? super AllTokensDataSource$load$1> q70Var) {
        super(q70Var);
        this.this$0 = allTokensDataSource;
    }

    @Override // kotlin.coroutines.jvm.internal.BaseContinuationImpl
    public final Object invokeSuspend(Object obj) {
        this.result = obj;
        this.label |= Integer.MIN_VALUE;
        return this.this$0.f(null, this);
    }
}
