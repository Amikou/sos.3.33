package net.safemoon.androidwallet.repository;

import androidx.lifecycle.LiveData;
import java.util.List;
import kotlin.Pair;
import net.safemoon.androidwallet.model.wallets.Wallet;

/* compiled from: WalletDataSource.kt */
/* loaded from: classes2.dex */
public final class WalletDataSource {
    public final mm4 a;

    public WalletDataSource(mm4 mm4Var) {
        fs1.f(mm4Var, "walletDao");
        this.a = mm4Var;
    }

    public final Object b(String str, String str2, String str3, String str4, String str5, String str6, String str7, int i, boolean z, q70<? super Long> q70Var) {
        return this.a.i(new Wallet(null, str, str5, str2, str3, str4, str6, str7, z ? 1 : 0, i, z, 1, null), q70Var);
    }

    public final Object c(q70<? super List<Wallet>> q70Var) {
        return this.a.f(q70Var);
    }

    public final Object d(Wallet wallet2, q70<? super te4> q70Var) {
        Object c = this.a.c(wallet2, q70Var);
        return c == gs1.d() ? c : te4.a;
    }

    public final LiveData<List<Wallet>> e() {
        return this.a.b();
    }

    public final Object f(String str, q70<? super Boolean> q70Var) {
        return this.a.j(str, q70Var);
    }

    public final Object g(long j, q70<? super Wallet> q70Var) {
        return this.a.h(j, q70Var);
    }

    public final LiveData<Integer> h() {
        return this.a.l();
    }

    public final Object i(long j, String str, int i, q70<? super te4> q70Var) {
        Object g = this.a.g(j, str, i, q70Var);
        return g == gs1.d() ? g : te4.a;
    }

    public final Object j(long j, int i, q70<? super te4> q70Var) {
        Object e = this.a.e(j, i, q70Var);
        return e == gs1.d() ? e : te4.a;
    }

    public final Object k(long j, String str, q70<? super te4> q70Var) {
        Object k = this.a.k(j, str, q70Var);
        return k == gs1.d() ? k : te4.a;
    }

    public final Object l(Pair<Long, Integer>[] pairArr, q70<? super te4> q70Var) {
        st1 b;
        b = as.b(qg1.a, null, null, new WalletDataSource$updateWalletOrders$2(pairArr, this, null), 3, null);
        return b == gs1.d() ? b : te4.a;
    }

    public final Object m(long j, String str, String str2, String str3, String str4, int i, String str5, q70<? super te4> q70Var) {
        Object d = this.a.d(j, str, str2, str3, str4, i, str5, q70Var);
        return d == gs1.d() ? d : te4.a;
    }
}
