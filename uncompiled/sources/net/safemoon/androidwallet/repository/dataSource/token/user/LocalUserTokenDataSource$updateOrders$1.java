package net.safemoon.androidwallet.repository.dataSource.token.user;

import kotlin.Pair;
import kotlin.coroutines.jvm.internal.SuspendLambda;
import kotlin.coroutines.jvm.internal.a;
import kotlinx.coroutines.CoroutineDispatcher;

/* compiled from: LocalUserTokenDataSource.kt */
@a(c = "net.safemoon.androidwallet.repository.dataSource.token.user.LocalUserTokenDataSource$updateOrders$1", f = "LocalUserTokenDataSource.kt", l = {97}, m = "invokeSuspend")
/* loaded from: classes2.dex */
public final class LocalUserTokenDataSource$updateOrders$1 extends SuspendLambda implements hd1<c90, q70<? super te4>, Object> {
    public final /* synthetic */ Pair<String, Integer>[] $orders;
    public int label;
    public final /* synthetic */ LocalUserTokenDataSource this$0;

    /* compiled from: LocalUserTokenDataSource.kt */
    @a(c = "net.safemoon.androidwallet.repository.dataSource.token.user.LocalUserTokenDataSource$updateOrders$1$1", f = "LocalUserTokenDataSource.kt", l = {99}, m = "invokeSuspend")
    /* renamed from: net.safemoon.androidwallet.repository.dataSource.token.user.LocalUserTokenDataSource$updateOrders$1$1  reason: invalid class name */
    /* loaded from: classes2.dex */
    public static final class AnonymousClass1 extends SuspendLambda implements hd1<c90, q70<? super te4>, Object> {
        public final /* synthetic */ Pair<String, Integer>[] $orders;
        public int I$0;
        public int I$1;
        public Object L$0;
        public Object L$1;
        public int label;
        public final /* synthetic */ LocalUserTokenDataSource this$0;

        /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
        public AnonymousClass1(Pair<String, Integer>[] pairArr, LocalUserTokenDataSource localUserTokenDataSource, q70<? super AnonymousClass1> q70Var) {
            super(2, q70Var);
            this.$orders = pairArr;
            this.this$0 = localUserTokenDataSource;
        }

        @Override // kotlin.coroutines.jvm.internal.BaseContinuationImpl
        public final q70<te4> create(Object obj, q70<?> q70Var) {
            return new AnonymousClass1(this.$orders, this.this$0, q70Var);
        }

        @Override // defpackage.hd1
        public final Object invoke(c90 c90Var, q70<? super te4> q70Var) {
            return ((AnonymousClass1) create(c90Var, q70Var)).invokeSuspend(te4.a);
        }

        /* JADX WARN: Removed duplicated region for block: B:10:0x0033  */
        /* JADX WARN: Removed duplicated region for block: B:14:0x005c  */
        /* JADX WARN: Unsupported multi-entry loop pattern (BACK_EDGE: B:11:0x0057 -> B:13:0x005a). Please submit an issue!!! */
        @Override // kotlin.coroutines.jvm.internal.BaseContinuationImpl
        /*
            Code decompiled incorrectly, please refer to instructions dump.
            To view partially-correct code enable 'Show inconsistent code' option in preferences
        */
        public final java.lang.Object invokeSuspend(java.lang.Object r10) {
            /*
                r9 = this;
                java.lang.Object r0 = defpackage.gs1.d()
                int r1 = r9.label
                r2 = 1
                if (r1 == 0) goto L24
                if (r1 != r2) goto L1c
                int r1 = r9.I$1
                int r3 = r9.I$0
                java.lang.Object r4 = r9.L$1
                kotlin.Pair[] r4 = (kotlin.Pair[]) r4
                java.lang.Object r5 = r9.L$0
                net.safemoon.androidwallet.repository.dataSource.token.user.LocalUserTokenDataSource r5 = (net.safemoon.androidwallet.repository.dataSource.token.user.LocalUserTokenDataSource) r5
                defpackage.o83.b(r10)
                r10 = r9
                goto L5a
            L1c:
                java.lang.IllegalStateException r10 = new java.lang.IllegalStateException
                java.lang.String r0 = "call to 'resume' before 'invoke' with coroutine"
                r10.<init>(r0)
                throw r10
            L24:
                defpackage.o83.b(r10)
                kotlin.Pair<java.lang.String, java.lang.Integer>[] r10 = r9.$orders
                net.safemoon.androidwallet.repository.dataSource.token.user.LocalUserTokenDataSource r1 = r9.this$0
                int r3 = r10.length
                r4 = 0
                r5 = r1
                r1 = r4
                r4 = r10
                r10 = r9
            L31:
                if (r1 >= r3) goto L5c
                r6 = r4[r1]
                eg4 r7 = net.safemoon.androidwallet.repository.dataSource.token.user.LocalUserTokenDataSource.l(r5)
                java.lang.Object r8 = r6.getFirst()
                java.lang.String r8 = (java.lang.String) r8
                java.lang.Object r6 = r6.getSecond()
                java.lang.Number r6 = (java.lang.Number) r6
                int r6 = r6.intValue()
                r10.L$0 = r5
                r10.L$1 = r4
                r10.I$0 = r3
                r10.I$1 = r1
                r10.label = r2
                java.lang.Object r6 = r7.i(r8, r6, r10)
                if (r6 != r0) goto L5a
                return r0
            L5a:
                int r1 = r1 + r2
                goto L31
            L5c:
                te4 r10 = defpackage.te4.a
                return r10
            */
            throw new UnsupportedOperationException("Method not decompiled: net.safemoon.androidwallet.repository.dataSource.token.user.LocalUserTokenDataSource$updateOrders$1.AnonymousClass1.invokeSuspend(java.lang.Object):java.lang.Object");
        }
    }

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public LocalUserTokenDataSource$updateOrders$1(Pair<String, Integer>[] pairArr, LocalUserTokenDataSource localUserTokenDataSource, q70<? super LocalUserTokenDataSource$updateOrders$1> q70Var) {
        super(2, q70Var);
        this.$orders = pairArr;
        this.this$0 = localUserTokenDataSource;
    }

    @Override // kotlin.coroutines.jvm.internal.BaseContinuationImpl
    public final q70<te4> create(Object obj, q70<?> q70Var) {
        return new LocalUserTokenDataSource$updateOrders$1(this.$orders, this.this$0, q70Var);
    }

    @Override // defpackage.hd1
    public final Object invoke(c90 c90Var, q70<? super te4> q70Var) {
        return ((LocalUserTokenDataSource$updateOrders$1) create(c90Var, q70Var)).invokeSuspend(te4.a);
    }

    @Override // kotlin.coroutines.jvm.internal.BaseContinuationImpl
    public final Object invokeSuspend(Object obj) {
        Object d = gs1.d();
        int i = this.label;
        if (i == 0) {
            o83.b(obj);
            CoroutineDispatcher b = tp0.b();
            AnonymousClass1 anonymousClass1 = new AnonymousClass1(this.$orders, this.this$0, null);
            this.label = 1;
            if (kotlinx.coroutines.a.e(b, anonymousClass1, this) == d) {
                return d;
            }
        } else if (i != 1) {
            throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        } else {
            o83.b(obj);
        }
        return te4.a;
    }
}
