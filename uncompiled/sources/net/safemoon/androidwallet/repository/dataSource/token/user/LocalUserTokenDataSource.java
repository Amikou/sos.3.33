package net.safemoon.androidwallet.repository.dataSource.token.user;

import androidx.lifecycle.LiveData;
import java.util.Comparator;
import java.util.List;
import kotlin.Pair;
import net.safemoon.androidwallet.common.TokenType;
import net.safemoon.androidwallet.model.token.abstraction.IToken;
import net.safemoon.androidwallet.model.token.room.RoomToken;

/* compiled from: LocalUserTokenDataSource.kt */
/* loaded from: classes2.dex */
public final class LocalUserTokenDataSource implements zm1 {
    public final eg4 a;

    /* compiled from: Transformations.kt */
    /* loaded from: classes2.dex */
    public static final class a<I, O> implements ud1<List<? extends RoomToken>, List<? extends RoomToken>> {
        @Override // defpackage.ud1
        public final List<? extends RoomToken> apply(List<? extends RoomToken> list) {
            List<? extends RoomToken> list2 = list;
            for (RoomToken roomToken : list2) {
                if (roomToken.getOrder() == 0) {
                    String symbolWithType = roomToken.getSymbolWithType();
                    switch (symbolWithType.hashCode()) {
                        case -1158212709:
                            if (symbolWithType.equals("BEP_SAFEMOON_V2")) {
                                roomToken.setOrder(-3);
                                break;
                            } else {
                                continue;
                            }
                        case -774390032:
                            if (symbolWithType.equals("ERC_ETH")) {
                                roomToken.setOrder(-1);
                                break;
                            } else {
                                continue;
                            }
                        case 295942048:
                            if (symbolWithType.equals("BEP_SAFEMOON")) {
                                roomToken.setOrder(-1);
                                break;
                            } else {
                                continue;
                            }
                        case 497889956:
                            if (symbolWithType.equals("BEP_BNB")) {
                                roomToken.setOrder(-2);
                                break;
                            } else {
                                continue;
                            }
                        case 1284540565:
                            if (symbolWithType.equals("ERC_PSAFEMOON")) {
                                roomToken.setOrder(-2);
                                break;
                            } else {
                                continue;
                            }
                    }
                }
            }
            return j20.e0(list2, new b());
        }
    }

    /* compiled from: Comparisons.kt */
    /* loaded from: classes2.dex */
    public static final class b<T> implements Comparator {
        @Override // java.util.Comparator
        public final int compare(T t, T t2) {
            return l30.a(Integer.valueOf(((RoomToken) t).getOrder()), Integer.valueOf(((RoomToken) t2).getOrder()));
        }
    }

    /* compiled from: Transformations.kt */
    /* loaded from: classes2.dex */
    public static final class c<I, O> implements ud1<List<? extends RoomToken>, List<? extends RoomToken>> {
        @Override // defpackage.ud1
        public final List<? extends RoomToken> apply(List<? extends RoomToken> list) {
            List<? extends RoomToken> list2 = list;
            for (RoomToken roomToken : list2) {
                if (roomToken.getOrder() == 0) {
                    String symbolWithType = roomToken.getSymbolWithType();
                    switch (symbolWithType.hashCode()) {
                        case -1158212709:
                            if (symbolWithType.equals("BEP_SAFEMOON_V2")) {
                                roomToken.setOrder(-3);
                                break;
                            } else {
                                continue;
                            }
                        case -774390032:
                            if (symbolWithType.equals("ERC_ETH")) {
                                roomToken.setOrder(-1);
                                break;
                            } else {
                                continue;
                            }
                        case 295942048:
                            if (symbolWithType.equals("BEP_SAFEMOON")) {
                                roomToken.setOrder(-1);
                                break;
                            } else {
                                continue;
                            }
                        case 497889956:
                            if (symbolWithType.equals("BEP_BNB")) {
                                roomToken.setOrder(-2);
                                break;
                            } else {
                                continue;
                            }
                        case 1284540565:
                            if (symbolWithType.equals("ERC_PSAFEMOON")) {
                                roomToken.setOrder(-2);
                                break;
                            } else {
                                continue;
                            }
                    }
                }
            }
            return j20.e0(list2, new d());
        }
    }

    /* compiled from: Comparisons.kt */
    /* loaded from: classes2.dex */
    public static final class d<T> implements Comparator {
        @Override // java.util.Comparator
        public final int compare(T t, T t2) {
            return l30.a(Integer.valueOf(((RoomToken) t).getOrder()), Integer.valueOf(((RoomToken) t2).getOrder()));
        }
    }

    public LocalUserTokenDataSource(eg4 eg4Var) {
        fs1.f(eg4Var, "userDao");
        this.a = eg4Var;
    }

    @Override // defpackage.zm1
    public LiveData<List<RoomToken>> a(TokenType tokenType) {
        fs1.f(tokenType, "type");
        LiveData<List<RoomToken>> b2 = cb4.b(t40.b(this.a.n(tokenType.getChainId())), new a());
        fs1.e(b2, "Transformations.map(this) { transform(it) }");
        return b2;
    }

    @Override // defpackage.zm1
    public Object b(String str, String str2, q70<? super te4> q70Var) {
        Object b2 = this.a.b(str, str2, q70Var);
        return b2 == gs1.d() ? b2 : te4.a;
    }

    @Override // defpackage.zm1
    public void c(String str, double d2) {
        fs1.f(str, "symbolWithType");
        this.a.c(str, d2);
    }

    @Override // defpackage.zm1
    public LiveData<RoomToken> d(String str) {
        fs1.f(str, "symbolWithType");
        return this.a.d(str);
    }

    @Override // defpackage.zm1
    public Object e(String str, q70<? super RoomToken> q70Var) {
        return this.a.e(str, q70Var);
    }

    @Override // defpackage.zm1
    public void f(String str, double d2, double d3) {
        fs1.f(str, "symbolWithType");
        this.a.f(str, d2, d3);
    }

    @Override // defpackage.zm1
    public boolean g(IToken iToken, TokenType tokenType) {
        fs1.f(iToken, "token");
        fs1.f(tokenType, "tokenType");
        return this.a.j(iToken.getSymbolWithType(), tokenType.getChainId());
    }

    @Override // defpackage.zm1
    public void h(IToken iToken) {
        fs1.f(iToken, "token");
        this.a.k(new RoomToken(iToken));
    }

    @Override // defpackage.zm1
    public void i(IToken iToken) {
        fs1.f(iToken, "token");
        this.a.m(new RoomToken(iToken));
    }

    @Override // defpackage.zm1
    public void j(Pair<String, Integer>... pairArr) {
        fs1.f(pairArr, "orders");
        as.b(qg1.a, null, null, new LocalUserTokenDataSource$updateOrders$1(pairArr, this, null), 3, null);
    }

    @Override // defpackage.zm1
    public LiveData<List<RoomToken>> k() {
        LiveData<List<RoomToken>> b2 = cb4.b(t40.b(this.a.a()), new c());
        fs1.e(b2, "Transformations.map(this) { transform(it) }");
        return b2;
    }
}
