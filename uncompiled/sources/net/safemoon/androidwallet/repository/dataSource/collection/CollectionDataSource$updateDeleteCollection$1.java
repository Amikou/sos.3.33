package net.safemoon.androidwallet.repository.dataSource.collection;

import kotlin.coroutines.jvm.internal.ContinuationImpl;
import kotlin.coroutines.jvm.internal.a;

/* compiled from: CollectionDataSource.kt */
@a(c = "net.safemoon.androidwallet.repository.dataSource.collection.CollectionDataSource", f = "CollectionDataSource.kt", l = {44}, m = "updateDeleteCollection")
/* loaded from: classes2.dex */
public final class CollectionDataSource$updateDeleteCollection$1 extends ContinuationImpl {
    public Object L$0;
    public Object L$1;
    public int label;
    public /* synthetic */ Object result;
    public final /* synthetic */ CollectionDataSource this$0;

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public CollectionDataSource$updateDeleteCollection$1(CollectionDataSource collectionDataSource, q70<? super CollectionDataSource$updateDeleteCollection$1> q70Var) {
        super(q70Var);
        this.this$0 = collectionDataSource;
    }

    @Override // kotlin.coroutines.jvm.internal.BaseContinuationImpl
    public final Object invokeSuspend(Object obj) {
        this.result = obj;
        this.label |= Integer.MIN_VALUE;
        return this.this$0.f(null, this);
    }
}
