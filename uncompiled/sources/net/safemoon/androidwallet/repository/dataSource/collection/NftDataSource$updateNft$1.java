package net.safemoon.androidwallet.repository.dataSource.collection;

import kotlin.coroutines.jvm.internal.ContinuationImpl;
import kotlin.coroutines.jvm.internal.a;

/* compiled from: NftDataSource.kt */
@a(c = "net.safemoon.androidwallet.repository.dataSource.collection.NftDataSource", f = "NftDataSource.kt", l = {29, 31, 30}, m = "updateNft")
/* loaded from: classes2.dex */
public final class NftDataSource$updateNft$1 extends ContinuationImpl {
    public Object L$0;
    public Object L$1;
    public Object L$2;
    public Object L$3;
    public int label;
    public /* synthetic */ Object result;
    public final /* synthetic */ NftDataSource this$0;

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public NftDataSource$updateNft$1(NftDataSource nftDataSource, q70<? super NftDataSource$updateNft$1> q70Var) {
        super(q70Var);
        this.this$0 = nftDataSource;
    }

    @Override // kotlin.coroutines.jvm.internal.BaseContinuationImpl
    public final Object invokeSuspend(Object obj) {
        this.result = obj;
        this.label |= Integer.MIN_VALUE;
        return this.this$0.k(null, this);
    }
}
