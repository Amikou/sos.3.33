package net.safemoon.androidwallet.repository.dataSource.collection;

import kotlin.coroutines.jvm.internal.ContinuationImpl;
import kotlin.coroutines.jvm.internal.a;
import net.safemoon.androidwallet.repository.dataSource.collection.AttributeDataSource;

/* compiled from: AttributeDataSource.kt */
@a(c = "net.safemoon.androidwallet.repository.dataSource.collection.AttributeDataSource$Companion", f = "AttributeDataSource.kt", l = {52, 57}, m = "parseAttribute")
/* loaded from: classes2.dex */
public final class AttributeDataSource$Companion$parseAttribute$1 extends ContinuationImpl {
    public Object L$0;
    public Object L$1;
    public Object L$2;
    public int label;
    public /* synthetic */ Object result;
    public final /* synthetic */ AttributeDataSource.Companion this$0;

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public AttributeDataSource$Companion$parseAttribute$1(AttributeDataSource.Companion companion, q70<? super AttributeDataSource$Companion$parseAttribute$1> q70Var) {
        super(q70Var);
        this.this$0 = companion;
    }

    @Override // kotlin.coroutines.jvm.internal.BaseContinuationImpl
    public final Object invokeSuspend(Object obj) {
        this.result = obj;
        this.label |= Integer.MIN_VALUE;
        return this.this$0.b(null, null, this);
    }
}
