package net.safemoon.androidwallet.repository.dataSource.collection;

import androidx.lifecycle.LiveData;
import java.util.List;
import net.safemoon.androidwallet.model.collectible.RoomNFT;

/* compiled from: NftDataSource.kt */
/* loaded from: classes2.dex */
public final class NftDataSource {
    public final of2 a;

    public NftDataSource(of2 of2Var) {
        fs1.f(of2Var, "dao");
        this.a = of2Var;
    }

    /* JADX WARN: Removed duplicated region for block: B:10:0x0024  */
    /* JADX WARN: Removed duplicated region for block: B:16:0x0049  */
    /* JADX WARN: Removed duplicated region for block: B:23:0x006b  */
    /* JADX WARN: Removed duplicated region for block: B:40:0x00b3  */
    /*
        Code decompiled incorrectly, please refer to instructions dump.
        To view partially-correct code enable 'Show inconsistent code' option in preferences
    */
    public final java.lang.Object a(net.safemoon.androidwallet.model.collectible.MoralisNFTs r12, int r13, defpackage.q70<? super defpackage.te4> r14) {
        /*
            r11 = this;
            boolean r0 = r14 instanceof net.safemoon.androidwallet.repository.dataSource.collection.NftDataSource$clearCacheFromMoralis$1
            if (r0 == 0) goto L13
            r0 = r14
            net.safemoon.androidwallet.repository.dataSource.collection.NftDataSource$clearCacheFromMoralis$1 r0 = (net.safemoon.androidwallet.repository.dataSource.collection.NftDataSource$clearCacheFromMoralis$1) r0
            int r1 = r0.label
            r2 = -2147483648(0xffffffff80000000, float:-0.0)
            r3 = r1 & r2
            if (r3 == 0) goto L13
            int r1 = r1 - r2
            r0.label = r1
            goto L18
        L13:
            net.safemoon.androidwallet.repository.dataSource.collection.NftDataSource$clearCacheFromMoralis$1 r0 = new net.safemoon.androidwallet.repository.dataSource.collection.NftDataSource$clearCacheFromMoralis$1
            r0.<init>(r11, r14)
        L18:
            java.lang.Object r14 = r0.result
            java.lang.Object r1 = defpackage.gs1.d()
            int r2 = r0.label
            r3 = 2
            r4 = 1
            if (r2 == 0) goto L49
            if (r2 == r4) goto L3d
            if (r2 != r3) goto L35
            java.lang.Object r12 = r0.L$1
            java.util.Iterator r12 = (java.util.Iterator) r12
            java.lang.Object r13 = r0.L$0
            net.safemoon.androidwallet.repository.dataSource.collection.NftDataSource r13 = (net.safemoon.androidwallet.repository.dataSource.collection.NftDataSource) r13
            defpackage.o83.b(r14)
            goto Lad
        L35:
            java.lang.IllegalStateException r12 = new java.lang.IllegalStateException
            java.lang.String r13 = "call to 'resume' before 'invoke' with coroutine"
            r12.<init>(r13)
            throw r12
        L3d:
            java.lang.Object r12 = r0.L$1
            net.safemoon.androidwallet.model.collectible.MoralisNFTs r12 = (net.safemoon.androidwallet.model.collectible.MoralisNFTs) r12
            java.lang.Object r13 = r0.L$0
            net.safemoon.androidwallet.repository.dataSource.collection.NftDataSource r13 = (net.safemoon.androidwallet.repository.dataSource.collection.NftDataSource) r13
            defpackage.o83.b(r14)
            goto L5a
        L49:
            defpackage.o83.b(r14)
            r0.L$0 = r11
            r0.L$1 = r12
            r0.label = r4
            java.lang.Object r14 = r11.e(r13, r0)
            if (r14 != r1) goto L59
            return r1
        L59:
            r13 = r11
        L5a:
            java.util.List r14 = (java.util.List) r14
            java.util.ArrayList r2 = new java.util.ArrayList
            r2.<init>()
            java.util.Iterator r14 = r14.iterator()
        L65:
            boolean r5 = r14.hasNext()
            if (r5 == 0) goto La9
            java.lang.Object r5 = r14.next()
            r6 = r5
            net.safemoon.androidwallet.model.collectible.RoomNFT r6 = (net.safemoon.androidwallet.model.collectible.RoomNFT) r6
            java.util.ArrayList r7 = r12.getResult()
            boolean r8 = r7 instanceof java.util.Collection
            r9 = 0
            if (r8 == 0) goto L82
            boolean r8 = r7.isEmpty()
            if (r8 == 0) goto L82
            goto La1
        L82:
            java.util.Iterator r7 = r7.iterator()
        L86:
            boolean r8 = r7.hasNext()
            if (r8 == 0) goto La1
            java.lang.Object r8 = r7.next()
            net.safemoon.androidwallet.model.collectible.MoralisNft r8 = (net.safemoon.androidwallet.model.collectible.MoralisNft) r8
            java.lang.String r10 = r6.getToken_id()
            java.lang.String r8 = r8.getTokenId()
            boolean r8 = defpackage.fs1.b(r10, r8)
            if (r8 == 0) goto L86
            r9 = r4
        La1:
            r6 = r9 ^ 1
            if (r6 == 0) goto L65
            r2.add(r5)
            goto L65
        La9:
            java.util.Iterator r12 = r2.iterator()
        Lad:
            boolean r14 = r12.hasNext()
            if (r14 == 0) goto Ld2
            java.lang.Object r14 = r12.next()
            net.safemoon.androidwallet.model.collectible.RoomNFT r14 = (net.safemoon.androidwallet.model.collectible.RoomNFT) r14
            java.lang.Long r2 = r14.getId()
            if (r2 != 0) goto Lc0
            goto Lad
        Lc0:
            r2.longValue()
            of2 r2 = r13.a
            r0.L$0 = r13
            r0.L$1 = r12
            r0.label = r3
            java.lang.Object r14 = r2.j(r14, r0)
            if (r14 != r1) goto Lad
            return r1
        Ld2:
            te4 r12 = defpackage.te4.a
            return r12
        */
        throw new UnsupportedOperationException("Method not decompiled: net.safemoon.androidwallet.repository.dataSource.collection.NftDataSource.a(net.safemoon.androidwallet.model.collectible.MoralisNFTs, int, q70):java.lang.Object");
    }

    /* JADX WARN: Removed duplicated region for block: B:10:0x0024  */
    /* JADX WARN: Removed duplicated region for block: B:16:0x0049  */
    /* JADX WARN: Removed duplicated region for block: B:23:0x006b  */
    /* JADX WARN: Removed duplicated region for block: B:46:0x00b8  */
    /*
        Code decompiled incorrectly, please refer to instructions dump.
        To view partially-correct code enable 'Show inconsistent code' option in preferences
    */
    public final java.lang.Object b(net.safemoon.androidwallet.model.collectible.Assets r11, long r12, defpackage.q70<? super defpackage.te4> r14) {
        /*
            r10 = this;
            boolean r0 = r14 instanceof net.safemoon.androidwallet.repository.dataSource.collection.NftDataSource$clearCacheFromOpenSea$1
            if (r0 == 0) goto L13
            r0 = r14
            net.safemoon.androidwallet.repository.dataSource.collection.NftDataSource$clearCacheFromOpenSea$1 r0 = (net.safemoon.androidwallet.repository.dataSource.collection.NftDataSource$clearCacheFromOpenSea$1) r0
            int r1 = r0.label
            r2 = -2147483648(0xffffffff80000000, float:-0.0)
            r3 = r1 & r2
            if (r3 == 0) goto L13
            int r1 = r1 - r2
            r0.label = r1
            goto L18
        L13:
            net.safemoon.androidwallet.repository.dataSource.collection.NftDataSource$clearCacheFromOpenSea$1 r0 = new net.safemoon.androidwallet.repository.dataSource.collection.NftDataSource$clearCacheFromOpenSea$1
            r0.<init>(r10, r14)
        L18:
            java.lang.Object r14 = r0.result
            java.lang.Object r1 = defpackage.gs1.d()
            int r2 = r0.label
            r3 = 2
            r4 = 1
            if (r2 == 0) goto L49
            if (r2 == r4) goto L3d
            if (r2 != r3) goto L35
            java.lang.Object r11 = r0.L$1
            java.util.Iterator r11 = (java.util.Iterator) r11
            java.lang.Object r12 = r0.L$0
            net.safemoon.androidwallet.repository.dataSource.collection.NftDataSource r12 = (net.safemoon.androidwallet.repository.dataSource.collection.NftDataSource) r12
            defpackage.o83.b(r14)
            goto Lb2
        L35:
            java.lang.IllegalStateException r11 = new java.lang.IllegalStateException
            java.lang.String r12 = "call to 'resume' before 'invoke' with coroutine"
            r11.<init>(r12)
            throw r11
        L3d:
            java.lang.Object r11 = r0.L$1
            net.safemoon.androidwallet.model.collectible.Assets r11 = (net.safemoon.androidwallet.model.collectible.Assets) r11
            java.lang.Object r12 = r0.L$0
            net.safemoon.androidwallet.repository.dataSource.collection.NftDataSource r12 = (net.safemoon.androidwallet.repository.dataSource.collection.NftDataSource) r12
            defpackage.o83.b(r14)
            goto L5a
        L49:
            defpackage.o83.b(r14)
            r0.L$0 = r10
            r0.L$1 = r11
            r0.label = r4
            java.lang.Object r14 = r10.d(r12, r0)
            if (r14 != r1) goto L59
            return r1
        L59:
            r12 = r10
        L5a:
            java.util.List r14 = (java.util.List) r14
            java.util.ArrayList r13 = new java.util.ArrayList
            r13.<init>()
            java.util.Iterator r14 = r14.iterator()
        L65:
            boolean r2 = r14.hasNext()
            if (r2 == 0) goto Lae
            java.lang.Object r2 = r14.next()
            r5 = r2
            net.safemoon.androidwallet.model.collectible.RoomNFT r5 = (net.safemoon.androidwallet.model.collectible.RoomNFT) r5
            java.util.List r6 = r11.getAssets()
            r7 = 0
            if (r6 != 0) goto L7a
            goto La8
        L7a:
            boolean r8 = r6.isEmpty()
            if (r8 == 0) goto L82
        L80:
            r5 = r7
            goto La5
        L82:
            java.util.Iterator r6 = r6.iterator()
        L86:
            boolean r8 = r6.hasNext()
            if (r8 == 0) goto L80
            java.lang.Object r8 = r6.next()
            net.safemoon.androidwallet.model.collectible.Asset r8 = (net.safemoon.androidwallet.model.collectible.Asset) r8
            java.lang.String r9 = r5.getToken_id()
            if (r8 != 0) goto L9a
            r8 = 0
            goto L9e
        L9a:
            java.lang.String r8 = r8.getToken_id()
        L9e:
            boolean r8 = defpackage.fs1.b(r9, r8)
            if (r8 == 0) goto L86
            r5 = r4
        La5:
            if (r5 != 0) goto La8
            r7 = r4
        La8:
            if (r7 == 0) goto L65
            r13.add(r2)
            goto L65
        Lae:
            java.util.Iterator r11 = r13.iterator()
        Lb2:
            boolean r13 = r11.hasNext()
            if (r13 == 0) goto Lcb
            java.lang.Object r13 = r11.next()
            net.safemoon.androidwallet.model.collectible.RoomNFT r13 = (net.safemoon.androidwallet.model.collectible.RoomNFT) r13
            r0.L$0 = r12
            r0.L$1 = r11
            r0.label = r3
            java.lang.Object r13 = r12.c(r13, r0)
            if (r13 != r1) goto Lb2
            return r1
        Lcb:
            te4 r11 = defpackage.te4.a
            return r11
        */
        throw new UnsupportedOperationException("Method not decompiled: net.safemoon.androidwallet.repository.dataSource.collection.NftDataSource.b(net.safemoon.androidwallet.model.collectible.Assets, long, q70):java.lang.Object");
    }

    public final Object c(RoomNFT roomNFT, q70<? super te4> q70Var) {
        Object j = this.a.j(roomNFT, q70Var);
        return j == gs1.d() ? j : te4.a;
    }

    public final Object d(long j, q70<? super List<RoomNFT>> q70Var) {
        return this.a.e(j, q70Var);
    }

    public final Object e(int i, q70<? super List<RoomNFT>> q70Var) {
        return this.a.h(i, q70Var);
    }

    public final Object f(long j, q70<? super RoomNFT> q70Var) {
        return this.a.d(j, q70Var);
    }

    public final Object g(String str, long j, q70<? super RoomNFT> q70Var) {
        return this.a.b(str, j, q70Var);
    }

    public final Object h(RoomNFT roomNFT, q70<? super Boolean> q70Var) {
        of2 of2Var = this.a;
        String token_id = roomNFT.getToken_id();
        if (token_id == null) {
            token_id = "";
        }
        return of2Var.c(token_id, roomNFT.getCollectionId(), q70Var);
    }

    public final LiveData<List<RoomNFT>> i(long j) {
        return this.a.i(j);
    }

    /* JADX WARN: Removed duplicated region for block: B:10:0x0025  */
    /* JADX WARN: Removed duplicated region for block: B:18:0x0054  */
    /* JADX WARN: Removed duplicated region for block: B:24:0x006d  */
    /* JADX WARN: Removed duplicated region for block: B:34:0x00af A[RETURN] */
    /* JADX WARN: Removed duplicated region for block: B:35:0x00b0 A[PHI: r10 
      PHI: (r10v13 java.lang.Object) = (r10v9 java.lang.Object), (r10v1 java.lang.Object) binds: [B:33:0x00ad, B:13:0x002b] A[DONT_GENERATE, DONT_INLINE], RETURN] */
    /*
        Code decompiled incorrectly, please refer to instructions dump.
        To view partially-correct code enable 'Show inconsistent code' option in preferences
    */
    public final java.lang.Object j(net.safemoon.androidwallet.model.collectible.RoomNFT r9, defpackage.q70<? super java.lang.Long> r10) {
        /*
            r8 = this;
            boolean r0 = r10 instanceof net.safemoon.androidwallet.repository.dataSource.collection.NftDataSource$save$1
            if (r0 == 0) goto L13
            r0 = r10
            net.safemoon.androidwallet.repository.dataSource.collection.NftDataSource$save$1 r0 = (net.safemoon.androidwallet.repository.dataSource.collection.NftDataSource$save$1) r0
            int r1 = r0.label
            r2 = -2147483648(0xffffffff80000000, float:-0.0)
            r3 = r1 & r2
            if (r3 == 0) goto L13
            int r1 = r1 - r2
            r0.label = r1
            goto L18
        L13:
            net.safemoon.androidwallet.repository.dataSource.collection.NftDataSource$save$1 r0 = new net.safemoon.androidwallet.repository.dataSource.collection.NftDataSource$save$1
            r0.<init>(r8, r10)
        L18:
            java.lang.Object r10 = r0.result
            java.lang.Object r1 = defpackage.gs1.d()
            int r2 = r0.label
            r3 = 3
            r4 = 2
            r5 = 1
            if (r2 == 0) goto L54
            if (r2 == r5) goto L48
            if (r2 == r4) goto L38
            if (r2 != r3) goto L30
            defpackage.o83.b(r10)
            goto Lb0
        L30:
            java.lang.IllegalStateException r9 = new java.lang.IllegalStateException
            java.lang.String r10 = "call to 'resume' before 'invoke' with coroutine"
            r9.<init>(r10)
            throw r9
        L38:
            java.lang.Object r9 = r0.L$2
            net.safemoon.androidwallet.model.collectible.RoomNFT r9 = (net.safemoon.androidwallet.model.collectible.RoomNFT) r9
            java.lang.Object r2 = r0.L$1
            net.safemoon.androidwallet.model.collectible.RoomNFT r2 = (net.safemoon.androidwallet.model.collectible.RoomNFT) r2
            java.lang.Object r4 = r0.L$0
            net.safemoon.androidwallet.repository.dataSource.collection.NftDataSource r4 = (net.safemoon.androidwallet.repository.dataSource.collection.NftDataSource) r4
            defpackage.o83.b(r10)
            goto L8c
        L48:
            java.lang.Object r9 = r0.L$1
            net.safemoon.androidwallet.model.collectible.RoomNFT r9 = (net.safemoon.androidwallet.model.collectible.RoomNFT) r9
            java.lang.Object r2 = r0.L$0
            net.safemoon.androidwallet.repository.dataSource.collection.NftDataSource r2 = (net.safemoon.androidwallet.repository.dataSource.collection.NftDataSource) r2
            defpackage.o83.b(r10)
            goto L65
        L54:
            defpackage.o83.b(r10)
            r0.L$0 = r8
            r0.L$1 = r9
            r0.label = r5
            java.lang.Object r10 = r8.h(r9, r0)
            if (r10 != r1) goto L64
            return r1
        L64:
            r2 = r8
        L65:
            java.lang.Boolean r10 = (java.lang.Boolean) r10
            boolean r10 = r10.booleanValue()
            if (r10 == 0) goto L9e
            of2 r10 = r2.a
            java.lang.String r5 = r9.getToken_id()
            if (r5 != 0) goto L77
            java.lang.String r5 = ""
        L77:
            long r6 = r9.getCollectionId()
            r0.L$0 = r2
            r0.L$1 = r9
            r0.L$2 = r9
            r0.label = r4
            java.lang.Object r10 = r10.b(r5, r6, r0)
            if (r10 != r1) goto L8a
            return r1
        L8a:
            r4 = r2
            r2 = r9
        L8c:
            net.safemoon.androidwallet.model.collectible.RoomNFT r10 = (net.safemoon.androidwallet.model.collectible.RoomNFT) r10
            java.lang.Long r5 = r10.getId()
            r9.setId(r5)
            int r10 = r10.getOrder()
            r9.setOrder(r10)
            r9 = r2
            r2 = r4
        L9e:
            of2 r10 = r2.a
            r2 = 0
            r0.L$0 = r2
            r0.L$1 = r2
            r0.L$2 = r2
            r0.label = r3
            java.lang.Object r10 = r10.g(r9, r0)
            if (r10 != r1) goto Lb0
            return r1
        Lb0:
            return r10
        */
        throw new UnsupportedOperationException("Method not decompiled: net.safemoon.androidwallet.repository.dataSource.collection.NftDataSource.j(net.safemoon.androidwallet.model.collectible.RoomNFT, q70):java.lang.Object");
    }

    /* JADX WARN: Code restructure failed: missing block: B:38:0x00b1, code lost:
        if ((r7.length() == 0) != false) goto L19;
     */
    /* JADX WARN: Code restructure failed: missing block: B:49:0x00ce, code lost:
        if ((r7.length() == 0) != false) goto L24;
     */
    /* JADX WARN: Code restructure failed: missing block: B:60:0x00eb, code lost:
        if ((r7.length() == 0) != false) goto L29;
     */
    /* JADX WARN: Code restructure failed: missing block: B:71:0x0107, code lost:
        if ((r6.length() == 0) != false) goto L34;
     */
    /* JADX WARN: Removed duplicated region for block: B:10:0x0025  */
    /* JADX WARN: Removed duplicated region for block: B:18:0x0058  */
    /* JADX WARN: Removed duplicated region for block: B:24:0x0071  */
    /* JADX WARN: Removed duplicated region for block: B:34:0x00a8  */
    /* JADX WARN: Removed duplicated region for block: B:41:0x00b6  */
    /* JADX WARN: Removed duplicated region for block: B:45:0x00c5  */
    /* JADX WARN: Removed duplicated region for block: B:52:0x00d3  */
    /* JADX WARN: Removed duplicated region for block: B:56:0x00e2  */
    /* JADX WARN: Removed duplicated region for block: B:63:0x00f0  */
    /* JADX WARN: Removed duplicated region for block: B:67:0x00ff  */
    /* JADX WARN: Removed duplicated region for block: B:74:0x010c  */
    /* JADX WARN: Removed duplicated region for block: B:77:0x013a A[RETURN] */
    /* JADX WARN: Removed duplicated region for block: B:78:0x013b A[PHI: r13 
      PHI: (r13v13 java.lang.Object) = (r13v12 java.lang.Object), (r13v1 java.lang.Object) binds: [B:76:0x0138, B:13:0x002b] A[DONT_GENERATE, DONT_INLINE], RETURN] */
    /* JADX WARN: Removed duplicated region for block: B:79:0x013c  */
    /*
        Code decompiled incorrectly, please refer to instructions dump.
        To view partially-correct code enable 'Show inconsistent code' option in preferences
    */
    public final java.lang.Object k(net.safemoon.androidwallet.model.collectible.RoomNFT r12, defpackage.q70<? super java.lang.Long> r13) {
        /*
            Method dump skipped, instructions count: 323
            To view this dump change 'Code comments level' option to 'DEBUG'
        */
        throw new UnsupportedOperationException("Method not decompiled: net.safemoon.androidwallet.repository.dataSource.collection.NftDataSource.k(net.safemoon.androidwallet.model.collectible.RoomNFT, q70):java.lang.Object");
    }

    /* JADX WARN: Removed duplicated region for block: B:10:0x0023  */
    /* JADX WARN: Removed duplicated region for block: B:14:0x0039  */
    /* JADX WARN: Removed duplicated region for block: B:17:0x0047  */
    /*
        Code decompiled incorrectly, please refer to instructions dump.
        To view partially-correct code enable 'Show inconsistent code' option in preferences
    */
    public final java.lang.Object l(java.util.List<kotlin.Pair<java.lang.Long, java.lang.Integer>> r8, defpackage.q70<? super defpackage.te4> r9) {
        /*
            r7 = this;
            boolean r0 = r9 instanceof net.safemoon.androidwallet.repository.dataSource.collection.NftDataSource$updateOrders$1
            if (r0 == 0) goto L13
            r0 = r9
            net.safemoon.androidwallet.repository.dataSource.collection.NftDataSource$updateOrders$1 r0 = (net.safemoon.androidwallet.repository.dataSource.collection.NftDataSource$updateOrders$1) r0
            int r1 = r0.label
            r2 = -2147483648(0xffffffff80000000, float:-0.0)
            r3 = r1 & r2
            if (r3 == 0) goto L13
            int r1 = r1 - r2
            r0.label = r1
            goto L18
        L13:
            net.safemoon.androidwallet.repository.dataSource.collection.NftDataSource$updateOrders$1 r0 = new net.safemoon.androidwallet.repository.dataSource.collection.NftDataSource$updateOrders$1
            r0.<init>(r7, r9)
        L18:
            java.lang.Object r9 = r0.result
            java.lang.Object r1 = defpackage.gs1.d()
            int r2 = r0.label
            r3 = 1
            if (r2 == 0) goto L39
            if (r2 != r3) goto L31
            java.lang.Object r8 = r0.L$1
            java.util.Iterator r8 = (java.util.Iterator) r8
            java.lang.Object r2 = r0.L$0
            net.safemoon.androidwallet.repository.dataSource.collection.NftDataSource r2 = (net.safemoon.androidwallet.repository.dataSource.collection.NftDataSource) r2
            defpackage.o83.b(r9)
            goto L41
        L31:
            java.lang.IllegalStateException r8 = new java.lang.IllegalStateException
            java.lang.String r9 = "call to 'resume' before 'invoke' with coroutine"
            r8.<init>(r9)
            throw r8
        L39:
            defpackage.o83.b(r9)
            java.util.Iterator r8 = r8.iterator()
            r2 = r7
        L41:
            boolean r9 = r8.hasNext()
            if (r9 == 0) goto L73
            java.lang.Object r9 = r8.next()
            kotlin.Pair r9 = (kotlin.Pair) r9
            java.lang.Object r4 = r9.getFirst()
            java.lang.Long r4 = (java.lang.Long) r4
            if (r4 != 0) goto L56
            goto L41
        L56:
            long r4 = r4.longValue()
            of2 r6 = r2.a
            java.lang.Object r9 = r9.getSecond()
            java.lang.Number r9 = (java.lang.Number) r9
            int r9 = r9.intValue()
            r0.L$0 = r2
            r0.L$1 = r8
            r0.label = r3
            java.lang.Object r9 = r6.a(r4, r9, r0)
            if (r9 != r1) goto L41
            return r1
        L73:
            te4 r8 = defpackage.te4.a
            return r8
        */
        throw new UnsupportedOperationException("Method not decompiled: net.safemoon.androidwallet.repository.dataSource.collection.NftDataSource.l(java.util.List, q70):java.lang.Object");
    }
}
