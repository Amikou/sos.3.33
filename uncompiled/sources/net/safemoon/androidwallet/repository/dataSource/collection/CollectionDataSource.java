package net.safemoon.androidwallet.repository.dataSource.collection;

import androidx.lifecycle.LiveData;
import java.util.List;
import net.safemoon.androidwallet.model.collectible.RoomCollection;
import net.safemoon.androidwallet.model.collectible.RoomCollectionAndNft;

/* compiled from: CollectionDataSource.kt */
/* loaded from: classes2.dex */
public final class CollectionDataSource {
    public final j10 a;
    public final of2 b;

    public CollectionDataSource(j10 j10Var, of2 of2Var) {
        fs1.f(j10Var, "dao");
        fs1.f(of2Var, "nftDao");
        this.a = j10Var;
        this.b = of2Var;
    }

    /* JADX WARN: Removed duplicated region for block: B:10:0x0029  */
    /* JADX WARN: Removed duplicated region for block: B:19:0x0068  */
    /* JADX WARN: Removed duplicated region for block: B:26:0x008e  */
    /* JADX WARN: Removed duplicated region for block: B:48:0x00e9  */
    /* JADX WARN: Removed duplicated region for block: B:54:0x0107  */
    /* JADX WARN: Removed duplicated region for block: B:60:0x0121  */
    /* JADX WARN: Unsupported multi-entry loop pattern (BACK_EDGE: B:52:0x0104 -> B:59:0x011f). Please submit an issue!!! */
    /* JADX WARN: Unsupported multi-entry loop pattern (BACK_EDGE: B:57:0x011d -> B:58:0x011e). Please submit an issue!!! */
    /*
        Code decompiled incorrectly, please refer to instructions dump.
        To view partially-correct code enable 'Show inconsistent code' option in preferences
    */
    public final java.lang.Object a(net.safemoon.androidwallet.model.collectible.MoralisNFTs r18, int r19, defpackage.q70<? super defpackage.te4> r20) {
        /*
            Method dump skipped, instructions count: 292
            To view this dump change 'Code comments level' option to 'DEBUG'
        */
        throw new UnsupportedOperationException("Method not decompiled: net.safemoon.androidwallet.repository.dataSource.collection.CollectionDataSource.a(net.safemoon.androidwallet.model.collectible.MoralisNFTs, int, q70):java.lang.Object");
    }

    /* JADX WARN: Removed duplicated region for block: B:10:0x002a  */
    /* JADX WARN: Removed duplicated region for block: B:19:0x006a  */
    /* JADX WARN: Removed duplicated region for block: B:26:0x0090  */
    /* JADX WARN: Removed duplicated region for block: B:32:0x00ba  */
    /* JADX WARN: Removed duplicated region for block: B:43:0x0113  */
    /* JADX WARN: Removed duplicated region for block: B:68:0x017d  */
    /* JADX WARN: Removed duplicated region for block: B:73:0x019a  */
    /* JADX WARN: Removed duplicated region for block: B:74:0x019d  */
    /* JADX WARN: Removed duplicated region for block: B:79:0x01b7  */
    /* JADX WARN: Unsupported multi-entry loop pattern (BACK_EDGE: B:73:0x019a -> B:66:0x0177). Please submit an issue!!! */
    /* JADX WARN: Unsupported multi-entry loop pattern (BACK_EDGE: B:77:0x01b3 -> B:78:0x01b4). Please submit an issue!!! */
    /*
        Code decompiled incorrectly, please refer to instructions dump.
        To view partially-correct code enable 'Show inconsistent code' option in preferences
    */
    public final java.lang.Object b(java.util.List<net.safemoon.androidwallet.model.collectible.Collectible> r19, int r20, defpackage.q70<? super defpackage.te4> r21) {
        /*
            Method dump skipped, instructions count: 442
            To view this dump change 'Code comments level' option to 'DEBUG'
        */
        throw new UnsupportedOperationException("Method not decompiled: net.safemoon.androidwallet.repository.dataSource.collection.CollectionDataSource.b(java.util.List, int, q70):java.lang.Object");
    }

    public final Object c(int i, q70<? super List<RoomCollection>> q70Var) {
        return this.a.b(i, q70Var);
    }

    public final LiveData<List<RoomCollectionAndNft>> d(int i) {
        return this.a.f(i);
    }

    /* JADX WARN: Removed duplicated region for block: B:10:0x0028  */
    /* JADX WARN: Removed duplicated region for block: B:16:0x0045  */
    /* JADX WARN: Removed duplicated region for block: B:23:0x0071  */
    /* JADX WARN: Removed duplicated region for block: B:35:0x00ca  */
    /* JADX WARN: Removed duplicated region for block: B:36:0x00f2  */
    /* JADX WARN: Removed duplicated region for block: B:47:0x0116 A[RETURN] */
    /* JADX WARN: Removed duplicated region for block: B:48:0x0117 A[PHI: r1 
      PHI: (r1v21 java.lang.Object) = (r1v11 java.lang.Object), (r1v1 java.lang.Object) binds: [B:46:0x0114, B:12:0x002c] A[DONT_GENERATE, DONT_INLINE], RETURN] */
    /* JADX WARN: Removed duplicated region for block: B:49:0x00c4 A[EDGE_INSN: B:49:0x00c4->B:33:0x00c4 ?: BREAK  , SYNTHETIC] */
    /*
        Code decompiled incorrectly, please refer to instructions dump.
        To view partially-correct code enable 'Show inconsistent code' option in preferences
    */
    public final java.lang.Object e(net.safemoon.androidwallet.model.collectible.RoomCollection r17, defpackage.q70<? super java.lang.Long> r18) {
        /*
            Method dump skipped, instructions count: 280
            To view this dump change 'Code comments level' option to 'DEBUG'
        */
        throw new UnsupportedOperationException("Method not decompiled: net.safemoon.androidwallet.repository.dataSource.collection.CollectionDataSource.e(net.safemoon.androidwallet.model.collectible.RoomCollection, q70):java.lang.Object");
    }

    /* JADX WARN: Removed duplicated region for block: B:10:0x0023  */
    /* JADX WARN: Removed duplicated region for block: B:14:0x0039  */
    /* JADX WARN: Removed duplicated region for block: B:17:0x0047  */
    /*
        Code decompiled incorrectly, please refer to instructions dump.
        To view partially-correct code enable 'Show inconsistent code' option in preferences
    */
    public final java.lang.Object f(java.util.List<kotlin.Pair<java.lang.Long, java.lang.Integer>> r8, defpackage.q70<? super defpackage.te4> r9) {
        /*
            r7 = this;
            boolean r0 = r9 instanceof net.safemoon.androidwallet.repository.dataSource.collection.CollectionDataSource$updateDeleteCollection$1
            if (r0 == 0) goto L13
            r0 = r9
            net.safemoon.androidwallet.repository.dataSource.collection.CollectionDataSource$updateDeleteCollection$1 r0 = (net.safemoon.androidwallet.repository.dataSource.collection.CollectionDataSource$updateDeleteCollection$1) r0
            int r1 = r0.label
            r2 = -2147483648(0xffffffff80000000, float:-0.0)
            r3 = r1 & r2
            if (r3 == 0) goto L13
            int r1 = r1 - r2
            r0.label = r1
            goto L18
        L13:
            net.safemoon.androidwallet.repository.dataSource.collection.CollectionDataSource$updateDeleteCollection$1 r0 = new net.safemoon.androidwallet.repository.dataSource.collection.CollectionDataSource$updateDeleteCollection$1
            r0.<init>(r7, r9)
        L18:
            java.lang.Object r9 = r0.result
            java.lang.Object r1 = defpackage.gs1.d()
            int r2 = r0.label
            r3 = 1
            if (r2 == 0) goto L39
            if (r2 != r3) goto L31
            java.lang.Object r8 = r0.L$1
            java.util.Iterator r8 = (java.util.Iterator) r8
            java.lang.Object r2 = r0.L$0
            net.safemoon.androidwallet.repository.dataSource.collection.CollectionDataSource r2 = (net.safemoon.androidwallet.repository.dataSource.collection.CollectionDataSource) r2
            defpackage.o83.b(r9)
            goto L41
        L31:
            java.lang.IllegalStateException r8 = new java.lang.IllegalStateException
            java.lang.String r9 = "call to 'resume' before 'invoke' with coroutine"
            r8.<init>(r9)
            throw r8
        L39:
            defpackage.o83.b(r9)
            java.util.Iterator r8 = r8.iterator()
            r2 = r7
        L41:
            boolean r9 = r8.hasNext()
            if (r9 == 0) goto L73
            java.lang.Object r9 = r8.next()
            kotlin.Pair r9 = (kotlin.Pair) r9
            java.lang.Object r4 = r9.getFirst()
            java.lang.Long r4 = (java.lang.Long) r4
            if (r4 != 0) goto L56
            goto L41
        L56:
            long r4 = r4.longValue()
            j10 r6 = r2.a
            java.lang.Object r9 = r9.getSecond()
            java.lang.Number r9 = (java.lang.Number) r9
            int r9 = r9.intValue()
            r0.L$0 = r2
            r0.L$1 = r8
            r0.label = r3
            java.lang.Object r9 = r6.d(r4, r9, r0)
            if (r9 != r1) goto L41
            return r1
        L73:
            te4 r8 = defpackage.te4.a
            return r8
        */
        throw new UnsupportedOperationException("Method not decompiled: net.safemoon.androidwallet.repository.dataSource.collection.CollectionDataSource.f(java.util.List, q70):java.lang.Object");
    }

    /* JADX WARN: Removed duplicated region for block: B:10:0x0023  */
    /* JADX WARN: Removed duplicated region for block: B:14:0x0039  */
    /* JADX WARN: Removed duplicated region for block: B:17:0x0047  */
    /*
        Code decompiled incorrectly, please refer to instructions dump.
        To view partially-correct code enable 'Show inconsistent code' option in preferences
    */
    public final java.lang.Object g(java.util.List<kotlin.Pair<java.lang.Long, java.lang.Integer>> r8, defpackage.q70<? super defpackage.te4> r9) {
        /*
            r7 = this;
            boolean r0 = r9 instanceof net.safemoon.androidwallet.repository.dataSource.collection.CollectionDataSource$updateOrders$1
            if (r0 == 0) goto L13
            r0 = r9
            net.safemoon.androidwallet.repository.dataSource.collection.CollectionDataSource$updateOrders$1 r0 = (net.safemoon.androidwallet.repository.dataSource.collection.CollectionDataSource$updateOrders$1) r0
            int r1 = r0.label
            r2 = -2147483648(0xffffffff80000000, float:-0.0)
            r3 = r1 & r2
            if (r3 == 0) goto L13
            int r1 = r1 - r2
            r0.label = r1
            goto L18
        L13:
            net.safemoon.androidwallet.repository.dataSource.collection.CollectionDataSource$updateOrders$1 r0 = new net.safemoon.androidwallet.repository.dataSource.collection.CollectionDataSource$updateOrders$1
            r0.<init>(r7, r9)
        L18:
            java.lang.Object r9 = r0.result
            java.lang.Object r1 = defpackage.gs1.d()
            int r2 = r0.label
            r3 = 1
            if (r2 == 0) goto L39
            if (r2 != r3) goto L31
            java.lang.Object r8 = r0.L$1
            java.util.Iterator r8 = (java.util.Iterator) r8
            java.lang.Object r2 = r0.L$0
            net.safemoon.androidwallet.repository.dataSource.collection.CollectionDataSource r2 = (net.safemoon.androidwallet.repository.dataSource.collection.CollectionDataSource) r2
            defpackage.o83.b(r9)
            goto L41
        L31:
            java.lang.IllegalStateException r8 = new java.lang.IllegalStateException
            java.lang.String r9 = "call to 'resume' before 'invoke' with coroutine"
            r8.<init>(r9)
            throw r8
        L39:
            defpackage.o83.b(r9)
            java.util.Iterator r8 = r8.iterator()
            r2 = r7
        L41:
            boolean r9 = r8.hasNext()
            if (r9 == 0) goto L73
            java.lang.Object r9 = r8.next()
            kotlin.Pair r9 = (kotlin.Pair) r9
            java.lang.Object r4 = r9.getFirst()
            java.lang.Long r4 = (java.lang.Long) r4
            if (r4 != 0) goto L56
            goto L41
        L56:
            long r4 = r4.longValue()
            j10 r6 = r2.a
            java.lang.Object r9 = r9.getSecond()
            java.lang.Number r9 = (java.lang.Number) r9
            int r9 = r9.intValue()
            r0.L$0 = r2
            r0.L$1 = r8
            r0.label = r3
            java.lang.Object r9 = r6.a(r4, r9, r0)
            if (r9 != r1) goto L41
            return r1
        L73:
            te4 r8 = defpackage.te4.a
            return r8
        */
        throw new UnsupportedOperationException("Method not decompiled: net.safemoon.androidwallet.repository.dataSource.collection.CollectionDataSource.g(java.util.List, q70):java.lang.Object");
    }
}
