package net.safemoon.androidwallet.repository.dataSource.collection;

import android.app.Application;
import kotlin.text.StringsKt__StringsKt;

/* compiled from: AttributeDataSource.kt */
/* loaded from: classes2.dex */
public final class AttributeDataSource {
    public static final Companion c = new Companion(null);
    public final Application a;
    public final long b;

    /* compiled from: AttributeDataSource.kt */
    /* loaded from: classes2.dex */
    public static final class Companion {
        public Companion() {
        }

        public /* synthetic */ Companion(qi0 qi0Var) {
            this();
        }

        public final Object a(String str, q70<? super String> q70Var) {
            try {
                if (StringsKt__StringsKt.M(str, "base64,", false, 2, null)) {
                    return StringsKt__StringsKt.w0(str, new String[]{"base64,"}, false, 0, 6, null).get(1);
                }
            } catch (Exception unused) {
            }
            return null;
        }

        /* JADX WARN: Removed duplicated region for block: B:10:0x0024  */
        /* JADX WARN: Removed duplicated region for block: B:16:0x0051  */
        /* JADX WARN: Removed duplicated region for block: B:37:0x00df  */
        /* JADX WARN: Removed duplicated region for block: B:40:0x00f7  */
        /* JADX WARN: Removed duplicated region for block: B:43:0x0106  */
        /* JADX WARN: Removed duplicated region for block: B:46:0x0115  */
        /* JADX WARN: Removed duplicated region for block: B:49:0x0124  */
        /*
            Code decompiled incorrectly, please refer to instructions dump.
            To view partially-correct code enable 'Show inconsistent code' option in preferences
        */
        public final java.lang.Object b(net.safemoon.androidwallet.model.collectible.RoomNFT r7, java.lang.String r8, defpackage.q70<? super defpackage.te4> r9) throws java.lang.Exception {
            /*
                Method dump skipped, instructions count: 302
                To view this dump change 'Code comments level' option to 'DEBUG'
            */
            throw new UnsupportedOperationException("Method not decompiled: net.safemoon.androidwallet.repository.dataSource.collection.AttributeDataSource.Companion.b(net.safemoon.androidwallet.model.collectible.RoomNFT, java.lang.String, q70):java.lang.Object");
        }
    }

    public AttributeDataSource(Application application, long j) {
        fs1.f(application, "application");
        this.a = application;
        this.b = j;
    }

    public final void a() {
        as.b(qg1.a, null, null, new AttributeDataSource$fetchAttributes$1(this, null), 3, null);
    }

    public final Application b() {
        return this.a;
    }

    public final long c() {
        return this.b;
    }
}
