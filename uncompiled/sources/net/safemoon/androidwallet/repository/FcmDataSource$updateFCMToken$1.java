package net.safemoon.androidwallet.repository;

import kotlin.coroutines.jvm.internal.ContinuationImpl;
import kotlin.coroutines.jvm.internal.a;

/* compiled from: FcmDataSource.kt */
@a(c = "net.safemoon.androidwallet.repository.FcmDataSource", f = "FcmDataSource.kt", l = {28, 29, 39}, m = "updateFCMToken")
/* loaded from: classes2.dex */
public final class FcmDataSource$updateFCMToken$1 extends ContinuationImpl {
    public Object L$0;
    public Object L$1;
    public Object L$2;
    public Object L$3;
    public Object L$4;
    public boolean Z$0;
    public int label;
    public /* synthetic */ Object result;
    public final /* synthetic */ FcmDataSource this$0;

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public FcmDataSource$updateFCMToken$1(FcmDataSource fcmDataSource, q70<? super FcmDataSource$updateFCMToken$1> q70Var) {
        super(q70Var);
        this.this$0 = fcmDataSource;
    }

    @Override // kotlin.coroutines.jvm.internal.BaseContinuationImpl
    public final Object invokeSuspend(Object obj) {
        Object f;
        this.result = obj;
        this.label |= Integer.MIN_VALUE;
        f = this.this$0.f(null, null, false, null, this);
        return f;
    }
}
