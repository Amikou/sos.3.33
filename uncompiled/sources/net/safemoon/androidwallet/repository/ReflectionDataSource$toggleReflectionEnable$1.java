package net.safemoon.androidwallet.repository;

import kotlin.coroutines.jvm.internal.ContinuationImpl;
import kotlin.coroutines.jvm.internal.a;

/* compiled from: ReflectionDataSource.kt */
@a(c = "net.safemoon.androidwallet.repository.ReflectionDataSource", f = "ReflectionDataSource.kt", l = {119}, m = "toggleReflectionEnable")
/* loaded from: classes2.dex */
public final class ReflectionDataSource$toggleReflectionEnable$1 extends ContinuationImpl {
    public int label;
    public /* synthetic */ Object result;
    public final /* synthetic */ ReflectionDataSource this$0;

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public ReflectionDataSource$toggleReflectionEnable$1(ReflectionDataSource reflectionDataSource, q70<? super ReflectionDataSource$toggleReflectionEnable$1> q70Var) {
        super(q70Var);
        this.this$0 = reflectionDataSource;
    }

    @Override // kotlin.coroutines.jvm.internal.BaseContinuationImpl
    public final Object invokeSuspend(Object obj) {
        this.result = obj;
        this.label |= Integer.MIN_VALUE;
        return this.this$0.r(null, this);
    }
}
