package net.safemoon.androidwallet.repository;

import kotlin.coroutines.jvm.internal.SuspendLambda;
import kotlin.coroutines.jvm.internal.a;

/* compiled from: FcmDataSource.kt */
@a(c = "net.safemoon.androidwallet.repository.FcmDataSource$updateIfPendingToSaveToken$1$1", f = "FcmDataSource.kt", l = {77}, m = "invokeSuspend")
/* loaded from: classes2.dex */
public final class FcmDataSource$updateIfPendingToSaveToken$1$1 extends SuspendLambda implements hd1<c90, q70<? super te4>, Object> {
    public final /* synthetic */ String $it;
    public int label;
    public final /* synthetic */ FcmDataSource this$0;

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public FcmDataSource$updateIfPendingToSaveToken$1$1(FcmDataSource fcmDataSource, String str, q70<? super FcmDataSource$updateIfPendingToSaveToken$1$1> q70Var) {
        super(2, q70Var);
        this.this$0 = fcmDataSource;
        this.$it = str;
    }

    @Override // kotlin.coroutines.jvm.internal.BaseContinuationImpl
    public final q70<te4> create(Object obj, q70<?> q70Var) {
        return new FcmDataSource$updateIfPendingToSaveToken$1$1(this.this$0, this.$it, q70Var);
    }

    @Override // defpackage.hd1
    public final Object invoke(c90 c90Var, q70<? super te4> q70Var) {
        return ((FcmDataSource$updateIfPendingToSaveToken$1$1) create(c90Var, q70Var)).invokeSuspend(te4.a);
    }

    @Override // kotlin.coroutines.jvm.internal.BaseContinuationImpl
    public final Object invokeSuspend(Object obj) {
        Object j;
        Object d = gs1.d();
        int i = this.label;
        try {
            if (i == 0) {
                o83.b(obj);
                FcmDataSource fcmDataSource = this.this$0;
                String str = this.$it;
                this.label = 1;
                j = fcmDataSource.j(str, this);
                if (j == d) {
                    return d;
                }
            } else if (i != 1) {
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            } else {
                o83.b(obj);
            }
        } catch (Exception unused) {
        }
        return te4.a;
    }
}
