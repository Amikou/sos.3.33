package net.safemoon.androidwallet.repository;

import android.app.Application;
import java.util.List;
import kotlin.coroutines.jvm.internal.SuspendLambda;
import kotlin.coroutines.jvm.internal.a;
import kotlinx.coroutines.CoroutineDispatcher;
import net.safemoon.androidwallet.model.swap.Swap;

/* compiled from: TokenListDataSource.kt */
/* loaded from: classes2.dex */
public final class TokenListDataSource {
    public final Application a;
    public final sy1 b;
    public final sy1 c;
    public final sy1 d;
    public final sy1 e;
    public List<? extends Swap> f;

    /* compiled from: TokenListDataSource.kt */
    @a(c = "net.safemoon.androidwallet.repository.TokenListDataSource$1", f = "TokenListDataSource.kt", l = {36}, m = "invokeSuspend")
    /* renamed from: net.safemoon.androidwallet.repository.TokenListDataSource$1  reason: invalid class name */
    /* loaded from: classes2.dex */
    public static final class AnonymousClass1 extends SuspendLambda implements hd1<c90, q70<? super te4>, Object> {
        public int label;

        /* compiled from: TokenListDataSource.kt */
        @a(c = "net.safemoon.androidwallet.repository.TokenListDataSource$1$1", f = "TokenListDataSource.kt", l = {37}, m = "invokeSuspend")
        /* renamed from: net.safemoon.androidwallet.repository.TokenListDataSource$1$1  reason: invalid class name and collision with other inner class name */
        /* loaded from: classes2.dex */
        public static final class C02151 extends SuspendLambda implements hd1<c90, q70<? super te4>, Object> {
            public int label;
            public final /* synthetic */ TokenListDataSource this$0;

            /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
            public C02151(TokenListDataSource tokenListDataSource, q70<? super C02151> q70Var) {
                super(2, q70Var);
                this.this$0 = tokenListDataSource;
            }

            @Override // kotlin.coroutines.jvm.internal.BaseContinuationImpl
            public final q70<te4> create(Object obj, q70<?> q70Var) {
                return new C02151(this.this$0, q70Var);
            }

            @Override // defpackage.hd1
            public final Object invoke(c90 c90Var, q70<? super te4> q70Var) {
                return ((C02151) create(c90Var, q70Var)).invokeSuspend(te4.a);
            }

            @Override // kotlin.coroutines.jvm.internal.BaseContinuationImpl
            public final Object invokeSuspend(Object obj) {
                Object d = gs1.d();
                int i = this.label;
                if (i == 0) {
                    o83.b(obj);
                    TokenListDataSource tokenListDataSource = this.this$0;
                    this.label = 1;
                    if (tokenListDataSource.f(this) == d) {
                        return d;
                    }
                } else if (i != 1) {
                    throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
                } else {
                    o83.b(obj);
                }
                return te4.a;
            }
        }

        public AnonymousClass1(q70<? super AnonymousClass1> q70Var) {
            super(2, q70Var);
        }

        @Override // kotlin.coroutines.jvm.internal.BaseContinuationImpl
        public final q70<te4> create(Object obj, q70<?> q70Var) {
            return new AnonymousClass1(q70Var);
        }

        @Override // defpackage.hd1
        public final Object invoke(c90 c90Var, q70<? super te4> q70Var) {
            return ((AnonymousClass1) create(c90Var, q70Var)).invokeSuspend(te4.a);
        }

        @Override // kotlin.coroutines.jvm.internal.BaseContinuationImpl
        public final Object invokeSuspend(Object obj) {
            Object d = gs1.d();
            int i = this.label;
            if (i == 0) {
                o83.b(obj);
                CoroutineDispatcher b = tp0.b();
                C02151 c02151 = new C02151(TokenListDataSource.this, null);
                this.label = 1;
                if (kotlinx.coroutines.a.e(b, c02151, this) == d) {
                    return d;
                }
            } else if (i != 1) {
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            } else {
                o83.b(obj);
            }
            return te4.a;
        }
    }

    public TokenListDataSource(Application application) {
        fs1.f(application, "application");
        this.a = application;
        this.b = zy1.a(new TokenListDataSource$userTokenDAO$2(this));
        this.c = zy1.a(new TokenListDataSource$allTokenRepo$2(this));
        this.d = zy1.a(new TokenListDataSource$customTokenDataSource$2(this));
        this.e = zy1.a(TokenListDataSource$marketClient$2.INSTANCE);
        as.b(qg1.a, null, null, new AnonymousClass1(null), 3, null);
    }

    public final pl1 b() {
        return (pl1) this.c.getValue();
    }

    public final Application c() {
        return this.a;
    }

    public final yc0 d() {
        return (yc0) this.d.getValue();
    }

    public final e42 e() {
        return (e42) this.e.getValue();
    }

    /* JADX WARN: Code restructure failed: missing block: B:24:0x004b, code lost:
        r5 = e().m();
        r0.L$0 = r4;
        r0.L$1 = r4;
        r0.label = 1;
        r5 = retrofit2.KotlinExtensions.c(r5, r0);
     */
    /* JADX WARN: Code restructure failed: missing block: B:25:0x005d, code lost:
        if (r5 != r1) goto L36;
     */
    /* JADX WARN: Code restructure failed: missing block: B:26:0x005f, code lost:
        return r1;
     */
    /* JADX WARN: Code restructure failed: missing block: B:27:0x0060, code lost:
        r0 = r4;
        r1 = r0;
     */
    /* JADX WARN: Removed duplicated region for block: B:10:0x0023  */
    /* JADX WARN: Removed duplicated region for block: B:16:0x0039  */
    /* JADX WARN: Removed duplicated region for block: B:36:0x007b A[Catch: Exception -> 0x0086, TryCatch #0 {Exception -> 0x0086, blocks: (B:12:0x002d, B:28:0x0062, B:36:0x007b, B:37:0x007f, B:39:0x0083, B:31:0x006e, B:34:0x0075, B:17:0x003c, B:24:0x004b, B:20:0x0042), top: B:43:0x0021 }] */
    /*
        Code decompiled incorrectly, please refer to instructions dump.
        To view partially-correct code enable 'Show inconsistent code' option in preferences
    */
    public final java.lang.Object f(defpackage.q70<? super java.util.List<? extends net.safemoon.androidwallet.model.swap.Swap>> r5) {
        /*
            r4 = this;
            boolean r0 = r5 instanceof net.safemoon.androidwallet.repository.TokenListDataSource$getSwapTokens$1
            if (r0 == 0) goto L13
            r0 = r5
            net.safemoon.androidwallet.repository.TokenListDataSource$getSwapTokens$1 r0 = (net.safemoon.androidwallet.repository.TokenListDataSource$getSwapTokens$1) r0
            int r1 = r0.label
            r2 = -2147483648(0xffffffff80000000, float:-0.0)
            r3 = r1 & r2
            if (r3 == 0) goto L13
            int r1 = r1 - r2
            r0.label = r1
            goto L18
        L13:
            net.safemoon.androidwallet.repository.TokenListDataSource$getSwapTokens$1 r0 = new net.safemoon.androidwallet.repository.TokenListDataSource$getSwapTokens$1
            r0.<init>(r4, r5)
        L18:
            java.lang.Object r5 = r0.result
            java.lang.Object r1 = defpackage.gs1.d()
            int r2 = r0.label
            r3 = 1
            if (r2 == 0) goto L39
            if (r2 != r3) goto L31
            java.lang.Object r1 = r0.L$1
            net.safemoon.androidwallet.repository.TokenListDataSource r1 = (net.safemoon.androidwallet.repository.TokenListDataSource) r1
            java.lang.Object r0 = r0.L$0
            net.safemoon.androidwallet.repository.TokenListDataSource r0 = (net.safemoon.androidwallet.repository.TokenListDataSource) r0
            defpackage.o83.b(r5)     // Catch: java.lang.Exception -> L86
            goto L62
        L31:
            java.lang.IllegalStateException r5 = new java.lang.IllegalStateException
            java.lang.String r0 = "call to 'resume' before 'invoke' with coroutine"
            r5.<init>(r0)
            throw r5
        L39:
            defpackage.o83.b(r5)
            java.util.List<? extends net.safemoon.androidwallet.model.swap.Swap> r5 = r4.f     // Catch: java.lang.Exception -> L86
            r2 = 0
            if (r5 != 0) goto L42
            goto L49
        L42:
            boolean r5 = r5.isEmpty()     // Catch: java.lang.Exception -> L86
            if (r5 != 0) goto L49
            r2 = r3
        L49:
            if (r2 != 0) goto L82
            e42 r5 = r4.e()     // Catch: java.lang.Exception -> L86
            retrofit2.b r5 = r5.m()     // Catch: java.lang.Exception -> L86
            r0.L$0 = r4     // Catch: java.lang.Exception -> L86
            r0.L$1 = r4     // Catch: java.lang.Exception -> L86
            r0.label = r3     // Catch: java.lang.Exception -> L86
            java.lang.Object r5 = retrofit2.KotlinExtensions.c(r5, r0)     // Catch: java.lang.Exception -> L86
            if (r5 != r1) goto L60
            return r1
        L60:
            r0 = r4
            r1 = r0
        L62:
            retrofit2.n r5 = (retrofit2.n) r5     // Catch: java.lang.Exception -> L86
            java.lang.Object r5 = r5.a()     // Catch: java.lang.Exception -> L86
            net.safemoon.androidwallet.model.swap.AllSwapTokens r5 = (net.safemoon.androidwallet.model.swap.AllSwapTokens) r5     // Catch: java.lang.Exception -> L86
            r2 = 0
            if (r5 != 0) goto L6e
            goto L79
        L6e:
            net.safemoon.androidwallet.model.swap.Data r5 = r5.getData()     // Catch: java.lang.Exception -> L86
            if (r5 != 0) goto L75
            goto L79
        L75:
            java.util.List r2 = r5.getTokens()     // Catch: java.lang.Exception -> L86
        L79:
            if (r2 != 0) goto L7f
            java.util.List r2 = defpackage.b20.g()     // Catch: java.lang.Exception -> L86
        L7f:
            r1.f = r2     // Catch: java.lang.Exception -> L86
            goto L83
        L82:
            r0 = r4
        L83:
            java.util.List<? extends net.safemoon.androidwallet.model.swap.Swap> r5 = r0.f     // Catch: java.lang.Exception -> L86
            goto L8a
        L86:
            java.util.List r5 = defpackage.b20.g()
        L8a:
            return r5
        */
        throw new UnsupportedOperationException("Method not decompiled: net.safemoon.androidwallet.repository.TokenListDataSource.f(q70):java.lang.Object");
    }

    /* JADX WARN: Multi-variable type inference failed */
    /* JADX WARN: Removed duplicated region for block: B:106:0x02ef  */
    /* JADX WARN: Removed duplicated region for block: B:10:0x002d  */
    /* JADX WARN: Removed duplicated region for block: B:118:0x037d  */
    /* JADX WARN: Removed duplicated region for block: B:20:0x008a  */
    /* JADX WARN: Removed duplicated region for block: B:27:0x00aa  */
    /* JADX WARN: Removed duplicated region for block: B:57:0x0142  */
    /* JADX WARN: Removed duplicated region for block: B:58:0x0151  */
    /* JADX WARN: Removed duplicated region for block: B:61:0x0159  */
    /* JADX WARN: Removed duplicated region for block: B:71:0x01d6  */
    /* JADX WARN: Removed duplicated region for block: B:72:0x01e7  */
    /* JADX WARN: Removed duplicated region for block: B:75:0x01fa  */
    /* JADX WARN: Removed duplicated region for block: B:82:0x0225  */
    /* JADX WARN: Removed duplicated region for block: B:92:0x02a9  */
    /* JADX WARN: Unsupported multi-entry loop pattern (BACK_EDGE: B:78:0x0216 -> B:79:0x0219). Please submit an issue!!! */
    /*
        Code decompiled incorrectly, please refer to instructions dump.
        To view partially-correct code enable 'Show inconsistent code' option in preferences
    */
    public final java.lang.Object g(java.lang.Integer r40, defpackage.q70<? super java.util.List<net.safemoon.androidwallet.model.priceAlert.PAToken>> r41) {
        /*
            Method dump skipped, instructions count: 952
            To view this dump change 'Code comments level' option to 'DEBUG'
        */
        throw new UnsupportedOperationException("Method not decompiled: net.safemoon.androidwallet.repository.TokenListDataSource.g(java.lang.Integer, q70):java.lang.Object");
    }

    public final eg4 h() {
        return (eg4) this.b.getValue();
    }
}
