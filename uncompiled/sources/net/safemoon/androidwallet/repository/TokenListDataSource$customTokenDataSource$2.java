package net.safemoon.androidwallet.repository;

import kotlin.jvm.internal.Lambda;

/* compiled from: TokenListDataSource.kt */
/* loaded from: classes2.dex */
public final class TokenListDataSource$customTokenDataSource$2 extends Lambda implements rc1<yc0> {
    public final /* synthetic */ TokenListDataSource this$0;

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public TokenListDataSource$customTokenDataSource$2(TokenListDataSource tokenListDataSource) {
        super(0);
        this.this$0 = tokenListDataSource;
    }

    @Override // defpackage.rc1
    public final yc0 invoke() {
        return zc0.a.a(this.this$0.c());
    }
}
