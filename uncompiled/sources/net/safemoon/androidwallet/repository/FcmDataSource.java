package net.safemoon.androidwallet.repository;

import android.app.Application;
import com.google.android.gms.tasks.d;
import com.google.firebase.messaging.FirebaseMessaging;

/* compiled from: FcmDataSource.kt */
/* loaded from: classes2.dex */
public final class FcmDataSource {
    public final Application a;
    public final sy1 b;

    public FcmDataSource(Application application) {
        fs1.f(application, "application");
        this.a = application;
        this.b = zy1.a(FcmDataSource$safemoonAPIInterface$2.INSTANCE);
    }

    public static /* synthetic */ void h(FcmDataSource fcmDataSource, String str, String str2, boolean z, tc1 tc1Var, int i, Object obj) {
        if ((i & 1) != 0) {
            str = b30.g(b30.a, fcmDataSource.a, null, 2, null);
        }
        if ((i & 2) != 0) {
            str2 = u21.a.a().getSymbol();
        }
        if ((i & 4) != 0) {
            z = bo3.e(fcmDataSource.a, "FCM_TOKEN", true);
        }
        fcmDataSource.g(str, str2, z, tc1Var);
    }

    public final Application c() {
        return this.a;
    }

    public final Object d(q70<? super String> q70Var) {
        return d.a(FirebaseMessaging.g().i());
    }

    public final ac3 e() {
        return (ac3) this.b.getValue();
    }

    /* JADX WARN: Removed duplicated region for block: B:10:0x002a  */
    /* JADX WARN: Removed duplicated region for block: B:20:0x0075  */
    /* JADX WARN: Removed duplicated region for block: B:26:0x00c7 A[RETURN] */
    /* JADX WARN: Removed duplicated region for block: B:27:0x00c8  */
    /* JADX WARN: Removed duplicated region for block: B:31:0x00e3 A[LOOP:0: B:29:0x00dd->B:31:0x00e3, LOOP_END] */
    /* JADX WARN: Removed duplicated region for block: B:35:0x0108  */
    /* JADX WARN: Removed duplicated region for block: B:36:0x0109  */
    /* JADX WARN: Removed duplicated region for block: B:39:0x0138 A[RETURN] */
    /* JADX WARN: Removed duplicated region for block: B:40:0x0139  */
    /*
        Code decompiled incorrectly, please refer to instructions dump.
        To view partially-correct code enable 'Show inconsistent code' option in preferences
    */
    public final java.lang.Object f(java.lang.String r22, java.lang.String r23, boolean r24, defpackage.tc1<? super java.lang.Boolean, defpackage.te4> r25, defpackage.q70<? super defpackage.te4> r26) {
        /*
            Method dump skipped, instructions count: 339
            To view this dump change 'Code comments level' option to 'DEBUG'
        */
        throw new UnsupportedOperationException("Method not decompiled: net.safemoon.androidwallet.repository.FcmDataSource.f(java.lang.String, java.lang.String, boolean, tc1, q70):java.lang.Object");
    }

    public final void g(String str, String str2, boolean z, tc1<? super Boolean, te4> tc1Var) {
        fs1.f(str, "lang");
        fs1.f(str2, "currency");
        fs1.f(tc1Var, "callBack");
        as.b(qg1.a, tp0.b(), null, new FcmDataSource$updateFCMTokenConfiguration$1(this, str, str2, z, tc1Var, null), 2, null);
    }

    public final void i() {
        String j = bo3.j(this.a, "TEMP_FCM_TOKEN", null);
        if (j == null) {
            return;
        }
        as.b(qg1.a, tp0.b(), null, new FcmDataSource$updateIfPendingToSaveToken$1$1(this, j, null), 2, null);
    }

    /* JADX WARN: Can't wrap try/catch for region: R(11:1|(2:3|(9:5|6|7|(1:(1:(6:11|12|13|(1:15)|16|17)(2:19|20))(3:21|22|23))(3:36|37|(1:39)(1:40))|24|(2:27|25)|28|29|(6:31|(1:33)|13|(0)|16|17)(2:34|35)))|42|6|7|(0)(0)|24|(1:25)|28|29|(0)(0)) */
    /* JADX WARN: Removed duplicated region for block: B:10:0x0024  */
    /* JADX WARN: Removed duplicated region for block: B:20:0x004d  */
    /* JADX WARN: Removed duplicated region for block: B:28:0x00a5 A[Catch: Exception -> 0x00ff, LOOP:0: B:26:0x009f->B:28:0x00a5, LOOP_END, TryCatch #0 {Exception -> 0x00ff, blocks: (B:13:0x002c, B:34:0x00e5, B:36:0x00ed, B:18:0x0045, B:25:0x008e, B:26:0x009f, B:28:0x00a5, B:29:0x00b3, B:31:0x00bc, B:37:0x00f7, B:38:0x00fe, B:21:0x0050), top: B:42:0x0022 }] */
    /* JADX WARN: Removed duplicated region for block: B:31:0x00bc A[Catch: Exception -> 0x00ff, TryCatch #0 {Exception -> 0x00ff, blocks: (B:13:0x002c, B:34:0x00e5, B:36:0x00ed, B:18:0x0045, B:25:0x008e, B:26:0x009f, B:28:0x00a5, B:29:0x00b3, B:31:0x00bc, B:37:0x00f7, B:38:0x00fe, B:21:0x0050), top: B:42:0x0022 }] */
    /* JADX WARN: Removed duplicated region for block: B:36:0x00ed A[Catch: Exception -> 0x00ff, TryCatch #0 {Exception -> 0x00ff, blocks: (B:13:0x002c, B:34:0x00e5, B:36:0x00ed, B:18:0x0045, B:25:0x008e, B:26:0x009f, B:28:0x00a5, B:29:0x00b3, B:31:0x00bc, B:37:0x00f7, B:38:0x00fe, B:21:0x0050), top: B:42:0x0022 }] */
    /* JADX WARN: Removed duplicated region for block: B:37:0x00f7 A[Catch: Exception -> 0x00ff, TryCatch #0 {Exception -> 0x00ff, blocks: (B:13:0x002c, B:34:0x00e5, B:36:0x00ed, B:18:0x0045, B:25:0x008e, B:26:0x009f, B:28:0x00a5, B:29:0x00b3, B:31:0x00bc, B:37:0x00f7, B:38:0x00fe, B:21:0x0050), top: B:42:0x0022 }] */
    /*
        Code decompiled incorrectly, please refer to instructions dump.
        To view partially-correct code enable 'Show inconsistent code' option in preferences
    */
    public final java.lang.Object j(java.lang.String r10, defpackage.q70<? super defpackage.te4> r11) {
        /*
            Method dump skipped, instructions count: 258
            To view this dump change 'Code comments level' option to 'DEBUG'
        */
        throw new UnsupportedOperationException("Method not decompiled: net.safemoon.androidwallet.repository.FcmDataSource.j(java.lang.String, q70):java.lang.Object");
    }
}
