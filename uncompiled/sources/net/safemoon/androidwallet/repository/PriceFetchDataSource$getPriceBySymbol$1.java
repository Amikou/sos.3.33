package net.safemoon.androidwallet.repository;

import kotlin.coroutines.jvm.internal.ContinuationImpl;
import kotlin.coroutines.jvm.internal.a;

/* compiled from: PriceFetchDataSource.kt */
@a(c = "net.safemoon.androidwallet.repository.PriceFetchDataSource", f = "PriceFetchDataSource.kt", l = {71, 88}, m = "getPriceBySymbol")
/* loaded from: classes2.dex */
public final class PriceFetchDataSource$getPriceBySymbol$1 extends ContinuationImpl {
    public Object L$0;
    public Object L$1;
    public int label;
    public /* synthetic */ Object result;
    public final /* synthetic */ PriceFetchDataSource this$0;

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public PriceFetchDataSource$getPriceBySymbol$1(PriceFetchDataSource priceFetchDataSource, q70<? super PriceFetchDataSource$getPriceBySymbol$1> q70Var) {
        super(q70Var);
        this.this$0 = priceFetchDataSource;
    }

    @Override // kotlin.coroutines.jvm.internal.BaseContinuationImpl
    public final Object invokeSuspend(Object obj) {
        this.result = obj;
        this.label |= Integer.MIN_VALUE;
        return this.this$0.f(null, this);
    }
}
