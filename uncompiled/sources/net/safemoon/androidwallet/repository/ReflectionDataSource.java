package net.safemoon.androidwallet.repository;

import androidx.lifecycle.LiveData;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import kotlin.NoWhenBranchMatchedException;
import net.safemoon.androidwallet.common.TokenType;
import net.safemoon.androidwallet.model.reflections.RoomReflectionsData;
import net.safemoon.androidwallet.model.reflections.RoomReflectionsDataAndToken;
import net.safemoon.androidwallet.model.reflections.RoomReflectionsToken;
import net.safemoon.androidwallet.model.reflections.RoomReflectionsTokenAndData;
import net.safemoon.androidwallet.model.token.abstraction.IToken;
import net.safemoon.androidwallet.viewmodels.ReflectionTrackerViewModel;

/* compiled from: ReflectionDataSource.kt */
/* loaded from: classes2.dex */
public final class ReflectionDataSource {
    public final t53 a;

    /* compiled from: ReflectionDataSource.kt */
    /* loaded from: classes2.dex */
    public /* synthetic */ class a {
        public static final /* synthetic */ int[] a;

        static {
            int[] iArr = new int[ReflectionTrackerViewModel.TimeSpan.values().length];
            iArr[ReflectionTrackerViewModel.TimeSpan.LOW.ordinal()] = 1;
            iArr[ReflectionTrackerViewModel.TimeSpan.MEDIUM.ordinal()] = 2;
            iArr[ReflectionTrackerViewModel.TimeSpan.HIGH.ordinal()] = 3;
            a = iArr;
        }
    }

    public ReflectionDataSource(t53 t53Var) {
        fs1.f(t53Var, "reflectionDao");
        this.a = t53Var;
    }

    public final Object a(RoomReflectionsToken roomReflectionsToken, q70<? super te4> q70Var) {
        Object l = this.a.l(roomReflectionsToken, q70Var);
        return l == gs1.d() ? l : te4.a;
    }

    public final LiveData<RoomReflectionsTokenAndData> b(String str) {
        fs1.f(str, "symbolWithType");
        return this.a.o(str);
    }

    public final Object c(String str, q70<? super RoomReflectionsToken> q70Var) {
        return this.a.i(str, q70Var);
    }

    public final LiveData<List<RoomReflectionsToken>> d(TokenType tokenType) {
        fs1.f(tokenType, "param");
        return this.a.f(tokenType.getChainId());
    }

    public final LiveData<List<RoomReflectionsTokenAndData>> e(TokenType tokenType) {
        if (tokenType != null) {
            return this.a.c(tokenType.getChainId());
        }
        return new gb2();
    }

    public final LiveData<List<RoomReflectionsDataAndToken>> f(ReflectionTrackerViewModel.a aVar) {
        long j;
        fs1.f(aVar, "reflectionParam");
        if (aVar.b() == ReflectionTrackerViewModel.TimeMode.DAILY) {
            int i = a.a[aVar.c().ordinal()];
            if (i == 1) {
                j = h(6, -1);
            } else if (i == 2) {
                j = h(6, -7);
            } else if (i != 3) {
                throw new NoWhenBranchMatchedException();
            } else {
                j = h(6, -30);
            }
        } else {
            j = 0;
        }
        if (aVar.b() == ReflectionTrackerViewModel.TimeMode.WEEKLY || aVar.b() == ReflectionTrackerViewModel.TimeMode.MONTHLY) {
            int i2 = a.a[aVar.c().ordinal()];
            if (i2 == 1) {
                j = h(2, -3);
            } else if (i2 == 2) {
                j = h(2, -6);
            } else if (i2 != 3) {
                throw new NoWhenBranchMatchedException();
            } else {
                j = h(2, -12);
            }
        }
        if (j > 0) {
            return this.a.d(aVar.a(), j);
        }
        return this.a.n(aVar.a());
    }

    public final Object g(q70<? super List<RoomReflectionsToken>> q70Var) {
        return this.a.m(q70Var);
    }

    public final long h(int i, int i2) {
        Calendar calendar = Calendar.getInstance();
        calendar.add(i, i2);
        b30 b30Var = b30.a;
        Date time = calendar.getTime();
        fs1.e(time, "timeNow.time");
        return b30Var.z(time);
    }

    public final Object i(String str, q70<? super RoomReflectionsData> q70Var) {
        return this.a.h(str, q70Var);
    }

    public final Object j(String str, q70<? super te4> q70Var) {
        Object r = this.a.r(str, q70Var);
        return r == gs1.d() ? r : te4.a;
    }

    public final Object k(q70<? super te4> q70Var) {
        Object k = this.a.k(q70Var);
        return k == gs1.d() ? k : te4.a;
    }

    /* JADX WARN: Removed duplicated region for block: B:10:0x0024  */
    /* JADX WARN: Removed duplicated region for block: B:16:0x0040  */
    /* JADX WARN: Removed duplicated region for block: B:22:0x0063  */
    /* JADX WARN: Removed duplicated region for block: B:27:0x0076  */
    /*
        Code decompiled incorrectly, please refer to instructions dump.
        To view partially-correct code enable 'Show inconsistent code' option in preferences
    */
    public final java.lang.Object l(net.safemoon.androidwallet.model.reflections.RoomReflectionsData r8, defpackage.q70<? super defpackage.te4> r9) {
        /*
            r7 = this;
            boolean r0 = r9 instanceof net.safemoon.androidwallet.repository.ReflectionDataSource$saveData$1
            if (r0 == 0) goto L13
            r0 = r9
            net.safemoon.androidwallet.repository.ReflectionDataSource$saveData$1 r0 = (net.safemoon.androidwallet.repository.ReflectionDataSource$saveData$1) r0
            int r1 = r0.label
            r2 = -2147483648(0xffffffff80000000, float:-0.0)
            r3 = r1 & r2
            if (r3 == 0) goto L13
            int r1 = r1 - r2
            r0.label = r1
            goto L18
        L13:
            net.safemoon.androidwallet.repository.ReflectionDataSource$saveData$1 r0 = new net.safemoon.androidwallet.repository.ReflectionDataSource$saveData$1
            r0.<init>(r7, r9)
        L18:
            java.lang.Object r9 = r0.result
            java.lang.Object r1 = defpackage.gs1.d()
            int r2 = r0.label
            r3 = 2
            r4 = 1
            if (r2 == 0) goto L40
            if (r2 == r4) goto L34
            if (r2 != r3) goto L2c
            defpackage.o83.b(r9)
            goto L73
        L2c:
            java.lang.IllegalStateException r8 = new java.lang.IllegalStateException
            java.lang.String r9 = "call to 'resume' before 'invoke' with coroutine"
            r8.<init>(r9)
            throw r8
        L34:
            java.lang.Object r8 = r0.L$1
            net.safemoon.androidwallet.model.reflections.RoomReflectionsData r8 = (net.safemoon.androidwallet.model.reflections.RoomReflectionsData) r8
            java.lang.Object r2 = r0.L$0
            net.safemoon.androidwallet.repository.ReflectionDataSource r2 = (net.safemoon.androidwallet.repository.ReflectionDataSource) r2
            defpackage.o83.b(r9)
            goto L5b
        L40:
            defpackage.o83.b(r9)
            t53 r9 = r7.a
            java.lang.String r2 = r8.getSymbolWithType()
            long r5 = r8.getTimeStamp()
            r0.L$0 = r7
            r0.L$1 = r8
            r0.label = r4
            java.lang.Object r9 = r9.j(r2, r5, r0)
            if (r9 != r1) goto L5a
            return r1
        L5a:
            r2 = r7
        L5b:
            java.lang.Boolean r9 = (java.lang.Boolean) r9
            boolean r9 = r9.booleanValue()
            if (r9 != 0) goto L76
            t53 r9 = r2.a
            r2 = 0
            r0.L$0 = r2
            r0.L$1 = r2
            r0.label = r3
            java.lang.Object r8 = r9.e(r8, r0)
            if (r8 != r1) goto L73
            return r1
        L73:
            te4 r8 = defpackage.te4.a
            return r8
        L76:
            te4 r8 = defpackage.te4.a
            return r8
        */
        throw new UnsupportedOperationException("Method not decompiled: net.safemoon.androidwallet.repository.ReflectionDataSource.l(net.safemoon.androidwallet.model.reflections.RoomReflectionsData, q70):java.lang.Object");
    }

    /* JADX WARN: Removed duplicated region for block: B:10:0x0024  */
    /* JADX WARN: Removed duplicated region for block: B:16:0x0040  */
    /* JADX WARN: Removed duplicated region for block: B:22:0x0063  */
    /* JADX WARN: Removed duplicated region for block: B:26:0x0074  */
    /*
        Code decompiled incorrectly, please refer to instructions dump.
        To view partially-correct code enable 'Show inconsistent code' option in preferences
    */
    public final java.lang.Object m(net.safemoon.androidwallet.model.reflections.RoomReflectionsToken r7, defpackage.q70<? super java.lang.Long> r8) {
        /*
            r6 = this;
            boolean r0 = r8 instanceof net.safemoon.androidwallet.repository.ReflectionDataSource$saveToken$2
            if (r0 == 0) goto L13
            r0 = r8
            net.safemoon.androidwallet.repository.ReflectionDataSource$saveToken$2 r0 = (net.safemoon.androidwallet.repository.ReflectionDataSource$saveToken$2) r0
            int r1 = r0.label
            r2 = -2147483648(0xffffffff80000000, float:-0.0)
            r3 = r1 & r2
            if (r3 == 0) goto L13
            int r1 = r1 - r2
            r0.label = r1
            goto L18
        L13:
            net.safemoon.androidwallet.repository.ReflectionDataSource$saveToken$2 r0 = new net.safemoon.androidwallet.repository.ReflectionDataSource$saveToken$2
            r0.<init>(r6, r8)
        L18:
            java.lang.Object r8 = r0.result
            java.lang.Object r1 = defpackage.gs1.d()
            int r2 = r0.label
            r3 = 2
            r4 = 1
            if (r2 == 0) goto L40
            if (r2 == r4) goto L34
            if (r2 != r3) goto L2c
            defpackage.o83.b(r8)
            goto L73
        L2c:
            java.lang.IllegalStateException r7 = new java.lang.IllegalStateException
            java.lang.String r8 = "call to 'resume' before 'invoke' with coroutine"
            r7.<init>(r8)
            throw r7
        L34:
            java.lang.Object r7 = r0.L$1
            net.safemoon.androidwallet.model.reflections.RoomReflectionsToken r7 = (net.safemoon.androidwallet.model.reflections.RoomReflectionsToken) r7
            java.lang.Object r2 = r0.L$0
            net.safemoon.androidwallet.repository.ReflectionDataSource r2 = (net.safemoon.androidwallet.repository.ReflectionDataSource) r2
            defpackage.o83.b(r8)
            goto L5b
        L40:
            defpackage.o83.b(r8)
            t53 r8 = r6.a
            java.lang.String r2 = r7.getSymbolWithType()
            int r5 = r7.getChainId()
            r0.L$0 = r6
            r0.L$1 = r7
            r0.label = r4
            java.lang.Object r8 = r8.s(r2, r5, r0)
            if (r8 != r1) goto L5a
            return r1
        L5a:
            r2 = r6
        L5b:
            java.lang.Boolean r8 = (java.lang.Boolean) r8
            boolean r8 = r8.booleanValue()
            if (r8 != 0) goto L74
            t53 r8 = r2.a
            r2 = 0
            r0.L$0 = r2
            r0.L$1 = r2
            r0.label = r3
            java.lang.Object r8 = r8.b(r7, r0)
            if (r8 != r1) goto L73
            return r1
        L73:
            return r8
        L74:
            r7 = 0
            java.lang.Long r7 = defpackage.hr.e(r7)
            return r7
        */
        throw new UnsupportedOperationException("Method not decompiled: net.safemoon.androidwallet.repository.ReflectionDataSource.m(net.safemoon.androidwallet.model.reflections.RoomReflectionsToken, q70):java.lang.Object");
    }

    public final Object n(IToken iToken, String str, long j, long j2, long j3, q70<? super Long> q70Var) {
        String symbolWithType = iToken.getSymbolWithType();
        String name = iToken.getName();
        String contractAddress = iToken.getContractAddress();
        return m(new RoomReflectionsToken(null, symbolWithType, iToken.getSymbol(), name, iToken.getIconResName(), contractAddress, iToken.getChainId(), iToken.getDecimals(), str, hr.e(j), false, hr.e(j2), hr.e(j3), null, null, 25601, null), q70Var);
    }

    public final Object o(int i, String str, q70<? super te4> q70Var) {
        Object p = this.a.p(i, str, q70Var);
        return p == gs1.d() ? p : te4.a;
    }

    public final Object p(double d, String str, q70<? super te4> q70Var) {
        Object a2 = this.a.a(d, str, q70Var);
        return a2 == gs1.d() ? a2 : te4.a;
    }

    public final Object q(long j, long j2, String str, q70<? super te4> q70Var) {
        Object g = this.a.g(j, j2, str, q70Var);
        return g == gs1.d() ? g : te4.a;
    }

    /* JADX WARN: Removed duplicated region for block: B:10:0x0023  */
    /* JADX WARN: Removed duplicated region for block: B:14:0x0031  */
    /*
        Code decompiled incorrectly, please refer to instructions dump.
        To view partially-correct code enable 'Show inconsistent code' option in preferences
    */
    public final java.lang.Object r(net.safemoon.androidwallet.model.reflections.RoomReflectionsToken r7, defpackage.q70<? super defpackage.te4> r8) {
        /*
            r6 = this;
            boolean r0 = r8 instanceof net.safemoon.androidwallet.repository.ReflectionDataSource$toggleReflectionEnable$1
            if (r0 == 0) goto L13
            r0 = r8
            net.safemoon.androidwallet.repository.ReflectionDataSource$toggleReflectionEnable$1 r0 = (net.safemoon.androidwallet.repository.ReflectionDataSource$toggleReflectionEnable$1) r0
            int r1 = r0.label
            r2 = -2147483648(0xffffffff80000000, float:-0.0)
            r3 = r1 & r2
            if (r3 == 0) goto L13
            int r1 = r1 - r2
            r0.label = r1
            goto L18
        L13:
            net.safemoon.androidwallet.repository.ReflectionDataSource$toggleReflectionEnable$1 r0 = new net.safemoon.androidwallet.repository.ReflectionDataSource$toggleReflectionEnable$1
            r0.<init>(r6, r8)
        L18:
            java.lang.Object r8 = r0.result
            java.lang.Object r1 = defpackage.gs1.d()
            int r2 = r0.label
            r3 = 1
            if (r2 == 0) goto L31
            if (r2 != r3) goto L29
            defpackage.o83.b(r8)
            goto L4f
        L29:
            java.lang.IllegalStateException r7 = new java.lang.IllegalStateException
            java.lang.String r8 = "call to 'resume' before 'invoke' with coroutine"
            r7.<init>(r8)
            throw r7
        L31:
            defpackage.o83.b(r8)
            java.lang.Long r8 = r7.getId()
            if (r8 != 0) goto L3b
            goto L4f
        L3b:
            long r4 = r8.longValue()
            t53 r8 = r6.a
            boolean r7 = r7.getEnableAdvanceMode()
            r7 = r7 ^ r3
            r0.label = r3
            java.lang.Object r7 = r8.q(r4, r7, r0)
            if (r7 != r1) goto L4f
            return r1
        L4f:
            te4 r7 = defpackage.te4.a
            return r7
        */
        throw new UnsupportedOperationException("Method not decompiled: net.safemoon.androidwallet.repository.ReflectionDataSource.r(net.safemoon.androidwallet.model.reflections.RoomReflectionsToken, q70):java.lang.Object");
    }
}
