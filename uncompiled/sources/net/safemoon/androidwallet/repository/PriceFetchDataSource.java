package net.safemoon.androidwallet.repository;

import android.app.Application;

/* compiled from: PriceFetchDataSource.kt */
/* loaded from: classes2.dex */
public final class PriceFetchDataSource {
    public final sy1 a;
    public final sy1 b;

    public PriceFetchDataSource(Application application) {
        fs1.f(application, "application");
        this.a = zy1.a(PriceFetchDataSource$marketInterface$2.INSTANCE);
        this.b = zy1.a(PriceFetchDataSource$cmcInterface$2.INSTANCE);
    }

    /* JADX WARN: Removed duplicated region for block: B:10:0x0028  */
    /* JADX WARN: Removed duplicated region for block: B:16:0x004a  */
    /* JADX WARN: Removed duplicated region for block: B:25:0x0083  */
    /* JADX WARN: Removed duplicated region for block: B:31:? A[RETURN, SYNTHETIC] */
    /*
        Code decompiled incorrectly, please refer to instructions dump.
        To view partially-correct code enable 'Show inconsistent code' option in preferences
    */
    public final java.lang.Object a(int r9, java.lang.String r10, java.lang.Integer r11, defpackage.q70<? super net.safemoon.androidwallet.model.Coin> r12) {
        /*
            r8 = this;
            boolean r9 = r12 instanceof net.safemoon.androidwallet.repository.PriceFetchDataSource$fetchPrice$1
            if (r9 == 0) goto L13
            r9 = r12
            net.safemoon.androidwallet.repository.PriceFetchDataSource$fetchPrice$1 r9 = (net.safemoon.androidwallet.repository.PriceFetchDataSource$fetchPrice$1) r9
            int r0 = r9.label
            r1 = -2147483648(0xffffffff80000000, float:-0.0)
            r2 = r0 & r1
            if (r2 == 0) goto L13
            int r0 = r0 - r1
            r9.label = r0
            goto L18
        L13:
            net.safemoon.androidwallet.repository.PriceFetchDataSource$fetchPrice$1 r9 = new net.safemoon.androidwallet.repository.PriceFetchDataSource$fetchPrice$1
            r9.<init>(r8, r12)
        L18:
            java.lang.Object r12 = r9.result
            java.lang.Object r0 = defpackage.gs1.d()
            int r1 = r9.label
            java.lang.String r2 = "null cannot be cast to non-null type kotlin.collections.Map<K, V>"
            r3 = 0
            r4 = 2
            r5 = 0
            r6 = 1
            if (r1 == 0) goto L4a
            if (r1 == r6) goto L3c
            if (r1 != r4) goto L34
            java.lang.Object r9 = r9.L$0
            java.lang.String r9 = (java.lang.String) r9
            defpackage.o83.b(r12)
            goto L95
        L34:
            java.lang.IllegalStateException r9 = new java.lang.IllegalStateException
            java.lang.String r10 = "call to 'resume' before 'invoke' with coroutine"
            r9.<init>(r10)
            throw r9
        L3c:
            int r10 = r9.I$0
            java.lang.Object r11 = r9.L$1
            java.lang.String r11 = (java.lang.String) r11
            java.lang.Object r1 = r9.L$0
            net.safemoon.androidwallet.repository.PriceFetchDataSource r1 = (net.safemoon.androidwallet.repository.PriceFetchDataSource) r1
            defpackage.o83.b(r12)
            goto L72
        L4a:
            defpackage.o83.b(r12)
            if (r11 != 0) goto L53
            r1 = r8
            r11 = r10
            r10 = r5
            goto L81
        L53:
            int r11 = r11.intValue()
            java.lang.String[] r12 = new java.lang.String[r6]
            java.lang.String r1 = java.lang.String.valueOf(r11)
            r12[r3] = r1
            r9.L$0 = r8
            r9.L$1 = r10
            r9.I$0 = r11
            r9.label = r6
            java.lang.Object r12 = r8.e(r12, r9)
            if (r12 != r0) goto L6e
            return r0
        L6e:
            r1 = r8
            r7 = r11
            r11 = r10
            r10 = r7
        L72:
            java.util.Map r12 = (java.util.Map) r12
            java.lang.String r10 = java.lang.String.valueOf(r10)
            java.util.Objects.requireNonNull(r12, r2)
            java.lang.Object r10 = r12.getOrDefault(r10, r5)
            net.safemoon.androidwallet.model.Coin r10 = (net.safemoon.androidwallet.model.Coin) r10
        L81:
            if (r10 != 0) goto La1
            java.lang.String[] r10 = new java.lang.String[r6]
            r10[r3] = r11
            r9.L$0 = r11
            r9.L$1 = r5
            r9.label = r4
            java.lang.Object r12 = r1.f(r10, r9)
            if (r12 != r0) goto L94
            return r0
        L94:
            r9 = r11
        L95:
            java.util.Map r12 = (java.util.Map) r12
            java.util.Objects.requireNonNull(r12, r2)
            java.lang.Object r9 = r12.getOrDefault(r9, r5)
            r10 = r9
            net.safemoon.androidwallet.model.Coin r10 = (net.safemoon.androidwallet.model.Coin) r10
        La1:
            return r10
        */
        throw new UnsupportedOperationException("Method not decompiled: net.safemoon.androidwallet.repository.PriceFetchDataSource.a(int, java.lang.String, java.lang.Integer, q70):java.lang.Object");
    }

    /* JADX WARN: Removed duplicated region for block: B:10:0x0024  */
    /* JADX WARN: Removed duplicated region for block: B:15:0x0032  */
    /*
        Code decompiled incorrectly, please refer to instructions dump.
        To view partially-correct code enable 'Show inconsistent code' option in preferences
    */
    public final java.lang.Object b(java.lang.String r6, defpackage.q70<? super net.safemoon.androidwallet.model.tokensInfo.CurrencyTokenInfo> r7) {
        /*
            r5 = this;
            boolean r0 = r7 instanceof net.safemoon.androidwallet.repository.PriceFetchDataSource$fetchPriceFromDexScreener$1
            if (r0 == 0) goto L13
            r0 = r7
            net.safemoon.androidwallet.repository.PriceFetchDataSource$fetchPriceFromDexScreener$1 r0 = (net.safemoon.androidwallet.repository.PriceFetchDataSource$fetchPriceFromDexScreener$1) r0
            int r1 = r0.label
            r2 = -2147483648(0xffffffff80000000, float:-0.0)
            r3 = r1 & r2
            if (r3 == 0) goto L13
            int r1 = r1 - r2
            r0.label = r1
            goto L18
        L13:
            net.safemoon.androidwallet.repository.PriceFetchDataSource$fetchPriceFromDexScreener$1 r0 = new net.safemoon.androidwallet.repository.PriceFetchDataSource$fetchPriceFromDexScreener$1
            r0.<init>(r5, r7)
        L18:
            java.lang.Object r7 = r0.result
            java.lang.Object r1 = defpackage.gs1.d()
            int r2 = r0.label
            r3 = 1
            r4 = 0
            if (r2 == 0) goto L32
            if (r2 != r3) goto L2a
            defpackage.o83.b(r7)     // Catch: java.lang.Exception -> L68
            goto L4f
        L2a:
            java.lang.IllegalStateException r6 = new java.lang.IllegalStateException
            java.lang.String r7 = "call to 'resume' before 'invoke' with coroutine"
            r6.<init>(r7)
            throw r6
        L32:
            defpackage.o83.b(r7)
            e42 r7 = defpackage.a4.k()     // Catch: java.lang.Exception -> L68
            net.safemoon.androidwallet.model.tokensInfo.Request r2 = new net.safemoon.androidwallet.model.tokensInfo.Request     // Catch: java.lang.Exception -> L68
            java.util.List r6 = defpackage.a20.b(r6)     // Catch: java.lang.Exception -> L68
            r2.<init>(r6)     // Catch: java.lang.Exception -> L68
            retrofit2.b r6 = r7.k(r2)     // Catch: java.lang.Exception -> L68
            r0.label = r3     // Catch: java.lang.Exception -> L68
            java.lang.Object r7 = retrofit2.KotlinExtensions.c(r6, r0)     // Catch: java.lang.Exception -> L68
            if (r7 != r1) goto L4f
            return r1
        L4f:
            retrofit2.n r7 = (retrofit2.n) r7     // Catch: java.lang.Exception -> L68
            java.lang.Object r6 = r7.a()     // Catch: java.lang.Exception -> L68
            net.safemoon.androidwallet.model.tokensInfo.CurrencyTokensInfoResult r6 = (net.safemoon.androidwallet.model.tokensInfo.CurrencyTokensInfoResult) r6     // Catch: java.lang.Exception -> L68
            if (r6 != 0) goto L5a
            goto L68
        L5a:
            java.util.List r6 = r6.getData()     // Catch: java.lang.Exception -> L68
            if (r6 != 0) goto L61
            goto L68
        L61:
            java.lang.Object r6 = defpackage.j20.M(r6)     // Catch: java.lang.Exception -> L68
            net.safemoon.androidwallet.model.tokensInfo.CurrencyTokenInfo r6 = (net.safemoon.androidwallet.model.tokensInfo.CurrencyTokenInfo) r6     // Catch: java.lang.Exception -> L68
            r4 = r6
        L68:
            return r4
        */
        throw new UnsupportedOperationException("Method not decompiled: net.safemoon.androidwallet.repository.PriceFetchDataSource.b(java.lang.String, q70):java.lang.Object");
    }

    public final jt c() {
        return (jt) this.b.getValue();
    }

    public final e42 d() {
        return (e42) this.a.getValue();
    }

    /* JADX WARN: Removed duplicated region for block: B:10:0x002a  */
    /* JADX WARN: Removed duplicated region for block: B:20:0x004f  */
    /* JADX WARN: Removed duplicated region for block: B:35:0x00af A[LOOP:2: B:33:0x00a9->B:35:0x00af, LOOP_END] */
    /* JADX WARN: Removed duplicated region for block: B:39:0x00df A[LOOP:3: B:37:0x00d9->B:39:0x00df, LOOP_END] */
    /* JADX WARN: Removed duplicated region for block: B:52:0x014f  */
    /* JADX WARN: Removed duplicated region for block: B:59:0x0173  */
    /* JADX WARN: Removed duplicated region for block: B:74:0x00f8 A[EXC_TOP_SPLITTER, SYNTHETIC] */
    /*
        Code decompiled incorrectly, please refer to instructions dump.
        To view partially-correct code enable 'Show inconsistent code' option in preferences
    */
    public final java.lang.Object e(java.lang.String[] r20, defpackage.q70<? super java.util.Map<java.lang.String, ? extends net.safemoon.androidwallet.model.Coin>> r21) {
        /*
            Method dump skipped, instructions count: 408
            To view this dump change 'Code comments level' option to 'DEBUG'
        */
        throw new UnsupportedOperationException("Method not decompiled: net.safemoon.androidwallet.repository.PriceFetchDataSource.e(java.lang.String[], q70):java.lang.Object");
    }

    /* JADX WARN: Removed duplicated region for block: B:10:0x002a  */
    /* JADX WARN: Removed duplicated region for block: B:20:0x004f  */
    /* JADX WARN: Removed duplicated region for block: B:35:0x00b7 A[LOOP:2: B:33:0x00b1->B:35:0x00b7, LOOP_END] */
    /* JADX WARN: Removed duplicated region for block: B:48:0x0127  */
    /* JADX WARN: Removed duplicated region for block: B:55:0x014b  */
    /* JADX WARN: Removed duplicated region for block: B:66:0x00d0 A[EXC_TOP_SPLITTER, SYNTHETIC] */
    /*
        Code decompiled incorrectly, please refer to instructions dump.
        To view partially-correct code enable 'Show inconsistent code' option in preferences
    */
    public final java.lang.Object f(java.lang.String[] r20, defpackage.q70<? super java.util.Map<java.lang.String, ? extends net.safemoon.androidwallet.model.Coin>> r21) {
        /*
            Method dump skipped, instructions count: 368
            To view this dump change 'Code comments level' option to 'DEBUG'
        */
        throw new UnsupportedOperationException("Method not decompiled: net.safemoon.androidwallet.repository.PriceFetchDataSource.f(java.lang.String[], q70):java.lang.Object");
    }
}
