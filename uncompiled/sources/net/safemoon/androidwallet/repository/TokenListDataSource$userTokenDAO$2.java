package net.safemoon.androidwallet.repository;

import kotlin.jvm.internal.Lambda;
import net.safemoon.androidwallet.database.room.ApplicationRoomDatabase;

/* compiled from: TokenListDataSource.kt */
/* loaded from: classes2.dex */
public final class TokenListDataSource$userTokenDAO$2 extends Lambda implements rc1<eg4> {
    public final /* synthetic */ TokenListDataSource this$0;

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public TokenListDataSource$userTokenDAO$2(TokenListDataSource tokenListDataSource) {
        super(0);
        this.this$0 = tokenListDataSource;
    }

    /* JADX WARN: Can't rename method to resolve collision */
    @Override // defpackage.rc1
    public final eg4 invoke() {
        return ApplicationRoomDatabase.k.c(ApplicationRoomDatabase.n, this.this$0.c(), null, 2, null).Z();
    }
}
