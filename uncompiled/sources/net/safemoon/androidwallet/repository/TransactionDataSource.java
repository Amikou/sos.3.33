package net.safemoon.androidwallet.repository;

import android.app.Application;
import net.safemoon.androidwallet.common.TokenType;
import net.safemoon.androidwallet.model.common.LoadingState;
import net.safemoon.androidwallet.model.transaction.history.Result;
import net.safemoon.androidwallet.ui.displayModel.UserTokenItemDisplayModel;
import org.web3j.abi.datatypes.Address;

/* compiled from: TransactionDataSource.kt */
/* loaded from: classes2.dex */
public final class TransactionDataSource extends gp2<Integer, Result> {
    public final Application c;
    public final String d;
    public final UserTokenItemDisplayModel e;
    public final TokenType f;
    public final gb2<LoadingState> g;
    public final sy1 h;

    public TransactionDataSource(Application application, String str, UserTokenItemDisplayModel userTokenItemDisplayModel, TokenType tokenType, gb2<LoadingState> gb2Var) {
        fs1.f(application, "application");
        fs1.f(str, Address.TYPE_NAME);
        fs1.f(userTokenItemDisplayModel, "userTokenItemDisplayModel");
        fs1.f(tokenType, "tokenType");
        fs1.f(gb2Var, "transactionLoadingState");
        this.c = application;
        this.d = str;
        this.e = userTokenItemDisplayModel;
        this.f = tokenType;
        this.g = gb2Var;
        this.h = zy1.a(new TransactionDataSource$pendinTxList$2(this));
    }

    /* JADX WARN: Removed duplicated region for block: B:10:0x0027  */
    /* JADX WARN: Removed duplicated region for block: B:18:0x003f  */
    /* JADX WARN: Removed duplicated region for block: B:36:0x00cb  */
    /* JADX WARN: Removed duplicated region for block: B:37:0x00cd A[Catch: Exception -> 0x0034, TryCatch #1 {Exception -> 0x0034, blocks: (B:12:0x002f, B:34:0x00b7, B:38:0x00d2, B:37:0x00cd), top: B:46:0x002f }] */
    @Override // defpackage.gp2
    /*
        Code decompiled incorrectly, please refer to instructions dump.
        To view partially-correct code enable 'Show inconsistent code' option in preferences
    */
    public java.lang.Object f(defpackage.gp2.a<java.lang.Integer> r18, defpackage.q70<? super defpackage.gp2.b<java.lang.Integer, net.safemoon.androidwallet.model.transaction.history.Result>> r19) {
        /*
            Method dump skipped, instructions count: 248
            To view this dump change 'Code comments level' option to 'DEBUG'
        */
        throw new UnsupportedOperationException("Method not decompiled: net.safemoon.androidwallet.repository.TransactionDataSource.f(gp2$a, q70):java.lang.Object");
    }

    @Override // defpackage.gp2
    /* renamed from: k */
    public Integer d(ip2<Integer, Result> ip2Var) {
        fs1.f(ip2Var, "state");
        return 1;
    }
}
