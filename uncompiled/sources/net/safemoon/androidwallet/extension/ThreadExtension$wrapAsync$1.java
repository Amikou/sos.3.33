package net.safemoon.androidwallet.extension;

import kotlin.coroutines.jvm.internal.SuspendLambda;
import kotlin.coroutines.jvm.internal.a;
import kotlinx.coroutines.CoroutineDispatcher;

/* compiled from: ThreadExtension.kt */
@a(c = "net.safemoon.androidwallet.extension.ThreadExtension$wrapAsync$1", f = "ThreadExtension.kt", l = {14}, m = "invokeSuspend")
/* loaded from: classes2.dex */
public final class ThreadExtension$wrapAsync$1 extends SuspendLambda implements hd1<c90, q70<? super te4>, Object> {
    public final /* synthetic */ hd1<c90, q70<? super te4>, Object> $block;
    public int label;

    /* compiled from: ThreadExtension.kt */
    @a(c = "net.safemoon.androidwallet.extension.ThreadExtension$wrapAsync$1$1", f = "ThreadExtension.kt", l = {15}, m = "invokeSuspend")
    /* renamed from: net.safemoon.androidwallet.extension.ThreadExtension$wrapAsync$1$1  reason: invalid class name */
    /* loaded from: classes2.dex */
    public static final class AnonymousClass1 extends SuspendLambda implements hd1<c90, q70<? super te4>, Object> {
        public final /* synthetic */ hd1<c90, q70<? super te4>, Object> $block;
        private /* synthetic */ Object L$0;
        public int label;

        /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
        /* JADX WARN: Multi-variable type inference failed */
        public AnonymousClass1(hd1<? super c90, ? super q70<? super te4>, ? extends Object> hd1Var, q70<? super AnonymousClass1> q70Var) {
            super(2, q70Var);
            this.$block = hd1Var;
        }

        @Override // kotlin.coroutines.jvm.internal.BaseContinuationImpl
        public final q70<te4> create(Object obj, q70<?> q70Var) {
            AnonymousClass1 anonymousClass1 = new AnonymousClass1(this.$block, q70Var);
            anonymousClass1.L$0 = obj;
            return anonymousClass1;
        }

        @Override // defpackage.hd1
        public final Object invoke(c90 c90Var, q70<? super te4> q70Var) {
            return ((AnonymousClass1) create(c90Var, q70Var)).invokeSuspend(te4.a);
        }

        @Override // kotlin.coroutines.jvm.internal.BaseContinuationImpl
        public final Object invokeSuspend(Object obj) {
            Object d = gs1.d();
            int i = this.label;
            if (i == 0) {
                o83.b(obj);
                hd1<c90, q70<? super te4>, Object> hd1Var = this.$block;
                this.label = 1;
                if (hd1Var.invoke((c90) this.L$0, this) == d) {
                    return d;
                }
            } else if (i != 1) {
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            } else {
                o83.b(obj);
            }
            return te4.a;
        }
    }

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    /* JADX WARN: Multi-variable type inference failed */
    public ThreadExtension$wrapAsync$1(hd1<? super c90, ? super q70<? super te4>, ? extends Object> hd1Var, q70<? super ThreadExtension$wrapAsync$1> q70Var) {
        super(2, q70Var);
        this.$block = hd1Var;
    }

    @Override // kotlin.coroutines.jvm.internal.BaseContinuationImpl
    public final q70<te4> create(Object obj, q70<?> q70Var) {
        return new ThreadExtension$wrapAsync$1(this.$block, q70Var);
    }

    @Override // defpackage.hd1
    public final Object invoke(c90 c90Var, q70<? super te4> q70Var) {
        return ((ThreadExtension$wrapAsync$1) create(c90Var, q70Var)).invokeSuspend(te4.a);
    }

    @Override // kotlin.coroutines.jvm.internal.BaseContinuationImpl
    public final Object invokeSuspend(Object obj) {
        Object d = gs1.d();
        int i = this.label;
        if (i == 0) {
            o83.b(obj);
            CoroutineDispatcher b = tp0.b();
            AnonymousClass1 anonymousClass1 = new AnonymousClass1(this.$block, null);
            this.label = 1;
            if (kotlinx.coroutines.a.e(b, anonymousClass1, this) == d) {
                return d;
            }
        } else if (i != 1) {
            throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        } else {
            o83.b(obj);
        }
        return te4.a;
    }
}
