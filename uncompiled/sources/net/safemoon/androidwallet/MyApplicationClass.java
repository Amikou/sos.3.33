package net.safemoon.androidwallet;

import android.app.Application;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.os.Build;
import android.webkit.WebView;
import androidx.appcompat.app.b;
import com.onesignal.OneSignal;
import com.yariksoffice.lingver.Lingver;
import java.util.ArrayList;
import java.util.HashMap;
import net.safemoon.androidwallet.model.Coin;
import net.safemoon.androidwallet.repository.FcmDataSource;
import net.safemoon.androidwallet.utils.PreFetchData;

/* loaded from: classes2.dex */
public class MyApplicationClass extends Application {
    public static MyApplicationClass p0;
    public NotificationManager a;
    public qy4 l0;
    public boolean f0 = false;
    public boolean g0 = true;
    public boolean h0 = true;
    public boolean i0 = false;
    public ArrayList<Coin> j0 = new ArrayList<>();
    public HashMap<String, Boolean> k0 = new HashMap<>();
    public boolean m0 = false;
    public String n0 = "";
    public String o0 = "";

    public MyApplicationClass() {
        p0 = this;
    }

    public static MyApplicationClass c() {
        return p0;
    }

    public void a() {
        if (Build.VERSION.SDK_INT >= 26) {
            NotificationChannel notificationChannel = new NotificationChannel("net.safemoon.androidwallet", "ANDROID CHANNEL", 4);
            notificationChannel.setDescription("Safemoon");
            notificationChannel.enableLights(true);
            notificationChannel.enableVibration(true);
            notificationChannel.setLightColor(-16711936);
            notificationChannel.setLockscreenVisibility(1);
            d().createNotificationChannel(notificationChannel);
        }
    }

    public final void b() {
        r81.a.a(getApplicationContext()).a();
    }

    public final NotificationManager d() {
        if (this.a == null) {
            this.a = (NotificationManager) getSystemService("notification");
        }
        return this.a;
    }

    public final void e() {
        OneSignal.E1(OneSignal.LOG_LEVEL.VERBOSE, OneSignal.LOG_LEVEL.NONE);
        OneSignal.L0(this);
    }

    @Override // android.app.Application
    public void onCreate() {
        super.onCreate();
        a4.p(this);
        PreFetchData.a.b(this);
        new FcmDataSource(this).i();
        registerActivityLifecycleCallbacks(new o43());
        int i = getApplicationContext().getResources().getConfiguration().uiMode & 48;
        int i2 = 1;
        if (i != 0 && i != 16 && i == 32) {
            i2 = 2;
        }
        b.G(bo3.f(getApplicationContext(), "MODE_NIGHT", i2));
        a();
        e();
        b();
        new WebView(this).destroy();
        Lingver.g(this, do3.a.a(getApplicationContext()));
        qy4 qy4Var = new qy4();
        this.l0 = qy4Var;
        i2.r(qy4Var, s.e, "12312");
    }
}
