package net.safemoon.androidwallet.utils;

import java.io.Serializable;

/* compiled from: ChartProvider.kt */
/* loaded from: classes2.dex */
public final class ChartParameter implements Serializable {
    private int defaultInterval;

    public final int getDefaultInterval() {
        return this.defaultInterval;
    }

    public final void setDefaultInterval(int i) {
        this.defaultInterval = i;
    }
}
