package net.safemoon.androidwallet.utils;

import android.app.Application;
import net.safemoon.androidwallet.repository.ReflectionDataSource;

/* compiled from: ReflectionCustomContract.kt */
/* loaded from: classes2.dex */
public final class ReflectionCustomContract {
    public final Application a;
    public final ReflectionDataSource b;
    public final sy1 c;

    public ReflectionCustomContract(Application application, ReflectionDataSource reflectionDataSource) {
        fs1.f(application, "appContext");
        fs1.f(reflectionDataSource, "reflectionDataSource");
        this.a = application;
        this.b = reflectionDataSource;
        this.c = zy1.a(new ReflectionCustomContract$address$2(this));
    }

    public final String b() {
        return (String) this.c.getValue();
    }

    public final Application c() {
        return this.a;
    }

    public final Application d() {
        return this.a;
    }

    /* JADX WARN: Removed duplicated region for block: B:10:0x0024  */
    /* JADX WARN: Removed duplicated region for block: B:17:0x0035  */
    /* JADX WARN: Removed duplicated region for block: B:34:0x007d A[Catch: Exception -> 0x002a, TryCatch #0 {Exception -> 0x002a, blocks: (B:11:0x0026, B:30:0x0075, B:41:0x0091, B:44:0x009d, B:47:0x00a6, B:50:0x00ad, B:34:0x007d, B:37:0x0086, B:40:0x008d, B:23:0x0049, B:27:0x006c, B:24:0x0059), top: B:54:0x0022 }] */
    /* JADX WARN: Removed duplicated region for block: B:43:0x009c A[ADDED_TO_REGION] */
    /* JADX WARN: Removed duplicated region for block: B:44:0x009d A[Catch: Exception -> 0x002a, TryCatch #0 {Exception -> 0x002a, blocks: (B:11:0x0026, B:30:0x0075, B:41:0x0091, B:44:0x009d, B:47:0x00a6, B:50:0x00ad, B:34:0x007d, B:37:0x0086, B:40:0x008d, B:23:0x0049, B:27:0x006c, B:24:0x0059), top: B:54:0x0022 }] */
    /*
        Code decompiled incorrectly, please refer to instructions dump.
        To view partially-correct code enable 'Show inconsistent code' option in preferences
    */
    public final java.lang.Object e(net.safemoon.androidwallet.common.TokenType r7, java.lang.String r8, java.lang.String r9, defpackage.q70<? super java.math.BigInteger> r10) {
        /*
            r6 = this;
            boolean r0 = r10 instanceof net.safemoon.androidwallet.utils.ReflectionCustomContract$getBalance$1
            if (r0 == 0) goto L13
            r0 = r10
            net.safemoon.androidwallet.utils.ReflectionCustomContract$getBalance$1 r0 = (net.safemoon.androidwallet.utils.ReflectionCustomContract$getBalance$1) r0
            int r1 = r0.label
            r2 = -2147483648(0xffffffff80000000, float:-0.0)
            r3 = r1 & r2
            if (r3 == 0) goto L13
            int r1 = r1 - r2
            r0.label = r1
            goto L18
        L13:
            net.safemoon.androidwallet.utils.ReflectionCustomContract$getBalance$1 r0 = new net.safemoon.androidwallet.utils.ReflectionCustomContract$getBalance$1
            r0.<init>(r6, r10)
        L18:
            java.lang.Object r10 = r0.result
            java.lang.Object r1 = defpackage.gs1.d()
            int r2 = r0.label
            r3 = 1
            r4 = 0
            if (r2 == 0) goto L35
            if (r2 != r3) goto L2d
            defpackage.o83.b(r10)     // Catch: java.lang.Exception -> L2a
            goto L75
        L2a:
            r7 = move-exception
            goto Lb3
        L2d:
            java.lang.IllegalStateException r7 = new java.lang.IllegalStateException
            java.lang.String r8 = "call to 'resume' before 'invoke' with coroutine"
            r7.<init>(r8)
            throw r7
        L35:
            defpackage.o83.b(r10)
            wq r10 = defpackage.e30.s(r7)
            int r2 = r9.length()
            if (r2 != 0) goto L44
            r2 = r3
            goto L45
        L44:
            r2 = 0
        L45:
            java.lang.String r5 = "address"
            if (r2 == 0) goto L59
            java.lang.String r9 = r6.b()     // Catch: java.lang.Exception -> L2a
            defpackage.fs1.e(r9, r5)     // Catch: java.lang.Exception -> L2a
            java.lang.String r7 = defpackage.e30.x(r7)     // Catch: java.lang.Exception -> L2a
            retrofit2.b r7 = r10.f(r9, r8, r7)     // Catch: java.lang.Exception -> L2a
            goto L68
        L59:
            java.lang.String r2 = r6.b()     // Catch: java.lang.Exception -> L2a
            java.lang.String r7 = defpackage.e30.x(r7)     // Catch: java.lang.Exception -> L2a
            defpackage.fs1.e(r2, r5)     // Catch: java.lang.Exception -> L2a
            retrofit2.b r7 = r10.c(r9, r2, r8, r7)     // Catch: java.lang.Exception -> L2a
        L68:
            if (r7 != 0) goto L6c
            r10 = r4
            goto L77
        L6c:
            r0.label = r3     // Catch: java.lang.Exception -> L2a
            java.lang.Object r10 = retrofit2.KotlinExtensions.c(r7, r0)     // Catch: java.lang.Exception -> L2a
            if (r10 != r1) goto L75
            return r1
        L75:
            retrofit2.n r10 = (retrofit2.n) r10     // Catch: java.lang.Exception -> L2a
        L77:
            java.lang.String r7 = "getBalance_response: "
            if (r10 != 0) goto L7d
        L7b:
            r8 = r4
            goto L91
        L7d:
            java.lang.Object r8 = r10.a()     // Catch: java.lang.Exception -> L2a
            net.safemoon.androidwallet.model.BalanceByBlock r8 = (net.safemoon.androidwallet.model.BalanceByBlock) r8     // Catch: java.lang.Exception -> L2a
            if (r8 != 0) goto L86
            goto L7b
        L86:
            java.lang.String r8 = r8.getResult()     // Catch: java.lang.Exception -> L2a
            if (r8 != 0) goto L8d
            goto L7b
        L8d:
            java.math.BigInteger r8 = defpackage.bv3.i(r8)     // Catch: java.lang.Exception -> L2a
        L91:
            java.lang.String r7 = defpackage.fs1.l(r7, r8)     // Catch: java.lang.Exception -> L2a
            java.io.PrintStream r8 = java.lang.System.out     // Catch: java.lang.Exception -> L2a
            r8.println(r7)     // Catch: java.lang.Exception -> L2a
            if (r10 != 0) goto L9d
            goto Lbd
        L9d:
            java.lang.Object r7 = r10.a()     // Catch: java.lang.Exception -> L2a
            net.safemoon.androidwallet.model.BalanceByBlock r7 = (net.safemoon.androidwallet.model.BalanceByBlock) r7     // Catch: java.lang.Exception -> L2a
            if (r7 != 0) goto La6
            goto Lbd
        La6:
            java.lang.String r7 = r7.getResult()     // Catch: java.lang.Exception -> L2a
            if (r7 != 0) goto Lad
            goto Lbd
        Lad:
            java.math.BigInteger r7 = defpackage.bv3.i(r7)     // Catch: java.lang.Exception -> L2a
            r4 = r7
            goto Lbd
        Lb3:
            java.io.PrintStream r8 = java.lang.System.out
            java.lang.String r9 = "getBalance_ERROR"
            r8.print(r9)
            r7.printStackTrace()
        Lbd:
            return r4
        */
        throw new UnsupportedOperationException("Method not decompiled: net.safemoon.androidwallet.utils.ReflectionCustomContract.e(net.safemoon.androidwallet.common.TokenType, java.lang.String, java.lang.String, q70):java.lang.Object");
    }

    /* JADX WARN: Removed duplicated region for block: B:10:0x002a  */
    /* JADX WARN: Removed duplicated region for block: B:15:0x0038  */
    /* JADX WARN: Removed duplicated region for block: B:30:0x0095 A[Catch: Exception -> 0x00be, TryCatch #0 {Exception -> 0x00be, blocks: (B:11:0x002c, B:28:0x0091, B:30:0x0095, B:32:0x009b, B:37:0x00a9, B:40:0x00b2, B:43:0x00b7, B:35:0x00a5, B:21:0x004c, B:25:0x0088, B:22:0x0067), top: B:48:0x0028 }] */
    /* JADX WARN: Removed duplicated region for block: B:50:? A[ADDED_TO_REGION, RETURN, SYNTHETIC] */
    /*
        Code decompiled incorrectly, please refer to instructions dump.
        To view partially-correct code enable 'Show inconsistent code' option in preferences
    */
    public final java.lang.Object f(net.safemoon.androidwallet.common.TokenType r19, java.lang.String r20, int r21, int r22, defpackage.q70<? super java.util.List<? extends net.safemoon.androidwallet.model.transaction.history.Result>> r23) {
        /*
            r18 = this;
            r0 = r23
            boolean r1 = r0 instanceof net.safemoon.androidwallet.utils.ReflectionCustomContract$getTxData$2
            if (r1 == 0) goto L17
            r1 = r0
            net.safemoon.androidwallet.utils.ReflectionCustomContract$getTxData$2 r1 = (net.safemoon.androidwallet.utils.ReflectionCustomContract$getTxData$2) r1
            int r2 = r1.label
            r3 = -2147483648(0xffffffff80000000, float:-0.0)
            r4 = r2 & r3
            if (r4 == 0) goto L17
            int r2 = r2 - r3
            r1.label = r2
            r2 = r18
            goto L1e
        L17:
            net.safemoon.androidwallet.utils.ReflectionCustomContract$getTxData$2 r1 = new net.safemoon.androidwallet.utils.ReflectionCustomContract$getTxData$2
            r2 = r18
            r1.<init>(r2, r0)
        L1e:
            java.lang.Object r0 = r1.result
            java.lang.Object r3 = defpackage.gs1.d()
            int r4 = r1.label
            r5 = 1
            r6 = 0
            if (r4 == 0) goto L38
            if (r4 != r5) goto L30
            defpackage.o83.b(r0)     // Catch: java.lang.Exception -> Lbe
            goto L91
        L30:
            java.lang.IllegalStateException r0 = new java.lang.IllegalStateException
            java.lang.String r1 = "call to 'resume' before 'invoke' with coroutine"
            r0.<init>(r1)
            throw r0
        L38:
            defpackage.o83.b(r0)
            wq r7 = defpackage.e30.s(r19)
            int r0 = r20.length()
            if (r0 != 0) goto L47
            r0 = r5
            goto L48
        L47:
            r0 = 0
        L48:
            r4 = 1000(0x3e8, float:1.401E-42)
            if (r0 == 0) goto L67
            java.lang.String r8 = r18.b()     // Catch: java.lang.Exception -> Lbe
            r9 = 0
            java.lang.Integer r10 = defpackage.hr.d(r4)     // Catch: java.lang.Exception -> Lbe
            java.lang.String r11 = defpackage.e30.x(r19)     // Catch: java.lang.Exception -> Lbe
            java.lang.String r12 = "asc"
            r15 = 2
            r16 = 0
            r13 = r21
            r14 = r22
            retrofit2.b r0 = defpackage.wq.a.a(r7, r8, r9, r10, r11, r12, r13, r14, r15, r16)     // Catch: java.lang.Exception -> Lbe
            goto L84
        L67:
            java.lang.String r8 = r18.b()     // Catch: java.lang.Exception -> Lbe
            java.lang.String r11 = defpackage.e30.x(r19)     // Catch: java.lang.Exception -> Lbe
            r9 = 0
            java.lang.Integer r10 = defpackage.hr.d(r4)     // Catch: java.lang.Exception -> Lbe
            java.lang.String r13 = "asc"
            r16 = 2
            r17 = 0
            r12 = r20
            r14 = r21
            r15 = r22
            retrofit2.b r0 = defpackage.wq.a.b(r7, r8, r9, r10, r11, r12, r13, r14, r15, r16, r17)     // Catch: java.lang.Exception -> Lbe
        L84:
            if (r0 != 0) goto L88
            r0 = r6
            goto L93
        L88:
            r1.label = r5     // Catch: java.lang.Exception -> Lbe
            java.lang.Object r0 = retrofit2.KotlinExtensions.c(r0, r1)     // Catch: java.lang.Exception -> Lbe
            if (r0 != r3) goto L91
            return r3
        L91:
            retrofit2.n r0 = (retrofit2.n) r0     // Catch: java.lang.Exception -> Lbe
        L93:
            if (r0 == 0) goto Lbe
            boolean r1 = r0.e()     // Catch: java.lang.Exception -> Lbe
            if (r1 == 0) goto Lbe
            java.lang.Object r1 = r0.a()     // Catch: java.lang.Exception -> Lbe
            net.safemoon.androidwallet.model.transaction.history.TransactionHistoryModel r1 = (net.safemoon.androidwallet.model.transaction.history.TransactionHistoryModel) r1     // Catch: java.lang.Exception -> Lbe
            if (r1 != 0) goto La5
            r1 = r6
            goto La7
        La5:
            java.util.ArrayList<net.safemoon.androidwallet.model.transaction.history.Result> r1 = r1.result     // Catch: java.lang.Exception -> Lbe
        La7:
            if (r1 == 0) goto Lbe
            java.lang.Object r0 = r0.a()     // Catch: java.lang.Exception -> Lbe
            net.safemoon.androidwallet.model.transaction.history.TransactionHistoryModel r0 = (net.safemoon.androidwallet.model.transaction.history.TransactionHistoryModel) r0     // Catch: java.lang.Exception -> Lbe
            if (r0 != 0) goto Lb2
            goto Lbe
        Lb2:
            java.util.ArrayList<net.safemoon.androidwallet.model.transaction.history.Result> r0 = r0.result     // Catch: java.lang.Exception -> Lbe
            if (r0 != 0) goto Lb7
            goto Lbe
        Lb7:
            int r1 = r0.size()     // Catch: java.lang.Exception -> Lbe
            if (r1 <= 0) goto Lbe
            r6 = r0
        Lbe:
            return r6
        */
        throw new UnsupportedOperationException("Method not decompiled: net.safemoon.androidwallet.utils.ReflectionCustomContract.f(net.safemoon.androidwallet.common.TokenType, java.lang.String, int, int, q70):java.lang.Object");
    }

    /* JADX WARN: Removed duplicated region for block: B:10:0x002a  */
    /* JADX WARN: Removed duplicated region for block: B:15:0x0038  */
    /* JADX WARN: Removed duplicated region for block: B:30:0x0092 A[Catch: Exception -> 0x00bb, TryCatch #0 {Exception -> 0x00bb, blocks: (B:11:0x002c, B:28:0x008e, B:30:0x0092, B:32:0x0098, B:37:0x00a6, B:40:0x00af, B:43:0x00b4, B:35:0x00a2, B:21:0x004c, B:25:0x0085, B:22:0x0066), top: B:48:0x0028 }] */
    /* JADX WARN: Removed duplicated region for block: B:50:? A[ADDED_TO_REGION, RETURN, SYNTHETIC] */
    /*
        Code decompiled incorrectly, please refer to instructions dump.
        To view partially-correct code enable 'Show inconsistent code' option in preferences
    */
    public final java.lang.Object g(net.safemoon.androidwallet.common.TokenType r19, java.lang.String r20, defpackage.q70<? super java.util.List<? extends net.safemoon.androidwallet.model.transaction.history.Result>> r21) {
        /*
            r18 = this;
            r0 = r21
            boolean r1 = r0 instanceof net.safemoon.androidwallet.utils.ReflectionCustomContract$getTxData$1
            if (r1 == 0) goto L17
            r1 = r0
            net.safemoon.androidwallet.utils.ReflectionCustomContract$getTxData$1 r1 = (net.safemoon.androidwallet.utils.ReflectionCustomContract$getTxData$1) r1
            int r2 = r1.label
            r3 = -2147483648(0xffffffff80000000, float:-0.0)
            r4 = r2 & r3
            if (r4 == 0) goto L17
            int r2 = r2 - r3
            r1.label = r2
            r2 = r18
            goto L1e
        L17:
            net.safemoon.androidwallet.utils.ReflectionCustomContract$getTxData$1 r1 = new net.safemoon.androidwallet.utils.ReflectionCustomContract$getTxData$1
            r2 = r18
            r1.<init>(r2, r0)
        L1e:
            java.lang.Object r0 = r1.result
            java.lang.Object r3 = defpackage.gs1.d()
            int r4 = r1.label
            r5 = 1
            r6 = 0
            if (r4 == 0) goto L38
            if (r4 != r5) goto L30
            defpackage.o83.b(r0)     // Catch: java.lang.Exception -> Lbb
            goto L8e
        L30:
            java.lang.IllegalStateException r0 = new java.lang.IllegalStateException
            java.lang.String r1 = "call to 'resume' before 'invoke' with coroutine"
            r0.<init>(r1)
            throw r0
        L38:
            defpackage.o83.b(r0)
            wq r7 = defpackage.e30.s(r19)
            int r0 = r20.length()
            if (r0 != 0) goto L47
            r0 = r5
            goto L48
        L47:
            r0 = 0
        L48:
            r4 = 1000(0x3e8, float:1.401E-42)
            if (r0 == 0) goto L66
            java.lang.String r8 = r18.b()     // Catch: java.lang.Exception -> Lbb
            r9 = 0
            java.lang.Integer r10 = defpackage.hr.d(r4)     // Catch: java.lang.Exception -> Lbb
            java.lang.String r11 = defpackage.e30.x(r19)     // Catch: java.lang.Exception -> Lbb
            java.lang.String r12 = "asc"
            r13 = 0
            r14 = 0
            r15 = 98
            r16 = 0
            retrofit2.b r0 = defpackage.wq.a.a(r7, r8, r9, r10, r11, r12, r13, r14, r15, r16)     // Catch: java.lang.Exception -> Lbb
            goto L81
        L66:
            java.lang.String r8 = r18.b()     // Catch: java.lang.Exception -> Lbb
            java.lang.String r11 = defpackage.e30.x(r19)     // Catch: java.lang.Exception -> Lbb
            r9 = 0
            java.lang.Integer r10 = defpackage.hr.d(r4)     // Catch: java.lang.Exception -> Lbb
            java.lang.String r13 = "asc"
            r14 = 0
            r15 = 0
            r16 = 194(0xc2, float:2.72E-43)
            r17 = 0
            r12 = r20
            retrofit2.b r0 = defpackage.wq.a.b(r7, r8, r9, r10, r11, r12, r13, r14, r15, r16, r17)     // Catch: java.lang.Exception -> Lbb
        L81:
            if (r0 != 0) goto L85
            r0 = r6
            goto L90
        L85:
            r1.label = r5     // Catch: java.lang.Exception -> Lbb
            java.lang.Object r0 = retrofit2.KotlinExtensions.c(r0, r1)     // Catch: java.lang.Exception -> Lbb
            if (r0 != r3) goto L8e
            return r3
        L8e:
            retrofit2.n r0 = (retrofit2.n) r0     // Catch: java.lang.Exception -> Lbb
        L90:
            if (r0 == 0) goto Lbb
            boolean r1 = r0.e()     // Catch: java.lang.Exception -> Lbb
            if (r1 == 0) goto Lbb
            java.lang.Object r1 = r0.a()     // Catch: java.lang.Exception -> Lbb
            net.safemoon.androidwallet.model.transaction.history.TransactionHistoryModel r1 = (net.safemoon.androidwallet.model.transaction.history.TransactionHistoryModel) r1     // Catch: java.lang.Exception -> Lbb
            if (r1 != 0) goto La2
            r1 = r6
            goto La4
        La2:
            java.util.ArrayList<net.safemoon.androidwallet.model.transaction.history.Result> r1 = r1.result     // Catch: java.lang.Exception -> Lbb
        La4:
            if (r1 == 0) goto Lbb
            java.lang.Object r0 = r0.a()     // Catch: java.lang.Exception -> Lbb
            net.safemoon.androidwallet.model.transaction.history.TransactionHistoryModel r0 = (net.safemoon.androidwallet.model.transaction.history.TransactionHistoryModel) r0     // Catch: java.lang.Exception -> Lbb
            if (r0 != 0) goto Laf
            goto Lbb
        Laf:
            java.util.ArrayList<net.safemoon.androidwallet.model.transaction.history.Result> r0 = r0.result     // Catch: java.lang.Exception -> Lbb
            if (r0 != 0) goto Lb4
            goto Lbb
        Lb4:
            int r1 = r0.size()     // Catch: java.lang.Exception -> Lbb
            if (r1 <= 0) goto Lbb
            r6 = r0
        Lbb:
            return r6
        */
        throw new UnsupportedOperationException("Method not decompiled: net.safemoon.androidwallet.utils.ReflectionCustomContract.g(net.safemoon.androidwallet.common.TokenType, java.lang.String, q70):java.lang.Object");
    }

    /* JADX WARN: Can't wrap try/catch for region: R(11:1|(2:3|(9:5|6|7|(1:(2:(1:(1:(1:(2:14|15))(6:17|18|19|(1:21)|22|23))(8:24|25|26|(1:28)(1:42)|29|(2:38|(1:40)(3:41|19|(0)))|22|23))|43)(2:44|45))(3:74|75|(1:77)(1:78))|46|(3:73|49|(6:(5:59|53|(1:55)|22|23)|52|53|(0)|22|23)(6:60|(2:66|(1:68)(7:69|26|(0)(0)|29|(1:31)(4:32|35|38|(0)(0))|22|23))|62|(0)(0)|22|23))|48|49|(0)(0)))|80|6|7|(0)(0)|46|(4:70|73|49|(0)(0))|48|49|(0)(0)) */
    /* JADX WARN: Removed duplicated region for block: B:10:0x002e  */
    /* JADX WARN: Removed duplicated region for block: B:28:0x0073  */
    /* JADX WARN: Removed duplicated region for block: B:42:0x00a1  */
    /* JADX WARN: Removed duplicated region for block: B:50:0x00c3 A[RETURN] */
    /* JADX WARN: Removed duplicated region for block: B:51:0x00c4 A[Catch: Exception -> 0x0131, TryCatch #0 {Exception -> 0x0131, blocks: (B:20:0x0051, B:78:0x0120, B:23:0x005e, B:61:0x00e4, B:68:0x00f7, B:71:0x00fe, B:74:0x0108, B:65:0x00ec, B:15:0x0038, B:26:0x006f, B:33:0x008a, B:48:0x00b5, B:44:0x00a5, B:47:0x00ac, B:51:0x00c4, B:54:0x00cd, B:57:0x00d4, B:36:0x0092, B:39:0x0099, B:29:0x0076), top: B:84:0x002c }] */
    /* JADX WARN: Removed duplicated region for block: B:63:0x00e8  */
    /* JADX WARN: Removed duplicated region for block: B:65:0x00ec A[Catch: Exception -> 0x0131, TryCatch #0 {Exception -> 0x0131, blocks: (B:20:0x0051, B:78:0x0120, B:23:0x005e, B:61:0x00e4, B:68:0x00f7, B:71:0x00fe, B:74:0x0108, B:65:0x00ec, B:15:0x0038, B:26:0x006f, B:33:0x008a, B:48:0x00b5, B:44:0x00a5, B:47:0x00ac, B:51:0x00c4, B:54:0x00cd, B:57:0x00d4, B:36:0x0092, B:39:0x0099, B:29:0x0076), top: B:84:0x002c }] */
    /* JADX WARN: Removed duplicated region for block: B:67:0x00f6 A[ADDED_TO_REGION] */
    /* JADX WARN: Removed duplicated region for block: B:68:0x00f7 A[Catch: Exception -> 0x0131, TryCatch #0 {Exception -> 0x0131, blocks: (B:20:0x0051, B:78:0x0120, B:23:0x005e, B:61:0x00e4, B:68:0x00f7, B:71:0x00fe, B:74:0x0108, B:65:0x00ec, B:15:0x0038, B:26:0x006f, B:33:0x008a, B:48:0x00b5, B:44:0x00a5, B:47:0x00ac, B:51:0x00c4, B:54:0x00cd, B:57:0x00d4, B:36:0x0092, B:39:0x0099, B:29:0x0076), top: B:84:0x002c }] */
    /* JADX WARN: Removed duplicated region for block: B:76:0x011e A[RETURN] */
    /* JADX WARN: Removed duplicated region for block: B:77:0x011f  */
    /* JADX WARN: Removed duplicated region for block: B:80:0x0130 A[RETURN] */
    /*
        Code decompiled incorrectly, please refer to instructions dump.
        To view partially-correct code enable 'Show inconsistent code' option in preferences
    */
    public final java.lang.Object h(java.lang.String r17, java.lang.String r18, defpackage.q70<? super defpackage.te4> r19) {
        /*
            Method dump skipped, instructions count: 308
            To view this dump change 'Code comments level' option to 'DEBUG'
        */
        throw new UnsupportedOperationException("Method not decompiled: net.safemoon.androidwallet.utils.ReflectionCustomContract.h(java.lang.String, java.lang.String, q70):java.lang.Object");
    }

    /* JADX WARN: Can't wrap try/catch for region: R(19:1|(2:3|(17:5|6|7|(1:(1:(1:(7:12|13|14|(4:17|(3:22|23|(3:33|34|35)(3:25|26|(2:28|29)(1:31)))|32|15)|42|43|44)(2:46|47))(7:48|49|50|(1:52)(1:59)|(4:58|14|(1:15)|42)|43|44))(1:60))(4:104|(7:107|(1:109)(1:120)|110|(1:112)(1:119)|(3:114|115|116)(1:118)|117|105)|121|122)|61|(3:64|(2:66|67)|62)|68|69|(7:72|(1:74)(1:85)|75|(1:77)(1:84)|(3:79|80|81)(1:83)|82|70)|86|87|(4:90|(1:96)(3:92|93|94)|95|88)|97|98|(2:100|(1:102)(4:103|50|(0)(0)|(1:54)(5:55|58|14|(1:15)|42)))|43|44))|124|6|7|(0)(0)|61|(1:62)|68|69|(1:70)|86|87|(1:88)|97|98|(0)|43|44) */
    /* JADX WARN: Removed duplicated region for block: B:10:0x002f  */
    /* JADX WARN: Removed duplicated region for block: B:22:0x0071  */
    /* JADX WARN: Removed duplicated region for block: B:38:0x00b1  */
    /* JADX WARN: Removed duplicated region for block: B:44:0x00dd  */
    /* JADX WARN: Removed duplicated region for block: B:57:0x0111  */
    /* JADX WARN: Removed duplicated region for block: B:63:0x013b A[Catch: Exception -> 0x01df, TRY_ENTER, TryCatch #0 {Exception -> 0x01df, blocks: (B:14:0x0045, B:77:0x0190, B:79:0x0196, B:82:0x019f, B:85:0x01bf, B:88:0x01cc, B:19:0x005a, B:67:0x016e, B:73:0x0182, B:76:0x0189, B:70:0x0179, B:63:0x013b), top: B:94:0x002d }] */
    /* JADX WARN: Removed duplicated region for block: B:69:0x0178  */
    /* JADX WARN: Removed duplicated region for block: B:70:0x0179 A[Catch: Exception -> 0x01df, TryCatch #0 {Exception -> 0x01df, blocks: (B:14:0x0045, B:77:0x0190, B:79:0x0196, B:82:0x019f, B:85:0x01bf, B:88:0x01cc, B:19:0x005a, B:67:0x016e, B:73:0x0182, B:76:0x0189, B:70:0x0179, B:63:0x013b), top: B:94:0x002d }] */
    /* JADX WARN: Removed duplicated region for block: B:79:0x0196 A[Catch: Exception -> 0x01df, TryCatch #0 {Exception -> 0x01df, blocks: (B:14:0x0045, B:77:0x0190, B:79:0x0196, B:82:0x019f, B:85:0x01bf, B:88:0x01cc, B:19:0x005a, B:67:0x016e, B:73:0x0182, B:76:0x0189, B:70:0x0179, B:63:0x013b), top: B:94:0x002d }] */
    /*
        Code decompiled incorrectly, please refer to instructions dump.
        To view partially-correct code enable 'Show inconsistent code' option in preferences
    */
    public final java.lang.Object i(java.util.List<net.safemoon.androidwallet.model.reflections.RoomReflectionsToken> r26, defpackage.q70<? super defpackage.te4> r27) {
        /*
            Method dump skipped, instructions count: 482
            To view this dump change 'Code comments level' option to 'DEBUG'
        */
        throw new UnsupportedOperationException("Method not decompiled: net.safemoon.androidwallet.utils.ReflectionCustomContract.i(java.util.List, q70):java.lang.Object");
    }

    /* JADX WARN: Removed duplicated region for block: B:10:0x0024  */
    /* JADX WARN: Removed duplicated region for block: B:21:0x0050  */
    /* JADX WARN: Removed duplicated region for block: B:24:0x005e  */
    /* JADX WARN: Removed duplicated region for block: B:31:0x00b0 A[RETURN] */
    /* JADX WARN: Removed duplicated region for block: B:33:0x00b4  */
    /* JADX WARN: Unsupported multi-entry loop pattern (BACK_EDGE: B:32:0x00b1 -> B:22:0x0058). Please submit an issue!!! */
    /* JADX WARN: Unsupported multi-entry loop pattern (BACK_EDGE: B:35:0x0058 -> B:22:0x0058). Please submit an issue!!! */
    /*
        Code decompiled incorrectly, please refer to instructions dump.
        To view partially-correct code enable 'Show inconsistent code' option in preferences
    */
    public final java.lang.Object j(java.util.List<net.safemoon.androidwallet.model.reflections.RoomReflectionsToken> r14, defpackage.q70<? super defpackage.te4> r15) {
        /*
            r13 = this;
            boolean r0 = r15 instanceof net.safemoon.androidwallet.utils.ReflectionCustomContract$saveLatestBalance$1
            if (r0 == 0) goto L13
            r0 = r15
            net.safemoon.androidwallet.utils.ReflectionCustomContract$saveLatestBalance$1 r0 = (net.safemoon.androidwallet.utils.ReflectionCustomContract$saveLatestBalance$1) r0
            int r1 = r0.label
            r2 = -2147483648(0xffffffff80000000, float:-0.0)
            r3 = r1 & r2
            if (r3 == 0) goto L13
            int r1 = r1 - r2
            r0.label = r1
            goto L18
        L13:
            net.safemoon.androidwallet.utils.ReflectionCustomContract$saveLatestBalance$1 r0 = new net.safemoon.androidwallet.utils.ReflectionCustomContract$saveLatestBalance$1
            r0.<init>(r13, r15)
        L18:
            java.lang.Object r15 = r0.result
            java.lang.Object r1 = defpackage.gs1.d()
            int r2 = r0.label
            r3 = 2
            r4 = 1
            if (r2 == 0) goto L50
            if (r2 == r4) goto L3c
            if (r2 != r3) goto L34
            java.lang.Object r14 = r0.L$1
            java.util.Iterator r14 = (java.util.Iterator) r14
            java.lang.Object r2 = r0.L$0
            net.safemoon.androidwallet.utils.ReflectionCustomContract r2 = (net.safemoon.androidwallet.utils.ReflectionCustomContract) r2
            defpackage.o83.b(r15)     // Catch: java.lang.Exception -> L58
            goto L58
        L34:
            java.lang.IllegalStateException r14 = new java.lang.IllegalStateException
            java.lang.String r15 = "call to 'resume' before 'invoke' with coroutine"
            r14.<init>(r15)
            throw r14
        L3c:
            java.lang.Object r14 = r0.L$2
            net.safemoon.androidwallet.model.reflections.RoomReflectionsToken r14 = (net.safemoon.androidwallet.model.reflections.RoomReflectionsToken) r14
            java.lang.Object r2 = r0.L$1
            java.util.Iterator r2 = (java.util.Iterator) r2
            java.lang.Object r5 = r0.L$0
            net.safemoon.androidwallet.utils.ReflectionCustomContract r5 = (net.safemoon.androidwallet.utils.ReflectionCustomContract) r5
            defpackage.o83.b(r15)     // Catch: java.lang.Exception -> L4d
            r12 = r5
            goto L86
        L4d:
            r14 = r2
            r2 = r5
            goto L58
        L50:
            defpackage.o83.b(r15)
            java.util.Iterator r14 = r14.iterator()
            r2 = r13
        L58:
            boolean r15 = r14.hasNext()
            if (r15 == 0) goto Lb4
            java.lang.Object r15 = r14.next()
            net.safemoon.androidwallet.model.reflections.RoomReflectionsToken r15 = (net.safemoon.androidwallet.model.reflections.RoomReflectionsToken) r15
            net.safemoon.androidwallet.viewmodels.blockChainClass.MyWeb3 r5 = new net.safemoon.androidwallet.viewmodels.blockChainClass.MyWeb3     // Catch: java.lang.Exception -> L58
            android.app.Application r6 = r2.d()     // Catch: java.lang.Exception -> L58
            net.safemoon.androidwallet.model.reflections.RoomReflectionsToken$Companion r7 = net.safemoon.androidwallet.model.reflections.RoomReflectionsToken.Companion     // Catch: java.lang.Exception -> L58
            net.safemoon.androidwallet.model.token.abstraction.IToken r7 = r7.toIToken(r15)     // Catch: java.lang.Exception -> L58
            r5.<init>(r6, r7)     // Catch: java.lang.Exception -> L58
            r0.L$0 = r2     // Catch: java.lang.Exception -> L58
            r0.L$1 = r14     // Catch: java.lang.Exception -> L58
            r0.L$2 = r15     // Catch: java.lang.Exception -> L58
            r0.label = r4     // Catch: java.lang.Exception -> L58
            java.lang.Object r5 = r5.f(r0)     // Catch: java.lang.Exception -> L58
            if (r5 != r1) goto L82
            return r1
        L82:
            r12 = r2
            r2 = r14
            r14 = r15
            r15 = r5
        L86:
            java.math.BigInteger r15 = (java.math.BigInteger) r15     // Catch: java.lang.Exception -> Lb1
            net.safemoon.androidwallet.repository.ReflectionDataSource r5 = r12.b     // Catch: java.lang.Exception -> Lb1
            defpackage.fs1.d(r15)     // Catch: java.lang.Exception -> Lb1
            long r6 = r15.longValue()     // Catch: java.lang.Exception -> Lb1
            b30 r15 = defpackage.b30.a     // Catch: java.lang.Exception -> Lb1
            java.util.Date r8 = new java.util.Date     // Catch: java.lang.Exception -> Lb1
            r8.<init>()     // Catch: java.lang.Exception -> Lb1
            long r8 = r15.z(r8)     // Catch: java.lang.Exception -> Lb1
            java.lang.String r10 = r14.getSymbolWithType()     // Catch: java.lang.Exception -> Lb1
            r0.L$0 = r12     // Catch: java.lang.Exception -> Lb1
            r0.L$1 = r2     // Catch: java.lang.Exception -> Lb1
            r14 = 0
            r0.L$2 = r14     // Catch: java.lang.Exception -> Lb1
            r0.label = r3     // Catch: java.lang.Exception -> Lb1
            r11 = r0
            java.lang.Object r14 = r5.q(r6, r8, r10, r11)     // Catch: java.lang.Exception -> Lb1
            if (r14 != r1) goto Lb1
            return r1
        Lb1:
            r14 = r2
            r2 = r12
            goto L58
        Lb4:
            te4 r14 = defpackage.te4.a
            return r14
        */
        throw new UnsupportedOperationException("Method not decompiled: net.safemoon.androidwallet.utils.ReflectionCustomContract.j(java.util.List, q70):java.lang.Object");
    }

    /* JADX WARN: Removed duplicated region for block: B:10:0x0032  */
    /* JADX WARN: Removed duplicated region for block: B:22:0x00a8  */
    /* JADX WARN: Removed duplicated region for block: B:32:0x00d0  */
    /* JADX WARN: Removed duplicated region for block: B:41:0x00ff  */
    /* JADX WARN: Removed duplicated region for block: B:46:0x014d  */
    /* JADX WARN: Removed duplicated region for block: B:49:0x015c  */
    /* JADX WARN: Removed duplicated region for block: B:58:0x018d  */
    /* JADX WARN: Removed duplicated region for block: B:74:? A[ADDED_TO_REGION, RETURN, SYNTHETIC] */
    /* JADX WARN: Removed duplicated region for block: B:76:? A[RETURN, SYNTHETIC] */
    /*
        Code decompiled incorrectly, please refer to instructions dump.
        To view partially-correct code enable 'Show inconsistent code' option in preferences
    */
    public final java.lang.Object k(net.safemoon.androidwallet.model.token.abstraction.IToken r35, defpackage.q70<? super java.lang.Long> r36) {
        /*
            Method dump skipped, instructions count: 485
            To view this dump change 'Code comments level' option to 'DEBUG'
        */
        throw new UnsupportedOperationException("Method not decompiled: net.safemoon.androidwallet.utils.ReflectionCustomContract.k(net.safemoon.androidwallet.model.token.abstraction.IToken, q70):java.lang.Object");
    }

    /* JADX WARN: Can't wrap try/catch for region: R(11:1|(2:3|(9:5|6|7|(1:(1:(7:11|12|13|(4:16|(3:21|22|(2:24|25)(1:27))|28|14)|35|36|37)(2:39|40))(2:41|42))(3:56|57|(4:59|(4:51|13|(1:14)|35)|36|37)(2:60|(1:62)(1:63)))|43|(1:55)|(1:47)(5:48|51|13|(1:14)|35)|36|37))|65|6|7|(0)(0)|43|(1:45)(2:52|55)|(0)(0)|36|37) */
    /* JADX WARN: Removed duplicated region for block: B:10:0x0025  */
    /* JADX WARN: Removed duplicated region for block: B:20:0x0052  */
    /* JADX WARN: Removed duplicated region for block: B:36:0x008b A[ADDED_TO_REGION] */
    /* JADX WARN: Removed duplicated region for block: B:37:0x008c A[Catch: Exception -> 0x00e1, TryCatch #0 {Exception -> 0x00e1, blocks: (B:13:0x0039, B:41:0x009b, B:43:0x00a1, B:46:0x00aa, B:49:0x00ca, B:18:0x004e, B:28:0x0075, B:37:0x008c, B:40:0x0093, B:31:0x007a, B:34:0x0083, B:21:0x0055, B:24:0x0067), top: B:55:0x0023 }] */
    /* JADX WARN: Removed duplicated region for block: B:43:0x00a1 A[Catch: Exception -> 0x00e1, TryCatch #0 {Exception -> 0x00e1, blocks: (B:13:0x0039, B:41:0x009b, B:43:0x00a1, B:46:0x00aa, B:49:0x00ca, B:18:0x004e, B:28:0x0075, B:37:0x008c, B:40:0x0093, B:31:0x007a, B:34:0x0083, B:21:0x0055, B:24:0x0067), top: B:55:0x0023 }] */
    /*
        Code decompiled incorrectly, please refer to instructions dump.
        To view partially-correct code enable 'Show inconsistent code' option in preferences
    */
    public final java.lang.Object l(java.lang.Integer r8, java.lang.String r9, defpackage.q70<? super defpackage.te4> r10) {
        /*
            Method dump skipped, instructions count: 228
            To view this dump change 'Code comments level' option to 'DEBUG'
        */
        throw new UnsupportedOperationException("Method not decompiled: net.safemoon.androidwallet.utils.ReflectionCustomContract.l(java.lang.Integer, java.lang.String, q70):java.lang.Object");
    }
}
