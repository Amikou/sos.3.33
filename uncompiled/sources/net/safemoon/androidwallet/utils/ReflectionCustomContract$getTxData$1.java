package net.safemoon.androidwallet.utils;

import kotlin.coroutines.jvm.internal.ContinuationImpl;
import kotlin.coroutines.jvm.internal.a;

/* compiled from: ReflectionCustomContract.kt */
@a(c = "net.safemoon.androidwallet.utils.ReflectionCustomContract", f = "ReflectionCustomContract.kt", l = {174}, m = "getTxData")
/* loaded from: classes2.dex */
public final class ReflectionCustomContract$getTxData$1 extends ContinuationImpl {
    public int label;
    public /* synthetic */ Object result;
    public final /* synthetic */ ReflectionCustomContract this$0;

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public ReflectionCustomContract$getTxData$1(ReflectionCustomContract reflectionCustomContract, q70<? super ReflectionCustomContract$getTxData$1> q70Var) {
        super(q70Var);
        this.this$0 = reflectionCustomContract;
    }

    @Override // kotlin.coroutines.jvm.internal.BaseContinuationImpl
    public final Object invokeSuspend(Object obj) {
        this.result = obj;
        this.label |= Integer.MIN_VALUE;
        return this.this$0.g(null, null, this);
    }
}
