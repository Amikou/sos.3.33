package net.safemoon.androidwallet.utils;

import android.app.Application;
import java.util.ArrayList;
import java.util.Iterator;
import kotlin.coroutines.jvm.internal.SuspendLambda;
import kotlin.coroutines.jvm.internal.a;
import kotlin.jvm.internal.Lambda;

/* compiled from: PreFetchData.kt */
@a(c = "net.safemoon.androidwallet.utils.PreFetchData$loadPreFetchData$1", f = "PreFetchData.kt", l = {36}, m = "invokeSuspend")
/* loaded from: classes2.dex */
public final class PreFetchData$loadPreFetchData$1 extends SuspendLambda implements hd1<c90, q70<? super te4>, Object> {
    public final /* synthetic */ Application $application;
    public int label;

    /* compiled from: PreFetchData.kt */
    /* renamed from: net.safemoon.androidwallet.utils.PreFetchData$loadPreFetchData$1$1  reason: invalid class name */
    /* loaded from: classes2.dex */
    public static final class AnonymousClass1 extends Lambda implements tc1<ArrayList<rt>, te4> {
        public static final AnonymousClass1 INSTANCE = new AnonymousClass1();

        public AnonymousClass1() {
            super(1);
        }

        /* renamed from: invoke  reason: avoid collision after fix types in other method */
        public final void invoke2(ArrayList<rt> arrayList) {
            fs1.f(arrayList, "it");
            PreFetchData.a.a().addAll(arrayList);
        }

        @Override // defpackage.tc1
        public /* bridge */ /* synthetic */ te4 invoke(ArrayList<rt> arrayList) {
            invoke2(arrayList);
            return te4.a;
        }
    }

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public PreFetchData$loadPreFetchData$1(Application application, q70<? super PreFetchData$loadPreFetchData$1> q70Var) {
        super(2, q70Var);
        this.$application = application;
    }

    @Override // kotlin.coroutines.jvm.internal.BaseContinuationImpl
    public final q70<te4> create(Object obj, q70<?> q70Var) {
        return new PreFetchData$loadPreFetchData$1(this.$application, q70Var);
    }

    @Override // defpackage.hd1
    public final Object invoke(c90 c90Var, q70<? super te4> q70Var) {
        return ((PreFetchData$loadPreFetchData$1) create(c90Var, q70Var)).invokeSuspend(te4.a);
    }

    @Override // kotlin.coroutines.jvm.internal.BaseContinuationImpl
    public final Object invokeSuspend(Object obj) {
        Object d = gs1.d();
        int i = this.label;
        if (i == 0) {
            o83.b(obj);
            PreFetch preFetch = new PreFetch(this.$application, AnonymousClass1.INSTANCE, AnonymousClass2.INSTANCE);
            this.label = 1;
            if (preFetch.h(this) == d) {
                return d;
            }
        } else if (i != 1) {
            throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        } else {
            o83.b(obj);
        }
        return te4.a;
    }

    /* compiled from: PreFetchData.kt */
    /* renamed from: net.safemoon.androidwallet.utils.PreFetchData$loadPreFetchData$1$2  reason: invalid class name */
    /* loaded from: classes2.dex */
    public static final class AnonymousClass2 extends Lambda implements tc1<rt, te4> {
        public static final AnonymousClass2 INSTANCE = new AnonymousClass2();

        public AnonymousClass2() {
            super(1);
        }

        /* renamed from: invoke  reason: avoid collision after fix types in other method */
        public final void invoke2(rt rtVar) {
            fs1.f(rtVar, "ct");
            Iterator<rt> it = PreFetchData.a.a().iterator();
            fs1.e(it, "swapListMap.iterator()");
            while (it.hasNext()) {
                rt next = it.next();
                fs1.e(next, "iterator.next()");
                rt rtVar2 = next;
                if (fs1.b(rtVar2.a(), rtVar.a()) && fs1.b(rtVar2.h(), rtVar.h())) {
                    rtVar2.i(rtVar.g());
                }
            }
        }

        @Override // defpackage.tc1
        public /* bridge */ /* synthetic */ te4 invoke(rt rtVar) {
            invoke2(rtVar);
            return te4.a;
        }
    }
}
