package net.safemoon.androidwallet.utils;

import android.app.Application;
import defpackage.st1;
import java.util.ArrayList;

/* compiled from: PreFetchData.kt */
/* loaded from: classes2.dex */
public final class PreFetchData {
    public static final PreFetchData a = new PreFetchData();
    public static final ArrayList<rt> b = new ArrayList<>();
    public static st1 c;

    public final ArrayList<rt> a() {
        return b;
    }

    public final void b(Application application) {
        st1 b2;
        fs1.f(application, "application");
        st1 st1Var = c;
        if (st1Var != null) {
            st1.a.a(st1Var, null, 1, null);
        }
        b.clear();
        b2 = as.b(qg1.a, null, null, new PreFetchData$loadPreFetchData$1(application, null), 3, null);
        c = b2;
    }
}
