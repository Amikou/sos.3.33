package net.safemoon.androidwallet.utils;

import android.view.View;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.a;
import net.safemoon.androidwallet.R;
import net.safemoon.androidwallet.utils.StoragePermissionLauncher;

/* compiled from: StoragePermissionLauncher.kt */
/* loaded from: classes2.dex */
public final class StoragePermissionLauncher {
    public final AppCompatActivity a;
    public rc1<te4> b;
    public final w7<String> c;

    public StoragePermissionLauncher(AppCompatActivity appCompatActivity) {
        fs1.f(appCompatActivity, "activityCompat");
        this.a = appCompatActivity;
        w7<String> registerForActivityResult = appCompatActivity.registerForActivityResult(new u7(), new r7() { // from class: zt3
            @Override // defpackage.r7
            public final void a(Object obj) {
                StoragePermissionLauncher.d(StoragePermissionLauncher.this, ((Boolean) obj).booleanValue());
            }
        });
        fs1.e(registerForActivityResult, "activityCompat.registerF…)\n            }\n        }");
        this.c = registerForActivityResult;
    }

    public static final void d(StoragePermissionLauncher storagePermissionLauncher, boolean z) {
        fs1.f(storagePermissionLauncher, "this$0");
        if (z) {
            rc1<te4> rc1Var = storagePermissionLauncher.b;
            if (rc1Var == null) {
                fs1.r("onCameraPermission");
                rc1Var = null;
            }
            rc1Var.invoke();
        }
    }

    public final void c(View view, rc1<te4> rc1Var) {
        fs1.f(view, "view");
        fs1.f(rc1Var, "onCameraPermission");
        this.b = rc1Var;
        if (m70.a(this.a, "android.permission.READ_EXTERNAL_STORAGE") == 0) {
            rc1Var.invoke();
        } else if (a.u(this.a, "android.permission.READ_EXTERNAL_STORAGE")) {
            String string = this.a.getString(R.string.permission_required);
            fs1.e(string, "activityCompat.getString…ring.permission_required)");
            e30.d0(view, view, string, -2, this.a.getString(R.string.action_ok), new StoragePermissionLauncher$onClickRequestPermission$1(this));
        } else {
            this.c.a("android.permission.READ_EXTERNAL_STORAGE");
        }
    }
}
