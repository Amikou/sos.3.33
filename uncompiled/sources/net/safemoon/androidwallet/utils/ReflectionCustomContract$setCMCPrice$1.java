package net.safemoon.androidwallet.utils;

import kotlin.coroutines.jvm.internal.ContinuationImpl;
import kotlin.coroutines.jvm.internal.a;

/* compiled from: ReflectionCustomContract.kt */
@a(c = "net.safemoon.androidwallet.utils.ReflectionCustomContract", f = "ReflectionCustomContract.kt", l = {134, 139}, m = "setCMCPrice")
/* loaded from: classes2.dex */
public final class ReflectionCustomContract$setCMCPrice$1 extends ContinuationImpl {
    public Object L$0;
    public Object L$1;
    public Object L$2;
    public Object L$3;
    public int label;
    public /* synthetic */ Object result;
    public final /* synthetic */ ReflectionCustomContract this$0;

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public ReflectionCustomContract$setCMCPrice$1(ReflectionCustomContract reflectionCustomContract, q70<? super ReflectionCustomContract$setCMCPrice$1> q70Var) {
        super(q70Var);
        this.this$0 = reflectionCustomContract;
    }

    @Override // kotlin.coroutines.jvm.internal.BaseContinuationImpl
    public final Object invokeSuspend(Object obj) {
        Object l;
        this.result = obj;
        this.label |= Integer.MIN_VALUE;
        l = this.this$0.l(null, null, this);
        return l;
    }
}
