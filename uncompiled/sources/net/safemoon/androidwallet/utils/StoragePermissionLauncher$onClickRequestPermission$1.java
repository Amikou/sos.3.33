package net.safemoon.androidwallet.utils;

import android.view.View;
import kotlin.jvm.internal.Lambda;

/* compiled from: StoragePermissionLauncher.kt */
/* loaded from: classes2.dex */
public final class StoragePermissionLauncher$onClickRequestPermission$1 extends Lambda implements tc1<View, te4> {
    public final /* synthetic */ StoragePermissionLauncher this$0;

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public StoragePermissionLauncher$onClickRequestPermission$1(StoragePermissionLauncher storagePermissionLauncher) {
        super(1);
        this.this$0 = storagePermissionLauncher;
    }

    @Override // defpackage.tc1
    public /* bridge */ /* synthetic */ te4 invoke(View view) {
        invoke2(view);
        return te4.a;
    }

    /* renamed from: invoke  reason: avoid collision after fix types in other method */
    public final void invoke2(View view) {
        w7 w7Var;
        fs1.f(view, "it");
        w7Var = this.this$0.c;
        w7Var.a("android.permission.READ_EXTERNAL_STORAGE");
    }
}
