package net.safemoon.androidwallet.utils;

import kotlin.coroutines.jvm.internal.ContinuationImpl;
import kotlin.coroutines.jvm.internal.a;

/* compiled from: PreFetchData.kt */
@a(c = "net.safemoon.androidwallet.utils.PreFetch", f = "PreFetchData.kt", l = {48, 49, 51}, m = "loadPreFetchData")
/* loaded from: classes2.dex */
public final class PreFetch$loadPreFetchData$1 extends ContinuationImpl {
    public Object L$0;
    public int label;
    public /* synthetic */ Object result;
    public final /* synthetic */ PreFetch this$0;

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public PreFetch$loadPreFetchData$1(PreFetch preFetch, q70<? super PreFetch$loadPreFetchData$1> q70Var) {
        super(q70Var);
        this.this$0 = preFetch;
    }

    @Override // kotlin.coroutines.jvm.internal.BaseContinuationImpl
    public final Object invokeSuspend(Object obj) {
        this.result = obj;
        this.label |= Integer.MIN_VALUE;
        return this.this$0.h(this);
    }
}
