package net.safemoon.androidwallet.utils;

import android.view.View;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.a;
import net.safemoon.androidwallet.R;
import net.safemoon.androidwallet.utils.CameraPermissionLauncher;

/* compiled from: CameraPermissionLauncher.kt */
/* loaded from: classes2.dex */
public final class CameraPermissionLauncher {
    public final AppCompatActivity a;
    public final View b;
    public rc1<Void> c;
    public final w7<String> d;

    public CameraPermissionLauncher(AppCompatActivity appCompatActivity, View view) {
        fs1.f(appCompatActivity, "activityCompat");
        fs1.f(view, "layout");
        this.a = appCompatActivity;
        this.b = view;
        w7<String> registerForActivityResult = appCompatActivity.registerForActivityResult(new u7(), new r7() { // from class: fv
            @Override // defpackage.r7
            public final void a(Object obj) {
                CameraPermissionLauncher.d(CameraPermissionLauncher.this, ((Boolean) obj).booleanValue());
            }
        });
        fs1.e(registerForActivityResult, "activityCompat.registerF…)\n            }\n        }");
        this.d = registerForActivityResult;
    }

    public static final void d(CameraPermissionLauncher cameraPermissionLauncher, boolean z) {
        fs1.f(cameraPermissionLauncher, "this$0");
        if (z) {
            rc1<Void> rc1Var = cameraPermissionLauncher.c;
            if (rc1Var == null) {
                fs1.r("onCameraPermission");
                rc1Var = null;
            }
            rc1Var.invoke();
        }
    }

    public final void c(View view, rc1<Void> rc1Var) {
        fs1.f(view, "view");
        fs1.f(rc1Var, "onCameraPermission");
        this.c = rc1Var;
        if (m70.a(this.a, "android.permission.CAMERA") == 0) {
            rc1Var.invoke();
        } else if (a.u(this.a, "android.permission.CAMERA")) {
            View view2 = this.b;
            String string = this.a.getString(R.string.permission_required);
            fs1.e(string, "activityCompat.getString…ring.permission_required)");
            e30.d0(view2, view, string, -2, this.a.getString(R.string.action_ok), new CameraPermissionLauncher$onClickRequestPermission$1(this));
        } else {
            this.d.a("android.permission.CAMERA");
        }
    }
}
