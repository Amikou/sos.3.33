package net.safemoon.androidwallet.utils.svg;

import android.graphics.Bitmap;
import com.bumptech.glide.Priority;
import com.bumptech.glide.load.resource.bitmap.DownsampleStrategy;

/* compiled from: GlideOptions.java */
/* loaded from: classes2.dex */
public final class a extends n73 {
    @Override // com.bumptech.glide.request.a
    /* renamed from: B0 */
    public a a(com.bumptech.glide.request.a<?> aVar) {
        return (a) super.a(aVar);
    }

    @Override // com.bumptech.glide.request.a
    /* renamed from: C0 */
    public a b() {
        return (a) super.b();
    }

    @Override // com.bumptech.glide.request.a
    /* renamed from: D0 */
    public a f() {
        return (a) super.f();
    }

    @Override // com.bumptech.glide.request.a
    /* renamed from: E0 */
    public a g() {
        return (a) super.clone();
    }

    @Override // com.bumptech.glide.request.a
    /* renamed from: G0 */
    public a h(Class<?> cls) {
        return (a) super.h(cls);
    }

    @Override // com.bumptech.glide.request.a
    /* renamed from: I0 */
    public a j(bp0 bp0Var) {
        return (a) super.j(bp0Var);
    }

    @Override // com.bumptech.glide.request.a
    /* renamed from: K0 */
    public a k(DownsampleStrategy downsampleStrategy) {
        return (a) super.k(downsampleStrategy);
    }

    @Override // com.bumptech.glide.request.a
    /* renamed from: L0 */
    public a l(int i) {
        return (a) super.l(i);
    }

    @Override // com.bumptech.glide.request.a
    /* renamed from: M0 */
    public a X() {
        return (a) super.X();
    }

    @Override // com.bumptech.glide.request.a
    /* renamed from: N0 */
    public a Y() {
        return (a) super.Y();
    }

    @Override // com.bumptech.glide.request.a
    /* renamed from: O0 */
    public a Z() {
        return (a) super.Z();
    }

    @Override // com.bumptech.glide.request.a
    /* renamed from: P0 */
    public a a0() {
        return (a) super.a0();
    }

    @Override // com.bumptech.glide.request.a
    /* renamed from: Q0 */
    public a d0(int i, int i2) {
        return (a) super.d0(i, i2);
    }

    @Override // com.bumptech.glide.request.a
    /* renamed from: R0 */
    public a e0(int i) {
        return (a) super.e0(i);
    }

    @Override // com.bumptech.glide.request.a
    /* renamed from: S0 */
    public a f0(Priority priority) {
        return (a) super.f0(priority);
    }

    @Override // com.bumptech.glide.request.a
    /* renamed from: T0 */
    public <Y> a l0(mn2<Y> mn2Var, Y y) {
        return (a) super.l0(mn2Var, y);
    }

    @Override // com.bumptech.glide.request.a
    /* renamed from: U0 */
    public a m0(fx1 fx1Var) {
        return (a) super.m0(fx1Var);
    }

    @Override // com.bumptech.glide.request.a
    /* renamed from: V0 */
    public a n0(float f) {
        return (a) super.n0(f);
    }

    @Override // com.bumptech.glide.request.a
    /* renamed from: W0 */
    public a o0(boolean z) {
        return (a) super.o0(z);
    }

    @Override // com.bumptech.glide.request.a
    /* renamed from: X0 */
    public a p0(za4<Bitmap> za4Var) {
        return (a) super.p0(za4Var);
    }

    @Override // com.bumptech.glide.request.a
    /* renamed from: Y0 */
    public a t0(boolean z) {
        return (a) super.t0(z);
    }
}
