package net.safemoon.androidwallet.utils.svg;

import android.content.Context;
import android.graphics.Bitmap;
import android.net.Uri;
import com.bumptech.glide.Priority;
import com.bumptech.glide.e;
import com.bumptech.glide.load.resource.bitmap.DownsampleStrategy;
import java.io.File;

/* compiled from: GlideRequest.java */
/* loaded from: classes2.dex */
public class b<TranscodeType> extends e<TranscodeType> {
    public b(com.bumptech.glide.a aVar, k73 k73Var, Class<TranscodeType> cls, Context context) {
        super(aVar, k73Var, cls, context);
    }

    @Override // com.bumptech.glide.request.a
    /* renamed from: A1 */
    public b<TranscodeType> t0(boolean z) {
        return (b) super.t0(z);
    }

    @Override // com.bumptech.glide.e
    /* renamed from: X0 */
    public b<TranscodeType> u0(j73<TranscodeType> j73Var) {
        return (b) super.u0(j73Var);
    }

    @Override // com.bumptech.glide.e
    /* renamed from: Y0 */
    public b<TranscodeType> v0(com.bumptech.glide.request.a<?> aVar) {
        return (b) super.a(aVar);
    }

    @Override // com.bumptech.glide.request.a
    /* renamed from: Z0 */
    public b<TranscodeType> d() {
        return (b) super.d();
    }

    @Override // com.bumptech.glide.request.a
    /* renamed from: a1 */
    public b<TranscodeType> e() {
        return (b) super.e();
    }

    @Override // com.bumptech.glide.e, com.bumptech.glide.request.a
    /* renamed from: b1 */
    public b<TranscodeType> g() {
        return (b) super.g();
    }

    @Override // com.bumptech.glide.request.a
    /* renamed from: c1 */
    public b<TranscodeType> h(Class<?> cls) {
        return (b) super.h(cls);
    }

    @Override // com.bumptech.glide.request.a
    /* renamed from: d1 */
    public b<TranscodeType> j(bp0 bp0Var) {
        return (b) super.j(bp0Var);
    }

    @Override // com.bumptech.glide.request.a
    /* renamed from: e1 */
    public b<TranscodeType> k(DownsampleStrategy downsampleStrategy) {
        return (b) super.k(downsampleStrategy);
    }

    @Override // com.bumptech.glide.request.a
    /* renamed from: f1 */
    public b<TranscodeType> l(int i) {
        return (b) super.l(i);
    }

    @Override // com.bumptech.glide.e
    /* renamed from: g1 */
    public b<TranscodeType> L0(j73<TranscodeType> j73Var) {
        return (b) super.L0(j73Var);
    }

    @Override // com.bumptech.glide.e
    /* renamed from: h1 */
    public b<TranscodeType> M0(Bitmap bitmap) {
        return (b) super.M0(bitmap);
    }

    @Override // com.bumptech.glide.e
    /* renamed from: i1 */
    public b<TranscodeType> N0(Uri uri) {
        return (b) super.N0(uri);
    }

    @Override // com.bumptech.glide.e
    /* renamed from: j1 */
    public b<TranscodeType> O0(File file) {
        return (b) super.O0(file);
    }

    @Override // com.bumptech.glide.e
    /* renamed from: k1 */
    public b<TranscodeType> P0(Integer num) {
        return (b) super.P0(num);
    }

    @Override // com.bumptech.glide.e
    /* renamed from: l1 */
    public b<TranscodeType> Q0(Object obj) {
        return (b) super.Q0(obj);
    }

    @Override // com.bumptech.glide.e
    /* renamed from: m1 */
    public b<TranscodeType> R0(String str) {
        return (b) super.R0(str);
    }

    @Override // com.bumptech.glide.e
    /* renamed from: n1 */
    public b<TranscodeType> S0(byte[] bArr) {
        return (b) super.S0(bArr);
    }

    @Override // com.bumptech.glide.request.a
    /* renamed from: o1 */
    public b<TranscodeType> X() {
        return (b) super.X();
    }

    @Override // com.bumptech.glide.request.a
    /* renamed from: p1 */
    public b<TranscodeType> Y() {
        return (b) super.Y();
    }

    @Override // com.bumptech.glide.request.a
    /* renamed from: q1 */
    public b<TranscodeType> Z() {
        return (b) super.Z();
    }

    @Override // com.bumptech.glide.request.a
    /* renamed from: r1 */
    public b<TranscodeType> a0() {
        return (b) super.a0();
    }

    @Override // com.bumptech.glide.request.a
    /* renamed from: s1 */
    public b<TranscodeType> d0(int i, int i2) {
        return (b) super.d0(i, i2);
    }

    @Override // com.bumptech.glide.request.a
    /* renamed from: t1 */
    public b<TranscodeType> e0(int i) {
        return (b) super.e0(i);
    }

    @Override // com.bumptech.glide.request.a
    /* renamed from: u1 */
    public b<TranscodeType> f0(Priority priority) {
        return (b) super.f0(priority);
    }

    @Override // com.bumptech.glide.request.a
    /* renamed from: v1 */
    public <Y> b<TranscodeType> l0(mn2<Y> mn2Var, Y y) {
        return (b) super.l0(mn2Var, y);
    }

    @Override // com.bumptech.glide.request.a
    /* renamed from: w1 */
    public b<TranscodeType> m0(fx1 fx1Var) {
        return (b) super.m0(fx1Var);
    }

    @Override // com.bumptech.glide.request.a
    /* renamed from: x1 */
    public b<TranscodeType> n0(float f) {
        return (b) super.n0(f);
    }

    @Override // com.bumptech.glide.request.a
    /* renamed from: y1 */
    public b<TranscodeType> o0(boolean z) {
        return (b) super.o0(z);
    }

    @Override // com.bumptech.glide.request.a
    /* renamed from: z1 */
    public b<TranscodeType> p0(za4<Bitmap> za4Var) {
        return (b) super.p0(za4Var);
    }
}
