package net.safemoon.androidwallet.utils.svg;

import com.caverock.androidsvg.SVG;
import com.caverock.androidsvg.SVGParseException;
import java.io.IOException;
import java.io.InputStream;

/* compiled from: SvgDecoder.kt */
/* loaded from: classes2.dex */
public final class c implements com.bumptech.glide.load.b<InputStream, SVG> {
    @Override // com.bumptech.glide.load.b
    /* renamed from: c */
    public s73<SVG> b(InputStream inputStream, int i, int i2, vn2 vn2Var) throws IOException {
        fs1.f(inputStream, "source");
        fs1.f(vn2Var, "options");
        try {
            SVG h = SVG.h(inputStream);
            h.t(i);
            h.s(i2);
            return new op3(h);
        } catch (SVGParseException e) {
            throw new IOException("Cannot load SVG from stream", e);
        }
    }

    @Override // com.bumptech.glide.load.b
    /* renamed from: d */
    public boolean a(InputStream inputStream, vn2 vn2Var) {
        fs1.f(inputStream, "source");
        fs1.f(vn2Var, "options");
        return true;
    }
}
