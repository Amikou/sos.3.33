package net.safemoon.androidwallet.utils.svg;

import android.content.Context;
import android.graphics.drawable.PictureDrawable;
import com.bumptech.glide.Registry;
import com.caverock.androidsvg.SVG;
import java.io.InputStream;

/* compiled from: SvgModule.kt */
/* loaded from: classes2.dex */
public final class SvgModule extends pf {
    @Override // defpackage.gz1, defpackage.h63
    public void b(Context context, com.bumptech.glide.a aVar, Registry registry) {
        fs1.f(context, "context");
        fs1.f(aVar, "glide");
        fs1.f(registry, "registry");
        registry.q(SVG.class, PictureDrawable.class, new yw3()).d(InputStream.class, SVG.class, new c());
    }

    @Override // defpackage.pf
    public boolean c() {
        return false;
    }
}
