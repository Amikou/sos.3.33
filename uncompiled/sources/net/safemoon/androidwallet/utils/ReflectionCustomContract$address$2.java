package net.safemoon.androidwallet.utils;

import kotlin.jvm.internal.Lambda;

/* compiled from: ReflectionCustomContract.kt */
/* loaded from: classes2.dex */
public final class ReflectionCustomContract$address$2 extends Lambda implements rc1<String> {
    public final /* synthetic */ ReflectionCustomContract this$0;

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public ReflectionCustomContract$address$2(ReflectionCustomContract reflectionCustomContract) {
        super(0);
        this.this$0 = reflectionCustomContract;
    }

    @Override // defpackage.rc1
    public final String invoke() {
        return bo3.i(this.this$0.c(), "SAFEMOON_ADDRESS");
    }
}
