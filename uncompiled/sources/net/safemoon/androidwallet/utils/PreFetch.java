package net.safemoon.androidwallet.utils;

import android.app.Application;
import java.util.ArrayList;
import java.util.Comparator;
import net.safemoon.androidwallet.common.TokenType;

/* compiled from: PreFetchData.kt */
/* loaded from: classes2.dex */
public final class PreFetch {
    public final Application a;
    public final tc1<ArrayList<rt>, te4> b;
    public final tc1<rt, te4> c;
    public final ArrayList<rt> d;
    public final TokenType e;

    /* compiled from: Comparisons.kt */
    /* loaded from: classes2.dex */
    public static final class a<T> implements Comparator {
        public a() {
        }

        @Override // java.util.Comparator
        public final int compare(T t, T t2) {
            return l30.a(Boolean.valueOf(PreFetch.this.d().getChainId() == ((rt) t2).b()), Boolean.valueOf(PreFetch.this.d().getChainId() == ((rt) t).b()));
        }
    }

    /* JADX WARN: Multi-variable type inference failed */
    public PreFetch(Application application, tc1<? super ArrayList<rt>, te4> tc1Var, tc1<? super rt, te4> tc1Var2) {
        fs1.f(application, "application");
        fs1.f(tc1Var, "callbackList");
        fs1.f(tc1Var2, "callback");
        this.a = application;
        this.b = tc1Var;
        this.c = tc1Var2;
        this.d = new ArrayList<>();
        this.e = e30.e(application);
    }

    public final Application a() {
        return this.a;
    }

    public final tc1<rt, te4> b() {
        return this.c;
    }

    public final tc1<ArrayList<rt>, te4> c() {
        return this.b;
    }

    public final TokenType d() {
        return this.e;
    }

    public final ArrayList<rt> e() {
        return this.d;
    }

    /* JADX WARN: Removed duplicated region for block: B:24:0x0049 A[SYNTHETIC] */
    /* JADX WARN: Removed duplicated region for block: B:26:0x002c A[SYNTHETIC] */
    /*
        Code decompiled incorrectly, please refer to instructions dump.
        To view partially-correct code enable 'Show inconsistent code' option in preferences
    */
    public final java.lang.Object f(defpackage.q70<? super defpackage.te4> r17) {
        /*
            r16 = this;
            b30 r0 = defpackage.b30.a
            java.util.List r0 = r0.a()
            java.util.Iterator r0 = r0.iterator()
        La:
            boolean r1 = r0.hasNext()
            if (r1 == 0) goto L96
            java.lang.Object r1 = r0.next()
            net.safemoon.androidwallet.common.TokenType r1 = (net.safemoon.androidwallet.common.TokenType) r1
            ni r2 = new ni
            android.app.Application r3 = r16.a()
            r2.<init>(r3)
            java.util.List r1 = r2.c(r1)
            java.util.ArrayList r2 = new java.util.ArrayList
            r2.<init>()
            java.util.Iterator r1 = r1.iterator()
        L2c:
            boolean r3 = r1.hasNext()
            if (r3 == 0) goto L4d
            java.lang.Object r3 = r1.next()
            r4 = r3
            net.safemoon.androidwallet.model.token.gson.GsonToken r4 = (net.safemoon.androidwallet.model.token.gson.GsonToken) r4
            if (r4 == 0) goto L46
            r4.getCmcId()
            java.lang.String r4 = r4.getCmcSlug()
            if (r4 == 0) goto L46
            r4 = 1
            goto L47
        L46:
            r4 = 0
        L47:
            if (r4 == 0) goto L2c
            r2.add(r3)
            goto L2c
        L4d:
            java.util.Iterator r1 = r2.iterator()
        L51:
            boolean r2 = r1.hasNext()
            if (r2 == 0) goto La
            java.lang.Object r2 = r1.next()
            net.safemoon.androidwallet.model.token.gson.GsonToken r2 = (net.safemoon.androidwallet.model.token.gson.GsonToken) r2
            java.util.ArrayList r3 = r16.e()
            rt r15 = new rt
            java.lang.String r5 = r2.getContractAddress()
            int r4 = r2.getCmcId()
            java.lang.String r6 = java.lang.String.valueOf(r4)
            java.lang.String r7 = r2.getCmcSlug()
            java.lang.String r8 = r2.getSymbol()
            int r9 = r2.getDecimals()
            int r10 = r2.getChainId()
            r11 = 0
            boolean r13 = r2.getAllowSwap()
            r14 = 64
            r2 = 0
            r4 = r15
            r17 = r0
            r0 = r15
            r15 = r2
            r4.<init>(r5, r6, r7, r8, r9, r10, r11, r13, r14, r15)
            r3.add(r0)
            r0 = r17
            goto L51
        L96:
            te4 r0 = defpackage.te4.a
            return r0
        */
        throw new UnsupportedOperationException("Method not decompiled: net.safemoon.androidwallet.utils.PreFetch.f(q70):java.lang.Object");
    }

    /* JADX WARN: Removed duplicated region for block: B:10:0x0029  */
    /* JADX WARN: Removed duplicated region for block: B:16:0x0065  */
    /* JADX WARN: Removed duplicated region for block: B:19:0x008e  */
    /* JADX WARN: Removed duplicated region for block: B:25:0x00be  */
    /* JADX WARN: Removed duplicated region for block: B:26:0x00ce  */
    /* JADX WARN: Removed duplicated region for block: B:46:0x014e  */
    /* JADX WARN: Unsupported multi-entry loop pattern (BACK_EDGE: B:25:0x00be -> B:45:0x0148). Please submit an issue!!! */
    /* JADX WARN: Unsupported multi-entry loop pattern (BACK_EDGE: B:27:0x00d2 -> B:45:0x0148). Please submit an issue!!! */
    /* JADX WARN: Unsupported multi-entry loop pattern (BACK_EDGE: B:32:0x00e1 -> B:45:0x0148). Please submit an issue!!! */
    /* JADX WARN: Unsupported multi-entry loop pattern (BACK_EDGE: B:36:0x0127 -> B:37:0x0128). Please submit an issue!!! */
    /*
        Code decompiled incorrectly, please refer to instructions dump.
        To view partially-correct code enable 'Show inconsistent code' option in preferences
    */
    public final java.lang.Object g(defpackage.q70<? super defpackage.te4> r32) {
        /*
            Method dump skipped, instructions count: 337
            To view this dump change 'Code comments level' option to 'DEBUG'
        */
        throw new UnsupportedOperationException("Method not decompiled: net.safemoon.androidwallet.utils.PreFetch.g(q70):java.lang.Object");
    }

    /* JADX WARN: Removed duplicated region for block: B:10:0x0025  */
    /* JADX WARN: Removed duplicated region for block: B:18:0x0047  */
    /* JADX WARN: Removed duplicated region for block: B:24:0x0067 A[RETURN] */
    /* JADX WARN: Removed duplicated region for block: B:27:0x007e A[RETURN] */
    /*
        Code decompiled incorrectly, please refer to instructions dump.
        To view partially-correct code enable 'Show inconsistent code' option in preferences
    */
    public final java.lang.Object h(defpackage.q70<? super defpackage.te4> r7) {
        /*
            r6 = this;
            boolean r0 = r7 instanceof net.safemoon.androidwallet.utils.PreFetch$loadPreFetchData$1
            if (r0 == 0) goto L13
            r0 = r7
            net.safemoon.androidwallet.utils.PreFetch$loadPreFetchData$1 r0 = (net.safemoon.androidwallet.utils.PreFetch$loadPreFetchData$1) r0
            int r1 = r0.label
            r2 = -2147483648(0xffffffff80000000, float:-0.0)
            r3 = r1 & r2
            if (r3 == 0) goto L13
            int r1 = r1 - r2
            r0.label = r1
            goto L18
        L13:
            net.safemoon.androidwallet.utils.PreFetch$loadPreFetchData$1 r0 = new net.safemoon.androidwallet.utils.PreFetch$loadPreFetchData$1
            r0.<init>(r6, r7)
        L18:
            java.lang.Object r7 = r0.result
            java.lang.Object r1 = defpackage.gs1.d()
            int r2 = r0.label
            r3 = 3
            r4 = 2
            r5 = 1
            if (r2 == 0) goto L47
            if (r2 == r5) goto L3f
            if (r2 == r4) goto L37
            if (r2 != r3) goto L2f
            defpackage.o83.b(r7)
            goto L7f
        L2f:
            java.lang.IllegalStateException r7 = new java.lang.IllegalStateException
            java.lang.String r0 = "call to 'resume' before 'invoke' with coroutine"
            r7.<init>(r0)
            throw r7
        L37:
            java.lang.Object r2 = r0.L$0
            net.safemoon.androidwallet.utils.PreFetch r2 = (net.safemoon.androidwallet.utils.PreFetch) r2
            defpackage.o83.b(r7)
            goto L68
        L3f:
            java.lang.Object r2 = r0.L$0
            net.safemoon.androidwallet.utils.PreFetch r2 = (net.safemoon.androidwallet.utils.PreFetch) r2
            defpackage.o83.b(r7)
            goto L5d
        L47:
            defpackage.o83.b(r7)
            java.util.ArrayList r7 = r6.e()
            r7.clear()
            r0.L$0 = r6
            r0.label = r5
            java.lang.Object r7 = r6.f(r0)
            if (r7 != r1) goto L5c
            return r1
        L5c:
            r2 = r6
        L5d:
            r0.L$0 = r2
            r0.label = r4
            java.lang.Object r7 = r2.i(r0)
            if (r7 != r1) goto L68
            return r1
        L68:
            tc1 r7 = r2.c()
            java.util.ArrayList r4 = r2.e()
            r7.invoke(r4)
            r7 = 0
            r0.L$0 = r7
            r0.label = r3
            java.lang.Object r7 = r2.g(r0)
            if (r7 != r1) goto L7f
            return r1
        L7f:
            te4 r7 = defpackage.te4.a
            return r7
        */
        throw new UnsupportedOperationException("Method not decompiled: net.safemoon.androidwallet.utils.PreFetch.h(q70):java.lang.Object");
    }

    /* JADX WARN: Can't wrap try/catch for region: R(10:1|(2:3|(8:5|6|7|(1:(1:(1:(1:(6:13|14|15|16|17|(2:19|(1:21)(8:23|(1:25)(1:58)|(4:27|(2:28|(2:30|(5:32|(2:34|(3:39|40|(2:42|43)))|48|49|(0))(3:50|51|52))(2:53|54))|44|(1:46)(1:47))|55|(1:57)|16|17|(3:59|60|61)(0)))(0))(2:62|63))(10:64|65|66|(0)(0)|(0)|55|(0)|16|17|(0)(0)))(8:67|68|69|(6:72|(2:73|(2:75|(4:77|(2:79|(2:84|(2:87|88)(1:86)))|97|(0)(0))(3:98|99|100))(2:101|102))|(1:90)(1:96)|(2:92|93)(1:95)|94|70)|103|104|17|(0)(0)))(2:105|106))(3:182|183|(1:185)(1:186))|107|(11:111|(11:172|(2:173|(2:175|(2:177|178))(2:180|181))|179|114|115|(4:123|(6:126|(1:128)(1:136)|129|(2:131|132)(2:134|135)|133|124)|137|138)|139|(6:142|(1:144)(1:152)|145|(2:147|148)(2:150|151)|149|140)|153|154|(8:163|(1:165)|69|(1:70)|103|104|17|(0)(0)))|113|114|115|(6:117|120|123|(1:124)|137|138)|139|(1:140)|153|154|(1:156)(10:157|160|163|(0)|69|(1:70)|103|104|17|(0)(0)))|60|61))|188|6|7|(0)(0)|107|(12:109|111|(13:166|169|172|(3:173|(0)(0)|176)|179|114|115|(0)|139|(1:140)|153|154|(0)(0))|113|114|115|(0)|139|(1:140)|153|154|(0)(0))|60|61) */
    /* JADX WARN: Removed duplicated region for block: B:100:0x0215 A[RETURN] */
    /* JADX WARN: Removed duplicated region for block: B:104:0x0227 A[Catch: Exception -> 0x034c, TryCatch #0 {Exception -> 0x034c, blocks: (B:15:0x0046, B:131:0x0296, B:133:0x029c, B:141:0x02c6, B:142:0x02cd, B:144:0x02d3, B:146:0x02f0, B:148:0x02fd, B:151:0x0306, B:160:0x0319, B:163:0x031e, B:157:0x0312, B:158:0x0317, B:164:0x032c, B:168:0x034a, B:20:0x0069, B:23:0x007e, B:101:0x0216, B:102:0x0221, B:104:0x0227, B:105:0x0232, B:107:0x0238, B:109:0x0257, B:111:0x0264, B:114:0x026d, B:128:0x028c, B:121:0x027c, B:122:0x0281, B:130:0x0292, B:26:0x0087, B:33:0x00a2, B:35:0x00aa, B:37:0x00b0, B:54:0x00e7, B:58:0x0100, B:61:0x0108, B:64:0x010f, B:65:0x0113, B:67:0x0119, B:71:0x0135, B:75:0x013e, B:76:0x0173, B:77:0x0175, B:78:0x017f, B:80:0x0185, B:84:0x019f, B:88:0x01a8, B:89:0x01d8, B:92:0x01e2, B:95:0x01ea, B:98:0x01f2, B:40:0x00ba, B:43:0x00c1, B:46:0x00c8, B:47:0x00cc, B:49:0x00d2, B:53:0x00e5, B:29:0x008e), top: B:172:0x002c }] */
    /* JADX WARN: Removed duplicated region for block: B:10:0x002e  */
    /* JADX WARN: Removed duplicated region for block: B:120:0x0279 A[LOOP:2: B:105:0x0232->B:120:0x0279, LOOP_END] */
    /* JADX WARN: Removed duplicated region for block: B:133:0x029c A[Catch: Exception -> 0x034c, TryCatch #0 {Exception -> 0x034c, blocks: (B:15:0x0046, B:131:0x0296, B:133:0x029c, B:141:0x02c6, B:142:0x02cd, B:144:0x02d3, B:146:0x02f0, B:148:0x02fd, B:151:0x0306, B:160:0x0319, B:163:0x031e, B:157:0x0312, B:158:0x0317, B:164:0x032c, B:168:0x034a, B:20:0x0069, B:23:0x007e, B:101:0x0216, B:102:0x0221, B:104:0x0227, B:105:0x0232, B:107:0x0238, B:109:0x0257, B:111:0x0264, B:114:0x026d, B:128:0x028c, B:121:0x027c, B:122:0x0281, B:130:0x0292, B:26:0x0087, B:33:0x00a2, B:35:0x00aa, B:37:0x00b0, B:54:0x00e7, B:58:0x0100, B:61:0x0108, B:64:0x010f, B:65:0x0113, B:67:0x0119, B:71:0x0135, B:75:0x013e, B:76:0x0173, B:77:0x0175, B:78:0x017f, B:80:0x0185, B:84:0x019f, B:88:0x01a8, B:89:0x01d8, B:92:0x01e2, B:95:0x01ea, B:98:0x01f2, B:40:0x00ba, B:43:0x00c1, B:46:0x00c8, B:47:0x00cc, B:49:0x00d2, B:53:0x00e5, B:29:0x008e), top: B:172:0x002c }] */
    /* JADX WARN: Removed duplicated region for block: B:138:0x02c1  */
    /* JADX WARN: Removed duplicated region for block: B:139:0x02c3  */
    /* JADX WARN: Removed duplicated region for block: B:141:0x02c6 A[Catch: Exception -> 0x034c, TryCatch #0 {Exception -> 0x034c, blocks: (B:15:0x0046, B:131:0x0296, B:133:0x029c, B:141:0x02c6, B:142:0x02cd, B:144:0x02d3, B:146:0x02f0, B:148:0x02fd, B:151:0x0306, B:160:0x0319, B:163:0x031e, B:157:0x0312, B:158:0x0317, B:164:0x032c, B:168:0x034a, B:20:0x0069, B:23:0x007e, B:101:0x0216, B:102:0x0221, B:104:0x0227, B:105:0x0232, B:107:0x0238, B:109:0x0257, B:111:0x0264, B:114:0x026d, B:128:0x028c, B:121:0x027c, B:122:0x0281, B:130:0x0292, B:26:0x0087, B:33:0x00a2, B:35:0x00aa, B:37:0x00b0, B:54:0x00e7, B:58:0x0100, B:61:0x0108, B:64:0x010f, B:65:0x0113, B:67:0x0119, B:71:0x0135, B:75:0x013e, B:76:0x0173, B:77:0x0175, B:78:0x017f, B:80:0x0185, B:84:0x019f, B:88:0x01a8, B:89:0x01d8, B:92:0x01e2, B:95:0x01ea, B:98:0x01f2, B:40:0x00ba, B:43:0x00c1, B:46:0x00c8, B:47:0x00cc, B:49:0x00d2, B:53:0x00e5, B:29:0x008e), top: B:172:0x002c }] */
    /* JADX WARN: Removed duplicated region for block: B:166:0x0344 A[RETURN] */
    /* JADX WARN: Removed duplicated region for block: B:168:0x034a A[Catch: Exception -> 0x034c, TRY_LEAVE, TryCatch #0 {Exception -> 0x034c, blocks: (B:15:0x0046, B:131:0x0296, B:133:0x029c, B:141:0x02c6, B:142:0x02cd, B:144:0x02d3, B:146:0x02f0, B:148:0x02fd, B:151:0x0306, B:160:0x0319, B:163:0x031e, B:157:0x0312, B:158:0x0317, B:164:0x032c, B:168:0x034a, B:20:0x0069, B:23:0x007e, B:101:0x0216, B:102:0x0221, B:104:0x0227, B:105:0x0232, B:107:0x0238, B:109:0x0257, B:111:0x0264, B:114:0x026d, B:128:0x028c, B:121:0x027c, B:122:0x0281, B:130:0x0292, B:26:0x0087, B:33:0x00a2, B:35:0x00aa, B:37:0x00b0, B:54:0x00e7, B:58:0x0100, B:61:0x0108, B:64:0x010f, B:65:0x0113, B:67:0x0119, B:71:0x0135, B:75:0x013e, B:76:0x0173, B:77:0x0175, B:78:0x017f, B:80:0x0185, B:84:0x019f, B:88:0x01a8, B:89:0x01d8, B:92:0x01e2, B:95:0x01ea, B:98:0x01f2, B:40:0x00ba, B:43:0x00c1, B:46:0x00c8, B:47:0x00cc, B:49:0x00d2, B:53:0x00e5, B:29:0x008e), top: B:172:0x002c }] */
    /* JADX WARN: Removed duplicated region for block: B:174:0x0311 A[SYNTHETIC] */
    /* JADX WARN: Removed duplicated region for block: B:182:0x0278 A[SYNTHETIC] */
    /* JADX WARN: Removed duplicated region for block: B:190:0x00e4 A[SYNTHETIC] */
    /* JADX WARN: Removed duplicated region for block: B:28:0x008b  */
    /* JADX WARN: Removed duplicated region for block: B:49:0x00d2 A[Catch: Exception -> 0x034c, TryCatch #0 {Exception -> 0x034c, blocks: (B:15:0x0046, B:131:0x0296, B:133:0x029c, B:141:0x02c6, B:142:0x02cd, B:144:0x02d3, B:146:0x02f0, B:148:0x02fd, B:151:0x0306, B:160:0x0319, B:163:0x031e, B:157:0x0312, B:158:0x0317, B:164:0x032c, B:168:0x034a, B:20:0x0069, B:23:0x007e, B:101:0x0216, B:102:0x0221, B:104:0x0227, B:105:0x0232, B:107:0x0238, B:109:0x0257, B:111:0x0264, B:114:0x026d, B:128:0x028c, B:121:0x027c, B:122:0x0281, B:130:0x0292, B:26:0x0087, B:33:0x00a2, B:35:0x00aa, B:37:0x00b0, B:54:0x00e7, B:58:0x0100, B:61:0x0108, B:64:0x010f, B:65:0x0113, B:67:0x0119, B:71:0x0135, B:75:0x013e, B:76:0x0173, B:77:0x0175, B:78:0x017f, B:80:0x0185, B:84:0x019f, B:88:0x01a8, B:89:0x01d8, B:92:0x01e2, B:95:0x01ea, B:98:0x01f2, B:40:0x00ba, B:43:0x00c1, B:46:0x00c8, B:47:0x00cc, B:49:0x00d2, B:53:0x00e5, B:29:0x008e), top: B:172:0x002c }] */
    /* JADX WARN: Removed duplicated region for block: B:58:0x0100 A[Catch: Exception -> 0x034c, TRY_ENTER, TryCatch #0 {Exception -> 0x034c, blocks: (B:15:0x0046, B:131:0x0296, B:133:0x029c, B:141:0x02c6, B:142:0x02cd, B:144:0x02d3, B:146:0x02f0, B:148:0x02fd, B:151:0x0306, B:160:0x0319, B:163:0x031e, B:157:0x0312, B:158:0x0317, B:164:0x032c, B:168:0x034a, B:20:0x0069, B:23:0x007e, B:101:0x0216, B:102:0x0221, B:104:0x0227, B:105:0x0232, B:107:0x0238, B:109:0x0257, B:111:0x0264, B:114:0x026d, B:128:0x028c, B:121:0x027c, B:122:0x0281, B:130:0x0292, B:26:0x0087, B:33:0x00a2, B:35:0x00aa, B:37:0x00b0, B:54:0x00e7, B:58:0x0100, B:61:0x0108, B:64:0x010f, B:65:0x0113, B:67:0x0119, B:71:0x0135, B:75:0x013e, B:76:0x0173, B:77:0x0175, B:78:0x017f, B:80:0x0185, B:84:0x019f, B:88:0x01a8, B:89:0x01d8, B:92:0x01e2, B:95:0x01ea, B:98:0x01f2, B:40:0x00ba, B:43:0x00c1, B:46:0x00c8, B:47:0x00cc, B:49:0x00d2, B:53:0x00e5, B:29:0x008e), top: B:172:0x002c }] */
    /* JADX WARN: Removed duplicated region for block: B:67:0x0119 A[Catch: Exception -> 0x034c, TryCatch #0 {Exception -> 0x034c, blocks: (B:15:0x0046, B:131:0x0296, B:133:0x029c, B:141:0x02c6, B:142:0x02cd, B:144:0x02d3, B:146:0x02f0, B:148:0x02fd, B:151:0x0306, B:160:0x0319, B:163:0x031e, B:157:0x0312, B:158:0x0317, B:164:0x032c, B:168:0x034a, B:20:0x0069, B:23:0x007e, B:101:0x0216, B:102:0x0221, B:104:0x0227, B:105:0x0232, B:107:0x0238, B:109:0x0257, B:111:0x0264, B:114:0x026d, B:128:0x028c, B:121:0x027c, B:122:0x0281, B:130:0x0292, B:26:0x0087, B:33:0x00a2, B:35:0x00aa, B:37:0x00b0, B:54:0x00e7, B:58:0x0100, B:61:0x0108, B:64:0x010f, B:65:0x0113, B:67:0x0119, B:71:0x0135, B:75:0x013e, B:76:0x0173, B:77:0x0175, B:78:0x017f, B:80:0x0185, B:84:0x019f, B:88:0x01a8, B:89:0x01d8, B:92:0x01e2, B:95:0x01ea, B:98:0x01f2, B:40:0x00ba, B:43:0x00c1, B:46:0x00c8, B:47:0x00cc, B:49:0x00d2, B:53:0x00e5, B:29:0x008e), top: B:172:0x002c }] */
    /* JADX WARN: Removed duplicated region for block: B:80:0x0185 A[Catch: Exception -> 0x034c, TryCatch #0 {Exception -> 0x034c, blocks: (B:15:0x0046, B:131:0x0296, B:133:0x029c, B:141:0x02c6, B:142:0x02cd, B:144:0x02d3, B:146:0x02f0, B:148:0x02fd, B:151:0x0306, B:160:0x0319, B:163:0x031e, B:157:0x0312, B:158:0x0317, B:164:0x032c, B:168:0x034a, B:20:0x0069, B:23:0x007e, B:101:0x0216, B:102:0x0221, B:104:0x0227, B:105:0x0232, B:107:0x0238, B:109:0x0257, B:111:0x0264, B:114:0x026d, B:128:0x028c, B:121:0x027c, B:122:0x0281, B:130:0x0292, B:26:0x0087, B:33:0x00a2, B:35:0x00aa, B:37:0x00b0, B:54:0x00e7, B:58:0x0100, B:61:0x0108, B:64:0x010f, B:65:0x0113, B:67:0x0119, B:71:0x0135, B:75:0x013e, B:76:0x0173, B:77:0x0175, B:78:0x017f, B:80:0x0185, B:84:0x019f, B:88:0x01a8, B:89:0x01d8, B:92:0x01e2, B:95:0x01ea, B:98:0x01f2, B:40:0x00ba, B:43:0x00c1, B:46:0x00c8, B:47:0x00cc, B:49:0x00d2, B:53:0x00e5, B:29:0x008e), top: B:172:0x002c }] */
    /* JADX WARN: Removed duplicated region for block: B:91:0x01e0 A[ADDED_TO_REGION] */
    /* JADX WARN: Removed duplicated region for block: B:92:0x01e2 A[Catch: Exception -> 0x034c, TryCatch #0 {Exception -> 0x034c, blocks: (B:15:0x0046, B:131:0x0296, B:133:0x029c, B:141:0x02c6, B:142:0x02cd, B:144:0x02d3, B:146:0x02f0, B:148:0x02fd, B:151:0x0306, B:160:0x0319, B:163:0x031e, B:157:0x0312, B:158:0x0317, B:164:0x032c, B:168:0x034a, B:20:0x0069, B:23:0x007e, B:101:0x0216, B:102:0x0221, B:104:0x0227, B:105:0x0232, B:107:0x0238, B:109:0x0257, B:111:0x0264, B:114:0x026d, B:128:0x028c, B:121:0x027c, B:122:0x0281, B:130:0x0292, B:26:0x0087, B:33:0x00a2, B:35:0x00aa, B:37:0x00b0, B:54:0x00e7, B:58:0x0100, B:61:0x0108, B:64:0x010f, B:65:0x0113, B:67:0x0119, B:71:0x0135, B:75:0x013e, B:76:0x0173, B:77:0x0175, B:78:0x017f, B:80:0x0185, B:84:0x019f, B:88:0x01a8, B:89:0x01d8, B:92:0x01e2, B:95:0x01ea, B:98:0x01f2, B:40:0x00ba, B:43:0x00c1, B:46:0x00c8, B:47:0x00cc, B:49:0x00d2, B:53:0x00e5, B:29:0x008e), top: B:172:0x002c }] */
    /* JADX WARN: Unsupported multi-entry loop pattern (BACK_EDGE: B:165:0x0342 -> B:167:0x0345). Please submit an issue!!! */
    /*
        Code decompiled incorrectly, please refer to instructions dump.
        To view partially-correct code enable 'Show inconsistent code' option in preferences
    */
    public final java.lang.Object i(defpackage.q70<? super defpackage.te4> r32) {
        /*
            Method dump skipped, instructions count: 847
            To view this dump change 'Code comments level' option to 'DEBUG'
        */
        throw new UnsupportedOperationException("Method not decompiled: net.safemoon.androidwallet.utils.PreFetch.i(q70):java.lang.Object");
    }
}
