package net.safemoon.androidwallet.utils;

import android.inputmethodservice.InputMethodService;
import android.view.View;
import android.view.ViewGroup;
import net.safemoon.androidwallet.R;

/* compiled from: SimpleIME.kt */
/* loaded from: classes2.dex */
public final class SimpleIME extends InputMethodService {
    @Override // android.inputmethodservice.InputMethodService
    public View onCreateInputView() {
        View inflate = getLayoutInflater().inflate(R.layout.view_custom_keyboard, (ViewGroup) null, false);
        fs1.e(inflate, "layoutInflater.inflate(R…om_keyboard, null, false)");
        return inflate;
    }
}
