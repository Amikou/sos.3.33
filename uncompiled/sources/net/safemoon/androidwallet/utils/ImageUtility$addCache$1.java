package net.safemoon.androidwallet.utils;

import kotlin.coroutines.jvm.internal.ContinuationImpl;
import kotlin.coroutines.jvm.internal.a;

/* compiled from: ImageUtility.kt */
@a(c = "net.safemoon.androidwallet.utils.ImageUtility", f = "ImageUtility.kt", l = {38}, m = "addCache")
/* loaded from: classes2.dex */
public final class ImageUtility$addCache$1 extends ContinuationImpl {
    public Object L$0;
    public int label;
    public /* synthetic */ Object result;
    public final /* synthetic */ ImageUtility this$0;

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public ImageUtility$addCache$1(ImageUtility imageUtility, q70<? super ImageUtility$addCache$1> q70Var) {
        super(q70Var);
        this.this$0 = imageUtility;
    }

    @Override // kotlin.coroutines.jvm.internal.BaseContinuationImpl
    public final Object invokeSuspend(Object obj) {
        this.result = obj;
        this.label |= Integer.MIN_VALUE;
        return this.this$0.b(null, this);
    }
}
