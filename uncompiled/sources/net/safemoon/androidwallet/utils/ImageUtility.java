package net.safemoon.androidwallet.utils;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.PorterDuff;
import android.graphics.PorterDuffColorFilter;
import android.graphics.drawable.Drawable;
import androidx.core.graphics.drawable.a;
import androidx.recyclerview.widget.RecyclerView;
import java.io.InputStream;
import java.io.OutputStream;
import net.safemoon.androidwallet.R;

/* compiled from: ImageUtility.kt */
/* loaded from: classes2.dex */
public final class ImageUtility {
    public final Context a;

    public ImageUtility(Context context) {
        fs1.f(context, "context");
        this.a = context;
    }

    /* JADX WARN: Removed duplicated region for block: B:10:0x0023  */
    /* JADX WARN: Removed duplicated region for block: B:14:0x0035  */
    /*
        Code decompiled incorrectly, please refer to instructions dump.
        To view partially-correct code enable 'Show inconsistent code' option in preferences
    */
    public final java.lang.Object a(android.graphics.Bitmap r7, java.lang.String r8, defpackage.q70<? super java.io.File> r9) {
        /*
            r6 = this;
            boolean r0 = r9 instanceof net.safemoon.androidwallet.utils.ImageUtility$addCache$2
            if (r0 == 0) goto L13
            r0 = r9
            net.safemoon.androidwallet.utils.ImageUtility$addCache$2 r0 = (net.safemoon.androidwallet.utils.ImageUtility$addCache$2) r0
            int r1 = r0.label
            r2 = -2147483648(0xffffffff80000000, float:-0.0)
            r3 = r1 & r2
            if (r3 == 0) goto L13
            int r1 = r1 - r2
            r0.label = r1
            goto L18
        L13:
            net.safemoon.androidwallet.utils.ImageUtility$addCache$2 r0 = new net.safemoon.androidwallet.utils.ImageUtility$addCache$2
            r0.<init>(r6, r9)
        L18:
            java.lang.Object r9 = r0.result
            java.lang.Object r1 = defpackage.gs1.d()
            int r2 = r0.label
            r3 = 1
            if (r2 == 0) goto L35
            if (r2 != r3) goto L2d
            java.lang.Object r7 = r0.L$0
            java.io.File r7 = (java.io.File) r7
            defpackage.o83.b(r9)
            goto L6a
        L2d:
            java.lang.IllegalStateException r7 = new java.lang.IllegalStateException
            java.lang.String r8 = "call to 'resume' before 'invoke' with coroutine"
            r7.<init>(r8)
            throw r7
        L35:
            defpackage.o83.b(r9)
            java.io.File r9 = new java.io.File
            android.content.Context r2 = r6.a
            java.io.File r2 = r2.getFilesDir()
            r9.<init>(r2, r8)
            boolean r8 = r9.exists()
            if (r8 != 0) goto L4c
            r9.mkdirs()
        L4c:
            java.io.File r8 = new java.io.File
            long r4 = r6.f()
            java.lang.String r2 = java.lang.String.valueOf(r4)
            r8.<init>(r9, r2)
            java.io.FileOutputStream r9 = new java.io.FileOutputStream
            r9.<init>(r8)
            r0.L$0 = r8
            r0.label = r3
            java.lang.Object r7 = r6.d(r7, r9, r0)
            if (r7 != r1) goto L69
            return r1
        L69:
            r7 = r8
        L6a:
            return r7
        */
        throw new UnsupportedOperationException("Method not decompiled: net.safemoon.androidwallet.utils.ImageUtility.a(android.graphics.Bitmap, java.lang.String, q70):java.lang.Object");
    }

    /* JADX WARN: Removed duplicated region for block: B:10:0x0023  */
    /* JADX WARN: Removed duplicated region for block: B:14:0x0035  */
    /*
        Code decompiled incorrectly, please refer to instructions dump.
        To view partially-correct code enable 'Show inconsistent code' option in preferences
    */
    public final java.lang.Object b(android.net.Uri r7, defpackage.q70<? super java.io.File> r8) {
        /*
            r6 = this;
            boolean r0 = r8 instanceof net.safemoon.androidwallet.utils.ImageUtility$addCache$1
            if (r0 == 0) goto L13
            r0 = r8
            net.safemoon.androidwallet.utils.ImageUtility$addCache$1 r0 = (net.safemoon.androidwallet.utils.ImageUtility$addCache$1) r0
            int r1 = r0.label
            r2 = -2147483648(0xffffffff80000000, float:-0.0)
            r3 = r1 & r2
            if (r3 == 0) goto L13
            int r1 = r1 - r2
            r0.label = r1
            goto L18
        L13:
            net.safemoon.androidwallet.utils.ImageUtility$addCache$1 r0 = new net.safemoon.androidwallet.utils.ImageUtility$addCache$1
            r0.<init>(r6, r8)
        L18:
            java.lang.Object r8 = r0.result
            java.lang.Object r1 = defpackage.gs1.d()
            int r2 = r0.label
            r3 = 1
            if (r2 == 0) goto L35
            if (r2 != r3) goto L2d
            java.lang.Object r7 = r0.L$0
            java.io.File r7 = (java.io.File) r7
            defpackage.o83.b(r8)
            goto L79
        L2d:
            java.lang.IllegalStateException r7 = new java.lang.IllegalStateException
            java.lang.String r8 = "call to 'resume' before 'invoke' with coroutine"
            r7.<init>(r8)
            throw r7
        L35:
            defpackage.o83.b(r8)
            java.io.File r8 = new java.io.File
            android.content.Context r2 = r6.a
            java.io.File r2 = r2.getFilesDir()
            java.lang.String r4 = "Avatar"
            r8.<init>(r2, r4)
            boolean r2 = r8.exists()
            if (r2 != 0) goto L4e
            r8.mkdirs()
        L4e:
            java.io.File r2 = new java.io.File
            long r4 = r6.f()
            java.lang.String r4 = java.lang.String.valueOf(r4)
            r2.<init>(r8, r4)
            android.content.Context r8 = r6.a
            android.content.ContentResolver r8 = r8.getContentResolver()
            java.io.InputStream r7 = r8.openInputStream(r7)
            java.io.FileOutputStream r8 = new java.io.FileOutputStream
            r8.<init>(r2)
            defpackage.fs1.d(r7)
            r0.L$0 = r2
            r0.label = r3
            java.lang.Object r7 = r6.e(r7, r8, r0)
            if (r7 != r1) goto L78
            return r1
        L78:
            r7 = r2
        L79:
            return r7
        */
        throw new UnsupportedOperationException("Method not decompiled: net.safemoon.androidwallet.utils.ImageUtility.b(android.net.Uri, q70):java.lang.Object");
    }

    public final Drawable c(Drawable drawable, int i) {
        if (drawable == null) {
            return null;
        }
        Drawable r = a.r(drawable);
        r.setColorFilter(new PorterDuffColorFilter(g83.d(this.a.getResources(), R.color.dark_grey, null), PorterDuff.Mode.SRC_ATOP));
        return r;
    }

    public final Object d(Bitmap bitmap, OutputStream outputStream, q70<? super te4> q70Var) {
        bitmap.compress(Bitmap.CompressFormat.PNG, 100, outputStream);
        return te4.a;
    }

    public final Object e(InputStream inputStream, OutputStream outputStream, q70<? super te4> q70Var) {
        byte[] bArr = new byte[RecyclerView.a0.FLAG_ADAPTER_FULLUPDATE];
        while (true) {
            int read = inputStream.read(bArr);
            if (read > 0) {
                outputStream.write(bArr, 0, read);
            } else {
                outputStream.close();
                inputStream.close();
                return te4.a;
            }
        }
    }

    public final long f() {
        return System.currentTimeMillis();
    }
}
