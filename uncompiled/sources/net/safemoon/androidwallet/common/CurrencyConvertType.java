package net.safemoon.androidwallet.common;

/* compiled from: CurrencyConvertType.kt */
/* loaded from: classes2.dex */
public enum CurrencyConvertType {
    USD("USDT", "$");
    
    private final String symbol;
    private final String value;

    CurrencyConvertType(String str, String str2) {
        this.value = str;
        this.symbol = str2;
    }

    public final String getSymbol() {
        return this.symbol;
    }

    public final String getValue() {
        return this.value;
    }
}
