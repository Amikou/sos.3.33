package net.safemoon.androidwallet.common;

import androidx.annotation.Keep;
import com.google.android.gms.common.annotation.KeepName;
import kotlin.NoWhenBranchMatchedException;
import net.safemoon.androidwallet.R;

/* compiled from: TokenType.kt */
@Keep
@KeepName
/* loaded from: classes2.dex */
public enum TokenType {
    BEP_20(56, false, true),
    ERC_20(1, false, true),
    POLYGON(137, false, true),
    AVALANCHE_C(43114, false, true),
    BEP_20_TEST(97, true, true),
    ERC_20_TEST(3, true, true),
    POLYGON_TEST(80001, true, true),
    AVALANCHE_FUJI_TEST(43113, true, true);
    
    private final int chainId;
    private final boolean isEnable;
    private final boolean isTestNet;
    public static final a Companion = new a(null);
    private static String API_KEY_BEP_20 = "";
    private static String API_KEY_BEP_20_TEST = "";
    private static String API_KEY_ERC_20 = "";
    private static String API_KEY_ERC_20_TEST = "";
    private static String API_KEY_POLYGON = "";
    private static String API_KEY_POLYGON_TEST = "";
    private static String API_KEY_AVALANCHE_C = "";
    private static String API_KEY_AVALANCHE_FUJI_TEST = "";

    /* compiled from: TokenType.kt */
    /* loaded from: classes2.dex */
    public static final class a {
        public a() {
        }

        public /* synthetic */ a(qi0 qi0Var) {
            this();
        }

        public final TokenType a(String str) {
            fs1.f(str, "symbol");
            Boolean bool = zr.b;
            fs1.e(bool, "IS_TEST_NET");
            if (bool.booleanValue()) {
                int hashCode = str.hashCode();
                if (hashCode != 65910) {
                    if (hashCode != 68985) {
                        if (hashCode == 73130586 && str.equals("MATIC")) {
                            return TokenType.POLYGON_TEST;
                        }
                    } else if (str.equals("ETH")) {
                        return TokenType.ERC_20_TEST;
                    }
                } else if (str.equals("BNB")) {
                    return TokenType.BEP_20_TEST;
                }
                throw new IllegalArgumentException(fs1.l("Unsupported value=", str));
            }
            switch (str.hashCode()) {
                case 65910:
                    if (str.equals("BNB")) {
                        return TokenType.BEP_20;
                    }
                    break;
                case 68985:
                    if (str.equals("ETH")) {
                        return TokenType.ERC_20;
                    }
                    break;
                case 2021164:
                    if (str.equals("AVAX")) {
                        return TokenType.AVALANCHE_C;
                    }
                    break;
                case 73130586:
                    if (str.equals("MATIC")) {
                        return TokenType.POLYGON;
                    }
                    break;
            }
            throw new IllegalArgumentException(fs1.l("Unsupported value=", str));
        }

        public final TokenType b(int i) {
            TokenType tokenType = TokenType.BEP_20;
            if (i != tokenType.getChainId()) {
                tokenType = TokenType.BEP_20_TEST;
                if (i != tokenType.getChainId()) {
                    tokenType = TokenType.ERC_20;
                    if (i != tokenType.getChainId()) {
                        tokenType = TokenType.ERC_20_TEST;
                        if (i != tokenType.getChainId()) {
                            tokenType = TokenType.POLYGON;
                            if (i != tokenType.getChainId()) {
                                tokenType = TokenType.POLYGON_TEST;
                                if (i != tokenType.getChainId()) {
                                    tokenType = TokenType.AVALANCHE_C;
                                    if (i != tokenType.getChainId()) {
                                        tokenType = TokenType.AVALANCHE_FUJI_TEST;
                                        if (i != tokenType.getChainId()) {
                                            throw new IllegalArgumentException(fs1.l("Unsupported value=", Integer.valueOf(i)));
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
            return tokenType;
        }

        /* JADX WARN: Code restructure failed: missing block: B:18:0x003e, code lost:
            if (r3.equals("AVALANCHE_C") != false) goto L5;
         */
        /* JADX WARN: Code restructure failed: missing block: B:27:0x005d, code lost:
            if (r3.equals("AVAX") != false) goto L5;
         */
        /* JADX WARN: Code restructure failed: missing block: B:43:?, code lost:
            return net.safemoon.androidwallet.common.TokenType.AVALANCHE_C;
         */
        /* JADX WARN: Code restructure failed: missing block: B:6:0x0014, code lost:
            if (r3.equals("AVALANCHE") != false) goto L5;
         */
        /*
            Code decompiled incorrectly, please refer to instructions dump.
            To view partially-correct code enable 'Show inconsistent code' option in preferences
        */
        public final net.safemoon.androidwallet.common.TokenType c(java.lang.String r3) {
            /*
                r2 = this;
                java.lang.String r0 = "chain"
                defpackage.fs1.f(r3, r0)
                int r0 = r3.hashCode()
                switch(r0) {
                    case -1425134918: goto L6d;
                    case -923398111: goto L62;
                    case 2021164: goto L57;
                    case 320463130: goto L4c;
                    case 413840535: goto L41;
                    case 1381490967: goto L38;
                    case 1743078378: goto L2d;
                    case 1955723088: goto L22;
                    case 2053229031: goto L17;
                    case 2128809491: goto Le;
                    default: goto Lc;
                }
            Lc:
                goto L78
            Le:
                java.lang.String r0 = "AVALANCHE"
                boolean r0 = r3.equals(r0)
                if (r0 == 0) goto L78
                goto L5f
            L17:
                java.lang.String r0 = "ERC_20"
                boolean r0 = r3.equals(r0)
                if (r0 == 0) goto L78
                net.safemoon.androidwallet.common.TokenType r3 = net.safemoon.androidwallet.common.TokenType.ERC_20
                goto L77
            L22:
                java.lang.String r0 = "BEP_20"
                boolean r0 = r3.equals(r0)
                if (r0 == 0) goto L78
                net.safemoon.androidwallet.common.TokenType r3 = net.safemoon.androidwallet.common.TokenType.BEP_20
                goto L77
            L2d:
                java.lang.String r0 = "ERC_20_TEST"
                boolean r0 = r3.equals(r0)
                if (r0 == 0) goto L78
                net.safemoon.androidwallet.common.TokenType r3 = net.safemoon.androidwallet.common.TokenType.ERC_20_TEST
                goto L77
            L38:
                java.lang.String r0 = "AVALANCHE_C"
                boolean r0 = r3.equals(r0)
                if (r0 == 0) goto L78
                goto L5f
            L41:
                java.lang.String r0 = "POLYGON_TEST"
                boolean r0 = r3.equals(r0)
                if (r0 == 0) goto L78
                net.safemoon.androidwallet.common.TokenType r3 = net.safemoon.androidwallet.common.TokenType.POLYGON_TEST
                goto L77
            L4c:
                java.lang.String r0 = "POLYGON"
                boolean r0 = r3.equals(r0)
                if (r0 == 0) goto L78
                net.safemoon.androidwallet.common.TokenType r3 = net.safemoon.androidwallet.common.TokenType.POLYGON
                goto L77
            L57:
                java.lang.String r0 = "AVAX"
                boolean r0 = r3.equals(r0)
                if (r0 == 0) goto L78
            L5f:
                net.safemoon.androidwallet.common.TokenType r3 = net.safemoon.androidwallet.common.TokenType.AVALANCHE_C
                goto L77
            L62:
                java.lang.String r0 = "BEP_20_TEST"
                boolean r0 = r3.equals(r0)
                if (r0 == 0) goto L78
                net.safemoon.androidwallet.common.TokenType r3 = net.safemoon.androidwallet.common.TokenType.BEP_20_TEST
                goto L77
            L6d:
                java.lang.String r0 = "AVALANCHE_C_TEST"
                boolean r0 = r3.equals(r0)
                if (r0 == 0) goto L78
                net.safemoon.androidwallet.common.TokenType r3 = net.safemoon.androidwallet.common.TokenType.AVALANCHE_FUJI_TEST
            L77:
                return r3
            L78:
                java.lang.IllegalArgumentException r0 = new java.lang.IllegalArgumentException
                java.lang.String r1 = "Unsupported value="
                java.lang.String r3 = defpackage.fs1.l(r1, r3)
                r0.<init>(r3)
                throw r0
            */
            throw new UnsupportedOperationException("Method not decompiled: net.safemoon.androidwallet.common.TokenType.a.c(java.lang.String):net.safemoon.androidwallet.common.TokenType");
        }

        public final void d(String str) {
            fs1.f(str, "<set-?>");
            TokenType.API_KEY_AVALANCHE_C = str;
        }

        public final void e(String str) {
            fs1.f(str, "<set-?>");
            TokenType.API_KEY_BEP_20 = str;
        }

        public final void f(String str) {
            fs1.f(str, "<set-?>");
            TokenType.API_KEY_ERC_20 = str;
        }

        public final void g(String str) {
            fs1.f(str, "<set-?>");
            TokenType.API_KEY_POLYGON = str;
        }
    }

    /* compiled from: TokenType.kt */
    /* loaded from: classes2.dex */
    public /* synthetic */ class b {
        public static final /* synthetic */ int[] a;

        static {
            int[] iArr = new int[TokenType.values().length];
            iArr[TokenType.BEP_20.ordinal()] = 1;
            iArr[TokenType.BEP_20_TEST.ordinal()] = 2;
            iArr[TokenType.ERC_20.ordinal()] = 3;
            iArr[TokenType.ERC_20_TEST.ordinal()] = 4;
            iArr[TokenType.POLYGON.ordinal()] = 5;
            iArr[TokenType.POLYGON_TEST.ordinal()] = 6;
            iArr[TokenType.AVALANCHE_C.ordinal()] = 7;
            iArr[TokenType.AVALANCHE_FUJI_TEST.ordinal()] = 8;
            a = iArr;
        }
    }

    TokenType(int i, boolean z, boolean z2) {
        this.chainId = i;
        this.isTestNet = z;
        this.isEnable = z2;
    }

    public static final TokenType fromValue(int i) {
        return Companion.b(i);
    }

    public static final TokenType fromValue(String str) {
        return Companion.c(str);
    }

    public final String getAssignKey() {
        switch (b.a[ordinal()]) {
            case 1:
                return API_KEY_BEP_20;
            case 2:
                return API_KEY_BEP_20_TEST;
            case 3:
                return API_KEY_ERC_20;
            case 4:
                return API_KEY_ERC_20_TEST;
            case 5:
                return API_KEY_POLYGON;
            case 6:
                return API_KEY_POLYGON_TEST;
            case 7:
                return API_KEY_AVALANCHE_C;
            case 8:
                return API_KEY_AVALANCHE_FUJI_TEST;
            default:
                throw new NoWhenBranchMatchedException();
        }
    }

    public final int getChainId() {
        return this.chainId;
    }

    public final String getChainTitle() {
        int i = this.chainId;
        if (i == ERC_20.chainId || i == ERC_20_TEST.chainId) {
            return "Ethereum";
        }
        if (i == BEP_20.chainId || i == BEP_20_TEST.chainId) {
            return "BSC";
        }
        if (i == POLYGON.chainId || i == POLYGON_TEST.chainId) {
            return "Polygon";
        }
        if (i == AVALANCHE_C.chainId || i == AVALANCHE_FUJI_TEST.chainId) {
            return "Avalanche";
        }
        throw new IllegalArgumentException(fs1.l("Unsupported value=", Integer.valueOf(this.chainId)));
    }

    public final String getDisplayName() {
        int i = this.chainId;
        if (i == ERC_20.chainId || i == ERC_20_TEST.chainId) {
            return "ERC-20";
        }
        if (i == BEP_20.chainId || i == BEP_20_TEST.chainId) {
            return "BEP-20";
        }
        if (i == POLYGON.chainId || i == POLYGON_TEST.chainId) {
            return "MATIC";
        }
        if (i == AVALANCHE_C.chainId || i == AVALANCHE_FUJI_TEST.chainId) {
            return "AVAX";
        }
        throw new IllegalArgumentException(fs1.l("Unsupported value=", Integer.valueOf(this.chainId)));
    }

    public final int getIcon() {
        int i = this.chainId;
        if (i == ERC_20.chainId || i == ERC_20_TEST.chainId) {
            return R.drawable.ethereum;
        }
        if (i == BEP_20.chainId || i == BEP_20_TEST.chainId) {
            return R.drawable.binance;
        }
        if (i == POLYGON.chainId || i == POLYGON_TEST.chainId) {
            return R.drawable.polygon_matic_chain;
        }
        if (i == AVALANCHE_C.chainId || i == AVALANCHE_FUJI_TEST.chainId) {
            return R.drawable.avalanche;
        }
        throw new IllegalArgumentException(fs1.l("Unsupported value=", Integer.valueOf(this.chainId)));
    }

    public final String getName() {
        int i = this.chainId;
        if (i == BEP_20.chainId) {
            return "BEP_20";
        }
        if (i == BEP_20_TEST.chainId) {
            return "BEP_20_TEST";
        }
        if (i == ERC_20.chainId) {
            return "ERC_20";
        }
        if (i == ERC_20_TEST.chainId) {
            return "ERC_20_TEST";
        }
        if (i == POLYGON.chainId) {
            return "POLYGON";
        }
        if (i == POLYGON_TEST.chainId) {
            return "POLYGON_TEST";
        }
        if (i == AVALANCHE_C.chainId) {
            return "AVALANCHE_C";
        }
        if (i == AVALANCHE_FUJI_TEST.chainId) {
            return "AVALANCHE_C_TEST";
        }
        throw new IllegalArgumentException(fs1.l("Unsupported value=", Integer.valueOf(this.chainId)));
    }

    public final String getNativeToken() {
        int i = this.chainId;
        if (i == ERC_20.chainId || i == ERC_20_TEST.chainId) {
            return "ETH";
        }
        if (i == BEP_20.chainId || i == BEP_20_TEST.chainId) {
            return "BNB";
        }
        if (i == POLYGON.chainId || i == POLYGON_TEST.chainId) {
            return "MATIC";
        }
        if (i == AVALANCHE_C.chainId || i == AVALANCHE_FUJI_TEST.chainId) {
            return "AVAX";
        }
        throw new IllegalArgumentException(fs1.l("Unsupported value=", Integer.valueOf(this.chainId)));
    }

    public final int getNetworkResource() {
        int i = this.chainId;
        if (i == ERC_20.chainId || i == ERC_20_TEST.chainId) {
            return R.string.network_type_ethereum;
        }
        if (i == BEP_20.chainId || i == BEP_20_TEST.chainId) {
            return R.string.network_type_bsc;
        }
        if (i == POLYGON.chainId || i == POLYGON_TEST.chainId) {
            return R.string.network_type_matic;
        }
        if (i == AVALANCHE_C.chainId || i == AVALANCHE_FUJI_TEST.chainId) {
            return R.string.network_type_avalanche_c;
        }
        throw new IllegalArgumentException(fs1.l("Unsupported value=", Integer.valueOf(this.chainId)));
    }

    public final String getOptionalName() {
        return this.chainId == AVALANCHE_C.chainId ? "AVALANCHE" : getName();
    }

    public final String getPlaneName() {
        int i = this.chainId;
        if (i == ERC_20.chainId || i == ERC_20_TEST.chainId) {
            return "Ethereum ERC-20   ";
        }
        if (i == BEP_20.chainId || i == BEP_20_TEST.chainId) {
            return "Smart Chain BEP-20";
        }
        if (i == POLYGON.chainId || i == POLYGON_TEST.chainId) {
            return "Polygon";
        }
        if (i == AVALANCHE_C.chainId || i == AVALANCHE_FUJI_TEST.chainId) {
            return "Avalanche C";
        }
        throw new IllegalArgumentException(fs1.l("Unsupported value=", Integer.valueOf(this.chainId)));
    }

    public final String getScanApi() {
        int i = this.chainId;
        if (i == ERC_20.chainId || i == ERC_20_TEST.chainId) {
            return "https://api.etherscan.io";
        }
        if (i == BEP_20.chainId || i == BEP_20_TEST.chainId) {
            return "https://api.bscscan.com";
        }
        if (i == POLYGON.chainId || i == POLYGON_TEST.chainId) {
            return "https://api.polygonscan.com";
        }
        if (i == AVALANCHE_C.chainId || i == AVALANCHE_FUJI_TEST.chainId) {
            return "https://api.snowtrace.io";
        }
        throw new IllegalArgumentException(fs1.l("Unsupported value=", Integer.valueOf(this.chainId)));
    }

    public final String getSymbolWithType() {
        int i = this.chainId;
        if (i == ERC_20.chainId || i == ERC_20_TEST.chainId) {
            return "ERC_ETH";
        }
        if (i == BEP_20.chainId || i == BEP_20_TEST.chainId) {
            return "BEP_BNB";
        }
        if (i == POLYGON.chainId || i == POLYGON_TEST.chainId) {
            return "POLYGON_MATIC";
        }
        if (i == AVALANCHE_C.chainId || i == AVALANCHE_FUJI_TEST.chainId) {
            return "AVALANCHE_C_AVAX";
        }
        throw new IllegalArgumentException(fs1.l("Unsupported value=", Integer.valueOf(this.chainId)));
    }

    public final String getTitle() {
        int i = this.chainId;
        if (i == ERC_20.chainId || i == ERC_20_TEST.chainId) {
            return "Ethereum";
        }
        if (i == BEP_20.chainId || i == BEP_20_TEST.chainId) {
            return "Smart Chain";
        }
        if (i == POLYGON.chainId || i == POLYGON_TEST.chainId) {
            return "Polygon";
        }
        if (i == AVALANCHE_C.chainId || i == AVALANCHE_FUJI_TEST.chainId) {
            return "Avalanche";
        }
        throw new IllegalArgumentException(fs1.l("Unsupported value=", Integer.valueOf(this.chainId)));
    }

    public final String getWrapAddress() {
        int i = this.chainId;
        return i == ERC_20.chainId ? "0xc02aaa39b223fe8d0a0e5c4f27ead9083c756cc2" : i == BEP_20.chainId ? "0xbb4CdB9CBd36B01bD1cBaEBF2De08d9173bc095c" : i == BEP_20_TEST.chainId ? "0xae13d989dac2f0debff460ac112a837c89baa7cd" : i == POLYGON.chainId ? "0x0d500B1d8E8eF31E21C99d1Db9A6444d3ADf1270" : i == AVALANCHE_C.chainId ? "0xb31f66aa3c1e785363f0875a1b74e27b85fd66c7" : "";
    }

    public final boolean isEnable() {
        return this.isEnable;
    }

    public final boolean isTestNet() {
        return this.isTestNet;
    }
}
