package net.safemoon.androidwallet.common;

/* compiled from: ActiveTokenListMode.kt */
/* loaded from: classes2.dex */
public enum ActiveTokenListMode {
    RECENT("recent"),
    GAINER("gainer"),
    LOSER("loser");
    
    private final String value;

    ActiveTokenListMode(String str) {
        this.value = str;
    }

    public final String getValue() {
        return this.value;
    }
}
