package net.safemoon.androidwallet;

import android.content.Context;
import kotlin.coroutines.jvm.internal.SuspendLambda;
import kotlin.coroutines.jvm.internal.a;

/* compiled from: EasyAppInitializer.kt */
/* loaded from: classes2.dex */
public final class EasyAppInitializer extends rf {
    public final c90 b;

    /* compiled from: EasyAppInitializer.kt */
    @a(c = "net.safemoon.androidwallet.EasyAppInitializer$1", f = "EasyAppInitializer.kt", l = {}, m = "invokeSuspend")
    /* renamed from: net.safemoon.androidwallet.EasyAppInitializer$1  reason: invalid class name */
    /* loaded from: classes2.dex */
    public static final class AnonymousClass1 extends SuspendLambda implements hd1<c90, q70<? super te4>, Object> {
        public int label;

        public AnonymousClass1(q70<? super AnonymousClass1> q70Var) {
            super(2, q70Var);
        }

        @Override // kotlin.coroutines.jvm.internal.BaseContinuationImpl
        public final q70<te4> create(Object obj, q70<?> q70Var) {
            return new AnonymousClass1(q70Var);
        }

        @Override // defpackage.hd1
        public final Object invoke(c90 c90Var, q70<? super te4> q70Var) {
            return ((AnonymousClass1) create(c90Var, q70Var)).invokeSuspend(te4.a);
        }

        @Override // kotlin.coroutines.jvm.internal.BaseContinuationImpl
        public final Object invokeSuspend(Object obj) {
            gs1.d();
            if (this.label == 0) {
                o83.b(obj);
                return te4.a;
            }
            throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        }
    }

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public EasyAppInitializer(Context context) {
        super(context);
        fs1.f(context, "context");
        c90 a = d90.a(tp0.b());
        this.b = a;
        kotlinx.coroutines.a.b(a, null, null, new AnonymousClass1(null), 3, null);
    }
}
