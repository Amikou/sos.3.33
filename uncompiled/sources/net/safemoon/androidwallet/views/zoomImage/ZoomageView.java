package net.safemoon.androidwallet.views.zoomImage;

import android.animation.Animator;
import android.animation.ValueAnimator;
import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Bitmap;
import android.graphics.Matrix;
import android.graphics.PointF;
import android.graphics.RectF;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.util.AttributeSet;
import android.view.GestureDetector;
import android.view.MotionEvent;
import android.view.ScaleGestureDetector;
import android.widget.ImageView;
import androidx.appcompat.widget.AppCompatImageView;
import com.github.mikephil.charting.utils.Utils;

/* loaded from: classes2.dex */
public class ZoomageView extends AppCompatImageView implements ScaleGestureDetector.OnScaleGestureListener {
    public int A0;
    public int B0;
    public ScaleGestureDetector C0;
    public ValueAnimator D0;
    public GestureDetector E0;
    public boolean F0;
    public boolean G0;
    public final GestureDetector.OnGestureListener H0;
    public ImageView.ScaleType a;
    public Matrix f0;
    public Matrix g0;
    public float[] h0;
    public float[] i0;
    public float j0;
    public float k0;
    public float l0;
    public float m0;
    public final RectF n0;
    public boolean o0;
    public boolean p0;
    public boolean q0;
    public boolean r0;
    public boolean s0;
    public boolean t0;
    public float u0;
    public int v0;
    public PointF w0;
    public float x0;
    public float y0;
    public float z0;

    /* loaded from: classes2.dex */
    public class a implements ValueAnimator.AnimatorUpdateListener {
        public final Matrix a;
        public final float[] f0 = new float[9];
        public final /* synthetic */ Matrix g0;
        public final /* synthetic */ float h0;
        public final /* synthetic */ float i0;
        public final /* synthetic */ float j0;
        public final /* synthetic */ float k0;

        public a(Matrix matrix, float f, float f2, float f3, float f4) {
            this.g0 = matrix;
            this.h0 = f;
            this.i0 = f2;
            this.j0 = f3;
            this.k0 = f4;
            this.a = new Matrix(ZoomageView.this.getImageMatrix());
        }

        @Override // android.animation.ValueAnimator.AnimatorUpdateListener
        public void onAnimationUpdate(ValueAnimator valueAnimator) {
            float floatValue = ((Float) valueAnimator.getAnimatedValue()).floatValue();
            this.a.set(this.g0);
            this.a.getValues(this.f0);
            float[] fArr = this.f0;
            fArr[2] = fArr[2] + (this.h0 * floatValue);
            fArr[5] = fArr[5] + (this.i0 * floatValue);
            fArr[0] = fArr[0] + (this.j0 * floatValue);
            fArr[4] = fArr[4] + (this.k0 * floatValue);
            this.a.setValues(fArr);
            ZoomageView.this.setImageMatrix(this.a);
        }
    }

    /* loaded from: classes2.dex */
    public class b extends e {
        public final /* synthetic */ Matrix a;

        /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
        public b(Matrix matrix) {
            super(ZoomageView.this, null);
            this.a = matrix;
        }

        @Override // android.animation.Animator.AnimatorListener
        public void onAnimationEnd(Animator animator) {
            ZoomageView.this.setImageMatrix(this.a);
        }
    }

    /* loaded from: classes2.dex */
    public class c implements ValueAnimator.AnimatorUpdateListener {
        public final float[] a = new float[9];
        public Matrix f0 = new Matrix();
        public final /* synthetic */ int g0;

        public c(int i) {
            this.g0 = i;
        }

        @Override // android.animation.ValueAnimator.AnimatorUpdateListener
        public void onAnimationUpdate(ValueAnimator valueAnimator) {
            this.f0.set(ZoomageView.this.getImageMatrix());
            this.f0.getValues(this.a);
            this.a[this.g0] = ((Float) valueAnimator.getAnimatedValue()).floatValue();
            this.f0.setValues(this.a);
            ZoomageView.this.setImageMatrix(this.f0);
        }
    }

    /* loaded from: classes2.dex */
    public class d extends GestureDetector.SimpleOnGestureListener {
        public d() {
        }

        @Override // android.view.GestureDetector.SimpleOnGestureListener, android.view.GestureDetector.OnDoubleTapListener
        public boolean onDoubleTapEvent(MotionEvent motionEvent) {
            if (motionEvent.getAction() == 1) {
                ZoomageView.this.F0 = true;
            }
            ZoomageView.this.G0 = false;
            return false;
        }

        @Override // android.view.GestureDetector.SimpleOnGestureListener, android.view.GestureDetector.OnGestureListener
        public boolean onDown(MotionEvent motionEvent) {
            return true;
        }

        @Override // android.view.GestureDetector.SimpleOnGestureListener, android.view.GestureDetector.OnDoubleTapListener
        public boolean onSingleTapConfirmed(MotionEvent motionEvent) {
            ZoomageView.this.G0 = false;
            return false;
        }

        @Override // android.view.GestureDetector.SimpleOnGestureListener, android.view.GestureDetector.OnGestureListener
        public boolean onSingleTapUp(MotionEvent motionEvent) {
            ZoomageView.this.G0 = true;
            return false;
        }
    }

    /* loaded from: classes2.dex */
    public class e implements Animator.AnimatorListener {
        public e(ZoomageView zoomageView) {
        }

        @Override // android.animation.Animator.AnimatorListener
        public void onAnimationCancel(Animator animator) {
        }

        @Override // android.animation.Animator.AnimatorListener
        public void onAnimationRepeat(Animator animator) {
        }

        @Override // android.animation.Animator.AnimatorListener
        public void onAnimationStart(Animator animator) {
        }

        public /* synthetic */ e(ZoomageView zoomageView, a aVar) {
            this(zoomageView);
        }
    }

    public ZoomageView(Context context) {
        super(context);
        this.f0 = new Matrix();
        this.g0 = new Matrix();
        this.h0 = new float[9];
        this.i0 = null;
        this.j0 = 0.6f;
        this.k0 = 8.0f;
        this.l0 = 0.6f;
        this.m0 = 8.0f;
        this.n0 = new RectF();
        this.w0 = new PointF(Utils.FLOAT_EPSILON, Utils.FLOAT_EPSILON);
        this.x0 = 1.0f;
        this.y0 = 1.0f;
        this.z0 = 1.0f;
        this.A0 = 1;
        this.B0 = 0;
        this.F0 = false;
        this.G0 = false;
        this.H0 = new d();
        r(context, null);
    }

    private float getCurrentDisplayedHeight() {
        return getDrawable() != null ? getDrawable().getIntrinsicHeight() * this.h0[4] : Utils.FLOAT_EPSILON;
    }

    private float getCurrentDisplayedWidth() {
        return getDrawable() != null ? getDrawable().getIntrinsicWidth() * this.h0[0] : Utils.FLOAT_EPSILON;
    }

    public boolean e(MotionEvent motionEvent) {
        return this.o0 && this.z0 > 1.0f;
    }

    public boolean f(MotionEvent motionEvent) {
        return this.p0;
    }

    public final void g(int i, float f) {
        ValueAnimator ofFloat = ValueAnimator.ofFloat(this.h0[i], f);
        ofFloat.addUpdateListener(new c(i));
        ofFloat.setDuration(200L);
        ofFloat.start();
    }

    public boolean getAnimateOnReset() {
        return this.s0;
    }

    public boolean getAutoCenter() {
        return this.t0;
    }

    public int getAutoResetMode() {
        return this.v0;
    }

    public float getCurrentScaleFactor() {
        return this.z0;
    }

    public boolean getDoubleTapToZoom() {
        return this.q0;
    }

    public float getDoubleTapToZoomScaleFactor() {
        return this.u0;
    }

    public boolean getRestrictBounds() {
        return this.r0;
    }

    public final void h(Matrix matrix, int i) {
        float[] fArr = new float[9];
        matrix.getValues(fArr);
        Matrix matrix2 = new Matrix(getImageMatrix());
        matrix2.getValues(this.h0);
        float f = fArr[0];
        float[] fArr2 = this.h0;
        float f2 = f - fArr2[0];
        float f3 = fArr[4] - fArr2[4];
        ValueAnimator ofFloat = ValueAnimator.ofFloat(Utils.FLOAT_EPSILON, 1.0f);
        this.D0 = ofFloat;
        ofFloat.addUpdateListener(new a(matrix2, fArr[2] - fArr2[2], fArr[5] - fArr2[5], f2, f3));
        this.D0.addListener(new b(matrix));
        this.D0.setDuration(i);
        this.D0.start();
    }

    public final void i() {
        h(this.g0, 200);
    }

    public final void j() {
        if (getCurrentDisplayedWidth() > getWidth()) {
            RectF rectF = this.n0;
            if (rectF.left > Utils.FLOAT_EPSILON) {
                g(2, Utils.FLOAT_EPSILON);
                return;
            } else if (rectF.right < getWidth()) {
                g(2, (this.n0.left + getWidth()) - this.n0.right);
                return;
            } else {
                return;
            }
        }
        RectF rectF2 = this.n0;
        if (rectF2.left < Utils.FLOAT_EPSILON) {
            g(2, Utils.FLOAT_EPSILON);
        } else if (rectF2.right > getWidth()) {
            g(2, (this.n0.left + getWidth()) - this.n0.right);
        }
    }

    public final void k() {
        if (getCurrentDisplayedHeight() > getHeight()) {
            RectF rectF = this.n0;
            if (rectF.top > Utils.FLOAT_EPSILON) {
                g(5, Utils.FLOAT_EPSILON);
                return;
            } else if (rectF.bottom < getHeight()) {
                g(5, (this.n0.top + getHeight()) - this.n0.bottom);
                return;
            } else {
                return;
            }
        }
        RectF rectF2 = this.n0;
        if (rectF2.top < Utils.FLOAT_EPSILON) {
            g(5, Utils.FLOAT_EPSILON);
        } else if (rectF2.bottom > getHeight()) {
            g(5, (this.n0.top + getHeight()) - this.n0.bottom);
        }
    }

    public final void l() {
        if (this.t0) {
            j();
            k();
        }
    }

    public boolean m(MotionEvent motionEvent) {
        return this.B0 > 1 || this.z0 > 1.0f || s();
    }

    public final float n(float f) {
        float width;
        float f2;
        if (getCurrentDisplayedWidth() >= getWidth()) {
            float f3 = this.n0.left;
            if (f3 <= Utils.FLOAT_EPSILON && f3 + f > Utils.FLOAT_EPSILON && !this.C0.isInProgress()) {
                return -this.n0.left;
            }
            if (this.n0.right < getWidth() || this.n0.right + f >= getWidth() || this.C0.isInProgress()) {
                return f;
            }
            width = getWidth();
            f2 = this.n0.right;
        } else if (this.C0.isInProgress()) {
            return f;
        } else {
            RectF rectF = this.n0;
            float f4 = rectF.left;
            if (f4 >= Utils.FLOAT_EPSILON && f4 + f < Utils.FLOAT_EPSILON) {
                return -f4;
            }
            if (rectF.right > getWidth() || this.n0.right + f <= getWidth()) {
                return f;
            }
            width = getWidth();
            f2 = this.n0.right;
        }
        return width - f2;
    }

    public final float o(float f) {
        float height;
        float f2;
        if (getCurrentDisplayedHeight() >= getHeight()) {
            float f3 = this.n0.top;
            if (f3 <= Utils.FLOAT_EPSILON && f3 + f > Utils.FLOAT_EPSILON && !this.C0.isInProgress()) {
                return -this.n0.top;
            }
            if (this.n0.bottom < getHeight() || this.n0.bottom + f >= getHeight() || this.C0.isInProgress()) {
                return f;
            }
            height = getHeight();
            f2 = this.n0.bottom;
        } else if (this.C0.isInProgress()) {
            return f;
        } else {
            RectF rectF = this.n0;
            float f4 = rectF.top;
            if (f4 >= Utils.FLOAT_EPSILON && f4 + f < Utils.FLOAT_EPSILON) {
                return -f4;
            }
            if (rectF.bottom > getHeight() || this.n0.bottom + f <= getHeight()) {
                return f;
            }
            height = getHeight();
            f2 = this.n0.bottom;
        }
        return height - f2;
    }

    @Override // android.view.ScaleGestureDetector.OnScaleGestureListener
    public boolean onScale(ScaleGestureDetector scaleGestureDetector) {
        float scaleFactor = this.x0 * scaleGestureDetector.getScaleFactor();
        float[] fArr = this.h0;
        float f = scaleFactor / fArr[0];
        this.y0 = f;
        float f2 = f * fArr[0];
        float f3 = this.l0;
        if (f2 < f3) {
            this.y0 = f3 / fArr[0];
        } else {
            float f4 = this.m0;
            if (f2 > f4) {
                this.y0 = f4 / fArr[0];
            }
        }
        return false;
    }

    @Override // android.view.ScaleGestureDetector.OnScaleGestureListener
    public boolean onScaleBegin(ScaleGestureDetector scaleGestureDetector) {
        this.x0 = this.h0[0];
        return true;
    }

    @Override // android.view.ScaleGestureDetector.OnScaleGestureListener
    public void onScaleEnd(ScaleGestureDetector scaleGestureDetector) {
        this.y0 = 1.0f;
    }

    @Override // android.view.View
    public boolean onTouchEvent(MotionEvent motionEvent) {
        if (!isClickable() && isEnabled() && (this.p0 || this.o0)) {
            ImageView.ScaleType scaleType = getScaleType();
            ImageView.ScaleType scaleType2 = ImageView.ScaleType.MATRIX;
            if (scaleType != scaleType2) {
                super.setScaleType(scaleType2);
            }
            if (this.i0 == null) {
                w();
            }
            this.B0 = motionEvent.getPointerCount();
            this.f0.set(getImageMatrix());
            this.f0.getValues(this.h0);
            x(this.h0);
            this.C0.onTouchEvent(motionEvent);
            this.E0.onTouchEvent(motionEvent);
            if (this.q0 && this.F0) {
                this.F0 = false;
                this.G0 = false;
                if (this.h0[0] != this.i0[0]) {
                    t();
                } else {
                    Matrix matrix = new Matrix(this.f0);
                    float f = this.u0;
                    matrix.postScale(f, f, this.C0.getFocusX(), this.C0.getFocusY());
                    h(matrix, 200);
                }
                return true;
            }
            if (!this.G0) {
                if (motionEvent.getActionMasked() != 0 && this.B0 == this.A0) {
                    if (motionEvent.getActionMasked() == 2) {
                        float focusX = this.C0.getFocusX();
                        float focusY = this.C0.getFocusY();
                        if (e(motionEvent)) {
                            this.f0.postTranslate(p(focusX, this.w0.x), q(focusY, this.w0.y));
                        }
                        if (f(motionEvent)) {
                            Matrix matrix2 = this.f0;
                            float f2 = this.y0;
                            matrix2.postScale(f2, f2, focusX, focusY);
                            this.z0 = this.h0[0] / this.i0[0];
                        }
                        setImageMatrix(this.f0);
                        this.w0.set(focusX, focusY);
                    }
                } else {
                    this.w0.set(this.C0.getFocusX(), this.C0.getFocusY());
                }
                if (motionEvent.getActionMasked() == 1 || motionEvent.getActionMasked() == 3) {
                    this.y0 = 1.0f;
                    v();
                }
            }
            getParent().requestDisallowInterceptTouchEvent(m(motionEvent));
            this.A0 = this.B0;
            return true;
        }
        return super.onTouchEvent(motionEvent);
    }

    public final float p(float f, float f2) {
        float f3 = f - f2;
        if (this.r0) {
            f3 = n(f3);
        }
        RectF rectF = this.n0;
        float f4 = rectF.right;
        return f4 + f3 < Utils.FLOAT_EPSILON ? -f4 : rectF.left + f3 > ((float) getWidth()) ? getWidth() - this.n0.left : f3;
    }

    public final float q(float f, float f2) {
        float f3 = f - f2;
        if (this.r0) {
            f3 = o(f3);
        }
        RectF rectF = this.n0;
        float f4 = rectF.bottom;
        return f4 + f3 < Utils.FLOAT_EPSILON ? -f4 : rectF.top + f3 > ((float) getHeight()) ? getHeight() - this.n0.top : f3;
    }

    public final void r(Context context, AttributeSet attributeSet) {
        this.C0 = new ScaleGestureDetector(context, this);
        this.E0 = new GestureDetector(context, this.H0);
        mc3.a(this.C0, false);
        this.a = getScaleType();
        TypedArray obtainStyledAttributes = context.obtainStyledAttributes(attributeSet, s23.ZoomageView);
        this.p0 = obtainStyledAttributes.getBoolean(9, true);
        this.o0 = obtainStyledAttributes.getBoolean(8, true);
        this.s0 = obtainStyledAttributes.getBoolean(0, true);
        this.t0 = obtainStyledAttributes.getBoolean(1, true);
        this.r0 = obtainStyledAttributes.getBoolean(7, false);
        this.q0 = obtainStyledAttributes.getBoolean(3, true);
        this.j0 = obtainStyledAttributes.getFloat(6, 0.6f);
        this.k0 = obtainStyledAttributes.getFloat(5, 8.0f);
        this.u0 = obtainStyledAttributes.getFloat(4, 3.0f);
        this.v0 = gk.a(obtainStyledAttributes.getInt(2, 0));
        y();
        obtainStyledAttributes.recycle();
    }

    public final boolean s() {
        ValueAnimator valueAnimator = this.D0;
        return valueAnimator != null && valueAnimator.isRunning();
    }

    public void setAnimateOnReset(boolean z) {
        this.s0 = z;
    }

    public void setAutoCenter(boolean z) {
        this.t0 = z;
    }

    public void setAutoResetMode(int i) {
        this.v0 = i;
    }

    public void setDoubleTapToZoom(boolean z) {
        this.q0 = z;
    }

    public void setDoubleTapToZoomScaleFactor(float f) {
        this.u0 = f;
        y();
    }

    @Override // android.view.View
    public void setEnabled(boolean z) {
        super.setEnabled(z);
        if (z) {
            return;
        }
        setScaleType(this.a);
    }

    @Override // androidx.appcompat.widget.AppCompatImageView, android.widget.ImageView
    public void setImageBitmap(Bitmap bitmap) {
        super.setImageBitmap(bitmap);
        setScaleType(this.a);
    }

    @Override // androidx.appcompat.widget.AppCompatImageView, android.widget.ImageView
    public void setImageDrawable(Drawable drawable) {
        super.setImageDrawable(drawable);
        setScaleType(this.a);
    }

    @Override // androidx.appcompat.widget.AppCompatImageView, android.widget.ImageView
    public void setImageResource(int i) {
        super.setImageResource(i);
        setScaleType(this.a);
    }

    @Override // androidx.appcompat.widget.AppCompatImageView, android.widget.ImageView
    public void setImageURI(Uri uri) {
        super.setImageURI(uri);
        setScaleType(this.a);
    }

    public void setRestrictBounds(boolean z) {
        this.r0 = z;
    }

    public void setScaleRange(float f, float f2) {
        this.j0 = f;
        this.k0 = f2;
        this.i0 = null;
        y();
    }

    @Override // android.widget.ImageView
    public void setScaleType(ImageView.ScaleType scaleType) {
        if (scaleType != null) {
            super.setScaleType(scaleType);
            this.a = scaleType;
            this.i0 = null;
        }
    }

    public void setTranslatable(boolean z) {
        this.o0 = z;
    }

    public void setZoomable(boolean z) {
        this.p0 = z;
    }

    public void t() {
        u(this.s0);
    }

    public void u(boolean z) {
        if (z) {
            i();
        } else {
            setImageMatrix(this.g0);
        }
    }

    public final void v() {
        int i = this.v0;
        if (i == 0) {
            if (this.h0[0] <= this.i0[0]) {
                t();
            } else {
                l();
            }
        } else if (i == 1) {
            if (this.h0[0] >= this.i0[0]) {
                t();
            } else {
                l();
            }
        } else if (i == 2) {
            t();
        } else if (i != 3) {
        } else {
            l();
        }
    }

    public final void w() {
        this.i0 = new float[9];
        Matrix matrix = new Matrix(getImageMatrix());
        this.g0 = matrix;
        matrix.getValues(this.i0);
        float f = this.j0;
        float[] fArr = this.i0;
        this.l0 = f * fArr[0];
        this.m0 = this.k0 * fArr[0];
    }

    public final void x(float[] fArr) {
        if (getDrawable() != null) {
            this.n0.set(fArr[2], fArr[5], (getDrawable().getIntrinsicWidth() * fArr[0]) + fArr[2], (getDrawable().getIntrinsicHeight() * fArr[4]) + fArr[5]);
        }
    }

    public final void y() {
        float f = this.j0;
        float f2 = this.k0;
        if (f >= f2) {
            throw new IllegalStateException("minScale must be less than maxScale");
        }
        if (f < Utils.FLOAT_EPSILON) {
            throw new IllegalStateException("minScale must be greater than 0");
        }
        if (f2 >= Utils.FLOAT_EPSILON) {
            if (this.u0 > f2) {
                this.u0 = f2;
            }
            if (this.u0 < f) {
                this.u0 = f;
                return;
            }
            return;
        }
        throw new IllegalStateException("maxScale must be greater than 0");
    }

    public ZoomageView(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        this.f0 = new Matrix();
        this.g0 = new Matrix();
        this.h0 = new float[9];
        this.i0 = null;
        this.j0 = 0.6f;
        this.k0 = 8.0f;
        this.l0 = 0.6f;
        this.m0 = 8.0f;
        this.n0 = new RectF();
        this.w0 = new PointF(Utils.FLOAT_EPSILON, Utils.FLOAT_EPSILON);
        this.x0 = 1.0f;
        this.y0 = 1.0f;
        this.z0 = 1.0f;
        this.A0 = 1;
        this.B0 = 0;
        this.F0 = false;
        this.G0 = false;
        this.H0 = new d();
        r(context, attributeSet);
    }

    public ZoomageView(Context context, AttributeSet attributeSet, int i) {
        super(context, attributeSet, i);
        this.f0 = new Matrix();
        this.g0 = new Matrix();
        this.h0 = new float[9];
        this.i0 = null;
        this.j0 = 0.6f;
        this.k0 = 8.0f;
        this.l0 = 0.6f;
        this.m0 = 8.0f;
        this.n0 = new RectF();
        this.w0 = new PointF(Utils.FLOAT_EPSILON, Utils.FLOAT_EPSILON);
        this.x0 = 1.0f;
        this.y0 = 1.0f;
        this.z0 = 1.0f;
        this.A0 = 1;
        this.B0 = 0;
        this.F0 = false;
        this.G0 = false;
        this.H0 = new d();
        r(context, attributeSet);
    }
}
