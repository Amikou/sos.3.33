package net.safemoon.androidwallet.views;

import android.content.ClipboardManager;
import kotlin.jvm.internal.Lambda;
import net.safemoon.androidwallet.activity.common.BasicActivity;

/* compiled from: CustomRecoveryWalletLayout.kt */
/* loaded from: classes2.dex */
public final class CustomRecoveryWalletLayout$manager$2 extends Lambda implements rc1<ClipboardManager> {
    public final /* synthetic */ CustomRecoveryWalletLayout this$0;

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public CustomRecoveryWalletLayout$manager$2(CustomRecoveryWalletLayout customRecoveryWalletLayout) {
        super(0);
        this.this$0 = customRecoveryWalletLayout;
    }

    /* JADX WARN: Can't rename method to resolve collision */
    @Override // defpackage.rc1
    public final ClipboardManager invoke() {
        BasicActivity attachedActivity = this.this$0.getAttachedActivity();
        Object systemService = attachedActivity == null ? null : attachedActivity.getSystemService("clipboard");
        if (systemService instanceof ClipboardManager) {
            return (ClipboardManager) systemService;
        }
        return null;
    }
}
