package net.safemoon.androidwallet.views.editText.autoSize;

import android.content.Context;
import android.content.res.Resources;
import android.content.res.TypedArray;
import android.os.Build;
import android.text.Editable;
import android.text.Layout;
import android.text.StaticLayout;
import android.text.TextPaint;
import android.text.TextWatcher;
import android.text.method.SingleLineTransformationMethod;
import android.text.method.TransformationMethod;
import android.util.AttributeSet;
import android.util.DisplayMetrics;
import android.util.TypedValue;
import android.view.View;
import android.widget.TextView;
import com.github.mikephil.charting.utils.Utils;
import java.util.ArrayList;
import java.util.Iterator;

/* compiled from: AutofitHelper.java */
/* loaded from: classes2.dex */
public class a {
    public TextView a;
    public TextPaint b;
    public float c;
    public int d;
    public float e;
    public float f;
    public float g;
    public boolean h;
    public boolean i;
    public ArrayList<d> j;
    public TextWatcher k = new c();
    public View.OnLayoutChangeListener l = new b();

    /* compiled from: AutofitHelper.java */
    /* loaded from: classes2.dex */
    public class b implements View.OnLayoutChangeListener {
        public b() {
        }

        @Override // android.view.View.OnLayoutChangeListener
        public void onLayoutChange(View view, int i, int i2, int i3, int i4, int i5, int i6, int i7, int i8) {
            a.this.c();
        }
    }

    /* compiled from: AutofitHelper.java */
    /* loaded from: classes2.dex */
    public class c implements TextWatcher {
        public c() {
        }

        @Override // android.text.TextWatcher
        public void afterTextChanged(Editable editable) {
        }

        @Override // android.text.TextWatcher
        public void beforeTextChanged(CharSequence charSequence, int i, int i2, int i3) {
        }

        @Override // android.text.TextWatcher
        public void onTextChanged(CharSequence charSequence, int i, int i2, int i3) {
            a.this.c();
        }
    }

    /* compiled from: AutofitHelper.java */
    /* loaded from: classes2.dex */
    public interface d {
        void b(float f, float f2);
    }

    public a(TextView textView) {
        float f = textView.getContext().getResources().getDisplayMetrics().scaledDensity;
        this.a = textView;
        this.b = new TextPaint();
        v(textView.getTextSize());
        this.d = i(textView);
        this.e = f * 8.0f;
        this.f = this.c;
        this.g = 0.5f;
    }

    public static void d(TextView textView, TextPaint textPaint, float f, float f2, int i, float f3) {
        int width;
        if (i <= 0 || i == Integer.MAX_VALUE || (width = (textView.getWidth() - textView.getPaddingLeft()) - textView.getPaddingRight()) <= 0) {
            return;
        }
        CharSequence text = textView.getText();
        TransformationMethod transformationMethod = textView.getTransformationMethod();
        if (transformationMethod != null) {
            text = transformationMethod.getTransformation(text, textView);
        }
        Context context = textView.getContext();
        Resources system = Resources.getSystem();
        if (context != null) {
            system = context.getResources();
        }
        DisplayMetrics displayMetrics = system.getDisplayMetrics();
        textPaint.set(textView.getPaint());
        textPaint.setTextSize(f2);
        float g = ((i != 1 || textPaint.measureText(text, 0, text.length()) <= ((float) width)) && h(text, textPaint, f2, (float) width, displayMetrics) <= i) ? f2 : g(text, textPaint, width, i, Utils.FLOAT_EPSILON, f2, f3, displayMetrics);
        if (g < f) {
            g = f;
        }
        textView.setTextSize(0, g);
    }

    public static a e(TextView textView) {
        return f(textView, null, 0);
    }

    public static a f(TextView textView, AttributeSet attributeSet, int i) {
        a aVar = new a(textView);
        boolean z = true;
        if (attributeSet != null) {
            Context context = textView.getContext();
            float l = aVar.l();
            TypedArray obtainStyledAttributes = context.obtainStyledAttributes(attributeSet, s23.AutofitEditText, i, 0);
            boolean z2 = obtainStyledAttributes.getBoolean(2, true);
            int dimensionPixelSize = obtainStyledAttributes.getDimensionPixelSize(0, (int) aVar.k());
            float f = obtainStyledAttributes.getFloat(1, l);
            obtainStyledAttributes.recycle();
            aVar.r(0, dimensionPixelSize).s(f);
            z = z2;
        }
        aVar.n(z);
        return aVar;
    }

    public static float g(CharSequence charSequence, TextPaint textPaint, float f, int i, float f2, float f3, float f4, DisplayMetrics displayMetrics) {
        StaticLayout staticLayout;
        int i2;
        float f5;
        float f6 = (f2 + f3) / 2.0f;
        textPaint.setTextSize(TypedValue.applyDimension(0, f6, displayMetrics));
        if (i != 1) {
            staticLayout = new StaticLayout(charSequence, textPaint, (int) f, Layout.Alignment.ALIGN_NORMAL, 1.0f, Utils.FLOAT_EPSILON, true);
            i2 = staticLayout.getLineCount();
        } else {
            staticLayout = null;
            i2 = 1;
        }
        if (i2 > i) {
            return f3 - f2 < f4 ? f2 : g(charSequence, textPaint, f, i, f2, f6, f4, displayMetrics);
        } else if (i2 < i) {
            return g(charSequence, textPaint, f, i, f6, f3, f4, displayMetrics);
        } else {
            float f7 = Utils.FLOAT_EPSILON;
            if (i == 1) {
                f5 = textPaint.measureText(charSequence, 0, charSequence.length());
            } else {
                for (int i3 = 0; i3 < i2; i3++) {
                    if (staticLayout.getLineWidth(i3) > f7) {
                        f7 = staticLayout.getLineWidth(i3);
                    }
                }
                f5 = f7;
            }
            if (f3 - f2 < f4) {
                return f2;
            }
            if (f5 > f) {
                return g(charSequence, textPaint, f, i, f2, f6, f4, displayMetrics);
            }
            return f5 < f ? g(charSequence, textPaint, f, i, f6, f3, f4, displayMetrics) : f6;
        }
    }

    public static int h(CharSequence charSequence, TextPaint textPaint, float f, float f2, DisplayMetrics displayMetrics) {
        textPaint.setTextSize(TypedValue.applyDimension(0, f, displayMetrics));
        return new StaticLayout(charSequence, textPaint, (int) f2, Layout.Alignment.ALIGN_NORMAL, 1.0f, Utils.FLOAT_EPSILON, true).getLineCount();
    }

    public static int i(TextView textView) {
        TransformationMethod transformationMethod = textView.getTransformationMethod();
        if (transformationMethod == null || !(transformationMethod instanceof SingleLineTransformationMethod)) {
            if (Build.VERSION.SDK_INT >= 16) {
                return textView.getMaxLines();
            }
            return -1;
        }
        return 1;
    }

    public a b(d dVar) {
        if (this.j == null) {
            this.j = new ArrayList<>();
        }
        this.j.add(dVar);
        return this;
    }

    public final void c() {
        float textSize = this.a.getTextSize();
        this.i = true;
        d(this.a, this.b, this.e, this.f, this.d, this.g);
        this.i = false;
        float textSize2 = this.a.getTextSize();
        if (textSize2 != textSize) {
            m(textSize2, textSize);
        }
    }

    public float j() {
        return this.f;
    }

    public float k() {
        return this.e;
    }

    public float l() {
        return this.g;
    }

    public final void m(float f, float f2) {
        ArrayList<d> arrayList = this.j;
        if (arrayList == null) {
            return;
        }
        Iterator<d> it = arrayList.iterator();
        while (it.hasNext()) {
            it.next().b(f, f2);
        }
    }

    public a n(boolean z) {
        if (this.h != z) {
            this.h = z;
            if (z) {
                this.a.addTextChangedListener(this.k);
                this.a.addOnLayoutChangeListener(this.l);
                c();
            } else {
                this.a.removeTextChangedListener(this.k);
                this.a.removeOnLayoutChangeListener(this.l);
                this.a.setTextSize(0, this.c);
            }
        }
        return this;
    }

    public a o(int i) {
        if (this.d != i) {
            this.d = i;
            c();
        }
        return this;
    }

    public a p(float f) {
        return q(2, f);
    }

    public a q(int i, float f) {
        Context context = this.a.getContext();
        Resources system = Resources.getSystem();
        if (context != null) {
            system = context.getResources();
        }
        t(TypedValue.applyDimension(i, f, system.getDisplayMetrics()));
        return this;
    }

    public a r(int i, float f) {
        Context context = this.a.getContext();
        Resources system = Resources.getSystem();
        if (context != null) {
            system = context.getResources();
        }
        u(TypedValue.applyDimension(i, f, system.getDisplayMetrics()));
        return this;
    }

    public a s(float f) {
        if (this.g != f) {
            this.g = f;
            c();
        }
        return this;
    }

    public final void t(float f) {
        if (f != this.f) {
            this.f = f;
            c();
        }
    }

    public final void u(float f) {
        if (f != this.e) {
            this.e = f;
            c();
        }
    }

    public final void v(float f) {
        if (this.c != f) {
            this.c = f;
        }
    }

    public void w(int i, float f) {
        if (this.i) {
            return;
        }
        Context context = this.a.getContext();
        Resources system = Resources.getSystem();
        if (context != null) {
            system = context.getResources();
        }
        v(TypedValue.applyDimension(i, f, system.getDisplayMetrics()));
    }
}
