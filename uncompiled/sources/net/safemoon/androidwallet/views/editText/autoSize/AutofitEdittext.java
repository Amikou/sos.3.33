package net.safemoon.androidwallet.views.editText.autoSize;

import android.content.Context;
import android.graphics.Rect;
import android.util.AttributeSet;
import androidx.appcompat.widget.AppCompatEditText;
import net.safemoon.androidwallet.views.editText.autoSize.a;

/* loaded from: classes2.dex */
public class AutofitEdittext extends AppCompatEditText implements a.d {
    public net.safemoon.androidwallet.views.editText.autoSize.a i0;
    public a j0;

    /* loaded from: classes2.dex */
    public interface a {
        void a(boolean z);
    }

    public AutofitEdittext(Context context) {
        super(context);
        d(context, null, 0);
    }

    @Override // net.safemoon.androidwallet.views.editText.autoSize.a.d
    public void b(float f, float f2) {
    }

    public void c(a aVar) {
        this.j0 = aVar;
    }

    public final void d(Context context, AttributeSet attributeSet, int i) {
        this.i0 = net.safemoon.androidwallet.views.editText.autoSize.a.f(this, attributeSet, i).b(this);
    }

    public net.safemoon.androidwallet.views.editText.autoSize.a getAutofitHelper() {
        return this.i0;
    }

    public float getMaxTextSize() {
        return this.i0.j();
    }

    public float getMinTextSize() {
        return this.i0.k();
    }

    public float getPrecision() {
        return this.i0.l();
    }

    @Override // android.widget.TextView, android.view.View
    public void onFocusChanged(boolean z, int i, Rect rect) {
        super.onFocusChanged(z, i, rect);
        a aVar = this.j0;
        if (aVar != null) {
            aVar.a(z);
        }
    }

    @Override // android.widget.TextView
    public void setLines(int i) {
        super.setLines(i);
        net.safemoon.androidwallet.views.editText.autoSize.a aVar = this.i0;
        if (aVar != null) {
            aVar.o(i);
        }
    }

    @Override // android.widget.TextView
    public void setMaxLines(int i) {
        super.setMaxLines(i);
        net.safemoon.androidwallet.views.editText.autoSize.a aVar = this.i0;
        if (aVar != null) {
            aVar.o(i);
        }
    }

    public void setMaxTextSize(float f) {
        this.i0.p(f);
    }

    public void setMinTextSize(int i) {
        this.i0.r(2, i);
    }

    public void setPrecision(float f) {
        this.i0.s(f);
    }

    public void setSizeToFit() {
        setSizeToFit(true);
    }

    @Override // android.widget.TextView
    public void setTextSize(int i, float f) {
        super.setTextSize(i, f);
        net.safemoon.androidwallet.views.editText.autoSize.a aVar = this.i0;
        if (aVar != null) {
            aVar.w(i, f);
        }
    }

    public void setMaxTextSize(int i, float f) {
        this.i0.q(i, f);
    }

    public void setMinTextSize(int i, float f) {
        this.i0.r(i, f);
    }

    public void setSizeToFit(boolean z) {
        this.i0.n(z);
    }

    public AutofitEdittext(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        d(context, attributeSet, 0);
    }

    public AutofitEdittext(Context context, AttributeSet attributeSet, int i) {
        super(context, attributeSet, i);
        d(context, attributeSet, i);
    }
}
