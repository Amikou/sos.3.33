package net.safemoon.androidwallet.views.editText.autoSize;

import android.annotation.TargetApi;
import android.content.Context;
import android.content.res.Resources;
import android.graphics.RectF;
import android.graphics.Typeface;
import android.text.Layout;
import android.text.StaticLayout;
import android.text.TextPaint;
import android.util.AttributeSet;
import android.util.SparseIntArray;
import android.util.TypedValue;
import androidx.appcompat.widget.AppCompatEditText;
import com.github.mikephil.charting.utils.Utils;

/* loaded from: classes2.dex */
public class AutoSizeEditText extends AppCompatEditText {
    public final RectF i0;
    public final SparseIntArray j0;
    public final b k0;
    public float l0;
    public float m0;
    public float n0;
    public Float o0;
    public int p0;
    public int q0;
    public boolean r0;
    public boolean s0;
    public TextPaint t0;

    /* loaded from: classes2.dex */
    public class a implements b {
        public final RectF a = new RectF();

        public a() {
        }

        @Override // net.safemoon.androidwallet.views.editText.autoSize.AutoSizeEditText.b
        @TargetApi(16)
        public int a(int i, RectF rectF) {
            AutoSizeEditText.this.t0.setTextSize(i);
            String obj = AutoSizeEditText.this.getText().toString();
            if (AutoSizeEditText.this.getMaxLines() == 1) {
                this.a.bottom = AutoSizeEditText.this.t0.getFontSpacing();
                this.a.right = AutoSizeEditText.this.t0.measureText(obj);
            } else {
                StaticLayout staticLayout = new StaticLayout(obj, AutoSizeEditText.this.t0, AutoSizeEditText.this.p0, Layout.Alignment.ALIGN_NORMAL, AutoSizeEditText.this.m0, AutoSizeEditText.this.n0, true);
                if (AutoSizeEditText.this.getMaxLines() != -1 && staticLayout.getLineCount() > AutoSizeEditText.this.getMaxLines()) {
                    return 1;
                }
                this.a.bottom = staticLayout.getHeight();
                int i2 = -1;
                for (int i3 = 0; i3 < staticLayout.getLineCount(); i3++) {
                    if (i2 < staticLayout.getLineWidth(i3)) {
                        i2 = (int) staticLayout.getLineWidth(i3);
                    }
                }
                this.a.right = i2;
            }
            this.a.offsetTo(Utils.FLOAT_EPSILON, Utils.FLOAT_EPSILON);
            return rectF.contains(this.a) ? -1 : 1;
        }
    }

    /* loaded from: classes2.dex */
    public interface b {
        int a(int i, RectF rectF);
    }

    public AutoSizeEditText(Context context) {
        this(context, null, 0);
    }

    public final void g() {
        if (this.s0) {
            int round = Math.round(this.o0.floatValue());
            int measuredHeight = (getMeasuredHeight() - getCompoundPaddingBottom()) - getCompoundPaddingTop();
            int measuredWidth = (getMeasuredWidth() - getCompoundPaddingLeft()) - getCompoundPaddingRight();
            this.p0 = measuredWidth;
            if (measuredWidth <= 0) {
                return;
            }
            RectF rectF = this.i0;
            rectF.right = measuredWidth;
            rectF.bottom = measuredHeight;
            super.setTextSize(0, i(round, (int) this.l0, this.k0, rectF));
        }
    }

    @Override // android.widget.TextView
    public int getMaxLines() {
        return this.q0;
    }

    public Float get_minTextSize() {
        return this.o0;
    }

    public final int h(int i, int i2, b bVar, RectF rectF) {
        int i3 = i2 - 1;
        int i4 = i;
        while (i <= i3) {
            i4 = (i + i3) >>> 1;
            int a2 = bVar.a(i4, rectF);
            if (a2 >= 0) {
                if (a2 <= 0) {
                    break;
                }
                i4--;
                i3 = i4;
            } else {
                int i5 = i4 + 1;
                i4 = i;
                i = i5;
            }
        }
        return i4;
    }

    public final int i(int i, int i2, b bVar, RectF rectF) {
        if (!this.r0) {
            return h(i, i2, bVar, rectF);
        }
        String obj = getText().toString();
        int length = obj == null ? 0 : obj.length();
        int i3 = this.j0.get(length);
        if (i3 != 0) {
            return i3;
        }
        int h = h(i, i2, bVar, rectF);
        this.j0.put(length, h);
        return h;
    }

    public final void j() {
        g();
    }

    @Override // android.view.View
    public void onSizeChanged(int i, int i2, int i3, int i4) {
        this.j0.clear();
        super.onSizeChanged(i, i2, i3, i4);
        if (i == i3 && i2 == i4) {
            return;
        }
        j();
    }

    @Override // android.widget.TextView
    public void onTextChanged(CharSequence charSequence, int i, int i2, int i3) {
        super.onTextChanged(charSequence, i, i2, i3);
        j();
    }

    public void setEnableSizeCache(boolean z) {
        this.r0 = z;
        this.j0.clear();
        g();
    }

    @Override // android.widget.TextView
    public void setLineSpacing(float f, float f2) {
        super.setLineSpacing(f, f2);
        this.m0 = f2;
        this.n0 = f;
    }

    @Override // android.widget.TextView
    public void setLines(int i) {
        super.setLines(i);
        this.q0 = i;
        j();
    }

    @Override // android.widget.TextView
    public void setMaxLines(int i) {
        super.setMaxLines(i);
        this.q0 = i;
        j();
    }

    public void setMinTextSize(Float f) {
        this.o0 = f;
        j();
    }

    @Override // android.widget.TextView
    public void setSingleLine() {
        super.setSingleLine();
        this.q0 = 1;
        j();
    }

    @Override // android.widget.TextView
    public void setTextSize(float f) {
        this.l0 = f;
        this.j0.clear();
        g();
    }

    @Override // android.widget.TextView
    public void setTypeface(Typeface typeface) {
        if (this.t0 == null) {
            this.t0 = new TextPaint(getPaint());
        }
        this.t0.setTypeface(typeface);
        super.setTypeface(typeface);
    }

    public AutoSizeEditText(Context context, AttributeSet attributeSet) {
        this(context, attributeSet, 0);
    }

    public AutoSizeEditText(Context context, AttributeSet attributeSet, int i) {
        super(context, attributeSet, i);
        this.i0 = new RectF();
        this.j0 = new SparseIntArray();
        this.m0 = 1.0f;
        this.n0 = Utils.FLOAT_EPSILON;
        this.r0 = true;
        this.o0 = Float.valueOf(TypedValue.applyDimension(2, 12.0f, getResources().getDisplayMetrics()));
        this.l0 = getTextSize();
        if (this.q0 == 0) {
            this.q0 = -1;
        }
        this.k0 = new a();
        this.s0 = true;
    }

    @Override // android.widget.TextView
    public void setSingleLine(boolean z) {
        super.setSingleLine(z);
        if (z) {
            this.q0 = 1;
        } else {
            this.q0 = -1;
        }
        j();
    }

    @Override // android.widget.TextView
    public void setTextSize(int i, float f) {
        Resources resources;
        Context context = getContext();
        if (context == null) {
            resources = Resources.getSystem();
        } else {
            resources = context.getResources();
        }
        this.l0 = TypedValue.applyDimension(i, f, resources.getDisplayMetrics());
        this.j0.clear();
        g();
    }
}
