package net.safemoon.androidwallet.views.editText.autoSize;

import android.content.Context;
import android.content.res.TypedArray;
import android.util.AttributeSet;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.TextView;
import com.github.mikephil.charting.utils.Utils;
import java.util.WeakHashMap;

/* loaded from: classes2.dex */
public class AutofitLayout extends FrameLayout {
    public boolean a;
    public float f0;
    public float g0;
    public WeakHashMap<View, a> h0;

    public AutofitLayout(Context context) {
        super(context);
        this.h0 = new WeakHashMap<>();
        a(context, null, 0);
    }

    public final void a(Context context, AttributeSet attributeSet, int i) {
        boolean z = true;
        int i2 = -1;
        float f = -1.0f;
        if (attributeSet != null) {
            TypedArray obtainStyledAttributes = context.obtainStyledAttributes(attributeSet, s23.AutofitEditText, i, 0);
            boolean z2 = obtainStyledAttributes.getBoolean(2, true);
            i2 = obtainStyledAttributes.getDimensionPixelSize(0, -1);
            f = obtainStyledAttributes.getFloat(1, -1.0f);
            obtainStyledAttributes.recycle();
            z = z2;
        }
        this.a = z;
        this.f0 = i2;
        this.g0 = f;
    }

    @Override // android.view.ViewGroup
    public void addView(View view, int i, ViewGroup.LayoutParams layoutParams) {
        super.addView(view, i, layoutParams);
        TextView textView = (TextView) view;
        a n = a.e(textView).n(this.a);
        float f = this.g0;
        if (f > Utils.FLOAT_EPSILON) {
            n.s(f);
        }
        float f2 = this.f0;
        if (f2 > Utils.FLOAT_EPSILON) {
            n.r(0, f2);
        }
        this.h0.put(textView, n);
    }

    public AutofitLayout(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        this.h0 = new WeakHashMap<>();
        a(context, attributeSet, 0);
    }

    public AutofitLayout(Context context, AttributeSet attributeSet, int i) {
        super(context, attributeSet, i);
        this.h0 = new WeakHashMap<>();
        a(context, attributeSet, i);
    }
}
