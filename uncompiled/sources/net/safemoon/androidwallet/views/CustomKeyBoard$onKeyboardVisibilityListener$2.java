package net.safemoon.androidwallet.views;

import android.content.Context;
import java.util.Objects;
import kotlin.jvm.internal.Lambda;

/* compiled from: CustomKeyBoard.kt */
/* loaded from: classes2.dex */
public final class CustomKeyBoard$onKeyboardVisibilityListener$2 extends Lambda implements rc1<om2> {
    public final /* synthetic */ CustomKeyBoard this$0;

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public CustomKeyBoard$onKeyboardVisibilityListener$2(CustomKeyBoard customKeyBoard) {
        super(0);
        this.this$0 = customKeyBoard;
    }

    @Override // defpackage.rc1
    public final om2 invoke() {
        Context context = this.this$0.getContext();
        Objects.requireNonNull(context, "null cannot be cast to non-null type net.safemoon.androidwallet.interfaces.OnKeyboardVisibilityListener");
        return (om2) context;
    }
}
