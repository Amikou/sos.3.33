package net.safemoon.androidwallet.views;

import android.graphics.Paint;
import kotlin.jvm.internal.Lambda;
import net.safemoon.androidwallet.R;

/* compiled from: ClipRelativeLayout.kt */
/* loaded from: classes2.dex */
public final class ClipRelativeLayout$paint$2 extends Lambda implements rc1<Paint> {
    public final /* synthetic */ ClipRelativeLayout this$0;

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public ClipRelativeLayout$paint$2(ClipRelativeLayout clipRelativeLayout) {
        super(0);
        this.this$0 = clipRelativeLayout;
    }

    /* JADX WARN: Can't rename method to resolve collision */
    @Override // defpackage.rc1
    public final Paint invoke() {
        Paint paint = new Paint();
        paint.setColor(m70.d(this.this$0.getContext(), R.color.screen_background_color));
        paint.setAntiAlias(true);
        paint.setStrokeCap(Paint.Cap.ROUND);
        return paint;
    }
}
