package net.safemoon.androidwallet.views.gesture;

import android.annotation.SuppressLint;
import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Point;
import android.graphics.Rect;
import android.graphics.RectF;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.view.MotionEvent;
import android.view.View;
import androidx.recyclerview.widget.RecyclerView;
import androidx.recyclerview.widget.k;
import com.github.mikephil.charting.utils.Utils;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Queue;
import net.safemoon.androidwallet.views.gesture.WalletRecyclerItemSwipeHelper;

/* compiled from: WalletRecyclerItemSwipeHelper.kt */
@SuppressLint({"ClickableViewAccessibility"})
/* loaded from: classes2.dex */
public abstract class WalletRecyclerItemSwipeHelper {
    public final bt1 a;
    public final RecyclerView b;
    public final lc2 c;
    public final Map<Integer, List<c>> d;
    public List<c> e;
    public Queue<Integer> f;
    public int g;
    public float h;
    public final d i;

    /* compiled from: WalletRecyclerItemSwipeHelper.kt */
    /* loaded from: classes2.dex */
    public static final class a extends RecyclerView.r {
        public a() {
        }

        @Override // androidx.recyclerview.widget.RecyclerView.r
        @SuppressLint({"NotifyDataSetChanged"})
        public void onScrollStateChanged(RecyclerView recyclerView, int i) {
            fs1.f(recyclerView, "recyclerView");
            super.onScrollStateChanged(recyclerView, i);
            if (WalletRecyclerItemSwipeHelper.this.g >= 0 || WalletRecyclerItemSwipeHelper.this.e.size() > 0) {
                WalletRecyclerItemSwipeHelper.this.g = -1;
                WalletRecyclerItemSwipeHelper.this.e.clear();
                RecyclerView.Adapter adapter = recyclerView.getAdapter();
                if (adapter == null) {
                    return;
                }
                adapter.notifyDataSetChanged();
            }
        }
    }

    /* compiled from: WalletRecyclerItemSwipeHelper.kt */
    /* loaded from: classes2.dex */
    public final class b extends k.i {
        public float f;
        public boolean g;
        public final /* synthetic */ WalletRecyclerItemSwipeHelper h;

        /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
        public b(WalletRecyclerItemSwipeHelper walletRecyclerItemSwipeHelper) {
            super(3, 4);
            fs1.f(walletRecyclerItemSwipeHelper, "this$0");
            this.h = walletRecyclerItemSwipeHelper;
            this.f = t40.a(80);
        }

        public static final void G(WalletRecyclerItemSwipeHelper walletRecyclerItemSwipeHelper, RecyclerView.a0 a0Var) {
            fs1.f(walletRecyclerItemSwipeHelper, "this$0");
            fs1.f(a0Var, "$viewHolder");
            walletRecyclerItemSwipeHelper.c.notifyItemChanged(a0Var.getAbsoluteAdapterPosition());
            walletRecyclerItemSwipeHelper.c.notifyItemChanged(walletRecyclerItemSwipeHelper.c.getItemCount() - 1);
            if (walletRecyclerItemSwipeHelper.c.getItemCount() > 2) {
                walletRecyclerItemSwipeHelper.c.notifyItemChanged(walletRecyclerItemSwipeHelper.c.getItemCount() - 2);
            }
        }

        public static final void I(WalletRecyclerItemSwipeHelper walletRecyclerItemSwipeHelper, int i) {
            fs1.f(walletRecyclerItemSwipeHelper, "this$0");
            RecyclerView.Adapter adapter = walletRecyclerItemSwipeHelper.b.getAdapter();
            fs1.d(adapter);
            adapter.notifyItemChanged(i);
        }

        @Override // androidx.recyclerview.widget.k.f
        public void A(RecyclerView.a0 a0Var, int i) {
            super.A(a0Var, i);
            if (i == 2) {
                View view = a0Var == null ? null : a0Var.itemView;
                if (view == null) {
                    return;
                }
                view.setAlpha(1.0f);
            }
        }

        @Override // androidx.recyclerview.widget.k.f
        public void B(RecyclerView.a0 a0Var, int i) {
            fs1.f(a0Var, "viewHolder");
            this.g = true;
            final int layoutPosition = a0Var.getLayoutPosition();
            if (this.h.g != layoutPosition) {
                this.h.f.add(Integer.valueOf(this.h.g));
            }
            this.h.g = layoutPosition;
            if (!this.h.d.containsKey(Integer.valueOf(this.h.g))) {
                this.h.e.clear();
            } else {
                WalletRecyclerItemSwipeHelper walletRecyclerItemSwipeHelper = this.h;
                Object obj = walletRecyclerItemSwipeHelper.d.get(Integer.valueOf(this.h.g));
                fs1.d(obj);
                walletRecyclerItemSwipeHelper.e = (List) obj;
            }
            this.h.d.clear();
            WalletRecyclerItemSwipeHelper walletRecyclerItemSwipeHelper2 = this.h;
            walletRecyclerItemSwipeHelper2.h = walletRecyclerItemSwipeHelper2.e.size() * 0.5f * this.f;
            this.h.o();
            if (this.h.c.b(a0Var)) {
                return;
            }
            RecyclerView recyclerView = this.h.b;
            final WalletRecyclerItemSwipeHelper walletRecyclerItemSwipeHelper3 = this.h;
            recyclerView.post(new Runnable() { // from class: un4
                @Override // java.lang.Runnable
                public final void run() {
                    WalletRecyclerItemSwipeHelper.b.I(WalletRecyclerItemSwipeHelper.this, layoutPosition);
                }
            });
        }

        public final void H(Canvas canvas, View view, List<c> list, int i, float f) {
            float right = view.getRight() - view.getHeight();
            float size = ((-1) * f) / list.size();
            for (c cVar : list) {
                float right2 = (view.getRight() - view.getPaddingEnd()) - size;
                cVar.b(canvas, new RectF(right2, view.getTop(), right, view.getBottom()), i, view);
                right = right2;
            }
        }

        @Override // androidx.recyclerview.widget.k.f
        public void c(RecyclerView recyclerView, final RecyclerView.a0 a0Var) {
            fs1.f(recyclerView, "recyclerView");
            fs1.f(a0Var, "viewHolder");
            super.c(recyclerView, a0Var);
            if (!this.g) {
                final WalletRecyclerItemSwipeHelper walletRecyclerItemSwipeHelper = this.h;
                recyclerView.post(new Runnable() { // from class: vn4
                    @Override // java.lang.Runnable
                    public final void run() {
                        WalletRecyclerItemSwipeHelper.b.G(WalletRecyclerItemSwipeHelper.this, a0Var);
                    }
                });
            }
            this.h.m().a();
        }

        @Override // androidx.recyclerview.widget.k.f
        public float l(float f) {
            return f * 0.1f;
        }

        @Override // androidx.recyclerview.widget.k.f
        public float m(RecyclerView.a0 a0Var) {
            fs1.f(a0Var, "viewHolder");
            return this.h.h;
        }

        @Override // androidx.recyclerview.widget.k.f
        public float n(float f) {
            return f * 50.0f;
        }

        @Override // androidx.recyclerview.widget.k.f
        public void u(Canvas canvas, RecyclerView recyclerView, RecyclerView.a0 a0Var, float f, float f2, int i, boolean z) {
            float f3;
            fs1.f(canvas, "c");
            fs1.f(recyclerView, "recyclerView");
            fs1.f(a0Var, "viewHolder");
            int layoutPosition = a0Var.getLayoutPosition();
            float width = (this.f * f) / a0Var.itemView.getWidth();
            View view = a0Var.itemView;
            fs1.e(view, "viewHolder.itemView");
            if (layoutPosition < 0) {
                this.h.g = layoutPosition;
            } else if (this.h.c.b(a0Var)) {
                if (i != 1 || f >= Utils.FLOAT_EPSILON) {
                    f3 = width;
                } else {
                    List<c> arrayList = new ArrayList<>();
                    if (this.h.d.containsKey(Integer.valueOf(layoutPosition))) {
                        Object obj = this.h.d.get(Integer.valueOf(layoutPosition));
                        fs1.d(obj);
                        arrayList = (List) obj;
                    } else {
                        this.h.n(a0Var, arrayList);
                        this.h.d.put(Integer.valueOf(layoutPosition), arrayList);
                    }
                    List<c> list = arrayList;
                    float size = ((list.size() * f) * this.f) / view.getWidth();
                    H(canvas, view, list, layoutPosition, size);
                    f3 = size;
                }
                super.u(canvas, recyclerView, a0Var, f3, f2, i, z);
            } else {
                super.u(canvas, recyclerView, a0Var, width, f2, i, z);
            }
        }

        @Override // androidx.recyclerview.widget.k.f
        public boolean y(RecyclerView recyclerView, RecyclerView.a0 a0Var, RecyclerView.a0 a0Var2) {
            fs1.f(recyclerView, "recyclerView");
            fs1.f(a0Var, "viewHolder");
            fs1.f(a0Var2, "target");
            this.g = false;
            this.h.c.c(a0Var.getBindingAdapterPosition(), a0Var2.getAbsoluteAdapterPosition());
            return true;
        }

        @Override // androidx.recyclerview.widget.k.f
        public void z(RecyclerView recyclerView, RecyclerView.a0 a0Var, int i, RecyclerView.a0 a0Var2, int i2, int i3, int i4) {
            fs1.f(recyclerView, "recyclerView");
            fs1.f(a0Var, "viewHolder");
            fs1.f(a0Var2, "target");
            super.z(recyclerView, a0Var, i, a0Var2, i2, i3, i4);
        }
    }

    /* compiled from: WalletRecyclerItemSwipeHelper.kt */
    /* loaded from: classes2.dex */
    public final class c {
        public final Drawable a;
        public final ColorDrawable b;
        public int c;
        public RectF d;
        public final int e;
        public final /* synthetic */ WalletRecyclerItemSwipeHelper f;

        public c(WalletRecyclerItemSwipeHelper walletRecyclerItemSwipeHelper, Drawable drawable, ColorDrawable colorDrawable) {
            fs1.f(walletRecyclerItemSwipeHelper, "this$0");
            this.f = walletRecyclerItemSwipeHelper;
            this.a = drawable;
            this.b = colorDrawable;
            this.e = (int) t40.a(14);
        }

        public final boolean a(float f, float f2) {
            RectF rectF = this.d;
            if (rectF != null) {
                fs1.d(rectF);
                if (rectF.contains(f, f2)) {
                    this.f.m().b(this.c);
                    return true;
                }
                return false;
            }
            return false;
        }

        public final void b(Canvas canvas, RectF rectF, int i, View view) {
            fs1.f(canvas, "c");
            fs1.f(rectF, "rect");
            fs1.f(view, "itemView");
            this.c = i;
            RectF rectF2 = new RectF(rectF.left, view.getTop(), view.getRight() - this.e, view.getBottom());
            this.d = rectF2;
            ColorDrawable colorDrawable = this.b;
            if (colorDrawable != null) {
                fs1.d(rectF2);
                int a = ((int) rectF2.left) + ((int) t40.a(5));
                RectF rectF3 = this.d;
                fs1.d(rectF3);
                int a2 = ((int) rectF3.top) + ((int) t40.a(Double.valueOf(5.5d)));
                RectF rectF4 = this.d;
                fs1.d(rectF4);
                RectF rectF5 = this.d;
                fs1.d(rectF5);
                colorDrawable.setBounds(a, a2, (int) rectF4.right, ((int) rectF5.bottom) - ((int) t40.a(Double.valueOf(5.5d))));
                this.b.draw(canvas);
            }
            Drawable drawable = this.a;
            if (drawable != null) {
                int intrinsicWidth = drawable.getIntrinsicWidth();
                int top = view.getTop() + ((view.getHeight() - this.a.getIntrinsicHeight()) / 2);
                RectF rectF6 = this.d;
                fs1.d(rectF6);
                int i2 = ((int) rectF6.right) - intrinsicWidth;
                Drawable drawable2 = this.a;
                drawable2.setBounds(i2 - intrinsicWidth, top, i2, this.a.getIntrinsicHeight() + top);
                this.a.draw(canvas);
            }
        }
    }

    /* compiled from: WalletRecyclerItemSwipeHelper.kt */
    /* loaded from: classes2.dex */
    public static final class d implements RecyclerView.q {
        public d() {
        }

        @Override // androidx.recyclerview.widget.RecyclerView.q
        public void a(RecyclerView recyclerView, MotionEvent motionEvent) {
            fs1.f(recyclerView, "rv");
            fs1.f(motionEvent, "e");
        }

        public final void b(MotionEvent motionEvent) {
            Iterator it = WalletRecyclerItemSwipeHelper.this.e.iterator();
            while (it.hasNext() && !((c) it.next()).a(motionEvent.getX(), motionEvent.getY())) {
            }
        }

        @Override // androidx.recyclerview.widget.RecyclerView.q
        public boolean c(RecyclerView recyclerView, MotionEvent motionEvent) {
            fs1.f(recyclerView, "rv");
            fs1.f(motionEvent, "e");
            if (WalletRecyclerItemSwipeHelper.this.g < 0) {
                return false;
            }
            Point point = new Point((int) motionEvent.getRawX(), (int) motionEvent.getRawY());
            RecyclerView.a0 Z = WalletRecyclerItemSwipeHelper.this.b.Z(WalletRecyclerItemSwipeHelper.this.g);
            if (Z == null) {
                return false;
            }
            View view = Z.itemView;
            fs1.e(view, "swipedViewHolder.itemView");
            Rect rect = new Rect();
            view.getGlobalVisibleRect(rect);
            if (motionEvent.getAction() == 1) {
                int i = rect.top;
                int i2 = point.y;
                if (i < i2 && rect.bottom > i2) {
                    b(motionEvent);
                }
                WalletRecyclerItemSwipeHelper.this.f.add(Integer.valueOf(WalletRecyclerItemSwipeHelper.this.g));
                WalletRecyclerItemSwipeHelper.this.g = -1;
                WalletRecyclerItemSwipeHelper.this.f.poll();
            }
            return false;
        }

        @Override // androidx.recyclerview.widget.RecyclerView.q
        public void e(boolean z) {
        }
    }

    public WalletRecyclerItemSwipeHelper(Context context, bt1 bt1Var, RecyclerView recyclerView, lc2 lc2Var) {
        fs1.f(context, "context");
        fs1.f(bt1Var, "itemListGestureListener");
        fs1.f(recyclerView, "recyclerView");
        fs1.f(lc2Var, "adapter");
        this.a = bt1Var;
        this.b = recyclerView;
        this.c = lc2Var;
        this.e = new ArrayList();
        this.f = new LinkedList();
        this.g = -1;
        this.h = 0.5f;
        d dVar = new d();
        this.i = dVar;
        this.e = new ArrayList();
        recyclerView.k(dVar);
        recyclerView.l(new a());
        this.d = new HashMap();
        this.f = new LinkedList<Integer>() { // from class: net.safemoon.androidwallet.views.gesture.WalletRecyclerItemSwipeHelper.2
            @Override // java.util.LinkedList, java.util.AbstractList, java.util.AbstractCollection, java.util.Collection, java.util.List, java.util.Deque, java.util.Queue
            public /* bridge */ /* synthetic */ boolean add(Object obj) {
                return add(((Number) obj).intValue());
            }

            public /* bridge */ boolean contains(Integer num) {
                return super.contains((Object) num);
            }

            public /* bridge */ int getSize() {
                return super.size();
            }

            public /* bridge */ int indexOf(Integer num) {
                return super.indexOf((Object) num);
            }

            public /* bridge */ int lastIndexOf(Integer num) {
                return super.lastIndexOf((Object) num);
            }

            @Override // java.util.LinkedList, java.util.AbstractSequentialList, java.util.AbstractList, java.util.List
            public final /* bridge */ Integer remove(int i) {
                return removeAt(i);
            }

            public /* bridge */ Integer removeAt(int i) {
                return (Integer) super.remove(i);
            }

            @Override // java.util.LinkedList, java.util.AbstractCollection, java.util.Collection, java.util.List, java.util.Deque
            public final /* bridge */ int size() {
                return getSize();
            }

            public boolean add(int i) {
                if (contains((Object) Integer.valueOf(i))) {
                    return false;
                }
                return super.add((AnonymousClass2) Integer.valueOf(i));
            }

            @Override // java.util.LinkedList, java.util.AbstractCollection, java.util.Collection, java.util.List, java.util.Deque
            public final /* bridge */ boolean contains(Object obj) {
                if (obj instanceof Integer) {
                    return contains((Integer) obj);
                }
                return false;
            }

            @Override // java.util.LinkedList, java.util.AbstractList, java.util.List
            public final /* bridge */ int indexOf(Object obj) {
                if (obj instanceof Integer) {
                    return indexOf((Integer) obj);
                }
                return -1;
            }

            @Override // java.util.LinkedList, java.util.AbstractList, java.util.List
            public final /* bridge */ int lastIndexOf(Object obj) {
                if (obj instanceof Integer) {
                    return lastIndexOf((Integer) obj);
                }
                return -1;
            }

            public /* bridge */ boolean remove(Integer num) {
                return super.remove((Object) num);
            }

            @Override // java.util.LinkedList, java.util.AbstractCollection, java.util.Collection, java.util.List, java.util.Deque
            public final /* bridge */ boolean remove(Object obj) {
                if (obj == null ? true : obj instanceof Integer) {
                    return remove((Integer) obj);
                }
                return false;
            }
        };
        l();
    }

    public static final void p(WalletRecyclerItemSwipeHelper walletRecyclerItemSwipeHelper, Integer num) {
        fs1.f(walletRecyclerItemSwipeHelper, "this$0");
        RecyclerView.Adapter adapter = walletRecyclerItemSwipeHelper.b.getAdapter();
        fs1.d(adapter);
        fs1.e(num, "pos");
        adapter.notifyItemChanged(num.intValue());
    }

    public final void l() {
        new k(new b(this)).g(this.b);
    }

    public final bt1 m() {
        return this.a;
    }

    public abstract void n(RecyclerView.a0 a0Var, List<c> list);

    public final synchronized void o() {
        while (!this.f.isEmpty()) {
            final Integer poll = this.f.poll();
            fs1.e(poll, "pos");
            if (poll.intValue() > -1) {
                this.b.post(new Runnable() { // from class: tn4
                    @Override // java.lang.Runnable
                    public final void run() {
                        WalletRecyclerItemSwipeHelper.p(WalletRecyclerItemSwipeHelper.this, poll);
                    }
                });
            }
        }
    }
}
