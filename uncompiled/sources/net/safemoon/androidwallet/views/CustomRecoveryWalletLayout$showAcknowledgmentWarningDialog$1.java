package net.safemoon.androidwallet.views;

import kotlin.jvm.internal.Lambda;

/* compiled from: CustomRecoveryWalletLayout.kt */
/* loaded from: classes2.dex */
public final class CustomRecoveryWalletLayout$showAcknowledgmentWarningDialog$1 extends Lambda implements rc1<te4> {
    public final /* synthetic */ int $checkType;
    public final /* synthetic */ CustomRecoveryWalletLayout this$0;

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public CustomRecoveryWalletLayout$showAcknowledgmentWarningDialog$1(int i, CustomRecoveryWalletLayout customRecoveryWalletLayout) {
        super(0);
        this.$checkType = i;
        this.this$0 = customRecoveryWalletLayout;
    }

    @Override // defpackage.rc1
    public /* bridge */ /* synthetic */ te4 invoke() {
        invoke2();
        return te4.a;
    }

    @Override // defpackage.rc1
    /* renamed from: invoke  reason: avoid collision after fix types in other method */
    public final void invoke2() {
        if (this.$checkType == 1) {
            this.this$0.y();
        } else {
            this.this$0.x();
        }
    }
}
