package net.safemoon.androidwallet.views;

import android.content.Context;
import android.text.TextUtils;
import android.util.AttributeSet;
import android.view.View;
import android.view.inputmethod.InputConnection;
import android.widget.Button;
import android.widget.EditText;
import androidx.constraintlayout.widget.ConstraintLayout;
import net.safemoon.androidwallet.R;
import net.safemoon.androidwallet.views.CustomKeyBoard;

/* compiled from: CustomKeyBoard.kt */
/* loaded from: classes2.dex */
public final class CustomKeyBoard extends ConstraintLayout {
    public EditText A0;
    public InputConnection B0;
    public final sy1 y0;
    public final sy1 z0;

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public CustomKeyBoard(Context context) {
        super(context);
        fs1.f(context, "context");
        this.y0 = zy1.a(new CustomKeyBoard$binding$2(this));
        this.z0 = zy1.a(new CustomKeyBoard$onKeyboardVisibilityListener$2(this));
        View.inflate(getContext(), R.layout.view_custom_keyboard, this);
        new View.OnClickListener() { // from class: gc0
            @Override // android.view.View.OnClickListener
            public final void onClick(View view) {
                CustomKeyBoard.x(CustomKeyBoard.this, view);
            }
        };
    }

    private final hi4 getBinding() {
        return (hi4) this.y0.getValue();
    }

    private final om2 getOnKeyboardVisibilityListener() {
        return (om2) this.z0.getValue();
    }

    public static final void x(CustomKeyBoard customKeyBoard, View view) {
        InputConnection inputConnection;
        fs1.f(customKeyBoard, "this$0");
        if (customKeyBoard.B0 == null) {
            return;
        }
        if (fs1.b(view, customKeyBoard.getBinding().a)) {
            InputConnection inputConnection2 = customKeyBoard.B0;
            fs1.d(inputConnection2);
            if (TextUtils.isEmpty(inputConnection2.getSelectedText(0))) {
                InputConnection inputConnection3 = customKeyBoard.B0;
                if (inputConnection3 == null) {
                    return;
                }
                inputConnection3.deleteSurroundingText(1, 0);
                return;
            }
            InputConnection inputConnection4 = customKeyBoard.B0;
            if (inputConnection4 == null) {
                return;
            }
            inputConnection4.commitText("", 1);
        } else if (fs1.b(view, customKeyBoard.getBinding().b)) {
            customKeyBoard.w(false);
        } else if (!(view instanceof Button) || (inputConnection = customKeyBoard.B0) == null) {
        } else {
            CharSequence textBeforeCursor = inputConnection.getTextBeforeCursor(1, 0);
            if (fs1.b(textBeforeCursor, ",")) {
                Button button = (Button) view;
                if (fs1.b(button.getText(), ",") || fs1.b(button.getText(), ".")) {
                    return;
                }
            }
            if (fs1.b(textBeforeCursor, ".")) {
                Button button2 = (Button) view;
                if (fs1.b(button2.getText(), ",") || fs1.b(button2.getText(), ".")) {
                    return;
                }
            }
            inputConnection.commitText(((Button) view).getText(), 1);
        }
    }

    public final EditText getEditText() {
        return this.A0;
    }

    public final void setEditText(EditText editText) {
        this.A0 = editText;
    }

    public final void v(EditText editText) {
        fs1.f(editText, "editText");
    }

    public final void w(boolean z) {
        getOnKeyboardVisibilityListener().i(z);
        setVisibility(z ? 0 : 8);
    }

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public CustomKeyBoard(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        fs1.f(context, "context");
        this.y0 = zy1.a(new CustomKeyBoard$binding$2(this));
        this.z0 = zy1.a(new CustomKeyBoard$onKeyboardVisibilityListener$2(this));
        View.inflate(getContext(), R.layout.view_custom_keyboard, this);
        new View.OnClickListener() { // from class: gc0
            @Override // android.view.View.OnClickListener
            public final void onClick(View view) {
                CustomKeyBoard.x(CustomKeyBoard.this, view);
            }
        };
    }

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public CustomKeyBoard(Context context, AttributeSet attributeSet, int i) {
        super(context, attributeSet, i);
        fs1.f(context, "context");
        this.y0 = zy1.a(new CustomKeyBoard$binding$2(this));
        this.z0 = zy1.a(new CustomKeyBoard$onKeyboardVisibilityListener$2(this));
        View.inflate(getContext(), R.layout.view_custom_keyboard, this);
        new View.OnClickListener() { // from class: gc0
            @Override // android.view.View.OnClickListener
            public final void onClick(View view) {
                CustomKeyBoard.x(CustomKeyBoard.this, view);
            }
        };
    }
}
