package net.safemoon.androidwallet.views;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Path;
import android.graphics.RectF;
import android.util.AttributeSet;
import android.widget.RelativeLayout;
import com.github.mikephil.charting.utils.Utils;

/* compiled from: ClipRelativeLayout.kt */
/* loaded from: classes2.dex */
public final class ClipRelativeLayout extends RelativeLayout {
    public final sy1 a;
    public Path f0;

    public ClipRelativeLayout(Context context) {
        this(context, null);
    }

    private final Paint getPaint() {
        return (Paint) this.a.getValue();
    }

    @Override // android.view.ViewGroup, android.view.View
    public void dispatchDraw(Canvas canvas) {
        super.dispatchDraw(canvas);
    }

    @Override // android.view.View
    public void onSizeChanged(int i, int i2, int i3, int i4) {
        super.onSizeChanged(i, i2, i3, i4);
        RectF rectF = new RectF(Utils.FLOAT_EPSILON, getHeight() - e30.u(this), getWidth(), getHeight());
        this.f0.reset();
        this.f0.moveTo(rectF.left, rectF.bottom);
        this.f0.lineTo(rectF.left, rectF.top);
        float centerX = rectF.centerX();
        float f = rectF.bottom;
        float f2 = rectF.right;
        this.f0.cubicTo((rectF.left + centerX) / 2.0f, f, (centerX + f2) / 2.0f, f, f2, rectF.top);
        this.f0.lineTo(rectF.right, rectF.top);
        this.f0.lineTo(rectF.right, rectF.bottom);
        this.f0.close();
    }

    public ClipRelativeLayout(Context context, AttributeSet attributeSet) {
        this(context, attributeSet, 0);
    }

    public ClipRelativeLayout(Context context, AttributeSet attributeSet, int i) {
        super(context, attributeSet, i, 0);
        this.a = zy1.a(new ClipRelativeLayout$paint$2(this));
        this.f0 = new Path();
    }
}
