package net.safemoon.androidwallet.views;

import android.content.Context;
import android.util.AttributeSet;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.ImageView;
import androidx.appcompat.widget.AppCompatImageView;
import androidx.lifecycle.Lifecycle;
import androidx.lifecycle.f;
import java.util.Comparator;
import java.util.Iterator;
import java.util.List;
import net.safemoon.androidwallet.R;
import net.safemoon.androidwallet.model.fiat.gson.Fiat;
import net.safemoon.androidwallet.model.fiat.room.RoomFiat;
import net.safemoon.androidwallet.views.CurrencyConverterLayout;
import net.safemoon.androidwallet.views.editText.autoSize.AutofitEdittext;

/* compiled from: CurrencyConverterLayout.kt */
/* loaded from: classes2.dex */
public final class CurrencyConverterLayout extends FrameLayout implements rz1 {
    public final f a;
    public final sy1 f0;
    public final yl1 g0;
    public RoomFiat h0;
    public List<RoomFiat> i0;
    public RoomFiat j0;
    public tc1<? super RoomFiat, Boolean> k0;
    public rc1<te4> l0;

    /* compiled from: Comparisons.kt */
    /* loaded from: classes2.dex */
    public static final class a<T> implements Comparator {
        @Override // java.util.Comparator
        public final int compare(T t, T t2) {
            return l30.a(((RoomFiat) t).getSymbol(), ((RoomFiat) t2).getSymbol());
        }
    }

    /* compiled from: Comparisons.kt */
    /* loaded from: classes2.dex */
    public static final class b<T> implements Comparator {
        @Override // java.util.Comparator
        public final int compare(T t, T t2) {
            return l30.a(((RoomFiat) t).getSymbol(), ((RoomFiat) t2).getSymbol());
        }
    }

    /* JADX WARN: 'this' call moved to the top of the method (can break code semantics) */
    public CurrencyConverterLayout(Context context) {
        this(context, null);
        fs1.f(context, "context");
    }

    public static final void g(CurrencyConverterLayout currencyConverterLayout, View view) {
        fs1.f(currencyConverterLayout, "this$0");
        rc1<te4> rc1Var = currencyConverterLayout.l0;
        if (rc1Var == null) {
            return;
        }
        rc1Var.invoke();
    }

    private final yp1 getBinding() {
        return (yp1) this.f0.getValue();
    }

    /* JADX WARN: Code restructure failed: missing block: B:20:0x0065, code lost:
        if (defpackage.dv3.t(r5 != null ? r5.getSymbol() : null, r4.getSymbol(), true) == false) goto L18;
     */
    /*
        Code decompiled incorrectly, please refer to instructions dump.
        To view partially-correct code enable 'Show inconsistent code' option in preferences
    */
    public static final void h(final net.safemoon.androidwallet.views.CurrencyConverterLayout r10, defpackage.yp1 r11, final java.util.List r12) {
        /*
            java.lang.String r0 = "this$0"
            defpackage.fs1.f(r10, r0)
            java.lang.String r0 = "$this_with"
            defpackage.fs1.f(r11, r0)
            if (r12 == 0) goto Lb0
            r10.i0 = r12
            net.safemoon.androidwallet.views.CurrencyConverterLayout$a r0 = new net.safemoon.androidwallet.views.CurrencyConverterLayout$a
            r0.<init>()
            java.util.List r0 = defpackage.j20.e0(r12, r0)
            java.util.ArrayList r1 = new java.util.ArrayList
            r1.<init>()
            java.util.Iterator r0 = r0.iterator()
        L20:
            boolean r2 = r0.hasNext()
            r3 = 0
            if (r2 == 0) goto L6f
            java.lang.Object r2 = r0.next()
            r4 = r2
            net.safemoon.androidwallet.model.fiat.room.RoomFiat r4 = (net.safemoon.androidwallet.model.fiat.room.RoomFiat) r4
            net.safemoon.androidwallet.viewmodels.SelectFiatViewModel$a r5 = net.safemoon.androidwallet.viewmodels.SelectFiatViewModel.e
            java.util.List r5 = r5.a()
            java.util.Iterator r5 = r5.iterator()
        L38:
            boolean r6 = r5.hasNext()
            r7 = 1
            if (r6 == 0) goto L51
            java.lang.Object r6 = r5.next()
            r8 = r6
            java.lang.String r8 = (java.lang.String) r8
            java.lang.String r9 = r4.getSymbol()
            boolean r8 = defpackage.dv3.t(r8, r9, r7)
            if (r8 == 0) goto L38
            goto L52
        L51:
            r6 = r3
        L52:
            if (r6 == 0) goto L68
            net.safemoon.androidwallet.model.fiat.room.RoomFiat r5 = r10.j0
            if (r5 != 0) goto L59
            goto L5d
        L59:
            java.lang.String r3 = r5.getSymbol()
        L5d:
            java.lang.String r4 = r4.getSymbol()
            boolean r3 = defpackage.dv3.t(r3, r4, r7)
            if (r3 != 0) goto L68
            goto L69
        L68:
            r7 = 0
        L69:
            if (r7 == 0) goto L20
            r1.add(r2)
            goto L20
        L6f:
            android.widget.LinearLayout r0 = r11.a
            r0.removeAllViews()
            java.util.Iterator r0 = r1.iterator()
        L78:
            boolean r1 = r0.hasNext()
            if (r1 == 0) goto Lb0
            java.lang.Object r1 = r0.next()
            net.safemoon.androidwallet.model.fiat.room.RoomFiat r1 = (net.safemoon.androidwallet.model.fiat.room.RoomFiat) r1
            android.content.Context r2 = r10.getContext()
            r4 = 2131558723(0x7f0d0143, float:1.874277E38)
            android.view.View r2 = android.widget.FrameLayout.inflate(r2, r4, r3)
            qp3 r2 = defpackage.qp3.a(r2)
            com.google.android.material.button.MaterialButton r4 = r2.b
            qb0 r5 = new qb0
            r5.<init>()
            r4.setOnClickListener(r5)
            com.google.android.material.button.MaterialButton r4 = r2.b
            java.lang.String r1 = r1.getSymbol()
            r4.setText(r1)
            android.widget.LinearLayout r1 = r11.a
            com.google.android.material.button.MaterialButton r2 = r2.b()
            r1.addView(r2)
            goto L78
        Lb0:
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: net.safemoon.androidwallet.views.CurrencyConverterLayout.h(net.safemoon.androidwallet.views.CurrencyConverterLayout, yp1, java.util.List):void");
    }

    public static final void i(List list, RoomFiat roomFiat, CurrencyConverterLayout currencyConverterLayout, View view) {
        fs1.f(roomFiat, "$fiat");
        fs1.f(currencyConverterLayout, "this$0");
        if (list.indexOf(roomFiat) >= 0) {
            currencyConverterLayout.h0 = roomFiat;
            tc1<? super RoomFiat, Boolean> tc1Var = currencyConverterLayout.k0;
            boolean z = false;
            if (tc1Var != null && tc1Var.invoke(roomFiat).booleanValue()) {
                z = true;
            }
            if (z) {
                currencyConverterLayout.setItem(roomFiat);
            }
        }
    }

    public static final void j(CurrencyConverterLayout currencyConverterLayout, List list) {
        Object obj;
        fs1.f(currencyConverterLayout, "this$0");
        if (list == null || currencyConverterLayout.h0 == null) {
            return;
        }
        Iterator it = list.iterator();
        while (true) {
            if (!it.hasNext()) {
                obj = null;
                break;
            }
            obj = it.next();
            RoomFiat roomFiat = currencyConverterLayout.h0;
            fs1.d(roomFiat);
            if (fs1.b(roomFiat.getSymbol(), ((RoomFiat) obj).getSymbol())) {
                break;
            }
        }
        RoomFiat roomFiat2 = (RoomFiat) obj;
        currencyConverterLayout.h0 = roomFiat2;
        if (roomFiat2 == null) {
            return;
        }
        currencyConverterLayout.setItem(roomFiat2);
    }

    public static final void n(CurrencyConverterLayout currencyConverterLayout, RoomFiat roomFiat, View view) {
        fs1.f(currencyConverterLayout, "this$0");
        fs1.f(roomFiat, "$fiat");
        if (currencyConverterLayout.i0.indexOf(roomFiat) >= 0) {
            currencyConverterLayout.h0 = roomFiat;
            tc1<? super RoomFiat, Boolean> tc1Var = currencyConverterLayout.k0;
            boolean z = false;
            if (tc1Var != null && tc1Var.invoke(roomFiat).booleanValue()) {
                z = true;
            }
            if (z) {
                currencyConverterLayout.setItem(roomFiat);
            }
        }
    }

    public final void f() {
        final yp1 binding = getBinding();
        binding.d.setOnClickListener(new View.OnClickListener() { // from class: rb0
            @Override // android.view.View.OnClickListener
            public final void onClick(View view) {
                CurrencyConverterLayout.g(CurrencyConverterLayout.this, view);
            }
        });
        AutofitEdittext editText = getEditText();
        AppCompatImageView appCompatImageView = binding.c;
        fs1.e(appCompatImageView, "ivCopy");
        cj4.m(editText, appCompatImageView);
        AutofitEdittext editText2 = getEditText();
        ImageView imageView = binding.b;
        fs1.e(imageView, "ivClear");
        cj4.l(editText2, imageView);
        q02.a(this.g0.a(), this, new tl2() { // from class: ob0
            @Override // defpackage.tl2
            public final void onChanged(Object obj) {
                CurrencyConverterLayout.j(CurrencyConverterLayout.this, (List) obj);
            }
        });
        this.g0.a().observe(this, new tl2() { // from class: pb0
            @Override // defpackage.tl2
            public final void onChanged(Object obj) {
                CurrencyConverterLayout.h(CurrencyConverterLayout.this, binding, (List) obj);
            }
        });
    }

    public final AutofitEdittext getEditText() {
        AutofitEdittext autofitEdittext = getBinding().f;
        fs1.e(autofitEdittext, "binding.txtValue");
        return autofitEdittext;
    }

    public final RoomFiat getSelectedItem() {
        return this.h0;
    }

    public final void k(tc1<? super RoomFiat, Boolean> tc1Var) {
        this.k0 = tc1Var;
    }

    public final void l(rc1<te4> rc1Var) {
        fs1.f(rc1Var, "selectFiatCallBack");
        this.l0 = rc1Var;
    }

    /* JADX WARN: Code restructure failed: missing block: B:18:0x0064, code lost:
        if (defpackage.dv3.t(r5 != null ? r5.getSymbol() : null, r4.getSymbol(), true) == false) goto L16;
     */
    /*
        Code decompiled incorrectly, please refer to instructions dump.
        To view partially-correct code enable 'Show inconsistent code' option in preferences
    */
    public final void m(net.safemoon.androidwallet.model.fiat.room.RoomFiat r11) {
        /*
            r10 = this;
            java.lang.String r0 = "item"
            defpackage.fs1.f(r11, r0)
            r10.j0 = r11
            yp1 r11 = r10.getBinding()
            java.util.List<net.safemoon.androidwallet.model.fiat.room.RoomFiat> r0 = r10.i0
            net.safemoon.androidwallet.views.CurrencyConverterLayout$b r1 = new net.safemoon.androidwallet.views.CurrencyConverterLayout$b
            r1.<init>()
            java.util.List r0 = defpackage.j20.e0(r0, r1)
            java.util.ArrayList r1 = new java.util.ArrayList
            r1.<init>()
            java.util.Iterator r0 = r0.iterator()
        L1f:
            boolean r2 = r0.hasNext()
            r3 = 0
            if (r2 == 0) goto L6e
            java.lang.Object r2 = r0.next()
            r4 = r2
            net.safemoon.androidwallet.model.fiat.room.RoomFiat r4 = (net.safemoon.androidwallet.model.fiat.room.RoomFiat) r4
            net.safemoon.androidwallet.viewmodels.SelectFiatViewModel$a r5 = net.safemoon.androidwallet.viewmodels.SelectFiatViewModel.e
            java.util.List r5 = r5.a()
            java.util.Iterator r5 = r5.iterator()
        L37:
            boolean r6 = r5.hasNext()
            r7 = 1
            if (r6 == 0) goto L50
            java.lang.Object r6 = r5.next()
            r8 = r6
            java.lang.String r8 = (java.lang.String) r8
            java.lang.String r9 = r4.getSymbol()
            boolean r8 = defpackage.dv3.t(r8, r9, r7)
            if (r8 == 0) goto L37
            goto L51
        L50:
            r6 = r3
        L51:
            if (r6 == 0) goto L67
            net.safemoon.androidwallet.model.fiat.room.RoomFiat r5 = r10.j0
            if (r5 != 0) goto L58
            goto L5c
        L58:
            java.lang.String r3 = r5.getSymbol()
        L5c:
            java.lang.String r4 = r4.getSymbol()
            boolean r3 = defpackage.dv3.t(r3, r4, r7)
            if (r3 != 0) goto L67
            goto L68
        L67:
            r7 = 0
        L68:
            if (r7 == 0) goto L1f
            r1.add(r2)
            goto L1f
        L6e:
            android.widget.LinearLayout r0 = r11.a
            r0.removeAllViews()
            java.util.Iterator r0 = r1.iterator()
        L77:
            boolean r1 = r0.hasNext()
            if (r1 == 0) goto Laf
            java.lang.Object r1 = r0.next()
            net.safemoon.androidwallet.model.fiat.room.RoomFiat r1 = (net.safemoon.androidwallet.model.fiat.room.RoomFiat) r1
            android.content.Context r2 = r10.getContext()
            r4 = 2131558723(0x7f0d0143, float:1.874277E38)
            android.view.View r2 = android.widget.FrameLayout.inflate(r2, r4, r3)
            qp3 r2 = defpackage.qp3.a(r2)
            com.google.android.material.button.MaterialButton r4 = r2.b
            sb0 r5 = new sb0
            r5.<init>()
            r4.setOnClickListener(r5)
            com.google.android.material.button.MaterialButton r4 = r2.b
            java.lang.String r1 = r1.getSymbol()
            r4.setText(r1)
            android.widget.LinearLayout r1 = r11.a
            com.google.android.material.button.MaterialButton r2 = r2.b()
            r1.addView(r2)
            goto L77
        Laf:
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: net.safemoon.androidwallet.views.CurrencyConverterLayout.m(net.safemoon.androidwallet.model.fiat.room.RoomFiat):void");
    }

    @Override // android.view.ViewGroup, android.view.View
    public void onAttachedToWindow() {
        super.onAttachedToWindow();
        this.a.o(Lifecycle.State.RESUMED);
    }

    @Override // android.view.ViewGroup, android.view.View
    public void onDetachedFromWindow() {
        super.onDetachedFromWindow();
        this.a.o(Lifecycle.State.DESTROYED);
    }

    public final void setItem(RoomFiat roomFiat) {
        fs1.f(roomFiat, "roomFiat");
        this.h0 = roomFiat;
        yp1 binding = getBinding();
        binding.e.setText(roomFiat.getSymbol());
        binding.d.setText(roomFiat.getName());
    }

    public final void setOtherSideItem(Fiat fiat) {
        fs1.f(fiat, "item");
        this.j0 = new RoomFiat(fiat);
    }

    /* JADX WARN: 'this' call moved to the top of the method (can break code semantics) */
    public CurrencyConverterLayout(Context context, AttributeSet attributeSet) {
        this(context, attributeSet, 0);
        fs1.f(context, "context");
    }

    @Override // defpackage.rz1
    public f getLifecycle() {
        return this.a;
    }

    public final void setOtherSideItem(RoomFiat roomFiat) {
        fs1.f(roomFiat, "item");
        this.j0 = roomFiat;
    }

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public CurrencyConverterLayout(Context context, AttributeSet attributeSet, int i) {
        super(context, attributeSet, i);
        fs1.f(context, "context");
        this.a = new f(this);
        this.f0 = zy1.a(new CurrencyConverterLayout$binding$2(this));
        e31 e31Var = e31.a;
        Context context2 = getContext();
        fs1.e(context2, "context");
        this.g0 = e31Var.b(context2);
        this.i0 = b20.g();
        context.obtainStyledAttributes(attributeSet, s23.CurrencyConverterLayout, i, 0).recycle();
        FrameLayout.inflate(context, R.layout.include_currency_converter_layout, this);
        f();
    }

    public final void setItem(Fiat fiat) {
        fs1.f(fiat, "fiat");
        this.h0 = new RoomFiat(fiat);
        yp1 binding = getBinding();
        binding.e.setText(fiat.getSymbol());
        binding.d.setText(fiat.getName());
    }
}
