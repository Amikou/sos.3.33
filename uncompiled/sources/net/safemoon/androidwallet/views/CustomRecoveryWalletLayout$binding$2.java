package net.safemoon.androidwallet.views;

import kotlin.jvm.internal.Lambda;

/* compiled from: CustomRecoveryWalletLayout.kt */
/* loaded from: classes2.dex */
public final class CustomRecoveryWalletLayout$binding$2 extends Lambda implements rc1<wk1> {
    public final /* synthetic */ CustomRecoveryWalletLayout this$0;

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public CustomRecoveryWalletLayout$binding$2(CustomRecoveryWalletLayout customRecoveryWalletLayout) {
        super(0);
        this.this$0 = customRecoveryWalletLayout;
    }

    @Override // defpackage.rc1
    public final wk1 invoke() {
        return wk1.a(this.this$0);
    }
}
