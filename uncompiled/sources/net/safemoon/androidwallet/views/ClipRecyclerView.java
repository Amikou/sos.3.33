package net.safemoon.androidwallet.views;

import android.content.Context;
import android.util.AttributeSet;
import androidx.recyclerview.widget.RecyclerView;

/* compiled from: ClipRecyclerView.kt */
/* loaded from: classes2.dex */
public final class ClipRecyclerView extends RecyclerView {
    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public ClipRecyclerView(Context context) {
        super(context);
        fs1.f(context, "context");
    }

    @Override // androidx.recyclerview.widget.RecyclerView, android.view.ViewGroup, android.view.View
    public void onLayout(boolean z, int i, int i2, int i3, int i4) {
        super.onLayout(z, i, i2, i3, i4);
        setPadding(0, 0, 0, t42.b(e30.u(this)));
    }

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public ClipRecyclerView(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        fs1.f(context, "context");
    }

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public ClipRecyclerView(Context context, AttributeSet attributeSet, int i) {
        super(context, attributeSet, i);
        fs1.f(context, "context");
    }
}
