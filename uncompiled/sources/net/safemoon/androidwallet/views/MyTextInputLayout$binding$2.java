package net.safemoon.androidwallet.views;

import android.view.View;
import kotlin.jvm.internal.Lambda;

/* compiled from: MyTextInputLayout.kt */
/* loaded from: classes2.dex */
public final class MyTextInputLayout$binding$2 extends Lambda implements rc1<fk4> {
    public final /* synthetic */ MyTextInputLayout this$0;

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public MyTextInputLayout$binding$2(MyTextInputLayout myTextInputLayout) {
        super(0);
        this.this$0 = myTextInputLayout;
    }

    /* JADX WARN: Can't rename method to resolve collision */
    @Override // defpackage.rc1
    public final fk4 invoke() {
        View customEnd;
        customEnd = this.this$0.getCustomEnd();
        return fk4.a(customEnd);
    }
}
