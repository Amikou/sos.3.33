package net.safemoon.androidwallet.views;

import android.content.Context;
import android.util.AttributeSet;
import android.view.View;
import androidx.cardview.widget.CardView;

/* compiled from: ShadowButton.kt */
/* loaded from: classes2.dex */
public final class ShadowButton extends CardView {
    public final sy1 n0;
    public final sy1 o0;

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public ShadowButton(Context context) {
        super(context);
        fs1.f(context, "context");
        this.n0 = zy1.a(new ShadowButton$customEnd$2(this));
        this.o0 = zy1.a(new ShadowButton$binding$2(this));
        getBinding();
    }

    private final bi4 getBinding() {
        return (bi4) this.o0.getValue();
    }

    /* JADX INFO: Access modifiers changed from: private */
    public final View getCustomEnd() {
        Object value = this.n0.getValue();
        fs1.e(value, "<get-customEnd>(...)");
        return (View) value;
    }

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public ShadowButton(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        fs1.f(context, "context");
        this.n0 = zy1.a(new ShadowButton$customEnd$2(this));
        this.o0 = zy1.a(new ShadowButton$binding$2(this));
        getBinding();
    }

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public ShadowButton(Context context, AttributeSet attributeSet, int i) {
        super(context, attributeSet, i);
        fs1.f(context, "context");
        this.n0 = zy1.a(new ShadowButton$customEnd$2(this));
        this.o0 = zy1.a(new ShadowButton$binding$2(this));
        getBinding();
    }
}
