package net.safemoon.androidwallet.views;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import kotlin.jvm.internal.Lambda;
import net.safemoon.androidwallet.R;

/* compiled from: ShadowButton.kt */
/* loaded from: classes2.dex */
public final class ShadowButton$customEnd$2 extends Lambda implements rc1<View> {
    public final /* synthetic */ ShadowButton this$0;

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public ShadowButton$customEnd$2(ShadowButton shadowButton) {
        super(0);
        this.this$0 = shadowButton;
    }

    /* JADX WARN: Can't rename method to resolve collision */
    @Override // defpackage.rc1
    public final View invoke() {
        return LayoutInflater.from(this.this$0.getContext()).inflate(R.layout.view_button_shadow, (ViewGroup) this.this$0, false);
    }
}
