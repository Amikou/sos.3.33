package net.safemoon.androidwallet.views.slidetoact;

import android.animation.Animator;
import android.animation.AnimatorSet;
import android.animation.ValueAnimator;
import android.annotation.SuppressLint;
import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Canvas;
import android.graphics.Outline;
import android.graphics.Paint;
import android.graphics.RectF;
import android.graphics.Typeface;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.os.VibrationEffect;
import android.os.Vibrator;
import android.text.TextPaint;
import android.text.method.TransformationMethod;
import android.util.AttributeSet;
import android.util.TypedValue;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewOutlineProvider;
import android.view.animation.AnticipateOvershootInterpolator;
import android.widget.TextView;
import com.github.mikephil.charting.utils.Utils;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Objects;
import net.safemoon.androidwallet.R;
import net.safemoon.androidwallet.views.slidetoact.SlideToActView;

/* compiled from: SlideToActView.kt */
/* loaded from: classes2.dex */
public final class SlideToActView extends View {
    public float A0;
    public float B0;
    public int C0;
    public float D0;
    public float E0;
    public final int F0;
    public int G0;
    public float H0;
    public int I0;
    public Drawable J0;
    public Drawable K0;
    public boolean L0;
    public int M0;
    public final Paint N0;
    public final Paint O0;
    public Paint P0;
    public TextView Q0;
    public RectF R0;
    public RectF S0;
    public final float T0;
    public float U0;
    public boolean V0;
    public boolean W0;
    public boolean X0;
    public boolean Y0;
    public boolean Z0;
    public float a;
    public boolean a1;
    public d b1;
    public b c1;
    public c d1;
    public e e1;
    public float f0;
    public int g0;
    public int h0;
    public int i0;
    public int j0;
    public int k0;
    public int l0;
    public int m0;
    public final int n0;
    public CharSequence o0;
    public int p0;
    public int q0;
    public int r0;
    public int s0;
    public long t0;
    public long u0;
    public int v0;
    public int w0;
    public int x0;
    public int y0;
    public int z0;

    /* compiled from: SlideToActView.kt */
    /* loaded from: classes2.dex */
    public static final class a {
        public a() {
        }

        public /* synthetic */ a(qi0 qi0Var) {
            this();
        }
    }

    /* compiled from: SlideToActView.kt */
    /* loaded from: classes2.dex */
    public interface b {
        void a(SlideToActView slideToActView);
    }

    /* compiled from: SlideToActView.kt */
    /* loaded from: classes2.dex */
    public interface c {
    }

    /* compiled from: SlideToActView.kt */
    /* loaded from: classes2.dex */
    public interface d {
        void a(SlideToActView slideToActView);

        void b(SlideToActView slideToActView, float f);
    }

    /* compiled from: SlideToActView.kt */
    /* loaded from: classes2.dex */
    public interface e {
        void a(SlideToActView slideToActView, boolean z);
    }

    /* compiled from: SlideToActView.kt */
    /* loaded from: classes2.dex */
    public final class f extends ViewOutlineProvider {
        public final /* synthetic */ SlideToActView a;

        public f(SlideToActView slideToActView) {
            fs1.f(slideToActView, "this$0");
            this.a = slideToActView;
        }

        @Override // android.view.ViewOutlineProvider
        public void getOutline(View view, Outline outline) {
            if (view == null || outline == null) {
                return;
            }
            outline.setRoundRect(this.a.k0, 0, this.a.j0 - this.a.k0, this.a.i0, this.a.l0);
        }
    }

    /* compiled from: SlideToActView.kt */
    /* loaded from: classes2.dex */
    public static final class g implements Animator.AnimatorListener {
        public g() {
        }

        @Override // android.animation.Animator.AnimatorListener
        public void onAnimationCancel(Animator animator) {
        }

        @Override // android.animation.Animator.AnimatorListener
        public void onAnimationEnd(Animator animator) {
            SlideToActView.this.W0 = true;
            d onSlideToActAnimationEventListener = SlideToActView.this.getOnSlideToActAnimationEventListener();
            if (onSlideToActAnimationEventListener != null) {
                onSlideToActAnimationEventListener.a(SlideToActView.this);
            }
            b onSlideCompleteListener = SlideToActView.this.getOnSlideCompleteListener();
            if (onSlideCompleteListener == null) {
                return;
            }
            onSlideCompleteListener.a(SlideToActView.this);
        }

        @Override // android.animation.Animator.AnimatorListener
        public void onAnimationRepeat(Animator animator) {
        }

        @Override // android.animation.Animator.AnimatorListener
        public void onAnimationStart(Animator animator) {
            d onSlideToActAnimationEventListener = SlideToActView.this.getOnSlideToActAnimationEventListener();
            if (onSlideToActAnimationEventListener == null) {
                return;
            }
            SlideToActView slideToActView = SlideToActView.this;
            onSlideToActAnimationEventListener.b(slideToActView, slideToActView.D0);
        }
    }

    static {
        new a(null);
    }

    /* JADX WARN: 'this' call moved to the top of the method (can break code semantics) */
    public SlideToActView(Context context) {
        this(context, null, 0, 6, null);
        fs1.f(context, "context");
    }

    /* JADX WARN: 'this' call moved to the top of the method (can break code semantics) */
    public SlideToActView(Context context, AttributeSet attributeSet) {
        this(context, attributeSet, 0, 4, null);
        fs1.f(context, "context");
    }

    public /* synthetic */ SlideToActView(Context context, AttributeSet attributeSet, int i, int i2, qi0 qi0Var) {
        this(context, (i2 & 2) != 0 ? null : attributeSet, (i2 & 4) != 0 ? R.attr.slideToActViewStyle : i);
    }

    public static final void o(SlideToActView slideToActView, ValueAnimator valueAnimator) {
        fs1.f(slideToActView, "this$0");
        Object animatedValue = valueAnimator.getAnimatedValue();
        Objects.requireNonNull(animatedValue, "null cannot be cast to non-null type kotlin.Int");
        slideToActView.setMPosition(((Integer) animatedValue).intValue());
        slideToActView.invalidate();
    }

    public static final void q(SlideToActView slideToActView, ValueAnimator valueAnimator) {
        fs1.f(slideToActView, "this$0");
        Object animatedValue = valueAnimator.getAnimatedValue();
        Objects.requireNonNull(animatedValue, "null cannot be cast to non-null type kotlin.Int");
        slideToActView.setMPosition(((Integer) animatedValue).intValue());
        slideToActView.invalidate();
    }

    public static final void r(SlideToActView slideToActView, ValueAnimator valueAnimator) {
        fs1.f(slideToActView, "this$0");
        Object animatedValue = valueAnimator.getAnimatedValue();
        Objects.requireNonNull(animatedValue, "null cannot be cast to non-null type kotlin.Int");
        slideToActView.m0 = ((Integer) animatedValue).intValue();
        slideToActView.invalidate();
    }

    public static final void s(SlideToActView slideToActView, ValueAnimator valueAnimator) {
        fs1.f(slideToActView, "this$0");
        Object animatedValue = valueAnimator.getAnimatedValue();
        Objects.requireNonNull(animatedValue, "null cannot be cast to non-null type kotlin.Int");
        slideToActView.k0 = ((Integer) animatedValue).intValue();
        if (Build.VERSION.SDK_INT >= 21) {
            slideToActView.invalidateOutline();
        }
        slideToActView.invalidate();
    }

    private final void setMEffectivePosition(int i) {
        if (this.Y0) {
            i = (this.j0 - this.i0) - i;
        }
        this.z0 = i;
    }

    private final void setMPosition(int i) {
        int i2;
        int i3;
        this.y0 = i;
        if (this.j0 - this.i0 == 0) {
            this.D0 = Utils.FLOAT_EPSILON;
            this.E0 = 1.0f;
            return;
        }
        float f2 = i;
        this.D0 = f2 / (i2 - i3);
        this.E0 = 1 - (f2 / (i2 - i3));
        setMEffectivePosition(i);
    }

    private final void setMTextSize(int i) {
        this.C0 = i;
        this.Q0.setTextSize(0, i);
        this.P0.set(this.Q0.getPaint());
    }

    public static final void t(SlideToActView slideToActView, ValueAnimator valueAnimator) {
        fs1.f(slideToActView, "this$0");
        if (slideToActView.L0) {
            return;
        }
        slideToActView.L0 = true;
        slideToActView.I0 = slideToActView.F0;
    }

    public final long getAnimDuration() {
        return this.t0;
    }

    public final long getBumpVibration() {
        return this.u0;
    }

    public final int getCompleteIcon() {
        return this.M0;
    }

    public final int getIconColor() {
        return this.w0;
    }

    public final int getInnerColor() {
        return this.s0;
    }

    public final b getOnSlideCompleteListener() {
        return this.c1;
    }

    public final c getOnSlideResetListener() {
        return this.d1;
    }

    public final d getOnSlideToActAnimationEventListener() {
        return this.b1;
    }

    public final e getOnSlideUserFailedListener() {
        return this.e1;
    }

    public final int getOuterColor() {
        return this.r0;
    }

    public final int getSliderIcon() {
        return this.x0;
    }

    public final CharSequence getText() {
        return this.o0;
    }

    public final int getTextAppearance() {
        return this.q0;
    }

    public final int getTextColor() {
        return this.v0;
    }

    public final int getTypeFace() {
        return this.p0;
    }

    public final boolean l(float f2, float f3) {
        if (Utils.FLOAT_EPSILON < f3) {
            int i = this.i0;
            if (f3 < i) {
                int i2 = this.z0;
                if (i2 < f2 && f2 < i + i2) {
                    return true;
                }
            }
        }
        return false;
    }

    @SuppressLint({"MissingPermission"})
    public final void m() {
        if (this.u0 > 0 && m70.a(getContext(), "android.permission.VIBRATE") == 0) {
            Object systemService = getContext().getSystemService("vibrator");
            Objects.requireNonNull(systemService, "null cannot be cast to non-null type android.os.Vibrator");
            Vibrator vibrator = (Vibrator) systemService;
            if (Build.VERSION.SDK_INT >= 26) {
                vibrator.vibrate(VibrationEffect.createOneShot(this.u0, -1));
            } else {
                vibrator.vibrate(this.u0);
            }
        }
    }

    public final void n(int i) {
        int i2;
        if (this.Y0) {
            i2 = this.y0 - i;
        } else {
            i2 = this.y0 + i;
        }
        setMPosition(i2);
        if (this.y0 < 0) {
            setMPosition(0);
        }
        int i3 = this.y0;
        int i4 = this.j0;
        int i5 = this.i0;
        if (i3 > i4 - i5) {
            setMPosition(i4 - i5);
        }
    }

    @Override // android.view.View
    public void onDraw(Canvas canvas) {
        super.onDraw(canvas);
        if (canvas == null) {
            return;
        }
        RectF rectF = this.S0;
        int i = this.k0;
        rectF.set(i, Utils.FLOAT_EPSILON, this.j0 - i, this.i0);
        RectF rectF2 = this.S0;
        int i2 = this.l0;
        canvas.drawRoundRect(rectF2, i2, i2, this.N0);
        this.P0.setAlpha((int) (255 * this.E0));
        TransformationMethod transformationMethod = this.Q0.getTransformationMethod();
        Drawable drawable = null;
        CharSequence transformation = transformationMethod == null ? null : transformationMethod.getTransformation(this.o0, this.Q0);
        if (transformation == null) {
            transformation = this.o0;
        }
        CharSequence charSequence = transformation;
        canvas.drawText(charSequence, 0, charSequence.length(), this.B0, this.A0, this.P0);
        int i3 = this.i0;
        int i4 = this.m0;
        float f2 = (i3 - (i4 * 2)) / i3;
        RectF rectF3 = this.R0;
        int i5 = this.z0;
        rectF3.set(i4 + i5, i4, (i5 + i3) - i4, i3 - i4);
        RectF rectF4 = this.R0;
        int i6 = this.l0;
        canvas.drawRoundRect(rectF4, i6 * f2, i6 * f2, this.O0);
        canvas.save();
        if (this.Y0) {
            canvas.scale(-1.0f, 1.0f, this.R0.centerX(), this.R0.centerY());
        }
        if (this.Z0) {
            float f3 = (-180) * this.D0;
            this.H0 = f3;
            canvas.rotate(f3, this.R0.centerX(), this.R0.centerY());
        }
        Drawable drawable2 = this.J0;
        if (drawable2 == null) {
            fs1.r("mDrawableArrow");
            drawable2 = null;
        }
        RectF rectF5 = this.R0;
        int i7 = this.G0;
        drawable2.setBounds(((int) rectF5.left) + i7, ((int) rectF5.top) + i7, ((int) rectF5.right) - i7, ((int) rectF5.bottom) - i7);
        Drawable drawable3 = this.J0;
        if (drawable3 == null) {
            fs1.r("mDrawableArrow");
            drawable3 = null;
        }
        int i8 = drawable3.getBounds().left;
        Drawable drawable4 = this.J0;
        if (drawable4 == null) {
            fs1.r("mDrawableArrow");
            drawable4 = null;
        }
        if (i8 <= drawable4.getBounds().right) {
            Drawable drawable5 = this.J0;
            if (drawable5 == null) {
                fs1.r("mDrawableArrow");
                drawable5 = null;
            }
            int i9 = drawable5.getBounds().top;
            Drawable drawable6 = this.J0;
            if (drawable6 == null) {
                fs1.r("mDrawableArrow");
                drawable6 = null;
            }
            if (i9 <= drawable6.getBounds().bottom) {
                Drawable drawable7 = this.J0;
                if (drawable7 == null) {
                    fs1.r("mDrawableArrow");
                } else {
                    drawable = drawable7;
                }
                drawable.draw(canvas);
            }
        }
        canvas.restore();
        Drawable drawable8 = this.K0;
        int i10 = this.k0;
        int i11 = this.I0;
        drawable8.setBounds(i10 + i11, i11, (this.j0 - i11) - i10, this.i0 - i11);
        lq3.a.i(this.K0, this.s0);
        if (this.L0) {
            this.K0.draw(canvas);
        }
    }

    @Override // android.view.View
    public void onMeasure(int i, int i2) {
        int mode = View.MeasureSpec.getMode(i);
        int size = View.MeasureSpec.getSize(i);
        if (mode == Integer.MIN_VALUE) {
            size = Math.min(this.h0, size);
        } else if (mode == 0) {
            size = this.h0;
        } else if (mode != 1073741824) {
            size = this.h0;
        }
        setMeasuredDimension(size, this.g0);
    }

    @Override // android.view.View
    public void onSizeChanged(int i, int i2, int i3, int i4) {
        this.j0 = i;
        this.i0 = i2;
        if (this.l0 == -1) {
            this.l0 = i2 / 2;
        }
        float f2 = 2;
        this.B0 = i / f2;
        this.A0 = (i2 / f2) - ((this.P0.descent() + this.P0.ascent()) / f2);
        setMPosition(0);
    }

    @Override // android.view.View
    public boolean onTouchEvent(MotionEvent motionEvent) {
        e eVar;
        if (motionEvent != null && motionEvent.getAction() == 0) {
            performClick();
        }
        if (motionEvent != null && isEnabled()) {
            int action = motionEvent.getAction();
            if (action != 0) {
                if (action != 1) {
                    if (action == 2 && this.V0) {
                        boolean z = this.D0 < 1.0f;
                        this.U0 = motionEvent.getX();
                        n((int) (motionEvent.getX() - this.U0));
                        invalidate();
                        if (this.u0 > 0 && z) {
                            if (this.D0 == 1.0f) {
                                m();
                            }
                        }
                    }
                } else {
                    getParent().requestDisallowInterceptTouchEvent(false);
                    int i = this.y0;
                    if ((i > 0 && this.X0) || (i > 0 && this.D0 < this.T0)) {
                        ValueAnimator ofInt = ValueAnimator.ofInt(i, 0);
                        ofInt.setDuration(this.t0);
                        ofInt.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() { // from class: oq3
                            @Override // android.animation.ValueAnimator.AnimatorUpdateListener
                            public final void onAnimationUpdate(ValueAnimator valueAnimator) {
                                SlideToActView.o(SlideToActView.this, valueAnimator);
                            }
                        });
                        ofInt.start();
                    } else if (i > 0 && this.D0 >= this.T0) {
                        setEnabled(false);
                        p();
                    } else if (this.V0 && i == 0 && (eVar = this.e1) != null) {
                        eVar.a(this, false);
                    }
                    this.V0 = false;
                }
            } else if (l(motionEvent.getX(), motionEvent.getY())) {
                this.V0 = true;
                this.U0 = motionEvent.getX();
                getParent().requestDisallowInterceptTouchEvent(true);
            } else {
                e eVar2 = this.e1;
                if (eVar2 != null) {
                    eVar2.a(this, true);
                }
            }
            return true;
        }
        return super.onTouchEvent(motionEvent);
    }

    public final void p() {
        AnimatorSet animatorSet = new AnimatorSet();
        ValueAnimator ofInt = ValueAnimator.ofInt(this.y0, this.j0 - this.i0);
        ofInt.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() { // from class: nq3
            @Override // android.animation.ValueAnimator.AnimatorUpdateListener
            public final void onAnimationUpdate(ValueAnimator valueAnimator) {
                SlideToActView.q(SlideToActView.this, valueAnimator);
            }
        });
        ValueAnimator ofInt2 = ValueAnimator.ofInt(this.m0, ((int) (this.R0.width() / 2)) + this.m0);
        ofInt2.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() { // from class: qq3
            @Override // android.animation.ValueAnimator.AnimatorUpdateListener
            public final void onAnimationUpdate(ValueAnimator valueAnimator) {
                SlideToActView.r(SlideToActView.this, valueAnimator);
            }
        });
        ofInt2.setInterpolator(new AnticipateOvershootInterpolator(2.0f));
        ValueAnimator ofInt3 = ValueAnimator.ofInt(0, (this.j0 - this.i0) / 2);
        ofInt3.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() { // from class: mq3
            @Override // android.animation.ValueAnimator.AnimatorUpdateListener
            public final void onAnimationUpdate(ValueAnimator valueAnimator) {
                SlideToActView.s(SlideToActView.this, valueAnimator);
            }
        });
        ValueAnimator c2 = lq3.a.c(this, this.K0, new ValueAnimator.AnimatorUpdateListener() { // from class: pq3
            @Override // android.animation.ValueAnimator.AnimatorUpdateListener
            public final void onAnimationUpdate(ValueAnimator valueAnimator) {
                SlideToActView.t(SlideToActView.this, valueAnimator);
            }
        });
        ArrayList arrayList = new ArrayList();
        if (this.y0 < this.j0 - this.i0) {
            fs1.e(ofInt, "finalPositionAnimator");
            arrayList.add(ofInt);
        }
        if (this.a1) {
            fs1.e(ofInt2, "marginAnimator");
            arrayList.add(ofInt2);
            fs1.e(ofInt3, "areaAnimator");
            arrayList.add(ofInt3);
            arrayList.add(c2);
        }
        Object[] array = arrayList.toArray(new Animator[0]);
        Objects.requireNonNull(array, "null cannot be cast to non-null type kotlin.Array<T>");
        Animator[] animatorArr = (Animator[]) array;
        animatorSet.playSequentially((Animator[]) Arrays.copyOf(animatorArr, animatorArr.length));
        animatorSet.setDuration(this.t0);
        animatorSet.addListener(new g());
        animatorSet.start();
    }

    @Override // android.view.View
    public boolean performClick() {
        return super.performClick();
    }

    public final void setAnimDuration(long j) {
        this.t0 = j;
    }

    public final void setAnimateCompletion(boolean z) {
        this.a1 = z;
    }

    public final void setBumpVibration(long j) {
        this.u0 = j;
    }

    public final void setCompleteIcon(int i) {
        this.M0 = i;
        if (i != 0) {
            lq3 lq3Var = lq3.a;
            Context context = getContext();
            fs1.e(context, "context");
            this.K0 = lq3Var.g(context, i);
            invalidate();
        }
    }

    public final void setIconColor(int i) {
        this.w0 = i;
        Drawable drawable = this.J0;
        if (drawable == null) {
            fs1.r("mDrawableArrow");
            drawable = null;
        }
        androidx.core.graphics.drawable.a.n(drawable, i);
        invalidate();
    }

    public final void setInnerColor(int i) {
        this.s0 = i;
        this.O0.setColor(i);
        invalidate();
    }

    public final void setLocked(boolean z) {
        this.X0 = z;
    }

    public final void setOnSlideCompleteListener(b bVar) {
        this.c1 = bVar;
    }

    public final void setOnSlideResetListener(c cVar) {
        this.d1 = cVar;
    }

    public final void setOnSlideToActAnimationEventListener(d dVar) {
        this.b1 = dVar;
    }

    public final void setOnSlideUserFailedListener(e eVar) {
        this.e1 = eVar;
    }

    public final void setOuterColor(int i) {
        this.r0 = i;
        this.N0.setColor(i);
        invalidate();
    }

    public final void setReversed(boolean z) {
        this.Y0 = z;
        setMPosition(this.y0);
        invalidate();
    }

    public final void setRotateIcon(boolean z) {
        this.Z0 = z;
    }

    public final void setSliderIcon(int i) {
        this.x0 = i;
        if (i != 0) {
            Drawable f2 = g83.f(getContext().getResources(), i, getContext().getTheme());
            if (f2 != null) {
                this.J0 = f2;
                androidx.core.graphics.drawable.a.n(f2, getIconColor());
            }
            invalidate();
        }
    }

    public final void setText(CharSequence charSequence) {
        fs1.f(charSequence, "value");
        this.o0 = charSequence;
        this.Q0.setText(charSequence);
        this.P0.set(this.Q0.getPaint());
        invalidate();
    }

    public final void setTextAppearance(int i) {
        this.q0 = i;
        if (i != 0) {
            t44.q(this.Q0, i);
            this.P0.set(this.Q0.getPaint());
            this.P0.setColor(this.Q0.getCurrentTextColor());
        }
    }

    public final void setTextColor(int i) {
        this.v0 = i;
        this.Q0.setTextColor(i);
        this.P0.setColor(this.v0);
        invalidate();
    }

    public final void setTypeFace(int i) {
        this.p0 = i;
        this.Q0.setTypeface(Typeface.create("sans-serif-light", i));
        this.P0.set(this.Q0.getPaint());
        invalidate();
    }

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public SlideToActView(Context context, AttributeSet attributeSet, int i) {
        super(context, attributeSet, i);
        fs1.f(context, "context");
        this.a = 72.0f;
        this.f0 = 280.0f;
        this.l0 = -1;
        String str = "";
        this.o0 = "";
        this.t0 = 300L;
        this.x0 = R.drawable.slidetoact_ic_arrow;
        this.A0 = -1.0f;
        this.B0 = -1.0f;
        this.E0 = 1.0f;
        this.N0 = new Paint(1);
        this.O0 = new Paint(1);
        this.P0 = new Paint(1);
        this.T0 = 0.8f;
        this.Z0 = true;
        this.a1 = true;
        TextView textView = new TextView(context);
        this.Q0 = textView;
        TextPaint paint = textView.getPaint();
        fs1.e(paint, "mTextView.paint");
        this.P0 = paint;
        TypedArray obtainStyledAttributes = context.getTheme().obtainStyledAttributes(attributeSet, s23.SlideToActView, i, R.style.SlideToActView);
        fs1.e(obtainStyledAttributes, "context.theme.obtainStyl….SlideToActView\n        )");
        try {
            this.g0 = (int) TypedValue.applyDimension(1, this.a, getResources().getDisplayMetrics());
            this.h0 = (int) TypedValue.applyDimension(1, this.f0, getResources().getDisplayMetrics());
            int d2 = m70.d(getContext(), R.color.slidetoact_defaultAccent);
            int d3 = m70.d(getContext(), R.color.slidetoact_white);
            this.g0 = obtainStyledAttributes.getDimensionPixelSize(10, this.g0);
            this.l0 = obtainStyledAttributes.getDimensionPixelSize(3, -1);
            int color = obtainStyledAttributes.getColor(8, d2);
            int color2 = obtainStyledAttributes.getColor(7, d3);
            if (obtainStyledAttributes.hasValue(17)) {
                d3 = obtainStyledAttributes.getColor(17, d3);
            } else if (obtainStyledAttributes.hasValue(7)) {
                d3 = color2;
            }
            String string = obtainStyledAttributes.getString(15);
            if (string != null) {
                str = string;
            }
            setText(str);
            setTypeFace(obtainStyledAttributes.getInt(19, 0));
            setMTextSize(obtainStyledAttributes.getDimensionPixelSize(18, obtainStyledAttributes.getResources().getDimensionPixelSize(R.dimen.slidetoact_default_text_size)));
            setTextColor(d3);
            setTextAppearance(obtainStyledAttributes.getResourceId(16, 0));
            setLocked(obtainStyledAttributes.getBoolean(13, false));
            setReversed(obtainStyledAttributes.getBoolean(14, false));
            setRotateIcon(obtainStyledAttributes.getBoolean(9, true));
            setAnimateCompletion(obtainStyledAttributes.getBoolean(0, true));
            setAnimDuration(obtainStyledAttributes.getInteger(1, 300));
            setBumpVibration(obtainStyledAttributes.getInt(4, 0));
            int dimensionPixelSize = obtainStyledAttributes.getDimensionPixelSize(2, obtainStyledAttributes.getResources().getDimensionPixelSize(R.dimen.slidetoact_default_area_margin));
            this.n0 = dimensionPixelSize;
            this.m0 = dimensionPixelSize;
            setSliderIcon(obtainStyledAttributes.getResourceId(11, R.drawable.slidetoact_ic_arrow));
            if (obtainStyledAttributes.hasValue(12)) {
                d2 = obtainStyledAttributes.getColor(12, d2);
            } else if (obtainStyledAttributes.hasValue(8)) {
                d2 = color;
            }
            int resourceId = obtainStyledAttributes.getResourceId(5, R.drawable.slidetoact_animated_ic_check);
            int dimensionPixelSize2 = obtainStyledAttributes.getDimensionPixelSize(6, obtainStyledAttributes.getResources().getDimensionPixelSize(R.dimen.slidetoact_default_icon_margin));
            this.F0 = dimensionPixelSize2;
            this.G0 = dimensionPixelSize2;
            this.I0 = dimensionPixelSize2;
            obtainStyledAttributes.recycle();
            int i2 = this.m0;
            int i3 = this.z0;
            int i4 = this.i0;
            this.R0 = new RectF(i2 + i3, i2, (i3 + i4) - i2, i4 - i2);
            int i5 = this.k0;
            this.S0 = new RectF(i5, Utils.FLOAT_EPSILON, this.j0 - i5, this.i0);
            this.K0 = lq3.a.g(context, resourceId);
            this.P0.setTextAlign(Paint.Align.CENTER);
            setOuterColor(color);
            setInnerColor(color2);
            setIconColor(d2);
            if (Build.VERSION.SDK_INT >= 21) {
                setOutlineProvider(new f(this));
            }
        } catch (Throwable th) {
            obtainStyledAttributes.recycle();
            throw th;
        }
    }
}
