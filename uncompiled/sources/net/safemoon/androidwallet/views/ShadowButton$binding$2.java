package net.safemoon.androidwallet.views;

import android.view.View;
import kotlin.jvm.internal.Lambda;

/* compiled from: ShadowButton.kt */
/* loaded from: classes2.dex */
public final class ShadowButton$binding$2 extends Lambda implements rc1<bi4> {
    public final /* synthetic */ ShadowButton this$0;

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public ShadowButton$binding$2(ShadowButton shadowButton) {
        super(0);
        this.this$0 = shadowButton;
    }

    /* JADX WARN: Can't rename method to resolve collision */
    @Override // defpackage.rc1
    public final bi4 invoke() {
        View customEnd;
        customEnd = this.this$0.getCustomEnd();
        return bi4.a(customEnd);
    }
}
