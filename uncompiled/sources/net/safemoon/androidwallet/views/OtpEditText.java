package net.safemoon.androidwallet.views;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.text.Editable;
import android.util.AttributeSet;
import android.view.ActionMode;
import android.view.View;
import androidx.appcompat.widget.AppCompatEditText;
import com.github.mikephil.charting.utils.Utils;
import net.safemoon.androidwallet.R;
import net.safemoon.androidwallet.views.OtpEditText;

/* loaded from: classes2.dex */
public class OtpEditText extends AppCompatEditText {
    public float i0;
    public float j0;
    public float k0;
    public int l0;
    public float m0;
    public Paint n0;
    public View.OnClickListener o0;

    public OtpEditText(Context context) {
        super(context);
        this.i0 = 24.0f;
        this.j0 = 6.0f;
        this.k0 = 8.0f;
        this.l0 = 6;
        this.m0 = 2.0f;
    }

    /* JADX INFO: Access modifiers changed from: private */
    public /* synthetic */ void e(View view) {
        setSelection(getText().length());
        View.OnClickListener onClickListener = this.o0;
        if (onClickListener != null) {
            onClickListener.onClick(view);
        }
    }

    public final void d(Context context, AttributeSet attributeSet) {
        float f = context.getResources().getDisplayMetrics().density;
        this.m0 *= f;
        Paint paint = new Paint(getPaint());
        this.n0 = paint;
        paint.setStrokeWidth(this.m0);
        this.n0.setColor(m70.d(getContext(), R.color.curve_green));
        setBackgroundResource(0);
        this.i0 *= f;
        this.k0 = f * this.k0;
        this.j0 = this.l0;
        super.setOnClickListener(new View.OnClickListener() { // from class: bo2
            @Override // android.view.View.OnClickListener
            public final void onClick(View view) {
                OtpEditText.this.e(view);
            }
        });
    }

    @Override // android.widget.TextView, android.view.View
    public void onDraw(Canvas canvas) {
        float f;
        int i;
        int width = (getWidth() - getPaddingRight()) - getPaddingLeft();
        float f2 = this.i0;
        if (f2 < Utils.FLOAT_EPSILON) {
            f = width / ((this.j0 * 2.0f) - 1.0f);
        } else {
            float f3 = this.j0;
            f = (width - (f2 * (f3 - 1.0f))) / f3;
        }
        int paddingLeft = getPaddingLeft();
        int height = getHeight() - getPaddingBottom();
        Editable text = getText();
        int length = text.length();
        float[] fArr = new float[length];
        getPaint().getTextWidths(getText(), 0, length, fArr);
        getPaint().setColor(m70.d(getContext(), R.color.t1));
        int i2 = 0;
        while (i2 < this.j0) {
            float f4 = paddingLeft;
            float f5 = height;
            canvas.drawLine(f4, f5, f4 + f, f5, this.n0);
            if (getText().length() > i2) {
                i = i2;
                canvas.drawText(text, i2, i2 + 1, ((f / 2.0f) + f4) - (fArr[0] / 2.0f), f5 - this.k0, getPaint());
            } else {
                i = i2;
            }
            float f6 = this.i0;
            paddingLeft = (int) (f4 + (f6 < Utils.FLOAT_EPSILON ? f * 2.0f : f6 + f));
            i2 = i + 1;
        }
    }

    @Override // androidx.appcompat.widget.AppCompatEditText, android.widget.TextView
    public void setCustomSelectionActionModeCallback(ActionMode.Callback callback) {
        throw new RuntimeException("setCustomSelectionActionModeCallback() not supported.");
    }

    @Override // android.view.View
    public void setOnClickListener(View.OnClickListener onClickListener) {
        this.o0 = onClickListener;
    }

    public OtpEditText(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        this.i0 = 24.0f;
        this.j0 = 6.0f;
        this.k0 = 8.0f;
        this.l0 = 6;
        this.m0 = 2.0f;
        d(context, attributeSet);
    }

    public OtpEditText(Context context, AttributeSet attributeSet, int i) {
        super(context, attributeSet, i);
        this.i0 = 24.0f;
        this.j0 = 6.0f;
        this.k0 = 8.0f;
        this.l0 = 6;
        this.m0 = 2.0f;
        d(context, attributeSet);
    }
}
