package net.safemoon.androidwallet.views;

import android.content.ClipData;
import android.content.ClipboardManager;
import android.content.Context;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.AttributeSet;
import android.view.View;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import com.google.android.material.button.MaterialButton;
import com.google.android.material.textfield.TextInputLayout;
import net.safemoon.androidwallet.views.MyTextInputLayout;

/* compiled from: MyTextInputLayout.kt */
/* loaded from: classes2.dex */
public final class MyTextInputLayout extends TextInputLayout {
    public final sy1 O1;
    public final sy1 P1;
    public final sy1 Q1;

    /* compiled from: TextView.kt */
    /* loaded from: classes2.dex */
    public static final class a implements TextWatcher {
        public a() {
        }

        @Override // android.text.TextWatcher
        public void afterTextChanged(Editable editable) {
            MyTextInputLayout.this.M0(editable);
        }

        @Override // android.text.TextWatcher
        public void beforeTextChanged(CharSequence charSequence, int i, int i2, int i3) {
        }

        @Override // android.text.TextWatcher
        public void onTextChanged(CharSequence charSequence, int i, int i2, int i3) {
        }
    }

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public MyTextInputLayout(Context context) {
        super(context);
        fs1.f(context, "context");
        this.O1 = zy1.a(new MyTextInputLayout$manager$2(this));
        this.P1 = zy1.a(new MyTextInputLayout$customEnd$2(this));
        this.Q1 = zy1.a(new MyTextInputLayout$binding$2(this));
    }

    public static final void K0(MyTextInputLayout myTextInputLayout, View view) {
        fs1.f(myTextInputLayout, "this$0");
        ClipData primaryClip = myTextInputLayout.getManager().getPrimaryClip();
        if (primaryClip == null || primaryClip.getItemCount() <= 0 || primaryClip.getItemAt(0).getText() == null) {
            return;
        }
        ClipData.Item itemAt = primaryClip.getItemAt(0);
        MaterialButton materialButton = myTextInputLayout.getBinding().a;
        fs1.e(materialButton, "binding.btnClear");
        materialButton.setVisibility(0);
        EditText editText = myTextInputLayout.getEditText();
        if (editText != null) {
            editText.setText(itemAt.getText());
        }
        EditText editText2 = myTextInputLayout.getEditText();
        myTextInputLayout.M0(editText2 == null ? null : editText2.getText());
    }

    public static final void L0(MyTextInputLayout myTextInputLayout, View view) {
        fs1.f(myTextInputLayout, "this$0");
        EditText editText = myTextInputLayout.getEditText();
        if (editText == null) {
            return;
        }
        editText.setText("");
    }

    private final fk4 getBinding() {
        return (fk4) this.Q1.getValue();
    }

    /* JADX INFO: Access modifiers changed from: private */
    public final View getCustomEnd() {
        Object value = this.P1.getValue();
        fs1.e(value, "<get-customEnd>(...)");
        return (View) value;
    }

    private final ClipboardManager getManager() {
        return (ClipboardManager) this.O1.getValue();
    }

    public final void M0(Editable editable) {
        if (editable == null) {
            return;
        }
        boolean z = editable.toString().length() == 0;
        getBinding().a.setVisibility(e30.l0(z));
        MaterialButton materialButton = getBinding().b;
        fs1.e(materialButton, "binding.btnPaste");
        materialButton.setVisibility(z ? 0 : 8);
        EditText editText = getEditText();
        if (editText == null) {
            return;
        }
        editText.setPadding(0, editText.getPaddingTop(), (z ? getBinding().b : getBinding().a).getWidth(), editText.getPaddingBottom());
        editText.requestLayout();
    }

    public final void setUpDefaultView() {
        Editable text;
        View childAt;
        try {
            childAt = getChildAt(0);
        } catch (Exception unused) {
        }
        if (childAt == null) {
            throw new NullPointerException("null cannot be cast to non-null type android.widget.FrameLayout");
        }
        View childAt2 = ((FrameLayout) childAt).getChildAt(2);
        if (childAt2 != null) {
            LinearLayout linearLayout = (LinearLayout) childAt2;
            linearLayout.addView(getCustomEnd());
            getBinding().b.setOnClickListener(new View.OnClickListener() { // from class: ob2
                @Override // android.view.View.OnClickListener
                public final void onClick(View view) {
                    MyTextInputLayout.K0(MyTextInputLayout.this, view);
                }
            });
            getBinding().a.setOnClickListener(new View.OnClickListener() { // from class: nb2
                @Override // android.view.View.OnClickListener
                public final void onClick(View view) {
                    MyTextInputLayout.L0(MyTextInputLayout.this, view);
                }
            });
            linearLayout.bringToFront();
            EditText editText = getEditText();
            if (editText != null && (text = editText.getText()) != null) {
                getBinding().a.setVisibility(e30.l0(text.toString().length() == 0));
                MaterialButton materialButton = getBinding().b;
                fs1.e(materialButton, "binding.btnPaste");
                materialButton.setVisibility(text.toString().length() == 0 ? 0 : 8);
            }
            EditText editText2 = getEditText();
            if (editText2 == null) {
                return;
            }
            editText2.addTextChangedListener(new a());
            return;
        }
        throw new NullPointerException("null cannot be cast to non-null type android.widget.LinearLayout");
    }

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public MyTextInputLayout(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        fs1.f(context, "context");
        this.O1 = zy1.a(new MyTextInputLayout$manager$2(this));
        this.P1 = zy1.a(new MyTextInputLayout$customEnd$2(this));
        this.Q1 = zy1.a(new MyTextInputLayout$binding$2(this));
    }

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public MyTextInputLayout(Context context, AttributeSet attributeSet, int i) {
        super(context, attributeSet, i);
        fs1.f(context, "context");
        this.O1 = zy1.a(new MyTextInputLayout$manager$2(this));
        this.P1 = zy1.a(new MyTextInputLayout$customEnd$2(this));
        this.Q1 = zy1.a(new MyTextInputLayout$binding$2(this));
    }
}
