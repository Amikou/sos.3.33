package net.safemoon.androidwallet.views;

import kotlin.jvm.internal.Lambda;

/* compiled from: CustomRecoveryWalletLayout.kt */
/* loaded from: classes2.dex */
public final class CustomRecoveryWalletLayout$isBioAuth$2 extends Lambda implements rc1<Boolean> {
    public final /* synthetic */ CustomRecoveryWalletLayout this$0;

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public CustomRecoveryWalletLayout$isBioAuth$2(CustomRecoveryWalletLayout customRecoveryWalletLayout) {
        super(0);
        this.this$0 = customRecoveryWalletLayout;
    }

    /* JADX WARN: Can't rename method to resolve collision */
    @Override // defpackage.rc1
    public final Boolean invoke() {
        return Boolean.valueOf(bo3.e(this.this$0.getAttachedActivity(), "TWO_FACTOR", false));
    }
}
