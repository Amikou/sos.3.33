package net.safemoon.androidwallet.views;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import kotlin.jvm.internal.Lambda;
import net.safemoon.androidwallet.R;

/* compiled from: MyTextInputLayout.kt */
/* loaded from: classes2.dex */
public final class MyTextInputLayout$customEnd$2 extends Lambda implements rc1<View> {
    public final /* synthetic */ MyTextInputLayout this$0;

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public MyTextInputLayout$customEnd$2(MyTextInputLayout myTextInputLayout) {
        super(0);
        this.this$0 = myTextInputLayout;
    }

    /* JADX WARN: Can't rename method to resolve collision */
    @Override // defpackage.rc1
    public final View invoke() {
        return LayoutInflater.from(this.this$0.getContext()).inflate(R.layout.view_til_end_layout, (ViewGroup) this.this$0, false);
    }
}
