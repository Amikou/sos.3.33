package net.safemoon.androidwallet.views;

import android.content.Context;
import android.util.AttributeSet;
import android.view.MotionEvent;
import com.github.mikephil.charting.charts.LineChart;

/* compiled from: TouchControlLineChart.kt */
/* loaded from: classes2.dex */
public final class TouchControlLineChart extends LineChart {
    public tc1<? super MotionEvent, te4> a;

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public TouchControlLineChart(Context context) {
        super(context);
        fs1.f(context, "context");
    }

    public final void a(tc1<? super MotionEvent, te4> tc1Var) {
        fs1.f(tc1Var, "callBack");
        this.a = tc1Var;
    }

    @Override // com.github.mikephil.charting.charts.BarLineChartBase, android.view.View
    public boolean onTouchEvent(MotionEvent motionEvent) {
        tc1<? super MotionEvent, te4> tc1Var = this.a;
        if (tc1Var != null) {
            tc1Var.invoke(motionEvent);
        }
        return super.onTouchEvent(motionEvent);
    }

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public TouchControlLineChart(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        fs1.f(context, "context");
    }

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public TouchControlLineChart(Context context, AttributeSet attributeSet, int i) {
        super(context, attributeSet, i);
        fs1.f(context, "context");
    }
}
