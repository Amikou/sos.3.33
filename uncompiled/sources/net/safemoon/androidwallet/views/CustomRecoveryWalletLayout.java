package net.safemoon.androidwallet.views;

import android.app.Activity;
import android.content.ClipboardManager;
import android.content.Context;
import android.util.AttributeSet;
import android.view.View;
import android.view.ViewGroup;
import androidx.appcompat.widget.LinearLayoutCompat;
import androidx.fragment.app.FragmentManager;
import androidx.lifecycle.Lifecycle;
import androidx.lifecycle.f;
import defpackage.qm1;
import java.lang.ref.WeakReference;
import net.safemoon.androidwallet.R;
import net.safemoon.androidwallet.activity.common.BasicActivity;
import net.safemoon.androidwallet.dialogs.G2FAVerfication;
import net.safemoon.androidwallet.model.wallets.Wallet;
import net.safemoon.androidwallet.views.CustomRecoveryWalletLayout;

/* compiled from: CustomRecoveryWalletLayout.kt */
/* loaded from: classes2.dex */
public final class CustomRecoveryWalletLayout extends LinearLayoutCompat implements rz1 {
    public final f a;
    public final sy1 f0;
    public final sy1 g0;
    public final w41 h0;
    public BasicActivity i0;
    public Wallet j0;
    public boolean k0;
    public boolean l0;
    public final sy1 m0;

    /* compiled from: CustomRecoveryWalletLayout.kt */
    /* loaded from: classes2.dex */
    public static final class a {
        public a() {
        }

        public /* synthetic */ a(qi0 qi0Var) {
            this();
        }
    }

    /* compiled from: CustomRecoveryWalletLayout.kt */
    /* loaded from: classes2.dex */
    public static final class b implements G2FAVerfication.b {
        public final /* synthetic */ int b;

        public b(int i) {
            this.b = i;
        }

        @Override // net.safemoon.androidwallet.dialogs.G2FAVerfication.b
        public void a() {
            y54.a("Khang").a("G2FAVerificationCallback onError() called", new Object[0]);
        }

        @Override // net.safemoon.androidwallet.dialogs.G2FAVerfication.b
        public void onSuccess() {
            if (CustomRecoveryWalletLayout.this.v()) {
                CustomRecoveryWalletLayout.this.B(this.b);
                return;
            }
            CustomRecoveryWalletLayout customRecoveryWalletLayout = CustomRecoveryWalletLayout.this;
            BasicActivity attachedActivity = customRecoveryWalletLayout.getAttachedActivity();
            fs1.d(attachedActivity);
            customRecoveryWalletLayout.w(attachedActivity, this.b);
        }
    }

    /* compiled from: CustomRecoveryWalletLayout.kt */
    /* loaded from: classes2.dex */
    public static final class c implements qm1.a {
        public c() {
        }

        @Override // defpackage.qm1.a
        public void a() {
            CustomRecoveryWalletLayout customRecoveryWalletLayout = CustomRecoveryWalletLayout.this;
            BasicActivity attachedActivity = customRecoveryWalletLayout.getAttachedActivity();
            fs1.d(attachedActivity);
            customRecoveryWalletLayout.w(attachedActivity, 1);
        }

        @Override // defpackage.qm1.a
        public void b(int i) {
        }
    }

    /* compiled from: CustomRecoveryWalletLayout.kt */
    /* loaded from: classes2.dex */
    public static final class d implements qm1.a {
        public d() {
        }

        @Override // defpackage.qm1.a
        public void a() {
            CustomRecoveryWalletLayout customRecoveryWalletLayout = CustomRecoveryWalletLayout.this;
            BasicActivity attachedActivity = customRecoveryWalletLayout.getAttachedActivity();
            fs1.d(attachedActivity);
            customRecoveryWalletLayout.w(attachedActivity, 2);
        }

        @Override // defpackage.qm1.a
        public void b(int i) {
        }
    }

    static {
        new a(null);
    }

    /* JADX WARN: 'this' call moved to the top of the method (can break code semantics) */
    public CustomRecoveryWalletLayout(Context context) {
        this(context, null);
        fs1.f(context, "context");
    }

    private final wk1 getBinding() {
        return (wk1) this.f0.getValue();
    }

    private final ClipboardManager getManager() {
        return (ClipboardManager) this.m0.getValue();
    }

    private final qm1.a getPassphraseAuthCallback() {
        return new c();
    }

    private final qm1.a getPrivateKeyAuthCallback() {
        return new d();
    }

    public static final void o(CustomRecoveryWalletLayout customRecoveryWalletLayout, View view) {
        fs1.f(customRecoveryWalletLayout, "this$0");
        customRecoveryWalletLayout.A();
    }

    public static final void p(CustomRecoveryWalletLayout customRecoveryWalletLayout, View view) {
        fs1.f(customRecoveryWalletLayout, "this$0");
        customRecoveryWalletLayout.z();
    }

    public static final void q(CustomRecoveryWalletLayout customRecoveryWalletLayout, View view) {
        fs1.f(customRecoveryWalletLayout, "this$0");
        BasicActivity attachedActivity = customRecoveryWalletLayout.getAttachedActivity();
        if (attachedActivity == null) {
            return;
        }
        customRecoveryWalletLayout.s(attachedActivity);
    }

    public static final void r(CustomRecoveryWalletLayout customRecoveryWalletLayout, View view) {
        fs1.f(customRecoveryWalletLayout, "this$0");
        BasicActivity attachedActivity = customRecoveryWalletLayout.getAttachedActivity();
        if (attachedActivity == null) {
            return;
        }
        customRecoveryWalletLayout.t(attachedActivity);
    }

    public final void A() {
        this.k0 = !this.k0;
        wk1 binding = getBinding();
        if (this.k0) {
            binding.b.setImageResource(R.drawable.ic_akt_up_arrow);
            binding.h.setVisibility(0);
            binding.j.setVisibility(0);
            if (this.l0) {
                z();
                return;
            }
            return;
        }
        binding.b.setImageResource(R.drawable.ic_akt_dropdown);
        binding.h.setVisibility(8);
        binding.j.setVisibility(8);
        binding.g.setVisibility(8);
        binding.g.setText("");
    }

    public final void B(int i) {
        if (i == 1) {
            w41 w41Var = this.h0;
            BasicActivity basicActivity = this.i0;
            fs1.d(basicActivity);
            w41Var.b(basicActivity, getPassphraseAuthCallback());
            return;
        }
        w41 w41Var2 = this.h0;
        BasicActivity basicActivity2 = this.i0;
        fs1.d(basicActivity2);
        w41Var2.b(basicActivity2, getPrivateKeyAuthCallback());
    }

    public final BasicActivity getAttachedActivity() {
        return this.i0;
    }

    public final void n() {
        wk1 binding = getBinding();
        binding.b.setOnClickListener(new View.OnClickListener() { // from class: lc0
            @Override // android.view.View.OnClickListener
            public final void onClick(View view) {
                CustomRecoveryWalletLayout.o(CustomRecoveryWalletLayout.this, view);
            }
        });
        binding.a.setOnClickListener(new View.OnClickListener() { // from class: nc0
            @Override // android.view.View.OnClickListener
            public final void onClick(View view) {
                CustomRecoveryWalletLayout.p(CustomRecoveryWalletLayout.this, view);
            }
        });
        binding.j.setOnClickListener(new View.OnClickListener() { // from class: mc0
            @Override // android.view.View.OnClickListener
            public final void onClick(View view) {
                CustomRecoveryWalletLayout.q(CustomRecoveryWalletLayout.this, view);
            }
        });
        binding.i.setOnClickListener(new View.OnClickListener() { // from class: kc0
            @Override // android.view.View.OnClickListener
            public final void onClick(View view) {
                CustomRecoveryWalletLayout.r(CustomRecoveryWalletLayout.this, view);
            }
        });
    }

    @Override // android.view.ViewGroup, android.view.View
    public void onAttachedToWindow() {
        super.onAttachedToWindow();
        this.a.o(Lifecycle.State.RESUMED);
    }

    @Override // android.view.ViewGroup, android.view.View
    public void onDetachedFromWindow() {
        super.onDetachedFromWindow();
        this.a.o(Lifecycle.State.DESTROYED);
    }

    public final void s(BasicActivity basicActivity) {
        if (bo3.d(basicActivity, "AUTH_2FA_IS_ENABLE")) {
            G2FAVerfication a2 = G2FAVerfication.C0.a(u(1), v());
            FragmentManager supportFragmentManager = basicActivity.getSupportFragmentManager();
            fs1.e(supportFragmentManager, "activity.supportFragmentManager");
            a2.H(supportFragmentManager);
        } else if (v()) {
            this.h0.b(basicActivity, getPassphraseAuthCallback());
        } else {
            w(basicActivity, 1);
        }
    }

    public final void setAttachedActivity(BasicActivity basicActivity) {
        this.i0 = basicActivity;
    }

    /* JADX WARN: Code restructure failed: missing block: B:12:0x0040, code lost:
        if ((r14.length() == 0) == true) goto L6;
     */
    /*
        Code decompiled incorrectly, please refer to instructions dump.
        To view partially-correct code enable 'Show inconsistent code' option in preferences
    */
    public final void setupRecoveryWallet(net.safemoon.androidwallet.model.wallets.Wallet r14) {
        /*
            r13 = this;
            java.lang.String r0 = "wallet"
            defpackage.fs1.f(r14, r0)
            r13.j0 = r14
            if (r14 != 0) goto La
            goto L4f
        La:
            wk1 r0 = r13.getBinding()
            android.widget.TextView r0 = r0.d
            java.lang.String r1 = r14.getName()
            r4 = 0
            r5 = 4
            r6 = 0
            java.lang.String r2 = "|"
            java.lang.String r3 = ""
            java.lang.String r7 = defpackage.dv3.D(r1, r2, r3, r4, r5, r6)
            r10 = 0
            r11 = 4
            r12 = 0
            java.lang.String r8 = ","
            java.lang.String r9 = ""
            java.lang.String r1 = defpackage.dv3.D(r7, r8, r9, r10, r11, r12)
            r0.setText(r1)
            java.lang.String r14 = r14.getPassPhrase()
            r0 = 1
            r1 = 0
            if (r14 != 0) goto L37
        L35:
            r0 = r1
            goto L42
        L37:
            int r14 = r14.length()
            if (r14 != 0) goto L3f
            r14 = r0
            goto L40
        L3f:
            r14 = r1
        L40:
            if (r14 != r0) goto L35
        L42:
            if (r0 == 0) goto L4f
            wk1 r14 = r13.getBinding()
            androidx.constraintlayout.widget.ConstraintLayout r14 = r14.c
            r0 = 8
            r14.setVisibility(r0)
        L4f:
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: net.safemoon.androidwallet.views.CustomRecoveryWalletLayout.setupRecoveryWallet(net.safemoon.androidwallet.model.wallets.Wallet):void");
    }

    public final void t(BasicActivity basicActivity) {
        if (bo3.d(basicActivity, "AUTH_2FA_IS_ENABLE")) {
            G2FAVerfication a2 = G2FAVerfication.C0.a(u(2), v());
            FragmentManager supportFragmentManager = basicActivity.getSupportFragmentManager();
            fs1.e(supportFragmentManager, "activity.supportFragmentManager");
            a2.H(supportFragmentManager);
        } else if (v()) {
            this.h0.b(basicActivity, getPrivateKeyAuthCallback());
        } else {
            w(basicActivity, 2);
        }
    }

    public final G2FAVerfication.b u(int i) {
        return new b(i);
    }

    public final boolean v() {
        return ((Boolean) this.g0.getValue()).booleanValue();
    }

    public final void w(Activity activity, int i) {
        bh.U(new WeakReference(activity), Integer.valueOf((int) R.string.acknowledgment_confirm_title), Integer.valueOf((int) R.string.akt_existing_wallets_mnemonic_dialog_content), R.string.acknowledgment_confirm_button_text, new CustomRecoveryWalletLayout$showAcknowledgmentWarningDialog$1(i, this));
    }

    public final void x() {
        Wallet wallet2 = this.j0;
        if (wallet2 == null) {
            return;
        }
        getBinding().f.setVisibility(8);
        getBinding().i.setVisibility(8);
        getBinding().e.setText(wallet2.getPrivateKey());
        getBinding().e.setVisibility(0);
    }

    public final void y() {
        Wallet wallet2 = this.j0;
        if (wallet2 == null) {
            return;
        }
        getBinding().g.setText(wallet2.getPassPhrase());
        getBinding().h.setVisibility(8);
        getBinding().j.setVisibility(8);
        getBinding().g.setVisibility(0);
    }

    public final void z() {
        this.l0 = !this.l0;
        wk1 binding = getBinding();
        if (this.l0) {
            binding.a.setImageResource(R.drawable.ic_akt_up_arrow);
            binding.f.setVisibility(0);
            binding.i.setVisibility(0);
            if (this.k0) {
                A();
                return;
            }
            return;
        }
        binding.a.setImageResource(R.drawable.ic_akt_dropdown);
        binding.f.setVisibility(8);
        binding.i.setVisibility(8);
        binding.e.setVisibility(8);
        binding.e.setText("");
    }

    /* JADX WARN: 'this' call moved to the top of the method (can break code semantics) */
    public CustomRecoveryWalletLayout(Context context, AttributeSet attributeSet) {
        this(context, attributeSet, 0);
        fs1.f(context, "context");
    }

    @Override // defpackage.rz1
    public f getLifecycle() {
        return this.a;
    }

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public CustomRecoveryWalletLayout(Context context, AttributeSet attributeSet, int i) {
        super(context, attributeSet, i);
        fs1.f(context, "context");
        this.a = new f(this);
        this.f0 = zy1.a(new CustomRecoveryWalletLayout$binding$2(this));
        this.g0 = zy1.a(new CustomRecoveryWalletLayout$isBioAuth$2(this));
        this.h0 = new w41();
        this.m0 = zy1.a(new CustomRecoveryWalletLayout$manager$2(this));
        context.obtainStyledAttributes(attributeSet, s23.CustomRecoveryWalletLayout, i, 0).recycle();
        ViewGroup.inflate(context, R.layout.holder_recovery_wallet, this);
        n();
    }
}
