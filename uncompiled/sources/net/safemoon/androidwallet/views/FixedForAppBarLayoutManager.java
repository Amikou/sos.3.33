package net.safemoon.androidwallet.views;

import android.content.Context;
import android.graphics.Rect;
import android.view.View;
import android.view.ViewParent;
import androidx.recyclerview.widget.LinearLayoutManager;
import java.util.Objects;

/* compiled from: FixedForAppBarLayoutManager.kt */
/* loaded from: classes2.dex */
public final class FixedForAppBarLayoutManager extends LinearLayoutManager {
    public FixedForAppBarLayoutManager(Context context) {
        super(context);
    }

    public final boolean U2(View view, boolean z) {
        int[] iArr = new int[2];
        view.getLocationOnScreen(iArr);
        ViewParent parent = view.getParent();
        Objects.requireNonNull(parent, "null cannot be cast to non-null type android.view.View");
        Rect rect = new Rect();
        ((View) parent).getGlobalVisibleRect(rect);
        if (v2() == 0) {
            int i = iArr[0];
            int width = iArr[0] + view.getWidth();
            if (z) {
                if (i < rect.left || width > rect.right) {
                    return false;
                }
            } else if (i > rect.right || width < rect.left) {
                return false;
            }
        } else {
            int i2 = iArr[1];
            int height = iArr[1] + view.getHeight();
            if (z) {
                if (i2 < rect.top || height > rect.bottom) {
                    return false;
                }
            } else if (i2 > rect.bottom || height < rect.top) {
                return false;
            }
        }
        return true;
    }

    public final View V2(int i, int i2, boolean z) {
        int i3 = i2 > i ? 1 : -1;
        while (i != i2) {
            View T = T(i);
            if (T != null) {
                if (U2(T, z)) {
                    return T;
                }
                i += i3;
            }
        }
        return null;
    }

    @Override // androidx.recyclerview.widget.LinearLayoutManager
    public int h2() {
        View V2 = V2(0, U(), false);
        if (V2 == null) {
            return -1;
        }
        return o0(V2);
    }

    @Override // androidx.recyclerview.widget.LinearLayoutManager
    public int i2() {
        View V2 = V2(U() - 1, -1, true);
        if (V2 == null) {
            return -1;
        }
        return o0(V2);
    }

    @Override // androidx.recyclerview.widget.LinearLayoutManager
    public int k2() {
        View V2 = V2(U() - 1, -1, false);
        if (V2 == null) {
            return -1;
        }
        return o0(V2);
    }
}
