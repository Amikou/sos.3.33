package net.safemoon.androidwallet.views.marker;

import android.annotation.SuppressLint;
import android.content.Context;
import android.view.View;
import android.widget.TextView;
import com.github.mikephil.charting.data.Entry;
import com.github.mikephil.charting.highlight.Highlight;
import net.safemoon.androidwallet.R;

/* compiled from: LineGraphMarkerView.kt */
@SuppressLint({"ViewConstructor"})
/* loaded from: classes2.dex */
public final class LineGraphMarkerView extends BaseGraphMarkerView {
    public final String f0;
    public final TextView g0;

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public LineGraphMarkerView(Context context, int i, String str) {
        super(context, i);
        fs1.f(str, "currencySymbol");
        this.f0 = str;
        View findViewById = findViewById(R.id.tvMarkerContent);
        fs1.e(findViewById, "findViewById(R.id.tvMarkerContent)");
        this.g0 = (TextView) findViewById;
    }

    @Override // net.safemoon.androidwallet.views.marker.BaseGraphMarkerView
    public int getMarkerColor() {
        return R.color.p5;
    }

    @Override // com.github.mikephil.charting.components.MarkerView, com.github.mikephil.charting.components.IMarker
    public void refreshContent(Entry entry, Highlight highlight) {
        TextView textView = this.g0;
        Context context = getContext();
        Object[] objArr = new Object[3];
        objArr[0] = re0.a.a(entry == null ? 0L : entry.getX());
        objArr[1] = this.f0;
        objArr[2] = entry == null ? null : e30.p(entry.getY(), 0, null, false, 7, null);
        textView.setText(context.getString(R.string.chart_text_new_line, objArr));
        super.refreshContent(entry, highlight);
    }
}
