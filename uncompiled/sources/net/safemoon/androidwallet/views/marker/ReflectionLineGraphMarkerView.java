package net.safemoon.androidwallet.views.marker;

import android.annotation.SuppressLint;
import android.content.Context;
import android.widget.TextView;
import com.github.mikephil.charting.data.Entry;
import com.github.mikephil.charting.highlight.Highlight;
import java.util.Date;
import net.safemoon.androidwallet.R;

/* compiled from: ReflectionLineGraphMarkerView.kt */
@SuppressLint({"ViewConstructor"})
/* loaded from: classes2.dex */
public final class ReflectionLineGraphMarkerView extends BaseGraphMarkerView {
    public final String f0;
    public final TextView g0;

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public ReflectionLineGraphMarkerView(Context context, int i, String str) {
        super(context, i);
        fs1.f(str, "currencySymbol");
        this.f0 = str;
        findViewById(R.id.wrapContent).setBackgroundResource(R.color.white);
        te4 te4Var = te4.a;
        TextView textView = (TextView) findViewById(R.id.tvMarkerContent);
        textView.setTextColor(m70.d(textView.getContext(), R.color.curve_green));
        this.g0 = textView;
    }

    @Override // net.safemoon.androidwallet.views.marker.BaseGraphMarkerView
    public int getMarkerColor() {
        return R.color.white;
    }

    @Override // com.github.mikephil.charting.components.MarkerView, com.github.mikephil.charting.components.IMarker
    public void refreshContent(Entry entry, Highlight highlight) {
        TextView textView = this.g0;
        Context context = getContext();
        Object[] objArr = new Object[3];
        b30 b30Var = b30.a;
        Date date = new Date((entry == null ? 0L : entry.getX()) * 1000);
        Context context2 = getContext();
        fs1.e(context2, "context");
        objArr[0] = b30Var.e(date, context2);
        objArr[1] = this.f0;
        objArr[2] = entry == null ? null : e30.p(entry.getY(), 0, null, false, 7, null);
        textView.setText(context.getString(R.string.chart_text_new_line, objArr));
        super.refreshContent(entry, highlight);
    }
}
