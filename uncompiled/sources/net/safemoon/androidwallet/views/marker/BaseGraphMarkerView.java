package net.safemoon.androidwallet.views.marker;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Path;
import com.github.mikephil.charting.charts.Chart;
import com.github.mikephil.charting.components.MarkerView;
import com.github.mikephil.charting.utils.MPPointF;
import com.github.mikephil.charting.utils.Utils;

/* compiled from: BaseGraphMarkerView.kt */
/* loaded from: classes2.dex */
public abstract class BaseGraphMarkerView extends MarkerView {
    public static final float a;

    /* compiled from: BaseGraphMarkerView.kt */
    /* loaded from: classes2.dex */
    public static final class a {
        public a() {
        }

        public /* synthetic */ a(qi0 qi0Var) {
            this();
        }
    }

    static {
        new a(null);
        a = t40.a(2);
    }

    public BaseGraphMarkerView(Context context, int i) {
        super(context, i);
    }

    @Override // com.github.mikephil.charting.components.MarkerView, com.github.mikephil.charting.components.IMarker
    public void draw(Canvas canvas, float f, float f2) {
        Path path;
        fs1.f(canvas, "canvas");
        Paint paint = new Paint();
        paint.setStyle(Paint.Style.FILL);
        paint.setColor(g83.d(getResources(), getMarkerColor(), null));
        Chart chartView = getChartView();
        float width = getWidth() - a;
        float height = getHeight();
        MPPointF offsetForDrawingAtPoint = getOffsetForDrawingAtPoint(f, f2);
        int save = canvas.save();
        float f3 = height + 40.0f;
        if (f2 < f3) {
            path = new Path();
            path.moveTo(Utils.FLOAT_EPSILON, Utils.FLOAT_EPSILON);
            if (f > chartView.getWidth() - width) {
                path.lineTo(width - 40.0f, Utils.FLOAT_EPSILON);
                path.lineTo(width, -40.0f);
                path.lineTo(width, Utils.FLOAT_EPSILON);
            } else {
                float f4 = width / 2;
                if (f > f4) {
                    path.lineTo(f4 - 20.0f, Utils.FLOAT_EPSILON);
                    path.lineTo(f4, -40.0f);
                    path.lineTo(f4 + 20.0f, Utils.FLOAT_EPSILON);
                } else {
                    path.lineTo(Utils.FLOAT_EPSILON, -40.0f);
                    path.lineTo(40.0f, Utils.FLOAT_EPSILON);
                }
            }
            float f5 = 0;
            float f6 = width + f5;
            path.lineTo(f6, Utils.FLOAT_EPSILON);
            float f7 = f5 + height;
            path.lineTo(f6, f7);
            path.lineTo(Utils.FLOAT_EPSILON, f7);
            path.lineTo(Utils.FLOAT_EPSILON, Utils.FLOAT_EPSILON);
            path.offset(offsetForDrawingAtPoint.x + f, offsetForDrawingAtPoint.y + f2);
        } else {
            Path path2 = new Path();
            path2.moveTo(Utils.FLOAT_EPSILON, Utils.FLOAT_EPSILON);
            float f8 = 0;
            float f9 = f8 + width;
            path2.lineTo(f9, Utils.FLOAT_EPSILON);
            float f10 = f8 + height;
            path2.lineTo(f9, f10);
            if (f > chartView.getWidth() - width) {
                path2.lineTo(width, f3);
                path2.lineTo(width - 40.0f, f10);
                path2.lineTo(Utils.FLOAT_EPSILON, f10);
            } else {
                float f11 = width / 2;
                if (f > f11) {
                    path2.lineTo(f11 + 20.0f, f10);
                    path2.lineTo(f11, f3);
                    path2.lineTo(f11 - 20.0f, f10);
                    path2.lineTo(Utils.FLOAT_EPSILON, f10);
                } else {
                    path2.lineTo(40.0f, f10);
                    path2.lineTo(Utils.FLOAT_EPSILON, f3);
                    path2.lineTo(Utils.FLOAT_EPSILON, f10);
                }
            }
            path2.lineTo(Utils.FLOAT_EPSILON, Utils.FLOAT_EPSILON);
            path2.offset(offsetForDrawingAtPoint.x + f, offsetForDrawingAtPoint.y + f2);
            path = path2;
        }
        canvas.drawPath(path, paint);
        canvas.translate(f + offsetForDrawingAtPoint.x, f2 + offsetForDrawingAtPoint.y);
        draw(canvas);
        canvas.restoreToCount(save);
    }

    public abstract int getMarkerColor();

    @Override // com.github.mikephil.charting.components.MarkerView, com.github.mikephil.charting.components.IMarker
    public MPPointF getOffset() {
        return new MPPointF(-(getWidth() - (a / 2)), -getHeight());
    }

    @Override // com.github.mikephil.charting.components.MarkerView, com.github.mikephil.charting.components.IMarker
    public MPPointF getOffsetForDrawingAtPoint(float f, float f2) {
        MPPointF offset = getOffset();
        Chart chartView = getChartView();
        float width = getWidth() - a;
        float height = getHeight();
        if (f2 <= height + 40.0f) {
            offset.y = 40.0f;
        } else {
            offset.y = (-height) - 40.0f;
        }
        if (f > chartView.getWidth() - width) {
            offset.x = -width;
        } else {
            offset.x = Utils.FLOAT_EPSILON;
            float f3 = width / 2;
            if (f > f3) {
                offset.x = -f3;
            }
        }
        return offset;
    }
}
