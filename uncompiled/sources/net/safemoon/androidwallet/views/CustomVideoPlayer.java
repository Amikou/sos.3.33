package net.safemoon.androidwallet.views;

import android.app.Activity;
import android.content.Context;
import android.content.res.Configuration;
import android.content.res.TypedArray;
import android.net.Uri;
import android.os.Build;
import android.util.AttributeSet;
import android.view.View;
import androidx.media3.common.Metadata;
import androidx.media3.common.PlaybackException;
import androidx.media3.common.h;
import androidx.media3.common.m;
import androidx.media3.common.n;
import androidx.media3.common.p;
import androidx.media3.common.q;
import androidx.media3.common.u;
import androidx.media3.common.x;
import androidx.media3.common.y;
import androidx.media3.common.z;
import androidx.media3.exoplayer.f;
import androidx.media3.ui.PlayerControlView;
import androidx.media3.ui.PlayerView;
import java.util.List;
import java.util.Objects;

/* compiled from: CustomVideoPlayer.kt */
/* loaded from: classes2.dex */
public final class CustomVideoPlayer extends PlayerView implements q.d {
    public String G0;
    public PlayerControlView H0;
    public a I0;
    public boolean J0;
    public int K0;
    public long L0;
    public f M0;

    /* compiled from: CustomVideoPlayer.kt */
    /* loaded from: classes2.dex */
    public interface a {
        void i(int i);

        void y(boolean z, int i);
    }

    /* compiled from: CustomVideoPlayer.kt */
    /* loaded from: classes2.dex */
    public static abstract class b implements a {
        @Override // net.safemoon.androidwallet.views.CustomVideoPlayer.a
        public void y(boolean z, int i) {
        }
    }

    /* JADX WARN: 'this' call moved to the top of the method (can break code semantics) */
    public CustomVideoPlayer(Context context) {
        this(context, null);
        fs1.f(context, "context");
    }

    @Override // androidx.media3.common.q.d
    public /* synthetic */ void D(int i) {
        nr2.p(this, i);
    }

    @Override // androidx.media3.common.q.d
    public /* synthetic */ void E(boolean z) {
        nr2.i(this, z);
    }

    @Override // androidx.media3.common.q.d
    public /* synthetic */ void F(int i) {
        nr2.t(this, i);
    }

    @Override // androidx.media3.common.q.d
    public /* synthetic */ void H(boolean z) {
        nr2.g(this, z);
    }

    @Override // androidx.media3.common.q.d
    public /* synthetic */ void I() {
        nr2.x(this);
    }

    @Override // androidx.media3.common.q.d
    public /* synthetic */ void J(q qVar, q.c cVar) {
        nr2.f(this, qVar, cVar);
    }

    @Override // androidx.media3.common.q.d
    public /* synthetic */ void L(u uVar, int i) {
        nr2.B(this, uVar, i);
    }

    @Override // androidx.media3.common.q.d
    public /* synthetic */ void N(boolean z) {
        nr2.y(this, z);
    }

    @Override // androidx.media3.common.q.d
    public /* synthetic */ void Q(int i, boolean z) {
        nr2.e(this, i, z);
    }

    @Override // androidx.media3.common.q.d
    public /* synthetic */ void R(boolean z, int i) {
        nr2.s(this, z, i);
    }

    public final void S0() {
        if (this.G0 == null || this.M0 != null) {
            return;
        }
        f e = new f.b(getContext()).e();
        try {
            setPlayer(e);
            PlayerControlView videoController = getVideoController();
            if (videoController != null) {
                videoController.setPlayer(e);
            }
            String playURL = getPlayURL();
            fs1.d(playURL);
            m d = m.d(Uri.parse(playURL));
            fs1.e(d, "fromUri(Uri.parse(playURL!!))");
            e.A(d);
            e.K(1);
            e.w(this.J0);
            e.i(this.K0, this.L0);
            e.O(this);
            e.d();
        } catch (Exception unused) {
        }
        te4 te4Var = te4.a;
        this.M0 = e;
    }

    @Override // androidx.media3.common.q.d
    public /* synthetic */ void T(n nVar) {
        nr2.k(this, nVar);
    }

    public final void T0() {
        f fVar = this.M0;
        if (fVar != null) {
            this.L0 = fVar.c0();
            this.K0 = fVar.I();
            this.J0 = fVar.k();
            fVar.a();
        }
        this.M0 = null;
    }

    @Override // androidx.media3.common.q.d
    public /* synthetic */ void V(x xVar) {
        nr2.C(this, xVar);
    }

    @Override // androidx.media3.common.q.d
    public /* synthetic */ void X() {
        nr2.v(this);
    }

    @Override // androidx.media3.common.q.d
    public /* synthetic */ void Y(y yVar) {
        nr2.D(this, yVar);
    }

    @Override // androidx.media3.common.q.d
    public /* synthetic */ void Z(h hVar) {
        nr2.d(this, hVar);
    }

    @Override // androidx.media3.common.q.d
    public /* synthetic */ void a0(m mVar, int i) {
        nr2.j(this, mVar, i);
    }

    @Override // androidx.media3.common.q.d
    public /* synthetic */ void b(boolean z) {
        nr2.z(this, z);
    }

    @Override // androidx.media3.common.q.d
    public /* synthetic */ void c0(PlaybackException playbackException) {
        nr2.r(this, playbackException);
    }

    public final String getPlayURL() {
        return this.G0;
    }

    public final PlayerControlView getVideoController() {
        return this.H0;
    }

    @Override // androidx.media3.common.q.d
    public /* synthetic */ void h0(PlaybackException playbackException) {
        nr2.q(this, playbackException);
    }

    @Override // androidx.media3.common.q.d
    public void i(int i) {
        nr2.o(this, i);
        a aVar = this.I0;
        if (aVar == null) {
            return;
        }
        aVar.i(i);
    }

    @Override // androidx.media3.common.q.d
    public /* synthetic */ void k(z zVar) {
        nr2.E(this, zVar);
    }

    @Override // androidx.media3.common.q.d
    public /* synthetic */ void k0(int i, int i2) {
        nr2.A(this, i, i2);
    }

    @Override // androidx.media3.common.q.d
    public /* synthetic */ void l0(q.b bVar) {
        nr2.a(this, bVar);
    }

    @Override // androidx.media3.common.q.d
    public /* synthetic */ void m0(q.e eVar, q.e eVar2, int i) {
        nr2.u(this, eVar, eVar2, i);
    }

    @Override // androidx.media3.common.q.d
    public /* synthetic */ void n(p pVar) {
        nr2.n(this, pVar);
    }

    @Override // androidx.media3.common.q.d
    public /* synthetic */ void o(nb0 nb0Var) {
        nr2.b(this, nb0Var);
    }

    @Override // android.view.ViewGroup, android.view.View
    public void onAttachedToWindow() {
        super.onAttachedToWindow();
        if (this.G0 == null || this.M0 != null) {
            return;
        }
        S0();
    }

    @Override // android.view.View
    public void onConfigurationChanged(Configuration configuration) {
        super.onConfigurationChanged(configuration);
        if (getContext() instanceof Activity) {
            Context context = getContext();
            Objects.requireNonNull(context, "null cannot be cast to non-null type android.app.Activity");
            int c = j7.c((Activity) context);
            Context context2 = getContext();
            Objects.requireNonNull(context2, "null cannot be cast to non-null type android.app.Activity");
            setResizeMode(c < j7.b((Activity) context2) ? 1 : 2);
        }
    }

    @Override // android.view.ViewGroup, android.view.View
    public void onDetachedFromWindow() {
        super.onDetachedFromWindow();
        T0();
    }

    @Override // android.view.View
    public void onVisibilityAggregated(boolean z) {
        super.onVisibilityAggregated(z);
        if (Build.VERSION.SDK_INT >= 24) {
            if (z) {
                S0();
            } else {
                T0();
            }
        }
    }

    @Override // android.view.View
    public void onVisibilityChanged(View view, int i) {
        fs1.f(view, "changedView");
        super.onVisibilityChanged(view, i);
        if (Build.VERSION.SDK_INT < 24) {
            if (i == 0) {
                S0();
            } else {
                T0();
            }
        }
    }

    @Override // androidx.media3.common.q.d
    public /* synthetic */ void p0(boolean z) {
        nr2.h(this, z);
    }

    @Override // androidx.media3.common.q.d
    public /* synthetic */ void s(int i) {
        nr2.w(this, i);
    }

    public final void setIPlayerListener(b bVar) {
        fs1.f(bVar, "iPlayerListener");
        this.I0 = bVar;
    }

    public final void setPlayURL(String str) {
        this.G0 = str;
        T0();
        S0();
    }

    public final void setVideoController(PlayerControlView playerControlView) {
        this.H0 = playerControlView;
        T0();
        S0();
    }

    @Override // androidx.media3.common.q.d
    public /* synthetic */ void t(Metadata metadata) {
        nr2.l(this, metadata);
    }

    @Override // androidx.media3.common.q.d
    public /* synthetic */ void u(List list) {
        nr2.c(this, list);
    }

    @Override // androidx.media3.common.q.d
    public void y(boolean z, int i) {
        nr2.m(this, z, i);
        a aVar = this.I0;
        if (aVar == null) {
            return;
        }
        aVar.y(z, i);
    }

    /* JADX WARN: 'this' call moved to the top of the method (can break code semantics) */
    public CustomVideoPlayer(Context context, AttributeSet attributeSet) {
        this(context, attributeSet, 0);
        fs1.f(context, "context");
    }

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public CustomVideoPlayer(Context context, AttributeSet attributeSet, int i) {
        super(context, attributeSet, i);
        fs1.f(context, "context");
        this.J0 = true;
        TypedArray obtainStyledAttributes = context.obtainStyledAttributes(attributeSet, s23.CustomVideoPlayer, i, 0);
        this.J0 = obtainStyledAttributes.getBoolean(0, true);
        obtainStyledAttributes.recycle();
    }
}
