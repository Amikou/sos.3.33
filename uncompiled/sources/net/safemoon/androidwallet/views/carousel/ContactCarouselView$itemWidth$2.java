package net.safemoon.androidwallet.views.carousel;

import android.app.Activity;
import android.content.Context;
import kotlin.jvm.internal.Lambda;

/* compiled from: ContactCarouselView.kt */
/* loaded from: classes2.dex */
public final class ContactCarouselView$itemWidth$2 extends Lambda implements rc1<Integer> {
    public final /* synthetic */ Context $context;

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public ContactCarouselView$itemWidth$2(Context context) {
        super(0);
        this.$context = context;
    }

    /* JADX WARN: Can't rename method to resolve collision */
    @Override // defpackage.rc1
    public final Integer invoke() {
        return Integer.valueOf(e30.A((Activity) this.$context) / 5);
    }
}
