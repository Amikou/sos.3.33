package net.safemoon.androidwallet.views.carousel;

import android.content.Context;
import android.util.AttributeSet;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import com.bumptech.glide.e;
import com.google.android.material.card.MaterialCardView;
import com.jama.carouselview.CarouselView;
import com.jama.carouselview.enums.OffsetType;
import java.util.ArrayList;
import java.util.List;
import net.safemoon.androidwallet.R;
import net.safemoon.androidwallet.model.contact.abstraction.IContact;
import net.safemoon.androidwallet.views.carousel.ContactCarouselView;

/* compiled from: ContactCarouselView.kt */
/* loaded from: classes2.dex */
public final class ContactCarouselView extends CarouselView implements lw {
    public b A0;
    public final List<IContact> w0;
    public final int x0;
    public int y0;
    public final sy1 z0;

    /* compiled from: ContactCarouselView.kt */
    /* loaded from: classes2.dex */
    public static final class a {
        public a() {
        }

        public /* synthetic */ a(qi0 qi0Var) {
            this();
        }
    }

    /* compiled from: ContactCarouselView.kt */
    /* loaded from: classes2.dex */
    public interface b {
        void a(boolean z, int i, IContact iContact);
    }

    static {
        new a(null);
    }

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public ContactCarouselView(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        fs1.f(context, "context");
        fs1.f(attributeSet, "attributeSet");
        this.w0 = new ArrayList();
        this.x0 = 250;
        this.y0 = -1;
        this.z0 = zy1.a(new ContactCarouselView$itemWidth$2(context));
    }

    private final int getContactItemPosition() throws NullPointerException {
        int i = 0;
        for (IContact iContact : this.w0) {
            int i2 = i + 1;
            if (iContact.getId() == this.y0) {
                return i;
            }
            i = i2;
        }
        throw new NullPointerException("Contact id[" + this.y0 + "] not in the item list");
    }

    private final int getItemWidth() {
        return ((Number) this.z0.getValue()).intValue();
    }

    public static final void u(ContactCarouselView contactCarouselView, IContact iContact, int i, View view) {
        fs1.f(contactCarouselView, "this$0");
        fs1.f(iContact, "$model");
        contactCarouselView.y0 = contactCarouselView.t(iContact) ? -1 : iContact.getId();
        b bVar = contactCarouselView.A0;
        if (bVar != null) {
            bVar.a(contactCarouselView.t(iContact), i, iContact);
        }
        contactCarouselView.v();
    }

    @Override // defpackage.lw
    public void a(View view, final int i) {
        fs1.f(view, "view");
        final IContact iContact = this.w0.get(i);
        ViewGroup viewGroup = (ViewGroup) view.findViewById(R.id.llItemContactCarouselContainer);
        viewGroup.setVisibility(0);
        viewGroup.getLayoutParams().width = getItemWidth();
        viewGroup.setOnClickListener(new View.OnClickListener() { // from class: r60
            @Override // android.view.View.OnClickListener
            public final void onClick(View view2) {
                ContactCarouselView.u(ContactCarouselView.this, iContact, i, view2);
            }
        });
        View findViewById = view.findViewById(R.id.tvContactText);
        fs1.e(findViewById, "view.findViewById(R.id.tvContactText)");
        ((TextView) findViewById).setText(iContact.getName());
        View findViewById2 = view.findViewById(R.id.ivContactIcon);
        fs1.e(findViewById2, "view.findViewById(R.id.ivContactIcon)");
        ImageView imageView = (ImageView) findViewById2;
        e a2 = com.bumptech.glide.a.u(imageView).y(iContact.getProfilePath()).e0(R.drawable.contact_no_icon).l(R.drawable.contact_no_icon).a(n73.v0());
        int i2 = this.x0;
        a2.d0(i2, i2).I0(imageView);
        MaterialCardView materialCardView = (MaterialCardView) view.findViewById(R.id.ivContactIconContainer);
        if (t(iContact)) {
            materialCardView.setStrokeWidth((int) t40.a(2));
        } else {
            materialCardView.setStrokeWidth(0);
        }
    }

    public final void s(int i) {
        setSize(this.w0.size());
        setResource(R.layout.item_contact_carousel);
        setAutoPlay(false);
        i(true);
        setCurrentItem(i);
        setCarouselOffset(OffsetType.CENTER);
        setScaleOnScroll(true);
        setScrollBarSize(5);
        setCarouselViewListener(this);
    }

    public final void setItems(List<? extends IContact> list, int i) {
        fs1.f(list, "items");
        if (fs1.b(this.w0, list)) {
            return;
        }
        w(list);
        s(i);
    }

    public final void setOnItemSelectedChangedListener(b bVar) {
        fs1.f(bVar, "listener");
        this.A0 = bVar;
    }

    public final void setSelectedItem(IContact iContact) {
        fs1.f(iContact, "item");
        this.y0 = iContact.getId();
        v();
    }

    public final boolean t(IContact iContact) {
        return iContact.getId() == this.y0;
    }

    public final void v() {
        try {
            m();
            int currentItem = getCurrentItem();
            setCurrentItem(getContactItemPosition());
            if (currentItem != getCurrentItem()) {
                d();
            }
        } catch (Exception e) {
            String message = e.getMessage();
            if (message == null) {
                return;
            }
            String simpleName = ContactCarouselView.class.getSimpleName();
            fs1.e(simpleName, "this::class.java.simpleName");
            e30.c0(message, simpleName);
        }
    }

    public final void w(List<? extends IContact> list) {
        this.w0.clear();
        this.w0.addAll(list);
    }
}
