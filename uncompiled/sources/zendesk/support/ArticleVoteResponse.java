package zendesk.support;

/* loaded from: classes3.dex */
class ArticleVoteResponse {
    private ArticleVote vote;

    public ArticleVote getVote() {
        return this.vote;
    }
}
