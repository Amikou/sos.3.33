package zendesk.support;

import okhttp3.RequestBody;
import retrofit2.b;

/* loaded from: classes3.dex */
interface UploadService {
    @kd0("/api/mobile/uploads/{token}.json")
    b<Void> deleteAttachment(@vp2("token") String str);

    @oo2("/api/mobile/uploads.json")
    b<UploadResponseWrapper> uploadAttachment(@yw2("filename") String str, @ar RequestBody requestBody);
}
