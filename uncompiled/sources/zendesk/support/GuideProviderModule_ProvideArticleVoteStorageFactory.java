package zendesk.support;

import zendesk.core.SessionStorage;

/* loaded from: classes3.dex */
public final class GuideProviderModule_ProvideArticleVoteStorageFactory implements y11<ArticleVoteStorage> {
    private final ew2<SessionStorage> baseStorageProvider;

    public GuideProviderModule_ProvideArticleVoteStorageFactory(ew2<SessionStorage> ew2Var) {
        this.baseStorageProvider = ew2Var;
    }

    public static GuideProviderModule_ProvideArticleVoteStorageFactory create(ew2<SessionStorage> ew2Var) {
        return new GuideProviderModule_ProvideArticleVoteStorageFactory(ew2Var);
    }

    public static ArticleVoteStorage provideArticleVoteStorage(SessionStorage sessionStorage) {
        return (ArticleVoteStorage) cu2.f(GuideProviderModule.provideArticleVoteStorage(sessionStorage));
    }

    @Override // defpackage.ew2
    public ArticleVoteStorage get() {
        return provideArticleVoteStorage(this.baseStorageProvider.get());
    }
}
