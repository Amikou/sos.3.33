package zendesk.support;

import com.zendesk.logger.Logger;
import java.util.Locale;
import zendesk.core.SettingsPack;
import zendesk.core.SettingsProvider;
import zendesk.core.ZendeskLocaleConverter;

/* loaded from: classes3.dex */
public class ZendeskHelpCenterSettingsProvider implements HelpCenterSettingsProvider {
    private static final String HELP_CENTER_KEY = "help_center";
    private static final String LOG_TAG = "HelpCenterSettingsProvider";
    private final Locale deviceLocale;
    private final ZendeskLocaleConverter localeConverter;
    private final SettingsProvider sdkSettingsProvider;

    public ZendeskHelpCenterSettingsProvider(SettingsProvider settingsProvider, ZendeskLocaleConverter zendeskLocaleConverter, Locale locale) {
        this.sdkSettingsProvider = settingsProvider;
        this.localeConverter = zendeskLocaleConverter;
        this.deviceLocale = locale;
    }

    /* JADX INFO: Access modifiers changed from: private */
    public void logIfLocaleNotAvailable(HelpCenterSettings helpCenterSettings) {
        if (helpCenterSettings == null || helpCenterSettings.getLocale() == null) {
            return;
        }
        String locale = helpCenterSettings.getLocale();
        String helpCenterLocaleString = this.localeConverter.toHelpCenterLocaleString(this.deviceLocale);
        if (!helpCenterLocaleString.equals(locale)) {
            Logger.k(LOG_TAG, "No support for %s, Help Center is %s in your application settings", helpCenterLocaleString, Boolean.valueOf(helpCenterSettings.isEnabled()));
        }
    }

    @Override // zendesk.support.HelpCenterSettingsProvider
    public void getSettings(final rs4<HelpCenterSettings> rs4Var) {
        this.sdkSettingsProvider.getSettingsForSdk(HELP_CENTER_KEY, HelpCenterSettings.class, new rs4<SettingsPack<HelpCenterSettings>>() { // from class: zendesk.support.ZendeskHelpCenterSettingsProvider.1
            @Override // defpackage.rs4
            public void onError(cw0 cw0Var) {
                if (rs4Var != null) {
                    Logger.b(ZendeskHelpCenterSettingsProvider.LOG_TAG, "Returning default Help Center Settings.", new Object[0]);
                    rs4Var.onSuccess(HelpCenterSettings.defaultSettings());
                }
            }

            @Override // defpackage.rs4
            public void onSuccess(SettingsPack<HelpCenterSettings> settingsPack) {
                ZendeskHelpCenterSettingsProvider.this.logIfLocaleNotAvailable(settingsPack.getSettings());
                Logger.b(ZendeskHelpCenterSettingsProvider.LOG_TAG, "Successfully retrieved Help Center Settings", new Object[0]);
                rs4 rs4Var2 = rs4Var;
                if (rs4Var2 != null) {
                    rs4Var2.onSuccess(settingsPack.getSettings());
                }
            }
        });
    }
}
