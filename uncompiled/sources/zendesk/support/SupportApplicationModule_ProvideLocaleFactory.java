package zendesk.support;

import java.util.Locale;

/* loaded from: classes3.dex */
public final class SupportApplicationModule_ProvideLocaleFactory implements y11<Locale> {
    private final SupportApplicationModule module;

    public SupportApplicationModule_ProvideLocaleFactory(SupportApplicationModule supportApplicationModule) {
        this.module = supportApplicationModule;
    }

    public static SupportApplicationModule_ProvideLocaleFactory create(SupportApplicationModule supportApplicationModule) {
        return new SupportApplicationModule_ProvideLocaleFactory(supportApplicationModule);
    }

    public static Locale provideLocale(SupportApplicationModule supportApplicationModule) {
        return (Locale) cu2.f(supportApplicationModule.provideLocale());
    }

    @Override // defpackage.ew2
    public Locale get() {
        return provideLocale(this.module);
    }
}
