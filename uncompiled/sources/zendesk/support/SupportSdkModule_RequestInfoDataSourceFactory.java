package zendesk.support;

import java.util.concurrent.Executor;
import java.util.concurrent.ExecutorService;
import zendesk.support.requestlist.RequestInfoDataSource;

/* loaded from: classes3.dex */
public final class SupportSdkModule_RequestInfoDataSourceFactory implements y11<RequestInfoDataSource.LocalDataSource> {
    private final ew2<ExecutorService> backgroundThreadExecutorProvider;
    private final ew2<Executor> mainThreadExecutorProvider;
    private final SupportSdkModule module;
    private final ew2<SupportUiStorage> supportUiStorageProvider;

    public SupportSdkModule_RequestInfoDataSourceFactory(SupportSdkModule supportSdkModule, ew2<SupportUiStorage> ew2Var, ew2<Executor> ew2Var2, ew2<ExecutorService> ew2Var3) {
        this.module = supportSdkModule;
        this.supportUiStorageProvider = ew2Var;
        this.mainThreadExecutorProvider = ew2Var2;
        this.backgroundThreadExecutorProvider = ew2Var3;
    }

    public static SupportSdkModule_RequestInfoDataSourceFactory create(SupportSdkModule supportSdkModule, ew2<SupportUiStorage> ew2Var, ew2<Executor> ew2Var2, ew2<ExecutorService> ew2Var3) {
        return new SupportSdkModule_RequestInfoDataSourceFactory(supportSdkModule, ew2Var, ew2Var2, ew2Var3);
    }

    public static RequestInfoDataSource.LocalDataSource requestInfoDataSource(SupportSdkModule supportSdkModule, SupportUiStorage supportUiStorage, Executor executor, ExecutorService executorService) {
        return (RequestInfoDataSource.LocalDataSource) cu2.f(supportSdkModule.requestInfoDataSource(supportUiStorage, executor, executorService));
    }

    @Override // defpackage.ew2
    public RequestInfoDataSource.LocalDataSource get() {
        return requestInfoDataSource(this.module, this.supportUiStorageProvider.get(), this.mainThreadExecutorProvider.get(), this.backgroundThreadExecutorProvider.get());
    }
}
