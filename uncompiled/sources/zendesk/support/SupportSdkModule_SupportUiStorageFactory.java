package zendesk.support;

import com.google.gson.Gson;

/* loaded from: classes3.dex */
public final class SupportSdkModule_SupportUiStorageFactory implements y11<SupportUiStorage> {
    private final ew2<ep0> diskLruCacheProvider;
    private final ew2<Gson> gsonProvider;
    private final SupportSdkModule module;

    public SupportSdkModule_SupportUiStorageFactory(SupportSdkModule supportSdkModule, ew2<ep0> ew2Var, ew2<Gson> ew2Var2) {
        this.module = supportSdkModule;
        this.diskLruCacheProvider = ew2Var;
        this.gsonProvider = ew2Var2;
    }

    public static SupportSdkModule_SupportUiStorageFactory create(SupportSdkModule supportSdkModule, ew2<ep0> ew2Var, ew2<Gson> ew2Var2) {
        return new SupportSdkModule_SupportUiStorageFactory(supportSdkModule, ew2Var, ew2Var2);
    }

    public static SupportUiStorage supportUiStorage(SupportSdkModule supportSdkModule, ep0 ep0Var, Gson gson) {
        return (SupportUiStorage) cu2.f(supportSdkModule.supportUiStorage(ep0Var, gson));
    }

    @Override // defpackage.ew2
    public SupportUiStorage get() {
        return supportUiStorage(this.module, this.diskLruCacheProvider.get(), this.gsonProvider.get());
    }
}
