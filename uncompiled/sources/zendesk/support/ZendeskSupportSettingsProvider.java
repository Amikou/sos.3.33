package zendesk.support;

import com.zendesk.logger.Logger;
import java.util.Locale;
import zendesk.core.AuthenticationType;
import zendesk.core.SettingsPack;
import zendesk.core.SettingsProvider;
import zendesk.core.ZendeskLocaleConverter;

/* loaded from: classes3.dex */
public class ZendeskSupportSettingsProvider implements SupportSettingsProvider {
    private static final String HELP_CENTER_KEY = "help_center";
    private static final String LOG_TAG = "ZendeskSettingsProvider";
    public static final String SUPPORT_KEY = "support";
    private final Locale deviceLocale;
    private final ZendeskLocaleConverter localeConverter;
    private final SettingsProvider sdkSettingsProvider;

    /* loaded from: classes3.dex */
    public class LoadSupportSettings extends rs4<SettingsPack<SupportSettings>> {
        private final rs4<SupportSdkSettings> callback;

        /* loaded from: classes3.dex */
        public class LoadHelpCenterSettings extends rs4<SettingsPack<HelpCenterSettings>> {
            private final rs4<SupportSdkSettings> callback;
            private final SettingsPack<SupportSettings> supportSettingsPack;

            public LoadHelpCenterSettings(rs4<SupportSdkSettings> rs4Var, SettingsPack<SupportSettings> settingsPack) {
                this.callback = rs4Var;
                this.supportSettingsPack = settingsPack;
            }

            @Override // defpackage.rs4
            public void onError(cw0 cw0Var) {
                if (this.callback != null) {
                    Logger.b(ZendeskSupportSettingsProvider.LOG_TAG, "Returning default Help Center Settings.", new Object[0]);
                    this.callback.onSuccess(new SupportSdkSettings(this.supportSettingsPack.getSettings(), HelpCenterSettings.defaultSettings(), this.supportSettingsPack.getCoreSettings().getAuthentication()));
                }
            }

            @Override // defpackage.rs4
            public void onSuccess(SettingsPack<HelpCenterSettings> settingsPack) {
                HelpCenterSettings settings = settingsPack.getSettings();
                AuthenticationType authentication = settingsPack.getCoreSettings().getAuthentication();
                LoadSupportSettings.this.logIfLocaleNotAvailable(settings);
                Logger.b(ZendeskSupportSettingsProvider.LOG_TAG, "Successfully retrieved Settings", new Object[0]);
                rs4<SupportSdkSettings> rs4Var = this.callback;
                if (rs4Var != null) {
                    rs4Var.onSuccess(new SupportSdkSettings(this.supportSettingsPack.getSettings(), settings, authentication));
                }
            }
        }

        public LoadSupportSettings(rs4<SupportSdkSettings> rs4Var) {
            this.callback = rs4Var;
        }

        /* JADX INFO: Access modifiers changed from: private */
        public void logIfLocaleNotAvailable(HelpCenterSettings helpCenterSettings) {
            if (helpCenterSettings == null || helpCenterSettings.getLocale() == null) {
                return;
            }
            String locale = helpCenterSettings.getLocale();
            String helpCenterLocaleString = ZendeskSupportSettingsProvider.this.localeConverter.toHelpCenterLocaleString(ZendeskSupportSettingsProvider.this.deviceLocale);
            if (!helpCenterLocaleString.equals(locale)) {
                Logger.k(ZendeskSupportSettingsProvider.LOG_TAG, "No support for %s, Help Center is %s in your application settings", helpCenterLocaleString, Boolean.valueOf(helpCenterSettings.isEnabled()));
            }
        }

        @Override // defpackage.rs4
        public void onError(cw0 cw0Var) {
            if (this.callback != null) {
                Logger.b(ZendeskSupportSettingsProvider.LOG_TAG, "Returning default Support Settings.", new Object[0]);
                this.callback.onSuccess(new SupportSdkSettings(SupportSettings.defaultSettings(), HelpCenterSettings.defaultSettings(), null));
            }
        }

        @Override // defpackage.rs4
        public void onSuccess(SettingsPack<SupportSettings> settingsPack) {
            ZendeskSupportSettingsProvider.this.sdkSettingsProvider.getSettingsForSdk(ZendeskSupportSettingsProvider.HELP_CENTER_KEY, HelpCenterSettings.class, new LoadHelpCenterSettings(this.callback, settingsPack));
        }
    }

    public ZendeskSupportSettingsProvider(SettingsProvider settingsProvider, ZendeskLocaleConverter zendeskLocaleConverter, Locale locale) {
        this.sdkSettingsProvider = settingsProvider;
        this.localeConverter = zendeskLocaleConverter;
        this.deviceLocale = locale;
    }

    @Override // zendesk.support.SupportSettingsProvider
    public void getSettings(rs4<SupportSdkSettings> rs4Var) {
        this.sdkSettingsProvider.getSettingsForSdk(SUPPORT_KEY, SupportSettings.class, new LoadSupportSettings(rs4Var));
    }
}
