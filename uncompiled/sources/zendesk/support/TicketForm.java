package zendesk.support;

import java.util.List;

/* loaded from: classes3.dex */
public class TicketForm {
    private long id;
    private String name;
    private List<TicketField> ticketFields;

    public TicketForm(long j, String str, List<TicketField> list) {
        this.id = j;
        this.name = str;
        this.ticketFields = l10.c(list);
    }

    public long getId() {
        return this.id;
    }

    public String getName() {
        return this.name;
    }

    public List<TicketField> getTicketFields() {
        return l10.c(this.ticketFields);
    }
}
