package zendesk.support;

import android.content.Context;

/* loaded from: classes3.dex */
public final class SupportApplicationModule_ProvideMetadataFactory implements y11<SupportSdkMetadata> {
    private final ew2<Context> contextProvider;
    private final SupportApplicationModule module;

    public SupportApplicationModule_ProvideMetadataFactory(SupportApplicationModule supportApplicationModule, ew2<Context> ew2Var) {
        this.module = supportApplicationModule;
        this.contextProvider = ew2Var;
    }

    public static SupportApplicationModule_ProvideMetadataFactory create(SupportApplicationModule supportApplicationModule, ew2<Context> ew2Var) {
        return new SupportApplicationModule_ProvideMetadataFactory(supportApplicationModule, ew2Var);
    }

    public static SupportSdkMetadata provideMetadata(SupportApplicationModule supportApplicationModule, Context context) {
        return (SupportSdkMetadata) cu2.f(supportApplicationModule.provideMetadata(context));
    }

    @Override // defpackage.ew2
    public SupportSdkMetadata get() {
        return provideMetadata(this.module, this.contextProvider.get());
    }
}
