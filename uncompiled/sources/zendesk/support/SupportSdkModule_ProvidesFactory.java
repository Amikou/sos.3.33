package zendesk.support;

import com.google.gson.Gson;

/* loaded from: classes3.dex */
public final class SupportSdkModule_ProvidesFactory implements y11<Gson> {
    private final SupportSdkModule module;

    public SupportSdkModule_ProvidesFactory(SupportSdkModule supportSdkModule) {
        this.module = supportSdkModule;
    }

    public static SupportSdkModule_ProvidesFactory create(SupportSdkModule supportSdkModule) {
        return new SupportSdkModule_ProvidesFactory(supportSdkModule);
    }

    public static Gson provides(SupportSdkModule supportSdkModule) {
        return (Gson) cu2.f(supportSdkModule.provides());
    }

    @Override // defpackage.ew2
    public Gson get() {
        return provides(this.module);
    }
}
