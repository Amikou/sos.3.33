package zendesk.support;

/* loaded from: classes3.dex */
public final class GuideModule_ProvidesSettingsProviderFactory implements y11<HelpCenterSettingsProvider> {
    private final GuideModule module;

    public GuideModule_ProvidesSettingsProviderFactory(GuideModule guideModule) {
        this.module = guideModule;
    }

    public static GuideModule_ProvidesSettingsProviderFactory create(GuideModule guideModule) {
        return new GuideModule_ProvidesSettingsProviderFactory(guideModule);
    }

    public static HelpCenterSettingsProvider providesSettingsProvider(GuideModule guideModule) {
        return (HelpCenterSettingsProvider) cu2.f(guideModule.providesSettingsProvider());
    }

    @Override // defpackage.ew2
    public HelpCenterSettingsProvider get() {
        return providesSettingsProvider(this.module);
    }
}
