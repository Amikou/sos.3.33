package zendesk.support;

/* loaded from: classes3.dex */
public class RawTicketFieldSystemOption {
    private String name;
    private String value;

    public String getName() {
        return this.name;
    }

    public String getValue() {
        return this.value;
    }
}
