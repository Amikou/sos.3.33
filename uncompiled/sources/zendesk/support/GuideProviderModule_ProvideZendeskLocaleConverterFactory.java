package zendesk.support;

import zendesk.core.ZendeskLocaleConverter;

/* loaded from: classes3.dex */
public final class GuideProviderModule_ProvideZendeskLocaleConverterFactory implements y11<ZendeskLocaleConverter> {

    /* loaded from: classes3.dex */
    public static final class InstanceHolder {
        private static final GuideProviderModule_ProvideZendeskLocaleConverterFactory INSTANCE = new GuideProviderModule_ProvideZendeskLocaleConverterFactory();

        private InstanceHolder() {
        }
    }

    public static GuideProviderModule_ProvideZendeskLocaleConverterFactory create() {
        return InstanceHolder.INSTANCE;
    }

    public static ZendeskLocaleConverter provideZendeskLocaleConverter() {
        return (ZendeskLocaleConverter) cu2.f(GuideProviderModule.provideZendeskLocaleConverter());
    }

    @Override // defpackage.ew2
    public ZendeskLocaleConverter get() {
        return provideZendeskLocaleConverter();
    }
}
