package zendesk.support;

/* loaded from: classes3.dex */
public final class GuideProviderModule_ProvideCustomNetworkConfigFactory implements y11<HelpCenterCachingNetworkConfig> {
    private final ew2<HelpCenterCachingInterceptor> helpCenterCachingInterceptorProvider;

    public GuideProviderModule_ProvideCustomNetworkConfigFactory(ew2<HelpCenterCachingInterceptor> ew2Var) {
        this.helpCenterCachingInterceptorProvider = ew2Var;
    }

    public static GuideProviderModule_ProvideCustomNetworkConfigFactory create(ew2<HelpCenterCachingInterceptor> ew2Var) {
        return new GuideProviderModule_ProvideCustomNetworkConfigFactory(ew2Var);
    }

    public static HelpCenterCachingNetworkConfig provideCustomNetworkConfig(Object obj) {
        return (HelpCenterCachingNetworkConfig) cu2.f(GuideProviderModule.provideCustomNetworkConfig((HelpCenterCachingInterceptor) obj));
    }

    @Override // defpackage.ew2
    public HelpCenterCachingNetworkConfig get() {
        return provideCustomNetworkConfig(this.helpCenterCachingInterceptorProvider.get());
    }
}
