package zendesk.support;

import java.io.File;
import okhttp3.MediaType;
import okhttp3.RequestBody;

/* loaded from: classes3.dex */
public class ZendeskUploadService {
    private static final String LOG_TAG = "ZendeskUploadService";
    private final UploadService uploadService;

    public ZendeskUploadService(UploadService uploadService) {
        this.uploadService = uploadService;
    }

    public void deleteAttachment(String str, rs4<Void> rs4Var) {
        this.uploadService.deleteAttachment(str).n(new w83(rs4Var));
    }

    public void uploadAttachment(String str, File file, String str2, rs4<UploadResponseWrapper> rs4Var) {
        this.uploadService.uploadAttachment(str, RequestBody.create(MediaType.parse(str2), file)).n(new w83(rs4Var));
    }
}
