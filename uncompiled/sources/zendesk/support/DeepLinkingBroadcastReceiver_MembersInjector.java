package zendesk.support;

import zendesk.core.ActionHandlerRegistry;

/* loaded from: classes3.dex */
public final class DeepLinkingBroadcastReceiver_MembersInjector implements i72<DeepLinkingBroadcastReceiver> {
    private final ew2<ActionHandlerRegistry> registryProvider;

    public DeepLinkingBroadcastReceiver_MembersInjector(ew2<ActionHandlerRegistry> ew2Var) {
        this.registryProvider = ew2Var;
    }

    public static i72<DeepLinkingBroadcastReceiver> create(ew2<ActionHandlerRegistry> ew2Var) {
        return new DeepLinkingBroadcastReceiver_MembersInjector(ew2Var);
    }

    public static void injectRegistry(DeepLinkingBroadcastReceiver deepLinkingBroadcastReceiver, ActionHandlerRegistry actionHandlerRegistry) {
        deepLinkingBroadcastReceiver.registry = actionHandlerRegistry;
    }

    public void injectMembers(DeepLinkingBroadcastReceiver deepLinkingBroadcastReceiver) {
        injectRegistry(deepLinkingBroadcastReceiver, this.registryProvider.get());
    }
}
