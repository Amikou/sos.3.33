package zendesk.support;

import defpackage.w83;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.TimeZone;

/* loaded from: classes3.dex */
public class ZendeskRequestService {
    private static final String LOG_TAG = "ZendeskRequestService";
    private static final String ROLE_AGENT = "agent";
    private static final String ROLE_USER = "end_user";
    private static final String TICKET_FIELDS_INCLUDE = "ticket_fields";
    private final DateFormat iso8601;
    private final w83.b<RequestResponse, Request> requestExtractor;
    private final RequestService requestService;
    private final w83.b<RequestsResponse, List<Request>> requestsExtractor;

    public ZendeskRequestService(RequestService requestService) {
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss'Z'", Locale.US);
        this.iso8601 = simpleDateFormat;
        this.requestsExtractor = new w83.b<RequestsResponse, List<Request>>() { // from class: zendesk.support.ZendeskRequestService.3
            @Override // defpackage.w83.b
            public List<Request> extract(RequestsResponse requestsResponse) {
                Map agentMap = ZendeskRequestService.getAgentMap(requestsResponse.getLastCommentingAgents());
                ArrayList arrayList = new ArrayList();
                for (Request request : requestsResponse.getRequests()) {
                    arrayList.add(ZendeskRequestService.updateLastCommentingAgents(request, agentMap));
                }
                return arrayList;
            }
        };
        this.requestExtractor = new w83.b<RequestResponse, Request>() { // from class: zendesk.support.ZendeskRequestService.4
            @Override // defpackage.w83.b
            public Request extract(RequestResponse requestResponse) {
                return ZendeskRequestService.updateLastCommentingAgents(requestResponse.getRequest(), ZendeskRequestService.getAgentMap(requestResponse.getLastCommentingAgents()));
            }
        };
        this.requestService = requestService;
        simpleDateFormat.setTimeZone(TimeZone.getTimeZone("UTC"));
    }

    /* JADX INFO: Access modifiers changed from: private */
    public static Map<Long, User> getAgentMap(List<User> list) {
        HashMap hashMap = new HashMap(list.size());
        for (User user : list) {
            hashMap.put(user.getId(), new User(user.getId(), user.getName(), user.getPhoto(), true, -1L, null, null));
        }
        return hashMap;
    }

    /* JADX INFO: Access modifiers changed from: private */
    public static Request updateLastCommentingAgents(Request request, Map<Long, User> map) {
        ArrayList arrayList = new ArrayList(request.getLastCommentingAgentsIds().size());
        for (Long l : request.getLastCommentingAgentsIds()) {
            arrayList.add(map.get(l));
        }
        request.setLastCommentingAgents(arrayList);
        return request;
    }

    public void addComment(String str, EndUserComment endUserComment, rs4<Request> rs4Var) {
        UpdateRequestWrapper updateRequestWrapper = new UpdateRequestWrapper();
        Request request = new Request();
        request.setComment(endUserComment);
        updateRequestWrapper.setRequest(request);
        this.requestService.addComment(str, updateRequestWrapper).n(new w83(rs4Var, new w83.b<RequestResponse, Request>() { // from class: zendesk.support.ZendeskRequestService.2
            @Override // defpackage.w83.b
            public Request extract(RequestResponse requestResponse) {
                return requestResponse.getRequest();
            }
        }));
    }

    public void createRequest(String str, CreateRequest createRequest, rs4<Request> rs4Var) {
        this.requestService.createRequest(str, new CreateRequestWrapper(createRequest)).n(new w83(rs4Var, new w83.b<RequestResponse, Request>() { // from class: zendesk.support.ZendeskRequestService.1
            @Override // defpackage.w83.b
            public Request extract(RequestResponse requestResponse) {
                return requestResponse.getRequest();
            }
        }));
    }

    public void getAllRequests(String str, String str2, rs4<List<Request>> rs4Var) {
        this.requestService.getAllRequests(str, str2).n(new w83(rs4Var, this.requestsExtractor));
    }

    public void getComments(String str, rs4<CommentsResponse> rs4Var) {
        this.requestService.getComments(str).n(new w83(rs4Var));
    }

    public void getCommentsSince(String str, Date date, boolean z, rs4<CommentsResponse> rs4Var) {
        this.requestService.getCommentsSince(str, this.iso8601.format(date), z ? ROLE_AGENT : null).n(new w83(rs4Var));
    }

    public void getRequest(String str, String str2, rs4<Request> rs4Var) {
        this.requestService.getRequest(str, str2).n(new w83(rs4Var, this.requestExtractor));
    }

    public void getTicketFormsById(String str, rs4<RawTicketFormResponse> rs4Var) {
        this.requestService.getTicketFormsById(str, TICKET_FIELDS_INCLUDE).n(new w83(rs4Var));
    }

    public void getAllRequests(String str, String str2, String str3, rs4<List<Request>> rs4Var) {
        this.requestService.getManyRequests(str, str2, str3).n(new w83(rs4Var, this.requestsExtractor));
    }
}
