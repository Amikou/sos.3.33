package zendesk.support;

import okhttp3.OkHttpClient;

/* loaded from: classes3.dex */
public final class GuideModule_ProvidesOkHttpClientFactory implements y11<OkHttpClient> {
    private final GuideModule module;

    public GuideModule_ProvidesOkHttpClientFactory(GuideModule guideModule) {
        this.module = guideModule;
    }

    public static GuideModule_ProvidesOkHttpClientFactory create(GuideModule guideModule) {
        return new GuideModule_ProvidesOkHttpClientFactory(guideModule);
    }

    public static OkHttpClient providesOkHttpClient(GuideModule guideModule) {
        return (OkHttpClient) cu2.f(guideModule.providesOkHttpClient());
    }

    @Override // defpackage.ew2
    public OkHttpClient get() {
        return providesOkHttpClient(this.module);
    }
}
