package zendesk.support;

import zendesk.core.RestServiceProvider;

/* loaded from: classes3.dex */
public final class ServiceModule_ProvidesRequestServiceFactory implements y11<RequestService> {
    private final ew2<RestServiceProvider> restServiceProvider;

    public ServiceModule_ProvidesRequestServiceFactory(ew2<RestServiceProvider> ew2Var) {
        this.restServiceProvider = ew2Var;
    }

    public static ServiceModule_ProvidesRequestServiceFactory create(ew2<RestServiceProvider> ew2Var) {
        return new ServiceModule_ProvidesRequestServiceFactory(ew2Var);
    }

    public static RequestService providesRequestService(RestServiceProvider restServiceProvider) {
        return (RequestService) cu2.f(ServiceModule.providesRequestService(restServiceProvider));
    }

    @Override // defpackage.ew2
    public RequestService get() {
        return providesRequestService(this.restServiceProvider.get());
    }
}
