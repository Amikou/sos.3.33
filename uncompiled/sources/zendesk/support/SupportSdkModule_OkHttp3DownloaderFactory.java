package zendesk.support;

import com.squareup.picasso.k;
import okhttp3.OkHttpClient;

/* loaded from: classes3.dex */
public final class SupportSdkModule_OkHttp3DownloaderFactory implements y11<k> {
    private final SupportSdkModule module;
    private final ew2<OkHttpClient> okHttpClientProvider;

    public SupportSdkModule_OkHttp3DownloaderFactory(SupportSdkModule supportSdkModule, ew2<OkHttpClient> ew2Var) {
        this.module = supportSdkModule;
        this.okHttpClientProvider = ew2Var;
    }

    public static SupportSdkModule_OkHttp3DownloaderFactory create(SupportSdkModule supportSdkModule, ew2<OkHttpClient> ew2Var) {
        return new SupportSdkModule_OkHttp3DownloaderFactory(supportSdkModule, ew2Var);
    }

    public static k okHttp3Downloader(SupportSdkModule supportSdkModule, OkHttpClient okHttpClient) {
        return (k) cu2.f(supportSdkModule.okHttp3Downloader(okHttpClient));
    }

    @Override // defpackage.ew2
    public k get() {
        return okHttp3Downloader(this.module, this.okHttpClientProvider.get());
    }
}
