package zendesk.support;

import retrofit2.b;

/* JADX INFO: Access modifiers changed from: package-private */
/* loaded from: classes3.dex */
public interface HelpCenterService {
    @kd0("/api/v2/help_center/votes/{vote_id}.json")
    b<Void> deleteVote(@vp2("vote_id") Long l);

    @oo2("/api/v2/help_center/articles/{article_id}/down.json")
    b<ArticleVoteResponse> downvoteArticle(@vp2("article_id") Long l, @ar String str);

    @ge1("/hc/api/mobile/{locale}/articles/{article_id}.json?respect_sanitization_settings=true")
    b<ArticleResponse> getArticle(@vp2("locale") String str, @vp2("article_id") Long l, @yw2("include") String str2);

    @ge1("/api/v2/help_center/{locale}/sections/{id}/articles.json?respect_sanitization_settings=true")
    b<ArticlesListResponse> getArticles(@vp2("locale") String str, @vp2("id") Long l, @yw2("label_names") String str2, @yw2("include") String str3, @yw2("per_page") int i);

    @ge1("/api/v2/help_center/{locale}/articles/{article_id}/attachments/{attachment_type}.json")
    b<AttachmentResponse> getAttachments(@vp2("locale") String str, @vp2("article_id") Long l, @vp2("attachment_type") String str2);

    @ge1("/api/v2/help_center/{locale}/categories.json?per_page=1000")
    b<CategoriesResponse> getCategories(@vp2("locale") String str);

    @ge1("/api/v2/help_center/{locale}/categories/{category_id}.json")
    b<CategoryResponse> getCategoryById(@vp2("locale") String str, @vp2("category_id") Long l);

    @ge1("/hc/api/mobile/{locale}/article_tree.json")
    b<HelpResponse> getHelp(@vp2("locale") String str, @yw2("category_ids") String str2, @yw2("section_ids") String str3, @yw2("include") String str4, @yw2("limit") int i, @yw2("article_labels") String str5, @yw2("per_page") int i2, @yw2("sort_by") String str6, @yw2("sort_order") String str7);

    @ge1("/api/v2/help_center/{locale}/sections/{section_id}.json")
    b<SectionResponse> getSectionById(@vp2("locale") String str, @vp2("section_id") Long l);

    @ge1("/api/v2/help_center/{locale}/categories/{id}/sections.json")
    b<SectionsResponse> getSections(@vp2("locale") String str, @vp2("id") Long l, @yw2("per_page") int i);

    @ge1("/api/mobile/help_center/search/deflect.json?respect_sanitization_settings=true")
    b<Object> getSuggestedArticles(@yw2("query") String str, @yw2("locale") String str2, @yw2("label_names") String str3, @yw2("category") Long l, @yw2("section") Long l2);

    @ge1("/api/v2/help_center/{locale}/articles.json?respect_sanitization_settings=true")
    b<ArticlesListResponse> listArticles(@vp2("locale") String str, @yw2("label_names") String str2, @yw2("include") String str3, @yw2("sort_by") String str4, @yw2("sort_order") String str5, @yw2("page") Integer num, @yw2("per_page") Integer num2);

    @ge1("/api/v2/help_center/articles/search.json?respect_sanitization_settings=true&origin=mobile_sdk")
    b<ArticlesSearchResponse> searchArticles(@yw2("query") String str, @yw2("locale") String str2, @yw2("include") String str3, @yw2("label_names") String str4, @yw2("category") String str5, @yw2("section") String str6, @yw2("page") Integer num, @yw2("per_page") Integer num2);

    @oo2("/api/v2/help_center/{locale}/articles/{article_id}/stats/view.json")
    b<Void> submitRecordArticleView(@vp2("article_id") Long l, @vp2("locale") String str, @ar RecordArticleViewRequest recordArticleViewRequest);

    @oo2("/api/v2/help_center/articles/{article_id}/up.json")
    b<ArticleVoteResponse> upvoteArticle(@vp2("article_id") Long l, @ar String str);
}
