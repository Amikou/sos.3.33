package zendesk.support;

import android.content.Context;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonElement;
import java.util.Map;
import zendesk.core.ActionDescription;
import zendesk.core.ActionHandler;
import zendesk.support.requestlist.RequestListActivity;
import zendesk.support.requestlist.RequestListConfiguration;

/* loaded from: classes3.dex */
public class RequestListActionHandler implements ActionHandler {
    private boolean conversationsEnabled;
    private final Gson gson = new GsonBuilder().create();

    @Override // zendesk.core.ActionHandler
    public boolean canHandle(String str) {
        return str.equals("action_conversation_list") && this.conversationsEnabled;
    }

    @Override // zendesk.core.ActionHandler
    public ActionDescription getActionDescription() {
        return null;
    }

    @Override // zendesk.core.ActionHandler
    public int getPriority() {
        return 0;
    }

    @Override // zendesk.core.ActionHandler
    public void handle(Map<String, Object> map, Context context) {
        RequestListActivity.builder().show(context, (RequestListConfiguration) a50.f(map, RequestListConfiguration.class));
    }

    @Override // zendesk.core.ActionHandler
    public void updateSettings(Map<String, JsonElement> map) {
        SupportSettings supportSettings = (SupportSettings) this.gson.fromJson(map == null ? null : map.get(ZendeskSupportSettingsProvider.SUPPORT_KEY), (Class<Object>) SupportSettings.class);
        if (supportSettings != null) {
            this.conversationsEnabled = supportSettings.getConversations().isEnabled();
        }
    }
}
