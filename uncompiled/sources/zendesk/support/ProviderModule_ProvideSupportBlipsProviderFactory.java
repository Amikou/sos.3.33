package zendesk.support;

import zendesk.core.BlipsProvider;

/* loaded from: classes3.dex */
public final class ProviderModule_ProvideSupportBlipsProviderFactory implements y11<SupportBlipsProvider> {
    private final ew2<BlipsProvider> blipsProvider;
    private final ProviderModule module;

    public ProviderModule_ProvideSupportBlipsProviderFactory(ProviderModule providerModule, ew2<BlipsProvider> ew2Var) {
        this.module = providerModule;
        this.blipsProvider = ew2Var;
    }

    public static ProviderModule_ProvideSupportBlipsProviderFactory create(ProviderModule providerModule, ew2<BlipsProvider> ew2Var) {
        return new ProviderModule_ProvideSupportBlipsProviderFactory(providerModule, ew2Var);
    }

    public static SupportBlipsProvider provideSupportBlipsProvider(ProviderModule providerModule, BlipsProvider blipsProvider) {
        return (SupportBlipsProvider) cu2.f(providerModule.provideSupportBlipsProvider(blipsProvider));
    }

    @Override // defpackage.ew2
    public SupportBlipsProvider get() {
        return provideSupportBlipsProvider(this.module, this.blipsProvider.get());
    }
}
