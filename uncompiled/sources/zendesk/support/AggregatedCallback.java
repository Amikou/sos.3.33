package zendesk.support;

import java.util.Collections;
import java.util.HashSet;
import java.util.Set;

/* loaded from: classes3.dex */
public final class AggregatedCallback<T> extends rs4<T> {
    private final Set<zb3<T>> callbackSet = Collections.synchronizedSet(new HashSet());

    public boolean add(rs4<T> rs4Var) {
        boolean isEmpty = this.callbackSet.isEmpty();
        this.callbackSet.add(zb3.a(rs4Var));
        return isEmpty;
    }

    public void cancel() {
        for (zb3<T> zb3Var : this.callbackSet) {
            zb3Var.cancel();
        }
        this.callbackSet.clear();
    }

    @Override // defpackage.rs4
    public void onError(cw0 cw0Var) {
        for (zb3<T> zb3Var : this.callbackSet) {
            zb3Var.onError(cw0Var);
        }
        this.callbackSet.clear();
    }

    @Override // defpackage.rs4
    public void onSuccess(T t) {
        for (zb3<T> zb3Var : this.callbackSet) {
            zb3Var.onSuccess(t);
        }
        this.callbackSet.clear();
    }
}
