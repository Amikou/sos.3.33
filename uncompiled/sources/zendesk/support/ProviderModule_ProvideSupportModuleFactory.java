package zendesk.support;

import zendesk.core.RestServiceProvider;

/* loaded from: classes3.dex */
public final class ProviderModule_ProvideSupportModuleFactory implements y11<SupportModule> {
    private final ew2<ArticleVoteStorage> articleVoteStorageProvider;
    private final ew2<SupportBlipsProvider> blipsProvider;
    private final ew2<HelpCenterProvider> helpCenterProvider;
    private final ProviderModule module;
    private final ew2<RequestProvider> requestProvider;
    private final ew2<RestServiceProvider> restServiceProvider;
    private final ew2<SupportSettingsProvider> settingsProvider;
    private final ew2<UploadProvider> uploadProvider;
    private final ew2<ZendeskTracker> zendeskTrackerProvider;

    public ProviderModule_ProvideSupportModuleFactory(ProviderModule providerModule, ew2<RequestProvider> ew2Var, ew2<UploadProvider> ew2Var2, ew2<HelpCenterProvider> ew2Var3, ew2<SupportSettingsProvider> ew2Var4, ew2<RestServiceProvider> ew2Var5, ew2<SupportBlipsProvider> ew2Var6, ew2<ZendeskTracker> ew2Var7, ew2<ArticleVoteStorage> ew2Var8) {
        this.module = providerModule;
        this.requestProvider = ew2Var;
        this.uploadProvider = ew2Var2;
        this.helpCenterProvider = ew2Var3;
        this.settingsProvider = ew2Var4;
        this.restServiceProvider = ew2Var5;
        this.blipsProvider = ew2Var6;
        this.zendeskTrackerProvider = ew2Var7;
        this.articleVoteStorageProvider = ew2Var8;
    }

    public static ProviderModule_ProvideSupportModuleFactory create(ProviderModule providerModule, ew2<RequestProvider> ew2Var, ew2<UploadProvider> ew2Var2, ew2<HelpCenterProvider> ew2Var3, ew2<SupportSettingsProvider> ew2Var4, ew2<RestServiceProvider> ew2Var5, ew2<SupportBlipsProvider> ew2Var6, ew2<ZendeskTracker> ew2Var7, ew2<ArticleVoteStorage> ew2Var8) {
        return new ProviderModule_ProvideSupportModuleFactory(providerModule, ew2Var, ew2Var2, ew2Var3, ew2Var4, ew2Var5, ew2Var6, ew2Var7, ew2Var8);
    }

    public static SupportModule provideSupportModule(ProviderModule providerModule, RequestProvider requestProvider, UploadProvider uploadProvider, HelpCenterProvider helpCenterProvider, SupportSettingsProvider supportSettingsProvider, RestServiceProvider restServiceProvider, SupportBlipsProvider supportBlipsProvider, Object obj, ArticleVoteStorage articleVoteStorage) {
        return (SupportModule) cu2.f(providerModule.provideSupportModule(requestProvider, uploadProvider, helpCenterProvider, supportSettingsProvider, restServiceProvider, supportBlipsProvider, (ZendeskTracker) obj, articleVoteStorage));
    }

    @Override // defpackage.ew2
    public SupportModule get() {
        return provideSupportModule(this.module, this.requestProvider.get(), this.uploadProvider.get(), this.helpCenterProvider.get(), this.settingsProvider.get(), this.restServiceProvider.get(), this.blipsProvider.get(), this.zendeskTrackerProvider.get(), this.articleVoteStorageProvider.get());
    }
}
