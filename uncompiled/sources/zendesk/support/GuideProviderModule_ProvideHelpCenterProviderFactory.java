package zendesk.support;

/* loaded from: classes3.dex */
public final class GuideProviderModule_ProvideHelpCenterProviderFactory implements y11<HelpCenterProvider> {
    private final ew2<HelpCenterBlipsProvider> blipsProvider;
    private final ew2<ZendeskHelpCenterService> helpCenterServiceProvider;
    private final ew2<HelpCenterSessionCache> helpCenterSessionCacheProvider;
    private final GuideProviderModule module;
    private final ew2<HelpCenterSettingsProvider> settingsProvider;

    public GuideProviderModule_ProvideHelpCenterProviderFactory(GuideProviderModule guideProviderModule, ew2<HelpCenterSettingsProvider> ew2Var, ew2<HelpCenterBlipsProvider> ew2Var2, ew2<ZendeskHelpCenterService> ew2Var3, ew2<HelpCenterSessionCache> ew2Var4) {
        this.module = guideProviderModule;
        this.settingsProvider = ew2Var;
        this.blipsProvider = ew2Var2;
        this.helpCenterServiceProvider = ew2Var3;
        this.helpCenterSessionCacheProvider = ew2Var4;
    }

    public static GuideProviderModule_ProvideHelpCenterProviderFactory create(GuideProviderModule guideProviderModule, ew2<HelpCenterSettingsProvider> ew2Var, ew2<HelpCenterBlipsProvider> ew2Var2, ew2<ZendeskHelpCenterService> ew2Var3, ew2<HelpCenterSessionCache> ew2Var4) {
        return new GuideProviderModule_ProvideHelpCenterProviderFactory(guideProviderModule, ew2Var, ew2Var2, ew2Var3, ew2Var4);
    }

    public static HelpCenterProvider provideHelpCenterProvider(GuideProviderModule guideProviderModule, HelpCenterSettingsProvider helpCenterSettingsProvider, HelpCenterBlipsProvider helpCenterBlipsProvider, Object obj, Object obj2) {
        return (HelpCenterProvider) cu2.f(guideProviderModule.provideHelpCenterProvider(helpCenterSettingsProvider, helpCenterBlipsProvider, (ZendeskHelpCenterService) obj, (HelpCenterSessionCache) obj2));
    }

    @Override // defpackage.ew2
    public HelpCenterProvider get() {
        return provideHelpCenterProvider(this.module, this.settingsProvider.get(), this.blipsProvider.get(), this.helpCenterServiceProvider.get(), this.helpCenterSessionCacheProvider.get());
    }
}
