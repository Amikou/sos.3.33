package zendesk.support;

import java.util.List;

/* loaded from: classes3.dex */
class RequestsResponse extends ResponseWrapper {
    private List<User> lastCommentingAgents;
    private List<Request> requests;

    public List<User> getLastCommentingAgents() {
        return l10.c(this.lastCommentingAgents);
    }

    public List<Request> getRequests() {
        return l10.c(this.requests);
    }
}
