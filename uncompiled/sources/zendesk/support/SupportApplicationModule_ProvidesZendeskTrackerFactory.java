package zendesk.support;

/* loaded from: classes3.dex */
public final class SupportApplicationModule_ProvidesZendeskTrackerFactory implements y11<ZendeskTracker> {
    private final SupportApplicationModule module;

    public SupportApplicationModule_ProvidesZendeskTrackerFactory(SupportApplicationModule supportApplicationModule) {
        this.module = supportApplicationModule;
    }

    public static SupportApplicationModule_ProvidesZendeskTrackerFactory create(SupportApplicationModule supportApplicationModule) {
        return new SupportApplicationModule_ProvidesZendeskTrackerFactory(supportApplicationModule);
    }

    public static ZendeskTracker providesZendeskTracker(SupportApplicationModule supportApplicationModule) {
        return (ZendeskTracker) cu2.f(supportApplicationModule.providesZendeskTracker());
    }

    @Override // defpackage.ew2
    public ZendeskTracker get() {
        return providesZendeskTracker(this.module);
    }
}
