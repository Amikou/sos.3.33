package zendesk.support;

import java.util.Locale;
import zendesk.core.SettingsProvider;
import zendesk.core.ZendeskLocaleConverter;

/* loaded from: classes3.dex */
public final class ProviderModule_ProvideSdkSettingsProviderFactory implements y11<SupportSettingsProvider> {
    private final ew2<ZendeskLocaleConverter> helpCenterLocaleConverterProvider;
    private final ew2<Locale> localeProvider;
    private final ProviderModule module;
    private final ew2<SettingsProvider> sdkSettingsProvider;

    public ProviderModule_ProvideSdkSettingsProviderFactory(ProviderModule providerModule, ew2<SettingsProvider> ew2Var, ew2<Locale> ew2Var2, ew2<ZendeskLocaleConverter> ew2Var3) {
        this.module = providerModule;
        this.sdkSettingsProvider = ew2Var;
        this.localeProvider = ew2Var2;
        this.helpCenterLocaleConverterProvider = ew2Var3;
    }

    public static ProviderModule_ProvideSdkSettingsProviderFactory create(ProviderModule providerModule, ew2<SettingsProvider> ew2Var, ew2<Locale> ew2Var2, ew2<ZendeskLocaleConverter> ew2Var3) {
        return new ProviderModule_ProvideSdkSettingsProviderFactory(providerModule, ew2Var, ew2Var2, ew2Var3);
    }

    public static SupportSettingsProvider provideSdkSettingsProvider(ProviderModule providerModule, SettingsProvider settingsProvider, Locale locale, ZendeskLocaleConverter zendeskLocaleConverter) {
        return (SupportSettingsProvider) cu2.f(providerModule.provideSdkSettingsProvider(settingsProvider, locale, zendeskLocaleConverter));
    }

    @Override // defpackage.ew2
    public SupportSettingsProvider get() {
        return provideSdkSettingsProvider(this.module, this.sdkSettingsProvider.get(), this.localeProvider.get(), this.helpCenterLocaleConverterProvider.get());
    }
}
