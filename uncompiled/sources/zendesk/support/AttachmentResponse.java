package zendesk.support;

import java.util.List;

/* loaded from: classes3.dex */
class AttachmentResponse {
    private List<HelpCenterAttachment> articleAttachments;

    public List<HelpCenterAttachment> getArticleAttachments() {
        return l10.c(this.articleAttachments);
    }
}
