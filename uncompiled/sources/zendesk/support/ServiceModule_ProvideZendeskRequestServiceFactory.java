package zendesk.support;

/* loaded from: classes3.dex */
public final class ServiceModule_ProvideZendeskRequestServiceFactory implements y11<ZendeskRequestService> {
    private final ew2<RequestService> requestServiceProvider;

    public ServiceModule_ProvideZendeskRequestServiceFactory(ew2<RequestService> ew2Var) {
        this.requestServiceProvider = ew2Var;
    }

    public static ServiceModule_ProvideZendeskRequestServiceFactory create(ew2<RequestService> ew2Var) {
        return new ServiceModule_ProvideZendeskRequestServiceFactory(ew2Var);
    }

    public static ZendeskRequestService provideZendeskRequestService(Object obj) {
        return (ZendeskRequestService) cu2.f(ServiceModule.provideZendeskRequestService((RequestService) obj));
    }

    @Override // defpackage.ew2
    public ZendeskRequestService get() {
        return provideZendeskRequestService(this.requestServiceProvider.get());
    }
}
