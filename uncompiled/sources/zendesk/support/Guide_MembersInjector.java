package zendesk.support;

/* loaded from: classes3.dex */
public final class Guide_MembersInjector implements i72<Guide> {
    private final ew2<HelpCenterBlipsProvider> blipsProvider;
    private final ew2<GuideModule> guideModuleProvider;

    public Guide_MembersInjector(ew2<GuideModule> ew2Var, ew2<HelpCenterBlipsProvider> ew2Var2) {
        this.guideModuleProvider = ew2Var;
        this.blipsProvider = ew2Var2;
    }

    public static i72<Guide> create(ew2<GuideModule> ew2Var, ew2<HelpCenterBlipsProvider> ew2Var2) {
        return new Guide_MembersInjector(ew2Var, ew2Var2);
    }

    public static void injectBlipsProvider(Guide guide, HelpCenterBlipsProvider helpCenterBlipsProvider) {
        guide.blipsProvider = helpCenterBlipsProvider;
    }

    public static void injectGuideModule(Guide guide, GuideModule guideModule) {
        guide.guideModule = guideModule;
    }

    public void injectMembers(Guide guide) {
        injectGuideModule(guide, this.guideModuleProvider.get());
        injectBlipsProvider(guide, this.blipsProvider.get());
    }
}
