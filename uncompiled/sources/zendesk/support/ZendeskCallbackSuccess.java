package zendesk.support;

/* loaded from: classes3.dex */
public abstract class ZendeskCallbackSuccess<E> extends rs4<E> {
    private final rs4 callback;

    public ZendeskCallbackSuccess(rs4 rs4Var) {
        this.callback = rs4Var;
    }

    @Override // defpackage.rs4
    public void onError(cw0 cw0Var) {
        rs4 rs4Var = this.callback;
        if (rs4Var != null) {
            rs4Var.onError(cw0Var);
        }
    }

    @Override // defpackage.rs4
    public abstract void onSuccess(E e);
}
