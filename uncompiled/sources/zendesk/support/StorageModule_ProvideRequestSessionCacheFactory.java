package zendesk.support;

/* loaded from: classes3.dex */
public final class StorageModule_ProvideRequestSessionCacheFactory implements y11<RequestSessionCache> {
    private final StorageModule module;

    public StorageModule_ProvideRequestSessionCacheFactory(StorageModule storageModule) {
        this.module = storageModule;
    }

    public static StorageModule_ProvideRequestSessionCacheFactory create(StorageModule storageModule) {
        return new StorageModule_ProvideRequestSessionCacheFactory(storageModule);
    }

    public static RequestSessionCache provideRequestSessionCache(StorageModule storageModule) {
        return (RequestSessionCache) cu2.f(storageModule.provideRequestSessionCache());
    }

    @Override // defpackage.ew2
    public RequestSessionCache get() {
        return provideRequestSessionCache(this.module);
    }
}
