package zendesk.support;

/* loaded from: classes3.dex */
public class RecordArticleViewRequest {
    private LastSearch lastSearch;
    private boolean uniqueSearchResultClick;

    public RecordArticleViewRequest(LastSearch lastSearch, boolean z) {
        this.lastSearch = lastSearch;
        this.uniqueSearchResultClick = z;
    }
}
