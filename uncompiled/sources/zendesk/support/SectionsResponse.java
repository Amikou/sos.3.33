package zendesk.support;

import java.util.List;

/* loaded from: classes3.dex */
public class SectionsResponse {
    public List<Section> sections;

    public List<Section> getSections() {
        return l10.c(this.sections);
    }
}
