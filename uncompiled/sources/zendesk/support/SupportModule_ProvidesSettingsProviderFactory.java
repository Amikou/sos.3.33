package zendesk.support;

/* loaded from: classes3.dex */
public final class SupportModule_ProvidesSettingsProviderFactory implements y11<SupportSettingsProvider> {
    private final SupportModule module;

    public SupportModule_ProvidesSettingsProviderFactory(SupportModule supportModule) {
        this.module = supportModule;
    }

    public static SupportModule_ProvidesSettingsProviderFactory create(SupportModule supportModule) {
        return new SupportModule_ProvidesSettingsProviderFactory(supportModule);
    }

    public static SupportSettingsProvider providesSettingsProvider(SupportModule supportModule) {
        return (SupportSettingsProvider) cu2.f(supportModule.providesSettingsProvider());
    }

    @Override // defpackage.ew2
    public SupportSettingsProvider get() {
        return providesSettingsProvider(this.module);
    }
}
