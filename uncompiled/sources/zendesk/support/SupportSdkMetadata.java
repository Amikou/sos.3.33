package zendesk.support;

import android.app.ActivityManager;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Build;
import com.zendesk.logger.Logger;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;

/* loaded from: classes3.dex */
public class SupportSdkMetadata {
    private static final int BAD_VALUE = -1;
    private static final long BYTES_MULTIPLIER = 1024;
    private static final String DEVICE_INFO_API_VERSION = "device_api";
    private static final String DEVICE_INFO_BATTERY = "device_battery";
    private static final String DEVICE_INFO_DEVICE_NAME = "device_name";
    private static final String DEVICE_INFO_LOW_MEMORY = "device_low_memory";
    private static final String DEVICE_INFO_MANUFACTURER = "device_manufacturer";
    private static final String DEVICE_INFO_MODEL_TYPE = "device_model";
    private static final String DEVICE_INFO_OS_VERSION = "device_os";
    private static final String DEVICE_INFO_TOTAL_MEMORY = "device_total_memory";
    private static final String DEVICE_INFO_USED_MEMORY = "device_used_memory";
    private static final int EXPECTED_TOKEN_COUNT = 3;
    private static final String LOG_TAG = "SupportSdkMetadata";
    private final ActivityManager activityManager;
    private final Context context;

    public SupportSdkMetadata(Context context) {
        this.context = context;
        this.activityManager = (ActivityManager) context.getSystemService("activity");
    }

    private int getBatteryLevel() {
        Intent registerReceiver = this.context.getApplicationContext().registerReceiver(null, new IntentFilter("android.intent.action.BATTERY_CHANGED"));
        if (registerReceiver != null) {
            return registerReceiver.getIntExtra("level", -1);
        }
        return -1;
    }

    private String getBytesInMb(long j) {
        return String.valueOf(j / 1048576);
    }

    private String getManufacturer() {
        String str = Build.MANUFACTURER;
        return "unknown".equals(str) || ru3.d(str) ? "" : str;
    }

    private String getModel() {
        String str = Build.MODEL;
        boolean z = "unknown".equals(str) || ru3.d(str);
        String str2 = Build.DEVICE;
        return (z && ("unknown".equals(str2) || ru3.d(str2))) ? "" : str.equals(str2) ? str : String.format(Locale.US, "%s/%s", str, str2);
    }

    private String getModelDeviceName() {
        return Build.DEVICE;
    }

    private long getTotalMemory() {
        Logger.b(LOG_TAG, "Using getTotalMemoryApi() to determine memory", new Object[0]);
        return getTotalMemoryApi();
    }

    private long getTotalMemoryApi() {
        ActivityManager.MemoryInfo memoryInfo = new ActivityManager.MemoryInfo();
        this.activityManager.getMemoryInfo(memoryInfo);
        return memoryInfo.totalMem;
    }

    /* JADX WARN: Removed duplicated region for block: B:25:0x0089 A[Catch: NumberFormatException -> 0x009d, NoSuchElementException -> 0x00a6, TRY_LEAVE, TryCatch #8 {NumberFormatException -> 0x009d, NoSuchElementException -> 0x00a6, blocks: (B:23:0x0082, B:25:0x0089), top: B:50:0x0082 }] */
    /* JADX WARN: Removed duplicated region for block: B:41:0x00b2 A[EXC_TOP_SPLITTER, SYNTHETIC] */
    /* JADX WARN: Removed duplicated region for block: B:52:? A[RETURN, SYNTHETIC] */
    /*
        Code decompiled incorrectly, please refer to instructions dump.
        To view partially-correct code enable 'Show inconsistent code' option in preferences
    */
    private long getTotalMemoryCompat() {
        /*
            r8 = this;
            java.lang.String r0 = "Failed to close /proc/meminfo file stream: "
            java.lang.String r1 = "SupportSdkMetadata"
            r2 = 0
            r3 = 0
            java.io.BufferedReader r4 = new java.io.BufferedReader     // Catch: java.lang.Throwable -> L36 java.io.IOException -> L3c
            java.io.FileReader r5 = new java.io.FileReader     // Catch: java.lang.Throwable -> L36 java.io.IOException -> L3c
            java.lang.String r6 = "/proc/meminfo"
            r5.<init>(r6)     // Catch: java.lang.Throwable -> L36 java.io.IOException -> L3c
            r4.<init>(r5)     // Catch: java.lang.Throwable -> L36 java.io.IOException -> L3c
            java.lang.String r3 = r4.readLine()     // Catch: java.io.IOException -> L34 java.lang.Throwable -> Laf
            r4.close()     // Catch: java.io.IOException -> L1a
            goto L7b
        L1a:
            r4 = move-exception
            java.lang.StringBuilder r5 = new java.lang.StringBuilder
            r5.<init>()
            r5.append(r0)
            java.lang.String r0 = r4.getMessage()
            r5.append(r0)
            java.lang.String r0 = r5.toString()
            java.lang.Object[] r5 = new java.lang.Object[r2]
            com.zendesk.logger.Logger.j(r1, r0, r4, r5)
            goto L7b
        L34:
            r3 = move-exception
            goto L40
        L36:
            r4 = move-exception
            r7 = r4
            r4 = r3
            r3 = r7
            goto Lb0
        L3c:
            r4 = move-exception
            r7 = r4
            r4 = r3
            r3 = r7
        L40:
            java.lang.StringBuilder r5 = new java.lang.StringBuilder     // Catch: java.lang.Throwable -> Laf
            r5.<init>()     // Catch: java.lang.Throwable -> Laf
            java.lang.String r6 = "Failed to determine total memory from /proc/meminfo: "
            r5.append(r6)     // Catch: java.lang.Throwable -> Laf
            java.lang.String r6 = r3.getMessage()     // Catch: java.lang.Throwable -> Laf
            r5.append(r6)     // Catch: java.lang.Throwable -> Laf
            java.lang.String r5 = r5.toString()     // Catch: java.lang.Throwable -> Laf
            java.lang.Object[] r6 = new java.lang.Object[r2]     // Catch: java.lang.Throwable -> Laf
            com.zendesk.logger.Logger.d(r1, r5, r3, r6)     // Catch: java.lang.Throwable -> Laf
            if (r4 == 0) goto L79
            r4.close()     // Catch: java.io.IOException -> L60
            goto L79
        L60:
            r3 = move-exception
            java.lang.StringBuilder r4 = new java.lang.StringBuilder
            r4.<init>()
            r4.append(r0)
            java.lang.String r0 = r3.getMessage()
            r4.append(r0)
            java.lang.String r0 = r4.toString()
            java.lang.Object[] r4 = new java.lang.Object[r2]
            com.zendesk.logger.Logger.j(r1, r0, r3, r4)
        L79:
            java.lang.String r3 = ""
        L7b:
            java.util.StringTokenizer r0 = new java.util.StringTokenizer
            r0.<init>(r3)
            r3 = -1
            int r5 = r0.countTokens()     // Catch: java.lang.NumberFormatException -> L9d java.util.NoSuchElementException -> La6
            r6 = 3
            if (r5 != r6) goto Lae
            r0.nextToken()     // Catch: java.lang.NumberFormatException -> L9d java.util.NoSuchElementException -> La6
            java.lang.String r0 = r0.nextToken()     // Catch: java.lang.NumberFormatException -> L9d java.util.NoSuchElementException -> La6
            java.lang.Long r0 = java.lang.Long.valueOf(r0)     // Catch: java.lang.NumberFormatException -> L9d java.util.NoSuchElementException -> La6
            long r0 = r0.longValue()     // Catch: java.lang.NumberFormatException -> L9d java.util.NoSuchElementException -> La6
            r2 = 1024(0x400, double:5.06E-321)
            long r0 = r0 * r2
            r3 = r0
            goto Lae
        L9d:
            r0 = move-exception
            java.lang.Object[] r2 = new java.lang.Object[r2]
            java.lang.String r5 = "Error reading memory size from proc/meminfo"
            com.zendesk.logger.Logger.d(r1, r5, r0, r2)
            goto Lae
        La6:
            r0 = move-exception
            java.lang.Object[] r2 = new java.lang.Object[r2]
            java.lang.String r5 = "Error reading tokens from the /proc/meminfo"
            com.zendesk.logger.Logger.d(r1, r5, r0, r2)
        Lae:
            return r3
        Laf:
            r3 = move-exception
        Lb0:
            if (r4 == 0) goto Lcf
            r4.close()     // Catch: java.io.IOException -> Lb6
            goto Lcf
        Lb6:
            r4 = move-exception
            java.lang.StringBuilder r5 = new java.lang.StringBuilder
            r5.<init>()
            r5.append(r0)
            java.lang.String r0 = r4.getMessage()
            r5.append(r0)
            java.lang.String r0 = r5.toString()
            java.lang.Object[] r2 = new java.lang.Object[r2]
            com.zendesk.logger.Logger.j(r1, r0, r4, r2)
        Lcf:
            throw r3
        */
        throw new UnsupportedOperationException("Method not decompiled: zendesk.support.SupportSdkMetadata.getTotalMemoryCompat():long");
    }

    private long getUsedMemory() {
        ActivityManager.MemoryInfo memoryInfo = new ActivityManager.MemoryInfo();
        this.activityManager.getMemoryInfo(memoryInfo);
        return getTotalMemory() - memoryInfo.availMem;
    }

    private int getVersionCode() {
        return Build.VERSION.SDK_INT;
    }

    private String getVersionName() {
        return Build.VERSION.RELEASE;
    }

    private boolean isLowMemory() {
        ActivityManager.MemoryInfo memoryInfo = new ActivityManager.MemoryInfo();
        this.activityManager.getMemoryInfo(memoryInfo);
        return memoryInfo.lowMemory;
    }

    public Map<String, String> getDeviceInfoAsMapForMetaData() {
        HashMap hashMap = new HashMap();
        hashMap.put(DEVICE_INFO_OS_VERSION, getVersionName());
        hashMap.put(DEVICE_INFO_API_VERSION, String.valueOf(getVersionCode()));
        hashMap.put(DEVICE_INFO_MODEL_TYPE, getModel());
        hashMap.put(DEVICE_INFO_DEVICE_NAME, getModelDeviceName());
        hashMap.put(DEVICE_INFO_MANUFACTURER, getManufacturer());
        hashMap.put(DEVICE_INFO_TOTAL_MEMORY, getBytesInMb(getTotalMemory()));
        hashMap.put(DEVICE_INFO_USED_MEMORY, getBytesInMb(getUsedMemory()));
        hashMap.put(DEVICE_INFO_LOW_MEMORY, String.valueOf(isLowMemory()));
        hashMap.put(DEVICE_INFO_BATTERY, String.valueOf(getBatteryLevel()));
        return hashMap;
    }

    public String getLocale() {
        return h12.d(Locale.getDefault());
    }
}
