package zendesk.support;

import com.google.gson.Gson;
import com.zendesk.logger.Logger;
import defpackage.ep0;
import java.io.IOException;
import java.lang.reflect.Type;
import okio.k;
import zendesk.support.Streams;

/* loaded from: classes3.dex */
public class SupportUiStorage {
    private static final int CACHE_INDEX = 0;
    private static final String LOG_TAG = "SupportUiStorage";
    public static final String REQUEST_MAPPER = "request_id_mapper";
    private final Gson gson;
    private final ep0 storage;

    public SupportUiStorage(ep0 ep0Var, Gson gson) {
        this.storage = ep0Var;
        this.gson = gson;
    }

    private static void abortEdit(ep0.c cVar) {
        Logger.k(LOG_TAG, "Unable to cache data", new Object[0]);
        if (cVar != null) {
            try {
                cVar.a();
            } catch (IOException e) {
                Logger.j(LOG_TAG, "Unable to abort write", e, new Object[0]);
            }
        }
    }

    private static String key(String str) {
        return to0.c(str);
    }

    public <E> E read(String str, final Type type) {
        E e;
        try {
            synchronized (this.storage) {
                e = (E) Streams.use(this.storage.u(key(str)), new Streams.Use<E, ep0.e>() { // from class: zendesk.support.SupportUiStorage.1
                    /* JADX WARN: Type inference failed for: r3v4, types: [E, java.lang.Object] */
                    @Override // zendesk.support.Streams.Use
                    public E use(ep0.e eVar) throws Exception {
                        return SupportUiStorage.this.gson.fromJson(Streams.toReader(k.l(eVar.a(0))), type);
                    }
                });
            }
            return e;
        } catch (IOException unused) {
            Logger.k(LOG_TAG, "Unable to read from cache", new Object[0]);
            return null;
        }
    }

    public void write(String str, Object obj) {
        ep0.c cVar = null;
        try {
            synchronized (this.storage) {
                cVar = this.storage.q(key(str));
            }
            if (cVar != null) {
                Streams.toJson(this.gson, k.h(cVar.f(0)), obj);
                cVar.e();
            }
        } catch (IOException unused) {
            abortEdit(cVar);
        }
    }
}
