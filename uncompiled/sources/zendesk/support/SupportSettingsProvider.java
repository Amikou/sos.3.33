package zendesk.support;

/* loaded from: classes3.dex */
public interface SupportSettingsProvider {
    void getSettings(rs4<SupportSdkSettings> rs4Var);
}
