package zendesk.support.request;

import com.zendesk.logger.Logger;
import zendesk.support.Request;
import zendesk.support.RequestProvider;
import zendesk.support.request.AsyncMiddleware;
import zendesk.support.suas.Dispatcher;
import zendesk.support.suas.GetState;

/* JADX INFO: Access modifiers changed from: package-private */
/* loaded from: classes3.dex */
public class ActionLoadRequest implements AsyncMiddleware.AsyncAction {
    private final ActionFactory af;
    private final RequestProvider requestProvider;

    public ActionLoadRequest(ActionFactory actionFactory, RequestProvider requestProvider) {
        this.af = actionFactory;
        this.requestProvider = requestProvider;
    }

    @Override // zendesk.support.request.AsyncMiddleware.AsyncAction
    public void actionQueued(Dispatcher dispatcher, GetState getState) {
        dispatcher.dispatch(this.af.loadRequest());
    }

    @Override // zendesk.support.request.AsyncMiddleware.AsyncAction
    public void execute(final Dispatcher dispatcher, GetState getState, final AsyncMiddleware.Callback callback) {
        StateConversation fromState = StateConversation.fromState(getState.getState());
        String remoteId = fromState.getRemoteId();
        if (!ru3.b(remoteId)) {
            Logger.b(RequestActivity.LOG_TAG, "Skip loading request. No remote id found.", new Object[0]);
            dispatcher.dispatch(this.af.skipAction());
            callback.done();
        } else if (fromState.getStatus() != null) {
            Logger.b(RequestActivity.LOG_TAG, "Skip loading request. Request status already available.", new Object[0]);
            dispatcher.dispatch(this.af.skipAction());
            callback.done();
        } else {
            this.requestProvider.getRequest(remoteId, new rs4<Request>() { // from class: zendesk.support.request.ActionLoadRequest.1
                @Override // defpackage.rs4
                public void onError(cw0 cw0Var) {
                    Logger.k(RequestActivity.LOG_TAG, "Error loading request. Error: '%s'", cw0Var.h());
                    dispatcher.dispatch(ActionLoadRequest.this.af.loadRequestError(cw0Var));
                    callback.done();
                }

                @Override // defpackage.rs4
                public void onSuccess(Request request) {
                    dispatcher.dispatch(ActionLoadRequest.this.af.loadRequestSuccess(request));
                    callback.done();
                }
            });
        }
    }
}
