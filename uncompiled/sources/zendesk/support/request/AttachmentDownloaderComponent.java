package zendesk.support.request;

import com.zendesk.logger.Logger;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;
import okhttp3.ResponseBody;
import zendesk.belvedere.MediaResult;
import zendesk.belvedere.a;
import zendesk.support.suas.Dispatcher;
import zendesk.support.suas.Listener;

/* JADX INFO: Access modifiers changed from: package-private */
/* loaded from: classes3.dex */
public class AttachmentDownloaderComponent implements Listener<StateConversation> {
    private final ActionFactory actionFactory;
    private final AttachmentDownloader attachmentDownloader;
    private final Dispatcher dispatcher;
    private final AttachmentDownloaderSelector selector = new AttachmentDownloaderSelector();

    /* loaded from: classes3.dex */
    public static class AttachmentDownloader {
        private final AttachmentDownloadService attachmentIo;
        private final a belvedere;
        private final Set<String> downloadingHistory = new HashSet();

        /* loaded from: classes3.dex */
        public class CacheCallback extends rs4<MediaResult> {
            private final rs4<MediaResult> callback;
            private final StateRequestAttachment requestAttachment;

            public CacheCallback(StateRequestAttachment stateRequestAttachment, rs4<MediaResult> rs4Var) {
                this.requestAttachment = stateRequestAttachment;
                this.callback = rs4Var;
            }

            @Override // defpackage.rs4
            public void onError(cw0 cw0Var) {
                AttachmentDownloader.this.handleError(this.requestAttachment.getUrl(), cw0Var, this.callback);
            }

            @Override // defpackage.rs4
            public void onSuccess(MediaResult mediaResult) {
                this.callback.onSuccess(mediaResult);
                AttachmentDownloader.this.downloadingHistory.remove(this.requestAttachment.getUrl());
            }
        }

        /* loaded from: classes3.dex */
        public class HttpCallback extends rs4<ResponseBody> {
            private final rs4<MediaResult> callback;
            private final Request request;
            private final StateRequestAttachment requestAttachment;

            public HttpCallback(Request request, StateRequestAttachment stateRequestAttachment, rs4<MediaResult> rs4Var) {
                this.request = request;
                this.requestAttachment = stateRequestAttachment;
                this.callback = rs4Var;
            }

            @Override // defpackage.rs4
            public void onError(cw0 cw0Var) {
                AttachmentDownloader.this.handleError(this.requestAttachment.getUrl(), cw0Var, this.callback);
            }

            @Override // defpackage.rs4
            public void onSuccess(ResponseBody responseBody) {
                AttachmentDownloader.this.attachmentIo.storeAttachment(responseBody, UtilsAttachment.getLocalFile(AttachmentDownloader.this.belvedere, this.request.getRequestId(), this.request.getRemoteAttachmentId(), this.requestAttachment.getName()), new CacheCallback(this.requestAttachment, this.callback));
            }
        }

        /* loaded from: classes3.dex */
        public static class Request {
            private final long remoteAttachmentId;
            private final StateRequestAttachment requestAttachment;
            private final String requestId;

            public Request(String str, long j, StateRequestAttachment stateRequestAttachment) {
                this.requestId = str;
                this.remoteAttachmentId = j;
                this.requestAttachment = stateRequestAttachment;
            }

            public long getRemoteAttachmentId() {
                return this.remoteAttachmentId;
            }

            public StateRequestAttachment getRequestAttachment() {
                return this.requestAttachment;
            }

            public String getRequestId() {
                return this.requestId;
            }
        }

        public AttachmentDownloader(a aVar, AttachmentDownloadService attachmentDownloadService) {
            this.belvedere = aVar;
            this.attachmentIo = attachmentDownloadService;
        }

        /* JADX INFO: Access modifiers changed from: private */
        public void handleError(String str, cw0 cw0Var, rs4 rs4Var) {
            this.downloadingHistory.remove(str);
            if (rs4Var != null) {
                rs4Var.onError(cw0Var);
            }
        }

        public void download(Request request, rs4<MediaResult> rs4Var) {
            StateRequestAttachment requestAttachment = request.getRequestAttachment();
            String url = requestAttachment.getUrl();
            if (this.downloadingHistory.contains(url)) {
                return;
            }
            this.downloadingHistory.add(url);
            this.attachmentIo.downloadAttachment(url, new HttpCallback(request, requestAttachment, rs4Var));
        }
    }

    /* loaded from: classes3.dex */
    public static class AttachmentDownloaderSelector {
        public List<AttachmentDownloader.Request> selectData(StateConversation stateConversation) {
            StateIdMapper attachmentIdMapper = stateConversation.getAttachmentIdMapper();
            String localId = stateConversation.getLocalId();
            List<StateMessage> messages = stateConversation.getMessages();
            LinkedList linkedList = new LinkedList();
            for (StateMessage stateMessage : messages) {
                for (StateRequestAttachment stateRequestAttachment : stateMessage.getAttachments()) {
                    long id = stateRequestAttachment.getId();
                    boolean z = stateRequestAttachment.getLocalFile() != null;
                    boolean hasRemoteId = attachmentIdMapper.hasRemoteId(Long.valueOf(id));
                    boolean b = ru3.b(stateRequestAttachment.getUrl());
                    if (!z && hasRemoteId && b) {
                        linkedList.add(new AttachmentDownloader.Request(localId, attachmentIdMapper.getRemoteId(Long.valueOf(id)).longValue(), stateRequestAttachment));
                    }
                }
            }
            return linkedList;
        }
    }

    /* loaded from: classes3.dex */
    public class DownloadCallback extends rs4<MediaResult> {
        private final StateRequestAttachment requestAttachment;

        public DownloadCallback(StateRequestAttachment stateRequestAttachment) {
            this.requestAttachment = stateRequestAttachment;
        }

        @Override // defpackage.rs4
        public void onError(cw0 cw0Var) {
            Logger.b(RequestActivity.LOG_TAG, "Unable to download attachment. Error: %s", cw0Var.h());
        }

        @Override // defpackage.rs4
        public void onSuccess(MediaResult mediaResult) {
            AttachmentDownloaderComponent.this.dispatcher.dispatch(AttachmentDownloaderComponent.this.actionFactory.attachmentDownloaded(this.requestAttachment, mediaResult));
        }
    }

    public AttachmentDownloaderComponent(Dispatcher dispatcher, ActionFactory actionFactory, AttachmentDownloader attachmentDownloader) {
        this.dispatcher = dispatcher;
        this.actionFactory = actionFactory;
        this.attachmentDownloader = attachmentDownloader;
    }

    @Override // zendesk.support.suas.Listener
    public void update(StateConversation stateConversation) {
        for (AttachmentDownloader.Request request : this.selector.selectData(stateConversation)) {
            this.attachmentDownloader.download(request, new DownloadCallback(request.getRequestAttachment()));
        }
    }
}
