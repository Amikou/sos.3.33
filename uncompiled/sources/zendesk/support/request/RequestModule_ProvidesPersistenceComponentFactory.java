package zendesk.support.request;

import java.util.concurrent.ExecutorService;
import zendesk.support.SupportUiStorage;
import zendesk.support.request.ComponentPersistence;

/* loaded from: classes3.dex */
public final class RequestModule_ProvidesPersistenceComponentFactory implements y11<ComponentPersistence> {
    private final ew2<ExecutorService> executorServiceProvider;
    private final ew2<ComponentPersistence.PersistenceQueue> queueProvider;
    private final ew2<SupportUiStorage> supportUiStorageProvider;

    public RequestModule_ProvidesPersistenceComponentFactory(ew2<SupportUiStorage> ew2Var, ew2<ComponentPersistence.PersistenceQueue> ew2Var2, ew2<ExecutorService> ew2Var3) {
        this.supportUiStorageProvider = ew2Var;
        this.queueProvider = ew2Var2;
        this.executorServiceProvider = ew2Var3;
    }

    public static RequestModule_ProvidesPersistenceComponentFactory create(ew2<SupportUiStorage> ew2Var, ew2<ComponentPersistence.PersistenceQueue> ew2Var2, ew2<ExecutorService> ew2Var3) {
        return new RequestModule_ProvidesPersistenceComponentFactory(ew2Var, ew2Var2, ew2Var3);
    }

    public static ComponentPersistence providesPersistenceComponent(SupportUiStorage supportUiStorage, Object obj, ExecutorService executorService) {
        return (ComponentPersistence) cu2.f(RequestModule.providesPersistenceComponent(supportUiStorage, (ComponentPersistence.PersistenceQueue) obj, executorService));
    }

    @Override // defpackage.ew2
    public ComponentPersistence get() {
        return providesPersistenceComponent(this.supportUiStorageProvider.get(), this.queueProvider.get(), this.executorServiceProvider.get());
    }
}
