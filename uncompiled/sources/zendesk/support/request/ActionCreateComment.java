package zendesk.support.request;

import com.zendesk.logger.Logger;
import java.util.ArrayList;
import java.util.List;
import zendesk.support.Comment;
import zendesk.support.CreateRequest;
import zendesk.support.EndUserComment;
import zendesk.support.Request;
import zendesk.support.RequestProvider;
import zendesk.support.request.AsyncMiddleware;
import zendesk.support.request.AttachmentUploadService;
import zendesk.support.suas.Dispatcher;
import zendesk.support.suas.GetState;

/* JADX INFO: Access modifiers changed from: package-private */
/* loaded from: classes3.dex */
public class ActionCreateComment implements AsyncMiddleware.AsyncAction {
    private final ActionFactory af;
    private rs4<AttachmentUploadService.AttachmentUploadResult> attachmentCallback;
    private final AttachmentUploadService attachmentUploader;
    private final StateMessage message;
    private final RequestProvider requestProvider;

    /* loaded from: classes3.dex */
    public static class CreateCommentResult {
        private final long commentRemoteId;
        private final AttachmentUploadService.AttachmentUploadResult localToRemoteAttachments;
        private final StateMessage message;
        private final String requestId;

        public CreateCommentResult(StateMessage stateMessage, String str, long j, AttachmentUploadService.AttachmentUploadResult attachmentUploadResult) {
            this.message = stateMessage;
            this.requestId = str;
            this.commentRemoteId = j;
            this.localToRemoteAttachments = attachmentUploadResult;
        }

        public long getCommentRemoteId() {
            return this.commentRemoteId;
        }

        public AttachmentUploadService.AttachmentUploadResult getLocalToRemoteAttachments() {
            return this.localToRemoteAttachments;
        }

        public StateMessage getMessage() {
            return this.message;
        }

        public String getRequestId() {
            return this.requestId;
        }
    }

    public ActionCreateComment(ActionFactory actionFactory, RequestProvider requestProvider, AttachmentUploadService attachmentUploadService, StateMessage stateMessage) {
        this.af = actionFactory;
        this.requestProvider = requestProvider;
        this.message = stateMessage;
        this.attachmentUploader = attachmentUploadService;
    }

    private void addComment(final AsyncMiddleware.Callback callback, final Dispatcher dispatcher, final StateConversation stateConversation, final AttachmentUploadService.AttachmentUploadResult attachmentUploadResult) {
        EndUserComment endUserComment = new EndUserComment();
        endUserComment.setValue(this.message.getBody());
        endUserComment.setAttachments(getAttachmentToken(attachmentUploadResult.getRequestAttachments()));
        final String remoteId = stateConversation.getRemoteId();
        this.requestProvider.addComment(remoteId, endUserComment, new rs4<Comment>() { // from class: zendesk.support.request.ActionCreateComment.2
            @Override // defpackage.rs4
            public void onError(cw0 cw0Var) {
                Logger.k(RequestActivity.LOG_TAG, "Unable to add comment to request. Error: '%s'. Message Id: %d", cw0Var.h(), Long.valueOf(ActionCreateComment.this.message.getId()));
                if (cw0Var.i() && cw0Var.e() == 422) {
                    Logger.k(RequestActivity.LOG_TAG, "This request (%s) is closed. Error: '%s'. Message Id: %d", remoteId, cw0Var.h(), Long.valueOf(ActionCreateComment.this.message.getId()));
                    dispatcher.dispatch(ActionCreateComment.this.af.requestClosed());
                }
                dispatcher.dispatch(ActionCreateComment.this.af.createCommentError(cw0Var, ActionCreateComment.this.message.withError(cw0Var)));
                callback.done();
            }

            @Override // defpackage.rs4
            public void onSuccess(Comment comment) {
                dispatcher.dispatch(ActionCreateComment.this.af.createCommentSuccess(new CreateCommentResult(ActionCreateComment.this.message.withAttachments(attachmentUploadResult.getRequestAttachments()).withDelivered(), remoteId, comment.getId().longValue(), attachmentUploadResult)));
                ActionCreateComment.this.requestProvider.markRequestAsRead(remoteId, stateConversation.getMessageIdMapper().getRemoteIds().size());
                callback.done();
            }
        });
    }

    /* JADX INFO: Access modifiers changed from: private */
    public void createMessage(StateConversation stateConversation, StateConfig stateConfig, Dispatcher dispatcher, AsyncMiddleware.Callback callback, AttachmentUploadService.AttachmentUploadResult attachmentUploadResult) {
        if (ru3.b(stateConversation.getRemoteId())) {
            Logger.b(RequestActivity.LOG_TAG, "Adding a comment to an existing request. Message Id %s", Long.valueOf(this.message.getId()));
            addComment(callback, dispatcher, stateConversation, attachmentUploadResult);
            return;
        }
        Logger.b(RequestActivity.LOG_TAG, "Creating a new request. Message Id %s", Long.valueOf(this.message.getId()));
        createRequest(callback, dispatcher, attachmentUploadResult, stateConfig);
    }

    private void createRequest(final AsyncMiddleware.Callback callback, final Dispatcher dispatcher, final AttachmentUploadService.AttachmentUploadResult attachmentUploadResult, StateConfig stateConfig) {
        CreateRequest createRequest = new CreateRequest();
        createRequest.setDescription(this.message.getBody());
        createRequest.setAttachments(getAttachmentToken(attachmentUploadResult.getRequestAttachments()));
        if (l10.i(stateConfig.getTags())) {
            createRequest.setTags(stateConfig.getTags());
        }
        if (ru3.b(stateConfig.getSubject())) {
            createRequest.setSubject(stateConfig.getSubject());
        }
        if (stateConfig.getTicketForm() != null) {
            if (stateConfig.getTicketForm().getId() != -1) {
                createRequest.setTicketFormId(Long.valueOf(stateConfig.getTicketForm().getId()));
            }
            createRequest.setCustomFields(stateConfig.getTicketForm().getTicketFieldsForApi());
        }
        this.requestProvider.createRequest(createRequest, new rs4<Request>() { // from class: zendesk.support.request.ActionCreateComment.3
            @Override // defpackage.rs4
            public void onError(cw0 cw0Var) {
                dispatcher.dispatch(ActionCreateComment.this.af.createRequestError(cw0Var, ActionCreateComment.this.message.withError(cw0Var)));
                callback.done();
            }

            @Override // defpackage.rs4
            public void onSuccess(Request request) {
                dispatcher.dispatch(ActionCreateComment.this.af.createRequestSuccess(new CreateCommentResult(ActionCreateComment.this.message.withAttachments(attachmentUploadResult.getRequestAttachments()).withDelivered(), request.getId(), request.getLastComment().getId().longValue(), attachmentUploadResult)));
                callback.done();
                if (request.getId() != null) {
                    ActionCreateComment.this.requestProvider.markRequestAsRead(request.getId(), 1);
                }
            }
        });
    }

    private List<String> getAttachmentToken(List<StateRequestAttachment> list) {
        ArrayList arrayList = new ArrayList();
        for (StateRequestAttachment stateRequestAttachment : list) {
            arrayList.add(stateRequestAttachment.getToken());
        }
        return arrayList;
    }

    private void waitForAttachments(final Dispatcher dispatcher, GetState getState, final AsyncMiddleware.Callback callback) {
        final StateConversation fromState = StateConversation.fromState(getState.getState());
        final StateConfig fromState2 = StateConfig.fromState(getState.getState());
        Logger.b(RequestActivity.LOG_TAG, "Waiting for attachments to be uploaded. Message Id: %s", Long.valueOf(this.message.getId()));
        rs4<AttachmentUploadService.AttachmentUploadResult> rs4Var = new rs4<AttachmentUploadService.AttachmentUploadResult>() { // from class: zendesk.support.request.ActionCreateComment.1
            @Override // defpackage.rs4
            public void onError(cw0 cw0Var) {
                Logger.k(RequestActivity.LOG_TAG, "Attachment upload error. '%s'. Message Id: %s", cw0Var.h(), Long.valueOf(ActionCreateComment.this.message.getId()));
                StateMessage withError = ActionCreateComment.this.message.withError(cw0Var);
                if (ru3.b(fromState.getRemoteId())) {
                    dispatcher.dispatch(ActionCreateComment.this.af.createCommentError(cw0Var, withError));
                } else {
                    dispatcher.dispatch(ActionCreateComment.this.af.createRequestError(cw0Var, withError));
                }
                callback.done();
            }

            @Override // defpackage.rs4
            public void onSuccess(AttachmentUploadService.AttachmentUploadResult attachmentUploadResult) {
                Logger.b(RequestActivity.LOG_TAG, "Attachments resolved and uploaded. Message Id: %s", Long.valueOf(ActionCreateComment.this.message.getId()));
                ActionCreateComment.this.createMessage(fromState, fromState2, dispatcher, callback, attachmentUploadResult);
            }
        };
        this.attachmentCallback = rs4Var;
        this.attachmentUploader.setResultListener(rs4Var);
    }

    @Override // zendesk.support.request.AsyncMiddleware.AsyncAction
    public void actionQueued(Dispatcher dispatcher, GetState getState) {
        List<StateRequestAttachment> attachments = this.message.getAttachments();
        String localId = StateConversation.fromState(getState.getState()).getLocalId();
        if (l10.i(attachments)) {
            Logger.b(RequestActivity.LOG_TAG, "Start uploading %d attachments. Message Id: %s", Integer.valueOf(attachments.size()), Long.valueOf(this.message.getId()));
            this.attachmentUploader.start(localId);
        }
        dispatcher.dispatch(this.af.createComment(this.message.withPending()));
    }

    @Override // zendesk.support.request.AsyncMiddleware.AsyncAction
    public void execute(Dispatcher dispatcher, GetState getState, AsyncMiddleware.Callback callback) {
        waitForAttachments(dispatcher, getState, callback);
    }
}
