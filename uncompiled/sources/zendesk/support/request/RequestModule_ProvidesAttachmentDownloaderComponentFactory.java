package zendesk.support.request;

import zendesk.support.request.AttachmentDownloaderComponent;
import zendesk.support.suas.Dispatcher;

/* loaded from: classes3.dex */
public final class RequestModule_ProvidesAttachmentDownloaderComponentFactory implements y11<AttachmentDownloaderComponent> {
    private final ew2<ActionFactory> actionFactoryProvider;
    private final ew2<AttachmentDownloaderComponent.AttachmentDownloader> attachmentDownloaderProvider;
    private final ew2<Dispatcher> dispatcherProvider;

    public RequestModule_ProvidesAttachmentDownloaderComponentFactory(ew2<Dispatcher> ew2Var, ew2<ActionFactory> ew2Var2, ew2<AttachmentDownloaderComponent.AttachmentDownloader> ew2Var3) {
        this.dispatcherProvider = ew2Var;
        this.actionFactoryProvider = ew2Var2;
        this.attachmentDownloaderProvider = ew2Var3;
    }

    public static RequestModule_ProvidesAttachmentDownloaderComponentFactory create(ew2<Dispatcher> ew2Var, ew2<ActionFactory> ew2Var2, ew2<AttachmentDownloaderComponent.AttachmentDownloader> ew2Var3) {
        return new RequestModule_ProvidesAttachmentDownloaderComponentFactory(ew2Var, ew2Var2, ew2Var3);
    }

    public static AttachmentDownloaderComponent providesAttachmentDownloaderComponent(Dispatcher dispatcher, Object obj, Object obj2) {
        return (AttachmentDownloaderComponent) cu2.f(RequestModule.providesAttachmentDownloaderComponent(dispatcher, (ActionFactory) obj, (AttachmentDownloaderComponent.AttachmentDownloader) obj2));
    }

    @Override // defpackage.ew2
    public AttachmentDownloaderComponent get() {
        return providesAttachmentDownloaderComponent(this.dispatcherProvider.get(), this.actionFactoryProvider.get(), this.attachmentDownloaderProvider.get());
    }
}
