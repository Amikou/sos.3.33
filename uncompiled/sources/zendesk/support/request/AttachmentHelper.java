package zendesk.support.request;

import androidx.appcompat.app.AppCompatActivity;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import zendesk.belvedere.BelvedereUi;
import zendesk.belvedere.MediaResult;

/* loaded from: classes3.dex */
class AttachmentHelper {
    private final int[] touchableItems;
    private long maxFileSize = -1;
    private List<StateRequestAttachment> selectedAttachments = new ArrayList();
    private List<StateRequestAttachment> additionalAttachments = new ArrayList();

    public AttachmentHelper(int... iArr) {
        this.touchableItems = iArr;
    }

    private List<MediaResult> requestAttachmentsToMediaResult(List<StateRequestAttachment> list) {
        ArrayList arrayList = new ArrayList(list.size());
        for (StateRequestAttachment stateRequestAttachment : list) {
            arrayList.add(StateRequestAttachment.convert(stateRequestAttachment));
        }
        return arrayList;
    }

    public List<StateRequestAttachment> getSelectedAttachments() {
        return l10.c(this.selectedAttachments);
    }

    public void showImagePicker(AppCompatActivity appCompatActivity) {
        BelvedereUi.b i = BelvedereUi.a(appCompatActivity).g().h("*/*", true).l(requestAttachmentsToMediaResult(this.selectedAttachments)).j(appCompatActivity.getResources().getBoolean(ly2.zs_request_image_picker_full_screen)).i(requestAttachmentsToMediaResult(this.additionalAttachments));
        int[] iArr = this.touchableItems;
        if (iArr != null && iArr.length > 0) {
            i = i.m(iArr);
        }
        long j = this.maxFileSize;
        if (j > 0) {
            i = i.k(j);
        }
        i.f(appCompatActivity);
    }

    public void updateAttachments(Collection<StateRequestAttachment> collection, Collection<StateRequestAttachment> collection2) {
        this.selectedAttachments = l10.c(new ArrayList(collection));
        this.additionalAttachments = l10.c(new ArrayList(collection2));
    }

    public void updateMaxFileSize(long j) {
        this.maxFileSize = j;
    }
}
