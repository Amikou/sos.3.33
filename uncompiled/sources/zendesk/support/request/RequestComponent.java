package zendesk.support.request;

/* loaded from: classes3.dex */
public interface RequestComponent {
    void inject(RequestActivity requestActivity);

    void inject(RequestViewConversationsDisabled requestViewConversationsDisabled);

    void inject(RequestViewConversationsEnabled requestViewConversationsEnabled);
}
