package zendesk.support.request;

import com.zendesk.logger.Logger;
import java.io.File;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Executor;
import zendesk.belvedere.MediaResult;
import zendesk.belvedere.a;
import zendesk.support.SupportUiStorage;
import zendesk.support.request.AsyncMiddleware;
import zendesk.support.request.ComponentPersistence;
import zendesk.support.suas.Action;
import zendesk.support.suas.Dispatcher;
import zendesk.support.suas.GetState;

/* loaded from: classes3.dex */
class ActionLoadCachedComments implements AsyncMiddleware.AsyncAction {
    private final ActionFactory af;
    private final a belvedere;
    private final Executor executorService;
    private final String sdkVersion;
    private final SupportUiStorage supportUiStorage;

    /* loaded from: classes3.dex */
    public static class LoadComments implements Runnable {
        private final ActionFactory af;
        private final a belvedere;
        private final AsyncMiddleware.Callback callback;
        private final Dispatcher dispatcher;
        private final String id;
        private final String sdkVersion;
        private final SupportUiStorage supportUiStorage;

        public LoadComments(String str, Dispatcher dispatcher, AsyncMiddleware.Callback callback, SupportUiStorage supportUiStorage, ActionFactory actionFactory, a aVar, String str2) {
            this.id = str;
            this.dispatcher = dispatcher;
            this.callback = callback;
            this.supportUiStorage = supportUiStorage;
            this.af = actionFactory;
            this.belvedere = aVar;
            this.sdkVersion = str2;
        }

        private StateMessage findLocalAttachmentForMessage(StateMessage stateMessage, StateIdMapper stateIdMapper, a aVar, String str) {
            List<StateRequestAttachment> attachments = stateMessage.getAttachments();
            if (l10.i(attachments)) {
                ArrayList arrayList = new ArrayList(stateMessage.getAttachments().size());
                for (StateRequestAttachment stateRequestAttachment : attachments) {
                    if (stateIdMapper.hasRemoteId(Long.valueOf(stateRequestAttachment.getId()))) {
                        arrayList.add(updateAttachment(stateRequestAttachment, UtilsAttachment.getLocalFile(aVar, str, stateIdMapper.getRemoteId(Long.valueOf(stateRequestAttachment.getId())).longValue(), stateRequestAttachment.getName())));
                    } else {
                        arrayList.add(stateRequestAttachment);
                    }
                }
                return stateMessage.withAttachments(arrayList);
            }
            return stateMessage;
        }

        private StateRequestAttachment updateAttachment(StateRequestAttachment stateRequestAttachment, MediaResult mediaResult) {
            String str;
            File file = null;
            if (stateRequestAttachment.getSize() == mediaResult.e().length()) {
                file = mediaResult.e();
                str = mediaResult.l().toString();
            } else {
                str = null;
            }
            return stateRequestAttachment.newBuilder().setLocalFile(file).setLocalUri(str).build();
        }

        public String getId() {
            return this.id;
        }

        public StateConversation resolveAttachments(StateConversation stateConversation) {
            ArrayList arrayList = new ArrayList(stateConversation.getMessages().size());
            for (StateMessage stateMessage : stateConversation.getMessages()) {
                arrayList.add(findLocalAttachmentForMessage(stateMessage, stateConversation.getAttachmentIdMapper(), this.belvedere, stateConversation.getLocalId()));
            }
            return stateConversation.newBuilder().setMessages(arrayList).build();
        }

        @Override // java.lang.Runnable
        public void run() {
            Action loadCommentsFromCacheError;
            ComponentPersistence.RequestPersistenceModel requestPersistenceModel = (ComponentPersistence.RequestPersistenceModel) this.supportUiStorage.read(this.id, ComponentPersistence.RequestPersistenceModel.class);
            if (requestPersistenceModel != null && requestPersistenceModel.getConversation() != null) {
                if (this.sdkVersion.equals(requestPersistenceModel.getVersion())) {
                    Logger.b(RequestActivity.LOG_TAG, "Successfully loaded request from disk", new Object[0]);
                    loadCommentsFromCacheError = this.af.loadCommentsFromCacheSuccess(resolveAttachments(requestPersistenceModel.getConversation()));
                } else {
                    Logger.b(RequestActivity.LOG_TAG, "Cached version doesn't match with SDK version. Ignoring cached data. [%s, %s]", requestPersistenceModel.getVersion(), "5.0.9");
                    loadCommentsFromCacheError = this.af.loadCommentsFromCacheError();
                }
            } else {
                Logger.b(RequestActivity.LOG_TAG, "Unable to loaded request from disk", new Object[0]);
                loadCommentsFromCacheError = this.af.loadCommentsFromCacheError();
            }
            this.dispatcher.dispatch(loadCommentsFromCacheError);
            this.callback.done();
        }
    }

    public ActionLoadCachedComments(ActionFactory actionFactory, SupportUiStorage supportUiStorage, a aVar, Executor executor, String str) {
        this.af = actionFactory;
        this.supportUiStorage = supportUiStorage;
        this.belvedere = aVar;
        this.executorService = executor;
        this.sdkVersion = str;
    }

    @Override // zendesk.support.request.AsyncMiddleware.AsyncAction
    public void actionQueued(Dispatcher dispatcher, GetState getState) {
        dispatcher.dispatch(this.af.loadCommentsFromCache());
    }

    @Override // zendesk.support.request.AsyncMiddleware.AsyncAction
    public void execute(Dispatcher dispatcher, GetState getState, AsyncMiddleware.Callback callback) {
        StateConversation fromState = StateConversation.fromState(getState.getState());
        if (ru3.b(fromState.getLocalId())) {
            this.executorService.execute(new LoadComments(fromState.getLocalId(), dispatcher, callback, this.supportUiStorage, this.af, this.belvedere, this.sdkVersion));
            return;
        }
        dispatcher.dispatch(this.af.skipAction());
        callback.done();
    }
}
