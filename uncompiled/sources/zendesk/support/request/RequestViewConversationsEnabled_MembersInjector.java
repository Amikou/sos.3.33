package zendesk.support.request;

import com.squareup.picasso.Picasso;
import zendesk.support.suas.Store;

/* loaded from: classes3.dex */
public final class RequestViewConversationsEnabled_MembersInjector implements i72<RequestViewConversationsEnabled> {
    private final ew2<ActionFactory> afProvider;
    private final ew2<CellFactory> cellFactoryProvider;
    private final ew2<Picasso> picassoProvider;
    private final ew2<Store> storeProvider;

    public RequestViewConversationsEnabled_MembersInjector(ew2<Store> ew2Var, ew2<ActionFactory> ew2Var2, ew2<CellFactory> ew2Var3, ew2<Picasso> ew2Var4) {
        this.storeProvider = ew2Var;
        this.afProvider = ew2Var2;
        this.cellFactoryProvider = ew2Var3;
        this.picassoProvider = ew2Var4;
    }

    public static i72<RequestViewConversationsEnabled> create(ew2<Store> ew2Var, ew2<ActionFactory> ew2Var2, ew2<CellFactory> ew2Var3, ew2<Picasso> ew2Var4) {
        return new RequestViewConversationsEnabled_MembersInjector(ew2Var, ew2Var2, ew2Var3, ew2Var4);
    }

    public static void injectAf(RequestViewConversationsEnabled requestViewConversationsEnabled, Object obj) {
        requestViewConversationsEnabled.af = (ActionFactory) obj;
    }

    public static void injectCellFactory(RequestViewConversationsEnabled requestViewConversationsEnabled, Object obj) {
        requestViewConversationsEnabled.cellFactory = (CellFactory) obj;
    }

    public static void injectPicasso(RequestViewConversationsEnabled requestViewConversationsEnabled, Picasso picasso) {
        requestViewConversationsEnabled.picasso = picasso;
    }

    public static void injectStore(RequestViewConversationsEnabled requestViewConversationsEnabled, Store store) {
        requestViewConversationsEnabled.store = store;
    }

    public void injectMembers(RequestViewConversationsEnabled requestViewConversationsEnabled) {
        injectStore(requestViewConversationsEnabled, this.storeProvider.get());
        injectAf(requestViewConversationsEnabled, this.afProvider.get());
        injectCellFactory(requestViewConversationsEnabled, this.cellFactoryProvider.get());
        injectPicasso(requestViewConversationsEnabled, this.picassoProvider.get());
    }
}
