package zendesk.support.request;

import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.graphics.drawable.Drawable;
import android.text.TextUtils;
import java.io.File;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.Locale;
import zendesk.belvedere.MediaResult;
import zendesk.belvedere.a;
import zendesk.support.IdUtil;
import zendesk.support.ZendeskSupportSettingsProvider;

/* JADX INFO: Access modifiers changed from: package-private */
/* loaded from: classes3.dex */
public class UtilsAttachment {
    private static final String ATTACHMENT_SEPARATOR = ", ";
    private static final String ATTACHMENT_TEXT_BODY = "[%s]";
    private static final String PATH_PLACEHOLDER = "%s%s%s";
    private static final AttachmentNameComparator REQUEST_ATTACHMENT_COMPARATOR;
    private static final String REQUEST_BELVEDERE_PATH;
    private static final String SUPPORT_BELVEDERE_BASE_PATH;

    /* loaded from: classes3.dex */
    public static class AttachmentNameComparator implements Comparator<StateRequestAttachment> {
        private AttachmentNameComparator() {
        }

        @Override // java.util.Comparator
        public int compare(StateRequestAttachment stateRequestAttachment, StateRequestAttachment stateRequestAttachment2) {
            return stateRequestAttachment.getName().compareTo(stateRequestAttachment2.getName());
        }
    }

    static {
        Locale locale = Locale.US;
        String str = File.separator;
        String format = String.format(locale, PATH_PLACEHOLDER, "zendesk", str, ZendeskSupportSettingsProvider.SUPPORT_KEY);
        SUPPORT_BELVEDERE_BASE_PATH = format;
        REQUEST_BELVEDERE_PATH = String.format(locale, PATH_PLACEHOLDER, format, str, "request");
        REQUEST_ATTACHMENT_COMPARATOR = new AttachmentNameComparator();
    }

    private UtilsAttachment() {
    }

    public static Drawable getAppIcon(Context context, ResolveInfo resolveInfo) {
        Drawable loadIcon = resolveInfo != null ? resolveInfo.loadIcon(context.getPackageManager()) : null;
        return loadIcon != null ? loadIcon : m70.f(context, 17301651);
    }

    public static ResolveInfo getAppInfoForFile(Context context, String str) {
        PackageManager packageManager = context.getPackageManager();
        MediaResult d = a.c(context).d("tmp", str);
        if (d == null) {
            return null;
        }
        Intent intent = new Intent("android.intent.action.VIEW");
        intent.setData(d.l());
        List<ResolveInfo> queryIntentActivities = packageManager.queryIntentActivities(intent, 65536);
        if (l10.g(queryIntentActivities)) {
            return null;
        }
        return queryIntentActivities.get(0);
    }

    public static CharSequence getAppName(Context context, ResolveInfo resolveInfo) {
        String loadLabel = resolveInfo != null ? resolveInfo.loadLabel(context.getPackageManager()) : "";
        return !TextUtils.isEmpty(loadLabel) ? loadLabel : context.getString(i13.request_attachment_generic_unknown_app);
    }

    public static String getAttachmentSubDir(String str, long j) {
        return String.format(Locale.US, PATH_PLACEHOLDER, str, File.separator, Long.valueOf(j));
    }

    public static String getCacheDirForRequestId(String str) {
        return String.format(Locale.US, PATH_PLACEHOLDER, REQUEST_BELVEDERE_PATH, File.separator, str);
    }

    public static String getContentDescriptionForAttachmentButton(Context context, int i) {
        StringBuilder sb = new StringBuilder();
        sb.append(context.getString(i13.request_menu_button_label_add_attachments));
        sb.append(". ");
        if (i == 0) {
            sb.append(context.getString(i13.zs_request_attachment_indicator_no_attachments_selected_accessibility));
        } else if (i == 1) {
            sb.append(context.getString(i13.zs_request_attachment_indicator_one_attachments_selected_accessibility));
        } else {
            sb.append(context.getString(i13.zs_request_attachment_indicator_n_attachments_selected_accessibility, Integer.valueOf(i)));
        }
        return sb.toString();
    }

    public static MediaResult getLocalFile(a aVar, String str, long j, String str2) {
        return aVar.d(getAttachmentSubDir(getCacheDirForRequestId(str), j), str2);
    }

    public static String getMessageBodyForAttachments(List<StateRequestAttachment> list) {
        List c = l10.c(list);
        Collections.sort(c, REQUEST_ATTACHMENT_COMPARATOR);
        StringBuilder sb = new StringBuilder();
        int size = c.size();
        for (int i = 0; i < size; i++) {
            sb.append(((StateRequestAttachment) c.get(i)).getName());
            if (i < size - 1) {
                sb.append(ATTACHMENT_SEPARATOR);
            }
        }
        return String.format(Locale.US, ATTACHMENT_TEXT_BODY, sb.toString());
    }

    public static String getTemporaryRequestCacheDir() {
        return String.format(Locale.US, PATH_PLACEHOLDER, REQUEST_BELVEDERE_PATH, File.separator, IdUtil.newStringId());
    }

    public static boolean hasAttachmentBody(StateMessage stateMessage) {
        if (l10.i(stateMessage.getAttachments())) {
            return stateMessage.getBody().equals(getMessageBodyForAttachments(stateMessage.getAttachments()));
        }
        return false;
    }

    public static boolean isImageAttachment(StateRequestAttachment stateRequestAttachment) {
        String mimeType = stateRequestAttachment.getMimeType();
        return ru3.b(mimeType) && mimeType.toLowerCase(Locale.US).startsWith("image");
    }
}
