package zendesk.support.request;

import android.annotation.SuppressLint;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import zendesk.support.CustomField;

/* JADX INFO: Access modifiers changed from: package-private */
/* loaded from: classes3.dex */
public class StateRequestTicketForm implements Serializable {
    public static final long NO_ID = -1;
    private final long id;
    private final Map<Long, String> ticketFields;

    public StateRequestTicketForm(long j, List<CustomField> list) {
        this.id = j;
        this.ticketFields = fieldsToMap(list);
    }

    @SuppressLint({"UseSparseArrays"})
    private static Map<Long, String> fieldsToMap(List<CustomField> list) {
        HashMap hashMap = new HashMap(list.size());
        for (CustomField customField : list) {
            if (customField != null && ru3.b(customField.getValueString())) {
                hashMap.put(customField.getId(), customField.getValueString());
            }
        }
        return hashMap;
    }

    private static List<CustomField> mapToFields(Map<Long, String> map) {
        ArrayList arrayList = new ArrayList(map.size());
        for (Map.Entry<Long, String> entry : map.entrySet()) {
            arrayList.add(new CustomField(entry.getKey(), entry.getValue()));
        }
        return arrayList;
    }

    public long getId() {
        return this.id;
    }

    public Map<Long, String> getTicketFields() {
        return this.ticketFields;
    }

    public List<CustomField> getTicketFieldsForApi() {
        return mapToFields(this.ticketFields);
    }

    public StateRequestTicketForm(List<CustomField> list) {
        this(-1L, list);
    }
}
