package zendesk.support.request;

import android.view.View;
import android.view.ViewGroup;
import androidx.appcompat.app.AppCompatActivity;
import androidx.transition.Fade;
import androidx.transition.d;
import com.zendesk.logger.Logger;
import java.util.concurrent.atomic.AtomicReference;
import zendesk.support.request.StateError;
import zendesk.support.suas.Listener;
import zendesk.support.suas.State;
import zendesk.support.suas.StateSelector;

/* JADX INFO: Access modifiers changed from: package-private */
/* loaded from: classes3.dex */
public class ComponentRequestRouter implements Listener<RequestScreen> {
    private final AppCompatActivity activity;
    private final RequestComponent component;
    private RequestView currentScreen;
    private final RequestViewConversationsDisabled disabledView;
    private final RequestViewConversationsEnabled enabledView;
    private final boolean isCleanStart;
    private final RequestViewLoading loadingView;
    private final ViewGroup root;
    private final AtomicReference<RequestScreen> screen = new AtomicReference<>();

    /* renamed from: zendesk.support.request.ComponentRequestRouter$1  reason: invalid class name */
    /* loaded from: classes3.dex */
    public static /* synthetic */ class AnonymousClass1 {
        public static final /* synthetic */ int[] $SwitchMap$zendesk$support$request$ComponentRequestRouter$RequestScreen;

        static {
            int[] iArr = new int[RequestScreen.values().length];
            $SwitchMap$zendesk$support$request$ComponentRequestRouter$RequestScreen = iArr;
            try {
                iArr[RequestScreen.Loading.ordinal()] = 1;
            } catch (NoSuchFieldError unused) {
            }
            try {
                $SwitchMap$zendesk$support$request$ComponentRequestRouter$RequestScreen[RequestScreen.EmailForm.ordinal()] = 2;
            } catch (NoSuchFieldError unused2) {
            }
            try {
                $SwitchMap$zendesk$support$request$ComponentRequestRouter$RequestScreen[RequestScreen.Conversation.ordinal()] = 3;
            } catch (NoSuchFieldError unused3) {
            }
            try {
                $SwitchMap$zendesk$support$request$ComponentRequestRouter$RequestScreen[RequestScreen.Fin.ordinal()] = 4;
            } catch (NoSuchFieldError unused4) {
            }
        }
    }

    /* loaded from: classes3.dex */
    public static class RequestRouterSelector implements StateSelector<RequestScreen> {
        @Override // zendesk.support.suas.StateSelector
        public RequestScreen selectData(State state) {
            StateConfig fromState = StateConfig.fromState(state);
            StateConversation fromState2 = StateConversation.fromState(state);
            StateSettings settings = fromState.getSettings();
            StateError fromState3 = StateError.fromState(state);
            boolean hasSettings = settings.hasSettings();
            boolean b = ru3.b(fromState2.getRemoteId());
            boolean isConversationsEnabled = settings.isConversationsEnabled();
            boolean hasIdentityEmailAddress = settings.hasIdentityEmailAddress();
            boolean isNeverRequestEmailOn = settings.isNeverRequestEmailOn();
            if (fromState3.getState() == StateError.ErrorType.NoAccess) {
                Logger.e(RequestActivity.LOG_TAG, "Network returned 'No Access'. Ticket is not longer valid. Error: '%s'", fromState3.getMessage());
                return RequestScreen.Fin;
            } else if (hasSettings) {
                if (isConversationsEnabled) {
                    return RequestScreen.Conversation;
                }
                if (b) {
                    Logger.k(RequestActivity.LOG_TAG, "Conversations are disabled. Exiting RequestActivity", new Object[0]);
                    return RequestScreen.Fin;
                } else if (!hasIdentityEmailAddress && isNeverRequestEmailOn) {
                    Logger.k(RequestActivity.LOG_TAG, "Conversations are disabled, never request email is enabled, with this configuration tickets would go into a black hole. Exiting RequestActivity.", new Object[0]);
                    return RequestScreen.Fin;
                } else {
                    return RequestScreen.EmailForm;
                }
            } else {
                return RequestScreen.Loading;
            }
        }
    }

    /* loaded from: classes3.dex */
    public enum RequestScreen {
        Loading,
        EmailForm,
        Conversation,
        Fin
    }

    public ComponentRequestRouter(AppCompatActivity appCompatActivity, ViewGroup viewGroup, RequestViewConversationsDisabled requestViewConversationsDisabled, RequestViewConversationsEnabled requestViewConversationsEnabled, RequestViewLoading requestViewLoading, RequestComponent requestComponent, boolean z) {
        this.activity = appCompatActivity;
        this.root = viewGroup;
        this.disabledView = requestViewConversationsDisabled;
        this.enabledView = requestViewConversationsEnabled;
        this.loadingView = requestViewLoading;
        this.component = requestComponent;
        this.isCleanStart = z;
    }

    public static ComponentRequestRouter create(AppCompatActivity appCompatActivity, boolean z, RequestComponent requestComponent) {
        return new ComponentRequestRouter(appCompatActivity, (ViewGroup) appCompatActivity.findViewById(f03.activity_request_root), (RequestViewConversationsDisabled) appCompatActivity.findViewById(f03.activity_request_conversation_disabled), (RequestViewConversationsEnabled) appCompatActivity.findViewById(f03.activity_request_conversation), (RequestViewLoading) appCompatActivity.findViewById(f03.activity_request_loading), requestComponent, z);
    }

    private void displayView(View view, View... viewArr) {
        d.a(this.root, new Fade());
        view.setVisibility(0);
        for (View view2 : viewArr) {
            view2.setVisibility(8);
        }
        this.activity.invalidateOptionsMenu();
        d.c(this.root);
    }

    public RequestView getCurrentScreen() {
        return this.currentScreen;
    }

    public StateSelector<RequestScreen> getSelector() {
        return new RequestRouterSelector();
    }

    @Override // zendesk.support.suas.Listener
    public void update(RequestScreen requestScreen) {
        if (this.screen.getAndSet(requestScreen) == requestScreen) {
            return;
        }
        int i = AnonymousClass1.$SwitchMap$zendesk$support$request$ComponentRequestRouter$RequestScreen[requestScreen.ordinal()];
        if (i == 1) {
            Logger.b(RequestActivity.LOG_TAG, "Installing screen: 'Loading Screen'", new Object[0]);
            RequestViewLoading requestViewLoading = this.loadingView;
            this.currentScreen = requestViewLoading;
            displayView(requestViewLoading, this.disabledView, this.enabledView);
        } else if (i == 2) {
            Logger.b(RequestActivity.LOG_TAG, "Installing screen: 'Conversations Disabled Screen'", new Object[0]);
            RequestViewConversationsDisabled requestViewConversationsDisabled = this.disabledView;
            this.currentScreen = requestViewConversationsDisabled;
            displayView(requestViewConversationsDisabled, this.enabledView, this.loadingView);
            this.disabledView.init(this.component);
        } else if (i != 3) {
            if (i != 4) {
                return;
            }
            Logger.b(RequestActivity.LOG_TAG, "Installing screen: 'Finish'", new Object[0]);
            this.activity.finish();
        } else {
            Logger.b(RequestActivity.LOG_TAG, "Installing screen: 'Conversations Enabled Screen'", new Object[0]);
            RequestViewConversationsEnabled requestViewConversationsEnabled = this.enabledView;
            this.currentScreen = requestViewConversationsEnabled;
            displayView(requestViewConversationsEnabled, this.disabledView, this.loadingView);
            this.enabledView.init(this.component, this.isCleanStart);
        }
    }
}
