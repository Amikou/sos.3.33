package zendesk.support.request;

import com.squareup.picasso.Picasso;
import zendesk.support.suas.Store;

/* loaded from: classes3.dex */
public final class RequestViewConversationsDisabled_MembersInjector implements i72<RequestViewConversationsDisabled> {
    private final ew2<ActionFactory> afProvider;
    private final ew2<Picasso> picassoProvider;
    private final ew2<Store> storeProvider;

    public RequestViewConversationsDisabled_MembersInjector(ew2<Store> ew2Var, ew2<ActionFactory> ew2Var2, ew2<Picasso> ew2Var3) {
        this.storeProvider = ew2Var;
        this.afProvider = ew2Var2;
        this.picassoProvider = ew2Var3;
    }

    public static i72<RequestViewConversationsDisabled> create(ew2<Store> ew2Var, ew2<ActionFactory> ew2Var2, ew2<Picasso> ew2Var3) {
        return new RequestViewConversationsDisabled_MembersInjector(ew2Var, ew2Var2, ew2Var3);
    }

    public static void injectAf(RequestViewConversationsDisabled requestViewConversationsDisabled, Object obj) {
        requestViewConversationsDisabled.af = (ActionFactory) obj;
    }

    public static void injectPicasso(RequestViewConversationsDisabled requestViewConversationsDisabled, Picasso picasso) {
        requestViewConversationsDisabled.picasso = picasso;
    }

    public static void injectStore(RequestViewConversationsDisabled requestViewConversationsDisabled, Store store) {
        requestViewConversationsDisabled.store = store;
    }

    public void injectMembers(RequestViewConversationsDisabled requestViewConversationsDisabled) {
        injectStore(requestViewConversationsDisabled, this.storeProvider.get());
        injectAf(requestViewConversationsDisabled, this.afProvider.get());
        injectPicasso(requestViewConversationsDisabled, this.picassoProvider.get());
    }
}
