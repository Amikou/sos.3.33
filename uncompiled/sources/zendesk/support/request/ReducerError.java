package zendesk.support.request;

import zendesk.support.suas.Action;
import zendesk.support.suas.Reducer;

/* loaded from: classes3.dex */
class ReducerError extends Reducer<StateError> {
    @Override // zendesk.support.suas.Reducer
    public /* bridge */ /* synthetic */ StateError reduce(StateError stateError, Action action) {
        return reduce2(stateError, (Action<?>) action);
    }

    @Override // zendesk.support.suas.Reducer
    public StateError getInitialState() {
        return new StateError();
    }

    /* JADX WARN: Removed duplicated region for block: B:45:0x0098  */
    /* JADX WARN: Removed duplicated region for block: B:49:0x00a2  */
    /* JADX WARN: Removed duplicated region for block: B:53:0x00bc  */
    /* JADX WARN: Removed duplicated region for block: B:55:0x00c2 A[ORIG_RETURN, RETURN] */
    /* renamed from: reduce  reason: avoid collision after fix types in other method */
    /*
        Code decompiled incorrectly, please refer to instructions dump.
        To view partially-correct code enable 'Show inconsistent code' option in preferences
    */
    public zendesk.support.request.StateError reduce2(zendesk.support.request.StateError r4, zendesk.support.suas.Action<?> r5) {
        /*
            r3 = this;
            boolean r0 = r5 instanceof zendesk.support.request.ActionFactory.ErrorAction
            if (r0 == 0) goto L25
            r0 = r5
            zendesk.support.request.ActionFactory$ErrorAction r0 = (zendesk.support.request.ActionFactory.ErrorAction) r0
            cw0 r0 = r0.getErrorResponse()
            boolean r1 = r0.i()
            if (r1 == 0) goto L25
            int r1 = r0.e()
            r2 = 401(0x191, float:5.62E-43)
            if (r1 != r2) goto L25
            zendesk.support.request.StateError r4 = new zendesk.support.request.StateError
            zendesk.support.request.StateError$ErrorType r5 = zendesk.support.request.StateError.ErrorType.NoAccess
            java.lang.String r0 = r0.h()
            r4.<init>(r5, r0)
            return r4
        L25:
            java.lang.String r0 = r5.getActionType()
            r0.hashCode()
            r1 = -1
            int r2 = r0.hashCode()
            switch(r2) {
                case -1193398337: goto L6c;
                case -1063298693: goto L61;
                case -292168757: goto L56;
                case -16010570: goto L4b;
                case 1532422677: goto L40;
                case 1921186300: goto L35;
                default: goto L34;
            }
        L34:
            goto L76
        L35:
            java.lang.String r2 = "CREATE_COMMENT"
            boolean r0 = r0.equals(r2)
            if (r0 != 0) goto L3e
            goto L76
        L3e:
            r1 = 5
            goto L76
        L40:
            java.lang.String r2 = "CREATE_REQUEST_ERROR"
            boolean r0 = r0.equals(r2)
            if (r0 != 0) goto L49
            goto L76
        L49:
            r1 = 4
            goto L76
        L4b:
            java.lang.String r2 = "LOAD_COMMENTS_INITIAL_SUCCESS"
            boolean r0 = r0.equals(r2)
            if (r0 != 0) goto L54
            goto L76
        L54:
            r1 = 3
            goto L76
        L56:
            java.lang.String r2 = "LOAD_COMMENT_INITIAL"
            boolean r0 = r0.equals(r2)
            if (r0 != 0) goto L5f
            goto L76
        L5f:
            r1 = 2
            goto L76
        L61:
            java.lang.String r2 = "LOAD_COMMENTS_INITIAL_ERROR"
            boolean r0 = r0.equals(r2)
            if (r0 != 0) goto L6a
            goto L76
        L6a:
            r1 = 1
            goto L76
        L6c:
            java.lang.String r2 = "LOAD_COMMENTS_UPDATE_SUCCESS"
            boolean r0 = r0.equals(r2)
            if (r0 != 0) goto L75
            goto L76
        L75:
            r1 = 0
        L76:
            switch(r1) {
                case 0: goto L90;
                case 1: goto L7a;
                case 2: goto L90;
                case 3: goto L90;
                case 4: goto L9e;
                case 5: goto Lb4;
                default: goto L79;
            }
        L79:
            goto Lc2
        L7a:
            boolean r0 = r5 instanceof zendesk.support.request.ActionFactory.ErrorAction
            if (r0 == 0) goto L90
            zendesk.support.request.ActionFactory$ErrorAction r5 = (zendesk.support.request.ActionFactory.ErrorAction) r5
            cw0 r4 = r5.getErrorResponse()
            zendesk.support.request.StateError r5 = new zendesk.support.request.StateError
            zendesk.support.request.StateError$ErrorType r0 = zendesk.support.request.StateError.ErrorType.InitialGetComments
            java.lang.String r4 = r4.h()
            r5.<init>(r0, r4)
            return r5
        L90:
            zendesk.support.request.StateError$ErrorType r0 = r4.getState()
            zendesk.support.request.StateError$ErrorType r1 = zendesk.support.request.StateError.ErrorType.InitialGetComments
            if (r0 != r1) goto L9e
            zendesk.support.request.StateError r4 = new zendesk.support.request.StateError
            r4.<init>()
            return r4
        L9e:
            boolean r0 = r5 instanceof zendesk.support.request.ActionFactory.ErrorAction
            if (r0 == 0) goto Lb4
            zendesk.support.request.ActionFactory$ErrorAction r5 = (zendesk.support.request.ActionFactory.ErrorAction) r5
            cw0 r4 = r5.getErrorResponse()
            zendesk.support.request.StateError r5 = new zendesk.support.request.StateError
            zendesk.support.request.StateError$ErrorType r0 = zendesk.support.request.StateError.ErrorType.InputFormSubmission
            java.lang.String r4 = r4.h()
            r5.<init>(r0, r4)
            return r5
        Lb4:
            zendesk.support.request.StateError$ErrorType r4 = r4.getState()
            zendesk.support.request.StateError$ErrorType r5 = zendesk.support.request.StateError.ErrorType.InputFormSubmission
            if (r4 != r5) goto Lc2
            zendesk.support.request.StateError r4 = new zendesk.support.request.StateError
            r4.<init>()
            return r4
        Lc2:
            r4 = 0
            return r4
        */
        throw new UnsupportedOperationException("Method not decompiled: zendesk.support.request.ReducerError.reduce2(zendesk.support.request.StateError, zendesk.support.suas.Action):zendesk.support.request.StateError");
    }
}
