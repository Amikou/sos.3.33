package zendesk.support.request;

import java.io.IOException;
import java.util.concurrent.Executor;
import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;
import okhttp3.ResponseBody;
import zendesk.belvedere.MediaResult;

/* JADX INFO: Access modifiers changed from: package-private */
/* loaded from: classes3.dex */
public class AttachmentDownloadService {
    private final Executor executor;
    private final OkHttpClient okHttpClient;

    /* loaded from: classes3.dex */
    public static class SaveToFileTask implements Runnable {
        private final rs4<MediaResult> callback;
        private final MediaResult destFile;
        private final ResponseBody responseBody;

        /* JADX WARN: Removed duplicated region for block: B:16:0x0053  */
        /* JADX WARN: Removed duplicated region for block: B:28:? A[RETURN, SYNTHETIC] */
        @Override // java.lang.Runnable
        /*
            Code decompiled incorrectly, please refer to instructions dump.
            To view partially-correct code enable 'Show inconsistent code' option in preferences
        */
        public void run() {
            /*
                r8 = this;
                r0 = 0
                zendesk.belvedere.MediaResult r1 = r8.destFile     // Catch: java.lang.Throwable -> L23 java.io.IOException -> L28
                java.io.File r1 = r1.e()     // Catch: java.lang.Throwable -> L23 java.io.IOException -> L28
                okio.m r1 = okio.k.f(r1)     // Catch: java.lang.Throwable -> L23 java.io.IOException -> L28
                okio.c r1 = okio.k.c(r1)     // Catch: java.lang.Throwable -> L23 java.io.IOException -> L28
                okhttp3.ResponseBody r2 = r8.responseBody     // Catch: java.io.IOException -> L21 java.lang.Throwable -> L5f
                okio.d r2 = r2.source()     // Catch: java.io.IOException -> L21 java.lang.Throwable -> L5f
                r1.P0(r2)     // Catch: java.io.IOException -> L21 java.lang.Throwable -> L5f
                zendesk.support.Streams.closeQuietly(r1)
                okhttp3.ResponseBody r1 = r8.responseBody
                zendesk.support.Streams.closeQuietly(r1)
                goto L4f
            L21:
                r0 = move-exception
                goto L2c
            L23:
                r1 = move-exception
                r7 = r1
                r1 = r0
                r0 = r7
                goto L60
            L28:
                r1 = move-exception
                r7 = r1
                r1 = r0
                r0 = r7
            L2c:
                java.lang.String r2 = "RequestActivity"
                java.lang.String r3 = "Unable to save attachment to disk. Error: '%s'"
                r4 = 1
                java.lang.Object[] r4 = new java.lang.Object[r4]     // Catch: java.lang.Throwable -> L5f
                r5 = 0
                java.lang.String r6 = r0.getMessage()     // Catch: java.lang.Throwable -> L5f
                r4[r5] = r6     // Catch: java.lang.Throwable -> L5f
                com.zendesk.logger.Logger.e(r2, r3, r4)     // Catch: java.lang.Throwable -> L5f
                dw0 r2 = new dw0     // Catch: java.lang.Throwable -> L5f
                java.lang.String r0 = r0.getMessage()     // Catch: java.lang.Throwable -> L5f
                r2.<init>(r0)     // Catch: java.lang.Throwable -> L5f
                zendesk.support.Streams.closeQuietly(r1)
                okhttp3.ResponseBody r0 = r8.responseBody
                zendesk.support.Streams.closeQuietly(r0)
                r0 = r2
            L4f:
                rs4<zendesk.belvedere.MediaResult> r1 = r8.callback
                if (r1 == 0) goto L5e
                if (r0 != 0) goto L5b
                zendesk.belvedere.MediaResult r0 = r8.destFile
                r1.onSuccess(r0)
                goto L5e
            L5b:
                r1.onError(r0)
            L5e:
                return
            L5f:
                r0 = move-exception
            L60:
                zendesk.support.Streams.closeQuietly(r1)
                okhttp3.ResponseBody r1 = r8.responseBody
                zendesk.support.Streams.closeQuietly(r1)
                throw r0
            */
            throw new UnsupportedOperationException("Method not decompiled: zendesk.support.request.AttachmentDownloadService.SaveToFileTask.run():void");
        }

        private SaveToFileTask(ResponseBody responseBody, MediaResult mediaResult, rs4<MediaResult> rs4Var) {
            this.responseBody = responseBody;
            this.destFile = mediaResult;
            this.callback = rs4Var;
        }
    }

    public AttachmentDownloadService(OkHttpClient okHttpClient, Executor executor) {
        this.okHttpClient = okHttpClient;
        this.executor = executor;
    }

    public void downloadAttachment(String str, final rs4<ResponseBody> rs4Var) {
        this.okHttpClient.newCall(new Request.Builder().get().url(str).build()).enqueue(new Callback() { // from class: zendesk.support.request.AttachmentDownloadService.1
            @Override // okhttp3.Callback
            public void onFailure(Call call, IOException iOException) {
                rs4Var.onError(new dw0(iOException.getMessage()));
            }

            @Override // okhttp3.Callback
            public void onResponse(Call call, Response response) throws IOException {
                if (response.isSuccessful()) {
                    rs4Var.onSuccess(response.body());
                } else {
                    rs4Var.onError(new dw0(response.message()));
                }
            }
        });
    }

    public void storeAttachment(ResponseBody responseBody, MediaResult mediaResult, rs4<MediaResult> rs4Var) {
        this.executor.execute(new SaveToFileTask(responseBody, mediaResult, rs4Var));
    }
}
