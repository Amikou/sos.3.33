package zendesk.support.request;

import java.util.List;
import zendesk.support.suas.Reducer;

/* loaded from: classes3.dex */
public final class RequestModule_ProvidesReducerFactory implements y11<List<Reducer>> {

    /* loaded from: classes3.dex */
    public static final class InstanceHolder {
        private static final RequestModule_ProvidesReducerFactory INSTANCE = new RequestModule_ProvidesReducerFactory();

        private InstanceHolder() {
        }
    }

    public static RequestModule_ProvidesReducerFactory create() {
        return InstanceHolder.INSTANCE;
    }

    public static List<Reducer> providesReducer() {
        return (List) cu2.f(RequestModule.providesReducer());
    }

    @Override // defpackage.ew2
    public List<Reducer> get() {
        return providesReducer();
    }
}
