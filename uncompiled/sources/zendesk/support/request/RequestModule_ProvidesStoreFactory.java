package zendesk.support.request;

import java.util.List;
import zendesk.support.suas.Reducer;
import zendesk.support.suas.Store;

/* loaded from: classes3.dex */
public final class RequestModule_ProvidesStoreFactory implements y11<Store> {
    private final ew2<AsyncMiddleware> asyncMiddlewareProvider;
    private final ew2<List<Reducer>> reducersProvider;

    public RequestModule_ProvidesStoreFactory(ew2<List<Reducer>> ew2Var, ew2<AsyncMiddleware> ew2Var2) {
        this.reducersProvider = ew2Var;
        this.asyncMiddlewareProvider = ew2Var2;
    }

    public static RequestModule_ProvidesStoreFactory create(ew2<List<Reducer>> ew2Var, ew2<AsyncMiddleware> ew2Var2) {
        return new RequestModule_ProvidesStoreFactory(ew2Var, ew2Var2);
    }

    public static Store providesStore(List<Reducer> list, Object obj) {
        return (Store) cu2.f(RequestModule.providesStore(list, (AsyncMiddleware) obj));
    }

    @Override // defpackage.ew2
    public Store get() {
        return providesStore(this.reducersProvider.get(), this.asyncMiddlewareProvider.get());
    }
}
