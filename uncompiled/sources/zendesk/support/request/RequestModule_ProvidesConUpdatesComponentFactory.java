package zendesk.support.request;

import android.content.Context;
import zendesk.core.ActionHandlerRegistry;
import zendesk.support.requestlist.RequestInfoDataSource;

/* loaded from: classes3.dex */
public final class RequestModule_ProvidesConUpdatesComponentFactory implements y11<ComponentUpdateActionHandlers> {
    private final ew2<ActionHandlerRegistry> actionHandlerRegistryProvider;
    private final ew2<Context> contextProvider;
    private final ew2<RequestInfoDataSource.LocalDataSource> dataSourceProvider;

    public RequestModule_ProvidesConUpdatesComponentFactory(ew2<Context> ew2Var, ew2<ActionHandlerRegistry> ew2Var2, ew2<RequestInfoDataSource.LocalDataSource> ew2Var3) {
        this.contextProvider = ew2Var;
        this.actionHandlerRegistryProvider = ew2Var2;
        this.dataSourceProvider = ew2Var3;
    }

    public static RequestModule_ProvidesConUpdatesComponentFactory create(ew2<Context> ew2Var, ew2<ActionHandlerRegistry> ew2Var2, ew2<RequestInfoDataSource.LocalDataSource> ew2Var3) {
        return new RequestModule_ProvidesConUpdatesComponentFactory(ew2Var, ew2Var2, ew2Var3);
    }

    public static ComponentUpdateActionHandlers providesConUpdatesComponent(Context context, ActionHandlerRegistry actionHandlerRegistry, RequestInfoDataSource.LocalDataSource localDataSource) {
        return (ComponentUpdateActionHandlers) cu2.f(RequestModule.providesConUpdatesComponent(context, actionHandlerRegistry, localDataSource));
    }

    @Override // defpackage.ew2
    public ComponentUpdateActionHandlers get() {
        return providesConUpdatesComponent(this.contextProvider.get(), this.actionHandlerRegistryProvider.get(), this.dataSourceProvider.get());
    }
}
