package zendesk.support.request;

import com.zendesk.logger.Logger;
import zendesk.core.AnonymousIdentity;
import zendesk.core.AuthenticationProvider;
import zendesk.core.Identity;
import zendesk.support.SupportSdkSettings;
import zendesk.support.SupportSettingsProvider;
import zendesk.support.request.AsyncMiddleware;
import zendesk.support.suas.Dispatcher;
import zendesk.support.suas.GetState;

/* JADX INFO: Access modifiers changed from: package-private */
/* loaded from: classes3.dex */
public class ActionLoadSettings implements AsyncMiddleware.AsyncAction {
    private final ActionFactory actionFactory;
    private final AuthenticationProvider authProvider;
    private final SupportSettingsProvider settingsProvider;

    public ActionLoadSettings(ActionFactory actionFactory, SupportSettingsProvider supportSettingsProvider, AuthenticationProvider authenticationProvider) {
        this.actionFactory = actionFactory;
        this.settingsProvider = supportSettingsProvider;
        this.authProvider = authenticationProvider;
    }

    /* JADX INFO: Access modifiers changed from: private */
    public StateSettings constructSettings(SupportSdkSettings supportSdkSettings) {
        Identity identity = this.authProvider.getIdentity();
        if (identity instanceof AnonymousIdentity) {
            AnonymousIdentity anonymousIdentity = (AnonymousIdentity) identity;
            return StateSettings.fromSupportSettings(supportSdkSettings, ru3.b(anonymousIdentity.getEmail()), ru3.b(anonymousIdentity.getName()));
        }
        return StateSettings.fromSupportSettings(supportSdkSettings, true, true);
    }

    @Override // zendesk.support.request.AsyncMiddleware.AsyncAction
    public void actionQueued(Dispatcher dispatcher, GetState getState) {
        dispatcher.dispatch(this.actionFactory.loadSettings());
    }

    @Override // zendesk.support.request.AsyncMiddleware.AsyncAction
    public void execute(final Dispatcher dispatcher, GetState getState, final AsyncMiddleware.Callback callback) {
        this.settingsProvider.getSettings(new rs4<SupportSdkSettings>() { // from class: zendesk.support.request.ActionLoadSettings.1
            @Override // defpackage.rs4
            public void onError(cw0 cw0Var) {
                Logger.k(RequestActivity.LOG_TAG, "Error loading settings. Error: '%s'", cw0Var.h());
                dispatcher.dispatch(ActionLoadSettings.this.actionFactory.loadSettingsError(cw0Var));
                callback.done();
            }

            @Override // defpackage.rs4
            public void onSuccess(SupportSdkSettings supportSdkSettings) {
                dispatcher.dispatch(ActionLoadSettings.this.actionFactory.loadSettingsSuccess(ActionLoadSettings.this.constructSettings(supportSdkSettings)));
                callback.done();
            }
        });
    }
}
