package zendesk.support.request;

import android.view.View;
import zendesk.support.CommentsResponse;
import zendesk.support.request.ActionCreateComment;
import zendesk.support.suas.Action;
import zendesk.support.suas.Listener;

/* JADX INFO: Access modifiers changed from: package-private */
/* loaded from: classes3.dex */
public class RequestAccessibilityHerald implements Listener<Action<?>> {
    private final View view;

    public RequestAccessibilityHerald(View view) {
        this.view = view;
    }

    private void announce(int i, Object... objArr) {
        this.view.announceForAccessibility(this.view.getContext().getString(i, objArr));
    }

    public static RequestAccessibilityHerald create(RequestActivity requestActivity) {
        return new RequestAccessibilityHerald(requestActivity.findViewById(f03.activity_request_root));
    }

    @Override // zendesk.support.suas.Listener
    public void update(Action<?> action) {
        F f;
        String actionType = action.getActionType();
        actionType.hashCode();
        char c = 65535;
        switch (actionType.hashCode()) {
            case -1679314784:
                if (actionType.equals(ActionFactory.CREATE_COMMENT_SUCCESS)) {
                    c = 0;
                    break;
                }
                break;
            case -1319777819:
                if (actionType.equals(ActionFactory.CREATE_COMMENT_ERROR)) {
                    c = 1;
                    break;
                }
                break;
            case -292168757:
                if (actionType.equals(ActionFactory.LOAD_COMMENTS_INITIAL)) {
                    c = 2;
                    break;
                }
                break;
        }
        switch (c) {
            case 0:
                announce(i13.zs_request_announce_comment_created_accessibility, ((ActionCreateComment.CreateCommentResult) action.getData()).getMessage().getPlainBody());
                return;
            case 1:
                announce(i13.zs_request_announce_comment_failed_accessibility, ((StateMessage) action.getData()).getPlainBody());
                return;
            case 2:
                jp2 jp2Var = (jp2) action.getData();
                if (jp2Var == null || (f = jp2Var.a) == 0 || !l10.i(((CommentsResponse) f).getComments())) {
                    return;
                }
                announce(i13.zs_request_announce_comments_loaded_accessibility, new Object[0]);
                return;
            default:
                return;
        }
    }
}
