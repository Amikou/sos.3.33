package zendesk.support.request;

import android.content.Context;
import com.squareup.picasso.Picasso;
import zendesk.core.ActionHandlerRegistry;
import zendesk.support.suas.Dispatcher;

/* loaded from: classes3.dex */
public final class RequestModule_ProvidesMessageFactoryFactory implements y11<CellFactory> {
    private final ew2<ActionFactory> actionFactoryProvider;
    private final ew2<z40> configHelperProvider;
    private final ew2<Context> contextProvider;
    private final ew2<Dispatcher> dispatcherProvider;
    private final RequestModule module;
    private final ew2<Picasso> picassoProvider;
    private final ew2<ActionHandlerRegistry> registryProvider;

    public RequestModule_ProvidesMessageFactoryFactory(RequestModule requestModule, ew2<Context> ew2Var, ew2<Picasso> ew2Var2, ew2<ActionFactory> ew2Var3, ew2<Dispatcher> ew2Var4, ew2<ActionHandlerRegistry> ew2Var5, ew2<z40> ew2Var6) {
        this.module = requestModule;
        this.contextProvider = ew2Var;
        this.picassoProvider = ew2Var2;
        this.actionFactoryProvider = ew2Var3;
        this.dispatcherProvider = ew2Var4;
        this.registryProvider = ew2Var5;
        this.configHelperProvider = ew2Var6;
    }

    public static RequestModule_ProvidesMessageFactoryFactory create(RequestModule requestModule, ew2<Context> ew2Var, ew2<Picasso> ew2Var2, ew2<ActionFactory> ew2Var3, ew2<Dispatcher> ew2Var4, ew2<ActionHandlerRegistry> ew2Var5, ew2<z40> ew2Var6) {
        return new RequestModule_ProvidesMessageFactoryFactory(requestModule, ew2Var, ew2Var2, ew2Var3, ew2Var4, ew2Var5, ew2Var6);
    }

    public static CellFactory providesMessageFactory(RequestModule requestModule, Context context, Picasso picasso, Object obj, Dispatcher dispatcher, ActionHandlerRegistry actionHandlerRegistry, z40 z40Var) {
        return (CellFactory) cu2.f(requestModule.providesMessageFactory(context, picasso, (ActionFactory) obj, dispatcher, actionHandlerRegistry, z40Var));
    }

    @Override // defpackage.ew2
    public CellFactory get() {
        return providesMessageFactory(this.module, this.contextProvider.get(), this.picassoProvider.get(), this.actionFactoryProvider.get(), this.dispatcherProvider.get(), this.registryProvider.get(), this.configHelperProvider.get());
    }
}
