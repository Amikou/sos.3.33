package zendesk.support.request;

import com.zendesk.logger.Logger;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import zendesk.belvedere.MediaResult;
import zendesk.support.suas.Action;
import zendesk.support.suas.Reducer;

/* loaded from: classes3.dex */
class ReducerAttachments extends Reducer<StateAttachments> {
    private static final String LOG_FORMAT_ATTACHMENT_DROPPED = "Cannot add attachment %s. Size is %d, max attachment size is %d.";
    private static final String LOG_MESSAGE_ATTACHMENTS_DISABLED = "Cannot add attachments, they are disabled in your Zendesk settings.";

    @Override // zendesk.support.suas.Reducer
    public /* bridge */ /* synthetic */ StateAttachments reduce(StateAttachments stateAttachments, Action action) {
        return reduce2(stateAttachments, (Action<?>) action);
    }

    @Override // zendesk.support.suas.Reducer
    public StateAttachments getInitialState() {
        return new StateAttachments();
    }

    /* renamed from: reduce  reason: avoid collision after fix types in other method */
    public StateAttachments reduce2(StateAttachments stateAttachments, Action<?> action) {
        String actionType = action.getActionType();
        actionType.hashCode();
        char c = 65535;
        switch (actionType.hashCode()) {
            case -1326172566:
                if (actionType.equals(ActionFactory.ATTACHMENTS_SELECTED)) {
                    c = 0;
                    break;
                }
                break;
            case -635275733:
                if (actionType.equals(ActionFactory.ATTACHMENTS_DESELECTED)) {
                    c = 1;
                    break;
                }
                break;
            case -91317760:
                if (actionType.equals(ActionFactory.LOAD_SETTINGS_SUCCESS)) {
                    c = 2;
                    break;
                }
                break;
            case 207206879:
                if (actionType.equals(ActionFactory.START_CONFIG)) {
                    c = 3;
                    break;
                }
                break;
            case 979542142:
                if (actionType.equals(ActionFactory.CLEAR_ATTACHMENTS)) {
                    c = 4;
                    break;
                }
                break;
        }
        switch (c) {
            case 0:
                ArrayList arrayList = new ArrayList();
                for (MediaResult mediaResult : (List) action.getData()) {
                    arrayList.add(StateRequestAttachment.convert(mediaResult));
                }
                return stateAttachments.newBuilder().addAllSelectedAttachments(arrayList).setSelectedAttachments(l10.b(arrayList, stateAttachments.getSelectedAttachments())).build();
            case 1:
                HashSet hashSet = new HashSet();
                for (MediaResult mediaResult2 : (List) action.getData()) {
                    hashSet.add(mediaResult2.j());
                }
                ArrayList arrayList2 = new ArrayList();
                for (StateRequestAttachment stateRequestAttachment : stateAttachments.getSelectedAttachments()) {
                    if (!hashSet.contains(stateRequestAttachment.getParsedLocalUri())) {
                        arrayList2.add(stateRequestAttachment);
                    }
                }
                return stateAttachments.newBuilder().setSelectedAttachments(arrayList2).build();
            case 2:
                StateSettings stateSettings = (StateSettings) action.getData();
                ArrayList arrayList3 = new ArrayList();
                if (stateSettings.isAttachmentsEnabled()) {
                    long maxAttachmentSize = stateSettings.getMaxAttachmentSize();
                    for (StateRequestAttachment stateRequestAttachment2 : stateAttachments.getSelectedAttachments()) {
                        if (stateRequestAttachment2.getSize() > maxAttachmentSize) {
                            Logger.b(RequestActivity.LOG_TAG, LOG_FORMAT_ATTACHMENT_DROPPED, stateRequestAttachment2.getName(), Long.valueOf(stateRequestAttachment2.getSize()), Long.valueOf(maxAttachmentSize));
                        } else {
                            arrayList3.add(stateRequestAttachment2);
                        }
                    }
                    return stateAttachments.newBuilder().setSelectedAttachments(arrayList3).build();
                }
                Logger.k(RequestActivity.LOG_TAG, LOG_MESSAGE_ATTACHMENTS_DISABLED, new Object[0]);
                return new StateAttachments();
            case 3:
                List<StateRequestAttachment> files = ((RequestConfiguration) action.getData()).getFiles();
                return stateAttachments.newBuilder().addAllSelectedAttachments(files).setSelectedAttachments(files).build();
            case 4:
                return new StateAttachments();
            default:
                return null;
        }
    }
}
