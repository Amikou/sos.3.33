package zendesk.support.request;

import android.app.Activity;
import android.view.View;
import androidx.coordinatorlayout.widget.CoordinatorLayout;
import com.google.android.material.snackbar.Snackbar;
import zendesk.support.request.StateError;
import zendesk.support.suas.Dispatcher;
import zendesk.support.suas.Listener;
import zendesk.support.suas.State;
import zendesk.support.suas.StateSelector;

/* JADX INFO: Access modifiers changed from: package-private */
/* loaded from: classes3.dex */
public class ComponentError implements Listener<ErrorStateModel> {
    private final ActionFactory af;
    private final CoordinatorLayout container;
    private final Dispatcher dispatcher;
    private StateError.ErrorType errorState;
    private Snackbar snackbar;

    /* renamed from: zendesk.support.request.ComponentError$2  reason: invalid class name */
    /* loaded from: classes3.dex */
    public static /* synthetic */ class AnonymousClass2 {
        public static final /* synthetic */ int[] $SwitchMap$zendesk$support$request$StateError$ErrorType;

        static {
            int[] iArr = new int[StateError.ErrorType.values().length];
            $SwitchMap$zendesk$support$request$StateError$ErrorType = iArr;
            try {
                iArr[StateError.ErrorType.InitialGetComments.ordinal()] = 1;
            } catch (NoSuchFieldError unused) {
            }
            try {
                $SwitchMap$zendesk$support$request$StateError$ErrorType[StateError.ErrorType.InputFormSubmission.ordinal()] = 2;
            } catch (NoSuchFieldError unused2) {
            }
        }
    }

    /* loaded from: classes3.dex */
    public static class ErrorStateModel {
        private final boolean conversationsEnabled;
        private final String errorMessage;
        private final StateError.ErrorType errorState;

        public ErrorStateModel(StateError.ErrorType errorType, String str, boolean z) {
            this.errorState = errorType;
            this.errorMessage = str;
            this.conversationsEnabled = z;
        }

        public String getErrorMessage() {
            return this.errorMessage;
        }

        public StateError.ErrorType getErrorState() {
            return this.errorState;
        }

        public boolean isConversationsEnabled() {
            return this.conversationsEnabled;
        }
    }

    /* loaded from: classes3.dex */
    public static class ErrorStateSelector implements StateSelector<ErrorStateModel> {
        @Override // zendesk.support.suas.StateSelector
        public ErrorStateModel selectData(State state) {
            StateError fromState = StateError.fromState(state);
            return new ErrorStateModel(fromState.getState(), fromState.getMessage(), StateConfig.fromState(state).getSettings().isConversationsEnabled());
        }
    }

    private ComponentError(CoordinatorLayout coordinatorLayout, Dispatcher dispatcher, ActionFactory actionFactory) {
        this.container = coordinatorLayout;
        this.dispatcher = dispatcher;
        this.af = actionFactory;
    }

    public static ComponentError create(Activity activity, Dispatcher dispatcher, ActionFactory actionFactory) {
        return new ComponentError((CoordinatorLayout) activity.findViewById(f03.activity_request), dispatcher, actionFactory);
    }

    public static StateSelector<ErrorStateModel> getSelector() {
        return new ErrorStateSelector();
    }

    @Override // zendesk.support.suas.Listener
    public void update(ErrorStateModel errorStateModel) {
        if (errorStateModel.errorState == this.errorState) {
            return;
        }
        this.errorState = errorStateModel.errorState;
        if (errorStateModel.errorState != StateError.ErrorType.NoError) {
            this.snackbar = Snackbar.b0(this.container, errorStateModel.getErrorMessage(), -2);
            int i = AnonymousClass2.$SwitchMap$zendesk$support$request$StateError$ErrorType[errorStateModel.getErrorState().ordinal()];
            if (i != 1) {
                if (i == 2 && !errorStateModel.isConversationsEnabled()) {
                    this.snackbar.f0(i13.request_error_create_request);
                    this.snackbar.Q();
                    return;
                }
                return;
            }
            this.snackbar.f0(i13.request_error_load_comments);
            this.snackbar.d0(i13.retry_view_button_label, new View.OnClickListener() { // from class: zendesk.support.request.ComponentError.1
                @Override // android.view.View.OnClickListener
                public void onClick(View view) {
                    ComponentError.this.errorState = StateError.ErrorType.NoError;
                    ComponentError.this.dispatcher.dispatch(ComponentError.this.af.initialLoadCommentsAsync());
                }
            });
            this.snackbar.Q();
            return;
        }
        Snackbar snackbar = this.snackbar;
        if (snackbar != null) {
            snackbar.v();
        }
    }
}
