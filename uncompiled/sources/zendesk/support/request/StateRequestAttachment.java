package zendesk.support.request;

import android.net.Uri;
import java.io.File;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import zendesk.belvedere.MediaResult;
import zendesk.support.Attachment;
import zendesk.support.AttachmentFile;
import zendesk.support.CommentResponse;
import zendesk.support.IdUtil;

/* JADX INFO: Access modifiers changed from: package-private */
/* loaded from: classes3.dex */
public class StateRequestAttachment implements Serializable, Comparable<StateRequestAttachment> {
    private static final String DEFAULT_MIME_TYPE = "application/octet-stream";
    private final int height;
    private final long id;
    private final transient File localFile;
    private final String localUri;
    private final String mimeType;
    private final String name;
    private final long size;
    private final String thumbnailUrl;
    private final String token;
    private final String url;
    private final int width;

    /* loaded from: classes3.dex */
    public static class Builder {
        private int height;
        private long id;
        private File localFile;
        private String localUri;
        private String mimeType;
        private String name;
        private long size;
        private String thumbnailUrl;
        private String token;
        private String url;
        private int width;

        public StateRequestAttachment build() {
            return new StateRequestAttachment(this);
        }

        public Builder setHeight(int i) {
            this.height = i;
            return this;
        }

        public Builder setId(long j) {
            this.id = j;
            return this;
        }

        public Builder setLocalFile(File file) {
            this.localFile = file;
            return this;
        }

        public Builder setLocalUri(String str) {
            this.localUri = str;
            return this;
        }

        public Builder setMimeType(String str) {
            this.mimeType = str;
            return this;
        }

        public Builder setName(String str) {
            this.name = str;
            return this;
        }

        public Builder setSize(long j) {
            this.size = j;
            return this;
        }

        public void setThumbnailUrl(String str) {
            this.thumbnailUrl = str;
        }

        public Builder setToken(String str) {
            this.token = str;
            return this;
        }

        public Builder setUrl(String str) {
            this.url = str;
            return this;
        }

        public Builder setWidth(int i) {
            this.width = i;
            return this;
        }

        private Builder(StateRequestAttachment stateRequestAttachment) {
            this.id = stateRequestAttachment.getId();
            this.localFile = stateRequestAttachment.getLocalFile();
            this.localUri = stateRequestAttachment.getLocalUri();
            this.url = stateRequestAttachment.getUrl();
            this.token = stateRequestAttachment.getToken();
            this.mimeType = stateRequestAttachment.getMimeType();
            this.name = stateRequestAttachment.getName();
            this.size = stateRequestAttachment.getSize();
            this.width = stateRequestAttachment.getWidth();
            this.height = stateRequestAttachment.getHeight();
            this.thumbnailUrl = stateRequestAttachment.getThumbnailUrl();
        }
    }

    public static jp2<Map<Long, StateRequestAttachment>, StateIdMapper> convert(List<CommentResponse> list, Map<Long, MediaResult> map, StateIdMapper stateIdMapper) {
        ArrayList arrayList = new ArrayList();
        for (CommentResponse commentResponse : list) {
            arrayList.addAll(commentResponse.getAttachments());
        }
        return convert(arrayList, stateIdMapper, map);
    }

    private static String getMimeTypeForFile(File file) {
        return z82.b(a41.a(file.getName()));
    }

    public int getHeight() {
        return this.height;
    }

    public long getId() {
        return this.id;
    }

    public File getLocalFile() {
        return this.localFile;
    }

    public String getLocalUri() {
        return this.localUri;
    }

    public String getMimeType() {
        return ru3.b(this.mimeType) ? this.mimeType : DEFAULT_MIME_TYPE;
    }

    public String getName() {
        return this.name;
    }

    public Uri getParsedLocalUri() {
        return Uri.parse(this.localUri);
    }

    public long getSize() {
        return this.size;
    }

    public String getThumbnailUrl() {
        return this.thumbnailUrl;
    }

    public String getToken() {
        return this.token;
    }

    public String getUrl() {
        return this.url;
    }

    public int getWidth() {
        return this.width;
    }

    public boolean isAvailableLocally() {
        return (this.localUri == null || getParsedLocalUri() == null || this.localFile == null) ? false : true;
    }

    public Builder newBuilder() {
        return new Builder();
    }

    public String toString() {
        return "RequestAttachment{id=" + this.id + ", localUri='" + this.localUri + "', localFile=" + this.localFile + ", url='" + this.url + "', token='" + this.token + "', mimeType='" + this.mimeType + "', name='" + this.name + "', size='" + this.size + "', width='" + this.width + "', height='" + this.height + "'}";
    }

    public StateRequestAttachment(long j, String str, File file, String str2, String str3, String str4, String str5, long j2, int i, int i2, String str6) {
        this.id = j;
        this.localUri = str;
        this.localFile = file;
        this.url = str2;
        this.token = str3;
        this.mimeType = str4;
        this.name = str5;
        this.size = j2;
        this.width = i;
        this.height = i2;
        this.thumbnailUrl = str6;
    }

    @Override // java.lang.Comparable
    public int compareTo(StateRequestAttachment stateRequestAttachment) {
        return (int) (this.id - stateRequestAttachment.id);
    }

    public static jp2<Map<Long, StateRequestAttachment>, StateIdMapper> convert(List<Attachment> list, StateIdMapper stateIdMapper, Map<Long, MediaResult> map) {
        long newLongId;
        String str;
        File file;
        HashMap hashMap = new HashMap(list.size());
        for (Attachment attachment : list) {
            if (attachment.getId() != null) {
                if (stateIdMapper.hasLocalId(attachment.getId())) {
                    newLongId = stateIdMapper.getLocalId(attachment.getId()).longValue();
                } else {
                    newLongId = IdUtil.newLongId();
                    stateIdMapper.addIdMapping(attachment.getId(), Long.valueOf(newLongId));
                }
                long j = newLongId;
                if (map.containsKey(attachment.getId())) {
                    MediaResult mediaResult = map.get(attachment.getId());
                    File e = mediaResult.e();
                    str = mediaResult.l().toString();
                    file = e;
                } else {
                    str = null;
                    file = null;
                }
                hashMap.put(attachment.getId(), new StateRequestAttachment(j, str, file, attachment.getContentUrl(), "", attachment.getContentType(), attachment.getFileName(), attachment.getSize() != null ? attachment.getSize().longValue() : -1L, (int) (attachment.getWidth() != null ? attachment.getWidth().longValue() : -1L), (int) (attachment.getHeight() != null ? attachment.getHeight().longValue() : -1L), l10.i(attachment.getThumbnails()) ? attachment.getThumbnails().get(0).getContentUrl() : ""));
            }
        }
        return jp2.a(hashMap, stateIdMapper);
    }

    private StateRequestAttachment(Builder builder) {
        this.localFile = builder.localFile;
        this.localUri = builder.localUri;
        this.mimeType = builder.mimeType;
        this.name = builder.name;
        this.id = builder.id;
        this.url = builder.url;
        this.token = builder.token;
        this.size = builder.size;
        this.width = builder.width;
        this.height = builder.height;
        this.thumbnailUrl = builder.thumbnailUrl;
    }

    public static StateRequestAttachment convert(MediaResult mediaResult) {
        return new StateRequestAttachment(IdUtil.newLongId(), mediaResult.l().toString(), mediaResult.e(), "", "", mediaResult.g(), mediaResult.h(), mediaResult.k(), (int) mediaResult.o(), (int) mediaResult.f(), "");
    }

    public static StateRequestAttachment convert(File file) {
        return new StateRequestAttachment(IdUtil.newLongId(), Uri.fromFile(file).toString(), file, "", "", getMimeTypeForFile(file), file.getName(), file.length(), -1, -1, "");
    }

    public static StateRequestAttachment convert(AttachmentFile attachmentFile) {
        return new StateRequestAttachment(IdUtil.newLongId(), Uri.fromFile(attachmentFile.getFile()).toString(), attachmentFile.getFile(), "", "", getMimeTypeForFile(attachmentFile.getFile()), attachmentFile.getFileName(), attachmentFile.getFile().length(), -1, -1, "");
    }

    public static MediaResult convert(StateRequestAttachment stateRequestAttachment) {
        return new MediaResult(stateRequestAttachment.getLocalFile(), stateRequestAttachment.getParsedLocalUri(), stateRequestAttachment.getParsedLocalUri(), stateRequestAttachment.getName(), stateRequestAttachment.getMimeType(), stateRequestAttachment.getSize(), stateRequestAttachment.getWidth(), stateRequestAttachment.getHeight());
    }
}
