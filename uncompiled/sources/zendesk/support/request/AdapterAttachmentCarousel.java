package zendesk.support.request;

import android.content.Context;
import android.content.pm.ResolveInfo;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import androidx.recyclerview.widget.RecyclerView;
import com.squareup.picasso.Picasso;
import java.util.Collections;
import zendesk.support.suas.Dispatcher;

/* JADX INFO: Access modifiers changed from: package-private */
/* loaded from: classes3.dex */
public class AdapterAttachmentCarousel extends RecyclerView.Adapter<CarouselViewHolder> {
    private static final int FILE_ATTACHMENT = 1;
    private static final int IMAGE_ATTACHMENT = 2;
    private final ActionFactory af;
    private final AttachmentHelper attachmentHelper;
    private final Dispatcher dispatcher;
    private final Picasso picasso;
    private final CarouselViewHolder.OnRemoveListener removeListener = new CarouselViewHolder.OnRemoveListener() { // from class: zendesk.support.request.AdapterAttachmentCarousel.1
        @Override // zendesk.support.request.AdapterAttachmentCarousel.CarouselViewHolder.OnRemoveListener
        public void onRemove(StateRequestAttachment stateRequestAttachment) {
            AdapterAttachmentCarousel.this.dispatcher.dispatch(AdapterAttachmentCarousel.this.af.deselectAttachment(Collections.singletonList(StateRequestAttachment.convert(stateRequestAttachment))));
        }
    };

    /* loaded from: classes3.dex */
    public static abstract class CarouselViewHolder extends RecyclerView.a0 {

        /* loaded from: classes3.dex */
        public interface OnRemoveListener {
            void onRemove(StateRequestAttachment stateRequestAttachment);
        }

        public CarouselViewHolder(View view) {
            super(view);
        }

        public abstract void bind(StateRequestAttachment stateRequestAttachment, OnRemoveListener onRemoveListener);
    }

    /* loaded from: classes3.dex */
    public static class FileAttachmentViewHolder extends CarouselViewHolder {
        private final ImageView appIcon;
        private final TextView appName;
        private final View container;
        private final TextView name;
        private final View remove;

        public FileAttachmentViewHolder(LayoutInflater layoutInflater, ViewGroup viewGroup) {
            super(layoutInflater.inflate(b13.zs_request_carousel_file, viewGroup, false));
            this.name = (TextView) this.itemView.findViewById(f03.request_attachment_carousel_file_title);
            this.appIcon = (ImageView) this.itemView.findViewById(f03.request_attachment_carousel_file_icon);
            this.appName = (TextView) this.itemView.findViewById(f03.request_attachment_carousel_file_app_name);
            this.remove = this.itemView.findViewById(f03.request_attachment_carousel_remove);
            this.container = this.itemView.findViewById(f03.request_attachment_file_carousel_container);
        }

        @Override // zendesk.support.request.AdapterAttachmentCarousel.CarouselViewHolder
        public void bind(final StateRequestAttachment stateRequestAttachment, final CarouselViewHolder.OnRemoveListener onRemoveListener) {
            Context context = this.itemView.getContext();
            ResolveInfo appInfoForFile = UtilsAttachment.getAppInfoForFile(context, stateRequestAttachment.getName());
            this.appIcon.setImageDrawable(UtilsAttachment.getAppIcon(context, appInfoForFile));
            this.appName.setText(UtilsAttachment.getAppName(context, appInfoForFile));
            this.name.setText(stateRequestAttachment.getName());
            this.remove.setOnClickListener(new View.OnClickListener() { // from class: zendesk.support.request.AdapterAttachmentCarousel.FileAttachmentViewHolder.1
                @Override // android.view.View.OnClickListener
                public void onClick(View view) {
                    onRemoveListener.onRemove(stateRequestAttachment);
                }
            });
            this.remove.setContentDescription(context.getString(i13.zs_request_attachment_carousel_remove_attachment_accessibility, stateRequestAttachment.getName()));
            this.container.setContentDescription(context.getString(i13.zs_request_attachment_carousel_attachment_accessibility, stateRequestAttachment.getName()));
        }
    }

    /* loaded from: classes3.dex */
    public static class ImageAttachmentViewHolder extends CarouselViewHolder {
        private final View container;
        private final ImageView imageView;
        private final Picasso picasso;
        private final View remove;

        public ImageAttachmentViewHolder(LayoutInflater layoutInflater, ViewGroup viewGroup, Picasso picasso) {
            super(layoutInflater.inflate(b13.zs_request_carousel_image, viewGroup, false));
            this.imageView = (ImageView) this.itemView.findViewById(f03.request_attachment_carousel_image);
            this.remove = this.itemView.findViewById(f03.request_attachment_carousel_remove);
            this.container = this.itemView.findViewById(f03.request_attachment_image_carousel_container);
            this.picasso = picasso;
        }

        @Override // zendesk.support.request.AdapterAttachmentCarousel.CarouselViewHolder
        public void bind(final StateRequestAttachment stateRequestAttachment, final CarouselViewHolder.OnRemoveListener onRemoveListener) {
            this.picasso.j(stateRequestAttachment.getParsedLocalUri()).d().a().f(this.imageView);
            this.remove.setOnClickListener(new View.OnClickListener() { // from class: zendesk.support.request.AdapterAttachmentCarousel.ImageAttachmentViewHolder.1
                @Override // android.view.View.OnClickListener
                public void onClick(View view) {
                    onRemoveListener.onRemove(stateRequestAttachment);
                }
            });
            Context context = this.itemView.getContext();
            this.remove.setContentDescription(context.getString(i13.zs_request_attachment_carousel_remove_attachment_accessibility, stateRequestAttachment.getName()));
            this.container.setContentDescription(context.getString(i13.zs_request_attachment_carousel_attachment_accessibility, stateRequestAttachment.getName()));
        }
    }

    public AdapterAttachmentCarousel(AttachmentHelper attachmentHelper, Picasso picasso, ActionFactory actionFactory, Dispatcher dispatcher) {
        this.attachmentHelper = attachmentHelper;
        this.picasso = picasso;
        this.af = actionFactory;
        this.dispatcher = dispatcher;
        setHasStableIds(true);
    }

    @Override // androidx.recyclerview.widget.RecyclerView.Adapter
    public int getItemCount() {
        return this.attachmentHelper.getSelectedAttachments().size();
    }

    @Override // androidx.recyclerview.widget.RecyclerView.Adapter
    public long getItemId(int i) {
        return this.attachmentHelper.getSelectedAttachments().get(i).getLocalUri().hashCode();
    }

    @Override // androidx.recyclerview.widget.RecyclerView.Adapter
    public int getItemViewType(int i) {
        return UtilsAttachment.isImageAttachment(this.attachmentHelper.getSelectedAttachments().get(i)) ? 2 : 1;
    }

    @Override // androidx.recyclerview.widget.RecyclerView.Adapter
    public void onBindViewHolder(CarouselViewHolder carouselViewHolder, int i) {
        carouselViewHolder.bind(this.attachmentHelper.getSelectedAttachments().get(i), this.removeListener);
    }

    @Override // androidx.recyclerview.widget.RecyclerView.Adapter
    public CarouselViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        LayoutInflater from = LayoutInflater.from(viewGroup.getContext());
        if (i != 1) {
            if (i != 2) {
                return null;
            }
            return new ImageAttachmentViewHolder(from, viewGroup, this.picasso);
        }
        return new FileAttachmentViewHolder(from, viewGroup);
    }
}
