package zendesk.support.request;

import java.util.concurrent.ExecutorService;
import okhttp3.OkHttpClient;

/* loaded from: classes3.dex */
public final class RequestModule_ProvidesAttachmentToDiskServiceFactory implements y11<AttachmentDownloadService> {
    private final ew2<ExecutorService> executorProvider;
    private final ew2<OkHttpClient> okHttpClientProvider;

    public RequestModule_ProvidesAttachmentToDiskServiceFactory(ew2<OkHttpClient> ew2Var, ew2<ExecutorService> ew2Var2) {
        this.okHttpClientProvider = ew2Var;
        this.executorProvider = ew2Var2;
    }

    public static RequestModule_ProvidesAttachmentToDiskServiceFactory create(ew2<OkHttpClient> ew2Var, ew2<ExecutorService> ew2Var2) {
        return new RequestModule_ProvidesAttachmentToDiskServiceFactory(ew2Var, ew2Var2);
    }

    public static AttachmentDownloadService providesAttachmentToDiskService(OkHttpClient okHttpClient, ExecutorService executorService) {
        return (AttachmentDownloadService) cu2.f(RequestModule.providesAttachmentToDiskService(okHttpClient, executorService));
    }

    @Override // defpackage.ew2
    public AttachmentDownloadService get() {
        return providesAttachmentToDiskService(this.okHttpClientProvider.get(), this.executorProvider.get());
    }
}
