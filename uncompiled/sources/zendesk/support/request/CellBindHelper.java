package zendesk.support.request;

import android.content.Context;
import android.content.Intent;
import android.content.pm.ResolveInfo;
import android.graphics.Rect;
import android.view.View;
import android.view.animation.AlphaAnimation;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;
import com.github.mikephil.charting.utils.Utils;
import com.squareup.picasso.Picasso;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import zendesk.belvedere.a;
import zendesk.support.request.CellType;
import zendesk.support.suas.Dispatcher;

/* JADX INFO: Access modifiers changed from: package-private */
/* loaded from: classes3.dex */
public class CellBindHelper {
    private static final SimpleDateFormat DATE_FORMAT = new SimpleDateFormat("d MMMM yyyy", Locale.getDefault());
    private final ActionFactory af;
    private final CellAttachmentLoadingUtil attachmentUtil;
    private final Context context;
    private final Dispatcher dispatcher;
    private final String today;
    private final String yesterday;

    public CellBindHelper(Context context, Picasso picasso, ActionFactory actionFactory, Dispatcher dispatcher) {
        this.context = context;
        this.af = actionFactory;
        this.dispatcher = dispatcher;
        this.attachmentUtil = new CellAttachmentLoadingUtil(picasso, context);
        this.today = context.getString(i13.request_message_date_today);
        this.yesterday = context.getString(i13.request_message_date_yesterday);
    }

    private boolean basicCellChecks(CellType.Base base, CellType.Base base2) {
        if (base == base2) {
            return true;
        }
        return base.getPositionType() == base2.getPositionType() && base.getClass().isInstance(base2);
    }

    private int getPixelForDp(int i) {
        if (i != 0) {
            return this.context.getResources().getDimensionPixelOffset(i);
        }
        return 0;
    }

    private boolean nullSafeEquals(Object obj, Object obj2) {
        if (obj == obj2) {
            return true;
        }
        if (obj == null || obj2 == null) {
            return false;
        }
        return obj.equals(obj2);
    }

    /* JADX INFO: Access modifiers changed from: private */
    public void openAttachment(Context context, StateRequestAttachment stateRequestAttachment) {
        Intent f = a.c(context).f(stateRequestAttachment.getParsedLocalUri(), stateRequestAttachment.getMimeType());
        if (context.getPackageManager().queryIntentActivities(f, 0).size() > 0) {
            context.startActivity(f);
        }
    }

    public void addOnClickListenerForFileAttachment(View view, final StateRequestAttachment stateRequestAttachment) {
        if (stateRequestAttachment.isAvailableLocally()) {
            view.setAlpha(1.0f);
            view.setOnClickListener(new View.OnClickListener() { // from class: zendesk.support.request.CellBindHelper.1
                @Override // android.view.View.OnClickListener
                public void onClick(View view2) {
                    CellBindHelper.this.openAttachment(view2.getContext(), stateRequestAttachment);
                }
            });
            return;
        }
        view.setAlpha(this.context.getResources().getInteger(s03.zs_request_file_attachment_downloading_cell_alpha) / 100.0f);
        view.setOnClickListener(new View.OnClickListener() { // from class: zendesk.support.request.CellBindHelper.2
            private final String toastMessage;

            {
                this.toastMessage = CellBindHelper.this.context.getString(i13.request_file_attachment_download_in_progress);
            }

            @Override // android.view.View.OnClickListener
            public void onClick(View view2) {
                Toast.makeText(view2.getContext(), this.toastMessage, 0).show();
            }
        });
    }

    public void addOnClickListenerForImageAttachment(View view, final StateRequestAttachment stateRequestAttachment) {
        if (stateRequestAttachment.isAvailableLocally()) {
            view.setOnClickListener(new View.OnClickListener() { // from class: zendesk.support.request.CellBindHelper.3
                @Override // android.view.View.OnClickListener
                public void onClick(View view2) {
                    CellBindHelper.this.openAttachment(view2.getContext(), stateRequestAttachment);
                }
            });
        } else {
            view.setOnClickListener(null);
        }
    }

    public boolean areAgentCellContentsTheSame(CellType.Agent agent, CellType.Base base) {
        if (basicCellChecks(agent, base) && (base instanceof CellType.Agent)) {
            CellType.Agent agent2 = (CellType.Agent) base;
            return ((agent.getAgent().getId() > agent2.getAgent().getId() ? 1 : (agent.getAgent().getId() == agent2.getAgent().getId() ? 0 : -1)) == 0) && agent.getAgent().getName().equals(agent2.getAgent().getName()) && (agent.isAgentNameVisible() == agent2.isAgentNameVisible());
        }
        return false;
    }

    public boolean areAttachmentCellContentsTheSame(CellType.Attachment attachment, CellType.Base base) {
        if (basicCellChecks(attachment, base) && (base instanceof CellType.Attachment)) {
            StateRequestAttachment attachment2 = attachment.getAttachment();
            StateRequestAttachment attachment3 = ((CellType.Attachment) base).getAttachment();
            return nullSafeEquals(attachment2.getLocalFile(), attachment3.getLocalFile()) && nullSafeEquals(attachment2.getLocalUri(), attachment3.getLocalUri()) && nullSafeEquals(attachment2.getUrl(), attachment3.getUrl());
        }
        return false;
    }

    public boolean areMessageContentsTheSame(CellType.Message message, CellType.Base base) {
        if (basicCellChecks(message, base) && (base instanceof CellType.Message)) {
            return message.getMessage().equals(((CellType.Message) base).getMessage());
        }
        return false;
    }

    public boolean areStatefulCellContentsTheSame(CellType.Stateful stateful, CellType.Base base) {
        if (basicCellChecks(stateful, base) && (base instanceof CellType.Stateful)) {
            CellType.Stateful stateful2 = (CellType.Stateful) base;
            return (stateful.isErrorShown() == stateful2.isErrorShown()) && (stateful.isMarkedAsDelivered() == stateful2.isMarkedAsDelivered()) && (stateful.getErrorGroupMessages().size() == stateful2.getErrorGroupMessages().size()) && (stateful.isLastErrorCellOfBlock() == stateful2.isLastErrorCellOfBlock());
        }
        return false;
    }

    public void bindAgentName(TextView textView, boolean z, StateRequestUser stateRequestUser) {
        if (z) {
            textView.setVisibility(0);
            textView.setText(stateRequestUser.getName());
            return;
        }
        textView.setVisibility(4);
    }

    public void bindAppInfo(ResolveInfo resolveInfo, TextView textView, ImageView imageView) {
        textView.setText(UtilsAttachment.getAppName(this.context, resolveInfo));
        imageView.setImageDrawable(UtilsAttachment.getAppIcon(this.context, resolveInfo));
    }

    public void bindDate(TextView textView, Date date) {
        String format;
        if (UtilsDate.isToday(date)) {
            format = this.today;
        } else if (UtilsDate.isYesterday(date)) {
            format = this.yesterday;
        } else {
            format = DATE_FORMAT.format(date);
        }
        textView.setText(format.toUpperCase(Locale.getDefault()));
    }

    public void bindImage(ImageView imageView, StateRequestAttachment stateRequestAttachment) {
        this.attachmentUtil.bindImage(imageView, stateRequestAttachment);
    }

    public void bindStatusLabel(TextView textView, boolean z, boolean z2) {
        int i;
        int i2 = -1;
        int i3 = 0;
        if (z) {
            i2 = vy2.zs_request_cell_label_color_error;
            i = i13.request_messages_status_error;
        } else if (z2) {
            i2 = vy2.zs_request_cell_label_color;
            i = i13.request_message_status_delivered;
        } else {
            i3 = 4;
            i = -1;
        }
        if (i2 > 0) {
            textView.setTextColor(m70.d(this.context, i2));
        }
        if (i > 0) {
            textView.setText(i);
        }
        textView.clearAnimation();
        if (i3 == 0 && i3 != textView.getVisibility()) {
            AlphaAnimation alphaAnimation = new AlphaAnimation((float) Utils.FLOAT_EPSILON, 1.0f);
            alphaAnimation.setDuration(250L);
            alphaAnimation.setInterpolator(zp2.a(Utils.FLOAT_EPSILON, Utils.FLOAT_EPSILON, 0.2f, 1.0f));
            textView.startAnimation(alphaAnimation);
        }
        textView.setVisibility(i3);
    }

    public int colorForError(boolean z) {
        int i;
        if (z) {
            i = vy2.zs_request_user_background_color_error;
        } else {
            i = vy2.zs_request_user_background_color;
        }
        return m70.d(this.context, i);
    }

    public int colorForErrorImage(boolean z) {
        if (z) {
            return m70.d(this.context, vy2.zs_request_user_background_image_color_error);
        }
        return 0;
    }

    public View.OnClickListener errorClickListener(boolean z, final List<StateMessage> list) {
        if (z) {
            return new View.OnClickListener() { // from class: zendesk.support.request.CellBindHelper.4
                @Override // android.view.View.OnClickListener
                public void onClick(View view) {
                    CellBindHelper.this.dispatcher.dispatch(CellBindHelper.this.af.showRetryDialog(list));
                }
            };
        }
        return null;
    }

    public ResolveInfo getAppInfo(String str) {
        return UtilsAttachment.getAppInfoForFile(this.context, str);
    }

    public Rect getInsets(int i, int i2, int i3, int i4) {
        return new Rect(getPixelForDp(i), getPixelForDp(i2), getPixelForDp(i3), getPixelForDp(i4));
    }
}
