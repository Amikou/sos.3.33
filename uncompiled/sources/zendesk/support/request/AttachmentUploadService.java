package zendesk.support.request;

import android.annotation.SuppressLint;
import android.net.Uri;
import com.zendesk.logger.Logger;
import java.io.File;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import zendesk.belvedere.MediaResult;
import zendesk.belvedere.a;
import zendesk.support.Attachment;
import zendesk.support.UploadProvider;
import zendesk.support.UploadResponse;

/* JADX INFO: Access modifiers changed from: package-private */
/* loaded from: classes3.dex */
public class AttachmentUploadService {
    private final a belvedere;
    private final List<StateRequestAttachment> errorItems;
    private final List<StateRequestAttachment> itemsForUpload;
    private final List<StateRequestAttachment> processedItems;
    private List<ResolveCallback> resolveCallbacks;
    private rs4<AttachmentUploadResult> resultListener;
    private final UploadProvider uploadProvider;
    private final Object lock = new Object();
    private String subDirectory = UtilsAttachment.getTemporaryRequestCacheDir();
    private final Map<Long, Long> localToRemoteMap = new HashMap();

    /* loaded from: classes3.dex */
    public static class AttachmentUploadResult {
        private final Map<Long, Long> localToRemoteIdMap;
        private final List<StateRequestAttachment> requestAttachments;

        public AttachmentUploadResult(List<StateRequestAttachment> list, Map<Long, Long> map) {
            this.requestAttachments = list;
            this.localToRemoteIdMap = map;
        }

        public Map<Long, Long> getLocalToRemoteIdMap() {
            return this.localToRemoteIdMap;
        }

        public List<StateRequestAttachment> getRequestAttachments() {
            return this.requestAttachments;
        }
    }

    /* loaded from: classes3.dex */
    public class AttachmentsCallback extends rs4<UploadResponse> {
        private final StateRequestAttachment requestAttachment;

        public AttachmentsCallback(StateRequestAttachment stateRequestAttachment) {
            this.requestAttachment = stateRequestAttachment;
        }

        @Override // defpackage.rs4
        public void onError(cw0 cw0Var) {
            Logger.k(RequestActivity.LOG_TAG, "Error uploading file: %s | Error: %s", this.requestAttachment, cw0Var.h());
            AttachmentUploadService.this.errorUpload(this.requestAttachment);
        }

        @Override // defpackage.rs4
        public void onSuccess(UploadResponse uploadResponse) {
            Logger.b(RequestActivity.LOG_TAG, "Successfully uploaded file: %s | Result: %s", this.requestAttachment, uploadResponse);
            AttachmentUploadService.this.localToRemoteMap.put(Long.valueOf(this.requestAttachment.getId()), uploadResponse.getAttachment().getId());
            AttachmentUploadService.this.uploadSuccess(this.requestAttachment, uploadResponse);
        }
    }

    /* loaded from: classes3.dex */
    public class ResolveCallback extends uu<List<MediaResult>> {
        private final StateRequestAttachment requestAttachment;

        private ResolveCallback(StateRequestAttachment stateRequestAttachment) {
            this.requestAttachment = stateRequestAttachment;
        }

        @Override // defpackage.uu
        public void success(List<MediaResult> list) {
            Uri parsedLocalUri = this.requestAttachment.getParsedLocalUri();
            if (list.size() > 0 && !AttachmentUploadService.this.isUploadFinished()) {
                Logger.k(RequestActivity.LOG_TAG, "Successfully resolved attachment: %s", parsedLocalUri);
                StateRequestAttachment updateRequestAttachment = AttachmentUploadService.this.updateRequestAttachment(this.requestAttachment, list.get(0));
                AttachmentUploadService.this.uploadProvider.uploadAttachment(updateRequestAttachment.getName(), updateRequestAttachment.getLocalFile(), updateRequestAttachment.getMimeType(), new AttachmentsCallback(updateRequestAttachment));
                return;
            }
            Logger.k(RequestActivity.LOG_TAG, "Unable to resolve attachment: %s", parsedLocalUri);
            AttachmentUploadService.this.errorUpload(this.requestAttachment);
        }
    }

    @SuppressLint({"UseSparseArrays"})
    public AttachmentUploadService(UploadProvider uploadProvider, a aVar, List<StateRequestAttachment> list) {
        this.uploadProvider = uploadProvider;
        this.belvedere = aVar;
        this.itemsForUpload = list;
        this.resolveCallbacks = new ArrayList(list.size());
        this.processedItems = new ArrayList(list.size());
        this.errorItems = new ArrayList(list.size());
    }

    /* JADX INFO: Access modifiers changed from: private */
    public void errorUpload(StateRequestAttachment stateRequestAttachment) {
        synchronized (this.lock) {
            this.errorItems.add(stateRequestAttachment);
        }
        notifyIfFinished();
    }

    /* JADX INFO: Access modifiers changed from: private */
    public boolean isUploadFinished() {
        boolean z;
        synchronized (this.lock) {
            boolean i = l10.i(this.errorItems);
            z = true;
            boolean z2 = this.processedItems.size() == this.itemsForUpload.size();
            if (!i && !z2) {
                z = false;
            }
        }
        return z;
    }

    private void notifyIfFinished() {
        Logger.b(RequestActivity.LOG_TAG, "Notify if finished. Listener: %s, isUploadFinished: %s", this.resultListener, Boolean.valueOf(isUploadFinished()));
        if (!isUploadFinished() || this.resultListener == null) {
            return;
        }
        if (l10.g(this.errorItems)) {
            this.resultListener.onSuccess(new AttachmentUploadResult(l10.c(this.processedItems), this.localToRemoteMap));
        } else {
            this.resultListener.onError(new dw0("Error uploading attachments."));
        }
        this.resultListener = null;
    }

    private MediaResult renameFile(File file, long j) {
        MediaResult d = this.belvedere.d(UtilsAttachment.getAttachmentSubDir(this.subDirectory, j), file.getName());
        Logger.b(RequestActivity.LOG_TAG, "Rename local file: %s -> %s", file.getAbsolutePath(), d.e().getAbsolutePath());
        if (file.renameTo(d.e())) {
            return d;
        }
        return null;
    }

    /* JADX INFO: Access modifiers changed from: private */
    public StateRequestAttachment updateRequestAttachment(StateRequestAttachment stateRequestAttachment, MediaResult mediaResult) {
        return stateRequestAttachment.newBuilder().setLocalFile(mediaResult.e()).setName(mediaResult.h()).setMimeType(mediaResult.g()).setLocalUri(mediaResult.l().toString()).build();
    }

    private void uploadAttachment(StateRequestAttachment stateRequestAttachment) {
        Uri parsedLocalUri = stateRequestAttachment.getParsedLocalUri();
        if (parsedLocalUri != null && !isUploadFinished()) {
            ResolveCallback resolveCallback = new ResolveCallback(stateRequestAttachment);
            this.resolveCallbacks.add(resolveCallback);
            this.belvedere.h(Collections.singletonList(parsedLocalUri), this.subDirectory, resolveCallback);
            return;
        }
        Logger.k(RequestActivity.LOG_TAG, "Unable to parse uri, skipping. | %s", stateRequestAttachment.getLocalUri());
        errorUpload(stateRequestAttachment);
    }

    /* JADX INFO: Access modifiers changed from: private */
    public void uploadSuccess(StateRequestAttachment stateRequestAttachment, UploadResponse uploadResponse) {
        String localUri;
        File localFile;
        Attachment attachment = uploadResponse.getAttachment();
        MediaResult renameFile = renameFile(stateRequestAttachment.getLocalFile(), attachment.getId().longValue());
        if (renameFile != null) {
            localUri = renameFile.l().toString();
            localFile = renameFile.e();
        } else {
            localUri = stateRequestAttachment.getLocalUri();
            localFile = stateRequestAttachment.getLocalFile();
        }
        StateRequestAttachment build = stateRequestAttachment.newBuilder().setLocalUri(localUri).setLocalFile(localFile).setToken(uploadResponse.getToken()).setUrl(attachment.getContentUrl()).setMimeType(attachment.getContentType()).setName(attachment.getFileName()).build();
        synchronized (this.lock) {
            this.processedItems.add(build);
        }
        notifyIfFinished();
    }

    public void setResultListener(rs4<AttachmentUploadResult> rs4Var) {
        this.resultListener = rs4Var;
        notifyIfFinished();
    }

    public void start(String str) {
        if (ru3.b(str)) {
            this.subDirectory = UtilsAttachment.getCacheDirForRequestId(str);
        }
        Logger.b(RequestActivity.LOG_TAG, "Start uploading attachments", new Object[0]);
        for (StateRequestAttachment stateRequestAttachment : this.itemsForUpload) {
            uploadAttachment(stateRequestAttachment);
        }
    }
}
