package zendesk.support.request;

import java.io.Serializable;

/* JADX INFO: Access modifiers changed from: package-private */
/* loaded from: classes3.dex */
public class StateMessageStatus implements Serializable {
    public static final int DELIVERED = 2;
    public static final int ERROR = 1;
    public static final int PENDING = 3;
    private final String errorResponse;
    private final int status;

    private StateMessageStatus(int i, String str) {
        this.status = i;
        this.errorResponse = str;
    }

    public static StateMessageStatus delivered() {
        return new StateMessageStatus(2, null);
    }

    public static StateMessageStatus error(String str) {
        return new StateMessageStatus(1, str);
    }

    public static StateMessageStatus pending() {
        return new StateMessageStatus(3, null);
    }

    public String getErrorResponse() {
        return this.errorResponse;
    }

    public int getStatus() {
        return this.status;
    }

    public String toString() {
        int i = this.status;
        String str = i != 1 ? i != 2 ? i != 3 ? "Unknown" : "Pending" : "Delivered" : "Error";
        return "MessageState{status=" + str + ", errorResponse=" + this.errorResponse + '}';
    }
}
