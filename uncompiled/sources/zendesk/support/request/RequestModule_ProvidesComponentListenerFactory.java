package zendesk.support.request;

/* loaded from: classes3.dex */
public final class RequestModule_ProvidesComponentListenerFactory implements y11<HeadlessComponentListener> {
    private final ew2<AttachmentDownloaderComponent> attachmentDownloaderProvider;
    private final ew2<ComponentPersistence> persistenceProvider;
    private final ew2<ComponentUpdateActionHandlers> updatesComponentProvider;

    public RequestModule_ProvidesComponentListenerFactory(ew2<ComponentPersistence> ew2Var, ew2<AttachmentDownloaderComponent> ew2Var2, ew2<ComponentUpdateActionHandlers> ew2Var3) {
        this.persistenceProvider = ew2Var;
        this.attachmentDownloaderProvider = ew2Var2;
        this.updatesComponentProvider = ew2Var3;
    }

    public static RequestModule_ProvidesComponentListenerFactory create(ew2<ComponentPersistence> ew2Var, ew2<AttachmentDownloaderComponent> ew2Var2, ew2<ComponentUpdateActionHandlers> ew2Var3) {
        return new RequestModule_ProvidesComponentListenerFactory(ew2Var, ew2Var2, ew2Var3);
    }

    public static HeadlessComponentListener providesComponentListener(Object obj, Object obj2, Object obj3) {
        return (HeadlessComponentListener) cu2.f(RequestModule.providesComponentListener((ComponentPersistence) obj, (AttachmentDownloaderComponent) obj2, (ComponentUpdateActionHandlers) obj3));
    }

    @Override // defpackage.ew2
    public HeadlessComponentListener get() {
        return providesComponentListener(this.persistenceProvider.get(), this.attachmentDownloaderProvider.get(), this.updatesComponentProvider.get());
    }
}
