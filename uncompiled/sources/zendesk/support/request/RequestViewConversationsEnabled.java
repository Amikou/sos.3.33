package zendesk.support.request;

import android.annotation.SuppressLint;
import android.content.Context;
import android.util.AttributeSet;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.animation.Interpolator;
import android.widget.FrameLayout;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.recyclerview.widget.f;
import androidx.recyclerview.widget.m;
import com.github.mikephil.charting.utils.Utils;
import com.squareup.picasso.Picasso;
import zendesk.belvedere.BelvedereUi;
import zendesk.belvedere.b;
import zendesk.support.request.CellType;
import zendesk.support.request.ComponentRequestAdapter;
import zendesk.support.request.ViewMessageComposer;
import zendesk.support.suas.CombinedSubscription;
import zendesk.support.suas.Store;
import zendesk.support.suas.Subscription;

@SuppressLint({"ViewConstructor"})
/* loaded from: classes3.dex */
public class RequestViewConversationsEnabled extends FrameLayout implements RequestView {
    private AppCompatActivity activity;
    public ActionFactory af;
    public CellFactory cellFactory;
    private ImagePickerDragAnimation imagePickerDragAnimation;
    private b imageStream;
    private ComponentMessageComposer messageComposerComponent;
    private ViewMessageComposer messageComposerView;
    public Picasso picasso;
    private RecyclerView recyclerView;
    public Store store;
    private Subscription subscription;
    private View toolbar;
    private View toolbarContainer;

    /* loaded from: classes3.dex */
    public static class ImagePickerDragAnimation implements b.c {
        private final Interpolator cubicBezierInterpolator = zp2.a(0.19f, Utils.FLOAT_EPSILON, 0.2f, 1.0f);
        private final View messageComposer;
        private final View recycler;
        private final View toolbar;
        private final View toolbarContainer;

        public ImagePickerDragAnimation(View view, View view2, View view3, View view4) {
            this.toolbarContainer = view;
            this.messageComposer = view2;
            this.recycler = view3;
            this.toolbar = view4;
        }

        private void animateBackground(int i, float f) {
            float interpolation = (int) (this.cubicBezierInterpolator.getInterpolation(f * 0.3f) * (-1.0f) * i);
            this.messageComposer.setTranslationY(interpolation);
            this.recycler.setTranslationY(interpolation);
        }

        private void animateToolbar(int i, float f) {
            float f2 = i;
            float f3 = f * f2;
            int F = ei4.F(this.toolbar);
            if (i > 0) {
                float f4 = f2 - f3;
                float f5 = F;
                if (f4 < f5) {
                    this.toolbarContainer.setTranslationY(f4 - f5);
                    return;
                }
            }
            this.toolbarContainer.setTranslationY(Utils.FLOAT_EPSILON);
        }

        @Override // zendesk.belvedere.b.c
        public void onScroll(int i, int i2, float f) {
            animateToolbar(i2, f);
            animateBackground(i2, f);
        }
    }

    /* loaded from: classes3.dex */
    public static class RecyclerListener implements ViewMessageComposer.OnHeightChangeListener, View.OnFocusChangeListener, View.OnLayoutChangeListener, i02 {
        private static final int FIXED_SCROLL_TIME = 50;
        private static final int SCROLL_INSTANT = 1;
        private static final int SCROLL_SMOOTH_FIXED_TIME = 3;
        private static final int SCROLL_SMOOTH_FIXED_VELOCITY = 2;
        private final LinearLayoutManager linearLayoutManager;
        private final int recyclerDefaultBottomPadding;
        private final RecyclerView recyclerView;

        public RecyclerListener(RecyclerView recyclerView, LinearLayoutManager linearLayoutManager) {
            this.recyclerView = recyclerView;
            this.linearLayoutManager = linearLayoutManager;
            this.recyclerDefaultBottomPadding = recyclerView.getResources().getDimensionPixelOffset(yy2.zs_request_recycler_padding_bottom);
        }

        private void postScrollToBottom(final int i) {
            this.recyclerView.post(new Runnable() { // from class: zendesk.support.request.RequestViewConversationsEnabled.RecyclerListener.3
                @Override // java.lang.Runnable
                public void run() {
                    RecyclerListener.this.scrollToBottom(i);
                }
            });
        }

        /* JADX INFO: Access modifiers changed from: private */
        public void scrollToBottom(int i) {
            View view;
            int itemCount = this.recyclerView.getAdapter().getItemCount() - 1;
            if (itemCount >= 0) {
                if (i == 1) {
                    RecyclerView.a0 Z = this.recyclerView.Z(itemCount);
                    this.linearLayoutManager.I2(itemCount, (this.recyclerView.getPaddingBottom() + ((Z == null || (view = Z.itemView) == null) ? 0 : view.getHeight())) * (-1));
                } else if (i == 3) {
                    m mVar = new m(this.recyclerView.getContext()) { // from class: zendesk.support.request.RequestViewConversationsEnabled.RecyclerListener.2
                        @Override // androidx.recyclerview.widget.m
                        public int calculateTimeForScrolling(int i2) {
                            return 50;
                        }
                    };
                    mVar.setTargetPosition(itemCount);
                    this.recyclerView.getLayoutManager().S1(mVar);
                } else if (i == 2) {
                    m mVar2 = new m(this.recyclerView.getContext());
                    mVar2.setTargetPosition(itemCount);
                    this.recyclerView.getLayoutManager().S1(mVar2);
                }
            }
        }

        @Override // defpackage.i02
        public void onChanged(int i, int i2, Object obj) {
            this.recyclerView.getAdapter().notifyItemRangeChanged(i, i2, obj);
        }

        @Override // android.view.View.OnFocusChangeListener
        public void onFocusChange(View view, boolean z) {
            if (z) {
                postScrollToBottom(2);
            }
        }

        @Override // zendesk.support.request.ViewMessageComposer.OnHeightChangeListener
        public void onHeightChange(final int i) {
            this.recyclerView.post(new Runnable() { // from class: zendesk.support.request.RequestViewConversationsEnabled.RecyclerListener.1
                @Override // java.lang.Runnable
                public void run() {
                    int paddingLeft = RecyclerListener.this.recyclerView.getPaddingLeft();
                    int paddingRight = RecyclerListener.this.recyclerView.getPaddingRight();
                    int paddingTop = RecyclerListener.this.recyclerView.getPaddingTop();
                    int i2 = RecyclerListener.this.recyclerDefaultBottomPadding;
                    int i3 = i;
                    if (i3 > 0) {
                        i2 += i3;
                    }
                    if (i2 != RecyclerListener.this.recyclerView.getPaddingBottom()) {
                        RecyclerListener.this.recyclerView.setPadding(paddingLeft, paddingTop, paddingRight, i2);
                        RecyclerListener.this.scrollToBottom(1);
                    }
                }
            });
        }

        @Override // defpackage.i02
        public void onInserted(int i, int i2) {
            this.recyclerView.getAdapter().notifyItemRangeChanged(i, i2);
            postScrollToBottom(3);
        }

        @Override // android.view.View.OnLayoutChangeListener
        public void onLayoutChange(View view, int i, int i2, int i3, int i4, int i5, int i6, int i7, int i8) {
            if (i4 < i8) {
                postScrollToBottom(1);
            }
        }

        @Override // defpackage.i02
        public void onMoved(int i, int i2) {
            this.recyclerView.getAdapter().notifyItemMoved(i, i2);
        }

        @Override // defpackage.i02
        public void onRemoved(int i, int i2) {
            this.recyclerView.getAdapter().notifyItemRangeRemoved(i, i2);
        }
    }

    /* loaded from: classes3.dex */
    public static class RequestItemAnimator extends f {
        private final ComponentRequestAdapter component;

        public RequestItemAnimator(ComponentRequestAdapter componentRequestAdapter) {
            this.component = componentRequestAdapter;
            setSupportsChangeAnimations(false);
        }

        @Override // androidx.recyclerview.widget.u, androidx.recyclerview.widget.RecyclerView.l
        public boolean canReuseUpdatedViewHolder(RecyclerView.a0 a0Var) {
            if (this.component.getMessageForPos(a0Var.getAdapterPosition()) instanceof CellType.Attachment) {
                return true;
            }
            return super.canReuseUpdatedViewHolder(a0Var);
        }
    }

    public RequestViewConversationsEnabled(Context context) {
        super(context);
        viewInit(context);
    }

    private Subscription bindComponents(Store store) {
        return CombinedSubscription.from(bindMessageComposer(store), bindRecycler(store), bindDialogComponent(store));
    }

    private Subscription bindDialogComponent(Store store) {
        return store.addListener(StateUi.class, new ComponentDialog(this.activity, this.af, store));
    }

    private Subscription bindMessageComposer(Store store) {
        ComponentMessageComposer componentMessageComposer = new ComponentMessageComposer(this.activity, this.imageStream, this.messageComposerView, store, this.af);
        this.messageComposerComponent = componentMessageComposer;
        return store.addListener(componentMessageComposer.getSelector(), this.messageComposerComponent);
    }

    private Subscription bindRecycler(Store store) {
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(this.activity);
        RecyclerListener recyclerListener = new RecyclerListener(this.recyclerView, linearLayoutManager);
        ComponentRequestAdapter componentRequestAdapter = new ComponentRequestAdapter(recyclerListener, this.cellFactory, this.recyclerView);
        CellMarginDecorator cellMarginDecorator = new CellMarginDecorator(componentRequestAdapter, this.activity);
        RequestItemAnimator requestItemAnimator = new RequestItemAnimator(componentRequestAdapter);
        ComponentRequestAdapter.RequestAdapter requestAdapter = new ComponentRequestAdapter.RequestAdapter(componentRequestAdapter);
        this.recyclerView.setItemAnimator(requestItemAnimator);
        this.recyclerView.setLayoutManager(linearLayoutManager);
        this.recyclerView.h(cellMarginDecorator);
        this.recyclerView.setAdapter(requestAdapter);
        this.recyclerView.setNestedScrollingEnabled(false);
        this.messageComposerView.setOnHeightChangeListener(recyclerListener);
        this.messageComposerView.addOnFocusChangeListener(recyclerListener);
        this.recyclerView.addOnLayoutChangeListener(recyclerListener);
        return store.addListener(componentRequestAdapter.getSelector(), componentRequestAdapter);
    }

    private void bindViews() {
        this.imageStream = BelvedereUi.b(this.activity);
        this.recyclerView = (RecyclerView) findViewById(f03.activity_request_recycler_view);
        this.messageComposerView = (ViewMessageComposer) findViewById(f03.activity_request_message_composer);
        this.toolbarContainer = this.activity.findViewById(f03.activity_request_appbar);
        this.toolbar = this.activity.findViewById(f03.activity_request_toolbar);
        this.messageComposerView.init(this.imageStream);
        installDragAnimation();
    }

    private void installDragAnimation() {
        ImagePickerDragAnimation imagePickerDragAnimation = new ImagePickerDragAnimation(this.toolbarContainer, this.messageComposerView, this.recyclerView, this.toolbar);
        this.imagePickerDragAnimation = imagePickerDragAnimation;
        this.imageStream.g(imagePickerDragAnimation);
    }

    private void viewInit(Context context) {
        FrameLayout.inflate(context, b13.zs_view_request_conversations_enabled, this);
        this.activity = (AppCompatActivity) context;
    }

    @Override // zendesk.support.request.RequestView
    public boolean hasUnsavedInput() {
        ComponentMessageComposer componentMessageComposer = this.messageComposerComponent;
        return componentMessageComposer != null && componentMessageComposer.hasUnsavedInput();
    }

    @Override // zendesk.support.request.RequestView
    public boolean inflateMenu(MenuInflater menuInflater, Menu menu) {
        return false;
    }

    public void init(RequestComponent requestComponent, boolean z) {
        requestComponent.inject(this);
        bindViews();
        Subscription bindComponents = bindComponents(this.store);
        this.subscription = bindComponents;
        bindComponents.informWithCurrentState();
        if (z) {
            this.store.dispatch(this.af.loadCommentsFromCacheAsync());
            this.store.dispatch(this.af.loadRequestAsync());
            this.store.dispatch(this.af.initialLoadCommentsAsync());
            this.messageComposerView.requestFocusForInput();
        }
    }

    @Override // android.view.ViewGroup, android.view.View
    public void onDetachedFromWindow() {
        super.onDetachedFromWindow();
        Subscription subscription = this.subscription;
        if (subscription != null) {
            subscription.removeListener();
        }
    }

    @Override // zendesk.support.request.RequestView
    public boolean onOptionsItemClicked(MenuItem menuItem) {
        return false;
    }

    public RequestViewConversationsEnabled(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        viewInit(context);
    }

    public RequestViewConversationsEnabled(Context context, AttributeSet attributeSet, int i) {
        super(context, attributeSet, i);
        viewInit(context);
    }
}
