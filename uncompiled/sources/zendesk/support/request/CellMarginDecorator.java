package zendesk.support.request;

import android.content.Context;
import androidx.recyclerview.widget.RecyclerView;

/* loaded from: classes3.dex */
class CellMarginDecorator extends RecyclerView.n {
    public static final int CELL = 1;
    public static final int CELL_LAST = 16;
    public static final int CELL_START_BLOCK = 2;
    public static final int CELL_WITH_LABEL = 8;
    private final ComponentRequestAdapter dataSource;
    private final int groupVerticalMargin;
    private final int verticalMargin;

    public CellMarginDecorator(ComponentRequestAdapter componentRequestAdapter, Context context) {
        this.dataSource = componentRequestAdapter;
        this.verticalMargin = context.getResources().getDimensionPixelOffset(yy2.zs_request_message_margin_vertical);
        this.groupVerticalMargin = context.getResources().getDimensionPixelOffset(yy2.zs_request_message_group_margin_vertical);
    }

    /* JADX WARN: Removed duplicated region for block: B:35:0x0074  */
    @Override // androidx.recyclerview.widget.RecyclerView.n
    /*
        Code decompiled incorrectly, please refer to instructions dump.
        To view partially-correct code enable 'Show inconsistent code' option in preferences
    */
    public void getItemOffsets(android.graphics.Rect r10, android.view.View r11, androidx.recyclerview.widget.RecyclerView r12, androidx.recyclerview.widget.RecyclerView.x r13) {
        /*
            r9 = this;
            int r11 = r12.f0(r11)
            r12 = -1
            if (r11 != r12) goto L8
            return
        L8:
            zendesk.support.request.ComponentRequestAdapter r12 = r9.dataSource
            zendesk.support.request.CellType$Base r11 = r12.getMessageForPos(r11)
            int r12 = r11.getPositionType()
            android.graphics.Rect r11 = r11.getInsets()
            r13 = r12 & 2
            r0 = 2
            r1 = 0
            r2 = 1
            if (r13 != r0) goto L1f
            r13 = r2
            goto L20
        L1f:
            r13 = r1
        L20:
            r0 = r12 & 8
            r3 = 8
            if (r0 != r3) goto L28
            r0 = r2
            goto L29
        L28:
            r0 = r1
        L29:
            r3 = r12 & 16
            r4 = 16
            if (r3 != r4) goto L31
            r3 = r2
            goto L32
        L31:
            r3 = r1
        L32:
            r4 = r12 & 1
            if (r4 != r2) goto L38
            r4 = r2
            goto L39
        L38:
            r4 = r1
        L39:
            int r5 = r11.left
            int r5 = -r5
            int r6 = r11.top
            int r6 = -r6
            int r7 = r11.right
            int r7 = -r7
            int r8 = r11.bottom
            int r8 = -r8
            if (r13 == 0) goto L4d
            if (r0 == 0) goto L4d
            int r12 = r9.groupVerticalMargin
        L4b:
            int r6 = r6 + r12
            goto L54
        L4d:
            if (r13 == 0) goto L56
            int r12 = r9.groupVerticalMargin
            int r6 = r6 + r12
            int r12 = r9.verticalMargin
        L54:
            int r8 = r8 + r12
            goto L72
        L56:
            if (r0 == 0) goto L5e
            int r12 = r9.verticalMargin
            int r6 = r6 + r12
            int r12 = r9.groupVerticalMargin
            goto L54
        L5e:
            if (r4 == 0) goto L63
            int r12 = r9.verticalMargin
            goto L4b
        L63:
            java.lang.Object[] r13 = new java.lang.Object[r2]
            java.lang.Integer r12 = java.lang.Integer.valueOf(r12)
            r13[r1] = r12
            java.lang.String r12 = "RequestActivity"
            java.lang.String r0 = "Unknown position type: %s"
            com.zendesk.logger.Logger.b(r12, r0, r13)
        L72:
            if (r3 == 0) goto L77
            int r11 = r11.bottom
            int r8 = -r11
        L77:
            r10.set(r5, r6, r7, r8)
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: zendesk.support.request.CellMarginDecorator.getItemOffsets(android.graphics.Rect, android.view.View, androidx.recyclerview.widget.RecyclerView, androidx.recyclerview.widget.RecyclerView$x):void");
    }

    public CellMarginDecorator(ComponentRequestAdapter componentRequestAdapter, int i, int i2) {
        this.dataSource = componentRequestAdapter;
        this.verticalMargin = i;
        this.groupVerticalMargin = i2;
    }
}
