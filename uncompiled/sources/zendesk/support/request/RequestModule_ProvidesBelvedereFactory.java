package zendesk.support.request;

import android.content.Context;
import zendesk.belvedere.a;

/* loaded from: classes3.dex */
public final class RequestModule_ProvidesBelvedereFactory implements y11<a> {
    private final ew2<Context> contextProvider;

    public RequestModule_ProvidesBelvedereFactory(ew2<Context> ew2Var) {
        this.contextProvider = ew2Var;
    }

    public static RequestModule_ProvidesBelvedereFactory create(ew2<Context> ew2Var) {
        return new RequestModule_ProvidesBelvedereFactory(ew2Var);
    }

    public static a providesBelvedere(Context context) {
        return (a) cu2.f(RequestModule.providesBelvedere(context));
    }

    @Override // defpackage.ew2
    public a get() {
        return providesBelvedere(this.contextProvider.get());
    }
}
