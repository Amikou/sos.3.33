package zendesk.support.request;

import zendesk.belvedere.a;
import zendesk.support.request.AttachmentDownloaderComponent;

/* loaded from: classes3.dex */
public final class RequestModule_ProvidesAttachmentDownloaderFactory implements y11<AttachmentDownloaderComponent.AttachmentDownloader> {
    private final ew2<AttachmentDownloadService> attachmentToDiskServiceProvider;
    private final ew2<a> belvedereProvider;

    public RequestModule_ProvidesAttachmentDownloaderFactory(ew2<a> ew2Var, ew2<AttachmentDownloadService> ew2Var2) {
        this.belvedereProvider = ew2Var;
        this.attachmentToDiskServiceProvider = ew2Var2;
    }

    public static RequestModule_ProvidesAttachmentDownloaderFactory create(ew2<a> ew2Var, ew2<AttachmentDownloadService> ew2Var2) {
        return new RequestModule_ProvidesAttachmentDownloaderFactory(ew2Var, ew2Var2);
    }

    public static AttachmentDownloaderComponent.AttachmentDownloader providesAttachmentDownloader(a aVar, Object obj) {
        return (AttachmentDownloaderComponent.AttachmentDownloader) cu2.f(RequestModule.providesAttachmentDownloader(aVar, (AttachmentDownloadService) obj));
    }

    @Override // defpackage.ew2
    public AttachmentDownloaderComponent.AttachmentDownloader get() {
        return providesAttachmentDownloader(this.belvedereProvider.get(), this.attachmentToDiskServiceProvider.get());
    }
}
