package zendesk.support.request;

import java.util.concurrent.Executor;
import java.util.concurrent.ExecutorService;
import zendesk.belvedere.a;
import zendesk.core.AuthenticationProvider;
import zendesk.support.RequestProvider;
import zendesk.support.SupportBlipsProvider;
import zendesk.support.SupportSettingsProvider;
import zendesk.support.SupportUiStorage;
import zendesk.support.UploadProvider;

/* loaded from: classes3.dex */
public final class RequestModule_ProvidesActionFactoryFactory implements y11<ActionFactory> {
    private final ew2<AuthenticationProvider> authProvider;
    private final ew2<a> belvedereProvider;
    private final ew2<SupportBlipsProvider> blipsProvider;
    private final ew2<ExecutorService> executorProvider;
    private final ew2<Executor> mainThreadExecutorProvider;
    private final ew2<RequestProvider> requestProvider;
    private final ew2<SupportSettingsProvider> settingsProvider;
    private final ew2<SupportUiStorage> supportUiStorageProvider;
    private final ew2<UploadProvider> uploadProvider;

    public RequestModule_ProvidesActionFactoryFactory(ew2<RequestProvider> ew2Var, ew2<SupportSettingsProvider> ew2Var2, ew2<UploadProvider> ew2Var3, ew2<a> ew2Var4, ew2<SupportUiStorage> ew2Var5, ew2<ExecutorService> ew2Var6, ew2<Executor> ew2Var7, ew2<AuthenticationProvider> ew2Var8, ew2<SupportBlipsProvider> ew2Var9) {
        this.requestProvider = ew2Var;
        this.settingsProvider = ew2Var2;
        this.uploadProvider = ew2Var3;
        this.belvedereProvider = ew2Var4;
        this.supportUiStorageProvider = ew2Var5;
        this.executorProvider = ew2Var6;
        this.mainThreadExecutorProvider = ew2Var7;
        this.authProvider = ew2Var8;
        this.blipsProvider = ew2Var9;
    }

    public static RequestModule_ProvidesActionFactoryFactory create(ew2<RequestProvider> ew2Var, ew2<SupportSettingsProvider> ew2Var2, ew2<UploadProvider> ew2Var3, ew2<a> ew2Var4, ew2<SupportUiStorage> ew2Var5, ew2<ExecutorService> ew2Var6, ew2<Executor> ew2Var7, ew2<AuthenticationProvider> ew2Var8, ew2<SupportBlipsProvider> ew2Var9) {
        return new RequestModule_ProvidesActionFactoryFactory(ew2Var, ew2Var2, ew2Var3, ew2Var4, ew2Var5, ew2Var6, ew2Var7, ew2Var8, ew2Var9);
    }

    public static ActionFactory providesActionFactory(RequestProvider requestProvider, SupportSettingsProvider supportSettingsProvider, UploadProvider uploadProvider, a aVar, SupportUiStorage supportUiStorage, ExecutorService executorService, Executor executor, AuthenticationProvider authenticationProvider, SupportBlipsProvider supportBlipsProvider) {
        return (ActionFactory) cu2.f(RequestModule.providesActionFactory(requestProvider, supportSettingsProvider, uploadProvider, aVar, supportUiStorage, executorService, executor, authenticationProvider, supportBlipsProvider));
    }

    @Override // defpackage.ew2
    public ActionFactory get() {
        return providesActionFactory(this.requestProvider.get(), this.settingsProvider.get(), this.uploadProvider.get(), this.belvedereProvider.get(), this.supportUiStorageProvider.get(), this.executorProvider.get(), this.mainThreadExecutorProvider.get(), this.authProvider.get(), this.blipsProvider.get());
    }
}
