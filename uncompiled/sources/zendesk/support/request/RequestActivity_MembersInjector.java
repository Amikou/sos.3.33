package zendesk.support.request;

import com.squareup.picasso.Picasso;
import zendesk.core.ActionHandlerRegistry;
import zendesk.support.suas.Store;

/* loaded from: classes3.dex */
public final class RequestActivity_MembersInjector implements i72<RequestActivity> {
    private final ew2<ActionHandlerRegistry> actionHandlerRegistryProvider;
    private final ew2<ActionFactory> afProvider;
    private final ew2<HeadlessComponentListener> headlessComponentListenerProvider;
    private final ew2<Picasso> picassoProvider;
    private final ew2<Store> storeProvider;

    public RequestActivity_MembersInjector(ew2<Store> ew2Var, ew2<ActionFactory> ew2Var2, ew2<HeadlessComponentListener> ew2Var3, ew2<Picasso> ew2Var4, ew2<ActionHandlerRegistry> ew2Var5) {
        this.storeProvider = ew2Var;
        this.afProvider = ew2Var2;
        this.headlessComponentListenerProvider = ew2Var3;
        this.picassoProvider = ew2Var4;
        this.actionHandlerRegistryProvider = ew2Var5;
    }

    public static i72<RequestActivity> create(ew2<Store> ew2Var, ew2<ActionFactory> ew2Var2, ew2<HeadlessComponentListener> ew2Var3, ew2<Picasso> ew2Var4, ew2<ActionHandlerRegistry> ew2Var5) {
        return new RequestActivity_MembersInjector(ew2Var, ew2Var2, ew2Var3, ew2Var4, ew2Var5);
    }

    public static void injectActionHandlerRegistry(RequestActivity requestActivity, ActionHandlerRegistry actionHandlerRegistry) {
        requestActivity.actionHandlerRegistry = actionHandlerRegistry;
    }

    public static void injectAf(RequestActivity requestActivity, Object obj) {
        requestActivity.af = (ActionFactory) obj;
    }

    public static void injectHeadlessComponentListener(RequestActivity requestActivity, Object obj) {
        requestActivity.headlessComponentListener = (HeadlessComponentListener) obj;
    }

    public static void injectPicasso(RequestActivity requestActivity, Picasso picasso) {
        requestActivity.picasso = picasso;
    }

    public static void injectStore(RequestActivity requestActivity, Store store) {
        requestActivity.store = store;
    }

    public void injectMembers(RequestActivity requestActivity) {
        injectStore(requestActivity, this.storeProvider.get());
        injectAf(requestActivity, this.afProvider.get());
        injectHeadlessComponentListener(requestActivity, this.headlessComponentListenerProvider.get());
        injectPicasso(requestActivity, this.picassoProvider.get());
        injectActionHandlerRegistry(requestActivity, this.actionHandlerRegistryProvider.get());
    }
}
