package zendesk.support.request;

import android.animation.ValueAnimator;
import android.view.View;
import android.widget.FrameLayout;

/* loaded from: classes3.dex */
class UtilsAnimation {
    private UtilsAnimation() {
    }

    public static ValueAnimator bottomPaddingAnimator(View view, int i, int i2, int i3) {
        ValueAnimator ofInt = ValueAnimator.ofInt(i, i2);
        ofInt.addUpdateListener(new ValueAnimator.AnimatorUpdateListener(view, ofInt) { // from class: zendesk.support.request.UtilsAnimation.2
            public final int paddingLeft;
            public final int paddingRight;
            public final int paddingTop;
            public final /* synthetic */ ValueAnimator val$valueAnimator;
            public final /* synthetic */ View val$view;

            {
                this.val$view = view;
                this.val$valueAnimator = ofInt;
                this.paddingLeft = view.getPaddingLeft();
                this.paddingRight = view.getPaddingRight();
                this.paddingTop = view.getPaddingTop();
            }

            @Override // android.animation.ValueAnimator.AnimatorUpdateListener
            public void onAnimationUpdate(ValueAnimator valueAnimator) {
                this.val$view.setPadding(this.paddingLeft, this.paddingTop, this.paddingRight, ((Integer) this.val$valueAnimator.getAnimatedValue()).intValue());
            }
        });
        ofInt.setDuration(i3);
        return ofInt;
    }

    public static ValueAnimator minHeightAnimator(final View view, int i, int i2, int i3) {
        ValueAnimator ofInt = ValueAnimator.ofInt(i, i2);
        ofInt.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() { // from class: zendesk.support.request.UtilsAnimation.4
            @Override // android.animation.ValueAnimator.AnimatorUpdateListener
            public void onAnimationUpdate(ValueAnimator valueAnimator) {
                view.setMinimumHeight(((Integer) valueAnimator.getAnimatedValue()).intValue());
            }
        });
        ofInt.setDuration(i3);
        return ofInt;
    }

    public static ValueAnimator sideMarginsAnimator(final View view, int i, int i2, int i3) {
        final FrameLayout.LayoutParams layoutParams = (FrameLayout.LayoutParams) view.getLayoutParams();
        ValueAnimator ofInt = ValueAnimator.ofInt(i, i2);
        ofInt.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() { // from class: zendesk.support.request.UtilsAnimation.3
            @Override // android.animation.ValueAnimator.AnimatorUpdateListener
            public void onAnimationUpdate(ValueAnimator valueAnimator) {
                layoutParams.leftMargin = ((Integer) valueAnimator.getAnimatedValue()).intValue();
                layoutParams.rightMargin = ((Integer) valueAnimator.getAnimatedValue()).intValue();
                view.requestLayout();
            }
        });
        ofInt.setDuration(i3);
        return ofInt;
    }

    public static ValueAnimator topPaddingAnimator(View view, int i, int i2, int i3) {
        ValueAnimator ofInt = ValueAnimator.ofInt(i, i2);
        ofInt.addUpdateListener(new ValueAnimator.AnimatorUpdateListener(view, ofInt) { // from class: zendesk.support.request.UtilsAnimation.1
            public final int paddingBottom;
            public final int paddingLeft;
            public final int paddingRight;
            public final /* synthetic */ ValueAnimator val$valueAnimator;
            public final /* synthetic */ View val$view;

            {
                this.val$view = view;
                this.val$valueAnimator = ofInt;
                this.paddingLeft = view.getPaddingLeft();
                this.paddingRight = view.getPaddingRight();
                this.paddingBottom = view.getPaddingBottom();
            }

            @Override // android.animation.ValueAnimator.AnimatorUpdateListener
            public void onAnimationUpdate(ValueAnimator valueAnimator) {
                this.val$view.setPadding(this.paddingLeft, ((Integer) this.val$valueAnimator.getAnimatedValue()).intValue(), this.paddingRight, this.paddingBottom);
            }
        });
        ofInt.setDuration(i3);
        return ofInt;
    }
}
