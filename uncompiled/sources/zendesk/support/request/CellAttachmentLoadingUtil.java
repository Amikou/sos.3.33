package zendesk.support.request;

import android.content.Context;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.view.ViewGroup;
import android.widget.ImageView;
import com.squareup.picasso.Picasso;
import com.squareup.picasso.o;
import com.squareup.picasso.r;
import com.zendesk.logger.Logger;
import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import zendesk.support.PicassoTransformations;

/* JADX INFO: Access modifiers changed from: package-private */
/* loaded from: classes3.dex */
public class CellAttachmentLoadingUtil {
    private static final String LOG_TAG = "AttachmentLoadingUtil";
    private final ImageLoadingLogic imageLoadingLogic;
    private final ImageSizingLogic imageSizingLogic;

    /* loaded from: classes3.dex */
    public static class ImageLoadingLogic {
        private static final int IMAGE_DOWNSCALE_FACTOR = 2;
        private final Picasso picasso;

        /* loaded from: classes3.dex */
        public static class DefaultDisplayStrategy implements LoadingStrategy {
            private DefaultDisplayStrategy() {
            }

            @Override // zendesk.support.request.CellAttachmentLoadingUtil.ImageLoadingLogic.LoadingStrategy
            public void load(ImageView imageView, ImageSizingLogic.ImageDimensions imageDimensions) {
            }
        }

        /* loaded from: classes3.dex */
        public static class DisplayImageFromLocalSource implements LoadingStrategy {
            private final o requestCreator;

            @Override // zendesk.support.request.CellAttachmentLoadingUtil.ImageLoadingLogic.LoadingStrategy
            public void load(ImageView imageView, ImageSizingLogic.ImageDimensions imageDimensions) {
                ImageLoadingLogic.loadImage(imageView, this.requestCreator.j().i(), imageDimensions, null);
            }

            private DisplayImageFromLocalSource(o oVar) {
                this.requestCreator = oVar;
            }
        }

        /* loaded from: classes3.dex */
        public static class DisplayImageFromWeb implements LoadingStrategy {
            public final Picasso picasso;
            public final String thumbnailUrl;
            public final String url;

            @Override // zendesk.support.request.CellAttachmentLoadingUtil.ImageLoadingLogic.LoadingStrategy
            public void load(final ImageView imageView, final ImageSizingLogic.ImageDimensions imageDimensions) {
                ImageLoadingLogic.loadImage(imageView, this.picasso.l(this.thumbnailUrl).l(PicassoTransformations.getBlurTransformation(imageView.getContext().getApplicationContext())), imageDimensions, new xu() { // from class: zendesk.support.request.CellAttachmentLoadingUtil.ImageLoadingLogic.DisplayImageFromWeb.1
                    @Override // defpackage.xu
                    public void onError(Exception exc) {
                        Logger.b(RequestActivity.LOG_TAG, "Unable to load thumbnail. Url: '%s'", DisplayImageFromWeb.this.thumbnailUrl, exc);
                        ImageView imageView2 = imageView;
                        DisplayImageFromWeb displayImageFromWeb = DisplayImageFromWeb.this;
                        ImageLoadingLogic.loadImage(imageView2, displayImageFromWeb.picasso.l(displayImageFromWeb.url).j(), imageDimensions, null);
                    }

                    @Override // defpackage.xu
                    public void onSuccess() {
                        ImageView imageView2 = imageView;
                        DisplayImageFromWeb displayImageFromWeb = DisplayImageFromWeb.this;
                        ImageLoadingLogic.loadImage(imageView2, displayImageFromWeb.picasso.l(displayImageFromWeb.url).j(), imageDimensions, null);
                    }
                });
            }

            private DisplayImageFromWeb(Picasso picasso, String str, String str2) {
                this.picasso = picasso;
                this.url = str;
                this.thumbnailUrl = str2;
            }
        }

        /* loaded from: classes3.dex */
        public interface LoadingStrategy {
            void load(ImageView imageView, ImageSizingLogic.ImageDimensions imageDimensions);
        }

        public ImageLoadingLogic(Picasso picasso) {
            this.picasso = picasso;
        }

        private LoadingStrategy getLoadingStrategy(StateRequestAttachment stateRequestAttachment) {
            if (stateRequestAttachment.getLocalFile() != null && stateRequestAttachment.getLocalFile().exists() && stateRequestAttachment.getLocalFile().length() > 0) {
                return new DisplayImageFromLocalSource(this.picasso.k(stateRequestAttachment.getLocalFile()));
            }
            if (ru3.b(stateRequestAttachment.getLocalUri()) && Uri.parse(stateRequestAttachment.getLocalUri()) != null) {
                return new DisplayImageFromLocalSource(this.picasso.j(stateRequestAttachment.getParsedLocalUri()));
            }
            if (ru3.b(stateRequestAttachment.getUrl()) && ru3.b(stateRequestAttachment.getThumbnailUrl())) {
                return new DisplayImageFromWeb(this.picasso, stateRequestAttachment.getUrl(), stateRequestAttachment.getThumbnailUrl());
            }
            Logger.b(RequestActivity.LOG_TAG, "Can't load image. Id: %s", Long.valueOf(stateRequestAttachment.getId()));
            return new DefaultDisplayStrategy();
        }

        /* JADX INFO: Access modifiers changed from: private */
        public static void loadImage(ImageView imageView, o oVar, ImageSizingLogic.ImageDimensions imageDimensions, xu xuVar) {
            oVar.l(PicassoTransformations.getRoundedTransformation(imageView.getContext().getResources().getDimensionPixelOffset(yy2.zs_request_attachment_corner_radius) / 2)).k(imageDimensions.getImageWidth() / 2, imageDimensions.getImageHeight() / 2).a().g(imageView, xuVar);
        }

        public void initImageView(ImageView imageView) {
            this.picasso.b(imageView);
            imageView.setImageResource(vy2.zs_color_transparent);
        }

        public boolean isImageLoading(ImageView imageView, StateRequestAttachment stateRequestAttachment) {
            Object tag = imageView.getTag();
            return (tag instanceof StateRequestAttachment) && ((StateRequestAttachment) tag).getId() == stateRequestAttachment.getId();
        }

        public void loadAttachment(ImageView imageView, StateRequestAttachment stateRequestAttachment, ImageSizingLogic.ImageDimensions imageDimensions) {
            getLoadingStrategy(stateRequestAttachment).load(imageView, imageDimensions);
        }

        public void setImageViewLoading(ImageView imageView, StateRequestAttachment stateRequestAttachment) {
            imageView.setTag(stateRequestAttachment);
        }
    }

    /* loaded from: classes3.dex */
    public static class ImageSizingLogic {
        private static final double ASPECT_RATIO = 1.7777777777777777d;
        private final Map<String, ImageDimensions> cachedDimensions = new HashMap();
        private final ImageDimensions maxSize;
        private final Picasso picasso;

        /* loaded from: classes3.dex */
        public static class DefaultStrategy implements DimensionStrategy {
            private DefaultStrategy() {
            }

            @Override // zendesk.support.request.CellAttachmentLoadingUtil.ImageSizingLogic.DimensionStrategy
            public void findDimensions(rs4<ImageDimensions> rs4Var) {
                rs4Var.onSuccess(new ImageDimensions());
            }
        }

        /* loaded from: classes3.dex */
        public interface DimensionStrategy {
            void findDimensions(rs4<ImageDimensions> rs4Var);
        }

        /* loaded from: classes3.dex */
        public static class ExistingDimensions implements DimensionStrategy {
            private final int height;
            private final ImageDimensions maxSize;
            private final int width;

            public ExistingDimensions(int i, int i2, ImageDimensions imageDimensions) {
                this.width = i;
                this.height = i2;
                this.maxSize = imageDimensions;
            }

            @Override // zendesk.support.request.CellAttachmentLoadingUtil.ImageSizingLogic.DimensionStrategy
            public void findDimensions(rs4<ImageDimensions> rs4Var) {
                rs4Var.onSuccess(ImageSizingLogic.determineTargetDimensions(this.width, this.height, this.maxSize.getImageWidth(), this.maxSize.getImageHeight()));
            }
        }

        /* loaded from: classes3.dex */
        public static class ReadFromBitmap implements DimensionStrategy {
            public final File file;
            private final ImageDimensions maxSize;

            public ReadFromBitmap(File file, ImageDimensions imageDimensions) {
                this.maxSize = imageDimensions;
                this.file = file;
            }

            private ImageDimensions loadImageDimensions(File file) {
                ImageDimensions imageDimensions = new ImageDimensions();
                BitmapFactory.Options options = new BitmapFactory.Options();
                options.inJustDecodeBounds = true;
                BitmapFactory.decodeFile(file.getAbsolutePath(), options);
                imageDimensions.setDimensions(options.outWidth, options.outHeight);
                return imageDimensions;
            }

            @Override // zendesk.support.request.CellAttachmentLoadingUtil.ImageSizingLogic.DimensionStrategy
            public void findDimensions(rs4<ImageDimensions> rs4Var) {
                ImageDimensions loadImageDimensions = loadImageDimensions(this.file);
                rs4Var.onSuccess(ImageSizingLogic.determineTargetDimensions(loadImageDimensions.getImageWidth(), loadImageDimensions.getImageHeight(), this.maxSize.getImageWidth(), this.maxSize.getImageHeight()));
            }
        }

        /* loaded from: classes3.dex */
        public static class ReadFromPicasso implements DimensionStrategy {
            private static final List<r> TARGET_REFERENCE_TRAP = new ArrayList();
            private final ImageDimensions maxSize;
            private final o requestCreator;

            @Override // zendesk.support.request.CellAttachmentLoadingUtil.ImageSizingLogic.DimensionStrategy
            public void findDimensions(final rs4<ImageDimensions> rs4Var) {
                r rVar = new r() { // from class: zendesk.support.request.CellAttachmentLoadingUtil.ImageSizingLogic.ReadFromPicasso.1
                    @Override // com.squareup.picasso.r
                    public void onBitmapFailed(Exception exc, Drawable drawable) {
                        Logger.b(RequestActivity.LOG_TAG, "Unable to load image.", new Object[0]);
                        rs4Var.onSuccess(new ImageDimensions());
                        ReadFromPicasso.TARGET_REFERENCE_TRAP.remove(this);
                    }

                    @Override // com.squareup.picasso.r
                    public void onBitmapLoaded(Bitmap bitmap, Picasso.LoadedFrom loadedFrom) {
                        rs4Var.onSuccess(ImageSizingLogic.determineTargetDimensions(bitmap.getWidth(), bitmap.getHeight(), ReadFromPicasso.this.maxSize.getImageWidth(), ReadFromPicasso.this.maxSize.getImageHeight()));
                        ReadFromPicasso.TARGET_REFERENCE_TRAP.remove(this);
                    }

                    @Override // com.squareup.picasso.r
                    public void onPrepareLoad(Drawable drawable) {
                    }
                };
                TARGET_REFERENCE_TRAP.add(rVar);
                this.requestCreator.h(rVar);
            }

            private ReadFromPicasso(o oVar, ImageDimensions imageDimensions) {
                this.requestCreator = oVar;
                this.maxSize = imageDimensions;
            }
        }

        public ImageSizingLogic(Picasso picasso, Context context) {
            this.picasso = picasso;
            this.maxSize = getMaxSize(context);
        }

        private int calculateMaxWidth(Context context) {
            Resources resources = context.getResources();
            return (resources.getDisplayMetrics().widthPixels - resources.getDimensionPixelSize(yy2.zs_request_message_composer_expanded_side_margin)) - resources.getDimensionPixelSize(yy2.zs_request_message_margin_side);
        }

        public static ImageDimensions determineTargetDimensions(int i, int i2, int i3, int i4) {
            ImageDimensions imageDimensions = new ImageDimensions();
            int i5 = (int) (i3 / ((i * 1.0d) / i2));
            if (i > i2) {
                if (i > i3) {
                    i = i3;
                    i2 = i5;
                }
            } else if (i2 > i5) {
                i = Math.min(i3, i);
                i2 = i5;
            }
            imageDimensions.setDimensions(i, Math.max(Math.min(i4, i2), 0));
            return imageDimensions;
        }

        private DimensionStrategy getDimensionStrategy(StateRequestAttachment stateRequestAttachment, ImageDimensions imageDimensions) {
            if (stateRequestAttachment.getHeight() > 0 && stateRequestAttachment.getWidth() > 0) {
                return new ExistingDimensions(stateRequestAttachment.getWidth(), stateRequestAttachment.getHeight(), imageDimensions);
            }
            if (ru3.b(stateRequestAttachment.getLocalUri()) && this.cachedDimensions.containsKey(stateRequestAttachment.getLocalUri())) {
                ImageDimensions imageDimensions2 = this.cachedDimensions.get(stateRequestAttachment.getLocalUri());
                return new ExistingDimensions(imageDimensions2.getImageWidth(), imageDimensions2.getImageHeight(), imageDimensions);
            } else if (stateRequestAttachment.getLocalFile() != null && stateRequestAttachment.getLocalFile().exists() && stateRequestAttachment.getLocalFile().length() > 0) {
                return new ReadFromBitmap(stateRequestAttachment.getLocalFile(), imageDimensions);
            } else {
                if (ru3.b(stateRequestAttachment.getLocalUri()) && Uri.parse(stateRequestAttachment.getLocalUri()) != null) {
                    return new ReadFromPicasso(this.picasso.j(Uri.parse(stateRequestAttachment.getLocalUri())), imageDimensions);
                }
                if (ru3.b(stateRequestAttachment.getUrl())) {
                    return new ReadFromPicasso(this.picasso.l(stateRequestAttachment.getUrl()), imageDimensions);
                }
                Logger.b(RequestActivity.LOG_TAG, "Can't load dimensions. Id: %s", Long.valueOf(stateRequestAttachment.getId()));
                return new DefaultStrategy();
            }
        }

        public ImageDimensions getMaxSize() {
            return this.maxSize;
        }

        public void loadDimensionsForAttachment(final StateRequestAttachment stateRequestAttachment, final rs4<ImageDimensions> rs4Var) {
            getDimensionStrategy(stateRequestAttachment, this.maxSize).findDimensions(new rs4<ImageDimensions>() { // from class: zendesk.support.request.CellAttachmentLoadingUtil.ImageSizingLogic.1
                @Override // defpackage.rs4
                public void onError(cw0 cw0Var) {
                }

                @Override // defpackage.rs4
                public void onSuccess(ImageDimensions imageDimensions) {
                    if (ru3.b(stateRequestAttachment.getLocalUri()) && imageDimensions.areKnown()) {
                        ImageSizingLogic.this.cachedDimensions.put(stateRequestAttachment.getLocalUri(), imageDimensions);
                    }
                    rs4Var.onSuccess(imageDimensions);
                }
            });
        }

        private ImageDimensions getMaxSize(Context context) {
            int calculateMaxWidth = calculateMaxWidth(context);
            return new ImageDimensions(calculateMaxWidth, (int) (calculateMaxWidth / ASPECT_RATIO));
        }

        /* loaded from: classes3.dex */
        public static class ImageDimensions {
            private static final int UNKNOWN_DIMENSION = -1;
            private int imageHeight;
            private int imageWidth;

            public ImageDimensions(int i, int i2) {
                this.imageWidth = -1;
                this.imageHeight = -1;
                this.imageWidth = i;
                this.imageHeight = i2;
            }

            public boolean areKnown() {
                return (this.imageWidth == -1 || this.imageHeight == -1) ? false : true;
            }

            public int getImageHeight() {
                return this.imageHeight;
            }

            public int getImageWidth() {
                return this.imageWidth;
            }

            public void setDimensions(int i, int i2) {
                this.imageWidth = i;
                this.imageHeight = i2;
            }

            public String toString() {
                return "ImageDimensions{width=" + this.imageWidth + ", height=" + this.imageHeight + '}';
            }

            public ImageDimensions() {
                this.imageWidth = -1;
                this.imageHeight = -1;
            }
        }
    }

    public CellAttachmentLoadingUtil(Picasso picasso, Context context) {
        this.imageSizingLogic = new ImageSizingLogic(picasso, context);
        this.imageLoadingLogic = new ImageLoadingLogic(picasso);
    }

    /* JADX INFO: Access modifiers changed from: private */
    public void adjustImageViewDimensions(ImageView imageView, ImageSizingLogic.ImageDimensions imageDimensions) {
        ViewGroup.LayoutParams layoutParams = imageView.getLayoutParams();
        layoutParams.width = imageDimensions.getImageWidth();
        layoutParams.height = imageDimensions.getImageHeight();
        imageView.setLayoutParams(layoutParams);
    }

    public void bindImage(final ImageView imageView, final StateRequestAttachment stateRequestAttachment) {
        if (this.imageLoadingLogic.isImageLoading(imageView, stateRequestAttachment)) {
            return;
        }
        this.imageLoadingLogic.setImageViewLoading(imageView, stateRequestAttachment);
        adjustImageViewDimensions(imageView, this.imageSizingLogic.getMaxSize());
        this.imageLoadingLogic.initImageView(imageView);
        this.imageSizingLogic.loadDimensionsForAttachment(stateRequestAttachment, new rs4<ImageSizingLogic.ImageDimensions>() { // from class: zendesk.support.request.CellAttachmentLoadingUtil.1
            @Override // defpackage.rs4
            public void onError(cw0 cw0Var) {
            }

            @Override // defpackage.rs4
            public void onSuccess(ImageSizingLogic.ImageDimensions imageDimensions) {
                if (!imageDimensions.areKnown()) {
                    Logger.b(RequestActivity.LOG_TAG, "Unable retrieve image size. Id: %s", Long.valueOf(stateRequestAttachment.getId()));
                    return;
                }
                CellAttachmentLoadingUtil.this.adjustImageViewDimensions(imageView, imageDimensions);
                CellAttachmentLoadingUtil.this.imageLoadingLogic.loadAttachment(imageView, stateRequestAttachment, imageDimensions);
            }
        });
    }
}
