package zendesk.support.request;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.Map;
import zendesk.support.Attachment;
import zendesk.support.CommentResponse;
import zendesk.support.IdUtil;

/* JADX INFO: Access modifiers changed from: package-private */
/* loaded from: classes3.dex */
public class StateMessage implements Serializable {
    private final List<StateRequestAttachment> attachments;
    private final Date date;
    private final String htmlBody;
    private final long id;
    private final String plainBody;
    private final StateMessageStatus state;
    private final long userId;

    public StateMessage(String str, List<StateRequestAttachment> list) {
        this.htmlBody = null;
        this.plainBody = str;
        this.date = new Date();
        this.id = IdUtil.newLongId();
        this.userId = -1L;
        this.state = StateMessageStatus.pending();
        this.attachments = list;
    }

    public static jp2<List<StateMessage>, StateIdMapper> convert(List<CommentResponse> list, StateIdMapper stateIdMapper, Map<Long, StateRequestAttachment> map) {
        long newLongId;
        ArrayList arrayList = new ArrayList(list.size());
        StateIdMapper stateIdMapper2 = stateIdMapper;
        for (CommentResponse commentResponse : list) {
            Long id = commentResponse.getId();
            Long authorId = commentResponse.getAuthorId();
            if (id != null && authorId != null) {
                ArrayList arrayList2 = new ArrayList();
                for (Attachment attachment : commentResponse.getAttachments()) {
                    if (map.containsKey(attachment.getId())) {
                        arrayList2.add(map.get(attachment.getId()));
                    }
                }
                if (stateIdMapper2.hasLocalId(id)) {
                    newLongId = stateIdMapper2.getLocalId(id).longValue();
                } else {
                    newLongId = IdUtil.newLongId();
                    stateIdMapper2 = stateIdMapper2.addIdMapping(id, Long.valueOf(newLongId));
                }
                arrayList.add(new StateMessage(commentResponse.getHtmlBody(), commentResponse.getBody(), commentResponse.getCreatedAt(), newLongId, authorId.longValue(), StateMessageStatus.delivered(), arrayList2));
            }
        }
        return jp2.a(arrayList, stateIdMapper2.copy());
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null || getClass() != obj.getClass()) {
            return false;
        }
        StateMessage stateMessage = (StateMessage) obj;
        if (this.id == stateMessage.id && this.userId == stateMessage.userId) {
            String str = this.htmlBody;
            if (str == null ? stateMessage.htmlBody == null : str.equals(stateMessage.htmlBody)) {
                String str2 = this.plainBody;
                if (str2 == null ? stateMessage.plainBody == null : str2.equals(stateMessage.plainBody)) {
                    Date date = this.date;
                    if (date == null ? stateMessage.date == null : date.equals(stateMessage.date)) {
                        StateMessageStatus stateMessageStatus = this.state;
                        if (stateMessageStatus == null ? stateMessage.state == null : stateMessageStatus.equals(stateMessage.state)) {
                            List<StateRequestAttachment> list = this.attachments;
                            List<StateRequestAttachment> list2 = stateMessage.attachments;
                            return list != null ? list.equals(list2) : list2 == null;
                        }
                        return false;
                    }
                    return false;
                }
                return false;
            }
            return false;
        }
        return false;
    }

    public List<StateRequestAttachment> getAttachments() {
        return this.attachments;
    }

    public String getBody() {
        return ru3.b(this.plainBody) ? this.plainBody : UtilsAttachment.getMessageBodyForAttachments(getAttachments());
    }

    public Date getDate() {
        return this.date;
    }

    public String getHtmlBody() {
        return this.htmlBody;
    }

    public long getId() {
        return this.id;
    }

    public String getPlainBody() {
        return this.plainBody;
    }

    public StateMessageStatus getState() {
        return this.state;
    }

    public long getUserId() {
        return this.userId;
    }

    public int hashCode() {
        return Arrays.hashCode(new Object[]{this.htmlBody, this.plainBody, this.date, Long.valueOf(this.id), Long.valueOf(this.userId), this.state, this.attachments});
    }

    public String toString() {
        return "Message{htmlBody='" + this.htmlBody + "', plainBody='" + this.plainBody + "', date=" + this.date + ", id=" + this.id + ", userId=" + this.userId + ", state=" + this.state + '}';
    }

    public StateMessage withAttachments(List<StateRequestAttachment> list) {
        return new StateMessage(this.htmlBody, this.plainBody, this.date, this.id, this.userId, this.state, list);
    }

    public StateMessage withDelivered() {
        return new StateMessage(this.htmlBody, this.plainBody, this.date, this.id, this.userId, StateMessageStatus.delivered(), this.attachments);
    }

    public StateMessage withError(cw0 cw0Var) {
        return new StateMessage(this.htmlBody, this.plainBody, this.date, this.id, this.userId, StateMessageStatus.error(cw0Var.h()), this.attachments);
    }

    public StateMessage withPending() {
        return new StateMessage(this.htmlBody, this.plainBody, this.date, this.id, this.userId, StateMessageStatus.pending(), this.attachments);
    }

    public StateMessage withUpdatedAttachment(StateRequestAttachment stateRequestAttachment) {
        ArrayList arrayList = new ArrayList(this.attachments.size());
        for (StateRequestAttachment stateRequestAttachment2 : this.attachments) {
            if (stateRequestAttachment2.getId() == stateRequestAttachment.getId()) {
                stateRequestAttachment2 = stateRequestAttachment;
            }
            arrayList.add(stateRequestAttachment2);
        }
        return new StateMessage(this.htmlBody, this.plainBody, this.date, this.id, this.userId, this.state, arrayList);
    }

    public StateMessage(String str, String str2, Date date, long j, long j2, StateMessageStatus stateMessageStatus, List<StateRequestAttachment> list) {
        this.htmlBody = str;
        this.plainBody = str2;
        this.date = date;
        this.id = j;
        this.userId = j2;
        this.state = stateMessageStatus;
        this.attachments = list;
    }
}
