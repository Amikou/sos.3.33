package zendesk.support.request;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import zendesk.support.Attachment;
import zendesk.support.User;

/* loaded from: classes3.dex */
class StateRequestUser implements Serializable {
    private final String avatar;
    private final long id;
    private final boolean isAgent;
    private final String name;

    public StateRequestUser(String str, String str2, boolean z, long j) {
        this.name = str;
        this.avatar = str2;
        this.isAgent = z;
        this.id = j;
    }

    public static boolean containsAgent(List<StateRequestUser> list) {
        for (StateRequestUser stateRequestUser : list) {
            if (stateRequestUser.isAgent()) {
                return true;
            }
        }
        return false;
    }

    public static List<StateRequestUser> convert(List<User> list) {
        ArrayList arrayList = new ArrayList(list.size());
        for (User user : list) {
            if (user.getId() != null) {
                Attachment photo = user.getPhoto();
                arrayList.add(new StateRequestUser(user.getName(), (photo == null || !ru3.b(photo.getContentUrl())) ? "" : photo.getContentUrl(), user.isAgent(), user.getId().longValue()));
            }
        }
        return arrayList;
    }

    public String getAvatar() {
        return this.avatar;
    }

    public long getId() {
        return this.id;
    }

    public String getName() {
        return this.name;
    }

    public boolean isAgent() {
        return this.isAgent;
    }
}
