package zendesk.support.request;

import android.animation.TimeInterpolator;
import android.animation.ValueAnimator;
import android.annotation.SuppressLint;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import androidx.recyclerview.widget.RecyclerView;
import androidx.recyclerview.widget.g;
import com.github.mikephil.charting.utils.Utils;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import zendesk.support.request.CellType;
import zendesk.support.suas.Listener;
import zendesk.support.suas.State;
import zendesk.support.suas.StateSelector;

/* JADX INFO: Access modifiers changed from: package-private */
/* loaded from: classes3.dex */
public class ComponentRequestAdapter implements Listener<List<CellType.Base>> {
    private static final long UPDATE_TIME_WINDOW = 200;
    private final i02 listUpdateCallback;
    private final RecyclerView recyclerView;
    private final RequestAdapterSelector requestAdapterSelector;
    private final List<CellType.Base> requestMessageList;
    private Runnable updateRunnable;

    /* loaded from: classes3.dex */
    public static class DiffCalculator extends g.b {
        private final List<CellType.Base> newList;
        private final List<CellType.Base> oldList;

        @Override // androidx.recyclerview.widget.g.b
        public boolean areContentsTheSame(int i, int i2) {
            return this.oldList.get(i).areContentsTheSame(this.newList.get(i2));
        }

        @Override // androidx.recyclerview.widget.g.b
        public boolean areItemsTheSame(int i, int i2) {
            return this.oldList.get(i).getUniqueId() == this.newList.get(i2).getUniqueId();
        }

        @Override // androidx.recyclerview.widget.g.b
        public int getNewListSize() {
            return this.newList.size();
        }

        @Override // androidx.recyclerview.widget.g.b
        public int getOldListSize() {
            return this.oldList.size();
        }

        private DiffCalculator(List<CellType.Base> list, List<CellType.Base> list2) {
            this.oldList = list;
            this.newList = list2;
        }
    }

    /* loaded from: classes3.dex */
    public static class RequestAdapter extends RecyclerView.Adapter<RequestViewHolder> {
        private final ComponentRequestAdapter dataSource;
        private int lastPosition = -1;

        public RequestAdapter(ComponentRequestAdapter componentRequestAdapter) {
            setHasStableIds(true);
            this.dataSource = componentRequestAdapter;
        }

        @Override // androidx.recyclerview.widget.RecyclerView.Adapter
        public int getItemCount() {
            return this.dataSource.getMessageCount();
        }

        @Override // androidx.recyclerview.widget.RecyclerView.Adapter
        public long getItemId(int i) {
            return this.dataSource.getMessageForPos(i).getUniqueId();
        }

        @Override // androidx.recyclerview.widget.RecyclerView.Adapter
        public int getItemViewType(int i) {
            return this.dataSource.getMessageForPos(i).getLayoutId();
        }

        @Override // androidx.recyclerview.widget.RecyclerView.Adapter
        @SuppressLint({"RecyclerView"})
        public void onBindViewHolder(RequestViewHolder requestViewHolder, int i) {
            this.dataSource.getMessageForPos(i).bind(requestViewHolder);
            int i2 = this.lastPosition;
            if (i > i2 && i2 != -1) {
                this.lastPosition = i;
                requestViewHolder.startAnimation();
            }
            if (this.lastPosition == -1) {
                this.lastPosition = i;
            }
        }

        @Override // androidx.recyclerview.widget.RecyclerView.Adapter
        public RequestViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
            return new RequestViewHolder(LayoutInflater.from(viewGroup.getContext()).inflate(i, viewGroup, false));
        }

        @Override // androidx.recyclerview.widget.RecyclerView.Adapter
        public void onViewDetachedFromWindow(RequestViewHolder requestViewHolder) {
            super.onViewDetachedFromWindow((RequestAdapter) requestViewHolder);
            requestViewHolder.clearAnimation();
        }
    }

    /* loaded from: classes3.dex */
    public static class RequestAdapterSelector implements StateSelector<List<CellType.Base>> {
        private final CellFactory messageFactory;

        public RequestAdapterSelector(CellFactory cellFactory) {
            this.messageFactory = cellFactory;
        }

        @Override // zendesk.support.suas.StateSelector
        public List<CellType.Base> selectData(State state) {
            StateConversation fromState = StateConversation.fromState(state);
            StateSettings settings = StateConfig.fromState(state).getSettings();
            return this.messageFactory.generateCells(fromState.getMessages(), fromState.getUsers(), fromState.getStatus(), settings.getSystemMessage());
        }
    }

    /* loaded from: classes3.dex */
    public static class RequestViewHolder extends RecyclerView.a0 {
        private static final long ANIMATION_DURATION = 250;
        private static final float ANIMATION_HEIGHT_RATIO = 0.6666667f;
        private static final TimeInterpolator TIME_INTERPOLATOR = zp2.a(0.2f, Utils.FLOAT_EPSILON, 0.4f, 1.0f);
        private ValueAnimator animation;
        @SuppressLint({"UseSparseArrays"})
        private final Map<Integer, View> viewCache;

        public RequestViewHolder(View view) {
            super(view);
            this.viewCache = new HashMap();
        }

        public void clearAnimation() {
            ValueAnimator valueAnimator = this.animation;
            if (valueAnimator != null) {
                valueAnimator.cancel();
                this.itemView.setTranslationY(Utils.FLOAT_EPSILON);
            }
        }

        public <E extends View> E findCachedView(int i) {
            E e;
            synchronized (this.viewCache) {
                if (this.viewCache.containsKey(Integer.valueOf(i))) {
                    e = (E) this.viewCache.get(Integer.valueOf(i));
                } else {
                    View findViewById = this.itemView.findViewById(i);
                    this.viewCache.put(Integer.valueOf(i), findViewById);
                    e = (E) findViewById;
                }
            }
            return e;
        }

        public void startAnimation() {
            int measuredHeight = this.itemView.getMeasuredHeight();
            if (measuredHeight == 0) {
                this.itemView.measure(0, 0);
                measuredHeight = this.itemView.getMeasuredHeight();
            }
            ValueAnimator ofFloat = ValueAnimator.ofFloat(measuredHeight * ANIMATION_HEIGHT_RATIO, Utils.FLOAT_EPSILON);
            this.animation = ofFloat;
            ofFloat.setInterpolator(TIME_INTERPOLATOR);
            this.animation.setDuration(ANIMATION_DURATION);
            this.animation.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() { // from class: zendesk.support.request.ComponentRequestAdapter.RequestViewHolder.1
                @Override // android.animation.ValueAnimator.AnimatorUpdateListener
                public void onAnimationUpdate(ValueAnimator valueAnimator) {
                    RequestViewHolder.this.itemView.setTranslationY(((Float) valueAnimator.getAnimatedValue()).floatValue());
                }
            });
            this.animation.start();
        }
    }

    public ComponentRequestAdapter(i02 i02Var, CellFactory cellFactory, RecyclerView recyclerView) {
        this.updateRunnable = null;
        this.listUpdateCallback = i02Var;
        this.recyclerView = recyclerView;
        this.requestMessageList = new ArrayList();
        this.requestAdapterSelector = new RequestAdapterSelector(cellFactory);
    }

    /* JADX INFO: Access modifiers changed from: private */
    public void updateDataSet(List<CellType.Base> list, List<CellType.Base> list2) {
        g.e c = g.c(new DiffCalculator(list, list2), true);
        this.requestMessageList.clear();
        this.requestMessageList.addAll(list2);
        c.c(this.listUpdateCallback);
    }

    public int getMessageCount() {
        return this.requestMessageList.size();
    }

    public CellType.Base getMessageForPos(int i) {
        return this.requestMessageList.get(i);
    }

    public StateSelector<List<CellType.Base>> getSelector() {
        return this.requestAdapterSelector;
    }

    @Override // zendesk.support.suas.Listener
    public void update(final List<CellType.Base> list) {
        this.recyclerView.removeCallbacks(this.updateRunnable);
        Runnable runnable = new Runnable() { // from class: zendesk.support.request.ComponentRequestAdapter.1
            @Override // java.lang.Runnable
            public void run() {
                ComponentRequestAdapter.this.updateDataSet(l10.c(ComponentRequestAdapter.this.requestMessageList), l10.c(list));
            }
        };
        this.updateRunnable = runnable;
        this.recyclerView.postDelayed(runnable, 200L);
    }

    public ComponentRequestAdapter(List<CellType.Base> list, i02 i02Var, RequestAdapterSelector requestAdapterSelector, RecyclerView recyclerView) {
        this.updateRunnable = null;
        this.requestMessageList = list;
        this.listUpdateCallback = i02Var;
        this.requestAdapterSelector = requestAdapterSelector;
        this.recyclerView = recyclerView;
    }
}
