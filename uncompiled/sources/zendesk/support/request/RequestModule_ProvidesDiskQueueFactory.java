package zendesk.support.request;

import java.util.concurrent.ExecutorService;
import zendesk.support.request.ComponentPersistence;

/* loaded from: classes3.dex */
public final class RequestModule_ProvidesDiskQueueFactory implements y11<ComponentPersistence.PersistenceQueue> {
    private final ew2<ExecutorService> executorServiceProvider;

    public RequestModule_ProvidesDiskQueueFactory(ew2<ExecutorService> ew2Var) {
        this.executorServiceProvider = ew2Var;
    }

    public static RequestModule_ProvidesDiskQueueFactory create(ew2<ExecutorService> ew2Var) {
        return new RequestModule_ProvidesDiskQueueFactory(ew2Var);
    }

    public static ComponentPersistence.PersistenceQueue providesDiskQueue(ExecutorService executorService) {
        return (ComponentPersistence.PersistenceQueue) cu2.f(RequestModule.providesDiskQueue(executorService));
    }

    @Override // defpackage.ew2
    public ComponentPersistence.PersistenceQueue get() {
        return providesDiskQueue(this.executorServiceProvider.get());
    }
}
