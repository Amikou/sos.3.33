package zendesk.support.request;

import zendesk.support.suas.Dispatcher;
import zendesk.support.suas.Store;

/* loaded from: classes3.dex */
public final class RequestModule_ProvidesDispatcherFactory implements y11<Dispatcher> {
    private final ew2<Store> storeProvider;

    public RequestModule_ProvidesDispatcherFactory(ew2<Store> ew2Var) {
        this.storeProvider = ew2Var;
    }

    public static RequestModule_ProvidesDispatcherFactory create(ew2<Store> ew2Var) {
        return new RequestModule_ProvidesDispatcherFactory(ew2Var);
    }

    public static Dispatcher providesDispatcher(Store store) {
        return (Dispatcher) cu2.f(RequestModule.providesDispatcher(store));
    }

    @Override // defpackage.ew2
    public Dispatcher get() {
        return providesDispatcher(this.storeProvider.get());
    }
}
