package zendesk.support.request;

/* loaded from: classes3.dex */
public final class RequestModule_ProvidesAsyncMiddlewareFactory implements y11<AsyncMiddleware> {

    /* loaded from: classes3.dex */
    public static final class InstanceHolder {
        private static final RequestModule_ProvidesAsyncMiddlewareFactory INSTANCE = new RequestModule_ProvidesAsyncMiddlewareFactory();

        private InstanceHolder() {
        }
    }

    public static RequestModule_ProvidesAsyncMiddlewareFactory create() {
        return InstanceHolder.INSTANCE;
    }

    public static AsyncMiddleware providesAsyncMiddleware() {
        return (AsyncMiddleware) cu2.f(RequestModule.providesAsyncMiddleware());
    }

    @Override // defpackage.ew2
    public AsyncMiddleware get() {
        return providesAsyncMiddleware();
    }
}
