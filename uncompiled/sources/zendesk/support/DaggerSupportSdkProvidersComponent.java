package zendesk.support;

import android.content.Context;
import java.util.Locale;
import zendesk.core.AuthenticationProvider;
import zendesk.core.BlipsProvider;
import zendesk.core.CoreModule;
import zendesk.core.CoreModule_ActionHandlerRegistryFactory;
import zendesk.core.CoreModule_GetApplicationContextFactory;
import zendesk.core.CoreModule_GetAuthenticationProviderFactory;
import zendesk.core.CoreModule_GetBlipsProviderFactory;
import zendesk.core.CoreModule_GetMemoryCacheFactory;
import zendesk.core.CoreModule_GetRestServiceProviderFactory;
import zendesk.core.CoreModule_GetSessionStorageFactory;
import zendesk.core.CoreModule_GetSettingsProviderFactory;
import zendesk.core.MemoryCache;
import zendesk.core.RestServiceProvider;
import zendesk.core.SessionStorage;
import zendesk.core.SettingsProvider;
import zendesk.core.ZendeskLocaleConverter;

/* loaded from: classes3.dex */
public final class DaggerSupportSdkProvidersComponent implements SupportSdkProvidersComponent {
    private final CoreModule coreModule;
    private ew2<Context> getApplicationContextProvider;
    private ew2<AuthenticationProvider> getAuthenticationProvider;
    private ew2<BlipsProvider> getBlipsProvider;
    private ew2<MemoryCache> getMemoryCacheProvider;
    private ew2<RestServiceProvider> getRestServiceProvider;
    private ew2<SessionStorage> getSessionStorageProvider;
    private ew2<SettingsProvider> getSettingsProvider;
    private ew2<Locale> provideLocaleProvider;
    private ew2<SupportSdkMetadata> provideMetadataProvider;
    private ew2<ProviderStore> provideProviderStoreProvider;
    private ew2<RequestMigrator> provideRequestMigratorProvider;
    private ew2<RequestProvider> provideRequestProvider;
    private ew2<RequestSessionCache> provideRequestSessionCacheProvider;
    private ew2<RequestStorage> provideRequestStorageProvider;
    private ew2<SupportSettingsProvider> provideSdkSettingsProvider;
    private ew2<SupportBlipsProvider> provideSupportBlipsProvider;
    private ew2<SupportModule> provideSupportModuleProvider;
    private ew2<UploadProvider> provideUploadProvider;
    private ew2<ZendeskLocaleConverter> provideZendeskLocaleConverterProvider;
    private ew2<ZendeskRequestService> provideZendeskRequestServiceProvider;
    private ew2<ZendeskUploadService> provideZendeskUploadServiceProvider;
    private ew2<ArticleVoteStorage> providesArticleVoteStorageProvider;
    private ew2<HelpCenterProvider> providesHelpCenterProvider;
    private ew2<RequestService> providesRequestServiceProvider;
    private ew2<UploadService> providesUploadServiceProvider;
    private ew2<ZendeskTracker> providesZendeskTrackerProvider;
    private final DaggerSupportSdkProvidersComponent supportSdkProvidersComponent;

    /* loaded from: classes3.dex */
    public static final class Builder {
        private CoreModule coreModule;
        private GuideModule guideModule;
        private ProviderModule providerModule;
        private StorageModule storageModule;
        private SupportApplicationModule supportApplicationModule;

        public SupportSdkProvidersComponent build() {
            cu2.a(this.supportApplicationModule, SupportApplicationModule.class);
            cu2.a(this.coreModule, CoreModule.class);
            if (this.providerModule == null) {
                this.providerModule = new ProviderModule();
            }
            cu2.a(this.guideModule, GuideModule.class);
            if (this.storageModule == null) {
                this.storageModule = new StorageModule();
            }
            return new DaggerSupportSdkProvidersComponent(this.supportApplicationModule, this.coreModule, this.providerModule, this.guideModule, this.storageModule);
        }

        public Builder coreModule(CoreModule coreModule) {
            this.coreModule = (CoreModule) cu2.b(coreModule);
            return this;
        }

        public Builder guideModule(GuideModule guideModule) {
            this.guideModule = (GuideModule) cu2.b(guideModule);
            return this;
        }

        public Builder providerModule(ProviderModule providerModule) {
            this.providerModule = (ProviderModule) cu2.b(providerModule);
            return this;
        }

        @Deprecated
        public Builder serviceModule(ServiceModule serviceModule) {
            cu2.b(serviceModule);
            return this;
        }

        public Builder storageModule(StorageModule storageModule) {
            this.storageModule = (StorageModule) cu2.b(storageModule);
            return this;
        }

        public Builder supportApplicationModule(SupportApplicationModule supportApplicationModule) {
            this.supportApplicationModule = (SupportApplicationModule) cu2.b(supportApplicationModule);
            return this;
        }

        private Builder() {
        }
    }

    public static Builder builder() {
        return new Builder();
    }

    private void initialize(SupportApplicationModule supportApplicationModule, CoreModule coreModule, ProviderModule providerModule, GuideModule guideModule, StorageModule storageModule) {
        this.providesHelpCenterProvider = GuideModule_ProvidesHelpCenterProviderFactory.create(guideModule);
        this.getSettingsProvider = CoreModule_GetSettingsProviderFactory.create(coreModule);
        this.provideLocaleProvider = fq0.a(SupportApplicationModule_ProvideLocaleFactory.create(supportApplicationModule));
        ew2<ZendeskLocaleConverter> a = fq0.a(ProviderModule_ProvideZendeskLocaleConverterFactory.create(providerModule));
        this.provideZendeskLocaleConverterProvider = a;
        this.provideSdkSettingsProvider = fq0.a(ProviderModule_ProvideSdkSettingsProviderFactory.create(providerModule, this.getSettingsProvider, this.provideLocaleProvider, a));
        this.getAuthenticationProvider = CoreModule_GetAuthenticationProviderFactory.create(coreModule);
        CoreModule_GetRestServiceProviderFactory create = CoreModule_GetRestServiceProviderFactory.create(coreModule);
        this.getRestServiceProvider = create;
        ew2<RequestService> a2 = fq0.a(ServiceModule_ProvidesRequestServiceFactory.create(create));
        this.providesRequestServiceProvider = a2;
        this.provideZendeskRequestServiceProvider = fq0.a(ServiceModule_ProvideZendeskRequestServiceFactory.create(a2));
        this.getSessionStorageProvider = CoreModule_GetSessionStorageFactory.create(coreModule);
        CoreModule_GetApplicationContextFactory create2 = CoreModule_GetApplicationContextFactory.create(coreModule);
        this.getApplicationContextProvider = create2;
        this.provideRequestMigratorProvider = fq0.a(StorageModule_ProvideRequestMigratorFactory.create(storageModule, create2));
        CoreModule_GetMemoryCacheFactory create3 = CoreModule_GetMemoryCacheFactory.create(coreModule);
        this.getMemoryCacheProvider = create3;
        this.provideRequestStorageProvider = fq0.a(StorageModule_ProvideRequestStorageFactory.create(storageModule, this.getSessionStorageProvider, this.provideRequestMigratorProvider, create3));
        this.provideRequestSessionCacheProvider = fq0.a(StorageModule_ProvideRequestSessionCacheFactory.create(storageModule));
        this.providesZendeskTrackerProvider = fq0.a(SupportApplicationModule_ProvidesZendeskTrackerFactory.create(supportApplicationModule));
        this.provideMetadataProvider = fq0.a(SupportApplicationModule_ProvideMetadataFactory.create(supportApplicationModule, this.getApplicationContextProvider));
        CoreModule_GetBlipsProviderFactory create4 = CoreModule_GetBlipsProviderFactory.create(coreModule);
        this.getBlipsProvider = create4;
        ew2<SupportBlipsProvider> a3 = fq0.a(ProviderModule_ProvideSupportBlipsProviderFactory.create(providerModule, create4));
        this.provideSupportBlipsProvider = a3;
        this.provideRequestProvider = fq0.a(ProviderModule_ProvideRequestProviderFactory.create(providerModule, this.provideSdkSettingsProvider, this.getAuthenticationProvider, this.provideZendeskRequestServiceProvider, this.provideRequestStorageProvider, this.provideRequestSessionCacheProvider, this.providesZendeskTrackerProvider, this.provideMetadataProvider, a3));
        ew2<UploadService> a4 = fq0.a(ServiceModule_ProvidesUploadServiceFactory.create(this.getRestServiceProvider));
        this.providesUploadServiceProvider = a4;
        ew2<ZendeskUploadService> a5 = fq0.a(ServiceModule_ProvideZendeskUploadServiceFactory.create(a4));
        this.provideZendeskUploadServiceProvider = a5;
        ew2<UploadProvider> a6 = fq0.a(ProviderModule_ProvideUploadProviderFactory.create(providerModule, a5));
        this.provideUploadProvider = a6;
        this.provideProviderStoreProvider = fq0.a(ProviderModule_ProvideProviderStoreFactory.create(providerModule, this.providesHelpCenterProvider, this.provideRequestProvider, a6));
        GuideModule_ProvidesArticleVoteStorageFactory create5 = GuideModule_ProvidesArticleVoteStorageFactory.create(guideModule);
        this.providesArticleVoteStorageProvider = create5;
        this.provideSupportModuleProvider = fq0.a(ProviderModule_ProvideSupportModuleFactory.create(providerModule, this.provideRequestProvider, this.provideUploadProvider, this.providesHelpCenterProvider, this.provideSdkSettingsProvider, this.getRestServiceProvider, this.provideSupportBlipsProvider, this.providesZendeskTrackerProvider, create5));
    }

    private Support injectSupport(Support support) {
        Support_MembersInjector.injectProviderStore(support, this.provideProviderStoreProvider.get());
        Support_MembersInjector.injectSupportModule(support, this.provideSupportModuleProvider.get());
        Support_MembersInjector.injectRequestMigrator(support, this.provideRequestMigratorProvider.get());
        Support_MembersInjector.injectBlipsProvider(support, this.provideSupportBlipsProvider.get());
        Support_MembersInjector.injectActionHandlerRegistry(support, CoreModule_ActionHandlerRegistryFactory.actionHandlerRegistry(this.coreModule));
        Support_MembersInjector.injectRequestProvider(support, this.provideRequestProvider.get());
        Support_MembersInjector.injectAuthenticationProvider(support, CoreModule_GetAuthenticationProviderFactory.getAuthenticationProvider(this.coreModule));
        return support;
    }

    @Override // zendesk.support.SupportSdkProvidersComponent
    public Support inject(Support support) {
        return injectSupport(support);
    }

    private DaggerSupportSdkProvidersComponent(SupportApplicationModule supportApplicationModule, CoreModule coreModule, ProviderModule providerModule, GuideModule guideModule, StorageModule storageModule) {
        this.supportSdkProvidersComponent = this;
        this.coreModule = coreModule;
        initialize(supportApplicationModule, coreModule, providerModule, guideModule, storageModule);
    }
}
