package zendesk.support;

import java.util.List;

/* loaded from: classes3.dex */
public interface RequestMigrator {
    void clearLegacyRequestStorage();

    List<RequestData> getLegacyRequests();
}
