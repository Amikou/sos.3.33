package zendesk.support;

import android.content.Context;
import com.squareup.picasso.Picasso;
import com.squareup.picasso.k;
import java.util.concurrent.ExecutorService;

/* loaded from: classes3.dex */
public final class SupportSdkModule_ProvidesPicassoFactory implements y11<Picasso> {
    private final ew2<Context> contextProvider;
    private final ew2<ExecutorService> executorServiceProvider;
    private final SupportSdkModule module;
    private final ew2<k> okHttp3DownloaderProvider;

    public SupportSdkModule_ProvidesPicassoFactory(SupportSdkModule supportSdkModule, ew2<Context> ew2Var, ew2<k> ew2Var2, ew2<ExecutorService> ew2Var3) {
        this.module = supportSdkModule;
        this.contextProvider = ew2Var;
        this.okHttp3DownloaderProvider = ew2Var2;
        this.executorServiceProvider = ew2Var3;
    }

    public static SupportSdkModule_ProvidesPicassoFactory create(SupportSdkModule supportSdkModule, ew2<Context> ew2Var, ew2<k> ew2Var2, ew2<ExecutorService> ew2Var3) {
        return new SupportSdkModule_ProvidesPicassoFactory(supportSdkModule, ew2Var, ew2Var2, ew2Var3);
    }

    public static Picasso providesPicasso(SupportSdkModule supportSdkModule, Context context, k kVar, ExecutorService executorService) {
        return (Picasso) cu2.f(supportSdkModule.providesPicasso(context, kVar, executorService));
    }

    @Override // defpackage.ew2
    public Picasso get() {
        return providesPicasso(this.module, this.contextProvider.get(), this.okHttp3DownloaderProvider.get(), this.executorServiceProvider.get());
    }
}
