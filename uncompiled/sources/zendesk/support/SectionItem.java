package zendesk.support;

import com.google.gson.annotations.SerializedName;
import java.util.ArrayList;
import java.util.List;
import org.web3j.ens.contracts.generated.PublicResolver;

/* loaded from: classes3.dex */
public class SectionItem implements HelpItem {
    private List<ArticleItem> articles;
    @SerializedName("category_id")
    private Long categoryId;
    @SerializedName(PublicResolver.FUNC_NAME)
    private String name;
    @SerializedName("id")
    private Long sectionId;
    @SerializedName("article_count")
    private int totalArticlesCount;

    public void addArticle(ArticleItem articleItem) {
        if (this.articles == null) {
            this.articles = new ArrayList();
        }
        this.articles.add(articleItem);
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null || getClass() != obj.getClass()) {
            return false;
        }
        SectionItem sectionItem = (SectionItem) obj;
        Long l = this.sectionId;
        if (l == null ? sectionItem.sectionId == null : l.equals(sectionItem.sectionId)) {
            Long l2 = this.categoryId;
            Long l3 = sectionItem.categoryId;
            return l2 != null ? l2.equals(l3) : l3 == null;
        }
        return false;
    }

    public List<HelpItem> getChildren() {
        ArrayList arrayList = new ArrayList();
        arrayList.addAll(this.articles);
        if (this.articles.size() < this.totalArticlesCount) {
            arrayList.add(new SeeAllArticlesItem(this));
        }
        return arrayList;
    }

    @Override // zendesk.support.HelpItem
    public Long getId() {
        return this.sectionId;
    }

    @Override // zendesk.support.HelpItem
    public String getName() {
        String str = this.name;
        return str == null ? "" : str;
    }

    @Override // zendesk.support.HelpItem
    public Long getParentId() {
        return this.categoryId;
    }

    public int getTotalArticlesCount() {
        return this.totalArticlesCount;
    }

    @Override // zendesk.support.HelpItem
    public int getViewType() {
        return 2;
    }

    public int hashCode() {
        Long l = this.sectionId;
        int hashCode = (l != null ? l.hashCode() : 0) * 31;
        Long l2 = this.categoryId;
        return hashCode + (l2 != null ? l2.hashCode() : 0);
    }
}
