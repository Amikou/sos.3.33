package zendesk.support;

/* loaded from: classes3.dex */
public interface ProviderStore {
    HelpCenterProvider helpCenterProvider();

    RequestProvider requestProvider();

    UploadProvider uploadProvider();
}
