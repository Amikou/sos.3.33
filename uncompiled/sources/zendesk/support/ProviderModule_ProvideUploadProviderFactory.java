package zendesk.support;

/* loaded from: classes3.dex */
public final class ProviderModule_ProvideUploadProviderFactory implements y11<UploadProvider> {
    private final ProviderModule module;
    private final ew2<ZendeskUploadService> uploadServiceProvider;

    public ProviderModule_ProvideUploadProviderFactory(ProviderModule providerModule, ew2<ZendeskUploadService> ew2Var) {
        this.module = providerModule;
        this.uploadServiceProvider = ew2Var;
    }

    public static ProviderModule_ProvideUploadProviderFactory create(ProviderModule providerModule, ew2<ZendeskUploadService> ew2Var) {
        return new ProviderModule_ProvideUploadProviderFactory(providerModule, ew2Var);
    }

    public static UploadProvider provideUploadProvider(ProviderModule providerModule, Object obj) {
        return (UploadProvider) cu2.f(providerModule.provideUploadProvider((ZendeskUploadService) obj));
    }

    @Override // defpackage.ew2
    public UploadProvider get() {
        return provideUploadProvider(this.module, this.uploadServiceProvider.get());
    }
}
