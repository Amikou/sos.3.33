package zendesk.support;

/* loaded from: classes3.dex */
class UpdateRequestWrapper {
    private Request request;

    public Request getRequest() {
        return this.request;
    }

    public void setRequest(Request request) {
        this.request = request;
    }
}
