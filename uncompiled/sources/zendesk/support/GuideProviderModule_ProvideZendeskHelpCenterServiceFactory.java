package zendesk.support;

import zendesk.core.ZendeskLocaleConverter;

/* loaded from: classes3.dex */
public final class GuideProviderModule_ProvideZendeskHelpCenterServiceFactory implements y11<ZendeskHelpCenterService> {
    private final ew2<HelpCenterService> helpCenterServiceProvider;
    private final ew2<ZendeskLocaleConverter> localeConverterProvider;

    public GuideProviderModule_ProvideZendeskHelpCenterServiceFactory(ew2<HelpCenterService> ew2Var, ew2<ZendeskLocaleConverter> ew2Var2) {
        this.helpCenterServiceProvider = ew2Var;
        this.localeConverterProvider = ew2Var2;
    }

    public static GuideProviderModule_ProvideZendeskHelpCenterServiceFactory create(ew2<HelpCenterService> ew2Var, ew2<ZendeskLocaleConverter> ew2Var2) {
        return new GuideProviderModule_ProvideZendeskHelpCenterServiceFactory(ew2Var, ew2Var2);
    }

    public static ZendeskHelpCenterService provideZendeskHelpCenterService(Object obj, ZendeskLocaleConverter zendeskLocaleConverter) {
        return (ZendeskHelpCenterService) cu2.f(GuideProviderModule.provideZendeskHelpCenterService((HelpCenterService) obj, zendeskLocaleConverter));
    }

    @Override // defpackage.ew2
    public ZendeskHelpCenterService get() {
        return provideZendeskHelpCenterService(this.helpCenterServiceProvider.get(), this.localeConverterProvider.get());
    }
}
