package zendesk.support;

/* loaded from: classes3.dex */
public interface HelpCenterTracker {

    /* loaded from: classes3.dex */
    public static class DefaultTracker implements HelpCenterTracker {
        @Override // zendesk.support.HelpCenterTracker
        public void helpCenterArticleViewed() {
        }

        @Override // zendesk.support.HelpCenterTracker
        public void helpCenterLoaded() {
        }

        @Override // zendesk.support.HelpCenterTracker
        public void helpCenterSearched(String str) {
        }
    }

    void helpCenterArticleViewed();

    void helpCenterLoaded();

    void helpCenterSearched(String str);
}
