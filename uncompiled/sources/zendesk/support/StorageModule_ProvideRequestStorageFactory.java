package zendesk.support;

import zendesk.core.MemoryCache;
import zendesk.core.SessionStorage;

/* loaded from: classes3.dex */
public final class StorageModule_ProvideRequestStorageFactory implements y11<RequestStorage> {
    private final ew2<SessionStorage> baseStorageProvider;
    private final ew2<MemoryCache> memoryCacheProvider;
    private final StorageModule module;
    private final ew2<RequestMigrator> requestMigratorProvider;

    public StorageModule_ProvideRequestStorageFactory(StorageModule storageModule, ew2<SessionStorage> ew2Var, ew2<RequestMigrator> ew2Var2, ew2<MemoryCache> ew2Var3) {
        this.module = storageModule;
        this.baseStorageProvider = ew2Var;
        this.requestMigratorProvider = ew2Var2;
        this.memoryCacheProvider = ew2Var3;
    }

    public static StorageModule_ProvideRequestStorageFactory create(StorageModule storageModule, ew2<SessionStorage> ew2Var, ew2<RequestMigrator> ew2Var2, ew2<MemoryCache> ew2Var3) {
        return new StorageModule_ProvideRequestStorageFactory(storageModule, ew2Var, ew2Var2, ew2Var3);
    }

    public static RequestStorage provideRequestStorage(StorageModule storageModule, SessionStorage sessionStorage, Object obj, MemoryCache memoryCache) {
        return (RequestStorage) cu2.f(storageModule.provideRequestStorage(sessionStorage, (RequestMigrator) obj, memoryCache));
    }

    @Override // defpackage.ew2
    public RequestStorage get() {
        return provideRequestStorage(this.module, this.baseStorageProvider.get(), this.requestMigratorProvider.get(), this.memoryCacheProvider.get());
    }
}
