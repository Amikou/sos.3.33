package zendesk.support;

/* loaded from: classes3.dex */
public enum RequestStatus {
    New,
    Open,
    Pending,
    Hold,
    Solved,
    Closed
}
