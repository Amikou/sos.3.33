package zendesk.support;

/* loaded from: classes3.dex */
public enum TicketFieldType {
    Checkbox,
    Date,
    Decimal,
    Description,
    Integer,
    PartialCreditCard,
    Priority,
    Status,
    TicketType,
    Regexp,
    Subject,
    Tagger,
    Text,
    TextArea,
    MultiSelect,
    Unknown
}
