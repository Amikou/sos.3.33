package zendesk.support;

import java.util.List;
import zendesk.core.ActionHandler;

/* loaded from: classes3.dex */
public final class SupportSdkModule_ProvidesActionHandlersFactory implements y11<List<ActionHandler>> {
    private final SupportSdkModule module;

    public SupportSdkModule_ProvidesActionHandlersFactory(SupportSdkModule supportSdkModule) {
        this.module = supportSdkModule;
    }

    public static SupportSdkModule_ProvidesActionHandlersFactory create(SupportSdkModule supportSdkModule) {
        return new SupportSdkModule_ProvidesActionHandlersFactory(supportSdkModule);
    }

    public static List<ActionHandler> providesActionHandlers(SupportSdkModule supportSdkModule) {
        return (List) cu2.f(supportSdkModule.providesActionHandlers());
    }

    @Override // defpackage.ew2
    public List<ActionHandler> get() {
        return providesActionHandlers(this.module);
    }
}
