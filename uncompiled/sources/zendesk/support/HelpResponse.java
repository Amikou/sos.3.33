package zendesk.support;

import com.google.gson.annotations.SerializedName;
import java.util.List;

/* JADX INFO: Access modifiers changed from: package-private */
/* loaded from: classes3.dex */
public class HelpResponse {
    private List<CategoryItem> categories;
    @SerializedName("category_count")
    private int categoryCount;

    public List<CategoryItem> getCategories() {
        return l10.c(this.categories);
    }

    public int getCategoryCount() {
        return this.categoryCount;
    }
}
