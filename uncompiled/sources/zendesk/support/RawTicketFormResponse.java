package zendesk.support;

import java.util.List;

/* loaded from: classes3.dex */
public class RawTicketFormResponse {
    private List<RawTicketField> ticketFields;
    private List<RawTicketForm> ticketForms;

    public List<RawTicketField> getTicketFields() {
        return l10.c(this.ticketFields);
    }

    public List<RawTicketForm> getTicketForms() {
        return l10.c(this.ticketForms);
    }
}
