package zendesk.support;

import android.content.Context;
import com.google.gson.Gson;
import com.squareup.picasso.Picasso;
import com.squareup.picasso.k;
import java.util.List;
import java.util.concurrent.Executor;
import java.util.concurrent.ExecutorService;
import okhttp3.OkHttpClient;
import zendesk.belvedere.a;
import zendesk.core.ActionHandler;
import zendesk.core.ActionHandlerRegistry;
import zendesk.core.AuthenticationProvider;
import zendesk.core.CoreModule;
import zendesk.core.CoreModule_ActionHandlerRegistryFactory;
import zendesk.core.CoreModule_GetApplicationContextFactory;
import zendesk.core.CoreModule_GetAuthenticationProviderFactory;
import zendesk.core.CoreModule_GetExecutorServiceFactory;
import zendesk.core.CoreModule_GetMemoryCacheFactory;
import zendesk.core.CoreModule_GetSessionStorageFactory;
import zendesk.core.MemoryCache;
import zendesk.core.SessionStorage;
import zendesk.support.request.RequestActivity;
import zendesk.support.request.RequestActivity_MembersInjector;
import zendesk.support.request.RequestComponent;
import zendesk.support.request.RequestModule;
import zendesk.support.request.RequestModule_ProvidesActionFactoryFactory;
import zendesk.support.request.RequestModule_ProvidesAsyncMiddlewareFactory;
import zendesk.support.request.RequestModule_ProvidesAttachmentDownloaderComponentFactory;
import zendesk.support.request.RequestModule_ProvidesAttachmentDownloaderFactory;
import zendesk.support.request.RequestModule_ProvidesAttachmentToDiskServiceFactory;
import zendesk.support.request.RequestModule_ProvidesBelvedereFactory;
import zendesk.support.request.RequestModule_ProvidesComponentListenerFactory;
import zendesk.support.request.RequestModule_ProvidesConUpdatesComponentFactory;
import zendesk.support.request.RequestModule_ProvidesDiskQueueFactory;
import zendesk.support.request.RequestModule_ProvidesDispatcherFactory;
import zendesk.support.request.RequestModule_ProvidesMessageFactoryFactory;
import zendesk.support.request.RequestModule_ProvidesPersistenceComponentFactory;
import zendesk.support.request.RequestModule_ProvidesReducerFactory;
import zendesk.support.request.RequestModule_ProvidesStoreFactory;
import zendesk.support.request.RequestViewConversationsDisabled;
import zendesk.support.request.RequestViewConversationsDisabled_MembersInjector;
import zendesk.support.request.RequestViewConversationsEnabled;
import zendesk.support.request.RequestViewConversationsEnabled_MembersInjector;
import zendesk.support.requestlist.RequestInfoDataSource;
import zendesk.support.requestlist.RequestListActivity;
import zendesk.support.requestlist.RequestListActivity_MembersInjector;
import zendesk.support.requestlist.RequestListComponent;
import zendesk.support.requestlist.RequestListModule;
import zendesk.support.requestlist.RequestListModule_ModelFactory;
import zendesk.support.requestlist.RequestListModule_PresenterFactory;
import zendesk.support.requestlist.RequestListModule_RefreshHandlerFactory;
import zendesk.support.requestlist.RequestListModule_RepositoryFactory;
import zendesk.support.requestlist.RequestListViewModule;
import zendesk.support.requestlist.RequestListViewModule_ViewFactory;
import zendesk.support.suas.Dispatcher;
import zendesk.support.suas.Reducer;
import zendesk.support.suas.Store;

/* loaded from: classes3.dex */
public final class DaggerSupportSdkComponent implements SupportSdkComponent {
    private ew2<ActionHandlerRegistry> actionHandlerRegistryProvider;
    private ew2<z40> configurationHelperProvider;
    private final CoreModule coreModule;
    private ew2<Context> getApplicationContextProvider;
    private ew2<AuthenticationProvider> getAuthenticationProvider;
    private ew2<ExecutorService> getExecutorServiceProvider;
    private ew2<MemoryCache> getMemoryCacheProvider;
    private ew2<SessionStorage> getSessionStorageProvider;
    private ew2<Executor> mainThreadExecutorProvider;
    private ew2<k> okHttp3DownloaderProvider;
    private ew2<List<ActionHandler>> providesActionHandlersProvider;
    private ew2<SupportBlipsProvider> providesBlipsProvider;
    private ew2<OkHttpClient> providesOkHttpClientProvider;
    private ew2<Picasso> providesPicassoProvider;
    private ew2<Gson> providesProvider;
    private ew2<ep0> providesRequestDiskLruCacheProvider;
    private ew2<RequestProvider> providesRequestProvider;
    private ew2<SupportSettingsProvider> providesSettingsProvider;
    private ew2<UploadProvider> providesUploadProvider;
    private ew2<RequestInfoDataSource.LocalDataSource> requestInfoDataSourceProvider;
    private final DaggerSupportSdkComponent supportSdkComponent;
    private ew2<SupportUiStorage> supportUiStorageProvider;

    /* loaded from: classes3.dex */
    public static final class Builder {
        private CoreModule coreModule;
        private SupportModule supportModule;
        private SupportSdkModule supportSdkModule;

        public SupportSdkComponent build() {
            cu2.a(this.coreModule, CoreModule.class);
            cu2.a(this.supportModule, SupportModule.class);
            if (this.supportSdkModule == null) {
                this.supportSdkModule = new SupportSdkModule();
            }
            return new DaggerSupportSdkComponent(this.coreModule, this.supportModule, this.supportSdkModule);
        }

        public Builder coreModule(CoreModule coreModule) {
            this.coreModule = (CoreModule) cu2.b(coreModule);
            return this;
        }

        public Builder supportModule(SupportModule supportModule) {
            this.supportModule = (SupportModule) cu2.b(supportModule);
            return this;
        }

        public Builder supportSdkModule(SupportSdkModule supportSdkModule) {
            this.supportSdkModule = (SupportSdkModule) cu2.b(supportSdkModule);
            return this;
        }

        private Builder() {
        }
    }

    /* loaded from: classes3.dex */
    public static final class RequestComponentImpl implements RequestComponent {
        private ew2 providesActionFactoryProvider;
        private ew2 providesAsyncMiddlewareProvider;
        private ew2 providesAttachmentDownloaderComponentProvider;
        private ew2 providesAttachmentDownloaderProvider;
        private ew2 providesAttachmentToDiskServiceProvider;
        private ew2<a> providesBelvedereProvider;
        private ew2 providesComponentListenerProvider;
        private ew2 providesConUpdatesComponentProvider;
        private ew2 providesDiskQueueProvider;
        private ew2<Dispatcher> providesDispatcherProvider;
        private ew2 providesMessageFactoryProvider;
        private ew2 providesPersistenceComponentProvider;
        private ew2<List<Reducer>> providesReducerProvider;
        private ew2<Store> providesStoreProvider;
        private final RequestComponentImpl requestComponentImpl;
        private final DaggerSupportSdkComponent supportSdkComponent;

        private void initialize(RequestModule requestModule) {
            this.providesReducerProvider = fq0.a(RequestModule_ProvidesReducerFactory.create());
            ew2 a = fq0.a(RequestModule_ProvidesAsyncMiddlewareFactory.create());
            this.providesAsyncMiddlewareProvider = a;
            this.providesStoreProvider = fq0.a(RequestModule_ProvidesStoreFactory.create(this.providesReducerProvider, a));
            this.providesBelvedereProvider = fq0.a(RequestModule_ProvidesBelvedereFactory.create(this.supportSdkComponent.getApplicationContextProvider));
            this.providesActionFactoryProvider = fq0.a(RequestModule_ProvidesActionFactoryFactory.create(this.supportSdkComponent.providesRequestProvider, this.supportSdkComponent.providesSettingsProvider, this.supportSdkComponent.providesUploadProvider, this.providesBelvedereProvider, this.supportSdkComponent.supportUiStorageProvider, this.supportSdkComponent.getExecutorServiceProvider, this.supportSdkComponent.mainThreadExecutorProvider, this.supportSdkComponent.getAuthenticationProvider, this.supportSdkComponent.providesBlipsProvider));
            this.providesDiskQueueProvider = fq0.a(RequestModule_ProvidesDiskQueueFactory.create(this.supportSdkComponent.getExecutorServiceProvider));
            this.providesPersistenceComponentProvider = fq0.a(RequestModule_ProvidesPersistenceComponentFactory.create(this.supportSdkComponent.supportUiStorageProvider, this.providesDiskQueueProvider, this.supportSdkComponent.getExecutorServiceProvider));
            this.providesDispatcherProvider = fq0.a(RequestModule_ProvidesDispatcherFactory.create(this.providesStoreProvider));
            ew2 a2 = fq0.a(RequestModule_ProvidesAttachmentToDiskServiceFactory.create(this.supportSdkComponent.providesOkHttpClientProvider, this.supportSdkComponent.getExecutorServiceProvider));
            this.providesAttachmentToDiskServiceProvider = a2;
            ew2 a3 = fq0.a(RequestModule_ProvidesAttachmentDownloaderFactory.create(this.providesBelvedereProvider, a2));
            this.providesAttachmentDownloaderProvider = a3;
            this.providesAttachmentDownloaderComponentProvider = fq0.a(RequestModule_ProvidesAttachmentDownloaderComponentFactory.create(this.providesDispatcherProvider, this.providesActionFactoryProvider, a3));
            ew2 a4 = tp3.a(RequestModule_ProvidesConUpdatesComponentFactory.create(this.supportSdkComponent.getApplicationContextProvider, this.supportSdkComponent.actionHandlerRegistryProvider, this.supportSdkComponent.requestInfoDataSourceProvider));
            this.providesConUpdatesComponentProvider = a4;
            this.providesComponentListenerProvider = fq0.a(RequestModule_ProvidesComponentListenerFactory.create(this.providesPersistenceComponentProvider, this.providesAttachmentDownloaderComponentProvider, a4));
            this.providesMessageFactoryProvider = fq0.a(RequestModule_ProvidesMessageFactoryFactory.create(requestModule, this.supportSdkComponent.getApplicationContextProvider, this.supportSdkComponent.providesPicassoProvider, this.providesActionFactoryProvider, this.providesDispatcherProvider, this.supportSdkComponent.actionHandlerRegistryProvider, this.supportSdkComponent.configurationHelperProvider));
        }

        private RequestActivity injectRequestActivity(RequestActivity requestActivity) {
            RequestActivity_MembersInjector.injectStore(requestActivity, this.providesStoreProvider.get());
            RequestActivity_MembersInjector.injectAf(requestActivity, this.providesActionFactoryProvider.get());
            RequestActivity_MembersInjector.injectHeadlessComponentListener(requestActivity, this.providesComponentListenerProvider.get());
            RequestActivity_MembersInjector.injectPicasso(requestActivity, (Picasso) this.supportSdkComponent.providesPicassoProvider.get());
            RequestActivity_MembersInjector.injectActionHandlerRegistry(requestActivity, CoreModule_ActionHandlerRegistryFactory.actionHandlerRegistry(this.supportSdkComponent.coreModule));
            return requestActivity;
        }

        private RequestViewConversationsDisabled injectRequestViewConversationsDisabled(RequestViewConversationsDisabled requestViewConversationsDisabled) {
            RequestViewConversationsDisabled_MembersInjector.injectStore(requestViewConversationsDisabled, this.providesStoreProvider.get());
            RequestViewConversationsDisabled_MembersInjector.injectAf(requestViewConversationsDisabled, this.providesActionFactoryProvider.get());
            RequestViewConversationsDisabled_MembersInjector.injectPicasso(requestViewConversationsDisabled, (Picasso) this.supportSdkComponent.providesPicassoProvider.get());
            return requestViewConversationsDisabled;
        }

        private RequestViewConversationsEnabled injectRequestViewConversationsEnabled(RequestViewConversationsEnabled requestViewConversationsEnabled) {
            RequestViewConversationsEnabled_MembersInjector.injectStore(requestViewConversationsEnabled, this.providesStoreProvider.get());
            RequestViewConversationsEnabled_MembersInjector.injectAf(requestViewConversationsEnabled, this.providesActionFactoryProvider.get());
            RequestViewConversationsEnabled_MembersInjector.injectCellFactory(requestViewConversationsEnabled, this.providesMessageFactoryProvider.get());
            RequestViewConversationsEnabled_MembersInjector.injectPicasso(requestViewConversationsEnabled, (Picasso) this.supportSdkComponent.providesPicassoProvider.get());
            return requestViewConversationsEnabled;
        }

        @Override // zendesk.support.request.RequestComponent
        public void inject(RequestActivity requestActivity) {
            injectRequestActivity(requestActivity);
        }

        private RequestComponentImpl(DaggerSupportSdkComponent daggerSupportSdkComponent, RequestModule requestModule) {
            this.requestComponentImpl = this;
            this.supportSdkComponent = daggerSupportSdkComponent;
            initialize(requestModule);
        }

        @Override // zendesk.support.request.RequestComponent
        public void inject(RequestViewConversationsEnabled requestViewConversationsEnabled) {
            injectRequestViewConversationsEnabled(requestViewConversationsEnabled);
        }

        @Override // zendesk.support.request.RequestComponent
        public void inject(RequestViewConversationsDisabled requestViewConversationsDisabled) {
            injectRequestViewConversationsDisabled(requestViewConversationsDisabled);
        }
    }

    /* loaded from: classes3.dex */
    public static final class RequestListComponentImpl implements RequestListComponent {
        private ew2 modelProvider;
        private ew2 presenterProvider;
        private ew2 refreshHandlerProvider;
        private ew2<RequestInfoDataSource.Repository> repositoryProvider;
        private final RequestListComponentImpl requestListComponentImpl;
        private final DaggerSupportSdkComponent supportSdkComponent;
        private ew2 viewProvider;

        private void initialize(RequestListModule requestListModule, RequestListViewModule requestListViewModule) {
            ew2<RequestInfoDataSource.Repository> a = fq0.a(RequestListModule_RepositoryFactory.create(this.supportSdkComponent.requestInfoDataSourceProvider, this.supportSdkComponent.supportUiStorageProvider, this.supportSdkComponent.providesRequestProvider, this.supportSdkComponent.mainThreadExecutorProvider, this.supportSdkComponent.getExecutorServiceProvider));
            this.repositoryProvider = a;
            ew2 a2 = fq0.a(RequestListModule_ModelFactory.create(requestListModule, a, this.supportSdkComponent.getMemoryCacheProvider, this.supportSdkComponent.providesBlipsProvider, this.supportSdkComponent.providesSettingsProvider));
            this.modelProvider = a2;
            this.presenterProvider = fq0.a(RequestListModule_PresenterFactory.create(requestListModule, a2));
            this.viewProvider = fq0.a(RequestListViewModule_ViewFactory.create(requestListViewModule, this.supportSdkComponent.providesPicassoProvider));
            this.refreshHandlerProvider = fq0.a(RequestListModule_RefreshHandlerFactory.create(this.presenterProvider));
        }

        private RequestListActivity injectRequestListActivity(RequestListActivity requestListActivity) {
            RequestListActivity_MembersInjector.injectPresenter(requestListActivity, this.presenterProvider.get());
            RequestListActivity_MembersInjector.injectView(requestListActivity, this.viewProvider.get());
            RequestListActivity_MembersInjector.injectModel(requestListActivity, this.modelProvider.get());
            RequestListActivity_MembersInjector.injectActionHandlerRegistry(requestListActivity, CoreModule_ActionHandlerRegistryFactory.actionHandlerRegistry(this.supportSdkComponent.coreModule));
            RequestListActivity_MembersInjector.injectSyncHandler(requestListActivity, this.refreshHandlerProvider.get());
            return requestListActivity;
        }

        @Override // zendesk.support.requestlist.RequestListComponent
        public void inject(RequestListActivity requestListActivity) {
            injectRequestListActivity(requestListActivity);
        }

        private RequestListComponentImpl(DaggerSupportSdkComponent daggerSupportSdkComponent, RequestListModule requestListModule, RequestListViewModule requestListViewModule) {
            this.requestListComponentImpl = this;
            this.supportSdkComponent = daggerSupportSdkComponent;
            initialize(requestListModule, requestListViewModule);
        }
    }

    public static Builder builder() {
        return new Builder();
    }

    private void initialize(CoreModule coreModule, SupportModule supportModule, SupportSdkModule supportSdkModule) {
        this.providesActionHandlersProvider = fq0.a(SupportSdkModule_ProvidesActionHandlersFactory.create(supportSdkModule));
        this.providesRequestProvider = SupportModule_ProvidesRequestProviderFactory.create(supportModule);
        this.providesSettingsProvider = SupportModule_ProvidesSettingsProviderFactory.create(supportModule);
        this.providesUploadProvider = SupportModule_ProvidesUploadProviderFactory.create(supportModule);
        this.getApplicationContextProvider = CoreModule_GetApplicationContextFactory.create(coreModule);
        CoreModule_GetSessionStorageFactory create = CoreModule_GetSessionStorageFactory.create(coreModule);
        this.getSessionStorageProvider = create;
        this.providesRequestDiskLruCacheProvider = fq0.a(SupportSdkModule_ProvidesRequestDiskLruCacheFactory.create(supportSdkModule, create));
        ew2<Gson> a = fq0.a(SupportSdkModule_ProvidesFactory.create(supportSdkModule));
        this.providesProvider = a;
        this.supportUiStorageProvider = fq0.a(SupportSdkModule_SupportUiStorageFactory.create(supportSdkModule, this.providesRequestDiskLruCacheProvider, a));
        this.getExecutorServiceProvider = CoreModule_GetExecutorServiceFactory.create(coreModule);
        this.mainThreadExecutorProvider = fq0.a(SupportSdkModule_MainThreadExecutorFactory.create(supportSdkModule));
        this.getAuthenticationProvider = CoreModule_GetAuthenticationProviderFactory.create(coreModule);
        this.providesBlipsProvider = SupportModule_ProvidesBlipsProviderFactory.create(supportModule);
        this.providesOkHttpClientProvider = SupportModule_ProvidesOkHttpClientFactory.create(supportModule);
        this.actionHandlerRegistryProvider = CoreModule_ActionHandlerRegistryFactory.create(coreModule);
        this.requestInfoDataSourceProvider = SupportSdkModule_RequestInfoDataSourceFactory.create(supportSdkModule, this.supportUiStorageProvider, this.mainThreadExecutorProvider, this.getExecutorServiceProvider);
        ew2<k> a2 = fq0.a(SupportSdkModule_OkHttp3DownloaderFactory.create(supportSdkModule, this.providesOkHttpClientProvider));
        this.okHttp3DownloaderProvider = a2;
        this.providesPicassoProvider = fq0.a(SupportSdkModule_ProvidesPicassoFactory.create(supportSdkModule, this.getApplicationContextProvider, a2, this.getExecutorServiceProvider));
        this.configurationHelperProvider = SupportSdkModule_ConfigurationHelperFactory.create(supportSdkModule);
        this.getMemoryCacheProvider = CoreModule_GetMemoryCacheFactory.create(coreModule);
    }

    private DeepLinkingBroadcastReceiver injectDeepLinkingBroadcastReceiver(DeepLinkingBroadcastReceiver deepLinkingBroadcastReceiver) {
        DeepLinkingBroadcastReceiver_MembersInjector.injectRegistry(deepLinkingBroadcastReceiver, CoreModule_ActionHandlerRegistryFactory.actionHandlerRegistry(this.coreModule));
        return deepLinkingBroadcastReceiver;
    }

    private SdkDependencyProvider injectSdkDependencyProvider(SdkDependencyProvider sdkDependencyProvider) {
        SdkDependencyProvider_MembersInjector.injectRegistry(sdkDependencyProvider, CoreModule_ActionHandlerRegistryFactory.actionHandlerRegistry(this.coreModule));
        SdkDependencyProvider_MembersInjector.injectActionHandlers(sdkDependencyProvider, this.providesActionHandlersProvider.get());
        return sdkDependencyProvider;
    }

    @Override // zendesk.support.SupportSdkComponent
    public void inject(SdkDependencyProvider sdkDependencyProvider) {
        injectSdkDependencyProvider(sdkDependencyProvider);
    }

    @Override // zendesk.support.SupportSdkComponent
    public RequestComponent plus(RequestModule requestModule) {
        cu2.b(requestModule);
        return new RequestComponentImpl(requestModule);
    }

    private DaggerSupportSdkComponent(CoreModule coreModule, SupportModule supportModule, SupportSdkModule supportSdkModule) {
        this.supportSdkComponent = this;
        this.coreModule = coreModule;
        initialize(coreModule, supportModule, supportSdkModule);
    }

    @Override // zendesk.support.SupportSdkComponent
    public void inject(DeepLinkingBroadcastReceiver deepLinkingBroadcastReceiver) {
        injectDeepLinkingBroadcastReceiver(deepLinkingBroadcastReceiver);
    }

    @Override // zendesk.support.SupportSdkComponent
    public RequestListComponent plus(RequestListModule requestListModule, RequestListViewModule requestListViewModule) {
        cu2.b(requestListModule);
        cu2.b(requestListViewModule);
        return new RequestListComponentImpl(requestListModule, requestListViewModule);
    }
}
