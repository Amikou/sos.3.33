package zendesk.support;

/* loaded from: classes3.dex */
public final class GuideModule_ProvidesHelpCenterProviderFactory implements y11<HelpCenterProvider> {
    private final GuideModule module;

    public GuideModule_ProvidesHelpCenterProviderFactory(GuideModule guideModule) {
        this.module = guideModule;
    }

    public static GuideModule_ProvidesHelpCenterProviderFactory create(GuideModule guideModule) {
        return new GuideModule_ProvidesHelpCenterProviderFactory(guideModule);
    }

    public static HelpCenterProvider providesHelpCenterProvider(GuideModule guideModule) {
        return (HelpCenterProvider) cu2.f(guideModule.providesHelpCenterProvider());
    }

    @Override // defpackage.ew2
    public HelpCenterProvider get() {
        return providesHelpCenterProvider(this.module);
    }
}
