package zendesk.support;

/* loaded from: classes3.dex */
public final class RequestData {
    private final int commentCount;
    private final String id;
    private int readCommentCount;

    private RequestData(String str, int i, int i2) {
        this.commentCount = i;
        this.id = str;
        this.readCommentCount = i2;
    }

    public static RequestData create(Request request) {
        return new RequestData(request.getId(), request.getCommentCount().intValue(), 0);
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null || RequestData.class != obj.getClass()) {
            return false;
        }
        String str = this.id;
        String str2 = ((RequestData) obj).id;
        return str != null ? str.equals(str2) : str2 == null;
    }

    public int getCommentCount() {
        return this.commentCount;
    }

    public String getId() {
        return this.id;
    }

    public int getReadCommentCount() {
        return this.readCommentCount;
    }

    public int hashCode() {
        String str = this.id;
        if (str != null) {
            return str.hashCode();
        }
        return 0;
    }

    public String toString() {
        return "RequestData{commentCount=" + this.commentCount + "readCommentCount=" + this.readCommentCount + ", id='" + this.id + "'}";
    }

    public int unreadComments() {
        return this.commentCount - this.readCommentCount;
    }

    public static RequestData create(String str, int i, int i2) {
        return new RequestData(str, i, i2);
    }
}
