package zendesk.support;

/* loaded from: classes3.dex */
public final class SupportModule_ProvidesBlipsProviderFactory implements y11<SupportBlipsProvider> {
    private final SupportModule module;

    public SupportModule_ProvidesBlipsProviderFactory(SupportModule supportModule) {
        this.module = supportModule;
    }

    public static SupportModule_ProvidesBlipsProviderFactory create(SupportModule supportModule) {
        return new SupportModule_ProvidesBlipsProviderFactory(supportModule);
    }

    public static SupportBlipsProvider providesBlipsProvider(SupportModule supportModule) {
        return (SupportBlipsProvider) cu2.f(supportModule.providesBlipsProvider());
    }

    @Override // defpackage.ew2
    public SupportBlipsProvider get() {
        return providesBlipsProvider(this.module);
    }
}
