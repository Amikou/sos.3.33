package zendesk.support;

/* loaded from: classes3.dex */
public interface ZendeskTracker {

    /* loaded from: classes3.dex */
    public static class DefaultTracker implements ZendeskTracker {
        @Override // zendesk.support.ZendeskTracker
        public void requestCreated() {
        }

        @Override // zendesk.support.ZendeskTracker
        public void requestUpdated() {
        }
    }

    void requestCreated();

    void requestUpdated();
}
