package zendesk.support;

import android.annotation.SuppressLint;
import com.zendesk.logger.Logger;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;

/* loaded from: classes3.dex */
public class ZendeskHelpCenterProvider implements HelpCenterProvider {
    private static final String EMPTY_JSON_BODY = "{}";
    private static final String LOG_TAG = "ZendeskHelpCenterProvider";
    private final HelpCenterBlipsProvider blipsProvider;
    private final ZendeskHelpCenterService helpCenterService;
    private final HelpCenterSessionCache helpCenterSessionCache;
    private final HelpCenterTracker helpCenterTracker;
    private final HelpCenterSettingsProvider settingsProvider;

    /* loaded from: classes3.dex */
    public static abstract class ZendeskCallbackSuccess<E> extends rs4<E> {
        private final rs4 callback;

        public ZendeskCallbackSuccess(rs4 rs4Var) {
            this.callback = rs4Var;
        }

        @Override // defpackage.rs4
        public void onError(cw0 cw0Var) {
            rs4 rs4Var = this.callback;
            if (rs4Var != null) {
                rs4Var.onError(cw0Var);
            }
        }

        @Override // defpackage.rs4
        public abstract void onSuccess(E e);
    }

    public ZendeskHelpCenterProvider(HelpCenterSettingsProvider helpCenterSettingsProvider, HelpCenterBlipsProvider helpCenterBlipsProvider, ZendeskHelpCenterService zendeskHelpCenterService, HelpCenterSessionCache helpCenterSessionCache, HelpCenterTracker helpCenterTracker) {
        this.settingsProvider = helpCenterSettingsProvider;
        this.blipsProvider = helpCenterBlipsProvider;
        this.helpCenterService = zendeskHelpCenterService;
        this.helpCenterSessionCache = helpCenterSessionCache;
        this.helpCenterTracker = helpCenterTracker;
    }

    /* JADX INFO: Access modifiers changed from: private */
    public boolean checkSettingsAndVotingEnabled(rs4<?> rs4Var, HelpCenterSettings helpCenterSettings) {
        if (!sanityCheckHelpCenterSettings(rs4Var, helpCenterSettings)) {
            if (helpCenterSettings.isArticleVotingEnabled()) {
                return true;
            }
            Logger.e(LOG_TAG, "Help Center voting is disabled in your app's settings. Can not continue with the call", new Object[0]);
            if (rs4Var != null) {
                rs4Var.onError(new dw0("Help Center voting is disabled in your app's settings. Can not continue with the call"));
            }
        }
        return false;
    }

    /* JADX INFO: Access modifiers changed from: private */
    public List<HelpItem> convert(HelpResponse helpResponse) {
        if (helpResponse == null) {
            return Collections.emptyList();
        }
        ArrayList arrayList = new ArrayList();
        for (CategoryItem categoryItem : helpResponse.getCategories()) {
            arrayList.add(categoryItem);
            for (SectionItem sectionItem : categoryItem.getSections()) {
                arrayList.add(sectionItem);
                arrayList.addAll(sectionItem.getChildren());
            }
        }
        return arrayList;
    }

    @SuppressLint({"UseSparseArrays"})
    public List<FlatArticle> asFlatArticleList(ArticlesResponse articlesResponse) {
        if (articlesResponse == null) {
            return new ArrayList();
        }
        List<Category> categories = articlesResponse.getCategories();
        List<Section> sections = articlesResponse.getSections();
        List<Article> articles = articlesResponse.getArticles();
        HashMap hashMap = new HashMap();
        HashMap hashMap2 = new HashMap();
        List<FlatArticle> arrayList = new ArrayList<>();
        if (l10.i(articles)) {
            for (Category category : categories) {
                hashMap.put(category.getId(), category);
            }
            for (Section section : sections) {
                hashMap2.put(section.getId(), section);
            }
            for (Article article : articles) {
                Section section2 = (Section) hashMap2.get(article.getSectionId());
                arrayList.add(new FlatArticle((Category) hashMap.get(section2.getCategoryId()), section2, article));
            }
        } else {
            Logger.b(LOG_TAG, "There are no articles contained in this account", new Object[0]);
            arrayList = Collections.emptyList();
        }
        Collections.sort(arrayList);
        return arrayList;
    }

    @SuppressLint({"UseSparseArrays"})
    public List<SearchArticle> asSearchArticleList(ArticlesResponse articlesResponse) {
        Section section;
        ArrayList arrayList = new ArrayList();
        if (articlesResponse == null) {
            return arrayList;
        }
        HashMap hashMap = new HashMap();
        HashMap hashMap2 = new HashMap();
        HashMap hashMap3 = new HashMap();
        List<Article> e = l10.e(articlesResponse.getArticles());
        List<Section> e2 = l10.e(articlesResponse.getSections());
        List<Category> e3 = l10.e(articlesResponse.getCategories());
        List<zendesk.core.User> e4 = l10.e(articlesResponse.getUsers());
        for (Section section2 : e2) {
            if (section2.getId() != null) {
                hashMap.put(section2.getId(), section2);
            }
        }
        for (Category category : e3) {
            if (category.getId() != null) {
                hashMap2.put(category.getId(), category);
            }
        }
        for (zendesk.core.User user : e4) {
            if (user.getId() != null) {
                hashMap3.put(user.getId(), user);
            }
        }
        for (Article article : e) {
            Category category2 = null;
            if (article.getSectionId() != null) {
                section = (Section) hashMap.get(article.getSectionId());
            } else {
                Logger.k(LOG_TAG, "Unable to determine section as section id was null.", new Object[0]);
                section = null;
            }
            if (section != null && section.getCategoryId() != null) {
                category2 = (Category) hashMap2.get(section.getCategoryId());
            } else {
                Logger.k(LOG_TAG, "Unable to determine category as section was null.", new Object[0]);
            }
            if (article.getAuthorId() != null) {
                article.setAuthor((zendesk.core.User) hashMap3.get(article.getAuthorId()));
            } else {
                Logger.k(LOG_TAG, "Unable to determine author as author id was null.", new Object[0]);
            }
            arrayList.add(new SearchArticle(article, section, category2));
        }
        return arrayList;
    }

    @Override // zendesk.support.HelpCenterProvider
    public void deleteVote(final Long l, final rs4<Void> rs4Var) {
        if (sanityCheck(rs4Var, l)) {
            return;
        }
        this.settingsProvider.getSettings(new ZendeskCallbackSuccess<HelpCenterSettings>(rs4Var) { // from class: zendesk.support.ZendeskHelpCenterProvider.14
            @Override // zendesk.support.ZendeskHelpCenterProvider.ZendeskCallbackSuccess, defpackage.rs4
            public void onSuccess(HelpCenterSettings helpCenterSettings) {
                if (ZendeskHelpCenterProvider.this.checkSettingsAndVotingEnabled(rs4Var, helpCenterSettings)) {
                    ZendeskHelpCenterProvider.this.helpCenterService.deleteVote(l, new ZendeskCallbackSuccess<Void>(rs4Var) { // from class: zendesk.support.ZendeskHelpCenterProvider.14.1
                        @Override // zendesk.support.ZendeskHelpCenterProvider.ZendeskCallbackSuccess, defpackage.rs4
                        public void onSuccess(Void r2) {
                            rs4 rs4Var2 = rs4Var;
                            if (rs4Var2 != null) {
                                rs4Var2.onSuccess(r2);
                            }
                        }
                    });
                }
            }
        });
    }

    @Override // zendesk.support.HelpCenterProvider
    public void downvoteArticle(final Long l, final rs4<ArticleVote> rs4Var) {
        if (sanityCheck(rs4Var, l)) {
            return;
        }
        this.settingsProvider.getSettings(new ZendeskCallbackSuccess<HelpCenterSettings>(rs4Var) { // from class: zendesk.support.ZendeskHelpCenterProvider.13
            @Override // zendesk.support.ZendeskHelpCenterProvider.ZendeskCallbackSuccess, defpackage.rs4
            public void onSuccess(HelpCenterSettings helpCenterSettings) {
                if (ZendeskHelpCenterProvider.this.checkSettingsAndVotingEnabled(rs4Var, helpCenterSettings)) {
                    ZendeskHelpCenterProvider.this.helpCenterService.downvoteArticle(l, ZendeskHelpCenterProvider.EMPTY_JSON_BODY, new ZendeskCallbackSuccess<ArticleVoteResponse>(rs4Var) { // from class: zendesk.support.ZendeskHelpCenterProvider.13.1
                        @Override // zendesk.support.ZendeskHelpCenterProvider.ZendeskCallbackSuccess, defpackage.rs4
                        public void onSuccess(ArticleVoteResponse articleVoteResponse) {
                            rs4 rs4Var2 = rs4Var;
                            if (rs4Var2 != null) {
                                rs4Var2.onSuccess(articleVoteResponse.getVote());
                            }
                            ZendeskHelpCenterProvider.this.blipsProvider.articleVote(l, -1);
                        }
                    });
                }
            }
        });
    }

    @Override // zendesk.support.HelpCenterProvider
    public void getArticle(final Long l, final rs4<Article> rs4Var) {
        if (sanityCheck(rs4Var, l)) {
            return;
        }
        this.settingsProvider.getSettings(new ZendeskCallbackSuccess<HelpCenterSettings>(rs4Var) { // from class: zendesk.support.ZendeskHelpCenterProvider.8
            @Override // zendesk.support.ZendeskHelpCenterProvider.ZendeskCallbackSuccess, defpackage.rs4
            public void onSuccess(HelpCenterSettings helpCenterSettings) {
                if (ZendeskHelpCenterProvider.this.sanityCheckHelpCenterSettings(rs4Var, helpCenterSettings)) {
                    return;
                }
                ZendeskHelpCenterProvider.this.helpCenterService.getArticle(l, ZendeskHelpCenterProvider.this.getLocale(helpCenterSettings), "users", new ZendeskCallbackSuccess<Article>(rs4Var) { // from class: zendesk.support.ZendeskHelpCenterProvider.8.1
                    @Override // zendesk.support.ZendeskHelpCenterProvider.ZendeskCallbackSuccess, defpackage.rs4
                    public void onSuccess(Article article) {
                        ZendeskHelpCenterProvider.this.submitRecordArticleView(article, h12.c(article.getLocale()), new rs4<Void>() { // from class: zendesk.support.ZendeskHelpCenterProvider.8.1.1
                            @Override // defpackage.rs4
                            public void onError(cw0 cw0Var) {
                                Logger.e(ZendeskHelpCenterProvider.LOG_TAG, "Error submitting Help Center reporting: [reason] %s [isNetworkError] %s [status] %d", cw0Var.h(), Boolean.valueOf(cw0Var.i()), Integer.valueOf(cw0Var.e()));
                            }

                            @Override // defpackage.rs4
                            public void onSuccess(Void r1) {
                            }
                        });
                        rs4 rs4Var2 = rs4Var;
                        if (rs4Var2 != null) {
                            rs4Var2.onSuccess(article);
                        }
                    }
                });
            }
        });
    }

    @Override // zendesk.support.HelpCenterProvider
    public void getArticles(Long l, rs4<List<Article>> rs4Var) {
        getArticles(l, null, rs4Var);
    }

    @Override // zendesk.support.HelpCenterProvider
    public void getAttachments(final Long l, final AttachmentType attachmentType, final rs4<List<HelpCenterAttachment>> rs4Var) {
        if (sanityCheck(rs4Var, l, attachmentType)) {
            return;
        }
        this.settingsProvider.getSettings(new ZendeskCallbackSuccess<HelpCenterSettings>(rs4Var) { // from class: zendesk.support.ZendeskHelpCenterProvider.11
            @Override // zendesk.support.ZendeskHelpCenterProvider.ZendeskCallbackSuccess, defpackage.rs4
            public void onSuccess(HelpCenterSettings helpCenterSettings) {
                if (ZendeskHelpCenterProvider.this.sanityCheckHelpCenterSettings(rs4Var, helpCenterSettings)) {
                    return;
                }
                ZendeskHelpCenterProvider.this.helpCenterService.getAttachments(ZendeskHelpCenterProvider.this.getLocale(helpCenterSettings), l, attachmentType, rs4Var);
            }
        });
    }

    @Override // zendesk.support.HelpCenterProvider
    public void getCategories(final rs4<List<Category>> rs4Var) {
        if (sanityCheck(rs4Var, new Object[0])) {
            return;
        }
        this.settingsProvider.getSettings(new ZendeskCallbackSuccess<HelpCenterSettings>(rs4Var) { // from class: zendesk.support.ZendeskHelpCenterProvider.2
            @Override // zendesk.support.ZendeskHelpCenterProvider.ZendeskCallbackSuccess, defpackage.rs4
            public void onSuccess(HelpCenterSettings helpCenterSettings) {
                if (ZendeskHelpCenterProvider.this.sanityCheckHelpCenterSettings(rs4Var, helpCenterSettings)) {
                    return;
                }
                ZendeskHelpCenterProvider.this.helpCenterService.getCategories(ZendeskHelpCenterProvider.this.getLocale(helpCenterSettings), rs4Var);
            }
        });
    }

    @Override // zendesk.support.HelpCenterProvider
    public void getCategory(final Long l, final rs4<Category> rs4Var) {
        if (sanityCheck(rs4Var, l)) {
            return;
        }
        this.settingsProvider.getSettings(new ZendeskCallbackSuccess<HelpCenterSettings>(rs4Var) { // from class: zendesk.support.ZendeskHelpCenterProvider.10
            @Override // zendesk.support.ZendeskHelpCenterProvider.ZendeskCallbackSuccess, defpackage.rs4
            public void onSuccess(HelpCenterSettings helpCenterSettings) {
                if (ZendeskHelpCenterProvider.this.sanityCheckHelpCenterSettings(rs4Var, helpCenterSettings)) {
                    return;
                }
                ZendeskHelpCenterProvider.this.helpCenterService.getCategoryById(l, ZendeskHelpCenterProvider.this.getLocale(helpCenterSettings), rs4Var);
            }
        });
    }

    @Override // zendesk.support.HelpCenterProvider
    public void getHelp(final HelpRequest helpRequest, final rs4<List<HelpItem>> rs4Var) {
        this.settingsProvider.getSettings(new ZendeskCallbackSuccess<HelpCenterSettings>(rs4Var) { // from class: zendesk.support.ZendeskHelpCenterProvider.1
            @Override // zendesk.support.ZendeskHelpCenterProvider.ZendeskCallbackSuccess, defpackage.rs4
            public void onSuccess(HelpCenterSettings helpCenterSettings) {
                if (ZendeskHelpCenterProvider.this.sanityCheckHelpCenterSettings(rs4Var, helpCenterSettings)) {
                    return;
                }
                ZendeskHelpCenterProvider.this.helpCenterService.getHelp(ZendeskHelpCenterProvider.this.getLocale(helpCenterSettings), helpRequest.getCategoryIds(), helpRequest.getSectionIds(), helpRequest.getIncludes(), helpRequest.getArticlesPerPageLimit(), ru3.g(helpRequest.getLabelNames()), new ZendeskCallbackSuccess<HelpResponse>(rs4Var) { // from class: zendesk.support.ZendeskHelpCenterProvider.1.1
                    @Override // zendesk.support.ZendeskHelpCenterProvider.ZendeskCallbackSuccess, defpackage.rs4
                    public void onSuccess(HelpResponse helpResponse) {
                        ZendeskHelpCenterProvider.this.helpCenterTracker.helpCenterLoaded();
                        AnonymousClass1 anonymousClass1 = AnonymousClass1.this;
                        rs4 rs4Var2 = rs4Var;
                        if (rs4Var2 != null) {
                            rs4Var2.onSuccess(ZendeskHelpCenterProvider.this.convert(helpResponse));
                        }
                    }
                });
            }
        });
    }

    public Locale getLocale(HelpCenterSettings helpCenterSettings) {
        Guide guide = Guide.INSTANCE;
        if (guide.getHelpCenterLocaleOverride() != null) {
            return guide.getHelpCenterLocaleOverride();
        }
        String locale = helpCenterSettings != null ? helpCenterSettings.getLocale() : "";
        return ru3.d(locale) ? Locale.getDefault() : h12.c(locale);
    }

    @Override // zendesk.support.HelpCenterProvider
    public void getSection(final Long l, final rs4<Section> rs4Var) {
        if (sanityCheck(rs4Var, l)) {
            return;
        }
        this.settingsProvider.getSettings(new ZendeskCallbackSuccess<HelpCenterSettings>(rs4Var) { // from class: zendesk.support.ZendeskHelpCenterProvider.9
            @Override // zendesk.support.ZendeskHelpCenterProvider.ZendeskCallbackSuccess, defpackage.rs4
            public void onSuccess(HelpCenterSettings helpCenterSettings) {
                if (ZendeskHelpCenterProvider.this.sanityCheckHelpCenterSettings(rs4Var, helpCenterSettings)) {
                    return;
                }
                ZendeskHelpCenterProvider.this.helpCenterService.getSectionById(l, ZendeskHelpCenterProvider.this.getLocale(helpCenterSettings), rs4Var);
            }
        });
    }

    @Override // zendesk.support.HelpCenterProvider
    public void getSections(final Long l, final rs4<List<Section>> rs4Var) {
        if (sanityCheck(rs4Var, l)) {
            return;
        }
        this.settingsProvider.getSettings(new ZendeskCallbackSuccess<HelpCenterSettings>(rs4Var) { // from class: zendesk.support.ZendeskHelpCenterProvider.3
            @Override // zendesk.support.ZendeskHelpCenterProvider.ZendeskCallbackSuccess, defpackage.rs4
            public void onSuccess(HelpCenterSettings helpCenterSettings) {
                if (ZendeskHelpCenterProvider.this.sanityCheckHelpCenterSettings(rs4Var, helpCenterSettings)) {
                    return;
                }
                ZendeskHelpCenterProvider.this.helpCenterService.getSectionsForCategory(l, ZendeskHelpCenterProvider.this.getLocale(helpCenterSettings), rs4Var);
            }
        });
    }

    @Override // zendesk.support.HelpCenterProvider
    public void getSuggestedArticles(final SuggestedArticleSearch suggestedArticleSearch, final rs4<Object> rs4Var) {
        if (sanityCheck(rs4Var, suggestedArticleSearch)) {
            return;
        }
        this.settingsProvider.getSettings(new ZendeskCallbackSuccess<HelpCenterSettings>(rs4Var) { // from class: zendesk.support.ZendeskHelpCenterProvider.15
            @Override // zendesk.support.ZendeskHelpCenterProvider.ZendeskCallbackSuccess, defpackage.rs4
            public void onSuccess(HelpCenterSettings helpCenterSettings) {
                if (ZendeskHelpCenterProvider.this.sanityCheckHelpCenterSettings(rs4Var, helpCenterSettings)) {
                    return;
                }
                ZendeskHelpCenterProvider.this.helpCenterService.getSuggestedArticles(suggestedArticleSearch.getQuery(), suggestedArticleSearch.getLocale() == null ? ZendeskHelpCenterProvider.this.getLocale(helpCenterSettings) : suggestedArticleSearch.getLocale(), ru3.d(suggestedArticleSearch.getLabelNames()) ? null : ru3.g(suggestedArticleSearch.getLabelNames()), suggestedArticleSearch.getCategoryId(), suggestedArticleSearch.getSectionId(), rs4Var);
            }
        });
    }

    @Override // zendesk.support.HelpCenterProvider
    public void listArticles(final ListArticleQuery listArticleQuery, final rs4<List<SearchArticle>> rs4Var) {
        if (sanityCheck(rs4Var, listArticleQuery)) {
            return;
        }
        this.settingsProvider.getSettings(new ZendeskCallbackSuccess<HelpCenterSettings>(rs4Var) { // from class: zendesk.support.ZendeskHelpCenterProvider.5
            @Override // zendesk.support.ZendeskHelpCenterProvider.ZendeskCallbackSuccess, defpackage.rs4
            public void onSuccess(HelpCenterSettings helpCenterSettings) {
                String include;
                if (ZendeskHelpCenterProvider.this.sanityCheckHelpCenterSettings(rs4Var, helpCenterSettings)) {
                    return;
                }
                if (listArticleQuery.getInclude() == null) {
                    include = ru3.g("categories", "sections", "users");
                } else {
                    include = listArticleQuery.getInclude();
                }
                String str = include;
                ZendeskHelpCenterProvider.this.helpCenterService.listArticles(ru3.g(listArticleQuery.getLabelNames()), listArticleQuery.getLocale() == null ? ZendeskHelpCenterProvider.this.getLocale(helpCenterSettings) : listArticleQuery.getLocale(), str, (listArticleQuery.getSortBy() == null ? SortBy.CREATED_AT : listArticleQuery.getSortBy()).getApiValue(), (listArticleQuery.getSortOrder() == null ? SortOrder.DESCENDING : listArticleQuery.getSortOrder()).getApiValue(), listArticleQuery.getPage(), listArticleQuery.getResultsPerPage(), new ZendeskCallbackSuccess<ArticlesListResponse>(rs4Var) { // from class: zendesk.support.ZendeskHelpCenterProvider.5.1
                    @Override // zendesk.support.ZendeskHelpCenterProvider.ZendeskCallbackSuccess, defpackage.rs4
                    public void onSuccess(ArticlesListResponse articlesListResponse) {
                        List<SearchArticle> asSearchArticleList = ZendeskHelpCenterProvider.this.asSearchArticleList(articlesListResponse);
                        rs4 rs4Var2 = rs4Var;
                        if (rs4Var2 != null) {
                            rs4Var2.onSuccess(asSearchArticleList);
                        }
                    }
                });
            }
        });
    }

    @Override // zendesk.support.HelpCenterProvider
    public void listArticlesFlat(final ListArticleQuery listArticleQuery, final rs4<List<FlatArticle>> rs4Var) {
        if (sanityCheck(rs4Var, listArticleQuery)) {
            return;
        }
        this.settingsProvider.getSettings(new ZendeskCallbackSuccess<HelpCenterSettings>(rs4Var) { // from class: zendesk.support.ZendeskHelpCenterProvider.6
            @Override // zendesk.support.ZendeskHelpCenterProvider.ZendeskCallbackSuccess, defpackage.rs4
            public void onSuccess(HelpCenterSettings helpCenterSettings) {
                if (ZendeskHelpCenterProvider.this.sanityCheckHelpCenterSettings(rs4Var, helpCenterSettings)) {
                    return;
                }
                ZendeskHelpCenterProvider.this.helpCenterService.listArticles(ru3.g(listArticleQuery.getLabelNames()), listArticleQuery.getLocale() == null ? ZendeskHelpCenterProvider.this.getLocale(helpCenterSettings) : listArticleQuery.getLocale(), "categories,sections", (listArticleQuery.getSortBy() == null ? SortBy.CREATED_AT : listArticleQuery.getSortBy()).getApiValue(), (listArticleQuery.getSortOrder() == null ? SortOrder.DESCENDING : listArticleQuery.getSortOrder()).getApiValue(), listArticleQuery.getPage(), listArticleQuery.getResultsPerPage(), new ZendeskCallbackSuccess<ArticlesListResponse>(rs4Var) { // from class: zendesk.support.ZendeskHelpCenterProvider.6.1
                    @Override // zendesk.support.ZendeskHelpCenterProvider.ZendeskCallbackSuccess, defpackage.rs4
                    public void onSuccess(ArticlesListResponse articlesListResponse) {
                        List<FlatArticle> asFlatArticleList = ZendeskHelpCenterProvider.this.asFlatArticleList(articlesListResponse);
                        rs4 rs4Var2 = rs4Var;
                        if (rs4Var2 != null) {
                            rs4Var2.onSuccess(asFlatArticleList);
                        }
                    }
                });
            }
        });
    }

    public boolean sanityCheck(rs4<?> rs4Var, Object... objArr) {
        if (objArr != null) {
            boolean z = true;
            for (Object obj : objArr) {
                if (obj == null) {
                    z = false;
                }
            }
            if (!z) {
                Logger.e(LOG_TAG, "One or more provided parameters are null.", new Object[0]);
                if (rs4Var != null) {
                    rs4Var.onError(new dw0("One or more provided parameters are null."));
                }
                return true;
            }
        }
        return false;
    }

    public boolean sanityCheckHelpCenterSettings(rs4<?> rs4Var, HelpCenterSettings helpCenterSettings) {
        if (helpCenterSettings == null) {
            Logger.e(LOG_TAG, "Help Center settings are null. Can not continue with the call", new Object[0]);
            if (rs4Var != null) {
                rs4Var.onError(new dw0("Help Center settings are null. Can not continue with the call"));
            }
            return true;
        } else if (helpCenterSettings.isEnabled()) {
            return false;
        } else {
            Logger.e(LOG_TAG, "Help Center is disabled in your app's settings. Can not continue with the call", new Object[0]);
            if (rs4Var != null) {
                rs4Var.onError(new dw0("Help Center is disabled in your app's settings. Can not continue with the call"));
            }
            return true;
        }
    }

    @Override // zendesk.support.HelpCenterProvider
    public void searchArticles(final HelpCenterSearch helpCenterSearch, final rs4<List<SearchArticle>> rs4Var) {
        if (sanityCheck(rs4Var, helpCenterSearch)) {
            return;
        }
        this.blipsProvider.helpCenterSearch(helpCenterSearch.getQuery());
        this.settingsProvider.getSettings(new ZendeskCallbackSuccess<HelpCenterSettings>(rs4Var) { // from class: zendesk.support.ZendeskHelpCenterProvider.7
            @Override // zendesk.support.ZendeskHelpCenterProvider.ZendeskCallbackSuccess, defpackage.rs4
            public void onSuccess(HelpCenterSettings helpCenterSettings) {
                Locale locale;
                if (ZendeskHelpCenterProvider.this.sanityCheckHelpCenterSettings(rs4Var, helpCenterSettings)) {
                    return;
                }
                String g = ru3.d(helpCenterSearch.getInclude()) ? ru3.g("categories", "sections", "users") : ru3.g(helpCenterSearch.getInclude());
                String g2 = ru3.d(helpCenterSearch.getLabelNames()) ? null : ru3.g(helpCenterSearch.getLabelNames());
                if (helpCenterSearch.getLocale() == null) {
                    locale = ZendeskHelpCenterProvider.this.getLocale(helpCenterSettings);
                } else {
                    locale = helpCenterSearch.getLocale();
                }
                ZendeskHelpCenterProvider.this.helpCenterService.searchArticles(helpCenterSearch.getQuery(), locale, g, g2, helpCenterSearch.getCategoryIds(), helpCenterSearch.getSectionIds(), helpCenterSearch.getPage(), helpCenterSearch.getPerPage(), new ZendeskCallbackSuccess<ArticlesSearchResponse>(rs4Var) { // from class: zendesk.support.ZendeskHelpCenterProvider.7.1
                    @Override // zendesk.support.ZendeskHelpCenterProvider.ZendeskCallbackSuccess, defpackage.rs4
                    public void onSuccess(ArticlesSearchResponse articlesSearchResponse) {
                        ZendeskHelpCenterProvider.this.helpCenterTracker.helpCenterSearched(helpCenterSearch.getQuery());
                        ZendeskHelpCenterProvider.this.helpCenterSessionCache.setLastSearch(helpCenterSearch.getQuery(), (articlesSearchResponse == null || !l10.i(articlesSearchResponse.getArticles())) ? 0 : articlesSearchResponse.getArticles().size());
                        List<SearchArticle> asSearchArticleList = ZendeskHelpCenterProvider.this.asSearchArticleList(articlesSearchResponse);
                        rs4 rs4Var2 = rs4Var;
                        if (rs4Var2 != null) {
                            rs4Var2.onSuccess(asSearchArticleList);
                        }
                    }
                });
            }
        });
    }

    @Override // zendesk.support.HelpCenterProvider
    public void submitRecordArticleView(final Article article, final Locale locale, final rs4<Void> rs4Var) {
        if (sanityCheck(rs4Var, article)) {
            return;
        }
        this.helpCenterTracker.helpCenterArticleViewed();
        this.blipsProvider.articleView(article);
        this.settingsProvider.getSettings(new ZendeskCallbackSuccess<HelpCenterSettings>(rs4Var) { // from class: zendesk.support.ZendeskHelpCenterProvider.16
            @Override // zendesk.support.ZendeskHelpCenterProvider.ZendeskCallbackSuccess, defpackage.rs4
            public void onSuccess(HelpCenterSettings helpCenterSettings) {
                if (ZendeskHelpCenterProvider.this.sanityCheckHelpCenterSettings(rs4Var, helpCenterSettings)) {
                    return;
                }
                ZendeskHelpCenterProvider.this.helpCenterService.submitRecordArticleView(article.getId(), locale, new RecordArticleViewRequest(ZendeskHelpCenterProvider.this.helpCenterSessionCache.getLastSearch(), ZendeskHelpCenterProvider.this.helpCenterSessionCache.isUniqueSearchResultClick()), new ZendeskCallbackSuccess<Void>(rs4Var) { // from class: zendesk.support.ZendeskHelpCenterProvider.16.1
                    @Override // zendesk.support.ZendeskHelpCenterProvider.ZendeskCallbackSuccess, defpackage.rs4
                    public void onSuccess(Void r2) {
                        ZendeskHelpCenterProvider.this.helpCenterSessionCache.unsetUniqueSearchResultClick();
                        rs4 rs4Var2 = rs4Var;
                        if (rs4Var2 != null) {
                            rs4Var2.onSuccess(r2);
                        }
                    }
                });
            }
        });
    }

    @Override // zendesk.support.HelpCenterProvider
    public void upvoteArticle(final Long l, final rs4<ArticleVote> rs4Var) {
        if (sanityCheck(rs4Var, l)) {
            return;
        }
        this.settingsProvider.getSettings(new ZendeskCallbackSuccess<HelpCenterSettings>(rs4Var) { // from class: zendesk.support.ZendeskHelpCenterProvider.12
            @Override // zendesk.support.ZendeskHelpCenterProvider.ZendeskCallbackSuccess, defpackage.rs4
            public void onSuccess(HelpCenterSettings helpCenterSettings) {
                if (ZendeskHelpCenterProvider.this.checkSettingsAndVotingEnabled(rs4Var, helpCenterSettings)) {
                    ZendeskHelpCenterProvider.this.helpCenterService.upvoteArticle(l, ZendeskHelpCenterProvider.EMPTY_JSON_BODY, new ZendeskCallbackSuccess<ArticleVoteResponse>(rs4Var) { // from class: zendesk.support.ZendeskHelpCenterProvider.12.1
                        @Override // zendesk.support.ZendeskHelpCenterProvider.ZendeskCallbackSuccess, defpackage.rs4
                        public void onSuccess(ArticleVoteResponse articleVoteResponse) {
                            rs4 rs4Var2 = rs4Var;
                            if (rs4Var2 != null) {
                                rs4Var2.onSuccess(articleVoteResponse.getVote());
                            }
                            ZendeskHelpCenterProvider.this.blipsProvider.articleVote(l, 1);
                        }
                    });
                }
            }
        });
    }

    @Override // zendesk.support.HelpCenterProvider
    public void getArticles(final Long l, final String str, final rs4<List<Article>> rs4Var) {
        if (sanityCheck(rs4Var, l)) {
            return;
        }
        this.settingsProvider.getSettings(new ZendeskCallbackSuccess<HelpCenterSettings>(rs4Var) { // from class: zendesk.support.ZendeskHelpCenterProvider.4
            @Override // zendesk.support.ZendeskHelpCenterProvider.ZendeskCallbackSuccess, defpackage.rs4
            public void onSuccess(HelpCenterSettings helpCenterSettings) {
                if (ZendeskHelpCenterProvider.this.sanityCheckHelpCenterSettings(rs4Var, helpCenterSettings)) {
                    return;
                }
                ZendeskHelpCenterProvider.this.helpCenterService.getArticlesForSection(l, ZendeskHelpCenterProvider.this.getLocale(helpCenterSettings), "users", str, rs4Var);
            }
        });
    }
}
