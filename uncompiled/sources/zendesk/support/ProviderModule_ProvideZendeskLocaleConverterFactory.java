package zendesk.support;

import zendesk.core.ZendeskLocaleConverter;

/* loaded from: classes3.dex */
public final class ProviderModule_ProvideZendeskLocaleConverterFactory implements y11<ZendeskLocaleConverter> {
    private final ProviderModule module;

    public ProviderModule_ProvideZendeskLocaleConverterFactory(ProviderModule providerModule) {
        this.module = providerModule;
    }

    public static ProviderModule_ProvideZendeskLocaleConverterFactory create(ProviderModule providerModule) {
        return new ProviderModule_ProvideZendeskLocaleConverterFactory(providerModule);
    }

    public static ZendeskLocaleConverter provideZendeskLocaleConverter(ProviderModule providerModule) {
        return (ZendeskLocaleConverter) cu2.f(providerModule.provideZendeskLocaleConverter());
    }

    @Override // defpackage.ew2
    public ZendeskLocaleConverter get() {
        return provideZendeskLocaleConverter(this.module);
    }
}
