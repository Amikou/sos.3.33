package zendesk.support;

/* loaded from: classes3.dex */
public final class SupportSdkModule_ConfigurationHelperFactory implements y11<z40> {
    private final SupportSdkModule module;

    public SupportSdkModule_ConfigurationHelperFactory(SupportSdkModule supportSdkModule) {
        this.module = supportSdkModule;
    }

    public static z40 configurationHelper(SupportSdkModule supportSdkModule) {
        return (z40) cu2.f(supportSdkModule.configurationHelper());
    }

    public static SupportSdkModule_ConfigurationHelperFactory create(SupportSdkModule supportSdkModule) {
        return new SupportSdkModule_ConfigurationHelperFactory(supportSdkModule);
    }

    @Override // defpackage.ew2
    public z40 get() {
        return configurationHelper(this.module);
    }
}
