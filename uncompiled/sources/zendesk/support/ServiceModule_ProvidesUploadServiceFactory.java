package zendesk.support;

import zendesk.core.RestServiceProvider;

/* loaded from: classes3.dex */
public final class ServiceModule_ProvidesUploadServiceFactory implements y11<UploadService> {
    private final ew2<RestServiceProvider> restServiceProvider;

    public ServiceModule_ProvidesUploadServiceFactory(ew2<RestServiceProvider> ew2Var) {
        this.restServiceProvider = ew2Var;
    }

    public static ServiceModule_ProvidesUploadServiceFactory create(ew2<RestServiceProvider> ew2Var) {
        return new ServiceModule_ProvidesUploadServiceFactory(ew2Var);
    }

    public static UploadService providesUploadService(RestServiceProvider restServiceProvider) {
        return (UploadService) cu2.f(ServiceModule.providesUploadService(restServiceProvider));
    }

    @Override // defpackage.ew2
    public UploadService get() {
        return providesUploadService(this.restServiceProvider.get());
    }
}
