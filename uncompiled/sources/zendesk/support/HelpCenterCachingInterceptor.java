package zendesk.support;

import java.io.IOException;
import okhttp3.Interceptor;
import okhttp3.Response;

/* loaded from: classes3.dex */
public class HelpCenterCachingInterceptor implements Interceptor {
    @Override // okhttp3.Interceptor
    public Response intercept(Interceptor.Chain chain) throws IOException {
        Response proceed = chain.proceed(chain.request());
        return ru3.b(proceed.headers().get("X-ZD-Cache-Control")) ? proceed.newBuilder().header("Cache-Control", proceed.header("X-ZD-Cache-Control")).build() : proceed;
    }
}
