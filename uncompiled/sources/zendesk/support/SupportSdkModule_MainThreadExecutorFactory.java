package zendesk.support;

import java.util.concurrent.Executor;

/* loaded from: classes3.dex */
public final class SupportSdkModule_MainThreadExecutorFactory implements y11<Executor> {
    private final SupportSdkModule module;

    public SupportSdkModule_MainThreadExecutorFactory(SupportSdkModule supportSdkModule) {
        this.module = supportSdkModule;
    }

    public static SupportSdkModule_MainThreadExecutorFactory create(SupportSdkModule supportSdkModule) {
        return new SupportSdkModule_MainThreadExecutorFactory(supportSdkModule);
    }

    public static Executor mainThreadExecutor(SupportSdkModule supportSdkModule) {
        return (Executor) cu2.f(supportSdkModule.mainThreadExecutor());
    }

    @Override // defpackage.ew2
    public Executor get() {
        return mainThreadExecutor(this.module);
    }
}
