package zendesk.support;

/* loaded from: classes3.dex */
public final class ServiceModule_ProvideZendeskUploadServiceFactory implements y11<ZendeskUploadService> {
    private final ew2<UploadService> uploadServiceProvider;

    public ServiceModule_ProvideZendeskUploadServiceFactory(ew2<UploadService> ew2Var) {
        this.uploadServiceProvider = ew2Var;
    }

    public static ServiceModule_ProvideZendeskUploadServiceFactory create(ew2<UploadService> ew2Var) {
        return new ServiceModule_ProvideZendeskUploadServiceFactory(ew2Var);
    }

    public static ZendeskUploadService provideZendeskUploadService(Object obj) {
        return (ZendeskUploadService) cu2.f(ServiceModule.provideZendeskUploadService((UploadService) obj));
    }

    @Override // defpackage.ew2
    public ZendeskUploadService get() {
        return provideZendeskUploadService(this.uploadServiceProvider.get());
    }
}
