package zendesk.support;

import java.util.List;

/* loaded from: classes3.dex */
class ArticleResponse {
    private Article article;
    private List<zendesk.core.User> users;

    public Article getArticle() {
        return this.article;
    }

    public List<zendesk.core.User> getUsers() {
        return l10.c(this.users);
    }
}
