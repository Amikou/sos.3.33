package zendesk.support;

import zendesk.core.AuthenticationProvider;

/* loaded from: classes3.dex */
public final class ProviderModule_ProvideRequestProviderFactory implements y11<RequestProvider> {
    private final ew2<AuthenticationProvider> authenticationProvider;
    private final ew2<SupportBlipsProvider> blipsProvider;
    private final ProviderModule module;
    private final ew2<ZendeskRequestService> requestServiceProvider;
    private final ew2<RequestSessionCache> requestSessionCacheProvider;
    private final ew2<RequestStorage> requestStorageProvider;
    private final ew2<SupportSettingsProvider> settingsProvider;
    private final ew2<SupportSdkMetadata> supportSdkMetadataProvider;
    private final ew2<ZendeskTracker> zendeskTrackerProvider;

    public ProviderModule_ProvideRequestProviderFactory(ProviderModule providerModule, ew2<SupportSettingsProvider> ew2Var, ew2<AuthenticationProvider> ew2Var2, ew2<ZendeskRequestService> ew2Var3, ew2<RequestStorage> ew2Var4, ew2<RequestSessionCache> ew2Var5, ew2<ZendeskTracker> ew2Var6, ew2<SupportSdkMetadata> ew2Var7, ew2<SupportBlipsProvider> ew2Var8) {
        this.module = providerModule;
        this.settingsProvider = ew2Var;
        this.authenticationProvider = ew2Var2;
        this.requestServiceProvider = ew2Var3;
        this.requestStorageProvider = ew2Var4;
        this.requestSessionCacheProvider = ew2Var5;
        this.zendeskTrackerProvider = ew2Var6;
        this.supportSdkMetadataProvider = ew2Var7;
        this.blipsProvider = ew2Var8;
    }

    public static ProviderModule_ProvideRequestProviderFactory create(ProviderModule providerModule, ew2<SupportSettingsProvider> ew2Var, ew2<AuthenticationProvider> ew2Var2, ew2<ZendeskRequestService> ew2Var3, ew2<RequestStorage> ew2Var4, ew2<RequestSessionCache> ew2Var5, ew2<ZendeskTracker> ew2Var6, ew2<SupportSdkMetadata> ew2Var7, ew2<SupportBlipsProvider> ew2Var8) {
        return new ProviderModule_ProvideRequestProviderFactory(providerModule, ew2Var, ew2Var2, ew2Var3, ew2Var4, ew2Var5, ew2Var6, ew2Var7, ew2Var8);
    }

    public static RequestProvider provideRequestProvider(ProviderModule providerModule, SupportSettingsProvider supportSettingsProvider, AuthenticationProvider authenticationProvider, Object obj, Object obj2, Object obj3, Object obj4, Object obj5, SupportBlipsProvider supportBlipsProvider) {
        return (RequestProvider) cu2.f(providerModule.provideRequestProvider(supportSettingsProvider, authenticationProvider, (ZendeskRequestService) obj, (RequestStorage) obj2, (RequestSessionCache) obj3, (ZendeskTracker) obj4, (SupportSdkMetadata) obj5, supportBlipsProvider));
    }

    @Override // defpackage.ew2
    public RequestProvider get() {
        return provideRequestProvider(this.module, this.settingsProvider.get(), this.authenticationProvider.get(), this.requestServiceProvider.get(), this.requestStorageProvider.get(), this.requestSessionCacheProvider.get(), this.zendeskTrackerProvider.get(), this.supportSdkMetadataProvider.get(), this.blipsProvider.get());
    }
}
