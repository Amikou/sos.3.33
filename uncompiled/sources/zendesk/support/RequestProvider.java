package zendesk.support;

import java.util.Date;
import java.util.List;

/* loaded from: classes3.dex */
public interface RequestProvider {
    void addComment(String str, EndUserComment endUserComment, rs4<Comment> rs4Var);

    void createRequest(CreateRequest createRequest, rs4<Request> rs4Var);

    void getAllRequests(rs4<List<Request>> rs4Var);

    void getComments(String str, rs4<CommentsResponse> rs4Var);

    void getCommentsSince(String str, Date date, boolean z, rs4<CommentsResponse> rs4Var);

    void getRequest(String str, rs4<Request> rs4Var);

    void getRequests(String str, rs4<List<Request>> rs4Var);

    void getTicketFormsById(List<Long> list, rs4<List<TicketForm>> rs4Var);

    void getUpdatesForDevice(rs4<RequestUpdates> rs4Var);

    void markRequestAsRead(String str, int i);

    void markRequestAsUnread(String str);
}
