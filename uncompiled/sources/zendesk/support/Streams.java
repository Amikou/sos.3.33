package zendesk.support;

import com.google.gson.Gson;
import com.zendesk.logger.Logger;
import java.io.Closeable;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.Reader;
import java.io.Writer;
import java.lang.reflect.Type;
import okio.c;
import okio.d;
import okio.k;
import okio.m;
import okio.n;

/* loaded from: classes3.dex */
public class Streams {

    /* loaded from: classes3.dex */
    public interface Use<R, P extends Closeable> {
        R use(P p) throws Exception;
    }

    public static void closeQuietly(Closeable closeable) {
        if (closeable != null) {
            try {
                closeable.close();
            } catch (RuntimeException e) {
                throw e;
            } catch (Exception unused) {
            }
        }
    }

    public static <T> T fromJson(final Gson gson, n nVar, final Type type) {
        return (T) use(toReader(nVar), new Use<T, Reader>() { // from class: zendesk.support.Streams.1
            /* JADX WARN: Type inference failed for: r3v1, types: [T, java.lang.Object] */
            @Override // zendesk.support.Streams.Use
            public T use(Reader reader) throws Exception {
                return Gson.this.fromJson(reader, type);
            }
        });
    }

    public static void toJson(final Gson gson, m mVar, final Object obj) {
        use(toWriter(mVar), new Use<Void, Writer>() { // from class: zendesk.support.Streams.2
            @Override // zendesk.support.Streams.Use
            public Void use(Writer writer) throws Exception {
                Gson.this.toJson(obj, writer);
                return null;
            }
        });
    }

    public static Reader toReader(n nVar) {
        if (nVar instanceof d) {
            return new InputStreamReader(((d) nVar).G1());
        }
        return new InputStreamReader(k.d(nVar).G1());
    }

    public static Writer toWriter(m mVar) {
        if (mVar instanceof c) {
            return new OutputStreamWriter(((c) mVar).D1());
        }
        return new OutputStreamWriter(k.c(mVar).D1());
    }

    public static <R, P extends Closeable> R use(P p, Use<R, P> use) {
        if (p == null) {
            return null;
        }
        try {
            return use.use(p);
        } catch (Exception e) {
            Logger.f("Streams", "Error using stream", e, new Object[0]);
            return null;
        } finally {
            closeQuietly(p);
        }
    }
}
