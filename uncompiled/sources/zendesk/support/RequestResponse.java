package zendesk.support;

import java.util.List;

/* loaded from: classes3.dex */
class RequestResponse {
    private List<User> lastCommentingAgents;
    private Request request;

    public List<User> getLastCommentingAgents() {
        return l10.c(this.lastCommentingAgents);
    }

    public Request getRequest() {
        return this.request;
    }
}
