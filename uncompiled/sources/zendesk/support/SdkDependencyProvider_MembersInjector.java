package zendesk.support;

import java.util.List;
import zendesk.core.ActionHandler;
import zendesk.core.ActionHandlerRegistry;

/* loaded from: classes3.dex */
public final class SdkDependencyProvider_MembersInjector implements i72<SdkDependencyProvider> {
    private final ew2<List<ActionHandler>> actionHandlersProvider;
    private final ew2<ActionHandlerRegistry> registryProvider;

    public SdkDependencyProvider_MembersInjector(ew2<ActionHandlerRegistry> ew2Var, ew2<List<ActionHandler>> ew2Var2) {
        this.registryProvider = ew2Var;
        this.actionHandlersProvider = ew2Var2;
    }

    public static i72<SdkDependencyProvider> create(ew2<ActionHandlerRegistry> ew2Var, ew2<List<ActionHandler>> ew2Var2) {
        return new SdkDependencyProvider_MembersInjector(ew2Var, ew2Var2);
    }

    public static void injectActionHandlers(SdkDependencyProvider sdkDependencyProvider, List<ActionHandler> list) {
        sdkDependencyProvider.actionHandlers = list;
    }

    public static void injectRegistry(SdkDependencyProvider sdkDependencyProvider, ActionHandlerRegistry actionHandlerRegistry) {
        sdkDependencyProvider.registry = actionHandlerRegistry;
    }

    public void injectMembers(SdkDependencyProvider sdkDependencyProvider) {
        injectRegistry(sdkDependencyProvider, this.registryProvider.get());
        injectActionHandlers(sdkDependencyProvider, this.actionHandlersProvider.get());
    }
}
