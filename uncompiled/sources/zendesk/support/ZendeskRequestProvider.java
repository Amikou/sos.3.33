package zendesk.support;

import com.zendesk.logger.Logger;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import zendesk.core.AnonymousIdentity;
import zendesk.core.AuthenticationProvider;
import zendesk.core.AuthenticationType;
import zendesk.core.Identity;

/* JADX INFO: Access modifiers changed from: package-private */
/* loaded from: classes3.dex */
public final class ZendeskRequestProvider implements RequestProvider {
    private static final String ALL_REQUEST_STATUSES = "new,open,pending,hold,solved,closed";
    private static final String GET_REQUESTS_SIDE_LOAD = "public_updated_at,last_commenting_agents,last_comment,first_comment";
    private static final String LOG_TAG = "ZendeskRequestProvider";
    private static final int MAX_TICKET_FIELDS = 5;
    private final AuthenticationProvider authenticationProvider;
    private final SupportBlipsProvider blipsProvider;
    private final SupportSdkMetadata metadata;
    private final ZendeskRequestService requestService;
    private final RequestSessionCache requestSessionCache;
    private final RequestStorage requestStorage;
    private final SupportSettingsProvider settingsProvider;
    private final ZendeskTracker zendeskTracker;

    public ZendeskRequestProvider(SupportSettingsProvider supportSettingsProvider, ZendeskRequestService zendeskRequestService, AuthenticationProvider authenticationProvider, RequestStorage requestStorage, RequestSessionCache requestSessionCache, ZendeskTracker zendeskTracker, SupportSdkMetadata supportSdkMetadata, SupportBlipsProvider supportBlipsProvider) {
        this.settingsProvider = supportSettingsProvider;
        this.requestService = zendeskRequestService;
        this.authenticationProvider = authenticationProvider;
        this.requestStorage = requestStorage;
        this.requestSessionCache = requestSessionCache;
        this.zendeskTracker = zendeskTracker;
        this.metadata = supportSdkMetadata;
        this.blipsProvider = supportBlipsProvider;
    }

    /* JADX INFO: Access modifiers changed from: private */
    public void addCommentInternal(final String str, EndUserComment endUserComment, final rs4<Comment> rs4Var) {
        this.requestService.addComment(str, endUserComment, new ZendeskCallbackSuccess<Request>(rs4Var) { // from class: zendesk.support.ZendeskRequestProvider.7
            @Override // zendesk.support.ZendeskCallbackSuccess, defpackage.rs4
            public void onSuccess(Request request) {
                ZendeskRequestProvider.this.zendeskTracker.requestUpdated();
                ZendeskRequestProvider.this.blipsProvider.requestUpdated(str);
                if (request.getId() != null && request.getCommentCount() != null) {
                    ZendeskRequestProvider.this.requestStorage.updateRequestData(Collections.singletonList(request));
                } else {
                    Logger.k(ZendeskRequestProvider.LOG_TAG, "The ID and / or comment count was missing. Cannot store comment totalUpdates.", new Object[0]);
                }
                rs4 rs4Var2 = rs4Var;
                if (rs4Var2 != null) {
                    rs4Var2.onSuccess(request.getLastComment());
                }
            }
        });
    }

    /* JADX INFO: Access modifiers changed from: private */
    public void addServerTags(CreateRequest createRequest, SupportSdkSettings supportSdkSettings) {
        List<String> b = l10.b(createRequest.getTags(), supportSdkSettings.getContactZendeskTags());
        if (l10.i(b)) {
            Logger.b(LOG_TAG, "Adding tags to feedback...", new Object[0]);
            createRequest.setTags(b);
        }
    }

    /* JADX INFO: Access modifiers changed from: private */
    public static void answerCallbackOnConversationsDisabled(rs4 rs4Var) {
        Logger.b(LOG_TAG, "Conversations disabled, this feature is not available on your plan or was disabled.", new Object[0]);
        if (rs4Var != null) {
            rs4Var.onError(new dw0("Access Denied"));
        }
    }

    /* JADX INFO: Access modifiers changed from: private */
    public static boolean areConversationsEnabled(SupportSdkSettings supportSdkSettings) {
        if (supportSdkSettings == null) {
            return false;
        }
        return supportSdkSettings.isConversationsEnabled();
    }

    /* JADX INFO: Access modifiers changed from: private */
    public static RequestUpdates calcRequestUpdates(List<RequestData> list) {
        HashMap hashMap = new HashMap(list.size());
        for (RequestData requestData : list) {
            int unreadComments = requestData.unreadComments();
            if (unreadComments != 0) {
                hashMap.put(requestData.getId(), Integer.valueOf(unreadComments));
            }
        }
        return new RequestUpdates(hashMap);
    }

    /* JADX INFO: Access modifiers changed from: private */
    public static List<TicketForm> convertTicketFormResponse(List<RawTicketForm> list, List<RawTicketField> list2) {
        ArrayList arrayList = new ArrayList();
        Map<Long, TicketField> createTicketFieldMap = createTicketFieldMap(list2);
        for (RawTicketForm rawTicketForm : list) {
            arrayList.add(createTicketFormFromResponse(rawTicketForm, createTicketFieldMap));
        }
        return arrayList;
    }

    private static Map<Long, TicketField> createTicketFieldMap(List<RawTicketField> list) {
        HashMap hashMap = new HashMap(list.size());
        for (RawTicketField rawTicketField : list) {
            hashMap.put(Long.valueOf(rawTicketField.getId()), TicketField.create(rawTicketField));
        }
        return hashMap;
    }

    private static TicketForm createTicketFormFromResponse(RawTicketForm rawTicketForm, Map<Long, TicketField> map) {
        ArrayList arrayList = new ArrayList();
        for (Long l : rawTicketForm.getTicketFieldIds()) {
            if (l != null && map.get(l) != null) {
                arrayList.add(map.get(l));
            }
        }
        return RawTicketForm.create(rawTicketForm, arrayList);
    }

    /* JADX INFO: Access modifiers changed from: private */
    public void getAllRequestsInternal(String str, AuthenticationType authenticationType, final rs4<List<Request>> rs4Var) {
        if (ru3.d(str)) {
            str = ALL_REQUEST_STATUSES;
        }
        ZendeskCallbackSuccess<List<Request>> zendeskCallbackSuccess = new ZendeskCallbackSuccess<List<Request>>(rs4Var) { // from class: zendesk.support.ZendeskRequestProvider.3
            @Override // zendesk.support.ZendeskCallbackSuccess, defpackage.rs4
            public void onSuccess(List<Request> list) {
                ZendeskRequestProvider.this.requestStorage.updateRequestData(list);
                rs4 rs4Var2 = rs4Var;
                if (rs4Var2 != null) {
                    rs4Var2.onSuccess(list);
                }
            }
        };
        if (authenticationType == AuthenticationType.ANONYMOUS) {
            List<RequestData> requestData = this.requestStorage.getRequestData();
            ArrayList arrayList = new ArrayList(requestData.size());
            for (RequestData requestData2 : requestData) {
                arrayList.add(requestData2.getId());
            }
            if (l10.g(arrayList)) {
                Logger.k(LOG_TAG, "getAllRequestsInternal: There are no requests to fetch", new Object[0]);
                if (rs4Var != null) {
                    rs4Var.onSuccess(new ArrayList());
                    return;
                }
                return;
            }
            this.requestService.getAllRequests(ru3.f(arrayList), str, GET_REQUESTS_SIDE_LOAD, zendeskCallbackSuccess);
            return;
        }
        this.requestService.getAllRequests(str, GET_REQUESTS_SIDE_LOAD, zendeskCallbackSuccess);
    }

    /* JADX INFO: Access modifiers changed from: private */
    public void internalCreateRequest(CreateRequest createRequest, AuthenticationType authenticationType, Identity identity, final rs4<Request> rs4Var) {
        ZendeskCallbackSuccess<Request> zendeskCallbackSuccess = new ZendeskCallbackSuccess<Request>(rs4Var) { // from class: zendesk.support.ZendeskRequestProvider.2
            @Override // zendesk.support.ZendeskCallbackSuccess, defpackage.rs4
            public void onSuccess(Request request) {
                if (request != null && request.getId() != null) {
                    ZendeskRequestProvider.this.zendeskTracker.requestCreated();
                    ZendeskRequestProvider.this.blipsProvider.requestCreated(request.getId());
                    ZendeskRequestProvider.this.requestStorage.updateRequestData(Collections.singletonList(request));
                    rs4 rs4Var2 = rs4Var;
                    if (rs4Var2 != null) {
                        rs4Var2.onSuccess(request);
                        return;
                    }
                    return;
                }
                onError(new dw0("The request was created, but the ID is unknown."));
            }
        };
        if (authenticationType == AuthenticationType.ANONYMOUS && identity != null && (identity instanceof AnonymousIdentity)) {
            this.requestService.createRequest(((AnonymousIdentity) identity).getSdkGuid(), createRequest, zendeskCallbackSuccess);
            return;
        }
        this.requestService.createRequest(null, createRequest, zendeskCallbackSuccess);
    }

    @Override // zendesk.support.RequestProvider
    public void addComment(final String str, final EndUserComment endUserComment, final rs4<Comment> rs4Var) {
        this.settingsProvider.getSettings(new ZendeskCallbackSuccess<SupportSdkSettings>(rs4Var) { // from class: zendesk.support.ZendeskRequestProvider.8
            @Override // zendesk.support.ZendeskCallbackSuccess, defpackage.rs4
            public void onSuccess(SupportSdkSettings supportSdkSettings) {
                if (ZendeskRequestProvider.areConversationsEnabled(supportSdkSettings)) {
                    ZendeskRequestProvider.this.addCommentInternal(str, endUserComment, rs4Var);
                } else {
                    ZendeskRequestProvider.answerCallbackOnConversationsDisabled(rs4Var);
                }
            }
        });
    }

    @Override // zendesk.support.RequestProvider
    public void createRequest(final CreateRequest createRequest, final rs4<Request> rs4Var) {
        if (!(createRequest != null)) {
            Logger.e(LOG_TAG, "configuration is invalid: request null", new Object[0]);
            if (rs4Var != null) {
                rs4Var.onError(new dw0("configuration is invalid: request null"));
                return;
            }
            return;
        }
        this.settingsProvider.getSettings(new ZendeskCallbackSuccess<SupportSdkSettings>(rs4Var) { // from class: zendesk.support.ZendeskRequestProvider.1
            @Override // zendesk.support.ZendeskCallbackSuccess, defpackage.rs4
            public void onSuccess(SupportSdkSettings supportSdkSettings) {
                createRequest.setMetadata(ZendeskRequestProvider.this.metadata.getDeviceInfoAsMapForMetaData());
                ZendeskRequestProvider.this.addServerTags(createRequest, supportSdkSettings);
                ZendeskRequestProvider.this.internalCreateRequest(createRequest, supportSdkSettings.getAuthenticationType(), ZendeskRequestProvider.this.authenticationProvider.getIdentity(), rs4Var);
            }
        });
    }

    @Override // zendesk.support.RequestProvider
    public void getAllRequests(rs4<List<Request>> rs4Var) {
        getRequests(null, rs4Var);
    }

    @Override // zendesk.support.RequestProvider
    public void getComments(final String str, final rs4<CommentsResponse> rs4Var) {
        this.settingsProvider.getSettings(new ZendeskCallbackSuccess<SupportSdkSettings>(rs4Var) { // from class: zendesk.support.ZendeskRequestProvider.5
            @Override // zendesk.support.ZendeskCallbackSuccess, defpackage.rs4
            public void onSuccess(SupportSdkSettings supportSdkSettings) {
                if (ZendeskRequestProvider.areConversationsEnabled(supportSdkSettings)) {
                    ZendeskRequestProvider.this.requestService.getComments(str, rs4Var);
                } else {
                    ZendeskRequestProvider.answerCallbackOnConversationsDisabled(rs4Var);
                }
            }
        });
    }

    @Override // zendesk.support.RequestProvider
    public void getCommentsSince(final String str, final Date date, final boolean z, final rs4<CommentsResponse> rs4Var) {
        this.settingsProvider.getSettings(new ZendeskCallbackSuccess<SupportSdkSettings>(rs4Var) { // from class: zendesk.support.ZendeskRequestProvider.6
            @Override // zendesk.support.ZendeskCallbackSuccess, defpackage.rs4
            public void onSuccess(SupportSdkSettings supportSdkSettings) {
                if (ZendeskRequestProvider.areConversationsEnabled(supportSdkSettings)) {
                    ZendeskRequestProvider.this.requestService.getCommentsSince(str, date, z, rs4Var);
                } else {
                    ZendeskRequestProvider.answerCallbackOnConversationsDisabled(rs4Var);
                }
            }
        });
    }

    @Override // zendesk.support.RequestProvider
    public void getRequest(final String str, final rs4<Request> rs4Var) {
        this.settingsProvider.getSettings(new ZendeskCallbackSuccess<SupportSdkSettings>(rs4Var) { // from class: zendesk.support.ZendeskRequestProvider.9
            @Override // zendesk.support.ZendeskCallbackSuccess, defpackage.rs4
            public void onSuccess(SupportSdkSettings supportSdkSettings) {
                if (ZendeskRequestProvider.areConversationsEnabled(supportSdkSettings)) {
                    ZendeskRequestProvider.this.requestService.getRequest(str, ZendeskRequestProvider.GET_REQUESTS_SIDE_LOAD, rs4Var);
                } else {
                    ZendeskRequestProvider.answerCallbackOnConversationsDisabled(rs4Var);
                }
            }
        });
    }

    @Override // zendesk.support.RequestProvider
    public void getRequests(final String str, final rs4<List<Request>> rs4Var) {
        this.settingsProvider.getSettings(new ZendeskCallbackSuccess<SupportSdkSettings>(rs4Var) { // from class: zendesk.support.ZendeskRequestProvider.4
            @Override // zendesk.support.ZendeskCallbackSuccess, defpackage.rs4
            public void onSuccess(SupportSdkSettings supportSdkSettings) {
                if (ZendeskRequestProvider.areConversationsEnabled(supportSdkSettings)) {
                    ZendeskRequestProvider.this.getAllRequestsInternal(str, supportSdkSettings.getAuthenticationType(), rs4Var);
                } else {
                    ZendeskRequestProvider.answerCallbackOnConversationsDisabled(rs4Var);
                }
            }
        });
    }

    @Override // zendesk.support.RequestProvider
    public void getTicketFormsById(List<Long> list, final rs4<List<TicketForm>> rs4Var) {
        if (l10.g(list)) {
            if (rs4Var != null) {
                rs4Var.onError(new dw0("Ticket forms must at least contain 1 Id"));
                return;
            }
            return;
        }
        final ArrayList arrayList = new ArrayList();
        if (list.size() > 5) {
            arrayList.addAll(list.subList(0, 5));
            Logger.b(LOG_TAG, "Maximum number of allowed ticket fields: %d.", 5);
        } else {
            arrayList.addAll(list);
        }
        if (!this.requestSessionCache.containsAllTicketForms(arrayList)) {
            this.settingsProvider.getSettings(new ZendeskCallbackSuccess<SupportSdkSettings>(rs4Var) { // from class: zendesk.support.ZendeskRequestProvider.10
                @Override // zendesk.support.ZendeskCallbackSuccess, defpackage.rs4
                public void onSuccess(SupportSdkSettings supportSdkSettings) {
                    if (supportSdkSettings.isTicketFormSupportAvailable()) {
                        ZendeskRequestProvider.this.requestService.getTicketFormsById(ru3.h(arrayList), new ZendeskCallbackSuccess<RawTicketFormResponse>(rs4Var) { // from class: zendesk.support.ZendeskRequestProvider.10.1
                            @Override // zendesk.support.ZendeskCallbackSuccess, defpackage.rs4
                            public void onSuccess(RawTicketFormResponse rawTicketFormResponse) {
                                List<TicketForm> convertTicketFormResponse = ZendeskRequestProvider.convertTicketFormResponse(rawTicketFormResponse.getTicketForms(), rawTicketFormResponse.getTicketFields());
                                ZendeskRequestProvider.this.requestSessionCache.updateTicketFormCache(convertTicketFormResponse);
                                rs4 rs4Var2 = rs4Var;
                                if (rs4Var2 != null) {
                                    rs4Var2.onSuccess(convertTicketFormResponse);
                                }
                            }
                        });
                        return;
                    }
                    rs4 rs4Var2 = rs4Var;
                    if (rs4Var2 != null) {
                        rs4Var2.onError(new dw0("Ticket form support disabled."));
                    }
                }
            });
        } else if (rs4Var != null) {
            rs4Var.onSuccess(this.requestSessionCache.getTicketFormsById(arrayList));
        }
    }

    @Override // zendesk.support.RequestProvider
    public void getUpdatesForDevice(final rs4<RequestUpdates> rs4Var) {
        if (!this.requestStorage.isRequestDataExpired()) {
            rs4Var.onSuccess(calcRequestUpdates(this.requestStorage.getRequestData()));
        } else {
            this.settingsProvider.getSettings(new rs4<SupportSdkSettings>() { // from class: zendesk.support.ZendeskRequestProvider.11
                @Override // defpackage.rs4
                public void onError(cw0 cw0Var) {
                    rs4Var.onError(cw0Var);
                }

                @Override // defpackage.rs4
                public void onSuccess(SupportSdkSettings supportSdkSettings) {
                    if (!supportSdkSettings.isConversationsEnabled()) {
                        ZendeskRequestProvider.answerCallbackOnConversationsDisabled(rs4Var);
                    } else {
                        ZendeskRequestProvider.this.getAllRequestsInternal(null, supportSdkSettings.getAuthenticationType(), new ZendeskCallbackSuccess<List<Request>>(rs4Var) { // from class: zendesk.support.ZendeskRequestProvider.11.1
                            @Override // zendesk.support.ZendeskCallbackSuccess, defpackage.rs4
                            public void onSuccess(List<Request> list) {
                                rs4Var.onSuccess(ZendeskRequestProvider.calcRequestUpdates(ZendeskRequestProvider.this.requestStorage.getRequestData()));
                            }
                        });
                    }
                }
            });
        }
    }

    @Override // zendesk.support.RequestProvider
    public void markRequestAsRead(String str, int i) {
        this.requestStorage.markRequestAsRead(str, i);
    }

    @Override // zendesk.support.RequestProvider
    public void markRequestAsUnread(String str) {
        this.requestStorage.markRequestAsUnread(str);
    }
}
