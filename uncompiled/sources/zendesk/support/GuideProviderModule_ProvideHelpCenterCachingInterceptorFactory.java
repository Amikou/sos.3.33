package zendesk.support;

/* loaded from: classes3.dex */
public final class GuideProviderModule_ProvideHelpCenterCachingInterceptorFactory implements y11<HelpCenterCachingInterceptor> {

    /* loaded from: classes3.dex */
    public static final class InstanceHolder {
        private static final GuideProviderModule_ProvideHelpCenterCachingInterceptorFactory INSTANCE = new GuideProviderModule_ProvideHelpCenterCachingInterceptorFactory();

        private InstanceHolder() {
        }
    }

    public static GuideProviderModule_ProvideHelpCenterCachingInterceptorFactory create() {
        return InstanceHolder.INSTANCE;
    }

    public static HelpCenterCachingInterceptor provideHelpCenterCachingInterceptor() {
        return (HelpCenterCachingInterceptor) cu2.f(GuideProviderModule.provideHelpCenterCachingInterceptor());
    }

    @Override // defpackage.ew2
    public HelpCenterCachingInterceptor get() {
        return provideHelpCenterCachingInterceptor();
    }
}
