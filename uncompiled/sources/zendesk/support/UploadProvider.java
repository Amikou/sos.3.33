package zendesk.support;

import java.io.File;

/* loaded from: classes3.dex */
public interface UploadProvider {
    void deleteAttachment(String str, rs4<Void> rs4Var);

    void uploadAttachment(String str, File file, String str2, rs4<UploadResponse> rs4Var);
}
