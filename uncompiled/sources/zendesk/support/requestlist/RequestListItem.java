package zendesk.support.requestlist;

import java.util.Date;
import java.util.List;
import zendesk.support.request.RequestConfiguration;
import zendesk.support.requestlist.RequestInfo;

/* JADX INFO: Access modifiers changed from: package-private */
/* loaded from: classes3.dex */
public class RequestListItem {
    private final RequestInfo requestInfo;

    public RequestListItem(RequestInfo requestInfo) {
        this.requestInfo = requestInfo;
    }

    public RequestConfiguration.Builder configure(RequestConfiguration.Builder builder) {
        return builder.withRequestInfo(this.requestInfo);
    }

    public String getAvatar() {
        return hasAgentReplied() ? this.requestInfo.getAgentInfos().get(0).getAvatar() : "";
    }

    public String getFirstMessage() {
        return this.requestInfo.getFirstMessageInfo().getBody();
    }

    public long getItemId() {
        int hashCode;
        String localId = this.requestInfo.getLocalId();
        String remoteId = this.requestInfo.getRemoteId();
        if (ru3.b(localId)) {
            hashCode = localId.hashCode();
        } else {
            hashCode = remoteId.hashCode();
        }
        return hashCode;
    }

    public List<String> getLastCommentingAgentNames() {
        return l10.k(this.requestInfo.getAgentInfos(), new os4<RequestInfo.AgentInfo, String>() { // from class: zendesk.support.requestlist.RequestListItem.1
            @Override // defpackage.os4
            public String apply(RequestInfo.AgentInfo agentInfo) {
                return agentInfo.getName();
            }
        });
    }

    public String getLastMessage() {
        return this.requestInfo.getLastMessageInfo().getBody();
    }

    public Date getLastUpdated() {
        return this.requestInfo.getLastUpdated();
    }

    public boolean hasAgentReplied() {
        return l10.i(this.requestInfo.getAgentInfos());
    }

    public boolean isClosed() {
        return this.requestInfo.isClosed();
    }

    public boolean isFailed() {
        return l10.i(this.requestInfo.getFailedMessageIds());
    }

    public boolean isUnread() {
        return this.requestInfo.isUnread();
    }
}
