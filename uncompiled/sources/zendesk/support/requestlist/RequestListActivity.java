package zendesk.support.requestlist;

import android.content.Context;
import android.os.Bundle;
import androidx.appcompat.app.AppCompatActivity;
import com.zendesk.logger.Logger;
import zendesk.core.ActionHandler;
import zendesk.core.ActionHandlerRegistry;
import zendesk.support.SdkDependencyProvider;
import zendesk.support.requestlist.RequestListConfiguration;

/* loaded from: classes3.dex */
public class RequestListActivity extends AppCompatActivity {
    public static final String LOG_TAG = "RequestListActivity";
    public ActionHandlerRegistry actionHandlerRegistry;
    public RequestListModel model;
    public RequestListPresenter presenter;
    public RequestListSyncHandler syncHandler;
    public RequestListView view;

    public static RequestListConfiguration.Builder builder() {
        return new RequestListConfiguration.Builder();
    }

    public static void refresh(Context context, ActionHandlerRegistry actionHandlerRegistry) {
        ActionHandler handlerByAction = actionHandlerRegistry.handlerByAction("request_list_refresh");
        if (handlerByAction != null) {
            handlerByAction.handle(null, context);
        }
    }

    @Override // androidx.fragment.app.FragmentActivity, androidx.activity.ComponentActivity, androidx.core.app.ComponentActivity, android.app.Activity
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        getTheme().applyStyle(a23.ZendeskActivityDefaultTheme, true);
        SdkDependencyProvider sdkDependencyProvider = SdkDependencyProvider.INSTANCE;
        if (!sdkDependencyProvider.isInitialized()) {
            Logger.e(LOG_TAG, SdkDependencyProvider.NOT_INITIALIZED_LOG, new Object[0]);
            finish();
            return;
        }
        RequestListConfiguration requestListConfiguration = (RequestListConfiguration) a50.e(getIntent().getExtras(), RequestListConfiguration.class);
        if (requestListConfiguration == null) {
            Logger.e(LOG_TAG, "No configuration found. Please use RequestListActivity.builder()", new Object[0]);
            finish();
            return;
        }
        sdkDependencyProvider.provideRequestListComponent(this, requestListConfiguration).inject(this);
        setContentView(this.view);
        this.presenter.onCreate(bundle == null, this.view);
        this.actionHandlerRegistry.add(this.syncHandler);
    }

    @Override // androidx.appcompat.app.AppCompatActivity, androidx.fragment.app.FragmentActivity, android.app.Activity
    public void onDestroy() {
        super.onDestroy();
        ActionHandlerRegistry actionHandlerRegistry = this.actionHandlerRegistry;
        if (actionHandlerRegistry != null) {
            actionHandlerRegistry.remove(this.syncHandler);
        }
        RequestListPresenter requestListPresenter = this.presenter;
        if (requestListPresenter != null) {
            requestListPresenter.onDestroy(isChangingConfigurations());
        }
    }

    @Override // androidx.fragment.app.FragmentActivity, android.app.Activity
    public void onPause() {
        super.onPause();
        this.syncHandler.setRunning(false);
    }

    @Override // androidx.fragment.app.FragmentActivity, android.app.Activity
    public void onResume() {
        super.onResume();
        this.syncHandler.setRunning(true);
    }

    @Override // androidx.appcompat.app.AppCompatActivity, androidx.fragment.app.FragmentActivity, android.app.Activity
    public void onStart() {
        super.onStart();
        this.view.onStart();
    }

    @Override // androidx.appcompat.app.AppCompatActivity, androidx.fragment.app.FragmentActivity, android.app.Activity
    public void onStop() {
        super.onStop();
        this.view.onStop();
    }
}
