package zendesk.support.requestlist;

import zendesk.core.ActionHandlerRegistry;

/* loaded from: classes3.dex */
public final class RequestListActivity_MembersInjector implements i72<RequestListActivity> {
    private final ew2<ActionHandlerRegistry> actionHandlerRegistryProvider;
    private final ew2<RequestListModel> modelProvider;
    private final ew2<RequestListPresenter> presenterProvider;
    private final ew2<RequestListSyncHandler> syncHandlerProvider;
    private final ew2<RequestListView> viewProvider;

    public RequestListActivity_MembersInjector(ew2<RequestListPresenter> ew2Var, ew2<RequestListView> ew2Var2, ew2<RequestListModel> ew2Var3, ew2<ActionHandlerRegistry> ew2Var4, ew2<RequestListSyncHandler> ew2Var5) {
        this.presenterProvider = ew2Var;
        this.viewProvider = ew2Var2;
        this.modelProvider = ew2Var3;
        this.actionHandlerRegistryProvider = ew2Var4;
        this.syncHandlerProvider = ew2Var5;
    }

    public static i72<RequestListActivity> create(ew2<RequestListPresenter> ew2Var, ew2<RequestListView> ew2Var2, ew2<RequestListModel> ew2Var3, ew2<ActionHandlerRegistry> ew2Var4, ew2<RequestListSyncHandler> ew2Var5) {
        return new RequestListActivity_MembersInjector(ew2Var, ew2Var2, ew2Var3, ew2Var4, ew2Var5);
    }

    public static void injectActionHandlerRegistry(RequestListActivity requestListActivity, ActionHandlerRegistry actionHandlerRegistry) {
        requestListActivity.actionHandlerRegistry = actionHandlerRegistry;
    }

    public static void injectModel(RequestListActivity requestListActivity, Object obj) {
        requestListActivity.model = (RequestListModel) obj;
    }

    public static void injectPresenter(RequestListActivity requestListActivity, Object obj) {
        requestListActivity.presenter = (RequestListPresenter) obj;
    }

    public static void injectSyncHandler(RequestListActivity requestListActivity, Object obj) {
        requestListActivity.syncHandler = (RequestListSyncHandler) obj;
    }

    public static void injectView(RequestListActivity requestListActivity, Object obj) {
        requestListActivity.view = (RequestListView) obj;
    }

    public void injectMembers(RequestListActivity requestListActivity) {
        injectPresenter(requestListActivity, this.presenterProvider.get());
        injectView(requestListActivity, this.viewProvider.get());
        injectModel(requestListActivity, this.modelProvider.get());
        injectActionHandlerRegistry(requestListActivity, this.actionHandlerRegistryProvider.get());
        injectSyncHandler(requestListActivity, this.syncHandlerProvider.get());
    }
}
