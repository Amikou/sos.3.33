package zendesk.support.requestlist;

import java.util.HashSet;
import java.util.Set;

/* loaded from: classes3.dex */
class CancelableCompositeCallback {
    private Set<zb3> zendeskCallbacks = new HashSet();

    public void add(zb3... zb3VarArr) {
        for (zb3 zb3Var : zb3VarArr) {
            add(zb3Var);
        }
    }

    public void cancel() {
        for (zb3 zb3Var : this.zendeskCallbacks) {
            zb3Var.cancel();
        }
        this.zendeskCallbacks.clear();
    }

    public void add(zb3 zb3Var) {
        this.zendeskCallbacks.add(zb3Var);
    }
}
