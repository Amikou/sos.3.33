package zendesk.support.requestlist;

import java.util.concurrent.Executor;
import java.util.concurrent.ExecutorService;
import zendesk.support.RequestProvider;
import zendesk.support.SupportUiStorage;
import zendesk.support.requestlist.RequestInfoDataSource;

/* loaded from: classes3.dex */
public final class RequestListModule_RepositoryFactory implements y11<RequestInfoDataSource.Repository> {
    private final ew2<ExecutorService> backgroundThreadExecutorProvider;
    private final ew2<RequestInfoDataSource.LocalDataSource> localDataSourceProvider;
    private final ew2<Executor> mainThreadExecutorProvider;
    private final ew2<RequestProvider> requestProvider;
    private final ew2<SupportUiStorage> supportUiStorageProvider;

    public RequestListModule_RepositoryFactory(ew2<RequestInfoDataSource.LocalDataSource> ew2Var, ew2<SupportUiStorage> ew2Var2, ew2<RequestProvider> ew2Var3, ew2<Executor> ew2Var4, ew2<ExecutorService> ew2Var5) {
        this.localDataSourceProvider = ew2Var;
        this.supportUiStorageProvider = ew2Var2;
        this.requestProvider = ew2Var3;
        this.mainThreadExecutorProvider = ew2Var4;
        this.backgroundThreadExecutorProvider = ew2Var5;
    }

    public static RequestListModule_RepositoryFactory create(ew2<RequestInfoDataSource.LocalDataSource> ew2Var, ew2<SupportUiStorage> ew2Var2, ew2<RequestProvider> ew2Var3, ew2<Executor> ew2Var4, ew2<ExecutorService> ew2Var5) {
        return new RequestListModule_RepositoryFactory(ew2Var, ew2Var2, ew2Var3, ew2Var4, ew2Var5);
    }

    public static RequestInfoDataSource.Repository repository(RequestInfoDataSource.LocalDataSource localDataSource, SupportUiStorage supportUiStorage, RequestProvider requestProvider, Executor executor, ExecutorService executorService) {
        return (RequestInfoDataSource.Repository) cu2.f(RequestListModule.repository(localDataSource, supportUiStorage, requestProvider, executor, executorService));
    }

    @Override // defpackage.ew2
    public RequestInfoDataSource.Repository get() {
        return repository(this.localDataSourceProvider.get(), this.supportUiStorageProvider.get(), this.requestProvider.get(), this.mainThreadExecutorProvider.get(), this.backgroundThreadExecutorProvider.get());
    }
}
