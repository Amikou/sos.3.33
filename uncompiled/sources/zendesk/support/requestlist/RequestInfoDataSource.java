package zendesk.support.requestlist;

import com.google.gson.reflect.TypeToken;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashSet;
import java.util.List;
import java.util.concurrent.Executor;
import zendesk.support.Attachment;
import zendesk.support.Comment;
import zendesk.support.Request;
import zendesk.support.RequestProvider;
import zendesk.support.RequestUpdates;
import zendesk.support.SupportUiStorage;
import zendesk.support.User;
import zendesk.support.requestlist.RequestInfo;

/* loaded from: classes3.dex */
public interface RequestInfoDataSource {
    public static final String LOCAL = "local_request_infos";
    public static final String REMOTE = "remote_request_infos";

    /* loaded from: classes3.dex */
    public static class Disk implements RequestInfoDataSource {
        private final Executor backgroundThreadExecutor;
        private final String cacheKey;
        private final Executor mainThreadExecutor;
        private final SupportUiStorage supportUiStorage;

        public Disk(Executor executor, Executor executor2, SupportUiStorage supportUiStorage, String str) {
            this.mainThreadExecutor = executor;
            this.backgroundThreadExecutor = executor2;
            this.supportUiStorage = supportUiStorage;
            this.cacheKey = str;
        }

        @Override // zendesk.support.requestlist.RequestInfoDataSource
        public void load(final rs4<List<RequestInfo>> rs4Var) {
            this.backgroundThreadExecutor.execute(new Runnable() { // from class: zendesk.support.requestlist.RequestInfoDataSource.Disk.1
                @Override // java.lang.Runnable
                public void run() {
                    final List list = (List) Disk.this.supportUiStorage.read(Disk.this.cacheKey, new TypeToken<List<RequestInfo>>() { // from class: zendesk.support.requestlist.RequestInfoDataSource.Disk.1.1
                    }.getType());
                    Disk.this.mainThreadExecutor.execute(new Runnable() { // from class: zendesk.support.requestlist.RequestInfoDataSource.Disk.1.2
                        @Override // java.lang.Runnable
                        public void run() {
                            rs4Var.onSuccess(l10.e(list));
                        }
                    });
                }
            });
        }

        public void save(final List<RequestInfo> list, final rs4<List<RequestInfo>> rs4Var) {
            this.backgroundThreadExecutor.execute(new Runnable() { // from class: zendesk.support.requestlist.RequestInfoDataSource.Disk.2
                @Override // java.lang.Runnable
                public void run() {
                    Disk.this.supportUiStorage.write(Disk.this.cacheKey, list);
                    if (rs4Var != null) {
                        Disk.this.mainThreadExecutor.execute(new Runnable() { // from class: zendesk.support.requestlist.RequestInfoDataSource.Disk.2.1
                            @Override // java.lang.Runnable
                            public void run() {
                                AnonymousClass2 anonymousClass2 = AnonymousClass2.this;
                                rs4Var.onSuccess(list);
                            }
                        });
                    }
                }
            });
        }
    }

    /* loaded from: classes3.dex */
    public static class LocalDataSource implements RequestInfoDataSource {
        private static final Comparator<RequestInfo> REQUEST_INFO_COMPARATOR = new RequestInfo.LastUpdatedComparator();
        private final Disk disk;

        public LocalDataSource(Disk disk) {
            this.disk = disk;
        }

        public void insert(final RequestInfo requestInfo, final rs4<List<RequestInfo>> rs4Var) {
            this.disk.load(new rs4<List<RequestInfo>>() { // from class: zendesk.support.requestlist.RequestInfoDataSource.LocalDataSource.1
                @Override // defpackage.rs4
                public void onError(cw0 cw0Var) {
                    rs4 rs4Var2 = rs4Var;
                    if (rs4Var2 != null) {
                        rs4Var2.onError(cw0Var);
                    }
                }

                @Override // defpackage.rs4
                public void onSuccess(List<RequestInfo> list) {
                    List<RequestInfo> a = l10.a(list, requestInfo, new ps4<RequestInfo, RequestInfo, Boolean>() { // from class: zendesk.support.requestlist.RequestInfoDataSource.LocalDataSource.1.1
                        @Override // defpackage.ps4
                        public Boolean apply(RequestInfo requestInfo2, RequestInfo requestInfo3) {
                            boolean equals = requestInfo3.getLocalId().equals(requestInfo2.getLocalId());
                            boolean z = true;
                            boolean z2 = ru3.b(requestInfo3.getRemoteId()) && requestInfo3.getRemoteId().equals(requestInfo2.getRemoteId());
                            if (!equals && !z2) {
                                z = false;
                            }
                            return Boolean.valueOf(z);
                        }
                    });
                    Collections.sort(a, LocalDataSource.REQUEST_INFO_COMPARATOR);
                    LocalDataSource.this.disk.save(a, rs4Var);
                }
            });
        }

        @Override // zendesk.support.requestlist.RequestInfoDataSource
        public void load(rs4<List<RequestInfo>> rs4Var) {
            this.disk.load(rs4Var);
        }

        public void remove(final String str, final rs4<List<RequestInfo>> rs4Var) {
            this.disk.load(new rs4<List<RequestInfo>>() { // from class: zendesk.support.requestlist.RequestInfoDataSource.LocalDataSource.2
                @Override // defpackage.rs4
                public void onError(cw0 cw0Var) {
                    rs4 rs4Var2 = rs4Var;
                    if (rs4Var2 != null) {
                        rs4Var2.onError(cw0Var);
                    }
                }

                @Override // defpackage.rs4
                public void onSuccess(List<RequestInfo> list) {
                    LocalDataSource.this.disk.save(l10.f(list, new os4<RequestInfo, Boolean>() { // from class: zendesk.support.requestlist.RequestInfoDataSource.LocalDataSource.2.1
                        @Override // defpackage.os4
                        public Boolean apply(RequestInfo requestInfo) {
                            return Boolean.valueOf(!str.equals(requestInfo.getLocalId()));
                        }
                    }), rs4Var);
                }
            });
        }
    }

    /* loaded from: classes3.dex */
    public static class Network implements RequestInfoDataSource {
        private final RequestProvider requestProvider;

        /* renamed from: zendesk.support.requestlist.RequestInfoDataSource$Network$1  reason: invalid class name */
        /* loaded from: classes3.dex */
        public class AnonymousClass1 extends rs4<List<Request>> {
            public final /* synthetic */ rs4 val$callback;

            public AnonymousClass1(rs4 rs4Var) {
                this.val$callback = rs4Var;
            }

            @Override // defpackage.rs4
            public void onError(cw0 cw0Var) {
                this.val$callback.onError(cw0Var);
            }

            @Override // defpackage.rs4
            public void onSuccess(final List<Request> list) {
                Network.this.requestProvider.getUpdatesForDevice(new rs4<RequestUpdates>() { // from class: zendesk.support.requestlist.RequestInfoDataSource.Network.1.1
                    @Override // defpackage.rs4
                    public void onError(cw0 cw0Var) {
                        AnonymousClass1.this.val$callback.onError(cw0Var);
                    }

                    @Override // defpackage.rs4
                    public void onSuccess(final RequestUpdates requestUpdates) {
                        AnonymousClass1.this.val$callback.onSuccess(l10.k(list, new os4<Request, RequestInfo>() { // from class: zendesk.support.requestlist.RequestInfoDataSource.Network.1.1.1
                            @Override // defpackage.os4
                            public RequestInfo apply(Request request) {
                                return Network.this.map(request, requestUpdates.isRequestUnread(request.getId()));
                            }
                        }));
                    }
                });
            }
        }

        public Network(RequestProvider requestProvider) {
            this.requestProvider = requestProvider;
        }

        /* JADX INFO: Access modifiers changed from: private */
        public RequestInfo map(Request request, boolean z) {
            Comment firstComment = request.getFirstComment();
            Comment lastComment = request.getLastComment();
            return new RequestInfo("", request.getId(), request.getStatus(), z, request.getPublicUpdatedAt(), l10.k(l10.f(request.getLastCommentingAgents(), new os4<User, Boolean>() { // from class: zendesk.support.requestlist.RequestInfoDataSource.Network.2
                @Override // defpackage.os4
                public Boolean apply(User user) {
                    return Boolean.valueOf(user != null);
                }
            }), new os4<User, RequestInfo.AgentInfo>() { // from class: zendesk.support.requestlist.RequestInfoDataSource.Network.3
                @Override // defpackage.os4
                public RequestInfo.AgentInfo apply(User user) {
                    Attachment photo = user.getPhoto();
                    return new RequestInfo.AgentInfo(String.valueOf(user.getId()), user.getName(), photo != null ? photo.getContentUrl() : "");
                }
            }), new RequestInfo.MessageInfo(String.valueOf(firstComment.getId()), firstComment.getCreatedAt(), firstComment.getBody()), new RequestInfo.MessageInfo(String.valueOf(lastComment.getId()), lastComment.getCreatedAt(), lastComment.getBody()), new HashSet());
        }

        @Override // zendesk.support.requestlist.RequestInfoDataSource
        public void load(rs4<List<RequestInfo>> rs4Var) {
            this.requestProvider.getAllRequests(new AnonymousClass1(rs4Var));
        }
    }

    /* loaded from: classes3.dex */
    public static class RemoteDataSource implements RequestInfoDataSource {
        private final Disk disk;
        private final Network network;

        public RemoteDataSource(Network network, Disk disk) {
            this.network = network;
            this.disk = disk;
        }

        @Override // zendesk.support.requestlist.RequestInfoDataSource
        public void load(final rs4<List<RequestInfo>> rs4Var) {
            this.network.load(new rs4<List<RequestInfo>>() { // from class: zendesk.support.requestlist.RequestInfoDataSource.RemoteDataSource.1
                @Override // defpackage.rs4
                public void onError(final cw0 cw0Var) {
                    RemoteDataSource.this.disk.load(new rs4<List<RequestInfo>>() { // from class: zendesk.support.requestlist.RequestInfoDataSource.RemoteDataSource.1.1
                        @Override // defpackage.rs4
                        public void onError(cw0 cw0Var2) {
                            rs4Var.onError(cw0Var2);
                        }

                        @Override // defpackage.rs4
                        public void onSuccess(List<RequestInfo> list) {
                            rs4Var.onSuccess(list);
                            rs4Var.onError(cw0Var);
                        }
                    });
                }

                @Override // defpackage.rs4
                public void onSuccess(List<RequestInfo> list) {
                    RemoteDataSource.this.disk.save(list, rs4Var);
                }
            });
        }
    }

    /* loaded from: classes3.dex */
    public static class Repository implements RequestInfoDataSource {
        private final RequestInfoDataSource localDataSource;
        private final RequestInfoMerger merger;
        private final RequestInfoDataSource remoteDataSource;

        public Repository(RequestInfoDataSource requestInfoDataSource, RequestInfoDataSource requestInfoDataSource2, RequestInfoMerger requestInfoMerger) {
            this.localDataSource = requestInfoDataSource;
            this.remoteDataSource = requestInfoDataSource2;
            this.merger = requestInfoMerger;
        }

        @Override // zendesk.support.requestlist.RequestInfoDataSource
        public void load(final rs4<List<RequestInfo>> rs4Var) {
            this.localDataSource.load(new rs4<List<RequestInfo>>() { // from class: zendesk.support.requestlist.RequestInfoDataSource.Repository.1
                @Override // defpackage.rs4
                public void onError(cw0 cw0Var) {
                }

                @Override // defpackage.rs4
                public void onSuccess(final List<RequestInfo> list) {
                    Repository.this.remoteDataSource.load(new rs4<List<RequestInfo>>() { // from class: zendesk.support.requestlist.RequestInfoDataSource.Repository.1.1
                        @Override // defpackage.rs4
                        public void onError(cw0 cw0Var) {
                            rs4Var.onError(cw0Var);
                        }

                        @Override // defpackage.rs4
                        public void onSuccess(List<RequestInfo> list2) {
                            AnonymousClass1 anonymousClass1 = AnonymousClass1.this;
                            rs4Var.onSuccess(Repository.this.merger.merge(list, list2));
                        }
                    });
                }
            });
        }
    }

    void load(rs4<List<RequestInfo>> rs4Var);
}
