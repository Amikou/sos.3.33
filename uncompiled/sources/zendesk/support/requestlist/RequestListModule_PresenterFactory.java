package zendesk.support.requestlist;

/* loaded from: classes3.dex */
public final class RequestListModule_PresenterFactory implements y11<RequestListPresenter> {
    private final ew2<RequestListModel> modelProvider;
    private final RequestListModule module;

    public RequestListModule_PresenterFactory(RequestListModule requestListModule, ew2<RequestListModel> ew2Var) {
        this.module = requestListModule;
        this.modelProvider = ew2Var;
    }

    public static RequestListModule_PresenterFactory create(RequestListModule requestListModule, ew2<RequestListModel> ew2Var) {
        return new RequestListModule_PresenterFactory(requestListModule, ew2Var);
    }

    public static RequestListPresenter presenter(RequestListModule requestListModule, Object obj) {
        return (RequestListPresenter) cu2.f(requestListModule.presenter((RequestListModel) obj));
    }

    @Override // defpackage.ew2
    public RequestListPresenter get() {
        return presenter(this.module, this.modelProvider.get());
    }
}
