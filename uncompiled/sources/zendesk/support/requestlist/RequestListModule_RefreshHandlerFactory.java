package zendesk.support.requestlist;

/* loaded from: classes3.dex */
public final class RequestListModule_RefreshHandlerFactory implements y11<RequestListSyncHandler> {
    private final ew2<RequestListPresenter> presenterProvider;

    public RequestListModule_RefreshHandlerFactory(ew2<RequestListPresenter> ew2Var) {
        this.presenterProvider = ew2Var;
    }

    public static RequestListModule_RefreshHandlerFactory create(ew2<RequestListPresenter> ew2Var) {
        return new RequestListModule_RefreshHandlerFactory(ew2Var);
    }

    public static RequestListSyncHandler refreshHandler(Object obj) {
        return (RequestListSyncHandler) cu2.f(RequestListModule.refreshHandler((RequestListPresenter) obj));
    }

    @Override // defpackage.ew2
    public RequestListSyncHandler get() {
        return refreshHandler(this.presenterProvider.get());
    }
}
