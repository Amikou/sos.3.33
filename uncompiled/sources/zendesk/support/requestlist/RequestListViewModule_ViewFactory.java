package zendesk.support.requestlist;

import com.squareup.picasso.Picasso;

/* loaded from: classes3.dex */
public final class RequestListViewModule_ViewFactory implements y11<RequestListView> {
    private final RequestListViewModule module;
    private final ew2<Picasso> picassoProvider;

    public RequestListViewModule_ViewFactory(RequestListViewModule requestListViewModule, ew2<Picasso> ew2Var) {
        this.module = requestListViewModule;
        this.picassoProvider = ew2Var;
    }

    public static RequestListViewModule_ViewFactory create(RequestListViewModule requestListViewModule, ew2<Picasso> ew2Var) {
        return new RequestListViewModule_ViewFactory(requestListViewModule, ew2Var);
    }

    public static RequestListView view(RequestListViewModule requestListViewModule, Picasso picasso) {
        return (RequestListView) cu2.f(requestListViewModule.view(picasso));
    }

    @Override // defpackage.ew2
    public RequestListView get() {
        return view(this.module, this.picassoProvider.get());
    }
}
