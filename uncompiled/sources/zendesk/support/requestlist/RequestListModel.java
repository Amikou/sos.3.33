package zendesk.support.requestlist;

import java.util.List;
import zendesk.core.MemoryCache;
import zendesk.support.AggregatedCallback;
import zendesk.support.SupportBlipsProvider;
import zendesk.support.SupportSdkSettings;
import zendesk.support.SupportSettingsProvider;

/* JADX INFO: Access modifiers changed from: package-private */
/* loaded from: classes3.dex */
public class RequestListModel {
    public static final String REQUEST_LIST_ITEMS_CACHE_KEY = "request_list_items";
    public static final String SETTINGS_CACHE_KEY = "request_list_settings";
    private final SupportBlipsProvider blipsProvider;
    private final MemoryCache cache;
    private final RequestInfoDataSource requestInfoDataSource;
    private final SupportSettingsProvider settingsProvider;
    private final AggregatedCallback<SupportSdkSettings> settingsAggregatedCallback = new AggregatedCallback<>();
    private final os4<RequestInfo, RequestListItem> mapper = new os4<RequestInfo, RequestListItem>() { // from class: zendesk.support.requestlist.RequestListModel.2
        @Override // defpackage.os4
        public RequestListItem apply(RequestInfo requestInfo) {
            return new RequestListItem(requestInfo);
        }
    };

    public RequestListModel(RequestInfoDataSource requestInfoDataSource, MemoryCache memoryCache, SupportBlipsProvider supportBlipsProvider, SupportSettingsProvider supportSettingsProvider) {
        this.requestInfoDataSource = requestInfoDataSource;
        this.cache = memoryCache;
        this.blipsProvider = supportBlipsProvider;
        this.settingsProvider = supportSettingsProvider;
    }

    /* JADX INFO: Access modifiers changed from: private */
    public List<RequestInfo> filterClosedRequests(List<RequestInfo> list, boolean z) {
        return z ? list : l10.f(list, new os4<RequestInfo, Boolean>() { // from class: zendesk.support.requestlist.RequestListModel.3
            @Override // defpackage.os4
            public Boolean apply(RequestInfo requestInfo) {
                return Boolean.valueOf(!requestInfo.isClosed());
            }
        });
    }

    public void cacheSupportSdkSettings(SupportSdkSettings supportSdkSettings) {
        this.cache.put(SETTINGS_CACHE_KEY, supportSdkSettings);
    }

    public void cleanup() {
        this.cache.remove(SETTINGS_CACHE_KEY);
        this.cache.remove(REQUEST_LIST_ITEMS_CACHE_KEY);
    }

    public List<RequestListItem> getCachedRequestInfos() {
        return (List) this.cache.get(REQUEST_LIST_ITEMS_CACHE_KEY);
    }

    public SupportSdkSettings getCachedSettings() {
        return (SupportSdkSettings) this.cache.get(SETTINGS_CACHE_KEY);
    }

    public void loadItems(boolean z, final SupportSdkSettings supportSdkSettings, final rs4<List<RequestListItem>> rs4Var) {
        if (!z && getCachedRequestInfos() != null) {
            rs4Var.onSuccess(getCachedRequestInfos());
        } else {
            this.requestInfoDataSource.load(new rs4<List<RequestInfo>>() { // from class: zendesk.support.requestlist.RequestListModel.1
                @Override // defpackage.rs4
                public void onError(cw0 cw0Var) {
                    rs4Var.onError(cw0Var);
                }

                @Override // defpackage.rs4
                public void onSuccess(List<RequestInfo> list) {
                    List k = l10.k(RequestListModel.this.filterClosedRequests(list, supportSdkSettings.isShowClosedRequests()), RequestListModel.this.mapper);
                    RequestListModel.this.cache.put(RequestListModel.REQUEST_LIST_ITEMS_CACHE_KEY, k);
                    rs4Var.onSuccess(k);
                }
            });
        }
    }

    public void loadSettings(rs4<SupportSdkSettings> rs4Var) {
        if (this.settingsAggregatedCallback.add(rs4Var)) {
            this.settingsProvider.getSettings(this.settingsAggregatedCallback);
        }
    }

    public void trackRequestListViewed() {
        this.blipsProvider.requestListViewed();
    }
}
