package zendesk.support.requestlist;

import java.util.Comparator;
import java.util.Date;
import java.util.List;
import java.util.Set;
import zendesk.support.RequestStatus;

/* loaded from: classes3.dex */
public class RequestInfo {
    private final List<AgentInfo> agentInfos;
    private final Set<String> failedMessageIds;
    private final MessageInfo firstMessageInfo;
    private final MessageInfo lastMessageInfo;
    private final Date lastUpdated;
    private final String localId;
    private final String remoteId;
    private final RequestStatus requestStatus;
    private final boolean unread;

    /* loaded from: classes3.dex */
    public static class AgentInfo {
        private final String avatar;
        private final String id;
        private final String name;

        public AgentInfo(String str, String str2, String str3) {
            this.id = str;
            this.name = str2;
            this.avatar = str3;
        }

        public boolean equals(Object obj) {
            if (this == obj) {
                return true;
            }
            if (obj == null || getClass() != obj.getClass()) {
                return false;
            }
            AgentInfo agentInfo = (AgentInfo) obj;
            String str = this.id;
            if (str == null ? agentInfo.id == null : str.equals(agentInfo.id)) {
                String str2 = this.name;
                if (str2 == null ? agentInfo.name == null : str2.equals(agentInfo.name)) {
                    String str3 = this.avatar;
                    String str4 = agentInfo.avatar;
                    return str3 != null ? str3.equals(str4) : str4 == null;
                }
                return false;
            }
            return false;
        }

        public String getAvatar() {
            return this.avatar;
        }

        public String getId() {
            return this.id;
        }

        public String getName() {
            return this.name;
        }

        public int hashCode() {
            String str = this.id;
            int hashCode = (str != null ? str.hashCode() : 0) * 31;
            String str2 = this.name;
            int hashCode2 = (hashCode + (str2 != null ? str2.hashCode() : 0)) * 31;
            String str3 = this.avatar;
            return hashCode2 + (str3 != null ? str3.hashCode() : 0);
        }
    }

    /* loaded from: classes3.dex */
    public static class LastUpdatedComparator implements Comparator<RequestInfo> {
        @Override // java.util.Comparator
        public int compare(RequestInfo requestInfo, RequestInfo requestInfo2) {
            if (requestInfo2 == null) {
                return 1;
            }
            if (requestInfo.getLastUpdated() == null) {
                return requestInfo2.getLastUpdated() == null ? 0 : -1;
            } else if (requestInfo2.getLastUpdated() == null) {
                return 1;
            } else {
                return requestInfo2.getLastUpdated().compareTo(requestInfo.getLastUpdated());
            }
        }
    }

    /* loaded from: classes3.dex */
    public static class MessageInfo {
        private final String body;
        private final Date date;
        private final String id;

        public MessageInfo(String str, Date date, String str2) {
            this.id = str;
            this.date = date;
            this.body = str2;
        }

        public boolean equals(Object obj) {
            if (this == obj) {
                return true;
            }
            if (obj == null || getClass() != obj.getClass()) {
                return false;
            }
            MessageInfo messageInfo = (MessageInfo) obj;
            String str = this.id;
            if (str == null ? messageInfo.id == null : str.equals(messageInfo.id)) {
                Date date = this.date;
                if (date == null ? messageInfo.date == null : date.equals(messageInfo.date)) {
                    String str2 = this.body;
                    String str3 = messageInfo.body;
                    return str2 != null ? str2.equals(str3) : str3 == null;
                }
                return false;
            }
            return false;
        }

        public String getBody() {
            return this.body;
        }

        public Date getDate() {
            return this.date;
        }

        public String getId() {
            return this.id;
        }

        public int hashCode() {
            String str = this.id;
            int hashCode = (str != null ? str.hashCode() : 0) * 31;
            Date date = this.date;
            int hashCode2 = (hashCode + (date != null ? date.hashCode() : 0)) * 31;
            String str2 = this.body;
            return hashCode2 + (str2 != null ? str2.hashCode() : 0);
        }
    }

    public RequestInfo(String str, String str2, RequestStatus requestStatus, boolean z, Date date, List<AgentInfo> list, MessageInfo messageInfo, MessageInfo messageInfo2, Set<String> set) {
        this.localId = str;
        this.remoteId = str2;
        this.requestStatus = requestStatus;
        this.unread = z;
        this.lastUpdated = date;
        this.agentInfos = list;
        this.firstMessageInfo = messageInfo;
        this.lastMessageInfo = messageInfo2;
        this.failedMessageIds = set;
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null || getClass() != obj.getClass()) {
            return false;
        }
        RequestInfo requestInfo = (RequestInfo) obj;
        if (this.unread != requestInfo.unread) {
            return false;
        }
        String str = this.localId;
        if (str == null ? requestInfo.localId == null : str.equals(requestInfo.localId)) {
            String str2 = this.remoteId;
            if (str2 == null ? requestInfo.remoteId == null : str2.equals(requestInfo.remoteId)) {
                if (this.requestStatus != requestInfo.requestStatus) {
                    return false;
                }
                Date date = this.lastUpdated;
                if (date == null ? requestInfo.lastUpdated == null : date.equals(requestInfo.lastUpdated)) {
                    List<AgentInfo> list = this.agentInfos;
                    if (list == null ? requestInfo.agentInfos == null : list.equals(requestInfo.agentInfos)) {
                        MessageInfo messageInfo = this.firstMessageInfo;
                        if (messageInfo == null ? requestInfo.firstMessageInfo == null : messageInfo.equals(requestInfo.firstMessageInfo)) {
                            MessageInfo messageInfo2 = this.lastMessageInfo;
                            if (messageInfo2 == null ? requestInfo.lastMessageInfo == null : messageInfo2.equals(requestInfo.lastMessageInfo)) {
                                Set<String> set = this.failedMessageIds;
                                return set != null ? set.equals(requestInfo.failedMessageIds) : requestInfo.failedMessageIds == null;
                            }
                            return false;
                        }
                        return false;
                    }
                    return false;
                }
                return false;
            }
            return false;
        }
        return false;
    }

    public List<AgentInfo> getAgentInfos() {
        return this.agentInfos;
    }

    public Set<String> getFailedMessageIds() {
        return this.failedMessageIds;
    }

    public MessageInfo getFirstMessageInfo() {
        return this.firstMessageInfo;
    }

    public MessageInfo getLastMessageInfo() {
        return this.lastMessageInfo;
    }

    public Date getLastUpdated() {
        return this.lastUpdated;
    }

    public String getLocalId() {
        return this.localId;
    }

    public String getRemoteId() {
        return this.remoteId;
    }

    public RequestStatus getRequestStatus() {
        return this.requestStatus;
    }

    public int hashCode() {
        String str = this.localId;
        int hashCode = (str != null ? str.hashCode() : 0) * 31;
        String str2 = this.remoteId;
        int hashCode2 = (hashCode + (str2 != null ? str2.hashCode() : 0)) * 31;
        RequestStatus requestStatus = this.requestStatus;
        int hashCode3 = (((hashCode2 + (requestStatus != null ? requestStatus.hashCode() : 0)) * 31) + (this.unread ? 1 : 0)) * 31;
        Date date = this.lastUpdated;
        int hashCode4 = (hashCode3 + (date != null ? date.hashCode() : 0)) * 31;
        List<AgentInfo> list = this.agentInfos;
        int hashCode5 = (hashCode4 + (list != null ? list.hashCode() : 0)) * 31;
        MessageInfo messageInfo = this.firstMessageInfo;
        int hashCode6 = (hashCode5 + (messageInfo != null ? messageInfo.hashCode() : 0)) * 31;
        MessageInfo messageInfo2 = this.lastMessageInfo;
        int hashCode7 = (hashCode6 + (messageInfo2 != null ? messageInfo2.hashCode() : 0)) * 31;
        Set<String> set = this.failedMessageIds;
        return hashCode7 + (set != null ? set.hashCode() : 0);
    }

    public boolean isClosed() {
        return RequestStatus.Closed.equals(this.requestStatus);
    }

    public boolean isUnread() {
        return this.unread;
    }
}
