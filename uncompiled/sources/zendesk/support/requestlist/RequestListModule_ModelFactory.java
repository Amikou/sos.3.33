package zendesk.support.requestlist;

import zendesk.core.MemoryCache;
import zendesk.support.SupportBlipsProvider;
import zendesk.support.SupportSettingsProvider;
import zendesk.support.requestlist.RequestInfoDataSource;

/* loaded from: classes3.dex */
public final class RequestListModule_ModelFactory implements y11<RequestListModel> {
    private final ew2<SupportBlipsProvider> blipsProvider;
    private final ew2<MemoryCache> memoryCacheProvider;
    private final RequestListModule module;
    private final ew2<RequestInfoDataSource.Repository> requestInfoDataSourceProvider;
    private final ew2<SupportSettingsProvider> settingsProvider;

    public RequestListModule_ModelFactory(RequestListModule requestListModule, ew2<RequestInfoDataSource.Repository> ew2Var, ew2<MemoryCache> ew2Var2, ew2<SupportBlipsProvider> ew2Var3, ew2<SupportSettingsProvider> ew2Var4) {
        this.module = requestListModule;
        this.requestInfoDataSourceProvider = ew2Var;
        this.memoryCacheProvider = ew2Var2;
        this.blipsProvider = ew2Var3;
        this.settingsProvider = ew2Var4;
    }

    public static RequestListModule_ModelFactory create(RequestListModule requestListModule, ew2<RequestInfoDataSource.Repository> ew2Var, ew2<MemoryCache> ew2Var2, ew2<SupportBlipsProvider> ew2Var3, ew2<SupportSettingsProvider> ew2Var4) {
        return new RequestListModule_ModelFactory(requestListModule, ew2Var, ew2Var2, ew2Var3, ew2Var4);
    }

    public static RequestListModel model(RequestListModule requestListModule, RequestInfoDataSource.Repository repository, MemoryCache memoryCache, SupportBlipsProvider supportBlipsProvider, SupportSettingsProvider supportSettingsProvider) {
        return (RequestListModel) cu2.f(requestListModule.model(repository, memoryCache, supportBlipsProvider, supportSettingsProvider));
    }

    @Override // defpackage.ew2
    public RequestListModel get() {
        return model(this.module, this.requestInfoDataSourceProvider.get(), this.memoryCacheProvider.get(), this.blipsProvider.get(), this.settingsProvider.get());
    }
}
