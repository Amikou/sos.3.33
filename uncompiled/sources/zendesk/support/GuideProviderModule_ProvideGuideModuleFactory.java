package zendesk.support;

import zendesk.core.RestServiceProvider;

/* loaded from: classes3.dex */
public final class GuideProviderModule_ProvideGuideModuleFactory implements y11<GuideModule> {
    private final ew2<ArticleVoteStorage> articleVoteStorageProvider;
    private final ew2<HelpCenterBlipsProvider> blipsProvider;
    private final ew2<HelpCenterProvider> helpCenterProvider;
    private final GuideProviderModule module;
    private final ew2<RestServiceProvider> restServiceProvider;
    private final ew2<HelpCenterSettingsProvider> settingsProvider;

    public GuideProviderModule_ProvideGuideModuleFactory(GuideProviderModule guideProviderModule, ew2<HelpCenterProvider> ew2Var, ew2<HelpCenterSettingsProvider> ew2Var2, ew2<HelpCenterBlipsProvider> ew2Var3, ew2<ArticleVoteStorage> ew2Var4, ew2<RestServiceProvider> ew2Var5) {
        this.module = guideProviderModule;
        this.helpCenterProvider = ew2Var;
        this.settingsProvider = ew2Var2;
        this.blipsProvider = ew2Var3;
        this.articleVoteStorageProvider = ew2Var4;
        this.restServiceProvider = ew2Var5;
    }

    public static GuideProviderModule_ProvideGuideModuleFactory create(GuideProviderModule guideProviderModule, ew2<HelpCenterProvider> ew2Var, ew2<HelpCenterSettingsProvider> ew2Var2, ew2<HelpCenterBlipsProvider> ew2Var3, ew2<ArticleVoteStorage> ew2Var4, ew2<RestServiceProvider> ew2Var5) {
        return new GuideProviderModule_ProvideGuideModuleFactory(guideProviderModule, ew2Var, ew2Var2, ew2Var3, ew2Var4, ew2Var5);
    }

    public static GuideModule provideGuideModule(GuideProviderModule guideProviderModule, HelpCenterProvider helpCenterProvider, HelpCenterSettingsProvider helpCenterSettingsProvider, HelpCenterBlipsProvider helpCenterBlipsProvider, ArticleVoteStorage articleVoteStorage, RestServiceProvider restServiceProvider) {
        return (GuideModule) cu2.f(guideProviderModule.provideGuideModule(helpCenterProvider, helpCenterSettingsProvider, helpCenterBlipsProvider, articleVoteStorage, restServiceProvider));
    }

    @Override // defpackage.ew2
    public GuideModule get() {
        return provideGuideModule(this.module, this.helpCenterProvider.get(), this.settingsProvider.get(), this.blipsProvider.get(), this.articleVoteStorageProvider.get(), this.restServiceProvider.get());
    }
}
