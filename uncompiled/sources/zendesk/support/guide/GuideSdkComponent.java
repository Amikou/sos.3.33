package zendesk.support.guide;

/* loaded from: classes3.dex */
public interface GuideSdkComponent {
    void inject(GuideSdkDependencyProvider guideSdkDependencyProvider);

    void inject(HelpCenterActivity helpCenterActivity);

    void inject(HelpCenterFragment helpCenterFragment);

    void inject(ViewArticleActivity viewArticleActivity);
}
