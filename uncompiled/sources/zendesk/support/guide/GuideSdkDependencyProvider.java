package zendesk.support.guide;

import android.annotation.SuppressLint;
import java.util.UUID;
import zendesk.core.ActionHandler;
import zendesk.core.ActionHandlerRegistry;
import zendesk.core.Zendesk;
import zendesk.support.Guide;
import zendesk.support.GuideModule;

/* loaded from: classes3.dex */
public enum GuideSdkDependencyProvider {
    INSTANCE;
    
    public static final String NOT_INITIALIZED_LOG = "Zendesk is not initialized or no identity was set. Make sure Zendesk.INSTANCE.init(...), Zendesk.INSTANCE.setIdentity(...), Guide.INSTANCE.init(...) was called ";
    public ActionHandler actionHandler;
    private GuideSdkComponent guideSdkComponent;
    private UUID id;
    public ActionHandlerRegistry registry;

    public void initForTesting(GuideSdkComponent guideSdkComponent, UUID uuid) {
        this.guideSdkComponent = guideSdkComponent;
        this.id = uuid;
    }

    public boolean isInitialized() {
        return Zendesk.INSTANCE.isInitialized() && Guide.INSTANCE.isInitialized();
    }

    @SuppressLint({"RestrictedApi"})
    public GuideSdkComponent provideGuideSdkComponent() {
        GuideModule guideModule = Guide.INSTANCE.guideModule();
        if (!guideModule.getId().equals(this.id)) {
            this.guideSdkComponent = DaggerGuideSdkComponent.builder().coreModule(Zendesk.INSTANCE.coreModule()).guideModule(guideModule).build();
            this.id = guideModule.getId();
            this.guideSdkComponent.inject(this);
            this.registry.add(this.actionHandler);
        }
        return this.guideSdkComponent;
    }
}
