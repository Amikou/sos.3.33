package zendesk.support.guide;

import java.util.List;
import zendesk.support.HelpCenterProvider;
import zendesk.support.HelpCenterSearch;
import zendesk.support.HelpCenterSettings;
import zendesk.support.HelpCenterSettingsProvider;
import zendesk.support.SearchArticle;

/* loaded from: classes3.dex */
public class HelpCenterModel implements HelpCenterMvp$Model {
    private final HelpCenterProvider provider;
    private final HelpCenterSettingsProvider settingsProvider;

    public HelpCenterModel(HelpCenterProvider helpCenterProvider, HelpCenterSettingsProvider helpCenterSettingsProvider) {
        this.provider = helpCenterProvider;
        this.settingsProvider = helpCenterSettingsProvider;
    }

    @Override // zendesk.support.guide.HelpCenterMvp$Model
    public void getSettings(rs4<HelpCenterSettings> rs4Var) {
        this.settingsProvider.getSettings(rs4Var);
    }

    @Override // zendesk.support.guide.HelpCenterMvp$Model
    public void search(List<Long> list, List<Long> list2, String str, String[] strArr, rs4<List<SearchArticle>> rs4Var) {
        this.provider.searchArticles(new HelpCenterSearch.Builder().withQuery(str).withCategoryIds(list).withSectionIds(list2).withLabelNames(strArr).build(), rs4Var);
    }
}
