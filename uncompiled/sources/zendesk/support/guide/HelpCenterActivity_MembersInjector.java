package zendesk.support.guide;

import zendesk.core.ActionHandlerRegistry;
import zendesk.core.NetworkInfoProvider;
import zendesk.support.HelpCenterProvider;
import zendesk.support.HelpCenterSettingsProvider;

/* loaded from: classes3.dex */
public final class HelpCenterActivity_MembersInjector implements i72<HelpCenterActivity> {
    private final ew2<ActionHandlerRegistry> actionHandlerRegistryProvider;
    private final ew2<z40> configurationHelperProvider;
    private final ew2<HelpCenterProvider> helpCenterProvider;
    private final ew2<NetworkInfoProvider> networkInfoProvider;
    private final ew2<HelpCenterSettingsProvider> settingsProvider;

    public HelpCenterActivity_MembersInjector(ew2<HelpCenterProvider> ew2Var, ew2<HelpCenterSettingsProvider> ew2Var2, ew2<NetworkInfoProvider> ew2Var3, ew2<ActionHandlerRegistry> ew2Var4, ew2<z40> ew2Var5) {
        this.helpCenterProvider = ew2Var;
        this.settingsProvider = ew2Var2;
        this.networkInfoProvider = ew2Var3;
        this.actionHandlerRegistryProvider = ew2Var4;
        this.configurationHelperProvider = ew2Var5;
    }

    public static i72<HelpCenterActivity> create(ew2<HelpCenterProvider> ew2Var, ew2<HelpCenterSettingsProvider> ew2Var2, ew2<NetworkInfoProvider> ew2Var3, ew2<ActionHandlerRegistry> ew2Var4, ew2<z40> ew2Var5) {
        return new HelpCenterActivity_MembersInjector(ew2Var, ew2Var2, ew2Var3, ew2Var4, ew2Var5);
    }

    public static void injectActionHandlerRegistry(HelpCenterActivity helpCenterActivity, ActionHandlerRegistry actionHandlerRegistry) {
        helpCenterActivity.actionHandlerRegistry = actionHandlerRegistry;
    }

    public static void injectConfigurationHelper(HelpCenterActivity helpCenterActivity, z40 z40Var) {
        helpCenterActivity.configurationHelper = z40Var;
    }

    public static void injectHelpCenterProvider(HelpCenterActivity helpCenterActivity, HelpCenterProvider helpCenterProvider) {
        helpCenterActivity.helpCenterProvider = helpCenterProvider;
    }

    public static void injectNetworkInfoProvider(HelpCenterActivity helpCenterActivity, NetworkInfoProvider networkInfoProvider) {
        helpCenterActivity.networkInfoProvider = networkInfoProvider;
    }

    public static void injectSettingsProvider(HelpCenterActivity helpCenterActivity, HelpCenterSettingsProvider helpCenterSettingsProvider) {
        helpCenterActivity.settingsProvider = helpCenterSettingsProvider;
    }

    public void injectMembers(HelpCenterActivity helpCenterActivity) {
        injectHelpCenterProvider(helpCenterActivity, this.helpCenterProvider.get());
        injectSettingsProvider(helpCenterActivity, this.settingsProvider.get());
        injectNetworkInfoProvider(helpCenterActivity, this.networkInfoProvider.get());
        injectActionHandlerRegistry(helpCenterActivity, this.actionHandlerRegistryProvider.get());
        injectConfigurationHelper(helpCenterActivity, this.configurationHelperProvider.get());
    }
}
