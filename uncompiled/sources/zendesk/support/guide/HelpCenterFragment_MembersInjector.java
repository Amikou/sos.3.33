package zendesk.support.guide;

import zendesk.core.NetworkInfoProvider;
import zendesk.support.HelpCenterProvider;

/* loaded from: classes3.dex */
public final class HelpCenterFragment_MembersInjector implements i72<HelpCenterFragment> {
    private final ew2<HelpCenterProvider> helpCenterProvider;
    private final ew2<NetworkInfoProvider> networkInfoProvider;

    public HelpCenterFragment_MembersInjector(ew2<HelpCenterProvider> ew2Var, ew2<NetworkInfoProvider> ew2Var2) {
        this.helpCenterProvider = ew2Var;
        this.networkInfoProvider = ew2Var2;
    }

    public static i72<HelpCenterFragment> create(ew2<HelpCenterProvider> ew2Var, ew2<NetworkInfoProvider> ew2Var2) {
        return new HelpCenterFragment_MembersInjector(ew2Var, ew2Var2);
    }

    public static void injectHelpCenterProvider(HelpCenterFragment helpCenterFragment, HelpCenterProvider helpCenterProvider) {
        helpCenterFragment.helpCenterProvider = helpCenterProvider;
    }

    public static void injectNetworkInfoProvider(HelpCenterFragment helpCenterFragment, NetworkInfoProvider networkInfoProvider) {
        helpCenterFragment.networkInfoProvider = networkInfoProvider;
    }

    public void injectMembers(HelpCenterFragment helpCenterFragment) {
        injectHelpCenterProvider(helpCenterFragment, this.helpCenterProvider.get());
        injectNetworkInfoProvider(helpCenterFragment, this.networkInfoProvider.get());
    }
}
