package zendesk.support.guide;

import okhttp3.OkHttpClient;
import zendesk.core.ActionHandlerRegistry;
import zendesk.core.ApplicationConfiguration;
import zendesk.core.NetworkInfoProvider;
import zendesk.support.ArticleVoteStorage;
import zendesk.support.HelpCenterProvider;
import zendesk.support.HelpCenterSettingsProvider;

/* loaded from: classes3.dex */
public final class ViewArticleActivity_MembersInjector implements i72<ViewArticleActivity> {
    private final ew2<ActionHandlerRegistry> actionHandlerRegistryProvider;
    private final ew2<ApplicationConfiguration> applicationConfigurationProvider;
    private final ew2<ArticleVoteStorage> articleVoteStorageProvider;
    private final ew2<z40> configurationHelperProvider;
    private final ew2<HelpCenterProvider> helpCenterProvider;
    private final ew2<NetworkInfoProvider> networkInfoProvider;
    private final ew2<OkHttpClient> okHttpClientProvider;
    private final ew2<HelpCenterSettingsProvider> settingsProvider;

    public ViewArticleActivity_MembersInjector(ew2<OkHttpClient> ew2Var, ew2<ApplicationConfiguration> ew2Var2, ew2<HelpCenterProvider> ew2Var3, ew2<ArticleVoteStorage> ew2Var4, ew2<NetworkInfoProvider> ew2Var5, ew2<HelpCenterSettingsProvider> ew2Var6, ew2<ActionHandlerRegistry> ew2Var7, ew2<z40> ew2Var8) {
        this.okHttpClientProvider = ew2Var;
        this.applicationConfigurationProvider = ew2Var2;
        this.helpCenterProvider = ew2Var3;
        this.articleVoteStorageProvider = ew2Var4;
        this.networkInfoProvider = ew2Var5;
        this.settingsProvider = ew2Var6;
        this.actionHandlerRegistryProvider = ew2Var7;
        this.configurationHelperProvider = ew2Var8;
    }

    public static i72<ViewArticleActivity> create(ew2<OkHttpClient> ew2Var, ew2<ApplicationConfiguration> ew2Var2, ew2<HelpCenterProvider> ew2Var3, ew2<ArticleVoteStorage> ew2Var4, ew2<NetworkInfoProvider> ew2Var5, ew2<HelpCenterSettingsProvider> ew2Var6, ew2<ActionHandlerRegistry> ew2Var7, ew2<z40> ew2Var8) {
        return new ViewArticleActivity_MembersInjector(ew2Var, ew2Var2, ew2Var3, ew2Var4, ew2Var5, ew2Var6, ew2Var7, ew2Var8);
    }

    public static void injectActionHandlerRegistry(ViewArticleActivity viewArticleActivity, ActionHandlerRegistry actionHandlerRegistry) {
        viewArticleActivity.actionHandlerRegistry = actionHandlerRegistry;
    }

    public static void injectApplicationConfiguration(ViewArticleActivity viewArticleActivity, ApplicationConfiguration applicationConfiguration) {
        viewArticleActivity.applicationConfiguration = applicationConfiguration;
    }

    public static void injectArticleVoteStorage(ViewArticleActivity viewArticleActivity, ArticleVoteStorage articleVoteStorage) {
        viewArticleActivity.articleVoteStorage = articleVoteStorage;
    }

    public static void injectConfigurationHelper(ViewArticleActivity viewArticleActivity, z40 z40Var) {
        viewArticleActivity.configurationHelper = z40Var;
    }

    public static void injectHelpCenterProvider(ViewArticleActivity viewArticleActivity, HelpCenterProvider helpCenterProvider) {
        viewArticleActivity.helpCenterProvider = helpCenterProvider;
    }

    public static void injectNetworkInfoProvider(ViewArticleActivity viewArticleActivity, NetworkInfoProvider networkInfoProvider) {
        viewArticleActivity.networkInfoProvider = networkInfoProvider;
    }

    public static void injectOkHttpClient(ViewArticleActivity viewArticleActivity, OkHttpClient okHttpClient) {
        viewArticleActivity.okHttpClient = okHttpClient;
    }

    public static void injectSettingsProvider(ViewArticleActivity viewArticleActivity, HelpCenterSettingsProvider helpCenterSettingsProvider) {
        viewArticleActivity.settingsProvider = helpCenterSettingsProvider;
    }

    public void injectMembers(ViewArticleActivity viewArticleActivity) {
        injectOkHttpClient(viewArticleActivity, this.okHttpClientProvider.get());
        injectApplicationConfiguration(viewArticleActivity, this.applicationConfigurationProvider.get());
        injectHelpCenterProvider(viewArticleActivity, this.helpCenterProvider.get());
        injectArticleVoteStorage(viewArticleActivity, this.articleVoteStorageProvider.get());
        injectNetworkInfoProvider(viewArticleActivity, this.networkInfoProvider.get());
        injectSettingsProvider(viewArticleActivity, this.settingsProvider.get());
        injectActionHandlerRegistry(viewArticleActivity, this.actionHandlerRegistryProvider.get());
        injectConfigurationHelper(viewArticleActivity, this.configurationHelperProvider.get());
    }
}
