package zendesk.support.guide;

import java.util.List;
import zendesk.support.ArticleItem;
import zendesk.support.HelpItem;
import zendesk.support.SectionItem;

/* loaded from: classes3.dex */
public interface HelpMvp$Model {
    void getArticles(List<Long> list, List<Long> list2, String[] strArr, rs4<List<HelpItem>> rs4Var);

    void getArticlesForSection(SectionItem sectionItem, String[] strArr, rs4<List<ArticleItem>> rs4Var);
}
