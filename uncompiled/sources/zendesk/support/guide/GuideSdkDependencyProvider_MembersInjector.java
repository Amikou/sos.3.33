package zendesk.support.guide;

import zendesk.core.ActionHandler;
import zendesk.core.ActionHandlerRegistry;

/* loaded from: classes3.dex */
public final class GuideSdkDependencyProvider_MembersInjector implements i72<GuideSdkDependencyProvider> {
    private final ew2<ActionHandler> actionHandlerProvider;
    private final ew2<ActionHandlerRegistry> registryProvider;

    public GuideSdkDependencyProvider_MembersInjector(ew2<ActionHandlerRegistry> ew2Var, ew2<ActionHandler> ew2Var2) {
        this.registryProvider = ew2Var;
        this.actionHandlerProvider = ew2Var2;
    }

    public static i72<GuideSdkDependencyProvider> create(ew2<ActionHandlerRegistry> ew2Var, ew2<ActionHandler> ew2Var2) {
        return new GuideSdkDependencyProvider_MembersInjector(ew2Var, ew2Var2);
    }

    public static void injectActionHandler(Object obj, ActionHandler actionHandler) {
        ((GuideSdkDependencyProvider) obj).actionHandler = actionHandler;
    }

    public static void injectRegistry(Object obj, ActionHandlerRegistry actionHandlerRegistry) {
        ((GuideSdkDependencyProvider) obj).registry = actionHandlerRegistry;
    }

    public void injectMembers(GuideSdkDependencyProvider guideSdkDependencyProvider) {
        injectRegistry(guideSdkDependencyProvider, this.registryProvider.get());
        injectActionHandler(guideSdkDependencyProvider, this.actionHandlerProvider.get());
    }
}
