package zendesk.support.guide;

/* loaded from: classes3.dex */
public final class GuideSdkModule_ConfigurationHelperFactory implements y11<z40> {
    private final GuideSdkModule module;

    public GuideSdkModule_ConfigurationHelperFactory(GuideSdkModule guideSdkModule) {
        this.module = guideSdkModule;
    }

    public static z40 configurationHelper(GuideSdkModule guideSdkModule) {
        return (z40) cu2.f(guideSdkModule.configurationHelper());
    }

    public static GuideSdkModule_ConfigurationHelperFactory create(GuideSdkModule guideSdkModule) {
        return new GuideSdkModule_ConfigurationHelperFactory(guideSdkModule);
    }

    @Override // defpackage.ew2
    public z40 get() {
        return configurationHelper(this.module);
    }
}
