package zendesk.support.guide;

import android.content.Context;
import android.text.SpannableString;
import android.text.style.StyleSpan;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import androidx.recyclerview.widget.RecyclerView;
import com.zendesk.logger.Logger;
import java.util.Collections;
import java.util.List;
import java.util.Locale;
import zendesk.support.HelpCenterProvider;
import zendesk.support.SearchArticle;

/* loaded from: classes3.dex */
public class HelpSearchRecyclerViewAdapter extends RecyclerView.Adapter {
    private static final int TYPE_ARTICLE = 531;
    private static final int TYPE_NO_RESULTS = 441;
    private static final int TYPE_PADDING = 423;
    private final HelpCenterProvider helpCenterProvider;
    private final HelpCenterConfiguration helpCenterUiConfig;
    private String query;
    private boolean resultsCleared = false;
    private List<SearchArticle> searchArticles;

    /* loaded from: classes3.dex */
    public class HelpSearchViewHolder extends RecyclerView.a0 {
        private Context context;
        private TextView subtitleTextView;
        private TextView titleTextView;

        public HelpSearchViewHolder(View view, Context context) {
            super(view);
            this.titleTextView = (TextView) view.findViewById(e03.title);
            this.subtitleTextView = (TextView) view.findViewById(e03.subtitle);
            this.context = context;
        }

        public void bindTo(final SearchArticle searchArticle) {
            if (searchArticle != null && searchArticle.getArticle() != null) {
                String title = searchArticle.getArticle().getTitle() != null ? searchArticle.getArticle().getTitle() : "";
                int indexOf = HelpSearchRecyclerViewAdapter.this.query == null ? -1 : title.toLowerCase(Locale.getDefault()).indexOf(HelpSearchRecyclerViewAdapter.this.query.toLowerCase(Locale.getDefault()));
                if (indexOf != -1) {
                    SpannableString spannableString = new SpannableString(title);
                    spannableString.setSpan(new StyleSpan(1), indexOf, HelpSearchRecyclerViewAdapter.this.query.length() + indexOf, 18);
                    this.titleTextView.setText(spannableString);
                } else {
                    this.titleTextView.setText(title);
                }
                this.subtitleTextView.setText(this.context.getString(t13.help_search_subtitle_format, searchArticle.getCategory().getName(), searchArticle.getSection().getName()));
                this.itemView.setOnClickListener(new View.OnClickListener() { // from class: zendesk.support.guide.HelpSearchRecyclerViewAdapter.HelpSearchViewHolder.1
                    @Override // android.view.View.OnClickListener
                    public void onClick(View view) {
                        HelpSearchRecyclerViewAdapter.this.helpCenterProvider.submitRecordArticleView(searchArticle.getArticle(), h12.c(searchArticle.getArticle().getLocale()), new rs4<Void>() { // from class: zendesk.support.guide.HelpSearchRecyclerViewAdapter.HelpSearchViewHolder.1.1
                            @Override // defpackage.rs4
                            public void onError(cw0 cw0Var) {
                                Logger.e(HelpCenterActivity.LOG_TAG, "Error submitting Help Center reporting: [reason] %s [isNetworkError] %s [status] %d", cw0Var.h(), Boolean.valueOf(cw0Var.j()), Integer.valueOf(cw0Var.e()));
                            }

                            @Override // defpackage.rs4
                            public void onSuccess(Void r1) {
                            }
                        });
                        ViewArticleActivity.builder(searchArticle.getArticle()).show(HelpSearchViewHolder.this.itemView.getContext(), HelpSearchRecyclerViewAdapter.this.helpCenterUiConfig.getConfigurations());
                    }
                });
                return;
            }
            Logger.e(HelpCenterActivity.LOG_TAG, "The article was null, cannot bind the view.", new Object[0]);
        }
    }

    /* loaded from: classes3.dex */
    public class NoResultsViewHolder extends RecyclerView.a0 {
        public NoResultsViewHolder(View view) {
            super(view);
        }
    }

    /* loaded from: classes3.dex */
    public class PaddingViewHolder extends RecyclerView.a0 {
        public PaddingViewHolder(View view) {
            super(view);
        }
    }

    public HelpSearchRecyclerViewAdapter(List<SearchArticle> list, String str, HelpCenterConfiguration helpCenterConfiguration, HelpCenterProvider helpCenterProvider) {
        this.searchArticles = list;
        this.query = str;
        this.helpCenterUiConfig = helpCenterConfiguration;
        this.helpCenterProvider = helpCenterProvider;
    }

    private int getPaddingExtraItem() {
        return this.helpCenterUiConfig.isContactUsButtonVisible() ? 1 : 0;
    }

    public void clearResults() {
        this.resultsCleared = true;
        this.searchArticles = Collections.emptyList();
        this.query = "";
        notifyDataSetChanged();
    }

    @Override // androidx.recyclerview.widget.RecyclerView.Adapter
    public int getItemCount() {
        if (this.resultsCleared) {
            return 0;
        }
        return Math.max(this.searchArticles.size() + getPaddingExtraItem(), 1);
    }

    @Override // androidx.recyclerview.widget.RecyclerView.Adapter
    public int getItemViewType(int i) {
        return (i == 0 && this.searchArticles.size() == 0) ? TYPE_NO_RESULTS : (i <= 0 || i != this.searchArticles.size()) ? TYPE_ARTICLE : TYPE_PADDING;
    }

    @Override // androidx.recyclerview.widget.RecyclerView.Adapter
    public void onBindViewHolder(RecyclerView.a0 a0Var, int i) {
        if (TYPE_ARTICLE == getItemViewType(i)) {
            ((HelpSearchViewHolder) a0Var).bindTo(this.searchArticles.get(i));
        }
    }

    @Override // androidx.recyclerview.widget.RecyclerView.Adapter
    public RecyclerView.a0 onCreateViewHolder(ViewGroup viewGroup, int i) {
        if (i != TYPE_PADDING) {
            if (i != TYPE_NO_RESULTS) {
                if (i != TYPE_ARTICLE) {
                    return null;
                }
                return new HelpSearchViewHolder(LayoutInflater.from(viewGroup.getContext()).inflate(a13.zs_row_search_article, viewGroup, false), viewGroup.getContext());
            }
            return new NoResultsViewHolder(LayoutInflater.from(viewGroup.getContext()).inflate(a13.zs_row_no_articles_found, viewGroup, false));
        }
        return new PaddingViewHolder(LayoutInflater.from(viewGroup.getContext()).inflate(a13.zs_row_padding, viewGroup, false));
    }

    public void update(List<SearchArticle> list, String str) {
        this.resultsCleared = false;
        this.searchArticles = list;
        this.query = str;
        notifyDataSetChanged();
    }
}
