package zendesk.support.guide;

import android.content.Context;
import android.content.res.ColorStateList;
import android.graphics.PorterDuff;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.GradientDrawable;
import android.graphics.drawable.StateListDrawable;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.RelativeLayout;
import androidx.core.graphics.drawable.a;
import com.zendesk.logger.Logger;
import zendesk.support.ArticleVote;
import zendesk.support.ArticleVoteStorage;
import zendesk.support.HelpCenterProvider;

/* JADX INFO: Access modifiers changed from: package-private */
/* loaded from: classes3.dex */
public class ArticleVotingView extends RelativeLayout {
    private Long articleId;
    private ArticleVote articleVote;
    private ArticleVoteStorage articleVoteStorage;
    private ImageButton downvoteButton;
    private ViewGroup downvoteButtonFrame;
    private HelpCenterProvider helpCenterProvider;
    private ImageButton upvoteButton;
    private ViewGroup upvoteButtonFrame;

    /* renamed from: zendesk.support.guide.ArticleVotingView$6  reason: invalid class name */
    /* loaded from: classes3.dex */
    public static /* synthetic */ class AnonymousClass6 {
        public static final /* synthetic */ int[] $SwitchMap$zendesk$support$guide$ArticleVotingView$VoteState;

        static {
            int[] iArr = new int[VoteState.values().length];
            $SwitchMap$zendesk$support$guide$ArticleVotingView$VoteState = iArr;
            try {
                iArr[VoteState.NONE.ordinal()] = 1;
            } catch (NoSuchFieldError unused) {
            }
            try {
                $SwitchMap$zendesk$support$guide$ArticleVotingView$VoteState[VoteState.UPVOTED.ordinal()] = 2;
            } catch (NoSuchFieldError unused2) {
            }
            try {
                $SwitchMap$zendesk$support$guide$ArticleVotingView$VoteState[VoteState.DOWNVOTED.ordinal()] = 3;
            } catch (NoSuchFieldError unused3) {
            }
        }
    }

    /* loaded from: classes3.dex */
    public enum VoteState {
        UPVOTED,
        DOWNVOTED,
        NONE;

        public static VoteState fromArticleVote(ArticleVote articleVote) {
            if (articleVote != null && articleVote.getValue() != null) {
                int intValue = articleVote.getValue().intValue();
                if (intValue > 0) {
                    return UPVOTED;
                }
                if (intValue < 0) {
                    return DOWNVOTED;
                }
                return NONE;
            }
            return NONE;
        }
    }

    public ArticleVotingView(Context context) {
        super(context);
        setupViews(context);
    }

    private GradientDrawable buildButtonBackground(Context context, int i) {
        int dimensionPixelSize = context.getResources().getDimensionPixelSize(kz2.zs_help_voting_button_border_corner_radius);
        int d = m70.d(context, uy2.zs_help_voting_button_border);
        int dimensionPixelSize2 = context.getResources().getDimensionPixelSize(kz2.zs_help_voting_button_border_width);
        GradientDrawable gradientDrawable = new GradientDrawable();
        gradientDrawable.setShape(0);
        gradientDrawable.setCornerRadius(dimensionPixelSize);
        gradientDrawable.setColor(i);
        gradientDrawable.setStroke(dimensionPixelSize2, d);
        return gradientDrawable;
    }

    private ColorStateList colorStateList(int i, int i2) {
        return new ColorStateList(new int[][]{new int[]{16843518}, new int[]{16842919}, new int[0]}, new int[]{i, i, i2});
    }

    /* JADX INFO: Access modifiers changed from: private */
    public void downvoteArticle() {
        if (this.articleId == null) {
            Logger.k(ViewArticleActivity.LOG_TAG, "Cannot downvote article, articleId is null. Make sure you've called bindTo()!", new Object[0]);
            return;
        }
        StringBuilder sb = new StringBuilder();
        sb.append("hcp == null -> ");
        sb.append(this.helpCenterProvider == null);
        Logger.e(ViewArticleActivity.LOG_TAG, sb.toString(), new Object[0]);
        this.helpCenterProvider.downvoteArticle(this.articleId, new rs4<ArticleVote>() { // from class: zendesk.support.guide.ArticleVotingView.4
            @Override // defpackage.rs4
            public void onError(cw0 cw0Var) {
                Logger.b(ViewArticleActivity.LOG_TAG, "Failed to downvote article. " + cw0Var, new Object[0]);
                ArticleVotingView articleVotingView = ArticleVotingView.this;
                articleVotingView.announceForAccessibility(articleVotingView.getResources().getString(t13.zs_view_article_voted_failed_accessibility_announce));
                ArticleVotingView articleVotingView2 = ArticleVotingView.this;
                articleVotingView2.updateButtons(VoteState.fromArticleVote(articleVotingView2.articleVote));
                ArticleVotingView.this.setVotingButtonsClickable(true);
            }

            @Override // defpackage.rs4
            public void onSuccess(ArticleVote articleVote) {
                Logger.b(ViewArticleActivity.LOG_TAG, "Successfully downvoted article!", new Object[0]);
                ArticleVotingView.this.articleVote = articleVote;
                ArticleVotingView articleVotingView = ArticleVotingView.this;
                articleVotingView.announceForAccessibility(articleVotingView.getResources().getString(t13.zs_view_article_voted_no_accessibility_announce));
                ArticleVotingView.this.articleVoteStorage.storeArticleVote(ArticleVotingView.this.articleId, articleVote);
                ArticleVotingView.this.setVotingButtonsClickable(true);
            }
        });
    }

    private StateListDrawable getVotingButtonBackground(int i) {
        GradientDrawable buildButtonBackground = buildButtonBackground(getContext(), i);
        GradientDrawable buildButtonBackground2 = buildButtonBackground(getContext(), -1);
        StateListDrawable stateListDrawable = new StateListDrawable();
        stateListDrawable.addState(new int[]{16843518}, buildButtonBackground);
        stateListDrawable.addState(new int[]{16842919}, buildButtonBackground);
        stateListDrawable.addState(new int[0], buildButtonBackground2);
        return stateListDrawable;
    }

    /* JADX INFO: Access modifiers changed from: private */
    public void removeVote() {
        if (this.articleId == null) {
            Logger.k(ViewArticleActivity.LOG_TAG, "Article vote was null, could not remove vote", new Object[0]);
        } else if (this.articleVote.getId() != null) {
            this.helpCenterProvider.deleteVote(this.articleVote.getId(), new rs4<Void>() { // from class: zendesk.support.guide.ArticleVotingView.5
                @Override // defpackage.rs4
                public void onError(cw0 cw0Var) {
                    Logger.b(ViewArticleActivity.LOG_TAG, "Failed to remove vote. " + cw0Var.k() + "\n" + cw0Var.f() + "\n" + cw0Var.g(), new Object[0]);
                    ArticleVotingView articleVotingView = ArticleVotingView.this;
                    articleVotingView.updateButtons(VoteState.fromArticleVote(articleVotingView.articleVote));
                    ArticleVotingView.this.setVotingButtonsClickable(true);
                }

                @Override // defpackage.rs4
                public void onSuccess(Void r3) {
                    Logger.b(ViewArticleActivity.LOG_TAG, "Successfully removed vote!", new Object[0]);
                    ArticleVotingView.this.articleVote = null;
                    ArticleVotingView.this.articleVoteStorage.removeStoredArticleVote(ArticleVotingView.this.articleId);
                    ArticleVotingView.this.setVotingButtonsClickable(true);
                }
            });
        }
    }

    /* JADX INFO: Access modifiers changed from: private */
    public void setVotingButtonsClickable(boolean z) {
        this.upvoteButton.setClickable(z);
        this.downvoteButton.setClickable(z);
    }

    private void setupViews(Context context) {
        LayoutInflater.from(context).inflate(a13.zs_view_article_voting, this);
        this.upvoteButtonFrame = (ViewGroup) findViewById(e03.upvote_button_frame);
        this.upvoteButton = (ImageButton) findViewById(e03.upvote_button);
        this.downvoteButtonFrame = (ViewGroup) findViewById(e03.downvote_button_frame);
        this.downvoteButton = (ImageButton) findViewById(e03.downvote_button);
        int e = ne4.e(hy2.colorPrimary, getContext(), uy2.zs_fallback_text_color);
        themeVotingButton(this.upvoteButton, rz2.zs_ic_thumb_up, e);
        themeVotingButton(this.downvoteButton, rz2.zs_ic_thumb_down, e);
        this.upvoteButton.setOnClickListener(new View.OnClickListener() { // from class: zendesk.support.guide.ArticleVotingView.1
            @Override // android.view.View.OnClickListener
            public void onClick(View view) {
                VoteState voteState;
                ArticleVotingView.this.setVotingButtonsClickable(false);
                if (ArticleVotingView.this.articleVote != null && ArticleVotingView.this.articleVote.getValue() != null && ArticleVotingView.this.articleVote.getValue().equals(1)) {
                    voteState = VoteState.NONE;
                    ArticleVotingView.this.removeVote();
                } else {
                    voteState = VoteState.UPVOTED;
                    ArticleVotingView.this.upvoteArticle();
                }
                ArticleVotingView.this.updateButtons(voteState);
            }
        });
        this.downvoteButton.setOnClickListener(new View.OnClickListener() { // from class: zendesk.support.guide.ArticleVotingView.2
            @Override // android.view.View.OnClickListener
            public void onClick(View view) {
                VoteState voteState;
                ArticleVotingView.this.setVotingButtonsClickable(false);
                if (ArticleVotingView.this.articleVote != null && ArticleVotingView.this.articleVote.getValue() != null && ArticleVotingView.this.articleVote.getValue().equals(-1)) {
                    voteState = VoteState.NONE;
                    ArticleVotingView.this.removeVote();
                } else {
                    voteState = VoteState.DOWNVOTED;
                    ArticleVotingView.this.downvoteArticle();
                }
                ArticleVotingView.this.updateButtons(voteState);
            }
        });
    }

    private void themeVotingButton(ImageButton imageButton, int i, int i2) {
        ei4.w0(imageButton, getVotingButtonBackground(i2));
        Drawable r = a.r(m70.f(getContext(), i));
        a.o(r, colorStateList(-1, i2));
        a.p(r, PorterDuff.Mode.SRC_IN);
        imageButton.setImageDrawable(r);
    }

    /* JADX INFO: Access modifiers changed from: private */
    public void updateButtons(VoteState voteState) {
        if (voteState == VoteState.NONE) {
            this.upvoteButtonFrame.setActivated(false);
            this.downvoteButtonFrame.setActivated(false);
        } else if (voteState == VoteState.UPVOTED) {
            this.upvoteButtonFrame.setActivated(true);
            this.downvoteButtonFrame.setActivated(false);
        } else if (voteState == VoteState.DOWNVOTED) {
            this.upvoteButtonFrame.setActivated(false);
            this.downvoteButtonFrame.setActivated(true);
        }
        updateContentDesc(voteState);
    }

    private void updateContentDesc(VoteState voteState) {
        int i = AnonymousClass6.$SwitchMap$zendesk$support$guide$ArticleVotingView$VoteState[voteState.ordinal()];
        if (i == 1) {
            this.upvoteButton.setContentDescription(getResources().getString(t13.zs_view_article_vote_yes_accessibility));
            this.downvoteButton.setContentDescription(getResources().getString(t13.zs_view_article_vote_no_accessibility));
        } else if (i == 2) {
            this.upvoteButton.setContentDescription(getResources().getString(t13.zs_view_article_vote_no_remove_accessibility));
            this.downvoteButton.setContentDescription(getResources().getString(t13.zs_view_article_vote_no_accessibility));
        } else if (i != 3) {
            Logger.b(ViewArticleActivity.LOG_TAG, "Unhandled voteState case", new Object[0]);
        } else {
            this.upvoteButton.setContentDescription(getResources().getString(t13.zs_view_article_vote_yes_accessibility));
            this.downvoteButton.setContentDescription(getResources().getString(t13.zs_view_article_vote_yes_remove_accessibility));
        }
    }

    /* JADX INFO: Access modifiers changed from: private */
    public void upvoteArticle() {
        Long l = this.articleId;
        if (l == null) {
            Logger.k(ViewArticleActivity.LOG_TAG, "Cannot upvote article, articleId is null. Make sure you've called bindTo()!", new Object[0]);
        } else {
            this.helpCenterProvider.upvoteArticle(l, new rs4<ArticleVote>() { // from class: zendesk.support.guide.ArticleVotingView.3
                @Override // defpackage.rs4
                public void onError(cw0 cw0Var) {
                    Logger.b(ViewArticleActivity.LOG_TAG, "Failed to upvote article. " + cw0Var, new Object[0]);
                    ArticleVotingView articleVotingView = ArticleVotingView.this;
                    articleVotingView.announceForAccessibility(articleVotingView.getResources().getString(t13.zs_view_article_voted_failed_accessibility_announce));
                    ArticleVotingView articleVotingView2 = ArticleVotingView.this;
                    articleVotingView2.updateButtons(VoteState.fromArticleVote(articleVotingView2.articleVote));
                    ArticleVotingView.this.setVotingButtonsClickable(true);
                }

                @Override // defpackage.rs4
                public void onSuccess(ArticleVote articleVote) {
                    Logger.b(ViewArticleActivity.LOG_TAG, "Successfully upvoted article!", new Object[0]);
                    ArticleVotingView.this.articleVote = articleVote;
                    ArticleVotingView articleVotingView = ArticleVotingView.this;
                    articleVotingView.announceForAccessibility(articleVotingView.getResources().getString(t13.zs_view_article_voted_yes_accessibility_announce));
                    ArticleVotingView.this.articleVoteStorage.storeArticleVote(ArticleVotingView.this.articleId, articleVote);
                    ArticleVotingView.this.setVotingButtonsClickable(true);
                }
            });
        }
    }

    public void bindTo(Long l, ArticleVoteStorage articleVoteStorage, HelpCenterProvider helpCenterProvider) {
        this.articleVoteStorage = articleVoteStorage;
        this.helpCenterProvider = helpCenterProvider;
        this.articleId = l;
        if (l != null) {
            ArticleVote storedArticleVote = articleVoteStorage.getStoredArticleVote(l);
            this.articleVote = storedArticleVote;
            updateButtons(VoteState.fromArticleVote(storedArticleVote));
        }
    }

    public ArticleVotingView(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        setupViews(context);
    }

    public ArticleVotingView(Context context, AttributeSet attributeSet, int i) {
        super(context, attributeSet, i);
        setupViews(context);
    }
}
