package zendesk.support.guide;

import zendesk.core.ActionHandler;

/* loaded from: classes3.dex */
public class GuideSdkModule {
    public static ActionHandler viewArticleActionHandler() {
        return new ViewArticleActionHandler();
    }

    public z40 configurationHelper() {
        return new z40();
    }
}
