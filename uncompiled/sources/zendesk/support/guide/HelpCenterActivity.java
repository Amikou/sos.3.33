package zendesk.support.guide;

import android.content.Context;
import android.content.res.Resources;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.SearchView;
import androidx.appcompat.widget.Toolbar;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import com.fasterxml.jackson.core.util.MinimalPrettyPrinter;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.material.snackbar.Snackbar;
import com.zendesk.logger.Logger;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import zendesk.core.ActionDescription;
import zendesk.core.ActionHandler;
import zendesk.core.ActionHandlerRegistry;
import zendesk.core.NetworkInfoProvider;
import zendesk.core.RetryAction;
import zendesk.messaging.Engine;
import zendesk.messaging.MessagingActivity;
import zendesk.support.HelpCenterProvider;
import zendesk.support.HelpCenterSettingsProvider;
import zendesk.support.SearchArticle;
import zendesk.support.guide.HelpCenterConfiguration;

/* loaded from: classes3.dex */
public class HelpCenterActivity extends AppCompatActivity implements HelpCenterMvp$View {
    public static final String LOG_TAG = "HelpCenterActivity";
    public ActionHandlerRegistry actionHandlerRegistry;
    public z40 configurationHelper;
    private FloatingActionButton contactUsButton;
    private MenuItem conversationsMenuItem;
    private List<Engine> engines;
    private Snackbar errorSnackbar;
    private HelpCenterConfiguration helpCenterConfiguration;
    public HelpCenterProvider helpCenterProvider;
    private View loadingView;
    public NetworkInfoProvider networkInfoProvider;
    private HelpCenterMvp$Presenter presenter;
    private MenuItem searchViewMenuItem;
    public HelpCenterSettingsProvider settingsProvider;
    private SnackbarStatus snackbarStatus = SnackbarStatus.NONE;

    /* renamed from: zendesk.support.guide.HelpCenterActivity$5  reason: invalid class name */
    /* loaded from: classes3.dex */
    public static /* synthetic */ class AnonymousClass5 {
        public static final /* synthetic */ int[] $SwitchMap$zendesk$support$guide$HelpCenterMvp$ErrorType;

        static {
            int[] iArr = new int[HelpCenterMvp$ErrorType.values().length];
            $SwitchMap$zendesk$support$guide$HelpCenterMvp$ErrorType = iArr;
            try {
                iArr[HelpCenterMvp$ErrorType.CATEGORY_LOAD.ordinal()] = 1;
            } catch (NoSuchFieldError unused) {
            }
            try {
                $SwitchMap$zendesk$support$guide$HelpCenterMvp$ErrorType[HelpCenterMvp$ErrorType.SECTION_LOAD.ordinal()] = 2;
            } catch (NoSuchFieldError unused2) {
            }
            try {
                $SwitchMap$zendesk$support$guide$HelpCenterMvp$ErrorType[HelpCenterMvp$ErrorType.ARTICLES_LOAD.ordinal()] = 3;
            } catch (NoSuchFieldError unused3) {
            }
        }
    }

    /* loaded from: classes3.dex */
    public enum SnackbarStatus {
        NO_CONNECTION,
        NONE,
        CONTENT_ERROR
    }

    private void addFragment(Fragment fragment) {
        getSupportFragmentManager().n().c(e03.fragment_container, fragment, fragment.getClass().getSimpleName()).j();
    }

    private void addOnBackStackChangedListener() {
        getSupportFragmentManager().i(new FragmentManager.m() { // from class: zendesk.support.guide.HelpCenterActivity.2
            @Override // androidx.fragment.app.FragmentManager.m
            public void onBackStackChanged() {
                if (HelpCenterActivity.this.getCurrentFragment().isHidden()) {
                    HelpCenterActivity.this.getSupportFragmentManager().n().x(HelpCenterActivity.this.getCurrentFragment()).j();
                    if (HelpCenterActivity.this.getCurrentFragment() instanceof HelpCenterFragment) {
                        ((HelpCenterFragment) HelpCenterActivity.this.getCurrentFragment()).setPresenter(HelpCenterActivity.this.presenter);
                    }
                }
            }
        });
    }

    public static HelpCenterConfiguration.Builder builder() {
        return new HelpCenterConfiguration.Builder();
    }

    /* JADX INFO: Access modifiers changed from: private */
    public Fragment getCurrentFragment() {
        return getSupportFragmentManager().j0(e03.fragment_container);
    }

    private HelpSearchFragment getSearchFragment() {
        if (getCurrentFragment() instanceof HelpSearchFragment) {
            Logger.b(LOG_TAG, "showSearchResults: current fragment is a HelpSearchFragment", new Object[0]);
            return (HelpSearchFragment) getCurrentFragment();
        }
        HelpSearchFragment newInstance = HelpSearchFragment.newInstance(this.helpCenterConfiguration, this.helpCenterProvider);
        getSupportFragmentManager().n().s(e03.fragment_container, newInstance).h(null).j();
        return newInstance;
    }

    private ActionBar initToolbar() {
        findViewById(e03.support_compat_shadow).setVisibility(8);
        setSupportActionBar((Toolbar) findViewById(e03.support_toolbar));
        return getSupportActionBar();
    }

    private boolean noFragmentAdded() {
        return getCurrentFragment() == null;
    }

    private void showCreateRequest(Map<String, Object> map) {
        ActionHandler handlerByAction = this.actionHandlerRegistry.handlerByAction("action_contact_option");
        if (handlerByAction != null) {
            ActionDescription actionDescription = handlerByAction.getActionDescription();
            Logger.b(LOG_TAG, "No Deflection ActionHandler Available, opening %s", actionDescription != null ? actionDescription.getLocalizedLabel() : handlerByAction.getClass().getSimpleName());
            handlerByAction.handle(map, this);
        }
    }

    @Override // zendesk.support.guide.HelpCenterMvp$View
    public void announceContentLoaded() {
        this.contactUsButton.announceForAccessibility(getString(t13.zs_help_center_content_loaded_accessibility));
    }

    @Override // zendesk.support.guide.HelpCenterMvp$View
    public void clearSearchResults() {
        if (getCurrentFragment() instanceof HelpSearchFragment) {
            ((HelpSearchFragment) getCurrentFragment()).clearResults();
        }
    }

    @Override // zendesk.support.guide.HelpCenterMvp$View
    public void dismissError() {
        Snackbar snackbar = this.errorSnackbar;
        if (snackbar != null) {
            snackbar.v();
        }
        this.snackbarStatus = SnackbarStatus.NONE;
    }

    @Override // zendesk.support.guide.HelpCenterMvp$View
    public void exitActivity() {
        finish();
    }

    @Override // zendesk.support.guide.HelpCenterMvp$View
    public Context getContext() {
        return getApplicationContext();
    }

    @Override // zendesk.support.guide.HelpCenterMvp$View
    public void hideLoadingState() {
        this.loadingView.setVisibility(8);
    }

    @Override // zendesk.support.guide.HelpCenterMvp$View
    public boolean isShowingHelp() {
        return getCurrentFragment() instanceof HelpCenterFragment;
    }

    @Override // androidx.fragment.app.FragmentActivity, androidx.activity.ComponentActivity, androidx.core.app.ComponentActivity, android.app.Activity
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        Resources.Theme theme = getTheme();
        theme.applyStyle(z13.ZendeskActivityDefaultTheme, true);
        theme.applyStyle(z13.ZendeskSupportActivityThemeDefaultIcon, false);
        setContentView(a13.zs_activity_help_center);
        GuideSdkDependencyProvider guideSdkDependencyProvider = GuideSdkDependencyProvider.INSTANCE;
        if (!guideSdkDependencyProvider.isInitialized()) {
            Logger.e(LOG_TAG, GuideSdkDependencyProvider.NOT_INITIALIZED_LOG, new Object[0]);
            finish();
            return;
        }
        guideSdkDependencyProvider.provideGuideSdkComponent().inject(this);
        HelpCenterConfiguration helpCenterConfiguration = (HelpCenterConfiguration) this.configurationHelper.f(getIntent().getExtras(), HelpCenterConfiguration.class);
        this.helpCenterConfiguration = helpCenterConfiguration;
        if (helpCenterConfiguration == null) {
            Logger.e(LOG_TAG, "No configuration found. Please use HelpCenterActivity.builder()", new Object[0]);
            finish();
            return;
        }
        this.engines = helpCenterConfiguration.getEngines();
        initToolbar().t(true);
        this.loadingView = findViewById(e03.loading_view);
        FloatingActionButton floatingActionButton = (FloatingActionButton) findViewById(e03.contact_us_button);
        this.contactUsButton = floatingActionButton;
        floatingActionButton.setOnClickListener(new View.OnClickListener() { // from class: zendesk.support.guide.HelpCenterActivity.1
            @Override // android.view.View.OnClickListener
            public void onClick(View view) {
                HelpCenterActivity.this.showContactZendesk();
            }
        });
        HelpCenterPresenter helpCenterPresenter = new HelpCenterPresenter(this, new HelpCenterModel(this.helpCenterProvider, this.settingsProvider), this.networkInfoProvider, this.actionHandlerRegistry);
        this.presenter = helpCenterPresenter;
        helpCenterPresenter.init(this.helpCenterConfiguration, this.engines);
        addOnBackStackChangedListener();
    }

    @Override // android.app.Activity
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(f13.zs_fragment_help_menu_conversations, menu);
        this.conversationsMenuItem = menu.findItem(e03.fragment_help_menu_contact);
        MenuItem findItem = menu.findItem(e03.fragment_help_menu_search);
        this.searchViewMenuItem = findItem;
        if (findItem != null) {
            if (!this.networkInfoProvider.isNetworkAvailable()) {
                this.searchViewMenuItem.setEnabled(false);
            }
            SearchView searchView = (SearchView) this.searchViewMenuItem.getActionView();
            searchView.setImeOptions(searchView.getImeOptions() | 268435456);
            searchView.setOnQueryTextListener(new SearchView.l() { // from class: zendesk.support.guide.HelpCenterActivity.3
                @Override // androidx.appcompat.widget.SearchView.l
                public boolean onQueryTextChange(String str) {
                    return false;
                }

                @Override // androidx.appcompat.widget.SearchView.l
                public boolean onQueryTextSubmit(String str) {
                    if (HelpCenterActivity.this.presenter != null) {
                        HelpCenterActivity.this.presenter.onSearchSubmit(str);
                        return true;
                    }
                    return false;
                }
            });
            return true;
        }
        return true;
    }

    @Override // android.app.Activity
    public boolean onOptionsItemSelected(MenuItem menuItem) {
        int itemId = menuItem.getItemId();
        if (itemId == 16908332) {
            onBackPressed();
            return true;
        } else if (itemId == e03.fragment_help_menu_contact) {
            showRequestList();
            return true;
        } else {
            return false;
        }
    }

    @Override // androidx.fragment.app.FragmentActivity, android.app.Activity
    public void onPause() {
        super.onPause();
        HelpCenterMvp$Presenter helpCenterMvp$Presenter = this.presenter;
        if (helpCenterMvp$Presenter != null) {
            helpCenterMvp$Presenter.onPause();
        }
    }

    @Override // android.app.Activity
    public boolean onPrepareOptionsMenu(Menu menu) {
        MenuItem menuItem;
        HelpCenterMvp$Presenter helpCenterMvp$Presenter = this.presenter;
        if (helpCenterMvp$Presenter != null && (menuItem = this.searchViewMenuItem) != null) {
            menuItem.setVisible(helpCenterMvp$Presenter.shouldShowSearchMenuItem());
        }
        if (this.presenter != null && this.conversationsMenuItem != null) {
            boolean z = true;
            boolean z2 = this.actionHandlerRegistry.handlerByAction("action_conversation_list") != null;
            MenuItem menuItem2 = this.conversationsMenuItem;
            if (!this.presenter.shouldShowConversationsMenuItem() || !z2) {
                z = false;
            }
            menuItem2.setVisible(z);
        }
        return super.onPrepareOptionsMenu(menu);
    }

    @Override // androidx.fragment.app.FragmentActivity, android.app.Activity
    public void onResume() {
        super.onResume();
        HelpCenterMvp$Presenter helpCenterMvp$Presenter = this.presenter;
        if (helpCenterMvp$Presenter != null) {
            helpCenterMvp$Presenter.onResume(this);
        }
    }

    @Override // androidx.appcompat.app.AppCompatActivity, androidx.fragment.app.FragmentActivity, android.app.Activity
    public void onStart() {
        Snackbar snackbar;
        super.onStart();
        if (this.snackbarStatus == SnackbarStatus.NONE || (snackbar = this.errorSnackbar) == null) {
            return;
        }
        snackbar.Q();
    }

    @Override // zendesk.support.guide.HelpCenterMvp$View
    public void setSearchEnabled(boolean z) {
        this.searchViewMenuItem.setEnabled(z);
    }

    @Override // zendesk.support.guide.HelpCenterMvp$View
    public void showContactUsButton() {
        this.contactUsButton.setVisibility(0);
    }

    @Override // zendesk.support.guide.HelpCenterMvp$View
    public void showContactZendesk() {
        HashMap hashMap = new HashMap();
        this.configurationHelper.d(hashMap, this.helpCenterConfiguration);
        if (l10.i(this.engines)) {
            MessagingActivity.builder().withEngines(this.engines).show(this, this.helpCenterConfiguration.getConfigurations());
        } else {
            showCreateRequest(hashMap);
        }
    }

    @Override // zendesk.support.guide.HelpCenterMvp$View
    public void showHelp(HelpCenterConfiguration helpCenterConfiguration) {
        if (noFragmentAdded()) {
            HelpCenterFragment newInstance = HelpCenterFragment.newInstance(helpCenterConfiguration);
            newInstance.setPresenter(this.presenter);
            addFragment(newInstance);
        } else if (getCurrentFragment() instanceof HelpCenterFragment) {
            ((HelpCenterFragment) getCurrentFragment()).setPresenter(this.presenter);
        }
        invalidateOptionsMenu();
    }

    @Override // zendesk.support.guide.HelpCenterMvp$View
    public void showLoadArticleErrorWithRetry(HelpCenterMvp$ErrorType helpCenterMvp$ErrorType, final RetryAction retryAction) {
        String string;
        if (helpCenterMvp$ErrorType == null) {
            Logger.k(LOG_TAG, "ErrorType was null, falling back to 'retry' as label", new Object[0]);
            string = getString(t13.zui_retry_button_label);
        } else {
            int i = AnonymousClass5.$SwitchMap$zendesk$support$guide$HelpCenterMvp$ErrorType[helpCenterMvp$ErrorType.ordinal()];
            if (i == 1) {
                string = getString(t13.support_categories_list_fragment_error_message);
            } else if (i == 2) {
                string = getString(t13.support_sections_list_fragment_error_message);
            } else if (i != 3) {
                Logger.k(LOG_TAG, "Unknown or unhandled error type, falling back to error type name as label", new Object[0]);
                string = getString(t13.support_help_search_no_results_label) + MinimalPrettyPrinter.DEFAULT_ROOT_VALUE_SEPARATOR + helpCenterMvp$ErrorType.name();
            } else {
                string = getString(t13.support_articles_list_fragment_error_message);
            }
        }
        if (this.snackbarStatus == SnackbarStatus.NONE) {
            Snackbar b0 = Snackbar.b0(this.contactUsButton, string, -2);
            this.errorSnackbar = b0;
            b0.d0(t13.zui_retry_button_label, new View.OnClickListener() { // from class: zendesk.support.guide.HelpCenterActivity.4
                @Override // android.view.View.OnClickListener
                public void onClick(View view) {
                    HelpCenterActivity.this.errorSnackbar.v();
                    HelpCenterActivity.this.snackbarStatus = SnackbarStatus.NONE;
                    retryAction.onRetry();
                }
            });
            this.errorSnackbar.Q();
            this.snackbarStatus = SnackbarStatus.CONTENT_ERROR;
        }
    }

    @Override // zendesk.support.guide.HelpCenterMvp$View
    public void showLoadingState() {
        Fragment currentFragment = getCurrentFragment();
        if (currentFragment != null && currentFragment.isVisible()) {
            getSupportFragmentManager().n().q(getCurrentFragment()).j();
        }
        this.loadingView.setVisibility(0);
    }

    @Override // zendesk.support.guide.HelpCenterMvp$View
    public void showNoConnectionError() {
        SnackbarStatus snackbarStatus = this.snackbarStatus;
        SnackbarStatus snackbarStatus2 = SnackbarStatus.NO_CONNECTION;
        if (snackbarStatus != snackbarStatus2) {
            Snackbar a0 = Snackbar.a0(this.contactUsButton, t13.zg_general_no_connection_message, -2);
            this.errorSnackbar = a0;
            a0.Q();
            this.snackbarStatus = snackbarStatus2;
        }
    }

    @Override // zendesk.support.guide.HelpCenterMvp$View
    public void showRequestList() {
        ActionHandler handlerByAction = this.actionHandlerRegistry.handlerByAction("action_conversation_list");
        if (handlerByAction != null) {
            HashMap hashMap = new HashMap();
            this.configurationHelper.d(hashMap, this.helpCenterConfiguration);
            handlerByAction.handle(hashMap, this);
        }
    }

    @Override // zendesk.support.guide.HelpCenterMvp$View
    public void showSearchResults(List<SearchArticle> list, String str) {
        getSearchFragment().updateResults(list, str);
    }
}
