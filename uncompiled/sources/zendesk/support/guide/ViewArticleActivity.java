package zendesk.support.guide;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebChromeClient;
import android.webkit.WebResourceResponse;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.coordinatorlayout.widget.CoordinatorLayout;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.material.snackbar.Snackbar;
import com.zendesk.logger.Logger;
import java.io.InputStream;
import java.nio.charset.Charset;
import java.text.DateFormat;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import okhttp3.MediaType;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;
import zendesk.core.ActionDescription;
import zendesk.core.ActionHandler;
import zendesk.core.ActionHandlerRegistry;
import zendesk.core.AnonymousIdentity;
import zendesk.core.ApplicationConfiguration;
import zendesk.core.Identity;
import zendesk.core.NetworkAware;
import zendesk.core.NetworkInfoProvider;
import zendesk.core.UrlHelper;
import zendesk.core.Zendesk;
import zendesk.messaging.Engine;
import zendesk.messaging.MessagingActivity;
import zendesk.support.Article;
import zendesk.support.ArticleVoteStorage;
import zendesk.support.AttachmentType;
import zendesk.support.HelpCenterAttachment;
import zendesk.support.HelpCenterProvider;
import zendesk.support.HelpCenterSettings;
import zendesk.support.HelpCenterSettingsProvider;
import zendesk.support.guide.ArticleConfiguration;

/* loaded from: classes3.dex */
public class ViewArticleActivity extends AppCompatActivity implements AdapterView.OnItemClickListener {
    private static final String ARTICLE_DETAIL_FORMAT_STRING = "%s %s <span dir=\"auto\">%s</span>";
    private static final String CSS_FILE = "file:///android_asset/help_center_article_style.css";
    private static final long FETCH_ATTACHMENTS_DELAY_MILLIS = 250;
    public static final String LOG_TAG = "ViewArticleActivity";
    private static final Integer NETWORK_AWARE_ID = 57564;
    private static final String TYPE_TEXT_HTML = "text/html";
    private static final String UTF_8_ENCODING_TYPE = "UTF-8";
    public ActionHandlerRegistry actionHandlerRegistry;
    private ArticleAttachmentAdapter adapter;
    public ApplicationConfiguration applicationConfiguration;
    private ArticleViewModel article;
    private WebView articleContentWebView;
    private Long articleId;
    public ArticleVoteStorage articleVoteStorage;
    private ArticleVotingView articleVotingView;
    private ListView attachmentListView;
    private zb3<List<HelpCenterAttachment>> attachmentRequestCallback;
    private ArticleConfiguration config;
    public z40 configurationHelper;
    private CoordinatorLayout coordinatorLayout;
    private List<Engine> engines;
    public HelpCenterProvider helpCenterProvider;
    public NetworkInfoProvider networkInfoProvider;
    public OkHttpClient okHttpClient;
    private ProgressBar progressView;
    public HelpCenterSettingsProvider settingsProvider;
    private Snackbar snackbar;
    private final AggregatedCallback<HelpCenterSettings> settingsAggregatedCallback = new AggregatedCallback<>();
    private final Handler handler = new Handler();
    private final NetworkAware networkConnectionCallbacks = new NetworkAware() { // from class: zendesk.support.guide.ViewArticleActivity.6
        public boolean connected = true;

        @Override // zendesk.core.NetworkAware
        public void onNetworkAvailable() {
            if (NetworkUtils.isConnectedOrConnecting(ViewArticleActivity.this)) {
                ViewArticleActivity.this.dimissSnackBar();
                this.connected = true;
                if (ViewArticleActivity.this.articleId == null || ViewArticleActivity.this.article != null) {
                    if (ViewArticleActivity.this.article != null) {
                        ViewArticleActivity viewArticleActivity = ViewArticleActivity.this;
                        viewArticleActivity.fetchAttachmentsForArticle(viewArticleActivity.article.getId());
                        return;
                    }
                    return;
                }
                ViewArticleActivity viewArticleActivity2 = ViewArticleActivity.this;
                viewArticleActivity2.fetchArticle(viewArticleActivity2.articleId.longValue());
            }
        }

        @Override // zendesk.core.NetworkAware
        @SuppressLint({"MissingPermission"})
        public void onNetworkUnavailable() {
            if (NetworkUtils.isConnectedOrConnecting(ViewArticleActivity.this) || !this.connected) {
                return;
            }
            this.connected = false;
            ViewArticleActivity.this.dimissSnackBar();
            ViewArticleActivity viewArticleActivity = ViewArticleActivity.this;
            viewArticleActivity.snackbar = Snackbar.a0(viewArticleActivity.coordinatorLayout, t13.zg_general_no_connection_message, -2);
            ViewArticleActivity.this.snackbar.Q();
        }
    };

    /* renamed from: zendesk.support.guide.ViewArticleActivity$8  reason: invalid class name */
    /* loaded from: classes3.dex */
    public static /* synthetic */ class AnonymousClass8 {
        public static final /* synthetic */ int[] $SwitchMap$zendesk$support$guide$ViewArticleActivity$LoadingState;

        static {
            int[] iArr = new int[LoadingState.values().length];
            $SwitchMap$zendesk$support$guide$ViewArticleActivity$LoadingState = iArr;
            try {
                iArr[LoadingState.LOADING.ordinal()] = 1;
            } catch (NoSuchFieldError unused) {
            }
            try {
                $SwitchMap$zendesk$support$guide$ViewArticleActivity$LoadingState[LoadingState.DISPLAYING.ordinal()] = 2;
            } catch (NoSuchFieldError unused2) {
            }
            try {
                $SwitchMap$zendesk$support$guide$ViewArticleActivity$LoadingState[LoadingState.ERRORED.ordinal()] = 3;
            } catch (NoSuchFieldError unused3) {
            }
            try {
                $SwitchMap$zendesk$support$guide$ViewArticleActivity$LoadingState[LoadingState.ERRORED_ATTACHMENT.ordinal()] = 4;
            } catch (NoSuchFieldError unused4) {
            }
        }
    }

    /* loaded from: classes3.dex */
    public static class ArticleAttachmentAdapter extends ArrayAdapter<HelpCenterAttachment> {
        public ArticleAttachmentAdapter(Context context) {
            super(context, a13.zs_row_article_attachment);
        }

        @Override // android.widget.ArrayAdapter, android.widget.Adapter
        public View getView(int i, View view, ViewGroup viewGroup) {
            ArticleAttachmentRow articleAttachmentRow;
            if (view instanceof ArticleAttachmentRow) {
                articleAttachmentRow = (ArticleAttachmentRow) view;
            } else {
                articleAttachmentRow = new ArticleAttachmentRow(getContext());
            }
            articleAttachmentRow.bind(getItem(i));
            return articleAttachmentRow;
        }
    }

    /* loaded from: classes3.dex */
    public static class ArticleAttachmentRow extends RelativeLayout {
        private final TextView fileName;
        private final TextView fileSize;

        public ArticleAttachmentRow(Context context) {
            super(context);
            RelativeLayout.inflate(context, a13.zs_row_article_attachment, this);
            this.fileName = (TextView) findViewById(e03.article_attachment_row_filename_text);
            this.fileSize = (TextView) findViewById(e03.article_attachment_row_filesize_text);
        }

        public void bind(HelpCenterAttachment helpCenterAttachment) {
            this.fileName.setText(helpCenterAttachment.getFileName());
            this.fileSize.setText(a41.b(helpCenterAttachment.getSize()));
        }
    }

    /* loaded from: classes3.dex */
    public class AttachmentRequestCallback extends rs4<List<HelpCenterAttachment>> {
        public AttachmentRequestCallback() {
        }

        @Override // defpackage.rs4
        public void onError(cw0 cw0Var) {
            ViewArticleActivity.this.adapter.clear();
            ViewArticleActivity.this.setLoadingState(LoadingState.ERRORED_ATTACHMENT);
            Logger.c(ViewArticleActivity.LOG_TAG, cw0Var);
        }

        @Override // defpackage.rs4
        public void onSuccess(List<HelpCenterAttachment> list) {
            ViewArticleActivity.this.adapter.clear();
            ViewArticleActivity.this.adapter.addAll(list);
            ViewArticleActivity.setListViewHeightBasedOnChildren(ViewArticleActivity.this.attachmentListView);
            ViewArticleActivity.this.setLoadingState(LoadingState.DISPLAYING);
        }
    }

    /* loaded from: classes3.dex */
    public enum LoadingState {
        LOADING,
        DISPLAYING,
        ERRORED,
        ERRORED_ATTACHMENT
    }

    /* JADX INFO: Access modifiers changed from: private */
    public void applyVoteButtonSettings() {
        loadSettings(new rs4<HelpCenterSettings>() { // from class: zendesk.support.guide.ViewArticleActivity.7
            @Override // defpackage.rs4
            public void onError(cw0 cw0Var) {
                ViewArticleActivity.this.articleVotingView.setVisibility(8);
            }

            @Override // defpackage.rs4
            public void onSuccess(HelpCenterSettings helpCenterSettings) {
                if (helpCenterSettings.isArticleVotingEnabled()) {
                    ViewArticleActivity.this.articleVotingView.setVisibility(0);
                } else {
                    ViewArticleActivity.this.articleVotingView.setVisibility(8);
                }
            }
        });
    }

    public static ArticleConfiguration.Builder builder(Article article) {
        return new ArticleConfiguration.Builder(article);
    }

    /* JADX INFO: Access modifiers changed from: private */
    public void dimissSnackBar() {
        Snackbar snackbar = this.snackbar;
        if (snackbar != null) {
            snackbar.v();
            this.snackbar = null;
        }
    }

    /* JADX INFO: Access modifiers changed from: private */
    public void fetchArticle(long j) {
        setLoadingState(LoadingState.LOADING);
        this.helpCenterProvider.getArticle(Long.valueOf(j), new rs4<Article>() { // from class: zendesk.support.guide.ViewArticleActivity.3
            @Override // defpackage.rs4
            public void onError(cw0 cw0Var) {
                ViewArticleActivity.this.setLoadingState(LoadingState.ERRORED);
            }

            @Override // defpackage.rs4
            public void onSuccess(Article article) {
                ViewArticleActivity.this.article = new ArticleViewModel(article);
                ViewArticleActivity.this.loadArticleBody();
            }
        });
    }

    /* JADX INFO: Access modifiers changed from: private */
    public void fetchAttachmentsForArticle(long j) {
        setLoadingState(LoadingState.LOADING);
        this.helpCenterProvider.getAttachments(Long.valueOf(j), AttachmentType.BLOCK, this.attachmentRequestCallback);
    }

    private ActionBar initToolbar() {
        findViewById(e03.view_article_compat_shadow).setVisibility(8);
        setSupportActionBar((Toolbar) findViewById(e03.view_article_toolbar));
        return getSupportActionBar();
    }

    /* JADX INFO: Access modifiers changed from: private */
    @SuppressLint({"RestrictedApi"})
    public void loadArticleBody() {
        setTitle(getString(t13.zs_view_article_loaded_accessibility, new Object[]{this.article.getTitle()}));
        setLoadingState(LoadingState.DISPLAYING);
        ActionBar supportActionBar = getSupportActionBar();
        if (supportActionBar != null) {
            supportActionBar.x(ne4.a(this.article.getTitle()));
        }
        String authorName = this.article.getAuthorName();
        String format = this.article.getCreatedAt() != null ? DateFormat.getDateInstance(1, y40.a(getResources().getConfiguration()).c(0)).format(this.article.getCreatedAt()) : null;
        this.articleContentWebView.loadDataWithBaseURL(this.applicationConfiguration.getZendeskUrl(), getString(t13.view_article_html_body, new Object[]{CSS_FILE, this.article.getTitle(), this.article.getBody(), (format == null || authorName == null) ? "" : String.format(Locale.US, ARTICLE_DETAIL_FORMAT_STRING, authorName, getString(t13.view_article_seperator), format)}), TYPE_TEXT_HTML, UTF_8_ENCODING_TYPE, null);
        this.handler.postDelayed(new Runnable() { // from class: zendesk.support.guide.ViewArticleActivity.4
            @Override // java.lang.Runnable
            public void run() {
                ViewArticleActivity viewArticleActivity = ViewArticleActivity.this;
                viewArticleActivity.fetchAttachmentsForArticle(viewArticleActivity.article.getId());
                ViewArticleActivity.this.applyVoteButtonSettings();
            }
        }, FETCH_ATTACHMENTS_DELAY_MILLIS);
    }

    private void loadSettings(rs4<HelpCenterSettings> rs4Var) {
        if (this.settingsAggregatedCallback.add(rs4Var)) {
            this.settingsProvider.getSettings(this.settingsAggregatedCallback);
        }
    }

    /* JADX INFO: Access modifiers changed from: private */
    public static void setListViewHeightBasedOnChildren(ListView listView) {
        ListAdapter adapter = listView.getAdapter();
        if (adapter == null) {
            return;
        }
        int makeMeasureSpec = View.MeasureSpec.makeMeasureSpec(listView.getWidth(), 0);
        View view = null;
        int i = 0;
        for (int i2 = 0; i2 < adapter.getCount(); i2++) {
            view = adapter.getView(i2, view, listView);
            if (i2 == 0) {
                view.setLayoutParams(new ViewGroup.LayoutParams(makeMeasureSpec, -2));
            }
            view.measure(makeMeasureSpec, 0);
            i += view.getMeasuredHeight();
        }
        ViewGroup.LayoutParams layoutParams = listView.getLayoutParams();
        layoutParams.height = i + (listView.getDividerHeight() * (adapter.getCount() - 1));
        listView.setLayoutParams(layoutParams);
        listView.requestLayout();
    }

    private void setupRequestInterceptor() {
        WebView webView = this.articleContentWebView;
        if (webView == null) {
            Logger.k(LOG_TAG, "The webview is null. Make sure you initialise it before trying to add the interceptor", new Object[0]);
        } else {
            webView.setWebViewClient(new WebViewClient() { // from class: zendesk.support.guide.ViewArticleActivity.2
                /* JADX WARN: Multi-variable type inference failed */
                /* JADX WARN: Type inference failed for: r0v10, types: [java.io.InputStream] */
                /* JADX WARN: Type inference failed for: r0v14 */
                @Override // android.webkit.WebViewClient
                public WebResourceResponse shouldInterceptRequest(WebView webView2, String str) {
                    InputStream inputStream;
                    String str2;
                    InputStream inputStream2;
                    String str3;
                    String zendeskUrl = ViewArticleActivity.this.applicationConfiguration.getZendeskUrl();
                    if (!ru3.d(zendeskUrl) && str.startsWith(zendeskUrl)) {
                        Identity identity = Zendesk.INSTANCE.getIdentity();
                        if (UrlHelper.isGuideRequest(str) && (identity instanceof AnonymousIdentity)) {
                            Logger.k(ViewArticleActivity.LOG_TAG, "Will not intercept request because it is anonymous guide request", new Object[0]);
                            return super.shouldInterceptRequest(webView2, str);
                        }
                        try {
                            Response execute = ViewArticleActivity.this.okHttpClient.newCall(new Request.Builder().url(str).build()).execute();
                            if (execute == null || !execute.isSuccessful() || execute.body() == null) {
                                str3 = null;
                                str2 = null;
                            } else {
                                inputStream = execute.body().byteStream();
                                try {
                                    MediaType contentType = execute.body().contentType();
                                    if (contentType != null) {
                                        str2 = (ru3.b(contentType.type()) && ru3.b(contentType.subtype())) ? String.format(Locale.US, "%s/%s", contentType.type(), contentType.subtype()) : null;
                                        try {
                                            Charset charset = contentType.charset();
                                            str3 = charset != null ? charset.name() : null;
                                        } catch (Exception e) {
                                            e = e;
                                            Logger.d(ViewArticleActivity.LOG_TAG, "Exception encountered when trying to intercept request", e, new Object[0]);
                                            inputStream2 = inputStream;
                                            return new WebResourceResponse(str2, r9, inputStream2);
                                        }
                                    } else {
                                        str3 = null;
                                        str2 = null;
                                    }
                                    r9 = inputStream;
                                } catch (Exception e2) {
                                    e = e2;
                                    str2 = null;
                                }
                            }
                            inputStream2 = r9;
                            r9 = str3;
                        } catch (Exception e3) {
                            e = e3;
                            inputStream = null;
                            str2 = null;
                        }
                        return new WebResourceResponse(str2, r9, inputStream2);
                    }
                    Logger.k(ViewArticleActivity.LOG_TAG, "Will not intercept request because the url is not hosted by Zendesk" + str, new Object[0]);
                    return super.shouldInterceptRequest(webView2, str);
                }

                @Override // android.webkit.WebViewClient
                public boolean shouldOverrideUrlLoading(WebView webView2, String str) {
                    ActionHandler handlerByAction = ViewArticleActivity.this.actionHandlerRegistry.handlerByAction(str);
                    if (handlerByAction != null) {
                        HashMap hashMap = new HashMap();
                        hashMap.put(ViewArticleActionHandler.HELP_CENTER_VIEW_ARTICLE, str);
                        ViewArticleActivity viewArticleActivity = ViewArticleActivity.this;
                        viewArticleActivity.configurationHelper.d(hashMap, viewArticleActivity.config);
                        handlerByAction.handle(hashMap, ViewArticleActivity.this);
                        return true;
                    }
                    Intent intent = new Intent("android.intent.action.VIEW", Uri.parse(str));
                    if (intent.resolveActivity(webView2.getContext().getPackageManager()) != null) {
                        webView2.getContext().startActivity(intent);
                        return true;
                    }
                    Logger.b(ViewArticleActivity.LOG_TAG, "No browser available to open url: " + str, new Object[0]);
                    return false;
                }
            });
        }
    }

    private boolean shouldShowContactUsButton() {
        return this.config.isContactUsButtonVisible() && ((this.actionHandlerRegistry.handlerByAction("action_contact_option") != null) || l10.i(this.engines));
    }

    private void showCreateRequest(Map<String, Object> map) {
        ActionHandler handlerByAction = this.actionHandlerRegistry.handlerByAction("action_contact_option");
        if (handlerByAction != null) {
            ActionDescription actionDescription = handlerByAction.getActionDescription();
            Logger.b(LOG_TAG, "No Deflection ActionHandler Available, opening %s", actionDescription != null ? actionDescription.getLocalizedLabel() : handlerByAction.getClass().getSimpleName());
            handlerByAction.handle(map, this);
        }
    }

    @Override // androidx.fragment.app.FragmentActivity, androidx.activity.ComponentActivity, androidx.core.app.ComponentActivity, android.app.Activity
    @SuppressLint({"SetJavaScriptEnabled", "RestrictedApi"})
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        getTheme().applyStyle(z13.ZendeskActivityDefaultTheme, true);
        setContentView(a13.zs_activity_view_article);
        GuideSdkDependencyProvider guideSdkDependencyProvider = GuideSdkDependencyProvider.INSTANCE;
        if (!guideSdkDependencyProvider.isInitialized()) {
            Logger.e(LOG_TAG, GuideSdkDependencyProvider.NOT_INITIALIZED_LOG, new Object[0]);
            finish();
            return;
        }
        guideSdkDependencyProvider.provideGuideSdkComponent().inject(this);
        ActionBar initToolbar = initToolbar();
        ArticleConfiguration articleConfiguration = (ArticleConfiguration) this.configurationHelper.f(getIntent().getExtras(), ArticleConfiguration.class);
        this.config = articleConfiguration;
        if (articleConfiguration != null && articleConfiguration.getConfigurationState() != -1) {
            this.engines = this.config.getEngines();
            this.attachmentListView = (ListView) findViewById(e03.view_article_attachment_list);
            ArticleAttachmentAdapter articleAttachmentAdapter = new ArticleAttachmentAdapter(this);
            this.adapter = articleAttachmentAdapter;
            this.attachmentListView.setAdapter((ListAdapter) articleAttachmentAdapter);
            this.attachmentListView.setOnItemClickListener(this);
            if (initToolbar != null) {
                initToolbar.t(true);
            }
            WebView webView = (WebView) findViewById(e03.view_article_content_webview);
            this.articleContentWebView = webView;
            webView.setWebChromeClient(new WebChromeClient());
            this.articleContentWebView.getSettings().setJavaScriptEnabled(true);
            setupRequestInterceptor();
            this.articleContentWebView.getSettings().setMixedContentMode(0);
            this.progressView = (ProgressBar) findViewById(e03.view_article_progress);
            this.coordinatorLayout = (CoordinatorLayout) findViewById(e03.view_article_attachment_coordinator);
            if (this.config.getConfigurationState() == 2) {
                ArticleViewModel article = this.config.getArticle();
                this.article = article;
                if (article != null) {
                    this.articleId = Long.valueOf(article.getId());
                }
                loadArticleBody();
            } else {
                fetchArticle(this.config.getArticleId());
                this.articleId = Long.valueOf(this.config.getArticleId());
            }
            if (shouldShowContactUsButton()) {
                FloatingActionButton floatingActionButton = (FloatingActionButton) findViewById(e03.contact_us_button);
                floatingActionButton.setOnClickListener(new View.OnClickListener() { // from class: zendesk.support.guide.ViewArticleActivity.1
                    @Override // android.view.View.OnClickListener
                    public void onClick(View view) {
                        ViewArticleActivity.this.showContactZendesk();
                    }
                });
                floatingActionButton.setVisibility(0);
            }
            ArticleVotingView articleVotingView = (ArticleVotingView) findViewById(e03.article_voting_container);
            this.articleVotingView = articleVotingView;
            articleVotingView.bindTo(this.articleId, this.articleVoteStorage, this.helpCenterProvider);
            this.articleVotingView.setVisibility(8);
            applyVoteButtonSettings();
            return;
        }
        Logger.e(LOG_TAG, "No configuration found. Please use ViewArticleActivity.builder()", new Object[0]);
        finish();
    }

    @Override // androidx.appcompat.app.AppCompatActivity, androidx.fragment.app.FragmentActivity, android.app.Activity
    public void onDestroy() {
        super.onDestroy();
        this.settingsAggregatedCallback.cancel();
        WebView webView = this.articleContentWebView;
        if (webView != null) {
            webView.destroy();
        }
    }

    @Override // android.widget.AdapterView.OnItemClickListener
    public void onItemClick(AdapterView<?> adapterView, View view, int i, long j) {
        Object itemAtPosition = adapterView.getItemAtPosition(i);
        if (itemAtPosition instanceof HelpCenterAttachment) {
            HelpCenterAttachment helpCenterAttachment = (HelpCenterAttachment) itemAtPosition;
            if (helpCenterAttachment.getContentUrl() != null) {
                Uri parse = Uri.parse(helpCenterAttachment.getContentUrl());
                Intent intent = new Intent();
                intent.setAction("android.intent.action.VIEW");
                intent.setData(parse);
                startActivity(intent);
                return;
            }
            Logger.k(LOG_TAG, "Unable to launch viewer, unable to parse URI for attachment", new Object[0]);
        }
    }

    @Override // android.app.Activity
    public boolean onOptionsItemSelected(MenuItem menuItem) {
        if (menuItem.getItemId() == 16908332) {
            onBackPressed();
            return true;
        }
        return super.onOptionsItemSelected(menuItem);
    }

    @Override // androidx.appcompat.app.AppCompatActivity, androidx.fragment.app.FragmentActivity, android.app.Activity
    public void onStart() {
        super.onStart();
        this.attachmentRequestCallback = zb3.a(new AttachmentRequestCallback());
        this.networkInfoProvider.addNetworkAwareListener(NETWORK_AWARE_ID, this.networkConnectionCallbacks);
        this.networkInfoProvider.register();
    }

    @Override // androidx.appcompat.app.AppCompatActivity, androidx.fragment.app.FragmentActivity, android.app.Activity
    public void onStop() {
        super.onStop();
        zb3<List<HelpCenterAttachment>> zb3Var = this.attachmentRequestCallback;
        if (zb3Var != null) {
            zb3Var.cancel();
            this.attachmentRequestCallback = null;
        }
        this.networkInfoProvider.removeNetworkAwareListener(NETWORK_AWARE_ID);
        this.networkInfoProvider.unregister();
    }

    public void setLoadingState(LoadingState loadingState) {
        if (loadingState == null) {
            Logger.k(LOG_TAG, "LoadingState was null, nothing to do", new Object[0]);
            return;
        }
        int i = AnonymousClass8.$SwitchMap$zendesk$support$guide$ViewArticleActivity$LoadingState[loadingState.ordinal()];
        if (i == 1) {
            ne4.d(this.progressView, 0);
            ne4.d(this.attachmentListView, 8);
        } else if (i == 2) {
            ne4.d(this.progressView, 8);
            ne4.d(this.attachmentListView, 0);
        } else if (i == 3) {
            showLoadingErrorState(t13.zs_view_article_error);
        } else if (i != 4) {
        } else {
            showLoadingErrorState(t13.view_article_attachments_error);
        }
    }

    public void showContactZendesk() {
        HashMap hashMap = new HashMap();
        this.configurationHelper.d(hashMap, this.config);
        if (l10.i(this.engines)) {
            MessagingActivity.builder().withEngines(this.engines).show(this, this.config.getConfigurations());
        } else {
            showCreateRequest(hashMap);
        }
    }

    public void showLoadingErrorState(int i) {
        ne4.d(this.progressView, 8);
        ne4.d(this.attachmentListView, 8);
        dimissSnackBar();
        Snackbar d0 = Snackbar.a0(this.coordinatorLayout, i, -2).d0(t13.zui_retry_button_label, new View.OnClickListener() { // from class: zendesk.support.guide.ViewArticleActivity.5
            @Override // android.view.View.OnClickListener
            public void onClick(View view) {
                if (ViewArticleActivity.this.articleId == null || ViewArticleActivity.this.article != null) {
                    if (ViewArticleActivity.this.article != null) {
                        ViewArticleActivity viewArticleActivity = ViewArticleActivity.this;
                        viewArticleActivity.fetchAttachmentsForArticle(viewArticleActivity.article.getId());
                    }
                } else {
                    ViewArticleActivity viewArticleActivity2 = ViewArticleActivity.this;
                    viewArticleActivity2.fetchArticle(viewArticleActivity2.articleId.longValue());
                }
                ViewArticleActivity.this.dimissSnackBar();
            }
        });
        this.snackbar = d0;
        d0.Q();
    }

    public static ArticleConfiguration.Builder builder(long j) {
        return new ArticleConfiguration.Builder(j);
    }

    public static ArticleConfiguration.Builder builder() {
        return new ArticleConfiguration.Builder();
    }
}
