package zendesk.support.guide;

import com.zendesk.logger.Logger;
import java.util.ArrayList;
import java.util.List;
import zendesk.core.NetworkInfoProvider;
import zendesk.core.RetryAction;
import zendesk.support.ArticleItem;
import zendesk.support.CategoryItem;
import zendesk.support.HelpItem;
import zendesk.support.SectionItem;
import zendesk.support.SeeAllArticlesItem;

/* loaded from: classes3.dex */
public class HelpAdapterPresenter implements HelpMvp$Presenter {
    private static final Integer RETRY_ACTION_ID = 5;
    private HelpCenterMvp$Presenter contentPresenter;
    private boolean hasError;
    private HelpCenterConfiguration helpCenterUiConfig;
    private HelpMvp$Model model;
    private NetworkInfoProvider networkInfoProvider;
    private boolean noResults;
    private RetryAction retryAction;
    private HelpMvp$View view;
    private List<HelpItem> helpItems = new ArrayList();
    private List<HelpItem> filteredItems = new ArrayList();
    private rs4<List<HelpItem>> callback = new rs4<List<HelpItem>>() { // from class: zendesk.support.guide.HelpAdapterPresenter.2
        @Override // defpackage.rs4
        public void onError(cw0 cw0Var) {
            HelpCenterMvp$ErrorType helpCenterMvp$ErrorType;
            if (!l10.i(HelpAdapterPresenter.this.helpCenterUiConfig.getCategoryIds())) {
                if (l10.i(HelpAdapterPresenter.this.helpCenterUiConfig.getSectionIds())) {
                    helpCenterMvp$ErrorType = HelpCenterMvp$ErrorType.SECTION_LOAD;
                } else {
                    helpCenterMvp$ErrorType = HelpCenterMvp$ErrorType.ARTICLES_LOAD;
                }
            } else {
                helpCenterMvp$ErrorType = HelpCenterMvp$ErrorType.CATEGORY_LOAD;
            }
            HelpAdapterPresenter.this.contentPresenter.onErrorWithRetry(helpCenterMvp$ErrorType, new RetryAction() { // from class: zendesk.support.guide.HelpAdapterPresenter.2.1
                @Override // zendesk.core.RetryAction
                public void onRetry() {
                    HelpAdapterPresenter.this.hasError = false;
                    HelpAdapterPresenter.this.view.showItems(HelpAdapterPresenter.this.filteredItems);
                    HelpAdapterPresenter.this.requestHelpContent();
                }
            });
            HelpAdapterPresenter.this.hasError = true;
            HelpAdapterPresenter.this.view.showItems(HelpAdapterPresenter.this.filteredItems);
        }

        @Override // defpackage.rs4
        public void onSuccess(List<HelpItem> list) {
            HelpAdapterPresenter.this.hasError = false;
            HelpAdapterPresenter.this.helpItems = l10.c(list);
            if (HelpAdapterPresenter.this.helpCenterUiConfig.isCollapseCategories()) {
                HelpAdapterPresenter helpAdapterPresenter = HelpAdapterPresenter.this;
                helpAdapterPresenter.filteredItems = helpAdapterPresenter.getCollapsedCategories(helpAdapterPresenter.helpItems);
            } else {
                HelpAdapterPresenter helpAdapterPresenter2 = HelpAdapterPresenter.this;
                helpAdapterPresenter2.filteredItems = l10.c(helpAdapterPresenter2.helpItems);
            }
            HelpAdapterPresenter helpAdapterPresenter3 = HelpAdapterPresenter.this;
            helpAdapterPresenter3.noResults = l10.g(helpAdapterPresenter3.filteredItems);
            HelpAdapterPresenter.this.view.showItems(HelpAdapterPresenter.this.filteredItems);
            HelpAdapterPresenter.this.contentPresenter.onLoad();
        }
    };

    public HelpAdapterPresenter(HelpMvp$View helpMvp$View, HelpMvp$Model helpMvp$Model, NetworkInfoProvider networkInfoProvider, HelpCenterConfiguration helpCenterConfiguration) {
        this.view = helpMvp$View;
        this.model = helpMvp$Model;
        this.networkInfoProvider = networkInfoProvider;
        this.helpCenterUiConfig = helpCenterConfiguration;
    }

    private void addItem(int i, HelpItem helpItem) {
        this.filteredItems.add(i, helpItem);
        this.view.addItem(i, helpItem);
    }

    private void collapseItem(int i) {
        if (i >= getItemCount() - 1) {
            return;
        }
        int i2 = i + 1;
        while (i2 < this.filteredItems.size() && 1 != this.filteredItems.get(i2).getViewType()) {
            removeItem(i2);
        }
    }

    private void expandItem(CategoryItem categoryItem, int i) {
        int i2 = i + 1;
        for (SectionItem sectionItem : categoryItem.getSections()) {
            addItem(i2, sectionItem);
            i2++;
            try {
                for (HelpItem helpItem : sectionItem.getChildren()) {
                    addItem(i2, helpItem);
                    i2++;
                }
            } catch (ClassCastException e) {
                Logger.d(HelpCenterActivity.LOG_TAG, "Error expanding item", e, new Object[0]);
            }
        }
    }

    /* JADX INFO: Access modifiers changed from: private */
    public List<HelpItem> getCollapsedCategories(List<HelpItem> list) {
        ArrayList arrayList = new ArrayList();
        if (list != null && list.size() != 0) {
            int size = list.size();
            for (int i = 0; i < size; i++) {
                if (1 == list.get(i).getViewType()) {
                    arrayList.add(list.get(i));
                    ((CategoryItem) list.get(i)).setExpanded(false);
                }
            }
        }
        return arrayList;
    }

    private int getPaddingItemCount() {
        return this.helpCenterUiConfig.isContactUsButtonVisible() ? 1 : 0;
    }

    /* JADX INFO: Access modifiers changed from: private */
    public void loadMoreArticles(final SeeAllArticlesItem seeAllArticlesItem) {
        final SectionItem section = seeAllArticlesItem.getSection();
        final RetryAction retryAction = new RetryAction() { // from class: zendesk.support.guide.HelpAdapterPresenter.3
            @Override // zendesk.core.RetryAction
            public void onRetry() {
                HelpAdapterPresenter.this.loadMoreArticles(seeAllArticlesItem);
            }
        };
        if (this.networkInfoProvider.isNetworkAvailable()) {
            this.model.getArticlesForSection(section, this.helpCenterUiConfig.getLabelNames(), new rs4<List<ArticleItem>>() { // from class: zendesk.support.guide.HelpAdapterPresenter.4
                @Override // defpackage.rs4
                public void onError(cw0 cw0Var) {
                    HelpAdapterPresenter.this.helpItems.remove(seeAllArticlesItem);
                    Logger.e(HelpCenterActivity.LOG_TAG, "Failed to load more articles", cw0Var);
                    HelpAdapterPresenter.this.contentPresenter.onErrorWithRetry(HelpCenterMvp$ErrorType.ARTICLES_LOAD, retryAction);
                }

                @Override // defpackage.rs4
                public void onSuccess(List<ArticleItem> list) {
                    int indexOf = HelpAdapterPresenter.this.helpItems.indexOf(seeAllArticlesItem);
                    int indexOf2 = HelpAdapterPresenter.this.filteredItems.indexOf(seeAllArticlesItem);
                    for (ArticleItem articleItem : list) {
                        if (!HelpAdapterPresenter.this.helpItems.contains(articleItem)) {
                            int i = indexOf + 1;
                            HelpAdapterPresenter.this.helpItems.add(indexOf, articleItem);
                            section.addArticle(articleItem);
                            if (indexOf2 != -1) {
                                HelpAdapterPresenter.this.filteredItems.add(indexOf2, articleItem);
                                HelpAdapterPresenter.this.view.addItem(indexOf2, articleItem);
                                indexOf2++;
                            }
                            indexOf = i;
                        }
                    }
                    HelpAdapterPresenter.this.helpItems.remove(seeAllArticlesItem);
                    int indexOf3 = HelpAdapterPresenter.this.filteredItems.indexOf(seeAllArticlesItem);
                    HelpAdapterPresenter.this.filteredItems.remove(seeAllArticlesItem);
                    HelpAdapterPresenter.this.view.removeItem(indexOf3);
                }
            });
            return;
        }
        this.retryAction = retryAction;
        this.networkInfoProvider.addRetryAction(RETRY_ACTION_ID, retryAction);
    }

    private void removeItem(int i) {
        this.filteredItems.remove(i);
        this.view.removeItem(i);
    }

    /* JADX INFO: Access modifiers changed from: private */
    public void requestHelpContent() {
        if (!this.networkInfoProvider.isNetworkAvailable()) {
            RetryAction retryAction = new RetryAction() { // from class: zendesk.support.guide.HelpAdapterPresenter.1
                @Override // zendesk.core.RetryAction
                public void onRetry() {
                    HelpAdapterPresenter.this.requestHelpContent();
                }
            };
            this.retryAction = retryAction;
            this.networkInfoProvider.addRetryAction(RETRY_ACTION_ID, retryAction);
        }
        this.model.getArticles(this.helpCenterUiConfig.getCategoryIds(), this.helpCenterUiConfig.getSectionIds(), this.helpCenterUiConfig.getLabelNames(), this.callback);
    }

    @Override // zendesk.support.guide.HelpMvp$Presenter
    public HelpItem getItem(int i) {
        return this.filteredItems.get(i);
    }

    @Override // zendesk.support.guide.HelpMvp$Presenter
    public int getItemCount() {
        if (this.hasError) {
            return 0;
        }
        return Math.max(this.filteredItems.size() + getPaddingItemCount(), 1);
    }

    @Override // zendesk.support.guide.HelpMvp$Presenter
    public HelpItem getItemForBinding(int i) {
        if (this.filteredItems.size() <= 0 || i >= this.filteredItems.size()) {
            return null;
        }
        return this.filteredItems.get(i);
    }

    @Override // zendesk.support.guide.HelpMvp$Presenter
    public int getItemViewType(int i) {
        if (this.noResults) {
            return 7;
        }
        if (this.filteredItems.size() > 0) {
            if (i == this.filteredItems.size()) {
                return 8;
            }
            return this.filteredItems.get(i).getViewType();
        }
        return 5;
    }

    @Override // zendesk.support.guide.HelpMvp$Presenter
    public void onAttached() {
        this.networkInfoProvider.register();
        if (l10.g(this.helpItems)) {
            requestHelpContent();
        }
    }

    @Override // zendesk.support.guide.HelpMvp$Presenter
    public boolean onCategoryClick(CategoryItem categoryItem, int i) {
        if (categoryItem == null) {
            return false;
        }
        boolean expanded = categoryItem.setExpanded(!categoryItem.isExpanded());
        if (expanded) {
            expandItem(categoryItem, this.filteredItems.indexOf(categoryItem));
        } else {
            collapseItem(this.filteredItems.indexOf(categoryItem));
        }
        return expanded;
    }

    @Override // zendesk.support.guide.HelpMvp$Presenter
    public void onDetached() {
        this.networkInfoProvider.removeRetryAction(RETRY_ACTION_ID);
        this.networkInfoProvider.unregister();
    }

    @Override // zendesk.support.guide.HelpMvp$Presenter
    public void onSeeAllClick(SeeAllArticlesItem seeAllArticlesItem) {
        loadMoreArticles(seeAllArticlesItem);
    }

    @Override // zendesk.support.guide.HelpMvp$Presenter
    public void setContentPresenter(HelpCenterMvp$Presenter helpCenterMvp$Presenter) {
        this.contentPresenter = helpCenterMvp$Presenter;
    }
}
