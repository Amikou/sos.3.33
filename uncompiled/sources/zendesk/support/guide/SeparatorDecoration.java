package zendesk.support.guide;

import android.graphics.Canvas;
import android.graphics.drawable.Drawable;
import android.view.View;
import android.view.ViewGroup;
import androidx.recyclerview.widget.RecyclerView;
import zendesk.support.guide.HelpRecyclerViewAdapter;

/* loaded from: classes3.dex */
public class SeparatorDecoration extends RecyclerView.n {
    private Drawable divider;

    public SeparatorDecoration(Drawable drawable) {
        this.divider = drawable;
    }

    private boolean isItemACategory(RecyclerView.a0 a0Var) {
        return a0Var instanceof HelpRecyclerViewAdapter.CategoryViewHolder;
    }

    private boolean isItemAnExpandedCategory(RecyclerView.a0 a0Var) {
        return (a0Var instanceof HelpRecyclerViewAdapter.CategoryViewHolder) && ((HelpRecyclerViewAdapter.CategoryViewHolder) a0Var).isExpanded();
    }

    private boolean isItemAnUnexpandedCategory(RecyclerView.a0 a0Var) {
        return (a0Var instanceof HelpRecyclerViewAdapter.CategoryViewHolder) && !((HelpRecyclerViewAdapter.CategoryViewHolder) a0Var).isExpanded();
    }

    @Override // androidx.recyclerview.widget.RecyclerView.n
    public void onDrawOver(Canvas canvas, RecyclerView recyclerView, RecyclerView.x xVar) {
        super.onDraw(canvas, recyclerView, xVar);
        if (recyclerView.getItemAnimator() == null || !recyclerView.getItemAnimator().isRunning()) {
            int childCount = recyclerView.getChildCount();
            for (int i = 0; i < childCount; i++) {
                View childAt = recyclerView.getChildAt(i);
                if (shouldShowTopSeparator(recyclerView, i)) {
                    int paddingLeft = recyclerView.getPaddingLeft();
                    int width = recyclerView.getWidth() - recyclerView.getPaddingRight();
                    int top = childAt.getTop() + ((ViewGroup.MarginLayoutParams) ((RecyclerView.LayoutParams) childAt.getLayoutParams())).topMargin;
                    this.divider.setBounds(paddingLeft, top, width, this.divider.getIntrinsicHeight() + top);
                    this.divider.draw(canvas);
                }
            }
        }
    }

    public boolean shouldShowTopSeparator(RecyclerView recyclerView, int i) {
        boolean isItemACategory = isItemACategory(recyclerView.h0(recyclerView.getChildAt(i)));
        boolean isItemAnExpandedCategory = isItemAnExpandedCategory(recyclerView.h0(recyclerView.getChildAt(i)));
        boolean z = i > 0 && isItemAnUnexpandedCategory(recyclerView.h0(recyclerView.getChildAt(i - 1)));
        if (isItemACategory) {
            return isItemAnExpandedCategory || !z;
        }
        return false;
    }
}
