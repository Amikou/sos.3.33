package zendesk.support.guide;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import zendesk.configurations.Configuration;
import zendesk.messaging.Engine;
import zendesk.messaging.EngineListRegistry;
import zendesk.support.Article;

/* loaded from: classes3.dex */
public class ArticleConfiguration implements Configuration {
    public static final int ARTICLE_ID = 1;
    public static final int ARTICLE_MODEL = 2;
    public static final int UNKNOWN = -1;
    private final ArticleViewModel article;
    private final long articleId;
    private final int configurationState;
    private final List<Configuration> configurations;
    private final boolean contactUsVisible;
    private final String engineRegistryId;

    /* loaded from: classes3.dex */
    public static class Builder {
        private ArticleViewModel article;
        private long articleId;
        private boolean contactUsVisible = true;
        private List<Configuration> configurations = Collections.emptyList();
        private List<Engine> engines = Collections.emptyList();
        private int configurationState = -1;

        public Builder(long j) {
            this.articleId = j;
        }

        private void setConfigurations(List<Configuration> list) {
            if (l10.i(list)) {
                this.configurations = list;
                ArticleConfiguration articleConfiguration = (ArticleConfiguration) z40.h().e(list, ArticleConfiguration.class);
                if (articleConfiguration != null) {
                    this.contactUsVisible = articleConfiguration.isContactUsButtonVisible();
                    this.engines = EngineListRegistry.INSTANCE.retrieveEngineList(articleConfiguration.engineRegistryId);
                }
            }
        }

        public Configuration config() {
            return new ArticleConfiguration(this, EngineListRegistry.INSTANCE.register(this.engines));
        }

        public Intent intent(Context context, Configuration... configurationArr) {
            return intent(context, Arrays.asList(configurationArr));
        }

        public void show(Context context, Configuration... configurationArr) {
            context.startActivity(intent(context, configurationArr));
        }

        public Builder withContactUsButtonVisible(boolean z) {
            this.contactUsVisible = z;
            return this;
        }

        public Builder withEngines(List<Engine> list) {
            this.engines = list;
            return this;
        }

        public Intent intent(Context context, List<Configuration> list) {
            setConfigurations(list);
            Configuration config = config();
            Intent intent = new Intent(context, ViewArticleActivity.class);
            z40.h().c(intent, config);
            return intent;
        }

        public void show(Context context, List<Configuration> list) {
            context.startActivity(intent(context, list));
        }

        public Builder withEngines(Engine... engineArr) {
            return withEngines(Arrays.asList(engineArr));
        }

        public Builder(Article article) {
            this.article = new ArticleViewModel(article);
        }

        public Builder() {
        }
    }

    public ArticleViewModel getArticle() {
        return this.article;
    }

    public long getArticleId() {
        return this.articleId;
    }

    public int getConfigurationState() {
        return this.configurationState;
    }

    @Override // zendesk.configurations.Configuration
    @SuppressLint({"RestrictedApi"})
    public List<Configuration> getConfigurations() {
        return z40.h().a(this.configurations, this);
    }

    public List<Engine> getEngines() {
        return EngineListRegistry.INSTANCE.retrieveEngineList(this.engineRegistryId);
    }

    public boolean isContactUsButtonVisible() {
        return this.contactUsVisible;
    }

    private ArticleConfiguration(Builder builder, String str) {
        this.configurationState = builder.configurationState;
        this.article = builder.article;
        this.articleId = builder.articleId;
        this.contactUsVisible = builder.contactUsVisible;
        this.configurations = builder.configurations;
        this.engineRegistryId = str;
    }
}
