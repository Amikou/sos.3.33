package zendesk.support.guide;

import java.util.ArrayList;
import java.util.List;
import zendesk.support.Article;
import zendesk.support.ArticleItem;
import zendesk.support.HelpCenterProvider;
import zendesk.support.HelpItem;
import zendesk.support.HelpRequest;
import zendesk.support.SectionItem;

/* loaded from: classes3.dex */
public class HelpModel implements HelpMvp$Model {
    private HelpCenterProvider provider;

    public HelpModel(HelpCenterProvider helpCenterProvider) {
        this.provider = helpCenterProvider;
    }

    /* JADX INFO: Access modifiers changed from: private */
    public ArticleItem convertArticle(Article article) {
        return new ArticleItem(article.getId(), article.getSectionId(), article.getTitle());
    }

    @Override // zendesk.support.guide.HelpMvp$Model
    public void getArticles(List<Long> list, List<Long> list2, String[] strArr, rs4<List<HelpItem>> rs4Var) {
        this.provider.getHelp(new HelpRequest.Builder().withCategoryIds(list).withSectionIds(list2).withLabelNames(strArr).includeCategories().includeSections().build(), rs4Var);
    }

    @Override // zendesk.support.guide.HelpMvp$Model
    public void getArticlesForSection(SectionItem sectionItem, String[] strArr, final rs4<List<ArticleItem>> rs4Var) {
        if (sectionItem != null && sectionItem.getId() != null) {
            this.provider.getArticles(sectionItem.getId(), ru3.g(strArr), new rs4<List<Article>>() { // from class: zendesk.support.guide.HelpModel.1
                @Override // defpackage.rs4
                public void onError(cw0 cw0Var) {
                    rs4 rs4Var2 = rs4Var;
                    if (rs4Var2 != null) {
                        rs4Var2.onError(cw0Var);
                    }
                }

                @Override // defpackage.rs4
                public void onSuccess(List<Article> list) {
                    ArrayList arrayList = new ArrayList(list.size());
                    for (Article article : list) {
                        arrayList.add(HelpModel.this.convertArticle(article));
                    }
                    rs4 rs4Var2 = rs4Var;
                    if (rs4Var2 != null) {
                        rs4Var2.onSuccess(arrayList);
                    }
                }
            });
        } else {
            rs4Var.onError(new dw0("SectionItem or its ID was null, cannot load more articles."));
        }
    }
}
