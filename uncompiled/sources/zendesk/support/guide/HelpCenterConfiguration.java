package zendesk.support.guide;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import com.zendesk.logger.Logger;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import zendesk.configurations.Configuration;
import zendesk.messaging.Engine;
import zendesk.messaging.EngineListRegistry;

/* loaded from: classes3.dex */
public class HelpCenterConfiguration implements Configuration {
    private final List<Long> categoryIds;
    private final boolean collapseCategories;
    private List<Configuration> configurations;
    private final boolean contactUsButtonVisibility;
    private final String engineRegistryId;
    private final String[] labelNames;
    private final List<Long> sectionIds;
    private final boolean showConversationsMenuButton;

    /* loaded from: classes3.dex */
    public static class Builder {
        private List<Long> categoryIds = Collections.emptyList();
        private List<Long> sectionIds = Collections.emptyList();
        private String[] labelNames = new String[0];
        private List<Engine> engines = Collections.emptyList();
        private boolean contactUsButtonVisible = true;
        private boolean collapseCategories = false;
        private boolean showConversationsMenuButton = true;
        private List<Configuration> configurations = new ArrayList();

        private void setConfigurations(List<Configuration> list) {
            this.configurations = list;
            HelpCenterConfiguration helpCenterConfiguration = (HelpCenterConfiguration) z40.h().e(list, HelpCenterConfiguration.class);
            if (helpCenterConfiguration != null) {
                this.contactUsButtonVisible = helpCenterConfiguration.contactUsButtonVisibility;
                this.categoryIds = helpCenterConfiguration.categoryIds;
                this.sectionIds = helpCenterConfiguration.sectionIds;
                this.collapseCategories = helpCenterConfiguration.collapseCategories;
                this.labelNames = helpCenterConfiguration.labelNames;
                this.engines = EngineListRegistry.INSTANCE.retrieveEngineList(helpCenterConfiguration.engineRegistryId);
                this.showConversationsMenuButton = helpCenterConfiguration.showConversationsMenuButton;
            }
        }

        public Configuration config() {
            return new HelpCenterConfiguration(this, EngineListRegistry.INSTANCE.register(this.engines));
        }

        public Intent intent(Context context, Configuration... configurationArr) {
            return intent(context, Arrays.asList(configurationArr));
        }

        public void show(Context context, Configuration... configurationArr) {
            context.startActivity(intent(context, configurationArr));
        }

        public Builder withArticlesForCategoryIds(Long... lArr) {
            return withArticlesForCategoryIds(Arrays.asList(lArr));
        }

        public Builder withArticlesForSectionIds(Long... lArr) {
            return withArticlesForSectionIds(Arrays.asList(lArr));
        }

        public Builder withCategoriesCollapsed(boolean z) {
            this.collapseCategories = z;
            return this;
        }

        public Builder withContactUsButtonVisible(boolean z) {
            this.contactUsButtonVisible = z;
            return this;
        }

        public Builder withEngines(List<Engine> list) {
            this.engines = list;
            return this;
        }

        public Builder withLabelNames(String... strArr) {
            if (l10.j(strArr)) {
                this.labelNames = strArr;
            }
            return this;
        }

        public Builder withShowConversationsMenuButton(boolean z) {
            this.showConversationsMenuButton = z;
            return this;
        }

        @SuppressLint({"RestrictedApi"})
        public Intent intent(Context context, List<Configuration> list) {
            setConfigurations(list);
            Intent intent = new Intent(context, HelpCenterActivity.class);
            z40.h().c(intent, config());
            return intent;
        }

        public void show(Context context, List<Configuration> list) {
            context.startActivity(intent(context, list));
        }

        public Builder withArticlesForCategoryIds(List<Long> list) {
            if (this.sectionIds.size() > 0) {
                Logger.k(HelpCenterActivity.LOG_TAG, "Builder: sections have already been specified. Removing section IDs to set category IDs.", new Object[0]);
                this.sectionIds = Collections.emptyList();
            }
            this.categoryIds = l10.c(list);
            return this;
        }

        public Builder withArticlesForSectionIds(List<Long> list) {
            if (this.categoryIds.size() > 0) {
                Logger.k(HelpCenterActivity.LOG_TAG, "Builder: categories have already been specified. Removing category IDs to set section IDs.", new Object[0]);
                this.categoryIds = Collections.emptyList();
            }
            this.sectionIds = l10.c(list);
            return this;
        }

        public Builder withEngines(Engine... engineArr) {
            return withEngines(Arrays.asList(engineArr));
        }

        public Builder withLabelNames(List<String> list) {
            return withLabelNames((String[]) list.toArray(new String[0]));
        }
    }

    public List<Long> getCategoryIds() {
        return this.categoryIds;
    }

    @Override // zendesk.configurations.Configuration
    @SuppressLint({"RestrictedApi"})
    public List<Configuration> getConfigurations() {
        return z40.h().a(this.configurations, this);
    }

    public List<Engine> getEngines() {
        return EngineListRegistry.INSTANCE.retrieveEngineList(this.engineRegistryId);
    }

    public String[] getLabelNames() {
        return this.labelNames;
    }

    public List<Long> getSectionIds() {
        return this.sectionIds;
    }

    public boolean isCollapseCategories() {
        return this.collapseCategories;
    }

    public boolean isContactUsButtonVisible() {
        return this.contactUsButtonVisibility;
    }

    public boolean isShowConversationsMenuButton() {
        return this.showConversationsMenuButton;
    }

    private HelpCenterConfiguration(Builder builder, String str) {
        this.categoryIds = builder.categoryIds;
        this.sectionIds = builder.sectionIds;
        this.labelNames = builder.labelNames;
        this.contactUsButtonVisibility = builder.contactUsButtonVisible;
        this.collapseCategories = builder.collapseCategories;
        this.showConversationsMenuButton = builder.showConversationsMenuButton;
        this.configurations = builder.configurations;
        this.engineRegistryId = str;
    }
}
