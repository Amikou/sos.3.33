package zendesk.support.guide;

import android.animation.ObjectAnimator;
import android.content.Context;
import android.graphics.PorterDuff;
import android.graphics.drawable.Drawable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.TextView;
import androidx.core.graphics.drawable.a;
import androidx.recyclerview.widget.RecyclerView;
import com.zendesk.logger.Logger;
import java.util.List;
import zendesk.core.NetworkInfoProvider;
import zendesk.support.CategoryItem;
import zendesk.support.HelpCenterProvider;
import zendesk.support.HelpItem;
import zendesk.support.SectionItem;
import zendesk.support.SeeAllArticlesItem;

/* loaded from: classes3.dex */
public class HelpRecyclerViewAdapter extends RecyclerView.Adapter<HelpViewHolder> implements HelpMvp$View {
    private Context context;
    private int defaultCategoryTitleColour;
    private final HelpCenterConfiguration helpCenterUiConfig;
    private int highlightCategoryTitleColour;
    private HelpMvp$Presenter presenter;

    /* loaded from: classes3.dex */
    public class ArticleViewHolder extends HelpViewHolder {
        public ArticleViewHolder(View view) {
            super(view);
            this.textView = (TextView) view;
        }

        @Override // zendesk.support.guide.HelpRecyclerViewAdapter.HelpViewHolder
        public void bindTo(final HelpItem helpItem, int i) {
            if (helpItem != null && helpItem.getId() != null) {
                this.textView.setText(ne4.a(helpItem.getName()));
                this.textView.setOnClickListener(new View.OnClickListener() { // from class: zendesk.support.guide.HelpRecyclerViewAdapter.ArticleViewHolder.1
                    @Override // android.view.View.OnClickListener
                    public void onClick(View view) {
                        ViewArticleActivity.builder(helpItem.getId().longValue()).show(HelpRecyclerViewAdapter.this.context, HelpRecyclerViewAdapter.this.helpCenterUiConfig.getConfigurations());
                    }
                });
                return;
            }
            Logger.e(HelpCenterActivity.LOG_TAG, "Article item was null, cannot bind", new Object[0]);
        }
    }

    /* loaded from: classes3.dex */
    public class CategoryViewHolder extends HelpViewHolder {
        private static final int ROTATION_END_LEVEL = 10000;
        private static final String ROTATION_PROPERTY_NAME = "level";
        private static final int ROTATION_START_LEVEL = 0;
        private boolean expanded;
        private Drawable expanderDrawable;

        public CategoryViewHolder(View view) {
            super(view);
            this.textView = (TextView) view;
            Drawable mutate = a.r(m70.f(view.getContext(), rz2.zs_help_ic_expand_more)).mutate();
            this.expanderDrawable = mutate;
            a.n(mutate, ne4.e(16842808, HelpRecyclerViewAdapter.this.context, uy2.zs_fallback_text_color));
            a.p(this.expanderDrawable, PorterDuff.Mode.SRC_IN);
            ((TextView) view).setCompoundDrawablesRelativeWithIntrinsicBounds((Drawable) null, (Drawable) null, this.expanderDrawable, (Drawable) null);
        }

        /* JADX INFO: Access modifiers changed from: private */
        public void setHighlightColor(boolean z) {
            if (z) {
                this.textView.setTextColor(HelpRecyclerViewAdapter.this.highlightCategoryTitleColour);
                this.expanderDrawable.setColorFilter(HelpRecyclerViewAdapter.this.highlightCategoryTitleColour, PorterDuff.Mode.SRC_IN);
                return;
            }
            this.textView.setTextColor(HelpRecyclerViewAdapter.this.defaultCategoryTitleColour);
            this.expanderDrawable.setColorFilter(HelpRecyclerViewAdapter.this.defaultCategoryTitleColour, PorterDuff.Mode.SRC_IN);
        }

        @Override // zendesk.support.guide.HelpRecyclerViewAdapter.HelpViewHolder
        public void bindTo(HelpItem helpItem, final int i) {
            if (helpItem == null) {
                Logger.e(HelpCenterActivity.LOG_TAG, "Category item was null, cannot bind", new Object[0]);
                return;
            }
            this.textView.setText(ne4.a(helpItem.getName()));
            final CategoryItem categoryItem = (CategoryItem) helpItem;
            boolean isExpanded = categoryItem.isExpanded();
            this.expanded = isExpanded;
            this.expanderDrawable.setLevel(isExpanded ? ROTATION_END_LEVEL : 0);
            setHighlightColor(categoryItem.isExpanded());
            this.textView.setOnClickListener(new View.OnClickListener() { // from class: zendesk.support.guide.HelpRecyclerViewAdapter.CategoryViewHolder.1
                @Override // android.view.View.OnClickListener
                public void onClick(View view) {
                    CategoryViewHolder categoryViewHolder = CategoryViewHolder.this;
                    categoryViewHolder.expanded = HelpRecyclerViewAdapter.this.presenter.onCategoryClick(categoryItem, i);
                    Drawable drawable = CategoryViewHolder.this.expanderDrawable;
                    int[] iArr = new int[2];
                    boolean z = CategoryViewHolder.this.expanded;
                    int i2 = CategoryViewHolder.ROTATION_END_LEVEL;
                    iArr[0] = z ? 0 : CategoryViewHolder.ROTATION_END_LEVEL;
                    if (!CategoryViewHolder.this.expanded) {
                        i2 = 0;
                    }
                    iArr[1] = i2;
                    ObjectAnimator.ofInt(drawable, CategoryViewHolder.ROTATION_PROPERTY_NAME, iArr).start();
                    CategoryViewHolder categoryViewHolder2 = CategoryViewHolder.this;
                    categoryViewHolder2.setHighlightColor(categoryViewHolder2.expanded);
                }
            });
        }

        public boolean isExpanded() {
            return this.expanded;
        }
    }

    /* loaded from: classes3.dex */
    public class ExtraPaddingViewHolder extends HelpViewHolder {
        public ExtraPaddingViewHolder(View view) {
            super(view);
        }

        @Override // zendesk.support.guide.HelpRecyclerViewAdapter.HelpViewHolder
        public void bindTo(HelpItem helpItem, int i) {
        }
    }

    /* loaded from: classes3.dex */
    public static abstract class HelpViewHolder extends RecyclerView.a0 {
        public TextView textView;

        public HelpViewHolder(View view) {
            super(view);
        }

        public abstract void bindTo(HelpItem helpItem, int i);
    }

    /* loaded from: classes3.dex */
    public class LoadingViewHolder extends HelpViewHolder {
        public LoadingViewHolder(View view) {
            super(view);
        }

        @Override // zendesk.support.guide.HelpRecyclerViewAdapter.HelpViewHolder
        public void bindTo(HelpItem helpItem, int i) {
        }
    }

    /* loaded from: classes3.dex */
    public class NoResultsViewHolder extends HelpViewHolder {
        public NoResultsViewHolder(View view) {
            super(view);
        }

        @Override // zendesk.support.guide.HelpRecyclerViewAdapter.HelpViewHolder
        public void bindTo(HelpItem helpItem, int i) {
        }
    }

    /* loaded from: classes3.dex */
    public class SectionViewHolder extends HelpViewHolder {
        public SectionViewHolder(View view) {
            super(view);
            this.textView = (TextView) view.findViewById(e03.section_title);
        }

        @Override // zendesk.support.guide.HelpRecyclerViewAdapter.HelpViewHolder
        public void bindTo(HelpItem helpItem, int i) {
            if (helpItem == null) {
                Logger.e(HelpCenterActivity.LOG_TAG, "Section item was null, cannot bind", new Object[0]);
            } else {
                this.textView.setText(ne4.a(helpItem.getName()));
            }
        }
    }

    /* loaded from: classes3.dex */
    public class SeeAllViewHolder extends HelpViewHolder {
        private ProgressBar progressBar;

        public SeeAllViewHolder(View view) {
            super(view);
            this.textView = (TextView) view.findViewById(e03.help_section_action_button);
            this.progressBar = (ProgressBar) view.findViewById(e03.help_section_loading_progress);
        }

        @Override // zendesk.support.guide.HelpRecyclerViewAdapter.HelpViewHolder
        public void bindTo(final HelpItem helpItem, int i) {
            if (!(helpItem instanceof SeeAllArticlesItem)) {
                Logger.e(HelpCenterActivity.LOG_TAG, "SeeAll item was null, cannot bind", new Object[0]);
                return;
            }
            final SeeAllArticlesItem seeAllArticlesItem = (SeeAllArticlesItem) helpItem;
            if (seeAllArticlesItem.isLoading()) {
                this.textView.setVisibility(8);
                this.progressBar.setVisibility(0);
            } else {
                this.textView.setVisibility(0);
                this.progressBar.setVisibility(8);
            }
            SectionItem section = seeAllArticlesItem.getSection();
            this.textView.setText(section != null ? HelpRecyclerViewAdapter.this.context.getString(t13.support_help_see_all_n_articles_label, Integer.valueOf(section.getTotalArticlesCount())) : HelpRecyclerViewAdapter.this.context.getString(t13.support_help_see_all_articles_label));
            this.textView.setOnClickListener(new View.OnClickListener() { // from class: zendesk.support.guide.HelpRecyclerViewAdapter.SeeAllViewHolder.1
                @Override // android.view.View.OnClickListener
                public void onClick(View view) {
                    SeeAllViewHolder.this.textView.setVisibility(8);
                    SeeAllViewHolder.this.progressBar.setVisibility(0);
                    HelpRecyclerViewAdapter.this.presenter.onSeeAllClick((SeeAllArticlesItem) helpItem);
                    seeAllArticlesItem.setLoading(true);
                }
            });
        }
    }

    public HelpRecyclerViewAdapter(HelpCenterConfiguration helpCenterConfiguration, HelpCenterProvider helpCenterProvider, NetworkInfoProvider networkInfoProvider) {
        this.presenter = new HelpAdapterPresenter(this, new HelpModel(helpCenterProvider), networkInfoProvider, helpCenterConfiguration);
        this.helpCenterUiConfig = helpCenterConfiguration;
    }

    private View inflateView(ViewGroup viewGroup, int i) {
        return LayoutInflater.from(viewGroup.getContext()).inflate(i, viewGroup, false);
    }

    @Override // zendesk.support.guide.HelpMvp$View
    public void addItem(int i, HelpItem helpItem) {
        notifyItemInserted(i);
    }

    @Override // androidx.recyclerview.widget.RecyclerView.Adapter
    public int getItemCount() {
        return this.presenter.getItemCount();
    }

    @Override // androidx.recyclerview.widget.RecyclerView.Adapter
    public int getItemViewType(int i) {
        return this.presenter.getItemViewType(i);
    }

    @Override // androidx.recyclerview.widget.RecyclerView.Adapter
    public void onAttachedToRecyclerView(RecyclerView recyclerView) {
        super.onAttachedToRecyclerView(recyclerView);
        Context context = recyclerView.getContext();
        this.context = context;
        this.highlightCategoryTitleColour = ne4.e(hy2.colorPrimary, context, uy2.zs_fallback_text_color);
        this.defaultCategoryTitleColour = m70.d(this.context, uy2.zs_help_text_color_primary);
        this.presenter.onAttached();
    }

    @Override // androidx.recyclerview.widget.RecyclerView.Adapter
    public void onDetachedFromRecyclerView(RecyclerView recyclerView) {
        super.onDetachedFromRecyclerView(recyclerView);
        this.presenter.onDetached();
        this.context = null;
    }

    @Override // zendesk.support.guide.HelpMvp$View
    public void removeItem(int i) {
        notifyItemRemoved(i);
    }

    public void setContentUpdateListener(HelpCenterMvp$Presenter helpCenterMvp$Presenter) {
        HelpMvp$Presenter helpMvp$Presenter = this.presenter;
        if (helpMvp$Presenter != null) {
            helpMvp$Presenter.setContentPresenter(helpCenterMvp$Presenter);
        }
    }

    @Override // zendesk.support.guide.HelpMvp$View
    public void showItems(List<HelpItem> list) {
        notifyDataSetChanged();
    }

    @Override // androidx.recyclerview.widget.RecyclerView.Adapter
    public void onBindViewHolder(HelpViewHolder helpViewHolder, int i) {
        if (helpViewHolder == null) {
            Logger.k(HelpCenterActivity.LOG_TAG, "Holder was null, possible unexpected item type", new Object[0]);
        } else {
            helpViewHolder.bindTo(this.presenter.getItemForBinding(i), i);
        }
    }

    @Override // androidx.recyclerview.widget.RecyclerView.Adapter
    public HelpViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        switch (i) {
            case 1:
                return new CategoryViewHolder(inflateView(viewGroup, a13.zs_row_category));
            case 2:
                return new SectionViewHolder(inflateView(viewGroup, a13.zs_row_section));
            case 3:
                return new ArticleViewHolder(inflateView(viewGroup, a13.zs_row_article));
            case 4:
                return new SeeAllViewHolder(inflateView(viewGroup, a13.zs_row_action));
            case 5:
                return new LoadingViewHolder(inflateView(viewGroup, a13.zs_row_loading_progress));
            case 6:
            default:
                Logger.k(HelpCenterActivity.LOG_TAG, "Unknown item type, returning null for holder", new Object[0]);
                return null;
            case 7:
                return new NoResultsViewHolder(inflateView(viewGroup, a13.zs_row_no_articles_found));
            case 8:
                return new ExtraPaddingViewHolder(inflateView(viewGroup, a13.zs_row_padding));
        }
    }
}
