package zendesk.support.guide;

import java.io.Serializable;
import java.util.Date;
import zendesk.support.Article;

/* JADX INFO: Access modifiers changed from: package-private */
/* loaded from: classes3.dex */
public class ArticleViewModel implements Serializable {
    private final String authorName;
    private final String body;
    private final Date createdAt;
    private final long id;
    private final String title;

    public ArticleViewModel(Article article) {
        this.id = article.getId().longValue();
        this.title = article.getTitle();
        this.body = article.getBody();
        this.createdAt = article.getCreatedAt();
        this.authorName = article.getAuthor() == null ? null : article.getAuthor().getName();
    }

    public String getAuthorName() {
        return this.authorName;
    }

    public String getBody() {
        return this.body;
    }

    public Date getCreatedAt() {
        return this.createdAt;
    }

    public long getId() {
        return this.id;
    }

    public String getTitle() {
        return this.title;
    }
}
