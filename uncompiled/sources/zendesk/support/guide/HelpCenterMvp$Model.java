package zendesk.support.guide;

import java.util.List;
import zendesk.support.HelpCenterSettings;
import zendesk.support.SearchArticle;

/* loaded from: classes3.dex */
public interface HelpCenterMvp$Model {
    void getSettings(rs4<HelpCenterSettings> rs4Var);

    void search(List<Long> list, List<Long> list2, String str, String[] strArr, rs4<List<SearchArticle>> rs4Var);
}
