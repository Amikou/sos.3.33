package zendesk.support.guide;

import zendesk.core.ActionHandler;

/* loaded from: classes3.dex */
public final class GuideSdkModule_ViewArticleActionHandlerFactory implements y11<ActionHandler> {

    /* loaded from: classes3.dex */
    public static final class InstanceHolder {
        private static final GuideSdkModule_ViewArticleActionHandlerFactory INSTANCE = new GuideSdkModule_ViewArticleActionHandlerFactory();

        private InstanceHolder() {
        }
    }

    public static GuideSdkModule_ViewArticleActionHandlerFactory create() {
        return InstanceHolder.INSTANCE;
    }

    public static ActionHandler viewArticleActionHandler() {
        return (ActionHandler) cu2.f(GuideSdkModule.viewArticleActionHandler());
    }

    @Override // defpackage.ew2
    public ActionHandler get() {
        return viewArticleActionHandler();
    }
}
