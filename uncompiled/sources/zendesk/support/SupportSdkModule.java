package zendesk.support;

import android.annotation.SuppressLint;
import android.content.Context;
import android.graphics.Bitmap;
import android.os.Handler;
import android.os.Looper;
import com.google.gson.Gson;
import com.squareup.picasso.Picasso;
import com.squareup.picasso.k;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Executor;
import java.util.concurrent.ExecutorService;
import okhttp3.OkHttpClient;
import zendesk.core.ActionHandler;
import zendesk.core.ApplicationConfiguration;
import zendesk.core.SessionStorage;
import zendesk.support.requestlist.RequestInfoDataSource;

/* loaded from: classes3.dex */
public class SupportSdkModule {
    private static final int MAX_DISK_CACHE_SIZE = 20971520;

    public z40 configurationHelper() {
        return new z40();
    }

    public Executor mainThreadExecutor() {
        return new Executor() { // from class: zendesk.support.SupportSdkModule.1
            public Handler handler = new Handler(Looper.getMainLooper());

            @Override // java.util.concurrent.Executor
            public void execute(Runnable runnable) {
                this.handler.post(runnable);
            }
        };
    }

    public k okHttp3Downloader(OkHttpClient okHttpClient) {
        return new k(okHttpClient);
    }

    public Gson provides() {
        return new Gson();
    }

    public List<ActionHandler> providesActionHandlers() {
        ArrayList arrayList = new ArrayList();
        arrayList.add(new DeepLinkToRequestActionHandler());
        return arrayList;
    }

    public Picasso providesPicasso(Context context, k kVar, ExecutorService executorService) {
        return new Picasso.b(context).c(kVar).d(executorService).b(Bitmap.Config.RGB_565).a();
    }

    public ep0 providesRequestDiskLruCache(SessionStorage sessionStorage) {
        try {
            return ep0.x(new File(sessionStorage.getZendeskDataDir(), "request"), 1, 1, 20971520L);
        } catch (IOException e) {
            e.printStackTrace();
            return null;
        }
    }

    @SuppressLint({"RestrictedApi"})
    public String providesZendeskUrl(ApplicationConfiguration applicationConfiguration) {
        return applicationConfiguration.getZendeskUrl();
    }

    public RequestInfoDataSource.LocalDataSource requestInfoDataSource(SupportUiStorage supportUiStorage, Executor executor, ExecutorService executorService) {
        return new RequestInfoDataSource.LocalDataSource(new RequestInfoDataSource.Disk(executor, executorService, supportUiStorage, RequestInfoDataSource.LOCAL));
    }

    public SupportUiStorage supportUiStorage(ep0 ep0Var, Gson gson) {
        return new SupportUiStorage(ep0Var, gson);
    }
}
