package zendesk.support;

import java.util.Locale;
import zendesk.core.BlipsProvider;

/* loaded from: classes3.dex */
public final class GuideProviderModule_ProvidesHelpCenterBlipsProviderFactory implements y11<HelpCenterBlipsProvider> {
    private final ew2<BlipsProvider> blipsProvider;
    private final ew2<Locale> localeProvider;
    private final GuideProviderModule module;

    public GuideProviderModule_ProvidesHelpCenterBlipsProviderFactory(GuideProviderModule guideProviderModule, ew2<BlipsProvider> ew2Var, ew2<Locale> ew2Var2) {
        this.module = guideProviderModule;
        this.blipsProvider = ew2Var;
        this.localeProvider = ew2Var2;
    }

    public static GuideProviderModule_ProvidesHelpCenterBlipsProviderFactory create(GuideProviderModule guideProviderModule, ew2<BlipsProvider> ew2Var, ew2<Locale> ew2Var2) {
        return new GuideProviderModule_ProvidesHelpCenterBlipsProviderFactory(guideProviderModule, ew2Var, ew2Var2);
    }

    public static HelpCenterBlipsProvider providesHelpCenterBlipsProvider(GuideProviderModule guideProviderModule, BlipsProvider blipsProvider, Locale locale) {
        return (HelpCenterBlipsProvider) cu2.f(guideProviderModule.providesHelpCenterBlipsProvider(blipsProvider, locale));
    }

    @Override // defpackage.ew2
    public HelpCenterBlipsProvider get() {
        return providesHelpCenterBlipsProvider(this.module, this.blipsProvider.get(), this.localeProvider.get());
    }
}
