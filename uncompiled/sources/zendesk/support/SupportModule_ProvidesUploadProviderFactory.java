package zendesk.support;

/* loaded from: classes3.dex */
public final class SupportModule_ProvidesUploadProviderFactory implements y11<UploadProvider> {
    private final SupportModule module;

    public SupportModule_ProvidesUploadProviderFactory(SupportModule supportModule) {
        this.module = supportModule;
    }

    public static SupportModule_ProvidesUploadProviderFactory create(SupportModule supportModule) {
        return new SupportModule_ProvidesUploadProviderFactory(supportModule);
    }

    public static UploadProvider providesUploadProvider(SupportModule supportModule) {
        return (UploadProvider) cu2.f(supportModule.providesUploadProvider());
    }

    @Override // defpackage.ew2
    public UploadProvider get() {
        return providesUploadProvider(this.module);
    }
}
