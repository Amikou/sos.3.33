package zendesk.support;

/* loaded from: classes3.dex */
public interface HelpCenterSessionCache {
    LastSearch getLastSearch();

    boolean isUniqueSearchResultClick();

    void setLastSearch(String str, int i);

    void unsetUniqueSearchResultClick();
}
