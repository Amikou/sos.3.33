package zendesk.support;

import zendesk.core.RestServiceProvider;

/* loaded from: classes3.dex */
public final class GuideProviderModule_ProvidesHelpCenterServiceFactory implements y11<HelpCenterService> {
    private final ew2<HelpCenterCachingNetworkConfig> helpCenterCachingNetworkConfigProvider;
    private final ew2<RestServiceProvider> restServiceProvider;

    public GuideProviderModule_ProvidesHelpCenterServiceFactory(ew2<RestServiceProvider> ew2Var, ew2<HelpCenterCachingNetworkConfig> ew2Var2) {
        this.restServiceProvider = ew2Var;
        this.helpCenterCachingNetworkConfigProvider = ew2Var2;
    }

    public static GuideProviderModule_ProvidesHelpCenterServiceFactory create(ew2<RestServiceProvider> ew2Var, ew2<HelpCenterCachingNetworkConfig> ew2Var2) {
        return new GuideProviderModule_ProvidesHelpCenterServiceFactory(ew2Var, ew2Var2);
    }

    public static HelpCenterService providesHelpCenterService(RestServiceProvider restServiceProvider, Object obj) {
        return (HelpCenterService) cu2.f(GuideProviderModule.providesHelpCenterService(restServiceProvider, (HelpCenterCachingNetworkConfig) obj));
    }

    @Override // defpackage.ew2
    public HelpCenterService get() {
        return providesHelpCenterService(this.restServiceProvider.get(), this.helpCenterCachingNetworkConfigProvider.get());
    }
}
