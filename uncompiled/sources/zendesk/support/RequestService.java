package zendesk.support;

import retrofit2.b;

/* loaded from: classes3.dex */
interface RequestService {
    @qo2("/api/mobile/requests/{id}.json?include=last_comment")
    b<RequestResponse> addComment(@vp2("id") String str, @ar UpdateRequestWrapper updateRequestWrapper);

    @oo2("/api/mobile/requests.json?include=last_comment")
    b<RequestResponse> createRequest(@ek1("Mobile-Sdk-Identity") String str, @ar CreateRequestWrapper createRequestWrapper);

    @ge1("/api/mobile/requests.json?sort_by=updated_at&sort_order=desc")
    b<RequestsResponse> getAllRequests(@yw2("status") String str, @yw2("include") String str2);

    @ge1("/api/mobile/requests/{id}/comments.json?sort_order=desc")
    b<CommentsResponse> getComments(@vp2("id") String str);

    @ge1("/api/mobile/requests/{id}/comments.json?sort_order=desc")
    b<CommentsResponse> getCommentsSince(@vp2("id") String str, @yw2("since") String str2, @yw2("role") String str3);

    @ge1("/api/mobile/requests/show_many.json?sort_order=desc")
    b<RequestsResponse> getManyRequests(@yw2("tokens") String str, @yw2("status") String str2, @yw2("include") String str3);

    @ge1("/api/mobile/requests/{id}.json")
    b<RequestResponse> getRequest(@vp2("id") String str, @yw2("include") String str2);

    @ge1("/api/v2/ticket_forms/show_many.json?active=true")
    b<RawTicketFormResponse> getTicketFormsById(@yw2("ids") String str, @yw2("include") String str2);
}
