package zendesk.support;

import java.util.List;

/* loaded from: classes3.dex */
public class CommentsResponse extends ResponseWrapper {
    private List<CommentResponse> comments;
    private List<Object> organizations;
    private List<User> users;

    public List<CommentResponse> getComments() {
        return l10.c(this.comments);
    }

    public List<Object> getOrganizations() {
        return l10.c(this.organizations);
    }

    public List<User> getUsers() {
        return l10.c(this.users);
    }
}
