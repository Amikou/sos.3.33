package zendesk.support;

/* loaded from: classes3.dex */
public final class GuideProviderModule_ProvideHelpCenterSessionCacheFactory implements y11<HelpCenterSessionCache> {

    /* loaded from: classes3.dex */
    public static final class InstanceHolder {
        private static final GuideProviderModule_ProvideHelpCenterSessionCacheFactory INSTANCE = new GuideProviderModule_ProvideHelpCenterSessionCacheFactory();

        private InstanceHolder() {
        }
    }

    public static GuideProviderModule_ProvideHelpCenterSessionCacheFactory create() {
        return InstanceHolder.INSTANCE;
    }

    public static HelpCenterSessionCache provideHelpCenterSessionCache() {
        return (HelpCenterSessionCache) cu2.f(GuideProviderModule.provideHelpCenterSessionCache());
    }

    @Override // defpackage.ew2
    public HelpCenterSessionCache get() {
        return provideHelpCenterSessionCache();
    }
}
