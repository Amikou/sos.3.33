package zendesk.support;

import java.util.List;

/* loaded from: classes3.dex */
public interface RequestSessionCache {
    boolean containsAllTicketForms(List<Long> list);

    List<TicketForm> getTicketFormsById(List<Long> list);

    void updateTicketFormCache(List<TicketForm> list);
}
