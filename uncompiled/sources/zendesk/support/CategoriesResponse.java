package zendesk.support;

import java.util.List;

/* loaded from: classes3.dex */
public class CategoriesResponse {
    private List<Category> categories;

    public List<Category> getCategories() {
        return l10.c(this.categories);
    }
}
