package zendesk.support;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

/* loaded from: classes3.dex */
public class ZendeskRequestSessionCache implements RequestSessionCache {
    private final Map<Long, TicketForm> cachedTicketForms = new HashMap();

    @Override // zendesk.support.RequestSessionCache
    public boolean containsAllTicketForms(List<Long> list) {
        boolean z;
        List e = l10.e(list);
        synchronized (this.cachedTicketForms) {
            Iterator it = e.iterator();
            while (true) {
                if (!it.hasNext()) {
                    z = true;
                    break;
                }
                if (!this.cachedTicketForms.containsKey((Long) it.next())) {
                    z = false;
                    break;
                }
            }
        }
        return z;
    }

    @Override // zendesk.support.RequestSessionCache
    public synchronized List<TicketForm> getTicketFormsById(List<Long> list) {
        ArrayList arrayList;
        arrayList = new ArrayList();
        List<Long> e = l10.e(list);
        synchronized (this.cachedTicketForms) {
            for (Long l : e) {
                arrayList.add(this.cachedTicketForms.get(l));
            }
        }
        return arrayList;
    }

    @Override // zendesk.support.RequestSessionCache
    public void updateTicketFormCache(List<TicketForm> list) {
        List<TicketForm> e = l10.e(list);
        HashMap hashMap = new HashMap();
        for (TicketForm ticketForm : e) {
            hashMap.put(Long.valueOf(ticketForm.getId()), ticketForm);
        }
        synchronized (this.cachedTicketForms) {
            this.cachedTicketForms.putAll(hashMap);
        }
    }
}
