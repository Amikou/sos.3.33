package zendesk.support;

import android.content.Context;

/* loaded from: classes3.dex */
public final class StorageModule_ProvideRequestMigratorFactory implements y11<RequestMigrator> {
    private final ew2<Context> contextProvider;
    private final StorageModule module;

    public StorageModule_ProvideRequestMigratorFactory(StorageModule storageModule, ew2<Context> ew2Var) {
        this.module = storageModule;
        this.contextProvider = ew2Var;
    }

    public static StorageModule_ProvideRequestMigratorFactory create(StorageModule storageModule, ew2<Context> ew2Var) {
        return new StorageModule_ProvideRequestMigratorFactory(storageModule, ew2Var);
    }

    public static RequestMigrator provideRequestMigrator(StorageModule storageModule, Context context) {
        return (RequestMigrator) cu2.f(storageModule.provideRequestMigrator(context));
    }

    @Override // defpackage.ew2
    public RequestMigrator get() {
        return provideRequestMigrator(this.module, this.contextProvider.get());
    }
}
