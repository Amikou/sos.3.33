package zendesk.support;

/* loaded from: classes3.dex */
public interface HelpCenterSettingsProvider {
    void getSettings(rs4<HelpCenterSettings> rs4Var);
}
