package zendesk.support;

import com.google.gson.annotations.SerializedName;

/* loaded from: classes3.dex */
public class ArticleItem implements HelpItem {
    private Long id;
    private String name;
    @SerializedName("section_id")
    private Long sectionId;

    public ArticleItem(Long l, Long l2, String str) {
        this.id = l;
        this.sectionId = l2;
        this.name = str;
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null || getClass() != obj.getClass()) {
            return false;
        }
        ArticleItem articleItem = (ArticleItem) obj;
        Long l = this.id;
        if (l == null ? articleItem.id == null : l.equals(articleItem.id)) {
            Long l2 = this.sectionId;
            Long l3 = articleItem.sectionId;
            return l2 != null ? l2.equals(l3) : l3 == null;
        }
        return false;
    }

    @Override // zendesk.support.HelpItem
    public Long getId() {
        return this.id;
    }

    @Override // zendesk.support.HelpItem
    public String getName() {
        String str = this.name;
        return str == null ? "" : str;
    }

    @Override // zendesk.support.HelpItem
    public Long getParentId() {
        return this.sectionId;
    }

    @Override // zendesk.support.HelpItem
    public int getViewType() {
        return 3;
    }

    public int hashCode() {
        Long l = this.id;
        int hashCode = (l != null ? l.hashCode() : 0) * 31;
        Long l2 = this.sectionId;
        return hashCode + (l2 != null ? l2.hashCode() : 0);
    }
}
