package zendesk.support;

/* loaded from: classes3.dex */
public interface HelpCenterBlipsProvider {
    void articleView(Article article);

    void articleVote(Long l, int i);

    void helpCenterSearch(String str);
}
