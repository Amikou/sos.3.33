package zendesk.support;

import android.content.Context;
import android.graphics.Paint;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.InsetDrawable;
import android.graphics.drawable.LayerDrawable;
import android.graphics.drawable.ShapeDrawable;
import android.graphics.drawable.shapes.OvalShape;
import android.util.AttributeSet;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.TextView;
import com.squareup.picasso.Picasso;
import com.squareup.picasso.o;

/* loaded from: classes3.dex */
public class ZendeskAvatarView extends FrameLayout {
    private static final int[] AVATAR_COLORS = {vy2.zs_avatar_view_color_01, vy2.zs_avatar_view_color_02, vy2.zs_avatar_view_color_03, vy2.zs_avatar_view_color_04, vy2.zs_avatar_view_color_05, vy2.zs_avatar_view_color_06, vy2.zs_avatar_view_color_07, vy2.zs_avatar_view_color_08, vy2.zs_avatar_view_color_09, vy2.zs_avatar_view_color_10, vy2.zs_avatar_view_color_11, vy2.zs_avatar_view_color_12, vy2.zs_avatar_view_color_13, vy2.zs_avatar_view_color_14, vy2.zs_avatar_view_color_15, vy2.zs_avatar_view_color_16, vy2.zs_avatar_view_color_17, vy2.zs_avatar_view_color_18, vy2.zs_avatar_view_color_19};
    private boolean enableOutline;
    private ImageView imageView;
    private int strokeColor;
    private int strokeWidth;
    private TextView textView;

    public ZendeskAvatarView(Context context) {
        this(context, null, 0);
    }

    private Drawable getBackgroundShape(int i) {
        ShapeDrawable shapeDrawable = new ShapeDrawable(new OvalShape());
        shapeDrawable.getPaint().setColor(m70.d(getContext(), i));
        if (this.enableOutline) {
            ShapeDrawable shapeDrawable2 = new ShapeDrawable(new OvalShape());
            Paint paint = shapeDrawable2.getPaint();
            paint.setStyle(Paint.Style.STROKE);
            paint.setAntiAlias(true);
            paint.setColor(this.strokeColor);
            paint.setStrokeWidth(this.strokeWidth);
            return new LayerDrawable(new Drawable[]{shapeDrawable, new InsetDrawable((Drawable) shapeDrawable2, this.strokeWidth / 2)});
        }
        return shapeDrawable;
    }

    private int getColorId(Object obj) {
        int[] iArr = AVATAR_COLORS;
        return iArr[Math.abs(obj.hashCode() % iArr.length)];
    }

    private void initViews() {
        TextView textView = new TextView(getContext());
        this.textView = textView;
        textView.setId(f03.zs_avatar_view_text_view);
        this.textView.setTextColor(m70.d(getContext(), vy2.zs_avatar_text_color));
        this.textView.setGravity(17);
        this.textView.setTextSize(2, 16.0f);
        ImageView imageView = new ImageView(getContext());
        this.imageView = imageView;
        imageView.setId(f03.zs_avatar_view_image_view);
        addView(this.textView);
        addView(this.imageView);
    }

    private void setTextView(String str) {
        setBackground(getBackgroundShape(getColorId(str)));
        this.textView.setText(String.valueOf(Character.toUpperCase(str.charAt(0))));
    }

    public void setStroke(int i, int i2) {
        this.strokeColor = i;
        this.strokeWidth = i2;
        this.enableOutline = true;
    }

    public void showUserWithAvatarImage(Picasso picasso, String str, String str2, int i) {
        this.imageView.setVisibility(0);
        this.imageView.setImageResource(vy2.zs_color_transparent);
        if (ru3.b(str2)) {
            this.textView.setVisibility(0);
            setTextView(str2);
        }
        this.imageView.setScaleType(ImageView.ScaleType.FIT_CENTER);
        o l = picasso.l(str);
        int i2 = i * 2;
        l.k(i2, i2).a().j().l(PicassoTransformations.getRoundWithBorderTransformation(i, this.strokeColor, this.strokeWidth)).f(this.imageView);
    }

    public void showUserWithIdentifier(Object obj) {
        if (obj != null) {
            setBackground(getBackgroundShape(getColorId(obj)));
            this.imageView.setScaleType(ImageView.ScaleType.CENTER);
            this.imageView.setImageResource(sz2.zs_request_list_account_icon);
            this.textView.setVisibility(4);
            this.imageView.setVisibility(0);
        }
    }

    public void showUserWithName(String str) {
        if (ru3.b(str)) {
            setTextView(str);
            this.textView.setVisibility(0);
            this.imageView.setVisibility(4);
        }
    }

    public ZendeskAvatarView(Context context, AttributeSet attributeSet) {
        this(context, attributeSet, 0);
    }

    public ZendeskAvatarView(Context context, AttributeSet attributeSet, int i) {
        super(context, attributeSet, i);
        this.enableOutline = false;
        this.strokeColor = 0;
        this.strokeWidth = 0;
        initViews();
    }
}
