package zendesk.support;

import java.util.Locale;
import zendesk.core.SettingsProvider;
import zendesk.core.ZendeskLocaleConverter;

/* loaded from: classes3.dex */
public final class GuideProviderModule_ProvideSettingsProviderFactory implements y11<HelpCenterSettingsProvider> {
    private final ew2<ZendeskLocaleConverter> localeConverterProvider;
    private final ew2<Locale> localeProvider;
    private final GuideProviderModule module;
    private final ew2<SettingsProvider> sdkSettingsProvider;

    public GuideProviderModule_ProvideSettingsProviderFactory(GuideProviderModule guideProviderModule, ew2<SettingsProvider> ew2Var, ew2<ZendeskLocaleConverter> ew2Var2, ew2<Locale> ew2Var3) {
        this.module = guideProviderModule;
        this.sdkSettingsProvider = ew2Var;
        this.localeConverterProvider = ew2Var2;
        this.localeProvider = ew2Var3;
    }

    public static GuideProviderModule_ProvideSettingsProviderFactory create(GuideProviderModule guideProviderModule, ew2<SettingsProvider> ew2Var, ew2<ZendeskLocaleConverter> ew2Var2, ew2<Locale> ew2Var3) {
        return new GuideProviderModule_ProvideSettingsProviderFactory(guideProviderModule, ew2Var, ew2Var2, ew2Var3);
    }

    public static HelpCenterSettingsProvider provideSettingsProvider(GuideProviderModule guideProviderModule, SettingsProvider settingsProvider, ZendeskLocaleConverter zendeskLocaleConverter, Locale locale) {
        return (HelpCenterSettingsProvider) cu2.f(guideProviderModule.provideSettingsProvider(settingsProvider, zendeskLocaleConverter, locale));
    }

    @Override // defpackage.ew2
    public HelpCenterSettingsProvider get() {
        return provideSettingsProvider(this.module, this.sdkSettingsProvider.get(), this.localeConverterProvider.get(), this.localeProvider.get());
    }
}
