package zendesk.support;

/* loaded from: classes3.dex */
public final class ProviderModule_ProvideProviderStoreFactory implements y11<ProviderStore> {
    private final ew2<HelpCenterProvider> helpCenterProvider;
    private final ProviderModule module;
    private final ew2<RequestProvider> requestProvider;
    private final ew2<UploadProvider> uploadProvider;

    public ProviderModule_ProvideProviderStoreFactory(ProviderModule providerModule, ew2<HelpCenterProvider> ew2Var, ew2<RequestProvider> ew2Var2, ew2<UploadProvider> ew2Var3) {
        this.module = providerModule;
        this.helpCenterProvider = ew2Var;
        this.requestProvider = ew2Var2;
        this.uploadProvider = ew2Var3;
    }

    public static ProviderModule_ProvideProviderStoreFactory create(ProviderModule providerModule, ew2<HelpCenterProvider> ew2Var, ew2<RequestProvider> ew2Var2, ew2<UploadProvider> ew2Var3) {
        return new ProviderModule_ProvideProviderStoreFactory(providerModule, ew2Var, ew2Var2, ew2Var3);
    }

    public static ProviderStore provideProviderStore(ProviderModule providerModule, HelpCenterProvider helpCenterProvider, RequestProvider requestProvider, UploadProvider uploadProvider) {
        return (ProviderStore) cu2.f(providerModule.provideProviderStore(helpCenterProvider, requestProvider, uploadProvider));
    }

    @Override // defpackage.ew2
    public ProviderStore get() {
        return provideProviderStore(this.module, this.helpCenterProvider.get(), this.requestProvider.get(), this.uploadProvider.get());
    }
}
