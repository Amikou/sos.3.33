package zendesk.support;

import zendesk.core.ActionHandlerRegistry;
import zendesk.core.AuthenticationProvider;

/* loaded from: classes3.dex */
public final class Support_MembersInjector implements i72<Support> {
    private final ew2<ActionHandlerRegistry> actionHandlerRegistryProvider;
    private final ew2<AuthenticationProvider> authenticationProvider;
    private final ew2<SupportBlipsProvider> blipsProvider;
    private final ew2<ProviderStore> providerStoreProvider;
    private final ew2<RequestMigrator> requestMigratorProvider;
    private final ew2<RequestProvider> requestProvider;
    private final ew2<SupportModule> supportModuleProvider;

    public Support_MembersInjector(ew2<ProviderStore> ew2Var, ew2<SupportModule> ew2Var2, ew2<RequestMigrator> ew2Var3, ew2<SupportBlipsProvider> ew2Var4, ew2<ActionHandlerRegistry> ew2Var5, ew2<RequestProvider> ew2Var6, ew2<AuthenticationProvider> ew2Var7) {
        this.providerStoreProvider = ew2Var;
        this.supportModuleProvider = ew2Var2;
        this.requestMigratorProvider = ew2Var3;
        this.blipsProvider = ew2Var4;
        this.actionHandlerRegistryProvider = ew2Var5;
        this.requestProvider = ew2Var6;
        this.authenticationProvider = ew2Var7;
    }

    public static i72<Support> create(ew2<ProviderStore> ew2Var, ew2<SupportModule> ew2Var2, ew2<RequestMigrator> ew2Var3, ew2<SupportBlipsProvider> ew2Var4, ew2<ActionHandlerRegistry> ew2Var5, ew2<RequestProvider> ew2Var6, ew2<AuthenticationProvider> ew2Var7) {
        return new Support_MembersInjector(ew2Var, ew2Var2, ew2Var3, ew2Var4, ew2Var5, ew2Var6, ew2Var7);
    }

    public static void injectActionHandlerRegistry(Support support, ActionHandlerRegistry actionHandlerRegistry) {
        support.actionHandlerRegistry = actionHandlerRegistry;
    }

    public static void injectAuthenticationProvider(Support support, AuthenticationProvider authenticationProvider) {
        support.authenticationProvider = authenticationProvider;
    }

    public static void injectBlipsProvider(Support support, SupportBlipsProvider supportBlipsProvider) {
        support.blipsProvider = supportBlipsProvider;
    }

    public static void injectProviderStore(Support support, ProviderStore providerStore) {
        support.providerStore = providerStore;
    }

    public static void injectRequestMigrator(Support support, Object obj) {
        support.requestMigrator = (RequestMigrator) obj;
    }

    public static void injectRequestProvider(Support support, RequestProvider requestProvider) {
        support.requestProvider = requestProvider;
    }

    public static void injectSupportModule(Support support, SupportModule supportModule) {
        support.supportModule = supportModule;
    }

    public void injectMembers(Support support) {
        injectProviderStore(support, this.providerStoreProvider.get());
        injectSupportModule(support, this.supportModuleProvider.get());
        injectRequestMigrator(support, this.requestMigratorProvider.get());
        injectBlipsProvider(support, this.blipsProvider.get());
        injectActionHandlerRegistry(support, this.actionHandlerRegistryProvider.get());
        injectRequestProvider(support, this.requestProvider.get());
        injectAuthenticationProvider(support, this.authenticationProvider.get());
    }
}
