package zendesk.support;

import java.util.Locale;

/* loaded from: classes3.dex */
public final class GuideProviderModule_ProvideDeviceLocaleFactory implements y11<Locale> {
    private final GuideProviderModule module;

    public GuideProviderModule_ProvideDeviceLocaleFactory(GuideProviderModule guideProviderModule) {
        this.module = guideProviderModule;
    }

    public static GuideProviderModule_ProvideDeviceLocaleFactory create(GuideProviderModule guideProviderModule) {
        return new GuideProviderModule_ProvideDeviceLocaleFactory(guideProviderModule);
    }

    public static Locale provideDeviceLocale(GuideProviderModule guideProviderModule) {
        return (Locale) cu2.f(guideProviderModule.provideDeviceLocale());
    }

    @Override // defpackage.ew2
    public Locale get() {
        return provideDeviceLocale(this.module);
    }
}
