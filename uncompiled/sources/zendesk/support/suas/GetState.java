package zendesk.support.suas;

/* loaded from: classes3.dex */
public interface GetState {
    State getState();
}
