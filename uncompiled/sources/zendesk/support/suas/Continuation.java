package zendesk.support.suas;

/* loaded from: classes3.dex */
public interface Continuation {
    void next(Action<?> action);
}
