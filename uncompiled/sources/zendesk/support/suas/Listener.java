package zendesk.support.suas;

/* loaded from: classes3.dex */
public interface Listener<E> {
    void update(E e);
}
