package zendesk.support.suas;

/* loaded from: classes3.dex */
public interface Filter<E> {
    boolean filter(E e, E e2);
}
