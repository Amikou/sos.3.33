package zendesk.support.suas;

/* loaded from: classes3.dex */
public interface Subscription {
    void addListener();

    void informWithCurrentState();

    void removeListener();
}
