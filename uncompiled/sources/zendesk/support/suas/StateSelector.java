package zendesk.support.suas;

/* loaded from: classes3.dex */
public interface StateSelector<E> {
    E selectData(State state);
}
