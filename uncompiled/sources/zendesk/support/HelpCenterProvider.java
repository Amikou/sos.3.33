package zendesk.support;

import java.util.List;
import java.util.Locale;

/* loaded from: classes3.dex */
public interface HelpCenterProvider {
    void deleteVote(Long l, rs4<Void> rs4Var);

    void downvoteArticle(Long l, rs4<ArticleVote> rs4Var);

    void getArticle(Long l, rs4<Article> rs4Var);

    void getArticles(Long l, String str, rs4<List<Article>> rs4Var);

    void getArticles(Long l, rs4<List<Article>> rs4Var);

    void getAttachments(Long l, AttachmentType attachmentType, rs4<List<HelpCenterAttachment>> rs4Var);

    void getCategories(rs4<List<Category>> rs4Var);

    void getCategory(Long l, rs4<Category> rs4Var);

    void getHelp(HelpRequest helpRequest, rs4<List<HelpItem>> rs4Var);

    void getSection(Long l, rs4<Section> rs4Var);

    void getSections(Long l, rs4<List<Section>> rs4Var);

    void getSuggestedArticles(SuggestedArticleSearch suggestedArticleSearch, rs4<Object> rs4Var);

    void listArticles(ListArticleQuery listArticleQuery, rs4<List<SearchArticle>> rs4Var);

    void listArticlesFlat(ListArticleQuery listArticleQuery, rs4<List<FlatArticle>> rs4Var);

    void searchArticles(HelpCenterSearch helpCenterSearch, rs4<List<SearchArticle>> rs4Var);

    void submitRecordArticleView(Article article, Locale locale, rs4<Void> rs4Var);

    void upvoteArticle(Long l, rs4<ArticleVote> rs4Var);
}
