package zendesk.support;

import zendesk.core.SessionStorage;

/* loaded from: classes3.dex */
public final class SupportSdkModule_ProvidesRequestDiskLruCacheFactory implements y11<ep0> {
    private final SupportSdkModule module;
    private final ew2<SessionStorage> sessionStorageProvider;

    public SupportSdkModule_ProvidesRequestDiskLruCacheFactory(SupportSdkModule supportSdkModule, ew2<SessionStorage> ew2Var) {
        this.module = supportSdkModule;
        this.sessionStorageProvider = ew2Var;
    }

    public static SupportSdkModule_ProvidesRequestDiskLruCacheFactory create(SupportSdkModule supportSdkModule, ew2<SessionStorage> ew2Var) {
        return new SupportSdkModule_ProvidesRequestDiskLruCacheFactory(supportSdkModule, ew2Var);
    }

    public static ep0 providesRequestDiskLruCache(SupportSdkModule supportSdkModule, SessionStorage sessionStorage) {
        return (ep0) cu2.f(supportSdkModule.providesRequestDiskLruCache(sessionStorage));
    }

    @Override // defpackage.ew2
    public ep0 get() {
        return providesRequestDiskLruCache(this.module, this.sessionStorageProvider.get());
    }
}
