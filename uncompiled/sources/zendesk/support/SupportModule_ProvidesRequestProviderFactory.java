package zendesk.support;

/* loaded from: classes3.dex */
public final class SupportModule_ProvidesRequestProviderFactory implements y11<RequestProvider> {
    private final SupportModule module;

    public SupportModule_ProvidesRequestProviderFactory(SupportModule supportModule) {
        this.module = supportModule;
    }

    public static SupportModule_ProvidesRequestProviderFactory create(SupportModule supportModule) {
        return new SupportModule_ProvidesRequestProviderFactory(supportModule);
    }

    public static RequestProvider providesRequestProvider(SupportModule supportModule) {
        return (RequestProvider) cu2.f(supportModule.providesRequestProvider());
    }

    @Override // defpackage.ew2
    public RequestProvider get() {
        return providesRequestProvider(this.module);
    }
}
