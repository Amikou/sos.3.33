package zendesk.support;

/* loaded from: classes3.dex */
public final class GuideModule_ProvidesArticleVoteStorageFactory implements y11<ArticleVoteStorage> {
    private final GuideModule module;

    public GuideModule_ProvidesArticleVoteStorageFactory(GuideModule guideModule) {
        this.module = guideModule;
    }

    public static GuideModule_ProvidesArticleVoteStorageFactory create(GuideModule guideModule) {
        return new GuideModule_ProvidesArticleVoteStorageFactory(guideModule);
    }

    public static ArticleVoteStorage providesArticleVoteStorage(GuideModule guideModule) {
        return (ArticleVoteStorage) cu2.f(guideModule.providesArticleVoteStorage());
    }

    @Override // defpackage.ew2
    public ArticleVoteStorage get() {
        return providesArticleVoteStorage(this.module);
    }
}
