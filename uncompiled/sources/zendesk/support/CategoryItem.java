package zendesk.support;

import com.google.gson.annotations.SerializedName;
import java.util.List;
import org.web3j.ens.contracts.generated.PublicResolver;

/* loaded from: classes3.dex */
public class CategoryItem implements HelpItem {
    private boolean expanded = true;
    @SerializedName("id")
    private Long id;
    @SerializedName(PublicResolver.FUNC_NAME)
    private String name;
    @SerializedName("section_count")
    private int sectionCount;
    private List<SectionItem> sections;

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null || getClass() != obj.getClass()) {
            return false;
        }
        Long l = this.id;
        Long l2 = ((CategoryItem) obj).id;
        return l != null ? l.equals(l2) : l2 == null;
    }

    @Override // zendesk.support.HelpItem
    public Long getId() {
        return this.id;
    }

    @Override // zendesk.support.HelpItem
    public String getName() {
        String str = this.name;
        return str == null ? "" : str;
    }

    @Override // zendesk.support.HelpItem
    public Long getParentId() {
        return null;
    }

    public List<SectionItem> getSections() {
        return l10.c(this.sections);
    }

    @Override // zendesk.support.HelpItem
    public int getViewType() {
        return 1;
    }

    public int hashCode() {
        Long l = this.id;
        if (l != null) {
            return l.hashCode();
        }
        return 0;
    }

    public boolean isExpanded() {
        return this.expanded;
    }

    public boolean setExpanded(boolean z) {
        this.expanded = z;
        return z;
    }

    public void setSections(List<SectionItem> list) {
        this.sections = l10.c(list);
    }
}
