package zendesk.support;

import java.io.File;

/* loaded from: classes3.dex */
class ZendeskUploadProvider implements UploadProvider {
    private final ZendeskUploadService uploadService;

    public ZendeskUploadProvider(ZendeskUploadService zendeskUploadService) {
        this.uploadService = zendeskUploadService;
    }

    @Override // zendesk.support.UploadProvider
    public void deleteAttachment(String str, rs4<Void> rs4Var) {
        this.uploadService.deleteAttachment(str, rs4Var);
    }

    @Override // zendesk.support.UploadProvider
    public void uploadAttachment(String str, File file, String str2, final rs4<UploadResponse> rs4Var) {
        this.uploadService.uploadAttachment(str, file, str2, new ZendeskCallbackSuccess<UploadResponseWrapper>(rs4Var) { // from class: zendesk.support.ZendeskUploadProvider.1
            @Override // zendesk.support.ZendeskCallbackSuccess, defpackage.rs4
            public void onSuccess(UploadResponseWrapper uploadResponseWrapper) {
                rs4 rs4Var2 = rs4Var;
                if (rs4Var2 != null) {
                    rs4Var2.onSuccess(uploadResponseWrapper.getUpload());
                }
            }
        });
    }
}
