package zendesk.support;

import java.util.Locale;
import zendesk.core.BlipsProvider;
import zendesk.core.CoreModule;
import zendesk.core.CoreModule_GetBlipsProviderFactory;
import zendesk.core.CoreModule_GetRestServiceProviderFactory;
import zendesk.core.CoreModule_GetSessionStorageFactory;
import zendesk.core.CoreModule_GetSettingsProviderFactory;
import zendesk.core.RestServiceProvider;
import zendesk.core.SessionStorage;
import zendesk.core.SettingsProvider;
import zendesk.core.ZendeskLocaleConverter;

/* loaded from: classes3.dex */
public final class DaggerGuideSdkProvidersComponent implements GuideSdkProvidersComponent {
    private ew2<BlipsProvider> getBlipsProvider;
    private ew2<RestServiceProvider> getRestServiceProvider;
    private ew2<SessionStorage> getSessionStorageProvider;
    private ew2<SettingsProvider> getSettingsProvider;
    private final DaggerGuideSdkProvidersComponent guideSdkProvidersComponent;
    private ew2<ArticleVoteStorage> provideArticleVoteStorageProvider;
    private ew2<HelpCenterCachingNetworkConfig> provideCustomNetworkConfigProvider;
    private ew2<Locale> provideDeviceLocaleProvider;
    private ew2<GuideModule> provideGuideModuleProvider;
    private ew2<HelpCenterCachingInterceptor> provideHelpCenterCachingInterceptorProvider;
    private ew2<HelpCenterProvider> provideHelpCenterProvider;
    private ew2<HelpCenterSessionCache> provideHelpCenterSessionCacheProvider;
    private ew2<HelpCenterSettingsProvider> provideSettingsProvider;
    private ew2<ZendeskHelpCenterService> provideZendeskHelpCenterServiceProvider;
    private ew2<ZendeskLocaleConverter> provideZendeskLocaleConverterProvider;
    private ew2<HelpCenterBlipsProvider> providesHelpCenterBlipsProvider;
    private ew2<HelpCenterService> providesHelpCenterServiceProvider;

    /* loaded from: classes3.dex */
    public static final class Builder {
        private CoreModule coreModule;
        private GuideProviderModule guideProviderModule;

        public GuideSdkProvidersComponent build() {
            cu2.a(this.coreModule, CoreModule.class);
            cu2.a(this.guideProviderModule, GuideProviderModule.class);
            return new DaggerGuideSdkProvidersComponent(this.coreModule, this.guideProviderModule);
        }

        public Builder coreModule(CoreModule coreModule) {
            this.coreModule = (CoreModule) cu2.b(coreModule);
            return this;
        }

        public Builder guideProviderModule(GuideProviderModule guideProviderModule) {
            this.guideProviderModule = (GuideProviderModule) cu2.b(guideProviderModule);
            return this;
        }

        private Builder() {
        }
    }

    public static Builder builder() {
        return new Builder();
    }

    private void initialize(CoreModule coreModule, GuideProviderModule guideProviderModule) {
        this.getSettingsProvider = CoreModule_GetSettingsProviderFactory.create(coreModule);
        this.provideZendeskLocaleConverterProvider = fq0.a(GuideProviderModule_ProvideZendeskLocaleConverterFactory.create());
        ew2<Locale> a = fq0.a(GuideProviderModule_ProvideDeviceLocaleFactory.create(guideProviderModule));
        this.provideDeviceLocaleProvider = a;
        this.provideSettingsProvider = fq0.a(GuideProviderModule_ProvideSettingsProviderFactory.create(guideProviderModule, this.getSettingsProvider, this.provideZendeskLocaleConverterProvider, a));
        CoreModule_GetBlipsProviderFactory create = CoreModule_GetBlipsProviderFactory.create(coreModule);
        this.getBlipsProvider = create;
        this.providesHelpCenterBlipsProvider = fq0.a(GuideProviderModule_ProvidesHelpCenterBlipsProviderFactory.create(guideProviderModule, create, this.provideDeviceLocaleProvider));
        this.getRestServiceProvider = CoreModule_GetRestServiceProviderFactory.create(coreModule);
        ew2<HelpCenterCachingInterceptor> a2 = tp3.a(GuideProviderModule_ProvideHelpCenterCachingInterceptorFactory.create());
        this.provideHelpCenterCachingInterceptorProvider = a2;
        ew2<HelpCenterCachingNetworkConfig> a3 = tp3.a(GuideProviderModule_ProvideCustomNetworkConfigFactory.create(a2));
        this.provideCustomNetworkConfigProvider = a3;
        ew2<HelpCenterService> a4 = fq0.a(GuideProviderModule_ProvidesHelpCenterServiceFactory.create(this.getRestServiceProvider, a3));
        this.providesHelpCenterServiceProvider = a4;
        this.provideZendeskHelpCenterServiceProvider = fq0.a(GuideProviderModule_ProvideZendeskHelpCenterServiceFactory.create(a4, this.provideZendeskLocaleConverterProvider));
        ew2<HelpCenterSessionCache> a5 = fq0.a(GuideProviderModule_ProvideHelpCenterSessionCacheFactory.create());
        this.provideHelpCenterSessionCacheProvider = a5;
        this.provideHelpCenterProvider = fq0.a(GuideProviderModule_ProvideHelpCenterProviderFactory.create(guideProviderModule, this.provideSettingsProvider, this.providesHelpCenterBlipsProvider, this.provideZendeskHelpCenterServiceProvider, a5));
        CoreModule_GetSessionStorageFactory create2 = CoreModule_GetSessionStorageFactory.create(coreModule);
        this.getSessionStorageProvider = create2;
        ew2<ArticleVoteStorage> a6 = fq0.a(GuideProviderModule_ProvideArticleVoteStorageFactory.create(create2));
        this.provideArticleVoteStorageProvider = a6;
        this.provideGuideModuleProvider = fq0.a(GuideProviderModule_ProvideGuideModuleFactory.create(guideProviderModule, this.provideHelpCenterProvider, this.provideSettingsProvider, this.providesHelpCenterBlipsProvider, a6, this.getRestServiceProvider));
    }

    private Guide injectGuide(Guide guide) {
        Guide_MembersInjector.injectGuideModule(guide, this.provideGuideModuleProvider.get());
        Guide_MembersInjector.injectBlipsProvider(guide, this.providesHelpCenterBlipsProvider.get());
        return guide;
    }

    @Override // zendesk.support.GuideSdkProvidersComponent
    public Guide inject(Guide guide) {
        return injectGuide(guide);
    }

    private DaggerGuideSdkProvidersComponent(CoreModule coreModule, GuideProviderModule guideProviderModule) {
        this.guideSdkProvidersComponent = this;
        initialize(coreModule, guideProviderModule);
    }
}
