package zendesk.support;

import com.zendesk.logger.Logger;
import defpackage.w83;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;
import zendesk.core.ZendeskLocaleConverter;

/* loaded from: classes3.dex */
public class ZendeskHelpCenterService {
    private static final String LOG_TAG = "ZendeskHelpCenterService";
    private static final int NUMBER_PER_PAGE = 1000;
    private final HelpCenterService helpCenterService;
    private final ZendeskLocaleConverter localeConverter;

    public ZendeskHelpCenterService(HelpCenterService helpCenterService, ZendeskLocaleConverter zendeskLocaleConverter) {
        this.helpCenterService = helpCenterService;
        this.localeConverter = zendeskLocaleConverter;
    }

    public void deleteVote(Long l, rs4<Void> rs4Var) {
        if (l == null) {
            Logger.e(LOG_TAG, "The vote id was null, can not delete the vote", new Object[0]);
            if (rs4Var != null) {
                rs4Var.onError(new dw0("The vote id was null, can not delete the vote"));
                return;
            }
            return;
        }
        this.helpCenterService.deleteVote(l).n(new w83(rs4Var));
    }

    public void downvoteArticle(Long l, String str, rs4<ArticleVoteResponse> rs4Var) {
        if (l == null) {
            Logger.e(LOG_TAG, "The article id was null, can not create down vote", new Object[0]);
            if (rs4Var != null) {
                rs4Var.onError(new dw0("The article id was null, can not create down vote"));
                return;
            }
            return;
        }
        this.helpCenterService.downvoteArticle(l, str).n(new w83(rs4Var));
    }

    public void getArticle(Long l, Locale locale, String str, rs4<Article> rs4Var) {
        String helpCenterLocaleString = this.localeConverter.toHelpCenterLocaleString(locale);
        this.helpCenterService.getArticle(helpCenterLocaleString, l, str).n(new w83(rs4Var, new w83.b<ArticleResponse, Article>() { // from class: zendesk.support.ZendeskHelpCenterService.4
            @Override // defpackage.w83.b
            public Article extract(ArticleResponse articleResponse) {
                return ZendeskHelpCenterService.this.matchArticleWithUsers(articleResponse.getArticle(), l10.e(articleResponse.getUsers()));
            }
        }));
    }

    public void getArticlesForSection(Long l, Locale locale, String str, String str2, rs4<List<Article>> rs4Var) {
        this.helpCenterService.getArticles(this.localeConverter.toHelpCenterLocaleString(locale), l, str2, str, 1000).n(new w83(rs4Var, new w83.b<ArticlesListResponse, List<Article>>() { // from class: zendesk.support.ZendeskHelpCenterService.3
            @Override // defpackage.w83.b
            public List<Article> extract(ArticlesListResponse articlesListResponse) {
                return ZendeskHelpCenterService.this.matchArticlesWithUsers(articlesListResponse.getUsers(), articlesListResponse.getArticles());
            }
        }));
    }

    public void getAttachments(Locale locale, Long l, AttachmentType attachmentType, rs4<List<HelpCenterAttachment>> rs4Var) {
        if (attachmentType == null) {
            Logger.e(LOG_TAG, "getAttachments() was called with null attachment type", new Object[0]);
            if (rs4Var != null) {
                rs4Var.onError(new dw0("getAttachments() was called with null attachment type"));
                return;
            }
            return;
        }
        this.helpCenterService.getAttachments(this.localeConverter.toHelpCenterLocaleString(locale), l, attachmentType.getAttachmentType()).n(new w83(rs4Var, new w83.b<AttachmentResponse, List<HelpCenterAttachment>>() { // from class: zendesk.support.ZendeskHelpCenterService.7
            @Override // defpackage.w83.b
            public List<HelpCenterAttachment> extract(AttachmentResponse attachmentResponse) {
                return attachmentResponse.getArticleAttachments();
            }
        }));
    }

    public void getCategories(Locale locale, rs4<List<Category>> rs4Var) {
        this.helpCenterService.getCategories(this.localeConverter.toHelpCenterLocaleString(locale)).n(new w83(rs4Var, new w83.b<CategoriesResponse, List<Category>>() { // from class: zendesk.support.ZendeskHelpCenterService.1
            @Override // defpackage.w83.b
            public List<Category> extract(CategoriesResponse categoriesResponse) {
                return categoriesResponse.getCategories();
            }
        }));
    }

    public void getCategoryById(Long l, Locale locale, rs4<Category> rs4Var) {
        this.helpCenterService.getCategoryById(this.localeConverter.toHelpCenterLocaleString(locale), l).n(new w83(rs4Var, new w83.b<CategoryResponse, Category>() { // from class: zendesk.support.ZendeskHelpCenterService.6
            @Override // defpackage.w83.b
            public Category extract(CategoryResponse categoryResponse) {
                return categoryResponse.getCategory();
            }
        }));
    }

    public void getHelp(Locale locale, String str, String str2, String str3, int i, String str4, rs4<HelpResponse> rs4Var) {
        this.helpCenterService.getHelp(this.localeConverter.toHelpCenterLocaleString(locale), str, str2, str3, i, str4, 1000, SortBy.CREATED_AT.getApiValue(), SortOrder.DESCENDING.getApiValue()).n(new w83(rs4Var));
    }

    public void getSectionById(Long l, Locale locale, rs4<Section> rs4Var) {
        this.helpCenterService.getSectionById(this.localeConverter.toHelpCenterLocaleString(locale), l).n(new w83(rs4Var, new w83.b<SectionResponse, Section>() { // from class: zendesk.support.ZendeskHelpCenterService.5
            @Override // defpackage.w83.b
            public Section extract(SectionResponse sectionResponse) {
                return sectionResponse.getSection();
            }
        }));
    }

    public void getSectionsForCategory(Long l, Locale locale, rs4<List<Section>> rs4Var) {
        this.helpCenterService.getSections(this.localeConverter.toHelpCenterLocaleString(locale), l, 1000).n(new w83(rs4Var, new w83.b<SectionsResponse, List<Section>>() { // from class: zendesk.support.ZendeskHelpCenterService.2
            @Override // defpackage.w83.b
            public List<Section> extract(SectionsResponse sectionsResponse) {
                return sectionsResponse.getSections();
            }
        }));
    }

    public void getSuggestedArticles(String str, Locale locale, String str2, Long l, Long l2, rs4<Object> rs4Var) {
        String helpCenterLocaleString = this.localeConverter.toHelpCenterLocaleString(locale);
        this.helpCenterService.getSuggestedArticles(str, helpCenterLocaleString, str2, l, l2).n(new w83(rs4Var));
    }

    public void listArticles(String str, Locale locale, String str2, String str3, String str4, Integer num, Integer num2, rs4<ArticlesListResponse> rs4Var) {
        this.helpCenterService.listArticles(this.localeConverter.toHelpCenterLocaleString(locale), str, str2, str3, str4, num, num2).n(new w83(rs4Var));
    }

    public Article matchArticleWithUsers(Article article, List<zendesk.core.User> list) {
        if (article == null) {
            return new Article();
        }
        Iterator<zendesk.core.User> it = list.iterator();
        while (true) {
            if (!it.hasNext()) {
                break;
            }
            zendesk.core.User next = it.next();
            if (next.getId() != null && next.getId().equals(article.getAuthorId())) {
                article.setAuthor(next);
                break;
            }
        }
        return article;
    }

    public List<Article> matchArticlesWithUsers(List<zendesk.core.User> list, List<Article> list2) {
        HashMap hashMap = new HashMap();
        for (zendesk.core.User user : list) {
            hashMap.put(user.getId(), user);
        }
        ArrayList arrayList = new ArrayList();
        for (Article article : list2) {
            zendesk.core.User user2 = (zendesk.core.User) hashMap.get(article.getAuthorId());
            if (user2 != null) {
                article.setAuthor(user2);
            }
            arrayList.add(article);
        }
        return arrayList;
    }

    public void searchArticles(String str, Locale locale, String str2, String str3, String str4, String str5, Integer num, Integer num2, rs4<ArticlesSearchResponse> rs4Var) {
        this.helpCenterService.searchArticles(str, this.localeConverter.toHelpCenterLocaleString(locale), str2, str3, str4, str5, num, num2).n(new w83(rs4Var));
    }

    public void submitRecordArticleView(Long l, Locale locale, RecordArticleViewRequest recordArticleViewRequest, rs4<Void> rs4Var) {
        this.helpCenterService.submitRecordArticleView(l, this.localeConverter.toHelpCenterLocaleString(locale), recordArticleViewRequest).n(new w83(rs4Var));
    }

    public void upvoteArticle(Long l, String str, rs4<ArticleVoteResponse> rs4Var) {
        if (l == null) {
            Logger.e(LOG_TAG, "The article id was null, can not create up vote", new Object[0]);
            if (rs4Var != null) {
                rs4Var.onError(new dw0("The article id was null, can not create up vote"));
                return;
            }
            return;
        }
        this.helpCenterService.upvoteArticle(l, str).n(new w83(rs4Var));
    }
}
