package zendesk.support;

import okhttp3.OkHttpClient;

/* loaded from: classes3.dex */
public final class SupportModule_ProvidesOkHttpClientFactory implements y11<OkHttpClient> {
    private final SupportModule module;

    public SupportModule_ProvidesOkHttpClientFactory(SupportModule supportModule) {
        this.module = supportModule;
    }

    public static SupportModule_ProvidesOkHttpClientFactory create(SupportModule supportModule) {
        return new SupportModule_ProvidesOkHttpClientFactory(supportModule);
    }

    public static OkHttpClient providesOkHttpClient(SupportModule supportModule) {
        return (OkHttpClient) cu2.f(supportModule.providesOkHttpClient());
    }

    @Override // defpackage.ew2
    public OkHttpClient get() {
        return providesOkHttpClient(this.module);
    }
}
