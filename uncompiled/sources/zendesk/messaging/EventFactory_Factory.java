package zendesk.messaging;

import zendesk.messaging.components.DateProvider;

/* loaded from: classes3.dex */
public final class EventFactory_Factory implements y11<EventFactory> {
    public final ew2<DateProvider> dateProvider;

    public EventFactory_Factory(ew2<DateProvider> ew2Var) {
        this.dateProvider = ew2Var;
    }

    public static EventFactory_Factory create(ew2<DateProvider> ew2Var) {
        return new EventFactory_Factory(ew2Var);
    }

    public static EventFactory newInstance(DateProvider dateProvider) {
        return new EventFactory(dateProvider);
    }

    @Override // defpackage.ew2
    public EventFactory get() {
        return newInstance(this.dateProvider.get());
    }
}
