package zendesk.messaging;

import android.content.Context;

/* loaded from: classes3.dex */
public final class MessagingEventSerializer_Factory implements y11<MessagingEventSerializer> {
    public final ew2<Context> contextProvider;
    public final ew2<TimestampFactory> timestampFactoryProvider;

    public MessagingEventSerializer_Factory(ew2<Context> ew2Var, ew2<TimestampFactory> ew2Var2) {
        this.contextProvider = ew2Var;
        this.timestampFactoryProvider = ew2Var2;
    }

    public static MessagingEventSerializer_Factory create(ew2<Context> ew2Var, ew2<TimestampFactory> ew2Var2) {
        return new MessagingEventSerializer_Factory(ew2Var, ew2Var2);
    }

    public static MessagingEventSerializer newInstance(Context context, Object obj) {
        return new MessagingEventSerializer(context, (TimestampFactory) obj);
    }

    @Override // defpackage.ew2
    public MessagingEventSerializer get() {
        return newInstance(this.contextProvider.get(), this.timestampFactoryProvider.get());
    }
}
