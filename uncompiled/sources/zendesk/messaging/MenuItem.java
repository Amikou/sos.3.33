package zendesk.messaging;

/* loaded from: classes3.dex */
public class MenuItem {
    public final int itemId;
    public final int labelId;

    public int getItemId() {
        return this.itemId;
    }

    public int getLabelId() {
        return this.labelId;
    }
}
