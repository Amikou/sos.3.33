package zendesk.messaging;

import android.content.Context;
import com.squareup.picasso.Picasso;

/* loaded from: classes3.dex */
public final class MessagingModule_PicassoFactory implements y11<Picasso> {
    public final ew2<Context> contextProvider;

    public MessagingModule_PicassoFactory(ew2<Context> ew2Var) {
        this.contextProvider = ew2Var;
    }

    public static MessagingModule_PicassoFactory create(ew2<Context> ew2Var) {
        return new MessagingModule_PicassoFactory(ew2Var);
    }

    public static Picasso picasso(Context context) {
        return (Picasso) cu2.f(MessagingModule.picasso(context));
    }

    @Override // defpackage.ew2
    public Picasso get() {
        return picasso(this.contextProvider.get());
    }
}
