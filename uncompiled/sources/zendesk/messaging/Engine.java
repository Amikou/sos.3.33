package zendesk.messaging;

/* loaded from: classes3.dex */
public interface Engine {

    /* loaded from: classes3.dex */
    public interface ConversationOnGoingCallback {
    }

    /* loaded from: classes3.dex */
    public static class TransferOptionDescription {
        public final String displayName;
        public final String engineId;

        public String getDisplayName() {
            return this.displayName;
        }

        public String getEngineId() {
            return this.engineId;
        }
    }

    /* loaded from: classes3.dex */
    public interface UpdateObserver {
    }

    String getId();

    void isConversationOngoing(ConversationOnGoingCallback conversationOnGoingCallback);

    void onEvent(Event event);

    boolean registerObserver(UpdateObserver updateObserver);

    void start(MessagingApi messagingApi);

    void stop();

    boolean unregisterObserver(UpdateObserver updateObserver);
}
