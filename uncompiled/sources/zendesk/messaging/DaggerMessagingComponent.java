package zendesk.messaging;

import android.content.Context;
import android.content.res.Resources;
import com.squareup.picasso.Picasso;
import java.util.List;
import zendesk.belvedere.a;
import zendesk.messaging.MessagingComponent;

/* loaded from: classes3.dex */
public final class DaggerMessagingComponent implements MessagingComponent {
    public ew2<Context> appContextProvider;
    public ew2<BelvedereMediaHolder> belvedereMediaHolderProvider;
    public ew2<a> belvedereProvider;
    public ew2<List<Engine>> enginesProvider;
    public final DaggerMessagingComponent messagingComponent;
    public final MessagingConfiguration messagingConfiguration;
    public ew2<MessagingConfiguration> messagingConfigurationProvider;
    public ew2<MessagingConversationLog> messagingConversationLogProvider;
    public ew2<MessagingEventSerializer> messagingEventSerializerProvider;
    public ew2<MessagingModel> messagingModelProvider;
    public ew2<MessagingViewModel> messagingViewModelProvider;
    public ew2<Picasso> picassoProvider;
    public ew2<Resources> resourcesProvider;
    public ew2<TimestampFactory> timestampFactoryProvider;

    /* loaded from: classes3.dex */
    public static final class Builder implements MessagingComponent.Builder {
        public Context appContext;
        public List<Engine> engines;
        public MessagingConfiguration messagingConfiguration;

        public Builder() {
        }

        @Override // zendesk.messaging.MessagingComponent.Builder
        public MessagingComponent build() {
            cu2.a(this.appContext, Context.class);
            cu2.a(this.engines, List.class);
            cu2.a(this.messagingConfiguration, MessagingConfiguration.class);
            return new DaggerMessagingComponent(this.appContext, this.engines, this.messagingConfiguration);
        }

        @Override // zendesk.messaging.MessagingComponent.Builder
        public /* bridge */ /* synthetic */ MessagingComponent.Builder engines(List list) {
            return engines((List<Engine>) list);
        }

        @Override // zendesk.messaging.MessagingComponent.Builder
        public Builder appContext(Context context) {
            this.appContext = (Context) cu2.b(context);
            return this;
        }

        @Override // zendesk.messaging.MessagingComponent.Builder
        public Builder engines(List<Engine> list) {
            this.engines = (List) cu2.b(list);
            return this;
        }

        @Override // zendesk.messaging.MessagingComponent.Builder
        public Builder messagingConfiguration(MessagingConfiguration messagingConfiguration) {
            this.messagingConfiguration = (MessagingConfiguration) cu2.b(messagingConfiguration);
            return this;
        }
    }

    public static MessagingComponent.Builder builder() {
        return new Builder();
    }

    @Override // zendesk.messaging.MessagingComponent
    public a belvedere() {
        return this.belvedereProvider.get();
    }

    @Override // zendesk.messaging.MessagingComponent
    public BelvedereMediaHolder belvedereMediaHolder() {
        return this.belvedereMediaHolderProvider.get();
    }

    public final void initialize(Context context, List<Engine> list, MessagingConfiguration messagingConfiguration) {
        y11 a = fr1.a(context);
        this.appContextProvider = a;
        this.picassoProvider = fq0.a(MessagingModule_PicassoFactory.create(a));
        this.resourcesProvider = fq0.a(MessagingModule_ResourcesFactory.create(this.appContextProvider));
        this.enginesProvider = fr1.a(list);
        this.messagingConfigurationProvider = fr1.a(messagingConfiguration);
        TimestampFactory_Factory create = TimestampFactory_Factory.create(this.appContextProvider);
        this.timestampFactoryProvider = create;
        ew2<MessagingEventSerializer> a2 = fq0.a(MessagingEventSerializer_Factory.create(this.appContextProvider, create));
        this.messagingEventSerializerProvider = a2;
        ew2<MessagingConversationLog> a3 = fq0.a(MessagingConversationLog_Factory.create(a2));
        this.messagingConversationLogProvider = a3;
        ew2<MessagingModel> a4 = fq0.a(MessagingModel_Factory.create(this.resourcesProvider, this.enginesProvider, this.messagingConfigurationProvider, a3));
        this.messagingModelProvider = a4;
        this.messagingViewModelProvider = fq0.a(MessagingViewModel_Factory.create(a4));
        this.belvedereProvider = fq0.a(MessagingModule_BelvedereFactory.create(this.appContextProvider));
        this.belvedereMediaHolderProvider = fq0.a(BelvedereMediaHolder_Factory.create());
    }

    @Override // zendesk.messaging.MessagingComponent
    public MessagingConfiguration messagingConfiguration() {
        return this.messagingConfiguration;
    }

    @Override // zendesk.messaging.MessagingComponent
    public MessagingViewModel messagingViewModel() {
        return this.messagingViewModelProvider.get();
    }

    @Override // zendesk.messaging.MessagingComponent
    public Picasso picasso() {
        return this.picassoProvider.get();
    }

    @Override // zendesk.messaging.MessagingComponent
    public Resources resources() {
        return this.resourcesProvider.get();
    }

    public DaggerMessagingComponent(Context context, List<Engine> list, MessagingConfiguration messagingConfiguration) {
        this.messagingComponent = this;
        this.messagingConfiguration = messagingConfiguration;
        initialize(context, list, messagingConfiguration);
    }
}
