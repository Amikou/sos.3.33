package zendesk.messaging;

/* loaded from: classes3.dex */
public class Typing {
    public final AgentDetails agentDetails;
    public final boolean isTyping;

    public Typing(boolean z) {
        this(z, null);
    }

    public AgentDetails getAgentDetails() {
        return this.agentDetails;
    }

    public boolean isTyping() {
        return this.isTyping;
    }

    public Typing(boolean z, AgentDetails agentDetails) {
        this.isTyping = z;
        this.agentDetails = agentDetails;
    }
}
