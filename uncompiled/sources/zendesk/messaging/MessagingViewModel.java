package zendesk.messaging;

import androidx.lifecycle.LiveData;
import java.util.List;
import zendesk.messaging.Update;
import zendesk.messaging.ui.MessagingState;

/* loaded from: classes3.dex */
public class MessagingViewModel extends dj4 implements EventListener {
    public final g72<Banner> liveBannersState;
    public final g72<DialogContent> liveDialogState;
    public final g72<MessagingState> liveMessagingState;
    public final LiveData<Update.Action.Navigation> liveNavigationStream;
    public final MessagingModel messagingModel;

    public MessagingViewModel(MessagingModel messagingModel) {
        this.messagingModel = messagingModel;
        g72<MessagingState> g72Var = new g72<>();
        this.liveMessagingState = g72Var;
        this.liveNavigationStream = messagingModel.getLiveNavigationUpdates();
        g72Var.setValue(new MessagingState.Builder().withEnabled(true).build());
        g72<Banner> g72Var2 = new g72<>();
        this.liveBannersState = g72Var2;
        this.liveDialogState = new g72<>();
        g72Var.a(messagingModel.getLiveMessagingItems(), new tl2<List<MessagingItem>>() { // from class: zendesk.messaging.MessagingViewModel.1
            @Override // defpackage.tl2
            public void onChanged(List<MessagingItem> list) {
                MessagingViewModel.this.liveMessagingState.setValue(((MessagingState) MessagingViewModel.this.liveMessagingState.getValue()).newBuilder().withMessagingItems(list).build());
            }
        });
        g72Var.a(messagingModel.getLiveComposerEnabled(), new tl2<Boolean>() { // from class: zendesk.messaging.MessagingViewModel.2
            @Override // defpackage.tl2
            public void onChanged(Boolean bool) {
                MessagingViewModel.this.liveMessagingState.setValue(((MessagingState) MessagingViewModel.this.liveMessagingState.getValue()).newBuilder().withEnabled(bool.booleanValue()).build());
            }
        });
        g72Var.a(messagingModel.getLiveTyping(), new tl2<Typing>() { // from class: zendesk.messaging.MessagingViewModel.3
            @Override // defpackage.tl2
            public void onChanged(Typing typing) {
                MessagingViewModel.this.liveMessagingState.setValue(((MessagingState) MessagingViewModel.this.liveMessagingState.getValue()).newBuilder().withTypingIndicatorState(new MessagingState.TypingState(typing.isTyping(), typing.getAgentDetails())).build());
            }
        });
        g72Var.a(messagingModel.getLiveConnection(), new tl2<ConnectionState>() { // from class: zendesk.messaging.MessagingViewModel.4
            @Override // defpackage.tl2
            public void onChanged(ConnectionState connectionState) {
                MessagingViewModel.this.liveMessagingState.setValue(((MessagingState) MessagingViewModel.this.liveMessagingState.getValue()).newBuilder().withConnectionState(connectionState).build());
            }
        });
        g72Var.a(messagingModel.getLiveComposerHint(), new tl2<String>() { // from class: zendesk.messaging.MessagingViewModel.5
            @Override // defpackage.tl2
            public void onChanged(String str) {
                MessagingViewModel.this.liveMessagingState.setValue(((MessagingState) MessagingViewModel.this.liveMessagingState.getValue()).newBuilder().withComposerHint(str).build());
            }
        });
        g72Var.a(messagingModel.getLiveKeyboardInputType(), new tl2<Integer>() { // from class: zendesk.messaging.MessagingViewModel.6
            @Override // defpackage.tl2
            public void onChanged(Integer num) {
                MessagingViewModel.this.liveMessagingState.setValue(((MessagingState) MessagingViewModel.this.liveMessagingState.getValue()).newBuilder().withKeyboardInputType(num.intValue()).build());
            }
        });
        g72Var.a(messagingModel.getLiveAttachmentSettings(), new tl2<AttachmentSettings>() { // from class: zendesk.messaging.MessagingViewModel.7
            @Override // defpackage.tl2
            public void onChanged(AttachmentSettings attachmentSettings) {
                MessagingViewModel.this.liveMessagingState.setValue(((MessagingState) MessagingViewModel.this.liveMessagingState.getValue()).newBuilder().withAttachmentSettings(attachmentSettings).build());
            }
        });
        g72Var2.a(messagingModel.getLiveInterfaceUpdates(), new tl2<Banner>() { // from class: zendesk.messaging.MessagingViewModel.8
            @Override // defpackage.tl2
            public void onChanged(Banner banner) {
                MessagingViewModel.this.liveBannersState.setValue(banner);
            }
        });
    }

    public SingleLiveEvent<DialogContent> getDialogUpdates() {
        return this.messagingModel.getLiveDialogUpdates();
    }

    public SingleLiveEvent<Banner> getLiveInterfaceUpdateItems() {
        return this.messagingModel.getLiveInterfaceUpdates();
    }

    public LiveData<List<MenuItem>> getLiveMenuItems() {
        return this.messagingModel.getLiveMenuItems();
    }

    public LiveData<MessagingState> getLiveMessagingState() {
        return this.liveMessagingState;
    }

    public LiveData<Update.Action.Navigation> getLiveNavigationStream() {
        return this.liveNavigationStream;
    }

    @Override // defpackage.dj4
    public void onCleared() {
        this.messagingModel.stop();
    }

    @Override // zendesk.messaging.EventListener
    public void onEvent(Event event) {
        this.messagingModel.onEvent(event);
    }

    public void start() {
        this.messagingModel.start();
    }
}
