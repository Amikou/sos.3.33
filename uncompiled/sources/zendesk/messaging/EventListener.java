package zendesk.messaging;

/* loaded from: classes3.dex */
public interface EventListener {
    void onEvent(Event event);
}
