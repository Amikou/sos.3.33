package zendesk.messaging;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.content.res.Resources;
import java.util.ArrayList;
import java.util.List;
import zendesk.configurations.Configuration;

/* loaded from: classes3.dex */
public class MessagingConfiguration implements Configuration {
    private AgentDetails botAgentDetails;
    private final int botAvatarDrawable;
    private final String botLabelString;
    private final int botLabelStringRes;
    private final List<Configuration> configurations;
    private final String engineRegistryKey;
    private final boolean multilineResponseOptionsEnabled;
    private final String toolbarTitle;
    private final int toolbarTitleRes;

    /* loaded from: classes3.dex */
    public static class Builder {
        public String botLabelString;
        public String toolbarTitle;
        public List<Configuration> configurations = new ArrayList();
        public List<Engine> engines = new ArrayList();
        public int toolbarTitleRes = R$string.zui_toolbar_title;
        public int botLabelStringRes = R$string.zui_default_bot_name;
        public boolean multilineResponseOptionsEnabled = false;
        public int botAvatarDrawable = R$drawable.zui_avatar_bot_default;

        public Configuration config(Context context) {
            return new MessagingConfiguration(this, EngineListRegistry.INSTANCE.register(this.engines));
        }

        @SuppressLint({"RestrictedApi"})
        public Intent intent(Context context, List<Configuration> list) {
            this.configurations = list;
            Configuration config = config(context);
            Intent intent = new Intent(context, MessagingActivity.class);
            z40.h().c(intent, config);
            return intent;
        }

        public void show(Context context, List<Configuration> list) {
            context.startActivity(intent(context, list));
        }

        public Builder withEngines(List<Engine> list) {
            this.engines = list;
            return this;
        }
    }

    public AgentDetails getBotAgentDetails(Resources resources) {
        if (this.botAgentDetails == null) {
            this.botAgentDetails = new AgentDetails(getBotLabelString(resources), "ANSWER_BOT", true, Integer.valueOf(this.botAvatarDrawable));
        }
        return this.botAgentDetails;
    }

    public int getBotAvatarDrawable() {
        return this.botAvatarDrawable;
    }

    public final String getBotLabelString(Resources resources) {
        return ru3.b(this.botLabelString) ? this.botLabelString : resources.getString(this.botLabelStringRes);
    }

    @Override // zendesk.configurations.Configuration
    public List<Configuration> getConfigurations() {
        return z40.h().a(this.configurations, this);
    }

    public List<Engine> getEngines() {
        return EngineListRegistry.INSTANCE.retrieveEngineList(this.engineRegistryKey);
    }

    public String getToolbarTitle(Resources resources) {
        return ru3.b(this.toolbarTitle) ? this.toolbarTitle : resources.getString(this.toolbarTitleRes);
    }

    public boolean isMultilineResponseOptionsEnabled() {
        return this.multilineResponseOptionsEnabled;
    }

    public MessagingConfiguration(Builder builder, String str) {
        this.configurations = builder.configurations;
        this.engineRegistryKey = str;
        this.toolbarTitle = builder.toolbarTitle;
        this.toolbarTitleRes = builder.toolbarTitleRes;
        this.botLabelString = builder.botLabelString;
        this.botLabelStringRes = builder.botLabelStringRes;
        this.botAvatarDrawable = builder.botAvatarDrawable;
        this.multilineResponseOptionsEnabled = builder.multilineResponseOptionsEnabled;
    }
}
