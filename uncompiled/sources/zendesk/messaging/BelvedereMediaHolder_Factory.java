package zendesk.messaging;

/* loaded from: classes3.dex */
public final class BelvedereMediaHolder_Factory implements y11<BelvedereMediaHolder> {

    /* loaded from: classes3.dex */
    public static final class InstanceHolder {
        public static final BelvedereMediaHolder_Factory INSTANCE = new BelvedereMediaHolder_Factory();
    }

    public static BelvedereMediaHolder_Factory create() {
        return InstanceHolder.INSTANCE;
    }

    public static BelvedereMediaHolder newInstance() {
        return new BelvedereMediaHolder();
    }

    @Override // defpackage.ew2
    public BelvedereMediaHolder get() {
        return newInstance();
    }
}
