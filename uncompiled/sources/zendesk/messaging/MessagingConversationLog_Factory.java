package zendesk.messaging;

/* loaded from: classes3.dex */
public final class MessagingConversationLog_Factory implements y11<MessagingConversationLog> {
    public final ew2<MessagingEventSerializer> messagingEventSerializerProvider;

    public MessagingConversationLog_Factory(ew2<MessagingEventSerializer> ew2Var) {
        this.messagingEventSerializerProvider = ew2Var;
    }

    public static MessagingConversationLog_Factory create(ew2<MessagingEventSerializer> ew2Var) {
        return new MessagingConversationLog_Factory(ew2Var);
    }

    public static MessagingConversationLog newInstance(Object obj) {
        return new MessagingConversationLog((MessagingEventSerializer) obj);
    }

    @Override // defpackage.ew2
    public MessagingConversationLog get() {
        return newInstance(this.messagingEventSerializerProvider.get());
    }
}
