package zendesk.messaging;

import java.util.Date;
import java.util.List;
import zendesk.messaging.Engine;

/* loaded from: classes3.dex */
public abstract class MessagingItem implements MessagingEvent {
    public final String id;
    public final Date timestamp;

    /* loaded from: classes3.dex */
    public static class Action {
        public final String displayName;

        public String getDisplayName() {
            return this.displayName;
        }
    }

    /* loaded from: classes3.dex */
    public static class ActionResponse extends Response {
        public List<Action> actions;
        public final String message;

        public List<Action> getActions() {
            return this.actions;
        }

        public String getMessage() {
            return this.message;
        }
    }

    /* loaded from: classes3.dex */
    public static class ArticlesResponse extends Response {
        public final List<ArticleSuggestion> articleSuggestions;

        /* loaded from: classes3.dex */
        public static class ArticleSuggestion {
            public final String snippet;
            public final String title;

            public String getSnippet() {
                return this.snippet;
            }

            public String getTitle() {
                return this.title;
            }
        }

        public List<ArticleSuggestion> getArticleSuggestions() {
            return this.articleSuggestions;
        }
    }

    /* loaded from: classes3.dex */
    public static class FileQuery extends Query {
        public final Attachment attachment;
        public final FailureReason failureReason;

        /* loaded from: classes3.dex */
        public enum FailureReason {
            FILE_SIZE_TOO_LARGE,
            FILE_SENDING_DISABLED,
            UNSUPPORTED_FILE_TYPE
        }

        public Attachment getAttachment() {
            return this.attachment;
        }

        public FailureReason getFailureReason() {
            return this.failureReason;
        }
    }

    /* loaded from: classes3.dex */
    public static class FileResponse extends Response {
        public final Attachment attachment;

        public Attachment getAttachment() {
            return this.attachment;
        }
    }

    /* loaded from: classes3.dex */
    public static class ImageQuery extends FileQuery {
    }

    /* loaded from: classes3.dex */
    public static class ImageResponse extends FileResponse {
    }

    /* loaded from: classes3.dex */
    public static class Option {
        public final String id;
        public final String text;

        public boolean equals(Object obj) {
            if (this == obj) {
                return true;
            }
            if (obj == null || getClass() != obj.getClass()) {
                return false;
            }
            Option option = (Option) obj;
            if (this.id.equals(option.id)) {
                return this.text.equals(option.text);
            }
            return false;
        }

        public String getText() {
            return this.text;
        }

        public int hashCode() {
            return (this.id.hashCode() * 31) + this.text.hashCode();
        }
    }

    /* loaded from: classes3.dex */
    public static class OptionsResponse extends MessagingItem {
        public final List<Option> options;

        public List<Option> getOptions() {
            return this.options;
        }
    }

    /* loaded from: classes3.dex */
    public static abstract class Query extends MessagingItem {
        public final Status status;

        /* loaded from: classes3.dex */
        public enum Status {
            PENDING,
            DELIVERED,
            FAILED,
            FAILED_NO_RETRY
        }

        public Status getStatus() {
            return this.status;
        }
    }

    /* loaded from: classes3.dex */
    public static abstract class Response extends MessagingItem {
        public final AgentDetails agentDetails;

        public Response(Date date, String str, AgentDetails agentDetails) {
            super(date, str);
            this.agentDetails = agentDetails;
        }

        public AgentDetails getAgentDetails() {
            return this.agentDetails;
        }
    }

    /* loaded from: classes3.dex */
    public static class SystemMessage extends MessagingItem {
        public final String systemMessage;

        public String getSystemMessage() {
            return this.systemMessage;
        }
    }

    /* loaded from: classes3.dex */
    public static class TextQuery extends Query {
        public final String message;

        public String getMessage() {
            return this.message;
        }
    }

    /* loaded from: classes3.dex */
    public static class TextResponse extends Response {
        public final String message;

        public String getMessage() {
            return this.message;
        }
    }

    /* loaded from: classes3.dex */
    public static class TransferResponse extends Response {
        public final boolean enabled;
        public final List<Engine.TransferOptionDescription> engineOptions;
        public final String message;

        public TransferResponse(Date date, String str, AgentDetails agentDetails, String str2, List<Engine.TransferOptionDescription> list, boolean z) {
            super(date, str, agentDetails);
            this.message = str2;
            this.engineOptions = list;
            this.enabled = z;
        }

        public List<Engine.TransferOptionDescription> getEngineOptions() {
            return this.engineOptions;
        }

        public String getMessage() {
            return this.message;
        }

        public boolean isEnabled() {
            return this.enabled;
        }
    }

    public MessagingItem(Date date, String str) {
        this.timestamp = date;
        this.id = str;
    }

    public String getId() {
        return this.id;
    }

    @Override // zendesk.messaging.MessagingEvent
    public Date getTimestamp() {
        return this.timestamp;
    }
}
