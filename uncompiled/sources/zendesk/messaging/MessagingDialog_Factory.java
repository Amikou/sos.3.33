package zendesk.messaging;

import androidx.appcompat.app.AppCompatActivity;
import zendesk.messaging.components.DateProvider;

/* loaded from: classes3.dex */
public final class MessagingDialog_Factory implements y11<MessagingDialog> {
    public final ew2<AppCompatActivity> appCompatActivityProvider;
    public final ew2<DateProvider> dateProvider;
    public final ew2<MessagingViewModel> messagingViewModelProvider;

    public MessagingDialog_Factory(ew2<AppCompatActivity> ew2Var, ew2<MessagingViewModel> ew2Var2, ew2<DateProvider> ew2Var3) {
        this.appCompatActivityProvider = ew2Var;
        this.messagingViewModelProvider = ew2Var2;
        this.dateProvider = ew2Var3;
    }

    public static MessagingDialog_Factory create(ew2<AppCompatActivity> ew2Var, ew2<MessagingViewModel> ew2Var2, ew2<DateProvider> ew2Var3) {
        return new MessagingDialog_Factory(ew2Var, ew2Var2, ew2Var3);
    }

    public static MessagingDialog newInstance(AppCompatActivity appCompatActivity, MessagingViewModel messagingViewModel, DateProvider dateProvider) {
        return new MessagingDialog(appCompatActivity, messagingViewModel, dateProvider);
    }

    @Override // defpackage.ew2
    public MessagingDialog get() {
        return newInstance(this.appCompatActivityProvider.get(), this.messagingViewModelProvider.get(), this.dateProvider.get());
    }
}
