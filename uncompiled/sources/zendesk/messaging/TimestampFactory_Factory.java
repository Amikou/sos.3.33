package zendesk.messaging;

import android.content.Context;

/* loaded from: classes3.dex */
public final class TimestampFactory_Factory implements y11<TimestampFactory> {
    public final ew2<Context> contextProvider;

    public TimestampFactory_Factory(ew2<Context> ew2Var) {
        this.contextProvider = ew2Var;
    }

    public static TimestampFactory_Factory create(ew2<Context> ew2Var) {
        return new TimestampFactory_Factory(ew2Var);
    }

    public static TimestampFactory newInstance(Context context) {
        return new TimestampFactory(context);
    }

    @Override // defpackage.ew2
    public TimestampFactory get() {
        return newInstance(this.contextProvider.get());
    }
}
