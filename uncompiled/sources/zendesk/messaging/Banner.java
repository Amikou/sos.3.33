package zendesk.messaging;

/* loaded from: classes3.dex */
public class Banner {
    public final String label;
    public final Position position;

    /* loaded from: classes3.dex */
    public enum Position {
        BOTTOM
    }

    public String getLabel() {
        return this.label;
    }

    public Position getPosition() {
        return this.position;
    }
}
