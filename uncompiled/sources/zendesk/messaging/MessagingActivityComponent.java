package zendesk.messaging;

import androidx.appcompat.app.AppCompatActivity;

/* loaded from: classes3.dex */
public interface MessagingActivityComponent {

    /* loaded from: classes3.dex */
    public interface Builder {
        Builder activity(AppCompatActivity appCompatActivity);

        MessagingActivityComponent build();

        Builder messagingComponent(MessagingComponent messagingComponent);
    }

    void inject(MessagingActivity messagingActivity);
}
