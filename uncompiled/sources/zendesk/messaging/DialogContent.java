package zendesk.messaging;

/* loaded from: classes3.dex */
public class DialogContent {
    public final Config config;
    public final String message;
    public final Config previousConfig;
    public final String title;

    /* loaded from: classes3.dex */
    public enum Config {
        TRANSCRIPT_PROMPT,
        TRANSCRIPT_EMAIL
    }

    public Config getConfig() {
        return this.config;
    }

    public String getMessage() {
        return this.message;
    }

    public String getTitle() {
        return this.title;
    }

    public Config previousConfig() {
        return this.previousConfig;
    }
}
