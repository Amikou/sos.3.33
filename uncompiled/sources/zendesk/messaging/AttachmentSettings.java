package zendesk.messaging;

/* loaded from: classes3.dex */
public class AttachmentSettings {
    public final long maxFileSize;
    public final boolean sendingEnabled;

    public AttachmentSettings(long j, boolean z) {
        this.maxFileSize = j;
        this.sendingEnabled = z;
    }

    public long getMaxFileSize() {
        return this.maxFileSize;
    }

    public boolean isSendingEnabled() {
        return this.sendingEnabled;
    }
}
