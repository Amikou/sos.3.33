package zendesk.messaging;

import android.os.Handler;
import android.os.Looper;
import androidx.appcompat.app.AppCompatActivity;
import zendesk.belvedere.BelvedereUi;
import zendesk.belvedere.b;
import zendesk.messaging.components.DateProvider;

/* loaded from: classes3.dex */
public abstract class MessagingActivityModule {
    public static b belvedereUi(AppCompatActivity appCompatActivity) {
        return BelvedereUi.b(appCompatActivity);
    }

    public static DateProvider dateProvider() {
        return new DateProvider();
    }

    public static Handler handler() {
        return new Handler(Looper.getMainLooper());
    }

    public static boolean multilineResponseOptionsEnabled(MessagingComponent messagingComponent) {
        return messagingComponent.messagingConfiguration().isMultilineResponseOptionsEnabled();
    }
}
