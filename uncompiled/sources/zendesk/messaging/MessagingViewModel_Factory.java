package zendesk.messaging;

/* loaded from: classes3.dex */
public final class MessagingViewModel_Factory implements y11<MessagingViewModel> {
    public final ew2<MessagingModel> messagingModelProvider;

    public MessagingViewModel_Factory(ew2<MessagingModel> ew2Var) {
        this.messagingModelProvider = ew2Var;
    }

    public static MessagingViewModel_Factory create(ew2<MessagingModel> ew2Var) {
        return new MessagingViewModel_Factory(ew2Var);
    }

    public static MessagingViewModel newInstance(Object obj) {
        return new MessagingViewModel((MessagingModel) obj);
    }

    @Override // defpackage.ew2
    public MessagingViewModel get() {
        return newInstance(this.messagingModelProvider.get());
    }
}
