package zendesk.messaging;

import java.util.Date;

/* loaded from: classes3.dex */
public interface MessagingEvent {
    Date getTimestamp();
}
