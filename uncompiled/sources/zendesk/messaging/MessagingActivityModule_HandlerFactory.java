package zendesk.messaging;

import android.os.Handler;

/* loaded from: classes3.dex */
public final class MessagingActivityModule_HandlerFactory implements y11<Handler> {

    /* loaded from: classes3.dex */
    public static final class InstanceHolder {
        public static final MessagingActivityModule_HandlerFactory INSTANCE = new MessagingActivityModule_HandlerFactory();
    }

    public static MessagingActivityModule_HandlerFactory create() {
        return InstanceHolder.INSTANCE;
    }

    public static Handler handler() {
        return (Handler) cu2.f(MessagingActivityModule.handler());
    }

    @Override // defpackage.ew2
    public Handler get() {
        return handler();
    }
}
