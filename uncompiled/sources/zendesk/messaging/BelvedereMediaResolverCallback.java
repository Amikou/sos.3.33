package zendesk.messaging;

import com.zendesk.logger.Logger;
import java.io.File;
import java.util.ArrayList;
import java.util.List;
import zendesk.belvedere.MediaResult;

/* loaded from: classes3.dex */
public class BelvedereMediaResolverCallback extends uu<List<MediaResult>> {
    public final EventFactory eventFactory;
    public final EventListener eventListener;

    public BelvedereMediaResolverCallback(EventListener eventListener, EventFactory eventFactory) {
        this.eventListener = eventListener;
        this.eventFactory = eventFactory;
    }

    @Override // defpackage.uu
    public void success(List<MediaResult> list) {
        Logger.b("BelvedereMediaResolverCallback", "Uris have been resolved, collecting files to send the event", new Object[0]);
        ArrayList arrayList = new ArrayList();
        for (MediaResult mediaResult : list) {
            File e = mediaResult.e();
            if (e == null) {
                Logger.k("BelvedereMediaResolverCallback", "Unable to get file, skipping Uri: %s", mediaResult.l().toString());
            } else {
                arrayList.add(e);
            }
        }
        if (arrayList.isEmpty()) {
            Logger.k("BelvedereMediaResolverCallback", "No files resolved. No event will be sent", new Object[0]);
            return;
        }
        Logger.b("BelvedereMediaResolverCallback", "Sending attachment event", new Object[0]);
        this.eventListener.onEvent(this.eventFactory.sendAttachment(arrayList));
    }
}
