package zendesk.messaging;

import zendesk.messaging.components.DateProvider;

/* loaded from: classes3.dex */
public final class MessagingActivityModule_DateProviderFactory implements y11<DateProvider> {

    /* loaded from: classes3.dex */
    public static final class InstanceHolder {
        public static final MessagingActivityModule_DateProviderFactory INSTANCE = new MessagingActivityModule_DateProviderFactory();
    }

    public static MessagingActivityModule_DateProviderFactory create() {
        return InstanceHolder.INSTANCE;
    }

    public static DateProvider dateProvider() {
        return (DateProvider) cu2.f(MessagingActivityModule.dateProvider());
    }

    @Override // defpackage.ew2
    public DateProvider get() {
        return dateProvider();
    }
}
