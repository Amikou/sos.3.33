package zendesk.messaging;

import android.content.Context;
import zendesk.belvedere.a;

/* loaded from: classes3.dex */
public final class MessagingModule_BelvedereFactory implements y11<a> {
    public final ew2<Context> contextProvider;

    public MessagingModule_BelvedereFactory(ew2<Context> ew2Var) {
        this.contextProvider = ew2Var;
    }

    public static a belvedere(Context context) {
        return (a) cu2.f(MessagingModule.belvedere(context));
    }

    public static MessagingModule_BelvedereFactory create(ew2<Context> ew2Var) {
        return new MessagingModule_BelvedereFactory(ew2Var);
    }

    @Override // defpackage.ew2
    public a get() {
        return belvedere(this.contextProvider.get());
    }
}
