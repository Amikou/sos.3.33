package zendesk.messaging;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;

/* loaded from: classes3.dex */
public class MessagingConversationLog {
    public static final Comparator<MessagingEvent> TIMESTAMP_COMPARATOR = new Comparator<MessagingEvent>() { // from class: zendesk.messaging.MessagingConversationLog.1
        @Override // java.util.Comparator
        public int compare(MessagingEvent messagingEvent, MessagingEvent messagingEvent2) {
            return messagingEvent.getTimestamp().compareTo(messagingEvent2.getTimestamp());
        }
    };
    public final MessagingEventSerializer messagingEventSerializer;
    public final List<MessagingItem> messagingItems = new ArrayList();
    public final List<Event> events = new ArrayList();

    public MessagingConversationLog(MessagingEventSerializer messagingEventSerializer) {
        this.messagingEventSerializer = messagingEventSerializer;
    }

    public void addEvent(Event event) {
        this.events.add(event);
    }

    public void setMessagingItems(List<MessagingItem> list) {
        this.messagingItems.clear();
        if (l10.i(list)) {
            this.messagingItems.addAll(list);
        }
    }
}
