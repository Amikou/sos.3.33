package zendesk.messaging;

import java.util.concurrent.atomic.AtomicInteger;

/* loaded from: classes3.dex */
public class ObservableCounter {
    public final AtomicInteger counter = new AtomicInteger();
    public final OnCountCompletedListener onCountCompletedListener;

    /* loaded from: classes3.dex */
    public interface OnCountCompletedListener {
    }

    public ObservableCounter(OnCountCompletedListener onCountCompletedListener) {
        this.onCountCompletedListener = onCountCompletedListener;
    }

    public void increment(int i) {
        this.counter.addAndGet(i);
    }
}
