package zendesk.messaging;

import androidx.appcompat.app.AppCompatActivity;
import zendesk.belvedere.b;

/* loaded from: classes3.dex */
public final class MessagingActivityModule_BelvedereUiFactory implements y11<b> {
    public final ew2<AppCompatActivity> activityProvider;

    public MessagingActivityModule_BelvedereUiFactory(ew2<AppCompatActivity> ew2Var) {
        this.activityProvider = ew2Var;
    }

    public static b belvedereUi(AppCompatActivity appCompatActivity) {
        return (b) cu2.f(MessagingActivityModule.belvedereUi(appCompatActivity));
    }

    public static MessagingActivityModule_BelvedereUiFactory create(ew2<AppCompatActivity> ew2Var) {
        return new MessagingActivityModule_BelvedereUiFactory(ew2Var);
    }

    @Override // defpackage.ew2
    public b get() {
        return belvedereUi(this.activityProvider.get());
    }
}
