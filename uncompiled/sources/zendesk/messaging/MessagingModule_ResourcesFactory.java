package zendesk.messaging;

import android.content.Context;
import android.content.res.Resources;

/* loaded from: classes3.dex */
public final class MessagingModule_ResourcesFactory implements y11<Resources> {
    public final ew2<Context> contextProvider;

    public MessagingModule_ResourcesFactory(ew2<Context> ew2Var) {
        this.contextProvider = ew2Var;
    }

    public static MessagingModule_ResourcesFactory create(ew2<Context> ew2Var) {
        return new MessagingModule_ResourcesFactory(ew2Var);
    }

    public static Resources resources(Context context) {
        return (Resources) cu2.f(MessagingModule.resources(context));
    }

    @Override // defpackage.ew2
    public Resources get() {
        return resources(this.contextProvider.get());
    }
}
