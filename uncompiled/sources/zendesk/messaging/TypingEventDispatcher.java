package zendesk.messaging;

import android.os.Handler;
import java.util.concurrent.TimeUnit;

/* loaded from: classes3.dex */
public class TypingEventDispatcher {
    public static final long TYPING_WINDOW = TimeUnit.SECONDS.toMillis(3);
    public final EventFactory eventFactory;
    public final EventListener eventListener;
    public final Handler handler;
    public boolean isTyping = false;
    public final Runnable typingStopRunnable;

    public TypingEventDispatcher(final EventListener eventListener, Handler handler, final EventFactory eventFactory) {
        this.eventListener = eventListener;
        this.handler = handler;
        this.eventFactory = eventFactory;
        this.typingStopRunnable = new Runnable() { // from class: zendesk.messaging.TypingEventDispatcher.1
            @Override // java.lang.Runnable
            public void run() {
                eventListener.onEvent(eventFactory.typingStop());
                TypingEventDispatcher.this.isTyping = false;
            }
        };
    }

    public void onTyping() {
        if (this.isTyping) {
            this.handler.removeCallbacks(this.typingStopRunnable);
            this.handler.postDelayed(this.typingStopRunnable, TYPING_WINDOW);
            return;
        }
        this.isTyping = true;
        this.eventListener.onEvent(this.eventFactory.typingStart());
        this.handler.postDelayed(this.typingStopRunnable, TYPING_WINDOW);
    }
}
