package zendesk.messaging;

import com.zendesk.logger.Logger;
import java.util.concurrent.atomic.AtomicBoolean;

/* loaded from: classes3.dex */
public class SingleLiveEvent<T> extends gb2<T> {
    public final AtomicBoolean pending = new AtomicBoolean(false);

    @Override // androidx.lifecycle.LiveData
    public void observe(rz1 rz1Var, final tl2<? super T> tl2Var) {
        if (hasActiveObservers()) {
            Logger.k("SingleLiveEvent", "Multiple observers registered but only one will be notified of changes.", new Object[0]);
        }
        super.observe(rz1Var, new tl2<T>() { // from class: zendesk.messaging.SingleLiveEvent.1
            @Override // defpackage.tl2
            public void onChanged(T t) {
                if (SingleLiveEvent.this.pending.compareAndSet(true, false)) {
                    tl2Var.onChanged(t);
                }
            }
        });
    }

    @Override // defpackage.gb2, androidx.lifecycle.LiveData
    public void setValue(T t) {
        this.pending.set(true);
        super.setValue(t);
    }
}
