package zendesk.messaging;

/* loaded from: classes3.dex */
public final class BelvedereMediaResolverCallback_Factory implements y11<BelvedereMediaResolverCallback> {
    public final ew2<EventFactory> eventFactoryProvider;
    public final ew2<EventListener> eventListenerProvider;

    public BelvedereMediaResolverCallback_Factory(ew2<EventListener> ew2Var, ew2<EventFactory> ew2Var2) {
        this.eventListenerProvider = ew2Var;
        this.eventFactoryProvider = ew2Var2;
    }

    public static BelvedereMediaResolverCallback_Factory create(ew2<EventListener> ew2Var, ew2<EventFactory> ew2Var2) {
        return new BelvedereMediaResolverCallback_Factory(ew2Var, ew2Var2);
    }

    public static BelvedereMediaResolverCallback newInstance(EventListener eventListener, EventFactory eventFactory) {
        return new BelvedereMediaResolverCallback(eventListener, eventFactory);
    }

    @Override // defpackage.ew2
    public BelvedereMediaResolverCallback get() {
        return newInstance(this.eventListenerProvider.get(), this.eventFactoryProvider.get());
    }
}
