package zendesk.messaging;

/* loaded from: classes3.dex */
public enum ConnectionState {
    CONNECTING,
    CONNECTED,
    RECONNECTING,
    FAILED,
    DISCONNECTED,
    UNREACHABLE
}
