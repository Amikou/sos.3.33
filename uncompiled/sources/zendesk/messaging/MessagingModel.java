package zendesk.messaging;

import android.content.res.Resources;
import androidx.lifecycle.LiveData;
import java.util.ArrayList;
import java.util.Date;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import zendesk.configurations.Configuration;
import zendesk.messaging.Engine;
import zendesk.messaging.Event;
import zendesk.messaging.MessagingItem;
import zendesk.messaging.ObservableCounter;
import zendesk.messaging.Update;

/* loaded from: classes3.dex */
public class MessagingModel implements MessagingApi, EventListener, Engine.UpdateObserver {
    public static final AttachmentSettings DEFAULT_ATTACHMENT_SETTINGS;
    public static final Update DEFAULT_INPUT_STATE_UPDATE;
    public static final Update DEFAULT_MENU_ITEMS;
    public final List<Configuration> configurations;
    public final MessagingConversationLog conversationLog;
    public Engine currentEngine;
    public final AgentDetails defaultAgentDetails;
    public final Map<Engine, List<MessagingItem>> engineItems;
    public final List<Engine> engines;
    public final gb2<AttachmentSettings> liveAttachmentSettings;
    public final gb2<Boolean> liveComposerEnabled;
    public final gb2<String> liveComposerHint;
    public final gb2<ConnectionState> liveConnection;
    public final SingleLiveEvent<DialogContent> liveDialogUpdates;
    public final SingleLiveEvent<Banner> liveInterfaceUpdates;
    public final gb2<Integer> liveKeyboardInputType;
    public final gb2<List<MenuItem>> liveMenuItems;
    public final gb2<List<MessagingItem>> liveMessagingItems;
    public final SingleLiveEvent<Update.Action.Navigation> liveNavigationUpdates;
    public final gb2<Typing> liveTyping;

    static {
        AttachmentSettings attachmentSettings = new AttachmentSettings(0L, false);
        DEFAULT_ATTACHMENT_SETTINGS = attachmentSettings;
        DEFAULT_INPUT_STATE_UPDATE = new Update.State.UpdateInputFieldState("", Boolean.TRUE, attachmentSettings, 131073);
        DEFAULT_MENU_ITEMS = new Update.ApplyMenuItems(new MenuItem[0]);
    }

    public MessagingModel(Resources resources, List<Engine> list, MessagingConfiguration messagingConfiguration, MessagingConversationLog messagingConversationLog) {
        this.engines = new ArrayList(list.size());
        for (Engine engine : list) {
            if (engine != null) {
                this.engines.add(engine);
            }
        }
        this.conversationLog = messagingConversationLog;
        this.configurations = messagingConfiguration.getConfigurations();
        this.defaultAgentDetails = messagingConfiguration.getBotAgentDetails(resources);
        this.engineItems = new LinkedHashMap();
        this.liveMessagingItems = new gb2<>();
        this.liveMenuItems = new gb2<>();
        this.liveTyping = new gb2<>();
        this.liveConnection = new gb2<>();
        this.liveComposerHint = new gb2<>();
        this.liveKeyboardInputType = new gb2<>();
        this.liveComposerEnabled = new gb2<>();
        this.liveAttachmentSettings = new gb2<>();
        this.liveNavigationUpdates = new SingleLiveEvent<>();
        this.liveInterfaceUpdates = new SingleLiveEvent<>();
        this.liveDialogUpdates = new SingleLiveEvent<>();
    }

    public gb2<AttachmentSettings> getLiveAttachmentSettings() {
        return this.liveAttachmentSettings;
    }

    public gb2<Boolean> getLiveComposerEnabled() {
        return this.liveComposerEnabled;
    }

    public gb2<String> getLiveComposerHint() {
        return this.liveComposerHint;
    }

    public LiveData<ConnectionState> getLiveConnection() {
        return this.liveConnection;
    }

    public SingleLiveEvent<DialogContent> getLiveDialogUpdates() {
        return this.liveDialogUpdates;
    }

    public SingleLiveEvent<Banner> getLiveInterfaceUpdates() {
        return this.liveInterfaceUpdates;
    }

    public gb2<Integer> getLiveKeyboardInputType() {
        return this.liveKeyboardInputType;
    }

    public LiveData<List<MenuItem>> getLiveMenuItems() {
        return this.liveMenuItems;
    }

    public LiveData<List<MessagingItem>> getLiveMessagingItems() {
        return this.liveMessagingItems;
    }

    public SingleLiveEvent<Update.Action.Navigation> getLiveNavigationUpdates() {
        return this.liveNavigationUpdates;
    }

    public LiveData<Typing> getLiveTyping() {
        return this.liveTyping;
    }

    @Override // zendesk.messaging.EventListener
    public void onEvent(Event event) {
        this.conversationLog.addEvent(event);
        if (event.getType().equals("transfer_option_clicked")) {
            Event.EngineSelection engineSelection = (Event.EngineSelection) event;
            for (Engine engine : this.engines) {
                if (engineSelection.getSelectedEngine().getEngineId().equals(engine.getId())) {
                    startEngine(engine);
                    return;
                }
            }
            return;
        }
        Engine engine2 = this.currentEngine;
        if (engine2 != null) {
            engine2.onEvent(event);
        }
    }

    public void start() {
        update(Update.State.UpdateInputFieldState.updateInputFieldEnabled(false));
        startInitialEngine(this.engines);
    }

    public final void startEngine(Engine engine) {
        Engine engine2 = this.currentEngine;
        if (engine2 != null && engine2 != engine) {
            stopEngine(engine2);
        }
        this.currentEngine = engine;
        engine.registerObserver(this);
        update(DEFAULT_INPUT_STATE_UPDATE);
        update(DEFAULT_MENU_ITEMS);
        engine.start(this);
    }

    public final void startInitialEngine(final List<Engine> list) {
        if (l10.g(list)) {
            return;
        }
        if (list.size() == 1) {
            startEngine(list.get(0));
            return;
        }
        final ArrayList arrayList = new ArrayList();
        final ObservableCounter observableCounter = new ObservableCounter(new ObservableCounter.OnCountCompletedListener() { // from class: zendesk.messaging.MessagingModel.1
        });
        observableCounter.increment(list.size());
        for (Engine engine : list) {
            engine.isConversationOngoing(new Engine.ConversationOnGoingCallback() { // from class: zendesk.messaging.MessagingModel.2
            });
        }
    }

    public void stop() {
        Engine engine = this.currentEngine;
        if (engine != null) {
            engine.stop();
            this.currentEngine.unregisterObserver(this);
        }
    }

    public final void stopEngine(Engine engine) {
        engine.stop();
        engine.unregisterObserver(this);
    }

    public void update(Update update) {
        String type = update.getType();
        type.hashCode();
        char c = 65535;
        switch (type.hashCode()) {
            case -1524638175:
                if (type.equals("update_input_field_state")) {
                    c = 0;
                    break;
                }
                break;
            case -358781964:
                if (type.equals("apply_messaging_items")) {
                    c = 1;
                    break;
                }
                break;
            case 35633838:
                if (type.equals("show_banner")) {
                    c = 2;
                    break;
                }
                break;
            case 64608020:
                if (type.equals("hide_typing")) {
                    c = 3;
                    break;
                }
                break;
            case 99891402:
                if (type.equals("show_dialog")) {
                    c = 4;
                    break;
                }
                break;
            case 381787729:
                if (type.equals("apply_menu_items")) {
                    c = 5;
                    break;
                }
                break;
            case 573178105:
                if (type.equals("show_typing")) {
                    c = 6;
                    break;
                }
                break;
            case 1766276262:
                if (type.equals("update_connection_state")) {
                    c = 7;
                    break;
                }
                break;
            case 1862666772:
                if (type.equals("navigation")) {
                    c = '\b';
                    break;
                }
                break;
        }
        switch (c) {
            case 0:
                Update.State.UpdateInputFieldState updateInputFieldState = (Update.State.UpdateInputFieldState) update;
                String hint = updateInputFieldState.getHint();
                if (hint != null) {
                    this.liveComposerHint.postValue(hint);
                }
                Boolean isEnabled = updateInputFieldState.isEnabled();
                if (isEnabled != null) {
                    this.liveComposerEnabled.postValue(isEnabled);
                }
                AttachmentSettings attachmentSettings = updateInputFieldState.getAttachmentSettings();
                if (attachmentSettings != null) {
                    this.liveAttachmentSettings.postValue(attachmentSettings);
                }
                Integer inputType = updateInputFieldState.getInputType();
                if (inputType != null) {
                    this.liveKeyboardInputType.postValue(inputType);
                    return;
                } else {
                    this.liveKeyboardInputType.postValue(131073);
                    return;
                }
            case 1:
                this.engineItems.put(this.currentEngine, ((Update.State.ApplyMessagingItems) update).getMessagingItems());
                ArrayList arrayList = new ArrayList();
                for (Map.Entry<Engine, List<MessagingItem>> entry : this.engineItems.entrySet()) {
                    for (MessagingItem messagingItem : entry.getValue()) {
                        if (messagingItem instanceof MessagingItem.TransferResponse) {
                            Date timestamp = messagingItem.getTimestamp();
                            String id = messagingItem.getId();
                            MessagingItem.TransferResponse transferResponse = (MessagingItem.TransferResponse) messagingItem;
                            messagingItem = new MessagingItem.TransferResponse(timestamp, id, transferResponse.getAgentDetails(), transferResponse.getMessage(), transferResponse.getEngineOptions(), this.currentEngine != null && entry.getKey().equals(this.currentEngine));
                        }
                        arrayList.add(messagingItem);
                    }
                }
                this.liveMessagingItems.postValue(arrayList);
                this.conversationLog.setMessagingItems(arrayList);
                return;
            case 2:
                this.liveInterfaceUpdates.postValue(((Update.ShowBanner) update).getBanner());
                return;
            case 3:
                this.liveTyping.postValue(new Typing(false));
                return;
            case 4:
                this.liveDialogUpdates.postValue(((Update.ShowDialog) update).getDialogContent());
                return;
            case 5:
                this.liveMenuItems.postValue(((Update.ApplyMenuItems) update).getMenuItems());
                return;
            case 6:
                this.liveTyping.postValue(new Typing(true, ((Update.State.ShowTyping) update).getAgentDetails()));
                return;
            case 7:
                this.liveConnection.postValue(((Update.State.UpdateConnectionState) update).getConnectionState());
                return;
            case '\b':
                this.liveNavigationUpdates.postValue((Update.Action.Navigation) update);
                return;
            default:
                return;
        }
    }
}
