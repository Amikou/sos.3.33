package zendesk.messaging;

import android.content.res.Resources;
import java.util.List;

/* loaded from: classes3.dex */
public final class MessagingModel_Factory implements y11<MessagingModel> {
    public final ew2<MessagingConversationLog> conversationLogProvider;
    public final ew2<List<Engine>> enginesProvider;
    public final ew2<MessagingConfiguration> messagingConfigurationProvider;
    public final ew2<Resources> resourcesProvider;

    public MessagingModel_Factory(ew2<Resources> ew2Var, ew2<List<Engine>> ew2Var2, ew2<MessagingConfiguration> ew2Var3, ew2<MessagingConversationLog> ew2Var4) {
        this.resourcesProvider = ew2Var;
        this.enginesProvider = ew2Var2;
        this.messagingConfigurationProvider = ew2Var3;
        this.conversationLogProvider = ew2Var4;
    }

    public static MessagingModel_Factory create(ew2<Resources> ew2Var, ew2<List<Engine>> ew2Var2, ew2<MessagingConfiguration> ew2Var3, ew2<MessagingConversationLog> ew2Var4) {
        return new MessagingModel_Factory(ew2Var, ew2Var2, ew2Var3, ew2Var4);
    }

    public static MessagingModel newInstance(Resources resources, List<Engine> list, MessagingConfiguration messagingConfiguration, Object obj) {
        return new MessagingModel(resources, list, messagingConfiguration, (MessagingConversationLog) obj);
    }

    @Override // defpackage.ew2
    public MessagingModel get() {
        return newInstance(this.resourcesProvider.get(), this.enginesProvider.get(), this.messagingConfigurationProvider.get(), this.conversationLogProvider.get());
    }
}
