package zendesk.messaging;

import android.content.Context;
import android.content.res.Resources;
import com.squareup.picasso.Picasso;
import zendesk.belvedere.a;

/* loaded from: classes3.dex */
public abstract class MessagingModule {
    public static a belvedere(Context context) {
        return a.c(context);
    }

    public static Picasso picasso(Context context) {
        return new Picasso.b(context).a();
    }

    public static Resources resources(Context context) {
        return context.getResources();
    }
}
