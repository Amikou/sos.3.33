package zendesk.messaging.ui;

import android.content.res.Resources;

/* loaded from: classes3.dex */
public final class MessagingCellPropsFactory_Factory implements y11<MessagingCellPropsFactory> {
    public final ew2<Resources> resourcesProvider;

    public MessagingCellPropsFactory_Factory(ew2<Resources> ew2Var) {
        this.resourcesProvider = ew2Var;
    }

    public static MessagingCellPropsFactory_Factory create(ew2<Resources> ew2Var) {
        return new MessagingCellPropsFactory_Factory(ew2Var);
    }

    public static MessagingCellPropsFactory newInstance(Resources resources) {
        return new MessagingCellPropsFactory(resources);
    }

    @Override // defpackage.ew2
    public MessagingCellPropsFactory get() {
        return newInstance(this.resourcesProvider.get());
    }
}
