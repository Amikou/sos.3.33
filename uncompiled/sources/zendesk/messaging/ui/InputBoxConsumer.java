package zendesk.messaging.ui;

import java.util.ArrayList;
import zendesk.belvedere.MediaResult;
import zendesk.belvedere.a;
import zendesk.belvedere.b;
import zendesk.messaging.BelvedereMediaHolder;
import zendesk.messaging.BelvedereMediaResolverCallback;
import zendesk.messaging.EventFactory;
import zendesk.messaging.EventListener;
import zendesk.messaging.ui.InputBox;

/* loaded from: classes3.dex */
public class InputBoxConsumer implements InputBox.InputTextConsumer {
    public final a belvedere;
    public final BelvedereMediaHolder belvedereMediaHolder;
    public final BelvedereMediaResolverCallback belvedereMediaResolverCallback;
    public final EventFactory eventFactory;
    public final EventListener eventListener;
    public final b imageStream;

    public InputBoxConsumer(EventListener eventListener, EventFactory eventFactory, b bVar, a aVar, BelvedereMediaHolder belvedereMediaHolder, BelvedereMediaResolverCallback belvedereMediaResolverCallback) {
        this.eventListener = eventListener;
        this.eventFactory = eventFactory;
        this.imageStream = bVar;
        this.belvedere = aVar;
        this.belvedereMediaHolder = belvedereMediaHolder;
        this.belvedereMediaResolverCallback = belvedereMediaResolverCallback;
    }

    @Override // zendesk.messaging.ui.InputBox.InputTextConsumer
    public boolean onConsumeText(String str) {
        if (ru3.b(str)) {
            this.eventListener.onEvent(this.eventFactory.textInput(str));
        }
        ArrayList arrayList = new ArrayList();
        for (MediaResult mediaResult : this.belvedereMediaHolder.getSelectedMedia()) {
            arrayList.add(mediaResult.l());
        }
        if (!arrayList.isEmpty()) {
            this.belvedere.h(arrayList, "zendesk/messaging", this.belvedereMediaResolverCallback);
            this.belvedereMediaHolder.clear();
        }
        if (this.imageStream.k()) {
            this.imageStream.h();
            return true;
        }
        return true;
    }
}
