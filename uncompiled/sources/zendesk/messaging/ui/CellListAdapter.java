package zendesk.messaging.ui;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import androidx.recyclerview.widget.RecyclerView;
import androidx.recyclerview.widget.g;
import androidx.recyclerview.widget.o;

/* loaded from: classes3.dex */
public class CellListAdapter extends o<MessagingCell, RecyclerView.a0> {

    /* loaded from: classes3.dex */
    public static class CellDiffUtil extends g.f<MessagingCell> {
        @Override // androidx.recyclerview.widget.g.f
        public boolean areContentsTheSame(MessagingCell messagingCell, MessagingCell messagingCell2) {
            return messagingCell.areContentsTheSame(messagingCell2);
        }

        @Override // androidx.recyclerview.widget.g.f
        public boolean areItemsTheSame(MessagingCell messagingCell, MessagingCell messagingCell2) {
            if (messagingCell.getId().equals(MessagingCellFactory.TYPING_INDICATOR_ID)) {
                return false;
            }
            return messagingCell.getId().equals(messagingCell2.getId());
        }
    }

    public CellListAdapter() {
        super(new CellDiffUtil());
    }

    @Override // androidx.recyclerview.widget.RecyclerView.Adapter
    public int getItemViewType(int i) {
        return getItem(i).getLayoutRes();
    }

    @Override // androidx.recyclerview.widget.RecyclerView.Adapter
    public void onBindViewHolder(RecyclerView.a0 a0Var, int i) {
        MessagingCell item = getItem(i);
        View view = a0Var.itemView;
        if (item.getViewClassType().isInstance(view)) {
            item.bind(view);
        }
    }

    @Override // androidx.recyclerview.widget.RecyclerView.Adapter
    public RecyclerView.a0 onCreateViewHolder(ViewGroup viewGroup, int i) {
        return new RecyclerView.a0(LayoutInflater.from(viewGroup.getContext()).inflate(i, viewGroup, false)) { // from class: zendesk.messaging.ui.CellListAdapter.1
        };
    }
}
