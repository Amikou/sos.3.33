package zendesk.messaging.ui;

import android.content.Context;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import androidx.coordinatorlayout.widget.CoordinatorLayout;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.recyclerview.widget.f;
import com.squareup.picasso.Picasso;
import java.util.concurrent.TimeUnit;
import zendesk.commonui.AlmostRealProgressBar;
import zendesk.messaging.EventFactory;
import zendesk.messaging.EventListener;
import zendesk.messaging.R$id;
import zendesk.messaging.R$layout;

/* loaded from: classes3.dex */
public class MessagingView extends CoordinatorLayout {
    public static final long DEFAULT_ANIMATION_DURATION = TimeUnit.MILLISECONDS.toMillis(300);
    public final CellListAdapter cellListAdapter;
    public final LostConnectionBanner lostConnectionBanner;
    public final AlmostRealProgressBar progressBar;

    public MessagingView(Context context) {
        this(context, null);
    }

    public void renderState(MessagingState messagingState, MessagingCellFactory messagingCellFactory, Picasso picasso, final EventListener eventListener, final EventFactory eventFactory) {
        if (messagingState == null) {
            return;
        }
        this.cellListAdapter.submitList(messagingCellFactory.createCells(messagingState.messagingItems, messagingState.typingState, picasso, messagingState.attachmentSettings));
        if (messagingState.progressBarVisible) {
            this.progressBar.n(AlmostRealProgressBar.k0);
        } else {
            this.progressBar.p(300L);
        }
        this.lostConnectionBanner.update(messagingState.connectionState);
        this.lostConnectionBanner.setOnRetryConnectionClickListener(new View.OnClickListener() { // from class: zendesk.messaging.ui.MessagingView.1
            @Override // android.view.View.OnClickListener
            public void onClick(View view) {
                eventListener.onEvent(eventFactory.reconnectButtonClick());
            }
        });
    }

    public MessagingView(Context context, AttributeSet attributeSet) {
        this(context, attributeSet, 0);
    }

    public MessagingView(Context context, AttributeSet attributeSet, int i) {
        super(context, attributeSet, i);
        LayoutInflater.from(context).inflate(R$layout.zui_view_messaging, (ViewGroup) this, true);
        this.progressBar = (AlmostRealProgressBar) findViewById(R$id.zui_progressBar);
        CellListAdapter cellListAdapter = new CellListAdapter();
        this.cellListAdapter = cellListAdapter;
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(context);
        RecyclerView recyclerView = (RecyclerView) findViewById(R$id.zui_recycler_view);
        recyclerView.setLayoutManager(linearLayoutManager);
        recyclerView.setAdapter(cellListAdapter);
        recyclerView.getRecycledViewPool().k(R$layout.zui_cell_response_options_stacked, 0);
        f fVar = new f();
        long j = DEFAULT_ANIMATION_DURATION;
        fVar.setAddDuration(j);
        fVar.setChangeDuration(j);
        fVar.setRemoveDuration(j);
        fVar.setMoveDuration(j);
        fVar.setSupportsChangeAnimations(false);
        recyclerView.setItemAnimator(fVar);
        InputBox inputBox = (InputBox) findViewById(R$id.zui_input_box);
        this.lostConnectionBanner = LostConnectionBanner.create(this, recyclerView, inputBox);
        new RecyclerViewScroller(recyclerView, linearLayoutManager, cellListAdapter).install(inputBox);
    }
}
