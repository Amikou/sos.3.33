package zendesk.messaging.ui;

import androidx.appcompat.app.AppCompatActivity;
import zendesk.belvedere.b;
import zendesk.messaging.BelvedereMediaHolder;

/* loaded from: classes3.dex */
public final class InputBoxAttachmentClickListener_Factory implements y11<InputBoxAttachmentClickListener> {
    public final ew2<AppCompatActivity> activityProvider;
    public final ew2<BelvedereMediaHolder> belvedereMediaHolderProvider;
    public final ew2<b> imageStreamProvider;

    public InputBoxAttachmentClickListener_Factory(ew2<AppCompatActivity> ew2Var, ew2<b> ew2Var2, ew2<BelvedereMediaHolder> ew2Var3) {
        this.activityProvider = ew2Var;
        this.imageStreamProvider = ew2Var2;
        this.belvedereMediaHolderProvider = ew2Var3;
    }

    public static InputBoxAttachmentClickListener_Factory create(ew2<AppCompatActivity> ew2Var, ew2<b> ew2Var2, ew2<BelvedereMediaHolder> ew2Var3) {
        return new InputBoxAttachmentClickListener_Factory(ew2Var, ew2Var2, ew2Var3);
    }

    public static InputBoxAttachmentClickListener newInstance(AppCompatActivity appCompatActivity, b bVar, BelvedereMediaHolder belvedereMediaHolder) {
        return new InputBoxAttachmentClickListener(appCompatActivity, bVar, belvedereMediaHolder);
    }

    @Override // defpackage.ew2
    public InputBoxAttachmentClickListener get() {
        return newInstance(this.activityProvider.get(), this.imageStreamProvider.get(), this.belvedereMediaHolderProvider.get());
    }
}
