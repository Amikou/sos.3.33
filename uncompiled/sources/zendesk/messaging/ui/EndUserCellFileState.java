package zendesk.messaging.ui;

import zendesk.messaging.Attachment;
import zendesk.messaging.AttachmentSettings;
import zendesk.messaging.MessagingItem;

/* loaded from: classes3.dex */
public class EndUserCellFileState extends EndUserCellBaseState {
    public final Attachment attachment;
    public final AttachmentSettings attachmentSettings;
    public final MessagingItem.FileQuery.FailureReason failureReason;

    public EndUserCellFileState(String str, MessagingCellProps messagingCellProps, MessagingItem.Query.Status status, MessageActionListener messageActionListener, Attachment attachment, MessagingItem.FileQuery.FailureReason failureReason, AttachmentSettings attachmentSettings) {
        super(str, messagingCellProps, status, messageActionListener);
        this.failureReason = failureReason;
        this.attachmentSettings = attachmentSettings;
    }

    @Override // zendesk.messaging.ui.EndUserCellBaseState
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj != null && getClass() == obj.getClass() && super.equals(obj)) {
            EndUserCellFileState endUserCellFileState = (EndUserCellFileState) obj;
            if (this.failureReason != endUserCellFileState.failureReason) {
                return false;
            }
            AttachmentSettings attachmentSettings = this.attachmentSettings;
            return attachmentSettings != null ? attachmentSettings.equals(endUserCellFileState.attachmentSettings) : endUserCellFileState.attachmentSettings == null;
        }
        return false;
    }

    public Attachment getAttachment() {
        return this.attachment;
    }

    public AttachmentSettings getAttachmentSettings() {
        return this.attachmentSettings;
    }

    public MessagingItem.FileQuery.FailureReason getFailureReason() {
        return this.failureReason;
    }

    @Override // zendesk.messaging.ui.EndUserCellBaseState
    public int hashCode() {
        int hashCode = ((super.hashCode() * 31) + 0) * 31;
        MessagingItem.FileQuery.FailureReason failureReason = this.failureReason;
        int hashCode2 = (hashCode + (failureReason != null ? failureReason.hashCode() : 0)) * 31;
        AttachmentSettings attachmentSettings = this.attachmentSettings;
        return hashCode2 + (attachmentSettings != null ? attachmentSettings.hashCode() : 0);
    }
}
