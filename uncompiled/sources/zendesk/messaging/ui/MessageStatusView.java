package zendesk.messaging.ui;

import android.content.Context;
import android.content.res.ColorStateList;
import android.util.AttributeSet;
import androidx.appcompat.widget.AppCompatImageView;
import zendesk.messaging.MessagingItem;
import zendesk.messaging.R$attr;
import zendesk.messaging.R$color;
import zendesk.messaging.R$drawable;

/* loaded from: classes3.dex */
public class MessageStatusView extends AppCompatImageView {
    public int deliveredIconColor;
    public int failedIconColor;
    public int pendingIconColor;

    /* renamed from: zendesk.messaging.ui.MessageStatusView$1  reason: invalid class name */
    /* loaded from: classes3.dex */
    public static /* synthetic */ class AnonymousClass1 {
        public static final /* synthetic */ int[] $SwitchMap$zendesk$messaging$MessagingItem$Query$Status;

        static {
            int[] iArr = new int[MessagingItem.Query.Status.values().length];
            $SwitchMap$zendesk$messaging$MessagingItem$Query$Status = iArr;
            try {
                iArr[MessagingItem.Query.Status.FAILED.ordinal()] = 1;
            } catch (NoSuchFieldError unused) {
            }
            try {
                $SwitchMap$zendesk$messaging$MessagingItem$Query$Status[MessagingItem.Query.Status.FAILED_NO_RETRY.ordinal()] = 2;
            } catch (NoSuchFieldError unused2) {
            }
            try {
                $SwitchMap$zendesk$messaging$MessagingItem$Query$Status[MessagingItem.Query.Status.DELIVERED.ordinal()] = 3;
            } catch (NoSuchFieldError unused3) {
            }
            try {
                $SwitchMap$zendesk$messaging$MessagingItem$Query$Status[MessagingItem.Query.Status.PENDING.ordinal()] = 4;
            } catch (NoSuchFieldError unused4) {
            }
        }
    }

    public MessageStatusView(Context context) {
        super(context);
        init();
    }

    public final void init() {
        this.deliveredIconColor = ne4.e(R$attr.colorPrimary, getContext(), R$color.zui_color_primary);
        this.failedIconColor = ne4.b(R$color.zui_error_text_color, getContext());
        this.pendingIconColor = ne4.b(R$color.zui_cell_pending_indicator_color, getContext());
    }

    public void setStatus(MessagingItem.Query.Status status) {
        int i = AnonymousClass1.$SwitchMap$zendesk$messaging$MessagingItem$Query$Status[status.ordinal()];
        if (i == 1 || i == 2) {
            cp1.c(this, ColorStateList.valueOf(this.failedIconColor));
            setImageResource(R$drawable.zui_ic_status_fail);
        } else if (i == 3) {
            cp1.c(this, ColorStateList.valueOf(this.deliveredIconColor));
            setImageResource(R$drawable.zui_ic_status_sent);
        } else if (i != 4) {
            setImageResource(0);
        } else {
            cp1.c(this, ColorStateList.valueOf(this.pendingIconColor));
            setImageResource(R$drawable.zui_ic_status_pending);
        }
    }

    public MessageStatusView(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        init();
    }

    public MessageStatusView(Context context, AttributeSet attributeSet, int i) {
        super(context, attributeSet, i);
        init();
    }
}
