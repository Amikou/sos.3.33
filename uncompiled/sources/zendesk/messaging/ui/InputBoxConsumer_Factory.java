package zendesk.messaging.ui;

import zendesk.belvedere.a;
import zendesk.belvedere.b;
import zendesk.messaging.BelvedereMediaHolder;
import zendesk.messaging.BelvedereMediaResolverCallback;
import zendesk.messaging.EventFactory;
import zendesk.messaging.EventListener;

/* loaded from: classes3.dex */
public final class InputBoxConsumer_Factory implements y11<InputBoxConsumer> {
    public final ew2<BelvedereMediaHolder> belvedereMediaHolderProvider;
    public final ew2<BelvedereMediaResolverCallback> belvedereMediaResolverCallbackProvider;
    public final ew2<a> belvedereProvider;
    public final ew2<EventFactory> eventFactoryProvider;
    public final ew2<EventListener> eventListenerProvider;
    public final ew2<b> imageStreamProvider;

    public InputBoxConsumer_Factory(ew2<EventListener> ew2Var, ew2<EventFactory> ew2Var2, ew2<b> ew2Var3, ew2<a> ew2Var4, ew2<BelvedereMediaHolder> ew2Var5, ew2<BelvedereMediaResolverCallback> ew2Var6) {
        this.eventListenerProvider = ew2Var;
        this.eventFactoryProvider = ew2Var2;
        this.imageStreamProvider = ew2Var3;
        this.belvedereProvider = ew2Var4;
        this.belvedereMediaHolderProvider = ew2Var5;
        this.belvedereMediaResolverCallbackProvider = ew2Var6;
    }

    public static InputBoxConsumer_Factory create(ew2<EventListener> ew2Var, ew2<EventFactory> ew2Var2, ew2<b> ew2Var3, ew2<a> ew2Var4, ew2<BelvedereMediaHolder> ew2Var5, ew2<BelvedereMediaResolverCallback> ew2Var6) {
        return new InputBoxConsumer_Factory(ew2Var, ew2Var2, ew2Var3, ew2Var4, ew2Var5, ew2Var6);
    }

    public static InputBoxConsumer newInstance(EventListener eventListener, EventFactory eventFactory, b bVar, a aVar, BelvedereMediaHolder belvedereMediaHolder, BelvedereMediaResolverCallback belvedereMediaResolverCallback) {
        return new InputBoxConsumer(eventListener, eventFactory, bVar, aVar, belvedereMediaHolder, belvedereMediaResolverCallback);
    }

    @Override // defpackage.ew2
    public InputBoxConsumer get() {
        return newInstance(this.eventListenerProvider.get(), this.eventFactoryProvider.get(), this.imageStreamProvider.get(), this.belvedereProvider.get(), this.belvedereMediaHolderProvider.get(), this.belvedereMediaResolverCallbackProvider.get());
    }
}
