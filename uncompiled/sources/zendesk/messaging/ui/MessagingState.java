package zendesk.messaging.ui;

import java.util.List;
import zendesk.messaging.AgentDetails;
import zendesk.messaging.AttachmentSettings;
import zendesk.messaging.ConnectionState;
import zendesk.messaging.MessagingItem;

/* loaded from: classes3.dex */
public class MessagingState {
    public final AttachmentSettings attachmentSettings;
    public final ConnectionState connectionState;
    public final boolean enabled;
    public final String hint;
    public final int keyboardInputType;
    public final List<MessagingItem> messagingItems;
    public final boolean progressBarVisible;
    public final TypingState typingState;

    /* loaded from: classes3.dex */
    public static class TypingState {
        public final AgentDetails agentDetails;
        public final boolean isTyping;

        public TypingState(boolean z) {
            this(z, null);
        }

        public AgentDetails getAgentDetails() {
            return this.agentDetails;
        }

        public boolean isTyping() {
            return this.isTyping;
        }

        public TypingState(boolean z, AgentDetails agentDetails) {
            this.isTyping = z;
            this.agentDetails = agentDetails;
        }
    }

    public Builder newBuilder() {
        return new Builder(this);
    }

    public MessagingState(List<MessagingItem> list, boolean z, boolean z2, TypingState typingState, ConnectionState connectionState, String str, AttachmentSettings attachmentSettings, int i) {
        this.messagingItems = list;
        this.progressBarVisible = z;
        this.enabled = z2;
        this.typingState = typingState;
        this.connectionState = connectionState;
        this.hint = str;
        this.attachmentSettings = attachmentSettings;
        this.keyboardInputType = i;
    }

    /* loaded from: classes3.dex */
    public static class Builder {
        public AttachmentSettings attachmentSettings;
        public ConnectionState connectionState;
        public boolean enabled;
        public String hint;
        public int keyboardInputType;
        public List<MessagingItem> messagingItems;
        public boolean progressBarVisible;
        public TypingState typingState;

        public Builder() {
            this.typingState = new TypingState(false);
            this.connectionState = ConnectionState.DISCONNECTED;
            this.keyboardInputType = 131073;
        }

        public MessagingState build() {
            return new MessagingState(l10.e(this.messagingItems), this.progressBarVisible, this.enabled, this.typingState, this.connectionState, this.hint, this.attachmentSettings, this.keyboardInputType);
        }

        public Builder withAttachmentSettings(AttachmentSettings attachmentSettings) {
            this.attachmentSettings = attachmentSettings;
            return this;
        }

        public Builder withComposerHint(String str) {
            this.hint = str;
            return this;
        }

        public Builder withConnectionState(ConnectionState connectionState) {
            this.connectionState = connectionState;
            return this;
        }

        public Builder withEnabled(boolean z) {
            this.enabled = z;
            return this;
        }

        public Builder withKeyboardInputType(int i) {
            this.keyboardInputType = i;
            return this;
        }

        public Builder withMessagingItems(List<MessagingItem> list) {
            this.messagingItems = list;
            return this;
        }

        public Builder withTypingIndicatorState(TypingState typingState) {
            this.typingState = typingState;
            return this;
        }

        public Builder(MessagingState messagingState) {
            this.typingState = new TypingState(false);
            this.connectionState = ConnectionState.DISCONNECTED;
            this.keyboardInputType = 131073;
            this.messagingItems = messagingState.messagingItems;
            this.enabled = messagingState.enabled;
            this.typingState = messagingState.typingState;
            this.connectionState = messagingState.connectionState;
            this.hint = messagingState.hint;
            this.attachmentSettings = messagingState.attachmentSettings;
            this.keyboardInputType = messagingState.keyboardInputType;
        }
    }
}
