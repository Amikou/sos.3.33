package zendesk.messaging.ui;

import android.content.Context;
import android.content.res.Resources;
import android.graphics.Rect;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.AttributeSet;
import android.view.KeyEvent;
import android.view.MotionEvent;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ImageView;
import java.util.ArrayList;
import java.util.List;
import zendesk.messaging.R$attr;
import zendesk.messaging.R$color;
import zendesk.messaging.R$dimen;
import zendesk.messaging.R$drawable;
import zendesk.messaging.R$id;
import zendesk.messaging.R$layout;

/* loaded from: classes3.dex */
public class InputBox extends FrameLayout {
    public AttachmentsIndicator attachmentsIndicator;
    public View.OnClickListener attachmentsIndicatorClickListener;
    public FrameLayout inputBox;
    public InputTextConsumer inputTextConsumer;
    public EditText inputTextField;
    public TextWatcher inputTextWatcher;
    public ImageView sendButton;
    public final List<View.OnClickListener> sendButtonClickListeners;

    /* loaded from: classes3.dex */
    public interface InputTextConsumer {
        boolean onConsumeText(String str);
    }

    public InputBox(Context context) {
        super(context);
        this.sendButtonClickListeners = new ArrayList();
        viewInit(context);
    }

    public boolean addSendButtonClickListener(View.OnClickListener onClickListener) {
        return this.sendButtonClickListeners.add(onClickListener);
    }

    public final void bindViews() {
        this.inputBox = (FrameLayout) findViewById(R$id.zui_view_input_box);
        this.inputTextField = (EditText) findViewById(R$id.input_box_input_text);
        this.attachmentsIndicator = (AttachmentsIndicator) findViewById(R$id.input_box_attachments_indicator);
        this.sendButton = (ImageView) findViewById(R$id.input_box_send_btn);
    }

    @Override // android.view.ViewGroup, android.view.View
    public boolean dispatchKeyEventPreIme(KeyEvent keyEvent) {
        if (keyEvent.getKeyCode() == 4) {
            this.inputTextField.clearFocus();
        }
        return super.dispatchKeyEventPreIme(keyEvent);
    }

    @Override // android.view.ViewGroup, android.view.View
    public boolean dispatchTouchEvent(MotionEvent motionEvent) {
        return !isEnabled() || super.dispatchTouchEvent(motionEvent);
    }

    public final void initListeners() {
        this.inputBox.setOnClickListener(new View.OnClickListener() { // from class: zendesk.messaging.ui.InputBox.1
            @Override // android.view.View.OnClickListener
            public void onClick(View view) {
                InputBox.this.inputTextField.requestFocus();
                InputMethodManager inputMethodManager = (InputMethodManager) InputBox.this.getContext().getSystemService("input_method");
                if (inputMethodManager != null) {
                    inputMethodManager.toggleSoftInput(2, 1);
                }
            }
        });
        this.attachmentsIndicator.setOnClickListener(new View.OnClickListener() { // from class: zendesk.messaging.ui.InputBox.2
            @Override // android.view.View.OnClickListener
            public void onClick(View view) {
                if (InputBox.this.attachmentsIndicatorClickListener != null) {
                    InputBox.this.attachmentsIndicatorClickListener.onClick(view);
                }
            }
        });
        this.sendButton.setOnClickListener(new View.OnClickListener() { // from class: zendesk.messaging.ui.InputBox.3
            @Override // android.view.View.OnClickListener
            public void onClick(View view) {
                if (InputBox.this.inputTextConsumer != null && InputBox.this.inputTextConsumer.onConsumeText(InputBox.this.inputTextField.getText().toString().trim())) {
                    InputBox.this.attachmentsIndicator.reset();
                    InputBox.this.inputTextField.setText((CharSequence) null);
                }
                for (View.OnClickListener onClickListener : InputBox.this.sendButtonClickListeners) {
                    onClickListener.onClick(view);
                }
            }
        });
        this.inputTextField.addTextChangedListener(new v44() { // from class: zendesk.messaging.ui.InputBox.4
            @Override // android.text.TextWatcher
            public void afterTextChanged(Editable editable) {
                boolean b = ru3.b(editable.toString());
                boolean z = true;
                boolean z2 = InputBox.this.attachmentsIndicator.getAttachmentsCount() > 0;
                InputBox inputBox = InputBox.this;
                if (!b && !z2) {
                    z = false;
                }
                inputBox.updateSendBtn(z);
                if (InputBox.this.inputTextWatcher != null) {
                    InputBox.this.inputTextWatcher.afterTextChanged(editable);
                }
            }
        });
        this.inputTextField.setOnFocusChangeListener(new View.OnFocusChangeListener() { // from class: zendesk.messaging.ui.InputBox.5
            @Override // android.view.View.OnFocusChangeListener
            public void onFocusChange(View view, boolean z) {
                if (z) {
                    InputBox.this.inputBox.setBackgroundResource(R$drawable.zui_background_composer_selected);
                } else {
                    InputBox.this.inputBox.setBackgroundResource(R$drawable.zui_background_composer_inactive);
                }
            }
        });
    }

    @Override // android.view.ViewGroup, android.view.View
    public boolean requestFocus(int i, Rect rect) {
        return this.inputTextField.requestFocus();
    }

    public void setAttachmentsCount(int i) {
        this.attachmentsIndicator.setAttachmentsCount(i);
        boolean b = ru3.b(this.inputTextField.getText().toString());
        boolean z = true;
        boolean z2 = this.attachmentsIndicator.getAttachmentsCount() > 0;
        if (!b && !z2) {
            z = false;
        }
        updateSendBtn(z);
    }

    public void setAttachmentsIndicatorClickListener(View.OnClickListener onClickListener) {
        this.attachmentsIndicatorClickListener = onClickListener;
        showAttachmentsIndicator(onClickListener != null);
    }

    @Override // android.view.View
    public void setEnabled(boolean z) {
        super.setEnabled(z);
        this.inputTextField.setEnabled(z);
        if (!z) {
            this.inputTextField.clearFocus();
        }
        this.inputBox.setEnabled(z);
        this.sendButton.setAlpha(z ? 1.0f : 0.2f);
        this.attachmentsIndicator.setAlpha(z ? 1.0f : 0.2f);
    }

    public void setHint(String str) {
        this.inputTextField.setHint(str);
    }

    public void setInputTextConsumer(InputTextConsumer inputTextConsumer) {
        this.inputTextConsumer = inputTextConsumer;
    }

    public void setInputTextWatcher(TextWatcher textWatcher) {
        this.inputTextWatcher = textWatcher;
    }

    public void setInputType(Integer num) {
        this.inputTextField.setInputType(num.intValue());
    }

    public final void showAttachmentsIndicator(boolean z) {
        if (z) {
            this.attachmentsIndicator.setEnabled(true);
            this.attachmentsIndicator.setVisibility(0);
            updateInputFieldPosition(true);
            return;
        }
        this.attachmentsIndicator.setEnabled(false);
        this.attachmentsIndicator.setVisibility(8);
        updateInputFieldPosition(false);
    }

    public final void updateInputFieldPosition(boolean z) {
        Resources resources = getResources();
        int dimensionPixelSize = resources.getDimensionPixelSize(R$dimen.zui_input_box_expanded_side_margin);
        int dimensionPixelSize2 = resources.getDimensionPixelSize(R$dimen.zui_input_box_collapsed_side_margin);
        FrameLayout.LayoutParams layoutParams = (FrameLayout.LayoutParams) this.inputTextField.getLayoutParams();
        if (z) {
            dimensionPixelSize = dimensionPixelSize2;
        }
        layoutParams.leftMargin = dimensionPixelSize;
        this.inputTextField.setLayoutParams(layoutParams);
    }

    public final void updateSendBtn(boolean z) {
        int b;
        Context context = getContext();
        if (z) {
            b = ne4.e(R$attr.colorPrimary, context, R$color.zui_color_primary);
        } else {
            b = ne4.b(R$color.zui_color_disabled, context);
        }
        this.sendButton.setEnabled(z);
        ne4.c(b, this.sendButton.getDrawable(), this.sendButton);
    }

    public final void viewInit(Context context) {
        FrameLayout.inflate(context, R$layout.zui_view_input_box, this);
        if (isInEditMode()) {
            return;
        }
        bindViews();
        initListeners();
        showAttachmentsIndicator(false);
        updateSendBtn(false);
    }

    public InputBox(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        this.sendButtonClickListeners = new ArrayList();
        viewInit(context);
    }

    public InputBox(Context context, AttributeSet attributeSet, int i) {
        super(context, attributeSet, i);
        this.sendButtonClickListeners = new ArrayList();
        viewInit(context);
    }
}
