package zendesk.messaging.ui;

import zendesk.messaging.AgentDetails;

/* loaded from: classes3.dex */
public class AvatarStateFactory {
    public AvatarState createAvatarState(AgentDetails agentDetails) {
        return new AvatarState(agentDetails.getAgentId(), ru3.b(agentDetails.getAgentName()) ? agentDetails.getAgentName().substring(0, 1) : "", agentDetails.getAvatarPath(), agentDetails.getAvatarDrawableRes());
    }
}
