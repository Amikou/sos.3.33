package zendesk.messaging.ui;

import java.util.List;
import zendesk.messaging.MessagingItem;

/* loaded from: classes3.dex */
public class ResponseOptionsViewState {
    public final ResponseOptionHandler listener;
    public final List<MessagingItem.Option> options;
    public final MessagingCellProps props;

    public ResponseOptionsViewState(List<MessagingItem.Option> list, ResponseOptionHandler responseOptionHandler, MessagingCellProps messagingCellProps) {
        this.options = list;
        this.listener = responseOptionHandler;
        this.props = messagingCellProps;
    }

    public ResponseOptionHandler getListener() {
        return this.listener;
    }

    public List<MessagingItem.Option> getOptions() {
        return this.options;
    }

    public MessagingCellProps getProps() {
        return this.props;
    }
}
