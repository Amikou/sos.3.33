package zendesk.messaging.ui;

import android.text.Editable;
import androidx.appcompat.app.AppCompatActivity;
import java.util.List;
import zendesk.belvedere.MediaResult;
import zendesk.belvedere.b;
import zendesk.messaging.AttachmentSettings;
import zendesk.messaging.BelvedereMediaHolder;
import zendesk.messaging.MessagingViewModel;
import zendesk.messaging.R$string;
import zendesk.messaging.TypingEventDispatcher;

/* loaded from: classes3.dex */
public class MessagingComposer {
    public static final int DEFAULT_HINT = R$string.zui_hint_type_message;
    public final AppCompatActivity activity;
    public final BelvedereMediaHolder belvedereMediaHolder;
    public BelvedereMediaPickerListener belvedereMediaPickerListener;
    public final b imageStream;
    public final InputBoxAttachmentClickListener inputBoxAttachmentClickListener;
    public final InputBoxConsumer inputBoxConsumer;
    public final TypingEventDispatcher typingEventDispatcher;
    public final MessagingViewModel viewModel;

    /* loaded from: classes3.dex */
    public static final class BelvedereMediaPickerListener implements b.InterfaceC0296b {
        public final BelvedereMediaHolder belvedereMediaHolder;
        public final b imageStream;
        public final InputBox inputBox;

        public BelvedereMediaPickerListener(BelvedereMediaHolder belvedereMediaHolder, InputBox inputBox, b bVar) {
            this.belvedereMediaHolder = belvedereMediaHolder;
            this.inputBox = inputBox;
            this.imageStream = bVar;
        }

        @Override // zendesk.belvedere.b.InterfaceC0296b
        public void onDismissed() {
            if (this.imageStream.i().getInputTrap().hasFocus()) {
                this.inputBox.requestFocus();
            }
        }

        @Override // zendesk.belvedere.b.InterfaceC0296b
        public void onMediaDeselected(List<MediaResult> list) {
            this.belvedereMediaHolder.removeAll(list);
            this.inputBox.setAttachmentsCount(this.belvedereMediaHolder.getSelectedMediaCount());
        }

        @Override // zendesk.belvedere.b.InterfaceC0296b
        public void onMediaSelected(List<MediaResult> list) {
            this.belvedereMediaHolder.addAll(list);
            this.inputBox.setAttachmentsCount(this.belvedereMediaHolder.getSelectedMediaCount());
        }

        @Override // zendesk.belvedere.b.InterfaceC0296b
        public void onVisible() {
        }
    }

    public MessagingComposer(AppCompatActivity appCompatActivity, MessagingViewModel messagingViewModel, b bVar, BelvedereMediaHolder belvedereMediaHolder, InputBoxConsumer inputBoxConsumer, InputBoxAttachmentClickListener inputBoxAttachmentClickListener, TypingEventDispatcher typingEventDispatcher) {
        this.activity = appCompatActivity;
        this.viewModel = messagingViewModel;
        this.imageStream = bVar;
        this.belvedereMediaHolder = belvedereMediaHolder;
        this.inputBoxConsumer = inputBoxConsumer;
        this.inputBoxAttachmentClickListener = inputBoxAttachmentClickListener;
        this.typingEventDispatcher = typingEventDispatcher;
    }

    public void bind(final InputBox inputBox) {
        inputBox.setInputTextConsumer(this.inputBoxConsumer);
        inputBox.setInputTextWatcher(new v44() { // from class: zendesk.messaging.ui.MessagingComposer.1
            @Override // android.text.TextWatcher
            public void afterTextChanged(Editable editable) {
                MessagingComposer.this.typingEventDispatcher.onTyping();
            }
        });
        BelvedereMediaPickerListener belvedereMediaPickerListener = new BelvedereMediaPickerListener(this.belvedereMediaHolder, inputBox, this.imageStream);
        this.belvedereMediaPickerListener = belvedereMediaPickerListener;
        this.imageStream.f(belvedereMediaPickerListener);
        this.viewModel.getLiveMessagingState().observe(this.activity, new tl2<MessagingState>() { // from class: zendesk.messaging.ui.MessagingComposer.2
            @Override // defpackage.tl2
            public void onChanged(MessagingState messagingState) {
                MessagingComposer.this.renderState(messagingState, inputBox);
            }
        });
    }

    public void renderState(MessagingState messagingState, InputBox inputBox) {
        if (messagingState != null) {
            inputBox.setHint(ru3.b(messagingState.hint) ? messagingState.hint : this.activity.getString(DEFAULT_HINT));
            inputBox.setEnabled(messagingState.enabled);
            inputBox.setInputType(Integer.valueOf(messagingState.keyboardInputType));
            AttachmentSettings attachmentSettings = messagingState.attachmentSettings;
            if (attachmentSettings != null && attachmentSettings.isSendingEnabled()) {
                inputBox.setAttachmentsIndicatorClickListener(this.inputBoxAttachmentClickListener);
                inputBox.setAttachmentsCount(this.belvedereMediaHolder.getSelectedMediaCount());
                return;
            }
            inputBox.setAttachmentsIndicatorClickListener(null);
        }
    }
}
