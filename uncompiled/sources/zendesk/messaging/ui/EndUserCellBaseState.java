package zendesk.messaging.ui;

import zendesk.messaging.MessagingItem;

/* loaded from: classes3.dex */
public abstract class EndUserCellBaseState {
    public final String id;
    public final MessageActionListener messageActionListener;
    public final MessagingCellProps props;
    public final MessagingItem.Query.Status status;

    public EndUserCellBaseState(String str, MessagingCellProps messagingCellProps, MessagingItem.Query.Status status, MessageActionListener messageActionListener) {
        this.id = str;
        this.props = messagingCellProps;
        this.status = status;
        this.messageActionListener = messageActionListener;
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null || getClass() != obj.getClass()) {
            return false;
        }
        EndUserCellBaseState endUserCellBaseState = (EndUserCellBaseState) obj;
        String str = this.id;
        if (str == null ? endUserCellBaseState.id == null : str.equals(endUserCellBaseState.id)) {
            MessagingCellProps messagingCellProps = this.props;
            if (messagingCellProps == null ? endUserCellBaseState.props == null : messagingCellProps.equals(endUserCellBaseState.props)) {
                if (this.status != endUserCellBaseState.status) {
                    return false;
                }
                return (this.messageActionListener != null) == (endUserCellBaseState.messageActionListener == null);
            }
            return false;
        }
        return false;
    }

    public String getId() {
        return this.id;
    }

    public MessageActionListener getMessageActionListener() {
        return this.messageActionListener;
    }

    public MessagingCellProps getProps() {
        return this.props;
    }

    public MessagingItem.Query.Status getStatus() {
        return this.status;
    }

    public int hashCode() {
        String str = this.id;
        int hashCode = (str != null ? str.hashCode() : 0) * 31;
        MessagingCellProps messagingCellProps = this.props;
        int hashCode2 = (hashCode + (messagingCellProps != null ? messagingCellProps.hashCode() : 0)) * 31;
        MessagingItem.Query.Status status = this.status;
        int hashCode3 = (hashCode2 + (status != null ? status.hashCode() : 0)) * 31;
        MessageActionListener messageActionListener = this.messageActionListener;
        return hashCode3 + (messageActionListener != null ? messageActionListener.hashCode() : 0);
    }
}
