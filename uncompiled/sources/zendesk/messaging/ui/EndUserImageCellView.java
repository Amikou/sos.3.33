package zendesk.messaging.ui;

import android.content.Context;
import android.util.AttributeSet;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import zendesk.messaging.MessagingItem;
import zendesk.messaging.R$dimen;
import zendesk.messaging.R$id;
import zendesk.messaging.R$layout;

/* loaded from: classes3.dex */
public class EndUserImageCellView extends LinearLayout implements Updatable<EndUserCellImageState> {
    public int cornerRadiusPx;
    public FileUploadProgressView fileUploadProgressView;
    public ImageView imageView;
    public TextView label;
    public MessageStatusView statusView;

    public EndUserImageCellView(Context context) {
        super(context);
        init();
    }

    public final void init() {
        setOrientation(1);
        setGravity(8388693);
        LinearLayout.inflate(getContext(), R$layout.zui_view_end_user_image_cell_content, this);
        this.cornerRadiusPx = getResources().getDimensionPixelSize(R$dimen.zui_cell_bubble_corner_radius);
    }

    public final void loadImageIntoImageView(EndUserCellImageState endUserCellImageState) {
        UtilsEndUserCellView.getImageLoadingPlaceholder(getContext());
        endUserCellImageState.getAttachment();
        throw null;
    }

    @Override // android.view.View
    public void onFinishInflate() {
        super.onFinishInflate();
        this.imageView = (ImageView) findViewById(R$id.zui_image_cell_image);
        this.fileUploadProgressView = (FileUploadProgressView) findViewById(R$id.zui_cell_file_upload_progress);
        this.statusView = (MessageStatusView) findViewById(R$id.zui_cell_status_view);
        this.label = (TextView) findViewById(R$id.zui_cell_label_message);
    }

    @Override // zendesk.messaging.ui.Updatable
    public void update(EndUserCellImageState endUserCellImageState) {
        loadImageIntoImageView(endUserCellImageState);
        if (endUserCellImageState.getStatus() == MessagingItem.Query.Status.PENDING) {
            this.fileUploadProgressView.setVisibility(0);
        } else {
            this.fileUploadProgressView.setVisibility(8);
        }
        this.statusView.setStatus(endUserCellImageState.getStatus());
        UtilsEndUserCellView.setImageViewColorFilter(endUserCellImageState, this.imageView, getContext());
        UtilsEndUserCellView.setLabelErrorMessage(endUserCellImageState, this.label, getContext());
        UtilsEndUserCellView.setClickListener(endUserCellImageState, this);
        UtilsEndUserCellView.setLongClickListener(endUserCellImageState, this);
        endUserCellImageState.getProps().apply(this, this.statusView);
    }

    public EndUserImageCellView(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        init();
    }

    public EndUserImageCellView(Context context, AttributeSet attributeSet, int i) {
        super(context, attributeSet, i);
        init();
    }
}
