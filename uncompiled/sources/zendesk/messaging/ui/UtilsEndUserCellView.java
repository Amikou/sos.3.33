package zendesk.messaging.ui;

import android.content.Context;
import android.graphics.PorterDuff;
import android.graphics.PorterDuffColorFilter;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.GradientDrawable;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import com.zendesk.logger.Logger;
import java.util.HashSet;
import java.util.Set;
import zendesk.messaging.MessagingItem;
import zendesk.messaging.R$attr;
import zendesk.messaging.R$color;
import zendesk.messaging.R$dimen;
import zendesk.messaging.R$drawable;
import zendesk.messaging.R$string;
import zendesk.messaging.ui.MessagePopUpHelper;

/* loaded from: classes3.dex */
public class UtilsEndUserCellView {
    public static final int ERROR_BACKGROUND = R$drawable.zui_background_cell_errored;
    public static final int FILE_BACKGROUND = R$drawable.zui_background_cell_file;
    public static final int USER_MESSAGE_BACKGROUND = R$drawable.zui_background_end_user_cell;
    public static final int TAP_TO_RETRY = R$string.zui_label_tap_retry;
    public static final int EXCEEDING_MAX_FILE_SIZE = R$string.zui_message_log_message_file_exceeds_max_size;
    public static final int ATTACHMENTS_NOT_SUPPORTED = R$string.zui_message_log_message_attachments_not_supported;
    public static final int ATTACHMENT_TYPE_NOT_SUPPORTED = R$string.zui_message_log_message_attachment_type_not_supported;
    public static final int ATTACHMENT_COULD_NOT_BE_SENT = R$string.zui_message_log_attachment_sending_failed;
    public static final int ERROR_BACKGROUND_COLOR = R$color.zui_error_background_color;
    public static final int PENDING_COLOR = R$color.zui_color_white_60;

    /* renamed from: zendesk.messaging.ui.UtilsEndUserCellView$5  reason: invalid class name */
    /* loaded from: classes3.dex */
    public static /* synthetic */ class AnonymousClass5 {
        public static final /* synthetic */ int[] $SwitchMap$zendesk$messaging$MessagingItem$FileQuery$FailureReason;
        public static final /* synthetic */ int[] $SwitchMap$zendesk$messaging$MessagingItem$Query$Status;

        static {
            int[] iArr = new int[MessagingItem.Query.Status.values().length];
            $SwitchMap$zendesk$messaging$MessagingItem$Query$Status = iArr;
            try {
                iArr[MessagingItem.Query.Status.PENDING.ordinal()] = 1;
            } catch (NoSuchFieldError unused) {
            }
            try {
                $SwitchMap$zendesk$messaging$MessagingItem$Query$Status[MessagingItem.Query.Status.FAILED_NO_RETRY.ordinal()] = 2;
            } catch (NoSuchFieldError unused2) {
            }
            try {
                $SwitchMap$zendesk$messaging$MessagingItem$Query$Status[MessagingItem.Query.Status.FAILED.ordinal()] = 3;
            } catch (NoSuchFieldError unused3) {
            }
            try {
                $SwitchMap$zendesk$messaging$MessagingItem$Query$Status[MessagingItem.Query.Status.DELIVERED.ordinal()] = 4;
            } catch (NoSuchFieldError unused4) {
            }
            int[] iArr2 = new int[MessagingItem.FileQuery.FailureReason.values().length];
            $SwitchMap$zendesk$messaging$MessagingItem$FileQuery$FailureReason = iArr2;
            try {
                iArr2[MessagingItem.FileQuery.FailureReason.FILE_SIZE_TOO_LARGE.ordinal()] = 1;
            } catch (NoSuchFieldError unused5) {
            }
            try {
                $SwitchMap$zendesk$messaging$MessagingItem$FileQuery$FailureReason[MessagingItem.FileQuery.FailureReason.FILE_SENDING_DISABLED.ordinal()] = 2;
            } catch (NoSuchFieldError unused6) {
            }
            try {
                $SwitchMap$zendesk$messaging$MessagingItem$FileQuery$FailureReason[MessagingItem.FileQuery.FailureReason.UNSUPPORTED_FILE_TYPE.ordinal()] = 3;
            } catch (NoSuchFieldError unused7) {
            }
        }
    }

    public static String getAttachmentLabelErrorMessage(EndUserCellFileState endUserCellFileState, Context context) {
        if (endUserCellFileState.getStatus() == MessagingItem.Query.Status.FAILED) {
            return context.getString(TAP_TO_RETRY);
        }
        return getAttachmentNonRetryableErrorMessage(endUserCellFileState, context);
    }

    public static String getAttachmentNonRetryableErrorMessage(EndUserCellFileState endUserCellFileState, Context context) {
        String string = context.getString(ATTACHMENT_COULD_NOT_BE_SENT);
        if (endUserCellFileState.getFailureReason() == null) {
            return string;
        }
        int i = AnonymousClass5.$SwitchMap$zendesk$messaging$MessagingItem$FileQuery$FailureReason[endUserCellFileState.getFailureReason().ordinal()];
        if (i == 1) {
            return endUserCellFileState.getAttachmentSettings() != null ? context.getString(EXCEEDING_MAX_FILE_SIZE, UtilsAttachment.formatFileSize(context, endUserCellFileState.getAttachmentSettings().getMaxFileSize())) : string;
        } else if (i != 2) {
            return i != 3 ? string : context.getString(ATTACHMENT_TYPE_NOT_SUPPORTED);
        } else {
            return context.getString(ATTACHMENTS_NOT_SUPPORTED);
        }
    }

    public static Drawable getImageLoadingPlaceholder(Context context) {
        int e = ne4.e(R$attr.colorPrimary, context, R$color.zui_color_primary);
        int e2 = ne4.e(R$attr.colorPrimaryDark, context, R$color.zui_color_primary_dark);
        float dimension = context.getResources().getDimension(R$dimen.zui_cell_bubble_corner_radius);
        GradientDrawable gradientDrawable = new GradientDrawable(GradientDrawable.Orientation.LEFT_RIGHT, new int[]{e2, e, e2});
        gradientDrawable.setCornerRadius(dimension);
        return gradientDrawable;
    }

    public static Set<MessagePopUpHelper.Option> getMenuOptions(MessagingItem.Query.Status status) {
        HashSet hashSet = new HashSet(2);
        if (status == MessagingItem.Query.Status.FAILED) {
            hashSet.add(MessagePopUpHelper.Option.DELETE);
            hashSet.add(MessagePopUpHelper.Option.RETRY);
        } else if (status == MessagingItem.Query.Status.FAILED_NO_RETRY) {
            hashSet.add(MessagePopUpHelper.Option.DELETE);
        }
        return hashSet;
    }

    public static boolean isFailedCell(EndUserCellBaseState endUserCellBaseState) {
        MessagingItem.Query.Status status = endUserCellBaseState.getStatus();
        return status == MessagingItem.Query.Status.FAILED || status == MessagingItem.Query.Status.FAILED_NO_RETRY;
    }

    public static void setAttachmentClickListener(final EndUserCellFileState endUserCellFileState, View view) {
        int i = AnonymousClass5.$SwitchMap$zendesk$messaging$MessagingItem$Query$Status[endUserCellFileState.getStatus().ordinal()];
        if (i == 1 || i == 2) {
            view.setOnClickListener(null);
        } else if (i == 3) {
            view.setOnClickListener(new View.OnClickListener() { // from class: zendesk.messaging.ui.UtilsEndUserCellView.2
                @Override // android.view.View.OnClickListener
                public void onClick(View view2) {
                    if (EndUserCellFileState.this.getMessageActionListener() != null) {
                        EndUserCellFileState.this.getMessageActionListener().retry(EndUserCellFileState.this.getId());
                    }
                }
            });
        } else if (i != 4) {
        } else {
            view.setOnClickListener(new View.OnClickListener() { // from class: zendesk.messaging.ui.UtilsEndUserCellView.3
                @Override // android.view.View.OnClickListener
                public void onClick(View view2) {
                    EndUserCellFileState.this.getAttachment();
                    throw null;
                }
            });
        }
    }

    public static void setCellBackground(EndUserCellBaseState endUserCellBaseState, View view) {
        if (isFailedCell(endUserCellBaseState)) {
            view.setBackgroundResource(ERROR_BACKGROUND);
        } else if (endUserCellBaseState instanceof EndUserCellFileState) {
            view.setBackgroundResource(FILE_BACKGROUND);
        } else {
            Drawable f = m70.f(view.getContext(), USER_MESSAGE_BACKGROUND);
            if (f != null) {
                f.setColorFilter(new PorterDuffColorFilter(ne4.e(R$attr.colorPrimary, view.getContext(), R$color.zui_color_primary), PorterDuff.Mode.SRC_ATOP));
                view.setBackground(f);
                return;
            }
            Logger.k("UtilsEndUserCellView", "Failed to set background, resource R.drawable.zui_background_end_user_cell could not be found", new Object[0]);
        }
    }

    public static void setClickListener(EndUserCellBaseState endUserCellBaseState, View view) {
        if (endUserCellBaseState instanceof EndUserCellMessageState) {
            setMessageClickListener((EndUserCellMessageState) endUserCellBaseState, view);
        } else if (endUserCellBaseState instanceof EndUserCellFileState) {
            setAttachmentClickListener((EndUserCellFileState) endUserCellBaseState, view);
        }
    }

    public static void setImageViewColorFilter(EndUserCellBaseState endUserCellBaseState, ImageView imageView, Context context) {
        if (isFailedCell(endUserCellBaseState)) {
            imageView.setColorFilter(ne4.b(ERROR_BACKGROUND_COLOR, context), PorterDuff.Mode.MULTIPLY);
        } else if (endUserCellBaseState.getStatus() == MessagingItem.Query.Status.PENDING) {
            imageView.setColorFilter(ne4.b(PENDING_COLOR, context), PorterDuff.Mode.MULTIPLY);
        } else {
            imageView.clearColorFilter();
        }
    }

    public static void setLabelErrorMessage(EndUserCellBaseState endUserCellBaseState, TextView textView, Context context) {
        if (!isFailedCell(endUserCellBaseState)) {
            textView.setVisibility(8);
            return;
        }
        textView.setVisibility(0);
        if (endUserCellBaseState instanceof EndUserCellFileState) {
            textView.setText(getAttachmentLabelErrorMessage((EndUserCellFileState) endUserCellBaseState, context));
        } else {
            textView.setText(context.getString(TAP_TO_RETRY));
        }
    }

    public static void setLongClickListener(final EndUserCellBaseState endUserCellBaseState, final View view) {
        view.setOnLongClickListener(new View.OnLongClickListener() { // from class: zendesk.messaging.ui.UtilsEndUserCellView.4
            @Override // android.view.View.OnLongClickListener
            public boolean onLongClick(View view2) {
                MessagePopUpHelper.showPopUpMenu(view, UtilsEndUserCellView.getMenuOptions(endUserCellBaseState.getStatus()), endUserCellBaseState.getMessageActionListener(), endUserCellBaseState.getId());
                return true;
            }
        });
    }

    public static void setMessageClickListener(final EndUserCellMessageState endUserCellMessageState, View view) {
        if (endUserCellMessageState.getStatus() == MessagingItem.Query.Status.FAILED || endUserCellMessageState.getStatus() == MessagingItem.Query.Status.FAILED_NO_RETRY) {
            view.setOnClickListener(new View.OnClickListener() { // from class: zendesk.messaging.ui.UtilsEndUserCellView.1
                @Override // android.view.View.OnClickListener
                public void onClick(View view2) {
                    if (EndUserCellMessageState.this.getMessageActionListener() != null) {
                        EndUserCellMessageState.this.getMessageActionListener().retry(EndUserCellMessageState.this.getId());
                    }
                }
            });
        }
    }
}
