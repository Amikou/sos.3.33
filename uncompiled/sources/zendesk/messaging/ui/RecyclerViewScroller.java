package zendesk.messaging.ui;

import android.view.View;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.recyclerview.widget.m;

/* loaded from: classes3.dex */
public class RecyclerViewScroller {
    public final RecyclerView.Adapter<RecyclerView.a0> adapter;
    public final LinearLayoutManager linearLayoutManager;
    public final RecyclerView recyclerView;
    public int lastCompletelyVisiblePosition = 0;
    public int secondCompletelyVisiblePosition = 0;

    public RecyclerViewScroller(final RecyclerView recyclerView, final LinearLayoutManager linearLayoutManager, final RecyclerView.Adapter<RecyclerView.a0> adapter) {
        this.recyclerView = recyclerView;
        this.linearLayoutManager = linearLayoutManager;
        this.adapter = adapter;
        recyclerView.l(new RecyclerView.r() { // from class: zendesk.messaging.ui.RecyclerViewScroller.1
            @Override // androidx.recyclerview.widget.RecyclerView.r
            public void onScrolled(RecyclerView recyclerView2, int i, int i2) {
                super.onScrolled(recyclerView2, i, i2);
                RecyclerViewScroller recyclerViewScroller = RecyclerViewScroller.this;
                recyclerViewScroller.secondCompletelyVisiblePosition = recyclerViewScroller.lastCompletelyVisiblePosition;
                RecyclerViewScroller.this.lastCompletelyVisiblePosition = linearLayoutManager.i2();
            }
        });
        recyclerView.addOnLayoutChangeListener(new View.OnLayoutChangeListener() { // from class: zendesk.messaging.ui.RecyclerViewScroller.2
            @Override // android.view.View.OnLayoutChangeListener
            public void onLayoutChange(View view, int i, int i2, int i3, int i4, int i5, int i6, int i7, int i8) {
                if (i4 < i8 && adapter.getItemCount() - 1 == RecyclerViewScroller.this.secondCompletelyVisiblePosition) {
                    RecyclerViewScroller.this.postScrollToBottom(1);
                }
            }
        });
        adapter.registerAdapterDataObserver(new RecyclerView.i() { // from class: zendesk.messaging.ui.RecyclerViewScroller.3
            @Override // androidx.recyclerview.widget.RecyclerView.i
            public void onItemRangeInserted(int i, int i2) {
                if (recyclerView.canScrollVertically(1)) {
                    return;
                }
                RecyclerViewScroller.this.postScrollToBottom(3);
            }
        });
    }

    public void install(final InputBox inputBox) {
        inputBox.addOnLayoutChangeListener(new View.OnLayoutChangeListener() { // from class: zendesk.messaging.ui.RecyclerViewScroller.4
            @Override // android.view.View.OnLayoutChangeListener
            public void onLayoutChange(View view, int i, final int i2, int i3, int i4, int i5, final int i6, int i7, int i8) {
                RecyclerViewScroller.this.recyclerView.post(new Runnable() { // from class: zendesk.messaging.ui.RecyclerViewScroller.4.1
                    @Override // java.lang.Runnable
                    public void run() {
                        int paddingLeft = RecyclerViewScroller.this.recyclerView.getPaddingLeft();
                        int paddingRight = RecyclerViewScroller.this.recyclerView.getPaddingRight();
                        int paddingTop = RecyclerViewScroller.this.recyclerView.getPaddingTop();
                        int height = inputBox.getHeight();
                        if (height != RecyclerViewScroller.this.recyclerView.getPaddingBottom()) {
                            RecyclerViewScroller.this.recyclerView.setPadding(paddingLeft, paddingTop, paddingRight, height);
                            RecyclerViewScroller.this.recyclerView.scrollBy(0, i6 - i2);
                        }
                    }
                });
            }
        });
        inputBox.addSendButtonClickListener(new View.OnClickListener() { // from class: zendesk.messaging.ui.RecyclerViewScroller.5
            @Override // android.view.View.OnClickListener
            public void onClick(View view) {
                RecyclerViewScroller.this.postScrollToBottom(1);
            }
        });
    }

    public final void postScrollToBottom(final int i) {
        this.recyclerView.post(new Runnable() { // from class: zendesk.messaging.ui.RecyclerViewScroller.7
            @Override // java.lang.Runnable
            public void run() {
                RecyclerViewScroller.this.scrollToBottom(i);
            }
        });
    }

    public final void scrollToBottom(int i) {
        int itemCount = this.adapter.getItemCount() - 1;
        if (itemCount >= 0) {
            if (i == 1) {
                RecyclerView.a0 Z = this.recyclerView.Z(itemCount);
                this.linearLayoutManager.I2(itemCount, (this.recyclerView.getPaddingBottom() + (Z != null ? Z.itemView.getHeight() : 0)) * (-1));
            } else if (i == 3) {
                m mVar = new m(this.recyclerView.getContext()) { // from class: zendesk.messaging.ui.RecyclerViewScroller.6
                    @Override // androidx.recyclerview.widget.m
                    public int calculateTimeForScrolling(int i2) {
                        return 50;
                    }
                };
                mVar.setTargetPosition(itemCount);
                this.linearLayoutManager.S1(mVar);
            } else if (i == 2) {
                m mVar2 = new m(this.recyclerView.getContext());
                mVar2.setTargetPosition(itemCount);
                this.linearLayoutManager.S1(mVar2);
            }
        }
    }
}
