package zendesk.messaging.ui;

import com.squareup.picasso.Picasso;
import zendesk.messaging.R$drawable;

/* loaded from: classes3.dex */
public class AvatarStateRenderer {
    public static final int DEFAULT_AVATAR_DRAWABLE = R$drawable.zui_ic_default_avatar_16;
    public final Picasso picasso;

    public AvatarStateRenderer(Picasso picasso) {
        this.picasso = picasso;
    }

    public void render(AvatarState avatarState, AvatarView avatarView) {
        if (ru3.b(avatarState.getAvatarUrl())) {
            avatarView.showImage(this.picasso, avatarState.getAvatarUrl());
        } else if (avatarState.getAvatarRes() != null) {
            avatarView.showDrawable(avatarState.getAvatarRes().intValue());
        } else if (ru3.b(avatarState.getAvatarLetter()) && avatarState.getAvatarLetter().matches("[a-zA-Z]")) {
            avatarView.showLetter(avatarState.getAvatarLetter(), avatarState.getUniqueIdentifier());
        } else {
            avatarView.showDefault(DEFAULT_AVATAR_DRAWABLE, avatarState.getUniqueIdentifier());
        }
    }
}
