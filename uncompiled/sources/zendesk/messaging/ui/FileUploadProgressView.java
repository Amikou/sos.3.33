package zendesk.messaging.ui;

import android.content.Context;
import android.graphics.PorterDuff;
import android.util.AttributeSet;
import android.widget.ProgressBar;
import zendesk.messaging.R$attr;
import zendesk.messaging.R$color;

/* loaded from: classes3.dex */
public class FileUploadProgressView extends ProgressBar {
    public FileUploadProgressView(Context context) {
        super(context);
        init();
    }

    public final void init() {
        setIndeterminate(true);
        getIndeterminateDrawable().setColorFilter(ne4.e(R$attr.colorPrimary, getContext(), R$color.zui_color_primary), PorterDuff.Mode.SRC_IN);
    }

    public FileUploadProgressView(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        init();
    }

    public FileUploadProgressView(Context context, AttributeSet attributeSet, int i) {
        super(context, attributeSet, i);
        init();
    }
}
