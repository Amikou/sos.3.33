package zendesk.messaging.ui;

import android.content.res.Resources;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import zendesk.messaging.MessagingItem;
import zendesk.messaging.R$dimen;

/* loaded from: classes3.dex */
public class MessagingCellPropsFactory {
    public final int defaultSpacing;
    public final int groupSpacing;

    /* loaded from: classes3.dex */
    public enum InteractionType {
        QUERY,
        RESPONSE,
        NONE
    }

    public MessagingCellPropsFactory(Resources resources) {
        this.defaultSpacing = resources.getDimensionPixelSize(R$dimen.zui_cell_vertical_spacing_default);
        this.groupSpacing = resources.getDimensionPixelSize(R$dimen.zui_cell_vertical_spacing_group);
    }

    public static InteractionType classifyInteraction(MessagingItem messagingItem) {
        if (messagingItem instanceof MessagingItem.Response) {
            return InteractionType.RESPONSE;
        }
        if (!(messagingItem instanceof MessagingItem.Query) && !(messagingItem instanceof MessagingItem.OptionsResponse)) {
            return InteractionType.NONE;
        }
        return InteractionType.QUERY;
    }

    public int avatarVisibility(MessagingItem messagingItem, MessagingItem messagingItem2) {
        InteractionType classifyInteraction = classifyInteraction(messagingItem);
        if (classifyInteraction == InteractionType.QUERY) {
            return 4;
        }
        if (messagingItem2 != null && classifyInteraction == classifyInteraction(messagingItem2)) {
            return ((messagingItem instanceof MessagingItem.Response) && (messagingItem2 instanceof MessagingItem.Response) && !((MessagingItem.Response) messagingItem).getAgentDetails().getAgentId().equals(((MessagingItem.Response) messagingItem2).getAgentDetails().getAgentId())) ? 0 : 4;
        }
        return 0;
    }

    public int cellSpacing(MessagingItem messagingItem, MessagingItem messagingItem2) {
        if (messagingItem2 == null) {
            return this.defaultSpacing;
        }
        if (messagingItem2 instanceof MessagingItem.SystemMessage) {
            return this.groupSpacing;
        }
        if (classifyInteraction(messagingItem) == classifyInteraction(messagingItem2)) {
            return this.groupSpacing;
        }
        return this.defaultSpacing;
    }

    public List<MessagingCellProps> create(List<MessagingItem> list) {
        if (l10.g(list)) {
            return Collections.emptyList();
        }
        ArrayList arrayList = new ArrayList(list.size());
        int i = 0;
        while (i < list.size()) {
            MessagingItem messagingItem = null;
            MessagingItem messagingItem2 = i > 0 ? list.get(i - 1) : null;
            MessagingItem messagingItem3 = list.get(i);
            i++;
            if (i < list.size()) {
                messagingItem = list.get(i);
            }
            arrayList.add(generateCellProps(messagingItem2, messagingItem3, messagingItem));
        }
        return arrayList;
    }

    public final MessagingCellProps generateCellProps(MessagingItem messagingItem, MessagingItem messagingItem2, MessagingItem messagingItem3) {
        return new MessagingCellProps(labelVisibility(messagingItem2, messagingItem), cellSpacing(messagingItem2, messagingItem3), avatarVisibility(messagingItem2, messagingItem3));
    }

    public int labelVisibility(MessagingItem messagingItem, MessagingItem messagingItem2) {
        InteractionType classifyInteraction = classifyInteraction(messagingItem);
        if (classifyInteraction == InteractionType.QUERY || messagingItem2 == null || classifyInteraction != classifyInteraction(messagingItem2)) {
            return 0;
        }
        return ((messagingItem instanceof MessagingItem.Response) && (messagingItem2 instanceof MessagingItem.Response) && !((MessagingItem.Response) messagingItem).getAgentDetails().getAgentId().equals(((MessagingItem.Response) messagingItem2).getAgentDetails().getAgentId())) ? 0 : 8;
    }
}
