package zendesk.messaging.ui;

import android.view.View;
import androidx.appcompat.app.AppCompatActivity;
import zendesk.belvedere.BelvedereUi;
import zendesk.belvedere.b;
import zendesk.messaging.BelvedereMediaHolder;
import zendesk.messaging.R$id;

/* loaded from: classes3.dex */
public class InputBoxAttachmentClickListener implements View.OnClickListener {
    public final AppCompatActivity activity;
    public final BelvedereMediaHolder belvedereMediaHolder;
    public final b imageStream;

    public InputBoxAttachmentClickListener(AppCompatActivity appCompatActivity, b bVar, BelvedereMediaHolder belvedereMediaHolder) {
        this.activity = appCompatActivity;
        this.imageStream = bVar;
        this.belvedereMediaHolder = belvedereMediaHolder;
    }

    @Override // android.view.View.OnClickListener
    public void onClick(View view) {
        if (!this.imageStream.k()) {
            showImagePicker();
        } else {
            this.imageStream.h();
        }
    }

    public void showImagePicker() {
        BelvedereUi.a(this.activity).g().h("*/*", true).l(this.belvedereMediaHolder.getSelectedMedia()).m(R$id.input_box_attachments_indicator, R$id.input_box_send_btn).j(true).f(this.activity);
    }
}
