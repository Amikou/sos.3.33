package zendesk.messaging.ui;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.util.AttributeSet;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import com.squareup.picasso.Picasso;
import zendesk.messaging.Attachment;
import zendesk.messaging.R$dimen;
import zendesk.messaging.R$drawable;
import zendesk.messaging.R$id;
import zendesk.messaging.R$layout;

/* loaded from: classes3.dex */
public class AgentImageCellView extends LinearLayout implements Updatable<State> {
    public AvatarView avatarView;
    public View botLabel;
    public int cornerRadius;
    public ImageView imageView;
    public View labelContainer;
    public TextView labelField;
    public final Drawable placeholder;

    /* loaded from: classes3.dex */
    public static class State {
        public final Attachment attachment;
        public final AvatarState avatarState;
        public final AvatarStateRenderer avatarStateRenderer;
        public final boolean isBot;
        public final String label;
        public final Picasso picasso;
        public final MessagingCellProps props;

        public State(Picasso picasso, MessagingCellProps messagingCellProps, Attachment attachment, String str, boolean z, AvatarState avatarState, AvatarStateRenderer avatarStateRenderer) {
            this.picasso = picasso;
            this.props = messagingCellProps;
            this.label = str;
            this.isBot = z;
            this.avatarState = avatarState;
            this.avatarStateRenderer = avatarStateRenderer;
        }

        public boolean equals(Object obj) {
            if (this == obj) {
                return true;
            }
            if (obj == null || getClass() != obj.getClass()) {
                return false;
            }
            State state = (State) obj;
            if (isBot() != state.isBot()) {
                return false;
            }
            if (getPicasso() == null ? state.getPicasso() == null : getPicasso().equals(state.getPicasso())) {
                if (getProps() == null ? state.getProps() == null : getProps().equals(state.getProps())) {
                    if (getLabel() == null ? state.getLabel() == null : getLabel().equals(state.getLabel())) {
                        getAttachment();
                        state.getAttachment();
                        if (getAvatarState() != null) {
                            return getAvatarState().equals(state.getAvatarState());
                        }
                        return state.getAvatarState() == null;
                    }
                    return false;
                }
                return false;
            }
            return false;
        }

        public Attachment getAttachment() {
            return this.attachment;
        }

        public AvatarState getAvatarState() {
            return this.avatarState;
        }

        public AvatarStateRenderer getAvatarStateRenderer() {
            return this.avatarStateRenderer;
        }

        public String getLabel() {
            return this.label;
        }

        public Picasso getPicasso() {
            return this.picasso;
        }

        public MessagingCellProps getProps() {
            return this.props;
        }

        public int hashCode() {
            int hashCode = (((getPicasso() != null ? getPicasso().hashCode() : 0) * 31) + (getProps() != null ? getProps().hashCode() : 0)) * 31;
            int hashCode2 = getLabel() != null ? getLabel().hashCode() : 0;
            getAttachment();
            return ((((((hashCode + hashCode2) * 31) + (isBot() ? 1 : 0)) * 31) + 0) * 31) + (getAvatarState() != null ? getAvatarState().hashCode() : 0);
        }

        public boolean isBot() {
            return this.isBot;
        }
    }

    public AgentImageCellView(Context context) {
        super(context);
        this.placeholder = m70.f(getContext(), R$drawable.zui_background_agent_cell);
        init();
    }

    public final void init() {
        setOrientation(0);
        LinearLayout.inflate(getContext(), R$layout.zui_view_agent_image_cell_content, this);
        this.cornerRadius = getResources().getDimensionPixelSize(R$dimen.zui_cell_bubble_corner_radius);
    }

    public final void loadImageIntoImageView(State state) {
        state.getPicasso();
        state.getAttachment();
        throw null;
    }

    @Override // android.view.View
    public void onFinishInflate() {
        super.onFinishInflate();
        this.avatarView = (AvatarView) findViewById(R$id.zui_agent_message_avatar);
        this.imageView = (ImageView) findViewById(R$id.zui_image_cell_image);
        this.labelContainer = findViewById(R$id.zui_cell_status_view);
        this.labelField = (TextView) findViewById(R$id.zui_cell_label_text_field);
        this.botLabel = findViewById(R$id.zui_cell_label_supplementary_label);
    }

    @Override // zendesk.messaging.ui.Updatable
    public void update(final State state) {
        loadImageIntoImageView(state);
        this.labelField.setText(state.getLabel());
        this.botLabel.setVisibility(state.isBot() ? 0 : 8);
        this.imageView.setOnClickListener(new View.OnClickListener() { // from class: zendesk.messaging.ui.AgentImageCellView.1
            @Override // android.view.View.OnClickListener
            public void onClick(View view) {
                state.getAttachment();
                throw null;
            }
        });
        state.getAvatarStateRenderer().render(state.getAvatarState(), this.avatarView);
        state.getProps().apply(this, this.labelContainer, this.avatarView);
    }

    public AgentImageCellView(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        this.placeholder = m70.f(getContext(), R$drawable.zui_background_agent_cell);
        init();
    }

    public AgentImageCellView(Context context, AttributeSet attributeSet, int i) {
        super(context, attributeSet, i);
        this.placeholder = m70.f(getContext(), R$drawable.zui_background_agent_cell);
        init();
    }
}
