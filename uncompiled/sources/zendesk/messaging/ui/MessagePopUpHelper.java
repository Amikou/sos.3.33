package zendesk.messaging.ui;

import android.view.MenuItem;
import android.view.View;
import defpackage.mt2;
import java.util.Set;
import zendesk.messaging.R$id;
import zendesk.messaging.R$menu;

/* loaded from: classes3.dex */
public class MessagePopUpHelper {

    /* loaded from: classes3.dex */
    public enum Option {
        COPY,
        RETRY,
        DELETE
    }

    public static mt2.d createOnMenuItemClickListener(final MessageActionListener messageActionListener, final String str) {
        if (messageActionListener == null) {
            return null;
        }
        return new mt2.d() { // from class: zendesk.messaging.ui.MessagePopUpHelper.1
            @Override // defpackage.mt2.d
            public boolean onMenuItemClick(MenuItem menuItem) {
                if (menuItem.getItemId() == R$id.zui_failed_message_retry) {
                    MessageActionListener.this.retry(str);
                    return true;
                } else if (menuItem.getItemId() == R$id.zui_failed_message_delete) {
                    MessageActionListener.this.delete(str);
                    return true;
                } else if (menuItem.getItemId() == R$id.zui_message_copy) {
                    MessageActionListener.this.copy(str);
                    return true;
                } else {
                    return false;
                }
            }
        };
    }

    public static mt2 createPopUpMenu(View view, int i, mt2.d dVar) {
        mt2 mt2Var = new mt2(view.getContext(), view);
        mt2Var.c(i);
        mt2Var.e(dVar);
        mt2Var.d(8388613);
        return mt2Var;
    }

    public static void showPopUpMenu(View view, Set<Option> set, MessageActionListener messageActionListener, String str) {
        mt2 createPopUpMenu = createPopUpMenu(view, R$menu.zui_message_options_copy_retry_delete, createOnMenuItemClickListener(messageActionListener, str));
        createPopUpMenu.a().getItem(0).setVisible(set.contains(Option.COPY));
        createPopUpMenu.a().getItem(1).setVisible(set.contains(Option.RETRY));
        createPopUpMenu.a().getItem(2).setVisible(set.contains(Option.DELETE));
        createPopUpMenu.f();
    }
}
