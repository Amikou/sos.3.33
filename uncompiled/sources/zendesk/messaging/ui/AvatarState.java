package zendesk.messaging.ui;

/* loaded from: classes3.dex */
public class AvatarState {
    public final String avatarLetter;
    public final Integer avatarRes;
    public final String avatarUrl;
    public final Object uniqueIdentifier;

    public AvatarState(Object obj, String str, String str2, Integer num) {
        this.uniqueIdentifier = obj;
        this.avatarLetter = str;
        this.avatarUrl = str2;
        this.avatarRes = num;
    }

    public String getAvatarLetter() {
        return this.avatarLetter;
    }

    public Integer getAvatarRes() {
        return this.avatarRes;
    }

    public String getAvatarUrl() {
        return this.avatarUrl;
    }

    public Object getUniqueIdentifier() {
        return this.uniqueIdentifier;
    }
}
