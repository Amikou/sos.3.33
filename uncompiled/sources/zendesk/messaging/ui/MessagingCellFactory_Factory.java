package zendesk.messaging.ui;

import zendesk.messaging.EventFactory;
import zendesk.messaging.EventListener;
import zendesk.messaging.components.DateProvider;

/* loaded from: classes3.dex */
public final class MessagingCellFactory_Factory implements y11<MessagingCellFactory> {
    public final ew2<AvatarStateFactory> avatarStateFactoryProvider;
    public final ew2<AvatarStateRenderer> avatarStateRendererProvider;
    public final ew2<MessagingCellPropsFactory> cellPropsFactoryProvider;
    public final ew2<DateProvider> dateProvider;
    public final ew2<EventFactory> eventFactoryProvider;
    public final ew2<EventListener> eventListenerProvider;
    public final ew2<Boolean> multilineResponseOptionsEnabledProvider;

    public MessagingCellFactory_Factory(ew2<MessagingCellPropsFactory> ew2Var, ew2<DateProvider> ew2Var2, ew2<EventListener> ew2Var3, ew2<EventFactory> ew2Var4, ew2<AvatarStateRenderer> ew2Var5, ew2<AvatarStateFactory> ew2Var6, ew2<Boolean> ew2Var7) {
        this.cellPropsFactoryProvider = ew2Var;
        this.dateProvider = ew2Var2;
        this.eventListenerProvider = ew2Var3;
        this.eventFactoryProvider = ew2Var4;
        this.avatarStateRendererProvider = ew2Var5;
        this.avatarStateFactoryProvider = ew2Var6;
        this.multilineResponseOptionsEnabledProvider = ew2Var7;
    }

    public static MessagingCellFactory_Factory create(ew2<MessagingCellPropsFactory> ew2Var, ew2<DateProvider> ew2Var2, ew2<EventListener> ew2Var3, ew2<EventFactory> ew2Var4, ew2<AvatarStateRenderer> ew2Var5, ew2<AvatarStateFactory> ew2Var6, ew2<Boolean> ew2Var7) {
        return new MessagingCellFactory_Factory(ew2Var, ew2Var2, ew2Var3, ew2Var4, ew2Var5, ew2Var6, ew2Var7);
    }

    public static MessagingCellFactory newInstance(MessagingCellPropsFactory messagingCellPropsFactory, DateProvider dateProvider, EventListener eventListener, EventFactory eventFactory, Object obj, Object obj2, boolean z) {
        return new MessagingCellFactory(messagingCellPropsFactory, dateProvider, eventListener, eventFactory, (AvatarStateRenderer) obj, (AvatarStateFactory) obj2, z);
    }

    @Override // defpackage.ew2
    public MessagingCellFactory get() {
        return newInstance(this.cellPropsFactoryProvider.get(), this.dateProvider.get(), this.eventListenerProvider.get(), this.eventFactoryProvider.get(), this.avatarStateRendererProvider.get(), this.avatarStateFactoryProvider.get(), this.multilineResponseOptionsEnabledProvider.get().booleanValue());
    }
}
