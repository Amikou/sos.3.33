package zendesk.messaging.ui;

import android.content.Context;

/* loaded from: classes3.dex */
public interface OnArticleSuggestionSelectionListener {
    void onArticleSuggestionSelected(Context context);
}
