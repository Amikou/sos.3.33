package zendesk.messaging.ui;

import android.view.View;
import zendesk.messaging.ui.Updatable;

/* loaded from: classes3.dex */
public class MessagingCell<T, V extends View & Updatable<T>> {
    public final String id;
    public final int layoutRes;
    public final T state;
    public final Class<V> viewClassType;

    public MessagingCell(String str, T t, int i, Class<V> cls) {
        this.id = str;
        this.state = t;
        this.layoutRes = i;
        this.viewClassType = cls;
    }

    public boolean areContentsTheSame(MessagingCell messagingCell) {
        return getId().equals(messagingCell.getId()) && messagingCell.state.equals(this.state);
    }

    public void bind(V v) {
        ((Updatable) v).update(this.state);
    }

    public String getId() {
        return this.id;
    }

    public int getLayoutRes() {
        return this.layoutRes;
    }

    public Class<V> getViewClassType() {
        return this.viewClassType;
    }
}
