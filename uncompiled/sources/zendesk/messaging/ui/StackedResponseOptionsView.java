package zendesk.messaging.ui;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.util.AttributeSet;
import android.widget.FrameLayout;
import androidx.recyclerview.widget.RecyclerView;
import com.google.android.flexbox.FlexboxLayoutManager;
import zendesk.messaging.MessagingItem;
import zendesk.messaging.R$drawable;
import zendesk.messaging.R$id;
import zendesk.messaging.R$layout;

/* loaded from: classes3.dex */
public class StackedResponseOptionsView extends FrameLayout implements Updatable<ResponseOptionsViewState> {
    public ResponseOptionsAdapter adapter;

    public StackedResponseOptionsView(Context context) {
        super(context);
        init();
    }

    public final void init() {
        FrameLayout.inflate(getContext(), R$layout.zui_view_response_options_content, this);
    }

    @Override // android.view.View
    public void onFinishInflate() {
        super.onFinishInflate();
        RecyclerView recyclerView = (RecyclerView) findViewById(R$id.zui_response_options_recycler);
        recyclerView.setItemAnimator(null);
        a71 a71Var = new a71(getContext());
        a71Var.n(3);
        Drawable f = m70.f(getContext(), R$drawable.zui_view_stacked_response_options_divider);
        if (f != null) {
            a71Var.k(f);
        }
        recyclerView.setLayoutManager(new FlexboxLayoutManager(getContext(), 1));
        recyclerView.h(a71Var);
        ResponseOptionsAdapter responseOptionsAdapter = new ResponseOptionsAdapter();
        this.adapter = responseOptionsAdapter;
        recyclerView.setAdapter(responseOptionsAdapter);
    }

    @Override // zendesk.messaging.ui.Updatable
    public void update(final ResponseOptionsViewState responseOptionsViewState) {
        responseOptionsViewState.getProps().apply(this);
        this.adapter.setResponseOptionHandler(new ResponseOptionHandler() { // from class: zendesk.messaging.ui.StackedResponseOptionsView.1
            @Override // zendesk.messaging.ui.ResponseOptionHandler
            public void onResponseOptionSelected(MessagingItem.Option option) {
                StackedResponseOptionsView.this.adapter.setSelectedOption(option);
                responseOptionsViewState.getListener().onResponseOptionSelected(option);
            }
        });
        this.adapter.submitList(responseOptionsViewState.getOptions());
    }

    public StackedResponseOptionsView(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        init();
    }

    public StackedResponseOptionsView(Context context, AttributeSet attributeSet, int i) {
        super(context, attributeSet, i);
        init();
    }
}
