package zendesk.messaging.ui;

import zendesk.messaging.MessagingItem;

/* loaded from: classes3.dex */
public interface ResponseOptionHandler {
    void onResponseOptionSelected(MessagingItem.Option option);
}
