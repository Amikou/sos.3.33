package zendesk.messaging.ui;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import androidx.recyclerview.widget.RecyclerView;
import androidx.recyclerview.widget.g;
import androidx.recyclerview.widget.o;
import java.util.List;
import zendesk.messaging.MessagingItem;
import zendesk.messaging.R$id;
import zendesk.messaging.R$layout;

/* loaded from: classes3.dex */
public class ResponseOptionsAdapter extends o<MessagingItem.Option, RecyclerView.a0> {
    public boolean canSelectOption;
    public ResponseOptionHandler responseOptionHandler;
    public MessagingItem.Option selectedOption;

    /* loaded from: classes3.dex */
    public static class ResponseOptionsDiffCallback extends g.f<MessagingItem.Option> {
        public ResponseOptionsDiffCallback() {
        }

        @Override // androidx.recyclerview.widget.g.f
        public boolean areContentsTheSame(MessagingItem.Option option, MessagingItem.Option option2) {
            return option.equals(option2);
        }

        @Override // androidx.recyclerview.widget.g.f
        public boolean areItemsTheSame(MessagingItem.Option option, MessagingItem.Option option2) {
            return option.equals(option2);
        }
    }

    public ResponseOptionsAdapter() {
        super(new ResponseOptionsDiffCallback());
        this.canSelectOption = true;
        this.selectedOption = null;
    }

    @Override // androidx.recyclerview.widget.RecyclerView.Adapter
    public int getItemViewType(int i) {
        if (getItem(i) == this.selectedOption) {
            return R$layout.zui_response_options_selected_option;
        }
        return R$layout.zui_response_options_option;
    }

    public final void notifyItemChanged(MessagingItem.Option option) {
        for (int i = 0; i < getItemCount(); i++) {
            if (getItem(i).equals(option)) {
                notifyItemChanged(i);
                return;
            }
        }
    }

    @Override // androidx.recyclerview.widget.RecyclerView.Adapter
    public void onBindViewHolder(final RecyclerView.a0 a0Var, int i) {
        final MessagingItem.Option item = getItem(i);
        ((TextView) a0Var.itemView.findViewById(R$id.zui_response_option_text)).setText(item.getText());
        a0Var.itemView.setOnClickListener(new View.OnClickListener() { // from class: zendesk.messaging.ui.ResponseOptionsAdapter.2
            @Override // android.view.View.OnClickListener
            public void onClick(View view) {
                if (ResponseOptionsAdapter.this.canSelectOption) {
                    if (ResponseOptionsAdapter.this.responseOptionHandler != null) {
                        a0Var.itemView.post(new Runnable() { // from class: zendesk.messaging.ui.ResponseOptionsAdapter.2.1
                            @Override // java.lang.Runnable
                            public void run() {
                                ResponseOptionsAdapter.this.responseOptionHandler.onResponseOptionSelected(item);
                            }
                        });
                    }
                    ResponseOptionsAdapter.this.canSelectOption = false;
                }
            }
        });
    }

    @Override // androidx.recyclerview.widget.RecyclerView.Adapter
    public RecyclerView.a0 onCreateViewHolder(ViewGroup viewGroup, int i) {
        return new RecyclerView.a0(LayoutInflater.from(viewGroup.getContext()).inflate(i, viewGroup, false)) { // from class: zendesk.messaging.ui.ResponseOptionsAdapter.1
        };
    }

    public void setResponseOptionHandler(ResponseOptionHandler responseOptionHandler) {
        this.responseOptionHandler = responseOptionHandler;
    }

    public void setSelectedOption(MessagingItem.Option option) {
        this.selectedOption = option;
        notifyItemChanged(option);
    }

    @Override // androidx.recyclerview.widget.o
    public void submitList(List<MessagingItem.Option> list) {
        super.submitList(list);
        this.canSelectOption = true;
        this.selectedOption = null;
    }
}
