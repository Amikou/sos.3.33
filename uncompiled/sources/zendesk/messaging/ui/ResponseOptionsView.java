package zendesk.messaging.ui;

import android.content.Context;
import android.graphics.Rect;
import android.util.AttributeSet;
import android.view.View;
import android.widget.FrameLayout;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import java.util.Collections;
import zendesk.messaging.MessagingItem;
import zendesk.messaging.R$dimen;
import zendesk.messaging.R$id;
import zendesk.messaging.R$layout;

/* loaded from: classes3.dex */
public class ResponseOptionsView extends FrameLayout implements Updatable<ResponseOptionsViewState> {
    public ResponseOptionsAdapter adapter;

    /* loaded from: classes3.dex */
    public static class ItemOffsetDecoration extends RecyclerView.n {
        public int itemOffset;

        public ItemOffsetDecoration(Context context, int i) {
            this.itemOffset = context.getResources().getDimensionPixelSize(i);
        }

        @Override // androidx.recyclerview.widget.RecyclerView.n
        public void getItemOffsets(Rect rect, View view, RecyclerView recyclerView, RecyclerView.x xVar) {
            super.getItemOffsets(rect, view, recyclerView, xVar);
            int f0 = recyclerView.f0(view);
            if (f0 == -1) {
                return;
            }
            boolean z = f0 == 0;
            if (ei4.E(recyclerView) == 0) {
                if (z) {
                    return;
                }
                rect.set(0, 0, this.itemOffset, 0);
            } else if (z) {
            } else {
                rect.set(this.itemOffset, 0, 0, 0);
            }
        }
    }

    public ResponseOptionsView(Context context) {
        super(context);
        init();
    }

    public final void init() {
        FrameLayout.inflate(getContext(), R$layout.zui_view_response_options_content, this);
    }

    @Override // android.view.View
    public void onFinishInflate() {
        super.onFinishInflate();
        RecyclerView recyclerView = (RecyclerView) findViewById(R$id.zui_response_options_recycler);
        recyclerView.setLayoutManager(new LinearLayoutManager(getContext(), 0, true));
        ResponseOptionsAdapter responseOptionsAdapter = new ResponseOptionsAdapter();
        this.adapter = responseOptionsAdapter;
        recyclerView.setAdapter(responseOptionsAdapter);
        recyclerView.h(new ItemOffsetDecoration(getContext(), R$dimen.zui_cell_response_options_horizontal_spacing));
    }

    @Override // zendesk.messaging.ui.Updatable
    public void update(final ResponseOptionsViewState responseOptionsViewState) {
        responseOptionsViewState.getProps().apply(this);
        this.adapter.setResponseOptionHandler(new ResponseOptionHandler() { // from class: zendesk.messaging.ui.ResponseOptionsView.1
            @Override // zendesk.messaging.ui.ResponseOptionHandler
            public void onResponseOptionSelected(MessagingItem.Option option) {
                ResponseOptionsView.this.adapter.submitList(Collections.singletonList(option));
                responseOptionsViewState.getListener().onResponseOptionSelected(option);
            }
        });
        this.adapter.submitList(responseOptionsViewState.getOptions());
    }

    public ResponseOptionsView(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        init();
    }

    public ResponseOptionsView(Context context, AttributeSet attributeSet, int i) {
        super(context, attributeSet, i);
        init();
    }
}
