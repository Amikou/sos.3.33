package zendesk.messaging.ui;

/* loaded from: classes3.dex */
public final class AvatarStateFactory_Factory implements y11<AvatarStateFactory> {

    /* loaded from: classes3.dex */
    public static final class InstanceHolder {
        public static final AvatarStateFactory_Factory INSTANCE = new AvatarStateFactory_Factory();
    }

    public static AvatarStateFactory_Factory create() {
        return InstanceHolder.INSTANCE;
    }

    public static AvatarStateFactory newInstance() {
        return new AvatarStateFactory();
    }

    @Override // defpackage.ew2
    public AvatarStateFactory get() {
        return newInstance();
    }
}
