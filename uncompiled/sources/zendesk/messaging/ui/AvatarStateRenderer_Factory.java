package zendesk.messaging.ui;

import com.squareup.picasso.Picasso;

/* loaded from: classes3.dex */
public final class AvatarStateRenderer_Factory implements y11<AvatarStateRenderer> {
    public final ew2<Picasso> picassoProvider;

    public AvatarStateRenderer_Factory(ew2<Picasso> ew2Var) {
        this.picassoProvider = ew2Var;
    }

    public static AvatarStateRenderer_Factory create(ew2<Picasso> ew2Var) {
        return new AvatarStateRenderer_Factory(ew2Var);
    }

    public static AvatarStateRenderer newInstance(Picasso picasso) {
        return new AvatarStateRenderer(picasso);
    }

    @Override // defpackage.ew2
    public AvatarStateRenderer get() {
        return newInstance(this.picassoProvider.get());
    }
}
