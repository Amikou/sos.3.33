package zendesk.messaging.ui;

import zendesk.messaging.MessagingItem;

/* loaded from: classes3.dex */
public class EndUserCellMessageState extends EndUserCellBaseState {
    public final String message;

    public EndUserCellMessageState(String str, MessagingCellProps messagingCellProps, MessagingItem.Query.Status status, MessageActionListener messageActionListener, String str2) {
        super(str, messagingCellProps, status, messageActionListener);
        this.message = str2;
    }

    @Override // zendesk.messaging.ui.EndUserCellBaseState
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj != null && getClass() == obj.getClass() && super.equals(obj)) {
            String str = this.message;
            String str2 = ((EndUserCellMessageState) obj).message;
            return str != null ? str.equals(str2) : str2 == null;
        }
        return false;
    }

    public String getMessage() {
        return this.message;
    }

    @Override // zendesk.messaging.ui.EndUserCellBaseState
    public int hashCode() {
        int hashCode = super.hashCode() * 31;
        String str = this.message;
        return hashCode + (str != null ? str.hashCode() : 0);
    }
}
