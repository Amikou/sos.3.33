package zendesk.messaging.ui;

import android.content.Context;
import android.graphics.PorterDuff;
import android.graphics.PorterDuffColorFilter;
import android.util.AttributeSet;
import androidx.appcompat.widget.AppCompatTextView;
import zendesk.messaging.R$attr;
import zendesk.messaging.R$color;
import zendesk.messaging.R$drawable;

/* loaded from: classes3.dex */
public class ResponseOptionSelectedView extends AppCompatTextView {
    public ResponseOptionSelectedView(Context context) {
        super(context);
        init();
    }

    public final void init() {
        setTextColor(m70.d(getContext(), R$color.zui_color_white_100));
        setBackgroundDrawable(m70.f(getContext(), R$drawable.zui_background_response_option_selected));
        getBackground().mutate().setColorFilter(new PorterDuffColorFilter(ne4.e(R$attr.colorPrimary, getContext(), R$color.zui_color_primary), PorterDuff.Mode.SRC_ATOP));
    }

    public ResponseOptionSelectedView(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        init();
    }

    public ResponseOptionSelectedView(Context context, AttributeSet attributeSet, int i) {
        super(context, attributeSet, i);
        init();
    }
}
