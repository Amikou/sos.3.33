package zendesk.messaging.ui;

import androidx.appcompat.app.AppCompatActivity;
import zendesk.belvedere.b;
import zendesk.messaging.BelvedereMediaHolder;
import zendesk.messaging.MessagingViewModel;
import zendesk.messaging.TypingEventDispatcher;

/* loaded from: classes3.dex */
public final class MessagingComposer_Factory implements y11<MessagingComposer> {
    public final ew2<AppCompatActivity> appCompatActivityProvider;
    public final ew2<BelvedereMediaHolder> belvedereMediaHolderProvider;
    public final ew2<b> imageStreamProvider;
    public final ew2<InputBoxAttachmentClickListener> inputBoxAttachmentClickListenerProvider;
    public final ew2<InputBoxConsumer> inputBoxConsumerProvider;
    public final ew2<MessagingViewModel> messagingViewModelProvider;
    public final ew2<TypingEventDispatcher> typingEventDispatcherProvider;

    public MessagingComposer_Factory(ew2<AppCompatActivity> ew2Var, ew2<MessagingViewModel> ew2Var2, ew2<b> ew2Var3, ew2<BelvedereMediaHolder> ew2Var4, ew2<InputBoxConsumer> ew2Var5, ew2<InputBoxAttachmentClickListener> ew2Var6, ew2<TypingEventDispatcher> ew2Var7) {
        this.appCompatActivityProvider = ew2Var;
        this.messagingViewModelProvider = ew2Var2;
        this.imageStreamProvider = ew2Var3;
        this.belvedereMediaHolderProvider = ew2Var4;
        this.inputBoxConsumerProvider = ew2Var5;
        this.inputBoxAttachmentClickListenerProvider = ew2Var6;
        this.typingEventDispatcherProvider = ew2Var7;
    }

    public static MessagingComposer_Factory create(ew2<AppCompatActivity> ew2Var, ew2<MessagingViewModel> ew2Var2, ew2<b> ew2Var3, ew2<BelvedereMediaHolder> ew2Var4, ew2<InputBoxConsumer> ew2Var5, ew2<InputBoxAttachmentClickListener> ew2Var6, ew2<TypingEventDispatcher> ew2Var7) {
        return new MessagingComposer_Factory(ew2Var, ew2Var2, ew2Var3, ew2Var4, ew2Var5, ew2Var6, ew2Var7);
    }

    public static MessagingComposer newInstance(AppCompatActivity appCompatActivity, MessagingViewModel messagingViewModel, b bVar, BelvedereMediaHolder belvedereMediaHolder, InputBoxConsumer inputBoxConsumer, Object obj, TypingEventDispatcher typingEventDispatcher) {
        return new MessagingComposer(appCompatActivity, messagingViewModel, bVar, belvedereMediaHolder, inputBoxConsumer, (InputBoxAttachmentClickListener) obj, typingEventDispatcher);
    }

    @Override // defpackage.ew2
    public MessagingComposer get() {
        return newInstance(this.appCompatActivityProvider.get(), this.messagingViewModelProvider.get(), this.imageStreamProvider.get(), this.belvedereMediaHolderProvider.get(), this.inputBoxConsumerProvider.get(), this.inputBoxAttachmentClickListenerProvider.get(), this.typingEventDispatcherProvider.get());
    }
}
