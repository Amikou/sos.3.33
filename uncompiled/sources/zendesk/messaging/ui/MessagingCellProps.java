package zendesk.messaging.ui;

import android.view.View;
import android.view.ViewGroup;
import androidx.recyclerview.widget.RecyclerView;

/* loaded from: classes3.dex */
public class MessagingCellProps {
    public final int avatarVisibility;
    public final int cellSpacing;
    public final int labelVisibility;

    public MessagingCellProps(int i, int i2, int i3) {
        this.labelVisibility = i;
        this.cellSpacing = i2;
        this.avatarVisibility = i3;
    }

    public void apply(View view) {
        apply(view, null, null);
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null || getClass() != obj.getClass()) {
            return false;
        }
        MessagingCellProps messagingCellProps = (MessagingCellProps) obj;
        return this.labelVisibility == messagingCellProps.labelVisibility && this.cellSpacing == messagingCellProps.cellSpacing;
    }

    public int hashCode() {
        return (this.labelVisibility * 31) + this.cellSpacing;
    }

    public void apply(View view, View view2) {
        apply(view, view2, null);
    }

    public void apply(View view, View view2, View view3) {
        if (view2 != null) {
            view2.setVisibility(this.labelVisibility);
        }
        if (view3 != null) {
            view3.setVisibility(this.avatarVisibility);
        }
        ((ViewGroup.MarginLayoutParams) ((RecyclerView.LayoutParams) view.getLayoutParams())).bottomMargin = this.cellSpacing;
        view.requestLayout();
    }
}
