package zendesk.messaging.ui;

/* loaded from: classes3.dex */
public interface MessageActionListener {
    void copy(String str);

    void delete(String str);

    void retry(String str);
}
