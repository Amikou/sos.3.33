package zendesk.messaging.ui;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.util.AttributeSet;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import zendesk.messaging.Attachment;
import zendesk.messaging.R$attr;
import zendesk.messaging.R$color;
import zendesk.messaging.R$drawable;
import zendesk.messaging.R$id;
import zendesk.messaging.R$layout;

/* loaded from: classes3.dex */
public class AgentFileCellView extends LinearLayout implements Updatable<State> {
    public ImageView appIcon;
    public AvatarView avatarView;
    public View botLabel;
    public LinearLayout bubble;
    public Drawable defaultAppIcon;
    public TextView fileDescriptor;
    public TextView fileName;
    public View labelContainer;
    public TextView labelField;

    /* loaded from: classes3.dex */
    public static class State {
        public final Attachment attachment;
        public final AvatarState avatarState;
        public final AvatarStateRenderer avatarStateRenderer;
        public final boolean isBot;
        public final String label;
        public final MessagingCellProps props;

        public State(Attachment attachment, MessagingCellProps messagingCellProps, String str, boolean z, AvatarState avatarState, AvatarStateRenderer avatarStateRenderer) {
            this.props = messagingCellProps;
            this.label = str;
            this.isBot = z;
            this.avatarState = avatarState;
            this.avatarStateRenderer = avatarStateRenderer;
        }

        public boolean equals(Object obj) {
            if (this == obj) {
                return true;
            }
            if (obj == null || getClass() != obj.getClass()) {
                return false;
            }
            State state = (State) obj;
            if (isBot() != state.isBot()) {
                return false;
            }
            getAttachment();
            state.getAttachment();
            if (getProps() == null ? state.getProps() == null : getProps().equals(state.getProps())) {
                if (getLabel() == null ? state.getLabel() == null : getLabel().equals(state.getLabel())) {
                    AvatarState avatarState = this.avatarState;
                    AvatarState avatarState2 = state.avatarState;
                    return avatarState != null ? avatarState.equals(avatarState2) : avatarState2 == null;
                }
                return false;
            }
            return false;
        }

        public Attachment getAttachment() {
            return this.attachment;
        }

        public String getLabel() {
            return this.label;
        }

        public MessagingCellProps getProps() {
            return this.props;
        }

        public int hashCode() {
            getAttachment();
            int hashCode = ((((((0 * 31) + (getProps() != null ? getProps().hashCode() : 0)) * 31) + (getLabel() != null ? getLabel().hashCode() : 0)) * 31) + (isBot() ? 1 : 0)) * 31;
            AvatarState avatarState = this.avatarState;
            return hashCode + (avatarState != null ? avatarState.hashCode() : 0);
        }

        public boolean isBot() {
            return this.isBot;
        }
    }

    public AgentFileCellView(Context context) {
        super(context);
        init();
    }

    private void setBubbleClickListeners(final State state) {
        this.bubble.setOnClickListener(new View.OnClickListener() { // from class: zendesk.messaging.ui.AgentFileCellView.1
            @Override // android.view.View.OnClickListener
            public void onClick(View view) {
                state.getAttachment();
                throw null;
            }
        });
    }

    public final void init() {
        setOrientation(0);
        LinearLayout.inflate(getContext(), R$layout.zui_view_agent_file_cell_content, this);
    }

    @Override // android.view.View
    public void onFinishInflate() {
        super.onFinishInflate();
        this.avatarView = (AvatarView) findViewById(R$id.zui_agent_message_avatar);
        this.bubble = (LinearLayout) findViewById(R$id.zui_cell_file_container);
        this.fileName = (TextView) findViewById(R$id.zui_file_cell_name);
        this.fileDescriptor = (TextView) findViewById(R$id.zui_cell_file_description);
        this.appIcon = (ImageView) findViewById(R$id.zui_cell_file_app_icon);
        this.labelContainer = findViewById(R$id.zui_cell_status_view);
        this.labelField = (TextView) findViewById(R$id.zui_cell_label_text_field);
        this.botLabel = findViewById(R$id.zui_cell_label_supplementary_label);
        this.defaultAppIcon = m70.f(getContext(), R$drawable.zui_ic_insert_drive_file);
        ne4.c(ne4.e(R$attr.colorPrimary, getContext(), R$color.zui_color_primary), this.defaultAppIcon, this.appIcon);
    }

    @Override // zendesk.messaging.ui.Updatable
    public void update(State state) {
        state.getAttachment();
        throw null;
    }

    public AgentFileCellView(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        init();
    }

    public AgentFileCellView(Context context, AttributeSet attributeSet, int i) {
        super(context, attributeSet, i);
        init();
    }
}
