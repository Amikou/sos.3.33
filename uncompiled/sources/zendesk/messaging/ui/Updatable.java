package zendesk.messaging.ui;

/* loaded from: classes3.dex */
public interface Updatable<T> {
    void update(T t);
}
