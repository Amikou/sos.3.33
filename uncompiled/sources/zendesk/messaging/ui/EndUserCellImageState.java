package zendesk.messaging.ui;

import com.squareup.picasso.Picasso;
import zendesk.messaging.Attachment;
import zendesk.messaging.AttachmentSettings;
import zendesk.messaging.MessagingItem;

/* loaded from: classes3.dex */
public class EndUserCellImageState extends EndUserCellFileState {
    public final Picasso picasso;

    public EndUserCellImageState(String str, MessagingCellProps messagingCellProps, MessagingItem.Query.Status status, MessageActionListener messageActionListener, Attachment attachment, MessagingItem.FileQuery.FailureReason failureReason, AttachmentSettings attachmentSettings, Picasso picasso) {
        super(str, messagingCellProps, status, messageActionListener, attachment, failureReason, attachmentSettings);
        this.picasso = picasso;
    }

    @Override // zendesk.messaging.ui.EndUserCellFileState, zendesk.messaging.ui.EndUserCellBaseState
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj != null && getClass() == obj.getClass() && super.equals(obj)) {
            Picasso picasso = this.picasso;
            Picasso picasso2 = ((EndUserCellImageState) obj).picasso;
            return picasso != null ? picasso.equals(picasso2) : picasso2 == null;
        }
        return false;
    }

    @Override // zendesk.messaging.ui.EndUserCellFileState, zendesk.messaging.ui.EndUserCellBaseState
    public int hashCode() {
        int hashCode = super.hashCode() * 31;
        Picasso picasso = this.picasso;
        return hashCode + (picasso != null ? picasso.hashCode() : 0);
    }
}
