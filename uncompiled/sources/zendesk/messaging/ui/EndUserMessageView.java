package zendesk.messaging.ui;

import android.content.Context;
import android.util.AttributeSet;
import android.widget.LinearLayout;
import android.widget.TextView;
import zendesk.messaging.MessagingItem;
import zendesk.messaging.R$color;
import zendesk.messaging.R$id;
import zendesk.messaging.R$layout;

/* loaded from: classes3.dex */
public class EndUserMessageView extends LinearLayout implements Updatable<EndUserCellMessageState> {
    public int errorTextColor;
    public TextView label;
    public MessageStatusView statusView;
    public int textColor;
    public TextView textField;

    public EndUserMessageView(Context context) {
        super(context);
        init();
    }

    public final void init() {
        setOrientation(1);
        setGravity(8388693);
        LinearLayout.inflate(getContext(), R$layout.zui_view_end_user_message_cell_content, this);
    }

    @Override // android.view.View
    public void onFinishInflate() {
        super.onFinishInflate();
        this.textField = (TextView) findViewById(R$id.zui_end_user_message_cell_text_field);
        this.statusView = (MessageStatusView) findViewById(R$id.zui_cell_status_view);
        this.label = (TextView) findViewById(R$id.zui_cell_label_message);
        Context context = getContext();
        this.errorTextColor = ne4.b(R$color.zui_text_color_dark_primary, context);
        this.textColor = ne4.b(R$color.zui_text_color_light_primary, context);
    }

    @Override // zendesk.messaging.ui.Updatable
    public void update(EndUserCellMessageState endUserCellMessageState) {
        UtilsEndUserCellView.setClickListener(endUserCellMessageState, this);
        UtilsEndUserCellView.setLongClickListener(endUserCellMessageState, this);
        UtilsEndUserCellView.setLabelErrorMessage(endUserCellMessageState, this.label, getContext());
        UtilsEndUserCellView.setCellBackground(endUserCellMessageState, this.textField);
        MessagingItem.Query.Status status = endUserCellMessageState.getStatus();
        this.textField.setTextColor(UtilsEndUserCellView.isFailedCell(endUserCellMessageState) ? this.errorTextColor : this.textColor);
        this.textField.setText(endUserCellMessageState.getMessage());
        this.textField.setTextIsSelectable(status == MessagingItem.Query.Status.DELIVERED);
        this.textField.requestLayout();
        this.statusView.setStatus(status);
        endUserCellMessageState.getProps().apply(this, this.statusView);
    }

    public EndUserMessageView(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        init();
    }

    public EndUserMessageView(Context context, AttributeSet attributeSet, int i) {
        super(context, attributeSet, i);
        init();
    }
}
