package zendesk.messaging.ui;

import android.content.Context;
import android.util.AttributeSet;
import android.widget.LinearLayout;
import android.widget.TextView;
import zendesk.messaging.R$id;
import zendesk.messaging.R$layout;

/* loaded from: classes3.dex */
public class SystemMessageView extends LinearLayout implements Updatable<State> {
    public TextView systemMessage;

    /* loaded from: classes3.dex */
    public static class State {
        public final MessagingCellProps props;
        public final String text;

        public State(MessagingCellProps messagingCellProps, String str) {
            this.props = messagingCellProps;
            this.text = str;
        }

        public boolean equals(Object obj) {
            if (this == obj) {
                return true;
            }
            if (obj == null || getClass() != obj.getClass()) {
                return false;
            }
            State state = (State) obj;
            String str = this.text;
            if (str == null ? state.text == null : str.equals(state.text)) {
                MessagingCellProps messagingCellProps = this.props;
                MessagingCellProps messagingCellProps2 = state.props;
                return messagingCellProps != null ? messagingCellProps.equals(messagingCellProps2) : messagingCellProps2 == null;
            }
            return false;
        }

        public String getText() {
            return this.text;
        }

        public int hashCode() {
            String str = this.text;
            int hashCode = (str != null ? str.hashCode() : 0) * 31;
            MessagingCellProps messagingCellProps = this.props;
            return hashCode + (messagingCellProps != null ? messagingCellProps.hashCode() : 0);
        }
    }

    public SystemMessageView(Context context) {
        super(context);
        init();
    }

    public final void init() {
        setOrientation(1);
        LinearLayout.inflate(getContext(), R$layout.zui_view_system_message, this);
        this.systemMessage = (TextView) findViewById(R$id.zui_system_message_text);
    }

    @Override // zendesk.messaging.ui.Updatable
    public void update(State state) {
        state.props.apply(this);
        this.systemMessage.setText(state.getText());
    }

    public SystemMessageView(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        init();
    }

    public SystemMessageView(Context context, AttributeSet attributeSet, int i) {
        super(context, attributeSet, i);
        init();
    }
}
