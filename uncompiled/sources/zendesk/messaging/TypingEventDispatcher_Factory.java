package zendesk.messaging;

import android.os.Handler;

/* loaded from: classes3.dex */
public final class TypingEventDispatcher_Factory implements y11<TypingEventDispatcher> {
    public final ew2<EventFactory> eventFactoryProvider;
    public final ew2<EventListener> eventListenerProvider;
    public final ew2<Handler> handlerProvider;

    public TypingEventDispatcher_Factory(ew2<EventListener> ew2Var, ew2<Handler> ew2Var2, ew2<EventFactory> ew2Var3) {
        this.eventListenerProvider = ew2Var;
        this.handlerProvider = ew2Var2;
        this.eventFactoryProvider = ew2Var3;
    }

    public static TypingEventDispatcher_Factory create(ew2<EventListener> ew2Var, ew2<Handler> ew2Var2, ew2<EventFactory> ew2Var3) {
        return new TypingEventDispatcher_Factory(ew2Var, ew2Var2, ew2Var3);
    }

    public static TypingEventDispatcher newInstance(EventListener eventListener, Handler handler, EventFactory eventFactory) {
        return new TypingEventDispatcher(eventListener, handler, eventFactory);
    }

    @Override // defpackage.ew2
    public TypingEventDispatcher get() {
        return newInstance(this.eventListenerProvider.get(), this.handlerProvider.get(), this.eventFactoryProvider.get());
    }
}
