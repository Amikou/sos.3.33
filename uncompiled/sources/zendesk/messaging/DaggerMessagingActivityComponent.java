package zendesk.messaging;

import android.content.res.Resources;
import android.os.Handler;
import androidx.appcompat.app.AppCompatActivity;
import com.squareup.picasso.Picasso;
import zendesk.belvedere.a;
import zendesk.belvedere.b;
import zendesk.messaging.MessagingActivityComponent;
import zendesk.messaging.components.DateProvider;
import zendesk.messaging.ui.AvatarStateFactory_Factory;
import zendesk.messaging.ui.AvatarStateRenderer_Factory;
import zendesk.messaging.ui.InputBoxAttachmentClickListener_Factory;
import zendesk.messaging.ui.InputBoxConsumer;
import zendesk.messaging.ui.InputBoxConsumer_Factory;
import zendesk.messaging.ui.MessagingCellFactory;
import zendesk.messaging.ui.MessagingCellFactory_Factory;
import zendesk.messaging.ui.MessagingCellPropsFactory;
import zendesk.messaging.ui.MessagingCellPropsFactory_Factory;
import zendesk.messaging.ui.MessagingComposer;
import zendesk.messaging.ui.MessagingComposer_Factory;

/* loaded from: classes3.dex */
public final class DaggerMessagingActivityComponent implements MessagingActivityComponent {
    public ew2<AppCompatActivity> activityProvider;
    public ew2 avatarStateRendererProvider;
    public ew2<BelvedereMediaHolder> belvedereMediaHolderProvider;
    public ew2<BelvedereMediaResolverCallback> belvedereMediaResolverCallbackProvider;
    public ew2<a> belvedereProvider;
    public ew2<b> belvedereUiProvider;
    public ew2<DateProvider> dateProvider;
    public ew2<EventFactory> eventFactoryProvider;
    public ew2<Handler> handlerProvider;
    public ew2 inputBoxAttachmentClickListenerProvider;
    public ew2<InputBoxConsumer> inputBoxConsumerProvider;
    public final DaggerMessagingActivityComponent messagingActivityComponent;
    public ew2<MessagingCellFactory> messagingCellFactoryProvider;
    public ew2<MessagingCellPropsFactory> messagingCellPropsFactoryProvider;
    public final MessagingComponent messagingComponent;
    public ew2<MessagingComponent> messagingComponentProvider;
    public ew2<MessagingComposer> messagingComposerProvider;
    public ew2<MessagingDialog> messagingDialogProvider;
    public ew2<MessagingViewModel> messagingViewModelProvider;
    public ew2<Boolean> multilineResponseOptionsEnabledProvider;
    public ew2<Picasso> picassoProvider;
    public ew2<Resources> resourcesProvider;
    public ew2<TypingEventDispatcher> typingEventDispatcherProvider;

    /* loaded from: classes3.dex */
    public static final class Builder implements MessagingActivityComponent.Builder {
        public AppCompatActivity activity;
        public MessagingComponent messagingComponent;

        public Builder() {
        }

        @Override // zendesk.messaging.MessagingActivityComponent.Builder
        public MessagingActivityComponent build() {
            cu2.a(this.activity, AppCompatActivity.class);
            cu2.a(this.messagingComponent, MessagingComponent.class);
            return new DaggerMessagingActivityComponent(this.messagingComponent, this.activity);
        }

        @Override // zendesk.messaging.MessagingActivityComponent.Builder
        public Builder activity(AppCompatActivity appCompatActivity) {
            this.activity = (AppCompatActivity) cu2.b(appCompatActivity);
            return this;
        }

        @Override // zendesk.messaging.MessagingActivityComponent.Builder
        public Builder messagingComponent(MessagingComponent messagingComponent) {
            this.messagingComponent = (MessagingComponent) cu2.b(messagingComponent);
            return this;
        }
    }

    /* loaded from: classes3.dex */
    public static final class zendesk_messaging_MessagingComponent_belvedere implements ew2<a> {
        public final MessagingComponent messagingComponent;

        public zendesk_messaging_MessagingComponent_belvedere(MessagingComponent messagingComponent) {
            this.messagingComponent = messagingComponent;
        }

        @Override // defpackage.ew2
        public a get() {
            return (a) cu2.e(this.messagingComponent.belvedere());
        }
    }

    /* loaded from: classes3.dex */
    public static final class zendesk_messaging_MessagingComponent_belvedereMediaHolder implements ew2<BelvedereMediaHolder> {
        public final MessagingComponent messagingComponent;

        public zendesk_messaging_MessagingComponent_belvedereMediaHolder(MessagingComponent messagingComponent) {
            this.messagingComponent = messagingComponent;
        }

        @Override // defpackage.ew2
        public BelvedereMediaHolder get() {
            return (BelvedereMediaHolder) cu2.e(this.messagingComponent.belvedereMediaHolder());
        }
    }

    /* loaded from: classes3.dex */
    public static final class zendesk_messaging_MessagingComponent_messagingViewModel implements ew2<MessagingViewModel> {
        public final MessagingComponent messagingComponent;

        public zendesk_messaging_MessagingComponent_messagingViewModel(MessagingComponent messagingComponent) {
            this.messagingComponent = messagingComponent;
        }

        @Override // defpackage.ew2
        public MessagingViewModel get() {
            return (MessagingViewModel) cu2.e(this.messagingComponent.messagingViewModel());
        }
    }

    /* loaded from: classes3.dex */
    public static final class zendesk_messaging_MessagingComponent_picasso implements ew2<Picasso> {
        public final MessagingComponent messagingComponent;

        public zendesk_messaging_MessagingComponent_picasso(MessagingComponent messagingComponent) {
            this.messagingComponent = messagingComponent;
        }

        /* JADX WARN: Can't rename method to resolve collision */
        @Override // defpackage.ew2
        public Picasso get() {
            return (Picasso) cu2.e(this.messagingComponent.picasso());
        }
    }

    /* loaded from: classes3.dex */
    public static final class zendesk_messaging_MessagingComponent_resources implements ew2<Resources> {
        public final MessagingComponent messagingComponent;

        public zendesk_messaging_MessagingComponent_resources(MessagingComponent messagingComponent) {
            this.messagingComponent = messagingComponent;
        }

        /* JADX WARN: Can't rename method to resolve collision */
        @Override // defpackage.ew2
        public Resources get() {
            return (Resources) cu2.e(this.messagingComponent.resources());
        }
    }

    public static MessagingActivityComponent.Builder builder() {
        return new Builder();
    }

    public final void initialize(MessagingComponent messagingComponent, AppCompatActivity appCompatActivity) {
        zendesk_messaging_MessagingComponent_resources zendesk_messaging_messagingcomponent_resources = new zendesk_messaging_MessagingComponent_resources(messagingComponent);
        this.resourcesProvider = zendesk_messaging_messagingcomponent_resources;
        this.messagingCellPropsFactoryProvider = fq0.a(MessagingCellPropsFactory_Factory.create(zendesk_messaging_messagingcomponent_resources));
        this.dateProvider = fq0.a(MessagingActivityModule_DateProviderFactory.create());
        this.messagingViewModelProvider = new zendesk_messaging_MessagingComponent_messagingViewModel(messagingComponent);
        this.eventFactoryProvider = fq0.a(EventFactory_Factory.create(this.dateProvider));
        zendesk_messaging_MessagingComponent_picasso zendesk_messaging_messagingcomponent_picasso = new zendesk_messaging_MessagingComponent_picasso(messagingComponent);
        this.picassoProvider = zendesk_messaging_messagingcomponent_picasso;
        this.avatarStateRendererProvider = fq0.a(AvatarStateRenderer_Factory.create(zendesk_messaging_messagingcomponent_picasso));
        y11 a = fr1.a(messagingComponent);
        this.messagingComponentProvider = a;
        this.multilineResponseOptionsEnabledProvider = fq0.a(MessagingActivityModule_MultilineResponseOptionsEnabledFactory.create(a));
        this.messagingCellFactoryProvider = fq0.a(MessagingCellFactory_Factory.create(this.messagingCellPropsFactoryProvider, this.dateProvider, this.messagingViewModelProvider, this.eventFactoryProvider, this.avatarStateRendererProvider, AvatarStateFactory_Factory.create(), this.multilineResponseOptionsEnabledProvider));
        y11 a2 = fr1.a(appCompatActivity);
        this.activityProvider = a2;
        this.belvedereUiProvider = fq0.a(MessagingActivityModule_BelvedereUiFactory.create(a2));
        this.belvedereMediaHolderProvider = new zendesk_messaging_MessagingComponent_belvedereMediaHolder(messagingComponent);
        this.belvedereProvider = new zendesk_messaging_MessagingComponent_belvedere(messagingComponent);
        ew2<BelvedereMediaResolverCallback> a3 = fq0.a(BelvedereMediaResolverCallback_Factory.create(this.messagingViewModelProvider, this.eventFactoryProvider));
        this.belvedereMediaResolverCallbackProvider = a3;
        this.inputBoxConsumerProvider = fq0.a(InputBoxConsumer_Factory.create(this.messagingViewModelProvider, this.eventFactoryProvider, this.belvedereUiProvider, this.belvedereProvider, this.belvedereMediaHolderProvider, a3));
        this.inputBoxAttachmentClickListenerProvider = InputBoxAttachmentClickListener_Factory.create(this.activityProvider, this.belvedereUiProvider, this.belvedereMediaHolderProvider);
        ew2<Handler> a4 = fq0.a(MessagingActivityModule_HandlerFactory.create());
        this.handlerProvider = a4;
        ew2<TypingEventDispatcher> a5 = fq0.a(TypingEventDispatcher_Factory.create(this.messagingViewModelProvider, a4, this.eventFactoryProvider));
        this.typingEventDispatcherProvider = a5;
        this.messagingComposerProvider = fq0.a(MessagingComposer_Factory.create(this.activityProvider, this.messagingViewModelProvider, this.belvedereUiProvider, this.belvedereMediaHolderProvider, this.inputBoxConsumerProvider, this.inputBoxAttachmentClickListenerProvider, a5));
        this.messagingDialogProvider = fq0.a(MessagingDialog_Factory.create(this.activityProvider, this.messagingViewModelProvider, this.dateProvider));
    }

    @Override // zendesk.messaging.MessagingActivityComponent
    public void inject(MessagingActivity messagingActivity) {
        injectMessagingActivity(messagingActivity);
    }

    public final MessagingActivity injectMessagingActivity(MessagingActivity messagingActivity) {
        MessagingActivity_MembersInjector.injectViewModel(messagingActivity, (MessagingViewModel) cu2.e(this.messagingComponent.messagingViewModel()));
        MessagingActivity_MembersInjector.injectMessagingCellFactory(messagingActivity, this.messagingCellFactoryProvider.get());
        MessagingActivity_MembersInjector.injectPicasso(messagingActivity, (Picasso) cu2.e(this.messagingComponent.picasso()));
        MessagingActivity_MembersInjector.injectEventFactory(messagingActivity, this.eventFactoryProvider.get());
        MessagingActivity_MembersInjector.injectMessagingComposer(messagingActivity, this.messagingComposerProvider.get());
        MessagingActivity_MembersInjector.injectMessagingDialog(messagingActivity, this.messagingDialogProvider.get());
        return messagingActivity;
    }

    public DaggerMessagingActivityComponent(MessagingComponent messagingComponent, AppCompatActivity appCompatActivity) {
        this.messagingActivityComponent = this;
        this.messagingComponent = messagingComponent;
        initialize(messagingComponent, appCompatActivity);
    }
}
