package zendesk.messaging;

/* loaded from: classes3.dex */
public final class MessagingActivityModule_MultilineResponseOptionsEnabledFactory implements y11<Boolean> {
    public final ew2<MessagingComponent> messagingComponentProvider;

    public MessagingActivityModule_MultilineResponseOptionsEnabledFactory(ew2<MessagingComponent> ew2Var) {
        this.messagingComponentProvider = ew2Var;
    }

    public static MessagingActivityModule_MultilineResponseOptionsEnabledFactory create(ew2<MessagingComponent> ew2Var) {
        return new MessagingActivityModule_MultilineResponseOptionsEnabledFactory(ew2Var);
    }

    public static boolean multilineResponseOptionsEnabled(Object obj) {
        return MessagingActivityModule.multilineResponseOptionsEnabled((MessagingComponent) obj);
    }

    @Override // defpackage.ew2
    public Boolean get() {
        return Boolean.valueOf(multilineResponseOptionsEnabled(this.messagingComponentProvider.get()));
    }
}
