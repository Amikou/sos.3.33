package zendesk.core;

/* loaded from: classes3.dex */
public interface ProviderStore {
    PushRegistrationProvider pushRegistrationProvider();

    UserProvider userProvider();
}
