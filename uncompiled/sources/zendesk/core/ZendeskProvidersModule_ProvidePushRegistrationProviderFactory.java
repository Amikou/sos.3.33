package zendesk.core;

import android.content.Context;

/* loaded from: classes3.dex */
public final class ZendeskProvidersModule_ProvidePushRegistrationProviderFactory implements y11<PushRegistrationProvider> {
    private final ew2<BlipsCoreProvider> blipsProvider;
    private final ew2<Context> contextProvider;
    private final ew2<IdentityManager> identityManagerProvider;
    private final ew2<PushDeviceIdStorage> pushDeviceIdStorageProvider;
    private final ew2<PushRegistrationService> pushRegistrationServiceProvider;
    private final ew2<SettingsProvider> settingsProvider;

    public ZendeskProvidersModule_ProvidePushRegistrationProviderFactory(ew2<PushRegistrationService> ew2Var, ew2<IdentityManager> ew2Var2, ew2<SettingsProvider> ew2Var3, ew2<BlipsCoreProvider> ew2Var4, ew2<PushDeviceIdStorage> ew2Var5, ew2<Context> ew2Var6) {
        this.pushRegistrationServiceProvider = ew2Var;
        this.identityManagerProvider = ew2Var2;
        this.settingsProvider = ew2Var3;
        this.blipsProvider = ew2Var4;
        this.pushDeviceIdStorageProvider = ew2Var5;
        this.contextProvider = ew2Var6;
    }

    public static ZendeskProvidersModule_ProvidePushRegistrationProviderFactory create(ew2<PushRegistrationService> ew2Var, ew2<IdentityManager> ew2Var2, ew2<SettingsProvider> ew2Var3, ew2<BlipsCoreProvider> ew2Var4, ew2<PushDeviceIdStorage> ew2Var5, ew2<Context> ew2Var6) {
        return new ZendeskProvidersModule_ProvidePushRegistrationProviderFactory(ew2Var, ew2Var2, ew2Var3, ew2Var4, ew2Var5, ew2Var6);
    }

    public static PushRegistrationProvider providePushRegistrationProvider(Object obj, Object obj2, SettingsProvider settingsProvider, Object obj3, Object obj4, Context context) {
        return (PushRegistrationProvider) cu2.f(ZendeskProvidersModule.providePushRegistrationProvider((PushRegistrationService) obj, (IdentityManager) obj2, settingsProvider, (BlipsCoreProvider) obj3, (PushDeviceIdStorage) obj4, context));
    }

    @Override // defpackage.ew2
    public PushRegistrationProvider get() {
        return providePushRegistrationProvider(this.pushRegistrationServiceProvider.get(), this.identityManagerProvider.get(), this.settingsProvider.get(), this.blipsProvider.get(), this.pushDeviceIdStorageProvider.get(), this.contextProvider.get());
    }
}
