package zendesk.core;

import android.content.Context;

/* loaded from: classes3.dex */
public final class ZendeskStorageModule_ProvideAdditionalSdkBaseStorageFactory implements y11<BaseStorage> {
    private final ew2<Context> contextProvider;
    private final ew2<Serializer> serializerProvider;

    public ZendeskStorageModule_ProvideAdditionalSdkBaseStorageFactory(ew2<Context> ew2Var, ew2<Serializer> ew2Var2) {
        this.contextProvider = ew2Var;
        this.serializerProvider = ew2Var2;
    }

    public static ZendeskStorageModule_ProvideAdditionalSdkBaseStorageFactory create(ew2<Context> ew2Var, ew2<Serializer> ew2Var2) {
        return new ZendeskStorageModule_ProvideAdditionalSdkBaseStorageFactory(ew2Var, ew2Var2);
    }

    public static BaseStorage provideAdditionalSdkBaseStorage(Context context, Object obj) {
        return (BaseStorage) cu2.f(ZendeskStorageModule.provideAdditionalSdkBaseStorage(context, (Serializer) obj));
    }

    @Override // defpackage.ew2
    public BaseStorage get() {
        return provideAdditionalSdkBaseStorage(this.contextProvider.get(), this.serializerProvider.get());
    }
}
