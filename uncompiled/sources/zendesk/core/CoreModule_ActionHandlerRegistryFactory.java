package zendesk.core;

/* loaded from: classes3.dex */
public final class CoreModule_ActionHandlerRegistryFactory implements y11<ActionHandlerRegistry> {
    private final CoreModule module;

    public CoreModule_ActionHandlerRegistryFactory(CoreModule coreModule) {
        this.module = coreModule;
    }

    public static ActionHandlerRegistry actionHandlerRegistry(CoreModule coreModule) {
        return (ActionHandlerRegistry) cu2.f(coreModule.actionHandlerRegistry());
    }

    public static CoreModule_ActionHandlerRegistryFactory create(CoreModule coreModule) {
        return new CoreModule_ActionHandlerRegistryFactory(coreModule);
    }

    @Override // defpackage.ew2
    public ActionHandlerRegistry get() {
        return actionHandlerRegistry(this.module);
    }
}
