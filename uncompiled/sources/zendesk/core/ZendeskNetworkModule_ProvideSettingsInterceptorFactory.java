package zendesk.core;

/* loaded from: classes3.dex */
public final class ZendeskNetworkModule_ProvideSettingsInterceptorFactory implements y11<ZendeskSettingsInterceptor> {
    private final ew2<SdkSettingsProviderInternal> sdkSettingsProvider;
    private final ew2<SettingsStorage> settingsStorageProvider;

    public ZendeskNetworkModule_ProvideSettingsInterceptorFactory(ew2<SdkSettingsProviderInternal> ew2Var, ew2<SettingsStorage> ew2Var2) {
        this.sdkSettingsProvider = ew2Var;
        this.settingsStorageProvider = ew2Var2;
    }

    public static ZendeskNetworkModule_ProvideSettingsInterceptorFactory create(ew2<SdkSettingsProviderInternal> ew2Var, ew2<SettingsStorage> ew2Var2) {
        return new ZendeskNetworkModule_ProvideSettingsInterceptorFactory(ew2Var, ew2Var2);
    }

    public static ZendeskSettingsInterceptor provideSettingsInterceptor(Object obj, Object obj2) {
        return (ZendeskSettingsInterceptor) cu2.f(ZendeskNetworkModule.provideSettingsInterceptor((SdkSettingsProviderInternal) obj, (SettingsStorage) obj2));
    }

    @Override // defpackage.ew2
    public ZendeskSettingsInterceptor get() {
        return provideSettingsInterceptor(this.sdkSettingsProvider.get(), this.settingsStorageProvider.get());
    }
}
