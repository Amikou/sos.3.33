package zendesk.core;

import retrofit2.o;

/* loaded from: classes3.dex */
public final class ZendeskProvidersModule_ProvideSdkSettingsServiceFactory implements y11<SdkSettingsService> {
    private final ew2<o> retrofitProvider;

    public ZendeskProvidersModule_ProvideSdkSettingsServiceFactory(ew2<o> ew2Var) {
        this.retrofitProvider = ew2Var;
    }

    public static ZendeskProvidersModule_ProvideSdkSettingsServiceFactory create(ew2<o> ew2Var) {
        return new ZendeskProvidersModule_ProvideSdkSettingsServiceFactory(ew2Var);
    }

    public static SdkSettingsService provideSdkSettingsService(o oVar) {
        return (SdkSettingsService) cu2.f(ZendeskProvidersModule.provideSdkSettingsService(oVar));
    }

    @Override // defpackage.ew2
    public SdkSettingsService get() {
        return provideSdkSettingsService(this.retrofitProvider.get());
    }
}
