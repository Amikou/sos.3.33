package zendesk.core;

import com.google.gson.JsonElement;
import java.util.Map;
import retrofit2.b;

/* loaded from: classes3.dex */
interface SdkSettingsService {
    @ge1("/api/private/mobile_sdk/settings/{applicationId}.json")
    b<Map<String, JsonElement>> getSettings(@ek1("Accept-Language") String str, @vp2("applicationId") String str2);
}
