package zendesk.core;

/* loaded from: classes3.dex */
class PushRegistrationResponse {
    private String identifier;

    public String getIdentifier() {
        return this.identifier;
    }
}
