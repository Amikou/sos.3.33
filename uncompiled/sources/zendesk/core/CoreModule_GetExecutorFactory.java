package zendesk.core;

import java.util.concurrent.Executor;

/* loaded from: classes3.dex */
public final class CoreModule_GetExecutorFactory implements y11<Executor> {
    private final CoreModule module;

    public CoreModule_GetExecutorFactory(CoreModule coreModule) {
        this.module = coreModule;
    }

    public static CoreModule_GetExecutorFactory create(CoreModule coreModule) {
        return new CoreModule_GetExecutorFactory(coreModule);
    }

    public static Executor getExecutor(CoreModule coreModule) {
        return (Executor) cu2.f(coreModule.getExecutor());
    }

    @Override // defpackage.ew2
    public Executor get() {
        return getExecutor(this.module);
    }
}
