package zendesk.core;

/* loaded from: classes3.dex */
public final class ZendeskProvidersModule_ProvideUserProviderFactory implements y11<UserProvider> {
    private final ew2<UserService> userServiceProvider;

    public ZendeskProvidersModule_ProvideUserProviderFactory(ew2<UserService> ew2Var) {
        this.userServiceProvider = ew2Var;
    }

    public static ZendeskProvidersModule_ProvideUserProviderFactory create(ew2<UserService> ew2Var) {
        return new ZendeskProvidersModule_ProvideUserProviderFactory(ew2Var);
    }

    public static UserProvider provideUserProvider(Object obj) {
        return (UserProvider) cu2.f(ZendeskProvidersModule.provideUserProvider((UserService) obj));
    }

    @Override // defpackage.ew2
    public UserProvider get() {
        return provideUserProvider(this.userServiceProvider.get());
    }
}
