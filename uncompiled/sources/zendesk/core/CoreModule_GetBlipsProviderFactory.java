package zendesk.core;

/* loaded from: classes3.dex */
public final class CoreModule_GetBlipsProviderFactory implements y11<BlipsProvider> {
    private final CoreModule module;

    public CoreModule_GetBlipsProviderFactory(CoreModule coreModule) {
        this.module = coreModule;
    }

    public static CoreModule_GetBlipsProviderFactory create(CoreModule coreModule) {
        return new CoreModule_GetBlipsProviderFactory(coreModule);
    }

    public static BlipsProvider getBlipsProvider(CoreModule coreModule) {
        return (BlipsProvider) cu2.f(coreModule.getBlipsProvider());
    }

    @Override // defpackage.ew2
    public BlipsProvider get() {
        return getBlipsProvider(this.module);
    }
}
