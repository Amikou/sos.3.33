package zendesk.core;

import android.content.Context;

/* loaded from: classes3.dex */
public final class ZendeskProvidersModule_ProvideZendeskSdkSettingsProviderFactory implements y11<ZendeskSettingsProvider> {
    private final ew2<ActionHandlerRegistry> actionHandlerRegistryProvider;
    private final ew2<ApplicationConfiguration> configurationProvider;
    private final ew2<Context> contextProvider;
    private final ew2<CoreSettingsStorage> coreSettingsStorageProvider;
    private final ew2<SdkSettingsService> sdkSettingsServiceProvider;
    private final ew2<Serializer> serializerProvider;
    private final ew2<SettingsStorage> settingsStorageProvider;
    private final ew2<ZendeskLocaleConverter> zendeskLocaleConverterProvider;

    public ZendeskProvidersModule_ProvideZendeskSdkSettingsProviderFactory(ew2<SdkSettingsService> ew2Var, ew2<SettingsStorage> ew2Var2, ew2<CoreSettingsStorage> ew2Var3, ew2<ActionHandlerRegistry> ew2Var4, ew2<Serializer> ew2Var5, ew2<ZendeskLocaleConverter> ew2Var6, ew2<ApplicationConfiguration> ew2Var7, ew2<Context> ew2Var8) {
        this.sdkSettingsServiceProvider = ew2Var;
        this.settingsStorageProvider = ew2Var2;
        this.coreSettingsStorageProvider = ew2Var3;
        this.actionHandlerRegistryProvider = ew2Var4;
        this.serializerProvider = ew2Var5;
        this.zendeskLocaleConverterProvider = ew2Var6;
        this.configurationProvider = ew2Var7;
        this.contextProvider = ew2Var8;
    }

    public static ZendeskProvidersModule_ProvideZendeskSdkSettingsProviderFactory create(ew2<SdkSettingsService> ew2Var, ew2<SettingsStorage> ew2Var2, ew2<CoreSettingsStorage> ew2Var3, ew2<ActionHandlerRegistry> ew2Var4, ew2<Serializer> ew2Var5, ew2<ZendeskLocaleConverter> ew2Var6, ew2<ApplicationConfiguration> ew2Var7, ew2<Context> ew2Var8) {
        return new ZendeskProvidersModule_ProvideZendeskSdkSettingsProviderFactory(ew2Var, ew2Var2, ew2Var3, ew2Var4, ew2Var5, ew2Var6, ew2Var7, ew2Var8);
    }

    public static ZendeskSettingsProvider provideZendeskSdkSettingsProvider(Object obj, Object obj2, Object obj3, ActionHandlerRegistry actionHandlerRegistry, Object obj4, ZendeskLocaleConverter zendeskLocaleConverter, ApplicationConfiguration applicationConfiguration, Context context) {
        return (ZendeskSettingsProvider) cu2.f(ZendeskProvidersModule.provideZendeskSdkSettingsProvider((SdkSettingsService) obj, (SettingsStorage) obj2, (CoreSettingsStorage) obj3, actionHandlerRegistry, (Serializer) obj4, zendeskLocaleConverter, applicationConfiguration, context));
    }

    @Override // defpackage.ew2
    public ZendeskSettingsProvider get() {
        return provideZendeskSdkSettingsProvider(this.sdkSettingsServiceProvider.get(), this.settingsStorageProvider.get(), this.coreSettingsStorageProvider.get(), this.actionHandlerRegistryProvider.get(), this.serializerProvider.get(), this.zendeskLocaleConverterProvider.get(), this.configurationProvider.get(), this.contextProvider.get());
    }
}
