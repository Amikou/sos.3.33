package zendesk.core;

import java.util.concurrent.ExecutorService;
import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;

/* loaded from: classes3.dex */
public final class ZendeskNetworkModule_ProvideBaseOkHttpClientFactory implements y11<OkHttpClient> {
    private final ew2<ExecutorService> executorServiceProvider;
    private final ew2<HttpLoggingInterceptor> loggingInterceptorProvider;
    private final ZendeskNetworkModule module;
    private final ew2<ZendeskOauthIdHeaderInterceptor> oauthIdHeaderInterceptorProvider;
    private final ew2<UserAgentAndClientHeadersInterceptor> userAgentAndClientHeadersInterceptorProvider;

    public ZendeskNetworkModule_ProvideBaseOkHttpClientFactory(ZendeskNetworkModule zendeskNetworkModule, ew2<HttpLoggingInterceptor> ew2Var, ew2<ZendeskOauthIdHeaderInterceptor> ew2Var2, ew2<UserAgentAndClientHeadersInterceptor> ew2Var3, ew2<ExecutorService> ew2Var4) {
        this.module = zendeskNetworkModule;
        this.loggingInterceptorProvider = ew2Var;
        this.oauthIdHeaderInterceptorProvider = ew2Var2;
        this.userAgentAndClientHeadersInterceptorProvider = ew2Var3;
        this.executorServiceProvider = ew2Var4;
    }

    public static ZendeskNetworkModule_ProvideBaseOkHttpClientFactory create(ZendeskNetworkModule zendeskNetworkModule, ew2<HttpLoggingInterceptor> ew2Var, ew2<ZendeskOauthIdHeaderInterceptor> ew2Var2, ew2<UserAgentAndClientHeadersInterceptor> ew2Var3, ew2<ExecutorService> ew2Var4) {
        return new ZendeskNetworkModule_ProvideBaseOkHttpClientFactory(zendeskNetworkModule, ew2Var, ew2Var2, ew2Var3, ew2Var4);
    }

    public static OkHttpClient provideBaseOkHttpClient(ZendeskNetworkModule zendeskNetworkModule, HttpLoggingInterceptor httpLoggingInterceptor, Object obj, Object obj2, ExecutorService executorService) {
        return (OkHttpClient) cu2.f(zendeskNetworkModule.provideBaseOkHttpClient(httpLoggingInterceptor, (ZendeskOauthIdHeaderInterceptor) obj, (UserAgentAndClientHeadersInterceptor) obj2, executorService));
    }

    @Override // defpackage.ew2
    public OkHttpClient get() {
        return provideBaseOkHttpClient(this.module, this.loggingInterceptorProvider.get(), this.oauthIdHeaderInterceptorProvider.get(), this.userAgentAndClientHeadersInterceptorProvider.get(), this.executorServiceProvider.get());
    }
}
