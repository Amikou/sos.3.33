package zendesk.core;

/* loaded from: classes3.dex */
public final class ZendeskNetworkModule_ProvidesAcceptHeaderInterceptorFactory implements y11<AcceptHeaderInterceptor> {

    /* loaded from: classes3.dex */
    public static final class InstanceHolder {
        private static final ZendeskNetworkModule_ProvidesAcceptHeaderInterceptorFactory INSTANCE = new ZendeskNetworkModule_ProvidesAcceptHeaderInterceptorFactory();

        private InstanceHolder() {
        }
    }

    public static ZendeskNetworkModule_ProvidesAcceptHeaderInterceptorFactory create() {
        return InstanceHolder.INSTANCE;
    }

    public static AcceptHeaderInterceptor providesAcceptHeaderInterceptor() {
        return (AcceptHeaderInterceptor) cu2.f(ZendeskNetworkModule.providesAcceptHeaderInterceptor());
    }

    @Override // defpackage.ew2
    public AcceptHeaderInterceptor get() {
        return providesAcceptHeaderInterceptor();
    }
}
