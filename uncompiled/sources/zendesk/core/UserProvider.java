package zendesk.core;

import java.util.List;
import java.util.Map;

/* loaded from: classes3.dex */
public interface UserProvider {
    void addTags(List<String> list, rs4<List<String>> rs4Var);

    void deleteTags(List<String> list, rs4<List<String>> rs4Var);

    void getUser(rs4<User> rs4Var);

    void getUserFields(rs4<List<UserField>> rs4Var);

    void setUserFields(Map<String, String> map, rs4<Map<String, String>> rs4Var);
}
