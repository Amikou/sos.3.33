package zendesk.core;

/* loaded from: classes3.dex */
public final class ZendeskApplicationModule_ProvideZendeskFactory implements y11<ZendeskShadow> {
    private final ew2<BlipsCoreProvider> blipsCoreProvider;
    private final ew2<CoreModule> coreModuleProvider;
    private final ew2<IdentityManager> identityManagerProvider;
    private final ew2<LegacyIdentityMigrator> legacyIdentityMigratorProvider;
    private final ew2<ProviderStore> providerStoreProvider;
    private final ew2<PushRegistrationProvider> pushRegistrationProvider;
    private final ew2<Storage> storageProvider;

    public ZendeskApplicationModule_ProvideZendeskFactory(ew2<Storage> ew2Var, ew2<LegacyIdentityMigrator> ew2Var2, ew2<IdentityManager> ew2Var3, ew2<BlipsCoreProvider> ew2Var4, ew2<PushRegistrationProvider> ew2Var5, ew2<CoreModule> ew2Var6, ew2<ProviderStore> ew2Var7) {
        this.storageProvider = ew2Var;
        this.legacyIdentityMigratorProvider = ew2Var2;
        this.identityManagerProvider = ew2Var3;
        this.blipsCoreProvider = ew2Var4;
        this.pushRegistrationProvider = ew2Var5;
        this.coreModuleProvider = ew2Var6;
        this.providerStoreProvider = ew2Var7;
    }

    public static ZendeskApplicationModule_ProvideZendeskFactory create(ew2<Storage> ew2Var, ew2<LegacyIdentityMigrator> ew2Var2, ew2<IdentityManager> ew2Var3, ew2<BlipsCoreProvider> ew2Var4, ew2<PushRegistrationProvider> ew2Var5, ew2<CoreModule> ew2Var6, ew2<ProviderStore> ew2Var7) {
        return new ZendeskApplicationModule_ProvideZendeskFactory(ew2Var, ew2Var2, ew2Var3, ew2Var4, ew2Var5, ew2Var6, ew2Var7);
    }

    public static ZendeskShadow provideZendesk(Object obj, Object obj2, Object obj3, Object obj4, PushRegistrationProvider pushRegistrationProvider, CoreModule coreModule, ProviderStore providerStore) {
        return (ZendeskShadow) cu2.f(ZendeskApplicationModule.provideZendesk((Storage) obj, (LegacyIdentityMigrator) obj2, (IdentityManager) obj3, (BlipsCoreProvider) obj4, pushRegistrationProvider, coreModule, providerStore));
    }

    @Override // defpackage.ew2
    public ZendeskShadow get() {
        return provideZendesk(this.storageProvider.get(), this.legacyIdentityMigratorProvider.get(), this.identityManagerProvider.get(), this.blipsCoreProvider.get(), this.pushRegistrationProvider.get(), this.coreModuleProvider.get(), this.providerStoreProvider.get());
    }
}
