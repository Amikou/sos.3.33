package zendesk.core;

/* loaded from: classes3.dex */
public final class ZendeskApplicationModule_ProvideApplicationConfigurationFactory implements y11<ApplicationConfiguration> {
    private final ZendeskApplicationModule module;

    public ZendeskApplicationModule_ProvideApplicationConfigurationFactory(ZendeskApplicationModule zendeskApplicationModule) {
        this.module = zendeskApplicationModule;
    }

    public static ZendeskApplicationModule_ProvideApplicationConfigurationFactory create(ZendeskApplicationModule zendeskApplicationModule) {
        return new ZendeskApplicationModule_ProvideApplicationConfigurationFactory(zendeskApplicationModule);
    }

    public static ApplicationConfiguration provideApplicationConfiguration(ZendeskApplicationModule zendeskApplicationModule) {
        return (ApplicationConfiguration) cu2.f(zendeskApplicationModule.provideApplicationConfiguration());
    }

    @Override // defpackage.ew2
    public ApplicationConfiguration get() {
        return provideApplicationConfiguration(this.module);
    }
}
