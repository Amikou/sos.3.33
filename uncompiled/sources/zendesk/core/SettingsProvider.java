package zendesk.core;

/* loaded from: classes3.dex */
public interface SettingsProvider {
    void getCoreSettings(rs4<CoreSettings> rs4Var);

    <E extends Settings> void getSettingsForSdk(String str, Class<E> cls, rs4<SettingsPack<E>> rs4Var);
}
