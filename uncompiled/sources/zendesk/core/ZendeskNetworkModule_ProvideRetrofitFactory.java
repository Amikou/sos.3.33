package zendesk.core;

import com.google.gson.Gson;
import okhttp3.OkHttpClient;
import retrofit2.o;

/* loaded from: classes3.dex */
public final class ZendeskNetworkModule_ProvideRetrofitFactory implements y11<o> {
    private final ew2<ApplicationConfiguration> configurationProvider;
    private final ew2<Gson> gsonProvider;
    private final ew2<OkHttpClient> okHttpClientProvider;

    public ZendeskNetworkModule_ProvideRetrofitFactory(ew2<ApplicationConfiguration> ew2Var, ew2<Gson> ew2Var2, ew2<OkHttpClient> ew2Var3) {
        this.configurationProvider = ew2Var;
        this.gsonProvider = ew2Var2;
        this.okHttpClientProvider = ew2Var3;
    }

    public static ZendeskNetworkModule_ProvideRetrofitFactory create(ew2<ApplicationConfiguration> ew2Var, ew2<Gson> ew2Var2, ew2<OkHttpClient> ew2Var3) {
        return new ZendeskNetworkModule_ProvideRetrofitFactory(ew2Var, ew2Var2, ew2Var3);
    }

    public static o provideRetrofit(ApplicationConfiguration applicationConfiguration, Gson gson, OkHttpClient okHttpClient) {
        return (o) cu2.f(ZendeskNetworkModule.provideRetrofit(applicationConfiguration, gson, okHttpClient));
    }

    @Override // defpackage.ew2
    public o get() {
        return provideRetrofit(this.configurationProvider.get(), this.gsonProvider.get(), this.okHttpClientProvider.get());
    }
}
