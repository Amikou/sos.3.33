package zendesk.core;

import okhttp3.OkHttpClient;

/* loaded from: classes3.dex */
public final class ZendeskNetworkModule_ProvideCoreOkHttpClientFactory implements y11<OkHttpClient> {
    private final ew2<AcceptHeaderInterceptor> acceptHeaderInterceptorProvider;
    private final ew2<AcceptLanguageHeaderInterceptor> acceptLanguageHeaderInterceptorProvider;
    private final ZendeskNetworkModule module;
    private final ew2<OkHttpClient> okHttpClientProvider;

    public ZendeskNetworkModule_ProvideCoreOkHttpClientFactory(ZendeskNetworkModule zendeskNetworkModule, ew2<OkHttpClient> ew2Var, ew2<AcceptLanguageHeaderInterceptor> ew2Var2, ew2<AcceptHeaderInterceptor> ew2Var3) {
        this.module = zendeskNetworkModule;
        this.okHttpClientProvider = ew2Var;
        this.acceptLanguageHeaderInterceptorProvider = ew2Var2;
        this.acceptHeaderInterceptorProvider = ew2Var3;
    }

    public static ZendeskNetworkModule_ProvideCoreOkHttpClientFactory create(ZendeskNetworkModule zendeskNetworkModule, ew2<OkHttpClient> ew2Var, ew2<AcceptLanguageHeaderInterceptor> ew2Var2, ew2<AcceptHeaderInterceptor> ew2Var3) {
        return new ZendeskNetworkModule_ProvideCoreOkHttpClientFactory(zendeskNetworkModule, ew2Var, ew2Var2, ew2Var3);
    }

    public static OkHttpClient provideCoreOkHttpClient(ZendeskNetworkModule zendeskNetworkModule, OkHttpClient okHttpClient, Object obj, Object obj2) {
        return (OkHttpClient) cu2.f(zendeskNetworkModule.provideCoreOkHttpClient(okHttpClient, (AcceptLanguageHeaderInterceptor) obj, (AcceptHeaderInterceptor) obj2));
    }

    @Override // defpackage.ew2
    public OkHttpClient get() {
        return provideCoreOkHttpClient(this.module, this.okHttpClientProvider.get(), this.acceptLanguageHeaderInterceptorProvider.get(), this.acceptHeaderInterceptorProvider.get());
    }
}
