package zendesk.core;

import android.content.Context;
import java.util.concurrent.ScheduledExecutorService;

/* loaded from: classes3.dex */
public final class ZendeskProvidersModule_ProvideCoreSdkModuleFactory implements y11<CoreModule> {
    private final ew2<ActionHandlerRegistry> actionHandlerRegistryProvider;
    private final ew2<AuthenticationProvider> authenticationProvider;
    private final ew2<BlipsProvider> blipsProvider;
    private final ew2<Context> contextProvider;
    private final ew2<ScheduledExecutorService> executorProvider;
    private final ew2<MachineIdStorage> machineIdStorageProvider;
    private final ew2<MemoryCache> memoryCacheProvider;
    private final ew2<NetworkInfoProvider> networkInfoProvider;
    private final ew2<PushRegistrationProvider> pushRegistrationProvider;
    private final ew2<RestServiceProvider> restServiceProvider;
    private final ew2<SessionStorage> sessionStorageProvider;
    private final ew2<SettingsProvider> settingsProvider;
    private final ew2<ApplicationConfiguration> zendeskConfigurationProvider;

    public ZendeskProvidersModule_ProvideCoreSdkModuleFactory(ew2<SettingsProvider> ew2Var, ew2<RestServiceProvider> ew2Var2, ew2<BlipsProvider> ew2Var3, ew2<SessionStorage> ew2Var4, ew2<NetworkInfoProvider> ew2Var5, ew2<MemoryCache> ew2Var6, ew2<ActionHandlerRegistry> ew2Var7, ew2<ScheduledExecutorService> ew2Var8, ew2<Context> ew2Var9, ew2<AuthenticationProvider> ew2Var10, ew2<ApplicationConfiguration> ew2Var11, ew2<PushRegistrationProvider> ew2Var12, ew2<MachineIdStorage> ew2Var13) {
        this.settingsProvider = ew2Var;
        this.restServiceProvider = ew2Var2;
        this.blipsProvider = ew2Var3;
        this.sessionStorageProvider = ew2Var4;
        this.networkInfoProvider = ew2Var5;
        this.memoryCacheProvider = ew2Var6;
        this.actionHandlerRegistryProvider = ew2Var7;
        this.executorProvider = ew2Var8;
        this.contextProvider = ew2Var9;
        this.authenticationProvider = ew2Var10;
        this.zendeskConfigurationProvider = ew2Var11;
        this.pushRegistrationProvider = ew2Var12;
        this.machineIdStorageProvider = ew2Var13;
    }

    public static ZendeskProvidersModule_ProvideCoreSdkModuleFactory create(ew2<SettingsProvider> ew2Var, ew2<RestServiceProvider> ew2Var2, ew2<BlipsProvider> ew2Var3, ew2<SessionStorage> ew2Var4, ew2<NetworkInfoProvider> ew2Var5, ew2<MemoryCache> ew2Var6, ew2<ActionHandlerRegistry> ew2Var7, ew2<ScheduledExecutorService> ew2Var8, ew2<Context> ew2Var9, ew2<AuthenticationProvider> ew2Var10, ew2<ApplicationConfiguration> ew2Var11, ew2<PushRegistrationProvider> ew2Var12, ew2<MachineIdStorage> ew2Var13) {
        return new ZendeskProvidersModule_ProvideCoreSdkModuleFactory(ew2Var, ew2Var2, ew2Var3, ew2Var4, ew2Var5, ew2Var6, ew2Var7, ew2Var8, ew2Var9, ew2Var10, ew2Var11, ew2Var12, ew2Var13);
    }

    public static CoreModule provideCoreSdkModule(SettingsProvider settingsProvider, RestServiceProvider restServiceProvider, BlipsProvider blipsProvider, SessionStorage sessionStorage, NetworkInfoProvider networkInfoProvider, MemoryCache memoryCache, ActionHandlerRegistry actionHandlerRegistry, ScheduledExecutorService scheduledExecutorService, Context context, AuthenticationProvider authenticationProvider, ApplicationConfiguration applicationConfiguration, PushRegistrationProvider pushRegistrationProvider, MachineIdStorage machineIdStorage) {
        return (CoreModule) cu2.f(ZendeskProvidersModule.provideCoreSdkModule(settingsProvider, restServiceProvider, blipsProvider, sessionStorage, networkInfoProvider, memoryCache, actionHandlerRegistry, scheduledExecutorService, context, authenticationProvider, applicationConfiguration, pushRegistrationProvider, machineIdStorage));
    }

    @Override // defpackage.ew2
    public CoreModule get() {
        return provideCoreSdkModule(this.settingsProvider.get(), this.restServiceProvider.get(), this.blipsProvider.get(), this.sessionStorageProvider.get(), this.networkInfoProvider.get(), this.memoryCacheProvider.get(), this.actionHandlerRegistryProvider.get(), this.executorProvider.get(), this.contextProvider.get(), this.authenticationProvider.get(), this.zendeskConfigurationProvider.get(), this.pushRegistrationProvider.get(), this.machineIdStorageProvider.get());
    }
}
