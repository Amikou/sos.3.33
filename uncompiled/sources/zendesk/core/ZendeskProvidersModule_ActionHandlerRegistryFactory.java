package zendesk.core;

/* loaded from: classes3.dex */
public final class ZendeskProvidersModule_ActionHandlerRegistryFactory implements y11<ActionHandlerRegistry> {

    /* loaded from: classes3.dex */
    public static final class InstanceHolder {
        private static final ZendeskProvidersModule_ActionHandlerRegistryFactory INSTANCE = new ZendeskProvidersModule_ActionHandlerRegistryFactory();

        private InstanceHolder() {
        }
    }

    public static ActionHandlerRegistry actionHandlerRegistry() {
        return (ActionHandlerRegistry) cu2.f(ZendeskProvidersModule.actionHandlerRegistry());
    }

    public static ZendeskProvidersModule_ActionHandlerRegistryFactory create() {
        return InstanceHolder.INSTANCE;
    }

    @Override // defpackage.ew2
    public ActionHandlerRegistry get() {
        return actionHandlerRegistry();
    }
}
