package zendesk.core;

/* loaded from: classes3.dex */
public final class CoreModule_GetMachineIdStorageFactory implements y11<MachineIdStorage> {
    private final CoreModule module;

    public CoreModule_GetMachineIdStorageFactory(CoreModule coreModule) {
        this.module = coreModule;
    }

    public static CoreModule_GetMachineIdStorageFactory create(CoreModule coreModule) {
        return new CoreModule_GetMachineIdStorageFactory(coreModule);
    }

    public static MachineIdStorage getMachineIdStorage(CoreModule coreModule) {
        return (MachineIdStorage) cu2.f(coreModule.getMachineIdStorage());
    }

    @Override // defpackage.ew2
    public MachineIdStorage get() {
        return getMachineIdStorage(this.module);
    }
}
