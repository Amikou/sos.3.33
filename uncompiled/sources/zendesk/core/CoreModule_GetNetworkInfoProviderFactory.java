package zendesk.core;

/* loaded from: classes3.dex */
public final class CoreModule_GetNetworkInfoProviderFactory implements y11<NetworkInfoProvider> {
    private final CoreModule module;

    public CoreModule_GetNetworkInfoProviderFactory(CoreModule coreModule) {
        this.module = coreModule;
    }

    public static CoreModule_GetNetworkInfoProviderFactory create(CoreModule coreModule) {
        return new CoreModule_GetNetworkInfoProviderFactory(coreModule);
    }

    public static NetworkInfoProvider getNetworkInfoProvider(CoreModule coreModule) {
        return (NetworkInfoProvider) cu2.f(coreModule.getNetworkInfoProvider());
    }

    @Override // defpackage.ew2
    public NetworkInfoProvider get() {
        return getNetworkInfoProvider(this.module);
    }
}
