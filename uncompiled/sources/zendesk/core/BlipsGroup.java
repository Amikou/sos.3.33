package zendesk.core;

/* loaded from: classes3.dex */
public enum BlipsGroup {
    REQUIRED,
    BEHAVIOURAL,
    PATHFINDER
}
