package zendesk.core;

/* loaded from: classes3.dex */
public final class ZendeskStorageModule_ProvideIdentityManagerFactory implements y11<IdentityManager> {
    private final ew2<IdentityStorage> identityStorageProvider;

    public ZendeskStorageModule_ProvideIdentityManagerFactory(ew2<IdentityStorage> ew2Var) {
        this.identityStorageProvider = ew2Var;
    }

    public static ZendeskStorageModule_ProvideIdentityManagerFactory create(ew2<IdentityStorage> ew2Var) {
        return new ZendeskStorageModule_ProvideIdentityManagerFactory(ew2Var);
    }

    public static IdentityManager provideIdentityManager(Object obj) {
        return (IdentityManager) cu2.f(ZendeskStorageModule.provideIdentityManager((IdentityStorage) obj));
    }

    @Override // defpackage.ew2
    public IdentityManager get() {
        return provideIdentityManager(this.identityStorageProvider.get());
    }
}
