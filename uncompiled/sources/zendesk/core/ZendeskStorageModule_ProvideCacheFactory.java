package zendesk.core;

import java.io.File;
import okhttp3.Cache;

/* loaded from: classes3.dex */
public final class ZendeskStorageModule_ProvideCacheFactory implements y11<Cache> {
    private final ew2<File> fileProvider;

    public ZendeskStorageModule_ProvideCacheFactory(ew2<File> ew2Var) {
        this.fileProvider = ew2Var;
    }

    public static ZendeskStorageModule_ProvideCacheFactory create(ew2<File> ew2Var) {
        return new ZendeskStorageModule_ProvideCacheFactory(ew2Var);
    }

    public static Cache provideCache(File file) {
        return (Cache) cu2.f(ZendeskStorageModule.provideCache(file));
    }

    @Override // defpackage.ew2
    public Cache get() {
        return provideCache(this.fileProvider.get());
    }
}
