package zendesk.core;

import com.zendesk.logger.Logger;
import defpackage.w83;
import java.io.IOException;
import java.util.Locale;
import retrofit2.n;

/* JADX INFO: Access modifiers changed from: package-private */
/* loaded from: classes3.dex */
public class ZendeskPushRegistrationProvider implements PushRegistrationProvider, PushRegistrationProviderInternal {
    private static final String LOG_TAG = "PushRegistrationProvider";
    private static final w83.b<PushRegistrationResponseWrapper, String> PUSH_RESPONSE_EXTRACTOR = new w83.b<PushRegistrationResponseWrapper, String>() { // from class: zendesk.core.ZendeskPushRegistrationProvider.4
        @Override // defpackage.w83.b
        public String extract(PushRegistrationResponseWrapper pushRegistrationResponseWrapper) {
            return (pushRegistrationResponseWrapper == null || pushRegistrationResponseWrapper.getRegistrationResponse() == null || !ru3.b(pushRegistrationResponseWrapper.getRegistrationResponse().getIdentifier())) ? "" : pushRegistrationResponseWrapper.getRegistrationResponse().getIdentifier();
        }
    };
    private final BlipsCoreProvider blipsProvider;
    private final IdentityManager identityManager;
    private final Locale locale;
    private final PushDeviceIdStorage pushIdStorage;
    private final PushRegistrationService pushService;
    private final SettingsProvider settingsProvider;

    /* loaded from: classes3.dex */
    public enum TokenType {
        Identifier(null),
        UrbanAirshipChannelId("urban_airship_channel_id");
        
        public final String name;

        TokenType(String str) {
            this.name = str;
        }

        public String getName() {
            return this.name;
        }
    }

    public ZendeskPushRegistrationProvider(PushRegistrationService pushRegistrationService, IdentityManager identityManager, SettingsProvider settingsProvider, BlipsCoreProvider blipsCoreProvider, PushDeviceIdStorage pushDeviceIdStorage, Locale locale) {
        this.pushService = pushRegistrationService;
        this.identityManager = identityManager;
        this.settingsProvider = settingsProvider;
        this.blipsProvider = blipsCoreProvider;
        this.pushIdStorage = pushDeviceIdStorage;
        this.locale = locale;
    }

    private boolean checkForStoredPushRegistration(String str, rs4<String> rs4Var) {
        if (!ru3.b(str)) {
            if (rs4Var != null) {
                rs4Var.onError(new dw0("Invalid identifier provided."));
            }
            Logger.k(LOG_TAG, "Invalid identifier provided.", new Object[0]);
            return true;
        } else if (this.pushIdStorage.hasRegisteredDeviceId() && str.equals(this.pushIdStorage.getRegisteredDeviceId())) {
            if (rs4Var != null) {
                rs4Var.onSuccess(str);
            }
            Logger.b(LOG_TAG, "Skipping registration because device is already registered with this ID.", new Object[0]);
            return true;
        } else {
            if (this.pushIdStorage.hasRegisteredDeviceId()) {
                this.pushIdStorage.removeRegisteredDeviceId();
                Logger.b(LOG_TAG, "Removing stored push registration response because a new one has been provided.", new Object[0]);
            }
            return false;
        }
    }

    private void getAuthTypeAndRequest(final String str, final TokenType tokenType, final rs4<String> rs4Var) {
        if (checkForStoredPushRegistration(str, rs4Var)) {
            return;
        }
        this.settingsProvider.getCoreSettings(new PassThroughErrorZendeskCallback<CoreSettings>(rs4Var) { // from class: zendesk.core.ZendeskPushRegistrationProvider.1
            @Override // zendesk.core.PassThroughErrorZendeskCallback, defpackage.rs4
            public void onSuccess(CoreSettings coreSettings) {
                AuthenticationType authentication = coreSettings.getAuthentication();
                if (authentication == null) {
                    rs4 rs4Var2 = rs4Var;
                    if (rs4Var2 != null) {
                        rs4Var2.onError(new dw0("Authentication type is null."));
                        return;
                    }
                    return;
                }
                ZendeskPushRegistrationProvider zendeskPushRegistrationProvider = ZendeskPushRegistrationProvider.this;
                PushRegistrationRequest pushRegistrationRequest = zendeskPushRegistrationProvider.getPushRegistrationRequest(str, zendeskPushRegistrationProvider.locale, authentication, tokenType);
                if (ZendeskPushRegistrationProvider.this.hasNoStoredAccessToken()) {
                    ZendeskPushRegistrationProvider.this.storePendingPushRegistrationRequest(pushRegistrationRequest, rs4Var);
                } else {
                    ZendeskPushRegistrationProvider.this.sendPushRegistrationRequest(pushRegistrationRequest, rs4Var);
                }
            }
        });
    }

    /* JADX INFO: Access modifiers changed from: private */
    public PushRegistrationRequest getPushRegistrationRequest(String str, Locale locale, AuthenticationType authenticationType, TokenType tokenType) {
        PushRegistrationRequest pushRegistrationRequest = new PushRegistrationRequest();
        pushRegistrationRequest.setIdentifier(str);
        pushRegistrationRequest.setLocale(h12.d(locale));
        if (tokenType == TokenType.UrbanAirshipChannelId) {
            pushRegistrationRequest.setTokenType(tokenType.name);
        }
        if (AuthenticationType.ANONYMOUS == authenticationType) {
            pushRegistrationRequest.setSdkGuid(this.identityManager.getSdkGuid());
        }
        return pushRegistrationRequest;
    }

    /* JADX INFO: Access modifiers changed from: private */
    public boolean hasNoStoredAccessToken() {
        return this.identityManager.getStoredAccessTokenAsBearerToken() == null;
    }

    /* JADX INFO: Access modifiers changed from: private */
    public void onSuccessfulRegistration(String str) {
        this.pushIdStorage.storeRegisteredDeviceId(str);
        this.pushIdStorage.removePushRegistrationRequest();
        this.blipsProvider.corePushEnabled();
    }

    /* JADX INFO: Access modifiers changed from: private */
    public void sendPushRegistrationRequest(PushRegistrationRequest pushRegistrationRequest, final rs4<String> rs4Var) {
        this.pushService.registerDevice(new PushRegistrationRequestWrapper(pushRegistrationRequest)).n(new w83(new PassThroughErrorZendeskCallback<String>(rs4Var) { // from class: zendesk.core.ZendeskPushRegistrationProvider.3
            @Override // zendesk.core.PassThroughErrorZendeskCallback, defpackage.rs4
            public void onSuccess(String str) {
                ZendeskPushRegistrationProvider.this.onSuccessfulRegistration(str);
                rs4 rs4Var2 = rs4Var;
                if (rs4Var2 != null) {
                    rs4Var2.onSuccess(str);
                }
            }
        }, PUSH_RESPONSE_EXTRACTOR));
    }

    /* JADX INFO: Access modifiers changed from: private */
    public void storePendingPushRegistrationRequest(PushRegistrationRequest pushRegistrationRequest, rs4<String> rs4Var) {
        this.pushIdStorage.storePushRegistrationRequest(pushRegistrationRequest);
        if (rs4Var != null) {
            rs4Var.onSuccess(pushRegistrationRequest.getIdentifier());
        }
    }

    @Override // zendesk.core.PushRegistrationProvider
    public boolean isRegisteredForPush() {
        return this.pushIdStorage.hasRegisteredDeviceId();
    }

    @Override // zendesk.core.PushRegistrationProvider
    public void registerWithDeviceIdentifier(String str, rs4<String> rs4Var) {
        getAuthTypeAndRequest(str, TokenType.Identifier, rs4Var);
    }

    @Override // zendesk.core.PushRegistrationProvider
    public void registerWithUAChannelId(String str, rs4<String> rs4Var) {
        getAuthTypeAndRequest(str, TokenType.UrbanAirshipChannelId, rs4Var);
    }

    @Override // zendesk.core.PushRegistrationProvider
    public void unregisterDevice(final rs4<Void> rs4Var) {
        String registeredDeviceId = this.pushIdStorage.getRegisteredDeviceId();
        final Long userId = this.identityManager.getUserId();
        if (registeredDeviceId != null) {
            this.pushService.unregisterDevice(registeredDeviceId).n(new w83(new PassThroughErrorZendeskCallback<Void>(rs4Var) { // from class: zendesk.core.ZendeskPushRegistrationProvider.2
                @Override // zendesk.core.PassThroughErrorZendeskCallback, defpackage.rs4
                public void onSuccess(Void r3) {
                    ZendeskPushRegistrationProvider.this.pushIdStorage.removeRegisteredDeviceId();
                    ZendeskPushRegistrationProvider.this.blipsProvider.corePushDisabled(userId);
                    rs4 rs4Var2 = rs4Var;
                    if (rs4Var2 != null) {
                        rs4Var2.onSuccess(r3);
                    }
                }
            }));
        }
    }

    @Override // zendesk.core.PushRegistrationProviderInternal
    public String sendPushRegistrationRequest(PushRegistrationRequest pushRegistrationRequest) {
        try {
            n<PushRegistrationResponseWrapper> execute = this.pushService.registerDevice(new PushRegistrationRequestWrapper(pushRegistrationRequest)).execute();
            if (execute.a() == null || execute.a().getRegistrationResponse() == null) {
                return "";
            }
            String identifier = execute.a().getRegistrationResponse().getIdentifier();
            onSuccessfulRegistration(identifier);
            return identifier;
        } catch (IOException e) {
            Logger.d(LOG_TAG, "Push registration request failed.", e, new Object[0]);
            return "";
        }
    }
}
