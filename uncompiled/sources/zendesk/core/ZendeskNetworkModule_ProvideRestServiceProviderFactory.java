package zendesk.core;

import okhttp3.OkHttpClient;
import retrofit2.o;

/* loaded from: classes3.dex */
public final class ZendeskNetworkModule_ProvideRestServiceProviderFactory implements y11<RestServiceProvider> {
    private final ew2<OkHttpClient> coreOkHttpClientProvider;
    private final ew2<OkHttpClient> mediaOkHttpClientProvider;
    private final ZendeskNetworkModule module;
    private final ew2<o> retrofitProvider;
    private final ew2<OkHttpClient> standardOkHttpClientProvider;

    public ZendeskNetworkModule_ProvideRestServiceProviderFactory(ZendeskNetworkModule zendeskNetworkModule, ew2<o> ew2Var, ew2<OkHttpClient> ew2Var2, ew2<OkHttpClient> ew2Var3, ew2<OkHttpClient> ew2Var4) {
        this.module = zendeskNetworkModule;
        this.retrofitProvider = ew2Var;
        this.mediaOkHttpClientProvider = ew2Var2;
        this.standardOkHttpClientProvider = ew2Var3;
        this.coreOkHttpClientProvider = ew2Var4;
    }

    public static ZendeskNetworkModule_ProvideRestServiceProviderFactory create(ZendeskNetworkModule zendeskNetworkModule, ew2<o> ew2Var, ew2<OkHttpClient> ew2Var2, ew2<OkHttpClient> ew2Var3, ew2<OkHttpClient> ew2Var4) {
        return new ZendeskNetworkModule_ProvideRestServiceProviderFactory(zendeskNetworkModule, ew2Var, ew2Var2, ew2Var3, ew2Var4);
    }

    public static RestServiceProvider provideRestServiceProvider(ZendeskNetworkModule zendeskNetworkModule, o oVar, OkHttpClient okHttpClient, OkHttpClient okHttpClient2, OkHttpClient okHttpClient3) {
        return (RestServiceProvider) cu2.f(zendeskNetworkModule.provideRestServiceProvider(oVar, okHttpClient, okHttpClient2, okHttpClient3));
    }

    @Override // defpackage.ew2
    public RestServiceProvider get() {
        return provideRestServiceProvider(this.module, this.retrofitProvider.get(), this.mediaOkHttpClientProvider.get(), this.standardOkHttpClientProvider.get(), this.coreOkHttpClientProvider.get());
    }
}
