package zendesk.core;

import retrofit2.b;

/* loaded from: classes3.dex */
interface UserService {
    @oo2("/api/mobile/user_tags.json")
    b<UserResponse> addTags(@ar UserTagRequest userTagRequest);

    @kd0("/api/mobile/user_tags/destroy_many.json")
    b<UserResponse> deleteTags(@yw2("tags") String str);

    @ge1("/api/mobile/users/me.json")
    b<UserResponse> getUser();

    @ge1("/api/mobile/user_fields.json")
    b<UserFieldResponse> getUserFields();

    @qo2("/api/mobile/users/me.json")
    b<UserResponse> setUserFields(@ar UserFieldRequest userFieldRequest);
}
