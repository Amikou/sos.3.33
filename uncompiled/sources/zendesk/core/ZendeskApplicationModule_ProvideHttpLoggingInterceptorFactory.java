package zendesk.core;

import okhttp3.logging.HttpLoggingInterceptor;

/* loaded from: classes3.dex */
public final class ZendeskApplicationModule_ProvideHttpLoggingInterceptorFactory implements y11<HttpLoggingInterceptor> {

    /* loaded from: classes3.dex */
    public static final class InstanceHolder {
        private static final ZendeskApplicationModule_ProvideHttpLoggingInterceptorFactory INSTANCE = new ZendeskApplicationModule_ProvideHttpLoggingInterceptorFactory();

        private InstanceHolder() {
        }
    }

    public static ZendeskApplicationModule_ProvideHttpLoggingInterceptorFactory create() {
        return InstanceHolder.INSTANCE;
    }

    public static HttpLoggingInterceptor provideHttpLoggingInterceptor() {
        return (HttpLoggingInterceptor) cu2.f(ZendeskApplicationModule.provideHttpLoggingInterceptor());
    }

    @Override // defpackage.ew2
    public HttpLoggingInterceptor get() {
        return provideHttpLoggingInterceptor();
    }
}
