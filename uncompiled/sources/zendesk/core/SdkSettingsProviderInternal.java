package zendesk.core;

/* loaded from: classes3.dex */
interface SdkSettingsProviderInternal {
    BlipsSettings getBlipsSettings();

    CoreSettings getCoreSettings();
}
