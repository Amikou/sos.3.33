package zendesk.core;

/* loaded from: classes3.dex */
public final class CoreModule_GetApplicationConfigurationFactory implements y11<ApplicationConfiguration> {
    private final CoreModule module;

    public CoreModule_GetApplicationConfigurationFactory(CoreModule coreModule) {
        this.module = coreModule;
    }

    public static CoreModule_GetApplicationConfigurationFactory create(CoreModule coreModule) {
        return new CoreModule_GetApplicationConfigurationFactory(coreModule);
    }

    public static ApplicationConfiguration getApplicationConfiguration(CoreModule coreModule) {
        return (ApplicationConfiguration) cu2.f(coreModule.getApplicationConfiguration());
    }

    @Override // defpackage.ew2
    public ApplicationConfiguration get() {
        return getApplicationConfiguration(this.module);
    }
}
