package zendesk.core;

/* loaded from: classes3.dex */
public interface NetworkAware {
    void onNetworkAvailable();

    void onNetworkUnavailable();
}
