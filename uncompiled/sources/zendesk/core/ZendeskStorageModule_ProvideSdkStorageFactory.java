package zendesk.core;

/* loaded from: classes3.dex */
public final class ZendeskStorageModule_ProvideSdkStorageFactory implements y11<Storage> {
    private final ew2<MemoryCache> memoryCacheProvider;
    private final ew2<BaseStorage> sdkBaseStorageProvider;
    private final ew2<SessionStorage> sessionStorageProvider;
    private final ew2<SettingsStorage> settingsStorageProvider;

    public ZendeskStorageModule_ProvideSdkStorageFactory(ew2<SettingsStorage> ew2Var, ew2<SessionStorage> ew2Var2, ew2<BaseStorage> ew2Var3, ew2<MemoryCache> ew2Var4) {
        this.settingsStorageProvider = ew2Var;
        this.sessionStorageProvider = ew2Var2;
        this.sdkBaseStorageProvider = ew2Var3;
        this.memoryCacheProvider = ew2Var4;
    }

    public static ZendeskStorageModule_ProvideSdkStorageFactory create(ew2<SettingsStorage> ew2Var, ew2<SessionStorage> ew2Var2, ew2<BaseStorage> ew2Var3, ew2<MemoryCache> ew2Var4) {
        return new ZendeskStorageModule_ProvideSdkStorageFactory(ew2Var, ew2Var2, ew2Var3, ew2Var4);
    }

    public static Storage provideSdkStorage(Object obj, SessionStorage sessionStorage, BaseStorage baseStorage, MemoryCache memoryCache) {
        return (Storage) cu2.f(ZendeskStorageModule.provideSdkStorage((SettingsStorage) obj, sessionStorage, baseStorage, memoryCache));
    }

    @Override // defpackage.ew2
    public Storage get() {
        return provideSdkStorage(this.settingsStorageProvider.get(), this.sessionStorageProvider.get(), this.sdkBaseStorageProvider.get(), this.memoryCacheProvider.get());
    }
}
