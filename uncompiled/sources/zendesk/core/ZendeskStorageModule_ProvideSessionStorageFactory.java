package zendesk.core;

import java.io.File;
import okhttp3.Cache;

/* loaded from: classes3.dex */
public final class ZendeskStorageModule_ProvideSessionStorageFactory implements y11<SessionStorage> {
    private final ew2<BaseStorage> additionalSdkStorageProvider;
    private final ew2<File> belvedereDirProvider;
    private final ew2<File> cacheDirProvider;
    private final ew2<Cache> cacheProvider;
    private final ew2<File> dataDirProvider;
    private final ew2<IdentityStorage> identityStorageProvider;
    private final ew2<BaseStorage> mediaCacheProvider;

    public ZendeskStorageModule_ProvideSessionStorageFactory(ew2<IdentityStorage> ew2Var, ew2<BaseStorage> ew2Var2, ew2<BaseStorage> ew2Var3, ew2<Cache> ew2Var4, ew2<File> ew2Var5, ew2<File> ew2Var6, ew2<File> ew2Var7) {
        this.identityStorageProvider = ew2Var;
        this.additionalSdkStorageProvider = ew2Var2;
        this.mediaCacheProvider = ew2Var3;
        this.cacheProvider = ew2Var4;
        this.cacheDirProvider = ew2Var5;
        this.dataDirProvider = ew2Var6;
        this.belvedereDirProvider = ew2Var7;
    }

    public static ZendeskStorageModule_ProvideSessionStorageFactory create(ew2<IdentityStorage> ew2Var, ew2<BaseStorage> ew2Var2, ew2<BaseStorage> ew2Var3, ew2<Cache> ew2Var4, ew2<File> ew2Var5, ew2<File> ew2Var6, ew2<File> ew2Var7) {
        return new ZendeskStorageModule_ProvideSessionStorageFactory(ew2Var, ew2Var2, ew2Var3, ew2Var4, ew2Var5, ew2Var6, ew2Var7);
    }

    public static SessionStorage provideSessionStorage(Object obj, BaseStorage baseStorage, BaseStorage baseStorage2, Cache cache, File file, File file2, File file3) {
        return (SessionStorage) cu2.f(ZendeskStorageModule.provideSessionStorage((IdentityStorage) obj, baseStorage, baseStorage2, cache, file, file2, file3));
    }

    @Override // defpackage.ew2
    public SessionStorage get() {
        return provideSessionStorage(this.identityStorageProvider.get(), this.additionalSdkStorageProvider.get(), this.mediaCacheProvider.get(), this.cacheProvider.get(), this.cacheDirProvider.get(), this.dataDirProvider.get(), this.belvedereDirProvider.get());
    }
}
