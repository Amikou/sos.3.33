package zendesk.core;

/* loaded from: classes3.dex */
public final class ZendeskStorageModule_ProvideSettingsStorageFactory implements y11<SettingsStorage> {
    private final ew2<BaseStorage> baseStorageProvider;

    public ZendeskStorageModule_ProvideSettingsStorageFactory(ew2<BaseStorage> ew2Var) {
        this.baseStorageProvider = ew2Var;
    }

    public static ZendeskStorageModule_ProvideSettingsStorageFactory create(ew2<BaseStorage> ew2Var) {
        return new ZendeskStorageModule_ProvideSettingsStorageFactory(ew2Var);
    }

    public static SettingsStorage provideSettingsStorage(BaseStorage baseStorage) {
        return (SettingsStorage) cu2.f(ZendeskStorageModule.provideSettingsStorage(baseStorage));
    }

    @Override // defpackage.ew2
    public SettingsStorage get() {
        return provideSettingsStorage(this.baseStorageProvider.get());
    }
}
