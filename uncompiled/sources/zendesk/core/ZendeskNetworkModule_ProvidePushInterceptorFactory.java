package zendesk.core;

/* loaded from: classes3.dex */
public final class ZendeskNetworkModule_ProvidePushInterceptorFactory implements y11<ZendeskPushInterceptor> {
    private final ew2<IdentityStorage> identityStorageProvider;
    private final ew2<PushDeviceIdStorage> pushDeviceIdStorageProvider;
    private final ew2<PushRegistrationProviderInternal> pushProvider;

    public ZendeskNetworkModule_ProvidePushInterceptorFactory(ew2<PushRegistrationProviderInternal> ew2Var, ew2<PushDeviceIdStorage> ew2Var2, ew2<IdentityStorage> ew2Var3) {
        this.pushProvider = ew2Var;
        this.pushDeviceIdStorageProvider = ew2Var2;
        this.identityStorageProvider = ew2Var3;
    }

    public static ZendeskNetworkModule_ProvidePushInterceptorFactory create(ew2<PushRegistrationProviderInternal> ew2Var, ew2<PushDeviceIdStorage> ew2Var2, ew2<IdentityStorage> ew2Var3) {
        return new ZendeskNetworkModule_ProvidePushInterceptorFactory(ew2Var, ew2Var2, ew2Var3);
    }

    public static ZendeskPushInterceptor providePushInterceptor(Object obj, Object obj2, Object obj3) {
        return (ZendeskPushInterceptor) cu2.f(ZendeskNetworkModule.providePushInterceptor((PushRegistrationProviderInternal) obj, (PushDeviceIdStorage) obj2, (IdentityStorage) obj3));
    }

    @Override // defpackage.ew2
    public ZendeskPushInterceptor get() {
        return providePushInterceptor(this.pushProvider.get(), this.pushDeviceIdStorageProvider.get(), this.identityStorageProvider.get());
    }
}
