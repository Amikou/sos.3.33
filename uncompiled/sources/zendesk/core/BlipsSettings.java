package zendesk.core;

/* loaded from: classes3.dex */
class BlipsSettings {
    private BlipsPermissions permissions;

    public BlipsSettings(BlipsPermissions blipsPermissions) {
        this.permissions = blipsPermissions;
    }

    public BlipsPermissions getBlipsPermissions() {
        return this.permissions;
    }
}
