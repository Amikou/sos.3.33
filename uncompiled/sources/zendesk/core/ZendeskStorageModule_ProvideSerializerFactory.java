package zendesk.core;

import com.google.gson.Gson;

/* loaded from: classes3.dex */
public final class ZendeskStorageModule_ProvideSerializerFactory implements y11<Serializer> {
    private final ew2<Gson> gsonProvider;

    public ZendeskStorageModule_ProvideSerializerFactory(ew2<Gson> ew2Var) {
        this.gsonProvider = ew2Var;
    }

    public static ZendeskStorageModule_ProvideSerializerFactory create(ew2<Gson> ew2Var) {
        return new ZendeskStorageModule_ProvideSerializerFactory(ew2Var);
    }

    public static Serializer provideSerializer(Gson gson) {
        return (Serializer) cu2.f(ZendeskStorageModule.provideSerializer(gson));
    }

    @Override // defpackage.ew2
    public Serializer get() {
        return provideSerializer(this.gsonProvider.get());
    }
}
