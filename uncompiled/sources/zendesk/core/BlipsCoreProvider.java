package zendesk.core;

/* JADX INFO: Access modifiers changed from: package-private */
/* loaded from: classes3.dex */
public interface BlipsCoreProvider {
    void coreInitialized();

    void corePushDisabled(Long l);

    void corePushEnabled();
}
