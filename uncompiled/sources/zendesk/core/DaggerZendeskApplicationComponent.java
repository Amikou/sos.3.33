package zendesk.core;

import android.content.Context;
import android.net.ConnectivityManager;
import com.google.gson.Gson;
import java.io.File;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.ScheduledExecutorService;
import okhttp3.Cache;
import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.o;

/* JADX INFO: Access modifiers changed from: package-private */
/* loaded from: classes3.dex */
public final class DaggerZendeskApplicationComponent implements ZendeskApplicationComponent {
    private ew2<ActionHandlerRegistry> actionHandlerRegistryProvider;
    private ew2<AcceptLanguageHeaderInterceptor> provideAcceptLanguageHeaderInterceptorProvider;
    private ew2<ZendeskAccessInterceptor> provideAccessInterceptorProvider;
    private ew2<AccessProvider> provideAccessProvider;
    private ew2<AccessService> provideAccessServiceProvider;
    private ew2<BaseStorage> provideAdditionalSdkBaseStorageProvider;
    private ew2<ApplicationConfiguration> provideApplicationConfigurationProvider;
    private ew2<Context> provideApplicationContextProvider;
    private ew2<ZendeskAuthHeaderInterceptor> provideAuthHeaderInterceptorProvider;
    private ew2<AuthenticationProvider> provideAuthProvider;
    private ew2<Serializer> provideBase64SerializerProvider;
    private ew2<OkHttpClient> provideBaseOkHttpClientProvider;
    private ew2<BlipsService> provideBlipsServiceProvider;
    private ew2<Cache> provideCacheProvider;
    private ew2<CachingInterceptor> provideCachingInterceptorProvider;
    private ew2<OkHttpClient> provideCoreOkHttpClientProvider;
    private ew2<o> provideCoreRetrofitProvider;
    private ew2<CoreModule> provideCoreSdkModuleProvider;
    private ew2<CoreSettingsStorage> provideCoreSettingsStorageProvider;
    private ew2<DeviceInfo> provideDeviceInfoProvider;
    private ew2<ScheduledExecutorService> provideExecutorProvider;
    private ew2<ExecutorService> provideExecutorServiceProvider;
    private ew2<Gson> provideGsonProvider;
    private ew2<HttpLoggingInterceptor> provideHttpLoggingInterceptorProvider;
    private ew2<BaseStorage> provideIdentityBaseStorageProvider;
    private ew2<IdentityManager> provideIdentityManagerProvider;
    private ew2<IdentityStorage> provideIdentityStorageProvider;
    private ew2<SharedPreferencesStorage> provideLegacyIdentityBaseStorageProvider;
    private ew2<LegacyIdentityMigrator> provideLegacyIdentityStorageProvider;
    private ew2<SharedPreferencesStorage> provideLegacyPushBaseStorageProvider;
    private ew2<MachineIdStorage> provideMachineIdStorageProvider;
    private ew2<OkHttpClient> provideMediaOkHttpClientProvider;
    private ew2<MemoryCache> provideMemoryCacheProvider;
    private ew2<OkHttpClient> provideOkHttpClientProvider;
    private ew2<ProviderStore> provideProviderStoreProvider;
    private ew2<PushDeviceIdStorage> providePushDeviceIdStorageProvider;
    private ew2<ZendeskPushInterceptor> providePushInterceptorProvider;
    private ew2<o> providePushProviderRetrofitProvider;
    private ew2<PushRegistrationProvider> providePushRegistrationProvider;
    private ew2<PushRegistrationProviderInternal> providePushRegistrationProviderInternalProvider;
    private ew2<PushRegistrationService> providePushRegistrationServiceProvider;
    private ew2<RestServiceProvider> provideRestServiceProvider;
    private ew2<o> provideRetrofitProvider;
    private ew2<BaseStorage> provideSdkBaseStorageProvider;
    private ew2<SettingsProvider> provideSdkSettingsProvider;
    private ew2<SdkSettingsProviderInternal> provideSdkSettingsProviderInternalProvider;
    private ew2<SdkSettingsService> provideSdkSettingsServiceProvider;
    private ew2<Storage> provideSdkStorageProvider;
    private ew2<Serializer> provideSerializerProvider;
    private ew2<SessionStorage> provideSessionStorageProvider;
    private ew2<BaseStorage> provideSettingsBaseStorageProvider;
    private ew2<ZendeskSettingsInterceptor> provideSettingsInterceptorProvider;
    private ew2<SettingsStorage> provideSettingsStorageProvider;
    private ew2<UserProvider> provideUserProvider;
    private ew2<UserService> provideUserServiceProvider;
    private ew2<ZendeskOauthIdHeaderInterceptor> provideZendeskBasicHeadersInterceptorProvider;
    private ew2<ZendeskLocaleConverter> provideZendeskLocaleConverterProvider;
    private ew2<ZendeskShadow> provideZendeskProvider;
    private ew2<ZendeskSettingsProvider> provideZendeskSdkSettingsProvider;
    private ew2<ZendeskUnauthorizedInterceptor> provideZendeskUnauthorizedInterceptorProvider;
    private ew2<BlipsCoreProvider> providerBlipsCoreProvider;
    private ew2<BlipsProvider> providerBlipsProvider;
    private ew2<ConnectivityManager> providerConnectivityManagerProvider;
    private ew2<NetworkInfoProvider> providerNetworkInfoProvider;
    private ew2<ZendeskBlipsProvider> providerZendeskBlipsProvider;
    private ew2<AcceptHeaderInterceptor> providesAcceptHeaderInterceptorProvider;
    private ew2<File> providesBelvedereDirProvider;
    private ew2<File> providesCacheDirProvider;
    private ew2<File> providesDataDirProvider;
    private ew2<BaseStorage> providesDiskLruStorageProvider;
    private ew2<UserAgentAndClientHeadersInterceptor> providesUserAgentHeaderInterceptorProvider;
    private final DaggerZendeskApplicationComponent zendeskApplicationComponent;

    /* loaded from: classes3.dex */
    public static final class Builder {
        private ZendeskApplicationModule zendeskApplicationModule;
        private ZendeskNetworkModule zendeskNetworkModule;

        public ZendeskApplicationComponent build() {
            cu2.a(this.zendeskApplicationModule, ZendeskApplicationModule.class);
            if (this.zendeskNetworkModule == null) {
                this.zendeskNetworkModule = new ZendeskNetworkModule();
            }
            return new DaggerZendeskApplicationComponent(this.zendeskApplicationModule, this.zendeskNetworkModule);
        }

        public Builder zendeskApplicationModule(ZendeskApplicationModule zendeskApplicationModule) {
            this.zendeskApplicationModule = (ZendeskApplicationModule) cu2.b(zendeskApplicationModule);
            return this;
        }

        public Builder zendeskNetworkModule(ZendeskNetworkModule zendeskNetworkModule) {
            this.zendeskNetworkModule = (ZendeskNetworkModule) cu2.b(zendeskNetworkModule);
            return this;
        }

        @Deprecated
        public Builder zendeskProvidersModule(ZendeskProvidersModule zendeskProvidersModule) {
            cu2.b(zendeskProvidersModule);
            return this;
        }

        @Deprecated
        public Builder zendeskStorageModule(ZendeskStorageModule zendeskStorageModule) {
            cu2.b(zendeskStorageModule);
            return this;
        }

        private Builder() {
        }
    }

    public static Builder builder() {
        return new Builder();
    }

    private void initialize(ZendeskApplicationModule zendeskApplicationModule, ZendeskNetworkModule zendeskNetworkModule) {
        this.provideApplicationContextProvider = fq0.a(ZendeskApplicationModule_ProvideApplicationContextFactory.create(zendeskApplicationModule));
        ew2<Gson> a = tp3.a(ZendeskApplicationModule_ProvideGsonFactory.create());
        this.provideGsonProvider = a;
        ew2<Serializer> a2 = fq0.a(ZendeskStorageModule_ProvideSerializerFactory.create(a));
        this.provideSerializerProvider = a2;
        ew2<BaseStorage> a3 = fq0.a(ZendeskStorageModule_ProvideSettingsBaseStorageFactory.create(this.provideApplicationContextProvider, a2));
        this.provideSettingsBaseStorageProvider = a3;
        this.provideSettingsStorageProvider = fq0.a(ZendeskStorageModule_ProvideSettingsStorageFactory.create(a3));
        ew2<BaseStorage> a4 = fq0.a(ZendeskStorageModule_ProvideIdentityBaseStorageFactory.create(this.provideApplicationContextProvider, this.provideSerializerProvider));
        this.provideIdentityBaseStorageProvider = a4;
        this.provideIdentityStorageProvider = fq0.a(ZendeskStorageModule_ProvideIdentityStorageFactory.create(a4));
        this.provideAdditionalSdkBaseStorageProvider = fq0.a(ZendeskStorageModule_ProvideAdditionalSdkBaseStorageFactory.create(this.provideApplicationContextProvider, this.provideSerializerProvider));
        ew2<File> a5 = fq0.a(ZendeskStorageModule_ProvidesCacheDirFactory.create(this.provideApplicationContextProvider));
        this.providesCacheDirProvider = a5;
        this.providesDiskLruStorageProvider = fq0.a(ZendeskStorageModule_ProvidesDiskLruStorageFactory.create(a5, this.provideSerializerProvider));
        this.provideCacheProvider = fq0.a(ZendeskStorageModule_ProvideCacheFactory.create(this.providesCacheDirProvider));
        this.providesDataDirProvider = fq0.a(ZendeskStorageModule_ProvidesDataDirFactory.create(this.provideApplicationContextProvider));
        ew2<File> a6 = fq0.a(ZendeskStorageModule_ProvidesBelvedereDirFactory.create(this.provideApplicationContextProvider));
        this.providesBelvedereDirProvider = a6;
        this.provideSessionStorageProvider = fq0.a(ZendeskStorageModule_ProvideSessionStorageFactory.create(this.provideIdentityStorageProvider, this.provideAdditionalSdkBaseStorageProvider, this.providesDiskLruStorageProvider, this.provideCacheProvider, this.providesCacheDirProvider, this.providesDataDirProvider, a6));
        this.provideSdkBaseStorageProvider = fq0.a(ZendeskStorageModule_ProvideSdkBaseStorageFactory.create(this.provideApplicationContextProvider, this.provideSerializerProvider));
        ew2<MemoryCache> a7 = fq0.a(ZendeskStorageModule_ProvideMemoryCacheFactory.create());
        this.provideMemoryCacheProvider = a7;
        this.provideSdkStorageProvider = fq0.a(ZendeskStorageModule_ProvideSdkStorageFactory.create(this.provideSettingsStorageProvider, this.provideSessionStorageProvider, this.provideSdkBaseStorageProvider, a7));
        this.provideLegacyIdentityBaseStorageProvider = fq0.a(ZendeskStorageModule_ProvideLegacyIdentityBaseStorageFactory.create(this.provideApplicationContextProvider, this.provideSerializerProvider));
        this.provideLegacyPushBaseStorageProvider = fq0.a(ZendeskStorageModule_ProvideLegacyPushBaseStorageFactory.create(this.provideApplicationContextProvider, this.provideSerializerProvider));
        this.provideIdentityManagerProvider = fq0.a(ZendeskStorageModule_ProvideIdentityManagerFactory.create(this.provideIdentityStorageProvider));
        ew2<PushDeviceIdStorage> a8 = fq0.a(ZendeskStorageModule_ProvidePushDeviceIdStorageFactory.create(this.provideAdditionalSdkBaseStorageProvider));
        this.providePushDeviceIdStorageProvider = a8;
        this.provideLegacyIdentityStorageProvider = fq0.a(ZendeskStorageModule_ProvideLegacyIdentityStorageFactory.create(this.provideLegacyIdentityBaseStorageProvider, this.provideLegacyPushBaseStorageProvider, this.provideIdentityStorageProvider, this.provideIdentityManagerProvider, a8));
        this.provideApplicationConfigurationProvider = fq0.a(ZendeskApplicationModule_ProvideApplicationConfigurationFactory.create(zendeskApplicationModule));
        this.provideHttpLoggingInterceptorProvider = tp3.a(ZendeskApplicationModule_ProvideHttpLoggingInterceptorFactory.create());
        this.provideZendeskBasicHeadersInterceptorProvider = tp3.a(ZendeskNetworkModule_ProvideZendeskBasicHeadersInterceptorFactory.create(zendeskNetworkModule, this.provideApplicationConfigurationProvider));
        this.providesUserAgentHeaderInterceptorProvider = tp3.a(ZendeskNetworkModule_ProvidesUserAgentHeaderInterceptorFactory.create(zendeskNetworkModule));
        ew2<ScheduledExecutorService> a9 = fq0.a(ZendeskApplicationModule_ProvideExecutorFactory.create());
        this.provideExecutorProvider = a9;
        ew2<ExecutorService> a10 = fq0.a(ZendeskApplicationModule_ProvideExecutorServiceFactory.create(a9));
        this.provideExecutorServiceProvider = a10;
        this.provideBaseOkHttpClientProvider = fq0.a(ZendeskNetworkModule_ProvideBaseOkHttpClientFactory.create(zendeskNetworkModule, this.provideHttpLoggingInterceptorProvider, this.provideZendeskBasicHeadersInterceptorProvider, this.providesUserAgentHeaderInterceptorProvider, a10));
        this.provideAcceptLanguageHeaderInterceptorProvider = tp3.a(ZendeskNetworkModule_ProvideAcceptLanguageHeaderInterceptorFactory.create(this.provideApplicationContextProvider));
        ew2<AcceptHeaderInterceptor> a11 = tp3.a(ZendeskNetworkModule_ProvidesAcceptHeaderInterceptorFactory.create());
        this.providesAcceptHeaderInterceptorProvider = a11;
        ew2<OkHttpClient> a12 = fq0.a(ZendeskNetworkModule_ProvideCoreOkHttpClientFactory.create(zendeskNetworkModule, this.provideBaseOkHttpClientProvider, this.provideAcceptLanguageHeaderInterceptorProvider, a11));
        this.provideCoreOkHttpClientProvider = a12;
        ew2<o> a13 = fq0.a(ZendeskNetworkModule_ProvideCoreRetrofitFactory.create(this.provideApplicationConfigurationProvider, this.provideGsonProvider, a12));
        this.provideCoreRetrofitProvider = a13;
        this.provideBlipsServiceProvider = fq0.a(ZendeskProvidersModule_ProvideBlipsServiceFactory.create(a13));
        this.provideDeviceInfoProvider = fq0.a(ZendeskApplicationModule_ProvideDeviceInfoFactory.create(this.provideApplicationContextProvider));
        this.provideBase64SerializerProvider = tp3.a(ZendeskApplicationModule_ProvideBase64SerializerFactory.create(zendeskApplicationModule, this.provideSerializerProvider));
        ew2<CoreSettingsStorage> a14 = fq0.a(ZendeskStorageModule_ProvideCoreSettingsStorageFactory.create(this.provideSettingsStorageProvider));
        this.provideCoreSettingsStorageProvider = a14;
        ew2<ZendeskBlipsProvider> a15 = fq0.a(ZendeskProvidersModule_ProviderZendeskBlipsProviderFactory.create(this.provideBlipsServiceProvider, this.provideDeviceInfoProvider, this.provideBase64SerializerProvider, this.provideIdentityManagerProvider, this.provideApplicationConfigurationProvider, a14, this.provideExecutorServiceProvider));
        this.providerZendeskBlipsProvider = a15;
        this.providerBlipsCoreProvider = fq0.a(ZendeskProvidersModule_ProviderBlipsCoreProviderFactory.create(a15));
        ew2<ZendeskAuthHeaderInterceptor> a16 = tp3.a(ZendeskNetworkModule_ProvideAuthHeaderInterceptorFactory.create(this.provideIdentityManagerProvider));
        this.provideAuthHeaderInterceptorProvider = a16;
        ew2<o> a17 = fq0.a(ZendeskNetworkModule_ProvidePushProviderRetrofitFactory.create(this.provideApplicationConfigurationProvider, this.provideGsonProvider, this.provideCoreOkHttpClientProvider, a16));
        this.providePushProviderRetrofitProvider = a17;
        this.providePushRegistrationServiceProvider = tp3.a(ZendeskProvidersModule_ProvidePushRegistrationServiceFactory.create(a17));
        this.provideSdkSettingsServiceProvider = tp3.a(ZendeskProvidersModule_ProvideSdkSettingsServiceFactory.create(this.provideCoreRetrofitProvider));
        this.actionHandlerRegistryProvider = fq0.a(ZendeskProvidersModule_ActionHandlerRegistryFactory.create());
        ew2<ZendeskLocaleConverter> a18 = fq0.a(ZendeskApplicationModule_ProvideZendeskLocaleConverterFactory.create(zendeskApplicationModule));
        this.provideZendeskLocaleConverterProvider = a18;
        ew2<ZendeskSettingsProvider> a19 = fq0.a(ZendeskProvidersModule_ProvideZendeskSdkSettingsProviderFactory.create(this.provideSdkSettingsServiceProvider, this.provideSettingsStorageProvider, this.provideCoreSettingsStorageProvider, this.actionHandlerRegistryProvider, this.provideSerializerProvider, a18, this.provideApplicationConfigurationProvider, this.provideApplicationContextProvider));
        this.provideZendeskSdkSettingsProvider = a19;
        ew2<SettingsProvider> a20 = fq0.a(ZendeskProvidersModule_ProvideSdkSettingsProviderFactory.create(a19));
        this.provideSdkSettingsProvider = a20;
        this.providePushRegistrationProvider = fq0.a(ZendeskProvidersModule_ProvidePushRegistrationProviderFactory.create(this.providePushRegistrationServiceProvider, this.provideIdentityManagerProvider, a20, this.providerBlipsCoreProvider, this.providePushDeviceIdStorageProvider, this.provideApplicationContextProvider));
        ew2<AccessService> a21 = tp3.a(ZendeskProvidersModule_ProvideAccessServiceFactory.create(this.provideCoreRetrofitProvider));
        this.provideAccessServiceProvider = a21;
        ew2<AccessProvider> a22 = fq0.a(ZendeskProvidersModule_ProvideAccessProviderFactory.create(this.provideIdentityManagerProvider, a21));
        this.provideAccessProvider = a22;
        this.provideAccessInterceptorProvider = tp3.a(ZendeskNetworkModule_ProvideAccessInterceptorFactory.create(this.provideIdentityManagerProvider, a22, this.provideSdkStorageProvider, this.provideCoreSettingsStorageProvider));
        this.provideZendeskUnauthorizedInterceptorProvider = tp3.a(ZendeskNetworkModule_ProvideZendeskUnauthorizedInterceptorFactory.create(this.provideSessionStorageProvider, this.provideIdentityManagerProvider));
        ew2<SdkSettingsProviderInternal> a23 = fq0.a(ZendeskProvidersModule_ProvideSdkSettingsProviderInternalFactory.create(this.provideZendeskSdkSettingsProvider));
        this.provideSdkSettingsProviderInternalProvider = a23;
        this.provideSettingsInterceptorProvider = tp3.a(ZendeskNetworkModule_ProvideSettingsInterceptorFactory.create(a23, this.provideSettingsStorageProvider));
        ew2<PushRegistrationProviderInternal> a24 = fq0.a(ZendeskProvidersModule_ProvidePushRegistrationProviderInternalFactory.create(this.providePushRegistrationProvider));
        this.providePushRegistrationProviderInternalProvider = a24;
        ew2<ZendeskPushInterceptor> a25 = tp3.a(ZendeskNetworkModule_ProvidePushInterceptorFactory.create(a24, this.providePushDeviceIdStorageProvider, this.provideIdentityStorageProvider));
        this.providePushInterceptorProvider = a25;
        ew2<OkHttpClient> a26 = fq0.a(ZendeskNetworkModule_ProvideOkHttpClientFactory.create(zendeskNetworkModule, this.provideBaseOkHttpClientProvider, this.provideAccessInterceptorProvider, this.provideZendeskUnauthorizedInterceptorProvider, this.provideAuthHeaderInterceptorProvider, this.provideSettingsInterceptorProvider, this.providesAcceptHeaderInterceptorProvider, a25, this.provideCacheProvider));
        this.provideOkHttpClientProvider = a26;
        this.provideRetrofitProvider = fq0.a(ZendeskNetworkModule_ProvideRetrofitFactory.create(this.provideApplicationConfigurationProvider, this.provideGsonProvider, a26));
        ew2<CachingInterceptor> a27 = tp3.a(ZendeskNetworkModule_ProvideCachingInterceptorFactory.create(this.providesDiskLruStorageProvider));
        this.provideCachingInterceptorProvider = a27;
        ew2<OkHttpClient> a28 = fq0.a(ZendeskNetworkModule_ProvideMediaOkHttpClientFactory.create(zendeskNetworkModule, this.provideBaseOkHttpClientProvider, this.provideAccessInterceptorProvider, this.provideAuthHeaderInterceptorProvider, this.provideSettingsInterceptorProvider, a27, this.provideZendeskUnauthorizedInterceptorProvider));
        this.provideMediaOkHttpClientProvider = a28;
        this.provideRestServiceProvider = fq0.a(ZendeskNetworkModule_ProvideRestServiceProviderFactory.create(zendeskNetworkModule, this.provideRetrofitProvider, a28, this.provideOkHttpClientProvider, this.provideCoreOkHttpClientProvider));
        this.providerBlipsProvider = fq0.a(ZendeskProvidersModule_ProviderBlipsProviderFactory.create(this.providerZendeskBlipsProvider));
        ew2<ConnectivityManager> a29 = fq0.a(ZendeskProvidersModule_ProviderConnectivityManagerFactory.create(this.provideApplicationContextProvider));
        this.providerConnectivityManagerProvider = a29;
        this.providerNetworkInfoProvider = fq0.a(ZendeskProvidersModule_ProviderNetworkInfoProviderFactory.create(a29));
        this.provideAuthProvider = fq0.a(ZendeskStorageModule_ProvideAuthProviderFactory.create(this.provideIdentityManagerProvider));
        ew2<MachineIdStorage> a30 = fq0.a(ZendeskStorageModule_ProvideMachineIdStorageFactory.create(this.provideApplicationContextProvider));
        this.provideMachineIdStorageProvider = a30;
        this.provideCoreSdkModuleProvider = tp3.a(ZendeskProvidersModule_ProvideCoreSdkModuleFactory.create(this.provideSdkSettingsProvider, this.provideRestServiceProvider, this.providerBlipsProvider, this.provideSessionStorageProvider, this.providerNetworkInfoProvider, this.provideMemoryCacheProvider, this.actionHandlerRegistryProvider, this.provideExecutorProvider, this.provideApplicationContextProvider, this.provideAuthProvider, this.provideApplicationConfigurationProvider, this.providePushRegistrationProvider, a30));
        ew2<UserService> a31 = tp3.a(ZendeskProvidersModule_ProvideUserServiceFactory.create(this.provideRetrofitProvider));
        this.provideUserServiceProvider = a31;
        ew2<UserProvider> a32 = fq0.a(ZendeskProvidersModule_ProvideUserProviderFactory.create(a31));
        this.provideUserProvider = a32;
        ew2<ProviderStore> a33 = fq0.a(ZendeskProvidersModule_ProvideProviderStoreFactory.create(a32, this.providePushRegistrationProvider));
        this.provideProviderStoreProvider = a33;
        this.provideZendeskProvider = fq0.a(ZendeskApplicationModule_ProvideZendeskFactory.create(this.provideSdkStorageProvider, this.provideLegacyIdentityStorageProvider, this.provideIdentityManagerProvider, this.providerBlipsCoreProvider, this.providePushRegistrationProvider, this.provideCoreSdkModuleProvider, a33));
    }

    @Override // zendesk.core.ZendeskApplicationComponent
    public ZendeskShadow zendeskShadow() {
        return this.provideZendeskProvider.get();
    }

    private DaggerZendeskApplicationComponent(ZendeskApplicationModule zendeskApplicationModule, ZendeskNetworkModule zendeskNetworkModule) {
        this.zendeskApplicationComponent = this;
        initialize(zendeskApplicationModule, zendeskNetworkModule);
    }
}
