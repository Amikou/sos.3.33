package zendesk.core;

import retrofit2.b;

/* loaded from: classes3.dex */
interface PushRegistrationService {
    @oo2("/api/mobile/push_notification_devices.json")
    b<PushRegistrationResponseWrapper> registerDevice(@ar PushRegistrationRequestWrapper pushRegistrationRequestWrapper);

    @kd0("/api/mobile/push_notification_devices/{id}.json")
    b<Void> unregisterDevice(@vp2("id") String str);
}
