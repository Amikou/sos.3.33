package zendesk.core;

import android.content.Context;
import java.io.File;

/* loaded from: classes3.dex */
public final class ZendeskStorageModule_ProvidesDataDirFactory implements y11<File> {
    private final ew2<Context> contextProvider;

    public ZendeskStorageModule_ProvidesDataDirFactory(ew2<Context> ew2Var) {
        this.contextProvider = ew2Var;
    }

    public static ZendeskStorageModule_ProvidesDataDirFactory create(ew2<Context> ew2Var) {
        return new ZendeskStorageModule_ProvidesDataDirFactory(ew2Var);
    }

    public static File providesDataDir(Context context) {
        return (File) cu2.f(ZendeskStorageModule.providesDataDir(context));
    }

    @Override // defpackage.ew2
    public File get() {
        return providesDataDir(this.contextProvider.get());
    }
}
