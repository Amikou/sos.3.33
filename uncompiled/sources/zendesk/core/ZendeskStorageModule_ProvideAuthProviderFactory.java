package zendesk.core;

/* loaded from: classes3.dex */
public final class ZendeskStorageModule_ProvideAuthProviderFactory implements y11<AuthenticationProvider> {
    private final ew2<IdentityManager> identityManagerProvider;

    public ZendeskStorageModule_ProvideAuthProviderFactory(ew2<IdentityManager> ew2Var) {
        this.identityManagerProvider = ew2Var;
    }

    public static ZendeskStorageModule_ProvideAuthProviderFactory create(ew2<IdentityManager> ew2Var) {
        return new ZendeskStorageModule_ProvideAuthProviderFactory(ew2Var);
    }

    public static AuthenticationProvider provideAuthProvider(Object obj) {
        return (AuthenticationProvider) cu2.f(ZendeskStorageModule.provideAuthProvider((IdentityManager) obj));
    }

    @Override // defpackage.ew2
    public AuthenticationProvider get() {
        return provideAuthProvider(this.identityManagerProvider.get());
    }
}
