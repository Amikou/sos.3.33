package zendesk.core;

/* loaded from: classes3.dex */
public final class ZendeskProvidersModule_ProvideProviderStoreFactory implements y11<ProviderStore> {
    private final ew2<PushRegistrationProvider> pushRegistrationProvider;
    private final ew2<UserProvider> userProvider;

    public ZendeskProvidersModule_ProvideProviderStoreFactory(ew2<UserProvider> ew2Var, ew2<PushRegistrationProvider> ew2Var2) {
        this.userProvider = ew2Var;
        this.pushRegistrationProvider = ew2Var2;
    }

    public static ZendeskProvidersModule_ProvideProviderStoreFactory create(ew2<UserProvider> ew2Var, ew2<PushRegistrationProvider> ew2Var2) {
        return new ZendeskProvidersModule_ProvideProviderStoreFactory(ew2Var, ew2Var2);
    }

    public static ProviderStore provideProviderStore(UserProvider userProvider, PushRegistrationProvider pushRegistrationProvider) {
        return (ProviderStore) cu2.f(ZendeskProvidersModule.provideProviderStore(userProvider, pushRegistrationProvider));
    }

    @Override // defpackage.ew2
    public ProviderStore get() {
        return provideProviderStore(this.userProvider.get(), this.pushRegistrationProvider.get());
    }
}
