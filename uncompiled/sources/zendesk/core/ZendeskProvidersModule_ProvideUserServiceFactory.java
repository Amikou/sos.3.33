package zendesk.core;

import retrofit2.o;

/* loaded from: classes3.dex */
public final class ZendeskProvidersModule_ProvideUserServiceFactory implements y11<UserService> {
    private final ew2<o> retrofitProvider;

    public ZendeskProvidersModule_ProvideUserServiceFactory(ew2<o> ew2Var) {
        this.retrofitProvider = ew2Var;
    }

    public static ZendeskProvidersModule_ProvideUserServiceFactory create(ew2<o> ew2Var) {
        return new ZendeskProvidersModule_ProvideUserServiceFactory(ew2Var);
    }

    public static UserService provideUserService(o oVar) {
        return (UserService) cu2.f(ZendeskProvidersModule.provideUserService(oVar));
    }

    @Override // defpackage.ew2
    public UserService get() {
        return provideUserService(this.retrofitProvider.get());
    }
}
