package zendesk.core;

import retrofit2.n;

/* loaded from: classes3.dex */
interface AccessProvider {
    public static final String NO_JWT_ERROR_MESSAGE = "The jwt user identifier is null or empty. We cannot proceed to get an access token";

    n<AuthenticationResponse> getAuthTokenViaAnonymous(AnonymousIdentity anonymousIdentity);

    n<AuthenticationResponse> getAuthTokenViaJwt(JwtIdentity jwtIdentity);
}
