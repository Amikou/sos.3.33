package zendesk.core;

import java.util.List;

/* loaded from: classes3.dex */
class UserTagRequest {
    private List<String> tags;

    public UserTagRequest(List<String> list) {
        this.tags = list;
    }
}
