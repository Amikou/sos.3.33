package zendesk.core;

import com.google.gson.Gson;
import okhttp3.OkHttpClient;
import retrofit2.o;

/* loaded from: classes3.dex */
public final class ZendeskNetworkModule_ProvidePushProviderRetrofitFactory implements y11<o> {
    private final ew2<ZendeskAuthHeaderInterceptor> authHeaderInterceptorProvider;
    private final ew2<ApplicationConfiguration> configurationProvider;
    private final ew2<Gson> gsonProvider;
    private final ew2<OkHttpClient> okHttpClientProvider;

    public ZendeskNetworkModule_ProvidePushProviderRetrofitFactory(ew2<ApplicationConfiguration> ew2Var, ew2<Gson> ew2Var2, ew2<OkHttpClient> ew2Var3, ew2<ZendeskAuthHeaderInterceptor> ew2Var4) {
        this.configurationProvider = ew2Var;
        this.gsonProvider = ew2Var2;
        this.okHttpClientProvider = ew2Var3;
        this.authHeaderInterceptorProvider = ew2Var4;
    }

    public static ZendeskNetworkModule_ProvidePushProviderRetrofitFactory create(ew2<ApplicationConfiguration> ew2Var, ew2<Gson> ew2Var2, ew2<OkHttpClient> ew2Var3, ew2<ZendeskAuthHeaderInterceptor> ew2Var4) {
        return new ZendeskNetworkModule_ProvidePushProviderRetrofitFactory(ew2Var, ew2Var2, ew2Var3, ew2Var4);
    }

    public static o providePushProviderRetrofit(ApplicationConfiguration applicationConfiguration, Gson gson, OkHttpClient okHttpClient, Object obj) {
        return (o) cu2.f(ZendeskNetworkModule.providePushProviderRetrofit(applicationConfiguration, gson, okHttpClient, (ZendeskAuthHeaderInterceptor) obj));
    }

    @Override // defpackage.ew2
    public o get() {
        return providePushProviderRetrofit(this.configurationProvider.get(), this.gsonProvider.get(), this.okHttpClientProvider.get(), this.authHeaderInterceptorProvider.get());
    }
}
