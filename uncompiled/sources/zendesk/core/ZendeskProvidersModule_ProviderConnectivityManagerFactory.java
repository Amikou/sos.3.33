package zendesk.core;

import android.content.Context;
import android.net.ConnectivityManager;

/* loaded from: classes3.dex */
public final class ZendeskProvidersModule_ProviderConnectivityManagerFactory implements y11<ConnectivityManager> {
    private final ew2<Context> contextProvider;

    public ZendeskProvidersModule_ProviderConnectivityManagerFactory(ew2<Context> ew2Var) {
        this.contextProvider = ew2Var;
    }

    public static ZendeskProvidersModule_ProviderConnectivityManagerFactory create(ew2<Context> ew2Var) {
        return new ZendeskProvidersModule_ProviderConnectivityManagerFactory(ew2Var);
    }

    public static ConnectivityManager providerConnectivityManager(Context context) {
        return (ConnectivityManager) cu2.f(ZendeskProvidersModule.providerConnectivityManager(context));
    }

    @Override // defpackage.ew2
    public ConnectivityManager get() {
        return providerConnectivityManager(this.contextProvider.get());
    }
}
