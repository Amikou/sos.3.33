package zendesk.core;

/* loaded from: classes3.dex */
public interface PushRegistrationProvider {
    boolean isRegisteredForPush();

    void registerWithDeviceIdentifier(String str, rs4<String> rs4Var);

    void registerWithUAChannelId(String str, rs4<String> rs4Var);

    void unregisterDevice(rs4<Void> rs4Var);
}
