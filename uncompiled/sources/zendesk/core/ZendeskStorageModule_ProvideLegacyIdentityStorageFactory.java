package zendesk.core;

/* loaded from: classes3.dex */
public final class ZendeskStorageModule_ProvideLegacyIdentityStorageFactory implements y11<LegacyIdentityMigrator> {
    private final ew2<IdentityManager> identityManagerProvider;
    private final ew2<IdentityStorage> identityStorageProvider;
    private final ew2<SharedPreferencesStorage> legacyIdentityBaseStorageProvider;
    private final ew2<SharedPreferencesStorage> legacyPushBaseStorageProvider;
    private final ew2<PushDeviceIdStorage> pushDeviceIdStorageProvider;

    public ZendeskStorageModule_ProvideLegacyIdentityStorageFactory(ew2<SharedPreferencesStorage> ew2Var, ew2<SharedPreferencesStorage> ew2Var2, ew2<IdentityStorage> ew2Var3, ew2<IdentityManager> ew2Var4, ew2<PushDeviceIdStorage> ew2Var5) {
        this.legacyIdentityBaseStorageProvider = ew2Var;
        this.legacyPushBaseStorageProvider = ew2Var2;
        this.identityStorageProvider = ew2Var3;
        this.identityManagerProvider = ew2Var4;
        this.pushDeviceIdStorageProvider = ew2Var5;
    }

    public static ZendeskStorageModule_ProvideLegacyIdentityStorageFactory create(ew2<SharedPreferencesStorage> ew2Var, ew2<SharedPreferencesStorage> ew2Var2, ew2<IdentityStorage> ew2Var3, ew2<IdentityManager> ew2Var4, ew2<PushDeviceIdStorage> ew2Var5) {
        return new ZendeskStorageModule_ProvideLegacyIdentityStorageFactory(ew2Var, ew2Var2, ew2Var3, ew2Var4, ew2Var5);
    }

    public static LegacyIdentityMigrator provideLegacyIdentityStorage(Object obj, Object obj2, Object obj3, Object obj4, Object obj5) {
        return (LegacyIdentityMigrator) cu2.f(ZendeskStorageModule.provideLegacyIdentityStorage((SharedPreferencesStorage) obj, (SharedPreferencesStorage) obj2, (IdentityStorage) obj3, (IdentityManager) obj4, (PushDeviceIdStorage) obj5));
    }

    @Override // defpackage.ew2
    public LegacyIdentityMigrator get() {
        return provideLegacyIdentityStorage(this.legacyIdentityBaseStorageProvider.get(), this.legacyPushBaseStorageProvider.get(), this.identityStorageProvider.get(), this.identityManagerProvider.get(), this.pushDeviceIdStorageProvider.get());
    }
}
