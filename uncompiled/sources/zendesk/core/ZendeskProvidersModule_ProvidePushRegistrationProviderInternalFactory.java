package zendesk.core;

/* loaded from: classes3.dex */
public final class ZendeskProvidersModule_ProvidePushRegistrationProviderInternalFactory implements y11<PushRegistrationProviderInternal> {
    private final ew2<PushRegistrationProvider> pushRegistrationProvider;

    public ZendeskProvidersModule_ProvidePushRegistrationProviderInternalFactory(ew2<PushRegistrationProvider> ew2Var) {
        this.pushRegistrationProvider = ew2Var;
    }

    public static ZendeskProvidersModule_ProvidePushRegistrationProviderInternalFactory create(ew2<PushRegistrationProvider> ew2Var) {
        return new ZendeskProvidersModule_ProvidePushRegistrationProviderInternalFactory(ew2Var);
    }

    public static PushRegistrationProviderInternal providePushRegistrationProviderInternal(PushRegistrationProvider pushRegistrationProvider) {
        return (PushRegistrationProviderInternal) cu2.f(ZendeskProvidersModule.providePushRegistrationProviderInternal(pushRegistrationProvider));
    }

    @Override // defpackage.ew2
    public PushRegistrationProviderInternal get() {
        return providePushRegistrationProviderInternal(this.pushRegistrationProvider.get());
    }
}
