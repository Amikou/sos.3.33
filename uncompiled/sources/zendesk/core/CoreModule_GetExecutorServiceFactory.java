package zendesk.core;

import java.util.concurrent.ExecutorService;

/* loaded from: classes3.dex */
public final class CoreModule_GetExecutorServiceFactory implements y11<ExecutorService> {
    private final CoreModule module;

    public CoreModule_GetExecutorServiceFactory(CoreModule coreModule) {
        this.module = coreModule;
    }

    public static CoreModule_GetExecutorServiceFactory create(CoreModule coreModule) {
        return new CoreModule_GetExecutorServiceFactory(coreModule);
    }

    public static ExecutorService getExecutorService(CoreModule coreModule) {
        return (ExecutorService) cu2.f(coreModule.getExecutorService());
    }

    @Override // defpackage.ew2
    public ExecutorService get() {
        return getExecutorService(this.module);
    }
}
