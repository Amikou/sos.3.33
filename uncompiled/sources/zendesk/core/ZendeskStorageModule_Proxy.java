package zendesk.core;

/* loaded from: classes3.dex */
public final class ZendeskStorageModule_Proxy {
    private ZendeskStorageModule_Proxy() {
    }

    public static ZendeskStorageModule newInstance() {
        return new ZendeskStorageModule();
    }
}
