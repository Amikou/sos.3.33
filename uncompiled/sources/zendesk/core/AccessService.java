package zendesk.core;

import retrofit2.b;

/* loaded from: classes3.dex */
interface AccessService {
    @oo2("/access/sdk/anonymous")
    b<AuthenticationResponse> getAuthTokenForAnonymous(@ar AuthenticationRequestWrapper authenticationRequestWrapper);

    @oo2("/access/sdk/jwt")
    b<AuthenticationResponse> getAuthTokenForJwt(@ar AuthenticationRequestWrapper authenticationRequestWrapper);
}
