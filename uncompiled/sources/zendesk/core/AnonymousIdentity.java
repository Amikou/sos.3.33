package zendesk.core;

/* loaded from: classes3.dex */
public final class AnonymousIdentity implements Identity {
    private String email;
    private String name;
    private String sdkGuid;

    /* loaded from: classes3.dex */
    public static class Builder {
        private String email;
        private String name;

        public Identity build() {
            return new AnonymousIdentity(this);
        }

        public Builder withEmailIdentifier(String str) {
            this.email = str;
            return this;
        }

        public Builder withNameIdentifier(String str) {
            this.name = str;
            return this;
        }
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null || AnonymousIdentity.class != obj.getClass()) {
            return false;
        }
        AnonymousIdentity anonymousIdentity = (AnonymousIdentity) obj;
        String str = this.email;
        if (str == null ? anonymousIdentity.email == null : str.equals(anonymousIdentity.email)) {
            String str2 = this.name;
            String str3 = anonymousIdentity.name;
            return str2 == null ? str3 == null : str2.equals(str3);
        }
        return false;
    }

    public String getEmail() {
        return this.email;
    }

    public String getName() {
        return this.name;
    }

    public String getSdkGuid() {
        return this.sdkGuid;
    }

    public int hashCode() {
        String str = this.email;
        int hashCode = (str != null ? str.hashCode() : 0) * 31;
        String str2 = this.name;
        return hashCode + (str2 != null ? str2.hashCode() : 0);
    }

    public void setSdkGuid(String str) {
        this.sdkGuid = str;
    }

    public AnonymousIdentity() {
    }

    private AnonymousIdentity(Builder builder) {
        this.email = builder.email;
        this.name = builder.name;
    }
}
