package zendesk.core;

import java.util.List;

/* loaded from: classes3.dex */
class UserFieldResponse {
    private List<UserField> userFields;

    public List<UserField> getUserFields() {
        return l10.c(this.userFields);
    }
}
