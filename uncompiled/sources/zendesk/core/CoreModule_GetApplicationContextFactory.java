package zendesk.core;

import android.content.Context;

/* loaded from: classes3.dex */
public final class CoreModule_GetApplicationContextFactory implements y11<Context> {
    private final CoreModule module;

    public CoreModule_GetApplicationContextFactory(CoreModule coreModule) {
        this.module = coreModule;
    }

    public static CoreModule_GetApplicationContextFactory create(CoreModule coreModule) {
        return new CoreModule_GetApplicationContextFactory(coreModule);
    }

    public static Context getApplicationContext(CoreModule coreModule) {
        return (Context) cu2.f(coreModule.getApplicationContext());
    }

    @Override // defpackage.ew2
    public Context get() {
        return getApplicationContext(this.module);
    }
}
