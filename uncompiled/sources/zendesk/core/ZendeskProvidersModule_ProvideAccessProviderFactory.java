package zendesk.core;

/* loaded from: classes3.dex */
public final class ZendeskProvidersModule_ProvideAccessProviderFactory implements y11<AccessProvider> {
    private final ew2<AccessService> accessServiceProvider;
    private final ew2<IdentityManager> identityManagerProvider;

    public ZendeskProvidersModule_ProvideAccessProviderFactory(ew2<IdentityManager> ew2Var, ew2<AccessService> ew2Var2) {
        this.identityManagerProvider = ew2Var;
        this.accessServiceProvider = ew2Var2;
    }

    public static ZendeskProvidersModule_ProvideAccessProviderFactory create(ew2<IdentityManager> ew2Var, ew2<AccessService> ew2Var2) {
        return new ZendeskProvidersModule_ProvideAccessProviderFactory(ew2Var, ew2Var2);
    }

    public static AccessProvider provideAccessProvider(Object obj, Object obj2) {
        return (AccessProvider) cu2.f(ZendeskProvidersModule.provideAccessProvider((IdentityManager) obj, (AccessService) obj2));
    }

    @Override // defpackage.ew2
    public AccessProvider get() {
        return provideAccessProvider(this.identityManagerProvider.get(), this.accessServiceProvider.get());
    }
}
