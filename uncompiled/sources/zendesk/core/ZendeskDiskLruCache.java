package zendesk.core;

import com.zendesk.logger.Logger;
import defpackage.ep0;
import java.io.ByteArrayInputStream;
import java.io.Closeable;
import java.io.File;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.util.Locale;
import okhttp3.MediaType;
import okhttp3.ResponseBody;
import okio.c;
import okio.k;
import okio.n;

/* loaded from: classes3.dex */
class ZendeskDiskLruCache implements BaseStorage {
    private static final int CACHE_INDEX = 0;
    private static final int ITEMS_PER_KEY = 1;
    private static final String LOG_TAG = "DiskLruStorage";
    private static final int VERSION_ONE = 1;
    private final File directory;
    private final long maxSize;
    private final Serializer serializer;
    private ep0 storage;

    public ZendeskDiskLruCache(File file, Serializer serializer, int i) {
        this.directory = file;
        long j = i;
        this.maxSize = j;
        this.storage = openCache(file, j);
        this.serializer = serializer;
    }

    private void close(Closeable closeable) {
        if (closeable != null) {
            try {
                closeable.close();
            } catch (IOException unused) {
            }
        }
    }

    private String getString(String str, int i) {
        Closeable closeable;
        Throwable th;
        n nVar;
        String str2;
        Closeable closeable2 = null;
        try {
            ep0.e u = this.storage.u(key(str));
            if (u != null) {
                nVar = k.l(u.a(i));
                try {
                    closeable = k.d(nVar);
                } catch (IOException e) {
                    e = e;
                    closeable = null;
                } catch (Throwable th2) {
                    th = th2;
                    closeable = null;
                    close(nVar);
                    close(closeable);
                    throw th;
                }
                try {
                    try {
                        closeable2 = nVar;
                        str2 = closeable.j1();
                    } catch (Throwable th3) {
                        th = th3;
                        close(nVar);
                        close(closeable);
                        throw th;
                    }
                } catch (IOException e2) {
                    e = e2;
                    Logger.j(LOG_TAG, "Unable to read from cache", e, new Object[0]);
                    close(nVar);
                    close(closeable);
                    return null;
                }
            } else {
                str2 = null;
                closeable = null;
            }
            close(closeable2);
            close(closeable);
            return str2;
        } catch (IOException e3) {
            e = e3;
            nVar = null;
            closeable = null;
        } catch (Throwable th4) {
            closeable = null;
            th = th4;
            nVar = null;
        }
    }

    private String key(String str) {
        return to0.c(str);
    }

    private String keyMediaType(String str) {
        return key(String.format(Locale.US, "%s_content_type", str));
    }

    private ep0 openCache(File file, long j) {
        try {
            return ep0.x(file, 1, 1, j);
        } catch (IOException unused) {
            Logger.k(LOG_TAG, "Unable to open cache", new Object[0]);
            return null;
        }
    }

    private void putString(String str, int i, String str2) {
        try {
            write(str, i, k.l(new ByteArrayInputStream(str2.getBytes("UTF-8"))));
        } catch (UnsupportedEncodingException e) {
            Logger.j(LOG_TAG, "Unable to encode string", e, new Object[0]);
        }
    }

    private void write(String str, int i, n nVar) {
        Closeable closeable;
        ep0.c q;
        c cVar = null;
        try {
            synchronized (this.directory) {
                q = this.storage.q(key(str));
            }
            if (q != null) {
                closeable = k.h(q.f(i));
                try {
                    try {
                        cVar = k.c(closeable);
                        cVar.P0(nVar);
                        cVar.flush();
                        q.e();
                    } catch (Throwable th) {
                        th = th;
                        close(cVar);
                        close(closeable);
                        close(nVar);
                        throw th;
                    }
                } catch (IOException e) {
                    e = e;
                    Logger.j(LOG_TAG, "Unable to cache data", e, new Object[0]);
                    close(cVar);
                    close(closeable);
                    close(nVar);
                }
            } else {
                closeable = null;
            }
        } catch (IOException e2) {
            e = e2;
            closeable = null;
        } catch (Throwable th2) {
            th = th2;
            closeable = null;
            close(cVar);
            close(closeable);
            close(nVar);
            throw th;
        }
        close(cVar);
        close(closeable);
        close(nVar);
    }

    @Override // zendesk.core.BaseStorage
    public void clear() {
        ep0 ep0Var = this.storage;
        if (ep0Var == null) {
            return;
        }
        try {
            try {
                if (ep0Var.v() != null && this.storage.v().exists() && l10.j(this.storage.v().listFiles())) {
                    this.storage.m();
                } else {
                    this.storage.close();
                }
            } catch (IOException e) {
                Logger.b(LOG_TAG, "Error clearing cache. Error: %s", e.getMessage());
            }
        } finally {
            this.storage = openCache(this.directory, this.maxSize);
        }
    }

    @Override // zendesk.core.BaseStorage
    public String get(String str) {
        if (this.storage == null) {
            return null;
        }
        return getString(str, 0);
    }

    @Override // zendesk.core.BaseStorage
    public void put(String str, String str2) {
        if (this.storage == null || ru3.d(str2)) {
            return;
        }
        putString(str, 0, str2);
    }

    @Override // zendesk.core.BaseStorage
    public void remove(String str) {
    }

    @Override // zendesk.core.BaseStorage
    public <E> E get(String str, Class<E> cls) {
        if (this.storage == null) {
            return null;
        }
        if (cls.equals(ResponseBody.class)) {
            try {
                ep0.e u = this.storage.u(key(str));
                if (u != null) {
                    n l = k.l(u.a(0));
                    long b = u.b(0);
                    String string = getString(keyMediaType(str), 0);
                    return (E) ResponseBody.create(ru3.b(string) ? MediaType.parse(string) : null, b, k.d(l));
                }
                return null;
            } catch (IOException e) {
                Logger.j(LOG_TAG, "Unable to read from cache", e, new Object[0]);
                return null;
            }
        }
        return (E) this.serializer.deserialize(getString(str, 0), cls);
    }

    @Override // zendesk.core.BaseStorage
    public void put(String str, Object obj) {
        if (this.storage == null) {
            return;
        }
        if (obj instanceof ResponseBody) {
            ResponseBody responseBody = (ResponseBody) obj;
            write(str, 0, responseBody.source());
            putString(keyMediaType(str), 0, responseBody.contentType().toString());
            return;
        }
        put(str, obj != null ? this.serializer.serialize(obj) : null);
    }

    public ZendeskDiskLruCache(File file, long j, ep0 ep0Var, Serializer serializer) {
        this.directory = file;
        this.maxSize = j;
        this.storage = ep0Var;
        this.serializer = serializer;
    }
}
