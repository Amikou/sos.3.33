package zendesk.core;

/* loaded from: classes3.dex */
public final class ZendeskProvidersModule_ProviderBlipsCoreProviderFactory implements y11<BlipsCoreProvider> {
    private final ew2<ZendeskBlipsProvider> zendeskBlipsProvider;

    public ZendeskProvidersModule_ProviderBlipsCoreProviderFactory(ew2<ZendeskBlipsProvider> ew2Var) {
        this.zendeskBlipsProvider = ew2Var;
    }

    public static ZendeskProvidersModule_ProviderBlipsCoreProviderFactory create(ew2<ZendeskBlipsProvider> ew2Var) {
        return new ZendeskProvidersModule_ProviderBlipsCoreProviderFactory(ew2Var);
    }

    public static BlipsCoreProvider providerBlipsCoreProvider(Object obj) {
        return (BlipsCoreProvider) cu2.f(ZendeskProvidersModule.providerBlipsCoreProvider((ZendeskBlipsProvider) obj));
    }

    @Override // defpackage.ew2
    public BlipsCoreProvider get() {
        return providerBlipsCoreProvider(this.zendeskBlipsProvider.get());
    }
}
