package zendesk.core;

/* loaded from: classes3.dex */
public final class ZendeskNetworkModule_ProvideZendeskUnauthorizedInterceptorFactory implements y11<ZendeskUnauthorizedInterceptor> {
    private final ew2<IdentityManager> identityManagerProvider;
    private final ew2<SessionStorage> sessionStorageProvider;

    public ZendeskNetworkModule_ProvideZendeskUnauthorizedInterceptorFactory(ew2<SessionStorage> ew2Var, ew2<IdentityManager> ew2Var2) {
        this.sessionStorageProvider = ew2Var;
        this.identityManagerProvider = ew2Var2;
    }

    public static ZendeskNetworkModule_ProvideZendeskUnauthorizedInterceptorFactory create(ew2<SessionStorage> ew2Var, ew2<IdentityManager> ew2Var2) {
        return new ZendeskNetworkModule_ProvideZendeskUnauthorizedInterceptorFactory(ew2Var, ew2Var2);
    }

    public static ZendeskUnauthorizedInterceptor provideZendeskUnauthorizedInterceptor(SessionStorage sessionStorage, Object obj) {
        return (ZendeskUnauthorizedInterceptor) cu2.f(ZendeskNetworkModule.provideZendeskUnauthorizedInterceptor(sessionStorage, (IdentityManager) obj));
    }

    @Override // defpackage.ew2
    public ZendeskUnauthorizedInterceptor get() {
        return provideZendeskUnauthorizedInterceptor(this.sessionStorageProvider.get(), this.identityManagerProvider.get());
    }
}
