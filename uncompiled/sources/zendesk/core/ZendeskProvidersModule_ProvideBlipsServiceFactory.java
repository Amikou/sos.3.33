package zendesk.core;

import retrofit2.o;

/* loaded from: classes3.dex */
public final class ZendeskProvidersModule_ProvideBlipsServiceFactory implements y11<BlipsService> {
    private final ew2<o> retrofitProvider;

    public ZendeskProvidersModule_ProvideBlipsServiceFactory(ew2<o> ew2Var) {
        this.retrofitProvider = ew2Var;
    }

    public static ZendeskProvidersModule_ProvideBlipsServiceFactory create(ew2<o> ew2Var) {
        return new ZendeskProvidersModule_ProvideBlipsServiceFactory(ew2Var);
    }

    public static BlipsService provideBlipsService(o oVar) {
        return (BlipsService) cu2.f(ZendeskProvidersModule.provideBlipsService(oVar));
    }

    @Override // defpackage.ew2
    public BlipsService get() {
        return provideBlipsService(this.retrofitProvider.get());
    }
}
