package zendesk.core;

/* loaded from: classes3.dex */
public final class CoreModule_GetMemoryCacheFactory implements y11<MemoryCache> {
    private final CoreModule module;

    public CoreModule_GetMemoryCacheFactory(CoreModule coreModule) {
        this.module = coreModule;
    }

    public static CoreModule_GetMemoryCacheFactory create(CoreModule coreModule) {
        return new CoreModule_GetMemoryCacheFactory(coreModule);
    }

    public static MemoryCache getMemoryCache(CoreModule coreModule) {
        return (MemoryCache) cu2.f(coreModule.getMemoryCache());
    }

    @Override // defpackage.ew2
    public MemoryCache get() {
        return getMemoryCache(this.module);
    }
}
