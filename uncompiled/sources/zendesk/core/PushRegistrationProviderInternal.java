package zendesk.core;

/* loaded from: classes3.dex */
interface PushRegistrationProviderInternal {
    String sendPushRegistrationRequest(PushRegistrationRequest pushRegistrationRequest);
}
