package zendesk.core;

import android.content.Context;

/* loaded from: classes3.dex */
public final class ZendeskStorageModule_ProvideMachineIdStorageFactory implements y11<MachineIdStorage> {
    private final ew2<Context> contextProvider;

    public ZendeskStorageModule_ProvideMachineIdStorageFactory(ew2<Context> ew2Var) {
        this.contextProvider = ew2Var;
    }

    public static ZendeskStorageModule_ProvideMachineIdStorageFactory create(ew2<Context> ew2Var) {
        return new ZendeskStorageModule_ProvideMachineIdStorageFactory(ew2Var);
    }

    public static MachineIdStorage provideMachineIdStorage(Context context) {
        return (MachineIdStorage) cu2.f(ZendeskStorageModule.provideMachineIdStorage(context));
    }

    @Override // defpackage.ew2
    public MachineIdStorage get() {
        return provideMachineIdStorage(this.contextProvider.get());
    }
}
