package zendesk.core;

/* loaded from: classes3.dex */
public final class CoreModule_GetRestServiceProviderFactory implements y11<RestServiceProvider> {
    private final CoreModule module;

    public CoreModule_GetRestServiceProviderFactory(CoreModule coreModule) {
        this.module = coreModule;
    }

    public static CoreModule_GetRestServiceProviderFactory create(CoreModule coreModule) {
        return new CoreModule_GetRestServiceProviderFactory(coreModule);
    }

    public static RestServiceProvider getRestServiceProvider(CoreModule coreModule) {
        return (RestServiceProvider) cu2.f(coreModule.getRestServiceProvider());
    }

    @Override // defpackage.ew2
    public RestServiceProvider get() {
        return getRestServiceProvider(this.module);
    }
}
