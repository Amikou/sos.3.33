package zendesk.core;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.ScheduledExecutorService;

/* loaded from: classes3.dex */
public final class ZendeskApplicationModule_ProvideExecutorServiceFactory implements y11<ExecutorService> {
    private final ew2<ScheduledExecutorService> scheduledExecutorServiceProvider;

    public ZendeskApplicationModule_ProvideExecutorServiceFactory(ew2<ScheduledExecutorService> ew2Var) {
        this.scheduledExecutorServiceProvider = ew2Var;
    }

    public static ZendeskApplicationModule_ProvideExecutorServiceFactory create(ew2<ScheduledExecutorService> ew2Var) {
        return new ZendeskApplicationModule_ProvideExecutorServiceFactory(ew2Var);
    }

    public static ExecutorService provideExecutorService(ScheduledExecutorService scheduledExecutorService) {
        return (ExecutorService) cu2.f(ZendeskApplicationModule.provideExecutorService(scheduledExecutorService));
    }

    @Override // defpackage.ew2
    public ExecutorService get() {
        return provideExecutorService(this.scheduledExecutorServiceProvider.get());
    }
}
