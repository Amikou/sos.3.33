package zendesk.core;

import com.google.gson.Gson;

/* loaded from: classes3.dex */
public final class ZendeskApplicationModule_ProvideGsonFactory implements y11<Gson> {

    /* loaded from: classes3.dex */
    public static final class InstanceHolder {
        private static final ZendeskApplicationModule_ProvideGsonFactory INSTANCE = new ZendeskApplicationModule_ProvideGsonFactory();

        private InstanceHolder() {
        }
    }

    public static ZendeskApplicationModule_ProvideGsonFactory create() {
        return InstanceHolder.INSTANCE;
    }

    public static Gson provideGson() {
        return (Gson) cu2.f(ZendeskApplicationModule.provideGson());
    }

    @Override // defpackage.ew2
    public Gson get() {
        return provideGson();
    }
}
