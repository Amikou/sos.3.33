package zendesk.core;

/* loaded from: classes3.dex */
public final class ZendeskNetworkModule_ProvideAuthHeaderInterceptorFactory implements y11<ZendeskAuthHeaderInterceptor> {
    private final ew2<IdentityManager> identityManagerProvider;

    public ZendeskNetworkModule_ProvideAuthHeaderInterceptorFactory(ew2<IdentityManager> ew2Var) {
        this.identityManagerProvider = ew2Var;
    }

    public static ZendeskNetworkModule_ProvideAuthHeaderInterceptorFactory create(ew2<IdentityManager> ew2Var) {
        return new ZendeskNetworkModule_ProvideAuthHeaderInterceptorFactory(ew2Var);
    }

    public static ZendeskAuthHeaderInterceptor provideAuthHeaderInterceptor(Object obj) {
        return (ZendeskAuthHeaderInterceptor) cu2.f(ZendeskNetworkModule.provideAuthHeaderInterceptor((IdentityManager) obj));
    }

    @Override // defpackage.ew2
    public ZendeskAuthHeaderInterceptor get() {
        return provideAuthHeaderInterceptor(this.identityManagerProvider.get());
    }
}
