package zendesk.core;

import java.util.concurrent.ScheduledExecutorService;

/* loaded from: classes3.dex */
public final class ZendeskApplicationModule_ProvideExecutorFactory implements y11<ScheduledExecutorService> {

    /* loaded from: classes3.dex */
    public static final class InstanceHolder {
        private static final ZendeskApplicationModule_ProvideExecutorFactory INSTANCE = new ZendeskApplicationModule_ProvideExecutorFactory();

        private InstanceHolder() {
        }
    }

    public static ZendeskApplicationModule_ProvideExecutorFactory create() {
        return InstanceHolder.INSTANCE;
    }

    public static ScheduledExecutorService provideExecutor() {
        return (ScheduledExecutorService) cu2.f(ZendeskApplicationModule.provideExecutor());
    }

    @Override // defpackage.ew2
    public ScheduledExecutorService get() {
        return provideExecutor();
    }
}
