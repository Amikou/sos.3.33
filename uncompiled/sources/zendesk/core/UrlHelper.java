package zendesk.core;

/* loaded from: classes3.dex */
public class UrlHelper {
    public static boolean isGuideRequest(String str) {
        return str.contains("/api/v2/help_center") || str.contains("/hc/api") || str.contains("/api/mobile/help_center");
    }
}
