package zendesk.core;

import android.content.Context;

/* loaded from: classes3.dex */
public final class ZendeskApplicationModule_ProvideApplicationContextFactory implements y11<Context> {
    private final ZendeskApplicationModule module;

    public ZendeskApplicationModule_ProvideApplicationContextFactory(ZendeskApplicationModule zendeskApplicationModule) {
        this.module = zendeskApplicationModule;
    }

    public static ZendeskApplicationModule_ProvideApplicationContextFactory create(ZendeskApplicationModule zendeskApplicationModule) {
        return new ZendeskApplicationModule_ProvideApplicationContextFactory(zendeskApplicationModule);
    }

    public static Context provideApplicationContext(ZendeskApplicationModule zendeskApplicationModule) {
        return (Context) cu2.f(zendeskApplicationModule.provideApplicationContext());
    }

    @Override // defpackage.ew2
    public Context get() {
        return provideApplicationContext(this.module);
    }
}
