package zendesk.core;

import android.content.Context;
import java.io.File;

/* loaded from: classes3.dex */
public final class ZendeskStorageModule_ProvidesCacheDirFactory implements y11<File> {
    private final ew2<Context> contextProvider;

    public ZendeskStorageModule_ProvidesCacheDirFactory(ew2<Context> ew2Var) {
        this.contextProvider = ew2Var;
    }

    public static ZendeskStorageModule_ProvidesCacheDirFactory create(ew2<Context> ew2Var) {
        return new ZendeskStorageModule_ProvidesCacheDirFactory(ew2Var);
    }

    public static File providesCacheDir(Context context) {
        return (File) cu2.f(ZendeskStorageModule.providesCacheDir(context));
    }

    @Override // defpackage.ew2
    public File get() {
        return providesCacheDir(this.contextProvider.get());
    }
}
