package zendesk.core;

/* loaded from: classes3.dex */
public final class ZendeskNetworkModule_ProvideCachingInterceptorFactory implements y11<CachingInterceptor> {
    private final ew2<BaseStorage> mediaCacheProvider;

    public ZendeskNetworkModule_ProvideCachingInterceptorFactory(ew2<BaseStorage> ew2Var) {
        this.mediaCacheProvider = ew2Var;
    }

    public static ZendeskNetworkModule_ProvideCachingInterceptorFactory create(ew2<BaseStorage> ew2Var) {
        return new ZendeskNetworkModule_ProvideCachingInterceptorFactory(ew2Var);
    }

    public static CachingInterceptor provideCachingInterceptor(BaseStorage baseStorage) {
        return (CachingInterceptor) cu2.f(ZendeskNetworkModule.provideCachingInterceptor(baseStorage));
    }

    @Override // defpackage.ew2
    public CachingInterceptor get() {
        return provideCachingInterceptor(this.mediaCacheProvider.get());
    }
}
