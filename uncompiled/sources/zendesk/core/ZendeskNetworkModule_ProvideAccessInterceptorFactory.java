package zendesk.core;

/* loaded from: classes3.dex */
public final class ZendeskNetworkModule_ProvideAccessInterceptorFactory implements y11<ZendeskAccessInterceptor> {
    private final ew2<AccessProvider> accessProvider;
    private final ew2<CoreSettingsStorage> coreSettingsStorageProvider;
    private final ew2<IdentityManager> identityManagerProvider;
    private final ew2<Storage> storageProvider;

    public ZendeskNetworkModule_ProvideAccessInterceptorFactory(ew2<IdentityManager> ew2Var, ew2<AccessProvider> ew2Var2, ew2<Storage> ew2Var3, ew2<CoreSettingsStorage> ew2Var4) {
        this.identityManagerProvider = ew2Var;
        this.accessProvider = ew2Var2;
        this.storageProvider = ew2Var3;
        this.coreSettingsStorageProvider = ew2Var4;
    }

    public static ZendeskNetworkModule_ProvideAccessInterceptorFactory create(ew2<IdentityManager> ew2Var, ew2<AccessProvider> ew2Var2, ew2<Storage> ew2Var3, ew2<CoreSettingsStorage> ew2Var4) {
        return new ZendeskNetworkModule_ProvideAccessInterceptorFactory(ew2Var, ew2Var2, ew2Var3, ew2Var4);
    }

    public static ZendeskAccessInterceptor provideAccessInterceptor(Object obj, Object obj2, Object obj3, Object obj4) {
        return (ZendeskAccessInterceptor) cu2.f(ZendeskNetworkModule.provideAccessInterceptor((IdentityManager) obj, (AccessProvider) obj2, (Storage) obj3, (CoreSettingsStorage) obj4));
    }

    @Override // defpackage.ew2
    public ZendeskAccessInterceptor get() {
        return provideAccessInterceptor(this.identityManagerProvider.get(), this.accessProvider.get(), this.storageProvider.get(), this.coreSettingsStorageProvider.get());
    }
}
