package zendesk.core;

/* loaded from: classes3.dex */
class AuthenticationResponse {
    private AccessToken authentication;

    public AccessToken getAuthentication() {
        return this.authentication;
    }
}
