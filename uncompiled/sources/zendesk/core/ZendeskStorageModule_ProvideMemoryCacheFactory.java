package zendesk.core;

/* loaded from: classes3.dex */
public final class ZendeskStorageModule_ProvideMemoryCacheFactory implements y11<MemoryCache> {

    /* loaded from: classes3.dex */
    public static final class InstanceHolder {
        private static final ZendeskStorageModule_ProvideMemoryCacheFactory INSTANCE = new ZendeskStorageModule_ProvideMemoryCacheFactory();

        private InstanceHolder() {
        }
    }

    public static ZendeskStorageModule_ProvideMemoryCacheFactory create() {
        return InstanceHolder.INSTANCE;
    }

    public static MemoryCache provideMemoryCache() {
        return (MemoryCache) cu2.f(ZendeskStorageModule.provideMemoryCache());
    }

    @Override // defpackage.ew2
    public MemoryCache get() {
        return provideMemoryCache();
    }
}
