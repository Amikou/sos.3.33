package zendesk.core;

/* loaded from: classes3.dex */
public final class ZendeskNetworkModule_ProvideZendeskBasicHeadersInterceptorFactory implements y11<ZendeskOauthIdHeaderInterceptor> {
    private final ew2<ApplicationConfiguration> configurationProvider;
    private final ZendeskNetworkModule module;

    public ZendeskNetworkModule_ProvideZendeskBasicHeadersInterceptorFactory(ZendeskNetworkModule zendeskNetworkModule, ew2<ApplicationConfiguration> ew2Var) {
        this.module = zendeskNetworkModule;
        this.configurationProvider = ew2Var;
    }

    public static ZendeskNetworkModule_ProvideZendeskBasicHeadersInterceptorFactory create(ZendeskNetworkModule zendeskNetworkModule, ew2<ApplicationConfiguration> ew2Var) {
        return new ZendeskNetworkModule_ProvideZendeskBasicHeadersInterceptorFactory(zendeskNetworkModule, ew2Var);
    }

    public static ZendeskOauthIdHeaderInterceptor provideZendeskBasicHeadersInterceptor(ZendeskNetworkModule zendeskNetworkModule, ApplicationConfiguration applicationConfiguration) {
        return (ZendeskOauthIdHeaderInterceptor) cu2.f(zendeskNetworkModule.provideZendeskBasicHeadersInterceptor(applicationConfiguration));
    }

    @Override // defpackage.ew2
    public ZendeskOauthIdHeaderInterceptor get() {
        return provideZendeskBasicHeadersInterceptor(this.module, this.configurationProvider.get());
    }
}
