package zendesk.core;

/* loaded from: classes3.dex */
public final class ZendeskStorageModule_ProvidePushDeviceIdStorageFactory implements y11<PushDeviceIdStorage> {
    private final ew2<BaseStorage> additionalSdkStorageProvider;

    public ZendeskStorageModule_ProvidePushDeviceIdStorageFactory(ew2<BaseStorage> ew2Var) {
        this.additionalSdkStorageProvider = ew2Var;
    }

    public static ZendeskStorageModule_ProvidePushDeviceIdStorageFactory create(ew2<BaseStorage> ew2Var) {
        return new ZendeskStorageModule_ProvidePushDeviceIdStorageFactory(ew2Var);
    }

    public static PushDeviceIdStorage providePushDeviceIdStorage(BaseStorage baseStorage) {
        return (PushDeviceIdStorage) cu2.f(ZendeskStorageModule.providePushDeviceIdStorage(baseStorage));
    }

    @Override // defpackage.ew2
    public PushDeviceIdStorage get() {
        return providePushDeviceIdStorage(this.additionalSdkStorageProvider.get());
    }
}
