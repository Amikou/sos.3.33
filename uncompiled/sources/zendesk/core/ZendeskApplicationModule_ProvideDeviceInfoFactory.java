package zendesk.core;

import android.content.Context;

/* loaded from: classes3.dex */
public final class ZendeskApplicationModule_ProvideDeviceInfoFactory implements y11<DeviceInfo> {
    private final ew2<Context> contextProvider;

    public ZendeskApplicationModule_ProvideDeviceInfoFactory(ew2<Context> ew2Var) {
        this.contextProvider = ew2Var;
    }

    public static ZendeskApplicationModule_ProvideDeviceInfoFactory create(ew2<Context> ew2Var) {
        return new ZendeskApplicationModule_ProvideDeviceInfoFactory(ew2Var);
    }

    public static DeviceInfo provideDeviceInfo(Context context) {
        return (DeviceInfo) cu2.f(ZendeskApplicationModule.provideDeviceInfo(context));
    }

    @Override // defpackage.ew2
    public DeviceInfo get() {
        return provideDeviceInfo(this.contextProvider.get());
    }
}
