package zendesk.core;

/* loaded from: classes3.dex */
public final class ZendeskProvidersModule_ProviderBlipsProviderFactory implements y11<BlipsProvider> {
    private final ew2<ZendeskBlipsProvider> zendeskBlipsProvider;

    public ZendeskProvidersModule_ProviderBlipsProviderFactory(ew2<ZendeskBlipsProvider> ew2Var) {
        this.zendeskBlipsProvider = ew2Var;
    }

    public static ZendeskProvidersModule_ProviderBlipsProviderFactory create(ew2<ZendeskBlipsProvider> ew2Var) {
        return new ZendeskProvidersModule_ProviderBlipsProviderFactory(ew2Var);
    }

    public static BlipsProvider providerBlipsProvider(Object obj) {
        return (BlipsProvider) cu2.f(ZendeskProvidersModule.providerBlipsProvider((ZendeskBlipsProvider) obj));
    }

    @Override // defpackage.ew2
    public BlipsProvider get() {
        return providerBlipsProvider(this.zendeskBlipsProvider.get());
    }
}
