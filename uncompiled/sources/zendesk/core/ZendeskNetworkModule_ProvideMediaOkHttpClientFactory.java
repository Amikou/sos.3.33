package zendesk.core;

import okhttp3.OkHttpClient;

/* loaded from: classes3.dex */
public final class ZendeskNetworkModule_ProvideMediaOkHttpClientFactory implements y11<OkHttpClient> {
    private final ew2<ZendeskAccessInterceptor> accessInterceptorProvider;
    private final ew2<ZendeskAuthHeaderInterceptor> authHeaderInterceptorProvider;
    private final ew2<CachingInterceptor> cachingInterceptorProvider;
    private final ZendeskNetworkModule module;
    private final ew2<OkHttpClient> okHttpClientProvider;
    private final ew2<ZendeskSettingsInterceptor> settingsInterceptorProvider;
    private final ew2<ZendeskUnauthorizedInterceptor> unauthorizedInterceptorProvider;

    public ZendeskNetworkModule_ProvideMediaOkHttpClientFactory(ZendeskNetworkModule zendeskNetworkModule, ew2<OkHttpClient> ew2Var, ew2<ZendeskAccessInterceptor> ew2Var2, ew2<ZendeskAuthHeaderInterceptor> ew2Var3, ew2<ZendeskSettingsInterceptor> ew2Var4, ew2<CachingInterceptor> ew2Var5, ew2<ZendeskUnauthorizedInterceptor> ew2Var6) {
        this.module = zendeskNetworkModule;
        this.okHttpClientProvider = ew2Var;
        this.accessInterceptorProvider = ew2Var2;
        this.authHeaderInterceptorProvider = ew2Var3;
        this.settingsInterceptorProvider = ew2Var4;
        this.cachingInterceptorProvider = ew2Var5;
        this.unauthorizedInterceptorProvider = ew2Var6;
    }

    public static ZendeskNetworkModule_ProvideMediaOkHttpClientFactory create(ZendeskNetworkModule zendeskNetworkModule, ew2<OkHttpClient> ew2Var, ew2<ZendeskAccessInterceptor> ew2Var2, ew2<ZendeskAuthHeaderInterceptor> ew2Var3, ew2<ZendeskSettingsInterceptor> ew2Var4, ew2<CachingInterceptor> ew2Var5, ew2<ZendeskUnauthorizedInterceptor> ew2Var6) {
        return new ZendeskNetworkModule_ProvideMediaOkHttpClientFactory(zendeskNetworkModule, ew2Var, ew2Var2, ew2Var3, ew2Var4, ew2Var5, ew2Var6);
    }

    public static OkHttpClient provideMediaOkHttpClient(ZendeskNetworkModule zendeskNetworkModule, OkHttpClient okHttpClient, Object obj, Object obj2, Object obj3, Object obj4, Object obj5) {
        return (OkHttpClient) cu2.f(zendeskNetworkModule.provideMediaOkHttpClient(okHttpClient, (ZendeskAccessInterceptor) obj, (ZendeskAuthHeaderInterceptor) obj2, (ZendeskSettingsInterceptor) obj3, (CachingInterceptor) obj4, (ZendeskUnauthorizedInterceptor) obj5));
    }

    @Override // defpackage.ew2
    public OkHttpClient get() {
        return provideMediaOkHttpClient(this.module, this.okHttpClientProvider.get(), this.accessInterceptorProvider.get(), this.authHeaderInterceptorProvider.get(), this.settingsInterceptorProvider.get(), this.cachingInterceptorProvider.get(), this.unauthorizedInterceptorProvider.get());
    }
}
