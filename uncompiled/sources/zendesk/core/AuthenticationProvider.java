package zendesk.core;

/* loaded from: classes3.dex */
public interface AuthenticationProvider {
    Identity getIdentity();
}
