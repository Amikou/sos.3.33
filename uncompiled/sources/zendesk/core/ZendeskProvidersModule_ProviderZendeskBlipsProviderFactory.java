package zendesk.core;

import java.util.concurrent.ExecutorService;

/* loaded from: classes3.dex */
public final class ZendeskProvidersModule_ProviderZendeskBlipsProviderFactory implements y11<ZendeskBlipsProvider> {
    private final ew2<ApplicationConfiguration> applicationConfigurationProvider;
    private final ew2<BlipsService> blipsServiceProvider;
    private final ew2<CoreSettingsStorage> coreSettingsStorageProvider;
    private final ew2<DeviceInfo> deviceInfoProvider;
    private final ew2<ExecutorService> executorProvider;
    private final ew2<IdentityManager> identityManagerProvider;
    private final ew2<Serializer> serializerProvider;

    public ZendeskProvidersModule_ProviderZendeskBlipsProviderFactory(ew2<BlipsService> ew2Var, ew2<DeviceInfo> ew2Var2, ew2<Serializer> ew2Var3, ew2<IdentityManager> ew2Var4, ew2<ApplicationConfiguration> ew2Var5, ew2<CoreSettingsStorage> ew2Var6, ew2<ExecutorService> ew2Var7) {
        this.blipsServiceProvider = ew2Var;
        this.deviceInfoProvider = ew2Var2;
        this.serializerProvider = ew2Var3;
        this.identityManagerProvider = ew2Var4;
        this.applicationConfigurationProvider = ew2Var5;
        this.coreSettingsStorageProvider = ew2Var6;
        this.executorProvider = ew2Var7;
    }

    public static ZendeskProvidersModule_ProviderZendeskBlipsProviderFactory create(ew2<BlipsService> ew2Var, ew2<DeviceInfo> ew2Var2, ew2<Serializer> ew2Var3, ew2<IdentityManager> ew2Var4, ew2<ApplicationConfiguration> ew2Var5, ew2<CoreSettingsStorage> ew2Var6, ew2<ExecutorService> ew2Var7) {
        return new ZendeskProvidersModule_ProviderZendeskBlipsProviderFactory(ew2Var, ew2Var2, ew2Var3, ew2Var4, ew2Var5, ew2Var6, ew2Var7);
    }

    public static ZendeskBlipsProvider providerZendeskBlipsProvider(Object obj, Object obj2, Object obj3, Object obj4, ApplicationConfiguration applicationConfiguration, Object obj5, ExecutorService executorService) {
        return (ZendeskBlipsProvider) cu2.f(ZendeskProvidersModule.providerZendeskBlipsProvider((BlipsService) obj, (DeviceInfo) obj2, (Serializer) obj3, (IdentityManager) obj4, applicationConfiguration, (CoreSettingsStorage) obj5, executorService));
    }

    @Override // defpackage.ew2
    public ZendeskBlipsProvider get() {
        return providerZendeskBlipsProvider(this.blipsServiceProvider.get(), this.deviceInfoProvider.get(), this.serializerProvider.get(), this.identityManagerProvider.get(), this.applicationConfigurationProvider.get(), this.coreSettingsStorageProvider.get(), this.executorProvider.get());
    }
}
