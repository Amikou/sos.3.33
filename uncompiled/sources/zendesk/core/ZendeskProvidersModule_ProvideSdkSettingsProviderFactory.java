package zendesk.core;

/* loaded from: classes3.dex */
public final class ZendeskProvidersModule_ProvideSdkSettingsProviderFactory implements y11<SettingsProvider> {
    private final ew2<ZendeskSettingsProvider> sdkSettingsProvider;

    public ZendeskProvidersModule_ProvideSdkSettingsProviderFactory(ew2<ZendeskSettingsProvider> ew2Var) {
        this.sdkSettingsProvider = ew2Var;
    }

    public static ZendeskProvidersModule_ProvideSdkSettingsProviderFactory create(ew2<ZendeskSettingsProvider> ew2Var) {
        return new ZendeskProvidersModule_ProvideSdkSettingsProviderFactory(ew2Var);
    }

    public static SettingsProvider provideSdkSettingsProvider(Object obj) {
        return (SettingsProvider) cu2.f(ZendeskProvidersModule.provideSdkSettingsProvider((ZendeskSettingsProvider) obj));
    }

    @Override // defpackage.ew2
    public SettingsProvider get() {
        return provideSdkSettingsProvider(this.sdkSettingsProvider.get());
    }
}
