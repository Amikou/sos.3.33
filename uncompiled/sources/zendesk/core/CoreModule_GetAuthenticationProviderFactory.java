package zendesk.core;

/* loaded from: classes3.dex */
public final class CoreModule_GetAuthenticationProviderFactory implements y11<AuthenticationProvider> {
    private final CoreModule module;

    public CoreModule_GetAuthenticationProviderFactory(CoreModule coreModule) {
        this.module = coreModule;
    }

    public static CoreModule_GetAuthenticationProviderFactory create(CoreModule coreModule) {
        return new CoreModule_GetAuthenticationProviderFactory(coreModule);
    }

    public static AuthenticationProvider getAuthenticationProvider(CoreModule coreModule) {
        return (AuthenticationProvider) cu2.f(coreModule.getAuthenticationProvider());
    }

    @Override // defpackage.ew2
    public AuthenticationProvider get() {
        return getAuthenticationProvider(this.module);
    }
}
