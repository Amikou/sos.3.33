package zendesk.core;

import android.content.Context;
import java.io.IOException;
import java.util.Locale;
import okhttp3.Interceptor;
import okhttp3.Request;
import okhttp3.Response;

/* JADX INFO: Access modifiers changed from: package-private */
/* loaded from: classes3.dex */
public class AcceptLanguageHeaderInterceptor implements Interceptor {
    private Context context;

    public AcceptLanguageHeaderInterceptor(Context context) {
        this.context = context;
    }

    @Override // okhttp3.Interceptor
    public Response intercept(Interceptor.Chain chain) throws IOException {
        Request request = chain.request();
        Locale currentLocale = DeviceInfo.getCurrentLocale(this.context);
        if (ru3.d(request.header(Constants.ACCEPT_LANGUAGE)) && currentLocale != null) {
            return chain.proceed(request.newBuilder().addHeader(Constants.ACCEPT_LANGUAGE, h12.d(currentLocale)).build());
        }
        return chain.proceed(request);
    }
}
