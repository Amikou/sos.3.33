package zendesk.core;

import retrofit2.o;

/* loaded from: classes3.dex */
public final class ZendeskProvidersModule_ProvideAccessServiceFactory implements y11<AccessService> {
    private final ew2<o> retrofitProvider;

    public ZendeskProvidersModule_ProvideAccessServiceFactory(ew2<o> ew2Var) {
        this.retrofitProvider = ew2Var;
    }

    public static ZendeskProvidersModule_ProvideAccessServiceFactory create(ew2<o> ew2Var) {
        return new ZendeskProvidersModule_ProvideAccessServiceFactory(ew2Var);
    }

    public static AccessService provideAccessService(o oVar) {
        return (AccessService) cu2.f(ZendeskProvidersModule.provideAccessService(oVar));
    }

    @Override // defpackage.ew2
    public AccessService get() {
        return provideAccessService(this.retrofitProvider.get());
    }
}
