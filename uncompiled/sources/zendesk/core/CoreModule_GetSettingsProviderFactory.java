package zendesk.core;

/* loaded from: classes3.dex */
public final class CoreModule_GetSettingsProviderFactory implements y11<SettingsProvider> {
    private final CoreModule module;

    public CoreModule_GetSettingsProviderFactory(CoreModule coreModule) {
        this.module = coreModule;
    }

    public static CoreModule_GetSettingsProviderFactory create(CoreModule coreModule) {
        return new CoreModule_GetSettingsProviderFactory(coreModule);
    }

    public static SettingsProvider getSettingsProvider(CoreModule coreModule) {
        return (SettingsProvider) cu2.f(coreModule.getSettingsProvider());
    }

    @Override // defpackage.ew2
    public SettingsProvider get() {
        return getSettingsProvider(this.module);
    }
}
