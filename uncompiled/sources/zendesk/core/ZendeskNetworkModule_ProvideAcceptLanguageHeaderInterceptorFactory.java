package zendesk.core;

import android.content.Context;

/* loaded from: classes3.dex */
public final class ZendeskNetworkModule_ProvideAcceptLanguageHeaderInterceptorFactory implements y11<AcceptLanguageHeaderInterceptor> {
    private final ew2<Context> contextProvider;

    public ZendeskNetworkModule_ProvideAcceptLanguageHeaderInterceptorFactory(ew2<Context> ew2Var) {
        this.contextProvider = ew2Var;
    }

    public static ZendeskNetworkModule_ProvideAcceptLanguageHeaderInterceptorFactory create(ew2<Context> ew2Var) {
        return new ZendeskNetworkModule_ProvideAcceptLanguageHeaderInterceptorFactory(ew2Var);
    }

    public static AcceptLanguageHeaderInterceptor provideAcceptLanguageHeaderInterceptor(Context context) {
        return (AcceptLanguageHeaderInterceptor) cu2.f(ZendeskNetworkModule.provideAcceptLanguageHeaderInterceptor(context));
    }

    @Override // defpackage.ew2
    public AcceptLanguageHeaderInterceptor get() {
        return provideAcceptLanguageHeaderInterceptor(this.contextProvider.get());
    }
}
