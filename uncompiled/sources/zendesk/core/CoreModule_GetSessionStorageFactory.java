package zendesk.core;

/* loaded from: classes3.dex */
public final class CoreModule_GetSessionStorageFactory implements y11<SessionStorage> {
    private final CoreModule module;

    public CoreModule_GetSessionStorageFactory(CoreModule coreModule) {
        this.module = coreModule;
    }

    public static CoreModule_GetSessionStorageFactory create(CoreModule coreModule) {
        return new CoreModule_GetSessionStorageFactory(coreModule);
    }

    public static SessionStorage getSessionStorage(CoreModule coreModule) {
        return (SessionStorage) cu2.f(coreModule.getSessionStorage());
    }

    @Override // defpackage.ew2
    public SessionStorage get() {
        return getSessionStorage(this.module);
    }
}
