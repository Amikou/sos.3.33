package zendesk.core;

/* loaded from: classes3.dex */
public final class ZendeskStorageModule_ProvideCoreSettingsStorageFactory implements y11<CoreSettingsStorage> {
    private final ew2<SettingsStorage> settingsStorageProvider;

    public ZendeskStorageModule_ProvideCoreSettingsStorageFactory(ew2<SettingsStorage> ew2Var) {
        this.settingsStorageProvider = ew2Var;
    }

    public static ZendeskStorageModule_ProvideCoreSettingsStorageFactory create(ew2<SettingsStorage> ew2Var) {
        return new ZendeskStorageModule_ProvideCoreSettingsStorageFactory(ew2Var);
    }

    public static CoreSettingsStorage provideCoreSettingsStorage(Object obj) {
        return (CoreSettingsStorage) cu2.f(ZendeskStorageModule.provideCoreSettingsStorage((SettingsStorage) obj));
    }

    @Override // defpackage.ew2
    public CoreSettingsStorage get() {
        return provideCoreSettingsStorage(this.settingsStorageProvider.get());
    }
}
