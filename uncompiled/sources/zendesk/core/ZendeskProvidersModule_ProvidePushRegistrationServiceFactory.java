package zendesk.core;

import retrofit2.o;

/* loaded from: classes3.dex */
public final class ZendeskProvidersModule_ProvidePushRegistrationServiceFactory implements y11<PushRegistrationService> {
    private final ew2<o> retrofitProvider;

    public ZendeskProvidersModule_ProvidePushRegistrationServiceFactory(ew2<o> ew2Var) {
        this.retrofitProvider = ew2Var;
    }

    public static ZendeskProvidersModule_ProvidePushRegistrationServiceFactory create(ew2<o> ew2Var) {
        return new ZendeskProvidersModule_ProvidePushRegistrationServiceFactory(ew2Var);
    }

    public static PushRegistrationService providePushRegistrationService(o oVar) {
        return (PushRegistrationService) cu2.f(ZendeskProvidersModule.providePushRegistrationService(oVar));
    }

    @Override // defpackage.ew2
    public PushRegistrationService get() {
        return providePushRegistrationService(this.retrofitProvider.get());
    }
}
