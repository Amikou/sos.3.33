package zendesk.core;

/* loaded from: classes3.dex */
public final class ZendeskApplicationModule_ProvideBase64SerializerFactory implements y11<Serializer> {
    private final ew2<Serializer> gsonSerializerProvider;
    private final ZendeskApplicationModule module;

    public ZendeskApplicationModule_ProvideBase64SerializerFactory(ZendeskApplicationModule zendeskApplicationModule, ew2<Serializer> ew2Var) {
        this.module = zendeskApplicationModule;
        this.gsonSerializerProvider = ew2Var;
    }

    public static ZendeskApplicationModule_ProvideBase64SerializerFactory create(ZendeskApplicationModule zendeskApplicationModule, ew2<Serializer> ew2Var) {
        return new ZendeskApplicationModule_ProvideBase64SerializerFactory(zendeskApplicationModule, ew2Var);
    }

    public static Serializer provideBase64Serializer(ZendeskApplicationModule zendeskApplicationModule, Object obj) {
        return (Serializer) cu2.f(zendeskApplicationModule.provideBase64Serializer((Serializer) obj));
    }

    @Override // defpackage.ew2
    public Serializer get() {
        return provideBase64Serializer(this.module, this.gsonSerializerProvider.get());
    }
}
