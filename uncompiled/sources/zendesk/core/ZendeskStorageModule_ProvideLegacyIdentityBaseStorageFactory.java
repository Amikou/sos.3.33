package zendesk.core;

import android.content.Context;

/* loaded from: classes3.dex */
public final class ZendeskStorageModule_ProvideLegacyIdentityBaseStorageFactory implements y11<SharedPreferencesStorage> {
    private final ew2<Context> contextProvider;
    private final ew2<Serializer> serializerProvider;

    public ZendeskStorageModule_ProvideLegacyIdentityBaseStorageFactory(ew2<Context> ew2Var, ew2<Serializer> ew2Var2) {
        this.contextProvider = ew2Var;
        this.serializerProvider = ew2Var2;
    }

    public static ZendeskStorageModule_ProvideLegacyIdentityBaseStorageFactory create(ew2<Context> ew2Var, ew2<Serializer> ew2Var2) {
        return new ZendeskStorageModule_ProvideLegacyIdentityBaseStorageFactory(ew2Var, ew2Var2);
    }

    public static SharedPreferencesStorage provideLegacyIdentityBaseStorage(Context context, Object obj) {
        return (SharedPreferencesStorage) cu2.f(ZendeskStorageModule.provideLegacyIdentityBaseStorage(context, (Serializer) obj));
    }

    @Override // defpackage.ew2
    public SharedPreferencesStorage get() {
        return provideLegacyIdentityBaseStorage(this.contextProvider.get(), this.serializerProvider.get());
    }
}
