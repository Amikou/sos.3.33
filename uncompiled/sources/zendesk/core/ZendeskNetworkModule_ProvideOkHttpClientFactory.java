package zendesk.core;

import okhttp3.Cache;
import okhttp3.OkHttpClient;

/* loaded from: classes3.dex */
public final class ZendeskNetworkModule_ProvideOkHttpClientFactory implements y11<OkHttpClient> {
    private final ew2<AcceptHeaderInterceptor> acceptHeaderInterceptorProvider;
    private final ew2<ZendeskAccessInterceptor> accessInterceptorProvider;
    private final ew2<ZendeskAuthHeaderInterceptor> authHeaderInterceptorProvider;
    private final ew2<Cache> cacheProvider;
    private final ZendeskNetworkModule module;
    private final ew2<OkHttpClient> okHttpClientProvider;
    private final ew2<ZendeskPushInterceptor> pushInterceptorProvider;
    private final ew2<ZendeskSettingsInterceptor> settingsInterceptorProvider;
    private final ew2<ZendeskUnauthorizedInterceptor> unauthorizedInterceptorProvider;

    public ZendeskNetworkModule_ProvideOkHttpClientFactory(ZendeskNetworkModule zendeskNetworkModule, ew2<OkHttpClient> ew2Var, ew2<ZendeskAccessInterceptor> ew2Var2, ew2<ZendeskUnauthorizedInterceptor> ew2Var3, ew2<ZendeskAuthHeaderInterceptor> ew2Var4, ew2<ZendeskSettingsInterceptor> ew2Var5, ew2<AcceptHeaderInterceptor> ew2Var6, ew2<ZendeskPushInterceptor> ew2Var7, ew2<Cache> ew2Var8) {
        this.module = zendeskNetworkModule;
        this.okHttpClientProvider = ew2Var;
        this.accessInterceptorProvider = ew2Var2;
        this.unauthorizedInterceptorProvider = ew2Var3;
        this.authHeaderInterceptorProvider = ew2Var4;
        this.settingsInterceptorProvider = ew2Var5;
        this.acceptHeaderInterceptorProvider = ew2Var6;
        this.pushInterceptorProvider = ew2Var7;
        this.cacheProvider = ew2Var8;
    }

    public static ZendeskNetworkModule_ProvideOkHttpClientFactory create(ZendeskNetworkModule zendeskNetworkModule, ew2<OkHttpClient> ew2Var, ew2<ZendeskAccessInterceptor> ew2Var2, ew2<ZendeskUnauthorizedInterceptor> ew2Var3, ew2<ZendeskAuthHeaderInterceptor> ew2Var4, ew2<ZendeskSettingsInterceptor> ew2Var5, ew2<AcceptHeaderInterceptor> ew2Var6, ew2<ZendeskPushInterceptor> ew2Var7, ew2<Cache> ew2Var8) {
        return new ZendeskNetworkModule_ProvideOkHttpClientFactory(zendeskNetworkModule, ew2Var, ew2Var2, ew2Var3, ew2Var4, ew2Var5, ew2Var6, ew2Var7, ew2Var8);
    }

    public static OkHttpClient provideOkHttpClient(ZendeskNetworkModule zendeskNetworkModule, OkHttpClient okHttpClient, Object obj, Object obj2, Object obj3, Object obj4, Object obj5, Object obj6, Cache cache) {
        return (OkHttpClient) cu2.f(zendeskNetworkModule.provideOkHttpClient(okHttpClient, (ZendeskAccessInterceptor) obj, (ZendeskUnauthorizedInterceptor) obj2, (ZendeskAuthHeaderInterceptor) obj3, (ZendeskSettingsInterceptor) obj4, (AcceptHeaderInterceptor) obj5, (ZendeskPushInterceptor) obj6, cache));
    }

    @Override // defpackage.ew2
    public OkHttpClient get() {
        return provideOkHttpClient(this.module, this.okHttpClientProvider.get(), this.accessInterceptorProvider.get(), this.unauthorizedInterceptorProvider.get(), this.authHeaderInterceptorProvider.get(), this.settingsInterceptorProvider.get(), this.acceptHeaderInterceptorProvider.get(), this.pushInterceptorProvider.get(), this.cacheProvider.get());
    }
}
