package zendesk.core;

import java.util.concurrent.ScheduledExecutorService;

/* loaded from: classes3.dex */
public final class CoreModule_GetScheduledExecutorServiceFactory implements y11<ScheduledExecutorService> {
    private final CoreModule module;

    public CoreModule_GetScheduledExecutorServiceFactory(CoreModule coreModule) {
        this.module = coreModule;
    }

    public static CoreModule_GetScheduledExecutorServiceFactory create(CoreModule coreModule) {
        return new CoreModule_GetScheduledExecutorServiceFactory(coreModule);
    }

    public static ScheduledExecutorService getScheduledExecutorService(CoreModule coreModule) {
        return (ScheduledExecutorService) cu2.f(coreModule.getScheduledExecutorService());
    }

    @Override // defpackage.ew2
    public ScheduledExecutorService get() {
        return getScheduledExecutorService(this.module);
    }
}
