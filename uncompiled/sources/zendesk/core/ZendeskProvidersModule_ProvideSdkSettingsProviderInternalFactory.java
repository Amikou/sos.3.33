package zendesk.core;

/* loaded from: classes3.dex */
public final class ZendeskProvidersModule_ProvideSdkSettingsProviderInternalFactory implements y11<SdkSettingsProviderInternal> {
    private final ew2<ZendeskSettingsProvider> sdkSettingsProvider;

    public ZendeskProvidersModule_ProvideSdkSettingsProviderInternalFactory(ew2<ZendeskSettingsProvider> ew2Var) {
        this.sdkSettingsProvider = ew2Var;
    }

    public static ZendeskProvidersModule_ProvideSdkSettingsProviderInternalFactory create(ew2<ZendeskSettingsProvider> ew2Var) {
        return new ZendeskProvidersModule_ProvideSdkSettingsProviderInternalFactory(ew2Var);
    }

    public static SdkSettingsProviderInternal provideSdkSettingsProviderInternal(Object obj) {
        return (SdkSettingsProviderInternal) cu2.f(ZendeskProvidersModule.provideSdkSettingsProviderInternal((ZendeskSettingsProvider) obj));
    }

    @Override // defpackage.ew2
    public SdkSettingsProviderInternal get() {
        return provideSdkSettingsProviderInternal(this.sdkSettingsProvider.get());
    }
}
