package zendesk.core;

/* loaded from: classes3.dex */
public final class ZendeskStorageModule_ProvideIdentityStorageFactory implements y11<IdentityStorage> {
    private final ew2<BaseStorage> baseStorageProvider;

    public ZendeskStorageModule_ProvideIdentityStorageFactory(ew2<BaseStorage> ew2Var) {
        this.baseStorageProvider = ew2Var;
    }

    public static ZendeskStorageModule_ProvideIdentityStorageFactory create(ew2<BaseStorage> ew2Var) {
        return new ZendeskStorageModule_ProvideIdentityStorageFactory(ew2Var);
    }

    public static IdentityStorage provideIdentityStorage(BaseStorage baseStorage) {
        return (IdentityStorage) cu2.f(ZendeskStorageModule.provideIdentityStorage(baseStorage));
    }

    @Override // defpackage.ew2
    public IdentityStorage get() {
        return provideIdentityStorage(this.baseStorageProvider.get());
    }
}
