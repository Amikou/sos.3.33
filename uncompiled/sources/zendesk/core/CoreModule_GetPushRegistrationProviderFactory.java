package zendesk.core;

/* loaded from: classes3.dex */
public final class CoreModule_GetPushRegistrationProviderFactory implements y11<PushRegistrationProvider> {
    private final CoreModule module;

    public CoreModule_GetPushRegistrationProviderFactory(CoreModule coreModule) {
        this.module = coreModule;
    }

    public static CoreModule_GetPushRegistrationProviderFactory create(CoreModule coreModule) {
        return new CoreModule_GetPushRegistrationProviderFactory(coreModule);
    }

    public static PushRegistrationProvider getPushRegistrationProvider(CoreModule coreModule) {
        return (PushRegistrationProvider) cu2.f(coreModule.getPushRegistrationProvider());
    }

    @Override // defpackage.ew2
    public PushRegistrationProvider get() {
        return getPushRegistrationProvider(this.module);
    }
}
