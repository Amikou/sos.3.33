package zendesk.core;

import java.util.Iterator;
import okhttp3.Interceptor;
import okhttp3.OkHttpClient;
import retrofit2.o;

/* loaded from: classes3.dex */
class ZendeskRestServiceProvider implements RestServiceProvider {
    private final OkHttpClient coreOkHttpClient;
    private final OkHttpClient mediaHttpClient;
    public final o retrofit;
    public final OkHttpClient standardOkHttpClient;

    public ZendeskRestServiceProvider(o oVar, OkHttpClient okHttpClient, OkHttpClient okHttpClient2, OkHttpClient okHttpClient3) {
        this.retrofit = oVar;
        this.mediaHttpClient = okHttpClient;
        this.standardOkHttpClient = okHttpClient2;
        this.coreOkHttpClient = okHttpClient3;
    }

    private OkHttpClient.Builder createNonAuthenticatedOkHttpClient() {
        OkHttpClient.Builder newBuilder = this.standardOkHttpClient.newBuilder();
        Iterator<Interceptor> it = newBuilder.interceptors().iterator();
        while (it.hasNext()) {
            if (it.next() instanceof ZendeskAuthHeaderInterceptor) {
                it.remove();
            }
        }
        return newBuilder;
    }

    @Override // zendesk.core.RestServiceProvider
    public <E> E createRestService(Class<E> cls, String str, String str2) {
        return (E) this.retrofit.d().f(this.standardOkHttpClient.newBuilder().addInterceptor(new UserAgentAndClientHeadersInterceptor(str, str2)).build()).d().b(cls);
    }

    @Override // zendesk.core.RestServiceProvider
    public <E> E createUnauthenticatedRestService(Class<E> cls, String str, String str2, CustomNetworkConfig customNetworkConfig) {
        OkHttpClient.Builder createNonAuthenticatedOkHttpClient = createNonAuthenticatedOkHttpClient();
        customNetworkConfig.configureOkHttpClient(createNonAuthenticatedOkHttpClient);
        createNonAuthenticatedOkHttpClient.addInterceptor(new UserAgentAndClientHeadersInterceptor(str, str2));
        o.b d = this.retrofit.d();
        customNetworkConfig.configureRetrofit(d);
        return (E) d.f(createNonAuthenticatedOkHttpClient.build()).d().b(cls);
    }

    @Override // zendesk.core.RestServiceProvider
    public OkHttpClient getCoreOkHttpClient() {
        return this.coreOkHttpClient;
    }

    @Override // zendesk.core.RestServiceProvider
    public OkHttpClient getMediaOkHttpClient() {
        return this.mediaHttpClient;
    }

    @Override // zendesk.core.RestServiceProvider
    public <E> E createRestService(Class<E> cls, String str, String str2, CustomNetworkConfig customNetworkConfig) {
        OkHttpClient.Builder newBuilder = this.standardOkHttpClient.newBuilder();
        customNetworkConfig.configureOkHttpClient(newBuilder);
        newBuilder.addInterceptor(new UserAgentAndClientHeadersInterceptor(str, str2));
        o.b d = this.retrofit.d();
        customNetworkConfig.configureRetrofit(d);
        return (E) d.f(newBuilder.build()).d().b(cls);
    }
}
