package zendesk.core;

import defpackage.w83;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/* loaded from: classes3.dex */
class ZendeskUserProvider implements UserProvider {
    private final UserService userService;
    private static final w83.b<UserResponse, User> USER_EXTRACTOR = new w83.b<UserResponse, User>() { // from class: zendesk.core.ZendeskUserProvider.6
        @Override // defpackage.w83.b
        public User extract(UserResponse userResponse) {
            return userResponse.getUser();
        }
    };
    private static final w83.b<UserFieldResponse, List<UserField>> FIELDS_EXTRACTOR = new w83.b<UserFieldResponse, List<UserField>>() { // from class: zendesk.core.ZendeskUserProvider.7
        @Override // defpackage.w83.b
        public List<UserField> extract(UserFieldResponse userFieldResponse) {
            return userFieldResponse.getUserFields();
        }
    };
    private static final w83.b<UserResponse, Map<String, String>> FIELDS_MAP_EXTRACTOR = new w83.b<UserResponse, Map<String, String>>() { // from class: zendesk.core.ZendeskUserProvider.8
        @Override // defpackage.w83.b
        public Map<String, String> extract(UserResponse userResponse) {
            if (userResponse != null && userResponse.getUser() != null) {
                return userResponse.getUser().getUserFields();
            }
            return l10.d(new HashMap());
        }
    };
    private static final w83.b<UserResponse, List<String>> TAGS_EXTRACTOR = new w83.b<UserResponse, List<String>>() { // from class: zendesk.core.ZendeskUserProvider.9
        @Override // defpackage.w83.b
        public List<String> extract(UserResponse userResponse) {
            if (userResponse != null && userResponse.getUser() != null) {
                return userResponse.getUser().getTags();
            }
            return l10.c(new ArrayList());
        }
    };

    public ZendeskUserProvider(UserService userService) {
        this.userService = userService;
    }

    @Override // zendesk.core.UserProvider
    public void addTags(List<String> list, final rs4<List<String>> rs4Var) {
        this.userService.addTags(new UserTagRequest(l10.e(list))).n(new w83(new PassThroughErrorZendeskCallback<List<String>>(rs4Var) { // from class: zendesk.core.ZendeskUserProvider.1
            @Override // zendesk.core.PassThroughErrorZendeskCallback, defpackage.rs4
            public void onSuccess(List<String> list2) {
                rs4 rs4Var2 = rs4Var;
                if (rs4Var2 != null) {
                    rs4Var2.onSuccess(list2);
                }
            }
        }, TAGS_EXTRACTOR));
    }

    @Override // zendesk.core.UserProvider
    public void deleteTags(List<String> list, final rs4<List<String>> rs4Var) {
        this.userService.deleteTags(ru3.f(l10.e(list))).n(new w83(new PassThroughErrorZendeskCallback<List<String>>(rs4Var) { // from class: zendesk.core.ZendeskUserProvider.2
            @Override // zendesk.core.PassThroughErrorZendeskCallback, defpackage.rs4
            public void onSuccess(List<String> list2) {
                rs4 rs4Var2 = rs4Var;
                if (rs4Var2 != null) {
                    rs4Var2.onSuccess(list2);
                }
            }
        }, TAGS_EXTRACTOR));
    }

    @Override // zendesk.core.UserProvider
    public void getUser(final rs4<User> rs4Var) {
        this.userService.getUser().n(new w83(new PassThroughErrorZendeskCallback<User>(rs4Var) { // from class: zendesk.core.ZendeskUserProvider.5
            @Override // zendesk.core.PassThroughErrorZendeskCallback, defpackage.rs4
            public void onSuccess(User user) {
                rs4 rs4Var2 = rs4Var;
                if (rs4Var2 != null) {
                    rs4Var2.onSuccess(user);
                }
            }
        }, USER_EXTRACTOR));
    }

    @Override // zendesk.core.UserProvider
    public void getUserFields(final rs4<List<UserField>> rs4Var) {
        this.userService.getUserFields().n(new w83(new PassThroughErrorZendeskCallback<List<UserField>>(rs4Var) { // from class: zendesk.core.ZendeskUserProvider.3
            @Override // zendesk.core.PassThroughErrorZendeskCallback, defpackage.rs4
            public void onSuccess(List<UserField> list) {
                rs4 rs4Var2 = rs4Var;
                if (rs4Var2 != null) {
                    rs4Var2.onSuccess(list);
                }
            }
        }, FIELDS_EXTRACTOR));
    }

    @Override // zendesk.core.UserProvider
    public void setUserFields(Map<String, String> map, final rs4<Map<String, String>> rs4Var) {
        this.userService.setUserFields(new UserFieldRequest(map)).n(new w83(new PassThroughErrorZendeskCallback<Map<String, String>>(rs4Var) { // from class: zendesk.core.ZendeskUserProvider.4
            @Override // zendesk.core.PassThroughErrorZendeskCallback, defpackage.rs4
            public void onSuccess(Map<String, String> map2) {
                rs4 rs4Var2 = rs4Var;
                if (rs4Var2 != null) {
                    rs4Var2.onSuccess(map2);
                }
            }
        }, FIELDS_MAP_EXTRACTOR));
    }
}
