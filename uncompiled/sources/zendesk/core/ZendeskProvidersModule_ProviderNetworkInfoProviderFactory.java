package zendesk.core;

import android.net.ConnectivityManager;

/* loaded from: classes3.dex */
public final class ZendeskProvidersModule_ProviderNetworkInfoProviderFactory implements y11<NetworkInfoProvider> {
    private final ew2<ConnectivityManager> connectivityManagerProvider;

    public ZendeskProvidersModule_ProviderNetworkInfoProviderFactory(ew2<ConnectivityManager> ew2Var) {
        this.connectivityManagerProvider = ew2Var;
    }

    public static ZendeskProvidersModule_ProviderNetworkInfoProviderFactory create(ew2<ConnectivityManager> ew2Var) {
        return new ZendeskProvidersModule_ProviderNetworkInfoProviderFactory(ew2Var);
    }

    public static NetworkInfoProvider providerNetworkInfoProvider(ConnectivityManager connectivityManager) {
        return (NetworkInfoProvider) cu2.f(ZendeskProvidersModule.providerNetworkInfoProvider(connectivityManager));
    }

    @Override // defpackage.ew2
    public NetworkInfoProvider get() {
        return providerNetworkInfoProvider(this.connectivityManagerProvider.get());
    }
}
