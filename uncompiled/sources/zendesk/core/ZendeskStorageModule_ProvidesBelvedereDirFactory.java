package zendesk.core;

import android.content.Context;
import java.io.File;

/* loaded from: classes3.dex */
public final class ZendeskStorageModule_ProvidesBelvedereDirFactory implements y11<File> {
    private final ew2<Context> contextProvider;

    public ZendeskStorageModule_ProvidesBelvedereDirFactory(ew2<Context> ew2Var) {
        this.contextProvider = ew2Var;
    }

    public static ZendeskStorageModule_ProvidesBelvedereDirFactory create(ew2<Context> ew2Var) {
        return new ZendeskStorageModule_ProvidesBelvedereDirFactory(ew2Var);
    }

    public static File providesBelvedereDir(Context context) {
        return (File) cu2.f(ZendeskStorageModule.providesBelvedereDir(context));
    }

    @Override // defpackage.ew2
    public File get() {
        return providesBelvedereDir(this.contextProvider.get());
    }
}
