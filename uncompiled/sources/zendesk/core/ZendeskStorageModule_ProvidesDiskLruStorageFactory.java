package zendesk.core;

import java.io.File;

/* loaded from: classes3.dex */
public final class ZendeskStorageModule_ProvidesDiskLruStorageFactory implements y11<BaseStorage> {
    private final ew2<File> fileProvider;
    private final ew2<Serializer> serializerProvider;

    public ZendeskStorageModule_ProvidesDiskLruStorageFactory(ew2<File> ew2Var, ew2<Serializer> ew2Var2) {
        this.fileProvider = ew2Var;
        this.serializerProvider = ew2Var2;
    }

    public static ZendeskStorageModule_ProvidesDiskLruStorageFactory create(ew2<File> ew2Var, ew2<Serializer> ew2Var2) {
        return new ZendeskStorageModule_ProvidesDiskLruStorageFactory(ew2Var, ew2Var2);
    }

    public static BaseStorage providesDiskLruStorage(File file, Object obj) {
        return (BaseStorage) cu2.f(ZendeskStorageModule.providesDiskLruStorage(file, (Serializer) obj));
    }

    @Override // defpackage.ew2
    public BaseStorage get() {
        return providesDiskLruStorage(this.fileProvider.get(), this.serializerProvider.get());
    }
}
