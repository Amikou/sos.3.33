package zendesk.belvedere;

import android.view.View;
import java.util.List;
import zendesk.belvedere.c;

/* compiled from: ImageStreamMvp.java */
/* loaded from: classes3.dex */
public interface f {
    void a(int i);

    boolean b();

    void c(boolean z);

    void d(int i);

    void e(List<MediaResult> list, List<MediaResult> list2, boolean z, boolean z2, c.b bVar);

    void f(View.OnClickListener onClickListener);

    void g(MediaIntent mediaIntent, b bVar);

    void h(int i);

    void i(View.OnClickListener onClickListener);
}
