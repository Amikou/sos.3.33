package zendesk.belvedere;

import android.content.Context;
import android.os.Build;
import android.text.TextUtils;
import androidx.fragment.app.Fragment;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/* compiled from: PermissionManager.java */
/* loaded from: classes3.dex */
public class j {
    public c a = null;

    /* compiled from: PermissionManager.java */
    /* loaded from: classes3.dex */
    public class a implements c {
        public final /* synthetic */ Context a;
        public final /* synthetic */ List b;
        public final /* synthetic */ d c;

        public a(Context context, List list, d dVar) {
            this.a = context;
            this.b = list;
            this.c = dVar;
        }

        @Override // zendesk.belvedere.j.c
        public void a(Map<String, Boolean> map) {
            List<MediaIntent> f = j.this.f(this.a, this.b);
            if (j.this.e(this.a)) {
                this.c.a(f);
            } else {
                this.c.b();
            }
        }
    }

    /* compiled from: PermissionManager.java */
    /* loaded from: classes3.dex */
    public class b implements c {
        public final /* synthetic */ c a;

        public b(c cVar) {
            this.a = cVar;
        }

        @Override // zendesk.belvedere.j.c
        public void a(Map<String, Boolean> map) {
            this.a.a(map);
            j.this.l(null);
        }
    }

    /* compiled from: PermissionManager.java */
    /* loaded from: classes3.dex */
    public interface c {
        void a(Map<String, Boolean> map);
    }

    /* compiled from: PermissionManager.java */
    /* loaded from: classes3.dex */
    public interface d {
        void a(List<MediaIntent> list);

        void b();
    }

    public final void d(Fragment fragment, List<String> list, c cVar) {
        l(new b(cVar));
        fragment.requestPermissions((String[]) list.toArray(new String[list.size()]), 9842);
    }

    public final boolean e(Context context) {
        return (Build.VERSION.SDK_INT < 19) || j(context, "android.permission.READ_EXTERNAL_STORAGE");
    }

    public final List<MediaIntent> f(Context context, List<MediaIntent> list) {
        ArrayList arrayList = new ArrayList();
        for (MediaIntent mediaIntent : list) {
            if (mediaIntent.d()) {
                if (TextUtils.isEmpty(mediaIntent.b())) {
                    arrayList.add(mediaIntent);
                } else if (j(context, mediaIntent.b())) {
                    arrayList.add(mediaIntent);
                }
            }
        }
        return arrayList;
    }

    public final List<String> g(Context context) {
        ArrayList arrayList = new ArrayList();
        if (!e(context)) {
            arrayList.add("android.permission.READ_EXTERNAL_STORAGE");
        }
        return arrayList;
    }

    public final List<String> h(List<MediaIntent> list) {
        ArrayList arrayList = new ArrayList();
        for (MediaIntent mediaIntent : list) {
            if (!TextUtils.isEmpty(mediaIntent.b()) && mediaIntent.d()) {
                arrayList.add(mediaIntent.b());
            }
        }
        return arrayList;
    }

    public void i(Fragment fragment, List<MediaIntent> list, d dVar) {
        Context context = fragment.getContext();
        ArrayList arrayList = new ArrayList();
        arrayList.addAll(g(context));
        arrayList.addAll(h(list));
        if (e(context) && arrayList.isEmpty()) {
            dVar.a(f(context, list));
        } else if (!e(context) && arrayList.isEmpty()) {
            dVar.b();
        } else {
            d(fragment, arrayList, new a(context, list, dVar));
        }
    }

    public final boolean j(Context context, String str) {
        return lq2.b(context, str);
    }

    public boolean k(int i, String[] strArr, int[] iArr) {
        if (i == 9842) {
            HashMap hashMap = new HashMap();
            int length = strArr.length;
            for (int i2 = 0; i2 < length; i2++) {
                if (iArr[i2] == 0) {
                    hashMap.put(strArr[i2], Boolean.TRUE);
                } else if (iArr[i2] == -1) {
                    hashMap.put(strArr[i2], Boolean.FALSE);
                }
            }
            c cVar = this.a;
            if (cVar != null) {
                cVar.a(hashMap);
                return true;
            }
            return true;
        }
        return false;
    }

    public final void l(c cVar) {
        this.a = cVar;
    }
}
