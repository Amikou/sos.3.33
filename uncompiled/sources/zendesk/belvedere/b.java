package zendesk.belvedere;

import android.content.Intent;
import android.os.Bundle;
import android.widget.Toast;
import androidx.fragment.app.Fragment;
import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.List;
import zendesk.belvedere.BelvedereUi;
import zendesk.belvedere.j;

/* compiled from: ImageStream.java */
/* loaded from: classes3.dex */
public class b extends Fragment {
    public WeakReference<KeyboardHelper> a = new WeakReference<>(null);
    public List<WeakReference<InterfaceC0296b>> f0 = new ArrayList();
    public List<WeakReference<d>> g0 = new ArrayList();
    public List<WeakReference<c>> h0 = new ArrayList();
    public h i0 = null;
    public BelvedereUi.UiConfig j0 = null;
    public boolean k0 = false;
    public j l0;
    public uu<List<MediaResult>> m0;

    /* compiled from: ImageStream.java */
    /* loaded from: classes3.dex */
    public class a extends uu<List<MediaResult>> {
        public a() {
        }

        @Override // defpackage.uu
        public void success(List<MediaResult> list) {
            ArrayList arrayList = new ArrayList(list.size());
            for (MediaResult mediaResult : list) {
                if (mediaResult.k() <= b.this.j0.c() || b.this.j0.c() == -1) {
                    arrayList.add(mediaResult);
                }
            }
            if (arrayList.size() != list.size()) {
                Toast.makeText(b.this.getContext(), k13.belvedere_image_stream_file_too_large, 0).show();
            }
            b.this.n(arrayList);
        }
    }

    /* compiled from: ImageStream.java */
    /* renamed from: zendesk.belvedere.b$b  reason: collision with other inner class name */
    /* loaded from: classes3.dex */
    public interface InterfaceC0296b {
        void onDismissed();

        void onMediaDeselected(List<MediaResult> list);

        void onMediaSelected(List<MediaResult> list);

        void onVisible();
    }

    /* compiled from: ImageStream.java */
    /* loaded from: classes3.dex */
    public interface c {
        void onScroll(int i, int i2, float f);
    }

    /* compiled from: ImageStream.java */
    /* loaded from: classes3.dex */
    public interface d {
        void a(List<MediaResult> list);
    }

    public void f(InterfaceC0296b interfaceC0296b) {
        this.f0.add(new WeakReference<>(interfaceC0296b));
    }

    public void g(c cVar) {
        this.h0.add(new WeakReference<>(cVar));
    }

    public void h() {
        if (k()) {
            this.i0.dismiss();
        }
    }

    public KeyboardHelper i() {
        return this.a.get();
    }

    public void j(List<MediaIntent> list, j.d dVar) {
        this.l0.i(this, list, dVar);
    }

    public boolean k() {
        return this.i0 != null;
    }

    public void l() {
        this.m0 = null;
        for (WeakReference<InterfaceC0296b> weakReference : this.f0) {
            InterfaceC0296b interfaceC0296b = weakReference.get();
            if (interfaceC0296b != null) {
                interfaceC0296b.onDismissed();
            }
        }
    }

    public void m(List<MediaResult> list) {
        for (WeakReference<InterfaceC0296b> weakReference : this.f0) {
            InterfaceC0296b interfaceC0296b = weakReference.get();
            if (interfaceC0296b != null) {
                interfaceC0296b.onMediaDeselected(list);
            }
        }
    }

    public void n(List<MediaResult> list) {
        for (WeakReference<InterfaceC0296b> weakReference : this.f0) {
            InterfaceC0296b interfaceC0296b = weakReference.get();
            if (interfaceC0296b != null) {
                interfaceC0296b.onMediaSelected(list);
            }
        }
    }

    public void o(List<MediaResult> list) {
        for (WeakReference<d> weakReference : this.g0) {
            d dVar = weakReference.get();
            if (dVar != null) {
                dVar.a(list);
            }
        }
    }

    @Override // androidx.fragment.app.Fragment
    public void onActivityResult(int i, int i2, Intent intent) {
        super.onActivityResult(i, i2, intent);
        this.m0 = new a();
        zendesk.belvedere.a.c(requireContext()).e(i, i2, intent, this.m0, false);
    }

    @Override // androidx.fragment.app.Fragment
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        setRetainInstance(true);
        this.l0 = new j();
    }

    @Override // androidx.fragment.app.Fragment
    public void onPause() {
        super.onPause();
        h hVar = this.i0;
        if (hVar != null) {
            hVar.dismiss();
            this.k0 = true;
            return;
        }
        this.k0 = false;
    }

    @Override // androidx.fragment.app.Fragment
    public void onRequestPermissionsResult(int i, String[] strArr, int[] iArr) {
        if (this.l0.k(i, strArr, iArr)) {
            return;
        }
        super.onRequestPermissionsResult(i, strArr, iArr);
    }

    public void p(int i, int i2, float f) {
        for (WeakReference<c> weakReference : this.h0) {
            c cVar = weakReference.get();
            if (cVar != null) {
                cVar.onScroll(i, i2, f);
            }
        }
    }

    public void q() {
        for (WeakReference<InterfaceC0296b> weakReference : this.f0) {
            InterfaceC0296b interfaceC0296b = weakReference.get();
            if (interfaceC0296b != null) {
                interfaceC0296b.onVisible();
            }
        }
    }

    public void r(h hVar, BelvedereUi.UiConfig uiConfig) {
        this.i0 = hVar;
        if (uiConfig != null) {
            this.j0 = uiConfig;
        }
    }

    public void s(KeyboardHelper keyboardHelper) {
        this.a = new WeakReference<>(keyboardHelper);
    }

    public boolean t() {
        return this.k0;
    }
}
