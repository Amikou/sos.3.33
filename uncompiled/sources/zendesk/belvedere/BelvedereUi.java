package zendesk.belvedere;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Parcel;
import android.os.Parcelable;
import android.view.View;
import android.view.ViewGroup;
import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentActivity;
import androidx.fragment.app.FragmentManager;
import java.util.ArrayList;
import java.util.List;
import zendesk.belvedere.j;

/* loaded from: classes3.dex */
public class BelvedereUi {

    /* loaded from: classes3.dex */
    public static class b {
        public final Context a;
        public final List<MediaIntent> b;
        public List<MediaResult> c;
        public List<MediaResult> d;
        public List<Integer> e;
        public long f;
        public boolean g;

        /* loaded from: classes3.dex */
        public class a implements j.d {
            public final /* synthetic */ zendesk.belvedere.b a;

            /* renamed from: zendesk.belvedere.BelvedereUi$b$a$a  reason: collision with other inner class name */
            /* loaded from: classes3.dex */
            public class RunnableC0293a implements Runnable {
                public final /* synthetic */ List a;
                public final /* synthetic */ Activity f0;
                public final /* synthetic */ ViewGroup g0;

                public RunnableC0293a(List list, Activity activity, ViewGroup viewGroup) {
                    this.a = list;
                    this.f0 = activity;
                    this.g0 = viewGroup;
                }

                @Override // java.lang.Runnable
                public void run() {
                    UiConfig uiConfig = new UiConfig(this.a, b.this.c, b.this.d, true, b.this.e, b.this.f, b.this.g);
                    a.this.a.r(h.v(this.f0, this.g0, a.this.a, uiConfig), uiConfig);
                }
            }

            /* renamed from: zendesk.belvedere.BelvedereUi$b$a$b  reason: collision with other inner class name */
            /* loaded from: classes3.dex */
            public class View$OnClickListenerC0294b implements View.OnClickListener {
                public final /* synthetic */ Activity a;

                public View$OnClickListenerC0294b(a aVar, Activity activity) {
                    this.a = activity;
                }

                @Override // android.view.View.OnClickListener
                public void onClick(View view) {
                    Intent intent = new Intent();
                    intent.setAction("android.settings.APPLICATION_DETAILS_SETTINGS");
                    intent.setData(Uri.fromParts("package", this.a.getPackageName(), null));
                    this.a.startActivity(intent);
                }
            }

            public a(zendesk.belvedere.b bVar) {
                this.a = bVar;
            }

            @Override // zendesk.belvedere.j.d
            public void a(List<MediaIntent> list) {
                FragmentActivity activity = this.a.getActivity();
                if (activity == null || activity.isChangingConfigurations()) {
                    return;
                }
                ViewGroup viewGroup = (ViewGroup) activity.getWindow().getDecorView();
                viewGroup.post(new RunnableC0293a(list, activity, viewGroup));
            }

            @Override // zendesk.belvedere.j.d
            public void b() {
                FragmentActivity activity = this.a.getActivity();
                if (activity != null) {
                    rg4.e((ViewGroup) activity.findViewById(16908290), activity.getString(k13.belvedere_permissions_rationale), 5000L, activity.getString(k13.belvedere_navigate_to_settings), new View$OnClickListenerC0294b(this, activity));
                }
            }
        }

        public void f(AppCompatActivity appCompatActivity) {
            zendesk.belvedere.b b = BelvedereUi.b(appCompatActivity);
            b.j(this.b, new a(b));
        }

        public b g() {
            this.b.add(zendesk.belvedere.a.c(this.a).a().a());
            return this;
        }

        public b h(String str, boolean z) {
            this.b.add(zendesk.belvedere.a.c(this.a).b().a(z).c(str).b());
            return this;
        }

        public b i(List<MediaResult> list) {
            this.d = new ArrayList(list);
            return this;
        }

        public b j(boolean z) {
            this.g = z;
            return this;
        }

        public b k(long j) {
            this.f = j;
            return this;
        }

        public b l(List<MediaResult> list) {
            this.c = new ArrayList(list);
            return this;
        }

        public b m(int... iArr) {
            ArrayList arrayList = new ArrayList(iArr.length);
            for (int i : iArr) {
                arrayList.add(Integer.valueOf(i));
            }
            this.e = arrayList;
            return this;
        }

        public b(Context context) {
            this.b = new ArrayList();
            this.c = new ArrayList();
            this.d = new ArrayList();
            this.e = new ArrayList();
            this.f = -1L;
            this.g = false;
            this.a = context;
        }
    }

    public static b a(Context context) {
        return new b(context);
    }

    public static zendesk.belvedere.b b(AppCompatActivity appCompatActivity) {
        zendesk.belvedere.b bVar;
        FragmentManager supportFragmentManager = appCompatActivity.getSupportFragmentManager();
        Fragment k0 = supportFragmentManager.k0("belvedere_image_stream");
        if (k0 instanceof zendesk.belvedere.b) {
            bVar = (zendesk.belvedere.b) k0;
        } else {
            bVar = new zendesk.belvedere.b();
            supportFragmentManager.n().e(bVar, "belvedere_image_stream").l();
        }
        bVar.s(KeyboardHelper.l(appCompatActivity));
        return bVar;
    }

    /* loaded from: classes3.dex */
    public static class UiConfig implements Parcelable {
        public static final Parcelable.Creator<UiConfig> CREATOR = new a();
        public final List<MediaIntent> a;
        public final List<MediaResult> f0;
        public final List<MediaResult> g0;
        public final List<Integer> h0;
        public final boolean i0;
        public final long j0;
        public final boolean k0;

        /* loaded from: classes3.dex */
        public static class a implements Parcelable.Creator<UiConfig> {
            @Override // android.os.Parcelable.Creator
            /* renamed from: a */
            public UiConfig createFromParcel(Parcel parcel) {
                return new UiConfig(parcel);
            }

            @Override // android.os.Parcelable.Creator
            /* renamed from: b */
            public UiConfig[] newArray(int i) {
                return new UiConfig[i];
            }
        }

        public UiConfig() {
            this.a = new ArrayList();
            this.f0 = new ArrayList();
            this.g0 = new ArrayList();
            this.h0 = new ArrayList();
            this.i0 = true;
            this.j0 = -1L;
            this.k0 = false;
        }

        public List<MediaResult> a() {
            return this.g0;
        }

        public List<MediaIntent> b() {
            return this.a;
        }

        public long c() {
            return this.j0;
        }

        public List<MediaResult> d() {
            return this.f0;
        }

        @Override // android.os.Parcelable
        public int describeContents() {
            return 0;
        }

        public List<Integer> e() {
            return this.h0;
        }

        public boolean f() {
            return this.k0;
        }

        @Override // android.os.Parcelable
        public void writeToParcel(Parcel parcel, int i) {
            parcel.writeTypedList(this.a);
            parcel.writeTypedList(this.f0);
            parcel.writeTypedList(this.g0);
            parcel.writeList(this.h0);
            parcel.writeInt(this.i0 ? 1 : 0);
            parcel.writeLong(this.j0);
            parcel.writeInt(this.k0 ? 1 : 0);
        }

        public UiConfig(List<MediaIntent> list, List<MediaResult> list2, List<MediaResult> list3, boolean z, List<Integer> list4, long j, boolean z2) {
            this.a = list;
            this.f0 = list2;
            this.g0 = list3;
            this.i0 = z;
            this.h0 = list4;
            this.j0 = j;
            this.k0 = z2;
        }

        public UiConfig(Parcel parcel) {
            this.a = parcel.createTypedArrayList(MediaIntent.CREATOR);
            Parcelable.Creator<MediaResult> creator = MediaResult.CREATOR;
            this.f0 = parcel.createTypedArrayList(creator);
            this.g0 = parcel.createTypedArrayList(creator);
            ArrayList arrayList = new ArrayList();
            this.h0 = arrayList;
            parcel.readList(arrayList, Integer.class.getClassLoader());
            this.i0 = parcel.readInt() == 1;
            this.j0 = parcel.readLong();
            this.k0 = parcel.readInt() == 1;
        }
    }
}
