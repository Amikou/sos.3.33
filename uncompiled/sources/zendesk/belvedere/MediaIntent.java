package zendesk.belvedere;

import android.content.Intent;
import android.os.Parcel;
import android.os.Parcelable;
import androidx.fragment.app.Fragment;
import java.util.ArrayList;
import java.util.List;

/* loaded from: classes3.dex */
public class MediaIntent implements Parcelable {
    public static final Parcelable.Creator<MediaIntent> CREATOR = new a();
    public final boolean a;
    public final int f0;
    public final Intent g0;
    public final String h0;
    public final int i0;

    /* loaded from: classes3.dex */
    public static class a implements Parcelable.Creator<MediaIntent> {
        @Override // android.os.Parcelable.Creator
        /* renamed from: a */
        public MediaIntent createFromParcel(Parcel parcel) {
            return new MediaIntent(parcel);
        }

        @Override // android.os.Parcelable.Creator
        /* renamed from: b */
        public MediaIntent[] newArray(int i) {
            return new MediaIntent[i];
        }
    }

    /* loaded from: classes3.dex */
    public static class b {
        public final s62 a;
        public final as1 b;
        public final int c;

        public b(int i, s62 s62Var, as1 as1Var) {
            this.c = i;
            this.a = s62Var;
            this.b = as1Var;
        }

        public MediaIntent a() {
            jp2<MediaIntent, MediaResult> c = this.a.c(this.c);
            MediaIntent mediaIntent = c.a;
            MediaResult mediaResult = c.b;
            if (mediaIntent.d()) {
                this.b.e(this.c, mediaResult);
            }
            return mediaIntent;
        }
    }

    /* loaded from: classes3.dex */
    public static class c {
        public final s62 a;
        public final int b;
        public String c = "*/*";
        public List<String> d = new ArrayList();
        public boolean e = false;

        public c(int i, s62 s62Var) {
            this.a = s62Var;
            this.b = i;
        }

        public c a(boolean z) {
            this.e = z;
            return this;
        }

        public MediaIntent b() {
            return this.a.f(this.b, this.c, this.e, this.d);
        }

        public c c(String str) {
            this.c = str;
            this.d = new ArrayList();
            return this;
        }
    }

    public MediaIntent(int i, Intent intent, String str, boolean z, int i2) {
        this.f0 = i;
        this.g0 = intent;
        this.h0 = str;
        this.a = z;
        this.i0 = i2;
    }

    public static MediaIntent e() {
        return new MediaIntent(-1, null, null, false, -1);
    }

    public Intent a() {
        return this.g0;
    }

    public String b() {
        return this.h0;
    }

    public int c() {
        return this.i0;
    }

    public boolean d() {
        return this.a;
    }

    @Override // android.os.Parcelable
    public int describeContents() {
        return 0;
    }

    public void f(Fragment fragment) {
        fragment.startActivityForResult(this.g0, this.f0);
    }

    @Override // android.os.Parcelable
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeInt(this.f0);
        parcel.writeParcelable(this.g0, i);
        parcel.writeString(this.h0);
        parcel.writeBooleanArray(new boolean[]{this.a});
        parcel.writeInt(this.i0);
    }

    public MediaIntent(Parcel parcel) {
        this.f0 = parcel.readInt();
        this.g0 = (Intent) parcel.readParcelable(MediaIntent.class.getClassLoader());
        this.h0 = parcel.readString();
        boolean[] zArr = new boolean[1];
        parcel.readBooleanArray(zArr);
        this.a = zArr[0];
        this.i0 = parcel.readInt();
    }
}
