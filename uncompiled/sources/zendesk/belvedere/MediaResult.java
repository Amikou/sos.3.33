package zendesk.belvedere;

import android.net.Uri;
import android.os.Parcel;
import android.os.Parcelable;
import java.io.File;

/* loaded from: classes3.dex */
public class MediaResult implements Parcelable, Comparable<MediaResult> {
    public static final Parcelable.Creator<MediaResult> CREATOR = new a();
    public final File a;
    public final Uri f0;
    public final Uri g0;
    public final String h0;
    public final String i0;
    public final long j0;
    public final long k0;
    public final long l0;

    /* loaded from: classes3.dex */
    public static class a implements Parcelable.Creator<MediaResult> {
        @Override // android.os.Parcelable.Creator
        /* renamed from: a */
        public MediaResult createFromParcel(Parcel parcel) {
            return new MediaResult(parcel, null);
        }

        @Override // android.os.Parcelable.Creator
        /* renamed from: b */
        public MediaResult[] newArray(int i) {
            return new MediaResult[i];
        }
    }

    public /* synthetic */ MediaResult(Parcel parcel, a aVar) {
        this(parcel);
    }

    public static MediaResult d() {
        return new MediaResult(null, null, null, null, null, -1L, -1L, -1L);
    }

    @Override // java.lang.Comparable
    /* renamed from: a */
    public int compareTo(MediaResult mediaResult) {
        return this.g0.compareTo(mediaResult.j());
    }

    @Override // android.os.Parcelable
    public int describeContents() {
        return 0;
    }

    public File e() {
        return this.a;
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj != null && getClass() == obj.getClass()) {
            MediaResult mediaResult = (MediaResult) obj;
            if (this.j0 == mediaResult.j0 && this.k0 == mediaResult.k0 && this.l0 == mediaResult.l0) {
                File file = this.a;
                if (file == null ? mediaResult.a == null : file.equals(mediaResult.a)) {
                    Uri uri = this.f0;
                    if (uri == null ? mediaResult.f0 == null : uri.equals(mediaResult.f0)) {
                        Uri uri2 = this.g0;
                        if (uri2 == null ? mediaResult.g0 == null : uri2.equals(mediaResult.g0)) {
                            String str = this.h0;
                            if (str == null ? mediaResult.h0 == null : str.equals(mediaResult.h0)) {
                                String str2 = this.i0;
                                String str3 = mediaResult.i0;
                                return str2 != null ? str2.equals(str3) : str3 == null;
                            }
                            return false;
                        }
                        return false;
                    }
                    return false;
                }
                return false;
            }
        }
        return false;
    }

    public long f() {
        return this.l0;
    }

    public String g() {
        return this.i0;
    }

    public String h() {
        return this.h0;
    }

    public int hashCode() {
        File file = this.a;
        int hashCode = (file != null ? file.hashCode() : 0) * 31;
        Uri uri = this.f0;
        int hashCode2 = (hashCode + (uri != null ? uri.hashCode() : 0)) * 31;
        Uri uri2 = this.g0;
        int hashCode3 = (hashCode2 + (uri2 != null ? uri2.hashCode() : 0)) * 31;
        String str = this.h0;
        int hashCode4 = (hashCode3 + (str != null ? str.hashCode() : 0)) * 31;
        String str2 = this.i0;
        int hashCode5 = str2 != null ? str2.hashCode() : 0;
        long j = this.j0;
        long j2 = this.k0;
        long j3 = this.l0;
        return ((((((hashCode4 + hashCode5) * 31) + ((int) (j ^ (j >>> 32)))) * 31) + ((int) (j2 ^ (j2 >>> 32)))) * 31) + ((int) (j3 ^ (j3 >>> 32)));
    }

    public Uri j() {
        return this.g0;
    }

    public long k() {
        return this.j0;
    }

    public Uri l() {
        return this.f0;
    }

    public long o() {
        return this.k0;
    }

    @Override // android.os.Parcelable
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeSerializable(this.a);
        parcel.writeParcelable(this.f0, i);
        parcel.writeString(this.h0);
        parcel.writeString(this.i0);
        parcel.writeParcelable(this.g0, i);
        parcel.writeLong(this.j0);
        parcel.writeLong(this.k0);
        parcel.writeLong(this.l0);
    }

    public MediaResult(File file, Uri uri, Uri uri2, String str, String str2, long j, long j2, long j3) {
        this.a = file;
        this.f0 = uri;
        this.g0 = uri2;
        this.i0 = str2;
        this.h0 = str;
        this.j0 = j;
        this.k0 = j2;
        this.l0 = j3;
    }

    public MediaResult(Parcel parcel) {
        this.a = (File) parcel.readSerializable();
        this.f0 = (Uri) parcel.readParcelable(MediaResult.class.getClassLoader());
        this.h0 = parcel.readString();
        this.i0 = parcel.readString();
        this.g0 = (Uri) parcel.readParcelable(MediaResult.class.getClassLoader());
        this.j0 = parcel.readLong();
        this.k0 = parcel.readLong();
        this.l0 = parcel.readLong();
    }
}
