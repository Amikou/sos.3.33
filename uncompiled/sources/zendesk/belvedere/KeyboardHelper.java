package zendesk.belvedere;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.graphics.Rect;
import android.os.Build;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.FrameLayout;
import java.lang.ref.WeakReference;
import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.List;
import okhttp3.internal.http2.Http2;

@SuppressLint({"ViewConstructor"})
/* loaded from: classes3.dex */
public class KeyboardHelper extends FrameLayout {
    public final int a;
    public int f0;
    public int g0;
    public boolean h0;
    public List<WeakReference<c>> i0;
    public d j0;
    public EditText k0;

    /* loaded from: classes3.dex */
    public static class a implements Runnable {
        public final /* synthetic */ EditText a;

        public a(EditText editText) {
            this.a = editText;
        }

        @Override // java.lang.Runnable
        public void run() {
            InputMethodManager inputMethodManager;
            if (!this.a.requestFocus() || (inputMethodManager = (InputMethodManager) this.a.getContext().getSystemService("input_method")) == null) {
                return;
            }
            inputMethodManager.showSoftInput(this.a, 1);
        }
    }

    /* loaded from: classes3.dex */
    public class b implements ViewTreeObserver.OnGlobalLayoutListener {
        public final Activity a;

        public /* synthetic */ b(KeyboardHelper keyboardHelper, Activity activity, a aVar) {
            this(activity);
        }

        @Override // android.view.ViewTreeObserver.OnGlobalLayoutListener
        public void onGlobalLayout() {
            int j = KeyboardHelper.this.j(this.a);
            KeyboardHelper.this.h0 = j > 0;
            if (j > 0 && KeyboardHelper.this.g0 != j) {
                KeyboardHelper.this.g0 = j;
                if (KeyboardHelper.this.j0 != null) {
                    KeyboardHelper.this.j0.a(j);
                }
            }
            if (KeyboardHelper.this.i0 == null || j <= 0) {
                KeyboardHelper.this.m();
            } else {
                KeyboardHelper.this.n();
            }
        }

        public b(Activity activity) {
            this.a = activity;
        }
    }

    /* loaded from: classes3.dex */
    public interface c {
        void onKeyboardDismissed();

        void onKeyboardVisible();
    }

    /* loaded from: classes3.dex */
    public interface d {
        void a(int i);
    }

    public KeyboardHelper(Activity activity) {
        super(activity);
        this.f0 = -1;
        this.g0 = -1;
        this.i0 = new ArrayList();
        this.a = getStatusBarHeight();
        int dimensionPixelSize = activity.getResources().getDimensionPixelSize(bz2.belvedere_dummy_edit_text_size);
        setLayoutParams(new ViewGroup.LayoutParams(dimensionPixelSize, dimensionPixelSize));
        EditText editText = new EditText(activity);
        this.k0 = editText;
        editText.setFocusable(true);
        this.k0.setFocusableInTouchMode(true);
        this.k0.setVisibility(0);
        this.k0.setImeOptions(268435456);
        this.k0.setInputType(Http2.INITIAL_MAX_FRAME_SIZE);
        addView(this.k0);
        activity.getWindow().getDecorView().findViewById(16908290).getViewTreeObserver().addOnGlobalLayoutListener(new b(this, activity, null));
    }

    private int getCachedInset() {
        if (this.f0 == -1) {
            this.f0 = getViewInset();
        }
        return this.f0;
    }

    private int getStatusBarHeight() {
        int identifier = getResources().getIdentifier("status_bar_height", "dimen", "android");
        if (identifier > 0) {
            return getResources().getDimensionPixelSize(identifier);
        }
        return 0;
    }

    private int getViewInset() {
        if (Build.VERSION.SDK_INT >= 21) {
            try {
                Field declaredField = View.class.getDeclaredField("mAttachInfo");
                declaredField.setAccessible(true);
                Object obj = declaredField.get(this);
                if (obj != null) {
                    Field declaredField2 = obj.getClass().getDeclaredField("mStableInsets");
                    declaredField2.setAccessible(true);
                    return ((Rect) declaredField2.get(obj)).bottom;
                }
                return 0;
            } catch (Exception unused) {
                return 0;
            }
        }
        return 0;
    }

    private int getViewPortHeight() {
        return (getRootView().getHeight() - this.a) - getCachedInset();
    }

    public static void k(Activity activity) {
        InputMethodManager inputMethodManager;
        View currentFocus = activity.getCurrentFocus();
        if (currentFocus == null || (inputMethodManager = (InputMethodManager) currentFocus.getContext().getSystemService("input_method")) == null) {
            return;
        }
        inputMethodManager.hideSoftInputFromWindow(currentFocus.getWindowToken(), 0);
    }

    public static KeyboardHelper l(Activity activity) {
        ViewGroup viewGroup = (ViewGroup) activity.getWindow().getDecorView();
        int childCount = viewGroup.getChildCount();
        for (int i = 0; i < childCount; i++) {
            if (viewGroup.getChildAt(i) instanceof KeyboardHelper) {
                return (KeyboardHelper) viewGroup.getChildAt(i);
            }
        }
        KeyboardHelper keyboardHelper = new KeyboardHelper(activity);
        viewGroup.addView(keyboardHelper);
        return keyboardHelper;
    }

    public static void o(EditText editText) {
        editText.post(new a(editText));
    }

    public EditText getInputTrap() {
        return this.k0;
    }

    public int getKeyboardHeight() {
        return this.g0;
    }

    public void i(c cVar) {
        this.i0.add(new WeakReference<>(cVar));
    }

    public final int j(Activity activity) {
        Rect rect = new Rect();
        activity.getWindow().getDecorView().getWindowVisibleDisplayFrame(rect);
        return getViewPortHeight() - (rect.bottom - rect.top);
    }

    public final void m() {
        for (WeakReference<c> weakReference : this.i0) {
            if (weakReference.get() != null) {
                weakReference.get().onKeyboardDismissed();
            }
        }
    }

    public final void n() {
        for (WeakReference<c> weakReference : this.i0) {
            if (weakReference.get() != null) {
                weakReference.get().onKeyboardVisible();
            }
        }
    }

    public void setKeyboardHeightListener(d dVar) {
        this.j0 = dVar;
    }
}
