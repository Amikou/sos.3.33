package zendesk.belvedere;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.LinearLayout;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import java.util.ArrayList;
import java.util.List;

/* loaded from: classes3.dex */
public class FloatingActionMenu extends LinearLayout implements View.OnClickListener {
    public FloatingActionButton a;
    public LayoutInflater f0;
    public final List<jp2<FloatingActionButton, View.OnClickListener>> g0;
    public View.OnClickListener h0;
    public boolean i0;
    public boolean j0;
    public int k0;
    public final c l0;

    /* loaded from: classes3.dex */
    public class a extends c {
        public final /* synthetic */ jp2 a;

        /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
        public a(jp2 jp2Var) {
            super(null);
            this.a = jp2Var;
        }

        @Override // android.view.animation.Animation.AnimationListener
        public void onAnimationEnd(Animation animation) {
            FloatingActionMenu.this.d((View) this.a.a, 4);
        }
    }

    /* loaded from: classes3.dex */
    public class b extends c {
        public b() {
            super(null);
        }

        @Override // android.view.animation.Animation.AnimationListener
        public void onAnimationEnd(Animation animation) {
            for (jp2 jp2Var : FloatingActionMenu.this.g0) {
                FloatingActionMenu.this.d((View) jp2Var.a, 8);
            }
        }
    }

    /* loaded from: classes3.dex */
    public static abstract class c implements Animation.AnimationListener {
        public c() {
        }

        @Override // android.view.animation.Animation.AnimationListener
        public void onAnimationRepeat(Animation animation) {
        }

        @Override // android.view.animation.Animation.AnimationListener
        public void onAnimationStart(Animation animation) {
        }

        public /* synthetic */ c(a aVar) {
            this();
        }
    }

    public FloatingActionMenu(Context context) {
        super(context);
        this.g0 = new ArrayList();
        this.j0 = true;
        this.l0 = new b();
        h(context);
    }

    public void c(int i, int i2, int i3, View.OnClickListener onClickListener) {
        FloatingActionButton floatingActionButton = (FloatingActionButton) this.f0.inflate(c13.belvedere_floating_action_menu_item, (ViewGroup) this, false);
        floatingActionButton.setOnClickListener(onClickListener);
        floatingActionButton.setImageDrawable(e(i, ny2.belvedere_floating_action_menu_item_icon_color));
        floatingActionButton.setId(i2);
        floatingActionButton.setContentDescription(getResources().getString(i3));
        this.g0.add(jp2.a(floatingActionButton, onClickListener));
        if (this.g0.size() == 1) {
            this.a.setImageDrawable(e(i, ny2.belvedere_floating_action_menu_icon_color));
            this.a.setContentDescription(getResources().getString(i3));
        } else if (this.g0.size() == 2) {
            addView(this.g0.get(0).a, 0);
            addView(floatingActionButton, 0);
            this.a.setImageDrawable(e(uz2.belvedere_fam_icon_add_file, ny2.belvedere_floating_action_menu_icon_color));
            this.a.setContentDescription(getResources().getString(k13.belvedere_fam_desc_expand_fam));
        } else {
            addView(floatingActionButton, 0);
        }
        if (this.g0.isEmpty()) {
            return;
        }
        g();
    }

    public final void d(View view, int i) {
        if (view != null) {
            view.setVisibility(i);
        }
    }

    public final Drawable e(int i, int i2) {
        Context context = getContext();
        Drawable r = androidx.core.graphics.drawable.a.r(m70.f(context, i));
        androidx.core.graphics.drawable.a.n(r, m70.d(context, i2));
        return r;
    }

    public final void f() {
        i(false);
        k(false);
        this.a.setContentDescription(getResources().getString(k13.belvedere_fam_desc_collapse_fam));
    }

    public void g() {
        if (this.g0.isEmpty()) {
            return;
        }
        if (this.j0) {
            this.a.setImageResource(uz2.belvedere_fam_icon_add_file);
        }
        this.j0 = false;
    }

    public final void h(Context context) {
        LinearLayout.inflate(context, c13.belvedere_floating_action_menu, this);
        if (isInEditMode()) {
            return;
        }
        setOrientation(1);
        setOnClickListener(this);
        FloatingActionButton floatingActionButton = (FloatingActionButton) findViewById(g03.floating_action_menu_fab);
        this.a = floatingActionButton;
        floatingActionButton.setOnClickListener(this);
        this.f0 = LayoutInflater.from(context);
        this.k0 = getResources().getInteger(t03.belvedere_fam_animation_delay_subsequent_item);
        l();
    }

    public final void i(boolean z) {
        if (z) {
            this.a.setImageResource(uz2.belvedere_fam_icon_close);
        } else {
            this.a.setImageResource(uz2.belvedere_fam_icon_add_file);
        }
    }

    public final void j() {
        i(true);
        k(true);
        this.a.setContentDescription(getResources().getString(k13.belvedere_fam_desc_expand_fam));
    }

    public final void k(boolean z) {
        if (this.g0.isEmpty()) {
            l();
            return;
        }
        long j = 0;
        if (z) {
            for (jp2<FloatingActionButton, View.OnClickListener> jp2Var : this.g0) {
                Animation loadAnimation = AnimationUtils.loadAnimation(getContext(), sx2.belvedere_show_menu_item);
                loadAnimation.setRepeatMode(2);
                loadAnimation.setStartOffset(j);
                FloatingActionButton floatingActionButton = jp2Var.a;
                if (floatingActionButton != null) {
                    d(floatingActionButton, 0);
                    jp2Var.a.startAnimation(loadAnimation);
                }
                j += this.k0;
            }
            return;
        }
        Animation animation = null;
        int size = this.g0.size() - 1;
        while (size >= 0) {
            jp2<FloatingActionButton, View.OnClickListener> jp2Var2 = this.g0.get(size);
            Animation loadAnimation2 = AnimationUtils.loadAnimation(getContext(), sx2.belvedere_hide_menu_item);
            loadAnimation2.setRepeatMode(2);
            loadAnimation2.setStartOffset(j);
            loadAnimation2.setAnimationListener(new a(jp2Var2));
            FloatingActionButton floatingActionButton2 = jp2Var2.a;
            if (floatingActionButton2 != null) {
                floatingActionButton2.startAnimation(loadAnimation2);
            }
            j += this.k0;
            size--;
            animation = loadAnimation2;
        }
        if (animation != null) {
            animation.setAnimationListener(this.l0);
        }
    }

    public void l() {
        this.j0 = true;
        if (this.i0) {
            f();
        }
        this.a.setImageResource(uz2.belvedere_fam_icon_send);
    }

    public final void m() {
        boolean z = !this.i0;
        this.i0 = z;
        if (z) {
            j();
        } else {
            f();
        }
    }

    @Override // android.view.View.OnClickListener
    public void onClick(View view) {
        View.OnClickListener onClickListener;
        if (this.j0 && (onClickListener = this.h0) != null) {
            onClickListener.onClick(this);
        } else if (this.g0.isEmpty()) {
        } else {
            if (this.g0.size() == 1) {
                jp2<FloatingActionButton, View.OnClickListener> jp2Var = this.g0.get(0);
                jp2Var.b.onClick(jp2Var.a);
                return;
            }
            m();
        }
    }

    public void setOnSendClickListener(View.OnClickListener onClickListener) {
        this.h0 = onClickListener;
    }

    public FloatingActionMenu(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        this.g0 = new ArrayList();
        this.j0 = true;
        this.l0 = new b();
        h(context);
    }

    public FloatingActionMenu(Context context, AttributeSet attributeSet, int i) {
        super(context, attributeSet, i);
        this.g0 = new ArrayList();
        this.j0 = true;
        this.l0 = new b();
        h(context);
    }
}
