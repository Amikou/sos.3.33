package zendesk.belvedere;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.text.TextUtils;
import android.util.Pair;
import java.io.File;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import zendesk.belvedere.MediaIntent;
import zendesk.belvedere.i;

/* compiled from: Belvedere.java */
/* loaded from: classes3.dex */
public class a {
    @SuppressLint({"StaticFieldLeak"})
    public static a e;
    public final Context a;
    public vt3 b;
    public as1 c;
    public s62 d;

    /* compiled from: Belvedere.java */
    /* renamed from: zendesk.belvedere.a$a  reason: collision with other inner class name */
    /* loaded from: classes3.dex */
    public static class C0295a {
        public Context a;
        public i.b b = new i.a();
        public boolean c = false;

        public C0295a(Context context) {
            this.a = context.getApplicationContext();
        }

        public a a() {
            return new a(this);
        }
    }

    public a(C0295a c0295a) {
        Context context = c0295a.a;
        this.a = context;
        c0295a.b.e(c0295a.c);
        i.d(c0295a.b);
        this.c = new as1();
        vt3 vt3Var = new vt3();
        this.b = vt3Var;
        this.d = new s62(context, vt3Var, this.c);
        i.a("Belvedere", "Belvedere initialized");
    }

    public static a c(Context context) {
        synchronized (a.class) {
            if (e == null) {
                if (context != null && context.getApplicationContext() != null) {
                    e = new C0295a(context.getApplicationContext()).a();
                } else {
                    throw new IllegalArgumentException("Invalid context provided");
                }
            }
        }
        return e;
    }

    public MediaIntent.b a() {
        return new MediaIntent.b(this.c.d(), this.d, this.c);
    }

    public MediaIntent.c b() {
        return new MediaIntent.c(this.c.d(), this.d);
    }

    public MediaResult d(String str, String str2) {
        Uri i;
        long j;
        long j2;
        File d = this.b.d(this.a, str, str2);
        i.a("Belvedere", String.format(Locale.US, "Get internal File: %s", d));
        if (d == null || (i = this.b.i(this.a, d)) == null) {
            return null;
        }
        MediaResult j3 = vt3.j(this.a, i);
        if (j3.g().contains("image")) {
            Pair<Integer, Integer> a = sq.a(d);
            j2 = ((Integer) a.second).intValue();
            j = ((Integer) a.first).intValue();
        } else {
            j = -1;
            j2 = -1;
        }
        return new MediaResult(d, i, i, str2, j3.g(), j3.k(), j, j2);
    }

    public void e(int i, int i2, Intent intent, uu<List<MediaResult>> uuVar, boolean z) {
        this.d.e(this.a, i, i2, intent, uuVar, z);
    }

    public Intent f(Uri uri, String str) {
        Intent intent = new Intent();
        intent.setAction("android.intent.action.VIEW");
        if (!TextUtils.isEmpty(str)) {
            intent.setDataAndType(uri, str);
        }
        g(intent, uri);
        return intent;
    }

    public void g(Intent intent, Uri uri) {
        i.a("Belvedere", String.format(Locale.US, "Grant Permission - Intent: %s - Uri: %s", intent, uri));
        this.b.l(this.a, intent, uri, 3);
    }

    public void h(List<Uri> list, String str, uu<List<MediaResult>> uuVar) {
        if (list != null && list.size() > 0) {
            q73.d(this.a, this.b, uuVar, list, str);
        } else {
            uuVar.internalSuccess(new ArrayList(0));
        }
    }
}
