package zendesk.belvedere;

import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import com.squareup.picasso.Picasso;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;
import zendesk.belvedere.FixedWidthImageView;
import zendesk.belvedere.SelectableView;
import zendesk.belvedere.c;

/* compiled from: ImageStreamItems.java */
/* loaded from: classes3.dex */
public class d {
    public static final int a = uz2.belvedere_ic_camera_black;
    public static final int b = c13.belvedere_stream_list_item_square_static;

    /* compiled from: ImageStreamItems.java */
    /* loaded from: classes3.dex */
    public static class a implements View.OnClickListener {
        public final /* synthetic */ c.b a;

        public a(c.b bVar) {
            this.a = bVar;
        }

        @Override // android.view.View.OnClickListener
        public void onClick(View view) {
            this.a.b();
        }
    }

    /* compiled from: ImageStreamItems.java */
    /* loaded from: classes3.dex */
    public static abstract class b {
        public final int a;
        public final MediaResult c;
        public final long b = UUID.randomUUID().hashCode();
        public boolean d = false;

        public b(int i, MediaResult mediaResult) {
            this.a = i;
            this.c = mediaResult;
        }

        public abstract void a(View view);

        public long b() {
            return this.b;
        }

        public int c() {
            return this.a;
        }

        public MediaResult d() {
            return this.c;
        }

        public boolean e() {
            return this.d;
        }

        public void f(boolean z) {
            this.d = z;
        }
    }

    /* compiled from: ImageStreamItems.java */
    /* loaded from: classes3.dex */
    public static class c extends b {
        public final int e;
        public final View.OnClickListener f;

        public /* synthetic */ c(int i, int i2, View.OnClickListener onClickListener, a aVar) {
            this(i, i2, onClickListener);
        }

        @Override // zendesk.belvedere.d.b
        public void a(View view) {
            ((ImageView) view.findViewById(g03.list_item_static_image)).setImageResource(this.e);
            view.findViewById(g03.list_item_static_click_area).setOnClickListener(this.f);
        }

        public c(int i, int i2, View.OnClickListener onClickListener) {
            super(i, null);
            this.e = i2;
            this.f = onClickListener;
        }
    }

    /* compiled from: ImageStreamItems.java */
    /* renamed from: zendesk.belvedere.d$d  reason: collision with other inner class name */
    /* loaded from: classes3.dex */
    public static class C0297d extends b {
        public final MediaResult e;
        public final ResolveInfo f;
        public final c.b g;

        /* compiled from: ImageStreamItems.java */
        /* renamed from: zendesk.belvedere.d$d$a */
        /* loaded from: classes3.dex */
        public class a implements SelectableView.c {
            public a() {
            }

            @Override // zendesk.belvedere.SelectableView.c
            public boolean a(boolean z) {
                return C0297d.this.g.a(C0297d.this);
            }
        }

        public C0297d(c.b bVar, MediaResult mediaResult, Context context) {
            super(c13.belvedere_stream_list_item_genric_file, mediaResult);
            this.e = mediaResult;
            this.f = h(mediaResult.h(), context);
            this.g = bVar;
        }

        @Override // zendesk.belvedere.d.b
        public void a(View view) {
            Context context = view.getContext();
            ImageView imageView = (ImageView) view.findViewById(g03.list_item_file_icon);
            TextView textView = (TextView) view.findViewById(g03.list_item_file_label);
            SelectableView selectableView = (SelectableView) view.findViewById(g03.list_item_file_holder);
            selectableView.setContentDescriptionStrings(context.getString(k13.belvedere_stream_item_unselect_file_desc, this.e.h()), context.getString(k13.belvedere_stream_item_select_file_desc, this.e.h()));
            ((TextView) view.findViewById(g03.list_item_file_title)).setText(this.e.h());
            if (this.f != null) {
                PackageManager packageManager = context.getPackageManager();
                textView.setText(this.f.loadLabel(packageManager));
                imageView.setImageDrawable(this.f.loadIcon(packageManager));
            } else {
                textView.setText(k13.belvedere_image_stream_unknown_app);
                imageView.setImageResource(17301651);
            }
            selectableView.setSelected(e());
            selectableView.setSelectionListener(new a());
        }

        public final ResolveInfo h(String str, Context context) {
            PackageManager packageManager = context.getPackageManager();
            MediaResult d = zendesk.belvedere.a.c(context).d("tmp", str);
            if (d == null) {
                return null;
            }
            Intent intent = new Intent("android.intent.action.VIEW");
            intent.setData(d.l());
            List<ResolveInfo> queryIntentActivities = packageManager.queryIntentActivities(intent, 65536);
            if (queryIntentActivities == null || queryIntentActivities.size() <= 0) {
                return null;
            }
            return queryIntentActivities.get(0);
        }
    }

    /* compiled from: ImageStreamItems.java */
    /* loaded from: classes3.dex */
    public static class e extends b {
        public final MediaResult e;
        public final c.b f;
        public FixedWidthImageView.b g;

        /* compiled from: ImageStreamItems.java */
        /* loaded from: classes3.dex */
        public class a implements FixedWidthImageView.c {
            public a() {
            }

            @Override // zendesk.belvedere.FixedWidthImageView.c
            public void a(FixedWidthImageView.b bVar) {
                e.this.g = bVar;
            }
        }

        /* compiled from: ImageStreamItems.java */
        /* loaded from: classes3.dex */
        public class b implements SelectableView.c {
            public b() {
            }

            @Override // zendesk.belvedere.SelectableView.c
            public boolean a(boolean z) {
                return e.this.f.a(e.this);
            }
        }

        public e(c.b bVar, MediaResult mediaResult) {
            super(c13.belvedere_stream_list_item, mediaResult);
            this.f = bVar;
            this.e = mediaResult;
        }

        @Override // zendesk.belvedere.d.b
        public void a(View view) {
            Context context = view.getContext();
            FixedWidthImageView fixedWidthImageView = (FixedWidthImageView) view.findViewById(g03.list_item_image);
            SelectableView selectableView = (SelectableView) view.findViewById(g03.list_item_selectable);
            selectableView.setContentDescriptionStrings(context.getString(k13.belvedere_stream_item_unselect_image_desc, this.e.h()), context.getString(k13.belvedere_stream_item_select_image_desc, this.e.h()));
            if (this.g != null) {
                fixedWidthImageView.f(Picasso.h(), this.e.j(), this.g);
            } else {
                fixedWidthImageView.e(Picasso.h(), this.e.j(), this.e.o(), this.e.f(), new a());
            }
            selectableView.setSelected(e());
            selectableView.setSelectionListener(new b());
        }
    }

    public static c a(c.b bVar) {
        return new c(b, a, new a(bVar), null);
    }

    public static List<b> b(List<MediaResult> list, c.b bVar, Context context) {
        ArrayList arrayList = new ArrayList(list.size());
        for (MediaResult mediaResult : list) {
            if (mediaResult.g() != null && mediaResult.g().startsWith("image")) {
                arrayList.add(new e(bVar, mediaResult));
            } else {
                arrayList.add(new C0297d(bVar, mediaResult, context));
            }
        }
        return arrayList;
    }
}
