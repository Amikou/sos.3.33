package zendesk.belvedere;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.util.AttributeSet;
import android.util.Pair;
import android.view.View;
import androidx.appcompat.widget.AppCompatImageView;
import com.fasterxml.jackson.core.util.MinimalPrettyPrinter;
import com.squareup.picasso.Picasso;
import com.squareup.picasso.r;
import java.util.concurrent.atomic.AtomicBoolean;

/* loaded from: classes3.dex */
public class FixedWidthImageView extends AppCompatImageView implements r {
    public int a;
    public int f0;
    public int g0;
    public int h0;
    public Uri i0;
    public Picasso j0;
    public final AtomicBoolean k0;
    public c l0;

    /* loaded from: classes3.dex */
    public class a implements Runnable {
        public a() {
        }

        @Override // java.lang.Runnable
        public void run() {
            FixedWidthImageView.this.requestLayout();
        }
    }

    /* loaded from: classes3.dex */
    public static class b {
        public final int a;
        public final int b;
        public final int c;
        public final int d;

        public b(int i, int i2, int i3, int i4) {
            this.a = i;
            this.b = i2;
            this.c = i3;
            this.d = i4;
        }
    }

    /* loaded from: classes3.dex */
    public interface c {
        void a(b bVar);
    }

    public FixedWidthImageView(Context context) {
        super(context);
        this.a = -1;
        this.f0 = -1;
        this.i0 = null;
        this.k0 = new AtomicBoolean(false);
        init();
    }

    public final void c(Picasso picasso, int i, int i2, Uri uri) {
        this.f0 = i2;
        post(new a());
        c cVar = this.l0;
        if (cVar != null) {
            cVar.a(new b(this.h0, this.g0, this.f0, this.a));
            this.l0 = null;
        }
        picasso.j(uri).k(i, i2).l(rg4.d(getContext(), bz2.belvedere_image_stream_item_radius)).f(this);
    }

    public final Pair<Integer, Integer> d(int i, int i2, int i3) {
        return Pair.create(Integer.valueOf(i), Integer.valueOf((int) (i3 * (i / i2))));
    }

    public void e(Picasso picasso, Uri uri, long j, long j2, c cVar) {
        if (uri != null && !uri.equals(this.i0)) {
            Picasso picasso2 = this.j0;
            if (picasso2 != null) {
                picasso2.c(this);
                this.j0.b(this);
            }
            this.i0 = uri;
            this.j0 = picasso;
            int i = (int) j;
            this.g0 = i;
            int i2 = (int) j2;
            this.h0 = i2;
            this.l0 = cVar;
            int i3 = this.a;
            if (i3 > 0) {
                g(picasso, uri, i3, i, i2);
                return;
            } else {
                this.k0.set(true);
                return;
            }
        }
        i.a("FixedWidthImageView", "Image already loaded. " + uri);
    }

    public void f(Picasso picasso, Uri uri, b bVar) {
        if (uri != null && !uri.equals(this.i0)) {
            Picasso picasso2 = this.j0;
            if (picasso2 != null) {
                picasso2.c(this);
                this.j0.b(this);
            }
            this.i0 = uri;
            this.j0 = picasso;
            this.g0 = bVar.b;
            this.h0 = bVar.a;
            this.f0 = bVar.c;
            int i = bVar.d;
            this.a = i;
            g(picasso, uri, i, this.g0, this.h0);
            return;
        }
        i.a("FixedWidthImageView", "Image already loaded. " + uri);
    }

    public final void g(Picasso picasso, Uri uri, int i, int i2, int i3) {
        i.a("FixedWidthImageView", "Start loading image: " + i + MinimalPrettyPrinter.DEFAULT_ROOT_VALUE_SEPARATOR + i2 + MinimalPrettyPrinter.DEFAULT_ROOT_VALUE_SEPARATOR + i3);
        if (i2 > 0 && i3 > 0) {
            Pair<Integer, Integer> d = d(i, i2, i3);
            c(picasso, ((Integer) d.first).intValue(), ((Integer) d.second).intValue(), uri);
            return;
        }
        picasso.j(uri).h(this);
    }

    public void init() {
        this.f0 = getResources().getDimensionPixelOffset(bz2.belvedere_image_stream_image_height);
    }

    @Override // com.squareup.picasso.r
    public void onBitmapFailed(Exception exc, Drawable drawable) {
    }

    @Override // com.squareup.picasso.r
    public void onBitmapLoaded(Bitmap bitmap, Picasso.LoadedFrom loadedFrom) {
        this.h0 = bitmap.getHeight();
        int width = bitmap.getWidth();
        this.g0 = width;
        Pair<Integer, Integer> d = d(this.a, width, this.h0);
        c(this.j0, ((Integer) d.first).intValue(), ((Integer) d.second).intValue(), this.i0);
    }

    @Override // android.widget.ImageView, android.view.View
    public void onMeasure(int i, int i2) {
        int size = View.MeasureSpec.getSize(i);
        int makeMeasureSpec = View.MeasureSpec.makeMeasureSpec(this.f0, 1073741824);
        if (this.a == -1) {
            this.a = size;
        }
        int i3 = this.a;
        if (i3 > 0) {
            i = View.MeasureSpec.makeMeasureSpec(i3, 1073741824);
            if (this.k0.compareAndSet(true, false)) {
                g(this.j0, this.i0, this.a, this.g0, this.h0);
            }
        }
        super.onMeasure(i, makeMeasureSpec);
    }

    @Override // com.squareup.picasso.r
    public void onPrepareLoad(Drawable drawable) {
    }

    public FixedWidthImageView(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        this.a = -1;
        this.f0 = -1;
        this.i0 = null;
        this.k0 = new AtomicBoolean(false);
        init();
    }

    public FixedWidthImageView(Context context, AttributeSet attributeSet, int i) {
        super(context, attributeSet, i);
        this.a = -1;
        this.f0 = -1;
        this.i0 = null;
        this.k0 = new AtomicBoolean(false);
        init();
    }
}
