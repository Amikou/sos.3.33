package zendesk.belvedere;

import android.animation.ValueAnimator;
import android.content.Context;
import android.util.AttributeSet;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.ImageView;

/* loaded from: classes3.dex */
public class SelectableView extends FrameLayout implements View.OnClickListener {
    public c a;
    public View f0;
    public View g0;
    public String h0;
    public String i0;

    /* loaded from: classes3.dex */
    public class a implements ValueAnimator.AnimatorUpdateListener {
        public a() {
        }

        @Override // android.animation.ValueAnimator.AnimatorUpdateListener
        public void onAnimationUpdate(ValueAnimator valueAnimator) {
            SelectableView.this.g(((Float) valueAnimator.getAnimatedValue()).floatValue());
        }
    }

    /* loaded from: classes3.dex */
    public class b implements ValueAnimator.AnimatorUpdateListener {
        public b() {
        }

        @Override // android.animation.ValueAnimator.AnimatorUpdateListener
        public void onAnimationUpdate(ValueAnimator valueAnimator) {
            SelectableView.this.c(((Float) valueAnimator.getAnimatedValue()).floatValue());
        }
    }

    /* loaded from: classes3.dex */
    public interface c {
        boolean a(boolean z);
    }

    public SelectableView(Context context) {
        super(context);
        f();
    }

    private View getChild() {
        View view = this.f0;
        if (view != null) {
            return view;
        }
        if (getChildCount() == 2) {
            int i = 0;
            while (true) {
                if (i >= getChildCount()) {
                    break;
                }
                View childAt = getChildAt(i);
                if (childAt.getId() != g03.belvedere_selectable_view_checkbox) {
                    this.f0 = childAt;
                    break;
                }
                i++;
            }
            return this.f0;
        }
        throw new RuntimeException("More then one child added to SelectableView");
    }

    private void setContentDesc(boolean z) {
        if (z) {
            setContentDescription(this.h0);
        } else {
            setContentDescription(this.i0);
        }
    }

    public final void c(float f) {
        getChild().setAlpha(f);
    }

    public final void d(boolean z) {
        if (z) {
            this.g0.setVisibility(0);
            this.g0.bringToFront();
            ei4.A0(this.g0, ei4.y(this.f0) + 1.0f);
            return;
        }
        this.g0.setVisibility(8);
    }

    public final ImageView e(int i) {
        FrameLayout.LayoutParams layoutParams = new FrameLayout.LayoutParams(-2, -2);
        layoutParams.gravity = 17;
        ImageView imageView = new ImageView(getContext());
        imageView.setId(g03.belvedere_selectable_view_checkbox);
        imageView.setImageDrawable(m70.f(getContext(), uz2.belvedere_ic_check_circle));
        ei4.w0(imageView, m70.f(getContext(), uz2.belvedere_ic_check_bg));
        imageView.setLayoutParams(layoutParams);
        imageView.setVisibility(8);
        rg4.b(imageView, i);
        return imageView;
    }

    public final void f() {
        setClickable(true);
        setFocusable(true);
        setOnClickListener(this);
        ImageView e = e(rg4.a(getContext(), zx2.colorPrimary));
        this.g0 = e;
        addView(e);
    }

    public final void g(float f) {
        getChild().setScaleX(f);
        getChild().setScaleY(f);
    }

    @Override // android.view.View.OnClickListener
    public void onClick(View view) {
        ValueAnimator ofFloat;
        ValueAnimator ofFloat2;
        boolean z = !isSelected();
        c cVar = this.a;
        if (cVar != null ? cVar.a(z) : true) {
            setSelected(z);
            if (z) {
                ofFloat = ValueAnimator.ofFloat(1.0f, 0.9f);
                ofFloat2 = ValueAnimator.ofFloat(1.0f, 0.8f);
            } else {
                ofFloat = ValueAnimator.ofFloat(0.9f, 1.0f);
                ofFloat2 = ValueAnimator.ofFloat(0.8f, 1.0f);
            }
            ofFloat.addUpdateListener(new a());
            ofFloat2.addUpdateListener(new b());
            ofFloat2.setDuration(75L);
            ofFloat.setDuration(75L);
            ofFloat.start();
            ofFloat2.start();
        }
    }

    public void setContentDescriptionStrings(String str, String str2) {
        this.h0 = str;
        this.i0 = str2;
        setContentDesc(isSelected());
    }

    @Override // android.view.View
    public void setSelected(boolean z) {
        super.setSelected(z);
        if (z) {
            g(0.9f);
            c(0.8f);
            d(true);
            setContentDesc(true);
            return;
        }
        g(1.0f);
        c(1.0f);
        d(false);
        setContentDesc(false);
    }

    public void setSelectionListener(c cVar) {
        this.a = cVar;
    }

    public SelectableView(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        f();
    }

    public SelectableView(Context context, AttributeSet attributeSet, int i) {
        super(context, attributeSet, i);
        f();
    }
}
