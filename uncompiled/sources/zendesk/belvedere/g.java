package zendesk.belvedere;

import android.view.View;
import com.github.mikephil.charting.utils.Utils;
import java.util.ArrayList;
import java.util.List;
import zendesk.belvedere.c;
import zendesk.belvedere.d;

/* compiled from: ImageStreamPresenter.java */
/* loaded from: classes3.dex */
public class g {
    public final xo1 a;
    public final f b;
    public final zendesk.belvedere.b c;
    public final c.b d = new c();

    /* compiled from: ImageStreamPresenter.java */
    /* loaded from: classes3.dex */
    public class a implements View.OnClickListener {
        public a() {
        }

        @Override // android.view.View.OnClickListener
        public void onClick(View view) {
            g.this.b.g(g.this.a.a(), g.this.c);
        }
    }

    /* compiled from: ImageStreamPresenter.java */
    /* loaded from: classes3.dex */
    public class b implements View.OnClickListener {
        public b() {
        }

        @Override // android.view.View.OnClickListener
        public void onClick(View view) {
            g.this.b.g(g.this.a.k(), g.this.c);
        }
    }

    /* compiled from: ImageStreamPresenter.java */
    /* loaded from: classes3.dex */
    public class c implements c.b {
        public c() {
        }

        @Override // zendesk.belvedere.c.b
        public boolean a(d.b bVar) {
            MediaResult d = bVar.d();
            long c = g.this.a.c();
            if ((d == null || d.k() > c) && c != -1) {
                g.this.b.a(k13.belvedere_image_stream_file_too_large);
                return false;
            }
            bVar.f(!bVar.e());
            List k = g.this.k(d, bVar.e());
            g.this.b.h(k.size());
            g.this.b.d(k.size());
            ArrayList arrayList = new ArrayList();
            arrayList.add(d);
            if (bVar.e()) {
                g.this.c.n(arrayList);
                return true;
            }
            g.this.c.m(arrayList);
            return true;
        }

        @Override // zendesk.belvedere.c.b
        public void b() {
            if (g.this.a.h()) {
                g.this.b.g(g.this.a.b(), g.this.c);
            }
        }
    }

    public g(xo1 xo1Var, f fVar, zendesk.belvedere.b bVar) {
        this.a = xo1Var;
        this.b = fVar;
        this.c = bVar;
    }

    public void e() {
        this.c.r(null, null);
        this.c.p(0, 0, Utils.FLOAT_EPSILON);
        this.c.l();
    }

    public void f() {
        i();
        g();
        this.b.h(this.a.j().size());
        this.b.d(this.a.j().size());
    }

    public final void g() {
        if (this.a.f()) {
            this.b.i(new a());
        }
        if (this.a.e()) {
            this.b.f(new b());
        }
    }

    public void h(int i, int i2, float f) {
        if (f >= Utils.FLOAT_EPSILON) {
            this.c.p(i, i2, f);
        }
    }

    public final void i() {
        boolean z = this.a.l() || this.b.b();
        this.b.c(z);
        this.b.e(this.a.g(), this.a.j(), z, this.a.h(), this.d);
        this.c.q();
    }

    public void j() {
        this.c.o(this.a.j());
    }

    public final List<MediaResult> k(MediaResult mediaResult, boolean z) {
        if (z) {
            return this.a.d(mediaResult);
        }
        return this.a.i(mediaResult);
    }
}
