package zendesk.belvedere;

import android.accessibilityservice.AccessibilityServiceInfo;
import android.animation.ArgbEvaluator;
import android.animation.ValueAnimator;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.graphics.Rect;
import android.graphics.drawable.BitmapDrawable;
import android.os.Build;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.accessibility.AccessibilityManager;
import android.widget.PopupWindow;
import android.widget.Toast;
import androidx.appcompat.widget.Toolbar;
import androidx.coordinatorlayout.widget.CoordinatorLayout;
import androidx.recyclerview.widget.RecyclerView;
import androidx.recyclerview.widget.StaggeredGridLayoutManager;
import com.github.mikephil.charting.utils.Utils;
import com.google.android.material.bottomsheet.BottomSheetBehavior;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;
import zendesk.belvedere.BelvedereUi;
import zendesk.belvedere.KeyboardHelper;
import zendesk.belvedere.c;

/* compiled from: ImageStreamUi.java */
/* loaded from: classes3.dex */
public class h extends PopupWindow implements zendesk.belvedere.f {
    public final zendesk.belvedere.g a;
    public final zendesk.belvedere.c f0;
    public final List<Integer> g0;
    public KeyboardHelper h0;
    public View i0;
    public View j0;
    public View k0;
    public View l0;
    public FloatingActionMenu m0;
    public RecyclerView n0;
    public Toolbar o0;
    public BottomSheetBehavior<View> p0;
    public Activity q0;

    /* compiled from: ImageStreamUi.java */
    /* loaded from: classes3.dex */
    public class a implements View.OnClickListener {
        public final /* synthetic */ boolean a;

        public a(boolean z) {
            this.a = z;
        }

        @Override // android.view.View.OnClickListener
        public void onClick(View view) {
            if (!this.a) {
                h.this.p0.V(4);
            } else {
                h.this.dismiss();
            }
        }
    }

    /* compiled from: ImageStreamUi.java */
    /* loaded from: classes3.dex */
    public class b extends BottomSheetBehavior.g {
        public b() {
        }

        @Override // com.google.android.material.bottomsheet.BottomSheetBehavior.g
        public void a(View view, float f) {
        }

        @Override // com.google.android.material.bottomsheet.BottomSheetBehavior.g
        public void b(View view, int i) {
            if (i != 5) {
                return;
            }
            h.this.dismiss();
        }
    }

    /* compiled from: ImageStreamUi.java */
    /* loaded from: classes3.dex */
    public class c implements KeyboardHelper.d {
        public c() {
        }

        @Override // zendesk.belvedere.KeyboardHelper.d
        public void a(int i) {
            if (i != h.this.p0.B()) {
                h.this.p0.R(h.this.i0.getPaddingTop() + h.this.h0.getKeyboardHeight());
            }
        }
    }

    /* compiled from: ImageStreamUi.java */
    /* loaded from: classes3.dex */
    public class d implements View.OnClickListener {
        public d() {
        }

        @Override // android.view.View.OnClickListener
        public void onClick(View view) {
            h.this.a.j();
            h.this.dismiss();
        }
    }

    /* compiled from: ImageStreamUi.java */
    /* loaded from: classes3.dex */
    public class e implements View.OnTouchListener {
        public final /* synthetic */ List a;
        public final /* synthetic */ Activity f0;

        public e(List list, Activity activity) {
            this.a = list;
            this.f0 = activity;
        }

        @Override // android.view.View.OnTouchListener
        @SuppressLint({"ClickableViewAccessibility"})
        public boolean onTouch(View view, MotionEvent motionEvent) {
            boolean z;
            int rawX = (int) motionEvent.getRawX();
            int rawY = (int) motionEvent.getRawY();
            Iterator it = this.a.iterator();
            while (true) {
                z = false;
                if (!it.hasNext()) {
                    z = true;
                    break;
                }
                View findViewById = this.f0.findViewById(((Integer) it.next()).intValue());
                if (findViewById != null) {
                    Rect rect = new Rect();
                    findViewById.getGlobalVisibleRect(rect);
                    boolean z2 = rawX >= rect.left && rawX <= rect.right;
                    boolean z3 = rawY >= rect.top && rawY <= rect.bottom;
                    if (z2 && z3) {
                        this.f0.dispatchTouchEvent(MotionEvent.obtain(motionEvent));
                        break;
                    }
                }
            }
            if (z) {
                h.this.dismiss();
            }
            return true;
        }
    }

    /* compiled from: ImageStreamUi.java */
    /* loaded from: classes3.dex */
    public class f implements ValueAnimator.AnimatorUpdateListener {
        public final /* synthetic */ Window a;
        public final /* synthetic */ ValueAnimator f0;

        public f(h hVar, Window window, ValueAnimator valueAnimator) {
            this.a = window;
            this.f0 = valueAnimator;
        }

        @Override // android.animation.ValueAnimator.AnimatorUpdateListener
        public void onAnimationUpdate(ValueAnimator valueAnimator) {
            this.a.setStatusBarColor(((Integer) this.f0.getAnimatedValue()).intValue());
        }
    }

    /* compiled from: ImageStreamUi.java */
    /* loaded from: classes3.dex */
    public class g extends CoordinatorLayout.Behavior<View> {
        public final boolean a;

        public /* synthetic */ g(h hVar, boolean z, a aVar) {
            this(z);
        }

        public final void a(int i, float f, int i2, View view) {
            float f2 = i;
            float f3 = f2 - (f * f2);
            float f4 = i2;
            if (f3 <= f4) {
                rg4.f(h.this.getContentView(), true);
                view.setAlpha(1.0f - (f3 / f4));
                view.setY(f3);
            } else {
                rg4.f(h.this.getContentView(), false);
            }
            h.this.w(f);
        }

        @Override // androidx.coordinatorlayout.widget.CoordinatorLayout.Behavior
        public boolean layoutDependsOn(CoordinatorLayout coordinatorLayout, View view, View view2) {
            return view2.getId() == g03.bottom_sheet;
        }

        @Override // androidx.coordinatorlayout.widget.CoordinatorLayout.Behavior
        public boolean onDependentViewChanged(CoordinatorLayout coordinatorLayout, View view, View view2) {
            int height = coordinatorLayout.getHeight() - h.this.p0.B();
            float height2 = ((coordinatorLayout.getHeight() - view2.getY()) - h.this.p0.B()) / height;
            a(height, height2, ei4.F(h.this.o0), view);
            if (this.a) {
                h.this.a.h(coordinatorLayout.getHeight(), height, height2);
                return true;
            }
            return true;
        }

        public g(boolean z) {
            this.a = z;
        }
    }

    public h(Activity activity, View view, zendesk.belvedere.b bVar, BelvedereUi.UiConfig uiConfig) {
        super(view, -1, -1, false);
        setInputMethodMode(2);
        setFocusable(true);
        setTouchable(true);
        setBackgroundDrawable(new BitmapDrawable());
        setOutsideTouchable(true);
        p(view);
        this.q0 = activity;
        this.f0 = new zendesk.belvedere.c();
        this.h0 = bVar.i();
        this.g0 = uiConfig.e();
        zendesk.belvedere.g gVar = new zendesk.belvedere.g(new zendesk.belvedere.e(view.getContext(), uiConfig), this, bVar);
        this.a = gVar;
        gVar.f();
    }

    public static h v(Activity activity, ViewGroup viewGroup, zendesk.belvedere.b bVar, BelvedereUi.UiConfig uiConfig) {
        h hVar = new h(activity, LayoutInflater.from(activity).inflate(c13.belvedere_image_stream, viewGroup, false), bVar, uiConfig);
        hVar.showAtLocation(viewGroup, 48, 0, 0);
        return hVar;
    }

    @Override // zendesk.belvedere.f
    public void a(int i) {
        Toast.makeText(this.q0, i, 0).show();
    }

    @Override // zendesk.belvedere.f
    public boolean b() {
        List<AccessibilityServiceInfo> enabledAccessibilityServiceList;
        if (Build.VERSION.SDK_INT < 24 || !(this.q0.isInMultiWindowMode() || this.q0.isInPictureInPictureMode())) {
            if (this.q0.getResources().getConfiguration().keyboard != 1) {
                return true;
            }
            AccessibilityManager accessibilityManager = (AccessibilityManager) this.q0.getSystemService("accessibility");
            return (accessibilityManager == null || (enabledAccessibilityServiceList = accessibilityManager.getEnabledAccessibilityServiceList(47)) == null || enabledAccessibilityServiceList.size() <= 0) ? false : true;
        }
        return true;
    }

    @Override // zendesk.belvedere.f
    public void c(boolean z) {
        t(this.f0);
        u(z);
        q(z);
        s(this.q0, this.g0);
        r(this.m0);
    }

    @Override // zendesk.belvedere.f
    public void d(int i) {
        if (i == 0) {
            this.m0.g();
        } else {
            this.m0.l();
        }
    }

    @Override // android.widget.PopupWindow
    public void dismiss() {
        super.dismiss();
        w(Utils.FLOAT_EPSILON);
        this.a.e();
    }

    @Override // zendesk.belvedere.f
    public void e(List<MediaResult> list, List<MediaResult> list2, boolean z, boolean z2, c.b bVar) {
        if (!z) {
            KeyboardHelper.o(this.h0.getInputTrap());
        }
        ViewGroup.LayoutParams layoutParams = this.i0.getLayoutParams();
        layoutParams.height = -1;
        this.i0.setLayoutParams(layoutParams);
        if (z2) {
            this.f0.a(zendesk.belvedere.d.a(bVar));
        }
        this.f0.b(zendesk.belvedere.d.b(list, bVar, this.i0.getContext()));
        this.f0.c(list2);
        this.f0.notifyDataSetChanged();
    }

    @Override // zendesk.belvedere.f
    public void f(View.OnClickListener onClickListener) {
        FloatingActionMenu floatingActionMenu = this.m0;
        if (floatingActionMenu != null) {
            floatingActionMenu.c(uz2.belvedere_ic_file, g03.belvedere_fam_item_documents, k13.belvedere_fam_desc_open_gallery, onClickListener);
        }
    }

    @Override // zendesk.belvedere.f
    public void g(MediaIntent mediaIntent, zendesk.belvedere.b bVar) {
        mediaIntent.f(bVar);
    }

    @Override // zendesk.belvedere.f
    public void h(int i) {
        if (i > 0) {
            this.o0.setTitle(String.format(Locale.getDefault(), "%s (%d)", this.q0.getString(k13.belvedere_image_stream_title), Integer.valueOf(i)));
            return;
        }
        this.o0.setTitle(k13.belvedere_image_stream_title);
    }

    @Override // zendesk.belvedere.f
    public void i(View.OnClickListener onClickListener) {
        FloatingActionMenu floatingActionMenu = this.m0;
        if (floatingActionMenu != null) {
            floatingActionMenu.c(uz2.belvedere_ic_collections, g03.belvedere_fam_item_google_photos, k13.belvedere_fam_desc_open_google_photos, onClickListener);
        }
    }

    public final void p(View view) {
        this.i0 = view.findViewById(g03.bottom_sheet);
        this.j0 = view.findViewById(g03.dismiss_area);
        this.n0 = (RecyclerView) view.findViewById(g03.image_list);
        this.o0 = (Toolbar) view.findViewById(g03.image_stream_toolbar);
        this.k0 = view.findViewById(g03.image_stream_toolbar_container);
        this.l0 = view.findViewById(g03.image_stream_compat_shadow);
        this.m0 = (FloatingActionMenu) view.findViewById(g03.floating_action_menu);
    }

    public final void q(boolean z) {
        ei4.A0(this.n0, this.i0.getContext().getResources().getDimensionPixelSize(bz2.belvedere_bottom_sheet_elevation));
        BottomSheetBehavior<View> y = BottomSheetBehavior.y(this.i0);
        this.p0 = y;
        y.o(new b());
        rg4.f(getContentView(), false);
        if (!z) {
            this.p0.R(this.i0.getPaddingTop() + this.h0.getKeyboardHeight());
            this.p0.V(4);
            this.h0.setKeyboardHeightListener(new c());
        } else {
            this.p0.U(true);
            this.p0.V(3);
            KeyboardHelper.k(this.q0);
        }
        this.n0.setClickable(true);
        this.i0.setVisibility(0);
    }

    public final void r(FloatingActionMenu floatingActionMenu) {
        floatingActionMenu.setOnSendClickListener(new d());
    }

    public final void s(Activity activity, List<Integer> list) {
        this.j0.setOnTouchListener(new e(list, activity));
    }

    public final void t(zendesk.belvedere.c cVar) {
        this.n0.setLayoutManager(new StaggeredGridLayoutManager(this.i0.getContext().getResources().getInteger(t03.belvedere_image_stream_column_count), 1));
        this.n0.setHasFixedSize(true);
        this.n0.setDrawingCacheEnabled(true);
        this.n0.setDrawingCacheQuality(1048576);
        androidx.recyclerview.widget.f fVar = new androidx.recyclerview.widget.f();
        fVar.setSupportsChangeAnimations(false);
        this.n0.setItemAnimator(fVar);
        this.n0.setAdapter(cVar);
    }

    public final void u(boolean z) {
        this.o0.setNavigationIcon(uz2.belvedere_ic_close);
        this.o0.setNavigationContentDescription(k13.belvedere_toolbar_desc_collapse);
        this.o0.setBackgroundColor(-1);
        this.o0.setNavigationOnClickListener(new a(z));
        if (Build.VERSION.SDK_INT < 21) {
            this.l0.setVisibility(0);
        }
        CoordinatorLayout.e eVar = (CoordinatorLayout.e) this.k0.getLayoutParams();
        if (eVar != null) {
            eVar.o(new g(this, !z, null));
        }
    }

    public final void w(float f2) {
        int color = this.o0.getResources().getColor(ny2.belvedere_image_stream_status_bar_color);
        int a2 = rg4.a(this.o0.getContext(), zx2.colorPrimaryDark);
        boolean z = f2 == 1.0f;
        Window window = this.q0.getWindow();
        int i = Build.VERSION.SDK_INT;
        if (i >= 21) {
            if (z) {
                if (window.getStatusBarColor() == a2) {
                    ValueAnimator ofObject = ValueAnimator.ofObject(new ArgbEvaluator(), Integer.valueOf(a2), Integer.valueOf(color));
                    ofObject.setDuration(100L);
                    ofObject.addUpdateListener(new f(this, window, ofObject));
                    ofObject.start();
                }
            } else {
                window.setStatusBarColor(a2);
            }
        }
        if (i >= 23) {
            View decorView = window.getDecorView();
            if (z) {
                decorView.setSystemUiVisibility(8192);
            } else {
                decorView.setSystemUiVisibility(0);
            }
        }
    }
}
