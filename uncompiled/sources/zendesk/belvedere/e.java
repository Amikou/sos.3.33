package zendesk.belvedere;

import android.content.Context;
import android.content.Intent;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import zendesk.belvedere.BelvedereUi;

/* compiled from: ImageStreamModel.java */
/* loaded from: classes3.dex */
public class e implements xo1 {
    public final yo1 a;
    public final List<MediaIntent> b;
    public final List<MediaResult> c;
    public final List<MediaResult> d;
    public final long e;
    public final boolean f;

    public e(Context context, BelvedereUi.UiConfig uiConfig) {
        this.a = new yo1(context);
        this.b = uiConfig.b();
        this.c = uiConfig.d();
        this.d = uiConfig.a();
        this.e = uiConfig.c();
        this.f = uiConfig.f();
    }

    @Override // defpackage.xo1
    public MediaIntent a() {
        MediaIntent k = k();
        if (k == null) {
            return null;
        }
        Intent a = k.a();
        a.setPackage("com.google.android.apps.photos");
        a.setAction("android.intent.action.GET_CONTENT");
        return k;
    }

    @Override // defpackage.xo1
    public MediaIntent b() {
        return m(2);
    }

    @Override // defpackage.xo1
    public long c() {
        return this.e;
    }

    @Override // defpackage.xo1
    public List<MediaResult> d(MediaResult mediaResult) {
        this.c.add(mediaResult);
        return this.c;
    }

    @Override // defpackage.xo1
    public boolean e() {
        return k() != null;
    }

    @Override // defpackage.xo1
    public boolean f() {
        return k() != null && this.a.a("com.google.android.apps.photos");
    }

    @Override // defpackage.xo1
    public List<MediaResult> g() {
        return n(this.a.b(500), n(this.d, this.c));
    }

    @Override // defpackage.xo1
    public boolean h() {
        return b() != null;
    }

    @Override // defpackage.xo1
    public List<MediaResult> i(MediaResult mediaResult) {
        this.c.remove(mediaResult);
        return this.c;
    }

    @Override // defpackage.xo1
    public List<MediaResult> j() {
        return this.c;
    }

    @Override // defpackage.xo1
    public MediaIntent k() {
        return m(1);
    }

    @Override // defpackage.xo1
    public boolean l() {
        return this.f;
    }

    public final MediaIntent m(int i) {
        for (MediaIntent mediaIntent : this.b) {
            if (mediaIntent.c() == i) {
                return mediaIntent;
            }
        }
        return null;
    }

    public final List<MediaResult> n(List<MediaResult> list, List<MediaResult> list2) {
        HashSet hashSet = new HashSet(list.size());
        for (MediaResult mediaResult : list) {
            hashSet.add(mediaResult.j());
        }
        ArrayList arrayList = new ArrayList(list.size() + list2.size());
        arrayList.addAll(list);
        for (int size = list2.size() - 1; size >= 0; size--) {
            MediaResult mediaResult2 = list2.get(size);
            if (!hashSet.contains(mediaResult2.j())) {
                arrayList.add(0, mediaResult2);
            }
        }
        return arrayList;
    }
}
