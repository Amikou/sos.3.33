package zendesk.belvedere;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import androidx.recyclerview.widget.RecyclerView;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import zendesk.belvedere.d;

/* compiled from: ImageStreamAdapter.java */
/* loaded from: classes3.dex */
public class c extends RecyclerView.Adapter<RecyclerView.a0> {
    public List<d.b> a = new ArrayList();
    public List<d.b> b = new ArrayList();
    public List<d.b> c = new ArrayList();

    /* compiled from: ImageStreamAdapter.java */
    /* loaded from: classes3.dex */
    public class a extends RecyclerView.a0 {
        public a(c cVar, View view) {
            super(view);
        }
    }

    /* compiled from: ImageStreamAdapter.java */
    /* loaded from: classes3.dex */
    public interface b {
        boolean a(d.b bVar);

        void b();
    }

    public void a(d.b bVar) {
        d(Collections.singletonList(bVar), this.b);
    }

    public void b(List<d.b> list) {
        d(this.a, list);
    }

    public void c(List<MediaResult> list) {
        ArrayList arrayList = new ArrayList(this.b);
        HashSet hashSet = new HashSet();
        for (MediaResult mediaResult : list) {
            hashSet.add(mediaResult.j());
        }
        for (d.b bVar : arrayList) {
            bVar.f(hashSet.contains(bVar.d().j()));
        }
        d(this.a, arrayList);
    }

    public final void d(List<d.b> list, List<d.b> list2) {
        ArrayList arrayList = new ArrayList(list.size() + list2.size());
        arrayList.addAll(list);
        arrayList.addAll(list2);
        this.a = list;
        this.b = list2;
        this.c = arrayList;
    }

    @Override // androidx.recyclerview.widget.RecyclerView.Adapter
    public int getItemCount() {
        return this.c.size();
    }

    @Override // androidx.recyclerview.widget.RecyclerView.Adapter
    public long getItemId(int i) {
        return this.c.get(i).b();
    }

    @Override // androidx.recyclerview.widget.RecyclerView.Adapter
    public int getItemViewType(int i) {
        return this.c.get(i).c();
    }

    @Override // androidx.recyclerview.widget.RecyclerView.Adapter
    public void onBindViewHolder(RecyclerView.a0 a0Var, int i) {
        this.c.get(i).a(a0Var.itemView);
    }

    @Override // androidx.recyclerview.widget.RecyclerView.Adapter
    public RecyclerView.a0 onCreateViewHolder(ViewGroup viewGroup, int i) {
        return new a(this, LayoutInflater.from(viewGroup.getContext()).inflate(i, viewGroup, false));
    }
}
