package zendesk.belvedere;

/* compiled from: L.java */
/* loaded from: classes3.dex */
public class i {
    public static b a = new a();

    /* compiled from: L.java */
    /* loaded from: classes3.dex */
    public static class a implements b {
        public boolean a = false;

        @Override // zendesk.belvedere.i.b
        public void a(String str, String str2) {
        }

        @Override // zendesk.belvedere.i.b
        public void b(String str, String str2) {
        }

        @Override // zendesk.belvedere.i.b
        public void c(String str, String str2) {
        }

        @Override // zendesk.belvedere.i.b
        public void d(String str, String str2, Throwable th) {
        }

        @Override // zendesk.belvedere.i.b
        public void e(boolean z) {
            this.a = z;
        }
    }

    /* compiled from: L.java */
    /* loaded from: classes3.dex */
    public interface b {
        void a(String str, String str2);

        void b(String str, String str2);

        void c(String str, String str2);

        void d(String str, String str2, Throwable th);

        void e(boolean z);
    }

    public static void a(String str, String str2) {
        a.c(str, str2);
    }

    public static void b(String str, String str2) {
        a.b(str, str2);
    }

    public static void c(String str, String str2, Throwable th) {
        a.d(str, str2, th);
    }

    public static void d(b bVar) {
        a = bVar;
    }

    public static void e(String str, String str2) {
        a.a(str, str2);
    }
}
