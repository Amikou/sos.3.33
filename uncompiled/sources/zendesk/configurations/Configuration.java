package zendesk.configurations;

import java.io.Serializable;
import java.util.List;

/* loaded from: classes3.dex */
public interface Configuration extends Serializable {
    List<Configuration> getConfigurations();
}
