package zendesk.commonui;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.animation.AnimatorSet;
import android.animation.ObjectAnimator;
import android.content.Context;
import android.os.Handler;
import android.os.Looper;
import android.os.Parcel;
import android.os.Parcelable;
import android.util.AttributeSet;
import android.view.View;
import android.view.animation.DecelerateInterpolator;
import android.widget.ProgressBar;
import com.github.mikephil.charting.utils.Utils;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

/* loaded from: classes3.dex */
public class AlmostRealProgressBar extends ProgressBar {
    public static final List<Step> k0 = Arrays.asList(new Step(60, 4000), new Step(90, u84.DEFAULT_POLLING_FREQUENCY));
    public c a;
    public c f0;
    public List<Step> g0;
    public Handler h0;
    public Runnable i0;
    public Runnable j0;

    /* loaded from: classes3.dex */
    public static class State extends View.BaseSavedState {
        public static final Parcelable.Creator<State> CREATOR = new a();
        public final int a;
        public final List<Step> f0;

        /* loaded from: classes3.dex */
        public class a implements Parcelable.Creator<State> {
            @Override // android.os.Parcelable.Creator
            /* renamed from: a */
            public State createFromParcel(Parcel parcel) {
                return new State(parcel, null);
            }

            @Override // android.os.Parcelable.Creator
            /* renamed from: b */
            public State[] newArray(int i) {
                return new State[i];
            }
        }

        public /* synthetic */ State(Parcel parcel, a aVar) {
            this(parcel);
        }

        @Override // android.view.View.BaseSavedState, android.view.AbsSavedState, android.os.Parcelable
        public void writeToParcel(Parcel parcel, int i) {
            super.writeToParcel(parcel, i);
            parcel.writeInt(this.a);
            parcel.writeTypedList(this.f0);
        }

        public State(Parcelable parcelable, int i, List<Step> list) {
            super(parcelable);
            this.a = i;
            this.f0 = list;
        }

        public State(Parcel parcel) {
            super(parcel);
            this.a = parcel.readInt();
            ArrayList arrayList = new ArrayList();
            this.f0 = arrayList;
            parcel.readTypedList(arrayList, Step.CREATOR);
        }
    }

    /* loaded from: classes3.dex */
    public class a implements Runnable {
        public final /* synthetic */ List a;

        public a(List list) {
            this.a = list;
        }

        @Override // java.lang.Runnable
        public void run() {
            AlmostRealProgressBar.this.j0 = null;
            List c = l10.c(this.a);
            Collections.sort(c);
            AlmostRealProgressBar.this.g0 = c;
            AlmostRealProgressBar almostRealProgressBar = AlmostRealProgressBar.this;
            almostRealProgressBar.j(almostRealProgressBar.g0, 0);
        }
    }

    /* loaded from: classes3.dex */
    public class b implements Runnable {
        public final /* synthetic */ long a;

        public b(long j) {
            this.a = j;
        }

        @Override // java.lang.Runnable
        public void run() {
            AlmostRealProgressBar.this.i0 = null;
            AlmostRealProgressBar.this.i(this.a);
        }
    }

    /* loaded from: classes3.dex */
    public static class c extends AnimatorListenerAdapter {
        public final Animator a;
        public boolean f0 = false;
        public boolean g0 = false;

        public c(Animator animator) {
            this.a = animator;
            animator.addListener(this);
        }

        public Animator a() {
            return this.a;
        }

        public boolean b() {
            return this.g0;
        }

        public boolean c() {
            return this.f0;
        }

        @Override // android.animation.AnimatorListenerAdapter, android.animation.Animator.AnimatorListener
        public void onAnimationCancel(Animator animator) {
            this.f0 = false;
            this.g0 = true;
        }

        @Override // android.animation.AnimatorListenerAdapter, android.animation.Animator.AnimatorListener
        public void onAnimationEnd(Animator animator) {
            this.f0 = false;
            this.g0 = true;
        }

        @Override // android.animation.AnimatorListenerAdapter, android.animation.Animator.AnimatorListener
        public void onAnimationRepeat(Animator animator) {
            this.f0 = true;
            this.g0 = false;
        }

        @Override // android.animation.AnimatorListenerAdapter, android.animation.Animator.AnimatorListener
        public void onAnimationStart(Animator animator) {
            this.f0 = true;
            this.g0 = false;
        }
    }

    public AlmostRealProgressBar(Context context) {
        super(context);
        this.h0 = new Handler(Looper.getMainLooper());
    }

    public final Step g(int i, int i2, Step step) {
        float f = i - i2;
        return new Step(step.a, ((float) step.f0) * (1.0f - (f / (step.a - i2))));
    }

    public final c h(long j) {
        Animator k = k(getProgress(), 100, j);
        Animator l = l(1.0f, Utils.FLOAT_EPSILON, 100L);
        Animator k2 = k(100, 0, 0L);
        Animator l2 = l(Utils.FLOAT_EPSILON, 1.0f, 0L);
        AnimatorSet animatorSet = new AnimatorSet();
        animatorSet.play(k).before(l);
        AnimatorSet animatorSet2 = new AnimatorSet();
        animatorSet2.play(k2).before(l2);
        AnimatorSet animatorSet3 = new AnimatorSet();
        animatorSet3.setDuration(j);
        animatorSet3.play(animatorSet).before(animatorSet2);
        return new c(animatorSet3);
    }

    public final void i(long j) {
        c cVar = this.a;
        if (cVar != null) {
            cVar.a().cancel();
            this.a = null;
            c h = h(j);
            this.f0 = h;
            h.a().start();
        }
    }

    public final void j(List<Step> list, int i) {
        if (this.a == null) {
            long j = 0;
            c cVar = this.f0;
            if (cVar != null && cVar.c() && !this.f0.b()) {
                j = this.f0.a().getDuration();
            }
            this.f0 = null;
            c o = o(list, i, j);
            this.a = o;
            o.a().start();
        }
    }

    public final Animator k(int i, int i2, long j) {
        ObjectAnimator ofInt = ObjectAnimator.ofInt(this, "progress", i, i2);
        ofInt.setInterpolator(new DecelerateInterpolator());
        ofInt.setDuration(j);
        return ofInt;
    }

    public final Animator l(float f, float f2, long j) {
        ObjectAnimator ofFloat = ObjectAnimator.ofFloat(this, "alpha", f, f2);
        ofFloat.setDuration(j);
        return ofFloat;
    }

    public final void m(State state) {
        if (state.a > 0) {
            ArrayList arrayList = new ArrayList(state.f0);
            ArrayList arrayList2 = new ArrayList();
            int i = 0;
            for (Step step : state.f0) {
                if (state.a >= step.a) {
                    i = step.a;
                } else {
                    arrayList2.add(step);
                }
            }
            if (l10.i(arrayList2)) {
                arrayList2.add(0, g(state.a, i, arrayList2.remove(0)));
            }
            j(arrayList2, state.a);
            this.g0 = arrayList;
            return;
        }
        setProgress(0);
    }

    public void n(List<Step> list) {
        Runnable runnable = this.i0;
        if (runnable != null) {
            this.h0.removeCallbacks(runnable);
            this.i0 = null;
        } else if (this.j0 == null) {
            a aVar = new a(list);
            this.j0 = aVar;
            this.h0.postDelayed(aVar, 100L);
        }
    }

    public final c o(List<Step> list, int i, long j) {
        ArrayList arrayList = new ArrayList(list.size());
        for (Step step : list) {
            Animator k = k(i, step.a, step.f0);
            int i2 = step.a;
            arrayList.add(k);
            i = i2;
        }
        AnimatorSet animatorSet = new AnimatorSet();
        animatorSet.playSequentially(arrayList);
        animatorSet.setStartDelay(j);
        return new c(animatorSet);
    }

    @Override // android.widget.ProgressBar, android.view.View
    public void onRestoreInstanceState(Parcelable parcelable) {
        if (parcelable instanceof State) {
            State state = (State) parcelable;
            m(state);
            parcelable = state.getSuperState();
        }
        super.onRestoreInstanceState(parcelable);
    }

    @Override // android.widget.ProgressBar, android.view.View
    public Parcelable onSaveInstanceState() {
        if (this.a != null && this.i0 == null) {
            return new State(super.onSaveInstanceState(), getProgress(), this.g0);
        }
        setProgress(0);
        return super.onSaveInstanceState();
    }

    public void p(long j) {
        Runnable runnable = this.j0;
        if (runnable != null) {
            this.h0.removeCallbacks(runnable);
            this.j0 = null;
        } else if (this.i0 == null) {
            b bVar = new b(j);
            this.i0 = bVar;
            this.h0.postDelayed(bVar, 200L);
        }
    }

    /* loaded from: classes3.dex */
    public static class Step implements Parcelable, Comparable<Step> {
        public static final Parcelable.Creator<Step> CREATOR = new a();
        public final int a;
        public final long f0;

        /* loaded from: classes3.dex */
        public class a implements Parcelable.Creator<Step> {
            @Override // android.os.Parcelable.Creator
            /* renamed from: a */
            public Step createFromParcel(Parcel parcel) {
                return new Step(parcel);
            }

            @Override // android.os.Parcelable.Creator
            /* renamed from: b */
            public Step[] newArray(int i) {
                return new Step[i];
            }
        }

        public Step(int i, long j) {
            this.a = i;
            this.f0 = j;
        }

        @Override // android.os.Parcelable
        public int describeContents() {
            return 0;
        }

        @Override // java.lang.Comparable
        /* renamed from: e */
        public int compareTo(Step step) {
            return this.a - step.a;
        }

        @Override // android.os.Parcelable
        public void writeToParcel(Parcel parcel, int i) {
            parcel.writeInt(this.a);
            parcel.writeLong(this.f0);
        }

        public Step(Parcel parcel) {
            this.a = parcel.readInt();
            this.f0 = parcel.readLong();
        }
    }

    public AlmostRealProgressBar(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        this.h0 = new Handler(Looper.getMainLooper());
    }

    public AlmostRealProgressBar(Context context, AttributeSet attributeSet, int i) {
        super(context, attributeSet, i);
        this.h0 = new Handler(Looper.getMainLooper());
    }
}
