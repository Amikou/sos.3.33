package online.osslab;

import android.animation.ObjectAnimator;
import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.RectF;
import android.util.AttributeSet;
import android.view.View;
import android.view.animation.DecelerateInterpolator;
import com.github.mikephil.charting.utils.Utils;

/* loaded from: classes2.dex */
public class CircleProgressBar extends View {
    public float a;
    public float f0;
    public float g0;
    public int h0;
    public int i0;
    public int j0;
    public RectF k0;
    public Paint l0;
    public Paint m0;

    public CircleProgressBar(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        this.a = Utils.FLOAT_EPSILON;
        this.f0 = getResources().getDimension(az2.default_stroke_width);
        this.g0 = getResources().getDimension(az2.default_background_stroke_width);
        this.h0 = -16777216;
        this.i0 = -7829368;
        this.j0 = -90;
        a(context, attributeSet);
    }

    public final void a(Context context, AttributeSet attributeSet) {
        this.k0 = new RectF();
        TypedArray obtainStyledAttributes = context.getTheme().obtainStyledAttributes(attributeSet, u23.CircleProgressBar, 0, 0);
        try {
            this.a = obtainStyledAttributes.getFloat(u23.CircleProgressBar_progress_value, this.a);
            this.f0 = obtainStyledAttributes.getDimension(u23.CircleProgressBar_progress_width, this.f0);
            this.g0 = obtainStyledAttributes.getDimension(u23.CircleProgressBar_background_width, this.g0);
            this.h0 = obtainStyledAttributes.getInt(u23.CircleProgressBar_progress_color, this.h0);
            this.i0 = obtainStyledAttributes.getInt(u23.CircleProgressBar_background_color, this.i0);
            obtainStyledAttributes.recycle();
            Paint paint = new Paint(1);
            this.l0 = paint;
            paint.setColor(this.i0);
            this.l0.setStyle(Paint.Style.STROKE);
            this.l0.setStrokeWidth(this.g0);
            Paint paint2 = new Paint(1);
            this.m0 = paint2;
            paint2.setColor(this.h0);
            this.m0.setStyle(Paint.Style.STROKE);
            this.m0.setStrokeWidth(this.f0);
        } catch (Throwable th) {
            obtainStyledAttributes.recycle();
            throw th;
        }
    }

    public int getBackgroundColor() {
        return this.i0;
    }

    public float getBackgroundProgressBarWidth() {
        return this.g0;
    }

    public int getColor() {
        return this.h0;
    }

    public float getProgress() {
        return this.a;
    }

    public float getProgressBarWidth() {
        return this.f0;
    }

    @Override // android.view.View
    public void onDraw(Canvas canvas) {
        super.onDraw(canvas);
        canvas.drawOval(this.k0, this.l0);
        canvas.drawArc(this.k0, this.j0, (this.a * 360.0f) / 100.0f, false, this.m0);
    }

    @Override // android.view.View
    public void onMeasure(int i, int i2) {
        int min = Math.min(View.getDefaultSize(getSuggestedMinimumWidth(), i), View.getDefaultSize(getSuggestedMinimumHeight(), i2));
        setMeasuredDimension(min, min);
        float f = this.f0;
        float f2 = this.g0;
        if (f <= f2) {
            f = f2;
        }
        RectF rectF = this.k0;
        float f3 = f / 2.0f;
        float f4 = Utils.FLOAT_EPSILON + f3;
        float f5 = min - f3;
        rectF.set(f4, f4, f5, f5);
    }

    @Override // android.view.View
    public void setBackgroundColor(int i) {
        this.i0 = i;
        this.l0.setColor(i);
        invalidate();
        requestLayout();
    }

    public void setBackgroundProgressBarWidth(float f) {
        this.g0 = f;
        this.l0.setStrokeWidth(f);
        requestLayout();
        invalidate();
    }

    public void setColor(int i) {
        this.h0 = i;
        this.m0.setColor(i);
        invalidate();
        requestLayout();
    }

    public void setProgress(float f) {
        if (f > 100.0f) {
            f = 100.0f;
        }
        this.a = f;
        invalidate();
    }

    public void setProgressBarWidth(float f) {
        this.f0 = f;
        this.m0.setStrokeWidth(f);
        requestLayout();
        invalidate();
    }

    public void setProgressWithAnimation(float f) {
        setProgressWithAnimation(f, 1500);
    }

    public void setProgressWithAnimation(float f, int i) {
        ObjectAnimator ofFloat = ObjectAnimator.ofFloat(this, "progress", f);
        ofFloat.setDuration(i);
        ofFloat.setInterpolator(new DecelerateInterpolator());
        ofFloat.start();
    }
}
