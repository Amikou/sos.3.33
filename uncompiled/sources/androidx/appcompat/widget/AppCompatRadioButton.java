package androidx.appcompat.widget;

import android.content.Context;
import android.content.res.ColorStateList;
import android.graphics.PorterDuff;
import android.graphics.drawable.Drawable;
import android.util.AttributeSet;
import android.widget.RadioButton;

/* loaded from: classes.dex */
public class AppCompatRadioButton extends RadioButton implements n64, m64 {
    public final df a;
    public final bf f0;
    public final a g0;

    public AppCompatRadioButton(Context context) {
        this(context, null);
    }

    @Override // android.widget.CompoundButton, android.widget.TextView, android.view.View
    public void drawableStateChanged() {
        super.drawableStateChanged();
        bf bfVar = this.f0;
        if (bfVar != null) {
            bfVar.b();
        }
        a aVar = this.g0;
        if (aVar != null) {
            aVar.b();
        }
    }

    @Override // android.widget.CompoundButton, android.widget.TextView
    public int getCompoundPaddingLeft() {
        int compoundPaddingLeft = super.getCompoundPaddingLeft();
        df dfVar = this.a;
        return dfVar != null ? dfVar.b(compoundPaddingLeft) : compoundPaddingLeft;
    }

    @Override // defpackage.m64
    public ColorStateList getSupportBackgroundTintList() {
        bf bfVar = this.f0;
        if (bfVar != null) {
            return bfVar.c();
        }
        return null;
    }

    @Override // defpackage.m64
    public PorterDuff.Mode getSupportBackgroundTintMode() {
        bf bfVar = this.f0;
        if (bfVar != null) {
            return bfVar.d();
        }
        return null;
    }

    @Override // defpackage.n64
    public ColorStateList getSupportButtonTintList() {
        df dfVar = this.a;
        if (dfVar != null) {
            return dfVar.c();
        }
        return null;
    }

    public PorterDuff.Mode getSupportButtonTintMode() {
        df dfVar = this.a;
        if (dfVar != null) {
            return dfVar.d();
        }
        return null;
    }

    @Override // android.view.View
    public void setBackgroundDrawable(Drawable drawable) {
        super.setBackgroundDrawable(drawable);
        bf bfVar = this.f0;
        if (bfVar != null) {
            bfVar.f(drawable);
        }
    }

    @Override // android.view.View
    public void setBackgroundResource(int i) {
        super.setBackgroundResource(i);
        bf bfVar = this.f0;
        if (bfVar != null) {
            bfVar.g(i);
        }
    }

    @Override // android.widget.CompoundButton
    public void setButtonDrawable(Drawable drawable) {
        super.setButtonDrawable(drawable);
        df dfVar = this.a;
        if (dfVar != null) {
            dfVar.f();
        }
    }

    @Override // defpackage.m64
    public void setSupportBackgroundTintList(ColorStateList colorStateList) {
        bf bfVar = this.f0;
        if (bfVar != null) {
            bfVar.i(colorStateList);
        }
    }

    @Override // defpackage.m64
    public void setSupportBackgroundTintMode(PorterDuff.Mode mode) {
        bf bfVar = this.f0;
        if (bfVar != null) {
            bfVar.j(mode);
        }
    }

    @Override // defpackage.n64
    public void setSupportButtonTintList(ColorStateList colorStateList) {
        df dfVar = this.a;
        if (dfVar != null) {
            dfVar.g(colorStateList);
        }
    }

    @Override // defpackage.n64
    public void setSupportButtonTintMode(PorterDuff.Mode mode) {
        df dfVar = this.a;
        if (dfVar != null) {
            dfVar.h(mode);
        }
    }

    public AppCompatRadioButton(Context context, AttributeSet attributeSet) {
        this(context, attributeSet, jy2.radioButtonStyle);
    }

    public AppCompatRadioButton(Context context, AttributeSet attributeSet, int i) {
        super(j64.b(context), attributeSet, i);
        b54.a(this, getContext());
        df dfVar = new df(this);
        this.a = dfVar;
        dfVar.e(attributeSet, i);
        bf bfVar = new bf(this);
        this.f0 = bfVar;
        bfVar.e(attributeSet, i);
        a aVar = new a(this);
        this.g0 = aVar;
        aVar.m(attributeSet, i);
    }

    @Override // android.widget.CompoundButton
    public void setButtonDrawable(int i) {
        setButtonDrawable(mf.d(getContext(), i));
    }
}
