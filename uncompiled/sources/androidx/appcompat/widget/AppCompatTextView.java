package androidx.appcompat.widget;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.res.ColorStateList;
import android.graphics.PorterDuff;
import android.graphics.Typeface;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.util.AttributeSet;
import android.view.ActionMode;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputConnection;
import android.view.textclassifier.TextClassifier;
import android.widget.TextView;
import defpackage.vt2;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Future;

/* loaded from: classes.dex */
public class AppCompatTextView extends TextView implements m64, o64, jk {
    private final bf mBackgroundTintHelper;
    private boolean mIsSetTypefaceProcessing;
    private Future<vt2> mPrecomputedTextFuture;
    private final of mTextClassifierHelper;
    private final a mTextHelper;

    public AppCompatTextView(Context context) {
        this(context, null);
    }

    private void consumeTextFutureAndSetBlocking() {
        Future<vt2> future = this.mPrecomputedTextFuture;
        if (future != null) {
            try {
                this.mPrecomputedTextFuture = null;
                t44.p(this, future.get());
            } catch (InterruptedException | ExecutionException unused) {
            }
        }
    }

    @Override // android.widget.TextView, android.view.View
    public void drawableStateChanged() {
        super.drawableStateChanged();
        bf bfVar = this.mBackgroundTintHelper;
        if (bfVar != null) {
            bfVar.b();
        }
        a aVar = this.mTextHelper;
        if (aVar != null) {
            aVar.b();
        }
    }

    @Override // android.widget.TextView
    public int getAutoSizeMaxTextSize() {
        if (jk.b) {
            return super.getAutoSizeMaxTextSize();
        }
        a aVar = this.mTextHelper;
        if (aVar != null) {
            return aVar.e();
        }
        return -1;
    }

    @Override // android.widget.TextView
    public int getAutoSizeMinTextSize() {
        if (jk.b) {
            return super.getAutoSizeMinTextSize();
        }
        a aVar = this.mTextHelper;
        if (aVar != null) {
            return aVar.f();
        }
        return -1;
    }

    @Override // android.widget.TextView
    public int getAutoSizeStepGranularity() {
        if (jk.b) {
            return super.getAutoSizeStepGranularity();
        }
        a aVar = this.mTextHelper;
        if (aVar != null) {
            return aVar.g();
        }
        return -1;
    }

    @Override // android.widget.TextView
    public int[] getAutoSizeTextAvailableSizes() {
        if (jk.b) {
            return super.getAutoSizeTextAvailableSizes();
        }
        a aVar = this.mTextHelper;
        return aVar != null ? aVar.h() : new int[0];
    }

    @Override // android.widget.TextView
    @SuppressLint({"WrongConstant"})
    public int getAutoSizeTextType() {
        if (jk.b) {
            return super.getAutoSizeTextType() == 1 ? 1 : 0;
        }
        a aVar = this.mTextHelper;
        if (aVar != null) {
            return aVar.i();
        }
        return 0;
    }

    @Override // android.widget.TextView
    public int getFirstBaselineToTopHeight() {
        return t44.b(this);
    }

    @Override // android.widget.TextView
    public int getLastBaselineToBottomHeight() {
        return t44.c(this);
    }

    @Override // defpackage.m64
    public ColorStateList getSupportBackgroundTintList() {
        bf bfVar = this.mBackgroundTintHelper;
        if (bfVar != null) {
            return bfVar.c();
        }
        return null;
    }

    @Override // defpackage.m64
    public PorterDuff.Mode getSupportBackgroundTintMode() {
        bf bfVar = this.mBackgroundTintHelper;
        if (bfVar != null) {
            return bfVar.d();
        }
        return null;
    }

    public ColorStateList getSupportCompoundDrawablesTintList() {
        return this.mTextHelper.j();
    }

    public PorterDuff.Mode getSupportCompoundDrawablesTintMode() {
        return this.mTextHelper.k();
    }

    @Override // android.widget.TextView
    public CharSequence getText() {
        consumeTextFutureAndSetBlocking();
        return super.getText();
    }

    @Override // android.widget.TextView
    public TextClassifier getTextClassifier() {
        of ofVar;
        if (Build.VERSION.SDK_INT < 28 && (ofVar = this.mTextClassifierHelper) != null) {
            return ofVar.a();
        }
        return super.getTextClassifier();
    }

    public vt2.a getTextMetricsParamsCompat() {
        return t44.g(this);
    }

    @Override // android.widget.TextView, android.view.View
    public InputConnection onCreateInputConnection(EditorInfo editorInfo) {
        InputConnection onCreateInputConnection = super.onCreateInputConnection(editorInfo);
        this.mTextHelper.r(this, onCreateInputConnection, editorInfo);
        return hf.a(onCreateInputConnection, editorInfo, this);
    }

    @Override // android.widget.TextView, android.view.View
    public void onLayout(boolean z, int i, int i2, int i3, int i4) {
        super.onLayout(z, i, i2, i3, i4);
        a aVar = this.mTextHelper;
        if (aVar != null) {
            aVar.o(z, i, i2, i3, i4);
        }
    }

    @Override // android.widget.TextView, android.view.View
    public void onMeasure(int i, int i2) {
        consumeTextFutureAndSetBlocking();
        super.onMeasure(i, i2);
    }

    @Override // android.widget.TextView
    public void onTextChanged(CharSequence charSequence, int i, int i2, int i3) {
        super.onTextChanged(charSequence, i, i2, i3);
        a aVar = this.mTextHelper;
        if (aVar == null || jk.b || !aVar.l()) {
            return;
        }
        this.mTextHelper.c();
    }

    @Override // android.widget.TextView
    public void setAutoSizeTextTypeUniformWithConfiguration(int i, int i2, int i3, int i4) throws IllegalArgumentException {
        if (jk.b) {
            super.setAutoSizeTextTypeUniformWithConfiguration(i, i2, i3, i4);
            return;
        }
        a aVar = this.mTextHelper;
        if (aVar != null) {
            aVar.t(i, i2, i3, i4);
        }
    }

    @Override // android.widget.TextView
    public void setAutoSizeTextTypeUniformWithPresetSizes(int[] iArr, int i) throws IllegalArgumentException {
        if (jk.b) {
            super.setAutoSizeTextTypeUniformWithPresetSizes(iArr, i);
            return;
        }
        a aVar = this.mTextHelper;
        if (aVar != null) {
            aVar.u(iArr, i);
        }
    }

    @Override // android.widget.TextView
    public void setAutoSizeTextTypeWithDefaults(int i) {
        if (jk.b) {
            super.setAutoSizeTextTypeWithDefaults(i);
            return;
        }
        a aVar = this.mTextHelper;
        if (aVar != null) {
            aVar.v(i);
        }
    }

    @Override // android.view.View
    public void setBackgroundDrawable(Drawable drawable) {
        super.setBackgroundDrawable(drawable);
        bf bfVar = this.mBackgroundTintHelper;
        if (bfVar != null) {
            bfVar.f(drawable);
        }
    }

    @Override // android.view.View
    public void setBackgroundResource(int i) {
        super.setBackgroundResource(i);
        bf bfVar = this.mBackgroundTintHelper;
        if (bfVar != null) {
            bfVar.g(i);
        }
    }

    @Override // android.widget.TextView
    public void setCompoundDrawables(Drawable drawable, Drawable drawable2, Drawable drawable3, Drawable drawable4) {
        super.setCompoundDrawables(drawable, drawable2, drawable3, drawable4);
        a aVar = this.mTextHelper;
        if (aVar != null) {
            aVar.p();
        }
    }

    @Override // android.widget.TextView
    public void setCompoundDrawablesRelative(Drawable drawable, Drawable drawable2, Drawable drawable3, Drawable drawable4) {
        super.setCompoundDrawablesRelative(drawable, drawable2, drawable3, drawable4);
        a aVar = this.mTextHelper;
        if (aVar != null) {
            aVar.p();
        }
    }

    @Override // android.widget.TextView
    public void setCompoundDrawablesRelativeWithIntrinsicBounds(Drawable drawable, Drawable drawable2, Drawable drawable3, Drawable drawable4) {
        super.setCompoundDrawablesRelativeWithIntrinsicBounds(drawable, drawable2, drawable3, drawable4);
        a aVar = this.mTextHelper;
        if (aVar != null) {
            aVar.p();
        }
    }

    @Override // android.widget.TextView
    public void setCompoundDrawablesWithIntrinsicBounds(Drawable drawable, Drawable drawable2, Drawable drawable3, Drawable drawable4) {
        super.setCompoundDrawablesWithIntrinsicBounds(drawable, drawable2, drawable3, drawable4);
        a aVar = this.mTextHelper;
        if (aVar != null) {
            aVar.p();
        }
    }

    @Override // android.widget.TextView
    public void setCustomSelectionActionModeCallback(ActionMode.Callback callback) {
        super.setCustomSelectionActionModeCallback(t44.s(this, callback));
    }

    @Override // android.widget.TextView
    public void setFirstBaselineToTopHeight(int i) {
        if (Build.VERSION.SDK_INT >= 28) {
            super.setFirstBaselineToTopHeight(i);
        } else {
            t44.m(this, i);
        }
    }

    @Override // android.widget.TextView
    public void setLastBaselineToBottomHeight(int i) {
        if (Build.VERSION.SDK_INT >= 28) {
            super.setLastBaselineToBottomHeight(i);
        } else {
            t44.n(this, i);
        }
    }

    @Override // android.widget.TextView
    public void setLineHeight(int i) {
        t44.o(this, i);
    }

    public void setPrecomputedText(vt2 vt2Var) {
        t44.p(this, vt2Var);
    }

    @Override // defpackage.m64
    public void setSupportBackgroundTintList(ColorStateList colorStateList) {
        bf bfVar = this.mBackgroundTintHelper;
        if (bfVar != null) {
            bfVar.i(colorStateList);
        }
    }

    @Override // defpackage.m64
    public void setSupportBackgroundTintMode(PorterDuff.Mode mode) {
        bf bfVar = this.mBackgroundTintHelper;
        if (bfVar != null) {
            bfVar.j(mode);
        }
    }

    @Override // defpackage.o64
    public void setSupportCompoundDrawablesTintList(ColorStateList colorStateList) {
        this.mTextHelper.w(colorStateList);
        this.mTextHelper.b();
    }

    @Override // defpackage.o64
    public void setSupportCompoundDrawablesTintMode(PorterDuff.Mode mode) {
        this.mTextHelper.x(mode);
        this.mTextHelper.b();
    }

    @Override // android.widget.TextView
    public void setTextAppearance(Context context, int i) {
        super.setTextAppearance(context, i);
        a aVar = this.mTextHelper;
        if (aVar != null) {
            aVar.q(context, i);
        }
    }

    @Override // android.widget.TextView
    public void setTextClassifier(TextClassifier textClassifier) {
        of ofVar;
        if (Build.VERSION.SDK_INT < 28 && (ofVar = this.mTextClassifierHelper) != null) {
            ofVar.b(textClassifier);
        } else {
            super.setTextClassifier(textClassifier);
        }
    }

    public void setTextFuture(Future<vt2> future) {
        this.mPrecomputedTextFuture = future;
        if (future != null) {
            requestLayout();
        }
    }

    public void setTextMetricsParamsCompat(vt2.a aVar) {
        t44.r(this, aVar);
    }

    @Override // android.widget.TextView
    public void setTextSize(int i, float f) {
        if (jk.b) {
            super.setTextSize(i, f);
            return;
        }
        a aVar = this.mTextHelper;
        if (aVar != null) {
            aVar.A(i, f);
        }
    }

    @Override // android.widget.TextView
    public void setTypeface(Typeface typeface, int i) {
        if (this.mIsSetTypefaceProcessing) {
            return;
        }
        Typeface typeface2 = null;
        if (typeface != null && i > 0) {
            typeface2 = yd4.a(getContext(), typeface, i);
        }
        this.mIsSetTypefaceProcessing = true;
        if (typeface2 != null) {
            typeface = typeface2;
        }
        try {
            super.setTypeface(typeface, i);
        } finally {
            this.mIsSetTypefaceProcessing = false;
        }
    }

    public AppCompatTextView(Context context, AttributeSet attributeSet) {
        this(context, attributeSet, 16842884);
    }

    public AppCompatTextView(Context context, AttributeSet attributeSet, int i) {
        super(j64.b(context), attributeSet, i);
        this.mIsSetTypefaceProcessing = false;
        b54.a(this, getContext());
        bf bfVar = new bf(this);
        this.mBackgroundTintHelper = bfVar;
        bfVar.e(attributeSet, i);
        a aVar = new a(this);
        this.mTextHelper = aVar;
        aVar.m(attributeSet, i);
        aVar.b();
        this.mTextClassifierHelper = new of(this);
    }

    @Override // android.widget.TextView
    public void setCompoundDrawablesRelativeWithIntrinsicBounds(int i, int i2, int i3, int i4) {
        Context context = getContext();
        setCompoundDrawablesRelativeWithIntrinsicBounds(i != 0 ? mf.d(context, i) : null, i2 != 0 ? mf.d(context, i2) : null, i3 != 0 ? mf.d(context, i3) : null, i4 != 0 ? mf.d(context, i4) : null);
        a aVar = this.mTextHelper;
        if (aVar != null) {
            aVar.p();
        }
    }

    @Override // android.widget.TextView
    public void setCompoundDrawablesWithIntrinsicBounds(int i, int i2, int i3, int i4) {
        Context context = getContext();
        setCompoundDrawablesWithIntrinsicBounds(i != 0 ? mf.d(context, i) : null, i2 != 0 ? mf.d(context, i2) : null, i3 != 0 ? mf.d(context, i3) : null, i4 != 0 ? mf.d(context, i4) : null);
        a aVar = this.mTextHelper;
        if (aVar != null) {
            aVar.p();
        }
    }
}
