package androidx.appcompat.widget;

import android.content.Context;
import android.content.res.ColorStateList;
import android.graphics.PorterDuff;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.text.Editable;
import android.util.AttributeSet;
import android.view.ActionMode;
import android.view.DragEvent;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputConnection;
import android.view.textclassifier.TextClassifier;
import android.widget.EditText;

/* loaded from: classes.dex */
public class AppCompatEditText extends EditText implements m64, rm2 {
    public final bf a;
    public final a f0;
    public final of g0;
    public final u44 h0;

    public AppCompatEditText(Context context) {
        this(context, null);
    }

    @Override // defpackage.rm2
    public f70 a(f70 f70Var) {
        return this.h0.a(this, f70Var);
    }

    @Override // android.widget.TextView, android.view.View
    public void drawableStateChanged() {
        super.drawableStateChanged();
        bf bfVar = this.a;
        if (bfVar != null) {
            bfVar.b();
        }
        a aVar = this.f0;
        if (aVar != null) {
            aVar.b();
        }
    }

    @Override // defpackage.m64
    public ColorStateList getSupportBackgroundTintList() {
        bf bfVar = this.a;
        if (bfVar != null) {
            return bfVar.c();
        }
        return null;
    }

    @Override // defpackage.m64
    public PorterDuff.Mode getSupportBackgroundTintMode() {
        bf bfVar = this.a;
        if (bfVar != null) {
            return bfVar.d();
        }
        return null;
    }

    @Override // android.widget.TextView
    public TextClassifier getTextClassifier() {
        of ofVar;
        if (Build.VERSION.SDK_INT < 28 && (ofVar = this.g0) != null) {
            return ofVar.a();
        }
        return super.getTextClassifier();
    }

    @Override // android.widget.TextView, android.view.View
    public InputConnection onCreateInputConnection(EditorInfo editorInfo) {
        InputConnection onCreateInputConnection = super.onCreateInputConnection(editorInfo);
        this.f0.r(this, onCreateInputConnection, editorInfo);
        InputConnection a = hf.a(onCreateInputConnection, editorInfo, this);
        String[] H = ei4.H(this);
        if (a == null || H == null) {
            return a;
        }
        hu0.d(editorInfo, H);
        return vq1.a(a, editorInfo, lf.a(this));
    }

    @Override // android.widget.TextView, android.view.View
    public boolean onDragEvent(DragEvent dragEvent) {
        if (lf.b(this, dragEvent)) {
            return true;
        }
        return super.onDragEvent(dragEvent);
    }

    @Override // android.widget.TextView
    public boolean onTextContextMenuItem(int i) {
        if (lf.c(this, i)) {
            return true;
        }
        return super.onTextContextMenuItem(i);
    }

    @Override // android.view.View
    public void setBackgroundDrawable(Drawable drawable) {
        super.setBackgroundDrawable(drawable);
        bf bfVar = this.a;
        if (bfVar != null) {
            bfVar.f(drawable);
        }
    }

    @Override // android.view.View
    public void setBackgroundResource(int i) {
        super.setBackgroundResource(i);
        bf bfVar = this.a;
        if (bfVar != null) {
            bfVar.g(i);
        }
    }

    @Override // android.widget.TextView
    public void setCustomSelectionActionModeCallback(ActionMode.Callback callback) {
        super.setCustomSelectionActionModeCallback(t44.s(this, callback));
    }

    @Override // defpackage.m64
    public void setSupportBackgroundTintList(ColorStateList colorStateList) {
        bf bfVar = this.a;
        if (bfVar != null) {
            bfVar.i(colorStateList);
        }
    }

    @Override // defpackage.m64
    public void setSupportBackgroundTintMode(PorterDuff.Mode mode) {
        bf bfVar = this.a;
        if (bfVar != null) {
            bfVar.j(mode);
        }
    }

    @Override // android.widget.TextView
    public void setTextAppearance(Context context, int i) {
        super.setTextAppearance(context, i);
        a aVar = this.f0;
        if (aVar != null) {
            aVar.q(context, i);
        }
    }

    @Override // android.widget.TextView
    public void setTextClassifier(TextClassifier textClassifier) {
        of ofVar;
        if (Build.VERSION.SDK_INT < 28 && (ofVar = this.g0) != null) {
            ofVar.b(textClassifier);
        } else {
            super.setTextClassifier(textClassifier);
        }
    }

    public AppCompatEditText(Context context, AttributeSet attributeSet) {
        this(context, attributeSet, jy2.editTextStyle);
    }

    @Override // android.widget.EditText, android.widget.TextView
    public Editable getText() {
        if (Build.VERSION.SDK_INT >= 28) {
            return super.getText();
        }
        return super.getEditableText();
    }

    public AppCompatEditText(Context context, AttributeSet attributeSet, int i) {
        super(j64.b(context), attributeSet, i);
        b54.a(this, getContext());
        bf bfVar = new bf(this);
        this.a = bfVar;
        bfVar.e(attributeSet, i);
        a aVar = new a(this);
        this.f0 = aVar;
        aVar.m(attributeSet, i);
        aVar.b();
        this.g0 = new of(this);
        this.h0 = new u44();
    }
}
