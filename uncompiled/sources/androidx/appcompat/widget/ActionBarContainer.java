package androidx.appcompat.widget;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.util.AttributeSet;
import android.view.ActionMode;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;

/* loaded from: classes.dex */
public class ActionBarContainer extends FrameLayout {
    public boolean a;
    public View f0;
    public View g0;
    public View h0;
    public Drawable i0;
    public Drawable j0;
    public Drawable k0;
    public boolean l0;
    public boolean m0;
    public int n0;

    public ActionBarContainer(Context context) {
        this(context, null);
    }

    public final int a(View view) {
        FrameLayout.LayoutParams layoutParams = (FrameLayout.LayoutParams) view.getLayoutParams();
        return view.getMeasuredHeight() + layoutParams.topMargin + layoutParams.bottomMargin;
    }

    public final boolean b(View view) {
        return view == null || view.getVisibility() == 8 || view.getMeasuredHeight() == 0;
    }

    @Override // android.view.ViewGroup, android.view.View
    public void drawableStateChanged() {
        super.drawableStateChanged();
        Drawable drawable = this.i0;
        if (drawable != null && drawable.isStateful()) {
            this.i0.setState(getDrawableState());
        }
        Drawable drawable2 = this.j0;
        if (drawable2 != null && drawable2.isStateful()) {
            this.j0.setState(getDrawableState());
        }
        Drawable drawable3 = this.k0;
        if (drawable3 == null || !drawable3.isStateful()) {
            return;
        }
        this.k0.setState(getDrawableState());
    }

    public View getTabContainer() {
        return this.f0;
    }

    @Override // android.view.ViewGroup, android.view.View
    public void jumpDrawablesToCurrentState() {
        super.jumpDrawablesToCurrentState();
        Drawable drawable = this.i0;
        if (drawable != null) {
            drawable.jumpToCurrentState();
        }
        Drawable drawable2 = this.j0;
        if (drawable2 != null) {
            drawable2.jumpToCurrentState();
        }
        Drawable drawable3 = this.k0;
        if (drawable3 != null) {
            drawable3.jumpToCurrentState();
        }
    }

    @Override // android.view.View
    public void onFinishInflate() {
        super.onFinishInflate();
        this.g0 = findViewById(q03.action_bar);
        this.h0 = findViewById(q03.action_context_bar);
    }

    @Override // android.view.View
    public boolean onHoverEvent(MotionEvent motionEvent) {
        super.onHoverEvent(motionEvent);
        return true;
    }

    @Override // android.view.ViewGroup
    public boolean onInterceptTouchEvent(MotionEvent motionEvent) {
        return this.a || super.onInterceptTouchEvent(motionEvent);
    }

    @Override // android.widget.FrameLayout, android.view.ViewGroup, android.view.View
    public void onLayout(boolean z, int i, int i2, int i3, int i4) {
        Drawable drawable;
        super.onLayout(z, i, i2, i3, i4);
        View view = this.f0;
        boolean z2 = true;
        boolean z3 = false;
        boolean z4 = (view == null || view.getVisibility() == 8) ? false : true;
        if (view != null && view.getVisibility() != 8) {
            int measuredHeight = getMeasuredHeight();
            int i5 = ((FrameLayout.LayoutParams) view.getLayoutParams()).bottomMargin;
            view.layout(i, (measuredHeight - view.getMeasuredHeight()) - i5, i3, measuredHeight - i5);
        }
        if (this.l0) {
            Drawable drawable2 = this.k0;
            if (drawable2 != null) {
                drawable2.setBounds(0, 0, getMeasuredWidth(), getMeasuredHeight());
            }
            z2 = z3;
        } else {
            if (this.i0 != null) {
                if (this.g0.getVisibility() == 0) {
                    this.i0.setBounds(this.g0.getLeft(), this.g0.getTop(), this.g0.getRight(), this.g0.getBottom());
                } else {
                    View view2 = this.h0;
                    if (view2 != null && view2.getVisibility() == 0) {
                        this.i0.setBounds(this.h0.getLeft(), this.h0.getTop(), this.h0.getRight(), this.h0.getBottom());
                    } else {
                        this.i0.setBounds(0, 0, 0, 0);
                    }
                }
                z3 = true;
            }
            this.m0 = z4;
            if (z4 && (drawable = this.j0) != null) {
                drawable.setBounds(view.getLeft(), view.getTop(), view.getRight(), view.getBottom());
            }
            z2 = z3;
        }
        if (z2) {
            invalidate();
        }
    }

    @Override // android.widget.FrameLayout, android.view.View
    public void onMeasure(int i, int i2) {
        int a;
        int i3;
        if (this.g0 == null && View.MeasureSpec.getMode(i2) == Integer.MIN_VALUE && (i3 = this.n0) >= 0) {
            i2 = View.MeasureSpec.makeMeasureSpec(Math.min(i3, View.MeasureSpec.getSize(i2)), Integer.MIN_VALUE);
        }
        super.onMeasure(i, i2);
        if (this.g0 == null) {
            return;
        }
        int mode = View.MeasureSpec.getMode(i2);
        View view = this.f0;
        if (view == null || view.getVisibility() == 8 || mode == 1073741824) {
            return;
        }
        if (!b(this.g0)) {
            a = a(this.g0);
        } else {
            a = !b(this.h0) ? a(this.h0) : 0;
        }
        setMeasuredDimension(getMeasuredWidth(), Math.min(a + a(this.f0), mode == Integer.MIN_VALUE ? View.MeasureSpec.getSize(i2) : Integer.MAX_VALUE));
    }

    @Override // android.view.View
    public boolean onTouchEvent(MotionEvent motionEvent) {
        super.onTouchEvent(motionEvent);
        return true;
    }

    public void setPrimaryBackground(Drawable drawable) {
        Drawable drawable2 = this.i0;
        if (drawable2 != null) {
            drawable2.setCallback(null);
            unscheduleDrawable(this.i0);
        }
        this.i0 = drawable;
        if (drawable != null) {
            drawable.setCallback(this);
            View view = this.g0;
            if (view != null) {
                this.i0.setBounds(view.getLeft(), this.g0.getTop(), this.g0.getRight(), this.g0.getBottom());
            }
        }
        boolean z = true;
        if (!this.l0 ? this.i0 != null || this.j0 != null : this.k0 != null) {
            z = false;
        }
        setWillNotDraw(z);
        invalidate();
        if (Build.VERSION.SDK_INT >= 21) {
            invalidateOutline();
        }
    }

    public void setSplitBackground(Drawable drawable) {
        Drawable drawable2;
        Drawable drawable3 = this.k0;
        if (drawable3 != null) {
            drawable3.setCallback(null);
            unscheduleDrawable(this.k0);
        }
        this.k0 = drawable;
        boolean z = false;
        if (drawable != null) {
            drawable.setCallback(this);
            if (this.l0 && (drawable2 = this.k0) != null) {
                drawable2.setBounds(0, 0, getMeasuredWidth(), getMeasuredHeight());
            }
        }
        if (!this.l0 ? !(this.i0 != null || this.j0 != null) : this.k0 == null) {
            z = true;
        }
        setWillNotDraw(z);
        invalidate();
        if (Build.VERSION.SDK_INT >= 21) {
            invalidateOutline();
        }
    }

    public void setStackedBackground(Drawable drawable) {
        Drawable drawable2;
        Drawable drawable3 = this.j0;
        if (drawable3 != null) {
            drawable3.setCallback(null);
            unscheduleDrawable(this.j0);
        }
        this.j0 = drawable;
        if (drawable != null) {
            drawable.setCallback(this);
            if (this.m0 && (drawable2 = this.j0) != null) {
                drawable2.setBounds(this.f0.getLeft(), this.f0.getTop(), this.f0.getRight(), this.f0.getBottom());
            }
        }
        boolean z = true;
        if (!this.l0 ? this.i0 != null || this.j0 != null : this.k0 != null) {
            z = false;
        }
        setWillNotDraw(z);
        invalidate();
        if (Build.VERSION.SDK_INT >= 21) {
            invalidateOutline();
        }
    }

    public void setTabContainer(ScrollingTabContainerView scrollingTabContainerView) {
        View view = this.f0;
        if (view != null) {
            removeView(view);
        }
        this.f0 = scrollingTabContainerView;
        if (scrollingTabContainerView != null) {
            addView(scrollingTabContainerView);
            ViewGroup.LayoutParams layoutParams = scrollingTabContainerView.getLayoutParams();
            layoutParams.width = -1;
            layoutParams.height = -2;
            scrollingTabContainerView.setAllowCollapse(false);
        }
    }

    public void setTransitioning(boolean z) {
        this.a = z;
        setDescendantFocusability(z ? 393216 : 262144);
    }

    @Override // android.view.View
    public void setVisibility(int i) {
        super.setVisibility(i);
        boolean z = i == 0;
        Drawable drawable = this.i0;
        if (drawable != null) {
            drawable.setVisible(z, false);
        }
        Drawable drawable2 = this.j0;
        if (drawable2 != null) {
            drawable2.setVisible(z, false);
        }
        Drawable drawable3 = this.k0;
        if (drawable3 != null) {
            drawable3.setVisible(z, false);
        }
    }

    @Override // android.view.ViewGroup, android.view.ViewParent
    public ActionMode startActionModeForChild(View view, ActionMode.Callback callback) {
        return null;
    }

    @Override // android.view.ViewGroup, android.view.ViewParent
    public ActionMode startActionModeForChild(View view, ActionMode.Callback callback, int i) {
        if (i != 0) {
            return super.startActionModeForChild(view, callback, i);
        }
        return null;
    }

    @Override // android.view.View
    public boolean verifyDrawable(Drawable drawable) {
        return (drawable == this.i0 && !this.l0) || (drawable == this.j0 && this.m0) || ((drawable == this.k0 && this.l0) || super.verifyDrawable(drawable));
    }

    public ActionBarContainer(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        ei4.w0(this, new f6(this));
        TypedArray obtainStyledAttributes = context.obtainStyledAttributes(attributeSet, d33.ActionBar);
        this.i0 = obtainStyledAttributes.getDrawable(d33.ActionBar_background);
        this.j0 = obtainStyledAttributes.getDrawable(d33.ActionBar_backgroundStacked);
        this.n0 = obtainStyledAttributes.getDimensionPixelSize(d33.ActionBar_height, -1);
        boolean z = true;
        if (getId() == q03.split_action_bar) {
            this.l0 = true;
            this.k0 = obtainStyledAttributes.getDrawable(d33.ActionBar_backgroundSplit);
        }
        obtainStyledAttributes.recycle();
        if (!this.l0 ? this.i0 != null || this.j0 != null : this.k0 != null) {
            z = false;
        }
        setWillNotDraw(z);
    }
}
