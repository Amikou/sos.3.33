package androidx.appcompat.widget;

import android.content.Context;
import android.content.res.ColorStateList;
import android.graphics.Bitmap;
import android.graphics.PorterDuff;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.util.AttributeSet;
import android.widget.ImageView;

/* loaded from: classes.dex */
public class AppCompatImageView extends ImageView implements m64, p64 {
    public final bf mBackgroundTintHelper;
    public final Cif mImageHelper;

    public AppCompatImageView(Context context) {
        this(context, null);
    }

    @Override // android.widget.ImageView, android.view.View
    public void drawableStateChanged() {
        super.drawableStateChanged();
        bf bfVar = this.mBackgroundTintHelper;
        if (bfVar != null) {
            bfVar.b();
        }
        Cif cif = this.mImageHelper;
        if (cif != null) {
            cif.b();
        }
    }

    @Override // defpackage.m64
    public ColorStateList getSupportBackgroundTintList() {
        bf bfVar = this.mBackgroundTintHelper;
        if (bfVar != null) {
            return bfVar.c();
        }
        return null;
    }

    @Override // defpackage.m64
    public PorterDuff.Mode getSupportBackgroundTintMode() {
        bf bfVar = this.mBackgroundTintHelper;
        if (bfVar != null) {
            return bfVar.d();
        }
        return null;
    }

    @Override // defpackage.p64
    public ColorStateList getSupportImageTintList() {
        Cif cif = this.mImageHelper;
        if (cif != null) {
            return cif.c();
        }
        return null;
    }

    @Override // defpackage.p64
    public PorterDuff.Mode getSupportImageTintMode() {
        Cif cif = this.mImageHelper;
        if (cif != null) {
            return cif.d();
        }
        return null;
    }

    @Override // android.widget.ImageView, android.view.View
    public boolean hasOverlappingRendering() {
        return this.mImageHelper.e() && super.hasOverlappingRendering();
    }

    @Override // android.view.View
    public void setBackgroundDrawable(Drawable drawable) {
        super.setBackgroundDrawable(drawable);
        bf bfVar = this.mBackgroundTintHelper;
        if (bfVar != null) {
            bfVar.f(drawable);
        }
    }

    @Override // android.view.View
    public void setBackgroundResource(int i) {
        super.setBackgroundResource(i);
        bf bfVar = this.mBackgroundTintHelper;
        if (bfVar != null) {
            bfVar.g(i);
        }
    }

    @Override // android.widget.ImageView
    public void setImageBitmap(Bitmap bitmap) {
        super.setImageBitmap(bitmap);
        Cif cif = this.mImageHelper;
        if (cif != null) {
            cif.b();
        }
    }

    @Override // android.widget.ImageView
    public void setImageDrawable(Drawable drawable) {
        super.setImageDrawable(drawable);
        Cif cif = this.mImageHelper;
        if (cif != null) {
            cif.b();
        }
    }

    @Override // android.widget.ImageView
    public void setImageResource(int i) {
        Cif cif = this.mImageHelper;
        if (cif != null) {
            cif.g(i);
        }
    }

    @Override // android.widget.ImageView
    public void setImageURI(Uri uri) {
        super.setImageURI(uri);
        Cif cif = this.mImageHelper;
        if (cif != null) {
            cif.b();
        }
    }

    @Override // defpackage.m64
    public void setSupportBackgroundTintList(ColorStateList colorStateList) {
        bf bfVar = this.mBackgroundTintHelper;
        if (bfVar != null) {
            bfVar.i(colorStateList);
        }
    }

    @Override // defpackage.m64
    public void setSupportBackgroundTintMode(PorterDuff.Mode mode) {
        bf bfVar = this.mBackgroundTintHelper;
        if (bfVar != null) {
            bfVar.j(mode);
        }
    }

    @Override // defpackage.p64
    public void setSupportImageTintList(ColorStateList colorStateList) {
        Cif cif = this.mImageHelper;
        if (cif != null) {
            cif.h(colorStateList);
        }
    }

    @Override // defpackage.p64
    public void setSupportImageTintMode(PorterDuff.Mode mode) {
        Cif cif = this.mImageHelper;
        if (cif != null) {
            cif.i(mode);
        }
    }

    public AppCompatImageView(Context context, AttributeSet attributeSet) {
        this(context, attributeSet, 0);
    }

    public AppCompatImageView(Context context, AttributeSet attributeSet, int i) {
        super(j64.b(context), attributeSet, i);
        b54.a(this, getContext());
        bf bfVar = new bf(this);
        this.mBackgroundTintHelper = bfVar;
        bfVar.e(attributeSet, i);
        Cif cif = new Cif(this);
        this.mImageHelper = cif;
        cif.f(attributeSet, i);
    }
}
