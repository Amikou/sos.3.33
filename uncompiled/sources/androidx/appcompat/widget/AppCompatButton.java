package androidx.appcompat.widget;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.res.ColorStateList;
import android.graphics.PorterDuff;
import android.graphics.drawable.Drawable;
import android.util.AttributeSet;
import android.view.ActionMode;
import android.view.accessibility.AccessibilityEvent;
import android.view.accessibility.AccessibilityNodeInfo;
import android.widget.Button;

/* loaded from: classes.dex */
public class AppCompatButton extends Button implements m64, jk, o64 {
    public final bf a;
    public final a f0;

    public AppCompatButton(Context context) {
        this(context, null);
    }

    @Override // android.widget.TextView, android.view.View
    public void drawableStateChanged() {
        super.drawableStateChanged();
        bf bfVar = this.a;
        if (bfVar != null) {
            bfVar.b();
        }
        a aVar = this.f0;
        if (aVar != null) {
            aVar.b();
        }
    }

    @Override // android.widget.TextView
    public int getAutoSizeMaxTextSize() {
        if (jk.b) {
            return super.getAutoSizeMaxTextSize();
        }
        a aVar = this.f0;
        if (aVar != null) {
            return aVar.e();
        }
        return -1;
    }

    @Override // android.widget.TextView
    public int getAutoSizeMinTextSize() {
        if (jk.b) {
            return super.getAutoSizeMinTextSize();
        }
        a aVar = this.f0;
        if (aVar != null) {
            return aVar.f();
        }
        return -1;
    }

    @Override // android.widget.TextView
    public int getAutoSizeStepGranularity() {
        if (jk.b) {
            return super.getAutoSizeStepGranularity();
        }
        a aVar = this.f0;
        if (aVar != null) {
            return aVar.g();
        }
        return -1;
    }

    @Override // android.widget.TextView
    public int[] getAutoSizeTextAvailableSizes() {
        if (jk.b) {
            return super.getAutoSizeTextAvailableSizes();
        }
        a aVar = this.f0;
        return aVar != null ? aVar.h() : new int[0];
    }

    @Override // android.widget.TextView
    @SuppressLint({"WrongConstant"})
    public int getAutoSizeTextType() {
        if (jk.b) {
            return super.getAutoSizeTextType() == 1 ? 1 : 0;
        }
        a aVar = this.f0;
        if (aVar != null) {
            return aVar.i();
        }
        return 0;
    }

    @Override // defpackage.m64
    public ColorStateList getSupportBackgroundTintList() {
        bf bfVar = this.a;
        if (bfVar != null) {
            return bfVar.c();
        }
        return null;
    }

    @Override // defpackage.m64
    public PorterDuff.Mode getSupportBackgroundTintMode() {
        bf bfVar = this.a;
        if (bfVar != null) {
            return bfVar.d();
        }
        return null;
    }

    public ColorStateList getSupportCompoundDrawablesTintList() {
        return this.f0.j();
    }

    public PorterDuff.Mode getSupportCompoundDrawablesTintMode() {
        return this.f0.k();
    }

    @Override // android.view.View
    public void onInitializeAccessibilityEvent(AccessibilityEvent accessibilityEvent) {
        super.onInitializeAccessibilityEvent(accessibilityEvent);
        accessibilityEvent.setClassName(Button.class.getName());
    }

    @Override // android.view.View
    public void onInitializeAccessibilityNodeInfo(AccessibilityNodeInfo accessibilityNodeInfo) {
        super.onInitializeAccessibilityNodeInfo(accessibilityNodeInfo);
        accessibilityNodeInfo.setClassName(Button.class.getName());
    }

    @Override // android.widget.TextView, android.view.View
    public void onLayout(boolean z, int i, int i2, int i3, int i4) {
        super.onLayout(z, i, i2, i3, i4);
        a aVar = this.f0;
        if (aVar != null) {
            aVar.o(z, i, i2, i3, i4);
        }
    }

    @Override // android.widget.TextView
    public void onTextChanged(CharSequence charSequence, int i, int i2, int i3) {
        super.onTextChanged(charSequence, i, i2, i3);
        a aVar = this.f0;
        if (aVar == null || jk.b || !aVar.l()) {
            return;
        }
        this.f0.c();
    }

    @Override // android.widget.TextView
    public void setAutoSizeTextTypeUniformWithConfiguration(int i, int i2, int i3, int i4) throws IllegalArgumentException {
        if (jk.b) {
            super.setAutoSizeTextTypeUniformWithConfiguration(i, i2, i3, i4);
            return;
        }
        a aVar = this.f0;
        if (aVar != null) {
            aVar.t(i, i2, i3, i4);
        }
    }

    @Override // android.widget.TextView
    public void setAutoSizeTextTypeUniformWithPresetSizes(int[] iArr, int i) throws IllegalArgumentException {
        if (jk.b) {
            super.setAutoSizeTextTypeUniformWithPresetSizes(iArr, i);
            return;
        }
        a aVar = this.f0;
        if (aVar != null) {
            aVar.u(iArr, i);
        }
    }

    @Override // android.widget.TextView
    public void setAutoSizeTextTypeWithDefaults(int i) {
        if (jk.b) {
            super.setAutoSizeTextTypeWithDefaults(i);
            return;
        }
        a aVar = this.f0;
        if (aVar != null) {
            aVar.v(i);
        }
    }

    @Override // android.view.View
    public void setBackgroundDrawable(Drawable drawable) {
        super.setBackgroundDrawable(drawable);
        bf bfVar = this.a;
        if (bfVar != null) {
            bfVar.f(drawable);
        }
    }

    @Override // android.view.View
    public void setBackgroundResource(int i) {
        super.setBackgroundResource(i);
        bf bfVar = this.a;
        if (bfVar != null) {
            bfVar.g(i);
        }
    }

    @Override // android.widget.TextView
    public void setCustomSelectionActionModeCallback(ActionMode.Callback callback) {
        super.setCustomSelectionActionModeCallback(t44.s(this, callback));
    }

    public void setSupportAllCaps(boolean z) {
        a aVar = this.f0;
        if (aVar != null) {
            aVar.s(z);
        }
    }

    @Override // defpackage.m64
    public void setSupportBackgroundTintList(ColorStateList colorStateList) {
        bf bfVar = this.a;
        if (bfVar != null) {
            bfVar.i(colorStateList);
        }
    }

    @Override // defpackage.m64
    public void setSupportBackgroundTintMode(PorterDuff.Mode mode) {
        bf bfVar = this.a;
        if (bfVar != null) {
            bfVar.j(mode);
        }
    }

    @Override // defpackage.o64
    public void setSupportCompoundDrawablesTintList(ColorStateList colorStateList) {
        this.f0.w(colorStateList);
        this.f0.b();
    }

    @Override // defpackage.o64
    public void setSupportCompoundDrawablesTintMode(PorterDuff.Mode mode) {
        this.f0.x(mode);
        this.f0.b();
    }

    @Override // android.widget.TextView
    public void setTextAppearance(Context context, int i) {
        super.setTextAppearance(context, i);
        a aVar = this.f0;
        if (aVar != null) {
            aVar.q(context, i);
        }
    }

    @Override // android.widget.TextView
    public void setTextSize(int i, float f) {
        if (jk.b) {
            super.setTextSize(i, f);
            return;
        }
        a aVar = this.f0;
        if (aVar != null) {
            aVar.A(i, f);
        }
    }

    public AppCompatButton(Context context, AttributeSet attributeSet) {
        this(context, attributeSet, jy2.buttonStyle);
    }

    public AppCompatButton(Context context, AttributeSet attributeSet, int i) {
        super(j64.b(context), attributeSet, i);
        b54.a(this, getContext());
        bf bfVar = new bf(this);
        this.a = bfVar;
        bfVar.e(attributeSet, i);
        a aVar = new a(this);
        this.f0 = aVar;
        aVar.m(attributeSet, i);
        aVar.b();
    }
}
