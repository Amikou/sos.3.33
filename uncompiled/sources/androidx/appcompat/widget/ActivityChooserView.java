package androidx.appcompat.widget;

import android.content.Context;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.content.res.Resources;
import android.content.res.TypedArray;
import android.database.DataSetObserver;
import android.graphics.drawable.Drawable;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import android.view.accessibility.AccessibilityNodeInfo;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.PopupWindow;
import android.widget.TextView;

/* loaded from: classes.dex */
public class ActivityChooserView extends ViewGroup {
    public final f a;
    public final g f0;
    public final View g0;
    public final Drawable h0;
    public final FrameLayout i0;
    public final ImageView j0;
    public final FrameLayout k0;
    public final ImageView l0;
    public final int m0;
    public m6 n0;
    public final DataSetObserver o0;
    public final ViewTreeObserver.OnGlobalLayoutListener p0;
    public ListPopupWindow q0;
    public PopupWindow.OnDismissListener r0;
    public boolean s0;
    public int t0;
    public boolean u0;
    public int v0;

    /* loaded from: classes.dex */
    public static class InnerLayout extends LinearLayout {
        public static final int[] a = {16842964};

        public InnerLayout(Context context, AttributeSet attributeSet) {
            super(context, attributeSet);
            l64 u = l64.u(context, attributeSet, a);
            setBackgroundDrawable(u.g(0));
            u.w();
        }
    }

    /* loaded from: classes.dex */
    public class a extends DataSetObserver {
        public a() {
        }

        @Override // android.database.DataSetObserver
        public void onChanged() {
            super.onChanged();
            ActivityChooserView.this.a.notifyDataSetChanged();
        }

        @Override // android.database.DataSetObserver
        public void onInvalidated() {
            super.onInvalidated();
            ActivityChooserView.this.a.notifyDataSetInvalidated();
        }
    }

    /* loaded from: classes.dex */
    public class b implements ViewTreeObserver.OnGlobalLayoutListener {
        public b() {
        }

        @Override // android.view.ViewTreeObserver.OnGlobalLayoutListener
        public void onGlobalLayout() {
            if (ActivityChooserView.this.b()) {
                if (!ActivityChooserView.this.isShown()) {
                    ActivityChooserView.this.getListPopupWindow().dismiss();
                    return;
                }
                ActivityChooserView.this.getListPopupWindow().b();
                m6 m6Var = ActivityChooserView.this.n0;
                if (m6Var != null) {
                    m6Var.k(true);
                }
            }
        }
    }

    /* loaded from: classes.dex */
    public class c extends View.AccessibilityDelegate {
        public c(ActivityChooserView activityChooserView) {
        }

        @Override // android.view.View.AccessibilityDelegate
        public void onInitializeAccessibilityNodeInfo(View view, AccessibilityNodeInfo accessibilityNodeInfo) {
            super.onInitializeAccessibilityNodeInfo(view, accessibilityNodeInfo);
            b6.I0(accessibilityNodeInfo).Z(true);
        }
    }

    /* loaded from: classes.dex */
    public class d extends g91 {
        public d(View view) {
            super(view);
        }

        @Override // defpackage.g91
        public po3 b() {
            return ActivityChooserView.this.getListPopupWindow();
        }

        @Override // defpackage.g91
        public boolean c() {
            ActivityChooserView.this.c();
            return true;
        }

        @Override // defpackage.g91
        public boolean d() {
            ActivityChooserView.this.a();
            return true;
        }
    }

    /* loaded from: classes.dex */
    public class e extends DataSetObserver {
        public e() {
        }

        @Override // android.database.DataSetObserver
        public void onChanged() {
            super.onChanged();
            ActivityChooserView.this.e();
        }
    }

    /* loaded from: classes.dex */
    public class f extends BaseAdapter {
        public z6 a;
        public int f0 = 4;
        public boolean g0;
        public boolean h0;
        public boolean i0;

        public f() {
        }

        public int a() {
            throw null;
        }

        public z6 b() {
            return this.a;
        }

        public ResolveInfo c() {
            throw null;
        }

        public int d() {
            throw null;
        }

        public boolean e() {
            return this.g0;
        }

        public void f(z6 z6Var) {
            ActivityChooserView.this.a.b();
            notifyDataSetChanged();
        }

        @Override // android.widget.Adapter
        public int getCount() {
            throw null;
        }

        @Override // android.widget.Adapter
        public Object getItem(int i) {
            int itemViewType = getItemViewType(i);
            if (itemViewType != 0) {
                if (itemViewType == 1) {
                    return null;
                }
                throw new IllegalArgumentException();
            } else if (this.g0) {
                throw null;
            } else {
                throw null;
            }
        }

        @Override // android.widget.Adapter
        public long getItemId(int i) {
            return i;
        }

        @Override // android.widget.BaseAdapter, android.widget.Adapter
        public int getItemViewType(int i) {
            return (this.i0 && i == getCount() - 1) ? 1 : 0;
        }

        @Override // android.widget.Adapter
        public View getView(int i, View view, ViewGroup viewGroup) {
            int itemViewType = getItemViewType(i);
            if (itemViewType != 0) {
                if (itemViewType == 1) {
                    if (view == null || view.getId() != 1) {
                        View inflate = LayoutInflater.from(ActivityChooserView.this.getContext()).inflate(d13.abc_activity_chooser_view_list_item, viewGroup, false);
                        inflate.setId(1);
                        ((TextView) inflate.findViewById(q03.title)).setText(ActivityChooserView.this.getContext().getString(u13.abc_activity_chooser_view_see_all));
                        return inflate;
                    }
                    return view;
                }
                throw new IllegalArgumentException();
            }
            if (view == null || view.getId() != q03.list_item) {
                view = LayoutInflater.from(ActivityChooserView.this.getContext()).inflate(d13.abc_activity_chooser_view_list_item, viewGroup, false);
            }
            PackageManager packageManager = ActivityChooserView.this.getContext().getPackageManager();
            ResolveInfo resolveInfo = (ResolveInfo) getItem(i);
            ((ImageView) view.findViewById(q03.icon)).setImageDrawable(resolveInfo.loadIcon(packageManager));
            ((TextView) view.findViewById(q03.title)).setText(resolveInfo.loadLabel(packageManager));
            if (this.g0 && i == 0 && this.h0) {
                view.setActivated(true);
            } else {
                view.setActivated(false);
            }
            return view;
        }

        @Override // android.widget.BaseAdapter, android.widget.Adapter
        public int getViewTypeCount() {
            return 3;
        }
    }

    /* loaded from: classes.dex */
    public class g implements AdapterView.OnItemClickListener, View.OnClickListener, View.OnLongClickListener, PopupWindow.OnDismissListener {
        public g() {
        }

        public final void a() {
            PopupWindow.OnDismissListener onDismissListener = ActivityChooserView.this.r0;
            if (onDismissListener != null) {
                onDismissListener.onDismiss();
            }
        }

        @Override // android.view.View.OnClickListener
        public void onClick(View view) {
            ActivityChooserView activityChooserView = ActivityChooserView.this;
            if (view != activityChooserView.k0) {
                if (view == activityChooserView.i0) {
                    activityChooserView.s0 = false;
                    activityChooserView.d(activityChooserView.t0);
                    return;
                }
                throw new IllegalArgumentException();
            }
            activityChooserView.a();
            ActivityChooserView.this.a.c();
            ActivityChooserView.this.a.b();
            throw null;
        }

        @Override // android.widget.PopupWindow.OnDismissListener
        public void onDismiss() {
            a();
            m6 m6Var = ActivityChooserView.this.n0;
            if (m6Var != null) {
                m6Var.k(false);
            }
        }

        @Override // android.widget.AdapterView.OnItemClickListener
        public void onItemClick(AdapterView<?> adapterView, View view, int i, long j) {
            int itemViewType = ((f) adapterView.getAdapter()).getItemViewType(i);
            if (itemViewType != 0) {
                if (itemViewType == 1) {
                    ActivityChooserView.this.d(Integer.MAX_VALUE);
                    return;
                }
                throw new IllegalArgumentException();
            }
            ActivityChooserView.this.a();
            ActivityChooserView activityChooserView = ActivityChooserView.this;
            if (!activityChooserView.s0) {
                activityChooserView.a.e();
                ActivityChooserView.this.a.b();
                throw null;
            } else if (i <= 0) {
            } else {
                activityChooserView.a.b();
                throw null;
            }
        }

        @Override // android.view.View.OnLongClickListener
        public boolean onLongClick(View view) {
            ActivityChooserView activityChooserView = ActivityChooserView.this;
            if (view == activityChooserView.k0) {
                if (activityChooserView.a.getCount() > 0) {
                    ActivityChooserView activityChooserView2 = ActivityChooserView.this;
                    activityChooserView2.s0 = true;
                    activityChooserView2.d(activityChooserView2.t0);
                }
                return true;
            }
            throw new IllegalArgumentException();
        }
    }

    public ActivityChooserView(Context context) {
        this(context, null);
    }

    public boolean a() {
        if (b()) {
            getListPopupWindow().dismiss();
            ViewTreeObserver viewTreeObserver = getViewTreeObserver();
            if (viewTreeObserver.isAlive()) {
                viewTreeObserver.removeGlobalOnLayoutListener(this.p0);
                return true;
            }
            return true;
        }
        return true;
    }

    public boolean b() {
        return getListPopupWindow().a();
    }

    public boolean c() {
        if (b() || !this.u0) {
            return false;
        }
        this.s0 = false;
        d(this.t0);
        return true;
    }

    public void d(int i) {
        this.a.b();
        throw new IllegalStateException("No data model. Did you call #setDataModel?");
    }

    public void e() {
        if (this.a.getCount() > 0) {
            this.i0.setEnabled(true);
        } else {
            this.i0.setEnabled(false);
        }
        int a2 = this.a.a();
        int d2 = this.a.d();
        if (a2 != 1 && (a2 <= 1 || d2 <= 0)) {
            this.k0.setVisibility(8);
        } else {
            this.k0.setVisibility(0);
            ResolveInfo c2 = this.a.c();
            PackageManager packageManager = getContext().getPackageManager();
            this.l0.setImageDrawable(c2.loadIcon(packageManager));
            if (this.v0 != 0) {
                this.k0.setContentDescription(getContext().getString(this.v0, c2.loadLabel(packageManager)));
            }
        }
        if (this.k0.getVisibility() == 0) {
            this.g0.setBackgroundDrawable(this.h0);
        } else {
            this.g0.setBackgroundDrawable(null);
        }
    }

    public z6 getDataModel() {
        return this.a.b();
    }

    public ListPopupWindow getListPopupWindow() {
        if (this.q0 == null) {
            ListPopupWindow listPopupWindow = new ListPopupWindow(getContext());
            this.q0 = listPopupWindow;
            listPopupWindow.p(this.a);
            this.q0.D(this);
            this.q0.J(true);
            this.q0.L(this.f0);
            this.q0.K(this.f0);
        }
        return this.q0;
    }

    @Override // android.view.ViewGroup, android.view.View
    public void onAttachedToWindow() {
        super.onAttachedToWindow();
        this.a.b();
        this.u0 = true;
    }

    @Override // android.view.ViewGroup, android.view.View
    public void onDetachedFromWindow() {
        super.onDetachedFromWindow();
        this.a.b();
        ViewTreeObserver viewTreeObserver = getViewTreeObserver();
        if (viewTreeObserver.isAlive()) {
            viewTreeObserver.removeGlobalOnLayoutListener(this.p0);
        }
        if (b()) {
            a();
        }
        this.u0 = false;
    }

    @Override // android.view.ViewGroup, android.view.View
    public void onLayout(boolean z, int i, int i2, int i3, int i4) {
        this.g0.layout(0, 0, i3 - i, i4 - i2);
        if (b()) {
            return;
        }
        a();
    }

    @Override // android.view.View
    public void onMeasure(int i, int i2) {
        View view = this.g0;
        if (this.k0.getVisibility() != 0) {
            i2 = View.MeasureSpec.makeMeasureSpec(View.MeasureSpec.getSize(i2), 1073741824);
        }
        measureChild(view, i, i2);
        setMeasuredDimension(view.getMeasuredWidth(), view.getMeasuredHeight());
    }

    public void setActivityChooserModel(z6 z6Var) {
        this.a.f(z6Var);
        if (b()) {
            a();
            c();
        }
    }

    public void setDefaultActionButtonContentDescription(int i) {
        this.v0 = i;
    }

    public void setExpandActivityOverflowButtonContentDescription(int i) {
        this.j0.setContentDescription(getContext().getString(i));
    }

    public void setExpandActivityOverflowButtonDrawable(Drawable drawable) {
        this.j0.setImageDrawable(drawable);
    }

    public void setInitialActivityCount(int i) {
        this.t0 = i;
    }

    public void setOnDismissListener(PopupWindow.OnDismissListener onDismissListener) {
        this.r0 = onDismissListener;
    }

    public void setProvider(m6 m6Var) {
        this.n0 = m6Var;
    }

    public ActivityChooserView(Context context, AttributeSet attributeSet) {
        this(context, attributeSet, 0);
    }

    public ActivityChooserView(Context context, AttributeSet attributeSet, int i) {
        super(context, attributeSet, i);
        this.o0 = new a();
        this.p0 = new b();
        this.t0 = 4;
        int[] iArr = d33.ActivityChooserView;
        TypedArray obtainStyledAttributes = context.obtainStyledAttributes(attributeSet, iArr, i, 0);
        ei4.r0(this, context, iArr, attributeSet, obtainStyledAttributes, i, 0);
        this.t0 = obtainStyledAttributes.getInt(d33.ActivityChooserView_initialActivityCount, 4);
        Drawable drawable = obtainStyledAttributes.getDrawable(d33.ActivityChooserView_expandActivityOverflowButtonDrawable);
        obtainStyledAttributes.recycle();
        LayoutInflater.from(getContext()).inflate(d13.abc_activity_chooser_view, (ViewGroup) this, true);
        g gVar = new g();
        this.f0 = gVar;
        View findViewById = findViewById(q03.activity_chooser_view_content);
        this.g0 = findViewById;
        this.h0 = findViewById.getBackground();
        FrameLayout frameLayout = (FrameLayout) findViewById(q03.default_activity_button);
        this.k0 = frameLayout;
        frameLayout.setOnClickListener(gVar);
        frameLayout.setOnLongClickListener(gVar);
        int i2 = q03.image;
        this.l0 = (ImageView) frameLayout.findViewById(i2);
        FrameLayout frameLayout2 = (FrameLayout) findViewById(q03.expand_activities_button);
        frameLayout2.setOnClickListener(gVar);
        frameLayout2.setAccessibilityDelegate(new c(this));
        frameLayout2.setOnTouchListener(new d(frameLayout2));
        this.i0 = frameLayout2;
        ImageView imageView = (ImageView) frameLayout2.findViewById(i2);
        this.j0 = imageView;
        imageView.setImageDrawable(drawable);
        f fVar = new f();
        this.a = fVar;
        fVar.registerDataSetObserver(new e());
        Resources resources = context.getResources();
        this.m0 = Math.max(resources.getDisplayMetrics().widthPixels / 2, resources.getDimensionPixelSize(lz2.abc_config_prefDialogWidth));
    }
}
