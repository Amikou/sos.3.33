package androidx.appcompat.widget;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.graphics.drawable.Drawable;
import android.os.Parcel;
import android.os.Parcelable;
import android.util.SparseBooleanArray;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewParent;
import androidx.appcompat.view.menu.ActionMenuItemView;
import androidx.appcompat.view.menu.i;
import androidx.appcompat.view.menu.j;
import androidx.appcompat.view.menu.l;
import androidx.appcompat.widget.ActionMenuView;
import defpackage.m6;
import java.util.ArrayList;

/* loaded from: classes.dex */
public class ActionMenuPresenter extends androidx.appcompat.view.menu.a implements m6.a {
    public final SparseBooleanArray A0;
    public e B0;
    public a C0;
    public c D0;
    public b E0;
    public final f F0;
    public int G0;
    public d n0;
    public Drawable o0;
    public boolean p0;
    public boolean q0;
    public boolean r0;
    public int s0;
    public int t0;
    public int u0;
    public boolean v0;
    public boolean w0;
    public boolean x0;
    public boolean y0;
    public int z0;

    @SuppressLint({"BanParcelableUsage"})
    /* loaded from: classes.dex */
    public static class SavedState implements Parcelable {
        public static final Parcelable.Creator<SavedState> CREATOR = new a();
        public int a;

        /* loaded from: classes.dex */
        public class a implements Parcelable.Creator<SavedState> {
            @Override // android.os.Parcelable.Creator
            /* renamed from: a */
            public SavedState createFromParcel(Parcel parcel) {
                return new SavedState(parcel);
            }

            @Override // android.os.Parcelable.Creator
            /* renamed from: b */
            public SavedState[] newArray(int i) {
                return new SavedState[i];
            }
        }

        public SavedState() {
        }

        @Override // android.os.Parcelable
        public int describeContents() {
            return 0;
        }

        @Override // android.os.Parcelable
        public void writeToParcel(Parcel parcel, int i) {
            parcel.writeInt(this.a);
        }

        public SavedState(Parcel parcel) {
            this.a = parcel.readInt();
        }
    }

    /* loaded from: classes.dex */
    public class a extends androidx.appcompat.view.menu.h {
        public a(Context context, l lVar, View view) {
            super(context, lVar, view, false, jy2.actionOverflowMenuStyle);
            if (!((androidx.appcompat.view.menu.g) lVar.getItem()).l()) {
                View view2 = ActionMenuPresenter.this.n0;
                f(view2 == null ? (View) ActionMenuPresenter.this.l0 : view2);
            }
            j(ActionMenuPresenter.this.F0);
        }

        @Override // androidx.appcompat.view.menu.h
        public void e() {
            ActionMenuPresenter actionMenuPresenter = ActionMenuPresenter.this;
            actionMenuPresenter.C0 = null;
            actionMenuPresenter.G0 = 0;
            super.e();
        }
    }

    /* loaded from: classes.dex */
    public class b extends ActionMenuItemView.b {
        public b() {
        }

        @Override // androidx.appcompat.view.menu.ActionMenuItemView.b
        public po3 a() {
            a aVar = ActionMenuPresenter.this.C0;
            if (aVar != null) {
                return aVar.c();
            }
            return null;
        }
    }

    /* loaded from: classes.dex */
    public class c implements Runnable {
        public e a;

        public c(e eVar) {
            this.a = eVar;
        }

        @Override // java.lang.Runnable
        public void run() {
            if (ActionMenuPresenter.this.g0 != null) {
                ActionMenuPresenter.this.g0.d();
            }
            View view = (View) ActionMenuPresenter.this.l0;
            if (view != null && view.getWindowToken() != null && this.a.m()) {
                ActionMenuPresenter.this.B0 = this.a;
            }
            ActionMenuPresenter.this.D0 = null;
        }
    }

    /* loaded from: classes.dex */
    public class d extends AppCompatImageView implements ActionMenuView.a {

        /* loaded from: classes.dex */
        public class a extends g91 {
            public a(View view, ActionMenuPresenter actionMenuPresenter) {
                super(view);
            }

            @Override // defpackage.g91
            public po3 b() {
                e eVar = ActionMenuPresenter.this.B0;
                if (eVar == null) {
                    return null;
                }
                return eVar.c();
            }

            @Override // defpackage.g91
            public boolean c() {
                ActionMenuPresenter.this.N();
                return true;
            }

            @Override // defpackage.g91
            public boolean d() {
                ActionMenuPresenter actionMenuPresenter = ActionMenuPresenter.this;
                if (actionMenuPresenter.D0 != null) {
                    return false;
                }
                actionMenuPresenter.E();
                return true;
            }
        }

        public d(Context context) {
            super(context, null, jy2.actionOverflowButtonStyle);
            setClickable(true);
            setFocusable(true);
            setVisibility(0);
            setEnabled(true);
            j74.a(this, getContentDescription());
            setOnTouchListener(new a(this, ActionMenuPresenter.this));
        }

        @Override // androidx.appcompat.widget.ActionMenuView.a
        public boolean a() {
            return false;
        }

        @Override // androidx.appcompat.widget.ActionMenuView.a
        public boolean b() {
            return false;
        }

        @Override // android.view.View
        public boolean performClick() {
            if (super.performClick()) {
                return true;
            }
            playSoundEffect(0);
            ActionMenuPresenter.this.N();
            return true;
        }

        @Override // android.widget.ImageView
        public boolean setFrame(int i, int i2, int i3, int i4) {
            boolean frame = super.setFrame(i, i2, i3, i4);
            Drawable drawable = getDrawable();
            Drawable background = getBackground();
            if (drawable != null && background != null) {
                int width = getWidth();
                int height = getHeight();
                int max = Math.max(width, height) / 2;
                int paddingLeft = (width + (getPaddingLeft() - getPaddingRight())) / 2;
                int paddingTop = (height + (getPaddingTop() - getPaddingBottom())) / 2;
                androidx.core.graphics.drawable.a.l(background, paddingLeft - max, paddingTop - max, paddingLeft + max, paddingTop + max);
            }
            return frame;
        }
    }

    /* loaded from: classes.dex */
    public class e extends androidx.appcompat.view.menu.h {
        public e(Context context, androidx.appcompat.view.menu.e eVar, View view, boolean z) {
            super(context, eVar, view, z, jy2.actionOverflowMenuStyle);
            h(8388613);
            j(ActionMenuPresenter.this.F0);
        }

        @Override // androidx.appcompat.view.menu.h
        public void e() {
            if (ActionMenuPresenter.this.g0 != null) {
                ActionMenuPresenter.this.g0.close();
            }
            ActionMenuPresenter.this.B0 = null;
            super.e();
        }
    }

    /* loaded from: classes.dex */
    public class f implements i.a {
        public f() {
        }

        @Override // androidx.appcompat.view.menu.i.a
        public void c(androidx.appcompat.view.menu.e eVar, boolean z) {
            if (eVar instanceof l) {
                eVar.F().e(false);
            }
            i.a p = ActionMenuPresenter.this.p();
            if (p != null) {
                p.c(eVar, z);
            }
        }

        @Override // androidx.appcompat.view.menu.i.a
        public boolean d(androidx.appcompat.view.menu.e eVar) {
            if (eVar == ActionMenuPresenter.this.g0) {
                return false;
            }
            ActionMenuPresenter.this.G0 = ((l) eVar).getItem().getItemId();
            i.a p = ActionMenuPresenter.this.p();
            if (p != null) {
                return p.d(eVar);
            }
            return false;
        }
    }

    public ActionMenuPresenter(Context context) {
        super(context, d13.abc_action_menu_layout, d13.abc_action_menu_item_layout);
        this.A0 = new SparseBooleanArray();
        this.F0 = new f();
    }

    public boolean B() {
        return E() | F();
    }

    public final View C(MenuItem menuItem) {
        ViewGroup viewGroup = (ViewGroup) this.l0;
        if (viewGroup == null) {
            return null;
        }
        int childCount = viewGroup.getChildCount();
        for (int i = 0; i < childCount; i++) {
            View childAt = viewGroup.getChildAt(i);
            if ((childAt instanceof j.a) && ((j.a) childAt).getItemData() == menuItem) {
                return childAt;
            }
        }
        return null;
    }

    public Drawable D() {
        d dVar = this.n0;
        if (dVar != null) {
            return dVar.getDrawable();
        }
        if (this.p0) {
            return this.o0;
        }
        return null;
    }

    public boolean E() {
        j jVar;
        c cVar = this.D0;
        if (cVar != null && (jVar = this.l0) != null) {
            ((View) jVar).removeCallbacks(cVar);
            this.D0 = null;
            return true;
        }
        e eVar = this.B0;
        if (eVar != null) {
            eVar.b();
            return true;
        }
        return false;
    }

    public boolean F() {
        a aVar = this.C0;
        if (aVar != null) {
            aVar.b();
            return true;
        }
        return false;
    }

    public boolean G() {
        return this.D0 != null || H();
    }

    public boolean H() {
        e eVar = this.B0;
        return eVar != null && eVar.d();
    }

    public void I(Configuration configuration) {
        if (!this.v0) {
            this.u0 = i6.b(this.f0).d();
        }
        androidx.appcompat.view.menu.e eVar = this.g0;
        if (eVar != null) {
            eVar.M(true);
        }
    }

    public void J(boolean z) {
        this.y0 = z;
    }

    public void K(ActionMenuView actionMenuView) {
        this.l0 = actionMenuView;
        actionMenuView.b(this.g0);
    }

    public void L(Drawable drawable) {
        d dVar = this.n0;
        if (dVar != null) {
            dVar.setImageDrawable(drawable);
            return;
        }
        this.p0 = true;
        this.o0 = drawable;
    }

    public void M(boolean z) {
        this.q0 = z;
        this.r0 = true;
    }

    public boolean N() {
        androidx.appcompat.view.menu.e eVar;
        if (!this.q0 || H() || (eVar = this.g0) == null || this.l0 == null || this.D0 != null || eVar.B().isEmpty()) {
            return false;
        }
        c cVar = new c(new e(this.f0, this.g0, this.n0, true));
        this.D0 = cVar;
        ((View) this.l0).post(cVar);
        return true;
    }

    @Override // defpackage.m6.a
    public void a(boolean z) {
        if (z) {
            super.l(null);
            return;
        }
        androidx.appcompat.view.menu.e eVar = this.g0;
        if (eVar != null) {
            eVar.e(false);
        }
    }

    @Override // androidx.appcompat.view.menu.a, androidx.appcompat.view.menu.i
    public void c(androidx.appcompat.view.menu.e eVar, boolean z) {
        B();
        super.c(eVar, z);
    }

    @Override // androidx.appcompat.view.menu.a, androidx.appcompat.view.menu.i
    public void d(boolean z) {
        super.d(z);
        ((View) this.l0).requestLayout();
        androidx.appcompat.view.menu.e eVar = this.g0;
        boolean z2 = false;
        if (eVar != null) {
            ArrayList<androidx.appcompat.view.menu.g> u = eVar.u();
            int size = u.size();
            for (int i = 0; i < size; i++) {
                m6 b2 = u.get(i).b();
                if (b2 != null) {
                    b2.i(this);
                }
            }
        }
        androidx.appcompat.view.menu.e eVar2 = this.g0;
        ArrayList<androidx.appcompat.view.menu.g> B = eVar2 != null ? eVar2.B() : null;
        if (this.q0 && B != null) {
            int size2 = B.size();
            if (size2 == 1) {
                z2 = !B.get(0).isActionViewExpanded();
            } else if (size2 > 0) {
                z2 = true;
            }
        }
        if (z2) {
            if (this.n0 == null) {
                this.n0 = new d(this.a);
            }
            ViewGroup viewGroup = (ViewGroup) this.n0.getParent();
            if (viewGroup != this.l0) {
                if (viewGroup != null) {
                    viewGroup.removeView(this.n0);
                }
                ActionMenuView actionMenuView = (ActionMenuView) this.l0;
                actionMenuView.addView(this.n0, actionMenuView.i());
            }
        } else {
            d dVar = this.n0;
            if (dVar != null) {
                ViewParent parent = dVar.getParent();
                j jVar = this.l0;
                if (parent == jVar) {
                    ((ViewGroup) jVar).removeView(this.n0);
                }
            }
        }
        ((ActionMenuView) this.l0).setOverflowReserved(this.q0);
    }

    /* JADX WARN: Multi-variable type inference failed */
    /* JADX WARN: Type inference failed for: r3v0 */
    /* JADX WARN: Type inference failed for: r3v1, types: [int] */
    /* JADX WARN: Type inference failed for: r3v12 */
    @Override // androidx.appcompat.view.menu.i
    public boolean e() {
        ArrayList<androidx.appcompat.view.menu.g> arrayList;
        int i;
        int i2;
        int i3;
        boolean z;
        int i4;
        ActionMenuPresenter actionMenuPresenter = this;
        androidx.appcompat.view.menu.e eVar = actionMenuPresenter.g0;
        View view = null;
        ?? r3 = 0;
        if (eVar != null) {
            arrayList = eVar.G();
            i = arrayList.size();
        } else {
            arrayList = null;
            i = 0;
        }
        int i5 = actionMenuPresenter.u0;
        int i6 = actionMenuPresenter.t0;
        int makeMeasureSpec = View.MeasureSpec.makeMeasureSpec(0, 0);
        ViewGroup viewGroup = (ViewGroup) actionMenuPresenter.l0;
        boolean z2 = false;
        int i7 = 0;
        int i8 = 0;
        for (int i9 = 0; i9 < i; i9++) {
            androidx.appcompat.view.menu.g gVar = arrayList.get(i9);
            if (gVar.o()) {
                i7++;
            } else if (gVar.n()) {
                i8++;
            } else {
                z2 = true;
            }
            if (actionMenuPresenter.y0 && gVar.isActionViewExpanded()) {
                i5 = 0;
            }
        }
        if (actionMenuPresenter.q0 && (z2 || i8 + i7 > i5)) {
            i5--;
        }
        int i10 = i5 - i7;
        SparseBooleanArray sparseBooleanArray = actionMenuPresenter.A0;
        sparseBooleanArray.clear();
        if (actionMenuPresenter.w0) {
            int i11 = actionMenuPresenter.z0;
            i3 = i6 / i11;
            i2 = i11 + ((i6 % i11) / i3);
        } else {
            i2 = 0;
            i3 = 0;
        }
        int i12 = 0;
        int i13 = 0;
        while (i12 < i) {
            androidx.appcompat.view.menu.g gVar2 = arrayList.get(i12);
            if (gVar2.o()) {
                View q = actionMenuPresenter.q(gVar2, view, viewGroup);
                if (actionMenuPresenter.w0) {
                    i3 -= ActionMenuView.o(q, i2, i3, makeMeasureSpec, r3);
                } else {
                    q.measure(makeMeasureSpec, makeMeasureSpec);
                }
                int measuredWidth = q.getMeasuredWidth();
                i6 -= measuredWidth;
                if (i13 == 0) {
                    i13 = measuredWidth;
                }
                int groupId = gVar2.getGroupId();
                if (groupId != 0) {
                    sparseBooleanArray.put(groupId, true);
                }
                gVar2.u(true);
                z = r3;
                i4 = i;
            } else if (gVar2.n()) {
                int groupId2 = gVar2.getGroupId();
                boolean z3 = sparseBooleanArray.get(groupId2);
                boolean z4 = (i10 > 0 || z3) && i6 > 0 && (!actionMenuPresenter.w0 || i3 > 0);
                boolean z5 = z4;
                i4 = i;
                if (z4) {
                    View q2 = actionMenuPresenter.q(gVar2, null, viewGroup);
                    if (actionMenuPresenter.w0) {
                        int o = ActionMenuView.o(q2, i2, i3, makeMeasureSpec, 0);
                        i3 -= o;
                        if (o == 0) {
                            z5 = false;
                        }
                    } else {
                        q2.measure(makeMeasureSpec, makeMeasureSpec);
                    }
                    boolean z6 = z5;
                    int measuredWidth2 = q2.getMeasuredWidth();
                    i6 -= measuredWidth2;
                    if (i13 == 0) {
                        i13 = measuredWidth2;
                    }
                    z4 = z6 & (!actionMenuPresenter.w0 ? i6 + i13 <= 0 : i6 < 0);
                }
                if (z4 && groupId2 != 0) {
                    sparseBooleanArray.put(groupId2, true);
                } else if (z3) {
                    sparseBooleanArray.put(groupId2, false);
                    for (int i14 = 0; i14 < i12; i14++) {
                        androidx.appcompat.view.menu.g gVar3 = arrayList.get(i14);
                        if (gVar3.getGroupId() == groupId2) {
                            if (gVar3.l()) {
                                i10++;
                            }
                            gVar3.u(false);
                        }
                    }
                }
                if (z4) {
                    i10--;
                }
                gVar2.u(z4);
                z = false;
            } else {
                z = r3;
                i4 = i;
                gVar2.u(z);
            }
            i12++;
            r3 = z;
            i = i4;
            view = null;
            actionMenuPresenter = this;
        }
        return true;
    }

    @Override // androidx.appcompat.view.menu.a, androidx.appcompat.view.menu.i
    public void i(Context context, androidx.appcompat.view.menu.e eVar) {
        super.i(context, eVar);
        Resources resources = context.getResources();
        i6 b2 = i6.b(context);
        if (!this.r0) {
            this.q0 = b2.h();
        }
        if (!this.x0) {
            this.s0 = b2.c();
        }
        if (!this.v0) {
            this.u0 = b2.d();
        }
        int i = this.s0;
        if (this.q0) {
            if (this.n0 == null) {
                d dVar = new d(this.a);
                this.n0 = dVar;
                if (this.p0) {
                    dVar.setImageDrawable(this.o0);
                    this.o0 = null;
                    this.p0 = false;
                }
                int makeMeasureSpec = View.MeasureSpec.makeMeasureSpec(0, 0);
                this.n0.measure(makeMeasureSpec, makeMeasureSpec);
            }
            i -= this.n0.getMeasuredWidth();
        } else {
            this.n0 = null;
        }
        this.t0 = i;
        this.z0 = (int) (resources.getDisplayMetrics().density * 56.0f);
    }

    @Override // androidx.appcompat.view.menu.i
    public void j(Parcelable parcelable) {
        int i;
        MenuItem findItem;
        if ((parcelable instanceof SavedState) && (i = ((SavedState) parcelable).a) > 0 && (findItem = this.g0.findItem(i)) != null) {
            l((l) findItem.getSubMenu());
        }
    }

    @Override // androidx.appcompat.view.menu.a
    public void k(androidx.appcompat.view.menu.g gVar, j.a aVar) {
        aVar.d(gVar, 0);
        ActionMenuItemView actionMenuItemView = (ActionMenuItemView) aVar;
        actionMenuItemView.setItemInvoker((ActionMenuView) this.l0);
        if (this.E0 == null) {
            this.E0 = new b();
        }
        actionMenuItemView.setPopupCallback(this.E0);
    }

    @Override // androidx.appcompat.view.menu.a, androidx.appcompat.view.menu.i
    public boolean l(l lVar) {
        boolean z = false;
        if (lVar.hasVisibleItems()) {
            l lVar2 = lVar;
            while (lVar2.i0() != this.g0) {
                lVar2 = (l) lVar2.i0();
            }
            View C = C(lVar2.getItem());
            if (C == null) {
                return false;
            }
            this.G0 = lVar.getItem().getItemId();
            int size = lVar.size();
            int i = 0;
            while (true) {
                if (i >= size) {
                    break;
                }
                MenuItem item = lVar.getItem(i);
                if (item.isVisible() && item.getIcon() != null) {
                    z = true;
                    break;
                }
                i++;
            }
            a aVar = new a(this.f0, lVar, C);
            this.C0 = aVar;
            aVar.g(z);
            this.C0.k();
            super.l(lVar);
            return true;
        }
        return false;
    }

    @Override // androidx.appcompat.view.menu.i
    public Parcelable m() {
        SavedState savedState = new SavedState();
        savedState.a = this.G0;
        return savedState;
    }

    @Override // androidx.appcompat.view.menu.a
    public boolean o(ViewGroup viewGroup, int i) {
        if (viewGroup.getChildAt(i) == this.n0) {
            return false;
        }
        return super.o(viewGroup, i);
    }

    @Override // androidx.appcompat.view.menu.a
    public View q(androidx.appcompat.view.menu.g gVar, View view, ViewGroup viewGroup) {
        View actionView = gVar.getActionView();
        if (actionView == null || gVar.j()) {
            actionView = super.q(gVar, view, viewGroup);
        }
        actionView.setVisibility(gVar.isActionViewExpanded() ? 8 : 0);
        ActionMenuView actionMenuView = (ActionMenuView) viewGroup;
        ViewGroup.LayoutParams layoutParams = actionView.getLayoutParams();
        if (!actionMenuView.checkLayoutParams(layoutParams)) {
            actionView.setLayoutParams(actionMenuView.generateLayoutParams(layoutParams));
        }
        return actionView;
    }

    @Override // androidx.appcompat.view.menu.a
    public j r(ViewGroup viewGroup) {
        j jVar = this.l0;
        j r = super.r(viewGroup);
        if (jVar != r) {
            ((ActionMenuView) r).setPresenter(this);
        }
        return r;
    }

    @Override // androidx.appcompat.view.menu.a
    public boolean t(int i, androidx.appcompat.view.menu.g gVar) {
        return gVar.l();
    }
}
