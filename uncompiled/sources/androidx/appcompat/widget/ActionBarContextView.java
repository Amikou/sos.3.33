package androidx.appcompat.widget;

import android.content.Context;
import android.text.TextUtils;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.accessibility.AccessibilityEvent;
import android.widget.LinearLayout;
import android.widget.TextView;

/* loaded from: classes.dex */
public class ActionBarContextView extends n4 {
    public CharSequence m0;
    public CharSequence n0;
    public View o0;
    public View p0;
    public View q0;
    public LinearLayout r0;
    public TextView s0;
    public TextView t0;
    public int u0;
    public int v0;
    public boolean w0;
    public int x0;

    /* loaded from: classes.dex */
    public class a implements View.OnClickListener {
        public final /* synthetic */ k6 a;

        public a(ActionBarContextView actionBarContextView, k6 k6Var) {
            this.a = k6Var;
        }

        @Override // android.view.View.OnClickListener
        public void onClick(View view) {
            this.a.c();
        }
    }

    public ActionBarContextView(Context context) {
        this(context, null);
    }

    public void g() {
        if (this.o0 == null) {
            k();
        }
    }

    @Override // android.view.ViewGroup
    public ViewGroup.LayoutParams generateDefaultLayoutParams() {
        return new ViewGroup.MarginLayoutParams(-1, -2);
    }

    @Override // android.view.ViewGroup
    public ViewGroup.LayoutParams generateLayoutParams(AttributeSet attributeSet) {
        return new ViewGroup.MarginLayoutParams(getContext(), attributeSet);
    }

    @Override // defpackage.n4
    public /* bridge */ /* synthetic */ int getAnimatedVisibility() {
        return super.getAnimatedVisibility();
    }

    @Override // defpackage.n4
    public /* bridge */ /* synthetic */ int getContentHeight() {
        return super.getContentHeight();
    }

    public CharSequence getSubtitle() {
        return this.n0;
    }

    public CharSequence getTitle() {
        return this.m0;
    }

    public void h(k6 k6Var) {
        View view = this.o0;
        if (view == null) {
            View inflate = LayoutInflater.from(getContext()).inflate(this.x0, (ViewGroup) this, false);
            this.o0 = inflate;
            addView(inflate);
        } else if (view.getParent() == null) {
            addView(this.o0);
        }
        View findViewById = this.o0.findViewById(q03.action_mode_close_button);
        this.p0 = findViewById;
        findViewById.setOnClickListener(new a(this, k6Var));
        androidx.appcompat.view.menu.e eVar = (androidx.appcompat.view.menu.e) k6Var.e();
        ActionMenuPresenter actionMenuPresenter = this.h0;
        if (actionMenuPresenter != null) {
            actionMenuPresenter.B();
        }
        ActionMenuPresenter actionMenuPresenter2 = new ActionMenuPresenter(getContext());
        this.h0 = actionMenuPresenter2;
        actionMenuPresenter2.M(true);
        ViewGroup.LayoutParams layoutParams = new ViewGroup.LayoutParams(-2, -1);
        eVar.c(this.h0, this.f0);
        ActionMenuView actionMenuView = (ActionMenuView) this.h0.r(this);
        this.g0 = actionMenuView;
        ei4.w0(actionMenuView, null);
        addView(this.g0, layoutParams);
    }

    public final void i() {
        if (this.r0 == null) {
            LayoutInflater.from(getContext()).inflate(d13.abc_action_bar_title_item, this);
            LinearLayout linearLayout = (LinearLayout) getChildAt(getChildCount() - 1);
            this.r0 = linearLayout;
            this.s0 = (TextView) linearLayout.findViewById(q03.action_bar_title);
            this.t0 = (TextView) this.r0.findViewById(q03.action_bar_subtitle);
            if (this.u0 != 0) {
                this.s0.setTextAppearance(getContext(), this.u0);
            }
            if (this.v0 != 0) {
                this.t0.setTextAppearance(getContext(), this.v0);
            }
        }
        this.s0.setText(this.m0);
        this.t0.setText(this.n0);
        boolean z = !TextUtils.isEmpty(this.m0);
        boolean z2 = !TextUtils.isEmpty(this.n0);
        int i = 0;
        this.t0.setVisibility(z2 ? 0 : 8);
        LinearLayout linearLayout2 = this.r0;
        if (!z && !z2) {
            i = 8;
        }
        linearLayout2.setVisibility(i);
        if (this.r0.getParent() == null) {
            addView(this.r0);
        }
    }

    public boolean j() {
        return this.w0;
    }

    public void k() {
        removeAllViews();
        this.q0 = null;
        this.g0 = null;
        this.h0 = null;
        View view = this.p0;
        if (view != null) {
            view.setOnClickListener(null);
        }
    }

    public boolean l() {
        ActionMenuPresenter actionMenuPresenter = this.h0;
        if (actionMenuPresenter != null) {
            return actionMenuPresenter.N();
        }
        return false;
    }

    @Override // android.view.ViewGroup, android.view.View
    public void onDetachedFromWindow() {
        super.onDetachedFromWindow();
        ActionMenuPresenter actionMenuPresenter = this.h0;
        if (actionMenuPresenter != null) {
            actionMenuPresenter.E();
            this.h0.F();
        }
    }

    @Override // android.view.View
    public void onInitializeAccessibilityEvent(AccessibilityEvent accessibilityEvent) {
        if (accessibilityEvent.getEventType() == 32) {
            accessibilityEvent.setSource(this);
            accessibilityEvent.setClassName(getClass().getName());
            accessibilityEvent.setPackageName(getContext().getPackageName());
            accessibilityEvent.setContentDescription(this.m0);
            return;
        }
        super.onInitializeAccessibilityEvent(accessibilityEvent);
    }

    @Override // android.view.ViewGroup, android.view.View
    public void onLayout(boolean z, int i, int i2, int i3, int i4) {
        boolean b = ok4.b(this);
        int paddingRight = b ? (i3 - i) - getPaddingRight() : getPaddingLeft();
        int paddingTop = getPaddingTop();
        int paddingTop2 = ((i4 - i2) - getPaddingTop()) - getPaddingBottom();
        View view = this.o0;
        if (view != null && view.getVisibility() != 8) {
            ViewGroup.MarginLayoutParams marginLayoutParams = (ViewGroup.MarginLayoutParams) this.o0.getLayoutParams();
            int i5 = b ? marginLayoutParams.rightMargin : marginLayoutParams.leftMargin;
            int i6 = b ? marginLayoutParams.leftMargin : marginLayoutParams.rightMargin;
            int d = n4.d(paddingRight, i5, b);
            paddingRight = n4.d(d + e(this.o0, d, paddingTop, paddingTop2, b), i6, b);
        }
        int i7 = paddingRight;
        LinearLayout linearLayout = this.r0;
        if (linearLayout != null && this.q0 == null && linearLayout.getVisibility() != 8) {
            i7 += e(this.r0, i7, paddingTop, paddingTop2, b);
        }
        int i8 = i7;
        View view2 = this.q0;
        if (view2 != null) {
            e(view2, i8, paddingTop, paddingTop2, b);
        }
        int paddingLeft = b ? getPaddingLeft() : (i3 - i) - getPaddingRight();
        ActionMenuView actionMenuView = this.g0;
        if (actionMenuView != null) {
            e(actionMenuView, paddingLeft, paddingTop, paddingTop2, !b);
        }
    }

    @Override // android.view.View
    public void onMeasure(int i, int i2) {
        if (View.MeasureSpec.getMode(i) == 1073741824) {
            if (View.MeasureSpec.getMode(i2) != 0) {
                int size = View.MeasureSpec.getSize(i);
                int i3 = this.i0;
                if (i3 <= 0) {
                    i3 = View.MeasureSpec.getSize(i2);
                }
                int paddingTop = getPaddingTop() + getPaddingBottom();
                int paddingLeft = (size - getPaddingLeft()) - getPaddingRight();
                int i4 = i3 - paddingTop;
                int makeMeasureSpec = View.MeasureSpec.makeMeasureSpec(i4, Integer.MIN_VALUE);
                View view = this.o0;
                if (view != null) {
                    int c = c(view, paddingLeft, makeMeasureSpec, 0);
                    ViewGroup.MarginLayoutParams marginLayoutParams = (ViewGroup.MarginLayoutParams) this.o0.getLayoutParams();
                    paddingLeft = c - (marginLayoutParams.leftMargin + marginLayoutParams.rightMargin);
                }
                ActionMenuView actionMenuView = this.g0;
                if (actionMenuView != null && actionMenuView.getParent() == this) {
                    paddingLeft = c(this.g0, paddingLeft, makeMeasureSpec, 0);
                }
                LinearLayout linearLayout = this.r0;
                if (linearLayout != null && this.q0 == null) {
                    if (this.w0) {
                        this.r0.measure(View.MeasureSpec.makeMeasureSpec(0, 0), makeMeasureSpec);
                        int measuredWidth = this.r0.getMeasuredWidth();
                        boolean z = measuredWidth <= paddingLeft;
                        if (z) {
                            paddingLeft -= measuredWidth;
                        }
                        this.r0.setVisibility(z ? 0 : 8);
                    } else {
                        paddingLeft = c(linearLayout, paddingLeft, makeMeasureSpec, 0);
                    }
                }
                View view2 = this.q0;
                if (view2 != null) {
                    ViewGroup.LayoutParams layoutParams = view2.getLayoutParams();
                    int i5 = layoutParams.width;
                    int i6 = i5 != -2 ? 1073741824 : Integer.MIN_VALUE;
                    if (i5 >= 0) {
                        paddingLeft = Math.min(i5, paddingLeft);
                    }
                    int i7 = layoutParams.height;
                    int i8 = i7 == -2 ? Integer.MIN_VALUE : 1073741824;
                    if (i7 >= 0) {
                        i4 = Math.min(i7, i4);
                    }
                    this.q0.measure(View.MeasureSpec.makeMeasureSpec(paddingLeft, i6), View.MeasureSpec.makeMeasureSpec(i4, i8));
                }
                if (this.i0 <= 0) {
                    int childCount = getChildCount();
                    int i9 = 0;
                    for (int i10 = 0; i10 < childCount; i10++) {
                        int measuredHeight = getChildAt(i10).getMeasuredHeight() + paddingTop;
                        if (measuredHeight > i9) {
                            i9 = measuredHeight;
                        }
                    }
                    setMeasuredDimension(size, i9);
                    return;
                }
                setMeasuredDimension(size, i3);
                return;
            }
            throw new IllegalStateException(getClass().getSimpleName() + " can only be used with android:layout_height=\"wrap_content\"");
        }
        throw new IllegalStateException(getClass().getSimpleName() + " can only be used with android:layout_width=\"match_parent\" (or fill_parent)");
    }

    @Override // defpackage.n4
    public void setContentHeight(int i) {
        this.i0 = i;
    }

    public void setCustomView(View view) {
        LinearLayout linearLayout;
        View view2 = this.q0;
        if (view2 != null) {
            removeView(view2);
        }
        this.q0 = view;
        if (view != null && (linearLayout = this.r0) != null) {
            removeView(linearLayout);
            this.r0 = null;
        }
        if (view != null) {
            addView(view);
        }
        requestLayout();
    }

    public void setSubtitle(CharSequence charSequence) {
        this.n0 = charSequence;
        i();
    }

    public void setTitle(CharSequence charSequence) {
        this.m0 = charSequence;
        i();
    }

    public void setTitleOptional(boolean z) {
        if (z != this.w0) {
            requestLayout();
        }
        this.w0 = z;
    }

    @Override // defpackage.n4, android.view.View
    public /* bridge */ /* synthetic */ void setVisibility(int i) {
        super.setVisibility(i);
    }

    @Override // android.view.ViewGroup
    public boolean shouldDelayChildPressedState() {
        return false;
    }

    public ActionBarContextView(Context context, AttributeSet attributeSet) {
        this(context, attributeSet, jy2.actionModeStyle);
    }

    public ActionBarContextView(Context context, AttributeSet attributeSet, int i) {
        super(context, attributeSet, i);
        l64 v = l64.v(context, attributeSet, d33.ActionMode, i, 0);
        ei4.w0(this, v.g(d33.ActionMode_background));
        this.u0 = v.n(d33.ActionMode_titleTextStyle, 0);
        this.v0 = v.n(d33.ActionMode_subtitleTextStyle, 0);
        this.i0 = v.m(d33.ActionMode_height, 0);
        this.x0 = v.n(d33.ActionMode_closeItemLayout, d13.abc_action_mode_close_item_material);
        v.w();
    }
}
