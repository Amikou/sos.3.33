package androidx.appcompat.widget;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.annotation.SuppressLint;
import android.content.Context;
import android.content.res.Configuration;
import android.content.res.TypedArray;
import android.graphics.Canvas;
import android.graphics.Rect;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.util.AttributeSet;
import android.view.Menu;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewPropertyAnimator;
import android.view.Window;
import android.view.WindowInsets;
import android.widget.OverScroller;
import androidx.appcompat.view.menu.i;
import com.github.mikephil.charting.utils.Utils;
import defpackage.jp4;

@SuppressLint({"UnknownNullness"})
/* loaded from: classes.dex */
public class ActionBarOverlayLayout extends ViewGroup implements kf0, te2, re2, se2 {
    public static final int[] J0 = {jy2.actionBarSize, 16842841};
    public jp4 A0;
    public jp4 B0;
    public d C0;
    public OverScroller D0;
    public ViewPropertyAnimator E0;
    public final AnimatorListenerAdapter F0;
    public final Runnable G0;
    public final Runnable H0;
    public final ue2 I0;
    public int a;
    public int f0;
    public ContentFrameLayout g0;
    public ActionBarContainer h0;
    public lf0 i0;
    public Drawable j0;
    public boolean k0;
    public boolean l0;
    public boolean m0;
    public boolean n0;
    public boolean o0;
    public int p0;
    public int q0;
    public final Rect r0;
    public final Rect s0;
    public final Rect t0;
    public final Rect u0;
    public final Rect v0;
    public final Rect w0;
    public final Rect x0;
    public jp4 y0;
    public jp4 z0;

    /* loaded from: classes.dex */
    public static class LayoutParams extends ViewGroup.MarginLayoutParams {
        public LayoutParams(Context context, AttributeSet attributeSet) {
            super(context, attributeSet);
        }

        public LayoutParams(int i, int i2) {
            super(i, i2);
        }

        public LayoutParams(ViewGroup.LayoutParams layoutParams) {
            super(layoutParams);
        }
    }

    /* loaded from: classes.dex */
    public class a extends AnimatorListenerAdapter {
        public a() {
        }

        @Override // android.animation.AnimatorListenerAdapter, android.animation.Animator.AnimatorListener
        public void onAnimationCancel(Animator animator) {
            ActionBarOverlayLayout actionBarOverlayLayout = ActionBarOverlayLayout.this;
            actionBarOverlayLayout.E0 = null;
            actionBarOverlayLayout.o0 = false;
        }

        @Override // android.animation.AnimatorListenerAdapter, android.animation.Animator.AnimatorListener
        public void onAnimationEnd(Animator animator) {
            ActionBarOverlayLayout actionBarOverlayLayout = ActionBarOverlayLayout.this;
            actionBarOverlayLayout.E0 = null;
            actionBarOverlayLayout.o0 = false;
        }
    }

    /* loaded from: classes.dex */
    public class b implements Runnable {
        public b() {
        }

        @Override // java.lang.Runnable
        public void run() {
            ActionBarOverlayLayout.this.m();
            ActionBarOverlayLayout actionBarOverlayLayout = ActionBarOverlayLayout.this;
            actionBarOverlayLayout.E0 = actionBarOverlayLayout.h0.animate().translationY(Utils.FLOAT_EPSILON).setListener(ActionBarOverlayLayout.this.F0);
        }
    }

    /* loaded from: classes.dex */
    public class c implements Runnable {
        public c() {
        }

        @Override // java.lang.Runnable
        public void run() {
            ActionBarOverlayLayout.this.m();
            ActionBarOverlayLayout actionBarOverlayLayout = ActionBarOverlayLayout.this;
            actionBarOverlayLayout.E0 = actionBarOverlayLayout.h0.animate().translationY(-ActionBarOverlayLayout.this.h0.getHeight()).setListener(ActionBarOverlayLayout.this.F0);
        }
    }

    /* loaded from: classes.dex */
    public interface d {
        void a();

        void b();

        void c(boolean z);

        void d();

        void e();

        void f(int i);
    }

    public ActionBarOverlayLayout(Context context) {
        this(context, null);
    }

    @Override // defpackage.kf0
    public boolean a() {
        r();
        return this.i0.a();
    }

    @Override // defpackage.kf0
    public boolean b() {
        r();
        return this.i0.b();
    }

    @Override // defpackage.kf0
    public boolean c() {
        r();
        return this.i0.c();
    }

    @Override // android.view.ViewGroup
    public boolean checkLayoutParams(ViewGroup.LayoutParams layoutParams) {
        return layoutParams instanceof LayoutParams;
    }

    @Override // defpackage.kf0
    public boolean d() {
        r();
        return this.i0.d();
    }

    @Override // android.view.View
    public void draw(Canvas canvas) {
        super.draw(canvas);
        if (this.j0 == null || this.k0) {
            return;
        }
        int bottom = this.h0.getVisibility() == 0 ? (int) (this.h0.getBottom() + this.h0.getTranslationY() + 0.5f) : 0;
        this.j0.setBounds(0, bottom, getWidth(), this.j0.getIntrinsicHeight() + bottom);
        this.j0.draw(canvas);
    }

    @Override // defpackage.kf0
    public boolean e() {
        r();
        return this.i0.e();
    }

    @Override // defpackage.kf0
    public void f(int i) {
        r();
        if (i == 2) {
            this.i0.s();
        } else if (i == 5) {
            this.i0.t();
        } else if (i != 109) {
        } else {
            setOverlayMode(true);
        }
    }

    @Override // android.view.View
    public boolean fitSystemWindows(Rect rect) {
        if (Build.VERSION.SDK_INT >= 21) {
            return super.fitSystemWindows(rect);
        }
        r();
        boolean i = i(this.h0, rect, true, true, false, true);
        this.u0.set(rect);
        ok4.a(this, this.u0, this.r0);
        if (!this.v0.equals(this.u0)) {
            this.v0.set(this.u0);
            i = true;
        }
        if (!this.s0.equals(this.r0)) {
            this.s0.set(this.r0);
            i = true;
        }
        if (i) {
            requestLayout();
        }
        return true;
    }

    @Override // defpackage.kf0
    public void g() {
        r();
        this.i0.f();
    }

    public int getActionBarHideOffset() {
        ActionBarContainer actionBarContainer = this.h0;
        if (actionBarContainer != null) {
            return -((int) actionBarContainer.getTranslationY());
        }
        return 0;
    }

    @Override // android.view.ViewGroup
    public int getNestedScrollAxes() {
        return this.I0.a();
    }

    public CharSequence getTitle() {
        r();
        return this.i0.getTitle();
    }

    public final void h() {
        m();
        this.H0.run();
    }

    /* JADX WARN: Removed duplicated region for block: B:13:0x0021  */
    /* JADX WARN: Removed duplicated region for block: B:17:0x002c  */
    /* JADX WARN: Removed duplicated region for block: B:9:0x0016  */
    /*
        Code decompiled incorrectly, please refer to instructions dump.
        To view partially-correct code enable 'Show inconsistent code' option in preferences
    */
    public final boolean i(android.view.View r3, android.graphics.Rect r4, boolean r5, boolean r6, boolean r7, boolean r8) {
        /*
            r2 = this;
            android.view.ViewGroup$LayoutParams r3 = r3.getLayoutParams()
            androidx.appcompat.widget.ActionBarOverlayLayout$LayoutParams r3 = (androidx.appcompat.widget.ActionBarOverlayLayout.LayoutParams) r3
            r0 = 1
            if (r5 == 0) goto L13
            int r5 = r3.leftMargin
            int r1 = r4.left
            if (r5 == r1) goto L13
            r3.leftMargin = r1
            r5 = r0
            goto L14
        L13:
            r5 = 0
        L14:
            if (r6 == 0) goto L1f
            int r6 = r3.topMargin
            int r1 = r4.top
            if (r6 == r1) goto L1f
            r3.topMargin = r1
            r5 = r0
        L1f:
            if (r8 == 0) goto L2a
            int r6 = r3.rightMargin
            int r8 = r4.right
            if (r6 == r8) goto L2a
            r3.rightMargin = r8
            r5 = r0
        L2a:
            if (r7 == 0) goto L35
            int r6 = r3.bottomMargin
            int r4 = r4.bottom
            if (r6 == r4) goto L35
            r3.bottomMargin = r4
            goto L36
        L35:
            r0 = r5
        L36:
            return r0
        */
        throw new UnsupportedOperationException("Method not decompiled: androidx.appcompat.widget.ActionBarOverlayLayout.i(android.view.View, android.graphics.Rect, boolean, boolean, boolean, boolean):boolean");
    }

    @Override // android.view.ViewGroup
    /* renamed from: j */
    public LayoutParams generateDefaultLayoutParams() {
        return new LayoutParams(-1, -1);
    }

    @Override // android.view.ViewGroup
    /* renamed from: k */
    public LayoutParams generateLayoutParams(AttributeSet attributeSet) {
        return new LayoutParams(getContext(), attributeSet);
    }

    public final lf0 l(View view) {
        if (view instanceof lf0) {
            return (lf0) view;
        }
        if (view instanceof Toolbar) {
            return ((Toolbar) view).getWrapper();
        }
        throw new IllegalStateException("Can't make a decor toolbar out of " + view.getClass().getSimpleName());
    }

    public void m() {
        removeCallbacks(this.G0);
        removeCallbacks(this.H0);
        ViewPropertyAnimator viewPropertyAnimator = this.E0;
        if (viewPropertyAnimator != null) {
            viewPropertyAnimator.cancel();
        }
    }

    public final void n(Context context) {
        TypedArray obtainStyledAttributes = getContext().getTheme().obtainStyledAttributes(J0);
        this.a = obtainStyledAttributes.getDimensionPixelSize(0, 0);
        Drawable drawable = obtainStyledAttributes.getDrawable(1);
        this.j0 = drawable;
        setWillNotDraw(drawable == null);
        obtainStyledAttributes.recycle();
        this.k0 = context.getApplicationInfo().targetSdkVersion < 19;
        this.D0 = new OverScroller(context);
    }

    public boolean o() {
        return this.l0;
    }

    @Override // android.view.View
    public WindowInsets onApplyWindowInsets(WindowInsets windowInsets) {
        r();
        jp4 y = jp4.y(windowInsets, this);
        boolean i = i(this.h0, new Rect(y.k(), y.m(), y.l(), y.j()), true, true, false, true);
        ei4.h(this, y, this.r0);
        Rect rect = this.r0;
        jp4 o = y.o(rect.left, rect.top, rect.right, rect.bottom);
        this.y0 = o;
        boolean z = true;
        if (!this.z0.equals(o)) {
            this.z0 = this.y0;
            i = true;
        }
        if (this.s0.equals(this.r0)) {
            z = i;
        } else {
            this.s0.set(this.r0);
        }
        if (z) {
            requestLayout();
        }
        return y.a().c().b().w();
    }

    @Override // android.view.View
    public void onConfigurationChanged(Configuration configuration) {
        super.onConfigurationChanged(configuration);
        n(getContext());
        ei4.q0(this);
    }

    @Override // android.view.ViewGroup, android.view.View
    public void onDetachedFromWindow() {
        super.onDetachedFromWindow();
        m();
    }

    @Override // android.view.ViewGroup, android.view.View
    public void onLayout(boolean z, int i, int i2, int i3, int i4) {
        int childCount = getChildCount();
        int paddingLeft = getPaddingLeft();
        int paddingTop = getPaddingTop();
        for (int i5 = 0; i5 < childCount; i5++) {
            View childAt = getChildAt(i5);
            if (childAt.getVisibility() != 8) {
                LayoutParams layoutParams = (LayoutParams) childAt.getLayoutParams();
                int measuredWidth = childAt.getMeasuredWidth();
                int measuredHeight = childAt.getMeasuredHeight();
                int i6 = ((ViewGroup.MarginLayoutParams) layoutParams).leftMargin + paddingLeft;
                int i7 = ((ViewGroup.MarginLayoutParams) layoutParams).topMargin + paddingTop;
                childAt.layout(i6, i7, measuredWidth + i6, measuredHeight + i7);
            }
        }
    }

    @Override // android.view.View
    public void onMeasure(int i, int i2) {
        int measuredHeight;
        r();
        measureChildWithMargins(this.h0, i, 0, i2, 0);
        LayoutParams layoutParams = (LayoutParams) this.h0.getLayoutParams();
        int max = Math.max(0, this.h0.getMeasuredWidth() + ((ViewGroup.MarginLayoutParams) layoutParams).leftMargin + ((ViewGroup.MarginLayoutParams) layoutParams).rightMargin);
        int max2 = Math.max(0, this.h0.getMeasuredHeight() + ((ViewGroup.MarginLayoutParams) layoutParams).topMargin + ((ViewGroup.MarginLayoutParams) layoutParams).bottomMargin);
        int combineMeasuredStates = View.combineMeasuredStates(0, this.h0.getMeasuredState());
        boolean z = (ei4.P(this) & 256) != 0;
        if (z) {
            measuredHeight = this.a;
            if (this.m0 && this.h0.getTabContainer() != null) {
                measuredHeight += this.a;
            }
        } else {
            measuredHeight = this.h0.getVisibility() != 8 ? this.h0.getMeasuredHeight() : 0;
        }
        this.t0.set(this.r0);
        int i3 = Build.VERSION.SDK_INT;
        if (i3 >= 21) {
            this.A0 = this.y0;
        } else {
            this.w0.set(this.u0);
        }
        if (!this.l0 && !z) {
            Rect rect = this.t0;
            rect.top += measuredHeight;
            rect.bottom += 0;
            if (i3 >= 21) {
                this.A0 = this.A0.o(0, measuredHeight, 0, 0);
            }
        } else if (i3 >= 21) {
            this.A0 = new jp4.b(this.A0).c(cr1.b(this.A0.k(), this.A0.m() + measuredHeight, this.A0.l(), this.A0.j() + 0)).a();
        } else {
            Rect rect2 = this.w0;
            rect2.top += measuredHeight;
            rect2.bottom += 0;
        }
        i(this.g0, this.t0, true, true, true, true);
        if (i3 >= 21 && !this.B0.equals(this.A0)) {
            jp4 jp4Var = this.A0;
            this.B0 = jp4Var;
            ei4.i(this.g0, jp4Var);
        } else if (i3 < 21 && !this.x0.equals(this.w0)) {
            this.x0.set(this.w0);
            this.g0.a(this.w0);
        }
        measureChildWithMargins(this.g0, i, 0, i2, 0);
        LayoutParams layoutParams2 = (LayoutParams) this.g0.getLayoutParams();
        int max3 = Math.max(max, this.g0.getMeasuredWidth() + ((ViewGroup.MarginLayoutParams) layoutParams2).leftMargin + ((ViewGroup.MarginLayoutParams) layoutParams2).rightMargin);
        int max4 = Math.max(max2, this.g0.getMeasuredHeight() + ((ViewGroup.MarginLayoutParams) layoutParams2).topMargin + ((ViewGroup.MarginLayoutParams) layoutParams2).bottomMargin);
        int combineMeasuredStates2 = View.combineMeasuredStates(combineMeasuredStates, this.g0.getMeasuredState());
        setMeasuredDimension(View.resolveSizeAndState(Math.max(max3 + getPaddingLeft() + getPaddingRight(), getSuggestedMinimumWidth()), i, combineMeasuredStates2), View.resolveSizeAndState(Math.max(max4 + getPaddingTop() + getPaddingBottom(), getSuggestedMinimumHeight()), i2, combineMeasuredStates2 << 16));
    }

    @Override // android.view.ViewGroup, android.view.ViewParent, defpackage.te2
    public boolean onNestedFling(View view, float f, float f2, boolean z) {
        if (this.n0 && z) {
            if (t(f2)) {
                h();
            } else {
                s();
            }
            this.o0 = true;
            return true;
        }
        return false;
    }

    @Override // android.view.ViewGroup, android.view.ViewParent, defpackage.te2
    public boolean onNestedPreFling(View view, float f, float f2) {
        return false;
    }

    @Override // android.view.ViewGroup, android.view.ViewParent, defpackage.te2
    public void onNestedPreScroll(View view, int i, int i2, int[] iArr) {
    }

    @Override // defpackage.re2
    public void onNestedPreScroll(View view, int i, int i2, int[] iArr, int i3) {
        if (i3 == 0) {
            onNestedPreScroll(view, i, i2, iArr);
        }
    }

    @Override // defpackage.se2
    public void onNestedScroll(View view, int i, int i2, int i3, int i4, int i5, int[] iArr) {
        onNestedScroll(view, i, i2, i3, i4, i5);
    }

    @Override // defpackage.re2
    public void onNestedScrollAccepted(View view, View view2, int i, int i2) {
        if (i2 == 0) {
            onNestedScrollAccepted(view, view2, i);
        }
    }

    @Override // defpackage.re2
    public boolean onStartNestedScroll(View view, View view2, int i, int i2) {
        return i2 == 0 && onStartNestedScroll(view, view2, i);
    }

    @Override // defpackage.re2
    public void onStopNestedScroll(View view, int i) {
        if (i == 0) {
            onStopNestedScroll(view);
        }
    }

    @Override // android.view.View
    @Deprecated
    public void onWindowSystemUiVisibilityChanged(int i) {
        if (Build.VERSION.SDK_INT >= 16) {
            super.onWindowSystemUiVisibilityChanged(i);
        }
        r();
        int i2 = this.q0 ^ i;
        this.q0 = i;
        boolean z = (i & 4) == 0;
        boolean z2 = (i & 256) != 0;
        d dVar = this.C0;
        if (dVar != null) {
            dVar.c(!z2);
            if (!z && z2) {
                this.C0.d();
            } else {
                this.C0.a();
            }
        }
        if ((i2 & 256) == 0 || this.C0 == null) {
            return;
        }
        ei4.q0(this);
    }

    @Override // android.view.View
    public void onWindowVisibilityChanged(int i) {
        super.onWindowVisibilityChanged(i);
        this.f0 = i;
        d dVar = this.C0;
        if (dVar != null) {
            dVar.f(i);
        }
    }

    public final void p() {
        m();
        postDelayed(this.H0, 600L);
    }

    public final void q() {
        m();
        postDelayed(this.G0, 600L);
    }

    public void r() {
        if (this.g0 == null) {
            this.g0 = (ContentFrameLayout) findViewById(q03.action_bar_activity_content);
            this.h0 = (ActionBarContainer) findViewById(q03.action_bar_container);
            this.i0 = l(findViewById(q03.action_bar));
        }
    }

    public final void s() {
        m();
        this.G0.run();
    }

    public void setActionBarHideOffset(int i) {
        m();
        this.h0.setTranslationY(-Math.max(0, Math.min(i, this.h0.getHeight())));
    }

    public void setActionBarVisibilityCallback(d dVar) {
        this.C0 = dVar;
        if (getWindowToken() != null) {
            this.C0.f(this.f0);
            int i = this.q0;
            if (i != 0) {
                onWindowSystemUiVisibilityChanged(i);
                ei4.q0(this);
            }
        }
    }

    public void setHasNonEmbeddedTabs(boolean z) {
        this.m0 = z;
    }

    public void setHideOnContentScrollEnabled(boolean z) {
        if (z != this.n0) {
            this.n0 = z;
            if (z) {
                return;
            }
            m();
            setActionBarHideOffset(0);
        }
    }

    public void setIcon(int i) {
        r();
        this.i0.setIcon(i);
    }

    public void setLogo(int i) {
        r();
        this.i0.k(i);
    }

    @Override // defpackage.kf0
    public void setMenu(Menu menu, i.a aVar) {
        r();
        this.i0.setMenu(menu, aVar);
    }

    @Override // defpackage.kf0
    public void setMenuPrepared() {
        r();
        this.i0.setMenuPrepared();
    }

    public void setOverlayMode(boolean z) {
        this.l0 = z;
        this.k0 = z && getContext().getApplicationInfo().targetSdkVersion < 19;
    }

    public void setShowingForActionMode(boolean z) {
    }

    public void setUiOptions(int i) {
    }

    @Override // defpackage.kf0
    public void setWindowCallback(Window.Callback callback) {
        r();
        this.i0.setWindowCallback(callback);
    }

    @Override // defpackage.kf0
    public void setWindowTitle(CharSequence charSequence) {
        r();
        this.i0.setWindowTitle(charSequence);
    }

    @Override // android.view.ViewGroup
    public boolean shouldDelayChildPressedState() {
        return false;
    }

    public final boolean t(float f) {
        this.D0.fling(0, 0, 0, (int) f, 0, 0, Integer.MIN_VALUE, Integer.MAX_VALUE);
        return this.D0.getFinalY() > this.h0.getHeight();
    }

    public ActionBarOverlayLayout(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        this.f0 = 0;
        this.r0 = new Rect();
        this.s0 = new Rect();
        this.t0 = new Rect();
        this.u0 = new Rect();
        this.v0 = new Rect();
        this.w0 = new Rect();
        this.x0 = new Rect();
        jp4 jp4Var = jp4.b;
        this.y0 = jp4Var;
        this.z0 = jp4Var;
        this.A0 = jp4Var;
        this.B0 = jp4Var;
        this.F0 = new a();
        this.G0 = new b();
        this.H0 = new c();
        n(context);
        this.I0 = new ue2(this);
    }

    @Override // android.view.ViewGroup
    public ViewGroup.LayoutParams generateLayoutParams(ViewGroup.LayoutParams layoutParams) {
        return new LayoutParams(layoutParams);
    }

    @Override // defpackage.re2
    public void onNestedScroll(View view, int i, int i2, int i3, int i4, int i5) {
        if (i5 == 0) {
            onNestedScroll(view, i, i2, i3, i4);
        }
    }

    @Override // android.view.ViewGroup, android.view.ViewParent, defpackage.te2
    public void onNestedScrollAccepted(View view, View view2, int i) {
        this.I0.b(view, view2, i);
        this.p0 = getActionBarHideOffset();
        m();
        d dVar = this.C0;
        if (dVar != null) {
            dVar.e();
        }
    }

    @Override // android.view.ViewGroup, android.view.ViewParent, defpackage.te2
    public boolean onStartNestedScroll(View view, View view2, int i) {
        if ((i & 2) == 0 || this.h0.getVisibility() != 0) {
            return false;
        }
        return this.n0;
    }

    @Override // android.view.ViewGroup, android.view.ViewParent, defpackage.te2
    public void onStopNestedScroll(View view) {
        if (this.n0 && !this.o0) {
            if (this.p0 <= this.h0.getHeight()) {
                q();
            } else {
                p();
            }
        }
        d dVar = this.C0;
        if (dVar != null) {
            dVar.b();
        }
    }

    @Override // android.view.ViewGroup, android.view.ViewParent, defpackage.te2
    public void onNestedScroll(View view, int i, int i2, int i3, int i4) {
        int i5 = this.p0 + i2;
        this.p0 = i5;
        setActionBarHideOffset(i5);
    }

    public void setIcon(Drawable drawable) {
        r();
        this.i0.setIcon(drawable);
    }
}
