package androidx.appcompat.widget;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewParent;
import android.view.Window;
import androidx.appcompat.view.menu.e;
import androidx.appcompat.view.menu.i;
import androidx.appcompat.widget.Toolbar;
import com.github.mikephil.charting.utils.Utils;

/* compiled from: ToolbarWidgetWrapper.java */
/* loaded from: classes.dex */
public class g implements lf0 {
    public Toolbar a;
    public int b;
    public View c;
    public View d;
    public Drawable e;
    public Drawable f;
    public Drawable g;
    public boolean h;
    public CharSequence i;
    public CharSequence j;
    public CharSequence k;
    public Window.Callback l;
    public boolean m;
    public ActionMenuPresenter n;
    public int o;
    public int p;
    public Drawable q;

    /* compiled from: ToolbarWidgetWrapper.java */
    /* loaded from: classes.dex */
    public class a implements View.OnClickListener {
        public final j6 a;

        public a() {
            this.a = new j6(g.this.a.getContext(), 0, 16908332, 0, 0, g.this.i);
        }

        @Override // android.view.View.OnClickListener
        public void onClick(View view) {
            g gVar = g.this;
            Window.Callback callback = gVar.l;
            if (callback == null || !gVar.m) {
                return;
            }
            callback.onMenuItemSelected(0, this.a);
        }
    }

    /* compiled from: ToolbarWidgetWrapper.java */
    /* loaded from: classes.dex */
    public class b extends yj4 {
        public boolean a = false;
        public final /* synthetic */ int b;

        public b(int i) {
            this.b = i;
        }

        @Override // defpackage.yj4, defpackage.xj4
        public void a(View view) {
            this.a = true;
        }

        @Override // defpackage.xj4
        public void b(View view) {
            if (this.a) {
                return;
            }
            g.this.a.setVisibility(this.b);
        }

        @Override // defpackage.yj4, defpackage.xj4
        public void c(View view) {
            g.this.a.setVisibility(0);
        }
    }

    public g(Toolbar toolbar, boolean z) {
        this(toolbar, z, u13.abc_action_bar_up_description, vz2.abc_ic_ab_back_material);
    }

    public void A(CharSequence charSequence) {
        this.k = charSequence;
        D();
    }

    public void B(CharSequence charSequence) {
        this.j = charSequence;
        if ((this.b & 8) != 0) {
            this.a.setSubtitle(charSequence);
        }
    }

    public final void C(CharSequence charSequence) {
        this.i = charSequence;
        if ((this.b & 8) != 0) {
            this.a.setTitle(charSequence);
        }
    }

    public final void D() {
        if ((this.b & 4) != 0) {
            if (TextUtils.isEmpty(this.k)) {
                this.a.setNavigationContentDescription(this.p);
            } else {
                this.a.setNavigationContentDescription(this.k);
            }
        }
    }

    public final void E() {
        if ((this.b & 4) != 0) {
            Toolbar toolbar = this.a;
            Drawable drawable = this.g;
            if (drawable == null) {
                drawable = this.q;
            }
            toolbar.setNavigationIcon(drawable);
            return;
        }
        this.a.setNavigationIcon((Drawable) null);
    }

    public final void F() {
        Drawable drawable;
        int i = this.b;
        if ((i & 2) == 0) {
            drawable = null;
        } else if ((i & 1) != 0) {
            drawable = this.f;
            if (drawable == null) {
                drawable = this.e;
            }
        } else {
            drawable = this.e;
        }
        this.a.setLogo(drawable);
    }

    @Override // defpackage.lf0
    public boolean a() {
        return this.a.A();
    }

    @Override // defpackage.lf0
    public boolean b() {
        return this.a.d();
    }

    @Override // defpackage.lf0
    public boolean c() {
        return this.a.z();
    }

    @Override // defpackage.lf0
    public void collapseActionView() {
        this.a.e();
    }

    @Override // defpackage.lf0
    public boolean d() {
        return this.a.w();
    }

    @Override // defpackage.lf0
    public boolean e() {
        return this.a.J();
    }

    @Override // defpackage.lf0
    public void f() {
        this.a.f();
    }

    @Override // defpackage.lf0
    public void g(ScrollingTabContainerView scrollingTabContainerView) {
        View view = this.c;
        if (view != null) {
            ViewParent parent = view.getParent();
            Toolbar toolbar = this.a;
            if (parent == toolbar) {
                toolbar.removeView(this.c);
            }
        }
        this.c = scrollingTabContainerView;
        if (scrollingTabContainerView == null || this.o != 2) {
            return;
        }
        this.a.addView(scrollingTabContainerView, 0);
        Toolbar.LayoutParams layoutParams = (Toolbar.LayoutParams) this.c.getLayoutParams();
        ((ViewGroup.MarginLayoutParams) layoutParams).width = -2;
        ((ViewGroup.MarginLayoutParams) layoutParams).height = -2;
        layoutParams.a = 8388691;
        scrollingTabContainerView.setAllowCollapse(true);
    }

    @Override // defpackage.lf0
    public Context getContext() {
        return this.a.getContext();
    }

    @Override // defpackage.lf0
    public CharSequence getTitle() {
        return this.a.getTitle();
    }

    @Override // defpackage.lf0
    public boolean h() {
        return this.a.v();
    }

    @Override // defpackage.lf0
    public void i(int i) {
        View view;
        int i2 = this.b ^ i;
        this.b = i;
        if (i2 != 0) {
            if ((i2 & 4) != 0) {
                if ((i & 4) != 0) {
                    D();
                }
                E();
            }
            if ((i2 & 3) != 0) {
                F();
            }
            if ((i2 & 8) != 0) {
                if ((i & 8) != 0) {
                    this.a.setTitle(this.i);
                    this.a.setSubtitle(this.j);
                } else {
                    this.a.setTitle((CharSequence) null);
                    this.a.setSubtitle((CharSequence) null);
                }
            }
            if ((i2 & 16) == 0 || (view = this.d) == null) {
                return;
            }
            if ((i & 16) != 0) {
                this.a.addView(view);
            } else {
                this.a.removeView(view);
            }
        }
    }

    @Override // defpackage.lf0
    public Menu j() {
        return this.a.getMenu();
    }

    @Override // defpackage.lf0
    public void k(int i) {
        z(i != 0 ? mf.d(getContext(), i) : null);
    }

    @Override // defpackage.lf0
    public int l() {
        return this.o;
    }

    @Override // defpackage.lf0
    public vj4 m(int i, long j) {
        return ei4.e(this.a).a(i == 0 ? 1.0f : Utils.FLOAT_EPSILON).d(j).f(new b(i));
    }

    @Override // defpackage.lf0
    public void n(i.a aVar, e.a aVar2) {
        this.a.setMenuCallbacks(aVar, aVar2);
    }

    @Override // defpackage.lf0
    public ViewGroup o() {
        return this.a;
    }

    @Override // defpackage.lf0
    public void p(boolean z) {
    }

    @Override // defpackage.lf0
    public int q() {
        return this.b;
    }

    @Override // defpackage.lf0
    public void r(int i) {
        A(i == 0 ? null : getContext().getString(i));
    }

    @Override // defpackage.lf0
    public void s() {
    }

    @Override // defpackage.lf0
    public void setIcon(int i) {
        setIcon(i != 0 ? mf.d(getContext(), i) : null);
    }

    @Override // defpackage.lf0
    public void setMenu(Menu menu, i.a aVar) {
        if (this.n == null) {
            ActionMenuPresenter actionMenuPresenter = new ActionMenuPresenter(this.a.getContext());
            this.n = actionMenuPresenter;
            actionMenuPresenter.s(q03.action_menu_presenter);
        }
        this.n.h(aVar);
        this.a.setMenu((androidx.appcompat.view.menu.e) menu, this.n);
    }

    @Override // defpackage.lf0
    public void setMenuPrepared() {
        this.m = true;
    }

    @Override // defpackage.lf0
    public void setTitle(CharSequence charSequence) {
        this.h = true;
        C(charSequence);
    }

    @Override // defpackage.lf0
    public void setVisibility(int i) {
        this.a.setVisibility(i);
    }

    @Override // defpackage.lf0
    public void setWindowCallback(Window.Callback callback) {
        this.l = callback;
    }

    @Override // defpackage.lf0
    public void setWindowTitle(CharSequence charSequence) {
        if (this.h) {
            return;
        }
        C(charSequence);
    }

    @Override // defpackage.lf0
    public void t() {
    }

    @Override // defpackage.lf0
    public void u(Drawable drawable) {
        this.g = drawable;
        E();
    }

    @Override // defpackage.lf0
    public void v(boolean z) {
        this.a.setCollapsible(z);
    }

    public final int w() {
        if (this.a.getNavigationIcon() != null) {
            this.q = this.a.getNavigationIcon();
            return 15;
        }
        return 11;
    }

    public void x(View view) {
        View view2 = this.d;
        if (view2 != null && (this.b & 16) != 0) {
            this.a.removeView(view2);
        }
        this.d = view;
        if (view == null || (this.b & 16) == 0) {
            return;
        }
        this.a.addView(view);
    }

    public void y(int i) {
        if (i == this.p) {
            return;
        }
        this.p = i;
        if (TextUtils.isEmpty(this.a.getNavigationContentDescription())) {
            r(this.p);
        }
    }

    public void z(Drawable drawable) {
        this.f = drawable;
        F();
    }

    public g(Toolbar toolbar, boolean z, int i, int i2) {
        Drawable drawable;
        this.o = 0;
        this.p = 0;
        this.a = toolbar;
        this.i = toolbar.getTitle();
        this.j = toolbar.getSubtitle();
        this.h = this.i != null;
        this.g = toolbar.getNavigationIcon();
        l64 v = l64.v(toolbar.getContext(), null, d33.ActionBar, jy2.actionBarStyle, 0);
        this.q = v.g(d33.ActionBar_homeAsUpIndicator);
        if (z) {
            CharSequence p = v.p(d33.ActionBar_title);
            if (!TextUtils.isEmpty(p)) {
                setTitle(p);
            }
            CharSequence p2 = v.p(d33.ActionBar_subtitle);
            if (!TextUtils.isEmpty(p2)) {
                B(p2);
            }
            Drawable g = v.g(d33.ActionBar_logo);
            if (g != null) {
                z(g);
            }
            Drawable g2 = v.g(d33.ActionBar_icon);
            if (g2 != null) {
                setIcon(g2);
            }
            if (this.g == null && (drawable = this.q) != null) {
                u(drawable);
            }
            i(v.k(d33.ActionBar_displayOptions, 0));
            int n = v.n(d33.ActionBar_customNavigationLayout, 0);
            if (n != 0) {
                x(LayoutInflater.from(this.a.getContext()).inflate(n, (ViewGroup) this.a, false));
                i(this.b | 16);
            }
            int m = v.m(d33.ActionBar_height, 0);
            if (m > 0) {
                ViewGroup.LayoutParams layoutParams = this.a.getLayoutParams();
                layoutParams.height = m;
                this.a.setLayoutParams(layoutParams);
            }
            int e = v.e(d33.ActionBar_contentInsetStart, -1);
            int e2 = v.e(d33.ActionBar_contentInsetEnd, -1);
            if (e >= 0 || e2 >= 0) {
                this.a.setContentInsetsRelative(Math.max(e, 0), Math.max(e2, 0));
            }
            int n2 = v.n(d33.ActionBar_titleTextStyle, 0);
            if (n2 != 0) {
                Toolbar toolbar2 = this.a;
                toolbar2.setTitleTextAppearance(toolbar2.getContext(), n2);
            }
            int n3 = v.n(d33.ActionBar_subtitleTextStyle, 0);
            if (n3 != 0) {
                Toolbar toolbar3 = this.a;
                toolbar3.setSubtitleTextAppearance(toolbar3.getContext(), n3);
            }
            int n4 = v.n(d33.ActionBar_popupTheme, 0);
            if (n4 != 0) {
                this.a.setPopupTheme(n4);
            }
        } else {
            this.b = w();
        }
        v.w();
        y(i);
        this.k = this.a.getNavigationContentDescription();
        this.a.setNavigationOnClickListener(new a());
    }

    @Override // defpackage.lf0
    public void setIcon(Drawable drawable) {
        this.e = drawable;
        F();
    }
}
