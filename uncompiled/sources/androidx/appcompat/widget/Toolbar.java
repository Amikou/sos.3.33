package androidx.appcompat.widget;

import android.content.Context;
import android.content.res.ColorStateList;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.os.Parcel;
import android.os.Parcelable;
import android.text.TextUtils;
import android.util.AttributeSet;
import android.view.ContextThemeWrapper;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewParent;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;
import androidx.appcompat.app.ActionBar;
import androidx.appcompat.view.menu.e;
import androidx.appcompat.view.menu.i;
import androidx.appcompat.view.menu.l;
import androidx.appcompat.widget.ActionMenuView;
import androidx.customview.view.AbsSavedState;
import java.util.ArrayList;
import java.util.List;

/* loaded from: classes.dex */
public class Toolbar extends ViewGroup {
    public int A0;
    public CharSequence B0;
    public CharSequence C0;
    public ColorStateList D0;
    public ColorStateList E0;
    public boolean F0;
    public boolean G0;
    public final ArrayList<View> H0;
    public final ArrayList<View> I0;
    public final int[] J0;
    public e K0;
    public final ActionMenuView.d L0;
    public g M0;
    public ActionMenuPresenter N0;
    public d O0;
    public i.a P0;
    public e.a Q0;
    public boolean R0;
    public final Runnable S0;
    public ActionMenuView a;
    public TextView f0;
    public TextView g0;
    public ImageButton h0;
    public ImageView i0;
    public Drawable j0;
    public CharSequence k0;
    public ImageButton l0;
    public View m0;
    public Context n0;
    public int o0;
    public int p0;
    public int q0;
    public int r0;
    public int s0;
    public int t0;
    public int u0;
    public int v0;
    public int w0;
    public y93 x0;
    public int y0;
    public int z0;

    /* loaded from: classes.dex */
    public class a implements ActionMenuView.d {
        public a() {
        }

        @Override // androidx.appcompat.widget.ActionMenuView.d
        public boolean onMenuItemClick(MenuItem menuItem) {
            e eVar = Toolbar.this.K0;
            if (eVar != null) {
                return eVar.onMenuItemClick(menuItem);
            }
            return false;
        }
    }

    /* loaded from: classes.dex */
    public class b implements Runnable {
        public b() {
        }

        @Override // java.lang.Runnable
        public void run() {
            Toolbar.this.J();
        }
    }

    /* loaded from: classes.dex */
    public class c implements View.OnClickListener {
        public c() {
        }

        @Override // android.view.View.OnClickListener
        public void onClick(View view) {
            Toolbar.this.e();
        }
    }

    /* loaded from: classes.dex */
    public class d implements i {
        public androidx.appcompat.view.menu.e a;
        public androidx.appcompat.view.menu.g f0;

        public d() {
        }

        @Override // androidx.appcompat.view.menu.i
        public void c(androidx.appcompat.view.menu.e eVar, boolean z) {
        }

        @Override // androidx.appcompat.view.menu.i
        public void d(boolean z) {
            if (this.f0 != null) {
                androidx.appcompat.view.menu.e eVar = this.a;
                boolean z2 = false;
                if (eVar != null) {
                    int size = eVar.size();
                    int i = 0;
                    while (true) {
                        if (i >= size) {
                            break;
                        } else if (this.a.getItem(i) == this.f0) {
                            z2 = true;
                            break;
                        } else {
                            i++;
                        }
                    }
                }
                if (z2) {
                    return;
                }
                f(this.a, this.f0);
            }
        }

        @Override // androidx.appcompat.view.menu.i
        public boolean e() {
            return false;
        }

        @Override // androidx.appcompat.view.menu.i
        public boolean f(androidx.appcompat.view.menu.e eVar, androidx.appcompat.view.menu.g gVar) {
            View view = Toolbar.this.m0;
            if (view instanceof n00) {
                ((n00) view).onActionViewCollapsed();
            }
            Toolbar toolbar = Toolbar.this;
            toolbar.removeView(toolbar.m0);
            Toolbar toolbar2 = Toolbar.this;
            toolbar2.removeView(toolbar2.l0);
            Toolbar toolbar3 = Toolbar.this;
            toolbar3.m0 = null;
            toolbar3.a();
            this.f0 = null;
            Toolbar.this.requestLayout();
            gVar.r(false);
            return true;
        }

        @Override // androidx.appcompat.view.menu.i
        public boolean g(androidx.appcompat.view.menu.e eVar, androidx.appcompat.view.menu.g gVar) {
            Toolbar.this.g();
            ViewParent parent = Toolbar.this.l0.getParent();
            Toolbar toolbar = Toolbar.this;
            if (parent != toolbar) {
                if (parent instanceof ViewGroup) {
                    ((ViewGroup) parent).removeView(toolbar.l0);
                }
                Toolbar toolbar2 = Toolbar.this;
                toolbar2.addView(toolbar2.l0);
            }
            Toolbar.this.m0 = gVar.getActionView();
            this.f0 = gVar;
            ViewParent parent2 = Toolbar.this.m0.getParent();
            Toolbar toolbar3 = Toolbar.this;
            if (parent2 != toolbar3) {
                if (parent2 instanceof ViewGroup) {
                    ((ViewGroup) parent2).removeView(toolbar3.m0);
                }
                LayoutParams generateDefaultLayoutParams = Toolbar.this.generateDefaultLayoutParams();
                Toolbar toolbar4 = Toolbar.this;
                generateDefaultLayoutParams.a = 8388611 | (toolbar4.r0 & 112);
                generateDefaultLayoutParams.b = 2;
                toolbar4.m0.setLayoutParams(generateDefaultLayoutParams);
                Toolbar toolbar5 = Toolbar.this;
                toolbar5.addView(toolbar5.m0);
            }
            Toolbar.this.G();
            Toolbar.this.requestLayout();
            gVar.r(true);
            View view = Toolbar.this.m0;
            if (view instanceof n00) {
                ((n00) view).onActionViewExpanded();
            }
            return true;
        }

        @Override // androidx.appcompat.view.menu.i
        public int getId() {
            return 0;
        }

        @Override // androidx.appcompat.view.menu.i
        public void i(Context context, androidx.appcompat.view.menu.e eVar) {
            androidx.appcompat.view.menu.g gVar;
            androidx.appcompat.view.menu.e eVar2 = this.a;
            if (eVar2 != null && (gVar = this.f0) != null) {
                eVar2.f(gVar);
            }
            this.a = eVar;
        }

        @Override // androidx.appcompat.view.menu.i
        public void j(Parcelable parcelable) {
        }

        @Override // androidx.appcompat.view.menu.i
        public boolean l(l lVar) {
            return false;
        }

        @Override // androidx.appcompat.view.menu.i
        public Parcelable m() {
            return null;
        }
    }

    /* loaded from: classes.dex */
    public interface e {
        boolean onMenuItemClick(MenuItem menuItem);
    }

    public Toolbar(Context context) {
        this(context, null);
    }

    private MenuInflater getMenuInflater() {
        return new kw3(getContext());
    }

    public boolean A() {
        ActionMenuView actionMenuView = this.a;
        return actionMenuView != null && actionMenuView.m();
    }

    public final int B(View view, int i, int[] iArr, int i2) {
        LayoutParams layoutParams = (LayoutParams) view.getLayoutParams();
        int i3 = ((ViewGroup.MarginLayoutParams) layoutParams).leftMargin - iArr[0];
        int max = i + Math.max(0, i3);
        iArr[0] = Math.max(0, -i3);
        int q = q(view, i2);
        int measuredWidth = view.getMeasuredWidth();
        view.layout(max, q, max + measuredWidth, view.getMeasuredHeight() + q);
        return max + measuredWidth + ((ViewGroup.MarginLayoutParams) layoutParams).rightMargin;
    }

    public final int C(View view, int i, int[] iArr, int i2) {
        LayoutParams layoutParams = (LayoutParams) view.getLayoutParams();
        int i3 = ((ViewGroup.MarginLayoutParams) layoutParams).rightMargin - iArr[1];
        int max = i - Math.max(0, i3);
        iArr[1] = Math.max(0, -i3);
        int q = q(view, i2);
        int measuredWidth = view.getMeasuredWidth();
        view.layout(max - measuredWidth, q, max, view.getMeasuredHeight() + q);
        return max - (measuredWidth + ((ViewGroup.MarginLayoutParams) layoutParams).leftMargin);
    }

    public final int D(View view, int i, int i2, int i3, int i4, int[] iArr) {
        ViewGroup.MarginLayoutParams marginLayoutParams = (ViewGroup.MarginLayoutParams) view.getLayoutParams();
        int i5 = marginLayoutParams.leftMargin - iArr[0];
        int i6 = marginLayoutParams.rightMargin - iArr[1];
        int max = Math.max(0, i5) + Math.max(0, i6);
        iArr[0] = Math.max(0, -i5);
        iArr[1] = Math.max(0, -i6);
        view.measure(ViewGroup.getChildMeasureSpec(i, getPaddingLeft() + getPaddingRight() + max + i2, marginLayoutParams.width), ViewGroup.getChildMeasureSpec(i3, getPaddingTop() + getPaddingBottom() + marginLayoutParams.topMargin + marginLayoutParams.bottomMargin + i4, marginLayoutParams.height));
        return view.getMeasuredWidth() + max;
    }

    public final void E(View view, int i, int i2, int i3, int i4, int i5) {
        ViewGroup.MarginLayoutParams marginLayoutParams = (ViewGroup.MarginLayoutParams) view.getLayoutParams();
        int childMeasureSpec = ViewGroup.getChildMeasureSpec(i, getPaddingLeft() + getPaddingRight() + marginLayoutParams.leftMargin + marginLayoutParams.rightMargin + i2, marginLayoutParams.width);
        int childMeasureSpec2 = ViewGroup.getChildMeasureSpec(i3, getPaddingTop() + getPaddingBottom() + marginLayoutParams.topMargin + marginLayoutParams.bottomMargin + i4, marginLayoutParams.height);
        int mode = View.MeasureSpec.getMode(childMeasureSpec2);
        if (mode != 1073741824 && i5 >= 0) {
            if (mode != 0) {
                i5 = Math.min(View.MeasureSpec.getSize(childMeasureSpec2), i5);
            }
            childMeasureSpec2 = View.MeasureSpec.makeMeasureSpec(i5, 1073741824);
        }
        view.measure(childMeasureSpec, childMeasureSpec2);
    }

    public final void F() {
        removeCallbacks(this.S0);
        post(this.S0);
    }

    public void G() {
        for (int childCount = getChildCount() - 1; childCount >= 0; childCount--) {
            View childAt = getChildAt(childCount);
            if (((LayoutParams) childAt.getLayoutParams()).b != 2 && childAt != this.a) {
                removeViewAt(childCount);
                this.I0.add(childAt);
            }
        }
    }

    public final boolean H() {
        if (this.R0) {
            int childCount = getChildCount();
            for (int i = 0; i < childCount; i++) {
                View childAt = getChildAt(i);
                if (I(childAt) && childAt.getMeasuredWidth() > 0 && childAt.getMeasuredHeight() > 0) {
                    return false;
                }
            }
            return true;
        }
        return false;
    }

    public final boolean I(View view) {
        return (view == null || view.getParent() != this || view.getVisibility() == 8) ? false : true;
    }

    public boolean J() {
        ActionMenuView actionMenuView = this.a;
        return actionMenuView != null && actionMenuView.r();
    }

    public void a() {
        for (int size = this.I0.size() - 1; size >= 0; size--) {
            addView(this.I0.get(size));
        }
        this.I0.clear();
    }

    public final void b(List<View> list, int i) {
        boolean z = ei4.E(this) == 1;
        int childCount = getChildCount();
        int b2 = ri1.b(i, ei4.E(this));
        list.clear();
        if (!z) {
            for (int i2 = 0; i2 < childCount; i2++) {
                View childAt = getChildAt(i2);
                LayoutParams layoutParams = (LayoutParams) childAt.getLayoutParams();
                if (layoutParams.b == 0 && I(childAt) && p(layoutParams.a) == b2) {
                    list.add(childAt);
                }
            }
            return;
        }
        for (int i3 = childCount - 1; i3 >= 0; i3--) {
            View childAt2 = getChildAt(i3);
            LayoutParams layoutParams2 = (LayoutParams) childAt2.getLayoutParams();
            if (layoutParams2.b == 0 && I(childAt2) && p(layoutParams2.a) == b2) {
                list.add(childAt2);
            }
        }
    }

    public final void c(View view, boolean z) {
        LayoutParams layoutParams;
        ViewGroup.LayoutParams layoutParams2 = view.getLayoutParams();
        if (layoutParams2 == null) {
            layoutParams = generateDefaultLayoutParams();
        } else if (!checkLayoutParams(layoutParams2)) {
            layoutParams = generateLayoutParams(layoutParams2);
        } else {
            layoutParams = (LayoutParams) layoutParams2;
        }
        layoutParams.b = 1;
        if (z && this.m0 != null) {
            view.setLayoutParams(layoutParams);
            this.I0.add(view);
            return;
        }
        addView(view, layoutParams);
    }

    @Override // android.view.ViewGroup
    public boolean checkLayoutParams(ViewGroup.LayoutParams layoutParams) {
        return super.checkLayoutParams(layoutParams) && (layoutParams instanceof LayoutParams);
    }

    public boolean d() {
        ActionMenuView actionMenuView;
        return getVisibility() == 0 && (actionMenuView = this.a) != null && actionMenuView.n();
    }

    public void e() {
        d dVar = this.O0;
        androidx.appcompat.view.menu.g gVar = dVar == null ? null : dVar.f0;
        if (gVar != null) {
            gVar.collapseActionView();
        }
    }

    public void f() {
        ActionMenuView actionMenuView = this.a;
        if (actionMenuView != null) {
            actionMenuView.e();
        }
    }

    public void g() {
        if (this.l0 == null) {
            AppCompatImageButton appCompatImageButton = new AppCompatImageButton(getContext(), null, jy2.toolbarNavigationButtonStyle);
            this.l0 = appCompatImageButton;
            appCompatImageButton.setImageDrawable(this.j0);
            this.l0.setContentDescription(this.k0);
            LayoutParams generateDefaultLayoutParams = generateDefaultLayoutParams();
            generateDefaultLayoutParams.a = 8388611 | (this.r0 & 112);
            generateDefaultLayoutParams.b = 2;
            this.l0.setLayoutParams(generateDefaultLayoutParams);
            this.l0.setOnClickListener(new c());
        }
    }

    public CharSequence getCollapseContentDescription() {
        ImageButton imageButton = this.l0;
        if (imageButton != null) {
            return imageButton.getContentDescription();
        }
        return null;
    }

    public Drawable getCollapseIcon() {
        ImageButton imageButton = this.l0;
        if (imageButton != null) {
            return imageButton.getDrawable();
        }
        return null;
    }

    public int getContentInsetEnd() {
        y93 y93Var = this.x0;
        if (y93Var != null) {
            return y93Var.a();
        }
        return 0;
    }

    public int getContentInsetEndWithActions() {
        int i = this.z0;
        return i != Integer.MIN_VALUE ? i : getContentInsetEnd();
    }

    public int getContentInsetLeft() {
        y93 y93Var = this.x0;
        if (y93Var != null) {
            return y93Var.b();
        }
        return 0;
    }

    public int getContentInsetRight() {
        y93 y93Var = this.x0;
        if (y93Var != null) {
            return y93Var.c();
        }
        return 0;
    }

    public int getContentInsetStart() {
        y93 y93Var = this.x0;
        if (y93Var != null) {
            return y93Var.d();
        }
        return 0;
    }

    public int getContentInsetStartWithNavigation() {
        int i = this.y0;
        return i != Integer.MIN_VALUE ? i : getContentInsetStart();
    }

    public int getCurrentContentInsetEnd() {
        androidx.appcompat.view.menu.e q;
        ActionMenuView actionMenuView = this.a;
        if ((actionMenuView == null || (q = actionMenuView.q()) == null || !q.hasVisibleItems()) ? false : true) {
            return Math.max(getContentInsetEnd(), Math.max(this.z0, 0));
        }
        return getContentInsetEnd();
    }

    public int getCurrentContentInsetLeft() {
        if (ei4.E(this) == 1) {
            return getCurrentContentInsetEnd();
        }
        return getCurrentContentInsetStart();
    }

    public int getCurrentContentInsetRight() {
        if (ei4.E(this) == 1) {
            return getCurrentContentInsetStart();
        }
        return getCurrentContentInsetEnd();
    }

    public int getCurrentContentInsetStart() {
        if (getNavigationIcon() != null) {
            return Math.max(getContentInsetStart(), Math.max(this.y0, 0));
        }
        return getContentInsetStart();
    }

    public Drawable getLogo() {
        ImageView imageView = this.i0;
        if (imageView != null) {
            return imageView.getDrawable();
        }
        return null;
    }

    public CharSequence getLogoDescription() {
        ImageView imageView = this.i0;
        if (imageView != null) {
            return imageView.getContentDescription();
        }
        return null;
    }

    public Menu getMenu() {
        j();
        return this.a.getMenu();
    }

    public CharSequence getNavigationContentDescription() {
        ImageButton imageButton = this.h0;
        if (imageButton != null) {
            return imageButton.getContentDescription();
        }
        return null;
    }

    public Drawable getNavigationIcon() {
        ImageButton imageButton = this.h0;
        if (imageButton != null) {
            return imageButton.getDrawable();
        }
        return null;
    }

    public ActionMenuPresenter getOuterActionMenuPresenter() {
        return this.N0;
    }

    public Drawable getOverflowIcon() {
        j();
        return this.a.getOverflowIcon();
    }

    Context getPopupContext() {
        return this.n0;
    }

    public int getPopupTheme() {
        return this.o0;
    }

    public CharSequence getSubtitle() {
        return this.C0;
    }

    public final TextView getSubtitleTextView() {
        return this.g0;
    }

    public CharSequence getTitle() {
        return this.B0;
    }

    public int getTitleMarginBottom() {
        return this.w0;
    }

    public int getTitleMarginEnd() {
        return this.u0;
    }

    public int getTitleMarginStart() {
        return this.t0;
    }

    public int getTitleMarginTop() {
        return this.v0;
    }

    public final TextView getTitleTextView() {
        return this.f0;
    }

    public lf0 getWrapper() {
        if (this.M0 == null) {
            this.M0 = new g(this, true);
        }
        return this.M0;
    }

    public final void h() {
        if (this.x0 == null) {
            this.x0 = new y93();
        }
    }

    public final void i() {
        if (this.i0 == null) {
            this.i0 = new AppCompatImageView(getContext());
        }
    }

    public final void j() {
        k();
        if (this.a.q() == null) {
            androidx.appcompat.view.menu.e eVar = (androidx.appcompat.view.menu.e) this.a.getMenu();
            if (this.O0 == null) {
                this.O0 = new d();
            }
            this.a.setExpandedActionViewsExclusive(true);
            eVar.c(this.O0, this.n0);
        }
    }

    public final void k() {
        if (this.a == null) {
            ActionMenuView actionMenuView = new ActionMenuView(getContext());
            this.a = actionMenuView;
            actionMenuView.setPopupTheme(this.o0);
            this.a.setOnMenuItemClickListener(this.L0);
            this.a.setMenuCallbacks(this.P0, this.Q0);
            LayoutParams generateDefaultLayoutParams = generateDefaultLayoutParams();
            generateDefaultLayoutParams.a = 8388613 | (this.r0 & 112);
            this.a.setLayoutParams(generateDefaultLayoutParams);
            c(this.a, false);
        }
    }

    public final void l() {
        if (this.h0 == null) {
            this.h0 = new AppCompatImageButton(getContext(), null, jy2.toolbarNavigationButtonStyle);
            LayoutParams generateDefaultLayoutParams = generateDefaultLayoutParams();
            generateDefaultLayoutParams.a = 8388611 | (this.r0 & 112);
            this.h0.setLayoutParams(generateDefaultLayoutParams);
        }
    }

    @Override // android.view.ViewGroup
    /* renamed from: m */
    public LayoutParams generateDefaultLayoutParams() {
        return new LayoutParams(-2, -2);
    }

    @Override // android.view.ViewGroup
    /* renamed from: n */
    public LayoutParams generateLayoutParams(AttributeSet attributeSet) {
        return new LayoutParams(getContext(), attributeSet);
    }

    @Override // android.view.ViewGroup
    /* renamed from: o */
    public LayoutParams generateLayoutParams(ViewGroup.LayoutParams layoutParams) {
        if (layoutParams instanceof LayoutParams) {
            return new LayoutParams((LayoutParams) layoutParams);
        }
        if (layoutParams instanceof ActionBar.LayoutParams) {
            return new LayoutParams((ActionBar.LayoutParams) layoutParams);
        }
        if (layoutParams instanceof ViewGroup.MarginLayoutParams) {
            return new LayoutParams((ViewGroup.MarginLayoutParams) layoutParams);
        }
        return new LayoutParams(layoutParams);
    }

    @Override // android.view.ViewGroup, android.view.View
    public void onDetachedFromWindow() {
        super.onDetachedFromWindow();
        removeCallbacks(this.S0);
    }

    @Override // android.view.View
    public boolean onHoverEvent(MotionEvent motionEvent) {
        int actionMasked = motionEvent.getActionMasked();
        if (actionMasked == 9) {
            this.G0 = false;
        }
        if (!this.G0) {
            boolean onHoverEvent = super.onHoverEvent(motionEvent);
            if (actionMasked == 9 && !onHoverEvent) {
                this.G0 = true;
            }
        }
        if (actionMasked == 10 || actionMasked == 3) {
            this.G0 = false;
        }
        return true;
    }

    /* JADX WARN: Removed duplicated region for block: B:105:0x029f A[LOOP:0: B:104:0x029d->B:105:0x029f, LOOP_END] */
    /* JADX WARN: Removed duplicated region for block: B:108:0x02c1 A[LOOP:1: B:107:0x02bf->B:108:0x02c1, LOOP_END] */
    /* JADX WARN: Removed duplicated region for block: B:112:0x02eb  */
    /* JADX WARN: Removed duplicated region for block: B:117:0x02fa A[LOOP:2: B:116:0x02f8->B:117:0x02fa, LOOP_END] */
    /* JADX WARN: Removed duplicated region for block: B:19:0x005f  */
    /* JADX WARN: Removed duplicated region for block: B:24:0x0076  */
    /* JADX WARN: Removed duplicated region for block: B:29:0x00b3  */
    /* JADX WARN: Removed duplicated region for block: B:34:0x00ca  */
    /* JADX WARN: Removed duplicated region for block: B:39:0x00e7  */
    /* JADX WARN: Removed duplicated region for block: B:40:0x0100  */
    /* JADX WARN: Removed duplicated region for block: B:42:0x0105  */
    /* JADX WARN: Removed duplicated region for block: B:43:0x011d  */
    /* JADX WARN: Removed duplicated region for block: B:50:0x012d  */
    /* JADX WARN: Removed duplicated region for block: B:51:0x0130  */
    /* JADX WARN: Removed duplicated region for block: B:53:0x0134  */
    /* JADX WARN: Removed duplicated region for block: B:54:0x0137  */
    /* JADX WARN: Removed duplicated region for block: B:66:0x0168  */
    /* JADX WARN: Removed duplicated region for block: B:76:0x01a6  */
    /* JADX WARN: Removed duplicated region for block: B:78:0x01b7  */
    /* JADX WARN: Removed duplicated region for block: B:91:0x0227  */
    @Override // android.view.ViewGroup, android.view.View
    /*
        Code decompiled incorrectly, please refer to instructions dump.
        To view partially-correct code enable 'Show inconsistent code' option in preferences
    */
    public void onLayout(boolean r20, int r21, int r22, int r23, int r24) {
        /*
            Method dump skipped, instructions count: 783
            To view this dump change 'Code comments level' option to 'DEBUG'
        */
        throw new UnsupportedOperationException("Method not decompiled: androidx.appcompat.widget.Toolbar.onLayout(boolean, int, int, int, int):void");
    }

    @Override // android.view.View
    public void onMeasure(int i, int i2) {
        int i3;
        int i4;
        int i5;
        int i6;
        int i7;
        int i8;
        int i9;
        int[] iArr = this.J0;
        boolean b2 = ok4.b(this);
        int i10 = !b2 ? 1 : 0;
        if (I(this.h0)) {
            E(this.h0, i, 0, i2, 0, this.s0);
            i3 = this.h0.getMeasuredWidth() + s(this.h0);
            i4 = Math.max(0, this.h0.getMeasuredHeight() + t(this.h0));
            i5 = View.combineMeasuredStates(0, this.h0.getMeasuredState());
        } else {
            i3 = 0;
            i4 = 0;
            i5 = 0;
        }
        if (I(this.l0)) {
            E(this.l0, i, 0, i2, 0, this.s0);
            i3 = this.l0.getMeasuredWidth() + s(this.l0);
            i4 = Math.max(i4, this.l0.getMeasuredHeight() + t(this.l0));
            i5 = View.combineMeasuredStates(i5, this.l0.getMeasuredState());
        }
        int currentContentInsetStart = getCurrentContentInsetStart();
        int max = 0 + Math.max(currentContentInsetStart, i3);
        iArr[b2 ? 1 : 0] = Math.max(0, currentContentInsetStart - i3);
        if (I(this.a)) {
            E(this.a, i, max, i2, 0, this.s0);
            i6 = this.a.getMeasuredWidth() + s(this.a);
            i4 = Math.max(i4, this.a.getMeasuredHeight() + t(this.a));
            i5 = View.combineMeasuredStates(i5, this.a.getMeasuredState());
        } else {
            i6 = 0;
        }
        int currentContentInsetEnd = getCurrentContentInsetEnd();
        int max2 = max + Math.max(currentContentInsetEnd, i6);
        iArr[i10] = Math.max(0, currentContentInsetEnd - i6);
        if (I(this.m0)) {
            max2 += D(this.m0, i, max2, i2, 0, iArr);
            i4 = Math.max(i4, this.m0.getMeasuredHeight() + t(this.m0));
            i5 = View.combineMeasuredStates(i5, this.m0.getMeasuredState());
        }
        if (I(this.i0)) {
            max2 += D(this.i0, i, max2, i2, 0, iArr);
            i4 = Math.max(i4, this.i0.getMeasuredHeight() + t(this.i0));
            i5 = View.combineMeasuredStates(i5, this.i0.getMeasuredState());
        }
        int childCount = getChildCount();
        for (int i11 = 0; i11 < childCount; i11++) {
            View childAt = getChildAt(i11);
            if (((LayoutParams) childAt.getLayoutParams()).b == 0 && I(childAt)) {
                max2 += D(childAt, i, max2, i2, 0, iArr);
                i4 = Math.max(i4, childAt.getMeasuredHeight() + t(childAt));
                i5 = View.combineMeasuredStates(i5, childAt.getMeasuredState());
            }
        }
        int i12 = this.v0 + this.w0;
        int i13 = this.t0 + this.u0;
        if (I(this.f0)) {
            D(this.f0, i, max2 + i13, i2, i12, iArr);
            int measuredWidth = this.f0.getMeasuredWidth() + s(this.f0);
            i7 = this.f0.getMeasuredHeight() + t(this.f0);
            i8 = View.combineMeasuredStates(i5, this.f0.getMeasuredState());
            i9 = measuredWidth;
        } else {
            i7 = 0;
            i8 = i5;
            i9 = 0;
        }
        if (I(this.g0)) {
            i9 = Math.max(i9, D(this.g0, i, max2 + i13, i2, i7 + i12, iArr));
            i7 += this.g0.getMeasuredHeight() + t(this.g0);
            i8 = View.combineMeasuredStates(i8, this.g0.getMeasuredState());
        }
        setMeasuredDimension(View.resolveSizeAndState(Math.max(max2 + i9 + getPaddingLeft() + getPaddingRight(), getSuggestedMinimumWidth()), i, (-16777216) & i8), H() ? 0 : View.resolveSizeAndState(Math.max(Math.max(i4, i7) + getPaddingTop() + getPaddingBottom(), getSuggestedMinimumHeight()), i2, i8 << 16));
    }

    @Override // android.view.View
    public void onRestoreInstanceState(Parcelable parcelable) {
        MenuItem findItem;
        if (!(parcelable instanceof SavedState)) {
            super.onRestoreInstanceState(parcelable);
            return;
        }
        SavedState savedState = (SavedState) parcelable;
        super.onRestoreInstanceState(savedState.a());
        ActionMenuView actionMenuView = this.a;
        androidx.appcompat.view.menu.e q = actionMenuView != null ? actionMenuView.q() : null;
        int i = savedState.g0;
        if (i != 0 && this.O0 != null && q != null && (findItem = q.findItem(i)) != null) {
            findItem.expandActionView();
        }
        if (savedState.h0) {
            F();
        }
    }

    @Override // android.view.View
    public void onRtlPropertiesChanged(int i) {
        if (Build.VERSION.SDK_INT >= 17) {
            super.onRtlPropertiesChanged(i);
        }
        h();
        this.x0.f(i == 1);
    }

    @Override // android.view.View
    public Parcelable onSaveInstanceState() {
        androidx.appcompat.view.menu.g gVar;
        SavedState savedState = new SavedState(super.onSaveInstanceState());
        d dVar = this.O0;
        if (dVar != null && (gVar = dVar.f0) != null) {
            savedState.g0 = gVar.getItemId();
        }
        savedState.h0 = A();
        return savedState;
    }

    @Override // android.view.View
    public boolean onTouchEvent(MotionEvent motionEvent) {
        int actionMasked = motionEvent.getActionMasked();
        if (actionMasked == 0) {
            this.F0 = false;
        }
        if (!this.F0) {
            boolean onTouchEvent = super.onTouchEvent(motionEvent);
            if (actionMasked == 0 && !onTouchEvent) {
                this.F0 = true;
            }
        }
        if (actionMasked == 1 || actionMasked == 3) {
            this.F0 = false;
        }
        return true;
    }

    public final int p(int i) {
        int E = ei4.E(this);
        int b2 = ri1.b(i, E) & 7;
        return (b2 == 1 || b2 == 3 || b2 == 5) ? b2 : E == 1 ? 5 : 3;
    }

    public final int q(View view, int i) {
        LayoutParams layoutParams = (LayoutParams) view.getLayoutParams();
        int measuredHeight = view.getMeasuredHeight();
        int i2 = i > 0 ? (measuredHeight - i) / 2 : 0;
        int r = r(layoutParams.a);
        if (r != 48) {
            if (r != 80) {
                int paddingTop = getPaddingTop();
                int paddingBottom = getPaddingBottom();
                int height = getHeight();
                int i3 = (((height - paddingTop) - paddingBottom) - measuredHeight) / 2;
                int i4 = ((ViewGroup.MarginLayoutParams) layoutParams).topMargin;
                if (i3 < i4) {
                    i3 = i4;
                } else {
                    int i5 = (((height - paddingBottom) - measuredHeight) - i3) - paddingTop;
                    int i6 = ((ViewGroup.MarginLayoutParams) layoutParams).bottomMargin;
                    if (i5 < i6) {
                        i3 = Math.max(0, i3 - (i6 - i5));
                    }
                }
                return paddingTop + i3;
            }
            return (((getHeight() - getPaddingBottom()) - measuredHeight) - ((ViewGroup.MarginLayoutParams) layoutParams).bottomMargin) - i2;
        }
        return getPaddingTop() - i2;
    }

    public final int r(int i) {
        int i2 = i & 112;
        return (i2 == 16 || i2 == 48 || i2 == 80) ? i2 : this.A0 & 112;
    }

    public final int s(View view) {
        ViewGroup.MarginLayoutParams marginLayoutParams = (ViewGroup.MarginLayoutParams) view.getLayoutParams();
        return b42.b(marginLayoutParams) + b42.a(marginLayoutParams);
    }

    public void setCollapseContentDescription(int i) {
        setCollapseContentDescription(i != 0 ? getContext().getText(i) : null);
    }

    public void setCollapseIcon(int i) {
        setCollapseIcon(mf.d(getContext(), i));
    }

    public void setCollapsible(boolean z) {
        this.R0 = z;
        requestLayout();
    }

    public void setContentInsetEndWithActions(int i) {
        if (i < 0) {
            i = Integer.MIN_VALUE;
        }
        if (i != this.z0) {
            this.z0 = i;
            if (getNavigationIcon() != null) {
                requestLayout();
            }
        }
    }

    public void setContentInsetStartWithNavigation(int i) {
        if (i < 0) {
            i = Integer.MIN_VALUE;
        }
        if (i != this.y0) {
            this.y0 = i;
            if (getNavigationIcon() != null) {
                requestLayout();
            }
        }
    }

    public void setContentInsetsAbsolute(int i, int i2) {
        h();
        this.x0.e(i, i2);
    }

    public void setContentInsetsRelative(int i, int i2) {
        h();
        this.x0.g(i, i2);
    }

    public void setLogo(int i) {
        setLogo(mf.d(getContext(), i));
    }

    public void setLogoDescription(int i) {
        setLogoDescription(getContext().getText(i));
    }

    public void setMenu(androidx.appcompat.view.menu.e eVar, ActionMenuPresenter actionMenuPresenter) {
        if (eVar == null && this.a == null) {
            return;
        }
        k();
        androidx.appcompat.view.menu.e q = this.a.q();
        if (q == eVar) {
            return;
        }
        if (q != null) {
            q.Q(this.N0);
            q.Q(this.O0);
        }
        if (this.O0 == null) {
            this.O0 = new d();
        }
        actionMenuPresenter.J(true);
        if (eVar != null) {
            eVar.c(actionMenuPresenter, this.n0);
            eVar.c(this.O0, this.n0);
        } else {
            actionMenuPresenter.i(this.n0, null);
            this.O0.i(this.n0, null);
            actionMenuPresenter.d(true);
            this.O0.d(true);
        }
        this.a.setPopupTheme(this.o0);
        this.a.setPresenter(actionMenuPresenter);
        this.N0 = actionMenuPresenter;
    }

    public void setMenuCallbacks(i.a aVar, e.a aVar2) {
        this.P0 = aVar;
        this.Q0 = aVar2;
        ActionMenuView actionMenuView = this.a;
        if (actionMenuView != null) {
            actionMenuView.setMenuCallbacks(aVar, aVar2);
        }
    }

    public void setNavigationContentDescription(int i) {
        setNavigationContentDescription(i != 0 ? getContext().getText(i) : null);
    }

    public void setNavigationIcon(int i) {
        setNavigationIcon(mf.d(getContext(), i));
    }

    public void setNavigationOnClickListener(View.OnClickListener onClickListener) {
        l();
        this.h0.setOnClickListener(onClickListener);
    }

    public void setOnMenuItemClickListener(e eVar) {
        this.K0 = eVar;
    }

    public void setOverflowIcon(Drawable drawable) {
        j();
        this.a.setOverflowIcon(drawable);
    }

    public void setPopupTheme(int i) {
        if (this.o0 != i) {
            this.o0 = i;
            if (i == 0) {
                this.n0 = getContext();
            } else {
                this.n0 = new ContextThemeWrapper(getContext(), i);
            }
        }
    }

    public void setSubtitle(int i) {
        setSubtitle(getContext().getText(i));
    }

    public void setSubtitleTextAppearance(Context context, int i) {
        this.q0 = i;
        TextView textView = this.g0;
        if (textView != null) {
            textView.setTextAppearance(context, i);
        }
    }

    public void setSubtitleTextColor(int i) {
        setSubtitleTextColor(ColorStateList.valueOf(i));
    }

    public void setTitle(int i) {
        setTitle(getContext().getText(i));
    }

    public void setTitleMargin(int i, int i2, int i3, int i4) {
        this.t0 = i;
        this.v0 = i2;
        this.u0 = i3;
        this.w0 = i4;
        requestLayout();
    }

    public void setTitleMarginBottom(int i) {
        this.w0 = i;
        requestLayout();
    }

    public void setTitleMarginEnd(int i) {
        this.u0 = i;
        requestLayout();
    }

    public void setTitleMarginStart(int i) {
        this.t0 = i;
        requestLayout();
    }

    public void setTitleMarginTop(int i) {
        this.v0 = i;
        requestLayout();
    }

    public void setTitleTextAppearance(Context context, int i) {
        this.p0 = i;
        TextView textView = this.f0;
        if (textView != null) {
            textView.setTextAppearance(context, i);
        }
    }

    public void setTitleTextColor(int i) {
        setTitleTextColor(ColorStateList.valueOf(i));
    }

    public final int t(View view) {
        ViewGroup.MarginLayoutParams marginLayoutParams = (ViewGroup.MarginLayoutParams) view.getLayoutParams();
        return marginLayoutParams.topMargin + marginLayoutParams.bottomMargin;
    }

    public final int u(List<View> list, int[] iArr) {
        int i = iArr[0];
        int i2 = iArr[1];
        int size = list.size();
        int i3 = 0;
        int i4 = 0;
        while (i3 < size) {
            View view = list.get(i3);
            LayoutParams layoutParams = (LayoutParams) view.getLayoutParams();
            int i5 = ((ViewGroup.MarginLayoutParams) layoutParams).leftMargin - i;
            int i6 = ((ViewGroup.MarginLayoutParams) layoutParams).rightMargin - i2;
            int max = Math.max(0, i5);
            int max2 = Math.max(0, i6);
            int max3 = Math.max(0, -i5);
            int max4 = Math.max(0, -i6);
            i4 += max + view.getMeasuredWidth() + max2;
            i3++;
            i2 = max4;
            i = max3;
        }
        return i4;
    }

    public boolean v() {
        d dVar = this.O0;
        return (dVar == null || dVar.f0 == null) ? false : true;
    }

    public boolean w() {
        ActionMenuView actionMenuView = this.a;
        return actionMenuView != null && actionMenuView.k();
    }

    public void x(int i) {
        getMenuInflater().inflate(i, getMenu());
    }

    public final boolean y(View view) {
        return view.getParent() == this || this.I0.contains(view);
    }

    public boolean z() {
        ActionMenuView actionMenuView = this.a;
        return actionMenuView != null && actionMenuView.l();
    }

    /* loaded from: classes.dex */
    public static class LayoutParams extends ActionBar.LayoutParams {
        public int b;

        public LayoutParams(Context context, AttributeSet attributeSet) {
            super(context, attributeSet);
            this.b = 0;
        }

        public void a(ViewGroup.MarginLayoutParams marginLayoutParams) {
            ((ViewGroup.MarginLayoutParams) this).leftMargin = marginLayoutParams.leftMargin;
            ((ViewGroup.MarginLayoutParams) this).topMargin = marginLayoutParams.topMargin;
            ((ViewGroup.MarginLayoutParams) this).rightMargin = marginLayoutParams.rightMargin;
            ((ViewGroup.MarginLayoutParams) this).bottomMargin = marginLayoutParams.bottomMargin;
        }

        public LayoutParams(int i, int i2) {
            super(i, i2);
            this.b = 0;
            this.a = 8388627;
        }

        public LayoutParams(LayoutParams layoutParams) {
            super((ActionBar.LayoutParams) layoutParams);
            this.b = 0;
            this.b = layoutParams.b;
        }

        public LayoutParams(ActionBar.LayoutParams layoutParams) {
            super(layoutParams);
            this.b = 0;
        }

        public LayoutParams(ViewGroup.MarginLayoutParams marginLayoutParams) {
            super(marginLayoutParams);
            this.b = 0;
            a(marginLayoutParams);
        }

        public LayoutParams(ViewGroup.LayoutParams layoutParams) {
            super(layoutParams);
            this.b = 0;
        }
    }

    public Toolbar(Context context, AttributeSet attributeSet) {
        this(context, attributeSet, jy2.toolbarStyle);
    }

    public void setCollapseContentDescription(CharSequence charSequence) {
        if (!TextUtils.isEmpty(charSequence)) {
            g();
        }
        ImageButton imageButton = this.l0;
        if (imageButton != null) {
            imageButton.setContentDescription(charSequence);
        }
    }

    public void setCollapseIcon(Drawable drawable) {
        if (drawable != null) {
            g();
            this.l0.setImageDrawable(drawable);
            return;
        }
        ImageButton imageButton = this.l0;
        if (imageButton != null) {
            imageButton.setImageDrawable(this.j0);
        }
    }

    public void setLogo(Drawable drawable) {
        if (drawable != null) {
            i();
            if (!y(this.i0)) {
                c(this.i0, true);
            }
        } else {
            ImageView imageView = this.i0;
            if (imageView != null && y(imageView)) {
                removeView(this.i0);
                this.I0.remove(this.i0);
            }
        }
        ImageView imageView2 = this.i0;
        if (imageView2 != null) {
            imageView2.setImageDrawable(drawable);
        }
    }

    public void setLogoDescription(CharSequence charSequence) {
        if (!TextUtils.isEmpty(charSequence)) {
            i();
        }
        ImageView imageView = this.i0;
        if (imageView != null) {
            imageView.setContentDescription(charSequence);
        }
    }

    public void setNavigationContentDescription(CharSequence charSequence) {
        if (!TextUtils.isEmpty(charSequence)) {
            l();
        }
        ImageButton imageButton = this.h0;
        if (imageButton != null) {
            imageButton.setContentDescription(charSequence);
        }
    }

    public void setNavigationIcon(Drawable drawable) {
        if (drawable != null) {
            l();
            if (!y(this.h0)) {
                c(this.h0, true);
            }
        } else {
            ImageButton imageButton = this.h0;
            if (imageButton != null && y(imageButton)) {
                removeView(this.h0);
                this.I0.remove(this.h0);
            }
        }
        ImageButton imageButton2 = this.h0;
        if (imageButton2 != null) {
            imageButton2.setImageDrawable(drawable);
        }
    }

    public void setSubtitle(CharSequence charSequence) {
        if (!TextUtils.isEmpty(charSequence)) {
            if (this.g0 == null) {
                Context context = getContext();
                AppCompatTextView appCompatTextView = new AppCompatTextView(context);
                this.g0 = appCompatTextView;
                appCompatTextView.setSingleLine();
                this.g0.setEllipsize(TextUtils.TruncateAt.END);
                int i = this.q0;
                if (i != 0) {
                    this.g0.setTextAppearance(context, i);
                }
                ColorStateList colorStateList = this.E0;
                if (colorStateList != null) {
                    this.g0.setTextColor(colorStateList);
                }
            }
            if (!y(this.g0)) {
                c(this.g0, true);
            }
        } else {
            TextView textView = this.g0;
            if (textView != null && y(textView)) {
                removeView(this.g0);
                this.I0.remove(this.g0);
            }
        }
        TextView textView2 = this.g0;
        if (textView2 != null) {
            textView2.setText(charSequence);
        }
        this.C0 = charSequence;
    }

    public void setSubtitleTextColor(ColorStateList colorStateList) {
        this.E0 = colorStateList;
        TextView textView = this.g0;
        if (textView != null) {
            textView.setTextColor(colorStateList);
        }
    }

    public void setTitle(CharSequence charSequence) {
        if (!TextUtils.isEmpty(charSequence)) {
            if (this.f0 == null) {
                Context context = getContext();
                AppCompatTextView appCompatTextView = new AppCompatTextView(context);
                this.f0 = appCompatTextView;
                appCompatTextView.setSingleLine();
                this.f0.setEllipsize(TextUtils.TruncateAt.END);
                int i = this.p0;
                if (i != 0) {
                    this.f0.setTextAppearance(context, i);
                }
                ColorStateList colorStateList = this.D0;
                if (colorStateList != null) {
                    this.f0.setTextColor(colorStateList);
                }
            }
            if (!y(this.f0)) {
                c(this.f0, true);
            }
        } else {
            TextView textView = this.f0;
            if (textView != null && y(textView)) {
                removeView(this.f0);
                this.I0.remove(this.f0);
            }
        }
        TextView textView2 = this.f0;
        if (textView2 != null) {
            textView2.setText(charSequence);
        }
        this.B0 = charSequence;
    }

    public void setTitleTextColor(ColorStateList colorStateList) {
        this.D0 = colorStateList;
        TextView textView = this.f0;
        if (textView != null) {
            textView.setTextColor(colorStateList);
        }
    }

    /* loaded from: classes.dex */
    public static class SavedState extends AbsSavedState {
        public static final Parcelable.Creator<SavedState> CREATOR = new a();
        public int g0;
        public boolean h0;

        /* loaded from: classes.dex */
        public class a implements Parcelable.ClassLoaderCreator<SavedState> {
            @Override // android.os.Parcelable.Creator
            /* renamed from: a */
            public SavedState createFromParcel(Parcel parcel) {
                return new SavedState(parcel, null);
            }

            @Override // android.os.Parcelable.ClassLoaderCreator
            /* renamed from: b */
            public SavedState createFromParcel(Parcel parcel, ClassLoader classLoader) {
                return new SavedState(parcel, classLoader);
            }

            @Override // android.os.Parcelable.Creator
            /* renamed from: c */
            public SavedState[] newArray(int i) {
                return new SavedState[i];
            }
        }

        public SavedState(Parcel parcel, ClassLoader classLoader) {
            super(parcel, classLoader);
            this.g0 = parcel.readInt();
            this.h0 = parcel.readInt() != 0;
        }

        @Override // androidx.customview.view.AbsSavedState, android.os.Parcelable
        public void writeToParcel(Parcel parcel, int i) {
            super.writeToParcel(parcel, i);
            parcel.writeInt(this.g0);
            parcel.writeInt(this.h0 ? 1 : 0);
        }

        public SavedState(Parcelable parcelable) {
            super(parcelable);
        }
    }

    public Toolbar(Context context, AttributeSet attributeSet, int i) {
        super(context, attributeSet, i);
        this.A0 = 8388627;
        this.H0 = new ArrayList<>();
        this.I0 = new ArrayList<>();
        this.J0 = new int[2];
        this.L0 = new a();
        this.S0 = new b();
        Context context2 = getContext();
        int[] iArr = d33.Toolbar;
        l64 v = l64.v(context2, attributeSet, iArr, i, 0);
        ei4.r0(this, context, iArr, attributeSet, v.r(), i, 0);
        this.p0 = v.n(d33.Toolbar_titleTextAppearance, 0);
        this.q0 = v.n(d33.Toolbar_subtitleTextAppearance, 0);
        this.A0 = v.l(d33.Toolbar_android_gravity, this.A0);
        this.r0 = v.l(d33.Toolbar_buttonGravity, 48);
        int e2 = v.e(d33.Toolbar_titleMargin, 0);
        int i2 = d33.Toolbar_titleMargins;
        e2 = v.s(i2) ? v.e(i2, e2) : e2;
        this.w0 = e2;
        this.v0 = e2;
        this.u0 = e2;
        this.t0 = e2;
        int e3 = v.e(d33.Toolbar_titleMarginStart, -1);
        if (e3 >= 0) {
            this.t0 = e3;
        }
        int e4 = v.e(d33.Toolbar_titleMarginEnd, -1);
        if (e4 >= 0) {
            this.u0 = e4;
        }
        int e5 = v.e(d33.Toolbar_titleMarginTop, -1);
        if (e5 >= 0) {
            this.v0 = e5;
        }
        int e6 = v.e(d33.Toolbar_titleMarginBottom, -1);
        if (e6 >= 0) {
            this.w0 = e6;
        }
        this.s0 = v.f(d33.Toolbar_maxButtonHeight, -1);
        int e7 = v.e(d33.Toolbar_contentInsetStart, Integer.MIN_VALUE);
        int e8 = v.e(d33.Toolbar_contentInsetEnd, Integer.MIN_VALUE);
        int f = v.f(d33.Toolbar_contentInsetLeft, 0);
        int f2 = v.f(d33.Toolbar_contentInsetRight, 0);
        h();
        this.x0.e(f, f2);
        if (e7 != Integer.MIN_VALUE || e8 != Integer.MIN_VALUE) {
            this.x0.g(e7, e8);
        }
        this.y0 = v.e(d33.Toolbar_contentInsetStartWithNavigation, Integer.MIN_VALUE);
        this.z0 = v.e(d33.Toolbar_contentInsetEndWithActions, Integer.MIN_VALUE);
        this.j0 = v.g(d33.Toolbar_collapseIcon);
        this.k0 = v.p(d33.Toolbar_collapseContentDescription);
        CharSequence p = v.p(d33.Toolbar_title);
        if (!TextUtils.isEmpty(p)) {
            setTitle(p);
        }
        CharSequence p2 = v.p(d33.Toolbar_subtitle);
        if (!TextUtils.isEmpty(p2)) {
            setSubtitle(p2);
        }
        this.n0 = getContext();
        setPopupTheme(v.n(d33.Toolbar_popupTheme, 0));
        Drawable g = v.g(d33.Toolbar_navigationIcon);
        if (g != null) {
            setNavigationIcon(g);
        }
        CharSequence p3 = v.p(d33.Toolbar_navigationContentDescription);
        if (!TextUtils.isEmpty(p3)) {
            setNavigationContentDescription(p3);
        }
        Drawable g2 = v.g(d33.Toolbar_logo);
        if (g2 != null) {
            setLogo(g2);
        }
        CharSequence p4 = v.p(d33.Toolbar_logoDescription);
        if (!TextUtils.isEmpty(p4)) {
            setLogoDescription(p4);
        }
        int i3 = d33.Toolbar_titleTextColor;
        if (v.s(i3)) {
            setTitleTextColor(v.c(i3));
        }
        int i4 = d33.Toolbar_subtitleTextColor;
        if (v.s(i4)) {
            setSubtitleTextColor(v.c(i4));
        }
        int i5 = d33.Toolbar_menu;
        if (v.s(i5)) {
            x(v.n(i5, 0));
        }
        v.w();
    }
}
