package androidx.appcompat.widget;

import android.content.Context;
import android.content.res.TypedArray;
import android.database.DataSetObserver;
import android.graphics.Rect;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.os.Handler;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewParent;
import android.widget.AbsListView;
import android.widget.AdapterView;
import android.widget.LinearLayout;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.PopupWindow;
import androidx.media3.common.PlaybackException;
import java.lang.reflect.Method;

/* loaded from: classes.dex */
public class ListPopupWindow implements po3 {
    public static Method J0;
    public static Method K0;
    public static Method L0;
    public final g A0;
    public final f B0;
    public final e C0;
    public final c D0;
    public final Handler E0;
    public final Rect F0;
    public Rect G0;
    public boolean H0;
    public PopupWindow I0;
    public Context a;
    public ListAdapter f0;
    public bs0 g0;
    public int h0;
    public int i0;
    public int j0;
    public int k0;
    public int l0;
    public boolean m0;
    public boolean n0;
    public boolean o0;
    public int p0;
    public boolean q0;
    public boolean r0;
    public int s0;
    public View t0;
    public int u0;
    public DataSetObserver v0;
    public View w0;
    public Drawable x0;
    public AdapterView.OnItemClickListener y0;
    public AdapterView.OnItemSelectedListener z0;

    /* loaded from: classes.dex */
    public class a implements Runnable {
        public a() {
        }

        @Override // java.lang.Runnable
        public void run() {
            View t = ListPopupWindow.this.t();
            if (t == null || t.getWindowToken() == null) {
                return;
            }
            ListPopupWindow.this.b();
        }
    }

    /* loaded from: classes.dex */
    public class b implements AdapterView.OnItemSelectedListener {
        public b() {
        }

        @Override // android.widget.AdapterView.OnItemSelectedListener
        public void onItemSelected(AdapterView<?> adapterView, View view, int i, long j) {
            bs0 bs0Var;
            if (i == -1 || (bs0Var = ListPopupWindow.this.g0) == null) {
                return;
            }
            bs0Var.setListSelectionHidden(false);
        }

        @Override // android.widget.AdapterView.OnItemSelectedListener
        public void onNothingSelected(AdapterView<?> adapterView) {
        }
    }

    /* loaded from: classes.dex */
    public class c implements Runnable {
        public c() {
        }

        @Override // java.lang.Runnable
        public void run() {
            ListPopupWindow.this.r();
        }
    }

    /* loaded from: classes.dex */
    public class d extends DataSetObserver {
        public d() {
        }

        @Override // android.database.DataSetObserver
        public void onChanged() {
            if (ListPopupWindow.this.a()) {
                ListPopupWindow.this.b();
            }
        }

        @Override // android.database.DataSetObserver
        public void onInvalidated() {
            ListPopupWindow.this.dismiss();
        }
    }

    /* loaded from: classes.dex */
    public class e implements AbsListView.OnScrollListener {
        public e() {
        }

        @Override // android.widget.AbsListView.OnScrollListener
        public void onScroll(AbsListView absListView, int i, int i2, int i3) {
        }

        @Override // android.widget.AbsListView.OnScrollListener
        public void onScrollStateChanged(AbsListView absListView, int i) {
            if (i != 1 || ListPopupWindow.this.A() || ListPopupWindow.this.I0.getContentView() == null) {
                return;
            }
            ListPopupWindow listPopupWindow = ListPopupWindow.this;
            listPopupWindow.E0.removeCallbacks(listPopupWindow.A0);
            ListPopupWindow.this.A0.run();
        }
    }

    /* loaded from: classes.dex */
    public class f implements View.OnTouchListener {
        public f() {
        }

        @Override // android.view.View.OnTouchListener
        public boolean onTouch(View view, MotionEvent motionEvent) {
            PopupWindow popupWindow;
            int action = motionEvent.getAction();
            int x = (int) motionEvent.getX();
            int y = (int) motionEvent.getY();
            if (action == 0 && (popupWindow = ListPopupWindow.this.I0) != null && popupWindow.isShowing() && x >= 0 && x < ListPopupWindow.this.I0.getWidth() && y >= 0 && y < ListPopupWindow.this.I0.getHeight()) {
                ListPopupWindow listPopupWindow = ListPopupWindow.this;
                listPopupWindow.E0.postDelayed(listPopupWindow.A0, 250L);
                return false;
            } else if (action == 1) {
                ListPopupWindow listPopupWindow2 = ListPopupWindow.this;
                listPopupWindow2.E0.removeCallbacks(listPopupWindow2.A0);
                return false;
            } else {
                return false;
            }
        }
    }

    /* loaded from: classes.dex */
    public class g implements Runnable {
        public g() {
        }

        @Override // java.lang.Runnable
        public void run() {
            bs0 bs0Var = ListPopupWindow.this.g0;
            if (bs0Var == null || !ei4.V(bs0Var) || ListPopupWindow.this.g0.getCount() <= ListPopupWindow.this.g0.getChildCount()) {
                return;
            }
            int childCount = ListPopupWindow.this.g0.getChildCount();
            ListPopupWindow listPopupWindow = ListPopupWindow.this;
            if (childCount <= listPopupWindow.s0) {
                listPopupWindow.I0.setInputMethodMode(2);
                ListPopupWindow.this.b();
            }
        }
    }

    static {
        if (Build.VERSION.SDK_INT <= 28) {
            try {
                J0 = PopupWindow.class.getDeclaredMethod("setClipToScreenEnabled", Boolean.TYPE);
            } catch (NoSuchMethodException unused) {
            }
            try {
                L0 = PopupWindow.class.getDeclaredMethod("setEpicenterBounds", Rect.class);
            } catch (NoSuchMethodException unused2) {
            }
        }
        if (Build.VERSION.SDK_INT <= 23) {
            try {
                K0 = PopupWindow.class.getDeclaredMethod("getMaxAvailableHeight", View.class, Integer.TYPE, Boolean.TYPE);
            } catch (NoSuchMethodException unused3) {
            }
        }
    }

    public ListPopupWindow(Context context) {
        this(context, null, jy2.listPopupWindowStyle);
    }

    public boolean A() {
        return this.I0.getInputMethodMode() == 2;
    }

    public boolean B() {
        return this.H0;
    }

    public final void C() {
        View view = this.t0;
        if (view != null) {
            ViewParent parent = view.getParent();
            if (parent instanceof ViewGroup) {
                ((ViewGroup) parent).removeView(this.t0);
            }
        }
    }

    public void D(View view) {
        this.w0 = view;
    }

    public void E(int i) {
        this.I0.setAnimationStyle(i);
    }

    public void F(int i) {
        Drawable background = this.I0.getBackground();
        if (background != null) {
            background.getPadding(this.F0);
            Rect rect = this.F0;
            this.i0 = rect.left + rect.right + i;
            return;
        }
        Q(i);
    }

    public void G(int i) {
        this.p0 = i;
    }

    public void H(Rect rect) {
        this.G0 = rect != null ? new Rect(rect) : null;
    }

    public void I(int i) {
        this.I0.setInputMethodMode(i);
    }

    public void J(boolean z) {
        this.H0 = z;
        this.I0.setFocusable(z);
    }

    public void K(PopupWindow.OnDismissListener onDismissListener) {
        this.I0.setOnDismissListener(onDismissListener);
    }

    public void L(AdapterView.OnItemClickListener onItemClickListener) {
        this.y0 = onItemClickListener;
    }

    public void M(boolean z) {
        this.o0 = true;
        this.n0 = z;
    }

    public final void N(boolean z) {
        if (Build.VERSION.SDK_INT <= 28) {
            Method method = J0;
            if (method != null) {
                try {
                    method.invoke(this.I0, Boolean.valueOf(z));
                    return;
                } catch (Exception unused) {
                    return;
                }
            }
            return;
        }
        this.I0.setIsClippedToScreen(z);
    }

    public void O(int i) {
        this.u0 = i;
    }

    public void P(int i) {
        bs0 bs0Var = this.g0;
        if (!a() || bs0Var == null) {
            return;
        }
        bs0Var.setListSelectionHidden(false);
        bs0Var.setSelection(i);
        if (bs0Var.getChoiceMode() != 0) {
            bs0Var.setItemChecked(i, true);
        }
    }

    public void Q(int i) {
        this.i0 = i;
    }

    @Override // defpackage.po3
    public boolean a() {
        return this.I0.isShowing();
    }

    @Override // defpackage.po3
    public void b() {
        int q = q();
        boolean A = A();
        nt2.b(this.I0, this.l0);
        boolean z = true;
        if (this.I0.isShowing()) {
            if (ei4.V(t())) {
                int i = this.i0;
                if (i == -1) {
                    i = -1;
                } else if (i == -2) {
                    i = t().getWidth();
                }
                int i2 = this.h0;
                if (i2 == -1) {
                    if (!A) {
                        q = -1;
                    }
                    if (A) {
                        this.I0.setWidth(this.i0 == -1 ? -1 : 0);
                        this.I0.setHeight(0);
                    } else {
                        this.I0.setWidth(this.i0 == -1 ? -1 : 0);
                        this.I0.setHeight(-1);
                    }
                } else if (i2 != -2) {
                    q = i2;
                }
                PopupWindow popupWindow = this.I0;
                if (this.r0 || this.q0) {
                    z = false;
                }
                popupWindow.setOutsideTouchable(z);
                this.I0.update(t(), this.j0, this.k0, i < 0 ? -1 : i, q < 0 ? -1 : q);
                return;
            }
            return;
        }
        int i3 = this.i0;
        if (i3 == -1) {
            i3 = -1;
        } else if (i3 == -2) {
            i3 = t().getWidth();
        }
        int i4 = this.h0;
        if (i4 == -1) {
            q = -1;
        } else if (i4 != -2) {
            q = i4;
        }
        this.I0.setWidth(i3);
        this.I0.setHeight(q);
        N(true);
        this.I0.setOutsideTouchable((this.r0 || this.q0) ? false : true);
        this.I0.setTouchInterceptor(this.B0);
        if (this.o0) {
            nt2.a(this.I0, this.n0);
        }
        if (Build.VERSION.SDK_INT <= 28) {
            Method method = L0;
            if (method != null) {
                try {
                    method.invoke(this.I0, this.G0);
                } catch (Exception unused) {
                }
            }
        } else {
            this.I0.setEpicenterBounds(this.G0);
        }
        nt2.c(this.I0, t(), this.j0, this.k0, this.p0);
        this.g0.setSelection(-1);
        if (!this.H0 || this.g0.isInTouchMode()) {
            r();
        }
        if (this.H0) {
            return;
        }
        this.E0.post(this.D0);
    }

    public void c(Drawable drawable) {
        this.I0.setBackgroundDrawable(drawable);
    }

    public int d() {
        return this.j0;
    }

    @Override // defpackage.po3
    public void dismiss() {
        this.I0.dismiss();
        C();
        this.I0.setContentView(null);
        this.g0 = null;
        this.E0.removeCallbacks(this.A0);
    }

    public void f(int i) {
        this.j0 = i;
    }

    public Drawable i() {
        return this.I0.getBackground();
    }

    @Override // defpackage.po3
    public ListView k() {
        return this.g0;
    }

    public void l(int i) {
        this.k0 = i;
        this.m0 = true;
    }

    public int o() {
        if (this.m0) {
            return this.k0;
        }
        return 0;
    }

    public void p(ListAdapter listAdapter) {
        DataSetObserver dataSetObserver = this.v0;
        if (dataSetObserver == null) {
            this.v0 = new d();
        } else {
            ListAdapter listAdapter2 = this.f0;
            if (listAdapter2 != null) {
                listAdapter2.unregisterDataSetObserver(dataSetObserver);
            }
        }
        this.f0 = listAdapter;
        if (listAdapter != null) {
            listAdapter.registerDataSetObserver(this.v0);
        }
        bs0 bs0Var = this.g0;
        if (bs0Var != null) {
            bs0Var.setAdapter(this.f0);
        }
    }

    /* JADX WARN: Multi-variable type inference failed */
    public final int q() {
        int i;
        int i2;
        int makeMeasureSpec;
        int i3;
        if (this.g0 == null) {
            Context context = this.a;
            new a();
            bs0 s = s(context, !this.H0);
            this.g0 = s;
            Drawable drawable = this.x0;
            if (drawable != null) {
                s.setSelector(drawable);
            }
            this.g0.setAdapter(this.f0);
            this.g0.setOnItemClickListener(this.y0);
            this.g0.setFocusable(true);
            this.g0.setFocusableInTouchMode(true);
            this.g0.setOnItemSelectedListener(new b());
            this.g0.setOnScrollListener(this.C0);
            AdapterView.OnItemSelectedListener onItemSelectedListener = this.z0;
            if (onItemSelectedListener != null) {
                this.g0.setOnItemSelectedListener(onItemSelectedListener);
            }
            bs0 bs0Var = this.g0;
            View view = this.t0;
            if (view != null) {
                LinearLayout linearLayout = new LinearLayout(context);
                linearLayout.setOrientation(1);
                LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(-1, 0, 1.0f);
                int i4 = this.u0;
                if (i4 == 0) {
                    linearLayout.addView(view);
                    linearLayout.addView(bs0Var, layoutParams);
                } else if (i4 != 1) {
                    StringBuilder sb = new StringBuilder();
                    sb.append("Invalid hint position ");
                    sb.append(this.u0);
                } else {
                    linearLayout.addView(bs0Var, layoutParams);
                    linearLayout.addView(view);
                }
                int i5 = this.i0;
                if (i5 >= 0) {
                    i3 = Integer.MIN_VALUE;
                } else {
                    i5 = 0;
                    i3 = 0;
                }
                view.measure(View.MeasureSpec.makeMeasureSpec(i5, i3), 0);
                LinearLayout.LayoutParams layoutParams2 = (LinearLayout.LayoutParams) view.getLayoutParams();
                i = view.getMeasuredHeight() + layoutParams2.topMargin + layoutParams2.bottomMargin;
                bs0Var = linearLayout;
            } else {
                i = 0;
            }
            this.I0.setContentView(bs0Var);
        } else {
            ViewGroup viewGroup = (ViewGroup) this.I0.getContentView();
            View view2 = this.t0;
            if (view2 != null) {
                LinearLayout.LayoutParams layoutParams3 = (LinearLayout.LayoutParams) view2.getLayoutParams();
                i = view2.getMeasuredHeight() + layoutParams3.topMargin + layoutParams3.bottomMargin;
            } else {
                i = 0;
            }
        }
        Drawable background = this.I0.getBackground();
        if (background != null) {
            background.getPadding(this.F0);
            Rect rect = this.F0;
            int i6 = rect.top;
            i2 = rect.bottom + i6;
            if (!this.m0) {
                this.k0 = -i6;
            }
        } else {
            this.F0.setEmpty();
            i2 = 0;
        }
        int u = u(t(), this.k0, this.I0.getInputMethodMode() == 2);
        if (this.q0 || this.h0 == -1) {
            return u + i2;
        }
        int i7 = this.i0;
        if (i7 == -2) {
            int i8 = this.a.getResources().getDisplayMetrics().widthPixels;
            Rect rect2 = this.F0;
            makeMeasureSpec = View.MeasureSpec.makeMeasureSpec(i8 - (rect2.left + rect2.right), Integer.MIN_VALUE);
        } else if (i7 != -1) {
            makeMeasureSpec = View.MeasureSpec.makeMeasureSpec(i7, 1073741824);
        } else {
            int i9 = this.a.getResources().getDisplayMetrics().widthPixels;
            Rect rect3 = this.F0;
            makeMeasureSpec = View.MeasureSpec.makeMeasureSpec(i9 - (rect3.left + rect3.right), 1073741824);
        }
        int d2 = this.g0.d(makeMeasureSpec, 0, -1, u - i, -1);
        if (d2 > 0) {
            i += i2 + this.g0.getPaddingTop() + this.g0.getPaddingBottom();
        }
        return d2 + i;
    }

    public void r() {
        bs0 bs0Var = this.g0;
        if (bs0Var != null) {
            bs0Var.setListSelectionHidden(true);
            bs0Var.requestLayout();
        }
    }

    public bs0 s(Context context, boolean z) {
        return new bs0(context, z);
    }

    public View t() {
        return this.w0;
    }

    public final int u(View view, int i, boolean z) {
        if (Build.VERSION.SDK_INT <= 23) {
            Method method = K0;
            if (method != null) {
                try {
                    return ((Integer) method.invoke(this.I0, view, Integer.valueOf(i), Boolean.valueOf(z))).intValue();
                } catch (Exception unused) {
                }
            }
            return this.I0.getMaxAvailableHeight(view, i);
        }
        return this.I0.getMaxAvailableHeight(view, i, z);
    }

    public Object v() {
        if (a()) {
            return this.g0.getSelectedItem();
        }
        return null;
    }

    public long w() {
        if (a()) {
            return this.g0.getSelectedItemId();
        }
        return Long.MIN_VALUE;
    }

    public int x() {
        if (a()) {
            return this.g0.getSelectedItemPosition();
        }
        return -1;
    }

    public View y() {
        if (a()) {
            return this.g0.getSelectedView();
        }
        return null;
    }

    public int z() {
        return this.i0;
    }

    public ListPopupWindow(Context context, AttributeSet attributeSet) {
        this(context, attributeSet, jy2.listPopupWindowStyle);
    }

    public ListPopupWindow(Context context, AttributeSet attributeSet, int i) {
        this(context, attributeSet, i, 0);
    }

    public ListPopupWindow(Context context, AttributeSet attributeSet, int i, int i2) {
        this.h0 = -2;
        this.i0 = -2;
        this.l0 = PlaybackException.ERROR_CODE_BEHIND_LIVE_WINDOW;
        this.p0 = 0;
        this.q0 = false;
        this.r0 = false;
        this.s0 = Integer.MAX_VALUE;
        this.u0 = 0;
        this.A0 = new g();
        this.B0 = new f();
        this.C0 = new e();
        this.D0 = new c();
        this.F0 = new Rect();
        this.a = context;
        this.E0 = new Handler(context.getMainLooper());
        TypedArray obtainStyledAttributes = context.obtainStyledAttributes(attributeSet, d33.ListPopupWindow, i, i2);
        this.j0 = obtainStyledAttributes.getDimensionPixelOffset(d33.ListPopupWindow_android_dropDownHorizontalOffset, 0);
        int dimensionPixelOffset = obtainStyledAttributes.getDimensionPixelOffset(d33.ListPopupWindow_android_dropDownVerticalOffset, 0);
        this.k0 = dimensionPixelOffset;
        if (dimensionPixelOffset != 0) {
            this.m0 = true;
        }
        obtainStyledAttributes.recycle();
        jf jfVar = new jf(context, attributeSet, i, i2);
        this.I0 = jfVar;
        jfVar.setInputMethodMode(1);
    }
}
