package androidx.appcompat.view.menu;

import android.content.Context;
import android.os.Parcelable;

/* compiled from: MenuPresenter.java */
/* loaded from: classes.dex */
public interface i {

    /* compiled from: MenuPresenter.java */
    /* loaded from: classes.dex */
    public interface a {
        void c(e eVar, boolean z);

        boolean d(e eVar);
    }

    void c(e eVar, boolean z);

    void d(boolean z);

    boolean e();

    boolean f(e eVar, g gVar);

    boolean g(e eVar, g gVar);

    int getId();

    void h(a aVar);

    void i(Context context, e eVar);

    void j(Parcelable parcelable);

    boolean l(l lVar);

    Parcelable m();
}
