package androidx.appcompat.view.menu;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Rect;
import android.graphics.drawable.Drawable;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.TextView;
import androidx.appcompat.view.menu.j;

/* loaded from: classes.dex */
public class ListMenuItemView extends LinearLayout implements j.a, AbsListView.SelectionBoundsAdjuster {
    public g a;
    public ImageView f0;
    public RadioButton g0;
    public TextView h0;
    public CheckBox i0;
    public TextView j0;
    public ImageView k0;
    public ImageView l0;
    public LinearLayout m0;
    public Drawable n0;
    public int o0;
    public Context p0;
    public boolean q0;
    public Drawable r0;
    public boolean s0;
    public LayoutInflater t0;
    public boolean u0;

    public ListMenuItemView(Context context, AttributeSet attributeSet) {
        this(context, attributeSet, jy2.listMenuViewStyle);
    }

    private LayoutInflater getInflater() {
        if (this.t0 == null) {
            this.t0 = LayoutInflater.from(getContext());
        }
        return this.t0;
    }

    private void setSubMenuArrowVisible(boolean z) {
        ImageView imageView = this.k0;
        if (imageView != null) {
            imageView.setVisibility(z ? 0 : 8);
        }
    }

    public final void a(View view) {
        b(view, -1);
    }

    @Override // android.widget.AbsListView.SelectionBoundsAdjuster
    public void adjustListItemSelectionBounds(Rect rect) {
        ImageView imageView = this.l0;
        if (imageView == null || imageView.getVisibility() != 0) {
            return;
        }
        LinearLayout.LayoutParams layoutParams = (LinearLayout.LayoutParams) this.l0.getLayoutParams();
        rect.top += this.l0.getHeight() + layoutParams.topMargin + layoutParams.bottomMargin;
    }

    public final void b(View view, int i) {
        LinearLayout linearLayout = this.m0;
        if (linearLayout != null) {
            linearLayout.addView(view, i);
        } else {
            addView(view, i);
        }
    }

    @Override // androidx.appcompat.view.menu.j.a
    public boolean c() {
        return false;
    }

    @Override // androidx.appcompat.view.menu.j.a
    public void d(g gVar, int i) {
        this.a = gVar;
        setVisibility(gVar.isVisible() ? 0 : 8);
        setTitle(gVar.i(this));
        setCheckable(gVar.isCheckable());
        setShortcut(gVar.A(), gVar.g());
        setIcon(gVar.getIcon());
        setEnabled(gVar.isEnabled());
        setSubMenuArrowVisible(gVar.hasSubMenu());
        setContentDescription(gVar.getContentDescription());
    }

    public final void e() {
        CheckBox checkBox = (CheckBox) getInflater().inflate(d13.abc_list_menu_item_checkbox, (ViewGroup) this, false);
        this.i0 = checkBox;
        a(checkBox);
    }

    public final void f() {
        ImageView imageView = (ImageView) getInflater().inflate(d13.abc_list_menu_item_icon, (ViewGroup) this, false);
        this.f0 = imageView;
        b(imageView, 0);
    }

    public final void g() {
        RadioButton radioButton = (RadioButton) getInflater().inflate(d13.abc_list_menu_item_radio, (ViewGroup) this, false);
        this.g0 = radioButton;
        a(radioButton);
    }

    @Override // androidx.appcompat.view.menu.j.a
    public g getItemData() {
        return this.a;
    }

    @Override // android.view.View
    public void onFinishInflate() {
        super.onFinishInflate();
        ei4.w0(this, this.n0);
        TextView textView = (TextView) findViewById(q03.title);
        this.h0 = textView;
        int i = this.o0;
        if (i != -1) {
            textView.setTextAppearance(this.p0, i);
        }
        this.j0 = (TextView) findViewById(q03.shortcut);
        ImageView imageView = (ImageView) findViewById(q03.submenuarrow);
        this.k0 = imageView;
        if (imageView != null) {
            imageView.setImageDrawable(this.r0);
        }
        this.l0 = (ImageView) findViewById(q03.group_divider);
        this.m0 = (LinearLayout) findViewById(q03.content);
    }

    @Override // android.widget.LinearLayout, android.view.View
    public void onMeasure(int i, int i2) {
        if (this.f0 != null && this.q0) {
            ViewGroup.LayoutParams layoutParams = getLayoutParams();
            LinearLayout.LayoutParams layoutParams2 = (LinearLayout.LayoutParams) this.f0.getLayoutParams();
            int i3 = layoutParams.height;
            if (i3 > 0 && layoutParams2.width <= 0) {
                layoutParams2.width = i3;
            }
        }
        super.onMeasure(i, i2);
    }

    public void setCheckable(boolean z) {
        CompoundButton compoundButton;
        CompoundButton compoundButton2;
        if (!z && this.g0 == null && this.i0 == null) {
            return;
        }
        if (this.a.m()) {
            if (this.g0 == null) {
                g();
            }
            compoundButton = this.g0;
            compoundButton2 = this.i0;
        } else {
            if (this.i0 == null) {
                e();
            }
            compoundButton = this.i0;
            compoundButton2 = this.g0;
        }
        if (z) {
            compoundButton.setChecked(this.a.isChecked());
            if (compoundButton.getVisibility() != 0) {
                compoundButton.setVisibility(0);
            }
            if (compoundButton2 == null || compoundButton2.getVisibility() == 8) {
                return;
            }
            compoundButton2.setVisibility(8);
            return;
        }
        CheckBox checkBox = this.i0;
        if (checkBox != null) {
            checkBox.setVisibility(8);
        }
        RadioButton radioButton = this.g0;
        if (radioButton != null) {
            radioButton.setVisibility(8);
        }
    }

    public void setChecked(boolean z) {
        CompoundButton compoundButton;
        if (this.a.m()) {
            if (this.g0 == null) {
                g();
            }
            compoundButton = this.g0;
        } else {
            if (this.i0 == null) {
                e();
            }
            compoundButton = this.i0;
        }
        compoundButton.setChecked(z);
    }

    public void setForceShowIcon(boolean z) {
        this.u0 = z;
        this.q0 = z;
    }

    public void setGroupDividerEnabled(boolean z) {
        ImageView imageView = this.l0;
        if (imageView != null) {
            imageView.setVisibility((this.s0 || !z) ? 8 : 0);
        }
    }

    public void setIcon(Drawable drawable) {
        boolean z = this.a.z() || this.u0;
        if (z || this.q0) {
            ImageView imageView = this.f0;
            if (imageView == null && drawable == null && !this.q0) {
                return;
            }
            if (imageView == null) {
                f();
            }
            if (drawable == null && !this.q0) {
                this.f0.setVisibility(8);
                return;
            }
            ImageView imageView2 = this.f0;
            if (!z) {
                drawable = null;
            }
            imageView2.setImageDrawable(drawable);
            if (this.f0.getVisibility() != 0) {
                this.f0.setVisibility(0);
            }
        }
    }

    public void setShortcut(boolean z, char c) {
        int i = (z && this.a.A()) ? 0 : 8;
        if (i == 0) {
            this.j0.setText(this.a.h());
        }
        if (this.j0.getVisibility() != i) {
            this.j0.setVisibility(i);
        }
    }

    public void setTitle(CharSequence charSequence) {
        if (charSequence != null) {
            this.h0.setText(charSequence);
            if (this.h0.getVisibility() != 0) {
                this.h0.setVisibility(0);
            }
        } else if (this.h0.getVisibility() != 8) {
            this.h0.setVisibility(8);
        }
    }

    public ListMenuItemView(Context context, AttributeSet attributeSet, int i) {
        super(context, attributeSet);
        l64 v = l64.v(getContext(), attributeSet, d33.MenuView, i, 0);
        this.n0 = v.g(d33.MenuView_android_itemBackground);
        this.o0 = v.n(d33.MenuView_android_itemTextAppearance, -1);
        this.q0 = v.a(d33.MenuView_preserveIconSpacing, false);
        this.p0 = context;
        this.r0 = v.g(d33.MenuView_subMenuArrow);
        TypedArray obtainStyledAttributes = context.getTheme().obtainStyledAttributes(null, new int[]{16843049}, jy2.dropDownListViewStyle, 0);
        this.s0 = obtainStyledAttributes.hasValue(0);
        v.w();
        obtainStyledAttributes.recycle();
    }
}
