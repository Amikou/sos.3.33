package androidx.appcompat.view.menu;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import androidx.appcompat.view.menu.j;
import java.util.ArrayList;

/* compiled from: MenuAdapter.java */
/* loaded from: classes.dex */
public class d extends BaseAdapter {
    public e a;
    public int f0 = -1;
    public boolean g0;
    public final boolean h0;
    public final LayoutInflater i0;
    public final int j0;

    public d(e eVar, LayoutInflater layoutInflater, boolean z, int i) {
        this.h0 = z;
        this.i0 = layoutInflater;
        this.a = eVar;
        this.j0 = i;
        a();
    }

    public void a() {
        g x = this.a.x();
        if (x != null) {
            ArrayList<g> B = this.a.B();
            int size = B.size();
            for (int i = 0; i < size; i++) {
                if (B.get(i) == x) {
                    this.f0 = i;
                    return;
                }
            }
        }
        this.f0 = -1;
    }

    public e b() {
        return this.a;
    }

    @Override // android.widget.Adapter
    /* renamed from: c */
    public g getItem(int i) {
        ArrayList<g> B = this.h0 ? this.a.B() : this.a.G();
        int i2 = this.f0;
        if (i2 >= 0 && i >= i2) {
            i++;
        }
        return B.get(i);
    }

    public void d(boolean z) {
        this.g0 = z;
    }

    @Override // android.widget.Adapter
    public int getCount() {
        ArrayList<g> B = this.h0 ? this.a.B() : this.a.G();
        if (this.f0 < 0) {
            return B.size();
        }
        return B.size() - 1;
    }

    @Override // android.widget.Adapter
    public long getItemId(int i) {
        return i;
    }

    @Override // android.widget.Adapter
    public View getView(int i, View view, ViewGroup viewGroup) {
        if (view == null) {
            view = this.i0.inflate(this.j0, viewGroup, false);
        }
        int groupId = getItem(i).getGroupId();
        int i2 = i - 1;
        ListMenuItemView listMenuItemView = (ListMenuItemView) view;
        listMenuItemView.setGroupDividerEnabled(this.a.H() && groupId != (i2 >= 0 ? getItem(i2).getGroupId() : groupId));
        j.a aVar = (j.a) view;
        if (this.g0) {
            listMenuItemView.setForceShowIcon(true);
        }
        aVar.d(getItem(i), 0);
        return view;
    }

    @Override // android.widget.BaseAdapter
    public void notifyDataSetChanged() {
        a();
        super.notifyDataSetChanged();
    }
}
