package androidx.appcompat.view.menu;

import android.content.Context;
import android.content.res.Resources;
import android.os.Parcelable;
import android.view.Gravity;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import android.widget.FrameLayout;
import android.widget.ListView;
import android.widget.PopupWindow;
import android.widget.TextView;
import androidx.appcompat.view.menu.i;
import androidx.appcompat.widget.MenuPopupWindow;

/* compiled from: StandardMenuPopup.java */
/* loaded from: classes.dex */
public final class k extends v72 implements PopupWindow.OnDismissListener, View.OnKeyListener {
    public static final int z0 = d13.abc_popup_menu_item_layout;
    public final Context f0;
    public final e g0;
    public final d h0;
    public final boolean i0;
    public final int j0;
    public final int k0;
    public final int l0;
    public final MenuPopupWindow m0;
    public PopupWindow.OnDismissListener p0;
    public View q0;
    public View r0;
    public i.a s0;
    public ViewTreeObserver t0;
    public boolean u0;
    public boolean v0;
    public int w0;
    public boolean y0;
    public final ViewTreeObserver.OnGlobalLayoutListener n0 = new a();
    public final View.OnAttachStateChangeListener o0 = new b();
    public int x0 = 0;

    /* compiled from: StandardMenuPopup.java */
    /* loaded from: classes.dex */
    public class a implements ViewTreeObserver.OnGlobalLayoutListener {
        public a() {
        }

        @Override // android.view.ViewTreeObserver.OnGlobalLayoutListener
        public void onGlobalLayout() {
            if (!k.this.a() || k.this.m0.B()) {
                return;
            }
            View view = k.this.r0;
            if (view != null && view.isShown()) {
                k.this.m0.b();
            } else {
                k.this.dismiss();
            }
        }
    }

    /* compiled from: StandardMenuPopup.java */
    /* loaded from: classes.dex */
    public class b implements View.OnAttachStateChangeListener {
        public b() {
        }

        @Override // android.view.View.OnAttachStateChangeListener
        public void onViewAttachedToWindow(View view) {
        }

        @Override // android.view.View.OnAttachStateChangeListener
        public void onViewDetachedFromWindow(View view) {
            ViewTreeObserver viewTreeObserver = k.this.t0;
            if (viewTreeObserver != null) {
                if (!viewTreeObserver.isAlive()) {
                    k.this.t0 = view.getViewTreeObserver();
                }
                k kVar = k.this;
                kVar.t0.removeGlobalOnLayoutListener(kVar.n0);
            }
            view.removeOnAttachStateChangeListener(this);
        }
    }

    public k(Context context, e eVar, View view, int i, int i2, boolean z) {
        this.f0 = context;
        this.g0 = eVar;
        this.i0 = z;
        this.h0 = new d(eVar, LayoutInflater.from(context), z, z0);
        this.k0 = i;
        this.l0 = i2;
        Resources resources = context.getResources();
        this.j0 = Math.max(resources.getDisplayMetrics().widthPixels / 2, resources.getDimensionPixelSize(lz2.abc_config_prefDialogWidth));
        this.q0 = view;
        this.m0 = new MenuPopupWindow(context, null, i, i2);
        eVar.c(this, context);
    }

    public final boolean B() {
        View view;
        if (a()) {
            return true;
        }
        if (this.u0 || (view = this.q0) == null) {
            return false;
        }
        this.r0 = view;
        this.m0.K(this);
        this.m0.L(this);
        this.m0.J(true);
        View view2 = this.r0;
        boolean z = this.t0 == null;
        ViewTreeObserver viewTreeObserver = view2.getViewTreeObserver();
        this.t0 = viewTreeObserver;
        if (z) {
            viewTreeObserver.addOnGlobalLayoutListener(this.n0);
        }
        view2.addOnAttachStateChangeListener(this.o0);
        this.m0.D(view2);
        this.m0.G(this.x0);
        if (!this.v0) {
            this.w0 = v72.q(this.h0, null, this.f0, this.j0);
            this.v0 = true;
        }
        this.m0.F(this.w0);
        this.m0.I(2);
        this.m0.H(p());
        this.m0.b();
        ListView k = this.m0.k();
        k.setOnKeyListener(this);
        if (this.y0 && this.g0.z() != null) {
            FrameLayout frameLayout = (FrameLayout) LayoutInflater.from(this.f0).inflate(d13.abc_popup_menu_header_item_layout, (ViewGroup) k, false);
            TextView textView = (TextView) frameLayout.findViewById(16908310);
            if (textView != null) {
                textView.setText(this.g0.z());
            }
            frameLayout.setEnabled(false);
            k.addHeaderView(frameLayout, null, false);
        }
        this.m0.p(this.h0);
        this.m0.b();
        return true;
    }

    @Override // defpackage.po3
    public boolean a() {
        return !this.u0 && this.m0.a();
    }

    @Override // defpackage.po3
    public void b() {
        if (!B()) {
            throw new IllegalStateException("StandardMenuPopup cannot be used without an anchor");
        }
    }

    @Override // androidx.appcompat.view.menu.i
    public void c(e eVar, boolean z) {
        if (eVar != this.g0) {
            return;
        }
        dismiss();
        i.a aVar = this.s0;
        if (aVar != null) {
            aVar.c(eVar, z);
        }
    }

    @Override // androidx.appcompat.view.menu.i
    public void d(boolean z) {
        this.v0 = false;
        d dVar = this.h0;
        if (dVar != null) {
            dVar.notifyDataSetChanged();
        }
    }

    @Override // defpackage.po3
    public void dismiss() {
        if (a()) {
            this.m0.dismiss();
        }
    }

    @Override // androidx.appcompat.view.menu.i
    public boolean e() {
        return false;
    }

    @Override // androidx.appcompat.view.menu.i
    public void h(i.a aVar) {
        this.s0 = aVar;
    }

    @Override // androidx.appcompat.view.menu.i
    public void j(Parcelable parcelable) {
    }

    @Override // defpackage.po3
    public ListView k() {
        return this.m0.k();
    }

    @Override // androidx.appcompat.view.menu.i
    public boolean l(l lVar) {
        if (lVar.hasVisibleItems()) {
            h hVar = new h(this.f0, lVar, this.r0, this.i0, this.k0, this.l0);
            hVar.j(this.s0);
            hVar.g(v72.z(lVar));
            hVar.i(this.p0);
            this.p0 = null;
            this.g0.e(false);
            int d = this.m0.d();
            int o = this.m0.o();
            if ((Gravity.getAbsoluteGravity(this.x0, ei4.E(this.q0)) & 7) == 5) {
                d += this.q0.getWidth();
            }
            if (hVar.n(d, o)) {
                i.a aVar = this.s0;
                if (aVar != null) {
                    aVar.d(lVar);
                    return true;
                }
                return true;
            }
        }
        return false;
    }

    @Override // androidx.appcompat.view.menu.i
    public Parcelable m() {
        return null;
    }

    @Override // defpackage.v72
    public void n(e eVar) {
    }

    @Override // android.widget.PopupWindow.OnDismissListener
    public void onDismiss() {
        this.u0 = true;
        this.g0.close();
        ViewTreeObserver viewTreeObserver = this.t0;
        if (viewTreeObserver != null) {
            if (!viewTreeObserver.isAlive()) {
                this.t0 = this.r0.getViewTreeObserver();
            }
            this.t0.removeGlobalOnLayoutListener(this.n0);
            this.t0 = null;
        }
        this.r0.removeOnAttachStateChangeListener(this.o0);
        PopupWindow.OnDismissListener onDismissListener = this.p0;
        if (onDismissListener != null) {
            onDismissListener.onDismiss();
        }
    }

    @Override // android.view.View.OnKeyListener
    public boolean onKey(View view, int i, KeyEvent keyEvent) {
        if (keyEvent.getAction() == 1 && i == 82) {
            dismiss();
            return true;
        }
        return false;
    }

    @Override // defpackage.v72
    public void r(View view) {
        this.q0 = view;
    }

    @Override // defpackage.v72
    public void t(boolean z) {
        this.h0.d(z);
    }

    @Override // defpackage.v72
    public void u(int i) {
        this.x0 = i;
    }

    @Override // defpackage.v72
    public void v(int i) {
        this.m0.f(i);
    }

    @Override // defpackage.v72
    public void w(PopupWindow.OnDismissListener onDismissListener) {
        this.p0 = onDismissListener;
    }

    @Override // defpackage.v72
    public void x(boolean z) {
        this.y0 = z;
    }

    @Override // defpackage.v72
    public void y(int i) {
        this.m0.l(i);
    }
}
