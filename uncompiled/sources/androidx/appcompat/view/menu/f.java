package androidx.appcompat.view.menu;

import android.content.DialogInterface;
import android.os.IBinder;
import android.view.KeyEvent;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import androidx.appcompat.app.a;
import androidx.appcompat.view.menu.i;
import androidx.media3.common.PlaybackException;

/* compiled from: MenuDialogHelper.java */
/* loaded from: classes.dex */
public class f implements DialogInterface.OnKeyListener, DialogInterface.OnClickListener, DialogInterface.OnDismissListener, i.a {
    public e a;
    public androidx.appcompat.app.a f0;
    public c g0;
    public i.a h0;

    public f(e eVar) {
        this.a = eVar;
    }

    public void a() {
        androidx.appcompat.app.a aVar = this.f0;
        if (aVar != null) {
            aVar.dismiss();
        }
    }

    public void b(IBinder iBinder) {
        e eVar = this.a;
        a.C0008a c0008a = new a.C0008a(eVar.w());
        c cVar = new c(c0008a.getContext(), d13.abc_list_menu_item_layout);
        this.g0 = cVar;
        cVar.h(this);
        this.a.b(this.g0);
        c0008a.a(this.g0.a(), this);
        View A = eVar.A();
        if (A != null) {
            c0008a.c(A);
        } else {
            c0008a.d(eVar.y()).setTitle(eVar.z());
        }
        c0008a.h(this);
        androidx.appcompat.app.a create = c0008a.create();
        this.f0 = create;
        create.setOnDismissListener(this);
        WindowManager.LayoutParams attributes = this.f0.getWindow().getAttributes();
        attributes.type = PlaybackException.ERROR_CODE_TIMEOUT;
        if (iBinder != null) {
            attributes.token = iBinder;
        }
        attributes.flags |= 131072;
        this.f0.show();
    }

    @Override // androidx.appcompat.view.menu.i.a
    public void c(e eVar, boolean z) {
        if (z || eVar == this.a) {
            a();
        }
        i.a aVar = this.h0;
        if (aVar != null) {
            aVar.c(eVar, z);
        }
    }

    @Override // androidx.appcompat.view.menu.i.a
    public boolean d(e eVar) {
        i.a aVar = this.h0;
        if (aVar != null) {
            return aVar.d(eVar);
        }
        return false;
    }

    @Override // android.content.DialogInterface.OnClickListener
    public void onClick(DialogInterface dialogInterface, int i) {
        this.a.N((g) this.g0.a().getItem(i), 0);
    }

    @Override // android.content.DialogInterface.OnDismissListener
    public void onDismiss(DialogInterface dialogInterface) {
        this.g0.c(this.a, true);
    }

    @Override // android.content.DialogInterface.OnKeyListener
    public boolean onKey(DialogInterface dialogInterface, int i, KeyEvent keyEvent) {
        Window window;
        View decorView;
        KeyEvent.DispatcherState keyDispatcherState;
        View decorView2;
        KeyEvent.DispatcherState keyDispatcherState2;
        if (i == 82 || i == 4) {
            if (keyEvent.getAction() == 0 && keyEvent.getRepeatCount() == 0) {
                Window window2 = this.f0.getWindow();
                if (window2 != null && (decorView2 = window2.getDecorView()) != null && (keyDispatcherState2 = decorView2.getKeyDispatcherState()) != null) {
                    keyDispatcherState2.startTracking(keyEvent, this);
                    return true;
                }
            } else if (keyEvent.getAction() == 1 && !keyEvent.isCanceled() && (window = this.f0.getWindow()) != null && (decorView = window.getDecorView()) != null && (keyDispatcherState = decorView.getKeyDispatcherState()) != null && keyDispatcherState.isTracking(keyEvent)) {
                this.a.e(true);
                dialogInterface.dismiss();
                return true;
            }
        }
        return this.a.performShortcut(i, keyEvent, 0);
    }
}
