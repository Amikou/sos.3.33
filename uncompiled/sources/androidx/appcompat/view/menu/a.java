package androidx.appcompat.view.menu;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import androidx.appcompat.view.menu.i;
import androidx.appcompat.view.menu.j;
import java.util.ArrayList;

/* compiled from: BaseMenuPresenter.java */
/* loaded from: classes.dex */
public abstract class a implements i {
    public Context a;
    public Context f0;
    public e g0;
    public LayoutInflater h0;
    public i.a i0;
    public int j0;
    public int k0;
    public j l0;
    public int m0;

    public a(Context context, int i, int i2) {
        this.a = context;
        this.h0 = LayoutInflater.from(context);
        this.j0 = i;
        this.k0 = i2;
    }

    public void b(View view, int i) {
        ViewGroup viewGroup = (ViewGroup) view.getParent();
        if (viewGroup != null) {
            viewGroup.removeView(view);
        }
        ((ViewGroup) this.l0).addView(view, i);
    }

    @Override // androidx.appcompat.view.menu.i
    public void c(e eVar, boolean z) {
        i.a aVar = this.i0;
        if (aVar != null) {
            aVar.c(eVar, z);
        }
    }

    @Override // androidx.appcompat.view.menu.i
    public void d(boolean z) {
        ViewGroup viewGroup = (ViewGroup) this.l0;
        if (viewGroup == null) {
            return;
        }
        e eVar = this.g0;
        int i = 0;
        if (eVar != null) {
            eVar.t();
            ArrayList<g> G = this.g0.G();
            int size = G.size();
            int i2 = 0;
            for (int i3 = 0; i3 < size; i3++) {
                g gVar = G.get(i3);
                if (t(i2, gVar)) {
                    View childAt = viewGroup.getChildAt(i2);
                    g itemData = childAt instanceof j.a ? ((j.a) childAt).getItemData() : null;
                    View q = q(gVar, childAt, viewGroup);
                    if (gVar != itemData) {
                        q.setPressed(false);
                        q.jumpDrawablesToCurrentState();
                    }
                    if (q != childAt) {
                        b(q, i2);
                    }
                    i2++;
                }
            }
            i = i2;
        }
        while (i < viewGroup.getChildCount()) {
            if (!o(viewGroup, i)) {
                i++;
            }
        }
    }

    @Override // androidx.appcompat.view.menu.i
    public boolean f(e eVar, g gVar) {
        return false;
    }

    @Override // androidx.appcompat.view.menu.i
    public boolean g(e eVar, g gVar) {
        return false;
    }

    @Override // androidx.appcompat.view.menu.i
    public int getId() {
        return this.m0;
    }

    @Override // androidx.appcompat.view.menu.i
    public void h(i.a aVar) {
        this.i0 = aVar;
    }

    @Override // androidx.appcompat.view.menu.i
    public void i(Context context, e eVar) {
        this.f0 = context;
        LayoutInflater.from(context);
        this.g0 = eVar;
    }

    public abstract void k(g gVar, j.a aVar);

    /* JADX WARN: Multi-variable type inference failed */
    /* JADX WARN: Type inference failed for: r2v4, types: [androidx.appcompat.view.menu.e] */
    @Override // androidx.appcompat.view.menu.i
    public boolean l(l lVar) {
        i.a aVar = this.i0;
        l lVar2 = lVar;
        if (aVar != null) {
            if (lVar == null) {
                lVar2 = this.g0;
            }
            return aVar.d(lVar2);
        }
        return false;
    }

    public j.a n(ViewGroup viewGroup) {
        return (j.a) this.h0.inflate(this.k0, viewGroup, false);
    }

    public boolean o(ViewGroup viewGroup, int i) {
        viewGroup.removeViewAt(i);
        return true;
    }

    public i.a p() {
        return this.i0;
    }

    public View q(g gVar, View view, ViewGroup viewGroup) {
        j.a n;
        if (view instanceof j.a) {
            n = (j.a) view;
        } else {
            n = n(viewGroup);
        }
        k(gVar, n);
        return (View) n;
    }

    public j r(ViewGroup viewGroup) {
        if (this.l0 == null) {
            j jVar = (j) this.h0.inflate(this.j0, viewGroup, false);
            this.l0 = jVar;
            jVar.b(this.g0);
            d(true);
        }
        return this.l0;
    }

    public void s(int i) {
        this.m0 = i;
    }

    public abstract boolean t(int i, g gVar);
}
