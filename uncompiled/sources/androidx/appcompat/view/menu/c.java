package androidx.appcompat.view.menu;

import android.content.Context;
import android.os.Bundle;
import android.os.Parcelable;
import android.util.SparseArray;
import android.view.ContextThemeWrapper;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.ListAdapter;
import androidx.appcompat.view.menu.i;
import androidx.appcompat.view.menu.j;
import java.util.ArrayList;

/* compiled from: ListMenuPresenter.java */
/* loaded from: classes.dex */
public class c implements i, AdapterView.OnItemClickListener {
    public Context a;
    public LayoutInflater f0;
    public e g0;
    public ExpandedMenuView h0;
    public int i0;
    public int j0;
    public int k0;
    public i.a l0;
    public a m0;
    public int n0;

    /* compiled from: ListMenuPresenter.java */
    /* loaded from: classes.dex */
    public class a extends BaseAdapter {
        public int a = -1;

        public a() {
            a();
        }

        public void a() {
            g x = c.this.g0.x();
            if (x != null) {
                ArrayList<g> B = c.this.g0.B();
                int size = B.size();
                for (int i = 0; i < size; i++) {
                    if (B.get(i) == x) {
                        this.a = i;
                        return;
                    }
                }
            }
            this.a = -1;
        }

        @Override // android.widget.Adapter
        /* renamed from: b */
        public g getItem(int i) {
            ArrayList<g> B = c.this.g0.B();
            int i2 = i + c.this.i0;
            int i3 = this.a;
            if (i3 >= 0 && i2 >= i3) {
                i2++;
            }
            return B.get(i2);
        }

        @Override // android.widget.Adapter
        public int getCount() {
            int size = c.this.g0.B().size() - c.this.i0;
            return this.a < 0 ? size : size - 1;
        }

        @Override // android.widget.Adapter
        public long getItemId(int i) {
            return i;
        }

        @Override // android.widget.Adapter
        public View getView(int i, View view, ViewGroup viewGroup) {
            if (view == null) {
                c cVar = c.this;
                view = cVar.f0.inflate(cVar.k0, viewGroup, false);
            }
            ((j.a) view).d(getItem(i), 0);
            return view;
        }

        @Override // android.widget.BaseAdapter
        public void notifyDataSetChanged() {
            a();
            super.notifyDataSetChanged();
        }
    }

    public c(Context context, int i) {
        this(i, 0);
        this.a = context;
        this.f0 = LayoutInflater.from(context);
    }

    public ListAdapter a() {
        if (this.m0 == null) {
            this.m0 = new a();
        }
        return this.m0;
    }

    public j b(ViewGroup viewGroup) {
        if (this.h0 == null) {
            this.h0 = (ExpandedMenuView) this.f0.inflate(d13.abc_expanded_menu_layout, viewGroup, false);
            if (this.m0 == null) {
                this.m0 = new a();
            }
            this.h0.setAdapter((ListAdapter) this.m0);
            this.h0.setOnItemClickListener(this);
        }
        return this.h0;
    }

    @Override // androidx.appcompat.view.menu.i
    public void c(e eVar, boolean z) {
        i.a aVar = this.l0;
        if (aVar != null) {
            aVar.c(eVar, z);
        }
    }

    @Override // androidx.appcompat.view.menu.i
    public void d(boolean z) {
        a aVar = this.m0;
        if (aVar != null) {
            aVar.notifyDataSetChanged();
        }
    }

    @Override // androidx.appcompat.view.menu.i
    public boolean e() {
        return false;
    }

    @Override // androidx.appcompat.view.menu.i
    public boolean f(e eVar, g gVar) {
        return false;
    }

    @Override // androidx.appcompat.view.menu.i
    public boolean g(e eVar, g gVar) {
        return false;
    }

    @Override // androidx.appcompat.view.menu.i
    public int getId() {
        return this.n0;
    }

    @Override // androidx.appcompat.view.menu.i
    public void h(i.a aVar) {
        this.l0 = aVar;
    }

    @Override // androidx.appcompat.view.menu.i
    public void i(Context context, e eVar) {
        if (this.j0 != 0) {
            ContextThemeWrapper contextThemeWrapper = new ContextThemeWrapper(context, this.j0);
            this.a = contextThemeWrapper;
            this.f0 = LayoutInflater.from(contextThemeWrapper);
        } else if (this.a != null) {
            this.a = context;
            if (this.f0 == null) {
                this.f0 = LayoutInflater.from(context);
            }
        }
        this.g0 = eVar;
        a aVar = this.m0;
        if (aVar != null) {
            aVar.notifyDataSetChanged();
        }
    }

    @Override // androidx.appcompat.view.menu.i
    public void j(Parcelable parcelable) {
        k((Bundle) parcelable);
    }

    public void k(Bundle bundle) {
        SparseArray<Parcelable> sparseParcelableArray = bundle.getSparseParcelableArray("android:menu:list");
        if (sparseParcelableArray != null) {
            this.h0.restoreHierarchyState(sparseParcelableArray);
        }
    }

    @Override // androidx.appcompat.view.menu.i
    public boolean l(l lVar) {
        if (lVar.hasVisibleItems()) {
            new f(lVar).b(null);
            i.a aVar = this.l0;
            if (aVar != null) {
                aVar.d(lVar);
                return true;
            }
            return true;
        }
        return false;
    }

    @Override // androidx.appcompat.view.menu.i
    public Parcelable m() {
        if (this.h0 == null) {
            return null;
        }
        Bundle bundle = new Bundle();
        n(bundle);
        return bundle;
    }

    public void n(Bundle bundle) {
        SparseArray<Parcelable> sparseArray = new SparseArray<>();
        ExpandedMenuView expandedMenuView = this.h0;
        if (expandedMenuView != null) {
            expandedMenuView.saveHierarchyState(sparseArray);
        }
        bundle.putSparseParcelableArray("android:menu:list", sparseArray);
    }

    @Override // android.widget.AdapterView.OnItemClickListener
    public void onItemClick(AdapterView<?> adapterView, View view, int i, long j) {
        this.g0.O(this.m0.getItem(i), this, 0);
    }

    public c(int i, int i2) {
        this.k0 = i;
        this.j0 = i2;
    }
}
