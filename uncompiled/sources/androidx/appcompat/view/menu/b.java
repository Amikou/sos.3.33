package androidx.appcompat.view.menu;

import android.content.Context;
import android.content.res.Resources;
import android.graphics.Rect;
import android.os.Build;
import android.os.Handler;
import android.os.Parcelable;
import android.os.SystemClock;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import android.widget.FrameLayout;
import android.widget.HeaderViewListAdapter;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.PopupWindow;
import android.widget.TextView;
import androidx.appcompat.view.menu.i;
import androidx.appcompat.widget.MenuPopupWindow;
import java.util.ArrayList;
import java.util.List;

/* compiled from: CascadingMenuPopup.java */
/* loaded from: classes.dex */
public final class b extends v72 implements View.OnKeyListener, PopupWindow.OnDismissListener {
    public static final int F0 = d13.abc_cascading_menu_item_layout;
    public boolean A0;
    public i.a B0;
    public ViewTreeObserver C0;
    public PopupWindow.OnDismissListener D0;
    public boolean E0;
    public final Context f0;
    public final int g0;
    public final int h0;
    public final int i0;
    public final boolean j0;
    public final Handler k0;
    public View s0;
    public View t0;
    public boolean v0;
    public boolean w0;
    public int x0;
    public int y0;
    public final List<e> l0 = new ArrayList();
    public final List<d> m0 = new ArrayList();
    public final ViewTreeObserver.OnGlobalLayoutListener n0 = new a();
    public final View.OnAttachStateChangeListener o0 = new View$OnAttachStateChangeListenerC0010b();
    public final t72 p0 = new c();
    public int q0 = 0;
    public int r0 = 0;
    public boolean z0 = false;
    public int u0 = F();

    /* compiled from: CascadingMenuPopup.java */
    /* loaded from: classes.dex */
    public class a implements ViewTreeObserver.OnGlobalLayoutListener {
        public a() {
        }

        @Override // android.view.ViewTreeObserver.OnGlobalLayoutListener
        public void onGlobalLayout() {
            if (!b.this.a() || b.this.m0.size() <= 0 || b.this.m0.get(0).a.B()) {
                return;
            }
            View view = b.this.t0;
            if (view != null && view.isShown()) {
                for (d dVar : b.this.m0) {
                    dVar.a.b();
                }
                return;
            }
            b.this.dismiss();
        }
    }

    /* compiled from: CascadingMenuPopup.java */
    /* renamed from: androidx.appcompat.view.menu.b$b  reason: collision with other inner class name */
    /* loaded from: classes.dex */
    public class View$OnAttachStateChangeListenerC0010b implements View.OnAttachStateChangeListener {
        public View$OnAttachStateChangeListenerC0010b() {
        }

        @Override // android.view.View.OnAttachStateChangeListener
        public void onViewAttachedToWindow(View view) {
        }

        @Override // android.view.View.OnAttachStateChangeListener
        public void onViewDetachedFromWindow(View view) {
            ViewTreeObserver viewTreeObserver = b.this.C0;
            if (viewTreeObserver != null) {
                if (!viewTreeObserver.isAlive()) {
                    b.this.C0 = view.getViewTreeObserver();
                }
                b bVar = b.this;
                bVar.C0.removeGlobalOnLayoutListener(bVar.n0);
            }
            view.removeOnAttachStateChangeListener(this);
        }
    }

    /* compiled from: CascadingMenuPopup.java */
    /* loaded from: classes.dex */
    public class c implements t72 {

        /* compiled from: CascadingMenuPopup.java */
        /* loaded from: classes.dex */
        public class a implements Runnable {
            public final /* synthetic */ d a;
            public final /* synthetic */ MenuItem f0;
            public final /* synthetic */ e g0;

            public a(d dVar, MenuItem menuItem, e eVar) {
                this.a = dVar;
                this.f0 = menuItem;
                this.g0 = eVar;
            }

            @Override // java.lang.Runnable
            public void run() {
                d dVar = this.a;
                if (dVar != null) {
                    b.this.E0 = true;
                    dVar.b.e(false);
                    b.this.E0 = false;
                }
                if (this.f0.isEnabled() && this.f0.hasSubMenu()) {
                    this.g0.N(this.f0, 4);
                }
            }
        }

        public c() {
        }

        @Override // defpackage.t72
        public void e(e eVar, MenuItem menuItem) {
            b.this.k0.removeCallbacksAndMessages(null);
            int size = b.this.m0.size();
            int i = 0;
            while (true) {
                if (i >= size) {
                    i = -1;
                    break;
                } else if (eVar == b.this.m0.get(i).b) {
                    break;
                } else {
                    i++;
                }
            }
            if (i == -1) {
                return;
            }
            int i2 = i + 1;
            b.this.k0.postAtTime(new a(i2 < b.this.m0.size() ? b.this.m0.get(i2) : null, menuItem, eVar), eVar, SystemClock.uptimeMillis() + 200);
        }

        @Override // defpackage.t72
        public void h(e eVar, MenuItem menuItem) {
            b.this.k0.removeCallbacksAndMessages(eVar);
        }
    }

    /* compiled from: CascadingMenuPopup.java */
    /* loaded from: classes.dex */
    public static class d {
        public final MenuPopupWindow a;
        public final e b;
        public final int c;

        public d(MenuPopupWindow menuPopupWindow, e eVar, int i) {
            this.a = menuPopupWindow;
            this.b = eVar;
            this.c = i;
        }

        public ListView a() {
            return this.a.k();
        }
    }

    public b(Context context, View view, int i, int i2, boolean z) {
        this.f0 = context;
        this.s0 = view;
        this.h0 = i;
        this.i0 = i2;
        this.j0 = z;
        Resources resources = context.getResources();
        this.g0 = Math.max(resources.getDisplayMetrics().widthPixels / 2, resources.getDimensionPixelSize(lz2.abc_config_prefDialogWidth));
        this.k0 = new Handler();
    }

    public final MenuPopupWindow B() {
        MenuPopupWindow menuPopupWindow = new MenuPopupWindow(this.f0, null, this.h0, this.i0);
        menuPopupWindow.T(this.p0);
        menuPopupWindow.L(this);
        menuPopupWindow.K(this);
        menuPopupWindow.D(this.s0);
        menuPopupWindow.G(this.r0);
        menuPopupWindow.J(true);
        menuPopupWindow.I(2);
        return menuPopupWindow;
    }

    public final int C(e eVar) {
        int size = this.m0.size();
        for (int i = 0; i < size; i++) {
            if (eVar == this.m0.get(i).b) {
                return i;
            }
        }
        return -1;
    }

    public final MenuItem D(e eVar, e eVar2) {
        int size = eVar.size();
        for (int i = 0; i < size; i++) {
            MenuItem item = eVar.getItem(i);
            if (item.hasSubMenu() && eVar2 == item.getSubMenu()) {
                return item;
            }
        }
        return null;
    }

    public final View E(d dVar, e eVar) {
        androidx.appcompat.view.menu.d dVar2;
        int i;
        int firstVisiblePosition;
        MenuItem D = D(dVar.b, eVar);
        if (D == null) {
            return null;
        }
        ListView a2 = dVar.a();
        ListAdapter adapter = a2.getAdapter();
        int i2 = 0;
        if (adapter instanceof HeaderViewListAdapter) {
            HeaderViewListAdapter headerViewListAdapter = (HeaderViewListAdapter) adapter;
            i = headerViewListAdapter.getHeadersCount();
            dVar2 = (androidx.appcompat.view.menu.d) headerViewListAdapter.getWrappedAdapter();
        } else {
            dVar2 = (androidx.appcompat.view.menu.d) adapter;
            i = 0;
        }
        int count = dVar2.getCount();
        while (true) {
            if (i2 >= count) {
                i2 = -1;
                break;
            } else if (D == dVar2.getItem(i2)) {
                break;
            } else {
                i2++;
            }
        }
        if (i2 != -1 && (firstVisiblePosition = (i2 + i) - a2.getFirstVisiblePosition()) >= 0 && firstVisiblePosition < a2.getChildCount()) {
            return a2.getChildAt(firstVisiblePosition);
        }
        return null;
    }

    public final int F() {
        return ei4.E(this.s0) == 1 ? 0 : 1;
    }

    public final int G(int i) {
        List<d> list = this.m0;
        ListView a2 = list.get(list.size() - 1).a();
        int[] iArr = new int[2];
        a2.getLocationOnScreen(iArr);
        Rect rect = new Rect();
        this.t0.getWindowVisibleDisplayFrame(rect);
        return this.u0 == 1 ? (iArr[0] + a2.getWidth()) + i > rect.right ? 0 : 1 : iArr[0] - i < 0 ? 1 : 0;
    }

    public final void H(e eVar) {
        d dVar;
        View view;
        int i;
        int i2;
        int i3;
        LayoutInflater from = LayoutInflater.from(this.f0);
        androidx.appcompat.view.menu.d dVar2 = new androidx.appcompat.view.menu.d(eVar, from, this.j0, F0);
        if (!a() && this.z0) {
            dVar2.d(true);
        } else if (a()) {
            dVar2.d(v72.z(eVar));
        }
        int q = v72.q(dVar2, null, this.f0, this.g0);
        MenuPopupWindow B = B();
        B.p(dVar2);
        B.F(q);
        B.G(this.r0);
        if (this.m0.size() > 0) {
            List<d> list = this.m0;
            dVar = list.get(list.size() - 1);
            view = E(dVar, eVar);
        } else {
            dVar = null;
            view = null;
        }
        if (view != null) {
            B.U(false);
            B.R(null);
            int G = G(q);
            boolean z = G == 1;
            this.u0 = G;
            if (Build.VERSION.SDK_INT >= 26) {
                B.D(view);
                i2 = 0;
                i = 0;
            } else {
                int[] iArr = new int[2];
                this.s0.getLocationOnScreen(iArr);
                int[] iArr2 = new int[2];
                view.getLocationOnScreen(iArr2);
                if ((this.r0 & 7) == 5) {
                    iArr[0] = iArr[0] + this.s0.getWidth();
                    iArr2[0] = iArr2[0] + view.getWidth();
                }
                i = iArr2[0] - iArr[0];
                i2 = iArr2[1] - iArr[1];
            }
            if ((this.r0 & 5) == 5) {
                if (!z) {
                    q = view.getWidth();
                    i3 = i - q;
                }
                i3 = i + q;
            } else {
                if (z) {
                    q = view.getWidth();
                    i3 = i + q;
                }
                i3 = i - q;
            }
            B.f(i3);
            B.M(true);
            B.l(i2);
        } else {
            if (this.v0) {
                B.f(this.x0);
            }
            if (this.w0) {
                B.l(this.y0);
            }
            B.H(p());
        }
        this.m0.add(new d(B, eVar, this.u0));
        B.b();
        ListView k = B.k();
        k.setOnKeyListener(this);
        if (dVar == null && this.A0 && eVar.z() != null) {
            FrameLayout frameLayout = (FrameLayout) from.inflate(d13.abc_popup_menu_header_item_layout, (ViewGroup) k, false);
            frameLayout.setEnabled(false);
            ((TextView) frameLayout.findViewById(16908310)).setText(eVar.z());
            k.addHeaderView(frameLayout, null, false);
            B.b();
        }
    }

    @Override // defpackage.po3
    public boolean a() {
        return this.m0.size() > 0 && this.m0.get(0).a.a();
    }

    @Override // defpackage.po3
    public void b() {
        if (a()) {
            return;
        }
        for (e eVar : this.l0) {
            H(eVar);
        }
        this.l0.clear();
        View view = this.s0;
        this.t0 = view;
        if (view != null) {
            boolean z = this.C0 == null;
            ViewTreeObserver viewTreeObserver = view.getViewTreeObserver();
            this.C0 = viewTreeObserver;
            if (z) {
                viewTreeObserver.addOnGlobalLayoutListener(this.n0);
            }
            this.t0.addOnAttachStateChangeListener(this.o0);
        }
    }

    @Override // androidx.appcompat.view.menu.i
    public void c(e eVar, boolean z) {
        int C = C(eVar);
        if (C < 0) {
            return;
        }
        int i = C + 1;
        if (i < this.m0.size()) {
            this.m0.get(i).b.e(false);
        }
        d remove = this.m0.remove(C);
        remove.b.Q(this);
        if (this.E0) {
            remove.a.S(null);
            remove.a.E(0);
        }
        remove.a.dismiss();
        int size = this.m0.size();
        if (size > 0) {
            this.u0 = this.m0.get(size - 1).c;
        } else {
            this.u0 = F();
        }
        if (size != 0) {
            if (z) {
                this.m0.get(0).b.e(false);
                return;
            }
            return;
        }
        dismiss();
        i.a aVar = this.B0;
        if (aVar != null) {
            aVar.c(eVar, true);
        }
        ViewTreeObserver viewTreeObserver = this.C0;
        if (viewTreeObserver != null) {
            if (viewTreeObserver.isAlive()) {
                this.C0.removeGlobalOnLayoutListener(this.n0);
            }
            this.C0 = null;
        }
        this.t0.removeOnAttachStateChangeListener(this.o0);
        this.D0.onDismiss();
    }

    @Override // androidx.appcompat.view.menu.i
    public void d(boolean z) {
        for (d dVar : this.m0) {
            v72.A(dVar.a().getAdapter()).notifyDataSetChanged();
        }
    }

    @Override // defpackage.po3
    public void dismiss() {
        int size = this.m0.size();
        if (size > 0) {
            d[] dVarArr = (d[]) this.m0.toArray(new d[size]);
            for (int i = size - 1; i >= 0; i--) {
                d dVar = dVarArr[i];
                if (dVar.a.a()) {
                    dVar.a.dismiss();
                }
            }
        }
    }

    @Override // androidx.appcompat.view.menu.i
    public boolean e() {
        return false;
    }

    @Override // androidx.appcompat.view.menu.i
    public void h(i.a aVar) {
        this.B0 = aVar;
    }

    @Override // androidx.appcompat.view.menu.i
    public void j(Parcelable parcelable) {
    }

    @Override // defpackage.po3
    public ListView k() {
        if (this.m0.isEmpty()) {
            return null;
        }
        List<d> list = this.m0;
        return list.get(list.size() - 1).a();
    }

    @Override // androidx.appcompat.view.menu.i
    public boolean l(l lVar) {
        for (d dVar : this.m0) {
            if (lVar == dVar.b) {
                dVar.a().requestFocus();
                return true;
            }
        }
        if (lVar.hasVisibleItems()) {
            n(lVar);
            i.a aVar = this.B0;
            if (aVar != null) {
                aVar.d(lVar);
            }
            return true;
        }
        return false;
    }

    @Override // androidx.appcompat.view.menu.i
    public Parcelable m() {
        return null;
    }

    @Override // defpackage.v72
    public void n(e eVar) {
        eVar.c(this, this.f0);
        if (a()) {
            H(eVar);
        } else {
            this.l0.add(eVar);
        }
    }

    @Override // defpackage.v72
    public boolean o() {
        return false;
    }

    @Override // android.widget.PopupWindow.OnDismissListener
    public void onDismiss() {
        d dVar;
        int size = this.m0.size();
        int i = 0;
        while (true) {
            if (i >= size) {
                dVar = null;
                break;
            }
            dVar = this.m0.get(i);
            if (!dVar.a.a()) {
                break;
            }
            i++;
        }
        if (dVar != null) {
            dVar.b.e(false);
        }
    }

    @Override // android.view.View.OnKeyListener
    public boolean onKey(View view, int i, KeyEvent keyEvent) {
        if (keyEvent.getAction() == 1 && i == 82) {
            dismiss();
            return true;
        }
        return false;
    }

    @Override // defpackage.v72
    public void r(View view) {
        if (this.s0 != view) {
            this.s0 = view;
            this.r0 = ri1.b(this.q0, ei4.E(view));
        }
    }

    @Override // defpackage.v72
    public void t(boolean z) {
        this.z0 = z;
    }

    @Override // defpackage.v72
    public void u(int i) {
        if (this.q0 != i) {
            this.q0 = i;
            this.r0 = ri1.b(i, ei4.E(this.s0));
        }
    }

    @Override // defpackage.v72
    public void v(int i) {
        this.v0 = true;
        this.x0 = i;
    }

    @Override // defpackage.v72
    public void w(PopupWindow.OnDismissListener onDismissListener) {
        this.D0 = onDismissListener;
    }

    @Override // defpackage.v72
    public void x(boolean z) {
        this.A0 = z;
    }

    @Override // defpackage.v72
    public void y(int i) {
        this.w0 = true;
        this.y0 = i;
    }
}
