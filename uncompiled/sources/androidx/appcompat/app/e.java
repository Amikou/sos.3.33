package androidx.appcompat.app;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.res.Configuration;
import android.content.res.TypedArray;
import android.graphics.drawable.Drawable;
import android.util.TypedValue;
import android.view.ContextThemeWrapper;
import android.view.KeyCharacterMap;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.animation.AccelerateInterpolator;
import android.view.animation.DecelerateInterpolator;
import android.view.animation.Interpolator;
import androidx.appcompat.app.ActionBar;
import androidx.appcompat.view.menu.e;
import androidx.appcompat.widget.ActionBarContainer;
import androidx.appcompat.widget.ActionBarContextView;
import androidx.appcompat.widget.ActionBarOverlayLayout;
import androidx.appcompat.widget.ScrollingTabContainerView;
import androidx.appcompat.widget.Toolbar;
import com.github.mikephil.charting.utils.Utils;
import defpackage.k6;
import java.lang.ref.WeakReference;
import java.util.ArrayList;

/* compiled from: WindowDecorActionBar.java */
/* loaded from: classes.dex */
public class e extends ActionBar implements ActionBarOverlayLayout.d {
    public static final Interpolator B = new AccelerateInterpolator();
    public static final Interpolator C = new DecelerateInterpolator();
    public final zj4 A;
    public Context a;
    public Context b;
    public ActionBarOverlayLayout c;
    public ActionBarContainer d;
    public lf0 e;
    public ActionBarContextView f;
    public View g;
    public ScrollingTabContainerView h;
    public boolean i;
    public d j;
    public k6 k;
    public k6.a l;
    public boolean m;
    public ArrayList<ActionBar.a> n;
    public boolean o;
    public int p;
    public boolean q;
    public boolean r;
    public boolean s;
    public boolean t;
    public boolean u;
    public wj4 v;
    public boolean w;
    public boolean x;
    public final xj4 y;
    public final xj4 z;

    /* compiled from: WindowDecorActionBar.java */
    /* loaded from: classes.dex */
    public class a extends yj4 {
        public a() {
        }

        @Override // defpackage.xj4
        public void b(View view) {
            View view2;
            e eVar = e.this;
            if (eVar.q && (view2 = eVar.g) != null) {
                view2.setTranslationY(Utils.FLOAT_EPSILON);
                e.this.d.setTranslationY(Utils.FLOAT_EPSILON);
            }
            e.this.d.setVisibility(8);
            e.this.d.setTransitioning(false);
            e eVar2 = e.this;
            eVar2.v = null;
            eVar2.C();
            ActionBarOverlayLayout actionBarOverlayLayout = e.this.c;
            if (actionBarOverlayLayout != null) {
                ei4.q0(actionBarOverlayLayout);
            }
        }
    }

    /* compiled from: WindowDecorActionBar.java */
    /* loaded from: classes.dex */
    public class b extends yj4 {
        public b() {
        }

        @Override // defpackage.xj4
        public void b(View view) {
            e eVar = e.this;
            eVar.v = null;
            eVar.d.requestLayout();
        }
    }

    /* compiled from: WindowDecorActionBar.java */
    /* loaded from: classes.dex */
    public class c implements zj4 {
        public c() {
        }

        @Override // defpackage.zj4
        public void a(View view) {
            ((View) e.this.d.getParent()).invalidate();
        }
    }

    /* compiled from: WindowDecorActionBar.java */
    /* loaded from: classes.dex */
    public class d extends k6 implements e.a {
        public final Context g0;
        public final androidx.appcompat.view.menu.e h0;
        public k6.a i0;
        public WeakReference<View> j0;

        public d(Context context, k6.a aVar) {
            this.g0 = context;
            this.i0 = aVar;
            androidx.appcompat.view.menu.e W = new androidx.appcompat.view.menu.e(context).W(1);
            this.h0 = W;
            W.V(this);
        }

        @Override // androidx.appcompat.view.menu.e.a
        public boolean a(androidx.appcompat.view.menu.e eVar, MenuItem menuItem) {
            k6.a aVar = this.i0;
            if (aVar != null) {
                return aVar.a(this, menuItem);
            }
            return false;
        }

        @Override // androidx.appcompat.view.menu.e.a
        public void b(androidx.appcompat.view.menu.e eVar) {
            if (this.i0 == null) {
                return;
            }
            k();
            e.this.f.l();
        }

        @Override // defpackage.k6
        public void c() {
            e eVar = e.this;
            if (eVar.j != this) {
                return;
            }
            if (!e.B(eVar.r, eVar.s, false)) {
                e eVar2 = e.this;
                eVar2.k = this;
                eVar2.l = this.i0;
            } else {
                this.i0.b(this);
            }
            this.i0 = null;
            e.this.A(false);
            e.this.f.g();
            e.this.e.o().sendAccessibilityEvent(32);
            e eVar3 = e.this;
            eVar3.c.setHideOnContentScrollEnabled(eVar3.x);
            e.this.j = null;
        }

        @Override // defpackage.k6
        public View d() {
            WeakReference<View> weakReference = this.j0;
            if (weakReference != null) {
                return weakReference.get();
            }
            return null;
        }

        @Override // defpackage.k6
        public Menu e() {
            return this.h0;
        }

        @Override // defpackage.k6
        public MenuInflater f() {
            return new kw3(this.g0);
        }

        @Override // defpackage.k6
        public CharSequence g() {
            return e.this.f.getSubtitle();
        }

        @Override // defpackage.k6
        public CharSequence i() {
            return e.this.f.getTitle();
        }

        @Override // defpackage.k6
        public void k() {
            if (e.this.j != this) {
                return;
            }
            this.h0.h0();
            try {
                this.i0.d(this, this.h0);
            } finally {
                this.h0.g0();
            }
        }

        @Override // defpackage.k6
        public boolean l() {
            return e.this.f.j();
        }

        @Override // defpackage.k6
        public void m(View view) {
            e.this.f.setCustomView(view);
            this.j0 = new WeakReference<>(view);
        }

        @Override // defpackage.k6
        public void n(int i) {
            o(e.this.a.getResources().getString(i));
        }

        @Override // defpackage.k6
        public void o(CharSequence charSequence) {
            e.this.f.setSubtitle(charSequence);
        }

        @Override // defpackage.k6
        public void q(int i) {
            r(e.this.a.getResources().getString(i));
        }

        @Override // defpackage.k6
        public void r(CharSequence charSequence) {
            e.this.f.setTitle(charSequence);
        }

        @Override // defpackage.k6
        public void s(boolean z) {
            super.s(z);
            e.this.f.setTitleOptional(z);
        }

        public boolean t() {
            this.h0.h0();
            try {
                return this.i0.c(this, this.h0);
            } finally {
                this.h0.g0();
            }
        }
    }

    public e(Activity activity, boolean z) {
        new ArrayList();
        this.n = new ArrayList<>();
        this.p = 0;
        this.q = true;
        this.u = true;
        this.y = new a();
        this.z = new b();
        this.A = new c();
        View decorView = activity.getWindow().getDecorView();
        I(decorView);
        if (z) {
            return;
        }
        this.g = decorView.findViewById(16908290);
    }

    public static boolean B(boolean z, boolean z2, boolean z3) {
        if (z3) {
            return true;
        }
        return (z || z2) ? false : true;
    }

    public void A(boolean z) {
        vj4 m;
        vj4 f;
        if (z) {
            P();
        } else {
            H();
        }
        if (!O()) {
            if (z) {
                this.e.setVisibility(4);
                this.f.setVisibility(0);
                return;
            }
            this.e.setVisibility(0);
            this.f.setVisibility(8);
            return;
        }
        if (z) {
            f = this.e.m(4, 100L);
            m = this.f.f(0, 200L);
        } else {
            m = this.e.m(0, 200L);
            f = this.f.f(8, 100L);
        }
        wj4 wj4Var = new wj4();
        wj4Var.d(f, m);
        wj4Var.h();
    }

    public void C() {
        k6.a aVar = this.l;
        if (aVar != null) {
            aVar.b(this.k);
            this.k = null;
            this.l = null;
        }
    }

    public void D(boolean z) {
        View view;
        int[] iArr;
        wj4 wj4Var = this.v;
        if (wj4Var != null) {
            wj4Var.a();
        }
        if (this.p == 0 && (this.w || z)) {
            this.d.setAlpha(1.0f);
            this.d.setTransitioning(true);
            wj4 wj4Var2 = new wj4();
            float f = -this.d.getHeight();
            if (z) {
                this.d.getLocationInWindow(new int[]{0, 0});
                f -= iArr[1];
            }
            vj4 k = ei4.e(this.d).k(f);
            k.i(this.A);
            wj4Var2.c(k);
            if (this.q && (view = this.g) != null) {
                wj4Var2.c(ei4.e(view).k(f));
            }
            wj4Var2.f(B);
            wj4Var2.e(250L);
            wj4Var2.g(this.y);
            this.v = wj4Var2;
            wj4Var2.h();
            return;
        }
        this.y.b(null);
    }

    public void E(boolean z) {
        View view;
        View view2;
        int[] iArr;
        wj4 wj4Var = this.v;
        if (wj4Var != null) {
            wj4Var.a();
        }
        this.d.setVisibility(0);
        if (this.p == 0 && (this.w || z)) {
            this.d.setTranslationY(Utils.FLOAT_EPSILON);
            float f = -this.d.getHeight();
            if (z) {
                this.d.getLocationInWindow(new int[]{0, 0});
                f -= iArr[1];
            }
            this.d.setTranslationY(f);
            wj4 wj4Var2 = new wj4();
            vj4 k = ei4.e(this.d).k(Utils.FLOAT_EPSILON);
            k.i(this.A);
            wj4Var2.c(k);
            if (this.q && (view2 = this.g) != null) {
                view2.setTranslationY(f);
                wj4Var2.c(ei4.e(this.g).k(Utils.FLOAT_EPSILON));
            }
            wj4Var2.f(C);
            wj4Var2.e(250L);
            wj4Var2.g(this.z);
            this.v = wj4Var2;
            wj4Var2.h();
        } else {
            this.d.setAlpha(1.0f);
            this.d.setTranslationY(Utils.FLOAT_EPSILON);
            if (this.q && (view = this.g) != null) {
                view.setTranslationY(Utils.FLOAT_EPSILON);
            }
            this.z.b(null);
        }
        ActionBarOverlayLayout actionBarOverlayLayout = this.c;
        if (actionBarOverlayLayout != null) {
            ei4.q0(actionBarOverlayLayout);
        }
    }

    public final lf0 F(View view) {
        if (view instanceof lf0) {
            return (lf0) view;
        }
        if (view instanceof Toolbar) {
            return ((Toolbar) view).getWrapper();
        }
        StringBuilder sb = new StringBuilder();
        sb.append("Can't make a decor toolbar out of ");
        sb.append(view != null ? view.getClass().getSimpleName() : "null");
        throw new IllegalStateException(sb.toString());
    }

    public int G() {
        return this.e.l();
    }

    public final void H() {
        if (this.t) {
            this.t = false;
            ActionBarOverlayLayout actionBarOverlayLayout = this.c;
            if (actionBarOverlayLayout != null) {
                actionBarOverlayLayout.setShowingForActionMode(false);
            }
            Q(false);
        }
    }

    public final void I(View view) {
        ActionBarOverlayLayout actionBarOverlayLayout = (ActionBarOverlayLayout) view.findViewById(q03.decor_content_parent);
        this.c = actionBarOverlayLayout;
        if (actionBarOverlayLayout != null) {
            actionBarOverlayLayout.setActionBarVisibilityCallback(this);
        }
        this.e = F(view.findViewById(q03.action_bar));
        this.f = (ActionBarContextView) view.findViewById(q03.action_context_bar);
        ActionBarContainer actionBarContainer = (ActionBarContainer) view.findViewById(q03.action_bar_container);
        this.d = actionBarContainer;
        lf0 lf0Var = this.e;
        if (lf0Var != null && this.f != null && actionBarContainer != null) {
            this.a = lf0Var.getContext();
            boolean z = (this.e.q() & 4) != 0;
            if (z) {
                this.i = true;
            }
            i6 b2 = i6.b(this.a);
            N(b2.a() || z);
            L(b2.g());
            TypedArray obtainStyledAttributes = this.a.obtainStyledAttributes(null, d33.ActionBar, jy2.actionBarStyle, 0);
            if (obtainStyledAttributes.getBoolean(d33.ActionBar_hideOnContentScroll, false)) {
                M(true);
            }
            int dimensionPixelSize = obtainStyledAttributes.getDimensionPixelSize(d33.ActionBar_elevation, 0);
            if (dimensionPixelSize != 0) {
                K(dimensionPixelSize);
            }
            obtainStyledAttributes.recycle();
            return;
        }
        throw new IllegalStateException(e.class.getSimpleName() + " can only be used with a compatible window decor layout");
    }

    public void J(int i, int i2) {
        int q = this.e.q();
        if ((i2 & 4) != 0) {
            this.i = true;
        }
        this.e.i((i & i2) | ((~i2) & q));
    }

    public void K(float f) {
        ei4.A0(this.d, f);
    }

    public final void L(boolean z) {
        this.o = z;
        if (!z) {
            this.e.g(null);
            this.d.setTabContainer(this.h);
        } else {
            this.d.setTabContainer(null);
            this.e.g(this.h);
        }
        boolean z2 = true;
        boolean z3 = G() == 2;
        ScrollingTabContainerView scrollingTabContainerView = this.h;
        if (scrollingTabContainerView != null) {
            if (z3) {
                scrollingTabContainerView.setVisibility(0);
                ActionBarOverlayLayout actionBarOverlayLayout = this.c;
                if (actionBarOverlayLayout != null) {
                    ei4.q0(actionBarOverlayLayout);
                }
            } else {
                scrollingTabContainerView.setVisibility(8);
            }
        }
        this.e.v(!this.o && z3);
        ActionBarOverlayLayout actionBarOverlayLayout2 = this.c;
        if (this.o || !z3) {
            z2 = false;
        }
        actionBarOverlayLayout2.setHasNonEmbeddedTabs(z2);
    }

    public void M(boolean z) {
        if (z && !this.c.o()) {
            throw new IllegalStateException("Action bar must be in overlay mode (Window.FEATURE_OVERLAY_ACTION_BAR) to enable hide on content scroll");
        }
        this.x = z;
        this.c.setHideOnContentScrollEnabled(z);
    }

    public void N(boolean z) {
        this.e.p(z);
    }

    public final boolean O() {
        return ei4.W(this.d);
    }

    public final void P() {
        if (this.t) {
            return;
        }
        this.t = true;
        ActionBarOverlayLayout actionBarOverlayLayout = this.c;
        if (actionBarOverlayLayout != null) {
            actionBarOverlayLayout.setShowingForActionMode(true);
        }
        Q(false);
    }

    public final void Q(boolean z) {
        if (B(this.r, this.s, this.t)) {
            if (this.u) {
                return;
            }
            this.u = true;
            E(z);
        } else if (this.u) {
            this.u = false;
            D(z);
        }
    }

    @Override // androidx.appcompat.widget.ActionBarOverlayLayout.d
    public void a() {
        if (this.s) {
            this.s = false;
            Q(true);
        }
    }

    @Override // androidx.appcompat.widget.ActionBarOverlayLayout.d
    public void b() {
    }

    @Override // androidx.appcompat.widget.ActionBarOverlayLayout.d
    public void c(boolean z) {
        this.q = z;
    }

    @Override // androidx.appcompat.widget.ActionBarOverlayLayout.d
    public void d() {
        if (this.s) {
            return;
        }
        this.s = true;
        Q(true);
    }

    @Override // androidx.appcompat.widget.ActionBarOverlayLayout.d
    public void e() {
        wj4 wj4Var = this.v;
        if (wj4Var != null) {
            wj4Var.a();
            this.v = null;
        }
    }

    @Override // androidx.appcompat.widget.ActionBarOverlayLayout.d
    public void f(int i) {
        this.p = i;
    }

    @Override // androidx.appcompat.app.ActionBar
    public boolean h() {
        lf0 lf0Var = this.e;
        if (lf0Var == null || !lf0Var.h()) {
            return false;
        }
        this.e.collapseActionView();
        return true;
    }

    @Override // androidx.appcompat.app.ActionBar
    public void i(boolean z) {
        if (z == this.m) {
            return;
        }
        this.m = z;
        int size = this.n.size();
        for (int i = 0; i < size; i++) {
            this.n.get(i).a(z);
        }
    }

    @Override // androidx.appcompat.app.ActionBar
    public int j() {
        return this.e.q();
    }

    @Override // androidx.appcompat.app.ActionBar
    public Context k() {
        if (this.b == null) {
            TypedValue typedValue = new TypedValue();
            this.a.getTheme().resolveAttribute(jy2.actionBarWidgetTheme, typedValue, true);
            int i = typedValue.resourceId;
            if (i != 0) {
                this.b = new ContextThemeWrapper(this.a, i);
            } else {
                this.b = this.a;
            }
        }
        return this.b;
    }

    @Override // androidx.appcompat.app.ActionBar
    public void l() {
        if (this.r) {
            return;
        }
        this.r = true;
        Q(false);
    }

    @Override // androidx.appcompat.app.ActionBar
    public void n(Configuration configuration) {
        L(i6.b(this.a).g());
    }

    @Override // androidx.appcompat.app.ActionBar
    public boolean p(int i, KeyEvent keyEvent) {
        Menu e;
        d dVar = this.j;
        if (dVar == null || (e = dVar.e()) == null) {
            return false;
        }
        e.setQwertyMode(KeyCharacterMap.load(keyEvent != null ? keyEvent.getDeviceId() : -1).getKeyboardType() != 1);
        return e.performShortcut(i, keyEvent, 0);
    }

    @Override // androidx.appcompat.app.ActionBar
    public void s(boolean z) {
        if (this.i) {
            return;
        }
        t(z);
    }

    @Override // androidx.appcompat.app.ActionBar
    public void t(boolean z) {
        J(z ? 4 : 0, 4);
    }

    @Override // androidx.appcompat.app.ActionBar
    public void u(int i) {
        this.e.r(i);
    }

    @Override // androidx.appcompat.app.ActionBar
    public void v(Drawable drawable) {
        this.e.u(drawable);
    }

    @Override // androidx.appcompat.app.ActionBar
    public void w(boolean z) {
        wj4 wj4Var;
        this.w = z;
        if (z || (wj4Var = this.v) == null) {
            return;
        }
        wj4Var.a();
    }

    @Override // androidx.appcompat.app.ActionBar
    public void x(CharSequence charSequence) {
        this.e.setTitle(charSequence);
    }

    @Override // androidx.appcompat.app.ActionBar
    public void y(CharSequence charSequence) {
        this.e.setWindowTitle(charSequence);
    }

    @Override // androidx.appcompat.app.ActionBar
    public k6 z(k6.a aVar) {
        d dVar = this.j;
        if (dVar != null) {
            dVar.c();
        }
        this.c.setHideOnContentScrollEnabled(false);
        this.f.k();
        d dVar2 = new d(this.f.getContext(), aVar);
        if (dVar2.t()) {
            this.j = dVar2;
            dVar2.k();
            this.f.h(dVar2);
            A(true);
            this.f.sendAccessibilityEvent(32);
            return dVar2;
        }
        return null;
    }

    public e(Dialog dialog) {
        new ArrayList();
        this.n = new ArrayList<>();
        this.p = 0;
        this.q = true;
        this.u = true;
        this.y = new a();
        this.z = new b();
        this.A = new c();
        I(dialog.getWindow().getDecorView());
    }
}
