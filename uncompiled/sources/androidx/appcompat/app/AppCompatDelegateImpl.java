package androidx.appcompat.app;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.Dialog;
import android.app.UiModeManager;
import android.content.BroadcastReceiver;
import android.content.ComponentName;
import android.content.Context;
import android.content.ContextWrapper;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.ActivityInfo;
import android.content.pm.PackageManager;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.content.res.TypedArray;
import android.graphics.Rect;
import android.graphics.drawable.Drawable;
import android.media.AudioManager;
import android.os.Build;
import android.os.Bundle;
import android.os.LocaleList;
import android.os.Parcel;
import android.os.Parcelable;
import android.os.PowerManager;
import android.text.TextUtils;
import android.util.AndroidRuntimeException;
import android.util.AttributeSet;
import android.util.TypedValue;
import android.view.ActionMode;
import android.view.ContextThemeWrapper;
import android.view.KeyCharacterMap;
import android.view.KeyEvent;
import android.view.KeyboardShortcutGroup;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewConfiguration;
import android.view.ViewGroup;
import android.view.ViewParent;
import android.view.Window;
import android.view.WindowManager;
import android.widget.FrameLayout;
import android.widget.PopupWindow;
import android.widget.TextView;
import androidx.appcompat.view.menu.e;
import androidx.appcompat.view.menu.i;
import androidx.appcompat.widget.ActionBarContextView;
import androidx.appcompat.widget.ContentFrameLayout;
import androidx.appcompat.widget.Toolbar;
import androidx.appcompat.widget.c;
import androidx.lifecycle.Lifecycle;
import androidx.media3.common.PlaybackException;
import androidx.recyclerview.widget.RecyclerView;
import com.github.mikephil.charting.utils.Utils;
import defpackage.g83;
import defpackage.hw3;
import defpackage.ix1;
import defpackage.k6;
import java.lang.Thread;
import java.util.List;

/* loaded from: classes.dex */
public class AppCompatDelegateImpl extends androidx.appcompat.app.b implements e.a, LayoutInflater.Factory2 {
    public static final vo3<String, Integer> f1 = new vo3<>();
    public static final boolean g1;
    public static final int[] h1;
    public static final boolean i1;
    public static final boolean j1;
    public static boolean k1;
    public TextView A0;
    public View B0;
    public boolean C0;
    public boolean D0;
    public boolean E0;
    public boolean F0;
    public boolean G0;
    public boolean H0;
    public boolean I0;
    public boolean J0;
    public PanelFeatureState[] K0;
    public PanelFeatureState L0;
    public boolean M0;
    public boolean N0;
    public boolean O0;
    public boolean P0;
    public boolean Q0;
    public int R0;
    public int S0;
    public boolean T0;
    public boolean U0;
    public q V0;
    public q W0;
    public boolean X0;
    public int Y0;
    public final Runnable Z0;
    public boolean a1;
    public Rect b1;
    public Rect c1;
    public androidx.appcompat.app.c d1;
    public oy1 e1;
    public final Object h0;
    public final Context i0;
    public Window j0;
    public o k0;
    public final cf l0;
    public ActionBar m0;
    public MenuInflater n0;
    public CharSequence o0;
    public kf0 p0;
    public i q0;
    public u r0;
    public k6 s0;
    public ActionBarContextView t0;
    public PopupWindow u0;
    public Runnable v0;
    public vj4 w0;
    public boolean x0;
    public boolean y0;
    public ViewGroup z0;

    /* loaded from: classes.dex */
    public static final class PanelFeatureState {
        public int a;
        public int b;
        public int c;
        public int d;
        public int e;
        public int f;
        public ViewGroup g;
        public View h;
        public View i;
        public androidx.appcompat.view.menu.e j;
        public androidx.appcompat.view.menu.c k;
        public Context l;
        public boolean m;
        public boolean n;
        public boolean o;
        public boolean p;
        public boolean q = false;
        public boolean r;
        public Bundle s;

        @SuppressLint({"BanParcelableUsage"})
        /* loaded from: classes.dex */
        public static class SavedState implements Parcelable {
            public static final Parcelable.Creator<SavedState> CREATOR = new a();
            public int a;
            public boolean f0;
            public Bundle g0;

            /* loaded from: classes.dex */
            public class a implements Parcelable.ClassLoaderCreator<SavedState> {
                @Override // android.os.Parcelable.Creator
                /* renamed from: a */
                public SavedState createFromParcel(Parcel parcel) {
                    return SavedState.a(parcel, null);
                }

                @Override // android.os.Parcelable.ClassLoaderCreator
                /* renamed from: b */
                public SavedState createFromParcel(Parcel parcel, ClassLoader classLoader) {
                    return SavedState.a(parcel, classLoader);
                }

                @Override // android.os.Parcelable.Creator
                /* renamed from: c */
                public SavedState[] newArray(int i) {
                    return new SavedState[i];
                }
            }

            public static SavedState a(Parcel parcel, ClassLoader classLoader) {
                SavedState savedState = new SavedState();
                savedState.a = parcel.readInt();
                boolean z = parcel.readInt() == 1;
                savedState.f0 = z;
                if (z) {
                    savedState.g0 = parcel.readBundle(classLoader);
                }
                return savedState;
            }

            @Override // android.os.Parcelable
            public int describeContents() {
                return 0;
            }

            @Override // android.os.Parcelable
            public void writeToParcel(Parcel parcel, int i) {
                parcel.writeInt(this.a);
                parcel.writeInt(this.f0 ? 1 : 0);
                if (this.f0) {
                    parcel.writeBundle(this.g0);
                }
            }
        }

        public PanelFeatureState(int i) {
            this.a = i;
        }

        public androidx.appcompat.view.menu.j a(i.a aVar) {
            if (this.j == null) {
                return null;
            }
            if (this.k == null) {
                androidx.appcompat.view.menu.c cVar = new androidx.appcompat.view.menu.c(this.l, d13.abc_list_menu_item_layout);
                this.k = cVar;
                cVar.h(aVar);
                this.j.b(this.k);
            }
            return this.k.b(this.g);
        }

        public boolean b() {
            if (this.h == null) {
                return false;
            }
            return this.i != null || this.k.a().getCount() > 0;
        }

        public void c(androidx.appcompat.view.menu.e eVar) {
            androidx.appcompat.view.menu.c cVar;
            androidx.appcompat.view.menu.e eVar2 = this.j;
            if (eVar == eVar2) {
                return;
            }
            if (eVar2 != null) {
                eVar2.Q(this.k);
            }
            this.j = eVar;
            if (eVar == null || (cVar = this.k) == null) {
                return;
            }
            eVar.b(cVar);
        }

        public void d(Context context) {
            TypedValue typedValue = new TypedValue();
            Resources.Theme newTheme = context.getResources().newTheme();
            newTheme.setTo(context.getTheme());
            newTheme.resolveAttribute(jy2.actionBarPopupTheme, typedValue, true);
            int i = typedValue.resourceId;
            if (i != 0) {
                newTheme.applyStyle(i, true);
            }
            newTheme.resolveAttribute(jy2.panelMenuListTheme, typedValue, true);
            int i2 = typedValue.resourceId;
            if (i2 != 0) {
                newTheme.applyStyle(i2, true);
            } else {
                newTheme.applyStyle(b23.Theme_AppCompat_CompactMenu, true);
            }
            o70 o70Var = new o70(context, 0);
            o70Var.getTheme().setTo(newTheme);
            this.l = o70Var;
            TypedArray obtainStyledAttributes = o70Var.obtainStyledAttributes(d33.AppCompatTheme);
            this.b = obtainStyledAttributes.getResourceId(d33.AppCompatTheme_panelBackground, 0);
            this.f = obtainStyledAttributes.getResourceId(d33.AppCompatTheme_android_windowAnimationStyle, 0);
            obtainStyledAttributes.recycle();
        }
    }

    /* loaded from: classes.dex */
    public class a implements Thread.UncaughtExceptionHandler {
        public final /* synthetic */ Thread.UncaughtExceptionHandler a;

        public a(Thread.UncaughtExceptionHandler uncaughtExceptionHandler) {
            this.a = uncaughtExceptionHandler;
        }

        public final boolean a(Throwable th) {
            String message;
            if (!(th instanceof Resources.NotFoundException) || (message = th.getMessage()) == null) {
                return false;
            }
            return message.contains("drawable") || message.contains("Drawable");
        }

        @Override // java.lang.Thread.UncaughtExceptionHandler
        public void uncaughtException(Thread thread, Throwable th) {
            if (a(th)) {
                Resources.NotFoundException notFoundException = new Resources.NotFoundException(th.getMessage() + ". If the resource you are trying to use is a vector resource, you may be referencing it in an unsupported way. See AppCompatDelegate.setCompatVectorFromResourcesEnabled() for more info.");
                notFoundException.initCause(th.getCause());
                notFoundException.setStackTrace(th.getStackTrace());
                this.a.uncaughtException(thread, notFoundException);
                return;
            }
            this.a.uncaughtException(thread, th);
        }
    }

    /* loaded from: classes.dex */
    public class b implements Runnable {
        public b() {
        }

        @Override // java.lang.Runnable
        public void run() {
            AppCompatDelegateImpl appCompatDelegateImpl = AppCompatDelegateImpl.this;
            if ((appCompatDelegateImpl.Y0 & 1) != 0) {
                appCompatDelegateImpl.Z(0);
            }
            AppCompatDelegateImpl appCompatDelegateImpl2 = AppCompatDelegateImpl.this;
            if ((appCompatDelegateImpl2.Y0 & 4096) != 0) {
                appCompatDelegateImpl2.Z(108);
            }
            AppCompatDelegateImpl appCompatDelegateImpl3 = AppCompatDelegateImpl.this;
            appCompatDelegateImpl3.X0 = false;
            appCompatDelegateImpl3.Y0 = 0;
        }
    }

    /* loaded from: classes.dex */
    public class c implements em2 {
        public c() {
        }

        @Override // defpackage.em2
        public jp4 a(View view, jp4 jp4Var) {
            int m = jp4Var.m();
            int P0 = AppCompatDelegateImpl.this.P0(jp4Var, null);
            if (m != P0) {
                jp4Var = jp4Var.r(jp4Var.k(), P0, jp4Var.l(), jp4Var.j());
            }
            return ei4.e0(view, jp4Var);
        }
    }

    /* loaded from: classes.dex */
    public class d implements c.a {
        public d() {
        }

        @Override // androidx.appcompat.widget.c.a
        public void a(Rect rect) {
            rect.top = AppCompatDelegateImpl.this.P0(null, rect);
        }
    }

    /* loaded from: classes.dex */
    public class e implements ContentFrameLayout.a {
        public e() {
        }

        @Override // androidx.appcompat.widget.ContentFrameLayout.a
        public void a() {
        }

        @Override // androidx.appcompat.widget.ContentFrameLayout.a
        public void onDetachedFromWindow() {
            AppCompatDelegateImpl.this.X();
        }
    }

    /* loaded from: classes.dex */
    public class f implements Runnable {

        /* loaded from: classes.dex */
        public class a extends yj4 {
            public a() {
            }

            @Override // defpackage.xj4
            public void b(View view) {
                AppCompatDelegateImpl.this.t0.setAlpha(1.0f);
                AppCompatDelegateImpl.this.w0.f(null);
                AppCompatDelegateImpl.this.w0 = null;
            }

            @Override // defpackage.yj4, defpackage.xj4
            public void c(View view) {
                AppCompatDelegateImpl.this.t0.setVisibility(0);
            }
        }

        public f() {
        }

        @Override // java.lang.Runnable
        public void run() {
            AppCompatDelegateImpl appCompatDelegateImpl = AppCompatDelegateImpl.this;
            appCompatDelegateImpl.u0.showAtLocation(appCompatDelegateImpl.t0, 55, 0, 0);
            AppCompatDelegateImpl.this.a0();
            if (AppCompatDelegateImpl.this.I0()) {
                AppCompatDelegateImpl.this.t0.setAlpha(Utils.FLOAT_EPSILON);
                AppCompatDelegateImpl appCompatDelegateImpl2 = AppCompatDelegateImpl.this;
                appCompatDelegateImpl2.w0 = ei4.e(appCompatDelegateImpl2.t0).a(1.0f);
                AppCompatDelegateImpl.this.w0.f(new a());
                return;
            }
            AppCompatDelegateImpl.this.t0.setAlpha(1.0f);
            AppCompatDelegateImpl.this.t0.setVisibility(0);
        }
    }

    /* loaded from: classes.dex */
    public class g extends yj4 {
        public g() {
        }

        @Override // defpackage.xj4
        public void b(View view) {
            AppCompatDelegateImpl.this.t0.setAlpha(1.0f);
            AppCompatDelegateImpl.this.w0.f(null);
            AppCompatDelegateImpl.this.w0 = null;
        }

        @Override // defpackage.yj4, defpackage.xj4
        public void c(View view) {
            AppCompatDelegateImpl.this.t0.setVisibility(0);
            AppCompatDelegateImpl.this.t0.sendAccessibilityEvent(32);
            if (AppCompatDelegateImpl.this.t0.getParent() instanceof View) {
                ei4.q0((View) AppCompatDelegateImpl.this.t0.getParent());
            }
        }
    }

    /* loaded from: classes.dex */
    public class h implements g6 {
        public h() {
        }

        @Override // defpackage.g6
        public Context a() {
            return AppCompatDelegateImpl.this.f0();
        }

        @Override // defpackage.g6
        public void b(Drawable drawable, int i) {
            ActionBar p = AppCompatDelegateImpl.this.p();
            if (p != null) {
                p.v(drawable);
                p.u(i);
            }
        }
    }

    /* loaded from: classes.dex */
    public final class i implements i.a {
        public i() {
        }

        @Override // androidx.appcompat.view.menu.i.a
        public void c(androidx.appcompat.view.menu.e eVar, boolean z) {
            AppCompatDelegateImpl.this.Q(eVar);
        }

        @Override // androidx.appcompat.view.menu.i.a
        public boolean d(androidx.appcompat.view.menu.e eVar) {
            Window.Callback k0 = AppCompatDelegateImpl.this.k0();
            if (k0 != null) {
                k0.onMenuOpened(108, eVar);
                return true;
            }
            return true;
        }
    }

    /* loaded from: classes.dex */
    public class j implements k6.a {
        public k6.a a;

        /* loaded from: classes.dex */
        public class a extends yj4 {
            public a() {
            }

            @Override // defpackage.xj4
            public void b(View view) {
                AppCompatDelegateImpl.this.t0.setVisibility(8);
                AppCompatDelegateImpl appCompatDelegateImpl = AppCompatDelegateImpl.this;
                PopupWindow popupWindow = appCompatDelegateImpl.u0;
                if (popupWindow != null) {
                    popupWindow.dismiss();
                } else if (appCompatDelegateImpl.t0.getParent() instanceof View) {
                    ei4.q0((View) AppCompatDelegateImpl.this.t0.getParent());
                }
                AppCompatDelegateImpl.this.t0.k();
                AppCompatDelegateImpl.this.w0.f(null);
                AppCompatDelegateImpl appCompatDelegateImpl2 = AppCompatDelegateImpl.this;
                appCompatDelegateImpl2.w0 = null;
                ei4.q0(appCompatDelegateImpl2.z0);
            }
        }

        public j(k6.a aVar) {
            this.a = aVar;
        }

        @Override // defpackage.k6.a
        public boolean a(k6 k6Var, MenuItem menuItem) {
            return this.a.a(k6Var, menuItem);
        }

        @Override // defpackage.k6.a
        public void b(k6 k6Var) {
            this.a.b(k6Var);
            AppCompatDelegateImpl appCompatDelegateImpl = AppCompatDelegateImpl.this;
            if (appCompatDelegateImpl.u0 != null) {
                appCompatDelegateImpl.j0.getDecorView().removeCallbacks(AppCompatDelegateImpl.this.v0);
            }
            AppCompatDelegateImpl appCompatDelegateImpl2 = AppCompatDelegateImpl.this;
            if (appCompatDelegateImpl2.t0 != null) {
                appCompatDelegateImpl2.a0();
                AppCompatDelegateImpl appCompatDelegateImpl3 = AppCompatDelegateImpl.this;
                appCompatDelegateImpl3.w0 = ei4.e(appCompatDelegateImpl3.t0).a(Utils.FLOAT_EPSILON);
                AppCompatDelegateImpl.this.w0.f(new a());
            }
            AppCompatDelegateImpl appCompatDelegateImpl4 = AppCompatDelegateImpl.this;
            cf cfVar = appCompatDelegateImpl4.l0;
            if (cfVar != null) {
                cfVar.onSupportActionModeFinished(appCompatDelegateImpl4.s0);
            }
            AppCompatDelegateImpl appCompatDelegateImpl5 = AppCompatDelegateImpl.this;
            appCompatDelegateImpl5.s0 = null;
            ei4.q0(appCompatDelegateImpl5.z0);
        }

        @Override // defpackage.k6.a
        public boolean c(k6 k6Var, Menu menu) {
            return this.a.c(k6Var, menu);
        }

        @Override // defpackage.k6.a
        public boolean d(k6 k6Var, Menu menu) {
            ei4.q0(AppCompatDelegateImpl.this.z0);
            return this.a.d(k6Var, menu);
        }
    }

    /* loaded from: classes.dex */
    public static class k {
        public static Context a(Context context, Configuration configuration) {
            return context.createConfigurationContext(configuration);
        }

        public static void b(Configuration configuration, Configuration configuration2, Configuration configuration3) {
            int i = configuration.densityDpi;
            int i2 = configuration2.densityDpi;
            if (i != i2) {
                configuration3.densityDpi = i2;
            }
        }
    }

    /* loaded from: classes.dex */
    public static class l {
        public static boolean a(PowerManager powerManager) {
            return powerManager.isPowerSaveMode();
        }
    }

    /* loaded from: classes.dex */
    public static class m {
        public static void a(Configuration configuration, Configuration configuration2, Configuration configuration3) {
            LocaleList locales = configuration.getLocales();
            LocaleList locales2 = configuration2.getLocales();
            if (locales.equals(locales2)) {
                return;
            }
            configuration3.setLocales(locales2);
            configuration3.locale = configuration2.locale;
        }
    }

    /* loaded from: classes.dex */
    public static class n {
        public static void a(Configuration configuration, Configuration configuration2, Configuration configuration3) {
            int i = configuration.colorMode & 3;
            int i2 = configuration2.colorMode;
            if (i != (i2 & 3)) {
                configuration3.colorMode |= i2 & 3;
            }
            int i3 = configuration.colorMode & 12;
            int i4 = configuration2.colorMode;
            if (i3 != (i4 & 12)) {
                configuration3.colorMode |= i4 & 12;
            }
        }
    }

    /* loaded from: classes.dex */
    public class p extends q {
        public final PowerManager c;

        public p(Context context) {
            super();
            this.c = (PowerManager) context.getApplicationContext().getSystemService("power");
        }

        @Override // androidx.appcompat.app.AppCompatDelegateImpl.q
        public IntentFilter b() {
            if (Build.VERSION.SDK_INT >= 21) {
                IntentFilter intentFilter = new IntentFilter();
                intentFilter.addAction("android.os.action.POWER_SAVE_MODE_CHANGED");
                return intentFilter;
            }
            return null;
        }

        @Override // androidx.appcompat.app.AppCompatDelegateImpl.q
        public int c() {
            return (Build.VERSION.SDK_INT < 21 || !l.a(this.c)) ? 1 : 2;
        }

        @Override // androidx.appcompat.app.AppCompatDelegateImpl.q
        public void d() {
            AppCompatDelegateImpl.this.e();
        }
    }

    /* loaded from: classes.dex */
    public abstract class q {
        public BroadcastReceiver a;

        /* loaded from: classes.dex */
        public class a extends BroadcastReceiver {
            public a() {
            }

            @Override // android.content.BroadcastReceiver
            public void onReceive(Context context, Intent intent) {
                q.this.d();
            }
        }

        public q() {
        }

        public void a() {
            BroadcastReceiver broadcastReceiver = this.a;
            if (broadcastReceiver != null) {
                try {
                    AppCompatDelegateImpl.this.i0.unregisterReceiver(broadcastReceiver);
                } catch (IllegalArgumentException unused) {
                }
                this.a = null;
            }
        }

        public abstract IntentFilter b();

        public abstract int c();

        public abstract void d();

        public void e() {
            a();
            IntentFilter b = b();
            if (b == null || b.countActions() == 0) {
                return;
            }
            if (this.a == null) {
                this.a = new a();
            }
            AppCompatDelegateImpl.this.i0.registerReceiver(this.a, b);
        }
    }

    /* loaded from: classes.dex */
    public class r extends q {
        public final sc4 c;

        public r(sc4 sc4Var) {
            super();
            this.c = sc4Var;
        }

        @Override // androidx.appcompat.app.AppCompatDelegateImpl.q
        public IntentFilter b() {
            IntentFilter intentFilter = new IntentFilter();
            intentFilter.addAction("android.intent.action.TIME_SET");
            intentFilter.addAction("android.intent.action.TIMEZONE_CHANGED");
            intentFilter.addAction("android.intent.action.TIME_TICK");
            return intentFilter;
        }

        @Override // androidx.appcompat.app.AppCompatDelegateImpl.q
        public int c() {
            return this.c.d() ? 2 : 1;
        }

        @Override // androidx.appcompat.app.AppCompatDelegateImpl.q
        public void d() {
            AppCompatDelegateImpl.this.e();
        }
    }

    /* loaded from: classes.dex */
    public static class s {
        public static void a(ContextThemeWrapper contextThemeWrapper, Configuration configuration) {
            contextThemeWrapper.applyOverrideConfiguration(configuration);
        }
    }

    /* loaded from: classes.dex */
    public class t extends ContentFrameLayout {
        public t(Context context) {
            super(context);
        }

        public final boolean b(int i, int i2) {
            return i < -5 || i2 < -5 || i > getWidth() + 5 || i2 > getHeight() + 5;
        }

        @Override // android.view.ViewGroup, android.view.View
        public boolean dispatchKeyEvent(KeyEvent keyEvent) {
            return AppCompatDelegateImpl.this.Y(keyEvent) || super.dispatchKeyEvent(keyEvent);
        }

        @Override // android.view.ViewGroup
        public boolean onInterceptTouchEvent(MotionEvent motionEvent) {
            if (motionEvent.getAction() == 0 && b((int) motionEvent.getX(), (int) motionEvent.getY())) {
                AppCompatDelegateImpl.this.S(0);
                return true;
            }
            return super.onInterceptTouchEvent(motionEvent);
        }

        @Override // android.view.View
        public void setBackgroundResource(int i) {
            setBackgroundDrawable(mf.d(getContext(), i));
        }
    }

    /* loaded from: classes.dex */
    public final class u implements i.a {
        public u() {
        }

        @Override // androidx.appcompat.view.menu.i.a
        public void c(androidx.appcompat.view.menu.e eVar, boolean z) {
            androidx.appcompat.view.menu.e F = eVar.F();
            boolean z2 = F != eVar;
            AppCompatDelegateImpl appCompatDelegateImpl = AppCompatDelegateImpl.this;
            if (z2) {
                eVar = F;
            }
            PanelFeatureState d0 = appCompatDelegateImpl.d0(eVar);
            if (d0 != null) {
                if (z2) {
                    AppCompatDelegateImpl.this.P(d0.a, d0, F);
                    AppCompatDelegateImpl.this.T(d0, true);
                    return;
                }
                AppCompatDelegateImpl.this.T(d0, z);
            }
        }

        @Override // androidx.appcompat.view.menu.i.a
        public boolean d(androidx.appcompat.view.menu.e eVar) {
            Window.Callback k0;
            if (eVar == eVar.F()) {
                AppCompatDelegateImpl appCompatDelegateImpl = AppCompatDelegateImpl.this;
                if (!appCompatDelegateImpl.E0 || (k0 = appCompatDelegateImpl.k0()) == null || AppCompatDelegateImpl.this.Q0) {
                    return true;
                }
                k0.onMenuOpened(108, eVar);
                return true;
            }
            return true;
        }
    }

    static {
        int i2 = Build.VERSION.SDK_INT;
        boolean z = i2 < 21;
        g1 = z;
        h1 = new int[]{16842836};
        i1 = !"robolectric".equals(Build.FINGERPRINT);
        j1 = i2 >= 17;
        if (!z || k1) {
            return;
        }
        Thread.setDefaultUncaughtExceptionHandler(new a(Thread.getDefaultUncaughtExceptionHandler()));
        k1 = true;
    }

    public AppCompatDelegateImpl(Activity activity, cf cfVar) {
        this(activity, null, cfVar, activity);
    }

    public static Configuration e0(Configuration configuration, Configuration configuration2) {
        Configuration configuration3 = new Configuration();
        configuration3.fontScale = Utils.FLOAT_EPSILON;
        if (configuration2 != null && configuration.diff(configuration2) != 0) {
            float f2 = configuration.fontScale;
            float f3 = configuration2.fontScale;
            if (f2 != f3) {
                configuration3.fontScale = f3;
            }
            int i2 = configuration.mcc;
            int i3 = configuration2.mcc;
            if (i2 != i3) {
                configuration3.mcc = i3;
            }
            int i4 = configuration.mnc;
            int i5 = configuration2.mnc;
            if (i4 != i5) {
                configuration3.mnc = i5;
            }
            int i6 = Build.VERSION.SDK_INT;
            if (i6 >= 24) {
                m.a(configuration, configuration2, configuration3);
            } else if (!sl2.a(configuration.locale, configuration2.locale)) {
                configuration3.locale = configuration2.locale;
            }
            int i7 = configuration.touchscreen;
            int i8 = configuration2.touchscreen;
            if (i7 != i8) {
                configuration3.touchscreen = i8;
            }
            int i9 = configuration.keyboard;
            int i10 = configuration2.keyboard;
            if (i9 != i10) {
                configuration3.keyboard = i10;
            }
            int i11 = configuration.keyboardHidden;
            int i12 = configuration2.keyboardHidden;
            if (i11 != i12) {
                configuration3.keyboardHidden = i12;
            }
            int i13 = configuration.navigation;
            int i14 = configuration2.navigation;
            if (i13 != i14) {
                configuration3.navigation = i14;
            }
            int i15 = configuration.navigationHidden;
            int i16 = configuration2.navigationHidden;
            if (i15 != i16) {
                configuration3.navigationHidden = i16;
            }
            int i17 = configuration.orientation;
            int i18 = configuration2.orientation;
            if (i17 != i18) {
                configuration3.orientation = i18;
            }
            int i19 = configuration.screenLayout & 15;
            int i20 = configuration2.screenLayout;
            if (i19 != (i20 & 15)) {
                configuration3.screenLayout |= i20 & 15;
            }
            int i21 = configuration.screenLayout & 192;
            int i22 = configuration2.screenLayout;
            if (i21 != (i22 & 192)) {
                configuration3.screenLayout |= i22 & 192;
            }
            int i23 = configuration.screenLayout & 48;
            int i24 = configuration2.screenLayout;
            if (i23 != (i24 & 48)) {
                configuration3.screenLayout |= i24 & 48;
            }
            int i25 = configuration.screenLayout & 768;
            int i26 = configuration2.screenLayout;
            if (i25 != (i26 & 768)) {
                configuration3.screenLayout |= i26 & 768;
            }
            if (i6 >= 26) {
                n.a(configuration, configuration2, configuration3);
            }
            int i27 = configuration.uiMode & 15;
            int i28 = configuration2.uiMode;
            if (i27 != (i28 & 15)) {
                configuration3.uiMode |= i28 & 15;
            }
            int i29 = configuration.uiMode & 48;
            int i30 = configuration2.uiMode;
            if (i29 != (i30 & 48)) {
                configuration3.uiMode |= i30 & 48;
            }
            int i31 = configuration.screenWidthDp;
            int i32 = configuration2.screenWidthDp;
            if (i31 != i32) {
                configuration3.screenWidthDp = i32;
            }
            int i33 = configuration.screenHeightDp;
            int i34 = configuration2.screenHeightDp;
            if (i33 != i34) {
                configuration3.screenHeightDp = i34;
            }
            int i35 = configuration.smallestScreenWidthDp;
            int i36 = configuration2.smallestScreenWidthDp;
            if (i35 != i36) {
                configuration3.smallestScreenWidthDp = i36;
            }
            if (i6 >= 17) {
                k.b(configuration, configuration2, configuration3);
            }
        }
        return configuration3;
    }

    public void A0(int i2) {
        if (i2 == 108) {
            ActionBar p2 = p();
            if (p2 != null) {
                p2.i(false);
            }
        } else if (i2 == 0) {
            PanelFeatureState i0 = i0(i2, true);
            if (i0.o) {
                T(i0, false);
            }
        }
    }

    public void B0(ViewGroup viewGroup) {
    }

    @Override // androidx.appcompat.app.b
    public boolean C(int i2) {
        int H0 = H0(i2);
        if (this.I0 && H0 == 108) {
            return false;
        }
        if (this.E0 && H0 == 1) {
            this.E0 = false;
        }
        if (H0 == 1) {
            L0();
            this.I0 = true;
            return true;
        } else if (H0 == 2) {
            L0();
            this.C0 = true;
            return true;
        } else if (H0 == 5) {
            L0();
            this.D0 = true;
            return true;
        } else if (H0 == 10) {
            L0();
            this.G0 = true;
            return true;
        } else if (H0 == 108) {
            L0();
            this.E0 = true;
            return true;
        } else if (H0 != 109) {
            return this.j0.requestFeature(H0);
        } else {
            L0();
            this.F0 = true;
            return true;
        }
    }

    public final void C0(PanelFeatureState panelFeatureState, KeyEvent keyEvent) {
        int i2;
        ViewGroup.LayoutParams layoutParams;
        if (panelFeatureState.o || this.Q0) {
            return;
        }
        if (panelFeatureState.a == 0) {
            if ((this.i0.getResources().getConfiguration().screenLayout & 15) == 4) {
                return;
            }
        }
        Window.Callback k0 = k0();
        if (k0 != null && !k0.onMenuOpened(panelFeatureState.a, panelFeatureState.j)) {
            T(panelFeatureState, true);
            return;
        }
        WindowManager windowManager = (WindowManager) this.i0.getSystemService("window");
        if (windowManager != null && F0(panelFeatureState, keyEvent)) {
            ViewGroup viewGroup = panelFeatureState.g;
            if (viewGroup != null && !panelFeatureState.q) {
                View view = panelFeatureState.i;
                if (view != null && (layoutParams = view.getLayoutParams()) != null && layoutParams.width == -1) {
                    i2 = -1;
                    panelFeatureState.n = false;
                    WindowManager.LayoutParams layoutParams2 = new WindowManager.LayoutParams(i2, -2, panelFeatureState.d, panelFeatureState.e, PlaybackException.ERROR_CODE_BEHIND_LIVE_WINDOW, 8519680, -3);
                    layoutParams2.gravity = panelFeatureState.c;
                    layoutParams2.windowAnimations = panelFeatureState.f;
                    windowManager.addView(panelFeatureState.g, layoutParams2);
                    panelFeatureState.o = true;
                }
            } else {
                if (viewGroup == null) {
                    if (!n0(panelFeatureState) || panelFeatureState.g == null) {
                        return;
                    }
                } else if (panelFeatureState.q && viewGroup.getChildCount() > 0) {
                    panelFeatureState.g.removeAllViews();
                }
                if (m0(panelFeatureState) && panelFeatureState.b()) {
                    ViewGroup.LayoutParams layoutParams3 = panelFeatureState.h.getLayoutParams();
                    if (layoutParams3 == null) {
                        layoutParams3 = new ViewGroup.LayoutParams(-2, -2);
                    }
                    panelFeatureState.g.setBackgroundResource(panelFeatureState.b);
                    ViewParent parent = panelFeatureState.h.getParent();
                    if (parent instanceof ViewGroup) {
                        ((ViewGroup) parent).removeView(panelFeatureState.h);
                    }
                    panelFeatureState.g.addView(panelFeatureState.h, layoutParams3);
                    if (!panelFeatureState.h.hasFocus()) {
                        panelFeatureState.h.requestFocus();
                    }
                } else {
                    panelFeatureState.q = true;
                    return;
                }
            }
            i2 = -2;
            panelFeatureState.n = false;
            WindowManager.LayoutParams layoutParams22 = new WindowManager.LayoutParams(i2, -2, panelFeatureState.d, panelFeatureState.e, PlaybackException.ERROR_CODE_BEHIND_LIVE_WINDOW, 8519680, -3);
            layoutParams22.gravity = panelFeatureState.c;
            layoutParams22.windowAnimations = panelFeatureState.f;
            windowManager.addView(panelFeatureState.g, layoutParams22);
            panelFeatureState.o = true;
        }
    }

    @Override // androidx.appcompat.app.b
    public void D(int i2) {
        b0();
        ViewGroup viewGroup = (ViewGroup) this.z0.findViewById(16908290);
        viewGroup.removeAllViews();
        LayoutInflater.from(this.i0).inflate(i2, viewGroup);
        this.k0.a().onContentChanged();
    }

    public final ActionBar D0() {
        return this.m0;
    }

    @Override // androidx.appcompat.app.b
    public void E(View view) {
        b0();
        ViewGroup viewGroup = (ViewGroup) this.z0.findViewById(16908290);
        viewGroup.removeAllViews();
        viewGroup.addView(view);
        this.k0.a().onContentChanged();
    }

    public final boolean E0(PanelFeatureState panelFeatureState, int i2, KeyEvent keyEvent, int i3) {
        androidx.appcompat.view.menu.e eVar;
        boolean z = false;
        if (keyEvent.isSystem()) {
            return false;
        }
        if ((panelFeatureState.m || F0(panelFeatureState, keyEvent)) && (eVar = panelFeatureState.j) != null) {
            z = eVar.performShortcut(i2, keyEvent, i3);
        }
        if (z && (i3 & 1) == 0 && this.p0 == null) {
            T(panelFeatureState, true);
        }
        return z;
    }

    @Override // androidx.appcompat.app.b
    public void F(View view, ViewGroup.LayoutParams layoutParams) {
        b0();
        ViewGroup viewGroup = (ViewGroup) this.z0.findViewById(16908290);
        viewGroup.removeAllViews();
        viewGroup.addView(view, layoutParams);
        this.k0.a().onContentChanged();
    }

    public final boolean F0(PanelFeatureState panelFeatureState, KeyEvent keyEvent) {
        kf0 kf0Var;
        kf0 kf0Var2;
        kf0 kf0Var3;
        if (this.Q0) {
            return false;
        }
        if (panelFeatureState.m) {
            return true;
        }
        PanelFeatureState panelFeatureState2 = this.L0;
        if (panelFeatureState2 != null && panelFeatureState2 != panelFeatureState) {
            T(panelFeatureState2, false);
        }
        Window.Callback k0 = k0();
        if (k0 != null) {
            panelFeatureState.i = k0.onCreatePanelView(panelFeatureState.a);
        }
        int i2 = panelFeatureState.a;
        boolean z = i2 == 0 || i2 == 108;
        if (z && (kf0Var3 = this.p0) != null) {
            kf0Var3.setMenuPrepared();
        }
        if (panelFeatureState.i == null && (!z || !(D0() instanceof androidx.appcompat.app.d))) {
            androidx.appcompat.view.menu.e eVar = panelFeatureState.j;
            if (eVar == null || panelFeatureState.r) {
                if (eVar == null && (!o0(panelFeatureState) || panelFeatureState.j == null)) {
                    return false;
                }
                if (z && this.p0 != null) {
                    if (this.q0 == null) {
                        this.q0 = new i();
                    }
                    this.p0.setMenu(panelFeatureState.j, this.q0);
                }
                panelFeatureState.j.h0();
                if (!k0.onCreatePanelMenu(panelFeatureState.a, panelFeatureState.j)) {
                    panelFeatureState.c(null);
                    if (z && (kf0Var = this.p0) != null) {
                        kf0Var.setMenu(null, this.q0);
                    }
                    return false;
                }
                panelFeatureState.r = false;
            }
            panelFeatureState.j.h0();
            Bundle bundle = panelFeatureState.s;
            if (bundle != null) {
                panelFeatureState.j.R(bundle);
                panelFeatureState.s = null;
            }
            if (!k0.onPreparePanel(0, panelFeatureState.i, panelFeatureState.j)) {
                if (z && (kf0Var2 = this.p0) != null) {
                    kf0Var2.setMenu(null, this.q0);
                }
                panelFeatureState.j.g0();
                return false;
            }
            boolean z2 = KeyCharacterMap.load(keyEvent != null ? keyEvent.getDeviceId() : -1).getKeyboardType() != 1;
            panelFeatureState.p = z2;
            panelFeatureState.j.setQwertyMode(z2);
            panelFeatureState.j.g0();
        }
        panelFeatureState.m = true;
        panelFeatureState.n = false;
        this.L0 = panelFeatureState;
        return true;
    }

    public final void G0(boolean z) {
        kf0 kf0Var = this.p0;
        if (kf0Var != null && kf0Var.b() && (!ViewConfiguration.get(this.i0).hasPermanentMenuKey() || this.p0.c())) {
            Window.Callback k0 = k0();
            if (this.p0.a() && z) {
                this.p0.d();
                if (this.Q0) {
                    return;
                }
                k0.onPanelClosed(108, i0(0, true).j);
                return;
            } else if (k0 == null || this.Q0) {
                return;
            } else {
                if (this.X0 && (this.Y0 & 1) != 0) {
                    this.j0.getDecorView().removeCallbacks(this.Z0);
                    this.Z0.run();
                }
                PanelFeatureState i0 = i0(0, true);
                androidx.appcompat.view.menu.e eVar = i0.j;
                if (eVar == null || i0.r || !k0.onPreparePanel(0, i0.i, eVar)) {
                    return;
                }
                k0.onMenuOpened(108, i0.j);
                this.p0.e();
                return;
            }
        }
        PanelFeatureState i02 = i0(0, true);
        i02.q = true;
        T(i02, false);
        C0(i02, null);
    }

    @Override // androidx.appcompat.app.b
    public void H(Toolbar toolbar) {
        if (this.h0 instanceof Activity) {
            ActionBar p2 = p();
            if (!(p2 instanceof androidx.appcompat.app.e)) {
                this.n0 = null;
                if (p2 != null) {
                    p2.o();
                }
                if (toolbar != null) {
                    androidx.appcompat.app.d dVar = new androidx.appcompat.app.d(toolbar, j0(), this.k0);
                    this.m0 = dVar;
                    this.j0.setCallback(dVar.B());
                } else {
                    this.m0 = null;
                    this.j0.setCallback(this.k0);
                }
                r();
                return;
            }
            throw new IllegalStateException("This Activity already has an action bar supplied by the window decor. Do not request Window.FEATURE_SUPPORT_ACTION_BAR and set windowActionBar to false in your theme to use a Toolbar instead.");
        }
    }

    public final int H0(int i2) {
        if (i2 == 8) {
            return 108;
        }
        if (i2 == 9) {
            return 109;
        }
        return i2;
    }

    @Override // androidx.appcompat.app.b
    public void I(int i2) {
        this.S0 = i2;
    }

    public final boolean I0() {
        ViewGroup viewGroup;
        return this.y0 && (viewGroup = this.z0) != null && ei4.W(viewGroup);
    }

    @Override // androidx.appcompat.app.b
    public final void J(CharSequence charSequence) {
        this.o0 = charSequence;
        kf0 kf0Var = this.p0;
        if (kf0Var != null) {
            kf0Var.setWindowTitle(charSequence);
        } else if (D0() != null) {
            D0().y(charSequence);
        } else {
            TextView textView = this.A0;
            if (textView != null) {
                textView.setText(charSequence);
            }
        }
    }

    public final boolean J0(ViewParent viewParent) {
        if (viewParent == null) {
            return false;
        }
        View decorView = this.j0.getDecorView();
        while (viewParent != null) {
            if (viewParent == decorView || !(viewParent instanceof View) || ei4.V((View) viewParent)) {
                return false;
            }
            viewParent = viewParent.getParent();
        }
        return true;
    }

    @Override // androidx.appcompat.app.b
    public k6 K(k6.a aVar) {
        cf cfVar;
        if (aVar != null) {
            k6 k6Var = this.s0;
            if (k6Var != null) {
                k6Var.c();
            }
            j jVar = new j(aVar);
            ActionBar p2 = p();
            if (p2 != null) {
                k6 z = p2.z(jVar);
                this.s0 = z;
                if (z != null && (cfVar = this.l0) != null) {
                    cfVar.onSupportActionModeStarted(z);
                }
            }
            if (this.s0 == null) {
                this.s0 = K0(jVar);
            }
            return this.s0;
        }
        throw new IllegalArgumentException("ActionMode callback can not be null.");
    }

    /* JADX WARN: Removed duplicated region for block: B:16:0x0025  */
    /* JADX WARN: Removed duplicated region for block: B:17:0x0029  */
    /*
        Code decompiled incorrectly, please refer to instructions dump.
        To view partially-correct code enable 'Show inconsistent code' option in preferences
    */
    public defpackage.k6 K0(defpackage.k6.a r8) {
        /*
            Method dump skipped, instructions count: 368
            To view this dump change 'Code comments level' option to 'DEBUG'
        */
        throw new UnsupportedOperationException("Method not decompiled: androidx.appcompat.app.AppCompatDelegateImpl.K0(k6$a):k6");
    }

    public final boolean L(boolean z) {
        if (this.Q0) {
            return false;
        }
        int O = O();
        boolean N0 = N0(s0(this.i0, O), z);
        if (O == 0) {
            h0(this.i0).e();
        } else {
            q qVar = this.V0;
            if (qVar != null) {
                qVar.a();
            }
        }
        if (O == 3) {
            g0(this.i0).e();
        } else {
            q qVar2 = this.W0;
            if (qVar2 != null) {
                qVar2.a();
            }
        }
        return N0;
    }

    public final void L0() {
        if (this.y0) {
            throw new AndroidRuntimeException("Window feature must be requested before adding content");
        }
    }

    public final void M() {
        ContentFrameLayout contentFrameLayout = (ContentFrameLayout) this.z0.findViewById(16908290);
        View decorView = this.j0.getDecorView();
        contentFrameLayout.setDecorPadding(decorView.getPaddingLeft(), decorView.getPaddingTop(), decorView.getPaddingRight(), decorView.getPaddingBottom());
        TypedArray obtainStyledAttributes = this.i0.obtainStyledAttributes(d33.AppCompatTheme);
        obtainStyledAttributes.getValue(d33.AppCompatTheme_windowMinWidthMajor, contentFrameLayout.getMinWidthMajor());
        obtainStyledAttributes.getValue(d33.AppCompatTheme_windowMinWidthMinor, contentFrameLayout.getMinWidthMinor());
        int i2 = d33.AppCompatTheme_windowFixedWidthMajor;
        if (obtainStyledAttributes.hasValue(i2)) {
            obtainStyledAttributes.getValue(i2, contentFrameLayout.getFixedWidthMajor());
        }
        int i3 = d33.AppCompatTheme_windowFixedWidthMinor;
        if (obtainStyledAttributes.hasValue(i3)) {
            obtainStyledAttributes.getValue(i3, contentFrameLayout.getFixedWidthMinor());
        }
        int i4 = d33.AppCompatTheme_windowFixedHeightMajor;
        if (obtainStyledAttributes.hasValue(i4)) {
            obtainStyledAttributes.getValue(i4, contentFrameLayout.getFixedHeightMajor());
        }
        int i5 = d33.AppCompatTheme_windowFixedHeightMinor;
        if (obtainStyledAttributes.hasValue(i5)) {
            obtainStyledAttributes.getValue(i5, contentFrameLayout.getFixedHeightMinor());
        }
        obtainStyledAttributes.recycle();
        contentFrameLayout.requestLayout();
    }

    public final AppCompatActivity M0() {
        for (Context context = this.i0; context != null; context = ((ContextWrapper) context).getBaseContext()) {
            if (context instanceof AppCompatActivity) {
                return (AppCompatActivity) context;
            }
            if (!(context instanceof ContextWrapper)) {
                break;
            }
        }
        return null;
    }

    public final void N(Window window) {
        if (this.j0 == null) {
            Window.Callback callback = window.getCallback();
            if (!(callback instanceof o)) {
                o oVar = new o(callback);
                this.k0 = oVar;
                window.setCallback(oVar);
                l64 u2 = l64.u(this.i0, null, h1);
                Drawable h2 = u2.h(0);
                if (h2 != null) {
                    window.setBackgroundDrawable(h2);
                }
                u2.w();
                this.j0 = window;
                return;
            }
            throw new IllegalStateException("AppCompat has already installed itself into the Window");
        }
        throw new IllegalStateException("AppCompat has already installed itself into the Window");
    }

    /* JADX WARN: Removed duplicated region for block: B:23:0x0053  */
    /*
        Code decompiled incorrectly, please refer to instructions dump.
        To view partially-correct code enable 'Show inconsistent code' option in preferences
    */
    public final boolean N0(int r7, boolean r8) {
        /*
            r6 = this;
            android.content.Context r0 = r6.i0
            r1 = 0
            android.content.res.Configuration r0 = r6.U(r0, r7, r1)
            boolean r2 = r6.q0()
            android.content.Context r3 = r6.i0
            android.content.res.Resources r3 = r3.getResources()
            android.content.res.Configuration r3 = r3.getConfiguration()
            int r3 = r3.uiMode
            r3 = r3 & 48
            int r0 = r0.uiMode
            r0 = r0 & 48
            r4 = 1
            if (r3 == r0) goto L47
            if (r8 == 0) goto L47
            if (r2 != 0) goto L47
            boolean r8 = r6.N0
            if (r8 == 0) goto L47
            boolean r8 = androidx.appcompat.app.AppCompatDelegateImpl.i1
            if (r8 != 0) goto L30
            boolean r8 = r6.O0
            if (r8 == 0) goto L47
        L30:
            java.lang.Object r8 = r6.h0
            boolean r5 = r8 instanceof android.app.Activity
            if (r5 == 0) goto L47
            android.app.Activity r8 = (android.app.Activity) r8
            boolean r8 = r8.isChild()
            if (r8 != 0) goto L47
            java.lang.Object r8 = r6.h0
            android.app.Activity r8 = (android.app.Activity) r8
            androidx.core.app.a.p(r8)
            r8 = r4
            goto L48
        L47:
            r8 = 0
        L48:
            if (r8 != 0) goto L50
            if (r3 == r0) goto L50
            r6.O0(r0, r2, r1)
            goto L51
        L50:
            r4 = r8
        L51:
            if (r4 == 0) goto L5e
            java.lang.Object r8 = r6.h0
            boolean r0 = r8 instanceof androidx.appcompat.app.AppCompatActivity
            if (r0 == 0) goto L5e
            androidx.appcompat.app.AppCompatActivity r8 = (androidx.appcompat.app.AppCompatActivity) r8
            r8.onNightModeChanged(r7)
        L5e:
            return r4
        */
        throw new UnsupportedOperationException("Method not decompiled: androidx.appcompat.app.AppCompatDelegateImpl.N0(int, boolean):boolean");
    }

    public final int O() {
        int i2 = this.R0;
        return i2 != -100 ? i2 : androidx.appcompat.app.b.l();
    }

    public final void O0(int i2, boolean z, Configuration configuration) {
        Resources resources = this.i0.getResources();
        Configuration configuration2 = new Configuration(resources.getConfiguration());
        if (configuration != null) {
            configuration2.updateFrom(configuration);
        }
        configuration2.uiMode = i2 | (resources.getConfiguration().uiMode & (-49));
        resources.updateConfiguration(configuration2, null);
        int i3 = Build.VERSION.SDK_INT;
        if (i3 < 26) {
            h83.a(resources);
        }
        int i4 = this.S0;
        if (i4 != 0) {
            this.i0.setTheme(i4);
            if (i3 >= 23) {
                this.i0.getTheme().applyStyle(this.S0, true);
            }
        }
        if (z) {
            Object obj = this.h0;
            if (obj instanceof Activity) {
                Activity activity = (Activity) obj;
                if (activity instanceof rz1) {
                    if (((rz1) activity).getLifecycle().b().isAtLeast(Lifecycle.State.STARTED)) {
                        activity.onConfigurationChanged(configuration2);
                    }
                } else if (this.P0) {
                    activity.onConfigurationChanged(configuration2);
                }
            }
        }
    }

    public void P(int i2, PanelFeatureState panelFeatureState, Menu menu) {
        if (menu == null) {
            if (panelFeatureState == null && i2 >= 0) {
                PanelFeatureState[] panelFeatureStateArr = this.K0;
                if (i2 < panelFeatureStateArr.length) {
                    panelFeatureState = panelFeatureStateArr[i2];
                }
            }
            if (panelFeatureState != null) {
                menu = panelFeatureState.j;
            }
        }
        if ((panelFeatureState == null || panelFeatureState.o) && !this.Q0) {
            this.k0.a().onPanelClosed(i2, menu);
        }
    }

    public final int P0(jp4 jp4Var, Rect rect) {
        int i2;
        boolean z;
        boolean z2;
        if (jp4Var != null) {
            i2 = jp4Var.m();
        } else {
            i2 = rect != null ? rect.top : 0;
        }
        ActionBarContextView actionBarContextView = this.t0;
        if (actionBarContextView == null || !(actionBarContextView.getLayoutParams() instanceof ViewGroup.MarginLayoutParams)) {
            z = false;
        } else {
            ViewGroup.MarginLayoutParams marginLayoutParams = (ViewGroup.MarginLayoutParams) this.t0.getLayoutParams();
            if (this.t0.isShown()) {
                if (this.b1 == null) {
                    this.b1 = new Rect();
                    this.c1 = new Rect();
                }
                Rect rect2 = this.b1;
                Rect rect3 = this.c1;
                if (jp4Var == null) {
                    rect2.set(rect);
                } else {
                    rect2.set(jp4Var.k(), jp4Var.m(), jp4Var.l(), jp4Var.j());
                }
                ok4.a(this.z0, rect2, rect3);
                int i3 = rect2.top;
                int i4 = rect2.left;
                int i5 = rect2.right;
                jp4 L = ei4.L(this.z0);
                int k2 = L == null ? 0 : L.k();
                int l2 = L == null ? 0 : L.l();
                if (marginLayoutParams.topMargin == i3 && marginLayoutParams.leftMargin == i4 && marginLayoutParams.rightMargin == i5) {
                    z2 = false;
                } else {
                    marginLayoutParams.topMargin = i3;
                    marginLayoutParams.leftMargin = i4;
                    marginLayoutParams.rightMargin = i5;
                    z2 = true;
                }
                if (i3 > 0 && this.B0 == null) {
                    View view = new View(this.i0);
                    this.B0 = view;
                    view.setVisibility(8);
                    FrameLayout.LayoutParams layoutParams = new FrameLayout.LayoutParams(-1, marginLayoutParams.topMargin, 51);
                    layoutParams.leftMargin = k2;
                    layoutParams.rightMargin = l2;
                    this.z0.addView(this.B0, -1, layoutParams);
                } else {
                    View view2 = this.B0;
                    if (view2 != null) {
                        ViewGroup.MarginLayoutParams marginLayoutParams2 = (ViewGroup.MarginLayoutParams) view2.getLayoutParams();
                        int i6 = marginLayoutParams2.height;
                        int i7 = marginLayoutParams.topMargin;
                        if (i6 != i7 || marginLayoutParams2.leftMargin != k2 || marginLayoutParams2.rightMargin != l2) {
                            marginLayoutParams2.height = i7;
                            marginLayoutParams2.leftMargin = k2;
                            marginLayoutParams2.rightMargin = l2;
                            this.B0.setLayoutParams(marginLayoutParams2);
                        }
                    }
                }
                View view3 = this.B0;
                r5 = view3 != null;
                if (r5 && view3.getVisibility() != 0) {
                    Q0(this.B0);
                }
                if (!this.G0 && r5) {
                    i2 = 0;
                }
                z = r5;
                r5 = z2;
            } else if (marginLayoutParams.topMargin != 0) {
                marginLayoutParams.topMargin = 0;
                z = false;
            } else {
                z = false;
                r5 = false;
            }
            if (r5) {
                this.t0.setLayoutParams(marginLayoutParams);
            }
        }
        View view4 = this.B0;
        if (view4 != null) {
            view4.setVisibility(z ? 0 : 8);
        }
        return i2;
    }

    public void Q(androidx.appcompat.view.menu.e eVar) {
        if (this.J0) {
            return;
        }
        this.J0 = true;
        this.p0.g();
        Window.Callback k0 = k0();
        if (k0 != null && !this.Q0) {
            k0.onPanelClosed(108, eVar);
        }
        this.J0 = false;
    }

    public final void Q0(View view) {
        int d2;
        if ((ei4.P(view) & 8192) != 0) {
            d2 = m70.d(this.i0, xy2.abc_decor_view_status_guard_light);
        } else {
            d2 = m70.d(this.i0, xy2.abc_decor_view_status_guard);
        }
        view.setBackgroundColor(d2);
    }

    public final void R() {
        q qVar = this.V0;
        if (qVar != null) {
            qVar.a();
        }
        q qVar2 = this.W0;
        if (qVar2 != null) {
            qVar2.a();
        }
    }

    public void S(int i2) {
        T(i0(i2, true), true);
    }

    public void T(PanelFeatureState panelFeatureState, boolean z) {
        ViewGroup viewGroup;
        kf0 kf0Var;
        if (z && panelFeatureState.a == 0 && (kf0Var = this.p0) != null && kf0Var.a()) {
            Q(panelFeatureState.j);
            return;
        }
        WindowManager windowManager = (WindowManager) this.i0.getSystemService("window");
        if (windowManager != null && panelFeatureState.o && (viewGroup = panelFeatureState.g) != null) {
            windowManager.removeView(viewGroup);
            if (z) {
                P(panelFeatureState.a, panelFeatureState, null);
            }
        }
        panelFeatureState.m = false;
        panelFeatureState.n = false;
        panelFeatureState.o = false;
        panelFeatureState.h = null;
        panelFeatureState.q = true;
        if (this.L0 == panelFeatureState) {
            this.L0 = null;
        }
    }

    public final Configuration U(Context context, int i2, Configuration configuration) {
        int i3;
        if (i2 != 1) {
            i3 = i2 != 2 ? context.getApplicationContext().getResources().getConfiguration().uiMode & 48 : 32;
        } else {
            i3 = 16;
        }
        Configuration configuration2 = new Configuration();
        configuration2.fontScale = Utils.FLOAT_EPSILON;
        if (configuration != null) {
            configuration2.setTo(configuration);
        }
        configuration2.uiMode = i3 | (configuration2.uiMode & (-49));
        return configuration2;
    }

    public final ViewGroup V() {
        ViewGroup viewGroup;
        Context context;
        TypedArray obtainStyledAttributes = this.i0.obtainStyledAttributes(d33.AppCompatTheme);
        int i2 = d33.AppCompatTheme_windowActionBar;
        if (obtainStyledAttributes.hasValue(i2)) {
            if (obtainStyledAttributes.getBoolean(d33.AppCompatTheme_windowNoTitle, false)) {
                C(1);
            } else if (obtainStyledAttributes.getBoolean(i2, false)) {
                C(108);
            }
            if (obtainStyledAttributes.getBoolean(d33.AppCompatTheme_windowActionBarOverlay, false)) {
                C(109);
            }
            if (obtainStyledAttributes.getBoolean(d33.AppCompatTheme_windowActionModeOverlay, false)) {
                C(10);
            }
            this.H0 = obtainStyledAttributes.getBoolean(d33.AppCompatTheme_android_windowIsFloating, false);
            obtainStyledAttributes.recycle();
            c0();
            this.j0.getDecorView();
            LayoutInflater from = LayoutInflater.from(this.i0);
            if (!this.I0) {
                if (this.H0) {
                    viewGroup = (ViewGroup) from.inflate(d13.abc_dialog_title_material, (ViewGroup) null);
                    this.F0 = false;
                    this.E0 = false;
                } else if (this.E0) {
                    TypedValue typedValue = new TypedValue();
                    this.i0.getTheme().resolveAttribute(jy2.actionBarTheme, typedValue, true);
                    if (typedValue.resourceId != 0) {
                        context = new o70(this.i0, typedValue.resourceId);
                    } else {
                        context = this.i0;
                    }
                    viewGroup = (ViewGroup) LayoutInflater.from(context).inflate(d13.abc_screen_toolbar, (ViewGroup) null);
                    kf0 kf0Var = (kf0) viewGroup.findViewById(q03.decor_content_parent);
                    this.p0 = kf0Var;
                    kf0Var.setWindowCallback(k0());
                    if (this.F0) {
                        this.p0.f(109);
                    }
                    if (this.C0) {
                        this.p0.f(2);
                    }
                    if (this.D0) {
                        this.p0.f(5);
                    }
                } else {
                    viewGroup = null;
                }
            } else {
                viewGroup = this.G0 ? (ViewGroup) from.inflate(d13.abc_screen_simple_overlay_action_mode, (ViewGroup) null) : (ViewGroup) from.inflate(d13.abc_screen_simple, (ViewGroup) null);
            }
            if (viewGroup != null) {
                if (Build.VERSION.SDK_INT >= 21) {
                    ei4.G0(viewGroup, new c());
                } else if (viewGroup instanceof androidx.appcompat.widget.c) {
                    ((androidx.appcompat.widget.c) viewGroup).setOnFitSystemWindowsListener(new d());
                }
                if (this.p0 == null) {
                    this.A0 = (TextView) viewGroup.findViewById(q03.title);
                }
                ok4.c(viewGroup);
                ContentFrameLayout contentFrameLayout = (ContentFrameLayout) viewGroup.findViewById(q03.action_bar_activity_content);
                ViewGroup viewGroup2 = (ViewGroup) this.j0.findViewById(16908290);
                if (viewGroup2 != null) {
                    while (viewGroup2.getChildCount() > 0) {
                        View childAt = viewGroup2.getChildAt(0);
                        viewGroup2.removeViewAt(0);
                        contentFrameLayout.addView(childAt);
                    }
                    viewGroup2.setId(-1);
                    contentFrameLayout.setId(16908290);
                    if (viewGroup2 instanceof FrameLayout) {
                        ((FrameLayout) viewGroup2).setForeground(null);
                    }
                }
                this.j0.setContentView(viewGroup);
                contentFrameLayout.setAttachListener(new e());
                return viewGroup;
            }
            throw new IllegalArgumentException("AppCompat does not support the current theme features: { windowActionBar: " + this.E0 + ", windowActionBarOverlay: " + this.F0 + ", android:windowIsFloating: " + this.H0 + ", windowActionModeOverlay: " + this.G0 + ", windowNoTitle: " + this.I0 + " }");
        }
        obtainStyledAttributes.recycle();
        throw new IllegalStateException("You need to use a Theme.AppCompat theme (or descendant) with this activity.");
    }

    /* JADX WARN: Code restructure failed: missing block: B:21:0x0070, code lost:
        if (((org.xmlpull.v1.XmlPullParser) r15).getDepth() > 1) goto L25;
     */
    /*
        Code decompiled incorrectly, please refer to instructions dump.
        To view partially-correct code enable 'Show inconsistent code' option in preferences
    */
    public android.view.View W(android.view.View r12, java.lang.String r13, android.content.Context r14, android.util.AttributeSet r15) {
        /*
            r11 = this;
            androidx.appcompat.app.c r0 = r11.d1
            r1 = 0
            if (r0 != 0) goto L4b
            android.content.Context r0 = r11.i0
            int[] r2 = defpackage.d33.AppCompatTheme
            android.content.res.TypedArray r0 = r0.obtainStyledAttributes(r2)
            int r2 = defpackage.d33.AppCompatTheme_viewInflaterClass
            java.lang.String r0 = r0.getString(r2)
            if (r0 != 0) goto L1d
            androidx.appcompat.app.c r0 = new androidx.appcompat.app.c
            r0.<init>()
            r11.d1 = r0
            goto L4b
        L1d:
            java.lang.Class r2 = java.lang.Class.forName(r0)     // Catch: java.lang.Throwable -> L32
            java.lang.Class[] r3 = new java.lang.Class[r1]     // Catch: java.lang.Throwable -> L32
            java.lang.reflect.Constructor r2 = r2.getDeclaredConstructor(r3)     // Catch: java.lang.Throwable -> L32
            java.lang.Object[] r3 = new java.lang.Object[r1]     // Catch: java.lang.Throwable -> L32
            java.lang.Object r2 = r2.newInstance(r3)     // Catch: java.lang.Throwable -> L32
            androidx.appcompat.app.c r2 = (androidx.appcompat.app.c) r2     // Catch: java.lang.Throwable -> L32
            r11.d1 = r2     // Catch: java.lang.Throwable -> L32
            goto L4b
        L32:
            java.lang.StringBuilder r2 = new java.lang.StringBuilder
            r2.<init>()
            java.lang.String r3 = "Failed to instantiate custom view inflater "
            r2.append(r3)
            r2.append(r0)
            java.lang.String r0 = ". Falling back to default."
            r2.append(r0)
            androidx.appcompat.app.c r0 = new androidx.appcompat.app.c
            r0.<init>()
            r11.d1 = r0
        L4b:
            boolean r8 = androidx.appcompat.app.AppCompatDelegateImpl.g1
            r0 = 1
            if (r8 == 0) goto L7b
            oy1 r2 = r11.e1
            if (r2 != 0) goto L5b
            oy1 r2 = new oy1
            r2.<init>()
            r11.e1 = r2
        L5b:
            oy1 r2 = r11.e1
            boolean r2 = r2.a(r15)
            if (r2 == 0) goto L65
            r7 = r0
            goto L7c
        L65:
            boolean r2 = r15 instanceof org.xmlpull.v1.XmlPullParser
            if (r2 == 0) goto L73
            r2 = r15
            org.xmlpull.v1.XmlPullParser r2 = (org.xmlpull.v1.XmlPullParser) r2
            int r2 = r2.getDepth()
            if (r2 <= r0) goto L7b
            goto L7a
        L73:
            r0 = r12
            android.view.ViewParent r0 = (android.view.ViewParent) r0
            boolean r0 = r11.J0(r0)
        L7a:
            r1 = r0
        L7b:
            r7 = r1
        L7c:
            androidx.appcompat.app.c r2 = r11.d1
            r9 = 1
            boolean r10 = androidx.appcompat.widget.h.b()
            r3 = r12
            r4 = r13
            r5 = r14
            r6 = r15
            android.view.View r12 = r2.q(r3, r4, r5, r6, r7, r8, r9, r10)
            return r12
        */
        throw new UnsupportedOperationException("Method not decompiled: androidx.appcompat.app.AppCompatDelegateImpl.W(android.view.View, java.lang.String, android.content.Context, android.util.AttributeSet):android.view.View");
    }

    public void X() {
        androidx.appcompat.view.menu.e eVar;
        kf0 kf0Var = this.p0;
        if (kf0Var != null) {
            kf0Var.g();
        }
        if (this.u0 != null) {
            this.j0.getDecorView().removeCallbacks(this.v0);
            if (this.u0.isShowing()) {
                try {
                    this.u0.dismiss();
                } catch (IllegalArgumentException unused) {
                }
            }
            this.u0 = null;
        }
        a0();
        PanelFeatureState i0 = i0(0, false);
        if (i0 == null || (eVar = i0.j) == null) {
            return;
        }
        eVar.close();
    }

    public boolean Y(KeyEvent keyEvent) {
        View decorView;
        Object obj = this.h0;
        if (((obj instanceof ix1.a) || (obj instanceof ef)) && (decorView = this.j0.getDecorView()) != null && ix1.d(decorView, keyEvent)) {
            return true;
        }
        if (keyEvent.getKeyCode() == 82 && this.k0.a().dispatchKeyEvent(keyEvent)) {
            return true;
        }
        int keyCode = keyEvent.getKeyCode();
        return keyEvent.getAction() == 0 ? u0(keyCode, keyEvent) : x0(keyCode, keyEvent);
    }

    public void Z(int i2) {
        PanelFeatureState i0;
        PanelFeatureState i02 = i0(i2, true);
        if (i02.j != null) {
            Bundle bundle = new Bundle();
            i02.j.T(bundle);
            if (bundle.size() > 0) {
                i02.s = bundle;
            }
            i02.j.h0();
            i02.j.clear();
        }
        i02.r = true;
        i02.q = true;
        if ((i2 != 108 && i2 != 0) || this.p0 == null || (i0 = i0(0, false)) == null) {
            return;
        }
        i0.m = false;
        F0(i0, null);
    }

    @Override // androidx.appcompat.view.menu.e.a
    public boolean a(androidx.appcompat.view.menu.e eVar, MenuItem menuItem) {
        PanelFeatureState d0;
        Window.Callback k0 = k0();
        if (k0 == null || this.Q0 || (d0 = d0(eVar.F())) == null) {
            return false;
        }
        return k0.onMenuItemSelected(d0.a, menuItem);
    }

    public void a0() {
        vj4 vj4Var = this.w0;
        if (vj4Var != null) {
            vj4Var.b();
        }
    }

    @Override // androidx.appcompat.view.menu.e.a
    public void b(androidx.appcompat.view.menu.e eVar) {
        G0(true);
    }

    public final void b0() {
        if (this.y0) {
            return;
        }
        this.z0 = V();
        CharSequence j0 = j0();
        if (!TextUtils.isEmpty(j0)) {
            kf0 kf0Var = this.p0;
            if (kf0Var != null) {
                kf0Var.setWindowTitle(j0);
            } else if (D0() != null) {
                D0().y(j0);
            } else {
                TextView textView = this.A0;
                if (textView != null) {
                    textView.setText(j0);
                }
            }
        }
        M();
        B0(this.z0);
        this.y0 = true;
        PanelFeatureState i0 = i0(0, false);
        if (this.Q0) {
            return;
        }
        if (i0 == null || i0.j == null) {
            p0(108);
        }
    }

    public final void c0() {
        if (this.j0 == null) {
            Object obj = this.h0;
            if (obj instanceof Activity) {
                N(((Activity) obj).getWindow());
            }
        }
        if (this.j0 == null) {
            throw new IllegalStateException("We have not been given a Window");
        }
    }

    @Override // androidx.appcompat.app.b
    public void d(View view, ViewGroup.LayoutParams layoutParams) {
        b0();
        ((ViewGroup) this.z0.findViewById(16908290)).addView(view, layoutParams);
        this.k0.a().onContentChanged();
    }

    public PanelFeatureState d0(Menu menu) {
        PanelFeatureState[] panelFeatureStateArr = this.K0;
        int length = panelFeatureStateArr != null ? panelFeatureStateArr.length : 0;
        for (int i2 = 0; i2 < length; i2++) {
            PanelFeatureState panelFeatureState = panelFeatureStateArr[i2];
            if (panelFeatureState != null && panelFeatureState.j == menu) {
                return panelFeatureState;
            }
        }
        return null;
    }

    @Override // androidx.appcompat.app.b
    public boolean e() {
        return L(true);
    }

    public final Context f0() {
        ActionBar p2 = p();
        Context k2 = p2 != null ? p2.k() : null;
        return k2 == null ? this.i0 : k2;
    }

    public final q g0(Context context) {
        if (this.W0 == null) {
            this.W0 = new p(context);
        }
        return this.W0;
    }

    @Override // androidx.appcompat.app.b
    public Context h(Context context) {
        this.N0 = true;
        int s0 = s0(context, O());
        Configuration configuration = null;
        if (j1 && (context instanceof ContextThemeWrapper)) {
            try {
                s.a((ContextThemeWrapper) context, U(context, s0, null));
                return context;
            } catch (IllegalStateException unused) {
            }
        }
        if (context instanceof o70) {
            try {
                ((o70) context).a(U(context, s0, null));
                return context;
            } catch (IllegalStateException unused2) {
            }
        }
        if (!i1) {
            return super.h(context);
        }
        if (Build.VERSION.SDK_INT >= 17) {
            Configuration configuration2 = new Configuration();
            configuration2.uiMode = -1;
            configuration2.fontScale = Utils.FLOAT_EPSILON;
            Configuration configuration3 = k.a(context, configuration2).getResources().getConfiguration();
            Configuration configuration4 = context.getResources().getConfiguration();
            configuration3.uiMode = configuration4.uiMode;
            if (!configuration3.equals(configuration4)) {
                configuration = e0(configuration3, configuration4);
            }
        }
        Configuration U = U(context, s0, configuration);
        o70 o70Var = new o70(context, b23.Theme_AppCompat_Empty);
        o70Var.a(U);
        boolean z = false;
        try {
            z = context.getTheme() != null;
        } catch (NullPointerException unused3) {
        }
        if (z) {
            g83.e.a(o70Var.getTheme());
        }
        return super.h(o70Var);
    }

    public final q h0(Context context) {
        if (this.V0 == null) {
            this.V0 = new r(sc4.a(context));
        }
        return this.V0;
    }

    public PanelFeatureState i0(int i2, boolean z) {
        PanelFeatureState[] panelFeatureStateArr = this.K0;
        if (panelFeatureStateArr == null || panelFeatureStateArr.length <= i2) {
            PanelFeatureState[] panelFeatureStateArr2 = new PanelFeatureState[i2 + 1];
            if (panelFeatureStateArr != null) {
                System.arraycopy(panelFeatureStateArr, 0, panelFeatureStateArr2, 0, panelFeatureStateArr.length);
            }
            this.K0 = panelFeatureStateArr2;
            panelFeatureStateArr = panelFeatureStateArr2;
        }
        PanelFeatureState panelFeatureState = panelFeatureStateArr[i2];
        if (panelFeatureState == null) {
            PanelFeatureState panelFeatureState2 = new PanelFeatureState(i2);
            panelFeatureStateArr[i2] = panelFeatureState2;
            return panelFeatureState2;
        }
        return panelFeatureState;
    }

    public final CharSequence j0() {
        Object obj = this.h0;
        if (obj instanceof Activity) {
            return ((Activity) obj).getTitle();
        }
        return this.o0;
    }

    @Override // androidx.appcompat.app.b
    public <T extends View> T k(int i2) {
        b0();
        return (T) this.j0.findViewById(i2);
    }

    public final Window.Callback k0() {
        return this.j0.getCallback();
    }

    public final void l0() {
        b0();
        if (this.E0 && this.m0 == null) {
            Object obj = this.h0;
            if (obj instanceof Activity) {
                this.m0 = new androidx.appcompat.app.e((Activity) this.h0, this.F0);
            } else if (obj instanceof Dialog) {
                this.m0 = new androidx.appcompat.app.e((Dialog) this.h0);
            }
            ActionBar actionBar = this.m0;
            if (actionBar != null) {
                actionBar.s(this.a1);
            }
        }
    }

    @Override // androidx.appcompat.app.b
    public final g6 m() {
        return new h();
    }

    public final boolean m0(PanelFeatureState panelFeatureState) {
        View view = panelFeatureState.i;
        if (view != null) {
            panelFeatureState.h = view;
            return true;
        } else if (panelFeatureState.j == null) {
            return false;
        } else {
            if (this.r0 == null) {
                this.r0 = new u();
            }
            View view2 = (View) panelFeatureState.a(this.r0);
            panelFeatureState.h = view2;
            return view2 != null;
        }
    }

    @Override // androidx.appcompat.app.b
    public int n() {
        return this.R0;
    }

    public final boolean n0(PanelFeatureState panelFeatureState) {
        panelFeatureState.d(f0());
        panelFeatureState.g = new t(panelFeatureState.l);
        panelFeatureState.c = 81;
        return true;
    }

    @Override // androidx.appcompat.app.b
    public MenuInflater o() {
        if (this.n0 == null) {
            l0();
            ActionBar actionBar = this.m0;
            this.n0 = new kw3(actionBar != null ? actionBar.k() : this.i0);
        }
        return this.n0;
    }

    public final boolean o0(PanelFeatureState panelFeatureState) {
        Context context = this.i0;
        int i2 = panelFeatureState.a;
        if ((i2 == 0 || i2 == 108) && this.p0 != null) {
            TypedValue typedValue = new TypedValue();
            Resources.Theme theme = context.getTheme();
            theme.resolveAttribute(jy2.actionBarTheme, typedValue, true);
            Resources.Theme theme2 = null;
            if (typedValue.resourceId != 0) {
                theme2 = context.getResources().newTheme();
                theme2.setTo(theme);
                theme2.applyStyle(typedValue.resourceId, true);
                theme2.resolveAttribute(jy2.actionBarWidgetTheme, typedValue, true);
            } else {
                theme.resolveAttribute(jy2.actionBarWidgetTheme, typedValue, true);
            }
            if (typedValue.resourceId != 0) {
                if (theme2 == null) {
                    theme2 = context.getResources().newTheme();
                    theme2.setTo(theme);
                }
                theme2.applyStyle(typedValue.resourceId, true);
            }
            if (theme2 != null) {
                o70 o70Var = new o70(context, 0);
                o70Var.getTheme().setTo(theme2);
                context = o70Var;
            }
        }
        androidx.appcompat.view.menu.e eVar = new androidx.appcompat.view.menu.e(context);
        eVar.V(this);
        panelFeatureState.c(eVar);
        return true;
    }

    @Override // android.view.LayoutInflater.Factory2
    public final View onCreateView(View view, String str, Context context, AttributeSet attributeSet) {
        return W(view, str, context, attributeSet);
    }

    @Override // androidx.appcompat.app.b
    public ActionBar p() {
        l0();
        return this.m0;
    }

    public final void p0(int i2) {
        this.Y0 = (1 << i2) | this.Y0;
        if (this.X0) {
            return;
        }
        ei4.l0(this.j0.getDecorView(), this.Z0);
        this.X0 = true;
    }

    @Override // androidx.appcompat.app.b
    public void q() {
        LayoutInflater from = LayoutInflater.from(this.i0);
        if (from.getFactory() == null) {
            py1.b(from, this);
        } else {
            boolean z = from.getFactory2() instanceof AppCompatDelegateImpl;
        }
    }

    public final boolean q0() {
        if (!this.U0 && (this.h0 instanceof Activity)) {
            PackageManager packageManager = this.i0.getPackageManager();
            if (packageManager == null) {
                return false;
            }
            try {
                int i2 = Build.VERSION.SDK_INT;
                ActivityInfo activityInfo = packageManager.getActivityInfo(new ComponentName(this.i0, this.h0.getClass()), i2 >= 29 ? 269221888 : i2 >= 24 ? 786432 : 0);
                this.T0 = (activityInfo == null || (activityInfo.configChanges & RecyclerView.a0.FLAG_ADAPTER_POSITION_UNKNOWN) == 0) ? false : true;
            } catch (PackageManager.NameNotFoundException unused) {
                this.T0 = false;
            }
        }
        this.U0 = true;
        return this.T0;
    }

    @Override // androidx.appcompat.app.b
    public void r() {
        ActionBar p2 = p();
        if (p2 == null || !p2.m()) {
            p0(0);
        }
    }

    public boolean r0() {
        return this.x0;
    }

    @Override // androidx.appcompat.app.b
    public void s(Configuration configuration) {
        ActionBar p2;
        if (this.E0 && this.y0 && (p2 = p()) != null) {
            p2.n(configuration);
        }
        gf.b().g(this.i0);
        L(false);
    }

    public int s0(Context context, int i2) {
        if (i2 != -100) {
            if (i2 != -1) {
                if (i2 == 0) {
                    if (Build.VERSION.SDK_INT < 23 || ((UiModeManager) context.getApplicationContext().getSystemService("uimode")).getNightMode() != 0) {
                        return h0(context).c();
                    }
                    return -1;
                } else if (i2 != 1 && i2 != 2) {
                    if (i2 == 3) {
                        return g0(context).c();
                    }
                    throw new IllegalStateException("Unknown value set for night mode. Please use one of the MODE_NIGHT values from AppCompatDelegate.");
                }
            }
            return i2;
        }
        return -1;
    }

    @Override // androidx.appcompat.app.b
    public void t(Bundle bundle) {
        this.N0 = true;
        L(false);
        c0();
        Object obj = this.h0;
        if (obj instanceof Activity) {
            String str = null;
            try {
                str = androidx.core.app.b.c((Activity) obj);
            } catch (IllegalArgumentException unused) {
            }
            if (str != null) {
                ActionBar D0 = D0();
                if (D0 == null) {
                    this.a1 = true;
                } else {
                    D0.s(true);
                }
            }
            androidx.appcompat.app.b.c(this);
        }
        this.O0 = true;
    }

    public boolean t0() {
        k6 k6Var = this.s0;
        if (k6Var != null) {
            k6Var.c();
            return true;
        }
        ActionBar p2 = p();
        return p2 != null && p2.h();
    }

    /* JADX WARN: Removed duplicated region for block: B:18:0x005b  */
    @Override // androidx.appcompat.app.b
    /*
        Code decompiled incorrectly, please refer to instructions dump.
        To view partially-correct code enable 'Show inconsistent code' option in preferences
    */
    public void u() {
        /*
            r3 = this;
            java.lang.Object r0 = r3.h0
            boolean r0 = r0 instanceof android.app.Activity
            if (r0 == 0) goto L9
            androidx.appcompat.app.b.A(r3)
        L9:
            boolean r0 = r3.X0
            if (r0 == 0) goto L18
            android.view.Window r0 = r3.j0
            android.view.View r0 = r0.getDecorView()
            java.lang.Runnable r1 = r3.Z0
            r0.removeCallbacks(r1)
        L18:
            r0 = 0
            r3.P0 = r0
            r0 = 1
            r3.Q0 = r0
            int r0 = r3.R0
            r1 = -100
            if (r0 == r1) goto L48
            java.lang.Object r0 = r3.h0
            boolean r1 = r0 instanceof android.app.Activity
            if (r1 == 0) goto L48
            android.app.Activity r0 = (android.app.Activity) r0
            boolean r0 = r0.isChangingConfigurations()
            if (r0 == 0) goto L48
            vo3<java.lang.String, java.lang.Integer> r0 = androidx.appcompat.app.AppCompatDelegateImpl.f1
            java.lang.Object r1 = r3.h0
            java.lang.Class r1 = r1.getClass()
            java.lang.String r1 = r1.getName()
            int r2 = r3.R0
            java.lang.Integer r2 = java.lang.Integer.valueOf(r2)
            r0.put(r1, r2)
            goto L57
        L48:
            vo3<java.lang.String, java.lang.Integer> r0 = androidx.appcompat.app.AppCompatDelegateImpl.f1
            java.lang.Object r1 = r3.h0
            java.lang.Class r1 = r1.getClass()
            java.lang.String r1 = r1.getName()
            r0.remove(r1)
        L57:
            androidx.appcompat.app.ActionBar r0 = r3.m0
            if (r0 == 0) goto L5e
            r0.o()
        L5e:
            r3.R()
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: androidx.appcompat.app.AppCompatDelegateImpl.u():void");
    }

    public boolean u0(int i2, KeyEvent keyEvent) {
        if (i2 == 4) {
            this.M0 = (keyEvent.getFlags() & 128) != 0;
        } else if (i2 == 82) {
            v0(0, keyEvent);
            return true;
        }
        return false;
    }

    @Override // androidx.appcompat.app.b
    public void v(Bundle bundle) {
        b0();
    }

    public final boolean v0(int i2, KeyEvent keyEvent) {
        if (keyEvent.getRepeatCount() == 0) {
            PanelFeatureState i0 = i0(i2, true);
            if (i0.o) {
                return false;
            }
            return F0(i0, keyEvent);
        }
        return false;
    }

    @Override // androidx.appcompat.app.b
    public void w() {
        ActionBar p2 = p();
        if (p2 != null) {
            p2.w(true);
        }
    }

    public boolean w0(int i2, KeyEvent keyEvent) {
        ActionBar p2 = p();
        if (p2 == null || !p2.p(i2, keyEvent)) {
            PanelFeatureState panelFeatureState = this.L0;
            if (panelFeatureState != null && E0(panelFeatureState, keyEvent.getKeyCode(), keyEvent, 1)) {
                PanelFeatureState panelFeatureState2 = this.L0;
                if (panelFeatureState2 != null) {
                    panelFeatureState2.n = true;
                }
                return true;
            }
            if (this.L0 == null) {
                PanelFeatureState i0 = i0(0, true);
                F0(i0, keyEvent);
                boolean E0 = E0(i0, keyEvent.getKeyCode(), keyEvent, 1);
                i0.m = false;
                if (E0) {
                    return true;
                }
            }
            return false;
        }
        return true;
    }

    @Override // androidx.appcompat.app.b
    public void x(Bundle bundle) {
    }

    public boolean x0(int i2, KeyEvent keyEvent) {
        if (i2 == 4) {
            boolean z = this.M0;
            this.M0 = false;
            PanelFeatureState i0 = i0(0, false);
            if (i0 != null && i0.o) {
                if (!z) {
                    T(i0, true);
                }
                return true;
            } else if (t0()) {
                return true;
            }
        } else if (i2 == 82) {
            y0(0, keyEvent);
            return true;
        }
        return false;
    }

    @Override // androidx.appcompat.app.b
    public void y() {
        this.P0 = true;
        e();
    }

    public final boolean y0(int i2, KeyEvent keyEvent) {
        boolean z;
        AudioManager audioManager;
        kf0 kf0Var;
        if (this.s0 != null) {
            return false;
        }
        boolean z2 = true;
        PanelFeatureState i0 = i0(i2, true);
        if (i2 == 0 && (kf0Var = this.p0) != null && kf0Var.b() && !ViewConfiguration.get(this.i0).hasPermanentMenuKey()) {
            if (!this.p0.a()) {
                if (!this.Q0 && F0(i0, keyEvent)) {
                    z2 = this.p0.e();
                }
                z2 = false;
            } else {
                z2 = this.p0.d();
            }
        } else {
            boolean z3 = i0.o;
            if (!z3 && !i0.n) {
                if (i0.m) {
                    if (i0.r) {
                        i0.m = false;
                        z = F0(i0, keyEvent);
                    } else {
                        z = true;
                    }
                    if (z) {
                        C0(i0, keyEvent);
                    }
                }
                z2 = false;
            } else {
                T(i0, true);
                z2 = z3;
            }
        }
        if (z2 && (audioManager = (AudioManager) this.i0.getApplicationContext().getSystemService("audio")) != null) {
            audioManager.playSoundEffect(0);
        }
        return z2;
    }

    @Override // androidx.appcompat.app.b
    public void z() {
        this.P0 = false;
        ActionBar p2 = p();
        if (p2 != null) {
            p2.w(false);
        }
    }

    public void z0(int i2) {
        ActionBar p2;
        if (i2 != 108 || (p2 = p()) == null) {
            return;
        }
        p2.i(true);
    }

    public AppCompatDelegateImpl(Dialog dialog, cf cfVar) {
        this(dialog.getContext(), dialog.getWindow(), cfVar, dialog);
    }

    @Override // android.view.LayoutInflater.Factory
    public View onCreateView(String str, Context context, AttributeSet attributeSet) {
        return onCreateView(null, str, context, attributeSet);
    }

    public AppCompatDelegateImpl(Context context, Window window, cf cfVar, Object obj) {
        vo3<String, Integer> vo3Var;
        Integer num;
        AppCompatActivity M0;
        this.w0 = null;
        this.x0 = true;
        this.R0 = -100;
        this.Z0 = new b();
        this.i0 = context;
        this.l0 = cfVar;
        this.h0 = obj;
        if (this.R0 == -100 && (obj instanceof Dialog) && (M0 = M0()) != null) {
            this.R0 = M0.getDelegate().n();
        }
        if (this.R0 == -100 && (num = (vo3Var = f1).get(obj.getClass().getName())) != null) {
            this.R0 = num.intValue();
            vo3Var.remove(obj.getClass().getName());
        }
        if (window != null) {
            N(window);
        }
        gf.h();
    }

    /* loaded from: classes.dex */
    public class o extends fp4 {
        public o(Window.Callback callback) {
            super(callback);
        }

        public final ActionMode b(ActionMode.Callback callback) {
            hw3.a aVar = new hw3.a(AppCompatDelegateImpl.this.i0, callback);
            k6 K = AppCompatDelegateImpl.this.K(aVar);
            if (K != null) {
                return aVar.e(K);
            }
            return null;
        }

        @Override // defpackage.fp4, android.view.Window.Callback
        public boolean dispatchKeyEvent(KeyEvent keyEvent) {
            return AppCompatDelegateImpl.this.Y(keyEvent) || super.dispatchKeyEvent(keyEvent);
        }

        @Override // defpackage.fp4, android.view.Window.Callback
        public boolean dispatchKeyShortcutEvent(KeyEvent keyEvent) {
            return super.dispatchKeyShortcutEvent(keyEvent) || AppCompatDelegateImpl.this.w0(keyEvent.getKeyCode(), keyEvent);
        }

        @Override // defpackage.fp4, android.view.Window.Callback
        public void onContentChanged() {
        }

        @Override // defpackage.fp4, android.view.Window.Callback
        public boolean onCreatePanelMenu(int i, Menu menu) {
            if (i != 0 || (menu instanceof androidx.appcompat.view.menu.e)) {
                return super.onCreatePanelMenu(i, menu);
            }
            return false;
        }

        @Override // defpackage.fp4, android.view.Window.Callback
        public boolean onMenuOpened(int i, Menu menu) {
            super.onMenuOpened(i, menu);
            AppCompatDelegateImpl.this.z0(i);
            return true;
        }

        @Override // defpackage.fp4, android.view.Window.Callback
        public void onPanelClosed(int i, Menu menu) {
            super.onPanelClosed(i, menu);
            AppCompatDelegateImpl.this.A0(i);
        }

        @Override // defpackage.fp4, android.view.Window.Callback
        public boolean onPreparePanel(int i, View view, Menu menu) {
            androidx.appcompat.view.menu.e eVar = menu instanceof androidx.appcompat.view.menu.e ? (androidx.appcompat.view.menu.e) menu : null;
            if (i == 0 && eVar == null) {
                return false;
            }
            if (eVar != null) {
                eVar.e0(true);
            }
            boolean onPreparePanel = super.onPreparePanel(i, view, menu);
            if (eVar != null) {
                eVar.e0(false);
            }
            return onPreparePanel;
        }

        @Override // defpackage.fp4, android.view.Window.Callback
        public void onProvideKeyboardShortcuts(List<KeyboardShortcutGroup> list, Menu menu, int i) {
            androidx.appcompat.view.menu.e eVar;
            PanelFeatureState i0 = AppCompatDelegateImpl.this.i0(0, true);
            if (i0 != null && (eVar = i0.j) != null) {
                super.onProvideKeyboardShortcuts(list, eVar, i);
            } else {
                super.onProvideKeyboardShortcuts(list, menu, i);
            }
        }

        @Override // defpackage.fp4, android.view.Window.Callback
        public ActionMode onWindowStartingActionMode(ActionMode.Callback callback) {
            if (Build.VERSION.SDK_INT >= 23) {
                return null;
            }
            if (AppCompatDelegateImpl.this.r0()) {
                return b(callback);
            }
            return super.onWindowStartingActionMode(callback);
        }

        @Override // defpackage.fp4, android.view.Window.Callback
        public ActionMode onWindowStartingActionMode(ActionMode.Callback callback, int i) {
            if (AppCompatDelegateImpl.this.r0() && i == 0) {
                return b(callback);
            }
            return super.onWindowStartingActionMode(callback, i);
        }
    }
}
