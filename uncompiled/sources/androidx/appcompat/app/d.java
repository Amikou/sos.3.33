package androidx.appcompat.app;

import android.content.Context;
import android.content.res.Configuration;
import android.graphics.drawable.Drawable;
import android.view.KeyCharacterMap;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import androidx.appcompat.app.ActionBar;
import androidx.appcompat.view.menu.e;
import androidx.appcompat.view.menu.i;
import androidx.appcompat.widget.Toolbar;
import androidx.appcompat.widget.g;
import java.util.ArrayList;

/* compiled from: ToolbarActionBar.java */
/* loaded from: classes.dex */
public class d extends ActionBar {
    public lf0 a;
    public boolean b;
    public Window.Callback c;
    public boolean d;
    public boolean e;
    public ArrayList<ActionBar.a> f = new ArrayList<>();
    public final Runnable g = new a();
    public final Toolbar.e h;

    /* compiled from: ToolbarActionBar.java */
    /* loaded from: classes.dex */
    public class a implements Runnable {
        public a() {
        }

        @Override // java.lang.Runnable
        public void run() {
            d.this.C();
        }
    }

    /* compiled from: ToolbarActionBar.java */
    /* loaded from: classes.dex */
    public class b implements Toolbar.e {
        public b() {
        }

        @Override // androidx.appcompat.widget.Toolbar.e
        public boolean onMenuItemClick(MenuItem menuItem) {
            return d.this.c.onMenuItemSelected(0, menuItem);
        }
    }

    /* compiled from: ToolbarActionBar.java */
    /* loaded from: classes.dex */
    public final class c implements i.a {
        public boolean a;

        public c() {
        }

        @Override // androidx.appcompat.view.menu.i.a
        public void c(androidx.appcompat.view.menu.e eVar, boolean z) {
            if (this.a) {
                return;
            }
            this.a = true;
            d.this.a.f();
            Window.Callback callback = d.this.c;
            if (callback != null) {
                callback.onPanelClosed(108, eVar);
            }
            this.a = false;
        }

        @Override // androidx.appcompat.view.menu.i.a
        public boolean d(androidx.appcompat.view.menu.e eVar) {
            Window.Callback callback = d.this.c;
            if (callback != null) {
                callback.onMenuOpened(108, eVar);
                return true;
            }
            return false;
        }
    }

    /* compiled from: ToolbarActionBar.java */
    /* renamed from: androidx.appcompat.app.d$d  reason: collision with other inner class name */
    /* loaded from: classes.dex */
    public final class C0009d implements e.a {
        public C0009d() {
        }

        @Override // androidx.appcompat.view.menu.e.a
        public boolean a(androidx.appcompat.view.menu.e eVar, MenuItem menuItem) {
            return false;
        }

        @Override // androidx.appcompat.view.menu.e.a
        public void b(androidx.appcompat.view.menu.e eVar) {
            d dVar = d.this;
            if (dVar.c != null) {
                if (dVar.a.a()) {
                    d.this.c.onPanelClosed(108, eVar);
                } else if (d.this.c.onPreparePanel(0, null, eVar)) {
                    d.this.c.onMenuOpened(108, eVar);
                }
            }
        }
    }

    /* compiled from: ToolbarActionBar.java */
    /* loaded from: classes.dex */
    public class e extends fp4 {
        public e(Window.Callback callback) {
            super(callback);
        }

        @Override // defpackage.fp4, android.view.Window.Callback
        public View onCreatePanelView(int i) {
            if (i == 0) {
                return new View(d.this.a.getContext());
            }
            return super.onCreatePanelView(i);
        }

        @Override // defpackage.fp4, android.view.Window.Callback
        public boolean onPreparePanel(int i, View view, Menu menu) {
            boolean onPreparePanel = super.onPreparePanel(i, view, menu);
            if (onPreparePanel) {
                d dVar = d.this;
                if (!dVar.b) {
                    dVar.a.setMenuPrepared();
                    d.this.b = true;
                }
            }
            return onPreparePanel;
        }
    }

    public d(Toolbar toolbar, CharSequence charSequence, Window.Callback callback) {
        b bVar = new b();
        this.h = bVar;
        this.a = new g(toolbar, false);
        e eVar = new e(callback);
        this.c = eVar;
        this.a.setWindowCallback(eVar);
        toolbar.setOnMenuItemClickListener(bVar);
        this.a.setWindowTitle(charSequence);
    }

    public final Menu A() {
        if (!this.d) {
            this.a.n(new c(), new C0009d());
            this.d = true;
        }
        return this.a.j();
    }

    public Window.Callback B() {
        return this.c;
    }

    public void C() {
        Menu A = A();
        androidx.appcompat.view.menu.e eVar = A instanceof androidx.appcompat.view.menu.e ? (androidx.appcompat.view.menu.e) A : null;
        if (eVar != null) {
            eVar.h0();
        }
        try {
            A.clear();
            if (!this.c.onCreatePanelMenu(0, A) || !this.c.onPreparePanel(0, null, A)) {
                A.clear();
            }
        } finally {
            if (eVar != null) {
                eVar.g0();
            }
        }
    }

    public void D(int i, int i2) {
        this.a.i((i & i2) | ((~i2) & this.a.q()));
    }

    @Override // androidx.appcompat.app.ActionBar
    public boolean g() {
        return this.a.d();
    }

    @Override // androidx.appcompat.app.ActionBar
    public boolean h() {
        if (this.a.h()) {
            this.a.collapseActionView();
            return true;
        }
        return false;
    }

    @Override // androidx.appcompat.app.ActionBar
    public void i(boolean z) {
        if (z == this.e) {
            return;
        }
        this.e = z;
        int size = this.f.size();
        for (int i = 0; i < size; i++) {
            this.f.get(i).a(z);
        }
    }

    @Override // androidx.appcompat.app.ActionBar
    public int j() {
        return this.a.q();
    }

    @Override // androidx.appcompat.app.ActionBar
    public Context k() {
        return this.a.getContext();
    }

    @Override // androidx.appcompat.app.ActionBar
    public void l() {
        this.a.setVisibility(8);
    }

    @Override // androidx.appcompat.app.ActionBar
    public boolean m() {
        this.a.o().removeCallbacks(this.g);
        ei4.l0(this.a.o(), this.g);
        return true;
    }

    @Override // androidx.appcompat.app.ActionBar
    public void n(Configuration configuration) {
        super.n(configuration);
    }

    @Override // androidx.appcompat.app.ActionBar
    public void o() {
        this.a.o().removeCallbacks(this.g);
    }

    @Override // androidx.appcompat.app.ActionBar
    public boolean p(int i, KeyEvent keyEvent) {
        Menu A = A();
        if (A != null) {
            A.setQwertyMode(KeyCharacterMap.load(keyEvent != null ? keyEvent.getDeviceId() : -1).getKeyboardType() != 1);
            return A.performShortcut(i, keyEvent, 0);
        }
        return false;
    }

    @Override // androidx.appcompat.app.ActionBar
    public boolean q(KeyEvent keyEvent) {
        if (keyEvent.getAction() == 1) {
            r();
        }
        return true;
    }

    @Override // androidx.appcompat.app.ActionBar
    public boolean r() {
        return this.a.e();
    }

    @Override // androidx.appcompat.app.ActionBar
    public void s(boolean z) {
    }

    @Override // androidx.appcompat.app.ActionBar
    public void t(boolean z) {
        D(z ? 4 : 0, 4);
    }

    @Override // androidx.appcompat.app.ActionBar
    public void u(int i) {
        this.a.r(i);
    }

    @Override // androidx.appcompat.app.ActionBar
    public void v(Drawable drawable) {
        this.a.u(drawable);
    }

    @Override // androidx.appcompat.app.ActionBar
    public void w(boolean z) {
    }

    @Override // androidx.appcompat.app.ActionBar
    public void x(CharSequence charSequence) {
        this.a.setTitle(charSequence);
    }

    @Override // androidx.appcompat.app.ActionBar
    public void y(CharSequence charSequence) {
        this.a.setWindowTitle(charSequence);
    }
}
