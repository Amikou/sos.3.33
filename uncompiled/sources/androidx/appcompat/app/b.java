package androidx.appcompat.app;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.res.Configuration;
import android.os.Bundle;
import android.view.MenuInflater;
import android.view.View;
import android.view.ViewGroup;
import androidx.appcompat.widget.Toolbar;
import defpackage.k6;
import java.lang.ref.WeakReference;
import java.util.Iterator;

/* compiled from: AppCompatDelegate.java */
/* loaded from: classes.dex */
public abstract class b {
    public static int a = -100;
    public static final uh<WeakReference<b>> f0 = new uh<>();
    public static final Object g0 = new Object();

    public static void A(b bVar) {
        synchronized (g0) {
            B(bVar);
        }
    }

    public static void B(b bVar) {
        synchronized (g0) {
            Iterator<WeakReference<b>> it = f0.iterator();
            while (it.hasNext()) {
                b bVar2 = it.next().get();
                if (bVar2 == bVar || bVar2 == null) {
                    it.remove();
                }
            }
        }
    }

    public static void G(int i) {
        if ((i == -1 || i == 0 || i == 1 || i == 2 || i == 3) && a != i) {
            a = i;
            f();
        }
    }

    public static void c(b bVar) {
        synchronized (g0) {
            B(bVar);
            f0.add(new WeakReference<>(bVar));
        }
    }

    public static void f() {
        synchronized (g0) {
            Iterator<WeakReference<b>> it = f0.iterator();
            while (it.hasNext()) {
                b bVar = it.next().get();
                if (bVar != null) {
                    bVar.e();
                }
            }
        }
    }

    public static b i(Activity activity, cf cfVar) {
        return new AppCompatDelegateImpl(activity, cfVar);
    }

    public static b j(Dialog dialog, cf cfVar) {
        return new AppCompatDelegateImpl(dialog, cfVar);
    }

    public static int l() {
        return a;
    }

    public abstract boolean C(int i);

    public abstract void D(int i);

    public abstract void E(View view);

    public abstract void F(View view, ViewGroup.LayoutParams layoutParams);

    public abstract void H(Toolbar toolbar);

    public void I(int i) {
    }

    public abstract void J(CharSequence charSequence);

    public abstract k6 K(k6.a aVar);

    public abstract void d(View view, ViewGroup.LayoutParams layoutParams);

    public abstract boolean e();

    @Deprecated
    public void g(Context context) {
    }

    public Context h(Context context) {
        g(context);
        return context;
    }

    public abstract <T extends View> T k(int i);

    public abstract g6 m();

    public int n() {
        return -100;
    }

    public abstract MenuInflater o();

    public abstract ActionBar p();

    public abstract void q();

    public abstract void r();

    public abstract void s(Configuration configuration);

    public abstract void t(Bundle bundle);

    public abstract void u();

    public abstract void v(Bundle bundle);

    public abstract void w();

    public abstract void x(Bundle bundle);

    public abstract void y();

    public abstract void z();
}
