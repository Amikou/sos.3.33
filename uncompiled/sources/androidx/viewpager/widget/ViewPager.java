package androidx.viewpager.widget;

import android.content.Context;
import android.content.res.TypedArray;
import android.database.DataSetObserver;
import android.graphics.Canvas;
import android.graphics.Rect;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.os.Parcel;
import android.os.Parcelable;
import android.os.SystemClock;
import android.util.AttributeSet;
import android.view.KeyEvent;
import android.view.MotionEvent;
import android.view.VelocityTracker;
import android.view.View;
import android.view.ViewConfiguration;
import android.view.ViewGroup;
import android.view.ViewParent;
import android.view.accessibility.AccessibilityEvent;
import android.view.animation.Interpolator;
import android.widget.EdgeEffect;
import android.widget.Scroller;
import androidx.customview.view.AbsSavedState;
import com.github.mikephil.charting.utils.Utils;
import java.lang.annotation.ElementType;
import java.lang.annotation.Inherited;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

/* loaded from: classes.dex */
public class ViewPager extends ViewGroup {
    public static final int[] g1 = {16842931};
    public static final Comparator<f> h1 = new a();
    public static final Interpolator i1 = new b();
    public static final l j1 = new l();
    public int A0;
    public boolean B0;
    public boolean C0;
    public int D0;
    public int E0;
    public int F0;
    public float G0;
    public float H0;
    public float I0;
    public float J0;
    public int K0;
    public VelocityTracker L0;
    public int M0;
    public int N0;
    public int O0;
    public int P0;
    public boolean Q0;
    public EdgeEffect R0;
    public EdgeEffect S0;
    public boolean T0;
    public boolean U0;
    public int V0;
    public List<i> W0;
    public i X0;
    public i Y0;
    public List<h> Z0;
    public int a;
    public j a1;
    public int b1;
    public int c1;
    public ArrayList<View> d1;
    public final Runnable e1;
    public final ArrayList<f> f0;
    public int f1;
    public final f g0;
    public final Rect h0;
    public dp2 i0;
    public int j0;
    public int k0;
    public Parcelable l0;
    public ClassLoader m0;
    public Scroller n0;
    public boolean o0;
    public k p0;
    public int q0;
    public Drawable r0;
    public int s0;
    public int t0;
    public float u0;
    public float v0;
    public int w0;
    public boolean x0;
    public boolean y0;
    public boolean z0;

    /* loaded from: classes.dex */
    public static class SavedState extends AbsSavedState {
        public static final Parcelable.Creator<SavedState> CREATOR = new a();
        public int g0;
        public Parcelable h0;
        public ClassLoader i0;

        /* loaded from: classes.dex */
        public static class a implements Parcelable.ClassLoaderCreator<SavedState> {
            @Override // android.os.Parcelable.Creator
            /* renamed from: a */
            public SavedState createFromParcel(Parcel parcel) {
                return new SavedState(parcel, null);
            }

            @Override // android.os.Parcelable.ClassLoaderCreator
            /* renamed from: b */
            public SavedState createFromParcel(Parcel parcel, ClassLoader classLoader) {
                return new SavedState(parcel, classLoader);
            }

            @Override // android.os.Parcelable.Creator
            /* renamed from: c */
            public SavedState[] newArray(int i) {
                return new SavedState[i];
            }
        }

        public SavedState(Parcelable parcelable) {
            super(parcelable);
        }

        public String toString() {
            return "FragmentPager.SavedState{" + Integer.toHexString(System.identityHashCode(this)) + " position=" + this.g0 + "}";
        }

        @Override // androidx.customview.view.AbsSavedState, android.os.Parcelable
        public void writeToParcel(Parcel parcel, int i) {
            super.writeToParcel(parcel, i);
            parcel.writeInt(this.g0);
            parcel.writeParcelable(this.h0, i);
        }

        public SavedState(Parcel parcel, ClassLoader classLoader) {
            super(parcel, classLoader);
            classLoader = classLoader == null ? getClass().getClassLoader() : classLoader;
            this.g0 = parcel.readInt();
            this.h0 = parcel.readParcelable(classLoader);
            this.i0 = classLoader;
        }
    }

    /* loaded from: classes.dex */
    public static class a implements Comparator<f> {
        @Override // java.util.Comparator
        /* renamed from: a */
        public int compare(f fVar, f fVar2) {
            return fVar.b - fVar2.b;
        }
    }

    /* loaded from: classes.dex */
    public static class b implements Interpolator {
        @Override // android.animation.TimeInterpolator
        public float getInterpolation(float f) {
            float f2 = f - 1.0f;
            return (f2 * f2 * f2 * f2 * f2) + 1.0f;
        }
    }

    /* loaded from: classes.dex */
    public class c implements Runnable {
        public c() {
        }

        @Override // java.lang.Runnable
        public void run() {
            ViewPager.this.setScrollState(0);
            ViewPager.this.H();
        }
    }

    /* loaded from: classes.dex */
    public class d implements em2 {
        public final Rect a = new Rect();

        public d() {
        }

        @Override // defpackage.em2
        public jp4 a(View view, jp4 jp4Var) {
            jp4 e0 = ei4.e0(view, jp4Var);
            if (e0.q()) {
                return e0;
            }
            Rect rect = this.a;
            rect.left = e0.k();
            rect.top = e0.m();
            rect.right = e0.l();
            rect.bottom = e0.j();
            int childCount = ViewPager.this.getChildCount();
            for (int i = 0; i < childCount; i++) {
                jp4 i2 = ei4.i(ViewPager.this.getChildAt(i), e0);
                rect.left = Math.min(i2.k(), rect.left);
                rect.top = Math.min(i2.m(), rect.top);
                rect.right = Math.min(i2.l(), rect.right);
                rect.bottom = Math.min(i2.j(), rect.bottom);
            }
            return e0.r(rect.left, rect.top, rect.right, rect.bottom);
        }
    }

    @Target({ElementType.TYPE})
    @Inherited
    @Retention(RetentionPolicy.RUNTIME)
    /* loaded from: classes.dex */
    public @interface e {
    }

    /* loaded from: classes.dex */
    public static class f {
        public Object a;
        public int b;
        public boolean c;
        public float d;
        public float e;
    }

    /* loaded from: classes.dex */
    public class g extends z5 {
        public g() {
        }

        @Override // defpackage.z5
        public void f(View view, AccessibilityEvent accessibilityEvent) {
            dp2 dp2Var;
            super.f(view, accessibilityEvent);
            accessibilityEvent.setClassName(ViewPager.class.getName());
            accessibilityEvent.setScrollable(n());
            if (accessibilityEvent.getEventType() != 4096 || (dp2Var = ViewPager.this.i0) == null) {
                return;
            }
            accessibilityEvent.setItemCount(dp2Var.e());
            accessibilityEvent.setFromIndex(ViewPager.this.j0);
            accessibilityEvent.setToIndex(ViewPager.this.j0);
        }

        @Override // defpackage.z5
        public void g(View view, b6 b6Var) {
            super.g(view, b6Var);
            b6Var.c0(ViewPager.class.getName());
            b6Var.y0(n());
            if (ViewPager.this.canScrollHorizontally(1)) {
                b6Var.a(4096);
            }
            if (ViewPager.this.canScrollHorizontally(-1)) {
                b6Var.a(8192);
            }
        }

        @Override // defpackage.z5
        public boolean j(View view, int i, Bundle bundle) {
            if (super.j(view, i, bundle)) {
                return true;
            }
            if (i != 4096) {
                if (i == 8192 && ViewPager.this.canScrollHorizontally(-1)) {
                    ViewPager viewPager = ViewPager.this;
                    viewPager.setCurrentItem(viewPager.j0 - 1);
                    return true;
                }
                return false;
            } else if (ViewPager.this.canScrollHorizontally(1)) {
                ViewPager viewPager2 = ViewPager.this;
                viewPager2.setCurrentItem(viewPager2.j0 + 1);
                return true;
            } else {
                return false;
            }
        }

        public final boolean n() {
            dp2 dp2Var = ViewPager.this.i0;
            return dp2Var != null && dp2Var.e() > 1;
        }
    }

    /* loaded from: classes.dex */
    public interface h {
        void b(ViewPager viewPager, dp2 dp2Var, dp2 dp2Var2);
    }

    /* loaded from: classes.dex */
    public interface i {
        void a(int i, float f, int i2);

        void d(int i);

        void e(int i);
    }

    /* loaded from: classes.dex */
    public interface j {
        void a(View view, float f);
    }

    /* loaded from: classes.dex */
    public class k extends DataSetObserver {
        public k() {
        }

        @Override // android.database.DataSetObserver
        public void onChanged() {
            ViewPager.this.i();
        }

        @Override // android.database.DataSetObserver
        public void onInvalidated() {
            ViewPager.this.i();
        }
    }

    /* loaded from: classes.dex */
    public static class l implements Comparator<View> {
        @Override // java.util.Comparator
        /* renamed from: a */
        public int compare(View view, View view2) {
            LayoutParams layoutParams = (LayoutParams) view.getLayoutParams();
            LayoutParams layoutParams2 = (LayoutParams) view2.getLayoutParams();
            boolean z = layoutParams.a;
            if (z != layoutParams2.a) {
                return z ? 1 : -1;
            }
            return layoutParams.e - layoutParams2.e;
        }
    }

    public ViewPager(Context context) {
        super(context);
        this.f0 = new ArrayList<>();
        this.g0 = new f();
        this.h0 = new Rect();
        this.k0 = -1;
        this.l0 = null;
        this.m0 = null;
        this.u0 = -3.4028235E38f;
        this.v0 = Float.MAX_VALUE;
        this.A0 = 1;
        this.K0 = -1;
        this.T0 = true;
        this.e1 = new c();
        this.f1 = 0;
        x();
    }

    private int getClientWidth() {
        return (getMeasuredWidth() - getPaddingLeft()) - getPaddingRight();
    }

    private void setScrollingCacheEnabled(boolean z) {
        if (this.y0 != z) {
            this.y0 = z;
        }
    }

    public static boolean y(View view) {
        return view.getClass().getAnnotation(e.class) != null;
    }

    public final boolean A(float f2, float f3) {
        return (f2 < ((float) this.E0) && f3 > Utils.FLOAT_EPSILON) || (f2 > ((float) (getWidth() - this.E0)) && f3 < Utils.FLOAT_EPSILON);
    }

    /* JADX WARN: Removed duplicated region for block: B:22:0x0064  */
    /*
        Code decompiled incorrectly, please refer to instructions dump.
        To view partially-correct code enable 'Show inconsistent code' option in preferences
    */
    public void B(int r13, float r14, int r15) {
        /*
            r12 = this;
            int r0 = r12.V0
            r1 = 0
            r2 = 1
            if (r0 <= 0) goto L6b
            int r0 = r12.getScrollX()
            int r3 = r12.getPaddingLeft()
            int r4 = r12.getPaddingRight()
            int r5 = r12.getWidth()
            int r6 = r12.getChildCount()
            r7 = r1
        L1b:
            if (r7 >= r6) goto L6b
            android.view.View r8 = r12.getChildAt(r7)
            android.view.ViewGroup$LayoutParams r9 = r8.getLayoutParams()
            androidx.viewpager.widget.ViewPager$LayoutParams r9 = (androidx.viewpager.widget.ViewPager.LayoutParams) r9
            boolean r10 = r9.a
            if (r10 != 0) goto L2c
            goto L68
        L2c:
            int r9 = r9.b
            r9 = r9 & 7
            if (r9 == r2) goto L4d
            r10 = 3
            if (r9 == r10) goto L47
            r10 = 5
            if (r9 == r10) goto L3a
            r9 = r3
            goto L5c
        L3a:
            int r9 = r5 - r4
            int r10 = r8.getMeasuredWidth()
            int r9 = r9 - r10
            int r10 = r8.getMeasuredWidth()
            int r4 = r4 + r10
            goto L59
        L47:
            int r9 = r8.getWidth()
            int r9 = r9 + r3
            goto L5c
        L4d:
            int r9 = r8.getMeasuredWidth()
            int r9 = r5 - r9
            int r9 = r9 / 2
            int r9 = java.lang.Math.max(r9, r3)
        L59:
            r11 = r9
            r9 = r3
            r3 = r11
        L5c:
            int r3 = r3 + r0
            int r10 = r8.getLeft()
            int r3 = r3 - r10
            if (r3 == 0) goto L67
            r8.offsetLeftAndRight(r3)
        L67:
            r3 = r9
        L68:
            int r7 = r7 + 1
            goto L1b
        L6b:
            r12.k(r13, r14, r15)
            androidx.viewpager.widget.ViewPager$j r13 = r12.a1
            if (r13 == 0) goto L9f
            int r13 = r12.getScrollX()
            int r14 = r12.getChildCount()
        L7a:
            if (r1 >= r14) goto L9f
            android.view.View r15 = r12.getChildAt(r1)
            android.view.ViewGroup$LayoutParams r0 = r15.getLayoutParams()
            androidx.viewpager.widget.ViewPager$LayoutParams r0 = (androidx.viewpager.widget.ViewPager.LayoutParams) r0
            boolean r0 = r0.a
            if (r0 == 0) goto L8b
            goto L9c
        L8b:
            int r0 = r15.getLeft()
            int r0 = r0 - r13
            float r0 = (float) r0
            int r3 = r12.getClientWidth()
            float r3 = (float) r3
            float r0 = r0 / r3
            androidx.viewpager.widget.ViewPager$j r3 = r12.a1
            r3.a(r15, r0)
        L9c:
            int r1 = r1 + 1
            goto L7a
        L9f:
            r12.U0 = r2
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: androidx.viewpager.widget.ViewPager.B(int, float, int):void");
    }

    public final void C(MotionEvent motionEvent) {
        int actionIndex = motionEvent.getActionIndex();
        if (motionEvent.getPointerId(actionIndex) == this.K0) {
            int i2 = actionIndex == 0 ? 1 : 0;
            this.G0 = motionEvent.getX(i2);
            this.K0 = motionEvent.getPointerId(i2);
            VelocityTracker velocityTracker = this.L0;
            if (velocityTracker != null) {
                velocityTracker.clear();
            }
        }
    }

    public boolean D() {
        int i2 = this.j0;
        if (i2 > 0) {
            setCurrentItem(i2 - 1, true);
            return true;
        }
        return false;
    }

    public boolean E() {
        dp2 dp2Var = this.i0;
        if (dp2Var == null || this.j0 >= dp2Var.e() - 1) {
            return false;
        }
        setCurrentItem(this.j0 + 1, true);
        return true;
    }

    public final boolean F(int i2) {
        if (this.f0.size() == 0) {
            if (this.T0) {
                return false;
            }
            this.U0 = false;
            B(0, Utils.FLOAT_EPSILON, 0);
            if (this.U0) {
                return false;
            }
            throw new IllegalStateException("onPageScrolled did not call superclass implementation");
        }
        f v = v();
        int clientWidth = getClientWidth();
        int i3 = this.q0;
        int i4 = clientWidth + i3;
        float f2 = clientWidth;
        int i5 = v.b;
        float f3 = ((i2 / f2) - v.e) / (v.d + (i3 / f2));
        this.U0 = false;
        B(i5, f3, (int) (i4 * f3));
        if (this.U0) {
            return true;
        }
        throw new IllegalStateException("onPageScrolled did not call superclass implementation");
    }

    public final boolean G(float f2) {
        boolean z;
        boolean z2;
        float f3 = this.G0 - f2;
        this.G0 = f2;
        float scrollX = getScrollX() + f3;
        float clientWidth = getClientWidth();
        float f4 = this.u0 * clientWidth;
        float f5 = this.v0 * clientWidth;
        boolean z3 = false;
        f fVar = this.f0.get(0);
        ArrayList<f> arrayList = this.f0;
        f fVar2 = arrayList.get(arrayList.size() - 1);
        if (fVar.b != 0) {
            f4 = fVar.e * clientWidth;
            z = false;
        } else {
            z = true;
        }
        if (fVar2.b != this.i0.e() - 1) {
            f5 = fVar2.e * clientWidth;
            z2 = false;
        } else {
            z2 = true;
        }
        if (scrollX < f4) {
            if (z) {
                this.R0.onPull(Math.abs(f4 - scrollX) / clientWidth);
                z3 = true;
            }
            scrollX = f4;
        } else if (scrollX > f5) {
            if (z2) {
                this.S0.onPull(Math.abs(scrollX - f5) / clientWidth);
                z3 = true;
            }
            scrollX = f5;
        }
        int i2 = (int) scrollX;
        this.G0 += scrollX - i2;
        scrollTo(i2, getScrollY());
        F(i2);
        return z3;
    }

    public void H() {
        I(this.j0);
    }

    /* JADX WARN: Code restructure failed: missing block: B:24:0x0060, code lost:
        if (r9 == r10) goto L27;
     */
    /* JADX WARN: Code restructure failed: missing block: B:27:0x0066, code lost:
        r8 = null;
     */
    /*
        Code decompiled incorrectly, please refer to instructions dump.
        To view partially-correct code enable 'Show inconsistent code' option in preferences
    */
    public void I(int r18) {
        /*
            Method dump skipped, instructions count: 614
            To view this dump change 'Code comments level' option to 'DEBUG'
        */
        throw new UnsupportedOperationException("Method not decompiled: androidx.viewpager.widget.ViewPager.I(int):void");
    }

    public final void J(int i2, int i3, int i4, int i5) {
        if (i3 > 0 && !this.f0.isEmpty()) {
            if (!this.n0.isFinished()) {
                this.n0.setFinalX(getCurrentItem() * getClientWidth());
                return;
            } else {
                scrollTo((int) ((getScrollX() / (((i3 - getPaddingLeft()) - getPaddingRight()) + i5)) * (((i2 - getPaddingLeft()) - getPaddingRight()) + i4)), getScrollY());
                return;
            }
        }
        f w = w(this.j0);
        int min = (int) ((w != null ? Math.min(w.e, this.v0) : Utils.FLOAT_EPSILON) * ((i2 - getPaddingLeft()) - getPaddingRight()));
        if (min != getScrollX()) {
            h(false);
            scrollTo(min, getScrollY());
        }
    }

    public final void K() {
        int i2 = 0;
        while (i2 < getChildCount()) {
            if (!((LayoutParams) getChildAt(i2).getLayoutParams()).a) {
                removeViewAt(i2);
                i2--;
            }
            i2++;
        }
    }

    public void L(h hVar) {
        List<h> list = this.Z0;
        if (list != null) {
            list.remove(hVar);
        }
    }

    public void M(i iVar) {
        List<i> list = this.W0;
        if (list != null) {
            list.remove(iVar);
        }
    }

    public final void N(boolean z) {
        ViewParent parent = getParent();
        if (parent != null) {
            parent.requestDisallowInterceptTouchEvent(z);
        }
    }

    public final boolean O() {
        this.K0 = -1;
        p();
        this.R0.onRelease();
        this.S0.onRelease();
        return this.R0.isFinished() || this.S0.isFinished();
    }

    public final void P(int i2, boolean z, int i3, boolean z2) {
        f w = w(i2);
        int clientWidth = w != null ? (int) (getClientWidth() * Math.max(this.u0, Math.min(w.e, this.v0))) : 0;
        if (z) {
            T(clientWidth, 0, i3);
            if (z2) {
                l(i2);
                return;
            }
            return;
        }
        if (z2) {
            l(i2);
        }
        h(false);
        scrollTo(clientWidth, 0);
        F(clientWidth);
    }

    public void Q(int i2, boolean z, boolean z2) {
        R(i2, z, z2, 0);
    }

    public void R(int i2, boolean z, boolean z2, int i3) {
        dp2 dp2Var = this.i0;
        if (dp2Var != null && dp2Var.e() > 0) {
            if (!z2 && this.j0 == i2 && this.f0.size() != 0) {
                setScrollingCacheEnabled(false);
                return;
            }
            if (i2 < 0) {
                i2 = 0;
            } else if (i2 >= this.i0.e()) {
                i2 = this.i0.e() - 1;
            }
            int i4 = this.A0;
            int i5 = this.j0;
            if (i2 > i5 + i4 || i2 < i5 - i4) {
                for (int i6 = 0; i6 < this.f0.size(); i6++) {
                    this.f0.get(i6).c = true;
                }
            }
            boolean z3 = this.j0 != i2;
            if (this.T0) {
                this.j0 = i2;
                if (z3) {
                    l(i2);
                }
                requestLayout();
                return;
            }
            I(i2);
            P(i2, z, i3, z3);
            return;
        }
        setScrollingCacheEnabled(false);
    }

    public i S(i iVar) {
        i iVar2 = this.Y0;
        this.Y0 = iVar;
        return iVar2;
    }

    public void T(int i2, int i3, int i4) {
        int scrollX;
        int abs;
        if (getChildCount() == 0) {
            setScrollingCacheEnabled(false);
            return;
        }
        Scroller scroller = this.n0;
        if ((scroller == null || scroller.isFinished()) ? false : true) {
            scrollX = this.o0 ? this.n0.getCurrX() : this.n0.getStartX();
            this.n0.abortAnimation();
            setScrollingCacheEnabled(false);
        } else {
            scrollX = getScrollX();
        }
        int i5 = scrollX;
        int scrollY = getScrollY();
        int i6 = i2 - i5;
        int i7 = i3 - scrollY;
        if (i6 == 0 && i7 == 0) {
            h(false);
            H();
            setScrollState(0);
            return;
        }
        setScrollingCacheEnabled(true);
        setScrollState(2);
        int clientWidth = getClientWidth();
        int i8 = clientWidth / 2;
        float f2 = clientWidth;
        float f3 = i8;
        float n = f3 + (n(Math.min(1.0f, (Math.abs(i6) * 1.0f) / f2)) * f3);
        int abs2 = Math.abs(i4);
        if (abs2 > 0) {
            abs = Math.round(Math.abs(n / abs2) * 1000.0f) * 4;
        } else {
            abs = (int) (((Math.abs(i6) / ((f2 * this.i0.h(this.j0)) + this.q0)) + 1.0f) * 100.0f);
        }
        int min = Math.min(abs, 600);
        this.o0 = false;
        this.n0.startScroll(i5, scrollY, i6, i7, min);
        ei4.j0(this);
    }

    public final void U() {
        if (this.c1 != 0) {
            ArrayList<View> arrayList = this.d1;
            if (arrayList == null) {
                this.d1 = new ArrayList<>();
            } else {
                arrayList.clear();
            }
            int childCount = getChildCount();
            for (int i2 = 0; i2 < childCount; i2++) {
                this.d1.add(getChildAt(i2));
            }
            Collections.sort(this.d1, j1);
        }
    }

    public f a(int i2, int i3) {
        f fVar = new f();
        fVar.b = i2;
        fVar.a = this.i0.j(this, i2);
        fVar.d = this.i0.h(i2);
        if (i3 >= 0 && i3 < this.f0.size()) {
            this.f0.add(i3, fVar);
        } else {
            this.f0.add(fVar);
        }
        return fVar;
    }

    @Override // android.view.ViewGroup, android.view.View
    public void addFocusables(ArrayList<View> arrayList, int i2, int i3) {
        f u;
        int size = arrayList.size();
        int descendantFocusability = getDescendantFocusability();
        if (descendantFocusability != 393216) {
            for (int i4 = 0; i4 < getChildCount(); i4++) {
                View childAt = getChildAt(i4);
                if (childAt.getVisibility() == 0 && (u = u(childAt)) != null && u.b == this.j0) {
                    childAt.addFocusables(arrayList, i2, i3);
                }
            }
        }
        if ((descendantFocusability != 262144 || size == arrayList.size()) && isFocusable()) {
            if ((i3 & 1) == 1 && isInTouchMode() && !isFocusableInTouchMode()) {
                return;
            }
            arrayList.add(this);
        }
    }

    @Override // android.view.ViewGroup, android.view.View
    public void addTouchables(ArrayList<View> arrayList) {
        f u;
        for (int i2 = 0; i2 < getChildCount(); i2++) {
            View childAt = getChildAt(i2);
            if (childAt.getVisibility() == 0 && (u = u(childAt)) != null && u.b == this.j0) {
                childAt.addTouchables(arrayList);
            }
        }
    }

    @Override // android.view.ViewGroup
    public void addView(View view, int i2, ViewGroup.LayoutParams layoutParams) {
        if (!checkLayoutParams(layoutParams)) {
            layoutParams = generateLayoutParams(layoutParams);
        }
        LayoutParams layoutParams2 = (LayoutParams) layoutParams;
        boolean y = layoutParams2.a | y(view);
        layoutParams2.a = y;
        if (!this.x0) {
            super.addView(view, i2, layoutParams);
        } else if (!y) {
            layoutParams2.d = true;
            addViewInLayout(view, i2, layoutParams);
        } else {
            throw new IllegalStateException("Cannot add pager decor view during layout");
        }
    }

    public void b(h hVar) {
        if (this.Z0 == null) {
            this.Z0 = new ArrayList();
        }
        this.Z0.add(hVar);
    }

    public void c(i iVar) {
        if (this.W0 == null) {
            this.W0 = new ArrayList();
        }
        this.W0.add(iVar);
    }

    @Override // android.view.View
    public boolean canScrollHorizontally(int i2) {
        if (this.i0 == null) {
            return false;
        }
        int clientWidth = getClientWidth();
        int scrollX = getScrollX();
        return i2 < 0 ? scrollX > ((int) (((float) clientWidth) * this.u0)) : i2 > 0 && scrollX < ((int) (((float) clientWidth) * this.v0));
    }

    @Override // android.view.ViewGroup
    public boolean checkLayoutParams(ViewGroup.LayoutParams layoutParams) {
        return (layoutParams instanceof LayoutParams) && super.checkLayoutParams(layoutParams);
    }

    @Override // android.view.View
    public void computeScroll() {
        this.o0 = true;
        if (!this.n0.isFinished() && this.n0.computeScrollOffset()) {
            int scrollX = getScrollX();
            int scrollY = getScrollY();
            int currX = this.n0.getCurrX();
            int currY = this.n0.getCurrY();
            if (scrollX != currX || scrollY != currY) {
                scrollTo(currX, currY);
                if (!F(currX)) {
                    this.n0.abortAnimation();
                    scrollTo(0, currY);
                }
            }
            ei4.j0(this);
            return;
        }
        h(true);
    }

    /* JADX WARN: Removed duplicated region for block: B:44:0x00c6  */
    /*
        Code decompiled incorrectly, please refer to instructions dump.
        To view partially-correct code enable 'Show inconsistent code' option in preferences
    */
    public boolean d(int r7) {
        /*
            r6 = this;
            android.view.View r0 = r6.findFocus()
            r1 = 1
            r2 = 0
            r3 = 0
            if (r0 != r6) goto Lb
        L9:
            r0 = r3
            goto L60
        Lb:
            if (r0 == 0) goto L60
            android.view.ViewParent r4 = r0.getParent()
        L11:
            boolean r5 = r4 instanceof android.view.ViewGroup
            if (r5 == 0) goto L1e
            if (r4 != r6) goto L19
            r4 = r1
            goto L1f
        L19:
            android.view.ViewParent r4 = r4.getParent()
            goto L11
        L1e:
            r4 = r2
        L1f:
            if (r4 != 0) goto L60
            java.lang.StringBuilder r4 = new java.lang.StringBuilder
            r4.<init>()
            java.lang.Class r5 = r0.getClass()
            java.lang.String r5 = r5.getSimpleName()
            r4.append(r5)
            android.view.ViewParent r0 = r0.getParent()
        L35:
            boolean r5 = r0 instanceof android.view.ViewGroup
            if (r5 == 0) goto L4e
            java.lang.String r5 = " => "
            r4.append(r5)
            java.lang.Class r5 = r0.getClass()
            java.lang.String r5 = r5.getSimpleName()
            r4.append(r5)
            android.view.ViewParent r0 = r0.getParent()
            goto L35
        L4e:
            java.lang.StringBuilder r0 = new java.lang.StringBuilder
            r0.<init>()
            java.lang.String r5 = "arrowScroll tried to find focus based on non-child current focused view "
            r0.append(r5)
            java.lang.String r4 = r4.toString()
            r0.append(r4)
            goto L9
        L60:
            android.view.FocusFinder r3 = android.view.FocusFinder.getInstance()
            android.view.View r3 = r3.findNextFocus(r6, r0, r7)
            r4 = 66
            r5 = 17
            if (r3 == 0) goto Lb1
            if (r3 == r0) goto Lb1
            if (r7 != r5) goto L91
            android.graphics.Rect r1 = r6.h0
            android.graphics.Rect r1 = r6.s(r1, r3)
            int r1 = r1.left
            android.graphics.Rect r2 = r6.h0
            android.graphics.Rect r2 = r6.s(r2, r0)
            int r2 = r2.left
            if (r0 == 0) goto L8b
            if (r1 < r2) goto L8b
            boolean r0 = r6.D()
            goto L8f
        L8b:
            boolean r0 = r3.requestFocus()
        L8f:
            r2 = r0
            goto Lc4
        L91:
            if (r7 != r4) goto Lc4
            android.graphics.Rect r1 = r6.h0
            android.graphics.Rect r1 = r6.s(r1, r3)
            int r1 = r1.left
            android.graphics.Rect r2 = r6.h0
            android.graphics.Rect r2 = r6.s(r2, r0)
            int r2 = r2.left
            if (r0 == 0) goto Lac
            if (r1 > r2) goto Lac
            boolean r0 = r6.E()
            goto L8f
        Lac:
            boolean r0 = r3.requestFocus()
            goto L8f
        Lb1:
            if (r7 == r5) goto Lc0
            if (r7 != r1) goto Lb6
            goto Lc0
        Lb6:
            if (r7 == r4) goto Lbb
            r0 = 2
            if (r7 != r0) goto Lc4
        Lbb:
            boolean r2 = r6.E()
            goto Lc4
        Lc0:
            boolean r2 = r6.D()
        Lc4:
            if (r2 == 0) goto Lcd
            int r7 = android.view.SoundEffectConstants.getContantForFocusDirection(r7)
            r6.playSoundEffect(r7)
        Lcd:
            return r2
        */
        throw new UnsupportedOperationException("Method not decompiled: androidx.viewpager.widget.ViewPager.d(int):boolean");
    }

    @Override // android.view.ViewGroup, android.view.View
    public boolean dispatchKeyEvent(KeyEvent keyEvent) {
        return super.dispatchKeyEvent(keyEvent) || r(keyEvent);
    }

    @Override // android.view.View
    public boolean dispatchPopulateAccessibilityEvent(AccessibilityEvent accessibilityEvent) {
        f u;
        if (accessibilityEvent.getEventType() == 4096) {
            return super.dispatchPopulateAccessibilityEvent(accessibilityEvent);
        }
        int childCount = getChildCount();
        for (int i2 = 0; i2 < childCount; i2++) {
            View childAt = getChildAt(i2);
            if (childAt.getVisibility() == 0 && (u = u(childAt)) != null && u.b == this.j0 && childAt.dispatchPopulateAccessibilityEvent(accessibilityEvent)) {
                return true;
            }
        }
        return false;
    }

    @Override // android.view.View
    public void draw(Canvas canvas) {
        dp2 dp2Var;
        super.draw(canvas);
        int overScrollMode = getOverScrollMode();
        boolean z = false;
        if (overScrollMode != 0 && (overScrollMode != 1 || (dp2Var = this.i0) == null || dp2Var.e() <= 1)) {
            this.R0.finish();
            this.S0.finish();
        } else {
            if (!this.R0.isFinished()) {
                int save = canvas.save();
                int height = (getHeight() - getPaddingTop()) - getPaddingBottom();
                int width = getWidth();
                canvas.rotate(270.0f);
                canvas.translate((-height) + getPaddingTop(), this.u0 * width);
                this.R0.setSize(height, width);
                z = false | this.R0.draw(canvas);
                canvas.restoreToCount(save);
            }
            if (!this.S0.isFinished()) {
                int save2 = canvas.save();
                int width2 = getWidth();
                int height2 = (getHeight() - getPaddingTop()) - getPaddingBottom();
                canvas.rotate(90.0f);
                canvas.translate(-getPaddingTop(), (-(this.v0 + 1.0f)) * width2);
                this.S0.setSize(height2, width2);
                z |= this.S0.draw(canvas);
                canvas.restoreToCount(save2);
            }
        }
        if (z) {
            ei4.j0(this);
        }
    }

    @Override // android.view.ViewGroup, android.view.View
    public void drawableStateChanged() {
        super.drawableStateChanged();
        Drawable drawable = this.r0;
        if (drawable == null || !drawable.isStateful()) {
            return;
        }
        drawable.setState(getDrawableState());
    }

    public boolean e() {
        if (this.B0) {
            return false;
        }
        this.Q0 = true;
        setScrollState(1);
        this.G0 = Utils.FLOAT_EPSILON;
        this.I0 = Utils.FLOAT_EPSILON;
        VelocityTracker velocityTracker = this.L0;
        if (velocityTracker == null) {
            this.L0 = VelocityTracker.obtain();
        } else {
            velocityTracker.clear();
        }
        long uptimeMillis = SystemClock.uptimeMillis();
        MotionEvent obtain = MotionEvent.obtain(uptimeMillis, uptimeMillis, 0, Utils.FLOAT_EPSILON, Utils.FLOAT_EPSILON, 0);
        this.L0.addMovement(obtain);
        obtain.recycle();
        return true;
    }

    public final void f(f fVar, int i2, f fVar2) {
        int i3;
        int i4;
        f fVar3;
        f fVar4;
        int e2 = this.i0.e();
        int clientWidth = getClientWidth();
        float f2 = clientWidth > 0 ? this.q0 / clientWidth : Utils.FLOAT_EPSILON;
        if (fVar2 != null) {
            int i5 = fVar2.b;
            int i6 = fVar.b;
            if (i5 < i6) {
                int i7 = 0;
                float f3 = fVar2.e + fVar2.d + f2;
                while (true) {
                    i5++;
                    if (i5 > fVar.b || i7 >= this.f0.size()) {
                        break;
                    }
                    f fVar5 = this.f0.get(i7);
                    while (true) {
                        fVar4 = fVar5;
                        if (i5 <= fVar4.b || i7 >= this.f0.size() - 1) {
                            break;
                        }
                        i7++;
                        fVar5 = this.f0.get(i7);
                    }
                    while (i5 < fVar4.b) {
                        f3 += this.i0.h(i5) + f2;
                        i5++;
                    }
                    fVar4.e = f3;
                    f3 += fVar4.d + f2;
                }
            } else if (i5 > i6) {
                int size = this.f0.size() - 1;
                float f4 = fVar2.e;
                while (true) {
                    i5--;
                    if (i5 < fVar.b || size < 0) {
                        break;
                    }
                    f fVar6 = this.f0.get(size);
                    while (true) {
                        fVar3 = fVar6;
                        if (i5 >= fVar3.b || size <= 0) {
                            break;
                        }
                        size--;
                        fVar6 = this.f0.get(size);
                    }
                    while (i5 > fVar3.b) {
                        f4 -= this.i0.h(i5) + f2;
                        i5--;
                    }
                    f4 -= fVar3.d + f2;
                    fVar3.e = f4;
                }
            }
        }
        int size2 = this.f0.size();
        float f5 = fVar.e;
        int i8 = fVar.b;
        int i9 = i8 - 1;
        this.u0 = i8 == 0 ? f5 : -3.4028235E38f;
        int i10 = e2 - 1;
        this.v0 = i8 == i10 ? (fVar.d + f5) - 1.0f : Float.MAX_VALUE;
        int i11 = i2 - 1;
        while (i11 >= 0) {
            f fVar7 = this.f0.get(i11);
            while (true) {
                i4 = fVar7.b;
                if (i9 <= i4) {
                    break;
                }
                f5 -= this.i0.h(i9) + f2;
                i9--;
            }
            f5 -= fVar7.d + f2;
            fVar7.e = f5;
            if (i4 == 0) {
                this.u0 = f5;
            }
            i11--;
            i9--;
        }
        float f6 = fVar.e + fVar.d + f2;
        int i12 = fVar.b + 1;
        int i13 = i2 + 1;
        while (i13 < size2) {
            f fVar8 = this.f0.get(i13);
            while (true) {
                i3 = fVar8.b;
                if (i12 >= i3) {
                    break;
                }
                f6 += this.i0.h(i12) + f2;
                i12++;
            }
            if (i3 == i10) {
                this.v0 = (fVar8.d + f6) - 1.0f;
            }
            fVar8.e = f6;
            f6 += fVar8.d + f2;
            i13++;
            i12++;
        }
    }

    public boolean g(View view, boolean z, int i2, int i3, int i4) {
        int i5;
        if (view instanceof ViewGroup) {
            ViewGroup viewGroup = (ViewGroup) view;
            int scrollX = view.getScrollX();
            int scrollY = view.getScrollY();
            for (int childCount = viewGroup.getChildCount() - 1; childCount >= 0; childCount--) {
                View childAt = viewGroup.getChildAt(childCount);
                int i6 = i3 + scrollX;
                if (i6 >= childAt.getLeft() && i6 < childAt.getRight() && (i5 = i4 + scrollY) >= childAt.getTop() && i5 < childAt.getBottom() && g(childAt, true, i2, i6 - childAt.getLeft(), i5 - childAt.getTop())) {
                    return true;
                }
            }
        }
        return z && view.canScrollHorizontally(-i2);
    }

    @Override // android.view.ViewGroup
    public ViewGroup.LayoutParams generateDefaultLayoutParams() {
        return new LayoutParams();
    }

    @Override // android.view.ViewGroup
    public ViewGroup.LayoutParams generateLayoutParams(ViewGroup.LayoutParams layoutParams) {
        return generateDefaultLayoutParams();
    }

    public dp2 getAdapter() {
        return this.i0;
    }

    @Override // android.view.ViewGroup
    public int getChildDrawingOrder(int i2, int i3) {
        if (this.c1 == 2) {
            i3 = (i2 - 1) - i3;
        }
        return ((LayoutParams) this.d1.get(i3).getLayoutParams()).f;
    }

    public int getCurrentItem() {
        return this.j0;
    }

    public int getOffscreenPageLimit() {
        return this.A0;
    }

    public int getPageMargin() {
        return this.q0;
    }

    public final void h(boolean z) {
        boolean z2 = this.f1 == 2;
        if (z2) {
            setScrollingCacheEnabled(false);
            if (!this.n0.isFinished()) {
                this.n0.abortAnimation();
                int scrollX = getScrollX();
                int scrollY = getScrollY();
                int currX = this.n0.getCurrX();
                int currY = this.n0.getCurrY();
                if (scrollX != currX || scrollY != currY) {
                    scrollTo(currX, currY);
                    if (currX != scrollX) {
                        F(currX);
                    }
                }
            }
        }
        this.z0 = false;
        for (int i2 = 0; i2 < this.f0.size(); i2++) {
            f fVar = this.f0.get(i2);
            if (fVar.c) {
                fVar.c = false;
                z2 = true;
            }
        }
        if (z2) {
            if (z) {
                ei4.l0(this, this.e1);
            } else {
                this.e1.run();
            }
        }
    }

    public void i() {
        int e2 = this.i0.e();
        this.a = e2;
        boolean z = this.f0.size() < (this.A0 * 2) + 1 && this.f0.size() < e2;
        int i2 = this.j0;
        int i3 = 0;
        boolean z2 = false;
        while (i3 < this.f0.size()) {
            f fVar = this.f0.get(i3);
            int f2 = this.i0.f(fVar.a);
            if (f2 != -1) {
                if (f2 == -2) {
                    this.f0.remove(i3);
                    i3--;
                    if (!z2) {
                        this.i0.s(this);
                        z2 = true;
                    }
                    this.i0.b(this, fVar.b, fVar.a);
                    int i4 = this.j0;
                    if (i4 == fVar.b) {
                        i2 = Math.max(0, Math.min(i4, e2 - 1));
                    }
                } else {
                    int i5 = fVar.b;
                    if (i5 != f2) {
                        if (i5 == this.j0) {
                            i2 = f2;
                        }
                        fVar.b = f2;
                    }
                }
                z = true;
            }
            i3++;
        }
        if (z2) {
            this.i0.d(this);
        }
        Collections.sort(this.f0, h1);
        if (z) {
            int childCount = getChildCount();
            for (int i6 = 0; i6 < childCount; i6++) {
                LayoutParams layoutParams = (LayoutParams) getChildAt(i6).getLayoutParams();
                if (!layoutParams.a) {
                    layoutParams.c = Utils.FLOAT_EPSILON;
                }
            }
            Q(i2, false, true);
            requestLayout();
        }
    }

    public final int j(int i2, float f2, int i3, int i4) {
        if (Math.abs(i4) <= this.O0 || Math.abs(i3) <= this.M0) {
            i2 += (int) (f2 + (i2 >= this.j0 ? 0.4f : 0.6f));
        } else if (i3 <= 0) {
            i2++;
        }
        if (this.f0.size() > 0) {
            ArrayList<f> arrayList = this.f0;
            return Math.max(this.f0.get(0).b, Math.min(i2, arrayList.get(arrayList.size() - 1).b));
        }
        return i2;
    }

    public final void k(int i2, float f2, int i3) {
        i iVar = this.X0;
        if (iVar != null) {
            iVar.a(i2, f2, i3);
        }
        List<i> list = this.W0;
        if (list != null) {
            int size = list.size();
            for (int i4 = 0; i4 < size; i4++) {
                i iVar2 = this.W0.get(i4);
                if (iVar2 != null) {
                    iVar2.a(i2, f2, i3);
                }
            }
        }
        i iVar3 = this.Y0;
        if (iVar3 != null) {
            iVar3.a(i2, f2, i3);
        }
    }

    public final void l(int i2) {
        i iVar = this.X0;
        if (iVar != null) {
            iVar.e(i2);
        }
        List<i> list = this.W0;
        if (list != null) {
            int size = list.size();
            for (int i3 = 0; i3 < size; i3++) {
                i iVar2 = this.W0.get(i3);
                if (iVar2 != null) {
                    iVar2.e(i2);
                }
            }
        }
        i iVar3 = this.Y0;
        if (iVar3 != null) {
            iVar3.e(i2);
        }
    }

    public final void m(int i2) {
        i iVar = this.X0;
        if (iVar != null) {
            iVar.d(i2);
        }
        List<i> list = this.W0;
        if (list != null) {
            int size = list.size();
            for (int i3 = 0; i3 < size; i3++) {
                i iVar2 = this.W0.get(i3);
                if (iVar2 != null) {
                    iVar2.d(i2);
                }
            }
        }
        i iVar3 = this.Y0;
        if (iVar3 != null) {
            iVar3.d(i2);
        }
    }

    public float n(float f2) {
        return (float) Math.sin((f2 - 0.5f) * 0.47123894f);
    }

    public final void o(boolean z) {
        int childCount = getChildCount();
        for (int i2 = 0; i2 < childCount; i2++) {
            getChildAt(i2).setLayerType(z ? this.b1 : 0, null);
        }
    }

    @Override // android.view.ViewGroup, android.view.View
    public void onAttachedToWindow() {
        super.onAttachedToWindow();
        this.T0 = true;
    }

    @Override // android.view.ViewGroup, android.view.View
    public void onDetachedFromWindow() {
        removeCallbacks(this.e1);
        Scroller scroller = this.n0;
        if (scroller != null && !scroller.isFinished()) {
            this.n0.abortAnimation();
        }
        super.onDetachedFromWindow();
    }

    @Override // android.view.View
    public void onDraw(Canvas canvas) {
        int width;
        int i2;
        float f2;
        float f3;
        super.onDraw(canvas);
        if (this.q0 <= 0 || this.r0 == null || this.f0.size() <= 0 || this.i0 == null) {
            return;
        }
        int scrollX = getScrollX();
        float width2 = getWidth();
        float f4 = this.q0 / width2;
        int i3 = 0;
        f fVar = this.f0.get(0);
        float f5 = fVar.e;
        int size = this.f0.size();
        int i4 = fVar.b;
        int i5 = this.f0.get(size - 1).b;
        while (i4 < i5) {
            while (true) {
                i2 = fVar.b;
                if (i4 <= i2 || i3 >= size) {
                    break;
                }
                i3++;
                fVar = this.f0.get(i3);
            }
            if (i4 == i2) {
                float f6 = fVar.e;
                float f7 = fVar.d;
                f2 = (f6 + f7) * width2;
                f5 = f6 + f7 + f4;
            } else {
                float h2 = this.i0.h(i4);
                f2 = (f5 + h2) * width2;
                f5 += h2 + f4;
            }
            if (this.q0 + f2 > scrollX) {
                f3 = f4;
                this.r0.setBounds(Math.round(f2), this.s0, Math.round(this.q0 + f2), this.t0);
                this.r0.draw(canvas);
            } else {
                f3 = f4;
            }
            if (f2 > scrollX + width) {
                return;
            }
            i4++;
            f4 = f3;
        }
    }

    @Override // android.view.ViewGroup
    public boolean onInterceptTouchEvent(MotionEvent motionEvent) {
        int action = motionEvent.getAction() & 255;
        if (action != 3 && action != 1) {
            if (action != 0) {
                if (this.B0) {
                    return true;
                }
                if (this.C0) {
                    return false;
                }
            }
            if (action == 0) {
                float x = motionEvent.getX();
                this.I0 = x;
                this.G0 = x;
                float y = motionEvent.getY();
                this.J0 = y;
                this.H0 = y;
                this.K0 = motionEvent.getPointerId(0);
                this.C0 = false;
                this.o0 = true;
                this.n0.computeScrollOffset();
                if (this.f1 == 2 && Math.abs(this.n0.getFinalX() - this.n0.getCurrX()) > this.P0) {
                    this.n0.abortAnimation();
                    this.z0 = false;
                    H();
                    this.B0 = true;
                    N(true);
                    setScrollState(1);
                } else {
                    h(false);
                    this.B0 = false;
                }
            } else if (action == 2) {
                int i2 = this.K0;
                if (i2 != -1) {
                    int findPointerIndex = motionEvent.findPointerIndex(i2);
                    float x2 = motionEvent.getX(findPointerIndex);
                    float f2 = x2 - this.G0;
                    float abs = Math.abs(f2);
                    float y2 = motionEvent.getY(findPointerIndex);
                    float abs2 = Math.abs(y2 - this.J0);
                    int i3 = (f2 > Utils.FLOAT_EPSILON ? 1 : (f2 == Utils.FLOAT_EPSILON ? 0 : -1));
                    if (i3 != 0 && !A(this.G0, f2) && g(this, false, (int) f2, (int) x2, (int) y2)) {
                        this.G0 = x2;
                        this.H0 = y2;
                        this.C0 = true;
                        return false;
                    }
                    int i4 = this.F0;
                    if (abs > i4 && abs * 0.5f > abs2) {
                        this.B0 = true;
                        N(true);
                        setScrollState(1);
                        float f3 = this.I0;
                        float f4 = this.F0;
                        this.G0 = i3 > 0 ? f3 + f4 : f3 - f4;
                        this.H0 = y2;
                        setScrollingCacheEnabled(true);
                    } else if (abs2 > i4) {
                        this.C0 = true;
                    }
                    if (this.B0 && G(x2)) {
                        ei4.j0(this);
                    }
                }
            } else if (action == 6) {
                C(motionEvent);
            }
            if (this.L0 == null) {
                this.L0 = VelocityTracker.obtain();
            }
            this.L0.addMovement(motionEvent);
            return this.B0;
        }
        O();
        return false;
    }

    /* JADX WARN: Removed duplicated region for block: B:22:0x0071  */
    /* JADX WARN: Removed duplicated region for block: B:29:0x008e  */
    @Override // android.view.ViewGroup, android.view.View
    /*
        Code decompiled incorrectly, please refer to instructions dump.
        To view partially-correct code enable 'Show inconsistent code' option in preferences
    */
    public void onLayout(boolean r19, int r20, int r21, int r22, int r23) {
        /*
            Method dump skipped, instructions count: 284
            To view this dump change 'Code comments level' option to 'DEBUG'
        */
        throw new UnsupportedOperationException("Method not decompiled: androidx.viewpager.widget.ViewPager.onLayout(boolean, int, int, int, int):void");
    }

    @Override // android.view.View
    public void onMeasure(int i2, int i3) {
        LayoutParams layoutParams;
        LayoutParams layoutParams2;
        int i4;
        setMeasuredDimension(ViewGroup.getDefaultSize(0, i2), ViewGroup.getDefaultSize(0, i3));
        int measuredWidth = getMeasuredWidth();
        this.E0 = Math.min(measuredWidth / 10, this.D0);
        int paddingLeft = (measuredWidth - getPaddingLeft()) - getPaddingRight();
        int measuredHeight = (getMeasuredHeight() - getPaddingTop()) - getPaddingBottom();
        int childCount = getChildCount();
        int i5 = 0;
        while (true) {
            boolean z = true;
            int i6 = 1073741824;
            if (i5 >= childCount) {
                break;
            }
            View childAt = getChildAt(i5);
            if (childAt.getVisibility() != 8 && (layoutParams2 = (LayoutParams) childAt.getLayoutParams()) != null && layoutParams2.a) {
                int i7 = layoutParams2.b;
                int i8 = i7 & 7;
                int i9 = i7 & 112;
                boolean z2 = i9 == 48 || i9 == 80;
                if (i8 != 3 && i8 != 5) {
                    z = false;
                }
                int i10 = Integer.MIN_VALUE;
                if (z2) {
                    i4 = Integer.MIN_VALUE;
                    i10 = 1073741824;
                } else {
                    i4 = z ? 1073741824 : Integer.MIN_VALUE;
                }
                int i11 = ((ViewGroup.LayoutParams) layoutParams2).width;
                if (i11 != -2) {
                    if (i11 == -1) {
                        i11 = paddingLeft;
                    }
                    i10 = 1073741824;
                } else {
                    i11 = paddingLeft;
                }
                int i12 = ((ViewGroup.LayoutParams) layoutParams2).height;
                if (i12 == -2) {
                    i12 = measuredHeight;
                    i6 = i4;
                } else if (i12 == -1) {
                    i12 = measuredHeight;
                }
                childAt.measure(View.MeasureSpec.makeMeasureSpec(i11, i10), View.MeasureSpec.makeMeasureSpec(i12, i6));
                if (z2) {
                    measuredHeight -= childAt.getMeasuredHeight();
                } else if (z) {
                    paddingLeft -= childAt.getMeasuredWidth();
                }
            }
            i5++;
        }
        View.MeasureSpec.makeMeasureSpec(paddingLeft, 1073741824);
        this.w0 = View.MeasureSpec.makeMeasureSpec(measuredHeight, 1073741824);
        this.x0 = true;
        H();
        this.x0 = false;
        int childCount2 = getChildCount();
        for (int i13 = 0; i13 < childCount2; i13++) {
            View childAt2 = getChildAt(i13);
            if (childAt2.getVisibility() != 8 && ((layoutParams = (LayoutParams) childAt2.getLayoutParams()) == null || !layoutParams.a)) {
                childAt2.measure(View.MeasureSpec.makeMeasureSpec((int) (paddingLeft * layoutParams.c), 1073741824), this.w0);
            }
        }
    }

    @Override // android.view.ViewGroup
    public boolean onRequestFocusInDescendants(int i2, Rect rect) {
        int i3;
        int i4;
        f u;
        int childCount = getChildCount();
        int i5 = -1;
        if ((i2 & 2) != 0) {
            i5 = childCount;
            i3 = 0;
            i4 = 1;
        } else {
            i3 = childCount - 1;
            i4 = -1;
        }
        while (i3 != i5) {
            View childAt = getChildAt(i3);
            if (childAt.getVisibility() == 0 && (u = u(childAt)) != null && u.b == this.j0 && childAt.requestFocus(i2, rect)) {
                return true;
            }
            i3 += i4;
        }
        return false;
    }

    @Override // android.view.View
    public void onRestoreInstanceState(Parcelable parcelable) {
        if (!(parcelable instanceof SavedState)) {
            super.onRestoreInstanceState(parcelable);
            return;
        }
        SavedState savedState = (SavedState) parcelable;
        super.onRestoreInstanceState(savedState.a());
        dp2 dp2Var = this.i0;
        if (dp2Var != null) {
            dp2Var.m(savedState.h0, savedState.i0);
            Q(savedState.g0, false, true);
            return;
        }
        this.k0 = savedState.g0;
        this.l0 = savedState.h0;
        this.m0 = savedState.i0;
    }

    @Override // android.view.View
    public Parcelable onSaveInstanceState() {
        SavedState savedState = new SavedState(super.onSaveInstanceState());
        savedState.g0 = this.j0;
        dp2 dp2Var = this.i0;
        if (dp2Var != null) {
            savedState.h0 = dp2Var.n();
        }
        return savedState;
    }

    @Override // android.view.View
    public void onSizeChanged(int i2, int i3, int i4, int i5) {
        super.onSizeChanged(i2, i3, i4, i5);
        if (i2 != i4) {
            int i6 = this.q0;
            J(i2, i4, i6, i6);
        }
    }

    @Override // android.view.View
    public boolean onTouchEvent(MotionEvent motionEvent) {
        dp2 dp2Var;
        if (this.Q0) {
            return true;
        }
        boolean z = false;
        if ((motionEvent.getAction() == 0 && motionEvent.getEdgeFlags() != 0) || (dp2Var = this.i0) == null || dp2Var.e() == 0) {
            return false;
        }
        if (this.L0 == null) {
            this.L0 = VelocityTracker.obtain();
        }
        this.L0.addMovement(motionEvent);
        int action = motionEvent.getAction() & 255;
        if (action == 0) {
            this.n0.abortAnimation();
            this.z0 = false;
            H();
            float x = motionEvent.getX();
            this.I0 = x;
            this.G0 = x;
            float y = motionEvent.getY();
            this.J0 = y;
            this.H0 = y;
            this.K0 = motionEvent.getPointerId(0);
        } else if (action != 1) {
            if (action == 2) {
                if (!this.B0) {
                    int findPointerIndex = motionEvent.findPointerIndex(this.K0);
                    if (findPointerIndex == -1) {
                        z = O();
                    } else {
                        float x2 = motionEvent.getX(findPointerIndex);
                        float abs = Math.abs(x2 - this.G0);
                        float y2 = motionEvent.getY(findPointerIndex);
                        float abs2 = Math.abs(y2 - this.H0);
                        if (abs > this.F0 && abs > abs2) {
                            this.B0 = true;
                            N(true);
                            float f2 = this.I0;
                            this.G0 = x2 - f2 > Utils.FLOAT_EPSILON ? f2 + this.F0 : f2 - this.F0;
                            this.H0 = y2;
                            setScrollState(1);
                            setScrollingCacheEnabled(true);
                            ViewParent parent = getParent();
                            if (parent != null) {
                                parent.requestDisallowInterceptTouchEvent(true);
                            }
                        }
                    }
                }
                if (this.B0) {
                    z = false | G(motionEvent.getX(motionEvent.findPointerIndex(this.K0)));
                }
            } else if (action != 3) {
                if (action == 5) {
                    int actionIndex = motionEvent.getActionIndex();
                    this.G0 = motionEvent.getX(actionIndex);
                    this.K0 = motionEvent.getPointerId(actionIndex);
                } else if (action == 6) {
                    C(motionEvent);
                    this.G0 = motionEvent.getX(motionEvent.findPointerIndex(this.K0));
                }
            } else if (this.B0) {
                P(this.j0, true, 0, false);
                z = O();
            }
        } else if (this.B0) {
            VelocityTracker velocityTracker = this.L0;
            velocityTracker.computeCurrentVelocity(1000, this.N0);
            int xVelocity = (int) velocityTracker.getXVelocity(this.K0);
            this.z0 = true;
            int clientWidth = getClientWidth();
            int scrollX = getScrollX();
            f v = v();
            float f3 = clientWidth;
            R(j(v.b, ((scrollX / f3) - v.e) / (v.d + (this.q0 / f3)), xVelocity, (int) (motionEvent.getX(motionEvent.findPointerIndex(this.K0)) - this.I0)), true, true, xVelocity);
            z = O();
        }
        if (z) {
            ei4.j0(this);
        }
        return true;
    }

    public final void p() {
        this.B0 = false;
        this.C0 = false;
        VelocityTracker velocityTracker = this.L0;
        if (velocityTracker != null) {
            velocityTracker.recycle();
            this.L0 = null;
        }
    }

    public void q() {
        if (this.Q0) {
            if (this.i0 != null) {
                VelocityTracker velocityTracker = this.L0;
                velocityTracker.computeCurrentVelocity(1000, this.N0);
                int xVelocity = (int) velocityTracker.getXVelocity(this.K0);
                this.z0 = true;
                int clientWidth = getClientWidth();
                int scrollX = getScrollX();
                f v = v();
                R(j(v.b, ((scrollX / clientWidth) - v.e) / v.d, xVelocity, (int) (this.G0 - this.I0)), true, true, xVelocity);
            }
            p();
            this.Q0 = false;
            return;
        }
        throw new IllegalStateException("No fake drag in progress. Call beginFakeDrag first.");
    }

    public boolean r(KeyEvent keyEvent) {
        if (keyEvent.getAction() == 0) {
            int keyCode = keyEvent.getKeyCode();
            if (keyCode == 21) {
                if (keyEvent.hasModifiers(2)) {
                    return D();
                }
                return d(17);
            } else if (keyCode == 22) {
                if (keyEvent.hasModifiers(2)) {
                    return E();
                }
                return d(66);
            } else if (keyCode == 61) {
                if (keyEvent.hasNoModifiers()) {
                    return d(2);
                }
                if (keyEvent.hasModifiers(1)) {
                    return d(1);
                }
            }
        }
        return false;
    }

    @Override // android.view.ViewGroup, android.view.ViewManager
    public void removeView(View view) {
        if (this.x0) {
            removeViewInLayout(view);
        } else {
            super.removeView(view);
        }
    }

    public final Rect s(Rect rect, View view) {
        if (rect == null) {
            rect = new Rect();
        }
        if (view == null) {
            rect.set(0, 0, 0, 0);
            return rect;
        }
        rect.left = view.getLeft();
        rect.right = view.getRight();
        rect.top = view.getTop();
        rect.bottom = view.getBottom();
        ViewParent parent = view.getParent();
        while ((parent instanceof ViewGroup) && parent != this) {
            ViewGroup viewGroup = (ViewGroup) parent;
            rect.left += viewGroup.getLeft();
            rect.right += viewGroup.getRight();
            rect.top += viewGroup.getTop();
            rect.bottom += viewGroup.getBottom();
            parent = viewGroup.getParent();
        }
        return rect;
    }

    public void setAdapter(dp2 dp2Var) {
        dp2 dp2Var2 = this.i0;
        if (dp2Var2 != null) {
            dp2Var2.q(null);
            this.i0.s(this);
            for (int i2 = 0; i2 < this.f0.size(); i2++) {
                f fVar = this.f0.get(i2);
                this.i0.b(this, fVar.b, fVar.a);
            }
            this.i0.d(this);
            this.f0.clear();
            K();
            this.j0 = 0;
            scrollTo(0, 0);
        }
        dp2 dp2Var3 = this.i0;
        this.i0 = dp2Var;
        this.a = 0;
        if (dp2Var != null) {
            if (this.p0 == null) {
                this.p0 = new k();
            }
            this.i0.q(this.p0);
            this.z0 = false;
            boolean z = this.T0;
            this.T0 = true;
            this.a = this.i0.e();
            if (this.k0 >= 0) {
                this.i0.m(this.l0, this.m0);
                Q(this.k0, false, true);
                this.k0 = -1;
                this.l0 = null;
                this.m0 = null;
            } else if (!z) {
                H();
            } else {
                requestLayout();
            }
        }
        List<h> list = this.Z0;
        if (list == null || list.isEmpty()) {
            return;
        }
        int size = this.Z0.size();
        for (int i3 = 0; i3 < size; i3++) {
            this.Z0.get(i3).b(this, dp2Var3, dp2Var);
        }
    }

    public void setCurrentItem(int i2) {
        this.z0 = false;
        Q(i2, !this.T0, false);
    }

    public void setOffscreenPageLimit(int i2) {
        if (i2 < 1) {
            StringBuilder sb = new StringBuilder();
            sb.append("Requested offscreen page limit ");
            sb.append(i2);
            sb.append(" too small; defaulting to ");
            sb.append(1);
            i2 = 1;
        }
        if (i2 != this.A0) {
            this.A0 = i2;
            H();
        }
    }

    @Deprecated
    public void setOnPageChangeListener(i iVar) {
        this.X0 = iVar;
    }

    public void setPageMargin(int i2) {
        int i3 = this.q0;
        this.q0 = i2;
        int width = getWidth();
        J(width, width, i2, i3);
        requestLayout();
    }

    public void setPageMarginDrawable(Drawable drawable) {
        this.r0 = drawable;
        if (drawable != null) {
            refreshDrawableState();
        }
        setWillNotDraw(drawable == null);
        invalidate();
    }

    public void setPageTransformer(boolean z, j jVar) {
        setPageTransformer(z, jVar, 2);
    }

    public void setScrollState(int i2) {
        if (this.f1 == i2) {
            return;
        }
        this.f1 = i2;
        if (this.a1 != null) {
            o(i2 != 0);
        }
        m(i2);
    }

    public f t(View view) {
        while (true) {
            ViewParent parent = view.getParent();
            if (parent != this) {
                if (parent == null || !(parent instanceof View)) {
                    return null;
                }
                view = (View) parent;
            } else {
                return u(view);
            }
        }
    }

    public f u(View view) {
        for (int i2 = 0; i2 < this.f0.size(); i2++) {
            f fVar = this.f0.get(i2);
            if (this.i0.k(view, fVar.a)) {
                return fVar;
            }
        }
        return null;
    }

    public final f v() {
        int i2;
        int clientWidth = getClientWidth();
        float f2 = Utils.FLOAT_EPSILON;
        float scrollX = clientWidth > 0 ? getScrollX() / clientWidth : 0.0f;
        float f3 = clientWidth > 0 ? this.q0 / clientWidth : 0.0f;
        f fVar = null;
        int i3 = 0;
        int i4 = -1;
        boolean z = true;
        float f4 = 0.0f;
        while (i3 < this.f0.size()) {
            f fVar2 = this.f0.get(i3);
            if (!z && fVar2.b != (i2 = i4 + 1)) {
                fVar2 = this.g0;
                fVar2.e = f2 + f4 + f3;
                fVar2.b = i2;
                fVar2.d = this.i0.h(i2);
                i3--;
            }
            f2 = fVar2.e;
            float f5 = fVar2.d + f2 + f3;
            if (!z && scrollX < f2) {
                return fVar;
            }
            if (scrollX < f5 || i3 == this.f0.size() - 1) {
                return fVar2;
            }
            i4 = fVar2.b;
            f4 = fVar2.d;
            i3++;
            z = false;
            fVar = fVar2;
        }
        return fVar;
    }

    @Override // android.view.View
    public boolean verifyDrawable(Drawable drawable) {
        return super.verifyDrawable(drawable) || drawable == this.r0;
    }

    public f w(int i2) {
        for (int i3 = 0; i3 < this.f0.size(); i3++) {
            f fVar = this.f0.get(i3);
            if (fVar.b == i2) {
                return fVar;
            }
        }
        return null;
    }

    public void x() {
        setWillNotDraw(false);
        setDescendantFocusability(262144);
        setFocusable(true);
        Context context = getContext();
        this.n0 = new Scroller(context, i1);
        ViewConfiguration viewConfiguration = ViewConfiguration.get(context);
        float f2 = context.getResources().getDisplayMetrics().density;
        this.F0 = viewConfiguration.getScaledPagingTouchSlop();
        this.M0 = (int) (400.0f * f2);
        this.N0 = viewConfiguration.getScaledMaximumFlingVelocity();
        this.R0 = new EdgeEffect(context);
        this.S0 = new EdgeEffect(context);
        this.O0 = (int) (25.0f * f2);
        this.P0 = (int) (2.0f * f2);
        this.D0 = (int) (f2 * 16.0f);
        ei4.t0(this, new g());
        if (ei4.C(this) == 0) {
            ei4.D0(this, 1);
        }
        ei4.G0(this, new d());
    }

    public boolean z() {
        return this.Q0;
    }

    /* loaded from: classes.dex */
    public static class LayoutParams extends ViewGroup.LayoutParams {
        public boolean a;
        public int b;
        public float c;
        public boolean d;
        public int e;
        public int f;

        public LayoutParams() {
            super(-1, -1);
            this.c = Utils.FLOAT_EPSILON;
        }

        public LayoutParams(Context context, AttributeSet attributeSet) {
            super(context, attributeSet);
            this.c = Utils.FLOAT_EPSILON;
            TypedArray obtainStyledAttributes = context.obtainStyledAttributes(attributeSet, ViewPager.g1);
            this.b = obtainStyledAttributes.getInteger(0, 48);
            obtainStyledAttributes.recycle();
        }
    }

    @Override // android.view.ViewGroup
    public ViewGroup.LayoutParams generateLayoutParams(AttributeSet attributeSet) {
        return new LayoutParams(getContext(), attributeSet);
    }

    public void setPageTransformer(boolean z, j jVar, int i2) {
        boolean z2 = jVar != null;
        boolean z3 = z2 != (this.a1 != null);
        this.a1 = jVar;
        setChildrenDrawingOrderEnabled(z2);
        if (z2) {
            this.c1 = z ? 2 : 1;
            this.b1 = i2;
        } else {
            this.c1 = 0;
        }
        if (z3) {
            H();
        }
    }

    public void setCurrentItem(int i2, boolean z) {
        this.z0 = false;
        Q(i2, z, false);
    }

    public void setPageMarginDrawable(int i2) {
        setPageMarginDrawable(m70.f(getContext(), i2));
    }

    public ViewPager(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        this.f0 = new ArrayList<>();
        this.g0 = new f();
        this.h0 = new Rect();
        this.k0 = -1;
        this.l0 = null;
        this.m0 = null;
        this.u0 = -3.4028235E38f;
        this.v0 = Float.MAX_VALUE;
        this.A0 = 1;
        this.K0 = -1;
        this.T0 = true;
        this.e1 = new c();
        this.f1 = 0;
        x();
    }
}
