package androidx.viewpager.widget;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Rect;
import android.graphics.drawable.Drawable;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewConfiguration;

/* loaded from: classes.dex */
public class PagerTabStrip extends PagerTitleStrip {
    public final Paint A0;
    public final Rect B0;
    public int C0;
    public boolean D0;
    public boolean E0;
    public int F0;
    public boolean G0;
    public float H0;
    public float I0;
    public int J0;
    public int u0;
    public int v0;
    public int w0;
    public int x0;
    public int y0;
    public int z0;

    /* loaded from: classes.dex */
    public class a implements View.OnClickListener {
        public a() {
        }

        @Override // android.view.View.OnClickListener
        public void onClick(View view) {
            ViewPager viewPager = PagerTabStrip.this.a;
            viewPager.setCurrentItem(viewPager.getCurrentItem() - 1);
        }
    }

    /* loaded from: classes.dex */
    public class b implements View.OnClickListener {
        public b() {
        }

        @Override // android.view.View.OnClickListener
        public void onClick(View view) {
            ViewPager viewPager = PagerTabStrip.this.a;
            viewPager.setCurrentItem(viewPager.getCurrentItem() + 1);
        }
    }

    public PagerTabStrip(Context context) {
        this(context, null);
    }

    @Override // androidx.viewpager.widget.PagerTitleStrip
    public void c(int i, float f, boolean z) {
        Rect rect = this.B0;
        int height = getHeight();
        int left = this.g0.getLeft() - this.z0;
        int right = this.g0.getRight() + this.z0;
        int i2 = height - this.v0;
        rect.set(left, i2, right, height);
        super.c(i, f, z);
        this.C0 = (int) (Math.abs(f - 0.5f) * 2.0f * 255.0f);
        rect.union(this.g0.getLeft() - this.z0, i2, this.g0.getRight() + this.z0, height);
        invalidate(rect);
    }

    public boolean getDrawFullUnderline() {
        return this.D0;
    }

    @Override // androidx.viewpager.widget.PagerTitleStrip
    public int getMinHeight() {
        return Math.max(super.getMinHeight(), this.y0);
    }

    public int getTabIndicatorColor() {
        return this.u0;
    }

    @Override // android.view.View
    public void onDraw(Canvas canvas) {
        super.onDraw(canvas);
        int height = getHeight();
        int left = this.g0.getLeft() - this.z0;
        int right = this.g0.getRight() + this.z0;
        this.A0.setColor((this.C0 << 24) | (this.u0 & 16777215));
        float f = height;
        canvas.drawRect(left, height - this.v0, right, f, this.A0);
        if (this.D0) {
            this.A0.setColor((-16777216) | (this.u0 & 16777215));
            canvas.drawRect(getPaddingLeft(), height - this.F0, getWidth() - getPaddingRight(), f, this.A0);
        }
    }

    @Override // android.view.View
    public boolean onTouchEvent(MotionEvent motionEvent) {
        int action = motionEvent.getAction();
        if (action == 0 || !this.G0) {
            float x = motionEvent.getX();
            float y = motionEvent.getY();
            if (action == 0) {
                this.H0 = x;
                this.I0 = y;
                this.G0 = false;
            } else if (action != 1) {
                if (action == 2 && (Math.abs(x - this.H0) > this.J0 || Math.abs(y - this.I0) > this.J0)) {
                    this.G0 = true;
                }
            } else if (x < this.g0.getLeft() - this.z0) {
                ViewPager viewPager = this.a;
                viewPager.setCurrentItem(viewPager.getCurrentItem() - 1);
            } else if (x > this.g0.getRight() + this.z0) {
                ViewPager viewPager2 = this.a;
                viewPager2.setCurrentItem(viewPager2.getCurrentItem() + 1);
            }
            return true;
        }
        return false;
    }

    @Override // android.view.View
    public void setBackgroundColor(int i) {
        super.setBackgroundColor(i);
        if (this.E0) {
            return;
        }
        this.D0 = (i & (-16777216)) == 0;
    }

    @Override // android.view.View
    public void setBackgroundDrawable(Drawable drawable) {
        super.setBackgroundDrawable(drawable);
        if (this.E0) {
            return;
        }
        this.D0 = drawable == null;
    }

    @Override // android.view.View
    public void setBackgroundResource(int i) {
        super.setBackgroundResource(i);
        if (this.E0) {
            return;
        }
        this.D0 = i == 0;
    }

    public void setDrawFullUnderline(boolean z) {
        this.D0 = z;
        this.E0 = true;
        invalidate();
    }

    @Override // android.view.View
    public void setPadding(int i, int i2, int i3, int i4) {
        int i5 = this.w0;
        if (i4 < i5) {
            i4 = i5;
        }
        super.setPadding(i, i2, i3, i4);
    }

    public void setTabIndicatorColor(int i) {
        this.u0 = i;
        this.A0.setColor(i);
        invalidate();
    }

    public void setTabIndicatorColorResource(int i) {
        setTabIndicatorColor(m70.d(getContext(), i));
    }

    @Override // androidx.viewpager.widget.PagerTitleStrip
    public void setTextSpacing(int i) {
        int i2 = this.x0;
        if (i < i2) {
            i = i2;
        }
        super.setTextSpacing(i);
    }

    public PagerTabStrip(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        Paint paint = new Paint();
        this.A0 = paint;
        this.B0 = new Rect();
        this.C0 = 255;
        this.D0 = false;
        this.E0 = false;
        int i = this.r0;
        this.u0 = i;
        paint.setColor(i);
        float f = context.getResources().getDisplayMetrics().density;
        this.v0 = (int) ((3.0f * f) + 0.5f);
        this.w0 = (int) ((6.0f * f) + 0.5f);
        this.x0 = (int) (64.0f * f);
        this.z0 = (int) ((16.0f * f) + 0.5f);
        this.F0 = (int) ((1.0f * f) + 0.5f);
        this.y0 = (int) ((f * 32.0f) + 0.5f);
        this.J0 = ViewConfiguration.get(context).getScaledTouchSlop();
        setPadding(getPaddingLeft(), getPaddingTop(), getPaddingRight(), getPaddingBottom());
        setTextSpacing(getTextSpacing());
        setWillNotDraw(false);
        this.f0.setFocusable(true);
        this.f0.setOnClickListener(new a());
        this.h0.setFocusable(true);
        this.h0.setOnClickListener(new b());
        if (getBackground() == null) {
            this.D0 = true;
        }
    }
}
