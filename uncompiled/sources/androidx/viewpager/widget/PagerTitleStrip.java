package androidx.viewpager.widget;

import android.content.Context;
import android.content.res.TypedArray;
import android.database.DataSetObserver;
import android.graphics.drawable.Drawable;
import android.text.TextUtils;
import android.text.method.SingleLineTransformationMethod;
import android.util.AttributeSet;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewParent;
import android.widget.TextView;
import androidx.viewpager.widget.ViewPager;
import com.github.mikephil.charting.utils.Utils;
import java.lang.ref.WeakReference;
import java.util.Locale;

@ViewPager.e
/* loaded from: classes.dex */
public class PagerTitleStrip extends ViewGroup {
    public static final int[] s0 = {16842804, 16842901, 16842904, 16842927};
    public static final int[] t0 = {16843660};
    public ViewPager a;
    public TextView f0;
    public TextView g0;
    public TextView h0;
    public int i0;
    public float j0;
    public int k0;
    public int l0;
    public boolean m0;
    public boolean n0;
    public final a o0;
    public WeakReference<dp2> p0;
    public int q0;
    public int r0;

    /* loaded from: classes.dex */
    public class a extends DataSetObserver implements ViewPager.i, ViewPager.h {
        public int a;

        public a() {
        }

        @Override // androidx.viewpager.widget.ViewPager.i
        public void a(int i, float f, int i2) {
            if (f > 0.5f) {
                i++;
            }
            PagerTitleStrip.this.c(i, f, false);
        }

        @Override // androidx.viewpager.widget.ViewPager.h
        public void b(ViewPager viewPager, dp2 dp2Var, dp2 dp2Var2) {
            PagerTitleStrip.this.a(dp2Var, dp2Var2);
        }

        @Override // androidx.viewpager.widget.ViewPager.i
        public void d(int i) {
            this.a = i;
        }

        @Override // androidx.viewpager.widget.ViewPager.i
        public void e(int i) {
            if (this.a == 0) {
                PagerTitleStrip pagerTitleStrip = PagerTitleStrip.this;
                pagerTitleStrip.b(pagerTitleStrip.a.getCurrentItem(), PagerTitleStrip.this.a.getAdapter());
                PagerTitleStrip pagerTitleStrip2 = PagerTitleStrip.this;
                float f = pagerTitleStrip2.j0;
                if (f < Utils.FLOAT_EPSILON) {
                    f = 0.0f;
                }
                pagerTitleStrip2.c(pagerTitleStrip2.a.getCurrentItem(), f, true);
            }
        }

        @Override // android.database.DataSetObserver
        public void onChanged() {
            PagerTitleStrip pagerTitleStrip = PagerTitleStrip.this;
            pagerTitleStrip.b(pagerTitleStrip.a.getCurrentItem(), PagerTitleStrip.this.a.getAdapter());
            PagerTitleStrip pagerTitleStrip2 = PagerTitleStrip.this;
            float f = pagerTitleStrip2.j0;
            if (f < Utils.FLOAT_EPSILON) {
                f = 0.0f;
            }
            pagerTitleStrip2.c(pagerTitleStrip2.a.getCurrentItem(), f, true);
        }
    }

    /* loaded from: classes.dex */
    public static class b extends SingleLineTransformationMethod {
        public Locale a;

        public b(Context context) {
            this.a = context.getResources().getConfiguration().locale;
        }

        @Override // android.text.method.ReplacementTransformationMethod, android.text.method.TransformationMethod
        public CharSequence getTransformation(CharSequence charSequence, View view) {
            CharSequence transformation = super.getTransformation(charSequence, view);
            if (transformation != null) {
                return transformation.toString().toUpperCase(this.a);
            }
            return null;
        }
    }

    public PagerTitleStrip(Context context) {
        this(context, null);
    }

    private static void setSingleLineAllCaps(TextView textView) {
        textView.setTransformationMethod(new b(textView.getContext()));
    }

    public void a(dp2 dp2Var, dp2 dp2Var2) {
        if (dp2Var != null) {
            dp2Var.t(this.o0);
            this.p0 = null;
        }
        if (dp2Var2 != null) {
            dp2Var2.l(this.o0);
            this.p0 = new WeakReference<>(dp2Var2);
        }
        ViewPager viewPager = this.a;
        if (viewPager != null) {
            this.i0 = -1;
            this.j0 = -1.0f;
            b(viewPager.getCurrentItem(), dp2Var2);
            requestLayout();
        }
    }

    public void b(int i, dp2 dp2Var) {
        int e = dp2Var != null ? dp2Var.e() : 0;
        this.m0 = true;
        CharSequence charSequence = null;
        this.f0.setText((i < 1 || dp2Var == null) ? null : dp2Var.g(i - 1));
        this.g0.setText((dp2Var == null || i >= e) ? null : dp2Var.g(i));
        int i2 = i + 1;
        if (i2 < e && dp2Var != null) {
            charSequence = dp2Var.g(i2);
        }
        this.h0.setText(charSequence);
        int makeMeasureSpec = View.MeasureSpec.makeMeasureSpec(Math.max(0, (int) (((getWidth() - getPaddingLeft()) - getPaddingRight()) * 0.8f)), Integer.MIN_VALUE);
        int makeMeasureSpec2 = View.MeasureSpec.makeMeasureSpec(Math.max(0, (getHeight() - getPaddingTop()) - getPaddingBottom()), Integer.MIN_VALUE);
        this.f0.measure(makeMeasureSpec, makeMeasureSpec2);
        this.g0.measure(makeMeasureSpec, makeMeasureSpec2);
        this.h0.measure(makeMeasureSpec, makeMeasureSpec2);
        this.i0 = i;
        if (!this.n0) {
            c(i, this.j0, false);
        }
        this.m0 = false;
    }

    public void c(int i, float f, boolean z) {
        int i2;
        int i3;
        int i4;
        int i5;
        if (i != this.i0) {
            b(i, this.a.getAdapter());
        } else if (!z && f == this.j0) {
            return;
        }
        this.n0 = true;
        int measuredWidth = this.f0.getMeasuredWidth();
        int measuredWidth2 = this.g0.getMeasuredWidth();
        int measuredWidth3 = this.h0.getMeasuredWidth();
        int i6 = measuredWidth2 / 2;
        int width = getWidth();
        int height = getHeight();
        int paddingLeft = getPaddingLeft();
        int paddingRight = getPaddingRight();
        int paddingTop = getPaddingTop();
        int paddingBottom = getPaddingBottom();
        int i7 = paddingRight + i6;
        int i8 = (width - (paddingLeft + i6)) - i7;
        float f2 = 0.5f + f;
        if (f2 > 1.0f) {
            f2 -= 1.0f;
        }
        int i9 = ((width - i7) - ((int) (i8 * f2))) - i6;
        int i10 = measuredWidth2 + i9;
        int baseline = this.f0.getBaseline();
        int baseline2 = this.g0.getBaseline();
        int baseline3 = this.h0.getBaseline();
        int max = Math.max(Math.max(baseline, baseline2), baseline3);
        int i11 = max - baseline;
        int i12 = max - baseline2;
        int i13 = max - baseline3;
        int max2 = Math.max(Math.max(this.f0.getMeasuredHeight() + i11, this.g0.getMeasuredHeight() + i12), this.h0.getMeasuredHeight() + i13);
        int i14 = this.l0 & 112;
        if (i14 == 16) {
            i2 = (((height - paddingTop) - paddingBottom) - max2) / 2;
        } else if (i14 != 80) {
            i3 = i11 + paddingTop;
            i4 = i12 + paddingTop;
            i5 = paddingTop + i13;
            TextView textView = this.g0;
            textView.layout(i9, i4, i10, textView.getMeasuredHeight() + i4);
            int min = Math.min(paddingLeft, (i9 - this.k0) - measuredWidth);
            TextView textView2 = this.f0;
            textView2.layout(min, i3, measuredWidth + min, textView2.getMeasuredHeight() + i3);
            int max3 = Math.max((width - paddingRight) - measuredWidth3, i10 + this.k0);
            TextView textView3 = this.h0;
            textView3.layout(max3, i5, max3 + measuredWidth3, textView3.getMeasuredHeight() + i5);
            this.j0 = f;
            this.n0 = false;
        } else {
            i2 = (height - paddingBottom) - max2;
        }
        i3 = i11 + i2;
        i4 = i12 + i2;
        i5 = i2 + i13;
        TextView textView4 = this.g0;
        textView4.layout(i9, i4, i10, textView4.getMeasuredHeight() + i4);
        int min2 = Math.min(paddingLeft, (i9 - this.k0) - measuredWidth);
        TextView textView22 = this.f0;
        textView22.layout(min2, i3, measuredWidth + min2, textView22.getMeasuredHeight() + i3);
        int max32 = Math.max((width - paddingRight) - measuredWidth3, i10 + this.k0);
        TextView textView32 = this.h0;
        textView32.layout(max32, i5, max32 + measuredWidth3, textView32.getMeasuredHeight() + i5);
        this.j0 = f;
        this.n0 = false;
    }

    public int getMinHeight() {
        Drawable background = getBackground();
        if (background != null) {
            return background.getIntrinsicHeight();
        }
        return 0;
    }

    public int getTextSpacing() {
        return this.k0;
    }

    @Override // android.view.ViewGroup, android.view.View
    public void onAttachedToWindow() {
        super.onAttachedToWindow();
        ViewParent parent = getParent();
        if (parent instanceof ViewPager) {
            ViewPager viewPager = (ViewPager) parent;
            dp2 adapter = viewPager.getAdapter();
            viewPager.S(this.o0);
            viewPager.b(this.o0);
            this.a = viewPager;
            WeakReference<dp2> weakReference = this.p0;
            a(weakReference != null ? weakReference.get() : null, adapter);
            return;
        }
        throw new IllegalStateException("PagerTitleStrip must be a direct child of a ViewPager.");
    }

    @Override // android.view.ViewGroup, android.view.View
    public void onDetachedFromWindow() {
        super.onDetachedFromWindow();
        ViewPager viewPager = this.a;
        if (viewPager != null) {
            a(viewPager.getAdapter(), null);
            this.a.S(null);
            this.a.L(this.o0);
            this.a = null;
        }
    }

    @Override // android.view.ViewGroup, android.view.View
    public void onLayout(boolean z, int i, int i2, int i3, int i4) {
        if (this.a != null) {
            float f = this.j0;
            if (f < Utils.FLOAT_EPSILON) {
                f = 0.0f;
            }
            c(this.i0, f, true);
        }
    }

    @Override // android.view.View
    public void onMeasure(int i, int i2) {
        int max;
        if (View.MeasureSpec.getMode(i) == 1073741824) {
            int paddingTop = getPaddingTop() + getPaddingBottom();
            int childMeasureSpec = ViewGroup.getChildMeasureSpec(i2, paddingTop, -2);
            int size = View.MeasureSpec.getSize(i);
            int childMeasureSpec2 = ViewGroup.getChildMeasureSpec(i, (int) (size * 0.2f), -2);
            this.f0.measure(childMeasureSpec2, childMeasureSpec);
            this.g0.measure(childMeasureSpec2, childMeasureSpec);
            this.h0.measure(childMeasureSpec2, childMeasureSpec);
            if (View.MeasureSpec.getMode(i2) == 1073741824) {
                max = View.MeasureSpec.getSize(i2);
            } else {
                max = Math.max(getMinHeight(), this.g0.getMeasuredHeight() + paddingTop);
            }
            setMeasuredDimension(size, View.resolveSizeAndState(max, i2, this.g0.getMeasuredState() << 16));
            return;
        }
        throw new IllegalStateException("Must measure with an exact width");
    }

    @Override // android.view.View, android.view.ViewParent
    public void requestLayout() {
        if (this.m0) {
            return;
        }
        super.requestLayout();
    }

    public void setGravity(int i) {
        this.l0 = i;
        requestLayout();
    }

    public void setNonPrimaryAlpha(float f) {
        int i = ((int) (f * 255.0f)) & 255;
        this.q0 = i;
        int i2 = (i << 24) | (this.r0 & 16777215);
        this.f0.setTextColor(i2);
        this.h0.setTextColor(i2);
    }

    public void setTextColor(int i) {
        this.r0 = i;
        this.g0.setTextColor(i);
        int i2 = (this.q0 << 24) | (this.r0 & 16777215);
        this.f0.setTextColor(i2);
        this.h0.setTextColor(i2);
    }

    public void setTextSize(int i, float f) {
        this.f0.setTextSize(i, f);
        this.g0.setTextSize(i, f);
        this.h0.setTextSize(i, f);
    }

    public void setTextSpacing(int i) {
        this.k0 = i;
        requestLayout();
    }

    public PagerTitleStrip(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        this.i0 = -1;
        this.j0 = -1.0f;
        this.o0 = new a();
        TextView textView = new TextView(context);
        this.f0 = textView;
        addView(textView);
        TextView textView2 = new TextView(context);
        this.g0 = textView2;
        addView(textView2);
        TextView textView3 = new TextView(context);
        this.h0 = textView3;
        addView(textView3);
        TypedArray obtainStyledAttributes = context.obtainStyledAttributes(attributeSet, s0);
        boolean z = false;
        int resourceId = obtainStyledAttributes.getResourceId(0, 0);
        if (resourceId != 0) {
            t44.q(this.f0, resourceId);
            t44.q(this.g0, resourceId);
            t44.q(this.h0, resourceId);
        }
        int dimensionPixelSize = obtainStyledAttributes.getDimensionPixelSize(1, 0);
        if (dimensionPixelSize != 0) {
            setTextSize(0, dimensionPixelSize);
        }
        if (obtainStyledAttributes.hasValue(2)) {
            int color = obtainStyledAttributes.getColor(2, 0);
            this.f0.setTextColor(color);
            this.g0.setTextColor(color);
            this.h0.setTextColor(color);
        }
        this.l0 = obtainStyledAttributes.getInteger(3, 80);
        obtainStyledAttributes.recycle();
        this.r0 = this.g0.getTextColors().getDefaultColor();
        setNonPrimaryAlpha(0.6f);
        this.f0.setEllipsize(TextUtils.TruncateAt.END);
        this.g0.setEllipsize(TextUtils.TruncateAt.END);
        this.h0.setEllipsize(TextUtils.TruncateAt.END);
        if (resourceId != 0) {
            TypedArray obtainStyledAttributes2 = context.obtainStyledAttributes(resourceId, t0);
            z = obtainStyledAttributes2.getBoolean(0, false);
            obtainStyledAttributes2.recycle();
        }
        if (z) {
            setSingleLineAllCaps(this.f0);
            setSingleLineAllCaps(this.g0);
            setSingleLineAllCaps(this.h0);
        } else {
            this.f0.setSingleLine();
            this.g0.setSingleLine();
            this.h0.setSingleLine();
        }
        this.k0 = (int) (context.getResources().getDisplayMetrics().density * 16.0f);
    }
}
