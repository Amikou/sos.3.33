package androidx.lifecycle;

import androidx.lifecycle.Lifecycle;
import kotlin.coroutines.CoroutineContext;

/* compiled from: Lifecycle.kt */
/* loaded from: classes.dex */
public final class LifecycleCoroutineScopeImpl extends LifecycleCoroutineScope implements e {
    public final Lifecycle a;
    public final CoroutineContext f0;

    public LifecycleCoroutineScopeImpl(Lifecycle lifecycle, CoroutineContext coroutineContext) {
        fs1.f(lifecycle, "lifecycle");
        fs1.f(coroutineContext, "coroutineContext");
        this.a = lifecycle;
        this.f0 = coroutineContext;
        if (a().b() == Lifecycle.State.DESTROYED) {
            zt1.f(m(), null, 1, null);
        }
    }

    @Override // androidx.lifecycle.LifecycleCoroutineScope
    public Lifecycle a() {
        return this.a;
    }

    public final void c() {
        as.b(this, tp0.c().l(), null, new LifecycleCoroutineScopeImpl$register$1(this, null), 2, null);
    }

    @Override // androidx.lifecycle.e
    public void e(rz1 rz1Var, Lifecycle.Event event) {
        fs1.f(rz1Var, "source");
        fs1.f(event, "event");
        if (a().b().compareTo(Lifecycle.State.DESTROYED) <= 0) {
            a().c(this);
            zt1.f(m(), null, 1, null);
        }
    }

    @Override // defpackage.c90
    public CoroutineContext m() {
        return this.f0;
    }
}
