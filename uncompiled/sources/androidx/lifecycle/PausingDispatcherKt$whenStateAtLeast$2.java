package androidx.lifecycle;

import androidx.lifecycle.Lifecycle;
import kotlin.coroutines.jvm.internal.SuspendLambda;

/* compiled from: PausingDispatcher.kt */
@kotlin.coroutines.jvm.internal.a(c = "androidx.lifecycle.PausingDispatcherKt$whenStateAtLeast$2", f = "PausingDispatcher.kt", l = {162}, m = "invokeSuspend")
/* loaded from: classes.dex */
public final class PausingDispatcherKt$whenStateAtLeast$2 extends SuspendLambda implements hd1<c90, q70<? super T>, Object> {
    public final /* synthetic */ hd1 $block;
    public final /* synthetic */ Lifecycle.State $minState;
    public final /* synthetic */ Lifecycle $this_whenStateAtLeast;
    private /* synthetic */ Object L$0;
    public int label;

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public PausingDispatcherKt$whenStateAtLeast$2(Lifecycle lifecycle, Lifecycle.State state, hd1 hd1Var, q70 q70Var) {
        super(2, q70Var);
        this.$this_whenStateAtLeast = lifecycle;
        this.$minState = state;
        this.$block = hd1Var;
    }

    @Override // kotlin.coroutines.jvm.internal.BaseContinuationImpl
    public final q70<te4> create(Object obj, q70<?> q70Var) {
        fs1.f(q70Var, "completion");
        PausingDispatcherKt$whenStateAtLeast$2 pausingDispatcherKt$whenStateAtLeast$2 = new PausingDispatcherKt$whenStateAtLeast$2(this.$this_whenStateAtLeast, this.$minState, this.$block, q70Var);
        pausingDispatcherKt$whenStateAtLeast$2.L$0 = obj;
        return pausingDispatcherKt$whenStateAtLeast$2;
    }

    @Override // defpackage.hd1
    public final Object invoke(c90 c90Var, Object obj) {
        return ((PausingDispatcherKt$whenStateAtLeast$2) create(c90Var, (q70) obj)).invokeSuspend(te4.a);
    }

    @Override // kotlin.coroutines.jvm.internal.BaseContinuationImpl
    public final Object invokeSuspend(Object obj) {
        LifecycleController lifecycleController;
        Object d = gs1.d();
        int i = this.label;
        if (i == 0) {
            o83.b(obj);
            st1 st1Var = (st1) ((c90) this.L$0).m().get(st1.f);
            if (st1Var != null) {
                dq2 dq2Var = new dq2();
                LifecycleController lifecycleController2 = new LifecycleController(this.$this_whenStateAtLeast, this.$minState, dq2Var.f0, st1Var);
                try {
                    hd1 hd1Var = this.$block;
                    this.L$0 = lifecycleController2;
                    this.label = 1;
                    obj = kotlinx.coroutines.a.e(dq2Var, hd1Var, this);
                    if (obj == d) {
                        return d;
                    }
                    lifecycleController = lifecycleController2;
                } catch (Throwable th) {
                    th = th;
                    lifecycleController = lifecycleController2;
                    lifecycleController.c();
                    throw th;
                }
            } else {
                throw new IllegalStateException("when[State] methods should have a parent job".toString());
            }
        } else if (i != 1) {
            throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        } else {
            lifecycleController = (LifecycleController) this.L$0;
            try {
                o83.b(obj);
            } catch (Throwable th2) {
                th = th2;
                lifecycleController.c();
                throw th;
            }
        }
        lifecycleController.c();
        return obj;
    }
}
