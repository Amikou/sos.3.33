package androidx.lifecycle;

import androidx.lifecycle.Lifecycle;

/* compiled from: PausingDispatcher.kt */
/* loaded from: classes.dex */
public final class PausingDispatcherKt {
    public static final <T> Object a(Lifecycle lifecycle, hd1<? super c90, ? super q70<? super T>, ? extends Object> hd1Var, q70<? super T> q70Var) {
        return b(lifecycle, Lifecycle.State.CREATED, hd1Var, q70Var);
    }

    public static final <T> Object b(Lifecycle lifecycle, Lifecycle.State state, hd1<? super c90, ? super q70<? super T>, ? extends Object> hd1Var, q70<? super T> q70Var) {
        return kotlinx.coroutines.a.e(tp0.c().l(), new PausingDispatcherKt$whenStateAtLeast$2(lifecycle, state, hd1Var, null), q70Var);
    }
}
