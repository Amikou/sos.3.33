package androidx.lifecycle;

import android.app.Application;
import java.lang.reflect.InvocationTargetException;

/* compiled from: ViewModelProvider.java */
/* loaded from: classes.dex */
public class l {
    public final b a;
    public final gj4 b;

    /* compiled from: ViewModelProvider.java */
    /* loaded from: classes.dex */
    public static class a extends d {
        public static a c;
        public Application b;

        public a(Application application) {
            this.b = application;
        }

        public static a c(Application application) {
            if (c == null) {
                c = new a(application);
            }
            return c;
        }

        @Override // androidx.lifecycle.l.d, androidx.lifecycle.l.b
        public <T extends dj4> T a(Class<T> cls) {
            if (gd.class.isAssignableFrom(cls)) {
                try {
                    return cls.getConstructor(Application.class).newInstance(this.b);
                } catch (IllegalAccessException e) {
                    throw new RuntimeException("Cannot create an instance of " + cls, e);
                } catch (InstantiationException e2) {
                    throw new RuntimeException("Cannot create an instance of " + cls, e2);
                } catch (NoSuchMethodException e3) {
                    throw new RuntimeException("Cannot create an instance of " + cls, e3);
                } catch (InvocationTargetException e4) {
                    throw new RuntimeException("Cannot create an instance of " + cls, e4);
                }
            }
            return (T) super.a(cls);
        }
    }

    /* compiled from: ViewModelProvider.java */
    /* loaded from: classes.dex */
    public interface b {
        <T extends dj4> T a(Class<T> cls);
    }

    /* compiled from: ViewModelProvider.java */
    /* loaded from: classes.dex */
    public static abstract class c extends e implements b {
        public <T extends dj4> T a(Class<T> cls) {
            throw new UnsupportedOperationException("create(String, Class<?>) must be called on implementaions of KeyedFactory");
        }

        public abstract <T extends dj4> T c(String str, Class<T> cls);
    }

    /* compiled from: ViewModelProvider.java */
    /* loaded from: classes.dex */
    public static class d implements b {
        public static d a;

        public static d b() {
            if (a == null) {
                a = new d();
            }
            return a;
        }

        @Override // androidx.lifecycle.l.b
        public <T extends dj4> T a(Class<T> cls) {
            try {
                return cls.newInstance();
            } catch (IllegalAccessException e) {
                throw new RuntimeException("Cannot create an instance of " + cls, e);
            } catch (InstantiationException e2) {
                throw new RuntimeException("Cannot create an instance of " + cls, e2);
            }
        }
    }

    /* compiled from: ViewModelProvider.java */
    /* loaded from: classes.dex */
    public static class e {
        public void b(dj4 dj4Var) {
        }
    }

    /* JADX WARN: Illegal instructions before constructor call */
    /*
        Code decompiled incorrectly, please refer to instructions dump.
        To view partially-correct code enable 'Show inconsistent code' option in preferences
    */
    public l(defpackage.hj4 r3) {
        /*
            r2 = this;
            gj4 r0 = r3.getViewModelStore()
            boolean r1 = r3 instanceof androidx.lifecycle.d
            if (r1 == 0) goto Lf
            androidx.lifecycle.d r3 = (androidx.lifecycle.d) r3
            androidx.lifecycle.l$b r3 = r3.getDefaultViewModelProviderFactory()
            goto L13
        Lf:
            androidx.lifecycle.l$d r3 = androidx.lifecycle.l.d.b()
        L13:
            r2.<init>(r0, r3)
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: androidx.lifecycle.l.<init>(hj4):void");
    }

    public <T extends dj4> T a(Class<T> cls) {
        String canonicalName = cls.getCanonicalName();
        if (canonicalName != null) {
            return (T) b("androidx.lifecycle.ViewModelProvider.DefaultKey:" + canonicalName, cls);
        }
        throw new IllegalArgumentException("Local and anonymous classes can not be ViewModels");
    }

    public <T extends dj4> T b(String str, Class<T> cls) {
        T t;
        T t2 = (T) this.b.b(str);
        if (cls.isInstance(t2)) {
            b bVar = this.a;
            if (bVar instanceof e) {
                ((e) bVar).b(t2);
            }
            return t2;
        }
        b bVar2 = this.a;
        if (bVar2 instanceof c) {
            t = (T) ((c) bVar2).c(str, cls);
        } else {
            t = (T) bVar2.a(cls);
        }
        this.b.d(str, t);
        return t;
    }

    public l(hj4 hj4Var, b bVar) {
        this(hj4Var.getViewModelStore(), bVar);
    }

    public l(gj4 gj4Var, b bVar) {
        this.a = bVar;
        this.b = gj4Var;
    }
}
