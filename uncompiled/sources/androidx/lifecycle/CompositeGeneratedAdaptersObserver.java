package androidx.lifecycle;

import androidx.lifecycle.Lifecycle;

/* JADX INFO: Access modifiers changed from: package-private */
/* loaded from: classes.dex */
public class CompositeGeneratedAdaptersObserver implements e {
    public final c[] a;

    public CompositeGeneratedAdaptersObserver(c[] cVarArr) {
        this.a = cVarArr;
    }

    @Override // androidx.lifecycle.e
    public void e(rz1 rz1Var, Lifecycle.Event event) {
        r82 r82Var = new r82();
        for (c cVar : this.a) {
            cVar.a(rz1Var, event, false, r82Var);
        }
        for (c cVar2 : this.a) {
            cVar2.a(rz1Var, event, true, r82Var);
        }
    }
}
