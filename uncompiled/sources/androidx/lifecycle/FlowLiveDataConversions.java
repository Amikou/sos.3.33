package androidx.lifecycle;

/* compiled from: FlowLiveData.kt */
/* loaded from: classes.dex */
public final class FlowLiveDataConversions {
    public static final <T> j71<T> a(LiveData<T> liveData) {
        fs1.f(liveData, "$this$asFlow");
        return n71.n(new FlowLiveDataConversions$asFlow$1(liveData, null));
    }
}
