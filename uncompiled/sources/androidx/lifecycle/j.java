package androidx.lifecycle;

import android.annotation.SuppressLint;
import android.app.Application;
import android.os.Bundle;
import androidx.lifecycle.l;
import androidx.savedstate.SavedStateRegistry;
import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.util.Arrays;

/* compiled from: SavedStateViewModelFactory.java */
/* loaded from: classes.dex */
public final class j extends l.c {
    public static final Class<?>[] f = {Application.class, ec3.class};
    public static final Class<?>[] g = {ec3.class};
    public final Application a;
    public final l.b b;
    public final Bundle c;
    public final Lifecycle d;
    public final SavedStateRegistry e;

    @SuppressLint({"LambdaLast"})
    public j(Application application, fc3 fc3Var, Bundle bundle) {
        l.b b;
        this.e = fc3Var.getSavedStateRegistry();
        this.d = fc3Var.getLifecycle();
        this.c = bundle;
        this.a = application;
        if (application != null) {
            b = l.a.c(application);
        } else {
            b = l.d.b();
        }
        this.b = b;
    }

    public static <T> Constructor<T> d(Class<T> cls, Class<?>[] clsArr) {
        for (Constructor<?> constructor : cls.getConstructors()) {
            Constructor<T> constructor2 = (Constructor<T>) constructor;
            if (Arrays.equals(clsArr, constructor2.getParameterTypes())) {
                return constructor2;
            }
        }
        return null;
    }

    @Override // androidx.lifecycle.l.c, androidx.lifecycle.l.b
    public <T extends dj4> T a(Class<T> cls) {
        String canonicalName = cls.getCanonicalName();
        if (canonicalName != null) {
            return (T) c(canonicalName, cls);
        }
        throw new IllegalArgumentException("Local and anonymous classes can not be ViewModels");
    }

    @Override // androidx.lifecycle.l.e
    public void b(dj4 dj4Var) {
        SavedStateHandleController.a(dj4Var, this.e, this.d);
    }

    @Override // androidx.lifecycle.l.c
    public <T extends dj4> T c(String str, Class<T> cls) {
        Constructor d;
        T t;
        boolean isAssignableFrom = gd.class.isAssignableFrom(cls);
        if (isAssignableFrom && this.a != null) {
            d = d(cls, f);
        } else {
            d = d(cls, g);
        }
        if (d == null) {
            return (T) this.b.a(cls);
        }
        SavedStateHandleController c = SavedStateHandleController.c(this.e, this.d, str, this.c);
        if (isAssignableFrom) {
            try {
                Application application = this.a;
                if (application != null) {
                    t = (T) d.newInstance(application, c.d());
                    t.setTagIfAbsent("androidx.lifecycle.savedstate.vm.tag", c);
                    return t;
                }
            } catch (IllegalAccessException e) {
                throw new RuntimeException("Failed to access " + cls, e);
            } catch (InstantiationException e2) {
                throw new RuntimeException("A " + cls + " cannot be instantiated.", e2);
            } catch (InvocationTargetException e3) {
                throw new RuntimeException("An exception happened in constructor of " + cls, e3.getCause());
            }
        }
        t = (T) d.newInstance(c.d());
        t.setTagIfAbsent("androidx.lifecycle.savedstate.vm.tag", c);
        return t;
    }
}
