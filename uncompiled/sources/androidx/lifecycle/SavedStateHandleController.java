package androidx.lifecycle;

import android.os.Bundle;
import androidx.lifecycle.Lifecycle;
import androidx.savedstate.SavedStateRegistry;

/* JADX INFO: Access modifiers changed from: package-private */
/* loaded from: classes.dex */
public final class SavedStateHandleController implements e {
    public final String a;
    public boolean f0 = false;
    public final ec3 g0;

    /* loaded from: classes.dex */
    public static final class a implements SavedStateRegistry.a {
        @Override // androidx.savedstate.SavedStateRegistry.a
        public void a(fc3 fc3Var) {
            if (fc3Var instanceof hj4) {
                gj4 viewModelStore = ((hj4) fc3Var).getViewModelStore();
                SavedStateRegistry savedStateRegistry = fc3Var.getSavedStateRegistry();
                for (String str : viewModelStore.c()) {
                    SavedStateHandleController.a(viewModelStore.b(str), savedStateRegistry, fc3Var.getLifecycle());
                }
                if (viewModelStore.c().isEmpty()) {
                    return;
                }
                savedStateRegistry.e(a.class);
                return;
            }
            throw new IllegalStateException("Internal error: OnRecreation should be registered only on componentsthat implement ViewModelStoreOwner");
        }
    }

    public SavedStateHandleController(String str, ec3 ec3Var) {
        this.a = str;
        this.g0 = ec3Var;
    }

    public static void a(dj4 dj4Var, SavedStateRegistry savedStateRegistry, Lifecycle lifecycle) {
        SavedStateHandleController savedStateHandleController = (SavedStateHandleController) dj4Var.getTag("androidx.lifecycle.savedstate.vm.tag");
        if (savedStateHandleController == null || savedStateHandleController.h()) {
            return;
        }
        savedStateHandleController.b(savedStateRegistry, lifecycle);
        i(savedStateRegistry, lifecycle);
    }

    public static SavedStateHandleController c(SavedStateRegistry savedStateRegistry, Lifecycle lifecycle, String str, Bundle bundle) {
        SavedStateHandleController savedStateHandleController = new SavedStateHandleController(str, ec3.a(savedStateRegistry.a(str), bundle));
        savedStateHandleController.b(savedStateRegistry, lifecycle);
        i(savedStateRegistry, lifecycle);
        return savedStateHandleController;
    }

    public static void i(final SavedStateRegistry savedStateRegistry, final Lifecycle lifecycle) {
        Lifecycle.State b = lifecycle.b();
        if (b != Lifecycle.State.INITIALIZED && !b.isAtLeast(Lifecycle.State.STARTED)) {
            lifecycle.a(new e() { // from class: androidx.lifecycle.SavedStateHandleController.1
                @Override // androidx.lifecycle.e
                public void e(rz1 rz1Var, Lifecycle.Event event) {
                    if (event == Lifecycle.Event.ON_START) {
                        Lifecycle.this.c(this);
                        savedStateRegistry.e(a.class);
                    }
                }
            });
        } else {
            savedStateRegistry.e(a.class);
        }
    }

    public void b(SavedStateRegistry savedStateRegistry, Lifecycle lifecycle) {
        if (!this.f0) {
            this.f0 = true;
            lifecycle.a(this);
            savedStateRegistry.d(this.a, this.g0.d());
            return;
        }
        throw new IllegalStateException("Already attached to lifecycleOwner");
    }

    public ec3 d() {
        return this.g0;
    }

    @Override // androidx.lifecycle.e
    public void e(rz1 rz1Var, Lifecycle.Event event) {
        if (event == Lifecycle.Event.ON_DESTROY) {
            this.f0 = false;
            rz1Var.getLifecycle().c(this);
        }
    }

    public boolean h() {
        return this.f0;
    }
}
