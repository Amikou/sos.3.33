package androidx.lifecycle;

import androidx.lifecycle.Lifecycle;
import kotlin.Result;

/* compiled from: WithLifecycleState.kt */
/* loaded from: classes.dex */
public final class WithLifecycleStateKt$suspendWithStateAtLeastUnchecked$$inlined$suspendCancellableCoroutine$lambda$1 implements e {
    public final /* synthetic */ ov a;
    public final /* synthetic */ Lifecycle f0;
    public final /* synthetic */ Lifecycle.State g0;
    public final /* synthetic */ rc1 h0;

    @Override // androidx.lifecycle.e
    public void e(rz1 rz1Var, Lifecycle.Event event) {
        Object m52constructorimpl;
        fs1.f(rz1Var, "source");
        fs1.f(event, "event");
        if (event == Lifecycle.Event.upTo(this.g0)) {
            this.f0.c(this);
            ov ovVar = this.a;
            rc1 rc1Var = this.h0;
            try {
                Result.a aVar = Result.Companion;
                m52constructorimpl = Result.m52constructorimpl(rc1Var.invoke());
            } catch (Throwable th) {
                Result.a aVar2 = Result.Companion;
                m52constructorimpl = Result.m52constructorimpl(o83.a(th));
            }
            ovVar.resumeWith(m52constructorimpl);
        } else if (event == Lifecycle.Event.ON_DESTROY) {
            this.f0.c(this);
            ov ovVar2 = this.a;
            LifecycleDestroyedException lifecycleDestroyedException = new LifecycleDestroyedException();
            Result.a aVar3 = Result.Companion;
            ovVar2.resumeWith(Result.m52constructorimpl(o83.a(lifecycleDestroyedException)));
        }
    }
}
