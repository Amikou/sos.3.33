package androidx.lifecycle;

import android.os.Handler;
import androidx.lifecycle.Lifecycle;

/* compiled from: ServiceLifecycleDispatcher.java */
/* loaded from: classes.dex */
public class k {
    public final f a;
    public final Handler b = new Handler();
    public a c;

    /* compiled from: ServiceLifecycleDispatcher.java */
    /* loaded from: classes.dex */
    public static class a implements Runnable {
        public final f a;
        public final Lifecycle.Event f0;
        public boolean g0 = false;

        public a(f fVar, Lifecycle.Event event) {
            this.a = fVar;
            this.f0 = event;
        }

        @Override // java.lang.Runnable
        public void run() {
            if (this.g0) {
                return;
            }
            this.a.h(this.f0);
            this.g0 = true;
        }
    }

    public k(rz1 rz1Var) {
        this.a = new f(rz1Var);
    }

    public Lifecycle a() {
        return this.a;
    }

    public void b() {
        f(Lifecycle.Event.ON_START);
    }

    public void c() {
        f(Lifecycle.Event.ON_CREATE);
    }

    public void d() {
        f(Lifecycle.Event.ON_STOP);
        f(Lifecycle.Event.ON_DESTROY);
    }

    public void e() {
        f(Lifecycle.Event.ON_START);
    }

    public final void f(Lifecycle.Event event) {
        a aVar = this.c;
        if (aVar != null) {
            aVar.run();
        }
        a aVar2 = new a(this.a, event);
        this.c = aVar2;
        this.b.postAtFrontOfQueue(aVar2);
    }
}
