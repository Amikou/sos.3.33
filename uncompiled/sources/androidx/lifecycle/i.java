package androidx.lifecycle;

import android.app.Activity;
import android.app.Application;
import android.content.Context;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import androidx.lifecycle.Lifecycle;
import androidx.lifecycle.ReportFragment;

/* compiled from: ProcessLifecycleOwner.java */
/* loaded from: classes.dex */
public class i implements rz1 {
    public static final i m0 = new i();
    public Handler i0;
    public int a = 0;
    public int f0 = 0;
    public boolean g0 = true;
    public boolean h0 = true;
    public final f j0 = new f(this);
    public Runnable k0 = new a();
    public ReportFragment.a l0 = new b();

    /* compiled from: ProcessLifecycleOwner.java */
    /* loaded from: classes.dex */
    public class a implements Runnable {
        public a() {
        }

        @Override // java.lang.Runnable
        public void run() {
            i.this.f();
            i.this.g();
        }
    }

    /* compiled from: ProcessLifecycleOwner.java */
    /* loaded from: classes.dex */
    public class b implements ReportFragment.a {
        public b() {
        }

        @Override // androidx.lifecycle.ReportFragment.a
        public void a() {
        }

        @Override // androidx.lifecycle.ReportFragment.a
        public void b() {
            i.this.c();
        }

        @Override // androidx.lifecycle.ReportFragment.a
        public void c() {
            i.this.b();
        }
    }

    /* compiled from: ProcessLifecycleOwner.java */
    /* loaded from: classes.dex */
    public class c extends pu0 {

        /* compiled from: ProcessLifecycleOwner.java */
        /* loaded from: classes.dex */
        public class a extends pu0 {
            public a() {
            }

            @Override // android.app.Application.ActivityLifecycleCallbacks
            public void onActivityPostResumed(Activity activity) {
                i.this.b();
            }

            @Override // android.app.Application.ActivityLifecycleCallbacks
            public void onActivityPostStarted(Activity activity) {
                i.this.c();
            }
        }

        public c() {
        }

        @Override // defpackage.pu0, android.app.Application.ActivityLifecycleCallbacks
        public void onActivityCreated(Activity activity, Bundle bundle) {
            if (Build.VERSION.SDK_INT < 29) {
                ReportFragment.f(activity).h(i.this.l0);
            }
        }

        @Override // defpackage.pu0, android.app.Application.ActivityLifecycleCallbacks
        public void onActivityPaused(Activity activity) {
            i.this.a();
        }

        @Override // android.app.Application.ActivityLifecycleCallbacks
        public void onActivityPreCreated(Activity activity, Bundle bundle) {
            activity.registerActivityLifecycleCallbacks(new a());
        }

        @Override // defpackage.pu0, android.app.Application.ActivityLifecycleCallbacks
        public void onActivityStopped(Activity activity) {
            i.this.d();
        }
    }

    public static rz1 h() {
        return m0;
    }

    public static void i(Context context) {
        m0.e(context);
    }

    public void a() {
        int i = this.f0 - 1;
        this.f0 = i;
        if (i == 0) {
            this.i0.postDelayed(this.k0, 700L);
        }
    }

    public void b() {
        int i = this.f0 + 1;
        this.f0 = i;
        if (i == 1) {
            if (this.g0) {
                this.j0.h(Lifecycle.Event.ON_RESUME);
                this.g0 = false;
                return;
            }
            this.i0.removeCallbacks(this.k0);
        }
    }

    public void c() {
        int i = this.a + 1;
        this.a = i;
        if (i == 1 && this.h0) {
            this.j0.h(Lifecycle.Event.ON_START);
            this.h0 = false;
        }
    }

    public void d() {
        this.a--;
        g();
    }

    public void e(Context context) {
        this.i0 = new Handler();
        this.j0.h(Lifecycle.Event.ON_CREATE);
        ((Application) context.getApplicationContext()).registerActivityLifecycleCallbacks(new c());
    }

    public void f() {
        if (this.f0 == 0) {
            this.g0 = true;
            this.j0.h(Lifecycle.Event.ON_PAUSE);
        }
    }

    public void g() {
        if (this.a == 0 && this.g0) {
            this.j0.h(Lifecycle.Event.ON_STOP);
            this.h0 = true;
        }
    }

    @Override // defpackage.rz1
    public Lifecycle getLifecycle() {
        return this.j0;
    }
}
