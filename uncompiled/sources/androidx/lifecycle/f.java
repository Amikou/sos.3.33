package androidx.lifecycle;

import android.annotation.SuppressLint;
import androidx.lifecycle.Lifecycle;
import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.Map;

/* compiled from: LifecycleRegistry.java */
/* loaded from: classes.dex */
public class f extends Lifecycle {
    public g21<qz1, a> b;
    public Lifecycle.State c;
    public final WeakReference<rz1> d;
    public int e;
    public boolean f;
    public boolean g;
    public ArrayList<Lifecycle.State> h;
    public final boolean i;

    /* compiled from: LifecycleRegistry.java */
    /* loaded from: classes.dex */
    public static class a {
        public Lifecycle.State a;
        public e b;

        public a(qz1 qz1Var, Lifecycle.State state) {
            this.b = g.f(qz1Var);
            this.a = state;
        }

        public void a(rz1 rz1Var, Lifecycle.Event event) {
            Lifecycle.State targetState = event.getTargetState();
            this.a = f.k(this.a, targetState);
            this.b.e(rz1Var, event);
            this.a = targetState;
        }
    }

    public f(rz1 rz1Var) {
        this(rz1Var, true);
    }

    public static Lifecycle.State k(Lifecycle.State state, Lifecycle.State state2) {
        return (state2 == null || state2.compareTo(state) >= 0) ? state : state2;
    }

    @Override // androidx.lifecycle.Lifecycle
    public void a(qz1 qz1Var) {
        rz1 rz1Var;
        f("addObserver");
        Lifecycle.State state = this.c;
        Lifecycle.State state2 = Lifecycle.State.DESTROYED;
        if (state != state2) {
            state2 = Lifecycle.State.INITIALIZED;
        }
        a aVar = new a(qz1Var, state2);
        if (this.b.o(qz1Var, aVar) == null && (rz1Var = this.d.get()) != null) {
            boolean z = this.e != 0 || this.f;
            Lifecycle.State e = e(qz1Var);
            this.e++;
            while (aVar.a.compareTo(e) < 0 && this.b.contains(qz1Var)) {
                n(aVar.a);
                Lifecycle.Event upFrom = Lifecycle.Event.upFrom(aVar.a);
                if (upFrom != null) {
                    aVar.a(rz1Var, upFrom);
                    m();
                    e = e(qz1Var);
                } else {
                    throw new IllegalStateException("no event up from " + aVar.a);
                }
            }
            if (!z) {
                p();
            }
            this.e--;
        }
    }

    @Override // androidx.lifecycle.Lifecycle
    public Lifecycle.State b() {
        return this.c;
    }

    @Override // androidx.lifecycle.Lifecycle
    public void c(qz1 qz1Var) {
        f("removeObserver");
        this.b.p(qz1Var);
    }

    public final void d(rz1 rz1Var) {
        Iterator<Map.Entry<qz1, a>> descendingIterator = this.b.descendingIterator();
        while (descendingIterator.hasNext() && !this.g) {
            Map.Entry<qz1, a> next = descendingIterator.next();
            a value = next.getValue();
            while (value.a.compareTo(this.c) > 0 && !this.g && this.b.contains(next.getKey())) {
                Lifecycle.Event downFrom = Lifecycle.Event.downFrom(value.a);
                if (downFrom != null) {
                    n(downFrom.getTargetState());
                    value.a(rz1Var, downFrom);
                    m();
                } else {
                    throw new IllegalStateException("no event down from " + value.a);
                }
            }
        }
    }

    public final Lifecycle.State e(qz1 qz1Var) {
        Map.Entry<qz1, a> q = this.b.q(qz1Var);
        Lifecycle.State state = null;
        Lifecycle.State state2 = q != null ? q.getValue().a : null;
        if (!this.h.isEmpty()) {
            ArrayList<Lifecycle.State> arrayList = this.h;
            state = arrayList.get(arrayList.size() - 1);
        }
        return k(k(this.c, state2), state);
    }

    @SuppressLint({"RestrictedApi"})
    public final void f(String str) {
        if (!this.i || gh.f().c()) {
            return;
        }
        throw new IllegalStateException("Method " + str + " must be called on the main thread");
    }

    /* JADX WARN: Multi-variable type inference failed */
    public final void g(rz1 rz1Var) {
        vb3<qz1, a>.d k = this.b.k();
        while (k.hasNext() && !this.g) {
            Map.Entry next = k.next();
            a aVar = (a) next.getValue();
            while (aVar.a.compareTo(this.c) < 0 && !this.g && this.b.contains(next.getKey())) {
                n(aVar.a);
                Lifecycle.Event upFrom = Lifecycle.Event.upFrom(aVar.a);
                if (upFrom != null) {
                    aVar.a(rz1Var, upFrom);
                    m();
                } else {
                    throw new IllegalStateException("no event up from " + aVar.a);
                }
            }
        }
    }

    public void h(Lifecycle.Event event) {
        f("handleLifecycleEvent");
        l(event.getTargetState());
    }

    public final boolean i() {
        if (this.b.size() == 0) {
            return true;
        }
        Lifecycle.State state = this.b.e().getValue().a;
        Lifecycle.State state2 = this.b.m().getValue().a;
        return state == state2 && this.c == state2;
    }

    @Deprecated
    public void j(Lifecycle.State state) {
        f("markState");
        o(state);
    }

    public final void l(Lifecycle.State state) {
        if (this.c == state) {
            return;
        }
        this.c = state;
        if (!this.f && this.e == 0) {
            this.f = true;
            p();
            this.f = false;
            return;
        }
        this.g = true;
    }

    public final void m() {
        ArrayList<Lifecycle.State> arrayList = this.h;
        arrayList.remove(arrayList.size() - 1);
    }

    public final void n(Lifecycle.State state) {
        this.h.add(state);
    }

    public void o(Lifecycle.State state) {
        f("setCurrentState");
        l(state);
    }

    public final void p() {
        rz1 rz1Var = this.d.get();
        if (rz1Var != null) {
            while (!i()) {
                this.g = false;
                if (this.c.compareTo(this.b.e().getValue().a) < 0) {
                    d(rz1Var);
                }
                Map.Entry<qz1, a> m = this.b.m();
                if (!this.g && m != null && this.c.compareTo(m.getValue().a) > 0) {
                    g(rz1Var);
                }
            }
            this.g = false;
            return;
        }
        throw new IllegalStateException("LifecycleOwner of this LifecycleRegistry is alreadygarbage collected. It is too late to change lifecycle state.");
    }

    public f(rz1 rz1Var, boolean z) {
        this.b = new g21<>();
        this.e = 0;
        this.f = false;
        this.g = false;
        this.h = new ArrayList<>();
        this.d = new WeakReference<>(rz1Var);
        this.c = Lifecycle.State.INITIALIZED;
        this.i = z;
    }
}
