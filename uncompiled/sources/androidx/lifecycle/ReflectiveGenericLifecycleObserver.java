package androidx.lifecycle;

import androidx.lifecycle.Lifecycle;
import androidx.lifecycle.b;

/* JADX INFO: Access modifiers changed from: package-private */
/* loaded from: classes.dex */
public class ReflectiveGenericLifecycleObserver implements e {
    public final Object a;
    public final b.a f0;

    public ReflectiveGenericLifecycleObserver(Object obj) {
        this.a = obj;
        this.f0 = b.c.c(obj.getClass());
    }

    @Override // androidx.lifecycle.e
    public void e(rz1 rz1Var, Lifecycle.Event event) {
        this.f0.a(rz1Var, event, this.a);
    }
}
