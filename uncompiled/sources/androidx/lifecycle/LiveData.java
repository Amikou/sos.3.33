package androidx.lifecycle;

import androidx.lifecycle.Lifecycle;
import java.util.Iterator;
import java.util.Map;

/* loaded from: classes.dex */
public abstract class LiveData<T> {
    public static final Object NOT_SET = new Object();
    public int mActiveCount;
    public boolean mChangingActiveState;
    public volatile Object mData;
    public final Object mDataLock;
    public boolean mDispatchInvalidated;
    public boolean mDispatchingValue;
    public vb3<tl2<? super T>, LiveData<T>.c> mObservers;
    public volatile Object mPendingData;
    public final Runnable mPostValueRunnable;
    public int mVersion;

    /* loaded from: classes.dex */
    public class LifecycleBoundObserver extends LiveData<T>.c implements e {
        public final rz1 i0;

        public LifecycleBoundObserver(rz1 rz1Var, tl2<? super T> tl2Var) {
            super(tl2Var);
            this.i0 = rz1Var;
        }

        @Override // androidx.lifecycle.LiveData.c
        public void b() {
            this.i0.getLifecycle().c(this);
        }

        @Override // androidx.lifecycle.LiveData.c
        public boolean c(rz1 rz1Var) {
            return this.i0 == rz1Var;
        }

        @Override // androidx.lifecycle.LiveData.c
        public boolean d() {
            return this.i0.getLifecycle().b().isAtLeast(Lifecycle.State.STARTED);
        }

        @Override // androidx.lifecycle.e
        public void e(rz1 rz1Var, Lifecycle.Event event) {
            Lifecycle.State b = this.i0.getLifecycle().b();
            if (b == Lifecycle.State.DESTROYED) {
                LiveData.this.removeObserver(this.a);
                return;
            }
            Lifecycle.State state = null;
            while (state != b) {
                a(d());
                state = b;
                b = this.i0.getLifecycle().b();
            }
        }
    }

    /* loaded from: classes.dex */
    public class a implements Runnable {
        public a() {
        }

        /* JADX WARN: Multi-variable type inference failed */
        @Override // java.lang.Runnable
        public void run() {
            Object obj;
            synchronized (LiveData.this.mDataLock) {
                obj = LiveData.this.mPendingData;
                LiveData.this.mPendingData = LiveData.NOT_SET;
            }
            LiveData.this.setValue(obj);
        }
    }

    /* loaded from: classes.dex */
    public class b extends LiveData<T>.c {
        public b(LiveData liveData, tl2<? super T> tl2Var) {
            super(tl2Var);
        }

        @Override // androidx.lifecycle.LiveData.c
        public boolean d() {
            return true;
        }
    }

    /* loaded from: classes.dex */
    public abstract class c {
        public final tl2<? super T> a;
        public boolean f0;
        public int g0 = -1;

        public c(tl2<? super T> tl2Var) {
            this.a = tl2Var;
        }

        public void a(boolean z) {
            if (z == this.f0) {
                return;
            }
            this.f0 = z;
            LiveData.this.changeActiveCounter(z ? 1 : -1);
            if (this.f0) {
                LiveData.this.dispatchingValue(this);
            }
        }

        public void b() {
        }

        public boolean c(rz1 rz1Var) {
            return false;
        }

        public abstract boolean d();
    }

    public LiveData(T t) {
        this.mDataLock = new Object();
        this.mObservers = new vb3<>();
        this.mActiveCount = 0;
        this.mPendingData = NOT_SET;
        this.mPostValueRunnable = new a();
        this.mData = t;
        this.mVersion = 0;
    }

    public static void assertMainThread(String str) {
        if (gh.f().c()) {
            return;
        }
        throw new IllegalStateException("Cannot invoke " + str + " on a background thread");
    }

    public void changeActiveCounter(int i) {
        int i2 = this.mActiveCount;
        this.mActiveCount = i + i2;
        if (this.mChangingActiveState) {
            return;
        }
        this.mChangingActiveState = true;
        while (true) {
            try {
                int i3 = this.mActiveCount;
                if (i2 == i3) {
                    return;
                }
                boolean z = i2 == 0 && i3 > 0;
                boolean z2 = i2 > 0 && i3 == 0;
                if (z) {
                    onActive();
                } else if (z2) {
                    onInactive();
                }
                i2 = i3;
            } finally {
                this.mChangingActiveState = false;
            }
        }
    }

    public final void considerNotify(LiveData<T>.c cVar) {
        if (cVar.f0) {
            if (!cVar.d()) {
                cVar.a(false);
                return;
            }
            int i = cVar.g0;
            int i2 = this.mVersion;
            if (i >= i2) {
                return;
            }
            cVar.g0 = i2;
            cVar.a.onChanged((Object) this.mData);
        }
    }

    public void dispatchingValue(LiveData<T>.c cVar) {
        if (this.mDispatchingValue) {
            this.mDispatchInvalidated = true;
            return;
        }
        this.mDispatchingValue = true;
        do {
            this.mDispatchInvalidated = false;
            if (cVar != null) {
                considerNotify(cVar);
                cVar = null;
            } else {
                vb3<tl2<? super T>, LiveData<T>.c>.d k = this.mObservers.k();
                while (k.hasNext()) {
                    considerNotify((c) k.next().getValue());
                    if (this.mDispatchInvalidated) {
                        break;
                    }
                }
            }
        } while (this.mDispatchInvalidated);
        this.mDispatchingValue = false;
    }

    public T getValue() {
        T t = (T) this.mData;
        if (t != NOT_SET) {
            return t;
        }
        return null;
    }

    public int getVersion() {
        return this.mVersion;
    }

    public boolean hasActiveObservers() {
        return this.mActiveCount > 0;
    }

    public boolean hasObservers() {
        return this.mObservers.size() > 0;
    }

    public void observe(rz1 rz1Var, tl2<? super T> tl2Var) {
        assertMainThread("observe");
        if (rz1Var.getLifecycle().b() == Lifecycle.State.DESTROYED) {
            return;
        }
        LifecycleBoundObserver lifecycleBoundObserver = new LifecycleBoundObserver(rz1Var, tl2Var);
        LiveData<T>.c o = this.mObservers.o(tl2Var, lifecycleBoundObserver);
        if (o != null && !o.c(rz1Var)) {
            throw new IllegalArgumentException("Cannot add the same observer with different lifecycles");
        }
        if (o != null) {
            return;
        }
        rz1Var.getLifecycle().a(lifecycleBoundObserver);
    }

    public void observeForever(tl2<? super T> tl2Var) {
        assertMainThread("observeForever");
        b bVar = new b(this, tl2Var);
        LiveData<T>.c o = this.mObservers.o(tl2Var, bVar);
        if (o instanceof LifecycleBoundObserver) {
            throw new IllegalArgumentException("Cannot add the same observer with different lifecycles");
        }
        if (o != null) {
            return;
        }
        bVar.a(true);
    }

    public void onActive() {
    }

    public void onInactive() {
    }

    public void postValue(T t) {
        boolean z;
        synchronized (this.mDataLock) {
            z = this.mPendingData == NOT_SET;
            this.mPendingData = t;
        }
        if (z) {
            gh.f().d(this.mPostValueRunnable);
        }
    }

    public void removeObserver(tl2<? super T> tl2Var) {
        assertMainThread("removeObserver");
        LiveData<T>.c p = this.mObservers.p(tl2Var);
        if (p == null) {
            return;
        }
        p.b();
        p.a(false);
    }

    public void removeObservers(rz1 rz1Var) {
        assertMainThread("removeObservers");
        Iterator<Map.Entry<tl2<? super T>, LiveData<T>.c>> it = this.mObservers.iterator();
        while (it.hasNext()) {
            Map.Entry<tl2<? super T>, LiveData<T>.c> next = it.next();
            if (next.getValue().c(rz1Var)) {
                removeObserver(next.getKey());
            }
        }
    }

    public void setValue(T t) {
        assertMainThread("setValue");
        this.mVersion++;
        this.mData = t;
        dispatchingValue(null);
    }

    public LiveData() {
        this.mDataLock = new Object();
        this.mObservers = new vb3<>();
        this.mActiveCount = 0;
        Object obj = NOT_SET;
        this.mPendingData = obj;
        this.mPostValueRunnable = new a();
        this.mData = obj;
        this.mVersion = -1;
    }
}
