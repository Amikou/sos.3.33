package androidx.lifecycle;

import androidx.lifecycle.Lifecycle;

/* JADX INFO: Access modifiers changed from: package-private */
/* loaded from: classes.dex */
public class SingleGeneratedAdapterObserver implements e {
    public final c a;

    public SingleGeneratedAdapterObserver(c cVar) {
        this.a = cVar;
    }

    @Override // androidx.lifecycle.e
    public void e(rz1 rz1Var, Lifecycle.Event event) {
        this.a.a(rz1Var, event, false, null);
        this.a.a(rz1Var, event, true, null);
    }
}
