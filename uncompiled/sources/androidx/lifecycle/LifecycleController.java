package androidx.lifecycle;

import androidx.lifecycle.Lifecycle;
import defpackage.st1;

/* compiled from: LifecycleController.kt */
/* loaded from: classes.dex */
public final class LifecycleController {
    public final e a;
    public final Lifecycle b;
    public final Lifecycle.State c;
    public final mp0 d;

    public LifecycleController(Lifecycle lifecycle, Lifecycle.State state, mp0 mp0Var, final st1 st1Var) {
        fs1.f(lifecycle, "lifecycle");
        fs1.f(state, "minState");
        fs1.f(mp0Var, "dispatchQueue");
        fs1.f(st1Var, "parentJob");
        this.b = lifecycle;
        this.c = state;
        this.d = mp0Var;
        e eVar = new e() { // from class: androidx.lifecycle.LifecycleController$observer$1
            @Override // androidx.lifecycle.e
            public final void e(rz1 rz1Var, Lifecycle.Event event) {
                Lifecycle.State state2;
                mp0 mp0Var2;
                mp0 mp0Var3;
                fs1.f(rz1Var, "source");
                fs1.f(event, "<anonymous parameter 1>");
                Lifecycle lifecycle2 = rz1Var.getLifecycle();
                fs1.e(lifecycle2, "source.lifecycle");
                if (lifecycle2.b() == Lifecycle.State.DESTROYED) {
                    LifecycleController lifecycleController = LifecycleController.this;
                    st1.a.a(st1Var, null, 1, null);
                    lifecycleController.c();
                    return;
                }
                Lifecycle lifecycle3 = rz1Var.getLifecycle();
                fs1.e(lifecycle3, "source.lifecycle");
                Lifecycle.State b = lifecycle3.b();
                state2 = LifecycleController.this.c;
                if (b.compareTo(state2) < 0) {
                    mp0Var3 = LifecycleController.this.d;
                    mp0Var3.g();
                    return;
                }
                mp0Var2 = LifecycleController.this.d;
                mp0Var2.h();
            }
        };
        this.a = eVar;
        if (lifecycle.b() == Lifecycle.State.DESTROYED) {
            st1.a.a(st1Var, null, 1, null);
            c();
            return;
        }
        lifecycle.a(eVar);
    }

    public final void c() {
        this.b.c(this.a);
        this.d.f();
    }
}
