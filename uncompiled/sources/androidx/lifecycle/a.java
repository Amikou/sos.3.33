package androidx.lifecycle;

import android.os.Bundle;
import androidx.lifecycle.l;
import androidx.savedstate.SavedStateRegistry;

/* compiled from: AbstractSavedStateViewModelFactory.java */
/* loaded from: classes.dex */
public abstract class a extends l.c {
    public final SavedStateRegistry a;
    public final Lifecycle b;
    public final Bundle c;

    public a(fc3 fc3Var, Bundle bundle) {
        this.a = fc3Var.getSavedStateRegistry();
        this.b = fc3Var.getLifecycle();
        this.c = bundle;
    }

    @Override // androidx.lifecycle.l.c, androidx.lifecycle.l.b
    public final <T extends dj4> T a(Class<T> cls) {
        String canonicalName = cls.getCanonicalName();
        if (canonicalName != null) {
            return (T) c(canonicalName, cls);
        }
        throw new IllegalArgumentException("Local and anonymous classes can not be ViewModels");
    }

    @Override // androidx.lifecycle.l.e
    public void b(dj4 dj4Var) {
        SavedStateHandleController.a(dj4Var, this.a, this.b);
    }

    @Override // androidx.lifecycle.l.c
    public final <T extends dj4> T c(String str, Class<T> cls) {
        SavedStateHandleController c = SavedStateHandleController.c(this.a, this.b, str, this.c);
        T t = (T) d(str, cls, c.d());
        t.setTagIfAbsent("androidx.lifecycle.savedstate.vm.tag", c);
        return t;
    }

    public abstract <T extends dj4> T d(String str, Class<T> cls, ec3 ec3Var);
}
