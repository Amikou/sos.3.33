package androidx.lifecycle;

import kotlin.coroutines.jvm.internal.SuspendLambda;

/* compiled from: Lifecycle.kt */
@kotlin.coroutines.jvm.internal.a(c = "androidx.lifecycle.LifecycleCoroutineScope$launchWhenCreated$1", f = "Lifecycle.kt", l = {74}, m = "invokeSuspend")
/* loaded from: classes.dex */
public final class LifecycleCoroutineScope$launchWhenCreated$1 extends SuspendLambda implements hd1<c90, q70<? super te4>, Object> {
    public final /* synthetic */ hd1 $block;
    public int label;
    public final /* synthetic */ LifecycleCoroutineScope this$0;

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public LifecycleCoroutineScope$launchWhenCreated$1(LifecycleCoroutineScope lifecycleCoroutineScope, hd1 hd1Var, q70 q70Var) {
        super(2, q70Var);
        this.this$0 = lifecycleCoroutineScope;
        this.$block = hd1Var;
    }

    @Override // kotlin.coroutines.jvm.internal.BaseContinuationImpl
    public final q70<te4> create(Object obj, q70<?> q70Var) {
        fs1.f(q70Var, "completion");
        return new LifecycleCoroutineScope$launchWhenCreated$1(this.this$0, this.$block, q70Var);
    }

    @Override // defpackage.hd1
    public final Object invoke(c90 c90Var, q70<? super te4> q70Var) {
        return ((LifecycleCoroutineScope$launchWhenCreated$1) create(c90Var, q70Var)).invokeSuspend(te4.a);
    }

    @Override // kotlin.coroutines.jvm.internal.BaseContinuationImpl
    public final Object invokeSuspend(Object obj) {
        Object d = gs1.d();
        int i = this.label;
        if (i == 0) {
            o83.b(obj);
            Lifecycle a = this.this$0.a();
            hd1 hd1Var = this.$block;
            this.label = 1;
            if (PausingDispatcherKt.a(a, hd1Var, this) == d) {
                return d;
            }
        } else if (i != 1) {
            throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        } else {
            o83.b(obj);
        }
        return te4.a;
    }
}
