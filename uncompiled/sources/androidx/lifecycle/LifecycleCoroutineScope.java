package androidx.lifecycle;

/* compiled from: Lifecycle.kt */
/* loaded from: classes.dex */
public abstract class LifecycleCoroutineScope implements c90 {
    public abstract Lifecycle a();

    public final st1 b(hd1<? super c90, ? super q70<? super te4>, ? extends Object> hd1Var) {
        st1 b;
        fs1.f(hd1Var, "block");
        b = as.b(this, null, null, new LifecycleCoroutineScope$launchWhenCreated$1(this, hd1Var, null), 3, null);
        return b;
    }
}
