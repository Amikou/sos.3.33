package androidx.viewpager2.adapter;

import android.os.Handler;
import androidx.lifecycle.Lifecycle;
import androidx.lifecycle.e;

/* loaded from: classes.dex */
class FragmentStateAdapter$5 implements e {
    public final /* synthetic */ Handler a;
    public final /* synthetic */ Runnable f0;

    @Override // androidx.lifecycle.e
    public void e(rz1 rz1Var, Lifecycle.Event event) {
        if (event == Lifecycle.Event.ON_DESTROY) {
            this.a.removeCallbacks(this.f0);
            rz1Var.getLifecycle().c(this);
        }
    }
}
