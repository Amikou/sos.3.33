package androidx.viewpager2.widget;

import androidx.viewpager2.widget.ViewPager2;
import java.util.ArrayList;
import java.util.ConcurrentModificationException;
import java.util.List;

/* compiled from: CompositeOnPageChangeCallback.java */
/* loaded from: classes.dex */
public final class a extends ViewPager2.i {
    public final List<ViewPager2.i> a;

    public a(int i) {
        this.a = new ArrayList(i);
    }

    @Override // androidx.viewpager2.widget.ViewPager2.i
    public void a(int i) {
        try {
            for (ViewPager2.i iVar : this.a) {
                iVar.a(i);
            }
        } catch (ConcurrentModificationException e) {
            e(e);
        }
    }

    @Override // androidx.viewpager2.widget.ViewPager2.i
    public void b(int i, float f, int i2) {
        try {
            for (ViewPager2.i iVar : this.a) {
                iVar.b(i, f, i2);
            }
        } catch (ConcurrentModificationException e) {
            e(e);
        }
    }

    @Override // androidx.viewpager2.widget.ViewPager2.i
    public void c(int i) {
        try {
            for (ViewPager2.i iVar : this.a) {
                iVar.c(i);
            }
        } catch (ConcurrentModificationException e) {
            e(e);
        }
    }

    public void d(ViewPager2.i iVar) {
        this.a.add(iVar);
    }

    public final void e(ConcurrentModificationException concurrentModificationException) {
        throw new IllegalStateException("Adding and removing callbacks during dispatch to callbacks is not supported", concurrentModificationException);
    }
}
