package androidx.viewpager2.widget;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Rect;
import android.os.Build;
import android.os.Bundle;
import android.os.Parcel;
import android.os.Parcelable;
import android.util.AttributeSet;
import android.util.SparseArray;
import android.view.Gravity;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.accessibility.AccessibilityEvent;
import android.view.accessibility.AccessibilityNodeInfo;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.recyclerview.widget.r;
import defpackage.b6;
import defpackage.e6;

/* loaded from: classes.dex */
public final class ViewPager2 extends ViewGroup {
    public static boolean y0 = true;
    public final Rect a;
    public final Rect f0;
    public androidx.viewpager2.widget.a g0;
    public int h0;
    public boolean i0;
    public RecyclerView.i j0;
    public LinearLayoutManager k0;
    public int l0;
    public Parcelable m0;
    public RecyclerView n0;
    public r o0;
    public androidx.viewpager2.widget.c p0;
    public androidx.viewpager2.widget.a q0;
    public c21 r0;
    public androidx.viewpager2.widget.b s0;
    public RecyclerView.l t0;
    public boolean u0;
    public boolean v0;
    public int w0;
    public e x0;

    /* loaded from: classes.dex */
    public class a extends g {
        public a() {
            super(null);
        }

        @Override // androidx.recyclerview.widget.RecyclerView.i
        public void onChanged() {
            ViewPager2 viewPager2 = ViewPager2.this;
            viewPager2.i0 = true;
            viewPager2.p0.j();
        }
    }

    /* loaded from: classes.dex */
    public class b extends i {
        public b() {
        }

        @Override // androidx.viewpager2.widget.ViewPager2.i
        public void a(int i) {
            if (i == 0) {
                ViewPager2.this.l();
            }
        }

        @Override // androidx.viewpager2.widget.ViewPager2.i
        public void c(int i) {
            ViewPager2 viewPager2 = ViewPager2.this;
            if (viewPager2.h0 != i) {
                viewPager2.h0 = i;
                viewPager2.x0.q();
            }
        }
    }

    /* loaded from: classes.dex */
    public class c extends i {
        public c() {
        }

        @Override // androidx.viewpager2.widget.ViewPager2.i
        public void c(int i) {
            ViewPager2.this.clearFocus();
            if (ViewPager2.this.hasFocus()) {
                ViewPager2.this.n0.requestFocus(2);
            }
        }
    }

    /* loaded from: classes.dex */
    public class d implements RecyclerView.o {
        public d(ViewPager2 viewPager2) {
        }

        @Override // androidx.recyclerview.widget.RecyclerView.o
        public void b(View view) {
        }

        @Override // androidx.recyclerview.widget.RecyclerView.o
        public void d(View view) {
            RecyclerView.LayoutParams layoutParams = (RecyclerView.LayoutParams) view.getLayoutParams();
            if (((ViewGroup.MarginLayoutParams) layoutParams).width != -1 || ((ViewGroup.MarginLayoutParams) layoutParams).height != -1) {
                throw new IllegalStateException("Pages must fill the whole ViewPager2 (use match_parent)");
            }
        }
    }

    /* loaded from: classes.dex */
    public abstract class e {
        public e(ViewPager2 viewPager2) {
        }

        public boolean a() {
            return false;
        }

        public boolean b(int i) {
            return false;
        }

        public boolean c(int i, Bundle bundle) {
            return false;
        }

        public boolean d() {
            return false;
        }

        public void e(RecyclerView.Adapter<?> adapter) {
        }

        public void f(RecyclerView.Adapter<?> adapter) {
        }

        public String g() {
            throw new IllegalStateException("Not implemented.");
        }

        public void h(androidx.viewpager2.widget.a aVar, RecyclerView recyclerView) {
        }

        public void i(AccessibilityNodeInfo accessibilityNodeInfo) {
        }

        public void j(b6 b6Var) {
        }

        public boolean k(int i) {
            throw new IllegalStateException("Not implemented.");
        }

        public boolean l(int i, Bundle bundle) {
            throw new IllegalStateException("Not implemented.");
        }

        public void m() {
        }

        public CharSequence n() {
            throw new IllegalStateException("Not implemented.");
        }

        public void o(AccessibilityEvent accessibilityEvent) {
        }

        public void p() {
        }

        public void q() {
        }

        public void r() {
        }

        public void s() {
        }

        public /* synthetic */ e(ViewPager2 viewPager2, a aVar) {
            this(viewPager2);
        }
    }

    /* loaded from: classes.dex */
    public class f extends e {
        public f() {
            super(ViewPager2.this, null);
        }

        @Override // androidx.viewpager2.widget.ViewPager2.e
        public boolean b(int i) {
            return (i == 8192 || i == 4096) && !ViewPager2.this.e();
        }

        @Override // androidx.viewpager2.widget.ViewPager2.e
        public boolean d() {
            return true;
        }

        @Override // androidx.viewpager2.widget.ViewPager2.e
        public void j(b6 b6Var) {
            if (ViewPager2.this.e()) {
                return;
            }
            b6Var.T(b6.a.i);
            b6Var.T(b6.a.h);
            b6Var.y0(false);
        }

        @Override // androidx.viewpager2.widget.ViewPager2.e
        public boolean k(int i) {
            if (b(i)) {
                return false;
            }
            throw new IllegalStateException();
        }

        @Override // androidx.viewpager2.widget.ViewPager2.e
        public CharSequence n() {
            if (d()) {
                return "androidx.viewpager.widget.ViewPager";
            }
            throw new IllegalStateException();
        }
    }

    /* loaded from: classes.dex */
    public static abstract class g extends RecyclerView.i {
        public g() {
        }

        @Override // androidx.recyclerview.widget.RecyclerView.i
        public final void onItemRangeChanged(int i, int i2) {
            onChanged();
        }

        @Override // androidx.recyclerview.widget.RecyclerView.i
        public final void onItemRangeInserted(int i, int i2) {
            onChanged();
        }

        @Override // androidx.recyclerview.widget.RecyclerView.i
        public final void onItemRangeMoved(int i, int i2, int i3) {
            onChanged();
        }

        @Override // androidx.recyclerview.widget.RecyclerView.i
        public final void onItemRangeRemoved(int i, int i2) {
            onChanged();
        }

        public /* synthetic */ g(a aVar) {
            this();
        }

        @Override // androidx.recyclerview.widget.RecyclerView.i
        public final void onItemRangeChanged(int i, int i2, Object obj) {
            onChanged();
        }
    }

    /* loaded from: classes.dex */
    public class h extends LinearLayoutManager {
        public h(Context context) {
            super(context);
        }

        @Override // androidx.recyclerview.widget.RecyclerView.LayoutManager
        public boolean B1(RecyclerView recyclerView, View view, Rect rect, boolean z, boolean z2) {
            return false;
        }

        @Override // androidx.recyclerview.widget.RecyclerView.LayoutManager
        public void V0(RecyclerView.t tVar, RecyclerView.x xVar, b6 b6Var) {
            super.V0(tVar, xVar, b6Var);
            ViewPager2.this.x0.j(b6Var);
        }

        @Override // androidx.recyclerview.widget.LinearLayoutManager
        public void V1(RecyclerView.x xVar, int[] iArr) {
            int offscreenPageLimit = ViewPager2.this.getOffscreenPageLimit();
            if (offscreenPageLimit == -1) {
                super.V1(xVar, iArr);
                return;
            }
            int pageSize = ViewPager2.this.getPageSize() * offscreenPageLimit;
            iArr[0] = pageSize;
            iArr[1] = pageSize;
        }

        @Override // androidx.recyclerview.widget.RecyclerView.LayoutManager
        public boolean p1(RecyclerView.t tVar, RecyclerView.x xVar, int i, Bundle bundle) {
            if (ViewPager2.this.x0.b(i)) {
                return ViewPager2.this.x0.k(i);
            }
            return super.p1(tVar, xVar, i, bundle);
        }
    }

    /* loaded from: classes.dex */
    public static abstract class i {
        public void a(int i) {
        }

        public void b(int i, float f, int i2) {
        }

        public void c(int i) {
        }
    }

    /* loaded from: classes.dex */
    public class j extends e {
        public final e6 a;
        public final e6 b;
        public RecyclerView.i c;

        /* loaded from: classes.dex */
        public class a implements e6 {
            public a() {
            }

            @Override // defpackage.e6
            public boolean a(View view, e6.a aVar) {
                j.this.v(((ViewPager2) view).getCurrentItem() + 1);
                return true;
            }
        }

        /* loaded from: classes.dex */
        public class b implements e6 {
            public b() {
            }

            @Override // defpackage.e6
            public boolean a(View view, e6.a aVar) {
                j.this.v(((ViewPager2) view).getCurrentItem() - 1);
                return true;
            }
        }

        /* loaded from: classes.dex */
        public class c extends g {
            public c() {
                super(null);
            }

            @Override // androidx.recyclerview.widget.RecyclerView.i
            public void onChanged() {
                j.this.w();
            }
        }

        public j() {
            super(ViewPager2.this, null);
            this.a = new a();
            this.b = new b();
        }

        @Override // androidx.viewpager2.widget.ViewPager2.e
        public boolean a() {
            return true;
        }

        @Override // androidx.viewpager2.widget.ViewPager2.e
        public boolean c(int i, Bundle bundle) {
            return i == 8192 || i == 4096;
        }

        @Override // androidx.viewpager2.widget.ViewPager2.e
        public void e(RecyclerView.Adapter<?> adapter) {
            w();
            if (adapter != null) {
                adapter.registerAdapterDataObserver(this.c);
            }
        }

        @Override // androidx.viewpager2.widget.ViewPager2.e
        public void f(RecyclerView.Adapter<?> adapter) {
            if (adapter != null) {
                adapter.unregisterAdapterDataObserver(this.c);
            }
        }

        @Override // androidx.viewpager2.widget.ViewPager2.e
        public String g() {
            if (a()) {
                return "androidx.viewpager.widget.ViewPager";
            }
            throw new IllegalStateException();
        }

        @Override // androidx.viewpager2.widget.ViewPager2.e
        public void h(androidx.viewpager2.widget.a aVar, RecyclerView recyclerView) {
            ei4.D0(recyclerView, 2);
            this.c = new c();
            if (ei4.C(ViewPager2.this) == 0) {
                ei4.D0(ViewPager2.this, 1);
            }
        }

        @Override // androidx.viewpager2.widget.ViewPager2.e
        public void i(AccessibilityNodeInfo accessibilityNodeInfo) {
            t(accessibilityNodeInfo);
            if (Build.VERSION.SDK_INT >= 16) {
                u(accessibilityNodeInfo);
            }
        }

        @Override // androidx.viewpager2.widget.ViewPager2.e
        public boolean l(int i, Bundle bundle) {
            int currentItem;
            if (c(i, bundle)) {
                if (i == 8192) {
                    currentItem = ViewPager2.this.getCurrentItem() - 1;
                } else {
                    currentItem = ViewPager2.this.getCurrentItem() + 1;
                }
                v(currentItem);
                return true;
            }
            throw new IllegalStateException();
        }

        @Override // androidx.viewpager2.widget.ViewPager2.e
        public void m() {
            w();
        }

        @Override // androidx.viewpager2.widget.ViewPager2.e
        public void o(AccessibilityEvent accessibilityEvent) {
            accessibilityEvent.setSource(ViewPager2.this);
            accessibilityEvent.setClassName(g());
        }

        @Override // androidx.viewpager2.widget.ViewPager2.e
        public void p() {
            w();
        }

        @Override // androidx.viewpager2.widget.ViewPager2.e
        public void q() {
            w();
        }

        @Override // androidx.viewpager2.widget.ViewPager2.e
        public void r() {
            w();
        }

        @Override // androidx.viewpager2.widget.ViewPager2.e
        public void s() {
            w();
            if (Build.VERSION.SDK_INT < 21) {
                ViewPager2.this.sendAccessibilityEvent(2048);
            }
        }

        public final void t(AccessibilityNodeInfo accessibilityNodeInfo) {
            int i;
            int i2;
            if (ViewPager2.this.getAdapter() == null) {
                i = 0;
                i2 = 0;
            } else if (ViewPager2.this.getOrientation() == 1) {
                i = ViewPager2.this.getAdapter().getItemCount();
                i2 = 0;
            } else {
                i2 = ViewPager2.this.getAdapter().getItemCount();
                i = 0;
            }
            b6.I0(accessibilityNodeInfo).e0(b6.b.b(i, i2, false, 0));
        }

        public final void u(AccessibilityNodeInfo accessibilityNodeInfo) {
            int itemCount;
            RecyclerView.Adapter adapter = ViewPager2.this.getAdapter();
            if (adapter == null || (itemCount = adapter.getItemCount()) == 0 || !ViewPager2.this.e()) {
                return;
            }
            if (ViewPager2.this.h0 > 0) {
                accessibilityNodeInfo.addAction(8192);
            }
            if (ViewPager2.this.h0 < itemCount - 1) {
                accessibilityNodeInfo.addAction(4096);
            }
            accessibilityNodeInfo.setScrollable(true);
        }

        public void v(int i) {
            if (ViewPager2.this.e()) {
                ViewPager2.this.i(i, true);
            }
        }

        public void w() {
            int itemCount;
            ViewPager2 viewPager2 = ViewPager2.this;
            ei4.n0(viewPager2, 16908360);
            ei4.n0(viewPager2, 16908361);
            ei4.n0(viewPager2, 16908358);
            ei4.n0(viewPager2, 16908359);
            if (ViewPager2.this.getAdapter() == null || (itemCount = ViewPager2.this.getAdapter().getItemCount()) == 0 || !ViewPager2.this.e()) {
                return;
            }
            if (ViewPager2.this.getOrientation() == 0) {
                boolean d = ViewPager2.this.d();
                int i = d ? 16908360 : 16908361;
                int i2 = d ? 16908361 : 16908360;
                if (ViewPager2.this.h0 < itemCount - 1) {
                    ei4.p0(viewPager2, new b6.a(i, null), null, this.a);
                }
                if (ViewPager2.this.h0 > 0) {
                    ei4.p0(viewPager2, new b6.a(i2, null), null, this.b);
                    return;
                }
                return;
            }
            if (ViewPager2.this.h0 < itemCount - 1) {
                ei4.p0(viewPager2, new b6.a(16908359, null), null, this.a);
            }
            if (ViewPager2.this.h0 > 0) {
                ei4.p0(viewPager2, new b6.a(16908358, null), null, this.b);
            }
        }
    }

    /* loaded from: classes.dex */
    public interface k {
        void a(View view, float f);
    }

    /* loaded from: classes.dex */
    public class l extends r {
        public l() {
        }

        @Override // androidx.recyclerview.widget.r, androidx.recyclerview.widget.v
        public View h(RecyclerView.LayoutManager layoutManager) {
            if (ViewPager2.this.c()) {
                return null;
            }
            return super.h(layoutManager);
        }
    }

    /* loaded from: classes.dex */
    public class m extends RecyclerView {
        public m(Context context) {
            super(context);
        }

        @Override // androidx.recyclerview.widget.RecyclerView, android.view.ViewGroup, android.view.View
        public CharSequence getAccessibilityClassName() {
            if (ViewPager2.this.x0.d()) {
                return ViewPager2.this.x0.n();
            }
            return super.getAccessibilityClassName();
        }

        @Override // android.view.View
        public void onInitializeAccessibilityEvent(AccessibilityEvent accessibilityEvent) {
            super.onInitializeAccessibilityEvent(accessibilityEvent);
            accessibilityEvent.setFromIndex(ViewPager2.this.h0);
            accessibilityEvent.setToIndex(ViewPager2.this.h0);
            ViewPager2.this.x0.o(accessibilityEvent);
        }

        @Override // androidx.recyclerview.widget.RecyclerView, android.view.ViewGroup
        public boolean onInterceptTouchEvent(MotionEvent motionEvent) {
            return ViewPager2.this.e() && super.onInterceptTouchEvent(motionEvent);
        }

        @Override // androidx.recyclerview.widget.RecyclerView, android.view.View
        @SuppressLint({"ClickableViewAccessibility"})
        public boolean onTouchEvent(MotionEvent motionEvent) {
            return ViewPager2.this.e() && super.onTouchEvent(motionEvent);
        }
    }

    /* loaded from: classes.dex */
    public static class n implements Runnable {
        public final int a;
        public final RecyclerView f0;

        public n(int i, RecyclerView recyclerView) {
            this.a = i;
            this.f0 = recyclerView;
        }

        @Override // java.lang.Runnable
        public void run() {
            this.f0.w1(this.a);
        }
    }

    public ViewPager2(Context context) {
        super(context);
        this.a = new Rect();
        this.f0 = new Rect();
        this.g0 = new androidx.viewpager2.widget.a(3);
        this.i0 = false;
        this.j0 = new a();
        this.l0 = -1;
        this.t0 = null;
        this.u0 = false;
        this.v0 = true;
        this.w0 = -1;
        b(context, null);
    }

    public final RecyclerView.o a() {
        return new d(this);
    }

    public final void b(Context context, AttributeSet attributeSet) {
        this.x0 = y0 ? new j() : new f();
        m mVar = new m(context);
        this.n0 = mVar;
        mVar.setId(ei4.m());
        this.n0.setDescendantFocusability(131072);
        h hVar = new h(context);
        this.k0 = hVar;
        this.n0.setLayoutManager(hVar);
        this.n0.setScrollingTouchSlop(1);
        j(context, attributeSet);
        this.n0.setLayoutParams(new ViewGroup.LayoutParams(-1, -1));
        this.n0.j(a());
        androidx.viewpager2.widget.c cVar = new androidx.viewpager2.widget.c(this);
        this.p0 = cVar;
        this.r0 = new c21(this, cVar, this.n0);
        l lVar = new l();
        this.o0 = lVar;
        lVar.b(this.n0);
        this.n0.l(this.p0);
        androidx.viewpager2.widget.a aVar = new androidx.viewpager2.widget.a(3);
        this.q0 = aVar;
        this.p0.m(aVar);
        b bVar = new b();
        c cVar2 = new c();
        this.q0.d(bVar);
        this.q0.d(cVar2);
        this.x0.h(this.q0, this.n0);
        this.q0.d(this.g0);
        androidx.viewpager2.widget.b bVar2 = new androidx.viewpager2.widget.b(this.k0);
        this.s0 = bVar2;
        this.q0.d(bVar2);
        RecyclerView recyclerView = this.n0;
        attachViewToParent(recyclerView, 0, recyclerView.getLayoutParams());
    }

    public boolean c() {
        return this.r0.a();
    }

    @Override // android.view.View
    public boolean canScrollHorizontally(int i2) {
        return this.n0.canScrollHorizontally(i2);
    }

    @Override // android.view.View
    public boolean canScrollVertically(int i2) {
        return this.n0.canScrollVertically(i2);
    }

    public boolean d() {
        return this.k0.k0() == 1;
    }

    @Override // android.view.ViewGroup, android.view.View
    public void dispatchRestoreInstanceState(SparseArray<Parcelable> sparseArray) {
        Parcelable parcelable = sparseArray.get(getId());
        if (parcelable instanceof SavedState) {
            int i2 = ((SavedState) parcelable).a;
            sparseArray.put(this.n0.getId(), sparseArray.get(i2));
            sparseArray.remove(i2);
        }
        super.dispatchRestoreInstanceState(sparseArray);
        h();
    }

    public boolean e() {
        return this.v0;
    }

    public final void f(RecyclerView.Adapter<?> adapter) {
        if (adapter != null) {
            adapter.registerAdapterDataObserver(this.j0);
        }
    }

    public void g() {
        if (this.s0.d() == null) {
            return;
        }
        double e2 = this.p0.e();
        int i2 = (int) e2;
        float f2 = (float) (e2 - i2);
        this.s0.b(i2, f2, Math.round(getPageSize() * f2));
    }

    @Override // android.view.ViewGroup, android.view.View
    public CharSequence getAccessibilityClassName() {
        if (this.x0.a()) {
            return this.x0.g();
        }
        return super.getAccessibilityClassName();
    }

    public RecyclerView.Adapter getAdapter() {
        return this.n0.getAdapter();
    }

    public int getCurrentItem() {
        return this.h0;
    }

    public int getItemDecorationCount() {
        return this.n0.getItemDecorationCount();
    }

    public int getOffscreenPageLimit() {
        return this.w0;
    }

    public int getOrientation() {
        return this.k0.v2();
    }

    public int getPageSize() {
        int height;
        int paddingBottom;
        RecyclerView recyclerView = this.n0;
        if (getOrientation() == 0) {
            height = recyclerView.getWidth() - recyclerView.getPaddingLeft();
            paddingBottom = recyclerView.getPaddingRight();
        } else {
            height = recyclerView.getHeight() - recyclerView.getPaddingTop();
            paddingBottom = recyclerView.getPaddingBottom();
        }
        return height - paddingBottom;
    }

    public int getScrollState() {
        return this.p0.f();
    }

    public final void h() {
        RecyclerView.Adapter adapter;
        if (this.l0 == -1 || (adapter = getAdapter()) == null) {
            return;
        }
        Parcelable parcelable = this.m0;
        if (parcelable != null) {
            if (adapter instanceof ft3) {
                ((ft3) adapter).b(parcelable);
            }
            this.m0 = null;
        }
        int max = Math.max(0, Math.min(this.l0, adapter.getItemCount() - 1));
        this.h0 = max;
        this.l0 = -1;
        this.n0.o1(max);
        this.x0.m();
    }

    public void i(int i2, boolean z) {
        RecyclerView.Adapter adapter = getAdapter();
        if (adapter == null) {
            if (this.l0 != -1) {
                this.l0 = Math.max(i2, 0);
            }
        } else if (adapter.getItemCount() <= 0) {
        } else {
            int min = Math.min(Math.max(i2, 0), adapter.getItemCount() - 1);
            if (min == this.h0 && this.p0.h()) {
                return;
            }
            int i3 = this.h0;
            if (min == i3 && z) {
                return;
            }
            double d2 = i3;
            this.h0 = min;
            this.x0.q();
            if (!this.p0.h()) {
                d2 = this.p0.e();
            }
            this.p0.k(min, z);
            if (!z) {
                this.n0.o1(min);
                return;
            }
            double d3 = min;
            if (Math.abs(d3 - d2) > 3.0d) {
                this.n0.o1(d3 > d2 ? min - 3 : min + 3);
                RecyclerView recyclerView = this.n0;
                recyclerView.post(new n(min, recyclerView));
                return;
            }
            this.n0.w1(min);
        }
    }

    public final void j(Context context, AttributeSet attributeSet) {
        int[] iArr = g23.ViewPager2;
        TypedArray obtainStyledAttributes = context.obtainStyledAttributes(attributeSet, iArr);
        if (Build.VERSION.SDK_INT >= 29) {
            saveAttributeDataForStyleable(context, iArr, attributeSet, obtainStyledAttributes, 0, 0);
        }
        try {
            setOrientation(obtainStyledAttributes.getInt(g23.ViewPager2_android_orientation, 0));
        } finally {
            obtainStyledAttributes.recycle();
        }
    }

    public final void k(RecyclerView.Adapter<?> adapter) {
        if (adapter != null) {
            adapter.unregisterAdapterDataObserver(this.j0);
        }
    }

    public void l() {
        r rVar = this.o0;
        if (rVar != null) {
            View h2 = rVar.h(this.k0);
            if (h2 == null) {
                return;
            }
            int o0 = this.k0.o0(h2);
            if (o0 != this.h0 && getScrollState() == 0) {
                this.q0.c(o0);
            }
            this.i0 = false;
            return;
        }
        throw new IllegalStateException("Design assumption violated.");
    }

    @Override // android.view.View
    public void onInitializeAccessibilityNodeInfo(AccessibilityNodeInfo accessibilityNodeInfo) {
        super.onInitializeAccessibilityNodeInfo(accessibilityNodeInfo);
        this.x0.i(accessibilityNodeInfo);
    }

    @Override // android.view.ViewGroup, android.view.View
    public void onLayout(boolean z, int i2, int i3, int i4, int i5) {
        int measuredWidth = this.n0.getMeasuredWidth();
        int measuredHeight = this.n0.getMeasuredHeight();
        this.a.left = getPaddingLeft();
        this.a.right = (i4 - i2) - getPaddingRight();
        this.a.top = getPaddingTop();
        this.a.bottom = (i5 - i3) - getPaddingBottom();
        Gravity.apply(8388659, measuredWidth, measuredHeight, this.a, this.f0);
        RecyclerView recyclerView = this.n0;
        Rect rect = this.f0;
        recyclerView.layout(rect.left, rect.top, rect.right, rect.bottom);
        if (this.i0) {
            l();
        }
    }

    @Override // android.view.View
    public void onMeasure(int i2, int i3) {
        measureChild(this.n0, i2, i3);
        int measuredWidth = this.n0.getMeasuredWidth();
        int measuredHeight = this.n0.getMeasuredHeight();
        int measuredState = this.n0.getMeasuredState();
        int paddingLeft = measuredWidth + getPaddingLeft() + getPaddingRight();
        int paddingTop = measuredHeight + getPaddingTop() + getPaddingBottom();
        setMeasuredDimension(ViewGroup.resolveSizeAndState(Math.max(paddingLeft, getSuggestedMinimumWidth()), i2, measuredState), ViewGroup.resolveSizeAndState(Math.max(paddingTop, getSuggestedMinimumHeight()), i3, measuredState << 16));
    }

    @Override // android.view.View
    public void onRestoreInstanceState(Parcelable parcelable) {
        if (!(parcelable instanceof SavedState)) {
            super.onRestoreInstanceState(parcelable);
            return;
        }
        SavedState savedState = (SavedState) parcelable;
        super.onRestoreInstanceState(savedState.getSuperState());
        this.l0 = savedState.f0;
        this.m0 = savedState.g0;
    }

    @Override // android.view.View
    public Parcelable onSaveInstanceState() {
        SavedState savedState = new SavedState(super.onSaveInstanceState());
        savedState.a = this.n0.getId();
        int i2 = this.l0;
        if (i2 == -1) {
            i2 = this.h0;
        }
        savedState.f0 = i2;
        Parcelable parcelable = this.m0;
        if (parcelable != null) {
            savedState.g0 = parcelable;
        } else {
            RecyclerView.Adapter adapter = this.n0.getAdapter();
            if (adapter instanceof ft3) {
                savedState.g0 = ((ft3) adapter).a();
            }
        }
        return savedState;
    }

    @Override // android.view.ViewGroup
    public void onViewAdded(View view) {
        throw new IllegalStateException(ViewPager2.class.getSimpleName() + " does not support direct child views");
    }

    @Override // android.view.View
    public boolean performAccessibilityAction(int i2, Bundle bundle) {
        if (this.x0.c(i2, bundle)) {
            return this.x0.l(i2, bundle);
        }
        return super.performAccessibilityAction(i2, bundle);
    }

    public void setAdapter(RecyclerView.Adapter adapter) {
        RecyclerView.Adapter adapter2 = this.n0.getAdapter();
        this.x0.f(adapter2);
        k(adapter2);
        this.n0.setAdapter(adapter);
        this.h0 = 0;
        h();
        this.x0.e(adapter);
        f(adapter);
    }

    public void setCurrentItem(int i2) {
        setCurrentItem(i2, true);
    }

    @Override // android.view.View
    public void setLayoutDirection(int i2) {
        super.setLayoutDirection(i2);
        this.x0.p();
    }

    public void setOffscreenPageLimit(int i2) {
        if (i2 < 1 && i2 != -1) {
            throw new IllegalArgumentException("Offscreen page limit must be OFFSCREEN_PAGE_LIMIT_DEFAULT or a number > 0");
        }
        this.w0 = i2;
        this.n0.requestLayout();
    }

    public void setOrientation(int i2) {
        this.k0.J2(i2);
        this.x0.r();
    }

    public void setPageTransformer(k kVar) {
        if (kVar != null) {
            if (!this.u0) {
                this.t0 = this.n0.getItemAnimator();
                this.u0 = true;
            }
            this.n0.setItemAnimator(null);
        } else if (this.u0) {
            this.n0.setItemAnimator(this.t0);
            this.t0 = null;
            this.u0 = false;
        }
        if (kVar == this.s0.d()) {
            return;
        }
        this.s0.e(kVar);
        g();
    }

    public void setUserInputEnabled(boolean z) {
        this.v0 = z;
        this.x0.s();
    }

    /* loaded from: classes.dex */
    public static class SavedState extends View.BaseSavedState {
        public static final Parcelable.Creator<SavedState> CREATOR = new a();
        public int a;
        public int f0;
        public Parcelable g0;

        /* loaded from: classes.dex */
        public static class a implements Parcelable.ClassLoaderCreator<SavedState> {
            @Override // android.os.Parcelable.Creator
            /* renamed from: a */
            public SavedState createFromParcel(Parcel parcel) {
                return createFromParcel(parcel, null);
            }

            @Override // android.os.Parcelable.ClassLoaderCreator
            /* renamed from: b */
            public SavedState createFromParcel(Parcel parcel, ClassLoader classLoader) {
                return Build.VERSION.SDK_INT >= 24 ? new SavedState(parcel, classLoader) : new SavedState(parcel);
            }

            @Override // android.os.Parcelable.Creator
            /* renamed from: c */
            public SavedState[] newArray(int i) {
                return new SavedState[i];
            }
        }

        public SavedState(Parcel parcel, ClassLoader classLoader) {
            super(parcel, classLoader);
            a(parcel, classLoader);
        }

        public final void a(Parcel parcel, ClassLoader classLoader) {
            this.a = parcel.readInt();
            this.f0 = parcel.readInt();
            this.g0 = parcel.readParcelable(classLoader);
        }

        @Override // android.view.View.BaseSavedState, android.view.AbsSavedState, android.os.Parcelable
        public void writeToParcel(Parcel parcel, int i) {
            super.writeToParcel(parcel, i);
            parcel.writeInt(this.a);
            parcel.writeInt(this.f0);
            parcel.writeParcelable(this.g0, i);
        }

        public SavedState(Parcel parcel) {
            super(parcel);
            a(parcel, null);
        }

        public SavedState(Parcelable parcelable) {
            super(parcelable);
        }
    }

    public void setCurrentItem(int i2, boolean z) {
        if (!c()) {
            i(i2, z);
            return;
        }
        throw new IllegalStateException("Cannot change current item when ViewPager2 is fake dragging");
    }

    public ViewPager2(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        this.a = new Rect();
        this.f0 = new Rect();
        this.g0 = new androidx.viewpager2.widget.a(3);
        this.i0 = false;
        this.j0 = new a();
        this.l0 = -1;
        this.t0 = null;
        this.u0 = false;
        this.v0 = true;
        this.w0 = -1;
        b(context, attributeSet);
    }

    public ViewPager2(Context context, AttributeSet attributeSet, int i2) {
        super(context, attributeSet, i2);
        this.a = new Rect();
        this.f0 = new Rect();
        this.g0 = new androidx.viewpager2.widget.a(3);
        this.i0 = false;
        this.j0 = new a();
        this.l0 = -1;
        this.t0 = null;
        this.u0 = false;
        this.v0 = true;
        this.w0 = -1;
        b(context, attributeSet);
    }
}
