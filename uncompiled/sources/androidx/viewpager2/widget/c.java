package androidx.viewpager2.widget;

import android.view.View;
import android.view.ViewGroup;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.viewpager2.widget.ViewPager2;
import com.github.mikephil.charting.utils.Utils;
import java.util.Locale;

/* compiled from: ScrollEventAdapter.java */
/* loaded from: classes.dex */
public final class c extends RecyclerView.r {
    public ViewPager2.i a;
    public final ViewPager2 b;
    public final RecyclerView c;
    public final LinearLayoutManager d;
    public int e;
    public int f;
    public a g;
    public int h;
    public int i;
    public boolean j;
    public boolean k;
    public boolean l;
    public boolean m;

    /* compiled from: ScrollEventAdapter.java */
    /* loaded from: classes.dex */
    public static final class a {
        public int a;
        public float b;
        public int c;

        public void a() {
            this.a = -1;
            this.b = Utils.FLOAT_EPSILON;
            this.c = 0;
        }
    }

    public c(ViewPager2 viewPager2) {
        this.b = viewPager2;
        RecyclerView recyclerView = viewPager2.n0;
        this.c = recyclerView;
        this.d = (LinearLayoutManager) recyclerView.getLayoutManager();
        this.g = new a();
        l();
    }

    public final void a(int i, float f, int i2) {
        ViewPager2.i iVar = this.a;
        if (iVar != null) {
            iVar.b(i, f, i2);
        }
    }

    public final void b(int i) {
        ViewPager2.i iVar = this.a;
        if (iVar != null) {
            iVar.c(i);
        }
    }

    public final void c(int i) {
        if ((this.e == 3 && this.f == 0) || this.f == i) {
            return;
        }
        this.f = i;
        ViewPager2.i iVar = this.a;
        if (iVar != null) {
            iVar.a(i);
        }
    }

    public final int d() {
        return this.d.h2();
    }

    public double e() {
        o();
        a aVar = this.g;
        return aVar.a + aVar.b;
    }

    public int f() {
        return this.f;
    }

    public boolean g() {
        return this.m;
    }

    public boolean h() {
        return this.f == 0;
    }

    public final boolean i() {
        int i = this.e;
        return i == 1 || i == 4;
    }

    public void j() {
        this.l = true;
    }

    public void k(int i, boolean z) {
        this.e = z ? 2 : 3;
        this.m = false;
        boolean z2 = this.i != i;
        this.i = i;
        c(2);
        if (z2) {
            b(i);
        }
    }

    public final void l() {
        this.e = 0;
        this.f = 0;
        this.g.a();
        this.h = -1;
        this.i = -1;
        this.j = false;
        this.k = false;
        this.m = false;
        this.l = false;
    }

    public void m(ViewPager2.i iVar) {
        this.a = iVar;
    }

    public final void n(boolean z) {
        this.m = z;
        this.e = z ? 4 : 1;
        int i = this.i;
        if (i != -1) {
            this.h = i;
            this.i = -1;
        } else if (this.h == -1) {
            this.h = d();
        }
        c(1);
    }

    public final void o() {
        int top;
        a aVar = this.g;
        int h2 = this.d.h2();
        aVar.a = h2;
        if (h2 == -1) {
            aVar.a();
            return;
        }
        View N = this.d.N(h2);
        if (N == null) {
            aVar.a();
            return;
        }
        int l0 = this.d.l0(N);
        int q0 = this.d.q0(N);
        int t0 = this.d.t0(N);
        int S = this.d.S(N);
        ViewGroup.LayoutParams layoutParams = N.getLayoutParams();
        if (layoutParams instanceof ViewGroup.MarginLayoutParams) {
            ViewGroup.MarginLayoutParams marginLayoutParams = (ViewGroup.MarginLayoutParams) layoutParams;
            l0 += marginLayoutParams.leftMargin;
            q0 += marginLayoutParams.rightMargin;
            t0 += marginLayoutParams.topMargin;
            S += marginLayoutParams.bottomMargin;
        }
        int height = N.getHeight() + t0 + S;
        int width = N.getWidth() + l0 + q0;
        if (this.d.v2() == 0) {
            top = (N.getLeft() - l0) - this.c.getPaddingLeft();
            if (this.b.d()) {
                top = -top;
            }
            height = width;
        } else {
            top = (N.getTop() - t0) - this.c.getPaddingTop();
        }
        int i = -top;
        aVar.c = i;
        if (i < 0) {
            if (new id(this.d).d()) {
                throw new IllegalStateException("Page(s) contain a ViewGroup with a LayoutTransition (or animateLayoutChanges=\"true\"), which interferes with the scrolling animation. Make sure to call getLayoutTransition().setAnimateParentHierarchy(false) on all ViewGroups with a LayoutTransition before an animation is started.");
            }
            throw new IllegalStateException(String.format(Locale.US, "Page can only be offset by a positive amount, not by %d", Integer.valueOf(aVar.c)));
        }
        aVar.b = height == 0 ? Utils.FLOAT_EPSILON : i / height;
    }

    @Override // androidx.recyclerview.widget.RecyclerView.r
    public void onScrollStateChanged(RecyclerView recyclerView, int i) {
        boolean z = true;
        if ((this.e != 1 || this.f != 1) && i == 1) {
            n(false);
        } else if (i() && i == 2) {
            if (this.k) {
                c(2);
                this.j = true;
            }
        } else {
            if (i() && i == 0) {
                o();
                if (!this.k) {
                    int i2 = this.g.a;
                    if (i2 != -1) {
                        a(i2, Utils.FLOAT_EPSILON, 0);
                    }
                } else {
                    a aVar = this.g;
                    if (aVar.c == 0) {
                        int i3 = this.h;
                        int i4 = aVar.a;
                        if (i3 != i4) {
                            b(i4);
                        }
                    } else {
                        z = false;
                    }
                }
                if (z) {
                    c(0);
                    l();
                }
            }
            if (this.e == 2 && i == 0 && this.l) {
                o();
                a aVar2 = this.g;
                if (aVar2.c == 0) {
                    int i5 = this.i;
                    int i6 = aVar2.a;
                    if (i5 != i6) {
                        if (i6 == -1) {
                            i6 = 0;
                        }
                        b(i6);
                    }
                    c(0);
                    l();
                }
            }
        }
    }

    /* JADX WARN: Code restructure failed: missing block: B:11:0x001d, code lost:
        if ((r5 < 0) == r3.b.d()) goto L36;
     */
    /* JADX WARN: Removed duplicated region for block: B:16:0x0025  */
    /* JADX WARN: Removed duplicated region for block: B:22:0x0039  */
    @Override // androidx.recyclerview.widget.RecyclerView.r
    /*
        Code decompiled incorrectly, please refer to instructions dump.
        To view partially-correct code enable 'Show inconsistent code' option in preferences
    */
    public void onScrolled(androidx.recyclerview.widget.RecyclerView r4, int r5, int r6) {
        /*
            r3 = this;
            r4 = 1
            r3.k = r4
            r3.o()
            boolean r0 = r3.j
            r1 = -1
            r2 = 0
            if (r0 == 0) goto L3d
            r3.j = r2
            if (r6 > 0) goto L22
            if (r6 != 0) goto L20
            if (r5 >= 0) goto L16
            r5 = r4
            goto L17
        L16:
            r5 = r2
        L17:
            androidx.viewpager2.widget.ViewPager2 r6 = r3.b
            boolean r6 = r6.d()
            if (r5 != r6) goto L20
            goto L22
        L20:
            r5 = r2
            goto L23
        L22:
            r5 = r4
        L23:
            if (r5 == 0) goto L2f
            androidx.viewpager2.widget.c$a r5 = r3.g
            int r6 = r5.c
            if (r6 == 0) goto L2f
            int r5 = r5.a
            int r5 = r5 + r4
            goto L33
        L2f:
            androidx.viewpager2.widget.c$a r5 = r3.g
            int r5 = r5.a
        L33:
            r3.i = r5
            int r6 = r3.h
            if (r6 == r5) goto L4b
            r3.b(r5)
            goto L4b
        L3d:
            int r5 = r3.e
            if (r5 != 0) goto L4b
            androidx.viewpager2.widget.c$a r5 = r3.g
            int r5 = r5.a
            if (r5 != r1) goto L48
            r5 = r2
        L48:
            r3.b(r5)
        L4b:
            androidx.viewpager2.widget.c$a r5 = r3.g
            int r6 = r5.a
            if (r6 != r1) goto L52
            r6 = r2
        L52:
            float r0 = r5.b
            int r5 = r5.c
            r3.a(r6, r0, r5)
            androidx.viewpager2.widget.c$a r5 = r3.g
            int r6 = r5.a
            int r0 = r3.i
            if (r6 == r0) goto L63
            if (r0 != r1) goto L71
        L63:
            int r5 = r5.c
            if (r5 != 0) goto L71
            int r5 = r3.f
            if (r5 == r4) goto L71
            r3.c(r2)
            r3.l()
        L71:
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: androidx.viewpager2.widget.c.onScrolled(androidx.recyclerview.widget.RecyclerView, int, int):void");
    }
}
