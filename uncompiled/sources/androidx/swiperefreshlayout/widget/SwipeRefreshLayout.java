package androidx.swiperefreshlayout.widget;

import android.content.Context;
import android.content.res.TypedArray;
import android.os.Build;
import android.os.Parcel;
import android.os.Parcelable;
import android.util.AttributeSet;
import android.util.DisplayMetrics;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewConfiguration;
import android.view.ViewGroup;
import android.view.ViewParent;
import android.view.animation.Animation;
import android.view.animation.DecelerateInterpolator;
import android.view.animation.Transformation;
import android.widget.AbsListView;
import android.widget.ListView;
import com.github.mikephil.charting.utils.Utils;

/* loaded from: classes.dex */
public class SwipeRefreshLayout extends ViewGroup implements se2, re2, pe2, te2 {
    public static final String U0 = SwipeRefreshLayout.class.getSimpleName();
    public static final int[] V0 = {16842766};
    public int A0;
    public int B0;
    public float C0;
    public int D0;
    public int E0;
    public int F0;
    public yy G0;
    public Animation H0;
    public Animation I0;
    public Animation J0;
    public Animation K0;
    public Animation L0;
    public boolean M0;
    public int N0;
    public boolean O0;
    public i P0;
    public boolean Q0;
    public Animation.AnimationListener R0;
    public final Animation S0;
    public final Animation T0;
    public View a;
    public j f0;
    public boolean g0;
    public int h0;
    public float i0;
    public float j0;
    public final ue2 k0;
    public final qe2 l0;
    public final int[] m0;
    public final int[] n0;
    public final int[] o0;
    public boolean p0;
    public int q0;
    public int r0;
    public float s0;
    public float t0;
    public boolean u0;
    public int v0;
    public boolean w0;
    public boolean x0;
    public final DecelerateInterpolator y0;
    public vy z0;

    /* loaded from: classes.dex */
    public class a implements Animation.AnimationListener {
        public a() {
        }

        @Override // android.view.animation.Animation.AnimationListener
        public void onAnimationEnd(Animation animation) {
            j jVar;
            SwipeRefreshLayout swipeRefreshLayout = SwipeRefreshLayout.this;
            if (swipeRefreshLayout.g0) {
                swipeRefreshLayout.G0.setAlpha(255);
                SwipeRefreshLayout.this.G0.start();
                SwipeRefreshLayout swipeRefreshLayout2 = SwipeRefreshLayout.this;
                if (swipeRefreshLayout2.M0 && (jVar = swipeRefreshLayout2.f0) != null) {
                    jVar.onRefresh();
                }
                SwipeRefreshLayout swipeRefreshLayout3 = SwipeRefreshLayout.this;
                swipeRefreshLayout3.r0 = swipeRefreshLayout3.z0.getTop();
                return;
            }
            swipeRefreshLayout.m();
        }

        @Override // android.view.animation.Animation.AnimationListener
        public void onAnimationRepeat(Animation animation) {
        }

        @Override // android.view.animation.Animation.AnimationListener
        public void onAnimationStart(Animation animation) {
        }
    }

    /* loaded from: classes.dex */
    public class b extends Animation {
        public b() {
        }

        @Override // android.view.animation.Animation
        public void applyTransformation(float f, Transformation transformation) {
            SwipeRefreshLayout.this.setAnimationProgress(f);
        }
    }

    /* loaded from: classes.dex */
    public class c extends Animation {
        public c() {
        }

        @Override // android.view.animation.Animation
        public void applyTransformation(float f, Transformation transformation) {
            SwipeRefreshLayout.this.setAnimationProgress(1.0f - f);
        }
    }

    /* loaded from: classes.dex */
    public class d extends Animation {
        public final /* synthetic */ int a;
        public final /* synthetic */ int f0;

        public d(int i, int i2) {
            this.a = i;
            this.f0 = i2;
        }

        @Override // android.view.animation.Animation
        public void applyTransformation(float f, Transformation transformation) {
            yy yyVar = SwipeRefreshLayout.this.G0;
            int i = this.a;
            yyVar.setAlpha((int) (i + ((this.f0 - i) * f)));
        }
    }

    /* loaded from: classes.dex */
    public class e implements Animation.AnimationListener {
        public e() {
        }

        @Override // android.view.animation.Animation.AnimationListener
        public void onAnimationEnd(Animation animation) {
            SwipeRefreshLayout swipeRefreshLayout = SwipeRefreshLayout.this;
            if (swipeRefreshLayout.w0) {
                return;
            }
            swipeRefreshLayout.s(null);
        }

        @Override // android.view.animation.Animation.AnimationListener
        public void onAnimationRepeat(Animation animation) {
        }

        @Override // android.view.animation.Animation.AnimationListener
        public void onAnimationStart(Animation animation) {
        }
    }

    /* loaded from: classes.dex */
    public class f extends Animation {
        public f() {
        }

        @Override // android.view.animation.Animation
        public void applyTransformation(float f, Transformation transformation) {
            int i;
            SwipeRefreshLayout swipeRefreshLayout = SwipeRefreshLayout.this;
            if (!swipeRefreshLayout.O0) {
                i = swipeRefreshLayout.E0 - Math.abs(swipeRefreshLayout.D0);
            } else {
                i = swipeRefreshLayout.E0;
            }
            SwipeRefreshLayout swipeRefreshLayout2 = SwipeRefreshLayout.this;
            int i2 = swipeRefreshLayout2.B0;
            SwipeRefreshLayout.this.setTargetOffsetTopAndBottom((i2 + ((int) ((i - i2) * f))) - swipeRefreshLayout2.z0.getTop());
            SwipeRefreshLayout.this.G0.e(1.0f - f);
        }
    }

    /* loaded from: classes.dex */
    public class g extends Animation {
        public g() {
        }

        @Override // android.view.animation.Animation
        public void applyTransformation(float f, Transformation transformation) {
            SwipeRefreshLayout.this.k(f);
        }
    }

    /* loaded from: classes.dex */
    public class h extends Animation {
        public h() {
        }

        @Override // android.view.animation.Animation
        public void applyTransformation(float f, Transformation transformation) {
            SwipeRefreshLayout swipeRefreshLayout = SwipeRefreshLayout.this;
            float f2 = swipeRefreshLayout.C0;
            swipeRefreshLayout.setAnimationProgress(f2 + ((-f2) * f));
            SwipeRefreshLayout.this.k(f);
        }
    }

    /* loaded from: classes.dex */
    public interface i {
        boolean a(SwipeRefreshLayout swipeRefreshLayout, View view);
    }

    /* loaded from: classes.dex */
    public interface j {
        void onRefresh();
    }

    public SwipeRefreshLayout(Context context) {
        this(context, null);
    }

    private void setColorViewAlpha(int i2) {
        this.z0.getBackground().setAlpha(i2);
        this.G0.setAlpha(i2);
    }

    public final void a(int i2, Animation.AnimationListener animationListener) {
        this.B0 = i2;
        this.S0.reset();
        this.S0.setDuration(200L);
        this.S0.setInterpolator(this.y0);
        if (animationListener != null) {
            this.z0.b(animationListener);
        }
        this.z0.clearAnimation();
        this.z0.startAnimation(this.S0);
    }

    public final void b(int i2, Animation.AnimationListener animationListener) {
        if (this.w0) {
            t(i2, animationListener);
            return;
        }
        this.B0 = i2;
        this.T0.reset();
        this.T0.setDuration(200L);
        this.T0.setInterpolator(this.y0);
        if (animationListener != null) {
            this.z0.b(animationListener);
        }
        this.z0.clearAnimation();
        this.z0.startAnimation(this.T0);
    }

    public boolean c() {
        i iVar = this.P0;
        if (iVar != null) {
            return iVar.a(this, this.a);
        }
        View view = this.a;
        if (view instanceof ListView) {
            return k02.a((ListView) view, -1);
        }
        return view.canScrollVertically(-1);
    }

    public final void d() {
        this.z0 = new vy(getContext());
        yy yyVar = new yy(getContext());
        this.G0 = yyVar;
        yyVar.l(1);
        this.z0.setImageDrawable(this.G0);
        this.z0.setVisibility(8);
        addView(this.z0);
    }

    @Override // android.view.View
    public boolean dispatchNestedFling(float f2, float f3, boolean z) {
        return this.l0.a(f2, f3, z);
    }

    @Override // android.view.View
    public boolean dispatchNestedPreFling(float f2, float f3) {
        return this.l0.b(f2, f3);
    }

    @Override // android.view.View
    public boolean dispatchNestedPreScroll(int i2, int i3, int[] iArr, int[] iArr2) {
        return this.l0.c(i2, i3, iArr, iArr2);
    }

    @Override // android.view.View
    public boolean dispatchNestedScroll(int i2, int i3, int i4, int i5, int[] iArr) {
        return this.l0.f(i2, i3, i4, i5, iArr);
    }

    public void e(int i2, int i3, int i4, int i5, int[] iArr, int i6, int[] iArr2) {
        if (i6 == 0) {
            this.l0.e(i2, i3, i4, i5, iArr, i6, iArr2);
        }
    }

    public final void f() {
        if (this.a == null) {
            for (int i2 = 0; i2 < getChildCount(); i2++) {
                View childAt = getChildAt(i2);
                if (!childAt.equals(this.z0)) {
                    this.a = childAt;
                    return;
                }
            }
        }
    }

    public final void g(float f2) {
        if (f2 > this.i0) {
            n(true, true);
            return;
        }
        this.g0 = false;
        this.G0.j(Utils.FLOAT_EPSILON, Utils.FLOAT_EPSILON);
        b(this.r0, this.w0 ? null : new e());
        this.G0.d(false);
    }

    @Override // android.view.ViewGroup
    public int getChildDrawingOrder(int i2, int i3) {
        int i4 = this.A0;
        return i4 < 0 ? i3 : i3 == i2 + (-1) ? i4 : i3 >= i4 ? i3 + 1 : i3;
    }

    @Override // android.view.ViewGroup
    public int getNestedScrollAxes() {
        return this.k0.a();
    }

    public int getProgressCircleDiameter() {
        return this.N0;
    }

    public int getProgressViewEndOffset() {
        return this.E0;
    }

    public int getProgressViewStartOffset() {
        return this.D0;
    }

    public final boolean h(Animation animation) {
        return (animation == null || !animation.hasStarted() || animation.hasEnded()) ? false : true;
    }

    @Override // android.view.View
    public boolean hasNestedScrollingParent() {
        return this.l0.j();
    }

    public boolean i() {
        return this.g0;
    }

    @Override // android.view.View, defpackage.pe2
    public boolean isNestedScrollingEnabled() {
        return this.l0.l();
    }

    public final void j(float f2) {
        this.G0.d(true);
        float min = Math.min(1.0f, Math.abs(f2 / this.i0));
        float max = (((float) Math.max(min - 0.4d, (double) Utils.DOUBLE_EPSILON)) * 5.0f) / 3.0f;
        float abs = Math.abs(f2) - this.i0;
        int i2 = this.F0;
        if (i2 <= 0) {
            if (this.O0) {
                i2 = this.E0 - this.D0;
            } else {
                i2 = this.E0;
            }
        }
        float f3 = i2;
        double max2 = Math.max((float) Utils.FLOAT_EPSILON, Math.min(abs, f3 * 2.0f) / f3) / 4.0f;
        float pow = ((float) (max2 - Math.pow(max2, 2.0d))) * 2.0f;
        int i3 = this.D0 + ((int) ((f3 * min) + (f3 * pow * 2.0f)));
        if (this.z0.getVisibility() != 0) {
            this.z0.setVisibility(0);
        }
        if (!this.w0) {
            this.z0.setScaleX(1.0f);
            this.z0.setScaleY(1.0f);
        }
        if (this.w0) {
            setAnimationProgress(Math.min(1.0f, f2 / this.i0));
        }
        if (f2 < this.i0) {
            if (this.G0.getAlpha() > 76 && !h(this.J0)) {
                r();
            }
        } else if (this.G0.getAlpha() < 255 && !h(this.K0)) {
            q();
        }
        this.G0.j(Utils.FLOAT_EPSILON, Math.min(0.8f, max * 0.8f));
        this.G0.e(Math.min(1.0f, max));
        this.G0.g((((max * 0.4f) - 0.25f) + (pow * 2.0f)) * 0.5f);
        setTargetOffsetTopAndBottom(i3 - this.r0);
    }

    public void k(float f2) {
        int i2 = this.B0;
        setTargetOffsetTopAndBottom((i2 + ((int) ((this.D0 - i2) * f2))) - this.z0.getTop());
    }

    public final void l(MotionEvent motionEvent) {
        int actionIndex = motionEvent.getActionIndex();
        if (motionEvent.getPointerId(actionIndex) == this.v0) {
            this.v0 = motionEvent.getPointerId(actionIndex == 0 ? 1 : 0);
        }
    }

    public void m() {
        this.z0.clearAnimation();
        this.G0.stop();
        this.z0.setVisibility(8);
        setColorViewAlpha(255);
        if (this.w0) {
            setAnimationProgress(Utils.FLOAT_EPSILON);
        } else {
            setTargetOffsetTopAndBottom(this.D0 - this.r0);
        }
        this.r0 = this.z0.getTop();
    }

    public final void n(boolean z, boolean z2) {
        if (this.g0 != z) {
            this.M0 = z2;
            f();
            this.g0 = z;
            if (z) {
                a(this.r0, this.R0);
            } else {
                s(this.R0);
            }
        }
    }

    public final Animation o(int i2, int i3) {
        d dVar = new d(i2, i3);
        dVar.setDuration(300L);
        this.z0.b(null);
        this.z0.clearAnimation();
        this.z0.startAnimation(dVar);
        return dVar;
    }

    @Override // android.view.ViewGroup, android.view.View
    public void onDetachedFromWindow() {
        super.onDetachedFromWindow();
        m();
    }

    @Override // android.view.ViewGroup
    public boolean onInterceptTouchEvent(MotionEvent motionEvent) {
        int findPointerIndex;
        f();
        int actionMasked = motionEvent.getActionMasked();
        if (this.x0 && actionMasked == 0) {
            this.x0 = false;
        }
        if (!isEnabled() || this.x0 || c() || this.g0 || this.p0) {
            return false;
        }
        if (actionMasked != 0) {
            if (actionMasked != 1) {
                if (actionMasked == 2) {
                    int i2 = this.v0;
                    if (i2 == -1 || (findPointerIndex = motionEvent.findPointerIndex(i2)) < 0) {
                        return false;
                    }
                    p(motionEvent.getY(findPointerIndex));
                } else if (actionMasked != 3) {
                    if (actionMasked == 6) {
                        l(motionEvent);
                    }
                }
            }
            this.u0 = false;
            this.v0 = -1;
        } else {
            setTargetOffsetTopAndBottom(this.D0 - this.z0.getTop());
            int pointerId = motionEvent.getPointerId(0);
            this.v0 = pointerId;
            this.u0 = false;
            int findPointerIndex2 = motionEvent.findPointerIndex(pointerId);
            if (findPointerIndex2 < 0) {
                return false;
            }
            this.t0 = motionEvent.getY(findPointerIndex2);
        }
        return this.u0;
    }

    @Override // android.view.ViewGroup, android.view.View
    public void onLayout(boolean z, int i2, int i3, int i4, int i5) {
        int measuredWidth = getMeasuredWidth();
        int measuredHeight = getMeasuredHeight();
        if (getChildCount() == 0) {
            return;
        }
        if (this.a == null) {
            f();
        }
        View view = this.a;
        if (view == null) {
            return;
        }
        int paddingLeft = getPaddingLeft();
        int paddingTop = getPaddingTop();
        view.layout(paddingLeft, paddingTop, ((measuredWidth - getPaddingLeft()) - getPaddingRight()) + paddingLeft, ((measuredHeight - getPaddingTop()) - getPaddingBottom()) + paddingTop);
        int measuredWidth2 = this.z0.getMeasuredWidth();
        int measuredHeight2 = this.z0.getMeasuredHeight();
        int i6 = measuredWidth / 2;
        int i7 = measuredWidth2 / 2;
        int i8 = this.r0;
        this.z0.layout(i6 - i7, i8, i6 + i7, measuredHeight2 + i8);
    }

    @Override // android.view.View
    public void onMeasure(int i2, int i3) {
        super.onMeasure(i2, i3);
        if (this.a == null) {
            f();
        }
        View view = this.a;
        if (view == null) {
            return;
        }
        view.measure(View.MeasureSpec.makeMeasureSpec((getMeasuredWidth() - getPaddingLeft()) - getPaddingRight(), 1073741824), View.MeasureSpec.makeMeasureSpec((getMeasuredHeight() - getPaddingTop()) - getPaddingBottom(), 1073741824));
        this.z0.measure(View.MeasureSpec.makeMeasureSpec(this.N0, 1073741824), View.MeasureSpec.makeMeasureSpec(this.N0, 1073741824));
        this.A0 = -1;
        for (int i4 = 0; i4 < getChildCount(); i4++) {
            if (getChildAt(i4) == this.z0) {
                this.A0 = i4;
                return;
            }
        }
    }

    @Override // android.view.ViewGroup, android.view.ViewParent, defpackage.te2
    public boolean onNestedFling(View view, float f2, float f3, boolean z) {
        return dispatchNestedFling(f2, f3, z);
    }

    @Override // android.view.ViewGroup, android.view.ViewParent, defpackage.te2
    public boolean onNestedPreFling(View view, float f2, float f3) {
        return dispatchNestedPreFling(f2, f3);
    }

    @Override // defpackage.re2
    public void onNestedPreScroll(View view, int i2, int i3, int[] iArr, int i4) {
        if (i4 == 0) {
            onNestedPreScroll(view, i2, i3, iArr);
        }
    }

    @Override // defpackage.se2
    public void onNestedScroll(View view, int i2, int i3, int i4, int i5, int i6, int[] iArr) {
        if (i6 != 0) {
            return;
        }
        int i7 = iArr[1];
        e(i2, i3, i4, i5, this.n0, i6, iArr);
        int i8 = i5 - (iArr[1] - i7);
        int i9 = i8 == 0 ? i5 + this.n0[1] : i8;
        if (i9 >= 0 || c()) {
            return;
        }
        float abs = this.j0 + Math.abs(i9);
        this.j0 = abs;
        j(abs);
        iArr[1] = iArr[1] + i8;
    }

    @Override // defpackage.re2
    public void onNestedScrollAccepted(View view, View view2, int i2, int i3) {
        if (i3 == 0) {
            onNestedScrollAccepted(view, view2, i2);
        }
    }

    @Override // android.view.View
    public void onRestoreInstanceState(Parcelable parcelable) {
        SavedState savedState = (SavedState) parcelable;
        super.onRestoreInstanceState(savedState.getSuperState());
        setRefreshing(savedState.a);
    }

    @Override // android.view.View
    public Parcelable onSaveInstanceState() {
        return new SavedState(super.onSaveInstanceState(), this.g0);
    }

    @Override // defpackage.re2
    public boolean onStartNestedScroll(View view, View view2, int i2, int i3) {
        if (i3 == 0) {
            return onStartNestedScroll(view, view2, i2);
        }
        return false;
    }

    @Override // defpackage.re2
    public void onStopNestedScroll(View view, int i2) {
        if (i2 == 0) {
            onStopNestedScroll(view);
        }
    }

    @Override // android.view.View
    public boolean onTouchEvent(MotionEvent motionEvent) {
        int actionMasked = motionEvent.getActionMasked();
        if (this.x0 && actionMasked == 0) {
            this.x0 = false;
        }
        if (!isEnabled() || this.x0 || c() || this.g0 || this.p0) {
            return false;
        }
        if (actionMasked == 0) {
            this.v0 = motionEvent.getPointerId(0);
            this.u0 = false;
        } else if (actionMasked == 1) {
            int findPointerIndex = motionEvent.findPointerIndex(this.v0);
            if (findPointerIndex < 0) {
                return false;
            }
            if (this.u0) {
                this.u0 = false;
                g((motionEvent.getY(findPointerIndex) - this.s0) * 0.5f);
            }
            this.v0 = -1;
            return false;
        } else if (actionMasked == 2) {
            int findPointerIndex2 = motionEvent.findPointerIndex(this.v0);
            if (findPointerIndex2 < 0) {
                return false;
            }
            float y = motionEvent.getY(findPointerIndex2);
            p(y);
            if (this.u0) {
                float f2 = (y - this.s0) * 0.5f;
                if (f2 <= Utils.FLOAT_EPSILON) {
                    return false;
                }
                getParent().requestDisallowInterceptTouchEvent(true);
                j(f2);
            }
        } else if (actionMasked == 3) {
            return false;
        } else {
            if (actionMasked == 5) {
                int actionIndex = motionEvent.getActionIndex();
                if (actionIndex < 0) {
                    return false;
                }
                this.v0 = motionEvent.getPointerId(actionIndex);
            } else if (actionMasked == 6) {
                l(motionEvent);
            }
        }
        return true;
    }

    public final void p(float f2) {
        float f3 = this.t0;
        int i2 = this.h0;
        if (f2 - f3 <= i2 || this.u0) {
            return;
        }
        this.s0 = f3 + i2;
        this.u0 = true;
        this.G0.setAlpha(76);
    }

    public final void q() {
        this.K0 = o(this.G0.getAlpha(), 255);
    }

    public final void r() {
        this.J0 = o(this.G0.getAlpha(), 76);
    }

    @Override // android.view.ViewGroup, android.view.ViewParent
    public void requestDisallowInterceptTouchEvent(boolean z) {
        View view;
        ViewParent parent;
        if ((Build.VERSION.SDK_INT < 21 && (this.a instanceof AbsListView)) || ((view = this.a) != null && !ei4.X(view))) {
            if (this.Q0 || (parent = getParent()) == null) {
                return;
            }
            parent.requestDisallowInterceptTouchEvent(z);
            return;
        }
        super.requestDisallowInterceptTouchEvent(z);
    }

    public void s(Animation.AnimationListener animationListener) {
        c cVar = new c();
        this.I0 = cVar;
        cVar.setDuration(150L);
        this.z0.b(animationListener);
        this.z0.clearAnimation();
        this.z0.startAnimation(this.I0);
    }

    public void setAnimationProgress(float f2) {
        this.z0.setScaleX(f2);
        this.z0.setScaleY(f2);
    }

    @Deprecated
    public void setColorScheme(int... iArr) {
        setColorSchemeResources(iArr);
    }

    public void setColorSchemeColors(int... iArr) {
        f();
        this.G0.f(iArr);
    }

    public void setColorSchemeResources(int... iArr) {
        Context context = getContext();
        int[] iArr2 = new int[iArr.length];
        for (int i2 = 0; i2 < iArr.length; i2++) {
            iArr2[i2] = m70.d(context, iArr[i2]);
        }
        setColorSchemeColors(iArr2);
    }

    public void setDistanceToTriggerSync(int i2) {
        this.i0 = i2;
    }

    @Override // android.view.View
    public void setEnabled(boolean z) {
        super.setEnabled(z);
        if (z) {
            return;
        }
        m();
    }

    @Deprecated
    public void setLegacyRequestDisallowInterceptTouchEventEnabled(boolean z) {
        this.Q0 = z;
    }

    @Override // android.view.View
    public void setNestedScrollingEnabled(boolean z) {
        this.l0.m(z);
    }

    public void setOnChildScrollUpCallback(i iVar) {
        this.P0 = iVar;
    }

    public void setOnRefreshListener(j jVar) {
        this.f0 = jVar;
    }

    @Deprecated
    public void setProgressBackgroundColor(int i2) {
        setProgressBackgroundColorSchemeResource(i2);
    }

    public void setProgressBackgroundColorSchemeColor(int i2) {
        this.z0.setBackgroundColor(i2);
    }

    public void setProgressBackgroundColorSchemeResource(int i2) {
        setProgressBackgroundColorSchemeColor(m70.d(getContext(), i2));
    }

    public void setProgressViewEndTarget(boolean z, int i2) {
        this.E0 = i2;
        this.w0 = z;
        this.z0.invalidate();
    }

    public void setProgressViewOffset(boolean z, int i2, int i3) {
        this.w0 = z;
        this.D0 = i2;
        this.E0 = i3;
        this.O0 = true;
        m();
        this.g0 = false;
    }

    public void setRefreshing(boolean z) {
        int i2;
        if (z && this.g0 != z) {
            this.g0 = z;
            if (!this.O0) {
                i2 = this.E0 + this.D0;
            } else {
                i2 = this.E0;
            }
            setTargetOffsetTopAndBottom(i2 - this.r0);
            this.M0 = false;
            u(this.R0);
            return;
        }
        n(z, false);
    }

    public void setSize(int i2) {
        if (i2 == 0 || i2 == 1) {
            DisplayMetrics displayMetrics = getResources().getDisplayMetrics();
            if (i2 == 0) {
                this.N0 = (int) (displayMetrics.density * 56.0f);
            } else {
                this.N0 = (int) (displayMetrics.density * 40.0f);
            }
            this.z0.setImageDrawable(null);
            this.G0.l(i2);
            this.z0.setImageDrawable(this.G0);
        }
    }

    public void setSlingshotDistance(int i2) {
        this.F0 = i2;
    }

    public void setTargetOffsetTopAndBottom(int i2) {
        this.z0.bringToFront();
        ei4.d0(this.z0, i2);
        this.r0 = this.z0.getTop();
    }

    @Override // android.view.View
    public boolean startNestedScroll(int i2) {
        return this.l0.o(i2);
    }

    @Override // android.view.View, defpackage.pe2
    public void stopNestedScroll() {
        this.l0.q();
    }

    public final void t(int i2, Animation.AnimationListener animationListener) {
        this.B0 = i2;
        this.C0 = this.z0.getScaleX();
        h hVar = new h();
        this.L0 = hVar;
        hVar.setDuration(150L);
        if (animationListener != null) {
            this.z0.b(animationListener);
        }
        this.z0.clearAnimation();
        this.z0.startAnimation(this.L0);
    }

    public final void u(Animation.AnimationListener animationListener) {
        this.z0.setVisibility(0);
        this.G0.setAlpha(255);
        b bVar = new b();
        this.H0 = bVar;
        bVar.setDuration(this.q0);
        if (animationListener != null) {
            this.z0.b(animationListener);
        }
        this.z0.clearAnimation();
        this.z0.startAnimation(this.H0);
    }

    /* loaded from: classes.dex */
    public static class SavedState extends View.BaseSavedState {
        public static final Parcelable.Creator<SavedState> CREATOR = new a();
        public final boolean a;

        /* loaded from: classes.dex */
        public class a implements Parcelable.Creator<SavedState> {
            @Override // android.os.Parcelable.Creator
            /* renamed from: a */
            public SavedState createFromParcel(Parcel parcel) {
                return new SavedState(parcel);
            }

            @Override // android.os.Parcelable.Creator
            /* renamed from: b */
            public SavedState[] newArray(int i) {
                return new SavedState[i];
            }
        }

        public SavedState(Parcelable parcelable, boolean z) {
            super(parcelable);
            this.a = z;
        }

        @Override // android.view.View.BaseSavedState, android.view.AbsSavedState, android.os.Parcelable
        public void writeToParcel(Parcel parcel, int i) {
            super.writeToParcel(parcel, i);
            parcel.writeByte(this.a ? (byte) 1 : (byte) 0);
        }

        public SavedState(Parcel parcel) {
            super(parcel);
            this.a = parcel.readByte() != 0;
        }
    }

    public SwipeRefreshLayout(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        this.g0 = false;
        this.i0 = -1.0f;
        this.m0 = new int[2];
        this.n0 = new int[2];
        this.o0 = new int[2];
        this.v0 = -1;
        this.A0 = -1;
        this.R0 = new a();
        this.S0 = new f();
        this.T0 = new g();
        this.h0 = ViewConfiguration.get(context).getScaledTouchSlop();
        this.q0 = getResources().getInteger(17694721);
        setWillNotDraw(false);
        this.y0 = new DecelerateInterpolator(2.0f);
        DisplayMetrics displayMetrics = getResources().getDisplayMetrics();
        this.N0 = (int) (displayMetrics.density * 40.0f);
        d();
        setChildrenDrawingOrderEnabled(true);
        int i2 = (int) (displayMetrics.density * 64.0f);
        this.E0 = i2;
        this.i0 = i2;
        this.k0 = new ue2(this);
        this.l0 = new qe2(this);
        setNestedScrollingEnabled(true);
        int i3 = -this.N0;
        this.r0 = i3;
        this.D0 = i3;
        k(1.0f);
        TypedArray obtainStyledAttributes = context.obtainStyledAttributes(attributeSet, V0);
        setEnabled(obtainStyledAttributes.getBoolean(0, true));
        obtainStyledAttributes.recycle();
    }

    @Override // android.view.ViewGroup, android.view.ViewParent, defpackage.te2
    public void onNestedPreScroll(View view, int i2, int i3, int[] iArr) {
        if (i3 > 0) {
            float f2 = this.j0;
            if (f2 > Utils.FLOAT_EPSILON) {
                float f3 = i3;
                if (f3 > f2) {
                    iArr[1] = (int) f2;
                    this.j0 = Utils.FLOAT_EPSILON;
                } else {
                    this.j0 = f2 - f3;
                    iArr[1] = i3;
                }
                j(this.j0);
            }
        }
        if (this.O0 && i3 > 0 && this.j0 == Utils.FLOAT_EPSILON && Math.abs(i3 - iArr[1]) > 0) {
            this.z0.setVisibility(8);
        }
        int[] iArr2 = this.m0;
        if (dispatchNestedPreScroll(i2 - iArr[0], i3 - iArr[1], iArr2, null)) {
            iArr[0] = iArr[0] + iArr2[0];
            iArr[1] = iArr[1] + iArr2[1];
        }
    }

    @Override // android.view.ViewGroup, android.view.ViewParent, defpackage.te2
    public void onNestedScrollAccepted(View view, View view2, int i2) {
        this.k0.b(view, view2, i2);
        startNestedScroll(i2 & 2);
        this.j0 = Utils.FLOAT_EPSILON;
        this.p0 = true;
    }

    @Override // android.view.ViewGroup, android.view.ViewParent, defpackage.te2
    public boolean onStartNestedScroll(View view, View view2, int i2) {
        return (!isEnabled() || this.x0 || this.g0 || (i2 & 2) == 0) ? false : true;
    }

    @Override // android.view.ViewGroup, android.view.ViewParent, defpackage.te2
    public void onStopNestedScroll(View view) {
        this.k0.d(view);
        this.p0 = false;
        float f2 = this.j0;
        if (f2 > Utils.FLOAT_EPSILON) {
            g(f2);
            this.j0 = Utils.FLOAT_EPSILON;
        }
        stopNestedScroll();
    }

    @Override // defpackage.re2
    public void onNestedScroll(View view, int i2, int i3, int i4, int i5, int i6) {
        onNestedScroll(view, i2, i3, i4, i5, i6, this.o0);
    }

    @Override // android.view.ViewGroup, android.view.ViewParent, defpackage.te2
    public void onNestedScroll(View view, int i2, int i3, int i4, int i5) {
        onNestedScroll(view, i2, i3, i4, i5, 0, this.o0);
    }
}
