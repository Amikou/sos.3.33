package androidx.recyclerview.widget;

import android.annotation.SuppressLint;
import androidx.recyclerview.widget.RecyclerView;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.concurrent.TimeUnit;

/* compiled from: GapWorker.java */
/* loaded from: classes.dex */
public final class j implements Runnable {
    public static final ThreadLocal<j> i0 = new ThreadLocal<>();
    public static Comparator<c> j0 = new a();
    public long f0;
    public long g0;
    public ArrayList<RecyclerView> a = new ArrayList<>();
    public ArrayList<c> h0 = new ArrayList<>();

    /* compiled from: GapWorker.java */
    /* loaded from: classes.dex */
    public class a implements Comparator<c> {
        @Override // java.util.Comparator
        /* renamed from: a */
        public int compare(c cVar, c cVar2) {
            RecyclerView recyclerView = cVar.d;
            if ((recyclerView == null) != (cVar2.d == null)) {
                return recyclerView == null ? 1 : -1;
            }
            boolean z = cVar.a;
            if (z != cVar2.a) {
                return z ? -1 : 1;
            }
            int i = cVar2.b - cVar.b;
            if (i != 0) {
                return i;
            }
            int i2 = cVar.c - cVar2.c;
            if (i2 != 0) {
                return i2;
            }
            return 0;
        }
    }

    /* compiled from: GapWorker.java */
    @SuppressLint({"VisibleForTests"})
    /* loaded from: classes.dex */
    public static class b implements RecyclerView.LayoutManager.c {
        public int a;
        public int b;
        public int[] c;
        public int d;

        @Override // androidx.recyclerview.widget.RecyclerView.LayoutManager.c
        public void a(int i, int i2) {
            if (i < 0) {
                throw new IllegalArgumentException("Layout positions must be non-negative");
            }
            if (i2 >= 0) {
                int i3 = this.d * 2;
                int[] iArr = this.c;
                if (iArr == null) {
                    int[] iArr2 = new int[4];
                    this.c = iArr2;
                    Arrays.fill(iArr2, -1);
                } else if (i3 >= iArr.length) {
                    int[] iArr3 = new int[i3 * 2];
                    this.c = iArr3;
                    System.arraycopy(iArr, 0, iArr3, 0, iArr.length);
                }
                int[] iArr4 = this.c;
                iArr4[i3] = i;
                iArr4[i3 + 1] = i2;
                this.d++;
                return;
            }
            throw new IllegalArgumentException("Pixel distance must be non-negative");
        }

        public void b() {
            int[] iArr = this.c;
            if (iArr != null) {
                Arrays.fill(iArr, -1);
            }
            this.d = 0;
        }

        public void c(RecyclerView recyclerView, boolean z) {
            this.d = 0;
            int[] iArr = this.c;
            if (iArr != null) {
                Arrays.fill(iArr, -1);
            }
            RecyclerView.LayoutManager layoutManager = recyclerView.q0;
            if (recyclerView.p0 == null || layoutManager == null || !layoutManager.B0()) {
                return;
            }
            if (z) {
                if (!recyclerView.h0.p()) {
                    layoutManager.A(recyclerView.p0.getItemCount(), this);
                }
            } else if (!recyclerView.q0()) {
                layoutManager.z(this.a, this.b, recyclerView.j1, this);
            }
            int i = this.d;
            if (i > layoutManager.q0) {
                layoutManager.q0 = i;
                layoutManager.r0 = z;
                recyclerView.f0.K();
            }
        }

        public boolean d(int i) {
            if (this.c != null) {
                int i2 = this.d * 2;
                for (int i3 = 0; i3 < i2; i3 += 2) {
                    if (this.c[i3] == i) {
                        return true;
                    }
                }
            }
            return false;
        }

        public void e(int i, int i2) {
            this.a = i;
            this.b = i2;
        }
    }

    /* compiled from: GapWorker.java */
    /* loaded from: classes.dex */
    public static class c {
        public boolean a;
        public int b;
        public int c;
        public RecyclerView d;
        public int e;

        public void a() {
            this.a = false;
            this.b = 0;
            this.c = 0;
            this.d = null;
            this.e = 0;
        }
    }

    public static boolean e(RecyclerView recyclerView, int i) {
        int j = recyclerView.i0.j();
        for (int i2 = 0; i2 < j; i2++) {
            RecyclerView.a0 i02 = RecyclerView.i0(recyclerView.i0.i(i2));
            if (i02.mPosition == i && !i02.isInvalid()) {
                return true;
            }
        }
        return false;
    }

    public void a(RecyclerView recyclerView) {
        this.a.add(recyclerView);
    }

    public final void b() {
        c cVar;
        int size = this.a.size();
        int i = 0;
        for (int i2 = 0; i2 < size; i2++) {
            RecyclerView recyclerView = this.a.get(i2);
            if (recyclerView.getWindowVisibility() == 0) {
                recyclerView.i1.c(recyclerView, false);
                i += recyclerView.i1.d;
            }
        }
        this.h0.ensureCapacity(i);
        int i3 = 0;
        for (int i4 = 0; i4 < size; i4++) {
            RecyclerView recyclerView2 = this.a.get(i4);
            if (recyclerView2.getWindowVisibility() == 0) {
                b bVar = recyclerView2.i1;
                int abs = Math.abs(bVar.a) + Math.abs(bVar.b);
                for (int i5 = 0; i5 < bVar.d * 2; i5 += 2) {
                    if (i3 >= this.h0.size()) {
                        cVar = new c();
                        this.h0.add(cVar);
                    } else {
                        cVar = this.h0.get(i3);
                    }
                    int[] iArr = bVar.c;
                    int i6 = iArr[i5 + 1];
                    cVar.a = i6 <= abs;
                    cVar.b = abs;
                    cVar.c = i6;
                    cVar.d = recyclerView2;
                    cVar.e = iArr[i5];
                    i3++;
                }
            }
        }
        Collections.sort(this.h0, j0);
    }

    public final void c(c cVar, long j) {
        RecyclerView.a0 i = i(cVar.d, cVar.e, cVar.a ? Long.MAX_VALUE : j);
        if (i == null || i.mNestedRecyclerView == null || !i.isBound() || i.isInvalid()) {
            return;
        }
        h(i.mNestedRecyclerView.get(), j);
    }

    public final void d(long j) {
        for (int i = 0; i < this.h0.size(); i++) {
            c cVar = this.h0.get(i);
            if (cVar.d == null) {
                return;
            }
            c(cVar, j);
            cVar.a();
        }
    }

    public void f(RecyclerView recyclerView, int i, int i2) {
        if (recyclerView.isAttachedToWindow() && this.f0 == 0) {
            this.f0 = recyclerView.getNanoTime();
            recyclerView.post(this);
        }
        recyclerView.i1.e(i, i2);
    }

    public void g(long j) {
        b();
        d(j);
    }

    public final void h(RecyclerView recyclerView, long j) {
        if (recyclerView == null) {
            return;
        }
        if (recyclerView.I0 && recyclerView.i0.j() != 0) {
            recyclerView.Z0();
        }
        b bVar = recyclerView.i1;
        bVar.c(recyclerView, true);
        if (bVar.d != 0) {
            try {
                u74.a("RV Nested Prefetch");
                recyclerView.j1.f(recyclerView.p0);
                for (int i = 0; i < bVar.d * 2; i += 2) {
                    i(recyclerView, bVar.c[i], j);
                }
            } finally {
                u74.b();
            }
        }
    }

    public final RecyclerView.a0 i(RecyclerView recyclerView, int i, long j) {
        if (e(recyclerView, i)) {
            return null;
        }
        RecyclerView.t tVar = recyclerView.f0;
        try {
            recyclerView.L0();
            RecyclerView.a0 I = tVar.I(i, false, j);
            if (I != null) {
                if (I.isBound() && !I.isInvalid()) {
                    tVar.B(I.itemView);
                } else {
                    tVar.a(I, false);
                }
            }
            return I;
        } finally {
            recyclerView.N0(false);
        }
    }

    public void j(RecyclerView recyclerView) {
        this.a.remove(recyclerView);
    }

    @Override // java.lang.Runnable
    public void run() {
        try {
            u74.a("RV Prefetch");
            if (!this.a.isEmpty()) {
                int size = this.a.size();
                long j = 0;
                for (int i = 0; i < size; i++) {
                    RecyclerView recyclerView = this.a.get(i);
                    if (recyclerView.getWindowVisibility() == 0) {
                        j = Math.max(recyclerView.getDrawingTime(), j);
                    }
                }
                if (j != 0) {
                    g(TimeUnit.MILLISECONDS.toNanos(j) + this.g0);
                }
            }
        } finally {
            this.f0 = 0L;
            u74.b();
        }
    }
}
