package androidx.recyclerview.widget;

import androidx.recyclerview.widget.RecyclerView;

/* compiled from: ViewInfoStore.java */
/* loaded from: classes.dex */
public class x {
    public final vo3<RecyclerView.a0, a> a = new vo3<>();
    public final i22<RecyclerView.a0> b = new i22<>();

    /* compiled from: ViewInfoStore.java */
    /* loaded from: classes.dex */
    public static class a {
        public static et2<a> d = new gt2(20);
        public int a;
        public RecyclerView.l.c b;
        public RecyclerView.l.c c;

        public static void a() {
            do {
            } while (d.b() != null);
        }

        public static a b() {
            a b = d.b();
            return b == null ? new a() : b;
        }

        public static void c(a aVar) {
            aVar.a = 0;
            aVar.b = null;
            aVar.c = null;
            d.a(aVar);
        }
    }

    /* compiled from: ViewInfoStore.java */
    /* loaded from: classes.dex */
    public interface b {
        void a(RecyclerView.a0 a0Var);

        void b(RecyclerView.a0 a0Var, RecyclerView.l.c cVar, RecyclerView.l.c cVar2);

        void c(RecyclerView.a0 a0Var, RecyclerView.l.c cVar, RecyclerView.l.c cVar2);

        void d(RecyclerView.a0 a0Var, RecyclerView.l.c cVar, RecyclerView.l.c cVar2);
    }

    public void a(RecyclerView.a0 a0Var, RecyclerView.l.c cVar) {
        a aVar = this.a.get(a0Var);
        if (aVar == null) {
            aVar = a.b();
            this.a.put(a0Var, aVar);
        }
        aVar.a |= 2;
        aVar.b = cVar;
    }

    public void b(RecyclerView.a0 a0Var) {
        a aVar = this.a.get(a0Var);
        if (aVar == null) {
            aVar = a.b();
            this.a.put(a0Var, aVar);
        }
        aVar.a |= 1;
    }

    public void c(long j, RecyclerView.a0 a0Var) {
        this.b.o(j, a0Var);
    }

    public void d(RecyclerView.a0 a0Var, RecyclerView.l.c cVar) {
        a aVar = this.a.get(a0Var);
        if (aVar == null) {
            aVar = a.b();
            this.a.put(a0Var, aVar);
        }
        aVar.c = cVar;
        aVar.a |= 8;
    }

    public void e(RecyclerView.a0 a0Var, RecyclerView.l.c cVar) {
        a aVar = this.a.get(a0Var);
        if (aVar == null) {
            aVar = a.b();
            this.a.put(a0Var, aVar);
        }
        aVar.b = cVar;
        aVar.a |= 4;
    }

    public void f() {
        this.a.clear();
        this.b.b();
    }

    public RecyclerView.a0 g(long j) {
        return this.b.g(j);
    }

    public boolean h(RecyclerView.a0 a0Var) {
        a aVar = this.a.get(a0Var);
        return (aVar == null || (aVar.a & 1) == 0) ? false : true;
    }

    public boolean i(RecyclerView.a0 a0Var) {
        a aVar = this.a.get(a0Var);
        return (aVar == null || (aVar.a & 4) == 0) ? false : true;
    }

    public void j() {
        a.a();
    }

    public void k(RecyclerView.a0 a0Var) {
        p(a0Var);
    }

    public final RecyclerView.l.c l(RecyclerView.a0 a0Var, int i) {
        a m;
        RecyclerView.l.c cVar;
        int f = this.a.f(a0Var);
        if (f >= 0 && (m = this.a.m(f)) != null) {
            int i2 = m.a;
            if ((i2 & i) != 0) {
                int i3 = (~i) & i2;
                m.a = i3;
                if (i == 4) {
                    cVar = m.b;
                } else if (i == 8) {
                    cVar = m.c;
                } else {
                    throw new IllegalArgumentException("Must provide flag PRE or POST");
                }
                if ((i3 & 12) == 0) {
                    this.a.k(f);
                    a.c(m);
                }
                return cVar;
            }
        }
        return null;
    }

    public RecyclerView.l.c m(RecyclerView.a0 a0Var) {
        return l(a0Var, 8);
    }

    public RecyclerView.l.c n(RecyclerView.a0 a0Var) {
        return l(a0Var, 4);
    }

    public void o(b bVar) {
        for (int size = this.a.size() - 1; size >= 0; size--) {
            RecyclerView.a0 i = this.a.i(size);
            a k = this.a.k(size);
            int i2 = k.a;
            if ((i2 & 3) == 3) {
                bVar.a(i);
            } else if ((i2 & 1) != 0) {
                RecyclerView.l.c cVar = k.b;
                if (cVar == null) {
                    bVar.a(i);
                } else {
                    bVar.c(i, cVar, k.c);
                }
            } else if ((i2 & 14) == 14) {
                bVar.b(i, k.b, k.c);
            } else if ((i2 & 12) == 12) {
                bVar.d(i, k.b, k.c);
            } else if ((i2 & 4) != 0) {
                bVar.c(i, k.b, null);
            } else if ((i2 & 8) != 0) {
                bVar.b(i, k.b, k.c);
            }
            a.c(k);
        }
    }

    public void p(RecyclerView.a0 a0Var) {
        a aVar = this.a.get(a0Var);
        if (aVar == null) {
            return;
        }
        aVar.a &= -2;
    }

    public void q(RecyclerView.a0 a0Var) {
        int t = this.b.t() - 1;
        while (true) {
            if (t < 0) {
                break;
            } else if (a0Var == this.b.u(t)) {
                this.b.s(t);
                break;
            } else {
                t--;
            }
        }
        a remove = this.a.remove(a0Var);
        if (remove != null) {
            a.c(remove);
        }
    }
}
