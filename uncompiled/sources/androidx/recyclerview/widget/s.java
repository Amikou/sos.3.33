package androidx.recyclerview.widget;

import android.os.Bundle;
import android.view.View;
import android.view.ViewGroup;
import android.view.accessibility.AccessibilityEvent;
import java.util.Map;
import java.util.WeakHashMap;

/* compiled from: RecyclerViewAccessibilityDelegate.java */
/* loaded from: classes.dex */
public class s extends z5 {
    public final RecyclerView d;
    public final a e;

    /* compiled from: RecyclerViewAccessibilityDelegate.java */
    /* loaded from: classes.dex */
    public static class a extends z5 {
        public final s d;
        public Map<View, z5> e = new WeakHashMap();

        public a(s sVar) {
            this.d = sVar;
        }

        @Override // defpackage.z5
        public boolean a(View view, AccessibilityEvent accessibilityEvent) {
            z5 z5Var = this.e.get(view);
            if (z5Var != null) {
                return z5Var.a(view, accessibilityEvent);
            }
            return super.a(view, accessibilityEvent);
        }

        @Override // defpackage.z5
        public c6 b(View view) {
            z5 z5Var = this.e.get(view);
            if (z5Var != null) {
                return z5Var.b(view);
            }
            return super.b(view);
        }

        @Override // defpackage.z5
        public void f(View view, AccessibilityEvent accessibilityEvent) {
            z5 z5Var = this.e.get(view);
            if (z5Var != null) {
                z5Var.f(view, accessibilityEvent);
            } else {
                super.f(view, accessibilityEvent);
            }
        }

        @Override // defpackage.z5
        public void g(View view, b6 b6Var) {
            if (!this.d.o() && this.d.d.getLayoutManager() != null) {
                this.d.d.getLayoutManager().W0(view, b6Var);
                z5 z5Var = this.e.get(view);
                if (z5Var != null) {
                    z5Var.g(view, b6Var);
                    return;
                } else {
                    super.g(view, b6Var);
                    return;
                }
            }
            super.g(view, b6Var);
        }

        @Override // defpackage.z5
        public void h(View view, AccessibilityEvent accessibilityEvent) {
            z5 z5Var = this.e.get(view);
            if (z5Var != null) {
                z5Var.h(view, accessibilityEvent);
            } else {
                super.h(view, accessibilityEvent);
            }
        }

        @Override // defpackage.z5
        public boolean i(ViewGroup viewGroup, View view, AccessibilityEvent accessibilityEvent) {
            z5 z5Var = this.e.get(viewGroup);
            if (z5Var != null) {
                return z5Var.i(viewGroup, view, accessibilityEvent);
            }
            return super.i(viewGroup, view, accessibilityEvent);
        }

        @Override // defpackage.z5
        public boolean j(View view, int i, Bundle bundle) {
            if (!this.d.o() && this.d.d.getLayoutManager() != null) {
                z5 z5Var = this.e.get(view);
                if (z5Var != null) {
                    if (z5Var.j(view, i, bundle)) {
                        return true;
                    }
                } else if (super.j(view, i, bundle)) {
                    return true;
                }
                return this.d.d.getLayoutManager().q1(view, i, bundle);
            }
            return super.j(view, i, bundle);
        }

        @Override // defpackage.z5
        public void l(View view, int i) {
            z5 z5Var = this.e.get(view);
            if (z5Var != null) {
                z5Var.l(view, i);
            } else {
                super.l(view, i);
            }
        }

        @Override // defpackage.z5
        public void m(View view, AccessibilityEvent accessibilityEvent) {
            z5 z5Var = this.e.get(view);
            if (z5Var != null) {
                z5Var.m(view, accessibilityEvent);
            } else {
                super.m(view, accessibilityEvent);
            }
        }

        public z5 n(View view) {
            return this.e.remove(view);
        }

        public void o(View view) {
            z5 n = ei4.n(view);
            if (n == null || n == this) {
                return;
            }
            this.e.put(view, n);
        }
    }

    public s(RecyclerView recyclerView) {
        this.d = recyclerView;
        z5 n = n();
        if (n != null && (n instanceof a)) {
            this.e = (a) n;
        } else {
            this.e = new a(this);
        }
    }

    @Override // defpackage.z5
    public void f(View view, AccessibilityEvent accessibilityEvent) {
        super.f(view, accessibilityEvent);
        if (!(view instanceof RecyclerView) || o()) {
            return;
        }
        RecyclerView recyclerView = (RecyclerView) view;
        if (recyclerView.getLayoutManager() != null) {
            recyclerView.getLayoutManager().S0(accessibilityEvent);
        }
    }

    @Override // defpackage.z5
    public void g(View view, b6 b6Var) {
        super.g(view, b6Var);
        if (o() || this.d.getLayoutManager() == null) {
            return;
        }
        this.d.getLayoutManager().U0(b6Var);
    }

    @Override // defpackage.z5
    public boolean j(View view, int i, Bundle bundle) {
        if (super.j(view, i, bundle)) {
            return true;
        }
        if (o() || this.d.getLayoutManager() == null) {
            return false;
        }
        return this.d.getLayoutManager().o1(i, bundle);
    }

    public z5 n() {
        return this.e;
    }

    public boolean o() {
        return this.d.q0();
    }
}
