package androidx.recyclerview.widget;

import android.graphics.PointF;
import android.view.View;
import androidx.recyclerview.widget.RecyclerView;
import com.github.mikephil.charting.utils.Utils;

/* compiled from: LinearSnapHelper.java */
/* loaded from: classes.dex */
public class n extends v {
    public q d;
    public q e;

    @Override // androidx.recyclerview.widget.v
    public int[] c(RecyclerView.LayoutManager layoutManager, View view) {
        int[] iArr = new int[2];
        if (layoutManager.v()) {
            iArr[0] = n(view, q(layoutManager));
        } else {
            iArr[0] = 0;
        }
        if (layoutManager.w()) {
            iArr[1] = n(view, r(layoutManager));
        } else {
            iArr[1] = 0;
        }
        return iArr;
    }

    @Override // androidx.recyclerview.widget.v
    public View h(RecyclerView.LayoutManager layoutManager) {
        if (layoutManager.w()) {
            return p(layoutManager, r(layoutManager));
        }
        if (layoutManager.v()) {
            return p(layoutManager, q(layoutManager));
        }
        return null;
    }

    @Override // androidx.recyclerview.widget.v
    public int i(RecyclerView.LayoutManager layoutManager, int i, int i2) {
        int j0;
        View h;
        int o0;
        int i3;
        PointF a;
        int i4;
        int i5;
        if (!(layoutManager instanceof RecyclerView.w.b) || (j0 = layoutManager.j0()) == 0 || (h = h(layoutManager)) == null || (o0 = layoutManager.o0(h)) == -1 || (a = ((RecyclerView.w.b) layoutManager).a(j0 - 1)) == null) {
            return -1;
        }
        if (layoutManager.v()) {
            i4 = o(layoutManager, q(layoutManager), i, 0);
            if (a.x < Utils.FLOAT_EPSILON) {
                i4 = -i4;
            }
        } else {
            i4 = 0;
        }
        if (layoutManager.w()) {
            i5 = o(layoutManager, r(layoutManager), 0, i2);
            if (a.y < Utils.FLOAT_EPSILON) {
                i5 = -i5;
            }
        } else {
            i5 = 0;
        }
        if (layoutManager.w()) {
            i4 = i5;
        }
        if (i4 == 0) {
            return -1;
        }
        int i6 = o0 + i4;
        int i7 = i6 >= 0 ? i6 : 0;
        return i7 >= j0 ? i3 : i7;
    }

    public final float m(RecyclerView.LayoutManager layoutManager, q qVar) {
        int U = layoutManager.U();
        if (U == 0) {
            return 1.0f;
        }
        View view = null;
        int i = Integer.MIN_VALUE;
        int i2 = Integer.MAX_VALUE;
        View view2 = null;
        for (int i3 = 0; i3 < U; i3++) {
            View T = layoutManager.T(i3);
            int o0 = layoutManager.o0(T);
            if (o0 != -1) {
                if (o0 < i2) {
                    view = T;
                    i2 = o0;
                }
                if (o0 > i) {
                    view2 = T;
                    i = o0;
                }
            }
        }
        if (view == null || view2 == null) {
            return 1.0f;
        }
        int max = Math.max(qVar.d(view), qVar.d(view2)) - Math.min(qVar.g(view), qVar.g(view2));
        if (max == 0) {
            return 1.0f;
        }
        return (max * 1.0f) / ((i - i2) + 1);
    }

    public final int n(View view, q qVar) {
        return (qVar.g(view) + (qVar.e(view) / 2)) - (qVar.m() + (qVar.n() / 2));
    }

    public final int o(RecyclerView.LayoutManager layoutManager, q qVar, int i, int i2) {
        int[] d = d(i, i2);
        float m = m(layoutManager, qVar);
        if (m <= Utils.FLOAT_EPSILON) {
            return 0;
        }
        return Math.round((Math.abs(d[0]) > Math.abs(d[1]) ? d[0] : d[1]) / m);
    }

    public final View p(RecyclerView.LayoutManager layoutManager, q qVar) {
        int U = layoutManager.U();
        View view = null;
        if (U == 0) {
            return null;
        }
        int m = qVar.m() + (qVar.n() / 2);
        int i = Integer.MAX_VALUE;
        for (int i2 = 0; i2 < U; i2++) {
            View T = layoutManager.T(i2);
            int abs = Math.abs((qVar.g(T) + (qVar.e(T) / 2)) - m);
            if (abs < i) {
                view = T;
                i = abs;
            }
        }
        return view;
    }

    public final q q(RecyclerView.LayoutManager layoutManager) {
        q qVar = this.e;
        if (qVar == null || qVar.a != layoutManager) {
            this.e = q.a(layoutManager);
        }
        return this.e;
    }

    public final q r(RecyclerView.LayoutManager layoutManager) {
        q qVar = this.d;
        if (qVar == null || qVar.a != layoutManager) {
            this.d = q.c(layoutManager);
        }
        return this.d;
    }
}
