package androidx.recyclerview.widget;

import android.animation.Animator;
import android.animation.ValueAnimator;
import android.content.res.Resources;
import android.graphics.Canvas;
import android.graphics.Rect;
import android.os.Build;
import android.view.GestureDetector;
import android.view.MotionEvent;
import android.view.VelocityTracker;
import android.view.View;
import android.view.ViewConfiguration;
import android.view.animation.Interpolator;
import androidx.recyclerview.widget.RecyclerView;
import com.github.mikephil.charting.utils.Utils;
import java.util.ArrayList;
import java.util.List;

/* compiled from: ItemTouchHelper.java */
/* loaded from: classes.dex */
public class k extends RecyclerView.n implements RecyclerView.o {
    public g A;
    public Rect C;
    public long D;
    public float d;
    public float e;
    public float f;
    public float g;
    public float h;
    public float i;
    public float j;
    public float k;
    public f m;
    public int o;
    public int q;
    public RecyclerView r;
    public VelocityTracker t;
    public List<RecyclerView.a0> u;
    public List<Integer> v;
    public gf1 z;
    public final List<View> a = new ArrayList();
    public final float[] b = new float[2];
    public RecyclerView.a0 c = null;
    public int l = -1;
    public int n = 0;
    public List<h> p = new ArrayList();
    public final Runnable s = new a();
    public RecyclerView.j w = null;
    public View x = null;
    public int y = -1;
    public final RecyclerView.q B = new b();

    /* compiled from: ItemTouchHelper.java */
    /* loaded from: classes.dex */
    public class a implements Runnable {
        public a() {
        }

        @Override // java.lang.Runnable
        public void run() {
            k kVar = k.this;
            if (kVar.c == null || !kVar.y()) {
                return;
            }
            k kVar2 = k.this;
            RecyclerView.a0 a0Var = kVar2.c;
            if (a0Var != null) {
                kVar2.t(a0Var);
            }
            k kVar3 = k.this;
            kVar3.r.removeCallbacks(kVar3.s);
            ei4.l0(k.this.r, this);
        }
    }

    /* compiled from: ItemTouchHelper.java */
    /* loaded from: classes.dex */
    public class b implements RecyclerView.q {
        public b() {
        }

        @Override // androidx.recyclerview.widget.RecyclerView.q
        public void a(RecyclerView recyclerView, MotionEvent motionEvent) {
            k.this.z.a(motionEvent);
            VelocityTracker velocityTracker = k.this.t;
            if (velocityTracker != null) {
                velocityTracker.addMovement(motionEvent);
            }
            if (k.this.l == -1) {
                return;
            }
            int actionMasked = motionEvent.getActionMasked();
            int findPointerIndex = motionEvent.findPointerIndex(k.this.l);
            if (findPointerIndex >= 0) {
                k.this.i(actionMasked, motionEvent, findPointerIndex);
            }
            k kVar = k.this;
            RecyclerView.a0 a0Var = kVar.c;
            if (a0Var == null) {
                return;
            }
            if (actionMasked != 1) {
                if (actionMasked == 2) {
                    if (findPointerIndex >= 0) {
                        kVar.E(motionEvent, kVar.o, findPointerIndex);
                        k.this.t(a0Var);
                        k kVar2 = k.this;
                        kVar2.r.removeCallbacks(kVar2.s);
                        k.this.s.run();
                        k.this.r.invalidate();
                        return;
                    }
                    return;
                } else if (actionMasked != 3) {
                    if (actionMasked != 6) {
                        return;
                    }
                    int actionIndex = motionEvent.getActionIndex();
                    int pointerId = motionEvent.getPointerId(actionIndex);
                    k kVar3 = k.this;
                    if (pointerId == kVar3.l) {
                        kVar3.l = motionEvent.getPointerId(actionIndex == 0 ? 1 : 0);
                        k kVar4 = k.this;
                        kVar4.E(motionEvent, kVar4.o, actionIndex);
                        return;
                    }
                    return;
                } else {
                    VelocityTracker velocityTracker2 = kVar.t;
                    if (velocityTracker2 != null) {
                        velocityTracker2.clear();
                    }
                }
            }
            k.this.z(null, 0);
            k.this.l = -1;
        }

        @Override // androidx.recyclerview.widget.RecyclerView.q
        public boolean c(RecyclerView recyclerView, MotionEvent motionEvent) {
            int findPointerIndex;
            h m;
            k.this.z.a(motionEvent);
            int actionMasked = motionEvent.getActionMasked();
            if (actionMasked == 0) {
                k.this.l = motionEvent.getPointerId(0);
                k.this.d = motionEvent.getX();
                k.this.e = motionEvent.getY();
                k.this.u();
                k kVar = k.this;
                if (kVar.c == null && (m = kVar.m(motionEvent)) != null) {
                    k kVar2 = k.this;
                    kVar2.d -= m.m0;
                    kVar2.e -= m.n0;
                    kVar2.l(m.i0, true);
                    if (k.this.a.remove(m.i0.itemView)) {
                        k kVar3 = k.this;
                        kVar3.m.c(kVar3.r, m.i0);
                    }
                    k.this.z(m.i0, m.j0);
                    k kVar4 = k.this;
                    kVar4.E(motionEvent, kVar4.o, 0);
                }
            } else if (actionMasked != 3 && actionMasked != 1) {
                int i = k.this.l;
                if (i != -1 && (findPointerIndex = motionEvent.findPointerIndex(i)) >= 0) {
                    k.this.i(actionMasked, motionEvent, findPointerIndex);
                }
            } else {
                k kVar5 = k.this;
                kVar5.l = -1;
                kVar5.z(null, 0);
            }
            VelocityTracker velocityTracker = k.this.t;
            if (velocityTracker != null) {
                velocityTracker.addMovement(motionEvent);
            }
            return k.this.c != null;
        }

        @Override // androidx.recyclerview.widget.RecyclerView.q
        public void e(boolean z) {
            if (z) {
                k.this.z(null, 0);
            }
        }
    }

    /* compiled from: ItemTouchHelper.java */
    /* loaded from: classes.dex */
    public class c extends h {
        public final /* synthetic */ int r0;
        public final /* synthetic */ RecyclerView.a0 s0;

        /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
        public c(RecyclerView.a0 a0Var, int i, int i2, float f, float f2, float f3, float f4, int i3, RecyclerView.a0 a0Var2) {
            super(a0Var, i, i2, f, f2, f3, f4);
            this.r0 = i3;
            this.s0 = a0Var2;
        }

        @Override // androidx.recyclerview.widget.k.h, android.animation.Animator.AnimatorListener
        public void onAnimationEnd(Animator animator) {
            super.onAnimationEnd(animator);
            if (this.o0) {
                return;
            }
            if (this.r0 <= 0) {
                k kVar = k.this;
                kVar.m.c(kVar.r, this.s0);
            } else {
                k.this.a.add(this.s0.itemView);
                this.l0 = true;
                int i = this.r0;
                if (i > 0) {
                    k.this.v(this, i);
                }
            }
            k kVar2 = k.this;
            View view = kVar2.x;
            View view2 = this.s0.itemView;
            if (view == view2) {
                kVar2.x(view2);
            }
        }
    }

    /* compiled from: ItemTouchHelper.java */
    /* loaded from: classes.dex */
    public class d implements Runnable {
        public final /* synthetic */ h a;
        public final /* synthetic */ int f0;

        public d(h hVar, int i) {
            this.a = hVar;
            this.f0 = i;
        }

        @Override // java.lang.Runnable
        public void run() {
            RecyclerView recyclerView = k.this.r;
            if (recyclerView == null || !recyclerView.isAttachedToWindow()) {
                return;
            }
            h hVar = this.a;
            if (hVar.o0 || hVar.i0.getAbsoluteAdapterPosition() == -1) {
                return;
            }
            RecyclerView.l itemAnimator = k.this.r.getItemAnimator();
            if ((itemAnimator == null || !itemAnimator.isRunning(null)) && !k.this.r()) {
                k.this.m.B(this.a.i0, this.f0);
            } else {
                k.this.r.post(this);
            }
        }
    }

    /* compiled from: ItemTouchHelper.java */
    /* loaded from: classes.dex */
    public class e implements RecyclerView.j {
        public e() {
        }

        @Override // androidx.recyclerview.widget.RecyclerView.j
        public int a(int i, int i2) {
            k kVar = k.this;
            View view = kVar.x;
            if (view == null) {
                return i2;
            }
            int i3 = kVar.y;
            if (i3 == -1) {
                i3 = kVar.r.indexOfChild(view);
                k.this.y = i3;
            }
            return i2 == i + (-1) ? i3 : i2 < i3 ? i2 : i2 + 1;
        }
    }

    /* compiled from: ItemTouchHelper.java */
    /* loaded from: classes.dex */
    public static abstract class f {
        public static final Interpolator b = new a();
        public static final Interpolator c = new b();
        public int a = -1;

        /* compiled from: ItemTouchHelper.java */
        /* loaded from: classes.dex */
        public class a implements Interpolator {
            @Override // android.animation.TimeInterpolator
            public float getInterpolation(float f) {
                return f * f * f * f * f;
            }
        }

        /* compiled from: ItemTouchHelper.java */
        /* loaded from: classes.dex */
        public class b implements Interpolator {
            @Override // android.animation.TimeInterpolator
            public float getInterpolation(float f) {
                float f2 = f - 1.0f;
                return (f2 * f2 * f2 * f2 * f2) + 1.0f;
            }
        }

        public static int e(int i, int i2) {
            int i3;
            int i4 = i & 789516;
            if (i4 == 0) {
                return i;
            }
            int i5 = i & (~i4);
            if (i2 == 0) {
                i3 = i4 << 2;
            } else {
                int i6 = i4 << 1;
                i5 |= (-789517) & i6;
                i3 = (i6 & 789516) << 2;
            }
            return i5 | i3;
        }

        public static int s(int i, int i2) {
            return i2 << (i * 8);
        }

        public static int t(int i, int i2) {
            int s = s(0, i2 | i);
            return s(2, i) | s(1, i2) | s;
        }

        public void A(RecyclerView.a0 a0Var, int i) {
            if (a0Var != null) {
                jt1.a.b(a0Var.itemView);
            }
        }

        public abstract void B(RecyclerView.a0 a0Var, int i);

        public boolean a(RecyclerView recyclerView, RecyclerView.a0 a0Var, RecyclerView.a0 a0Var2) {
            return true;
        }

        public RecyclerView.a0 b(RecyclerView.a0 a0Var, List<RecyclerView.a0> list, int i, int i2) {
            int bottom;
            int abs;
            int top;
            int abs2;
            int left;
            int abs3;
            int right;
            int abs4;
            int width = i + a0Var.itemView.getWidth();
            int height = i2 + a0Var.itemView.getHeight();
            int left2 = i - a0Var.itemView.getLeft();
            int top2 = i2 - a0Var.itemView.getTop();
            int size = list.size();
            RecyclerView.a0 a0Var2 = null;
            int i3 = -1;
            for (int i4 = 0; i4 < size; i4++) {
                RecyclerView.a0 a0Var3 = list.get(i4);
                if (left2 > 0 && (right = a0Var3.itemView.getRight() - width) < 0 && a0Var3.itemView.getRight() > a0Var.itemView.getRight() && (abs4 = Math.abs(right)) > i3) {
                    a0Var2 = a0Var3;
                    i3 = abs4;
                }
                if (left2 < 0 && (left = a0Var3.itemView.getLeft() - i) > 0 && a0Var3.itemView.getLeft() < a0Var.itemView.getLeft() && (abs3 = Math.abs(left)) > i3) {
                    a0Var2 = a0Var3;
                    i3 = abs3;
                }
                if (top2 < 0 && (top = a0Var3.itemView.getTop() - i2) > 0 && a0Var3.itemView.getTop() < a0Var.itemView.getTop() && (abs2 = Math.abs(top)) > i3) {
                    a0Var2 = a0Var3;
                    i3 = abs2;
                }
                if (top2 > 0 && (bottom = a0Var3.itemView.getBottom() - height) < 0 && a0Var3.itemView.getBottom() > a0Var.itemView.getBottom() && (abs = Math.abs(bottom)) > i3) {
                    a0Var2 = a0Var3;
                    i3 = abs;
                }
            }
            return a0Var2;
        }

        public void c(RecyclerView recyclerView, RecyclerView.a0 a0Var) {
            jt1.a.a(a0Var.itemView);
        }

        public int d(int i, int i2) {
            int i3;
            int i4 = i & 3158064;
            if (i4 == 0) {
                return i;
            }
            int i5 = i & (~i4);
            if (i2 == 0) {
                i3 = i4 >> 2;
            } else {
                int i6 = i4 >> 1;
                i5 |= (-3158065) & i6;
                i3 = (i6 & 3158064) >> 2;
            }
            return i5 | i3;
        }

        public final int f(RecyclerView recyclerView, RecyclerView.a0 a0Var) {
            return d(k(recyclerView, a0Var), ei4.E(recyclerView));
        }

        public long g(RecyclerView recyclerView, int i, float f, float f2) {
            RecyclerView.l itemAnimator = recyclerView.getItemAnimator();
            if (itemAnimator == null) {
                return i == 8 ? 200L : 250L;
            } else if (i == 8) {
                return itemAnimator.getMoveDuration();
            } else {
                return itemAnimator.getRemoveDuration();
            }
        }

        public int h() {
            return 0;
        }

        public final int i(RecyclerView recyclerView) {
            if (this.a == -1) {
                this.a = recyclerView.getResources().getDimensionPixelSize(hz2.item_touch_helper_max_drag_scroll_per_frame);
            }
            return this.a;
        }

        public float j(RecyclerView.a0 a0Var) {
            return 0.5f;
        }

        public abstract int k(RecyclerView recyclerView, RecyclerView.a0 a0Var);

        public float l(float f) {
            return f;
        }

        public float m(RecyclerView.a0 a0Var) {
            return 0.5f;
        }

        public float n(float f) {
            return f;
        }

        public boolean o(RecyclerView recyclerView, RecyclerView.a0 a0Var) {
            return (f(recyclerView, a0Var) & 16711680) != 0;
        }

        public int p(RecyclerView recyclerView, int i, int i2, int i3, long j) {
            int signum = (int) (((int) (((int) Math.signum(i2)) * i(recyclerView) * c.getInterpolation(Math.min(1.0f, (Math.abs(i2) * 1.0f) / i)))) * b.getInterpolation(j <= 2000 ? ((float) j) / 2000.0f : 1.0f));
            return signum == 0 ? i2 > 0 ? 1 : -1 : signum;
        }

        public boolean q() {
            return true;
        }

        public boolean r() {
            return true;
        }

        public void u(Canvas canvas, RecyclerView recyclerView, RecyclerView.a0 a0Var, float f, float f2, int i, boolean z) {
            jt1.a.d(canvas, recyclerView, a0Var.itemView, f, f2, i, z);
        }

        public void v(Canvas canvas, RecyclerView recyclerView, RecyclerView.a0 a0Var, float f, float f2, int i, boolean z) {
            jt1.a.c(canvas, recyclerView, a0Var.itemView, f, f2, i, z);
        }

        public void w(Canvas canvas, RecyclerView recyclerView, RecyclerView.a0 a0Var, List<h> list, int i, float f, float f2) {
            int size = list.size();
            for (int i2 = 0; i2 < size; i2++) {
                h hVar = list.get(i2);
                hVar.e();
                int save = canvas.save();
                u(canvas, recyclerView, hVar.i0, hVar.m0, hVar.n0, hVar.j0, false);
                canvas.restoreToCount(save);
            }
            if (a0Var != null) {
                int save2 = canvas.save();
                u(canvas, recyclerView, a0Var, f, f2, i, true);
                canvas.restoreToCount(save2);
            }
        }

        public void x(Canvas canvas, RecyclerView recyclerView, RecyclerView.a0 a0Var, List<h> list, int i, float f, float f2) {
            int size = list.size();
            boolean z = false;
            for (int i2 = 0; i2 < size; i2++) {
                h hVar = list.get(i2);
                int save = canvas.save();
                v(canvas, recyclerView, hVar.i0, hVar.m0, hVar.n0, hVar.j0, false);
                canvas.restoreToCount(save);
            }
            if (a0Var != null) {
                int save2 = canvas.save();
                v(canvas, recyclerView, a0Var, f, f2, i, true);
                canvas.restoreToCount(save2);
            }
            for (int i3 = size - 1; i3 >= 0; i3--) {
                h hVar2 = list.get(i3);
                boolean z2 = hVar2.p0;
                if (z2 && !hVar2.l0) {
                    list.remove(i3);
                } else if (!z2) {
                    z = true;
                }
            }
            if (z) {
                recyclerView.invalidate();
            }
        }

        public abstract boolean y(RecyclerView recyclerView, RecyclerView.a0 a0Var, RecyclerView.a0 a0Var2);

        public void z(RecyclerView recyclerView, RecyclerView.a0 a0Var, int i, RecyclerView.a0 a0Var2, int i2, int i3, int i4) {
            RecyclerView.LayoutManager layoutManager = recyclerView.getLayoutManager();
            if (layoutManager instanceof j) {
                ((j) layoutManager).g(a0Var.itemView, a0Var2.itemView, i3, i4);
                return;
            }
            if (layoutManager.v()) {
                if (layoutManager.b0(a0Var2.itemView) <= recyclerView.getPaddingLeft()) {
                    recyclerView.o1(i2);
                }
                if (layoutManager.e0(a0Var2.itemView) >= recyclerView.getWidth() - recyclerView.getPaddingRight()) {
                    recyclerView.o1(i2);
                }
            }
            if (layoutManager.w()) {
                if (layoutManager.f0(a0Var2.itemView) <= recyclerView.getPaddingTop()) {
                    recyclerView.o1(i2);
                }
                if (layoutManager.Z(a0Var2.itemView) >= recyclerView.getHeight() - recyclerView.getPaddingBottom()) {
                    recyclerView.o1(i2);
                }
            }
        }
    }

    /* compiled from: ItemTouchHelper.java */
    /* loaded from: classes.dex */
    public class g extends GestureDetector.SimpleOnGestureListener {
        public boolean a = true;

        public g() {
        }

        public void a() {
            this.a = false;
        }

        @Override // android.view.GestureDetector.SimpleOnGestureListener, android.view.GestureDetector.OnGestureListener
        public boolean onDown(MotionEvent motionEvent) {
            return true;
        }

        @Override // android.view.GestureDetector.SimpleOnGestureListener, android.view.GestureDetector.OnGestureListener
        public void onLongPress(MotionEvent motionEvent) {
            View n;
            RecyclerView.a0 h0;
            if (!this.a || (n = k.this.n(motionEvent)) == null || (h0 = k.this.r.h0(n)) == null) {
                return;
            }
            k kVar = k.this;
            if (kVar.m.o(kVar.r, h0)) {
                int pointerId = motionEvent.getPointerId(0);
                int i = k.this.l;
                if (pointerId == i) {
                    int findPointerIndex = motionEvent.findPointerIndex(i);
                    float x = motionEvent.getX(findPointerIndex);
                    float y = motionEvent.getY(findPointerIndex);
                    k kVar2 = k.this;
                    kVar2.d = x;
                    kVar2.e = y;
                    kVar2.i = Utils.FLOAT_EPSILON;
                    kVar2.h = Utils.FLOAT_EPSILON;
                    if (kVar2.m.r()) {
                        k.this.z(h0, 2);
                    }
                }
            }
        }
    }

    /* compiled from: ItemTouchHelper.java */
    /* loaded from: classes.dex */
    public static class h implements Animator.AnimatorListener {
        public final float a;
        public final float f0;
        public final float g0;
        public final float h0;
        public final RecyclerView.a0 i0;
        public final int j0;
        public final ValueAnimator k0;
        public boolean l0;
        public float m0;
        public float n0;
        public boolean o0 = false;
        public boolean p0 = false;
        public float q0;

        /* compiled from: ItemTouchHelper.java */
        /* loaded from: classes.dex */
        public class a implements ValueAnimator.AnimatorUpdateListener {
            public a() {
            }

            @Override // android.animation.ValueAnimator.AnimatorUpdateListener
            public void onAnimationUpdate(ValueAnimator valueAnimator) {
                h.this.c(valueAnimator.getAnimatedFraction());
            }
        }

        public h(RecyclerView.a0 a0Var, int i, int i2, float f, float f2, float f3, float f4) {
            this.j0 = i2;
            this.i0 = a0Var;
            this.a = f;
            this.f0 = f2;
            this.g0 = f3;
            this.h0 = f4;
            ValueAnimator ofFloat = ValueAnimator.ofFloat(Utils.FLOAT_EPSILON, 1.0f);
            this.k0 = ofFloat;
            ofFloat.addUpdateListener(new a());
            ofFloat.setTarget(a0Var.itemView);
            ofFloat.addListener(this);
            c(Utils.FLOAT_EPSILON);
        }

        public void a() {
            this.k0.cancel();
        }

        public void b(long j) {
            this.k0.setDuration(j);
        }

        public void c(float f) {
            this.q0 = f;
        }

        public void d() {
            this.i0.setIsRecyclable(false);
            this.k0.start();
        }

        public void e() {
            float f = this.a;
            float f2 = this.g0;
            if (f == f2) {
                this.m0 = this.i0.itemView.getTranslationX();
            } else {
                this.m0 = f + (this.q0 * (f2 - f));
            }
            float f3 = this.f0;
            float f4 = this.h0;
            if (f3 == f4) {
                this.n0 = this.i0.itemView.getTranslationY();
            } else {
                this.n0 = f3 + (this.q0 * (f4 - f3));
            }
        }

        @Override // android.animation.Animator.AnimatorListener
        public void onAnimationCancel(Animator animator) {
            c(1.0f);
        }

        @Override // android.animation.Animator.AnimatorListener
        public void onAnimationEnd(Animator animator) {
            if (!this.p0) {
                this.i0.setIsRecyclable(true);
            }
            this.p0 = true;
        }

        @Override // android.animation.Animator.AnimatorListener
        public void onAnimationRepeat(Animator animator) {
        }

        @Override // android.animation.Animator.AnimatorListener
        public void onAnimationStart(Animator animator) {
        }
    }

    /* compiled from: ItemTouchHelper.java */
    /* loaded from: classes.dex */
    public static abstract class i extends f {
        public int d;
        public int e;

        public i(int i, int i2) {
            this.d = i2;
            this.e = i;
        }

        public int C(RecyclerView recyclerView, RecyclerView.a0 a0Var) {
            return this.e;
        }

        public int D(RecyclerView recyclerView, RecyclerView.a0 a0Var) {
            return this.d;
        }

        @Override // androidx.recyclerview.widget.k.f
        public int k(RecyclerView recyclerView, RecyclerView.a0 a0Var) {
            return f.t(C(recyclerView, a0Var), D(recyclerView, a0Var));
        }
    }

    /* compiled from: ItemTouchHelper.java */
    /* loaded from: classes.dex */
    public interface j {
        void g(View view, View view2, int i, int i2);
    }

    public k(f fVar) {
        this.m = fVar;
    }

    public static boolean s(View view, float f2, float f3, float f4, float f5) {
        return f2 >= f4 && f2 <= f4 + ((float) view.getWidth()) && f3 >= f5 && f3 <= f5 + ((float) view.getHeight());
    }

    public final void A() {
        this.q = ViewConfiguration.get(this.r.getContext()).getScaledTouchSlop();
        this.r.h(this);
        this.r.k(this.B);
        this.r.j(this);
        B();
    }

    public final void B() {
        this.A = new g();
        this.z = new gf1(this.r.getContext(), this.A);
    }

    public final void C() {
        g gVar = this.A;
        if (gVar != null) {
            gVar.a();
            this.A = null;
        }
        if (this.z != null) {
            this.z = null;
        }
    }

    public final int D(RecyclerView.a0 a0Var) {
        if (this.n == 2) {
            return 0;
        }
        int k = this.m.k(this.r, a0Var);
        int d2 = (this.m.d(k, ei4.E(this.r)) & 65280) >> 8;
        if (d2 == 0) {
            return 0;
        }
        int i2 = (k & 65280) >> 8;
        if (Math.abs(this.h) > Math.abs(this.i)) {
            int h2 = h(a0Var, d2);
            if (h2 > 0) {
                return (i2 & h2) == 0 ? f.e(h2, ei4.E(this.r)) : h2;
            }
            int j2 = j(a0Var, d2);
            if (j2 > 0) {
                return j2;
            }
        } else {
            int j3 = j(a0Var, d2);
            if (j3 > 0) {
                return j3;
            }
            int h3 = h(a0Var, d2);
            if (h3 > 0) {
                return (i2 & h3) == 0 ? f.e(h3, ei4.E(this.r)) : h3;
            }
        }
        return 0;
    }

    public void E(MotionEvent motionEvent, int i2, int i3) {
        float x = motionEvent.getX(i3);
        float y = motionEvent.getY(i3);
        float f2 = x - this.d;
        this.h = f2;
        this.i = y - this.e;
        if ((i2 & 4) == 0) {
            this.h = Math.max((float) Utils.FLOAT_EPSILON, f2);
        }
        if ((i2 & 8) == 0) {
            this.h = Math.min((float) Utils.FLOAT_EPSILON, this.h);
        }
        if ((i2 & 1) == 0) {
            this.i = Math.max((float) Utils.FLOAT_EPSILON, this.i);
        }
        if ((i2 & 2) == 0) {
            this.i = Math.min((float) Utils.FLOAT_EPSILON, this.i);
        }
    }

    @Override // androidx.recyclerview.widget.RecyclerView.o
    public void b(View view) {
        x(view);
        RecyclerView.a0 h0 = this.r.h0(view);
        if (h0 == null) {
            return;
        }
        RecyclerView.a0 a0Var = this.c;
        if (a0Var != null && h0 == a0Var) {
            z(null, 0);
            return;
        }
        l(h0, false);
        if (this.a.remove(h0.itemView)) {
            this.m.c(this.r, h0);
        }
    }

    @Override // androidx.recyclerview.widget.RecyclerView.o
    public void d(View view) {
    }

    public final void f() {
        if (Build.VERSION.SDK_INT >= 21) {
            return;
        }
        if (this.w == null) {
            this.w = new e();
        }
        this.r.setChildDrawingOrderCallback(this.w);
    }

    public void g(RecyclerView recyclerView) {
        RecyclerView recyclerView2 = this.r;
        if (recyclerView2 == recyclerView) {
            return;
        }
        if (recyclerView2 != null) {
            k();
        }
        this.r = recyclerView;
        if (recyclerView != null) {
            Resources resources = recyclerView.getResources();
            this.f = resources.getDimension(hz2.item_touch_helper_swipe_escape_velocity);
            this.g = resources.getDimension(hz2.item_touch_helper_swipe_escape_max_velocity);
            A();
        }
    }

    @Override // androidx.recyclerview.widget.RecyclerView.n
    public void getItemOffsets(Rect rect, View view, RecyclerView recyclerView, RecyclerView.x xVar) {
        rect.setEmpty();
    }

    public final int h(RecyclerView.a0 a0Var, int i2) {
        if ((i2 & 12) != 0) {
            int i3 = this.h > Utils.FLOAT_EPSILON ? 8 : 4;
            VelocityTracker velocityTracker = this.t;
            if (velocityTracker != null && this.l > -1) {
                velocityTracker.computeCurrentVelocity(1000, this.m.n(this.g));
                float xVelocity = this.t.getXVelocity(this.l);
                float yVelocity = this.t.getYVelocity(this.l);
                int i4 = xVelocity <= Utils.FLOAT_EPSILON ? 4 : 8;
                float abs = Math.abs(xVelocity);
                if ((i4 & i2) != 0 && i3 == i4 && abs >= this.m.l(this.f) && abs > Math.abs(yVelocity)) {
                    return i4;
                }
            }
            float width = this.r.getWidth() * this.m.m(a0Var);
            if ((i2 & i3) == 0 || Math.abs(this.h) <= width) {
                return 0;
            }
            return i3;
        }
        return 0;
    }

    public void i(int i2, MotionEvent motionEvent, int i3) {
        RecyclerView.a0 p;
        int f2;
        if (this.c != null || i2 != 2 || this.n == 2 || !this.m.q() || this.r.getScrollState() == 1 || (p = p(motionEvent)) == null || (f2 = (this.m.f(this.r, p) & 65280) >> 8) == 0) {
            return;
        }
        float x = motionEvent.getX(i3);
        float y = motionEvent.getY(i3);
        float f3 = x - this.d;
        float f4 = y - this.e;
        float abs = Math.abs(f3);
        float abs2 = Math.abs(f4);
        int i4 = this.q;
        if (abs >= i4 || abs2 >= i4) {
            if (abs > abs2) {
                if (f3 < Utils.FLOAT_EPSILON && (f2 & 4) == 0) {
                    return;
                }
                if (f3 > Utils.FLOAT_EPSILON && (f2 & 8) == 0) {
                    return;
                }
            } else if (f4 < Utils.FLOAT_EPSILON && (f2 & 1) == 0) {
                return;
            } else {
                if (f4 > Utils.FLOAT_EPSILON && (f2 & 2) == 0) {
                    return;
                }
            }
            this.i = Utils.FLOAT_EPSILON;
            this.h = Utils.FLOAT_EPSILON;
            this.l = motionEvent.getPointerId(0);
            z(p, 1);
        }
    }

    public final int j(RecyclerView.a0 a0Var, int i2) {
        if ((i2 & 3) != 0) {
            int i3 = this.i > Utils.FLOAT_EPSILON ? 2 : 1;
            VelocityTracker velocityTracker = this.t;
            if (velocityTracker != null && this.l > -1) {
                velocityTracker.computeCurrentVelocity(1000, this.m.n(this.g));
                float xVelocity = this.t.getXVelocity(this.l);
                float yVelocity = this.t.getYVelocity(this.l);
                int i4 = yVelocity <= Utils.FLOAT_EPSILON ? 1 : 2;
                float abs = Math.abs(yVelocity);
                if ((i4 & i2) != 0 && i4 == i3 && abs >= this.m.l(this.f) && abs > Math.abs(xVelocity)) {
                    return i4;
                }
            }
            float height = this.r.getHeight() * this.m.m(a0Var);
            if ((i2 & i3) == 0 || Math.abs(this.i) <= height) {
                return 0;
            }
            return i3;
        }
        return 0;
    }

    public final void k() {
        this.r.b1(this);
        this.r.e1(this.B);
        this.r.d1(this);
        for (int size = this.p.size() - 1; size >= 0; size--) {
            h hVar = this.p.get(0);
            hVar.a();
            this.m.c(this.r, hVar.i0);
        }
        this.p.clear();
        this.x = null;
        this.y = -1;
        w();
        C();
    }

    public void l(RecyclerView.a0 a0Var, boolean z) {
        for (int size = this.p.size() - 1; size >= 0; size--) {
            h hVar = this.p.get(size);
            if (hVar.i0 == a0Var) {
                hVar.o0 |= z;
                if (!hVar.p0) {
                    hVar.a();
                }
                this.p.remove(size);
                return;
            }
        }
    }

    public h m(MotionEvent motionEvent) {
        if (this.p.isEmpty()) {
            return null;
        }
        View n = n(motionEvent);
        for (int size = this.p.size() - 1; size >= 0; size--) {
            h hVar = this.p.get(size);
            if (hVar.i0.itemView == n) {
                return hVar;
            }
        }
        return null;
    }

    public View n(MotionEvent motionEvent) {
        float x = motionEvent.getX();
        float y = motionEvent.getY();
        RecyclerView.a0 a0Var = this.c;
        if (a0Var != null) {
            View view = a0Var.itemView;
            if (s(view, x, y, this.j + this.h, this.k + this.i)) {
                return view;
            }
        }
        for (int size = this.p.size() - 1; size >= 0; size--) {
            h hVar = this.p.get(size);
            View view2 = hVar.i0.itemView;
            if (s(view2, x, y, hVar.m0, hVar.n0)) {
                return view2;
            }
        }
        return this.r.S(x, y);
    }

    public final List<RecyclerView.a0> o(RecyclerView.a0 a0Var) {
        RecyclerView.a0 a0Var2 = a0Var;
        List<RecyclerView.a0> list = this.u;
        if (list == null) {
            this.u = new ArrayList();
            this.v = new ArrayList();
        } else {
            list.clear();
            this.v.clear();
        }
        int h2 = this.m.h();
        int round = Math.round(this.j + this.h) - h2;
        int round2 = Math.round(this.k + this.i) - h2;
        int i2 = h2 * 2;
        int width = a0Var2.itemView.getWidth() + round + i2;
        int height = a0Var2.itemView.getHeight() + round2 + i2;
        int i3 = (round + width) / 2;
        int i4 = (round2 + height) / 2;
        RecyclerView.LayoutManager layoutManager = this.r.getLayoutManager();
        int U = layoutManager.U();
        int i5 = 0;
        while (i5 < U) {
            View T = layoutManager.T(i5);
            if (T != a0Var2.itemView && T.getBottom() >= round2 && T.getTop() <= height && T.getRight() >= round && T.getLeft() <= width) {
                RecyclerView.a0 h0 = this.r.h0(T);
                if (this.m.a(this.r, this.c, h0)) {
                    int abs = Math.abs(i3 - ((T.getLeft() + T.getRight()) / 2));
                    int abs2 = Math.abs(i4 - ((T.getTop() + T.getBottom()) / 2));
                    int i6 = (abs * abs) + (abs2 * abs2);
                    int size = this.u.size();
                    int i7 = 0;
                    for (int i8 = 0; i8 < size && i6 > this.v.get(i8).intValue(); i8++) {
                        i7++;
                    }
                    this.u.add(i7, h0);
                    this.v.add(i7, Integer.valueOf(i6));
                }
            }
            i5++;
            a0Var2 = a0Var;
        }
        return this.u;
    }

    @Override // androidx.recyclerview.widget.RecyclerView.n
    public void onDraw(Canvas canvas, RecyclerView recyclerView, RecyclerView.x xVar) {
        float f2;
        float f3;
        this.y = -1;
        if (this.c != null) {
            q(this.b);
            float[] fArr = this.b;
            float f4 = fArr[0];
            f3 = fArr[1];
            f2 = f4;
        } else {
            f2 = 0.0f;
            f3 = 0.0f;
        }
        this.m.w(canvas, recyclerView, this.c, this.p, this.n, f2, f3);
    }

    @Override // androidx.recyclerview.widget.RecyclerView.n
    public void onDrawOver(Canvas canvas, RecyclerView recyclerView, RecyclerView.x xVar) {
        float f2;
        float f3;
        if (this.c != null) {
            q(this.b);
            float[] fArr = this.b;
            float f4 = fArr[0];
            f3 = fArr[1];
            f2 = f4;
        } else {
            f2 = 0.0f;
            f3 = 0.0f;
        }
        this.m.x(canvas, recyclerView, this.c, this.p, this.n, f2, f3);
    }

    public final RecyclerView.a0 p(MotionEvent motionEvent) {
        View n;
        RecyclerView.LayoutManager layoutManager = this.r.getLayoutManager();
        int i2 = this.l;
        if (i2 == -1) {
            return null;
        }
        int findPointerIndex = motionEvent.findPointerIndex(i2);
        float abs = Math.abs(motionEvent.getX(findPointerIndex) - this.d);
        float abs2 = Math.abs(motionEvent.getY(findPointerIndex) - this.e);
        int i3 = this.q;
        if (abs >= i3 || abs2 >= i3) {
            if (abs <= abs2 || !layoutManager.v()) {
                if ((abs2 <= abs || !layoutManager.w()) && (n = n(motionEvent)) != null) {
                    return this.r.h0(n);
                }
                return null;
            }
            return null;
        }
        return null;
    }

    public final void q(float[] fArr) {
        if ((this.o & 12) != 0) {
            fArr[0] = (this.j + this.h) - this.c.itemView.getLeft();
        } else {
            fArr[0] = this.c.itemView.getTranslationX();
        }
        if ((this.o & 3) != 0) {
            fArr[1] = (this.k + this.i) - this.c.itemView.getTop();
        } else {
            fArr[1] = this.c.itemView.getTranslationY();
        }
    }

    public boolean r() {
        int size = this.p.size();
        for (int i2 = 0; i2 < size; i2++) {
            if (!this.p.get(i2).p0) {
                return true;
            }
        }
        return false;
    }

    public void t(RecyclerView.a0 a0Var) {
        if (!this.r.isLayoutRequested() && this.n == 2) {
            float j2 = this.m.j(a0Var);
            int i2 = (int) (this.j + this.h);
            int i3 = (int) (this.k + this.i);
            if (Math.abs(i3 - a0Var.itemView.getTop()) >= a0Var.itemView.getHeight() * j2 || Math.abs(i2 - a0Var.itemView.getLeft()) >= a0Var.itemView.getWidth() * j2) {
                List<RecyclerView.a0> o = o(a0Var);
                if (o.size() == 0) {
                    return;
                }
                RecyclerView.a0 b2 = this.m.b(a0Var, o, i2, i3);
                if (b2 == null) {
                    this.u.clear();
                    this.v.clear();
                    return;
                }
                int absoluteAdapterPosition = b2.getAbsoluteAdapterPosition();
                int absoluteAdapterPosition2 = a0Var.getAbsoluteAdapterPosition();
                if (this.m.y(this.r, a0Var, b2)) {
                    this.m.z(this.r, a0Var, absoluteAdapterPosition2, b2, absoluteAdapterPosition, i2, i3);
                }
            }
        }
    }

    public void u() {
        VelocityTracker velocityTracker = this.t;
        if (velocityTracker != null) {
            velocityTracker.recycle();
        }
        this.t = VelocityTracker.obtain();
    }

    public void v(h hVar, int i2) {
        this.r.post(new d(hVar, i2));
    }

    public final void w() {
        VelocityTracker velocityTracker = this.t;
        if (velocityTracker != null) {
            velocityTracker.recycle();
            this.t = null;
        }
    }

    public void x(View view) {
        if (view == this.x) {
            this.x = null;
            if (this.w != null) {
                this.r.setChildDrawingOrderCallback(null);
            }
        }
    }

    /* JADX WARN: Code restructure failed: missing block: B:34:0x00c1, code lost:
        if (r1 > 0) goto L24;
     */
    /* JADX WARN: Removed duplicated region for block: B:27:0x0084  */
    /* JADX WARN: Removed duplicated region for block: B:38:0x00c7  */
    /* JADX WARN: Removed duplicated region for block: B:41:0x00e1  */
    /* JADX WARN: Removed duplicated region for block: B:42:0x00fd  */
    /* JADX WARN: Removed duplicated region for block: B:44:0x0100 A[ADDED_TO_REGION] */
    /* JADX WARN: Removed duplicated region for block: B:50:0x010c  */
    /*
        Code decompiled incorrectly, please refer to instructions dump.
        To view partially-correct code enable 'Show inconsistent code' option in preferences
    */
    public boolean y() {
        /*
            Method dump skipped, instructions count: 277
            To view this dump change 'Code comments level' option to 'DEBUG'
        */
        throw new UnsupportedOperationException("Method not decompiled: androidx.recyclerview.widget.k.y():boolean");
    }

    /* JADX WARN: Removed duplicated region for block: B:48:0x012a  */
    /* JADX WARN: Removed duplicated region for block: B:54:0x0136  */
    /*
        Code decompiled incorrectly, please refer to instructions dump.
        To view partially-correct code enable 'Show inconsistent code' option in preferences
    */
    public void z(androidx.recyclerview.widget.RecyclerView.a0 r24, int r25) {
        /*
            Method dump skipped, instructions count: 334
            To view this dump change 'Code comments level' option to 'DEBUG'
        */
        throw new UnsupportedOperationException("Method not decompiled: androidx.recyclerview.widget.k.z(androidx.recyclerview.widget.RecyclerView$a0, int):void");
    }
}
