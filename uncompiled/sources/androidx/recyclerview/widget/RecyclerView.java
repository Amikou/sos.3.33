package androidx.recyclerview.widget;

import android.animation.LayoutTransition;
import android.annotation.SuppressLint;
import android.content.Context;
import android.content.res.Resources;
import android.content.res.TypedArray;
import android.database.Observable;
import android.graphics.Canvas;
import android.graphics.Matrix;
import android.graphics.PointF;
import android.graphics.Rect;
import android.graphics.RectF;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.StateListDrawable;
import android.os.Build;
import android.os.Bundle;
import android.os.Parcel;
import android.os.Parcelable;
import android.os.SystemClock;
import android.util.AttributeSet;
import android.util.SparseArray;
import android.view.Display;
import android.view.FocusFinder;
import android.view.MotionEvent;
import android.view.VelocityTracker;
import android.view.View;
import android.view.ViewConfiguration;
import android.view.ViewGroup;
import android.view.ViewParent;
import android.view.accessibility.AccessibilityEvent;
import android.view.accessibility.AccessibilityManager;
import android.view.animation.Interpolator;
import android.widget.EdgeEffect;
import android.widget.OverScroller;
import androidx.customview.view.AbsSavedState;
import androidx.media3.common.PlaybackException;
import androidx.recyclerview.widget.a;
import androidx.recyclerview.widget.e;
import androidx.recyclerview.widget.j;
import androidx.recyclerview.widget.s;
import androidx.recyclerview.widget.w;
import androidx.recyclerview.widget.x;
import com.fasterxml.jackson.core.util.MinimalPrettyPrinter;
import com.github.mikephil.charting.utils.Utils;
import defpackage.b6;
import java.lang.ref.WeakReference;
import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/* loaded from: classes.dex */
public class RecyclerView extends ViewGroup implements pe2 {
    public static final int[] D1 = {16843830};
    public static final boolean E1;
    public static final boolean F1;
    public static final boolean G1;
    public static final boolean H1;
    public static final boolean I1;
    public static final boolean J1;
    public static final Class<?>[] K1;
    public static final Interpolator L1;
    public int A0;
    public int A1;
    public boolean B0;
    public int B1;
    public boolean C0;
    public final x.b C1;
    public boolean D0;
    public int E0;
    public boolean F0;
    public final AccessibilityManager G0;
    public List<o> H0;
    public boolean I0;
    public boolean J0;
    public int K0;
    public int L0;
    public k M0;
    public EdgeEffect N0;
    public EdgeEffect O0;
    public EdgeEffect P0;
    public EdgeEffect Q0;
    public l R0;
    public int S0;
    public int T0;
    public VelocityTracker U0;
    public int V0;
    public int W0;
    public int X0;
    public int Y0;
    public int Z0;
    public final v a;
    public p a1;
    public final int b1;
    public final int c1;
    public float d1;
    public float e1;
    public final t f0;
    public boolean f1;
    public SavedState g0;
    public final z g1;
    public androidx.recyclerview.widget.a h0;
    public androidx.recyclerview.widget.j h1;
    public androidx.recyclerview.widget.e i0;
    public j.b i1;
    public final androidx.recyclerview.widget.x j0;
    public final x j1;
    public boolean k0;
    public r k1;
    public final Runnable l0;
    public List<r> l1;
    public final Rect m0;
    public boolean m1;
    public final Rect n0;
    public boolean n1;
    public final RectF o0;
    public l.b o1;
    public Adapter p0;
    public boolean p1;
    public LayoutManager q0;
    public androidx.recyclerview.widget.s q1;
    public u r0;
    public j r1;
    public final List<u> s0;
    public final int[] s1;
    public final ArrayList<n> t0;
    public qe2 t1;
    public final ArrayList<q> u0;
    public final int[] u1;
    public q v0;
    public final int[] v1;
    public boolean w0;
    public final int[] w1;
    public boolean x0;
    public final List<a0> x1;
    public boolean y0;
    public Runnable y1;
    public boolean z0;
    public boolean z1;

    /* loaded from: classes.dex */
    public static abstract class Adapter<VH extends a0> {
        private final h mObservable = new h();
        private boolean mHasStableIds = false;
        private StateRestorationPolicy mStateRestorationPolicy = StateRestorationPolicy.ALLOW;

        /* loaded from: classes.dex */
        public enum StateRestorationPolicy {
            ALLOW,
            PREVENT_WHEN_EMPTY,
            PREVENT
        }

        public final void bindViewHolder(VH vh, int i) {
            boolean z = vh.mBindingAdapter == null;
            if (z) {
                vh.mPosition = i;
                if (hasStableIds()) {
                    vh.mItemId = getItemId(i);
                }
                vh.setFlags(1, 519);
                u74.a("RV OnBindView");
            }
            vh.mBindingAdapter = this;
            onBindViewHolder(vh, i, vh.getUnmodifiedPayloads());
            if (z) {
                vh.clearPayload();
                ViewGroup.LayoutParams layoutParams = vh.itemView.getLayoutParams();
                if (layoutParams instanceof LayoutParams) {
                    ((LayoutParams) layoutParams).g0 = true;
                }
                u74.b();
            }
        }

        public boolean canRestoreState() {
            int i = g.a[this.mStateRestorationPolicy.ordinal()];
            if (i != 1) {
                return i != 2 || getItemCount() > 0;
            }
            return false;
        }

        public final VH createViewHolder(ViewGroup viewGroup, int i) {
            try {
                u74.a("RV CreateView");
                VH onCreateViewHolder = onCreateViewHolder(viewGroup, i);
                if (onCreateViewHolder.itemView.getParent() == null) {
                    onCreateViewHolder.mItemViewType = i;
                    return onCreateViewHolder;
                }
                throw new IllegalStateException("ViewHolder views must not be attached when created. Ensure that you are not passing 'true' to the attachToRoot parameter of LayoutInflater.inflate(..., boolean attachToRoot)");
            } finally {
                u74.b();
            }
        }

        public int findRelativeAdapterPositionIn(Adapter<? extends a0> adapter, a0 a0Var, int i) {
            if (adapter == this) {
                return i;
            }
            return -1;
        }

        public abstract int getItemCount();

        public long getItemId(int i) {
            return -1L;
        }

        public int getItemViewType(int i) {
            return 0;
        }

        public final StateRestorationPolicy getStateRestorationPolicy() {
            return this.mStateRestorationPolicy;
        }

        public final boolean hasObservers() {
            return this.mObservable.a();
        }

        public final boolean hasStableIds() {
            return this.mHasStableIds;
        }

        public final void notifyDataSetChanged() {
            this.mObservable.b();
        }

        public final void notifyItemChanged(int i) {
            this.mObservable.d(i, 1);
        }

        public final void notifyItemInserted(int i) {
            this.mObservable.f(i, 1);
        }

        public final void notifyItemMoved(int i, int i2) {
            this.mObservable.c(i, i2);
        }

        public final void notifyItemRangeChanged(int i, int i2) {
            this.mObservable.d(i, i2);
        }

        public final void notifyItemRangeInserted(int i, int i2) {
            this.mObservable.f(i, i2);
        }

        public final void notifyItemRangeRemoved(int i, int i2) {
            this.mObservable.g(i, i2);
        }

        public final void notifyItemRemoved(int i) {
            this.mObservable.g(i, 1);
        }

        public void onAttachedToRecyclerView(RecyclerView recyclerView) {
        }

        public abstract void onBindViewHolder(VH vh, int i);

        public void onBindViewHolder(VH vh, int i, List<Object> list) {
            onBindViewHolder(vh, i);
        }

        public abstract VH onCreateViewHolder(ViewGroup viewGroup, int i);

        public void onDetachedFromRecyclerView(RecyclerView recyclerView) {
        }

        public boolean onFailedToRecycleView(VH vh) {
            return false;
        }

        public void onViewAttachedToWindow(VH vh) {
        }

        public void onViewDetachedFromWindow(VH vh) {
        }

        public void onViewRecycled(VH vh) {
        }

        public void registerAdapterDataObserver(i iVar) {
            this.mObservable.registerObserver(iVar);
        }

        public void setHasStableIds(boolean z) {
            if (!hasObservers()) {
                this.mHasStableIds = z;
                return;
            }
            throw new IllegalStateException("Cannot change whether this adapter has stable IDs while the adapter has registered observers.");
        }

        public void setStateRestorationPolicy(StateRestorationPolicy stateRestorationPolicy) {
            this.mStateRestorationPolicy = stateRestorationPolicy;
            this.mObservable.h();
        }

        public void unregisterAdapterDataObserver(i iVar) {
            this.mObservable.unregisterObserver(iVar);
        }

        public final void notifyItemChanged(int i, Object obj) {
            this.mObservable.e(i, 1, obj);
        }

        public final void notifyItemRangeChanged(int i, int i2, Object obj) {
            this.mObservable.e(i, i2, obj);
        }
    }

    /* loaded from: classes.dex */
    public static abstract class LayoutManager {
        public androidx.recyclerview.widget.e a;
        public RecyclerView f0;
        public final w.b g0;
        public final w.b h0;
        public androidx.recyclerview.widget.w i0;
        public androidx.recyclerview.widget.w j0;
        public w k0;
        public boolean l0;
        public boolean m0;
        public boolean n0;
        public boolean o0;
        public boolean p0;
        public int q0;
        public boolean r0;
        public int s0;
        public int t0;
        public int u0;
        public int v0;

        /* loaded from: classes.dex */
        public static class Properties {
            public int a;
            public int b;
            public boolean c;
            public boolean d;
        }

        /* loaded from: classes.dex */
        public class a implements w.b {
            public a() {
            }

            @Override // androidx.recyclerview.widget.w.b
            public View a(int i) {
                return LayoutManager.this.T(i);
            }

            @Override // androidx.recyclerview.widget.w.b
            public int b(View view) {
                return LayoutManager.this.b0(view) - ((ViewGroup.MarginLayoutParams) ((LayoutParams) view.getLayoutParams())).leftMargin;
            }

            @Override // androidx.recyclerview.widget.w.b
            public int c() {
                return LayoutManager.this.getPaddingLeft();
            }

            @Override // androidx.recyclerview.widget.w.b
            public int d() {
                return LayoutManager.this.v0() - LayoutManager.this.getPaddingRight();
            }

            @Override // androidx.recyclerview.widget.w.b
            public int e(View view) {
                return LayoutManager.this.e0(view) + ((ViewGroup.MarginLayoutParams) ((LayoutParams) view.getLayoutParams())).rightMargin;
            }
        }

        /* loaded from: classes.dex */
        public class b implements w.b {
            public b() {
            }

            @Override // androidx.recyclerview.widget.w.b
            public View a(int i) {
                return LayoutManager.this.T(i);
            }

            @Override // androidx.recyclerview.widget.w.b
            public int b(View view) {
                return LayoutManager.this.f0(view) - ((ViewGroup.MarginLayoutParams) ((LayoutParams) view.getLayoutParams())).topMargin;
            }

            @Override // androidx.recyclerview.widget.w.b
            public int c() {
                return LayoutManager.this.getPaddingTop();
            }

            @Override // androidx.recyclerview.widget.w.b
            public int d() {
                return LayoutManager.this.h0() - LayoutManager.this.getPaddingBottom();
            }

            @Override // androidx.recyclerview.widget.w.b
            public int e(View view) {
                return LayoutManager.this.Z(view) + ((ViewGroup.MarginLayoutParams) ((LayoutParams) view.getLayoutParams())).bottomMargin;
            }
        }

        /* loaded from: classes.dex */
        public interface c {
            void a(int i, int i2);
        }

        public LayoutManager() {
            a aVar = new a();
            this.g0 = aVar;
            b bVar = new b();
            this.h0 = bVar;
            this.i0 = new androidx.recyclerview.widget.w(aVar);
            this.j0 = new androidx.recyclerview.widget.w(bVar);
            this.l0 = false;
            this.m0 = false;
            this.n0 = false;
            this.o0 = true;
            this.p0 = true;
        }

        public static boolean E0(int i, int i2, int i3) {
            int mode = View.MeasureSpec.getMode(i2);
            int size = View.MeasureSpec.getSize(i2);
            if (i3 <= 0 || i == i3) {
                if (mode == Integer.MIN_VALUE) {
                    return size >= i;
                } else if (mode != 0) {
                    return mode == 1073741824 && size == i;
                } else {
                    return true;
                }
            }
            return false;
        }

        /* JADX WARN: Code restructure failed: missing block: B:9:0x0017, code lost:
            if (r5 == 1073741824) goto L8;
         */
        /*
            Code decompiled incorrectly, please refer to instructions dump.
            To view partially-correct code enable 'Show inconsistent code' option in preferences
        */
        public static int V(int r4, int r5, int r6, int r7, boolean r8) {
            /*
                int r4 = r4 - r6
                r6 = 0
                int r4 = java.lang.Math.max(r6, r4)
                r0 = -2
                r1 = -1
                r2 = -2147483648(0xffffffff80000000, float:-0.0)
                r3 = 1073741824(0x40000000, float:2.0)
                if (r8 == 0) goto L1a
                if (r7 < 0) goto L11
                goto L1c
            L11:
                if (r7 != r1) goto L2f
                if (r5 == r2) goto L20
                if (r5 == 0) goto L2f
                if (r5 == r3) goto L20
                goto L2f
            L1a:
                if (r7 < 0) goto L1e
            L1c:
                r5 = r3
                goto L31
            L1e:
                if (r7 != r1) goto L22
            L20:
                r7 = r4
                goto L31
            L22:
                if (r7 != r0) goto L2f
                if (r5 == r2) goto L2c
                if (r5 != r3) goto L29
                goto L2c
            L29:
                r7 = r4
                r5 = r6
                goto L31
            L2c:
                r7 = r4
                r5 = r2
                goto L31
            L2f:
                r5 = r6
                r7 = r5
            L31:
                int r4 = android.view.View.MeasureSpec.makeMeasureSpec(r7, r5)
                return r4
            */
            throw new UnsupportedOperationException("Method not decompiled: androidx.recyclerview.widget.RecyclerView.LayoutManager.V(int, int, int, int, boolean):int");
        }

        public static Properties p0(Context context, AttributeSet attributeSet, int i, int i2) {
            Properties properties = new Properties();
            TypedArray obtainStyledAttributes = context.obtainStyledAttributes(attributeSet, e23.RecyclerView, i, i2);
            properties.a = obtainStyledAttributes.getInt(e23.RecyclerView_android_orientation, 1);
            properties.b = obtainStyledAttributes.getInt(e23.RecyclerView_spanCount, 1);
            properties.c = obtainStyledAttributes.getBoolean(e23.RecyclerView_reverseLayout, false);
            properties.d = obtainStyledAttributes.getBoolean(e23.RecyclerView_stackFromEnd, false);
            obtainStyledAttributes.recycle();
            return properties;
        }

        public static int y(int i, int i2, int i3) {
            int mode = View.MeasureSpec.getMode(i);
            int size = View.MeasureSpec.getSize(i);
            if (mode != Integer.MIN_VALUE) {
                return mode != 1073741824 ? Math.max(i2, i3) : size;
            }
            return Math.min(size, Math.max(i2, i3));
        }

        public void A(int i, c cVar) {
        }

        public final boolean A0(RecyclerView recyclerView, int i, int i2) {
            View focusedChild = recyclerView.getFocusedChild();
            if (focusedChild == null) {
                return false;
            }
            int paddingLeft = getPaddingLeft();
            int paddingTop = getPaddingTop();
            int v0 = v0() - getPaddingRight();
            int h0 = h0() - getPaddingBottom();
            Rect rect = this.f0.m0;
            a0(focusedChild, rect);
            return rect.left - i < v0 && rect.right - i > paddingLeft && rect.top - i2 < h0 && rect.bottom - i2 > paddingTop;
        }

        public boolean A1(RecyclerView recyclerView, View view, Rect rect, boolean z) {
            return B1(recyclerView, view, rect, z, false);
        }

        public int B(x xVar) {
            return 0;
        }

        public final boolean B0() {
            return this.p0;
        }

        public boolean B1(RecyclerView recyclerView, View view, Rect rect, boolean z, boolean z2) {
            int[] W = W(view, rect);
            int i = W[0];
            int i2 = W[1];
            if ((!z2 || A0(recyclerView, i, i2)) && !(i == 0 && i2 == 0)) {
                if (z) {
                    recyclerView.scrollBy(i, i2);
                } else {
                    recyclerView.s1(i, i2);
                }
                return true;
            }
            return false;
        }

        public int C(x xVar) {
            return 0;
        }

        public boolean C0(t tVar, x xVar) {
            return false;
        }

        public void C1() {
            RecyclerView recyclerView = this.f0;
            if (recyclerView != null) {
                recyclerView.requestLayout();
            }
        }

        public int D(x xVar) {
            return 0;
        }

        public boolean D0() {
            return this.o0;
        }

        public void D1() {
            this.l0 = true;
        }

        public int E(x xVar) {
            return 0;
        }

        public final void E1(t tVar, int i, View view) {
            a0 i0 = RecyclerView.i0(view);
            if (i0.shouldIgnore()) {
                return;
            }
            if (i0.isInvalid() && !i0.isRemoved() && !this.f0.p0.hasStableIds()) {
                z1(i);
                tVar.C(i0);
                return;
            }
            I(i);
            tVar.D(view);
            this.f0.j0.k(i0);
        }

        public int F(x xVar) {
            return 0;
        }

        public boolean F0() {
            w wVar = this.k0;
            return wVar != null && wVar.isRunning();
        }

        public int F1(int i, t tVar, x xVar) {
            return 0;
        }

        public int G(x xVar) {
            return 0;
        }

        public boolean G0(View view, boolean z, boolean z2) {
            boolean z3 = this.i0.b(view, 24579) && this.j0.b(view, 24579);
            return z ? z3 : !z3;
        }

        public void G1(int i) {
        }

        public void H(t tVar) {
            for (int U = U() - 1; U >= 0; U--) {
                E1(tVar, U, T(U));
            }
        }

        public void H0(View view, int i, int i2, int i3, int i4) {
            LayoutParams layoutParams = (LayoutParams) view.getLayoutParams();
            Rect rect = layoutParams.f0;
            view.layout(i + rect.left + ((ViewGroup.MarginLayoutParams) layoutParams).leftMargin, i2 + rect.top + ((ViewGroup.MarginLayoutParams) layoutParams).topMargin, (i3 - rect.right) - ((ViewGroup.MarginLayoutParams) layoutParams).rightMargin, (i4 - rect.bottom) - ((ViewGroup.MarginLayoutParams) layoutParams).bottomMargin);
        }

        public int H1(int i, t tVar, x xVar) {
            return 0;
        }

        public void I(int i) {
            J(i, T(i));
        }

        public void I0(View view, int i, int i2) {
            LayoutParams layoutParams = (LayoutParams) view.getLayoutParams();
            Rect n0 = this.f0.n0(view);
            int i3 = i + n0.left + n0.right;
            int i4 = i2 + n0.top + n0.bottom;
            int V = V(v0(), w0(), getPaddingLeft() + getPaddingRight() + ((ViewGroup.MarginLayoutParams) layoutParams).leftMargin + ((ViewGroup.MarginLayoutParams) layoutParams).rightMargin + i3, ((ViewGroup.MarginLayoutParams) layoutParams).width, v());
            int V2 = V(h0(), i0(), getPaddingTop() + getPaddingBottom() + ((ViewGroup.MarginLayoutParams) layoutParams).topMargin + ((ViewGroup.MarginLayoutParams) layoutParams).bottomMargin + i4, ((ViewGroup.MarginLayoutParams) layoutParams).height, w());
            if (O1(view, V, V2, layoutParams)) {
                view.measure(V, V2);
            }
        }

        public void I1(RecyclerView recyclerView) {
            J1(View.MeasureSpec.makeMeasureSpec(recyclerView.getWidth(), 1073741824), View.MeasureSpec.makeMeasureSpec(recyclerView.getHeight(), 1073741824));
        }

        public final void J(int i, View view) {
            this.a.d(i);
        }

        public void J0(int i, int i2) {
            View T = T(i);
            if (T != null) {
                I(i);
                s(T, i2);
                return;
            }
            throw new IllegalArgumentException("Cannot move a child from non-existing index:" + i + this.f0.toString());
        }

        public void J1(int i, int i2) {
            this.u0 = View.MeasureSpec.getSize(i);
            int mode = View.MeasureSpec.getMode(i);
            this.s0 = mode;
            if (mode == 0 && !RecyclerView.F1) {
                this.u0 = 0;
            }
            this.v0 = View.MeasureSpec.getSize(i2);
            int mode2 = View.MeasureSpec.getMode(i2);
            this.t0 = mode2;
            if (mode2 != 0 || RecyclerView.F1) {
                return;
            }
            this.v0 = 0;
        }

        public void K(RecyclerView recyclerView) {
            this.m0 = true;
            O0(recyclerView);
        }

        public void K0(int i) {
            RecyclerView recyclerView = this.f0;
            if (recyclerView != null) {
                recyclerView.E0(i);
            }
        }

        public void K1(int i, int i2) {
            this.f0.setMeasuredDimension(i, i2);
        }

        public void L(RecyclerView recyclerView, t tVar) {
            this.m0 = false;
            Q0(recyclerView, tVar);
        }

        public void L0(int i) {
            RecyclerView recyclerView = this.f0;
            if (recyclerView != null) {
                recyclerView.F0(i);
            }
        }

        public void L1(Rect rect, int i, int i2) {
            K1(y(i, rect.width() + getPaddingLeft() + getPaddingRight(), n0()), y(i2, rect.height() + getPaddingTop() + getPaddingBottom(), m0()));
        }

        public View M(View view) {
            View T;
            RecyclerView recyclerView = this.f0;
            if (recyclerView == null || (T = recyclerView.T(view)) == null || this.a.n(T)) {
                return null;
            }
            return T;
        }

        public void M0(Adapter adapter, Adapter adapter2) {
        }

        public void M1(int i, int i2) {
            int U = U();
            if (U == 0) {
                this.f0.x(i, i2);
                return;
            }
            int i3 = Integer.MIN_VALUE;
            int i4 = Integer.MAX_VALUE;
            int i5 = Integer.MAX_VALUE;
            int i6 = Integer.MIN_VALUE;
            for (int i7 = 0; i7 < U; i7++) {
                View T = T(i7);
                Rect rect = this.f0.m0;
                a0(T, rect);
                int i8 = rect.left;
                if (i8 < i4) {
                    i4 = i8;
                }
                int i9 = rect.right;
                if (i9 > i3) {
                    i3 = i9;
                }
                int i10 = rect.top;
                if (i10 < i5) {
                    i5 = i10;
                }
                int i11 = rect.bottom;
                if (i11 > i6) {
                    i6 = i11;
                }
            }
            this.f0.m0.set(i4, i5, i3, i6);
            L1(this.f0.m0, i, i2);
        }

        public View N(int i) {
            int U = U();
            for (int i2 = 0; i2 < U; i2++) {
                View T = T(i2);
                a0 i0 = RecyclerView.i0(T);
                if (i0 != null && i0.getLayoutPosition() == i && !i0.shouldIgnore() && (this.f0.j1.e() || !i0.isRemoved())) {
                    return T;
                }
            }
            return null;
        }

        public boolean N0(RecyclerView recyclerView, ArrayList<View> arrayList, int i, int i2) {
            return false;
        }

        public void N1(RecyclerView recyclerView) {
            if (recyclerView == null) {
                this.f0 = null;
                this.a = null;
                this.u0 = 0;
                this.v0 = 0;
            } else {
                this.f0 = recyclerView;
                this.a = recyclerView.i0;
                this.u0 = recyclerView.getWidth();
                this.v0 = recyclerView.getHeight();
            }
            this.s0 = 1073741824;
            this.t0 = 1073741824;
        }

        public abstract LayoutParams O();

        public void O0(RecyclerView recyclerView) {
        }

        public boolean O1(View view, int i, int i2, LayoutParams layoutParams) {
            return (!view.isLayoutRequested() && this.o0 && E0(view.getWidth(), i, ((ViewGroup.MarginLayoutParams) layoutParams).width) && E0(view.getHeight(), i2, ((ViewGroup.MarginLayoutParams) layoutParams).height)) ? false : true;
        }

        public LayoutParams P(Context context, AttributeSet attributeSet) {
            return new LayoutParams(context, attributeSet);
        }

        @Deprecated
        public void P0(RecyclerView recyclerView) {
        }

        public boolean P1() {
            return false;
        }

        public LayoutParams Q(ViewGroup.LayoutParams layoutParams) {
            if (layoutParams instanceof LayoutParams) {
                return new LayoutParams((LayoutParams) layoutParams);
            }
            if (layoutParams instanceof ViewGroup.MarginLayoutParams) {
                return new LayoutParams((ViewGroup.MarginLayoutParams) layoutParams);
            }
            return new LayoutParams(layoutParams);
        }

        public void Q0(RecyclerView recyclerView, t tVar) {
            P0(recyclerView);
        }

        public boolean Q1(View view, int i, int i2, LayoutParams layoutParams) {
            return (this.o0 && E0(view.getMeasuredWidth(), i, ((ViewGroup.MarginLayoutParams) layoutParams).width) && E0(view.getMeasuredHeight(), i2, ((ViewGroup.MarginLayoutParams) layoutParams).height)) ? false : true;
        }

        public int R() {
            return -1;
        }

        public View R0(View view, int i, t tVar, x xVar) {
            return null;
        }

        public void R1(RecyclerView recyclerView, x xVar, int i) {
        }

        public int S(View view) {
            return ((LayoutParams) view.getLayoutParams()).f0.bottom;
        }

        public void S0(AccessibilityEvent accessibilityEvent) {
            RecyclerView recyclerView = this.f0;
            T0(recyclerView.f0, recyclerView.j1, accessibilityEvent);
        }

        public void S1(w wVar) {
            w wVar2 = this.k0;
            if (wVar2 != null && wVar != wVar2 && wVar2.isRunning()) {
                this.k0.stop();
            }
            this.k0 = wVar;
            wVar.start(this.f0, this);
        }

        public View T(int i) {
            androidx.recyclerview.widget.e eVar = this.a;
            if (eVar != null) {
                return eVar.f(i);
            }
            return null;
        }

        public void T0(t tVar, x xVar, AccessibilityEvent accessibilityEvent) {
            RecyclerView recyclerView = this.f0;
            if (recyclerView == null || accessibilityEvent == null) {
                return;
            }
            boolean z = true;
            if (!recyclerView.canScrollVertically(1) && !this.f0.canScrollVertically(-1) && !this.f0.canScrollHorizontally(-1) && !this.f0.canScrollHorizontally(1)) {
                z = false;
            }
            accessibilityEvent.setScrollable(z);
            Adapter adapter = this.f0.p0;
            if (adapter != null) {
                accessibilityEvent.setItemCount(adapter.getItemCount());
            }
        }

        public void T1() {
            w wVar = this.k0;
            if (wVar != null) {
                wVar.stop();
            }
        }

        public int U() {
            androidx.recyclerview.widget.e eVar = this.a;
            if (eVar != null) {
                return eVar.g();
            }
            return 0;
        }

        public void U0(b6 b6Var) {
            RecyclerView recyclerView = this.f0;
            V0(recyclerView.f0, recyclerView.j1, b6Var);
        }

        public boolean U1() {
            return false;
        }

        public void V0(t tVar, x xVar, b6 b6Var) {
            if (this.f0.canScrollVertically(-1) || this.f0.canScrollHorizontally(-1)) {
                b6Var.a(8192);
                b6Var.y0(true);
            }
            if (this.f0.canScrollVertically(1) || this.f0.canScrollHorizontally(1)) {
                b6Var.a(4096);
                b6Var.y0(true);
            }
            b6Var.e0(b6.b.b(r0(tVar, xVar), Y(tVar, xVar), C0(tVar, xVar), s0(tVar, xVar)));
        }

        public final int[] W(View view, Rect rect) {
            int[] iArr = new int[2];
            int paddingLeft = getPaddingLeft();
            int paddingTop = getPaddingTop();
            int v0 = v0() - getPaddingRight();
            int h0 = h0() - getPaddingBottom();
            int left = (view.getLeft() + rect.left) - view.getScrollX();
            int top = (view.getTop() + rect.top) - view.getScrollY();
            int width = rect.width() + left;
            int height = rect.height() + top;
            int i = left - paddingLeft;
            int min = Math.min(0, i);
            int i2 = top - paddingTop;
            int min2 = Math.min(0, i2);
            int i3 = width - v0;
            int max = Math.max(0, i3);
            int max2 = Math.max(0, height - h0);
            if (k0() != 1) {
                if (min == 0) {
                    min = Math.min(i, max);
                }
                max = min;
            } else if (max == 0) {
                max = Math.max(min, i3);
            }
            if (min2 == 0) {
                min2 = Math.min(i2, max2);
            }
            iArr[0] = max;
            iArr[1] = min2;
            return iArr;
        }

        public void W0(View view, b6 b6Var) {
            a0 i0 = RecyclerView.i0(view);
            if (i0 == null || i0.isRemoved() || this.a.n(i0.itemView)) {
                return;
            }
            RecyclerView recyclerView = this.f0;
            X0(recyclerView.f0, recyclerView.j1, view, b6Var);
        }

        public boolean X() {
            RecyclerView recyclerView = this.f0;
            return recyclerView != null && recyclerView.k0;
        }

        public void X0(t tVar, x xVar, View view, b6 b6Var) {
        }

        public int Y(t tVar, x xVar) {
            return -1;
        }

        public View Y0(View view, int i) {
            return null;
        }

        public int Z(View view) {
            return view.getBottom() + S(view);
        }

        public void Z0(RecyclerView recyclerView, int i, int i2) {
        }

        public void a0(View view, Rect rect) {
            RecyclerView.k0(view, rect);
        }

        public void a1(RecyclerView recyclerView) {
        }

        public int b0(View view) {
            return view.getLeft() - l0(view);
        }

        public void b1(RecyclerView recyclerView, int i, int i2, int i3) {
        }

        public int c0(View view) {
            Rect rect = ((LayoutParams) view.getLayoutParams()).f0;
            return view.getMeasuredHeight() + rect.top + rect.bottom;
        }

        public void c1(RecyclerView recyclerView, int i, int i2) {
        }

        public int d0(View view) {
            Rect rect = ((LayoutParams) view.getLayoutParams()).f0;
            return view.getMeasuredWidth() + rect.left + rect.right;
        }

        public void d1(RecyclerView recyclerView, int i, int i2) {
        }

        public int e0(View view) {
            return view.getRight() + q0(view);
        }

        public void e1(RecyclerView recyclerView, int i, int i2, Object obj) {
            d1(recyclerView, i, i2);
        }

        public int f0(View view) {
            return view.getTop() - t0(view);
        }

        public void f1(t tVar, x xVar) {
        }

        public View g0() {
            View focusedChild;
            RecyclerView recyclerView = this.f0;
            if (recyclerView == null || (focusedChild = recyclerView.getFocusedChild()) == null || this.a.n(focusedChild)) {
                return null;
            }
            return focusedChild;
        }

        public void g1(x xVar) {
        }

        public int getPaddingBottom() {
            RecyclerView recyclerView = this.f0;
            if (recyclerView != null) {
                return recyclerView.getPaddingBottom();
            }
            return 0;
        }

        public int getPaddingEnd() {
            RecyclerView recyclerView = this.f0;
            if (recyclerView != null) {
                return ei4.I(recyclerView);
            }
            return 0;
        }

        public int getPaddingLeft() {
            RecyclerView recyclerView = this.f0;
            if (recyclerView != null) {
                return recyclerView.getPaddingLeft();
            }
            return 0;
        }

        public int getPaddingRight() {
            RecyclerView recyclerView = this.f0;
            if (recyclerView != null) {
                return recyclerView.getPaddingRight();
            }
            return 0;
        }

        public int getPaddingStart() {
            RecyclerView recyclerView = this.f0;
            if (recyclerView != null) {
                return ei4.J(recyclerView);
            }
            return 0;
        }

        public int getPaddingTop() {
            RecyclerView recyclerView = this.f0;
            if (recyclerView != null) {
                return recyclerView.getPaddingTop();
            }
            return 0;
        }

        public int h0() {
            return this.v0;
        }

        public void h1(t tVar, x xVar, int i, int i2) {
            this.f0.x(i, i2);
        }

        public int i0() {
            return this.t0;
        }

        @Deprecated
        public boolean i1(RecyclerView recyclerView, View view, View view2) {
            return F0() || recyclerView.y0();
        }

        public int j0() {
            RecyclerView recyclerView = this.f0;
            Adapter adapter = recyclerView != null ? recyclerView.getAdapter() : null;
            if (adapter != null) {
                return adapter.getItemCount();
            }
            return 0;
        }

        public boolean j1(RecyclerView recyclerView, x xVar, View view, View view2) {
            return i1(recyclerView, view, view2);
        }

        public int k0() {
            return ei4.E(this.f0);
        }

        public void k1(Parcelable parcelable) {
        }

        public int l0(View view) {
            return ((LayoutParams) view.getLayoutParams()).f0.left;
        }

        public Parcelable l1() {
            return null;
        }

        public void m(View view) {
            n(view, -1);
        }

        public int m0() {
            return ei4.F(this.f0);
        }

        public void m1(int i) {
        }

        public void n(View view, int i) {
            q(view, i, true);
        }

        public int n0() {
            return ei4.G(this.f0);
        }

        public void n1(w wVar) {
            if (this.k0 == wVar) {
                this.k0 = null;
            }
        }

        public void o(View view) {
            p(view, -1);
        }

        public int o0(View view) {
            return ((LayoutParams) view.getLayoutParams()).a();
        }

        public boolean o1(int i, Bundle bundle) {
            RecyclerView recyclerView = this.f0;
            return p1(recyclerView.f0, recyclerView.j1, i, bundle);
        }

        public void p(View view, int i) {
            q(view, i, false);
        }

        public boolean p1(t tVar, x xVar, int i, Bundle bundle) {
            int h0;
            int v0;
            int i2;
            int i3;
            RecyclerView recyclerView = this.f0;
            if (recyclerView == null) {
                return false;
            }
            if (i == 4096) {
                h0 = recyclerView.canScrollVertically(1) ? (h0() - getPaddingTop()) - getPaddingBottom() : 0;
                if (this.f0.canScrollHorizontally(1)) {
                    v0 = (v0() - getPaddingLeft()) - getPaddingRight();
                    i2 = h0;
                    i3 = v0;
                }
                i2 = h0;
                i3 = 0;
            } else if (i != 8192) {
                i3 = 0;
                i2 = 0;
            } else {
                h0 = recyclerView.canScrollVertically(-1) ? -((h0() - getPaddingTop()) - getPaddingBottom()) : 0;
                if (this.f0.canScrollHorizontally(-1)) {
                    v0 = -((v0() - getPaddingLeft()) - getPaddingRight());
                    i2 = h0;
                    i3 = v0;
                }
                i2 = h0;
                i3 = 0;
            }
            if (i2 == 0 && i3 == 0) {
                return false;
            }
            this.f0.v1(i3, i2, null, Integer.MIN_VALUE, true);
            return true;
        }

        public final void q(View view, int i, boolean z) {
            a0 i0 = RecyclerView.i0(view);
            if (!z && !i0.isRemoved()) {
                this.f0.j0.p(i0);
            } else {
                this.f0.j0.b(i0);
            }
            LayoutParams layoutParams = (LayoutParams) view.getLayoutParams();
            if (!i0.wasReturnedFromScrap() && !i0.isScrap()) {
                if (view.getParent() == this.f0) {
                    int m = this.a.m(view);
                    if (i == -1) {
                        i = this.a.g();
                    }
                    if (m == -1) {
                        throw new IllegalStateException("Added View has RecyclerView as parent but view is not a real child. Unfiltered index:" + this.f0.indexOfChild(view) + this.f0.Q());
                    } else if (m != i) {
                        this.f0.q0.J0(m, i);
                    }
                } else {
                    this.a.a(view, i, false);
                    layoutParams.g0 = true;
                    w wVar = this.k0;
                    if (wVar != null && wVar.isRunning()) {
                        this.k0.onChildAttachedToWindow(view);
                    }
                }
            } else {
                if (i0.isScrap()) {
                    i0.unScrap();
                } else {
                    i0.clearReturnedFromScrapFlag();
                }
                this.a.c(view, i, view.getLayoutParams(), false);
            }
            if (layoutParams.h0) {
                i0.itemView.invalidate();
                layoutParams.h0 = false;
            }
        }

        public int q0(View view) {
            return ((LayoutParams) view.getLayoutParams()).f0.right;
        }

        public boolean q1(View view, int i, Bundle bundle) {
            RecyclerView recyclerView = this.f0;
            return r1(recyclerView.f0, recyclerView.j1, view, i, bundle);
        }

        public void r(String str) {
            RecyclerView recyclerView = this.f0;
            if (recyclerView != null) {
                recyclerView.p(str);
            }
        }

        public int r0(t tVar, x xVar) {
            return -1;
        }

        public boolean r1(t tVar, x xVar, View view, int i, Bundle bundle) {
            return false;
        }

        public void s(View view, int i) {
            t(view, i, (LayoutParams) view.getLayoutParams());
        }

        public int s0(t tVar, x xVar) {
            return 0;
        }

        public void s1() {
            for (int U = U() - 1; U >= 0; U--) {
                this.a.q(U);
            }
        }

        public void t(View view, int i, LayoutParams layoutParams) {
            a0 i0 = RecyclerView.i0(view);
            if (i0.isRemoved()) {
                this.f0.j0.b(i0);
            } else {
                this.f0.j0.p(i0);
            }
            this.a.c(view, i, layoutParams, i0.isRemoved());
        }

        public int t0(View view) {
            return ((LayoutParams) view.getLayoutParams()).f0.top;
        }

        public void t1(t tVar) {
            for (int U = U() - 1; U >= 0; U--) {
                if (!RecyclerView.i0(T(U)).shouldIgnore()) {
                    w1(U, tVar);
                }
            }
        }

        public void u(View view, Rect rect) {
            RecyclerView recyclerView = this.f0;
            if (recyclerView == null) {
                rect.set(0, 0, 0, 0);
            } else {
                rect.set(recyclerView.n0(view));
            }
        }

        public void u0(View view, boolean z, Rect rect) {
            Matrix matrix;
            if (z) {
                Rect rect2 = ((LayoutParams) view.getLayoutParams()).f0;
                rect.set(-rect2.left, -rect2.top, view.getWidth() + rect2.right, view.getHeight() + rect2.bottom);
            } else {
                rect.set(0, 0, view.getWidth(), view.getHeight());
            }
            if (this.f0 != null && (matrix = view.getMatrix()) != null && !matrix.isIdentity()) {
                RectF rectF = this.f0.o0;
                rectF.set(rect);
                matrix.mapRect(rectF);
                rect.set((int) Math.floor(rectF.left), (int) Math.floor(rectF.top), (int) Math.ceil(rectF.right), (int) Math.ceil(rectF.bottom));
            }
            rect.offset(view.getLeft(), view.getTop());
        }

        public void u1(t tVar) {
            int j = tVar.j();
            for (int i = j - 1; i >= 0; i--) {
                View n = tVar.n(i);
                a0 i0 = RecyclerView.i0(n);
                if (!i0.shouldIgnore()) {
                    i0.setIsRecyclable(false);
                    if (i0.isTmpDetached()) {
                        this.f0.removeDetachedView(n, false);
                    }
                    l lVar = this.f0.R0;
                    if (lVar != null) {
                        lVar.endAnimation(i0);
                    }
                    i0.setIsRecyclable(true);
                    tVar.y(n);
                }
            }
            tVar.e();
            if (j > 0) {
                this.f0.invalidate();
            }
        }

        public boolean v() {
            return false;
        }

        public int v0() {
            return this.u0;
        }

        public void v1(View view, t tVar) {
            y1(view);
            tVar.B(view);
        }

        public boolean w() {
            return false;
        }

        public int w0() {
            return this.s0;
        }

        public void w1(int i, t tVar) {
            View T = T(i);
            z1(i);
            tVar.B(T);
        }

        public boolean x(LayoutParams layoutParams) {
            return layoutParams != null;
        }

        public boolean x0() {
            int U = U();
            for (int i = 0; i < U; i++) {
                ViewGroup.LayoutParams layoutParams = T(i).getLayoutParams();
                if (layoutParams.width < 0 && layoutParams.height < 0) {
                    return true;
                }
            }
            return false;
        }

        public boolean x1(Runnable runnable) {
            RecyclerView recyclerView = this.f0;
            if (recyclerView != null) {
                return recyclerView.removeCallbacks(runnable);
            }
            return false;
        }

        public boolean y0() {
            return this.m0;
        }

        public void y1(View view) {
            this.a.p(view);
        }

        public void z(int i, int i2, x xVar, c cVar) {
        }

        public boolean z0() {
            return this.n0;
        }

        public void z1(int i) {
            if (T(i) != null) {
                this.a.q(i);
            }
        }
    }

    /* loaded from: classes.dex */
    public class a implements Runnable {
        public a() {
        }

        @Override // java.lang.Runnable
        public void run() {
            RecyclerView recyclerView = RecyclerView.this;
            if (!recyclerView.z0 || recyclerView.isLayoutRequested()) {
                return;
            }
            RecyclerView recyclerView2 = RecyclerView.this;
            if (!recyclerView2.w0) {
                recyclerView2.requestLayout();
            } else if (recyclerView2.C0) {
                recyclerView2.B0 = true;
            } else {
                recyclerView2.v();
            }
        }
    }

    /* loaded from: classes.dex */
    public static abstract class a0 {
        public static final int FLAG_ADAPTER_FULLUPDATE = 1024;
        public static final int FLAG_ADAPTER_POSITION_UNKNOWN = 512;
        public static final int FLAG_APPEARED_IN_PRE_LAYOUT = 4096;
        public static final int FLAG_BOUNCED_FROM_HIDDEN_LIST = 8192;
        public static final int FLAG_BOUND = 1;
        public static final int FLAG_IGNORE = 128;
        public static final int FLAG_INVALID = 4;
        public static final int FLAG_MOVED = 2048;
        public static final int FLAG_NOT_RECYCLABLE = 16;
        public static final int FLAG_REMOVED = 8;
        public static final int FLAG_RETURNED_FROM_SCRAP = 32;
        public static final int FLAG_TMP_DETACHED = 256;
        public static final int FLAG_UPDATE = 2;
        private static final List<Object> FULLUPDATE_PAYLOADS = Collections.emptyList();
        public static final int PENDING_ACCESSIBILITY_STATE_NOT_SET = -1;
        public final View itemView;
        public Adapter<? extends a0> mBindingAdapter;
        public int mFlags;
        public WeakReference<RecyclerView> mNestedRecyclerView;
        public RecyclerView mOwnerRecyclerView;
        public int mPosition = -1;
        public int mOldPosition = -1;
        public long mItemId = -1;
        public int mItemViewType = -1;
        public int mPreLayoutPosition = -1;
        public a0 mShadowedHolder = null;
        public a0 mShadowingHolder = null;
        public List<Object> mPayloads = null;
        public List<Object> mUnmodifiedPayloads = null;
        private int mIsRecyclableCount = 0;
        public t mScrapContainer = null;
        public boolean mInChangeScrap = false;
        private int mWasImportantForAccessibilityBeforeHidden = 0;
        public int mPendingAccessibilityState = -1;

        public a0(View view) {
            if (view != null) {
                this.itemView = view;
                return;
            }
            throw new IllegalArgumentException("itemView may not be null");
        }

        private void createPayloadsIfNeeded() {
            if (this.mPayloads == null) {
                ArrayList arrayList = new ArrayList();
                this.mPayloads = arrayList;
                this.mUnmodifiedPayloads = Collections.unmodifiableList(arrayList);
            }
        }

        public void addChangePayload(Object obj) {
            if (obj == null) {
                addFlags(FLAG_ADAPTER_FULLUPDATE);
            } else if ((1024 & this.mFlags) == 0) {
                createPayloadsIfNeeded();
                this.mPayloads.add(obj);
            }
        }

        public void addFlags(int i) {
            this.mFlags = i | this.mFlags;
        }

        public void clearOldPosition() {
            this.mOldPosition = -1;
            this.mPreLayoutPosition = -1;
        }

        public void clearPayload() {
            List<Object> list = this.mPayloads;
            if (list != null) {
                list.clear();
            }
            this.mFlags &= -1025;
        }

        public void clearReturnedFromScrapFlag() {
            this.mFlags &= -33;
        }

        public void clearTmpDetachFlag() {
            this.mFlags &= -257;
        }

        public boolean doesTransientStatePreventRecycling() {
            return (this.mFlags & 16) == 0 && ei4.T(this.itemView);
        }

        public void flagRemovedAndOffsetPosition(int i, int i2, boolean z) {
            addFlags(8);
            offsetPosition(i2, z);
            this.mPosition = i;
        }

        public final int getAbsoluteAdapterPosition() {
            RecyclerView recyclerView = this.mOwnerRecyclerView;
            if (recyclerView == null) {
                return -1;
            }
            return recyclerView.d0(this);
        }

        @Deprecated
        public final int getAdapterPosition() {
            return getBindingAdapterPosition();
        }

        public final Adapter<? extends a0> getBindingAdapter() {
            return this.mBindingAdapter;
        }

        public final int getBindingAdapterPosition() {
            RecyclerView recyclerView;
            Adapter adapter;
            int d0;
            if (this.mBindingAdapter == null || (recyclerView = this.mOwnerRecyclerView) == null || (adapter = recyclerView.getAdapter()) == null || (d0 = this.mOwnerRecyclerView.d0(this)) == -1) {
                return -1;
            }
            return adapter.findRelativeAdapterPositionIn(this.mBindingAdapter, this, d0);
        }

        public final long getItemId() {
            return this.mItemId;
        }

        public final int getItemViewType() {
            return this.mItemViewType;
        }

        public final int getLayoutPosition() {
            int i = this.mPreLayoutPosition;
            return i == -1 ? this.mPosition : i;
        }

        public final int getOldPosition() {
            return this.mOldPosition;
        }

        @Deprecated
        public final int getPosition() {
            int i = this.mPreLayoutPosition;
            return i == -1 ? this.mPosition : i;
        }

        public List<Object> getUnmodifiedPayloads() {
            if ((this.mFlags & FLAG_ADAPTER_FULLUPDATE) == 0) {
                List<Object> list = this.mPayloads;
                if (list != null && list.size() != 0) {
                    return this.mUnmodifiedPayloads;
                }
                return FULLUPDATE_PAYLOADS;
            }
            return FULLUPDATE_PAYLOADS;
        }

        public boolean hasAnyOfTheFlags(int i) {
            return (i & this.mFlags) != 0;
        }

        public boolean isAdapterPositionUnknown() {
            return (this.mFlags & FLAG_ADAPTER_POSITION_UNKNOWN) != 0 || isInvalid();
        }

        public boolean isAttachedToTransitionOverlay() {
            return (this.itemView.getParent() == null || this.itemView.getParent() == this.mOwnerRecyclerView) ? false : true;
        }

        public boolean isBound() {
            return (this.mFlags & 1) != 0;
        }

        public boolean isInvalid() {
            return (this.mFlags & 4) != 0;
        }

        public final boolean isRecyclable() {
            return (this.mFlags & 16) == 0 && !ei4.T(this.itemView);
        }

        public boolean isRemoved() {
            return (this.mFlags & 8) != 0;
        }

        public boolean isScrap() {
            return this.mScrapContainer != null;
        }

        public boolean isTmpDetached() {
            return (this.mFlags & 256) != 0;
        }

        public boolean isUpdated() {
            return (this.mFlags & 2) != 0;
        }

        public boolean needsUpdate() {
            return (this.mFlags & 2) != 0;
        }

        public void offsetPosition(int i, boolean z) {
            if (this.mOldPosition == -1) {
                this.mOldPosition = this.mPosition;
            }
            if (this.mPreLayoutPosition == -1) {
                this.mPreLayoutPosition = this.mPosition;
            }
            if (z) {
                this.mPreLayoutPosition += i;
            }
            this.mPosition += i;
            if (this.itemView.getLayoutParams() != null) {
                ((LayoutParams) this.itemView.getLayoutParams()).g0 = true;
            }
        }

        public void onEnteredHiddenState(RecyclerView recyclerView) {
            int i = this.mPendingAccessibilityState;
            if (i != -1) {
                this.mWasImportantForAccessibilityBeforeHidden = i;
            } else {
                this.mWasImportantForAccessibilityBeforeHidden = ei4.C(this.itemView);
            }
            recyclerView.q1(this, 4);
        }

        public void onLeftHiddenState(RecyclerView recyclerView) {
            recyclerView.q1(this, this.mWasImportantForAccessibilityBeforeHidden);
            this.mWasImportantForAccessibilityBeforeHidden = 0;
        }

        public void resetInternal() {
            this.mFlags = 0;
            this.mPosition = -1;
            this.mOldPosition = -1;
            this.mItemId = -1L;
            this.mPreLayoutPosition = -1;
            this.mIsRecyclableCount = 0;
            this.mShadowedHolder = null;
            this.mShadowingHolder = null;
            clearPayload();
            this.mWasImportantForAccessibilityBeforeHidden = 0;
            this.mPendingAccessibilityState = -1;
            RecyclerView.s(this);
        }

        public void saveOldPosition() {
            if (this.mOldPosition == -1) {
                this.mOldPosition = this.mPosition;
            }
        }

        public void setFlags(int i, int i2) {
            this.mFlags = (i & i2) | (this.mFlags & (~i2));
        }

        public final void setIsRecyclable(boolean z) {
            int i = this.mIsRecyclableCount;
            int i2 = z ? i - 1 : i + 1;
            this.mIsRecyclableCount = i2;
            if (i2 < 0) {
                this.mIsRecyclableCount = 0;
                StringBuilder sb = new StringBuilder();
                sb.append("isRecyclable decremented below 0: unmatched pair of setIsRecyable() calls for ");
                sb.append(this);
            } else if (!z && i2 == 1) {
                this.mFlags |= 16;
            } else if (z && i2 == 0) {
                this.mFlags &= -17;
            }
        }

        public void setScrapContainer(t tVar, boolean z) {
            this.mScrapContainer = tVar;
            this.mInChangeScrap = z;
        }

        public boolean shouldBeKeptAsChild() {
            return (this.mFlags & 16) != 0;
        }

        public boolean shouldIgnore() {
            return (this.mFlags & 128) != 0;
        }

        public void stopIgnoring() {
            this.mFlags &= -129;
        }

        public String toString() {
            String simpleName = getClass().isAnonymousClass() ? "ViewHolder" : getClass().getSimpleName();
            StringBuilder sb = new StringBuilder(simpleName + "{" + Integer.toHexString(hashCode()) + " position=" + this.mPosition + " id=" + this.mItemId + ", oldPos=" + this.mOldPosition + ", pLpos:" + this.mPreLayoutPosition);
            if (isScrap()) {
                sb.append(" scrap ");
                sb.append(this.mInChangeScrap ? "[changeScrap]" : "[attachedScrap]");
            }
            if (isInvalid()) {
                sb.append(" invalid");
            }
            if (!isBound()) {
                sb.append(" unbound");
            }
            if (needsUpdate()) {
                sb.append(" update");
            }
            if (isRemoved()) {
                sb.append(" removed");
            }
            if (shouldIgnore()) {
                sb.append(" ignored");
            }
            if (isTmpDetached()) {
                sb.append(" tmpDetached");
            }
            if (!isRecyclable()) {
                sb.append(" not recyclable(" + this.mIsRecyclableCount + ")");
            }
            if (isAdapterPositionUnknown()) {
                sb.append(" undefined adapter position");
            }
            if (this.itemView.getParent() == null) {
                sb.append(" no parent");
            }
            sb.append("}");
            return sb.toString();
        }

        public void unScrap() {
            this.mScrapContainer.J(this);
        }

        public boolean wasReturnedFromScrap() {
            return (this.mFlags & 32) != 0;
        }
    }

    /* loaded from: classes.dex */
    public class b implements Runnable {
        public b() {
        }

        @Override // java.lang.Runnable
        public void run() {
            l lVar = RecyclerView.this.R0;
            if (lVar != null) {
                lVar.runPendingAnimations();
            }
            RecyclerView.this.p1 = false;
        }
    }

    /* loaded from: classes.dex */
    public class c implements Interpolator {
        @Override // android.animation.TimeInterpolator
        public float getInterpolation(float f) {
            float f2 = f - 1.0f;
            return (f2 * f2 * f2 * f2 * f2) + 1.0f;
        }
    }

    /* loaded from: classes.dex */
    public class d implements x.b {
        public d() {
        }

        @Override // androidx.recyclerview.widget.x.b
        public void a(a0 a0Var) {
            RecyclerView recyclerView = RecyclerView.this;
            recyclerView.q0.v1(a0Var.itemView, recyclerView.f0);
        }

        @Override // androidx.recyclerview.widget.x.b
        public void b(a0 a0Var, l.c cVar, l.c cVar2) {
            RecyclerView.this.m(a0Var, cVar, cVar2);
        }

        @Override // androidx.recyclerview.widget.x.b
        public void c(a0 a0Var, l.c cVar, l.c cVar2) {
            RecyclerView.this.f0.J(a0Var);
            RecyclerView.this.o(a0Var, cVar, cVar2);
        }

        @Override // androidx.recyclerview.widget.x.b
        public void d(a0 a0Var, l.c cVar, l.c cVar2) {
            a0Var.setIsRecyclable(false);
            RecyclerView recyclerView = RecyclerView.this;
            if (recyclerView.I0) {
                if (recyclerView.R0.animateChange(a0Var, a0Var, cVar, cVar2)) {
                    RecyclerView.this.R0();
                }
            } else if (recyclerView.R0.animatePersistence(a0Var, cVar, cVar2)) {
                RecyclerView.this.R0();
            }
        }
    }

    /* loaded from: classes.dex */
    public class e implements e.b {
        public e() {
        }

        @Override // androidx.recyclerview.widget.e.b
        public View a(int i) {
            return RecyclerView.this.getChildAt(i);
        }

        @Override // androidx.recyclerview.widget.e.b
        public void b(View view) {
            a0 i0 = RecyclerView.i0(view);
            if (i0 != null) {
                i0.onEnteredHiddenState(RecyclerView.this);
            }
        }

        @Override // androidx.recyclerview.widget.e.b
        public int c() {
            return RecyclerView.this.getChildCount();
        }

        @Override // androidx.recyclerview.widget.e.b
        public void d() {
            int c = c();
            for (int i = 0; i < c; i++) {
                View a = a(i);
                RecyclerView.this.A(a);
                a.clearAnimation();
            }
            RecyclerView.this.removeAllViews();
        }

        @Override // androidx.recyclerview.widget.e.b
        public int e(View view) {
            return RecyclerView.this.indexOfChild(view);
        }

        @Override // androidx.recyclerview.widget.e.b
        public a0 f(View view) {
            return RecyclerView.i0(view);
        }

        @Override // androidx.recyclerview.widget.e.b
        public void g(int i) {
            a0 i0;
            View a = a(i);
            if (a != null && (i0 = RecyclerView.i0(a)) != null) {
                if (i0.isTmpDetached() && !i0.shouldIgnore()) {
                    throw new IllegalArgumentException("called detach on an already detached child " + i0 + RecyclerView.this.Q());
                }
                i0.addFlags(256);
            }
            RecyclerView.this.detachViewFromParent(i);
        }

        @Override // androidx.recyclerview.widget.e.b
        public void h(View view) {
            a0 i0 = RecyclerView.i0(view);
            if (i0 != null) {
                i0.onLeftHiddenState(RecyclerView.this);
            }
        }

        @Override // androidx.recyclerview.widget.e.b
        public void i(View view, int i) {
            RecyclerView.this.addView(view, i);
            RecyclerView.this.z(view);
        }

        @Override // androidx.recyclerview.widget.e.b
        public void j(int i) {
            View childAt = RecyclerView.this.getChildAt(i);
            if (childAt != null) {
                RecyclerView.this.A(childAt);
                childAt.clearAnimation();
            }
            RecyclerView.this.removeViewAt(i);
        }

        @Override // androidx.recyclerview.widget.e.b
        public void k(View view, int i, ViewGroup.LayoutParams layoutParams) {
            a0 i0 = RecyclerView.i0(view);
            if (i0 != null) {
                if (!i0.isTmpDetached() && !i0.shouldIgnore()) {
                    throw new IllegalArgumentException("Called attach on a child which is not detached: " + i0 + RecyclerView.this.Q());
                }
                i0.clearTmpDetachFlag();
            }
            RecyclerView.this.attachViewToParent(view, i, layoutParams);
        }
    }

    /* loaded from: classes.dex */
    public class f implements a.InterfaceC0056a {
        public f() {
        }

        @Override // androidx.recyclerview.widget.a.InterfaceC0056a
        public void a(int i, int i2) {
            RecyclerView.this.H0(i, i2);
            RecyclerView.this.m1 = true;
        }

        @Override // androidx.recyclerview.widget.a.InterfaceC0056a
        public void b(a.b bVar) {
            i(bVar);
        }

        @Override // androidx.recyclerview.widget.a.InterfaceC0056a
        public void c(int i, int i2, Object obj) {
            RecyclerView.this.D1(i, i2, obj);
            RecyclerView.this.n1 = true;
        }

        @Override // androidx.recyclerview.widget.a.InterfaceC0056a
        public void d(a.b bVar) {
            i(bVar);
        }

        @Override // androidx.recyclerview.widget.a.InterfaceC0056a
        public a0 e(int i) {
            a0 b0 = RecyclerView.this.b0(i, true);
            if (b0 == null || RecyclerView.this.i0.n(b0.itemView)) {
                return null;
            }
            return b0;
        }

        @Override // androidx.recyclerview.widget.a.InterfaceC0056a
        public void f(int i, int i2) {
            RecyclerView.this.I0(i, i2, false);
            RecyclerView.this.m1 = true;
        }

        @Override // androidx.recyclerview.widget.a.InterfaceC0056a
        public void g(int i, int i2) {
            RecyclerView.this.G0(i, i2);
            RecyclerView.this.m1 = true;
        }

        @Override // androidx.recyclerview.widget.a.InterfaceC0056a
        public void h(int i, int i2) {
            RecyclerView.this.I0(i, i2, true);
            RecyclerView recyclerView = RecyclerView.this;
            recyclerView.m1 = true;
            recyclerView.j1.d += i2;
        }

        public void i(a.b bVar) {
            int i = bVar.a;
            if (i == 1) {
                RecyclerView recyclerView = RecyclerView.this;
                recyclerView.q0.Z0(recyclerView, bVar.b, bVar.d);
            } else if (i == 2) {
                RecyclerView recyclerView2 = RecyclerView.this;
                recyclerView2.q0.c1(recyclerView2, bVar.b, bVar.d);
            } else if (i == 4) {
                RecyclerView recyclerView3 = RecyclerView.this;
                recyclerView3.q0.e1(recyclerView3, bVar.b, bVar.d, bVar.c);
            } else if (i != 8) {
            } else {
                RecyclerView recyclerView4 = RecyclerView.this;
                recyclerView4.q0.b1(recyclerView4, bVar.b, bVar.d, 1);
            }
        }
    }

    /* loaded from: classes.dex */
    public static /* synthetic */ class g {
        public static final /* synthetic */ int[] a;

        static {
            int[] iArr = new int[Adapter.StateRestorationPolicy.values().length];
            a = iArr;
            try {
                iArr[Adapter.StateRestorationPolicy.PREVENT.ordinal()] = 1;
            } catch (NoSuchFieldError unused) {
            }
            try {
                a[Adapter.StateRestorationPolicy.PREVENT_WHEN_EMPTY.ordinal()] = 2;
            } catch (NoSuchFieldError unused2) {
            }
        }
    }

    /* loaded from: classes.dex */
    public static class h extends Observable<i> {
        public boolean a() {
            return !((Observable) this).mObservers.isEmpty();
        }

        public void b() {
            for (int size = ((Observable) this).mObservers.size() - 1; size >= 0; size--) {
                ((i) ((Observable) this).mObservers.get(size)).onChanged();
            }
        }

        public void c(int i, int i2) {
            for (int size = ((Observable) this).mObservers.size() - 1; size >= 0; size--) {
                ((i) ((Observable) this).mObservers.get(size)).onItemRangeMoved(i, i2, 1);
            }
        }

        public void d(int i, int i2) {
            e(i, i2, null);
        }

        public void e(int i, int i2, Object obj) {
            for (int size = ((Observable) this).mObservers.size() - 1; size >= 0; size--) {
                ((i) ((Observable) this).mObservers.get(size)).onItemRangeChanged(i, i2, obj);
            }
        }

        public void f(int i, int i2) {
            for (int size = ((Observable) this).mObservers.size() - 1; size >= 0; size--) {
                ((i) ((Observable) this).mObservers.get(size)).onItemRangeInserted(i, i2);
            }
        }

        public void g(int i, int i2) {
            for (int size = ((Observable) this).mObservers.size() - 1; size >= 0; size--) {
                ((i) ((Observable) this).mObservers.get(size)).onItemRangeRemoved(i, i2);
            }
        }

        public void h() {
            for (int size = ((Observable) this).mObservers.size() - 1; size >= 0; size--) {
                ((i) ((Observable) this).mObservers.get(size)).onStateRestorationPolicyChanged();
            }
        }
    }

    /* loaded from: classes.dex */
    public static abstract class i {
        public void onChanged() {
        }

        public void onItemRangeChanged(int i, int i2) {
        }

        public void onItemRangeChanged(int i, int i2, Object obj) {
            onItemRangeChanged(i, i2);
        }

        public void onItemRangeInserted(int i, int i2) {
        }

        public void onItemRangeMoved(int i, int i2, int i3) {
        }

        public void onItemRangeRemoved(int i, int i2) {
        }

        public void onStateRestorationPolicyChanged() {
        }
    }

    /* loaded from: classes.dex */
    public interface j {
        int a(int i, int i2);
    }

    /* loaded from: classes.dex */
    public static class k {
        public EdgeEffect a(RecyclerView recyclerView, int i) {
            return new EdgeEffect(recyclerView.getContext());
        }
    }

    /* loaded from: classes.dex */
    public static abstract class l {
        public static final int FLAG_APPEARED_IN_PRE_LAYOUT = 4096;
        public static final int FLAG_CHANGED = 2;
        public static final int FLAG_INVALIDATED = 4;
        public static final int FLAG_MOVED = 2048;
        public static final int FLAG_REMOVED = 8;
        private b mListener = null;
        private ArrayList<a> mFinishedListeners = new ArrayList<>();
        private long mAddDuration = 120;
        private long mRemoveDuration = 120;
        private long mMoveDuration = 250;
        private long mChangeDuration = 250;

        /* loaded from: classes.dex */
        public interface a {
            void a();
        }

        /* loaded from: classes.dex */
        public interface b {
            void a(a0 a0Var);
        }

        /* loaded from: classes.dex */
        public static class c {
            public int a;
            public int b;

            public c a(a0 a0Var) {
                return b(a0Var, 0);
            }

            public c b(a0 a0Var, int i) {
                View view = a0Var.itemView;
                this.a = view.getLeft();
                this.b = view.getTop();
                view.getRight();
                view.getBottom();
                return this;
            }
        }

        public static int buildAdapterChangeFlagsForAnimations(a0 a0Var) {
            int i = a0Var.mFlags & 14;
            if (a0Var.isInvalid()) {
                return 4;
            }
            if ((i & 4) == 0) {
                int oldPosition = a0Var.getOldPosition();
                int absoluteAdapterPosition = a0Var.getAbsoluteAdapterPosition();
                return (oldPosition == -1 || absoluteAdapterPosition == -1 || oldPosition == absoluteAdapterPosition) ? i : i | 2048;
            }
            return i;
        }

        public abstract boolean animateAppearance(a0 a0Var, c cVar, c cVar2);

        public abstract boolean animateChange(a0 a0Var, a0 a0Var2, c cVar, c cVar2);

        public abstract boolean animateDisappearance(a0 a0Var, c cVar, c cVar2);

        public abstract boolean animatePersistence(a0 a0Var, c cVar, c cVar2);

        public boolean canReuseUpdatedViewHolder(a0 a0Var) {
            return true;
        }

        public boolean canReuseUpdatedViewHolder(a0 a0Var, List<Object> list) {
            return canReuseUpdatedViewHolder(a0Var);
        }

        public final void dispatchAnimationFinished(a0 a0Var) {
            onAnimationFinished(a0Var);
            b bVar = this.mListener;
            if (bVar != null) {
                bVar.a(a0Var);
            }
        }

        public final void dispatchAnimationStarted(a0 a0Var) {
            onAnimationStarted(a0Var);
        }

        public final void dispatchAnimationsFinished() {
            int size = this.mFinishedListeners.size();
            for (int i = 0; i < size; i++) {
                this.mFinishedListeners.get(i).a();
            }
            this.mFinishedListeners.clear();
        }

        public abstract void endAnimation(a0 a0Var);

        public abstract void endAnimations();

        public long getAddDuration() {
            return this.mAddDuration;
        }

        public long getChangeDuration() {
            return this.mChangeDuration;
        }

        public long getMoveDuration() {
            return this.mMoveDuration;
        }

        public long getRemoveDuration() {
            return this.mRemoveDuration;
        }

        public abstract boolean isRunning();

        public final boolean isRunning(a aVar) {
            boolean isRunning = isRunning();
            if (aVar != null) {
                if (!isRunning) {
                    aVar.a();
                } else {
                    this.mFinishedListeners.add(aVar);
                }
            }
            return isRunning;
        }

        public c obtainHolderInfo() {
            return new c();
        }

        public void onAnimationFinished(a0 a0Var) {
        }

        public void onAnimationStarted(a0 a0Var) {
        }

        public c recordPostLayoutInformation(x xVar, a0 a0Var) {
            return obtainHolderInfo().a(a0Var);
        }

        public c recordPreLayoutInformation(x xVar, a0 a0Var, int i, List<Object> list) {
            return obtainHolderInfo().a(a0Var);
        }

        public abstract void runPendingAnimations();

        public void setAddDuration(long j) {
            this.mAddDuration = j;
        }

        public void setChangeDuration(long j) {
            this.mChangeDuration = j;
        }

        public void setListener(b bVar) {
            this.mListener = bVar;
        }

        public void setMoveDuration(long j) {
            this.mMoveDuration = j;
        }

        public void setRemoveDuration(long j) {
            this.mRemoveDuration = j;
        }
    }

    /* loaded from: classes.dex */
    public class m implements l.b {
        public m() {
        }

        @Override // androidx.recyclerview.widget.RecyclerView.l.b
        public void a(a0 a0Var) {
            a0Var.setIsRecyclable(true);
            if (a0Var.mShadowedHolder != null && a0Var.mShadowingHolder == null) {
                a0Var.mShadowedHolder = null;
            }
            a0Var.mShadowingHolder = null;
            if (a0Var.shouldBeKeptAsChild() || RecyclerView.this.a1(a0Var.itemView) || !a0Var.isTmpDetached()) {
                return;
            }
            RecyclerView.this.removeDetachedView(a0Var.itemView, false);
        }
    }

    /* loaded from: classes.dex */
    public static abstract class n {
        @Deprecated
        public void getItemOffsets(Rect rect, int i, RecyclerView recyclerView) {
            rect.set(0, 0, 0, 0);
        }

        @Deprecated
        public void onDraw(Canvas canvas, RecyclerView recyclerView) {
        }

        public void onDraw(Canvas canvas, RecyclerView recyclerView, x xVar) {
            onDraw(canvas, recyclerView);
        }

        @Deprecated
        public void onDrawOver(Canvas canvas, RecyclerView recyclerView) {
        }

        public void onDrawOver(Canvas canvas, RecyclerView recyclerView, x xVar) {
            onDrawOver(canvas, recyclerView);
        }

        public void getItemOffsets(Rect rect, View view, RecyclerView recyclerView, x xVar) {
            getItemOffsets(rect, ((LayoutParams) view.getLayoutParams()).a(), recyclerView);
        }
    }

    /* loaded from: classes.dex */
    public interface o {
        void b(View view);

        void d(View view);
    }

    /* loaded from: classes.dex */
    public static abstract class p {
        public abstract boolean a(int i, int i2);
    }

    /* loaded from: classes.dex */
    public interface q {
        void a(RecyclerView recyclerView, MotionEvent motionEvent);

        boolean c(RecyclerView recyclerView, MotionEvent motionEvent);

        void e(boolean z);
    }

    /* loaded from: classes.dex */
    public static abstract class r {
        public void onScrollStateChanged(RecyclerView recyclerView, int i) {
        }

        public void onScrolled(RecyclerView recyclerView, int i, int i2) {
        }
    }

    /* loaded from: classes.dex */
    public static class s {
        public SparseArray<a> a = new SparseArray<>();
        public int b = 0;

        /* loaded from: classes.dex */
        public static class a {
            public final ArrayList<a0> a = new ArrayList<>();
            public int b = 5;
            public long c = 0;
            public long d = 0;
        }

        public void a() {
            this.b++;
        }

        public void b() {
            for (int i = 0; i < this.a.size(); i++) {
                this.a.valueAt(i).a.clear();
            }
        }

        public void c() {
            this.b--;
        }

        public void d(int i, long j) {
            a g = g(i);
            g.d = j(g.d, j);
        }

        public void e(int i, long j) {
            a g = g(i);
            g.c = j(g.c, j);
        }

        public a0 f(int i) {
            a aVar = this.a.get(i);
            if (aVar == null || aVar.a.isEmpty()) {
                return null;
            }
            ArrayList<a0> arrayList = aVar.a;
            for (int size = arrayList.size() - 1; size >= 0; size--) {
                if (!arrayList.get(size).isAttachedToTransitionOverlay()) {
                    return arrayList.remove(size);
                }
            }
            return null;
        }

        public final a g(int i) {
            a aVar = this.a.get(i);
            if (aVar == null) {
                a aVar2 = new a();
                this.a.put(i, aVar2);
                return aVar2;
            }
            return aVar;
        }

        public void h(Adapter adapter, Adapter adapter2, boolean z) {
            if (adapter != null) {
                c();
            }
            if (!z && this.b == 0) {
                b();
            }
            if (adapter2 != null) {
                a();
            }
        }

        public void i(a0 a0Var) {
            int itemViewType = a0Var.getItemViewType();
            ArrayList<a0> arrayList = g(itemViewType).a;
            if (this.a.get(itemViewType).b <= arrayList.size()) {
                return;
            }
            a0Var.resetInternal();
            arrayList.add(a0Var);
        }

        public long j(long j, long j2) {
            return j == 0 ? j2 : ((j / 4) * 3) + (j2 / 4);
        }

        public void k(int i, int i2) {
            a g = g(i);
            g.b = i2;
            ArrayList<a0> arrayList = g.a;
            while (arrayList.size() > i2) {
                arrayList.remove(arrayList.size() - 1);
            }
        }

        public boolean l(int i, long j, long j2) {
            long j3 = g(i).d;
            return j3 == 0 || j + j3 < j2;
        }

        public boolean m(int i, long j, long j2) {
            long j3 = g(i).c;
            return j3 == 0 || j + j3 < j2;
        }
    }

    /* loaded from: classes.dex */
    public final class t {
        public final ArrayList<a0> a;
        public ArrayList<a0> b;
        public final ArrayList<a0> c;
        public final List<a0> d;
        public int e;
        public int f;
        public s g;

        public t() {
            ArrayList<a0> arrayList = new ArrayList<>();
            this.a = arrayList;
            this.b = null;
            this.c = new ArrayList<>();
            this.d = Collections.unmodifiableList(arrayList);
            this.e = 2;
            this.f = 2;
        }

        public void A(int i) {
            a(this.c.get(i), true);
            this.c.remove(i);
        }

        public void B(View view) {
            a0 i0 = RecyclerView.i0(view);
            if (i0.isTmpDetached()) {
                RecyclerView.this.removeDetachedView(view, false);
            }
            if (i0.isScrap()) {
                i0.unScrap();
            } else if (i0.wasReturnedFromScrap()) {
                i0.clearReturnedFromScrapFlag();
            }
            C(i0);
            if (RecyclerView.this.R0 == null || i0.isRecyclable()) {
                return;
            }
            RecyclerView.this.R0.endAnimation(i0);
        }

        public void C(a0 a0Var) {
            boolean z;
            boolean z2 = true;
            if (!a0Var.isScrap() && a0Var.itemView.getParent() == null) {
                if (!a0Var.isTmpDetached()) {
                    if (!a0Var.shouldIgnore()) {
                        boolean doesTransientStatePreventRecycling = a0Var.doesTransientStatePreventRecycling();
                        Adapter adapter = RecyclerView.this.p0;
                        if ((adapter != null && doesTransientStatePreventRecycling && adapter.onFailedToRecycleView(a0Var)) || a0Var.isRecyclable()) {
                            if (this.f <= 0 || a0Var.hasAnyOfTheFlags(526)) {
                                z = false;
                            } else {
                                int size = this.c.size();
                                if (size >= this.f && size > 0) {
                                    A(0);
                                    size--;
                                }
                                if (RecyclerView.H1 && size > 0 && !RecyclerView.this.i1.d(a0Var.mPosition)) {
                                    int i = size - 1;
                                    while (i >= 0) {
                                        if (!RecyclerView.this.i1.d(this.c.get(i).mPosition)) {
                                            break;
                                        }
                                        i--;
                                    }
                                    size = i + 1;
                                }
                                this.c.add(size, a0Var);
                                z = true;
                            }
                            if (z) {
                                z2 = false;
                            } else {
                                a(a0Var, true);
                            }
                            r1 = z;
                        } else {
                            z2 = false;
                        }
                        RecyclerView.this.j0.q(a0Var);
                        if (r1 || z2 || !doesTransientStatePreventRecycling) {
                            return;
                        }
                        a0Var.mBindingAdapter = null;
                        a0Var.mOwnerRecyclerView = null;
                        return;
                    }
                    throw new IllegalArgumentException("Trying to recycle an ignored view holder. You should first call stopIgnoringView(view) before calling recycle." + RecyclerView.this.Q());
                }
                throw new IllegalArgumentException("Tmp detached view should be removed from RecyclerView before it can be recycled: " + a0Var + RecyclerView.this.Q());
            }
            StringBuilder sb = new StringBuilder();
            sb.append("Scrapped or attached views may not be recycled. isScrap:");
            sb.append(a0Var.isScrap());
            sb.append(" isAttached:");
            sb.append(a0Var.itemView.getParent() != null);
            sb.append(RecyclerView.this.Q());
            throw new IllegalArgumentException(sb.toString());
        }

        public void D(View view) {
            a0 i0 = RecyclerView.i0(view);
            if (!i0.hasAnyOfTheFlags(12) && i0.isUpdated() && !RecyclerView.this.q(i0)) {
                if (this.b == null) {
                    this.b = new ArrayList<>();
                }
                i0.setScrapContainer(this, true);
                this.b.add(i0);
            } else if (i0.isInvalid() && !i0.isRemoved() && !RecyclerView.this.p0.hasStableIds()) {
                throw new IllegalArgumentException("Called scrap view with an invalid view. Invalid views cannot be reused from scrap, they should rebound from recycler pool." + RecyclerView.this.Q());
            } else {
                i0.setScrapContainer(this, false);
                this.a.add(i0);
            }
        }

        public void E(s sVar) {
            s sVar2 = this.g;
            if (sVar2 != null) {
                sVar2.c();
            }
            this.g = sVar;
            if (sVar == null || RecyclerView.this.getAdapter() == null) {
                return;
            }
            this.g.a();
        }

        public void F(y yVar) {
        }

        public void G(int i) {
            this.e = i;
            K();
        }

        public final boolean H(a0 a0Var, int i, int i2, long j) {
            a0Var.mBindingAdapter = null;
            a0Var.mOwnerRecyclerView = RecyclerView.this;
            int itemViewType = a0Var.getItemViewType();
            long nanoTime = RecyclerView.this.getNanoTime();
            if (j == Long.MAX_VALUE || this.g.l(itemViewType, nanoTime, j)) {
                RecyclerView.this.p0.bindViewHolder(a0Var, i);
                this.g.d(a0Var.getItemViewType(), RecyclerView.this.getNanoTime() - nanoTime);
                b(a0Var);
                if (RecyclerView.this.j1.e()) {
                    a0Var.mPreLayoutPosition = i2;
                    return true;
                }
                return true;
            }
            return false;
        }

        /* JADX WARN: Removed duplicated region for block: B:18:0x0037  */
        /* JADX WARN: Removed duplicated region for block: B:27:0x005c  */
        /* JADX WARN: Removed duplicated region for block: B:29:0x005f  */
        /* JADX WARN: Removed duplicated region for block: B:62:0x0130  */
        /* JADX WARN: Removed duplicated region for block: B:68:0x014d  */
        /* JADX WARN: Removed duplicated region for block: B:71:0x0170  */
        /* JADX WARN: Removed duplicated region for block: B:76:0x017f  */
        /* JADX WARN: Removed duplicated region for block: B:85:0x01a9  */
        /* JADX WARN: Removed duplicated region for block: B:86:0x01b7  */
        /* JADX WARN: Removed duplicated region for block: B:92:0x01d3 A[ADDED_TO_REGION] */
        /*
            Code decompiled incorrectly, please refer to instructions dump.
            To view partially-correct code enable 'Show inconsistent code' option in preferences
        */
        public androidx.recyclerview.widget.RecyclerView.a0 I(int r17, boolean r18, long r19) {
            /*
                Method dump skipped, instructions count: 530
                To view this dump change 'Code comments level' option to 'DEBUG'
            */
            throw new UnsupportedOperationException("Method not decompiled: androidx.recyclerview.widget.RecyclerView.t.I(int, boolean, long):androidx.recyclerview.widget.RecyclerView$a0");
        }

        public void J(a0 a0Var) {
            if (a0Var.mInChangeScrap) {
                this.b.remove(a0Var);
            } else {
                this.a.remove(a0Var);
            }
            a0Var.mScrapContainer = null;
            a0Var.mInChangeScrap = false;
            a0Var.clearReturnedFromScrapFlag();
        }

        public void K() {
            LayoutManager layoutManager = RecyclerView.this.q0;
            this.f = this.e + (layoutManager != null ? layoutManager.q0 : 0);
            for (int size = this.c.size() - 1; size >= 0 && this.c.size() > this.f; size--) {
                A(size);
            }
        }

        public boolean L(a0 a0Var) {
            if (a0Var.isRemoved()) {
                return RecyclerView.this.j1.e();
            }
            int i = a0Var.mPosition;
            if (i >= 0 && i < RecyclerView.this.p0.getItemCount()) {
                if (RecyclerView.this.j1.e() || RecyclerView.this.p0.getItemViewType(a0Var.mPosition) == a0Var.getItemViewType()) {
                    return !RecyclerView.this.p0.hasStableIds() || a0Var.getItemId() == RecyclerView.this.p0.getItemId(a0Var.mPosition);
                }
                return false;
            }
            throw new IndexOutOfBoundsException("Inconsistency detected. Invalid view holder adapter position" + a0Var + RecyclerView.this.Q());
        }

        public void M(int i, int i2) {
            int i3;
            int i4 = i2 + i;
            for (int size = this.c.size() - 1; size >= 0; size--) {
                a0 a0Var = this.c.get(size);
                if (a0Var != null && (i3 = a0Var.mPosition) >= i && i3 < i4) {
                    a0Var.addFlags(2);
                    A(size);
                }
            }
        }

        public void a(a0 a0Var, boolean z) {
            RecyclerView.s(a0Var);
            View view = a0Var.itemView;
            androidx.recyclerview.widget.s sVar = RecyclerView.this.q1;
            if (sVar != null) {
                z5 n = sVar.n();
                ei4.t0(view, n instanceof s.a ? ((s.a) n).n(view) : null);
            }
            if (z) {
                g(a0Var);
            }
            a0Var.mBindingAdapter = null;
            a0Var.mOwnerRecyclerView = null;
            i().i(a0Var);
        }

        public final void b(a0 a0Var) {
            if (RecyclerView.this.x0()) {
                View view = a0Var.itemView;
                if (ei4.C(view) == 0) {
                    ei4.D0(view, 1);
                }
                androidx.recyclerview.widget.s sVar = RecyclerView.this.q1;
                if (sVar == null) {
                    return;
                }
                z5 n = sVar.n();
                if (n instanceof s.a) {
                    ((s.a) n).o(view);
                }
                ei4.t0(view, n);
            }
        }

        public void c() {
            this.a.clear();
            z();
        }

        public void d() {
            int size = this.c.size();
            for (int i = 0; i < size; i++) {
                this.c.get(i).clearOldPosition();
            }
            int size2 = this.a.size();
            for (int i2 = 0; i2 < size2; i2++) {
                this.a.get(i2).clearOldPosition();
            }
            ArrayList<a0> arrayList = this.b;
            if (arrayList != null) {
                int size3 = arrayList.size();
                for (int i3 = 0; i3 < size3; i3++) {
                    this.b.get(i3).clearOldPosition();
                }
            }
        }

        public void e() {
            this.a.clear();
            ArrayList<a0> arrayList = this.b;
            if (arrayList != null) {
                arrayList.clear();
            }
        }

        public int f(int i) {
            if (i >= 0 && i < RecyclerView.this.j1.b()) {
                return !RecyclerView.this.j1.e() ? i : RecyclerView.this.h0.m(i);
            }
            throw new IndexOutOfBoundsException("invalid position " + i + ". State item count is " + RecyclerView.this.j1.b() + RecyclerView.this.Q());
        }

        public void g(a0 a0Var) {
            u uVar = RecyclerView.this.r0;
            if (uVar != null) {
                uVar.a(a0Var);
            }
            int size = RecyclerView.this.s0.size();
            for (int i = 0; i < size; i++) {
                RecyclerView.this.s0.get(i).a(a0Var);
            }
            Adapter adapter = RecyclerView.this.p0;
            if (adapter != null) {
                adapter.onViewRecycled(a0Var);
            }
            RecyclerView recyclerView = RecyclerView.this;
            if (recyclerView.j1 != null) {
                recyclerView.j0.q(a0Var);
            }
        }

        public a0 h(int i) {
            int size;
            int m;
            ArrayList<a0> arrayList = this.b;
            if (arrayList != null && (size = arrayList.size()) != 0) {
                for (int i2 = 0; i2 < size; i2++) {
                    a0 a0Var = this.b.get(i2);
                    if (!a0Var.wasReturnedFromScrap() && a0Var.getLayoutPosition() == i) {
                        a0Var.addFlags(32);
                        return a0Var;
                    }
                }
                if (RecyclerView.this.p0.hasStableIds() && (m = RecyclerView.this.h0.m(i)) > 0 && m < RecyclerView.this.p0.getItemCount()) {
                    long itemId = RecyclerView.this.p0.getItemId(m);
                    for (int i3 = 0; i3 < size; i3++) {
                        a0 a0Var2 = this.b.get(i3);
                        if (!a0Var2.wasReturnedFromScrap() && a0Var2.getItemId() == itemId) {
                            a0Var2.addFlags(32);
                            return a0Var2;
                        }
                    }
                }
            }
            return null;
        }

        public s i() {
            if (this.g == null) {
                this.g = new s();
            }
            return this.g;
        }

        public int j() {
            return this.a.size();
        }

        public List<a0> k() {
            return this.d;
        }

        public a0 l(long j, int i, boolean z) {
            for (int size = this.a.size() - 1; size >= 0; size--) {
                a0 a0Var = this.a.get(size);
                if (a0Var.getItemId() == j && !a0Var.wasReturnedFromScrap()) {
                    if (i == a0Var.getItemViewType()) {
                        a0Var.addFlags(32);
                        if (a0Var.isRemoved() && !RecyclerView.this.j1.e()) {
                            a0Var.setFlags(2, 14);
                        }
                        return a0Var;
                    } else if (!z) {
                        this.a.remove(size);
                        RecyclerView.this.removeDetachedView(a0Var.itemView, false);
                        y(a0Var.itemView);
                    }
                }
            }
            int size2 = this.c.size();
            while (true) {
                size2--;
                if (size2 < 0) {
                    return null;
                }
                a0 a0Var2 = this.c.get(size2);
                if (a0Var2.getItemId() == j && !a0Var2.isAttachedToTransitionOverlay()) {
                    if (i == a0Var2.getItemViewType()) {
                        if (!z) {
                            this.c.remove(size2);
                        }
                        return a0Var2;
                    } else if (!z) {
                        A(size2);
                        return null;
                    }
                }
            }
        }

        public a0 m(int i, boolean z) {
            View e;
            int size = this.a.size();
            for (int i2 = 0; i2 < size; i2++) {
                a0 a0Var = this.a.get(i2);
                if (!a0Var.wasReturnedFromScrap() && a0Var.getLayoutPosition() == i && !a0Var.isInvalid() && (RecyclerView.this.j1.h || !a0Var.isRemoved())) {
                    a0Var.addFlags(32);
                    return a0Var;
                }
            }
            if (!z && (e = RecyclerView.this.i0.e(i)) != null) {
                a0 i0 = RecyclerView.i0(e);
                RecyclerView.this.i0.s(e);
                int m = RecyclerView.this.i0.m(e);
                if (m != -1) {
                    RecyclerView.this.i0.d(m);
                    D(e);
                    i0.addFlags(8224);
                    return i0;
                }
                throw new IllegalStateException("layout index should not be -1 after unhiding a view:" + i0 + RecyclerView.this.Q());
            }
            int size2 = this.c.size();
            for (int i3 = 0; i3 < size2; i3++) {
                a0 a0Var2 = this.c.get(i3);
                if (!a0Var2.isInvalid() && a0Var2.getLayoutPosition() == i && !a0Var2.isAttachedToTransitionOverlay()) {
                    if (!z) {
                        this.c.remove(i3);
                    }
                    return a0Var2;
                }
            }
            return null;
        }

        public View n(int i) {
            return this.a.get(i).itemView;
        }

        public View o(int i) {
            return p(i, false);
        }

        public View p(int i, boolean z) {
            return I(i, z, Long.MAX_VALUE).itemView;
        }

        public final void q(ViewGroup viewGroup, boolean z) {
            for (int childCount = viewGroup.getChildCount() - 1; childCount >= 0; childCount--) {
                View childAt = viewGroup.getChildAt(childCount);
                if (childAt instanceof ViewGroup) {
                    q((ViewGroup) childAt, true);
                }
            }
            if (z) {
                if (viewGroup.getVisibility() == 4) {
                    viewGroup.setVisibility(0);
                    viewGroup.setVisibility(4);
                    return;
                }
                int visibility = viewGroup.getVisibility();
                viewGroup.setVisibility(4);
                viewGroup.setVisibility(visibility);
            }
        }

        public final void r(a0 a0Var) {
            View view = a0Var.itemView;
            if (view instanceof ViewGroup) {
                q((ViewGroup) view, false);
            }
        }

        public void s() {
            int size = this.c.size();
            for (int i = 0; i < size; i++) {
                LayoutParams layoutParams = (LayoutParams) this.c.get(i).itemView.getLayoutParams();
                if (layoutParams != null) {
                    layoutParams.g0 = true;
                }
            }
        }

        public void t() {
            int size = this.c.size();
            for (int i = 0; i < size; i++) {
                a0 a0Var = this.c.get(i);
                if (a0Var != null) {
                    a0Var.addFlags(6);
                    a0Var.addChangePayload(null);
                }
            }
            Adapter adapter = RecyclerView.this.p0;
            if (adapter == null || !adapter.hasStableIds()) {
                z();
            }
        }

        public void u(int i, int i2) {
            int size = this.c.size();
            for (int i3 = 0; i3 < size; i3++) {
                a0 a0Var = this.c.get(i3);
                if (a0Var != null && a0Var.mPosition >= i) {
                    a0Var.offsetPosition(i2, false);
                }
            }
        }

        public void v(int i, int i2) {
            int i3;
            int i4;
            int i5;
            int i6;
            if (i < i2) {
                i3 = -1;
                i5 = i;
                i4 = i2;
            } else {
                i3 = 1;
                i4 = i;
                i5 = i2;
            }
            int size = this.c.size();
            for (int i7 = 0; i7 < size; i7++) {
                a0 a0Var = this.c.get(i7);
                if (a0Var != null && (i6 = a0Var.mPosition) >= i5 && i6 <= i4) {
                    if (i6 == i) {
                        a0Var.offsetPosition(i2 - i, false);
                    } else {
                        a0Var.offsetPosition(i3, false);
                    }
                }
            }
        }

        public void w(int i, int i2, boolean z) {
            int i3 = i + i2;
            for (int size = this.c.size() - 1; size >= 0; size--) {
                a0 a0Var = this.c.get(size);
                if (a0Var != null) {
                    int i4 = a0Var.mPosition;
                    if (i4 >= i3) {
                        a0Var.offsetPosition(-i2, z);
                    } else if (i4 >= i) {
                        a0Var.addFlags(8);
                        A(size);
                    }
                }
            }
        }

        public void x(Adapter adapter, Adapter adapter2, boolean z) {
            c();
            i().h(adapter, adapter2, z);
        }

        public void y(View view) {
            a0 i0 = RecyclerView.i0(view);
            i0.mScrapContainer = null;
            i0.mInChangeScrap = false;
            i0.clearReturnedFromScrapFlag();
            C(i0);
        }

        public void z() {
            for (int size = this.c.size() - 1; size >= 0; size--) {
                A(size);
            }
            this.c.clear();
            if (RecyclerView.H1) {
                RecyclerView.this.i1.b();
            }
        }
    }

    /* loaded from: classes.dex */
    public interface u {
        void a(a0 a0Var);
    }

    /* loaded from: classes.dex */
    public class v extends i {
        public v() {
        }

        public void a() {
            if (RecyclerView.G1) {
                RecyclerView recyclerView = RecyclerView.this;
                if (recyclerView.x0 && recyclerView.w0) {
                    ei4.l0(recyclerView, recyclerView.l0);
                    return;
                }
            }
            RecyclerView recyclerView2 = RecyclerView.this;
            recyclerView2.F0 = true;
            recyclerView2.requestLayout();
        }

        @Override // androidx.recyclerview.widget.RecyclerView.i
        public void onChanged() {
            RecyclerView.this.p(null);
            RecyclerView recyclerView = RecyclerView.this;
            recyclerView.j1.g = true;
            recyclerView.U0(true);
            if (RecyclerView.this.h0.p()) {
                return;
            }
            RecyclerView.this.requestLayout();
        }

        @Override // androidx.recyclerview.widget.RecyclerView.i
        public void onItemRangeChanged(int i, int i2, Object obj) {
            RecyclerView.this.p(null);
            if (RecyclerView.this.h0.r(i, i2, obj)) {
                a();
            }
        }

        @Override // androidx.recyclerview.widget.RecyclerView.i
        public void onItemRangeInserted(int i, int i2) {
            RecyclerView.this.p(null);
            if (RecyclerView.this.h0.s(i, i2)) {
                a();
            }
        }

        @Override // androidx.recyclerview.widget.RecyclerView.i
        public void onItemRangeMoved(int i, int i2, int i3) {
            RecyclerView.this.p(null);
            if (RecyclerView.this.h0.t(i, i2, i3)) {
                a();
            }
        }

        @Override // androidx.recyclerview.widget.RecyclerView.i
        public void onItemRangeRemoved(int i, int i2) {
            RecyclerView.this.p(null);
            if (RecyclerView.this.h0.u(i, i2)) {
                a();
            }
        }

        @Override // androidx.recyclerview.widget.RecyclerView.i
        public void onStateRestorationPolicyChanged() {
            Adapter adapter;
            RecyclerView recyclerView = RecyclerView.this;
            if (recyclerView.g0 == null || (adapter = recyclerView.p0) == null || !adapter.canRestoreState()) {
                return;
            }
            RecyclerView.this.requestLayout();
        }
    }

    /* loaded from: classes.dex */
    public static abstract class w {
        private LayoutManager mLayoutManager;
        private boolean mPendingInitialRun;
        private RecyclerView mRecyclerView;
        private boolean mRunning;
        private boolean mStarted;
        private View mTargetView;
        private int mTargetPosition = -1;
        private final a mRecyclingAction = new a(0, 0);

        /* loaded from: classes.dex */
        public static class a {
            public int a;
            public int b;
            public int c;
            public int d;
            public Interpolator e;
            public boolean f;
            public int g;

            public a(int i, int i2) {
                this(i, i2, Integer.MIN_VALUE, null);
            }

            public boolean a() {
                return this.d >= 0;
            }

            public void b(int i) {
                this.d = i;
            }

            public void c(RecyclerView recyclerView) {
                int i = this.d;
                if (i >= 0) {
                    this.d = -1;
                    recyclerView.A0(i);
                    this.f = false;
                } else if (this.f) {
                    e();
                    recyclerView.g1.e(this.a, this.b, this.c, this.e);
                    this.g++;
                    this.f = false;
                } else {
                    this.g = 0;
                }
            }

            public void d(int i, int i2, int i3, Interpolator interpolator) {
                this.a = i;
                this.b = i2;
                this.c = i3;
                this.e = interpolator;
                this.f = true;
            }

            public final void e() {
                if (this.e != null && this.c < 1) {
                    throw new IllegalStateException("If you provide an interpolator, you must set a positive duration");
                }
                if (this.c < 1) {
                    throw new IllegalStateException("Scroll duration must be a positive number");
                }
            }

            public a(int i, int i2, int i3, Interpolator interpolator) {
                this.d = -1;
                this.f = false;
                this.g = 0;
                this.a = i;
                this.b = i2;
                this.c = i3;
                this.e = interpolator;
            }
        }

        /* loaded from: classes.dex */
        public interface b {
            PointF a(int i);
        }

        public PointF computeScrollVectorForPosition(int i) {
            LayoutManager layoutManager = getLayoutManager();
            if (layoutManager instanceof b) {
                return ((b) layoutManager).a(i);
            }
            StringBuilder sb = new StringBuilder();
            sb.append("You should override computeScrollVectorForPosition when the LayoutManager does not implement ");
            sb.append(b.class.getCanonicalName());
            return null;
        }

        public View findViewByPosition(int i) {
            return this.mRecyclerView.q0.N(i);
        }

        public int getChildCount() {
            return this.mRecyclerView.q0.U();
        }

        public int getChildPosition(View view) {
            return this.mRecyclerView.g0(view);
        }

        public LayoutManager getLayoutManager() {
            return this.mLayoutManager;
        }

        public int getTargetPosition() {
            return this.mTargetPosition;
        }

        @Deprecated
        public void instantScrollToPosition(int i) {
            this.mRecyclerView.o1(i);
        }

        public boolean isPendingInitialRun() {
            return this.mPendingInitialRun;
        }

        public boolean isRunning() {
            return this.mRunning;
        }

        public void normalize(PointF pointF) {
            float f = pointF.x;
            float f2 = pointF.y;
            float sqrt = (float) Math.sqrt((f * f) + (f2 * f2));
            pointF.x /= sqrt;
            pointF.y /= sqrt;
        }

        public void onAnimation(int i, int i2) {
            PointF computeScrollVectorForPosition;
            RecyclerView recyclerView = this.mRecyclerView;
            if (this.mTargetPosition == -1 || recyclerView == null) {
                stop();
            }
            if (this.mPendingInitialRun && this.mTargetView == null && this.mLayoutManager != null && (computeScrollVectorForPosition = computeScrollVectorForPosition(this.mTargetPosition)) != null) {
                float f = computeScrollVectorForPosition.x;
                if (f != Utils.FLOAT_EPSILON || computeScrollVectorForPosition.y != Utils.FLOAT_EPSILON) {
                    recyclerView.n1((int) Math.signum(f), (int) Math.signum(computeScrollVectorForPosition.y), null);
                }
            }
            this.mPendingInitialRun = false;
            View view = this.mTargetView;
            if (view != null) {
                if (getChildPosition(view) == this.mTargetPosition) {
                    onTargetFound(this.mTargetView, recyclerView.j1, this.mRecyclingAction);
                    this.mRecyclingAction.c(recyclerView);
                    stop();
                } else {
                    this.mTargetView = null;
                }
            }
            if (this.mRunning) {
                onSeekTargetStep(i, i2, recyclerView.j1, this.mRecyclingAction);
                boolean a2 = this.mRecyclingAction.a();
                this.mRecyclingAction.c(recyclerView);
                if (a2 && this.mRunning) {
                    this.mPendingInitialRun = true;
                    recyclerView.g1.d();
                }
            }
        }

        public void onChildAttachedToWindow(View view) {
            if (getChildPosition(view) == getTargetPosition()) {
                this.mTargetView = view;
            }
        }

        public abstract void onSeekTargetStep(int i, int i2, x xVar, a aVar);

        public abstract void onStart();

        public abstract void onStop();

        public abstract void onTargetFound(View view, x xVar, a aVar);

        public void setTargetPosition(int i) {
            this.mTargetPosition = i;
        }

        public void start(RecyclerView recyclerView, LayoutManager layoutManager) {
            recyclerView.g1.f();
            if (this.mStarted) {
                StringBuilder sb = new StringBuilder();
                sb.append("An instance of ");
                sb.append(getClass().getSimpleName());
                sb.append(" was started more than once. Each instance of");
                sb.append(getClass().getSimpleName());
                sb.append(" is intended to only be used once. You should create a new instance for each use.");
            }
            this.mRecyclerView = recyclerView;
            this.mLayoutManager = layoutManager;
            int i = this.mTargetPosition;
            if (i != -1) {
                recyclerView.j1.a = i;
                this.mRunning = true;
                this.mPendingInitialRun = true;
                this.mTargetView = findViewByPosition(getTargetPosition());
                onStart();
                this.mRecyclerView.g1.d();
                this.mStarted = true;
                return;
            }
            throw new IllegalArgumentException("Invalid target position");
        }

        public final void stop() {
            if (this.mRunning) {
                this.mRunning = false;
                onStop();
                this.mRecyclerView.j1.a = -1;
                this.mTargetView = null;
                this.mTargetPosition = -1;
                this.mPendingInitialRun = false;
                this.mLayoutManager.n1(this);
                this.mLayoutManager = null;
                this.mRecyclerView = null;
            }
        }
    }

    /* loaded from: classes.dex */
    public static class x {
        public SparseArray<Object> b;
        public int m;
        public long n;
        public int o;
        public int p;
        public int a = -1;
        public int c = 0;
        public int d = 0;
        public int e = 1;
        public int f = 0;
        public boolean g = false;
        public boolean h = false;
        public boolean i = false;
        public boolean j = false;
        public boolean k = false;
        public boolean l = false;

        public void a(int i) {
            if ((this.e & i) != 0) {
                return;
            }
            throw new IllegalStateException("Layout state should be one of " + Integer.toBinaryString(i) + " but it is " + Integer.toBinaryString(this.e));
        }

        public int b() {
            if (this.h) {
                return this.c - this.d;
            }
            return this.f;
        }

        public int c() {
            return this.a;
        }

        public boolean d() {
            return this.a != -1;
        }

        public boolean e() {
            return this.h;
        }

        public void f(Adapter adapter) {
            this.e = 1;
            this.f = adapter.getItemCount();
            this.h = false;
            this.i = false;
            this.j = false;
        }

        public boolean g() {
            return this.l;
        }

        public String toString() {
            return "State{mTargetPosition=" + this.a + ", mData=" + this.b + ", mItemCount=" + this.f + ", mIsMeasuring=" + this.j + ", mPreviousLayoutItemCount=" + this.c + ", mDeletedInvisibleItemCountSincePreviousLayout=" + this.d + ", mStructureChanged=" + this.g + ", mInPreLayout=" + this.h + ", mRunSimpleAnimations=" + this.k + ", mRunPredictiveAnimations=" + this.l + '}';
        }
    }

    /* loaded from: classes.dex */
    public static abstract class y {
    }

    /* loaded from: classes.dex */
    public class z implements Runnable {
        public int a;
        public int f0;
        public OverScroller g0;
        public Interpolator h0;
        public boolean i0;
        public boolean j0;

        public z() {
            Interpolator interpolator = RecyclerView.L1;
            this.h0 = interpolator;
            this.i0 = false;
            this.j0 = false;
            this.g0 = new OverScroller(RecyclerView.this.getContext(), interpolator);
        }

        public final int a(int i, int i2) {
            int abs = Math.abs(i);
            int abs2 = Math.abs(i2);
            boolean z = abs > abs2;
            RecyclerView recyclerView = RecyclerView.this;
            int width = z ? recyclerView.getWidth() : recyclerView.getHeight();
            if (!z) {
                abs = abs2;
            }
            return Math.min((int) (((abs / width) + 1.0f) * 300.0f), (int) PlaybackException.ERROR_CODE_IO_UNSPECIFIED);
        }

        public void b(int i, int i2) {
            RecyclerView.this.setScrollState(2);
            this.f0 = 0;
            this.a = 0;
            Interpolator interpolator = this.h0;
            Interpolator interpolator2 = RecyclerView.L1;
            if (interpolator != interpolator2) {
                this.h0 = interpolator2;
                this.g0 = new OverScroller(RecyclerView.this.getContext(), interpolator2);
            }
            this.g0.fling(0, 0, i, i2, Integer.MIN_VALUE, Integer.MAX_VALUE, Integer.MIN_VALUE, Integer.MAX_VALUE);
            d();
        }

        public final void c() {
            RecyclerView.this.removeCallbacks(this);
            ei4.l0(RecyclerView.this, this);
        }

        public void d() {
            if (this.i0) {
                this.j0 = true;
            } else {
                c();
            }
        }

        public void e(int i, int i2, int i3, Interpolator interpolator) {
            if (i3 == Integer.MIN_VALUE) {
                i3 = a(i, i2);
            }
            int i4 = i3;
            if (interpolator == null) {
                interpolator = RecyclerView.L1;
            }
            if (this.h0 != interpolator) {
                this.h0 = interpolator;
                this.g0 = new OverScroller(RecyclerView.this.getContext(), interpolator);
            }
            this.f0 = 0;
            this.a = 0;
            RecyclerView.this.setScrollState(2);
            this.g0.startScroll(0, 0, i, i2, i4);
            if (Build.VERSION.SDK_INT < 23) {
                this.g0.computeScrollOffset();
            }
            d();
        }

        public void f() {
            RecyclerView.this.removeCallbacks(this);
            this.g0.abortAnimation();
        }

        @Override // java.lang.Runnable
        public void run() {
            int i;
            int i2;
            RecyclerView recyclerView = RecyclerView.this;
            if (recyclerView.q0 == null) {
                f();
                return;
            }
            this.j0 = false;
            this.i0 = true;
            recyclerView.v();
            OverScroller overScroller = this.g0;
            if (overScroller.computeScrollOffset()) {
                int currX = overScroller.getCurrX();
                int currY = overScroller.getCurrY();
                int i3 = currX - this.a;
                int i4 = currY - this.f0;
                this.a = currX;
                this.f0 = currY;
                RecyclerView recyclerView2 = RecyclerView.this;
                int[] iArr = recyclerView2.w1;
                iArr[0] = 0;
                iArr[1] = 0;
                if (recyclerView2.G(i3, i4, iArr, null, 1)) {
                    int[] iArr2 = RecyclerView.this.w1;
                    i3 -= iArr2[0];
                    i4 -= iArr2[1];
                }
                if (RecyclerView.this.getOverScrollMode() != 2) {
                    RecyclerView.this.u(i3, i4);
                }
                RecyclerView recyclerView3 = RecyclerView.this;
                if (recyclerView3.p0 != null) {
                    int[] iArr3 = recyclerView3.w1;
                    iArr3[0] = 0;
                    iArr3[1] = 0;
                    recyclerView3.n1(i3, i4, iArr3);
                    RecyclerView recyclerView4 = RecyclerView.this;
                    int[] iArr4 = recyclerView4.w1;
                    i2 = iArr4[0];
                    i = iArr4[1];
                    i3 -= i2;
                    i4 -= i;
                    w wVar = recyclerView4.q0.k0;
                    if (wVar != null && !wVar.isPendingInitialRun() && wVar.isRunning()) {
                        int b = RecyclerView.this.j1.b();
                        if (b == 0) {
                            wVar.stop();
                        } else if (wVar.getTargetPosition() >= b) {
                            wVar.setTargetPosition(b - 1);
                            wVar.onAnimation(i2, i);
                        } else {
                            wVar.onAnimation(i2, i);
                        }
                    }
                } else {
                    i = 0;
                    i2 = 0;
                }
                if (!RecyclerView.this.t0.isEmpty()) {
                    RecyclerView.this.invalidate();
                }
                RecyclerView recyclerView5 = RecyclerView.this;
                int[] iArr5 = recyclerView5.w1;
                iArr5[0] = 0;
                iArr5[1] = 0;
                recyclerView5.H(i2, i, i3, i4, null, 1, iArr5);
                RecyclerView recyclerView6 = RecyclerView.this;
                int[] iArr6 = recyclerView6.w1;
                int i5 = i3 - iArr6[0];
                int i6 = i4 - iArr6[1];
                if (i2 != 0 || i != 0) {
                    recyclerView6.J(i2, i);
                }
                if (!RecyclerView.this.awakenScrollBars()) {
                    RecyclerView.this.invalidate();
                }
                boolean z = overScroller.isFinished() || (((overScroller.getCurrX() == overScroller.getFinalX()) || i5 != 0) && ((overScroller.getCurrY() == overScroller.getFinalY()) || i6 != 0));
                w wVar2 = RecyclerView.this.q0.k0;
                if (!(wVar2 != null && wVar2.isPendingInitialRun()) && z) {
                    if (RecyclerView.this.getOverScrollMode() != 2) {
                        int currVelocity = (int) overScroller.getCurrVelocity();
                        int i7 = i5 < 0 ? -currVelocity : i5 > 0 ? currVelocity : 0;
                        if (i6 < 0) {
                            currVelocity = -currVelocity;
                        } else if (i6 <= 0) {
                            currVelocity = 0;
                        }
                        RecyclerView.this.a(i7, currVelocity);
                    }
                    if (RecyclerView.H1) {
                        RecyclerView.this.i1.b();
                    }
                } else {
                    d();
                    RecyclerView recyclerView7 = RecyclerView.this;
                    androidx.recyclerview.widget.j jVar = recyclerView7.h1;
                    if (jVar != null) {
                        jVar.f(recyclerView7, i2, i);
                    }
                }
            }
            w wVar3 = RecyclerView.this.q0.k0;
            if (wVar3 != null && wVar3.isPendingInitialRun()) {
                wVar3.onAnimation(0, 0);
            }
            this.i0 = false;
            if (this.j0) {
                c();
                return;
            }
            RecyclerView.this.setScrollState(0);
            RecyclerView.this.A1(1);
        }
    }

    static {
        int i2 = Build.VERSION.SDK_INT;
        E1 = i2 == 18 || i2 == 19 || i2 == 20;
        F1 = i2 >= 23;
        G1 = i2 >= 16;
        H1 = i2 >= 21;
        I1 = i2 <= 15;
        J1 = i2 <= 15;
        Class<?> cls = Integer.TYPE;
        K1 = new Class[]{Context.class, AttributeSet.class, cls, cls};
        L1 = new c();
    }

    public RecyclerView(Context context) {
        this(context, null);
    }

    public static RecyclerView X(View view) {
        if (view instanceof ViewGroup) {
            if (view instanceof RecyclerView) {
                return (RecyclerView) view;
            }
            ViewGroup viewGroup = (ViewGroup) view;
            int childCount = viewGroup.getChildCount();
            for (int i2 = 0; i2 < childCount; i2++) {
                RecyclerView X = X(viewGroup.getChildAt(i2));
                if (X != null) {
                    return X;
                }
            }
            return null;
        }
        return null;
    }

    private qe2 getScrollingChildHelper() {
        if (this.t1 == null) {
            this.t1 = new qe2(this);
        }
        return this.t1;
    }

    public static a0 i0(View view) {
        if (view == null) {
            return null;
        }
        return ((LayoutParams) view.getLayoutParams()).a;
    }

    public static void k0(View view, Rect rect) {
        LayoutParams layoutParams = (LayoutParams) view.getLayoutParams();
        Rect rect2 = layoutParams.f0;
        rect.set((view.getLeft() - rect2.left) - ((ViewGroup.MarginLayoutParams) layoutParams).leftMargin, (view.getTop() - rect2.top) - ((ViewGroup.MarginLayoutParams) layoutParams).topMargin, view.getRight() + rect2.right + ((ViewGroup.MarginLayoutParams) layoutParams).rightMargin, view.getBottom() + rect2.bottom + ((ViewGroup.MarginLayoutParams) layoutParams).bottomMargin);
    }

    public static void s(a0 a0Var) {
        WeakReference<RecyclerView> weakReference = a0Var.mNestedRecyclerView;
        if (weakReference != null) {
            RecyclerView recyclerView = weakReference.get();
            while (recyclerView != null) {
                if (recyclerView == a0Var.itemView) {
                    return;
                }
                ViewParent parent = recyclerView.getParent();
                recyclerView = parent instanceof View ? (View) parent : null;
            }
            a0Var.mNestedRecyclerView = null;
        }
    }

    public void A(View view) {
        a0 i0 = i0(view);
        K0(view);
        Adapter adapter = this.p0;
        if (adapter != null && i0 != null) {
            adapter.onViewDetachedFromWindow(i0);
        }
        List<o> list = this.H0;
        if (list != null) {
            for (int size = list.size() - 1; size >= 0; size--) {
                this.H0.get(size).b(view);
            }
        }
    }

    public void A0(int i2) {
        if (this.q0 == null) {
            return;
        }
        setScrollState(2);
        this.q0.G1(i2);
        awakenScrollBars();
    }

    public void A1(int i2) {
        getScrollingChildHelper().r(i2);
    }

    public final void B() {
        int i2 = this.E0;
        this.E0 = 0;
        if (i2 == 0 || !x0()) {
            return;
        }
        AccessibilityEvent obtain = AccessibilityEvent.obtain();
        obtain.setEventType(2048);
        a6.b(obtain, i2);
        sendAccessibilityEventUnchecked(obtain);
    }

    public void B0() {
        int j2 = this.i0.j();
        for (int i2 = 0; i2 < j2; i2++) {
            ((LayoutParams) this.i0.i(i2).getLayoutParams()).g0 = true;
        }
        this.f0.s();
    }

    public void B1() {
        setScrollState(0);
        C1();
    }

    public void C() {
        if (this.p0 == null || this.q0 == null) {
            return;
        }
        this.j1.j = false;
        boolean z2 = this.z1 && !(this.A1 == getWidth() && this.B1 == getHeight());
        this.A1 = 0;
        this.B1 = 0;
        this.z1 = false;
        if (this.j1.e == 1) {
            D();
            this.q0.I1(this);
            E();
        } else if (!this.h0.q() && !z2 && this.q0.v0() == getWidth() && this.q0.h0() == getHeight()) {
            this.q0.I1(this);
        } else {
            this.q0.I1(this);
            E();
        }
        F();
    }

    public void C0() {
        int j2 = this.i0.j();
        for (int i2 = 0; i2 < j2; i2++) {
            a0 i0 = i0(this.i0.i(i2));
            if (i0 != null && !i0.shouldIgnore()) {
                i0.addFlags(6);
            }
        }
        B0();
        this.f0.t();
    }

    public final void C1() {
        this.g1.f();
        LayoutManager layoutManager = this.q0;
        if (layoutManager != null) {
            layoutManager.T1();
        }
    }

    public final void D() {
        boolean z2 = true;
        this.j1.a(1);
        R(this.j1);
        this.j1.j = false;
        x1();
        this.j0.f();
        L0();
        T0();
        k1();
        x xVar = this.j1;
        xVar.i = (xVar.k && this.n1) ? false : false;
        this.n1 = false;
        this.m1 = false;
        xVar.h = xVar.l;
        xVar.f = this.p0.getItemCount();
        W(this.s1);
        if (this.j1.k) {
            int g2 = this.i0.g();
            for (int i2 = 0; i2 < g2; i2++) {
                a0 i0 = i0(this.i0.f(i2));
                if (!i0.shouldIgnore() && (!i0.isInvalid() || this.p0.hasStableIds())) {
                    this.j0.e(i0, this.R0.recordPreLayoutInformation(this.j1, i0, l.buildAdapterChangeFlagsForAnimations(i0), i0.getUnmodifiedPayloads()));
                    if (this.j1.i && i0.isUpdated() && !i0.isRemoved() && !i0.shouldIgnore() && !i0.isInvalid()) {
                        this.j0.c(e0(i0), i0);
                    }
                }
            }
        }
        if (this.j1.l) {
            l1();
            x xVar2 = this.j1;
            boolean z3 = xVar2.g;
            xVar2.g = false;
            this.q0.f1(this.f0, xVar2);
            this.j1.g = z3;
            for (int i3 = 0; i3 < this.i0.g(); i3++) {
                a0 i02 = i0(this.i0.f(i3));
                if (!i02.shouldIgnore() && !this.j0.i(i02)) {
                    int buildAdapterChangeFlagsForAnimations = l.buildAdapterChangeFlagsForAnimations(i02);
                    boolean hasAnyOfTheFlags = i02.hasAnyOfTheFlags(8192);
                    if (!hasAnyOfTheFlags) {
                        buildAdapterChangeFlagsForAnimations |= 4096;
                    }
                    l.c recordPreLayoutInformation = this.R0.recordPreLayoutInformation(this.j1, i02, buildAdapterChangeFlagsForAnimations, i02.getUnmodifiedPayloads());
                    if (hasAnyOfTheFlags) {
                        W0(i02, recordPreLayoutInformation);
                    } else {
                        this.j0.a(i02, recordPreLayoutInformation);
                    }
                }
            }
            t();
        } else {
            t();
        }
        M0();
        z1(false);
        this.j1.e = 2;
    }

    public final void D0(int i2, int i3, MotionEvent motionEvent, int i4) {
        LayoutManager layoutManager = this.q0;
        if (layoutManager == null || this.C0) {
            return;
        }
        int[] iArr = this.w1;
        iArr[0] = 0;
        iArr[1] = 0;
        boolean v2 = layoutManager.v();
        boolean w2 = this.q0.w();
        y1(w2 ? v2 | 2 : v2, i4);
        if (G(v2 != 0 ? i2 : 0, w2 ? i3 : 0, this.w1, this.u1, i4)) {
            int[] iArr2 = this.w1;
            i2 -= iArr2[0];
            i3 -= iArr2[1];
        }
        m1(v2 != 0 ? i2 : 0, w2 ? i3 : 0, motionEvent, i4);
        androidx.recyclerview.widget.j jVar = this.h1;
        if (jVar != null && (i2 != 0 || i3 != 0)) {
            jVar.f(this, i2, i3);
        }
        A1(i4);
    }

    public void D1(int i2, int i3, Object obj) {
        int i4;
        int j2 = this.i0.j();
        int i5 = i2 + i3;
        for (int i6 = 0; i6 < j2; i6++) {
            View i7 = this.i0.i(i6);
            a0 i0 = i0(i7);
            if (i0 != null && !i0.shouldIgnore() && (i4 = i0.mPosition) >= i2 && i4 < i5) {
                i0.addFlags(2);
                i0.addChangePayload(obj);
                ((LayoutParams) i7.getLayoutParams()).g0 = true;
            }
        }
        this.f0.M(i2, i3);
    }

    public final void E() {
        x1();
        L0();
        this.j1.a(6);
        this.h0.j();
        this.j1.f = this.p0.getItemCount();
        this.j1.d = 0;
        if (this.g0 != null && this.p0.canRestoreState()) {
            Parcelable parcelable = this.g0.g0;
            if (parcelable != null) {
                this.q0.k1(parcelable);
            }
            this.g0 = null;
        }
        x xVar = this.j1;
        xVar.h = false;
        this.q0.f1(this.f0, xVar);
        x xVar2 = this.j1;
        xVar2.g = false;
        xVar2.k = xVar2.k && this.R0 != null;
        xVar2.e = 4;
        M0();
        z1(false);
    }

    public void E0(int i2) {
        int g2 = this.i0.g();
        for (int i3 = 0; i3 < g2; i3++) {
            this.i0.f(i3).offsetLeftAndRight(i2);
        }
    }

    public final void F() {
        this.j1.a(4);
        x1();
        L0();
        x xVar = this.j1;
        xVar.e = 1;
        if (xVar.k) {
            for (int g2 = this.i0.g() - 1; g2 >= 0; g2--) {
                a0 i0 = i0(this.i0.f(g2));
                if (!i0.shouldIgnore()) {
                    long e0 = e0(i0);
                    l.c recordPostLayoutInformation = this.R0.recordPostLayoutInformation(this.j1, i0);
                    a0 g3 = this.j0.g(e0);
                    if (g3 != null && !g3.shouldIgnore()) {
                        boolean h2 = this.j0.h(g3);
                        boolean h3 = this.j0.h(i0);
                        if (h2 && g3 == i0) {
                            this.j0.d(i0, recordPostLayoutInformation);
                        } else {
                            l.c n2 = this.j0.n(g3);
                            this.j0.d(i0, recordPostLayoutInformation);
                            l.c m2 = this.j0.m(i0);
                            if (n2 == null) {
                                p0(e0, i0, g3);
                            } else {
                                n(g3, i0, n2, m2, h2, h3);
                            }
                        }
                    } else {
                        this.j0.d(i0, recordPostLayoutInformation);
                    }
                }
            }
            this.j0.o(this.C1);
        }
        this.q0.u1(this.f0);
        x xVar2 = this.j1;
        xVar2.c = xVar2.f;
        this.I0 = false;
        this.J0 = false;
        xVar2.k = false;
        xVar2.l = false;
        this.q0.l0 = false;
        ArrayList<a0> arrayList = this.f0.b;
        if (arrayList != null) {
            arrayList.clear();
        }
        LayoutManager layoutManager = this.q0;
        if (layoutManager.r0) {
            layoutManager.q0 = 0;
            layoutManager.r0 = false;
            this.f0.K();
        }
        this.q0.g1(this.j1);
        M0();
        z1(false);
        this.j0.f();
        int[] iArr = this.s1;
        if (y(iArr[0], iArr[1])) {
            J(0, 0);
        }
        X0();
        i1();
    }

    public void F0(int i2) {
        int g2 = this.i0.g();
        for (int i3 = 0; i3 < g2; i3++) {
            this.i0.f(i3).offsetTopAndBottom(i2);
        }
    }

    public boolean G(int i2, int i3, int[] iArr, int[] iArr2, int i4) {
        return getScrollingChildHelper().d(i2, i3, iArr, iArr2, i4);
    }

    public void G0(int i2, int i3) {
        int j2 = this.i0.j();
        for (int i4 = 0; i4 < j2; i4++) {
            a0 i0 = i0(this.i0.i(i4));
            if (i0 != null && !i0.shouldIgnore() && i0.mPosition >= i2) {
                i0.offsetPosition(i3, false);
                this.j1.g = true;
            }
        }
        this.f0.u(i2, i3);
        requestLayout();
    }

    public final void H(int i2, int i3, int i4, int i5, int[] iArr, int i6, int[] iArr2) {
        getScrollingChildHelper().e(i2, i3, i4, i5, iArr, i6, iArr2);
    }

    public void H0(int i2, int i3) {
        int i4;
        int i5;
        int i6;
        int i7;
        int j2 = this.i0.j();
        if (i2 < i3) {
            i6 = -1;
            i5 = i2;
            i4 = i3;
        } else {
            i4 = i2;
            i5 = i3;
            i6 = 1;
        }
        for (int i8 = 0; i8 < j2; i8++) {
            a0 i0 = i0(this.i0.i(i8));
            if (i0 != null && (i7 = i0.mPosition) >= i5 && i7 <= i4) {
                if (i7 == i2) {
                    i0.offsetPosition(i3 - i2, false);
                } else {
                    i0.offsetPosition(i6, false);
                }
                this.j1.g = true;
            }
        }
        this.f0.v(i2, i3);
        requestLayout();
    }

    public void I(int i2) {
        LayoutManager layoutManager = this.q0;
        if (layoutManager != null) {
            layoutManager.m1(i2);
        }
        P0(i2);
        r rVar = this.k1;
        if (rVar != null) {
            rVar.onScrollStateChanged(this, i2);
        }
        List<r> list = this.l1;
        if (list != null) {
            for (int size = list.size() - 1; size >= 0; size--) {
                this.l1.get(size).onScrollStateChanged(this, i2);
            }
        }
    }

    public void I0(int i2, int i3, boolean z2) {
        int i4 = i2 + i3;
        int j2 = this.i0.j();
        for (int i5 = 0; i5 < j2; i5++) {
            a0 i0 = i0(this.i0.i(i5));
            if (i0 != null && !i0.shouldIgnore()) {
                int i6 = i0.mPosition;
                if (i6 >= i4) {
                    i0.offsetPosition(-i3, z2);
                    this.j1.g = true;
                } else if (i6 >= i2) {
                    i0.flagRemovedAndOffsetPosition(i2 - 1, -i3, z2);
                    this.j1.g = true;
                }
            }
        }
        this.f0.w(i2, i3, z2);
        requestLayout();
    }

    public void J(int i2, int i3) {
        this.L0++;
        int scrollX = getScrollX();
        int scrollY = getScrollY();
        onScrollChanged(scrollX, scrollY, scrollX - i2, scrollY - i3);
        Q0(i2, i3);
        r rVar = this.k1;
        if (rVar != null) {
            rVar.onScrolled(this, i2, i3);
        }
        List<r> list = this.l1;
        if (list != null) {
            for (int size = list.size() - 1; size >= 0; size--) {
                this.l1.get(size).onScrolled(this, i2, i3);
            }
        }
        this.L0--;
    }

    public void J0(View view) {
    }

    public void K() {
        int i2;
        for (int size = this.x1.size() - 1; size >= 0; size--) {
            a0 a0Var = this.x1.get(size);
            if (a0Var.itemView.getParent() == this && !a0Var.shouldIgnore() && (i2 = a0Var.mPendingAccessibilityState) != -1) {
                ei4.D0(a0Var.itemView, i2);
                a0Var.mPendingAccessibilityState = -1;
            }
        }
        this.x1.clear();
    }

    public void K0(View view) {
    }

    public final boolean L(MotionEvent motionEvent) {
        q qVar = this.v0;
        if (qVar == null) {
            if (motionEvent.getAction() == 0) {
                return false;
            }
            return V(motionEvent);
        }
        qVar.a(this, motionEvent);
        int action = motionEvent.getAction();
        if (action == 3 || action == 1) {
            this.v0 = null;
        }
        return true;
    }

    public void L0() {
        this.K0++;
    }

    public void M() {
        if (this.Q0 != null) {
            return;
        }
        EdgeEffect a2 = this.M0.a(this, 3);
        this.Q0 = a2;
        if (this.k0) {
            a2.setSize((getMeasuredWidth() - getPaddingLeft()) - getPaddingRight(), (getMeasuredHeight() - getPaddingTop()) - getPaddingBottom());
        } else {
            a2.setSize(getMeasuredWidth(), getMeasuredHeight());
        }
    }

    public void M0() {
        N0(true);
    }

    public void N() {
        if (this.N0 != null) {
            return;
        }
        EdgeEffect a2 = this.M0.a(this, 0);
        this.N0 = a2;
        if (this.k0) {
            a2.setSize((getMeasuredHeight() - getPaddingTop()) - getPaddingBottom(), (getMeasuredWidth() - getPaddingLeft()) - getPaddingRight());
        } else {
            a2.setSize(getMeasuredHeight(), getMeasuredWidth());
        }
    }

    public void N0(boolean z2) {
        int i2 = this.K0 - 1;
        this.K0 = i2;
        if (i2 < 1) {
            this.K0 = 0;
            if (z2) {
                B();
                K();
            }
        }
    }

    public void O() {
        if (this.P0 != null) {
            return;
        }
        EdgeEffect a2 = this.M0.a(this, 2);
        this.P0 = a2;
        if (this.k0) {
            a2.setSize((getMeasuredHeight() - getPaddingTop()) - getPaddingBottom(), (getMeasuredWidth() - getPaddingLeft()) - getPaddingRight());
        } else {
            a2.setSize(getMeasuredHeight(), getMeasuredWidth());
        }
    }

    public final void O0(MotionEvent motionEvent) {
        int actionIndex = motionEvent.getActionIndex();
        if (motionEvent.getPointerId(actionIndex) == this.T0) {
            int i2 = actionIndex == 0 ? 1 : 0;
            this.T0 = motionEvent.getPointerId(i2);
            int x2 = (int) (motionEvent.getX(i2) + 0.5f);
            this.X0 = x2;
            this.V0 = x2;
            int y2 = (int) (motionEvent.getY(i2) + 0.5f);
            this.Y0 = y2;
            this.W0 = y2;
        }
    }

    public void P() {
        if (this.O0 != null) {
            return;
        }
        EdgeEffect a2 = this.M0.a(this, 1);
        this.O0 = a2;
        if (this.k0) {
            a2.setSize((getMeasuredWidth() - getPaddingLeft()) - getPaddingRight(), (getMeasuredHeight() - getPaddingTop()) - getPaddingBottom());
        } else {
            a2.setSize(getMeasuredWidth(), getMeasuredHeight());
        }
    }

    public void P0(int i2) {
    }

    public String Q() {
        return MinimalPrettyPrinter.DEFAULT_ROOT_VALUE_SEPARATOR + super.toString() + ", adapter:" + this.p0 + ", layout:" + this.q0 + ", context:" + getContext();
    }

    public void Q0(int i2, int i3) {
    }

    public final void R(x xVar) {
        if (getScrollState() == 2) {
            OverScroller overScroller = this.g1.g0;
            xVar.p = overScroller.getFinalX() - overScroller.getCurrX();
            overScroller.getFinalY();
            overScroller.getCurrY();
            return;
        }
        xVar.p = 0;
    }

    public void R0() {
        if (this.p1 || !this.w0) {
            return;
        }
        ei4.l0(this, this.y1);
        this.p1 = true;
    }

    public View S(float f2, float f3) {
        for (int g2 = this.i0.g() - 1; g2 >= 0; g2--) {
            View f4 = this.i0.f(g2);
            float translationX = f4.getTranslationX();
            float translationY = f4.getTranslationY();
            if (f2 >= f4.getLeft() + translationX && f2 <= f4.getRight() + translationX && f3 >= f4.getTop() + translationY && f3 <= f4.getBottom() + translationY) {
                return f4;
            }
        }
        return null;
    }

    public final boolean S0() {
        return this.R0 != null && this.q0.U1();
    }

    /* JADX WARN: Code restructure failed: missing block: B:15:?, code lost:
        return r3;
     */
    /*
        Code decompiled incorrectly, please refer to instructions dump.
        To view partially-correct code enable 'Show inconsistent code' option in preferences
    */
    public android.view.View T(android.view.View r3) {
        /*
            r2 = this;
            android.view.ViewParent r0 = r3.getParent()
        L4:
            if (r0 == 0) goto L14
            if (r0 == r2) goto L14
            boolean r1 = r0 instanceof android.view.View
            if (r1 == 0) goto L14
            r3 = r0
            android.view.View r3 = (android.view.View) r3
            android.view.ViewParent r0 = r3.getParent()
            goto L4
        L14:
            if (r0 != r2) goto L17
            goto L18
        L17:
            r3 = 0
        L18:
            return r3
        */
        throw new UnsupportedOperationException("Method not decompiled: androidx.recyclerview.widget.RecyclerView.T(android.view.View):android.view.View");
    }

    public final void T0() {
        boolean z2;
        if (this.I0) {
            this.h0.y();
            if (this.J0) {
                this.q0.a1(this);
            }
        }
        if (S0()) {
            this.h0.w();
        } else {
            this.h0.j();
        }
        boolean z3 = false;
        boolean z4 = this.m1 || this.n1;
        this.j1.k = this.z0 && this.R0 != null && ((z2 = this.I0) || z4 || this.q0.l0) && (!z2 || this.p0.hasStableIds());
        x xVar = this.j1;
        if (xVar.k && z4 && !this.I0 && S0()) {
            z3 = true;
        }
        xVar.l = z3;
    }

    public a0 U(View view) {
        View T = T(view);
        if (T == null) {
            return null;
        }
        return h0(T);
    }

    public void U0(boolean z2) {
        this.J0 = z2 | this.J0;
        this.I0 = true;
        C0();
    }

    public final boolean V(MotionEvent motionEvent) {
        int action = motionEvent.getAction();
        int size = this.u0.size();
        for (int i2 = 0; i2 < size; i2++) {
            q qVar = this.u0.get(i2);
            if (qVar.c(this, motionEvent) && action != 3) {
                this.v0 = qVar;
                return true;
            }
        }
        return false;
    }

    /* JADX WARN: Removed duplicated region for block: B:12:0x0040  */
    /* JADX WARN: Removed duplicated region for block: B:13:0x0056  */
    /*
        Code decompiled incorrectly, please refer to instructions dump.
        To view partially-correct code enable 'Show inconsistent code' option in preferences
    */
    public final void V0(float r7, float r8, float r9, float r10) {
        /*
            r6 = this;
            r0 = 0
            int r1 = (r8 > r0 ? 1 : (r8 == r0 ? 0 : -1))
            r2 = 1065353216(0x3f800000, float:1.0)
            r3 = 1
            if (r1 >= 0) goto L21
            r6.N()
            android.widget.EdgeEffect r1 = r6.N0
            float r4 = -r8
            int r5 = r6.getWidth()
            float r5 = (float) r5
            float r4 = r4 / r5
            int r5 = r6.getHeight()
            float r5 = (float) r5
            float r9 = r9 / r5
            float r9 = r2 - r9
            defpackage.bu0.c(r1, r4, r9)
        L1f:
            r9 = r3
            goto L3c
        L21:
            int r1 = (r8 > r0 ? 1 : (r8 == r0 ? 0 : -1))
            if (r1 <= 0) goto L3b
            r6.O()
            android.widget.EdgeEffect r1 = r6.P0
            int r4 = r6.getWidth()
            float r4 = (float) r4
            float r4 = r8 / r4
            int r5 = r6.getHeight()
            float r5 = (float) r5
            float r9 = r9 / r5
            defpackage.bu0.c(r1, r4, r9)
            goto L1f
        L3b:
            r9 = 0
        L3c:
            int r1 = (r10 > r0 ? 1 : (r10 == r0 ? 0 : -1))
            if (r1 >= 0) goto L56
            r6.P()
            android.widget.EdgeEffect r9 = r6.O0
            float r1 = -r10
            int r2 = r6.getHeight()
            float r2 = (float) r2
            float r1 = r1 / r2
            int r2 = r6.getWidth()
            float r2 = (float) r2
            float r7 = r7 / r2
            defpackage.bu0.c(r9, r1, r7)
            goto L72
        L56:
            int r1 = (r10 > r0 ? 1 : (r10 == r0 ? 0 : -1))
            if (r1 <= 0) goto L71
            r6.M()
            android.widget.EdgeEffect r9 = r6.Q0
            int r1 = r6.getHeight()
            float r1 = (float) r1
            float r1 = r10 / r1
            int r4 = r6.getWidth()
            float r4 = (float) r4
            float r7 = r7 / r4
            float r2 = r2 - r7
            defpackage.bu0.c(r9, r1, r2)
            goto L72
        L71:
            r3 = r9
        L72:
            if (r3 != 0) goto L7c
            int r7 = (r8 > r0 ? 1 : (r8 == r0 ? 0 : -1))
            if (r7 != 0) goto L7c
            int r7 = (r10 > r0 ? 1 : (r10 == r0 ? 0 : -1))
            if (r7 == 0) goto L7f
        L7c:
            defpackage.ei4.j0(r6)
        L7f:
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: androidx.recyclerview.widget.RecyclerView.V0(float, float, float, float):void");
    }

    public final void W(int[] iArr) {
        int g2 = this.i0.g();
        if (g2 == 0) {
            iArr[0] = -1;
            iArr[1] = -1;
            return;
        }
        int i2 = Integer.MAX_VALUE;
        int i3 = Integer.MIN_VALUE;
        for (int i4 = 0; i4 < g2; i4++) {
            a0 i0 = i0(this.i0.f(i4));
            if (!i0.shouldIgnore()) {
                int layoutPosition = i0.getLayoutPosition();
                if (layoutPosition < i2) {
                    i2 = layoutPosition;
                }
                if (layoutPosition > i3) {
                    i3 = layoutPosition;
                }
            }
        }
        iArr[0] = i2;
        iArr[1] = i3;
    }

    public void W0(a0 a0Var, l.c cVar) {
        a0Var.setFlags(0, 8192);
        if (this.j1.i && a0Var.isUpdated() && !a0Var.isRemoved() && !a0Var.shouldIgnore()) {
            this.j0.c(e0(a0Var), a0Var);
        }
        this.j0.e(a0Var, cVar);
    }

    public final void X0() {
        View findViewById;
        if (!this.f1 || this.p0 == null || !hasFocus() || getDescendantFocusability() == 393216) {
            return;
        }
        if (getDescendantFocusability() == 131072 && isFocused()) {
            return;
        }
        if (!isFocused()) {
            View focusedChild = getFocusedChild();
            if (J1 && (focusedChild.getParent() == null || !focusedChild.hasFocus())) {
                if (this.i0.g() == 0) {
                    requestFocus();
                    return;
                }
            } else if (!this.i0.n(focusedChild)) {
                return;
            }
        }
        View view = null;
        a0 a02 = (this.j1.n == -1 || !this.p0.hasStableIds()) ? null : a0(this.j1.n);
        if (a02 != null && !this.i0.n(a02.itemView) && a02.itemView.hasFocusable()) {
            view = a02.itemView;
        } else if (this.i0.g() > 0) {
            view = Y();
        }
        if (view != null) {
            int i2 = this.j1.o;
            if (i2 != -1 && (findViewById = view.findViewById(i2)) != null && findViewById.isFocusable()) {
                view = findViewById;
            }
            view.requestFocus();
        }
    }

    public final View Y() {
        a0 Z;
        x xVar = this.j1;
        int i2 = xVar.m;
        if (i2 == -1) {
            i2 = 0;
        }
        int b2 = xVar.b();
        for (int i3 = i2; i3 < b2; i3++) {
            a0 Z2 = Z(i3);
            if (Z2 == null) {
                break;
            } else if (Z2.itemView.hasFocusable()) {
                return Z2.itemView;
            }
        }
        int min = Math.min(b2, i2);
        while (true) {
            min--;
            if (min < 0 || (Z = Z(min)) == null) {
                return null;
            }
            if (Z.itemView.hasFocusable()) {
                return Z.itemView;
            }
        }
    }

    public final void Y0() {
        boolean z2;
        EdgeEffect edgeEffect = this.N0;
        if (edgeEffect != null) {
            edgeEffect.onRelease();
            z2 = this.N0.isFinished();
        } else {
            z2 = false;
        }
        EdgeEffect edgeEffect2 = this.O0;
        if (edgeEffect2 != null) {
            edgeEffect2.onRelease();
            z2 |= this.O0.isFinished();
        }
        EdgeEffect edgeEffect3 = this.P0;
        if (edgeEffect3 != null) {
            edgeEffect3.onRelease();
            z2 |= this.P0.isFinished();
        }
        EdgeEffect edgeEffect4 = this.Q0;
        if (edgeEffect4 != null) {
            edgeEffect4.onRelease();
            z2 |= this.Q0.isFinished();
        }
        if (z2) {
            ei4.j0(this);
        }
    }

    public a0 Z(int i2) {
        a0 a0Var = null;
        if (this.I0) {
            return null;
        }
        int j2 = this.i0.j();
        for (int i3 = 0; i3 < j2; i3++) {
            a0 i0 = i0(this.i0.i(i3));
            if (i0 != null && !i0.isRemoved() && d0(i0) == i2) {
                if (!this.i0.n(i0.itemView)) {
                    return i0;
                }
                a0Var = i0;
            }
        }
        return a0Var;
    }

    public void Z0() {
        l lVar = this.R0;
        if (lVar != null) {
            lVar.endAnimations();
        }
        LayoutManager layoutManager = this.q0;
        if (layoutManager != null) {
            layoutManager.t1(this.f0);
            this.q0.u1(this.f0);
        }
        this.f0.c();
    }

    public void a(int i2, int i3) {
        if (i2 < 0) {
            N();
            if (this.N0.isFinished()) {
                this.N0.onAbsorb(-i2);
            }
        } else if (i2 > 0) {
            O();
            if (this.P0.isFinished()) {
                this.P0.onAbsorb(i2);
            }
        }
        if (i3 < 0) {
            P();
            if (this.O0.isFinished()) {
                this.O0.onAbsorb(-i3);
            }
        } else if (i3 > 0) {
            M();
            if (this.Q0.isFinished()) {
                this.Q0.onAbsorb(i3);
            }
        }
        if (i2 == 0 && i3 == 0) {
            return;
        }
        ei4.j0(this);
    }

    public a0 a0(long j2) {
        Adapter adapter = this.p0;
        a0 a0Var = null;
        if (adapter != null && adapter.hasStableIds()) {
            int j3 = this.i0.j();
            for (int i2 = 0; i2 < j3; i2++) {
                a0 i0 = i0(this.i0.i(i2));
                if (i0 != null && !i0.isRemoved() && i0.getItemId() == j2) {
                    if (!this.i0.n(i0.itemView)) {
                        return i0;
                    }
                    a0Var = i0;
                }
            }
        }
        return a0Var;
    }

    public boolean a1(View view) {
        x1();
        boolean r2 = this.i0.r(view);
        if (r2) {
            a0 i0 = i0(view);
            this.f0.J(i0);
            this.f0.C(i0);
        }
        z1(!r2);
        return r2;
    }

    @Override // android.view.ViewGroup, android.view.View
    public void addFocusables(ArrayList<View> arrayList, int i2, int i3) {
        LayoutManager layoutManager = this.q0;
        if (layoutManager == null || !layoutManager.N0(this, arrayList, i2, i3)) {
            super.addFocusables(arrayList, i2, i3);
        }
    }

    /* JADX WARN: Removed duplicated region for block: B:17:0x0034  */
    /* JADX WARN: Removed duplicated region for block: B:22:0x0036 A[SYNTHETIC] */
    /*
        Code decompiled incorrectly, please refer to instructions dump.
        To view partially-correct code enable 'Show inconsistent code' option in preferences
    */
    public androidx.recyclerview.widget.RecyclerView.a0 b0(int r6, boolean r7) {
        /*
            r5 = this;
            androidx.recyclerview.widget.e r0 = r5.i0
            int r0 = r0.j()
            r1 = 0
            r2 = 0
        L8:
            if (r2 >= r0) goto L3a
            androidx.recyclerview.widget.e r3 = r5.i0
            android.view.View r3 = r3.i(r2)
            androidx.recyclerview.widget.RecyclerView$a0 r3 = i0(r3)
            if (r3 == 0) goto L37
            boolean r4 = r3.isRemoved()
            if (r4 != 0) goto L37
            if (r7 == 0) goto L23
            int r4 = r3.mPosition
            if (r4 == r6) goto L2a
            goto L37
        L23:
            int r4 = r3.getLayoutPosition()
            if (r4 == r6) goto L2a
            goto L37
        L2a:
            androidx.recyclerview.widget.e r1 = r5.i0
            android.view.View r4 = r3.itemView
            boolean r1 = r1.n(r4)
            if (r1 == 0) goto L36
            r1 = r3
            goto L37
        L36:
            return r3
        L37:
            int r2 = r2 + 1
            goto L8
        L3a:
            return r1
        */
        throw new UnsupportedOperationException("Method not decompiled: androidx.recyclerview.widget.RecyclerView.b0(int, boolean):androidx.recyclerview.widget.RecyclerView$a0");
    }

    public void b1(n nVar) {
        LayoutManager layoutManager = this.q0;
        if (layoutManager != null) {
            layoutManager.r("Cannot remove item decoration during a scroll  or layout");
        }
        this.t0.remove(nVar);
        if (this.t0.isEmpty()) {
            setWillNotDraw(getOverScrollMode() == 2);
        }
        B0();
        requestLayout();
    }

    /* JADX WARN: Multi-variable type inference failed */
    /* JADX WARN: Type inference failed for: r0v1, types: [boolean] */
    /* JADX WARN: Type inference failed for: r0v6 */
    public boolean c0(int i2, int i3) {
        LayoutManager layoutManager = this.q0;
        if (layoutManager == null || this.C0) {
            return false;
        }
        int v2 = layoutManager.v();
        boolean w2 = this.q0.w();
        if (v2 == 0 || Math.abs(i2) < this.b1) {
            i2 = 0;
        }
        if (!w2 || Math.abs(i3) < this.b1) {
            i3 = 0;
        }
        if (i2 == 0 && i3 == 0) {
            return false;
        }
        float f2 = i2;
        float f3 = i3;
        if (!dispatchNestedPreFling(f2, f3)) {
            boolean z2 = v2 != 0 || w2;
            dispatchNestedFling(f2, f3, z2);
            p pVar = this.a1;
            if (pVar != null && pVar.a(i2, i3)) {
                return true;
            }
            if (z2) {
                if (w2) {
                    v2 = (v2 == true ? 1 : 0) | 2;
                }
                y1(v2, 1);
                int i4 = this.c1;
                int max = Math.max(-i4, Math.min(i2, i4));
                int i5 = this.c1;
                this.g1.b(max, Math.max(-i5, Math.min(i3, i5)));
                return true;
            }
        }
        return false;
    }

    public void c1(int i2) {
        int itemDecorationCount = getItemDecorationCount();
        if (i2 >= 0 && i2 < itemDecorationCount) {
            b1(o0(i2));
            return;
        }
        throw new IndexOutOfBoundsException(i2 + " is an invalid index for size " + itemDecorationCount);
    }

    @Override // android.view.ViewGroup
    public boolean checkLayoutParams(ViewGroup.LayoutParams layoutParams) {
        return (layoutParams instanceof LayoutParams) && this.q0.x((LayoutParams) layoutParams);
    }

    @Override // android.view.View
    public int computeHorizontalScrollExtent() {
        LayoutManager layoutManager = this.q0;
        if (layoutManager != null && layoutManager.v()) {
            return this.q0.B(this.j1);
        }
        return 0;
    }

    @Override // android.view.View
    public int computeHorizontalScrollOffset() {
        LayoutManager layoutManager = this.q0;
        if (layoutManager != null && layoutManager.v()) {
            return this.q0.C(this.j1);
        }
        return 0;
    }

    @Override // android.view.View
    public int computeHorizontalScrollRange() {
        LayoutManager layoutManager = this.q0;
        if (layoutManager != null && layoutManager.v()) {
            return this.q0.D(this.j1);
        }
        return 0;
    }

    @Override // android.view.View
    public int computeVerticalScrollExtent() {
        LayoutManager layoutManager = this.q0;
        if (layoutManager != null && layoutManager.w()) {
            return this.q0.E(this.j1);
        }
        return 0;
    }

    @Override // android.view.View
    public int computeVerticalScrollOffset() {
        LayoutManager layoutManager = this.q0;
        if (layoutManager != null && layoutManager.w()) {
            return this.q0.F(this.j1);
        }
        return 0;
    }

    @Override // android.view.View
    public int computeVerticalScrollRange() {
        LayoutManager layoutManager = this.q0;
        if (layoutManager != null && layoutManager.w()) {
            return this.q0.G(this.j1);
        }
        return 0;
    }

    public int d0(a0 a0Var) {
        if (a0Var.hasAnyOfTheFlags(524) || !a0Var.isBound()) {
            return -1;
        }
        return this.h0.e(a0Var.mPosition);
    }

    public void d1(o oVar) {
        List<o> list = this.H0;
        if (list == null) {
            return;
        }
        list.remove(oVar);
    }

    @Override // android.view.View
    public boolean dispatchNestedFling(float f2, float f3, boolean z2) {
        return getScrollingChildHelper().a(f2, f3, z2);
    }

    @Override // android.view.View
    public boolean dispatchNestedPreFling(float f2, float f3) {
        return getScrollingChildHelper().b(f2, f3);
    }

    @Override // android.view.View
    public boolean dispatchNestedPreScroll(int i2, int i3, int[] iArr, int[] iArr2) {
        return getScrollingChildHelper().c(i2, i3, iArr, iArr2);
    }

    @Override // android.view.View
    public boolean dispatchNestedScroll(int i2, int i3, int i4, int i5, int[] iArr) {
        return getScrollingChildHelper().f(i2, i3, i4, i5, iArr);
    }

    @Override // android.view.View
    public boolean dispatchPopulateAccessibilityEvent(AccessibilityEvent accessibilityEvent) {
        onPopulateAccessibilityEvent(accessibilityEvent);
        return true;
    }

    @Override // android.view.ViewGroup, android.view.View
    public void dispatchRestoreInstanceState(SparseArray<Parcelable> sparseArray) {
        dispatchThawSelfOnly(sparseArray);
    }

    @Override // android.view.ViewGroup, android.view.View
    public void dispatchSaveInstanceState(SparseArray<Parcelable> sparseArray) {
        dispatchFreezeSelfOnly(sparseArray);
    }

    @Override // android.view.View
    public void draw(Canvas canvas) {
        boolean z2;
        super.draw(canvas);
        int size = this.t0.size();
        boolean z3 = false;
        for (int i2 = 0; i2 < size; i2++) {
            this.t0.get(i2).onDrawOver(canvas, this, this.j1);
        }
        EdgeEffect edgeEffect = this.N0;
        boolean z4 = true;
        if (edgeEffect == null || edgeEffect.isFinished()) {
            z2 = false;
        } else {
            int save = canvas.save();
            int paddingBottom = this.k0 ? getPaddingBottom() : 0;
            canvas.rotate(270.0f);
            canvas.translate((-getHeight()) + paddingBottom, Utils.FLOAT_EPSILON);
            EdgeEffect edgeEffect2 = this.N0;
            z2 = edgeEffect2 != null && edgeEffect2.draw(canvas);
            canvas.restoreToCount(save);
        }
        EdgeEffect edgeEffect3 = this.O0;
        if (edgeEffect3 != null && !edgeEffect3.isFinished()) {
            int save2 = canvas.save();
            if (this.k0) {
                canvas.translate(getPaddingLeft(), getPaddingTop());
            }
            EdgeEffect edgeEffect4 = this.O0;
            z2 |= edgeEffect4 != null && edgeEffect4.draw(canvas);
            canvas.restoreToCount(save2);
        }
        EdgeEffect edgeEffect5 = this.P0;
        if (edgeEffect5 != null && !edgeEffect5.isFinished()) {
            int save3 = canvas.save();
            int width = getWidth();
            int paddingTop = this.k0 ? getPaddingTop() : 0;
            canvas.rotate(90.0f);
            canvas.translate(paddingTop, -width);
            EdgeEffect edgeEffect6 = this.P0;
            z2 |= edgeEffect6 != null && edgeEffect6.draw(canvas);
            canvas.restoreToCount(save3);
        }
        EdgeEffect edgeEffect7 = this.Q0;
        if (edgeEffect7 != null && !edgeEffect7.isFinished()) {
            int save4 = canvas.save();
            canvas.rotate(180.0f);
            if (this.k0) {
                canvas.translate((-getWidth()) + getPaddingRight(), (-getHeight()) + getPaddingBottom());
            } else {
                canvas.translate(-getWidth(), -getHeight());
            }
            EdgeEffect edgeEffect8 = this.Q0;
            if (edgeEffect8 != null && edgeEffect8.draw(canvas)) {
                z3 = true;
            }
            z2 |= z3;
            canvas.restoreToCount(save4);
        }
        if (z2 || this.R0 == null || this.t0.size() <= 0 || !this.R0.isRunning()) {
            z4 = z2;
        }
        if (z4) {
            ei4.j0(this);
        }
    }

    @Override // android.view.ViewGroup
    public boolean drawChild(Canvas canvas, View view, long j2) {
        return super.drawChild(canvas, view, j2);
    }

    public long e0(a0 a0Var) {
        return this.p0.hasStableIds() ? a0Var.getItemId() : a0Var.mPosition;
    }

    public void e1(q qVar) {
        this.u0.remove(qVar);
        if (this.v0 == qVar) {
            this.v0 = null;
        }
    }

    public int f0(View view) {
        a0 i0 = i0(view);
        if (i0 != null) {
            return i0.getAbsoluteAdapterPosition();
        }
        return -1;
    }

    public void f1(r rVar) {
        List<r> list = this.l1;
        if (list != null) {
            list.remove(rVar);
        }
    }

    @Override // android.view.ViewGroup, android.view.ViewParent
    public View focusSearch(View view, int i2) {
        View view2;
        boolean z2;
        View Y0 = this.q0.Y0(view, i2);
        if (Y0 != null) {
            return Y0;
        }
        boolean z3 = (this.p0 == null || this.q0 == null || y0() || this.C0) ? false : true;
        FocusFinder focusFinder = FocusFinder.getInstance();
        if (z3 && (i2 == 2 || i2 == 1)) {
            if (this.q0.w()) {
                int i3 = i2 == 2 ? 130 : 33;
                z2 = focusFinder.findNextFocus(this, view, i3) == null;
                if (I1) {
                    i2 = i3;
                }
            } else {
                z2 = false;
            }
            if (!z2 && this.q0.v()) {
                int i4 = (this.q0.k0() == 1) ^ (i2 == 2) ? 66 : 17;
                boolean z4 = focusFinder.findNextFocus(this, view, i4) == null;
                if (I1) {
                    i2 = i4;
                }
                z2 = z4;
            }
            if (z2) {
                v();
                if (T(view) == null) {
                    return null;
                }
                x1();
                this.q0.R0(view, i2, this.f0, this.j1);
                z1(false);
            }
            view2 = focusFinder.findNextFocus(this, view, i2);
        } else {
            View findNextFocus = focusFinder.findNextFocus(this, view, i2);
            if (findNextFocus == null && z3) {
                v();
                if (T(view) == null) {
                    return null;
                }
                x1();
                view2 = this.q0.R0(view, i2, this.f0, this.j1);
                z1(false);
            } else {
                view2 = findNextFocus;
            }
        }
        if (view2 == null || view2.hasFocusable()) {
            return z0(view, view2, i2) ? view2 : super.focusSearch(view, i2);
        } else if (getFocusedChild() == null) {
            return super.focusSearch(view, i2);
        } else {
            h1(view2, null);
            return view;
        }
    }

    public final void g(a0 a0Var) {
        View view = a0Var.itemView;
        boolean z2 = view.getParent() == this;
        this.f0.J(h0(view));
        if (a0Var.isTmpDetached()) {
            this.i0.c(view, -1, view.getLayoutParams(), true);
        } else if (!z2) {
            this.i0.b(view, true);
        } else {
            this.i0.k(view);
        }
    }

    public int g0(View view) {
        a0 i0 = i0(view);
        if (i0 != null) {
            return i0.getLayoutPosition();
        }
        return -1;
    }

    public void g1() {
        a0 a0Var;
        int g2 = this.i0.g();
        for (int i2 = 0; i2 < g2; i2++) {
            View f2 = this.i0.f(i2);
            a0 h0 = h0(f2);
            if (h0 != null && (a0Var = h0.mShadowingHolder) != null) {
                View view = a0Var.itemView;
                int left = f2.getLeft();
                int top = f2.getTop();
                if (left != view.getLeft() || top != view.getTop()) {
                    view.layout(left, top, view.getWidth() + left, view.getHeight() + top);
                }
            }
        }
    }

    @Override // android.view.ViewGroup
    public ViewGroup.LayoutParams generateDefaultLayoutParams() {
        LayoutManager layoutManager = this.q0;
        if (layoutManager != null) {
            return layoutManager.O();
        }
        throw new IllegalStateException("RecyclerView has no LayoutManager" + Q());
    }

    @Override // android.view.ViewGroup
    public ViewGroup.LayoutParams generateLayoutParams(AttributeSet attributeSet) {
        LayoutManager layoutManager = this.q0;
        if (layoutManager != null) {
            return layoutManager.P(getContext(), attributeSet);
        }
        throw new IllegalStateException("RecyclerView has no LayoutManager" + Q());
    }

    @Override // android.view.ViewGroup, android.view.View
    public CharSequence getAccessibilityClassName() {
        return "androidx.recyclerview.widget.RecyclerView";
    }

    public Adapter getAdapter() {
        return this.p0;
    }

    @Override // android.view.View
    public int getBaseline() {
        LayoutManager layoutManager = this.q0;
        if (layoutManager != null) {
            return layoutManager.R();
        }
        return super.getBaseline();
    }

    @Override // android.view.ViewGroup
    public int getChildDrawingOrder(int i2, int i3) {
        j jVar = this.r1;
        if (jVar == null) {
            return super.getChildDrawingOrder(i2, i3);
        }
        return jVar.a(i2, i3);
    }

    @Override // android.view.ViewGroup
    public boolean getClipToPadding() {
        return this.k0;
    }

    public androidx.recyclerview.widget.s getCompatAccessibilityDelegate() {
        return this.q1;
    }

    public k getEdgeEffectFactory() {
        return this.M0;
    }

    public l getItemAnimator() {
        return this.R0;
    }

    public int getItemDecorationCount() {
        return this.t0.size();
    }

    public LayoutManager getLayoutManager() {
        return this.q0;
    }

    public int getMaxFlingVelocity() {
        return this.c1;
    }

    public int getMinFlingVelocity() {
        return this.b1;
    }

    /* JADX INFO: Access modifiers changed from: package-private */
    public long getNanoTime() {
        if (H1) {
            return System.nanoTime();
        }
        return 0L;
    }

    public p getOnFlingListener() {
        return this.a1;
    }

    public boolean getPreserveFocusAfterLayout() {
        return this.f1;
    }

    public s getRecycledViewPool() {
        return this.f0.i();
    }

    public int getScrollState() {
        return this.S0;
    }

    public void h(n nVar) {
        i(nVar, -1);
    }

    public a0 h0(View view) {
        ViewParent parent = view.getParent();
        if (parent != null && parent != this) {
            throw new IllegalArgumentException("View " + view + " is not a direct child of " + this);
        }
        return i0(view);
    }

    public final void h1(View view, View view2) {
        View view3 = view2 != null ? view2 : view;
        this.m0.set(0, 0, view3.getWidth(), view3.getHeight());
        ViewGroup.LayoutParams layoutParams = view3.getLayoutParams();
        if (layoutParams instanceof LayoutParams) {
            LayoutParams layoutParams2 = (LayoutParams) layoutParams;
            if (!layoutParams2.g0) {
                Rect rect = layoutParams2.f0;
                Rect rect2 = this.m0;
                rect2.left -= rect.left;
                rect2.right += rect.right;
                rect2.top -= rect.top;
                rect2.bottom += rect.bottom;
            }
        }
        if (view2 != null) {
            offsetDescendantRectToMyCoords(view2, this.m0);
            offsetRectIntoDescendantCoords(view, this.m0);
        }
        this.q0.B1(this, view, this.m0, !this.z0, view2 == null);
    }

    @Override // android.view.View
    public boolean hasNestedScrollingParent() {
        return getScrollingChildHelper().j();
    }

    public void i(n nVar, int i2) {
        LayoutManager layoutManager = this.q0;
        if (layoutManager != null) {
            layoutManager.r("Cannot add item decoration during a scroll  or layout");
        }
        if (this.t0.isEmpty()) {
            setWillNotDraw(false);
        }
        if (i2 < 0) {
            this.t0.add(nVar);
        } else {
            this.t0.add(i2, nVar);
        }
        B0();
        requestLayout();
    }

    public final void i1() {
        x xVar = this.j1;
        xVar.n = -1L;
        xVar.m = -1;
        xVar.o = -1;
    }

    @Override // android.view.View
    public boolean isAttachedToWindow() {
        return this.w0;
    }

    @Override // android.view.ViewGroup
    public final boolean isLayoutSuppressed() {
        return this.C0;
    }

    @Override // android.view.View, defpackage.pe2
    public boolean isNestedScrollingEnabled() {
        return getScrollingChildHelper().l();
    }

    public void j(o oVar) {
        if (this.H0 == null) {
            this.H0 = new ArrayList();
        }
        this.H0.add(oVar);
    }

    public void j0(View view, Rect rect) {
        k0(view, rect);
    }

    public final void j1() {
        VelocityTracker velocityTracker = this.U0;
        if (velocityTracker != null) {
            velocityTracker.clear();
        }
        A1(0);
        Y0();
    }

    public void k(q qVar) {
        this.u0.add(qVar);
    }

    public final void k1() {
        int absoluteAdapterPosition;
        View focusedChild = (this.f1 && hasFocus() && this.p0 != null) ? getFocusedChild() : null;
        a0 U = focusedChild != null ? U(focusedChild) : null;
        if (U == null) {
            i1();
            return;
        }
        this.j1.n = this.p0.hasStableIds() ? U.getItemId() : -1L;
        x xVar = this.j1;
        if (this.I0) {
            absoluteAdapterPosition = -1;
        } else {
            absoluteAdapterPosition = U.isRemoved() ? U.mOldPosition : U.getAbsoluteAdapterPosition();
        }
        xVar.m = absoluteAdapterPosition;
        this.j1.o = l0(U.itemView);
    }

    public void l(r rVar) {
        if (this.l1 == null) {
            this.l1 = new ArrayList();
        }
        this.l1.add(rVar);
    }

    public final int l0(View view) {
        int id = view.getId();
        while (!view.isFocused() && (view instanceof ViewGroup) && view.hasFocus()) {
            view = ((ViewGroup) view).getFocusedChild();
            if (view.getId() != -1) {
                id = view.getId();
            }
        }
        return id;
    }

    public void l1() {
        int j2 = this.i0.j();
        for (int i2 = 0; i2 < j2; i2++) {
            a0 i0 = i0(this.i0.i(i2));
            if (!i0.shouldIgnore()) {
                i0.saveOldPosition();
            }
        }
    }

    public void m(a0 a0Var, l.c cVar, l.c cVar2) {
        a0Var.setIsRecyclable(false);
        if (this.R0.animateAppearance(a0Var, cVar, cVar2)) {
            R0();
        }
    }

    public final String m0(Context context, String str) {
        if (str.charAt(0) == '.') {
            return context.getPackageName() + str;
        } else if (str.contains(".")) {
            return str;
        } else {
            return RecyclerView.class.getPackage().getName() + '.' + str;
        }
    }

    public boolean m1(int i2, int i3, MotionEvent motionEvent, int i4) {
        int i5;
        int i6;
        int i7;
        int i8;
        v();
        if (this.p0 != null) {
            int[] iArr = this.w1;
            iArr[0] = 0;
            iArr[1] = 0;
            n1(i2, i3, iArr);
            int[] iArr2 = this.w1;
            int i9 = iArr2[0];
            int i10 = iArr2[1];
            i5 = i10;
            i6 = i9;
            i7 = i2 - i9;
            i8 = i3 - i10;
        } else {
            i5 = 0;
            i6 = 0;
            i7 = 0;
            i8 = 0;
        }
        if (!this.t0.isEmpty()) {
            invalidate();
        }
        int[] iArr3 = this.w1;
        iArr3[0] = 0;
        iArr3[1] = 0;
        H(i6, i5, i7, i8, this.u1, i4, iArr3);
        int[] iArr4 = this.w1;
        int i11 = i7 - iArr4[0];
        int i12 = i8 - iArr4[1];
        boolean z2 = (iArr4[0] == 0 && iArr4[1] == 0) ? false : true;
        int i13 = this.X0;
        int[] iArr5 = this.u1;
        this.X0 = i13 - iArr5[0];
        this.Y0 -= iArr5[1];
        int[] iArr6 = this.v1;
        iArr6[0] = iArr6[0] + iArr5[0];
        iArr6[1] = iArr6[1] + iArr5[1];
        if (getOverScrollMode() != 2) {
            if (motionEvent != null && !v92.a(motionEvent, 8194)) {
                V0(motionEvent.getX(), i11, motionEvent.getY(), i12);
            }
            u(i2, i3);
        }
        if (i6 != 0 || i5 != 0) {
            J(i6, i5);
        }
        if (!awakenScrollBars()) {
            invalidate();
        }
        return (!z2 && i6 == 0 && i5 == 0) ? false : true;
    }

    public final void n(a0 a0Var, a0 a0Var2, l.c cVar, l.c cVar2, boolean z2, boolean z3) {
        a0Var.setIsRecyclable(false);
        if (z2) {
            g(a0Var);
        }
        if (a0Var != a0Var2) {
            if (z3) {
                g(a0Var2);
            }
            a0Var.mShadowedHolder = a0Var2;
            g(a0Var);
            this.f0.J(a0Var);
            a0Var2.setIsRecyclable(false);
            a0Var2.mShadowingHolder = a0Var;
        }
        if (this.R0.animateChange(a0Var, a0Var2, cVar, cVar2)) {
            R0();
        }
    }

    public Rect n0(View view) {
        LayoutParams layoutParams = (LayoutParams) view.getLayoutParams();
        if (!layoutParams.g0) {
            return layoutParams.f0;
        }
        if (this.j1.e() && (layoutParams.b() || layoutParams.d())) {
            return layoutParams.f0;
        }
        Rect rect = layoutParams.f0;
        rect.set(0, 0, 0, 0);
        int size = this.t0.size();
        for (int i2 = 0; i2 < size; i2++) {
            this.m0.set(0, 0, 0, 0);
            this.t0.get(i2).getItemOffsets(this.m0, view, this, this.j1);
            int i3 = rect.left;
            Rect rect2 = this.m0;
            rect.left = i3 + rect2.left;
            rect.top += rect2.top;
            rect.right += rect2.right;
            rect.bottom += rect2.bottom;
        }
        layoutParams.g0 = false;
        return rect;
    }

    public void n1(int i2, int i3, int[] iArr) {
        x1();
        L0();
        u74.a("RV Scroll");
        R(this.j1);
        int F12 = i2 != 0 ? this.q0.F1(i2, this.f0, this.j1) : 0;
        int H12 = i3 != 0 ? this.q0.H1(i3, this.f0, this.j1) : 0;
        u74.b();
        g1();
        M0();
        z1(false);
        if (iArr != null) {
            iArr[0] = F12;
            iArr[1] = H12;
        }
    }

    public void o(a0 a0Var, l.c cVar, l.c cVar2) {
        g(a0Var);
        a0Var.setIsRecyclable(false);
        if (this.R0.animateDisappearance(a0Var, cVar, cVar2)) {
            R0();
        }
    }

    public n o0(int i2) {
        int itemDecorationCount = getItemDecorationCount();
        if (i2 >= 0 && i2 < itemDecorationCount) {
            return this.t0.get(i2);
        }
        throw new IndexOutOfBoundsException(i2 + " is an invalid index for size " + itemDecorationCount);
    }

    public void o1(int i2) {
        if (this.C0) {
            return;
        }
        B1();
        LayoutManager layoutManager = this.q0;
        if (layoutManager == null) {
            return;
        }
        layoutManager.G1(i2);
        awakenScrollBars();
    }

    @Override // android.view.ViewGroup, android.view.View
    public void onAttachedToWindow() {
        super.onAttachedToWindow();
        this.K0 = 0;
        boolean z2 = true;
        this.w0 = true;
        if (!this.z0 || isLayoutRequested()) {
            z2 = false;
        }
        this.z0 = z2;
        LayoutManager layoutManager = this.q0;
        if (layoutManager != null) {
            layoutManager.K(this);
        }
        this.p1 = false;
        if (H1) {
            ThreadLocal<androidx.recyclerview.widget.j> threadLocal = androidx.recyclerview.widget.j.i0;
            androidx.recyclerview.widget.j jVar = threadLocal.get();
            this.h1 = jVar;
            if (jVar == null) {
                this.h1 = new androidx.recyclerview.widget.j();
                Display x2 = ei4.x(this);
                float f2 = 60.0f;
                if (!isInEditMode() && x2 != null) {
                    float refreshRate = x2.getRefreshRate();
                    if (refreshRate >= 30.0f) {
                        f2 = refreshRate;
                    }
                }
                androidx.recyclerview.widget.j jVar2 = this.h1;
                jVar2.g0 = 1.0E9f / f2;
                threadLocal.set(jVar2);
            }
            this.h1.a(this);
        }
    }

    @Override // android.view.ViewGroup, android.view.View
    public void onDetachedFromWindow() {
        androidx.recyclerview.widget.j jVar;
        super.onDetachedFromWindow();
        l lVar = this.R0;
        if (lVar != null) {
            lVar.endAnimations();
        }
        B1();
        this.w0 = false;
        LayoutManager layoutManager = this.q0;
        if (layoutManager != null) {
            layoutManager.L(this, this.f0);
        }
        this.x1.clear();
        removeCallbacks(this.y1);
        this.j0.j();
        if (!H1 || (jVar = this.h1) == null) {
            return;
        }
        jVar.j(this);
        this.h1 = null;
    }

    @Override // android.view.View
    public void onDraw(Canvas canvas) {
        super.onDraw(canvas);
        int size = this.t0.size();
        for (int i2 = 0; i2 < size; i2++) {
            this.t0.get(i2).onDraw(canvas, this, this.j1);
        }
    }

    /* JADX WARN: Removed duplicated region for block: B:31:0x0068  */
    @Override // android.view.View
    /*
        Code decompiled incorrectly, please refer to instructions dump.
        To view partially-correct code enable 'Show inconsistent code' option in preferences
    */
    public boolean onGenericMotionEvent(android.view.MotionEvent r6) {
        /*
            r5 = this;
            androidx.recyclerview.widget.RecyclerView$LayoutManager r0 = r5.q0
            r1 = 0
            if (r0 != 0) goto L6
            return r1
        L6:
            boolean r0 = r5.C0
            if (r0 == 0) goto Lb
            return r1
        Lb:
            int r0 = r6.getAction()
            r2 = 8
            if (r0 != r2) goto L78
            int r0 = r6.getSource()
            r0 = r0 & 2
            r2 = 0
            if (r0 == 0) goto L3e
            androidx.recyclerview.widget.RecyclerView$LayoutManager r0 = r5.q0
            boolean r0 = r0.w()
            if (r0 == 0) goto L2c
            r0 = 9
            float r0 = r6.getAxisValue(r0)
            float r0 = -r0
            goto L2d
        L2c:
            r0 = r2
        L2d:
            androidx.recyclerview.widget.RecyclerView$LayoutManager r3 = r5.q0
            boolean r3 = r3.v()
            if (r3 == 0) goto L3c
            r3 = 10
            float r3 = r6.getAxisValue(r3)
            goto L64
        L3c:
            r3 = r2
            goto L64
        L3e:
            int r0 = r6.getSource()
            r3 = 4194304(0x400000, float:5.877472E-39)
            r0 = r0 & r3
            if (r0 == 0) goto L62
            r0 = 26
            float r0 = r6.getAxisValue(r0)
            androidx.recyclerview.widget.RecyclerView$LayoutManager r3 = r5.q0
            boolean r3 = r3.w()
            if (r3 == 0) goto L57
            float r0 = -r0
            goto L3c
        L57:
            androidx.recyclerview.widget.RecyclerView$LayoutManager r3 = r5.q0
            boolean r3 = r3.v()
            if (r3 == 0) goto L62
            r3 = r0
            r0 = r2
            goto L64
        L62:
            r0 = r2
            r3 = r0
        L64:
            int r4 = (r0 > r2 ? 1 : (r0 == r2 ? 0 : -1))
            if (r4 != 0) goto L6c
            int r2 = (r3 > r2 ? 1 : (r3 == r2 ? 0 : -1))
            if (r2 == 0) goto L78
        L6c:
            float r2 = r5.d1
            float r3 = r3 * r2
            int r2 = (int) r3
            float r3 = r5.e1
            float r0 = r0 * r3
            int r0 = (int) r0
            r3 = 1
            r5.D0(r2, r0, r6, r3)
        L78:
            return r1
        */
        throw new UnsupportedOperationException("Method not decompiled: androidx.recyclerview.widget.RecyclerView.onGenericMotionEvent(android.view.MotionEvent):boolean");
    }

    @Override // android.view.ViewGroup
    public boolean onInterceptTouchEvent(MotionEvent motionEvent) {
        boolean z2;
        if (this.C0) {
            return false;
        }
        this.v0 = null;
        if (V(motionEvent)) {
            r();
            return true;
        }
        LayoutManager layoutManager = this.q0;
        if (layoutManager == null) {
            return false;
        }
        boolean v2 = layoutManager.v();
        boolean w2 = this.q0.w();
        if (this.U0 == null) {
            this.U0 = VelocityTracker.obtain();
        }
        this.U0.addMovement(motionEvent);
        int actionMasked = motionEvent.getActionMasked();
        int actionIndex = motionEvent.getActionIndex();
        if (actionMasked == 0) {
            if (this.D0) {
                this.D0 = false;
            }
            this.T0 = motionEvent.getPointerId(0);
            int x2 = (int) (motionEvent.getX() + 0.5f);
            this.X0 = x2;
            this.V0 = x2;
            int y2 = (int) (motionEvent.getY() + 0.5f);
            this.Y0 = y2;
            this.W0 = y2;
            if (this.S0 == 2) {
                getParent().requestDisallowInterceptTouchEvent(true);
                setScrollState(1);
                A1(1);
            }
            int[] iArr = this.v1;
            iArr[1] = 0;
            iArr[0] = 0;
            int i2 = v2;
            if (w2) {
                i2 = (v2 ? 1 : 0) | 2;
            }
            y1(i2, 0);
        } else if (actionMasked == 1) {
            this.U0.clear();
            A1(0);
        } else if (actionMasked == 2) {
            int findPointerIndex = motionEvent.findPointerIndex(this.T0);
            if (findPointerIndex < 0) {
                StringBuilder sb = new StringBuilder();
                sb.append("Error processing scroll; pointer index for id ");
                sb.append(this.T0);
                sb.append(" not found. Did any MotionEvents get skipped?");
                return false;
            }
            int x3 = (int) (motionEvent.getX(findPointerIndex) + 0.5f);
            int y3 = (int) (motionEvent.getY(findPointerIndex) + 0.5f);
            if (this.S0 != 1) {
                int i3 = x3 - this.V0;
                int i4 = y3 - this.W0;
                if (!v2 || Math.abs(i3) <= this.Z0) {
                    z2 = false;
                } else {
                    this.X0 = x3;
                    z2 = true;
                }
                if (w2 && Math.abs(i4) > this.Z0) {
                    this.Y0 = y3;
                    z2 = true;
                }
                if (z2) {
                    setScrollState(1);
                }
            }
        } else if (actionMasked == 3) {
            r();
        } else if (actionMasked == 5) {
            this.T0 = motionEvent.getPointerId(actionIndex);
            int x4 = (int) (motionEvent.getX(actionIndex) + 0.5f);
            this.X0 = x4;
            this.V0 = x4;
            int y4 = (int) (motionEvent.getY(actionIndex) + 0.5f);
            this.Y0 = y4;
            this.W0 = y4;
        } else if (actionMasked == 6) {
            O0(motionEvent);
        }
        return this.S0 == 1;
    }

    @Override // android.view.ViewGroup, android.view.View
    public void onLayout(boolean z2, int i2, int i3, int i4, int i5) {
        u74.a("RV OnLayout");
        C();
        u74.b();
        this.z0 = true;
    }

    @Override // android.view.View
    public void onMeasure(int i2, int i3) {
        LayoutManager layoutManager = this.q0;
        if (layoutManager == null) {
            x(i2, i3);
            return;
        }
        boolean z2 = false;
        if (layoutManager.z0()) {
            int mode = View.MeasureSpec.getMode(i2);
            int mode2 = View.MeasureSpec.getMode(i3);
            this.q0.h1(this.f0, this.j1, i2, i3);
            if (mode == 1073741824 && mode2 == 1073741824) {
                z2 = true;
            }
            this.z1 = z2;
            if (z2 || this.p0 == null) {
                return;
            }
            if (this.j1.e == 1) {
                D();
            }
            this.q0.J1(i2, i3);
            this.j1.j = true;
            E();
            this.q0.M1(i2, i3);
            if (this.q0.P1()) {
                this.q0.J1(View.MeasureSpec.makeMeasureSpec(getMeasuredWidth(), 1073741824), View.MeasureSpec.makeMeasureSpec(getMeasuredHeight(), 1073741824));
                this.j1.j = true;
                E();
                this.q0.M1(i2, i3);
            }
            this.A1 = getMeasuredWidth();
            this.B1 = getMeasuredHeight();
        } else if (this.x0) {
            this.q0.h1(this.f0, this.j1, i2, i3);
        } else {
            if (this.F0) {
                x1();
                L0();
                T0();
                M0();
                x xVar = this.j1;
                if (xVar.l) {
                    xVar.h = true;
                } else {
                    this.h0.j();
                    this.j1.h = false;
                }
                this.F0 = false;
                z1(false);
            } else if (this.j1.l) {
                setMeasuredDimension(getMeasuredWidth(), getMeasuredHeight());
                return;
            }
            Adapter adapter = this.p0;
            if (adapter != null) {
                this.j1.f = adapter.getItemCount();
            } else {
                this.j1.f = 0;
            }
            x1();
            this.q0.h1(this.f0, this.j1, i2, i3);
            z1(false);
            this.j1.h = false;
        }
    }

    @Override // android.view.ViewGroup
    public boolean onRequestFocusInDescendants(int i2, Rect rect) {
        if (y0()) {
            return false;
        }
        return super.onRequestFocusInDescendants(i2, rect);
    }

    @Override // android.view.View
    public void onRestoreInstanceState(Parcelable parcelable) {
        if (!(parcelable instanceof SavedState)) {
            super.onRestoreInstanceState(parcelable);
            return;
        }
        SavedState savedState = (SavedState) parcelable;
        this.g0 = savedState;
        super.onRestoreInstanceState(savedState.a());
        requestLayout();
    }

    @Override // android.view.View
    public Parcelable onSaveInstanceState() {
        SavedState savedState = new SavedState(super.onSaveInstanceState());
        SavedState savedState2 = this.g0;
        if (savedState2 != null) {
            savedState.b(savedState2);
        } else {
            LayoutManager layoutManager = this.q0;
            if (layoutManager != null) {
                savedState.g0 = layoutManager.l1();
            } else {
                savedState.g0 = null;
            }
        }
        return savedState;
    }

    @Override // android.view.View
    public void onSizeChanged(int i2, int i3, int i4, int i5) {
        super.onSizeChanged(i2, i3, i4, i5);
        if (i2 == i4 && i3 == i5) {
            return;
        }
        w0();
    }

    /* JADX WARN: Removed duplicated region for block: B:49:0x00d9  */
    /* JADX WARN: Removed duplicated region for block: B:55:0x00ef  */
    @Override // android.view.View
    /*
        Code decompiled incorrectly, please refer to instructions dump.
        To view partially-correct code enable 'Show inconsistent code' option in preferences
    */
    public boolean onTouchEvent(android.view.MotionEvent r18) {
        /*
            Method dump skipped, instructions count: 471
            To view this dump change 'Code comments level' option to 'DEBUG'
        */
        throw new UnsupportedOperationException("Method not decompiled: androidx.recyclerview.widget.RecyclerView.onTouchEvent(android.view.MotionEvent):boolean");
    }

    public void p(String str) {
        if (y0()) {
            if (str == null) {
                throw new IllegalStateException("Cannot call this method while RecyclerView is computing a layout or scrolling" + Q());
            }
            throw new IllegalStateException(str);
        } else if (this.L0 > 0) {
            new IllegalStateException("" + Q());
        }
    }

    public final void p0(long j2, a0 a0Var, a0 a0Var2) {
        int g2 = this.i0.g();
        for (int i2 = 0; i2 < g2; i2++) {
            a0 i0 = i0(this.i0.f(i2));
            if (i0 != a0Var && e0(i0) == j2) {
                Adapter adapter = this.p0;
                if (adapter != null && adapter.hasStableIds()) {
                    throw new IllegalStateException("Two different ViewHolders have the same stable ID. Stable IDs in your adapter MUST BE unique and SHOULD NOT change.\n ViewHolder 1:" + i0 + " \n View Holder 2:" + a0Var + Q());
                }
                throw new IllegalStateException("Two different ViewHolders have the same change ID. This might happen due to inconsistent Adapter update events or if the LayoutManager lays out the same View multiple times.\n ViewHolder 1:" + i0 + " \n View Holder 2:" + a0Var + Q());
            }
        }
        StringBuilder sb = new StringBuilder();
        sb.append("Problem while matching changed view holders with the newones. The pre-layout information for the change holder ");
        sb.append(a0Var2);
        sb.append(" cannot be found but it is necessary for ");
        sb.append(a0Var);
        sb.append(Q());
    }

    public final void p1(Adapter adapter, boolean z2, boolean z3) {
        Adapter adapter2 = this.p0;
        if (adapter2 != null) {
            adapter2.unregisterAdapterDataObserver(this.a);
            this.p0.onDetachedFromRecyclerView(this);
        }
        if (!z2 || z3) {
            Z0();
        }
        this.h0.y();
        Adapter adapter3 = this.p0;
        this.p0 = adapter;
        if (adapter != null) {
            adapter.registerAdapterDataObserver(this.a);
            adapter.onAttachedToRecyclerView(this);
        }
        LayoutManager layoutManager = this.q0;
        if (layoutManager != null) {
            layoutManager.M0(adapter3, this.p0);
        }
        this.f0.x(adapter3, this.p0, z2);
        this.j1.g = true;
    }

    public boolean q(a0 a0Var) {
        l lVar = this.R0;
        return lVar == null || lVar.canReuseUpdatedViewHolder(a0Var, a0Var.getUnmodifiedPayloads());
    }

    public boolean q0() {
        return !this.z0 || this.I0 || this.h0.p();
    }

    public boolean q1(a0 a0Var, int i2) {
        if (y0()) {
            a0Var.mPendingAccessibilityState = i2;
            this.x1.add(a0Var);
            return false;
        }
        ei4.D0(a0Var.itemView, i2);
        return true;
    }

    public final void r() {
        j1();
        setScrollState(0);
    }

    public final boolean r0() {
        int g2 = this.i0.g();
        for (int i2 = 0; i2 < g2; i2++) {
            a0 i0 = i0(this.i0.f(i2));
            if (i0 != null && !i0.shouldIgnore() && i0.isUpdated()) {
                return true;
            }
        }
        return false;
    }

    public boolean r1(AccessibilityEvent accessibilityEvent) {
        if (y0()) {
            int a2 = accessibilityEvent != null ? a6.a(accessibilityEvent) : 0;
            this.E0 |= a2 != 0 ? a2 : 0;
            return true;
        }
        return false;
    }

    @Override // android.view.ViewGroup
    public void removeDetachedView(View view, boolean z2) {
        a0 i0 = i0(view);
        if (i0 != null) {
            if (i0.isTmpDetached()) {
                i0.clearTmpDetachFlag();
            } else if (!i0.shouldIgnore()) {
                throw new IllegalArgumentException("Called removeDetachedView with a view which is not flagged as tmp detached." + i0 + Q());
            }
        }
        view.clearAnimation();
        A(view);
        super.removeDetachedView(view, z2);
    }

    @Override // android.view.ViewGroup, android.view.ViewParent
    public void requestChildFocus(View view, View view2) {
        if (!this.q0.j1(this, this.j1, view, view2) && view2 != null) {
            h1(view, view2);
        }
        super.requestChildFocus(view, view2);
    }

    @Override // android.view.ViewGroup, android.view.ViewParent
    public boolean requestChildRectangleOnScreen(View view, Rect rect, boolean z2) {
        return this.q0.A1(this, view, rect, z2);
    }

    @Override // android.view.ViewGroup, android.view.ViewParent
    public void requestDisallowInterceptTouchEvent(boolean z2) {
        int size = this.u0.size();
        for (int i2 = 0; i2 < size; i2++) {
            this.u0.get(i2).e(z2);
        }
        super.requestDisallowInterceptTouchEvent(z2);
    }

    @Override // android.view.View, android.view.ViewParent
    public void requestLayout() {
        if (this.A0 == 0 && !this.C0) {
            super.requestLayout();
        } else {
            this.B0 = true;
        }
    }

    public void s0() {
        this.h0 = new androidx.recyclerview.widget.a(new f());
    }

    public void s1(int i2, int i3) {
        t1(i2, i3, null);
    }

    @Override // android.view.View
    public void scrollBy(int i2, int i3) {
        LayoutManager layoutManager = this.q0;
        if (layoutManager == null || this.C0) {
            return;
        }
        boolean v2 = layoutManager.v();
        boolean w2 = this.q0.w();
        if (v2 || w2) {
            if (!v2) {
                i2 = 0;
            }
            if (!w2) {
                i3 = 0;
            }
            m1(i2, i3, null, 0);
        }
    }

    @Override // android.view.View
    public void scrollTo(int i2, int i3) {
    }

    @Override // android.view.View, android.view.accessibility.AccessibilityEventSource
    public void sendAccessibilityEventUnchecked(AccessibilityEvent accessibilityEvent) {
        if (r1(accessibilityEvent)) {
            return;
        }
        super.sendAccessibilityEventUnchecked(accessibilityEvent);
    }

    public void setAccessibilityDelegateCompat(androidx.recyclerview.widget.s sVar) {
        this.q1 = sVar;
        ei4.t0(this, sVar);
    }

    public void setAdapter(Adapter adapter) {
        setLayoutFrozen(false);
        p1(adapter, false, true);
        U0(false);
        requestLayout();
    }

    public void setChildDrawingOrderCallback(j jVar) {
        if (jVar == this.r1) {
            return;
        }
        this.r1 = jVar;
        setChildrenDrawingOrderEnabled(jVar != null);
    }

    @Override // android.view.ViewGroup
    public void setClipToPadding(boolean z2) {
        if (z2 != this.k0) {
            w0();
        }
        this.k0 = z2;
        super.setClipToPadding(z2);
        if (this.z0) {
            requestLayout();
        }
    }

    public void setEdgeEffectFactory(k kVar) {
        du2.e(kVar);
        this.M0 = kVar;
        w0();
    }

    public void setHasFixedSize(boolean z2) {
        this.x0 = z2;
    }

    public void setItemAnimator(l lVar) {
        l lVar2 = this.R0;
        if (lVar2 != null) {
            lVar2.endAnimations();
            this.R0.setListener(null);
        }
        this.R0 = lVar;
        if (lVar != null) {
            lVar.setListener(this.o1);
        }
    }

    public void setItemViewCacheSize(int i2) {
        this.f0.G(i2);
    }

    @Deprecated
    public void setLayoutFrozen(boolean z2) {
        suppressLayout(z2);
    }

    public void setLayoutManager(LayoutManager layoutManager) {
        if (layoutManager == this.q0) {
            return;
        }
        B1();
        if (this.q0 != null) {
            l lVar = this.R0;
            if (lVar != null) {
                lVar.endAnimations();
            }
            this.q0.t1(this.f0);
            this.q0.u1(this.f0);
            this.f0.c();
            if (this.w0) {
                this.q0.L(this, this.f0);
            }
            this.q0.N1(null);
            this.q0 = null;
        } else {
            this.f0.c();
        }
        this.i0.o();
        this.q0 = layoutManager;
        if (layoutManager != null) {
            if (layoutManager.f0 == null) {
                layoutManager.N1(this);
                if (this.w0) {
                    this.q0.K(this);
                }
            } else {
                throw new IllegalArgumentException("LayoutManager " + layoutManager + " is already attached to a RecyclerView:" + layoutManager.f0.Q());
            }
        }
        this.f0.K();
        requestLayout();
    }

    @Override // android.view.ViewGroup
    @Deprecated
    public void setLayoutTransition(LayoutTransition layoutTransition) {
        if (Build.VERSION.SDK_INT < 18) {
            if (layoutTransition == null) {
                suppressLayout(false);
                return;
            } else if (layoutTransition.getAnimator(0) == null && layoutTransition.getAnimator(1) == null && layoutTransition.getAnimator(2) == null && layoutTransition.getAnimator(3) == null && layoutTransition.getAnimator(4) == null) {
                suppressLayout(true);
                return;
            }
        }
        if (layoutTransition == null) {
            super.setLayoutTransition(null);
            return;
        }
        throw new IllegalArgumentException("Providing a LayoutTransition into RecyclerView is not supported. Please use setItemAnimator() instead for animating changes to the items in this RecyclerView");
    }

    @Override // android.view.View
    public void setNestedScrollingEnabled(boolean z2) {
        getScrollingChildHelper().m(z2);
    }

    public void setOnFlingListener(p pVar) {
        this.a1 = pVar;
    }

    @Deprecated
    public void setOnScrollListener(r rVar) {
        this.k1 = rVar;
    }

    public void setPreserveFocusAfterLayout(boolean z2) {
        this.f1 = z2;
    }

    public void setRecycledViewPool(s sVar) {
        this.f0.E(sVar);
    }

    @Deprecated
    public void setRecyclerListener(u uVar) {
        this.r0 = uVar;
    }

    void setScrollState(int i2) {
        if (i2 == this.S0) {
            return;
        }
        this.S0 = i2;
        if (i2 != 2) {
            C1();
        }
        I(i2);
    }

    public void setScrollingTouchSlop(int i2) {
        ViewConfiguration viewConfiguration = ViewConfiguration.get(getContext());
        if (i2 != 0) {
            if (i2 != 1) {
                StringBuilder sb = new StringBuilder();
                sb.append("setScrollingTouchSlop(): bad argument constant ");
                sb.append(i2);
                sb.append("; using default value");
            } else {
                this.Z0 = viewConfiguration.getScaledPagingTouchSlop();
                return;
            }
        }
        this.Z0 = viewConfiguration.getScaledTouchSlop();
    }

    public void setViewCacheExtension(y yVar) {
        this.f0.F(yVar);
    }

    @Override // android.view.View
    public boolean startNestedScroll(int i2) {
        return getScrollingChildHelper().o(i2);
    }

    @Override // android.view.View, defpackage.pe2
    public void stopNestedScroll() {
        getScrollingChildHelper().q();
    }

    @Override // android.view.ViewGroup
    public final void suppressLayout(boolean z2) {
        if (z2 != this.C0) {
            p("Do not suppressLayout in layout or scroll");
            if (!z2) {
                this.C0 = false;
                if (this.B0 && this.q0 != null && this.p0 != null) {
                    requestLayout();
                }
                this.B0 = false;
                return;
            }
            long uptimeMillis = SystemClock.uptimeMillis();
            onTouchEvent(MotionEvent.obtain(uptimeMillis, uptimeMillis, 3, Utils.FLOAT_EPSILON, Utils.FLOAT_EPSILON, 0));
            this.C0 = true;
            this.D0 = true;
            B1();
        }
    }

    public void t() {
        int j2 = this.i0.j();
        for (int i2 = 0; i2 < j2; i2++) {
            a0 i0 = i0(this.i0.i(i2));
            if (!i0.shouldIgnore()) {
                i0.clearOldPosition();
            }
        }
        this.f0.d();
    }

    @SuppressLint({"InlinedApi"})
    public final void t0() {
        if (ei4.D(this) == 0) {
            ei4.E0(this, 8);
        }
    }

    public void t1(int i2, int i3, Interpolator interpolator) {
        u1(i2, i3, interpolator, Integer.MIN_VALUE);
    }

    public void u(int i2, int i3) {
        boolean z2;
        EdgeEffect edgeEffect = this.N0;
        if (edgeEffect == null || edgeEffect.isFinished() || i2 <= 0) {
            z2 = false;
        } else {
            this.N0.onRelease();
            z2 = this.N0.isFinished();
        }
        EdgeEffect edgeEffect2 = this.P0;
        if (edgeEffect2 != null && !edgeEffect2.isFinished() && i2 < 0) {
            this.P0.onRelease();
            z2 |= this.P0.isFinished();
        }
        EdgeEffect edgeEffect3 = this.O0;
        if (edgeEffect3 != null && !edgeEffect3.isFinished() && i3 > 0) {
            this.O0.onRelease();
            z2 |= this.O0.isFinished();
        }
        EdgeEffect edgeEffect4 = this.Q0;
        if (edgeEffect4 != null && !edgeEffect4.isFinished() && i3 < 0) {
            this.Q0.onRelease();
            z2 |= this.Q0.isFinished();
        }
        if (z2) {
            ei4.j0(this);
        }
    }

    public final void u0() {
        this.i0 = new androidx.recyclerview.widget.e(new e());
    }

    public void u1(int i2, int i3, Interpolator interpolator, int i4) {
        v1(i2, i3, interpolator, i4, false);
    }

    public void v() {
        if (this.z0 && !this.I0) {
            if (this.h0.p()) {
                if (this.h0.o(4) && !this.h0.o(11)) {
                    u74.a("RV PartialInvalidate");
                    x1();
                    L0();
                    this.h0.w();
                    if (!this.B0) {
                        if (r0()) {
                            C();
                        } else {
                            this.h0.i();
                        }
                    }
                    z1(true);
                    M0();
                    u74.b();
                    return;
                } else if (this.h0.p()) {
                    u74.a("RV FullInvalidate");
                    C();
                    u74.b();
                    return;
                } else {
                    return;
                }
            }
            return;
        }
        u74.a("RV FullInvalidate");
        C();
        u74.b();
    }

    public void v0(StateListDrawable stateListDrawable, Drawable drawable, StateListDrawable stateListDrawable2, Drawable drawable2) {
        if (stateListDrawable != null && drawable != null && stateListDrawable2 != null && drawable2 != null) {
            Resources resources = getContext().getResources();
            new androidx.recyclerview.widget.i(this, stateListDrawable, drawable, stateListDrawable2, drawable2, resources.getDimensionPixelSize(hz2.fastscroll_default_thickness), resources.getDimensionPixelSize(hz2.fastscroll_minimum_range), resources.getDimensionPixelOffset(hz2.fastscroll_margin));
            return;
        }
        throw new IllegalArgumentException("Trying to set fast scroller without both required drawables." + Q());
    }

    public void v1(int i2, int i3, Interpolator interpolator, int i4, boolean z2) {
        LayoutManager layoutManager = this.q0;
        if (layoutManager == null || this.C0) {
            return;
        }
        if (!layoutManager.v()) {
            i2 = 0;
        }
        if (!this.q0.w()) {
            i3 = 0;
        }
        if (i2 == 0 && i3 == 0) {
            return;
        }
        if (i4 == Integer.MIN_VALUE || i4 > 0) {
            if (z2) {
                int i5 = i2 != 0 ? 1 : 0;
                if (i3 != 0) {
                    i5 |= 2;
                }
                y1(i5, 1);
            }
            this.g1.e(i2, i3, i4, interpolator);
            return;
        }
        scrollBy(i2, i3);
    }

    public final void w(Context context, String str, AttributeSet attributeSet, int i2, int i3) {
        ClassLoader classLoader;
        Constructor constructor;
        if (str != null) {
            String trim = str.trim();
            if (trim.isEmpty()) {
                return;
            }
            String m0 = m0(context, trim);
            try {
                if (isInEditMode()) {
                    classLoader = getClass().getClassLoader();
                } else {
                    classLoader = context.getClassLoader();
                }
                Class<? extends U> asSubclass = Class.forName(m0, false, classLoader).asSubclass(LayoutManager.class);
                Object[] objArr = null;
                try {
                    constructor = asSubclass.getConstructor(K1);
                    objArr = new Object[]{context, attributeSet, Integer.valueOf(i2), Integer.valueOf(i3)};
                } catch (NoSuchMethodException e2) {
                    try {
                        constructor = asSubclass.getConstructor(new Class[0]);
                    } catch (NoSuchMethodException e3) {
                        e3.initCause(e2);
                        throw new IllegalStateException(attributeSet.getPositionDescription() + ": Error creating LayoutManager " + m0, e3);
                    }
                }
                constructor.setAccessible(true);
                setLayoutManager((LayoutManager) constructor.newInstance(objArr));
            } catch (ClassCastException e4) {
                throw new IllegalStateException(attributeSet.getPositionDescription() + ": Class is not a LayoutManager " + m0, e4);
            } catch (ClassNotFoundException e5) {
                throw new IllegalStateException(attributeSet.getPositionDescription() + ": Unable to find LayoutManager " + m0, e5);
            } catch (IllegalAccessException e6) {
                throw new IllegalStateException(attributeSet.getPositionDescription() + ": Cannot access non-public constructor " + m0, e6);
            } catch (InstantiationException e7) {
                throw new IllegalStateException(attributeSet.getPositionDescription() + ": Could not instantiate the LayoutManager: " + m0, e7);
            } catch (InvocationTargetException e8) {
                throw new IllegalStateException(attributeSet.getPositionDescription() + ": Could not instantiate the LayoutManager: " + m0, e8);
            }
        }
    }

    public void w0() {
        this.Q0 = null;
        this.O0 = null;
        this.P0 = null;
        this.N0 = null;
    }

    public void w1(int i2) {
        LayoutManager layoutManager;
        if (this.C0 || (layoutManager = this.q0) == null) {
            return;
        }
        layoutManager.R1(this, this.j1, i2);
    }

    public void x(int i2, int i3) {
        setMeasuredDimension(LayoutManager.y(i2, getPaddingLeft() + getPaddingRight(), ei4.G(this)), LayoutManager.y(i3, getPaddingTop() + getPaddingBottom(), ei4.F(this)));
    }

    public boolean x0() {
        AccessibilityManager accessibilityManager = this.G0;
        return accessibilityManager != null && accessibilityManager.isEnabled();
    }

    public void x1() {
        int i2 = this.A0 + 1;
        this.A0 = i2;
        if (i2 != 1 || this.C0) {
            return;
        }
        this.B0 = false;
    }

    public final boolean y(int i2, int i3) {
        W(this.s1);
        int[] iArr = this.s1;
        return (iArr[0] == i2 && iArr[1] == i3) ? false : true;
    }

    public boolean y0() {
        return this.K0 > 0;
    }

    public boolean y1(int i2, int i3) {
        return getScrollingChildHelper().p(i2, i3);
    }

    public void z(View view) {
        a0 i0 = i0(view);
        J0(view);
        Adapter adapter = this.p0;
        if (adapter != null && i0 != null) {
            adapter.onViewAttachedToWindow(i0);
        }
        List<o> list = this.H0;
        if (list != null) {
            for (int size = list.size() - 1; size >= 0; size--) {
                this.H0.get(size).d(view);
            }
        }
    }

    public final boolean z0(View view, View view2, int i2) {
        int i3;
        if (view2 == null || view2 == this || view2 == view || T(view2) == null) {
            return false;
        }
        if (view == null || T(view) == null) {
            return true;
        }
        this.m0.set(0, 0, view.getWidth(), view.getHeight());
        this.n0.set(0, 0, view2.getWidth(), view2.getHeight());
        offsetDescendantRectToMyCoords(view, this.m0);
        offsetDescendantRectToMyCoords(view2, this.n0);
        char c2 = 65535;
        int i4 = this.q0.k0() == 1 ? -1 : 1;
        Rect rect = this.m0;
        int i5 = rect.left;
        Rect rect2 = this.n0;
        int i6 = rect2.left;
        if ((i5 < i6 || rect.right <= i6) && rect.right < rect2.right) {
            i3 = 1;
        } else {
            int i7 = rect.right;
            int i8 = rect2.right;
            i3 = ((i7 > i8 || i5 >= i8) && i5 > i6) ? -1 : 0;
        }
        int i9 = rect.top;
        int i10 = rect2.top;
        if ((i9 < i10 || rect.bottom <= i10) && rect.bottom < rect2.bottom) {
            c2 = 1;
        } else {
            int i11 = rect.bottom;
            int i12 = rect2.bottom;
            if ((i11 <= i12 && i9 < i12) || i9 <= i10) {
                c2 = 0;
            }
        }
        if (i2 == 1) {
            return c2 < 0 || (c2 == 0 && i3 * i4 < 0);
        } else if (i2 == 2) {
            return c2 > 0 || (c2 == 0 && i3 * i4 > 0);
        } else if (i2 == 17) {
            return i3 < 0;
        } else if (i2 == 33) {
            return c2 < 0;
        } else if (i2 == 66) {
            return i3 > 0;
        } else if (i2 == 130) {
            return c2 > 0;
        } else {
            throw new IllegalArgumentException("Invalid direction: " + i2 + Q());
        }
    }

    public void z1(boolean z2) {
        if (this.A0 < 1) {
            this.A0 = 1;
        }
        if (!z2 && !this.C0) {
            this.B0 = false;
        }
        if (this.A0 == 1) {
            if (z2 && this.B0 && !this.C0 && this.q0 != null && this.p0 != null) {
                C();
            }
            if (!this.C0) {
                this.B0 = false;
            }
        }
        this.A0--;
    }

    public RecyclerView(Context context, AttributeSet attributeSet) {
        this(context, attributeSet, fy2.recyclerViewStyle);
    }

    /* loaded from: classes.dex */
    public static class SavedState extends AbsSavedState {
        public static final Parcelable.Creator<SavedState> CREATOR = new a();
        public Parcelable g0;

        /* loaded from: classes.dex */
        public class a implements Parcelable.ClassLoaderCreator<SavedState> {
            @Override // android.os.Parcelable.Creator
            /* renamed from: a */
            public SavedState createFromParcel(Parcel parcel) {
                return new SavedState(parcel, null);
            }

            @Override // android.os.Parcelable.ClassLoaderCreator
            /* renamed from: b */
            public SavedState createFromParcel(Parcel parcel, ClassLoader classLoader) {
                return new SavedState(parcel, classLoader);
            }

            @Override // android.os.Parcelable.Creator
            /* renamed from: c */
            public SavedState[] newArray(int i) {
                return new SavedState[i];
            }
        }

        public SavedState(Parcel parcel, ClassLoader classLoader) {
            super(parcel, classLoader);
            this.g0 = parcel.readParcelable(classLoader == null ? LayoutManager.class.getClassLoader() : classLoader);
        }

        public void b(SavedState savedState) {
            this.g0 = savedState.g0;
        }

        @Override // androidx.customview.view.AbsSavedState, android.os.Parcelable
        public void writeToParcel(Parcel parcel, int i) {
            super.writeToParcel(parcel, i);
            parcel.writeParcelable(this.g0, 0);
        }

        public SavedState(Parcelable parcelable) {
            super(parcelable);
        }
    }

    public RecyclerView(Context context, AttributeSet attributeSet, int i2) {
        super(context, attributeSet, i2);
        this.a = new v();
        this.f0 = new t();
        this.j0 = new androidx.recyclerview.widget.x();
        this.l0 = new a();
        this.m0 = new Rect();
        this.n0 = new Rect();
        this.o0 = new RectF();
        this.s0 = new ArrayList();
        this.t0 = new ArrayList<>();
        this.u0 = new ArrayList<>();
        this.A0 = 0;
        this.I0 = false;
        this.J0 = false;
        this.K0 = 0;
        this.L0 = 0;
        this.M0 = new k();
        this.R0 = new androidx.recyclerview.widget.f();
        this.S0 = 0;
        this.T0 = -1;
        this.d1 = Float.MIN_VALUE;
        this.e1 = Float.MIN_VALUE;
        boolean z2 = true;
        this.f1 = true;
        this.g1 = new z();
        this.i1 = H1 ? new j.b() : null;
        this.j1 = new x();
        this.m1 = false;
        this.n1 = false;
        this.o1 = new m();
        this.p1 = false;
        this.s1 = new int[2];
        this.u1 = new int[2];
        this.v1 = new int[2];
        this.w1 = new int[2];
        this.x1 = new ArrayList();
        this.y1 = new b();
        this.A1 = 0;
        this.B1 = 0;
        this.C1 = new d();
        setScrollContainer(true);
        setFocusableInTouchMode(true);
        ViewConfiguration viewConfiguration = ViewConfiguration.get(context);
        this.Z0 = viewConfiguration.getScaledTouchSlop();
        this.d1 = gi4.b(viewConfiguration, context);
        this.e1 = gi4.d(viewConfiguration, context);
        this.b1 = viewConfiguration.getScaledMinimumFlingVelocity();
        this.c1 = viewConfiguration.getScaledMaximumFlingVelocity();
        setWillNotDraw(getOverScrollMode() == 2);
        this.R0.setListener(this.o1);
        s0();
        u0();
        t0();
        if (ei4.C(this) == 0) {
            ei4.D0(this, 1);
        }
        this.G0 = (AccessibilityManager) getContext().getSystemService("accessibility");
        setAccessibilityDelegateCompat(new androidx.recyclerview.widget.s(this));
        int[] iArr = e23.RecyclerView;
        TypedArray obtainStyledAttributes = context.obtainStyledAttributes(attributeSet, iArr, i2, 0);
        ei4.r0(this, context, iArr, attributeSet, obtainStyledAttributes, i2, 0);
        String string = obtainStyledAttributes.getString(e23.RecyclerView_layoutManager);
        if (obtainStyledAttributes.getInt(e23.RecyclerView_android_descendantFocusability, -1) == -1) {
            setDescendantFocusability(262144);
        }
        this.k0 = obtainStyledAttributes.getBoolean(e23.RecyclerView_android_clipToPadding, true);
        boolean z3 = obtainStyledAttributes.getBoolean(e23.RecyclerView_fastScrollEnabled, false);
        this.y0 = z3;
        if (z3) {
            v0((StateListDrawable) obtainStyledAttributes.getDrawable(e23.RecyclerView_fastScrollVerticalThumbDrawable), obtainStyledAttributes.getDrawable(e23.RecyclerView_fastScrollVerticalTrackDrawable), (StateListDrawable) obtainStyledAttributes.getDrawable(e23.RecyclerView_fastScrollHorizontalThumbDrawable), obtainStyledAttributes.getDrawable(e23.RecyclerView_fastScrollHorizontalTrackDrawable));
        }
        obtainStyledAttributes.recycle();
        w(context, string, attributeSet, i2, 0);
        if (Build.VERSION.SDK_INT >= 21) {
            int[] iArr2 = D1;
            TypedArray obtainStyledAttributes2 = context.obtainStyledAttributes(attributeSet, iArr2, i2, 0);
            ei4.r0(this, context, iArr2, attributeSet, obtainStyledAttributes2, i2, 0);
            z2 = obtainStyledAttributes2.getBoolean(0, true);
            obtainStyledAttributes2.recycle();
        }
        setNestedScrollingEnabled(z2);
    }

    /* loaded from: classes.dex */
    public static class LayoutParams extends ViewGroup.MarginLayoutParams {
        public a0 a;
        public final Rect f0;
        public boolean g0;
        public boolean h0;

        public LayoutParams(Context context, AttributeSet attributeSet) {
            super(context, attributeSet);
            this.f0 = new Rect();
            this.g0 = true;
            this.h0 = false;
        }

        public int a() {
            return this.a.getLayoutPosition();
        }

        public boolean b() {
            return this.a.isUpdated();
        }

        public boolean c() {
            return this.a.isRemoved();
        }

        public boolean d() {
            return this.a.isInvalid();
        }

        public LayoutParams(int i, int i2) {
            super(i, i2);
            this.f0 = new Rect();
            this.g0 = true;
            this.h0 = false;
        }

        public LayoutParams(ViewGroup.MarginLayoutParams marginLayoutParams) {
            super(marginLayoutParams);
            this.f0 = new Rect();
            this.g0 = true;
            this.h0 = false;
        }

        public LayoutParams(ViewGroup.LayoutParams layoutParams) {
            super(layoutParams);
            this.f0 = new Rect();
            this.g0 = true;
            this.h0 = false;
        }

        public LayoutParams(LayoutParams layoutParams) {
            super((ViewGroup.LayoutParams) layoutParams);
            this.f0 = new Rect();
            this.g0 = true;
            this.h0 = false;
        }
    }

    @Override // android.view.ViewGroup
    public ViewGroup.LayoutParams generateLayoutParams(ViewGroup.LayoutParams layoutParams) {
        LayoutManager layoutManager = this.q0;
        if (layoutManager != null) {
            return layoutManager.Q(layoutParams);
        }
        throw new IllegalStateException("RecyclerView has no LayoutManager" + Q());
    }
}
