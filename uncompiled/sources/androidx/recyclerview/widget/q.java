package androidx.recyclerview.widget;

import android.graphics.Rect;
import android.view.View;
import android.view.ViewGroup;
import androidx.recyclerview.widget.RecyclerView;

/* compiled from: OrientationHelper.java */
/* loaded from: classes.dex */
public abstract class q {
    public final RecyclerView.LayoutManager a;
    public int b;
    public final Rect c;

    /* compiled from: OrientationHelper.java */
    /* loaded from: classes.dex */
    public class a extends q {
        public a(RecyclerView.LayoutManager layoutManager) {
            super(layoutManager, null);
        }

        @Override // androidx.recyclerview.widget.q
        public int d(View view) {
            return this.a.e0(view) + ((ViewGroup.MarginLayoutParams) ((RecyclerView.LayoutParams) view.getLayoutParams())).rightMargin;
        }

        @Override // androidx.recyclerview.widget.q
        public int e(View view) {
            RecyclerView.LayoutParams layoutParams = (RecyclerView.LayoutParams) view.getLayoutParams();
            return this.a.d0(view) + ((ViewGroup.MarginLayoutParams) layoutParams).leftMargin + ((ViewGroup.MarginLayoutParams) layoutParams).rightMargin;
        }

        @Override // androidx.recyclerview.widget.q
        public int f(View view) {
            RecyclerView.LayoutParams layoutParams = (RecyclerView.LayoutParams) view.getLayoutParams();
            return this.a.c0(view) + ((ViewGroup.MarginLayoutParams) layoutParams).topMargin + ((ViewGroup.MarginLayoutParams) layoutParams).bottomMargin;
        }

        @Override // androidx.recyclerview.widget.q
        public int g(View view) {
            return this.a.b0(view) - ((ViewGroup.MarginLayoutParams) ((RecyclerView.LayoutParams) view.getLayoutParams())).leftMargin;
        }

        @Override // androidx.recyclerview.widget.q
        public int h() {
            return this.a.v0();
        }

        @Override // androidx.recyclerview.widget.q
        public int i() {
            return this.a.v0() - this.a.getPaddingRight();
        }

        @Override // androidx.recyclerview.widget.q
        public int j() {
            return this.a.getPaddingRight();
        }

        @Override // androidx.recyclerview.widget.q
        public int k() {
            return this.a.w0();
        }

        @Override // androidx.recyclerview.widget.q
        public int l() {
            return this.a.i0();
        }

        @Override // androidx.recyclerview.widget.q
        public int m() {
            return this.a.getPaddingLeft();
        }

        @Override // androidx.recyclerview.widget.q
        public int n() {
            return (this.a.v0() - this.a.getPaddingLeft()) - this.a.getPaddingRight();
        }

        @Override // androidx.recyclerview.widget.q
        public int p(View view) {
            this.a.u0(view, true, this.c);
            return this.c.right;
        }

        @Override // androidx.recyclerview.widget.q
        public int q(View view) {
            this.a.u0(view, true, this.c);
            return this.c.left;
        }

        @Override // androidx.recyclerview.widget.q
        public void r(int i) {
            this.a.K0(i);
        }
    }

    /* compiled from: OrientationHelper.java */
    /* loaded from: classes.dex */
    public class b extends q {
        public b(RecyclerView.LayoutManager layoutManager) {
            super(layoutManager, null);
        }

        @Override // androidx.recyclerview.widget.q
        public int d(View view) {
            return this.a.Z(view) + ((ViewGroup.MarginLayoutParams) ((RecyclerView.LayoutParams) view.getLayoutParams())).bottomMargin;
        }

        @Override // androidx.recyclerview.widget.q
        public int e(View view) {
            RecyclerView.LayoutParams layoutParams = (RecyclerView.LayoutParams) view.getLayoutParams();
            return this.a.c0(view) + ((ViewGroup.MarginLayoutParams) layoutParams).topMargin + ((ViewGroup.MarginLayoutParams) layoutParams).bottomMargin;
        }

        @Override // androidx.recyclerview.widget.q
        public int f(View view) {
            RecyclerView.LayoutParams layoutParams = (RecyclerView.LayoutParams) view.getLayoutParams();
            return this.a.d0(view) + ((ViewGroup.MarginLayoutParams) layoutParams).leftMargin + ((ViewGroup.MarginLayoutParams) layoutParams).rightMargin;
        }

        @Override // androidx.recyclerview.widget.q
        public int g(View view) {
            return this.a.f0(view) - ((ViewGroup.MarginLayoutParams) ((RecyclerView.LayoutParams) view.getLayoutParams())).topMargin;
        }

        @Override // androidx.recyclerview.widget.q
        public int h() {
            return this.a.h0();
        }

        @Override // androidx.recyclerview.widget.q
        public int i() {
            return this.a.h0() - this.a.getPaddingBottom();
        }

        @Override // androidx.recyclerview.widget.q
        public int j() {
            return this.a.getPaddingBottom();
        }

        @Override // androidx.recyclerview.widget.q
        public int k() {
            return this.a.i0();
        }

        @Override // androidx.recyclerview.widget.q
        public int l() {
            return this.a.w0();
        }

        @Override // androidx.recyclerview.widget.q
        public int m() {
            return this.a.getPaddingTop();
        }

        @Override // androidx.recyclerview.widget.q
        public int n() {
            return (this.a.h0() - this.a.getPaddingTop()) - this.a.getPaddingBottom();
        }

        @Override // androidx.recyclerview.widget.q
        public int p(View view) {
            this.a.u0(view, true, this.c);
            return this.c.bottom;
        }

        @Override // androidx.recyclerview.widget.q
        public int q(View view) {
            this.a.u0(view, true, this.c);
            return this.c.top;
        }

        @Override // androidx.recyclerview.widget.q
        public void r(int i) {
            this.a.L0(i);
        }
    }

    public /* synthetic */ q(RecyclerView.LayoutManager layoutManager, a aVar) {
        this(layoutManager);
    }

    public static q a(RecyclerView.LayoutManager layoutManager) {
        return new a(layoutManager);
    }

    public static q b(RecyclerView.LayoutManager layoutManager, int i) {
        if (i != 0) {
            if (i == 1) {
                return c(layoutManager);
            }
            throw new IllegalArgumentException("invalid orientation");
        }
        return a(layoutManager);
    }

    public static q c(RecyclerView.LayoutManager layoutManager) {
        return new b(layoutManager);
    }

    public abstract int d(View view);

    public abstract int e(View view);

    public abstract int f(View view);

    public abstract int g(View view);

    public abstract int h();

    public abstract int i();

    public abstract int j();

    public abstract int k();

    public abstract int l();

    public abstract int m();

    public abstract int n();

    public int o() {
        if (Integer.MIN_VALUE == this.b) {
            return 0;
        }
        return n() - this.b;
    }

    public abstract int p(View view);

    public abstract int q(View view);

    public abstract void r(int i);

    public void s() {
        this.b = n();
    }

    public q(RecyclerView.LayoutManager layoutManager) {
        this.b = Integer.MIN_VALUE;
        this.c = new Rect();
        this.a = layoutManager;
    }
}
