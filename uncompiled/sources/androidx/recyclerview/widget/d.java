package androidx.recyclerview.widget;

import android.os.Handler;
import android.os.Looper;
import androidx.recyclerview.widget.RecyclerView;
import androidx.recyclerview.widget.c;
import androidx.recyclerview.widget.g;
import java.util.Collections;
import java.util.List;
import java.util.concurrent.CopyOnWriteArrayList;
import java.util.concurrent.Executor;

/* compiled from: AsyncListDiffer.java */
/* loaded from: classes.dex */
public class d<T> {
    public static final Executor h = new c();
    public final i02 a;
    public final androidx.recyclerview.widget.c<T> b;
    public Executor c;
    public final List<b<T>> d;
    public List<T> e;
    public List<T> f;
    public int g;

    /* compiled from: AsyncListDiffer.java */
    /* loaded from: classes.dex */
    public class a implements Runnable {
        public final /* synthetic */ List a;
        public final /* synthetic */ List f0;
        public final /* synthetic */ int g0;
        public final /* synthetic */ Runnable h0;

        /* compiled from: AsyncListDiffer.java */
        /* renamed from: androidx.recyclerview.widget.d$a$a  reason: collision with other inner class name */
        /* loaded from: classes.dex */
        public class C0057a extends g.b {
            public C0057a() {
            }

            @Override // androidx.recyclerview.widget.g.b
            public boolean areContentsTheSame(int i, int i2) {
                Object obj = a.this.a.get(i);
                Object obj2 = a.this.f0.get(i2);
                if (obj == null || obj2 == null) {
                    if (obj == null && obj2 == null) {
                        return true;
                    }
                    throw new AssertionError();
                }
                return d.this.b.b().areContentsTheSame(obj, obj2);
            }

            @Override // androidx.recyclerview.widget.g.b
            public boolean areItemsTheSame(int i, int i2) {
                Object obj = a.this.a.get(i);
                Object obj2 = a.this.f0.get(i2);
                if (obj == null || obj2 == null) {
                    return obj == null && obj2 == null;
                }
                return d.this.b.b().areItemsTheSame(obj, obj2);
            }

            @Override // androidx.recyclerview.widget.g.b
            public Object getChangePayload(int i, int i2) {
                Object obj = a.this.a.get(i);
                Object obj2 = a.this.f0.get(i2);
                if (obj != null && obj2 != null) {
                    return d.this.b.b().getChangePayload(obj, obj2);
                }
                throw new AssertionError();
            }

            @Override // androidx.recyclerview.widget.g.b
            public int getNewListSize() {
                return a.this.f0.size();
            }

            @Override // androidx.recyclerview.widget.g.b
            public int getOldListSize() {
                return a.this.a.size();
            }
        }

        /* compiled from: AsyncListDiffer.java */
        /* loaded from: classes.dex */
        public class b implements Runnable {
            public final /* synthetic */ g.e a;

            public b(g.e eVar) {
                this.a = eVar;
            }

            @Override // java.lang.Runnable
            public void run() {
                a aVar = a.this;
                d dVar = d.this;
                if (dVar.g == aVar.g0) {
                    dVar.c(aVar.f0, this.a, aVar.h0);
                }
            }
        }

        public a(List list, List list2, int i, Runnable runnable) {
            this.a = list;
            this.f0 = list2;
            this.g0 = i;
            this.h0 = runnable;
        }

        @Override // java.lang.Runnable
        public void run() {
            d.this.c.execute(new b(g.b(new C0057a())));
        }
    }

    /* compiled from: AsyncListDiffer.java */
    /* loaded from: classes.dex */
    public interface b<T> {
        void a(List<T> list, List<T> list2);
    }

    /* compiled from: AsyncListDiffer.java */
    /* loaded from: classes.dex */
    public static class c implements Executor {
        public final Handler a = new Handler(Looper.getMainLooper());

        @Override // java.util.concurrent.Executor
        public void execute(Runnable runnable) {
            this.a.post(runnable);
        }
    }

    public d(RecyclerView.Adapter adapter, g.f<T> fVar) {
        this(new androidx.recyclerview.widget.b(adapter), new c.a(fVar).a());
    }

    public void a(b<T> bVar) {
        this.d.add(bVar);
    }

    public List<T> b() {
        return this.f;
    }

    public void c(List<T> list, g.e eVar, Runnable runnable) {
        List<T> list2 = this.f;
        this.e = list;
        this.f = Collections.unmodifiableList(list);
        eVar.c(this.a);
        d(list2, runnable);
    }

    public final void d(List<T> list, Runnable runnable) {
        for (b<T> bVar : this.d) {
            bVar.a(list, this.f);
        }
        if (runnable != null) {
            runnable.run();
        }
    }

    public void e(List<T> list) {
        f(list, null);
    }

    public void f(List<T> list, Runnable runnable) {
        int i = this.g + 1;
        this.g = i;
        List<T> list2 = this.e;
        if (list == list2) {
            if (runnable != null) {
                runnable.run();
                return;
            }
            return;
        }
        List<T> list3 = this.f;
        if (list == null) {
            int size = list2.size();
            this.e = null;
            this.f = Collections.emptyList();
            this.a.onRemoved(0, size);
            d(list3, runnable);
        } else if (list2 == null) {
            this.e = list;
            this.f = Collections.unmodifiableList(list);
            this.a.onInserted(0, list.size());
            d(list3, runnable);
        } else {
            this.b.a().execute(new a(list2, list, i, runnable));
        }
    }

    public d(i02 i02Var, androidx.recyclerview.widget.c<T> cVar) {
        this.d = new CopyOnWriteArrayList();
        this.f = Collections.emptyList();
        this.a = i02Var;
        this.b = cVar;
        if (cVar.c() != null) {
            this.c = cVar.c();
        } else {
            this.c = h;
        }
    }
}
