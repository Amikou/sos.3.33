package androidx.recyclerview.widget;

import android.content.Context;
import android.util.DisplayMetrics;
import android.view.View;
import android.view.animation.DecelerateInterpolator;
import android.widget.Scroller;
import androidx.recyclerview.widget.RecyclerView;

/* compiled from: SnapHelper.java */
/* loaded from: classes.dex */
public abstract class v extends RecyclerView.p {
    public RecyclerView a;
    public Scroller b;
    public final RecyclerView.r c = new a();

    /* compiled from: SnapHelper.java */
    /* loaded from: classes.dex */
    public class a extends RecyclerView.r {
        public boolean a = false;

        public a() {
        }

        @Override // androidx.recyclerview.widget.RecyclerView.r
        public void onScrollStateChanged(RecyclerView recyclerView, int i) {
            super.onScrollStateChanged(recyclerView, i);
            if (i == 0 && this.a) {
                this.a = false;
                v.this.l();
            }
        }

        @Override // androidx.recyclerview.widget.RecyclerView.r
        public void onScrolled(RecyclerView recyclerView, int i, int i2) {
            if (i == 0 && i2 == 0) {
                return;
            }
            this.a = true;
        }
    }

    /* compiled from: SnapHelper.java */
    /* loaded from: classes.dex */
    public class b extends m {
        public b(Context context) {
            super(context);
        }

        @Override // androidx.recyclerview.widget.m
        public float calculateSpeedPerPixel(DisplayMetrics displayMetrics) {
            return 100.0f / displayMetrics.densityDpi;
        }

        @Override // androidx.recyclerview.widget.m, androidx.recyclerview.widget.RecyclerView.w
        public void onTargetFound(View view, RecyclerView.x xVar, RecyclerView.w.a aVar) {
            v vVar = v.this;
            RecyclerView recyclerView = vVar.a;
            if (recyclerView == null) {
                return;
            }
            int[] c = vVar.c(recyclerView.getLayoutManager(), view);
            int i = c[0];
            int i2 = c[1];
            int calculateTimeForDeceleration = calculateTimeForDeceleration(Math.max(Math.abs(i), Math.abs(i2)));
            if (calculateTimeForDeceleration > 0) {
                aVar.d(i, i2, calculateTimeForDeceleration, this.mDecelerateInterpolator);
            }
        }
    }

    @Override // androidx.recyclerview.widget.RecyclerView.p
    public boolean a(int i, int i2) {
        RecyclerView.LayoutManager layoutManager = this.a.getLayoutManager();
        if (layoutManager == null || this.a.getAdapter() == null) {
            return false;
        }
        int minFlingVelocity = this.a.getMinFlingVelocity();
        return (Math.abs(i2) > minFlingVelocity || Math.abs(i) > minFlingVelocity) && k(layoutManager, i, i2);
    }

    public void b(RecyclerView recyclerView) throws IllegalStateException {
        RecyclerView recyclerView2 = this.a;
        if (recyclerView2 == recyclerView) {
            return;
        }
        if (recyclerView2 != null) {
            g();
        }
        this.a = recyclerView;
        if (recyclerView != null) {
            j();
            this.b = new Scroller(this.a.getContext(), new DecelerateInterpolator());
            l();
        }
    }

    public abstract int[] c(RecyclerView.LayoutManager layoutManager, View view);

    public int[] d(int i, int i2) {
        this.b.fling(0, 0, i, i2, Integer.MIN_VALUE, Integer.MAX_VALUE, Integer.MIN_VALUE, Integer.MAX_VALUE);
        return new int[]{this.b.getFinalX(), this.b.getFinalY()};
    }

    public RecyclerView.w e(RecyclerView.LayoutManager layoutManager) {
        return f(layoutManager);
    }

    @Deprecated
    public m f(RecyclerView.LayoutManager layoutManager) {
        if (layoutManager instanceof RecyclerView.w.b) {
            return new b(this.a.getContext());
        }
        return null;
    }

    public final void g() {
        this.a.f1(this.c);
        this.a.setOnFlingListener(null);
    }

    public abstract View h(RecyclerView.LayoutManager layoutManager);

    public abstract int i(RecyclerView.LayoutManager layoutManager, int i, int i2);

    public final void j() throws IllegalStateException {
        if (this.a.getOnFlingListener() == null) {
            this.a.l(this.c);
            this.a.setOnFlingListener(this);
            return;
        }
        throw new IllegalStateException("An instance of OnFlingListener already set.");
    }

    public final boolean k(RecyclerView.LayoutManager layoutManager, int i, int i2) {
        RecyclerView.w e;
        int i3;
        if (!(layoutManager instanceof RecyclerView.w.b) || (e = e(layoutManager)) == null || (i3 = i(layoutManager, i, i2)) == -1) {
            return false;
        }
        e.setTargetPosition(i3);
        layoutManager.S1(e);
        return true;
    }

    public void l() {
        RecyclerView.LayoutManager layoutManager;
        View h;
        RecyclerView recyclerView = this.a;
        if (recyclerView == null || (layoutManager = recyclerView.getLayoutManager()) == null || (h = h(layoutManager)) == null) {
            return;
        }
        int[] c = c(layoutManager, h);
        if (c[0] == 0 && c[1] == 0) {
            return;
        }
        this.a.s1(c[0], c[1]);
    }
}
