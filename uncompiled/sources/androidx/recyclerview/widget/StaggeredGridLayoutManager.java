package androidx.recyclerview.widget;

import android.annotation.SuppressLint;
import android.content.Context;
import android.graphics.PointF;
import android.graphics.Rect;
import android.os.Parcel;
import android.os.Parcelable;
import android.util.AttributeSet;
import android.view.View;
import android.view.ViewGroup;
import android.view.accessibility.AccessibilityEvent;
import androidx.recyclerview.widget.RecyclerView;
import com.github.mikephil.charting.utils.Utils;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.BitSet;
import java.util.List;

/* loaded from: classes.dex */
public class StaggeredGridLayoutManager extends RecyclerView.LayoutManager implements RecyclerView.w.b {
    public int A0;
    public int B0;
    public final l C0;
    public BitSet F0;
    public boolean K0;
    public boolean L0;
    public SavedState M0;
    public int N0;
    public int[] S0;
    public c[] x0;
    public q y0;
    public q z0;
    public int w0 = -1;
    public boolean D0 = false;
    public boolean E0 = false;
    public int G0 = -1;
    public int H0 = Integer.MIN_VALUE;
    public LazySpanLookup I0 = new LazySpanLookup();
    public int J0 = 2;
    public final Rect O0 = new Rect();
    public final b P0 = new b();
    public boolean Q0 = false;
    public boolean R0 = true;
    public final Runnable T0 = new a();

    /* loaded from: classes.dex */
    public static class LayoutParams extends RecyclerView.LayoutParams {
        public c i0;
        public boolean j0;

        public LayoutParams(Context context, AttributeSet attributeSet) {
            super(context, attributeSet);
        }

        public boolean e() {
            return this.j0;
        }

        public LayoutParams(int i, int i2) {
            super(i, i2);
        }

        public LayoutParams(ViewGroup.MarginLayoutParams marginLayoutParams) {
            super(marginLayoutParams);
        }

        public LayoutParams(ViewGroup.LayoutParams layoutParams) {
            super(layoutParams);
        }
    }

    @SuppressLint({"BanParcelableUsage"})
    /* loaded from: classes.dex */
    public static class SavedState implements Parcelable {
        public static final Parcelable.Creator<SavedState> CREATOR = new a();
        public int a;
        public int f0;
        public int g0;
        public int[] h0;
        public int i0;
        public int[] j0;
        public List<LazySpanLookup.FullSpanItem> k0;
        public boolean l0;
        public boolean m0;
        public boolean n0;

        /* loaded from: classes.dex */
        public class a implements Parcelable.Creator<SavedState> {
            @Override // android.os.Parcelable.Creator
            /* renamed from: a */
            public SavedState createFromParcel(Parcel parcel) {
                return new SavedState(parcel);
            }

            @Override // android.os.Parcelable.Creator
            /* renamed from: b */
            public SavedState[] newArray(int i) {
                return new SavedState[i];
            }
        }

        public SavedState() {
        }

        public void a() {
            this.h0 = null;
            this.g0 = 0;
            this.a = -1;
            this.f0 = -1;
        }

        public void b() {
            this.h0 = null;
            this.g0 = 0;
            this.i0 = 0;
            this.j0 = null;
            this.k0 = null;
        }

        @Override // android.os.Parcelable
        public int describeContents() {
            return 0;
        }

        @Override // android.os.Parcelable
        public void writeToParcel(Parcel parcel, int i) {
            parcel.writeInt(this.a);
            parcel.writeInt(this.f0);
            parcel.writeInt(this.g0);
            if (this.g0 > 0) {
                parcel.writeIntArray(this.h0);
            }
            parcel.writeInt(this.i0);
            if (this.i0 > 0) {
                parcel.writeIntArray(this.j0);
            }
            parcel.writeInt(this.l0 ? 1 : 0);
            parcel.writeInt(this.m0 ? 1 : 0);
            parcel.writeInt(this.n0 ? 1 : 0);
            parcel.writeList(this.k0);
        }

        public SavedState(Parcel parcel) {
            this.a = parcel.readInt();
            this.f0 = parcel.readInt();
            int readInt = parcel.readInt();
            this.g0 = readInt;
            if (readInt > 0) {
                int[] iArr = new int[readInt];
                this.h0 = iArr;
                parcel.readIntArray(iArr);
            }
            int readInt2 = parcel.readInt();
            this.i0 = readInt2;
            if (readInt2 > 0) {
                int[] iArr2 = new int[readInt2];
                this.j0 = iArr2;
                parcel.readIntArray(iArr2);
            }
            this.l0 = parcel.readInt() == 1;
            this.m0 = parcel.readInt() == 1;
            this.n0 = parcel.readInt() == 1;
            this.k0 = parcel.readArrayList(LazySpanLookup.FullSpanItem.class.getClassLoader());
        }

        public SavedState(SavedState savedState) {
            this.g0 = savedState.g0;
            this.a = savedState.a;
            this.f0 = savedState.f0;
            this.h0 = savedState.h0;
            this.i0 = savedState.i0;
            this.j0 = savedState.j0;
            this.l0 = savedState.l0;
            this.m0 = savedState.m0;
            this.n0 = savedState.n0;
            this.k0 = savedState.k0;
        }
    }

    /* loaded from: classes.dex */
    public class a implements Runnable {
        public a() {
        }

        @Override // java.lang.Runnable
        public void run() {
            StaggeredGridLayoutManager.this.b2();
        }
    }

    /* loaded from: classes.dex */
    public class b {
        public int a;
        public int b;
        public boolean c;
        public boolean d;
        public boolean e;
        public int[] f;

        public b() {
            c();
        }

        public void a() {
            this.b = this.c ? StaggeredGridLayoutManager.this.y0.i() : StaggeredGridLayoutManager.this.y0.m();
        }

        public void b(int i) {
            if (this.c) {
                this.b = StaggeredGridLayoutManager.this.y0.i() - i;
            } else {
                this.b = StaggeredGridLayoutManager.this.y0.m() + i;
            }
        }

        public void c() {
            this.a = -1;
            this.b = Integer.MIN_VALUE;
            this.c = false;
            this.d = false;
            this.e = false;
            int[] iArr = this.f;
            if (iArr != null) {
                Arrays.fill(iArr, -1);
            }
        }

        public void d(c[] cVarArr) {
            int length = cVarArr.length;
            int[] iArr = this.f;
            if (iArr == null || iArr.length < length) {
                this.f = new int[StaggeredGridLayoutManager.this.x0.length];
            }
            for (int i = 0; i < length; i++) {
                this.f[i] = cVarArr[i].p(Integer.MIN_VALUE);
            }
        }
    }

    /* loaded from: classes.dex */
    public class c {
        public ArrayList<View> a = new ArrayList<>();
        public int b = Integer.MIN_VALUE;
        public int c = Integer.MIN_VALUE;
        public int d = 0;
        public final int e;

        public c(int i) {
            this.e = i;
        }

        public void a(View view) {
            LayoutParams n = n(view);
            n.i0 = this;
            this.a.add(view);
            this.c = Integer.MIN_VALUE;
            if (this.a.size() == 1) {
                this.b = Integer.MIN_VALUE;
            }
            if (n.c() || n.b()) {
                this.d += StaggeredGridLayoutManager.this.y0.e(view);
            }
        }

        public void b(boolean z, int i) {
            int p;
            if (z) {
                p = l(Integer.MIN_VALUE);
            } else {
                p = p(Integer.MIN_VALUE);
            }
            e();
            if (p == Integer.MIN_VALUE) {
                return;
            }
            if (!z || p >= StaggeredGridLayoutManager.this.y0.i()) {
                if (z || p <= StaggeredGridLayoutManager.this.y0.m()) {
                    if (i != Integer.MIN_VALUE) {
                        p += i;
                    }
                    this.c = p;
                    this.b = p;
                }
            }
        }

        public void c() {
            LazySpanLookup.FullSpanItem f;
            ArrayList<View> arrayList = this.a;
            View view = arrayList.get(arrayList.size() - 1);
            LayoutParams n = n(view);
            this.c = StaggeredGridLayoutManager.this.y0.d(view);
            if (n.j0 && (f = StaggeredGridLayoutManager.this.I0.f(n.a())) != null && f.f0 == 1) {
                this.c += f.a(this.e);
            }
        }

        public void d() {
            LazySpanLookup.FullSpanItem f;
            View view = this.a.get(0);
            LayoutParams n = n(view);
            this.b = StaggeredGridLayoutManager.this.y0.g(view);
            if (n.j0 && (f = StaggeredGridLayoutManager.this.I0.f(n.a())) != null && f.f0 == -1) {
                this.b -= f.a(this.e);
            }
        }

        public void e() {
            this.a.clear();
            q();
            this.d = 0;
        }

        public int f() {
            if (StaggeredGridLayoutManager.this.D0) {
                return i(this.a.size() - 1, -1, true);
            }
            return i(0, this.a.size(), true);
        }

        public int g() {
            if (StaggeredGridLayoutManager.this.D0) {
                return i(0, this.a.size(), true);
            }
            return i(this.a.size() - 1, -1, true);
        }

        public int h(int i, int i2, boolean z, boolean z2, boolean z3) {
            int m = StaggeredGridLayoutManager.this.y0.m();
            int i3 = StaggeredGridLayoutManager.this.y0.i();
            int i4 = i2 > i ? 1 : -1;
            while (i != i2) {
                View view = this.a.get(i);
                int g = StaggeredGridLayoutManager.this.y0.g(view);
                int d = StaggeredGridLayoutManager.this.y0.d(view);
                boolean z4 = false;
                boolean z5 = !z3 ? g >= i3 : g > i3;
                if (!z3 ? d > m : d >= m) {
                    z4 = true;
                }
                if (z5 && z4) {
                    if (z && z2) {
                        if (g >= m && d <= i3) {
                            return StaggeredGridLayoutManager.this.o0(view);
                        }
                    } else if (z2) {
                        return StaggeredGridLayoutManager.this.o0(view);
                    } else {
                        if (g < m || d > i3) {
                            return StaggeredGridLayoutManager.this.o0(view);
                        }
                    }
                }
                i += i4;
            }
            return -1;
        }

        public int i(int i, int i2, boolean z) {
            return h(i, i2, false, false, z);
        }

        public int j() {
            return this.d;
        }

        public int k() {
            int i = this.c;
            if (i != Integer.MIN_VALUE) {
                return i;
            }
            c();
            return this.c;
        }

        public int l(int i) {
            int i2 = this.c;
            if (i2 != Integer.MIN_VALUE) {
                return i2;
            }
            if (this.a.size() == 0) {
                return i;
            }
            c();
            return this.c;
        }

        public View m(int i, int i2) {
            View view = null;
            if (i2 == -1) {
                int size = this.a.size();
                int i3 = 0;
                while (i3 < size) {
                    View view2 = this.a.get(i3);
                    StaggeredGridLayoutManager staggeredGridLayoutManager = StaggeredGridLayoutManager.this;
                    if (staggeredGridLayoutManager.D0 && staggeredGridLayoutManager.o0(view2) <= i) {
                        break;
                    }
                    StaggeredGridLayoutManager staggeredGridLayoutManager2 = StaggeredGridLayoutManager.this;
                    if ((!staggeredGridLayoutManager2.D0 && staggeredGridLayoutManager2.o0(view2) >= i) || !view2.hasFocusable()) {
                        break;
                    }
                    i3++;
                    view = view2;
                }
            } else {
                int size2 = this.a.size() - 1;
                while (size2 >= 0) {
                    View view3 = this.a.get(size2);
                    StaggeredGridLayoutManager staggeredGridLayoutManager3 = StaggeredGridLayoutManager.this;
                    if (staggeredGridLayoutManager3.D0 && staggeredGridLayoutManager3.o0(view3) >= i) {
                        break;
                    }
                    StaggeredGridLayoutManager staggeredGridLayoutManager4 = StaggeredGridLayoutManager.this;
                    if ((!staggeredGridLayoutManager4.D0 && staggeredGridLayoutManager4.o0(view3) <= i) || !view3.hasFocusable()) {
                        break;
                    }
                    size2--;
                    view = view3;
                }
            }
            return view;
        }

        public LayoutParams n(View view) {
            return (LayoutParams) view.getLayoutParams();
        }

        public int o() {
            int i = this.b;
            if (i != Integer.MIN_VALUE) {
                return i;
            }
            d();
            return this.b;
        }

        public int p(int i) {
            int i2 = this.b;
            if (i2 != Integer.MIN_VALUE) {
                return i2;
            }
            if (this.a.size() == 0) {
                return i;
            }
            d();
            return this.b;
        }

        public void q() {
            this.b = Integer.MIN_VALUE;
            this.c = Integer.MIN_VALUE;
        }

        public void r(int i) {
            int i2 = this.b;
            if (i2 != Integer.MIN_VALUE) {
                this.b = i2 + i;
            }
            int i3 = this.c;
            if (i3 != Integer.MIN_VALUE) {
                this.c = i3 + i;
            }
        }

        public void s() {
            int size = this.a.size();
            View remove = this.a.remove(size - 1);
            LayoutParams n = n(remove);
            n.i0 = null;
            if (n.c() || n.b()) {
                this.d -= StaggeredGridLayoutManager.this.y0.e(remove);
            }
            if (size == 1) {
                this.b = Integer.MIN_VALUE;
            }
            this.c = Integer.MIN_VALUE;
        }

        public void t() {
            View remove = this.a.remove(0);
            LayoutParams n = n(remove);
            n.i0 = null;
            if (this.a.size() == 0) {
                this.c = Integer.MIN_VALUE;
            }
            if (n.c() || n.b()) {
                this.d -= StaggeredGridLayoutManager.this.y0.e(remove);
            }
            this.b = Integer.MIN_VALUE;
        }

        public void u(View view) {
            LayoutParams n = n(view);
            n.i0 = this;
            this.a.add(0, view);
            this.b = Integer.MIN_VALUE;
            if (this.a.size() == 1) {
                this.c = Integer.MIN_VALUE;
            }
            if (n.c() || n.b()) {
                this.d += StaggeredGridLayoutManager.this.y0.e(view);
            }
        }

        public void v(int i) {
            this.b = i;
            this.c = i;
        }
    }

    public StaggeredGridLayoutManager(Context context, AttributeSet attributeSet, int i, int i2) {
        RecyclerView.LayoutManager.Properties p0 = RecyclerView.LayoutManager.p0(context, attributeSet, i, i2);
        Q2(p0.a);
        S2(p0.b);
        R2(p0.c);
        this.C0 = new l();
        j2();
    }

    /* JADX WARN: Code restructure failed: missing block: B:31:0x0074, code lost:
        if (r10 == r11) goto L45;
     */
    /* JADX WARN: Code restructure failed: missing block: B:36:0x0086, code lost:
        if (r10 == r11) goto L45;
     */
    /* JADX WARN: Code restructure failed: missing block: B:37:0x0088, code lost:
        r10 = true;
     */
    /* JADX WARN: Code restructure failed: missing block: B:38:0x008a, code lost:
        r10 = false;
     */
    /*
        Code decompiled incorrectly, please refer to instructions dump.
        To view partially-correct code enable 'Show inconsistent code' option in preferences
    */
    public android.view.View A2() {
        /*
            r12 = this;
            int r0 = r12.U()
            r1 = 1
            int r0 = r0 - r1
            java.util.BitSet r2 = new java.util.BitSet
            int r3 = r12.w0
            r2.<init>(r3)
            int r3 = r12.w0
            r4 = 0
            r2.set(r4, r3, r1)
            int r3 = r12.A0
            r5 = -1
            if (r3 != r1) goto L20
            boolean r3 = r12.C2()
            if (r3 == 0) goto L20
            r3 = r1
            goto L21
        L20:
            r3 = r5
        L21:
            boolean r6 = r12.E0
            if (r6 == 0) goto L27
            r6 = r5
            goto L2b
        L27:
            int r0 = r0 + 1
            r6 = r0
            r0 = r4
        L2b:
            if (r0 >= r6) goto L2e
            r5 = r1
        L2e:
            if (r0 == r6) goto Lab
            android.view.View r7 = r12.T(r0)
            android.view.ViewGroup$LayoutParams r8 = r7.getLayoutParams()
            androidx.recyclerview.widget.StaggeredGridLayoutManager$LayoutParams r8 = (androidx.recyclerview.widget.StaggeredGridLayoutManager.LayoutParams) r8
            androidx.recyclerview.widget.StaggeredGridLayoutManager$c r9 = r8.i0
            int r9 = r9.e
            boolean r9 = r2.get(r9)
            if (r9 == 0) goto L54
            androidx.recyclerview.widget.StaggeredGridLayoutManager$c r9 = r8.i0
            boolean r9 = r12.c2(r9)
            if (r9 == 0) goto L4d
            return r7
        L4d:
            androidx.recyclerview.widget.StaggeredGridLayoutManager$c r9 = r8.i0
            int r9 = r9.e
            r2.clear(r9)
        L54:
            boolean r9 = r8.j0
            if (r9 == 0) goto L59
            goto La9
        L59:
            int r9 = r0 + r5
            if (r9 == r6) goto La9
            android.view.View r9 = r12.T(r9)
            boolean r10 = r12.E0
            if (r10 == 0) goto L77
            androidx.recyclerview.widget.q r10 = r12.y0
            int r10 = r10.d(r7)
            androidx.recyclerview.widget.q r11 = r12.y0
            int r11 = r11.d(r9)
            if (r10 >= r11) goto L74
            return r7
        L74:
            if (r10 != r11) goto L8a
            goto L88
        L77:
            androidx.recyclerview.widget.q r10 = r12.y0
            int r10 = r10.g(r7)
            androidx.recyclerview.widget.q r11 = r12.y0
            int r11 = r11.g(r9)
            if (r10 <= r11) goto L86
            return r7
        L86:
            if (r10 != r11) goto L8a
        L88:
            r10 = r1
            goto L8b
        L8a:
            r10 = r4
        L8b:
            if (r10 == 0) goto La9
            android.view.ViewGroup$LayoutParams r9 = r9.getLayoutParams()
            androidx.recyclerview.widget.StaggeredGridLayoutManager$LayoutParams r9 = (androidx.recyclerview.widget.StaggeredGridLayoutManager.LayoutParams) r9
            androidx.recyclerview.widget.StaggeredGridLayoutManager$c r8 = r8.i0
            int r8 = r8.e
            androidx.recyclerview.widget.StaggeredGridLayoutManager$c r9 = r9.i0
            int r9 = r9.e
            int r8 = r8 - r9
            if (r8 >= 0) goto La0
            r8 = r1
            goto La1
        La0:
            r8 = r4
        La1:
            if (r3 >= 0) goto La5
            r9 = r1
            goto La6
        La5:
            r9 = r4
        La6:
            if (r8 == r9) goto La9
            return r7
        La9:
            int r0 = r0 + r5
            goto L2e
        Lab:
            r0 = 0
            return r0
        */
        throw new UnsupportedOperationException("Method not decompiled: androidx.recyclerview.widget.StaggeredGridLayoutManager.A2():android.view.View");
    }

    @Override // androidx.recyclerview.widget.RecyclerView.LayoutManager
    public int B(RecyclerView.x xVar) {
        return d2(xVar);
    }

    public void B2() {
        this.I0.b();
        C1();
    }

    @Override // androidx.recyclerview.widget.RecyclerView.LayoutManager
    public int C(RecyclerView.x xVar) {
        return e2(xVar);
    }

    public boolean C2() {
        return k0() == 1;
    }

    @Override // androidx.recyclerview.widget.RecyclerView.LayoutManager
    public int D(RecyclerView.x xVar) {
        return f2(xVar);
    }

    public final void D2(View view, int i, int i2, boolean z) {
        boolean O1;
        u(view, this.O0);
        LayoutParams layoutParams = (LayoutParams) view.getLayoutParams();
        int i3 = ((ViewGroup.MarginLayoutParams) layoutParams).leftMargin;
        Rect rect = this.O0;
        int a3 = a3(i, i3 + rect.left, ((ViewGroup.MarginLayoutParams) layoutParams).rightMargin + rect.right);
        int i4 = ((ViewGroup.MarginLayoutParams) layoutParams).topMargin;
        Rect rect2 = this.O0;
        int a32 = a3(i2, i4 + rect2.top, ((ViewGroup.MarginLayoutParams) layoutParams).bottomMargin + rect2.bottom);
        if (z) {
            O1 = Q1(view, a3, a32, layoutParams);
        } else {
            O1 = O1(view, a3, a32, layoutParams);
        }
        if (O1) {
            view.measure(a3, a32);
        }
    }

    @Override // androidx.recyclerview.widget.RecyclerView.LayoutManager
    public int E(RecyclerView.x xVar) {
        return d2(xVar);
    }

    public final void E2(View view, LayoutParams layoutParams, boolean z) {
        if (layoutParams.j0) {
            if (this.A0 == 1) {
                D2(view, this.N0, RecyclerView.LayoutManager.V(h0(), i0(), getPaddingTop() + getPaddingBottom(), ((ViewGroup.MarginLayoutParams) layoutParams).height, true), z);
            } else {
                D2(view, RecyclerView.LayoutManager.V(v0(), w0(), getPaddingLeft() + getPaddingRight(), ((ViewGroup.MarginLayoutParams) layoutParams).width, true), this.N0, z);
            }
        } else if (this.A0 == 1) {
            D2(view, RecyclerView.LayoutManager.V(this.B0, w0(), 0, ((ViewGroup.MarginLayoutParams) layoutParams).width, false), RecyclerView.LayoutManager.V(h0(), i0(), getPaddingTop() + getPaddingBottom(), ((ViewGroup.MarginLayoutParams) layoutParams).height, true), z);
        } else {
            D2(view, RecyclerView.LayoutManager.V(v0(), w0(), getPaddingLeft() + getPaddingRight(), ((ViewGroup.MarginLayoutParams) layoutParams).width, true), RecyclerView.LayoutManager.V(this.B0, i0(), 0, ((ViewGroup.MarginLayoutParams) layoutParams).height, false), z);
        }
    }

    @Override // androidx.recyclerview.widget.RecyclerView.LayoutManager
    public int F(RecyclerView.x xVar) {
        return e2(xVar);
    }

    @Override // androidx.recyclerview.widget.RecyclerView.LayoutManager
    public int F1(int i, RecyclerView.t tVar, RecyclerView.x xVar) {
        return O2(i, tVar, xVar);
    }

    /* JADX WARN: Code restructure failed: missing block: B:87:0x0157, code lost:
        if (b2() != false) goto L83;
     */
    /*
        Code decompiled incorrectly, please refer to instructions dump.
        To view partially-correct code enable 'Show inconsistent code' option in preferences
    */
    public final void F2(androidx.recyclerview.widget.RecyclerView.t r9, androidx.recyclerview.widget.RecyclerView.x r10, boolean r11) {
        /*
            Method dump skipped, instructions count: 379
            To view this dump change 'Code comments level' option to 'DEBUG'
        */
        throw new UnsupportedOperationException("Method not decompiled: androidx.recyclerview.widget.StaggeredGridLayoutManager.F2(androidx.recyclerview.widget.RecyclerView$t, androidx.recyclerview.widget.RecyclerView$x, boolean):void");
    }

    @Override // androidx.recyclerview.widget.RecyclerView.LayoutManager
    public int G(RecyclerView.x xVar) {
        return f2(xVar);
    }

    @Override // androidx.recyclerview.widget.RecyclerView.LayoutManager
    public void G1(int i) {
        SavedState savedState = this.M0;
        if (savedState != null && savedState.a != i) {
            savedState.a();
        }
        this.G0 = i;
        this.H0 = Integer.MIN_VALUE;
        C1();
    }

    public final boolean G2(int i) {
        if (this.A0 == 0) {
            return (i == -1) != this.E0;
        }
        return ((i == -1) == this.E0) == C2();
    }

    @Override // androidx.recyclerview.widget.RecyclerView.LayoutManager
    public int H1(int i, RecyclerView.t tVar, RecyclerView.x xVar) {
        return O2(i, tVar, xVar);
    }

    public void H2(int i, RecyclerView.x xVar) {
        int i2;
        int s2;
        if (i > 0) {
            s2 = t2();
            i2 = 1;
        } else {
            i2 = -1;
            s2 = s2();
        }
        this.C0.a = true;
        X2(s2, xVar);
        P2(i2);
        l lVar = this.C0;
        lVar.c = s2 + lVar.d;
        lVar.b = Math.abs(i);
    }

    public final void I2(View view) {
        for (int i = this.w0 - 1; i >= 0; i--) {
            this.x0[i].u(view);
        }
    }

    public final void J2(RecyclerView.t tVar, l lVar) {
        int min;
        int min2;
        if (!lVar.a || lVar.i) {
            return;
        }
        if (lVar.b == 0) {
            if (lVar.e == -1) {
                K2(tVar, lVar.g);
            } else {
                L2(tVar, lVar.f);
            }
        } else if (lVar.e == -1) {
            int i = lVar.f;
            int v2 = i - v2(i);
            if (v2 < 0) {
                min2 = lVar.g;
            } else {
                min2 = lVar.g - Math.min(v2, lVar.b);
            }
            K2(tVar, min2);
        } else {
            int w2 = w2(lVar.g) - lVar.g;
            if (w2 < 0) {
                min = lVar.f;
            } else {
                min = Math.min(w2, lVar.b) + lVar.f;
            }
            L2(tVar, min);
        }
    }

    @Override // androidx.recyclerview.widget.RecyclerView.LayoutManager
    public void K0(int i) {
        super.K0(i);
        for (int i2 = 0; i2 < this.w0; i2++) {
            this.x0[i2].r(i);
        }
    }

    public final void K2(RecyclerView.t tVar, int i) {
        for (int U = U() - 1; U >= 0; U--) {
            View T = T(U);
            if (this.y0.g(T) < i || this.y0.q(T) < i) {
                return;
            }
            LayoutParams layoutParams = (LayoutParams) T.getLayoutParams();
            if (layoutParams.j0) {
                for (int i2 = 0; i2 < this.w0; i2++) {
                    if (this.x0[i2].a.size() == 1) {
                        return;
                    }
                }
                for (int i3 = 0; i3 < this.w0; i3++) {
                    this.x0[i3].s();
                }
            } else if (layoutParams.i0.a.size() == 1) {
                return;
            } else {
                layoutParams.i0.s();
            }
            v1(T, tVar);
        }
    }

    @Override // androidx.recyclerview.widget.RecyclerView.LayoutManager
    public void L0(int i) {
        super.L0(i);
        for (int i2 = 0; i2 < this.w0; i2++) {
            this.x0[i2].r(i);
        }
    }

    @Override // androidx.recyclerview.widget.RecyclerView.LayoutManager
    public void L1(Rect rect, int i, int i2) {
        int y;
        int y2;
        int paddingLeft = getPaddingLeft() + getPaddingRight();
        int paddingTop = getPaddingTop() + getPaddingBottom();
        if (this.A0 == 1) {
            y2 = RecyclerView.LayoutManager.y(i2, rect.height() + paddingTop, m0());
            y = RecyclerView.LayoutManager.y(i, (this.B0 * this.w0) + paddingLeft, n0());
        } else {
            y = RecyclerView.LayoutManager.y(i, rect.width() + paddingLeft, n0());
            y2 = RecyclerView.LayoutManager.y(i2, (this.B0 * this.w0) + paddingTop, m0());
        }
        K1(y, y2);
    }

    public final void L2(RecyclerView.t tVar, int i) {
        while (U() > 0) {
            View T = T(0);
            if (this.y0.d(T) > i || this.y0.p(T) > i) {
                return;
            }
            LayoutParams layoutParams = (LayoutParams) T.getLayoutParams();
            if (layoutParams.j0) {
                for (int i2 = 0; i2 < this.w0; i2++) {
                    if (this.x0[i2].a.size() == 1) {
                        return;
                    }
                }
                for (int i3 = 0; i3 < this.w0; i3++) {
                    this.x0[i3].t();
                }
            } else if (layoutParams.i0.a.size() == 1) {
                return;
            } else {
                layoutParams.i0.t();
            }
            v1(T, tVar);
        }
    }

    @Override // androidx.recyclerview.widget.RecyclerView.LayoutManager
    public void M0(RecyclerView.Adapter adapter, RecyclerView.Adapter adapter2) {
        this.I0.b();
        for (int i = 0; i < this.w0; i++) {
            this.x0[i].e();
        }
    }

    public final void M2() {
        if (this.z0.k() == 1073741824) {
            return;
        }
        float f = Utils.FLOAT_EPSILON;
        int U = U();
        for (int i = 0; i < U; i++) {
            View T = T(i);
            float e = this.z0.e(T);
            if (e >= f) {
                if (((LayoutParams) T.getLayoutParams()).e()) {
                    e = (e * 1.0f) / this.w0;
                }
                f = Math.max(f, e);
            }
        }
        int i2 = this.B0;
        int round = Math.round(f * this.w0);
        if (this.z0.k() == Integer.MIN_VALUE) {
            round = Math.min(round, this.z0.n());
        }
        Y2(round);
        if (this.B0 == i2) {
            return;
        }
        for (int i3 = 0; i3 < U; i3++) {
            View T2 = T(i3);
            LayoutParams layoutParams = (LayoutParams) T2.getLayoutParams();
            if (!layoutParams.j0) {
                if (C2() && this.A0 == 1) {
                    int i4 = this.w0;
                    int i5 = layoutParams.i0.e;
                    T2.offsetLeftAndRight(((-((i4 - 1) - i5)) * this.B0) - ((-((i4 - 1) - i5)) * i2));
                } else {
                    int i6 = layoutParams.i0.e;
                    int i7 = this.B0 * i6;
                    int i8 = i6 * i2;
                    if (this.A0 == 1) {
                        T2.offsetLeftAndRight(i7 - i8);
                    } else {
                        T2.offsetTopAndBottom(i7 - i8);
                    }
                }
            }
        }
    }

    public final void N2() {
        if (this.A0 != 1 && C2()) {
            this.E0 = !this.D0;
        } else {
            this.E0 = this.D0;
        }
    }

    @Override // androidx.recyclerview.widget.RecyclerView.LayoutManager
    public RecyclerView.LayoutParams O() {
        if (this.A0 == 0) {
            return new LayoutParams(-2, -1);
        }
        return new LayoutParams(-1, -2);
    }

    public int O2(int i, RecyclerView.t tVar, RecyclerView.x xVar) {
        if (U() == 0 || i == 0) {
            return 0;
        }
        H2(i, xVar);
        int k2 = k2(tVar, this.C0, xVar);
        if (this.C0.b >= k2) {
            i = i < 0 ? -k2 : k2;
        }
        this.y0.r(-i);
        this.K0 = this.E0;
        l lVar = this.C0;
        lVar.b = 0;
        J2(tVar, lVar);
        return i;
    }

    @Override // androidx.recyclerview.widget.RecyclerView.LayoutManager
    public RecyclerView.LayoutParams P(Context context, AttributeSet attributeSet) {
        return new LayoutParams(context, attributeSet);
    }

    public final void P2(int i) {
        l lVar = this.C0;
        lVar.e = i;
        lVar.d = this.E0 != (i == -1) ? -1 : 1;
    }

    @Override // androidx.recyclerview.widget.RecyclerView.LayoutManager
    public RecyclerView.LayoutParams Q(ViewGroup.LayoutParams layoutParams) {
        if (layoutParams instanceof ViewGroup.MarginLayoutParams) {
            return new LayoutParams((ViewGroup.MarginLayoutParams) layoutParams);
        }
        return new LayoutParams(layoutParams);
    }

    @Override // androidx.recyclerview.widget.RecyclerView.LayoutManager
    public void Q0(RecyclerView recyclerView, RecyclerView.t tVar) {
        super.Q0(recyclerView, tVar);
        x1(this.T0);
        for (int i = 0; i < this.w0; i++) {
            this.x0[i].e();
        }
        recyclerView.requestLayout();
    }

    public void Q2(int i) {
        if (i != 0 && i != 1) {
            throw new IllegalArgumentException("invalid orientation.");
        }
        r(null);
        if (i == this.A0) {
            return;
        }
        this.A0 = i;
        q qVar = this.y0;
        this.y0 = this.z0;
        this.z0 = qVar;
        C1();
    }

    @Override // androidx.recyclerview.widget.RecyclerView.LayoutManager
    public View R0(View view, int i, RecyclerView.t tVar, RecyclerView.x xVar) {
        View M;
        int s2;
        int g;
        int g2;
        int g3;
        View m;
        if (U() == 0 || (M = M(view)) == null) {
            return null;
        }
        N2();
        int g22 = g2(i);
        if (g22 == Integer.MIN_VALUE) {
            return null;
        }
        LayoutParams layoutParams = (LayoutParams) M.getLayoutParams();
        boolean z = layoutParams.j0;
        c cVar = layoutParams.i0;
        if (g22 == 1) {
            s2 = t2();
        } else {
            s2 = s2();
        }
        X2(s2, xVar);
        P2(g22);
        l lVar = this.C0;
        lVar.c = lVar.d + s2;
        lVar.b = (int) (this.y0.n() * 0.33333334f);
        l lVar2 = this.C0;
        lVar2.h = true;
        lVar2.a = false;
        k2(tVar, lVar2, xVar);
        this.K0 = this.E0;
        if (z || (m = cVar.m(s2, g22)) == null || m == M) {
            if (G2(g22)) {
                for (int i2 = this.w0 - 1; i2 >= 0; i2--) {
                    View m2 = this.x0[i2].m(s2, g22);
                    if (m2 != null && m2 != M) {
                        return m2;
                    }
                }
            } else {
                for (int i3 = 0; i3 < this.w0; i3++) {
                    View m3 = this.x0[i3].m(s2, g22);
                    if (m3 != null && m3 != M) {
                        return m3;
                    }
                }
            }
            boolean z2 = (this.D0 ^ true) == (g22 == -1);
            if (!z) {
                if (z2) {
                    g3 = cVar.f();
                } else {
                    g3 = cVar.g();
                }
                View N = N(g3);
                if (N != null && N != M) {
                    return N;
                }
            }
            if (G2(g22)) {
                for (int i4 = this.w0 - 1; i4 >= 0; i4--) {
                    if (i4 != cVar.e) {
                        if (z2) {
                            g2 = this.x0[i4].f();
                        } else {
                            g2 = this.x0[i4].g();
                        }
                        View N2 = N(g2);
                        if (N2 != null && N2 != M) {
                            return N2;
                        }
                    }
                }
            } else {
                for (int i5 = 0; i5 < this.w0; i5++) {
                    if (z2) {
                        g = this.x0[i5].f();
                    } else {
                        g = this.x0[i5].g();
                    }
                    View N3 = N(g);
                    if (N3 != null && N3 != M) {
                        return N3;
                    }
                }
            }
            return null;
        }
        return m;
    }

    @Override // androidx.recyclerview.widget.RecyclerView.LayoutManager
    public void R1(RecyclerView recyclerView, RecyclerView.x xVar, int i) {
        m mVar = new m(recyclerView.getContext());
        mVar.setTargetPosition(i);
        S1(mVar);
    }

    public void R2(boolean z) {
        r(null);
        SavedState savedState = this.M0;
        if (savedState != null && savedState.l0 != z) {
            savedState.l0 = z;
        }
        this.D0 = z;
        C1();
    }

    @Override // androidx.recyclerview.widget.RecyclerView.LayoutManager
    public void S0(AccessibilityEvent accessibilityEvent) {
        super.S0(accessibilityEvent);
        if (U() > 0) {
            View n2 = n2(false);
            View m2 = m2(false);
            if (n2 == null || m2 == null) {
                return;
            }
            int o0 = o0(n2);
            int o02 = o0(m2);
            if (o0 < o02) {
                accessibilityEvent.setFromIndex(o0);
                accessibilityEvent.setToIndex(o02);
                return;
            }
            accessibilityEvent.setFromIndex(o02);
            accessibilityEvent.setToIndex(o0);
        }
    }

    public void S2(int i) {
        r(null);
        if (i != this.w0) {
            B2();
            this.w0 = i;
            this.F0 = new BitSet(this.w0);
            this.x0 = new c[this.w0];
            for (int i2 = 0; i2 < this.w0; i2++) {
                this.x0[i2] = new c(i2);
            }
            C1();
        }
    }

    public final void T2(int i, int i2) {
        for (int i3 = 0; i3 < this.w0; i3++) {
            if (!this.x0[i3].a.isEmpty()) {
                Z2(this.x0[i3], i, i2);
            }
        }
    }

    @Override // androidx.recyclerview.widget.RecyclerView.LayoutManager
    public boolean U1() {
        return this.M0 == null;
    }

    public final boolean U2(RecyclerView.x xVar, b bVar) {
        int l2;
        if (this.K0) {
            l2 = p2(xVar.b());
        } else {
            l2 = l2(xVar.b());
        }
        bVar.a = l2;
        bVar.b = Integer.MIN_VALUE;
        return true;
    }

    public final void V1(View view) {
        for (int i = this.w0 - 1; i >= 0; i--) {
            this.x0[i].a(view);
        }
    }

    public boolean V2(RecyclerView.x xVar, b bVar) {
        int i;
        int m;
        if (!xVar.e() && (i = this.G0) != -1) {
            if (i >= 0 && i < xVar.b()) {
                SavedState savedState = this.M0;
                if (savedState != null && savedState.a != -1 && savedState.g0 >= 1) {
                    bVar.b = Integer.MIN_VALUE;
                    bVar.a = this.G0;
                } else {
                    View N = N(this.G0);
                    if (N != null) {
                        bVar.a = this.E0 ? t2() : s2();
                        if (this.H0 != Integer.MIN_VALUE) {
                            if (bVar.c) {
                                bVar.b = (this.y0.i() - this.H0) - this.y0.d(N);
                            } else {
                                bVar.b = (this.y0.m() + this.H0) - this.y0.g(N);
                            }
                            return true;
                        } else if (this.y0.e(N) > this.y0.n()) {
                            if (bVar.c) {
                                m = this.y0.i();
                            } else {
                                m = this.y0.m();
                            }
                            bVar.b = m;
                            return true;
                        } else {
                            int g = this.y0.g(N) - this.y0.m();
                            if (g < 0) {
                                bVar.b = -g;
                                return true;
                            }
                            int i2 = this.y0.i() - this.y0.d(N);
                            if (i2 < 0) {
                                bVar.b = i2;
                                return true;
                            }
                            bVar.b = Integer.MIN_VALUE;
                        }
                    } else {
                        int i3 = this.G0;
                        bVar.a = i3;
                        int i4 = this.H0;
                        if (i4 == Integer.MIN_VALUE) {
                            bVar.c = a2(i3) == 1;
                            bVar.a();
                        } else {
                            bVar.b(i4);
                        }
                        bVar.d = true;
                    }
                }
                return true;
            }
            this.G0 = -1;
            this.H0 = Integer.MIN_VALUE;
        }
        return false;
    }

    public final void W1(b bVar) {
        int m;
        SavedState savedState = this.M0;
        int i = savedState.g0;
        if (i > 0) {
            if (i == this.w0) {
                for (int i2 = 0; i2 < this.w0; i2++) {
                    this.x0[i2].e();
                    SavedState savedState2 = this.M0;
                    int i3 = savedState2.h0[i2];
                    if (i3 != Integer.MIN_VALUE) {
                        if (savedState2.m0) {
                            m = this.y0.i();
                        } else {
                            m = this.y0.m();
                        }
                        i3 += m;
                    }
                    this.x0[i2].v(i3);
                }
            } else {
                savedState.b();
                SavedState savedState3 = this.M0;
                savedState3.a = savedState3.f0;
            }
        }
        SavedState savedState4 = this.M0;
        this.L0 = savedState4.n0;
        R2(savedState4.l0);
        N2();
        SavedState savedState5 = this.M0;
        int i4 = savedState5.a;
        if (i4 != -1) {
            this.G0 = i4;
            bVar.c = savedState5.m0;
        } else {
            bVar.c = this.E0;
        }
        if (savedState5.i0 > 1) {
            LazySpanLookup lazySpanLookup = this.I0;
            lazySpanLookup.a = savedState5.j0;
            lazySpanLookup.b = savedState5.k0;
        }
    }

    public void W2(RecyclerView.x xVar, b bVar) {
        if (V2(xVar, bVar) || U2(xVar, bVar)) {
            return;
        }
        bVar.a();
        bVar.a = 0;
    }

    public boolean X1() {
        int l = this.x0[0].l(Integer.MIN_VALUE);
        for (int i = 1; i < this.w0; i++) {
            if (this.x0[i].l(Integer.MIN_VALUE) != l) {
                return false;
            }
        }
        return true;
    }

    public final void X2(int i, RecyclerView.x xVar) {
        int i2;
        int i3;
        int c2;
        l lVar = this.C0;
        boolean z = false;
        lVar.b = 0;
        lVar.c = i;
        if (!F0() || (c2 = xVar.c()) == -1) {
            i2 = 0;
            i3 = 0;
        } else {
            if (this.E0 == (c2 < i)) {
                i2 = this.y0.n();
                i3 = 0;
            } else {
                i3 = this.y0.n();
                i2 = 0;
            }
        }
        if (X()) {
            this.C0.f = this.y0.m() - i3;
            this.C0.g = this.y0.i() + i2;
        } else {
            this.C0.g = this.y0.h() + i2;
            this.C0.f = -i3;
        }
        l lVar2 = this.C0;
        lVar2.h = false;
        lVar2.a = true;
        if (this.y0.k() == 0 && this.y0.h() == 0) {
            z = true;
        }
        lVar2.i = z;
    }

    public boolean Y1() {
        int p = this.x0[0].p(Integer.MIN_VALUE);
        for (int i = 1; i < this.w0; i++) {
            if (this.x0[i].p(Integer.MIN_VALUE) != p) {
                return false;
            }
        }
        return true;
    }

    public void Y2(int i) {
        this.B0 = i / this.w0;
        this.N0 = View.MeasureSpec.makeMeasureSpec(i, this.z0.k());
    }

    @Override // androidx.recyclerview.widget.RecyclerView.LayoutManager
    public void Z0(RecyclerView recyclerView, int i, int i2) {
        z2(i, i2, 1);
    }

    public final void Z1(View view, LayoutParams layoutParams, l lVar) {
        if (lVar.e == 1) {
            if (layoutParams.j0) {
                V1(view);
            } else {
                layoutParams.i0.a(view);
            }
        } else if (layoutParams.j0) {
            I2(view);
        } else {
            layoutParams.i0.u(view);
        }
    }

    public final void Z2(c cVar, int i, int i2) {
        int j = cVar.j();
        if (i == -1) {
            if (cVar.o() + j <= i2) {
                this.F0.set(cVar.e, false);
            }
        } else if (cVar.k() - j >= i2) {
            this.F0.set(cVar.e, false);
        }
    }

    @Override // androidx.recyclerview.widget.RecyclerView.w.b
    public PointF a(int i) {
        int a2 = a2(i);
        PointF pointF = new PointF();
        if (a2 == 0) {
            return null;
        }
        if (this.A0 == 0) {
            pointF.x = a2;
            pointF.y = Utils.FLOAT_EPSILON;
        } else {
            pointF.x = Utils.FLOAT_EPSILON;
            pointF.y = a2;
        }
        return pointF;
    }

    @Override // androidx.recyclerview.widget.RecyclerView.LayoutManager
    public void a1(RecyclerView recyclerView) {
        this.I0.b();
        C1();
    }

    public final int a2(int i) {
        if (U() == 0) {
            return this.E0 ? 1 : -1;
        }
        return (i < s2()) != this.E0 ? -1 : 1;
    }

    public final int a3(int i, int i2, int i3) {
        if (i2 == 0 && i3 == 0) {
            return i;
        }
        int mode = View.MeasureSpec.getMode(i);
        return (mode == Integer.MIN_VALUE || mode == 1073741824) ? View.MeasureSpec.makeMeasureSpec(Math.max(0, (View.MeasureSpec.getSize(i) - i2) - i3), mode) : i;
    }

    @Override // androidx.recyclerview.widget.RecyclerView.LayoutManager
    public void b1(RecyclerView recyclerView, int i, int i2, int i3) {
        z2(i, i2, 8);
    }

    public boolean b2() {
        int s2;
        int t2;
        if (U() == 0 || this.J0 == 0 || !y0()) {
            return false;
        }
        if (this.E0) {
            s2 = t2();
            t2 = s2();
        } else {
            s2 = s2();
            t2 = t2();
        }
        if (s2 == 0 && A2() != null) {
            this.I0.b();
            D1();
            C1();
            return true;
        } else if (this.Q0) {
            int i = this.E0 ? -1 : 1;
            int i2 = t2 + 1;
            LazySpanLookup.FullSpanItem e = this.I0.e(s2, i2, i, true);
            if (e == null) {
                this.Q0 = false;
                this.I0.d(i2);
                return false;
            }
            LazySpanLookup.FullSpanItem e2 = this.I0.e(s2, e.a, i * (-1), true);
            if (e2 == null) {
                this.I0.d(e.a);
            } else {
                this.I0.d(e2.a + 1);
            }
            D1();
            C1();
            return true;
        } else {
            return false;
        }
    }

    @Override // androidx.recyclerview.widget.RecyclerView.LayoutManager
    public void c1(RecyclerView recyclerView, int i, int i2) {
        z2(i, i2, 2);
    }

    public final boolean c2(c cVar) {
        if (this.E0) {
            if (cVar.k() < this.y0.i()) {
                ArrayList<View> arrayList = cVar.a;
                return !cVar.n(arrayList.get(arrayList.size() - 1)).j0;
            }
        } else if (cVar.o() > this.y0.m()) {
            return !cVar.n(cVar.a.get(0)).j0;
        }
        return false;
    }

    public final int d2(RecyclerView.x xVar) {
        if (U() == 0) {
            return 0;
        }
        return t.a(xVar, this.y0, n2(!this.R0), m2(!this.R0), this, this.R0);
    }

    @Override // androidx.recyclerview.widget.RecyclerView.LayoutManager
    public void e1(RecyclerView recyclerView, int i, int i2, Object obj) {
        z2(i, i2, 4);
    }

    public final int e2(RecyclerView.x xVar) {
        if (U() == 0) {
            return 0;
        }
        return t.b(xVar, this.y0, n2(!this.R0), m2(!this.R0), this, this.R0, this.E0);
    }

    @Override // androidx.recyclerview.widget.RecyclerView.LayoutManager
    public void f1(RecyclerView.t tVar, RecyclerView.x xVar) {
        F2(tVar, xVar, true);
    }

    public final int f2(RecyclerView.x xVar) {
        if (U() == 0) {
            return 0;
        }
        return t.c(xVar, this.y0, n2(!this.R0), m2(!this.R0), this, this.R0);
    }

    @Override // androidx.recyclerview.widget.RecyclerView.LayoutManager
    public void g1(RecyclerView.x xVar) {
        super.g1(xVar);
        this.G0 = -1;
        this.H0 = Integer.MIN_VALUE;
        this.M0 = null;
        this.P0.c();
    }

    public final int g2(int i) {
        return i != 1 ? i != 2 ? i != 17 ? i != 33 ? i != 66 ? (i == 130 && this.A0 == 1) ? 1 : Integer.MIN_VALUE : this.A0 == 0 ? 1 : Integer.MIN_VALUE : this.A0 == 1 ? -1 : Integer.MIN_VALUE : this.A0 == 0 ? -1 : Integer.MIN_VALUE : (this.A0 != 1 && C2()) ? -1 : 1 : (this.A0 != 1 && C2()) ? 1 : -1;
    }

    public final LazySpanLookup.FullSpanItem h2(int i) {
        LazySpanLookup.FullSpanItem fullSpanItem = new LazySpanLookup.FullSpanItem();
        fullSpanItem.g0 = new int[this.w0];
        for (int i2 = 0; i2 < this.w0; i2++) {
            fullSpanItem.g0[i2] = i - this.x0[i2].l(i);
        }
        return fullSpanItem;
    }

    public final LazySpanLookup.FullSpanItem i2(int i) {
        LazySpanLookup.FullSpanItem fullSpanItem = new LazySpanLookup.FullSpanItem();
        fullSpanItem.g0 = new int[this.w0];
        for (int i2 = 0; i2 < this.w0; i2++) {
            fullSpanItem.g0[i2] = this.x0[i2].p(i) - i;
        }
        return fullSpanItem;
    }

    public final void j2() {
        this.y0 = q.b(this, this.A0);
        this.z0 = q.b(this, 1 - this.A0);
    }

    @Override // androidx.recyclerview.widget.RecyclerView.LayoutManager
    public void k1(Parcelable parcelable) {
        if (parcelable instanceof SavedState) {
            SavedState savedState = (SavedState) parcelable;
            this.M0 = savedState;
            if (this.G0 != -1) {
                savedState.a();
                this.M0.b();
            }
            C1();
        }
    }

    /* JADX WARN: Multi-variable type inference failed */
    /* JADX WARN: Type inference failed for: r9v0 */
    /* JADX WARN: Type inference failed for: r9v1, types: [int, boolean] */
    /* JADX WARN: Type inference failed for: r9v7 */
    public final int k2(RecyclerView.t tVar, l lVar, RecyclerView.x xVar) {
        int i;
        int m;
        int u2;
        c cVar;
        int e;
        int i2;
        int i3;
        int e2;
        boolean z;
        boolean Y1;
        ?? r9 = 0;
        this.F0.set(0, this.w0, true);
        if (this.C0.i) {
            i = lVar.e == 1 ? Integer.MAX_VALUE : Integer.MIN_VALUE;
        } else if (lVar.e == 1) {
            i = lVar.g + lVar.b;
        } else {
            i = lVar.f - lVar.b;
        }
        int i4 = i;
        T2(lVar.e, i4);
        if (this.E0) {
            m = this.y0.i();
        } else {
            m = this.y0.m();
        }
        int i5 = m;
        boolean z2 = false;
        while (lVar.a(xVar) && (this.C0.i || !this.F0.isEmpty())) {
            View b2 = lVar.b(tVar);
            LayoutParams layoutParams = (LayoutParams) b2.getLayoutParams();
            int a2 = layoutParams.a();
            int g = this.I0.g(a2);
            boolean z3 = g == -1 ? true : r9;
            if (z3) {
                cVar = layoutParams.j0 ? this.x0[r9] : y2(lVar);
                this.I0.n(a2, cVar);
            } else {
                cVar = this.x0[g];
            }
            c cVar2 = cVar;
            layoutParams.i0 = cVar2;
            if (lVar.e == 1) {
                o(b2);
            } else {
                p(b2, r9);
            }
            E2(b2, layoutParams, r9);
            if (lVar.e == 1) {
                int u22 = layoutParams.j0 ? u2(i5) : cVar2.l(i5);
                int e3 = this.y0.e(b2) + u22;
                if (z3 && layoutParams.j0) {
                    LazySpanLookup.FullSpanItem h2 = h2(u22);
                    h2.f0 = -1;
                    h2.a = a2;
                    this.I0.a(h2);
                }
                i2 = e3;
                e = u22;
            } else {
                int x2 = layoutParams.j0 ? x2(i5) : cVar2.p(i5);
                e = x2 - this.y0.e(b2);
                if (z3 && layoutParams.j0) {
                    LazySpanLookup.FullSpanItem i22 = i2(x2);
                    i22.f0 = 1;
                    i22.a = a2;
                    this.I0.a(i22);
                }
                i2 = x2;
            }
            if (layoutParams.j0 && lVar.d == -1) {
                if (z3) {
                    this.Q0 = true;
                } else {
                    if (lVar.e == 1) {
                        Y1 = X1();
                    } else {
                        Y1 = Y1();
                    }
                    if (!Y1) {
                        LazySpanLookup.FullSpanItem f = this.I0.f(a2);
                        if (f != null) {
                            f.h0 = true;
                        }
                        this.Q0 = true;
                    }
                }
            }
            Z1(b2, layoutParams, lVar);
            if (C2() && this.A0 == 1) {
                int i6 = layoutParams.j0 ? this.z0.i() : this.z0.i() - (((this.w0 - 1) - cVar2.e) * this.B0);
                e2 = i6;
                i3 = i6 - this.z0.e(b2);
            } else {
                int m2 = layoutParams.j0 ? this.z0.m() : (cVar2.e * this.B0) + this.z0.m();
                i3 = m2;
                e2 = this.z0.e(b2) + m2;
            }
            if (this.A0 == 1) {
                H0(b2, i3, e, e2, i2);
            } else {
                H0(b2, e, i3, i2, e2);
            }
            if (layoutParams.j0) {
                T2(this.C0.e, i4);
            } else {
                Z2(cVar2, this.C0.e, i4);
            }
            J2(tVar, this.C0);
            if (this.C0.h && b2.hasFocusable()) {
                if (layoutParams.j0) {
                    this.F0.clear();
                } else {
                    z = false;
                    this.F0.set(cVar2.e, false);
                    r9 = z;
                    z2 = true;
                }
            }
            z = false;
            r9 = z;
            z2 = true;
        }
        int i7 = r9;
        if (!z2) {
            J2(tVar, this.C0);
        }
        if (this.C0.e == -1) {
            u2 = this.y0.m() - x2(this.y0.m());
        } else {
            u2 = u2(this.y0.i()) - this.y0.i();
        }
        return u2 > 0 ? Math.min(lVar.b, u2) : i7;
    }

    @Override // androidx.recyclerview.widget.RecyclerView.LayoutManager
    public Parcelable l1() {
        int p;
        int m;
        int[] iArr;
        if (this.M0 != null) {
            return new SavedState(this.M0);
        }
        SavedState savedState = new SavedState();
        savedState.l0 = this.D0;
        savedState.m0 = this.K0;
        savedState.n0 = this.L0;
        LazySpanLookup lazySpanLookup = this.I0;
        if (lazySpanLookup != null && (iArr = lazySpanLookup.a) != null) {
            savedState.j0 = iArr;
            savedState.i0 = iArr.length;
            savedState.k0 = lazySpanLookup.b;
        } else {
            savedState.i0 = 0;
        }
        if (U() > 0) {
            savedState.a = this.K0 ? t2() : s2();
            savedState.f0 = o2();
            int i = this.w0;
            savedState.g0 = i;
            savedState.h0 = new int[i];
            for (int i2 = 0; i2 < this.w0; i2++) {
                if (this.K0) {
                    p = this.x0[i2].l(Integer.MIN_VALUE);
                    if (p != Integer.MIN_VALUE) {
                        m = this.y0.i();
                        p -= m;
                        savedState.h0[i2] = p;
                    } else {
                        savedState.h0[i2] = p;
                    }
                } else {
                    p = this.x0[i2].p(Integer.MIN_VALUE);
                    if (p != Integer.MIN_VALUE) {
                        m = this.y0.m();
                        p -= m;
                        savedState.h0[i2] = p;
                    } else {
                        savedState.h0[i2] = p;
                    }
                }
            }
        } else {
            savedState.a = -1;
            savedState.f0 = -1;
            savedState.g0 = 0;
        }
        return savedState;
    }

    public final int l2(int i) {
        int U = U();
        for (int i2 = 0; i2 < U; i2++) {
            int o0 = o0(T(i2));
            if (o0 >= 0 && o0 < i) {
                return o0;
            }
        }
        return 0;
    }

    @Override // androidx.recyclerview.widget.RecyclerView.LayoutManager
    public void m1(int i) {
        if (i == 0) {
            b2();
        }
    }

    public View m2(boolean z) {
        int m = this.y0.m();
        int i = this.y0.i();
        View view = null;
        for (int U = U() - 1; U >= 0; U--) {
            View T = T(U);
            int g = this.y0.g(T);
            int d = this.y0.d(T);
            if (d > m && g < i) {
                if (d <= i || !z) {
                    return T;
                }
                if (view == null) {
                    view = T;
                }
            }
        }
        return view;
    }

    public View n2(boolean z) {
        int m = this.y0.m();
        int i = this.y0.i();
        int U = U();
        View view = null;
        for (int i2 = 0; i2 < U; i2++) {
            View T = T(i2);
            int g = this.y0.g(T);
            if (this.y0.d(T) > m && g < i) {
                if (g >= m || !z) {
                    return T;
                }
                if (view == null) {
                    view = T;
                }
            }
        }
        return view;
    }

    public int o2() {
        View m2 = this.E0 ? m2(true) : n2(true);
        if (m2 == null) {
            return -1;
        }
        return o0(m2);
    }

    public final int p2(int i) {
        for (int U = U() - 1; U >= 0; U--) {
            int o0 = o0(T(U));
            if (o0 >= 0 && o0 < i) {
                return o0;
            }
        }
        return 0;
    }

    public final void q2(RecyclerView.t tVar, RecyclerView.x xVar, boolean z) {
        int i;
        int u2 = u2(Integer.MIN_VALUE);
        if (u2 != Integer.MIN_VALUE && (i = this.y0.i() - u2) > 0) {
            int i2 = i - (-O2(-i, tVar, xVar));
            if (!z || i2 <= 0) {
                return;
            }
            this.y0.r(i2);
        }
    }

    @Override // androidx.recyclerview.widget.RecyclerView.LayoutManager
    public void r(String str) {
        if (this.M0 == null) {
            super.r(str);
        }
    }

    public final void r2(RecyclerView.t tVar, RecyclerView.x xVar, boolean z) {
        int m;
        int x2 = x2(Integer.MAX_VALUE);
        if (x2 != Integer.MAX_VALUE && (m = x2 - this.y0.m()) > 0) {
            int O2 = m - O2(m, tVar, xVar);
            if (!z || O2 <= 0) {
                return;
            }
            this.y0.r(-O2);
        }
    }

    public int s2() {
        if (U() == 0) {
            return 0;
        }
        return o0(T(0));
    }

    public int t2() {
        int U = U();
        if (U == 0) {
            return 0;
        }
        return o0(T(U - 1));
    }

    public final int u2(int i) {
        int l = this.x0[0].l(i);
        for (int i2 = 1; i2 < this.w0; i2++) {
            int l2 = this.x0[i2].l(i);
            if (l2 > l) {
                l = l2;
            }
        }
        return l;
    }

    @Override // androidx.recyclerview.widget.RecyclerView.LayoutManager
    public boolean v() {
        return this.A0 == 0;
    }

    public final int v2(int i) {
        int p = this.x0[0].p(i);
        for (int i2 = 1; i2 < this.w0; i2++) {
            int p2 = this.x0[i2].p(i);
            if (p2 > p) {
                p = p2;
            }
        }
        return p;
    }

    @Override // androidx.recyclerview.widget.RecyclerView.LayoutManager
    public boolean w() {
        return this.A0 == 1;
    }

    public final int w2(int i) {
        int l = this.x0[0].l(i);
        for (int i2 = 1; i2 < this.w0; i2++) {
            int l2 = this.x0[i2].l(i);
            if (l2 < l) {
                l = l2;
            }
        }
        return l;
    }

    @Override // androidx.recyclerview.widget.RecyclerView.LayoutManager
    public boolean x(RecyclerView.LayoutParams layoutParams) {
        return layoutParams instanceof LayoutParams;
    }

    public final int x2(int i) {
        int p = this.x0[0].p(i);
        for (int i2 = 1; i2 < this.w0; i2++) {
            int p2 = this.x0[i2].p(i);
            if (p2 < p) {
                p = p2;
            }
        }
        return p;
    }

    public final c y2(l lVar) {
        int i;
        int i2;
        int i3 = -1;
        if (G2(lVar.e)) {
            i = this.w0 - 1;
            i2 = -1;
        } else {
            i = 0;
            i3 = this.w0;
            i2 = 1;
        }
        c cVar = null;
        if (lVar.e == 1) {
            int i4 = Integer.MAX_VALUE;
            int m = this.y0.m();
            while (i != i3) {
                c cVar2 = this.x0[i];
                int l = cVar2.l(m);
                if (l < i4) {
                    cVar = cVar2;
                    i4 = l;
                }
                i += i2;
            }
            return cVar;
        }
        int i5 = Integer.MIN_VALUE;
        int i6 = this.y0.i();
        while (i != i3) {
            c cVar3 = this.x0[i];
            int p = cVar3.p(i6);
            if (p > i5) {
                cVar = cVar3;
                i5 = p;
            }
            i += i2;
        }
        return cVar;
    }

    @Override // androidx.recyclerview.widget.RecyclerView.LayoutManager
    public void z(int i, int i2, RecyclerView.x xVar, RecyclerView.LayoutManager.c cVar) {
        int l;
        int i3;
        if (this.A0 != 0) {
            i = i2;
        }
        if (U() == 0 || i == 0) {
            return;
        }
        H2(i, xVar);
        int[] iArr = this.S0;
        if (iArr == null || iArr.length < this.w0) {
            this.S0 = new int[this.w0];
        }
        int i4 = 0;
        for (int i5 = 0; i5 < this.w0; i5++) {
            l lVar = this.C0;
            if (lVar.d == -1) {
                l = lVar.f;
                i3 = this.x0[i5].p(l);
            } else {
                l = this.x0[i5].l(lVar.g);
                i3 = this.C0.g;
            }
            int i6 = l - i3;
            if (i6 >= 0) {
                this.S0[i4] = i6;
                i4++;
            }
        }
        Arrays.sort(this.S0, 0, i4);
        for (int i7 = 0; i7 < i4 && this.C0.a(xVar); i7++) {
            cVar.a(this.C0.c, this.S0[i7]);
            l lVar2 = this.C0;
            lVar2.c += lVar2.d;
        }
    }

    @Override // androidx.recyclerview.widget.RecyclerView.LayoutManager
    public boolean z0() {
        return this.J0 != 0;
    }

    /* JADX WARN: Removed duplicated region for block: B:15:0x0025  */
    /* JADX WARN: Removed duplicated region for block: B:21:0x003c  */
    /* JADX WARN: Removed duplicated region for block: B:23:0x0043 A[RETURN] */
    /* JADX WARN: Removed duplicated region for block: B:24:0x0044  */
    /*
        Code decompiled incorrectly, please refer to instructions dump.
        To view partially-correct code enable 'Show inconsistent code' option in preferences
    */
    public final void z2(int r7, int r8, int r9) {
        /*
            r6 = this;
            boolean r0 = r6.E0
            if (r0 == 0) goto L9
            int r0 = r6.t2()
            goto Ld
        L9:
            int r0 = r6.s2()
        Ld:
            r1 = 8
            if (r9 != r1) goto L1a
            if (r7 >= r8) goto L16
            int r2 = r8 + 1
            goto L1c
        L16:
            int r2 = r7 + 1
            r3 = r8
            goto L1d
        L1a:
            int r2 = r7 + r8
        L1c:
            r3 = r7
        L1d:
            androidx.recyclerview.widget.StaggeredGridLayoutManager$LazySpanLookup r4 = r6.I0
            r4.h(r3)
            r4 = 1
            if (r9 == r4) goto L3c
            r5 = 2
            if (r9 == r5) goto L36
            if (r9 == r1) goto L2b
            goto L41
        L2b:
            androidx.recyclerview.widget.StaggeredGridLayoutManager$LazySpanLookup r9 = r6.I0
            r9.k(r7, r4)
            androidx.recyclerview.widget.StaggeredGridLayoutManager$LazySpanLookup r7 = r6.I0
            r7.j(r8, r4)
            goto L41
        L36:
            androidx.recyclerview.widget.StaggeredGridLayoutManager$LazySpanLookup r9 = r6.I0
            r9.k(r7, r8)
            goto L41
        L3c:
            androidx.recyclerview.widget.StaggeredGridLayoutManager$LazySpanLookup r9 = r6.I0
            r9.j(r7, r8)
        L41:
            if (r2 > r0) goto L44
            return
        L44:
            boolean r7 = r6.E0
            if (r7 == 0) goto L4d
            int r7 = r6.s2()
            goto L51
        L4d:
            int r7 = r6.t2()
        L51:
            if (r3 > r7) goto L56
            r6.C1()
        L56:
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: androidx.recyclerview.widget.StaggeredGridLayoutManager.z2(int, int, int):void");
    }

    /* loaded from: classes.dex */
    public static class LazySpanLookup {
        public int[] a;
        public List<FullSpanItem> b;

        public void a(FullSpanItem fullSpanItem) {
            if (this.b == null) {
                this.b = new ArrayList();
            }
            int size = this.b.size();
            for (int i = 0; i < size; i++) {
                FullSpanItem fullSpanItem2 = this.b.get(i);
                if (fullSpanItem2.a == fullSpanItem.a) {
                    this.b.remove(i);
                }
                if (fullSpanItem2.a >= fullSpanItem.a) {
                    this.b.add(i, fullSpanItem);
                    return;
                }
            }
            this.b.add(fullSpanItem);
        }

        public void b() {
            int[] iArr = this.a;
            if (iArr != null) {
                Arrays.fill(iArr, -1);
            }
            this.b = null;
        }

        public void c(int i) {
            int[] iArr = this.a;
            if (iArr == null) {
                int[] iArr2 = new int[Math.max(i, 10) + 1];
                this.a = iArr2;
                Arrays.fill(iArr2, -1);
            } else if (i >= iArr.length) {
                int[] iArr3 = new int[o(i)];
                this.a = iArr3;
                System.arraycopy(iArr, 0, iArr3, 0, iArr.length);
                int[] iArr4 = this.a;
                Arrays.fill(iArr4, iArr.length, iArr4.length, -1);
            }
        }

        public int d(int i) {
            List<FullSpanItem> list = this.b;
            if (list != null) {
                for (int size = list.size() - 1; size >= 0; size--) {
                    if (this.b.get(size).a >= i) {
                        this.b.remove(size);
                    }
                }
            }
            return h(i);
        }

        public FullSpanItem e(int i, int i2, int i3, boolean z) {
            List<FullSpanItem> list = this.b;
            if (list == null) {
                return null;
            }
            int size = list.size();
            for (int i4 = 0; i4 < size; i4++) {
                FullSpanItem fullSpanItem = this.b.get(i4);
                int i5 = fullSpanItem.a;
                if (i5 >= i2) {
                    return null;
                }
                if (i5 >= i && (i3 == 0 || fullSpanItem.f0 == i3 || (z && fullSpanItem.h0))) {
                    return fullSpanItem;
                }
            }
            return null;
        }

        public FullSpanItem f(int i) {
            List<FullSpanItem> list = this.b;
            if (list == null) {
                return null;
            }
            for (int size = list.size() - 1; size >= 0; size--) {
                FullSpanItem fullSpanItem = this.b.get(size);
                if (fullSpanItem.a == i) {
                    return fullSpanItem;
                }
            }
            return null;
        }

        public int g(int i) {
            int[] iArr = this.a;
            if (iArr == null || i >= iArr.length) {
                return -1;
            }
            return iArr[i];
        }

        public int h(int i) {
            int[] iArr = this.a;
            if (iArr != null && i < iArr.length) {
                int i2 = i(i);
                if (i2 == -1) {
                    int[] iArr2 = this.a;
                    Arrays.fill(iArr2, i, iArr2.length, -1);
                    return this.a.length;
                }
                int min = Math.min(i2 + 1, this.a.length);
                Arrays.fill(this.a, i, min, -1);
                return min;
            }
            return -1;
        }

        public final int i(int i) {
            if (this.b == null) {
                return -1;
            }
            FullSpanItem f = f(i);
            if (f != null) {
                this.b.remove(f);
            }
            int size = this.b.size();
            int i2 = 0;
            while (true) {
                if (i2 >= size) {
                    i2 = -1;
                    break;
                } else if (this.b.get(i2).a >= i) {
                    break;
                } else {
                    i2++;
                }
            }
            if (i2 != -1) {
                this.b.remove(i2);
                return this.b.get(i2).a;
            }
            return -1;
        }

        public void j(int i, int i2) {
            int[] iArr = this.a;
            if (iArr == null || i >= iArr.length) {
                return;
            }
            int i3 = i + i2;
            c(i3);
            int[] iArr2 = this.a;
            System.arraycopy(iArr2, i, iArr2, i3, (iArr2.length - i) - i2);
            Arrays.fill(this.a, i, i3, -1);
            l(i, i2);
        }

        public void k(int i, int i2) {
            int[] iArr = this.a;
            if (iArr == null || i >= iArr.length) {
                return;
            }
            int i3 = i + i2;
            c(i3);
            int[] iArr2 = this.a;
            System.arraycopy(iArr2, i3, iArr2, i, (iArr2.length - i) - i2);
            int[] iArr3 = this.a;
            Arrays.fill(iArr3, iArr3.length - i2, iArr3.length, -1);
            m(i, i2);
        }

        public final void l(int i, int i2) {
            List<FullSpanItem> list = this.b;
            if (list == null) {
                return;
            }
            for (int size = list.size() - 1; size >= 0; size--) {
                FullSpanItem fullSpanItem = this.b.get(size);
                int i3 = fullSpanItem.a;
                if (i3 >= i) {
                    fullSpanItem.a = i3 + i2;
                }
            }
        }

        public final void m(int i, int i2) {
            List<FullSpanItem> list = this.b;
            if (list == null) {
                return;
            }
            int i3 = i + i2;
            for (int size = list.size() - 1; size >= 0; size--) {
                FullSpanItem fullSpanItem = this.b.get(size);
                int i4 = fullSpanItem.a;
                if (i4 >= i) {
                    if (i4 < i3) {
                        this.b.remove(size);
                    } else {
                        fullSpanItem.a = i4 - i2;
                    }
                }
            }
        }

        public void n(int i, c cVar) {
            c(i);
            this.a[i] = cVar.e;
        }

        public int o(int i) {
            int length = this.a.length;
            while (length <= i) {
                length *= 2;
            }
            return length;
        }

        @SuppressLint({"BanParcelableUsage"})
        /* loaded from: classes.dex */
        public static class FullSpanItem implements Parcelable {
            public static final Parcelable.Creator<FullSpanItem> CREATOR = new a();
            public int a;
            public int f0;
            public int[] g0;
            public boolean h0;

            /* loaded from: classes.dex */
            public class a implements Parcelable.Creator<FullSpanItem> {
                @Override // android.os.Parcelable.Creator
                /* renamed from: a */
                public FullSpanItem createFromParcel(Parcel parcel) {
                    return new FullSpanItem(parcel);
                }

                @Override // android.os.Parcelable.Creator
                /* renamed from: b */
                public FullSpanItem[] newArray(int i) {
                    return new FullSpanItem[i];
                }
            }

            public FullSpanItem(Parcel parcel) {
                this.a = parcel.readInt();
                this.f0 = parcel.readInt();
                this.h0 = parcel.readInt() == 1;
                int readInt = parcel.readInt();
                if (readInt > 0) {
                    int[] iArr = new int[readInt];
                    this.g0 = iArr;
                    parcel.readIntArray(iArr);
                }
            }

            public int a(int i) {
                int[] iArr = this.g0;
                if (iArr == null) {
                    return 0;
                }
                return iArr[i];
            }

            @Override // android.os.Parcelable
            public int describeContents() {
                return 0;
            }

            public String toString() {
                return "FullSpanItem{mPosition=" + this.a + ", mGapDir=" + this.f0 + ", mHasUnwantedGapAfter=" + this.h0 + ", mGapPerSpan=" + Arrays.toString(this.g0) + '}';
            }

            @Override // android.os.Parcelable
            public void writeToParcel(Parcel parcel, int i) {
                parcel.writeInt(this.a);
                parcel.writeInt(this.f0);
                parcel.writeInt(this.h0 ? 1 : 0);
                int[] iArr = this.g0;
                if (iArr != null && iArr.length > 0) {
                    parcel.writeInt(iArr.length);
                    parcel.writeIntArray(this.g0);
                    return;
                }
                parcel.writeInt(0);
            }

            public FullSpanItem() {
            }
        }
    }

    public StaggeredGridLayoutManager(int i, int i2) {
        this.A0 = i2;
        S2(i);
        this.C0 = new l();
        j2();
    }
}
