package androidx.recyclerview.widget;

import java.util.ArrayDeque;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.Iterator;
import java.util.List;

/* compiled from: DiffUtil.java */
/* loaded from: classes.dex */
public class g {
    public static final Comparator<d> a = new a();

    /* compiled from: DiffUtil.java */
    /* loaded from: classes.dex */
    public class a implements Comparator<d> {
        @Override // java.util.Comparator
        /* renamed from: a */
        public int compare(d dVar, d dVar2) {
            return dVar.a - dVar2.a;
        }
    }

    /* compiled from: DiffUtil.java */
    /* loaded from: classes.dex */
    public static abstract class b {
        public abstract boolean areContentsTheSame(int i, int i2);

        public abstract boolean areItemsTheSame(int i, int i2);

        public Object getChangePayload(int i, int i2) {
            return null;
        }

        public abstract int getNewListSize();

        public abstract int getOldListSize();
    }

    /* compiled from: DiffUtil.java */
    /* loaded from: classes.dex */
    public static class c {
        public final int[] a;
        public final int b;

        public c(int i) {
            int[] iArr = new int[i];
            this.a = iArr;
            this.b = iArr.length / 2;
        }

        public int[] a() {
            return this.a;
        }

        public int b(int i) {
            return this.a[i + this.b];
        }

        public void c(int i, int i2) {
            this.a[i + this.b] = i2;
        }
    }

    /* compiled from: DiffUtil.java */
    /* loaded from: classes.dex */
    public static class d {
        public final int a;
        public final int b;
        public final int c;

        public d(int i, int i2, int i3) {
            this.a = i;
            this.b = i2;
            this.c = i3;
        }

        public int a() {
            return this.a + this.c;
        }

        public int b() {
            return this.b + this.c;
        }
    }

    /* compiled from: DiffUtil.java */
    /* loaded from: classes.dex */
    public static class e {
        public final List<d> a;
        public final int[] b;
        public final int[] c;
        public final b d;
        public final int e;
        public final int f;
        public final boolean g;

        public e(b bVar, List<d> list, int[] iArr, int[] iArr2, boolean z) {
            this.a = list;
            this.b = iArr;
            this.c = iArr2;
            Arrays.fill(iArr, 0);
            Arrays.fill(iArr2, 0);
            this.d = bVar;
            this.e = bVar.getOldListSize();
            this.f = bVar.getNewListSize();
            this.g = z;
            a();
            e();
        }

        public static C0059g g(Collection<C0059g> collection, int i, boolean z) {
            C0059g c0059g;
            Iterator<C0059g> it = collection.iterator();
            while (true) {
                if (!it.hasNext()) {
                    c0059g = null;
                    break;
                }
                c0059g = it.next();
                if (c0059g.a == i && c0059g.c == z) {
                    it.remove();
                    break;
                }
            }
            while (it.hasNext()) {
                C0059g next = it.next();
                if (z) {
                    next.b--;
                } else {
                    next.b++;
                }
            }
            return c0059g;
        }

        public final void a() {
            d dVar = this.a.isEmpty() ? null : this.a.get(0);
            if (dVar == null || dVar.a != 0 || dVar.b != 0) {
                this.a.add(0, new d(0, 0, 0));
            }
            this.a.add(new d(this.e, this.f, 0));
        }

        public int b(int i) {
            if (i >= 0 && i < this.e) {
                int i2 = this.b[i];
                if ((i2 & 15) == 0) {
                    return -1;
                }
                return i2 >> 4;
            }
            throw new IndexOutOfBoundsException("Index out of bounds - passed position = " + i + ", old list size = " + this.e);
        }

        public void c(i02 i02Var) {
            mo moVar;
            int i;
            if (i02Var instanceof mo) {
                moVar = (mo) i02Var;
            } else {
                moVar = new mo(i02Var);
            }
            int i2 = this.e;
            ArrayDeque arrayDeque = new ArrayDeque();
            int i3 = this.e;
            int i4 = this.f;
            for (int size = this.a.size() - 1; size >= 0; size--) {
                d dVar = this.a.get(size);
                int a = dVar.a();
                int b = dVar.b();
                while (true) {
                    if (i3 <= a) {
                        break;
                    }
                    i3--;
                    int i5 = this.b[i3];
                    if ((i5 & 12) != 0) {
                        int i6 = i5 >> 4;
                        C0059g g = g(arrayDeque, i6, false);
                        if (g != null) {
                            int i7 = (i2 - g.b) - 1;
                            moVar.onMoved(i3, i7);
                            if ((i5 & 4) != 0) {
                                moVar.onChanged(i7, 1, this.d.getChangePayload(i3, i6));
                            }
                        } else {
                            arrayDeque.add(new C0059g(i3, (i2 - i3) - 1, true));
                        }
                    } else {
                        moVar.onRemoved(i3, 1);
                        i2--;
                    }
                }
                while (i4 > b) {
                    i4--;
                    int i8 = this.c[i4];
                    if ((i8 & 12) != 0) {
                        int i9 = i8 >> 4;
                        C0059g g2 = g(arrayDeque, i9, true);
                        if (g2 == null) {
                            arrayDeque.add(new C0059g(i4, i2 - i3, false));
                        } else {
                            moVar.onMoved((i2 - g2.b) - 1, i3);
                            if ((i8 & 4) != 0) {
                                moVar.onChanged(i3, 1, this.d.getChangePayload(i9, i4));
                            }
                        }
                    } else {
                        moVar.onInserted(i3, 1);
                        i2++;
                    }
                }
                int i10 = dVar.a;
                int i11 = dVar.b;
                for (i = 0; i < dVar.c; i++) {
                    if ((this.b[i10] & 15) == 2) {
                        moVar.onChanged(i10, 1, this.d.getChangePayload(i10, i11));
                    }
                    i10++;
                    i11++;
                }
                i3 = dVar.a;
                i4 = dVar.b;
            }
            moVar.a();
        }

        public final void d(int i) {
            int size = this.a.size();
            int i2 = 0;
            for (int i3 = 0; i3 < size; i3++) {
                d dVar = this.a.get(i3);
                while (i2 < dVar.b) {
                    if (this.c[i2] == 0 && this.d.areItemsTheSame(i, i2)) {
                        int i4 = this.d.areContentsTheSame(i, i2) ? 8 : 4;
                        this.b[i] = (i2 << 4) | i4;
                        this.c[i2] = (i << 4) | i4;
                        return;
                    }
                    i2++;
                }
                i2 = dVar.b();
            }
        }

        public final void e() {
            for (d dVar : this.a) {
                for (int i = 0; i < dVar.c; i++) {
                    int i2 = dVar.a + i;
                    int i3 = dVar.b + i;
                    int i4 = this.d.areContentsTheSame(i2, i3) ? 1 : 2;
                    this.b[i2] = (i3 << 4) | i4;
                    this.c[i3] = (i2 << 4) | i4;
                }
            }
            if (this.g) {
                f();
            }
        }

        public final void f() {
            int i = 0;
            for (d dVar : this.a) {
                while (i < dVar.a) {
                    if (this.b[i] == 0) {
                        d(i);
                    }
                    i++;
                }
                i = dVar.a();
            }
        }
    }

    /* compiled from: DiffUtil.java */
    /* loaded from: classes.dex */
    public static abstract class f<T> {
        public abstract boolean areContentsTheSame(T t, T t2);

        public abstract boolean areItemsTheSame(T t, T t2);

        public Object getChangePayload(T t, T t2) {
            return null;
        }
    }

    /* compiled from: DiffUtil.java */
    /* renamed from: androidx.recyclerview.widget.g$g  reason: collision with other inner class name */
    /* loaded from: classes.dex */
    public static class C0059g {
        public int a;
        public int b;
        public boolean c;

        public C0059g(int i, int i2, boolean z) {
            this.a = i;
            this.b = i2;
            this.c = z;
        }
    }

    /* compiled from: DiffUtil.java */
    /* loaded from: classes.dex */
    public static class h {
        public int a;
        public int b;
        public int c;
        public int d;

        public h() {
        }

        public int a() {
            return this.d - this.c;
        }

        public int b() {
            return this.b - this.a;
        }

        public h(int i, int i2, int i3, int i4) {
            this.a = i;
            this.b = i2;
            this.c = i3;
            this.d = i4;
        }
    }

    /* compiled from: DiffUtil.java */
    /* loaded from: classes.dex */
    public static class i {
        public int a;
        public int b;
        public int c;
        public int d;
        public boolean e;

        public int a() {
            return Math.min(this.c - this.a, this.d - this.b);
        }

        public boolean b() {
            return this.d - this.b != this.c - this.a;
        }

        public boolean c() {
            return this.d - this.b > this.c - this.a;
        }

        public d d() {
            if (b()) {
                if (this.e) {
                    return new d(this.a, this.b, a());
                }
                if (c()) {
                    return new d(this.a, this.b + 1, a());
                }
                return new d(this.a + 1, this.b, a());
            }
            int i = this.a;
            return new d(i, this.b, this.c - i);
        }
    }

    public static i a(h hVar, b bVar, c cVar, c cVar2, int i2) {
        int b2;
        int i3;
        int i4;
        boolean z = (hVar.b() - hVar.a()) % 2 == 0;
        int b3 = hVar.b() - hVar.a();
        int i5 = -i2;
        for (int i6 = i5; i6 <= i2; i6 += 2) {
            if (i6 != i5 && (i6 == i2 || cVar2.b(i6 + 1) >= cVar2.b(i6 - 1))) {
                b2 = cVar2.b(i6 - 1);
                i3 = b2 - 1;
            } else {
                b2 = cVar2.b(i6 + 1);
                i3 = b2;
            }
            int i7 = hVar.d - ((hVar.b - i3) - i6);
            int i8 = (i2 == 0 || i3 != b2) ? i7 : i7 + 1;
            while (i3 > hVar.a && i7 > hVar.c && bVar.areItemsTheSame(i3 - 1, i7 - 1)) {
                i3--;
                i7--;
            }
            cVar2.c(i6, i3);
            if (z && (i4 = b3 - i6) >= i5 && i4 <= i2 && cVar.b(i4) >= i3) {
                i iVar = new i();
                iVar.a = i3;
                iVar.b = i7;
                iVar.c = b2;
                iVar.d = i8;
                iVar.e = true;
                return iVar;
            }
        }
        return null;
    }

    public static e b(b bVar) {
        return c(bVar, true);
    }

    public static e c(b bVar, boolean z) {
        int oldListSize = bVar.getOldListSize();
        int newListSize = bVar.getNewListSize();
        ArrayList arrayList = new ArrayList();
        ArrayList arrayList2 = new ArrayList();
        arrayList2.add(new h(0, oldListSize, 0, newListSize));
        int i2 = ((((oldListSize + newListSize) + 1) / 2) * 2) + 1;
        c cVar = new c(i2);
        c cVar2 = new c(i2);
        ArrayList arrayList3 = new ArrayList();
        while (!arrayList2.isEmpty()) {
            h hVar = (h) arrayList2.remove(arrayList2.size() - 1);
            i e2 = e(hVar, bVar, cVar, cVar2);
            if (e2 != null) {
                if (e2.a() > 0) {
                    arrayList.add(e2.d());
                }
                h hVar2 = arrayList3.isEmpty() ? new h() : (h) arrayList3.remove(arrayList3.size() - 1);
                hVar2.a = hVar.a;
                hVar2.c = hVar.c;
                hVar2.b = e2.a;
                hVar2.d = e2.b;
                arrayList2.add(hVar2);
                hVar.b = hVar.b;
                hVar.d = hVar.d;
                hVar.a = e2.c;
                hVar.c = e2.d;
                arrayList2.add(hVar);
            } else {
                arrayList3.add(hVar);
            }
        }
        Collections.sort(arrayList, a);
        return new e(bVar, arrayList, cVar.a(), cVar2.a(), z);
    }

    public static i d(h hVar, b bVar, c cVar, c cVar2, int i2) {
        int b2;
        int i3;
        int i4;
        boolean z = Math.abs(hVar.b() - hVar.a()) % 2 == 1;
        int b3 = hVar.b() - hVar.a();
        int i5 = -i2;
        for (int i6 = i5; i6 <= i2; i6 += 2) {
            if (i6 != i5 && (i6 == i2 || cVar.b(i6 + 1) <= cVar.b(i6 - 1))) {
                b2 = cVar.b(i6 - 1);
                i3 = b2 + 1;
            } else {
                b2 = cVar.b(i6 + 1);
                i3 = b2;
            }
            int i7 = (hVar.c + (i3 - hVar.a)) - i6;
            int i8 = (i2 == 0 || i3 != b2) ? i7 : i7 - 1;
            while (i3 < hVar.b && i7 < hVar.d && bVar.areItemsTheSame(i3, i7)) {
                i3++;
                i7++;
            }
            cVar.c(i6, i3);
            if (z && (i4 = b3 - i6) >= i5 + 1 && i4 <= i2 - 1 && cVar2.b(i4) <= i3) {
                i iVar = new i();
                iVar.a = b2;
                iVar.b = i8;
                iVar.c = i3;
                iVar.d = i7;
                iVar.e = false;
                return iVar;
            }
        }
        return null;
    }

    public static i e(h hVar, b bVar, c cVar, c cVar2) {
        if (hVar.b() >= 1 && hVar.a() >= 1) {
            int b2 = ((hVar.b() + hVar.a()) + 1) / 2;
            cVar.c(1, hVar.a);
            cVar2.c(1, hVar.b);
            for (int i2 = 0; i2 < b2; i2++) {
                i d2 = d(hVar, bVar, cVar, cVar2, i2);
                if (d2 != null) {
                    return d2;
                }
                i a2 = a(hVar, bVar, cVar, cVar2, i2);
                if (a2 != null) {
                    return a2;
                }
            }
        }
        return null;
    }
}
