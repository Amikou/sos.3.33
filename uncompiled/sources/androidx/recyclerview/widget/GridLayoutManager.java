package androidx.recyclerview.widget;

import android.content.Context;
import android.graphics.Rect;
import android.util.AttributeSet;
import android.util.SparseIntArray;
import android.view.View;
import android.view.ViewGroup;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import defpackage.b6;

/* loaded from: classes.dex */
public class GridLayoutManager extends LinearLayoutManager {
    public boolean M0;
    public int N0;
    public int[] O0;
    public View[] P0;
    public final SparseIntArray Q0;
    public final SparseIntArray R0;
    public b S0;
    public final Rect T0;
    public boolean U0;

    /* loaded from: classes.dex */
    public static final class a extends b {
        @Override // androidx.recyclerview.widget.GridLayoutManager.b
        public int e(int i, int i2) {
            return i % i2;
        }

        @Override // androidx.recyclerview.widget.GridLayoutManager.b
        public int f(int i) {
            return 1;
        }
    }

    /* loaded from: classes.dex */
    public static abstract class b {
        public final SparseIntArray a = new SparseIntArray();
        public final SparseIntArray b = new SparseIntArray();
        public boolean c = false;
        public boolean d = false;

        public static int a(SparseIntArray sparseIntArray, int i) {
            int size = sparseIntArray.size() - 1;
            int i2 = 0;
            while (i2 <= size) {
                int i3 = (i2 + size) >>> 1;
                if (sparseIntArray.keyAt(i3) < i) {
                    i2 = i3 + 1;
                } else {
                    size = i3 - 1;
                }
            }
            int i4 = i2 - 1;
            if (i4 < 0 || i4 >= sparseIntArray.size()) {
                return -1;
            }
            return sparseIntArray.keyAt(i4);
        }

        public int b(int i, int i2) {
            if (!this.d) {
                return d(i, i2);
            }
            int i3 = this.b.get(i, -1);
            if (i3 != -1) {
                return i3;
            }
            int d = d(i, i2);
            this.b.put(i, d);
            return d;
        }

        public int c(int i, int i2) {
            if (!this.c) {
                return e(i, i2);
            }
            int i3 = this.a.get(i, -1);
            if (i3 != -1) {
                return i3;
            }
            int e = e(i, i2);
            this.a.put(i, e);
            return e;
        }

        public int d(int i, int i2) {
            int i3;
            int i4;
            int i5;
            int a;
            if (!this.d || (a = a(this.b, i)) == -1) {
                i3 = 0;
                i4 = 0;
                i5 = 0;
            } else {
                i3 = this.b.get(a);
                i4 = a + 1;
                i5 = c(a, i2) + f(a);
                if (i5 == i2) {
                    i3++;
                    i5 = 0;
                }
            }
            int f = f(i);
            while (i4 < i) {
                int f2 = f(i4);
                i5 += f2;
                if (i5 == i2) {
                    i3++;
                    i5 = 0;
                } else if (i5 > i2) {
                    i3++;
                    i5 = f2;
                }
                i4++;
            }
            return i5 + f > i2 ? i3 + 1 : i3;
        }

        public abstract int e(int i, int i2);

        public abstract int f(int i);

        public void g() {
            this.b.clear();
        }

        public void h() {
            this.a.clear();
        }
    }

    public GridLayoutManager(Context context, AttributeSet attributeSet, int i, int i2) {
        super(context, attributeSet, i, i2);
        this.M0 = false;
        this.N0 = -1;
        this.Q0 = new SparseIntArray();
        this.R0 = new SparseIntArray();
        this.S0 = new a();
        this.T0 = new Rect();
        l3(RecyclerView.LayoutManager.p0(context, attributeSet, i, i2).b);
    }

    public static int[] X2(int[] iArr, int i, int i2) {
        int i3;
        if (iArr == null || iArr.length != i + 1 || iArr[iArr.length - 1] != i2) {
            iArr = new int[i + 1];
        }
        int i4 = 0;
        iArr[0] = 0;
        int i5 = i2 / i;
        int i6 = i2 % i;
        int i7 = 0;
        for (int i8 = 1; i8 <= i; i8++) {
            i4 += i6;
            if (i4 <= 0 || i - i4 >= i6) {
                i3 = i5;
            } else {
                i3 = i5 + 1;
                i4 -= i;
            }
            i7 += i3;
            iArr[i8] = i7;
        }
        return iArr;
    }

    @Override // androidx.recyclerview.widget.LinearLayoutManager
    public void A2(RecyclerView.t tVar, RecyclerView.x xVar, LinearLayoutManager.a aVar, int i) {
        super.A2(tVar, xVar, aVar, i);
        m3();
        if (xVar.b() > 0 && !xVar.e()) {
            b3(tVar, xVar, aVar, i);
        }
        c3();
    }

    @Override // androidx.recyclerview.widget.LinearLayoutManager, androidx.recyclerview.widget.RecyclerView.LayoutManager
    public int C(RecyclerView.x xVar) {
        if (this.U0) {
            return Z2(xVar);
        }
        return super.C(xVar);
    }

    @Override // androidx.recyclerview.widget.LinearLayoutManager, androidx.recyclerview.widget.RecyclerView.LayoutManager
    public int D(RecyclerView.x xVar) {
        if (this.U0) {
            return a3(xVar);
        }
        return super.D(xVar);
    }

    @Override // androidx.recyclerview.widget.LinearLayoutManager, androidx.recyclerview.widget.RecyclerView.LayoutManager
    public int F(RecyclerView.x xVar) {
        if (this.U0) {
            return Z2(xVar);
        }
        return super.F(xVar);
    }

    @Override // androidx.recyclerview.widget.LinearLayoutManager, androidx.recyclerview.widget.RecyclerView.LayoutManager
    public int F1(int i, RecyclerView.t tVar, RecyclerView.x xVar) {
        m3();
        c3();
        return super.F1(i, tVar, xVar);
    }

    @Override // androidx.recyclerview.widget.LinearLayoutManager, androidx.recyclerview.widget.RecyclerView.LayoutManager
    public int G(RecyclerView.x xVar) {
        if (this.U0) {
            return a3(xVar);
        }
        return super.G(xVar);
    }

    @Override // androidx.recyclerview.widget.LinearLayoutManager, androidx.recyclerview.widget.RecyclerView.LayoutManager
    public int H1(int i, RecyclerView.t tVar, RecyclerView.x xVar) {
        m3();
        c3();
        return super.H1(i, tVar, xVar);
    }

    @Override // androidx.recyclerview.widget.RecyclerView.LayoutManager
    public void L1(Rect rect, int i, int i2) {
        int y;
        int y2;
        if (this.O0 == null) {
            super.L1(rect, i, i2);
        }
        int paddingLeft = getPaddingLeft() + getPaddingRight();
        int paddingTop = getPaddingTop() + getPaddingBottom();
        if (this.w0 == 1) {
            y2 = RecyclerView.LayoutManager.y(i2, rect.height() + paddingTop, m0());
            int[] iArr = this.O0;
            y = RecyclerView.LayoutManager.y(i, iArr[iArr.length - 1] + paddingLeft, n0());
        } else {
            y = RecyclerView.LayoutManager.y(i, rect.width() + paddingLeft, n0());
            int[] iArr2 = this.O0;
            y2 = RecyclerView.LayoutManager.y(i2, iArr2[iArr2.length - 1] + paddingTop, m0());
        }
        K1(y, y2);
    }

    @Override // androidx.recyclerview.widget.LinearLayoutManager
    public void L2(boolean z) {
        if (!z) {
            super.L2(false);
            return;
        }
        throw new UnsupportedOperationException("GridLayoutManager does not support stack from end. Consider using reverse layout");
    }

    @Override // androidx.recyclerview.widget.LinearLayoutManager, androidx.recyclerview.widget.RecyclerView.LayoutManager
    public RecyclerView.LayoutParams O() {
        if (this.w0 == 0) {
            return new LayoutParams(-2, -1);
        }
        return new LayoutParams(-1, -2);
    }

    @Override // androidx.recyclerview.widget.RecyclerView.LayoutManager
    public RecyclerView.LayoutParams P(Context context, AttributeSet attributeSet) {
        return new LayoutParams(context, attributeSet);
    }

    @Override // androidx.recyclerview.widget.RecyclerView.LayoutManager
    public RecyclerView.LayoutParams Q(ViewGroup.LayoutParams layoutParams) {
        if (layoutParams instanceof ViewGroup.MarginLayoutParams) {
            return new LayoutParams((ViewGroup.MarginLayoutParams) layoutParams);
        }
        return new LayoutParams(layoutParams);
    }

    /* JADX WARN: Code restructure failed: missing block: B:59:0x00d6, code lost:
        if (r13 == (r2 > r15)) goto L50;
     */
    /* JADX WARN: Code restructure failed: missing block: B:72:0x00f6, code lost:
        if (r13 == (r2 > r7)) goto L51;
     */
    /* JADX WARN: Removed duplicated region for block: B:79:0x0107  */
    @Override // androidx.recyclerview.widget.LinearLayoutManager, androidx.recyclerview.widget.RecyclerView.LayoutManager
    /*
        Code decompiled incorrectly, please refer to instructions dump.
        To view partially-correct code enable 'Show inconsistent code' option in preferences
    */
    public android.view.View R0(android.view.View r24, int r25, androidx.recyclerview.widget.RecyclerView.t r26, androidx.recyclerview.widget.RecyclerView.x r27) {
        /*
            Method dump skipped, instructions count: 337
            To view this dump change 'Code comments level' option to 'DEBUG'
        */
        throw new UnsupportedOperationException("Method not decompiled: androidx.recyclerview.widget.GridLayoutManager.R0(android.view.View, int, androidx.recyclerview.widget.RecyclerView$t, androidx.recyclerview.widget.RecyclerView$x):android.view.View");
    }

    @Override // androidx.recyclerview.widget.LinearLayoutManager, androidx.recyclerview.widget.RecyclerView.LayoutManager
    public boolean U1() {
        return this.H0 == null && !this.M0;
    }

    public final void U2(RecyclerView.t tVar, RecyclerView.x xVar, int i, boolean z) {
        int i2;
        int i3;
        int i4 = 0;
        int i5 = -1;
        if (z) {
            i3 = 1;
            i5 = i;
            i2 = 0;
        } else {
            i2 = i - 1;
            i3 = -1;
        }
        while (i2 != i5) {
            View view = this.P0[i2];
            LayoutParams layoutParams = (LayoutParams) view.getLayoutParams();
            int h3 = h3(tVar, xVar, o0(view));
            layoutParams.j0 = h3;
            layoutParams.i0 = i4;
            i4 += h3;
            i2 += i3;
        }
    }

    public final void V2() {
        int U = U();
        for (int i = 0; i < U; i++) {
            LayoutParams layoutParams = (LayoutParams) T(i).getLayoutParams();
            int a2 = layoutParams.a();
            this.Q0.put(a2, layoutParams.f());
            this.R0.put(a2, layoutParams.e());
        }
    }

    @Override // androidx.recyclerview.widget.LinearLayoutManager
    public void W1(RecyclerView.x xVar, LinearLayoutManager.c cVar, RecyclerView.LayoutManager.c cVar2) {
        int i = this.N0;
        for (int i2 = 0; i2 < this.N0 && cVar.c(xVar) && i > 0; i2++) {
            int i3 = cVar.d;
            cVar2.a(i3, Math.max(0, cVar.g));
            i -= this.S0.f(i3);
            cVar.d += cVar.e;
        }
    }

    public final void W2(int i) {
        this.O0 = X2(this.O0, this.N0, i);
    }

    @Override // androidx.recyclerview.widget.RecyclerView.LayoutManager
    public void X0(RecyclerView.t tVar, RecyclerView.x xVar, View view, b6 b6Var) {
        ViewGroup.LayoutParams layoutParams = view.getLayoutParams();
        if (!(layoutParams instanceof LayoutParams)) {
            super.W0(view, b6Var);
            return;
        }
        LayoutParams layoutParams2 = (LayoutParams) layoutParams;
        int f3 = f3(tVar, xVar, layoutParams2.a());
        if (this.w0 == 0) {
            b6Var.f0(b6.c.a(layoutParams2.e(), layoutParams2.f(), f3, 1, false, false));
        } else {
            b6Var.f0(b6.c.a(f3, 1, layoutParams2.e(), layoutParams2.f(), false, false));
        }
    }

    @Override // androidx.recyclerview.widget.RecyclerView.LayoutManager
    public int Y(RecyclerView.t tVar, RecyclerView.x xVar) {
        if (this.w0 == 1) {
            return this.N0;
        }
        if (xVar.b() < 1) {
            return 0;
        }
        return f3(tVar, xVar, xVar.b() - 1) + 1;
    }

    public final void Y2() {
        this.Q0.clear();
        this.R0.clear();
    }

    @Override // androidx.recyclerview.widget.RecyclerView.LayoutManager
    public void Z0(RecyclerView recyclerView, int i, int i2) {
        this.S0.h();
        this.S0.g();
    }

    public final int Z2(RecyclerView.x xVar) {
        int max;
        if (U() != 0 && xVar.b() != 0) {
            c2();
            boolean x2 = x2();
            View g2 = g2(!x2, true);
            View f2 = f2(!x2, true);
            if (g2 != null && f2 != null) {
                int b2 = this.S0.b(o0(g2), this.N0);
                int b3 = this.S0.b(o0(f2), this.N0);
                int min = Math.min(b2, b3);
                int max2 = Math.max(b2, b3);
                int b4 = this.S0.b(xVar.b() - 1, this.N0) + 1;
                if (this.B0) {
                    max = Math.max(0, (b4 - max2) - 1);
                } else {
                    max = Math.max(0, min);
                }
                if (x2) {
                    return Math.round((max * (Math.abs(this.y0.d(f2) - this.y0.g(g2)) / ((this.S0.b(o0(f2), this.N0) - this.S0.b(o0(g2), this.N0)) + 1))) + (this.y0.m() - this.y0.g(g2)));
                }
                return max;
            }
        }
        return 0;
    }

    @Override // androidx.recyclerview.widget.RecyclerView.LayoutManager
    public void a1(RecyclerView recyclerView) {
        this.S0.h();
        this.S0.g();
    }

    public final int a3(RecyclerView.x xVar) {
        if (U() != 0 && xVar.b() != 0) {
            c2();
            View g2 = g2(!x2(), true);
            View f2 = f2(!x2(), true);
            if (g2 != null && f2 != null) {
                if (!x2()) {
                    return this.S0.b(xVar.b() - 1, this.N0) + 1;
                }
                int d = this.y0.d(f2) - this.y0.g(g2);
                int b2 = this.S0.b(o0(g2), this.N0);
                return (int) ((d / ((this.S0.b(o0(f2), this.N0) - b2) + 1)) * (this.S0.b(xVar.b() - 1, this.N0) + 1));
            }
        }
        return 0;
    }

    @Override // androidx.recyclerview.widget.RecyclerView.LayoutManager
    public void b1(RecyclerView recyclerView, int i, int i2, int i3) {
        this.S0.h();
        this.S0.g();
    }

    public final void b3(RecyclerView.t tVar, RecyclerView.x xVar, LinearLayoutManager.a aVar, int i) {
        boolean z = i == 1;
        int g3 = g3(tVar, xVar, aVar.b);
        if (z) {
            while (g3 > 0) {
                int i2 = aVar.b;
                if (i2 <= 0) {
                    return;
                }
                int i3 = i2 - 1;
                aVar.b = i3;
                g3 = g3(tVar, xVar, i3);
            }
            return;
        }
        int b2 = xVar.b() - 1;
        int i4 = aVar.b;
        while (i4 < b2) {
            int i5 = i4 + 1;
            int g32 = g3(tVar, xVar, i5);
            if (g32 <= g3) {
                break;
            }
            i4 = i5;
            g3 = g32;
        }
        aVar.b = i4;
    }

    @Override // androidx.recyclerview.widget.RecyclerView.LayoutManager
    public void c1(RecyclerView recyclerView, int i, int i2) {
        this.S0.h();
        this.S0.g();
    }

    public final void c3() {
        View[] viewArr = this.P0;
        if (viewArr == null || viewArr.length != this.N0) {
            this.P0 = new View[this.N0];
        }
    }

    public int d3(int i, int i2) {
        if (this.w0 == 1 && w2()) {
            int[] iArr = this.O0;
            int i3 = this.N0;
            return iArr[i3 - i] - iArr[(i3 - i) - i2];
        }
        int[] iArr2 = this.O0;
        return iArr2[i2 + i] - iArr2[i];
    }

    @Override // androidx.recyclerview.widget.RecyclerView.LayoutManager
    public void e1(RecyclerView recyclerView, int i, int i2, Object obj) {
        this.S0.h();
        this.S0.g();
    }

    public int e3() {
        return this.N0;
    }

    @Override // androidx.recyclerview.widget.LinearLayoutManager, androidx.recyclerview.widget.RecyclerView.LayoutManager
    public void f1(RecyclerView.t tVar, RecyclerView.x xVar) {
        if (xVar.e()) {
            V2();
        }
        super.f1(tVar, xVar);
        Y2();
    }

    public final int f3(RecyclerView.t tVar, RecyclerView.x xVar, int i) {
        if (!xVar.e()) {
            return this.S0.b(i, this.N0);
        }
        int f = tVar.f(i);
        if (f == -1) {
            StringBuilder sb = new StringBuilder();
            sb.append("Cannot find span size for pre layout position. ");
            sb.append(i);
            return 0;
        }
        return this.S0.b(f, this.N0);
    }

    @Override // androidx.recyclerview.widget.LinearLayoutManager, androidx.recyclerview.widget.RecyclerView.LayoutManager
    public void g1(RecyclerView.x xVar) {
        super.g1(xVar);
        this.M0 = false;
    }

    public final int g3(RecyclerView.t tVar, RecyclerView.x xVar, int i) {
        if (!xVar.e()) {
            return this.S0.c(i, this.N0);
        }
        int i2 = this.R0.get(i, -1);
        if (i2 != -1) {
            return i2;
        }
        int f = tVar.f(i);
        if (f == -1) {
            StringBuilder sb = new StringBuilder();
            sb.append("Cannot find span size for pre layout position. It is not cached, not in the adapter. Pos:");
            sb.append(i);
            return 0;
        }
        return this.S0.c(f, this.N0);
    }

    public final int h3(RecyclerView.t tVar, RecyclerView.x xVar, int i) {
        if (!xVar.e()) {
            return this.S0.f(i);
        }
        int i2 = this.Q0.get(i, -1);
        if (i2 != -1) {
            return i2;
        }
        int f = tVar.f(i);
        if (f == -1) {
            StringBuilder sb = new StringBuilder();
            sb.append("Cannot find span size for pre layout position. It is not cached, not in the adapter. Pos:");
            sb.append(i);
            return 1;
        }
        return this.S0.f(f);
    }

    public final void i3(float f, int i) {
        W2(Math.max(Math.round(f * this.N0), i));
    }

    public final void j3(View view, int i, boolean z) {
        int i2;
        int i3;
        LayoutParams layoutParams = (LayoutParams) view.getLayoutParams();
        Rect rect = layoutParams.f0;
        int i4 = rect.top + rect.bottom + ((ViewGroup.MarginLayoutParams) layoutParams).topMargin + ((ViewGroup.MarginLayoutParams) layoutParams).bottomMargin;
        int i5 = rect.left + rect.right + ((ViewGroup.MarginLayoutParams) layoutParams).leftMargin + ((ViewGroup.MarginLayoutParams) layoutParams).rightMargin;
        int d3 = d3(layoutParams.i0, layoutParams.j0);
        if (this.w0 == 1) {
            i3 = RecyclerView.LayoutManager.V(d3, i, i5, ((ViewGroup.MarginLayoutParams) layoutParams).width, false);
            i2 = RecyclerView.LayoutManager.V(this.y0.n(), i0(), i4, ((ViewGroup.MarginLayoutParams) layoutParams).height, true);
        } else {
            int V = RecyclerView.LayoutManager.V(d3, i, i4, ((ViewGroup.MarginLayoutParams) layoutParams).height, false);
            int V2 = RecyclerView.LayoutManager.V(this.y0.n(), w0(), i5, ((ViewGroup.MarginLayoutParams) layoutParams).width, true);
            i2 = V;
            i3 = V2;
        }
        k3(view, i3, i2, z);
    }

    public final void k3(View view, int i, int i2, boolean z) {
        boolean O1;
        RecyclerView.LayoutParams layoutParams = (RecyclerView.LayoutParams) view.getLayoutParams();
        if (z) {
            O1 = Q1(view, i, i2, layoutParams);
        } else {
            O1 = O1(view, i, i2, layoutParams);
        }
        if (O1) {
            view.measure(i, i2);
        }
    }

    public void l3(int i) {
        if (i == this.N0) {
            return;
        }
        this.M0 = true;
        if (i >= 1) {
            this.N0 = i;
            this.S0.h();
            C1();
            return;
        }
        throw new IllegalArgumentException("Span count should be at least 1. Provided " + i);
    }

    public final void m3() {
        int h0;
        int paddingTop;
        if (v2() == 1) {
            h0 = v0() - getPaddingRight();
            paddingTop = getPaddingLeft();
        } else {
            h0 = h0() - getPaddingBottom();
            paddingTop = getPaddingTop();
        }
        W2(h0 - paddingTop);
    }

    @Override // androidx.recyclerview.widget.LinearLayoutManager
    public View p2(RecyclerView.t tVar, RecyclerView.x xVar, boolean z, boolean z2) {
        int i;
        int U = U();
        int i2 = -1;
        int i3 = 1;
        if (z2) {
            i = U() - 1;
            i3 = -1;
        } else {
            i2 = U;
            i = 0;
        }
        int b2 = xVar.b();
        c2();
        int m = this.y0.m();
        int i4 = this.y0.i();
        View view = null;
        View view2 = null;
        while (i != i2) {
            View T = T(i);
            int o0 = o0(T);
            if (o0 >= 0 && o0 < b2 && g3(tVar, xVar, o0) == 0) {
                if (((RecyclerView.LayoutParams) T.getLayoutParams()).c()) {
                    if (view2 == null) {
                        view2 = T;
                    }
                } else if (this.y0.g(T) < i4 && this.y0.d(T) >= m) {
                    return T;
                } else {
                    if (view == null) {
                        view = T;
                    }
                }
            }
            i += i3;
        }
        return view != null ? view : view2;
    }

    @Override // androidx.recyclerview.widget.RecyclerView.LayoutManager
    public int r0(RecyclerView.t tVar, RecyclerView.x xVar) {
        if (this.w0 == 0) {
            return this.N0;
        }
        if (xVar.b() < 1) {
            return 0;
        }
        return f3(tVar, xVar, xVar.b() - 1) + 1;
    }

    @Override // androidx.recyclerview.widget.RecyclerView.LayoutManager
    public boolean x(RecyclerView.LayoutParams layoutParams) {
        return layoutParams instanceof LayoutParams;
    }

    /* JADX WARN: Code restructure failed: missing block: B:37:0x009f, code lost:
        r21.b = true;
     */
    /* JADX WARN: Code restructure failed: missing block: B:38:0x00a1, code lost:
        return;
     */
    /* JADX WARN: Multi-variable type inference failed */
    /* JADX WARN: Type inference failed for: r5v0 */
    /* JADX WARN: Type inference failed for: r5v1, types: [int, boolean] */
    /* JADX WARN: Type inference failed for: r5v19 */
    @Override // androidx.recyclerview.widget.LinearLayoutManager
    /*
        Code decompiled incorrectly, please refer to instructions dump.
        To view partially-correct code enable 'Show inconsistent code' option in preferences
    */
    public void y2(androidx.recyclerview.widget.RecyclerView.t r18, androidx.recyclerview.widget.RecyclerView.x r19, androidx.recyclerview.widget.LinearLayoutManager.c r20, androidx.recyclerview.widget.LinearLayoutManager.b r21) {
        /*
            Method dump skipped, instructions count: 563
            To view this dump change 'Code comments level' option to 'DEBUG'
        */
        throw new UnsupportedOperationException("Method not decompiled: androidx.recyclerview.widget.GridLayoutManager.y2(androidx.recyclerview.widget.RecyclerView$t, androidx.recyclerview.widget.RecyclerView$x, androidx.recyclerview.widget.LinearLayoutManager$c, androidx.recyclerview.widget.LinearLayoutManager$b):void");
    }

    /* loaded from: classes.dex */
    public static class LayoutParams extends RecyclerView.LayoutParams {
        public int i0;
        public int j0;

        public LayoutParams(Context context, AttributeSet attributeSet) {
            super(context, attributeSet);
            this.i0 = -1;
            this.j0 = 0;
        }

        public int e() {
            return this.i0;
        }

        public int f() {
            return this.j0;
        }

        public LayoutParams(int i, int i2) {
            super(i, i2);
            this.i0 = -1;
            this.j0 = 0;
        }

        public LayoutParams(ViewGroup.MarginLayoutParams marginLayoutParams) {
            super(marginLayoutParams);
            this.i0 = -1;
            this.j0 = 0;
        }

        public LayoutParams(ViewGroup.LayoutParams layoutParams) {
            super(layoutParams);
            this.i0 = -1;
            this.j0 = 0;
        }
    }

    public GridLayoutManager(Context context, int i) {
        super(context);
        this.M0 = false;
        this.N0 = -1;
        this.Q0 = new SparseIntArray();
        this.R0 = new SparseIntArray();
        this.S0 = new a();
        this.T0 = new Rect();
        l3(i);
    }

    public GridLayoutManager(Context context, int i, int i2, boolean z) {
        super(context, i2, z);
        this.M0 = false;
        this.N0 = -1;
        this.Q0 = new SparseIntArray();
        this.R0 = new SparseIntArray();
        this.S0 = new a();
        this.T0 = new Rect();
        l3(i);
    }
}
