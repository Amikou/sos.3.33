package androidx.recyclerview.widget;

import android.annotation.SuppressLint;
import android.content.Context;
import android.graphics.PointF;
import android.os.Parcel;
import android.os.Parcelable;
import android.util.AttributeSet;
import android.view.View;
import android.view.accessibility.AccessibilityEvent;
import androidx.recyclerview.widget.RecyclerView;
import androidx.recyclerview.widget.k;
import com.github.mikephil.charting.utils.Utils;
import java.util.List;

/* loaded from: classes.dex */
public class LinearLayoutManager extends RecyclerView.LayoutManager implements k.j, RecyclerView.w.b {
    public boolean A0;
    public boolean B0;
    public boolean C0;
    public boolean D0;
    public int E0;
    public int F0;
    public boolean G0;
    public SavedState H0;
    public final a I0;
    public final b J0;
    public int K0;
    public int[] L0;
    public int w0;
    public c x0;
    public q y0;
    public boolean z0;

    @SuppressLint({"BanParcelableUsage"})
    /* loaded from: classes.dex */
    public static class SavedState implements Parcelable {
        public static final Parcelable.Creator<SavedState> CREATOR = new a();
        public int a;
        public int f0;
        public boolean g0;

        /* loaded from: classes.dex */
        public class a implements Parcelable.Creator<SavedState> {
            @Override // android.os.Parcelable.Creator
            /* renamed from: a */
            public SavedState createFromParcel(Parcel parcel) {
                return new SavedState(parcel);
            }

            @Override // android.os.Parcelable.Creator
            /* renamed from: b */
            public SavedState[] newArray(int i) {
                return new SavedState[i];
            }
        }

        public SavedState() {
        }

        public boolean a() {
            return this.a >= 0;
        }

        public void b() {
            this.a = -1;
        }

        @Override // android.os.Parcelable
        public int describeContents() {
            return 0;
        }

        @Override // android.os.Parcelable
        public void writeToParcel(Parcel parcel, int i) {
            parcel.writeInt(this.a);
            parcel.writeInt(this.f0);
            parcel.writeInt(this.g0 ? 1 : 0);
        }

        public SavedState(Parcel parcel) {
            this.a = parcel.readInt();
            this.f0 = parcel.readInt();
            this.g0 = parcel.readInt() == 1;
        }

        public SavedState(SavedState savedState) {
            this.a = savedState.a;
            this.f0 = savedState.f0;
            this.g0 = savedState.g0;
        }
    }

    /* loaded from: classes.dex */
    public static class a {
        public q a;
        public int b;
        public int c;
        public boolean d;
        public boolean e;

        public a() {
            e();
        }

        public void a() {
            int m;
            if (this.d) {
                m = this.a.i();
            } else {
                m = this.a.m();
            }
            this.c = m;
        }

        public void b(View view, int i) {
            if (this.d) {
                this.c = this.a.d(view) + this.a.o();
            } else {
                this.c = this.a.g(view);
            }
            this.b = i;
        }

        public void c(View view, int i) {
            int o = this.a.o();
            if (o >= 0) {
                b(view, i);
                return;
            }
            this.b = i;
            if (this.d) {
                int i2 = (this.a.i() - o) - this.a.d(view);
                this.c = this.a.i() - i2;
                if (i2 > 0) {
                    int e = this.c - this.a.e(view);
                    int m = this.a.m();
                    int min = e - (m + Math.min(this.a.g(view) - m, 0));
                    if (min < 0) {
                        this.c += Math.min(i2, -min);
                        return;
                    }
                    return;
                }
                return;
            }
            int g = this.a.g(view);
            int m2 = g - this.a.m();
            this.c = g;
            if (m2 > 0) {
                int i3 = (this.a.i() - Math.min(0, (this.a.i() - o) - this.a.d(view))) - (g + this.a.e(view));
                if (i3 < 0) {
                    this.c -= Math.min(m2, -i3);
                }
            }
        }

        public boolean d(View view, RecyclerView.x xVar) {
            RecyclerView.LayoutParams layoutParams = (RecyclerView.LayoutParams) view.getLayoutParams();
            return !layoutParams.c() && layoutParams.a() >= 0 && layoutParams.a() < xVar.b();
        }

        public void e() {
            this.b = -1;
            this.c = Integer.MIN_VALUE;
            this.d = false;
            this.e = false;
        }

        public String toString() {
            return "AnchorInfo{mPosition=" + this.b + ", mCoordinate=" + this.c + ", mLayoutFromEnd=" + this.d + ", mValid=" + this.e + '}';
        }
    }

    /* loaded from: classes.dex */
    public static class b {
        public int a;
        public boolean b;
        public boolean c;
        public boolean d;

        public void a() {
            this.a = 0;
            this.b = false;
            this.c = false;
            this.d = false;
        }
    }

    /* loaded from: classes.dex */
    public static class c {
        public int b;
        public int c;
        public int d;
        public int e;
        public int f;
        public int g;
        public boolean j;
        public int k;
        public boolean m;
        public boolean a = true;
        public int h = 0;
        public int i = 0;
        public List<RecyclerView.a0> l = null;

        public void a() {
            b(null);
        }

        public void b(View view) {
            View f = f(view);
            if (f == null) {
                this.d = -1;
            } else {
                this.d = ((RecyclerView.LayoutParams) f.getLayoutParams()).a();
            }
        }

        public boolean c(RecyclerView.x xVar) {
            int i = this.d;
            return i >= 0 && i < xVar.b();
        }

        public View d(RecyclerView.t tVar) {
            if (this.l != null) {
                return e();
            }
            View o = tVar.o(this.d);
            this.d += this.e;
            return o;
        }

        public final View e() {
            int size = this.l.size();
            for (int i = 0; i < size; i++) {
                View view = this.l.get(i).itemView;
                RecyclerView.LayoutParams layoutParams = (RecyclerView.LayoutParams) view.getLayoutParams();
                if (!layoutParams.c() && this.d == layoutParams.a()) {
                    b(view);
                    return view;
                }
            }
            return null;
        }

        public View f(View view) {
            int a;
            int size = this.l.size();
            View view2 = null;
            int i = Integer.MAX_VALUE;
            for (int i2 = 0; i2 < size; i2++) {
                View view3 = this.l.get(i2).itemView;
                RecyclerView.LayoutParams layoutParams = (RecyclerView.LayoutParams) view3.getLayoutParams();
                if (view3 != view && !layoutParams.c() && (a = (layoutParams.a() - this.d) * this.e) >= 0 && a < i) {
                    view2 = view3;
                    if (a == 0) {
                        break;
                    }
                    i = a;
                }
            }
            return view2;
        }
    }

    public LinearLayoutManager(Context context) {
        this(context, 1, false);
    }

    @Override // androidx.recyclerview.widget.RecyclerView.LayoutManager
    public void A(int i, RecyclerView.LayoutManager.c cVar) {
        boolean z;
        int i2;
        SavedState savedState = this.H0;
        if (savedState != null && savedState.a()) {
            SavedState savedState2 = this.H0;
            z = savedState2.g0;
            i2 = savedState2.a;
        } else {
            G2();
            z = this.B0;
            i2 = this.E0;
            if (i2 == -1) {
                i2 = z ? i - 1 : 0;
            }
        }
        int i3 = z ? -1 : 1;
        for (int i4 = 0; i4 < this.K0 && i2 >= 0 && i2 < i; i4++) {
            cVar.a(i2, 0);
            i2 += i3;
        }
    }

    public void A2(RecyclerView.t tVar, RecyclerView.x xVar, a aVar, int i) {
    }

    @Override // androidx.recyclerview.widget.RecyclerView.LayoutManager
    public int B(RecyclerView.x xVar) {
        return X1(xVar);
    }

    public final void B2(RecyclerView.t tVar, c cVar) {
        if (!cVar.a || cVar.m) {
            return;
        }
        int i = cVar.g;
        int i2 = cVar.i;
        if (cVar.f == -1) {
            D2(tVar, i, i2);
        } else {
            E2(tVar, i, i2);
        }
    }

    @Override // androidx.recyclerview.widget.RecyclerView.LayoutManager
    public int C(RecyclerView.x xVar) {
        return Y1(xVar);
    }

    public final void C2(RecyclerView.t tVar, int i, int i2) {
        if (i == i2) {
            return;
        }
        if (i2 <= i) {
            while (i > i2) {
                w1(i, tVar);
                i--;
            }
            return;
        }
        for (int i3 = i2 - 1; i3 >= i; i3--) {
            w1(i3, tVar);
        }
    }

    @Override // androidx.recyclerview.widget.RecyclerView.LayoutManager
    public int D(RecyclerView.x xVar) {
        return Z1(xVar);
    }

    public final void D2(RecyclerView.t tVar, int i, int i2) {
        int U = U();
        if (i < 0) {
            return;
        }
        int h = (this.y0.h() - i) + i2;
        if (this.B0) {
            for (int i3 = 0; i3 < U; i3++) {
                View T = T(i3);
                if (this.y0.g(T) < h || this.y0.q(T) < h) {
                    C2(tVar, 0, i3);
                    return;
                }
            }
            return;
        }
        int i4 = U - 1;
        for (int i5 = i4; i5 >= 0; i5--) {
            View T2 = T(i5);
            if (this.y0.g(T2) < h || this.y0.q(T2) < h) {
                C2(tVar, i4, i5);
                return;
            }
        }
    }

    @Override // androidx.recyclerview.widget.RecyclerView.LayoutManager
    public int E(RecyclerView.x xVar) {
        return X1(xVar);
    }

    public final void E2(RecyclerView.t tVar, int i, int i2) {
        if (i < 0) {
            return;
        }
        int i3 = i - i2;
        int U = U();
        if (!this.B0) {
            for (int i4 = 0; i4 < U; i4++) {
                View T = T(i4);
                if (this.y0.d(T) > i3 || this.y0.p(T) > i3) {
                    C2(tVar, 0, i4);
                    return;
                }
            }
            return;
        }
        int i5 = U - 1;
        for (int i6 = i5; i6 >= 0; i6--) {
            View T2 = T(i6);
            if (this.y0.d(T2) > i3 || this.y0.p(T2) > i3) {
                C2(tVar, i5, i6);
                return;
            }
        }
    }

    @Override // androidx.recyclerview.widget.RecyclerView.LayoutManager
    public int F(RecyclerView.x xVar) {
        return Y1(xVar);
    }

    @Override // androidx.recyclerview.widget.RecyclerView.LayoutManager
    public int F1(int i, RecyclerView.t tVar, RecyclerView.x xVar) {
        if (this.w0 == 1) {
            return 0;
        }
        return H2(i, tVar, xVar);
    }

    public boolean F2() {
        return this.y0.k() == 0 && this.y0.h() == 0;
    }

    @Override // androidx.recyclerview.widget.RecyclerView.LayoutManager
    public int G(RecyclerView.x xVar) {
        return Z1(xVar);
    }

    @Override // androidx.recyclerview.widget.RecyclerView.LayoutManager
    public void G1(int i) {
        this.E0 = i;
        this.F0 = Integer.MIN_VALUE;
        SavedState savedState = this.H0;
        if (savedState != null) {
            savedState.b();
        }
        C1();
    }

    public final void G2() {
        if (this.w0 != 1 && w2()) {
            this.B0 = !this.A0;
        } else {
            this.B0 = this.A0;
        }
    }

    @Override // androidx.recyclerview.widget.RecyclerView.LayoutManager
    public int H1(int i, RecyclerView.t tVar, RecyclerView.x xVar) {
        if (this.w0 == 0) {
            return 0;
        }
        return H2(i, tVar, xVar);
    }

    public int H2(int i, RecyclerView.t tVar, RecyclerView.x xVar) {
        if (U() == 0 || i == 0) {
            return 0;
        }
        c2();
        this.x0.a = true;
        int i2 = i > 0 ? 1 : -1;
        int abs = Math.abs(i);
        P2(i2, abs, true, xVar);
        c cVar = this.x0;
        int d2 = cVar.g + d2(tVar, cVar, xVar, false);
        if (d2 < 0) {
            return 0;
        }
        if (abs > d2) {
            i = i2 * d2;
        }
        this.y0.r(-i);
        this.x0.k = i;
        return i;
    }

    public void I2(int i, int i2) {
        this.E0 = i;
        this.F0 = i2;
        SavedState savedState = this.H0;
        if (savedState != null) {
            savedState.b();
        }
        C1();
    }

    public void J2(int i) {
        if (i != 0 && i != 1) {
            throw new IllegalArgumentException("invalid orientation:" + i);
        }
        r(null);
        if (i != this.w0 || this.y0 == null) {
            q b2 = q.b(this, i);
            this.y0 = b2;
            this.I0.a = b2;
            this.w0 = i;
            C1();
        }
    }

    public void K2(boolean z) {
        r(null);
        if (z == this.A0) {
            return;
        }
        this.A0 = z;
        C1();
    }

    public void L2(boolean z) {
        r(null);
        if (this.C0 == z) {
            return;
        }
        this.C0 = z;
        C1();
    }

    public final boolean M2(RecyclerView.t tVar, RecyclerView.x xVar, a aVar) {
        View p2;
        boolean z = false;
        if (U() == 0) {
            return false;
        }
        View g0 = g0();
        if (g0 != null && aVar.d(g0, xVar)) {
            aVar.c(g0, o0(g0));
            return true;
        }
        boolean z2 = this.z0;
        boolean z3 = this.C0;
        if (z2 == z3 && (p2 = p2(tVar, xVar, aVar.d, z3)) != null) {
            aVar.b(p2, o0(p2));
            if (!xVar.e() && U1()) {
                int g = this.y0.g(p2);
                int d = this.y0.d(p2);
                int m = this.y0.m();
                int i = this.y0.i();
                boolean z4 = d <= m && g < m;
                if (g >= i && d > i) {
                    z = true;
                }
                if (z4 || z) {
                    if (aVar.d) {
                        m = i;
                    }
                    aVar.c = m;
                }
            }
            return true;
        }
        return false;
    }

    @Override // androidx.recyclerview.widget.RecyclerView.LayoutManager
    public View N(int i) {
        int U = U();
        if (U == 0) {
            return null;
        }
        int o0 = i - o0(T(0));
        if (o0 >= 0 && o0 < U) {
            View T = T(o0);
            if (o0(T) == i) {
                return T;
            }
        }
        return super.N(i);
    }

    public final boolean N2(RecyclerView.x xVar, a aVar) {
        int i;
        int g;
        if (!xVar.e() && (i = this.E0) != -1) {
            if (i >= 0 && i < xVar.b()) {
                aVar.b = this.E0;
                SavedState savedState = this.H0;
                if (savedState != null && savedState.a()) {
                    boolean z = this.H0.g0;
                    aVar.d = z;
                    if (z) {
                        aVar.c = this.y0.i() - this.H0.f0;
                    } else {
                        aVar.c = this.y0.m() + this.H0.f0;
                    }
                    return true;
                } else if (this.F0 == Integer.MIN_VALUE) {
                    View N = N(this.E0);
                    if (N != null) {
                        if (this.y0.e(N) > this.y0.n()) {
                            aVar.a();
                            return true;
                        } else if (this.y0.g(N) - this.y0.m() < 0) {
                            aVar.c = this.y0.m();
                            aVar.d = false;
                            return true;
                        } else if (this.y0.i() - this.y0.d(N) < 0) {
                            aVar.c = this.y0.i();
                            aVar.d = true;
                            return true;
                        } else {
                            if (aVar.d) {
                                g = this.y0.d(N) + this.y0.o();
                            } else {
                                g = this.y0.g(N);
                            }
                            aVar.c = g;
                        }
                    } else {
                        if (U() > 0) {
                            aVar.d = (this.E0 < o0(T(0))) == this.B0;
                        }
                        aVar.a();
                    }
                    return true;
                } else {
                    boolean z2 = this.B0;
                    aVar.d = z2;
                    if (z2) {
                        aVar.c = this.y0.i() - this.F0;
                    } else {
                        aVar.c = this.y0.m() + this.F0;
                    }
                    return true;
                }
            }
            this.E0 = -1;
            this.F0 = Integer.MIN_VALUE;
        }
        return false;
    }

    @Override // androidx.recyclerview.widget.RecyclerView.LayoutManager
    public RecyclerView.LayoutParams O() {
        return new RecyclerView.LayoutParams(-2, -2);
    }

    public final void O2(RecyclerView.t tVar, RecyclerView.x xVar, a aVar) {
        if (N2(xVar, aVar) || M2(tVar, xVar, aVar)) {
            return;
        }
        aVar.a();
        aVar.b = this.C0 ? xVar.b() - 1 : 0;
    }

    @Override // androidx.recyclerview.widget.RecyclerView.LayoutManager
    public boolean P1() {
        return (i0() == 1073741824 || w0() == 1073741824 || !x0()) ? false : true;
    }

    public final void P2(int i, int i2, boolean z, RecyclerView.x xVar) {
        int m;
        this.x0.m = F2();
        this.x0.f = i;
        int[] iArr = this.L0;
        iArr[0] = 0;
        iArr[1] = 0;
        V1(xVar, iArr);
        int max = Math.max(0, this.L0[0]);
        int max2 = Math.max(0, this.L0[1]);
        boolean z2 = i == 1;
        c cVar = this.x0;
        int i3 = z2 ? max2 : max;
        cVar.h = i3;
        if (!z2) {
            max = max2;
        }
        cVar.i = max;
        if (z2) {
            cVar.h = i3 + this.y0.j();
            View s2 = s2();
            c cVar2 = this.x0;
            cVar2.e = this.B0 ? -1 : 1;
            int o0 = o0(s2);
            c cVar3 = this.x0;
            cVar2.d = o0 + cVar3.e;
            cVar3.b = this.y0.d(s2);
            m = this.y0.d(s2) - this.y0.i();
        } else {
            View t2 = t2();
            this.x0.h += this.y0.m();
            c cVar4 = this.x0;
            cVar4.e = this.B0 ? 1 : -1;
            int o02 = o0(t2);
            c cVar5 = this.x0;
            cVar4.d = o02 + cVar5.e;
            cVar5.b = this.y0.g(t2);
            m = (-this.y0.g(t2)) + this.y0.m();
        }
        c cVar6 = this.x0;
        cVar6.c = i2;
        if (z) {
            cVar6.c = i2 - m;
        }
        cVar6.g = m;
    }

    @Override // androidx.recyclerview.widget.RecyclerView.LayoutManager
    public void Q0(RecyclerView recyclerView, RecyclerView.t tVar) {
        super.Q0(recyclerView, tVar);
        if (this.G0) {
            t1(tVar);
            tVar.c();
        }
    }

    public final void Q2(int i, int i2) {
        this.x0.c = this.y0.i() - i2;
        c cVar = this.x0;
        cVar.e = this.B0 ? -1 : 1;
        cVar.d = i;
        cVar.f = 1;
        cVar.b = i2;
        cVar.g = Integer.MIN_VALUE;
    }

    @Override // androidx.recyclerview.widget.RecyclerView.LayoutManager
    public View R0(View view, int i, RecyclerView.t tVar, RecyclerView.x xVar) {
        int a2;
        View n2;
        View s2;
        G2();
        if (U() == 0 || (a2 = a2(i)) == Integer.MIN_VALUE) {
            return null;
        }
        c2();
        P2(a2, (int) (this.y0.n() * 0.33333334f), false, xVar);
        c cVar = this.x0;
        cVar.g = Integer.MIN_VALUE;
        cVar.a = false;
        d2(tVar, cVar, xVar, true);
        if (a2 == -1) {
            n2 = o2();
        } else {
            n2 = n2();
        }
        if (a2 == -1) {
            s2 = t2();
        } else {
            s2 = s2();
        }
        if (s2.hasFocusable()) {
            if (n2 == null) {
                return null;
            }
            return s2;
        }
        return n2;
    }

    @Override // androidx.recyclerview.widget.RecyclerView.LayoutManager
    public void R1(RecyclerView recyclerView, RecyclerView.x xVar, int i) {
        m mVar = new m(recyclerView.getContext());
        mVar.setTargetPosition(i);
        S1(mVar);
    }

    public final void R2(a aVar) {
        Q2(aVar.b, aVar.c);
    }

    @Override // androidx.recyclerview.widget.RecyclerView.LayoutManager
    public void S0(AccessibilityEvent accessibilityEvent) {
        super.S0(accessibilityEvent);
        if (U() > 0) {
            accessibilityEvent.setFromIndex(h2());
            accessibilityEvent.setToIndex(k2());
        }
    }

    public final void S2(int i, int i2) {
        this.x0.c = i2 - this.y0.m();
        c cVar = this.x0;
        cVar.d = i;
        cVar.e = this.B0 ? 1 : -1;
        cVar.f = -1;
        cVar.b = i2;
        cVar.g = Integer.MIN_VALUE;
    }

    public final void T2(a aVar) {
        S2(aVar.b, aVar.c);
    }

    @Override // androidx.recyclerview.widget.RecyclerView.LayoutManager
    public boolean U1() {
        return this.H0 == null && this.z0 == this.C0;
    }

    public void V1(RecyclerView.x xVar, int[] iArr) {
        int i;
        int u2 = u2(xVar);
        if (this.x0.f == -1) {
            i = 0;
        } else {
            i = u2;
            u2 = 0;
        }
        iArr[0] = u2;
        iArr[1] = i;
    }

    public void W1(RecyclerView.x xVar, c cVar, RecyclerView.LayoutManager.c cVar2) {
        int i = cVar.d;
        if (i < 0 || i >= xVar.b()) {
            return;
        }
        cVar2.a(i, Math.max(0, cVar.g));
    }

    public final int X1(RecyclerView.x xVar) {
        if (U() == 0) {
            return 0;
        }
        c2();
        return t.a(xVar, this.y0, g2(!this.D0, true), f2(!this.D0, true), this, this.D0);
    }

    public final int Y1(RecyclerView.x xVar) {
        if (U() == 0) {
            return 0;
        }
        c2();
        return t.b(xVar, this.y0, g2(!this.D0, true), f2(!this.D0, true), this, this.D0, this.B0);
    }

    public final int Z1(RecyclerView.x xVar) {
        if (U() == 0) {
            return 0;
        }
        c2();
        return t.c(xVar, this.y0, g2(!this.D0, true), f2(!this.D0, true), this, this.D0);
    }

    @Override // androidx.recyclerview.widget.RecyclerView.w.b
    public PointF a(int i) {
        if (U() == 0) {
            return null;
        }
        int i2 = (i < o0(T(0))) != this.B0 ? -1 : 1;
        if (this.w0 == 0) {
            return new PointF(i2, Utils.FLOAT_EPSILON);
        }
        return new PointF(Utils.FLOAT_EPSILON, i2);
    }

    public int a2(int i) {
        return i != 1 ? i != 2 ? i != 17 ? i != 33 ? i != 66 ? (i == 130 && this.w0 == 1) ? 1 : Integer.MIN_VALUE : this.w0 == 0 ? 1 : Integer.MIN_VALUE : this.w0 == 1 ? -1 : Integer.MIN_VALUE : this.w0 == 0 ? -1 : Integer.MIN_VALUE : (this.w0 != 1 && w2()) ? -1 : 1 : (this.w0 != 1 && w2()) ? 1 : -1;
    }

    public c b2() {
        return new c();
    }

    public void c2() {
        if (this.x0 == null) {
            this.x0 = b2();
        }
    }

    public int d2(RecyclerView.t tVar, c cVar, RecyclerView.x xVar, boolean z) {
        int i = cVar.c;
        int i2 = cVar.g;
        if (i2 != Integer.MIN_VALUE) {
            if (i < 0) {
                cVar.g = i2 + i;
            }
            B2(tVar, cVar);
        }
        int i3 = cVar.c + cVar.h;
        b bVar = this.J0;
        while (true) {
            if ((!cVar.m && i3 <= 0) || !cVar.c(xVar)) {
                break;
            }
            bVar.a();
            y2(tVar, xVar, cVar, bVar);
            if (!bVar.b) {
                cVar.b += bVar.a * cVar.f;
                if (!bVar.c || cVar.l != null || !xVar.e()) {
                    int i4 = cVar.c;
                    int i5 = bVar.a;
                    cVar.c = i4 - i5;
                    i3 -= i5;
                }
                int i6 = cVar.g;
                if (i6 != Integer.MIN_VALUE) {
                    int i7 = i6 + bVar.a;
                    cVar.g = i7;
                    int i8 = cVar.c;
                    if (i8 < 0) {
                        cVar.g = i7 + i8;
                    }
                    B2(tVar, cVar);
                }
                if (z && bVar.d) {
                    break;
                }
            } else {
                break;
            }
        }
        return i - cVar.c;
    }

    public final View e2() {
        return l2(0, U());
    }

    @Override // androidx.recyclerview.widget.RecyclerView.LayoutManager
    public void f1(RecyclerView.t tVar, RecyclerView.x xVar) {
        int i;
        int i2;
        int i3;
        int i4;
        int q2;
        int i5;
        View N;
        int g;
        int i6;
        int i7 = -1;
        if ((this.H0 != null || this.E0 != -1) && xVar.b() == 0) {
            t1(tVar);
            return;
        }
        SavedState savedState = this.H0;
        if (savedState != null && savedState.a()) {
            this.E0 = this.H0.a;
        }
        c2();
        this.x0.a = false;
        G2();
        View g0 = g0();
        a aVar = this.I0;
        if (aVar.e && this.E0 == -1 && this.H0 == null) {
            if (g0 != null && (this.y0.g(g0) >= this.y0.i() || this.y0.d(g0) <= this.y0.m())) {
                this.I0.c(g0, o0(g0));
            }
        } else {
            aVar.e();
            a aVar2 = this.I0;
            aVar2.d = this.B0 ^ this.C0;
            O2(tVar, xVar, aVar2);
            this.I0.e = true;
        }
        c cVar = this.x0;
        cVar.f = cVar.k >= 0 ? 1 : -1;
        int[] iArr = this.L0;
        iArr[0] = 0;
        iArr[1] = 0;
        V1(xVar, iArr);
        int max = Math.max(0, this.L0[0]) + this.y0.m();
        int max2 = Math.max(0, this.L0[1]) + this.y0.j();
        if (xVar.e() && (i5 = this.E0) != -1 && this.F0 != Integer.MIN_VALUE && (N = N(i5)) != null) {
            if (this.B0) {
                i6 = this.y0.i() - this.y0.d(N);
                g = this.F0;
            } else {
                g = this.y0.g(N) - this.y0.m();
                i6 = this.F0;
            }
            int i8 = i6 - g;
            if (i8 > 0) {
                max += i8;
            } else {
                max2 -= i8;
            }
        }
        a aVar3 = this.I0;
        if (!aVar3.d ? !this.B0 : this.B0) {
            i7 = 1;
        }
        A2(tVar, xVar, aVar3, i7);
        H(tVar);
        this.x0.m = F2();
        this.x0.j = xVar.e();
        this.x0.i = 0;
        a aVar4 = this.I0;
        if (aVar4.d) {
            T2(aVar4);
            c cVar2 = this.x0;
            cVar2.h = max;
            d2(tVar, cVar2, xVar, false);
            c cVar3 = this.x0;
            i2 = cVar3.b;
            int i9 = cVar3.d;
            int i10 = cVar3.c;
            if (i10 > 0) {
                max2 += i10;
            }
            R2(this.I0);
            c cVar4 = this.x0;
            cVar4.h = max2;
            cVar4.d += cVar4.e;
            d2(tVar, cVar4, xVar, false);
            c cVar5 = this.x0;
            i = cVar5.b;
            int i11 = cVar5.c;
            if (i11 > 0) {
                S2(i9, i2);
                c cVar6 = this.x0;
                cVar6.h = i11;
                d2(tVar, cVar6, xVar, false);
                i2 = this.x0.b;
            }
        } else {
            R2(aVar4);
            c cVar7 = this.x0;
            cVar7.h = max2;
            d2(tVar, cVar7, xVar, false);
            c cVar8 = this.x0;
            i = cVar8.b;
            int i12 = cVar8.d;
            int i13 = cVar8.c;
            if (i13 > 0) {
                max += i13;
            }
            T2(this.I0);
            c cVar9 = this.x0;
            cVar9.h = max;
            cVar9.d += cVar9.e;
            d2(tVar, cVar9, xVar, false);
            c cVar10 = this.x0;
            i2 = cVar10.b;
            int i14 = cVar10.c;
            if (i14 > 0) {
                Q2(i12, i);
                c cVar11 = this.x0;
                cVar11.h = i14;
                d2(tVar, cVar11, xVar, false);
                i = this.x0.b;
            }
        }
        if (U() > 0) {
            if (this.B0 ^ this.C0) {
                int q22 = q2(i, tVar, xVar, true);
                i3 = i2 + q22;
                i4 = i + q22;
                q2 = r2(i3, tVar, xVar, false);
            } else {
                int r2 = r2(i2, tVar, xVar, true);
                i3 = i2 + r2;
                i4 = i + r2;
                q2 = q2(i4, tVar, xVar, false);
            }
            i2 = i3 + q2;
            i = i4 + q2;
        }
        z2(tVar, xVar, i2, i);
        if (!xVar.e()) {
            this.y0.s();
        } else {
            this.I0.e();
        }
        this.z0 = this.C0;
    }

    public View f2(boolean z, boolean z2) {
        if (this.B0) {
            return m2(0, U(), z, z2);
        }
        return m2(U() - 1, -1, z, z2);
    }

    @Override // androidx.recyclerview.widget.k.j
    public void g(View view, View view2, int i, int i2) {
        r("Cannot drop a view during a scroll or layout calculation");
        c2();
        G2();
        int o0 = o0(view);
        int o02 = o0(view2);
        boolean z = o0 < o02 ? true : true;
        if (this.B0) {
            if (z) {
                I2(o02, this.y0.i() - (this.y0.g(view2) + this.y0.e(view)));
            } else {
                I2(o02, this.y0.i() - this.y0.d(view2));
            }
        } else if (z) {
            I2(o02, this.y0.g(view2));
        } else {
            I2(o02, this.y0.d(view2) - this.y0.e(view));
        }
    }

    @Override // androidx.recyclerview.widget.RecyclerView.LayoutManager
    public void g1(RecyclerView.x xVar) {
        super.g1(xVar);
        this.H0 = null;
        this.E0 = -1;
        this.F0 = Integer.MIN_VALUE;
        this.I0.e();
    }

    public View g2(boolean z, boolean z2) {
        if (this.B0) {
            return m2(U() - 1, -1, z, z2);
        }
        return m2(0, U(), z, z2);
    }

    public int h2() {
        View m2 = m2(0, U(), false, true);
        if (m2 == null) {
            return -1;
        }
        return o0(m2);
    }

    public int i2() {
        View m2 = m2(U() - 1, -1, true, false);
        if (m2 == null) {
            return -1;
        }
        return o0(m2);
    }

    public final View j2() {
        return l2(U() - 1, -1);
    }

    @Override // androidx.recyclerview.widget.RecyclerView.LayoutManager
    public void k1(Parcelable parcelable) {
        if (parcelable instanceof SavedState) {
            SavedState savedState = (SavedState) parcelable;
            this.H0 = savedState;
            if (this.E0 != -1) {
                savedState.b();
            }
            C1();
        }
    }

    public int k2() {
        View m2 = m2(U() - 1, -1, false, true);
        if (m2 == null) {
            return -1;
        }
        return o0(m2);
    }

    @Override // androidx.recyclerview.widget.RecyclerView.LayoutManager
    public Parcelable l1() {
        if (this.H0 != null) {
            return new SavedState(this.H0);
        }
        SavedState savedState = new SavedState();
        if (U() > 0) {
            c2();
            boolean z = this.z0 ^ this.B0;
            savedState.g0 = z;
            if (z) {
                View s2 = s2();
                savedState.f0 = this.y0.i() - this.y0.d(s2);
                savedState.a = o0(s2);
            } else {
                View t2 = t2();
                savedState.a = o0(t2);
                savedState.f0 = this.y0.g(t2) - this.y0.m();
            }
        } else {
            savedState.b();
        }
        return savedState;
    }

    public View l2(int i, int i2) {
        int i3;
        int i4;
        c2();
        if ((i2 > i ? (char) 1 : i2 < i ? (char) 65535 : (char) 0) == 0) {
            return T(i);
        }
        if (this.y0.g(T(i)) < this.y0.m()) {
            i3 = 16644;
            i4 = 16388;
        } else {
            i3 = 4161;
            i4 = 4097;
        }
        if (this.w0 == 0) {
            return this.i0.a(i, i2, i3, i4);
        }
        return this.j0.a(i, i2, i3, i4);
    }

    public View m2(int i, int i2, boolean z, boolean z2) {
        c2();
        int i3 = z ? 24579 : 320;
        int i4 = z2 ? 320 : 0;
        if (this.w0 == 0) {
            return this.i0.a(i, i2, i3, i4);
        }
        return this.j0.a(i, i2, i3, i4);
    }

    public final View n2() {
        return this.B0 ? e2() : j2();
    }

    public final View o2() {
        return this.B0 ? j2() : e2();
    }

    public View p2(RecyclerView.t tVar, RecyclerView.x xVar, boolean z, boolean z2) {
        int i;
        int i2;
        c2();
        int U = U();
        int i3 = -1;
        if (z2) {
            i = U() - 1;
            i2 = -1;
        } else {
            i3 = U;
            i = 0;
            i2 = 1;
        }
        int b2 = xVar.b();
        int m = this.y0.m();
        int i4 = this.y0.i();
        View view = null;
        View view2 = null;
        View view3 = null;
        while (i != i3) {
            View T = T(i);
            int o0 = o0(T);
            int g = this.y0.g(T);
            int d = this.y0.d(T);
            if (o0 >= 0 && o0 < b2) {
                if (!((RecyclerView.LayoutParams) T.getLayoutParams()).c()) {
                    boolean z3 = d <= m && g < m;
                    boolean z4 = g >= i4 && d > i4;
                    if (!z3 && !z4) {
                        return T;
                    }
                    if (z) {
                        if (!z4) {
                            if (view != null) {
                            }
                            view = T;
                        }
                        view2 = T;
                    } else {
                        if (!z3) {
                            if (view != null) {
                            }
                            view = T;
                        }
                        view2 = T;
                    }
                } else if (view3 == null) {
                    view3 = T;
                }
            }
            i += i2;
        }
        return view != null ? view : view2 != null ? view2 : view3;
    }

    public final int q2(int i, RecyclerView.t tVar, RecyclerView.x xVar, boolean z) {
        int i2;
        int i3 = this.y0.i() - i;
        if (i3 > 0) {
            int i4 = -H2(-i3, tVar, xVar);
            int i5 = i + i4;
            if (!z || (i2 = this.y0.i() - i5) <= 0) {
                return i4;
            }
            this.y0.r(i2);
            return i2 + i4;
        }
        return 0;
    }

    @Override // androidx.recyclerview.widget.RecyclerView.LayoutManager
    public void r(String str) {
        if (this.H0 == null) {
            super.r(str);
        }
    }

    public final int r2(int i, RecyclerView.t tVar, RecyclerView.x xVar, boolean z) {
        int m;
        int m2 = i - this.y0.m();
        if (m2 > 0) {
            int i2 = -H2(m2, tVar, xVar);
            int i3 = i + i2;
            if (!z || (m = i3 - this.y0.m()) <= 0) {
                return i2;
            }
            this.y0.r(-m);
            return i2 - m;
        }
        return 0;
    }

    public final View s2() {
        return T(this.B0 ? 0 : U() - 1);
    }

    public final View t2() {
        return T(this.B0 ? U() - 1 : 0);
    }

    @Deprecated
    public int u2(RecyclerView.x xVar) {
        if (xVar.d()) {
            return this.y0.n();
        }
        return 0;
    }

    @Override // androidx.recyclerview.widget.RecyclerView.LayoutManager
    public boolean v() {
        return this.w0 == 0;
    }

    public int v2() {
        return this.w0;
    }

    @Override // androidx.recyclerview.widget.RecyclerView.LayoutManager
    public boolean w() {
        return this.w0 == 1;
    }

    public boolean w2() {
        return k0() == 1;
    }

    public boolean x2() {
        return this.D0;
    }

    public void y2(RecyclerView.t tVar, RecyclerView.x xVar, c cVar, b bVar) {
        int i;
        int i2;
        int i3;
        int i4;
        int f;
        View d = cVar.d(tVar);
        if (d == null) {
            bVar.b = true;
            return;
        }
        RecyclerView.LayoutParams layoutParams = (RecyclerView.LayoutParams) d.getLayoutParams();
        if (cVar.l == null) {
            if (this.B0 == (cVar.f == -1)) {
                o(d);
            } else {
                p(d, 0);
            }
        } else {
            if (this.B0 == (cVar.f == -1)) {
                m(d);
            } else {
                n(d, 0);
            }
        }
        I0(d, 0, 0);
        bVar.a = this.y0.e(d);
        if (this.w0 == 1) {
            if (w2()) {
                f = v0() - getPaddingRight();
                i4 = f - this.y0.f(d);
            } else {
                i4 = getPaddingLeft();
                f = this.y0.f(d) + i4;
            }
            if (cVar.f == -1) {
                int i5 = cVar.b;
                i3 = i5;
                i2 = f;
                i = i5 - bVar.a;
            } else {
                int i6 = cVar.b;
                i = i6;
                i2 = f;
                i3 = bVar.a + i6;
            }
        } else {
            int paddingTop = getPaddingTop();
            int f2 = this.y0.f(d) + paddingTop;
            if (cVar.f == -1) {
                int i7 = cVar.b;
                i2 = i7;
                i = paddingTop;
                i3 = f2;
                i4 = i7 - bVar.a;
            } else {
                int i8 = cVar.b;
                i = paddingTop;
                i2 = bVar.a + i8;
                i3 = f2;
                i4 = i8;
            }
        }
        H0(d, i4, i, i2, i3);
        if (layoutParams.c() || layoutParams.b()) {
            bVar.c = true;
        }
        bVar.d = d.hasFocusable();
    }

    @Override // androidx.recyclerview.widget.RecyclerView.LayoutManager
    public void z(int i, int i2, RecyclerView.x xVar, RecyclerView.LayoutManager.c cVar) {
        if (this.w0 != 0) {
            i = i2;
        }
        if (U() == 0 || i == 0) {
            return;
        }
        c2();
        P2(i > 0 ? 1 : -1, Math.abs(i), true, xVar);
        W1(xVar, this.x0, cVar);
    }

    @Override // androidx.recyclerview.widget.RecyclerView.LayoutManager
    public boolean z0() {
        return true;
    }

    public final void z2(RecyclerView.t tVar, RecyclerView.x xVar, int i, int i2) {
        if (!xVar.g() || U() == 0 || xVar.e() || !U1()) {
            return;
        }
        List<RecyclerView.a0> k = tVar.k();
        int size = k.size();
        int o0 = o0(T(0));
        int i3 = 0;
        int i4 = 0;
        for (int i5 = 0; i5 < size; i5++) {
            RecyclerView.a0 a0Var = k.get(i5);
            if (!a0Var.isRemoved()) {
                if ((a0Var.getLayoutPosition() < o0) != this.B0 ? true : true) {
                    i3 += this.y0.e(a0Var.itemView);
                } else {
                    i4 += this.y0.e(a0Var.itemView);
                }
            }
        }
        this.x0.l = k;
        if (i3 > 0) {
            S2(o0(t2()), i);
            c cVar = this.x0;
            cVar.h = i3;
            cVar.c = 0;
            cVar.a();
            d2(tVar, this.x0, xVar, false);
        }
        if (i4 > 0) {
            Q2(o0(s2()), i2);
            c cVar2 = this.x0;
            cVar2.h = i4;
            cVar2.c = 0;
            cVar2.a();
            d2(tVar, this.x0, xVar, false);
        }
        this.x0.l = null;
    }

    public LinearLayoutManager(Context context, int i, boolean z) {
        this.w0 = 1;
        this.A0 = false;
        this.B0 = false;
        this.C0 = false;
        this.D0 = true;
        this.E0 = -1;
        this.F0 = Integer.MIN_VALUE;
        this.H0 = null;
        this.I0 = new a();
        this.J0 = new b();
        this.K0 = 2;
        this.L0 = new int[2];
        J2(i);
        K2(z);
    }

    public LinearLayoutManager(Context context, AttributeSet attributeSet, int i, int i2) {
        this.w0 = 1;
        this.A0 = false;
        this.B0 = false;
        this.C0 = false;
        this.D0 = true;
        this.E0 = -1;
        this.F0 = Integer.MIN_VALUE;
        this.H0 = null;
        this.I0 = new a();
        this.J0 = new b();
        this.K0 = 2;
        this.L0 = new int[2];
        RecyclerView.LayoutManager.Properties p0 = RecyclerView.LayoutManager.p0(context, attributeSet, i, i2);
        J2(p0.a);
        K2(p0.c);
        L2(p0.d);
    }
}
