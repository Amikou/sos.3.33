package androidx.recyclerview.widget;

import android.view.View;
import androidx.recyclerview.widget.RecyclerView;

/* compiled from: SimpleItemAnimator.java */
/* loaded from: classes.dex */
public abstract class u extends RecyclerView.l {
    private static final boolean DEBUG = false;
    private static final String TAG = "SimpleItemAnimator";
    public boolean mSupportsChangeAnimations = true;

    public abstract boolean animateAdd(RecyclerView.a0 a0Var);

    @Override // androidx.recyclerview.widget.RecyclerView.l
    public boolean animateAppearance(RecyclerView.a0 a0Var, RecyclerView.l.c cVar, RecyclerView.l.c cVar2) {
        int i;
        int i2;
        if (cVar != null && ((i = cVar.a) != (i2 = cVar2.a) || cVar.b != cVar2.b)) {
            return animateMove(a0Var, i, cVar.b, i2, cVar2.b);
        }
        return animateAdd(a0Var);
    }

    public abstract boolean animateChange(RecyclerView.a0 a0Var, RecyclerView.a0 a0Var2, int i, int i2, int i3, int i4);

    @Override // androidx.recyclerview.widget.RecyclerView.l
    public boolean animateChange(RecyclerView.a0 a0Var, RecyclerView.a0 a0Var2, RecyclerView.l.c cVar, RecyclerView.l.c cVar2) {
        int i;
        int i2;
        int i3 = cVar.a;
        int i4 = cVar.b;
        if (a0Var2.shouldIgnore()) {
            int i5 = cVar.a;
            i2 = cVar.b;
            i = i5;
        } else {
            i = cVar2.a;
            i2 = cVar2.b;
        }
        return animateChange(a0Var, a0Var2, i3, i4, i, i2);
    }

    @Override // androidx.recyclerview.widget.RecyclerView.l
    public boolean animateDisappearance(RecyclerView.a0 a0Var, RecyclerView.l.c cVar, RecyclerView.l.c cVar2) {
        int i = cVar.a;
        int i2 = cVar.b;
        View view = a0Var.itemView;
        int left = cVar2 == null ? view.getLeft() : cVar2.a;
        int top = cVar2 == null ? view.getTop() : cVar2.b;
        if (!a0Var.isRemoved() && (i != left || i2 != top)) {
            view.layout(left, top, view.getWidth() + left, view.getHeight() + top);
            return animateMove(a0Var, i, i2, left, top);
        }
        return animateRemove(a0Var);
    }

    public abstract boolean animateMove(RecyclerView.a0 a0Var, int i, int i2, int i3, int i4);

    @Override // androidx.recyclerview.widget.RecyclerView.l
    public boolean animatePersistence(RecyclerView.a0 a0Var, RecyclerView.l.c cVar, RecyclerView.l.c cVar2) {
        int i = cVar.a;
        int i2 = cVar2.a;
        if (i == i2 && cVar.b == cVar2.b) {
            dispatchMoveFinished(a0Var);
            return false;
        }
        return animateMove(a0Var, i, cVar.b, i2, cVar2.b);
    }

    public abstract boolean animateRemove(RecyclerView.a0 a0Var);

    @Override // androidx.recyclerview.widget.RecyclerView.l
    public boolean canReuseUpdatedViewHolder(RecyclerView.a0 a0Var) {
        return !this.mSupportsChangeAnimations || a0Var.isInvalid();
    }

    public final void dispatchAddFinished(RecyclerView.a0 a0Var) {
        onAddFinished(a0Var);
        dispatchAnimationFinished(a0Var);
    }

    public final void dispatchAddStarting(RecyclerView.a0 a0Var) {
        onAddStarting(a0Var);
    }

    public final void dispatchChangeFinished(RecyclerView.a0 a0Var, boolean z) {
        onChangeFinished(a0Var, z);
        dispatchAnimationFinished(a0Var);
    }

    public final void dispatchChangeStarting(RecyclerView.a0 a0Var, boolean z) {
        onChangeStarting(a0Var, z);
    }

    public final void dispatchMoveFinished(RecyclerView.a0 a0Var) {
        onMoveFinished(a0Var);
        dispatchAnimationFinished(a0Var);
    }

    public final void dispatchMoveStarting(RecyclerView.a0 a0Var) {
        onMoveStarting(a0Var);
    }

    public final void dispatchRemoveFinished(RecyclerView.a0 a0Var) {
        onRemoveFinished(a0Var);
        dispatchAnimationFinished(a0Var);
    }

    public final void dispatchRemoveStarting(RecyclerView.a0 a0Var) {
        onRemoveStarting(a0Var);
    }

    public boolean getSupportsChangeAnimations() {
        return this.mSupportsChangeAnimations;
    }

    public void onAddFinished(RecyclerView.a0 a0Var) {
    }

    public void onAddStarting(RecyclerView.a0 a0Var) {
    }

    public void onChangeFinished(RecyclerView.a0 a0Var, boolean z) {
    }

    public void onChangeStarting(RecyclerView.a0 a0Var, boolean z) {
    }

    public void onMoveFinished(RecyclerView.a0 a0Var) {
    }

    public void onMoveStarting(RecyclerView.a0 a0Var) {
    }

    public void onRemoveFinished(RecyclerView.a0 a0Var) {
    }

    public void onRemoveStarting(RecyclerView.a0 a0Var) {
    }

    public void setSupportsChangeAnimations(boolean z) {
        this.mSupportsChangeAnimations = z;
    }
}
