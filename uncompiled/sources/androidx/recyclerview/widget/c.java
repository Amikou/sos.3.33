package androidx.recyclerview.widget;

import androidx.recyclerview.widget.g;
import java.util.concurrent.Executor;
import java.util.concurrent.Executors;

/* compiled from: AsyncDifferConfig.java */
/* loaded from: classes.dex */
public final class c<T> {
    public final Executor a;
    public final Executor b;
    public final g.f<T> c;

    /* compiled from: AsyncDifferConfig.java */
    /* loaded from: classes.dex */
    public static final class a<T> {
        public static final Object d = new Object();
        public static Executor e;
        public Executor a;
        public Executor b;
        public final g.f<T> c;

        public a(g.f<T> fVar) {
            this.c = fVar;
        }

        public c<T> a() {
            if (this.b == null) {
                synchronized (d) {
                    if (e == null) {
                        e = Executors.newFixedThreadPool(2);
                    }
                }
                this.b = e;
            }
            return new c<>(this.a, this.b, this.c);
        }
    }

    public c(Executor executor, Executor executor2, g.f<T> fVar) {
        this.a = executor;
        this.b = executor2;
        this.c = fVar;
    }

    public Executor a() {
        return this.b;
    }

    public g.f<T> b() {
        return this.c;
    }

    public Executor c() {
        return this.a;
    }
}
