package androidx.recyclerview.widget;

import android.view.View;
import androidx.recyclerview.widget.RecyclerView;

/* compiled from: ScrollbarHelper.java */
/* loaded from: classes.dex */
public class t {
    public static int a(RecyclerView.x xVar, q qVar, View view, View view2, RecyclerView.LayoutManager layoutManager, boolean z) {
        if (layoutManager.U() == 0 || xVar.b() == 0 || view == null || view2 == null) {
            return 0;
        }
        if (!z) {
            return Math.abs(layoutManager.o0(view) - layoutManager.o0(view2)) + 1;
        }
        return Math.min(qVar.n(), qVar.d(view2) - qVar.g(view));
    }

    public static int b(RecyclerView.x xVar, q qVar, View view, View view2, RecyclerView.LayoutManager layoutManager, boolean z, boolean z2) {
        int max;
        if (layoutManager.U() == 0 || xVar.b() == 0 || view == null || view2 == null) {
            return 0;
        }
        int min = Math.min(layoutManager.o0(view), layoutManager.o0(view2));
        int max2 = Math.max(layoutManager.o0(view), layoutManager.o0(view2));
        if (z2) {
            max = Math.max(0, (xVar.b() - max2) - 1);
        } else {
            max = Math.max(0, min);
        }
        if (z) {
            return Math.round((max * (Math.abs(qVar.d(view2) - qVar.g(view)) / (Math.abs(layoutManager.o0(view) - layoutManager.o0(view2)) + 1))) + (qVar.m() - qVar.g(view)));
        }
        return max;
    }

    public static int c(RecyclerView.x xVar, q qVar, View view, View view2, RecyclerView.LayoutManager layoutManager, boolean z) {
        if (layoutManager.U() == 0 || xVar.b() == 0 || view == null || view2 == null) {
            return 0;
        }
        if (!z) {
            return xVar.b();
        }
        return (int) (((qVar.d(view2) - qVar.g(view)) / (Math.abs(layoutManager.o0(view) - layoutManager.o0(view2)) + 1)) * xVar.b());
    }
}
