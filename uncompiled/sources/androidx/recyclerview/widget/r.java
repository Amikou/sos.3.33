package androidx.recyclerview.widget;

import android.content.Context;
import android.graphics.PointF;
import android.util.DisplayMetrics;
import android.view.View;
import androidx.recyclerview.widget.RecyclerView;
import com.github.mikephil.charting.utils.Utils;

/* compiled from: PagerSnapHelper.java */
/* loaded from: classes.dex */
public class r extends v {
    public q d;
    public q e;

    /* compiled from: PagerSnapHelper.java */
    /* loaded from: classes.dex */
    public class a extends m {
        public a(Context context) {
            super(context);
        }

        @Override // androidx.recyclerview.widget.m
        public float calculateSpeedPerPixel(DisplayMetrics displayMetrics) {
            return 100.0f / displayMetrics.densityDpi;
        }

        @Override // androidx.recyclerview.widget.m
        public int calculateTimeForScrolling(int i) {
            return Math.min(100, super.calculateTimeForScrolling(i));
        }

        @Override // androidx.recyclerview.widget.m, androidx.recyclerview.widget.RecyclerView.w
        public void onTargetFound(View view, RecyclerView.x xVar, RecyclerView.w.a aVar) {
            r rVar = r.this;
            int[] c = rVar.c(rVar.a.getLayoutManager(), view);
            int i = c[0];
            int i2 = c[1];
            int calculateTimeForDeceleration = calculateTimeForDeceleration(Math.max(Math.abs(i), Math.abs(i2)));
            if (calculateTimeForDeceleration > 0) {
                aVar.d(i, i2, calculateTimeForDeceleration, this.mDecelerateInterpolator);
            }
        }
    }

    @Override // androidx.recyclerview.widget.v
    public int[] c(RecyclerView.LayoutManager layoutManager, View view) {
        int[] iArr = new int[2];
        if (layoutManager.v()) {
            iArr[0] = m(view, o(layoutManager));
        } else {
            iArr[0] = 0;
        }
        if (layoutManager.w()) {
            iArr[1] = m(view, q(layoutManager));
        } else {
            iArr[1] = 0;
        }
        return iArr;
    }

    @Override // androidx.recyclerview.widget.v
    public RecyclerView.w e(RecyclerView.LayoutManager layoutManager) {
        if (layoutManager instanceof RecyclerView.w.b) {
            return new a(this.a.getContext());
        }
        return null;
    }

    @Override // androidx.recyclerview.widget.v
    public View h(RecyclerView.LayoutManager layoutManager) {
        if (layoutManager.w()) {
            return n(layoutManager, q(layoutManager));
        }
        if (layoutManager.v()) {
            return n(layoutManager, o(layoutManager));
        }
        return null;
    }

    @Override // androidx.recyclerview.widget.v
    public int i(RecyclerView.LayoutManager layoutManager, int i, int i2) {
        q p;
        int j0 = layoutManager.j0();
        if (j0 == 0 || (p = p(layoutManager)) == null) {
            return -1;
        }
        int i3 = Integer.MIN_VALUE;
        int i4 = Integer.MAX_VALUE;
        int U = layoutManager.U();
        View view = null;
        View view2 = null;
        for (int i5 = 0; i5 < U; i5++) {
            View T = layoutManager.T(i5);
            if (T != null) {
                int m = m(T, p);
                if (m <= 0 && m > i3) {
                    view2 = T;
                    i3 = m;
                }
                if (m >= 0 && m < i4) {
                    view = T;
                    i4 = m;
                }
            }
        }
        boolean r = r(layoutManager, i, i2);
        if (!r || view == null) {
            if (r || view2 == null) {
                if (r) {
                    view = view2;
                }
                if (view == null) {
                    return -1;
                }
                int o0 = layoutManager.o0(view) + (s(layoutManager) == r ? -1 : 1);
                if (o0 < 0 || o0 >= j0) {
                    return -1;
                }
                return o0;
            }
            return layoutManager.o0(view2);
        }
        return layoutManager.o0(view);
    }

    public final int m(View view, q qVar) {
        return (qVar.g(view) + (qVar.e(view) / 2)) - (qVar.m() + (qVar.n() / 2));
    }

    public final View n(RecyclerView.LayoutManager layoutManager, q qVar) {
        int U = layoutManager.U();
        View view = null;
        if (U == 0) {
            return null;
        }
        int m = qVar.m() + (qVar.n() / 2);
        int i = Integer.MAX_VALUE;
        for (int i2 = 0; i2 < U; i2++) {
            View T = layoutManager.T(i2);
            int abs = Math.abs((qVar.g(T) + (qVar.e(T) / 2)) - m);
            if (abs < i) {
                view = T;
                i = abs;
            }
        }
        return view;
    }

    public final q o(RecyclerView.LayoutManager layoutManager) {
        q qVar = this.e;
        if (qVar == null || qVar.a != layoutManager) {
            this.e = q.a(layoutManager);
        }
        return this.e;
    }

    public final q p(RecyclerView.LayoutManager layoutManager) {
        if (layoutManager.w()) {
            return q(layoutManager);
        }
        if (layoutManager.v()) {
            return o(layoutManager);
        }
        return null;
    }

    public final q q(RecyclerView.LayoutManager layoutManager) {
        q qVar = this.d;
        if (qVar == null || qVar.a != layoutManager) {
            this.d = q.c(layoutManager);
        }
        return this.d;
    }

    public final boolean r(RecyclerView.LayoutManager layoutManager, int i, int i2) {
        return layoutManager.v() ? i > 0 : i2 > 0;
    }

    public final boolean s(RecyclerView.LayoutManager layoutManager) {
        PointF a2;
        int j0 = layoutManager.j0();
        if (!(layoutManager instanceof RecyclerView.w.b) || (a2 = ((RecyclerView.w.b) layoutManager).a(j0 - 1)) == null) {
            return false;
        }
        return a2.x < Utils.FLOAT_EPSILON || a2.y < Utils.FLOAT_EPSILON;
    }
}
