package androidx.recyclerview.widget;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.animation.TimeInterpolator;
import android.animation.ValueAnimator;
import android.view.View;
import android.view.ViewPropertyAnimator;
import androidx.recyclerview.widget.RecyclerView;
import com.github.mikephil.charting.utils.Utils;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

/* compiled from: DefaultItemAnimator.java */
/* loaded from: classes.dex */
public class f extends u {
    private static final boolean DEBUG = false;
    private static TimeInterpolator sDefaultInterpolator;
    private ArrayList<RecyclerView.a0> mPendingRemovals = new ArrayList<>();
    private ArrayList<RecyclerView.a0> mPendingAdditions = new ArrayList<>();
    private ArrayList<j> mPendingMoves = new ArrayList<>();
    private ArrayList<i> mPendingChanges = new ArrayList<>();
    public ArrayList<ArrayList<RecyclerView.a0>> mAdditionsList = new ArrayList<>();
    public ArrayList<ArrayList<j>> mMovesList = new ArrayList<>();
    public ArrayList<ArrayList<i>> mChangesList = new ArrayList<>();
    public ArrayList<RecyclerView.a0> mAddAnimations = new ArrayList<>();
    public ArrayList<RecyclerView.a0> mMoveAnimations = new ArrayList<>();
    public ArrayList<RecyclerView.a0> mRemoveAnimations = new ArrayList<>();
    public ArrayList<RecyclerView.a0> mChangeAnimations = new ArrayList<>();

    /* compiled from: DefaultItemAnimator.java */
    /* loaded from: classes.dex */
    public class a implements Runnable {
        public final /* synthetic */ ArrayList a;

        public a(ArrayList arrayList) {
            this.a = arrayList;
        }

        @Override // java.lang.Runnable
        public void run() {
            Iterator it = this.a.iterator();
            while (it.hasNext()) {
                j jVar = (j) it.next();
                f.this.animateMoveImpl(jVar.a, jVar.b, jVar.c, jVar.d, jVar.e);
            }
            this.a.clear();
            f.this.mMovesList.remove(this.a);
        }
    }

    /* compiled from: DefaultItemAnimator.java */
    /* loaded from: classes.dex */
    public class b implements Runnable {
        public final /* synthetic */ ArrayList a;

        public b(ArrayList arrayList) {
            this.a = arrayList;
        }

        @Override // java.lang.Runnable
        public void run() {
            Iterator it = this.a.iterator();
            while (it.hasNext()) {
                f.this.animateChangeImpl((i) it.next());
            }
            this.a.clear();
            f.this.mChangesList.remove(this.a);
        }
    }

    /* compiled from: DefaultItemAnimator.java */
    /* loaded from: classes.dex */
    public class c implements Runnable {
        public final /* synthetic */ ArrayList a;

        public c(ArrayList arrayList) {
            this.a = arrayList;
        }

        @Override // java.lang.Runnable
        public void run() {
            Iterator it = this.a.iterator();
            while (it.hasNext()) {
                f.this.animateAddImpl((RecyclerView.a0) it.next());
            }
            this.a.clear();
            f.this.mAdditionsList.remove(this.a);
        }
    }

    /* compiled from: DefaultItemAnimator.java */
    /* loaded from: classes.dex */
    public class d extends AnimatorListenerAdapter {
        public final /* synthetic */ RecyclerView.a0 a;
        public final /* synthetic */ ViewPropertyAnimator f0;
        public final /* synthetic */ View g0;

        public d(RecyclerView.a0 a0Var, ViewPropertyAnimator viewPropertyAnimator, View view) {
            this.a = a0Var;
            this.f0 = viewPropertyAnimator;
            this.g0 = view;
        }

        @Override // android.animation.AnimatorListenerAdapter, android.animation.Animator.AnimatorListener
        public void onAnimationEnd(Animator animator) {
            this.f0.setListener(null);
            this.g0.setAlpha(1.0f);
            f.this.dispatchRemoveFinished(this.a);
            f.this.mRemoveAnimations.remove(this.a);
            f.this.dispatchFinishedWhenDone();
        }

        @Override // android.animation.AnimatorListenerAdapter, android.animation.Animator.AnimatorListener
        public void onAnimationStart(Animator animator) {
            f.this.dispatchRemoveStarting(this.a);
        }
    }

    /* compiled from: DefaultItemAnimator.java */
    /* loaded from: classes.dex */
    public class e extends AnimatorListenerAdapter {
        public final /* synthetic */ RecyclerView.a0 a;
        public final /* synthetic */ View f0;
        public final /* synthetic */ ViewPropertyAnimator g0;

        public e(RecyclerView.a0 a0Var, View view, ViewPropertyAnimator viewPropertyAnimator) {
            this.a = a0Var;
            this.f0 = view;
            this.g0 = viewPropertyAnimator;
        }

        @Override // android.animation.AnimatorListenerAdapter, android.animation.Animator.AnimatorListener
        public void onAnimationCancel(Animator animator) {
            this.f0.setAlpha(1.0f);
        }

        @Override // android.animation.AnimatorListenerAdapter, android.animation.Animator.AnimatorListener
        public void onAnimationEnd(Animator animator) {
            this.g0.setListener(null);
            f.this.dispatchAddFinished(this.a);
            f.this.mAddAnimations.remove(this.a);
            f.this.dispatchFinishedWhenDone();
        }

        @Override // android.animation.AnimatorListenerAdapter, android.animation.Animator.AnimatorListener
        public void onAnimationStart(Animator animator) {
            f.this.dispatchAddStarting(this.a);
        }
    }

    /* compiled from: DefaultItemAnimator.java */
    /* renamed from: androidx.recyclerview.widget.f$f  reason: collision with other inner class name */
    /* loaded from: classes.dex */
    public class C0058f extends AnimatorListenerAdapter {
        public final /* synthetic */ RecyclerView.a0 a;
        public final /* synthetic */ int f0;
        public final /* synthetic */ View g0;
        public final /* synthetic */ int h0;
        public final /* synthetic */ ViewPropertyAnimator i0;

        public C0058f(RecyclerView.a0 a0Var, int i, View view, int i2, ViewPropertyAnimator viewPropertyAnimator) {
            this.a = a0Var;
            this.f0 = i;
            this.g0 = view;
            this.h0 = i2;
            this.i0 = viewPropertyAnimator;
        }

        @Override // android.animation.AnimatorListenerAdapter, android.animation.Animator.AnimatorListener
        public void onAnimationCancel(Animator animator) {
            if (this.f0 != 0) {
                this.g0.setTranslationX(Utils.FLOAT_EPSILON);
            }
            if (this.h0 != 0) {
                this.g0.setTranslationY(Utils.FLOAT_EPSILON);
            }
        }

        @Override // android.animation.AnimatorListenerAdapter, android.animation.Animator.AnimatorListener
        public void onAnimationEnd(Animator animator) {
            this.i0.setListener(null);
            f.this.dispatchMoveFinished(this.a);
            f.this.mMoveAnimations.remove(this.a);
            f.this.dispatchFinishedWhenDone();
        }

        @Override // android.animation.AnimatorListenerAdapter, android.animation.Animator.AnimatorListener
        public void onAnimationStart(Animator animator) {
            f.this.dispatchMoveStarting(this.a);
        }
    }

    /* compiled from: DefaultItemAnimator.java */
    /* loaded from: classes.dex */
    public class g extends AnimatorListenerAdapter {
        public final /* synthetic */ i a;
        public final /* synthetic */ ViewPropertyAnimator f0;
        public final /* synthetic */ View g0;

        public g(i iVar, ViewPropertyAnimator viewPropertyAnimator, View view) {
            this.a = iVar;
            this.f0 = viewPropertyAnimator;
            this.g0 = view;
        }

        @Override // android.animation.AnimatorListenerAdapter, android.animation.Animator.AnimatorListener
        public void onAnimationEnd(Animator animator) {
            this.f0.setListener(null);
            this.g0.setAlpha(1.0f);
            this.g0.setTranslationX(Utils.FLOAT_EPSILON);
            this.g0.setTranslationY(Utils.FLOAT_EPSILON);
            f.this.dispatchChangeFinished(this.a.a, true);
            f.this.mChangeAnimations.remove(this.a.a);
            f.this.dispatchFinishedWhenDone();
        }

        @Override // android.animation.AnimatorListenerAdapter, android.animation.Animator.AnimatorListener
        public void onAnimationStart(Animator animator) {
            f.this.dispatchChangeStarting(this.a.a, true);
        }
    }

    /* compiled from: DefaultItemAnimator.java */
    /* loaded from: classes.dex */
    public class h extends AnimatorListenerAdapter {
        public final /* synthetic */ i a;
        public final /* synthetic */ ViewPropertyAnimator f0;
        public final /* synthetic */ View g0;

        public h(i iVar, ViewPropertyAnimator viewPropertyAnimator, View view) {
            this.a = iVar;
            this.f0 = viewPropertyAnimator;
            this.g0 = view;
        }

        @Override // android.animation.AnimatorListenerAdapter, android.animation.Animator.AnimatorListener
        public void onAnimationEnd(Animator animator) {
            this.f0.setListener(null);
            this.g0.setAlpha(1.0f);
            this.g0.setTranslationX(Utils.FLOAT_EPSILON);
            this.g0.setTranslationY(Utils.FLOAT_EPSILON);
            f.this.dispatchChangeFinished(this.a.b, false);
            f.this.mChangeAnimations.remove(this.a.b);
            f.this.dispatchFinishedWhenDone();
        }

        @Override // android.animation.AnimatorListenerAdapter, android.animation.Animator.AnimatorListener
        public void onAnimationStart(Animator animator) {
            f.this.dispatchChangeStarting(this.a.b, false);
        }
    }

    /* compiled from: DefaultItemAnimator.java */
    /* loaded from: classes.dex */
    public static class j {
        public RecyclerView.a0 a;
        public int b;
        public int c;
        public int d;
        public int e;

        public j(RecyclerView.a0 a0Var, int i, int i2, int i3, int i4) {
            this.a = a0Var;
            this.b = i;
            this.c = i2;
            this.d = i3;
            this.e = i4;
        }
    }

    private void animateRemoveImpl(RecyclerView.a0 a0Var) {
        View view = a0Var.itemView;
        ViewPropertyAnimator animate = view.animate();
        this.mRemoveAnimations.add(a0Var);
        animate.setDuration(getRemoveDuration()).alpha(Utils.FLOAT_EPSILON).setListener(new d(a0Var, animate, view)).start();
    }

    private void endChangeAnimation(List<i> list, RecyclerView.a0 a0Var) {
        for (int size = list.size() - 1; size >= 0; size--) {
            i iVar = list.get(size);
            if (endChangeAnimationIfNecessary(iVar, a0Var) && iVar.a == null && iVar.b == null) {
                list.remove(iVar);
            }
        }
    }

    private void endChangeAnimationIfNecessary(i iVar) {
        RecyclerView.a0 a0Var = iVar.a;
        if (a0Var != null) {
            endChangeAnimationIfNecessary(iVar, a0Var);
        }
        RecyclerView.a0 a0Var2 = iVar.b;
        if (a0Var2 != null) {
            endChangeAnimationIfNecessary(iVar, a0Var2);
        }
    }

    private void resetAnimation(RecyclerView.a0 a0Var) {
        if (sDefaultInterpolator == null) {
            sDefaultInterpolator = new ValueAnimator().getInterpolator();
        }
        a0Var.itemView.animate().setInterpolator(sDefaultInterpolator);
        endAnimation(a0Var);
    }

    @Override // androidx.recyclerview.widget.u
    public boolean animateAdd(RecyclerView.a0 a0Var) {
        resetAnimation(a0Var);
        a0Var.itemView.setAlpha(Utils.FLOAT_EPSILON);
        this.mPendingAdditions.add(a0Var);
        return true;
    }

    public void animateAddImpl(RecyclerView.a0 a0Var) {
        View view = a0Var.itemView;
        ViewPropertyAnimator animate = view.animate();
        this.mAddAnimations.add(a0Var);
        animate.alpha(1.0f).setDuration(getAddDuration()).setListener(new e(a0Var, view, animate)).start();
    }

    @Override // androidx.recyclerview.widget.u
    public boolean animateChange(RecyclerView.a0 a0Var, RecyclerView.a0 a0Var2, int i2, int i3, int i4, int i5) {
        if (a0Var == a0Var2) {
            return animateMove(a0Var, i2, i3, i4, i5);
        }
        float translationX = a0Var.itemView.getTranslationX();
        float translationY = a0Var.itemView.getTranslationY();
        float alpha = a0Var.itemView.getAlpha();
        resetAnimation(a0Var);
        int i6 = (int) ((i4 - i2) - translationX);
        int i7 = (int) ((i5 - i3) - translationY);
        a0Var.itemView.setTranslationX(translationX);
        a0Var.itemView.setTranslationY(translationY);
        a0Var.itemView.setAlpha(alpha);
        if (a0Var2 != null) {
            resetAnimation(a0Var2);
            a0Var2.itemView.setTranslationX(-i6);
            a0Var2.itemView.setTranslationY(-i7);
            a0Var2.itemView.setAlpha(Utils.FLOAT_EPSILON);
        }
        this.mPendingChanges.add(new i(a0Var, a0Var2, i2, i3, i4, i5));
        return true;
    }

    public void animateChangeImpl(i iVar) {
        RecyclerView.a0 a0Var = iVar.a;
        View view = a0Var == null ? null : a0Var.itemView;
        RecyclerView.a0 a0Var2 = iVar.b;
        View view2 = a0Var2 != null ? a0Var2.itemView : null;
        if (view != null) {
            ViewPropertyAnimator duration = view.animate().setDuration(getChangeDuration());
            this.mChangeAnimations.add(iVar.a);
            duration.translationX(iVar.e - iVar.c);
            duration.translationY(iVar.f - iVar.d);
            duration.alpha(Utils.FLOAT_EPSILON).setListener(new g(iVar, duration, view)).start();
        }
        if (view2 != null) {
            ViewPropertyAnimator animate = view2.animate();
            this.mChangeAnimations.add(iVar.b);
            animate.translationX(Utils.FLOAT_EPSILON).translationY(Utils.FLOAT_EPSILON).setDuration(getChangeDuration()).alpha(1.0f).setListener(new h(iVar, animate, view2)).start();
        }
    }

    @Override // androidx.recyclerview.widget.u
    public boolean animateMove(RecyclerView.a0 a0Var, int i2, int i3, int i4, int i5) {
        View view = a0Var.itemView;
        int translationX = i2 + ((int) view.getTranslationX());
        int translationY = i3 + ((int) a0Var.itemView.getTranslationY());
        resetAnimation(a0Var);
        int i6 = i4 - translationX;
        int i7 = i5 - translationY;
        if (i6 == 0 && i7 == 0) {
            dispatchMoveFinished(a0Var);
            return false;
        }
        if (i6 != 0) {
            view.setTranslationX(-i6);
        }
        if (i7 != 0) {
            view.setTranslationY(-i7);
        }
        this.mPendingMoves.add(new j(a0Var, translationX, translationY, i4, i5));
        return true;
    }

    public void animateMoveImpl(RecyclerView.a0 a0Var, int i2, int i3, int i4, int i5) {
        View view = a0Var.itemView;
        int i6 = i4 - i2;
        int i7 = i5 - i3;
        if (i6 != 0) {
            view.animate().translationX(Utils.FLOAT_EPSILON);
        }
        if (i7 != 0) {
            view.animate().translationY(Utils.FLOAT_EPSILON);
        }
        ViewPropertyAnimator animate = view.animate();
        this.mMoveAnimations.add(a0Var);
        animate.setDuration(getMoveDuration()).setListener(new C0058f(a0Var, i6, view, i7, animate)).start();
    }

    @Override // androidx.recyclerview.widget.u
    public boolean animateRemove(RecyclerView.a0 a0Var) {
        resetAnimation(a0Var);
        this.mPendingRemovals.add(a0Var);
        return true;
    }

    @Override // androidx.recyclerview.widget.RecyclerView.l
    public boolean canReuseUpdatedViewHolder(RecyclerView.a0 a0Var, List<Object> list) {
        return !list.isEmpty() || super.canReuseUpdatedViewHolder(a0Var, list);
    }

    public void cancelAll(List<RecyclerView.a0> list) {
        for (int size = list.size() - 1; size >= 0; size--) {
            list.get(size).itemView.animate().cancel();
        }
    }

    public void dispatchFinishedWhenDone() {
        if (isRunning()) {
            return;
        }
        dispatchAnimationsFinished();
    }

    @Override // androidx.recyclerview.widget.RecyclerView.l
    public void endAnimation(RecyclerView.a0 a0Var) {
        View view = a0Var.itemView;
        view.animate().cancel();
        int size = this.mPendingMoves.size();
        while (true) {
            size--;
            if (size < 0) {
                break;
            } else if (this.mPendingMoves.get(size).a == a0Var) {
                view.setTranslationY(Utils.FLOAT_EPSILON);
                view.setTranslationX(Utils.FLOAT_EPSILON);
                dispatchMoveFinished(a0Var);
                this.mPendingMoves.remove(size);
            }
        }
        endChangeAnimation(this.mPendingChanges, a0Var);
        if (this.mPendingRemovals.remove(a0Var)) {
            view.setAlpha(1.0f);
            dispatchRemoveFinished(a0Var);
        }
        if (this.mPendingAdditions.remove(a0Var)) {
            view.setAlpha(1.0f);
            dispatchAddFinished(a0Var);
        }
        for (int size2 = this.mChangesList.size() - 1; size2 >= 0; size2--) {
            ArrayList<i> arrayList = this.mChangesList.get(size2);
            endChangeAnimation(arrayList, a0Var);
            if (arrayList.isEmpty()) {
                this.mChangesList.remove(size2);
            }
        }
        for (int size3 = this.mMovesList.size() - 1; size3 >= 0; size3--) {
            ArrayList<j> arrayList2 = this.mMovesList.get(size3);
            int size4 = arrayList2.size() - 1;
            while (true) {
                if (size4 < 0) {
                    break;
                } else if (arrayList2.get(size4).a == a0Var) {
                    view.setTranslationY(Utils.FLOAT_EPSILON);
                    view.setTranslationX(Utils.FLOAT_EPSILON);
                    dispatchMoveFinished(a0Var);
                    arrayList2.remove(size4);
                    if (arrayList2.isEmpty()) {
                        this.mMovesList.remove(size3);
                    }
                } else {
                    size4--;
                }
            }
        }
        for (int size5 = this.mAdditionsList.size() - 1; size5 >= 0; size5--) {
            ArrayList<RecyclerView.a0> arrayList3 = this.mAdditionsList.get(size5);
            if (arrayList3.remove(a0Var)) {
                view.setAlpha(1.0f);
                dispatchAddFinished(a0Var);
                if (arrayList3.isEmpty()) {
                    this.mAdditionsList.remove(size5);
                }
            }
        }
        this.mRemoveAnimations.remove(a0Var);
        this.mAddAnimations.remove(a0Var);
        this.mChangeAnimations.remove(a0Var);
        this.mMoveAnimations.remove(a0Var);
        dispatchFinishedWhenDone();
    }

    @Override // androidx.recyclerview.widget.RecyclerView.l
    public void endAnimations() {
        int size = this.mPendingMoves.size();
        while (true) {
            size--;
            if (size < 0) {
                break;
            }
            j jVar = this.mPendingMoves.get(size);
            View view = jVar.a.itemView;
            view.setTranslationY(Utils.FLOAT_EPSILON);
            view.setTranslationX(Utils.FLOAT_EPSILON);
            dispatchMoveFinished(jVar.a);
            this.mPendingMoves.remove(size);
        }
        for (int size2 = this.mPendingRemovals.size() - 1; size2 >= 0; size2--) {
            dispatchRemoveFinished(this.mPendingRemovals.get(size2));
            this.mPendingRemovals.remove(size2);
        }
        int size3 = this.mPendingAdditions.size();
        while (true) {
            size3--;
            if (size3 < 0) {
                break;
            }
            RecyclerView.a0 a0Var = this.mPendingAdditions.get(size3);
            a0Var.itemView.setAlpha(1.0f);
            dispatchAddFinished(a0Var);
            this.mPendingAdditions.remove(size3);
        }
        for (int size4 = this.mPendingChanges.size() - 1; size4 >= 0; size4--) {
            endChangeAnimationIfNecessary(this.mPendingChanges.get(size4));
        }
        this.mPendingChanges.clear();
        if (isRunning()) {
            for (int size5 = this.mMovesList.size() - 1; size5 >= 0; size5--) {
                ArrayList<j> arrayList = this.mMovesList.get(size5);
                for (int size6 = arrayList.size() - 1; size6 >= 0; size6--) {
                    j jVar2 = arrayList.get(size6);
                    View view2 = jVar2.a.itemView;
                    view2.setTranslationY(Utils.FLOAT_EPSILON);
                    view2.setTranslationX(Utils.FLOAT_EPSILON);
                    dispatchMoveFinished(jVar2.a);
                    arrayList.remove(size6);
                    if (arrayList.isEmpty()) {
                        this.mMovesList.remove(arrayList);
                    }
                }
            }
            for (int size7 = this.mAdditionsList.size() - 1; size7 >= 0; size7--) {
                ArrayList<RecyclerView.a0> arrayList2 = this.mAdditionsList.get(size7);
                for (int size8 = arrayList2.size() - 1; size8 >= 0; size8--) {
                    RecyclerView.a0 a0Var2 = arrayList2.get(size8);
                    a0Var2.itemView.setAlpha(1.0f);
                    dispatchAddFinished(a0Var2);
                    arrayList2.remove(size8);
                    if (arrayList2.isEmpty()) {
                        this.mAdditionsList.remove(arrayList2);
                    }
                }
            }
            for (int size9 = this.mChangesList.size() - 1; size9 >= 0; size9--) {
                ArrayList<i> arrayList3 = this.mChangesList.get(size9);
                for (int size10 = arrayList3.size() - 1; size10 >= 0; size10--) {
                    endChangeAnimationIfNecessary(arrayList3.get(size10));
                    if (arrayList3.isEmpty()) {
                        this.mChangesList.remove(arrayList3);
                    }
                }
            }
            cancelAll(this.mRemoveAnimations);
            cancelAll(this.mMoveAnimations);
            cancelAll(this.mAddAnimations);
            cancelAll(this.mChangeAnimations);
            dispatchAnimationsFinished();
        }
    }

    @Override // androidx.recyclerview.widget.RecyclerView.l
    public boolean isRunning() {
        return (this.mPendingAdditions.isEmpty() && this.mPendingChanges.isEmpty() && this.mPendingMoves.isEmpty() && this.mPendingRemovals.isEmpty() && this.mMoveAnimations.isEmpty() && this.mRemoveAnimations.isEmpty() && this.mAddAnimations.isEmpty() && this.mChangeAnimations.isEmpty() && this.mMovesList.isEmpty() && this.mAdditionsList.isEmpty() && this.mChangesList.isEmpty()) ? false : true;
    }

    @Override // androidx.recyclerview.widget.RecyclerView.l
    public void runPendingAnimations() {
        boolean z = !this.mPendingRemovals.isEmpty();
        boolean z2 = !this.mPendingMoves.isEmpty();
        boolean z3 = !this.mPendingChanges.isEmpty();
        boolean z4 = !this.mPendingAdditions.isEmpty();
        if (z || z2 || z4 || z3) {
            Iterator<RecyclerView.a0> it = this.mPendingRemovals.iterator();
            while (it.hasNext()) {
                animateRemoveImpl(it.next());
            }
            this.mPendingRemovals.clear();
            if (z2) {
                ArrayList<j> arrayList = new ArrayList<>();
                arrayList.addAll(this.mPendingMoves);
                this.mMovesList.add(arrayList);
                this.mPendingMoves.clear();
                a aVar = new a(arrayList);
                if (z) {
                    ei4.m0(arrayList.get(0).a.itemView, aVar, getRemoveDuration());
                } else {
                    aVar.run();
                }
            }
            if (z3) {
                ArrayList<i> arrayList2 = new ArrayList<>();
                arrayList2.addAll(this.mPendingChanges);
                this.mChangesList.add(arrayList2);
                this.mPendingChanges.clear();
                b bVar = new b(arrayList2);
                if (z) {
                    ei4.m0(arrayList2.get(0).a.itemView, bVar, getRemoveDuration());
                } else {
                    bVar.run();
                }
            }
            if (z4) {
                ArrayList<RecyclerView.a0> arrayList3 = new ArrayList<>();
                arrayList3.addAll(this.mPendingAdditions);
                this.mAdditionsList.add(arrayList3);
                this.mPendingAdditions.clear();
                c cVar = new c(arrayList3);
                if (!z && !z2 && !z3) {
                    cVar.run();
                } else {
                    ei4.m0(arrayList3.get(0).itemView, cVar, (z ? getRemoveDuration() : 0L) + Math.max(z2 ? getMoveDuration() : 0L, z3 ? getChangeDuration() : 0L));
                }
            }
        }
    }

    /* compiled from: DefaultItemAnimator.java */
    /* loaded from: classes.dex */
    public static class i {
        public RecyclerView.a0 a;
        public RecyclerView.a0 b;
        public int c;
        public int d;
        public int e;
        public int f;

        public i(RecyclerView.a0 a0Var, RecyclerView.a0 a0Var2) {
            this.a = a0Var;
            this.b = a0Var2;
        }

        public String toString() {
            return "ChangeInfo{oldHolder=" + this.a + ", newHolder=" + this.b + ", fromX=" + this.c + ", fromY=" + this.d + ", toX=" + this.e + ", toY=" + this.f + '}';
        }

        public i(RecyclerView.a0 a0Var, RecyclerView.a0 a0Var2, int i, int i2, int i3, int i4) {
            this(a0Var, a0Var2);
            this.c = i;
            this.d = i2;
            this.e = i3;
            this.f = i4;
        }
    }

    private boolean endChangeAnimationIfNecessary(i iVar, RecyclerView.a0 a0Var) {
        boolean z = false;
        if (iVar.b == a0Var) {
            iVar.b = null;
        } else if (iVar.a != a0Var) {
            return false;
        } else {
            iVar.a = null;
            z = true;
        }
        a0Var.itemView.setAlpha(1.0f);
        a0Var.itemView.setTranslationX(Utils.FLOAT_EPSILON);
        a0Var.itemView.setTranslationY(Utils.FLOAT_EPSILON);
        dispatchChangeFinished(a0Var, z);
        return true;
    }
}
