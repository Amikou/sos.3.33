package androidx.recyclerview.widget;

import androidx.recyclerview.widget.RecyclerView;
import androidx.recyclerview.widget.RecyclerView.a0;
import androidx.recyclerview.widget.c;
import androidx.recyclerview.widget.d;
import androidx.recyclerview.widget.g;
import java.util.List;

/* compiled from: ListAdapter.java */
/* loaded from: classes.dex */
public abstract class o<T, VH extends RecyclerView.a0> extends RecyclerView.Adapter<VH> {
    public final d<T> mDiffer;
    public final d.b<T> mListener;

    /* compiled from: ListAdapter.java */
    /* loaded from: classes.dex */
    public class a implements d.b<Object> {
        public a() {
        }

        @Override // androidx.recyclerview.widget.d.b
        public void a(List<Object> list, List<Object> list2) {
            o.this.onCurrentListChanged(list, list2);
        }
    }

    public o(g.f<T> fVar) {
        a aVar = new a();
        this.mListener = aVar;
        d<T> dVar = new d<>(new b(this), new c.a(fVar).a());
        this.mDiffer = dVar;
        dVar.a(aVar);
    }

    public List<T> getCurrentList() {
        return this.mDiffer.b();
    }

    public T getItem(int i) {
        return this.mDiffer.b().get(i);
    }

    @Override // androidx.recyclerview.widget.RecyclerView.Adapter
    public int getItemCount() {
        return this.mDiffer.b().size();
    }

    public void onCurrentListChanged(List<T> list, List<T> list2) {
    }

    public void submitList(List<T> list) {
        this.mDiffer.e(list);
    }

    public void submitList(List<T> list, Runnable runnable) {
        this.mDiffer.f(list, runnable);
    }
}
