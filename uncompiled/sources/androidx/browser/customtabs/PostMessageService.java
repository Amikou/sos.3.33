package androidx.browser.customtabs;

import android.app.Service;
import android.content.Intent;
import android.os.Bundle;
import android.os.IBinder;
import android.os.RemoteException;
import defpackage.mm1;

/* loaded from: classes.dex */
public class PostMessageService extends Service {
    public mm1.a a = new a(this);

    /* loaded from: classes.dex */
    public class a extends mm1.a {
        public a(PostMessageService postMessageService) {
        }

        @Override // defpackage.mm1
        public void V0(tl1 tl1Var, String str, Bundle bundle) throws RemoteException {
            tl1Var.j1(str, bundle);
        }

        @Override // defpackage.mm1
        public void t1(tl1 tl1Var, Bundle bundle) throws RemoteException {
            tl1Var.n1(bundle);
        }
    }

    @Override // android.app.Service
    public IBinder onBind(Intent intent) {
        return this.a;
    }
}
