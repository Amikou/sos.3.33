package androidx.browser.customtabs;

import android.app.PendingIntent;
import android.app.Service;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.os.IBinder;
import android.os.RemoteException;
import androidx.browser.customtabs.CustomTabsService;
import defpackage.ul1;
import java.util.List;
import java.util.NoSuchElementException;

/* loaded from: classes.dex */
public abstract class CustomTabsService extends Service {
    public final vo3<IBinder, IBinder.DeathRecipient> a = new vo3<>();
    public ul1.a f0 = new a();

    /* loaded from: classes.dex */
    public class a extends ul1.a {
        public a() {
        }

        /* JADX INFO: Access modifiers changed from: private */
        public /* synthetic */ void I1(vc0 vc0Var) {
            CustomTabsService.this.a(vc0Var);
        }

        @Override // defpackage.ul1
        public boolean D0(tl1 tl1Var, Uri uri) {
            return CustomTabsService.this.g(new vc0(tl1Var, null), uri);
        }

        @Override // defpackage.ul1
        public boolean H0(tl1 tl1Var, Uri uri, Bundle bundle, List<Bundle> list) {
            return CustomTabsService.this.c(new vc0(tl1Var, H1(bundle)), uri, bundle, list);
        }

        public final PendingIntent H1(Bundle bundle) {
            if (bundle == null) {
                return null;
            }
            PendingIntent pendingIntent = (PendingIntent) bundle.getParcelable("android.support.customtabs.extra.SESSION_ID");
            bundle.remove("android.support.customtabs.extra.SESSION_ID");
            return pendingIntent;
        }

        public final boolean J1(tl1 tl1Var, PendingIntent pendingIntent) {
            final vc0 vc0Var = new vc0(tl1Var, pendingIntent);
            try {
                IBinder.DeathRecipient deathRecipient = new IBinder.DeathRecipient() { // from class: sc0
                    @Override // android.os.IBinder.DeathRecipient
                    public final void binderDied() {
                        CustomTabsService.a.this.I1(vc0Var);
                    }
                };
                synchronized (CustomTabsService.this.a) {
                    tl1Var.asBinder().linkToDeath(deathRecipient, 0);
                    CustomTabsService.this.a.put(tl1Var.asBinder(), deathRecipient);
                }
                return CustomTabsService.this.d(vc0Var);
            } catch (RemoteException unused) {
                return false;
            }
        }

        @Override // defpackage.ul1
        public boolean M(tl1 tl1Var) {
            return J1(tl1Var, null);
        }

        @Override // defpackage.ul1
        public Bundle N(String str, Bundle bundle) {
            return CustomTabsService.this.b(str, bundle);
        }

        @Override // defpackage.ul1
        public boolean c0(long j) {
            return CustomTabsService.this.j(j);
        }

        @Override // defpackage.ul1
        public boolean d0(tl1 tl1Var, Bundle bundle) {
            return J1(tl1Var, H1(bundle));
        }

        @Override // defpackage.ul1
        public boolean g1(tl1 tl1Var, int i, Uri uri, Bundle bundle) {
            return CustomTabsService.this.i(new vc0(tl1Var, H1(bundle)), i, uri, bundle);
        }

        @Override // defpackage.ul1
        public boolean i(tl1 tl1Var, Uri uri, Bundle bundle) {
            return CustomTabsService.this.g(new vc0(tl1Var, H1(bundle)), uri);
        }

        @Override // defpackage.ul1
        public boolean s0(tl1 tl1Var, Uri uri, int i, Bundle bundle) {
            return CustomTabsService.this.f(new vc0(tl1Var, H1(bundle)), uri, i, bundle);
        }

        @Override // defpackage.ul1
        public int t(tl1 tl1Var, String str, Bundle bundle) {
            return CustomTabsService.this.e(new vc0(tl1Var, H1(bundle)), str, bundle);
        }

        @Override // defpackage.ul1
        public boolean z1(tl1 tl1Var, Bundle bundle) {
            return CustomTabsService.this.h(new vc0(tl1Var, H1(bundle)), bundle);
        }
    }

    public boolean a(vc0 vc0Var) {
        try {
            synchronized (this.a) {
                IBinder a2 = vc0Var.a();
                if (a2 == null) {
                    return false;
                }
                a2.unlinkToDeath(this.a.get(a2), 0);
                this.a.remove(a2);
                return true;
            }
        } catch (NoSuchElementException unused) {
            return false;
        }
    }

    public abstract Bundle b(String str, Bundle bundle);

    public abstract boolean c(vc0 vc0Var, Uri uri, Bundle bundle, List<Bundle> list);

    public abstract boolean d(vc0 vc0Var);

    public abstract int e(vc0 vc0Var, String str, Bundle bundle);

    public abstract boolean f(vc0 vc0Var, Uri uri, int i, Bundle bundle);

    public abstract boolean g(vc0 vc0Var, Uri uri);

    public abstract boolean h(vc0 vc0Var, Bundle bundle);

    public abstract boolean i(vc0 vc0Var, int i, Uri uri, Bundle bundle);

    public abstract boolean j(long j);

    @Override // android.app.Service
    public IBinder onBind(Intent intent) {
        return this.f0;
    }
}
