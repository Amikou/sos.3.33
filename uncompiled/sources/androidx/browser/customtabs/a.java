package androidx.browser.customtabs;

import android.app.PendingIntent;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.os.RemoteException;
import android.text.TextUtils;
import defpackage.tl1;

/* compiled from: CustomTabsClient.java */
/* loaded from: classes.dex */
public class a {
    public final ul1 a;
    public final ComponentName b;

    /* compiled from: CustomTabsClient.java */
    /* renamed from: androidx.browser.customtabs.a$a  reason: collision with other inner class name */
    /* loaded from: classes.dex */
    public class BinderC0017a extends tl1.a {
        public Handler a = new Handler(Looper.getMainLooper());
        public final /* synthetic */ qc0 b;

        /* compiled from: CustomTabsClient.java */
        /* renamed from: androidx.browser.customtabs.a$a$a  reason: collision with other inner class name */
        /* loaded from: classes.dex */
        public class RunnableC0018a implements Runnable {
            public final /* synthetic */ int a;
            public final /* synthetic */ Bundle f0;

            public RunnableC0018a(int i, Bundle bundle) {
                this.a = i;
                this.f0 = bundle;
            }

            @Override // java.lang.Runnable
            public void run() {
                BinderC0017a.this.b.d(this.a, this.f0);
            }
        }

        /* compiled from: CustomTabsClient.java */
        /* renamed from: androidx.browser.customtabs.a$a$b */
        /* loaded from: classes.dex */
        public class b implements Runnable {
            public final /* synthetic */ String a;
            public final /* synthetic */ Bundle f0;

            public b(String str, Bundle bundle) {
                this.a = str;
                this.f0 = bundle;
            }

            @Override // java.lang.Runnable
            public void run() {
                BinderC0017a.this.b.a(this.a, this.f0);
            }
        }

        /* compiled from: CustomTabsClient.java */
        /* renamed from: androidx.browser.customtabs.a$a$c */
        /* loaded from: classes.dex */
        public class c implements Runnable {
            public final /* synthetic */ Bundle a;

            public c(Bundle bundle) {
                this.a = bundle;
            }

            @Override // java.lang.Runnable
            public void run() {
                BinderC0017a.this.b.c(this.a);
            }
        }

        /* compiled from: CustomTabsClient.java */
        /* renamed from: androidx.browser.customtabs.a$a$d */
        /* loaded from: classes.dex */
        public class d implements Runnable {
            public final /* synthetic */ String a;
            public final /* synthetic */ Bundle f0;

            public d(String str, Bundle bundle) {
                this.a = str;
                this.f0 = bundle;
            }

            @Override // java.lang.Runnable
            public void run() {
                BinderC0017a.this.b.e(this.a, this.f0);
            }
        }

        /* compiled from: CustomTabsClient.java */
        /* renamed from: androidx.browser.customtabs.a$a$e */
        /* loaded from: classes.dex */
        public class e implements Runnable {
            public final /* synthetic */ int a;
            public final /* synthetic */ Uri f0;
            public final /* synthetic */ boolean g0;
            public final /* synthetic */ Bundle h0;

            public e(int i, Uri uri, boolean z, Bundle bundle) {
                this.a = i;
                this.f0 = uri;
                this.g0 = z;
                this.h0 = bundle;
            }

            @Override // java.lang.Runnable
            public void run() {
                BinderC0017a.this.b.f(this.a, this.f0, this.g0, this.h0);
            }
        }

        public BinderC0017a(a aVar, qc0 qc0Var) {
            this.b = qc0Var;
        }

        @Override // defpackage.tl1
        public void I0(String str, Bundle bundle) throws RemoteException {
            if (this.b == null) {
                return;
            }
            this.a.post(new b(str, bundle));
        }

        @Override // defpackage.tl1
        public Bundle L(String str, Bundle bundle) throws RemoteException {
            qc0 qc0Var = this.b;
            if (qc0Var == null) {
                return null;
            }
            return qc0Var.b(str, bundle);
        }

        @Override // defpackage.tl1
        public void W0(int i, Bundle bundle) {
            if (this.b == null) {
                return;
            }
            this.a.post(new RunnableC0018a(i, bundle));
        }

        @Override // defpackage.tl1
        public void j1(String str, Bundle bundle) throws RemoteException {
            if (this.b == null) {
                return;
            }
            this.a.post(new d(str, bundle));
        }

        @Override // defpackage.tl1
        public void n1(Bundle bundle) throws RemoteException {
            if (this.b == null) {
                return;
            }
            this.a.post(new c(bundle));
        }

        @Override // defpackage.tl1
        public void r1(int i, Uri uri, boolean z, Bundle bundle) throws RemoteException {
            if (this.b == null) {
                return;
            }
            this.a.post(new e(i, uri, z, bundle));
        }
    }

    public a(ul1 ul1Var, ComponentName componentName, Context context) {
        this.a = ul1Var;
        this.b = componentName;
    }

    public static boolean a(Context context, String str, tc0 tc0Var) {
        tc0Var.b(context.getApplicationContext());
        Intent intent = new Intent("android.support.customtabs.action.CustomTabsService");
        if (!TextUtils.isEmpty(str)) {
            intent.setPackage(str);
        }
        return context.bindService(intent, tc0Var, 33);
    }

    public final tl1.a b(qc0 qc0Var) {
        return new BinderC0017a(this, qc0Var);
    }

    public uc0 c(qc0 qc0Var) {
        return d(qc0Var, null);
    }

    public final uc0 d(qc0 qc0Var, PendingIntent pendingIntent) {
        boolean M;
        tl1.a b = b(qc0Var);
        try {
            if (pendingIntent != null) {
                Bundle bundle = new Bundle();
                bundle.putParcelable("android.support.customtabs.extra.SESSION_ID", pendingIntent);
                M = this.a.d0(b, bundle);
            } else {
                M = this.a.M(b);
            }
            if (M) {
                return new uc0(this.a, b, this.b, pendingIntent);
            }
            return null;
        } catch (RemoteException unused) {
            return null;
        }
    }

    public boolean e(long j) {
        try {
            return this.a.c0(j);
        } catch (RemoteException unused) {
            return false;
        }
    }
}
