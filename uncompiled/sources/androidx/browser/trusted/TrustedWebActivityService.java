package androidx.browser.trusted;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.Service;
import android.content.ComponentName;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.BitmapFactory;
import android.os.Binder;
import android.os.Build;
import android.os.Bundle;
import android.os.IBinder;
import android.os.Parcelable;
import androidx.browser.trusted.a;
import androidx.core.app.c;
import defpackage.wm1;
import java.util.Locale;

/* loaded from: classes.dex */
public abstract class TrustedWebActivityService extends Service {
    public NotificationManager a;
    public int f0 = -1;
    public final wm1.a g0 = new a();

    /* loaded from: classes.dex */
    public class a extends wm1.a {
        public a() {
        }

        @Override // defpackage.wm1
        public void N0(Bundle bundle) {
            b();
            a.b a = a.b.a(bundle);
            TrustedWebActivityService.this.e(a.a, a.b);
        }

        @Override // defpackage.wm1
        public Bundle P0(Bundle bundle) {
            b();
            a.d a = a.d.a(bundle);
            return new a.e(TrustedWebActivityService.this.j(a.a, a.b, a.c, a.d)).a();
        }

        @Override // defpackage.wm1
        public Bundle R(String str, Bundle bundle, IBinder iBinder) {
            b();
            return TrustedWebActivityService.this.f(str, bundle, bc4.a(iBinder));
        }

        public final void b() {
            TrustedWebActivityService trustedWebActivityService = TrustedWebActivityService.this;
            if (trustedWebActivityService.f0 == -1) {
                trustedWebActivityService.getPackageManager().getPackagesForUid(Binder.getCallingUid());
                TrustedWebActivityService.this.c().a();
                TrustedWebActivityService.this.getPackageManager();
            }
            if (TrustedWebActivityService.this.f0 != Binder.getCallingUid()) {
                throw new SecurityException("Caller is not verified as Trusted Web Activity provider.");
            }
        }

        @Override // defpackage.wm1
        public int t0() {
            b();
            return TrustedWebActivityService.this.i();
        }

        @Override // defpackage.wm1
        public Bundle v0() {
            b();
            return TrustedWebActivityService.this.h();
        }

        @Override // defpackage.wm1
        public Bundle x() {
            b();
            return new a.C0019a(TrustedWebActivityService.this.g()).a();
        }

        @Override // defpackage.wm1
        public Bundle z0(Bundle bundle) {
            b();
            return new a.e(TrustedWebActivityService.this.d(a.c.a(bundle).a)).a();
        }
    }

    public static String a(String str) {
        return str.toLowerCase(Locale.ROOT).replace(' ', '_') + "_channel_id";
    }

    public final void b() {
        if (this.a == null) {
            throw new IllegalStateException("TrustedWebActivityService has not been properly initialized. Did onCreate() call super.onCreate()?");
        }
    }

    public abstract e74 c();

    public boolean d(String str) {
        b();
        if (c.d(this).a()) {
            if (Build.VERSION.SDK_INT < 26) {
                return true;
            }
            return bh2.b(this.a, a(str));
        }
        return false;
    }

    public void e(String str, int i) {
        b();
        this.a.cancel(str, i);
    }

    public Bundle f(String str, Bundle bundle, bc4 bc4Var) {
        return null;
    }

    public Parcelable[] g() {
        b();
        if (Build.VERSION.SDK_INT >= 23) {
            return ah2.a(this.a);
        }
        throw new IllegalStateException("onGetActiveNotifications cannot be called pre-M.");
    }

    public Bundle h() {
        int i = i();
        Bundle bundle = new Bundle();
        if (i == -1) {
            return bundle;
        }
        bundle.putParcelable("android.support.customtabs.trusted.SMALL_ICON_BITMAP", BitmapFactory.decodeResource(getResources(), i));
        return bundle;
    }

    public int i() {
        try {
            Bundle bundle = getPackageManager().getServiceInfo(new ComponentName(this, getClass()), 128).metaData;
            if (bundle == null) {
                return -1;
            }
            return bundle.getInt("android.support.customtabs.trusted.SMALL_ICON", -1);
        } catch (PackageManager.NameNotFoundException unused) {
            return -1;
        }
    }

    public boolean j(String str, int i, Notification notification, String str2) {
        b();
        if (c.d(this).a()) {
            if (Build.VERSION.SDK_INT >= 26) {
                String a2 = a(str2);
                notification = bh2.a(this, this.a, notification, a2, str2);
                if (!bh2.b(this.a, a2)) {
                    return false;
                }
            }
            this.a.notify(str, i, notification);
            return true;
        }
        return false;
    }

    @Override // android.app.Service
    public final IBinder onBind(Intent intent) {
        return this.g0;
    }

    @Override // android.app.Service
    public void onCreate() {
        super.onCreate();
        this.a = (NotificationManager) getSystemService("notification");
    }

    @Override // android.app.Service
    public final boolean onUnbind(Intent intent) {
        this.f0 = -1;
        return super.onUnbind(intent);
    }
}
