package androidx.biometric;

import android.content.DialogInterface;
import android.os.Handler;
import android.os.Looper;
import androidx.biometric.BiometricPrompt;
import androidx.biometric.a;
import androidx.fragment.app.FragmentActivity;
import androidx.lifecycle.LiveData;
import java.lang.ref.WeakReference;
import java.util.concurrent.Executor;

/* compiled from: BiometricViewModel.java */
/* loaded from: classes.dex */
public class e extends dj4 {
    public Executor a;
    public BiometricPrompt.a b;
    public BiometricPrompt.d c;
    public BiometricPrompt.c d;
    public androidx.biometric.a e;
    public uv f;
    public DialogInterface.OnClickListener g;
    public CharSequence h;
    public boolean j;
    public boolean k;
    public boolean l;
    public boolean m;
    public boolean n;
    public gb2<BiometricPrompt.b> o;
    public gb2<np> p;
    public gb2<CharSequence> q;
    public gb2<Boolean> r;
    public gb2<Boolean> s;
    public gb2<Boolean> u;
    public gb2<Integer> w;
    public gb2<CharSequence> x;
    public int i = 0;
    public boolean t = true;
    public int v = 0;

    /* compiled from: BiometricViewModel.java */
    /* loaded from: classes.dex */
    public class a extends BiometricPrompt.a {
        public a(e eVar) {
        }
    }

    /* compiled from: BiometricViewModel.java */
    /* loaded from: classes.dex */
    public static final class b extends a.d {
        public final WeakReference<e> a;

        public b(e eVar) {
            this.a = new WeakReference<>(eVar);
        }

        @Override // androidx.biometric.a.d
        public void a(int i, CharSequence charSequence) {
            if (this.a.get() == null || this.a.get().w() || !this.a.get().u()) {
                return;
            }
            this.a.get().E(new np(i, charSequence));
        }

        @Override // androidx.biometric.a.d
        public void b() {
            if (this.a.get() == null || !this.a.get().u()) {
                return;
            }
            this.a.get().F(true);
        }

        @Override // androidx.biometric.a.d
        public void c(CharSequence charSequence) {
            if (this.a.get() != null) {
                this.a.get().G(charSequence);
            }
        }

        @Override // androidx.biometric.a.d
        public void d(BiometricPrompt.b bVar) {
            if (this.a.get() == null || !this.a.get().u()) {
                return;
            }
            if (bVar.a() == -1) {
                bVar = new BiometricPrompt.b(bVar.b(), this.a.get().o());
            }
            this.a.get().H(bVar);
        }
    }

    /* compiled from: BiometricViewModel.java */
    /* loaded from: classes.dex */
    public static class c implements Executor {
        public final Handler a = new Handler(Looper.getMainLooper());

        @Override // java.util.concurrent.Executor
        public void execute(Runnable runnable) {
            this.a.post(runnable);
        }
    }

    /* compiled from: BiometricViewModel.java */
    /* loaded from: classes.dex */
    public static class d implements DialogInterface.OnClickListener {
        public final WeakReference<e> a;

        public d(e eVar) {
            this.a = new WeakReference<>(eVar);
        }

        @Override // android.content.DialogInterface.OnClickListener
        public void onClick(DialogInterface dialogInterface, int i) {
            if (this.a.get() != null) {
                this.a.get().W(true);
            }
        }
    }

    public static <T> void a0(gb2<T> gb2Var, T t) {
        if (Thread.currentThread() == Looper.getMainLooper().getThread()) {
            gb2Var.setValue(t);
        } else {
            gb2Var.postValue(t);
        }
    }

    public boolean A() {
        return this.n;
    }

    public LiveData<Boolean> B() {
        if (this.s == null) {
            this.s = new gb2<>();
        }
        return this.s;
    }

    public boolean C() {
        return this.j;
    }

    public void D() {
        this.b = null;
    }

    public void E(np npVar) {
        if (this.p == null) {
            this.p = new gb2<>();
        }
        a0(this.p, npVar);
    }

    public void F(boolean z) {
        if (this.r == null) {
            this.r = new gb2<>();
        }
        a0(this.r, Boolean.valueOf(z));
    }

    public void G(CharSequence charSequence) {
        if (this.q == null) {
            this.q = new gb2<>();
        }
        a0(this.q, charSequence);
    }

    public void H(BiometricPrompt.b bVar) {
        if (this.o == null) {
            this.o = new gb2<>();
        }
        a0(this.o, bVar);
    }

    public void I(boolean z) {
        this.k = z;
    }

    public void J(int i) {
        this.i = i;
    }

    public void K(FragmentActivity fragmentActivity) {
        new WeakReference(fragmentActivity);
    }

    public void L(BiometricPrompt.a aVar) {
        this.b = aVar;
    }

    public void M(Executor executor) {
        this.a = executor;
    }

    public void N(boolean z) {
        this.l = z;
    }

    public void O(BiometricPrompt.c cVar) {
        this.d = cVar;
    }

    public void P(boolean z) {
        this.m = z;
    }

    public void Q(boolean z) {
        if (this.u == null) {
            this.u = new gb2<>();
        }
        a0(this.u, Boolean.valueOf(z));
    }

    public void R(boolean z) {
        this.t = z;
    }

    public void S(CharSequence charSequence) {
        if (this.x == null) {
            this.x = new gb2<>();
        }
        a0(this.x, charSequence);
    }

    public void T(int i) {
        this.v = i;
    }

    public void U(int i) {
        if (this.w == null) {
            this.w = new gb2<>();
        }
        a0(this.w, Integer.valueOf(i));
    }

    public void V(boolean z) {
        this.n = z;
    }

    public void W(boolean z) {
        if (this.s == null) {
            this.s = new gb2<>();
        }
        a0(this.s, Boolean.valueOf(z));
    }

    public void X(CharSequence charSequence) {
        this.h = charSequence;
    }

    public void Y(BiometricPrompt.d dVar) {
        this.c = dVar;
    }

    public void Z(boolean z) {
        this.j = z;
    }

    public int a() {
        BiometricPrompt.d dVar = this.c;
        if (dVar != null) {
            return androidx.biometric.b.b(dVar, this.d);
        }
        return 0;
    }

    public androidx.biometric.a b() {
        if (this.e == null) {
            this.e = new androidx.biometric.a(new b(this));
        }
        return this.e;
    }

    public gb2<np> c() {
        if (this.p == null) {
            this.p = new gb2<>();
        }
        return this.p;
    }

    public LiveData<CharSequence> d() {
        if (this.q == null) {
            this.q = new gb2<>();
        }
        return this.q;
    }

    public LiveData<BiometricPrompt.b> e() {
        if (this.o == null) {
            this.o = new gb2<>();
        }
        return this.o;
    }

    public int f() {
        return this.i;
    }

    public uv g() {
        if (this.f == null) {
            this.f = new uv();
        }
        return this.f;
    }

    public BiometricPrompt.a h() {
        if (this.b == null) {
            this.b = new a(this);
        }
        return this.b;
    }

    public Executor i() {
        Executor executor = this.a;
        return executor != null ? executor : new c();
    }

    public BiometricPrompt.c j() {
        return this.d;
    }

    public CharSequence k() {
        BiometricPrompt.d dVar = this.c;
        if (dVar != null) {
            return dVar.b();
        }
        return null;
    }

    public LiveData<CharSequence> l() {
        if (this.x == null) {
            this.x = new gb2<>();
        }
        return this.x;
    }

    public int m() {
        return this.v;
    }

    public LiveData<Integer> n() {
        if (this.w == null) {
            this.w = new gb2<>();
        }
        return this.w;
    }

    public int o() {
        int a2 = a();
        return (!androidx.biometric.b.d(a2) || androidx.biometric.b.c(a2)) ? -1 : 2;
    }

    public DialogInterface.OnClickListener p() {
        if (this.g == null) {
            this.g = new d(this);
        }
        return this.g;
    }

    public CharSequence q() {
        CharSequence charSequence = this.h;
        if (charSequence != null) {
            return charSequence;
        }
        BiometricPrompt.d dVar = this.c;
        if (dVar != null) {
            return dVar.c();
        }
        return null;
    }

    public CharSequence r() {
        BiometricPrompt.d dVar = this.c;
        if (dVar != null) {
            return dVar.d();
        }
        return null;
    }

    public CharSequence s() {
        BiometricPrompt.d dVar = this.c;
        if (dVar != null) {
            return dVar.e();
        }
        return null;
    }

    public LiveData<Boolean> t() {
        if (this.r == null) {
            this.r = new gb2<>();
        }
        return this.r;
    }

    public boolean u() {
        return this.k;
    }

    public boolean v() {
        BiometricPrompt.d dVar = this.c;
        return dVar == null || dVar.f();
    }

    public boolean w() {
        return this.l;
    }

    public boolean x() {
        return this.m;
    }

    public LiveData<Boolean> y() {
        if (this.u == null) {
            this.u = new gb2<>();
        }
        return this.u;
    }

    public boolean z() {
        return this.t;
    }
}
