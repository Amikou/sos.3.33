package androidx.biometric;

import android.content.Context;
import android.hardware.biometrics.BiometricManager;
import android.hardware.biometrics.BiometricPrompt;
import android.os.Build;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

/* compiled from: BiometricManager.java */
/* loaded from: classes.dex */
public class d {
    public final InterfaceC0016d a;
    public final BiometricManager b;
    public final y41 c;

    /* compiled from: BiometricManager.java */
    /* loaded from: classes.dex */
    public static class a {
        public static int a(BiometricManager biometricManager) {
            return biometricManager.canAuthenticate();
        }

        public static BiometricManager b(Context context) {
            return (BiometricManager) context.getSystemService(BiometricManager.class);
        }

        public static Method c() {
            try {
                return BiometricManager.class.getMethod("canAuthenticate", BiometricPrompt.CryptoObject.class);
            } catch (NoSuchMethodException unused) {
                return null;
            }
        }
    }

    /* compiled from: BiometricManager.java */
    /* loaded from: classes.dex */
    public static class b {
        public static int a(BiometricManager biometricManager, int i) {
            return biometricManager.canAuthenticate(i);
        }
    }

    /* compiled from: BiometricManager.java */
    /* loaded from: classes.dex */
    public static class c implements InterfaceC0016d {
        public final Context a;

        public c(Context context) {
            this.a = context.getApplicationContext();
        }

        @Override // androidx.biometric.d.InterfaceC0016d
        public BiometricManager a() {
            return a.b(this.a);
        }

        @Override // androidx.biometric.d.InterfaceC0016d
        public boolean b() {
            return rx1.a(this.a) != null;
        }

        @Override // androidx.biometric.d.InterfaceC0016d
        public boolean c() {
            return vo2.a(this.a);
        }

        @Override // androidx.biometric.d.InterfaceC0016d
        public boolean d() {
            return rx1.b(this.a);
        }

        @Override // androidx.biometric.d.InterfaceC0016d
        public boolean e() {
            return wm0.a(this.a, Build.MODEL);
        }

        @Override // androidx.biometric.d.InterfaceC0016d
        public y41 f() {
            return y41.b(this.a);
        }
    }

    /* compiled from: BiometricManager.java */
    /* renamed from: androidx.biometric.d$d  reason: collision with other inner class name */
    /* loaded from: classes.dex */
    public interface InterfaceC0016d {
        BiometricManager a();

        boolean b();

        boolean c();

        boolean d();

        boolean e();

        y41 f();
    }

    public d(InterfaceC0016d interfaceC0016d) {
        this.a = interfaceC0016d;
        int i = Build.VERSION.SDK_INT;
        this.b = i >= 29 ? interfaceC0016d.a() : null;
        this.c = i <= 29 ? interfaceC0016d.f() : null;
    }

    public static d g(Context context) {
        return new d(new c(context));
    }

    public int a(int i) {
        if (Build.VERSION.SDK_INT >= 30) {
            BiometricManager biometricManager = this.b;
            if (biometricManager == null) {
                return 1;
            }
            return b.a(biometricManager, i);
        }
        return b(i);
    }

    public final int b(int i) {
        if (androidx.biometric.b.e(i)) {
            if (i != 0 && this.a.b()) {
                if (androidx.biometric.b.c(i)) {
                    return this.a.d() ? 0 : 11;
                }
                int i2 = Build.VERSION.SDK_INT;
                if (i2 == 29) {
                    if (androidx.biometric.b.f(i)) {
                        return f();
                    }
                    return e();
                } else if (i2 == 28) {
                    if (this.a.c()) {
                        return d();
                    }
                    return 12;
                } else {
                    return c();
                }
            }
            return 12;
        }
        return -2;
    }

    public final int c() {
        y41 y41Var = this.c;
        if (y41Var == null) {
            return 1;
        }
        if (y41Var.e()) {
            return !this.c.d() ? 11 : 0;
        }
        return 12;
    }

    public final int d() {
        if (this.a.d()) {
            return c() == 0 ? 0 : -1;
        }
        return c();
    }

    public final int e() {
        BiometricPrompt.CryptoObject d;
        Method c2 = a.c();
        if (c2 != null && (d = f.d(f.a())) != null) {
            try {
                Object invoke = Build.VERSION.SDK_INT == 29 ? c2.invoke(this.b, d) : null;
                if (invoke instanceof Integer) {
                    return ((Integer) invoke).intValue();
                }
            } catch (IllegalAccessException | IllegalArgumentException | InvocationTargetException unused) {
            }
        }
        int f = f();
        return (this.a.e() || f != 0) ? f : d();
    }

    public final int f() {
        BiometricManager biometricManager = this.b;
        if (biometricManager == null) {
            return 1;
        }
        return a.a(biometricManager);
    }
}
