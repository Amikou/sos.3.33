package androidx.biometric;

import android.hardware.biometrics.BiometricPrompt;
import android.os.Build;
import androidx.biometric.BiometricPrompt;
import defpackage.y41;

/* compiled from: AuthenticationCallbackProvider.java */
/* loaded from: classes.dex */
public class a {
    public BiometricPrompt.AuthenticationCallback a;
    public y41.b b;
    public final d c;

    /* compiled from: AuthenticationCallbackProvider.java */
    /* renamed from: androidx.biometric.a$a  reason: collision with other inner class name */
    /* loaded from: classes.dex */
    public class C0013a extends y41.b {
        public C0013a() {
        }

        @Override // defpackage.y41.b
        public void a(int i, CharSequence charSequence) {
            a.this.c.a(i, charSequence);
        }

        @Override // defpackage.y41.b
        public void b() {
            a.this.c.b();
        }

        @Override // defpackage.y41.b
        public void c(int i, CharSequence charSequence) {
            a.this.c.c(charSequence);
        }

        @Override // defpackage.y41.b
        public void d(y41.c cVar) {
            a.this.c.d(new BiometricPrompt.b(cVar != null ? f.c(cVar.a()) : null, 2));
        }
    }

    /* compiled from: AuthenticationCallbackProvider.java */
    /* loaded from: classes.dex */
    public static class b {

        /* compiled from: AuthenticationCallbackProvider.java */
        /* renamed from: androidx.biometric.a$b$a  reason: collision with other inner class name */
        /* loaded from: classes.dex */
        public class C0014a extends BiometricPrompt.AuthenticationCallback {
            public final /* synthetic */ d a;

            public C0014a(d dVar) {
                this.a = dVar;
            }

            @Override // android.hardware.biometrics.BiometricPrompt.AuthenticationCallback
            public void onAuthenticationError(int i, CharSequence charSequence) {
                this.a.a(i, charSequence);
            }

            @Override // android.hardware.biometrics.BiometricPrompt.AuthenticationCallback
            public void onAuthenticationFailed() {
                this.a.b();
            }

            @Override // android.hardware.biometrics.BiometricPrompt.AuthenticationCallback
            public void onAuthenticationHelp(int i, CharSequence charSequence) {
            }

            @Override // android.hardware.biometrics.BiometricPrompt.AuthenticationCallback
            public void onAuthenticationSucceeded(BiometricPrompt.AuthenticationResult authenticationResult) {
                BiometricPrompt.c b = authenticationResult != null ? f.b(b.b(authenticationResult)) : null;
                int i = Build.VERSION.SDK_INT;
                int i2 = -1;
                if (i >= 30) {
                    if (authenticationResult != null) {
                        i2 = c.a(authenticationResult);
                    }
                } else if (i != 29) {
                    i2 = 2;
                }
                this.a.d(new BiometricPrompt.b(b, i2));
            }
        }

        public static BiometricPrompt.AuthenticationCallback a(d dVar) {
            return new C0014a(dVar);
        }

        public static BiometricPrompt.CryptoObject b(BiometricPrompt.AuthenticationResult authenticationResult) {
            return authenticationResult.getCryptoObject();
        }
    }

    /* compiled from: AuthenticationCallbackProvider.java */
    /* loaded from: classes.dex */
    public static class c {
        public static int a(BiometricPrompt.AuthenticationResult authenticationResult) {
            return authenticationResult.getAuthenticationType();
        }
    }

    /* compiled from: AuthenticationCallbackProvider.java */
    /* loaded from: classes.dex */
    public static class d {
        public void a(int i, CharSequence charSequence) {
            throw null;
        }

        public void b() {
            throw null;
        }

        public void c(CharSequence charSequence) {
            throw null;
        }

        public void d(BiometricPrompt.b bVar) {
            throw null;
        }
    }

    public a(d dVar) {
        this.c = dVar;
    }

    public BiometricPrompt.AuthenticationCallback a() {
        if (this.a == null) {
            this.a = b.a(this.c);
        }
        return this.a;
    }

    public y41.b b() {
        if (this.b == null) {
            this.b = new C0013a();
        }
        return this.b;
    }
}
