package androidx.biometric;

import android.app.KeyguardManager;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.hardware.biometrics.BiometricPrompt;
import android.os.Build;
import android.os.Bundle;
import android.os.CancellationSignal;
import android.os.Handler;
import android.os.Looper;
import android.text.TextUtils;
import androidx.biometric.BiometricPrompt;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentActivity;
import androidx.fragment.app.FragmentManager;
import androidx.media3.common.PlaybackException;
import com.fasterxml.jackson.core.util.MinimalPrettyPrinter;
import java.lang.ref.WeakReference;
import java.util.concurrent.Executor;

/* compiled from: BiometricFragment.java */
/* loaded from: classes.dex */
public class c extends Fragment {
    public Handler a = new Handler(Looper.getMainLooper());
    public androidx.biometric.e f0;

    /* compiled from: BiometricFragment.java */
    /* loaded from: classes.dex */
    public class a implements Runnable {
        public final /* synthetic */ int a;
        public final /* synthetic */ CharSequence f0;

        public a(int i, CharSequence charSequence) {
            this.a = i;
            this.f0 = charSequence;
        }

        @Override // java.lang.Runnable
        public void run() {
            c.this.f0.h().a(this.a, this.f0);
        }
    }

    /* compiled from: BiometricFragment.java */
    /* loaded from: classes.dex */
    public class b implements Runnable {
        public b() {
        }

        @Override // java.lang.Runnable
        public void run() {
            c.this.f0.h().b();
        }
    }

    /* compiled from: BiometricFragment.java */
    /* renamed from: androidx.biometric.c$c  reason: collision with other inner class name */
    /* loaded from: classes.dex */
    public class C0015c implements tl2<BiometricPrompt.b> {
        public C0015c() {
        }

        @Override // defpackage.tl2
        /* renamed from: a */
        public void onChanged(BiometricPrompt.b bVar) {
            if (bVar != null) {
                c.this.y(bVar);
                c.this.f0.H(null);
            }
        }
    }

    /* compiled from: BiometricFragment.java */
    /* loaded from: classes.dex */
    public class d implements tl2<np> {
        public d() {
        }

        @Override // defpackage.tl2
        /* renamed from: a */
        public void onChanged(np npVar) {
            if (npVar != null) {
                c.this.v(npVar.b(), npVar.c());
                c.this.f0.E(null);
            }
        }
    }

    /* compiled from: BiometricFragment.java */
    /* loaded from: classes.dex */
    public class e implements tl2<CharSequence> {
        public e() {
        }

        @Override // defpackage.tl2
        /* renamed from: a */
        public void onChanged(CharSequence charSequence) {
            if (charSequence != null) {
                c.this.x(charSequence);
                c.this.f0.E(null);
            }
        }
    }

    /* compiled from: BiometricFragment.java */
    /* loaded from: classes.dex */
    public class f implements tl2<Boolean> {
        public f() {
        }

        @Override // defpackage.tl2
        /* renamed from: a */
        public void onChanged(Boolean bool) {
            if (bool.booleanValue()) {
                c.this.w();
                c.this.f0.F(false);
            }
        }
    }

    /* compiled from: BiometricFragment.java */
    /* loaded from: classes.dex */
    public class g implements tl2<Boolean> {
        public g() {
        }

        @Override // defpackage.tl2
        /* renamed from: a */
        public void onChanged(Boolean bool) {
            if (bool.booleanValue()) {
                if (c.this.r()) {
                    c.this.A();
                } else {
                    c.this.z();
                }
                c.this.f0.W(false);
            }
        }
    }

    /* compiled from: BiometricFragment.java */
    /* loaded from: classes.dex */
    public class h implements tl2<Boolean> {
        public h() {
        }

        @Override // defpackage.tl2
        /* renamed from: a */
        public void onChanged(Boolean bool) {
            if (bool.booleanValue()) {
                c.this.h(1);
                c.this.k();
                c.this.f0.Q(false);
            }
        }
    }

    /* compiled from: BiometricFragment.java */
    /* loaded from: classes.dex */
    public class i implements Runnable {
        public i() {
        }

        @Override // java.lang.Runnable
        public void run() {
            c.this.f0.R(false);
        }
    }

    /* compiled from: BiometricFragment.java */
    /* loaded from: classes.dex */
    public class j implements Runnable {
        public final /* synthetic */ int a;
        public final /* synthetic */ CharSequence f0;

        public j(int i, CharSequence charSequence) {
            this.a = i;
            this.f0 = charSequence;
        }

        @Override // java.lang.Runnable
        public void run() {
            c.this.B(this.a, this.f0);
        }
    }

    /* compiled from: BiometricFragment.java */
    /* loaded from: classes.dex */
    public class k implements Runnable {
        public final /* synthetic */ BiometricPrompt.b a;

        public k(BiometricPrompt.b bVar) {
            this.a = bVar;
        }

        @Override // java.lang.Runnable
        public void run() {
            c.this.f0.h().c(this.a);
        }
    }

    /* compiled from: BiometricFragment.java */
    /* loaded from: classes.dex */
    public static class l {
        public static Intent a(KeyguardManager keyguardManager, CharSequence charSequence, CharSequence charSequence2) {
            return keyguardManager.createConfirmDeviceCredentialIntent(charSequence, charSequence2);
        }
    }

    /* compiled from: BiometricFragment.java */
    /* loaded from: classes.dex */
    public static class m {
        public static void a(android.hardware.biometrics.BiometricPrompt biometricPrompt, BiometricPrompt.CryptoObject cryptoObject, CancellationSignal cancellationSignal, Executor executor, BiometricPrompt.AuthenticationCallback authenticationCallback) {
            biometricPrompt.authenticate(cryptoObject, cancellationSignal, executor, authenticationCallback);
        }

        public static void b(android.hardware.biometrics.BiometricPrompt biometricPrompt, CancellationSignal cancellationSignal, Executor executor, BiometricPrompt.AuthenticationCallback authenticationCallback) {
            biometricPrompt.authenticate(cancellationSignal, executor, authenticationCallback);
        }

        public static android.hardware.biometrics.BiometricPrompt c(BiometricPrompt.Builder builder) {
            return builder.build();
        }

        public static BiometricPrompt.Builder d(Context context) {
            return new BiometricPrompt.Builder(context);
        }

        public static void e(BiometricPrompt.Builder builder, CharSequence charSequence) {
            builder.setDescription(charSequence);
        }

        public static void f(BiometricPrompt.Builder builder, CharSequence charSequence, Executor executor, DialogInterface.OnClickListener onClickListener) {
            builder.setNegativeButton(charSequence, executor, onClickListener);
        }

        public static void g(BiometricPrompt.Builder builder, CharSequence charSequence) {
            builder.setSubtitle(charSequence);
        }

        public static void h(BiometricPrompt.Builder builder, CharSequence charSequence) {
            builder.setTitle(charSequence);
        }
    }

    /* compiled from: BiometricFragment.java */
    /* loaded from: classes.dex */
    public static class n {
        public static void a(BiometricPrompt.Builder builder, boolean z) {
            builder.setConfirmationRequired(z);
        }

        public static void b(BiometricPrompt.Builder builder, boolean z) {
            builder.setDeviceCredentialAllowed(z);
        }
    }

    /* compiled from: BiometricFragment.java */
    /* loaded from: classes.dex */
    public static class o {
        public static void a(BiometricPrompt.Builder builder, int i) {
            builder.setAllowedAuthenticators(i);
        }
    }

    /* compiled from: BiometricFragment.java */
    /* loaded from: classes.dex */
    public static class p implements Executor {
        public final Handler a = new Handler(Looper.getMainLooper());

        @Override // java.util.concurrent.Executor
        public void execute(Runnable runnable) {
            this.a.post(runnable);
        }
    }

    /* compiled from: BiometricFragment.java */
    /* loaded from: classes.dex */
    public static class q implements Runnable {
        public final WeakReference<c> a;

        public q(c cVar) {
            this.a = new WeakReference<>(cVar);
        }

        @Override // java.lang.Runnable
        public void run() {
            if (this.a.get() != null) {
                this.a.get().J();
            }
        }
    }

    /* compiled from: BiometricFragment.java */
    /* loaded from: classes.dex */
    public static class r implements Runnable {
        public final WeakReference<androidx.biometric.e> a;

        public r(androidx.biometric.e eVar) {
            this.a = new WeakReference<>(eVar);
        }

        @Override // java.lang.Runnable
        public void run() {
            if (this.a.get() != null) {
                this.a.get().P(false);
            }
        }
    }

    /* compiled from: BiometricFragment.java */
    /* loaded from: classes.dex */
    public static class s implements Runnable {
        public final WeakReference<androidx.biometric.e> a;

        public s(androidx.biometric.e eVar) {
            this.a = new WeakReference<>(eVar);
        }

        @Override // java.lang.Runnable
        public void run() {
            if (this.a.get() != null) {
                this.a.get().V(false);
            }
        }
    }

    public static int i(y41 y41Var) {
        if (y41Var.e()) {
            return !y41Var.d() ? 11 : 0;
        }
        return 12;
    }

    public static c u() {
        return new c();
    }

    public void A() {
        if (Build.VERSION.SDK_INT < 21) {
            return;
        }
        t();
    }

    public void B(int i2, CharSequence charSequence) {
        C(i2, charSequence);
        k();
    }

    public final void C(int i2, CharSequence charSequence) {
        if (!this.f0.w() && this.f0.u()) {
            this.f0.I(false);
            this.f0.i().execute(new a(i2, charSequence));
        }
    }

    public final void D() {
        if (this.f0.u()) {
            this.f0.i().execute(new b());
        }
    }

    public final void E(BiometricPrompt.b bVar) {
        F(bVar);
        k();
    }

    public final void F(BiometricPrompt.b bVar) {
        if (this.f0.u()) {
            this.f0.I(false);
            this.f0.i().execute(new k(bVar));
        }
    }

    public final void G() {
        BiometricPrompt.Builder d2 = m.d(requireContext().getApplicationContext());
        CharSequence s2 = this.f0.s();
        CharSequence r2 = this.f0.r();
        CharSequence k2 = this.f0.k();
        if (s2 != null) {
            m.h(d2, s2);
        }
        if (r2 != null) {
            m.g(d2, r2);
        }
        if (k2 != null) {
            m.e(d2, k2);
        }
        CharSequence q2 = this.f0.q();
        if (!TextUtils.isEmpty(q2)) {
            m.f(d2, q2, this.f0.i(), this.f0.p());
        }
        int i2 = Build.VERSION.SDK_INT;
        if (i2 >= 29) {
            n.a(d2, this.f0.v());
        }
        int a2 = this.f0.a();
        if (i2 >= 30) {
            o.a(d2, a2);
        } else if (i2 >= 29) {
            n.b(d2, androidx.biometric.b.c(a2));
        }
        f(m.c(d2), getContext());
    }

    public final void H() {
        Context applicationContext = requireContext().getApplicationContext();
        y41 b2 = y41.b(applicationContext);
        int i2 = i(b2);
        if (i2 != 0) {
            B(i2, ew0.a(applicationContext, i2));
        } else if (Build.VERSION.SDK_INT >= 19 && isAdded()) {
            this.f0.R(true);
            if (!wm0.f(applicationContext, Build.MODEL)) {
                this.a.postDelayed(new i(), 500L);
                x41.y().u(getParentFragmentManager(), "androidx.biometric.FingerprintDialogFragment");
            }
            this.f0.J(0);
            g(b2, applicationContext);
        }
    }

    public final void I(CharSequence charSequence) {
        if (charSequence == null) {
            charSequence = getString(l13.default_error_msg);
        }
        this.f0.U(2);
        this.f0.S(charSequence);
    }

    public void J() {
        if (this.f0.C() || getContext() == null) {
            return;
        }
        this.f0.Z(true);
        this.f0.I(true);
        if (s()) {
            H();
        } else {
            G();
        }
    }

    public void e(BiometricPrompt.d dVar, BiometricPrompt.c cVar) {
        FragmentActivity activity = getActivity();
        if (activity == null) {
            return;
        }
        this.f0.Y(dVar);
        int b2 = androidx.biometric.b.b(dVar, cVar);
        int i2 = Build.VERSION.SDK_INT;
        if (i2 >= 23 && i2 < 30 && b2 == 15 && cVar == null) {
            this.f0.O(androidx.biometric.f.a());
        } else {
            this.f0.O(cVar);
        }
        if (r()) {
            this.f0.X(getString(l13.confirm_device_credential_password));
        } else {
            this.f0.X(null);
        }
        if (i2 >= 21 && r() && androidx.biometric.d.g(activity).a(255) != 0) {
            this.f0.I(true);
            t();
        } else if (this.f0.x()) {
            this.a.postDelayed(new q(this), 600L);
        } else {
            J();
        }
    }

    public void f(android.hardware.biometrics.BiometricPrompt biometricPrompt, Context context) {
        BiometricPrompt.CryptoObject d2 = androidx.biometric.f.d(this.f0.j());
        CancellationSignal b2 = this.f0.g().b();
        p pVar = new p();
        BiometricPrompt.AuthenticationCallback a2 = this.f0.b().a();
        try {
            if (d2 == null) {
                m.b(biometricPrompt, b2, pVar, a2);
            } else {
                m.a(biometricPrompt, d2, b2, pVar, a2);
            }
        } catch (NullPointerException unused) {
            B(1, context != null ? context.getString(l13.default_error_msg) : "");
        }
    }

    public void g(y41 y41Var, Context context) {
        try {
            y41Var.a(androidx.biometric.f.e(this.f0.j()), 0, this.f0.g().c(), this.f0.b().b(), null);
        } catch (NullPointerException unused) {
            B(1, ew0.a(context, 1));
        }
    }

    public void h(int i2) {
        if (i2 == 3 || !this.f0.A()) {
            if (s()) {
                this.f0.J(i2);
                if (i2 == 1) {
                    C(10, ew0.a(getContext(), 10));
                }
            }
            this.f0.g().a();
        }
    }

    public final void j() {
        FragmentActivity activity = getActivity();
        if (activity == null) {
            return;
        }
        androidx.biometric.e eVar = (androidx.biometric.e) new androidx.lifecycle.l(getActivity()).a(androidx.biometric.e.class);
        this.f0 = eVar;
        eVar.K(activity);
        this.f0.e().observe(this, new C0015c());
        this.f0.c().observe(this, new d());
        this.f0.d().observe(this, new e());
        this.f0.t().observe(this, new f());
        this.f0.B().observe(this, new g());
        this.f0.y().observe(this, new h());
    }

    public void k() {
        this.f0.Z(false);
        l();
        if (!this.f0.w() && isAdded()) {
            getParentFragmentManager().n().r(this).k();
        }
        Context context = getContext();
        if (context == null || !wm0.e(context, Build.MODEL)) {
            return;
        }
        this.f0.P(true);
        this.a.postDelayed(new r(this.f0), 600L);
    }

    public final void l() {
        this.f0.Z(false);
        if (isAdded()) {
            FragmentManager parentFragmentManager = getParentFragmentManager();
            x41 x41Var = (x41) parentFragmentManager.k0("androidx.biometric.FingerprintDialogFragment");
            if (x41Var != null) {
                if (x41Var.isAdded()) {
                    x41Var.i();
                } else {
                    parentFragmentManager.n().r(x41Var).k();
                }
            }
        }
    }

    public final int m() {
        Context context = getContext();
        if (context == null || !wm0.f(context, Build.MODEL)) {
            return PlaybackException.ERROR_CODE_IO_UNSPECIFIED;
        }
        return 0;
    }

    public final void n(int i2) {
        if (i2 == -1) {
            E(new BiometricPrompt.b(null, 1));
        } else {
            B(10, getString(l13.generic_error_user_canceled));
        }
    }

    public final boolean o() {
        FragmentActivity activity = getActivity();
        return activity != null && activity.isChangingConfigurations();
    }

    @Override // androidx.fragment.app.Fragment
    public void onActivityResult(int i2, int i3, Intent intent) {
        super.onActivityResult(i2, i3, intent);
        if (i2 == 1) {
            this.f0.N(false);
            n(i3);
        }
    }

    @Override // androidx.fragment.app.Fragment
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        j();
    }

    @Override // androidx.fragment.app.Fragment
    public void onStart() {
        super.onStart();
        if (Build.VERSION.SDK_INT == 29 && androidx.biometric.b.c(this.f0.a())) {
            this.f0.V(true);
            this.a.postDelayed(new s(this.f0), 250L);
        }
    }

    @Override // androidx.fragment.app.Fragment
    public void onStop() {
        super.onStop();
        if (Build.VERSION.SDK_INT >= 29 || this.f0.w() || o()) {
            return;
        }
        h(0);
    }

    public final boolean p() {
        FragmentActivity activity = getActivity();
        return (activity == null || this.f0.j() == null || !wm0.g(activity, Build.MANUFACTURER, Build.MODEL)) ? false : true;
    }

    public final boolean q() {
        return Build.VERSION.SDK_INT == 28 && !vo2.a(getContext());
    }

    public boolean r() {
        return Build.VERSION.SDK_INT <= 28 && androidx.biometric.b.c(this.f0.a());
    }

    public final boolean s() {
        return Build.VERSION.SDK_INT < 28 || p() || q();
    }

    public final void t() {
        FragmentActivity activity = getActivity();
        if (activity == null) {
            return;
        }
        KeyguardManager a2 = rx1.a(activity);
        if (a2 == null) {
            B(12, getString(l13.generic_error_no_keyguard));
            return;
        }
        CharSequence s2 = this.f0.s();
        CharSequence r2 = this.f0.r();
        CharSequence k2 = this.f0.k();
        if (r2 == null) {
            r2 = k2;
        }
        Intent a3 = l.a(a2, s2, r2);
        if (a3 == null) {
            B(14, getString(l13.generic_error_no_device_credential));
            return;
        }
        this.f0.N(true);
        if (s()) {
            l();
        }
        a3.setFlags(134742016);
        startActivityForResult(a3, 1);
    }

    public void v(int i2, CharSequence charSequence) {
        if (!ew0.b(i2)) {
            i2 = 8;
        }
        Context context = getContext();
        int i3 = Build.VERSION.SDK_INT;
        if (i3 >= 21 && i3 < 29 && ew0.c(i2) && context != null && rx1.b(context) && androidx.biometric.b.c(this.f0.a())) {
            t();
        } else if (!s()) {
            if (charSequence == null) {
                charSequence = getString(l13.default_error_msg) + MinimalPrettyPrinter.DEFAULT_ROOT_VALUE_SEPARATOR + i2;
            }
            B(i2, charSequence);
        } else {
            if (charSequence == null) {
                charSequence = ew0.a(getContext(), i2);
            }
            if (i2 == 5) {
                int f2 = this.f0.f();
                if (f2 == 0 || f2 == 3) {
                    C(i2, charSequence);
                }
                k();
                return;
            }
            if (this.f0.z()) {
                B(i2, charSequence);
            } else {
                I(charSequence);
                this.a.postDelayed(new j(i2, charSequence), m());
            }
            this.f0.R(true);
        }
    }

    public void w() {
        if (s()) {
            I(getString(l13.fingerprint_not_recognized));
        }
        D();
    }

    public void x(CharSequence charSequence) {
        if (s()) {
            I(charSequence);
        }
    }

    public void y(BiometricPrompt.b bVar) {
        E(bVar);
    }

    public void z() {
        CharSequence q2 = this.f0.q();
        if (q2 == null) {
            q2 = getString(l13.default_error_msg);
        }
        B(13, q2);
        h(2);
    }
}
