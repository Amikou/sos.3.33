package androidx.biometric;

import android.os.Build;
import androidx.biometric.BiometricPrompt;

/* compiled from: AuthenticatorUtils.java */
/* loaded from: classes.dex */
public class b {
    public static String a(int i) {
        return i != 15 ? i != 255 ? i != 32768 ? i != 32783 ? i != 33023 ? String.valueOf(i) : "BIOMETRIC_WEAK | DEVICE_CREDENTIAL" : "BIOMETRIC_STRONG | DEVICE_CREDENTIAL" : "DEVICE_CREDENTIAL" : "BIOMETRIC_WEAK" : "BIOMETRIC_STRONG";
    }

    public static int b(BiometricPrompt.d dVar, BiometricPrompt.c cVar) {
        if (dVar.a() != 0) {
            return dVar.a();
        }
        int i = cVar != null ? 15 : 255;
        return dVar.g() ? 32768 | i : i;
    }

    public static boolean c(int i) {
        return (i & 32768) != 0;
    }

    public static boolean d(int i) {
        return (i & 32767) != 0;
    }

    public static boolean e(int i) {
        if (i == 15 || i == 255) {
            return true;
        }
        if (i == 32768) {
            return Build.VERSION.SDK_INT >= 30;
        } else if (i != 32783) {
            return i == 33023 || i == 0;
        } else {
            int i2 = Build.VERSION.SDK_INT;
            return i2 < 28 || i2 > 29;
        }
    }

    public static boolean f(int i) {
        return (i & 255) == 255;
    }
}
