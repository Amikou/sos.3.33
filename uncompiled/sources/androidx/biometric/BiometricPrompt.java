package androidx.biometric;

import android.os.Build;
import android.security.identity.IdentityCredential;
import android.text.TextUtils;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentActivity;
import androidx.fragment.app.FragmentManager;
import androidx.lifecycle.Lifecycle;
import androidx.lifecycle.h;
import androidx.lifecycle.l;
import java.lang.ref.WeakReference;
import java.security.Signature;
import java.util.concurrent.Executor;
import javax.crypto.Cipher;
import javax.crypto.Mac;

/* loaded from: classes.dex */
public class BiometricPrompt {
    public FragmentManager a;

    /* loaded from: classes.dex */
    public static class ResetCallbackObserver implements qz1 {
        public final WeakReference<e> a;

        public ResetCallbackObserver(e eVar) {
            this.a = new WeakReference<>(eVar);
        }

        @h(Lifecycle.Event.ON_DESTROY)
        public void resetCallback() {
            if (this.a.get() != null) {
                this.a.get().D();
            }
        }
    }

    /* loaded from: classes.dex */
    public static abstract class a {
        public void a(int i, CharSequence charSequence) {
        }

        public void b() {
        }

        public void c(b bVar) {
        }
    }

    /* loaded from: classes.dex */
    public static class b {
        public final c a;
        public final int b;

        public b(c cVar, int i) {
            this.a = cVar;
            this.b = i;
        }

        public int a() {
            return this.b;
        }

        public c b() {
            return this.a;
        }
    }

    /* loaded from: classes.dex */
    public static class d {
        public final CharSequence a;
        public final CharSequence b;
        public final CharSequence c;
        public final CharSequence d;
        public final boolean e;
        public final boolean f;
        public final int g;

        /* loaded from: classes.dex */
        public static class a {
            public CharSequence a = null;
            public CharSequence b = null;
            public CharSequence c = null;
            public CharSequence d = null;
            public boolean e = true;
            public boolean f = false;
            public int g = 0;

            public d a() {
                boolean z;
                if (!TextUtils.isEmpty(this.a)) {
                    if (androidx.biometric.b.e(this.g)) {
                        int i = this.g;
                        if (i != 0) {
                            z = androidx.biometric.b.c(i);
                        } else {
                            z = this.f;
                        }
                        if (TextUtils.isEmpty(this.d) && !z) {
                            throw new IllegalArgumentException("Negative text must be set and non-empty.");
                        }
                        if (!TextUtils.isEmpty(this.d) && z) {
                            throw new IllegalArgumentException("Negative text must not be set if device credential authentication is allowed.");
                        }
                        return new d(this.a, this.b, this.c, this.d, this.e, this.f, this.g);
                    }
                    throw new IllegalArgumentException("Authenticator combination is unsupported on API " + Build.VERSION.SDK_INT + ": " + androidx.biometric.b.a(this.g));
                }
                throw new IllegalArgumentException("Title must be set and non-empty.");
            }

            public a b(int i) {
                this.g = i;
                return this;
            }

            public a c(boolean z) {
                this.e = z;
                return this;
            }

            public a d(CharSequence charSequence) {
                this.b = charSequence;
                return this;
            }

            public a e(CharSequence charSequence) {
                this.a = charSequence;
                return this;
            }
        }

        public d(CharSequence charSequence, CharSequence charSequence2, CharSequence charSequence3, CharSequence charSequence4, boolean z, boolean z2, int i) {
            this.a = charSequence;
            this.b = charSequence2;
            this.c = charSequence3;
            this.d = charSequence4;
            this.e = z;
            this.f = z2;
            this.g = i;
        }

        public int a() {
            return this.g;
        }

        public CharSequence b() {
            return this.c;
        }

        public CharSequence c() {
            CharSequence charSequence = this.d;
            return charSequence != null ? charSequence : "";
        }

        public CharSequence d() {
            return this.b;
        }

        public CharSequence e() {
            return this.a;
        }

        public boolean f() {
            return this.e;
        }

        @Deprecated
        public boolean g() {
            return this.f;
        }
    }

    public BiometricPrompt(FragmentActivity fragmentActivity, a aVar) {
        if (fragmentActivity == null) {
            throw new IllegalArgumentException("FragmentActivity must not be null.");
        }
        if (aVar != null) {
            h(fragmentActivity.getSupportFragmentManager(), g(fragmentActivity), null, aVar);
            return;
        }
        throw new IllegalArgumentException("AuthenticationCallback must not be null.");
    }

    public static void a(Fragment fragment, e eVar) {
        if (eVar != null) {
            fragment.getLifecycle().a(new ResetCallbackObserver(eVar));
        }
    }

    public static androidx.biometric.c e(FragmentManager fragmentManager) {
        return (androidx.biometric.c) fragmentManager.k0("androidx.biometric.BiometricFragment");
    }

    public static androidx.biometric.c f(FragmentManager fragmentManager) {
        androidx.biometric.c e = e(fragmentManager);
        if (e == null) {
            androidx.biometric.c u = androidx.biometric.c.u();
            fragmentManager.n().e(u, "androidx.biometric.BiometricFragment").k();
            fragmentManager.g0();
            return u;
        }
        return e;
    }

    public static e g(FragmentActivity fragmentActivity) {
        if (fragmentActivity != null) {
            return (e) new l(fragmentActivity).a(e.class);
        }
        return null;
    }

    public void b(d dVar) {
        if (dVar != null) {
            c(dVar, null);
            return;
        }
        throw new IllegalArgumentException("PromptInfo cannot be null.");
    }

    public final void c(d dVar, c cVar) {
        FragmentManager fragmentManager = this.a;
        if (fragmentManager == null || fragmentManager.M0()) {
            return;
        }
        f(this.a).e(dVar, cVar);
    }

    public void d() {
        androidx.biometric.c e;
        FragmentManager fragmentManager = this.a;
        if (fragmentManager == null || (e = e(fragmentManager)) == null) {
            return;
        }
        e.h(3);
    }

    public final void h(FragmentManager fragmentManager, e eVar, Executor executor, a aVar) {
        this.a = fragmentManager;
        if (eVar != null) {
            if (executor != null) {
                eVar.M(executor);
            }
            eVar.L(aVar);
        }
    }

    /* loaded from: classes.dex */
    public static class c {
        public final Signature a;
        public final Cipher b;
        public final Mac c;
        public final IdentityCredential d;

        public c(Signature signature) {
            this.a = signature;
            this.b = null;
            this.c = null;
            this.d = null;
        }

        public Cipher a() {
            return this.b;
        }

        public IdentityCredential b() {
            return this.d;
        }

        public Mac c() {
            return this.c;
        }

        public Signature d() {
            return this.a;
        }

        public c(Cipher cipher) {
            this.a = null;
            this.b = cipher;
            this.c = null;
            this.d = null;
        }

        public c(Mac mac) {
            this.a = null;
            this.b = null;
            this.c = mac;
            this.d = null;
        }

        public c(IdentityCredential identityCredential) {
            this.a = null;
            this.b = null;
            this.c = null;
            this.d = identityCredential;
        }
    }

    public BiometricPrompt(Fragment fragment, a aVar) {
        if (fragment == null) {
            throw new IllegalArgumentException("Fragment must not be null.");
        }
        if (aVar != null) {
            FragmentActivity activity = fragment.getActivity();
            FragmentManager childFragmentManager = fragment.getChildFragmentManager();
            e g = g(activity);
            a(fragment, g);
            h(childFragmentManager, g, null, aVar);
            return;
        }
        throw new IllegalArgumentException("AuthenticationCallback must not be null.");
    }
}
