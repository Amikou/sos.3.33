package androidx.biometric;

import android.annotation.SuppressLint;
import android.hardware.biometrics.BiometricPrompt;
import android.os.Build;
import android.security.identity.IdentityCredential;
import android.security.keystore.KeyGenParameterSpec;
import androidx.biometric.BiometricPrompt;
import defpackage.y41;
import java.io.IOException;
import java.security.InvalidAlgorithmParameterException;
import java.security.InvalidKeyException;
import java.security.KeyStore;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.NoSuchProviderException;
import java.security.Signature;
import java.security.UnrecoverableKeyException;
import java.security.cert.CertificateException;
import javax.crypto.Cipher;
import javax.crypto.KeyGenerator;
import javax.crypto.Mac;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.SecretKey;

/* compiled from: CryptoObjectUtils.java */
/* loaded from: classes.dex */
public class f {

    /* compiled from: CryptoObjectUtils.java */
    /* loaded from: classes.dex */
    public static class a {
        public static KeyGenParameterSpec a(KeyGenParameterSpec.Builder builder) {
            return builder.build();
        }

        public static KeyGenParameterSpec.Builder b(String str, int i) {
            return new KeyGenParameterSpec.Builder(str, i);
        }

        public static void c(KeyGenerator keyGenerator, KeyGenParameterSpec keyGenParameterSpec) throws InvalidAlgorithmParameterException {
            keyGenerator.init(keyGenParameterSpec);
        }

        public static void d(KeyGenParameterSpec.Builder builder) {
            builder.setBlockModes("CBC");
        }

        public static void e(KeyGenParameterSpec.Builder builder) {
            builder.setEncryptionPaddings("PKCS7Padding");
        }
    }

    /* compiled from: CryptoObjectUtils.java */
    /* loaded from: classes.dex */
    public static class b {
        public static BiometricPrompt.CryptoObject a(Signature signature) {
            return new BiometricPrompt.CryptoObject(signature);
        }

        public static BiometricPrompt.CryptoObject b(Cipher cipher) {
            return new BiometricPrompt.CryptoObject(cipher);
        }

        public static BiometricPrompt.CryptoObject c(Mac mac) {
            return new BiometricPrompt.CryptoObject(mac);
        }

        public static Cipher d(BiometricPrompt.CryptoObject cryptoObject) {
            return cryptoObject.getCipher();
        }

        public static Mac e(BiometricPrompt.CryptoObject cryptoObject) {
            return cryptoObject.getMac();
        }

        public static Signature f(BiometricPrompt.CryptoObject cryptoObject) {
            return cryptoObject.getSignature();
        }
    }

    /* compiled from: CryptoObjectUtils.java */
    /* loaded from: classes.dex */
    public static class c {
        public static BiometricPrompt.CryptoObject a(IdentityCredential identityCredential) {
            return new BiometricPrompt.CryptoObject(identityCredential);
        }

        public static IdentityCredential b(BiometricPrompt.CryptoObject cryptoObject) {
            return cryptoObject.getIdentityCredential();
        }
    }

    @SuppressLint({"TrulyRandom"})
    public static BiometricPrompt.c a() {
        try {
            KeyStore keyStore = KeyStore.getInstance("AndroidKeyStore");
            keyStore.load(null);
            KeyGenParameterSpec.Builder b2 = a.b("androidxBiometric", 3);
            a.d(b2);
            a.e(b2);
            KeyGenerator keyGenerator = KeyGenerator.getInstance("AES", "AndroidKeyStore");
            a.c(keyGenerator, a.a(b2));
            keyGenerator.generateKey();
            Cipher cipher = Cipher.getInstance("AES/CBC/PKCS7Padding");
            cipher.init(1, (SecretKey) keyStore.getKey("androidxBiometric", null));
            return new BiometricPrompt.c(cipher);
        } catch (IOException | InvalidAlgorithmParameterException | InvalidKeyException | KeyStoreException | NoSuchAlgorithmException | NoSuchProviderException | UnrecoverableKeyException | CertificateException | NoSuchPaddingException unused) {
            return null;
        }
    }

    public static BiometricPrompt.c b(BiometricPrompt.CryptoObject cryptoObject) {
        IdentityCredential b2;
        if (cryptoObject == null) {
            return null;
        }
        Cipher d = b.d(cryptoObject);
        if (d != null) {
            return new BiometricPrompt.c(d);
        }
        Signature f = b.f(cryptoObject);
        if (f != null) {
            return new BiometricPrompt.c(f);
        }
        Mac e = b.e(cryptoObject);
        if (e != null) {
            return new BiometricPrompt.c(e);
        }
        if (Build.VERSION.SDK_INT < 30 || (b2 = c.b(cryptoObject)) == null) {
            return null;
        }
        return new BiometricPrompt.c(b2);
    }

    public static BiometricPrompt.c c(y41.d dVar) {
        if (dVar == null) {
            return null;
        }
        Cipher a2 = dVar.a();
        if (a2 != null) {
            return new BiometricPrompt.c(a2);
        }
        Signature c2 = dVar.c();
        if (c2 != null) {
            return new BiometricPrompt.c(c2);
        }
        Mac b2 = dVar.b();
        if (b2 != null) {
            return new BiometricPrompt.c(b2);
        }
        return null;
    }

    public static BiometricPrompt.CryptoObject d(BiometricPrompt.c cVar) {
        IdentityCredential b2;
        if (cVar == null) {
            return null;
        }
        Cipher a2 = cVar.a();
        if (a2 != null) {
            return b.b(a2);
        }
        Signature d = cVar.d();
        if (d != null) {
            return b.a(d);
        }
        Mac c2 = cVar.c();
        if (c2 != null) {
            return b.c(c2);
        }
        if (Build.VERSION.SDK_INT < 30 || (b2 = cVar.b()) == null) {
            return null;
        }
        return c.a(b2);
    }

    public static y41.d e(BiometricPrompt.c cVar) {
        if (cVar == null) {
            return null;
        }
        Cipher a2 = cVar.a();
        if (a2 != null) {
            return new y41.d(a2);
        }
        Signature d = cVar.d();
        if (d != null) {
            return new y41.d(d);
        }
        Mac c2 = cVar.c();
        if (c2 != null) {
            return new y41.d(c2);
        }
        if (Build.VERSION.SDK_INT >= 30) {
            cVar.b();
        }
        return null;
    }
}
