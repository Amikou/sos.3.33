package androidx.work;

import androidx.work.ListenableWorker;
import kotlin.coroutines.jvm.internal.SuspendLambda;

/* compiled from: CoroutineWorker.kt */
@kotlin.coroutines.jvm.internal.a(c = "androidx.work.CoroutineWorker$startWork$1", f = "CoroutineWorker.kt", l = {68}, m = "invokeSuspend")
/* loaded from: classes.dex */
public final class CoroutineWorker$startWork$1 extends SuspendLambda implements hd1<c90, q70<? super te4>, Object> {
    public int label;
    public final /* synthetic */ CoroutineWorker this$0;

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public CoroutineWorker$startWork$1(CoroutineWorker coroutineWorker, q70<? super CoroutineWorker$startWork$1> q70Var) {
        super(2, q70Var);
        this.this$0 = coroutineWorker;
    }

    @Override // kotlin.coroutines.jvm.internal.BaseContinuationImpl
    public final q70<te4> create(Object obj, q70<?> q70Var) {
        return new CoroutineWorker$startWork$1(this.this$0, q70Var);
    }

    @Override // defpackage.hd1
    public final Object invoke(c90 c90Var, q70<? super te4> q70Var) {
        return ((CoroutineWorker$startWork$1) create(c90Var, q70Var)).invokeSuspend(te4.a);
    }

    @Override // kotlin.coroutines.jvm.internal.BaseContinuationImpl
    public final Object invokeSuspend(Object obj) {
        Object d = gs1.d();
        int i = this.label;
        try {
            if (i == 0) {
                o83.b(obj);
                CoroutineWorker coroutineWorker = this.this$0;
                this.label = 1;
                obj = coroutineWorker.r(this);
                if (obj == d) {
                    return d;
                }
            } else if (i != 1) {
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            } else {
                o83.b(obj);
            }
            this.this$0.v().p((ListenableWorker.a) obj);
        } catch (Throwable th) {
            this.this$0.v().q(th);
        }
        return te4.a;
    }
}
