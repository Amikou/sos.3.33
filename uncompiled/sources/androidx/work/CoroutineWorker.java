package androidx.work;

import android.content.Context;
import androidx.work.ListenableWorker;
import defpackage.st1;
import kotlinx.coroutines.CoroutineDispatcher;

/* compiled from: CoroutineWorker.kt */
/* loaded from: classes.dex */
public abstract class CoroutineWorker extends ListenableWorker {
    public final q30 j0;
    public final wm3<ListenableWorker.a> k0;
    public final CoroutineDispatcher l0;

    /* compiled from: CoroutineWorker.kt */
    /* loaded from: classes.dex */
    public static final class a implements Runnable {
        public a() {
        }

        @Override // java.lang.Runnable
        public final void run() {
            if (CoroutineWorker.this.v().isCancelled()) {
                st1.a.a(CoroutineWorker.this.w(), null, 1, null);
            }
        }
    }

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public CoroutineWorker(Context context, WorkerParameters workerParameters) {
        super(context, workerParameters);
        q30 b;
        fs1.f(context, "appContext");
        fs1.f(workerParameters, "params");
        b = zt1.b(null, 1, null);
        this.j0 = b;
        wm3<ListenableWorker.a> t = wm3.t();
        fs1.e(t, "create()");
        this.k0 = t;
        t.d(new a(), h().c());
        this.l0 = tp0.a();
    }

    public static /* synthetic */ Object u(CoroutineWorker coroutineWorker, q70 q70Var) {
        throw new IllegalStateException("Not implemented");
    }

    @Override // androidx.work.ListenableWorker
    public final l02<s81> d() {
        q30 b;
        b = zt1.b(null, 1, null);
        c90 a2 = d90.a(s().plus(b));
        JobListenableFuture jobListenableFuture = new JobListenableFuture(b, null, 2, null);
        as.b(a2, null, null, new CoroutineWorker$getForegroundInfoAsync$1(jobListenableFuture, this, null), 3, null);
        return jobListenableFuture;
    }

    @Override // androidx.work.ListenableWorker
    public final void m() {
        super.m();
        this.k0.cancel(false);
    }

    @Override // androidx.work.ListenableWorker
    public final l02<ListenableWorker.a> p() {
        as.b(d90.a(s().plus(this.j0)), null, null, new CoroutineWorker$startWork$1(this, null), 3, null);
        return this.k0;
    }

    public abstract Object r(q70<? super ListenableWorker.a> q70Var);

    public CoroutineDispatcher s() {
        return this.l0;
    }

    public Object t(q70<? super s81> q70Var) {
        return u(this, q70Var);
    }

    public final wm3<ListenableWorker.a> v() {
        return this.k0;
    }

    public final q30 w() {
        return this.j0;
    }
}
