package androidx.work;

import android.os.Build;
import java.util.concurrent.Executor;
import java.util.concurrent.Executors;
import java.util.concurrent.ThreadFactory;
import java.util.concurrent.atomic.AtomicInteger;

/* compiled from: Configuration.java */
/* loaded from: classes.dex */
public final class a {
    public final Executor a;
    public final Executor b;
    public final br4 c;
    public final zq1 d;
    public final ba3 e;
    public final qq1 f;
    public final String g;
    public final int h;
    public final int i;
    public final int j;
    public final int k;

    /* compiled from: Configuration.java */
    /* renamed from: androidx.work.a$a  reason: collision with other inner class name */
    /* loaded from: classes.dex */
    public class ThreadFactoryC0069a implements ThreadFactory {
        public final AtomicInteger a = new AtomicInteger(0);
        public final /* synthetic */ boolean f0;

        public ThreadFactoryC0069a(a aVar, boolean z) {
            this.f0 = z;
        }

        @Override // java.util.concurrent.ThreadFactory
        public Thread newThread(Runnable runnable) {
            String str = this.f0 ? "WM.task-" : "androidx.work-";
            return new Thread(runnable, str + this.a.incrementAndGet());
        }
    }

    /* compiled from: Configuration.java */
    /* loaded from: classes.dex */
    public static final class b {
        public Executor a;
        public br4 b;
        public zq1 c;
        public Executor d;
        public ba3 e;
        public qq1 f;
        public String g;
        public int h = 4;
        public int i = 0;
        public int j = Integer.MAX_VALUE;
        public int k = 20;

        public a a() {
            return new a(this);
        }
    }

    /* compiled from: Configuration.java */
    /* loaded from: classes.dex */
    public interface c {
        a a();
    }

    public a(b bVar) {
        Executor executor = bVar.a;
        if (executor == null) {
            this.a = a(false);
        } else {
            this.a = executor;
        }
        Executor executor2 = bVar.d;
        if (executor2 == null) {
            this.b = a(true);
        } else {
            this.b = executor2;
        }
        br4 br4Var = bVar.b;
        if (br4Var == null) {
            this.c = br4.c();
        } else {
            this.c = br4Var;
        }
        zq1 zq1Var = bVar.c;
        if (zq1Var == null) {
            this.d = zq1.c();
        } else {
            this.d = zq1Var;
        }
        ba3 ba3Var = bVar.e;
        if (ba3Var == null) {
            this.e = new lk0();
        } else {
            this.e = ba3Var;
        }
        this.h = bVar.h;
        this.i = bVar.i;
        this.j = bVar.j;
        this.k = bVar.k;
        this.f = bVar.f;
        this.g = bVar.g;
    }

    public final Executor a(boolean z) {
        return Executors.newFixedThreadPool(Math.max(2, Math.min(Runtime.getRuntime().availableProcessors() - 1, 4)), b(z));
    }

    public final ThreadFactory b(boolean z) {
        return new ThreadFactoryC0069a(this, z);
    }

    public String c() {
        return this.g;
    }

    public qq1 d() {
        return this.f;
    }

    public Executor e() {
        return this.a;
    }

    public zq1 f() {
        return this.d;
    }

    public int g() {
        return this.j;
    }

    public int h() {
        if (Build.VERSION.SDK_INT == 23) {
            return this.k / 2;
        }
        return this.k;
    }

    public int i() {
        return this.i;
    }

    public int j() {
        return this.h;
    }

    public ba3 k() {
        return this.e;
    }

    public Executor l() {
        return this.b;
    }

    public br4 m() {
        return this.c;
    }
}
