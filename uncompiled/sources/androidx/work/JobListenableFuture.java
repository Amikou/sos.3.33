package androidx.work;

import java.util.concurrent.CancellationException;
import java.util.concurrent.Executor;
import java.util.concurrent.TimeUnit;
import kotlin.jvm.internal.Lambda;

/* compiled from: ListenableFuture.kt */
/* loaded from: classes.dex */
public final class JobListenableFuture<R> implements l02<R> {
    public final st1 a;
    public final wm3<R> f0;

    /* compiled from: ListenableFuture.kt */
    /* renamed from: androidx.work.JobListenableFuture$1  reason: invalid class name */
    /* loaded from: classes.dex */
    public static final class AnonymousClass1 extends Lambda implements tc1<Throwable, te4> {
        public final /* synthetic */ JobListenableFuture<R> this$0;

        /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
        public AnonymousClass1(JobListenableFuture<R> jobListenableFuture) {
            super(1);
            this.this$0 = jobListenableFuture;
        }

        @Override // defpackage.tc1
        public /* bridge */ /* synthetic */ te4 invoke(Throwable th) {
            invoke2(th);
            return te4.a;
        }

        /* renamed from: invoke  reason: avoid collision after fix types in other method */
        public final void invoke2(Throwable th) {
            if (th == null) {
                if (!this.this$0.f0.isDone()) {
                    throw new IllegalArgumentException("Failed requirement.".toString());
                }
            } else if (th instanceof CancellationException) {
                this.this$0.f0.cancel(true);
            } else {
                wm3 wm3Var = this.this$0.f0;
                Throwable cause = th.getCause();
                if (cause != null) {
                    th = cause;
                }
                wm3Var.q(th);
            }
        }
    }

    public JobListenableFuture(st1 st1Var, wm3<R> wm3Var) {
        fs1.f(st1Var, "job");
        fs1.f(wm3Var, "underlying");
        this.a = st1Var;
        this.f0 = wm3Var;
        st1Var.z(new AnonymousClass1(this));
    }

    public final void b(R r) {
        this.f0.p(r);
    }

    @Override // java.util.concurrent.Future
    public boolean cancel(boolean z) {
        return this.f0.cancel(z);
    }

    @Override // defpackage.l02
    public void d(Runnable runnable, Executor executor) {
        this.f0.d(runnable, executor);
    }

    @Override // java.util.concurrent.Future
    public R get() {
        return this.f0.get();
    }

    @Override // java.util.concurrent.Future
    public R get(long j, TimeUnit timeUnit) {
        return this.f0.get(j, timeUnit);
    }

    @Override // java.util.concurrent.Future
    public boolean isCancelled() {
        return this.f0.isCancelled();
    }

    @Override // java.util.concurrent.Future
    public boolean isDone() {
        return this.f0.isDone();
    }

    /* JADX WARN: Illegal instructions before constructor call */
    /*
        Code decompiled incorrectly, please refer to instructions dump.
        To view partially-correct code enable 'Show inconsistent code' option in preferences
    */
    public /* synthetic */ JobListenableFuture(defpackage.st1 r1, defpackage.wm3 r2, int r3, defpackage.qi0 r4) {
        /*
            r0 = this;
            r3 = r3 & 2
            if (r3 == 0) goto Ld
            wm3 r2 = defpackage.wm3.t()
            java.lang.String r3 = "create()"
            defpackage.fs1.e(r2, r3)
        Ld:
            r0.<init>(r1, r2)
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: androidx.work.JobListenableFuture.<init>(st1, wm3, int, qi0):void");
    }
}
