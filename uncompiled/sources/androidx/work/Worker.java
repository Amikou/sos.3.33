package androidx.work;

import android.annotation.SuppressLint;
import android.content.Context;
import androidx.annotation.Keep;
import androidx.work.ListenableWorker;

/* loaded from: classes.dex */
public abstract class Worker extends ListenableWorker {
    public wm3<ListenableWorker.a> j0;

    /* loaded from: classes.dex */
    public class a implements Runnable {
        public a() {
        }

        @Override // java.lang.Runnable
        public void run() {
            try {
                Worker.this.j0.p(Worker.this.r());
            } catch (Throwable th) {
                Worker.this.j0.q(th);
            }
        }
    }

    @Keep
    @SuppressLint({"BanKeepAnnotation"})
    public Worker(Context context, WorkerParameters workerParameters) {
        super(context, workerParameters);
    }

    @Override // androidx.work.ListenableWorker
    public final l02<ListenableWorker.a> p() {
        this.j0 = wm3.t();
        c().execute(new a());
        return this.j0;
    }

    public abstract ListenableWorker.a r();
}
