package androidx.work.impl;

import android.content.Context;
import androidx.room.RoomDatabase;
import androidx.room.l;
import androidx.work.impl.a;
import defpackage.tw3;
import java.util.concurrent.Executor;
import java.util.concurrent.TimeUnit;

/* loaded from: classes.dex */
public abstract class WorkDatabase extends RoomDatabase {
    public static final long n = TimeUnit.DAYS.toMillis(1);

    /* loaded from: classes.dex */
    public class a implements tw3.c {
        public final /* synthetic */ Context a;

        public a(Context context) {
            this.a = context;
        }

        @Override // defpackage.tw3.c
        public tw3 a(tw3.b bVar) {
            tw3.b.a a = tw3.b.a(this.a);
            a.c(bVar.b).b(bVar.c).d(true);
            return new hc1().a(a.a());
        }
    }

    /* loaded from: classes.dex */
    public class b extends RoomDatabase.b {
        @Override // androidx.room.RoomDatabase.b
        public void c(sw3 sw3Var) {
            super.c(sw3Var);
            sw3Var.B();
            try {
                sw3Var.K(WorkDatabase.K());
                sw3Var.s0();
            } finally {
                sw3Var.N0();
            }
        }
    }

    public static WorkDatabase G(Context context, Executor executor, boolean z) {
        RoomDatabase.a a2;
        if (z) {
            a2 = l.c(context, WorkDatabase.class).c();
        } else {
            a2 = l.a(context, WorkDatabase.class, zp4.d());
            a2.f(new a(context));
        }
        return (WorkDatabase) a2.g(executor).a(I()).b(androidx.work.impl.a.a).b(new a.h(context, 2, 3)).b(androidx.work.impl.a.b).b(androidx.work.impl.a.c).b(new a.h(context, 5, 6)).b(androidx.work.impl.a.d).b(androidx.work.impl.a.e).b(androidx.work.impl.a.f).b(new a.i(context)).b(new a.h(context, 10, 11)).b(androidx.work.impl.a.g).e().d();
    }

    public static RoomDatabase.b I() {
        return new b();
    }

    public static long J() {
        return System.currentTimeMillis() - n;
    }

    public static String K() {
        return "DELETE FROM workspec WHERE state IN (2, 3, 5) AND (period_start_time + minimum_retention_duration) < " + J() + " AND (SELECT COUNT(*)=0 FROM dependency WHERE     prerequisite_id=id AND     work_spec_id NOT IN         (SELECT id FROM workspec WHERE state IN (2, 3, 5)))";
    }

    public abstract jm0 H();

    public abstract iu2 L();

    public abstract v24 M();

    public abstract kq4 N();

    public abstract nq4 O();

    public abstract uq4 P();

    public abstract xq4 Q();
}
