package androidx.work.impl;

import android.content.Context;
import android.os.Build;

/* compiled from: WorkDatabaseMigrations.java */
/* loaded from: classes.dex */
public class a {
    public static w82 a = new C0070a(1, 2);
    public static w82 b = new b(3, 4);
    public static w82 c = new c(4, 5);
    public static w82 d = new d(6, 7);
    public static w82 e = new e(7, 8);
    public static w82 f = new f(8, 9);
    public static w82 g = new g(11, 12);

    /* compiled from: WorkDatabaseMigrations.java */
    /* renamed from: androidx.work.impl.a$a  reason: collision with other inner class name */
    /* loaded from: classes.dex */
    public class C0070a extends w82 {
        public C0070a(int i, int i2) {
            super(i, i2);
        }

        @Override // defpackage.w82
        public void a(sw3 sw3Var) {
            sw3Var.K("CREATE TABLE IF NOT EXISTS `SystemIdInfo` (`work_spec_id` TEXT NOT NULL, `system_id` INTEGER NOT NULL, PRIMARY KEY(`work_spec_id`), FOREIGN KEY(`work_spec_id`) REFERENCES `WorkSpec`(`id`) ON UPDATE CASCADE ON DELETE CASCADE )");
            sw3Var.K("INSERT INTO SystemIdInfo(work_spec_id, system_id) SELECT work_spec_id, alarm_id AS system_id FROM alarmInfo");
            sw3Var.K("DROP TABLE IF EXISTS alarmInfo");
            sw3Var.K("INSERT OR IGNORE INTO worktag(tag, work_spec_id) SELECT worker_class_name AS tag, id AS work_spec_id FROM workspec");
        }
    }

    /* compiled from: WorkDatabaseMigrations.java */
    /* loaded from: classes.dex */
    public class b extends w82 {
        public b(int i, int i2) {
            super(i, i2);
        }

        @Override // defpackage.w82
        public void a(sw3 sw3Var) {
            if (Build.VERSION.SDK_INT >= 23) {
                sw3Var.K("UPDATE workspec SET schedule_requested_at=0 WHERE state NOT IN (2, 3, 5) AND schedule_requested_at=-1 AND interval_duration<>0");
            }
        }
    }

    /* compiled from: WorkDatabaseMigrations.java */
    /* loaded from: classes.dex */
    public class c extends w82 {
        public c(int i, int i2) {
            super(i, i2);
        }

        @Override // defpackage.w82
        public void a(sw3 sw3Var) {
            sw3Var.K("ALTER TABLE workspec ADD COLUMN `trigger_content_update_delay` INTEGER NOT NULL DEFAULT -1");
            sw3Var.K("ALTER TABLE workspec ADD COLUMN `trigger_max_content_delay` INTEGER NOT NULL DEFAULT -1");
        }
    }

    /* compiled from: WorkDatabaseMigrations.java */
    /* loaded from: classes.dex */
    public class d extends w82 {
        public d(int i, int i2) {
            super(i, i2);
        }

        @Override // defpackage.w82
        public void a(sw3 sw3Var) {
            sw3Var.K("CREATE TABLE IF NOT EXISTS `WorkProgress` (`work_spec_id` TEXT NOT NULL, `progress` BLOB NOT NULL, PRIMARY KEY(`work_spec_id`), FOREIGN KEY(`work_spec_id`) REFERENCES `WorkSpec`(`id`) ON UPDATE CASCADE ON DELETE CASCADE )");
        }
    }

    /* compiled from: WorkDatabaseMigrations.java */
    /* loaded from: classes.dex */
    public class e extends w82 {
        public e(int i, int i2) {
            super(i, i2);
        }

        @Override // defpackage.w82
        public void a(sw3 sw3Var) {
            sw3Var.K("CREATE INDEX IF NOT EXISTS `index_WorkSpec_period_start_time` ON `workspec` (`period_start_time`)");
        }
    }

    /* compiled from: WorkDatabaseMigrations.java */
    /* loaded from: classes.dex */
    public class f extends w82 {
        public f(int i, int i2) {
            super(i, i2);
        }

        @Override // defpackage.w82
        public void a(sw3 sw3Var) {
            sw3Var.K("ALTER TABLE workspec ADD COLUMN `run_in_foreground` INTEGER NOT NULL DEFAULT 0");
        }
    }

    /* compiled from: WorkDatabaseMigrations.java */
    /* loaded from: classes.dex */
    public class g extends w82 {
        public g(int i, int i2) {
            super(i, i2);
        }

        @Override // defpackage.w82
        public void a(sw3 sw3Var) {
            sw3Var.K("ALTER TABLE workspec ADD COLUMN `out_of_quota_policy` INTEGER NOT NULL DEFAULT 0");
        }
    }

    /* compiled from: WorkDatabaseMigrations.java */
    /* loaded from: classes.dex */
    public static class h extends w82 {
        public final Context c;

        public h(Context context, int i, int i2) {
            super(i, i2);
            this.c = context;
        }

        @Override // defpackage.w82
        public void a(sw3 sw3Var) {
            if (this.b >= 10) {
                sw3Var.u0("INSERT OR REPLACE INTO `Preference` (`key`, `long_value`) VALUES (@key, @long_value)", new Object[]{"reschedule_needed", 1});
            } else {
                this.c.getSharedPreferences("androidx.work.util.preferences", 0).edit().putBoolean("reschedule_needed", true).apply();
            }
        }
    }

    /* compiled from: WorkDatabaseMigrations.java */
    /* loaded from: classes.dex */
    public static class i extends w82 {
        public final Context c;

        public i(Context context) {
            super(9, 10);
            this.c = context;
        }

        @Override // defpackage.w82
        public void a(sw3 sw3Var) {
            sw3Var.K("CREATE TABLE IF NOT EXISTS `Preference` (`key` TEXT NOT NULL, `long_value` INTEGER, PRIMARY KEY(`key`))");
            lu2.b(this.c, sw3Var);
            kn1.a(this.c, sw3Var);
        }
    }
}
