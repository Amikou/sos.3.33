package androidx.work.impl;

import androidx.room.RoomDatabase;
import androidx.room.c;
import androidx.room.f;
import androidx.room.m;
import defpackage.f34;
import defpackage.tw3;
import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import org.web3j.ens.contracts.generated.PublicResolver;

/* loaded from: classes.dex */
public final class WorkDatabase_Impl extends WorkDatabase {
    public volatile uq4 o;
    public volatile jm0 p;
    public volatile xq4 q;
    public volatile v24 r;
    public volatile kq4 s;
    public volatile nq4 t;
    public volatile iu2 u;

    /* loaded from: classes.dex */
    public class a extends m.a {
        public a(int i) {
            super(i);
        }

        @Override // androidx.room.m.a
        public void a(sw3 sw3Var) {
            sw3Var.K("CREATE TABLE IF NOT EXISTS `Dependency` (`work_spec_id` TEXT NOT NULL, `prerequisite_id` TEXT NOT NULL, PRIMARY KEY(`work_spec_id`, `prerequisite_id`), FOREIGN KEY(`work_spec_id`) REFERENCES `WorkSpec`(`id`) ON UPDATE CASCADE ON DELETE CASCADE , FOREIGN KEY(`prerequisite_id`) REFERENCES `WorkSpec`(`id`) ON UPDATE CASCADE ON DELETE CASCADE )");
            sw3Var.K("CREATE INDEX IF NOT EXISTS `index_Dependency_work_spec_id` ON `Dependency` (`work_spec_id`)");
            sw3Var.K("CREATE INDEX IF NOT EXISTS `index_Dependency_prerequisite_id` ON `Dependency` (`prerequisite_id`)");
            sw3Var.K("CREATE TABLE IF NOT EXISTS `WorkSpec` (`id` TEXT NOT NULL, `state` INTEGER NOT NULL, `worker_class_name` TEXT NOT NULL, `input_merger_class_name` TEXT, `input` BLOB NOT NULL, `output` BLOB NOT NULL, `initial_delay` INTEGER NOT NULL, `interval_duration` INTEGER NOT NULL, `flex_duration` INTEGER NOT NULL, `run_attempt_count` INTEGER NOT NULL, `backoff_policy` INTEGER NOT NULL, `backoff_delay_duration` INTEGER NOT NULL, `period_start_time` INTEGER NOT NULL, `minimum_retention_duration` INTEGER NOT NULL, `schedule_requested_at` INTEGER NOT NULL, `run_in_foreground` INTEGER NOT NULL, `out_of_quota_policy` INTEGER NOT NULL, `required_network_type` INTEGER, `requires_charging` INTEGER NOT NULL, `requires_device_idle` INTEGER NOT NULL, `requires_battery_not_low` INTEGER NOT NULL, `requires_storage_not_low` INTEGER NOT NULL, `trigger_content_update_delay` INTEGER NOT NULL, `trigger_max_content_delay` INTEGER NOT NULL, `content_uri_triggers` BLOB, PRIMARY KEY(`id`))");
            sw3Var.K("CREATE INDEX IF NOT EXISTS `index_WorkSpec_schedule_requested_at` ON `WorkSpec` (`schedule_requested_at`)");
            sw3Var.K("CREATE INDEX IF NOT EXISTS `index_WorkSpec_period_start_time` ON `WorkSpec` (`period_start_time`)");
            sw3Var.K("CREATE TABLE IF NOT EXISTS `WorkTag` (`tag` TEXT NOT NULL, `work_spec_id` TEXT NOT NULL, PRIMARY KEY(`tag`, `work_spec_id`), FOREIGN KEY(`work_spec_id`) REFERENCES `WorkSpec`(`id`) ON UPDATE CASCADE ON DELETE CASCADE )");
            sw3Var.K("CREATE INDEX IF NOT EXISTS `index_WorkTag_work_spec_id` ON `WorkTag` (`work_spec_id`)");
            sw3Var.K("CREATE TABLE IF NOT EXISTS `SystemIdInfo` (`work_spec_id` TEXT NOT NULL, `system_id` INTEGER NOT NULL, PRIMARY KEY(`work_spec_id`), FOREIGN KEY(`work_spec_id`) REFERENCES `WorkSpec`(`id`) ON UPDATE CASCADE ON DELETE CASCADE )");
            sw3Var.K("CREATE TABLE IF NOT EXISTS `WorkName` (`name` TEXT NOT NULL, `work_spec_id` TEXT NOT NULL, PRIMARY KEY(`name`, `work_spec_id`), FOREIGN KEY(`work_spec_id`) REFERENCES `WorkSpec`(`id`) ON UPDATE CASCADE ON DELETE CASCADE )");
            sw3Var.K("CREATE INDEX IF NOT EXISTS `index_WorkName_work_spec_id` ON `WorkName` (`work_spec_id`)");
            sw3Var.K("CREATE TABLE IF NOT EXISTS `WorkProgress` (`work_spec_id` TEXT NOT NULL, `progress` BLOB NOT NULL, PRIMARY KEY(`work_spec_id`), FOREIGN KEY(`work_spec_id`) REFERENCES `WorkSpec`(`id`) ON UPDATE CASCADE ON DELETE CASCADE )");
            sw3Var.K("CREATE TABLE IF NOT EXISTS `Preference` (`key` TEXT NOT NULL, `long_value` INTEGER, PRIMARY KEY(`key`))");
            sw3Var.K("CREATE TABLE IF NOT EXISTS room_master_table (id INTEGER PRIMARY KEY,identity_hash TEXT)");
            sw3Var.K("INSERT OR REPLACE INTO room_master_table (id,identity_hash) VALUES(42, 'c103703e120ae8cc73c9248622f3cd1e')");
        }

        @Override // androidx.room.m.a
        public void b(sw3 sw3Var) {
            sw3Var.K("DROP TABLE IF EXISTS `Dependency`");
            sw3Var.K("DROP TABLE IF EXISTS `WorkSpec`");
            sw3Var.K("DROP TABLE IF EXISTS `WorkTag`");
            sw3Var.K("DROP TABLE IF EXISTS `SystemIdInfo`");
            sw3Var.K("DROP TABLE IF EXISTS `WorkName`");
            sw3Var.K("DROP TABLE IF EXISTS `WorkProgress`");
            sw3Var.K("DROP TABLE IF EXISTS `Preference`");
            if (WorkDatabase_Impl.this.g != null) {
                int size = WorkDatabase_Impl.this.g.size();
                for (int i = 0; i < size; i++) {
                    ((RoomDatabase.b) WorkDatabase_Impl.this.g.get(i)).b(sw3Var);
                }
            }
        }

        @Override // androidx.room.m.a
        public void c(sw3 sw3Var) {
            if (WorkDatabase_Impl.this.g != null) {
                int size = WorkDatabase_Impl.this.g.size();
                for (int i = 0; i < size; i++) {
                    ((RoomDatabase.b) WorkDatabase_Impl.this.g.get(i)).a(sw3Var);
                }
            }
        }

        @Override // androidx.room.m.a
        public void d(sw3 sw3Var) {
            WorkDatabase_Impl.this.a = sw3Var;
            sw3Var.K("PRAGMA foreign_keys = ON");
            WorkDatabase_Impl.this.x(sw3Var);
            if (WorkDatabase_Impl.this.g != null) {
                int size = WorkDatabase_Impl.this.g.size();
                for (int i = 0; i < size; i++) {
                    ((RoomDatabase.b) WorkDatabase_Impl.this.g.get(i)).c(sw3Var);
                }
            }
        }

        @Override // androidx.room.m.a
        public void e(sw3 sw3Var) {
        }

        @Override // androidx.room.m.a
        public void f(sw3 sw3Var) {
            id0.b(sw3Var);
        }

        @Override // androidx.room.m.a
        public m.b g(sw3 sw3Var) {
            HashMap hashMap = new HashMap(2);
            hashMap.put("work_spec_id", new f34.a("work_spec_id", "TEXT", true, 1, null, 1));
            hashMap.put("prerequisite_id", new f34.a("prerequisite_id", "TEXT", true, 2, null, 1));
            HashSet hashSet = new HashSet(2);
            hashSet.add(new f34.b("WorkSpec", "CASCADE", "CASCADE", Arrays.asList("work_spec_id"), Arrays.asList("id")));
            hashSet.add(new f34.b("WorkSpec", "CASCADE", "CASCADE", Arrays.asList("prerequisite_id"), Arrays.asList("id")));
            HashSet hashSet2 = new HashSet(2);
            hashSet2.add(new f34.d("index_Dependency_work_spec_id", false, Arrays.asList("work_spec_id")));
            hashSet2.add(new f34.d("index_Dependency_prerequisite_id", false, Arrays.asList("prerequisite_id")));
            f34 f34Var = new f34("Dependency", hashMap, hashSet, hashSet2);
            f34 a = f34.a(sw3Var, "Dependency");
            if (!f34Var.equals(a)) {
                return new m.b(false, "Dependency(androidx.work.impl.model.Dependency).\n Expected:\n" + f34Var + "\n Found:\n" + a);
            }
            HashMap hashMap2 = new HashMap(25);
            hashMap2.put("id", new f34.a("id", "TEXT", true, 1, null, 1));
            hashMap2.put("state", new f34.a("state", "INTEGER", true, 0, null, 1));
            hashMap2.put("worker_class_name", new f34.a("worker_class_name", "TEXT", true, 0, null, 1));
            hashMap2.put("input_merger_class_name", new f34.a("input_merger_class_name", "TEXT", false, 0, null, 1));
            hashMap2.put("input", new f34.a("input", "BLOB", true, 0, null, 1));
            hashMap2.put("output", new f34.a("output", "BLOB", true, 0, null, 1));
            hashMap2.put("initial_delay", new f34.a("initial_delay", "INTEGER", true, 0, null, 1));
            hashMap2.put("interval_duration", new f34.a("interval_duration", "INTEGER", true, 0, null, 1));
            hashMap2.put("flex_duration", new f34.a("flex_duration", "INTEGER", true, 0, null, 1));
            hashMap2.put("run_attempt_count", new f34.a("run_attempt_count", "INTEGER", true, 0, null, 1));
            hashMap2.put("backoff_policy", new f34.a("backoff_policy", "INTEGER", true, 0, null, 1));
            hashMap2.put("backoff_delay_duration", new f34.a("backoff_delay_duration", "INTEGER", true, 0, null, 1));
            hashMap2.put("period_start_time", new f34.a("period_start_time", "INTEGER", true, 0, null, 1));
            hashMap2.put("minimum_retention_duration", new f34.a("minimum_retention_duration", "INTEGER", true, 0, null, 1));
            hashMap2.put("schedule_requested_at", new f34.a("schedule_requested_at", "INTEGER", true, 0, null, 1));
            hashMap2.put("run_in_foreground", new f34.a("run_in_foreground", "INTEGER", true, 0, null, 1));
            hashMap2.put("out_of_quota_policy", new f34.a("out_of_quota_policy", "INTEGER", true, 0, null, 1));
            hashMap2.put("required_network_type", new f34.a("required_network_type", "INTEGER", false, 0, null, 1));
            hashMap2.put("requires_charging", new f34.a("requires_charging", "INTEGER", true, 0, null, 1));
            hashMap2.put("requires_device_idle", new f34.a("requires_device_idle", "INTEGER", true, 0, null, 1));
            hashMap2.put("requires_battery_not_low", new f34.a("requires_battery_not_low", "INTEGER", true, 0, null, 1));
            hashMap2.put("requires_storage_not_low", new f34.a("requires_storage_not_low", "INTEGER", true, 0, null, 1));
            hashMap2.put("trigger_content_update_delay", new f34.a("trigger_content_update_delay", "INTEGER", true, 0, null, 1));
            hashMap2.put("trigger_max_content_delay", new f34.a("trigger_max_content_delay", "INTEGER", true, 0, null, 1));
            hashMap2.put("content_uri_triggers", new f34.a("content_uri_triggers", "BLOB", false, 0, null, 1));
            HashSet hashSet3 = new HashSet(0);
            HashSet hashSet4 = new HashSet(2);
            hashSet4.add(new f34.d("index_WorkSpec_schedule_requested_at", false, Arrays.asList("schedule_requested_at")));
            hashSet4.add(new f34.d("index_WorkSpec_period_start_time", false, Arrays.asList("period_start_time")));
            f34 f34Var2 = new f34("WorkSpec", hashMap2, hashSet3, hashSet4);
            f34 a2 = f34.a(sw3Var, "WorkSpec");
            if (!f34Var2.equals(a2)) {
                return new m.b(false, "WorkSpec(androidx.work.impl.model.WorkSpec).\n Expected:\n" + f34Var2 + "\n Found:\n" + a2);
            }
            HashMap hashMap3 = new HashMap(2);
            hashMap3.put("tag", new f34.a("tag", "TEXT", true, 1, null, 1));
            hashMap3.put("work_spec_id", new f34.a("work_spec_id", "TEXT", true, 2, null, 1));
            HashSet hashSet5 = new HashSet(1);
            hashSet5.add(new f34.b("WorkSpec", "CASCADE", "CASCADE", Arrays.asList("work_spec_id"), Arrays.asList("id")));
            HashSet hashSet6 = new HashSet(1);
            hashSet6.add(new f34.d("index_WorkTag_work_spec_id", false, Arrays.asList("work_spec_id")));
            f34 f34Var3 = new f34("WorkTag", hashMap3, hashSet5, hashSet6);
            f34 a3 = f34.a(sw3Var, "WorkTag");
            if (!f34Var3.equals(a3)) {
                return new m.b(false, "WorkTag(androidx.work.impl.model.WorkTag).\n Expected:\n" + f34Var3 + "\n Found:\n" + a3);
            }
            HashMap hashMap4 = new HashMap(2);
            hashMap4.put("work_spec_id", new f34.a("work_spec_id", "TEXT", true, 1, null, 1));
            hashMap4.put("system_id", new f34.a("system_id", "INTEGER", true, 0, null, 1));
            HashSet hashSet7 = new HashSet(1);
            hashSet7.add(new f34.b("WorkSpec", "CASCADE", "CASCADE", Arrays.asList("work_spec_id"), Arrays.asList("id")));
            f34 f34Var4 = new f34("SystemIdInfo", hashMap4, hashSet7, new HashSet(0));
            f34 a4 = f34.a(sw3Var, "SystemIdInfo");
            if (!f34Var4.equals(a4)) {
                return new m.b(false, "SystemIdInfo(androidx.work.impl.model.SystemIdInfo).\n Expected:\n" + f34Var4 + "\n Found:\n" + a4);
            }
            HashMap hashMap5 = new HashMap(2);
            hashMap5.put(PublicResolver.FUNC_NAME, new f34.a(PublicResolver.FUNC_NAME, "TEXT", true, 1, null, 1));
            hashMap5.put("work_spec_id", new f34.a("work_spec_id", "TEXT", true, 2, null, 1));
            HashSet hashSet8 = new HashSet(1);
            hashSet8.add(new f34.b("WorkSpec", "CASCADE", "CASCADE", Arrays.asList("work_spec_id"), Arrays.asList("id")));
            HashSet hashSet9 = new HashSet(1);
            hashSet9.add(new f34.d("index_WorkName_work_spec_id", false, Arrays.asList("work_spec_id")));
            f34 f34Var5 = new f34("WorkName", hashMap5, hashSet8, hashSet9);
            f34 a5 = f34.a(sw3Var, "WorkName");
            if (!f34Var5.equals(a5)) {
                return new m.b(false, "WorkName(androidx.work.impl.model.WorkName).\n Expected:\n" + f34Var5 + "\n Found:\n" + a5);
            }
            HashMap hashMap6 = new HashMap(2);
            hashMap6.put("work_spec_id", new f34.a("work_spec_id", "TEXT", true, 1, null, 1));
            hashMap6.put("progress", new f34.a("progress", "BLOB", true, 0, null, 1));
            HashSet hashSet10 = new HashSet(1);
            hashSet10.add(new f34.b("WorkSpec", "CASCADE", "CASCADE", Arrays.asList("work_spec_id"), Arrays.asList("id")));
            f34 f34Var6 = new f34("WorkProgress", hashMap6, hashSet10, new HashSet(0));
            f34 a6 = f34.a(sw3Var, "WorkProgress");
            if (!f34Var6.equals(a6)) {
                return new m.b(false, "WorkProgress(androidx.work.impl.model.WorkProgress).\n Expected:\n" + f34Var6 + "\n Found:\n" + a6);
            }
            HashMap hashMap7 = new HashMap(2);
            hashMap7.put("key", new f34.a("key", "TEXT", true, 1, null, 1));
            hashMap7.put("long_value", new f34.a("long_value", "INTEGER", false, 0, null, 1));
            f34 f34Var7 = new f34("Preference", hashMap7, new HashSet(0), new HashSet(0));
            f34 a7 = f34.a(sw3Var, "Preference");
            if (!f34Var7.equals(a7)) {
                return new m.b(false, "Preference(androidx.work.impl.model.Preference).\n Expected:\n" + f34Var7 + "\n Found:\n" + a7);
            }
            return new m.b(true, null);
        }
    }

    @Override // androidx.work.impl.WorkDatabase
    public jm0 H() {
        jm0 jm0Var;
        if (this.p != null) {
            return this.p;
        }
        synchronized (this) {
            if (this.p == null) {
                this.p = new km0(this);
            }
            jm0Var = this.p;
        }
        return jm0Var;
    }

    @Override // androidx.work.impl.WorkDatabase
    public iu2 L() {
        iu2 iu2Var;
        if (this.u != null) {
            return this.u;
        }
        synchronized (this) {
            if (this.u == null) {
                this.u = new ju2(this);
            }
            iu2Var = this.u;
        }
        return iu2Var;
    }

    @Override // androidx.work.impl.WorkDatabase
    public v24 M() {
        v24 v24Var;
        if (this.r != null) {
            return this.r;
        }
        synchronized (this) {
            if (this.r == null) {
                this.r = new w24(this);
            }
            v24Var = this.r;
        }
        return v24Var;
    }

    @Override // androidx.work.impl.WorkDatabase
    public kq4 N() {
        kq4 kq4Var;
        if (this.s != null) {
            return this.s;
        }
        synchronized (this) {
            if (this.s == null) {
                this.s = new lq4(this);
            }
            kq4Var = this.s;
        }
        return kq4Var;
    }

    @Override // androidx.work.impl.WorkDatabase
    public nq4 O() {
        nq4 nq4Var;
        if (this.t != null) {
            return this.t;
        }
        synchronized (this) {
            if (this.t == null) {
                this.t = new oq4(this);
            }
            nq4Var = this.t;
        }
        return nq4Var;
    }

    @Override // androidx.work.impl.WorkDatabase
    public uq4 P() {
        uq4 uq4Var;
        if (this.o != null) {
            return this.o;
        }
        synchronized (this) {
            if (this.o == null) {
                this.o = new vq4(this);
            }
            uq4Var = this.o;
        }
        return uq4Var;
    }

    @Override // androidx.work.impl.WorkDatabase
    public xq4 Q() {
        xq4 xq4Var;
        if (this.q != null) {
            return this.q;
        }
        synchronized (this) {
            if (this.q == null) {
                this.q = new yq4(this);
            }
            xq4Var = this.q;
        }
        return xq4Var;
    }

    @Override // androidx.room.RoomDatabase
    public f h() {
        return new f(this, new HashMap(0), new HashMap(0), "Dependency", "WorkSpec", "WorkTag", "SystemIdInfo", "WorkName", "WorkProgress", "Preference");
    }

    @Override // androidx.room.RoomDatabase
    public tw3 i(c cVar) {
        return cVar.a.a(tw3.b.a(cVar.b).c(cVar.c).b(new m(cVar, new a(12), "c103703e120ae8cc73c9248622f3cd1e", "49f946663a8deb7054212b8adda248c6")).a());
    }
}
