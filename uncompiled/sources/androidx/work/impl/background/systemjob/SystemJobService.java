package androidx.work.impl.background.systemjob;

import android.app.Application;
import android.app.job.JobParameters;
import android.app.job.JobService;
import android.os.Build;
import android.os.PersistableBundle;
import android.text.TextUtils;
import androidx.work.WorkerParameters;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

/* loaded from: classes.dex */
public class SystemJobService extends JobService implements qy0 {
    public static final String g0 = v12.f("SystemJobService");
    public hq4 a;
    public final Map<String, JobParameters> f0 = new HashMap();

    public static String a(JobParameters jobParameters) {
        try {
            PersistableBundle extras = jobParameters.getExtras();
            if (extras == null || !extras.containsKey("EXTRA_WORK_SPEC_ID")) {
                return null;
            }
            return extras.getString("EXTRA_WORK_SPEC_ID");
        } catch (NullPointerException unused) {
            return null;
        }
    }

    @Override // defpackage.qy0
    public void c(String str, boolean z) {
        JobParameters remove;
        v12.c().a(g0, String.format("%s executed on JobScheduler", str), new Throwable[0]);
        synchronized (this.f0) {
            remove = this.f0.remove(str);
        }
        if (remove != null) {
            jobFinished(remove, z);
        }
    }

    @Override // android.app.Service
    public void onCreate() {
        super.onCreate();
        try {
            hq4 m = hq4.m(getApplicationContext());
            this.a = m;
            m.o().d(this);
        } catch (IllegalStateException unused) {
            if (Application.class.equals(getApplication().getClass())) {
                v12.c().h(g0, "Could not find WorkManager instance; this may be because an auto-backup is in progress. Ignoring JobScheduler commands for now. Please make sure that you are initializing WorkManager if you have manually disabled WorkManagerInitializer.", new Throwable[0]);
                return;
            }
            throw new IllegalStateException("WorkManager needs to be initialized via a ContentProvider#onCreate() or an Application#onCreate().");
        }
    }

    @Override // android.app.Service
    public void onDestroy() {
        super.onDestroy();
        hq4 hq4Var = this.a;
        if (hq4Var != null) {
            hq4Var.o().i(this);
        }
    }

    @Override // android.app.job.JobService
    public boolean onStartJob(JobParameters jobParameters) {
        if (this.a == null) {
            v12.c().a(g0, "WorkManager is not initialized; requesting retry.", new Throwable[0]);
            jobFinished(jobParameters, true);
            return false;
        }
        String a = a(jobParameters);
        if (TextUtils.isEmpty(a)) {
            v12.c().b(g0, "WorkSpec id not found!", new Throwable[0]);
            return false;
        }
        synchronized (this.f0) {
            if (this.f0.containsKey(a)) {
                v12.c().a(g0, String.format("Job is already being executed by SystemJobService: %s", a), new Throwable[0]);
                return false;
            }
            v12.c().a(g0, String.format("onStartJob for %s", a), new Throwable[0]);
            this.f0.put(a, jobParameters);
            WorkerParameters.a aVar = null;
            int i = Build.VERSION.SDK_INT;
            if (i >= 24) {
                aVar = new WorkerParameters.a();
                if (jobParameters.getTriggeredContentUris() != null) {
                    aVar.b = Arrays.asList(jobParameters.getTriggeredContentUris());
                }
                if (jobParameters.getTriggeredContentAuthorities() != null) {
                    aVar.a = Arrays.asList(jobParameters.getTriggeredContentAuthorities());
                }
                if (i >= 28) {
                    jobParameters.getNetwork();
                }
            }
            this.a.x(a, aVar);
            return true;
        }
    }

    @Override // android.app.job.JobService
    public boolean onStopJob(JobParameters jobParameters) {
        if (this.a == null) {
            v12.c().a(g0, "WorkManager is not initialized; requesting retry.", new Throwable[0]);
            return true;
        }
        String a = a(jobParameters);
        if (TextUtils.isEmpty(a)) {
            v12.c().b(g0, "WorkSpec id not found!", new Throwable[0]);
            return false;
        }
        v12.c().a(g0, String.format("onStopJob for %s", a), new Throwable[0]);
        synchronized (this.f0) {
            this.f0.remove(a);
        }
        this.a.z(a);
        return !this.a.o().f(a);
    }
}
