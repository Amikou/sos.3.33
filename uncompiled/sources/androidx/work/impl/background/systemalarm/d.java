package androidx.work.impl.background.systemalarm;

import android.content.Context;
import android.content.Intent;
import android.os.Handler;
import android.os.Looper;
import android.os.PowerManager;
import android.text.TextUtils;
import java.util.ArrayList;
import java.util.List;

/* compiled from: SystemAlarmDispatcher.java */
/* loaded from: classes.dex */
public class d implements qy0 {
    public static final String o0 = v12.f("SystemAlarmDispatcher");
    public final Context a;
    public final q34 f0;
    public final zq4 g0;
    public final bv2 h0;
    public final hq4 i0;
    public final androidx.work.impl.background.systemalarm.a j0;
    public final Handler k0;
    public final List<Intent> l0;
    public Intent m0;
    public c n0;

    /* compiled from: SystemAlarmDispatcher.java */
    /* loaded from: classes.dex */
    public class a implements Runnable {
        public a() {
        }

        @Override // java.lang.Runnable
        public void run() {
            d dVar;
            RunnableC0071d runnableC0071d;
            synchronized (d.this.l0) {
                d dVar2 = d.this;
                dVar2.m0 = dVar2.l0.get(0);
            }
            Intent intent = d.this.m0;
            if (intent != null) {
                String action = intent.getAction();
                int intExtra = d.this.m0.getIntExtra("KEY_START_ID", 0);
                v12 c = v12.c();
                String str = d.o0;
                c.a(str, String.format("Processing command %s, %s", d.this.m0, Integer.valueOf(intExtra)), new Throwable[0]);
                PowerManager.WakeLock b = rl4.b(d.this.a, String.format("%s (%s)", action, Integer.valueOf(intExtra)));
                try {
                    v12.c().a(str, String.format("Acquiring operation wake lock (%s) %s", action, b), new Throwable[0]);
                    b.acquire();
                    d dVar3 = d.this;
                    dVar3.j0.p(dVar3.m0, intExtra, dVar3);
                    v12.c().a(str, String.format("Releasing operation wake lock (%s) %s", action, b), new Throwable[0]);
                    b.release();
                    dVar = d.this;
                    runnableC0071d = new RunnableC0071d(dVar);
                } catch (Throwable th) {
                    try {
                        v12 c2 = v12.c();
                        String str2 = d.o0;
                        c2.b(str2, "Unexpected error in onHandleIntent", th);
                        v12.c().a(str2, String.format("Releasing operation wake lock (%s) %s", action, b), new Throwable[0]);
                        b.release();
                        dVar = d.this;
                        runnableC0071d = new RunnableC0071d(dVar);
                    } catch (Throwable th2) {
                        v12.c().a(d.o0, String.format("Releasing operation wake lock (%s) %s", action, b), new Throwable[0]);
                        b.release();
                        d dVar4 = d.this;
                        dVar4.k(new RunnableC0071d(dVar4));
                        throw th2;
                    }
                }
                dVar.k(runnableC0071d);
            }
        }
    }

    /* compiled from: SystemAlarmDispatcher.java */
    /* loaded from: classes.dex */
    public static class b implements Runnable {
        public final d a;
        public final Intent f0;
        public final int g0;

        public b(d dVar, Intent intent, int i) {
            this.a = dVar;
            this.f0 = intent;
            this.g0 = i;
        }

        @Override // java.lang.Runnable
        public void run() {
            this.a.a(this.f0, this.g0);
        }
    }

    /* compiled from: SystemAlarmDispatcher.java */
    /* loaded from: classes.dex */
    public interface c {
        void a();
    }

    /* compiled from: SystemAlarmDispatcher.java */
    /* renamed from: androidx.work.impl.background.systemalarm.d$d  reason: collision with other inner class name */
    /* loaded from: classes.dex */
    public static class RunnableC0071d implements Runnable {
        public final d a;

        public RunnableC0071d(d dVar) {
            this.a = dVar;
        }

        @Override // java.lang.Runnable
        public void run() {
            this.a.d();
        }
    }

    public d(Context context) {
        this(context, null, null);
    }

    public boolean a(Intent intent, int i) {
        v12 c2 = v12.c();
        String str = o0;
        c2.a(str, String.format("Adding command %s (%s)", intent, Integer.valueOf(i)), new Throwable[0]);
        b();
        String action = intent.getAction();
        if (TextUtils.isEmpty(action)) {
            v12.c().h(str, "Unknown command. Ignoring", new Throwable[0]);
            return false;
        } else if ("ACTION_CONSTRAINTS_CHANGED".equals(action) && i("ACTION_CONSTRAINTS_CHANGED")) {
            return false;
        } else {
            intent.putExtra("KEY_START_ID", i);
            synchronized (this.l0) {
                boolean z = this.l0.isEmpty() ? false : true;
                this.l0.add(intent);
                if (!z) {
                    l();
                }
            }
            return true;
        }
    }

    public final void b() {
        if (this.k0.getLooper().getThread() != Thread.currentThread()) {
            throw new IllegalStateException("Needs to be invoked on the main thread.");
        }
    }

    @Override // defpackage.qy0
    public void c(String str, boolean z) {
        k(new b(this, androidx.work.impl.background.systemalarm.a.d(this.a, str, z), 0));
    }

    public void d() {
        v12 c2 = v12.c();
        String str = o0;
        c2.a(str, "Checking if commands are complete.", new Throwable[0]);
        b();
        synchronized (this.l0) {
            if (this.m0 != null) {
                v12.c().a(str, String.format("Removing command %s", this.m0), new Throwable[0]);
                if (this.l0.remove(0).equals(this.m0)) {
                    this.m0 = null;
                } else {
                    throw new IllegalStateException("Dequeue-d command is not the first.");
                }
            }
            wl3 c3 = this.f0.c();
            if (!this.j0.o() && this.l0.isEmpty() && !c3.a()) {
                v12.c().a(str, "No more commands & intents.", new Throwable[0]);
                c cVar = this.n0;
                if (cVar != null) {
                    cVar.a();
                }
            } else if (!this.l0.isEmpty()) {
                l();
            }
        }
    }

    public bv2 e() {
        return this.h0;
    }

    public q34 f() {
        return this.f0;
    }

    public hq4 g() {
        return this.i0;
    }

    public zq4 h() {
        return this.g0;
    }

    public final boolean i(String str) {
        b();
        synchronized (this.l0) {
            for (Intent intent : this.l0) {
                if (str.equals(intent.getAction())) {
                    return true;
                }
            }
            return false;
        }
    }

    public void j() {
        v12.c().a(o0, "Destroying SystemAlarmDispatcher", new Throwable[0]);
        this.h0.i(this);
        this.g0.a();
        this.n0 = null;
    }

    public void k(Runnable runnable) {
        this.k0.post(runnable);
    }

    public final void l() {
        b();
        PowerManager.WakeLock b2 = rl4.b(this.a, "ProcessCommand");
        try {
            b2.acquire();
            this.i0.r().b(new a());
        } finally {
            b2.release();
        }
    }

    public void m(c cVar) {
        if (this.n0 != null) {
            v12.c().b(o0, "A completion listener for SystemAlarmDispatcher already exists.", new Throwable[0]);
        } else {
            this.n0 = cVar;
        }
    }

    public d(Context context, bv2 bv2Var, hq4 hq4Var) {
        Context applicationContext = context.getApplicationContext();
        this.a = applicationContext;
        this.j0 = new androidx.work.impl.background.systemalarm.a(applicationContext);
        this.g0 = new zq4();
        hq4Var = hq4Var == null ? hq4.m(context) : hq4Var;
        this.i0 = hq4Var;
        bv2Var = bv2Var == null ? hq4Var.o() : bv2Var;
        this.h0 = bv2Var;
        this.f0 = hq4Var.r();
        bv2Var.d(this);
        this.l0 = new ArrayList();
        this.m0 = null;
        this.k0 = new Handler(Looper.getMainLooper());
    }
}
