package androidx.work.impl.background.systemalarm;

import android.content.Context;
import android.content.Intent;
import androidx.work.impl.background.systemalarm.d;
import java.util.ArrayList;
import java.util.List;

/* compiled from: ConstraintsCommandHandler.java */
/* loaded from: classes.dex */
public class b {
    public static final String e = v12.f("ConstraintsCmdHandler");
    public final Context a;
    public final int b;
    public final d c;
    public final wp4 d;

    public b(Context context, int i, d dVar) {
        this.a = context;
        this.b = i;
        this.c = dVar;
        this.d = new wp4(context, dVar.f(), null);
    }

    public void a() {
        List<tq4> f = this.c.g().q().P().f();
        ConstraintProxy.a(this.a, f);
        this.d.d(f);
        ArrayList<tq4> arrayList = new ArrayList(f.size());
        long currentTimeMillis = System.currentTimeMillis();
        for (tq4 tq4Var : f) {
            String str = tq4Var.a;
            if (currentTimeMillis >= tq4Var.a() && (!tq4Var.b() || this.d.c(str))) {
                arrayList.add(tq4Var);
            }
        }
        for (tq4 tq4Var2 : arrayList) {
            String str2 = tq4Var2.a;
            Intent b = a.b(this.a, str2);
            v12.c().a(e, String.format("Creating a delay_met command for workSpec with id (%s)", str2), new Throwable[0]);
            d dVar = this.c;
            dVar.k(new d.b(dVar, b, this.b));
        }
        this.d.e();
    }
}
