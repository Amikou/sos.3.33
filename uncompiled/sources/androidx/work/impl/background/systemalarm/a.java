package androidx.work.impl.background.systemalarm;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import androidx.work.impl.WorkDatabase;
import androidx.work.impl.background.systemalarm.d;
import java.util.HashMap;
import java.util.Map;

/* compiled from: CommandHandler.java */
/* loaded from: classes.dex */
public class a implements qy0 {
    public static final String h0 = v12.f("CommandHandler");
    public final Context a;
    public final Map<String, qy0> f0 = new HashMap();
    public final Object g0 = new Object();

    public a(Context context) {
        this.a = context;
    }

    public static Intent a(Context context) {
        Intent intent = new Intent(context, SystemAlarmService.class);
        intent.setAction("ACTION_CONSTRAINTS_CHANGED");
        return intent;
    }

    public static Intent b(Context context, String str) {
        Intent intent = new Intent(context, SystemAlarmService.class);
        intent.setAction("ACTION_DELAY_MET");
        intent.putExtra("KEY_WORKSPEC_ID", str);
        return intent;
    }

    public static Intent d(Context context, String str, boolean z) {
        Intent intent = new Intent(context, SystemAlarmService.class);
        intent.setAction("ACTION_EXECUTION_COMPLETED");
        intent.putExtra("KEY_WORKSPEC_ID", str);
        intent.putExtra("KEY_NEEDS_RESCHEDULE", z);
        return intent;
    }

    public static Intent e(Context context) {
        Intent intent = new Intent(context, SystemAlarmService.class);
        intent.setAction("ACTION_RESCHEDULE");
        return intent;
    }

    public static Intent f(Context context, String str) {
        Intent intent = new Intent(context, SystemAlarmService.class);
        intent.setAction("ACTION_SCHEDULE_WORK");
        intent.putExtra("KEY_WORKSPEC_ID", str);
        return intent;
    }

    public static Intent g(Context context, String str) {
        Intent intent = new Intent(context, SystemAlarmService.class);
        intent.setAction("ACTION_STOP_WORK");
        intent.putExtra("KEY_WORKSPEC_ID", str);
        return intent;
    }

    public static boolean n(Bundle bundle, String... strArr) {
        if (bundle == null || bundle.isEmpty()) {
            return false;
        }
        for (String str : strArr) {
            if (bundle.get(str) == null) {
                return false;
            }
        }
        return true;
    }

    @Override // defpackage.qy0
    public void c(String str, boolean z) {
        synchronized (this.g0) {
            qy0 remove = this.f0.remove(str);
            if (remove != null) {
                remove.c(str, z);
            }
        }
    }

    public final void h(Intent intent, int i, d dVar) {
        v12.c().a(h0, String.format("Handling constraints changed %s", intent), new Throwable[0]);
        new b(this.a, i, dVar).a();
    }

    public final void i(Intent intent, int i, d dVar) {
        Bundle extras = intent.getExtras();
        synchronized (this.g0) {
            String string = extras.getString("KEY_WORKSPEC_ID");
            v12 c = v12.c();
            String str = h0;
            c.a(str, String.format("Handing delay met for %s", string), new Throwable[0]);
            if (!this.f0.containsKey(string)) {
                c cVar = new c(this.a, i, string, dVar);
                this.f0.put(string, cVar);
                cVar.e();
            } else {
                v12.c().a(str, String.format("WorkSpec %s is already being handled for ACTION_DELAY_MET", string), new Throwable[0]);
            }
        }
    }

    public final void j(Intent intent, int i) {
        Bundle extras = intent.getExtras();
        String string = extras.getString("KEY_WORKSPEC_ID");
        boolean z = extras.getBoolean("KEY_NEEDS_RESCHEDULE");
        v12.c().a(h0, String.format("Handling onExecutionCompleted %s, %s", intent, Integer.valueOf(i)), new Throwable[0]);
        c(string, z);
    }

    public final void k(Intent intent, int i, d dVar) {
        v12.c().a(h0, String.format("Handling reschedule %s, %s", intent, Integer.valueOf(i)), new Throwable[0]);
        dVar.g().u();
    }

    public final void l(Intent intent, int i, d dVar) {
        String string = intent.getExtras().getString("KEY_WORKSPEC_ID");
        v12 c = v12.c();
        String str = h0;
        c.a(str, String.format("Handling schedule work for %s", string), new Throwable[0]);
        WorkDatabase q = dVar.g().q();
        q.e();
        try {
            tq4 l = q.P().l(string);
            if (l == null) {
                v12 c2 = v12.c();
                c2.h(str, "Skipping scheduling " + string + " because it's no longer in the DB", new Throwable[0]);
            } else if (l.b.isFinished()) {
                v12 c3 = v12.c();
                c3.h(str, "Skipping scheduling " + string + "because it is finished.", new Throwable[0]);
            } else {
                long a = l.a();
                if (!l.b()) {
                    v12.c().a(str, String.format("Setting up Alarms for %s at %s", string, Long.valueOf(a)), new Throwable[0]);
                    ta.c(this.a, dVar.g(), string, a);
                } else {
                    v12.c().a(str, String.format("Opportunistically setting an alarm for %s at %s", string, Long.valueOf(a)), new Throwable[0]);
                    ta.c(this.a, dVar.g(), string, a);
                    dVar.k(new d.b(dVar, a(this.a), i));
                }
                q.E();
            }
        } finally {
            q.j();
        }
    }

    public final void m(Intent intent, d dVar) {
        String string = intent.getExtras().getString("KEY_WORKSPEC_ID");
        v12.c().a(h0, String.format("Handing stopWork work for %s", string), new Throwable[0]);
        dVar.g().z(string);
        ta.a(this.a, dVar.g(), string);
        dVar.c(string, false);
    }

    public boolean o() {
        boolean z;
        synchronized (this.g0) {
            z = !this.f0.isEmpty();
        }
        return z;
    }

    public void p(Intent intent, int i, d dVar) {
        String action = intent.getAction();
        if ("ACTION_CONSTRAINTS_CHANGED".equals(action)) {
            h(intent, i, dVar);
        } else if ("ACTION_RESCHEDULE".equals(action)) {
            k(intent, i, dVar);
        } else if (!n(intent.getExtras(), "KEY_WORKSPEC_ID")) {
            v12.c().b(h0, String.format("Invalid request for %s, requires %s.", action, "KEY_WORKSPEC_ID"), new Throwable[0]);
        } else if ("ACTION_SCHEDULE_WORK".equals(action)) {
            l(intent, i, dVar);
        } else if ("ACTION_DELAY_MET".equals(action)) {
            i(intent, i, dVar);
        } else if ("ACTION_STOP_WORK".equals(action)) {
            m(intent, dVar);
        } else if ("ACTION_EXECUTION_COMPLETED".equals(action)) {
            j(intent, i);
        } else {
            v12.c().h(h0, String.format("Ignoring intent %s", intent), new Throwable[0]);
        }
    }
}
