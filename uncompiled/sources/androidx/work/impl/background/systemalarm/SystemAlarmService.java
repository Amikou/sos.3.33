package androidx.work.impl.background.systemalarm;

import android.content.Intent;
import androidx.lifecycle.LifecycleService;
import androidx.work.impl.background.systemalarm.d;

/* loaded from: classes.dex */
public class SystemAlarmService extends LifecycleService implements d.c {
    public static final String h0 = v12.f("SystemAlarmService");
    public d f0;
    public boolean g0;

    @Override // androidx.work.impl.background.systemalarm.d.c
    public void a() {
        this.g0 = true;
        v12.c().a(h0, "All commands completed in dispatcher", new Throwable[0]);
        rl4.a();
        stopSelf();
    }

    public final void e() {
        d dVar = new d(this);
        this.f0 = dVar;
        dVar.m(this);
    }

    @Override // androidx.lifecycle.LifecycleService, android.app.Service
    public void onCreate() {
        super.onCreate();
        e();
        this.g0 = false;
    }

    @Override // androidx.lifecycle.LifecycleService, android.app.Service
    public void onDestroy() {
        super.onDestroy();
        this.g0 = true;
        this.f0.j();
    }

    @Override // androidx.lifecycle.LifecycleService, android.app.Service
    public int onStartCommand(Intent intent, int i, int i2) {
        super.onStartCommand(intent, i, i2);
        if (this.g0) {
            v12.c().d(h0, "Re-initializing SystemAlarmDispatcher after a request to shut-down.", new Throwable[0]);
            this.f0.j();
            e();
            this.g0 = false;
        }
        if (intent != null) {
            this.f0.a(intent, i2);
            return 3;
        }
        return 3;
    }
}
