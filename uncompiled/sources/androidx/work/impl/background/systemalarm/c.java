package androidx.work.impl.background.systemalarm;

import android.content.Context;
import android.content.Intent;
import android.os.PowerManager;
import androidx.work.impl.background.systemalarm.d;
import defpackage.zq4;
import java.util.Collections;
import java.util.List;

/* compiled from: DelayMetCommandHandler.java */
/* loaded from: classes.dex */
public class c implements vp4, qy0, zq4.b {
    public static final String n0 = v12.f("DelayMetCommandHandler");
    public final Context a;
    public final int f0;
    public final String g0;
    public final d h0;
    public final wp4 i0;
    public PowerManager.WakeLock l0;
    public boolean m0 = false;
    public int k0 = 0;
    public final Object j0 = new Object();

    public c(Context context, int i, String str, d dVar) {
        this.a = context;
        this.f0 = i;
        this.h0 = dVar;
        this.g0 = str;
        this.i0 = new wp4(context, dVar.f(), this);
    }

    @Override // defpackage.zq4.b
    public void a(String str) {
        v12.c().a(n0, String.format("Exceeded time limits on execution for %s", str), new Throwable[0]);
        g();
    }

    @Override // defpackage.vp4
    public void b(List<String> list) {
        g();
    }

    @Override // defpackage.qy0
    public void c(String str, boolean z) {
        v12.c().a(n0, String.format("onExecuted %s, %s", str, Boolean.valueOf(z)), new Throwable[0]);
        d();
        if (z) {
            Intent f = a.f(this.a, this.g0);
            d dVar = this.h0;
            dVar.k(new d.b(dVar, f, this.f0));
        }
        if (this.m0) {
            Intent a = a.a(this.a);
            d dVar2 = this.h0;
            dVar2.k(new d.b(dVar2, a, this.f0));
        }
    }

    public final void d() {
        synchronized (this.j0) {
            this.i0.e();
            this.h0.h().c(this.g0);
            PowerManager.WakeLock wakeLock = this.l0;
            if (wakeLock != null && wakeLock.isHeld()) {
                v12.c().a(n0, String.format("Releasing wakelock %s for WorkSpec %s", this.l0, this.g0), new Throwable[0]);
                this.l0.release();
            }
        }
    }

    public void e() {
        this.l0 = rl4.b(this.a, String.format("%s (%s)", this.g0, Integer.valueOf(this.f0)));
        v12 c = v12.c();
        String str = n0;
        c.a(str, String.format("Acquiring wakelock %s for WorkSpec %s", this.l0, this.g0), new Throwable[0]);
        this.l0.acquire();
        tq4 l = this.h0.g().q().P().l(this.g0);
        if (l == null) {
            g();
            return;
        }
        boolean b = l.b();
        this.m0 = b;
        if (!b) {
            v12.c().a(str, String.format("No constraints for %s", this.g0), new Throwable[0]);
            f(Collections.singletonList(this.g0));
            return;
        }
        this.i0.d(Collections.singletonList(l));
    }

    @Override // defpackage.vp4
    public void f(List<String> list) {
        if (list.contains(this.g0)) {
            synchronized (this.j0) {
                if (this.k0 == 0) {
                    this.k0 = 1;
                    v12.c().a(n0, String.format("onAllConstraintsMet for %s", this.g0), new Throwable[0]);
                    if (this.h0.e().j(this.g0)) {
                        this.h0.h().b(this.g0, 600000L, this);
                    } else {
                        d();
                    }
                } else {
                    v12.c().a(n0, String.format("Already started work for %s", this.g0), new Throwable[0]);
                }
            }
        }
    }

    public final void g() {
        synchronized (this.j0) {
            if (this.k0 < 2) {
                this.k0 = 2;
                v12 c = v12.c();
                String str = n0;
                c.a(str, String.format("Stopping work for WorkSpec %s", this.g0), new Throwable[0]);
                Intent g = a.g(this.a, this.g0);
                d dVar = this.h0;
                dVar.k(new d.b(dVar, g, this.f0));
                if (this.h0.e().g(this.g0)) {
                    v12.c().a(str, String.format("WorkSpec %s needs to be rescheduled", this.g0), new Throwable[0]);
                    Intent f = a.f(this.a, this.g0);
                    d dVar2 = this.h0;
                    dVar2.k(new d.b(dVar2, f, this.f0));
                } else {
                    v12.c().a(str, String.format("Processor does not have WorkSpec %s. No need to reschedule ", this.g0), new Throwable[0]);
                }
            } else {
                v12.c().a(n0, String.format("Already stopped work for %s", this.g0), new Throwable[0]);
            }
        }
    }
}
