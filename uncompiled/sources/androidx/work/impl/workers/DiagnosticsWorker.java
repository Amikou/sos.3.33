package androidx.work.impl.workers;

import android.content.Context;
import android.os.Build;
import android.text.TextUtils;
import androidx.work.ListenableWorker;
import androidx.work.Worker;
import androidx.work.WorkerParameters;
import androidx.work.impl.WorkDatabase;
import java.util.List;
import java.util.concurrent.TimeUnit;

/* loaded from: classes.dex */
public class DiagnosticsWorker extends Worker {
    public static final String k0 = v12.f("DiagnosticsWrkr");

    public DiagnosticsWorker(Context context, WorkerParameters workerParameters) {
        super(context, workerParameters);
    }

    public static String s(tq4 tq4Var, String str, Integer num, String str2) {
        return String.format("\n%s\t %s\t %s\t %s\t %s\t %s\t", tq4Var.a, tq4Var.c, num, tq4Var.b.name(), str, str2);
    }

    public static String t(kq4 kq4Var, xq4 xq4Var, v24 v24Var, List<tq4> list) {
        StringBuilder sb = new StringBuilder();
        sb.append(String.format("\n Id \t Class Name\t %s\t State\t Unique Name\t Tags\t", Build.VERSION.SDK_INT >= 23 ? "Job Id" : "Alarm Id"));
        for (tq4 tq4Var : list) {
            Integer num = null;
            u24 c = v24Var.c(tq4Var.a);
            if (c != null) {
                num = Integer.valueOf(c.b);
            }
            sb.append(s(tq4Var, TextUtils.join(",", kq4Var.b(tq4Var.a)), num, TextUtils.join(",", xq4Var.b(tq4Var.a))));
        }
        return sb.toString();
    }

    @Override // androidx.work.Worker
    public ListenableWorker.a r() {
        WorkDatabase q = hq4.m(a()).q();
        uq4 P = q.P();
        kq4 N = q.N();
        xq4 Q = q.Q();
        v24 M = q.M();
        List<tq4> d = P.d(System.currentTimeMillis() - TimeUnit.DAYS.toMillis(1L));
        List<tq4> h = P.h();
        List<tq4> s = P.s(200);
        if (d != null && !d.isEmpty()) {
            v12 c = v12.c();
            String str = k0;
            c.d(str, "Recently completed work:\n\n", new Throwable[0]);
            v12.c().d(str, t(N, Q, M, d), new Throwable[0]);
        }
        if (h != null && !h.isEmpty()) {
            v12 c2 = v12.c();
            String str2 = k0;
            c2.d(str2, "Running work:\n\n", new Throwable[0]);
            v12.c().d(str2, t(N, Q, M, h), new Throwable[0]);
        }
        if (s != null && !s.isEmpty()) {
            v12 c3 = v12.c();
            String str3 = k0;
            c3.d(str3, "Enqueued work:\n\n", new Throwable[0]);
            v12.c().d(str3, t(N, Q, M, s), new Throwable[0]);
        }
        return ListenableWorker.a.c();
    }
}
