package androidx.work.impl.workers;

import android.content.Context;
import android.text.TextUtils;
import androidx.work.ListenableWorker;
import androidx.work.WorkerParameters;
import androidx.work.impl.WorkDatabase;
import java.util.Collections;
import java.util.List;

/* loaded from: classes.dex */
public class ConstraintTrackingWorker extends ListenableWorker implements vp4 {
    public static final String o0 = v12.f("ConstraintTrkngWrkr");
    public WorkerParameters j0;
    public final Object k0;
    public volatile boolean l0;
    public wm3<ListenableWorker.a> m0;
    public ListenableWorker n0;

    /* loaded from: classes.dex */
    public class a implements Runnable {
        public a() {
        }

        @Override // java.lang.Runnable
        public void run() {
            ConstraintTrackingWorker.this.u();
        }
    }

    /* loaded from: classes.dex */
    public class b implements Runnable {
        public final /* synthetic */ l02 a;

        public b(l02 l02Var) {
            this.a = l02Var;
        }

        @Override // java.lang.Runnable
        public void run() {
            synchronized (ConstraintTrackingWorker.this.k0) {
                if (ConstraintTrackingWorker.this.l0) {
                    ConstraintTrackingWorker.this.t();
                } else {
                    ConstraintTrackingWorker.this.m0.r(this.a);
                }
            }
        }
    }

    public ConstraintTrackingWorker(Context context, WorkerParameters workerParameters) {
        super(context, workerParameters);
        this.j0 = workerParameters;
        this.k0 = new Object();
        this.l0 = false;
        this.m0 = wm3.t();
    }

    @Override // defpackage.vp4
    public void b(List<String> list) {
        v12.c().a(o0, String.format("Constraints changed for %s", list), new Throwable[0]);
        synchronized (this.k0) {
            this.l0 = true;
        }
    }

    @Override // defpackage.vp4
    public void f(List<String> list) {
    }

    @Override // androidx.work.ListenableWorker
    public q34 h() {
        return hq4.m(a()).r();
    }

    @Override // androidx.work.ListenableWorker
    public boolean j() {
        ListenableWorker listenableWorker = this.n0;
        return listenableWorker != null && listenableWorker.j();
    }

    @Override // androidx.work.ListenableWorker
    public void m() {
        super.m();
        ListenableWorker listenableWorker = this.n0;
        if (listenableWorker == null || listenableWorker.k()) {
            return;
        }
        this.n0.q();
    }

    @Override // androidx.work.ListenableWorker
    public l02<ListenableWorker.a> p() {
        c().execute(new a());
        return this.m0;
    }

    public WorkDatabase r() {
        return hq4.m(a()).q();
    }

    public void s() {
        this.m0.p(ListenableWorker.a.a());
    }

    public void t() {
        this.m0.p(ListenableWorker.a.b());
    }

    public void u() {
        String l = g().l("androidx.work.impl.workers.ConstraintTrackingWorker.ARGUMENT_CLASS_NAME");
        if (TextUtils.isEmpty(l)) {
            v12.c().b(o0, "No worker to delegate to.", new Throwable[0]);
            s();
            return;
        }
        ListenableWorker b2 = i().b(a(), l, this.j0);
        this.n0 = b2;
        if (b2 == null) {
            v12.c().a(o0, "No worker to delegate to.", new Throwable[0]);
            s();
            return;
        }
        tq4 l2 = r().P().l(e().toString());
        if (l2 == null) {
            s();
            return;
        }
        wp4 wp4Var = new wp4(a(), h(), this);
        wp4Var.d(Collections.singletonList(l2));
        if (wp4Var.c(e().toString())) {
            v12.c().a(o0, String.format("Constraints met for delegate %s", l), new Throwable[0]);
            try {
                l02<ListenableWorker.a> p = this.n0.p();
                p.d(new b(p), c());
                return;
            } catch (Throwable th) {
                v12 c = v12.c();
                String str = o0;
                c.a(str, String.format("Delegated worker %s threw exception in startWork.", l), th);
                synchronized (this.k0) {
                    if (this.l0) {
                        v12.c().a(str, "Constraints were unmet, Retrying.", new Throwable[0]);
                        t();
                    } else {
                        s();
                    }
                    return;
                }
            }
        }
        v12.c().a(o0, String.format("Constraints not met for delegate %s. Requesting retry.", l), new Throwable[0]);
        t();
    }
}
