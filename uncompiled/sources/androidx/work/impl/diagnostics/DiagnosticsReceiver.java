package androidx.work.impl.diagnostics;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import androidx.work.c;
import androidx.work.impl.workers.DiagnosticsWorker;

/* loaded from: classes.dex */
public class DiagnosticsReceiver extends BroadcastReceiver {
    public static final String a = v12.f("DiagnosticsRcvr");

    @Override // android.content.BroadcastReceiver
    public void onReceive(Context context, Intent intent) {
        if (intent == null) {
            return;
        }
        v12.c().a(a, "Requesting diagnostics", new Throwable[0]);
        try {
            gq4.f(context).b(c.d(DiagnosticsWorker.class));
        } catch (IllegalStateException e) {
            v12.c().b(a, "WorkManager is not initialized", e);
        }
    }
}
