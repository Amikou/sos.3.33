package androidx.work.impl.foreground;

import android.app.Notification;
import android.app.NotificationManager;
import android.content.Intent;
import android.os.Build;
import android.os.Handler;
import android.os.Looper;
import androidx.lifecycle.LifecycleService;
import androidx.work.impl.foreground.a;

/* loaded from: classes.dex */
public class SystemForegroundService extends LifecycleService implements a.b {
    public static final String j0 = v12.f("SystemFgService");
    public Handler f0;
    public boolean g0;
    public androidx.work.impl.foreground.a h0;
    public NotificationManager i0;

    /* loaded from: classes.dex */
    public class a implements Runnable {
        public final /* synthetic */ int a;
        public final /* synthetic */ Notification f0;
        public final /* synthetic */ int g0;

        public a(int i, Notification notification, int i2) {
            this.a = i;
            this.f0 = notification;
            this.g0 = i2;
        }

        @Override // java.lang.Runnable
        public void run() {
            if (Build.VERSION.SDK_INT >= 29) {
                SystemForegroundService.this.startForeground(this.a, this.f0, this.g0);
            } else {
                SystemForegroundService.this.startForeground(this.a, this.f0);
            }
        }
    }

    /* loaded from: classes.dex */
    public class b implements Runnable {
        public final /* synthetic */ int a;
        public final /* synthetic */ Notification f0;

        public b(int i, Notification notification) {
            this.a = i;
            this.f0 = notification;
        }

        @Override // java.lang.Runnable
        public void run() {
            SystemForegroundService.this.i0.notify(this.a, this.f0);
        }
    }

    /* loaded from: classes.dex */
    public class c implements Runnable {
        public final /* synthetic */ int a;

        public c(int i) {
            this.a = i;
        }

        @Override // java.lang.Runnable
        public void run() {
            SystemForegroundService.this.i0.cancel(this.a);
        }
    }

    @Override // androidx.work.impl.foreground.a.b
    public void b(int i, int i2, Notification notification) {
        this.f0.post(new a(i, notification, i2));
    }

    @Override // androidx.work.impl.foreground.a.b
    public void c(int i, Notification notification) {
        this.f0.post(new b(i, notification));
    }

    @Override // androidx.work.impl.foreground.a.b
    public void d(int i) {
        this.f0.post(new c(i));
    }

    public final void e() {
        this.f0 = new Handler(Looper.getMainLooper());
        this.i0 = (NotificationManager) getApplicationContext().getSystemService("notification");
        androidx.work.impl.foreground.a aVar = new androidx.work.impl.foreground.a(getApplicationContext());
        this.h0 = aVar;
        aVar.m(this);
    }

    @Override // androidx.lifecycle.LifecycleService, android.app.Service
    public void onCreate() {
        super.onCreate();
        e();
    }

    @Override // androidx.lifecycle.LifecycleService, android.app.Service
    public void onDestroy() {
        super.onDestroy();
        this.h0.k();
    }

    @Override // androidx.lifecycle.LifecycleService, android.app.Service
    public int onStartCommand(Intent intent, int i, int i2) {
        super.onStartCommand(intent, i, i2);
        if (this.g0) {
            v12.c().d(j0, "Re-initializing SystemForegroundService after a request to shut-down.", new Throwable[0]);
            this.h0.k();
            e();
            this.g0 = false;
        }
        if (intent != null) {
            this.h0.l(intent);
            return 3;
        }
        return 3;
    }

    @Override // androidx.work.impl.foreground.a.b
    public void stop() {
        this.g0 = true;
        v12.c().a(j0, "All commands completed.", new Throwable[0]);
        if (Build.VERSION.SDK_INT >= 26) {
            stopForeground(true);
        }
        stopSelf();
    }
}
