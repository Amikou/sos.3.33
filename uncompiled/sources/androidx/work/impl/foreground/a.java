package androidx.work.impl.foreground;

import android.app.Notification;
import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.text.TextUtils;
import androidx.work.impl.WorkDatabase;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.UUID;

/* compiled from: SystemForegroundDispatcher.java */
/* loaded from: classes.dex */
public class a implements vp4, qy0 {
    public static final String o0 = v12.f("SystemFgDispatcher");
    public Context a;
    public hq4 f0;
    public final q34 g0;
    public final Object h0 = new Object();
    public String i0;
    public final Map<String, s81> j0;
    public final Map<String, tq4> k0;
    public final Set<tq4> l0;
    public final wp4 m0;
    public b n0;

    /* compiled from: SystemForegroundDispatcher.java */
    /* renamed from: androidx.work.impl.foreground.a$a  reason: collision with other inner class name */
    /* loaded from: classes.dex */
    public class RunnableC0072a implements Runnable {
        public final /* synthetic */ WorkDatabase a;
        public final /* synthetic */ String f0;

        public RunnableC0072a(WorkDatabase workDatabase, String str) {
            this.a = workDatabase;
            this.f0 = str;
        }

        @Override // java.lang.Runnable
        public void run() {
            tq4 l = this.a.P().l(this.f0);
            if (l == null || !l.b()) {
                return;
            }
            synchronized (a.this.h0) {
                a.this.k0.put(this.f0, l);
                a.this.l0.add(l);
                a aVar = a.this;
                aVar.m0.d(aVar.l0);
            }
        }
    }

    /* compiled from: SystemForegroundDispatcher.java */
    /* loaded from: classes.dex */
    public interface b {
        void b(int i, int i2, Notification notification);

        void c(int i, Notification notification);

        void d(int i);

        void stop();
    }

    public a(Context context) {
        this.a = context;
        hq4 m = hq4.m(context);
        this.f0 = m;
        q34 r = m.r();
        this.g0 = r;
        this.i0 = null;
        this.j0 = new LinkedHashMap();
        this.l0 = new HashSet();
        this.k0 = new HashMap();
        this.m0 = new wp4(this.a, r, this);
        this.f0.o().d(this);
    }

    public static Intent a(Context context, String str, s81 s81Var) {
        Intent intent = new Intent(context, SystemForegroundService.class);
        intent.setAction("ACTION_NOTIFY");
        intent.putExtra("KEY_NOTIFICATION_ID", s81Var.c());
        intent.putExtra("KEY_FOREGROUND_SERVICE_TYPE", s81Var.a());
        intent.putExtra("KEY_NOTIFICATION", s81Var.b());
        intent.putExtra("KEY_WORKSPEC_ID", str);
        return intent;
    }

    public static Intent d(Context context, String str, s81 s81Var) {
        Intent intent = new Intent(context, SystemForegroundService.class);
        intent.setAction("ACTION_START_FOREGROUND");
        intent.putExtra("KEY_WORKSPEC_ID", str);
        intent.putExtra("KEY_NOTIFICATION_ID", s81Var.c());
        intent.putExtra("KEY_FOREGROUND_SERVICE_TYPE", s81Var.a());
        intent.putExtra("KEY_NOTIFICATION", s81Var.b());
        intent.putExtra("KEY_WORKSPEC_ID", str);
        return intent;
    }

    public static Intent e(Context context) {
        Intent intent = new Intent(context, SystemForegroundService.class);
        intent.setAction("ACTION_STOP_FOREGROUND");
        return intent;
    }

    @Override // defpackage.vp4
    public void b(List<String> list) {
        if (list.isEmpty()) {
            return;
        }
        for (String str : list) {
            v12.c().a(o0, String.format("Constraints unmet for WorkSpec %s", str), new Throwable[0]);
            this.f0.y(str);
        }
    }

    @Override // defpackage.qy0
    public void c(String str, boolean z) {
        Map.Entry<String, s81> entry;
        synchronized (this.h0) {
            tq4 remove = this.k0.remove(str);
            if (remove != null ? this.l0.remove(remove) : false) {
                this.m0.d(this.l0);
            }
        }
        s81 remove2 = this.j0.remove(str);
        if (str.equals(this.i0) && this.j0.size() > 0) {
            Iterator<Map.Entry<String, s81>> it = this.j0.entrySet().iterator();
            Map.Entry<String, s81> next = it.next();
            while (true) {
                entry = next;
                if (!it.hasNext()) {
                    break;
                }
                next = it.next();
            }
            this.i0 = entry.getKey();
            if (this.n0 != null) {
                s81 value = entry.getValue();
                this.n0.b(value.c(), value.a(), value.b());
                this.n0.d(value.c());
            }
        }
        b bVar = this.n0;
        if (remove2 == null || bVar == null) {
            return;
        }
        v12.c().a(o0, String.format("Removing Notification (id: %s, workSpecId: %s ,notificationType: %s)", Integer.valueOf(remove2.c()), str, Integer.valueOf(remove2.a())), new Throwable[0]);
        bVar.d(remove2.c());
    }

    @Override // defpackage.vp4
    public void f(List<String> list) {
    }

    public final void g(Intent intent) {
        v12.c().d(o0, String.format("Stopping foreground work for %s", intent), new Throwable[0]);
        String stringExtra = intent.getStringExtra("KEY_WORKSPEC_ID");
        if (stringExtra == null || TextUtils.isEmpty(stringExtra)) {
            return;
        }
        this.f0.h(UUID.fromString(stringExtra));
    }

    public final void h(Intent intent) {
        int i = 0;
        int intExtra = intent.getIntExtra("KEY_NOTIFICATION_ID", 0);
        int intExtra2 = intent.getIntExtra("KEY_FOREGROUND_SERVICE_TYPE", 0);
        String stringExtra = intent.getStringExtra("KEY_WORKSPEC_ID");
        Notification notification = (Notification) intent.getParcelableExtra("KEY_NOTIFICATION");
        v12.c().a(o0, String.format("Notifying with (id: %s, workSpecId: %s, notificationType: %s)", Integer.valueOf(intExtra), stringExtra, Integer.valueOf(intExtra2)), new Throwable[0]);
        if (notification == null || this.n0 == null) {
            return;
        }
        this.j0.put(stringExtra, new s81(intExtra, notification, intExtra2));
        if (TextUtils.isEmpty(this.i0)) {
            this.i0 = stringExtra;
            this.n0.b(intExtra, intExtra2, notification);
            return;
        }
        this.n0.c(intExtra, notification);
        if (intExtra2 == 0 || Build.VERSION.SDK_INT < 29) {
            return;
        }
        for (Map.Entry<String, s81> entry : this.j0.entrySet()) {
            i |= entry.getValue().a();
        }
        s81 s81Var = this.j0.get(this.i0);
        if (s81Var != null) {
            this.n0.b(s81Var.c(), i, s81Var.b());
        }
    }

    public final void i(Intent intent) {
        v12.c().d(o0, String.format("Started foreground service %s", intent), new Throwable[0]);
        this.g0.b(new RunnableC0072a(this.f0.q(), intent.getStringExtra("KEY_WORKSPEC_ID")));
    }

    public void j(Intent intent) {
        v12.c().d(o0, "Stopping foreground service", new Throwable[0]);
        b bVar = this.n0;
        if (bVar != null) {
            bVar.stop();
        }
    }

    public void k() {
        this.n0 = null;
        synchronized (this.h0) {
            this.m0.e();
        }
        this.f0.o().i(this);
    }

    public void l(Intent intent) {
        String action = intent.getAction();
        if ("ACTION_START_FOREGROUND".equals(action)) {
            i(intent);
            h(intent);
        } else if ("ACTION_NOTIFY".equals(action)) {
            h(intent);
        } else if ("ACTION_CANCEL_WORK".equals(action)) {
            g(intent);
        } else if ("ACTION_STOP_FOREGROUND".equals(action)) {
            j(intent);
        }
    }

    public void m(b bVar) {
        if (this.n0 != null) {
            v12.c().b(o0, "A callback already exists.", new Throwable[0]);
        } else {
            this.n0 = bVar;
        }
    }
}
