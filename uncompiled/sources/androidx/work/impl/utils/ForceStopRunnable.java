package androidx.work.impl.utils;

import android.annotation.SuppressLint;
import android.app.ActivityManager;
import android.app.AlarmManager;
import android.app.ApplicationExitInfo;
import android.app.PendingIntent;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.database.sqlite.SQLiteAccessPermException;
import android.database.sqlite.SQLiteCantOpenDatabaseException;
import android.database.sqlite.SQLiteConstraintException;
import android.database.sqlite.SQLiteDatabaseCorruptException;
import android.database.sqlite.SQLiteDatabaseLockedException;
import android.database.sqlite.SQLiteTableLockedException;
import android.os.Build;
import android.text.TextUtils;
import androidx.work.WorkInfo;
import androidx.work.a;
import androidx.work.impl.WorkDatabase;
import java.util.List;
import java.util.concurrent.TimeUnit;

/* loaded from: classes.dex */
public class ForceStopRunnable implements Runnable {
    public static final String h0 = v12.f("ForceStopRunnable");
    public static final long i0 = TimeUnit.DAYS.toMillis(3650);
    public final Context a;
    public final hq4 f0;
    public int g0 = 0;

    /* loaded from: classes.dex */
    public static class BroadcastReceiver extends android.content.BroadcastReceiver {
        public static final String a = v12.f("ForceStopRunnable$Rcvr");

        @Override // android.content.BroadcastReceiver
        public void onReceive(Context context, Intent intent) {
            if (intent == null || !"ACTION_FORCE_STOP_RESCHEDULE".equals(intent.getAction())) {
                return;
            }
            v12.c().g(a, "Rescheduling alarm that keeps track of force-stops.", new Throwable[0]);
            ForceStopRunnable.g(context);
        }
    }

    public ForceStopRunnable(Context context, hq4 hq4Var) {
        this.a = context.getApplicationContext();
        this.f0 = hq4Var;
    }

    public static Intent c(Context context) {
        Intent intent = new Intent();
        intent.setComponent(new ComponentName(context, BroadcastReceiver.class));
        intent.setAction("ACTION_FORCE_STOP_RESCHEDULE");
        return intent;
    }

    public static PendingIntent d(Context context, int i) {
        return PendingIntent.getBroadcast(context, -1, c(context), i);
    }

    @SuppressLint({"ClassVerificationFailure"})
    public static void g(Context context) {
        AlarmManager alarmManager = (AlarmManager) context.getSystemService("alarm");
        PendingIntent d = d(context, yr.c() ? 167772160 : 134217728);
        long currentTimeMillis = System.currentTimeMillis() + i0;
        if (alarmManager != null) {
            if (Build.VERSION.SDK_INT >= 19) {
                alarmManager.setExact(0, currentTimeMillis, d);
            } else {
                alarmManager.set(0, currentTimeMillis, d);
            }
        }
    }

    public boolean a() {
        boolean i = Build.VERSION.SDK_INT >= 23 ? y24.i(this.a, this.f0) : false;
        WorkDatabase q = this.f0.q();
        uq4 P = q.P();
        nq4 O = q.O();
        q.e();
        try {
            List<tq4> h = P.h();
            boolean z = (h == null || h.isEmpty()) ? false : true;
            if (z) {
                for (tq4 tq4Var : h) {
                    P.a(WorkInfo.State.ENQUEUED, tq4Var.a);
                    P.b(tq4Var.a, -1L);
                }
            }
            O.a();
            q.E();
            return z || i;
        } finally {
            q.j();
        }
    }

    public void b() {
        boolean a = a();
        if (h()) {
            v12.c().a(h0, "Rescheduling Workers.", new Throwable[0]);
            this.f0.u();
            this.f0.n().c(false);
        } else if (e()) {
            v12.c().a(h0, "Application was force-stopped, rescheduling.", new Throwable[0]);
            this.f0.u();
        } else if (a) {
            v12.c().a(h0, "Found unfinished work, scheduling it.", new Throwable[0]);
            gd3.b(this.f0.k(), this.f0.q(), this.f0.p());
        }
    }

    @SuppressLint({"ClassVerificationFailure"})
    public boolean e() {
        try {
            PendingIntent d = d(this.a, yr.c() ? 570425344 : 536870912);
            if (Build.VERSION.SDK_INT >= 30) {
                if (d != null) {
                    d.cancel();
                }
                List<ApplicationExitInfo> historicalProcessExitReasons = ((ActivityManager) this.a.getSystemService("activity")).getHistoricalProcessExitReasons(null, 0, 0);
                if (historicalProcessExitReasons != null && !historicalProcessExitReasons.isEmpty()) {
                    for (int i = 0; i < historicalProcessExitReasons.size(); i++) {
                        if (historicalProcessExitReasons.get(i).getReason() == 10) {
                            return true;
                        }
                    }
                }
            } else if (d == null) {
                g(this.a);
                return true;
            }
            return false;
        } catch (IllegalArgumentException | SecurityException e) {
            v12.c().h(h0, "Ignoring exception", e);
            return true;
        }
    }

    public boolean f() {
        a k = this.f0.k();
        if (TextUtils.isEmpty(k.c())) {
            v12.c().a(h0, "The default process name was not specified.", new Throwable[0]);
            return true;
        }
        boolean b = av2.b(this.a, k);
        v12.c().a(h0, String.format("Is default app process = %s", Boolean.valueOf(b)), new Throwable[0]);
        return b;
    }

    public boolean h() {
        return this.f0.n().a();
    }

    public void i(long j) {
        try {
            Thread.sleep(j);
        } catch (InterruptedException unused) {
        }
    }

    @Override // java.lang.Runnable
    public void run() {
        int i;
        try {
            if (f()) {
                while (true) {
                    zp4.e(this.a);
                    v12.c().a(h0, "Performing cleanup operations.", new Throwable[0]);
                    try {
                        b();
                        break;
                    } catch (SQLiteAccessPermException | SQLiteCantOpenDatabaseException | SQLiteConstraintException | SQLiteDatabaseCorruptException | SQLiteDatabaseLockedException | SQLiteTableLockedException e) {
                        i = this.g0 + 1;
                        this.g0 = i;
                        if (i >= 3) {
                            v12 c = v12.c();
                            String str = h0;
                            c.b(str, "The file system on the device is in a bad state. WorkManager cannot access the app's internal data store.", e);
                            IllegalStateException illegalStateException = new IllegalStateException("The file system on the device is in a bad state. WorkManager cannot access the app's internal data store.", e);
                            qq1 d = this.f0.k().d();
                            if (d != null) {
                                v12.c().a(str, "Routing exception to the specified exception handler", illegalStateException);
                                d.a(illegalStateException);
                            } else {
                                throw illegalStateException;
                            }
                        } else {
                            v12.c().a(h0, String.format("Retrying after %s", Long.valueOf(i * 300)), e);
                            i(this.g0 * 300);
                        }
                    }
                    v12.c().a(h0, String.format("Retrying after %s", Long.valueOf(i * 300)), e);
                    i(this.g0 * 300);
                }
            }
        } finally {
            this.f0.t();
        }
    }
}
