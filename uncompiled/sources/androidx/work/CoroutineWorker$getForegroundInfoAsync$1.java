package androidx.work;

import kotlin.coroutines.jvm.internal.SuspendLambda;

/* compiled from: CoroutineWorker.kt */
@kotlin.coroutines.jvm.internal.a(c = "androidx.work.CoroutineWorker$getForegroundInfoAsync$1", f = "CoroutineWorker.kt", l = {134}, m = "invokeSuspend")
/* loaded from: classes.dex */
public final class CoroutineWorker$getForegroundInfoAsync$1 extends SuspendLambda implements hd1<c90, q70<? super te4>, Object> {
    public final /* synthetic */ JobListenableFuture<s81> $jobFuture;
    public Object L$0;
    public int label;
    public final /* synthetic */ CoroutineWorker this$0;

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public CoroutineWorker$getForegroundInfoAsync$1(JobListenableFuture<s81> jobListenableFuture, CoroutineWorker coroutineWorker, q70<? super CoroutineWorker$getForegroundInfoAsync$1> q70Var) {
        super(2, q70Var);
        this.$jobFuture = jobListenableFuture;
        this.this$0 = coroutineWorker;
    }

    @Override // kotlin.coroutines.jvm.internal.BaseContinuationImpl
    public final q70<te4> create(Object obj, q70<?> q70Var) {
        return new CoroutineWorker$getForegroundInfoAsync$1(this.$jobFuture, this.this$0, q70Var);
    }

    @Override // defpackage.hd1
    public final Object invoke(c90 c90Var, q70<? super te4> q70Var) {
        return ((CoroutineWorker$getForegroundInfoAsync$1) create(c90Var, q70Var)).invokeSuspend(te4.a);
    }

    @Override // kotlin.coroutines.jvm.internal.BaseContinuationImpl
    public final Object invokeSuspend(Object obj) {
        JobListenableFuture jobListenableFuture;
        Object d = gs1.d();
        int i = this.label;
        if (i == 0) {
            o83.b(obj);
            JobListenableFuture jobListenableFuture2 = this.$jobFuture;
            CoroutineWorker coroutineWorker = this.this$0;
            this.L$0 = jobListenableFuture2;
            this.label = 1;
            Object t = coroutineWorker.t(this);
            if (t == d) {
                return d;
            }
            jobListenableFuture = jobListenableFuture2;
            obj = t;
        } else if (i != 1) {
            throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        } else {
            jobListenableFuture = (JobListenableFuture) this.L$0;
            o83.b(obj);
        }
        jobListenableFuture.b(obj);
        return te4.a;
    }
}
