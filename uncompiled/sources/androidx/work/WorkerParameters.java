package androidx.work;

import android.net.Uri;
import java.util.Collection;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.UUID;
import java.util.concurrent.Executor;

/* loaded from: classes.dex */
public final class WorkerParameters {
    public UUID a;
    public b b;
    public Executor c;
    public q34 d;
    public br4 e;
    public u81 f;

    /* loaded from: classes.dex */
    public static class a {
        public List<String> a = Collections.emptyList();
        public List<Uri> b = Collections.emptyList();
    }

    public WorkerParameters(UUID uuid, b bVar, Collection<String> collection, a aVar, int i, Executor executor, q34 q34Var, br4 br4Var, ov2 ov2Var, u81 u81Var) {
        this.a = uuid;
        this.b = bVar;
        new HashSet(collection);
        this.c = executor;
        this.d = q34Var;
        this.e = br4Var;
        this.f = u81Var;
    }

    public Executor a() {
        return this.c;
    }

    public u81 b() {
        return this.f;
    }

    public UUID c() {
        return this.a;
    }

    public b d() {
        return this.b;
    }

    public q34 e() {
        return this.d;
    }

    public br4 f() {
        return this.e;
    }
}
