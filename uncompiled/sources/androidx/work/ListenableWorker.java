package androidx.work;

import android.annotation.SuppressLint;
import android.content.Context;
import androidx.annotation.Keep;
import java.util.UUID;
import java.util.concurrent.Executor;

/* loaded from: classes.dex */
public abstract class ListenableWorker {
    public Context a;
    public WorkerParameters f0;
    public volatile boolean g0;
    public boolean h0;
    public boolean i0;

    /* loaded from: classes.dex */
    public static abstract class a {

        /* renamed from: androidx.work.ListenableWorker$a$a  reason: collision with other inner class name */
        /* loaded from: classes.dex */
        public static final class C0068a extends a {
            public final androidx.work.b a;

            public C0068a() {
                this(androidx.work.b.c);
            }

            public androidx.work.b e() {
                return this.a;
            }

            public boolean equals(Object obj) {
                if (this == obj) {
                    return true;
                }
                if (obj == null || C0068a.class != obj.getClass()) {
                    return false;
                }
                return this.a.equals(((C0068a) obj).a);
            }

            public int hashCode() {
                return (C0068a.class.getName().hashCode() * 31) + this.a.hashCode();
            }

            public String toString() {
                return "Failure {mOutputData=" + this.a + '}';
            }

            public C0068a(androidx.work.b bVar) {
                this.a = bVar;
            }
        }

        /* loaded from: classes.dex */
        public static final class b extends a {
            public boolean equals(Object obj) {
                if (this == obj) {
                    return true;
                }
                return obj != null && b.class == obj.getClass();
            }

            public int hashCode() {
                return b.class.getName().hashCode();
            }

            public String toString() {
                return "Retry";
            }
        }

        /* loaded from: classes.dex */
        public static final class c extends a {
            public final androidx.work.b a;

            public c() {
                this(androidx.work.b.c);
            }

            public androidx.work.b e() {
                return this.a;
            }

            public boolean equals(Object obj) {
                if (this == obj) {
                    return true;
                }
                if (obj == null || c.class != obj.getClass()) {
                    return false;
                }
                return this.a.equals(((c) obj).a);
            }

            public int hashCode() {
                return (c.class.getName().hashCode() * 31) + this.a.hashCode();
            }

            public String toString() {
                return "Success {mOutputData=" + this.a + '}';
            }

            public c(androidx.work.b bVar) {
                this.a = bVar;
            }
        }

        public static a a() {
            return new C0068a();
        }

        public static a b() {
            return new b();
        }

        public static a c() {
            return new c();
        }

        public static a d(androidx.work.b bVar) {
            return new c(bVar);
        }
    }

    @Keep
    @SuppressLint({"BanKeepAnnotation"})
    public ListenableWorker(Context context, WorkerParameters workerParameters) {
        if (context == null) {
            throw new IllegalArgumentException("Application Context is null");
        }
        if (workerParameters != null) {
            this.a = context;
            this.f0 = workerParameters;
            return;
        }
        throw new IllegalArgumentException("WorkerParameters is null");
    }

    public final Context a() {
        return this.a;
    }

    public Executor c() {
        return this.f0.a();
    }

    public l02<s81> d() {
        wm3 t = wm3.t();
        t.q(new IllegalStateException("Expedited WorkRequests require a ListenableWorker to provide an implementation for `getForegroundInfoAsync()`"));
        return t;
    }

    public final UUID e() {
        return this.f0.c();
    }

    public final b g() {
        return this.f0.d();
    }

    public q34 h() {
        return this.f0.e();
    }

    public br4 i() {
        return this.f0.f();
    }

    public boolean j() {
        return this.i0;
    }

    public final boolean k() {
        return this.g0;
    }

    public final boolean l() {
        return this.h0;
    }

    public void m() {
    }

    public void n(boolean z) {
        this.i0 = z;
    }

    public final void o() {
        this.h0 = true;
    }

    public abstract l02<a> p();

    public final void q() {
        this.g0 = true;
        m();
    }
}
