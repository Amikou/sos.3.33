package androidx.work;

import android.os.Build;
import java.util.HashSet;
import java.util.Set;
import java.util.UUID;
import java.util.concurrent.TimeUnit;

/* compiled from: WorkRequest.java */
/* loaded from: classes.dex */
public abstract class d {
    public UUID a;
    public tq4 b;
    public Set<String> c;

    /* compiled from: WorkRequest.java */
    /* loaded from: classes.dex */
    public static abstract class a<B extends a<?, ?>, W extends d> {
        public tq4 c;
        public boolean a = false;
        public Set<String> d = new HashSet();
        public UUID b = UUID.randomUUID();

        public a(Class<? extends ListenableWorker> cls) {
            this.c = new tq4(this.b.toString(), cls.getName());
            a(cls.getName());
        }

        public final B a(String str) {
            this.d.add(str);
            return d();
        }

        public final W b() {
            W c = c();
            h60 h60Var = this.c.j;
            int i = Build.VERSION.SDK_INT;
            boolean z = (i >= 24 && h60Var.e()) || h60Var.f() || h60Var.g() || (i >= 23 && h60Var.h());
            tq4 tq4Var = this.c;
            if (tq4Var.q) {
                if (!z) {
                    if (tq4Var.g > 0) {
                        throw new IllegalArgumentException("Expedited jobs cannot be delayed");
                    }
                } else {
                    throw new IllegalArgumentException("Expedited jobs only support network and storage constraints");
                }
            }
            this.b = UUID.randomUUID();
            tq4 tq4Var2 = new tq4(this.c);
            this.c = tq4Var2;
            tq4Var2.a = this.b.toString();
            return c;
        }

        public abstract W c();

        public abstract B d();

        public final B e(h60 h60Var) {
            this.c.j = h60Var;
            return d();
        }

        public B f(long j, TimeUnit timeUnit) {
            this.c.g = timeUnit.toMillis(j);
            if (Long.MAX_VALUE - System.currentTimeMillis() > this.c.g) {
                return d();
            }
            throw new IllegalArgumentException("The given initial delay is too large and will cause an overflow!");
        }

        public final B g(b bVar) {
            this.c.e = bVar;
            return d();
        }
    }

    public d(UUID uuid, tq4 tq4Var, Set<String> set) {
        this.a = uuid;
        this.b = tq4Var;
        this.c = set;
    }

    public String a() {
        return this.a.toString();
    }

    public Set<String> b() {
        return this.c;
    }

    public tq4 c() {
        return this.b;
    }
}
