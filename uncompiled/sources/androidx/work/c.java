package androidx.work;

import android.os.Build;
import androidx.work.d;

/* compiled from: OneTimeWorkRequest.java */
/* loaded from: classes.dex */
public final class c extends d {

    /* compiled from: OneTimeWorkRequest.java */
    /* loaded from: classes.dex */
    public static final class a extends d.a<a, c> {
        public a(Class<? extends ListenableWorker> cls) {
            super(cls);
            this.c.d = OverwritingInputMerger.class.getName();
        }

        @Override // androidx.work.d.a
        /* renamed from: h */
        public c c() {
            if (this.a && Build.VERSION.SDK_INT >= 23 && this.c.j.h()) {
                throw new IllegalArgumentException("Cannot set backoff criteria on an idle mode job");
            }
            return new c(this);
        }

        @Override // androidx.work.d.a
        /* renamed from: i */
        public a d() {
            return this;
        }
    }

    public c(a aVar) {
        super(aVar.b, aVar.c, aVar.d);
    }

    public static c d(Class<? extends ListenableWorker> cls) {
        return new a(cls).b();
    }
}
