package androidx.work;

import android.content.Context;
import androidx.work.a;
import java.util.Collections;
import java.util.List;

/* loaded from: classes.dex */
public final class WorkManagerInitializer implements rq1<gq4> {
    public static final String a = v12.f("WrkMgrInitializer");

    @Override // defpackage.rq1
    public List<Class<? extends rq1<?>>> a() {
        return Collections.emptyList();
    }

    @Override // defpackage.rq1
    /* renamed from: c */
    public gq4 b(Context context) {
        v12.c().a(a, "Initializing WorkManager with default configuration.", new Throwable[0]);
        gq4.g(context, new a.b().a());
        return gq4.f(context);
    }
}
