package androidx.constraintlayout.core;

import androidx.constraintlayout.core.b;
import com.fasterxml.jackson.core.util.MinimalPrettyPrinter;
import com.github.mikephil.charting.utils.Utils;
import java.util.Arrays;

/* compiled from: SolverVariableValues.java */
/* loaded from: classes.dex */
public class e implements b.a {
    public static float m = 0.001f;
    public int a = 16;
    public int b = 16;
    public int[] c = new int[16];
    public int[] d = new int[16];
    public int[] e = new int[16];
    public float[] f = new float[16];
    public int[] g = new int[16];
    public int[] h = new int[16];
    public int i = 0;
    public int j = -1;
    public final b k;
    public final ut l;

    public e(b bVar, ut utVar) {
        this.k = bVar;
        this.l = utVar;
        clear();
    }

    @Override // androidx.constraintlayout.core.b.a
    public int a() {
        return this.i;
    }

    @Override // androidx.constraintlayout.core.b.a
    public boolean b(SolverVariable solverVariable) {
        return p(solverVariable) != -1;
    }

    @Override // androidx.constraintlayout.core.b.a
    public float c(b bVar, boolean z) {
        float j = j(bVar.a);
        i(bVar.a, z);
        e eVar = (e) bVar.e;
        int a = eVar.a();
        int i = 0;
        int i2 = 0;
        while (i < a) {
            int[] iArr = eVar.e;
            if (iArr[i2] != -1) {
                f(this.l.d[iArr[i2]], eVar.f[i2] * j, z);
                i++;
            }
            i2++;
        }
        return j;
    }

    @Override // androidx.constraintlayout.core.b.a
    public void clear() {
        int i = this.i;
        for (int i2 = 0; i2 < i; i2++) {
            SolverVariable e = e(i2);
            if (e != null) {
                e.f(this.k);
            }
        }
        for (int i3 = 0; i3 < this.a; i3++) {
            this.e[i3] = -1;
            this.d[i3] = -1;
        }
        for (int i4 = 0; i4 < this.b; i4++) {
            this.c[i4] = -1;
        }
        this.i = 0;
        this.j = -1;
    }

    @Override // androidx.constraintlayout.core.b.a
    public void d(SolverVariable solverVariable, float f) {
        float f2 = m;
        if (f > (-f2) && f < f2) {
            i(solverVariable, true);
            return;
        }
        if (this.i == 0) {
            m(0, solverVariable, f);
            l(solverVariable, 0);
            this.j = 0;
            return;
        }
        int p = p(solverVariable);
        if (p != -1) {
            this.f[p] = f;
            return;
        }
        if (this.i + 1 >= this.a) {
            o();
        }
        int i = this.i;
        int i2 = this.j;
        int i3 = -1;
        for (int i4 = 0; i4 < i; i4++) {
            int[] iArr = this.e;
            int i5 = iArr[i2];
            int i6 = solverVariable.g0;
            if (i5 == i6) {
                this.f[i2] = f;
                return;
            }
            if (iArr[i2] < i6) {
                i3 = i2;
            }
            i2 = this.h[i2];
            if (i2 == -1) {
                break;
            }
        }
        q(i3, solverVariable, f);
    }

    @Override // androidx.constraintlayout.core.b.a
    public SolverVariable e(int i) {
        int i2 = this.i;
        if (i2 == 0) {
            return null;
        }
        int i3 = this.j;
        for (int i4 = 0; i4 < i2; i4++) {
            if (i4 == i && i3 != -1) {
                return this.l.d[this.e[i3]];
            }
            i3 = this.h[i3];
            if (i3 == -1) {
                break;
            }
        }
        return null;
    }

    @Override // androidx.constraintlayout.core.b.a
    public void f(SolverVariable solverVariable, float f, boolean z) {
        float f2 = m;
        if (f <= (-f2) || f >= f2) {
            int p = p(solverVariable);
            if (p == -1) {
                d(solverVariable, f);
                return;
            }
            float[] fArr = this.f;
            fArr[p] = fArr[p] + f;
            float f3 = fArr[p];
            float f4 = m;
            if (f3 <= (-f4) || fArr[p] >= f4) {
                return;
            }
            fArr[p] = 0.0f;
            i(solverVariable, z);
        }
    }

    @Override // androidx.constraintlayout.core.b.a
    public void g() {
        int i = this.i;
        int i2 = this.j;
        for (int i3 = 0; i3 < i; i3++) {
            float[] fArr = this.f;
            fArr[i2] = fArr[i2] * (-1.0f);
            i2 = this.h[i2];
            if (i2 == -1) {
                return;
            }
        }
    }

    @Override // androidx.constraintlayout.core.b.a
    public float h(int i) {
        int i2 = this.i;
        int i3 = this.j;
        for (int i4 = 0; i4 < i2; i4++) {
            if (i4 == i) {
                return this.f[i3];
            }
            i3 = this.h[i3];
            if (i3 == -1) {
                return Utils.FLOAT_EPSILON;
            }
        }
        return Utils.FLOAT_EPSILON;
    }

    @Override // androidx.constraintlayout.core.b.a
    public float i(SolverVariable solverVariable, boolean z) {
        int p = p(solverVariable);
        if (p == -1) {
            return Utils.FLOAT_EPSILON;
        }
        r(solverVariable);
        float f = this.f[p];
        if (this.j == p) {
            this.j = this.h[p];
        }
        this.e[p] = -1;
        int[] iArr = this.g;
        if (iArr[p] != -1) {
            int[] iArr2 = this.h;
            iArr2[iArr[p]] = iArr2[p];
        }
        int[] iArr3 = this.h;
        if (iArr3[p] != -1) {
            iArr[iArr3[p]] = iArr[p];
        }
        this.i--;
        solverVariable.q0--;
        if (z) {
            solverVariable.f(this.k);
        }
        return f;
    }

    @Override // androidx.constraintlayout.core.b.a
    public float j(SolverVariable solverVariable) {
        int p = p(solverVariable);
        return p != -1 ? this.f[p] : Utils.FLOAT_EPSILON;
    }

    @Override // androidx.constraintlayout.core.b.a
    public void k(float f) {
        int i = this.i;
        int i2 = this.j;
        for (int i3 = 0; i3 < i; i3++) {
            float[] fArr = this.f;
            fArr[i2] = fArr[i2] / f;
            i2 = this.h[i2];
            if (i2 == -1) {
                return;
            }
        }
    }

    public final void l(SolverVariable solverVariable, int i) {
        int[] iArr;
        int i2 = solverVariable.g0 % this.b;
        int[] iArr2 = this.c;
        int i3 = iArr2[i2];
        if (i3 == -1) {
            iArr2[i2] = i;
        } else {
            while (true) {
                iArr = this.d;
                if (iArr[i3] == -1) {
                    break;
                }
                i3 = iArr[i3];
            }
            iArr[i3] = i;
        }
        this.d[i] = -1;
    }

    public final void m(int i, SolverVariable solverVariable, float f) {
        this.e[i] = solverVariable.g0;
        this.f[i] = f;
        this.g[i] = -1;
        this.h[i] = -1;
        solverVariable.a(this.k);
        solverVariable.q0++;
        this.i++;
    }

    public final int n() {
        for (int i = 0; i < this.a; i++) {
            if (this.e[i] == -1) {
                return i;
            }
        }
        return -1;
    }

    public final void o() {
        int i = this.a * 2;
        this.e = Arrays.copyOf(this.e, i);
        this.f = Arrays.copyOf(this.f, i);
        this.g = Arrays.copyOf(this.g, i);
        this.h = Arrays.copyOf(this.h, i);
        this.d = Arrays.copyOf(this.d, i);
        for (int i2 = this.a; i2 < i; i2++) {
            this.e[i2] = -1;
            this.d[i2] = -1;
        }
        this.a = i;
    }

    public int p(SolverVariable solverVariable) {
        int[] iArr;
        if (this.i != 0 && solverVariable != null) {
            int i = solverVariable.g0;
            int i2 = this.c[i % this.b];
            if (i2 == -1) {
                return -1;
            }
            if (this.e[i2] == i) {
                return i2;
            }
            while (true) {
                iArr = this.d;
                if (iArr[i2] == -1 || this.e[iArr[i2]] == i) {
                    break;
                }
                i2 = iArr[i2];
            }
            if (iArr[i2] != -1 && this.e[iArr[i2]] == i) {
                return iArr[i2];
            }
        }
        return -1;
    }

    public final void q(int i, SolverVariable solverVariable, float f) {
        int n = n();
        m(n, solverVariable, f);
        if (i != -1) {
            this.g[n] = i;
            int[] iArr = this.h;
            iArr[n] = iArr[i];
            iArr[i] = n;
        } else {
            this.g[n] = -1;
            if (this.i > 0) {
                this.h[n] = this.j;
                this.j = n;
            } else {
                this.h[n] = -1;
            }
        }
        int[] iArr2 = this.h;
        if (iArr2[n] != -1) {
            this.g[iArr2[n]] = n;
        }
        l(solverVariable, n);
    }

    public final void r(SolverVariable solverVariable) {
        int[] iArr;
        int i = solverVariable.g0;
        int i2 = i % this.b;
        int[] iArr2 = this.c;
        int i3 = iArr2[i2];
        if (i3 == -1) {
            return;
        }
        if (this.e[i3] == i) {
            int[] iArr3 = this.d;
            iArr2[i2] = iArr3[i3];
            iArr3[i3] = -1;
            return;
        }
        while (true) {
            iArr = this.d;
            if (iArr[i3] == -1 || this.e[iArr[i3]] == i) {
                break;
            }
            i3 = iArr[i3];
        }
        int i4 = iArr[i3];
        if (i4 == -1 || this.e[i4] != i) {
            return;
        }
        iArr[i3] = iArr[i4];
        iArr[i4] = -1;
    }

    public String toString() {
        String str = hashCode() + " { ";
        int i = this.i;
        for (int i2 = 0; i2 < i; i2++) {
            SolverVariable e = e(i2);
            if (e != null) {
                String str2 = str + e + " = " + h(i2) + MinimalPrettyPrinter.DEFAULT_ROOT_VALUE_SEPARATOR;
                int p = p(e);
                String str3 = str2 + "[p: ";
                String str4 = (this.g[p] != -1 ? str3 + this.l.d[this.e[this.g[p]]] : str3 + "none") + ", n: ";
                str = (this.h[p] != -1 ? str4 + this.l.d[this.e[this.h[p]]] : str4 + "none") + "]";
            }
        }
        return str + " }";
    }
}
