package androidx.constraintlayout.core.widgets;

import androidx.constraintlayout.core.widgets.ConstraintWidget;
import com.github.mikephil.charting.utils.Utils;
import java.util.ArrayList;

/* compiled from: ChainHead.java */
/* loaded from: classes.dex */
public class c {
    public ConstraintWidget a;
    public ConstraintWidget b;
    public ConstraintWidget c;
    public ConstraintWidget d;
    public ConstraintWidget e;
    public ConstraintWidget f;
    public ConstraintWidget g;
    public ArrayList<ConstraintWidget> h;
    public int i;
    public int j;
    public float k = Utils.FLOAT_EPSILON;
    public int l;
    public int m;
    public int n;
    public int o;
    public boolean p;
    public boolean q;
    public boolean r;
    public boolean s;
    public boolean t;

    public c(ConstraintWidget constraintWidget, int i, boolean z) {
        this.p = false;
        this.a = constraintWidget;
        this.o = i;
        this.p = z;
    }

    public static boolean c(ConstraintWidget constraintWidget, int i) {
        if (constraintWidget.U() != 8 && constraintWidget.X[i] == ConstraintWidget.DimensionBehaviour.MATCH_CONSTRAINT) {
            int[] iArr = constraintWidget.u;
            if (iArr[i] == 0 || iArr[i] == 3) {
                return true;
            }
        }
        return false;
    }

    public void a() {
        if (!this.t) {
            b();
        }
        this.t = true;
    }

    public final void b() {
        int i = this.o * 2;
        ConstraintWidget constraintWidget = this.a;
        boolean z = false;
        ConstraintWidget constraintWidget2 = constraintWidget;
        boolean z2 = false;
        while (!z2) {
            this.i++;
            ConstraintWidget[] constraintWidgetArr = constraintWidget.J0;
            int i2 = this.o;
            ConstraintWidget constraintWidget3 = null;
            constraintWidgetArr[i2] = null;
            constraintWidget.I0[i2] = null;
            if (constraintWidget.U() != 8) {
                this.l++;
                ConstraintWidget.DimensionBehaviour w = constraintWidget.w(this.o);
                ConstraintWidget.DimensionBehaviour dimensionBehaviour = ConstraintWidget.DimensionBehaviour.MATCH_CONSTRAINT;
                if (w != dimensionBehaviour) {
                    this.m += constraintWidget.G(this.o);
                }
                int f = this.m + constraintWidget.U[i].f();
                this.m = f;
                int i3 = i + 1;
                this.m = f + constraintWidget.U[i3].f();
                int f2 = this.n + constraintWidget.U[i].f();
                this.n = f2;
                this.n = f2 + constraintWidget.U[i3].f();
                if (this.b == null) {
                    this.b = constraintWidget;
                }
                this.d = constraintWidget;
                ConstraintWidget.DimensionBehaviour[] dimensionBehaviourArr = constraintWidget.X;
                int i4 = this.o;
                if (dimensionBehaviourArr[i4] == dimensionBehaviour) {
                    int[] iArr = constraintWidget.u;
                    if (iArr[i4] == 0 || iArr[i4] == 3 || iArr[i4] == 2) {
                        this.j++;
                        float[] fArr = constraintWidget.H0;
                        float f3 = fArr[i4];
                        if (f3 > Utils.FLOAT_EPSILON) {
                            this.k += fArr[i4];
                        }
                        if (c(constraintWidget, i4)) {
                            if (f3 < Utils.FLOAT_EPSILON) {
                                this.q = true;
                            } else {
                                this.r = true;
                            }
                            if (this.h == null) {
                                this.h = new ArrayList<>();
                            }
                            this.h.add(constraintWidget);
                        }
                        if (this.f == null) {
                            this.f = constraintWidget;
                        }
                        ConstraintWidget constraintWidget4 = this.g;
                        if (constraintWidget4 != null) {
                            constraintWidget4.I0[this.o] = constraintWidget;
                        }
                        this.g = constraintWidget;
                    }
                    if (this.o == 0) {
                        if (constraintWidget.s == 0 && constraintWidget.v == 0) {
                            int i5 = constraintWidget.w;
                        }
                    } else if (constraintWidget.t == 0 && constraintWidget.y == 0) {
                        int i6 = constraintWidget.z;
                    }
                }
            }
            if (constraintWidget2 != constraintWidget) {
                constraintWidget2.J0[this.o] = constraintWidget;
            }
            ConstraintAnchor constraintAnchor = constraintWidget.U[i + 1].f;
            if (constraintAnchor != null) {
                ConstraintWidget constraintWidget5 = constraintAnchor.d;
                ConstraintAnchor[] constraintAnchorArr = constraintWidget5.U;
                if (constraintAnchorArr[i].f != null && constraintAnchorArr[i].f.d == constraintWidget) {
                    constraintWidget3 = constraintWidget5;
                }
            }
            if (constraintWidget3 == null) {
                constraintWidget3 = constraintWidget;
                z2 = true;
            }
            constraintWidget2 = constraintWidget;
            constraintWidget = constraintWidget3;
        }
        ConstraintWidget constraintWidget6 = this.b;
        if (constraintWidget6 != null) {
            this.m -= constraintWidget6.U[i].f();
        }
        ConstraintWidget constraintWidget7 = this.d;
        if (constraintWidget7 != null) {
            this.m -= constraintWidget7.U[i + 1].f();
        }
        this.c = constraintWidget;
        if (this.o == 0 && this.p) {
            this.e = constraintWidget;
        } else {
            this.e = this.a;
        }
        if (this.r && this.q) {
            z = true;
        }
        this.s = z;
    }
}
