package androidx.constraintlayout.core.widgets.analyzer;

import androidx.constraintlayout.core.widgets.ConstraintAnchor;
import androidx.constraintlayout.core.widgets.ConstraintWidget;
import androidx.constraintlayout.core.widgets.analyzer.DependencyNode;
import androidx.constraintlayout.core.widgets.analyzer.WidgetRun;
import com.github.mikephil.charting.utils.Utils;

/* compiled from: VerticalWidgetRun.java */
/* loaded from: classes.dex */
public class d extends WidgetRun {
    public DependencyNode k;
    public androidx.constraintlayout.core.widgets.analyzer.a l;

    /* compiled from: VerticalWidgetRun.java */
    /* loaded from: classes.dex */
    public static /* synthetic */ class a {
        public static final /* synthetic */ int[] a;

        static {
            int[] iArr = new int[WidgetRun.RunType.values().length];
            a = iArr;
            try {
                iArr[WidgetRun.RunType.START.ordinal()] = 1;
            } catch (NoSuchFieldError unused) {
            }
            try {
                a[WidgetRun.RunType.END.ordinal()] = 2;
            } catch (NoSuchFieldError unused2) {
            }
            try {
                a[WidgetRun.RunType.CENTER.ordinal()] = 3;
            } catch (NoSuchFieldError unused3) {
            }
        }
    }

    public d(ConstraintWidget constraintWidget) {
        super(constraintWidget);
        DependencyNode dependencyNode = new DependencyNode(this);
        this.k = dependencyNode;
        this.l = null;
        this.h.e = DependencyNode.Type.TOP;
        this.i.e = DependencyNode.Type.BOTTOM;
        dependencyNode.e = DependencyNode.Type.BASELINE;
        this.f = 1;
    }

    @Override // androidx.constraintlayout.core.widgets.analyzer.WidgetRun, defpackage.im0
    public void a(im0 im0Var) {
        androidx.constraintlayout.core.widgets.analyzer.a aVar;
        float f;
        float x;
        float f2;
        int i;
        ConstraintWidget constraintWidget;
        int i2 = a.a[this.j.ordinal()];
        if (i2 == 1) {
            p(im0Var);
        } else if (i2 == 2) {
            o(im0Var);
        } else if (i2 == 3) {
            ConstraintWidget constraintWidget2 = this.b;
            n(im0Var, constraintWidget2.N, constraintWidget2.P, 1);
            return;
        }
        androidx.constraintlayout.core.widgets.analyzer.a aVar2 = this.e;
        if (aVar2.c && !aVar2.j && this.d == ConstraintWidget.DimensionBehaviour.MATCH_CONSTRAINT) {
            ConstraintWidget constraintWidget3 = this.b;
            int i3 = constraintWidget3.t;
            if (i3 != 2) {
                if (i3 == 3 && constraintWidget3.d.e.j) {
                    int y = constraintWidget3.y();
                    if (y == -1) {
                        ConstraintWidget constraintWidget4 = this.b;
                        f = constraintWidget4.d.e.g;
                        x = constraintWidget4.x();
                    } else if (y == 0) {
                        f2 = constraintWidget.d.e.g * this.b.x();
                        i = (int) (f2 + 0.5f);
                        this.e.d(i);
                    } else if (y == 1) {
                        ConstraintWidget constraintWidget5 = this.b;
                        f = constraintWidget5.d.e.g;
                        x = constraintWidget5.x();
                    } else {
                        i = 0;
                        this.e.d(i);
                    }
                    f2 = f / x;
                    i = (int) (f2 + 0.5f);
                    this.e.d(i);
                }
            } else {
                ConstraintWidget M = constraintWidget3.M();
                if (M != null) {
                    if (M.e.e.j) {
                        this.e.d((int) ((aVar.g * this.b.A) + 0.5f));
                    }
                }
            }
        }
        DependencyNode dependencyNode = this.h;
        if (dependencyNode.c) {
            DependencyNode dependencyNode2 = this.i;
            if (dependencyNode2.c) {
                if (dependencyNode.j && dependencyNode2.j && this.e.j) {
                    return;
                }
                if (!this.e.j && this.d == ConstraintWidget.DimensionBehaviour.MATCH_CONSTRAINT) {
                    ConstraintWidget constraintWidget6 = this.b;
                    if (constraintWidget6.s == 0 && !constraintWidget6.j0()) {
                        int i4 = this.h.l.get(0).g;
                        DependencyNode dependencyNode3 = this.h;
                        int i5 = i4 + dependencyNode3.f;
                        int i6 = this.i.l.get(0).g + this.i.f;
                        dependencyNode3.d(i5);
                        this.i.d(i6);
                        this.e.d(i6 - i5);
                        return;
                    }
                }
                if (!this.e.j && this.d == ConstraintWidget.DimensionBehaviour.MATCH_CONSTRAINT && this.a == 1 && this.h.l.size() > 0 && this.i.l.size() > 0) {
                    int i7 = (this.i.l.get(0).g + this.i.f) - (this.h.l.get(0).g + this.h.f);
                    androidx.constraintlayout.core.widgets.analyzer.a aVar3 = this.e;
                    int i8 = aVar3.m;
                    if (i7 < i8) {
                        aVar3.d(i7);
                    } else {
                        aVar3.d(i8);
                    }
                }
                if (this.e.j && this.h.l.size() > 0 && this.i.l.size() > 0) {
                    DependencyNode dependencyNode4 = this.h.l.get(0);
                    DependencyNode dependencyNode5 = this.i.l.get(0);
                    int i9 = dependencyNode4.g + this.h.f;
                    int i10 = dependencyNode5.g + this.i.f;
                    float Q = this.b.Q();
                    if (dependencyNode4 == dependencyNode5) {
                        i9 = dependencyNode4.g;
                        i10 = dependencyNode5.g;
                        Q = 0.5f;
                    }
                    this.h.d((int) (i9 + 0.5f + (((i10 - i9) - this.e.g) * Q)));
                    this.i.d(this.h.g + this.e.g);
                }
            }
        }
    }

    @Override // androidx.constraintlayout.core.widgets.analyzer.WidgetRun
    public void d() {
        ConstraintWidget M;
        ConstraintWidget M2;
        ConstraintWidget constraintWidget = this.b;
        if (constraintWidget.a) {
            this.e.d(constraintWidget.z());
        }
        if (!this.e.j) {
            this.d = this.b.S();
            if (this.b.Y()) {
                this.l = new eo(this);
            }
            ConstraintWidget.DimensionBehaviour dimensionBehaviour = this.d;
            if (dimensionBehaviour != ConstraintWidget.DimensionBehaviour.MATCH_CONSTRAINT) {
                if (dimensionBehaviour == ConstraintWidget.DimensionBehaviour.MATCH_PARENT && (M2 = this.b.M()) != null && M2.S() == ConstraintWidget.DimensionBehaviour.FIXED) {
                    int z = (M2.z() - this.b.N.f()) - this.b.P.f();
                    b(this.h, M2.e.h, this.b.N.f());
                    b(this.i, M2.e.i, -this.b.P.f());
                    this.e.d(z);
                    return;
                } else if (this.d == ConstraintWidget.DimensionBehaviour.FIXED) {
                    this.e.d(this.b.z());
                }
            }
        } else if (this.d == ConstraintWidget.DimensionBehaviour.MATCH_PARENT && (M = this.b.M()) != null && M.S() == ConstraintWidget.DimensionBehaviour.FIXED) {
            b(this.h, M.e.h, this.b.N.f());
            b(this.i, M.e.i, -this.b.P.f());
            return;
        }
        androidx.constraintlayout.core.widgets.analyzer.a aVar = this.e;
        boolean z2 = aVar.j;
        if (z2) {
            ConstraintWidget constraintWidget2 = this.b;
            if (constraintWidget2.a) {
                ConstraintAnchor[] constraintAnchorArr = constraintWidget2.U;
                if (constraintAnchorArr[2].f != null && constraintAnchorArr[3].f != null) {
                    if (constraintWidget2.j0()) {
                        this.h.f = this.b.U[2].f();
                        this.i.f = -this.b.U[3].f();
                    } else {
                        DependencyNode h = h(this.b.U[2]);
                        if (h != null) {
                            b(this.h, h, this.b.U[2].f());
                        }
                        DependencyNode h2 = h(this.b.U[3]);
                        if (h2 != null) {
                            b(this.i, h2, -this.b.U[3].f());
                        }
                        this.h.b = true;
                        this.i.b = true;
                    }
                    if (this.b.Y()) {
                        b(this.k, this.h, this.b.r());
                        return;
                    }
                    return;
                } else if (constraintAnchorArr[2].f != null) {
                    DependencyNode h3 = h(constraintAnchorArr[2]);
                    if (h3 != null) {
                        b(this.h, h3, this.b.U[2].f());
                        b(this.i, this.h, this.e.g);
                        if (this.b.Y()) {
                            b(this.k, this.h, this.b.r());
                            return;
                        }
                        return;
                    }
                    return;
                } else if (constraintAnchorArr[3].f != null) {
                    DependencyNode h4 = h(constraintAnchorArr[3]);
                    if (h4 != null) {
                        b(this.i, h4, -this.b.U[3].f());
                        b(this.h, this.i, -this.e.g);
                    }
                    if (this.b.Y()) {
                        b(this.k, this.h, this.b.r());
                        return;
                    }
                    return;
                } else if (constraintAnchorArr[4].f != null) {
                    DependencyNode h5 = h(constraintAnchorArr[4]);
                    if (h5 != null) {
                        b(this.k, h5, 0);
                        b(this.h, this.k, -this.b.r());
                        b(this.i, this.h, this.e.g);
                        return;
                    }
                    return;
                } else if ((constraintWidget2 instanceof lk1) || constraintWidget2.M() == null || this.b.q(ConstraintAnchor.Type.CENTER).f != null) {
                    return;
                } else {
                    b(this.h, this.b.M().e.h, this.b.X());
                    b(this.i, this.h, this.e.g);
                    if (this.b.Y()) {
                        b(this.k, this.h, this.b.r());
                        return;
                    }
                    return;
                }
            }
        }
        if (!z2 && this.d == ConstraintWidget.DimensionBehaviour.MATCH_CONSTRAINT) {
            ConstraintWidget constraintWidget3 = this.b;
            int i = constraintWidget3.t;
            if (i != 2) {
                if (i == 3 && !constraintWidget3.j0()) {
                    ConstraintWidget constraintWidget4 = this.b;
                    if (constraintWidget4.s != 3) {
                        androidx.constraintlayout.core.widgets.analyzer.a aVar2 = constraintWidget4.d.e;
                        this.e.l.add(aVar2);
                        aVar2.k.add(this.e);
                        androidx.constraintlayout.core.widgets.analyzer.a aVar3 = this.e;
                        aVar3.b = true;
                        aVar3.k.add(this.h);
                        this.e.k.add(this.i);
                    }
                }
            } else {
                ConstraintWidget M3 = constraintWidget3.M();
                if (M3 != null) {
                    androidx.constraintlayout.core.widgets.analyzer.a aVar4 = M3.e.e;
                    this.e.l.add(aVar4);
                    aVar4.k.add(this.e);
                    androidx.constraintlayout.core.widgets.analyzer.a aVar5 = this.e;
                    aVar5.b = true;
                    aVar5.k.add(this.h);
                    this.e.k.add(this.i);
                }
            }
        } else {
            aVar.b(this);
        }
        ConstraintWidget constraintWidget5 = this.b;
        ConstraintAnchor[] constraintAnchorArr2 = constraintWidget5.U;
        if (constraintAnchorArr2[2].f != null && constraintAnchorArr2[3].f != null) {
            if (constraintWidget5.j0()) {
                this.h.f = this.b.U[2].f();
                this.i.f = -this.b.U[3].f();
            } else {
                DependencyNode h6 = h(this.b.U[2]);
                DependencyNode h7 = h(this.b.U[3]);
                if (h6 != null) {
                    h6.b(this);
                }
                if (h7 != null) {
                    h7.b(this);
                }
                this.j = WidgetRun.RunType.CENTER;
            }
            if (this.b.Y()) {
                c(this.k, this.h, 1, this.l);
            }
        } else if (constraintAnchorArr2[2].f != null) {
            DependencyNode h8 = h(constraintAnchorArr2[2]);
            if (h8 != null) {
                b(this.h, h8, this.b.U[2].f());
                c(this.i, this.h, 1, this.e);
                if (this.b.Y()) {
                    c(this.k, this.h, 1, this.l);
                }
                ConstraintWidget.DimensionBehaviour dimensionBehaviour2 = this.d;
                ConstraintWidget.DimensionBehaviour dimensionBehaviour3 = ConstraintWidget.DimensionBehaviour.MATCH_CONSTRAINT;
                if (dimensionBehaviour2 == dimensionBehaviour3 && this.b.x() > Utils.FLOAT_EPSILON) {
                    c cVar = this.b.d;
                    if (cVar.d == dimensionBehaviour3) {
                        cVar.e.k.add(this.e);
                        this.e.l.add(this.b.d.e);
                        this.e.a = this;
                    }
                }
            }
        } else if (constraintAnchorArr2[3].f != null) {
            DependencyNode h9 = h(constraintAnchorArr2[3]);
            if (h9 != null) {
                b(this.i, h9, -this.b.U[3].f());
                c(this.h, this.i, -1, this.e);
                if (this.b.Y()) {
                    c(this.k, this.h, 1, this.l);
                }
            }
        } else if (constraintAnchorArr2[4].f != null) {
            DependencyNode h10 = h(constraintAnchorArr2[4]);
            if (h10 != null) {
                b(this.k, h10, 0);
                c(this.h, this.k, -1, this.l);
                c(this.i, this.h, 1, this.e);
            }
        } else if (!(constraintWidget5 instanceof lk1) && constraintWidget5.M() != null) {
            b(this.h, this.b.M().e.h, this.b.X());
            c(this.i, this.h, 1, this.e);
            if (this.b.Y()) {
                c(this.k, this.h, 1, this.l);
            }
            ConstraintWidget.DimensionBehaviour dimensionBehaviour4 = this.d;
            ConstraintWidget.DimensionBehaviour dimensionBehaviour5 = ConstraintWidget.DimensionBehaviour.MATCH_CONSTRAINT;
            if (dimensionBehaviour4 == dimensionBehaviour5 && this.b.x() > Utils.FLOAT_EPSILON) {
                c cVar2 = this.b.d;
                if (cVar2.d == dimensionBehaviour5) {
                    cVar2.e.k.add(this.e);
                    this.e.l.add(this.b.d.e);
                    this.e.a = this;
                }
            }
        }
        if (this.e.l.size() == 0) {
            this.e.c = true;
        }
    }

    @Override // androidx.constraintlayout.core.widgets.analyzer.WidgetRun
    public void e() {
        DependencyNode dependencyNode = this.h;
        if (dependencyNode.j) {
            this.b.k1(dependencyNode.g);
        }
    }

    @Override // androidx.constraintlayout.core.widgets.analyzer.WidgetRun
    public void f() {
        this.c = null;
        this.h.c();
        this.i.c();
        this.k.c();
        this.e.c();
        this.g = false;
    }

    @Override // androidx.constraintlayout.core.widgets.analyzer.WidgetRun
    public boolean m() {
        return this.d != ConstraintWidget.DimensionBehaviour.MATCH_CONSTRAINT || this.b.t == 0;
    }

    public void q() {
        this.g = false;
        this.h.c();
        this.h.j = false;
        this.i.c();
        this.i.j = false;
        this.k.c();
        this.k.j = false;
        this.e.j = false;
    }

    public String toString() {
        return "VerticalRun " + this.b.v();
    }
}
