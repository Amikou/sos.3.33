package androidx.constraintlayout.core.widgets.analyzer;

import androidx.constraintlayout.core.widgets.ConstraintWidget;
import androidx.constraintlayout.core.widgets.analyzer.DependencyNode;

/* compiled from: HelperReferences.java */
/* loaded from: classes.dex */
public class b extends WidgetRun {
    public b(ConstraintWidget constraintWidget) {
        super(constraintWidget);
    }

    @Override // androidx.constraintlayout.core.widgets.analyzer.WidgetRun, defpackage.im0
    public void a(im0 im0Var) {
        androidx.constraintlayout.core.widgets.a aVar = (androidx.constraintlayout.core.widgets.a) this.b;
        int s1 = aVar.s1();
        int i = 0;
        int i2 = -1;
        for (DependencyNode dependencyNode : this.h.l) {
            int i3 = dependencyNode.g;
            if (i2 == -1 || i3 < i2) {
                i2 = i3;
            }
            if (i < i3) {
                i = i3;
            }
        }
        if (s1 != 0 && s1 != 2) {
            this.h.d(i + aVar.t1());
        } else {
            this.h.d(i2 + aVar.t1());
        }
    }

    @Override // androidx.constraintlayout.core.widgets.analyzer.WidgetRun
    public void d() {
        ConstraintWidget constraintWidget = this.b;
        if (constraintWidget instanceof androidx.constraintlayout.core.widgets.a) {
            this.h.b = true;
            androidx.constraintlayout.core.widgets.a aVar = (androidx.constraintlayout.core.widgets.a) constraintWidget;
            int s1 = aVar.s1();
            boolean r1 = aVar.r1();
            int i = 0;
            if (s1 == 0) {
                this.h.e = DependencyNode.Type.LEFT;
                while (i < aVar.Q0) {
                    ConstraintWidget constraintWidget2 = aVar.P0[i];
                    if (r1 || constraintWidget2.U() != 8) {
                        DependencyNode dependencyNode = constraintWidget2.d.h;
                        dependencyNode.k.add(this.h);
                        this.h.l.add(dependencyNode);
                    }
                    i++;
                }
                q(this.b.d.h);
                q(this.b.d.i);
            } else if (s1 == 1) {
                this.h.e = DependencyNode.Type.RIGHT;
                while (i < aVar.Q0) {
                    ConstraintWidget constraintWidget3 = aVar.P0[i];
                    if (r1 || constraintWidget3.U() != 8) {
                        DependencyNode dependencyNode2 = constraintWidget3.d.i;
                        dependencyNode2.k.add(this.h);
                        this.h.l.add(dependencyNode2);
                    }
                    i++;
                }
                q(this.b.d.h);
                q(this.b.d.i);
            } else if (s1 == 2) {
                this.h.e = DependencyNode.Type.TOP;
                while (i < aVar.Q0) {
                    ConstraintWidget constraintWidget4 = aVar.P0[i];
                    if (r1 || constraintWidget4.U() != 8) {
                        DependencyNode dependencyNode3 = constraintWidget4.e.h;
                        dependencyNode3.k.add(this.h);
                        this.h.l.add(dependencyNode3);
                    }
                    i++;
                }
                q(this.b.e.h);
                q(this.b.e.i);
            } else if (s1 != 3) {
            } else {
                this.h.e = DependencyNode.Type.BOTTOM;
                while (i < aVar.Q0) {
                    ConstraintWidget constraintWidget5 = aVar.P0[i];
                    if (r1 || constraintWidget5.U() != 8) {
                        DependencyNode dependencyNode4 = constraintWidget5.e.i;
                        dependencyNode4.k.add(this.h);
                        this.h.l.add(dependencyNode4);
                    }
                    i++;
                }
                q(this.b.e.h);
                q(this.b.e.i);
            }
        }
    }

    @Override // androidx.constraintlayout.core.widgets.analyzer.WidgetRun
    public void e() {
        ConstraintWidget constraintWidget = this.b;
        if (constraintWidget instanceof androidx.constraintlayout.core.widgets.a) {
            int s1 = ((androidx.constraintlayout.core.widgets.a) constraintWidget).s1();
            if (s1 != 0 && s1 != 1) {
                this.b.k1(this.h.g);
            } else {
                this.b.j1(this.h.g);
            }
        }
    }

    @Override // androidx.constraintlayout.core.widgets.analyzer.WidgetRun
    public void f() {
        this.c = null;
        this.h.c();
    }

    @Override // androidx.constraintlayout.core.widgets.analyzer.WidgetRun
    public boolean m() {
        return false;
    }

    public final void q(DependencyNode dependencyNode) {
        this.h.k.add(dependencyNode);
        dependencyNode.l.add(this.h);
    }
}
