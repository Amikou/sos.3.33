package androidx.constraintlayout.core.widgets.analyzer;

import java.util.ArrayList;
import java.util.List;

/* loaded from: classes.dex */
public class DependencyNode implements im0 {
    public WidgetRun d;
    public int f;
    public int g;
    public im0 a = null;
    public boolean b = false;
    public boolean c = false;
    public Type e = Type.UNKNOWN;
    public int h = 1;
    public a i = null;
    public boolean j = false;
    public List<im0> k = new ArrayList();
    public List<DependencyNode> l = new ArrayList();

    /* loaded from: classes.dex */
    public enum Type {
        UNKNOWN,
        HORIZONTAL_DIMENSION,
        VERTICAL_DIMENSION,
        LEFT,
        RIGHT,
        TOP,
        BOTTOM,
        BASELINE
    }

    public DependencyNode(WidgetRun widgetRun) {
        this.d = widgetRun;
    }

    @Override // defpackage.im0
    public void a(im0 im0Var) {
        for (DependencyNode dependencyNode : this.l) {
            if (!dependencyNode.j) {
                return;
            }
        }
        this.c = true;
        im0 im0Var2 = this.a;
        if (im0Var2 != null) {
            im0Var2.a(this);
        }
        if (this.b) {
            this.d.a(this);
            return;
        }
        DependencyNode dependencyNode2 = null;
        int i = 0;
        for (DependencyNode dependencyNode3 : this.l) {
            if (!(dependencyNode3 instanceof a)) {
                i++;
                dependencyNode2 = dependencyNode3;
            }
        }
        if (dependencyNode2 != null && i == 1 && dependencyNode2.j) {
            a aVar = this.i;
            if (aVar != null) {
                if (!aVar.j) {
                    return;
                }
                this.f = this.h * aVar.g;
            }
            d(dependencyNode2.g + this.f);
        }
        im0 im0Var3 = this.a;
        if (im0Var3 != null) {
            im0Var3.a(this);
        }
    }

    public void b(im0 im0Var) {
        this.k.add(im0Var);
        if (this.j) {
            im0Var.a(im0Var);
        }
    }

    public void c() {
        this.l.clear();
        this.k.clear();
        this.j = false;
        this.g = 0;
        this.c = false;
        this.b = false;
    }

    public void d(int i) {
        if (this.j) {
            return;
        }
        this.j = true;
        this.g = i;
        for (im0 im0Var : this.k) {
            im0Var.a(im0Var);
        }
    }

    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(this.d.b.v());
        sb.append(":");
        sb.append(this.e);
        sb.append("(");
        sb.append(this.j ? Integer.valueOf(this.g) : "unresolved");
        sb.append(") <t=");
        sb.append(this.l.size());
        sb.append(":d=");
        sb.append(this.k.size());
        sb.append(">");
        return sb.toString();
    }
}
