package androidx.constraintlayout.core.widgets.analyzer;

import androidx.constraintlayout.core.widgets.ConstraintAnchor;
import androidx.constraintlayout.core.widgets.ConstraintWidget;
import androidx.constraintlayout.core.widgets.analyzer.DependencyNode;
import androidx.constraintlayout.core.widgets.analyzer.WidgetRun;

/* compiled from: HorizontalWidgetRun.java */
/* loaded from: classes.dex */
public class c extends WidgetRun {
    public static int[] k = new int[2];

    /* compiled from: HorizontalWidgetRun.java */
    /* loaded from: classes.dex */
    public static /* synthetic */ class a {
        public static final /* synthetic */ int[] a;

        static {
            int[] iArr = new int[WidgetRun.RunType.values().length];
            a = iArr;
            try {
                iArr[WidgetRun.RunType.START.ordinal()] = 1;
            } catch (NoSuchFieldError unused) {
            }
            try {
                a[WidgetRun.RunType.END.ordinal()] = 2;
            } catch (NoSuchFieldError unused2) {
            }
            try {
                a[WidgetRun.RunType.CENTER.ordinal()] = 3;
            } catch (NoSuchFieldError unused3) {
            }
        }
    }

    public c(ConstraintWidget constraintWidget) {
        super(constraintWidget);
        this.h.e = DependencyNode.Type.LEFT;
        this.i.e = DependencyNode.Type.RIGHT;
        this.f = 0;
    }

    /* JADX WARN: Code restructure failed: missing block: B:119:0x02b9, code lost:
        if (r14 != 1) goto L131;
     */
    @Override // androidx.constraintlayout.core.widgets.analyzer.WidgetRun, defpackage.im0
    /*
        Code decompiled incorrectly, please refer to instructions dump.
        To view partially-correct code enable 'Show inconsistent code' option in preferences
    */
    public void a(defpackage.im0 r17) {
        /*
            Method dump skipped, instructions count: 1087
            To view this dump change 'Code comments level' option to 'DEBUG'
        */
        throw new UnsupportedOperationException("Method not decompiled: androidx.constraintlayout.core.widgets.analyzer.c.a(im0):void");
    }

    @Override // androidx.constraintlayout.core.widgets.analyzer.WidgetRun
    public void d() {
        ConstraintWidget M;
        ConstraintWidget M2;
        ConstraintWidget constraintWidget = this.b;
        if (constraintWidget.a) {
            this.e.d(constraintWidget.V());
        }
        if (!this.e.j) {
            ConstraintWidget.DimensionBehaviour C = this.b.C();
            this.d = C;
            if (C != ConstraintWidget.DimensionBehaviour.MATCH_CONSTRAINT) {
                ConstraintWidget.DimensionBehaviour dimensionBehaviour = ConstraintWidget.DimensionBehaviour.MATCH_PARENT;
                if (C == dimensionBehaviour && (M2 = this.b.M()) != null && (M2.C() == ConstraintWidget.DimensionBehaviour.FIXED || M2.C() == dimensionBehaviour)) {
                    int V = (M2.V() - this.b.M.f()) - this.b.O.f();
                    b(this.h, M2.d.h, this.b.M.f());
                    b(this.i, M2.d.i, -this.b.O.f());
                    this.e.d(V);
                    return;
                } else if (this.d == ConstraintWidget.DimensionBehaviour.FIXED) {
                    this.e.d(this.b.V());
                }
            }
        } else {
            ConstraintWidget.DimensionBehaviour dimensionBehaviour2 = this.d;
            ConstraintWidget.DimensionBehaviour dimensionBehaviour3 = ConstraintWidget.DimensionBehaviour.MATCH_PARENT;
            if (dimensionBehaviour2 == dimensionBehaviour3 && (M = this.b.M()) != null && (M.C() == ConstraintWidget.DimensionBehaviour.FIXED || M.C() == dimensionBehaviour3)) {
                b(this.h, M.d.h, this.b.M.f());
                b(this.i, M.d.i, -this.b.O.f());
                return;
            }
        }
        androidx.constraintlayout.core.widgets.analyzer.a aVar = this.e;
        if (aVar.j) {
            ConstraintWidget constraintWidget2 = this.b;
            if (constraintWidget2.a) {
                ConstraintAnchor[] constraintAnchorArr = constraintWidget2.U;
                if (constraintAnchorArr[0].f != null && constraintAnchorArr[1].f != null) {
                    if (constraintWidget2.h0()) {
                        this.h.f = this.b.U[0].f();
                        this.i.f = -this.b.U[1].f();
                        return;
                    }
                    DependencyNode h = h(this.b.U[0]);
                    if (h != null) {
                        b(this.h, h, this.b.U[0].f());
                    }
                    DependencyNode h2 = h(this.b.U[1]);
                    if (h2 != null) {
                        b(this.i, h2, -this.b.U[1].f());
                    }
                    this.h.b = true;
                    this.i.b = true;
                    return;
                } else if (constraintAnchorArr[0].f != null) {
                    DependencyNode h3 = h(constraintAnchorArr[0]);
                    if (h3 != null) {
                        b(this.h, h3, this.b.U[0].f());
                        b(this.i, this.h, this.e.g);
                        return;
                    }
                    return;
                } else if (constraintAnchorArr[1].f != null) {
                    DependencyNode h4 = h(constraintAnchorArr[1]);
                    if (h4 != null) {
                        b(this.i, h4, -this.b.U[1].f());
                        b(this.h, this.i, -this.e.g);
                        return;
                    }
                    return;
                } else if ((constraintWidget2 instanceof lk1) || constraintWidget2.M() == null || this.b.q(ConstraintAnchor.Type.CENTER).f != null) {
                    return;
                } else {
                    b(this.h, this.b.M().d.h, this.b.W());
                    b(this.i, this.h, this.e.g);
                    return;
                }
            }
        }
        if (this.d == ConstraintWidget.DimensionBehaviour.MATCH_CONSTRAINT) {
            ConstraintWidget constraintWidget3 = this.b;
            int i = constraintWidget3.s;
            if (i == 2) {
                ConstraintWidget M3 = constraintWidget3.M();
                if (M3 != null) {
                    androidx.constraintlayout.core.widgets.analyzer.a aVar2 = M3.e.e;
                    this.e.l.add(aVar2);
                    aVar2.k.add(this.e);
                    androidx.constraintlayout.core.widgets.analyzer.a aVar3 = this.e;
                    aVar3.b = true;
                    aVar3.k.add(this.h);
                    this.e.k.add(this.i);
                }
            } else if (i == 3) {
                if (constraintWidget3.t == 3) {
                    this.h.a = this;
                    this.i.a = this;
                    d dVar = constraintWidget3.e;
                    dVar.h.a = this;
                    dVar.i.a = this;
                    aVar.a = this;
                    if (constraintWidget3.j0()) {
                        this.e.l.add(this.b.e.e);
                        this.b.e.e.k.add(this.e);
                        d dVar2 = this.b.e;
                        dVar2.e.a = this;
                        this.e.l.add(dVar2.h);
                        this.e.l.add(this.b.e.i);
                        this.b.e.h.k.add(this.e);
                        this.b.e.i.k.add(this.e);
                    } else if (this.b.h0()) {
                        this.b.e.e.l.add(this.e);
                        this.e.k.add(this.b.e.e);
                    } else {
                        this.b.e.e.l.add(this.e);
                    }
                } else {
                    androidx.constraintlayout.core.widgets.analyzer.a aVar4 = constraintWidget3.e.e;
                    aVar.l.add(aVar4);
                    aVar4.k.add(this.e);
                    this.b.e.h.k.add(this.e);
                    this.b.e.i.k.add(this.e);
                    androidx.constraintlayout.core.widgets.analyzer.a aVar5 = this.e;
                    aVar5.b = true;
                    aVar5.k.add(this.h);
                    this.e.k.add(this.i);
                    this.h.l.add(this.e);
                    this.i.l.add(this.e);
                }
            }
        }
        ConstraintWidget constraintWidget4 = this.b;
        ConstraintAnchor[] constraintAnchorArr2 = constraintWidget4.U;
        if (constraintAnchorArr2[0].f != null && constraintAnchorArr2[1].f != null) {
            if (constraintWidget4.h0()) {
                this.h.f = this.b.U[0].f();
                this.i.f = -this.b.U[1].f();
                return;
            }
            DependencyNode h5 = h(this.b.U[0]);
            DependencyNode h6 = h(this.b.U[1]);
            if (h5 != null) {
                h5.b(this);
            }
            if (h6 != null) {
                h6.b(this);
            }
            this.j = WidgetRun.RunType.CENTER;
        } else if (constraintAnchorArr2[0].f != null) {
            DependencyNode h7 = h(constraintAnchorArr2[0]);
            if (h7 != null) {
                b(this.h, h7, this.b.U[0].f());
                c(this.i, this.h, 1, this.e);
            }
        } else if (constraintAnchorArr2[1].f != null) {
            DependencyNode h8 = h(constraintAnchorArr2[1]);
            if (h8 != null) {
                b(this.i, h8, -this.b.U[1].f());
                c(this.h, this.i, -1, this.e);
            }
        } else if ((constraintWidget4 instanceof lk1) || constraintWidget4.M() == null) {
        } else {
            b(this.h, this.b.M().d.h, this.b.W());
            c(this.i, this.h, 1, this.e);
        }
    }

    @Override // androidx.constraintlayout.core.widgets.analyzer.WidgetRun
    public void e() {
        DependencyNode dependencyNode = this.h;
        if (dependencyNode.j) {
            this.b.j1(dependencyNode.g);
        }
    }

    @Override // androidx.constraintlayout.core.widgets.analyzer.WidgetRun
    public void f() {
        this.c = null;
        this.h.c();
        this.i.c();
        this.e.c();
        this.g = false;
    }

    @Override // androidx.constraintlayout.core.widgets.analyzer.WidgetRun
    public boolean m() {
        return this.d != ConstraintWidget.DimensionBehaviour.MATCH_CONSTRAINT || this.b.s == 0;
    }

    public final void q(int[] iArr, int i, int i2, int i3, int i4, float f, int i5) {
        int i6 = i2 - i;
        int i7 = i4 - i3;
        if (i5 != -1) {
            if (i5 == 0) {
                iArr[0] = (int) ((i7 * f) + 0.5f);
                iArr[1] = i7;
                return;
            } else if (i5 != 1) {
                return;
            } else {
                iArr[0] = i6;
                iArr[1] = (int) ((i6 * f) + 0.5f);
                return;
            }
        }
        int i8 = (int) ((i7 * f) + 0.5f);
        int i9 = (int) ((i6 / f) + 0.5f);
        if (i8 <= i6) {
            iArr[0] = i8;
            iArr[1] = i7;
        } else if (i9 <= i7) {
            iArr[0] = i6;
            iArr[1] = i9;
        }
    }

    public void r() {
        this.g = false;
        this.h.c();
        this.h.j = false;
        this.i.c();
        this.i.j = false;
        this.e.j = false;
    }

    public String toString() {
        return "HorizontalRun " + this.b.v();
    }
}
