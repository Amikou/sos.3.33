package androidx.constraintlayout.core.widgets.analyzer;

import androidx.constraintlayout.core.widgets.ConstraintAnchor;
import androidx.constraintlayout.core.widgets.ConstraintWidget;

/* loaded from: classes.dex */
public abstract class WidgetRun implements im0 {
    public int a;
    public ConstraintWidget b;
    public aa3 c;
    public ConstraintWidget.DimensionBehaviour d;
    public androidx.constraintlayout.core.widgets.analyzer.a e = new androidx.constraintlayout.core.widgets.analyzer.a(this);
    public int f = 0;
    public boolean g = false;
    public DependencyNode h = new DependencyNode(this);
    public DependencyNode i = new DependencyNode(this);
    public RunType j = RunType.NONE;

    /* loaded from: classes.dex */
    public enum RunType {
        NONE,
        START,
        END,
        CENTER
    }

    /* loaded from: classes.dex */
    public static /* synthetic */ class a {
        public static final /* synthetic */ int[] a;

        static {
            int[] iArr = new int[ConstraintAnchor.Type.values().length];
            a = iArr;
            try {
                iArr[ConstraintAnchor.Type.LEFT.ordinal()] = 1;
            } catch (NoSuchFieldError unused) {
            }
            try {
                a[ConstraintAnchor.Type.RIGHT.ordinal()] = 2;
            } catch (NoSuchFieldError unused2) {
            }
            try {
                a[ConstraintAnchor.Type.TOP.ordinal()] = 3;
            } catch (NoSuchFieldError unused3) {
            }
            try {
                a[ConstraintAnchor.Type.BASELINE.ordinal()] = 4;
            } catch (NoSuchFieldError unused4) {
            }
            try {
                a[ConstraintAnchor.Type.BOTTOM.ordinal()] = 5;
            } catch (NoSuchFieldError unused5) {
            }
        }
    }

    public WidgetRun(ConstraintWidget constraintWidget) {
        this.b = constraintWidget;
    }

    @Override // defpackage.im0
    public void a(im0 im0Var) {
    }

    public final void b(DependencyNode dependencyNode, DependencyNode dependencyNode2, int i) {
        dependencyNode.l.add(dependencyNode2);
        dependencyNode.f = i;
        dependencyNode2.k.add(dependencyNode);
    }

    public final void c(DependencyNode dependencyNode, DependencyNode dependencyNode2, int i, androidx.constraintlayout.core.widgets.analyzer.a aVar) {
        dependencyNode.l.add(dependencyNode2);
        dependencyNode.l.add(this.e);
        dependencyNode.h = i;
        dependencyNode.i = aVar;
        dependencyNode2.k.add(dependencyNode);
        aVar.k.add(dependencyNode);
    }

    public abstract void d();

    public abstract void e();

    public abstract void f();

    public final int g(int i, int i2) {
        int max;
        if (i2 == 0) {
            ConstraintWidget constraintWidget = this.b;
            int i3 = constraintWidget.w;
            max = Math.max(constraintWidget.v, i);
            if (i3 > 0) {
                max = Math.min(i3, i);
            }
            if (max == i) {
                return i;
            }
        } else {
            ConstraintWidget constraintWidget2 = this.b;
            int i4 = constraintWidget2.z;
            max = Math.max(constraintWidget2.y, i);
            if (i4 > 0) {
                max = Math.min(i4, i);
            }
            if (max == i) {
                return i;
            }
        }
        return max;
    }

    public final DependencyNode h(ConstraintAnchor constraintAnchor) {
        ConstraintAnchor constraintAnchor2 = constraintAnchor.f;
        if (constraintAnchor2 == null) {
            return null;
        }
        ConstraintWidget constraintWidget = constraintAnchor2.d;
        int i = a.a[constraintAnchor2.e.ordinal()];
        if (i != 1) {
            if (i != 2) {
                if (i != 3) {
                    if (i != 4) {
                        if (i != 5) {
                            return null;
                        }
                        return constraintWidget.e.i;
                    }
                    return constraintWidget.e.k;
                }
                return constraintWidget.e.h;
            }
            return constraintWidget.d.i;
        }
        return constraintWidget.d.h;
    }

    public final DependencyNode i(ConstraintAnchor constraintAnchor, int i) {
        ConstraintAnchor constraintAnchor2 = constraintAnchor.f;
        if (constraintAnchor2 == null) {
            return null;
        }
        ConstraintWidget constraintWidget = constraintAnchor2.d;
        WidgetRun widgetRun = i == 0 ? constraintWidget.d : constraintWidget.e;
        int i2 = a.a[constraintAnchor2.e.ordinal()];
        if (i2 != 1) {
            if (i2 != 2) {
                if (i2 != 3) {
                    if (i2 != 5) {
                        return null;
                    }
                }
            }
            return widgetRun.i;
        }
        return widgetRun.h;
    }

    public long j() {
        androidx.constraintlayout.core.widgets.analyzer.a aVar = this.e;
        if (aVar.j) {
            return aVar.g;
        }
        return 0L;
    }

    public boolean k() {
        return this.g;
    }

    public final void l(int i, int i2) {
        int i3;
        int i4 = this.a;
        if (i4 == 0) {
            this.e.d(g(i2, i));
        } else if (i4 == 1) {
            this.e.d(Math.min(g(this.e.m, i), i2));
        } else if (i4 == 2) {
            ConstraintWidget M = this.b.M();
            if (M != null) {
                androidx.constraintlayout.core.widgets.analyzer.a aVar = (i == 0 ? M.d : M.e).e;
                if (aVar.j) {
                    ConstraintWidget constraintWidget = this.b;
                    this.e.d(g((int) ((aVar.g * (i == 0 ? constraintWidget.x : constraintWidget.A)) + 0.5f), i));
                }
            }
        } else if (i4 != 3) {
        } else {
            ConstraintWidget constraintWidget2 = this.b;
            WidgetRun widgetRun = constraintWidget2.d;
            ConstraintWidget.DimensionBehaviour dimensionBehaviour = widgetRun.d;
            ConstraintWidget.DimensionBehaviour dimensionBehaviour2 = ConstraintWidget.DimensionBehaviour.MATCH_CONSTRAINT;
            if (dimensionBehaviour == dimensionBehaviour2 && widgetRun.a == 3) {
                d dVar = constraintWidget2.e;
                if (dVar.d == dimensionBehaviour2 && dVar.a == 3) {
                    return;
                }
            }
            if (i == 0) {
                widgetRun = constraintWidget2.e;
            }
            if (widgetRun.e.j) {
                float x = constraintWidget2.x();
                if (i == 1) {
                    i3 = (int) ((widgetRun.e.g / x) + 0.5f);
                } else {
                    i3 = (int) ((x * widgetRun.e.g) + 0.5f);
                }
                this.e.d(i3);
            }
        }
    }

    public abstract boolean m();

    public void n(im0 im0Var, ConstraintAnchor constraintAnchor, ConstraintAnchor constraintAnchor2, int i) {
        DependencyNode h = h(constraintAnchor);
        DependencyNode h2 = h(constraintAnchor2);
        if (h.j && h2.j) {
            int f = h.g + constraintAnchor.f();
            int f2 = h2.g - constraintAnchor2.f();
            int i2 = f2 - f;
            if (!this.e.j && this.d == ConstraintWidget.DimensionBehaviour.MATCH_CONSTRAINT) {
                l(i, i2);
            }
            androidx.constraintlayout.core.widgets.analyzer.a aVar = this.e;
            if (aVar.j) {
                if (aVar.g == i2) {
                    this.h.d(f);
                    this.i.d(f2);
                    return;
                }
                ConstraintWidget constraintWidget = this.b;
                float A = i == 0 ? constraintWidget.A() : constraintWidget.Q();
                if (h == h2) {
                    f = h.g;
                    f2 = h2.g;
                    A = 0.5f;
                }
                this.h.d((int) (f + 0.5f + (((f2 - f) - this.e.g) * A)));
                this.i.d(this.h.g + this.e.g);
            }
        }
    }

    public void o(im0 im0Var) {
    }

    public void p(im0 im0Var) {
    }
}
