package androidx.constraintlayout.core.widgets;

import androidx.constraintlayout.core.widgets.ConstraintWidget;

/* compiled from: Optimizer.java */
/* loaded from: classes.dex */
public class g {
    public static boolean[] a = new boolean[3];

    public static void a(d dVar, androidx.constraintlayout.core.c cVar, ConstraintWidget constraintWidget) {
        constraintWidget.p = -1;
        constraintWidget.q = -1;
        ConstraintWidget.DimensionBehaviour dimensionBehaviour = dVar.X[0];
        ConstraintWidget.DimensionBehaviour dimensionBehaviour2 = ConstraintWidget.DimensionBehaviour.WRAP_CONTENT;
        if (dimensionBehaviour != dimensionBehaviour2 && constraintWidget.X[0] == ConstraintWidget.DimensionBehaviour.MATCH_PARENT) {
            int i = constraintWidget.M.g;
            int V = dVar.V() - constraintWidget.O.g;
            ConstraintAnchor constraintAnchor = constraintWidget.M;
            constraintAnchor.i = cVar.q(constraintAnchor);
            ConstraintAnchor constraintAnchor2 = constraintWidget.O;
            constraintAnchor2.i = cVar.q(constraintAnchor2);
            cVar.f(constraintWidget.M.i, i);
            cVar.f(constraintWidget.O.i, V);
            constraintWidget.p = 2;
            constraintWidget.L0(i, V);
        }
        if (dVar.X[1] == dimensionBehaviour2 || constraintWidget.X[1] != ConstraintWidget.DimensionBehaviour.MATCH_PARENT) {
            return;
        }
        int i2 = constraintWidget.N.g;
        int z = dVar.z() - constraintWidget.P.g;
        ConstraintAnchor constraintAnchor3 = constraintWidget.N;
        constraintAnchor3.i = cVar.q(constraintAnchor3);
        ConstraintAnchor constraintAnchor4 = constraintWidget.P;
        constraintAnchor4.i = cVar.q(constraintAnchor4);
        cVar.f(constraintWidget.N.i, i2);
        cVar.f(constraintWidget.P.i, z);
        if (constraintWidget.j0 > 0 || constraintWidget.U() == 8) {
            ConstraintAnchor constraintAnchor5 = constraintWidget.Q;
            constraintAnchor5.i = cVar.q(constraintAnchor5);
            cVar.f(constraintWidget.Q.i, constraintWidget.j0 + i2);
        }
        constraintWidget.q = 2;
        constraintWidget.c1(i2, z);
    }

    public static final boolean b(int i, int i2) {
        return (i & i2) == i2;
    }
}
