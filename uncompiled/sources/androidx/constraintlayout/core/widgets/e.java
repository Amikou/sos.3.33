package androidx.constraintlayout.core.widgets;

import androidx.constraintlayout.core.widgets.ConstraintWidget;
import java.util.ArrayList;
import java.util.HashMap;

/* compiled from: Flow.java */
/* loaded from: classes.dex */
public class e extends i {
    public ConstraintWidget[] z1;
    public int c1 = -1;
    public int d1 = -1;
    public int e1 = -1;
    public int f1 = -1;
    public int g1 = -1;
    public int h1 = -1;
    public float i1 = 0.5f;
    public float j1 = 0.5f;
    public float k1 = 0.5f;
    public float l1 = 0.5f;
    public float m1 = 0.5f;
    public float n1 = 0.5f;
    public int o1 = 0;
    public int p1 = 0;
    public int q1 = 2;
    public int r1 = 2;
    public int s1 = 0;
    public int t1 = -1;
    public int u1 = 0;
    public ArrayList<a> v1 = new ArrayList<>();
    public ConstraintWidget[] w1 = null;
    public ConstraintWidget[] x1 = null;
    public int[] y1 = null;
    public int A1 = 0;

    /* compiled from: Flow.java */
    /* loaded from: classes.dex */
    public class a {
        public int a;
        public ConstraintAnchor d;
        public ConstraintAnchor e;
        public ConstraintAnchor f;
        public ConstraintAnchor g;
        public int h;
        public int i;
        public int j;
        public int k;
        public int q;
        public ConstraintWidget b = null;
        public int c = 0;
        public int l = 0;
        public int m = 0;
        public int n = 0;
        public int o = 0;
        public int p = 0;

        public a(int i, ConstraintAnchor constraintAnchor, ConstraintAnchor constraintAnchor2, ConstraintAnchor constraintAnchor3, ConstraintAnchor constraintAnchor4, int i2) {
            this.a = 0;
            this.h = 0;
            this.i = 0;
            this.j = 0;
            this.k = 0;
            this.q = 0;
            this.a = i;
            this.d = constraintAnchor;
            this.e = constraintAnchor2;
            this.f = constraintAnchor3;
            this.g = constraintAnchor4;
            this.h = e.this.w1();
            this.i = e.this.y1();
            this.j = e.this.x1();
            this.k = e.this.v1();
            this.q = i2;
        }

        public void b(ConstraintWidget constraintWidget) {
            if (this.a == 0) {
                int i2 = e.this.i2(constraintWidget, this.q);
                if (constraintWidget.C() == ConstraintWidget.DimensionBehaviour.MATCH_CONSTRAINT) {
                    this.p++;
                    i2 = 0;
                }
                this.l += i2 + (constraintWidget.U() != 8 ? e.this.o1 : 0);
                int h2 = e.this.h2(constraintWidget, this.q);
                if (this.b == null || this.c < h2) {
                    this.b = constraintWidget;
                    this.c = h2;
                    this.m = h2;
                }
            } else {
                int i22 = e.this.i2(constraintWidget, this.q);
                int h22 = e.this.h2(constraintWidget, this.q);
                if (constraintWidget.S() == ConstraintWidget.DimensionBehaviour.MATCH_CONSTRAINT) {
                    this.p++;
                    h22 = 0;
                }
                this.m += h22 + (constraintWidget.U() != 8 ? e.this.p1 : 0);
                if (this.b == null || this.c < i22) {
                    this.b = constraintWidget;
                    this.c = i22;
                    this.l = i22;
                }
            }
            this.o++;
        }

        public void c() {
            this.c = 0;
            this.b = null;
            this.l = 0;
            this.m = 0;
            this.n = 0;
            this.o = 0;
            this.p = 0;
        }

        public void d(boolean z, int i, boolean z2) {
            ConstraintWidget constraintWidget;
            char c;
            float f;
            float f2;
            int i2 = this.o;
            for (int i3 = 0; i3 < i2 && this.n + i3 < e.this.A1; i3++) {
                ConstraintWidget constraintWidget2 = e.this.z1[this.n + i3];
                if (constraintWidget2 != null) {
                    constraintWidget2.u0();
                }
            }
            if (i2 == 0 || this.b == null) {
                return;
            }
            boolean z3 = z2 && i == 0;
            int i4 = -1;
            int i5 = -1;
            for (int i6 = 0; i6 < i2; i6++) {
                int i7 = z ? (i2 - 1) - i6 : i6;
                if (this.n + i7 >= e.this.A1) {
                    break;
                }
                if (e.this.z1[this.n + i7].U() == 0) {
                    if (i4 == -1) {
                        i4 = i6;
                    }
                    i5 = i6;
                }
            }
            ConstraintWidget constraintWidget3 = null;
            if (this.a == 0) {
                ConstraintWidget constraintWidget4 = this.b;
                constraintWidget4.b1(e.this.d1);
                int i8 = this.i;
                if (i > 0) {
                    i8 += e.this.p1;
                }
                constraintWidget4.N.a(this.e, i8);
                if (z2) {
                    constraintWidget4.P.a(this.g, this.k);
                }
                if (i > 0) {
                    this.e.d.P.a(constraintWidget4.N, 0);
                }
                if (e.this.r1 == 3 && !constraintWidget4.Y()) {
                    for (int i9 = 0; i9 < i2; i9++) {
                        int i10 = z ? (i2 - 1) - i9 : i9;
                        if (this.n + i10 >= e.this.A1) {
                            break;
                        }
                        constraintWidget = e.this.z1[this.n + i10];
                        if (constraintWidget.Y()) {
                            break;
                        }
                    }
                }
                constraintWidget = constraintWidget4;
                int i11 = 0;
                while (i11 < i2) {
                    int i12 = z ? (i2 - 1) - i11 : i11;
                    if (this.n + i12 >= e.this.A1) {
                        return;
                    }
                    ConstraintWidget constraintWidget5 = e.this.z1[this.n + i12];
                    if (i11 == 0) {
                        constraintWidget5.l(constraintWidget5.M, this.d, this.h);
                    }
                    if (i12 == 0) {
                        int i13 = e.this.c1;
                        float f3 = e.this.i1;
                        if (z) {
                            f3 = 1.0f - f3;
                        }
                        if (this.n != 0 || e.this.e1 == -1) {
                            if (z2 && e.this.g1 != -1) {
                                i13 = e.this.g1;
                                if (z) {
                                    f2 = e.this.m1;
                                    f = 1.0f - f2;
                                    f3 = f;
                                } else {
                                    f = e.this.m1;
                                    f3 = f;
                                }
                            }
                        } else {
                            i13 = e.this.e1;
                            if (z) {
                                f2 = e.this.k1;
                                f = 1.0f - f2;
                                f3 = f;
                            } else {
                                f = e.this.k1;
                                f3 = f;
                            }
                        }
                        constraintWidget5.K0(i13);
                        constraintWidget5.J0(f3);
                    }
                    if (i11 == i2 - 1) {
                        constraintWidget5.l(constraintWidget5.O, this.f, this.j);
                    }
                    if (constraintWidget3 != null) {
                        constraintWidget5.M.a(constraintWidget3.O, e.this.o1);
                        if (i11 == i4) {
                            constraintWidget5.M.u(this.h);
                        }
                        constraintWidget3.O.a(constraintWidget5.M, 0);
                        if (i11 == i5 + 1) {
                            constraintWidget3.O.u(this.j);
                        }
                    }
                    if (constraintWidget5 != constraintWidget4) {
                        c = 3;
                        if (e.this.r1 != 3 || !constraintWidget.Y() || constraintWidget5 == constraintWidget || !constraintWidget5.Y()) {
                            int i14 = e.this.r1;
                            if (i14 == 0) {
                                constraintWidget5.N.a(constraintWidget4.N, 0);
                            } else if (i14 == 1) {
                                constraintWidget5.P.a(constraintWidget4.P, 0);
                            } else if (z3) {
                                constraintWidget5.N.a(this.e, this.i);
                                constraintWidget5.P.a(this.g, this.k);
                            } else {
                                constraintWidget5.N.a(constraintWidget4.N, 0);
                                constraintWidget5.P.a(constraintWidget4.P, 0);
                            }
                        } else {
                            constraintWidget5.Q.a(constraintWidget.Q, 0);
                        }
                    } else {
                        c = 3;
                    }
                    i11++;
                    constraintWidget3 = constraintWidget5;
                }
                return;
            }
            ConstraintWidget constraintWidget6 = this.b;
            constraintWidget6.K0(e.this.c1);
            int i15 = this.h;
            if (i > 0) {
                i15 += e.this.o1;
            }
            if (z) {
                constraintWidget6.O.a(this.f, i15);
                if (z2) {
                    constraintWidget6.M.a(this.d, this.j);
                }
                if (i > 0) {
                    this.f.d.M.a(constraintWidget6.O, 0);
                }
            } else {
                constraintWidget6.M.a(this.d, i15);
                if (z2) {
                    constraintWidget6.O.a(this.f, this.j);
                }
                if (i > 0) {
                    this.d.d.O.a(constraintWidget6.M, 0);
                }
            }
            int i16 = 0;
            while (i16 < i2 && this.n + i16 < e.this.A1) {
                ConstraintWidget constraintWidget7 = e.this.z1[this.n + i16];
                if (i16 == 0) {
                    constraintWidget7.l(constraintWidget7.N, this.e, this.i);
                    int i17 = e.this.d1;
                    float f4 = e.this.j1;
                    if (this.n != 0 || e.this.f1 == -1) {
                        if (z2 && e.this.h1 != -1) {
                            i17 = e.this.h1;
                            f4 = e.this.n1;
                        }
                    } else {
                        i17 = e.this.f1;
                        f4 = e.this.l1;
                    }
                    constraintWidget7.b1(i17);
                    constraintWidget7.a1(f4);
                }
                if (i16 == i2 - 1) {
                    constraintWidget7.l(constraintWidget7.P, this.g, this.k);
                }
                if (constraintWidget3 != null) {
                    constraintWidget7.N.a(constraintWidget3.P, e.this.p1);
                    if (i16 == i4) {
                        constraintWidget7.N.u(this.i);
                    }
                    constraintWidget3.P.a(constraintWidget7.N, 0);
                    if (i16 == i5 + 1) {
                        constraintWidget3.P.u(this.k);
                    }
                }
                if (constraintWidget7 != constraintWidget6) {
                    if (z) {
                        int i18 = e.this.q1;
                        if (i18 == 0) {
                            constraintWidget7.O.a(constraintWidget6.O, 0);
                        } else if (i18 == 1) {
                            constraintWidget7.M.a(constraintWidget6.M, 0);
                        } else if (i18 == 2) {
                            constraintWidget7.M.a(constraintWidget6.M, 0);
                            constraintWidget7.O.a(constraintWidget6.O, 0);
                        }
                    } else {
                        int i19 = e.this.q1;
                        if (i19 == 0) {
                            constraintWidget7.M.a(constraintWidget6.M, 0);
                        } else if (i19 == 1) {
                            constraintWidget7.O.a(constraintWidget6.O, 0);
                        } else if (i19 == 2) {
                            if (z3) {
                                constraintWidget7.M.a(this.d, this.h);
                                constraintWidget7.O.a(this.f, this.j);
                            } else {
                                constraintWidget7.M.a(constraintWidget6.M, 0);
                                constraintWidget7.O.a(constraintWidget6.O, 0);
                            }
                        }
                        i16++;
                        constraintWidget3 = constraintWidget7;
                    }
                }
                i16++;
                constraintWidget3 = constraintWidget7;
            }
        }

        public int e() {
            if (this.a == 1) {
                return this.m - e.this.p1;
            }
            return this.m;
        }

        public int f() {
            if (this.a == 0) {
                return this.l - e.this.o1;
            }
            return this.l;
        }

        public void g(int i) {
            int i2 = this.p;
            if (i2 == 0) {
                return;
            }
            int i3 = this.o;
            int i4 = i / i2;
            for (int i5 = 0; i5 < i3 && this.n + i5 < e.this.A1; i5++) {
                ConstraintWidget constraintWidget = e.this.z1[this.n + i5];
                if (this.a == 0) {
                    if (constraintWidget != null && constraintWidget.C() == ConstraintWidget.DimensionBehaviour.MATCH_CONSTRAINT && constraintWidget.s == 0) {
                        e.this.A1(constraintWidget, ConstraintWidget.DimensionBehaviour.FIXED, i4, constraintWidget.S(), constraintWidget.z());
                    }
                } else if (constraintWidget != null && constraintWidget.S() == ConstraintWidget.DimensionBehaviour.MATCH_CONSTRAINT && constraintWidget.t == 0) {
                    e.this.A1(constraintWidget, constraintWidget.C(), constraintWidget.V(), ConstraintWidget.DimensionBehaviour.FIXED, i4);
                }
            }
            h();
        }

        public final void h() {
            this.l = 0;
            this.m = 0;
            this.b = null;
            this.c = 0;
            int i = this.o;
            for (int i2 = 0; i2 < i && this.n + i2 < e.this.A1; i2++) {
                ConstraintWidget constraintWidget = e.this.z1[this.n + i2];
                if (this.a != 0) {
                    int i22 = e.this.i2(constraintWidget, this.q);
                    int h2 = e.this.h2(constraintWidget, this.q);
                    int i3 = e.this.p1;
                    if (constraintWidget.U() == 8) {
                        i3 = 0;
                    }
                    this.m += h2 + i3;
                    if (this.b == null || this.c < i22) {
                        this.b = constraintWidget;
                        this.c = i22;
                        this.l = i22;
                    }
                } else {
                    int V = constraintWidget.V();
                    int i4 = e.this.o1;
                    if (constraintWidget.U() == 8) {
                        i4 = 0;
                    }
                    this.l += V + i4;
                    int h22 = e.this.h2(constraintWidget, this.q);
                    if (this.b == null || this.c < h22) {
                        this.b = constraintWidget;
                        this.c = h22;
                        this.m = h22;
                    }
                }
            }
        }

        public void i(int i) {
            this.n = i;
        }

        public void j(int i, ConstraintAnchor constraintAnchor, ConstraintAnchor constraintAnchor2, ConstraintAnchor constraintAnchor3, ConstraintAnchor constraintAnchor4, int i2, int i3, int i4, int i5, int i6) {
            this.a = i;
            this.d = constraintAnchor;
            this.e = constraintAnchor2;
            this.f = constraintAnchor3;
            this.g = constraintAnchor4;
            this.h = i2;
            this.i = i3;
            this.j = i4;
            this.k = i5;
            this.q = i6;
        }
    }

    public void A2(int i) {
        this.r1 = i;
    }

    public void B2(float f) {
        this.j1 = f;
    }

    public void C2(int i) {
        this.p1 = i;
    }

    public void D2(int i) {
        this.d1 = i;
    }

    public void E2(int i) {
        this.s1 = i;
    }

    @Override // androidx.constraintlayout.core.widgets.ConstraintWidget
    public void g(androidx.constraintlayout.core.c cVar, boolean z) {
        super.g(cVar, z);
        boolean z2 = M() != null && ((d) M()).M1();
        int i = this.s1;
        if (i != 0) {
            if (i == 1) {
                int size = this.v1.size();
                int i2 = 0;
                while (i2 < size) {
                    this.v1.get(i2).d(z2, i2, i2 == size + (-1));
                    i2++;
                }
            } else if (i == 2) {
                g2(z2);
            }
        } else if (this.v1.size() > 0) {
            this.v1.get(0).d(z2, 0, true);
        }
        D1(false);
    }

    public final void g2(boolean z) {
        ConstraintWidget constraintWidget;
        float f;
        int i;
        if (this.y1 == null || this.x1 == null || this.w1 == null) {
            return;
        }
        for (int i2 = 0; i2 < this.A1; i2++) {
            this.z1[i2].u0();
        }
        int[] iArr = this.y1;
        int i3 = iArr[0];
        int i4 = iArr[1];
        ConstraintWidget constraintWidget2 = null;
        float f2 = this.i1;
        int i5 = 0;
        while (i5 < i3) {
            if (z) {
                i = (i3 - i5) - 1;
                f = 1.0f - this.i1;
            } else {
                f = f2;
                i = i5;
            }
            ConstraintWidget constraintWidget3 = this.x1[i];
            if (constraintWidget3 != null && constraintWidget3.U() != 8) {
                if (i5 == 0) {
                    constraintWidget3.l(constraintWidget3.M, this.M, w1());
                    constraintWidget3.K0(this.c1);
                    constraintWidget3.J0(f);
                }
                if (i5 == i3 - 1) {
                    constraintWidget3.l(constraintWidget3.O, this.O, x1());
                }
                if (i5 > 0 && constraintWidget2 != null) {
                    constraintWidget3.l(constraintWidget3.M, constraintWidget2.O, this.o1);
                    constraintWidget2.l(constraintWidget2.O, constraintWidget3.M, 0);
                }
                constraintWidget2 = constraintWidget3;
            }
            i5++;
            f2 = f;
        }
        for (int i6 = 0; i6 < i4; i6++) {
            ConstraintWidget constraintWidget4 = this.w1[i6];
            if (constraintWidget4 != null && constraintWidget4.U() != 8) {
                if (i6 == 0) {
                    constraintWidget4.l(constraintWidget4.N, this.N, y1());
                    constraintWidget4.b1(this.d1);
                    constraintWidget4.a1(this.j1);
                }
                if (i6 == i4 - 1) {
                    constraintWidget4.l(constraintWidget4.P, this.P, v1());
                }
                if (i6 > 0 && constraintWidget2 != null) {
                    constraintWidget4.l(constraintWidget4.N, constraintWidget2.P, this.p1);
                    constraintWidget2.l(constraintWidget2.P, constraintWidget4.N, 0);
                }
                constraintWidget2 = constraintWidget4;
            }
        }
        for (int i7 = 0; i7 < i3; i7++) {
            for (int i8 = 0; i8 < i4; i8++) {
                int i9 = (i8 * i3) + i7;
                if (this.u1 == 1) {
                    i9 = (i7 * i4) + i8;
                }
                ConstraintWidget[] constraintWidgetArr = this.z1;
                if (i9 < constraintWidgetArr.length && (constraintWidget = constraintWidgetArr[i9]) != null && constraintWidget.U() != 8) {
                    ConstraintWidget constraintWidget5 = this.x1[i7];
                    ConstraintWidget constraintWidget6 = this.w1[i8];
                    if (constraintWidget != constraintWidget5) {
                        constraintWidget.l(constraintWidget.M, constraintWidget5.M, 0);
                        constraintWidget.l(constraintWidget.O, constraintWidget5.O, 0);
                    }
                    if (constraintWidget != constraintWidget6) {
                        constraintWidget.l(constraintWidget.N, constraintWidget6.N, 0);
                        constraintWidget.l(constraintWidget.P, constraintWidget6.P, 0);
                    }
                }
            }
        }
    }

    public final int h2(ConstraintWidget constraintWidget, int i) {
        if (constraintWidget == null) {
            return 0;
        }
        if (constraintWidget.S() == ConstraintWidget.DimensionBehaviour.MATCH_CONSTRAINT) {
            int i2 = constraintWidget.t;
            if (i2 == 0) {
                return 0;
            }
            if (i2 == 2) {
                int i3 = (int) (constraintWidget.A * i);
                if (i3 != constraintWidget.z()) {
                    constraintWidget.V0(true);
                    A1(constraintWidget, constraintWidget.C(), constraintWidget.V(), ConstraintWidget.DimensionBehaviour.FIXED, i3);
                }
                return i3;
            } else if (i2 == 1) {
                return constraintWidget.z();
            } else {
                if (i2 == 3) {
                    return (int) ((constraintWidget.V() * constraintWidget.b0) + 0.5f);
                }
            }
        }
        return constraintWidget.z();
    }

    public final int i2(ConstraintWidget constraintWidget, int i) {
        if (constraintWidget == null) {
            return 0;
        }
        if (constraintWidget.C() == ConstraintWidget.DimensionBehaviour.MATCH_CONSTRAINT) {
            int i2 = constraintWidget.s;
            if (i2 == 0) {
                return 0;
            }
            if (i2 == 2) {
                int i3 = (int) (constraintWidget.x * i);
                if (i3 != constraintWidget.V()) {
                    constraintWidget.V0(true);
                    A1(constraintWidget, ConstraintWidget.DimensionBehaviour.FIXED, i3, constraintWidget.S(), constraintWidget.z());
                }
                return i3;
            } else if (i2 == 1) {
                return constraintWidget.V();
            } else {
                if (i2 == 3) {
                    return (int) ((constraintWidget.z() * constraintWidget.b0) + 0.5f);
                }
            }
        }
        return constraintWidget.V();
    }

    /* JADX WARN: Removed duplicated region for block: B:45:0x0068  */
    /* JADX WARN: Unsupported multi-entry loop pattern (BACK_EDGE: B:105:0x011b -> B:42:0x0063). Please submit an issue!!! */
    /* JADX WARN: Unsupported multi-entry loop pattern (BACK_EDGE: B:106:0x011d -> B:42:0x0063). Please submit an issue!!! */
    /* JADX WARN: Unsupported multi-entry loop pattern (BACK_EDGE: B:108:0x0123 -> B:42:0x0063). Please submit an issue!!! */
    /* JADX WARN: Unsupported multi-entry loop pattern (BACK_EDGE: B:109:0x0125 -> B:42:0x0063). Please submit an issue!!! */
    /*
        Code decompiled incorrectly, please refer to instructions dump.
        To view partially-correct code enable 'Show inconsistent code' option in preferences
    */
    public final void j2(androidx.constraintlayout.core.widgets.ConstraintWidget[] r17, int r18, int r19, int r20, int[] r21) {
        /*
            Method dump skipped, instructions count: 306
            To view this dump change 'Code comments level' option to 'DEBUG'
        */
        throw new UnsupportedOperationException("Method not decompiled: androidx.constraintlayout.core.widgets.e.j2(androidx.constraintlayout.core.widgets.ConstraintWidget[], int, int, int, int[]):void");
    }

    public final void k2(ConstraintWidget[] constraintWidgetArr, int i, int i2, int i3, int[] iArr) {
        int i4;
        int i5;
        int i6;
        ConstraintAnchor constraintAnchor;
        int x1;
        ConstraintAnchor constraintAnchor2;
        int v1;
        int i7;
        if (i == 0) {
            return;
        }
        this.v1.clear();
        a aVar = new a(i2, this.M, this.N, this.O, this.P, i3);
        this.v1.add(aVar);
        if (i2 == 0) {
            i4 = 0;
            int i8 = 0;
            int i9 = 0;
            while (i9 < i) {
                ConstraintWidget constraintWidget = constraintWidgetArr[i9];
                int i22 = i2(constraintWidget, i3);
                if (constraintWidget.C() == ConstraintWidget.DimensionBehaviour.MATCH_CONSTRAINT) {
                    i4++;
                }
                int i10 = i4;
                boolean z = (i8 == i3 || (this.o1 + i8) + i22 > i3) && aVar.b != null;
                if (!z && i9 > 0 && (i7 = this.t1) > 0 && i9 % i7 == 0) {
                    z = true;
                }
                if (z) {
                    aVar = new a(i2, this.M, this.N, this.O, this.P, i3);
                    aVar.i(i9);
                    this.v1.add(aVar);
                } else if (i9 > 0) {
                    i8 += this.o1 + i22;
                    aVar.b(constraintWidget);
                    i9++;
                    i4 = i10;
                }
                i8 = i22;
                aVar.b(constraintWidget);
                i9++;
                i4 = i10;
            }
        } else {
            i4 = 0;
            int i11 = 0;
            int i12 = 0;
            while (i12 < i) {
                ConstraintWidget constraintWidget2 = constraintWidgetArr[i12];
                int h2 = h2(constraintWidget2, i3);
                if (constraintWidget2.S() == ConstraintWidget.DimensionBehaviour.MATCH_CONSTRAINT) {
                    i4++;
                }
                int i13 = i4;
                boolean z2 = (i11 == i3 || (this.p1 + i11) + h2 > i3) && aVar.b != null;
                if (!z2 && i12 > 0 && (i5 = this.t1) > 0 && i12 % i5 == 0) {
                    z2 = true;
                }
                if (z2) {
                    aVar = new a(i2, this.M, this.N, this.O, this.P, i3);
                    aVar.i(i12);
                    this.v1.add(aVar);
                } else if (i12 > 0) {
                    i11 += this.p1 + h2;
                    aVar.b(constraintWidget2);
                    i12++;
                    i4 = i13;
                }
                i11 = h2;
                aVar.b(constraintWidget2);
                i12++;
                i4 = i13;
            }
        }
        int size = this.v1.size();
        ConstraintAnchor constraintAnchor3 = this.M;
        ConstraintAnchor constraintAnchor4 = this.N;
        ConstraintAnchor constraintAnchor5 = this.O;
        ConstraintAnchor constraintAnchor6 = this.P;
        int w1 = w1();
        int y1 = y1();
        int x12 = x1();
        int v12 = v1();
        ConstraintWidget.DimensionBehaviour C = C();
        ConstraintWidget.DimensionBehaviour dimensionBehaviour = ConstraintWidget.DimensionBehaviour.WRAP_CONTENT;
        boolean z3 = C == dimensionBehaviour || S() == dimensionBehaviour;
        if (i4 > 0 && z3) {
            for (int i14 = 0; i14 < size; i14++) {
                a aVar2 = this.v1.get(i14);
                if (i2 == 0) {
                    aVar2.g(i3 - aVar2.f());
                } else {
                    aVar2.g(i3 - aVar2.e());
                }
            }
        }
        int i15 = y1;
        int i16 = x12;
        int i17 = 0;
        int i18 = 0;
        int i19 = 0;
        int i20 = w1;
        ConstraintAnchor constraintAnchor7 = constraintAnchor4;
        ConstraintAnchor constraintAnchor8 = constraintAnchor3;
        int i21 = v12;
        while (i19 < size) {
            a aVar3 = this.v1.get(i19);
            if (i2 == 0) {
                if (i19 < size - 1) {
                    constraintAnchor2 = this.v1.get(i19 + 1).b.N;
                    v1 = 0;
                } else {
                    constraintAnchor2 = this.P;
                    v1 = v1();
                }
                ConstraintAnchor constraintAnchor9 = aVar3.b.P;
                ConstraintAnchor constraintAnchor10 = constraintAnchor8;
                ConstraintAnchor constraintAnchor11 = constraintAnchor8;
                int i23 = i17;
                ConstraintAnchor constraintAnchor12 = constraintAnchor7;
                int i24 = i18;
                ConstraintAnchor constraintAnchor13 = constraintAnchor5;
                ConstraintAnchor constraintAnchor14 = constraintAnchor5;
                i6 = i19;
                aVar3.j(i2, constraintAnchor10, constraintAnchor12, constraintAnchor13, constraintAnchor2, i20, i15, i16, v1, i3);
                int max = Math.max(i24, aVar3.f());
                i17 = i23 + aVar3.e();
                if (i6 > 0) {
                    i17 += this.p1;
                }
                constraintAnchor8 = constraintAnchor11;
                i18 = max;
                i15 = 0;
                constraintAnchor7 = constraintAnchor9;
                constraintAnchor = constraintAnchor14;
                int i25 = v1;
                constraintAnchor6 = constraintAnchor2;
                i21 = i25;
            } else {
                ConstraintAnchor constraintAnchor15 = constraintAnchor8;
                int i26 = i17;
                int i27 = i18;
                i6 = i19;
                if (i6 < size - 1) {
                    constraintAnchor = this.v1.get(i6 + 1).b.M;
                    x1 = 0;
                } else {
                    constraintAnchor = this.O;
                    x1 = x1();
                }
                ConstraintAnchor constraintAnchor16 = aVar3.b.O;
                aVar3.j(i2, constraintAnchor15, constraintAnchor7, constraintAnchor, constraintAnchor6, i20, i15, x1, i21, i3);
                i18 = i27 + aVar3.f();
                int max2 = Math.max(i26, aVar3.e());
                if (i6 > 0) {
                    i18 += this.o1;
                }
                i17 = max2;
                i20 = 0;
                i16 = x1;
                constraintAnchor8 = constraintAnchor16;
            }
            i19 = i6 + 1;
            constraintAnchor5 = constraintAnchor;
        }
        iArr[0] = i18;
        iArr[1] = i17;
    }

    public final void l2(ConstraintWidget[] constraintWidgetArr, int i, int i2, int i3, int[] iArr) {
        a aVar;
        if (i == 0) {
            return;
        }
        if (this.v1.size() == 0) {
            aVar = new a(i2, this.M, this.N, this.O, this.P, i3);
            this.v1.add(aVar);
        } else {
            a aVar2 = this.v1.get(0);
            aVar2.c();
            aVar = aVar2;
            aVar.j(i2, this.M, this.N, this.O, this.P, w1(), y1(), x1(), v1(), i3);
        }
        for (int i4 = 0; i4 < i; i4++) {
            aVar.b(constraintWidgetArr[i4]);
        }
        iArr[0] = aVar.f();
        iArr[1] = aVar.e();
    }

    public void m2(float f) {
        this.k1 = f;
    }

    @Override // defpackage.mk1, androidx.constraintlayout.core.widgets.ConstraintWidget
    public void n(ConstraintWidget constraintWidget, HashMap<ConstraintWidget, ConstraintWidget> hashMap) {
        super.n(constraintWidget, hashMap);
        e eVar = (e) constraintWidget;
        this.c1 = eVar.c1;
        this.d1 = eVar.d1;
        this.e1 = eVar.e1;
        this.f1 = eVar.f1;
        this.g1 = eVar.g1;
        this.h1 = eVar.h1;
        this.i1 = eVar.i1;
        this.j1 = eVar.j1;
        this.k1 = eVar.k1;
        this.l1 = eVar.l1;
        this.m1 = eVar.m1;
        this.n1 = eVar.n1;
        this.o1 = eVar.o1;
        this.p1 = eVar.p1;
        this.q1 = eVar.q1;
        this.r1 = eVar.r1;
        this.s1 = eVar.s1;
        this.t1 = eVar.t1;
        this.u1 = eVar.u1;
    }

    public void n2(int i) {
        this.e1 = i;
    }

    public void o2(float f) {
        this.l1 = f;
    }

    public void p2(int i) {
        this.f1 = i;
    }

    public void q2(int i) {
        this.q1 = i;
    }

    public void r2(float f) {
        this.i1 = f;
    }

    public void s2(int i) {
        this.o1 = i;
    }

    public void t2(int i) {
        this.c1 = i;
    }

    public void u2(float f) {
        this.m1 = f;
    }

    public void v2(int i) {
        this.g1 = i;
    }

    public void w2(float f) {
        this.n1 = f;
    }

    public void x2(int i) {
        this.h1 = i;
    }

    public void y2(int i) {
        this.t1 = i;
    }

    @Override // androidx.constraintlayout.core.widgets.i
    public void z1(int i, int i2, int i3, int i4) {
        int i5;
        int i6;
        int[] iArr;
        boolean z;
        if (this.Q0 > 0 && !B1()) {
            E1(0, 0);
            D1(false);
            return;
        }
        int w1 = w1();
        int x1 = x1();
        int y1 = y1();
        int v1 = v1();
        int[] iArr2 = new int[2];
        int i7 = (i2 - w1) - x1;
        int i8 = this.u1;
        if (i8 == 1) {
            i7 = (i4 - y1) - v1;
        }
        int i9 = i7;
        if (i8 == 0) {
            if (this.c1 == -1) {
                this.c1 = 0;
            }
            if (this.d1 == -1) {
                this.d1 = 0;
            }
        } else {
            if (this.c1 == -1) {
                this.c1 = 0;
            }
            if (this.d1 == -1) {
                this.d1 = 0;
            }
        }
        ConstraintWidget[] constraintWidgetArr = this.P0;
        int i10 = 0;
        int i11 = 0;
        while (true) {
            i5 = this.Q0;
            if (i10 >= i5) {
                break;
            }
            if (this.P0[i10].U() == 8) {
                i11++;
            }
            i10++;
        }
        if (i11 > 0) {
            constraintWidgetArr = new ConstraintWidget[i5 - i11];
            int i12 = 0;
            for (int i13 = 0; i13 < this.Q0; i13++) {
                ConstraintWidget constraintWidget = this.P0[i13];
                if (constraintWidget.U() != 8) {
                    constraintWidgetArr[i12] = constraintWidget;
                    i12++;
                }
            }
            i6 = i12;
        } else {
            i6 = i5;
        }
        this.z1 = constraintWidgetArr;
        this.A1 = i6;
        int i14 = this.s1;
        if (i14 == 0) {
            iArr = iArr2;
            z = true;
            l2(constraintWidgetArr, i6, this.u1, i9, iArr2);
        } else if (i14 == 1) {
            z = true;
            iArr = iArr2;
            k2(constraintWidgetArr, i6, this.u1, i9, iArr2);
        } else if (i14 != 2) {
            z = true;
            iArr = iArr2;
        } else {
            z = true;
            iArr = iArr2;
            j2(constraintWidgetArr, i6, this.u1, i9, iArr2);
        }
        int i15 = iArr[0] + w1 + x1;
        int i16 = iArr[z ? 1 : 0] + y1 + v1;
        if (i == 1073741824) {
            i15 = i2;
        } else if (i == Integer.MIN_VALUE) {
            i15 = Math.min(i15, i2);
        } else if (i != 0) {
            i15 = 0;
        }
        if (i3 == 1073741824) {
            i16 = i4;
        } else if (i3 == Integer.MIN_VALUE) {
            i16 = Math.min(i16, i4);
        } else if (i3 != 0) {
            i16 = 0;
        }
        E1(i15, i16);
        h1(i15);
        I0(i16);
        if (this.Q0 <= 0) {
            z = false;
        }
        D1(z);
    }

    public void z2(int i) {
        this.u1 = i;
    }
}
