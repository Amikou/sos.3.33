package androidx.constraintlayout.core.widgets;

import androidx.constraintlayout.core.SolverVariable;
import androidx.constraintlayout.core.widgets.ConstraintWidget;
import androidx.recyclerview.widget.RecyclerView;
import com.github.mikephil.charting.utils.Utils;
import defpackage.jo;
import java.lang.ref.WeakReference;
import java.util.Arrays;
import java.util.HashSet;
import java.util.Iterator;

/* compiled from: ConstraintWidgetContainer.java */
/* loaded from: classes.dex */
public class d extends bp4 {
    public int S0;
    public int W0;
    public int X0;
    public jo Q0 = new jo(this);
    public lm0 R0 = new lm0(this);
    public jo.b T0 = null;
    public boolean U0 = false;
    public androidx.constraintlayout.core.c V0 = new androidx.constraintlayout.core.c();
    public int Y0 = 0;
    public int Z0 = 0;
    public c[] a1 = new c[4];
    public c[] b1 = new c[4];
    public int c1 = 257;
    public boolean d1 = false;
    public boolean e1 = false;
    public WeakReference<ConstraintAnchor> f1 = null;
    public WeakReference<ConstraintAnchor> g1 = null;
    public WeakReference<ConstraintAnchor> h1 = null;
    public WeakReference<ConstraintAnchor> i1 = null;
    public HashSet<ConstraintWidget> j1 = new HashSet<>();
    public jo.a k1 = new jo.a();

    public static boolean P1(int i, ConstraintWidget constraintWidget, jo.b bVar, jo.a aVar, int i2) {
        int i3;
        int i4;
        if (bVar == null) {
            return false;
        }
        if (constraintWidget.U() != 8 && !(constraintWidget instanceof f) && !(constraintWidget instanceof a)) {
            aVar.a = constraintWidget.C();
            aVar.b = constraintWidget.S();
            aVar.c = constraintWidget.V();
            aVar.d = constraintWidget.z();
            aVar.i = false;
            aVar.j = i2;
            ConstraintWidget.DimensionBehaviour dimensionBehaviour = aVar.a;
            ConstraintWidget.DimensionBehaviour dimensionBehaviour2 = ConstraintWidget.DimensionBehaviour.MATCH_CONSTRAINT;
            boolean z = dimensionBehaviour == dimensionBehaviour2;
            boolean z2 = aVar.b == dimensionBehaviour2;
            boolean z3 = z && constraintWidget.b0 > Utils.FLOAT_EPSILON;
            boolean z4 = z2 && constraintWidget.b0 > Utils.FLOAT_EPSILON;
            if (z && constraintWidget.Z(0) && constraintWidget.s == 0 && !z3) {
                aVar.a = ConstraintWidget.DimensionBehaviour.WRAP_CONTENT;
                if (z2 && constraintWidget.t == 0) {
                    aVar.a = ConstraintWidget.DimensionBehaviour.FIXED;
                }
                z = false;
            }
            if (z2 && constraintWidget.Z(1) && constraintWidget.t == 0 && !z4) {
                aVar.b = ConstraintWidget.DimensionBehaviour.WRAP_CONTENT;
                if (z && constraintWidget.s == 0) {
                    aVar.b = ConstraintWidget.DimensionBehaviour.FIXED;
                }
                z2 = false;
            }
            if (constraintWidget.m0()) {
                aVar.a = ConstraintWidget.DimensionBehaviour.FIXED;
                z = false;
            }
            if (constraintWidget.n0()) {
                aVar.b = ConstraintWidget.DimensionBehaviour.FIXED;
                z2 = false;
            }
            if (z3) {
                if (constraintWidget.u[0] == 4) {
                    aVar.a = ConstraintWidget.DimensionBehaviour.FIXED;
                } else if (!z2) {
                    ConstraintWidget.DimensionBehaviour dimensionBehaviour3 = aVar.b;
                    ConstraintWidget.DimensionBehaviour dimensionBehaviour4 = ConstraintWidget.DimensionBehaviour.FIXED;
                    if (dimensionBehaviour3 == dimensionBehaviour4) {
                        i4 = aVar.d;
                    } else {
                        aVar.a = ConstraintWidget.DimensionBehaviour.WRAP_CONTENT;
                        bVar.b(constraintWidget, aVar);
                        i4 = aVar.f;
                    }
                    aVar.a = dimensionBehaviour4;
                    aVar.c = (int) (constraintWidget.x() * i4);
                }
            }
            if (z4) {
                if (constraintWidget.u[1] == 4) {
                    aVar.b = ConstraintWidget.DimensionBehaviour.FIXED;
                } else if (!z) {
                    ConstraintWidget.DimensionBehaviour dimensionBehaviour5 = aVar.a;
                    ConstraintWidget.DimensionBehaviour dimensionBehaviour6 = ConstraintWidget.DimensionBehaviour.FIXED;
                    if (dimensionBehaviour5 == dimensionBehaviour6) {
                        i3 = aVar.c;
                    } else {
                        aVar.b = ConstraintWidget.DimensionBehaviour.WRAP_CONTENT;
                        bVar.b(constraintWidget, aVar);
                        i3 = aVar.e;
                    }
                    aVar.b = dimensionBehaviour6;
                    if (constraintWidget.y() == -1) {
                        aVar.d = (int) (i3 / constraintWidget.x());
                    } else {
                        aVar.d = (int) (constraintWidget.x() * i3);
                    }
                }
            }
            bVar.b(constraintWidget, aVar);
            constraintWidget.h1(aVar.e);
            constraintWidget.I0(aVar.f);
            constraintWidget.H0(aVar.h);
            constraintWidget.x0(aVar.g);
            aVar.j = jo.a.k;
            return aVar.i;
        }
        aVar.e = 0;
        aVar.f = 0;
        return false;
    }

    public void A1(ConstraintAnchor constraintAnchor) {
        WeakReference<ConstraintAnchor> weakReference = this.h1;
        if (weakReference == null || weakReference.get() == null || constraintAnchor.e() > this.h1.get().e()) {
            this.h1 = new WeakReference<>(constraintAnchor);
        }
    }

    public void B1(ConstraintAnchor constraintAnchor) {
        WeakReference<ConstraintAnchor> weakReference = this.f1;
        if (weakReference == null || weakReference.get() == null || constraintAnchor.e() > this.f1.get().e()) {
            this.f1 = new WeakReference<>(constraintAnchor);
        }
    }

    public boolean C1(boolean z) {
        return this.R0.f(z);
    }

    public boolean D1(boolean z) {
        return this.R0.g(z);
    }

    public boolean E1(boolean z, int i) {
        return this.R0.h(z, i);
    }

    public jo.b F1() {
        return this.T0;
    }

    public int G1() {
        return this.c1;
    }

    public androidx.constraintlayout.core.c H1() {
        return this.V0;
    }

    public boolean I1() {
        return false;
    }

    public void J1() {
        this.R0.j();
    }

    public void K1() {
        this.R0.k();
    }

    public boolean L1() {
        return this.e1;
    }

    public boolean M1() {
        return this.U0;
    }

    public boolean N1() {
        return this.d1;
    }

    public long O1(int i, int i2, int i3, int i4, int i5, int i6, int i7, int i8, int i9) {
        this.W0 = i8;
        this.X0 = i9;
        return this.Q0.d(this, i, i8, i9, i2, i3, i4, i5, i6, i7);
    }

    public boolean Q1(int i) {
        return (this.c1 & i) == i;
    }

    public final void R1() {
        this.Y0 = 0;
        this.Z0 = 0;
    }

    public void S1(jo.b bVar) {
        this.T0 = bVar;
        this.R0.n(bVar);
    }

    public void T1(int i) {
        this.c1 = i;
        androidx.constraintlayout.core.c.r = Q1(RecyclerView.a0.FLAG_ADAPTER_POSITION_UNKNOWN);
    }

    public void U1(int i) {
        this.S0 = i;
    }

    public void V1(boolean z) {
        this.U0 = z;
    }

    public boolean W1(androidx.constraintlayout.core.c cVar, boolean[] zArr) {
        zArr[2] = false;
        boolean Q1 = Q1(64);
        n1(cVar, Q1);
        int size = this.P0.size();
        boolean z = false;
        for (int i = 0; i < size; i++) {
            ConstraintWidget constraintWidget = this.P0.get(i);
            constraintWidget.n1(cVar, Q1);
            if (constraintWidget.b0()) {
                z = true;
            }
        }
        return z;
    }

    public void X1() {
        this.Q0.e(this);
    }

    @Override // androidx.constraintlayout.core.widgets.ConstraintWidget
    public void m1(boolean z, boolean z2) {
        super.m1(z, z2);
        int size = this.P0.size();
        for (int i = 0; i < size; i++) {
            this.P0.get(i).m1(z, z2);
        }
    }

    /* JADX WARN: Removed duplicated region for block: B:154:0x0310  */
    /* JADX WARN: Removed duplicated region for block: B:155:0x0312  */
    /* JADX WARN: Type inference failed for: r6v3 */
    /* JADX WARN: Type inference failed for: r6v4, types: [boolean] */
    /* JADX WARN: Type inference failed for: r6v6 */
    @Override // defpackage.bp4
    /*
        Code decompiled incorrectly, please refer to instructions dump.
        To view partially-correct code enable 'Show inconsistent code' option in preferences
    */
    public void p1() {
        /*
            Method dump skipped, instructions count: 815
            To view this dump change 'Code comments level' option to 'DEBUG'
        */
        throw new UnsupportedOperationException("Method not decompiled: androidx.constraintlayout.core.widgets.d.p1():void");
    }

    @Override // defpackage.bp4, androidx.constraintlayout.core.widgets.ConstraintWidget
    public void s0() {
        this.V0.D();
        this.W0 = 0;
        this.X0 = 0;
        super.s0();
    }

    public void s1(ConstraintWidget constraintWidget, int i) {
        if (i == 0) {
            u1(constraintWidget);
        } else if (i == 1) {
            z1(constraintWidget);
        }
    }

    public boolean t1(androidx.constraintlayout.core.c cVar) {
        boolean Q1 = Q1(64);
        g(cVar, Q1);
        int size = this.P0.size();
        boolean z = false;
        for (int i = 0; i < size; i++) {
            ConstraintWidget constraintWidget = this.P0.get(i);
            constraintWidget.P0(0, false);
            constraintWidget.P0(1, false);
            if (constraintWidget instanceof a) {
                z = true;
            }
        }
        if (z) {
            for (int i2 = 0; i2 < size; i2++) {
                ConstraintWidget constraintWidget2 = this.P0.get(i2);
                if (constraintWidget2 instanceof a) {
                    ((a) constraintWidget2).v1();
                }
            }
        }
        this.j1.clear();
        for (int i3 = 0; i3 < size; i3++) {
            ConstraintWidget constraintWidget3 = this.P0.get(i3);
            if (constraintWidget3.f()) {
                if (constraintWidget3 instanceof i) {
                    this.j1.add(constraintWidget3);
                } else {
                    constraintWidget3.g(cVar, Q1);
                }
            }
        }
        while (this.j1.size() > 0) {
            int size2 = this.j1.size();
            Iterator<ConstraintWidget> it = this.j1.iterator();
            while (true) {
                if (!it.hasNext()) {
                    break;
                }
                i iVar = (i) it.next();
                if (iVar.s1(this.j1)) {
                    iVar.g(cVar, Q1);
                    this.j1.remove(iVar);
                    break;
                }
            }
            if (size2 == this.j1.size()) {
                Iterator<ConstraintWidget> it2 = this.j1.iterator();
                while (it2.hasNext()) {
                    it2.next().g(cVar, Q1);
                }
                this.j1.clear();
            }
        }
        if (androidx.constraintlayout.core.c.r) {
            HashSet<ConstraintWidget> hashSet = new HashSet<>();
            for (int i4 = 0; i4 < size; i4++) {
                ConstraintWidget constraintWidget4 = this.P0.get(i4);
                if (!constraintWidget4.f()) {
                    hashSet.add(constraintWidget4);
                }
            }
            e(this, cVar, hashSet, C() == ConstraintWidget.DimensionBehaviour.WRAP_CONTENT ? 0 : 1, false);
            Iterator<ConstraintWidget> it3 = hashSet.iterator();
            while (it3.hasNext()) {
                ConstraintWidget next = it3.next();
                g.a(this, cVar, next);
                next.g(cVar, Q1);
            }
        } else {
            for (int i5 = 0; i5 < size; i5++) {
                ConstraintWidget constraintWidget5 = this.P0.get(i5);
                if (constraintWidget5 instanceof d) {
                    ConstraintWidget.DimensionBehaviour[] dimensionBehaviourArr = constraintWidget5.X;
                    ConstraintWidget.DimensionBehaviour dimensionBehaviour = dimensionBehaviourArr[0];
                    ConstraintWidget.DimensionBehaviour dimensionBehaviour2 = dimensionBehaviourArr[1];
                    ConstraintWidget.DimensionBehaviour dimensionBehaviour3 = ConstraintWidget.DimensionBehaviour.WRAP_CONTENT;
                    if (dimensionBehaviour == dimensionBehaviour3) {
                        constraintWidget5.M0(ConstraintWidget.DimensionBehaviour.FIXED);
                    }
                    if (dimensionBehaviour2 == dimensionBehaviour3) {
                        constraintWidget5.d1(ConstraintWidget.DimensionBehaviour.FIXED);
                    }
                    constraintWidget5.g(cVar, Q1);
                    if (dimensionBehaviour == dimensionBehaviour3) {
                        constraintWidget5.M0(dimensionBehaviour);
                    }
                    if (dimensionBehaviour2 == dimensionBehaviour3) {
                        constraintWidget5.d1(dimensionBehaviour2);
                    }
                } else {
                    g.a(this, cVar, constraintWidget5);
                    if (!constraintWidget5.f()) {
                        constraintWidget5.g(cVar, Q1);
                    }
                }
            }
        }
        if (this.Y0 > 0) {
            b.b(this, cVar, null, 0);
        }
        if (this.Z0 > 0) {
            b.b(this, cVar, null, 1);
        }
        return true;
    }

    public final void u1(ConstraintWidget constraintWidget) {
        int i = this.Y0 + 1;
        c[] cVarArr = this.b1;
        if (i >= cVarArr.length) {
            this.b1 = (c[]) Arrays.copyOf(cVarArr, cVarArr.length * 2);
        }
        this.b1[this.Y0] = new c(constraintWidget, 0, M1());
        this.Y0++;
    }

    public void v1(ConstraintAnchor constraintAnchor) {
        WeakReference<ConstraintAnchor> weakReference = this.i1;
        if (weakReference == null || weakReference.get() == null || constraintAnchor.e() > this.i1.get().e()) {
            this.i1 = new WeakReference<>(constraintAnchor);
        }
    }

    public void w1(ConstraintAnchor constraintAnchor) {
        WeakReference<ConstraintAnchor> weakReference = this.g1;
        if (weakReference == null || weakReference.get() == null || constraintAnchor.e() > this.g1.get().e()) {
            this.g1 = new WeakReference<>(constraintAnchor);
        }
    }

    public final void x1(ConstraintAnchor constraintAnchor, SolverVariable solverVariable) {
        this.V0.h(solverVariable, this.V0.q(constraintAnchor), 0, 5);
    }

    public final void y1(ConstraintAnchor constraintAnchor, SolverVariable solverVariable) {
        this.V0.h(this.V0.q(constraintAnchor), solverVariable, 0, 5);
    }

    public final void z1(ConstraintWidget constraintWidget) {
        int i = this.Z0 + 1;
        c[] cVarArr = this.a1;
        if (i >= cVarArr.length) {
            this.a1 = (c[]) Arrays.copyOf(cVarArr, cVarArr.length * 2);
        }
        this.a1[this.Z0] = new c(constraintWidget, 1, M1());
        this.Z0++;
    }
}
