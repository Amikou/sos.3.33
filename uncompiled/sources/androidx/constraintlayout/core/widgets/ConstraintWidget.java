package androidx.constraintlayout.core.widgets;

import androidx.constraintlayout.core.widgets.ConstraintAnchor;
import androidx.constraintlayout.core.widgets.analyzer.DependencyNode;
import androidx.constraintlayout.core.widgets.analyzer.WidgetRun;
import com.fasterxml.jackson.core.util.MinimalPrettyPrinter;
import com.github.mikephil.charting.utils.Utils;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;

/* loaded from: classes.dex */
public class ConstraintWidget {
    public static float O0 = 0.5f;
    public float A;
    public boolean A0;
    public boolean B;
    public boolean B0;
    public boolean C;
    public boolean C0;
    public int D;
    public int D0;
    public float E;
    public int E0;
    public int[] F;
    public boolean F0;
    public float G;
    public boolean G0;
    public boolean H;
    public float[] H0;
    public boolean I;
    public ConstraintWidget[] I0;
    public boolean J;
    public ConstraintWidget[] J0;
    public int K;
    public ConstraintWidget K0;
    public int L;
    public ConstraintWidget L0;
    public ConstraintAnchor M;
    public int M0;
    public ConstraintAnchor N;
    public int N0;
    public ConstraintAnchor O;
    public ConstraintAnchor P;
    public ConstraintAnchor Q;
    public ConstraintAnchor R;
    public ConstraintAnchor S;
    public ConstraintAnchor T;
    public ConstraintAnchor[] U;
    public ArrayList<ConstraintAnchor> V;
    public boolean[] W;
    public DimensionBehaviour[] X;
    public ConstraintWidget Y;
    public int Z;
    public int a0;
    public gx b;
    public float b0;
    public gx c;
    public int c0;
    public int d0;
    public int e0;
    public int f0;
    public int g0;
    public int h0;
    public int i0;
    public int j0;
    public int k0;
    public boolean l;
    public int l0;
    public boolean m;
    public float m0;
    public boolean n;
    public float n0;
    public boolean o;
    public Object o0;
    public int p;
    public int p0;
    public int q;
    public int q0;
    public int r;
    public String r0;
    public int s;
    public String s0;
    public int t;
    public int t0;
    public int[] u;
    public int u0;
    public int v;
    public int v0;
    public int w;
    public int w0;
    public float x;
    public boolean x0;
    public int y;
    public boolean y0;
    public int z;
    public boolean z0;
    public boolean a = false;
    public androidx.constraintlayout.core.widgets.analyzer.c d = null;
    public androidx.constraintlayout.core.widgets.analyzer.d e = null;
    public boolean[] f = {true, true};
    public boolean g = true;
    public boolean h = false;
    public boolean i = true;
    public int j = -1;
    public int k = -1;

    /* loaded from: classes.dex */
    public enum DimensionBehaviour {
        FIXED,
        WRAP_CONTENT,
        MATCH_CONSTRAINT,
        MATCH_PARENT
    }

    /* loaded from: classes.dex */
    public static /* synthetic */ class a {
        public static final /* synthetic */ int[] a;
        public static final /* synthetic */ int[] b;

        static {
            int[] iArr = new int[DimensionBehaviour.values().length];
            b = iArr;
            try {
                iArr[DimensionBehaviour.FIXED.ordinal()] = 1;
            } catch (NoSuchFieldError unused) {
            }
            try {
                b[DimensionBehaviour.WRAP_CONTENT.ordinal()] = 2;
            } catch (NoSuchFieldError unused2) {
            }
            try {
                b[DimensionBehaviour.MATCH_PARENT.ordinal()] = 3;
            } catch (NoSuchFieldError unused3) {
            }
            try {
                b[DimensionBehaviour.MATCH_CONSTRAINT.ordinal()] = 4;
            } catch (NoSuchFieldError unused4) {
            }
            int[] iArr2 = new int[ConstraintAnchor.Type.values().length];
            a = iArr2;
            try {
                iArr2[ConstraintAnchor.Type.LEFT.ordinal()] = 1;
            } catch (NoSuchFieldError unused5) {
            }
            try {
                a[ConstraintAnchor.Type.TOP.ordinal()] = 2;
            } catch (NoSuchFieldError unused6) {
            }
            try {
                a[ConstraintAnchor.Type.RIGHT.ordinal()] = 3;
            } catch (NoSuchFieldError unused7) {
            }
            try {
                a[ConstraintAnchor.Type.BOTTOM.ordinal()] = 4;
            } catch (NoSuchFieldError unused8) {
            }
            try {
                a[ConstraintAnchor.Type.BASELINE.ordinal()] = 5;
            } catch (NoSuchFieldError unused9) {
            }
            try {
                a[ConstraintAnchor.Type.CENTER.ordinal()] = 6;
            } catch (NoSuchFieldError unused10) {
            }
            try {
                a[ConstraintAnchor.Type.CENTER_X.ordinal()] = 7;
            } catch (NoSuchFieldError unused11) {
            }
            try {
                a[ConstraintAnchor.Type.CENTER_Y.ordinal()] = 8;
            } catch (NoSuchFieldError unused12) {
            }
            try {
                a[ConstraintAnchor.Type.NONE.ordinal()] = 9;
            } catch (NoSuchFieldError unused13) {
            }
        }
    }

    public ConstraintWidget() {
        new cp4(this);
        this.l = false;
        this.m = false;
        this.n = false;
        this.o = false;
        this.p = -1;
        this.q = -1;
        this.r = 0;
        this.s = 0;
        this.t = 0;
        this.u = new int[2];
        this.v = 0;
        this.w = 0;
        this.x = 1.0f;
        this.y = 0;
        this.z = 0;
        this.A = 1.0f;
        this.D = -1;
        this.E = 1.0f;
        this.F = new int[]{Integer.MAX_VALUE, Integer.MAX_VALUE};
        this.G = Utils.FLOAT_EPSILON;
        this.H = false;
        this.J = false;
        this.K = 0;
        this.L = 0;
        this.M = new ConstraintAnchor(this, ConstraintAnchor.Type.LEFT);
        this.N = new ConstraintAnchor(this, ConstraintAnchor.Type.TOP);
        this.O = new ConstraintAnchor(this, ConstraintAnchor.Type.RIGHT);
        this.P = new ConstraintAnchor(this, ConstraintAnchor.Type.BOTTOM);
        this.Q = new ConstraintAnchor(this, ConstraintAnchor.Type.BASELINE);
        this.R = new ConstraintAnchor(this, ConstraintAnchor.Type.CENTER_X);
        this.S = new ConstraintAnchor(this, ConstraintAnchor.Type.CENTER_Y);
        ConstraintAnchor constraintAnchor = new ConstraintAnchor(this, ConstraintAnchor.Type.CENTER);
        this.T = constraintAnchor;
        this.U = new ConstraintAnchor[]{this.M, this.O, this.N, this.P, this.Q, constraintAnchor};
        this.V = new ArrayList<>();
        this.W = new boolean[2];
        DimensionBehaviour dimensionBehaviour = DimensionBehaviour.FIXED;
        this.X = new DimensionBehaviour[]{dimensionBehaviour, dimensionBehaviour};
        this.Y = null;
        this.Z = 0;
        this.a0 = 0;
        this.b0 = Utils.FLOAT_EPSILON;
        this.c0 = -1;
        this.d0 = 0;
        this.e0 = 0;
        this.f0 = 0;
        this.g0 = 0;
        this.h0 = 0;
        this.i0 = 0;
        this.j0 = 0;
        float f = O0;
        this.m0 = f;
        this.n0 = f;
        this.p0 = 0;
        this.q0 = 0;
        this.r0 = null;
        this.s0 = null;
        this.D0 = 0;
        this.E0 = 0;
        this.H0 = new float[]{-1.0f, -1.0f};
        this.I0 = new ConstraintWidget[]{null, null};
        this.J0 = new ConstraintWidget[]{null, null};
        this.K0 = null;
        this.L0 = null;
        this.M0 = -1;
        this.N0 = -1;
        d();
    }

    public float A() {
        return this.m0;
    }

    /* JADX WARN: Unsupported multi-entry loop pattern (BACK_EDGE: B:38:0x0084 -> B:39:0x0085). Please submit an issue!!! */
    public void A0(String str) {
        float f;
        int i = 0;
        if (str != null && str.length() != 0) {
            int i2 = -1;
            int length = str.length();
            int indexOf = str.indexOf(44);
            int i3 = 0;
            if (indexOf > 0 && indexOf < length - 1) {
                String substring = str.substring(0, indexOf);
                if (substring.equalsIgnoreCase("W")) {
                    i2 = 0;
                } else if (substring.equalsIgnoreCase("H")) {
                    i2 = 1;
                }
                i3 = indexOf + 1;
            }
            int indexOf2 = str.indexOf(58);
            if (indexOf2 >= 0 && indexOf2 < length - 1) {
                String substring2 = str.substring(i3, indexOf2);
                String substring3 = str.substring(indexOf2 + 1);
                if (substring2.length() > 0 && substring3.length() > 0) {
                    float parseFloat = Float.parseFloat(substring2);
                    float parseFloat2 = Float.parseFloat(substring3);
                    if (parseFloat > Utils.FLOAT_EPSILON && parseFloat2 > Utils.FLOAT_EPSILON) {
                        if (i2 == 1) {
                            f = Math.abs(parseFloat2 / parseFloat);
                        } else {
                            f = Math.abs(parseFloat / parseFloat2);
                        }
                    }
                }
                f = i;
            } else {
                String substring4 = str.substring(i3);
                if (substring4.length() > 0) {
                    f = Float.parseFloat(substring4);
                }
                f = i;
            }
            i = (f > i ? 1 : (f == i ? 0 : -1));
            if (i > 0) {
                this.b0 = f;
                this.c0 = i2;
                return;
            }
            return;
        }
        this.b0 = Utils.FLOAT_EPSILON;
    }

    public int B() {
        return this.D0;
    }

    public void B0(int i) {
        if (this.H) {
            int i2 = i - this.j0;
            int i3 = this.a0 + i2;
            this.e0 = i2;
            this.N.t(i2);
            this.P.t(i3);
            this.Q.t(i);
            this.m = true;
        }
    }

    public DimensionBehaviour C() {
        return this.X[0];
    }

    public void C0(int i, int i2) {
        if (this.l) {
            return;
        }
        this.M.t(i);
        this.O.t(i2);
        this.d0 = i;
        this.Z = i2 - i;
        this.l = true;
    }

    public int D() {
        ConstraintAnchor constraintAnchor = this.M;
        int i = constraintAnchor != null ? 0 + constraintAnchor.g : 0;
        ConstraintAnchor constraintAnchor2 = this.O;
        return constraintAnchor2 != null ? i + constraintAnchor2.g : i;
    }

    public void D0(int i) {
        this.M.t(i);
        this.d0 = i;
    }

    public int E() {
        return this.K;
    }

    public void E0(int i) {
        this.N.t(i);
        this.e0 = i;
    }

    public int F() {
        return this.L;
    }

    public void F0(int i, int i2) {
        if (this.m) {
            return;
        }
        this.N.t(i);
        this.P.t(i2);
        this.e0 = i;
        this.a0 = i2 - i;
        if (this.H) {
            this.Q.t(i + this.j0);
        }
        this.m = true;
    }

    public int G(int i) {
        if (i == 0) {
            return V();
        }
        if (i == 1) {
            return z();
        }
        return 0;
    }

    public void G0(int i, int i2, int i3, int i4) {
        int i5;
        int i6;
        int i7 = i3 - i;
        int i8 = i4 - i2;
        this.d0 = i;
        this.e0 = i2;
        if (this.q0 == 8) {
            this.Z = 0;
            this.a0 = 0;
            return;
        }
        DimensionBehaviour[] dimensionBehaviourArr = this.X;
        DimensionBehaviour dimensionBehaviour = dimensionBehaviourArr[0];
        DimensionBehaviour dimensionBehaviour2 = DimensionBehaviour.FIXED;
        if (dimensionBehaviour == dimensionBehaviour2 && i7 < (i6 = this.Z)) {
            i7 = i6;
        }
        if (dimensionBehaviourArr[1] == dimensionBehaviour2 && i8 < (i5 = this.a0)) {
            i8 = i5;
        }
        this.Z = i7;
        this.a0 = i8;
        int i9 = this.l0;
        if (i8 < i9) {
            this.a0 = i9;
        }
        int i10 = this.k0;
        if (i7 < i10) {
            this.Z = i10;
        }
        int i11 = this.w;
        if (i11 > 0 && dimensionBehaviourArr[0] == DimensionBehaviour.MATCH_CONSTRAINT) {
            this.Z = Math.min(this.Z, i11);
        }
        int i12 = this.z;
        if (i12 > 0 && this.X[1] == DimensionBehaviour.MATCH_CONSTRAINT) {
            this.a0 = Math.min(this.a0, i12);
        }
        int i13 = this.Z;
        if (i7 != i13) {
            this.j = i13;
        }
        int i14 = this.a0;
        if (i8 != i14) {
            this.k = i14;
        }
    }

    public int H() {
        return this.F[1];
    }

    public void H0(boolean z) {
        this.H = z;
    }

    public int I() {
        return this.F[0];
    }

    public void I0(int i) {
        this.a0 = i;
        int i2 = this.l0;
        if (i < i2) {
            this.a0 = i2;
        }
    }

    public int J() {
        return this.l0;
    }

    public void J0(float f) {
        this.m0 = f;
    }

    public int K() {
        return this.k0;
    }

    public void K0(int i) {
        this.D0 = i;
    }

    public ConstraintWidget L(int i) {
        ConstraintAnchor constraintAnchor;
        ConstraintAnchor constraintAnchor2;
        if (i != 0) {
            if (i == 1 && (constraintAnchor2 = (constraintAnchor = this.P).f) != null && constraintAnchor2.f == constraintAnchor) {
                return constraintAnchor2.d;
            }
            return null;
        }
        ConstraintAnchor constraintAnchor3 = this.O;
        ConstraintAnchor constraintAnchor4 = constraintAnchor3.f;
        if (constraintAnchor4 == null || constraintAnchor4.f != constraintAnchor3) {
            return null;
        }
        return constraintAnchor4.d;
    }

    public void L0(int i, int i2) {
        this.d0 = i;
        int i3 = i2 - i;
        this.Z = i3;
        int i4 = this.k0;
        if (i3 < i4) {
            this.Z = i4;
        }
    }

    public ConstraintWidget M() {
        return this.Y;
    }

    public void M0(DimensionBehaviour dimensionBehaviour) {
        this.X[0] = dimensionBehaviour;
    }

    public ConstraintWidget N(int i) {
        ConstraintAnchor constraintAnchor;
        ConstraintAnchor constraintAnchor2;
        if (i != 0) {
            if (i == 1 && (constraintAnchor2 = (constraintAnchor = this.N).f) != null && constraintAnchor2.f == constraintAnchor) {
                return constraintAnchor2.d;
            }
            return null;
        }
        ConstraintAnchor constraintAnchor3 = this.M;
        ConstraintAnchor constraintAnchor4 = constraintAnchor3.f;
        if (constraintAnchor4 == null || constraintAnchor4.f != constraintAnchor3) {
            return null;
        }
        return constraintAnchor4.d;
    }

    public void N0(int i, int i2, int i3, float f) {
        this.s = i;
        this.v = i2;
        if (i3 == Integer.MAX_VALUE) {
            i3 = 0;
        }
        this.w = i3;
        this.x = f;
        if (f <= Utils.FLOAT_EPSILON || f >= 1.0f || i != 0) {
            return;
        }
        this.s = 2;
    }

    public int O() {
        return W() + this.Z;
    }

    public void O0(float f) {
        this.H0[0] = f;
    }

    public WidgetRun P(int i) {
        if (i == 0) {
            return this.d;
        }
        if (i == 1) {
            return this.e;
        }
        return null;
    }

    public void P0(int i, boolean z) {
        this.W[i] = z;
    }

    public float Q() {
        return this.n0;
    }

    public void Q0(boolean z) {
        this.I = z;
    }

    public int R() {
        return this.E0;
    }

    public void R0(boolean z) {
        this.J = z;
    }

    public DimensionBehaviour S() {
        return this.X[1];
    }

    public void S0(int i, int i2) {
        this.K = i;
        this.L = i2;
        V0(false);
    }

    public int T() {
        int i = this.M != null ? 0 + this.N.g : 0;
        return this.O != null ? i + this.P.g : i;
    }

    public void T0(int i) {
        this.F[1] = i;
    }

    public int U() {
        return this.q0;
    }

    public void U0(int i) {
        this.F[0] = i;
    }

    public int V() {
        if (this.q0 == 8) {
            return 0;
        }
        return this.Z;
    }

    public void V0(boolean z) {
        this.g = z;
    }

    public int W() {
        ConstraintWidget constraintWidget = this.Y;
        if (constraintWidget != null && (constraintWidget instanceof d)) {
            return ((d) constraintWidget).W0 + this.d0;
        }
        return this.d0;
    }

    public void W0(int i) {
        if (i < 0) {
            this.l0 = 0;
        } else {
            this.l0 = i;
        }
    }

    public int X() {
        ConstraintWidget constraintWidget = this.Y;
        if (constraintWidget != null && (constraintWidget instanceof d)) {
            return ((d) constraintWidget).X0 + this.e0;
        }
        return this.e0;
    }

    public void X0(int i) {
        if (i < 0) {
            this.k0 = 0;
        } else {
            this.k0 = i;
        }
    }

    public boolean Y() {
        return this.H;
    }

    public void Y0(int i, int i2) {
        this.d0 = i;
        this.e0 = i2;
    }

    public boolean Z(int i) {
        if (i == 0) {
            return (this.M.f != null ? 1 : 0) + (this.O.f != null ? 1 : 0) < 2;
        }
        return ((this.N.f != null ? 1 : 0) + (this.P.f != null ? 1 : 0)) + (this.Q.f != null ? 1 : 0) < 2;
    }

    public void Z0(ConstraintWidget constraintWidget) {
        this.Y = constraintWidget;
    }

    public boolean a0() {
        int size = this.V.size();
        for (int i = 0; i < size; i++) {
            if (this.V.get(i).m()) {
                return true;
            }
        }
        return false;
    }

    public void a1(float f) {
        this.n0 = f;
    }

    public boolean b0() {
        return (this.j == -1 && this.k == -1) ? false : true;
    }

    public void b1(int i) {
        this.E0 = i;
    }

    public boolean c0(int i, int i2) {
        ConstraintAnchor constraintAnchor;
        ConstraintAnchor constraintAnchor2;
        if (i == 0) {
            ConstraintAnchor constraintAnchor3 = this.M.f;
            return constraintAnchor3 != null && constraintAnchor3.n() && (constraintAnchor2 = this.O.f) != null && constraintAnchor2.n() && (this.O.f.e() - this.O.f()) - (this.M.f.e() + this.M.f()) >= i2;
        }
        ConstraintAnchor constraintAnchor4 = this.N.f;
        return constraintAnchor4 != null && constraintAnchor4.n() && (constraintAnchor = this.P.f) != null && constraintAnchor.n() && (this.P.f.e() - this.P.f()) - (this.N.f.e() + this.N.f()) >= i2;
        return false;
    }

    public void c1(int i, int i2) {
        this.e0 = i;
        int i3 = i2 - i;
        this.a0 = i3;
        int i4 = this.l0;
        if (i3 < i4) {
            this.a0 = i4;
        }
    }

    public final void d() {
        this.V.add(this.M);
        this.V.add(this.N);
        this.V.add(this.O);
        this.V.add(this.P);
        this.V.add(this.R);
        this.V.add(this.S);
        this.V.add(this.T);
        this.V.add(this.Q);
    }

    public void d0(ConstraintAnchor.Type type, ConstraintWidget constraintWidget, ConstraintAnchor.Type type2, int i, int i2) {
        q(type).b(constraintWidget.q(type2), i, i2, true);
    }

    public void d1(DimensionBehaviour dimensionBehaviour) {
        this.X[1] = dimensionBehaviour;
    }

    public void e(d dVar, androidx.constraintlayout.core.c cVar, HashSet<ConstraintWidget> hashSet, int i, boolean z) {
        if (z) {
            if (!hashSet.contains(this)) {
                return;
            }
            g.a(dVar, cVar, this);
            hashSet.remove(this);
            g(cVar, dVar.Q1(64));
        }
        if (i == 0) {
            HashSet<ConstraintAnchor> d = this.M.d();
            if (d != null) {
                Iterator<ConstraintAnchor> it = d.iterator();
                while (it.hasNext()) {
                    it.next().d.e(dVar, cVar, hashSet, i, true);
                }
            }
            HashSet<ConstraintAnchor> d2 = this.O.d();
            if (d2 != null) {
                Iterator<ConstraintAnchor> it2 = d2.iterator();
                while (it2.hasNext()) {
                    it2.next().d.e(dVar, cVar, hashSet, i, true);
                }
                return;
            }
            return;
        }
        HashSet<ConstraintAnchor> d3 = this.N.d();
        if (d3 != null) {
            Iterator<ConstraintAnchor> it3 = d3.iterator();
            while (it3.hasNext()) {
                it3.next().d.e(dVar, cVar, hashSet, i, true);
            }
        }
        HashSet<ConstraintAnchor> d4 = this.P.d();
        if (d4 != null) {
            Iterator<ConstraintAnchor> it4 = d4.iterator();
            while (it4.hasNext()) {
                it4.next().d.e(dVar, cVar, hashSet, i, true);
            }
        }
        HashSet<ConstraintAnchor> d5 = this.Q.d();
        if (d5 != null) {
            Iterator<ConstraintAnchor> it5 = d5.iterator();
            while (it5.hasNext()) {
                it5.next().d.e(dVar, cVar, hashSet, i, true);
            }
        }
    }

    public final boolean e0(int i) {
        int i2 = i * 2;
        ConstraintAnchor[] constraintAnchorArr = this.U;
        if (constraintAnchorArr[i2].f != null && constraintAnchorArr[i2].f.f != constraintAnchorArr[i2]) {
            int i3 = i2 + 1;
            if (constraintAnchorArr[i3].f != null && constraintAnchorArr[i3].f.f == constraintAnchorArr[i3]) {
                return true;
            }
        }
        return false;
    }

    public void e1(int i, int i2, int i3, float f) {
        this.t = i;
        this.y = i2;
        if (i3 == Integer.MAX_VALUE) {
            i3 = 0;
        }
        this.z = i3;
        this.A = f;
        if (f <= Utils.FLOAT_EPSILON || f >= 1.0f || i != 0) {
            return;
        }
        this.t = 2;
    }

    public boolean f() {
        return (this instanceof i) || (this instanceof f);
    }

    public boolean f0() {
        return this.n;
    }

    public void f1(float f) {
        this.H0[1] = f;
    }

    /* JADX WARN: Removed duplicated region for block: B:123:0x01ed  */
    /* JADX WARN: Removed duplicated region for block: B:127:0x01f8  */
    /* JADX WARN: Removed duplicated region for block: B:131:0x0200  */
    /* JADX WARN: Removed duplicated region for block: B:134:0x020b  */
    /* JADX WARN: Removed duplicated region for block: B:135:0x020d  */
    /* JADX WARN: Removed duplicated region for block: B:138:0x0216  */
    /* JADX WARN: Removed duplicated region for block: B:139:0x0218  */
    /* JADX WARN: Removed duplicated region for block: B:142:0x0235  */
    /* JADX WARN: Removed duplicated region for block: B:181:0x02c1  */
    /* JADX WARN: Removed duplicated region for block: B:185:0x02d8  */
    /* JADX WARN: Removed duplicated region for block: B:189:0x02e2  */
    /* JADX WARN: Removed duplicated region for block: B:192:0x02e7  */
    /* JADX WARN: Removed duplicated region for block: B:199:0x02fc  */
    /* JADX WARN: Removed duplicated region for block: B:204:0x0305  */
    /* JADX WARN: Removed duplicated region for block: B:205:0x0307  */
    /* JADX WARN: Removed duplicated region for block: B:208:0x031f  */
    /* JADX WARN: Removed duplicated region for block: B:219:0x0338  */
    /* JADX WARN: Removed duplicated region for block: B:228:0x0380  */
    /* JADX WARN: Removed duplicated region for block: B:231:0x0390  */
    /* JADX WARN: Removed duplicated region for block: B:232:0x0398  */
    /* JADX WARN: Removed duplicated region for block: B:235:0x039e  */
    /* JADX WARN: Removed duplicated region for block: B:236:0x03a7  */
    /* JADX WARN: Removed duplicated region for block: B:239:0x03cb  */
    /* JADX WARN: Removed duplicated region for block: B:240:0x03ce  */
    /* JADX WARN: Removed duplicated region for block: B:244:0x043d  */
    /* JADX WARN: Removed duplicated region for block: B:261:0x04a1  */
    /* JADX WARN: Removed duplicated region for block: B:265:0x04b5  */
    /* JADX WARN: Removed duplicated region for block: B:266:0x04b7  */
    /* JADX WARN: Removed duplicated region for block: B:268:0x04ba  */
    /* JADX WARN: Removed duplicated region for block: B:303:0x0555  */
    /* JADX WARN: Removed duplicated region for block: B:304:0x0558  */
    /* JADX WARN: Removed duplicated region for block: B:308:0x059e  */
    /* JADX WARN: Removed duplicated region for block: B:312:0x05c9  */
    /* JADX WARN: Removed duplicated region for block: B:315:0x05d3  */
    /* JADX WARN: Removed duplicated region for block: B:36:0x0079  */
    /* JADX WARN: Removed duplicated region for block: B:46:0x00aa  */
    /* JADX WARN: Removed duplicated region for block: B:78:0x0145  */
    /* JADX WARN: Removed duplicated region for block: B:94:0x0180  */
    /*
        Code decompiled incorrectly, please refer to instructions dump.
        To view partially-correct code enable 'Show inconsistent code' option in preferences
    */
    public void g(androidx.constraintlayout.core.c r54, boolean r55) {
        /*
            Method dump skipped, instructions count: 1529
            To view this dump change 'Code comments level' option to 'DEBUG'
        */
        throw new UnsupportedOperationException("Method not decompiled: androidx.constraintlayout.core.widgets.ConstraintWidget.g(androidx.constraintlayout.core.c, boolean):void");
    }

    public boolean g0(int i) {
        return this.W[i];
    }

    public void g1(int i) {
        this.q0 = i;
    }

    public boolean h() {
        return this.q0 != 8;
    }

    public boolean h0() {
        ConstraintAnchor constraintAnchor = this.M;
        ConstraintAnchor constraintAnchor2 = constraintAnchor.f;
        if (constraintAnchor2 == null || constraintAnchor2.f != constraintAnchor) {
            ConstraintAnchor constraintAnchor3 = this.O;
            ConstraintAnchor constraintAnchor4 = constraintAnchor3.f;
            return constraintAnchor4 != null && constraintAnchor4.f == constraintAnchor3;
        }
        return true;
    }

    public void h1(int i) {
        this.Z = i;
        int i2 = this.k0;
        if (i < i2) {
            this.Z = i2;
        }
    }

    /* JADX WARN: Code restructure failed: missing block: B:268:0x049c, code lost:
        if ((r4 instanceof androidx.constraintlayout.core.widgets.a) != false) goto L211;
     */
    /* JADX WARN: Removed duplicated region for block: B:115:0x01e8 A[ADDED_TO_REGION] */
    /* JADX WARN: Removed duplicated region for block: B:206:0x0340  */
    /* JADX WARN: Removed duplicated region for block: B:207:0x0344  */
    /* JADX WARN: Removed duplicated region for block: B:246:0x0422  */
    /* JADX WARN: Removed duplicated region for block: B:254:0x0469  */
    /* JADX WARN: Removed duplicated region for block: B:262:0x048c  */
    /* JADX WARN: Removed duplicated region for block: B:275:0x04be  */
    /* JADX WARN: Removed duplicated region for block: B:288:0x04e2  */
    /* JADX WARN: Removed duplicated region for block: B:319:0x052d  */
    /* JADX WARN: Removed duplicated region for block: B:326:0x053f A[ADDED_TO_REGION] */
    /* JADX WARN: Removed duplicated region for block: B:338:0x055b A[ADDED_TO_REGION] */
    /* JADX WARN: Removed duplicated region for block: B:359:0x0592 A[ADDED_TO_REGION] */
    /* JADX WARN: Removed duplicated region for block: B:35:0x0092  */
    /* JADX WARN: Removed duplicated region for block: B:36:0x0096  */
    /* JADX WARN: Removed duplicated region for block: B:386:? A[ADDED_TO_REGION, RETURN, SYNTHETIC] */
    /* JADX WARN: Removed duplicated region for block: B:389:? A[ADDED_TO_REGION, RETURN, SYNTHETIC] */
    /* JADX WARN: Removed duplicated region for block: B:38:0x009a  */
    /* JADX WARN: Removed duplicated region for block: B:46:0x00bb  */
    /* JADX WARN: Removed duplicated region for block: B:48:0x00c0  */
    /* JADX WARN: Removed duplicated region for block: B:58:0x00e8  */
    /*
        Code decompiled incorrectly, please refer to instructions dump.
        To view partially-correct code enable 'Show inconsistent code' option in preferences
    */
    public final void i(androidx.constraintlayout.core.c r38, boolean r39, boolean r40, boolean r41, boolean r42, androidx.constraintlayout.core.SolverVariable r43, androidx.constraintlayout.core.SolverVariable r44, androidx.constraintlayout.core.widgets.ConstraintWidget.DimensionBehaviour r45, boolean r46, androidx.constraintlayout.core.widgets.ConstraintAnchor r47, androidx.constraintlayout.core.widgets.ConstraintAnchor r48, int r49, int r50, int r51, int r52, float r53, boolean r54, boolean r55, boolean r56, boolean r57, boolean r58, int r59, int r60, int r61, int r62, float r63, boolean r64) {
        /*
            Method dump skipped, instructions count: 1485
            To view this dump change 'Code comments level' option to 'DEBUG'
        */
        throw new UnsupportedOperationException("Method not decompiled: androidx.constraintlayout.core.widgets.ConstraintWidget.i(androidx.constraintlayout.core.c, boolean, boolean, boolean, boolean, androidx.constraintlayout.core.SolverVariable, androidx.constraintlayout.core.SolverVariable, androidx.constraintlayout.core.widgets.ConstraintWidget$DimensionBehaviour, boolean, androidx.constraintlayout.core.widgets.ConstraintAnchor, androidx.constraintlayout.core.widgets.ConstraintAnchor, int, int, int, int, float, boolean, boolean, boolean, boolean, boolean, int, int, int, int, float, boolean):void");
    }

    public boolean i0() {
        return this.I;
    }

    public void i1(int i) {
        if (i < 0 || i > 3) {
            return;
        }
        this.r = i;
    }

    public void j(ConstraintAnchor.Type type, ConstraintWidget constraintWidget, ConstraintAnchor.Type type2) {
        k(type, constraintWidget, type2, 0);
    }

    public boolean j0() {
        ConstraintAnchor constraintAnchor = this.N;
        ConstraintAnchor constraintAnchor2 = constraintAnchor.f;
        if (constraintAnchor2 == null || constraintAnchor2.f != constraintAnchor) {
            ConstraintAnchor constraintAnchor3 = this.P;
            ConstraintAnchor constraintAnchor4 = constraintAnchor3.f;
            return constraintAnchor4 != null && constraintAnchor4.f == constraintAnchor3;
        }
        return true;
    }

    public void j1(int i) {
        this.d0 = i;
    }

    public void k(ConstraintAnchor.Type type, ConstraintWidget constraintWidget, ConstraintAnchor.Type type2, int i) {
        ConstraintAnchor.Type type3;
        ConstraintAnchor.Type type4;
        boolean z;
        ConstraintAnchor.Type type5 = ConstraintAnchor.Type.CENTER;
        if (type == type5) {
            if (type2 == type5) {
                ConstraintAnchor.Type type6 = ConstraintAnchor.Type.LEFT;
                ConstraintAnchor q = q(type6);
                ConstraintAnchor.Type type7 = ConstraintAnchor.Type.RIGHT;
                ConstraintAnchor q2 = q(type7);
                ConstraintAnchor.Type type8 = ConstraintAnchor.Type.TOP;
                ConstraintAnchor q3 = q(type8);
                ConstraintAnchor.Type type9 = ConstraintAnchor.Type.BOTTOM;
                ConstraintAnchor q4 = q(type9);
                boolean z2 = true;
                if ((q == null || !q.o()) && (q2 == null || !q2.o())) {
                    k(type6, constraintWidget, type6, 0);
                    k(type7, constraintWidget, type7, 0);
                    z = true;
                } else {
                    z = false;
                }
                if ((q3 == null || !q3.o()) && (q4 == null || !q4.o())) {
                    k(type8, constraintWidget, type8, 0);
                    k(type9, constraintWidget, type9, 0);
                } else {
                    z2 = false;
                }
                if (z && z2) {
                    q(type5).a(constraintWidget.q(type5), 0);
                    return;
                } else if (z) {
                    ConstraintAnchor.Type type10 = ConstraintAnchor.Type.CENTER_X;
                    q(type10).a(constraintWidget.q(type10), 0);
                    return;
                } else if (z2) {
                    ConstraintAnchor.Type type11 = ConstraintAnchor.Type.CENTER_Y;
                    q(type11).a(constraintWidget.q(type11), 0);
                    return;
                } else {
                    return;
                }
            }
            ConstraintAnchor.Type type12 = ConstraintAnchor.Type.LEFT;
            if (type2 != type12 && type2 != ConstraintAnchor.Type.RIGHT) {
                ConstraintAnchor.Type type13 = ConstraintAnchor.Type.TOP;
                if (type2 == type13 || type2 == ConstraintAnchor.Type.BOTTOM) {
                    k(type13, constraintWidget, type2, 0);
                    k(ConstraintAnchor.Type.BOTTOM, constraintWidget, type2, 0);
                    q(type5).a(constraintWidget.q(type2), 0);
                    return;
                }
                return;
            }
            k(type12, constraintWidget, type2, 0);
            k(ConstraintAnchor.Type.RIGHT, constraintWidget, type2, 0);
            q(type5).a(constraintWidget.q(type2), 0);
            return;
        }
        ConstraintAnchor.Type type14 = ConstraintAnchor.Type.CENTER_X;
        if (type == type14 && (type2 == (type4 = ConstraintAnchor.Type.LEFT) || type2 == ConstraintAnchor.Type.RIGHT)) {
            ConstraintAnchor q5 = q(type4);
            ConstraintAnchor q6 = constraintWidget.q(type2);
            ConstraintAnchor q7 = q(ConstraintAnchor.Type.RIGHT);
            q5.a(q6, 0);
            q7.a(q6, 0);
            q(type14).a(q6, 0);
            return;
        }
        ConstraintAnchor.Type type15 = ConstraintAnchor.Type.CENTER_Y;
        if (type == type15 && (type2 == (type3 = ConstraintAnchor.Type.TOP) || type2 == ConstraintAnchor.Type.BOTTOM)) {
            ConstraintAnchor q8 = constraintWidget.q(type2);
            q(type3).a(q8, 0);
            q(ConstraintAnchor.Type.BOTTOM).a(q8, 0);
            q(type15).a(q8, 0);
        } else if (type == type14 && type2 == type14) {
            ConstraintAnchor.Type type16 = ConstraintAnchor.Type.LEFT;
            q(type16).a(constraintWidget.q(type16), 0);
            ConstraintAnchor.Type type17 = ConstraintAnchor.Type.RIGHT;
            q(type17).a(constraintWidget.q(type17), 0);
            q(type14).a(constraintWidget.q(type2), 0);
        } else if (type == type15 && type2 == type15) {
            ConstraintAnchor.Type type18 = ConstraintAnchor.Type.TOP;
            q(type18).a(constraintWidget.q(type18), 0);
            ConstraintAnchor.Type type19 = ConstraintAnchor.Type.BOTTOM;
            q(type19).a(constraintWidget.q(type19), 0);
            q(type15).a(constraintWidget.q(type2), 0);
        } else {
            ConstraintAnchor q9 = q(type);
            ConstraintAnchor q10 = constraintWidget.q(type2);
            if (q9.p(q10)) {
                ConstraintAnchor.Type type20 = ConstraintAnchor.Type.BASELINE;
                if (type == type20) {
                    ConstraintAnchor q11 = q(ConstraintAnchor.Type.TOP);
                    ConstraintAnchor q12 = q(ConstraintAnchor.Type.BOTTOM);
                    if (q11 != null) {
                        q11.q();
                    }
                    if (q12 != null) {
                        q12.q();
                    }
                } else if (type != ConstraintAnchor.Type.TOP && type != ConstraintAnchor.Type.BOTTOM) {
                    if (type == ConstraintAnchor.Type.LEFT || type == ConstraintAnchor.Type.RIGHT) {
                        ConstraintAnchor q13 = q(type5);
                        if (q13.j() != q10) {
                            q13.q();
                        }
                        ConstraintAnchor g = q(type).g();
                        ConstraintAnchor q14 = q(type14);
                        if (q14.o()) {
                            g.q();
                            q14.q();
                        }
                    }
                } else {
                    ConstraintAnchor q15 = q(type20);
                    if (q15 != null) {
                        q15.q();
                    }
                    ConstraintAnchor q16 = q(type5);
                    if (q16.j() != q10) {
                        q16.q();
                    }
                    ConstraintAnchor g2 = q(type).g();
                    ConstraintAnchor q17 = q(type15);
                    if (q17.o()) {
                        g2.q();
                        q17.q();
                    }
                }
                q9.a(q10, i);
            }
        }
    }

    public boolean k0() {
        return this.J;
    }

    public void k1(int i) {
        this.e0 = i;
    }

    public void l(ConstraintAnchor constraintAnchor, ConstraintAnchor constraintAnchor2, int i) {
        if (constraintAnchor.h() == this) {
            k(constraintAnchor.k(), constraintAnchor2.h(), constraintAnchor2.k(), i);
        }
    }

    public boolean l0() {
        return this.g && this.q0 != 8;
    }

    public void l1(boolean z, boolean z2, boolean z3, boolean z4) {
        if (this.D == -1) {
            if (z3 && !z4) {
                this.D = 0;
            } else if (!z3 && z4) {
                this.D = 1;
                if (this.c0 == -1) {
                    this.E = 1.0f / this.E;
                }
            }
        }
        if (this.D == 0 && (!this.N.o() || !this.P.o())) {
            this.D = 1;
        } else if (this.D == 1 && (!this.M.o() || !this.O.o())) {
            this.D = 0;
        }
        if (this.D == -1 && (!this.N.o() || !this.P.o() || !this.M.o() || !this.O.o())) {
            if (this.N.o() && this.P.o()) {
                this.D = 0;
            } else if (this.M.o() && this.O.o()) {
                this.E = 1.0f / this.E;
                this.D = 1;
            }
        }
        if (this.D == -1) {
            int i = this.v;
            if (i > 0 && this.y == 0) {
                this.D = 0;
            } else if (i != 0 || this.y <= 0) {
            } else {
                this.E = 1.0f / this.E;
                this.D = 1;
            }
        }
    }

    public void m(ConstraintWidget constraintWidget, float f, int i) {
        ConstraintAnchor.Type type = ConstraintAnchor.Type.CENTER;
        d0(type, constraintWidget, type, i, 0);
        this.G = f;
    }

    public boolean m0() {
        return this.l || (this.M.n() && this.O.n());
    }

    public void m1(boolean z, boolean z2) {
        int i;
        int i2;
        boolean k = z & this.d.k();
        boolean k2 = z2 & this.e.k();
        androidx.constraintlayout.core.widgets.analyzer.c cVar = this.d;
        int i3 = cVar.h.g;
        androidx.constraintlayout.core.widgets.analyzer.d dVar = this.e;
        int i4 = dVar.h.g;
        int i5 = cVar.i.g;
        int i6 = dVar.i.g;
        int i7 = i6 - i4;
        if (i5 - i3 < 0 || i7 < 0 || i3 == Integer.MIN_VALUE || i3 == Integer.MAX_VALUE || i4 == Integer.MIN_VALUE || i4 == Integer.MAX_VALUE || i5 == Integer.MIN_VALUE || i5 == Integer.MAX_VALUE || i6 == Integer.MIN_VALUE || i6 == Integer.MAX_VALUE) {
            i5 = 0;
            i3 = 0;
            i6 = 0;
            i4 = 0;
        }
        int i8 = i5 - i3;
        int i9 = i6 - i4;
        if (k) {
            this.d0 = i3;
        }
        if (k2) {
            this.e0 = i4;
        }
        if (this.q0 == 8) {
            this.Z = 0;
            this.a0 = 0;
            return;
        }
        if (k) {
            if (this.X[0] == DimensionBehaviour.FIXED && i8 < (i2 = this.Z)) {
                i8 = i2;
            }
            this.Z = i8;
            int i10 = this.k0;
            if (i8 < i10) {
                this.Z = i10;
            }
        }
        if (k2) {
            if (this.X[1] == DimensionBehaviour.FIXED && i9 < (i = this.a0)) {
                i9 = i;
            }
            this.a0 = i9;
            int i11 = this.l0;
            if (i9 < i11) {
                this.a0 = i11;
            }
        }
    }

    public void n(ConstraintWidget constraintWidget, HashMap<ConstraintWidget, ConstraintWidget> hashMap) {
        this.p = constraintWidget.p;
        this.q = constraintWidget.q;
        this.s = constraintWidget.s;
        this.t = constraintWidget.t;
        int[] iArr = this.u;
        int[] iArr2 = constraintWidget.u;
        iArr[0] = iArr2[0];
        iArr[1] = iArr2[1];
        this.v = constraintWidget.v;
        this.w = constraintWidget.w;
        this.y = constraintWidget.y;
        this.z = constraintWidget.z;
        this.A = constraintWidget.A;
        this.B = constraintWidget.B;
        this.C = constraintWidget.C;
        this.D = constraintWidget.D;
        this.E = constraintWidget.E;
        int[] iArr3 = constraintWidget.F;
        this.F = Arrays.copyOf(iArr3, iArr3.length);
        this.G = constraintWidget.G;
        this.H = constraintWidget.H;
        this.I = constraintWidget.I;
        this.M.q();
        this.N.q();
        this.O.q();
        this.P.q();
        this.Q.q();
        this.R.q();
        this.S.q();
        this.T.q();
        this.X = (DimensionBehaviour[]) Arrays.copyOf(this.X, 2);
        this.Y = this.Y == null ? null : hashMap.get(constraintWidget.Y);
        this.Z = constraintWidget.Z;
        this.a0 = constraintWidget.a0;
        this.b0 = constraintWidget.b0;
        this.c0 = constraintWidget.c0;
        this.d0 = constraintWidget.d0;
        this.e0 = constraintWidget.e0;
        this.f0 = constraintWidget.f0;
        this.g0 = constraintWidget.g0;
        this.h0 = constraintWidget.h0;
        this.i0 = constraintWidget.i0;
        this.j0 = constraintWidget.j0;
        this.k0 = constraintWidget.k0;
        this.l0 = constraintWidget.l0;
        this.m0 = constraintWidget.m0;
        this.n0 = constraintWidget.n0;
        this.o0 = constraintWidget.o0;
        this.p0 = constraintWidget.p0;
        this.q0 = constraintWidget.q0;
        this.r0 = constraintWidget.r0;
        this.s0 = constraintWidget.s0;
        this.t0 = constraintWidget.t0;
        this.u0 = constraintWidget.u0;
        this.v0 = constraintWidget.v0;
        this.w0 = constraintWidget.w0;
        this.x0 = constraintWidget.x0;
        this.y0 = constraintWidget.y0;
        this.z0 = constraintWidget.z0;
        this.A0 = constraintWidget.A0;
        this.B0 = constraintWidget.B0;
        this.C0 = constraintWidget.C0;
        this.D0 = constraintWidget.D0;
        this.E0 = constraintWidget.E0;
        this.F0 = constraintWidget.F0;
        this.G0 = constraintWidget.G0;
        float[] fArr = this.H0;
        float[] fArr2 = constraintWidget.H0;
        fArr[0] = fArr2[0];
        fArr[1] = fArr2[1];
        ConstraintWidget[] constraintWidgetArr = this.I0;
        ConstraintWidget[] constraintWidgetArr2 = constraintWidget.I0;
        constraintWidgetArr[0] = constraintWidgetArr2[0];
        constraintWidgetArr[1] = constraintWidgetArr2[1];
        ConstraintWidget[] constraintWidgetArr3 = this.J0;
        ConstraintWidget[] constraintWidgetArr4 = constraintWidget.J0;
        constraintWidgetArr3[0] = constraintWidgetArr4[0];
        constraintWidgetArr3[1] = constraintWidgetArr4[1];
        ConstraintWidget constraintWidget2 = constraintWidget.K0;
        this.K0 = constraintWidget2 == null ? null : hashMap.get(constraintWidget2);
        ConstraintWidget constraintWidget3 = constraintWidget.L0;
        this.L0 = constraintWidget3 != null ? hashMap.get(constraintWidget3) : null;
    }

    public boolean n0() {
        return this.m || (this.N.n() && this.P.n());
    }

    public void n1(androidx.constraintlayout.core.c cVar, boolean z) {
        androidx.constraintlayout.core.widgets.analyzer.d dVar;
        androidx.constraintlayout.core.widgets.analyzer.c cVar2;
        int x = cVar.x(this.M);
        int x2 = cVar.x(this.N);
        int x3 = cVar.x(this.O);
        int x4 = cVar.x(this.P);
        if (z && (cVar2 = this.d) != null) {
            DependencyNode dependencyNode = cVar2.h;
            if (dependencyNode.j) {
                DependencyNode dependencyNode2 = cVar2.i;
                if (dependencyNode2.j) {
                    x = dependencyNode.g;
                    x3 = dependencyNode2.g;
                }
            }
        }
        if (z && (dVar = this.e) != null) {
            DependencyNode dependencyNode3 = dVar.h;
            if (dependencyNode3.j) {
                DependencyNode dependencyNode4 = dVar.i;
                if (dependencyNode4.j) {
                    x2 = dependencyNode3.g;
                    x4 = dependencyNode4.g;
                }
            }
        }
        int i = x4 - x2;
        if (x3 - x < 0 || i < 0 || x == Integer.MIN_VALUE || x == Integer.MAX_VALUE || x2 == Integer.MIN_VALUE || x2 == Integer.MAX_VALUE || x3 == Integer.MIN_VALUE || x3 == Integer.MAX_VALUE || x4 == Integer.MIN_VALUE || x4 == Integer.MAX_VALUE) {
            x4 = 0;
            x = 0;
            x2 = 0;
            x3 = 0;
        }
        G0(x, x2, x3, x4);
    }

    public void o(androidx.constraintlayout.core.c cVar) {
        cVar.q(this.M);
        cVar.q(this.N);
        cVar.q(this.O);
        cVar.q(this.P);
        if (this.j0 > 0) {
            cVar.q(this.Q);
        }
    }

    public boolean o0() {
        return this.o;
    }

    public void p() {
        if (this.d == null) {
            this.d = new androidx.constraintlayout.core.widgets.analyzer.c(this);
        }
        if (this.e == null) {
            this.e = new androidx.constraintlayout.core.widgets.analyzer.d(this);
        }
    }

    public void p0() {
        this.n = true;
    }

    public ConstraintAnchor q(ConstraintAnchor.Type type) {
        switch (a.a[type.ordinal()]) {
            case 1:
                return this.M;
            case 2:
                return this.N;
            case 3:
                return this.O;
            case 4:
                return this.P;
            case 5:
                return this.Q;
            case 6:
                return this.T;
            case 7:
                return this.R;
            case 8:
                return this.S;
            case 9:
                return null;
            default:
                throw new AssertionError(type.name());
        }
    }

    public void q0() {
        this.o = true;
    }

    public int r() {
        return this.j0;
    }

    public boolean r0() {
        DimensionBehaviour[] dimensionBehaviourArr = this.X;
        DimensionBehaviour dimensionBehaviour = dimensionBehaviourArr[0];
        DimensionBehaviour dimensionBehaviour2 = DimensionBehaviour.MATCH_CONSTRAINT;
        return dimensionBehaviour == dimensionBehaviour2 && dimensionBehaviourArr[1] == dimensionBehaviour2;
    }

    public float s(int i) {
        if (i == 0) {
            return this.m0;
        }
        if (i == 1) {
            return this.n0;
        }
        return -1.0f;
    }

    public void s0() {
        this.M.q();
        this.N.q();
        this.O.q();
        this.P.q();
        this.Q.q();
        this.R.q();
        this.S.q();
        this.T.q();
        this.Y = null;
        this.G = Utils.FLOAT_EPSILON;
        this.Z = 0;
        this.a0 = 0;
        this.b0 = Utils.FLOAT_EPSILON;
        this.c0 = -1;
        this.d0 = 0;
        this.e0 = 0;
        this.h0 = 0;
        this.i0 = 0;
        this.j0 = 0;
        this.k0 = 0;
        this.l0 = 0;
        float f = O0;
        this.m0 = f;
        this.n0 = f;
        DimensionBehaviour[] dimensionBehaviourArr = this.X;
        DimensionBehaviour dimensionBehaviour = DimensionBehaviour.FIXED;
        dimensionBehaviourArr[0] = dimensionBehaviour;
        dimensionBehaviourArr[1] = dimensionBehaviour;
        this.o0 = null;
        this.p0 = 0;
        this.q0 = 0;
        this.s0 = null;
        this.B0 = false;
        this.C0 = false;
        this.D0 = 0;
        this.E0 = 0;
        this.F0 = false;
        this.G0 = false;
        float[] fArr = this.H0;
        fArr[0] = -1.0f;
        fArr[1] = -1.0f;
        this.p = -1;
        this.q = -1;
        int[] iArr = this.F;
        iArr[0] = Integer.MAX_VALUE;
        iArr[1] = Integer.MAX_VALUE;
        this.s = 0;
        this.t = 0;
        this.x = 1.0f;
        this.A = 1.0f;
        this.w = Integer.MAX_VALUE;
        this.z = Integer.MAX_VALUE;
        this.v = 0;
        this.y = 0;
        this.D = -1;
        this.E = 1.0f;
        boolean[] zArr = this.f;
        zArr[0] = true;
        zArr[1] = true;
        this.J = false;
        boolean[] zArr2 = this.W;
        zArr2[0] = false;
        zArr2[1] = false;
        this.g = true;
        int[] iArr2 = this.u;
        iArr2[0] = 0;
        iArr2[1] = 0;
        this.j = -1;
        this.k = -1;
    }

    public int t() {
        return X() + this.a0;
    }

    public void t0() {
        u0();
        a1(O0);
        J0(O0);
    }

    public String toString() {
        String str;
        StringBuilder sb = new StringBuilder();
        String str2 = "";
        if (this.s0 != null) {
            str = "type: " + this.s0 + MinimalPrettyPrinter.DEFAULT_ROOT_VALUE_SEPARATOR;
        } else {
            str = "";
        }
        sb.append(str);
        if (this.r0 != null) {
            str2 = "id: " + this.r0 + MinimalPrettyPrinter.DEFAULT_ROOT_VALUE_SEPARATOR;
        }
        sb.append(str2);
        sb.append("(");
        sb.append(this.d0);
        sb.append(", ");
        sb.append(this.e0);
        sb.append(") - (");
        sb.append(this.Z);
        sb.append(" x ");
        sb.append(this.a0);
        sb.append(")");
        return sb.toString();
    }

    public Object u() {
        return this.o0;
    }

    public void u0() {
        ConstraintWidget M = M();
        if (M != null && (M instanceof d) && ((d) M()).I1()) {
            return;
        }
        int size = this.V.size();
        for (int i = 0; i < size; i++) {
            this.V.get(i).q();
        }
    }

    public String v() {
        return this.r0;
    }

    public void v0() {
        this.l = false;
        this.m = false;
        this.n = false;
        this.o = false;
        int size = this.V.size();
        for (int i = 0; i < size; i++) {
            this.V.get(i).r();
        }
    }

    public DimensionBehaviour w(int i) {
        if (i == 0) {
            return C();
        }
        if (i == 1) {
            return S();
        }
        return null;
    }

    public void w0(ut utVar) {
        this.M.s(utVar);
        this.N.s(utVar);
        this.O.s(utVar);
        this.P.s(utVar);
        this.Q.s(utVar);
        this.T.s(utVar);
        this.R.s(utVar);
        this.S.s(utVar);
    }

    public float x() {
        return this.b0;
    }

    public void x0(int i) {
        this.j0 = i;
        this.H = i > 0;
    }

    public int y() {
        return this.c0;
    }

    public void y0(Object obj) {
        this.o0 = obj;
    }

    public int z() {
        if (this.q0 == 8) {
            return 0;
        }
        return this.a0;
    }

    public void z0(String str) {
        this.r0 = str;
    }
}
