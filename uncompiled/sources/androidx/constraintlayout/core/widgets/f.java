package androidx.constraintlayout.core.widgets;

import androidx.constraintlayout.core.SolverVariable;
import androidx.constraintlayout.core.widgets.ConstraintAnchor;
import androidx.constraintlayout.core.widgets.ConstraintWidget;
import java.util.HashMap;

/* compiled from: Guideline.java */
/* loaded from: classes.dex */
public class f extends ConstraintWidget {
    public float P0 = -1.0f;
    public int Q0 = -1;
    public int R0 = -1;
    public ConstraintAnchor S0 = this.N;
    public int T0 = 0;
    public boolean U0;

    /* compiled from: Guideline.java */
    /* loaded from: classes.dex */
    public static /* synthetic */ class a {
        public static final /* synthetic */ int[] a;

        static {
            int[] iArr = new int[ConstraintAnchor.Type.values().length];
            a = iArr;
            try {
                iArr[ConstraintAnchor.Type.LEFT.ordinal()] = 1;
            } catch (NoSuchFieldError unused) {
            }
            try {
                a[ConstraintAnchor.Type.RIGHT.ordinal()] = 2;
            } catch (NoSuchFieldError unused2) {
            }
            try {
                a[ConstraintAnchor.Type.TOP.ordinal()] = 3;
            } catch (NoSuchFieldError unused3) {
            }
            try {
                a[ConstraintAnchor.Type.BOTTOM.ordinal()] = 4;
            } catch (NoSuchFieldError unused4) {
            }
            try {
                a[ConstraintAnchor.Type.BASELINE.ordinal()] = 5;
            } catch (NoSuchFieldError unused5) {
            }
            try {
                a[ConstraintAnchor.Type.CENTER.ordinal()] = 6;
            } catch (NoSuchFieldError unused6) {
            }
            try {
                a[ConstraintAnchor.Type.CENTER_X.ordinal()] = 7;
            } catch (NoSuchFieldError unused7) {
            }
            try {
                a[ConstraintAnchor.Type.CENTER_Y.ordinal()] = 8;
            } catch (NoSuchFieldError unused8) {
            }
            try {
                a[ConstraintAnchor.Type.NONE.ordinal()] = 9;
            } catch (NoSuchFieldError unused9) {
            }
        }
    }

    public f() {
        this.V.clear();
        this.V.add(this.S0);
        int length = this.U.length;
        for (int i = 0; i < length; i++) {
            this.U[i] = this.S0;
        }
    }

    @Override // androidx.constraintlayout.core.widgets.ConstraintWidget
    public void g(androidx.constraintlayout.core.c cVar, boolean z) {
        d dVar = (d) M();
        if (dVar == null) {
            return;
        }
        ConstraintAnchor q = dVar.q(ConstraintAnchor.Type.LEFT);
        ConstraintAnchor q2 = dVar.q(ConstraintAnchor.Type.RIGHT);
        ConstraintWidget constraintWidget = this.Y;
        boolean z2 = true;
        boolean z3 = constraintWidget != null && constraintWidget.X[0] == ConstraintWidget.DimensionBehaviour.WRAP_CONTENT;
        if (this.T0 == 0) {
            q = dVar.q(ConstraintAnchor.Type.TOP);
            q2 = dVar.q(ConstraintAnchor.Type.BOTTOM);
            ConstraintWidget constraintWidget2 = this.Y;
            if (constraintWidget2 == null || constraintWidget2.X[1] != ConstraintWidget.DimensionBehaviour.WRAP_CONTENT) {
                z2 = false;
            }
            z3 = z2;
        }
        if (this.U0 && this.S0.n()) {
            SolverVariable q3 = cVar.q(this.S0);
            cVar.f(q3, this.S0.e());
            if (this.Q0 != -1) {
                if (z3) {
                    cVar.h(cVar.q(q2), q3, 0, 5);
                }
            } else if (this.R0 != -1 && z3) {
                SolverVariable q4 = cVar.q(q2);
                cVar.h(q3, cVar.q(q), 0, 5);
                cVar.h(q4, q3, 0, 5);
            }
            this.U0 = false;
        } else if (this.Q0 != -1) {
            SolverVariable q5 = cVar.q(this.S0);
            cVar.e(q5, cVar.q(q), this.Q0, 8);
            if (z3) {
                cVar.h(cVar.q(q2), q5, 0, 5);
            }
        } else if (this.R0 != -1) {
            SolverVariable q6 = cVar.q(this.S0);
            SolverVariable q7 = cVar.q(q2);
            cVar.e(q6, q7, -this.R0, 8);
            if (z3) {
                cVar.h(q6, cVar.q(q), 0, 5);
                cVar.h(q7, q6, 0, 5);
            }
        } else if (this.P0 != -1.0f) {
            cVar.d(androidx.constraintlayout.core.c.s(cVar, cVar.q(this.S0), cVar.q(q2), this.P0));
        }
    }

    @Override // androidx.constraintlayout.core.widgets.ConstraintWidget
    public boolean h() {
        return true;
    }

    @Override // androidx.constraintlayout.core.widgets.ConstraintWidget
    public boolean m0() {
        return this.U0;
    }

    @Override // androidx.constraintlayout.core.widgets.ConstraintWidget
    public void n(ConstraintWidget constraintWidget, HashMap<ConstraintWidget, ConstraintWidget> hashMap) {
        super.n(constraintWidget, hashMap);
        f fVar = (f) constraintWidget;
        this.P0 = fVar.P0;
        this.Q0 = fVar.Q0;
        this.R0 = fVar.R0;
        x1(fVar.T0);
    }

    @Override // androidx.constraintlayout.core.widgets.ConstraintWidget
    public boolean n0() {
        return this.U0;
    }

    @Override // androidx.constraintlayout.core.widgets.ConstraintWidget
    public void n1(androidx.constraintlayout.core.c cVar, boolean z) {
        if (M() == null) {
            return;
        }
        int x = cVar.x(this.S0);
        if (this.T0 == 1) {
            j1(x);
            k1(0);
            I0(M().z());
            h1(0);
            return;
        }
        j1(0);
        k1(x);
        h1(M().V());
        I0(0);
    }

    public ConstraintAnchor o1() {
        return this.S0;
    }

    public int p1() {
        return this.T0;
    }

    @Override // androidx.constraintlayout.core.widgets.ConstraintWidget
    public ConstraintAnchor q(ConstraintAnchor.Type type) {
        int i = a.a[type.ordinal()];
        if (i != 1 && i != 2) {
            if ((i == 3 || i == 4) && this.T0 == 0) {
                return this.S0;
            }
            return null;
        } else if (this.T0 == 1) {
            return this.S0;
        } else {
            return null;
        }
    }

    public int q1() {
        return this.Q0;
    }

    public int r1() {
        return this.R0;
    }

    public float s1() {
        return this.P0;
    }

    public void t1(int i) {
        this.S0.t(i);
        this.U0 = true;
    }

    public void u1(int i) {
        if (i > -1) {
            this.P0 = -1.0f;
            this.Q0 = i;
            this.R0 = -1;
        }
    }

    public void v1(int i) {
        if (i > -1) {
            this.P0 = -1.0f;
            this.Q0 = -1;
            this.R0 = i;
        }
    }

    public void w1(float f) {
        if (f > -1.0f) {
            this.P0 = f;
            this.Q0 = -1;
            this.R0 = -1;
        }
    }

    public void x1(int i) {
        if (this.T0 == i) {
            return;
        }
        this.T0 = i;
        this.V.clear();
        if (this.T0 == 1) {
            this.S0 = this.M;
        } else {
            this.S0 = this.N;
        }
        this.V.add(this.S0);
        int length = this.U.length;
        for (int i2 = 0; i2 < length; i2++) {
            this.U[i2] = this.S0;
        }
    }
}
