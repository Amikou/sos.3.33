package androidx.constraintlayout.core.widgets;

import androidx.constraintlayout.core.widgets.ConstraintWidget;
import defpackage.jo;
import java.util.HashSet;

/* compiled from: VirtualLayout.java */
/* loaded from: classes.dex */
public class i extends mk1 {
    public int R0 = 0;
    public int S0 = 0;
    public int T0 = 0;
    public int U0 = 0;
    public int V0 = 0;
    public int W0 = 0;
    public boolean X0 = false;
    public int Y0 = 0;
    public int Z0 = 0;
    public jo.a a1 = new jo.a();
    public jo.b b1 = null;

    public void A1(ConstraintWidget constraintWidget, ConstraintWidget.DimensionBehaviour dimensionBehaviour, int i, ConstraintWidget.DimensionBehaviour dimensionBehaviour2, int i2) {
        while (this.b1 == null && M() != null) {
            this.b1 = ((d) M()).F1();
        }
        jo.a aVar = this.a1;
        aVar.a = dimensionBehaviour;
        aVar.b = dimensionBehaviour2;
        aVar.c = i;
        aVar.d = i2;
        this.b1.b(constraintWidget, aVar);
        constraintWidget.h1(this.a1.e);
        constraintWidget.I0(this.a1.f);
        constraintWidget.H0(this.a1.h);
        constraintWidget.x0(this.a1.g);
    }

    public boolean B1() {
        ConstraintWidget constraintWidget = this.Y;
        jo.b F1 = constraintWidget != null ? ((d) constraintWidget).F1() : null;
        if (F1 == null) {
            return false;
        }
        int i = 0;
        while (true) {
            boolean z = true;
            if (i >= this.Q0) {
                return true;
            }
            ConstraintWidget constraintWidget2 = this.P0[i];
            if (constraintWidget2 != null && !(constraintWidget2 instanceof f)) {
                ConstraintWidget.DimensionBehaviour w = constraintWidget2.w(0);
                ConstraintWidget.DimensionBehaviour w2 = constraintWidget2.w(1);
                ConstraintWidget.DimensionBehaviour dimensionBehaviour = ConstraintWidget.DimensionBehaviour.MATCH_CONSTRAINT;
                if (w != dimensionBehaviour || constraintWidget2.s == 1 || w2 != dimensionBehaviour || constraintWidget2.t == 1) {
                    z = false;
                }
                if (!z) {
                    if (w == dimensionBehaviour) {
                        w = ConstraintWidget.DimensionBehaviour.WRAP_CONTENT;
                    }
                    if (w2 == dimensionBehaviour) {
                        w2 = ConstraintWidget.DimensionBehaviour.WRAP_CONTENT;
                    }
                    jo.a aVar = this.a1;
                    aVar.a = w;
                    aVar.b = w2;
                    aVar.c = constraintWidget2.V();
                    this.a1.d = constraintWidget2.z();
                    F1.b(constraintWidget2, this.a1);
                    constraintWidget2.h1(this.a1.e);
                    constraintWidget2.I0(this.a1.f);
                    constraintWidget2.x0(this.a1.g);
                }
            }
            i++;
        }
    }

    public boolean C1() {
        return this.X0;
    }

    public void D1(boolean z) {
        this.X0 = z;
    }

    public void E1(int i, int i2) {
        this.Y0 = i;
        this.Z0 = i2;
    }

    public void F1(int i) {
        this.R0 = i;
        this.S0 = i;
        this.T0 = i;
        this.U0 = i;
    }

    public void G1(int i) {
        this.S0 = i;
    }

    public void H1(int i) {
        this.U0 = i;
    }

    public void I1(int i) {
        this.V0 = i;
    }

    public void J1(int i) {
        this.W0 = i;
    }

    public void K1(int i) {
        this.T0 = i;
        this.V0 = i;
        this.W0 = i;
    }

    public void L1(int i) {
        this.R0 = i;
    }

    @Override // defpackage.mk1, defpackage.lk1
    public void c(d dVar) {
        r1();
    }

    public void q1(boolean z) {
        int i = this.T0;
        if (i > 0 || this.U0 > 0) {
            if (z) {
                this.V0 = this.U0;
                this.W0 = i;
                return;
            }
            this.V0 = i;
            this.W0 = this.U0;
        }
    }

    public void r1() {
        for (int i = 0; i < this.Q0; i++) {
            ConstraintWidget constraintWidget = this.P0[i];
            if (constraintWidget != null) {
                constraintWidget.R0(true);
            }
        }
    }

    public boolean s1(HashSet<ConstraintWidget> hashSet) {
        for (int i = 0; i < this.Q0; i++) {
            if (hashSet.contains(this.P0[i])) {
                return true;
            }
        }
        return false;
    }

    public int t1() {
        return this.Z0;
    }

    public int u1() {
        return this.Y0;
    }

    public int v1() {
        return this.S0;
    }

    public int w1() {
        return this.V0;
    }

    public int x1() {
        return this.W0;
    }

    public int y1() {
        return this.R0;
    }

    public void z1(int i, int i2, int i3, int i4) {
    }
}
