package androidx.constraintlayout.core.widgets;

import androidx.constraintlayout.core.SolverVariable;
import androidx.constraintlayout.core.widgets.ConstraintAnchor;
import androidx.constraintlayout.core.widgets.ConstraintWidget;
import java.util.HashMap;

/* compiled from: Barrier.java */
/* loaded from: classes.dex */
public class a extends mk1 {
    public int R0 = 0;
    public boolean S0 = true;
    public int T0 = 0;
    public boolean U0 = false;

    @Override // androidx.constraintlayout.core.widgets.ConstraintWidget
    public void g(androidx.constraintlayout.core.c cVar, boolean z) {
        Object[] objArr;
        boolean z2;
        int i;
        int i2;
        int i3;
        ConstraintAnchor[] constraintAnchorArr = this.U;
        constraintAnchorArr[0] = this.M;
        constraintAnchorArr[2] = this.N;
        constraintAnchorArr[1] = this.O;
        constraintAnchorArr[3] = this.P;
        int i4 = 0;
        while (true) {
            objArr = this.U;
            if (i4 >= objArr.length) {
                break;
            }
            objArr[i4].i = cVar.q(objArr[i4]);
            i4++;
        }
        int i5 = this.R0;
        if (i5 < 0 || i5 >= 4) {
            return;
        }
        ConstraintAnchor constraintAnchor = objArr[i5];
        if (!this.U0) {
            q1();
        }
        if (this.U0) {
            this.U0 = false;
            int i6 = this.R0;
            if (i6 == 0 || i6 == 1) {
                cVar.f(this.M.i, this.d0);
                cVar.f(this.O.i, this.d0);
                return;
            } else if (i6 == 2 || i6 == 3) {
                cVar.f(this.N.i, this.e0);
                cVar.f(this.P.i, this.e0);
                return;
            } else {
                return;
            }
        }
        for (int i7 = 0; i7 < this.Q0; i7++) {
            ConstraintWidget constraintWidget = this.P0[i7];
            if ((this.S0 || constraintWidget.h()) && ((((i2 = this.R0) == 0 || i2 == 1) && constraintWidget.C() == ConstraintWidget.DimensionBehaviour.MATCH_CONSTRAINT && constraintWidget.M.f != null && constraintWidget.O.f != null) || (((i3 = this.R0) == 2 || i3 == 3) && constraintWidget.S() == ConstraintWidget.DimensionBehaviour.MATCH_CONSTRAINT && constraintWidget.N.f != null && constraintWidget.P.f != null))) {
                z2 = true;
                break;
            }
        }
        z2 = false;
        boolean z3 = this.M.l() || this.O.l();
        boolean z4 = this.N.l() || this.P.l();
        int i8 = !z2 && (((i = this.R0) == 0 && z3) || ((i == 2 && z4) || ((i == 1 && z3) || (i == 3 && z4)))) ? 5 : 4;
        for (int i9 = 0; i9 < this.Q0; i9++) {
            ConstraintWidget constraintWidget2 = this.P0[i9];
            if (this.S0 || constraintWidget2.h()) {
                SolverVariable q = cVar.q(constraintWidget2.U[this.R0]);
                ConstraintAnchor[] constraintAnchorArr2 = constraintWidget2.U;
                int i10 = this.R0;
                constraintAnchorArr2[i10].i = q;
                int i11 = (constraintAnchorArr2[i10].f == null || constraintAnchorArr2[i10].f.d != this) ? 0 : constraintAnchorArr2[i10].g + 0;
                if (i10 != 0 && i10 != 2) {
                    cVar.g(constraintAnchor.i, q, this.T0 + i11, z2);
                } else {
                    cVar.i(constraintAnchor.i, q, this.T0 - i11, z2);
                }
                cVar.e(constraintAnchor.i, q, this.T0 + i11, i8);
            }
        }
        int i12 = this.R0;
        if (i12 == 0) {
            cVar.e(this.O.i, this.M.i, 0, 8);
            cVar.e(this.M.i, this.Y.O.i, 0, 4);
            cVar.e(this.M.i, this.Y.M.i, 0, 0);
        } else if (i12 == 1) {
            cVar.e(this.M.i, this.O.i, 0, 8);
            cVar.e(this.M.i, this.Y.M.i, 0, 4);
            cVar.e(this.M.i, this.Y.O.i, 0, 0);
        } else if (i12 == 2) {
            cVar.e(this.P.i, this.N.i, 0, 8);
            cVar.e(this.N.i, this.Y.P.i, 0, 4);
            cVar.e(this.N.i, this.Y.N.i, 0, 0);
        } else if (i12 == 3) {
            cVar.e(this.N.i, this.P.i, 0, 8);
            cVar.e(this.N.i, this.Y.N.i, 0, 4);
            cVar.e(this.N.i, this.Y.P.i, 0, 0);
        }
    }

    @Override // androidx.constraintlayout.core.widgets.ConstraintWidget
    public boolean h() {
        return true;
    }

    @Override // androidx.constraintlayout.core.widgets.ConstraintWidget
    public boolean m0() {
        return this.U0;
    }

    @Override // defpackage.mk1, androidx.constraintlayout.core.widgets.ConstraintWidget
    public void n(ConstraintWidget constraintWidget, HashMap<ConstraintWidget, ConstraintWidget> hashMap) {
        super.n(constraintWidget, hashMap);
        a aVar = (a) constraintWidget;
        this.R0 = aVar.R0;
        this.S0 = aVar.S0;
        this.T0 = aVar.T0;
    }

    @Override // androidx.constraintlayout.core.widgets.ConstraintWidget
    public boolean n0() {
        return this.U0;
    }

    public boolean q1() {
        int i;
        int i2;
        int i3;
        int i4 = 0;
        boolean z = true;
        while (true) {
            i = this.Q0;
            if (i4 >= i) {
                break;
            }
            ConstraintWidget constraintWidget = this.P0[i4];
            if ((this.S0 || constraintWidget.h()) && ((((i2 = this.R0) == 0 || i2 == 1) && !constraintWidget.m0()) || (((i3 = this.R0) == 2 || i3 == 3) && !constraintWidget.n0()))) {
                z = false;
            }
            i4++;
        }
        if (!z || i <= 0) {
            return false;
        }
        int i5 = 0;
        boolean z2 = false;
        for (int i6 = 0; i6 < this.Q0; i6++) {
            ConstraintWidget constraintWidget2 = this.P0[i6];
            if (this.S0 || constraintWidget2.h()) {
                if (!z2) {
                    int i7 = this.R0;
                    if (i7 == 0) {
                        i5 = constraintWidget2.q(ConstraintAnchor.Type.LEFT).e();
                    } else if (i7 == 1) {
                        i5 = constraintWidget2.q(ConstraintAnchor.Type.RIGHT).e();
                    } else if (i7 == 2) {
                        i5 = constraintWidget2.q(ConstraintAnchor.Type.TOP).e();
                    } else if (i7 == 3) {
                        i5 = constraintWidget2.q(ConstraintAnchor.Type.BOTTOM).e();
                    }
                    z2 = true;
                }
                int i8 = this.R0;
                if (i8 == 0) {
                    i5 = Math.min(i5, constraintWidget2.q(ConstraintAnchor.Type.LEFT).e());
                } else if (i8 == 1) {
                    i5 = Math.max(i5, constraintWidget2.q(ConstraintAnchor.Type.RIGHT).e());
                } else if (i8 == 2) {
                    i5 = Math.min(i5, constraintWidget2.q(ConstraintAnchor.Type.TOP).e());
                } else if (i8 == 3) {
                    i5 = Math.max(i5, constraintWidget2.q(ConstraintAnchor.Type.BOTTOM).e());
                }
            }
        }
        int i9 = i5 + this.T0;
        int i10 = this.R0;
        if (i10 != 0 && i10 != 1) {
            F0(i9, i9);
        } else {
            C0(i9, i9);
        }
        this.U0 = true;
        return true;
    }

    public boolean r1() {
        return this.S0;
    }

    public int s1() {
        return this.R0;
    }

    public int t1() {
        return this.T0;
    }

    @Override // androidx.constraintlayout.core.widgets.ConstraintWidget
    public String toString() {
        String str = "[Barrier] " + v() + " {";
        for (int i = 0; i < this.Q0; i++) {
            ConstraintWidget constraintWidget = this.P0[i];
            if (i > 0) {
                str = str + ", ";
            }
            str = str + constraintWidget.v();
        }
        return str + "}";
    }

    public int u1() {
        int i = this.R0;
        if (i == 0 || i == 1) {
            return 0;
        }
        return (i == 2 || i == 3) ? 1 : -1;
    }

    public void v1() {
        for (int i = 0; i < this.Q0; i++) {
            ConstraintWidget constraintWidget = this.P0[i];
            if (this.S0 || constraintWidget.h()) {
                int i2 = this.R0;
                if (i2 == 0 || i2 == 1) {
                    constraintWidget.P0(0, true);
                } else if (i2 == 2 || i2 == 3) {
                    constraintWidget.P0(1, true);
                }
            }
        }
    }

    public void w1(boolean z) {
        this.S0 = z;
    }

    public void x1(int i) {
        this.R0 = i;
    }

    public void y1(int i) {
        this.T0 = i;
    }
}
