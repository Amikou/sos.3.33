package androidx.constraintlayout.core.widgets;

import androidx.constraintlayout.core.widgets.ConstraintAnchor;

/* compiled from: Placeholder.java */
/* loaded from: classes.dex */
public class h extends i {
    @Override // androidx.constraintlayout.core.widgets.ConstraintWidget
    public void g(androidx.constraintlayout.core.c cVar, boolean z) {
        super.g(cVar, z);
        if (this.Q0 > 0) {
            ConstraintWidget constraintWidget = this.P0[0];
            constraintWidget.t0();
            ConstraintAnchor.Type type = ConstraintAnchor.Type.LEFT;
            constraintWidget.j(type, this, type);
            ConstraintAnchor.Type type2 = ConstraintAnchor.Type.RIGHT;
            constraintWidget.j(type2, this, type2);
            ConstraintAnchor.Type type3 = ConstraintAnchor.Type.TOP;
            constraintWidget.j(type3, this, type3);
            ConstraintAnchor.Type type4 = ConstraintAnchor.Type.BOTTOM;
            constraintWidget.j(type4, this, type4);
        }
    }

    @Override // androidx.constraintlayout.core.widgets.i
    public void z1(int i, int i2, int i3, int i4) {
        int w1 = w1() + x1() + 0;
        int y1 = y1() + v1() + 0;
        if (this.Q0 > 0) {
            w1 += this.P0[0].V();
            y1 += this.P0[0].z();
        }
        int max = Math.max(K(), w1);
        int max2 = Math.max(J(), y1);
        if (i != 1073741824) {
            if (i == Integer.MIN_VALUE) {
                i2 = Math.min(max, i2);
            } else {
                i2 = i == 0 ? max : 0;
            }
        }
        if (i3 != 1073741824) {
            if (i3 == Integer.MIN_VALUE) {
                i4 = Math.min(max2, i4);
            } else {
                i4 = i3 == 0 ? max2 : 0;
            }
        }
        E1(i2, i4);
        h1(i2);
        I0(i4);
        D1(this.Q0 > 0);
    }
}
