package androidx.constraintlayout.core;

import androidx.constraintlayout.core.b;
import com.fasterxml.jackson.core.util.MinimalPrettyPrinter;
import com.github.mikephil.charting.utils.Utils;
import java.util.Arrays;
import java.util.Comparator;

/* compiled from: PriorityGoalRow.java */
/* loaded from: classes.dex */
public class d extends androidx.constraintlayout.core.b {
    public int g;
    public SolverVariable[] h;
    public SolverVariable[] i;
    public int j;
    public b k;

    /* compiled from: PriorityGoalRow.java */
    /* loaded from: classes.dex */
    public class a implements Comparator<SolverVariable> {
        public a(d dVar) {
        }

        @Override // java.util.Comparator
        /* renamed from: a */
        public int compare(SolverVariable solverVariable, SolverVariable solverVariable2) {
            return solverVariable.g0 - solverVariable2.g0;
        }
    }

    /* compiled from: PriorityGoalRow.java */
    /* loaded from: classes.dex */
    public class b {
        public SolverVariable a;

        public b(d dVar) {
        }

        public boolean a(SolverVariable solverVariable, float f) {
            boolean z = true;
            if (!this.a.a) {
                for (int i = 0; i < 9; i++) {
                    float f2 = solverVariable.m0[i];
                    if (f2 != Utils.FLOAT_EPSILON) {
                        float f3 = f2 * f;
                        if (Math.abs(f3) < 1.0E-4f) {
                            f3 = 0.0f;
                        }
                        this.a.m0[i] = f3;
                    } else {
                        this.a.m0[i] = 0.0f;
                    }
                }
                return true;
            }
            for (int i2 = 0; i2 < 9; i2++) {
                float[] fArr = this.a.m0;
                fArr[i2] = fArr[i2] + (solverVariable.m0[i2] * f);
                if (Math.abs(fArr[i2]) < 1.0E-4f) {
                    this.a.m0[i2] = 0.0f;
                } else {
                    z = false;
                }
            }
            if (z) {
                d.this.G(this.a);
            }
            return false;
        }

        public void b(SolverVariable solverVariable) {
            this.a = solverVariable;
        }

        public final boolean c() {
            for (int i = 8; i >= 0; i--) {
                float f = this.a.m0[i];
                if (f > Utils.FLOAT_EPSILON) {
                    return false;
                }
                if (f < Utils.FLOAT_EPSILON) {
                    return true;
                }
            }
            return false;
        }

        public final boolean d(SolverVariable solverVariable) {
            int i = 8;
            while (true) {
                if (i < 0) {
                    break;
                }
                float f = solverVariable.m0[i];
                float f2 = this.a.m0[i];
                if (f2 == f) {
                    i--;
                } else if (f2 < f) {
                    return true;
                }
            }
            return false;
        }

        public void e() {
            Arrays.fill(this.a.m0, (float) Utils.FLOAT_EPSILON);
        }

        public String toString() {
            String str = "[ ";
            if (this.a != null) {
                for (int i = 0; i < 9; i++) {
                    str = str + this.a.m0[i] + MinimalPrettyPrinter.DEFAULT_ROOT_VALUE_SEPARATOR;
                }
            }
            return str + "] " + this.a;
        }
    }

    public d(ut utVar) {
        super(utVar);
        this.g = 128;
        this.h = new SolverVariable[128];
        this.i = new SolverVariable[128];
        this.j = 0;
        this.k = new b(this);
    }

    @Override // androidx.constraintlayout.core.b
    public void B(c cVar, androidx.constraintlayout.core.b bVar, boolean z) {
        SolverVariable solverVariable = bVar.a;
        if (solverVariable == null) {
            return;
        }
        b.a aVar = bVar.e;
        int a2 = aVar.a();
        for (int i = 0; i < a2; i++) {
            SolverVariable e = aVar.e(i);
            float h = aVar.h(i);
            this.k.b(e);
            if (this.k.a(solverVariable, h)) {
                F(e);
            }
            this.b += bVar.b * h;
        }
        G(solverVariable);
    }

    public final void F(SolverVariable solverVariable) {
        int i;
        int i2 = this.j + 1;
        SolverVariable[] solverVariableArr = this.h;
        if (i2 > solverVariableArr.length) {
            SolverVariable[] solverVariableArr2 = (SolverVariable[]) Arrays.copyOf(solverVariableArr, solverVariableArr.length * 2);
            this.h = solverVariableArr2;
            this.i = (SolverVariable[]) Arrays.copyOf(solverVariableArr2, solverVariableArr2.length * 2);
        }
        SolverVariable[] solverVariableArr3 = this.h;
        int i3 = this.j;
        solverVariableArr3[i3] = solverVariable;
        int i4 = i3 + 1;
        this.j = i4;
        if (i4 > 1 && solverVariableArr3[i4 - 1].g0 > solverVariable.g0) {
            int i5 = 0;
            while (true) {
                i = this.j;
                if (i5 >= i) {
                    break;
                }
                this.i[i5] = this.h[i5];
                i5++;
            }
            Arrays.sort(this.i, 0, i, new a(this));
            for (int i6 = 0; i6 < this.j; i6++) {
                this.h[i6] = this.i[i6];
            }
        }
        solverVariable.a = true;
        solverVariable.a(this);
    }

    public final void G(SolverVariable solverVariable) {
        int i = 0;
        while (i < this.j) {
            if (this.h[i] == solverVariable) {
                while (true) {
                    int i2 = this.j;
                    if (i < i2 - 1) {
                        SolverVariable[] solverVariableArr = this.h;
                        int i3 = i + 1;
                        solverVariableArr[i] = solverVariableArr[i3];
                        i = i3;
                    } else {
                        this.j = i2 - 1;
                        solverVariable.a = false;
                        return;
                    }
                }
            } else {
                i++;
            }
        }
    }

    @Override // androidx.constraintlayout.core.b, androidx.constraintlayout.core.c.a
    public SolverVariable b(c cVar, boolean[] zArr) {
        int i = -1;
        for (int i2 = 0; i2 < this.j; i2++) {
            SolverVariable solverVariable = this.h[i2];
            if (!zArr[solverVariable.g0]) {
                this.k.b(solverVariable);
                if (i == -1) {
                    if (!this.k.c()) {
                    }
                    i = i2;
                } else {
                    if (!this.k.d(this.h[i])) {
                    }
                    i = i2;
                }
            }
        }
        if (i == -1) {
            return null;
        }
        return this.h[i];
    }

    @Override // androidx.constraintlayout.core.b, androidx.constraintlayout.core.c.a
    public void c(SolverVariable solverVariable) {
        this.k.b(solverVariable);
        this.k.e();
        solverVariable.m0[solverVariable.i0] = 1.0f;
        F(solverVariable);
    }

    @Override // androidx.constraintlayout.core.b, androidx.constraintlayout.core.c.a
    public void clear() {
        this.j = 0;
        this.b = Utils.FLOAT_EPSILON;
    }

    @Override // androidx.constraintlayout.core.b, androidx.constraintlayout.core.c.a
    public boolean isEmpty() {
        return this.j == 0;
    }

    @Override // androidx.constraintlayout.core.b
    public String toString() {
        String str = " goal -> (" + this.b + ") : ";
        for (int i = 0; i < this.j; i++) {
            this.k.b(this.h[i]);
            str = str + this.k + MinimalPrettyPrinter.DEFAULT_ROOT_VALUE_SEPARATOR;
        }
        return str;
    }
}
