package androidx.constraintlayout.core;

import com.github.mikephil.charting.utils.Utils;
import java.util.Arrays;

/* loaded from: classes.dex */
public class SolverVariable implements Comparable<SolverVariable> {
    public static int u0 = 1;
    public boolean a;
    public String f0;
    public float j0;
    public Type n0;
    public int g0 = -1;
    public int h0 = -1;
    public int i0 = 0;
    public boolean k0 = false;
    public float[] l0 = new float[9];
    public float[] m0 = new float[9];
    public b[] o0 = new b[16];
    public int p0 = 0;
    public int q0 = 0;
    public boolean r0 = false;
    public int s0 = -1;
    public float t0 = Utils.FLOAT_EPSILON;

    /* loaded from: classes.dex */
    public enum Type {
        UNRESTRICTED,
        CONSTANT,
        SLACK,
        ERROR,
        UNKNOWN
    }

    public SolverVariable(Type type, String str) {
        this.n0 = type;
    }

    public static void e() {
        u0++;
    }

    public final void a(b bVar) {
        int i = 0;
        while (true) {
            int i2 = this.p0;
            if (i < i2) {
                if (this.o0[i] == bVar) {
                    return;
                }
                i++;
            } else {
                b[] bVarArr = this.o0;
                if (i2 >= bVarArr.length) {
                    this.o0 = (b[]) Arrays.copyOf(bVarArr, bVarArr.length * 2);
                }
                b[] bVarArr2 = this.o0;
                int i3 = this.p0;
                bVarArr2[i3] = bVar;
                this.p0 = i3 + 1;
                return;
            }
        }
    }

    @Override // java.lang.Comparable
    /* renamed from: d */
    public int compareTo(SolverVariable solverVariable) {
        return this.g0 - solverVariable.g0;
    }

    public final void f(b bVar) {
        int i = this.p0;
        int i2 = 0;
        while (i2 < i) {
            if (this.o0[i2] == bVar) {
                while (i2 < i - 1) {
                    b[] bVarArr = this.o0;
                    int i3 = i2 + 1;
                    bVarArr[i2] = bVarArr[i3];
                    i2 = i3;
                }
                this.p0--;
                return;
            }
            i2++;
        }
    }

    public void g() {
        this.f0 = null;
        this.n0 = Type.UNKNOWN;
        this.i0 = 0;
        this.g0 = -1;
        this.h0 = -1;
        this.j0 = Utils.FLOAT_EPSILON;
        this.k0 = false;
        this.r0 = false;
        this.s0 = -1;
        this.t0 = Utils.FLOAT_EPSILON;
        int i = this.p0;
        for (int i2 = 0; i2 < i; i2++) {
            this.o0[i2] = null;
        }
        this.p0 = 0;
        this.q0 = 0;
        this.a = false;
        Arrays.fill(this.m0, (float) Utils.FLOAT_EPSILON);
    }

    public void h(c cVar, float f) {
        this.j0 = f;
        this.k0 = true;
        this.r0 = false;
        this.s0 = -1;
        this.t0 = Utils.FLOAT_EPSILON;
        int i = this.p0;
        this.h0 = -1;
        for (int i2 = 0; i2 < i; i2++) {
            this.o0[i2].A(cVar, this, false);
        }
        this.p0 = 0;
    }

    public void j(Type type, String str) {
        this.n0 = type;
    }

    public final void k(c cVar, b bVar) {
        int i = this.p0;
        for (int i2 = 0; i2 < i; i2++) {
            this.o0[i2].B(cVar, bVar, false);
        }
        this.p0 = 0;
    }

    public String toString() {
        if (this.f0 != null) {
            return "" + this.f0;
        }
        return "" + this.g0;
    }
}
