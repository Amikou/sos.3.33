package androidx.constraintlayout.core;

import androidx.constraintlayout.core.SolverVariable;
import androidx.constraintlayout.core.widgets.ConstraintAnchor;
import androidx.constraintlayout.core.widgets.ConstraintWidget;
import com.github.mikephil.charting.utils.Utils;
import java.util.Arrays;
import java.util.HashMap;

/* compiled from: LinearSystem.java */
/* loaded from: classes.dex */
public class c {
    public static boolean r = false;
    public static boolean s = true;
    public static boolean t = true;
    public static boolean u = true;
    public static boolean v = false;
    public static int w = 1000;
    public static s82 x;
    public static long y;
    public static long z;
    public a d;
    public androidx.constraintlayout.core.b[] g;
    public final ut n;
    public a q;
    public boolean a = false;
    public int b = 0;
    public HashMap<String, SolverVariable> c = null;
    public int e = 32;
    public int f = 32;
    public boolean h = false;
    public boolean i = false;
    public boolean[] j = new boolean[32];
    public int k = 1;
    public int l = 0;
    public int m = 32;
    public SolverVariable[] o = new SolverVariable[w];
    public int p = 0;

    /* compiled from: LinearSystem.java */
    /* loaded from: classes.dex */
    public interface a {
        void a(a aVar);

        SolverVariable b(c cVar, boolean[] zArr);

        void c(SolverVariable solverVariable);

        void clear();

        SolverVariable getKey();

        boolean isEmpty();
    }

    /* compiled from: LinearSystem.java */
    /* loaded from: classes.dex */
    public class b extends androidx.constraintlayout.core.b {
        public b(c cVar, ut utVar) {
            this.e = new e(this, utVar);
        }
    }

    public c() {
        this.g = null;
        this.g = new androidx.constraintlayout.core.b[32];
        C();
        ut utVar = new ut();
        this.n = utVar;
        this.d = new d(utVar);
        if (v) {
            this.q = new b(this, utVar);
        } else {
            this.q = new androidx.constraintlayout.core.b(utVar);
        }
    }

    public static androidx.constraintlayout.core.b s(c cVar, SolverVariable solverVariable, SolverVariable solverVariable2, float f) {
        return cVar.r().j(solverVariable, solverVariable2, f);
    }

    public static s82 w() {
        return x;
    }

    public void A(a aVar) throws Exception {
        u(aVar);
        B(aVar, false);
        n();
    }

    public final int B(a aVar, boolean z2) {
        for (int i = 0; i < this.k; i++) {
            this.j[i] = false;
        }
        boolean z3 = false;
        int i2 = 0;
        while (!z3) {
            i2++;
            if (i2 >= this.k * 2) {
                return i2;
            }
            if (aVar.getKey() != null) {
                this.j[aVar.getKey().g0] = true;
            }
            SolverVariable b2 = aVar.b(this, this.j);
            if (b2 != null) {
                boolean[] zArr = this.j;
                int i3 = b2.g0;
                if (zArr[i3]) {
                    return i2;
                }
                zArr[i3] = true;
            }
            if (b2 != null) {
                float f = Float.MAX_VALUE;
                int i4 = -1;
                for (int i5 = 0; i5 < this.l; i5++) {
                    androidx.constraintlayout.core.b bVar = this.g[i5];
                    if (bVar.a.n0 != SolverVariable.Type.UNRESTRICTED && !bVar.f && bVar.t(b2)) {
                        float j = bVar.e.j(b2);
                        if (j < Utils.FLOAT_EPSILON) {
                            float f2 = (-bVar.b) / j;
                            if (f2 < f) {
                                i4 = i5;
                                f = f2;
                            }
                        }
                    }
                }
                if (i4 > -1) {
                    androidx.constraintlayout.core.b bVar2 = this.g[i4];
                    bVar2.a.h0 = -1;
                    bVar2.x(b2);
                    SolverVariable solverVariable = bVar2.a;
                    solverVariable.h0 = i4;
                    solverVariable.k(this, bVar2);
                }
            } else {
                z3 = true;
            }
        }
        return i2;
    }

    public final void C() {
        int i = 0;
        if (v) {
            while (i < this.l) {
                androidx.constraintlayout.core.b bVar = this.g[i];
                if (bVar != null) {
                    this.n.a.a(bVar);
                }
                this.g[i] = null;
                i++;
            }
            return;
        }
        while (i < this.l) {
            androidx.constraintlayout.core.b bVar2 = this.g[i];
            if (bVar2 != null) {
                this.n.b.a(bVar2);
            }
            this.g[i] = null;
            i++;
        }
    }

    public void D() {
        ut utVar;
        int i = 0;
        while (true) {
            utVar = this.n;
            SolverVariable[] solverVariableArr = utVar.d;
            if (i >= solverVariableArr.length) {
                break;
            }
            SolverVariable solverVariable = solverVariableArr[i];
            if (solverVariable != null) {
                solverVariable.g();
            }
            i++;
        }
        utVar.c.c(this.o, this.p);
        this.p = 0;
        Arrays.fill(this.n.d, (Object) null);
        HashMap<String, SolverVariable> hashMap = this.c;
        if (hashMap != null) {
            hashMap.clear();
        }
        this.b = 0;
        this.d.clear();
        this.k = 1;
        for (int i2 = 0; i2 < this.l; i2++) {
            androidx.constraintlayout.core.b[] bVarArr = this.g;
            if (bVarArr[i2] != null) {
                bVarArr[i2].c = false;
            }
        }
        C();
        this.l = 0;
        if (v) {
            this.q = new b(this, this.n);
        } else {
            this.q = new androidx.constraintlayout.core.b(this.n);
        }
    }

    public final SolverVariable a(SolverVariable.Type type, String str) {
        SolverVariable b2 = this.n.c.b();
        if (b2 == null) {
            b2 = new SolverVariable(type, str);
            b2.j(type, str);
        } else {
            b2.g();
            b2.j(type, str);
        }
        int i = this.p;
        int i2 = w;
        if (i >= i2) {
            int i3 = i2 * 2;
            w = i3;
            this.o = (SolverVariable[]) Arrays.copyOf(this.o, i3);
        }
        SolverVariable[] solverVariableArr = this.o;
        int i4 = this.p;
        this.p = i4 + 1;
        solverVariableArr[i4] = b2;
        return b2;
    }

    public void b(ConstraintWidget constraintWidget, ConstraintWidget constraintWidget2, float f, int i) {
        ConstraintAnchor.Type type = ConstraintAnchor.Type.LEFT;
        SolverVariable q = q(constraintWidget.q(type));
        ConstraintAnchor.Type type2 = ConstraintAnchor.Type.TOP;
        SolverVariable q2 = q(constraintWidget.q(type2));
        ConstraintAnchor.Type type3 = ConstraintAnchor.Type.RIGHT;
        SolverVariable q3 = q(constraintWidget.q(type3));
        ConstraintAnchor.Type type4 = ConstraintAnchor.Type.BOTTOM;
        SolverVariable q4 = q(constraintWidget.q(type4));
        SolverVariable q5 = q(constraintWidget2.q(type));
        SolverVariable q6 = q(constraintWidget2.q(type2));
        SolverVariable q7 = q(constraintWidget2.q(type3));
        SolverVariable q8 = q(constraintWidget2.q(type4));
        androidx.constraintlayout.core.b r2 = r();
        double d = f;
        double d2 = i;
        r2.q(q2, q4, q6, q8, (float) (Math.sin(d) * d2));
        d(r2);
        androidx.constraintlayout.core.b r3 = r();
        r3.q(q, q3, q5, q7, (float) (Math.cos(d) * d2));
        d(r3);
    }

    public void c(SolverVariable solverVariable, SolverVariable solverVariable2, int i, float f, SolverVariable solverVariable3, SolverVariable solverVariable4, int i2, int i3) {
        androidx.constraintlayout.core.b r2 = r();
        r2.h(solverVariable, solverVariable2, i, f, solverVariable3, solverVariable4, i2);
        if (i3 != 8) {
            r2.d(this, i3);
        }
        d(r2);
    }

    /* JADX WARN: Removed duplicated region for block: B:36:0x0082 A[RETURN] */
    /* JADX WARN: Removed duplicated region for block: B:37:0x0083  */
    /*
        Code decompiled incorrectly, please refer to instructions dump.
        To view partially-correct code enable 'Show inconsistent code' option in preferences
    */
    public void d(androidx.constraintlayout.core.b r6) {
        /*
            r5 = this;
            if (r6 != 0) goto L3
            return
        L3:
            int r0 = r5.l
            r1 = 1
            int r0 = r0 + r1
            int r2 = r5.m
            if (r0 >= r2) goto L12
            int r0 = r5.k
            int r0 = r0 + r1
            int r2 = r5.f
            if (r0 < r2) goto L15
        L12:
            r5.y()
        L15:
            r0 = 0
            boolean r2 = r6.f
            if (r2 != 0) goto L84
            r6.D(r5)
            boolean r2 = r6.isEmpty()
            if (r2 == 0) goto L24
            return
        L24:
            r6.r()
            boolean r2 = r6.f(r5)
            if (r2 == 0) goto L7b
            androidx.constraintlayout.core.SolverVariable r2 = r5.p()
            r6.a = r2
            int r3 = r5.l
            r5.l(r6)
            int r4 = r5.l
            int r3 = r3 + r1
            if (r4 != r3) goto L7b
            androidx.constraintlayout.core.c$a r0 = r5.q
            r0.a(r6)
            androidx.constraintlayout.core.c$a r0 = r5.q
            r5.B(r0, r1)
            int r0 = r2.h0
            r3 = -1
            if (r0 != r3) goto L7c
            androidx.constraintlayout.core.SolverVariable r0 = r6.a
            if (r0 != r2) goto L59
            androidx.constraintlayout.core.SolverVariable r0 = r6.v(r2)
            if (r0 == 0) goto L59
            r6.x(r0)
        L59:
            boolean r0 = r6.f
            if (r0 != 0) goto L62
            androidx.constraintlayout.core.SolverVariable r0 = r6.a
            r0.k(r5, r6)
        L62:
            boolean r0 = androidx.constraintlayout.core.c.v
            if (r0 == 0) goto L6e
            ut r0 = r5.n
            ft2<androidx.constraintlayout.core.b> r0 = r0.a
            r0.a(r6)
            goto L75
        L6e:
            ut r0 = r5.n
            ft2<androidx.constraintlayout.core.b> r0 = r0.b
            r0.a(r6)
        L75:
            int r0 = r5.l
            int r0 = r0 - r1
            r5.l = r0
            goto L7c
        L7b:
            r1 = r0
        L7c:
            boolean r0 = r6.s()
            if (r0 != 0) goto L83
            return
        L83:
            r0 = r1
        L84:
            if (r0 != 0) goto L89
            r5.l(r6)
        L89:
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: androidx.constraintlayout.core.c.d(androidx.constraintlayout.core.b):void");
    }

    public androidx.constraintlayout.core.b e(SolverVariable solverVariable, SolverVariable solverVariable2, int i, int i2) {
        if (s && i2 == 8 && solverVariable2.k0 && solverVariable.h0 == -1) {
            solverVariable.h(this, solverVariable2.j0 + i);
            return null;
        }
        androidx.constraintlayout.core.b r2 = r();
        r2.n(solverVariable, solverVariable2, i);
        if (i2 != 8) {
            r2.d(this, i2);
        }
        d(r2);
        return r2;
    }

    public void f(SolverVariable solverVariable, int i) {
        if (s && solverVariable.h0 == -1) {
            float f = i;
            solverVariable.h(this, f);
            for (int i2 = 0; i2 < this.b + 1; i2++) {
                SolverVariable solverVariable2 = this.n.d[i2];
                if (solverVariable2 != null && solverVariable2.r0 && solverVariable2.s0 == solverVariable.g0) {
                    solverVariable2.h(this, solverVariable2.t0 + f);
                }
            }
            return;
        }
        int i3 = solverVariable.h0;
        if (i3 != -1) {
            androidx.constraintlayout.core.b bVar = this.g[i3];
            if (bVar.f) {
                bVar.b = i;
                return;
            } else if (bVar.e.a() == 0) {
                bVar.f = true;
                bVar.b = i;
                return;
            } else {
                androidx.constraintlayout.core.b r2 = r();
                r2.m(solverVariable, i);
                d(r2);
                return;
            }
        }
        androidx.constraintlayout.core.b r3 = r();
        r3.i(solverVariable, i);
        d(r3);
    }

    public void g(SolverVariable solverVariable, SolverVariable solverVariable2, int i, boolean z2) {
        androidx.constraintlayout.core.b r2 = r();
        SolverVariable t2 = t();
        t2.i0 = 0;
        r2.o(solverVariable, solverVariable2, t2, i);
        d(r2);
    }

    public void h(SolverVariable solverVariable, SolverVariable solverVariable2, int i, int i2) {
        androidx.constraintlayout.core.b r2 = r();
        SolverVariable t2 = t();
        t2.i0 = 0;
        r2.o(solverVariable, solverVariable2, t2, i);
        if (i2 != 8) {
            m(r2, (int) (r2.e.j(t2) * (-1.0f)), i2);
        }
        d(r2);
    }

    public void i(SolverVariable solverVariable, SolverVariable solverVariable2, int i, boolean z2) {
        androidx.constraintlayout.core.b r2 = r();
        SolverVariable t2 = t();
        t2.i0 = 0;
        r2.p(solverVariable, solverVariable2, t2, i);
        d(r2);
    }

    public void j(SolverVariable solverVariable, SolverVariable solverVariable2, int i, int i2) {
        androidx.constraintlayout.core.b r2 = r();
        SolverVariable t2 = t();
        t2.i0 = 0;
        r2.p(solverVariable, solverVariable2, t2, i);
        if (i2 != 8) {
            m(r2, (int) (r2.e.j(t2) * (-1.0f)), i2);
        }
        d(r2);
    }

    public void k(SolverVariable solverVariable, SolverVariable solverVariable2, SolverVariable solverVariable3, SolverVariable solverVariable4, float f, int i) {
        androidx.constraintlayout.core.b r2 = r();
        r2.k(solverVariable, solverVariable2, solverVariable3, solverVariable4, f);
        if (i != 8) {
            r2.d(this, i);
        }
        d(r2);
    }

    public final void l(androidx.constraintlayout.core.b bVar) {
        int i;
        if (t && bVar.f) {
            bVar.a.h(this, bVar.b);
        } else {
            androidx.constraintlayout.core.b[] bVarArr = this.g;
            int i2 = this.l;
            bVarArr[i2] = bVar;
            SolverVariable solverVariable = bVar.a;
            solverVariable.h0 = i2;
            this.l = i2 + 1;
            solverVariable.k(this, bVar);
        }
        if (t && this.a) {
            int i3 = 0;
            while (i3 < this.l) {
                if (this.g[i3] == null) {
                    System.out.println("WTF");
                }
                androidx.constraintlayout.core.b[] bVarArr2 = this.g;
                if (bVarArr2[i3] != null && bVarArr2[i3].f) {
                    androidx.constraintlayout.core.b bVar2 = bVarArr2[i3];
                    bVar2.a.h(this, bVar2.b);
                    if (v) {
                        this.n.a.a(bVar2);
                    } else {
                        this.n.b.a(bVar2);
                    }
                    this.g[i3] = null;
                    int i4 = i3 + 1;
                    int i5 = i4;
                    while (true) {
                        i = this.l;
                        if (i4 >= i) {
                            break;
                        }
                        androidx.constraintlayout.core.b[] bVarArr3 = this.g;
                        int i6 = i4 - 1;
                        bVarArr3[i6] = bVarArr3[i4];
                        if (bVarArr3[i6].a.h0 == i4) {
                            bVarArr3[i6].a.h0 = i6;
                        }
                        i5 = i4;
                        i4++;
                    }
                    if (i5 < i) {
                        this.g[i5] = null;
                    }
                    this.l = i - 1;
                    i3--;
                }
                i3++;
            }
            this.a = false;
        }
    }

    public void m(androidx.constraintlayout.core.b bVar, int i, int i2) {
        bVar.e(o(i2, null), i);
    }

    public final void n() {
        for (int i = 0; i < this.l; i++) {
            androidx.constraintlayout.core.b bVar = this.g[i];
            bVar.a.j0 = bVar.b;
        }
    }

    public SolverVariable o(int i, String str) {
        if (this.k + 1 >= this.f) {
            y();
        }
        SolverVariable a2 = a(SolverVariable.Type.ERROR, str);
        int i2 = this.b + 1;
        this.b = i2;
        this.k++;
        a2.g0 = i2;
        a2.i0 = i;
        this.n.d[i2] = a2;
        this.d.c(a2);
        return a2;
    }

    public SolverVariable p() {
        if (this.k + 1 >= this.f) {
            y();
        }
        SolverVariable a2 = a(SolverVariable.Type.SLACK, null);
        int i = this.b + 1;
        this.b = i;
        this.k++;
        a2.g0 = i;
        this.n.d[i] = a2;
        return a2;
    }

    public SolverVariable q(Object obj) {
        SolverVariable solverVariable = null;
        if (obj == null) {
            return null;
        }
        if (this.k + 1 >= this.f) {
            y();
        }
        if (obj instanceof ConstraintAnchor) {
            ConstraintAnchor constraintAnchor = (ConstraintAnchor) obj;
            solverVariable = constraintAnchor.i();
            if (solverVariable == null) {
                constraintAnchor.s(this.n);
                solverVariable = constraintAnchor.i();
            }
            int i = solverVariable.g0;
            if (i == -1 || i > this.b || this.n.d[i] == null) {
                if (i != -1) {
                    solverVariable.g();
                }
                int i2 = this.b + 1;
                this.b = i2;
                this.k++;
                solverVariable.g0 = i2;
                solverVariable.n0 = SolverVariable.Type.UNRESTRICTED;
                this.n.d[i2] = solverVariable;
            }
        }
        return solverVariable;
    }

    public androidx.constraintlayout.core.b r() {
        androidx.constraintlayout.core.b b2;
        if (v) {
            b2 = this.n.a.b();
            if (b2 == null) {
                b2 = new b(this, this.n);
                z++;
            } else {
                b2.y();
            }
        } else {
            b2 = this.n.b.b();
            if (b2 == null) {
                b2 = new androidx.constraintlayout.core.b(this.n);
                y++;
            } else {
                b2.y();
            }
        }
        SolverVariable.e();
        return b2;
    }

    public SolverVariable t() {
        if (this.k + 1 >= this.f) {
            y();
        }
        SolverVariable a2 = a(SolverVariable.Type.SLACK, null);
        int i = this.b + 1;
        this.b = i;
        this.k++;
        a2.g0 = i;
        this.n.d[i] = a2;
        return a2;
    }

    public final int u(a aVar) throws Exception {
        boolean z2;
        int i = 0;
        while (true) {
            if (i >= this.l) {
                z2 = false;
                break;
            }
            androidx.constraintlayout.core.b[] bVarArr = this.g;
            if (bVarArr[i].a.n0 != SolverVariable.Type.UNRESTRICTED && bVarArr[i].b < Utils.FLOAT_EPSILON) {
                z2 = true;
                break;
            }
            i++;
        }
        if (z2) {
            boolean z3 = false;
            int i2 = 0;
            while (!z3) {
                i2++;
                float f = Float.MAX_VALUE;
                int i3 = -1;
                int i4 = -1;
                int i5 = 0;
                for (int i6 = 0; i6 < this.l; i6++) {
                    androidx.constraintlayout.core.b bVar = this.g[i6];
                    if (bVar.a.n0 != SolverVariable.Type.UNRESTRICTED && !bVar.f && bVar.b < Utils.FLOAT_EPSILON) {
                        int i7 = 9;
                        if (u) {
                            int a2 = bVar.e.a();
                            int i8 = 0;
                            while (i8 < a2) {
                                SolverVariable e = bVar.e.e(i8);
                                float j = bVar.e.j(e);
                                if (j > Utils.FLOAT_EPSILON) {
                                    int i9 = 0;
                                    while (i9 < i7) {
                                        float f2 = e.l0[i9] / j;
                                        if ((f2 < f && i9 == i5) || i9 > i5) {
                                            i4 = e.g0;
                                            i5 = i9;
                                            i3 = i6;
                                            f = f2;
                                        }
                                        i9++;
                                        i7 = 9;
                                    }
                                }
                                i8++;
                                i7 = 9;
                            }
                        } else {
                            for (int i10 = 1; i10 < this.k; i10++) {
                                SolverVariable solverVariable = this.n.d[i10];
                                float j2 = bVar.e.j(solverVariable);
                                if (j2 > Utils.FLOAT_EPSILON) {
                                    for (int i11 = 0; i11 < 9; i11++) {
                                        float f3 = solverVariable.l0[i11] / j2;
                                        if ((f3 < f && i11 == i5) || i11 > i5) {
                                            i4 = i10;
                                            i3 = i6;
                                            i5 = i11;
                                            f = f3;
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
                if (i3 != -1) {
                    androidx.constraintlayout.core.b bVar2 = this.g[i3];
                    bVar2.a.h0 = -1;
                    bVar2.x(this.n.d[i4]);
                    SolverVariable solverVariable2 = bVar2.a;
                    solverVariable2.h0 = i3;
                    solverVariable2.k(this, bVar2);
                } else {
                    z3 = true;
                }
                if (i2 > this.k / 2) {
                    z3 = true;
                }
            }
            return i2;
        }
        return 0;
    }

    public ut v() {
        return this.n;
    }

    public int x(Object obj) {
        SolverVariable i = ((ConstraintAnchor) obj).i();
        if (i != null) {
            return (int) (i.j0 + 0.5f);
        }
        return 0;
    }

    public final void y() {
        int i = this.e * 2;
        this.e = i;
        this.g = (androidx.constraintlayout.core.b[]) Arrays.copyOf(this.g, i);
        ut utVar = this.n;
        utVar.d = (SolverVariable[]) Arrays.copyOf(utVar.d, this.e);
        int i2 = this.e;
        this.j = new boolean[i2];
        this.f = i2;
        this.m = i2;
    }

    public void z() throws Exception {
        if (this.d.isEmpty()) {
            n();
        } else if (!this.h && !this.i) {
            A(this.d);
        } else {
            boolean z2 = false;
            int i = 0;
            while (true) {
                if (i >= this.l) {
                    z2 = true;
                    break;
                } else if (!this.g[i].f) {
                    break;
                } else {
                    i++;
                }
            }
            if (!z2) {
                A(this.d);
            } else {
                n();
            }
        }
    }
}
