package androidx.constraintlayout.core;

import androidx.constraintlayout.core.SolverVariable;
import androidx.constraintlayout.core.c;
import com.github.mikephil.charting.utils.Utils;
import java.util.ArrayList;

/* compiled from: ArrayRow.java */
/* loaded from: classes.dex */
public class b implements c.a {
    public boolean c;
    public a e;
    public SolverVariable a = null;
    public float b = Utils.FLOAT_EPSILON;
    public ArrayList<SolverVariable> d = new ArrayList<>();
    public boolean f = false;

    /* compiled from: ArrayRow.java */
    /* loaded from: classes.dex */
    public interface a {
        int a();

        boolean b(SolverVariable solverVariable);

        float c(b bVar, boolean z);

        void clear();

        void d(SolverVariable solverVariable, float f);

        SolverVariable e(int i);

        void f(SolverVariable solverVariable, float f, boolean z);

        void g();

        float h(int i);

        float i(SolverVariable solverVariable, boolean z);

        float j(SolverVariable solverVariable);

        void k(float f);
    }

    public b() {
    }

    public void A(c cVar, SolverVariable solverVariable, boolean z) {
        if (solverVariable == null || !solverVariable.k0) {
            return;
        }
        this.b += solverVariable.j0 * this.e.j(solverVariable);
        this.e.i(solverVariable, z);
        if (z) {
            solverVariable.f(this);
        }
        if (c.t && this.e.a() == 0) {
            this.f = true;
            cVar.a = true;
        }
    }

    public void B(c cVar, b bVar, boolean z) {
        this.b += bVar.b * this.e.c(bVar, z);
        if (z) {
            bVar.a.f(this);
        }
        if (c.t && this.a != null && this.e.a() == 0) {
            this.f = true;
            cVar.a = true;
        }
    }

    public void C(c cVar, SolverVariable solverVariable, boolean z) {
        if (solverVariable == null || !solverVariable.r0) {
            return;
        }
        float j = this.e.j(solverVariable);
        this.b += solverVariable.t0 * j;
        this.e.i(solverVariable, z);
        if (z) {
            solverVariable.f(this);
        }
        this.e.f(cVar.n.d[solverVariable.s0], j, z);
        if (c.t && this.e.a() == 0) {
            this.f = true;
            cVar.a = true;
        }
    }

    public void D(c cVar) {
        if (cVar.g.length == 0) {
            return;
        }
        boolean z = false;
        while (!z) {
            int a2 = this.e.a();
            for (int i = 0; i < a2; i++) {
                SolverVariable e = this.e.e(i);
                if (e.h0 != -1 || e.k0 || e.r0) {
                    this.d.add(e);
                }
            }
            int size = this.d.size();
            if (size > 0) {
                for (int i2 = 0; i2 < size; i2++) {
                    SolverVariable solverVariable = this.d.get(i2);
                    if (solverVariable.k0) {
                        A(cVar, solverVariable, true);
                    } else if (solverVariable.r0) {
                        C(cVar, solverVariable, true);
                    } else {
                        B(cVar, cVar.g[solverVariable.h0], true);
                    }
                }
                this.d.clear();
            } else {
                z = true;
            }
        }
        if (c.t && this.a != null && this.e.a() == 0) {
            this.f = true;
            cVar.a = true;
        }
    }

    @Override // androidx.constraintlayout.core.c.a
    public void a(c.a aVar) {
        if (aVar instanceof b) {
            b bVar = (b) aVar;
            this.a = null;
            this.e.clear();
            for (int i = 0; i < bVar.e.a(); i++) {
                this.e.f(bVar.e.e(i), bVar.e.h(i), true);
            }
        }
    }

    @Override // androidx.constraintlayout.core.c.a
    public SolverVariable b(c cVar, boolean[] zArr) {
        return w(zArr, null);
    }

    @Override // androidx.constraintlayout.core.c.a
    public void c(SolverVariable solverVariable) {
        int i = solverVariable.i0;
        float f = 1.0f;
        if (i != 1) {
            if (i == 2) {
                f = 1000.0f;
            } else if (i == 3) {
                f = 1000000.0f;
            } else if (i == 4) {
                f = 1.0E9f;
            } else if (i == 5) {
                f = 1.0E12f;
            }
        }
        this.e.d(solverVariable, f);
    }

    @Override // androidx.constraintlayout.core.c.a
    public void clear() {
        this.e.clear();
        this.a = null;
        this.b = Utils.FLOAT_EPSILON;
    }

    public b d(c cVar, int i) {
        this.e.d(cVar.o(i, "ep"), 1.0f);
        this.e.d(cVar.o(i, "em"), -1.0f);
        return this;
    }

    public b e(SolverVariable solverVariable, int i) {
        this.e.d(solverVariable, i);
        return this;
    }

    public boolean f(c cVar) {
        boolean z;
        SolverVariable g = g(cVar);
        if (g == null) {
            z = true;
        } else {
            x(g);
            z = false;
        }
        if (this.e.a() == 0) {
            this.f = true;
        }
        return z;
    }

    public SolverVariable g(c cVar) {
        boolean u;
        boolean u2;
        int a2 = this.e.a();
        SolverVariable solverVariable = null;
        boolean z = false;
        boolean z2 = false;
        float f = 0.0f;
        float f2 = 0.0f;
        SolverVariable solverVariable2 = null;
        for (int i = 0; i < a2; i++) {
            float h = this.e.h(i);
            SolverVariable e = this.e.e(i);
            if (e.n0 == SolverVariable.Type.UNRESTRICTED) {
                if (solverVariable == null) {
                    u2 = u(e, cVar);
                } else if (f > h) {
                    u2 = u(e, cVar);
                } else if (!z && u(e, cVar)) {
                    f = h;
                    solverVariable = e;
                    z = true;
                }
                z = u2;
                f = h;
                solverVariable = e;
            } else if (solverVariable == null && h < Utils.FLOAT_EPSILON) {
                if (solverVariable2 == null) {
                    u = u(e, cVar);
                } else if (f2 > h) {
                    u = u(e, cVar);
                } else if (!z2 && u(e, cVar)) {
                    f2 = h;
                    solverVariable2 = e;
                    z2 = true;
                }
                z2 = u;
                f2 = h;
                solverVariable2 = e;
            }
        }
        return solverVariable != null ? solverVariable : solverVariable2;
    }

    @Override // androidx.constraintlayout.core.c.a
    public SolverVariable getKey() {
        return this.a;
    }

    public b h(SolverVariable solverVariable, SolverVariable solverVariable2, int i, float f, SolverVariable solverVariable3, SolverVariable solverVariable4, int i2) {
        if (solverVariable2 == solverVariable3) {
            this.e.d(solverVariable, 1.0f);
            this.e.d(solverVariable4, 1.0f);
            this.e.d(solverVariable2, -2.0f);
            return this;
        }
        if (f == 0.5f) {
            this.e.d(solverVariable, 1.0f);
            this.e.d(solverVariable2, -1.0f);
            this.e.d(solverVariable3, -1.0f);
            this.e.d(solverVariable4, 1.0f);
            if (i > 0 || i2 > 0) {
                this.b = (-i) + i2;
            }
        } else if (f <= Utils.FLOAT_EPSILON) {
            this.e.d(solverVariable, -1.0f);
            this.e.d(solverVariable2, 1.0f);
            this.b = i;
        } else if (f >= 1.0f) {
            this.e.d(solverVariable4, -1.0f);
            this.e.d(solverVariable3, 1.0f);
            this.b = -i2;
        } else {
            float f2 = 1.0f - f;
            this.e.d(solverVariable, f2 * 1.0f);
            this.e.d(solverVariable2, f2 * (-1.0f));
            this.e.d(solverVariable3, (-1.0f) * f);
            this.e.d(solverVariable4, 1.0f * f);
            if (i > 0 || i2 > 0) {
                this.b = ((-i) * f2) + (i2 * f);
            }
        }
        return this;
    }

    public b i(SolverVariable solverVariable, int i) {
        this.a = solverVariable;
        float f = i;
        solverVariable.j0 = f;
        this.b = f;
        this.f = true;
        return this;
    }

    @Override // androidx.constraintlayout.core.c.a
    public boolean isEmpty() {
        return this.a == null && this.b == Utils.FLOAT_EPSILON && this.e.a() == 0;
    }

    public b j(SolverVariable solverVariable, SolverVariable solverVariable2, float f) {
        this.e.d(solverVariable, -1.0f);
        this.e.d(solverVariable2, f);
        return this;
    }

    public b k(SolverVariable solverVariable, SolverVariable solverVariable2, SolverVariable solverVariable3, SolverVariable solverVariable4, float f) {
        this.e.d(solverVariable, -1.0f);
        this.e.d(solverVariable2, 1.0f);
        this.e.d(solverVariable3, f);
        this.e.d(solverVariable4, -f);
        return this;
    }

    public b l(float f, float f2, float f3, SolverVariable solverVariable, SolverVariable solverVariable2, SolverVariable solverVariable3, SolverVariable solverVariable4) {
        this.b = Utils.FLOAT_EPSILON;
        if (f2 == Utils.FLOAT_EPSILON || f == f3) {
            this.e.d(solverVariable, 1.0f);
            this.e.d(solverVariable2, -1.0f);
            this.e.d(solverVariable4, 1.0f);
            this.e.d(solverVariable3, -1.0f);
        } else if (f == Utils.FLOAT_EPSILON) {
            this.e.d(solverVariable, 1.0f);
            this.e.d(solverVariable2, -1.0f);
        } else if (f3 == Utils.FLOAT_EPSILON) {
            this.e.d(solverVariable3, 1.0f);
            this.e.d(solverVariable4, -1.0f);
        } else {
            float f4 = (f / f2) / (f3 / f2);
            this.e.d(solverVariable, 1.0f);
            this.e.d(solverVariable2, -1.0f);
            this.e.d(solverVariable4, f4);
            this.e.d(solverVariable3, -f4);
        }
        return this;
    }

    public b m(SolverVariable solverVariable, int i) {
        if (i < 0) {
            this.b = i * (-1);
            this.e.d(solverVariable, 1.0f);
        } else {
            this.b = i;
            this.e.d(solverVariable, -1.0f);
        }
        return this;
    }

    public b n(SolverVariable solverVariable, SolverVariable solverVariable2, int i) {
        boolean z = false;
        if (i != 0) {
            if (i < 0) {
                i *= -1;
                z = true;
            }
            this.b = i;
        }
        if (!z) {
            this.e.d(solverVariable, -1.0f);
            this.e.d(solverVariable2, 1.0f);
        } else {
            this.e.d(solverVariable, 1.0f);
            this.e.d(solverVariable2, -1.0f);
        }
        return this;
    }

    public b o(SolverVariable solverVariable, SolverVariable solverVariable2, SolverVariable solverVariable3, int i) {
        boolean z = false;
        if (i != 0) {
            if (i < 0) {
                i *= -1;
                z = true;
            }
            this.b = i;
        }
        if (!z) {
            this.e.d(solverVariable, -1.0f);
            this.e.d(solverVariable2, 1.0f);
            this.e.d(solverVariable3, 1.0f);
        } else {
            this.e.d(solverVariable, 1.0f);
            this.e.d(solverVariable2, -1.0f);
            this.e.d(solverVariable3, -1.0f);
        }
        return this;
    }

    public b p(SolverVariable solverVariable, SolverVariable solverVariable2, SolverVariable solverVariable3, int i) {
        boolean z = false;
        if (i != 0) {
            if (i < 0) {
                i *= -1;
                z = true;
            }
            this.b = i;
        }
        if (!z) {
            this.e.d(solverVariable, -1.0f);
            this.e.d(solverVariable2, 1.0f);
            this.e.d(solverVariable3, -1.0f);
        } else {
            this.e.d(solverVariable, 1.0f);
            this.e.d(solverVariable2, -1.0f);
            this.e.d(solverVariable3, 1.0f);
        }
        return this;
    }

    public b q(SolverVariable solverVariable, SolverVariable solverVariable2, SolverVariable solverVariable3, SolverVariable solverVariable4, float f) {
        this.e.d(solverVariable3, 0.5f);
        this.e.d(solverVariable4, 0.5f);
        this.e.d(solverVariable, -0.5f);
        this.e.d(solverVariable2, -0.5f);
        this.b = -f;
        return this;
    }

    public void r() {
        float f = this.b;
        if (f < Utils.FLOAT_EPSILON) {
            this.b = f * (-1.0f);
            this.e.g();
        }
    }

    public boolean s() {
        SolverVariable solverVariable = this.a;
        return solverVariable != null && (solverVariable.n0 == SolverVariable.Type.UNRESTRICTED || this.b >= Utils.FLOAT_EPSILON);
    }

    public boolean t(SolverVariable solverVariable) {
        return this.e.b(solverVariable);
    }

    public String toString() {
        return z();
    }

    public final boolean u(SolverVariable solverVariable, c cVar) {
        return solverVariable.q0 <= 1;
    }

    public SolverVariable v(SolverVariable solverVariable) {
        return w(null, solverVariable);
    }

    public final SolverVariable w(boolean[] zArr, SolverVariable solverVariable) {
        SolverVariable.Type type;
        int a2 = this.e.a();
        SolverVariable solverVariable2 = null;
        float f = 0.0f;
        for (int i = 0; i < a2; i++) {
            float h = this.e.h(i);
            if (h < Utils.FLOAT_EPSILON) {
                SolverVariable e = this.e.e(i);
                if ((zArr == null || !zArr[e.g0]) && e != solverVariable && (((type = e.n0) == SolverVariable.Type.SLACK || type == SolverVariable.Type.ERROR) && h < f)) {
                    f = h;
                    solverVariable2 = e;
                }
            }
        }
        return solverVariable2;
    }

    public void x(SolverVariable solverVariable) {
        SolverVariable solverVariable2 = this.a;
        if (solverVariable2 != null) {
            this.e.d(solverVariable2, -1.0f);
            this.a.h0 = -1;
            this.a = null;
        }
        float i = this.e.i(solverVariable, true) * (-1.0f);
        this.a = solverVariable;
        if (i == 1.0f) {
            return;
        }
        this.b /= i;
        this.e.k(i);
    }

    public void y() {
        this.a = null;
        this.e.clear();
        this.b = Utils.FLOAT_EPSILON;
        this.f = false;
    }

    /* JADX WARN: Removed duplicated region for block: B:29:0x00bf  */
    /* JADX WARN: Removed duplicated region for block: B:30:0x00cf  */
    /*
        Code decompiled incorrectly, please refer to instructions dump.
        To view partially-correct code enable 'Show inconsistent code' option in preferences
    */
    public java.lang.String z() {
        /*
            Method dump skipped, instructions count: 255
            To view this dump change 'Code comments level' option to 'DEBUG'
        */
        throw new UnsupportedOperationException("Method not decompiled: androidx.constraintlayout.core.b.z():java.lang.String");
    }

    public b(ut utVar) {
        this.e = new androidx.constraintlayout.core.a(this, utVar);
    }
}
