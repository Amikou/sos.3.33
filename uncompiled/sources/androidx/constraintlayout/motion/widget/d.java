package androidx.constraintlayout.motion.widget;

import android.content.Context;
import android.content.res.TypedArray;
import android.util.AttributeSet;
import android.util.SparseIntArray;
import java.util.HashMap;

/* compiled from: KeyPosition.java */
/* loaded from: classes.dex */
public class d extends lx1 {
    public String h = null;
    public int i = androidx.constraintlayout.motion.widget.a.f;
    public int j = 0;
    public float k = Float.NaN;
    public float l = Float.NaN;
    public float m = Float.NaN;
    public float n = Float.NaN;
    public float o = Float.NaN;
    public float p = Float.NaN;
    public int q = 0;
    public float r = Float.NaN;
    public float s = Float.NaN;

    /* compiled from: KeyPosition.java */
    /* loaded from: classes.dex */
    public static class a {
        public static SparseIntArray a;

        static {
            SparseIntArray sparseIntArray = new SparseIntArray();
            a = sparseIntArray;
            sparseIntArray.append(w23.KeyPosition_motionTarget, 1);
            a.append(w23.KeyPosition_framePosition, 2);
            a.append(w23.KeyPosition_transitionEasing, 3);
            a.append(w23.KeyPosition_curveFit, 4);
            a.append(w23.KeyPosition_drawPath, 5);
            a.append(w23.KeyPosition_percentX, 6);
            a.append(w23.KeyPosition_percentY, 7);
            a.append(w23.KeyPosition_keyPositionType, 9);
            a.append(w23.KeyPosition_sizePercent, 8);
            a.append(w23.KeyPosition_percentWidth, 11);
            a.append(w23.KeyPosition_percentHeight, 12);
            a.append(w23.KeyPosition_pathMotionArc, 10);
        }

        public static void b(d dVar, TypedArray typedArray) {
            int indexCount = typedArray.getIndexCount();
            for (int i = 0; i < indexCount; i++) {
                int index = typedArray.getIndex(i);
                switch (a.get(index)) {
                    case 1:
                        if (MotionLayout.T1) {
                            int resourceId = typedArray.getResourceId(index, dVar.b);
                            dVar.b = resourceId;
                            if (resourceId == -1) {
                                dVar.c = typedArray.getString(index);
                                break;
                            } else {
                                break;
                            }
                        } else if (typedArray.peekValue(index).type == 3) {
                            dVar.c = typedArray.getString(index);
                            break;
                        } else {
                            dVar.b = typedArray.getResourceId(index, dVar.b);
                            break;
                        }
                    case 2:
                        dVar.a = typedArray.getInt(index, dVar.a);
                        break;
                    case 3:
                        if (typedArray.peekValue(index).type == 3) {
                            dVar.h = typedArray.getString(index);
                            break;
                        } else {
                            dVar.h = yt0.c[typedArray.getInteger(index, 0)];
                            break;
                        }
                    case 4:
                        dVar.g = typedArray.getInteger(index, dVar.g);
                        break;
                    case 5:
                        dVar.j = typedArray.getInt(index, dVar.j);
                        break;
                    case 6:
                        dVar.m = typedArray.getFloat(index, dVar.m);
                        break;
                    case 7:
                        dVar.n = typedArray.getFloat(index, dVar.n);
                        break;
                    case 8:
                        float f = typedArray.getFloat(index, dVar.l);
                        dVar.k = f;
                        dVar.l = f;
                        break;
                    case 9:
                        dVar.q = typedArray.getInt(index, dVar.q);
                        break;
                    case 10:
                        dVar.i = typedArray.getInt(index, dVar.i);
                        break;
                    case 11:
                        dVar.k = typedArray.getFloat(index, dVar.k);
                        break;
                    case 12:
                        dVar.l = typedArray.getFloat(index, dVar.l);
                        break;
                    default:
                        StringBuilder sb = new StringBuilder();
                        sb.append("unused attribute 0x");
                        sb.append(Integer.toHexString(index));
                        sb.append("   ");
                        sb.append(a.get(index));
                        break;
                }
            }
            int i2 = dVar.a;
        }
    }

    public d() {
        this.d = 2;
    }

    @Override // androidx.constraintlayout.motion.widget.a
    public void a(HashMap<String, ak4> hashMap) {
    }

    @Override // androidx.constraintlayout.motion.widget.a
    /* renamed from: b */
    public androidx.constraintlayout.motion.widget.a clone() {
        return new d().c(this);
    }

    @Override // androidx.constraintlayout.motion.widget.a
    public androidx.constraintlayout.motion.widget.a c(androidx.constraintlayout.motion.widget.a aVar) {
        super.c(aVar);
        d dVar = (d) aVar;
        this.h = dVar.h;
        this.i = dVar.i;
        this.j = dVar.j;
        this.k = dVar.k;
        this.l = Float.NaN;
        this.m = dVar.m;
        this.n = dVar.n;
        this.o = dVar.o;
        this.p = dVar.p;
        this.r = dVar.r;
        this.s = dVar.s;
        return this;
    }

    @Override // androidx.constraintlayout.motion.widget.a
    public void e(Context context, AttributeSet attributeSet) {
        a.b(this, context.obtainStyledAttributes(attributeSet, w23.KeyPosition));
    }

    public void m(int i) {
        this.q = i;
    }

    public void n(String str, Object obj) {
        str.hashCode();
        char c = 65535;
        switch (str.hashCode()) {
            case -1812823328:
                if (str.equals("transitionEasing")) {
                    c = 0;
                    break;
                }
                break;
            case -1127236479:
                if (str.equals("percentWidth")) {
                    c = 1;
                    break;
                }
                break;
            case -1017587252:
                if (str.equals("percentHeight")) {
                    c = 2;
                    break;
                }
                break;
            case -827014263:
                if (str.equals("drawPath")) {
                    c = 3;
                    break;
                }
                break;
            case -200259324:
                if (str.equals("sizePercent")) {
                    c = 4;
                    break;
                }
                break;
            case 428090547:
                if (str.equals("percentX")) {
                    c = 5;
                    break;
                }
                break;
            case 428090548:
                if (str.equals("percentY")) {
                    c = 6;
                    break;
                }
                break;
        }
        switch (c) {
            case 0:
                this.h = obj.toString();
                return;
            case 1:
                this.k = k(obj);
                return;
            case 2:
                this.l = k(obj);
                return;
            case 3:
                this.j = l(obj);
                return;
            case 4:
                float k = k(obj);
                this.k = k;
                this.l = k;
                return;
            case 5:
                this.m = k(obj);
                return;
            case 6:
                this.n = k(obj);
                return;
            default:
                return;
        }
    }
}
