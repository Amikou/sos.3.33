package androidx.constraintlayout.motion.widget;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Canvas;
import android.graphics.DashPathEffect;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.graphics.Path;
import android.graphics.Rect;
import android.graphics.RectF;
import android.os.Build;
import android.os.Bundle;
import android.util.AttributeSet;
import android.util.SparseArray;
import android.util.SparseBooleanArray;
import android.util.SparseIntArray;
import android.view.Display;
import android.view.MotionEvent;
import android.view.VelocityTracker;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Interpolator;
import androidx.constraintlayout.core.widgets.ConstraintWidget;
import androidx.constraintlayout.motion.widget.g;
import androidx.constraintlayout.widget.Barrier;
import androidx.constraintlayout.widget.ConstraintHelper;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.constraintlayout.widget.Constraints;
import com.github.mikephil.charting.utils.Utils;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.concurrent.CopyOnWriteArrayList;

/* loaded from: classes.dex */
public class MotionLayout extends ConstraintLayout implements se2 {
    public static boolean T1;
    public Interpolator A0;
    public boolean A1;
    public float B0;
    public i B1;
    public int C0;
    public Runnable C1;
    public int D0;
    public int[] D1;
    public int E0;
    public int E1;
    public int F0;
    public boolean F1;
    public int G0;
    public int G1;
    public boolean H0;
    public HashMap<View, bk4> H1;
    public HashMap<View, u92> I0;
    public int I1;
    public long J0;
    public int J1;
    public float K0;
    public Rect K1;
    public float L0;
    public boolean L1;
    public float M0;
    public TransitionState M1;
    public long N0;
    public f N1;
    public float O0;
    public boolean O1;
    public boolean P0;
    public RectF P1;
    public boolean Q0;
    public View Q1;
    public j R0;
    public Matrix R1;
    public float S0;
    public ArrayList<Integer> S1;
    public float T0;
    public int U0;
    public e V0;
    public boolean W0;
    public st3 X0;
    public d Y0;
    public pm0 Z0;
    public int a1;
    public int b1;
    public boolean c1;
    public float d1;
    public float e1;
    public long f1;
    public float g1;
    public boolean h1;
    public ArrayList<MotionHelper> i1;
    public ArrayList<MotionHelper> j1;
    public ArrayList<MotionHelper> k1;
    public CopyOnWriteArrayList<j> l1;
    public int m1;
    public long n1;
    public float o1;
    public int p1;
    public float q1;
    public boolean r1;
    public int s1;
    public int t1;
    public int u1;
    public int v1;
    public int w1;
    public int x1;
    public androidx.constraintlayout.motion.widget.g y0;
    public float y1;
    public Interpolator z0;
    public gx1 z1;

    /* loaded from: classes.dex */
    public enum TransitionState {
        UNDEFINED,
        SETUP,
        MOVING,
        FINISHED
    }

    /* loaded from: classes.dex */
    public class a implements Runnable {
        public final /* synthetic */ View a;

        public a(MotionLayout motionLayout, View view) {
            this.a = view;
        }

        @Override // java.lang.Runnable
        public void run() {
            this.a.setNestedScrollingEnabled(true);
        }
    }

    /* loaded from: classes.dex */
    public class b implements Runnable {
        public b() {
        }

        @Override // java.lang.Runnable
        public void run() {
            MotionLayout.this.B1.a();
        }
    }

    /* loaded from: classes.dex */
    public static /* synthetic */ class c {
        public static final /* synthetic */ int[] a;

        static {
            int[] iArr = new int[TransitionState.values().length];
            a = iArr;
            try {
                iArr[TransitionState.UNDEFINED.ordinal()] = 1;
            } catch (NoSuchFieldError unused) {
            }
            try {
                a[TransitionState.SETUP.ordinal()] = 2;
            } catch (NoSuchFieldError unused2) {
            }
            try {
                a[TransitionState.MOVING.ordinal()] = 3;
            } catch (NoSuchFieldError unused3) {
            }
            try {
                a[TransitionState.FINISHED.ordinal()] = 4;
            } catch (NoSuchFieldError unused4) {
            }
        }
    }

    /* loaded from: classes.dex */
    public class d extends w92 {
        public float a = Utils.FLOAT_EPSILON;
        public float b = Utils.FLOAT_EPSILON;
        public float c;

        public d() {
        }

        @Override // defpackage.w92
        public float a() {
            return MotionLayout.this.B0;
        }

        public void b(float f, float f2, float f3) {
            this.a = f;
            this.b = f2;
            this.c = f3;
        }

        @Override // android.animation.TimeInterpolator
        public float getInterpolation(float f) {
            float f2;
            float f3;
            float f4 = this.a;
            if (f4 > Utils.FLOAT_EPSILON) {
                float f5 = this.c;
                if (f4 / f5 < f) {
                    f = f4 / f5;
                }
                MotionLayout.this.B0 = f4 - (f5 * f);
                f2 = (f4 * f) - (((f5 * f) * f) / 2.0f);
                f3 = this.b;
            } else {
                float f6 = this.c;
                if ((-f4) / f6 < f) {
                    f = (-f4) / f6;
                }
                MotionLayout.this.B0 = (f6 * f) + f4;
                f2 = (f4 * f) + (((f6 * f) * f) / 2.0f);
                f3 = this.b;
            }
            return f2 + f3;
        }
    }

    /* loaded from: classes.dex */
    public class e {
        public float[] a;
        public int[] b;
        public float[] c;
        public Path d;
        public Paint e;
        public Paint f;
        public Paint g;
        public Paint h;
        public Paint i;
        public float[] j;
        public DashPathEffect k;
        public int l;
        public Rect m = new Rect();
        public boolean n = false;
        public int o;

        public e() {
            this.o = 1;
            Paint paint = new Paint();
            this.e = paint;
            paint.setAntiAlias(true);
            this.e.setColor(-21965);
            this.e.setStrokeWidth(2.0f);
            this.e.setStyle(Paint.Style.STROKE);
            Paint paint2 = new Paint();
            this.f = paint2;
            paint2.setAntiAlias(true);
            this.f.setColor(-2067046);
            this.f.setStrokeWidth(2.0f);
            this.f.setStyle(Paint.Style.STROKE);
            Paint paint3 = new Paint();
            this.g = paint3;
            paint3.setAntiAlias(true);
            this.g.setColor(-13391360);
            this.g.setStrokeWidth(2.0f);
            this.g.setStyle(Paint.Style.STROKE);
            Paint paint4 = new Paint();
            this.h = paint4;
            paint4.setAntiAlias(true);
            this.h.setColor(-13391360);
            this.h.setTextSize(MotionLayout.this.getContext().getResources().getDisplayMetrics().density * 12.0f);
            this.j = new float[8];
            Paint paint5 = new Paint();
            this.i = paint5;
            paint5.setAntiAlias(true);
            DashPathEffect dashPathEffect = new DashPathEffect(new float[]{4.0f, 8.0f}, Utils.FLOAT_EPSILON);
            this.k = dashPathEffect;
            this.g.setPathEffect(dashPathEffect);
            this.c = new float[100];
            this.b = new int[50];
            if (this.n) {
                this.e.setStrokeWidth(8.0f);
                this.i.setStrokeWidth(8.0f);
                this.f.setStrokeWidth(8.0f);
                this.o = 4;
            }
        }

        public void a(Canvas canvas, HashMap<View, u92> hashMap, int i, int i2) {
            if (hashMap == null || hashMap.size() == 0) {
                return;
            }
            canvas.save();
            if (!MotionLayout.this.isInEditMode() && (i2 & 1) == 2) {
                String str = MotionLayout.this.getContext().getResources().getResourceName(MotionLayout.this.E0) + ":" + MotionLayout.this.getProgress();
                canvas.drawText(str, 10.0f, MotionLayout.this.getHeight() - 30, this.h);
                canvas.drawText(str, 11.0f, MotionLayout.this.getHeight() - 29, this.e);
            }
            for (u92 u92Var : hashMap.values()) {
                int m = u92Var.m();
                if (i2 > 0 && m == 0) {
                    m = 1;
                }
                if (m != 0) {
                    this.l = u92Var.c(this.c, this.b);
                    if (m >= 1) {
                        int i3 = i / 16;
                        float[] fArr = this.a;
                        if (fArr == null || fArr.length != i3 * 2) {
                            this.a = new float[i3 * 2];
                            this.d = new Path();
                        }
                        int i4 = this.o;
                        canvas.translate(i4, i4);
                        this.e.setColor(1996488704);
                        this.i.setColor(1996488704);
                        this.f.setColor(1996488704);
                        this.g.setColor(1996488704);
                        u92Var.d(this.a, i3);
                        b(canvas, m, this.l, u92Var);
                        this.e.setColor(-21965);
                        this.f.setColor(-2067046);
                        this.i.setColor(-2067046);
                        this.g.setColor(-13391360);
                        int i5 = this.o;
                        canvas.translate(-i5, -i5);
                        b(canvas, m, this.l, u92Var);
                        if (m == 5) {
                            j(canvas, u92Var);
                        }
                    }
                }
            }
            canvas.restore();
        }

        public void b(Canvas canvas, int i, int i2, u92 u92Var) {
            if (i == 4) {
                d(canvas);
            }
            if (i == 2) {
                g(canvas);
            }
            if (i == 3) {
                e(canvas);
            }
            c(canvas);
            k(canvas, i, i2, u92Var);
        }

        public final void c(Canvas canvas) {
            canvas.drawLines(this.a, this.e);
        }

        public final void d(Canvas canvas) {
            boolean z = false;
            boolean z2 = false;
            for (int i = 0; i < this.l; i++) {
                int[] iArr = this.b;
                if (iArr[i] == 1) {
                    z = true;
                }
                if (iArr[i] == 0) {
                    z2 = true;
                }
            }
            if (z) {
                g(canvas);
            }
            if (z2) {
                e(canvas);
            }
        }

        public final void e(Canvas canvas) {
            float[] fArr = this.a;
            float f = fArr[0];
            float f2 = fArr[1];
            float f3 = fArr[fArr.length - 2];
            float f4 = fArr[fArr.length - 1];
            canvas.drawLine(Math.min(f, f3), Math.max(f2, f4), Math.max(f, f3), Math.max(f2, f4), this.g);
            canvas.drawLine(Math.min(f, f3), Math.min(f2, f4), Math.min(f, f3), Math.max(f2, f4), this.g);
        }

        public final void f(Canvas canvas, float f, float f2) {
            float[] fArr = this.a;
            float f3 = fArr[0];
            float f4 = fArr[1];
            float f5 = fArr[fArr.length - 2];
            float f6 = fArr[fArr.length - 1];
            float min = Math.min(f3, f5);
            float max = Math.max(f4, f6);
            float min2 = f - Math.min(f3, f5);
            float max2 = Math.max(f4, f6) - f2;
            String str = "" + (((int) (((min2 * 100.0f) / Math.abs(f5 - f3)) + 0.5d)) / 100.0f);
            l(str, this.h);
            canvas.drawText(str, ((min2 / 2.0f) - (this.m.width() / 2)) + min, f2 - 20.0f, this.h);
            canvas.drawLine(f, f2, Math.min(f3, f5), f2, this.g);
            String str2 = "" + (((int) (((max2 * 100.0f) / Math.abs(f6 - f4)) + 0.5d)) / 100.0f);
            l(str2, this.h);
            canvas.drawText(str2, f + 5.0f, max - ((max2 / 2.0f) - (this.m.height() / 2)), this.h);
            canvas.drawLine(f, f2, f, Math.max(f4, f6), this.g);
        }

        public final void g(Canvas canvas) {
            float[] fArr = this.a;
            canvas.drawLine(fArr[0], fArr[1], fArr[fArr.length - 2], fArr[fArr.length - 1], this.g);
        }

        public final void h(Canvas canvas, float f, float f2) {
            float[] fArr = this.a;
            float f3 = fArr[0];
            float f4 = fArr[1];
            float f5 = fArr[fArr.length - 2];
            float f6 = fArr[fArr.length - 1];
            float hypot = (float) Math.hypot(f3 - f5, f4 - f6);
            float f7 = f5 - f3;
            float f8 = f6 - f4;
            float f9 = (((f - f3) * f7) + ((f2 - f4) * f8)) / (hypot * hypot);
            float f10 = f3 + (f7 * f9);
            float f11 = f4 + (f9 * f8);
            Path path = new Path();
            path.moveTo(f, f2);
            path.lineTo(f10, f11);
            float hypot2 = (float) Math.hypot(f10 - f, f11 - f2);
            String str = "" + (((int) ((hypot2 * 100.0f) / hypot)) / 100.0f);
            l(str, this.h);
            canvas.drawTextOnPath(str, path, (hypot2 / 2.0f) - (this.m.width() / 2), -20.0f, this.h);
            canvas.drawLine(f, f2, f10, f11, this.g);
        }

        public final void i(Canvas canvas, float f, float f2, int i, int i2) {
            String str = "" + (((int) ((((f - (i / 2)) * 100.0f) / (MotionLayout.this.getWidth() - i)) + 0.5d)) / 100.0f);
            l(str, this.h);
            canvas.drawText(str, ((f / 2.0f) - (this.m.width() / 2)) + Utils.FLOAT_EPSILON, f2 - 20.0f, this.h);
            canvas.drawLine(f, f2, Math.min((float) Utils.FLOAT_EPSILON, 1.0f), f2, this.g);
            String str2 = "" + (((int) ((((f2 - (i2 / 2)) * 100.0f) / (MotionLayout.this.getHeight() - i2)) + 0.5d)) / 100.0f);
            l(str2, this.h);
            canvas.drawText(str2, f + 5.0f, Utils.FLOAT_EPSILON - ((f2 / 2.0f) - (this.m.height() / 2)), this.h);
            canvas.drawLine(f, f2, f, Math.max((float) Utils.FLOAT_EPSILON, 1.0f), this.g);
        }

        public final void j(Canvas canvas, u92 u92Var) {
            this.d.reset();
            for (int i = 0; i <= 50; i++) {
                u92Var.e(i / 50, this.j, 0);
                Path path = this.d;
                float[] fArr = this.j;
                path.moveTo(fArr[0], fArr[1]);
                Path path2 = this.d;
                float[] fArr2 = this.j;
                path2.lineTo(fArr2[2], fArr2[3]);
                Path path3 = this.d;
                float[] fArr3 = this.j;
                path3.lineTo(fArr3[4], fArr3[5]);
                Path path4 = this.d;
                float[] fArr4 = this.j;
                path4.lineTo(fArr4[6], fArr4[7]);
                this.d.close();
            }
            this.e.setColor(1140850688);
            canvas.translate(2.0f, 2.0f);
            canvas.drawPath(this.d, this.e);
            canvas.translate(-2.0f, -2.0f);
            this.e.setColor(-65536);
            canvas.drawPath(this.d, this.e);
        }

        public final void k(Canvas canvas, int i, int i2, u92 u92Var) {
            int i3;
            int i4;
            float f;
            float f2;
            View view = u92Var.b;
            if (view != null) {
                i3 = view.getWidth();
                i4 = u92Var.b.getHeight();
            } else {
                i3 = 0;
                i4 = 0;
            }
            for (int i5 = 1; i5 < i2 - 1; i5++) {
                if (i != 4 || this.b[i5 - 1] != 0) {
                    float[] fArr = this.c;
                    int i6 = i5 * 2;
                    float f3 = fArr[i6];
                    float f4 = fArr[i6 + 1];
                    this.d.reset();
                    this.d.moveTo(f3, f4 + 10.0f);
                    this.d.lineTo(f3 + 10.0f, f4);
                    this.d.lineTo(f3, f4 - 10.0f);
                    this.d.lineTo(f3 - 10.0f, f4);
                    this.d.close();
                    int i7 = i5 - 1;
                    u92Var.q(i7);
                    if (i == 4) {
                        int[] iArr = this.b;
                        if (iArr[i7] == 1) {
                            h(canvas, f3 - Utils.FLOAT_EPSILON, f4 - Utils.FLOAT_EPSILON);
                        } else if (iArr[i7] == 0) {
                            f(canvas, f3 - Utils.FLOAT_EPSILON, f4 - Utils.FLOAT_EPSILON);
                        } else if (iArr[i7] == 2) {
                            f = f4;
                            f2 = f3;
                            i(canvas, f3 - Utils.FLOAT_EPSILON, f4 - Utils.FLOAT_EPSILON, i3, i4);
                            canvas.drawPath(this.d, this.i);
                        }
                        f = f4;
                        f2 = f3;
                        canvas.drawPath(this.d, this.i);
                    } else {
                        f = f4;
                        f2 = f3;
                    }
                    if (i == 2) {
                        h(canvas, f2 - Utils.FLOAT_EPSILON, f - Utils.FLOAT_EPSILON);
                    }
                    if (i == 3) {
                        f(canvas, f2 - Utils.FLOAT_EPSILON, f - Utils.FLOAT_EPSILON);
                    }
                    if (i == 6) {
                        i(canvas, f2 - Utils.FLOAT_EPSILON, f - Utils.FLOAT_EPSILON, i3, i4);
                    }
                    canvas.drawPath(this.d, this.i);
                }
            }
            float[] fArr2 = this.a;
            if (fArr2.length > 1) {
                canvas.drawCircle(fArr2[0], fArr2[1], 8.0f, this.f);
                float[] fArr3 = this.a;
                canvas.drawCircle(fArr3[fArr3.length - 2], fArr3[fArr3.length - 1], 8.0f, this.f);
            }
        }

        public void l(String str, Paint paint) {
            paint.getTextBounds(str, 0, str.length(), this.m);
        }
    }

    /* loaded from: classes.dex */
    public class f {
        public androidx.constraintlayout.core.widgets.d a = new androidx.constraintlayout.core.widgets.d();
        public androidx.constraintlayout.core.widgets.d b = new androidx.constraintlayout.core.widgets.d();
        public androidx.constraintlayout.widget.a c = null;
        public androidx.constraintlayout.widget.a d = null;
        public int e;
        public int f;

        public f() {
        }

        /* JADX WARN: Removed duplicated region for block: B:24:0x00d6  */
        /* JADX WARN: Removed duplicated region for block: B:41:0x0123 A[SYNTHETIC] */
        /*
            Code decompiled incorrectly, please refer to instructions dump.
            To view partially-correct code enable 'Show inconsistent code' option in preferences
        */
        public void a() {
            /*
                Method dump skipped, instructions count: 326
                To view this dump change 'Code comments level' option to 'DEBUG'
            */
            throw new UnsupportedOperationException("Method not decompiled: androidx.constraintlayout.motion.widget.MotionLayout.f.a():void");
        }

        public final void b(int i, int i2) {
            int optimizationLevel = MotionLayout.this.getOptimizationLevel();
            MotionLayout motionLayout = MotionLayout.this;
            if (motionLayout.D0 == motionLayout.getStartState()) {
                MotionLayout motionLayout2 = MotionLayout.this;
                androidx.constraintlayout.core.widgets.d dVar = this.b;
                androidx.constraintlayout.widget.a aVar = this.d;
                motionLayout2.p(dVar, optimizationLevel, (aVar == null || aVar.c == 0) ? i : i2, (aVar == null || aVar.c == 0) ? i2 : i);
                androidx.constraintlayout.widget.a aVar2 = this.c;
                if (aVar2 != null) {
                    MotionLayout motionLayout3 = MotionLayout.this;
                    androidx.constraintlayout.core.widgets.d dVar2 = this.a;
                    int i3 = aVar2.c;
                    int i4 = i3 == 0 ? i : i2;
                    if (i3 == 0) {
                        i = i2;
                    }
                    motionLayout3.p(dVar2, optimizationLevel, i4, i);
                    return;
                }
                return;
            }
            androidx.constraintlayout.widget.a aVar3 = this.c;
            if (aVar3 != null) {
                MotionLayout motionLayout4 = MotionLayout.this;
                androidx.constraintlayout.core.widgets.d dVar3 = this.a;
                int i5 = aVar3.c;
                motionLayout4.p(dVar3, optimizationLevel, i5 == 0 ? i : i2, i5 == 0 ? i2 : i);
            }
            MotionLayout motionLayout5 = MotionLayout.this;
            androidx.constraintlayout.core.widgets.d dVar4 = this.b;
            androidx.constraintlayout.widget.a aVar4 = this.d;
            int i6 = (aVar4 == null || aVar4.c == 0) ? i : i2;
            if (aVar4 == null || aVar4.c == 0) {
                i = i2;
            }
            motionLayout5.p(dVar4, optimizationLevel, i6, i);
        }

        public void c(androidx.constraintlayout.core.widgets.d dVar, androidx.constraintlayout.core.widgets.d dVar2) {
            ConstraintWidget constraintWidget;
            ArrayList<ConstraintWidget> o1 = dVar.o1();
            HashMap<ConstraintWidget, ConstraintWidget> hashMap = new HashMap<>();
            hashMap.put(dVar, dVar2);
            dVar2.o1().clear();
            dVar2.n(dVar, hashMap);
            Iterator<ConstraintWidget> it = o1.iterator();
            while (it.hasNext()) {
                ConstraintWidget next = it.next();
                if (next instanceof androidx.constraintlayout.core.widgets.a) {
                    constraintWidget = new androidx.constraintlayout.core.widgets.a();
                } else if (next instanceof androidx.constraintlayout.core.widgets.f) {
                    constraintWidget = new androidx.constraintlayout.core.widgets.f();
                } else if (next instanceof androidx.constraintlayout.core.widgets.e) {
                    constraintWidget = new androidx.constraintlayout.core.widgets.e();
                } else if (next instanceof androidx.constraintlayout.core.widgets.h) {
                    constraintWidget = new androidx.constraintlayout.core.widgets.h();
                } else if (next instanceof lk1) {
                    constraintWidget = new mk1();
                } else {
                    constraintWidget = new ConstraintWidget();
                }
                dVar2.b(constraintWidget);
                hashMap.put(next, constraintWidget);
            }
            Iterator<ConstraintWidget> it2 = o1.iterator();
            while (it2.hasNext()) {
                ConstraintWidget next2 = it2.next();
                hashMap.get(next2).n(next2, hashMap);
            }
        }

        public ConstraintWidget d(androidx.constraintlayout.core.widgets.d dVar, View view) {
            if (dVar.u() == view) {
                return dVar;
            }
            ArrayList<ConstraintWidget> o1 = dVar.o1();
            int size = o1.size();
            for (int i = 0; i < size; i++) {
                ConstraintWidget constraintWidget = o1.get(i);
                if (constraintWidget.u() == view) {
                    return constraintWidget;
                }
            }
            return null;
        }

        public void e(androidx.constraintlayout.core.widgets.d dVar, androidx.constraintlayout.widget.a aVar, androidx.constraintlayout.widget.a aVar2) {
            this.c = aVar;
            this.d = aVar2;
            this.a = new androidx.constraintlayout.core.widgets.d();
            this.b = new androidx.constraintlayout.core.widgets.d();
            this.a.S1(MotionLayout.this.g0.F1());
            this.b.S1(MotionLayout.this.g0.F1());
            this.a.r1();
            this.b.r1();
            c(MotionLayout.this.g0, this.a);
            c(MotionLayout.this.g0, this.b);
            if (MotionLayout.this.M0 > 0.5d) {
                if (aVar != null) {
                    j(this.a, aVar);
                }
                j(this.b, aVar2);
            } else {
                j(this.b, aVar2);
                if (aVar != null) {
                    j(this.a, aVar);
                }
            }
            this.a.V1(MotionLayout.this.l());
            this.a.X1();
            this.b.V1(MotionLayout.this.l());
            this.b.X1();
            ViewGroup.LayoutParams layoutParams = MotionLayout.this.getLayoutParams();
            if (layoutParams != null) {
                if (layoutParams.width == -2) {
                    androidx.constraintlayout.core.widgets.d dVar2 = this.a;
                    ConstraintWidget.DimensionBehaviour dimensionBehaviour = ConstraintWidget.DimensionBehaviour.WRAP_CONTENT;
                    dVar2.M0(dimensionBehaviour);
                    this.b.M0(dimensionBehaviour);
                }
                if (layoutParams.height == -2) {
                    androidx.constraintlayout.core.widgets.d dVar3 = this.a;
                    ConstraintWidget.DimensionBehaviour dimensionBehaviour2 = ConstraintWidget.DimensionBehaviour.WRAP_CONTENT;
                    dVar3.d1(dimensionBehaviour2);
                    this.b.d1(dimensionBehaviour2);
                }
            }
        }

        public boolean f(int i, int i2) {
            return (i == this.e && i2 == this.f) ? false : true;
        }

        public void g(int i, int i2) {
            int mode = View.MeasureSpec.getMode(i);
            int mode2 = View.MeasureSpec.getMode(i2);
            MotionLayout motionLayout = MotionLayout.this;
            motionLayout.w1 = mode;
            motionLayout.x1 = mode2;
            motionLayout.getOptimizationLevel();
            b(i, i2);
            if (((MotionLayout.this.getParent() instanceof MotionLayout) && mode == 1073741824 && mode2 == 1073741824) ? false : true) {
                b(i, i2);
                MotionLayout.this.s1 = this.a.V();
                MotionLayout.this.t1 = this.a.z();
                MotionLayout.this.u1 = this.b.V();
                MotionLayout.this.v1 = this.b.z();
                MotionLayout motionLayout2 = MotionLayout.this;
                motionLayout2.r1 = (motionLayout2.s1 == motionLayout2.u1 && motionLayout2.t1 == motionLayout2.v1) ? false : true;
            }
            MotionLayout motionLayout3 = MotionLayout.this;
            int i3 = motionLayout3.s1;
            int i4 = motionLayout3.t1;
            int i5 = motionLayout3.w1;
            if (i5 == Integer.MIN_VALUE || i5 == 0) {
                i3 = (int) (i3 + (motionLayout3.y1 * (motionLayout3.u1 - i3)));
            }
            int i6 = i3;
            int i7 = motionLayout3.x1;
            if (i7 == Integer.MIN_VALUE || i7 == 0) {
                i4 = (int) (i4 + (motionLayout3.y1 * (motionLayout3.v1 - i4)));
            }
            MotionLayout.this.o(i, i2, i6, i4, this.a.N1() || this.b.N1(), this.a.L1() || this.b.L1());
        }

        public void h() {
            g(MotionLayout.this.F0, MotionLayout.this.G0);
            MotionLayout.this.q0();
        }

        public void i(int i, int i2) {
            this.e = i;
            this.f = i2;
        }

        public final void j(androidx.constraintlayout.core.widgets.d dVar, androidx.constraintlayout.widget.a aVar) {
            SparseArray<ConstraintWidget> sparseArray = new SparseArray<>();
            Constraints.LayoutParams layoutParams = new Constraints.LayoutParams(-2, -2);
            sparseArray.clear();
            sparseArray.put(0, dVar);
            sparseArray.put(MotionLayout.this.getId(), dVar);
            if (aVar != null && aVar.c != 0) {
                MotionLayout motionLayout = MotionLayout.this;
                motionLayout.p(this.b, motionLayout.getOptimizationLevel(), View.MeasureSpec.makeMeasureSpec(MotionLayout.this.getHeight(), 1073741824), View.MeasureSpec.makeMeasureSpec(MotionLayout.this.getWidth(), 1073741824));
            }
            Iterator<ConstraintWidget> it = dVar.o1().iterator();
            while (it.hasNext()) {
                ConstraintWidget next = it.next();
                sparseArray.put(((View) next.u()).getId(), next);
            }
            Iterator<ConstraintWidget> it2 = dVar.o1().iterator();
            while (it2.hasNext()) {
                ConstraintWidget next2 = it2.next();
                View view = (View) next2.u();
                aVar.l(view.getId(), layoutParams);
                next2.h1(aVar.C(view.getId()));
                next2.I0(aVar.x(view.getId()));
                if (view instanceof ConstraintHelper) {
                    aVar.j((ConstraintHelper) view, next2, layoutParams, sparseArray);
                    if (view instanceof Barrier) {
                        ((Barrier) view).w();
                    }
                }
                if (Build.VERSION.SDK_INT >= 17) {
                    layoutParams.resolveLayoutDirection(MotionLayout.this.getLayoutDirection());
                } else {
                    layoutParams.resolveLayoutDirection(0);
                }
                MotionLayout.this.d(false, view, next2, layoutParams, sparseArray);
                if (aVar.B(view.getId()) == 1) {
                    next2.g1(view.getVisibility());
                } else {
                    next2.g1(aVar.A(view.getId()));
                }
            }
            Iterator<ConstraintWidget> it3 = dVar.o1().iterator();
            while (it3.hasNext()) {
                ConstraintWidget next3 = it3.next();
                if (next3 instanceof androidx.constraintlayout.core.widgets.i) {
                    lk1 lk1Var = (lk1) next3;
                    ((ConstraintHelper) next3.u()).u(dVar, lk1Var, sparseArray);
                    ((androidx.constraintlayout.core.widgets.i) lk1Var).r1();
                }
            }
        }
    }

    /* loaded from: classes.dex */
    public interface g {
        void a(MotionEvent motionEvent);

        void b();

        float c();

        float d();

        void e(int i);
    }

    /* loaded from: classes.dex */
    public static class h implements g {
        public static h b = new h();
        public VelocityTracker a;

        public static h f() {
            b.a = VelocityTracker.obtain();
            return b;
        }

        @Override // androidx.constraintlayout.motion.widget.MotionLayout.g
        public void a(MotionEvent motionEvent) {
            VelocityTracker velocityTracker = this.a;
            if (velocityTracker != null) {
                velocityTracker.addMovement(motionEvent);
            }
        }

        @Override // androidx.constraintlayout.motion.widget.MotionLayout.g
        public void b() {
            VelocityTracker velocityTracker = this.a;
            if (velocityTracker != null) {
                velocityTracker.recycle();
                this.a = null;
            }
        }

        @Override // androidx.constraintlayout.motion.widget.MotionLayout.g
        public float c() {
            VelocityTracker velocityTracker = this.a;
            return velocityTracker != null ? velocityTracker.getYVelocity() : Utils.FLOAT_EPSILON;
        }

        @Override // androidx.constraintlayout.motion.widget.MotionLayout.g
        public float d() {
            VelocityTracker velocityTracker = this.a;
            return velocityTracker != null ? velocityTracker.getXVelocity() : Utils.FLOAT_EPSILON;
        }

        @Override // androidx.constraintlayout.motion.widget.MotionLayout.g
        public void e(int i) {
            VelocityTracker velocityTracker = this.a;
            if (velocityTracker != null) {
                velocityTracker.computeCurrentVelocity(i);
            }
        }
    }

    /* loaded from: classes.dex */
    public class i {
        public float a = Float.NaN;
        public float b = Float.NaN;
        public int c = -1;
        public int d = -1;

        public i() {
        }

        public void a() {
            int i = this.c;
            if (i != -1 || this.d != -1) {
                if (i == -1) {
                    MotionLayout.this.w0(this.d);
                } else {
                    int i2 = this.d;
                    if (i2 == -1) {
                        MotionLayout.this.setState(i, -1, -1);
                    } else {
                        MotionLayout.this.setTransition(i, i2);
                    }
                }
                MotionLayout.this.setState(TransitionState.SETUP);
            }
            if (Float.isNaN(this.b)) {
                if (Float.isNaN(this.a)) {
                    return;
                }
                MotionLayout.this.setProgress(this.a);
                return;
            }
            MotionLayout.this.setProgress(this.a, this.b);
            this.a = Float.NaN;
            this.b = Float.NaN;
            this.c = -1;
            this.d = -1;
        }

        public Bundle b() {
            Bundle bundle = new Bundle();
            bundle.putFloat("motion.progress", this.a);
            bundle.putFloat("motion.velocity", this.b);
            bundle.putInt("motion.StartState", this.c);
            bundle.putInt("motion.EndState", this.d);
            return bundle;
        }

        public void c() {
            this.d = MotionLayout.this.E0;
            this.c = MotionLayout.this.C0;
            this.b = MotionLayout.this.getVelocity();
            this.a = MotionLayout.this.getProgress();
        }

        public void d(int i) {
            this.d = i;
        }

        public void e(float f) {
            this.a = f;
        }

        public void f(int i) {
            this.c = i;
        }

        public void g(Bundle bundle) {
            this.a = bundle.getFloat("motion.progress");
            this.b = bundle.getFloat("motion.velocity");
            this.c = bundle.getInt("motion.StartState");
            this.d = bundle.getInt("motion.EndState");
        }

        public void h(float f) {
            this.b = f;
        }
    }

    /* loaded from: classes.dex */
    public interface j {
        void a(MotionLayout motionLayout, int i, int i2, float f);

        void b(MotionLayout motionLayout, int i, int i2);

        void c(MotionLayout motionLayout, int i, boolean z, float f);

        void d(MotionLayout motionLayout, int i);
    }

    public MotionLayout(Context context) {
        super(context);
        this.A0 = null;
        this.B0 = Utils.FLOAT_EPSILON;
        this.C0 = -1;
        this.D0 = -1;
        this.E0 = -1;
        this.F0 = 0;
        this.G0 = 0;
        this.H0 = true;
        this.I0 = new HashMap<>();
        this.J0 = 0L;
        this.K0 = 1.0f;
        this.L0 = Utils.FLOAT_EPSILON;
        this.M0 = Utils.FLOAT_EPSILON;
        this.O0 = Utils.FLOAT_EPSILON;
        this.Q0 = false;
        this.U0 = 0;
        this.W0 = false;
        this.X0 = new st3();
        this.Y0 = new d();
        this.c1 = false;
        this.h1 = false;
        this.i1 = null;
        this.j1 = null;
        this.k1 = null;
        this.l1 = null;
        this.m1 = 0;
        this.n1 = -1L;
        this.o1 = Utils.FLOAT_EPSILON;
        this.p1 = 0;
        this.q1 = Utils.FLOAT_EPSILON;
        this.r1 = false;
        this.z1 = new gx1();
        this.A1 = false;
        this.C1 = null;
        this.D1 = null;
        this.E1 = 0;
        this.F1 = false;
        this.G1 = 0;
        this.H1 = new HashMap<>();
        this.K1 = new Rect();
        this.L1 = false;
        this.M1 = TransitionState.UNDEFINED;
        this.N1 = new f();
        this.O1 = false;
        this.P1 = new RectF();
        this.Q1 = null;
        this.R1 = null;
        this.S1 = new ArrayList<>();
        k0(null);
    }

    public static boolean D0(float f2, float f3, float f4) {
        if (f2 > Utils.FLOAT_EPSILON) {
            float f5 = f2 / f4;
            return f3 + ((f2 * f5) - (((f4 * f5) * f5) / 2.0f)) > 1.0f;
        }
        float f6 = (-f2) / f4;
        return f3 + ((f2 * f6) + (((f4 * f6) * f6) / 2.0f)) < Utils.FLOAT_EPSILON;
    }

    public void A0() {
        this.N1.e(this.g0, this.y0.l(this.C0), this.y0.l(this.E0));
        p0();
    }

    public void B0(int i2, androidx.constraintlayout.widget.a aVar) {
        androidx.constraintlayout.motion.widget.g gVar = this.y0;
        if (gVar != null) {
            gVar.U(i2, aVar);
        }
        A0();
        if (this.D0 == i2) {
            aVar.i(this);
        }
    }

    public void C0(int i2, View... viewArr) {
        androidx.constraintlayout.motion.widget.g gVar = this.y0;
        if (gVar != null) {
            gVar.c0(i2, viewArr);
        }
    }

    public void R(float f2) {
        androidx.constraintlayout.motion.widget.g gVar = this.y0;
        if (gVar == null) {
            return;
        }
        float f3 = this.M0;
        float f4 = this.L0;
        if (f3 != f4 && this.P0) {
            this.M0 = f4;
        }
        float f5 = this.M0;
        if (f5 == f2) {
            return;
        }
        this.W0 = false;
        this.O0 = f2;
        this.K0 = gVar.p() / 1000.0f;
        setProgress(this.O0);
        this.z0 = null;
        this.A0 = this.y0.s();
        this.P0 = false;
        this.J0 = getNanoTime();
        this.Q0 = true;
        this.L0 = f5;
        this.M0 = f5;
        invalidate();
    }

    public boolean S(int i2, u92 u92Var) {
        androidx.constraintlayout.motion.widget.g gVar = this.y0;
        if (gVar != null) {
            return gVar.g(i2, u92Var);
        }
        return false;
    }

    public final boolean T(View view, MotionEvent motionEvent, float f2, float f3) {
        Matrix matrix = view.getMatrix();
        if (matrix.isIdentity()) {
            motionEvent.offsetLocation(f2, f3);
            boolean onTouchEvent = view.onTouchEvent(motionEvent);
            motionEvent.offsetLocation(-f2, -f3);
            return onTouchEvent;
        }
        MotionEvent obtain = MotionEvent.obtain(motionEvent);
        obtain.offsetLocation(f2, f3);
        if (this.R1 == null) {
            this.R1 = new Matrix();
        }
        matrix.invert(this.R1);
        obtain.transform(this.R1);
        boolean onTouchEvent2 = view.onTouchEvent(obtain);
        obtain.recycle();
        return onTouchEvent2;
    }

    public final void U() {
        androidx.constraintlayout.motion.widget.g gVar = this.y0;
        if (gVar == null) {
            return;
        }
        int F = gVar.F();
        androidx.constraintlayout.motion.widget.g gVar2 = this.y0;
        V(F, gVar2.l(gVar2.F()));
        SparseIntArray sparseIntArray = new SparseIntArray();
        SparseIntArray sparseIntArray2 = new SparseIntArray();
        Iterator<g.b> it = this.y0.o().iterator();
        while (it.hasNext()) {
            g.b next = it.next();
            g.b bVar = this.y0.c;
            W(next);
            int A = next.A();
            int y = next.y();
            String c2 = xe0.c(getContext(), A);
            String c3 = xe0.c(getContext(), y);
            if (sparseIntArray.get(A) == y) {
                StringBuilder sb = new StringBuilder();
                sb.append("CHECK: two transitions with the same start and end ");
                sb.append(c2);
                sb.append("->");
                sb.append(c3);
            }
            if (sparseIntArray2.get(y) == A) {
                StringBuilder sb2 = new StringBuilder();
                sb2.append("CHECK: you can't have reverse transitions");
                sb2.append(c2);
                sb2.append("->");
                sb2.append(c3);
            }
            sparseIntArray.put(A, y);
            sparseIntArray2.put(y, A);
            if (this.y0.l(A) == null) {
                StringBuilder sb3 = new StringBuilder();
                sb3.append(" no such constraintSetStart ");
                sb3.append(c2);
            }
            if (this.y0.l(y) == null) {
                StringBuilder sb4 = new StringBuilder();
                sb4.append(" no such constraintSetEnd ");
                sb4.append(c2);
            }
        }
    }

    public final void V(int i2, androidx.constraintlayout.widget.a aVar) {
        String c2 = xe0.c(getContext(), i2);
        int childCount = getChildCount();
        for (int i3 = 0; i3 < childCount; i3++) {
            View childAt = getChildAt(i3);
            int id = childAt.getId();
            if (id == -1) {
                StringBuilder sb = new StringBuilder();
                sb.append("CHECK: ");
                sb.append(c2);
                sb.append(" ALL VIEWS SHOULD HAVE ID's ");
                sb.append(childAt.getClass().getName());
                sb.append(" does not!");
            }
            if (aVar.w(id) == null) {
                StringBuilder sb2 = new StringBuilder();
                sb2.append("CHECK: ");
                sb2.append(c2);
                sb2.append(" NO CONSTRAINTS for ");
                sb2.append(xe0.d(childAt));
            }
        }
        int[] y = aVar.y();
        for (int i4 = 0; i4 < y.length; i4++) {
            int i5 = y[i4];
            String c3 = xe0.c(getContext(), i5);
            if (findViewById(y[i4]) == null) {
                StringBuilder sb3 = new StringBuilder();
                sb3.append("CHECK: ");
                sb3.append(c2);
                sb3.append(" NO View matches id ");
                sb3.append(c3);
            }
            if (aVar.x(i5) == -1) {
                StringBuilder sb4 = new StringBuilder();
                sb4.append("CHECK: ");
                sb4.append(c2);
                sb4.append("(");
                sb4.append(c3);
                sb4.append(") no LAYOUT_HEIGHT");
            }
            if (aVar.C(i5) == -1) {
                StringBuilder sb5 = new StringBuilder();
                sb5.append("CHECK: ");
                sb5.append(c2);
                sb5.append("(");
                sb5.append(c3);
                sb5.append(") no LAYOUT_HEIGHT");
            }
        }
    }

    public final void W(g.b bVar) {
        bVar.A();
        bVar.y();
    }

    public final void X() {
        int childCount = getChildCount();
        for (int i2 = 0; i2 < childCount; i2++) {
            View childAt = getChildAt(i2);
            u92 u92Var = this.I0.get(childAt);
            if (u92Var != null) {
                u92Var.D(childAt);
            }
        }
    }

    public void Y(boolean z) {
        int childCount = getChildCount();
        for (int i2 = 0; i2 < childCount; i2++) {
            u92 u92Var = this.I0.get(getChildAt(i2));
            if (u92Var != null) {
                u92Var.f(z);
            }
        }
    }

    /* JADX WARN: Removed duplicated region for block: B:101:0x0197  */
    /* JADX WARN: Removed duplicated region for block: B:111:0x01ae  */
    /* JADX WARN: Removed duplicated region for block: B:117:0x01bd  */
    /* JADX WARN: Removed duplicated region for block: B:120:0x01ca  */
    /* JADX WARN: Removed duplicated region for block: B:127:0x01eb  */
    /* JADX WARN: Removed duplicated region for block: B:132:0x0206  */
    /* JADX WARN: Removed duplicated region for block: B:146:0x0228  */
    /* JADX WARN: Removed duplicated region for block: B:72:0x0110  */
    /* JADX WARN: Removed duplicated region for block: B:74:0x0117  */
    /* JADX WARN: Removed duplicated region for block: B:89:0x014e  */
    /* JADX WARN: Removed duplicated region for block: B:90:0x0150  */
    /* JADX WARN: Removed duplicated region for block: B:93:0x0158  */
    /* JADX WARN: Removed duplicated region for block: B:96:0x016f  */
    /*
        Code decompiled incorrectly, please refer to instructions dump.
        To view partially-correct code enable 'Show inconsistent code' option in preferences
    */
    public void Z(boolean r24) {
        /*
            Method dump skipped, instructions count: 629
            To view this dump change 'Code comments level' option to 'DEBUG'
        */
        throw new UnsupportedOperationException("Method not decompiled: androidx.constraintlayout.motion.widget.MotionLayout.Z(boolean):void");
    }

    public final void a0() {
        boolean z;
        float signum = Math.signum(this.O0 - this.M0);
        long nanoTime = getNanoTime();
        Interpolator interpolator = this.z0;
        float f2 = this.M0 + (!(interpolator instanceof st3) ? ((((float) (nanoTime - this.N0)) * signum) * 1.0E-9f) / this.K0 : 0.0f);
        if (this.P0) {
            f2 = this.O0;
        }
        int i2 = (signum > Utils.FLOAT_EPSILON ? 1 : (signum == Utils.FLOAT_EPSILON ? 0 : -1));
        if ((i2 <= 0 || f2 < this.O0) && (signum > Utils.FLOAT_EPSILON || f2 > this.O0)) {
            z = false;
        } else {
            f2 = this.O0;
            z = true;
        }
        if (interpolator != null && !z) {
            if (this.W0) {
                f2 = interpolator.getInterpolation(((float) (nanoTime - this.J0)) * 1.0E-9f);
            } else {
                f2 = interpolator.getInterpolation(f2);
            }
        }
        if ((i2 > 0 && f2 >= this.O0) || (signum <= Utils.FLOAT_EPSILON && f2 <= this.O0)) {
            f2 = this.O0;
        }
        this.y1 = f2;
        int childCount = getChildCount();
        long nanoTime2 = getNanoTime();
        Interpolator interpolator2 = this.A0;
        if (interpolator2 != null) {
            f2 = interpolator2.getInterpolation(f2);
        }
        for (int i3 = 0; i3 < childCount; i3++) {
            View childAt = getChildAt(i3);
            u92 u92Var = this.I0.get(childAt);
            if (u92Var != null) {
                u92Var.x(childAt, f2, nanoTime2, this.z1);
            }
        }
        if (this.r1) {
            requestLayout();
        }
    }

    public final void b0() {
        CopyOnWriteArrayList<j> copyOnWriteArrayList;
        if ((this.R0 == null && ((copyOnWriteArrayList = this.l1) == null || copyOnWriteArrayList.isEmpty())) || this.q1 == this.L0) {
            return;
        }
        if (this.p1 != -1) {
            j jVar = this.R0;
            if (jVar != null) {
                jVar.b(this, this.C0, this.E0);
            }
            CopyOnWriteArrayList<j> copyOnWriteArrayList2 = this.l1;
            if (copyOnWriteArrayList2 != null) {
                Iterator<j> it = copyOnWriteArrayList2.iterator();
                while (it.hasNext()) {
                    it.next().b(this, this.C0, this.E0);
                }
            }
        }
        this.p1 = -1;
        float f2 = this.L0;
        this.q1 = f2;
        j jVar2 = this.R0;
        if (jVar2 != null) {
            jVar2.a(this, this.C0, this.E0, f2);
        }
        CopyOnWriteArrayList<j> copyOnWriteArrayList3 = this.l1;
        if (copyOnWriteArrayList3 != null) {
            Iterator<j> it2 = copyOnWriteArrayList3.iterator();
            while (it2.hasNext()) {
                it2.next().a(this, this.C0, this.E0, this.L0);
            }
        }
    }

    public void c0() {
        int i2;
        CopyOnWriteArrayList<j> copyOnWriteArrayList;
        if ((this.R0 != null || ((copyOnWriteArrayList = this.l1) != null && !copyOnWriteArrayList.isEmpty())) && this.p1 == -1) {
            this.p1 = this.D0;
            if (this.S1.isEmpty()) {
                i2 = -1;
            } else {
                ArrayList<Integer> arrayList = this.S1;
                i2 = arrayList.get(arrayList.size() - 1).intValue();
            }
            int i3 = this.D0;
            if (i2 != i3 && i3 != -1) {
                this.S1.add(Integer.valueOf(i3));
            }
        }
        o0();
        Runnable runnable = this.C1;
        if (runnable != null) {
            runnable.run();
        }
        int[] iArr = this.D1;
        if (iArr == null || this.E1 <= 0) {
            return;
        }
        w0(iArr[0]);
        int[] iArr2 = this.D1;
        System.arraycopy(iArr2, 1, iArr2, 0, iArr2.length - 1);
        this.E1--;
    }

    public void d0(int i2, boolean z, float f2) {
        j jVar = this.R0;
        if (jVar != null) {
            jVar.c(this, i2, z, f2);
        }
        CopyOnWriteArrayList<j> copyOnWriteArrayList = this.l1;
        if (copyOnWriteArrayList != null) {
            Iterator<j> it = copyOnWriteArrayList.iterator();
            while (it.hasNext()) {
                it.next().c(this, i2, z, f2);
            }
        }
    }

    @Override // androidx.constraintlayout.widget.ConstraintLayout, android.view.ViewGroup, android.view.View
    public void dispatchDraw(Canvas canvas) {
        long j2;
        androidx.constraintlayout.motion.widget.j jVar;
        ArrayList<MotionHelper> arrayList = this.k1;
        if (arrayList != null) {
            Iterator<MotionHelper> it = arrayList.iterator();
            while (it.hasNext()) {
                it.next().C(canvas);
            }
        }
        Z(false);
        androidx.constraintlayout.motion.widget.g gVar = this.y0;
        if (gVar != null && (jVar = gVar.s) != null) {
            jVar.c();
        }
        super.dispatchDraw(canvas);
        if (this.y0 == null) {
            return;
        }
        if ((this.U0 & 1) == 1 && !isInEditMode()) {
            this.m1++;
            long nanoTime = getNanoTime();
            long j3 = this.n1;
            if (j3 != -1) {
                if (nanoTime - j3 > 200000000) {
                    this.o1 = ((int) ((this.m1 / (((float) j2) * 1.0E-9f)) * 100.0f)) / 100.0f;
                    this.m1 = 0;
                    this.n1 = nanoTime;
                }
            } else {
                this.n1 = nanoTime;
            }
            Paint paint = new Paint();
            paint.setTextSize(42.0f);
            StringBuilder sb = new StringBuilder();
            sb.append(this.o1 + " fps " + xe0.e(this, this.C0) + " -> ");
            sb.append(xe0.e(this, this.E0));
            sb.append(" (progress: ");
            sb.append(((int) (getProgress() * 1000.0f)) / 10.0f);
            sb.append(" ) state=");
            int i2 = this.D0;
            sb.append(i2 == -1 ? "undefined" : xe0.e(this, i2));
            String sb2 = sb.toString();
            paint.setColor(-16777216);
            canvas.drawText(sb2, 11.0f, getHeight() - 29, paint);
            paint.setColor(-7864184);
            canvas.drawText(sb2, 10.0f, getHeight() - 30, paint);
        }
        if (this.U0 > 1) {
            if (this.V0 == null) {
                this.V0 = new e();
            }
            this.V0.a(canvas, this.I0, this.y0.p(), this.U0);
        }
        ArrayList<MotionHelper> arrayList2 = this.k1;
        if (arrayList2 != null) {
            Iterator<MotionHelper> it2 = arrayList2.iterator();
            while (it2.hasNext()) {
                it2.next().B(canvas);
            }
        }
    }

    public void e0(int i2, float f2, float f3, float f4, float[] fArr) {
        String resourceName;
        HashMap<View, u92> hashMap = this.I0;
        View i3 = i(i2);
        u92 u92Var = hashMap.get(i3);
        if (u92Var != null) {
            u92Var.l(f2, f3, f4, fArr);
            float y = i3.getY();
            this.S0 = f2;
            this.T0 = y;
            return;
        }
        if (i3 == null) {
            resourceName = "" + i2;
        } else {
            resourceName = i3.getContext().getResources().getResourceName(i2);
        }
        StringBuilder sb = new StringBuilder();
        sb.append("WARNING could not find view id ");
        sb.append(resourceName);
    }

    public androidx.constraintlayout.widget.a f0(int i2) {
        androidx.constraintlayout.motion.widget.g gVar = this.y0;
        if (gVar == null) {
            return null;
        }
        return gVar.l(i2);
    }

    public u92 g0(int i2) {
        return this.I0.get(findViewById(i2));
    }

    public int[] getConstraintSetIds() {
        androidx.constraintlayout.motion.widget.g gVar = this.y0;
        if (gVar == null) {
            return null;
        }
        return gVar.n();
    }

    public int getCurrentState() {
        return this.D0;
    }

    public ArrayList<g.b> getDefinedTransitions() {
        androidx.constraintlayout.motion.widget.g gVar = this.y0;
        if (gVar == null) {
            return null;
        }
        return gVar.o();
    }

    public pm0 getDesignTool() {
        if (this.Z0 == null) {
            this.Z0 = new pm0(this);
        }
        return this.Z0;
    }

    public int getEndState() {
        return this.E0;
    }

    public long getNanoTime() {
        return System.nanoTime();
    }

    public float getProgress() {
        return this.M0;
    }

    public androidx.constraintlayout.motion.widget.g getScene() {
        return this.y0;
    }

    public int getStartState() {
        return this.C0;
    }

    public float getTargetPosition() {
        return this.O0;
    }

    public Bundle getTransitionState() {
        if (this.B1 == null) {
            this.B1 = new i();
        }
        this.B1.c();
        return this.B1.b();
    }

    public long getTransitionTimeMs() {
        androidx.constraintlayout.motion.widget.g gVar = this.y0;
        if (gVar != null) {
            this.K0 = gVar.p() / 1000.0f;
        }
        return this.K0 * 1000.0f;
    }

    public float getVelocity() {
        return this.B0;
    }

    public g.b h0(int i2) {
        return this.y0.G(i2);
    }

    public void i0(View view, float f2, float f3, float[] fArr, int i2) {
        float f4;
        float f5 = this.B0;
        float f6 = this.M0;
        if (this.z0 != null) {
            float signum = Math.signum(this.O0 - f6);
            float interpolation = this.z0.getInterpolation(this.M0 + 1.0E-5f);
            float interpolation2 = this.z0.getInterpolation(this.M0);
            f5 = (signum * ((interpolation - interpolation2) / 1.0E-5f)) / this.K0;
            f4 = interpolation2;
        } else {
            f4 = f6;
        }
        Interpolator interpolator = this.z0;
        if (interpolator instanceof w92) {
            f5 = ((w92) interpolator).a();
        }
        u92 u92Var = this.I0.get(view);
        if ((i2 & 1) == 0) {
            u92Var.r(f4, view.getWidth(), view.getHeight(), f2, f3, fArr);
        } else {
            u92Var.l(f4, f2, f3, fArr);
        }
        if (i2 < 2) {
            fArr[0] = fArr[0] * f5;
            fArr[1] = fArr[1] * f5;
        }
    }

    @Override // android.view.View
    public boolean isAttachedToWindow() {
        if (Build.VERSION.SDK_INT >= 19) {
            return super.isAttachedToWindow();
        }
        return getWindowToken() != null;
    }

    public final boolean j0(float f2, float f3, View view, MotionEvent motionEvent) {
        boolean z;
        View childAt;
        if (view instanceof ViewGroup) {
            ViewGroup viewGroup = (ViewGroup) view;
            for (int childCount = viewGroup.getChildCount() - 1; childCount >= 0; childCount--) {
                if (j0((childAt.getLeft() + f2) - view.getScrollX(), (childAt.getTop() + f3) - view.getScrollY(), viewGroup.getChildAt(childCount), motionEvent)) {
                    z = true;
                    break;
                }
            }
        }
        z = false;
        if (!z) {
            this.P1.set(f2, f3, (view.getRight() + f2) - view.getLeft(), (view.getBottom() + f3) - view.getTop());
            if ((motionEvent.getAction() != 0 || this.P1.contains(motionEvent.getX(), motionEvent.getY())) && T(view, motionEvent, -f2, -f3)) {
                return true;
            }
        }
        return z;
    }

    public final void k0(AttributeSet attributeSet) {
        androidx.constraintlayout.motion.widget.g gVar;
        T1 = isInEditMode();
        if (attributeSet != null) {
            TypedArray obtainStyledAttributes = getContext().obtainStyledAttributes(attributeSet, w23.MotionLayout);
            int indexCount = obtainStyledAttributes.getIndexCount();
            boolean z = true;
            for (int i2 = 0; i2 < indexCount; i2++) {
                int index = obtainStyledAttributes.getIndex(i2);
                if (index == w23.MotionLayout_layoutDescription) {
                    this.y0 = new androidx.constraintlayout.motion.widget.g(getContext(), this, obtainStyledAttributes.getResourceId(index, -1));
                } else if (index == w23.MotionLayout_currentState) {
                    this.D0 = obtainStyledAttributes.getResourceId(index, -1);
                } else if (index == w23.MotionLayout_motionProgress) {
                    this.O0 = obtainStyledAttributes.getFloat(index, Utils.FLOAT_EPSILON);
                    this.Q0 = true;
                } else if (index == w23.MotionLayout_applyMotionScene) {
                    z = obtainStyledAttributes.getBoolean(index, z);
                } else if (index == w23.MotionLayout_showPaths) {
                    if (this.U0 == 0) {
                        this.U0 = obtainStyledAttributes.getBoolean(index, false) ? 2 : 0;
                    }
                } else if (index == w23.MotionLayout_motionDebug) {
                    this.U0 = obtainStyledAttributes.getInt(index, 0);
                }
            }
            obtainStyledAttributes.recycle();
            if (!z) {
                this.y0 = null;
            }
        }
        if (this.U0 != 0) {
            U();
        }
        if (this.D0 != -1 || (gVar = this.y0) == null) {
            return;
        }
        this.D0 = gVar.F();
        this.C0 = this.y0.F();
        this.E0 = this.y0.q();
    }

    public boolean l0() {
        return this.H0;
    }

    public g m0() {
        return h.f();
    }

    @Override // androidx.constraintlayout.widget.ConstraintLayout
    public void n(int i2) {
        this.o0 = null;
    }

    public void n0() {
        androidx.constraintlayout.motion.widget.g gVar = this.y0;
        if (gVar == null) {
            return;
        }
        if (gVar.h(this, this.D0)) {
            requestLayout();
            return;
        }
        int i2 = this.D0;
        if (i2 != -1) {
            this.y0.f(this, i2);
        }
        if (this.y0.b0()) {
            this.y0.Z();
        }
    }

    public final void o0() {
        CopyOnWriteArrayList<j> copyOnWriteArrayList;
        if (this.R0 == null && ((copyOnWriteArrayList = this.l1) == null || copyOnWriteArrayList.isEmpty())) {
            return;
        }
        Iterator<Integer> it = this.S1.iterator();
        while (it.hasNext()) {
            Integer next = it.next();
            j jVar = this.R0;
            if (jVar != null) {
                jVar.d(this, next.intValue());
            }
            CopyOnWriteArrayList<j> copyOnWriteArrayList2 = this.l1;
            if (copyOnWriteArrayList2 != null) {
                Iterator<j> it2 = copyOnWriteArrayList2.iterator();
                while (it2.hasNext()) {
                    it2.next().d(this, next.intValue());
                }
            }
        }
        this.S1.clear();
    }

    @Override // android.view.ViewGroup, android.view.View
    public void onAttachedToWindow() {
        g.b bVar;
        int i2;
        Display display;
        super.onAttachedToWindow();
        if (Build.VERSION.SDK_INT >= 17 && (display = getDisplay()) != null) {
            display.getRotation();
        }
        androidx.constraintlayout.motion.widget.g gVar = this.y0;
        if (gVar != null && (i2 = this.D0) != -1) {
            androidx.constraintlayout.widget.a l = gVar.l(i2);
            this.y0.T(this);
            ArrayList<MotionHelper> arrayList = this.k1;
            if (arrayList != null) {
                Iterator<MotionHelper> it = arrayList.iterator();
                while (it.hasNext()) {
                    it.next().A(this);
                }
            }
            if (l != null) {
                l.i(this);
            }
            this.C0 = this.D0;
        }
        n0();
        i iVar = this.B1;
        if (iVar != null) {
            if (this.L1) {
                post(new b());
                return;
            } else {
                iVar.a();
                return;
            }
        }
        androidx.constraintlayout.motion.widget.g gVar2 = this.y0;
        if (gVar2 == null || (bVar = gVar2.c) == null || bVar.x() != 4) {
            return;
        }
        t0();
        setState(TransitionState.SETUP);
        setState(TransitionState.MOVING);
    }

    @Override // android.view.ViewGroup
    public boolean onInterceptTouchEvent(MotionEvent motionEvent) {
        androidx.constraintlayout.motion.widget.h B;
        int q;
        RectF p;
        androidx.constraintlayout.motion.widget.g gVar = this.y0;
        if (gVar != null && this.H0) {
            androidx.constraintlayout.motion.widget.j jVar = gVar.s;
            if (jVar != null) {
                jVar.h(motionEvent);
            }
            g.b bVar = this.y0.c;
            if (bVar != null && bVar.C() && (B = bVar.B()) != null && ((motionEvent.getAction() != 0 || (p = B.p(this, new RectF())) == null || p.contains(motionEvent.getX(), motionEvent.getY())) && (q = B.q()) != -1)) {
                View view = this.Q1;
                if (view == null || view.getId() != q) {
                    this.Q1 = findViewById(q);
                }
                View view2 = this.Q1;
                if (view2 != null) {
                    this.P1.set(view2.getLeft(), this.Q1.getTop(), this.Q1.getRight(), this.Q1.getBottom());
                    if (this.P1.contains(motionEvent.getX(), motionEvent.getY()) && !j0(this.Q1.getLeft(), this.Q1.getTop(), this.Q1, motionEvent)) {
                        return onTouchEvent(motionEvent);
                    }
                }
            }
        }
        return false;
    }

    @Override // androidx.constraintlayout.widget.ConstraintLayout, android.view.ViewGroup, android.view.View
    public void onLayout(boolean z, int i2, int i3, int i4, int i5) {
        this.A1 = true;
        try {
            if (this.y0 == null) {
                super.onLayout(z, i2, i3, i4, i5);
                return;
            }
            int i6 = i4 - i2;
            int i7 = i5 - i3;
            if (this.a1 != i6 || this.b1 != i7) {
                p0();
                Z(true);
            }
            this.a1 = i6;
            this.b1 = i7;
        } finally {
            this.A1 = false;
        }
    }

    @Override // androidx.constraintlayout.widget.ConstraintLayout, android.view.View
    public void onMeasure(int i2, int i3) {
        int i4;
        int i5;
        if (this.y0 == null) {
            super.onMeasure(i2, i3);
            return;
        }
        boolean z = false;
        boolean z2 = (this.F0 == i2 && this.G0 == i3) ? false : true;
        if (this.O1) {
            this.O1 = false;
            n0();
            o0();
            z2 = true;
        }
        if (this.l0) {
            z2 = true;
        }
        this.F0 = i2;
        this.G0 = i3;
        int F = this.y0.F();
        int q = this.y0.q();
        if ((z2 || this.N1.f(F, q)) && this.C0 != -1) {
            super.onMeasure(i2, i3);
            this.N1.e(this.g0, this.y0.l(F), this.y0.l(q));
            this.N1.h();
            this.N1.i(F, q);
        } else {
            if (z2) {
                super.onMeasure(i2, i3);
            }
            z = true;
        }
        if (this.r1 || z) {
            int paddingTop = getPaddingTop() + getPaddingBottom();
            int V = this.g0.V() + getPaddingLeft() + getPaddingRight();
            int z3 = this.g0.z() + paddingTop;
            int i6 = this.w1;
            if (i6 == Integer.MIN_VALUE || i6 == 0) {
                V = (int) (this.s1 + (this.y1 * (this.u1 - i4)));
                requestLayout();
            }
            int i7 = this.x1;
            if (i7 == Integer.MIN_VALUE || i7 == 0) {
                z3 = (int) (this.t1 + (this.y1 * (this.v1 - i5)));
                requestLayout();
            }
            setMeasuredDimension(V, z3);
        }
        a0();
    }

    @Override // android.view.ViewGroup, android.view.ViewParent, defpackage.te2
    public boolean onNestedFling(View view, float f2, float f3, boolean z) {
        return false;
    }

    @Override // android.view.ViewGroup, android.view.ViewParent, defpackage.te2
    public boolean onNestedPreFling(View view, float f2, float f3) {
        return false;
    }

    @Override // defpackage.re2
    public void onNestedPreScroll(View view, int i2, int i3, int[] iArr, int i4) {
        g.b bVar;
        androidx.constraintlayout.motion.widget.h B;
        int q;
        androidx.constraintlayout.motion.widget.g gVar = this.y0;
        if (gVar == null || (bVar = gVar.c) == null || !bVar.C()) {
            return;
        }
        int i5 = -1;
        if (!bVar.C() || (B = bVar.B()) == null || (q = B.q()) == -1 || view.getId() == q) {
            if (gVar.w()) {
                androidx.constraintlayout.motion.widget.h B2 = bVar.B();
                if (B2 != null && (B2.e() & 4) != 0) {
                    i5 = i3;
                }
                float f2 = this.L0;
                if ((f2 == 1.0f || f2 == Utils.FLOAT_EPSILON) && view.canScrollVertically(i5)) {
                    return;
                }
            }
            if (bVar.B() != null && (bVar.B().e() & 1) != 0) {
                float x = gVar.x(i2, i3);
                float f3 = this.M0;
                if ((f3 <= Utils.FLOAT_EPSILON && x < Utils.FLOAT_EPSILON) || (f3 >= 1.0f && x > Utils.FLOAT_EPSILON)) {
                    if (Build.VERSION.SDK_INT >= 21) {
                        view.setNestedScrollingEnabled(false);
                        view.post(new a(this, view));
                        return;
                    }
                    return;
                }
            }
            float f4 = this.L0;
            long nanoTime = getNanoTime();
            float f5 = i2;
            this.d1 = f5;
            float f6 = i3;
            this.e1 = f6;
            this.g1 = (float) ((nanoTime - this.f1) * 1.0E-9d);
            this.f1 = nanoTime;
            gVar.P(f5, f6);
            if (f4 != this.L0) {
                iArr[0] = i2;
                iArr[1] = i3;
            }
            Z(false);
            if (iArr[0] == 0 && iArr[1] == 0) {
                return;
            }
            this.c1 = true;
        }
    }

    @Override // defpackage.re2
    public void onNestedScroll(View view, int i2, int i3, int i4, int i5, int i6) {
    }

    @Override // defpackage.se2
    public void onNestedScroll(View view, int i2, int i3, int i4, int i5, int i6, int[] iArr) {
        if (this.c1 || i2 != 0 || i3 != 0) {
            iArr[0] = iArr[0] + i4;
            iArr[1] = iArr[1] + i5;
        }
        this.c1 = false;
    }

    @Override // defpackage.re2
    public void onNestedScrollAccepted(View view, View view2, int i2, int i3) {
        this.f1 = getNanoTime();
        this.g1 = Utils.FLOAT_EPSILON;
        this.d1 = Utils.FLOAT_EPSILON;
        this.e1 = Utils.FLOAT_EPSILON;
    }

    @Override // android.view.View
    public void onRtlPropertiesChanged(int i2) {
        androidx.constraintlayout.motion.widget.g gVar = this.y0;
        if (gVar != null) {
            gVar.W(l());
        }
    }

    @Override // defpackage.re2
    public boolean onStartNestedScroll(View view, View view2, int i2, int i3) {
        g.b bVar;
        androidx.constraintlayout.motion.widget.g gVar = this.y0;
        return (gVar == null || (bVar = gVar.c) == null || bVar.B() == null || (this.y0.c.B().e() & 2) != 0) ? false : true;
    }

    @Override // defpackage.re2
    public void onStopNestedScroll(View view, int i2) {
        androidx.constraintlayout.motion.widget.g gVar = this.y0;
        if (gVar != null) {
            float f2 = this.g1;
            if (f2 == Utils.FLOAT_EPSILON) {
                return;
            }
            gVar.Q(this.d1 / f2, this.e1 / f2);
        }
    }

    @Override // android.view.View
    public boolean onTouchEvent(MotionEvent motionEvent) {
        androidx.constraintlayout.motion.widget.g gVar = this.y0;
        if (gVar != null && this.H0 && gVar.b0()) {
            g.b bVar = this.y0.c;
            if (bVar != null && !bVar.C()) {
                return super.onTouchEvent(motionEvent);
            }
            this.y0.R(motionEvent, getCurrentState(), this);
            return true;
        }
        return super.onTouchEvent(motionEvent);
    }

    @Override // androidx.constraintlayout.widget.ConstraintLayout, android.view.ViewGroup
    public void onViewAdded(View view) {
        super.onViewAdded(view);
        if (view instanceof MotionHelper) {
            MotionHelper motionHelper = (MotionHelper) view;
            if (this.l1 == null) {
                this.l1 = new CopyOnWriteArrayList<>();
            }
            this.l1.add(motionHelper);
            if (motionHelper.z()) {
                if (this.i1 == null) {
                    this.i1 = new ArrayList<>();
                }
                this.i1.add(motionHelper);
            }
            if (motionHelper.y()) {
                if (this.j1 == null) {
                    this.j1 = new ArrayList<>();
                }
                this.j1.add(motionHelper);
            }
            if (motionHelper.x()) {
                if (this.k1 == null) {
                    this.k1 = new ArrayList<>();
                }
                this.k1.add(motionHelper);
            }
        }
    }

    @Override // androidx.constraintlayout.widget.ConstraintLayout, android.view.ViewGroup
    public void onViewRemoved(View view) {
        super.onViewRemoved(view);
        ArrayList<MotionHelper> arrayList = this.i1;
        if (arrayList != null) {
            arrayList.remove(view);
        }
        ArrayList<MotionHelper> arrayList2 = this.j1;
        if (arrayList2 != null) {
            arrayList2.remove(view);
        }
    }

    public void p0() {
        this.N1.h();
        invalidate();
    }

    public final void q0() {
        int childCount = getChildCount();
        this.N1.a();
        boolean z = true;
        this.Q0 = true;
        SparseArray sparseArray = new SparseArray();
        int i2 = 0;
        for (int i3 = 0; i3 < childCount; i3++) {
            View childAt = getChildAt(i3);
            sparseArray.put(childAt.getId(), this.I0.get(childAt));
        }
        int width = getWidth();
        int height = getHeight();
        int j2 = this.y0.j();
        if (j2 != -1) {
            for (int i4 = 0; i4 < childCount; i4++) {
                u92 u92Var = this.I0.get(getChildAt(i4));
                if (u92Var != null) {
                    u92Var.C(j2);
                }
            }
        }
        SparseBooleanArray sparseBooleanArray = new SparseBooleanArray();
        int[] iArr = new int[this.I0.size()];
        int i5 = 0;
        for (int i6 = 0; i6 < childCount; i6++) {
            u92 u92Var2 = this.I0.get(getChildAt(i6));
            if (u92Var2.h() != -1) {
                sparseBooleanArray.put(u92Var2.h(), true);
                iArr[i5] = u92Var2.h();
                i5++;
            }
        }
        if (this.k1 != null) {
            for (int i7 = 0; i7 < i5; i7++) {
                u92 u92Var3 = this.I0.get(findViewById(iArr[i7]));
                if (u92Var3 != null) {
                    this.y0.t(u92Var3);
                }
            }
            Iterator<MotionHelper> it = this.k1.iterator();
            while (it.hasNext()) {
                it.next().D(this, this.I0);
            }
            for (int i8 = 0; i8 < i5; i8++) {
                u92 u92Var4 = this.I0.get(findViewById(iArr[i8]));
                if (u92Var4 != null) {
                    u92Var4.H(width, height, this.K0, getNanoTime());
                }
            }
        } else {
            for (int i9 = 0; i9 < i5; i9++) {
                u92 u92Var5 = this.I0.get(findViewById(iArr[i9]));
                if (u92Var5 != null) {
                    this.y0.t(u92Var5);
                    u92Var5.H(width, height, this.K0, getNanoTime());
                }
            }
        }
        for (int i10 = 0; i10 < childCount; i10++) {
            View childAt2 = getChildAt(i10);
            u92 u92Var6 = this.I0.get(childAt2);
            if (!sparseBooleanArray.get(childAt2.getId()) && u92Var6 != null) {
                this.y0.t(u92Var6);
                u92Var6.H(width, height, this.K0, getNanoTime());
            }
        }
        float E = this.y0.E();
        if (E != Utils.FLOAT_EPSILON) {
            boolean z2 = ((double) E) < Utils.DOUBLE_EPSILON;
            float abs = Math.abs(E);
            float f2 = -3.4028235E38f;
            float f3 = Float.MAX_VALUE;
            int i11 = 0;
            float f4 = -3.4028235E38f;
            float f5 = Float.MAX_VALUE;
            while (true) {
                if (i11 >= childCount) {
                    z = false;
                    break;
                }
                u92 u92Var7 = this.I0.get(getChildAt(i11));
                if (!Float.isNaN(u92Var7.k)) {
                    break;
                }
                float n = u92Var7.n();
                float o = u92Var7.o();
                float f6 = z2 ? o - n : o + n;
                f5 = Math.min(f5, f6);
                f4 = Math.max(f4, f6);
                i11++;
            }
            if (!z) {
                while (i2 < childCount) {
                    u92 u92Var8 = this.I0.get(getChildAt(i2));
                    float n2 = u92Var8.n();
                    float o2 = u92Var8.o();
                    float f7 = z2 ? o2 - n2 : o2 + n2;
                    u92Var8.m = 1.0f / (1.0f - abs);
                    u92Var8.l = abs - (((f7 - f5) * abs) / (f4 - f5));
                    i2++;
                }
                return;
            }
            for (int i12 = 0; i12 < childCount; i12++) {
                u92 u92Var9 = this.I0.get(getChildAt(i12));
                if (!Float.isNaN(u92Var9.k)) {
                    f3 = Math.min(f3, u92Var9.k);
                    f2 = Math.max(f2, u92Var9.k);
                }
            }
            while (i2 < childCount) {
                u92 u92Var10 = this.I0.get(getChildAt(i2));
                if (!Float.isNaN(u92Var10.k)) {
                    u92Var10.m = 1.0f / (1.0f - abs);
                    if (z2) {
                        u92Var10.l = abs - (((f2 - u92Var10.k) / (f2 - f3)) * abs);
                    } else {
                        u92Var10.l = abs - (((u92Var10.k - f3) * abs) / (f2 - f3));
                    }
                }
                i2++;
            }
        }
    }

    public final Rect r0(ConstraintWidget constraintWidget) {
        this.K1.top = constraintWidget.X();
        this.K1.left = constraintWidget.W();
        Rect rect = this.K1;
        int V = constraintWidget.V();
        Rect rect2 = this.K1;
        rect.right = V + rect2.left;
        int z = constraintWidget.z();
        Rect rect3 = this.K1;
        rect2.bottom = z + rect3.top;
        return rect3;
    }

    @Override // androidx.constraintlayout.widget.ConstraintLayout, android.view.View, android.view.ViewParent
    public void requestLayout() {
        androidx.constraintlayout.motion.widget.g gVar;
        g.b bVar;
        if (this.r1 || this.D0 != -1 || (gVar = this.y0) == null || (bVar = gVar.c) == null || bVar.z() != 0) {
            super.requestLayout();
        }
    }

    /* JADX WARN: Code restructure failed: missing block: B:17:0x0037, code lost:
        if (r10 != 7) goto L18;
     */
    /*
        Code decompiled incorrectly, please refer to instructions dump.
        To view partially-correct code enable 'Show inconsistent code' option in preferences
    */
    public void s0(int r10, float r11, float r12) {
        /*
            Method dump skipped, instructions count: 254
            To view this dump change 'Code comments level' option to 'DEBUG'
        */
        throw new UnsupportedOperationException("Method not decompiled: androidx.constraintlayout.motion.widget.MotionLayout.s0(int, float, float):void");
    }

    public void setDebugMode(int i2) {
        this.U0 = i2;
        invalidate();
    }

    public void setDelayedApplicationOfInitialState(boolean z) {
        this.L1 = z;
    }

    public void setInteractionEnabled(boolean z) {
        this.H0 = z;
    }

    public void setInterpolatedProgress(float f2) {
        if (this.y0 != null) {
            setState(TransitionState.MOVING);
            Interpolator s = this.y0.s();
            if (s != null) {
                setProgress(s.getInterpolation(f2));
                return;
            }
        }
        setProgress(f2);
    }

    public void setOnHide(float f2) {
        ArrayList<MotionHelper> arrayList = this.j1;
        if (arrayList != null) {
            int size = arrayList.size();
            for (int i2 = 0; i2 < size; i2++) {
                this.j1.get(i2).setProgress(f2);
            }
        }
    }

    public void setOnShow(float f2) {
        ArrayList<MotionHelper> arrayList = this.i1;
        if (arrayList != null) {
            int size = arrayList.size();
            for (int i2 = 0; i2 < size; i2++) {
                this.i1.get(i2).setProgress(f2);
            }
        }
    }

    public void setProgress(float f2, float f3) {
        if (!isAttachedToWindow()) {
            if (this.B1 == null) {
                this.B1 = new i();
            }
            this.B1.e(f2);
            this.B1.h(f3);
            return;
        }
        setProgress(f2);
        setState(TransitionState.MOVING);
        this.B0 = f3;
        R(1.0f);
    }

    public void setScene(androidx.constraintlayout.motion.widget.g gVar) {
        this.y0 = gVar;
        gVar.W(l());
        p0();
    }

    public void setStartState(int i2) {
        if (!isAttachedToWindow()) {
            if (this.B1 == null) {
                this.B1 = new i();
            }
            this.B1.f(i2);
            this.B1.d(i2);
            return;
        }
        this.D0 = i2;
    }

    public void setState(TransitionState transitionState) {
        TransitionState transitionState2 = TransitionState.FINISHED;
        if (transitionState == transitionState2 && this.D0 == -1) {
            return;
        }
        TransitionState transitionState3 = this.M1;
        this.M1 = transitionState;
        TransitionState transitionState4 = TransitionState.MOVING;
        if (transitionState3 == transitionState4 && transitionState == transitionState4) {
            b0();
        }
        int i2 = c.a[transitionState3.ordinal()];
        if (i2 != 1 && i2 != 2) {
            if (i2 == 3 && transitionState == transitionState2) {
                c0();
                return;
            }
            return;
        }
        if (transitionState == transitionState4) {
            b0();
        }
        if (transitionState == transitionState2) {
            c0();
        }
    }

    public void setTransition(int i2, int i3) {
        if (!isAttachedToWindow()) {
            if (this.B1 == null) {
                this.B1 = new i();
            }
            this.B1.f(i2);
            this.B1.d(i3);
            return;
        }
        androidx.constraintlayout.motion.widget.g gVar = this.y0;
        if (gVar != null) {
            this.C0 = i2;
            this.E0 = i3;
            gVar.X(i2, i3);
            this.N1.e(this.g0, this.y0.l(i2), this.y0.l(i3));
            p0();
            this.M0 = Utils.FLOAT_EPSILON;
            v0();
        }
    }

    public void setTransitionDuration(int i2) {
        androidx.constraintlayout.motion.widget.g gVar = this.y0;
        if (gVar == null) {
            return;
        }
        gVar.V(i2);
    }

    public void setTransitionListener(j jVar) {
        this.R0 = jVar;
    }

    public void setTransitionState(Bundle bundle) {
        if (this.B1 == null) {
            this.B1 = new i();
        }
        this.B1.g(bundle);
        if (isAttachedToWindow()) {
            this.B1.a();
        }
    }

    public void t0() {
        R(1.0f);
        this.C1 = null;
    }

    @Override // android.view.View
    public String toString() {
        Context context = getContext();
        return xe0.c(context, this.C0) + "->" + xe0.c(context, this.E0) + " (pos:" + this.M0 + " Dpos/Dt:" + this.B0;
    }

    public void u0(Runnable runnable) {
        R(1.0f);
        this.C1 = runnable;
    }

    public void v0() {
        R(Utils.FLOAT_EPSILON);
    }

    public void w0(int i2) {
        if (!isAttachedToWindow()) {
            if (this.B1 == null) {
                this.B1 = new i();
            }
            this.B1.d(i2);
            return;
        }
        y0(i2, -1, -1);
    }

    public void x0(int i2, int i3) {
        if (!isAttachedToWindow()) {
            if (this.B1 == null) {
                this.B1 = new i();
            }
            this.B1.d(i2);
            return;
        }
        z0(i2, -1, -1, i3);
    }

    public void y0(int i2, int i3, int i4) {
        z0(i2, i3, i4, -1);
    }

    public void z0(int i2, int i3, int i4, int i5) {
        ct3 ct3Var;
        int a2;
        androidx.constraintlayout.motion.widget.g gVar = this.y0;
        if (gVar != null && (ct3Var = gVar.b) != null && (a2 = ct3Var.a(this.D0, i2, i3, i4)) != -1) {
            i2 = a2;
        }
        int i6 = this.D0;
        if (i6 == i2) {
            return;
        }
        if (this.C0 == i2) {
            R(Utils.FLOAT_EPSILON);
            if (i5 > 0) {
                this.K0 = i5 / 1000.0f;
            }
        } else if (this.E0 == i2) {
            R(1.0f);
            if (i5 > 0) {
                this.K0 = i5 / 1000.0f;
            }
        } else {
            this.E0 = i2;
            if (i6 != -1) {
                setTransition(i6, i2);
                R(1.0f);
                this.M0 = Utils.FLOAT_EPSILON;
                t0();
                if (i5 > 0) {
                    this.K0 = i5 / 1000.0f;
                    return;
                }
                return;
            }
            this.W0 = false;
            this.O0 = 1.0f;
            this.L0 = Utils.FLOAT_EPSILON;
            this.M0 = Utils.FLOAT_EPSILON;
            this.N0 = getNanoTime();
            this.J0 = getNanoTime();
            this.P0 = false;
            this.z0 = null;
            if (i5 == -1) {
                this.K0 = this.y0.p() / 1000.0f;
            }
            this.C0 = -1;
            this.y0.X(-1, this.E0);
            SparseArray sparseArray = new SparseArray();
            if (i5 == 0) {
                this.K0 = this.y0.p() / 1000.0f;
            } else if (i5 > 0) {
                this.K0 = i5 / 1000.0f;
            }
            int childCount = getChildCount();
            this.I0.clear();
            for (int i7 = 0; i7 < childCount; i7++) {
                View childAt = getChildAt(i7);
                this.I0.put(childAt, new u92(childAt));
                sparseArray.put(childAt.getId(), this.I0.get(childAt));
            }
            this.Q0 = true;
            this.N1.e(this.g0, null, this.y0.l(i2));
            p0();
            this.N1.a();
            X();
            int width = getWidth();
            int height = getHeight();
            if (this.k1 != null) {
                for (int i8 = 0; i8 < childCount; i8++) {
                    u92 u92Var = this.I0.get(getChildAt(i8));
                    if (u92Var != null) {
                        this.y0.t(u92Var);
                    }
                }
                Iterator<MotionHelper> it = this.k1.iterator();
                while (it.hasNext()) {
                    it.next().D(this, this.I0);
                }
                for (int i9 = 0; i9 < childCount; i9++) {
                    u92 u92Var2 = this.I0.get(getChildAt(i9));
                    if (u92Var2 != null) {
                        u92Var2.H(width, height, this.K0, getNanoTime());
                    }
                }
            } else {
                for (int i10 = 0; i10 < childCount; i10++) {
                    u92 u92Var3 = this.I0.get(getChildAt(i10));
                    if (u92Var3 != null) {
                        this.y0.t(u92Var3);
                        u92Var3.H(width, height, this.K0, getNanoTime());
                    }
                }
            }
            float E = this.y0.E();
            if (E != Utils.FLOAT_EPSILON) {
                float f2 = Float.MAX_VALUE;
                float f3 = -3.4028235E38f;
                for (int i11 = 0; i11 < childCount; i11++) {
                    u92 u92Var4 = this.I0.get(getChildAt(i11));
                    float o = u92Var4.o() + u92Var4.n();
                    f2 = Math.min(f2, o);
                    f3 = Math.max(f3, o);
                }
                for (int i12 = 0; i12 < childCount; i12++) {
                    u92 u92Var5 = this.I0.get(getChildAt(i12));
                    float n = u92Var5.n();
                    float o2 = u92Var5.o();
                    u92Var5.m = 1.0f / (1.0f - E);
                    u92Var5.l = E - ((((n + o2) - f2) * E) / (f3 - f2));
                }
            }
            this.L0 = Utils.FLOAT_EPSILON;
            this.M0 = Utils.FLOAT_EPSILON;
            this.Q0 = true;
            invalidate();
        }
    }

    public void setProgress(float f2) {
        int i2 = (f2 > Utils.FLOAT_EPSILON ? 1 : (f2 == Utils.FLOAT_EPSILON ? 0 : -1));
        if (i2 >= 0) {
            int i3 = (f2 > 1.0f ? 1 : (f2 == 1.0f ? 0 : -1));
        }
        if (!isAttachedToWindow()) {
            if (this.B1 == null) {
                this.B1 = new i();
            }
            this.B1.e(f2);
            return;
        }
        if (i2 <= 0) {
            if (this.M0 == 1.0f && this.D0 == this.E0) {
                setState(TransitionState.MOVING);
            }
            this.D0 = this.C0;
            if (this.M0 == Utils.FLOAT_EPSILON) {
                setState(TransitionState.FINISHED);
            }
        } else if (f2 >= 1.0f) {
            if (this.M0 == Utils.FLOAT_EPSILON && this.D0 == this.C0) {
                setState(TransitionState.MOVING);
            }
            this.D0 = this.E0;
            if (this.M0 == 1.0f) {
                setState(TransitionState.FINISHED);
            }
        } else {
            this.D0 = -1;
            setState(TransitionState.MOVING);
        }
        if (this.y0 == null) {
            return;
        }
        this.P0 = true;
        this.O0 = f2;
        this.L0 = f2;
        this.N0 = -1L;
        this.J0 = -1L;
        this.z0 = null;
        this.Q0 = true;
        invalidate();
    }

    @Override // androidx.constraintlayout.widget.ConstraintLayout
    public void setState(int i2, int i3, int i4) {
        setState(TransitionState.SETUP);
        this.D0 = i2;
        this.C0 = -1;
        this.E0 = -1;
        e60 e60Var = this.o0;
        if (e60Var != null) {
            e60Var.d(i2, i3, i4);
            return;
        }
        androidx.constraintlayout.motion.widget.g gVar = this.y0;
        if (gVar != null) {
            gVar.l(i2).i(this);
        }
    }

    public void setTransition(int i2) {
        if (this.y0 != null) {
            g.b h0 = h0(i2);
            this.C0 = h0.A();
            this.E0 = h0.y();
            if (!isAttachedToWindow()) {
                if (this.B1 == null) {
                    this.B1 = new i();
                }
                this.B1.f(this.C0);
                this.B1.d(this.E0);
                return;
            }
            float f2 = Float.NaN;
            int i3 = this.D0;
            int i4 = this.C0;
            float f3 = Utils.FLOAT_EPSILON;
            if (i3 == i4) {
                f2 = 0.0f;
            } else if (i3 == this.E0) {
                f2 = 1.0f;
            }
            this.y0.Y(h0);
            this.N1.e(this.g0, this.y0.l(this.C0), this.y0.l(this.E0));
            p0();
            if (this.M0 != f2) {
                if (f2 == Utils.FLOAT_EPSILON) {
                    Y(true);
                    this.y0.l(this.C0).i(this);
                } else if (f2 == 1.0f) {
                    Y(false);
                    this.y0.l(this.E0).i(this);
                }
            }
            if (!Float.isNaN(f2)) {
                f3 = f2;
            }
            this.M0 = f3;
            if (Float.isNaN(f2)) {
                StringBuilder sb = new StringBuilder();
                sb.append(xe0.b());
                sb.append(" transitionToStart ");
                v0();
                return;
            }
            setProgress(f2);
        }
    }

    public void setTransition(g.b bVar) {
        this.y0.Y(bVar);
        setState(TransitionState.SETUP);
        if (this.D0 == this.y0.q()) {
            this.M0 = 1.0f;
            this.L0 = 1.0f;
            this.O0 = 1.0f;
        } else {
            this.M0 = Utils.FLOAT_EPSILON;
            this.L0 = Utils.FLOAT_EPSILON;
            this.O0 = Utils.FLOAT_EPSILON;
        }
        this.N0 = bVar.D(1) ? -1L : getNanoTime();
        int F = this.y0.F();
        int q = this.y0.q();
        if (F == this.C0 && q == this.E0) {
            return;
        }
        this.C0 = F;
        this.E0 = q;
        this.y0.X(F, q);
        this.N1.e(this.g0, this.y0.l(this.C0), this.y0.l(this.E0));
        this.N1.i(this.C0, this.E0);
        this.N1.h();
        p0();
    }

    public MotionLayout(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        this.A0 = null;
        this.B0 = Utils.FLOAT_EPSILON;
        this.C0 = -1;
        this.D0 = -1;
        this.E0 = -1;
        this.F0 = 0;
        this.G0 = 0;
        this.H0 = true;
        this.I0 = new HashMap<>();
        this.J0 = 0L;
        this.K0 = 1.0f;
        this.L0 = Utils.FLOAT_EPSILON;
        this.M0 = Utils.FLOAT_EPSILON;
        this.O0 = Utils.FLOAT_EPSILON;
        this.Q0 = false;
        this.U0 = 0;
        this.W0 = false;
        this.X0 = new st3();
        this.Y0 = new d();
        this.c1 = false;
        this.h1 = false;
        this.i1 = null;
        this.j1 = null;
        this.k1 = null;
        this.l1 = null;
        this.m1 = 0;
        this.n1 = -1L;
        this.o1 = Utils.FLOAT_EPSILON;
        this.p1 = 0;
        this.q1 = Utils.FLOAT_EPSILON;
        this.r1 = false;
        this.z1 = new gx1();
        this.A1 = false;
        this.C1 = null;
        this.D1 = null;
        this.E1 = 0;
        this.F1 = false;
        this.G1 = 0;
        this.H1 = new HashMap<>();
        this.K1 = new Rect();
        this.L1 = false;
        this.M1 = TransitionState.UNDEFINED;
        this.N1 = new f();
        this.O1 = false;
        this.P1 = new RectF();
        this.Q1 = null;
        this.R1 = null;
        this.S1 = new ArrayList<>();
        k0(attributeSet);
    }

    public MotionLayout(Context context, AttributeSet attributeSet, int i2) {
        super(context, attributeSet, i2);
        this.A0 = null;
        this.B0 = Utils.FLOAT_EPSILON;
        this.C0 = -1;
        this.D0 = -1;
        this.E0 = -1;
        this.F0 = 0;
        this.G0 = 0;
        this.H0 = true;
        this.I0 = new HashMap<>();
        this.J0 = 0L;
        this.K0 = 1.0f;
        this.L0 = Utils.FLOAT_EPSILON;
        this.M0 = Utils.FLOAT_EPSILON;
        this.O0 = Utils.FLOAT_EPSILON;
        this.Q0 = false;
        this.U0 = 0;
        this.W0 = false;
        this.X0 = new st3();
        this.Y0 = new d();
        this.c1 = false;
        this.h1 = false;
        this.i1 = null;
        this.j1 = null;
        this.k1 = null;
        this.l1 = null;
        this.m1 = 0;
        this.n1 = -1L;
        this.o1 = Utils.FLOAT_EPSILON;
        this.p1 = 0;
        this.q1 = Utils.FLOAT_EPSILON;
        this.r1 = false;
        this.z1 = new gx1();
        this.A1 = false;
        this.C1 = null;
        this.D1 = null;
        this.E1 = 0;
        this.F1 = false;
        this.G1 = 0;
        this.H1 = new HashMap<>();
        this.K1 = new Rect();
        this.L1 = false;
        this.M1 = TransitionState.UNDEFINED;
        this.N1 = new f();
        this.O1 = false;
        this.P1 = new RectF();
        this.Q1 = null;
        this.R1 = null;
        this.S1 = new ArrayList<>();
        k0(attributeSet);
    }
}
