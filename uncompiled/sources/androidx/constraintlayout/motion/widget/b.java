package androidx.constraintlayout.motion.widget;

import android.content.Context;
import android.content.res.TypedArray;
import android.os.Build;
import android.util.AttributeSet;
import android.util.SparseIntArray;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;

/* compiled from: KeyAttributes.java */
/* loaded from: classes.dex */
public class b extends androidx.constraintlayout.motion.widget.a {
    public String g;
    public int h = -1;
    public boolean i = false;
    public float j = Float.NaN;
    public float k = Float.NaN;
    public float l = Float.NaN;
    public float m = Float.NaN;
    public float n = Float.NaN;
    public float o = Float.NaN;
    public float p = Float.NaN;
    public float q = Float.NaN;
    public float r = Float.NaN;
    public float s = Float.NaN;
    public float t = Float.NaN;
    public float u = Float.NaN;
    public float v = Float.NaN;
    public float w = Float.NaN;

    /* compiled from: KeyAttributes.java */
    /* loaded from: classes.dex */
    public static class a {
        public static SparseIntArray a;

        static {
            SparseIntArray sparseIntArray = new SparseIntArray();
            a = sparseIntArray;
            sparseIntArray.append(w23.KeyAttribute_android_alpha, 1);
            a.append(w23.KeyAttribute_android_elevation, 2);
            a.append(w23.KeyAttribute_android_rotation, 4);
            a.append(w23.KeyAttribute_android_rotationX, 5);
            a.append(w23.KeyAttribute_android_rotationY, 6);
            a.append(w23.KeyAttribute_android_transformPivotX, 19);
            a.append(w23.KeyAttribute_android_transformPivotY, 20);
            a.append(w23.KeyAttribute_android_scaleX, 7);
            a.append(w23.KeyAttribute_transitionPathRotate, 8);
            a.append(w23.KeyAttribute_transitionEasing, 9);
            a.append(w23.KeyAttribute_motionTarget, 10);
            a.append(w23.KeyAttribute_framePosition, 12);
            a.append(w23.KeyAttribute_curveFit, 13);
            a.append(w23.KeyAttribute_android_scaleY, 14);
            a.append(w23.KeyAttribute_android_translationX, 15);
            a.append(w23.KeyAttribute_android_translationY, 16);
            a.append(w23.KeyAttribute_android_translationZ, 17);
            a.append(w23.KeyAttribute_motionProgress, 18);
        }

        public static void a(b bVar, TypedArray typedArray) {
            int indexCount = typedArray.getIndexCount();
            for (int i = 0; i < indexCount; i++) {
                int index = typedArray.getIndex(i);
                switch (a.get(index)) {
                    case 1:
                        bVar.j = typedArray.getFloat(index, bVar.j);
                        break;
                    case 2:
                        bVar.k = typedArray.getDimension(index, bVar.k);
                        break;
                    case 3:
                    case 11:
                    default:
                        StringBuilder sb = new StringBuilder();
                        sb.append("unused attribute 0x");
                        sb.append(Integer.toHexString(index));
                        sb.append("   ");
                        sb.append(a.get(index));
                        break;
                    case 4:
                        bVar.l = typedArray.getFloat(index, bVar.l);
                        break;
                    case 5:
                        bVar.m = typedArray.getFloat(index, bVar.m);
                        break;
                    case 6:
                        bVar.n = typedArray.getFloat(index, bVar.n);
                        break;
                    case 7:
                        bVar.r = typedArray.getFloat(index, bVar.r);
                        break;
                    case 8:
                        bVar.q = typedArray.getFloat(index, bVar.q);
                        break;
                    case 9:
                        bVar.g = typedArray.getString(index);
                        break;
                    case 10:
                        if (MotionLayout.T1) {
                            int resourceId = typedArray.getResourceId(index, bVar.b);
                            bVar.b = resourceId;
                            if (resourceId == -1) {
                                bVar.c = typedArray.getString(index);
                                break;
                            } else {
                                break;
                            }
                        } else if (typedArray.peekValue(index).type == 3) {
                            bVar.c = typedArray.getString(index);
                            break;
                        } else {
                            bVar.b = typedArray.getResourceId(index, bVar.b);
                            break;
                        }
                    case 12:
                        bVar.a = typedArray.getInt(index, bVar.a);
                        break;
                    case 13:
                        bVar.h = typedArray.getInteger(index, bVar.h);
                        break;
                    case 14:
                        bVar.s = typedArray.getFloat(index, bVar.s);
                        break;
                    case 15:
                        bVar.t = typedArray.getDimension(index, bVar.t);
                        break;
                    case 16:
                        bVar.u = typedArray.getDimension(index, bVar.u);
                        break;
                    case 17:
                        if (Build.VERSION.SDK_INT >= 21) {
                            bVar.v = typedArray.getDimension(index, bVar.v);
                            break;
                        } else {
                            break;
                        }
                    case 18:
                        bVar.w = typedArray.getFloat(index, bVar.w);
                        break;
                    case 19:
                        bVar.o = typedArray.getDimension(index, bVar.o);
                        break;
                    case 20:
                        bVar.p = typedArray.getDimension(index, bVar.p);
                        break;
                }
            }
        }
    }

    public b() {
        this.d = 1;
        this.e = new HashMap<>();
    }

    public void R(String str, Object obj) {
        str.hashCode();
        char c = 65535;
        switch (str.hashCode()) {
            case -1913008125:
                if (str.equals("motionProgress")) {
                    c = 0;
                    break;
                }
                break;
            case -1812823328:
                if (str.equals("transitionEasing")) {
                    c = 1;
                    break;
                }
                break;
            case -1249320806:
                if (str.equals("rotationX")) {
                    c = 2;
                    break;
                }
                break;
            case -1249320805:
                if (str.equals("rotationY")) {
                    c = 3;
                    break;
                }
                break;
            case -1225497657:
                if (str.equals("translationX")) {
                    c = 4;
                    break;
                }
                break;
            case -1225497656:
                if (str.equals("translationY")) {
                    c = 5;
                    break;
                }
                break;
            case -1225497655:
                if (str.equals("translationZ")) {
                    c = 6;
                    break;
                }
                break;
            case -908189618:
                if (str.equals("scaleX")) {
                    c = 7;
                    break;
                }
                break;
            case -908189617:
                if (str.equals("scaleY")) {
                    c = '\b';
                    break;
                }
                break;
            case -760884510:
                if (str.equals("transformPivotX")) {
                    c = '\t';
                    break;
                }
                break;
            case -760884509:
                if (str.equals("transformPivotY")) {
                    c = '\n';
                    break;
                }
                break;
            case -40300674:
                if (str.equals("rotation")) {
                    c = 11;
                    break;
                }
                break;
            case -4379043:
                if (str.equals("elevation")) {
                    c = '\f';
                    break;
                }
                break;
            case 37232917:
                if (str.equals("transitionPathRotate")) {
                    c = '\r';
                    break;
                }
                break;
            case 92909918:
                if (str.equals("alpha")) {
                    c = 14;
                    break;
                }
                break;
            case 579057826:
                if (str.equals("curveFit")) {
                    c = 15;
                    break;
                }
                break;
            case 1941332754:
                if (str.equals("visibility")) {
                    c = 16;
                    break;
                }
                break;
        }
        switch (c) {
            case 0:
                this.w = k(obj);
                return;
            case 1:
                obj.toString();
                return;
            case 2:
                this.m = k(obj);
                return;
            case 3:
                this.n = k(obj);
                return;
            case 4:
                this.t = k(obj);
                return;
            case 5:
                this.u = k(obj);
                return;
            case 6:
                this.v = k(obj);
                return;
            case 7:
                this.r = k(obj);
                return;
            case '\b':
                this.s = k(obj);
                return;
            case '\t':
                this.o = k(obj);
                return;
            case '\n':
                this.p = k(obj);
                return;
            case 11:
                this.l = k(obj);
                return;
            case '\f':
                this.k = k(obj);
                return;
            case '\r':
                this.q = k(obj);
                return;
            case 14:
                this.j = k(obj);
                return;
            case 15:
                this.h = l(obj);
                return;
            case 16:
                this.i = j(obj);
                return;
            default:
                return;
        }
    }

    /* JADX WARN: Can't fix incorrect switch cases order, some code will duplicate */
    /* JADX WARN: Code restructure failed: missing block: B:41:0x009a, code lost:
        if (r1.equals("scaleY") == false) goto L12;
     */
    @Override // androidx.constraintlayout.motion.widget.a
    /*
        Code decompiled incorrectly, please refer to instructions dump.
        To view partially-correct code enable 'Show inconsistent code' option in preferences
    */
    public void a(java.util.HashMap<java.lang.String, defpackage.ak4> r7) {
        /*
            Method dump skipped, instructions count: 572
            To view this dump change 'Code comments level' option to 'DEBUG'
        */
        throw new UnsupportedOperationException("Method not decompiled: androidx.constraintlayout.motion.widget.b.a(java.util.HashMap):void");
    }

    @Override // androidx.constraintlayout.motion.widget.a
    /* renamed from: b */
    public androidx.constraintlayout.motion.widget.a clone() {
        return new b().c(this);
    }

    @Override // androidx.constraintlayout.motion.widget.a
    public androidx.constraintlayout.motion.widget.a c(androidx.constraintlayout.motion.widget.a aVar) {
        super.c(aVar);
        b bVar = (b) aVar;
        this.h = bVar.h;
        this.i = bVar.i;
        this.j = bVar.j;
        this.k = bVar.k;
        this.l = bVar.l;
        this.m = bVar.m;
        this.n = bVar.n;
        this.o = bVar.o;
        this.p = bVar.p;
        this.q = bVar.q;
        this.r = bVar.r;
        this.s = bVar.s;
        this.t = bVar.t;
        this.u = bVar.u;
        this.v = bVar.v;
        this.w = bVar.w;
        return this;
    }

    @Override // androidx.constraintlayout.motion.widget.a
    public void d(HashSet<String> hashSet) {
        if (!Float.isNaN(this.j)) {
            hashSet.add("alpha");
        }
        if (!Float.isNaN(this.k)) {
            hashSet.add("elevation");
        }
        if (!Float.isNaN(this.l)) {
            hashSet.add("rotation");
        }
        if (!Float.isNaN(this.m)) {
            hashSet.add("rotationX");
        }
        if (!Float.isNaN(this.n)) {
            hashSet.add("rotationY");
        }
        if (!Float.isNaN(this.o)) {
            hashSet.add("transformPivotX");
        }
        if (!Float.isNaN(this.p)) {
            hashSet.add("transformPivotY");
        }
        if (!Float.isNaN(this.t)) {
            hashSet.add("translationX");
        }
        if (!Float.isNaN(this.u)) {
            hashSet.add("translationY");
        }
        if (!Float.isNaN(this.v)) {
            hashSet.add("translationZ");
        }
        if (!Float.isNaN(this.q)) {
            hashSet.add("transitionPathRotate");
        }
        if (!Float.isNaN(this.r)) {
            hashSet.add("scaleX");
        }
        if (!Float.isNaN(this.s)) {
            hashSet.add("scaleY");
        }
        if (!Float.isNaN(this.w)) {
            hashSet.add("progress");
        }
        if (this.e.size() > 0) {
            Iterator<String> it = this.e.keySet().iterator();
            while (it.hasNext()) {
                hashSet.add("CUSTOM," + it.next());
            }
        }
    }

    @Override // androidx.constraintlayout.motion.widget.a
    public void e(Context context, AttributeSet attributeSet) {
        a.a(this, context.obtainStyledAttributes(attributeSet, w23.KeyAttribute));
    }

    @Override // androidx.constraintlayout.motion.widget.a
    public void h(HashMap<String, Integer> hashMap) {
        if (this.h == -1) {
            return;
        }
        if (!Float.isNaN(this.j)) {
            hashMap.put("alpha", Integer.valueOf(this.h));
        }
        if (!Float.isNaN(this.k)) {
            hashMap.put("elevation", Integer.valueOf(this.h));
        }
        if (!Float.isNaN(this.l)) {
            hashMap.put("rotation", Integer.valueOf(this.h));
        }
        if (!Float.isNaN(this.m)) {
            hashMap.put("rotationX", Integer.valueOf(this.h));
        }
        if (!Float.isNaN(this.n)) {
            hashMap.put("rotationY", Integer.valueOf(this.h));
        }
        if (!Float.isNaN(this.o)) {
            hashMap.put("transformPivotX", Integer.valueOf(this.h));
        }
        if (!Float.isNaN(this.p)) {
            hashMap.put("transformPivotY", Integer.valueOf(this.h));
        }
        if (!Float.isNaN(this.t)) {
            hashMap.put("translationX", Integer.valueOf(this.h));
        }
        if (!Float.isNaN(this.u)) {
            hashMap.put("translationY", Integer.valueOf(this.h));
        }
        if (!Float.isNaN(this.v)) {
            hashMap.put("translationZ", Integer.valueOf(this.h));
        }
        if (!Float.isNaN(this.q)) {
            hashMap.put("transitionPathRotate", Integer.valueOf(this.h));
        }
        if (!Float.isNaN(this.r)) {
            hashMap.put("scaleX", Integer.valueOf(this.h));
        }
        if (!Float.isNaN(this.s)) {
            hashMap.put("scaleY", Integer.valueOf(this.h));
        }
        if (!Float.isNaN(this.w)) {
            hashMap.put("progress", Integer.valueOf(this.h));
        }
        if (this.e.size() > 0) {
            Iterator<String> it = this.e.keySet().iterator();
            while (it.hasNext()) {
                hashMap.put("CUSTOM," + it.next(), Integer.valueOf(this.h));
            }
        }
    }
}
