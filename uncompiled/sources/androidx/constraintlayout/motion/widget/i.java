package androidx.constraintlayout.motion.widget;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Rect;
import android.util.Xml;
import android.view.View;
import android.view.animation.AccelerateDecelerateInterpolator;
import android.view.animation.AccelerateInterpolator;
import android.view.animation.AnimationUtils;
import android.view.animation.AnticipateInterpolator;
import android.view.animation.BounceInterpolator;
import android.view.animation.DecelerateInterpolator;
import android.view.animation.Interpolator;
import android.view.animation.OvershootInterpolator;
import androidx.constraintlayout.motion.widget.g;
import androidx.constraintlayout.motion.widget.i;
import androidx.constraintlayout.widget.ConstraintAttribute;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.constraintlayout.widget.a;
import com.github.mikephil.charting.utils.Utils;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;
import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserException;

/* compiled from: ViewTransition.java */
/* loaded from: classes.dex */
public class i {
    public int a;
    public int e;
    public jx1 f;
    public a.C0021a g;
    public int j;
    public String k;
    public Context o;
    public int b = -1;
    public boolean c = false;
    public int d = 0;
    public int h = -1;
    public int i = -1;
    public int l = 0;
    public String m = null;
    public int n = -1;
    public int p = -1;
    public int q = -1;
    public int r = -1;
    public int s = -1;
    public int t = -1;
    public int u = -1;

    /* compiled from: ViewTransition.java */
    /* loaded from: classes.dex */
    public class a implements Interpolator {
        public final /* synthetic */ yt0 a;

        public a(i iVar, yt0 yt0Var) {
            this.a = yt0Var;
        }

        @Override // android.animation.TimeInterpolator
        public float getInterpolation(float f) {
            return (float) this.a.a(f);
        }
    }

    /* compiled from: ViewTransition.java */
    /* loaded from: classes.dex */
    public static class b {
        public final int a;
        public final int b;
        public long c;
        public u92 d;
        public int e;
        public j g;
        public Interpolator h;
        public float j;
        public float k;
        public long l;
        public boolean n;
        public gx1 f = new gx1();
        public boolean i = false;
        public Rect m = new Rect();

        public b(j jVar, u92 u92Var, int i, int i2, int i3, Interpolator interpolator, int i4, int i5) {
            this.n = false;
            this.g = jVar;
            this.d = u92Var;
            this.e = i2;
            long nanoTime = System.nanoTime();
            this.c = nanoTime;
            this.l = nanoTime;
            this.g.b(this);
            this.h = interpolator;
            this.a = i4;
            this.b = i5;
            if (i3 == 3) {
                this.n = true;
            }
            this.k = i == 0 ? Float.MAX_VALUE : 1.0f / i;
            a();
        }

        public void a() {
            if (this.i) {
                c();
            } else {
                b();
            }
        }

        public void b() {
            long nanoTime = System.nanoTime();
            this.l = nanoTime;
            float f = this.j + (((float) ((nanoTime - this.l) * 1.0E-6d)) * this.k);
            this.j = f;
            if (f >= 1.0f) {
                this.j = 1.0f;
            }
            Interpolator interpolator = this.h;
            float interpolation = interpolator == null ? this.j : interpolator.getInterpolation(this.j);
            u92 u92Var = this.d;
            boolean x = u92Var.x(u92Var.b, interpolation, nanoTime, this.f);
            if (this.j >= 1.0f) {
                if (this.a != -1) {
                    this.d.v().setTag(this.a, Long.valueOf(System.nanoTime()));
                }
                if (this.b != -1) {
                    this.d.v().setTag(this.b, null);
                }
                if (!this.n) {
                    this.g.g(this);
                }
            }
            if (this.j < 1.0f || x) {
                this.g.e();
            }
        }

        public void c() {
            long nanoTime = System.nanoTime();
            this.l = nanoTime;
            float f = this.j - (((float) ((nanoTime - this.l) * 1.0E-6d)) * this.k);
            this.j = f;
            if (f < Utils.FLOAT_EPSILON) {
                this.j = Utils.FLOAT_EPSILON;
            }
            Interpolator interpolator = this.h;
            float interpolation = interpolator == null ? this.j : interpolator.getInterpolation(this.j);
            u92 u92Var = this.d;
            boolean x = u92Var.x(u92Var.b, interpolation, nanoTime, this.f);
            if (this.j <= Utils.FLOAT_EPSILON) {
                if (this.a != -1) {
                    this.d.v().setTag(this.a, Long.valueOf(System.nanoTime()));
                }
                if (this.b != -1) {
                    this.d.v().setTag(this.b, null);
                }
                this.g.g(this);
            }
            if (this.j > Utils.FLOAT_EPSILON || x) {
                this.g.e();
            }
        }

        public void d(int i, float f, float f2) {
            if (i == 1) {
                if (this.i) {
                    return;
                }
                e(true);
            } else if (i != 2) {
            } else {
                this.d.v().getHitRect(this.m);
                if (this.m.contains((int) f, (int) f2) || this.i) {
                    return;
                }
                e(true);
            }
        }

        public void e(boolean z) {
            int i;
            this.i = z;
            if (z && (i = this.e) != -1) {
                this.k = i == 0 ? Float.MAX_VALUE : 1.0f / i;
            }
            this.g.e();
            this.l = System.nanoTime();
        }
    }

    public i(Context context, XmlPullParser xmlPullParser) {
        boolean z;
        this.o = context;
        try {
            int eventType = xmlPullParser.getEventType();
            while (eventType != 1) {
                if (eventType == 2) {
                    String name = xmlPullParser.getName();
                    switch (name.hashCode()) {
                        case -1962203927:
                            if (name.equals("ConstraintOverride")) {
                                z = true;
                                break;
                            }
                            z = true;
                            break;
                        case -1239391468:
                            if (name.equals("KeyFrameSet")) {
                                z = true;
                                break;
                            }
                            z = true;
                            break;
                        case 61998586:
                            if (name.equals("ViewTransition")) {
                                z = false;
                                break;
                            }
                            z = true;
                            break;
                        case 366511058:
                            if (name.equals("CustomMethod")) {
                                z = true;
                                break;
                            }
                            z = true;
                            break;
                        case 1791837707:
                            if (name.equals("CustomAttribute")) {
                                z = true;
                                break;
                            }
                            z = true;
                            break;
                        default:
                            z = true;
                            break;
                    }
                    if (!z) {
                        l(context, xmlPullParser);
                    } else if (z) {
                        this.f = new jx1(context, xmlPullParser);
                    } else if (z) {
                        this.g = androidx.constraintlayout.widget.a.m(context, xmlPullParser);
                    } else if (!z && !z) {
                        StringBuilder sb = new StringBuilder();
                        sb.append(xe0.a());
                        sb.append(" unknown tag ");
                        sb.append(name);
                        StringBuilder sb2 = new StringBuilder();
                        sb2.append(".xml:");
                        sb2.append(xmlPullParser.getLineNumber());
                    } else {
                        ConstraintAttribute.i(context, xmlPullParser, this.g.g);
                    }
                } else if (eventType != 3) {
                    continue;
                } else if ("ViewTransition".equals(xmlPullParser.getName())) {
                    return;
                }
                eventType = xmlPullParser.next();
            }
        } catch (IOException e) {
            e.printStackTrace();
        } catch (XmlPullParserException e2) {
            e2.printStackTrace();
        }
    }

    /* JADX INFO: Access modifiers changed from: private */
    public /* synthetic */ void j(View[] viewArr) {
        if (this.p != -1) {
            for (View view : viewArr) {
                view.setTag(this.p, Long.valueOf(System.nanoTime()));
            }
        }
        if (this.q != -1) {
            for (View view2 : viewArr) {
                view2.setTag(this.q, null);
            }
        }
    }

    public void b(j jVar, MotionLayout motionLayout, View view) {
        u92 u92Var = new u92(view);
        u92Var.A(view);
        this.f.a(u92Var);
        u92Var.H(motionLayout.getWidth(), motionLayout.getHeight(), this.h, System.nanoTime());
        new b(jVar, u92Var, this.h, this.i, this.b, f(motionLayout.getContext()), this.p, this.q);
    }

    public void c(j jVar, MotionLayout motionLayout, int i, androidx.constraintlayout.widget.a aVar, final View... viewArr) {
        int[] constraintSetIds;
        if (this.c) {
            return;
        }
        int i2 = this.e;
        if (i2 == 2) {
            b(jVar, motionLayout, viewArr[0]);
            return;
        }
        if (i2 == 1) {
            for (int i3 : motionLayout.getConstraintSetIds()) {
                if (i3 != i) {
                    androidx.constraintlayout.widget.a f0 = motionLayout.f0(i3);
                    for (View view : viewArr) {
                        a.C0021a w = f0.w(view.getId());
                        a.C0021a c0021a = this.g;
                        if (c0021a != null) {
                            c0021a.d(w);
                            w.g.putAll(this.g.g);
                        }
                    }
                }
            }
        }
        androidx.constraintlayout.widget.a aVar2 = new androidx.constraintlayout.widget.a();
        aVar2.q(aVar);
        for (View view2 : viewArr) {
            a.C0021a w2 = aVar2.w(view2.getId());
            a.C0021a c0021a2 = this.g;
            if (c0021a2 != null) {
                c0021a2.d(w2);
                w2.g.putAll(this.g.g);
            }
        }
        motionLayout.B0(i, aVar2);
        int i4 = i03.view_transition;
        motionLayout.B0(i4, aVar);
        motionLayout.setState(i4, -1, -1);
        g.b bVar = new g.b(-1, motionLayout.y0, i4, i);
        for (View view3 : viewArr) {
            n(bVar, view3);
        }
        motionLayout.setTransition(bVar);
        motionLayout.u0(new Runnable() { // from class: ik4
            @Override // java.lang.Runnable
            public final void run() {
                i.this.j(viewArr);
            }
        });
    }

    public boolean d(View view) {
        int i = this.r;
        boolean z = i == -1 || view.getTag(i) != null;
        int i2 = this.s;
        return z && (i2 == -1 || view.getTag(i2) == null);
    }

    public int e() {
        return this.a;
    }

    public Interpolator f(Context context) {
        int i = this.l;
        if (i != -2) {
            if (i != -1) {
                if (i != 0) {
                    if (i != 1) {
                        if (i != 2) {
                            if (i != 4) {
                                if (i != 5) {
                                    if (i != 6) {
                                        return null;
                                    }
                                    return new AnticipateInterpolator();
                                }
                                return new OvershootInterpolator();
                            }
                            return new BounceInterpolator();
                        }
                        return new DecelerateInterpolator();
                    }
                    return new AccelerateInterpolator();
                }
                return new AccelerateDecelerateInterpolator();
            }
            return new a(this, yt0.c(this.m));
        }
        return AnimationUtils.loadInterpolator(context, this.n);
    }

    public int g() {
        return this.t;
    }

    public int h() {
        return this.u;
    }

    public int i() {
        return this.b;
    }

    public boolean k(View view) {
        String str;
        if (view == null) {
            return false;
        }
        if (!(this.j == -1 && this.k == null) && d(view)) {
            if (view.getId() == this.j) {
                return true;
            }
            return this.k != null && (view.getLayoutParams() instanceof ConstraintLayout.LayoutParams) && (str = ((ConstraintLayout.LayoutParams) view.getLayoutParams()).X) != null && str.matches(this.k);
        }
        return false;
    }

    public final void l(Context context, XmlPullParser xmlPullParser) {
        TypedArray obtainStyledAttributes = context.obtainStyledAttributes(Xml.asAttributeSet(xmlPullParser), w23.ViewTransition);
        int indexCount = obtainStyledAttributes.getIndexCount();
        for (int i = 0; i < indexCount; i++) {
            int index = obtainStyledAttributes.getIndex(i);
            if (index == w23.ViewTransition_android_id) {
                this.a = obtainStyledAttributes.getResourceId(index, this.a);
            } else if (index == w23.ViewTransition_motionTarget) {
                if (MotionLayout.T1) {
                    int resourceId = obtainStyledAttributes.getResourceId(index, this.j);
                    this.j = resourceId;
                    if (resourceId == -1) {
                        this.k = obtainStyledAttributes.getString(index);
                    }
                } else if (obtainStyledAttributes.peekValue(index).type == 3) {
                    this.k = obtainStyledAttributes.getString(index);
                } else {
                    this.j = obtainStyledAttributes.getResourceId(index, this.j);
                }
            } else if (index == w23.ViewTransition_onStateTransition) {
                this.b = obtainStyledAttributes.getInt(index, this.b);
            } else if (index == w23.ViewTransition_transitionDisable) {
                this.c = obtainStyledAttributes.getBoolean(index, this.c);
            } else if (index == w23.ViewTransition_pathMotionArc) {
                this.d = obtainStyledAttributes.getInt(index, this.d);
            } else if (index == w23.ViewTransition_duration) {
                this.h = obtainStyledAttributes.getInt(index, this.h);
            } else if (index == w23.ViewTransition_upDuration) {
                this.i = obtainStyledAttributes.getInt(index, this.i);
            } else if (index == w23.ViewTransition_viewTransitionMode) {
                this.e = obtainStyledAttributes.getInt(index, this.e);
            } else if (index == w23.ViewTransition_motionInterpolator) {
                int i2 = obtainStyledAttributes.peekValue(index).type;
                if (i2 == 1) {
                    int resourceId2 = obtainStyledAttributes.getResourceId(index, -1);
                    this.n = resourceId2;
                    if (resourceId2 != -1) {
                        this.l = -2;
                    }
                } else if (i2 == 3) {
                    String string = obtainStyledAttributes.getString(index);
                    this.m = string;
                    if (string != null && string.indexOf("/") > 0) {
                        this.n = obtainStyledAttributes.getResourceId(index, -1);
                        this.l = -2;
                    } else {
                        this.l = -1;
                    }
                } else {
                    this.l = obtainStyledAttributes.getInteger(index, this.l);
                }
            } else if (index == w23.ViewTransition_setsTag) {
                this.p = obtainStyledAttributes.getResourceId(index, this.p);
            } else if (index == w23.ViewTransition_clearsTag) {
                this.q = obtainStyledAttributes.getResourceId(index, this.q);
            } else if (index == w23.ViewTransition_ifTagSet) {
                this.r = obtainStyledAttributes.getResourceId(index, this.r);
            } else if (index == w23.ViewTransition_ifTagNotSet) {
                this.s = obtainStyledAttributes.getResourceId(index, this.s);
            } else if (index == w23.ViewTransition_SharedValueId) {
                this.u = obtainStyledAttributes.getResourceId(index, this.u);
            } else if (index == w23.ViewTransition_SharedValue) {
                this.t = obtainStyledAttributes.getInteger(index, this.t);
            }
        }
        obtainStyledAttributes.recycle();
    }

    public boolean m(int i) {
        int i2 = this.b;
        return i2 == 1 ? i == 0 : i2 == 2 ? i == 1 : i2 == 3 && i == 0;
    }

    public final void n(g.b bVar, View view) {
        int i = this.h;
        if (i != -1) {
            bVar.E(i);
        }
        bVar.I(this.d);
        bVar.G(this.l, this.m, this.n);
        int id = view.getId();
        jx1 jx1Var = this.f;
        if (jx1Var != null) {
            ArrayList<androidx.constraintlayout.motion.widget.a> d = jx1Var.d(-1);
            jx1 jx1Var2 = new jx1();
            Iterator<androidx.constraintlayout.motion.widget.a> it = d.iterator();
            while (it.hasNext()) {
                jx1Var2.c(it.next().clone().i(id));
            }
            bVar.t(jx1Var2);
        }
    }

    public String toString() {
        return "ViewTransition(" + xe0.c(this.o, this.a) + ")";
    }
}
