package androidx.constraintlayout.motion.widget;

import android.content.Context;
import android.content.res.TypedArray;
import android.os.Build;
import android.util.AttributeSet;
import android.util.SparseIntArray;
import com.github.mikephil.charting.utils.Utils;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;

/* compiled from: KeyTimeCycle.java */
/* loaded from: classes.dex */
public class e extends androidx.constraintlayout.motion.widget.a {
    public String g;
    public String v;
    public int h = -1;
    public float i = Float.NaN;
    public float j = Float.NaN;
    public float k = Float.NaN;
    public float l = Float.NaN;
    public float m = Float.NaN;
    public float n = Float.NaN;
    public float o = Float.NaN;
    public float p = Float.NaN;
    public float q = Float.NaN;
    public float r = Float.NaN;
    public float s = Float.NaN;
    public float t = Float.NaN;
    public int u = 0;
    public float w = Float.NaN;
    public float x = Utils.FLOAT_EPSILON;

    /* compiled from: KeyTimeCycle.java */
    /* loaded from: classes.dex */
    public static class a {
        public static SparseIntArray a;

        static {
            SparseIntArray sparseIntArray = new SparseIntArray();
            a = sparseIntArray;
            sparseIntArray.append(w23.KeyTimeCycle_android_alpha, 1);
            a.append(w23.KeyTimeCycle_android_elevation, 2);
            a.append(w23.KeyTimeCycle_android_rotation, 4);
            a.append(w23.KeyTimeCycle_android_rotationX, 5);
            a.append(w23.KeyTimeCycle_android_rotationY, 6);
            a.append(w23.KeyTimeCycle_android_scaleX, 7);
            a.append(w23.KeyTimeCycle_transitionPathRotate, 8);
            a.append(w23.KeyTimeCycle_transitionEasing, 9);
            a.append(w23.KeyTimeCycle_motionTarget, 10);
            a.append(w23.KeyTimeCycle_framePosition, 12);
            a.append(w23.KeyTimeCycle_curveFit, 13);
            a.append(w23.KeyTimeCycle_android_scaleY, 14);
            a.append(w23.KeyTimeCycle_android_translationX, 15);
            a.append(w23.KeyTimeCycle_android_translationY, 16);
            a.append(w23.KeyTimeCycle_android_translationZ, 17);
            a.append(w23.KeyTimeCycle_motionProgress, 18);
            a.append(w23.KeyTimeCycle_wavePeriod, 20);
            a.append(w23.KeyTimeCycle_waveOffset, 21);
            a.append(w23.KeyTimeCycle_waveShape, 19);
        }

        public static void a(e eVar, TypedArray typedArray) {
            int indexCount = typedArray.getIndexCount();
            for (int i = 0; i < indexCount; i++) {
                int index = typedArray.getIndex(i);
                switch (a.get(index)) {
                    case 1:
                        eVar.i = typedArray.getFloat(index, eVar.i);
                        break;
                    case 2:
                        eVar.j = typedArray.getDimension(index, eVar.j);
                        break;
                    case 3:
                    case 11:
                    default:
                        StringBuilder sb = new StringBuilder();
                        sb.append("unused attribute 0x");
                        sb.append(Integer.toHexString(index));
                        sb.append("   ");
                        sb.append(a.get(index));
                        break;
                    case 4:
                        eVar.k = typedArray.getFloat(index, eVar.k);
                        break;
                    case 5:
                        eVar.l = typedArray.getFloat(index, eVar.l);
                        break;
                    case 6:
                        eVar.m = typedArray.getFloat(index, eVar.m);
                        break;
                    case 7:
                        eVar.o = typedArray.getFloat(index, eVar.o);
                        break;
                    case 8:
                        eVar.n = typedArray.getFloat(index, eVar.n);
                        break;
                    case 9:
                        eVar.g = typedArray.getString(index);
                        break;
                    case 10:
                        if (MotionLayout.T1) {
                            int resourceId = typedArray.getResourceId(index, eVar.b);
                            eVar.b = resourceId;
                            if (resourceId == -1) {
                                eVar.c = typedArray.getString(index);
                                break;
                            } else {
                                break;
                            }
                        } else if (typedArray.peekValue(index).type == 3) {
                            eVar.c = typedArray.getString(index);
                            break;
                        } else {
                            eVar.b = typedArray.getResourceId(index, eVar.b);
                            break;
                        }
                    case 12:
                        eVar.a = typedArray.getInt(index, eVar.a);
                        break;
                    case 13:
                        eVar.h = typedArray.getInteger(index, eVar.h);
                        break;
                    case 14:
                        eVar.p = typedArray.getFloat(index, eVar.p);
                        break;
                    case 15:
                        eVar.q = typedArray.getDimension(index, eVar.q);
                        break;
                    case 16:
                        eVar.r = typedArray.getDimension(index, eVar.r);
                        break;
                    case 17:
                        if (Build.VERSION.SDK_INT >= 21) {
                            eVar.s = typedArray.getDimension(index, eVar.s);
                            break;
                        } else {
                            break;
                        }
                    case 18:
                        eVar.t = typedArray.getFloat(index, eVar.t);
                        break;
                    case 19:
                        if (typedArray.peekValue(index).type == 3) {
                            eVar.v = typedArray.getString(index);
                            eVar.u = 7;
                            break;
                        } else {
                            eVar.u = typedArray.getInt(index, eVar.u);
                            break;
                        }
                    case 20:
                        eVar.w = typedArray.getFloat(index, eVar.w);
                        break;
                    case 21:
                        if (typedArray.peekValue(index).type == 5) {
                            eVar.x = typedArray.getDimension(index, eVar.x);
                            break;
                        } else {
                            eVar.x = typedArray.getFloat(index, eVar.x);
                            break;
                        }
                }
            }
        }
    }

    public e() {
        this.d = 3;
        this.e = new HashMap<>();
    }

    /* JADX WARN: Can't fix incorrect switch cases order, some code will duplicate */
    /* JADX WARN: Code restructure failed: missing block: B:33:0x0089, code lost:
        if (r1.equals("scaleY") == false) goto L12;
     */
    /*
        Code decompiled incorrectly, please refer to instructions dump.
        To view partially-correct code enable 'Show inconsistent code' option in preferences
    */
    public void U(java.util.HashMap<java.lang.String, defpackage.gk4> r11) {
        /*
            Method dump skipped, instructions count: 596
            To view this dump change 'Code comments level' option to 'DEBUG'
        */
        throw new UnsupportedOperationException("Method not decompiled: androidx.constraintlayout.motion.widget.e.U(java.util.HashMap):void");
    }

    @Override // androidx.constraintlayout.motion.widget.a
    public void a(HashMap<String, ak4> hashMap) {
        throw new IllegalArgumentException(" KeyTimeCycles do not support SplineSet");
    }

    @Override // androidx.constraintlayout.motion.widget.a
    /* renamed from: b */
    public androidx.constraintlayout.motion.widget.a clone() {
        return new e().c(this);
    }

    @Override // androidx.constraintlayout.motion.widget.a
    public androidx.constraintlayout.motion.widget.a c(androidx.constraintlayout.motion.widget.a aVar) {
        super.c(aVar);
        e eVar = (e) aVar;
        this.g = eVar.g;
        this.h = eVar.h;
        this.u = eVar.u;
        this.w = eVar.w;
        this.x = eVar.x;
        this.t = eVar.t;
        this.i = eVar.i;
        this.j = eVar.j;
        this.k = eVar.k;
        this.n = eVar.n;
        this.l = eVar.l;
        this.m = eVar.m;
        this.o = eVar.o;
        this.p = eVar.p;
        this.q = eVar.q;
        this.r = eVar.r;
        this.s = eVar.s;
        return this;
    }

    @Override // androidx.constraintlayout.motion.widget.a
    public void d(HashSet<String> hashSet) {
        if (!Float.isNaN(this.i)) {
            hashSet.add("alpha");
        }
        if (!Float.isNaN(this.j)) {
            hashSet.add("elevation");
        }
        if (!Float.isNaN(this.k)) {
            hashSet.add("rotation");
        }
        if (!Float.isNaN(this.l)) {
            hashSet.add("rotationX");
        }
        if (!Float.isNaN(this.m)) {
            hashSet.add("rotationY");
        }
        if (!Float.isNaN(this.q)) {
            hashSet.add("translationX");
        }
        if (!Float.isNaN(this.r)) {
            hashSet.add("translationY");
        }
        if (!Float.isNaN(this.s)) {
            hashSet.add("translationZ");
        }
        if (!Float.isNaN(this.n)) {
            hashSet.add("transitionPathRotate");
        }
        if (!Float.isNaN(this.o)) {
            hashSet.add("scaleX");
        }
        if (!Float.isNaN(this.p)) {
            hashSet.add("scaleY");
        }
        if (!Float.isNaN(this.t)) {
            hashSet.add("progress");
        }
        if (this.e.size() > 0) {
            Iterator<String> it = this.e.keySet().iterator();
            while (it.hasNext()) {
                hashSet.add("CUSTOM," + it.next());
            }
        }
    }

    @Override // androidx.constraintlayout.motion.widget.a
    public void e(Context context, AttributeSet attributeSet) {
        a.a(this, context.obtainStyledAttributes(attributeSet, w23.KeyTimeCycle));
    }

    @Override // androidx.constraintlayout.motion.widget.a
    public void h(HashMap<String, Integer> hashMap) {
        if (this.h == -1) {
            return;
        }
        if (!Float.isNaN(this.i)) {
            hashMap.put("alpha", Integer.valueOf(this.h));
        }
        if (!Float.isNaN(this.j)) {
            hashMap.put("elevation", Integer.valueOf(this.h));
        }
        if (!Float.isNaN(this.k)) {
            hashMap.put("rotation", Integer.valueOf(this.h));
        }
        if (!Float.isNaN(this.l)) {
            hashMap.put("rotationX", Integer.valueOf(this.h));
        }
        if (!Float.isNaN(this.m)) {
            hashMap.put("rotationY", Integer.valueOf(this.h));
        }
        if (!Float.isNaN(this.q)) {
            hashMap.put("translationX", Integer.valueOf(this.h));
        }
        if (!Float.isNaN(this.r)) {
            hashMap.put("translationY", Integer.valueOf(this.h));
        }
        if (!Float.isNaN(this.s)) {
            hashMap.put("translationZ", Integer.valueOf(this.h));
        }
        if (!Float.isNaN(this.n)) {
            hashMap.put("transitionPathRotate", Integer.valueOf(this.h));
        }
        if (!Float.isNaN(this.o)) {
            hashMap.put("scaleX", Integer.valueOf(this.h));
        }
        if (!Float.isNaN(this.o)) {
            hashMap.put("scaleY", Integer.valueOf(this.h));
        }
        if (!Float.isNaN(this.t)) {
            hashMap.put("progress", Integer.valueOf(this.h));
        }
        if (this.e.size() > 0) {
            Iterator<String> it = this.e.keySet().iterator();
            while (it.hasNext()) {
                hashMap.put("CUSTOM," + it.next(), Integer.valueOf(this.h));
            }
        }
    }
}
