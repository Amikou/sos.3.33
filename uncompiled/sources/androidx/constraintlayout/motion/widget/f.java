package androidx.constraintlayout.motion.widget;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.RectF;
import android.util.AttributeSet;
import android.util.SparseIntArray;
import android.view.View;
import androidx.constraintlayout.widget.ConstraintAttribute;
import com.fasterxml.jackson.core.util.MinimalPrettyPrinter;
import java.lang.reflect.Method;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Locale;

/* compiled from: KeyTrigger.java */
/* loaded from: classes.dex */
public class f extends androidx.constraintlayout.motion.widget.a {
    public HashMap<String, Method> A;
    public int g = -1;
    public String h = null;
    public int i;
    public String j;
    public String k;
    public int l;
    public int m;
    public View n;
    public float o;
    public boolean p;
    public boolean q;
    public boolean r;
    public float s;
    public float t;
    public boolean u;
    public int v;
    public int w;
    public int x;
    public RectF y;
    public RectF z;

    /* compiled from: KeyTrigger.java */
    /* loaded from: classes.dex */
    public static class a {
        public static SparseIntArray a;

        static {
            SparseIntArray sparseIntArray = new SparseIntArray();
            a = sparseIntArray;
            sparseIntArray.append(w23.KeyTrigger_framePosition, 8);
            a.append(w23.KeyTrigger_onCross, 4);
            a.append(w23.KeyTrigger_onNegativeCross, 1);
            a.append(w23.KeyTrigger_onPositiveCross, 2);
            a.append(w23.KeyTrigger_motionTarget, 7);
            a.append(w23.KeyTrigger_triggerId, 6);
            a.append(w23.KeyTrigger_triggerSlack, 5);
            a.append(w23.KeyTrigger_motion_triggerOnCollision, 9);
            a.append(w23.KeyTrigger_motion_postLayoutCollision, 10);
            a.append(w23.KeyTrigger_triggerReceiver, 11);
            a.append(w23.KeyTrigger_viewTransitionOnCross, 12);
            a.append(w23.KeyTrigger_viewTransitionOnNegativeCross, 13);
            a.append(w23.KeyTrigger_viewTransitionOnPositiveCross, 14);
        }

        public static void a(f fVar, TypedArray typedArray, Context context) {
            int indexCount = typedArray.getIndexCount();
            for (int i = 0; i < indexCount; i++) {
                int index = typedArray.getIndex(i);
                switch (a.get(index)) {
                    case 1:
                        fVar.j = typedArray.getString(index);
                        break;
                    case 2:
                        fVar.k = typedArray.getString(index);
                        break;
                    case 3:
                    default:
                        StringBuilder sb = new StringBuilder();
                        sb.append("unused attribute 0x");
                        sb.append(Integer.toHexString(index));
                        sb.append("   ");
                        sb.append(a.get(index));
                        break;
                    case 4:
                        fVar.h = typedArray.getString(index);
                        break;
                    case 5:
                        fVar.o = typedArray.getFloat(index, fVar.o);
                        break;
                    case 6:
                        fVar.l = typedArray.getResourceId(index, fVar.l);
                        break;
                    case 7:
                        if (MotionLayout.T1) {
                            int resourceId = typedArray.getResourceId(index, fVar.b);
                            fVar.b = resourceId;
                            if (resourceId == -1) {
                                fVar.c = typedArray.getString(index);
                                break;
                            } else {
                                break;
                            }
                        } else if (typedArray.peekValue(index).type == 3) {
                            fVar.c = typedArray.getString(index);
                            break;
                        } else {
                            fVar.b = typedArray.getResourceId(index, fVar.b);
                            break;
                        }
                    case 8:
                        int integer = typedArray.getInteger(index, fVar.a);
                        fVar.a = integer;
                        fVar.s = (integer + 0.5f) / 100.0f;
                        break;
                    case 9:
                        fVar.m = typedArray.getResourceId(index, fVar.m);
                        break;
                    case 10:
                        fVar.u = typedArray.getBoolean(index, fVar.u);
                        break;
                    case 11:
                        fVar.i = typedArray.getResourceId(index, fVar.i);
                        break;
                    case 12:
                        fVar.x = typedArray.getResourceId(index, fVar.x);
                        break;
                    case 13:
                        fVar.v = typedArray.getResourceId(index, fVar.v);
                        break;
                    case 14:
                        fVar.w = typedArray.getResourceId(index, fVar.w);
                        break;
                }
            }
        }
    }

    public f() {
        int i = androidx.constraintlayout.motion.widget.a.f;
        this.i = i;
        this.j = null;
        this.k = null;
        this.l = i;
        this.m = i;
        this.n = null;
        this.o = 0.1f;
        this.p = true;
        this.q = true;
        this.r = true;
        this.s = Float.NaN;
        this.u = false;
        this.v = i;
        this.w = i;
        this.x = i;
        this.y = new RectF();
        this.z = new RectF();
        this.A = new HashMap<>();
        this.d = 5;
        this.e = new HashMap<>();
    }

    public final void A(String str, View view) {
        boolean z = str.length() == 1;
        if (!z) {
            str = str.substring(1).toLowerCase(Locale.ROOT);
        }
        for (String str2 : this.e.keySet()) {
            String lowerCase = str2.toLowerCase(Locale.ROOT);
            if (z || lowerCase.matches(str)) {
                ConstraintAttribute constraintAttribute = this.e.get(str2);
                if (constraintAttribute != null) {
                    constraintAttribute.a(view);
                }
            }
        }
    }

    public final void B(RectF rectF, View view, boolean z) {
        rectF.top = view.getTop();
        rectF.bottom = view.getBottom();
        rectF.left = view.getLeft();
        rectF.right = view.getRight();
        if (z) {
            view.getMatrix().mapRect(rectF);
        }
    }

    @Override // androidx.constraintlayout.motion.widget.a
    public void a(HashMap<String, ak4> hashMap) {
    }

    @Override // androidx.constraintlayout.motion.widget.a
    /* renamed from: b */
    public androidx.constraintlayout.motion.widget.a clone() {
        return new f().c(this);
    }

    @Override // androidx.constraintlayout.motion.widget.a
    public androidx.constraintlayout.motion.widget.a c(androidx.constraintlayout.motion.widget.a aVar) {
        super.c(aVar);
        f fVar = (f) aVar;
        this.g = fVar.g;
        this.h = fVar.h;
        this.i = fVar.i;
        this.j = fVar.j;
        this.k = fVar.k;
        this.l = fVar.l;
        this.m = fVar.m;
        this.n = fVar.n;
        this.o = fVar.o;
        this.p = fVar.p;
        this.q = fVar.q;
        this.r = fVar.r;
        this.s = fVar.s;
        this.t = fVar.t;
        this.u = fVar.u;
        this.y = fVar.y;
        this.z = fVar.z;
        this.A = fVar.A;
        return this;
    }

    @Override // androidx.constraintlayout.motion.widget.a
    public void d(HashSet<String> hashSet) {
    }

    @Override // androidx.constraintlayout.motion.widget.a
    public void e(Context context, AttributeSet attributeSet) {
        a.a(this, context.obtainStyledAttributes(attributeSet, w23.KeyTrigger), context);
    }

    /* JADX WARN: Removed duplicated region for block: B:38:0x008c  */
    /* JADX WARN: Removed duplicated region for block: B:43:0x00a0  */
    /* JADX WARN: Removed duplicated region for block: B:49:0x00b5  */
    /* JADX WARN: Removed duplicated region for block: B:56:0x00ce  */
    /*
        Code decompiled incorrectly, please refer to instructions dump.
        To view partially-correct code enable 'Show inconsistent code' option in preferences
    */
    public void y(float r10, android.view.View r11) {
        /*
            Method dump skipped, instructions count: 354
            To view this dump change 'Code comments level' option to 'DEBUG'
        */
        throw new UnsupportedOperationException("Method not decompiled: androidx.constraintlayout.motion.widget.f.y(float, android.view.View):void");
    }

    public final void z(String str, View view) {
        Method method;
        if (str == null) {
            return;
        }
        if (str.startsWith(".")) {
            A(str, view);
            return;
        }
        if (this.A.containsKey(str)) {
            method = this.A.get(str);
            if (method == null) {
                return;
            }
        } else {
            method = null;
        }
        if (method == null) {
            try {
                method = view.getClass().getMethod(str, new Class[0]);
                this.A.put(str, method);
            } catch (NoSuchMethodException unused) {
                this.A.put(str, null);
                StringBuilder sb = new StringBuilder();
                sb.append("Could not find method \"");
                sb.append(str);
                sb.append("\"on class ");
                sb.append(view.getClass().getSimpleName());
                sb.append(MinimalPrettyPrinter.DEFAULT_ROOT_VALUE_SEPARATOR);
                sb.append(xe0.d(view));
                return;
            }
        }
        try {
            method.invoke(view, new Object[0]);
        } catch (Exception unused2) {
            StringBuilder sb2 = new StringBuilder();
            sb2.append("Exception in call \"");
            sb2.append(this.h);
            sb2.append("\"on class ");
            sb2.append(view.getClass().getSimpleName());
            sb2.append(MinimalPrettyPrinter.DEFAULT_ROOT_VALUE_SEPARATOR);
            sb2.append(xe0.d(view));
        }
    }
}
