package androidx.constraintlayout.motion.widget;

import android.content.Context;
import android.content.res.TypedArray;
import android.content.res.XmlResourceParser;
import android.graphics.RectF;
import android.util.AttributeSet;
import android.util.SparseArray;
import android.util.SparseIntArray;
import android.util.Xml;
import android.view.MotionEvent;
import android.view.View;
import android.view.animation.AccelerateDecelerateInterpolator;
import android.view.animation.AccelerateInterpolator;
import android.view.animation.AnimationUtils;
import android.view.animation.AnticipateInterpolator;
import android.view.animation.BounceInterpolator;
import android.view.animation.DecelerateInterpolator;
import android.view.animation.Interpolator;
import android.view.animation.OvershootInterpolator;
import androidx.constraintlayout.motion.widget.MotionLayout;
import com.github.mikephil.charting.utils.Utils;
import java.io.IOException;
import java.io.PrintStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserException;

/* compiled from: MotionScene.java */
/* loaded from: classes.dex */
public class g {
    public final MotionLayout a;
    public MotionEvent n;
    public MotionLayout.g q;
    public boolean r;
    public final j s;
    public float t;
    public float u;
    public ct3 b = null;
    public b c = null;
    public boolean d = false;
    public ArrayList<b> e = new ArrayList<>();
    public b f = null;
    public ArrayList<b> g = new ArrayList<>();
    public SparseArray<androidx.constraintlayout.widget.a> h = new SparseArray<>();
    public HashMap<String, Integer> i = new HashMap<>();
    public SparseIntArray j = new SparseIntArray();
    public boolean k = false;
    public int l = 400;
    public int m = 0;
    public boolean o = false;
    public boolean p = false;

    /* compiled from: MotionScene.java */
    /* loaded from: classes.dex */
    public class a implements Interpolator {
        public final /* synthetic */ yt0 a;

        public a(g gVar, yt0 yt0Var) {
            this.a = yt0Var;
        }

        @Override // android.animation.TimeInterpolator
        public float getInterpolation(float f) {
            return (float) this.a.a(f);
        }
    }

    public g(Context context, MotionLayout motionLayout, int i) {
        this.a = motionLayout;
        this.s = new j(motionLayout);
        K(context, i);
        SparseArray<androidx.constraintlayout.widget.a> sparseArray = this.h;
        int i2 = i03.motion_base;
        sparseArray.put(i2, new androidx.constraintlayout.widget.a());
        this.i.put("motion_base", Integer.valueOf(i2));
    }

    public static String a0(String str) {
        if (str == null) {
            return "";
        }
        int indexOf = str.indexOf(47);
        return indexOf < 0 ? str : str.substring(indexOf + 1);
    }

    public float A() {
        b bVar = this.c;
        return (bVar == null || bVar.l == null) ? Utils.FLOAT_EPSILON : this.c.l.l();
    }

    public float B() {
        b bVar = this.c;
        return (bVar == null || bVar.l == null) ? Utils.FLOAT_EPSILON : this.c.l.m();
    }

    public float C() {
        b bVar = this.c;
        return (bVar == null || bVar.l == null) ? Utils.FLOAT_EPSILON : this.c.l.n();
    }

    public float D() {
        b bVar = this.c;
        return (bVar == null || bVar.l == null) ? Utils.FLOAT_EPSILON : this.c.l.o();
    }

    public float E() {
        b bVar = this.c;
        return bVar != null ? bVar.i : Utils.FLOAT_EPSILON;
    }

    public int F() {
        b bVar = this.c;
        if (bVar == null) {
            return -1;
        }
        return bVar.d;
    }

    public b G(int i) {
        Iterator<b> it = this.e.iterator();
        while (it.hasNext()) {
            b next = it.next();
            if (next.a == i) {
                return next;
            }
        }
        return null;
    }

    public List<b> H(int i) {
        int y = y(i);
        ArrayList arrayList = new ArrayList();
        Iterator<b> it = this.e.iterator();
        while (it.hasNext()) {
            b next = it.next();
            if (next.d == y || next.c == y) {
                arrayList.add(next);
            }
        }
        return arrayList;
    }

    public final boolean I(int i) {
        int i2 = this.j.get(i);
        int size = this.j.size();
        while (i2 > 0) {
            if (i2 == i) {
                return true;
            }
            int i3 = size - 1;
            if (size < 0) {
                return true;
            }
            i2 = this.j.get(i2);
            size = i3;
        }
        return false;
    }

    public final boolean J() {
        return this.q != null;
    }

    public final void K(Context context, int i) {
        XmlResourceParser xml = context.getResources().getXml(i);
        b bVar = null;
        try {
            int eventType = xml.getEventType();
            while (true) {
                char c = 1;
                if (eventType == 1) {
                    return;
                }
                if (eventType == 0) {
                    xml.getName();
                    continue;
                } else if (eventType != 2) {
                    continue;
                } else {
                    String name = xml.getName();
                    if (this.k) {
                        System.out.println("parsing = " + name);
                    }
                    switch (name.hashCode()) {
                        case -1349929691:
                            if (name.equals("ConstraintSet")) {
                                c = 5;
                                break;
                            }
                            c = 65535;
                            break;
                        case -1239391468:
                            if (name.equals("KeyFrameSet")) {
                                c = '\b';
                                break;
                            }
                            c = 65535;
                            break;
                        case -687739768:
                            if (name.equals("Include")) {
                                c = 7;
                                break;
                            }
                            c = 65535;
                            break;
                        case 61998586:
                            if (name.equals("ViewTransition")) {
                                c = '\t';
                                break;
                            }
                            c = 65535;
                            break;
                        case 269306229:
                            if (name.equals("Transition")) {
                                break;
                            }
                            c = 65535;
                            break;
                        case 312750793:
                            if (name.equals("OnClick")) {
                                c = 3;
                                break;
                            }
                            c = 65535;
                            break;
                        case 327855227:
                            if (name.equals("OnSwipe")) {
                                c = 2;
                                break;
                            }
                            c = 65535;
                            break;
                        case 793277014:
                            if (name.equals("MotionScene")) {
                                c = 0;
                                break;
                            }
                            c = 65535;
                            break;
                        case 1382829617:
                            if (name.equals("StateSet")) {
                                c = 4;
                                break;
                            }
                            c = 65535;
                            break;
                        case 1942574248:
                            if (name.equals("include")) {
                                c = 6;
                                break;
                            }
                            c = 65535;
                            break;
                        default:
                            c = 65535;
                            break;
                    }
                    switch (c) {
                        case 0:
                            O(context, xml);
                            continue;
                        case 1:
                            ArrayList<b> arrayList = this.e;
                            b bVar2 = new b(this, context, xml);
                            arrayList.add(bVar2);
                            if (this.c == null && !bVar2.b) {
                                this.c = bVar2;
                                if (bVar2.l != null) {
                                    this.c.l.w(this.r);
                                }
                            }
                            if (bVar2.b) {
                                if (bVar2.c == -1) {
                                    this.f = bVar2;
                                } else {
                                    this.g.add(bVar2);
                                }
                                this.e.remove(bVar2);
                            }
                            bVar = bVar2;
                            continue;
                        case 2:
                            if (bVar == null) {
                                String resourceEntryName = context.getResources().getResourceEntryName(i);
                                int lineNumber = xml.getLineNumber();
                                StringBuilder sb = new StringBuilder();
                                sb.append(" OnSwipe (");
                                sb.append(resourceEntryName);
                                sb.append(".xml:");
                                sb.append(lineNumber);
                                sb.append(")");
                            }
                            if (bVar != null) {
                                bVar.l = new h(context, this.a, xml);
                                continue;
                            } else {
                                continue;
                            }
                        case 3:
                            if (bVar != null) {
                                bVar.u(context, xml);
                                continue;
                            } else {
                                continue;
                            }
                        case 4:
                            this.b = new ct3(context, xml);
                            continue;
                        case 5:
                            L(context, xml);
                            continue;
                        case 6:
                        case 7:
                            N(context, xml);
                            continue;
                        case '\b':
                            jx1 jx1Var = new jx1(context, xml);
                            if (bVar != null) {
                                bVar.k.add(jx1Var);
                                continue;
                            } else {
                                continue;
                            }
                        case '\t':
                            this.s.a(new i(context, xml));
                            continue;
                        default:
                            continue;
                    }
                }
                eventType = xml.next();
            }
        } catch (IOException e) {
            e.printStackTrace();
        } catch (XmlPullParserException e2) {
            e2.printStackTrace();
        }
    }

    /* JADX WARN: Can't fix incorrect switch cases order, some code will duplicate */
    /* JADX WARN: Code restructure failed: missing block: B:16:0x0053, code lost:
        if (r8.equals("constraintRotate") == false) goto L9;
     */
    /*
        Code decompiled incorrectly, please refer to instructions dump.
        To view partially-correct code enable 'Show inconsistent code' option in preferences
    */
    public final int L(android.content.Context r14, org.xmlpull.v1.XmlPullParser r15) {
        /*
            r13 = this;
            androidx.constraintlayout.widget.a r0 = new androidx.constraintlayout.widget.a
            r0.<init>()
            r1 = 0
            r0.R(r1)
            int r2 = r15.getAttributeCount()
            r3 = -1
            r4 = r1
            r5 = r3
            r6 = r5
        L11:
            r7 = 1
            if (r4 >= r2) goto L8a
            java.lang.String r8 = r15.getAttributeName(r4)
            java.lang.String r9 = r15.getAttributeValue(r4)
            boolean r10 = r13.k
            if (r10 == 0) goto L36
            java.io.PrintStream r10 = java.lang.System.out
            java.lang.StringBuilder r11 = new java.lang.StringBuilder
            r11.<init>()
            java.lang.String r12 = "id string = "
            r11.append(r12)
            r11.append(r9)
            java.lang.String r11 = r11.toString()
            r10.println(r11)
        L36:
            r8.hashCode()
            int r10 = r8.hashCode()
            switch(r10) {
                case -1496482599: goto L56;
                case -1153153640: goto L4d;
                case 3355: goto L42;
                default: goto L40;
            }
        L40:
            r7 = r3
            goto L60
        L42:
            java.lang.String r7 = "id"
            boolean r7 = r8.equals(r7)
            if (r7 != 0) goto L4b
            goto L40
        L4b:
            r7 = 2
            goto L60
        L4d:
            java.lang.String r10 = "constraintRotate"
            boolean r8 = r8.equals(r10)
            if (r8 != 0) goto L60
            goto L40
        L56:
            java.lang.String r7 = "deriveConstraintsFrom"
            boolean r7 = r8.equals(r7)
            if (r7 != 0) goto L5f
            goto L40
        L5f:
            r7 = r1
        L60:
            switch(r7) {
                case 0: goto L83;
                case 1: goto L7c;
                case 2: goto L64;
                default: goto L63;
            }
        L63:
            goto L87
        L64:
            int r5 = r13.r(r14, r9)
            java.util.HashMap<java.lang.String, java.lang.Integer> r7 = r13.i
            java.lang.String r8 = a0(r9)
            java.lang.Integer r9 = java.lang.Integer.valueOf(r5)
            r7.put(r8, r9)
            java.lang.String r7 = defpackage.xe0.c(r14, r5)
            r0.a = r7
            goto L87
        L7c:
            int r7 = java.lang.Integer.parseInt(r9)
            r0.c = r7
            goto L87
        L83:
            int r6 = r13.r(r14, r9)
        L87:
            int r4 = r4 + 1
            goto L11
        L8a:
            if (r5 == r3) goto La4
            androidx.constraintlayout.motion.widget.MotionLayout r1 = r13.a
            int r1 = r1.U0
            if (r1 == 0) goto L95
            r0.S(r7)
        L95:
            r0.E(r14, r15)
            if (r6 == r3) goto L9f
            android.util.SparseIntArray r14 = r13.j
            r14.put(r5, r6)
        L9f:
            android.util.SparseArray<androidx.constraintlayout.widget.a> r14 = r13.h
            r14.put(r5, r0)
        La4:
            return r5
        */
        throw new UnsupportedOperationException("Method not decompiled: androidx.constraintlayout.motion.widget.g.L(android.content.Context, org.xmlpull.v1.XmlPullParser):int");
    }

    public final int M(Context context, int i) {
        XmlResourceParser xml = context.getResources().getXml(i);
        try {
            for (int eventType = xml.getEventType(); eventType != 1; eventType = xml.next()) {
                String name = xml.getName();
                if (2 == eventType && "ConstraintSet".equals(name)) {
                    return L(context, xml);
                }
            }
            return -1;
        } catch (IOException e) {
            e.printStackTrace();
            return -1;
        } catch (XmlPullParserException e2) {
            e2.printStackTrace();
            return -1;
        }
    }

    public final void N(Context context, XmlPullParser xmlPullParser) {
        TypedArray obtainStyledAttributes = context.obtainStyledAttributes(Xml.asAttributeSet(xmlPullParser), w23.include);
        int indexCount = obtainStyledAttributes.getIndexCount();
        for (int i = 0; i < indexCount; i++) {
            int index = obtainStyledAttributes.getIndex(i);
            if (index == w23.include_constraintSet) {
                M(context, obtainStyledAttributes.getResourceId(index, -1));
            }
        }
        obtainStyledAttributes.recycle();
    }

    public final void O(Context context, XmlPullParser xmlPullParser) {
        TypedArray obtainStyledAttributes = context.obtainStyledAttributes(Xml.asAttributeSet(xmlPullParser), w23.MotionScene);
        int indexCount = obtainStyledAttributes.getIndexCount();
        for (int i = 0; i < indexCount; i++) {
            int index = obtainStyledAttributes.getIndex(i);
            if (index == w23.MotionScene_defaultDuration) {
                int i2 = obtainStyledAttributes.getInt(index, this.l);
                this.l = i2;
                if (i2 < 8) {
                    this.l = 8;
                }
            } else if (index == w23.MotionScene_layoutDuringTransition) {
                this.m = obtainStyledAttributes.getInteger(index, 0);
            }
        }
        obtainStyledAttributes.recycle();
    }

    public void P(float f, float f2) {
        b bVar = this.c;
        if (bVar == null || bVar.l == null) {
            return;
        }
        this.c.l.t(f, f2);
    }

    public void Q(float f, float f2) {
        b bVar = this.c;
        if (bVar == null || bVar.l == null) {
            return;
        }
        this.c.l.u(f, f2);
    }

    public void R(MotionEvent motionEvent, int i, MotionLayout motionLayout) {
        MotionLayout.g gVar;
        MotionEvent motionEvent2;
        RectF rectF = new RectF();
        if (this.q == null) {
            this.q = this.a.m0();
        }
        this.q.a(motionEvent);
        if (i != -1) {
            int action = motionEvent.getAction();
            boolean z = false;
            if (action != 0) {
                if (action == 2 && !this.o) {
                    float rawY = motionEvent.getRawY() - this.u;
                    float rawX = motionEvent.getRawX() - this.t;
                    if ((rawX == Utils.DOUBLE_EPSILON && rawY == Utils.DOUBLE_EPSILON) || (motionEvent2 = this.n) == null) {
                        return;
                    }
                    b i2 = i(i, rawX, rawY, motionEvent2);
                    if (i2 != null) {
                        motionLayout.setTransition(i2);
                        RectF p = this.c.l.p(this.a, rectF);
                        if (p != null && !p.contains(this.n.getX(), this.n.getY())) {
                            z = true;
                        }
                        this.p = z;
                        this.c.l.y(this.t, this.u);
                    }
                }
            } else {
                this.t = motionEvent.getRawX();
                this.u = motionEvent.getRawY();
                this.n = motionEvent;
                this.o = false;
                if (this.c.l != null) {
                    RectF f = this.c.l.f(this.a, rectF);
                    if (f == null || f.contains(this.n.getX(), this.n.getY())) {
                        RectF p2 = this.c.l.p(this.a, rectF);
                        if (p2 != null && !p2.contains(this.n.getX(), this.n.getY())) {
                            this.p = true;
                        } else {
                            this.p = false;
                        }
                        this.c.l.v(this.t, this.u);
                        return;
                    }
                    this.n = null;
                    this.o = true;
                    return;
                }
                return;
            }
        }
        if (this.o) {
            return;
        }
        b bVar = this.c;
        if (bVar != null && bVar.l != null && !this.p) {
            this.c.l.r(motionEvent, this.q, i, this);
        }
        this.t = motionEvent.getRawX();
        this.u = motionEvent.getRawY();
        if (motionEvent.getAction() != 1 || (gVar = this.q) == null) {
            return;
        }
        gVar.b();
        this.q = null;
        int i3 = motionLayout.D0;
        if (i3 != -1) {
            h(motionLayout, i3);
        }
    }

    public final void S(int i, MotionLayout motionLayout) {
        androidx.constraintlayout.widget.a aVar = this.h.get(i);
        aVar.b = aVar.a;
        int i2 = this.j.get(i);
        if (i2 > 0) {
            S(i2, motionLayout);
            androidx.constraintlayout.widget.a aVar2 = this.h.get(i2);
            if (aVar2 == null) {
                StringBuilder sb = new StringBuilder();
                sb.append("ERROR! invalid deriveConstraintsFrom: @id/");
                sb.append(xe0.c(this.a.getContext(), i2));
                return;
            }
            aVar.b += "/" + aVar2.b;
            aVar.M(aVar2);
        } else {
            aVar.b += "  layout";
            aVar.L(motionLayout);
        }
        aVar.h(aVar);
    }

    public void T(MotionLayout motionLayout) {
        for (int i = 0; i < this.h.size(); i++) {
            int keyAt = this.h.keyAt(i);
            if (I(keyAt)) {
                return;
            }
            S(keyAt, motionLayout);
        }
    }

    public void U(int i, androidx.constraintlayout.widget.a aVar) {
        this.h.put(i, aVar);
    }

    public void V(int i) {
        b bVar = this.c;
        if (bVar != null) {
            bVar.E(i);
        } else {
            this.l = i;
        }
    }

    public void W(boolean z) {
        this.r = z;
        b bVar = this.c;
        if (bVar == null || bVar.l == null) {
            return;
        }
        this.c.l.w(this.r);
    }

    /* JADX WARN: Code restructure failed: missing block: B:9:0x0013, code lost:
        if (r2 != (-1)) goto L9;
     */
    /*
        Code decompiled incorrectly, please refer to instructions dump.
        To view partially-correct code enable 'Show inconsistent code' option in preferences
    */
    public void X(int r7, int r8) {
        /*
            r6 = this;
            ct3 r0 = r6.b
            r1 = -1
            if (r0 == 0) goto L16
            int r0 = r0.c(r7, r1, r1)
            if (r0 == r1) goto Lc
            goto Ld
        Lc:
            r0 = r7
        Ld:
            ct3 r2 = r6.b
            int r2 = r2.c(r8, r1, r1)
            if (r2 == r1) goto L17
            goto L18
        L16:
            r0 = r7
        L17:
            r2 = r8
        L18:
            androidx.constraintlayout.motion.widget.g$b r3 = r6.c
            if (r3 == 0) goto L2b
            int r3 = androidx.constraintlayout.motion.widget.g.b.a(r3)
            if (r3 != r8) goto L2b
            androidx.constraintlayout.motion.widget.g$b r3 = r6.c
            int r3 = androidx.constraintlayout.motion.widget.g.b.c(r3)
            if (r3 != r7) goto L2b
            return
        L2b:
            java.util.ArrayList<androidx.constraintlayout.motion.widget.g$b> r3 = r6.e
            java.util.Iterator r3 = r3.iterator()
        L31:
            boolean r4 = r3.hasNext()
            if (r4 == 0) goto L6b
            java.lang.Object r4 = r3.next()
            androidx.constraintlayout.motion.widget.g$b r4 = (androidx.constraintlayout.motion.widget.g.b) r4
            int r5 = androidx.constraintlayout.motion.widget.g.b.a(r4)
            if (r5 != r2) goto L49
            int r5 = androidx.constraintlayout.motion.widget.g.b.c(r4)
            if (r5 == r0) goto L55
        L49:
            int r5 = androidx.constraintlayout.motion.widget.g.b.a(r4)
            if (r5 != r8) goto L31
            int r5 = androidx.constraintlayout.motion.widget.g.b.c(r4)
            if (r5 != r7) goto L31
        L55:
            r6.c = r4
            if (r4 == 0) goto L6a
            androidx.constraintlayout.motion.widget.h r7 = androidx.constraintlayout.motion.widget.g.b.l(r4)
            if (r7 == 0) goto L6a
            androidx.constraintlayout.motion.widget.g$b r7 = r6.c
            androidx.constraintlayout.motion.widget.h r7 = androidx.constraintlayout.motion.widget.g.b.l(r7)
            boolean r8 = r6.r
            r7.w(r8)
        L6a:
            return
        L6b:
            androidx.constraintlayout.motion.widget.g$b r7 = r6.f
            java.util.ArrayList<androidx.constraintlayout.motion.widget.g$b> r3 = r6.g
            java.util.Iterator r3 = r3.iterator()
        L73:
            boolean r4 = r3.hasNext()
            if (r4 == 0) goto L87
            java.lang.Object r4 = r3.next()
            androidx.constraintlayout.motion.widget.g$b r4 = (androidx.constraintlayout.motion.widget.g.b) r4
            int r5 = androidx.constraintlayout.motion.widget.g.b.a(r4)
            if (r5 != r8) goto L73
            r7 = r4
            goto L73
        L87:
            androidx.constraintlayout.motion.widget.g$b r8 = new androidx.constraintlayout.motion.widget.g$b
            r8.<init>(r6, r7)
            androidx.constraintlayout.motion.widget.g.b.d(r8, r0)
            androidx.constraintlayout.motion.widget.g.b.b(r8, r2)
            if (r0 == r1) goto L99
            java.util.ArrayList<androidx.constraintlayout.motion.widget.g$b> r7 = r6.e
            r7.add(r8)
        L99:
            r6.c = r8
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: androidx.constraintlayout.motion.widget.g.X(int, int):void");
    }

    public void Y(b bVar) {
        this.c = bVar;
        if (bVar == null || bVar.l == null) {
            return;
        }
        this.c.l.w(this.r);
    }

    public void Z() {
        b bVar = this.c;
        if (bVar == null || bVar.l == null) {
            return;
        }
        this.c.l.z();
    }

    public boolean b0() {
        Iterator<b> it = this.e.iterator();
        while (it.hasNext()) {
            if (it.next().l != null) {
                return true;
            }
        }
        b bVar = this.c;
        return (bVar == null || bVar.l == null) ? false : true;
    }

    public void c0(int i, View... viewArr) {
        this.s.i(i, viewArr);
    }

    public void f(MotionLayout motionLayout, int i) {
        Iterator<b> it = this.e.iterator();
        while (it.hasNext()) {
            b next = it.next();
            if (next.m.size() > 0) {
                Iterator it2 = next.m.iterator();
                while (it2.hasNext()) {
                    ((b.a) it2.next()).c(motionLayout);
                }
            }
        }
        Iterator<b> it3 = this.g.iterator();
        while (it3.hasNext()) {
            b next2 = it3.next();
            if (next2.m.size() > 0) {
                Iterator it4 = next2.m.iterator();
                while (it4.hasNext()) {
                    ((b.a) it4.next()).c(motionLayout);
                }
            }
        }
        Iterator<b> it5 = this.e.iterator();
        while (it5.hasNext()) {
            b next3 = it5.next();
            if (next3.m.size() > 0) {
                Iterator it6 = next3.m.iterator();
                while (it6.hasNext()) {
                    ((b.a) it6.next()).a(motionLayout, i, next3);
                }
            }
        }
        Iterator<b> it7 = this.g.iterator();
        while (it7.hasNext()) {
            b next4 = it7.next();
            if (next4.m.size() > 0) {
                Iterator it8 = next4.m.iterator();
                while (it8.hasNext()) {
                    ((b.a) it8.next()).a(motionLayout, i, next4);
                }
            }
        }
    }

    public boolean g(int i, u92 u92Var) {
        return this.s.d(i, u92Var);
    }

    public boolean h(MotionLayout motionLayout, int i) {
        b bVar;
        if (J() || this.d) {
            return false;
        }
        Iterator<b> it = this.e.iterator();
        while (it.hasNext()) {
            b next = it.next();
            if (next.n != 0 && ((bVar = this.c) != next || !bVar.D(2))) {
                if (i != next.d || (next.n != 4 && next.n != 2)) {
                    if (i == next.c && (next.n == 3 || next.n == 1)) {
                        MotionLayout.TransitionState transitionState = MotionLayout.TransitionState.FINISHED;
                        motionLayout.setState(transitionState);
                        motionLayout.setTransition(next);
                        if (next.n == 3) {
                            motionLayout.v0();
                            motionLayout.setState(MotionLayout.TransitionState.SETUP);
                            motionLayout.setState(MotionLayout.TransitionState.MOVING);
                        } else {
                            motionLayout.setProgress(Utils.FLOAT_EPSILON);
                            motionLayout.Z(true);
                            motionLayout.setState(MotionLayout.TransitionState.SETUP);
                            motionLayout.setState(MotionLayout.TransitionState.MOVING);
                            motionLayout.setState(transitionState);
                            motionLayout.n0();
                        }
                        return true;
                    }
                } else {
                    MotionLayout.TransitionState transitionState2 = MotionLayout.TransitionState.FINISHED;
                    motionLayout.setState(transitionState2);
                    motionLayout.setTransition(next);
                    if (next.n == 4) {
                        motionLayout.t0();
                        motionLayout.setState(MotionLayout.TransitionState.SETUP);
                        motionLayout.setState(MotionLayout.TransitionState.MOVING);
                    } else {
                        motionLayout.setProgress(1.0f);
                        motionLayout.Z(true);
                        motionLayout.setState(MotionLayout.TransitionState.SETUP);
                        motionLayout.setState(MotionLayout.TransitionState.MOVING);
                        motionLayout.setState(transitionState2);
                        motionLayout.n0();
                    }
                    return true;
                }
            }
        }
        return false;
    }

    public b i(int i, float f, float f2, MotionEvent motionEvent) {
        if (i != -1) {
            List<b> H = H(i);
            float f3 = Utils.FLOAT_EPSILON;
            b bVar = null;
            RectF rectF = new RectF();
            for (b bVar2 : H) {
                if (!bVar2.o && bVar2.l != null) {
                    bVar2.l.w(this.r);
                    RectF p = bVar2.l.p(this.a, rectF);
                    if (p == null || motionEvent == null || p.contains(motionEvent.getX(), motionEvent.getY())) {
                        RectF f4 = bVar2.l.f(this.a, rectF);
                        if (f4 == null || motionEvent == null || f4.contains(motionEvent.getX(), motionEvent.getY())) {
                            float a2 = bVar2.l.a(f, f2);
                            if (bVar2.l.l && motionEvent != null) {
                                float x = motionEvent.getX() - bVar2.l.i;
                                float y = motionEvent.getY() - bVar2.l.j;
                                a2 = ((float) (Math.atan2(f2 + y, f + x) - Math.atan2(x, y))) * 10.0f;
                            }
                            float f5 = a2 * (bVar2.c == i ? -1.0f : 1.1f);
                            if (f5 > f3) {
                                bVar = bVar2;
                                f3 = f5;
                            }
                        }
                    }
                }
            }
            return bVar;
        }
        return this.c;
    }

    public int j() {
        b bVar = this.c;
        if (bVar != null) {
            return bVar.p;
        }
        return -1;
    }

    public int k() {
        b bVar = this.c;
        if (bVar == null || bVar.l == null) {
            return 0;
        }
        return this.c.l.d();
    }

    public androidx.constraintlayout.widget.a l(int i) {
        return m(i, -1, -1);
    }

    public androidx.constraintlayout.widget.a m(int i, int i2, int i3) {
        int c;
        if (this.k) {
            PrintStream printStream = System.out;
            printStream.println("id " + i);
            PrintStream printStream2 = System.out;
            printStream2.println("size " + this.h.size());
        }
        ct3 ct3Var = this.b;
        if (ct3Var != null && (c = ct3Var.c(i, i2, i3)) != -1) {
            i = c;
        }
        if (this.h.get(i) == null) {
            StringBuilder sb = new StringBuilder();
            sb.append("Warning could not find ConstraintSet id/");
            sb.append(xe0.c(this.a.getContext(), i));
            sb.append(" In MotionScene");
            SparseArray<androidx.constraintlayout.widget.a> sparseArray = this.h;
            return sparseArray.get(sparseArray.keyAt(0));
        }
        return this.h.get(i);
    }

    public int[] n() {
        int size = this.h.size();
        int[] iArr = new int[size];
        for (int i = 0; i < size; i++) {
            iArr[i] = this.h.keyAt(i);
        }
        return iArr;
    }

    public ArrayList<b> o() {
        return this.e;
    }

    public int p() {
        b bVar = this.c;
        if (bVar != null) {
            return bVar.h;
        }
        return this.l;
    }

    public int q() {
        b bVar = this.c;
        if (bVar == null) {
            return -1;
        }
        return bVar.c;
    }

    public final int r(Context context, String str) {
        int i;
        if (str.contains("/")) {
            i = context.getResources().getIdentifier(str.substring(str.indexOf(47) + 1), "id", context.getPackageName());
            if (this.k) {
                System.out.println("id getMap res = " + i);
            }
        } else {
            i = -1;
        }
        return (i != -1 || str.length() <= 1) ? i : Integer.parseInt(str.substring(1));
    }

    public Interpolator s() {
        int i = this.c.e;
        if (i != -2) {
            if (i != -1) {
                if (i != 0) {
                    if (i != 1) {
                        if (i != 2) {
                            if (i != 4) {
                                if (i != 5) {
                                    if (i != 6) {
                                        return null;
                                    }
                                    return new AnticipateInterpolator();
                                }
                                return new OvershootInterpolator();
                            }
                            return new BounceInterpolator();
                        }
                        return new DecelerateInterpolator();
                    }
                    return new AccelerateInterpolator();
                }
                return new AccelerateDecelerateInterpolator();
            }
            return new a(this, yt0.c(this.c.f));
        }
        return AnimationUtils.loadInterpolator(this.a.getContext(), this.c.g);
    }

    public void t(u92 u92Var) {
        b bVar = this.c;
        if (bVar != null) {
            Iterator it = bVar.k.iterator();
            while (it.hasNext()) {
                ((jx1) it.next()).b(u92Var);
            }
            return;
        }
        b bVar2 = this.f;
        if (bVar2 != null) {
            Iterator it2 = bVar2.k.iterator();
            while (it2.hasNext()) {
                ((jx1) it2.next()).b(u92Var);
            }
        }
    }

    public float u() {
        b bVar = this.c;
        return (bVar == null || bVar.l == null) ? Utils.FLOAT_EPSILON : this.c.l.g();
    }

    public float v() {
        b bVar = this.c;
        return (bVar == null || bVar.l == null) ? Utils.FLOAT_EPSILON : this.c.l.h();
    }

    public boolean w() {
        b bVar = this.c;
        if (bVar == null || bVar.l == null) {
            return false;
        }
        return this.c.l.i();
    }

    public float x(float f, float f2) {
        b bVar = this.c;
        return (bVar == null || bVar.l == null) ? Utils.FLOAT_EPSILON : this.c.l.j(f, f2);
    }

    public final int y(int i) {
        int c;
        ct3 ct3Var = this.b;
        return (ct3Var == null || (c = ct3Var.c(i, -1, -1)) == -1) ? i : c;
    }

    public int z() {
        b bVar = this.c;
        if (bVar == null || bVar.l == null) {
            return 0;
        }
        return this.c.l.k();
    }

    /* compiled from: MotionScene.java */
    /* loaded from: classes.dex */
    public static class b {
        public int a;
        public boolean b;
        public int c;
        public int d;
        public int e;
        public String f;
        public int g;
        public int h;
        public float i;
        public final g j;
        public ArrayList<jx1> k;
        public h l;
        public ArrayList<a> m;
        public int n;
        public boolean o;
        public int p;
        public int q;
        public int r;

        /* compiled from: MotionScene.java */
        /* loaded from: classes.dex */
        public static class a implements View.OnClickListener {
            public final b a;
            public int f0;
            public int g0;

            public a(Context context, b bVar, XmlPullParser xmlPullParser) {
                this.f0 = -1;
                this.g0 = 17;
                this.a = bVar;
                TypedArray obtainStyledAttributes = context.obtainStyledAttributes(Xml.asAttributeSet(xmlPullParser), w23.OnClick);
                int indexCount = obtainStyledAttributes.getIndexCount();
                for (int i = 0; i < indexCount; i++) {
                    int index = obtainStyledAttributes.getIndex(i);
                    if (index == w23.OnClick_targetId) {
                        this.f0 = obtainStyledAttributes.getResourceId(index, this.f0);
                    } else if (index == w23.OnClick_clickAction) {
                        this.g0 = obtainStyledAttributes.getInt(index, this.g0);
                    }
                }
                obtainStyledAttributes.recycle();
            }

            /* JADX WARN: Multi-variable type inference failed */
            /* JADX WARN: Type inference failed for: r7v3, types: [android.view.View] */
            public void a(MotionLayout motionLayout, int i, b bVar) {
                int i2 = this.f0;
                MotionLayout motionLayout2 = motionLayout;
                if (i2 != -1) {
                    motionLayout2 = motionLayout.findViewById(i2);
                }
                if (motionLayout2 != null) {
                    int i3 = bVar.d;
                    int i4 = bVar.c;
                    if (i3 == -1) {
                        motionLayout2.setOnClickListener(this);
                        return;
                    }
                    int i5 = this.g0;
                    boolean z = false;
                    boolean z2 = ((i5 & 1) != 0 && i == i3) | ((i5 & 1) != 0 && i == i3) | ((i5 & 256) != 0 && i == i3) | ((i5 & 16) != 0 && i == i4);
                    if ((i5 & 4096) != 0 && i == i4) {
                        z = true;
                    }
                    if (z2 || z) {
                        motionLayout2.setOnClickListener(this);
                        return;
                    }
                    return;
                }
                StringBuilder sb = new StringBuilder();
                sb.append("OnClick could not find id ");
                sb.append(this.f0);
            }

            public boolean b(b bVar, MotionLayout motionLayout) {
                b bVar2 = this.a;
                if (bVar2 == bVar) {
                    return true;
                }
                int i = bVar2.c;
                int i2 = this.a.d;
                if (i2 == -1) {
                    return motionLayout.D0 != i;
                }
                int i3 = motionLayout.D0;
                return i3 == i2 || i3 == i;
            }

            public void c(MotionLayout motionLayout) {
                int i = this.f0;
                if (i == -1) {
                    return;
                }
                View findViewById = motionLayout.findViewById(i);
                if (findViewById == null) {
                    StringBuilder sb = new StringBuilder();
                    sb.append(" (*)  could not find id ");
                    sb.append(this.f0);
                    return;
                }
                findViewById.setOnClickListener(null);
            }

            /* JADX WARN: Removed duplicated region for block: B:44:0x00a3  */
            /* JADX WARN: Removed duplicated region for block: B:61:? A[RETURN, SYNTHETIC] */
            @Override // android.view.View.OnClickListener
            /*
                Code decompiled incorrectly, please refer to instructions dump.
                To view partially-correct code enable 'Show inconsistent code' option in preferences
            */
            public void onClick(android.view.View r8) {
                /*
                    Method dump skipped, instructions count: 233
                    To view this dump change 'Code comments level' option to 'DEBUG'
                */
                throw new UnsupportedOperationException("Method not decompiled: androidx.constraintlayout.motion.widget.g.b.a.onClick(android.view.View):void");
            }
        }

        public b(g gVar, b bVar) {
            this.a = -1;
            this.b = false;
            this.c = -1;
            this.d = -1;
            this.e = 0;
            this.f = null;
            this.g = -1;
            this.h = 400;
            this.i = Utils.FLOAT_EPSILON;
            this.k = new ArrayList<>();
            this.l = null;
            this.m = new ArrayList<>();
            this.n = 0;
            this.o = false;
            this.p = -1;
            this.q = 0;
            this.r = 0;
            this.j = gVar;
            this.h = gVar.l;
            if (bVar != null) {
                this.p = bVar.p;
                this.e = bVar.e;
                this.f = bVar.f;
                this.g = bVar.g;
                this.h = bVar.h;
                this.k = bVar.k;
                this.i = bVar.i;
                this.q = bVar.q;
            }
        }

        public int A() {
            return this.d;
        }

        public h B() {
            return this.l;
        }

        public boolean C() {
            return !this.o;
        }

        public boolean D(int i) {
            return (i & this.r) != 0;
        }

        public void E(int i) {
            this.h = Math.max(i, 8);
        }

        public void F(boolean z) {
            this.o = !z;
        }

        public void G(int i, String str, int i2) {
            this.e = i;
            this.f = str;
            this.g = i2;
        }

        public void H(int i) {
            h B = B();
            if (B != null) {
                B.x(i);
            }
        }

        public void I(int i) {
            this.p = i;
        }

        public void t(jx1 jx1Var) {
            this.k.add(jx1Var);
        }

        public void u(Context context, XmlPullParser xmlPullParser) {
            this.m.add(new a(context, this, xmlPullParser));
        }

        public final void v(g gVar, Context context, TypedArray typedArray) {
            int indexCount = typedArray.getIndexCount();
            for (int i = 0; i < indexCount; i++) {
                int index = typedArray.getIndex(i);
                if (index == w23.Transition_constraintSetEnd) {
                    this.c = typedArray.getResourceId(index, -1);
                    String resourceTypeName = context.getResources().getResourceTypeName(this.c);
                    if ("layout".equals(resourceTypeName)) {
                        androidx.constraintlayout.widget.a aVar = new androidx.constraintlayout.widget.a();
                        aVar.D(context, this.c);
                        gVar.h.append(this.c, aVar);
                    } else if ("xml".equals(resourceTypeName)) {
                        this.c = gVar.M(context, this.c);
                    }
                } else if (index == w23.Transition_constraintSetStart) {
                    this.d = typedArray.getResourceId(index, this.d);
                    String resourceTypeName2 = context.getResources().getResourceTypeName(this.d);
                    if ("layout".equals(resourceTypeName2)) {
                        androidx.constraintlayout.widget.a aVar2 = new androidx.constraintlayout.widget.a();
                        aVar2.D(context, this.d);
                        gVar.h.append(this.d, aVar2);
                    } else if ("xml".equals(resourceTypeName2)) {
                        this.d = gVar.M(context, this.d);
                    }
                } else if (index == w23.Transition_motionInterpolator) {
                    int i2 = typedArray.peekValue(index).type;
                    if (i2 == 1) {
                        int resourceId = typedArray.getResourceId(index, -1);
                        this.g = resourceId;
                        if (resourceId != -1) {
                            this.e = -2;
                        }
                    } else if (i2 == 3) {
                        String string = typedArray.getString(index);
                        this.f = string;
                        if (string != null) {
                            if (string.indexOf("/") > 0) {
                                this.g = typedArray.getResourceId(index, -1);
                                this.e = -2;
                            } else {
                                this.e = -1;
                            }
                        }
                    } else {
                        this.e = typedArray.getInteger(index, this.e);
                    }
                } else if (index == w23.Transition_duration) {
                    int i3 = typedArray.getInt(index, this.h);
                    this.h = i3;
                    if (i3 < 8) {
                        this.h = 8;
                    }
                } else if (index == w23.Transition_staggered) {
                    this.i = typedArray.getFloat(index, this.i);
                } else if (index == w23.Transition_autoTransition) {
                    this.n = typedArray.getInteger(index, this.n);
                } else if (index == w23.Transition_android_id) {
                    this.a = typedArray.getResourceId(index, this.a);
                } else if (index == w23.Transition_transitionDisable) {
                    this.o = typedArray.getBoolean(index, this.o);
                } else if (index == w23.Transition_pathMotionArc) {
                    this.p = typedArray.getInteger(index, -1);
                } else if (index == w23.Transition_layoutDuringTransition) {
                    this.q = typedArray.getInteger(index, 0);
                } else if (index == w23.Transition_transitionFlags) {
                    this.r = typedArray.getInteger(index, 0);
                }
            }
            if (this.d == -1) {
                this.b = true;
            }
        }

        public final void w(g gVar, Context context, AttributeSet attributeSet) {
            TypedArray obtainStyledAttributes = context.obtainStyledAttributes(attributeSet, w23.Transition);
            v(gVar, context, obtainStyledAttributes);
            obtainStyledAttributes.recycle();
        }

        public int x() {
            return this.n;
        }

        public int y() {
            return this.c;
        }

        public int z() {
            return this.q;
        }

        public b(int i, g gVar, int i2, int i3) {
            this.a = -1;
            this.b = false;
            this.c = -1;
            this.d = -1;
            this.e = 0;
            this.f = null;
            this.g = -1;
            this.h = 400;
            this.i = Utils.FLOAT_EPSILON;
            this.k = new ArrayList<>();
            this.l = null;
            this.m = new ArrayList<>();
            this.n = 0;
            this.o = false;
            this.p = -1;
            this.q = 0;
            this.r = 0;
            this.a = i;
            this.j = gVar;
            this.d = i2;
            this.c = i3;
            this.h = gVar.l;
            this.q = gVar.m;
        }

        public b(g gVar, Context context, XmlPullParser xmlPullParser) {
            this.a = -1;
            this.b = false;
            this.c = -1;
            this.d = -1;
            this.e = 0;
            this.f = null;
            this.g = -1;
            this.h = 400;
            this.i = Utils.FLOAT_EPSILON;
            this.k = new ArrayList<>();
            this.l = null;
            this.m = new ArrayList<>();
            this.n = 0;
            this.o = false;
            this.p = -1;
            this.q = 0;
            this.r = 0;
            this.h = gVar.l;
            this.q = gVar.m;
            this.j = gVar;
            w(gVar, context, Xml.asAttributeSet(xmlPullParser));
        }
    }
}
