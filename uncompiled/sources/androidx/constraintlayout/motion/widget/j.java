package androidx.constraintlayout.motion.widget;

import android.graphics.Rect;
import android.view.MotionEvent;
import android.view.View;
import androidx.constraintlayout.motion.widget.i;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.constraintlayout.widget.b;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.Iterator;

/* compiled from: ViewTransitionController.java */
/* loaded from: classes.dex */
public class j {
    public final MotionLayout a;
    public HashSet<View> c;
    public ArrayList<i.b> e;
    public ArrayList<i> b = new ArrayList<>();
    public String d = "ViewTransitionController";
    public ArrayList<i.b> f = new ArrayList<>();

    /* compiled from: ViewTransitionController.java */
    /* loaded from: classes.dex */
    public class a implements b.a {
        public a(j jVar, i iVar, int i, boolean z, int i2) {
        }
    }

    public j(MotionLayout motionLayout) {
        this.a = motionLayout;
    }

    public void a(i iVar) {
        this.b.add(iVar);
        this.c = null;
        if (iVar.i() == 4) {
            f(iVar, true);
        } else if (iVar.i() == 5) {
            f(iVar, false);
        }
    }

    public void b(i.b bVar) {
        if (this.e == null) {
            this.e = new ArrayList<>();
        }
        this.e.add(bVar);
    }

    public void c() {
        ArrayList<i.b> arrayList = this.e;
        if (arrayList == null) {
            return;
        }
        Iterator<i.b> it = arrayList.iterator();
        while (it.hasNext()) {
            it.next().a();
        }
        this.e.removeAll(this.f);
        this.f.clear();
        if (this.e.isEmpty()) {
            this.e = null;
        }
    }

    public boolean d(int i, u92 u92Var) {
        Iterator<i> it = this.b.iterator();
        while (it.hasNext()) {
            i next = it.next();
            if (next.e() == i) {
                next.f.a(u92Var);
                return true;
            }
        }
        return false;
    }

    public void e() {
        this.a.invalidate();
    }

    public final void f(i iVar, boolean z) {
        ConstraintLayout.getSharedValues().a(iVar.h(), new a(this, iVar, iVar.h(), z, iVar.g()));
    }

    public void g(i.b bVar) {
        this.f.add(bVar);
    }

    public void h(MotionEvent motionEvent) {
        i iVar;
        int currentState = this.a.getCurrentState();
        if (currentState == -1) {
            return;
        }
        if (this.c == null) {
            this.c = new HashSet<>();
            Iterator<i> it = this.b.iterator();
            while (it.hasNext()) {
                i next = it.next();
                int childCount = this.a.getChildCount();
                for (int i = 0; i < childCount; i++) {
                    View childAt = this.a.getChildAt(i);
                    if (next.k(childAt)) {
                        childAt.getId();
                        this.c.add(childAt);
                    }
                }
            }
        }
        float x = motionEvent.getX();
        float y = motionEvent.getY();
        Rect rect = new Rect();
        int action = motionEvent.getAction();
        ArrayList<i.b> arrayList = this.e;
        if (arrayList != null && !arrayList.isEmpty()) {
            Iterator<i.b> it2 = this.e.iterator();
            while (it2.hasNext()) {
                it2.next().d(action, x, y);
            }
        }
        if (action == 0 || action == 1) {
            androidx.constraintlayout.widget.a f0 = this.a.f0(currentState);
            Iterator<i> it3 = this.b.iterator();
            while (it3.hasNext()) {
                i next2 = it3.next();
                if (next2.m(action)) {
                    Iterator<View> it4 = this.c.iterator();
                    while (it4.hasNext()) {
                        View next3 = it4.next();
                        if (next2.k(next3)) {
                            next3.getHitRect(rect);
                            if (rect.contains((int) x, (int) y)) {
                                iVar = next2;
                                next2.c(this, this.a, currentState, f0, next3);
                            } else {
                                iVar = next2;
                            }
                            next2 = iVar;
                        }
                    }
                }
            }
        }
    }

    public void i(int i, View... viewArr) {
        ArrayList arrayList = new ArrayList();
        Iterator<i> it = this.b.iterator();
        while (it.hasNext()) {
            i next = it.next();
            if (next.e() == i) {
                for (View view : viewArr) {
                    if (next.d(view)) {
                        arrayList.add(view);
                    }
                }
                if (!arrayList.isEmpty()) {
                    j(next, (View[]) arrayList.toArray(new View[0]));
                    arrayList.clear();
                }
            }
        }
    }

    public final void j(i iVar, View... viewArr) {
        int currentState = this.a.getCurrentState();
        if (iVar.e == 2) {
            iVar.c(this, this.a, currentState, null, viewArr);
        } else if (currentState == -1) {
            StringBuilder sb = new StringBuilder();
            sb.append("No support for ViewTransition within transition yet. Currently: ");
            sb.append(this.a.toString());
        } else {
            androidx.constraintlayout.widget.a f0 = this.a.f0(currentState);
            if (f0 == null) {
                return;
            }
            iVar.c(this, this.a, currentState, f0, viewArr);
        }
    }
}
