package androidx.constraintlayout.motion.widget;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.RectF;
import android.util.AttributeSet;
import android.util.Xml;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import androidx.constraintlayout.motion.widget.MotionLayout;
import androidx.core.widget.NestedScrollView;
import com.github.mikephil.charting.utils.Utils;
import org.xmlpull.v1.XmlPullParser;

/* compiled from: TouchResponse.java */
/* loaded from: classes.dex */
public class h {
    public static final float[][] G = {new float[]{0.5f, Utils.FLOAT_EPSILON}, new float[]{Utils.FLOAT_EPSILON, 0.5f}, new float[]{1.0f, 0.5f}, new float[]{0.5f, 1.0f}, new float[]{0.5f, 0.5f}, new float[]{Utils.FLOAT_EPSILON, 0.5f}, new float[]{1.0f, 0.5f}};
    public static final float[][] H = {new float[]{Utils.FLOAT_EPSILON, -1.0f}, new float[]{Utils.FLOAT_EPSILON, 1.0f}, new float[]{-1.0f, Utils.FLOAT_EPSILON}, new float[]{1.0f, Utils.FLOAT_EPSILON}, new float[]{-1.0f, Utils.FLOAT_EPSILON}, new float[]{1.0f, Utils.FLOAT_EPSILON}};
    public float r;
    public float s;
    public final MotionLayout t;
    public int a = 0;
    public int b = 0;
    public int c = 0;
    public int d = -1;
    public int e = -1;
    public int f = -1;
    public float g = 0.5f;
    public float h = 0.5f;
    public float i = 0.5f;
    public float j = 0.5f;
    public int k = -1;
    public boolean l = false;
    public float m = Utils.FLOAT_EPSILON;
    public float n = 1.0f;
    public boolean o = false;
    public float[] p = new float[2];
    public int[] q = new int[2];
    public float u = 4.0f;
    public float v = 1.2f;
    public boolean w = true;
    public float x = 1.0f;
    public int y = 0;
    public float z = 10.0f;
    public float A = 10.0f;
    public float B = 1.0f;
    public float C = Float.NaN;
    public float D = Float.NaN;
    public int E = 0;
    public int F = 0;

    /* compiled from: TouchResponse.java */
    /* loaded from: classes.dex */
    public class a implements View.OnTouchListener {
        public a(h hVar) {
        }

        @Override // android.view.View.OnTouchListener
        public boolean onTouch(View view, MotionEvent motionEvent) {
            return false;
        }
    }

    /* compiled from: TouchResponse.java */
    /* loaded from: classes.dex */
    public class b implements NestedScrollView.b {
        public b(h hVar) {
        }

        @Override // androidx.core.widget.NestedScrollView.b
        public void a(NestedScrollView nestedScrollView, int i, int i2, int i3, int i4) {
        }
    }

    public h(Context context, MotionLayout motionLayout, XmlPullParser xmlPullParser) {
        this.t = motionLayout;
        c(context, Xml.asAttributeSet(xmlPullParser));
    }

    public float a(float f, float f2) {
        return (f * this.m) + (f2 * this.n);
    }

    public final void b(TypedArray typedArray) {
        int indexCount = typedArray.getIndexCount();
        for (int i = 0; i < indexCount; i++) {
            int index = typedArray.getIndex(i);
            if (index == w23.OnSwipe_touchAnchorId) {
                this.d = typedArray.getResourceId(index, this.d);
            } else if (index == w23.OnSwipe_touchAnchorSide) {
                int i2 = typedArray.getInt(index, this.a);
                this.a = i2;
                float[][] fArr = G;
                this.h = fArr[i2][0];
                this.g = fArr[i2][1];
            } else if (index == w23.OnSwipe_dragDirection) {
                int i3 = typedArray.getInt(index, this.b);
                this.b = i3;
                float[][] fArr2 = H;
                if (i3 < fArr2.length) {
                    this.m = fArr2[i3][0];
                    this.n = fArr2[i3][1];
                } else {
                    this.n = Float.NaN;
                    this.m = Float.NaN;
                    this.l = true;
                }
            } else if (index == w23.OnSwipe_maxVelocity) {
                this.u = typedArray.getFloat(index, this.u);
            } else if (index == w23.OnSwipe_maxAcceleration) {
                this.v = typedArray.getFloat(index, this.v);
            } else if (index == w23.OnSwipe_moveWhenScrollAtTop) {
                this.w = typedArray.getBoolean(index, this.w);
            } else if (index == w23.OnSwipe_dragScale) {
                this.x = typedArray.getFloat(index, this.x);
            } else if (index == w23.OnSwipe_dragThreshold) {
                this.z = typedArray.getFloat(index, this.z);
            } else if (index == w23.OnSwipe_touchRegionId) {
                this.e = typedArray.getResourceId(index, this.e);
            } else if (index == w23.OnSwipe_onTouchUp) {
                this.c = typedArray.getInt(index, this.c);
            } else if (index == w23.OnSwipe_nestedScrollFlags) {
                this.y = typedArray.getInteger(index, 0);
            } else if (index == w23.OnSwipe_limitBoundsTo) {
                this.f = typedArray.getResourceId(index, 0);
            } else if (index == w23.OnSwipe_rotationCenterId) {
                this.k = typedArray.getResourceId(index, this.k);
            } else if (index == w23.OnSwipe_springDamping) {
                this.A = typedArray.getFloat(index, this.A);
            } else if (index == w23.OnSwipe_springMass) {
                this.B = typedArray.getFloat(index, this.B);
            } else if (index == w23.OnSwipe_springStiffness) {
                this.C = typedArray.getFloat(index, this.C);
            } else if (index == w23.OnSwipe_springStopThreshold) {
                this.D = typedArray.getFloat(index, this.D);
            } else if (index == w23.OnSwipe_springBoundary) {
                this.E = typedArray.getInt(index, this.E);
            } else if (index == w23.OnSwipe_autoCompleteMode) {
                this.F = typedArray.getInt(index, this.F);
            }
        }
    }

    public final void c(Context context, AttributeSet attributeSet) {
        TypedArray obtainStyledAttributes = context.obtainStyledAttributes(attributeSet, w23.OnSwipe);
        b(obtainStyledAttributes);
        obtainStyledAttributes.recycle();
    }

    public int d() {
        return this.F;
    }

    public int e() {
        return this.y;
    }

    public RectF f(ViewGroup viewGroup, RectF rectF) {
        View findViewById;
        int i = this.f;
        if (i == -1 || (findViewById = viewGroup.findViewById(i)) == null) {
            return null;
        }
        rectF.set(findViewById.getLeft(), findViewById.getTop(), findViewById.getRight(), findViewById.getBottom());
        return rectF;
    }

    public float g() {
        return this.v;
    }

    public float h() {
        return this.u;
    }

    public boolean i() {
        return this.w;
    }

    public float j(float f, float f2) {
        this.t.e0(this.d, this.t.getProgress(), this.h, this.g, this.p);
        float f3 = this.m;
        if (f3 != Utils.FLOAT_EPSILON) {
            float[] fArr = this.p;
            if (fArr[0] == Utils.FLOAT_EPSILON) {
                fArr[0] = 1.0E-7f;
            }
            return (f * f3) / fArr[0];
        }
        float[] fArr2 = this.p;
        if (fArr2[1] == Utils.FLOAT_EPSILON) {
            fArr2[1] = 1.0E-7f;
        }
        return (f2 * this.n) / fArr2[1];
    }

    public int k() {
        return this.E;
    }

    public float l() {
        return this.A;
    }

    public float m() {
        return this.B;
    }

    public float n() {
        return this.C;
    }

    public float o() {
        return this.D;
    }

    public RectF p(ViewGroup viewGroup, RectF rectF) {
        View findViewById;
        int i = this.e;
        if (i == -1 || (findViewById = viewGroup.findViewById(i)) == null) {
            return null;
        }
        rectF.set(findViewById.getLeft(), findViewById.getTop(), findViewById.getRight(), findViewById.getBottom());
        return rectF;
    }

    public int q() {
        return this.e;
    }

    public void r(MotionEvent motionEvent, MotionLayout.g gVar, int i, g gVar2) {
        float f;
        int i2;
        float f2;
        if (this.l) {
            s(motionEvent, gVar, i, gVar2);
            return;
        }
        gVar.a(motionEvent);
        int action = motionEvent.getAction();
        if (action == 0) {
            this.r = motionEvent.getRawX();
            this.s = motionEvent.getRawY();
            this.o = false;
        } else if (action == 1) {
            this.o = false;
            gVar.e(1000);
            float d = gVar.d();
            float c = gVar.c();
            float progress = this.t.getProgress();
            int i3 = this.d;
            if (i3 != -1) {
                this.t.e0(i3, progress, this.h, this.g, this.p);
            } else {
                float min = Math.min(this.t.getWidth(), this.t.getHeight());
                float[] fArr = this.p;
                fArr[1] = this.n * min;
                fArr[0] = min * this.m;
            }
            float f3 = this.m;
            float[] fArr2 = this.p;
            float f4 = fArr2[0];
            float f5 = fArr2[1];
            if (f3 != Utils.FLOAT_EPSILON) {
                f = d / fArr2[0];
            } else {
                f = c / fArr2[1];
            }
            float f6 = !Float.isNaN(f) ? (f / 3.0f) + progress : progress;
            if (f6 == Utils.FLOAT_EPSILON || f6 == 1.0f || (i2 = this.c) == 3) {
                if (Utils.FLOAT_EPSILON >= f6 || 1.0f <= f6) {
                    this.t.setState(MotionLayout.TransitionState.FINISHED);
                    return;
                }
                return;
            }
            float f7 = ((double) f6) < 0.5d ? 0.0f : 1.0f;
            if (i2 == 6) {
                if (progress + f < Utils.FLOAT_EPSILON) {
                    f = Math.abs(f);
                }
                f7 = 1.0f;
            }
            if (this.c == 7) {
                if (progress + f > 1.0f) {
                    f = -Math.abs(f);
                }
                f7 = 0.0f;
            }
            this.t.s0(this.c, f7, f);
            if (Utils.FLOAT_EPSILON >= progress || 1.0f <= progress) {
                this.t.setState(MotionLayout.TransitionState.FINISHED);
            }
        } else if (action != 2) {
        } else {
            float rawY = motionEvent.getRawY() - this.s;
            float rawX = motionEvent.getRawX() - this.r;
            if (Math.abs((this.m * rawX) + (this.n * rawY)) > this.z || this.o) {
                float progress2 = this.t.getProgress();
                if (!this.o) {
                    this.o = true;
                    this.t.setProgress(progress2);
                }
                int i4 = this.d;
                if (i4 != -1) {
                    this.t.e0(i4, progress2, this.h, this.g, this.p);
                } else {
                    float min2 = Math.min(this.t.getWidth(), this.t.getHeight());
                    float[] fArr3 = this.p;
                    fArr3[1] = this.n * min2;
                    fArr3[0] = min2 * this.m;
                }
                float f8 = this.m;
                float[] fArr4 = this.p;
                if (Math.abs(((f8 * fArr4[0]) + (this.n * fArr4[1])) * this.x) < 0.01d) {
                    float[] fArr5 = this.p;
                    fArr5[0] = 0.01f;
                    fArr5[1] = 0.01f;
                }
                if (this.m != Utils.FLOAT_EPSILON) {
                    f2 = rawX / this.p[0];
                } else {
                    f2 = rawY / this.p[1];
                }
                float max = Math.max(Math.min(progress2 + f2, 1.0f), (float) Utils.FLOAT_EPSILON);
                if (this.c == 6) {
                    max = Math.max(max, 0.01f);
                }
                if (this.c == 7) {
                    max = Math.min(max, 0.99f);
                }
                float progress3 = this.t.getProgress();
                if (max != progress3) {
                    int i5 = (progress3 > Utils.FLOAT_EPSILON ? 1 : (progress3 == Utils.FLOAT_EPSILON ? 0 : -1));
                    if (i5 == 0 || progress3 == 1.0f) {
                        this.t.Y(i5 == 0);
                    }
                    this.t.setProgress(max);
                    gVar.e(1000);
                    this.t.B0 = this.m != Utils.FLOAT_EPSILON ? gVar.d() / this.p[0] : gVar.c() / this.p[1];
                } else {
                    this.t.B0 = Utils.FLOAT_EPSILON;
                }
                this.r = motionEvent.getRawX();
                this.s = motionEvent.getRawY();
            }
        }
    }

    /* JADX WARN: Removed duplicated region for block: B:57:0x0269  */
    /* JADX WARN: Removed duplicated region for block: B:58:0x028d  */
    /* JADX WARN: Removed duplicated region for block: B:61:0x02aa  */
    /* JADX WARN: Removed duplicated region for block: B:62:0x02b6  */
    /*
        Code decompiled incorrectly, please refer to instructions dump.
        To view partially-correct code enable 'Show inconsistent code' option in preferences
    */
    public void s(android.view.MotionEvent r24, androidx.constraintlayout.motion.widget.MotionLayout.g r25, int r26, androidx.constraintlayout.motion.widget.g r27) {
        /*
            Method dump skipped, instructions count: 826
            To view this dump change 'Code comments level' option to 'DEBUG'
        */
        throw new UnsupportedOperationException("Method not decompiled: androidx.constraintlayout.motion.widget.h.s(android.view.MotionEvent, androidx.constraintlayout.motion.widget.MotionLayout$g, int, androidx.constraintlayout.motion.widget.g):void");
    }

    public void t(float f, float f2) {
        float f3;
        float progress = this.t.getProgress();
        if (!this.o) {
            this.o = true;
            this.t.setProgress(progress);
        }
        this.t.e0(this.d, progress, this.h, this.g, this.p);
        float f4 = this.m;
        float[] fArr = this.p;
        if (Math.abs((f4 * fArr[0]) + (this.n * fArr[1])) < 0.01d) {
            float[] fArr2 = this.p;
            fArr2[0] = 0.01f;
            fArr2[1] = 0.01f;
        }
        float f5 = this.m;
        if (f5 != Utils.FLOAT_EPSILON) {
            f3 = (f * f5) / this.p[0];
        } else {
            f3 = (f2 * this.n) / this.p[1];
        }
        float max = Math.max(Math.min(progress + f3, 1.0f), (float) Utils.FLOAT_EPSILON);
        if (max != this.t.getProgress()) {
            this.t.setProgress(max);
        }
    }

    public String toString() {
        if (Float.isNaN(this.m)) {
            return "rotation";
        }
        return this.m + " , " + this.n;
    }

    public void u(float f, float f2) {
        float f3;
        this.o = false;
        float progress = this.t.getProgress();
        this.t.e0(this.d, progress, this.h, this.g, this.p);
        float f4 = this.m;
        float[] fArr = this.p;
        float f5 = fArr[0];
        float f6 = this.n;
        float f7 = fArr[1];
        float f8 = Utils.FLOAT_EPSILON;
        if (f4 != Utils.FLOAT_EPSILON) {
            f3 = (f * f4) / fArr[0];
        } else {
            f3 = (f2 * f6) / fArr[1];
        }
        if (!Float.isNaN(f3)) {
            progress += f3 / 3.0f;
        }
        if (progress != Utils.FLOAT_EPSILON) {
            boolean z = progress != 1.0f;
            int i = this.c;
            if ((i != 3) && z) {
                MotionLayout motionLayout = this.t;
                if (progress >= 0.5d) {
                    f8 = 1.0f;
                }
                motionLayout.s0(i, f8, f3);
            }
        }
    }

    public void v(float f, float f2) {
        this.r = f;
        this.s = f2;
    }

    public void w(boolean z) {
        if (z) {
            float[][] fArr = H;
            fArr[4] = fArr[3];
            fArr[5] = fArr[2];
            float[][] fArr2 = G;
            fArr2[5] = fArr2[2];
            fArr2[6] = fArr2[1];
        } else {
            float[][] fArr3 = H;
            fArr3[4] = fArr3[2];
            fArr3[5] = fArr3[3];
            float[][] fArr4 = G;
            fArr4[5] = fArr4[1];
            fArr4[6] = fArr4[2];
        }
        float[][] fArr5 = G;
        int i = this.a;
        this.h = fArr5[i][0];
        this.g = fArr5[i][1];
        int i2 = this.b;
        float[][] fArr6 = H;
        if (i2 >= fArr6.length) {
            return;
        }
        this.m = fArr6[i2][0];
        this.n = fArr6[i2][1];
    }

    public void x(int i) {
        this.c = i;
    }

    public void y(float f, float f2) {
        this.r = f;
        this.s = f2;
        this.o = false;
    }

    public void z() {
        View view;
        int i = this.d;
        if (i != -1) {
            view = this.t.findViewById(i);
            if (view == null) {
                StringBuilder sb = new StringBuilder();
                sb.append("cannot find TouchAnchorId @id/");
                sb.append(xe0.c(this.t.getContext(), this.d));
            }
        } else {
            view = null;
        }
        if (view instanceof NestedScrollView) {
            NestedScrollView nestedScrollView = (NestedScrollView) view;
            nestedScrollView.setOnTouchListener(new a(this));
            nestedScrollView.setOnScrollChangeListener(new b(this));
        }
    }
}
