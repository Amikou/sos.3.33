package androidx.constraintlayout.widget;

import android.content.Context;
import android.content.res.Resources;
import android.content.res.TypedArray;
import android.graphics.Canvas;
import android.os.Build;
import android.util.AttributeSet;
import android.util.SparseArray;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewParent;
import androidx.constraintlayout.core.widgets.ConstraintWidget;
import androidx.constraintlayout.core.widgets.d;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.constraintlayout.widget.a;
import com.github.mikephil.charting.utils.Utils;
import java.util.Arrays;
import java.util.HashMap;

/* loaded from: classes.dex */
public abstract class ConstraintHelper extends View {
    public int[] a;
    public int f0;
    public Context g0;
    public lk1 h0;
    public boolean i0;
    public String j0;
    public String k0;
    public View[] l0;
    public HashMap<Integer, String> m0;

    public ConstraintHelper(Context context) {
        super(context);
        this.a = new int[32];
        this.i0 = false;
        this.l0 = null;
        this.m0 = new HashMap<>();
        this.g0 = context;
        o(null);
    }

    public final void e(String str) {
        if (str == null || str.length() == 0 || this.g0 == null) {
            return;
        }
        String trim = str.trim();
        if (getParent() instanceof ConstraintLayout) {
            ConstraintLayout constraintLayout = (ConstraintLayout) getParent();
        }
        int m = m(trim);
        if (m != 0) {
            this.m0.put(Integer.valueOf(m), trim);
            f(m);
            return;
        }
        StringBuilder sb = new StringBuilder();
        sb.append("Could not find id of \"");
        sb.append(trim);
        sb.append("\"");
    }

    public final void f(int i) {
        if (i == getId()) {
            return;
        }
        int i2 = this.f0 + 1;
        int[] iArr = this.a;
        if (i2 > iArr.length) {
            this.a = Arrays.copyOf(iArr, iArr.length * 2);
        }
        int[] iArr2 = this.a;
        int i3 = this.f0;
        iArr2[i3] = i;
        this.f0 = i3 + 1;
    }

    public final void g(String str) {
        if (str == null || str.length() == 0 || this.g0 == null) {
            return;
        }
        String trim = str.trim();
        ConstraintLayout constraintLayout = getParent() instanceof ConstraintLayout ? (ConstraintLayout) getParent() : null;
        if (constraintLayout == null) {
            return;
        }
        int childCount = constraintLayout.getChildCount();
        for (int i = 0; i < childCount; i++) {
            View childAt = constraintLayout.getChildAt(i);
            ViewGroup.LayoutParams layoutParams = childAt.getLayoutParams();
            if ((layoutParams instanceof ConstraintLayout.LayoutParams) && trim.equals(((ConstraintLayout.LayoutParams) layoutParams).X)) {
                if (childAt.getId() == -1) {
                    StringBuilder sb = new StringBuilder();
                    sb.append("to use ConstraintTag view ");
                    sb.append(childAt.getClass().getSimpleName());
                    sb.append(" must have an ID");
                } else {
                    f(childAt.getId());
                }
            }
        }
    }

    public int[] getReferencedIds() {
        return Arrays.copyOf(this.a, this.f0);
    }

    public void h() {
        ViewParent parent = getParent();
        if (parent == null || !(parent instanceof ConstraintLayout)) {
            return;
        }
        i((ConstraintLayout) parent);
    }

    public void i(ConstraintLayout constraintLayout) {
        int visibility = getVisibility();
        float elevation = Build.VERSION.SDK_INT >= 21 ? getElevation() : 0.0f;
        for (int i = 0; i < this.f0; i++) {
            View i2 = constraintLayout.i(this.a[i]);
            if (i2 != null) {
                i2.setVisibility(visibility);
                if (elevation > Utils.FLOAT_EPSILON && Build.VERSION.SDK_INT >= 21) {
                    i2.setTranslationZ(i2.getTranslationZ() + elevation);
                }
            }
        }
    }

    public void j(ConstraintLayout constraintLayout) {
    }

    public final int[] k(View view, String str) {
        String[] split = str.split(",");
        view.getContext();
        int[] iArr = new int[split.length];
        int i = 0;
        for (String str2 : split) {
            int m = m(str2.trim());
            if (m != 0) {
                iArr[i] = m;
                i++;
            }
        }
        return i != split.length ? Arrays.copyOf(iArr, i) : iArr;
    }

    public final int l(ConstraintLayout constraintLayout, String str) {
        Resources resources;
        if (str == null || constraintLayout == null || (resources = this.g0.getResources()) == null) {
            return 0;
        }
        int childCount = constraintLayout.getChildCount();
        for (int i = 0; i < childCount; i++) {
            View childAt = constraintLayout.getChildAt(i);
            if (childAt.getId() != -1) {
                String str2 = null;
                try {
                    str2 = resources.getResourceEntryName(childAt.getId());
                } catch (Resources.NotFoundException unused) {
                }
                if (str.equals(str2)) {
                    return childAt.getId();
                }
            }
        }
        return 0;
    }

    public final int m(String str) {
        ConstraintLayout constraintLayout = getParent() instanceof ConstraintLayout ? (ConstraintLayout) getParent() : null;
        int i = 0;
        if (isInEditMode() && constraintLayout != null) {
            Object g = constraintLayout.g(0, str);
            if (g instanceof Integer) {
                i = ((Integer) g).intValue();
            }
        }
        if (i == 0 && constraintLayout != null) {
            i = l(constraintLayout, str);
        }
        if (i == 0) {
            try {
                i = i03.class.getField(str).getInt(null);
            } catch (Exception unused) {
            }
        }
        return i == 0 ? this.g0.getResources().getIdentifier(str, "id", this.g0.getPackageName()) : i;
    }

    public View[] n(ConstraintLayout constraintLayout) {
        View[] viewArr = this.l0;
        if (viewArr == null || viewArr.length != this.f0) {
            this.l0 = new View[this.f0];
        }
        for (int i = 0; i < this.f0; i++) {
            this.l0[i] = constraintLayout.i(this.a[i]);
        }
        return this.l0;
    }

    public void o(AttributeSet attributeSet) {
        if (attributeSet != null) {
            TypedArray obtainStyledAttributes = getContext().obtainStyledAttributes(attributeSet, w23.ConstraintLayout_Layout);
            int indexCount = obtainStyledAttributes.getIndexCount();
            for (int i = 0; i < indexCount; i++) {
                int index = obtainStyledAttributes.getIndex(i);
                if (index == w23.ConstraintLayout_Layout_constraint_referenced_ids) {
                    String string = obtainStyledAttributes.getString(index);
                    this.j0 = string;
                    setIds(string);
                } else if (index == w23.ConstraintLayout_Layout_constraint_referenced_tags) {
                    String string2 = obtainStyledAttributes.getString(index);
                    this.k0 = string2;
                    setReferenceTags(string2);
                }
            }
            obtainStyledAttributes.recycle();
        }
    }

    @Override // android.view.View
    public void onAttachedToWindow() {
        super.onAttachedToWindow();
        String str = this.j0;
        if (str != null) {
            setIds(str);
        }
        String str2 = this.k0;
        if (str2 != null) {
            setReferenceTags(str2);
        }
    }

    @Override // android.view.View
    public void onDraw(Canvas canvas) {
    }

    @Override // android.view.View
    public void onMeasure(int i, int i2) {
        if (this.i0) {
            super.onMeasure(i, i2);
        } else {
            setMeasuredDimension(0, 0);
        }
    }

    public void p(a.C0021a c0021a, mk1 mk1Var, ConstraintLayout.LayoutParams layoutParams, SparseArray<ConstraintWidget> sparseArray) {
        a.b bVar = c0021a.e;
        int[] iArr = bVar.i0;
        if (iArr != null) {
            setReferencedIds(iArr);
        } else {
            String str = bVar.j0;
            if (str != null) {
                if (str.length() > 0) {
                    a.b bVar2 = c0021a.e;
                    bVar2.i0 = k(this, bVar2.j0);
                } else {
                    c0021a.e.i0 = null;
                }
            }
        }
        if (mk1Var == null) {
            return;
        }
        mk1Var.a();
        if (c0021a.e.i0 == null) {
            return;
        }
        int i = 0;
        while (true) {
            int[] iArr2 = c0021a.e.i0;
            if (i >= iArr2.length) {
                return;
            }
            ConstraintWidget constraintWidget = sparseArray.get(iArr2[i]);
            if (constraintWidget != null) {
                mk1Var.b(constraintWidget);
            }
            i++;
        }
    }

    public void q(ConstraintWidget constraintWidget, boolean z) {
    }

    public void r(ConstraintLayout constraintLayout) {
    }

    public void s(ConstraintLayout constraintLayout) {
    }

    public void setIds(String str) {
        this.j0 = str;
        if (str == null) {
            return;
        }
        int i = 0;
        this.f0 = 0;
        while (true) {
            int indexOf = str.indexOf(44, i);
            if (indexOf == -1) {
                e(str.substring(i));
                return;
            } else {
                e(str.substring(i, indexOf));
                i = indexOf + 1;
            }
        }
    }

    public void setReferenceTags(String str) {
        this.k0 = str;
        if (str == null) {
            return;
        }
        int i = 0;
        this.f0 = 0;
        while (true) {
            int indexOf = str.indexOf(44, i);
            if (indexOf == -1) {
                g(str.substring(i));
                return;
            } else {
                g(str.substring(i, indexOf));
                i = indexOf + 1;
            }
        }
    }

    public void setReferencedIds(int[] iArr) {
        this.j0 = null;
        this.f0 = 0;
        for (int i : iArr) {
            f(i);
        }
    }

    @Override // android.view.View
    public void setTag(int i, Object obj) {
        super.setTag(i, obj);
        if (obj == null && this.j0 == null) {
            f(i);
        }
    }

    public void t(ConstraintLayout constraintLayout) {
    }

    public void u(d dVar, lk1 lk1Var, SparseArray<ConstraintWidget> sparseArray) {
        lk1Var.a();
        for (int i = 0; i < this.f0; i++) {
            lk1Var.b(sparseArray.get(this.a[i]));
        }
    }

    public void v(ConstraintLayout constraintLayout) {
        String str;
        int l;
        if (isInEditMode()) {
            setIds(this.j0);
        }
        lk1 lk1Var = this.h0;
        if (lk1Var == null) {
            return;
        }
        lk1Var.a();
        for (int i = 0; i < this.f0; i++) {
            int i2 = this.a[i];
            View i3 = constraintLayout.i(i2);
            if (i3 == null && (l = l(constraintLayout, (str = this.m0.get(Integer.valueOf(i2))))) != 0) {
                this.a[i] = l;
                this.m0.put(Integer.valueOf(l), str);
                i3 = constraintLayout.i(l);
            }
            if (i3 != null) {
                this.h0.b(constraintLayout.j(i3));
            }
        }
        this.h0.c(constraintLayout.g0);
    }

    public void w() {
        if (this.h0 == null) {
            return;
        }
        ViewGroup.LayoutParams layoutParams = getLayoutParams();
        if (layoutParams instanceof ConstraintLayout.LayoutParams) {
            ((ConstraintLayout.LayoutParams) layoutParams).q0 = (ConstraintWidget) this.h0;
        }
    }

    public ConstraintHelper(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        this.a = new int[32];
        this.i0 = false;
        this.l0 = null;
        this.m0 = new HashMap<>();
        this.g0 = context;
        o(attributeSet);
    }

    public ConstraintHelper(Context context, AttributeSet attributeSet, int i) {
        super(context, attributeSet, i);
        this.a = new int[32];
        this.i0 = false;
        this.l0 = null;
        this.m0 = new HashMap<>();
        this.g0 = context;
        o(attributeSet);
    }
}
