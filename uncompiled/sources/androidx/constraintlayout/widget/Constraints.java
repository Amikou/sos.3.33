package androidx.constraintlayout.widget;

import android.content.Context;
import android.content.res.TypedArray;
import android.os.Build;
import android.util.AttributeSet;
import android.view.ViewGroup;
import androidx.constraintlayout.widget.ConstraintLayout;
import com.github.mikephil.charting.utils.Utils;

/* loaded from: classes.dex */
public class Constraints extends ViewGroup {
    public a a;

    public Constraints(Context context) {
        super(context);
        super.setVisibility(8);
    }

    @Override // android.view.ViewGroup
    /* renamed from: a */
    public LayoutParams generateDefaultLayoutParams() {
        return new LayoutParams(-2, -2);
    }

    @Override // android.view.ViewGroup
    /* renamed from: b */
    public LayoutParams generateLayoutParams(AttributeSet attributeSet) {
        return new LayoutParams(getContext(), attributeSet);
    }

    public final void c(AttributeSet attributeSet) {
    }

    public a getConstraintSet() {
        if (this.a == null) {
            this.a = new a();
        }
        this.a.r(this);
        return this.a;
    }

    @Override // android.view.ViewGroup, android.view.View
    public void onLayout(boolean z, int i, int i2, int i3, int i4) {
    }

    @Override // android.view.ViewGroup
    public ViewGroup.LayoutParams generateLayoutParams(ViewGroup.LayoutParams layoutParams) {
        return new ConstraintLayout.LayoutParams(layoutParams);
    }

    public Constraints(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        c(attributeSet);
        super.setVisibility(8);
    }

    public Constraints(Context context, AttributeSet attributeSet, int i) {
        super(context, attributeSet, i);
        c(attributeSet);
        super.setVisibility(8);
    }

    /* loaded from: classes.dex */
    public static class LayoutParams extends ConstraintLayout.LayoutParams {
        public float A0;
        public float B0;
        public float C0;
        public float D0;
        public float r0;
        public boolean s0;
        public float t0;
        public float u0;
        public float v0;
        public float w0;
        public float x0;
        public float y0;
        public float z0;

        public LayoutParams(int i, int i2) {
            super(i, i2);
            this.r0 = 1.0f;
            this.s0 = false;
            this.t0 = Utils.FLOAT_EPSILON;
            this.u0 = Utils.FLOAT_EPSILON;
            this.v0 = Utils.FLOAT_EPSILON;
            this.w0 = Utils.FLOAT_EPSILON;
            this.x0 = 1.0f;
            this.y0 = 1.0f;
            this.z0 = Utils.FLOAT_EPSILON;
            this.A0 = Utils.FLOAT_EPSILON;
            this.B0 = Utils.FLOAT_EPSILON;
            this.C0 = Utils.FLOAT_EPSILON;
            this.D0 = Utils.FLOAT_EPSILON;
        }

        public LayoutParams(Context context, AttributeSet attributeSet) {
            super(context, attributeSet);
            this.r0 = 1.0f;
            this.s0 = false;
            this.t0 = Utils.FLOAT_EPSILON;
            this.u0 = Utils.FLOAT_EPSILON;
            this.v0 = Utils.FLOAT_EPSILON;
            this.w0 = Utils.FLOAT_EPSILON;
            this.x0 = 1.0f;
            this.y0 = 1.0f;
            this.z0 = Utils.FLOAT_EPSILON;
            this.A0 = Utils.FLOAT_EPSILON;
            this.B0 = Utils.FLOAT_EPSILON;
            this.C0 = Utils.FLOAT_EPSILON;
            this.D0 = Utils.FLOAT_EPSILON;
            TypedArray obtainStyledAttributes = context.obtainStyledAttributes(attributeSet, w23.ConstraintSet);
            int indexCount = obtainStyledAttributes.getIndexCount();
            for (int i = 0; i < indexCount; i++) {
                int index = obtainStyledAttributes.getIndex(i);
                if (index == w23.ConstraintSet_android_alpha) {
                    this.r0 = obtainStyledAttributes.getFloat(index, this.r0);
                } else if (index == w23.ConstraintSet_android_elevation) {
                    if (Build.VERSION.SDK_INT >= 21) {
                        this.t0 = obtainStyledAttributes.getFloat(index, this.t0);
                        this.s0 = true;
                    }
                } else if (index == w23.ConstraintSet_android_rotationX) {
                    this.v0 = obtainStyledAttributes.getFloat(index, this.v0);
                } else if (index == w23.ConstraintSet_android_rotationY) {
                    this.w0 = obtainStyledAttributes.getFloat(index, this.w0);
                } else if (index == w23.ConstraintSet_android_rotation) {
                    this.u0 = obtainStyledAttributes.getFloat(index, this.u0);
                } else if (index == w23.ConstraintSet_android_scaleX) {
                    this.x0 = obtainStyledAttributes.getFloat(index, this.x0);
                } else if (index == w23.ConstraintSet_android_scaleY) {
                    this.y0 = obtainStyledAttributes.getFloat(index, this.y0);
                } else if (index == w23.ConstraintSet_android_transformPivotX) {
                    this.z0 = obtainStyledAttributes.getFloat(index, this.z0);
                } else if (index == w23.ConstraintSet_android_transformPivotY) {
                    this.A0 = obtainStyledAttributes.getFloat(index, this.A0);
                } else if (index == w23.ConstraintSet_android_translationX) {
                    this.B0 = obtainStyledAttributes.getFloat(index, this.B0);
                } else if (index == w23.ConstraintSet_android_translationY) {
                    this.C0 = obtainStyledAttributes.getFloat(index, this.C0);
                } else if (index == w23.ConstraintSet_android_translationZ && Build.VERSION.SDK_INT >= 21) {
                    this.D0 = obtainStyledAttributes.getFloat(index, this.D0);
                }
            }
            obtainStyledAttributes.recycle();
        }
    }
}
