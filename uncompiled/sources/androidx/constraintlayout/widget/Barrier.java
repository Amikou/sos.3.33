package androidx.constraintlayout.widget;

import android.content.Context;
import android.content.res.TypedArray;
import android.os.Build;
import android.util.AttributeSet;
import android.util.SparseArray;
import androidx.constraintlayout.core.widgets.ConstraintWidget;
import androidx.constraintlayout.core.widgets.d;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.constraintlayout.widget.a;

/* loaded from: classes.dex */
public class Barrier extends ConstraintHelper {
    public int n0;
    public int o0;
    public androidx.constraintlayout.core.widgets.a p0;

    public Barrier(Context context) {
        super(context);
        super.setVisibility(8);
    }

    public boolean getAllowsGoneWidget() {
        return this.p0.r1();
    }

    public int getMargin() {
        return this.p0.t1();
    }

    public int getType() {
        return this.n0;
    }

    @Override // androidx.constraintlayout.widget.ConstraintHelper
    public void o(AttributeSet attributeSet) {
        super.o(attributeSet);
        this.p0 = new androidx.constraintlayout.core.widgets.a();
        if (attributeSet != null) {
            TypedArray obtainStyledAttributes = getContext().obtainStyledAttributes(attributeSet, w23.ConstraintLayout_Layout);
            int indexCount = obtainStyledAttributes.getIndexCount();
            for (int i = 0; i < indexCount; i++) {
                int index = obtainStyledAttributes.getIndex(i);
                if (index == w23.ConstraintLayout_Layout_barrierDirection) {
                    setType(obtainStyledAttributes.getInt(index, 0));
                } else if (index == w23.ConstraintLayout_Layout_barrierAllowsGoneWidgets) {
                    this.p0.w1(obtainStyledAttributes.getBoolean(index, true));
                } else if (index == w23.ConstraintLayout_Layout_barrierMargin) {
                    this.p0.y1(obtainStyledAttributes.getDimensionPixelSize(index, 0));
                }
            }
            obtainStyledAttributes.recycle();
        }
        this.h0 = this.p0;
        w();
    }

    @Override // androidx.constraintlayout.widget.ConstraintHelper
    public void p(a.C0021a c0021a, mk1 mk1Var, ConstraintLayout.LayoutParams layoutParams, SparseArray<ConstraintWidget> sparseArray) {
        super.p(c0021a, mk1Var, layoutParams, sparseArray);
        if (mk1Var instanceof androidx.constraintlayout.core.widgets.a) {
            androidx.constraintlayout.core.widgets.a aVar = (androidx.constraintlayout.core.widgets.a) mk1Var;
            x(aVar, c0021a.e.f0, ((d) mk1Var.M()).M1());
            aVar.w1(c0021a.e.n0);
            aVar.y1(c0021a.e.g0);
        }
    }

    @Override // androidx.constraintlayout.widget.ConstraintHelper
    public void q(ConstraintWidget constraintWidget, boolean z) {
        x(constraintWidget, this.n0, z);
    }

    public void setAllowsGoneWidget(boolean z) {
        this.p0.w1(z);
    }

    public void setDpMargin(int i) {
        androidx.constraintlayout.core.widgets.a aVar = this.p0;
        aVar.y1((int) ((i * getResources().getDisplayMetrics().density) + 0.5f));
    }

    public void setMargin(int i) {
        this.p0.y1(i);
    }

    public void setType(int i) {
        this.n0 = i;
    }

    public final void x(ConstraintWidget constraintWidget, int i, boolean z) {
        this.o0 = i;
        if (Build.VERSION.SDK_INT < 17) {
            int i2 = this.n0;
            if (i2 == 5) {
                this.o0 = 0;
            } else if (i2 == 6) {
                this.o0 = 1;
            }
        } else if (z) {
            int i3 = this.n0;
            if (i3 == 5) {
                this.o0 = 1;
            } else if (i3 == 6) {
                this.o0 = 0;
            }
        } else {
            int i4 = this.n0;
            if (i4 == 5) {
                this.o0 = 0;
            } else if (i4 == 6) {
                this.o0 = 1;
            }
        }
        if (constraintWidget instanceof androidx.constraintlayout.core.widgets.a) {
            ((androidx.constraintlayout.core.widgets.a) constraintWidget).x1(this.o0);
        }
    }

    public Barrier(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        super.setVisibility(8);
    }

    public Barrier(Context context, AttributeSet attributeSet, int i) {
        super(context, attributeSet, i);
        super.setVisibility(8);
    }
}
