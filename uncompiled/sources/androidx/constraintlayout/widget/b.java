package androidx.constraintlayout.widget;

import android.util.SparseIntArray;
import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;

/* compiled from: SharedValues.java */
/* loaded from: classes.dex */
public class b {
    public HashMap<Integer, HashSet<WeakReference<a>>> a;

    /* compiled from: SharedValues.java */
    /* loaded from: classes.dex */
    public interface a {
    }

    public b() {
        new SparseIntArray();
        this.a = new HashMap<>();
    }

    public void a(int i, a aVar) {
        HashSet<WeakReference<a>> hashSet = this.a.get(Integer.valueOf(i));
        if (hashSet == null) {
            hashSet = new HashSet<>();
            this.a.put(Integer.valueOf(i), hashSet);
        }
        hashSet.add(new WeakReference<>(aVar));
    }

    public void b(int i, a aVar) {
        HashSet<WeakReference<a>> hashSet = this.a.get(Integer.valueOf(i));
        if (hashSet == null) {
            return;
        }
        ArrayList arrayList = new ArrayList();
        Iterator<WeakReference<a>> it = hashSet.iterator();
        while (it.hasNext()) {
            WeakReference<a> next = it.next();
            a aVar2 = next.get();
            if (aVar2 == null || aVar2 == aVar) {
                arrayList.add(next);
            }
        }
        hashSet.removeAll(arrayList);
    }
}
