package androidx.constraintlayout.widget;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.res.Resources;
import android.content.res.TypedArray;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.os.Build;
import android.util.AttributeSet;
import android.util.SparseArray;
import android.util.SparseIntArray;
import android.view.View;
import android.view.ViewGroup;
import androidx.constraintlayout.core.widgets.ConstraintAnchor;
import androidx.constraintlayout.core.widgets.ConstraintWidget;
import androidx.constraintlayout.core.widgets.d;
import androidx.constraintlayout.core.widgets.f;
import androidx.constraintlayout.core.widgets.g;
import androidx.constraintlayout.core.widgets.i;
import com.github.mikephil.charting.utils.Utils;
import defpackage.jo;
import java.util.ArrayList;
import java.util.HashMap;
import okhttp3.internal.http2.Http2Connection;

/* loaded from: classes.dex */
public class ConstraintLayout extends ViewGroup {
    public static androidx.constraintlayout.widget.b x0;
    public SparseArray<View> a;
    public ArrayList<ConstraintHelper> f0;
    public d g0;
    public int h0;
    public int i0;
    public int j0;
    public int k0;
    public boolean l0;
    public int m0;
    public androidx.constraintlayout.widget.a n0;
    public e60 o0;
    public int p0;
    public HashMap<String, Integer> q0;
    public int r0;
    public int s0;
    public SparseArray<ConstraintWidget> t0;
    public b u0;
    public int v0;
    public int w0;

    /* loaded from: classes.dex */
    public static /* synthetic */ class a {
        public static final /* synthetic */ int[] a;

        static {
            int[] iArr = new int[ConstraintWidget.DimensionBehaviour.values().length];
            a = iArr;
            try {
                iArr[ConstraintWidget.DimensionBehaviour.FIXED.ordinal()] = 1;
            } catch (NoSuchFieldError unused) {
            }
            try {
                a[ConstraintWidget.DimensionBehaviour.WRAP_CONTENT.ordinal()] = 2;
            } catch (NoSuchFieldError unused2) {
            }
            try {
                a[ConstraintWidget.DimensionBehaviour.MATCH_PARENT.ordinal()] = 3;
            } catch (NoSuchFieldError unused3) {
            }
            try {
                a[ConstraintWidget.DimensionBehaviour.MATCH_CONSTRAINT.ordinal()] = 4;
            } catch (NoSuchFieldError unused4) {
            }
        }
    }

    /* loaded from: classes.dex */
    public class b implements jo.b {
        public ConstraintLayout a;
        public int b;
        public int c;
        public int d;
        public int e;
        public int f;
        public int g;

        public b(ConstraintLayout constraintLayout) {
            this.a = constraintLayout;
        }

        @Override // defpackage.jo.b
        public final void a() {
            int childCount = this.a.getChildCount();
            for (int i = 0; i < childCount; i++) {
                View childAt = this.a.getChildAt(i);
                if (childAt instanceof Placeholder) {
                    ((Placeholder) childAt).b(this.a);
                }
            }
            int size = this.a.f0.size();
            if (size > 0) {
                for (int i2 = 0; i2 < size; i2++) {
                    ((ConstraintHelper) this.a.f0.get(i2)).s(this.a);
                }
            }
        }

        @Override // defpackage.jo.b
        @SuppressLint({"WrongCall"})
        public final void b(ConstraintWidget constraintWidget, jo.a aVar) {
            int makeMeasureSpec;
            int makeMeasureSpec2;
            int baseline;
            int max;
            int i;
            int i2;
            int i3;
            if (constraintWidget == null) {
                return;
            }
            if (constraintWidget.U() == 8 && !constraintWidget.i0()) {
                aVar.e = 0;
                aVar.f = 0;
                aVar.g = 0;
            } else if (constraintWidget.M() == null) {
            } else {
                ConstraintWidget.DimensionBehaviour dimensionBehaviour = aVar.a;
                ConstraintWidget.DimensionBehaviour dimensionBehaviour2 = aVar.b;
                int i4 = aVar.c;
                int i5 = aVar.d;
                int i6 = this.b + this.c;
                int i7 = this.d;
                View view = (View) constraintWidget.u();
                int[] iArr = a.a;
                int i8 = iArr[dimensionBehaviour.ordinal()];
                if (i8 == 1) {
                    makeMeasureSpec = View.MeasureSpec.makeMeasureSpec(i4, 1073741824);
                } else if (i8 == 2) {
                    makeMeasureSpec = ViewGroup.getChildMeasureSpec(this.f, i7, -2);
                } else if (i8 == 3) {
                    makeMeasureSpec = ViewGroup.getChildMeasureSpec(this.f, i7 + constraintWidget.D(), -1);
                } else if (i8 != 4) {
                    makeMeasureSpec = 0;
                } else {
                    makeMeasureSpec = ViewGroup.getChildMeasureSpec(this.f, i7, -2);
                    boolean z = constraintWidget.s == 1;
                    int i9 = aVar.j;
                    if (i9 == jo.a.l || i9 == jo.a.m) {
                        if (aVar.j == jo.a.m || !z || (z && (view.getMeasuredHeight() == constraintWidget.z())) || (view instanceof Placeholder) || constraintWidget.m0()) {
                            makeMeasureSpec = View.MeasureSpec.makeMeasureSpec(constraintWidget.V(), 1073741824);
                        }
                    }
                }
                int i10 = iArr[dimensionBehaviour2.ordinal()];
                if (i10 == 1) {
                    makeMeasureSpec2 = View.MeasureSpec.makeMeasureSpec(i5, 1073741824);
                } else if (i10 == 2) {
                    makeMeasureSpec2 = ViewGroup.getChildMeasureSpec(this.g, i6, -2);
                } else if (i10 == 3) {
                    makeMeasureSpec2 = ViewGroup.getChildMeasureSpec(this.g, i6 + constraintWidget.T(), -1);
                } else if (i10 != 4) {
                    makeMeasureSpec2 = 0;
                } else {
                    makeMeasureSpec2 = ViewGroup.getChildMeasureSpec(this.g, i6, -2);
                    boolean z2 = constraintWidget.t == 1;
                    int i11 = aVar.j;
                    if (i11 == jo.a.l || i11 == jo.a.m) {
                        if (aVar.j == jo.a.m || !z2 || (z2 && (view.getMeasuredWidth() == constraintWidget.V())) || (view instanceof Placeholder) || constraintWidget.n0()) {
                            makeMeasureSpec2 = View.MeasureSpec.makeMeasureSpec(constraintWidget.z(), 1073741824);
                        }
                    }
                }
                d dVar = (d) constraintWidget.M();
                if (dVar != null && g.b(ConstraintLayout.this.m0, 256) && view.getMeasuredWidth() == constraintWidget.V() && view.getMeasuredWidth() < dVar.V() && view.getMeasuredHeight() == constraintWidget.z() && view.getMeasuredHeight() < dVar.z() && view.getBaseline() == constraintWidget.r() && !constraintWidget.l0()) {
                    if (d(constraintWidget.E(), makeMeasureSpec, constraintWidget.V()) && d(constraintWidget.F(), makeMeasureSpec2, constraintWidget.z())) {
                        aVar.e = constraintWidget.V();
                        aVar.f = constraintWidget.z();
                        aVar.g = constraintWidget.r();
                        return;
                    }
                }
                ConstraintWidget.DimensionBehaviour dimensionBehaviour3 = ConstraintWidget.DimensionBehaviour.MATCH_CONSTRAINT;
                boolean z3 = dimensionBehaviour == dimensionBehaviour3;
                boolean z4 = dimensionBehaviour2 == dimensionBehaviour3;
                ConstraintWidget.DimensionBehaviour dimensionBehaviour4 = ConstraintWidget.DimensionBehaviour.MATCH_PARENT;
                boolean z5 = dimensionBehaviour2 == dimensionBehaviour4 || dimensionBehaviour2 == ConstraintWidget.DimensionBehaviour.FIXED;
                boolean z6 = dimensionBehaviour == dimensionBehaviour4 || dimensionBehaviour == ConstraintWidget.DimensionBehaviour.FIXED;
                boolean z7 = z3 && constraintWidget.b0 > Utils.FLOAT_EPSILON;
                boolean z8 = z4 && constraintWidget.b0 > Utils.FLOAT_EPSILON;
                if (view == null) {
                    return;
                }
                LayoutParams layoutParams = (LayoutParams) view.getLayoutParams();
                int i12 = aVar.j;
                if (i12 != jo.a.l && i12 != jo.a.m && z3 && constraintWidget.s == 0 && z4 && constraintWidget.t == 0) {
                    i3 = -1;
                    i2 = 0;
                    baseline = 0;
                    max = 0;
                } else {
                    if ((view instanceof VirtualLayout) && (constraintWidget instanceof i)) {
                        ((VirtualLayout) view).x((i) constraintWidget, makeMeasureSpec, makeMeasureSpec2);
                    } else {
                        view.measure(makeMeasureSpec, makeMeasureSpec2);
                    }
                    constraintWidget.S0(makeMeasureSpec, makeMeasureSpec2);
                    int measuredWidth = view.getMeasuredWidth();
                    int measuredHeight = view.getMeasuredHeight();
                    baseline = view.getBaseline();
                    int i13 = constraintWidget.v;
                    max = i13 > 0 ? Math.max(i13, measuredWidth) : measuredWidth;
                    int i14 = constraintWidget.w;
                    if (i14 > 0) {
                        max = Math.min(i14, max);
                    }
                    int i15 = constraintWidget.y;
                    if (i15 > 0) {
                        i2 = Math.max(i15, measuredHeight);
                        i = makeMeasureSpec;
                    } else {
                        i = makeMeasureSpec;
                        i2 = measuredHeight;
                    }
                    int i16 = constraintWidget.z;
                    if (i16 > 0) {
                        i2 = Math.min(i16, i2);
                    }
                    if (!g.b(ConstraintLayout.this.m0, 1)) {
                        if (z7 && z5) {
                            max = (int) ((i2 * constraintWidget.b0) + 0.5f);
                        } else if (z8 && z6) {
                            i2 = (int) ((max / constraintWidget.b0) + 0.5f);
                        }
                    }
                    if (measuredWidth != max || measuredHeight != i2) {
                        int makeMeasureSpec3 = measuredWidth != max ? View.MeasureSpec.makeMeasureSpec(max, 1073741824) : i;
                        if (measuredHeight != i2) {
                            makeMeasureSpec2 = View.MeasureSpec.makeMeasureSpec(i2, 1073741824);
                        }
                        view.measure(makeMeasureSpec3, makeMeasureSpec2);
                        constraintWidget.S0(makeMeasureSpec3, makeMeasureSpec2);
                        max = view.getMeasuredWidth();
                        i2 = view.getMeasuredHeight();
                        baseline = view.getBaseline();
                    }
                    i3 = -1;
                }
                boolean z9 = baseline != i3;
                aVar.i = (max == aVar.c && i2 == aVar.d) ? false : true;
                if (layoutParams.b0) {
                    z9 = true;
                }
                if (z9 && baseline != -1 && constraintWidget.r() != baseline) {
                    aVar.i = true;
                }
                aVar.e = max;
                aVar.f = i2;
                aVar.h = z9;
                aVar.g = baseline;
            }
        }

        public void c(int i, int i2, int i3, int i4, int i5, int i6) {
            this.b = i3;
            this.c = i4;
            this.d = i5;
            this.e = i6;
            this.f = i;
            this.g = i2;
        }

        public final boolean d(int i, int i2, int i3) {
            if (i == i2) {
                return true;
            }
            int mode = View.MeasureSpec.getMode(i);
            View.MeasureSpec.getSize(i);
            int mode2 = View.MeasureSpec.getMode(i2);
            int size = View.MeasureSpec.getSize(i2);
            if (mode2 == 1073741824) {
                return (mode == Integer.MIN_VALUE || mode == 0) && i3 == size;
            }
            return false;
        }
    }

    public ConstraintLayout(Context context) {
        super(context);
        this.a = new SparseArray<>();
        this.f0 = new ArrayList<>(4);
        this.g0 = new d();
        this.h0 = 0;
        this.i0 = 0;
        this.j0 = Integer.MAX_VALUE;
        this.k0 = Integer.MAX_VALUE;
        this.l0 = true;
        this.m0 = 257;
        this.n0 = null;
        this.o0 = null;
        this.p0 = -1;
        this.q0 = new HashMap<>();
        this.r0 = -1;
        this.s0 = -1;
        this.t0 = new SparseArray<>();
        this.u0 = new b(this);
        this.v0 = 0;
        this.w0 = 0;
        k(null, 0, 0);
    }

    private int getPaddingWidth() {
        int max = Math.max(0, getPaddingLeft()) + Math.max(0, getPaddingRight());
        int max2 = Build.VERSION.SDK_INT >= 17 ? Math.max(0, getPaddingEnd()) + Math.max(0, getPaddingStart()) : 0;
        return max2 > 0 ? max2 : max;
    }

    public static androidx.constraintlayout.widget.b getSharedValues() {
        if (x0 == null) {
            x0 = new androidx.constraintlayout.widget.b();
        }
        return x0;
    }

    @Override // android.view.ViewGroup
    public boolean checkLayoutParams(ViewGroup.LayoutParams layoutParams) {
        return layoutParams instanceof LayoutParams;
    }

    public void d(boolean z, View view, ConstraintWidget constraintWidget, LayoutParams layoutParams, SparseArray<ConstraintWidget> sparseArray) {
        int i;
        float f;
        int i2;
        int i3;
        ConstraintWidget constraintWidget2;
        ConstraintWidget constraintWidget3;
        ConstraintWidget constraintWidget4;
        ConstraintWidget constraintWidget5;
        int i4;
        layoutParams.c();
        constraintWidget.g1(view.getVisibility());
        if (layoutParams.e0) {
            constraintWidget.Q0(true);
            constraintWidget.g1(8);
        }
        constraintWidget.y0(view);
        if (view instanceof ConstraintHelper) {
            ((ConstraintHelper) view).q(constraintWidget, this.g0.M1());
        }
        if (layoutParams.c0) {
            f fVar = (f) constraintWidget;
            int i5 = layoutParams.n0;
            int i6 = layoutParams.o0;
            float f2 = layoutParams.p0;
            if (Build.VERSION.SDK_INT < 17) {
                i5 = layoutParams.a;
                i6 = layoutParams.b;
                f2 = layoutParams.c;
            }
            if (f2 != -1.0f) {
                fVar.w1(f2);
                return;
            } else if (i5 != -1) {
                fVar.u1(i5);
                return;
            } else if (i6 != -1) {
                fVar.v1(i6);
                return;
            } else {
                return;
            }
        }
        int i7 = layoutParams.g0;
        int i8 = layoutParams.h0;
        int i9 = layoutParams.i0;
        int i10 = layoutParams.j0;
        int i11 = layoutParams.k0;
        int i12 = layoutParams.l0;
        float f3 = layoutParams.m0;
        if (Build.VERSION.SDK_INT < 17) {
            i7 = layoutParams.d;
            int i13 = layoutParams.e;
            int i14 = layoutParams.f;
            int i15 = layoutParams.g;
            int i16 = layoutParams.v;
            int i17 = layoutParams.x;
            float f4 = layoutParams.D;
            if (i7 == -1 && i13 == -1) {
                int i18 = layoutParams.s;
                if (i18 != -1) {
                    i7 = i18;
                } else {
                    int i19 = layoutParams.r;
                    if (i19 != -1) {
                        i13 = i19;
                    }
                }
            }
            if (i14 == -1 && i15 == -1) {
                i2 = layoutParams.t;
                if (i2 == -1) {
                    int i20 = layoutParams.u;
                    if (i20 != -1) {
                        i = i17;
                        f = f4;
                        i11 = i16;
                        i3 = i20;
                        i8 = i13;
                        i2 = i14;
                    }
                }
                i = i17;
                f = f4;
                i11 = i16;
                i3 = i15;
                i8 = i13;
            }
            i2 = i14;
            i = i17;
            f = f4;
            i11 = i16;
            i3 = i15;
            i8 = i13;
        } else {
            i = i12;
            f = f3;
            i2 = i9;
            i3 = i10;
        }
        int i21 = layoutParams.o;
        if (i21 != -1) {
            ConstraintWidget constraintWidget6 = sparseArray.get(i21);
            if (constraintWidget6 != null) {
                constraintWidget.m(constraintWidget6, layoutParams.q, layoutParams.p);
            }
        } else {
            if (i7 != -1) {
                ConstraintWidget constraintWidget7 = sparseArray.get(i7);
                if (constraintWidget7 != null) {
                    ConstraintAnchor.Type type = ConstraintAnchor.Type.LEFT;
                    constraintWidget.d0(type, constraintWidget7, type, ((ViewGroup.MarginLayoutParams) layoutParams).leftMargin, i11);
                }
            } else if (i8 != -1 && (constraintWidget2 = sparseArray.get(i8)) != null) {
                constraintWidget.d0(ConstraintAnchor.Type.LEFT, constraintWidget2, ConstraintAnchor.Type.RIGHT, ((ViewGroup.MarginLayoutParams) layoutParams).leftMargin, i11);
            }
            if (i2 != -1) {
                ConstraintWidget constraintWidget8 = sparseArray.get(i2);
                if (constraintWidget8 != null) {
                    constraintWidget.d0(ConstraintAnchor.Type.RIGHT, constraintWidget8, ConstraintAnchor.Type.LEFT, ((ViewGroup.MarginLayoutParams) layoutParams).rightMargin, i);
                }
            } else if (i3 != -1 && (constraintWidget3 = sparseArray.get(i3)) != null) {
                ConstraintAnchor.Type type2 = ConstraintAnchor.Type.RIGHT;
                constraintWidget.d0(type2, constraintWidget3, type2, ((ViewGroup.MarginLayoutParams) layoutParams).rightMargin, i);
            }
            int i22 = layoutParams.h;
            if (i22 != -1) {
                ConstraintWidget constraintWidget9 = sparseArray.get(i22);
                if (constraintWidget9 != null) {
                    ConstraintAnchor.Type type3 = ConstraintAnchor.Type.TOP;
                    constraintWidget.d0(type3, constraintWidget9, type3, ((ViewGroup.MarginLayoutParams) layoutParams).topMargin, layoutParams.w);
                }
            } else {
                int i23 = layoutParams.i;
                if (i23 != -1 && (constraintWidget4 = sparseArray.get(i23)) != null) {
                    constraintWidget.d0(ConstraintAnchor.Type.TOP, constraintWidget4, ConstraintAnchor.Type.BOTTOM, ((ViewGroup.MarginLayoutParams) layoutParams).topMargin, layoutParams.w);
                }
            }
            int i24 = layoutParams.j;
            if (i24 != -1) {
                ConstraintWidget constraintWidget10 = sparseArray.get(i24);
                if (constraintWidget10 != null) {
                    constraintWidget.d0(ConstraintAnchor.Type.BOTTOM, constraintWidget10, ConstraintAnchor.Type.TOP, ((ViewGroup.MarginLayoutParams) layoutParams).bottomMargin, layoutParams.y);
                }
            } else {
                int i25 = layoutParams.k;
                if (i25 != -1 && (constraintWidget5 = sparseArray.get(i25)) != null) {
                    ConstraintAnchor.Type type4 = ConstraintAnchor.Type.BOTTOM;
                    constraintWidget.d0(type4, constraintWidget5, type4, ((ViewGroup.MarginLayoutParams) layoutParams).bottomMargin, layoutParams.y);
                }
            }
            int i26 = layoutParams.l;
            if (i26 != -1) {
                s(constraintWidget, layoutParams, sparseArray, i26, ConstraintAnchor.Type.BASELINE);
            } else {
                int i27 = layoutParams.m;
                if (i27 != -1) {
                    s(constraintWidget, layoutParams, sparseArray, i27, ConstraintAnchor.Type.TOP);
                } else {
                    int i28 = layoutParams.n;
                    if (i28 != -1) {
                        s(constraintWidget, layoutParams, sparseArray, i28, ConstraintAnchor.Type.BOTTOM);
                    }
                }
            }
            if (f >= Utils.FLOAT_EPSILON) {
                constraintWidget.J0(f);
            }
            float f5 = layoutParams.E;
            if (f5 >= Utils.FLOAT_EPSILON) {
                constraintWidget.a1(f5);
            }
        }
        if (z && ((i4 = layoutParams.S) != -1 || layoutParams.T != -1)) {
            constraintWidget.Y0(i4, layoutParams.T);
        }
        if (!layoutParams.Z) {
            if (((ViewGroup.MarginLayoutParams) layoutParams).width == -1) {
                if (layoutParams.V) {
                    constraintWidget.M0(ConstraintWidget.DimensionBehaviour.MATCH_CONSTRAINT);
                } else {
                    constraintWidget.M0(ConstraintWidget.DimensionBehaviour.MATCH_PARENT);
                }
                constraintWidget.q(ConstraintAnchor.Type.LEFT).g = ((ViewGroup.MarginLayoutParams) layoutParams).leftMargin;
                constraintWidget.q(ConstraintAnchor.Type.RIGHT).g = ((ViewGroup.MarginLayoutParams) layoutParams).rightMargin;
            } else {
                constraintWidget.M0(ConstraintWidget.DimensionBehaviour.MATCH_CONSTRAINT);
                constraintWidget.h1(0);
            }
        } else {
            constraintWidget.M0(ConstraintWidget.DimensionBehaviour.FIXED);
            constraintWidget.h1(((ViewGroup.MarginLayoutParams) layoutParams).width);
            if (((ViewGroup.MarginLayoutParams) layoutParams).width == -2) {
                constraintWidget.M0(ConstraintWidget.DimensionBehaviour.WRAP_CONTENT);
            }
        }
        if (!layoutParams.a0) {
            if (((ViewGroup.MarginLayoutParams) layoutParams).height == -1) {
                if (layoutParams.W) {
                    constraintWidget.d1(ConstraintWidget.DimensionBehaviour.MATCH_CONSTRAINT);
                } else {
                    constraintWidget.d1(ConstraintWidget.DimensionBehaviour.MATCH_PARENT);
                }
                constraintWidget.q(ConstraintAnchor.Type.TOP).g = ((ViewGroup.MarginLayoutParams) layoutParams).topMargin;
                constraintWidget.q(ConstraintAnchor.Type.BOTTOM).g = ((ViewGroup.MarginLayoutParams) layoutParams).bottomMargin;
            } else {
                constraintWidget.d1(ConstraintWidget.DimensionBehaviour.MATCH_CONSTRAINT);
                constraintWidget.I0(0);
            }
        } else {
            constraintWidget.d1(ConstraintWidget.DimensionBehaviour.FIXED);
            constraintWidget.I0(((ViewGroup.MarginLayoutParams) layoutParams).height);
            if (((ViewGroup.MarginLayoutParams) layoutParams).height == -2) {
                constraintWidget.d1(ConstraintWidget.DimensionBehaviour.WRAP_CONTENT);
            }
        }
        constraintWidget.A0(layoutParams.F);
        constraintWidget.O0(layoutParams.G);
        constraintWidget.f1(layoutParams.H);
        constraintWidget.K0(layoutParams.I);
        constraintWidget.b1(layoutParams.J);
        constraintWidget.i1(layoutParams.Y);
        constraintWidget.N0(layoutParams.K, layoutParams.M, layoutParams.O, layoutParams.Q);
        constraintWidget.e1(layoutParams.L, layoutParams.N, layoutParams.P, layoutParams.R);
    }

    @Override // android.view.ViewGroup, android.view.View
    public void dispatchDraw(Canvas canvas) {
        Object tag;
        int size;
        ArrayList<ConstraintHelper> arrayList = this.f0;
        if (arrayList != null && (size = arrayList.size()) > 0) {
            for (int i = 0; i < size; i++) {
                this.f0.get(i).t(this);
            }
        }
        super.dispatchDraw(canvas);
        if (isInEditMode()) {
            float width = getWidth();
            float height = getHeight();
            int childCount = getChildCount();
            for (int i2 = 0; i2 < childCount; i2++) {
                View childAt = getChildAt(i2);
                if (childAt.getVisibility() != 8 && (tag = childAt.getTag()) != null && (tag instanceof String)) {
                    String[] split = ((String) tag).split(",");
                    if (split.length == 4) {
                        int parseInt = Integer.parseInt(split[0]);
                        int parseInt2 = Integer.parseInt(split[1]);
                        int parseInt3 = Integer.parseInt(split[2]);
                        int i3 = (int) ((parseInt / 1080.0f) * width);
                        int i4 = (int) ((parseInt2 / 1920.0f) * height);
                        Paint paint = new Paint();
                        paint.setColor(-65536);
                        float f = i3;
                        float f2 = i4;
                        float f3 = i3 + ((int) ((parseInt3 / 1080.0f) * width));
                        canvas.drawLine(f, f2, f3, f2, paint);
                        float parseInt4 = i4 + ((int) ((Integer.parseInt(split[3]) / 1920.0f) * height));
                        canvas.drawLine(f3, f2, f3, parseInt4, paint);
                        canvas.drawLine(f3, parseInt4, f, parseInt4, paint);
                        canvas.drawLine(f, parseInt4, f, f2, paint);
                        paint.setColor(-16711936);
                        canvas.drawLine(f, f2, f3, parseInt4, paint);
                        canvas.drawLine(f, parseInt4, f3, f2, paint);
                    }
                }
            }
        }
    }

    @Override // android.view.ViewGroup
    /* renamed from: e */
    public LayoutParams generateDefaultLayoutParams() {
        return new LayoutParams(-2, -2);
    }

    @Override // android.view.ViewGroup
    /* renamed from: f */
    public LayoutParams generateLayoutParams(AttributeSet attributeSet) {
        return new LayoutParams(getContext(), attributeSet);
    }

    @Override // android.view.View
    public void forceLayout() {
        m();
        super.forceLayout();
    }

    public Object g(int i, Object obj) {
        if (i == 0 && (obj instanceof String)) {
            String str = (String) obj;
            HashMap<String, Integer> hashMap = this.q0;
            if (hashMap == null || !hashMap.containsKey(str)) {
                return null;
            }
            return this.q0.get(str);
        }
        return null;
    }

    public int getMaxHeight() {
        return this.k0;
    }

    public int getMaxWidth() {
        return this.j0;
    }

    public int getMinHeight() {
        return this.i0;
    }

    public int getMinWidth() {
        return this.h0;
    }

    public int getOptimizationLevel() {
        return this.g0.G1();
    }

    public final ConstraintWidget h(int i) {
        if (i == 0) {
            return this.g0;
        }
        View view = this.a.get(i);
        if (view == null && (view = findViewById(i)) != null && view != this && view.getParent() == this) {
            onViewAdded(view);
        }
        if (view == this) {
            return this.g0;
        }
        if (view == null) {
            return null;
        }
        return ((LayoutParams) view.getLayoutParams()).q0;
    }

    public View i(int i) {
        return this.a.get(i);
    }

    public final ConstraintWidget j(View view) {
        if (view == this) {
            return this.g0;
        }
        if (view != null) {
            if (view.getLayoutParams() instanceof LayoutParams) {
                return ((LayoutParams) view.getLayoutParams()).q0;
            }
            view.setLayoutParams(generateLayoutParams(view.getLayoutParams()));
            if (view.getLayoutParams() instanceof LayoutParams) {
                return ((LayoutParams) view.getLayoutParams()).q0;
            }
            return null;
        }
        return null;
    }

    public final void k(AttributeSet attributeSet, int i, int i2) {
        this.g0.y0(this);
        this.g0.S1(this.u0);
        this.a.put(getId(), this);
        this.n0 = null;
        if (attributeSet != null) {
            TypedArray obtainStyledAttributes = getContext().obtainStyledAttributes(attributeSet, w23.ConstraintLayout_Layout, i, i2);
            int indexCount = obtainStyledAttributes.getIndexCount();
            for (int i3 = 0; i3 < indexCount; i3++) {
                int index = obtainStyledAttributes.getIndex(i3);
                if (index == w23.ConstraintLayout_Layout_android_minWidth) {
                    this.h0 = obtainStyledAttributes.getDimensionPixelOffset(index, this.h0);
                } else if (index == w23.ConstraintLayout_Layout_android_minHeight) {
                    this.i0 = obtainStyledAttributes.getDimensionPixelOffset(index, this.i0);
                } else if (index == w23.ConstraintLayout_Layout_android_maxWidth) {
                    this.j0 = obtainStyledAttributes.getDimensionPixelOffset(index, this.j0);
                } else if (index == w23.ConstraintLayout_Layout_android_maxHeight) {
                    this.k0 = obtainStyledAttributes.getDimensionPixelOffset(index, this.k0);
                } else if (index == w23.ConstraintLayout_Layout_layout_optimizationLevel) {
                    this.m0 = obtainStyledAttributes.getInt(index, this.m0);
                } else if (index == w23.ConstraintLayout_Layout_layoutDescription) {
                    int resourceId = obtainStyledAttributes.getResourceId(index, 0);
                    if (resourceId != 0) {
                        try {
                            n(resourceId);
                        } catch (Resources.NotFoundException unused) {
                            this.o0 = null;
                        }
                    }
                } else if (index == w23.ConstraintLayout_Layout_constraintSet) {
                    int resourceId2 = obtainStyledAttributes.getResourceId(index, 0);
                    try {
                        androidx.constraintlayout.widget.a aVar = new androidx.constraintlayout.widget.a();
                        this.n0 = aVar;
                        aVar.D(getContext(), resourceId2);
                    } catch (Resources.NotFoundException unused2) {
                        this.n0 = null;
                    }
                    this.p0 = resourceId2;
                }
            }
            obtainStyledAttributes.recycle();
        }
        this.g0.T1(this.m0);
    }

    public boolean l() {
        if (Build.VERSION.SDK_INT >= 17) {
            return ((getContext().getApplicationInfo().flags & 4194304) != 0) && 1 == getLayoutDirection();
        }
        return false;
    }

    public final void m() {
        this.l0 = true;
        this.r0 = -1;
        this.s0 = -1;
    }

    public void n(int i) {
        this.o0 = new e60(getContext(), this, i);
    }

    public void o(int i, int i2, int i3, int i4, boolean z, boolean z2) {
        b bVar = this.u0;
        int i5 = bVar.e;
        int resolveSizeAndState = ViewGroup.resolveSizeAndState(i3 + bVar.d, i, 0);
        int min = Math.min(this.j0, resolveSizeAndState & 16777215);
        int min2 = Math.min(this.k0, ViewGroup.resolveSizeAndState(i4 + i5, i2, 0) & 16777215);
        if (z) {
            min |= Http2Connection.OKHTTP_CLIENT_WINDOW_SIZE;
        }
        if (z2) {
            min2 |= Http2Connection.OKHTTP_CLIENT_WINDOW_SIZE;
        }
        setMeasuredDimension(min, min2);
        this.r0 = min;
        this.s0 = min2;
    }

    @Override // android.view.ViewGroup, android.view.View
    public void onLayout(boolean z, int i, int i2, int i3, int i4) {
        View content;
        int childCount = getChildCount();
        boolean isInEditMode = isInEditMode();
        for (int i5 = 0; i5 < childCount; i5++) {
            View childAt = getChildAt(i5);
            LayoutParams layoutParams = (LayoutParams) childAt.getLayoutParams();
            ConstraintWidget constraintWidget = layoutParams.q0;
            if ((childAt.getVisibility() != 8 || layoutParams.c0 || layoutParams.d0 || layoutParams.f0 || isInEditMode) && !layoutParams.e0) {
                int W = constraintWidget.W();
                int X = constraintWidget.X();
                int V = constraintWidget.V() + W;
                int z2 = constraintWidget.z() + X;
                childAt.layout(W, X, V, z2);
                if ((childAt instanceof Placeholder) && (content = ((Placeholder) childAt).getContent()) != null) {
                    content.setVisibility(0);
                    content.layout(W, X, V, z2);
                }
            }
        }
        int size = this.f0.size();
        if (size > 0) {
            for (int i6 = 0; i6 < size; i6++) {
                this.f0.get(i6).r(this);
            }
        }
    }

    @Override // android.view.View
    public void onMeasure(int i, int i2) {
        if (this.v0 == i) {
            int i3 = this.w0;
        }
        if (!this.l0) {
            int childCount = getChildCount();
            int i4 = 0;
            while (true) {
                if (i4 >= childCount) {
                    break;
                } else if (getChildAt(i4).isLayoutRequested()) {
                    this.l0 = true;
                    break;
                } else {
                    i4++;
                }
            }
        }
        boolean z = this.l0;
        this.v0 = i;
        this.w0 = i2;
        this.g0.V1(l());
        if (this.l0) {
            this.l0 = false;
            if (t()) {
                this.g0.X1();
            }
        }
        p(this.g0, this.m0, i, i2);
        o(i, i2, this.g0.V(), this.g0.z(), this.g0.N1(), this.g0.L1());
    }

    @Override // android.view.ViewGroup
    public void onViewAdded(View view) {
        super.onViewAdded(view);
        ConstraintWidget j = j(view);
        if ((view instanceof Guideline) && !(j instanceof f)) {
            LayoutParams layoutParams = (LayoutParams) view.getLayoutParams();
            f fVar = new f();
            layoutParams.q0 = fVar;
            layoutParams.c0 = true;
            fVar.x1(layoutParams.U);
        }
        if (view instanceof ConstraintHelper) {
            ConstraintHelper constraintHelper = (ConstraintHelper) view;
            constraintHelper.w();
            ((LayoutParams) view.getLayoutParams()).d0 = true;
            if (!this.f0.contains(constraintHelper)) {
                this.f0.add(constraintHelper);
            }
        }
        this.a.put(view.getId(), view);
        this.l0 = true;
    }

    @Override // android.view.ViewGroup
    public void onViewRemoved(View view) {
        super.onViewRemoved(view);
        this.a.remove(view.getId());
        this.g0.q1(j(view));
        this.f0.remove(view);
        this.l0 = true;
    }

    public void p(d dVar, int i, int i2, int i3) {
        int max;
        int mode = View.MeasureSpec.getMode(i2);
        int size = View.MeasureSpec.getSize(i2);
        int mode2 = View.MeasureSpec.getMode(i3);
        int size2 = View.MeasureSpec.getSize(i3);
        int max2 = Math.max(0, getPaddingTop());
        int max3 = Math.max(0, getPaddingBottom());
        int i4 = max2 + max3;
        int paddingWidth = getPaddingWidth();
        this.u0.c(i2, i3, max2, max3, paddingWidth, i4);
        if (Build.VERSION.SDK_INT >= 17) {
            int max4 = Math.max(0, getPaddingStart());
            int max5 = Math.max(0, getPaddingEnd());
            if (max4 <= 0 && max5 <= 0) {
                max4 = Math.max(0, getPaddingLeft());
            } else if (l()) {
                max4 = max5;
            }
            max = max4;
        } else {
            max = Math.max(0, getPaddingLeft());
        }
        int i5 = size - paddingWidth;
        int i6 = size2 - i4;
        r(dVar, mode, i5, mode2, i6);
        dVar.O1(i, mode, i5, mode2, i6, this.r0, this.s0, max, max2);
    }

    public final void q() {
        boolean isInEditMode = isInEditMode();
        int childCount = getChildCount();
        for (int i = 0; i < childCount; i++) {
            ConstraintWidget j = j(getChildAt(i));
            if (j != null) {
                j.s0();
            }
        }
        if (isInEditMode) {
            for (int i2 = 0; i2 < childCount; i2++) {
                View childAt = getChildAt(i2);
                try {
                    String resourceName = getResources().getResourceName(childAt.getId());
                    setDesignInformation(0, resourceName, Integer.valueOf(childAt.getId()));
                    int indexOf = resourceName.indexOf(47);
                    if (indexOf != -1) {
                        resourceName = resourceName.substring(indexOf + 1);
                    }
                    h(childAt.getId()).z0(resourceName);
                } catch (Resources.NotFoundException unused) {
                }
            }
        }
        if (this.p0 != -1) {
            for (int i3 = 0; i3 < childCount; i3++) {
                View childAt2 = getChildAt(i3);
                if (childAt2.getId() == this.p0 && (childAt2 instanceof Constraints)) {
                    this.n0 = ((Constraints) childAt2).getConstraintSet();
                }
            }
        }
        androidx.constraintlayout.widget.a aVar = this.n0;
        if (aVar != null) {
            aVar.k(this, true);
        }
        this.g0.r1();
        int size = this.f0.size();
        if (size > 0) {
            for (int i4 = 0; i4 < size; i4++) {
                this.f0.get(i4).v(this);
            }
        }
        for (int i5 = 0; i5 < childCount; i5++) {
            View childAt3 = getChildAt(i5);
            if (childAt3 instanceof Placeholder) {
                ((Placeholder) childAt3).c(this);
            }
        }
        this.t0.clear();
        this.t0.put(0, this.g0);
        this.t0.put(getId(), this.g0);
        for (int i6 = 0; i6 < childCount; i6++) {
            View childAt4 = getChildAt(i6);
            this.t0.put(childAt4.getId(), j(childAt4));
        }
        for (int i7 = 0; i7 < childCount; i7++) {
            View childAt5 = getChildAt(i7);
            ConstraintWidget j2 = j(childAt5);
            if (j2 != null) {
                LayoutParams layoutParams = (LayoutParams) childAt5.getLayoutParams();
                this.g0.b(j2);
                d(isInEditMode, childAt5, j2, layoutParams, this.t0);
            }
        }
    }

    public void r(d dVar, int i, int i2, int i3, int i4) {
        ConstraintWidget.DimensionBehaviour dimensionBehaviour;
        b bVar = this.u0;
        int i5 = bVar.e;
        int i6 = bVar.d;
        ConstraintWidget.DimensionBehaviour dimensionBehaviour2 = ConstraintWidget.DimensionBehaviour.FIXED;
        int childCount = getChildCount();
        if (i == Integer.MIN_VALUE) {
            dimensionBehaviour = ConstraintWidget.DimensionBehaviour.WRAP_CONTENT;
            if (childCount == 0) {
                i2 = Math.max(0, this.h0);
            }
        } else if (i == 0) {
            dimensionBehaviour = ConstraintWidget.DimensionBehaviour.WRAP_CONTENT;
            if (childCount == 0) {
                i2 = Math.max(0, this.h0);
            }
            i2 = 0;
        } else if (i != 1073741824) {
            dimensionBehaviour = dimensionBehaviour2;
            i2 = 0;
        } else {
            i2 = Math.min(this.j0 - i6, i2);
            dimensionBehaviour = dimensionBehaviour2;
        }
        if (i3 == Integer.MIN_VALUE) {
            dimensionBehaviour2 = ConstraintWidget.DimensionBehaviour.WRAP_CONTENT;
            if (childCount == 0) {
                i4 = Math.max(0, this.i0);
            }
        } else if (i3 != 0) {
            if (i3 == 1073741824) {
                i4 = Math.min(this.k0 - i5, i4);
            }
            i4 = 0;
        } else {
            dimensionBehaviour2 = ConstraintWidget.DimensionBehaviour.WRAP_CONTENT;
            if (childCount == 0) {
                i4 = Math.max(0, this.i0);
            }
            i4 = 0;
        }
        if (i2 != dVar.V() || i4 != dVar.z()) {
            dVar.K1();
        }
        dVar.j1(0);
        dVar.k1(0);
        dVar.U0(this.j0 - i6);
        dVar.T0(this.k0 - i5);
        dVar.X0(0);
        dVar.W0(0);
        dVar.M0(dimensionBehaviour);
        dVar.h1(i2);
        dVar.d1(dimensionBehaviour2);
        dVar.I0(i4);
        dVar.X0(this.h0 - i6);
        dVar.W0(this.i0 - i5);
    }

    @Override // android.view.View, android.view.ViewParent
    public void requestLayout() {
        m();
        super.requestLayout();
    }

    public final void s(ConstraintWidget constraintWidget, LayoutParams layoutParams, SparseArray<ConstraintWidget> sparseArray, int i, ConstraintAnchor.Type type) {
        View view = this.a.get(i);
        ConstraintWidget constraintWidget2 = sparseArray.get(i);
        if (constraintWidget2 == null || view == null || !(view.getLayoutParams() instanceof LayoutParams)) {
            return;
        }
        layoutParams.b0 = true;
        ConstraintAnchor.Type type2 = ConstraintAnchor.Type.BASELINE;
        if (type == type2) {
            LayoutParams layoutParams2 = (LayoutParams) view.getLayoutParams();
            layoutParams2.b0 = true;
            layoutParams2.q0.H0(true);
        }
        constraintWidget.q(type2).b(constraintWidget2.q(type), layoutParams.C, layoutParams.B, true);
        constraintWidget.H0(true);
        constraintWidget.q(ConstraintAnchor.Type.TOP).q();
        constraintWidget.q(ConstraintAnchor.Type.BOTTOM).q();
    }

    public void setConstraintSet(androidx.constraintlayout.widget.a aVar) {
        this.n0 = aVar;
    }

    public void setDesignInformation(int i, Object obj, Object obj2) {
        if (i == 0 && (obj instanceof String) && (obj2 instanceof Integer)) {
            if (this.q0 == null) {
                this.q0 = new HashMap<>();
            }
            String str = (String) obj;
            int indexOf = str.indexOf("/");
            if (indexOf != -1) {
                str = str.substring(indexOf + 1);
            }
            this.q0.put(str, Integer.valueOf(((Integer) obj2).intValue()));
        }
    }

    @Override // android.view.View
    public void setId(int i) {
        this.a.remove(getId());
        super.setId(i);
        this.a.put(getId(), this);
    }

    public void setMaxHeight(int i) {
        if (i == this.k0) {
            return;
        }
        this.k0 = i;
        requestLayout();
    }

    public void setMaxWidth(int i) {
        if (i == this.j0) {
            return;
        }
        this.j0 = i;
        requestLayout();
    }

    public void setMinHeight(int i) {
        if (i == this.i0) {
            return;
        }
        this.i0 = i;
        requestLayout();
    }

    public void setMinWidth(int i) {
        if (i == this.h0) {
            return;
        }
        this.h0 = i;
        requestLayout();
    }

    public void setOnConstraintsChanged(i60 i60Var) {
        e60 e60Var = this.o0;
        if (e60Var != null) {
            e60Var.c(i60Var);
        }
    }

    public void setOptimizationLevel(int i) {
        this.m0 = i;
        this.g0.T1(i);
    }

    public void setState(int i, int i2, int i3) {
        e60 e60Var = this.o0;
        if (e60Var != null) {
            e60Var.d(i, i2, i3);
        }
    }

    @Override // android.view.ViewGroup
    public boolean shouldDelayChildPressedState() {
        return false;
    }

    public final boolean t() {
        int childCount = getChildCount();
        boolean z = false;
        int i = 0;
        while (true) {
            if (i >= childCount) {
                break;
            } else if (getChildAt(i).isLayoutRequested()) {
                z = true;
                break;
            } else {
                i++;
            }
        }
        if (z) {
            q();
        }
        return z;
    }

    @Override // android.view.ViewGroup
    public ViewGroup.LayoutParams generateLayoutParams(ViewGroup.LayoutParams layoutParams) {
        return new LayoutParams(layoutParams);
    }

    public ConstraintLayout(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        this.a = new SparseArray<>();
        this.f0 = new ArrayList<>(4);
        this.g0 = new d();
        this.h0 = 0;
        this.i0 = 0;
        this.j0 = Integer.MAX_VALUE;
        this.k0 = Integer.MAX_VALUE;
        this.l0 = true;
        this.m0 = 257;
        this.n0 = null;
        this.o0 = null;
        this.p0 = -1;
        this.q0 = new HashMap<>();
        this.r0 = -1;
        this.s0 = -1;
        this.t0 = new SparseArray<>();
        this.u0 = new b(this);
        this.v0 = 0;
        this.w0 = 0;
        k(attributeSet, 0, 0);
    }

    public ConstraintLayout(Context context, AttributeSet attributeSet, int i) {
        super(context, attributeSet, i);
        this.a = new SparseArray<>();
        this.f0 = new ArrayList<>(4);
        this.g0 = new d();
        this.h0 = 0;
        this.i0 = 0;
        this.j0 = Integer.MAX_VALUE;
        this.k0 = Integer.MAX_VALUE;
        this.l0 = true;
        this.m0 = 257;
        this.n0 = null;
        this.o0 = null;
        this.p0 = -1;
        this.q0 = new HashMap<>();
        this.r0 = -1;
        this.s0 = -1;
        this.t0 = new SparseArray<>();
        this.u0 = new b(this);
        this.v0 = 0;
        this.w0 = 0;
        k(attributeSet, i, 0);
    }

    /* loaded from: classes.dex */
    public static class LayoutParams extends ViewGroup.MarginLayoutParams {
        public int A;
        public int B;
        public int C;
        public float D;
        public float E;
        public String F;
        public float G;
        public float H;
        public int I;
        public int J;
        public int K;
        public int L;
        public int M;
        public int N;
        public int O;
        public int P;
        public float Q;
        public float R;
        public int S;
        public int T;
        public int U;
        public boolean V;
        public boolean W;
        public String X;
        public int Y;
        public boolean Z;
        public int a;
        public boolean a0;
        public int b;
        public boolean b0;
        public float c;
        public boolean c0;
        public int d;
        public boolean d0;
        public int e;
        public boolean e0;
        public int f;
        public boolean f0;
        public int g;
        public int g0;
        public int h;
        public int h0;
        public int i;
        public int i0;
        public int j;
        public int j0;
        public int k;
        public int k0;
        public int l;
        public int l0;
        public int m;
        public float m0;
        public int n;
        public int n0;
        public int o;
        public int o0;
        public int p;
        public float p0;
        public float q;
        public ConstraintWidget q0;
        public int r;
        public int s;
        public int t;
        public int u;
        public int v;
        public int w;
        public int x;
        public int y;
        public int z;

        /* loaded from: classes.dex */
        public static class a {
            public static final SparseIntArray a;

            static {
                SparseIntArray sparseIntArray = new SparseIntArray();
                a = sparseIntArray;
                sparseIntArray.append(w23.ConstraintLayout_Layout_layout_constraintWidth, 64);
                sparseIntArray.append(w23.ConstraintLayout_Layout_layout_constraintHeight, 65);
                sparseIntArray.append(w23.ConstraintLayout_Layout_layout_constraintLeft_toLeftOf, 8);
                sparseIntArray.append(w23.ConstraintLayout_Layout_layout_constraintLeft_toRightOf, 9);
                sparseIntArray.append(w23.ConstraintLayout_Layout_layout_constraintRight_toLeftOf, 10);
                sparseIntArray.append(w23.ConstraintLayout_Layout_layout_constraintRight_toRightOf, 11);
                sparseIntArray.append(w23.ConstraintLayout_Layout_layout_constraintTop_toTopOf, 12);
                sparseIntArray.append(w23.ConstraintLayout_Layout_layout_constraintTop_toBottomOf, 13);
                sparseIntArray.append(w23.ConstraintLayout_Layout_layout_constraintBottom_toTopOf, 14);
                sparseIntArray.append(w23.ConstraintLayout_Layout_layout_constraintBottom_toBottomOf, 15);
                sparseIntArray.append(w23.ConstraintLayout_Layout_layout_constraintBaseline_toBaselineOf, 16);
                sparseIntArray.append(w23.ConstraintLayout_Layout_layout_constraintBaseline_toTopOf, 52);
                sparseIntArray.append(w23.ConstraintLayout_Layout_layout_constraintBaseline_toBottomOf, 53);
                sparseIntArray.append(w23.ConstraintLayout_Layout_layout_constraintCircle, 2);
                sparseIntArray.append(w23.ConstraintLayout_Layout_layout_constraintCircleRadius, 3);
                sparseIntArray.append(w23.ConstraintLayout_Layout_layout_constraintCircleAngle, 4);
                sparseIntArray.append(w23.ConstraintLayout_Layout_layout_editor_absoluteX, 49);
                sparseIntArray.append(w23.ConstraintLayout_Layout_layout_editor_absoluteY, 50);
                sparseIntArray.append(w23.ConstraintLayout_Layout_layout_constraintGuide_begin, 5);
                sparseIntArray.append(w23.ConstraintLayout_Layout_layout_constraintGuide_end, 6);
                sparseIntArray.append(w23.ConstraintLayout_Layout_layout_constraintGuide_percent, 7);
                sparseIntArray.append(w23.ConstraintLayout_Layout_android_orientation, 1);
                sparseIntArray.append(w23.ConstraintLayout_Layout_layout_constraintStart_toEndOf, 17);
                sparseIntArray.append(w23.ConstraintLayout_Layout_layout_constraintStart_toStartOf, 18);
                sparseIntArray.append(w23.ConstraintLayout_Layout_layout_constraintEnd_toStartOf, 19);
                sparseIntArray.append(w23.ConstraintLayout_Layout_layout_constraintEnd_toEndOf, 20);
                sparseIntArray.append(w23.ConstraintLayout_Layout_layout_goneMarginLeft, 21);
                sparseIntArray.append(w23.ConstraintLayout_Layout_layout_goneMarginTop, 22);
                sparseIntArray.append(w23.ConstraintLayout_Layout_layout_goneMarginRight, 23);
                sparseIntArray.append(w23.ConstraintLayout_Layout_layout_goneMarginBottom, 24);
                sparseIntArray.append(w23.ConstraintLayout_Layout_layout_goneMarginStart, 25);
                sparseIntArray.append(w23.ConstraintLayout_Layout_layout_goneMarginEnd, 26);
                sparseIntArray.append(w23.ConstraintLayout_Layout_layout_goneMarginBaseline, 55);
                sparseIntArray.append(w23.ConstraintLayout_Layout_layout_marginBaseline, 54);
                sparseIntArray.append(w23.ConstraintLayout_Layout_layout_constraintHorizontal_bias, 29);
                sparseIntArray.append(w23.ConstraintLayout_Layout_layout_constraintVertical_bias, 30);
                sparseIntArray.append(w23.ConstraintLayout_Layout_layout_constraintDimensionRatio, 44);
                sparseIntArray.append(w23.ConstraintLayout_Layout_layout_constraintHorizontal_weight, 45);
                sparseIntArray.append(w23.ConstraintLayout_Layout_layout_constraintVertical_weight, 46);
                sparseIntArray.append(w23.ConstraintLayout_Layout_layout_constraintHorizontal_chainStyle, 47);
                sparseIntArray.append(w23.ConstraintLayout_Layout_layout_constraintVertical_chainStyle, 48);
                sparseIntArray.append(w23.ConstraintLayout_Layout_layout_constrainedWidth, 27);
                sparseIntArray.append(w23.ConstraintLayout_Layout_layout_constrainedHeight, 28);
                sparseIntArray.append(w23.ConstraintLayout_Layout_layout_constraintWidth_default, 31);
                sparseIntArray.append(w23.ConstraintLayout_Layout_layout_constraintHeight_default, 32);
                sparseIntArray.append(w23.ConstraintLayout_Layout_layout_constraintWidth_min, 33);
                sparseIntArray.append(w23.ConstraintLayout_Layout_layout_constraintWidth_max, 34);
                sparseIntArray.append(w23.ConstraintLayout_Layout_layout_constraintWidth_percent, 35);
                sparseIntArray.append(w23.ConstraintLayout_Layout_layout_constraintHeight_min, 36);
                sparseIntArray.append(w23.ConstraintLayout_Layout_layout_constraintHeight_max, 37);
                sparseIntArray.append(w23.ConstraintLayout_Layout_layout_constraintHeight_percent, 38);
                sparseIntArray.append(w23.ConstraintLayout_Layout_layout_constraintLeft_creator, 39);
                sparseIntArray.append(w23.ConstraintLayout_Layout_layout_constraintTop_creator, 40);
                sparseIntArray.append(w23.ConstraintLayout_Layout_layout_constraintRight_creator, 41);
                sparseIntArray.append(w23.ConstraintLayout_Layout_layout_constraintBottom_creator, 42);
                sparseIntArray.append(w23.ConstraintLayout_Layout_layout_constraintBaseline_creator, 43);
                sparseIntArray.append(w23.ConstraintLayout_Layout_layout_constraintTag, 51);
                sparseIntArray.append(w23.ConstraintLayout_Layout_layout_wrapBehaviorInParent, 66);
            }
        }

        public LayoutParams(Context context, AttributeSet attributeSet) {
            super(context, attributeSet);
            this.a = -1;
            this.b = -1;
            this.c = -1.0f;
            this.d = -1;
            this.e = -1;
            this.f = -1;
            this.g = -1;
            this.h = -1;
            this.i = -1;
            this.j = -1;
            this.k = -1;
            this.l = -1;
            this.m = -1;
            this.n = -1;
            this.o = -1;
            this.p = 0;
            this.q = Utils.FLOAT_EPSILON;
            this.r = -1;
            this.s = -1;
            this.t = -1;
            this.u = -1;
            this.v = Integer.MIN_VALUE;
            this.w = Integer.MIN_VALUE;
            this.x = Integer.MIN_VALUE;
            this.y = Integer.MIN_VALUE;
            this.z = Integer.MIN_VALUE;
            this.A = Integer.MIN_VALUE;
            this.B = Integer.MIN_VALUE;
            this.C = 0;
            this.D = 0.5f;
            this.E = 0.5f;
            this.F = null;
            this.G = -1.0f;
            this.H = -1.0f;
            this.I = 0;
            this.J = 0;
            this.K = 0;
            this.L = 0;
            this.M = 0;
            this.N = 0;
            this.O = 0;
            this.P = 0;
            this.Q = 1.0f;
            this.R = 1.0f;
            this.S = -1;
            this.T = -1;
            this.U = -1;
            this.V = false;
            this.W = false;
            this.X = null;
            this.Y = 0;
            this.Z = true;
            this.a0 = true;
            this.b0 = false;
            this.c0 = false;
            this.d0 = false;
            this.e0 = false;
            this.f0 = false;
            this.g0 = -1;
            this.h0 = -1;
            this.i0 = -1;
            this.j0 = -1;
            this.k0 = Integer.MIN_VALUE;
            this.l0 = Integer.MIN_VALUE;
            this.m0 = 0.5f;
            this.q0 = new ConstraintWidget();
            TypedArray obtainStyledAttributes = context.obtainStyledAttributes(attributeSet, w23.ConstraintLayout_Layout);
            int indexCount = obtainStyledAttributes.getIndexCount();
            for (int i = 0; i < indexCount; i++) {
                int index = obtainStyledAttributes.getIndex(i);
                int i2 = a.a.get(index);
                switch (i2) {
                    case 1:
                        this.U = obtainStyledAttributes.getInt(index, this.U);
                        break;
                    case 2:
                        int resourceId = obtainStyledAttributes.getResourceId(index, this.o);
                        this.o = resourceId;
                        if (resourceId == -1) {
                            this.o = obtainStyledAttributes.getInt(index, -1);
                            break;
                        } else {
                            break;
                        }
                    case 3:
                        this.p = obtainStyledAttributes.getDimensionPixelSize(index, this.p);
                        break;
                    case 4:
                        float f = obtainStyledAttributes.getFloat(index, this.q) % 360.0f;
                        this.q = f;
                        if (f < Utils.FLOAT_EPSILON) {
                            this.q = (360.0f - f) % 360.0f;
                            break;
                        } else {
                            break;
                        }
                    case 5:
                        this.a = obtainStyledAttributes.getDimensionPixelOffset(index, this.a);
                        break;
                    case 6:
                        this.b = obtainStyledAttributes.getDimensionPixelOffset(index, this.b);
                        break;
                    case 7:
                        this.c = obtainStyledAttributes.getFloat(index, this.c);
                        break;
                    case 8:
                        int resourceId2 = obtainStyledAttributes.getResourceId(index, this.d);
                        this.d = resourceId2;
                        if (resourceId2 == -1) {
                            this.d = obtainStyledAttributes.getInt(index, -1);
                            break;
                        } else {
                            break;
                        }
                    case 9:
                        int resourceId3 = obtainStyledAttributes.getResourceId(index, this.e);
                        this.e = resourceId3;
                        if (resourceId3 == -1) {
                            this.e = obtainStyledAttributes.getInt(index, -1);
                            break;
                        } else {
                            break;
                        }
                    case 10:
                        int resourceId4 = obtainStyledAttributes.getResourceId(index, this.f);
                        this.f = resourceId4;
                        if (resourceId4 == -1) {
                            this.f = obtainStyledAttributes.getInt(index, -1);
                            break;
                        } else {
                            break;
                        }
                    case 11:
                        int resourceId5 = obtainStyledAttributes.getResourceId(index, this.g);
                        this.g = resourceId5;
                        if (resourceId5 == -1) {
                            this.g = obtainStyledAttributes.getInt(index, -1);
                            break;
                        } else {
                            break;
                        }
                    case 12:
                        int resourceId6 = obtainStyledAttributes.getResourceId(index, this.h);
                        this.h = resourceId6;
                        if (resourceId6 == -1) {
                            this.h = obtainStyledAttributes.getInt(index, -1);
                            break;
                        } else {
                            break;
                        }
                    case 13:
                        int resourceId7 = obtainStyledAttributes.getResourceId(index, this.i);
                        this.i = resourceId7;
                        if (resourceId7 == -1) {
                            this.i = obtainStyledAttributes.getInt(index, -1);
                            break;
                        } else {
                            break;
                        }
                    case 14:
                        int resourceId8 = obtainStyledAttributes.getResourceId(index, this.j);
                        this.j = resourceId8;
                        if (resourceId8 == -1) {
                            this.j = obtainStyledAttributes.getInt(index, -1);
                            break;
                        } else {
                            break;
                        }
                    case 15:
                        int resourceId9 = obtainStyledAttributes.getResourceId(index, this.k);
                        this.k = resourceId9;
                        if (resourceId9 == -1) {
                            this.k = obtainStyledAttributes.getInt(index, -1);
                            break;
                        } else {
                            break;
                        }
                    case 16:
                        int resourceId10 = obtainStyledAttributes.getResourceId(index, this.l);
                        this.l = resourceId10;
                        if (resourceId10 == -1) {
                            this.l = obtainStyledAttributes.getInt(index, -1);
                            break;
                        } else {
                            break;
                        }
                    case 17:
                        int resourceId11 = obtainStyledAttributes.getResourceId(index, this.r);
                        this.r = resourceId11;
                        if (resourceId11 == -1) {
                            this.r = obtainStyledAttributes.getInt(index, -1);
                            break;
                        } else {
                            break;
                        }
                    case 18:
                        int resourceId12 = obtainStyledAttributes.getResourceId(index, this.s);
                        this.s = resourceId12;
                        if (resourceId12 == -1) {
                            this.s = obtainStyledAttributes.getInt(index, -1);
                            break;
                        } else {
                            break;
                        }
                    case 19:
                        int resourceId13 = obtainStyledAttributes.getResourceId(index, this.t);
                        this.t = resourceId13;
                        if (resourceId13 == -1) {
                            this.t = obtainStyledAttributes.getInt(index, -1);
                            break;
                        } else {
                            break;
                        }
                    case 20:
                        int resourceId14 = obtainStyledAttributes.getResourceId(index, this.u);
                        this.u = resourceId14;
                        if (resourceId14 == -1) {
                            this.u = obtainStyledAttributes.getInt(index, -1);
                            break;
                        } else {
                            break;
                        }
                    case 21:
                        this.v = obtainStyledAttributes.getDimensionPixelSize(index, this.v);
                        break;
                    case 22:
                        this.w = obtainStyledAttributes.getDimensionPixelSize(index, this.w);
                        break;
                    case 23:
                        this.x = obtainStyledAttributes.getDimensionPixelSize(index, this.x);
                        break;
                    case 24:
                        this.y = obtainStyledAttributes.getDimensionPixelSize(index, this.y);
                        break;
                    case 25:
                        this.z = obtainStyledAttributes.getDimensionPixelSize(index, this.z);
                        break;
                    case 26:
                        this.A = obtainStyledAttributes.getDimensionPixelSize(index, this.A);
                        break;
                    case 27:
                        this.V = obtainStyledAttributes.getBoolean(index, this.V);
                        break;
                    case 28:
                        this.W = obtainStyledAttributes.getBoolean(index, this.W);
                        break;
                    case 29:
                        this.D = obtainStyledAttributes.getFloat(index, this.D);
                        break;
                    case 30:
                        this.E = obtainStyledAttributes.getFloat(index, this.E);
                        break;
                    case 31:
                        this.K = obtainStyledAttributes.getInt(index, 0);
                        break;
                    case 32:
                        this.L = obtainStyledAttributes.getInt(index, 0);
                        break;
                    case 33:
                        try {
                            this.M = obtainStyledAttributes.getDimensionPixelSize(index, this.M);
                            break;
                        } catch (Exception unused) {
                            if (obtainStyledAttributes.getInt(index, this.M) == -2) {
                                this.M = -2;
                                break;
                            } else {
                                break;
                            }
                        }
                    case 34:
                        try {
                            this.O = obtainStyledAttributes.getDimensionPixelSize(index, this.O);
                            break;
                        } catch (Exception unused2) {
                            if (obtainStyledAttributes.getInt(index, this.O) == -2) {
                                this.O = -2;
                                break;
                            } else {
                                break;
                            }
                        }
                    case 35:
                        this.Q = Math.max((float) Utils.FLOAT_EPSILON, obtainStyledAttributes.getFloat(index, this.Q));
                        this.K = 2;
                        break;
                    case 36:
                        try {
                            this.N = obtainStyledAttributes.getDimensionPixelSize(index, this.N);
                            break;
                        } catch (Exception unused3) {
                            if (obtainStyledAttributes.getInt(index, this.N) == -2) {
                                this.N = -2;
                                break;
                            } else {
                                break;
                            }
                        }
                    case 37:
                        try {
                            this.P = obtainStyledAttributes.getDimensionPixelSize(index, this.P);
                            break;
                        } catch (Exception unused4) {
                            if (obtainStyledAttributes.getInt(index, this.P) == -2) {
                                this.P = -2;
                                break;
                            } else {
                                break;
                            }
                        }
                    case 38:
                        this.R = Math.max((float) Utils.FLOAT_EPSILON, obtainStyledAttributes.getFloat(index, this.R));
                        this.L = 2;
                        break;
                    default:
                        switch (i2) {
                            case 44:
                                androidx.constraintlayout.widget.a.I(this, obtainStyledAttributes.getString(index));
                                continue;
                            case 45:
                                this.G = obtainStyledAttributes.getFloat(index, this.G);
                                continue;
                            case 46:
                                this.H = obtainStyledAttributes.getFloat(index, this.H);
                                continue;
                            case 47:
                                this.I = obtainStyledAttributes.getInt(index, 0);
                                continue;
                            case 48:
                                this.J = obtainStyledAttributes.getInt(index, 0);
                                continue;
                            case 49:
                                this.S = obtainStyledAttributes.getDimensionPixelOffset(index, this.S);
                                continue;
                            case 50:
                                this.T = obtainStyledAttributes.getDimensionPixelOffset(index, this.T);
                                continue;
                            case 51:
                                this.X = obtainStyledAttributes.getString(index);
                                continue;
                            case 52:
                                int resourceId15 = obtainStyledAttributes.getResourceId(index, this.m);
                                this.m = resourceId15;
                                if (resourceId15 == -1) {
                                    this.m = obtainStyledAttributes.getInt(index, -1);
                                    break;
                                } else {
                                    continue;
                                }
                            case 53:
                                int resourceId16 = obtainStyledAttributes.getResourceId(index, this.n);
                                this.n = resourceId16;
                                if (resourceId16 == -1) {
                                    this.n = obtainStyledAttributes.getInt(index, -1);
                                    break;
                                } else {
                                    continue;
                                }
                            case 54:
                                this.C = obtainStyledAttributes.getDimensionPixelSize(index, this.C);
                                continue;
                            case 55:
                                this.B = obtainStyledAttributes.getDimensionPixelSize(index, this.B);
                                continue;
                            default:
                                switch (i2) {
                                    case 64:
                                        androidx.constraintlayout.widget.a.G(this, obtainStyledAttributes, index, 0);
                                        continue;
                                    case 65:
                                        androidx.constraintlayout.widget.a.G(this, obtainStyledAttributes, index, 1);
                                        continue;
                                    case 66:
                                        this.Y = obtainStyledAttributes.getInt(index, this.Y);
                                        continue;
                                        continue;
                                }
                        }
                }
            }
            obtainStyledAttributes.recycle();
            c();
        }

        public String a() {
            return this.X;
        }

        public ConstraintWidget b() {
            return this.q0;
        }

        public void c() {
            this.c0 = false;
            this.Z = true;
            this.a0 = true;
            int i = ((ViewGroup.MarginLayoutParams) this).width;
            if (i == -2 && this.V) {
                this.Z = false;
                if (this.K == 0) {
                    this.K = 1;
                }
            }
            int i2 = ((ViewGroup.MarginLayoutParams) this).height;
            if (i2 == -2 && this.W) {
                this.a0 = false;
                if (this.L == 0) {
                    this.L = 1;
                }
            }
            if (i == 0 || i == -1) {
                this.Z = false;
                if (i == 0 && this.K == 1) {
                    ((ViewGroup.MarginLayoutParams) this).width = -2;
                    this.V = true;
                }
            }
            if (i2 == 0 || i2 == -1) {
                this.a0 = false;
                if (i2 == 0 && this.L == 1) {
                    ((ViewGroup.MarginLayoutParams) this).height = -2;
                    this.W = true;
                }
            }
            if (this.c == -1.0f && this.a == -1 && this.b == -1) {
                return;
            }
            this.c0 = true;
            this.Z = true;
            this.a0 = true;
            if (!(this.q0 instanceof f)) {
                this.q0 = new f();
            }
            ((f) this.q0).x1(this.U);
        }

        /* JADX WARN: Removed duplicated region for block: B:10:0x0041  */
        /* JADX WARN: Removed duplicated region for block: B:19:0x0054  */
        /* JADX WARN: Removed duplicated region for block: B:22:0x005b  */
        /* JADX WARN: Removed duplicated region for block: B:25:0x0062  */
        /* JADX WARN: Removed duplicated region for block: B:28:0x0068  */
        /* JADX WARN: Removed duplicated region for block: B:31:0x006e  */
        /* JADX WARN: Removed duplicated region for block: B:38:0x0080  */
        /* JADX WARN: Removed duplicated region for block: B:39:0x0088  */
        /* JADX WARN: Removed duplicated region for block: B:43:0x009a  */
        @Override // android.view.ViewGroup.MarginLayoutParams, android.view.ViewGroup.LayoutParams
        @android.annotation.TargetApi(17)
        /*
            Code decompiled incorrectly, please refer to instructions dump.
            To view partially-correct code enable 'Show inconsistent code' option in preferences
        */
        public void resolveLayoutDirection(int r11) {
            /*
                Method dump skipped, instructions count: 265
                To view this dump change 'Code comments level' option to 'DEBUG'
            */
            throw new UnsupportedOperationException("Method not decompiled: androidx.constraintlayout.widget.ConstraintLayout.LayoutParams.resolveLayoutDirection(int):void");
        }

        public LayoutParams(int i, int i2) {
            super(i, i2);
            this.a = -1;
            this.b = -1;
            this.c = -1.0f;
            this.d = -1;
            this.e = -1;
            this.f = -1;
            this.g = -1;
            this.h = -1;
            this.i = -1;
            this.j = -1;
            this.k = -1;
            this.l = -1;
            this.m = -1;
            this.n = -1;
            this.o = -1;
            this.p = 0;
            this.q = Utils.FLOAT_EPSILON;
            this.r = -1;
            this.s = -1;
            this.t = -1;
            this.u = -1;
            this.v = Integer.MIN_VALUE;
            this.w = Integer.MIN_VALUE;
            this.x = Integer.MIN_VALUE;
            this.y = Integer.MIN_VALUE;
            this.z = Integer.MIN_VALUE;
            this.A = Integer.MIN_VALUE;
            this.B = Integer.MIN_VALUE;
            this.C = 0;
            this.D = 0.5f;
            this.E = 0.5f;
            this.F = null;
            this.G = -1.0f;
            this.H = -1.0f;
            this.I = 0;
            this.J = 0;
            this.K = 0;
            this.L = 0;
            this.M = 0;
            this.N = 0;
            this.O = 0;
            this.P = 0;
            this.Q = 1.0f;
            this.R = 1.0f;
            this.S = -1;
            this.T = -1;
            this.U = -1;
            this.V = false;
            this.W = false;
            this.X = null;
            this.Y = 0;
            this.Z = true;
            this.a0 = true;
            this.b0 = false;
            this.c0 = false;
            this.d0 = false;
            this.e0 = false;
            this.f0 = false;
            this.g0 = -1;
            this.h0 = -1;
            this.i0 = -1;
            this.j0 = -1;
            this.k0 = Integer.MIN_VALUE;
            this.l0 = Integer.MIN_VALUE;
            this.m0 = 0.5f;
            this.q0 = new ConstraintWidget();
        }

        public LayoutParams(ViewGroup.LayoutParams layoutParams) {
            super(layoutParams);
            this.a = -1;
            this.b = -1;
            this.c = -1.0f;
            this.d = -1;
            this.e = -1;
            this.f = -1;
            this.g = -1;
            this.h = -1;
            this.i = -1;
            this.j = -1;
            this.k = -1;
            this.l = -1;
            this.m = -1;
            this.n = -1;
            this.o = -1;
            this.p = 0;
            this.q = Utils.FLOAT_EPSILON;
            this.r = -1;
            this.s = -1;
            this.t = -1;
            this.u = -1;
            this.v = Integer.MIN_VALUE;
            this.w = Integer.MIN_VALUE;
            this.x = Integer.MIN_VALUE;
            this.y = Integer.MIN_VALUE;
            this.z = Integer.MIN_VALUE;
            this.A = Integer.MIN_VALUE;
            this.B = Integer.MIN_VALUE;
            this.C = 0;
            this.D = 0.5f;
            this.E = 0.5f;
            this.F = null;
            this.G = -1.0f;
            this.H = -1.0f;
            this.I = 0;
            this.J = 0;
            this.K = 0;
            this.L = 0;
            this.M = 0;
            this.N = 0;
            this.O = 0;
            this.P = 0;
            this.Q = 1.0f;
            this.R = 1.0f;
            this.S = -1;
            this.T = -1;
            this.U = -1;
            this.V = false;
            this.W = false;
            this.X = null;
            this.Y = 0;
            this.Z = true;
            this.a0 = true;
            this.b0 = false;
            this.c0 = false;
            this.d0 = false;
            this.e0 = false;
            this.f0 = false;
            this.g0 = -1;
            this.h0 = -1;
            this.i0 = -1;
            this.j0 = -1;
            this.k0 = Integer.MIN_VALUE;
            this.l0 = Integer.MIN_VALUE;
            this.m0 = 0.5f;
            this.q0 = new ConstraintWidget();
        }
    }
}
