package androidx.constraintlayout.widget;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Canvas;
import android.util.AttributeSet;
import android.view.View;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.constraintlayout.widget.b;

/* loaded from: classes.dex */
public class ReactiveGuide extends View implements b.a {
    public int a;
    public boolean f0;
    public int g0;
    public boolean h0;

    public ReactiveGuide(Context context) {
        super(context);
        this.a = -1;
        this.f0 = false;
        this.g0 = 0;
        this.h0 = true;
        super.setVisibility(8);
        a(null);
    }

    public final void a(AttributeSet attributeSet) {
        if (attributeSet != null) {
            TypedArray obtainStyledAttributes = getContext().obtainStyledAttributes(attributeSet, w23.ConstraintLayout_ReactiveGuide);
            int indexCount = obtainStyledAttributes.getIndexCount();
            for (int i = 0; i < indexCount; i++) {
                int index = obtainStyledAttributes.getIndex(i);
                if (index == w23.ConstraintLayout_ReactiveGuide_reactiveGuide_valueId) {
                    this.a = obtainStyledAttributes.getResourceId(index, this.a);
                } else if (index == w23.ConstraintLayout_ReactiveGuide_reactiveGuide_animateChange) {
                    this.f0 = obtainStyledAttributes.getBoolean(index, this.f0);
                } else if (index == w23.ConstraintLayout_ReactiveGuide_reactiveGuide_applyToConstraintSet) {
                    this.g0 = obtainStyledAttributes.getResourceId(index, this.g0);
                } else if (index == w23.ConstraintLayout_ReactiveGuide_reactiveGuide_applyToAllConstraintSets) {
                    this.h0 = obtainStyledAttributes.getBoolean(index, this.h0);
                }
            }
            obtainStyledAttributes.recycle();
        }
        if (this.a != -1) {
            ConstraintLayout.getSharedValues().a(this.a, this);
        }
    }

    @Override // android.view.View
    @SuppressLint({"MissingSuperCall"})
    public void draw(Canvas canvas) {
    }

    public int getApplyToConstraintSetId() {
        return this.g0;
    }

    public int getAttributeId() {
        return this.a;
    }

    @Override // android.view.View
    public void onMeasure(int i, int i2) {
        setMeasuredDimension(0, 0);
    }

    public void setAnimateChange(boolean z) {
        this.f0 = z;
    }

    public void setApplyToConstraintSetId(int i) {
        this.g0 = i;
    }

    public void setAttributeId(int i) {
        b sharedValues = ConstraintLayout.getSharedValues();
        int i2 = this.a;
        if (i2 != -1) {
            sharedValues.b(i2, this);
        }
        this.a = i;
        if (i != -1) {
            sharedValues.a(i, this);
        }
    }

    public void setGuidelineBegin(int i) {
        ConstraintLayout.LayoutParams layoutParams = (ConstraintLayout.LayoutParams) getLayoutParams();
        layoutParams.a = i;
        setLayoutParams(layoutParams);
    }

    public void setGuidelineEnd(int i) {
        ConstraintLayout.LayoutParams layoutParams = (ConstraintLayout.LayoutParams) getLayoutParams();
        layoutParams.b = i;
        setLayoutParams(layoutParams);
    }

    public void setGuidelinePercent(float f) {
        ConstraintLayout.LayoutParams layoutParams = (ConstraintLayout.LayoutParams) getLayoutParams();
        layoutParams.c = f;
        setLayoutParams(layoutParams);
    }

    @Override // android.view.View
    public void setVisibility(int i) {
    }

    public ReactiveGuide(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        this.a = -1;
        this.f0 = false;
        this.g0 = 0;
        this.h0 = true;
        super.setVisibility(8);
        a(attributeSet);
    }

    public ReactiveGuide(Context context, AttributeSet attributeSet, int i) {
        super(context, attributeSet, i);
        this.a = -1;
        this.f0 = false;
        this.g0 = 0;
        this.h0 = true;
        super.setVisibility(8);
        a(attributeSet);
    }
}
