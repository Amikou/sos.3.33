package androidx.constraintlayout.widget;

import android.content.Context;
import android.content.res.TypedArray;
import android.content.res.XmlResourceParser;
import android.os.Build;
import android.util.AttributeSet;
import android.util.SparseArray;
import android.util.SparseIntArray;
import android.util.Xml;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import androidx.constraintlayout.core.widgets.ConstraintWidget;
import androidx.constraintlayout.motion.widget.MotionLayout;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.constraintlayout.widget.Constraints;
import com.github.mikephil.charting.utils.Utils;
import java.io.IOException;
import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserException;

/* compiled from: ConstraintSet.java */
/* loaded from: classes.dex */
public class a {
    public static final int[] g = {0, 4, 8};
    public static SparseIntArray h = new SparseIntArray();
    public static SparseIntArray i = new SparseIntArray();
    public String a;
    public String b = "";
    public int c = 0;
    public HashMap<String, ConstraintAttribute> d = new HashMap<>();
    public boolean e = true;
    public HashMap<Integer, C0021a> f = new HashMap<>();

    /* compiled from: ConstraintSet.java */
    /* renamed from: androidx.constraintlayout.widget.a$a  reason: collision with other inner class name */
    /* loaded from: classes.dex */
    public static class C0021a {
        public int a;
        public String b;
        public final d c = new d();
        public final c d = new c();
        public final b e = new b();
        public final e f = new e();
        public HashMap<String, ConstraintAttribute> g = new HashMap<>();
        public C0022a h;

        /* compiled from: ConstraintSet.java */
        /* renamed from: androidx.constraintlayout.widget.a$a$a  reason: collision with other inner class name */
        /* loaded from: classes.dex */
        public static class C0022a {
            public int[] a = new int[10];
            public int[] b = new int[10];
            public int c = 0;
            public int[] d = new int[10];
            public float[] e = new float[10];
            public int f = 0;
            public int[] g = new int[5];
            public String[] h = new String[5];
            public int i = 0;
            public int[] j = new int[4];
            public boolean[] k = new boolean[4];
            public int l = 0;

            public void a(int i, float f) {
                int i2 = this.f;
                int[] iArr = this.d;
                if (i2 >= iArr.length) {
                    this.d = Arrays.copyOf(iArr, iArr.length * 2);
                    float[] fArr = this.e;
                    this.e = Arrays.copyOf(fArr, fArr.length * 2);
                }
                int[] iArr2 = this.d;
                int i3 = this.f;
                iArr2[i3] = i;
                float[] fArr2 = this.e;
                this.f = i3 + 1;
                fArr2[i3] = f;
            }

            public void b(int i, int i2) {
                int i3 = this.c;
                int[] iArr = this.a;
                if (i3 >= iArr.length) {
                    this.a = Arrays.copyOf(iArr, iArr.length * 2);
                    int[] iArr2 = this.b;
                    this.b = Arrays.copyOf(iArr2, iArr2.length * 2);
                }
                int[] iArr3 = this.a;
                int i4 = this.c;
                iArr3[i4] = i;
                int[] iArr4 = this.b;
                this.c = i4 + 1;
                iArr4[i4] = i2;
            }

            public void c(int i, String str) {
                int i2 = this.i;
                int[] iArr = this.g;
                if (i2 >= iArr.length) {
                    this.g = Arrays.copyOf(iArr, iArr.length * 2);
                    String[] strArr = this.h;
                    this.h = (String[]) Arrays.copyOf(strArr, strArr.length * 2);
                }
                int[] iArr2 = this.g;
                int i3 = this.i;
                iArr2[i3] = i;
                String[] strArr2 = this.h;
                this.i = i3 + 1;
                strArr2[i3] = str;
            }

            public void d(int i, boolean z) {
                int i2 = this.l;
                int[] iArr = this.j;
                if (i2 >= iArr.length) {
                    this.j = Arrays.copyOf(iArr, iArr.length * 2);
                    boolean[] zArr = this.k;
                    this.k = Arrays.copyOf(zArr, zArr.length * 2);
                }
                int[] iArr2 = this.j;
                int i3 = this.l;
                iArr2[i3] = i;
                boolean[] zArr2 = this.k;
                this.l = i3 + 1;
                zArr2[i3] = z;
            }

            public void e(C0021a c0021a) {
                for (int i = 0; i < this.c; i++) {
                    a.O(c0021a, this.a[i], this.b[i]);
                }
                for (int i2 = 0; i2 < this.f; i2++) {
                    a.N(c0021a, this.d[i2], this.e[i2]);
                }
                for (int i3 = 0; i3 < this.i; i3++) {
                    a.P(c0021a, this.g[i3], this.h[i3]);
                }
                for (int i4 = 0; i4 < this.l; i4++) {
                    a.Q(c0021a, this.j[i4], this.k[i4]);
                }
            }
        }

        public void d(C0021a c0021a) {
            C0022a c0022a = this.h;
            if (c0022a != null) {
                c0022a.e(c0021a);
            }
        }

        public void e(ConstraintLayout.LayoutParams layoutParams) {
            b bVar = this.e;
            layoutParams.d = bVar.h;
            layoutParams.e = bVar.i;
            layoutParams.f = bVar.j;
            layoutParams.g = bVar.k;
            layoutParams.h = bVar.l;
            layoutParams.i = bVar.m;
            layoutParams.j = bVar.n;
            layoutParams.k = bVar.o;
            layoutParams.l = bVar.p;
            layoutParams.m = bVar.q;
            layoutParams.n = bVar.r;
            layoutParams.r = bVar.s;
            layoutParams.s = bVar.t;
            layoutParams.t = bVar.u;
            layoutParams.u = bVar.v;
            ((ViewGroup.MarginLayoutParams) layoutParams).leftMargin = bVar.F;
            ((ViewGroup.MarginLayoutParams) layoutParams).rightMargin = bVar.G;
            ((ViewGroup.MarginLayoutParams) layoutParams).topMargin = bVar.H;
            ((ViewGroup.MarginLayoutParams) layoutParams).bottomMargin = bVar.I;
            layoutParams.z = bVar.R;
            layoutParams.A = bVar.Q;
            layoutParams.w = bVar.N;
            layoutParams.y = bVar.P;
            layoutParams.D = bVar.w;
            layoutParams.E = bVar.x;
            layoutParams.o = bVar.z;
            layoutParams.p = bVar.A;
            layoutParams.q = bVar.B;
            layoutParams.F = bVar.y;
            layoutParams.S = bVar.C;
            layoutParams.T = bVar.D;
            layoutParams.H = bVar.T;
            layoutParams.G = bVar.U;
            layoutParams.J = bVar.W;
            layoutParams.I = bVar.V;
            layoutParams.V = bVar.l0;
            layoutParams.W = bVar.m0;
            layoutParams.K = bVar.X;
            layoutParams.L = bVar.Y;
            layoutParams.O = bVar.Z;
            layoutParams.P = bVar.a0;
            layoutParams.M = bVar.b0;
            layoutParams.N = bVar.c0;
            layoutParams.Q = bVar.d0;
            layoutParams.R = bVar.e0;
            layoutParams.U = bVar.E;
            layoutParams.c = bVar.g;
            layoutParams.a = bVar.e;
            layoutParams.b = bVar.f;
            ((ViewGroup.MarginLayoutParams) layoutParams).width = bVar.c;
            ((ViewGroup.MarginLayoutParams) layoutParams).height = bVar.d;
            String str = bVar.k0;
            if (str != null) {
                layoutParams.X = str;
            }
            layoutParams.Y = bVar.o0;
            if (Build.VERSION.SDK_INT >= 17) {
                layoutParams.setMarginStart(bVar.K);
                layoutParams.setMarginEnd(this.e.J);
            }
            layoutParams.c();
        }

        /* renamed from: f */
        public C0021a clone() {
            C0021a c0021a = new C0021a();
            c0021a.e.a(this.e);
            c0021a.d.a(this.d);
            c0021a.c.a(this.c);
            c0021a.f.a(this.f);
            c0021a.a = this.a;
            c0021a.h = this.h;
            return c0021a;
        }

        public final void g(int i, ConstraintLayout.LayoutParams layoutParams) {
            this.a = i;
            b bVar = this.e;
            bVar.h = layoutParams.d;
            bVar.i = layoutParams.e;
            bVar.j = layoutParams.f;
            bVar.k = layoutParams.g;
            bVar.l = layoutParams.h;
            bVar.m = layoutParams.i;
            bVar.n = layoutParams.j;
            bVar.o = layoutParams.k;
            bVar.p = layoutParams.l;
            bVar.q = layoutParams.m;
            bVar.r = layoutParams.n;
            bVar.s = layoutParams.r;
            bVar.t = layoutParams.s;
            bVar.u = layoutParams.t;
            bVar.v = layoutParams.u;
            bVar.w = layoutParams.D;
            bVar.x = layoutParams.E;
            bVar.y = layoutParams.F;
            bVar.z = layoutParams.o;
            bVar.A = layoutParams.p;
            bVar.B = layoutParams.q;
            bVar.C = layoutParams.S;
            bVar.D = layoutParams.T;
            bVar.E = layoutParams.U;
            bVar.g = layoutParams.c;
            bVar.e = layoutParams.a;
            bVar.f = layoutParams.b;
            bVar.c = ((ViewGroup.MarginLayoutParams) layoutParams).width;
            bVar.d = ((ViewGroup.MarginLayoutParams) layoutParams).height;
            bVar.F = ((ViewGroup.MarginLayoutParams) layoutParams).leftMargin;
            bVar.G = ((ViewGroup.MarginLayoutParams) layoutParams).rightMargin;
            bVar.H = ((ViewGroup.MarginLayoutParams) layoutParams).topMargin;
            bVar.I = ((ViewGroup.MarginLayoutParams) layoutParams).bottomMargin;
            bVar.L = layoutParams.C;
            bVar.T = layoutParams.H;
            bVar.U = layoutParams.G;
            bVar.W = layoutParams.J;
            bVar.V = layoutParams.I;
            bVar.l0 = layoutParams.V;
            bVar.m0 = layoutParams.W;
            bVar.X = layoutParams.K;
            bVar.Y = layoutParams.L;
            bVar.Z = layoutParams.O;
            bVar.a0 = layoutParams.P;
            bVar.b0 = layoutParams.M;
            bVar.c0 = layoutParams.N;
            bVar.d0 = layoutParams.Q;
            bVar.e0 = layoutParams.R;
            bVar.k0 = layoutParams.X;
            bVar.N = layoutParams.w;
            bVar.P = layoutParams.y;
            bVar.M = layoutParams.v;
            bVar.O = layoutParams.x;
            bVar.R = layoutParams.z;
            bVar.Q = layoutParams.A;
            bVar.S = layoutParams.B;
            bVar.o0 = layoutParams.Y;
            if (Build.VERSION.SDK_INT >= 17) {
                bVar.J = layoutParams.getMarginEnd();
                this.e.K = layoutParams.getMarginStart();
            }
        }

        public final void h(int i, Constraints.LayoutParams layoutParams) {
            g(i, layoutParams);
            this.c.d = layoutParams.r0;
            e eVar = this.f;
            eVar.b = layoutParams.u0;
            eVar.c = layoutParams.v0;
            eVar.d = layoutParams.w0;
            eVar.e = layoutParams.x0;
            eVar.f = layoutParams.y0;
            eVar.g = layoutParams.z0;
            eVar.h = layoutParams.A0;
            eVar.j = layoutParams.B0;
            eVar.k = layoutParams.C0;
            eVar.l = layoutParams.D0;
            eVar.n = layoutParams.t0;
            eVar.m = layoutParams.s0;
        }

        public final void i(ConstraintHelper constraintHelper, int i, Constraints.LayoutParams layoutParams) {
            h(i, layoutParams);
            if (constraintHelper instanceof Barrier) {
                b bVar = this.e;
                bVar.h0 = 1;
                Barrier barrier = (Barrier) constraintHelper;
                bVar.f0 = barrier.getType();
                this.e.i0 = barrier.getReferencedIds();
                this.e.g0 = barrier.getMargin();
            }
        }
    }

    /* compiled from: ConstraintSet.java */
    /* loaded from: classes.dex */
    public static class b {
        public static SparseIntArray p0;
        public int c;
        public int d;
        public int[] i0;
        public String j0;
        public String k0;
        public boolean a = false;
        public boolean b = false;
        public int e = -1;
        public int f = -1;
        public float g = -1.0f;
        public int h = -1;
        public int i = -1;
        public int j = -1;
        public int k = -1;
        public int l = -1;
        public int m = -1;
        public int n = -1;
        public int o = -1;
        public int p = -1;
        public int q = -1;
        public int r = -1;
        public int s = -1;
        public int t = -1;
        public int u = -1;
        public int v = -1;
        public float w = 0.5f;
        public float x = 0.5f;
        public String y = null;
        public int z = -1;
        public int A = 0;
        public float B = Utils.FLOAT_EPSILON;
        public int C = -1;
        public int D = -1;
        public int E = -1;
        public int F = 0;
        public int G = 0;
        public int H = 0;
        public int I = 0;
        public int J = 0;
        public int K = 0;
        public int L = 0;
        public int M = Integer.MIN_VALUE;
        public int N = Integer.MIN_VALUE;
        public int O = Integer.MIN_VALUE;
        public int P = Integer.MIN_VALUE;
        public int Q = Integer.MIN_VALUE;
        public int R = Integer.MIN_VALUE;
        public int S = Integer.MIN_VALUE;
        public float T = -1.0f;
        public float U = -1.0f;
        public int V = 0;
        public int W = 0;
        public int X = 0;
        public int Y = 0;
        public int Z = -1;
        public int a0 = -1;
        public int b0 = -1;
        public int c0 = -1;
        public float d0 = 1.0f;
        public float e0 = 1.0f;
        public int f0 = -1;
        public int g0 = 0;
        public int h0 = -1;
        public boolean l0 = false;
        public boolean m0 = false;
        public boolean n0 = true;
        public int o0 = 0;

        static {
            SparseIntArray sparseIntArray = new SparseIntArray();
            p0 = sparseIntArray;
            sparseIntArray.append(w23.Layout_layout_constraintLeft_toLeftOf, 24);
            p0.append(w23.Layout_layout_constraintLeft_toRightOf, 25);
            p0.append(w23.Layout_layout_constraintRight_toLeftOf, 28);
            p0.append(w23.Layout_layout_constraintRight_toRightOf, 29);
            p0.append(w23.Layout_layout_constraintTop_toTopOf, 35);
            p0.append(w23.Layout_layout_constraintTop_toBottomOf, 34);
            p0.append(w23.Layout_layout_constraintBottom_toTopOf, 4);
            p0.append(w23.Layout_layout_constraintBottom_toBottomOf, 3);
            p0.append(w23.Layout_layout_constraintBaseline_toBaselineOf, 1);
            p0.append(w23.Layout_layout_editor_absoluteX, 6);
            p0.append(w23.Layout_layout_editor_absoluteY, 7);
            p0.append(w23.Layout_layout_constraintGuide_begin, 17);
            p0.append(w23.Layout_layout_constraintGuide_end, 18);
            p0.append(w23.Layout_layout_constraintGuide_percent, 19);
            p0.append(w23.Layout_android_orientation, 26);
            p0.append(w23.Layout_layout_constraintStart_toEndOf, 31);
            p0.append(w23.Layout_layout_constraintStart_toStartOf, 32);
            p0.append(w23.Layout_layout_constraintEnd_toStartOf, 10);
            p0.append(w23.Layout_layout_constraintEnd_toEndOf, 9);
            p0.append(w23.Layout_layout_goneMarginLeft, 13);
            p0.append(w23.Layout_layout_goneMarginTop, 16);
            p0.append(w23.Layout_layout_goneMarginRight, 14);
            p0.append(w23.Layout_layout_goneMarginBottom, 11);
            p0.append(w23.Layout_layout_goneMarginStart, 15);
            p0.append(w23.Layout_layout_goneMarginEnd, 12);
            p0.append(w23.Layout_layout_constraintVertical_weight, 38);
            p0.append(w23.Layout_layout_constraintHorizontal_weight, 37);
            p0.append(w23.Layout_layout_constraintHorizontal_chainStyle, 39);
            p0.append(w23.Layout_layout_constraintVertical_chainStyle, 40);
            p0.append(w23.Layout_layout_constraintHorizontal_bias, 20);
            p0.append(w23.Layout_layout_constraintVertical_bias, 36);
            p0.append(w23.Layout_layout_constraintDimensionRatio, 5);
            p0.append(w23.Layout_layout_constraintLeft_creator, 76);
            p0.append(w23.Layout_layout_constraintTop_creator, 76);
            p0.append(w23.Layout_layout_constraintRight_creator, 76);
            p0.append(w23.Layout_layout_constraintBottom_creator, 76);
            p0.append(w23.Layout_layout_constraintBaseline_creator, 76);
            p0.append(w23.Layout_android_layout_marginLeft, 23);
            p0.append(w23.Layout_android_layout_marginRight, 27);
            p0.append(w23.Layout_android_layout_marginStart, 30);
            p0.append(w23.Layout_android_layout_marginEnd, 8);
            p0.append(w23.Layout_android_layout_marginTop, 33);
            p0.append(w23.Layout_android_layout_marginBottom, 2);
            p0.append(w23.Layout_android_layout_width, 22);
            p0.append(w23.Layout_android_layout_height, 21);
            p0.append(w23.Layout_layout_constraintWidth, 41);
            p0.append(w23.Layout_layout_constraintHeight, 42);
            p0.append(w23.Layout_layout_constrainedWidth, 41);
            p0.append(w23.Layout_layout_constrainedHeight, 42);
            p0.append(w23.Layout_layout_wrapBehaviorInParent, 97);
            p0.append(w23.Layout_layout_constraintCircle, 61);
            p0.append(w23.Layout_layout_constraintCircleRadius, 62);
            p0.append(w23.Layout_layout_constraintCircleAngle, 63);
            p0.append(w23.Layout_layout_constraintWidth_percent, 69);
            p0.append(w23.Layout_layout_constraintHeight_percent, 70);
            p0.append(w23.Layout_chainUseRtl, 71);
            p0.append(w23.Layout_barrierDirection, 72);
            p0.append(w23.Layout_barrierMargin, 73);
            p0.append(w23.Layout_constraint_referenced_ids, 74);
            p0.append(w23.Layout_barrierAllowsGoneWidgets, 75);
        }

        public void a(b bVar) {
            this.a = bVar.a;
            this.c = bVar.c;
            this.b = bVar.b;
            this.d = bVar.d;
            this.e = bVar.e;
            this.f = bVar.f;
            this.g = bVar.g;
            this.h = bVar.h;
            this.i = bVar.i;
            this.j = bVar.j;
            this.k = bVar.k;
            this.l = bVar.l;
            this.m = bVar.m;
            this.n = bVar.n;
            this.o = bVar.o;
            this.p = bVar.p;
            this.q = bVar.q;
            this.r = bVar.r;
            this.s = bVar.s;
            this.t = bVar.t;
            this.u = bVar.u;
            this.v = bVar.v;
            this.w = bVar.w;
            this.x = bVar.x;
            this.y = bVar.y;
            this.z = bVar.z;
            this.A = bVar.A;
            this.B = bVar.B;
            this.C = bVar.C;
            this.D = bVar.D;
            this.E = bVar.E;
            this.F = bVar.F;
            this.G = bVar.G;
            this.H = bVar.H;
            this.I = bVar.I;
            this.J = bVar.J;
            this.K = bVar.K;
            this.L = bVar.L;
            this.M = bVar.M;
            this.N = bVar.N;
            this.O = bVar.O;
            this.P = bVar.P;
            this.Q = bVar.Q;
            this.R = bVar.R;
            this.S = bVar.S;
            this.T = bVar.T;
            this.U = bVar.U;
            this.V = bVar.V;
            this.W = bVar.W;
            this.X = bVar.X;
            this.Y = bVar.Y;
            this.Z = bVar.Z;
            this.a0 = bVar.a0;
            this.b0 = bVar.b0;
            this.c0 = bVar.c0;
            this.d0 = bVar.d0;
            this.e0 = bVar.e0;
            this.f0 = bVar.f0;
            this.g0 = bVar.g0;
            this.h0 = bVar.h0;
            this.k0 = bVar.k0;
            int[] iArr = bVar.i0;
            if (iArr != null && bVar.j0 == null) {
                this.i0 = Arrays.copyOf(iArr, iArr.length);
            } else {
                this.i0 = null;
            }
            this.j0 = bVar.j0;
            this.l0 = bVar.l0;
            this.m0 = bVar.m0;
            this.n0 = bVar.n0;
            this.o0 = bVar.o0;
        }

        public void b(Context context, AttributeSet attributeSet) {
            TypedArray obtainStyledAttributes = context.obtainStyledAttributes(attributeSet, w23.Layout);
            this.b = true;
            int indexCount = obtainStyledAttributes.getIndexCount();
            for (int i = 0; i < indexCount; i++) {
                int index = obtainStyledAttributes.getIndex(i);
                int i2 = p0.get(index);
                if (i2 == 80) {
                    this.l0 = obtainStyledAttributes.getBoolean(index, this.l0);
                } else if (i2 == 81) {
                    this.m0 = obtainStyledAttributes.getBoolean(index, this.m0);
                } else if (i2 != 97) {
                    switch (i2) {
                        case 1:
                            this.p = a.F(obtainStyledAttributes, index, this.p);
                            continue;
                        case 2:
                            this.I = obtainStyledAttributes.getDimensionPixelSize(index, this.I);
                            continue;
                        case 3:
                            this.o = a.F(obtainStyledAttributes, index, this.o);
                            continue;
                        case 4:
                            this.n = a.F(obtainStyledAttributes, index, this.n);
                            continue;
                        case 5:
                            this.y = obtainStyledAttributes.getString(index);
                            continue;
                        case 6:
                            this.C = obtainStyledAttributes.getDimensionPixelOffset(index, this.C);
                            continue;
                        case 7:
                            this.D = obtainStyledAttributes.getDimensionPixelOffset(index, this.D);
                            continue;
                        case 8:
                            if (Build.VERSION.SDK_INT >= 17) {
                                this.J = obtainStyledAttributes.getDimensionPixelSize(index, this.J);
                                break;
                            } else {
                                continue;
                            }
                        case 9:
                            this.v = a.F(obtainStyledAttributes, index, this.v);
                            continue;
                        case 10:
                            this.u = a.F(obtainStyledAttributes, index, this.u);
                            continue;
                        case 11:
                            this.P = obtainStyledAttributes.getDimensionPixelSize(index, this.P);
                            continue;
                        case 12:
                            this.Q = obtainStyledAttributes.getDimensionPixelSize(index, this.Q);
                            continue;
                        case 13:
                            this.M = obtainStyledAttributes.getDimensionPixelSize(index, this.M);
                            continue;
                        case 14:
                            this.O = obtainStyledAttributes.getDimensionPixelSize(index, this.O);
                            continue;
                        case 15:
                            this.R = obtainStyledAttributes.getDimensionPixelSize(index, this.R);
                            continue;
                        case 16:
                            this.N = obtainStyledAttributes.getDimensionPixelSize(index, this.N);
                            continue;
                        case 17:
                            this.e = obtainStyledAttributes.getDimensionPixelOffset(index, this.e);
                            continue;
                        case 18:
                            this.f = obtainStyledAttributes.getDimensionPixelOffset(index, this.f);
                            continue;
                        case 19:
                            this.g = obtainStyledAttributes.getFloat(index, this.g);
                            continue;
                        case 20:
                            this.w = obtainStyledAttributes.getFloat(index, this.w);
                            continue;
                        case 21:
                            this.d = obtainStyledAttributes.getLayoutDimension(index, this.d);
                            continue;
                        case 22:
                            this.c = obtainStyledAttributes.getLayoutDimension(index, this.c);
                            continue;
                        case 23:
                            this.F = obtainStyledAttributes.getDimensionPixelSize(index, this.F);
                            continue;
                        case 24:
                            this.h = a.F(obtainStyledAttributes, index, this.h);
                            continue;
                        case 25:
                            this.i = a.F(obtainStyledAttributes, index, this.i);
                            continue;
                        case 26:
                            this.E = obtainStyledAttributes.getInt(index, this.E);
                            continue;
                        case 27:
                            this.G = obtainStyledAttributes.getDimensionPixelSize(index, this.G);
                            continue;
                        case 28:
                            this.j = a.F(obtainStyledAttributes, index, this.j);
                            continue;
                        case 29:
                            this.k = a.F(obtainStyledAttributes, index, this.k);
                            continue;
                        case 30:
                            if (Build.VERSION.SDK_INT >= 17) {
                                this.K = obtainStyledAttributes.getDimensionPixelSize(index, this.K);
                                break;
                            } else {
                                continue;
                            }
                        case 31:
                            this.s = a.F(obtainStyledAttributes, index, this.s);
                            continue;
                        case 32:
                            this.t = a.F(obtainStyledAttributes, index, this.t);
                            continue;
                        case 33:
                            this.H = obtainStyledAttributes.getDimensionPixelSize(index, this.H);
                            continue;
                        case 34:
                            this.m = a.F(obtainStyledAttributes, index, this.m);
                            continue;
                        case 35:
                            this.l = a.F(obtainStyledAttributes, index, this.l);
                            continue;
                        case 36:
                            this.x = obtainStyledAttributes.getFloat(index, this.x);
                            continue;
                        case 37:
                            this.U = obtainStyledAttributes.getFloat(index, this.U);
                            continue;
                        case 38:
                            this.T = obtainStyledAttributes.getFloat(index, this.T);
                            continue;
                        case 39:
                            this.V = obtainStyledAttributes.getInt(index, this.V);
                            continue;
                        case 40:
                            this.W = obtainStyledAttributes.getInt(index, this.W);
                            continue;
                        case 41:
                            a.G(this, obtainStyledAttributes, index, 0);
                            continue;
                        case 42:
                            a.G(this, obtainStyledAttributes, index, 1);
                            continue;
                        default:
                            switch (i2) {
                                case 54:
                                    this.X = obtainStyledAttributes.getInt(index, this.X);
                                    continue;
                                case 55:
                                    this.Y = obtainStyledAttributes.getInt(index, this.Y);
                                    continue;
                                case 56:
                                    this.Z = obtainStyledAttributes.getDimensionPixelSize(index, this.Z);
                                    continue;
                                case 57:
                                    this.a0 = obtainStyledAttributes.getDimensionPixelSize(index, this.a0);
                                    continue;
                                case 58:
                                    this.b0 = obtainStyledAttributes.getDimensionPixelSize(index, this.b0);
                                    continue;
                                case 59:
                                    this.c0 = obtainStyledAttributes.getDimensionPixelSize(index, this.c0);
                                    continue;
                                default:
                                    switch (i2) {
                                        case 61:
                                            this.z = a.F(obtainStyledAttributes, index, this.z);
                                            continue;
                                        case 62:
                                            this.A = obtainStyledAttributes.getDimensionPixelSize(index, this.A);
                                            continue;
                                        case 63:
                                            this.B = obtainStyledAttributes.getFloat(index, this.B);
                                            continue;
                                        default:
                                            switch (i2) {
                                                case 69:
                                                    this.d0 = obtainStyledAttributes.getFloat(index, 1.0f);
                                                    continue;
                                                case 70:
                                                    this.e0 = obtainStyledAttributes.getFloat(index, 1.0f);
                                                    continue;
                                                case 71:
                                                    continue;
                                                    continue;
                                                    continue;
                                                case 72:
                                                    this.f0 = obtainStyledAttributes.getInt(index, this.f0);
                                                    continue;
                                                case 73:
                                                    this.g0 = obtainStyledAttributes.getDimensionPixelSize(index, this.g0);
                                                    continue;
                                                case 74:
                                                    this.j0 = obtainStyledAttributes.getString(index);
                                                    continue;
                                                case 75:
                                                    this.n0 = obtainStyledAttributes.getBoolean(index, this.n0);
                                                    continue;
                                                case 76:
                                                    StringBuilder sb = new StringBuilder();
                                                    sb.append("unused attribute 0x");
                                                    sb.append(Integer.toHexString(index));
                                                    sb.append("   ");
                                                    sb.append(p0.get(index));
                                                    continue;
                                                case 77:
                                                    this.k0 = obtainStyledAttributes.getString(index);
                                                    continue;
                                                default:
                                                    switch (i2) {
                                                        case 91:
                                                            this.q = a.F(obtainStyledAttributes, index, this.q);
                                                            continue;
                                                        case 92:
                                                            this.r = a.F(obtainStyledAttributes, index, this.r);
                                                            continue;
                                                        case 93:
                                                            this.L = obtainStyledAttributes.getDimensionPixelSize(index, this.L);
                                                            continue;
                                                        case 94:
                                                            this.S = obtainStyledAttributes.getDimensionPixelSize(index, this.S);
                                                            continue;
                                                        default:
                                                            StringBuilder sb2 = new StringBuilder();
                                                            sb2.append("Unknown attribute 0x");
                                                            sb2.append(Integer.toHexString(index));
                                                            sb2.append("   ");
                                                            sb2.append(p0.get(index));
                                                            continue;
                                                            continue;
                                                    }
                                            }
                                    }
                            }
                    }
                } else {
                    this.o0 = obtainStyledAttributes.getInt(index, this.o0);
                }
            }
            obtainStyledAttributes.recycle();
        }
    }

    /* compiled from: ConstraintSet.java */
    /* loaded from: classes.dex */
    public static class c {
        public static SparseIntArray o;
        public boolean a = false;
        public int b = -1;
        public int c = 0;
        public String d = null;
        public int e = -1;
        public int f = 0;
        public float g = Float.NaN;
        public int h = -1;
        public float i = Float.NaN;
        public float j = Float.NaN;
        public int k = -1;
        public String l = null;
        public int m = -3;
        public int n = -1;

        static {
            SparseIntArray sparseIntArray = new SparseIntArray();
            o = sparseIntArray;
            sparseIntArray.append(w23.Motion_motionPathRotate, 1);
            o.append(w23.Motion_pathMotionArc, 2);
            o.append(w23.Motion_transitionEasing, 3);
            o.append(w23.Motion_drawPath, 4);
            o.append(w23.Motion_animateRelativeTo, 5);
            o.append(w23.Motion_animateCircleAngleTo, 6);
            o.append(w23.Motion_motionStagger, 7);
            o.append(w23.Motion_quantizeMotionSteps, 8);
            o.append(w23.Motion_quantizeMotionPhase, 9);
            o.append(w23.Motion_quantizeMotionInterpolator, 10);
        }

        public void a(c cVar) {
            this.a = cVar.a;
            this.b = cVar.b;
            this.d = cVar.d;
            this.e = cVar.e;
            this.f = cVar.f;
            this.i = cVar.i;
            this.g = cVar.g;
            this.h = cVar.h;
        }

        public void b(Context context, AttributeSet attributeSet) {
            TypedArray obtainStyledAttributes = context.obtainStyledAttributes(attributeSet, w23.Motion);
            this.a = true;
            int indexCount = obtainStyledAttributes.getIndexCount();
            for (int i = 0; i < indexCount; i++) {
                int index = obtainStyledAttributes.getIndex(i);
                switch (o.get(index)) {
                    case 1:
                        this.i = obtainStyledAttributes.getFloat(index, this.i);
                        break;
                    case 2:
                        this.e = obtainStyledAttributes.getInt(index, this.e);
                        break;
                    case 3:
                        if (obtainStyledAttributes.peekValue(index).type == 3) {
                            this.d = obtainStyledAttributes.getString(index);
                            break;
                        } else {
                            this.d = yt0.c[obtainStyledAttributes.getInteger(index, 0)];
                            break;
                        }
                    case 4:
                        this.f = obtainStyledAttributes.getInt(index, 0);
                        break;
                    case 5:
                        this.b = a.F(obtainStyledAttributes, index, this.b);
                        break;
                    case 6:
                        this.c = obtainStyledAttributes.getInteger(index, this.c);
                        break;
                    case 7:
                        this.g = obtainStyledAttributes.getFloat(index, this.g);
                        break;
                    case 8:
                        this.k = obtainStyledAttributes.getInteger(index, this.k);
                        break;
                    case 9:
                        this.j = obtainStyledAttributes.getFloat(index, this.j);
                        break;
                    case 10:
                        int i2 = obtainStyledAttributes.peekValue(index).type;
                        if (i2 == 1) {
                            int resourceId = obtainStyledAttributes.getResourceId(index, -1);
                            this.n = resourceId;
                            if (resourceId != -1) {
                                this.m = -2;
                                break;
                            } else {
                                break;
                            }
                        } else if (i2 == 3) {
                            String string = obtainStyledAttributes.getString(index);
                            this.l = string;
                            if (string.indexOf("/") > 0) {
                                this.n = obtainStyledAttributes.getResourceId(index, -1);
                                this.m = -2;
                                break;
                            } else {
                                this.m = -1;
                                break;
                            }
                        } else {
                            this.m = obtainStyledAttributes.getInteger(index, this.n);
                            break;
                        }
                }
            }
            obtainStyledAttributes.recycle();
        }
    }

    /* compiled from: ConstraintSet.java */
    /* loaded from: classes.dex */
    public static class d {
        public boolean a = false;
        public int b = 0;
        public int c = 0;
        public float d = 1.0f;
        public float e = Float.NaN;

        public void a(d dVar) {
            this.a = dVar.a;
            this.b = dVar.b;
            this.d = dVar.d;
            this.e = dVar.e;
            this.c = dVar.c;
        }

        public void b(Context context, AttributeSet attributeSet) {
            TypedArray obtainStyledAttributes = context.obtainStyledAttributes(attributeSet, w23.PropertySet);
            this.a = true;
            int indexCount = obtainStyledAttributes.getIndexCount();
            for (int i = 0; i < indexCount; i++) {
                int index = obtainStyledAttributes.getIndex(i);
                if (index == w23.PropertySet_android_alpha) {
                    this.d = obtainStyledAttributes.getFloat(index, this.d);
                } else if (index == w23.PropertySet_android_visibility) {
                    this.b = obtainStyledAttributes.getInt(index, this.b);
                    this.b = a.g[this.b];
                } else if (index == w23.PropertySet_visibilityMode) {
                    this.c = obtainStyledAttributes.getInt(index, this.c);
                } else if (index == w23.PropertySet_motionProgress) {
                    this.e = obtainStyledAttributes.getFloat(index, this.e);
                }
            }
            obtainStyledAttributes.recycle();
        }
    }

    /* compiled from: ConstraintSet.java */
    /* loaded from: classes.dex */
    public static class e {
        public static SparseIntArray o;
        public boolean a = false;
        public float b = Utils.FLOAT_EPSILON;
        public float c = Utils.FLOAT_EPSILON;
        public float d = Utils.FLOAT_EPSILON;
        public float e = 1.0f;
        public float f = 1.0f;
        public float g = Float.NaN;
        public float h = Float.NaN;
        public int i = -1;
        public float j = Utils.FLOAT_EPSILON;
        public float k = Utils.FLOAT_EPSILON;
        public float l = Utils.FLOAT_EPSILON;
        public boolean m = false;
        public float n = Utils.FLOAT_EPSILON;

        static {
            SparseIntArray sparseIntArray = new SparseIntArray();
            o = sparseIntArray;
            sparseIntArray.append(w23.Transform_android_rotation, 1);
            o.append(w23.Transform_android_rotationX, 2);
            o.append(w23.Transform_android_rotationY, 3);
            o.append(w23.Transform_android_scaleX, 4);
            o.append(w23.Transform_android_scaleY, 5);
            o.append(w23.Transform_android_transformPivotX, 6);
            o.append(w23.Transform_android_transformPivotY, 7);
            o.append(w23.Transform_android_translationX, 8);
            o.append(w23.Transform_android_translationY, 9);
            o.append(w23.Transform_android_translationZ, 10);
            o.append(w23.Transform_android_elevation, 11);
            o.append(w23.Transform_transformPivotTarget, 12);
        }

        public void a(e eVar) {
            this.a = eVar.a;
            this.b = eVar.b;
            this.c = eVar.c;
            this.d = eVar.d;
            this.e = eVar.e;
            this.f = eVar.f;
            this.g = eVar.g;
            this.h = eVar.h;
            this.i = eVar.i;
            this.j = eVar.j;
            this.k = eVar.k;
            this.l = eVar.l;
            this.m = eVar.m;
            this.n = eVar.n;
        }

        public void b(Context context, AttributeSet attributeSet) {
            TypedArray obtainStyledAttributes = context.obtainStyledAttributes(attributeSet, w23.Transform);
            this.a = true;
            int indexCount = obtainStyledAttributes.getIndexCount();
            for (int i = 0; i < indexCount; i++) {
                int index = obtainStyledAttributes.getIndex(i);
                switch (o.get(index)) {
                    case 1:
                        this.b = obtainStyledAttributes.getFloat(index, this.b);
                        break;
                    case 2:
                        this.c = obtainStyledAttributes.getFloat(index, this.c);
                        break;
                    case 3:
                        this.d = obtainStyledAttributes.getFloat(index, this.d);
                        break;
                    case 4:
                        this.e = obtainStyledAttributes.getFloat(index, this.e);
                        break;
                    case 5:
                        this.f = obtainStyledAttributes.getFloat(index, this.f);
                        break;
                    case 6:
                        this.g = obtainStyledAttributes.getDimension(index, this.g);
                        break;
                    case 7:
                        this.h = obtainStyledAttributes.getDimension(index, this.h);
                        break;
                    case 8:
                        this.j = obtainStyledAttributes.getDimension(index, this.j);
                        break;
                    case 9:
                        this.k = obtainStyledAttributes.getDimension(index, this.k);
                        break;
                    case 10:
                        if (Build.VERSION.SDK_INT >= 21) {
                            this.l = obtainStyledAttributes.getDimension(index, this.l);
                            break;
                        } else {
                            break;
                        }
                    case 11:
                        if (Build.VERSION.SDK_INT >= 21) {
                            this.m = true;
                            this.n = obtainStyledAttributes.getDimension(index, this.n);
                            break;
                        } else {
                            break;
                        }
                    case 12:
                        this.i = a.F(obtainStyledAttributes, index, this.i);
                        break;
                }
            }
            obtainStyledAttributes.recycle();
        }
    }

    static {
        h.append(w23.Constraint_layout_constraintLeft_toLeftOf, 25);
        h.append(w23.Constraint_layout_constraintLeft_toRightOf, 26);
        h.append(w23.Constraint_layout_constraintRight_toLeftOf, 29);
        h.append(w23.Constraint_layout_constraintRight_toRightOf, 30);
        h.append(w23.Constraint_layout_constraintTop_toTopOf, 36);
        h.append(w23.Constraint_layout_constraintTop_toBottomOf, 35);
        h.append(w23.Constraint_layout_constraintBottom_toTopOf, 4);
        h.append(w23.Constraint_layout_constraintBottom_toBottomOf, 3);
        h.append(w23.Constraint_layout_constraintBaseline_toBaselineOf, 1);
        h.append(w23.Constraint_layout_constraintBaseline_toTopOf, 91);
        h.append(w23.Constraint_layout_constraintBaseline_toBottomOf, 92);
        h.append(w23.Constraint_layout_editor_absoluteX, 6);
        h.append(w23.Constraint_layout_editor_absoluteY, 7);
        h.append(w23.Constraint_layout_constraintGuide_begin, 17);
        h.append(w23.Constraint_layout_constraintGuide_end, 18);
        h.append(w23.Constraint_layout_constraintGuide_percent, 19);
        h.append(w23.Constraint_android_orientation, 27);
        h.append(w23.Constraint_layout_constraintStart_toEndOf, 32);
        h.append(w23.Constraint_layout_constraintStart_toStartOf, 33);
        h.append(w23.Constraint_layout_constraintEnd_toStartOf, 10);
        h.append(w23.Constraint_layout_constraintEnd_toEndOf, 9);
        h.append(w23.Constraint_layout_goneMarginLeft, 13);
        h.append(w23.Constraint_layout_goneMarginTop, 16);
        h.append(w23.Constraint_layout_goneMarginRight, 14);
        h.append(w23.Constraint_layout_goneMarginBottom, 11);
        h.append(w23.Constraint_layout_goneMarginStart, 15);
        h.append(w23.Constraint_layout_goneMarginEnd, 12);
        h.append(w23.Constraint_layout_constraintVertical_weight, 40);
        h.append(w23.Constraint_layout_constraintHorizontal_weight, 39);
        h.append(w23.Constraint_layout_constraintHorizontal_chainStyle, 41);
        h.append(w23.Constraint_layout_constraintVertical_chainStyle, 42);
        h.append(w23.Constraint_layout_constraintHorizontal_bias, 20);
        h.append(w23.Constraint_layout_constraintVertical_bias, 37);
        h.append(w23.Constraint_layout_constraintDimensionRatio, 5);
        h.append(w23.Constraint_layout_constraintLeft_creator, 87);
        h.append(w23.Constraint_layout_constraintTop_creator, 87);
        h.append(w23.Constraint_layout_constraintRight_creator, 87);
        h.append(w23.Constraint_layout_constraintBottom_creator, 87);
        h.append(w23.Constraint_layout_constraintBaseline_creator, 87);
        h.append(w23.Constraint_android_layout_marginLeft, 24);
        h.append(w23.Constraint_android_layout_marginRight, 28);
        h.append(w23.Constraint_android_layout_marginStart, 31);
        h.append(w23.Constraint_android_layout_marginEnd, 8);
        h.append(w23.Constraint_android_layout_marginTop, 34);
        h.append(w23.Constraint_android_layout_marginBottom, 2);
        h.append(w23.Constraint_android_layout_width, 23);
        h.append(w23.Constraint_android_layout_height, 21);
        h.append(w23.Constraint_layout_constraintWidth, 95);
        h.append(w23.Constraint_layout_constraintHeight, 96);
        h.append(w23.Constraint_android_visibility, 22);
        h.append(w23.Constraint_android_alpha, 43);
        h.append(w23.Constraint_android_elevation, 44);
        h.append(w23.Constraint_android_rotationX, 45);
        h.append(w23.Constraint_android_rotationY, 46);
        h.append(w23.Constraint_android_rotation, 60);
        h.append(w23.Constraint_android_scaleX, 47);
        h.append(w23.Constraint_android_scaleY, 48);
        h.append(w23.Constraint_android_transformPivotX, 49);
        h.append(w23.Constraint_android_transformPivotY, 50);
        h.append(w23.Constraint_android_translationX, 51);
        h.append(w23.Constraint_android_translationY, 52);
        h.append(w23.Constraint_android_translationZ, 53);
        h.append(w23.Constraint_layout_constraintWidth_default, 54);
        h.append(w23.Constraint_layout_constraintHeight_default, 55);
        h.append(w23.Constraint_layout_constraintWidth_max, 56);
        h.append(w23.Constraint_layout_constraintHeight_max, 57);
        h.append(w23.Constraint_layout_constraintWidth_min, 58);
        h.append(w23.Constraint_layout_constraintHeight_min, 59);
        h.append(w23.Constraint_layout_constraintCircle, 61);
        h.append(w23.Constraint_layout_constraintCircleRadius, 62);
        h.append(w23.Constraint_layout_constraintCircleAngle, 63);
        h.append(w23.Constraint_animateRelativeTo, 64);
        h.append(w23.Constraint_transitionEasing, 65);
        h.append(w23.Constraint_drawPath, 66);
        h.append(w23.Constraint_transitionPathRotate, 67);
        h.append(w23.Constraint_motionStagger, 79);
        h.append(w23.Constraint_android_id, 38);
        h.append(w23.Constraint_motionProgress, 68);
        h.append(w23.Constraint_layout_constraintWidth_percent, 69);
        h.append(w23.Constraint_layout_constraintHeight_percent, 70);
        h.append(w23.Constraint_layout_wrapBehaviorInParent, 97);
        h.append(w23.Constraint_chainUseRtl, 71);
        h.append(w23.Constraint_barrierDirection, 72);
        h.append(w23.Constraint_barrierMargin, 73);
        h.append(w23.Constraint_constraint_referenced_ids, 74);
        h.append(w23.Constraint_barrierAllowsGoneWidgets, 75);
        h.append(w23.Constraint_pathMotionArc, 76);
        h.append(w23.Constraint_layout_constraintTag, 77);
        h.append(w23.Constraint_visibilityMode, 78);
        h.append(w23.Constraint_layout_constrainedWidth, 80);
        h.append(w23.Constraint_layout_constrainedHeight, 81);
        h.append(w23.Constraint_polarRelativeTo, 82);
        h.append(w23.Constraint_transformPivotTarget, 83);
        h.append(w23.Constraint_quantizeMotionSteps, 84);
        h.append(w23.Constraint_quantizeMotionPhase, 85);
        h.append(w23.Constraint_quantizeMotionInterpolator, 86);
        SparseIntArray sparseIntArray = i;
        int i2 = w23.ConstraintOverride_layout_editor_absoluteY;
        sparseIntArray.append(i2, 6);
        i.append(i2, 7);
        i.append(w23.ConstraintOverride_android_orientation, 27);
        i.append(w23.ConstraintOverride_layout_goneMarginLeft, 13);
        i.append(w23.ConstraintOverride_layout_goneMarginTop, 16);
        i.append(w23.ConstraintOverride_layout_goneMarginRight, 14);
        i.append(w23.ConstraintOverride_layout_goneMarginBottom, 11);
        i.append(w23.ConstraintOverride_layout_goneMarginStart, 15);
        i.append(w23.ConstraintOverride_layout_goneMarginEnd, 12);
        i.append(w23.ConstraintOverride_layout_constraintVertical_weight, 40);
        i.append(w23.ConstraintOverride_layout_constraintHorizontal_weight, 39);
        i.append(w23.ConstraintOverride_layout_constraintHorizontal_chainStyle, 41);
        i.append(w23.ConstraintOverride_layout_constraintVertical_chainStyle, 42);
        i.append(w23.ConstraintOverride_layout_constraintHorizontal_bias, 20);
        i.append(w23.ConstraintOverride_layout_constraintVertical_bias, 37);
        i.append(w23.ConstraintOverride_layout_constraintDimensionRatio, 5);
        i.append(w23.ConstraintOverride_layout_constraintLeft_creator, 87);
        i.append(w23.ConstraintOverride_layout_constraintTop_creator, 87);
        i.append(w23.ConstraintOverride_layout_constraintRight_creator, 87);
        i.append(w23.ConstraintOverride_layout_constraintBottom_creator, 87);
        i.append(w23.ConstraintOverride_layout_constraintBaseline_creator, 87);
        i.append(w23.ConstraintOverride_android_layout_marginLeft, 24);
        i.append(w23.ConstraintOverride_android_layout_marginRight, 28);
        i.append(w23.ConstraintOverride_android_layout_marginStart, 31);
        i.append(w23.ConstraintOverride_android_layout_marginEnd, 8);
        i.append(w23.ConstraintOverride_android_layout_marginTop, 34);
        i.append(w23.ConstraintOverride_android_layout_marginBottom, 2);
        i.append(w23.ConstraintOverride_android_layout_width, 23);
        i.append(w23.ConstraintOverride_android_layout_height, 21);
        i.append(w23.ConstraintOverride_layout_constraintWidth, 95);
        i.append(w23.ConstraintOverride_layout_constraintHeight, 96);
        i.append(w23.ConstraintOverride_android_visibility, 22);
        i.append(w23.ConstraintOverride_android_alpha, 43);
        i.append(w23.ConstraintOverride_android_elevation, 44);
        i.append(w23.ConstraintOverride_android_rotationX, 45);
        i.append(w23.ConstraintOverride_android_rotationY, 46);
        i.append(w23.ConstraintOverride_android_rotation, 60);
        i.append(w23.ConstraintOverride_android_scaleX, 47);
        i.append(w23.ConstraintOverride_android_scaleY, 48);
        i.append(w23.ConstraintOverride_android_transformPivotX, 49);
        i.append(w23.ConstraintOverride_android_transformPivotY, 50);
        i.append(w23.ConstraintOverride_android_translationX, 51);
        i.append(w23.ConstraintOverride_android_translationY, 52);
        i.append(w23.ConstraintOverride_android_translationZ, 53);
        i.append(w23.ConstraintOverride_layout_constraintWidth_default, 54);
        i.append(w23.ConstraintOverride_layout_constraintHeight_default, 55);
        i.append(w23.ConstraintOverride_layout_constraintWidth_max, 56);
        i.append(w23.ConstraintOverride_layout_constraintHeight_max, 57);
        i.append(w23.ConstraintOverride_layout_constraintWidth_min, 58);
        i.append(w23.ConstraintOverride_layout_constraintHeight_min, 59);
        i.append(w23.ConstraintOverride_layout_constraintCircleRadius, 62);
        i.append(w23.ConstraintOverride_layout_constraintCircleAngle, 63);
        i.append(w23.ConstraintOverride_animateRelativeTo, 64);
        i.append(w23.ConstraintOverride_transitionEasing, 65);
        i.append(w23.ConstraintOverride_drawPath, 66);
        i.append(w23.ConstraintOverride_transitionPathRotate, 67);
        i.append(w23.ConstraintOverride_motionStagger, 79);
        i.append(w23.ConstraintOverride_android_id, 38);
        i.append(w23.ConstraintOverride_motionTarget, 98);
        i.append(w23.ConstraintOverride_motionProgress, 68);
        i.append(w23.ConstraintOverride_layout_constraintWidth_percent, 69);
        i.append(w23.ConstraintOverride_layout_constraintHeight_percent, 70);
        i.append(w23.ConstraintOverride_chainUseRtl, 71);
        i.append(w23.ConstraintOverride_barrierDirection, 72);
        i.append(w23.ConstraintOverride_barrierMargin, 73);
        i.append(w23.ConstraintOverride_constraint_referenced_ids, 74);
        i.append(w23.ConstraintOverride_barrierAllowsGoneWidgets, 75);
        i.append(w23.ConstraintOverride_pathMotionArc, 76);
        i.append(w23.ConstraintOverride_layout_constraintTag, 77);
        i.append(w23.ConstraintOverride_visibilityMode, 78);
        i.append(w23.ConstraintOverride_layout_constrainedWidth, 80);
        i.append(w23.ConstraintOverride_layout_constrainedHeight, 81);
        i.append(w23.ConstraintOverride_polarRelativeTo, 82);
        i.append(w23.ConstraintOverride_transformPivotTarget, 83);
        i.append(w23.ConstraintOverride_quantizeMotionSteps, 84);
        i.append(w23.ConstraintOverride_quantizeMotionPhase, 85);
        i.append(w23.ConstraintOverride_quantizeMotionInterpolator, 86);
        i.append(w23.ConstraintOverride_layout_wrapBehaviorInParent, 97);
    }

    public static int F(TypedArray typedArray, int i2, int i3) {
        int resourceId = typedArray.getResourceId(i2, i3);
        return resourceId == -1 ? typedArray.getInt(i2, -1) : resourceId;
    }

    public static void G(Object obj, TypedArray typedArray, int i2, int i3) {
        if (obj == null) {
            return;
        }
        int i4 = typedArray.peekValue(i2).type;
        if (i4 != 3) {
            int i5 = -2;
            boolean z = false;
            if (i4 != 5) {
                int i6 = typedArray.getInt(i2, 0);
                if (i6 != -4) {
                    i5 = (i6 == -3 || !(i6 == -2 || i6 == -1)) ? 0 : i6;
                } else {
                    z = true;
                }
            } else {
                i5 = typedArray.getDimensionPixelSize(i2, 0);
            }
            if (obj instanceof ConstraintLayout.LayoutParams) {
                ConstraintLayout.LayoutParams layoutParams = (ConstraintLayout.LayoutParams) obj;
                if (i3 == 0) {
                    ((ViewGroup.MarginLayoutParams) layoutParams).width = i5;
                    layoutParams.V = z;
                    return;
                }
                ((ViewGroup.MarginLayoutParams) layoutParams).height = i5;
                layoutParams.W = z;
                return;
            } else if (obj instanceof b) {
                b bVar = (b) obj;
                if (i3 == 0) {
                    bVar.c = i5;
                    bVar.l0 = z;
                    return;
                }
                bVar.d = i5;
                bVar.m0 = z;
                return;
            } else if (obj instanceof C0021a.C0022a) {
                C0021a.C0022a c0022a = (C0021a.C0022a) obj;
                if (i3 == 0) {
                    c0022a.b(23, i5);
                    c0022a.d(80, z);
                    return;
                }
                c0022a.b(21, i5);
                c0022a.d(81, z);
                return;
            } else {
                return;
            }
        }
        H(obj, typedArray.getString(i2), i3);
    }

    public static void H(Object obj, String str, int i2) {
        if (str == null) {
            return;
        }
        int indexOf = str.indexOf(61);
        int length = str.length();
        if (indexOf <= 0 || indexOf >= length - 1) {
            return;
        }
        String substring = str.substring(0, indexOf);
        String substring2 = str.substring(indexOf + 1);
        if (substring2.length() > 0) {
            String trim = substring.trim();
            String trim2 = substring2.trim();
            if ("ratio".equalsIgnoreCase(trim)) {
                if (obj instanceof ConstraintLayout.LayoutParams) {
                    ConstraintLayout.LayoutParams layoutParams = (ConstraintLayout.LayoutParams) obj;
                    if (i2 == 0) {
                        ((ViewGroup.MarginLayoutParams) layoutParams).width = 0;
                    } else {
                        ((ViewGroup.MarginLayoutParams) layoutParams).height = 0;
                    }
                    I(layoutParams, trim2);
                    return;
                } else if (obj instanceof b) {
                    ((b) obj).y = trim2;
                    return;
                } else if (obj instanceof C0021a.C0022a) {
                    ((C0021a.C0022a) obj).c(5, trim2);
                    return;
                } else {
                    return;
                }
            }
            try {
                if ("weight".equalsIgnoreCase(trim)) {
                    float parseFloat = Float.parseFloat(trim2);
                    if (obj instanceof ConstraintLayout.LayoutParams) {
                        ConstraintLayout.LayoutParams layoutParams2 = (ConstraintLayout.LayoutParams) obj;
                        if (i2 == 0) {
                            ((ViewGroup.MarginLayoutParams) layoutParams2).width = 0;
                            layoutParams2.G = parseFloat;
                        } else {
                            ((ViewGroup.MarginLayoutParams) layoutParams2).height = 0;
                            layoutParams2.H = parseFloat;
                        }
                    } else if (obj instanceof b) {
                        b bVar = (b) obj;
                        if (i2 == 0) {
                            bVar.c = 0;
                            bVar.U = parseFloat;
                        } else {
                            bVar.d = 0;
                            bVar.T = parseFloat;
                        }
                    } else if (obj instanceof C0021a.C0022a) {
                        C0021a.C0022a c0022a = (C0021a.C0022a) obj;
                        if (i2 == 0) {
                            c0022a.b(23, 0);
                            c0022a.a(39, parseFloat);
                        } else {
                            c0022a.b(21, 0);
                            c0022a.a(40, parseFloat);
                        }
                    }
                } else if (!"parent".equalsIgnoreCase(trim)) {
                } else {
                    float max = Math.max((float) Utils.FLOAT_EPSILON, Math.min(1.0f, Float.parseFloat(trim2)));
                    if (obj instanceof ConstraintLayout.LayoutParams) {
                        ConstraintLayout.LayoutParams layoutParams3 = (ConstraintLayout.LayoutParams) obj;
                        if (i2 == 0) {
                            ((ViewGroup.MarginLayoutParams) layoutParams3).width = 0;
                            layoutParams3.Q = max;
                            layoutParams3.K = 2;
                        } else {
                            ((ViewGroup.MarginLayoutParams) layoutParams3).height = 0;
                            layoutParams3.R = max;
                            layoutParams3.L = 2;
                        }
                    } else if (obj instanceof b) {
                        b bVar2 = (b) obj;
                        if (i2 == 0) {
                            bVar2.c = 0;
                            bVar2.d0 = max;
                            bVar2.X = 2;
                        } else {
                            bVar2.d = 0;
                            bVar2.e0 = max;
                            bVar2.Y = 2;
                        }
                    } else if (obj instanceof C0021a.C0022a) {
                        C0021a.C0022a c0022a2 = (C0021a.C0022a) obj;
                        if (i2 == 0) {
                            c0022a2.b(23, 0);
                            c0022a2.b(54, 2);
                        } else {
                            c0022a2.b(21, 0);
                            c0022a2.b(55, 2);
                        }
                    }
                }
            } catch (NumberFormatException unused) {
            }
        }
    }

    public static void I(ConstraintLayout.LayoutParams layoutParams, String str) {
        if (str != null) {
            int length = str.length();
            int indexOf = str.indexOf(44);
            int i2 = 0;
            int i3 = -1;
            if (indexOf > 0 && indexOf < length - 1) {
                String substring = str.substring(0, indexOf);
                if (!substring.equalsIgnoreCase("W")) {
                    i2 = substring.equalsIgnoreCase("H") ? 1 : -1;
                }
                i3 = i2;
                i2 = indexOf + 1;
            }
            int indexOf2 = str.indexOf(58);
            try {
                if (indexOf2 >= 0 && indexOf2 < length - 1) {
                    String substring2 = str.substring(i2, indexOf2);
                    String substring3 = str.substring(indexOf2 + 1);
                    if (substring2.length() > 0 && substring3.length() > 0) {
                        float parseFloat = Float.parseFloat(substring2);
                        float parseFloat2 = Float.parseFloat(substring3);
                        if (parseFloat > Utils.FLOAT_EPSILON && parseFloat2 > Utils.FLOAT_EPSILON) {
                            if (i3 == 1) {
                                Math.abs(parseFloat2 / parseFloat);
                            } else {
                                Math.abs(parseFloat / parseFloat2);
                            }
                        }
                    }
                } else {
                    String substring4 = str.substring(i2);
                    if (substring4.length() > 0) {
                        Float.parseFloat(substring4);
                    }
                }
            } catch (NumberFormatException unused) {
            }
        }
        layoutParams.F = str;
    }

    public static void K(Context context, C0021a c0021a, TypedArray typedArray) {
        int indexCount = typedArray.getIndexCount();
        C0021a.C0022a c0022a = new C0021a.C0022a();
        c0021a.h = c0022a;
        c0021a.d.a = false;
        c0021a.e.b = false;
        c0021a.c.a = false;
        c0021a.f.a = false;
        for (int i2 = 0; i2 < indexCount; i2++) {
            int index = typedArray.getIndex(i2);
            switch (i.get(index)) {
                case 2:
                    c0022a.b(2, typedArray.getDimensionPixelSize(index, c0021a.e.I));
                    break;
                case 3:
                case 4:
                case 9:
                case 10:
                case 25:
                case 26:
                case 29:
                case 30:
                case 32:
                case 33:
                case 35:
                case 36:
                case 61:
                case 88:
                case 89:
                case 90:
                case 91:
                case 92:
                default:
                    StringBuilder sb = new StringBuilder();
                    sb.append("Unknown attribute 0x");
                    sb.append(Integer.toHexString(index));
                    sb.append("   ");
                    sb.append(h.get(index));
                    break;
                case 5:
                    c0022a.c(5, typedArray.getString(index));
                    break;
                case 6:
                    c0022a.b(6, typedArray.getDimensionPixelOffset(index, c0021a.e.C));
                    break;
                case 7:
                    c0022a.b(7, typedArray.getDimensionPixelOffset(index, c0021a.e.D));
                    break;
                case 8:
                    if (Build.VERSION.SDK_INT >= 17) {
                        c0022a.b(8, typedArray.getDimensionPixelSize(index, c0021a.e.J));
                        break;
                    } else {
                        break;
                    }
                case 11:
                    c0022a.b(11, typedArray.getDimensionPixelSize(index, c0021a.e.P));
                    break;
                case 12:
                    c0022a.b(12, typedArray.getDimensionPixelSize(index, c0021a.e.Q));
                    break;
                case 13:
                    c0022a.b(13, typedArray.getDimensionPixelSize(index, c0021a.e.M));
                    break;
                case 14:
                    c0022a.b(14, typedArray.getDimensionPixelSize(index, c0021a.e.O));
                    break;
                case 15:
                    c0022a.b(15, typedArray.getDimensionPixelSize(index, c0021a.e.R));
                    break;
                case 16:
                    c0022a.b(16, typedArray.getDimensionPixelSize(index, c0021a.e.N));
                    break;
                case 17:
                    c0022a.b(17, typedArray.getDimensionPixelOffset(index, c0021a.e.e));
                    break;
                case 18:
                    c0022a.b(18, typedArray.getDimensionPixelOffset(index, c0021a.e.f));
                    break;
                case 19:
                    c0022a.a(19, typedArray.getFloat(index, c0021a.e.g));
                    break;
                case 20:
                    c0022a.a(20, typedArray.getFloat(index, c0021a.e.w));
                    break;
                case 21:
                    c0022a.b(21, typedArray.getLayoutDimension(index, c0021a.e.d));
                    break;
                case 22:
                    c0022a.b(22, g[typedArray.getInt(index, c0021a.c.b)]);
                    break;
                case 23:
                    c0022a.b(23, typedArray.getLayoutDimension(index, c0021a.e.c));
                    break;
                case 24:
                    c0022a.b(24, typedArray.getDimensionPixelSize(index, c0021a.e.F));
                    break;
                case 27:
                    c0022a.b(27, typedArray.getInt(index, c0021a.e.E));
                    break;
                case 28:
                    c0022a.b(28, typedArray.getDimensionPixelSize(index, c0021a.e.G));
                    break;
                case 31:
                    if (Build.VERSION.SDK_INT >= 17) {
                        c0022a.b(31, typedArray.getDimensionPixelSize(index, c0021a.e.K));
                        break;
                    } else {
                        break;
                    }
                case 34:
                    c0022a.b(34, typedArray.getDimensionPixelSize(index, c0021a.e.H));
                    break;
                case 37:
                    c0022a.a(37, typedArray.getFloat(index, c0021a.e.x));
                    break;
                case 38:
                    int resourceId = typedArray.getResourceId(index, c0021a.a);
                    c0021a.a = resourceId;
                    c0022a.b(38, resourceId);
                    break;
                case 39:
                    c0022a.a(39, typedArray.getFloat(index, c0021a.e.U));
                    break;
                case 40:
                    c0022a.a(40, typedArray.getFloat(index, c0021a.e.T));
                    break;
                case 41:
                    c0022a.b(41, typedArray.getInt(index, c0021a.e.V));
                    break;
                case 42:
                    c0022a.b(42, typedArray.getInt(index, c0021a.e.W));
                    break;
                case 43:
                    c0022a.a(43, typedArray.getFloat(index, c0021a.c.d));
                    break;
                case 44:
                    if (Build.VERSION.SDK_INT >= 21) {
                        c0022a.d(44, true);
                        c0022a.a(44, typedArray.getDimension(index, c0021a.f.n));
                        break;
                    } else {
                        break;
                    }
                case 45:
                    c0022a.a(45, typedArray.getFloat(index, c0021a.f.c));
                    break;
                case 46:
                    c0022a.a(46, typedArray.getFloat(index, c0021a.f.d));
                    break;
                case 47:
                    c0022a.a(47, typedArray.getFloat(index, c0021a.f.e));
                    break;
                case 48:
                    c0022a.a(48, typedArray.getFloat(index, c0021a.f.f));
                    break;
                case 49:
                    c0022a.a(49, typedArray.getDimension(index, c0021a.f.g));
                    break;
                case 50:
                    c0022a.a(50, typedArray.getDimension(index, c0021a.f.h));
                    break;
                case 51:
                    c0022a.a(51, typedArray.getDimension(index, c0021a.f.j));
                    break;
                case 52:
                    c0022a.a(52, typedArray.getDimension(index, c0021a.f.k));
                    break;
                case 53:
                    if (Build.VERSION.SDK_INT >= 21) {
                        c0022a.a(53, typedArray.getDimension(index, c0021a.f.l));
                        break;
                    } else {
                        break;
                    }
                case 54:
                    c0022a.b(54, typedArray.getInt(index, c0021a.e.X));
                    break;
                case 55:
                    c0022a.b(55, typedArray.getInt(index, c0021a.e.Y));
                    break;
                case 56:
                    c0022a.b(56, typedArray.getDimensionPixelSize(index, c0021a.e.Z));
                    break;
                case 57:
                    c0022a.b(57, typedArray.getDimensionPixelSize(index, c0021a.e.a0));
                    break;
                case 58:
                    c0022a.b(58, typedArray.getDimensionPixelSize(index, c0021a.e.b0));
                    break;
                case 59:
                    c0022a.b(59, typedArray.getDimensionPixelSize(index, c0021a.e.c0));
                    break;
                case 60:
                    c0022a.a(60, typedArray.getFloat(index, c0021a.f.b));
                    break;
                case 62:
                    c0022a.b(62, typedArray.getDimensionPixelSize(index, c0021a.e.A));
                    break;
                case 63:
                    c0022a.a(63, typedArray.getFloat(index, c0021a.e.B));
                    break;
                case 64:
                    c0022a.b(64, F(typedArray, index, c0021a.d.b));
                    break;
                case 65:
                    if (typedArray.peekValue(index).type == 3) {
                        c0022a.c(65, typedArray.getString(index));
                        break;
                    } else {
                        c0022a.c(65, yt0.c[typedArray.getInteger(index, 0)]);
                        break;
                    }
                case 66:
                    c0022a.b(66, typedArray.getInt(index, 0));
                    break;
                case 67:
                    c0022a.a(67, typedArray.getFloat(index, c0021a.d.i));
                    break;
                case 68:
                    c0022a.a(68, typedArray.getFloat(index, c0021a.c.e));
                    break;
                case 69:
                    c0022a.a(69, typedArray.getFloat(index, 1.0f));
                    break;
                case 70:
                    c0022a.a(70, typedArray.getFloat(index, 1.0f));
                    break;
                case 71:
                    break;
                case 72:
                    c0022a.b(72, typedArray.getInt(index, c0021a.e.f0));
                    break;
                case 73:
                    c0022a.b(73, typedArray.getDimensionPixelSize(index, c0021a.e.g0));
                    break;
                case 74:
                    c0022a.c(74, typedArray.getString(index));
                    break;
                case 75:
                    c0022a.d(75, typedArray.getBoolean(index, c0021a.e.n0));
                    break;
                case 76:
                    c0022a.b(76, typedArray.getInt(index, c0021a.d.e));
                    break;
                case 77:
                    c0022a.c(77, typedArray.getString(index));
                    break;
                case 78:
                    c0022a.b(78, typedArray.getInt(index, c0021a.c.c));
                    break;
                case 79:
                    c0022a.a(79, typedArray.getFloat(index, c0021a.d.g));
                    break;
                case 80:
                    c0022a.d(80, typedArray.getBoolean(index, c0021a.e.l0));
                    break;
                case 81:
                    c0022a.d(81, typedArray.getBoolean(index, c0021a.e.m0));
                    break;
                case 82:
                    c0022a.b(82, typedArray.getInteger(index, c0021a.d.c));
                    break;
                case 83:
                    c0022a.b(83, F(typedArray, index, c0021a.f.i));
                    break;
                case 84:
                    c0022a.b(84, typedArray.getInteger(index, c0021a.d.k));
                    break;
                case 85:
                    c0022a.a(85, typedArray.getFloat(index, c0021a.d.j));
                    break;
                case 86:
                    int i3 = typedArray.peekValue(index).type;
                    if (i3 == 1) {
                        c0021a.d.n = typedArray.getResourceId(index, -1);
                        c0022a.b(89, c0021a.d.n);
                        c cVar = c0021a.d;
                        if (cVar.n != -1) {
                            cVar.m = -2;
                            c0022a.b(88, -2);
                            break;
                        } else {
                            break;
                        }
                    } else if (i3 == 3) {
                        c0021a.d.l = typedArray.getString(index);
                        c0022a.c(90, c0021a.d.l);
                        if (c0021a.d.l.indexOf("/") > 0) {
                            c0021a.d.n = typedArray.getResourceId(index, -1);
                            c0022a.b(89, c0021a.d.n);
                            c0021a.d.m = -2;
                            c0022a.b(88, -2);
                            break;
                        } else {
                            c0021a.d.m = -1;
                            c0022a.b(88, -1);
                            break;
                        }
                    } else {
                        c cVar2 = c0021a.d;
                        cVar2.m = typedArray.getInteger(index, cVar2.n);
                        c0022a.b(88, c0021a.d.m);
                        break;
                    }
                case 87:
                    StringBuilder sb2 = new StringBuilder();
                    sb2.append("unused attribute 0x");
                    sb2.append(Integer.toHexString(index));
                    sb2.append("   ");
                    sb2.append(h.get(index));
                    break;
                case 93:
                    c0022a.b(93, typedArray.getDimensionPixelSize(index, c0021a.e.L));
                    break;
                case 94:
                    c0022a.b(94, typedArray.getDimensionPixelSize(index, c0021a.e.S));
                    break;
                case 95:
                    G(c0022a, typedArray, index, 0);
                    break;
                case 96:
                    G(c0022a, typedArray, index, 1);
                    break;
                case 97:
                    c0022a.b(97, typedArray.getInt(index, c0021a.e.o0));
                    break;
                case 98:
                    if (MotionLayout.T1) {
                        int resourceId2 = typedArray.getResourceId(index, c0021a.a);
                        c0021a.a = resourceId2;
                        if (resourceId2 == -1) {
                            c0021a.b = typedArray.getString(index);
                            break;
                        } else {
                            break;
                        }
                    } else if (typedArray.peekValue(index).type == 3) {
                        c0021a.b = typedArray.getString(index);
                        break;
                    } else {
                        c0021a.a = typedArray.getResourceId(index, c0021a.a);
                        break;
                    }
            }
        }
    }

    public static void N(C0021a c0021a, int i2, float f) {
        if (i2 == 19) {
            c0021a.e.g = f;
        } else if (i2 == 20) {
            c0021a.e.w = f;
        } else if (i2 == 37) {
            c0021a.e.x = f;
        } else if (i2 == 60) {
            c0021a.f.b = f;
        } else if (i2 == 63) {
            c0021a.e.B = f;
        } else if (i2 == 79) {
            c0021a.d.g = f;
        } else if (i2 == 85) {
            c0021a.d.j = f;
        } else if (i2 == 39) {
            c0021a.e.U = f;
        } else if (i2 != 40) {
            switch (i2) {
                case 43:
                    c0021a.c.d = f;
                    return;
                case 44:
                    e eVar = c0021a.f;
                    eVar.n = f;
                    eVar.m = true;
                    return;
                case 45:
                    c0021a.f.c = f;
                    return;
                case 46:
                    c0021a.f.d = f;
                    return;
                case 47:
                    c0021a.f.e = f;
                    return;
                case 48:
                    c0021a.f.f = f;
                    return;
                case 49:
                    c0021a.f.g = f;
                    return;
                case 50:
                    c0021a.f.h = f;
                    return;
                case 51:
                    c0021a.f.j = f;
                    return;
                case 52:
                    c0021a.f.k = f;
                    return;
                case 53:
                    c0021a.f.l = f;
                    return;
                default:
                    switch (i2) {
                        case 67:
                            c0021a.d.i = f;
                            return;
                        case 68:
                            c0021a.c.e = f;
                            return;
                        case 69:
                            c0021a.e.d0 = f;
                            return;
                        case 70:
                            c0021a.e.e0 = f;
                            return;
                        default:
                            return;
                    }
            }
        } else {
            c0021a.e.T = f;
        }
    }

    public static void O(C0021a c0021a, int i2, int i3) {
        if (i2 == 6) {
            c0021a.e.C = i3;
        } else if (i2 == 7) {
            c0021a.e.D = i3;
        } else if (i2 == 8) {
            c0021a.e.J = i3;
        } else if (i2 == 27) {
            c0021a.e.E = i3;
        } else if (i2 == 28) {
            c0021a.e.G = i3;
        } else if (i2 == 41) {
            c0021a.e.V = i3;
        } else if (i2 == 42) {
            c0021a.e.W = i3;
        } else if (i2 == 61) {
            c0021a.e.z = i3;
        } else if (i2 == 62) {
            c0021a.e.A = i3;
        } else if (i2 == 72) {
            c0021a.e.f0 = i3;
        } else if (i2 == 73) {
            c0021a.e.g0 = i3;
        } else if (i2 == 88) {
            c0021a.d.m = i3;
        } else if (i2 != 89) {
            switch (i2) {
                case 2:
                    c0021a.e.I = i3;
                    return;
                case 11:
                    c0021a.e.P = i3;
                    return;
                case 12:
                    c0021a.e.Q = i3;
                    return;
                case 13:
                    c0021a.e.M = i3;
                    return;
                case 14:
                    c0021a.e.O = i3;
                    return;
                case 15:
                    c0021a.e.R = i3;
                    return;
                case 16:
                    c0021a.e.N = i3;
                    return;
                case 17:
                    c0021a.e.e = i3;
                    return;
                case 18:
                    c0021a.e.f = i3;
                    return;
                case 31:
                    c0021a.e.K = i3;
                    return;
                case 34:
                    c0021a.e.H = i3;
                    return;
                case 38:
                    c0021a.a = i3;
                    return;
                case 64:
                    c0021a.d.b = i3;
                    return;
                case 66:
                    c0021a.d.f = i3;
                    return;
                case 76:
                    c0021a.d.e = i3;
                    return;
                case 78:
                    c0021a.c.c = i3;
                    return;
                case 93:
                    c0021a.e.L = i3;
                    return;
                case 94:
                    c0021a.e.S = i3;
                    return;
                case 97:
                    c0021a.e.o0 = i3;
                    return;
                default:
                    switch (i2) {
                        case 21:
                            c0021a.e.d = i3;
                            return;
                        case 22:
                            c0021a.c.b = i3;
                            return;
                        case 23:
                            c0021a.e.c = i3;
                            return;
                        case 24:
                            c0021a.e.F = i3;
                            return;
                        default:
                            switch (i2) {
                                case 54:
                                    c0021a.e.X = i3;
                                    return;
                                case 55:
                                    c0021a.e.Y = i3;
                                    return;
                                case 56:
                                    c0021a.e.Z = i3;
                                    return;
                                case 57:
                                    c0021a.e.a0 = i3;
                                    return;
                                case 58:
                                    c0021a.e.b0 = i3;
                                    return;
                                case 59:
                                    c0021a.e.c0 = i3;
                                    return;
                                default:
                                    switch (i2) {
                                        case 82:
                                            c0021a.d.c = i3;
                                            return;
                                        case 83:
                                            c0021a.f.i = i3;
                                            return;
                                        case 84:
                                            c0021a.d.k = i3;
                                            return;
                                        default:
                                            return;
                                    }
                            }
                    }
            }
        } else {
            c0021a.d.n = i3;
        }
    }

    public static void P(C0021a c0021a, int i2, String str) {
        if (i2 == 5) {
            c0021a.e.y = str;
        } else if (i2 == 65) {
            c0021a.d.d = str;
        } else if (i2 == 74) {
            b bVar = c0021a.e;
            bVar.j0 = str;
            bVar.i0 = null;
        } else if (i2 == 77) {
            c0021a.e.k0 = str;
        } else if (i2 != 90) {
        } else {
            c0021a.d.l = str;
        }
    }

    public static void Q(C0021a c0021a, int i2, boolean z) {
        if (i2 == 44) {
            c0021a.f.m = z;
        } else if (i2 == 75) {
            c0021a.e.n0 = z;
        } else if (i2 == 80) {
            c0021a.e.l0 = z;
        } else if (i2 != 81) {
        } else {
            c0021a.e.m0 = z;
        }
    }

    public static C0021a m(Context context, XmlPullParser xmlPullParser) {
        AttributeSet asAttributeSet = Xml.asAttributeSet(xmlPullParser);
        C0021a c0021a = new C0021a();
        TypedArray obtainStyledAttributes = context.obtainStyledAttributes(asAttributeSet, w23.ConstraintOverride);
        K(context, c0021a, obtainStyledAttributes);
        obtainStyledAttributes.recycle();
        return c0021a;
    }

    public int A(int i2) {
        return v(i2).c.b;
    }

    public int B(int i2) {
        return v(i2).c.c;
    }

    public int C(int i2) {
        return v(i2).e.c;
    }

    public void D(Context context, int i2) {
        XmlResourceParser xml = context.getResources().getXml(i2);
        try {
            for (int eventType = xml.getEventType(); eventType != 1; eventType = xml.next()) {
                if (eventType == 0) {
                    xml.getName();
                    continue;
                } else if (eventType != 2) {
                    continue;
                } else {
                    String name = xml.getName();
                    C0021a u = u(context, Xml.asAttributeSet(xml), false);
                    if (name.equalsIgnoreCase("Guideline")) {
                        u.e.a = true;
                    }
                    this.f.put(Integer.valueOf(u.a), u);
                    continue;
                }
            }
        } catch (IOException e2) {
            e2.printStackTrace();
        } catch (XmlPullParserException e3) {
            e3.printStackTrace();
        }
    }

    /* JADX WARN: Code restructure failed: missing block: B:119:0x01cb, code lost:
        continue;
     */
    /*
        Code decompiled incorrectly, please refer to instructions dump.
        To view partially-correct code enable 'Show inconsistent code' option in preferences
    */
    public void E(android.content.Context r10, org.xmlpull.v1.XmlPullParser r11) {
        /*
            Method dump skipped, instructions count: 560
            To view this dump change 'Code comments level' option to 'DEBUG'
        */
        throw new UnsupportedOperationException("Method not decompiled: androidx.constraintlayout.widget.a.E(android.content.Context, org.xmlpull.v1.XmlPullParser):void");
    }

    public final void J(Context context, C0021a c0021a, TypedArray typedArray, boolean z) {
        if (z) {
            K(context, c0021a, typedArray);
            return;
        }
        int indexCount = typedArray.getIndexCount();
        for (int i2 = 0; i2 < indexCount; i2++) {
            int index = typedArray.getIndex(i2);
            if (index != w23.Constraint_android_id && w23.Constraint_android_layout_marginStart != index && w23.Constraint_android_layout_marginEnd != index) {
                c0021a.d.a = true;
                c0021a.e.b = true;
                c0021a.c.a = true;
                c0021a.f.a = true;
            }
            switch (h.get(index)) {
                case 1:
                    b bVar = c0021a.e;
                    bVar.p = F(typedArray, index, bVar.p);
                    break;
                case 2:
                    b bVar2 = c0021a.e;
                    bVar2.I = typedArray.getDimensionPixelSize(index, bVar2.I);
                    break;
                case 3:
                    b bVar3 = c0021a.e;
                    bVar3.o = F(typedArray, index, bVar3.o);
                    break;
                case 4:
                    b bVar4 = c0021a.e;
                    bVar4.n = F(typedArray, index, bVar4.n);
                    break;
                case 5:
                    c0021a.e.y = typedArray.getString(index);
                    break;
                case 6:
                    b bVar5 = c0021a.e;
                    bVar5.C = typedArray.getDimensionPixelOffset(index, bVar5.C);
                    break;
                case 7:
                    b bVar6 = c0021a.e;
                    bVar6.D = typedArray.getDimensionPixelOffset(index, bVar6.D);
                    break;
                case 8:
                    if (Build.VERSION.SDK_INT >= 17) {
                        b bVar7 = c0021a.e;
                        bVar7.J = typedArray.getDimensionPixelSize(index, bVar7.J);
                        break;
                    } else {
                        break;
                    }
                case 9:
                    b bVar8 = c0021a.e;
                    bVar8.v = F(typedArray, index, bVar8.v);
                    break;
                case 10:
                    b bVar9 = c0021a.e;
                    bVar9.u = F(typedArray, index, bVar9.u);
                    break;
                case 11:
                    b bVar10 = c0021a.e;
                    bVar10.P = typedArray.getDimensionPixelSize(index, bVar10.P);
                    break;
                case 12:
                    b bVar11 = c0021a.e;
                    bVar11.Q = typedArray.getDimensionPixelSize(index, bVar11.Q);
                    break;
                case 13:
                    b bVar12 = c0021a.e;
                    bVar12.M = typedArray.getDimensionPixelSize(index, bVar12.M);
                    break;
                case 14:
                    b bVar13 = c0021a.e;
                    bVar13.O = typedArray.getDimensionPixelSize(index, bVar13.O);
                    break;
                case 15:
                    b bVar14 = c0021a.e;
                    bVar14.R = typedArray.getDimensionPixelSize(index, bVar14.R);
                    break;
                case 16:
                    b bVar15 = c0021a.e;
                    bVar15.N = typedArray.getDimensionPixelSize(index, bVar15.N);
                    break;
                case 17:
                    b bVar16 = c0021a.e;
                    bVar16.e = typedArray.getDimensionPixelOffset(index, bVar16.e);
                    break;
                case 18:
                    b bVar17 = c0021a.e;
                    bVar17.f = typedArray.getDimensionPixelOffset(index, bVar17.f);
                    break;
                case 19:
                    b bVar18 = c0021a.e;
                    bVar18.g = typedArray.getFloat(index, bVar18.g);
                    break;
                case 20:
                    b bVar19 = c0021a.e;
                    bVar19.w = typedArray.getFloat(index, bVar19.w);
                    break;
                case 21:
                    b bVar20 = c0021a.e;
                    bVar20.d = typedArray.getLayoutDimension(index, bVar20.d);
                    break;
                case 22:
                    d dVar = c0021a.c;
                    dVar.b = typedArray.getInt(index, dVar.b);
                    d dVar2 = c0021a.c;
                    dVar2.b = g[dVar2.b];
                    break;
                case 23:
                    b bVar21 = c0021a.e;
                    bVar21.c = typedArray.getLayoutDimension(index, bVar21.c);
                    break;
                case 24:
                    b bVar22 = c0021a.e;
                    bVar22.F = typedArray.getDimensionPixelSize(index, bVar22.F);
                    break;
                case 25:
                    b bVar23 = c0021a.e;
                    bVar23.h = F(typedArray, index, bVar23.h);
                    break;
                case 26:
                    b bVar24 = c0021a.e;
                    bVar24.i = F(typedArray, index, bVar24.i);
                    break;
                case 27:
                    b bVar25 = c0021a.e;
                    bVar25.E = typedArray.getInt(index, bVar25.E);
                    break;
                case 28:
                    b bVar26 = c0021a.e;
                    bVar26.G = typedArray.getDimensionPixelSize(index, bVar26.G);
                    break;
                case 29:
                    b bVar27 = c0021a.e;
                    bVar27.j = F(typedArray, index, bVar27.j);
                    break;
                case 30:
                    b bVar28 = c0021a.e;
                    bVar28.k = F(typedArray, index, bVar28.k);
                    break;
                case 31:
                    if (Build.VERSION.SDK_INT >= 17) {
                        b bVar29 = c0021a.e;
                        bVar29.K = typedArray.getDimensionPixelSize(index, bVar29.K);
                        break;
                    } else {
                        break;
                    }
                case 32:
                    b bVar30 = c0021a.e;
                    bVar30.s = F(typedArray, index, bVar30.s);
                    break;
                case 33:
                    b bVar31 = c0021a.e;
                    bVar31.t = F(typedArray, index, bVar31.t);
                    break;
                case 34:
                    b bVar32 = c0021a.e;
                    bVar32.H = typedArray.getDimensionPixelSize(index, bVar32.H);
                    break;
                case 35:
                    b bVar33 = c0021a.e;
                    bVar33.m = F(typedArray, index, bVar33.m);
                    break;
                case 36:
                    b bVar34 = c0021a.e;
                    bVar34.l = F(typedArray, index, bVar34.l);
                    break;
                case 37:
                    b bVar35 = c0021a.e;
                    bVar35.x = typedArray.getFloat(index, bVar35.x);
                    break;
                case 38:
                    c0021a.a = typedArray.getResourceId(index, c0021a.a);
                    break;
                case 39:
                    b bVar36 = c0021a.e;
                    bVar36.U = typedArray.getFloat(index, bVar36.U);
                    break;
                case 40:
                    b bVar37 = c0021a.e;
                    bVar37.T = typedArray.getFloat(index, bVar37.T);
                    break;
                case 41:
                    b bVar38 = c0021a.e;
                    bVar38.V = typedArray.getInt(index, bVar38.V);
                    break;
                case 42:
                    b bVar39 = c0021a.e;
                    bVar39.W = typedArray.getInt(index, bVar39.W);
                    break;
                case 43:
                    d dVar3 = c0021a.c;
                    dVar3.d = typedArray.getFloat(index, dVar3.d);
                    break;
                case 44:
                    if (Build.VERSION.SDK_INT >= 21) {
                        e eVar = c0021a.f;
                        eVar.m = true;
                        eVar.n = typedArray.getDimension(index, eVar.n);
                        break;
                    } else {
                        break;
                    }
                case 45:
                    e eVar2 = c0021a.f;
                    eVar2.c = typedArray.getFloat(index, eVar2.c);
                    break;
                case 46:
                    e eVar3 = c0021a.f;
                    eVar3.d = typedArray.getFloat(index, eVar3.d);
                    break;
                case 47:
                    e eVar4 = c0021a.f;
                    eVar4.e = typedArray.getFloat(index, eVar4.e);
                    break;
                case 48:
                    e eVar5 = c0021a.f;
                    eVar5.f = typedArray.getFloat(index, eVar5.f);
                    break;
                case 49:
                    e eVar6 = c0021a.f;
                    eVar6.g = typedArray.getDimension(index, eVar6.g);
                    break;
                case 50:
                    e eVar7 = c0021a.f;
                    eVar7.h = typedArray.getDimension(index, eVar7.h);
                    break;
                case 51:
                    e eVar8 = c0021a.f;
                    eVar8.j = typedArray.getDimension(index, eVar8.j);
                    break;
                case 52:
                    e eVar9 = c0021a.f;
                    eVar9.k = typedArray.getDimension(index, eVar9.k);
                    break;
                case 53:
                    if (Build.VERSION.SDK_INT >= 21) {
                        e eVar10 = c0021a.f;
                        eVar10.l = typedArray.getDimension(index, eVar10.l);
                        break;
                    } else {
                        break;
                    }
                case 54:
                    b bVar40 = c0021a.e;
                    bVar40.X = typedArray.getInt(index, bVar40.X);
                    break;
                case 55:
                    b bVar41 = c0021a.e;
                    bVar41.Y = typedArray.getInt(index, bVar41.Y);
                    break;
                case 56:
                    b bVar42 = c0021a.e;
                    bVar42.Z = typedArray.getDimensionPixelSize(index, bVar42.Z);
                    break;
                case 57:
                    b bVar43 = c0021a.e;
                    bVar43.a0 = typedArray.getDimensionPixelSize(index, bVar43.a0);
                    break;
                case 58:
                    b bVar44 = c0021a.e;
                    bVar44.b0 = typedArray.getDimensionPixelSize(index, bVar44.b0);
                    break;
                case 59:
                    b bVar45 = c0021a.e;
                    bVar45.c0 = typedArray.getDimensionPixelSize(index, bVar45.c0);
                    break;
                case 60:
                    e eVar11 = c0021a.f;
                    eVar11.b = typedArray.getFloat(index, eVar11.b);
                    break;
                case 61:
                    b bVar46 = c0021a.e;
                    bVar46.z = F(typedArray, index, bVar46.z);
                    break;
                case 62:
                    b bVar47 = c0021a.e;
                    bVar47.A = typedArray.getDimensionPixelSize(index, bVar47.A);
                    break;
                case 63:
                    b bVar48 = c0021a.e;
                    bVar48.B = typedArray.getFloat(index, bVar48.B);
                    break;
                case 64:
                    c cVar = c0021a.d;
                    cVar.b = F(typedArray, index, cVar.b);
                    break;
                case 65:
                    if (typedArray.peekValue(index).type == 3) {
                        c0021a.d.d = typedArray.getString(index);
                        break;
                    } else {
                        c0021a.d.d = yt0.c[typedArray.getInteger(index, 0)];
                        break;
                    }
                case 66:
                    c0021a.d.f = typedArray.getInt(index, 0);
                    break;
                case 67:
                    c cVar2 = c0021a.d;
                    cVar2.i = typedArray.getFloat(index, cVar2.i);
                    break;
                case 68:
                    d dVar4 = c0021a.c;
                    dVar4.e = typedArray.getFloat(index, dVar4.e);
                    break;
                case 69:
                    c0021a.e.d0 = typedArray.getFloat(index, 1.0f);
                    break;
                case 70:
                    c0021a.e.e0 = typedArray.getFloat(index, 1.0f);
                    break;
                case 71:
                    break;
                case 72:
                    b bVar49 = c0021a.e;
                    bVar49.f0 = typedArray.getInt(index, bVar49.f0);
                    break;
                case 73:
                    b bVar50 = c0021a.e;
                    bVar50.g0 = typedArray.getDimensionPixelSize(index, bVar50.g0);
                    break;
                case 74:
                    c0021a.e.j0 = typedArray.getString(index);
                    break;
                case 75:
                    b bVar51 = c0021a.e;
                    bVar51.n0 = typedArray.getBoolean(index, bVar51.n0);
                    break;
                case 76:
                    c cVar3 = c0021a.d;
                    cVar3.e = typedArray.getInt(index, cVar3.e);
                    break;
                case 77:
                    c0021a.e.k0 = typedArray.getString(index);
                    break;
                case 78:
                    d dVar5 = c0021a.c;
                    dVar5.c = typedArray.getInt(index, dVar5.c);
                    break;
                case 79:
                    c cVar4 = c0021a.d;
                    cVar4.g = typedArray.getFloat(index, cVar4.g);
                    break;
                case 80:
                    b bVar52 = c0021a.e;
                    bVar52.l0 = typedArray.getBoolean(index, bVar52.l0);
                    break;
                case 81:
                    b bVar53 = c0021a.e;
                    bVar53.m0 = typedArray.getBoolean(index, bVar53.m0);
                    break;
                case 82:
                    c cVar5 = c0021a.d;
                    cVar5.c = typedArray.getInteger(index, cVar5.c);
                    break;
                case 83:
                    e eVar12 = c0021a.f;
                    eVar12.i = F(typedArray, index, eVar12.i);
                    break;
                case 84:
                    c cVar6 = c0021a.d;
                    cVar6.k = typedArray.getInteger(index, cVar6.k);
                    break;
                case 85:
                    c cVar7 = c0021a.d;
                    cVar7.j = typedArray.getFloat(index, cVar7.j);
                    break;
                case 86:
                    int i3 = typedArray.peekValue(index).type;
                    if (i3 == 1) {
                        c0021a.d.n = typedArray.getResourceId(index, -1);
                        c cVar8 = c0021a.d;
                        if (cVar8.n != -1) {
                            cVar8.m = -2;
                            break;
                        } else {
                            break;
                        }
                    } else if (i3 == 3) {
                        c0021a.d.l = typedArray.getString(index);
                        if (c0021a.d.l.indexOf("/") > 0) {
                            c0021a.d.n = typedArray.getResourceId(index, -1);
                            c0021a.d.m = -2;
                            break;
                        } else {
                            c0021a.d.m = -1;
                            break;
                        }
                    } else {
                        c cVar9 = c0021a.d;
                        cVar9.m = typedArray.getInteger(index, cVar9.n);
                        break;
                    }
                case 87:
                    StringBuilder sb = new StringBuilder();
                    sb.append("unused attribute 0x");
                    sb.append(Integer.toHexString(index));
                    sb.append("   ");
                    sb.append(h.get(index));
                    break;
                case 88:
                case 89:
                case 90:
                default:
                    StringBuilder sb2 = new StringBuilder();
                    sb2.append("Unknown attribute 0x");
                    sb2.append(Integer.toHexString(index));
                    sb2.append("   ");
                    sb2.append(h.get(index));
                    break;
                case 91:
                    b bVar54 = c0021a.e;
                    bVar54.q = F(typedArray, index, bVar54.q);
                    break;
                case 92:
                    b bVar55 = c0021a.e;
                    bVar55.r = F(typedArray, index, bVar55.r);
                    break;
                case 93:
                    b bVar56 = c0021a.e;
                    bVar56.L = typedArray.getDimensionPixelSize(index, bVar56.L);
                    break;
                case 94:
                    b bVar57 = c0021a.e;
                    bVar57.S = typedArray.getDimensionPixelSize(index, bVar57.S);
                    break;
                case 95:
                    G(c0021a.e, typedArray, index, 0);
                    break;
                case 96:
                    G(c0021a.e, typedArray, index, 1);
                    break;
                case 97:
                    b bVar58 = c0021a.e;
                    bVar58.o0 = typedArray.getInt(index, bVar58.o0);
                    break;
            }
        }
        b bVar59 = c0021a.e;
        if (bVar59.j0 != null) {
            bVar59.i0 = null;
        }
    }

    public void L(ConstraintLayout constraintLayout) {
        int childCount = constraintLayout.getChildCount();
        for (int i2 = 0; i2 < childCount; i2++) {
            View childAt = constraintLayout.getChildAt(i2);
            ConstraintLayout.LayoutParams layoutParams = (ConstraintLayout.LayoutParams) childAt.getLayoutParams();
            int id = childAt.getId();
            if (this.e && id == -1) {
                throw new RuntimeException("All children of ConstraintLayout must have ids to use ConstraintSet");
            }
            if (!this.f.containsKey(Integer.valueOf(id))) {
                this.f.put(Integer.valueOf(id), new C0021a());
            }
            C0021a c0021a = this.f.get(Integer.valueOf(id));
            if (c0021a != null) {
                if (!c0021a.e.b) {
                    c0021a.g(id, layoutParams);
                    if (childAt instanceof ConstraintHelper) {
                        c0021a.e.i0 = ((ConstraintHelper) childAt).getReferencedIds();
                        if (childAt instanceof Barrier) {
                            Barrier barrier = (Barrier) childAt;
                            c0021a.e.n0 = barrier.getAllowsGoneWidget();
                            c0021a.e.f0 = barrier.getType();
                            c0021a.e.g0 = barrier.getMargin();
                        }
                    }
                    c0021a.e.b = true;
                }
                d dVar = c0021a.c;
                if (!dVar.a) {
                    dVar.b = childAt.getVisibility();
                    c0021a.c.d = childAt.getAlpha();
                    c0021a.c.a = true;
                }
                int i3 = Build.VERSION.SDK_INT;
                if (i3 >= 17) {
                    e eVar = c0021a.f;
                    if (!eVar.a) {
                        eVar.a = true;
                        eVar.b = childAt.getRotation();
                        c0021a.f.c = childAt.getRotationX();
                        c0021a.f.d = childAt.getRotationY();
                        c0021a.f.e = childAt.getScaleX();
                        c0021a.f.f = childAt.getScaleY();
                        float pivotX = childAt.getPivotX();
                        float pivotY = childAt.getPivotY();
                        if (pivotX != Utils.DOUBLE_EPSILON || pivotY != Utils.DOUBLE_EPSILON) {
                            e eVar2 = c0021a.f;
                            eVar2.g = pivotX;
                            eVar2.h = pivotY;
                        }
                        c0021a.f.j = childAt.getTranslationX();
                        c0021a.f.k = childAt.getTranslationY();
                        if (i3 >= 21) {
                            c0021a.f.l = childAt.getTranslationZ();
                            e eVar3 = c0021a.f;
                            if (eVar3.m) {
                                eVar3.n = childAt.getElevation();
                            }
                        }
                    }
                }
            }
        }
    }

    public void M(a aVar) {
        for (Integer num : aVar.f.keySet()) {
            int intValue = num.intValue();
            C0021a c0021a = aVar.f.get(num);
            if (!this.f.containsKey(Integer.valueOf(intValue))) {
                this.f.put(Integer.valueOf(intValue), new C0021a());
            }
            C0021a c0021a2 = this.f.get(Integer.valueOf(intValue));
            if (c0021a2 != null) {
                b bVar = c0021a2.e;
                if (!bVar.b) {
                    bVar.a(c0021a.e);
                }
                d dVar = c0021a2.c;
                if (!dVar.a) {
                    dVar.a(c0021a.c);
                }
                e eVar = c0021a2.f;
                if (!eVar.a) {
                    eVar.a(c0021a.f);
                }
                c cVar = c0021a2.d;
                if (!cVar.a) {
                    cVar.a(c0021a.d);
                }
                for (String str : c0021a.g.keySet()) {
                    if (!c0021a2.g.containsKey(str)) {
                        c0021a2.g.put(str, c0021a.g.get(str));
                    }
                }
            }
        }
    }

    public void R(boolean z) {
        this.e = z;
    }

    public void S(boolean z) {
    }

    public void g(ConstraintLayout constraintLayout) {
        C0021a c0021a;
        int childCount = constraintLayout.getChildCount();
        for (int i2 = 0; i2 < childCount; i2++) {
            View childAt = constraintLayout.getChildAt(i2);
            int id = childAt.getId();
            if (!this.f.containsKey(Integer.valueOf(id))) {
                StringBuilder sb = new StringBuilder();
                sb.append("id unknown ");
                sb.append(xe0.d(childAt));
            } else if (this.e && id == -1) {
                throw new RuntimeException("All children of ConstraintLayout must have ids to use ConstraintSet");
            } else {
                if (this.f.containsKey(Integer.valueOf(id)) && (c0021a = this.f.get(Integer.valueOf(id))) != null) {
                    ConstraintAttribute.j(childAt, c0021a.g);
                }
            }
        }
    }

    public void h(a aVar) {
        for (C0021a c0021a : aVar.f.values()) {
            if (c0021a.h != null) {
                if (c0021a.b != null) {
                    for (Integer num : this.f.keySet()) {
                        C0021a w = w(num.intValue());
                        String str = w.e.k0;
                        if (str != null && c0021a.b.matches(str)) {
                            c0021a.h.e(w);
                            w.g.putAll((HashMap) c0021a.g.clone());
                        }
                    }
                } else {
                    c0021a.h.e(w(c0021a.a));
                }
            }
        }
    }

    public void i(ConstraintLayout constraintLayout) {
        k(constraintLayout, true);
        constraintLayout.setConstraintSet(null);
        constraintLayout.requestLayout();
    }

    public void j(ConstraintHelper constraintHelper, ConstraintWidget constraintWidget, ConstraintLayout.LayoutParams layoutParams, SparseArray<ConstraintWidget> sparseArray) {
        C0021a c0021a;
        int id = constraintHelper.getId();
        if (this.f.containsKey(Integer.valueOf(id)) && (c0021a = this.f.get(Integer.valueOf(id))) != null && (constraintWidget instanceof mk1)) {
            constraintHelper.p(c0021a, (mk1) constraintWidget, layoutParams, sparseArray);
        }
    }

    public void k(ConstraintLayout constraintLayout, boolean z) {
        View findViewById;
        int childCount = constraintLayout.getChildCount();
        HashSet hashSet = new HashSet(this.f.keySet());
        for (int i2 = 0; i2 < childCount; i2++) {
            View childAt = constraintLayout.getChildAt(i2);
            int id = childAt.getId();
            if (!this.f.containsKey(Integer.valueOf(id))) {
                StringBuilder sb = new StringBuilder();
                sb.append("id unknown ");
                sb.append(xe0.d(childAt));
            } else if (this.e && id == -1) {
                throw new RuntimeException("All children of ConstraintLayout must have ids to use ConstraintSet");
            } else {
                if (id != -1) {
                    if (this.f.containsKey(Integer.valueOf(id))) {
                        hashSet.remove(Integer.valueOf(id));
                        C0021a c0021a = this.f.get(Integer.valueOf(id));
                        if (c0021a != null) {
                            if (childAt instanceof Barrier) {
                                c0021a.e.h0 = 1;
                                Barrier barrier = (Barrier) childAt;
                                barrier.setId(id);
                                barrier.setType(c0021a.e.f0);
                                barrier.setMargin(c0021a.e.g0);
                                barrier.setAllowsGoneWidget(c0021a.e.n0);
                                b bVar = c0021a.e;
                                int[] iArr = bVar.i0;
                                if (iArr != null) {
                                    barrier.setReferencedIds(iArr);
                                } else {
                                    String str = bVar.j0;
                                    if (str != null) {
                                        bVar.i0 = t(barrier, str);
                                        barrier.setReferencedIds(c0021a.e.i0);
                                    }
                                }
                            }
                            ConstraintLayout.LayoutParams layoutParams = (ConstraintLayout.LayoutParams) childAt.getLayoutParams();
                            layoutParams.c();
                            c0021a.e(layoutParams);
                            if (z) {
                                ConstraintAttribute.j(childAt, c0021a.g);
                            }
                            childAt.setLayoutParams(layoutParams);
                            d dVar = c0021a.c;
                            if (dVar.c == 0) {
                                childAt.setVisibility(dVar.b);
                            }
                            int i3 = Build.VERSION.SDK_INT;
                            if (i3 >= 17) {
                                childAt.setAlpha(c0021a.c.d);
                                childAt.setRotation(c0021a.f.b);
                                childAt.setRotationX(c0021a.f.c);
                                childAt.setRotationY(c0021a.f.d);
                                childAt.setScaleX(c0021a.f.e);
                                childAt.setScaleY(c0021a.f.f);
                                e eVar = c0021a.f;
                                if (eVar.i != -1) {
                                    if (((View) childAt.getParent()).findViewById(c0021a.f.i) != null) {
                                        float top = (findViewById.getTop() + findViewById.getBottom()) / 2.0f;
                                        float left = (findViewById.getLeft() + findViewById.getRight()) / 2.0f;
                                        if (childAt.getRight() - childAt.getLeft() > 0 && childAt.getBottom() - childAt.getTop() > 0) {
                                            childAt.setPivotX(left - childAt.getLeft());
                                            childAt.setPivotY(top - childAt.getTop());
                                        }
                                    }
                                } else {
                                    if (!Float.isNaN(eVar.g)) {
                                        childAt.setPivotX(c0021a.f.g);
                                    }
                                    if (!Float.isNaN(c0021a.f.h)) {
                                        childAt.setPivotY(c0021a.f.h);
                                    }
                                }
                                childAt.setTranslationX(c0021a.f.j);
                                childAt.setTranslationY(c0021a.f.k);
                                if (i3 >= 21) {
                                    childAt.setTranslationZ(c0021a.f.l);
                                    e eVar2 = c0021a.f;
                                    if (eVar2.m) {
                                        childAt.setElevation(eVar2.n);
                                    }
                                }
                            }
                        }
                    } else {
                        StringBuilder sb2 = new StringBuilder();
                        sb2.append("WARNING NO CONSTRAINTS for view ");
                        sb2.append(id);
                    }
                }
            }
        }
        Iterator it = hashSet.iterator();
        while (it.hasNext()) {
            Integer num = (Integer) it.next();
            C0021a c0021a2 = this.f.get(num);
            if (c0021a2 != null) {
                if (c0021a2.e.h0 == 1) {
                    Barrier barrier2 = new Barrier(constraintLayout.getContext());
                    barrier2.setId(num.intValue());
                    b bVar2 = c0021a2.e;
                    int[] iArr2 = bVar2.i0;
                    if (iArr2 != null) {
                        barrier2.setReferencedIds(iArr2);
                    } else {
                        String str2 = bVar2.j0;
                        if (str2 != null) {
                            bVar2.i0 = t(barrier2, str2);
                            barrier2.setReferencedIds(c0021a2.e.i0);
                        }
                    }
                    barrier2.setType(c0021a2.e.f0);
                    barrier2.setMargin(c0021a2.e.g0);
                    ConstraintLayout.LayoutParams generateDefaultLayoutParams = constraintLayout.generateDefaultLayoutParams();
                    barrier2.w();
                    c0021a2.e(generateDefaultLayoutParams);
                    constraintLayout.addView(barrier2, generateDefaultLayoutParams);
                }
                if (c0021a2.e.a) {
                    View guideline = new Guideline(constraintLayout.getContext());
                    guideline.setId(num.intValue());
                    ConstraintLayout.LayoutParams generateDefaultLayoutParams2 = constraintLayout.generateDefaultLayoutParams();
                    c0021a2.e(generateDefaultLayoutParams2);
                    constraintLayout.addView(guideline, generateDefaultLayoutParams2);
                }
            }
        }
        for (int i4 = 0; i4 < childCount; i4++) {
            View childAt2 = constraintLayout.getChildAt(i4);
            if (childAt2 instanceof ConstraintHelper) {
                ((ConstraintHelper) childAt2).j(constraintLayout);
            }
        }
    }

    public void l(int i2, ConstraintLayout.LayoutParams layoutParams) {
        C0021a c0021a;
        if (!this.f.containsKey(Integer.valueOf(i2)) || (c0021a = this.f.get(Integer.valueOf(i2))) == null) {
            return;
        }
        c0021a.e(layoutParams);
    }

    public void n(int i2, int i3) {
        C0021a c0021a;
        if (!this.f.containsKey(Integer.valueOf(i2)) || (c0021a = this.f.get(Integer.valueOf(i2))) == null) {
            return;
        }
        switch (i3) {
            case 1:
                b bVar = c0021a.e;
                bVar.i = -1;
                bVar.h = -1;
                bVar.F = -1;
                bVar.M = Integer.MIN_VALUE;
                return;
            case 2:
                b bVar2 = c0021a.e;
                bVar2.k = -1;
                bVar2.j = -1;
                bVar2.G = -1;
                bVar2.O = Integer.MIN_VALUE;
                return;
            case 3:
                b bVar3 = c0021a.e;
                bVar3.m = -1;
                bVar3.l = -1;
                bVar3.H = 0;
                bVar3.N = Integer.MIN_VALUE;
                return;
            case 4:
                b bVar4 = c0021a.e;
                bVar4.n = -1;
                bVar4.o = -1;
                bVar4.I = 0;
                bVar4.P = Integer.MIN_VALUE;
                return;
            case 5:
                b bVar5 = c0021a.e;
                bVar5.p = -1;
                bVar5.q = -1;
                bVar5.r = -1;
                bVar5.L = 0;
                bVar5.S = Integer.MIN_VALUE;
                return;
            case 6:
                b bVar6 = c0021a.e;
                bVar6.s = -1;
                bVar6.t = -1;
                bVar6.K = 0;
                bVar6.R = Integer.MIN_VALUE;
                return;
            case 7:
                b bVar7 = c0021a.e;
                bVar7.u = -1;
                bVar7.v = -1;
                bVar7.J = 0;
                bVar7.Q = Integer.MIN_VALUE;
                return;
            case 8:
                b bVar8 = c0021a.e;
                bVar8.B = -1.0f;
                bVar8.A = -1;
                bVar8.z = -1;
                return;
            default:
                throw new IllegalArgumentException("unknown constraint");
        }
    }

    public void o(Context context, int i2) {
        p((ConstraintLayout) LayoutInflater.from(context).inflate(i2, (ViewGroup) null));
    }

    public void p(ConstraintLayout constraintLayout) {
        int childCount = constraintLayout.getChildCount();
        this.f.clear();
        for (int i2 = 0; i2 < childCount; i2++) {
            View childAt = constraintLayout.getChildAt(i2);
            ConstraintLayout.LayoutParams layoutParams = (ConstraintLayout.LayoutParams) childAt.getLayoutParams();
            int id = childAt.getId();
            if (this.e && id == -1) {
                throw new RuntimeException("All children of ConstraintLayout must have ids to use ConstraintSet");
            }
            if (!this.f.containsKey(Integer.valueOf(id))) {
                this.f.put(Integer.valueOf(id), new C0021a());
            }
            C0021a c0021a = this.f.get(Integer.valueOf(id));
            if (c0021a != null) {
                c0021a.g = ConstraintAttribute.c(this.d, childAt);
                c0021a.g(id, layoutParams);
                c0021a.c.b = childAt.getVisibility();
                int i3 = Build.VERSION.SDK_INT;
                if (i3 >= 17) {
                    c0021a.c.d = childAt.getAlpha();
                    c0021a.f.b = childAt.getRotation();
                    c0021a.f.c = childAt.getRotationX();
                    c0021a.f.d = childAt.getRotationY();
                    c0021a.f.e = childAt.getScaleX();
                    c0021a.f.f = childAt.getScaleY();
                    float pivotX = childAt.getPivotX();
                    float pivotY = childAt.getPivotY();
                    if (pivotX != Utils.DOUBLE_EPSILON || pivotY != Utils.DOUBLE_EPSILON) {
                        e eVar = c0021a.f;
                        eVar.g = pivotX;
                        eVar.h = pivotY;
                    }
                    c0021a.f.j = childAt.getTranslationX();
                    c0021a.f.k = childAt.getTranslationY();
                    if (i3 >= 21) {
                        c0021a.f.l = childAt.getTranslationZ();
                        e eVar2 = c0021a.f;
                        if (eVar2.m) {
                            eVar2.n = childAt.getElevation();
                        }
                    }
                }
                if (childAt instanceof Barrier) {
                    Barrier barrier = (Barrier) childAt;
                    c0021a.e.n0 = barrier.getAllowsGoneWidget();
                    c0021a.e.i0 = barrier.getReferencedIds();
                    c0021a.e.f0 = barrier.getType();
                    c0021a.e.g0 = barrier.getMargin();
                }
            }
        }
    }

    public void q(a aVar) {
        this.f.clear();
        for (Integer num : aVar.f.keySet()) {
            C0021a c0021a = aVar.f.get(num);
            if (c0021a != null) {
                this.f.put(num, c0021a.clone());
            }
        }
    }

    public void r(Constraints constraints) {
        int childCount = constraints.getChildCount();
        this.f.clear();
        for (int i2 = 0; i2 < childCount; i2++) {
            View childAt = constraints.getChildAt(i2);
            Constraints.LayoutParams layoutParams = (Constraints.LayoutParams) childAt.getLayoutParams();
            int id = childAt.getId();
            if (this.e && id == -1) {
                throw new RuntimeException("All children of ConstraintLayout must have ids to use ConstraintSet");
            }
            if (!this.f.containsKey(Integer.valueOf(id))) {
                this.f.put(Integer.valueOf(id), new C0021a());
            }
            C0021a c0021a = this.f.get(Integer.valueOf(id));
            if (c0021a != null) {
                if (childAt instanceof ConstraintHelper) {
                    c0021a.i((ConstraintHelper) childAt, id, layoutParams);
                }
                c0021a.h(id, layoutParams);
            }
        }
    }

    public void s(int i2, int i3, int i4, float f) {
        b bVar = v(i2).e;
        bVar.z = i3;
        bVar.A = i4;
        bVar.B = f;
    }

    public final int[] t(View view, String str) {
        int i2;
        Object g2;
        String[] split = str.split(",");
        Context context = view.getContext();
        int[] iArr = new int[split.length];
        int i3 = 0;
        int i4 = 0;
        while (i3 < split.length) {
            String trim = split[i3].trim();
            try {
                i2 = i03.class.getField(trim).getInt(null);
            } catch (Exception unused) {
                i2 = 0;
            }
            if (i2 == 0) {
                i2 = context.getResources().getIdentifier(trim, "id", context.getPackageName());
            }
            if (i2 == 0 && view.isInEditMode() && (view.getParent() instanceof ConstraintLayout) && (g2 = ((ConstraintLayout) view.getParent()).g(0, trim)) != null && (g2 instanceof Integer)) {
                i2 = ((Integer) g2).intValue();
            }
            iArr[i4] = i2;
            i3++;
            i4++;
        }
        return i4 != split.length ? Arrays.copyOf(iArr, i4) : iArr;
    }

    public final C0021a u(Context context, AttributeSet attributeSet, boolean z) {
        C0021a c0021a = new C0021a();
        TypedArray obtainStyledAttributes = context.obtainStyledAttributes(attributeSet, z ? w23.ConstraintOverride : w23.Constraint);
        J(context, c0021a, obtainStyledAttributes, z);
        obtainStyledAttributes.recycle();
        return c0021a;
    }

    public final C0021a v(int i2) {
        if (!this.f.containsKey(Integer.valueOf(i2))) {
            this.f.put(Integer.valueOf(i2), new C0021a());
        }
        return this.f.get(Integer.valueOf(i2));
    }

    public C0021a w(int i2) {
        if (this.f.containsKey(Integer.valueOf(i2))) {
            return this.f.get(Integer.valueOf(i2));
        }
        return null;
    }

    public int x(int i2) {
        return v(i2).e.d;
    }

    public int[] y() {
        Integer[] numArr = (Integer[]) this.f.keySet().toArray(new Integer[0]);
        int length = numArr.length;
        int[] iArr = new int[length];
        for (int i2 = 0; i2 < length; i2++) {
            iArr[i2] = numArr[i2].intValue();
        }
        return iArr;
    }

    public C0021a z(int i2) {
        return v(i2);
    }
}
