package androidx.constraintlayout.helper.widget;

import android.content.Context;
import android.content.res.TypedArray;
import android.util.AttributeSet;
import android.view.View;
import androidx.constraintlayout.helper.widget.Carousel;
import androidx.constraintlayout.motion.widget.MotionHelper;
import androidx.constraintlayout.motion.widget.MotionLayout;
import androidx.constraintlayout.motion.widget.g;
import androidx.constraintlayout.widget.a;
import com.github.mikephil.charting.utils.Utils;
import java.util.ArrayList;

/* loaded from: classes.dex */
public class Carousel extends MotionHelper {
    public int A0;
    public int B0;
    public float C0;
    public int D0;
    public int E0;
    public int F0;
    public float G0;
    public int H0;
    public int I0;
    public Runnable J0;
    public b r0;
    public final ArrayList<View> s0;
    public int t0;
    public int u0;
    public MotionLayout v0;
    public int w0;
    public boolean x0;
    public int y0;
    public int z0;

    /* loaded from: classes.dex */
    public class a implements Runnable {

        /* renamed from: androidx.constraintlayout.helper.widget.Carousel$a$a  reason: collision with other inner class name */
        /* loaded from: classes.dex */
        public class RunnableC0020a implements Runnable {
            public final /* synthetic */ float a;

            public RunnableC0020a(float f) {
                this.a = f;
            }

            @Override // java.lang.Runnable
            public void run() {
                Carousel.this.v0.s0(5, 1.0f, this.a);
            }
        }

        public a() {
        }

        @Override // java.lang.Runnable
        public void run() {
            Carousel.this.v0.setProgress(Utils.FLOAT_EPSILON);
            Carousel.this.Q();
            Carousel.this.r0.b(Carousel.this.u0);
            float velocity = Carousel.this.v0.getVelocity();
            if (Carousel.this.F0 != 2 || velocity <= Carousel.this.G0 || Carousel.this.u0 >= Carousel.this.r0.a() - 1) {
                return;
            }
            float f = velocity * Carousel.this.C0;
            if (Carousel.this.u0 != 0 || Carousel.this.t0 <= Carousel.this.u0) {
                if (Carousel.this.u0 != Carousel.this.r0.a() - 1 || Carousel.this.t0 >= Carousel.this.u0) {
                    Carousel.this.v0.post(new RunnableC0020a(f));
                }
            }
        }
    }

    /* loaded from: classes.dex */
    public interface b {
        int a();

        void b(int i);

        void c(View view, int i);
    }

    public Carousel(Context context) {
        super(context);
        this.r0 = null;
        this.s0 = new ArrayList<>();
        this.t0 = 0;
        this.u0 = 0;
        this.w0 = -1;
        this.x0 = false;
        this.y0 = -1;
        this.z0 = -1;
        this.A0 = -1;
        this.B0 = -1;
        this.C0 = 0.9f;
        this.D0 = 0;
        this.E0 = 4;
        this.F0 = 1;
        this.G0 = 2.0f;
        this.H0 = -1;
        this.I0 = 200;
        this.J0 = new a();
    }

    /* JADX INFO: Access modifiers changed from: private */
    public /* synthetic */ void P() {
        this.v0.setTransitionDuration(this.I0);
        if (this.H0 < this.u0) {
            this.v0.x0(this.A0, this.I0);
        } else {
            this.v0.x0(this.B0, this.I0);
        }
    }

    public final boolean N(int i, boolean z) {
        MotionLayout motionLayout;
        g.b h0;
        if (i == -1 || (motionLayout = this.v0) == null || (h0 = motionLayout.h0(i)) == null || z == h0.C()) {
            return false;
        }
        h0.F(z);
        return true;
    }

    public final void O(Context context, AttributeSet attributeSet) {
        if (attributeSet != null) {
            TypedArray obtainStyledAttributes = context.obtainStyledAttributes(attributeSet, w23.Carousel);
            int indexCount = obtainStyledAttributes.getIndexCount();
            for (int i = 0; i < indexCount; i++) {
                int index = obtainStyledAttributes.getIndex(i);
                if (index == w23.Carousel_carousel_firstView) {
                    this.w0 = obtainStyledAttributes.getResourceId(index, this.w0);
                } else if (index == w23.Carousel_carousel_backwardTransition) {
                    this.y0 = obtainStyledAttributes.getResourceId(index, this.y0);
                } else if (index == w23.Carousel_carousel_forwardTransition) {
                    this.z0 = obtainStyledAttributes.getResourceId(index, this.z0);
                } else if (index == w23.Carousel_carousel_emptyViewsBehavior) {
                    this.E0 = obtainStyledAttributes.getInt(index, this.E0);
                } else if (index == w23.Carousel_carousel_previousState) {
                    this.A0 = obtainStyledAttributes.getResourceId(index, this.A0);
                } else if (index == w23.Carousel_carousel_nextState) {
                    this.B0 = obtainStyledAttributes.getResourceId(index, this.B0);
                } else if (index == w23.Carousel_carousel_touchUp_dampeningFactor) {
                    this.C0 = obtainStyledAttributes.getFloat(index, this.C0);
                } else if (index == w23.Carousel_carousel_touchUpMode) {
                    this.F0 = obtainStyledAttributes.getInt(index, this.F0);
                } else if (index == w23.Carousel_carousel_touchUp_velocityThreshold) {
                    this.G0 = obtainStyledAttributes.getFloat(index, this.G0);
                } else if (index == w23.Carousel_carousel_infinite) {
                    this.x0 = obtainStyledAttributes.getBoolean(index, this.x0);
                }
            }
            obtainStyledAttributes.recycle();
        }
    }

    public final void Q() {
        b bVar = this.r0;
        if (bVar == null || this.v0 == null || bVar.a() == 0) {
            return;
        }
        int size = this.s0.size();
        for (int i = 0; i < size; i++) {
            View view = this.s0.get(i);
            int i2 = (this.u0 + i) - this.D0;
            if (this.x0) {
                if (i2 < 0) {
                    int i3 = this.E0;
                    if (i3 != 4) {
                        S(view, i3);
                    } else {
                        S(view, 0);
                    }
                    if (i2 % this.r0.a() == 0) {
                        this.r0.c(view, 0);
                    } else {
                        b bVar2 = this.r0;
                        bVar2.c(view, bVar2.a() + (i2 % this.r0.a()));
                    }
                } else if (i2 >= this.r0.a()) {
                    if (i2 == this.r0.a()) {
                        i2 = 0;
                    } else if (i2 > this.r0.a()) {
                        i2 %= this.r0.a();
                    }
                    int i4 = this.E0;
                    if (i4 != 4) {
                        S(view, i4);
                    } else {
                        S(view, 0);
                    }
                    this.r0.c(view, i2);
                } else {
                    S(view, 0);
                    this.r0.c(view, i2);
                }
            } else if (i2 < 0) {
                S(view, this.E0);
            } else if (i2 >= this.r0.a()) {
                S(view, this.E0);
            } else {
                S(view, 0);
                this.r0.c(view, i2);
            }
        }
        int i5 = this.H0;
        if (i5 != -1 && i5 != this.u0) {
            this.v0.post(new Runnable() { // from class: fw
                @Override // java.lang.Runnable
                public final void run() {
                    Carousel.this.P();
                }
            });
        } else if (i5 == this.u0) {
            this.H0 = -1;
        }
        if (this.y0 == -1 || this.z0 == -1 || this.x0) {
            return;
        }
        int a2 = this.r0.a();
        if (this.u0 == 0) {
            N(this.y0, false);
        } else {
            N(this.y0, true);
            this.v0.setTransition(this.y0);
        }
        if (this.u0 == a2 - 1) {
            N(this.z0, false);
            return;
        }
        N(this.z0, true);
        this.v0.setTransition(this.z0);
    }

    public final boolean R(int i, View view, int i2) {
        a.C0021a w;
        androidx.constraintlayout.widget.a f0 = this.v0.f0(i);
        if (f0 == null || (w = f0.w(view.getId())) == null) {
            return false;
        }
        w.c.c = 1;
        view.setVisibility(i2);
        return true;
    }

    public final boolean S(View view, int i) {
        MotionLayout motionLayout = this.v0;
        if (motionLayout == null) {
            return false;
        }
        boolean z = false;
        for (int i2 : motionLayout.getConstraintSetIds()) {
            z |= R(i2, view, i);
        }
        return z;
    }

    @Override // androidx.constraintlayout.motion.widget.MotionHelper, androidx.constraintlayout.motion.widget.MotionLayout.j
    public void a(MotionLayout motionLayout, int i, int i2, float f) {
    }

    @Override // androidx.constraintlayout.motion.widget.MotionHelper, androidx.constraintlayout.motion.widget.MotionLayout.j
    public void d(MotionLayout motionLayout, int i) {
        int i2 = this.u0;
        this.t0 = i2;
        if (i == this.B0) {
            this.u0 = i2 + 1;
        } else if (i == this.A0) {
            this.u0 = i2 - 1;
        }
        if (this.x0) {
            if (this.u0 >= this.r0.a()) {
                this.u0 = 0;
            }
            if (this.u0 < 0) {
                this.u0 = this.r0.a() - 1;
            }
        } else {
            if (this.u0 >= this.r0.a()) {
                this.u0 = this.r0.a() - 1;
            }
            if (this.u0 < 0) {
                this.u0 = 0;
            }
        }
        if (this.t0 != this.u0) {
            this.v0.post(this.J0);
        }
    }

    public int getCount() {
        b bVar = this.r0;
        if (bVar != null) {
            return bVar.a();
        }
        return 0;
    }

    public int getCurrentIndex() {
        return this.u0;
    }

    @Override // androidx.constraintlayout.widget.ConstraintHelper, android.view.View
    public void onAttachedToWindow() {
        super.onAttachedToWindow();
        if (getParent() instanceof MotionLayout) {
            MotionLayout motionLayout = (MotionLayout) getParent();
            for (int i = 0; i < this.f0; i++) {
                int i2 = this.a[i];
                View i3 = motionLayout.i(i2);
                if (this.w0 == i2) {
                    this.D0 = i;
                }
                this.s0.add(i3);
            }
            this.v0 = motionLayout;
            if (this.F0 == 2) {
                g.b h0 = motionLayout.h0(this.z0);
                if (h0 != null) {
                    h0.H(5);
                }
                g.b h02 = this.v0.h0(this.y0);
                if (h02 != null) {
                    h02.H(5);
                }
            }
            Q();
        }
    }

    public void setAdapter(b bVar) {
        this.r0 = bVar;
    }

    public Carousel(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        this.r0 = null;
        this.s0 = new ArrayList<>();
        this.t0 = 0;
        this.u0 = 0;
        this.w0 = -1;
        this.x0 = false;
        this.y0 = -1;
        this.z0 = -1;
        this.A0 = -1;
        this.B0 = -1;
        this.C0 = 0.9f;
        this.D0 = 0;
        this.E0 = 4;
        this.F0 = 1;
        this.G0 = 2.0f;
        this.H0 = -1;
        this.I0 = 200;
        this.J0 = new a();
        O(context, attributeSet);
    }

    public Carousel(Context context, AttributeSet attributeSet, int i) {
        super(context, attributeSet, i);
        this.r0 = null;
        this.s0 = new ArrayList<>();
        this.t0 = 0;
        this.u0 = 0;
        this.w0 = -1;
        this.x0 = false;
        this.y0 = -1;
        this.z0 = -1;
        this.A0 = -1;
        this.B0 = -1;
        this.C0 = 0.9f;
        this.D0 = 0;
        this.E0 = 4;
        this.F0 = 1;
        this.G0 = 2.0f;
        this.H0 = -1;
        this.I0 = 200;
        this.J0 = new a();
        O(context, attributeSet);
    }
}
