package androidx.constraintlayout.helper.widget;

import android.content.Context;
import android.content.res.TypedArray;
import android.os.Build;
import android.util.AttributeSet;
import android.view.View;
import androidx.constraintlayout.core.widgets.ConstraintWidget;
import androidx.constraintlayout.widget.ConstraintHelper;
import androidx.constraintlayout.widget.ConstraintLayout;
import com.github.mikephil.charting.utils.Utils;

/* loaded from: classes.dex */
public class Layer extends ConstraintHelper {
    public View[] A0;
    public float B0;
    public float C0;
    public boolean D0;
    public boolean E0;
    public float n0;
    public float o0;
    public float p0;
    public ConstraintLayout q0;
    public float r0;
    public float s0;
    public float t0;
    public float u0;
    public float v0;
    public float w0;
    public float x0;
    public float y0;
    public boolean z0;

    public Layer(Context context) {
        super(context);
        this.n0 = Float.NaN;
        this.o0 = Float.NaN;
        this.p0 = Float.NaN;
        this.r0 = 1.0f;
        this.s0 = 1.0f;
        this.t0 = Float.NaN;
        this.u0 = Float.NaN;
        this.v0 = Float.NaN;
        this.w0 = Float.NaN;
        this.x0 = Float.NaN;
        this.y0 = Float.NaN;
        this.z0 = true;
        this.A0 = null;
        this.B0 = Utils.FLOAT_EPSILON;
        this.C0 = Utils.FLOAT_EPSILON;
    }

    @Override // androidx.constraintlayout.widget.ConstraintHelper
    public void j(ConstraintLayout constraintLayout) {
        i(constraintLayout);
    }

    @Override // androidx.constraintlayout.widget.ConstraintHelper
    public void o(AttributeSet attributeSet) {
        super.o(attributeSet);
        this.i0 = false;
        if (attributeSet != null) {
            TypedArray obtainStyledAttributes = getContext().obtainStyledAttributes(attributeSet, w23.ConstraintLayout_Layout);
            int indexCount = obtainStyledAttributes.getIndexCount();
            for (int i = 0; i < indexCount; i++) {
                int index = obtainStyledAttributes.getIndex(i);
                if (index == w23.ConstraintLayout_Layout_android_visibility) {
                    this.D0 = true;
                } else if (index == w23.ConstraintLayout_Layout_android_elevation) {
                    this.E0 = true;
                }
            }
            obtainStyledAttributes.recycle();
        }
    }

    @Override // androidx.constraintlayout.widget.ConstraintHelper, android.view.View
    public void onAttachedToWindow() {
        super.onAttachedToWindow();
        this.q0 = (ConstraintLayout) getParent();
        if (this.D0 || this.E0) {
            int visibility = getVisibility();
            float elevation = Build.VERSION.SDK_INT >= 21 ? getElevation() : 0.0f;
            for (int i = 0; i < this.f0; i++) {
                View i2 = this.q0.i(this.a[i]);
                if (i2 != null) {
                    if (this.D0) {
                        i2.setVisibility(visibility);
                    }
                    if (this.E0 && elevation > Utils.FLOAT_EPSILON && Build.VERSION.SDK_INT >= 21) {
                        i2.setTranslationZ(i2.getTranslationZ() + elevation);
                    }
                }
            }
        }
    }

    @Override // androidx.constraintlayout.widget.ConstraintHelper
    public void r(ConstraintLayout constraintLayout) {
        y();
        this.t0 = Float.NaN;
        this.u0 = Float.NaN;
        ConstraintWidget b = ((ConstraintLayout.LayoutParams) getLayoutParams()).b();
        b.h1(0);
        b.I0(0);
        x();
        layout(((int) this.x0) - getPaddingLeft(), ((int) this.y0) - getPaddingTop(), ((int) this.v0) + getPaddingRight(), ((int) this.w0) + getPaddingBottom());
        z();
    }

    @Override // android.view.View
    public void setElevation(float f) {
        super.setElevation(f);
        h();
    }

    @Override // android.view.View
    public void setPivotX(float f) {
        this.n0 = f;
        z();
    }

    @Override // android.view.View
    public void setPivotY(float f) {
        this.o0 = f;
        z();
    }

    @Override // android.view.View
    public void setRotation(float f) {
        this.p0 = f;
        z();
    }

    @Override // android.view.View
    public void setScaleX(float f) {
        this.r0 = f;
        z();
    }

    @Override // android.view.View
    public void setScaleY(float f) {
        this.s0 = f;
        z();
    }

    @Override // android.view.View
    public void setTranslationX(float f) {
        this.B0 = f;
        z();
    }

    @Override // android.view.View
    public void setTranslationY(float f) {
        this.C0 = f;
        z();
    }

    @Override // android.view.View
    public void setVisibility(int i) {
        super.setVisibility(i);
        h();
    }

    @Override // androidx.constraintlayout.widget.ConstraintHelper
    public void t(ConstraintLayout constraintLayout) {
        this.q0 = constraintLayout;
        float rotation = getRotation();
        if (rotation == Utils.FLOAT_EPSILON) {
            if (Float.isNaN(this.p0)) {
                return;
            }
            this.p0 = rotation;
            return;
        }
        this.p0 = rotation;
    }

    public void x() {
        if (this.q0 == null) {
            return;
        }
        if (this.z0 || Float.isNaN(this.t0) || Float.isNaN(this.u0)) {
            if (!Float.isNaN(this.n0) && !Float.isNaN(this.o0)) {
                this.u0 = this.o0;
                this.t0 = this.n0;
                return;
            }
            View[] n = n(this.q0);
            int left = n[0].getLeft();
            int top = n[0].getTop();
            int right = n[0].getRight();
            int bottom = n[0].getBottom();
            for (int i = 0; i < this.f0; i++) {
                View view = n[i];
                left = Math.min(left, view.getLeft());
                top = Math.min(top, view.getTop());
                right = Math.max(right, view.getRight());
                bottom = Math.max(bottom, view.getBottom());
            }
            this.v0 = right;
            this.w0 = bottom;
            this.x0 = left;
            this.y0 = top;
            if (Float.isNaN(this.n0)) {
                this.t0 = (left + right) / 2;
            } else {
                this.t0 = this.n0;
            }
            if (Float.isNaN(this.o0)) {
                this.u0 = (top + bottom) / 2;
            } else {
                this.u0 = this.o0;
            }
        }
    }

    public final void y() {
        int i;
        if (this.q0 == null || (i = this.f0) == 0) {
            return;
        }
        View[] viewArr = this.A0;
        if (viewArr == null || viewArr.length != i) {
            this.A0 = new View[i];
        }
        for (int i2 = 0; i2 < this.f0; i2++) {
            this.A0[i2] = this.q0.i(this.a[i2]);
        }
    }

    public final void z() {
        if (this.q0 == null) {
            return;
        }
        if (this.A0 == null) {
            y();
        }
        x();
        double radians = Float.isNaN(this.p0) ? Utils.DOUBLE_EPSILON : Math.toRadians(this.p0);
        float sin = (float) Math.sin(radians);
        float cos = (float) Math.cos(radians);
        float f = this.r0;
        float f2 = f * cos;
        float f3 = this.s0;
        float f4 = (-f3) * sin;
        float f5 = f * sin;
        float f6 = f3 * cos;
        for (int i = 0; i < this.f0; i++) {
            View view = this.A0[i];
            float left = ((view.getLeft() + view.getRight()) / 2) - this.t0;
            float top = ((view.getTop() + view.getBottom()) / 2) - this.u0;
            view.setTranslationX((((f2 * left) + (f4 * top)) - left) + this.B0);
            view.setTranslationY((((left * f5) + (f6 * top)) - top) + this.C0);
            view.setScaleY(this.s0);
            view.setScaleX(this.r0);
            if (!Float.isNaN(this.p0)) {
                view.setRotation(this.p0);
            }
        }
    }

    public Layer(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        this.n0 = Float.NaN;
        this.o0 = Float.NaN;
        this.p0 = Float.NaN;
        this.r0 = 1.0f;
        this.s0 = 1.0f;
        this.t0 = Float.NaN;
        this.u0 = Float.NaN;
        this.v0 = Float.NaN;
        this.w0 = Float.NaN;
        this.x0 = Float.NaN;
        this.y0 = Float.NaN;
        this.z0 = true;
        this.A0 = null;
        this.B0 = Utils.FLOAT_EPSILON;
        this.C0 = Utils.FLOAT_EPSILON;
    }

    public Layer(Context context, AttributeSet attributeSet, int i) {
        super(context, attributeSet, i);
        this.n0 = Float.NaN;
        this.o0 = Float.NaN;
        this.p0 = Float.NaN;
        this.r0 = 1.0f;
        this.s0 = 1.0f;
        this.t0 = Float.NaN;
        this.u0 = Float.NaN;
        this.v0 = Float.NaN;
        this.w0 = Float.NaN;
        this.x0 = Float.NaN;
        this.y0 = Float.NaN;
        this.z0 = true;
        this.A0 = null;
        this.B0 = Utils.FLOAT_EPSILON;
        this.C0 = Utils.FLOAT_EPSILON;
    }
}
