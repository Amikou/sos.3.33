package androidx.constraintlayout.helper.widget;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.res.TypedArray;
import android.os.Build;
import android.util.AttributeSet;
import android.util.SparseArray;
import android.view.View;
import androidx.constraintlayout.core.widgets.ConstraintWidget;
import androidx.constraintlayout.core.widgets.e;
import androidx.constraintlayout.core.widgets.i;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.constraintlayout.widget.VirtualLayout;
import androidx.constraintlayout.widget.a;

/* loaded from: classes.dex */
public class Flow extends VirtualLayout {
    public e p0;

    public Flow(Context context) {
        super(context);
    }

    @Override // androidx.constraintlayout.widget.VirtualLayout, androidx.constraintlayout.widget.ConstraintHelper
    public void o(AttributeSet attributeSet) {
        super.o(attributeSet);
        this.p0 = new e();
        if (attributeSet != null) {
            TypedArray obtainStyledAttributes = getContext().obtainStyledAttributes(attributeSet, w23.ConstraintLayout_Layout);
            int indexCount = obtainStyledAttributes.getIndexCount();
            for (int i = 0; i < indexCount; i++) {
                int index = obtainStyledAttributes.getIndex(i);
                if (index == w23.ConstraintLayout_Layout_android_orientation) {
                    this.p0.z2(obtainStyledAttributes.getInt(index, 0));
                } else if (index == w23.ConstraintLayout_Layout_android_padding) {
                    this.p0.F1(obtainStyledAttributes.getDimensionPixelSize(index, 0));
                } else if (index == w23.ConstraintLayout_Layout_android_paddingStart) {
                    if (Build.VERSION.SDK_INT >= 17) {
                        this.p0.K1(obtainStyledAttributes.getDimensionPixelSize(index, 0));
                    }
                } else if (index == w23.ConstraintLayout_Layout_android_paddingEnd) {
                    if (Build.VERSION.SDK_INT >= 17) {
                        this.p0.H1(obtainStyledAttributes.getDimensionPixelSize(index, 0));
                    }
                } else if (index == w23.ConstraintLayout_Layout_android_paddingLeft) {
                    this.p0.I1(obtainStyledAttributes.getDimensionPixelSize(index, 0));
                } else if (index == w23.ConstraintLayout_Layout_android_paddingTop) {
                    this.p0.L1(obtainStyledAttributes.getDimensionPixelSize(index, 0));
                } else if (index == w23.ConstraintLayout_Layout_android_paddingRight) {
                    this.p0.J1(obtainStyledAttributes.getDimensionPixelSize(index, 0));
                } else if (index == w23.ConstraintLayout_Layout_android_paddingBottom) {
                    this.p0.G1(obtainStyledAttributes.getDimensionPixelSize(index, 0));
                } else if (index == w23.ConstraintLayout_Layout_flow_wrapMode) {
                    this.p0.E2(obtainStyledAttributes.getInt(index, 0));
                } else if (index == w23.ConstraintLayout_Layout_flow_horizontalStyle) {
                    this.p0.t2(obtainStyledAttributes.getInt(index, 0));
                } else if (index == w23.ConstraintLayout_Layout_flow_verticalStyle) {
                    this.p0.D2(obtainStyledAttributes.getInt(index, 0));
                } else if (index == w23.ConstraintLayout_Layout_flow_firstHorizontalStyle) {
                    this.p0.n2(obtainStyledAttributes.getInt(index, 0));
                } else if (index == w23.ConstraintLayout_Layout_flow_lastHorizontalStyle) {
                    this.p0.v2(obtainStyledAttributes.getInt(index, 0));
                } else if (index == w23.ConstraintLayout_Layout_flow_firstVerticalStyle) {
                    this.p0.p2(obtainStyledAttributes.getInt(index, 0));
                } else if (index == w23.ConstraintLayout_Layout_flow_lastVerticalStyle) {
                    this.p0.x2(obtainStyledAttributes.getInt(index, 0));
                } else if (index == w23.ConstraintLayout_Layout_flow_horizontalBias) {
                    this.p0.r2(obtainStyledAttributes.getFloat(index, 0.5f));
                } else if (index == w23.ConstraintLayout_Layout_flow_firstHorizontalBias) {
                    this.p0.m2(obtainStyledAttributes.getFloat(index, 0.5f));
                } else if (index == w23.ConstraintLayout_Layout_flow_lastHorizontalBias) {
                    this.p0.u2(obtainStyledAttributes.getFloat(index, 0.5f));
                } else if (index == w23.ConstraintLayout_Layout_flow_firstVerticalBias) {
                    this.p0.o2(obtainStyledAttributes.getFloat(index, 0.5f));
                } else if (index == w23.ConstraintLayout_Layout_flow_lastVerticalBias) {
                    this.p0.w2(obtainStyledAttributes.getFloat(index, 0.5f));
                } else if (index == w23.ConstraintLayout_Layout_flow_verticalBias) {
                    this.p0.B2(obtainStyledAttributes.getFloat(index, 0.5f));
                } else if (index == w23.ConstraintLayout_Layout_flow_horizontalAlign) {
                    this.p0.q2(obtainStyledAttributes.getInt(index, 2));
                } else if (index == w23.ConstraintLayout_Layout_flow_verticalAlign) {
                    this.p0.A2(obtainStyledAttributes.getInt(index, 2));
                } else if (index == w23.ConstraintLayout_Layout_flow_horizontalGap) {
                    this.p0.s2(obtainStyledAttributes.getDimensionPixelSize(index, 0));
                } else if (index == w23.ConstraintLayout_Layout_flow_verticalGap) {
                    this.p0.C2(obtainStyledAttributes.getDimensionPixelSize(index, 0));
                } else if (index == w23.ConstraintLayout_Layout_flow_maxElementsWrap) {
                    this.p0.y2(obtainStyledAttributes.getInt(index, -1));
                }
            }
            obtainStyledAttributes.recycle();
        }
        this.h0 = this.p0;
        w();
    }

    @Override // androidx.constraintlayout.widget.ConstraintHelper, android.view.View
    @SuppressLint({"WrongCall"})
    public void onMeasure(int i, int i2) {
        x(this.p0, i, i2);
    }

    @Override // androidx.constraintlayout.widget.ConstraintHelper
    public void p(a.C0021a c0021a, mk1 mk1Var, ConstraintLayout.LayoutParams layoutParams, SparseArray<ConstraintWidget> sparseArray) {
        super.p(c0021a, mk1Var, layoutParams, sparseArray);
        if (mk1Var instanceof e) {
            e eVar = (e) mk1Var;
            int i = layoutParams.U;
            if (i != -1) {
                eVar.z2(i);
            }
        }
    }

    @Override // androidx.constraintlayout.widget.ConstraintHelper
    public void q(ConstraintWidget constraintWidget, boolean z) {
        this.p0.q1(z);
    }

    public void setFirstHorizontalBias(float f) {
        this.p0.m2(f);
        requestLayout();
    }

    public void setFirstHorizontalStyle(int i) {
        this.p0.n2(i);
        requestLayout();
    }

    public void setFirstVerticalBias(float f) {
        this.p0.o2(f);
        requestLayout();
    }

    public void setFirstVerticalStyle(int i) {
        this.p0.p2(i);
        requestLayout();
    }

    public void setHorizontalAlign(int i) {
        this.p0.q2(i);
        requestLayout();
    }

    public void setHorizontalBias(float f) {
        this.p0.r2(f);
        requestLayout();
    }

    public void setHorizontalGap(int i) {
        this.p0.s2(i);
        requestLayout();
    }

    public void setHorizontalStyle(int i) {
        this.p0.t2(i);
        requestLayout();
    }

    public void setLastHorizontalBias(float f) {
        this.p0.u2(f);
        requestLayout();
    }

    public void setLastHorizontalStyle(int i) {
        this.p0.v2(i);
        requestLayout();
    }

    public void setLastVerticalBias(float f) {
        this.p0.w2(f);
        requestLayout();
    }

    public void setLastVerticalStyle(int i) {
        this.p0.x2(i);
        requestLayout();
    }

    public void setMaxElementsWrap(int i) {
        this.p0.y2(i);
        requestLayout();
    }

    public void setOrientation(int i) {
        this.p0.z2(i);
        requestLayout();
    }

    public void setPadding(int i) {
        this.p0.F1(i);
        requestLayout();
    }

    public void setPaddingBottom(int i) {
        this.p0.G1(i);
        requestLayout();
    }

    public void setPaddingLeft(int i) {
        this.p0.I1(i);
        requestLayout();
    }

    public void setPaddingRight(int i) {
        this.p0.J1(i);
        requestLayout();
    }

    public void setPaddingTop(int i) {
        this.p0.L1(i);
        requestLayout();
    }

    public void setVerticalAlign(int i) {
        this.p0.A2(i);
        requestLayout();
    }

    public void setVerticalBias(float f) {
        this.p0.B2(f);
        requestLayout();
    }

    public void setVerticalGap(int i) {
        this.p0.C2(i);
        requestLayout();
    }

    public void setVerticalStyle(int i) {
        this.p0.D2(i);
        requestLayout();
    }

    public void setWrapMode(int i) {
        this.p0.E2(i);
        requestLayout();
    }

    @Override // androidx.constraintlayout.widget.VirtualLayout
    public void x(i iVar, int i, int i2) {
        int mode = View.MeasureSpec.getMode(i);
        int size = View.MeasureSpec.getSize(i);
        int mode2 = View.MeasureSpec.getMode(i2);
        int size2 = View.MeasureSpec.getSize(i2);
        if (iVar != null) {
            iVar.z1(mode, size, mode2, size2);
            setMeasuredDimension(iVar.u1(), iVar.t1());
            return;
        }
        setMeasuredDimension(0, 0);
    }

    public Flow(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
    }

    public Flow(Context context, AttributeSet attributeSet, int i) {
        super(context, attributeSet, i);
    }
}
