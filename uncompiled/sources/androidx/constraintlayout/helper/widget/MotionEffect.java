package androidx.constraintlayout.helper.widget;

import android.content.Context;
import android.content.res.TypedArray;
import android.util.AttributeSet;
import androidx.constraintlayout.motion.widget.MotionHelper;

/* loaded from: classes.dex */
public class MotionEffect extends MotionHelper {
    public float r0;
    public int s0;
    public int t0;
    public int u0;
    public int v0;
    public boolean w0;
    public int x0;
    public int y0;

    public MotionEffect(Context context) {
        super(context);
        this.r0 = 0.1f;
        this.s0 = 49;
        this.t0 = 50;
        this.u0 = 0;
        this.v0 = 0;
        this.w0 = true;
        this.x0 = -1;
        this.y0 = -1;
    }

    /* JADX WARN: Code restructure failed: missing block: B:55:0x017f, code lost:
        if (r14 == com.github.mikephil.charting.utils.Utils.FLOAT_EPSILON) goto L62;
     */
    /* JADX WARN: Code restructure failed: missing block: B:66:0x0193, code lost:
        if (r14 == com.github.mikephil.charting.utils.Utils.FLOAT_EPSILON) goto L62;
     */
    /* JADX WARN: Code restructure failed: missing block: B:75:0x01a3, code lost:
        if (r15 == com.github.mikephil.charting.utils.Utils.FLOAT_EPSILON) goto L62;
     */
    /* JADX WARN: Code restructure failed: missing block: B:84:0x01b3, code lost:
        if (r15 == com.github.mikephil.charting.utils.Utils.FLOAT_EPSILON) goto L63;
     */
    @Override // androidx.constraintlayout.motion.widget.MotionHelper
    /*
        Code decompiled incorrectly, please refer to instructions dump.
        To view partially-correct code enable 'Show inconsistent code' option in preferences
    */
    public void D(androidx.constraintlayout.motion.widget.MotionLayout r22, java.util.HashMap<android.view.View, defpackage.u92> r23) {
        /*
            Method dump skipped, instructions count: 493
            To view this dump change 'Code comments level' option to 'DEBUG'
        */
        throw new UnsupportedOperationException("Method not decompiled: androidx.constraintlayout.helper.widget.MotionEffect.D(androidx.constraintlayout.motion.widget.MotionLayout, java.util.HashMap):void");
    }

    public final void E(Context context, AttributeSet attributeSet) {
        if (attributeSet != null) {
            TypedArray obtainStyledAttributes = context.obtainStyledAttributes(attributeSet, w23.MotionEffect);
            int indexCount = obtainStyledAttributes.getIndexCount();
            for (int i = 0; i < indexCount; i++) {
                int index = obtainStyledAttributes.getIndex(i);
                if (index == w23.MotionEffect_motionEffect_start) {
                    int i2 = obtainStyledAttributes.getInt(index, this.s0);
                    this.s0 = i2;
                    this.s0 = Math.max(Math.min(i2, 99), 0);
                } else if (index == w23.MotionEffect_motionEffect_end) {
                    int i3 = obtainStyledAttributes.getInt(index, this.t0);
                    this.t0 = i3;
                    this.t0 = Math.max(Math.min(i3, 99), 0);
                } else if (index == w23.MotionEffect_motionEffect_translationX) {
                    this.u0 = obtainStyledAttributes.getDimensionPixelOffset(index, this.u0);
                } else if (index == w23.MotionEffect_motionEffect_translationY) {
                    this.v0 = obtainStyledAttributes.getDimensionPixelOffset(index, this.v0);
                } else if (index == w23.MotionEffect_motionEffect_alpha) {
                    this.r0 = obtainStyledAttributes.getFloat(index, this.r0);
                } else if (index == w23.MotionEffect_motionEffect_move) {
                    this.y0 = obtainStyledAttributes.getInt(index, this.y0);
                } else if (index == w23.MotionEffect_motionEffect_strict) {
                    this.w0 = obtainStyledAttributes.getBoolean(index, this.w0);
                } else if (index == w23.MotionEffect_motionEffect_viewTransition) {
                    this.x0 = obtainStyledAttributes.getResourceId(index, this.x0);
                }
            }
            int i4 = this.s0;
            int i5 = this.t0;
            if (i4 == i5) {
                if (i4 > 0) {
                    this.s0 = i4 - 1;
                } else {
                    this.t0 = i5 + 1;
                }
            }
            obtainStyledAttributes.recycle();
        }
    }

    @Override // androidx.constraintlayout.motion.widget.MotionHelper
    public boolean x() {
        return true;
    }

    public MotionEffect(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        this.r0 = 0.1f;
        this.s0 = 49;
        this.t0 = 50;
        this.u0 = 0;
        this.v0 = 0;
        this.w0 = true;
        this.x0 = -1;
        this.y0 = -1;
        E(context, attributeSet);
    }

    public MotionEffect(Context context, AttributeSet attributeSet, int i) {
        super(context, attributeSet, i);
        this.r0 = 0.1f;
        this.s0 = 49;
        this.t0 = 50;
        this.u0 = 0;
        this.v0 = 0;
        this.w0 = true;
        this.x0 = -1;
        this.y0 = -1;
        E(context, attributeSet);
    }
}
