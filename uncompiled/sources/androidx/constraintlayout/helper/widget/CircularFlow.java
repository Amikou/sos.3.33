package androidx.constraintlayout.helper.widget;

import android.content.Context;
import android.content.res.TypedArray;
import android.util.AttributeSet;
import android.view.View;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.constraintlayout.widget.VirtualLayout;
import java.util.Arrays;

/* loaded from: classes.dex */
public class CircularFlow extends VirtualLayout {
    public static float A0;
    public static int z0;
    public ConstraintLayout p0;
    public int q0;
    public float[] r0;
    public int[] s0;
    public int t0;
    public int u0;
    public String v0;
    public String w0;
    public Float x0;
    public Integer y0;

    public CircularFlow(Context context) {
        super(context);
    }

    private void setAngles(String str) {
        if (str == null) {
            return;
        }
        int i = 0;
        this.u0 = 0;
        while (true) {
            int indexOf = str.indexOf(44, i);
            if (indexOf == -1) {
                y(str.substring(i).trim());
                return;
            } else {
                y(str.substring(i, indexOf).trim());
                i = indexOf + 1;
            }
        }
    }

    private void setRadius(String str) {
        if (str == null) {
            return;
        }
        int i = 0;
        this.t0 = 0;
        while (true) {
            int indexOf = str.indexOf(44, i);
            if (indexOf == -1) {
                z(str.substring(i).trim());
                return;
            } else {
                z(str.substring(i, indexOf).trim());
                i = indexOf + 1;
            }
        }
    }

    public final void A() {
        this.p0 = (ConstraintLayout) getParent();
        for (int i = 0; i < this.f0; i++) {
            View i2 = this.p0.i(this.a[i]);
            if (i2 != null) {
                int i3 = z0;
                float f = A0;
                int[] iArr = this.s0;
                if (iArr != null && i < iArr.length) {
                    i3 = iArr[i];
                } else {
                    Integer num = this.y0;
                    if (num != null && num.intValue() != -1) {
                        this.t0++;
                        if (this.s0 == null) {
                            this.s0 = new int[1];
                        }
                        int[] radius = getRadius();
                        this.s0 = radius;
                        radius[this.t0 - 1] = i3;
                    } else {
                        StringBuilder sb = new StringBuilder();
                        sb.append("Added radius to view with id: ");
                        sb.append(this.m0.get(Integer.valueOf(i2.getId())));
                    }
                }
                float[] fArr = this.r0;
                if (fArr != null && i < fArr.length) {
                    f = fArr[i];
                } else {
                    Float f2 = this.x0;
                    if (f2 != null && f2.floatValue() != -1.0f) {
                        this.u0++;
                        if (this.r0 == null) {
                            this.r0 = new float[1];
                        }
                        float[] angles = getAngles();
                        this.r0 = angles;
                        angles[this.u0 - 1] = f;
                    } else {
                        StringBuilder sb2 = new StringBuilder();
                        sb2.append("Added angle to view with id: ");
                        sb2.append(this.m0.get(Integer.valueOf(i2.getId())));
                    }
                }
                ConstraintLayout.LayoutParams layoutParams = (ConstraintLayout.LayoutParams) i2.getLayoutParams();
                layoutParams.q = f;
                layoutParams.o = this.q0;
                layoutParams.p = i3;
                i2.setLayoutParams(layoutParams);
            }
        }
        h();
    }

    public float[] getAngles() {
        return Arrays.copyOf(this.r0, this.u0);
    }

    public int[] getRadius() {
        return Arrays.copyOf(this.s0, this.t0);
    }

    @Override // androidx.constraintlayout.widget.VirtualLayout, androidx.constraintlayout.widget.ConstraintHelper
    public void o(AttributeSet attributeSet) {
        super.o(attributeSet);
        if (attributeSet != null) {
            TypedArray obtainStyledAttributes = getContext().obtainStyledAttributes(attributeSet, w23.ConstraintLayout_Layout);
            int indexCount = obtainStyledAttributes.getIndexCount();
            for (int i = 0; i < indexCount; i++) {
                int index = obtainStyledAttributes.getIndex(i);
                if (index == w23.ConstraintLayout_Layout_circularflow_viewCenter) {
                    this.q0 = obtainStyledAttributes.getResourceId(index, 0);
                } else if (index == w23.ConstraintLayout_Layout_circularflow_angles) {
                    String string = obtainStyledAttributes.getString(index);
                    this.v0 = string;
                    setAngles(string);
                } else if (index == w23.ConstraintLayout_Layout_circularflow_radiusInDP) {
                    String string2 = obtainStyledAttributes.getString(index);
                    this.w0 = string2;
                    setRadius(string2);
                } else if (index == w23.ConstraintLayout_Layout_circularflow_defaultAngle) {
                    Float valueOf = Float.valueOf(obtainStyledAttributes.getFloat(index, A0));
                    this.x0 = valueOf;
                    setDefaultAngle(valueOf.floatValue());
                } else if (index == w23.ConstraintLayout_Layout_circularflow_defaultRadius) {
                    Integer valueOf2 = Integer.valueOf(obtainStyledAttributes.getDimensionPixelSize(index, z0));
                    this.y0 = valueOf2;
                    setDefaultRadius(valueOf2.intValue());
                }
            }
            obtainStyledAttributes.recycle();
        }
    }

    @Override // androidx.constraintlayout.widget.VirtualLayout, androidx.constraintlayout.widget.ConstraintHelper, android.view.View
    public void onAttachedToWindow() {
        super.onAttachedToWindow();
        String str = this.v0;
        if (str != null) {
            this.r0 = new float[1];
            setAngles(str);
        }
        String str2 = this.w0;
        if (str2 != null) {
            this.s0 = new int[1];
            setRadius(str2);
        }
        Float f = this.x0;
        if (f != null) {
            setDefaultAngle(f.floatValue());
        }
        Integer num = this.y0;
        if (num != null) {
            setDefaultRadius(num.intValue());
        }
        A();
    }

    public void setDefaultAngle(float f) {
        A0 = f;
    }

    public void setDefaultRadius(int i) {
        z0 = i;
    }

    public final void y(String str) {
        float[] fArr;
        if (str == null || str.length() == 0 || this.g0 == null || (fArr = this.r0) == null) {
            return;
        }
        if (this.u0 + 1 > fArr.length) {
            this.r0 = Arrays.copyOf(fArr, fArr.length + 1);
        }
        this.r0[this.u0] = Integer.parseInt(str);
        this.u0++;
    }

    public final void z(String str) {
        int[] iArr;
        if (str == null || str.length() == 0 || this.g0 == null || (iArr = this.s0) == null) {
            return;
        }
        if (this.t0 + 1 > iArr.length) {
            this.s0 = Arrays.copyOf(iArr, iArr.length + 1);
        }
        this.s0[this.t0] = (int) (Integer.parseInt(str) * this.g0.getResources().getDisplayMetrics().density);
        this.t0++;
    }

    public CircularFlow(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
    }

    public CircularFlow(Context context, AttributeSet attributeSet, int i) {
        super(context, attributeSet, i);
    }
}
