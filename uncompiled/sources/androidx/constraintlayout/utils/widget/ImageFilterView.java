package androidx.constraintlayout.utils.widget;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Canvas;
import android.graphics.ColorMatrix;
import android.graphics.ColorMatrixColorFilter;
import android.graphics.Matrix;
import android.graphics.Outline;
import android.graphics.Path;
import android.graphics.RectF;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.LayerDrawable;
import android.os.Build;
import android.util.AttributeSet;
import android.view.View;
import android.view.ViewOutlineProvider;
import android.widget.ImageView;
import androidx.appcompat.widget.AppCompatImageView;
import com.github.mikephil.charting.utils.Utils;

/* loaded from: classes.dex */
public class ImageFilterView extends AppCompatImageView {
    public c a;
    public boolean f0;
    public Drawable g0;
    public Drawable h0;
    public float i0;
    public float j0;
    public float k0;
    public Path l0;
    public ViewOutlineProvider m0;
    public RectF n0;
    public Drawable[] o0;
    public LayerDrawable p0;
    public float q0;
    public float r0;
    public float s0;
    public float t0;

    /* loaded from: classes.dex */
    public class a extends ViewOutlineProvider {
        public a() {
        }

        @Override // android.view.ViewOutlineProvider
        public void getOutline(View view, Outline outline) {
            int width = ImageFilterView.this.getWidth();
            int height = ImageFilterView.this.getHeight();
            outline.setRoundRect(0, 0, width, height, (Math.min(width, height) * ImageFilterView.this.j0) / 2.0f);
        }
    }

    /* loaded from: classes.dex */
    public class b extends ViewOutlineProvider {
        public b() {
        }

        @Override // android.view.ViewOutlineProvider
        public void getOutline(View view, Outline outline) {
            outline.setRoundRect(0, 0, ImageFilterView.this.getWidth(), ImageFilterView.this.getHeight(), ImageFilterView.this.k0);
        }
    }

    /* loaded from: classes.dex */
    public static class c {
        public float[] a = new float[20];
        public ColorMatrix b = new ColorMatrix();
        public ColorMatrix c = new ColorMatrix();
        public float d = 1.0f;
        public float e = 1.0f;
        public float f = 1.0f;
        public float g = 1.0f;

        public final void a(float f) {
            float[] fArr = this.a;
            fArr[0] = f;
            fArr[1] = 0.0f;
            fArr[2] = 0.0f;
            fArr[3] = 0.0f;
            fArr[4] = 0.0f;
            fArr[5] = 0.0f;
            fArr[6] = f;
            fArr[7] = 0.0f;
            fArr[8] = 0.0f;
            fArr[9] = 0.0f;
            fArr[10] = 0.0f;
            fArr[11] = 0.0f;
            fArr[12] = f;
            fArr[13] = 0.0f;
            fArr[14] = 0.0f;
            fArr[15] = 0.0f;
            fArr[16] = 0.0f;
            fArr[17] = 0.0f;
            fArr[18] = 1.0f;
            fArr[19] = 0.0f;
        }

        public final void b(float f) {
            float f2 = 1.0f - f;
            float f3 = 0.2999f * f2;
            float f4 = 0.587f * f2;
            float f5 = f2 * 0.114f;
            float[] fArr = this.a;
            fArr[0] = f3 + f;
            fArr[1] = f4;
            fArr[2] = f5;
            fArr[3] = 0.0f;
            fArr[4] = 0.0f;
            fArr[5] = f3;
            fArr[6] = f4 + f;
            fArr[7] = f5;
            fArr[8] = 0.0f;
            fArr[9] = 0.0f;
            fArr[10] = f3;
            fArr[11] = f4;
            fArr[12] = f5 + f;
            fArr[13] = 0.0f;
            fArr[14] = 0.0f;
            fArr[15] = 0.0f;
            fArr[16] = 0.0f;
            fArr[17] = 0.0f;
            fArr[18] = 1.0f;
            fArr[19] = 0.0f;
        }

        public void c(ImageView imageView) {
            boolean z;
            this.b.reset();
            float f = this.e;
            boolean z2 = true;
            if (f != 1.0f) {
                b(f);
                this.b.set(this.a);
                z = true;
            } else {
                z = false;
            }
            float f2 = this.f;
            if (f2 != 1.0f) {
                this.c.setScale(f2, f2, f2, 1.0f);
                this.b.postConcat(this.c);
                z = true;
            }
            float f3 = this.g;
            if (f3 != 1.0f) {
                d(f3);
                this.c.set(this.a);
                this.b.postConcat(this.c);
                z = true;
            }
            float f4 = this.d;
            if (f4 != 1.0f) {
                a(f4);
                this.c.set(this.a);
                this.b.postConcat(this.c);
            } else {
                z2 = z;
            }
            if (z2) {
                imageView.setColorFilter(new ColorMatrixColorFilter(this.b));
            } else {
                imageView.clearColorFilter();
            }
        }

        public final void d(float f) {
            float log;
            float f2;
            float f3;
            if (f <= Utils.FLOAT_EPSILON) {
                f = 0.01f;
            }
            float f4 = (5000.0f / f) / 100.0f;
            if (f4 > 66.0f) {
                double d = f4 - 60.0f;
                f2 = ((float) Math.pow(d, -0.13320475816726685d)) * 329.69873f;
                log = ((float) Math.pow(d, 0.07551484555006027d)) * 288.12216f;
            } else {
                log = (((float) Math.log(f4)) * 99.4708f) - 161.11957f;
                f2 = 255.0f;
            }
            if (f4 < 66.0f) {
                f3 = f4 > 19.0f ? (((float) Math.log(f4 - 10.0f)) * 138.51773f) - 305.0448f : 0.0f;
            } else {
                f3 = 255.0f;
            }
            float min = Math.min(255.0f, Math.max(f2, (float) Utils.FLOAT_EPSILON));
            float min2 = Math.min(255.0f, Math.max(log, (float) Utils.FLOAT_EPSILON));
            float min3 = Math.min(255.0f, Math.max(f3, (float) Utils.FLOAT_EPSILON));
            float min4 = Math.min(255.0f, Math.max(255.0f, (float) Utils.FLOAT_EPSILON));
            float min5 = Math.min(255.0f, Math.max((((float) Math.log(50.0f)) * 99.4708f) - 161.11957f, (float) Utils.FLOAT_EPSILON));
            float min6 = min3 / Math.min(255.0f, Math.max((((float) Math.log(40.0f)) * 138.51773f) - 305.0448f, (float) Utils.FLOAT_EPSILON));
            float[] fArr = this.a;
            fArr[0] = min / min4;
            fArr[1] = 0.0f;
            fArr[2] = 0.0f;
            fArr[3] = 0.0f;
            fArr[4] = 0.0f;
            fArr[5] = 0.0f;
            fArr[6] = min2 / min5;
            fArr[7] = 0.0f;
            fArr[8] = 0.0f;
            fArr[9] = 0.0f;
            fArr[10] = 0.0f;
            fArr[11] = 0.0f;
            fArr[12] = min6;
            fArr[13] = 0.0f;
            fArr[14] = 0.0f;
            fArr[15] = 0.0f;
            fArr[16] = 0.0f;
            fArr[17] = 0.0f;
            fArr[18] = 1.0f;
            fArr[19] = 0.0f;
        }
    }

    public ImageFilterView(Context context) {
        super(context);
        this.a = new c();
        this.f0 = true;
        this.g0 = null;
        this.h0 = null;
        this.i0 = Utils.FLOAT_EPSILON;
        this.j0 = Utils.FLOAT_EPSILON;
        this.k0 = Float.NaN;
        this.o0 = new Drawable[2];
        this.q0 = Float.NaN;
        this.r0 = Float.NaN;
        this.s0 = Float.NaN;
        this.t0 = Float.NaN;
        e(context, null);
    }

    private void setOverlay(boolean z) {
        this.f0 = z;
    }

    @Override // android.view.View
    public void draw(Canvas canvas) {
        boolean z;
        if (Build.VERSION.SDK_INT >= 21 || this.j0 == Utils.FLOAT_EPSILON || this.l0 == null) {
            z = false;
        } else {
            z = true;
            canvas.save();
            canvas.clipPath(this.l0);
        }
        super.draw(canvas);
        if (z) {
            canvas.restore();
        }
    }

    public final void e(Context context, AttributeSet attributeSet) {
        if (attributeSet != null) {
            TypedArray obtainStyledAttributes = getContext().obtainStyledAttributes(attributeSet, w23.ImageFilterView);
            int indexCount = obtainStyledAttributes.getIndexCount();
            this.g0 = obtainStyledAttributes.getDrawable(w23.ImageFilterView_altSrc);
            for (int i = 0; i < indexCount; i++) {
                int index = obtainStyledAttributes.getIndex(i);
                if (index == w23.ImageFilterView_crossfade) {
                    this.i0 = obtainStyledAttributes.getFloat(index, Utils.FLOAT_EPSILON);
                } else if (index == w23.ImageFilterView_warmth) {
                    setWarmth(obtainStyledAttributes.getFloat(index, Utils.FLOAT_EPSILON));
                } else if (index == w23.ImageFilterView_saturation) {
                    setSaturation(obtainStyledAttributes.getFloat(index, Utils.FLOAT_EPSILON));
                } else if (index == w23.ImageFilterView_contrast) {
                    setContrast(obtainStyledAttributes.getFloat(index, Utils.FLOAT_EPSILON));
                } else if (index == w23.ImageFilterView_brightness) {
                    setBrightness(obtainStyledAttributes.getFloat(index, Utils.FLOAT_EPSILON));
                } else if (index == w23.ImageFilterView_round) {
                    if (Build.VERSION.SDK_INT >= 21) {
                        setRound(obtainStyledAttributes.getDimension(index, Utils.FLOAT_EPSILON));
                    }
                } else if (index == w23.ImageFilterView_roundPercent) {
                    if (Build.VERSION.SDK_INT >= 21) {
                        setRoundPercent(obtainStyledAttributes.getFloat(index, Utils.FLOAT_EPSILON));
                    }
                } else if (index == w23.ImageFilterView_overlay) {
                    setOverlay(obtainStyledAttributes.getBoolean(index, this.f0));
                } else if (index == w23.ImageFilterView_imagePanX) {
                    setImagePanX(obtainStyledAttributes.getFloat(index, this.q0));
                } else if (index == w23.ImageFilterView_imagePanY) {
                    setImagePanY(obtainStyledAttributes.getFloat(index, this.r0));
                } else if (index == w23.ImageFilterView_imageRotate) {
                    setImageRotate(obtainStyledAttributes.getFloat(index, this.t0));
                } else if (index == w23.ImageFilterView_imageZoom) {
                    setImageZoom(obtainStyledAttributes.getFloat(index, this.s0));
                }
            }
            obtainStyledAttributes.recycle();
            Drawable drawable = getDrawable();
            this.h0 = drawable;
            if (this.g0 != null && drawable != null) {
                Drawable[] drawableArr = this.o0;
                Drawable mutate = getDrawable().mutate();
                this.h0 = mutate;
                drawableArr[0] = mutate;
                this.o0[1] = this.g0.mutate();
                LayerDrawable layerDrawable = new LayerDrawable(this.o0);
                this.p0 = layerDrawable;
                layerDrawable.getDrawable(1).setAlpha((int) (this.i0 * 255.0f));
                if (!this.f0) {
                    this.p0.getDrawable(0).setAlpha((int) ((1.0f - this.i0) * 255.0f));
                }
                super.setImageDrawable(this.p0);
                return;
            }
            Drawable drawable2 = getDrawable();
            this.h0 = drawable2;
            if (drawable2 != null) {
                Drawable[] drawableArr2 = this.o0;
                Drawable mutate2 = drawable2.mutate();
                this.h0 = mutate2;
                drawableArr2[0] = mutate2;
            }
        }
    }

    public final void f() {
        if (Float.isNaN(this.q0) && Float.isNaN(this.r0) && Float.isNaN(this.s0) && Float.isNaN(this.t0)) {
            return;
        }
        boolean isNaN = Float.isNaN(this.q0);
        float f = Utils.FLOAT_EPSILON;
        float f2 = isNaN ? 0.0f : this.q0;
        float f3 = Float.isNaN(this.r0) ? 0.0f : this.r0;
        float f4 = Float.isNaN(this.s0) ? 1.0f : this.s0;
        if (!Float.isNaN(this.t0)) {
            f = this.t0;
        }
        Matrix matrix = new Matrix();
        matrix.reset();
        float intrinsicWidth = getDrawable().getIntrinsicWidth();
        float intrinsicHeight = getDrawable().getIntrinsicHeight();
        float width = getWidth();
        float height = getHeight();
        float f5 = f4 * (intrinsicWidth * height < intrinsicHeight * width ? width / intrinsicWidth : height / intrinsicHeight);
        matrix.postScale(f5, f5);
        float f6 = intrinsicWidth * f5;
        float f7 = f5 * intrinsicHeight;
        matrix.postTranslate((((f2 * (width - f6)) + width) - f6) * 0.5f, (((f3 * (height - f7)) + height) - f7) * 0.5f);
        matrix.postRotate(f, width / 2.0f, height / 2.0f);
        setImageMatrix(matrix);
        setScaleType(ImageView.ScaleType.MATRIX);
    }

    public final void g() {
        if (Float.isNaN(this.q0) && Float.isNaN(this.r0) && Float.isNaN(this.s0) && Float.isNaN(this.t0)) {
            setScaleType(ImageView.ScaleType.FIT_CENTER);
        } else {
            f();
        }
    }

    public float getBrightness() {
        return this.a.d;
    }

    public float getContrast() {
        return this.a.f;
    }

    public float getCrossfade() {
        return this.i0;
    }

    public float getImagePanX() {
        return this.q0;
    }

    public float getImagePanY() {
        return this.r0;
    }

    public float getImageRotate() {
        return this.t0;
    }

    public float getImageZoom() {
        return this.s0;
    }

    public float getRound() {
        return this.k0;
    }

    public float getRoundPercent() {
        return this.j0;
    }

    public float getSaturation() {
        return this.a.e;
    }

    public float getWarmth() {
        return this.a.g;
    }

    @Override // android.view.View
    public void layout(int i, int i2, int i3, int i4) {
        super.layout(i, i2, i3, i4);
        f();
    }

    public void setAltImageResource(int i) {
        Drawable mutate = mf.d(getContext(), i).mutate();
        this.g0 = mutate;
        Drawable[] drawableArr = this.o0;
        drawableArr[0] = this.h0;
        drawableArr[1] = mutate;
        LayerDrawable layerDrawable = new LayerDrawable(this.o0);
        this.p0 = layerDrawable;
        super.setImageDrawable(layerDrawable);
        setCrossfade(this.i0);
    }

    public void setBrightness(float f) {
        c cVar = this.a;
        cVar.d = f;
        cVar.c(this);
    }

    public void setContrast(float f) {
        c cVar = this.a;
        cVar.f = f;
        cVar.c(this);
    }

    public void setCrossfade(float f) {
        this.i0 = f;
        if (this.o0 != null) {
            if (!this.f0) {
                this.p0.getDrawable(0).setAlpha((int) ((1.0f - this.i0) * 255.0f));
            }
            this.p0.getDrawable(1).setAlpha((int) (this.i0 * 255.0f));
            super.setImageDrawable(this.p0);
        }
    }

    @Override // androidx.appcompat.widget.AppCompatImageView, android.widget.ImageView
    public void setImageDrawable(Drawable drawable) {
        if (this.g0 != null && drawable != null) {
            Drawable mutate = drawable.mutate();
            this.h0 = mutate;
            Drawable[] drawableArr = this.o0;
            drawableArr[0] = mutate;
            drawableArr[1] = this.g0;
            LayerDrawable layerDrawable = new LayerDrawable(this.o0);
            this.p0 = layerDrawable;
            super.setImageDrawable(layerDrawable);
            setCrossfade(this.i0);
            return;
        }
        super.setImageDrawable(drawable);
    }

    public void setImagePanX(float f) {
        this.q0 = f;
        g();
    }

    public void setImagePanY(float f) {
        this.r0 = f;
        g();
    }

    @Override // androidx.appcompat.widget.AppCompatImageView, android.widget.ImageView
    public void setImageResource(int i) {
        if (this.g0 != null) {
            Drawable mutate = mf.d(getContext(), i).mutate();
            this.h0 = mutate;
            Drawable[] drawableArr = this.o0;
            drawableArr[0] = mutate;
            drawableArr[1] = this.g0;
            LayerDrawable layerDrawable = new LayerDrawable(this.o0);
            this.p0 = layerDrawable;
            super.setImageDrawable(layerDrawable);
            setCrossfade(this.i0);
            return;
        }
        super.setImageResource(i);
    }

    public void setImageRotate(float f) {
        this.t0 = f;
        g();
    }

    public void setImageZoom(float f) {
        this.s0 = f;
        g();
    }

    public void setRound(float f) {
        if (Float.isNaN(f)) {
            this.k0 = f;
            float f2 = this.j0;
            this.j0 = -1.0f;
            setRoundPercent(f2);
            return;
        }
        boolean z = this.k0 != f;
        this.k0 = f;
        if (f != Utils.FLOAT_EPSILON) {
            if (this.l0 == null) {
                this.l0 = new Path();
            }
            if (this.n0 == null) {
                this.n0 = new RectF();
            }
            if (Build.VERSION.SDK_INT >= 21) {
                if (this.m0 == null) {
                    b bVar = new b();
                    this.m0 = bVar;
                    setOutlineProvider(bVar);
                }
                setClipToOutline(true);
            }
            this.n0.set(Utils.FLOAT_EPSILON, Utils.FLOAT_EPSILON, getWidth(), getHeight());
            this.l0.reset();
            Path path = this.l0;
            RectF rectF = this.n0;
            float f3 = this.k0;
            path.addRoundRect(rectF, f3, f3, Path.Direction.CW);
        } else if (Build.VERSION.SDK_INT >= 21) {
            setClipToOutline(false);
        }
        if (!z || Build.VERSION.SDK_INT < 21) {
            return;
        }
        invalidateOutline();
    }

    public void setRoundPercent(float f) {
        boolean z = this.j0 != f;
        this.j0 = f;
        if (f != Utils.FLOAT_EPSILON) {
            if (this.l0 == null) {
                this.l0 = new Path();
            }
            if (this.n0 == null) {
                this.n0 = new RectF();
            }
            if (Build.VERSION.SDK_INT >= 21) {
                if (this.m0 == null) {
                    a aVar = new a();
                    this.m0 = aVar;
                    setOutlineProvider(aVar);
                }
                setClipToOutline(true);
            }
            int width = getWidth();
            int height = getHeight();
            float min = (Math.min(width, height) * this.j0) / 2.0f;
            this.n0.set(Utils.FLOAT_EPSILON, Utils.FLOAT_EPSILON, width, height);
            this.l0.reset();
            this.l0.addRoundRect(this.n0, min, min, Path.Direction.CW);
        } else if (Build.VERSION.SDK_INT >= 21) {
            setClipToOutline(false);
        }
        if (!z || Build.VERSION.SDK_INT < 21) {
            return;
        }
        invalidateOutline();
    }

    public void setSaturation(float f) {
        c cVar = this.a;
        cVar.e = f;
        cVar.c(this);
    }

    public void setWarmth(float f) {
        c cVar = this.a;
        cVar.g = f;
        cVar.c(this);
    }

    public ImageFilterView(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        this.a = new c();
        this.f0 = true;
        this.g0 = null;
        this.h0 = null;
        this.i0 = Utils.FLOAT_EPSILON;
        this.j0 = Utils.FLOAT_EPSILON;
        this.k0 = Float.NaN;
        this.o0 = new Drawable[2];
        this.q0 = Float.NaN;
        this.r0 = Float.NaN;
        this.s0 = Float.NaN;
        this.t0 = Float.NaN;
        e(context, attributeSet);
    }

    public ImageFilterView(Context context, AttributeSet attributeSet, int i) {
        super(context, attributeSet, i);
        this.a = new c();
        this.f0 = true;
        this.g0 = null;
        this.h0 = null;
        this.i0 = Utils.FLOAT_EPSILON;
        this.j0 = Utils.FLOAT_EPSILON;
        this.k0 = Float.NaN;
        this.o0 = new Drawable[2];
        this.q0 = Float.NaN;
        this.r0 = Float.NaN;
        this.s0 = Float.NaN;
        this.t0 = Float.NaN;
        e(context, attributeSet);
    }
}
