package androidx.constraintlayout.utils.widget;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Canvas;
import android.graphics.Matrix;
import android.graphics.Outline;
import android.graphics.Path;
import android.graphics.RectF;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.LayerDrawable;
import android.os.Build;
import android.util.AttributeSet;
import android.view.View;
import android.view.ViewOutlineProvider;
import android.widget.ImageView;
import androidx.appcompat.widget.AppCompatImageButton;
import androidx.constraintlayout.utils.widget.ImageFilterView;
import com.github.mikephil.charting.utils.Utils;

/* loaded from: classes.dex */
public class ImageFilterButton extends AppCompatImageButton {
    public ImageFilterView.c g0;
    public float h0;
    public float i0;
    public float j0;
    public Path k0;
    public ViewOutlineProvider l0;
    public RectF m0;
    public Drawable[] n0;
    public LayerDrawable o0;
    public boolean p0;
    public Drawable q0;
    public Drawable r0;
    public float s0;
    public float t0;
    public float u0;
    public float v0;

    /* loaded from: classes.dex */
    public class a extends ViewOutlineProvider {
        public a() {
        }

        @Override // android.view.ViewOutlineProvider
        public void getOutline(View view, Outline outline) {
            int width = ImageFilterButton.this.getWidth();
            int height = ImageFilterButton.this.getHeight();
            outline.setRoundRect(0, 0, width, height, (Math.min(width, height) * ImageFilterButton.this.i0) / 2.0f);
        }
    }

    /* loaded from: classes.dex */
    public class b extends ViewOutlineProvider {
        public b() {
        }

        @Override // android.view.ViewOutlineProvider
        public void getOutline(View view, Outline outline) {
            outline.setRoundRect(0, 0, ImageFilterButton.this.getWidth(), ImageFilterButton.this.getHeight(), ImageFilterButton.this.j0);
        }
    }

    public ImageFilterButton(Context context) {
        super(context);
        this.g0 = new ImageFilterView.c();
        this.h0 = Utils.FLOAT_EPSILON;
        this.i0 = Utils.FLOAT_EPSILON;
        this.j0 = Float.NaN;
        this.n0 = new Drawable[2];
        this.p0 = true;
        this.q0 = null;
        this.r0 = null;
        this.s0 = Float.NaN;
        this.t0 = Float.NaN;
        this.u0 = Float.NaN;
        this.v0 = Float.NaN;
        c(context, null);
    }

    private void setOverlay(boolean z) {
        this.p0 = z;
    }

    public final void c(Context context, AttributeSet attributeSet) {
        setPadding(0, 0, 0, 0);
        if (attributeSet != null) {
            TypedArray obtainStyledAttributes = getContext().obtainStyledAttributes(attributeSet, w23.ImageFilterView);
            int indexCount = obtainStyledAttributes.getIndexCount();
            this.q0 = obtainStyledAttributes.getDrawable(w23.ImageFilterView_altSrc);
            for (int i = 0; i < indexCount; i++) {
                int index = obtainStyledAttributes.getIndex(i);
                if (index == w23.ImageFilterView_crossfade) {
                    this.h0 = obtainStyledAttributes.getFloat(index, Utils.FLOAT_EPSILON);
                } else if (index == w23.ImageFilterView_warmth) {
                    setWarmth(obtainStyledAttributes.getFloat(index, Utils.FLOAT_EPSILON));
                } else if (index == w23.ImageFilterView_saturation) {
                    setSaturation(obtainStyledAttributes.getFloat(index, Utils.FLOAT_EPSILON));
                } else if (index == w23.ImageFilterView_contrast) {
                    setContrast(obtainStyledAttributes.getFloat(index, Utils.FLOAT_EPSILON));
                } else if (index == w23.ImageFilterView_round) {
                    if (Build.VERSION.SDK_INT >= 21) {
                        setRound(obtainStyledAttributes.getDimension(index, Utils.FLOAT_EPSILON));
                    }
                } else if (index == w23.ImageFilterView_roundPercent) {
                    if (Build.VERSION.SDK_INT >= 21) {
                        setRoundPercent(obtainStyledAttributes.getFloat(index, Utils.FLOAT_EPSILON));
                    }
                } else if (index == w23.ImageFilterView_overlay) {
                    setOverlay(obtainStyledAttributes.getBoolean(index, this.p0));
                } else if (index == w23.ImageFilterView_imagePanX) {
                    setImagePanX(obtainStyledAttributes.getFloat(index, this.s0));
                } else if (index == w23.ImageFilterView_imagePanY) {
                    setImagePanY(obtainStyledAttributes.getFloat(index, this.t0));
                } else if (index == w23.ImageFilterView_imageRotate) {
                    setImageRotate(obtainStyledAttributes.getFloat(index, this.v0));
                } else if (index == w23.ImageFilterView_imageZoom) {
                    setImageZoom(obtainStyledAttributes.getFloat(index, this.u0));
                }
            }
            obtainStyledAttributes.recycle();
            Drawable drawable = getDrawable();
            this.r0 = drawable;
            if (this.q0 != null && drawable != null) {
                Drawable[] drawableArr = this.n0;
                Drawable mutate = getDrawable().mutate();
                this.r0 = mutate;
                drawableArr[0] = mutate;
                this.n0[1] = this.q0.mutate();
                LayerDrawable layerDrawable = new LayerDrawable(this.n0);
                this.o0 = layerDrawable;
                layerDrawable.getDrawable(1).setAlpha((int) (this.h0 * 255.0f));
                if (!this.p0) {
                    this.o0.getDrawable(0).setAlpha((int) ((1.0f - this.h0) * 255.0f));
                }
                super.setImageDrawable(this.o0);
                return;
            }
            Drawable drawable2 = getDrawable();
            this.r0 = drawable2;
            if (drawable2 != null) {
                Drawable[] drawableArr2 = this.n0;
                Drawable mutate2 = drawable2.mutate();
                this.r0 = mutate2;
                drawableArr2[0] = mutate2;
            }
        }
    }

    public final void d() {
        if (Float.isNaN(this.s0) && Float.isNaN(this.t0) && Float.isNaN(this.u0) && Float.isNaN(this.v0)) {
            return;
        }
        boolean isNaN = Float.isNaN(this.s0);
        float f = Utils.FLOAT_EPSILON;
        float f2 = isNaN ? 0.0f : this.s0;
        float f3 = Float.isNaN(this.t0) ? 0.0f : this.t0;
        float f4 = Float.isNaN(this.u0) ? 1.0f : this.u0;
        if (!Float.isNaN(this.v0)) {
            f = this.v0;
        }
        Matrix matrix = new Matrix();
        matrix.reset();
        float intrinsicWidth = getDrawable().getIntrinsicWidth();
        float intrinsicHeight = getDrawable().getIntrinsicHeight();
        float width = getWidth();
        float height = getHeight();
        float f5 = f4 * (intrinsicWidth * height < intrinsicHeight * width ? width / intrinsicWidth : height / intrinsicHeight);
        matrix.postScale(f5, f5);
        float f6 = intrinsicWidth * f5;
        float f7 = f5 * intrinsicHeight;
        matrix.postTranslate((((f2 * (width - f6)) + width) - f6) * 0.5f, (((f3 * (height - f7)) + height) - f7) * 0.5f);
        matrix.postRotate(f, width / 2.0f, height / 2.0f);
        setImageMatrix(matrix);
        setScaleType(ImageView.ScaleType.MATRIX);
    }

    @Override // android.view.View
    public void draw(Canvas canvas) {
        boolean z;
        if (Build.VERSION.SDK_INT >= 21 || this.j0 == Utils.FLOAT_EPSILON || this.k0 == null) {
            z = false;
        } else {
            z = true;
            canvas.save();
            canvas.clipPath(this.k0);
        }
        super.draw(canvas);
        if (z) {
            canvas.restore();
        }
    }

    public final void e() {
        if (Float.isNaN(this.s0) && Float.isNaN(this.t0) && Float.isNaN(this.u0) && Float.isNaN(this.v0)) {
            setScaleType(ImageView.ScaleType.FIT_CENTER);
        } else {
            d();
        }
    }

    public float getContrast() {
        return this.g0.f;
    }

    public float getCrossfade() {
        return this.h0;
    }

    public float getImagePanX() {
        return this.s0;
    }

    public float getImagePanY() {
        return this.t0;
    }

    public float getImageRotate() {
        return this.v0;
    }

    public float getImageZoom() {
        return this.u0;
    }

    public float getRound() {
        return this.j0;
    }

    public float getRoundPercent() {
        return this.i0;
    }

    public float getSaturation() {
        return this.g0.e;
    }

    public float getWarmth() {
        return this.g0.g;
    }

    @Override // android.view.View
    public void layout(int i, int i2, int i3, int i4) {
        super.layout(i, i2, i3, i4);
        d();
    }

    public void setAltImageResource(int i) {
        Drawable mutate = mf.d(getContext(), i).mutate();
        this.q0 = mutate;
        Drawable[] drawableArr = this.n0;
        drawableArr[0] = this.r0;
        drawableArr[1] = mutate;
        LayerDrawable layerDrawable = new LayerDrawable(this.n0);
        this.o0 = layerDrawable;
        super.setImageDrawable(layerDrawable);
        setCrossfade(this.h0);
    }

    public void setBrightness(float f) {
        ImageFilterView.c cVar = this.g0;
        cVar.d = f;
        cVar.c(this);
    }

    public void setContrast(float f) {
        ImageFilterView.c cVar = this.g0;
        cVar.f = f;
        cVar.c(this);
    }

    public void setCrossfade(float f) {
        this.h0 = f;
        if (this.n0 != null) {
            if (!this.p0) {
                this.o0.getDrawable(0).setAlpha((int) ((1.0f - this.h0) * 255.0f));
            }
            this.o0.getDrawable(1).setAlpha((int) (this.h0 * 255.0f));
            super.setImageDrawable(this.o0);
        }
    }

    @Override // androidx.appcompat.widget.AppCompatImageButton, android.widget.ImageView
    public void setImageDrawable(Drawable drawable) {
        if (this.q0 != null && drawable != null) {
            Drawable mutate = drawable.mutate();
            this.r0 = mutate;
            Drawable[] drawableArr = this.n0;
            drawableArr[0] = mutate;
            drawableArr[1] = this.q0;
            LayerDrawable layerDrawable = new LayerDrawable(this.n0);
            this.o0 = layerDrawable;
            super.setImageDrawable(layerDrawable);
            setCrossfade(this.h0);
            return;
        }
        super.setImageDrawable(drawable);
    }

    public void setImagePanX(float f) {
        this.s0 = f;
        e();
    }

    public void setImagePanY(float f) {
        this.t0 = f;
        e();
    }

    @Override // androidx.appcompat.widget.AppCompatImageButton, android.widget.ImageView
    public void setImageResource(int i) {
        if (this.q0 != null) {
            Drawable mutate = mf.d(getContext(), i).mutate();
            this.r0 = mutate;
            Drawable[] drawableArr = this.n0;
            drawableArr[0] = mutate;
            drawableArr[1] = this.q0;
            LayerDrawable layerDrawable = new LayerDrawable(this.n0);
            this.o0 = layerDrawable;
            super.setImageDrawable(layerDrawable);
            setCrossfade(this.h0);
            return;
        }
        super.setImageResource(i);
    }

    public void setImageRotate(float f) {
        this.v0 = f;
        e();
    }

    public void setImageZoom(float f) {
        this.u0 = f;
        e();
    }

    public void setRound(float f) {
        if (Float.isNaN(f)) {
            this.j0 = f;
            float f2 = this.i0;
            this.i0 = -1.0f;
            setRoundPercent(f2);
            return;
        }
        boolean z = this.j0 != f;
        this.j0 = f;
        if (f != Utils.FLOAT_EPSILON) {
            if (this.k0 == null) {
                this.k0 = new Path();
            }
            if (this.m0 == null) {
                this.m0 = new RectF();
            }
            if (Build.VERSION.SDK_INT >= 21) {
                if (this.l0 == null) {
                    b bVar = new b();
                    this.l0 = bVar;
                    setOutlineProvider(bVar);
                }
                setClipToOutline(true);
            }
            this.m0.set(Utils.FLOAT_EPSILON, Utils.FLOAT_EPSILON, getWidth(), getHeight());
            this.k0.reset();
            Path path = this.k0;
            RectF rectF = this.m0;
            float f3 = this.j0;
            path.addRoundRect(rectF, f3, f3, Path.Direction.CW);
        } else if (Build.VERSION.SDK_INT >= 21) {
            setClipToOutline(false);
        }
        if (!z || Build.VERSION.SDK_INT < 21) {
            return;
        }
        invalidateOutline();
    }

    public void setRoundPercent(float f) {
        boolean z = this.i0 != f;
        this.i0 = f;
        if (f != Utils.FLOAT_EPSILON) {
            if (this.k0 == null) {
                this.k0 = new Path();
            }
            if (this.m0 == null) {
                this.m0 = new RectF();
            }
            if (Build.VERSION.SDK_INT >= 21) {
                if (this.l0 == null) {
                    a aVar = new a();
                    this.l0 = aVar;
                    setOutlineProvider(aVar);
                }
                setClipToOutline(true);
            }
            int width = getWidth();
            int height = getHeight();
            float min = (Math.min(width, height) * this.i0) / 2.0f;
            this.m0.set(Utils.FLOAT_EPSILON, Utils.FLOAT_EPSILON, width, height);
            this.k0.reset();
            this.k0.addRoundRect(this.m0, min, min, Path.Direction.CW);
        } else if (Build.VERSION.SDK_INT >= 21) {
            setClipToOutline(false);
        }
        if (!z || Build.VERSION.SDK_INT < 21) {
            return;
        }
        invalidateOutline();
    }

    public void setSaturation(float f) {
        ImageFilterView.c cVar = this.g0;
        cVar.e = f;
        cVar.c(this);
    }

    public void setWarmth(float f) {
        ImageFilterView.c cVar = this.g0;
        cVar.g = f;
        cVar.c(this);
    }

    public ImageFilterButton(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        this.g0 = new ImageFilterView.c();
        this.h0 = Utils.FLOAT_EPSILON;
        this.i0 = Utils.FLOAT_EPSILON;
        this.j0 = Float.NaN;
        this.n0 = new Drawable[2];
        this.p0 = true;
        this.q0 = null;
        this.r0 = null;
        this.s0 = Float.NaN;
        this.t0 = Float.NaN;
        this.u0 = Float.NaN;
        this.v0 = Float.NaN;
        c(context, attributeSet);
    }

    public ImageFilterButton(Context context, AttributeSet attributeSet, int i) {
        super(context, attributeSet, i);
        this.g0 = new ImageFilterView.c();
        this.h0 = Utils.FLOAT_EPSILON;
        this.i0 = Utils.FLOAT_EPSILON;
        this.j0 = Float.NaN;
        this.n0 = new Drawable[2];
        this.p0 = true;
        this.q0 = null;
        this.r0 = null;
        this.s0 = Float.NaN;
        this.t0 = Float.NaN;
        this.u0 = Float.NaN;
        this.v0 = Float.NaN;
        c(context, attributeSet);
    }
}
