package androidx.constraintlayout.utils.widget;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Canvas;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.util.AttributeSet;
import android.view.ViewParent;
import androidx.constraintlayout.motion.widget.MotionLayout;

/* loaded from: classes.dex */
public class MotionTelltales extends MockView {
    public Paint p0;
    public MotionLayout q0;
    public float[] r0;
    public Matrix s0;
    public int t0;
    public int u0;
    public float v0;

    public MotionTelltales(Context context) {
        super(context);
        this.p0 = new Paint();
        this.r0 = new float[2];
        this.s0 = new Matrix();
        this.t0 = 0;
        this.u0 = -65281;
        this.v0 = 0.25f;
        a(context, null);
    }

    private void a(Context context, AttributeSet attributeSet) {
        if (attributeSet != null) {
            TypedArray obtainStyledAttributes = context.obtainStyledAttributes(attributeSet, w23.MotionTelltales);
            int indexCount = obtainStyledAttributes.getIndexCount();
            for (int i = 0; i < indexCount; i++) {
                int index = obtainStyledAttributes.getIndex(i);
                if (index == w23.MotionTelltales_telltales_tailColor) {
                    this.u0 = obtainStyledAttributes.getColor(index, this.u0);
                } else if (index == w23.MotionTelltales_telltales_velocityMode) {
                    this.t0 = obtainStyledAttributes.getInt(index, this.t0);
                } else if (index == w23.MotionTelltales_telltales_tailScale) {
                    this.v0 = obtainStyledAttributes.getFloat(index, this.v0);
                }
            }
            obtainStyledAttributes.recycle();
        }
        this.p0.setColor(this.u0);
        this.p0.setStrokeWidth(5.0f);
    }

    @Override // android.view.View
    public void onAttachedToWindow() {
        super.onAttachedToWindow();
    }

    @Override // androidx.constraintlayout.utils.widget.MockView, android.view.View
    public void onDraw(Canvas canvas) {
        super.onDraw(canvas);
        getMatrix().invert(this.s0);
        if (this.q0 == null) {
            ViewParent parent = getParent();
            if (parent instanceof MotionLayout) {
                this.q0 = (MotionLayout) parent;
                return;
            }
            return;
        }
        int width = getWidth();
        int height = getHeight();
        float[] fArr = {0.1f, 0.25f, 0.5f, 0.75f, 0.9f};
        for (int i = 0; i < 5; i++) {
            float f = fArr[i];
            for (int i2 = 0; i2 < 5; i2++) {
                float f2 = fArr[i2];
                this.q0.i0(this, f2, f, this.r0, this.t0);
                this.s0.mapVectors(this.r0);
                float f3 = width * f2;
                float f4 = height * f;
                float[] fArr2 = this.r0;
                float f5 = fArr2[0];
                float f6 = this.v0;
                float f7 = f4 - (fArr2[1] * f6);
                this.s0.mapVectors(fArr2);
                canvas.drawLine(f3, f4, f3 - (f5 * f6), f7, this.p0);
            }
        }
    }

    @Override // android.view.View
    public void onLayout(boolean z, int i, int i2, int i3, int i4) {
        super.onLayout(z, i, i2, i3, i4);
        postInvalidate();
    }

    public void setText(CharSequence charSequence) {
        this.j0 = charSequence.toString();
        requestLayout();
    }

    public MotionTelltales(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        this.p0 = new Paint();
        this.r0 = new float[2];
        this.s0 = new Matrix();
        this.t0 = 0;
        this.u0 = -65281;
        this.v0 = 0.25f;
        a(context, attributeSet);
    }

    public MotionTelltales(Context context, AttributeSet attributeSet, int i) {
        super(context, attributeSet, i);
        this.p0 = new Paint();
        this.r0 = new float[2];
        this.s0 = new Matrix();
        this.t0 = 0;
        this.u0 = -65281;
        this.v0 = 0.25f;
        a(context, attributeSet);
    }
}
