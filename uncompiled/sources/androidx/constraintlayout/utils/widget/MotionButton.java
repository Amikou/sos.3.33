package androidx.constraintlayout.utils.widget;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Canvas;
import android.graphics.Outline;
import android.graphics.Path;
import android.graphics.RectF;
import android.os.Build;
import android.util.AttributeSet;
import android.view.View;
import android.view.ViewOutlineProvider;
import androidx.appcompat.widget.AppCompatButton;
import com.github.mikephil.charting.utils.Utils;

/* loaded from: classes.dex */
public class MotionButton extends AppCompatButton {
    public float g0;
    public float h0;
    public Path i0;
    public ViewOutlineProvider j0;
    public RectF k0;

    /* loaded from: classes.dex */
    public class a extends ViewOutlineProvider {
        public a() {
        }

        @Override // android.view.ViewOutlineProvider
        public void getOutline(View view, Outline outline) {
            int width = MotionButton.this.getWidth();
            int height = MotionButton.this.getHeight();
            outline.setRoundRect(0, 0, width, height, (Math.min(width, height) * MotionButton.this.g0) / 2.0f);
        }
    }

    /* loaded from: classes.dex */
    public class b extends ViewOutlineProvider {
        public b() {
        }

        @Override // android.view.ViewOutlineProvider
        public void getOutline(View view, Outline outline) {
            outline.setRoundRect(0, 0, MotionButton.this.getWidth(), MotionButton.this.getHeight(), MotionButton.this.h0);
        }
    }

    public MotionButton(Context context) {
        super(context);
        this.g0 = Utils.FLOAT_EPSILON;
        this.h0 = Float.NaN;
        c(context, null);
    }

    public final void c(Context context, AttributeSet attributeSet) {
        setPadding(0, 0, 0, 0);
        if (attributeSet != null) {
            TypedArray obtainStyledAttributes = getContext().obtainStyledAttributes(attributeSet, w23.ImageFilterView);
            int indexCount = obtainStyledAttributes.getIndexCount();
            for (int i = 0; i < indexCount; i++) {
                int index = obtainStyledAttributes.getIndex(i);
                if (index == w23.ImageFilterView_round) {
                    if (Build.VERSION.SDK_INT >= 21) {
                        setRound(obtainStyledAttributes.getDimension(index, Utils.FLOAT_EPSILON));
                    }
                } else if (index == w23.ImageFilterView_roundPercent && Build.VERSION.SDK_INT >= 21) {
                    setRoundPercent(obtainStyledAttributes.getFloat(index, Utils.FLOAT_EPSILON));
                }
            }
            obtainStyledAttributes.recycle();
        }
    }

    @Override // android.view.View
    public void draw(Canvas canvas) {
        boolean z;
        if (Build.VERSION.SDK_INT >= 21 || this.h0 == Utils.FLOAT_EPSILON || this.i0 == null) {
            z = false;
        } else {
            z = true;
            canvas.save();
            canvas.clipPath(this.i0);
        }
        super.draw(canvas);
        if (z) {
            canvas.restore();
        }
    }

    public float getRound() {
        return this.h0;
    }

    public float getRoundPercent() {
        return this.g0;
    }

    public void setRound(float f) {
        if (Float.isNaN(f)) {
            this.h0 = f;
            float f2 = this.g0;
            this.g0 = -1.0f;
            setRoundPercent(f2);
            return;
        }
        boolean z = this.h0 != f;
        this.h0 = f;
        if (f != Utils.FLOAT_EPSILON) {
            if (this.i0 == null) {
                this.i0 = new Path();
            }
            if (this.k0 == null) {
                this.k0 = new RectF();
            }
            if (Build.VERSION.SDK_INT >= 21) {
                if (this.j0 == null) {
                    b bVar = new b();
                    this.j0 = bVar;
                    setOutlineProvider(bVar);
                }
                setClipToOutline(true);
            }
            this.k0.set(Utils.FLOAT_EPSILON, Utils.FLOAT_EPSILON, getWidth(), getHeight());
            this.i0.reset();
            Path path = this.i0;
            RectF rectF = this.k0;
            float f3 = this.h0;
            path.addRoundRect(rectF, f3, f3, Path.Direction.CW);
        } else if (Build.VERSION.SDK_INT >= 21) {
            setClipToOutline(false);
        }
        if (!z || Build.VERSION.SDK_INT < 21) {
            return;
        }
        invalidateOutline();
    }

    public void setRoundPercent(float f) {
        boolean z = this.g0 != f;
        this.g0 = f;
        if (f != Utils.FLOAT_EPSILON) {
            if (this.i0 == null) {
                this.i0 = new Path();
            }
            if (this.k0 == null) {
                this.k0 = new RectF();
            }
            if (Build.VERSION.SDK_INT >= 21) {
                if (this.j0 == null) {
                    a aVar = new a();
                    this.j0 = aVar;
                    setOutlineProvider(aVar);
                }
                setClipToOutline(true);
            }
            int width = getWidth();
            int height = getHeight();
            float min = (Math.min(width, height) * this.g0) / 2.0f;
            this.k0.set(Utils.FLOAT_EPSILON, Utils.FLOAT_EPSILON, width, height);
            this.i0.reset();
            this.i0.addRoundRect(this.k0, min, min, Path.Direction.CW);
        } else if (Build.VERSION.SDK_INT >= 21) {
            setClipToOutline(false);
        }
        if (!z || Build.VERSION.SDK_INT < 21) {
            return;
        }
        invalidateOutline();
    }

    public MotionButton(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        this.g0 = Utils.FLOAT_EPSILON;
        this.h0 = Float.NaN;
        c(context, attributeSet);
    }

    public MotionButton(Context context, AttributeSet attributeSet, int i) {
        super(context, attributeSet, i);
        this.g0 = Utils.FLOAT_EPSILON;
        this.h0 = Float.NaN;
        c(context, attributeSet);
    }
}
