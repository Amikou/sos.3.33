package androidx.constraintlayout.utils.widget;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Rect;
import android.util.AttributeSet;
import android.view.View;
import com.github.mikephil.charting.utils.Utils;

/* loaded from: classes.dex */
public class MockView extends View {
    public Paint a;
    public Paint f0;
    public Paint g0;
    public boolean h0;
    public boolean i0;
    public String j0;
    public Rect k0;
    public int l0;
    public int m0;
    public int n0;
    public int o0;

    public MockView(Context context) {
        super(context);
        this.a = new Paint();
        this.f0 = new Paint();
        this.g0 = new Paint();
        this.h0 = true;
        this.i0 = true;
        this.j0 = null;
        this.k0 = new Rect();
        this.l0 = Color.argb(255, 0, 0, 0);
        this.m0 = Color.argb(255, 200, 200, 200);
        this.n0 = Color.argb(255, 50, 50, 50);
        this.o0 = 4;
        a(context, null);
    }

    public final void a(Context context, AttributeSet attributeSet) {
        if (attributeSet != null) {
            TypedArray obtainStyledAttributes = context.obtainStyledAttributes(attributeSet, w23.MockView);
            int indexCount = obtainStyledAttributes.getIndexCount();
            for (int i = 0; i < indexCount; i++) {
                int index = obtainStyledAttributes.getIndex(i);
                if (index == w23.MockView_mock_label) {
                    this.j0 = obtainStyledAttributes.getString(index);
                } else if (index == w23.MockView_mock_showDiagonals) {
                    this.h0 = obtainStyledAttributes.getBoolean(index, this.h0);
                } else if (index == w23.MockView_mock_diagonalsColor) {
                    this.l0 = obtainStyledAttributes.getColor(index, this.l0);
                } else if (index == w23.MockView_mock_labelBackgroundColor) {
                    this.n0 = obtainStyledAttributes.getColor(index, this.n0);
                } else if (index == w23.MockView_mock_labelColor) {
                    this.m0 = obtainStyledAttributes.getColor(index, this.m0);
                } else if (index == w23.MockView_mock_showLabel) {
                    this.i0 = obtainStyledAttributes.getBoolean(index, this.i0);
                }
            }
            obtainStyledAttributes.recycle();
        }
        if (this.j0 == null) {
            try {
                this.j0 = context.getResources().getResourceEntryName(getId());
            } catch (Exception unused) {
            }
        }
        this.a.setColor(this.l0);
        this.a.setAntiAlias(true);
        this.f0.setColor(this.m0);
        this.f0.setAntiAlias(true);
        this.g0.setColor(this.n0);
        this.o0 = Math.round(this.o0 * (getResources().getDisplayMetrics().xdpi / 160.0f));
    }

    @Override // android.view.View
    public void onDraw(Canvas canvas) {
        super.onDraw(canvas);
        int width = getWidth();
        int height = getHeight();
        if (this.h0) {
            width--;
            height--;
            float f = width;
            float f2 = height;
            canvas.drawLine(Utils.FLOAT_EPSILON, Utils.FLOAT_EPSILON, f, f2, this.a);
            canvas.drawLine(Utils.FLOAT_EPSILON, f2, f, Utils.FLOAT_EPSILON, this.a);
            canvas.drawLine(Utils.FLOAT_EPSILON, Utils.FLOAT_EPSILON, f, Utils.FLOAT_EPSILON, this.a);
            canvas.drawLine(f, Utils.FLOAT_EPSILON, f, f2, this.a);
            canvas.drawLine(f, f2, Utils.FLOAT_EPSILON, f2, this.a);
            canvas.drawLine(Utils.FLOAT_EPSILON, f2, Utils.FLOAT_EPSILON, Utils.FLOAT_EPSILON, this.a);
        }
        String str = this.j0;
        if (str == null || !this.i0) {
            return;
        }
        this.f0.getTextBounds(str, 0, str.length(), this.k0);
        float width2 = (width - this.k0.width()) / 2.0f;
        float height2 = ((height - this.k0.height()) / 2.0f) + this.k0.height();
        this.k0.offset((int) width2, (int) height2);
        Rect rect = this.k0;
        int i = rect.left;
        int i2 = this.o0;
        rect.set(i - i2, rect.top - i2, rect.right + i2, rect.bottom + i2);
        canvas.drawRect(this.k0, this.g0);
        canvas.drawText(this.j0, width2, height2, this.f0);
    }

    public MockView(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        this.a = new Paint();
        this.f0 = new Paint();
        this.g0 = new Paint();
        this.h0 = true;
        this.i0 = true;
        this.j0 = null;
        this.k0 = new Rect();
        this.l0 = Color.argb(255, 0, 0, 0);
        this.m0 = Color.argb(255, 200, 200, 200);
        this.n0 = Color.argb(255, 50, 50, 50);
        this.o0 = 4;
        a(context, attributeSet);
    }

    public MockView(Context context, AttributeSet attributeSet, int i) {
        super(context, attributeSet, i);
        this.a = new Paint();
        this.f0 = new Paint();
        this.g0 = new Paint();
        this.h0 = true;
        this.i0 = true;
        this.j0 = null;
        this.k0 = new Rect();
        this.l0 = Color.argb(255, 0, 0, 0);
        this.m0 = Color.argb(255, 200, 200, 200);
        this.n0 = Color.argb(255, 50, 50, 50);
        this.o0 = 4;
        a(context, attributeSet);
    }
}
