package androidx.constraintlayout.utils.widget;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Bitmap;
import android.graphics.BitmapShader;
import android.graphics.Canvas;
import android.graphics.Matrix;
import android.graphics.Outline;
import android.graphics.Paint;
import android.graphics.Path;
import android.graphics.Rect;
import android.graphics.RectF;
import android.graphics.Shader;
import android.graphics.Typeface;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.text.Layout;
import android.text.TextPaint;
import android.util.AttributeSet;
import android.util.TypedValue;
import android.view.View;
import android.view.ViewOutlineProvider;
import com.github.mikephil.charting.utils.Utils;

/* loaded from: classes.dex */
public class MotionLabel extends View implements c71 {
    public Layout A0;
    public int B0;
    public int C0;
    public boolean D0;
    public float E0;
    public float F0;
    public float G0;
    public Drawable H0;
    public Matrix I0;
    public Bitmap J0;
    public BitmapShader K0;
    public Matrix L0;
    public float M0;
    public float N0;
    public float O0;
    public float P0;
    public Paint Q0;
    public int R0;
    public Rect S0;
    public Paint T0;
    public float U0;
    public float V0;
    public float W0;
    public float X0;
    public float Y0;
    public TextPaint a;
    public Path f0;
    public int g0;
    public int h0;
    public boolean i0;
    public float j0;
    public float k0;
    public ViewOutlineProvider l0;
    public RectF m0;
    public float n0;
    public float o0;
    public int p0;
    public int q0;
    public float r0;
    public String s0;
    public boolean t0;
    public Rect u0;
    public int v0;
    public int w0;
    public int x0;
    public int y0;
    public String z0;

    /* loaded from: classes.dex */
    public class a extends ViewOutlineProvider {
        public a() {
        }

        @Override // android.view.ViewOutlineProvider
        public void getOutline(View view, Outline outline) {
            int width = MotionLabel.this.getWidth();
            int height = MotionLabel.this.getHeight();
            outline.setRoundRect(0, 0, width, height, (Math.min(width, height) * MotionLabel.this.j0) / 2.0f);
        }
    }

    /* loaded from: classes.dex */
    public class b extends ViewOutlineProvider {
        public b() {
        }

        @Override // android.view.ViewOutlineProvider
        public void getOutline(View view, Outline outline) {
            outline.setRoundRect(0, 0, MotionLabel.this.getWidth(), MotionLabel.this.getHeight(), MotionLabel.this.k0);
        }
    }

    public MotionLabel(Context context) {
        super(context);
        this.a = new TextPaint();
        this.f0 = new Path();
        this.g0 = 65535;
        this.h0 = 65535;
        this.i0 = false;
        this.j0 = Utils.FLOAT_EPSILON;
        this.k0 = Float.NaN;
        this.n0 = 48.0f;
        this.o0 = Float.NaN;
        this.r0 = Utils.FLOAT_EPSILON;
        this.s0 = "Hello World";
        this.t0 = true;
        this.u0 = new Rect();
        this.v0 = 1;
        this.w0 = 1;
        this.x0 = 1;
        this.y0 = 1;
        this.B0 = 8388659;
        this.C0 = 0;
        this.D0 = false;
        this.M0 = Float.NaN;
        this.N0 = Float.NaN;
        this.O0 = Utils.FLOAT_EPSILON;
        this.P0 = Utils.FLOAT_EPSILON;
        this.Q0 = new Paint();
        this.R0 = 0;
        this.V0 = Float.NaN;
        this.W0 = Float.NaN;
        this.X0 = Float.NaN;
        this.Y0 = Float.NaN;
        g(context, null);
    }

    private float getHorizontalOffset() {
        float f = Float.isNaN(this.o0) ? 1.0f : this.n0 / this.o0;
        TextPaint textPaint = this.a;
        String str = this.s0;
        return (((((Float.isNaN(this.F0) ? getMeasuredWidth() : this.F0) - getPaddingLeft()) - getPaddingRight()) - (f * textPaint.measureText(str, 0, str.length()))) * (this.O0 + 1.0f)) / 2.0f;
    }

    private float getVerticalOffset() {
        float f = Float.isNaN(this.o0) ? 1.0f : this.n0 / this.o0;
        Paint.FontMetrics fontMetrics = this.a.getFontMetrics();
        float measuredHeight = ((Float.isNaN(this.G0) ? getMeasuredHeight() : this.G0) - getPaddingTop()) - getPaddingBottom();
        float f2 = fontMetrics.descent;
        float f3 = fontMetrics.ascent;
        return (((measuredHeight - ((f2 - f3) * f)) * (1.0f - this.P0)) / 2.0f) - (f * f3);
    }

    @Override // defpackage.c71
    public void a(float f, float f2, float f3, float f4) {
        int i = (int) (f + 0.5f);
        this.E0 = f - i;
        int i2 = (int) (f3 + 0.5f);
        int i3 = i2 - i;
        int i4 = (int) (f4 + 0.5f);
        int i5 = (int) (0.5f + f2);
        int i6 = i4 - i5;
        float f5 = f3 - f;
        this.F0 = f5;
        float f6 = f4 - f2;
        this.G0 = f6;
        d(f, f2, f3, f4);
        if (getMeasuredHeight() == i6 && getMeasuredWidth() == i3) {
            super.layout(i, i5, i2, i4);
        } else {
            measure(View.MeasureSpec.makeMeasureSpec(i3, 1073741824), View.MeasureSpec.makeMeasureSpec(i6, 1073741824));
            super.layout(i, i5, i2, i4);
        }
        if (this.D0) {
            if (this.S0 == null) {
                this.T0 = new Paint();
                this.S0 = new Rect();
                this.T0.set(this.a);
                this.U0 = this.T0.getTextSize();
            }
            this.F0 = f5;
            this.G0 = f6;
            Paint paint = this.T0;
            String str = this.s0;
            paint.getTextBounds(str, 0, str.length(), this.S0);
            float height = this.S0.height() * 1.3f;
            float f7 = (f5 - this.w0) - this.v0;
            float f8 = (f6 - this.y0) - this.x0;
            float width = this.S0.width();
            if (width * f8 > height * f7) {
                this.a.setTextSize((this.U0 * f7) / width);
            } else {
                this.a.setTextSize((this.U0 * f8) / height);
            }
            if (this.i0 || !Float.isNaN(this.o0)) {
                f(Float.isNaN(this.o0) ? 1.0f : this.n0 / this.o0);
            }
        }
    }

    public final void d(float f, float f2, float f3, float f4) {
        if (this.L0 == null) {
            return;
        }
        this.F0 = f3 - f;
        this.G0 = f4 - f2;
        l();
    }

    public Bitmap e(Bitmap bitmap, int i) {
        System.nanoTime();
        int width = bitmap.getWidth() / 2;
        int height = bitmap.getHeight() / 2;
        Bitmap createScaledBitmap = Bitmap.createScaledBitmap(bitmap, width, height, true);
        for (int i2 = 0; i2 < i && width >= 32 && height >= 32; i2++) {
            width /= 2;
            height /= 2;
            createScaledBitmap = Bitmap.createScaledBitmap(createScaledBitmap, width, height, true);
        }
        return createScaledBitmap;
    }

    public void f(float f) {
        if (this.i0 || f != 1.0f) {
            this.f0.reset();
            String str = this.s0;
            int length = str.length();
            this.a.getTextBounds(str, 0, length, this.u0);
            this.a.getTextPath(str, 0, length, Utils.FLOAT_EPSILON, Utils.FLOAT_EPSILON, this.f0);
            if (f != 1.0f) {
                StringBuilder sb = new StringBuilder();
                sb.append(xe0.a());
                sb.append(" scale ");
                sb.append(f);
                Matrix matrix = new Matrix();
                matrix.postScale(f, f);
                this.f0.transform(matrix);
            }
            Rect rect = this.u0;
            rect.right--;
            rect.left++;
            rect.bottom++;
            rect.top--;
            RectF rectF = new RectF();
            rectF.bottom = getHeight();
            rectF.right = getWidth();
            this.t0 = false;
        }
    }

    public final void g(Context context, AttributeSet attributeSet) {
        i(context, attributeSet);
        if (attributeSet != null) {
            TypedArray obtainStyledAttributes = getContext().obtainStyledAttributes(attributeSet, w23.MotionLabel);
            int indexCount = obtainStyledAttributes.getIndexCount();
            for (int i = 0; i < indexCount; i++) {
                int index = obtainStyledAttributes.getIndex(i);
                if (index == w23.MotionLabel_android_text) {
                    setText(obtainStyledAttributes.getText(index));
                } else if (index == w23.MotionLabel_android_fontFamily) {
                    this.z0 = obtainStyledAttributes.getString(index);
                } else if (index == w23.MotionLabel_scaleFromTextSize) {
                    this.o0 = obtainStyledAttributes.getDimensionPixelSize(index, (int) this.o0);
                } else if (index == w23.MotionLabel_android_textSize) {
                    this.n0 = obtainStyledAttributes.getDimensionPixelSize(index, (int) this.n0);
                } else if (index == w23.MotionLabel_android_textStyle) {
                    this.p0 = obtainStyledAttributes.getInt(index, this.p0);
                } else if (index == w23.MotionLabel_android_typeface) {
                    this.q0 = obtainStyledAttributes.getInt(index, this.q0);
                } else if (index == w23.MotionLabel_android_textColor) {
                    this.g0 = obtainStyledAttributes.getColor(index, this.g0);
                } else if (index == w23.MotionLabel_borderRound) {
                    float dimension = obtainStyledAttributes.getDimension(index, this.k0);
                    this.k0 = dimension;
                    if (Build.VERSION.SDK_INT >= 21) {
                        setRound(dimension);
                    }
                } else if (index == w23.MotionLabel_borderRoundPercent) {
                    float f = obtainStyledAttributes.getFloat(index, this.j0);
                    this.j0 = f;
                    if (Build.VERSION.SDK_INT >= 21) {
                        setRoundPercent(f);
                    }
                } else if (index == w23.MotionLabel_android_gravity) {
                    setGravity(obtainStyledAttributes.getInt(index, -1));
                } else if (index == w23.MotionLabel_android_autoSizeTextType) {
                    this.C0 = obtainStyledAttributes.getInt(index, 0);
                } else if (index == w23.MotionLabel_textOutlineColor) {
                    this.h0 = obtainStyledAttributes.getInt(index, this.h0);
                    this.i0 = true;
                } else if (index == w23.MotionLabel_textOutlineThickness) {
                    this.r0 = obtainStyledAttributes.getDimension(index, this.r0);
                    this.i0 = true;
                } else if (index == w23.MotionLabel_textBackground) {
                    this.H0 = obtainStyledAttributes.getDrawable(index);
                    this.i0 = true;
                } else if (index == w23.MotionLabel_textBackgroundPanX) {
                    this.V0 = obtainStyledAttributes.getFloat(index, this.V0);
                } else if (index == w23.MotionLabel_textBackgroundPanY) {
                    this.W0 = obtainStyledAttributes.getFloat(index, this.W0);
                } else if (index == w23.MotionLabel_textPanX) {
                    this.O0 = obtainStyledAttributes.getFloat(index, this.O0);
                } else if (index == w23.MotionLabel_textPanY) {
                    this.P0 = obtainStyledAttributes.getFloat(index, this.P0);
                } else if (index == w23.MotionLabel_textBackgroundRotate) {
                    this.Y0 = obtainStyledAttributes.getFloat(index, this.Y0);
                } else if (index == w23.MotionLabel_textBackgroundZoom) {
                    this.X0 = obtainStyledAttributes.getFloat(index, this.X0);
                } else if (index == w23.MotionLabel_textureHeight) {
                    this.M0 = obtainStyledAttributes.getDimension(index, this.M0);
                } else if (index == w23.MotionLabel_textureWidth) {
                    this.N0 = obtainStyledAttributes.getDimension(index, this.N0);
                } else if (index == w23.MotionLabel_textureEffect) {
                    this.R0 = obtainStyledAttributes.getInt(index, this.R0);
                }
            }
            obtainStyledAttributes.recycle();
        }
        k();
        j();
    }

    public float getRound() {
        return this.k0;
    }

    public float getRoundPercent() {
        return this.j0;
    }

    public float getScaleFromTextSize() {
        return this.o0;
    }

    public float getTextBackgroundPanX() {
        return this.V0;
    }

    public float getTextBackgroundPanY() {
        return this.W0;
    }

    public float getTextBackgroundRotate() {
        return this.Y0;
    }

    public float getTextBackgroundZoom() {
        return this.X0;
    }

    public int getTextOutlineColor() {
        return this.h0;
    }

    public float getTextPanX() {
        return this.O0;
    }

    public float getTextPanY() {
        return this.P0;
    }

    public float getTextureHeight() {
        return this.M0;
    }

    public float getTextureWidth() {
        return this.N0;
    }

    public Typeface getTypeface() {
        return this.a.getTypeface();
    }

    public final void h(String str, int i, int i2) {
        Typeface typeface;
        Typeface create;
        if (str != null) {
            typeface = Typeface.create(str, i2);
            if (typeface != null) {
                setTypeface(typeface);
                return;
            }
        } else {
            typeface = null;
        }
        if (i == 1) {
            typeface = Typeface.SANS_SERIF;
        } else if (i == 2) {
            typeface = Typeface.SERIF;
        } else if (i == 3) {
            typeface = Typeface.MONOSPACE;
        }
        float f = Utils.FLOAT_EPSILON;
        if (i2 > 0) {
            if (typeface == null) {
                create = Typeface.defaultFromStyle(i2);
            } else {
                create = Typeface.create(typeface, i2);
            }
            setTypeface(create);
            int i3 = (~(create != null ? create.getStyle() : 0)) & i2;
            this.a.setFakeBoldText((i3 & 1) != 0);
            TextPaint textPaint = this.a;
            if ((i3 & 2) != 0) {
                f = -0.25f;
            }
            textPaint.setTextSkewX(f);
            return;
        }
        this.a.setFakeBoldText(false);
        this.a.setTextSkewX(Utils.FLOAT_EPSILON);
        setTypeface(typeface);
    }

    public final void i(Context context, AttributeSet attributeSet) {
        TypedValue typedValue = new TypedValue();
        context.getTheme().resolveAttribute(jy2.colorPrimary, typedValue, true);
        TextPaint textPaint = this.a;
        int i = typedValue.data;
        this.g0 = i;
        textPaint.setColor(i);
    }

    public void j() {
        this.v0 = getPaddingLeft();
        this.w0 = getPaddingRight();
        this.x0 = getPaddingTop();
        this.y0 = getPaddingBottom();
        h(this.z0, this.q0, this.p0);
        this.a.setColor(this.g0);
        this.a.setStrokeWidth(this.r0);
        this.a.setStyle(Paint.Style.FILL_AND_STROKE);
        this.a.setFlags(128);
        setTextSize(this.n0);
        this.a.setAntiAlias(true);
    }

    public final void k() {
        if (this.H0 != null) {
            this.L0 = new Matrix();
            int intrinsicWidth = this.H0.getIntrinsicWidth();
            int intrinsicHeight = this.H0.getIntrinsicHeight();
            if (intrinsicWidth <= 0 && (intrinsicWidth = getWidth()) == 0) {
                intrinsicWidth = Float.isNaN(this.N0) ? 128 : (int) this.N0;
            }
            if (intrinsicHeight <= 0 && (intrinsicHeight = getHeight()) == 0) {
                intrinsicHeight = Float.isNaN(this.M0) ? 128 : (int) this.M0;
            }
            if (this.R0 != 0) {
                intrinsicWidth /= 2;
                intrinsicHeight /= 2;
            }
            this.J0 = Bitmap.createBitmap(intrinsicWidth, intrinsicHeight, Bitmap.Config.ARGB_8888);
            Canvas canvas = new Canvas(this.J0);
            this.H0.setBounds(0, 0, canvas.getWidth(), canvas.getHeight());
            this.H0.setFilterBitmap(true);
            this.H0.draw(canvas);
            if (this.R0 != 0) {
                this.J0 = e(this.J0, 4);
            }
            Bitmap bitmap = this.J0;
            Shader.TileMode tileMode = Shader.TileMode.REPEAT;
            this.K0 = new BitmapShader(bitmap, tileMode, tileMode);
        }
    }

    public final void l() {
        boolean isNaN = Float.isNaN(this.V0);
        float f = Utils.FLOAT_EPSILON;
        float f2 = isNaN ? 0.0f : this.V0;
        float f3 = Float.isNaN(this.W0) ? 0.0f : this.W0;
        float f4 = Float.isNaN(this.X0) ? 1.0f : this.X0;
        if (!Float.isNaN(this.Y0)) {
            f = this.Y0;
        }
        this.L0.reset();
        float width = this.J0.getWidth();
        float height = this.J0.getHeight();
        float f5 = Float.isNaN(this.N0) ? this.F0 : this.N0;
        float f6 = Float.isNaN(this.M0) ? this.G0 : this.M0;
        float f7 = f4 * (width * f6 < height * f5 ? f5 / width : f6 / height);
        this.L0.postScale(f7, f7);
        float f8 = width * f7;
        float f9 = f5 - f8;
        float f10 = f7 * height;
        float f11 = f6 - f10;
        if (!Float.isNaN(this.M0)) {
            f11 = this.M0 / 2.0f;
        }
        if (!Float.isNaN(this.N0)) {
            f9 = this.N0 / 2.0f;
        }
        this.L0.postTranslate((((f2 * f9) + f5) - f8) * 0.5f, (((f3 * f11) + f6) - f10) * 0.5f);
        this.L0.postRotate(f, f5 / 2.0f, f6 / 2.0f);
        this.K0.setLocalMatrix(this.L0);
    }

    @Override // android.view.View
    public void layout(int i, int i2, int i3, int i4) {
        super.layout(i, i2, i3, i4);
        boolean isNaN = Float.isNaN(this.o0);
        float f = isNaN ? 1.0f : this.n0 / this.o0;
        this.F0 = i3 - i;
        this.G0 = i4 - i2;
        if (this.D0) {
            if (this.S0 == null) {
                this.T0 = new Paint();
                this.S0 = new Rect();
                this.T0.set(this.a);
                this.U0 = this.T0.getTextSize();
            }
            Paint paint = this.T0;
            String str = this.s0;
            paint.getTextBounds(str, 0, str.length(), this.S0);
            int width = this.S0.width();
            int height = (int) (this.S0.height() * 1.3f);
            float f2 = (this.F0 - this.w0) - this.v0;
            float f3 = (this.G0 - this.y0) - this.x0;
            if (isNaN) {
                float f4 = width;
                float f5 = height;
                if (f4 * f3 > f5 * f2) {
                    this.a.setTextSize((this.U0 * f2) / f4);
                } else {
                    this.a.setTextSize((this.U0 * f3) / f5);
                }
            } else {
                float f6 = width;
                float f7 = height;
                f = f6 * f3 > f7 * f2 ? f2 / f6 : f3 / f7;
            }
        }
        if (this.i0 || !isNaN) {
            d(i, i2, i3, i4);
            f(f);
        }
    }

    @Override // android.view.View
    public void onDraw(Canvas canvas) {
        float f = Float.isNaN(this.o0) ? 1.0f : this.n0 / this.o0;
        super.onDraw(canvas);
        if (!this.i0 && f == 1.0f) {
            canvas.drawText(this.s0, this.E0 + this.v0 + getHorizontalOffset(), this.x0 + getVerticalOffset(), this.a);
            return;
        }
        if (this.t0) {
            f(f);
        }
        if (this.I0 == null) {
            this.I0 = new Matrix();
        }
        if (this.i0) {
            this.Q0.set(this.a);
            this.I0.reset();
            float horizontalOffset = this.v0 + getHorizontalOffset();
            float verticalOffset = this.x0 + getVerticalOffset();
            this.I0.postTranslate(horizontalOffset, verticalOffset);
            this.I0.preScale(f, f);
            this.f0.transform(this.I0);
            if (this.K0 != null) {
                this.a.setFilterBitmap(true);
                this.a.setShader(this.K0);
            } else {
                this.a.setColor(this.g0);
            }
            this.a.setStyle(Paint.Style.FILL);
            this.a.setStrokeWidth(this.r0);
            canvas.drawPath(this.f0, this.a);
            if (this.K0 != null) {
                this.a.setShader(null);
            }
            this.a.setColor(this.h0);
            this.a.setStyle(Paint.Style.STROKE);
            this.a.setStrokeWidth(this.r0);
            canvas.drawPath(this.f0, this.a);
            this.I0.reset();
            this.I0.postTranslate(-horizontalOffset, -verticalOffset);
            this.f0.transform(this.I0);
            this.a.set(this.Q0);
            return;
        }
        float horizontalOffset2 = this.v0 + getHorizontalOffset();
        float verticalOffset2 = this.x0 + getVerticalOffset();
        this.I0.reset();
        this.I0.preTranslate(horizontalOffset2, verticalOffset2);
        this.f0.transform(this.I0);
        this.a.setColor(this.g0);
        this.a.setStyle(Paint.Style.FILL_AND_STROKE);
        this.a.setStrokeWidth(this.r0);
        canvas.drawPath(this.f0, this.a);
        this.I0.reset();
        this.I0.preTranslate(-horizontalOffset2, -verticalOffset2);
        this.f0.transform(this.I0);
    }

    @Override // android.view.View
    public void onMeasure(int i, int i2) {
        int mode = View.MeasureSpec.getMode(i);
        int mode2 = View.MeasureSpec.getMode(i2);
        int size = View.MeasureSpec.getSize(i);
        int size2 = View.MeasureSpec.getSize(i2);
        this.D0 = false;
        this.v0 = getPaddingLeft();
        this.w0 = getPaddingRight();
        this.x0 = getPaddingTop();
        this.y0 = getPaddingBottom();
        if (mode == 1073741824 && mode2 == 1073741824) {
            if (this.C0 != 0) {
                this.D0 = true;
            }
        } else {
            TextPaint textPaint = this.a;
            String str = this.s0;
            textPaint.getTextBounds(str, 0, str.length(), this.u0);
            if (mode != 1073741824) {
                size = (int) (this.u0.width() + 0.99999f);
            }
            size += this.v0 + this.w0;
            if (mode2 != 1073741824) {
                int fontMetricsInt = (int) (this.a.getFontMetricsInt(null) + 0.99999f);
                if (mode2 == Integer.MIN_VALUE) {
                    fontMetricsInt = Math.min(size2, fontMetricsInt);
                }
                size2 = this.x0 + this.y0 + fontMetricsInt;
            }
        }
        setMeasuredDimension(size, size2);
    }

    @SuppressLint({"RtlHardcoded"})
    public void setGravity(int i) {
        if ((i & 8388615) == 0) {
            i |= 8388611;
        }
        if ((i & 112) == 0) {
            i |= 48;
        }
        if (i != this.B0) {
            invalidate();
        }
        this.B0 = i;
        int i2 = i & 112;
        if (i2 == 48) {
            this.P0 = -1.0f;
        } else if (i2 != 80) {
            this.P0 = Utils.FLOAT_EPSILON;
        } else {
            this.P0 = 1.0f;
        }
        int i3 = i & 8388615;
        if (i3 != 3) {
            if (i3 != 5) {
                if (i3 != 8388611) {
                    if (i3 != 8388613) {
                        this.O0 = Utils.FLOAT_EPSILON;
                        return;
                    }
                }
            }
            this.O0 = 1.0f;
            return;
        }
        this.O0 = -1.0f;
    }

    public void setRound(float f) {
        if (Float.isNaN(f)) {
            this.k0 = f;
            float f2 = this.j0;
            this.j0 = -1.0f;
            setRoundPercent(f2);
            return;
        }
        boolean z = this.k0 != f;
        this.k0 = f;
        if (f != Utils.FLOAT_EPSILON) {
            if (this.f0 == null) {
                this.f0 = new Path();
            }
            if (this.m0 == null) {
                this.m0 = new RectF();
            }
            if (Build.VERSION.SDK_INT >= 21) {
                if (this.l0 == null) {
                    b bVar = new b();
                    this.l0 = bVar;
                    setOutlineProvider(bVar);
                }
                setClipToOutline(true);
            }
            this.m0.set(Utils.FLOAT_EPSILON, Utils.FLOAT_EPSILON, getWidth(), getHeight());
            this.f0.reset();
            Path path = this.f0;
            RectF rectF = this.m0;
            float f3 = this.k0;
            path.addRoundRect(rectF, f3, f3, Path.Direction.CW);
        } else if (Build.VERSION.SDK_INT >= 21) {
            setClipToOutline(false);
        }
        if (!z || Build.VERSION.SDK_INT < 21) {
            return;
        }
        invalidateOutline();
    }

    public void setRoundPercent(float f) {
        boolean z = this.j0 != f;
        this.j0 = f;
        if (f != Utils.FLOAT_EPSILON) {
            if (this.f0 == null) {
                this.f0 = new Path();
            }
            if (this.m0 == null) {
                this.m0 = new RectF();
            }
            if (Build.VERSION.SDK_INT >= 21) {
                if (this.l0 == null) {
                    a aVar = new a();
                    this.l0 = aVar;
                    setOutlineProvider(aVar);
                }
                setClipToOutline(true);
            }
            int width = getWidth();
            int height = getHeight();
            float min = (Math.min(width, height) * this.j0) / 2.0f;
            this.m0.set(Utils.FLOAT_EPSILON, Utils.FLOAT_EPSILON, width, height);
            this.f0.reset();
            this.f0.addRoundRect(this.m0, min, min, Path.Direction.CW);
        } else if (Build.VERSION.SDK_INT >= 21) {
            setClipToOutline(false);
        }
        if (!z || Build.VERSION.SDK_INT < 21) {
            return;
        }
        invalidateOutline();
    }

    public void setScaleFromTextSize(float f) {
        this.o0 = f;
    }

    public void setText(CharSequence charSequence) {
        this.s0 = charSequence.toString();
        invalidate();
    }

    public void setTextBackgroundPanX(float f) {
        this.V0 = f;
        l();
        invalidate();
    }

    public void setTextBackgroundPanY(float f) {
        this.W0 = f;
        l();
        invalidate();
    }

    public void setTextBackgroundRotate(float f) {
        this.Y0 = f;
        l();
        invalidate();
    }

    public void setTextBackgroundZoom(float f) {
        this.X0 = f;
        l();
        invalidate();
    }

    public void setTextFillColor(int i) {
        this.g0 = i;
        invalidate();
    }

    public void setTextOutlineColor(int i) {
        this.h0 = i;
        this.i0 = true;
        invalidate();
    }

    public void setTextOutlineThickness(float f) {
        this.r0 = f;
        this.i0 = true;
        if (Float.isNaN(f)) {
            this.r0 = 1.0f;
            this.i0 = false;
        }
        invalidate();
    }

    public void setTextPanX(float f) {
        this.O0 = f;
        invalidate();
    }

    public void setTextPanY(float f) {
        this.P0 = f;
        invalidate();
    }

    public void setTextSize(float f) {
        this.n0 = f;
        StringBuilder sb = new StringBuilder();
        sb.append(xe0.a());
        sb.append("  ");
        sb.append(f);
        sb.append(" / ");
        sb.append(this.o0);
        TextPaint textPaint = this.a;
        if (!Float.isNaN(this.o0)) {
            f = this.o0;
        }
        textPaint.setTextSize(f);
        f(Float.isNaN(this.o0) ? 1.0f : this.n0 / this.o0);
        requestLayout();
        invalidate();
    }

    public void setTextureHeight(float f) {
        this.M0 = f;
        l();
        invalidate();
    }

    public void setTextureWidth(float f) {
        this.N0 = f;
        l();
        invalidate();
    }

    public void setTypeface(Typeface typeface) {
        if (this.a.getTypeface() != typeface) {
            this.a.setTypeface(typeface);
            if (this.A0 != null) {
                this.A0 = null;
                requestLayout();
                invalidate();
            }
        }
    }

    public MotionLabel(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        this.a = new TextPaint();
        this.f0 = new Path();
        this.g0 = 65535;
        this.h0 = 65535;
        this.i0 = false;
        this.j0 = Utils.FLOAT_EPSILON;
        this.k0 = Float.NaN;
        this.n0 = 48.0f;
        this.o0 = Float.NaN;
        this.r0 = Utils.FLOAT_EPSILON;
        this.s0 = "Hello World";
        this.t0 = true;
        this.u0 = new Rect();
        this.v0 = 1;
        this.w0 = 1;
        this.x0 = 1;
        this.y0 = 1;
        this.B0 = 8388659;
        this.C0 = 0;
        this.D0 = false;
        this.M0 = Float.NaN;
        this.N0 = Float.NaN;
        this.O0 = Utils.FLOAT_EPSILON;
        this.P0 = Utils.FLOAT_EPSILON;
        this.Q0 = new Paint();
        this.R0 = 0;
        this.V0 = Float.NaN;
        this.W0 = Float.NaN;
        this.X0 = Float.NaN;
        this.Y0 = Float.NaN;
        g(context, attributeSet);
    }

    public MotionLabel(Context context, AttributeSet attributeSet, int i) {
        super(context, attributeSet, i);
        this.a = new TextPaint();
        this.f0 = new Path();
        this.g0 = 65535;
        this.h0 = 65535;
        this.i0 = false;
        this.j0 = Utils.FLOAT_EPSILON;
        this.k0 = Float.NaN;
        this.n0 = 48.0f;
        this.o0 = Float.NaN;
        this.r0 = Utils.FLOAT_EPSILON;
        this.s0 = "Hello World";
        this.t0 = true;
        this.u0 = new Rect();
        this.v0 = 1;
        this.w0 = 1;
        this.x0 = 1;
        this.y0 = 1;
        this.B0 = 8388659;
        this.C0 = 0;
        this.D0 = false;
        this.M0 = Float.NaN;
        this.N0 = Float.NaN;
        this.O0 = Utils.FLOAT_EPSILON;
        this.P0 = Utils.FLOAT_EPSILON;
        this.Q0 = new Paint();
        this.R0 = 0;
        this.V0 = Float.NaN;
        this.W0 = Float.NaN;
        this.X0 = Float.NaN;
        this.Y0 = Float.NaN;
        g(context, attributeSet);
    }
}
