package androidx.room;

import android.annotation.SuppressLint;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteException;
import android.os.Build;
import androidx.lifecycle.LiveData;
import androidx.room.f;
import java.lang.ref.WeakReference;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Locale;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.Callable;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.locks.Lock;

/* compiled from: InvalidationTracker.java */
/* loaded from: classes.dex */
public class f {
    public static final String[] m = {"UPDATE", "DELETE", "INSERT"};
    public final String[] b;
    public Map<String, Set<String>> c;
    public final RoomDatabase e;
    public volatile ww3 h;
    public b i;
    public final js1 j;
    public androidx.room.a d = null;
    public AtomicBoolean f = new AtomicBoolean(false);
    public volatile boolean g = false;
    @SuppressLint({"RestrictedApi"})
    public final vb3<c, d> k = new vb3<>();
    public Runnable l = new a();
    public final HashMap<String, Integer> a = new HashMap<>();

    /* compiled from: InvalidationTracker.java */
    /* loaded from: classes.dex */
    public class a implements Runnable {
        public a() {
        }

        public final Set<Integer> a() {
            HashSet hashSet = new HashSet();
            Cursor C = f.this.e.C(new pp3("SELECT * FROM room_table_modification_log WHERE invalidated = 1;"));
            while (C.moveToNext()) {
                try {
                    hashSet.add(Integer.valueOf(C.getInt(0)));
                } catch (Throwable th) {
                    C.close();
                    throw th;
                }
            }
            C.close();
            if (!hashSet.isEmpty()) {
                f.this.h.T();
            }
            return hashSet;
        }

        /* JADX WARN: Code restructure failed: missing block: B:25:0x0071, code lost:
            if (r0 != null) goto L31;
         */
        /* JADX WARN: Code restructure failed: missing block: B:26:0x0073, code lost:
            r0.b();
         */
        /* JADX WARN: Code restructure failed: missing block: B:36:0x0091, code lost:
            if (r0 == null) goto L32;
         */
        /* JADX WARN: Code restructure failed: missing block: B:38:0x0094, code lost:
            if (r1 == null) goto L51;
         */
        /* JADX WARN: Code restructure failed: missing block: B:40:0x009a, code lost:
            if (r1.isEmpty() != false) goto L50;
         */
        /* JADX WARN: Code restructure failed: missing block: B:41:0x009c, code lost:
            r0 = r5.a.k;
         */
        /* JADX WARN: Code restructure failed: missing block: B:42:0x00a0, code lost:
            monitor-enter(r0);
         */
        /* JADX WARN: Code restructure failed: missing block: B:43:0x00a1, code lost:
            r2 = r5.a.k.iterator();
         */
        /* JADX WARN: Code restructure failed: missing block: B:45:0x00ad, code lost:
            if (r2.hasNext() == false) goto L42;
         */
        /* JADX WARN: Code restructure failed: missing block: B:46:0x00af, code lost:
            r2.next().getValue().a(r1);
         */
        /* JADX WARN: Code restructure failed: missing block: B:47:0x00bf, code lost:
            monitor-exit(r0);
         */
        /* JADX WARN: Code restructure failed: missing block: B:52:0x00c4, code lost:
            return;
         */
        /* JADX WARN: Code restructure failed: missing block: B:62:?, code lost:
            return;
         */
        /* JADX WARN: Code restructure failed: missing block: B:63:?, code lost:
            return;
         */
        @Override // java.lang.Runnable
        /*
            Code decompiled incorrectly, please refer to instructions dump.
            To view partially-correct code enable 'Show inconsistent code' option in preferences
        */
        public void run() {
            /*
                r5 = this;
                androidx.room.f r0 = androidx.room.f.this
                androidx.room.RoomDatabase r0 = r0.e
                java.util.concurrent.locks.Lock r0 = r0.m()
                r0.lock()
                r1 = 0
                androidx.room.f r2 = androidx.room.f.this     // Catch: java.lang.Throwable -> L7c java.lang.Throwable -> L8a
                boolean r2 = r2.f()     // Catch: java.lang.Throwable -> L7c java.lang.Throwable -> L8a
                if (r2 != 0) goto L21
                r0.unlock()
                androidx.room.f r0 = androidx.room.f.this
                androidx.room.a r0 = r0.d
                if (r0 == 0) goto L20
                r0.b()
            L20:
                return
            L21:
                androidx.room.f r2 = androidx.room.f.this     // Catch: java.lang.Throwable -> L7c java.lang.Throwable -> L8a
                java.util.concurrent.atomic.AtomicBoolean r2 = r2.f     // Catch: java.lang.Throwable -> L7c java.lang.Throwable -> L8a
                r3 = 1
                r4 = 0
                boolean r2 = r2.compareAndSet(r3, r4)     // Catch: java.lang.Throwable -> L7c java.lang.Throwable -> L8a
                if (r2 != 0) goto L3a
                r0.unlock()
                androidx.room.f r0 = androidx.room.f.this
                androidx.room.a r0 = r0.d
                if (r0 == 0) goto L39
                r0.b()
            L39:
                return
            L3a:
                androidx.room.f r2 = androidx.room.f.this     // Catch: java.lang.Throwable -> L7c java.lang.Throwable -> L8a
                androidx.room.RoomDatabase r2 = r2.e     // Catch: java.lang.Throwable -> L7c java.lang.Throwable -> L8a
                boolean r2 = r2.t()     // Catch: java.lang.Throwable -> L7c java.lang.Throwable -> L8a
                if (r2 == 0) goto L51
                r0.unlock()
                androidx.room.f r0 = androidx.room.f.this
                androidx.room.a r0 = r0.d
                if (r0 == 0) goto L50
                r0.b()
            L50:
                return
            L51:
                androidx.room.f r2 = androidx.room.f.this     // Catch: java.lang.Throwable -> L7c java.lang.Throwable -> L8a
                androidx.room.RoomDatabase r2 = r2.e     // Catch: java.lang.Throwable -> L7c java.lang.Throwable -> L8a
                tw3 r2 = r2.o()     // Catch: java.lang.Throwable -> L7c java.lang.Throwable -> L8a
                sw3 r2 = r2.D0()     // Catch: java.lang.Throwable -> L7c java.lang.Throwable -> L8a
                r2.v0()     // Catch: java.lang.Throwable -> L7c java.lang.Throwable -> L8a
                java.util.Set r1 = r5.a()     // Catch: java.lang.Throwable -> L77
                r2.s0()     // Catch: java.lang.Throwable -> L77
                r2.N0()     // Catch: java.lang.Throwable -> L7c java.lang.Throwable -> L8a
                r0.unlock()
                androidx.room.f r0 = androidx.room.f.this
                androidx.room.a r0 = r0.d
                if (r0 == 0) goto L94
            L73:
                r0.b()
                goto L94
            L77:
                r3 = move-exception
                r2.N0()     // Catch: java.lang.Throwable -> L7c java.lang.Throwable -> L8a
                throw r3     // Catch: java.lang.Throwable -> L7c java.lang.Throwable -> L8a
            L7c:
                r1 = move-exception
                r0.unlock()
                androidx.room.f r0 = androidx.room.f.this
                androidx.room.a r0 = r0.d
                if (r0 == 0) goto L89
                r0.b()
            L89:
                throw r1
            L8a:
                r0.unlock()
                androidx.room.f r0 = androidx.room.f.this
                androidx.room.a r0 = r0.d
                if (r0 == 0) goto L94
                goto L73
            L94:
                if (r1 == 0) goto Lc4
                boolean r0 = r1.isEmpty()
                if (r0 != 0) goto Lc4
                androidx.room.f r0 = androidx.room.f.this
                vb3<androidx.room.f$c, androidx.room.f$d> r0 = r0.k
                monitor-enter(r0)
                androidx.room.f r2 = androidx.room.f.this     // Catch: java.lang.Throwable -> Lc1
                vb3<androidx.room.f$c, androidx.room.f$d> r2 = r2.k     // Catch: java.lang.Throwable -> Lc1
                java.util.Iterator r2 = r2.iterator()     // Catch: java.lang.Throwable -> Lc1
            La9:
                boolean r3 = r2.hasNext()     // Catch: java.lang.Throwable -> Lc1
                if (r3 == 0) goto Lbf
                java.lang.Object r3 = r2.next()     // Catch: java.lang.Throwable -> Lc1
                java.util.Map$Entry r3 = (java.util.Map.Entry) r3     // Catch: java.lang.Throwable -> Lc1
                java.lang.Object r3 = r3.getValue()     // Catch: java.lang.Throwable -> Lc1
                androidx.room.f$d r3 = (androidx.room.f.d) r3     // Catch: java.lang.Throwable -> Lc1
                r3.a(r1)     // Catch: java.lang.Throwable -> Lc1
                goto La9
            Lbf:
                monitor-exit(r0)     // Catch: java.lang.Throwable -> Lc1
                goto Lc4
            Lc1:
                r1 = move-exception
                monitor-exit(r0)     // Catch: java.lang.Throwable -> Lc1
                throw r1
            Lc4:
                return
            */
            throw new UnsupportedOperationException("Method not decompiled: androidx.room.f.a.run():void");
        }
    }

    /* compiled from: InvalidationTracker.java */
    /* loaded from: classes.dex */
    public static class b {
        public final long[] a;
        public final boolean[] b;
        public final int[] c;
        public boolean d;
        public boolean e;

        public b(int i) {
            long[] jArr = new long[i];
            this.a = jArr;
            boolean[] zArr = new boolean[i];
            this.b = zArr;
            this.c = new int[i];
            Arrays.fill(jArr, 0L);
            Arrays.fill(zArr, false);
        }

        public int[] a() {
            synchronized (this) {
                if (this.d && !this.e) {
                    int length = this.a.length;
                    int i = 0;
                    while (true) {
                        int i2 = 1;
                        if (i < length) {
                            boolean z = this.a[i] > 0;
                            boolean[] zArr = this.b;
                            if (z != zArr[i]) {
                                int[] iArr = this.c;
                                if (!z) {
                                    i2 = 2;
                                }
                                iArr[i] = i2;
                            } else {
                                this.c[i] = 0;
                            }
                            zArr[i] = z;
                            i++;
                        } else {
                            this.e = true;
                            this.d = false;
                            return this.c;
                        }
                    }
                }
                return null;
            }
        }

        public boolean b(int... iArr) {
            boolean z;
            synchronized (this) {
                z = false;
                for (int i : iArr) {
                    long[] jArr = this.a;
                    long j = jArr[i];
                    jArr[i] = 1 + j;
                    if (j == 0) {
                        this.d = true;
                        z = true;
                    }
                }
            }
            return z;
        }

        public boolean c(int... iArr) {
            boolean z;
            synchronized (this) {
                z = false;
                for (int i : iArr) {
                    long[] jArr = this.a;
                    long j = jArr[i];
                    jArr[i] = j - 1;
                    if (j == 1) {
                        this.d = true;
                        z = true;
                    }
                }
            }
            return z;
        }

        public void d() {
            synchronized (this) {
                this.e = false;
            }
        }

        public void e() {
            synchronized (this) {
                Arrays.fill(this.b, false);
                this.d = true;
            }
        }
    }

    /* compiled from: InvalidationTracker.java */
    /* loaded from: classes.dex */
    public static abstract class c {
        public final String[] a;

        public c(String[] strArr) {
            this.a = (String[]) Arrays.copyOf(strArr, strArr.length);
        }

        public boolean a() {
            return false;
        }

        public abstract void b(Set<String> set);
    }

    /* compiled from: InvalidationTracker.java */
    /* loaded from: classes.dex */
    public static class d {
        public final int[] a;
        public final String[] b;
        public final c c;
        public final Set<String> d;

        public d(c cVar, int[] iArr, String[] strArr) {
            this.c = cVar;
            this.a = iArr;
            this.b = strArr;
            if (iArr.length == 1) {
                HashSet hashSet = new HashSet();
                hashSet.add(strArr[0]);
                this.d = Collections.unmodifiableSet(hashSet);
                return;
            }
            this.d = null;
        }

        public void a(Set<Integer> set) {
            int length = this.a.length;
            Set<String> set2 = null;
            for (int i = 0; i < length; i++) {
                if (set.contains(Integer.valueOf(this.a[i]))) {
                    if (length == 1) {
                        set2 = this.d;
                    } else {
                        if (set2 == null) {
                            set2 = new HashSet<>(length);
                        }
                        set2.add(this.b[i]);
                    }
                }
            }
            if (set2 != null) {
                this.c.b(set2);
            }
        }

        public void b(String[] strArr) {
            Set<String> set = null;
            if (this.b.length == 1) {
                int length = strArr.length;
                int i = 0;
                while (true) {
                    if (i >= length) {
                        break;
                    } else if (strArr[i].equalsIgnoreCase(this.b[0])) {
                        set = this.d;
                        break;
                    } else {
                        i++;
                    }
                }
            } else {
                HashSet hashSet = new HashSet();
                for (String str : strArr) {
                    String[] strArr2 = this.b;
                    int length2 = strArr2.length;
                    int i2 = 0;
                    while (true) {
                        if (i2 < length2) {
                            String str2 = strArr2[i2];
                            if (str2.equalsIgnoreCase(str)) {
                                hashSet.add(str2);
                                break;
                            }
                            i2++;
                        }
                    }
                }
                if (hashSet.size() > 0) {
                    set = hashSet;
                }
            }
            if (set != null) {
                this.c.b(set);
            }
        }
    }

    /* compiled from: InvalidationTracker.java */
    /* loaded from: classes.dex */
    public static class e extends c {
        public final f b;
        public final WeakReference<c> c;

        public e(f fVar, c cVar) {
            super(cVar.a);
            this.b = fVar;
            this.c = new WeakReference<>(cVar);
        }

        @Override // androidx.room.f.c
        public void b(Set<String> set) {
            c cVar = this.c.get();
            if (cVar == null) {
                this.b.k(this);
            } else {
                cVar.b(set);
            }
        }
    }

    public f(RoomDatabase roomDatabase, Map<String, String> map, Map<String, Set<String>> map2, String... strArr) {
        this.e = roomDatabase;
        this.i = new b(strArr.length);
        this.c = map2;
        this.j = new js1(roomDatabase);
        int length = strArr.length;
        this.b = new String[length];
        for (int i = 0; i < length; i++) {
            String str = strArr[i];
            Locale locale = Locale.US;
            String lowerCase = str.toLowerCase(locale);
            this.a.put(lowerCase, Integer.valueOf(i));
            String str2 = map.get(strArr[i]);
            if (str2 != null) {
                this.b[i] = str2.toLowerCase(locale);
            } else {
                this.b[i] = lowerCase;
            }
        }
        for (Map.Entry<String, String> entry : map.entrySet()) {
            Locale locale2 = Locale.US;
            String lowerCase2 = entry.getValue().toLowerCase(locale2);
            if (this.a.containsKey(lowerCase2)) {
                String lowerCase3 = entry.getKey().toLowerCase(locale2);
                HashMap<String, Integer> hashMap = this.a;
                hashMap.put(lowerCase3, hashMap.get(lowerCase2));
            }
        }
    }

    public static void c(StringBuilder sb, String str, String str2) {
        sb.append("`");
        sb.append("room_table_modification_trigger_");
        sb.append(str);
        sb.append("_");
        sb.append(str2);
        sb.append("`");
    }

    public static void d(sw3 sw3Var) {
        if (Build.VERSION.SDK_INT >= 16 && sw3Var.q1()) {
            sw3Var.v0();
        } else {
            sw3Var.B();
        }
    }

    @SuppressLint({"RestrictedApi"})
    public void a(c cVar) {
        d o;
        String[] l = l(cVar.a);
        int[] iArr = new int[l.length];
        int length = l.length;
        for (int i = 0; i < length; i++) {
            Integer num = this.a.get(l[i].toLowerCase(Locale.US));
            if (num != null) {
                iArr[i] = num.intValue();
            } else {
                throw new IllegalArgumentException("There is no table with name " + l[i]);
            }
        }
        d dVar = new d(cVar, iArr, l);
        synchronized (this.k) {
            o = this.k.o(cVar, dVar);
        }
        if (o == null && this.i.b(iArr)) {
            q();
        }
    }

    public void b(c cVar) {
        a(new e(this, cVar));
    }

    public <T> LiveData<T> e(String[] strArr, boolean z, Callable<T> callable) {
        return this.j.a(s(strArr), z, callable);
    }

    public boolean f() {
        if (this.e.z()) {
            if (!this.g) {
                this.e.o().D0();
            }
            return this.g;
        }
        return false;
    }

    public void g(sw3 sw3Var) {
        synchronized (this) {
            if (this.g) {
                return;
            }
            sw3Var.K("PRAGMA temp_store = MEMORY;");
            sw3Var.K("PRAGMA recursive_triggers='ON';");
            sw3Var.K("CREATE TEMP TABLE room_table_modification_log(table_id INTEGER PRIMARY KEY, invalidated INTEGER NOT NULL DEFAULT 0)");
            r(sw3Var);
            this.h = sw3Var.U("UPDATE room_table_modification_log SET invalidated = 0 WHERE invalidated = 1 ");
            this.g = true;
        }
    }

    public void h(String... strArr) {
        synchronized (this.k) {
            Iterator<Map.Entry<c, d>> it = this.k.iterator();
            while (it.hasNext()) {
                Map.Entry<c, d> next = it.next();
                if (!next.getKey().a()) {
                    next.getValue().b(strArr);
                }
            }
        }
    }

    public void i() {
        synchronized (this) {
            this.g = false;
            this.i.e();
        }
    }

    public void j() {
        if (this.f.compareAndSet(false, true)) {
            androidx.room.a aVar = this.d;
            if (aVar != null) {
                aVar.e();
            }
            this.e.p().execute(this.l);
        }
    }

    @SuppressLint({"RestrictedApi"})
    public void k(c cVar) {
        d p;
        synchronized (this.k) {
            p = this.k.p(cVar);
        }
        if (p == null || !this.i.c(p.a)) {
            return;
        }
        q();
    }

    public final String[] l(String[] strArr) {
        HashSet hashSet = new HashSet();
        for (String str : strArr) {
            String lowerCase = str.toLowerCase(Locale.US);
            if (this.c.containsKey(lowerCase)) {
                hashSet.addAll(this.c.get(lowerCase));
            } else {
                hashSet.add(str);
            }
        }
        return (String[]) hashSet.toArray(new String[hashSet.size()]);
    }

    public void m(androidx.room.a aVar) {
        this.d = aVar;
        aVar.h(new Runnable() { // from class: ks1
            @Override // java.lang.Runnable
            public final void run() {
                f.this.i();
            }
        });
    }

    public void n(Context context, String str) {
        new g(context, str, this, this.e.p());
    }

    public final void o(sw3 sw3Var, int i) {
        String[] strArr;
        sw3Var.K("INSERT OR IGNORE INTO room_table_modification_log VALUES(" + i + ", 0)");
        String str = this.b[i];
        StringBuilder sb = new StringBuilder();
        for (String str2 : m) {
            sb.setLength(0);
            sb.append("CREATE TEMP TRIGGER IF NOT EXISTS ");
            c(sb, str, str2);
            sb.append(" AFTER ");
            sb.append(str2);
            sb.append(" ON `");
            sb.append(str);
            sb.append("` BEGIN UPDATE ");
            sb.append("room_table_modification_log");
            sb.append(" SET ");
            sb.append("invalidated");
            sb.append(" = 1");
            sb.append(" WHERE ");
            sb.append("table_id");
            sb.append(" = ");
            sb.append(i);
            sb.append(" AND ");
            sb.append("invalidated");
            sb.append(" = 0");
            sb.append("; END");
            sw3Var.K(sb.toString());
        }
    }

    public final void p(sw3 sw3Var, int i) {
        String[] strArr;
        String str = this.b[i];
        StringBuilder sb = new StringBuilder();
        for (String str2 : m) {
            sb.setLength(0);
            sb.append("DROP TRIGGER IF EXISTS ");
            c(sb, str, str2);
            sw3Var.K(sb.toString());
        }
    }

    public void q() {
        if (this.e.z()) {
            r(this.e.o().D0());
        }
    }

    public void r(sw3 sw3Var) {
        if (sw3Var.h1()) {
            return;
        }
        while (true) {
            try {
                Lock m2 = this.e.m();
                m2.lock();
                int[] a2 = this.i.a();
                if (a2 == null) {
                    m2.unlock();
                    return;
                }
                int length = a2.length;
                d(sw3Var);
                for (int i = 0; i < length; i++) {
                    int i2 = a2[i];
                    if (i2 == 1) {
                        o(sw3Var, i);
                    } else if (i2 == 2) {
                        p(sw3Var, i);
                    }
                }
                sw3Var.s0();
                sw3Var.N0();
                this.i.d();
                m2.unlock();
            } catch (SQLiteException | IllegalStateException unused) {
                return;
            }
        }
    }

    public final String[] s(String[] strArr) {
        String[] l = l(strArr);
        for (String str : l) {
            if (!this.a.containsKey(str.toLowerCase(Locale.US))) {
                throw new IllegalArgumentException("There is no table with name " + str);
            }
        }
        return l;
    }
}
