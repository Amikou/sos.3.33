package androidx.room;

import android.database.Cursor;
import defpackage.tw3;
import java.util.List;

/* compiled from: RoomOpenHelper.java */
/* loaded from: classes.dex */
public class m extends tw3.a {
    public c b;
    public final a c;
    public final String d;
    public final String e;

    /* compiled from: RoomOpenHelper.java */
    /* loaded from: classes.dex */
    public static abstract class a {
        public final int a;

        public a(int i) {
            this.a = i;
        }

        public abstract void a(sw3 sw3Var);

        public abstract void b(sw3 sw3Var);

        public abstract void c(sw3 sw3Var);

        public abstract void d(sw3 sw3Var);

        public abstract void e(sw3 sw3Var);

        public abstract void f(sw3 sw3Var);

        public abstract b g(sw3 sw3Var);
    }

    /* compiled from: RoomOpenHelper.java */
    /* loaded from: classes.dex */
    public static class b {
        public final boolean a;
        public final String b;

        public b(boolean z, String str) {
            this.a = z;
            this.b = str;
        }
    }

    public m(c cVar, a aVar, String str, String str2) {
        super(aVar.a);
        this.b = cVar;
        this.c = aVar;
        this.d = str;
        this.e = str2;
    }

    public static boolean j(sw3 sw3Var) {
        Cursor E0 = sw3Var.E0("SELECT count(*) FROM sqlite_master WHERE name != 'android_metadata'");
        try {
            boolean z = false;
            if (E0.moveToFirst()) {
                if (E0.getInt(0) == 0) {
                    z = true;
                }
            }
            return z;
        } finally {
            E0.close();
        }
    }

    public static boolean k(sw3 sw3Var) {
        Cursor E0 = sw3Var.E0("SELECT 1 FROM sqlite_master WHERE type = 'table' AND name='room_master_table'");
        try {
            boolean z = false;
            if (E0.moveToFirst()) {
                if (E0.getInt(0) != 0) {
                    z = true;
                }
            }
            return z;
        } finally {
            E0.close();
        }
    }

    @Override // defpackage.tw3.a
    public void b(sw3 sw3Var) {
        super.b(sw3Var);
    }

    @Override // defpackage.tw3.a
    public void d(sw3 sw3Var) {
        boolean j = j(sw3Var);
        this.c.a(sw3Var);
        if (!j) {
            b g = this.c.g(sw3Var);
            if (!g.a) {
                throw new IllegalStateException("Pre-packaged database has an invalid schema: " + g.b);
            }
        }
        l(sw3Var);
        this.c.c(sw3Var);
    }

    @Override // defpackage.tw3.a
    public void e(sw3 sw3Var, int i, int i2) {
        g(sw3Var, i, i2);
    }

    @Override // defpackage.tw3.a
    public void f(sw3 sw3Var) {
        super.f(sw3Var);
        h(sw3Var);
        this.c.d(sw3Var);
        this.b = null;
    }

    @Override // defpackage.tw3.a
    public void g(sw3 sw3Var, int i, int i2) {
        boolean z;
        List<w82> c;
        c cVar = this.b;
        if (cVar == null || (c = cVar.d.c(i, i2)) == null) {
            z = false;
        } else {
            this.c.f(sw3Var);
            for (w82 w82Var : c) {
                w82Var.a(sw3Var);
            }
            b g = this.c.g(sw3Var);
            if (g.a) {
                this.c.e(sw3Var);
                l(sw3Var);
                z = true;
            } else {
                throw new IllegalStateException("Migration didn't properly handle: " + g.b);
            }
        }
        if (z) {
            return;
        }
        c cVar2 = this.b;
        if (cVar2 != null && !cVar2.a(i, i2)) {
            this.c.b(sw3Var);
            this.c.a(sw3Var);
            return;
        }
        throw new IllegalStateException("A migration from " + i + " to " + i2 + " was required but not found. Please provide the necessary Migration path via RoomDatabase.Builder.addMigration(Migration ...) or allow for destructive migrations via one of the RoomDatabase.Builder.fallbackToDestructiveMigration* methods.");
    }

    public final void h(sw3 sw3Var) {
        if (k(sw3Var)) {
            Cursor D = sw3Var.D(new pp3("SELECT identity_hash FROM room_master_table WHERE id = 42 LIMIT 1"));
            try {
                String string = D.moveToFirst() ? D.getString(0) : null;
                D.close();
                if (!this.d.equals(string) && !this.e.equals(string)) {
                    throw new IllegalStateException("Room cannot verify the data integrity. Looks like you've changed schema but forgot to update the version number. You can simply fix this by increasing the version number.");
                }
                return;
            } catch (Throwable th) {
                D.close();
                throw th;
            }
        }
        b g = this.c.g(sw3Var);
        if (g.a) {
            this.c.e(sw3Var);
            l(sw3Var);
            return;
        }
        throw new IllegalStateException("Pre-packaged database has an invalid schema: " + g.b);
    }

    public final void i(sw3 sw3Var) {
        sw3Var.K("CREATE TABLE IF NOT EXISTS room_master_table (id INTEGER PRIMARY KEY,identity_hash TEXT)");
    }

    public final void l(sw3 sw3Var) {
        i(sw3Var);
        sw3Var.K(j93.a(this.d));
    }
}
