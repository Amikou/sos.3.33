package androidx.room;

import android.content.Context;
import androidx.room.RoomDatabase;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.nio.channels.Channels;
import java.nio.channels.ReadableByteChannel;
import java.util.concurrent.Callable;

/* compiled from: SQLiteCopyOpenHelper.java */
/* loaded from: classes.dex */
public class o implements tw3, cm0 {
    public final Context a;
    public final String f0;
    public final File g0;
    public final Callable<InputStream> h0;
    public final int i0;
    public final tw3 j0;
    public c k0;
    public boolean l0;

    public o(Context context, String str, File file, Callable<InputStream> callable, int i, tw3 tw3Var) {
        this.a = context;
        this.f0 = str;
        this.g0 = file;
        this.h0 = callable;
        this.i0 = i;
        this.j0 = tw3Var;
    }

    @Override // defpackage.tw3
    public synchronized sw3 D0() {
        if (!this.l0) {
            d(true);
            this.l0 = true;
        }
        return this.j0.D0();
    }

    public final void a(File file, boolean z) throws IOException {
        ReadableByteChannel newChannel;
        if (this.f0 != null) {
            newChannel = Channels.newChannel(this.a.getAssets().open(this.f0));
        } else if (this.g0 != null) {
            newChannel = new FileInputStream(this.g0).getChannel();
        } else {
            Callable<InputStream> callable = this.h0;
            if (callable != null) {
                try {
                    newChannel = Channels.newChannel(callable.call());
                } catch (Exception e) {
                    throw new IOException("inputStreamCallable exception on call", e);
                }
            } else {
                throw new IllegalStateException("copyFromAssetPath, copyFromFile and copyFromInputStream are all null!");
            }
        }
        File createTempFile = File.createTempFile("room-copy-helper", ".tmp", this.a.getCacheDir());
        createTempFile.deleteOnExit();
        z31.a(newChannel, new FileOutputStream(createTempFile).getChannel());
        File parentFile = file.getParentFile();
        if (parentFile != null && !parentFile.exists() && !parentFile.mkdirs()) {
            throw new IOException("Failed to create directories for " + file.getAbsolutePath());
        }
        b(createTempFile, z);
        if (createTempFile.renameTo(file)) {
            return;
        }
        throw new IOException("Failed to move intermediate file (" + createTempFile.getAbsolutePath() + ") to destination (" + file.getAbsolutePath() + ").");
    }

    public final void b(File file, boolean z) {
        c cVar = this.k0;
        if (cVar != null) {
            RoomDatabase.d dVar = cVar.f;
        }
    }

    public void c(c cVar) {
        this.k0 = cVar;
    }

    @Override // defpackage.tw3, java.io.Closeable, java.lang.AutoCloseable
    public synchronized void close() {
        this.j0.close();
        this.l0 = false;
    }

    public final void d(boolean z) {
        String databaseName = getDatabaseName();
        File databasePath = this.a.getDatabasePath(databaseName);
        c cVar = this.k0;
        s80 s80Var = new s80(databaseName, this.a.getFilesDir(), cVar == null || cVar.m);
        try {
            s80Var.b();
            if (!databasePath.exists()) {
                try {
                    a(databasePath, z);
                } catch (IOException e) {
                    throw new RuntimeException("Unable to copy database file.", e);
                }
            } else if (this.k0 == null) {
            } else {
                try {
                    int d = id0.d(databasePath);
                    int i = this.i0;
                    if (d == i) {
                        return;
                    }
                    if (this.k0.a(d, i)) {
                        return;
                    }
                    if (this.a.deleteDatabase(databaseName)) {
                        try {
                            a(databasePath, z);
                        } catch (IOException unused) {
                        }
                    } else {
                        StringBuilder sb = new StringBuilder();
                        sb.append("Failed to delete database file (");
                        sb.append(databaseName);
                        sb.append(") for a copy destructive migration.");
                    }
                } catch (IOException unused2) {
                }
            }
        } finally {
            s80Var.c();
        }
    }

    @Override // defpackage.tw3
    public String getDatabaseName() {
        return this.j0.getDatabaseName();
    }

    @Override // defpackage.cm0
    public tw3 getDelegate() {
        return this.j0;
    }

    @Override // defpackage.tw3
    public void setWriteAheadLoggingEnabled(boolean z) {
        this.j0.setWriteAheadLoggingEnabled(z);
    }
}
