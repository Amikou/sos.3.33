package androidx.room;

import androidx.room.RoomDatabase;
import java.util.concurrent.Executor;

/* compiled from: QueryInterceptorOpenHelper.java */
/* loaded from: classes.dex */
public final class i implements tw3, cm0 {
    public final tw3 a;
    public final RoomDatabase.e f0;
    public final Executor g0;

    public i(tw3 tw3Var, RoomDatabase.e eVar, Executor executor) {
        this.a = tw3Var;
        this.f0 = eVar;
        this.g0 = executor;
    }

    @Override // defpackage.tw3
    public sw3 D0() {
        return new h(this.a.D0(), this.f0, this.g0);
    }

    @Override // defpackage.tw3, java.io.Closeable, java.lang.AutoCloseable
    public void close() {
        this.a.close();
    }

    @Override // defpackage.tw3
    public String getDatabaseName() {
        return this.a.getDatabaseName();
    }

    @Override // defpackage.cm0
    public tw3 getDelegate() {
        return this.a;
    }

    @Override // defpackage.tw3
    public void setWriteAheadLoggingEnabled(boolean z) {
        this.a.setWriteAheadLoggingEnabled(z);
    }
}
