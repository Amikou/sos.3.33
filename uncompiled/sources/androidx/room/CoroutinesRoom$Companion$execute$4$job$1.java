package androidx.room;

import java.util.concurrent.Callable;
import kotlin.Result;
import kotlin.coroutines.jvm.internal.SuspendLambda;

/* compiled from: CoroutinesRoom.kt */
@kotlin.coroutines.jvm.internal.a(c = "androidx.room.CoroutinesRoom$Companion$execute$4$job$1", f = "CoroutinesRoom.kt", l = {}, m = "invokeSuspend")
/* loaded from: classes.dex */
public final class CoroutinesRoom$Companion$execute$4$job$1 extends SuspendLambda implements hd1<c90, q70<? super te4>, Object> {
    public final /* synthetic */ Callable<R> $callable;
    public final /* synthetic */ ov<R> $continuation;
    public int label;

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    /* JADX WARN: Multi-variable type inference failed */
    public CoroutinesRoom$Companion$execute$4$job$1(Callable<R> callable, ov<? super R> ovVar, q70<? super CoroutinesRoom$Companion$execute$4$job$1> q70Var) {
        super(2, q70Var);
        this.$callable = callable;
        this.$continuation = ovVar;
    }

    @Override // kotlin.coroutines.jvm.internal.BaseContinuationImpl
    public final q70<te4> create(Object obj, q70<?> q70Var) {
        return new CoroutinesRoom$Companion$execute$4$job$1(this.$callable, this.$continuation, q70Var);
    }

    @Override // defpackage.hd1
    public final Object invoke(c90 c90Var, q70<? super te4> q70Var) {
        return ((CoroutinesRoom$Companion$execute$4$job$1) create(c90Var, q70Var)).invokeSuspend(te4.a);
    }

    @Override // kotlin.coroutines.jvm.internal.BaseContinuationImpl
    public final Object invokeSuspend(Object obj) {
        gs1.d();
        if (this.label == 0) {
            o83.b(obj);
            try {
                Object call = this.$callable.call();
                q70 q70Var = this.$continuation;
                Result.a aVar = Result.Companion;
                q70Var.resumeWith(Result.m52constructorimpl(call));
            } catch (Throwable th) {
                q70 q70Var2 = this.$continuation;
                Result.a aVar2 = Result.Companion;
                q70Var2.resumeWith(Result.m52constructorimpl(o83.a(th)));
            }
            return te4.a;
        }
        throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
    }
}
