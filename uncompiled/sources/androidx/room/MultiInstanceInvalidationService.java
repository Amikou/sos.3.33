package androidx.room;

import android.app.Service;
import android.content.Intent;
import android.os.IBinder;
import android.os.RemoteCallbackList;
import android.os.RemoteException;
import androidx.room.e;
import java.util.HashMap;

/* loaded from: classes.dex */
public class MultiInstanceInvalidationService extends Service {
    public int a = 0;
    public final HashMap<Integer, String> f0 = new HashMap<>();
    public final RemoteCallbackList<d> g0 = new a();
    public final e.a h0 = new b();

    /* loaded from: classes.dex */
    public class a extends RemoteCallbackList<d> {
        public a() {
        }

        @Override // android.os.RemoteCallbackList
        /* renamed from: a */
        public void onCallbackDied(d dVar, Object obj) {
            MultiInstanceInvalidationService.this.f0.remove(Integer.valueOf(((Integer) obj).intValue()));
        }
    }

    /* loaded from: classes.dex */
    public class b extends e.a {
        public b() {
        }

        @Override // androidx.room.e
        public int K(d dVar, String str) {
            if (str == null) {
                return 0;
            }
            synchronized (MultiInstanceInvalidationService.this.g0) {
                MultiInstanceInvalidationService multiInstanceInvalidationService = MultiInstanceInvalidationService.this;
                int i = multiInstanceInvalidationService.a + 1;
                multiInstanceInvalidationService.a = i;
                if (multiInstanceInvalidationService.g0.register(dVar, Integer.valueOf(i))) {
                    MultiInstanceInvalidationService.this.f0.put(Integer.valueOf(i), str);
                    return i;
                }
                MultiInstanceInvalidationService multiInstanceInvalidationService2 = MultiInstanceInvalidationService.this;
                multiInstanceInvalidationService2.a--;
                return 0;
            }
        }

        @Override // androidx.room.e
        public void m1(int i, String[] strArr) {
            synchronized (MultiInstanceInvalidationService.this.g0) {
                String str = MultiInstanceInvalidationService.this.f0.get(Integer.valueOf(i));
                if (str == null) {
                    return;
                }
                int beginBroadcast = MultiInstanceInvalidationService.this.g0.beginBroadcast();
                for (int i2 = 0; i2 < beginBroadcast; i2++) {
                    int intValue = ((Integer) MultiInstanceInvalidationService.this.g0.getBroadcastCookie(i2)).intValue();
                    String str2 = MultiInstanceInvalidationService.this.f0.get(Integer.valueOf(intValue));
                    if (i != intValue && str.equals(str2)) {
                        try {
                            MultiInstanceInvalidationService.this.g0.getBroadcastItem(i2).z(strArr);
                        } catch (RemoteException unused) {
                        }
                    }
                }
                MultiInstanceInvalidationService.this.g0.finishBroadcast();
            }
        }

        @Override // androidx.room.e
        public void x1(d dVar, int i) {
            synchronized (MultiInstanceInvalidationService.this.g0) {
                MultiInstanceInvalidationService.this.g0.unregister(dVar);
                MultiInstanceInvalidationService.this.f0.remove(Integer.valueOf(i));
            }
        }
    }

    @Override // android.app.Service
    public IBinder onBind(Intent intent) {
        return this.h0;
    }
}
