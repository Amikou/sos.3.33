package androidx.room;

import android.annotation.SuppressLint;
import android.app.ActivityManager;
import android.content.Context;
import android.database.Cursor;
import android.os.Build;
import android.os.CancellationSignal;
import android.os.Looper;
import androidx.room.RoomDatabase;
import androidx.room.migration.Migration;
import defpackage.tw3;
import java.io.File;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.BitSet;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeMap;
import java.util.concurrent.Callable;
import java.util.concurrent.Executor;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantReadWriteLock;

/* loaded from: classes.dex */
public abstract class RoomDatabase {
    @Deprecated
    public volatile sw3 a;
    public Executor b;
    public Executor c;
    public tw3 d;
    public boolean f;
    @Deprecated
    public List<b> g;
    public androidx.room.a j;
    public final ReentrantReadWriteLock i = new ReentrantReadWriteLock();
    public final ThreadLocal<Integer> k = new ThreadLocal<>();
    public final Map<String, Object> l = Collections.synchronizedMap(new HashMap());
    public final f e = h();
    public final Map<Class<?>, Object> m = new HashMap();
    public Map<Class<? extends fk>, fk> h = new HashMap();

    /* loaded from: classes.dex */
    public enum JournalMode {
        AUTOMATIC,
        TRUNCATE,
        WRITE_AHEAD_LOGGING;

        public static boolean a(ActivityManager activityManager) {
            if (Build.VERSION.SDK_INT >= 19) {
                return ow3.b(activityManager);
            }
            return false;
        }

        public JournalMode resolve(Context context) {
            ActivityManager activityManager;
            if (this != AUTOMATIC) {
                return this;
            }
            if (Build.VERSION.SDK_INT >= 16 && (activityManager = (ActivityManager) context.getSystemService("activity")) != null && !a(activityManager)) {
                return WRITE_AHEAD_LOGGING;
            }
            return TRUNCATE;
        }
    }

    /* loaded from: classes.dex */
    public static class a<T extends RoomDatabase> {
        public final Class<T> a;
        public final String b;
        public final Context c;
        public ArrayList<b> d;
        public d e;
        public e f;
        public Executor g;
        public List<Object> h;
        public List<fk> i;
        public Executor j;
        public Executor k;
        public tw3.c l;
        public boolean m;
        public boolean o;
        public boolean q;
        public TimeUnit s;
        public Set<Integer> u;
        public Set<Integer> v;
        public String w;
        public File x;
        public Callable<InputStream> y;
        public long r = -1;
        public JournalMode n = JournalMode.AUTOMATIC;
        public boolean p = true;
        public final c t = new c();

        public a(Context context, Class<T> cls, String str) {
            this.c = context;
            this.a = cls;
            this.b = str;
        }

        public a<T> a(b bVar) {
            if (this.d == null) {
                this.d = new ArrayList<>();
            }
            this.d.add(bVar);
            return this;
        }

        public a<T> b(Migration... migrationArr) {
            if (this.v == null) {
                this.v = new HashSet();
            }
            for (Migration migration : migrationArr) {
                this.v.add(Integer.valueOf(migration.a));
                this.v.add(Integer.valueOf(migration.b));
            }
            this.t.b(migrationArr);
            return this;
        }

        public a<T> c() {
            this.m = true;
            return this;
        }

        @SuppressLint({"RestrictedApi"})
        public T d() {
            Executor executor;
            if (this.c != null) {
                if (this.a != null) {
                    Executor executor2 = this.j;
                    if (executor2 == null && this.k == null) {
                        Executor e = gh.e();
                        this.k = e;
                        this.j = e;
                    } else if (executor2 != null && this.k == null) {
                        this.k = executor2;
                    } else if (executor2 == null && (executor = this.k) != null) {
                        this.j = executor;
                    }
                    Set<Integer> set = this.v;
                    if (set != null && this.u != null) {
                        for (Integer num : set) {
                            if (this.u.contains(num)) {
                                throw new IllegalArgumentException("Inconsistency detected. A Migration was supplied to addMigration(Migration... migrations) that has a start or end version equal to a start version supplied to fallbackToDestructiveMigrationFrom(int... startVersions). Start version: " + num);
                            }
                        }
                    }
                    dk dkVar = this.l;
                    if (dkVar == null) {
                        dkVar = new hc1();
                    }
                    long j = this.r;
                    if (j > 0) {
                        if (this.b != null) {
                            dkVar = new dk(dkVar, new androidx.room.a(j, this.s, this.k));
                        } else {
                            throw new IllegalArgumentException("Cannot create auto-closing database for an in-memory database.");
                        }
                    }
                    String str = this.w;
                    if (str != null || this.x != null || this.y != null) {
                        if (this.b != null) {
                            int i = str == null ? 0 : 1;
                            File file = this.x;
                            int i2 = i + (file == null ? 0 : 1);
                            Callable<InputStream> callable = this.y;
                            if (i2 + (callable != null ? 1 : 0) == 1) {
                                dkVar = new xa3(str, file, callable, dkVar);
                            } else {
                                throw new IllegalArgumentException("More than one of createFromAsset(), createFromInputStream(), and createFromFile() were called on this Builder, but the database can only be created using one of the three configurations.");
                            }
                        } else {
                            throw new IllegalArgumentException("Cannot create from asset or file for an in-memory database.");
                        }
                    }
                    e eVar = this.f;
                    j jVar = eVar != null ? new j(dkVar, eVar, this.g) : dkVar;
                    Context context = this.c;
                    androidx.room.c cVar = new androidx.room.c(context, this.b, jVar, this.t, this.d, this.m, this.n.resolve(context), this.j, this.k, this.o, this.p, this.q, this.u, this.w, this.x, this.y, this.e, this.h, this.i);
                    T t = (T) l.b(this.a, "_Impl");
                    t.u(cVar);
                    return t;
                }
                throw new IllegalArgumentException("Must provide an abstract class that extends RoomDatabase");
            }
            throw new IllegalArgumentException("Cannot provide null context for the database.");
        }

        public a<T> e() {
            this.p = false;
            this.q = true;
            return this;
        }

        public a<T> f(tw3.c cVar) {
            this.l = cVar;
            return this;
        }

        public a<T> g(Executor executor) {
            this.j = executor;
            return this;
        }
    }

    /* loaded from: classes.dex */
    public static abstract class b {
        public void a(sw3 sw3Var) {
        }

        public void b(sw3 sw3Var) {
        }

        public void c(sw3 sw3Var) {
        }
    }

    /* loaded from: classes.dex */
    public static class c {
        public HashMap<Integer, TreeMap<Integer, w82>> a = new HashMap<>();

        public final void a(w82 w82Var) {
            int i = w82Var.a;
            int i2 = w82Var.b;
            TreeMap<Integer, w82> treeMap = this.a.get(Integer.valueOf(i));
            if (treeMap == null) {
                treeMap = new TreeMap<>();
                this.a.put(Integer.valueOf(i), treeMap);
            }
            w82 w82Var2 = treeMap.get(Integer.valueOf(i2));
            if (w82Var2 != null) {
                StringBuilder sb = new StringBuilder();
                sb.append("Overriding migration ");
                sb.append(w82Var2);
                sb.append(" with ");
                sb.append(w82Var);
            }
            treeMap.put(Integer.valueOf(i2), w82Var);
        }

        public void b(w82... w82VarArr) {
            for (w82 w82Var : w82VarArr) {
                a(w82Var);
            }
        }

        public List<w82> c(int i, int i2) {
            if (i == i2) {
                return Collections.emptyList();
            }
            return d(new ArrayList(), i2 > i, i, i2);
        }

        /* JADX WARN: Removed duplicated region for block: B:31:0x0016 A[SYNTHETIC] */
        /* JADX WARN: Removed duplicated region for block: B:9:0x0017  */
        /*
            Code decompiled incorrectly, please refer to instructions dump.
            To view partially-correct code enable 'Show inconsistent code' option in preferences
        */
        public final java.util.List<defpackage.w82> d(java.util.List<defpackage.w82> r7, boolean r8, int r9, int r10) {
            /*
                r6 = this;
            L0:
                if (r8 == 0) goto L5
                if (r9 >= r10) goto L5a
                goto L7
            L5:
                if (r9 <= r10) goto L5a
            L7:
                java.util.HashMap<java.lang.Integer, java.util.TreeMap<java.lang.Integer, w82>> r0 = r6.a
                java.lang.Integer r1 = java.lang.Integer.valueOf(r9)
                java.lang.Object r0 = r0.get(r1)
                java.util.TreeMap r0 = (java.util.TreeMap) r0
                r1 = 0
                if (r0 != 0) goto L17
                return r1
            L17:
                if (r8 == 0) goto L1e
                java.util.NavigableSet r2 = r0.descendingKeySet()
                goto L22
            L1e:
                java.util.Set r2 = r0.keySet()
            L22:
                java.util.Iterator r2 = r2.iterator()
            L26:
                boolean r3 = r2.hasNext()
                r4 = 1
                r5 = 0
                if (r3 == 0) goto L56
                java.lang.Object r3 = r2.next()
                java.lang.Integer r3 = (java.lang.Integer) r3
                int r3 = r3.intValue()
                if (r8 == 0) goto L40
                if (r3 > r10) goto L45
                if (r3 <= r9) goto L45
            L3e:
                r5 = r4
                goto L45
            L40:
                if (r3 < r10) goto L45
                if (r3 >= r9) goto L45
                goto L3e
            L45:
                if (r5 == 0) goto L26
                java.lang.Integer r9 = java.lang.Integer.valueOf(r3)
                java.lang.Object r9 = r0.get(r9)
                w82 r9 = (defpackage.w82) r9
                r7.add(r9)
                r9 = r3
                goto L57
            L56:
                r4 = r5
            L57:
                if (r4 != 0) goto L0
                return r1
            L5a:
                return r7
            */
            throw new UnsupportedOperationException("Method not decompiled: androidx.room.RoomDatabase.c.d(java.util.List, boolean, int, int):java.util.List");
        }

        public Map<Integer, Map<Integer, w82>> e() {
            return Collections.unmodifiableMap(this.a);
        }
    }

    /* loaded from: classes.dex */
    public static abstract class d {
    }

    /* loaded from: classes.dex */
    public interface e {
        void a(String str, List<Object> list);
    }

    /* JADX INFO: Access modifiers changed from: private */
    public /* synthetic */ Object A(sw3 sw3Var) {
        v();
        return null;
    }

    /* JADX INFO: Access modifiers changed from: private */
    public /* synthetic */ Object B(sw3 sw3Var) {
        w();
        return null;
    }

    public static boolean y() {
        return Looper.getMainLooper().getThread() == Thread.currentThread();
    }

    public Cursor C(vw3 vw3Var) {
        return D(vw3Var, null);
    }

    public Cursor D(vw3 vw3Var, CancellationSignal cancellationSignal) {
        c();
        d();
        if (cancellationSignal != null && Build.VERSION.SDK_INT >= 16) {
            return this.d.D0().t0(vw3Var, cancellationSignal);
        }
        return this.d.D0().D(vw3Var);
    }

    @Deprecated
    public void E() {
        this.d.D0().s0();
    }

    /* JADX WARN: Multi-variable type inference failed */
    public final <T> T F(Class<T> cls, tw3 tw3Var) {
        if (cls.isInstance(tw3Var)) {
            return tw3Var;
        }
        if (tw3Var instanceof cm0) {
            return (T) F(cls, ((cm0) tw3Var).getDelegate());
        }
        return null;
    }

    public void c() {
        if (!this.f && y()) {
            throw new IllegalStateException("Cannot access database on the main thread since it may potentially lock the UI for a long period of time.");
        }
    }

    public void d() {
        if (!t() && this.k.get() != null) {
            throw new IllegalStateException("Cannot access database on a different coroutine context inherited from a suspending transaction.");
        }
    }

    @Deprecated
    public void e() {
        c();
        androidx.room.a aVar = this.j;
        if (aVar == null) {
            v();
        } else {
            aVar.c(new ud1() { // from class: h93
                @Override // defpackage.ud1
                public final Object apply(Object obj) {
                    Object A;
                    A = RoomDatabase.this.A((sw3) obj);
                    return A;
                }
            });
        }
    }

    public abstract void f();

    public ww3 g(String str) {
        c();
        d();
        return this.d.D0().U(str);
    }

    public abstract f h();

    public abstract tw3 i(androidx.room.c cVar);

    @Deprecated
    public void j() {
        androidx.room.a aVar = this.j;
        if (aVar == null) {
            w();
        } else {
            aVar.c(new ud1() { // from class: i93
                @Override // defpackage.ud1
                public final Object apply(Object obj) {
                    Object B;
                    B = RoomDatabase.this.B((sw3) obj);
                    return B;
                }
            });
        }
    }

    public List<w82> k(Map<Class<? extends fk>, fk> map) {
        return Collections.emptyList();
    }

    public Map<String, Object> l() {
        return this.l;
    }

    public Lock m() {
        return this.i.readLock();
    }

    public f n() {
        return this.e;
    }

    public tw3 o() {
        return this.d;
    }

    public Executor p() {
        return this.b;
    }

    public Set<Class<? extends fk>> q() {
        return Collections.emptySet();
    }

    public Map<Class<?>, List<Class<?>>> r() {
        return Collections.emptyMap();
    }

    public Executor s() {
        return this.c;
    }

    public boolean t() {
        return this.d.D0().h1();
    }

    public void u(androidx.room.c cVar) {
        this.d = i(cVar);
        Set<Class<? extends fk>> q = q();
        BitSet bitSet = new BitSet();
        Iterator<Class<? extends fk>> it = q.iterator();
        while (true) {
            int i = -1;
            if (it.hasNext()) {
                Class<? extends fk> next = it.next();
                int size = cVar.h.size() - 1;
                while (true) {
                    if (size < 0) {
                        break;
                    } else if (next.isAssignableFrom(cVar.h.get(size).getClass())) {
                        bitSet.set(size);
                        i = size;
                        break;
                    } else {
                        size--;
                    }
                }
                if (i >= 0) {
                    this.h.put(next, cVar.h.get(i));
                } else {
                    throw new IllegalArgumentException("A required auto migration spec (" + next.getCanonicalName() + ") is missing in the database configuration.");
                }
            } else {
                for (int size2 = cVar.h.size() - 1; size2 >= 0; size2--) {
                    if (!bitSet.get(size2)) {
                        throw new IllegalArgumentException("Unexpected auto migration specs found. Annotate AutoMigrationSpec implementation with @ProvidedAutoMigrationSpec annotation or remove this spec from the builder.");
                    }
                }
                Iterator<w82> it2 = k(this.h).iterator();
                while (true) {
                    if (!it2.hasNext()) {
                        break;
                    }
                    w82 next2 = it2.next();
                    if (!cVar.d.e().containsKey(Integer.valueOf(next2.a))) {
                        cVar.d.b(next2);
                    }
                }
                o oVar = (o) F(o.class, this.d);
                if (oVar != null) {
                    oVar.c(cVar);
                }
                androidx.room.b bVar = (androidx.room.b) F(androidx.room.b.class, this.d);
                if (bVar != null) {
                    androidx.room.a a2 = bVar.a();
                    this.j = a2;
                    this.e.m(a2);
                }
                if (Build.VERSION.SDK_INT >= 16) {
                    this.d.setWriteAheadLoggingEnabled(cVar.j == JournalMode.WRITE_AHEAD_LOGGING);
                }
                this.g = cVar.e;
                this.b = cVar.k;
                this.c = new t84(cVar.l);
                this.f = cVar.i;
                if (cVar.m) {
                    this.e.n(cVar.b, cVar.c);
                }
                Map<Class<?>, List<Class<?>>> r = r();
                BitSet bitSet2 = new BitSet();
                for (Map.Entry<Class<?>, List<Class<?>>> entry : r.entrySet()) {
                    Class<?> key = entry.getKey();
                    for (Class<?> cls : entry.getValue()) {
                        int size3 = cVar.g.size() - 1;
                        while (true) {
                            if (size3 < 0) {
                                size3 = -1;
                                break;
                            } else if (cls.isAssignableFrom(cVar.g.get(size3).getClass())) {
                                bitSet2.set(size3);
                                break;
                            } else {
                                size3--;
                            }
                        }
                        if (size3 >= 0) {
                            this.m.put(cls, cVar.g.get(size3));
                        } else {
                            throw new IllegalArgumentException("A required type converter (" + cls + ") for " + key.getCanonicalName() + " is missing in the database configuration.");
                        }
                    }
                }
                for (int size4 = cVar.g.size() - 1; size4 >= 0; size4--) {
                    if (!bitSet2.get(size4)) {
                        throw new IllegalArgumentException("Unexpected type converter " + cVar.g.get(size4) + ". Annotate TypeConverter class with @ProvidedTypeConverter annotation or remove this converter from the builder.");
                    }
                }
                return;
            }
        }
    }

    public final void v() {
        c();
        sw3 D0 = this.d.D0();
        this.e.r(D0);
        if (Build.VERSION.SDK_INT >= 16 && D0.q1()) {
            D0.v0();
        } else {
            D0.B();
        }
    }

    public final void w() {
        this.d.D0().N0();
        if (t()) {
            return;
        }
        this.e.j();
    }

    public void x(sw3 sw3Var) {
        this.e.g(sw3Var);
    }

    public boolean z() {
        androidx.room.a aVar = this.j;
        if (aVar != null) {
            return aVar.g();
        }
        sw3 sw3Var = this.a;
        return sw3Var != null && sw3Var.isOpen();
    }
}
