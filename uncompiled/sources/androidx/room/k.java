package androidx.room;

import androidx.room.RoomDatabase;
import androidx.room.k;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Executor;

/* compiled from: QueryInterceptorStatement.java */
/* loaded from: classes.dex */
public final class k implements ww3 {
    public final ww3 a;
    public final RoomDatabase.e f0;
    public final String g0;
    public final List<Object> h0 = new ArrayList();
    public final Executor i0;

    public k(ww3 ww3Var, RoomDatabase.e eVar, String str, Executor executor) {
        this.a = ww3Var;
        this.f0 = eVar;
        this.g0 = str;
        this.i0 = executor;
    }

    /* JADX INFO: Access modifiers changed from: private */
    public /* synthetic */ void c() {
        this.f0.a(this.g0, this.h0);
    }

    /* JADX INFO: Access modifiers changed from: private */
    public /* synthetic */ void d() {
        this.f0.a(this.g0, this.h0);
    }

    @Override // defpackage.uw3
    public void A0(int i, byte[] bArr) {
        e(i, bArr);
        this.a.A0(i, bArr);
    }

    @Override // defpackage.ww3
    public long C1() {
        this.i0.execute(new Runnable() { // from class: jx2
            @Override // java.lang.Runnable
            public final void run() {
                k.this.c();
            }
        });
        return this.a.C1();
    }

    @Override // defpackage.uw3
    public void L(int i, String str) {
        e(i, str);
        this.a.L(i, str);
    }

    @Override // defpackage.ww3
    public int T() {
        this.i0.execute(new Runnable() { // from class: kx2
            @Override // java.lang.Runnable
            public final void run() {
                k.this.d();
            }
        });
        return this.a.T();
    }

    @Override // defpackage.uw3
    public void Y(int i, double d) {
        e(i, Double.valueOf(d));
        this.a.Y(i, d);
    }

    @Override // defpackage.uw3
    public void Y0(int i) {
        e(i, this.h0.toArray());
        this.a.Y0(i);
    }

    @Override // java.io.Closeable, java.lang.AutoCloseable
    public void close() throws IOException {
        this.a.close();
    }

    public final void e(int i, Object obj) {
        int i2 = i - 1;
        if (i2 >= this.h0.size()) {
            for (int size = this.h0.size(); size <= i2; size++) {
                this.h0.add(null);
            }
        }
        this.h0.set(i2, obj);
    }

    @Override // defpackage.uw3
    public void q0(int i, long j) {
        e(i, Long.valueOf(j));
        this.a.q0(i, j);
    }
}
