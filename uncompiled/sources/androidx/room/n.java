package androidx.room;

import android.annotation.SuppressLint;
import androidx.lifecycle.LiveData;
import androidx.room.f;
import java.util.Set;
import java.util.concurrent.Callable;
import java.util.concurrent.Executor;
import java.util.concurrent.atomic.AtomicBoolean;

/* compiled from: RoomTrackingLiveData.java */
/* loaded from: classes.dex */
public class n<T> extends LiveData<T> {
    public final RoomDatabase a;
    public final boolean b;
    public final Callable<T> c;
    public final js1 d;
    public final f.c e;
    public final AtomicBoolean f = new AtomicBoolean(true);
    public final AtomicBoolean g = new AtomicBoolean(false);
    public final AtomicBoolean h = new AtomicBoolean(false);
    public final Runnable i = new a();
    public final Runnable j = new b();

    /* compiled from: RoomTrackingLiveData.java */
    /* loaded from: classes.dex */
    public class a implements Runnable {
        public a() {
        }

        @Override // java.lang.Runnable
        public void run() {
            boolean z;
            if (n.this.h.compareAndSet(false, true)) {
                n.this.a.n().b(n.this.e);
            }
            do {
                if (n.this.g.compareAndSet(false, true)) {
                    T t = null;
                    z = false;
                    while (n.this.f.compareAndSet(true, false)) {
                        try {
                            try {
                                t = n.this.c.call();
                                z = true;
                            } catch (Exception e) {
                                throw new RuntimeException("Exception while computing database live data.", e);
                            }
                        } finally {
                            n.this.g.set(false);
                        }
                    }
                    if (z) {
                        n.this.postValue(t);
                    }
                } else {
                    z = false;
                }
                if (!z) {
                    return;
                }
            } while (n.this.f.get());
        }
    }

    /* compiled from: RoomTrackingLiveData.java */
    /* loaded from: classes.dex */
    public class b implements Runnable {
        public b() {
        }

        @Override // java.lang.Runnable
        public void run() {
            boolean hasActiveObservers = n.this.hasActiveObservers();
            if (n.this.f.compareAndSet(false, true) && hasActiveObservers) {
                n.this.b().execute(n.this.i);
            }
        }
    }

    /* compiled from: RoomTrackingLiveData.java */
    /* loaded from: classes.dex */
    public class c extends f.c {
        public c(String[] strArr) {
            super(strArr);
        }

        @Override // androidx.room.f.c
        public void b(Set<String> set) {
            gh.f().b(n.this.j);
        }
    }

    @SuppressLint({"RestrictedApi"})
    public n(RoomDatabase roomDatabase, js1 js1Var, boolean z, Callable<T> callable, String[] strArr) {
        this.a = roomDatabase;
        this.b = z;
        this.c = callable;
        this.d = js1Var;
        this.e = new c(strArr);
    }

    public Executor b() {
        if (this.b) {
            return this.a.s();
        }
        return this.a.p();
    }

    @Override // androidx.lifecycle.LiveData
    public void onActive() {
        super.onActive();
        this.d.b(this);
        b().execute(this.i);
    }

    @Override // androidx.lifecycle.LiveData
    public void onInactive() {
        super.onInactive();
        this.d.c(this);
    }
}
