package androidx.room;

import androidx.room.RoomDatabase;
import defpackage.tw3;
import java.util.concurrent.Executor;

/* compiled from: QueryInterceptorOpenHelperFactory.java */
/* loaded from: classes.dex */
public final class j implements tw3.c {
    public final tw3.c a;
    public final RoomDatabase.e b;
    public final Executor c;

    public j(tw3.c cVar, RoomDatabase.e eVar, Executor executor) {
        this.a = cVar;
        this.b = eVar;
        this.c = executor;
    }

    @Override // defpackage.tw3.c
    public tw3 a(tw3.b bVar) {
        return new i(this.a.a(bVar), this.b, this.c);
    }
}
