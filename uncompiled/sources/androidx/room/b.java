package androidx.room;

import android.content.ContentResolver;
import android.database.CharArrayBuffer;
import android.database.ContentObserver;
import android.database.Cursor;
import android.database.DataSetObserver;
import android.database.SQLException;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.CancellationSignal;
import android.util.Pair;
import androidx.room.b;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/* compiled from: AutoClosingRoomOpenHelper.java */
/* loaded from: classes.dex */
public final class b implements tw3, cm0 {
    public final tw3 a;
    public final a f0;
    public final androidx.room.a g0;

    /* compiled from: AutoClosingRoomOpenHelper.java */
    /* loaded from: classes.dex */
    public static final class a implements sw3 {
        public final androidx.room.a a;

        public a(androidx.room.a aVar) {
            this.a = aVar;
        }

        public static /* synthetic */ Boolean g(sw3 sw3Var) {
            if (Build.VERSION.SDK_INT >= 16) {
                return Boolean.valueOf(sw3Var.q1());
            }
            return Boolean.FALSE;
        }

        public static /* synthetic */ Object h(sw3 sw3Var) {
            return null;
        }

        @Override // defpackage.sw3
        public void B() {
            try {
                this.a.e().B();
            } catch (Throwable th) {
                this.a.b();
                throw th;
            }
        }

        @Override // defpackage.sw3
        public Cursor D(vw3 vw3Var) {
            try {
                return new c(this.a.e().D(vw3Var), this.a);
            } catch (Throwable th) {
                this.a.b();
                throw th;
            }
        }

        @Override // defpackage.sw3
        public Cursor E0(String str) {
            try {
                return new c(this.a.e().E0(str), this.a);
            } catch (Throwable th) {
                this.a.b();
                throw th;
            }
        }

        @Override // defpackage.sw3
        public List<Pair<String, String>> G() {
            return (List) this.a.c(xj.a);
        }

        @Override // defpackage.sw3
        public void K(final String str) throws SQLException {
            this.a.c(new ud1() { // from class: tj
                @Override // defpackage.ud1
                public final Object apply(Object obj) {
                    Object K;
                    K = ((sw3) obj).K(str);
                    return K;
                }
            });
        }

        @Override // defpackage.sw3
        public void N0() {
            if (this.a.d() != null) {
                try {
                    this.a.d().N0();
                    return;
                } finally {
                    this.a.b();
                }
            }
            throw new IllegalStateException("End transaction called but delegateDb is null");
        }

        @Override // defpackage.sw3
        public ww3 U(String str) {
            return new C0061b(str, this.a);
        }

        @Override // java.io.Closeable, java.lang.AutoCloseable
        public void close() throws IOException {
            this.a.a();
        }

        @Override // defpackage.sw3
        public String getPath() {
            return (String) this.a.c(yj.a);
        }

        @Override // defpackage.sw3
        public boolean h1() {
            if (this.a.d() == null) {
                return false;
            }
            return ((Boolean) this.a.c(zj.a)).booleanValue();
        }

        public void i() {
            this.a.c(wj.a);
        }

        @Override // defpackage.sw3
        public boolean isOpen() {
            sw3 d = this.a.d();
            if (d == null) {
                return false;
            }
            return d.isOpen();
        }

        @Override // defpackage.sw3
        public boolean q1() {
            return ((Boolean) this.a.c(vj.a)).booleanValue();
        }

        @Override // defpackage.sw3
        public void s0() {
            sw3 d = this.a.d();
            if (d != null) {
                d.s0();
                return;
            }
            throw new IllegalStateException("setTransactionSuccessful called but delegateDb is null");
        }

        @Override // defpackage.sw3
        public Cursor t0(vw3 vw3Var, CancellationSignal cancellationSignal) {
            try {
                return new c(this.a.e().t0(vw3Var, cancellationSignal), this.a);
            } catch (Throwable th) {
                this.a.b();
                throw th;
            }
        }

        @Override // defpackage.sw3
        public void u0(final String str, final Object[] objArr) throws SQLException {
            this.a.c(new ud1() { // from class: uj
                @Override // defpackage.ud1
                public final Object apply(Object obj) {
                    Object u0;
                    u0 = ((sw3) obj).u0(str, objArr);
                    return u0;
                }
            });
        }

        @Override // defpackage.sw3
        public void v0() {
            try {
                this.a.e().v0();
            } catch (Throwable th) {
                this.a.b();
                throw th;
            }
        }
    }

    /* compiled from: AutoClosingRoomOpenHelper.java */
    /* renamed from: androidx.room.b$b  reason: collision with other inner class name */
    /* loaded from: classes.dex */
    public static class C0061b implements ww3 {
        public final String a;
        public final ArrayList<Object> f0 = new ArrayList<>();
        public final androidx.room.a g0;

        public C0061b(String str, androidx.room.a aVar) {
            this.a = str;
            this.g0 = aVar;
        }

        /* JADX INFO: Access modifiers changed from: private */
        public /* synthetic */ Object d(ud1 ud1Var, sw3 sw3Var) {
            ww3 U = sw3Var.U(this.a);
            b(U);
            return ud1Var.apply(U);
        }

        @Override // defpackage.uw3
        public void A0(int i, byte[] bArr) {
            e(i, bArr);
        }

        @Override // defpackage.ww3
        public long C1() {
            return ((Long) c(ck.a)).longValue();
        }

        @Override // defpackage.uw3
        public void L(int i, String str) {
            e(i, str);
        }

        @Override // defpackage.ww3
        public int T() {
            return ((Integer) c(bk.a)).intValue();
        }

        @Override // defpackage.uw3
        public void Y(int i, double d) {
            e(i, Double.valueOf(d));
        }

        @Override // defpackage.uw3
        public void Y0(int i) {
            e(i, null);
        }

        public final void b(ww3 ww3Var) {
            int i = 0;
            while (i < this.f0.size()) {
                int i2 = i + 1;
                Object obj = this.f0.get(i);
                if (obj == null) {
                    ww3Var.Y0(i2);
                } else if (obj instanceof Long) {
                    ww3Var.q0(i2, ((Long) obj).longValue());
                } else if (obj instanceof Double) {
                    ww3Var.Y(i2, ((Double) obj).doubleValue());
                } else if (obj instanceof String) {
                    ww3Var.L(i2, (String) obj);
                } else if (obj instanceof byte[]) {
                    ww3Var.A0(i2, (byte[]) obj);
                }
                i = i2;
            }
        }

        public final <T> T c(final ud1<ww3, T> ud1Var) {
            return (T) this.g0.c(new ud1() { // from class: ak
                @Override // defpackage.ud1
                public final Object apply(Object obj) {
                    Object d;
                    d = b.C0061b.this.d(ud1Var, (sw3) obj);
                    return d;
                }
            });
        }

        @Override // java.io.Closeable, java.lang.AutoCloseable
        public void close() throws IOException {
        }

        public final void e(int i, Object obj) {
            int i2 = i - 1;
            if (i2 >= this.f0.size()) {
                for (int size = this.f0.size(); size <= i2; size++) {
                    this.f0.add(null);
                }
            }
            this.f0.set(i2, obj);
        }

        @Override // defpackage.uw3
        public void q0(int i, long j) {
            e(i, Long.valueOf(j));
        }
    }

    /* compiled from: AutoClosingRoomOpenHelper.java */
    /* loaded from: classes.dex */
    public static final class c implements Cursor {
        public final Cursor a;
        public final androidx.room.a f0;

        public c(Cursor cursor, androidx.room.a aVar) {
            this.a = cursor;
            this.f0 = aVar;
        }

        @Override // android.database.Cursor, java.io.Closeable, java.lang.AutoCloseable
        public void close() {
            this.a.close();
            this.f0.b();
        }

        @Override // android.database.Cursor
        public void copyStringToBuffer(int i, CharArrayBuffer charArrayBuffer) {
            this.a.copyStringToBuffer(i, charArrayBuffer);
        }

        @Override // android.database.Cursor
        @Deprecated
        public void deactivate() {
            this.a.deactivate();
        }

        @Override // android.database.Cursor
        public byte[] getBlob(int i) {
            return this.a.getBlob(i);
        }

        @Override // android.database.Cursor
        public int getColumnCount() {
            return this.a.getColumnCount();
        }

        @Override // android.database.Cursor
        public int getColumnIndex(String str) {
            return this.a.getColumnIndex(str);
        }

        @Override // android.database.Cursor
        public int getColumnIndexOrThrow(String str) throws IllegalArgumentException {
            return this.a.getColumnIndexOrThrow(str);
        }

        @Override // android.database.Cursor
        public String getColumnName(int i) {
            return this.a.getColumnName(i);
        }

        @Override // android.database.Cursor
        public String[] getColumnNames() {
            return this.a.getColumnNames();
        }

        @Override // android.database.Cursor
        public int getCount() {
            return this.a.getCount();
        }

        @Override // android.database.Cursor
        public double getDouble(int i) {
            return this.a.getDouble(i);
        }

        @Override // android.database.Cursor
        public Bundle getExtras() {
            return this.a.getExtras();
        }

        @Override // android.database.Cursor
        public float getFloat(int i) {
            return this.a.getFloat(i);
        }

        @Override // android.database.Cursor
        public int getInt(int i) {
            return this.a.getInt(i);
        }

        @Override // android.database.Cursor
        public long getLong(int i) {
            return this.a.getLong(i);
        }

        @Override // android.database.Cursor
        public Uri getNotificationUri() {
            return ow3.a(this.a);
        }

        @Override // android.database.Cursor
        public List<Uri> getNotificationUris() {
            return rw3.a(this.a);
        }

        @Override // android.database.Cursor
        public int getPosition() {
            return this.a.getPosition();
        }

        @Override // android.database.Cursor
        public short getShort(int i) {
            return this.a.getShort(i);
        }

        @Override // android.database.Cursor
        public String getString(int i) {
            return this.a.getString(i);
        }

        @Override // android.database.Cursor
        public int getType(int i) {
            return this.a.getType(i);
        }

        @Override // android.database.Cursor
        public boolean getWantsAllOnMoveCalls() {
            return this.a.getWantsAllOnMoveCalls();
        }

        @Override // android.database.Cursor
        public boolean isAfterLast() {
            return this.a.isAfterLast();
        }

        @Override // android.database.Cursor
        public boolean isBeforeFirst() {
            return this.a.isBeforeFirst();
        }

        @Override // android.database.Cursor
        public boolean isClosed() {
            return this.a.isClosed();
        }

        @Override // android.database.Cursor
        public boolean isFirst() {
            return this.a.isFirst();
        }

        @Override // android.database.Cursor
        public boolean isLast() {
            return this.a.isLast();
        }

        @Override // android.database.Cursor
        public boolean isNull(int i) {
            return this.a.isNull(i);
        }

        @Override // android.database.Cursor
        public boolean move(int i) {
            return this.a.move(i);
        }

        @Override // android.database.Cursor
        public boolean moveToFirst() {
            return this.a.moveToFirst();
        }

        @Override // android.database.Cursor
        public boolean moveToLast() {
            return this.a.moveToLast();
        }

        @Override // android.database.Cursor
        public boolean moveToNext() {
            return this.a.moveToNext();
        }

        @Override // android.database.Cursor
        public boolean moveToPosition(int i) {
            return this.a.moveToPosition(i);
        }

        @Override // android.database.Cursor
        public boolean moveToPrevious() {
            return this.a.moveToPrevious();
        }

        @Override // android.database.Cursor
        public void registerContentObserver(ContentObserver contentObserver) {
            this.a.registerContentObserver(contentObserver);
        }

        @Override // android.database.Cursor
        public void registerDataSetObserver(DataSetObserver dataSetObserver) {
            this.a.registerDataSetObserver(dataSetObserver);
        }

        @Override // android.database.Cursor
        @Deprecated
        public boolean requery() {
            return this.a.requery();
        }

        @Override // android.database.Cursor
        public Bundle respond(Bundle bundle) {
            return this.a.respond(bundle);
        }

        @Override // android.database.Cursor
        public void setExtras(Bundle bundle) {
            qw3.a(this.a, bundle);
        }

        @Override // android.database.Cursor
        public void setNotificationUri(ContentResolver contentResolver, Uri uri) {
            this.a.setNotificationUri(contentResolver, uri);
        }

        @Override // android.database.Cursor
        public void setNotificationUris(ContentResolver contentResolver, List<Uri> list) {
            rw3.b(this.a, contentResolver, list);
        }

        @Override // android.database.Cursor
        public void unregisterContentObserver(ContentObserver contentObserver) {
            this.a.unregisterContentObserver(contentObserver);
        }

        @Override // android.database.Cursor
        public void unregisterDataSetObserver(DataSetObserver dataSetObserver) {
            this.a.unregisterDataSetObserver(dataSetObserver);
        }
    }

    public b(tw3 tw3Var, androidx.room.a aVar) {
        this.a = tw3Var;
        this.g0 = aVar;
        aVar.f(tw3Var);
        this.f0 = new a(aVar);
    }

    @Override // defpackage.tw3
    public sw3 D0() {
        this.f0.i();
        return this.f0;
    }

    public androidx.room.a a() {
        return this.g0;
    }

    @Override // defpackage.tw3, java.io.Closeable, java.lang.AutoCloseable
    public void close() {
        try {
            this.f0.close();
        } catch (IOException e) {
            wq3.a(e);
        }
    }

    @Override // defpackage.tw3
    public String getDatabaseName() {
        return this.a.getDatabaseName();
    }

    @Override // defpackage.cm0
    public tw3 getDelegate() {
        return this.a;
    }

    @Override // defpackage.tw3
    public void setWriteAheadLoggingEnabled(boolean z) {
        this.a.setWriteAheadLoggingEnabled(z);
    }
}
