package androidx.room;

import android.os.CancellationSignal;
import java.util.concurrent.Callable;
import kotlin.coroutines.intrinsics.IntrinsicsKt__IntrinsicsJvmKt;
import kotlinx.coroutines.CoroutineDispatcher;

/* compiled from: CoroutinesRoom.kt */
/* loaded from: classes.dex */
public final class CoroutinesRoom {
    public static final Companion a = new Companion(null);

    /* compiled from: CoroutinesRoom.kt */
    /* loaded from: classes.dex */
    public static final class Companion {
        public Companion() {
        }

        public /* synthetic */ Companion(qi0 qi0Var) {
            this();
        }

        public final <R> Object a(RoomDatabase roomDatabase, boolean z, CancellationSignal cancellationSignal, Callable<R> callable, q70<? super R> q70Var) {
            st1 b;
            if (roomDatabase.z() && roomDatabase.t()) {
                return callable.call();
            }
            s84 s84Var = (s84) q70Var.getContext().get(s84.f0);
            CoroutineDispatcher c = s84Var == null ? null : s84Var.c();
            if (c == null) {
                c = z ? f90.b(roomDatabase) : f90.a(roomDatabase);
            }
            pv pvVar = new pv(IntrinsicsKt__IntrinsicsJvmKt.c(q70Var), 1);
            pvVar.A();
            b = as.b(qg1.a, c, null, new CoroutinesRoom$Companion$execute$4$job$1(callable, pvVar, null), 2, null);
            pvVar.f(new CoroutinesRoom$Companion$execute$4$1(cancellationSignal, b));
            Object x = pvVar.x();
            if (x == gs1.d()) {
                ef0.c(q70Var);
            }
            return x;
        }

        public final <R> Object b(RoomDatabase roomDatabase, boolean z, Callable<R> callable, q70<? super R> q70Var) {
            if (roomDatabase.z() && roomDatabase.t()) {
                return callable.call();
            }
            s84 s84Var = (s84) q70Var.getContext().get(s84.f0);
            CoroutineDispatcher c = s84Var == null ? null : s84Var.c();
            if (c == null) {
                c = z ? f90.b(roomDatabase) : f90.a(roomDatabase);
            }
            return kotlinx.coroutines.a.e(c, new CoroutinesRoom$Companion$execute$2(callable, null), q70Var);
        }
    }

    public static final <R> Object a(RoomDatabase roomDatabase, boolean z, CancellationSignal cancellationSignal, Callable<R> callable, q70<? super R> q70Var) {
        return a.a(roomDatabase, z, cancellationSignal, callable, q70Var);
    }

    public static final <R> Object b(RoomDatabase roomDatabase, boolean z, Callable<R> callable, q70<? super R> q70Var) {
        return a.b(roomDatabase, z, callable, q70Var);
    }
}
