package androidx.room;

import android.os.Handler;
import android.os.Looper;
import android.os.SystemClock;
import java.io.IOException;
import java.util.concurrent.Executor;
import java.util.concurrent.TimeUnit;

/* compiled from: AutoCloser.java */
/* loaded from: classes.dex */
public final class a {
    public final long e;
    public final Executor f;
    public sw3 i;
    public tw3 a = null;
    public final Handler b = new Handler(Looper.getMainLooper());
    public Runnable c = null;
    public final Object d = new Object();
    public int g = 0;
    public long h = SystemClock.uptimeMillis();
    public boolean j = false;
    public final Runnable k = new RunnableC0060a();
    public final Runnable l = new b();

    /* compiled from: AutoCloser.java */
    /* renamed from: androidx.room.a$a  reason: collision with other inner class name */
    /* loaded from: classes.dex */
    public class RunnableC0060a implements Runnable {
        public RunnableC0060a() {
        }

        @Override // java.lang.Runnable
        public void run() {
            a aVar = a.this;
            aVar.f.execute(aVar.l);
        }
    }

    /* compiled from: AutoCloser.java */
    /* loaded from: classes.dex */
    public class b implements Runnable {
        public b() {
        }

        @Override // java.lang.Runnable
        public void run() {
            synchronized (a.this.d) {
                long uptimeMillis = SystemClock.uptimeMillis();
                a aVar = a.this;
                if (uptimeMillis - aVar.h < aVar.e) {
                    return;
                }
                if (aVar.g != 0) {
                    return;
                }
                Runnable runnable = aVar.c;
                if (runnable != null) {
                    runnable.run();
                    sw3 sw3Var = a.this.i;
                    if (sw3Var != null && sw3Var.isOpen()) {
                        try {
                            a.this.i.close();
                        } catch (IOException e) {
                            wq3.a(e);
                        }
                        a.this.i = null;
                    }
                    return;
                }
                throw new IllegalStateException("mOnAutoCloseCallback is null but it should have been set before use. Please file a bug against Room at: https://issuetracker.google.com/issues/new?component=413107&template=1096568");
            }
        }
    }

    public a(long j, TimeUnit timeUnit, Executor executor) {
        this.e = timeUnit.toMillis(j);
        this.f = executor;
    }

    public void a() throws IOException {
        synchronized (this.d) {
            this.j = true;
            sw3 sw3Var = this.i;
            if (sw3Var != null) {
                sw3Var.close();
            }
            this.i = null;
        }
    }

    public void b() {
        synchronized (this.d) {
            int i = this.g;
            if (i > 0) {
                int i2 = i - 1;
                this.g = i2;
                if (i2 == 0) {
                    if (this.i == null) {
                        return;
                    }
                    this.b.postDelayed(this.k, this.e);
                }
                return;
            }
            throw new IllegalStateException("ref count is 0 or lower but we're supposed to decrement");
        }
    }

    public <V> V c(ud1<sw3, V> ud1Var) {
        try {
            return ud1Var.apply(e());
        } finally {
            b();
        }
    }

    public sw3 d() {
        sw3 sw3Var;
        synchronized (this.d) {
            sw3Var = this.i;
        }
        return sw3Var;
    }

    public sw3 e() {
        synchronized (this.d) {
            this.b.removeCallbacks(this.k);
            this.g++;
            if (!this.j) {
                sw3 sw3Var = this.i;
                if (sw3Var != null && sw3Var.isOpen()) {
                    return this.i;
                }
                tw3 tw3Var = this.a;
                if (tw3Var != null) {
                    sw3 D0 = tw3Var.D0();
                    this.i = D0;
                    return D0;
                }
                throw new IllegalStateException("AutoCloser has not been initialized. Please file a bug against Room at: https://issuetracker.google.com/issues/new?component=413107&template=1096568");
            }
            throw new IllegalStateException("Attempting to open already closed database.");
        }
    }

    public void f(tw3 tw3Var) {
        if (this.a != null) {
            return;
        }
        this.a = tw3Var;
    }

    public boolean g() {
        return !this.j;
    }

    public void h(Runnable runnable) {
        this.c = runnable;
    }
}
