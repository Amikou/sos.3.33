package androidx.room;

import android.database.Cursor;
import android.database.SQLException;
import android.os.CancellationSignal;
import android.util.Pair;
import androidx.room.RoomDatabase;
import androidx.room.h;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.concurrent.Executor;

/* compiled from: QueryInterceptorDatabase.java */
/* loaded from: classes.dex */
public final class h implements sw3 {
    public final sw3 a;
    public final RoomDatabase.e f0;
    public final Executor g0;

    public h(sw3 sw3Var, RoomDatabase.e eVar, Executor executor) {
        this.a = sw3Var;
        this.f0 = eVar;
        this.g0 = executor;
    }

    /* JADX INFO: Access modifiers changed from: private */
    public /* synthetic */ void j() {
        this.f0.a("BEGIN EXCLUSIVE TRANSACTION", Collections.emptyList());
    }

    /* JADX INFO: Access modifiers changed from: private */
    public /* synthetic */ void l() {
        this.f0.a("BEGIN DEFERRED TRANSACTION", Collections.emptyList());
    }

    /* JADX INFO: Access modifiers changed from: private */
    public /* synthetic */ void m() {
        this.f0.a("END TRANSACTION", Collections.emptyList());
    }

    /* JADX INFO: Access modifiers changed from: private */
    public /* synthetic */ void n(String str) {
        this.f0.a(str, new ArrayList(0));
    }

    /* JADX INFO: Access modifiers changed from: private */
    public /* synthetic */ void q(String str, List list) {
        this.f0.a(str, list);
    }

    /* JADX INFO: Access modifiers changed from: private */
    public /* synthetic */ void r(String str) {
        this.f0.a(str, Collections.emptyList());
    }

    /* JADX INFO: Access modifiers changed from: private */
    public /* synthetic */ void u(vw3 vw3Var, ix2 ix2Var) {
        this.f0.a(vw3Var.a(), ix2Var.a());
    }

    /* JADX INFO: Access modifiers changed from: private */
    public /* synthetic */ void v(vw3 vw3Var, ix2 ix2Var) {
        this.f0.a(vw3Var.a(), ix2Var.a());
    }

    /* JADX INFO: Access modifiers changed from: private */
    public /* synthetic */ void w() {
        this.f0.a("TRANSACTION SUCCESSFUL", Collections.emptyList());
    }

    @Override // defpackage.sw3
    public void B() {
        this.g0.execute(new Runnable() { // from class: cx2
            @Override // java.lang.Runnable
            public final void run() {
                h.this.j();
            }
        });
        this.a.B();
    }

    @Override // defpackage.sw3
    public Cursor D(final vw3 vw3Var) {
        final ix2 ix2Var = new ix2();
        vw3Var.b(ix2Var);
        this.g0.execute(new Runnable() { // from class: dx2
            @Override // java.lang.Runnable
            public final void run() {
                h.this.u(vw3Var, ix2Var);
            }
        });
        return this.a.D(vw3Var);
    }

    @Override // defpackage.sw3
    public Cursor E0(final String str) {
        this.g0.execute(new Runnable() { // from class: gx2
            @Override // java.lang.Runnable
            public final void run() {
                h.this.r(str);
            }
        });
        return this.a.E0(str);
    }

    @Override // defpackage.sw3
    public List<Pair<String, String>> G() {
        return this.a.G();
    }

    @Override // defpackage.sw3
    public void K(final String str) throws SQLException {
        this.g0.execute(new Runnable() { // from class: fx2
            @Override // java.lang.Runnable
            public final void run() {
                h.this.n(str);
            }
        });
        this.a.K(str);
    }

    @Override // defpackage.sw3
    public void N0() {
        this.g0.execute(new Runnable() { // from class: zw2
            @Override // java.lang.Runnable
            public final void run() {
                h.this.m();
            }
        });
        this.a.N0();
    }

    @Override // defpackage.sw3
    public ww3 U(String str) {
        return new k(this.a.U(str), this.f0, str, this.g0);
    }

    @Override // java.io.Closeable, java.lang.AutoCloseable
    public void close() throws IOException {
        this.a.close();
    }

    @Override // defpackage.sw3
    public String getPath() {
        return this.a.getPath();
    }

    @Override // defpackage.sw3
    public boolean h1() {
        return this.a.h1();
    }

    @Override // defpackage.sw3
    public boolean isOpen() {
        return this.a.isOpen();
    }

    @Override // defpackage.sw3
    public boolean q1() {
        return this.a.q1();
    }

    @Override // defpackage.sw3
    public void s0() {
        this.g0.execute(new Runnable() { // from class: bx2
            @Override // java.lang.Runnable
            public final void run() {
                h.this.w();
            }
        });
        this.a.s0();
    }

    @Override // defpackage.sw3
    public Cursor t0(final vw3 vw3Var, CancellationSignal cancellationSignal) {
        final ix2 ix2Var = new ix2();
        vw3Var.b(ix2Var);
        this.g0.execute(new Runnable() { // from class: ex2
            @Override // java.lang.Runnable
            public final void run() {
                h.this.v(vw3Var, ix2Var);
            }
        });
        return this.a.D(vw3Var);
    }

    @Override // defpackage.sw3
    public void u0(final String str, Object[] objArr) throws SQLException {
        final ArrayList arrayList = new ArrayList();
        arrayList.addAll(Arrays.asList(objArr));
        this.g0.execute(new Runnable() { // from class: hx2
            @Override // java.lang.Runnable
            public final void run() {
                h.this.q(str, arrayList);
            }
        });
        this.a.u0(str, arrayList.toArray());
    }

    @Override // defpackage.sw3
    public void v0() {
        this.g0.execute(new Runnable() { // from class: ax2
            @Override // java.lang.Runnable
            public final void run() {
                h.this.l();
            }
        });
        this.a.v0();
    }
}
