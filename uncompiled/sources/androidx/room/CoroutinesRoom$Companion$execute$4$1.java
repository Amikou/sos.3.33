package androidx.room;

import android.os.Build;
import android.os.CancellationSignal;
import defpackage.st1;
import kotlin.jvm.internal.Lambda;

/* compiled from: CoroutinesRoom.kt */
/* loaded from: classes.dex */
public final class CoroutinesRoom$Companion$execute$4$1 extends Lambda implements tc1<Throwable, te4> {
    public final /* synthetic */ CancellationSignal $cancellationSignal;
    public final /* synthetic */ st1 $job;

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public CoroutinesRoom$Companion$execute$4$1(CancellationSignal cancellationSignal, st1 st1Var) {
        super(1);
        this.$cancellationSignal = cancellationSignal;
        this.$job = st1Var;
    }

    @Override // defpackage.tc1
    public /* bridge */ /* synthetic */ te4 invoke(Throwable th) {
        invoke2(th);
        return te4.a;
    }

    /* renamed from: invoke  reason: avoid collision after fix types in other method */
    public final void invoke2(Throwable th) {
        if (Build.VERSION.SDK_INT >= 16) {
            nw3.a(this.$cancellationSignal);
        }
        st1.a.a(this.$job, null, 1, null);
    }
}
