package androidx.room;

import android.annotation.SuppressLint;
import android.content.Context;
import androidx.room.RoomDatabase;
import defpackage.tw3;
import java.io.File;
import java.io.InputStream;
import java.util.Collections;
import java.util.List;
import java.util.Set;
import java.util.concurrent.Callable;
import java.util.concurrent.Executor;

/* compiled from: DatabaseConfiguration.java */
/* loaded from: classes.dex */
public class c {
    public final tw3.c a;
    public final Context b;
    public final String c;
    public final RoomDatabase.c d;
    public final List<RoomDatabase.b> e;
    public final RoomDatabase.d f;
    public final List<Object> g;
    public final List<fk> h;
    public final boolean i;
    public final RoomDatabase.JournalMode j;
    public final Executor k;
    public final Executor l;
    public final boolean m;
    public final boolean n;
    public final boolean o;
    public final Set<Integer> p;
    public final Callable<InputStream> q;

    @SuppressLint({"LambdaLast"})
    public c(Context context, String str, tw3.c cVar, RoomDatabase.c cVar2, List<RoomDatabase.b> list, boolean z, RoomDatabase.JournalMode journalMode, Executor executor, Executor executor2, boolean z2, boolean z3, boolean z4, Set<Integer> set, String str2, File file, Callable<InputStream> callable, RoomDatabase.d dVar, List<Object> list2, List<fk> list3) {
        this.a = cVar;
        this.b = context;
        this.c = str;
        this.d = cVar2;
        this.e = list;
        this.i = z;
        this.j = journalMode;
        this.k = executor;
        this.l = executor2;
        this.m = z2;
        this.n = z3;
        this.o = z4;
        this.p = set;
        this.q = callable;
        this.g = list2 == null ? Collections.emptyList() : list2;
        this.h = list3 == null ? Collections.emptyList() : list3;
    }

    public boolean a(int i, int i2) {
        Set<Integer> set;
        if ((i > i2) && this.o) {
            return false;
        }
        return this.n && ((set = this.p) == null || !set.contains(Integer.valueOf(i)));
    }
}
