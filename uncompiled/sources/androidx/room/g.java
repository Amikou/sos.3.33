package androidx.room;

import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.os.IBinder;
import android.os.RemoteException;
import androidx.room.d;
import androidx.room.e;
import androidx.room.f;
import java.util.Set;
import java.util.concurrent.Executor;
import java.util.concurrent.atomic.AtomicBoolean;

/* compiled from: MultiInstanceInvalidationClient.java */
/* loaded from: classes.dex */
public class g {
    public final Context a;
    public final String b;
    public int c;
    public final f d;
    public final f.c e;
    public androidx.room.e f;
    public final Executor g;
    public final androidx.room.d h = new a();
    public final AtomicBoolean i = new AtomicBoolean(false);
    public final ServiceConnection j;
    public final Runnable k;
    public final Runnable l;

    /* compiled from: MultiInstanceInvalidationClient.java */
    /* loaded from: classes.dex */
    public class a extends d.a {

        /* compiled from: MultiInstanceInvalidationClient.java */
        /* renamed from: androidx.room.g$a$a  reason: collision with other inner class name */
        /* loaded from: classes.dex */
        public class RunnableC0064a implements Runnable {
            public final /* synthetic */ String[] a;

            public RunnableC0064a(String[] strArr) {
                this.a = strArr;
            }

            @Override // java.lang.Runnable
            public void run() {
                g.this.d.h(this.a);
            }
        }

        public a() {
        }

        @Override // androidx.room.d
        public void z(String[] strArr) {
            g.this.g.execute(new RunnableC0064a(strArr));
        }
    }

    /* compiled from: MultiInstanceInvalidationClient.java */
    /* loaded from: classes.dex */
    public class b implements ServiceConnection {
        public b() {
        }

        @Override // android.content.ServiceConnection
        public void onServiceConnected(ComponentName componentName, IBinder iBinder) {
            g.this.f = e.a.b(iBinder);
            g gVar = g.this;
            gVar.g.execute(gVar.k);
        }

        @Override // android.content.ServiceConnection
        public void onServiceDisconnected(ComponentName componentName) {
            g gVar = g.this;
            gVar.g.execute(gVar.l);
            g.this.f = null;
        }
    }

    /* compiled from: MultiInstanceInvalidationClient.java */
    /* loaded from: classes.dex */
    public class c implements Runnable {
        public c() {
        }

        @Override // java.lang.Runnable
        public void run() {
            try {
                g gVar = g.this;
                androidx.room.e eVar = gVar.f;
                if (eVar != null) {
                    gVar.c = eVar.K(gVar.h, gVar.b);
                    g gVar2 = g.this;
                    gVar2.d.a(gVar2.e);
                }
            } catch (RemoteException unused) {
            }
        }
    }

    /* compiled from: MultiInstanceInvalidationClient.java */
    /* loaded from: classes.dex */
    public class d implements Runnable {
        public d() {
        }

        @Override // java.lang.Runnable
        public void run() {
            g gVar = g.this;
            gVar.d.k(gVar.e);
        }
    }

    /* compiled from: MultiInstanceInvalidationClient.java */
    /* loaded from: classes.dex */
    public class e extends f.c {
        public e(String[] strArr) {
            super(strArr);
        }

        @Override // androidx.room.f.c
        public boolean a() {
            return true;
        }

        @Override // androidx.room.f.c
        public void b(Set<String> set) {
            if (g.this.i.get()) {
                return;
            }
            try {
                g gVar = g.this;
                androidx.room.e eVar = gVar.f;
                if (eVar != null) {
                    eVar.m1(gVar.c, (String[]) set.toArray(new String[0]));
                }
            } catch (RemoteException unused) {
            }
        }
    }

    public g(Context context, String str, f fVar, Executor executor) {
        b bVar = new b();
        this.j = bVar;
        this.k = new c();
        this.l = new d();
        Context applicationContext = context.getApplicationContext();
        this.a = applicationContext;
        this.b = str;
        this.d = fVar;
        this.g = executor;
        this.e = new e((String[]) fVar.a.keySet().toArray(new String[0]));
        applicationContext.bindService(new Intent(applicationContext, MultiInstanceInvalidationService.class), bVar, 1);
    }
}
