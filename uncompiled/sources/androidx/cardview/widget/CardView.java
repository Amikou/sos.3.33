package androidx.cardview.widget;

import android.content.Context;
import android.content.res.ColorStateList;
import android.content.res.TypedArray;
import android.graphics.Color;
import android.graphics.Rect;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.util.AttributeSet;
import android.view.View;
import android.widget.FrameLayout;
import com.github.mikephil.charting.utils.Utils;

/* loaded from: classes.dex */
public class CardView extends FrameLayout {
    public static final int[] l0 = {16842801};
    public static final ew m0;
    public boolean a;
    public boolean f0;
    public int g0;
    public int h0;
    public final Rect i0;
    public final Rect j0;
    public final dw k0;

    /* loaded from: classes.dex */
    public class a implements dw {
        public Drawable a;

        public a() {
        }

        @Override // defpackage.dw
        public void a(int i, int i2, int i3, int i4) {
            CardView.this.j0.set(i, i2, i3, i4);
            CardView cardView = CardView.this;
            Rect rect = cardView.i0;
            CardView.super.setPadding(i + rect.left, i2 + rect.top, i3 + rect.right, i4 + rect.bottom);
        }

        @Override // defpackage.dw
        public void b(int i, int i2) {
            CardView cardView = CardView.this;
            if (i > cardView.g0) {
                CardView.super.setMinimumWidth(i);
            }
            CardView cardView2 = CardView.this;
            if (i2 > cardView2.h0) {
                CardView.super.setMinimumHeight(i2);
            }
        }

        @Override // defpackage.dw
        public void c(Drawable drawable) {
            this.a = drawable;
            CardView.this.setBackgroundDrawable(drawable);
        }

        @Override // defpackage.dw
        public boolean d() {
            return CardView.this.getPreventCornerOverlap();
        }

        @Override // defpackage.dw
        public boolean e() {
            return CardView.this.getUseCompatPadding();
        }

        @Override // defpackage.dw
        public Drawable f() {
            return this.a;
        }

        @Override // defpackage.dw
        public View g() {
            return CardView.this;
        }
    }

    static {
        int i = Build.VERSION.SDK_INT;
        if (i >= 21) {
            m0 = new bw();
        } else if (i >= 17) {
            m0 = new aw();
        } else {
            m0 = new cw();
        }
        m0.l();
    }

    public CardView(Context context) {
        this(context, null);
    }

    public ColorStateList getCardBackgroundColor() {
        return m0.f(this.k0);
    }

    public float getCardElevation() {
        return m0.k(this.k0);
    }

    public int getContentPaddingBottom() {
        return this.i0.bottom;
    }

    public int getContentPaddingLeft() {
        return this.i0.left;
    }

    public int getContentPaddingRight() {
        return this.i0.right;
    }

    public int getContentPaddingTop() {
        return this.i0.top;
    }

    public float getMaxCardElevation() {
        return m0.i(this.k0);
    }

    public boolean getPreventCornerOverlap() {
        return this.f0;
    }

    public float getRadius() {
        return m0.e(this.k0);
    }

    public boolean getUseCompatPadding() {
        return this.a;
    }

    @Override // android.widget.FrameLayout, android.view.View
    public void onMeasure(int i, int i2) {
        ew ewVar = m0;
        if (!(ewVar instanceof bw)) {
            int mode = View.MeasureSpec.getMode(i);
            if (mode == Integer.MIN_VALUE || mode == 1073741824) {
                i = View.MeasureSpec.makeMeasureSpec(Math.max((int) Math.ceil(ewVar.b(this.k0)), View.MeasureSpec.getSize(i)), mode);
            }
            int mode2 = View.MeasureSpec.getMode(i2);
            if (mode2 == Integer.MIN_VALUE || mode2 == 1073741824) {
                i2 = View.MeasureSpec.makeMeasureSpec(Math.max((int) Math.ceil(ewVar.g(this.k0)), View.MeasureSpec.getSize(i2)), mode2);
            }
            super.onMeasure(i, i2);
            return;
        }
        super.onMeasure(i, i2);
    }

    public void setCardBackgroundColor(int i) {
        m0.n(this.k0, ColorStateList.valueOf(i));
    }

    public void setCardElevation(float f) {
        m0.h(this.k0, f);
    }

    public void setContentPadding(int i, int i2, int i3, int i4) {
        this.i0.set(i, i2, i3, i4);
        m0.d(this.k0);
    }

    public void setMaxCardElevation(float f) {
        m0.o(this.k0, f);
    }

    @Override // android.view.View
    public void setMinimumHeight(int i) {
        this.h0 = i;
        super.setMinimumHeight(i);
    }

    @Override // android.view.View
    public void setMinimumWidth(int i) {
        this.g0 = i;
        super.setMinimumWidth(i);
    }

    @Override // android.view.View
    public void setPadding(int i, int i2, int i3, int i4) {
    }

    @Override // android.view.View
    public void setPaddingRelative(int i, int i2, int i3, int i4) {
    }

    public void setPreventCornerOverlap(boolean z) {
        if (z != this.f0) {
            this.f0 = z;
            m0.j(this.k0);
        }
    }

    public void setRadius(float f) {
        m0.a(this.k0, f);
    }

    public void setUseCompatPadding(boolean z) {
        if (this.a != z) {
            this.a = z;
            m0.c(this.k0);
        }
    }

    public CardView(Context context, AttributeSet attributeSet) {
        this(context, attributeSet, by2.cardViewStyle);
    }

    public void setCardBackgroundColor(ColorStateList colorStateList) {
        m0.n(this.k0, colorStateList);
    }

    public CardView(Context context, AttributeSet attributeSet, int i) {
        super(context, attributeSet, i);
        int color;
        ColorStateList valueOf;
        Rect rect = new Rect();
        this.i0 = rect;
        this.j0 = new Rect();
        a aVar = new a();
        this.k0 = aVar;
        TypedArray obtainStyledAttributes = context.obtainStyledAttributes(attributeSet, v23.CardView, i, v13.CardView);
        int i2 = v23.CardView_cardBackgroundColor;
        if (obtainStyledAttributes.hasValue(i2)) {
            valueOf = obtainStyledAttributes.getColorStateList(i2);
        } else {
            TypedArray obtainStyledAttributes2 = getContext().obtainStyledAttributes(l0);
            int color2 = obtainStyledAttributes2.getColor(0, 0);
            obtainStyledAttributes2.recycle();
            float[] fArr = new float[3];
            Color.colorToHSV(color2, fArr);
            if (fArr[2] > 0.5f) {
                color = getResources().getColor(py2.cardview_light_background);
            } else {
                color = getResources().getColor(py2.cardview_dark_background);
            }
            valueOf = ColorStateList.valueOf(color);
        }
        ColorStateList colorStateList = valueOf;
        float dimension = obtainStyledAttributes.getDimension(v23.CardView_cardCornerRadius, Utils.FLOAT_EPSILON);
        float dimension2 = obtainStyledAttributes.getDimension(v23.CardView_cardElevation, Utils.FLOAT_EPSILON);
        float dimension3 = obtainStyledAttributes.getDimension(v23.CardView_cardMaxElevation, Utils.FLOAT_EPSILON);
        this.a = obtainStyledAttributes.getBoolean(v23.CardView_cardUseCompatPadding, false);
        this.f0 = obtainStyledAttributes.getBoolean(v23.CardView_cardPreventCornerOverlap, true);
        int dimensionPixelSize = obtainStyledAttributes.getDimensionPixelSize(v23.CardView_contentPadding, 0);
        rect.left = obtainStyledAttributes.getDimensionPixelSize(v23.CardView_contentPaddingLeft, dimensionPixelSize);
        rect.top = obtainStyledAttributes.getDimensionPixelSize(v23.CardView_contentPaddingTop, dimensionPixelSize);
        rect.right = obtainStyledAttributes.getDimensionPixelSize(v23.CardView_contentPaddingRight, dimensionPixelSize);
        rect.bottom = obtainStyledAttributes.getDimensionPixelSize(v23.CardView_contentPaddingBottom, dimensionPixelSize);
        float f = dimension2 > dimension3 ? dimension2 : dimension3;
        this.g0 = obtainStyledAttributes.getDimensionPixelSize(v23.CardView_android_minWidth, 0);
        this.h0 = obtainStyledAttributes.getDimensionPixelSize(v23.CardView_android_minHeight, 0);
        obtainStyledAttributes.recycle();
        m0.m(aVar, context, colorStateList, dimension, dimension2, f);
    }
}
