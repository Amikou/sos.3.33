package androidx.media3.exoplayer.mediacodec;

import android.media.MediaCodec;
import android.media.MediaCrypto;
import android.media.MediaFormat;
import android.os.Bundle;
import android.os.Handler;
import android.os.HandlerThread;
import android.view.Surface;
import androidx.media3.exoplayer.mediacodec.a;
import androidx.media3.exoplayer.mediacodec.d;
import java.io.IOException;
import java.nio.ByteBuffer;

/* compiled from: AsynchronousMediaCodecAdapter.java */
/* loaded from: classes.dex */
public final class a implements d {
    public final MediaCodec a;
    public final yi b;
    public final androidx.media3.exoplayer.mediacodec.b c;
    public final boolean d;
    public boolean e;
    public int f;

    /* compiled from: AsynchronousMediaCodecAdapter.java */
    /* loaded from: classes.dex */
    public static final class b implements d.b {
        public final dw3<HandlerThread> a;
        public final dw3<HandlerThread> b;
        public final boolean c;

        public b(final int i, boolean z) {
            this(new dw3() { // from class: vi
                @Override // defpackage.dw3
                public final Object get() {
                    HandlerThread e;
                    e = a.b.e(i);
                    return e;
                }
            }, new dw3() { // from class: wi
                @Override // defpackage.dw3
                public final Object get() {
                    HandlerThread f;
                    f = a.b.f(i);
                    return f;
                }
            }, z);
        }

        public static /* synthetic */ HandlerThread e(int i) {
            return new HandlerThread(a.t(i));
        }

        public static /* synthetic */ HandlerThread f(int i) {
            return new HandlerThread(a.u(i));
        }

        @Override // androidx.media3.exoplayer.mediacodec.d.b
        /* renamed from: d */
        public a a(d.a aVar) throws IOException {
            MediaCodec mediaCodec;
            String str = aVar.a.a;
            a aVar2 = null;
            try {
                v74.a("createCodec:" + str);
                mediaCodec = MediaCodec.createByCodecName(str);
                try {
                    a aVar3 = new a(mediaCodec, this.a.get(), this.b.get(), this.c);
                    try {
                        v74.c();
                        aVar3.w(aVar.b, aVar.d, aVar.e, aVar.f);
                        return aVar3;
                    } catch (Exception e) {
                        e = e;
                        aVar2 = aVar3;
                        if (aVar2 != null) {
                            aVar2.a();
                        } else if (mediaCodec != null) {
                            mediaCodec.release();
                        }
                        throw e;
                    }
                } catch (Exception e2) {
                    e = e2;
                }
            } catch (Exception e3) {
                e = e3;
                mediaCodec = null;
            }
        }

        public b(dw3<HandlerThread> dw3Var, dw3<HandlerThread> dw3Var2, boolean z) {
            this.a = dw3Var;
            this.b = dw3Var2;
            this.c = z;
        }
    }

    public static String t(int i) {
        return v(i, "ExoPlayer:MediaCodecAsyncAdapter:");
    }

    public static String u(int i) {
        return v(i, "ExoPlayer:MediaCodecQueueingThread:");
    }

    public static String v(int i, String str) {
        StringBuilder sb = new StringBuilder(str);
        if (i == 1) {
            sb.append("Audio");
        } else if (i == 2) {
            sb.append("Video");
        } else {
            sb.append("Unknown(");
            sb.append(i);
            sb.append(")");
        }
        return sb.toString();
    }

    /* JADX INFO: Access modifiers changed from: private */
    public /* synthetic */ void x(d.c cVar, MediaCodec mediaCodec, long j, long j2) {
        cVar.a(this, j, j2);
    }

    @Override // androidx.media3.exoplayer.mediacodec.d
    public void a() {
        try {
            if (this.f == 1) {
                this.c.p();
                this.b.o();
            }
            this.f = 2;
        } finally {
            if (!this.e) {
                this.a.release();
                this.e = true;
            }
        }
    }

    @Override // androidx.media3.exoplayer.mediacodec.d
    public boolean b() {
        return false;
    }

    @Override // androidx.media3.exoplayer.mediacodec.d
    public void c(final d.c cVar, Handler handler) {
        y();
        this.a.setOnFrameRenderedListener(new MediaCodec.OnFrameRenderedListener() { // from class: ui
            @Override // android.media.MediaCodec.OnFrameRenderedListener
            public final void onFrameRendered(MediaCodec mediaCodec, long j, long j2) {
                a.this.x(cVar, mediaCodec, j, j2);
            }
        }, handler);
    }

    @Override // androidx.media3.exoplayer.mediacodec.d
    public void d(int i, int i2, ra0 ra0Var, long j, int i3) {
        this.c.n(i, i2, ra0Var, j, i3);
    }

    @Override // androidx.media3.exoplayer.mediacodec.d
    public MediaFormat e() {
        return this.b.g();
    }

    @Override // androidx.media3.exoplayer.mediacodec.d
    public void f(Bundle bundle) {
        y();
        this.a.setParameters(bundle);
    }

    @Override // androidx.media3.exoplayer.mediacodec.d
    public void flush() {
        this.c.i();
        this.a.flush();
        this.b.e();
        this.a.start();
    }

    @Override // androidx.media3.exoplayer.mediacodec.d
    public void g(int i, long j) {
        this.a.releaseOutputBuffer(i, j);
    }

    @Override // androidx.media3.exoplayer.mediacodec.d
    public int h() {
        return this.b.c();
    }

    @Override // androidx.media3.exoplayer.mediacodec.d
    public int i(MediaCodec.BufferInfo bufferInfo) {
        return this.b.d(bufferInfo);
    }

    @Override // androidx.media3.exoplayer.mediacodec.d
    public void j(int i, boolean z) {
        this.a.releaseOutputBuffer(i, z);
    }

    @Override // androidx.media3.exoplayer.mediacodec.d
    public void k(int i) {
        y();
        this.a.setVideoScalingMode(i);
    }

    @Override // androidx.media3.exoplayer.mediacodec.d
    public ByteBuffer l(int i) {
        return this.a.getInputBuffer(i);
    }

    @Override // androidx.media3.exoplayer.mediacodec.d
    public void m(Surface surface) {
        y();
        this.a.setOutputSurface(surface);
    }

    @Override // androidx.media3.exoplayer.mediacodec.d
    public void n(int i, int i2, int i3, long j, int i4) {
        this.c.m(i, i2, i3, j, i4);
    }

    @Override // androidx.media3.exoplayer.mediacodec.d
    public ByteBuffer o(int i) {
        return this.a.getOutputBuffer(i);
    }

    public final void w(MediaFormat mediaFormat, Surface surface, MediaCrypto mediaCrypto, int i) {
        this.b.h(this.a);
        v74.a("configureCodec");
        this.a.configure(mediaFormat, surface, mediaCrypto, i);
        v74.c();
        this.c.q();
        v74.a("startCodec");
        this.a.start();
        v74.c();
        this.f = 1;
    }

    public final void y() {
        if (this.d) {
            try {
                this.c.r();
            } catch (InterruptedException e) {
                Thread.currentThread().interrupt();
                throw new IllegalStateException(e);
            }
        }
    }

    public a(MediaCodec mediaCodec, HandlerThread handlerThread, HandlerThread handlerThread2, boolean z) {
        this.a = mediaCodec;
        this.b = new yi(handlerThread);
        this.c = new androidx.media3.exoplayer.mediacodec.b(mediaCodec, handlerThread2);
        this.d = z;
        this.f = 0;
    }
}
