package androidx.media3.exoplayer.mediacodec;

import androidx.media3.exoplayer.mediacodec.a;
import androidx.media3.exoplayer.mediacodec.d;
import androidx.media3.exoplayer.mediacodec.g;
import java.io.IOException;

/* compiled from: DefaultMediaCodecAdapterFactory.java */
/* loaded from: classes.dex */
public final class c implements d.b {
    public int a = 0;
    public boolean b;

    @Override // androidx.media3.exoplayer.mediacodec.d.b
    public d a(d.a aVar) throws IOException {
        int i;
        int i2 = androidx.media3.common.util.b.a;
        if (i2 >= 23 && ((i = this.a) == 1 || (i == 0 && i2 >= 31))) {
            int i3 = y82.i(aVar.c.p0);
            p12.f("DMCodecAdapterFactory", "Creating an asynchronous MediaCodec adapter for track type " + androidx.media3.common.util.b.i0(i3));
            return new a.b(i3, this.b).a(aVar);
        }
        return new g.b().a(aVar);
    }
}
