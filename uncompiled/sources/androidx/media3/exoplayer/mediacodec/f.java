package androidx.media3.exoplayer.mediacodec;

import androidx.media3.exoplayer.mediacodec.MediaCodecUtil;
import java.util.List;

/* compiled from: MediaCodecSelector.java */
/* loaded from: classes.dex */
public interface f {
    public static final f a = v52.b;

    List<e> a(String str, boolean z, boolean z2) throws MediaCodecUtil.DecoderQueryException;
}
