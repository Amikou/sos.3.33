package androidx.media3.exoplayer.mediacodec;

import android.graphics.Point;
import android.media.MediaCodecInfo;
import android.util.Pair;
import androidx.media3.common.j;
import androidx.media3.exoplayer.mediacodec.MediaCodecUtil;
import androidx.recyclerview.widget.RecyclerView;
import okhttp3.internal.http2.Http2;

/* compiled from: MediaCodecInfo.java */
/* loaded from: classes.dex */
public final class e {
    public final String a;
    public final String b;
    public final String c;
    public final MediaCodecInfo.CodecCapabilities d;
    public final boolean e;
    public final boolean f;
    public final boolean g;
    public final boolean h;

    public e(String str, String str2, String str3, MediaCodecInfo.CodecCapabilities codecCapabilities, boolean z, boolean z2, boolean z3, boolean z4, boolean z5, boolean z6) {
        this.a = (String) ii.e(str);
        this.b = str2;
        this.c = str3;
        this.d = codecCapabilities;
        this.g = z;
        this.e = z4;
        this.f = z6;
        this.h = y82.q(str2);
    }

    public static boolean A(String str, int i) {
        if ("video/hevc".equals(str) && 2 == i) {
            String str2 = androidx.media3.common.util.b.b;
            if ("sailfish".equals(str2) || "marlin".equals(str2)) {
                return true;
            }
        }
        return false;
    }

    public static final boolean B(String str) {
        return ("OMX.MTK.VIDEO.DECODER.HEVC".equals(str) && "mcv5a".equals(androidx.media3.common.util.b.b)) ? false : true;
    }

    public static e C(String str, String str2, String str3, MediaCodecInfo.CodecCapabilities codecCapabilities, boolean z, boolean z2, boolean z3, boolean z4, boolean z5) {
        return new e(str, str2, str3, codecCapabilities, z, z2, z3, (z4 || codecCapabilities == null || !h(codecCapabilities) || z(str)) ? false : true, codecCapabilities != null && s(codecCapabilities), z5 || (codecCapabilities != null && q(codecCapabilities)));
    }

    public static int a(String str, String str2, int i) {
        int i2;
        if (i > 1 || ((androidx.media3.common.util.b.a >= 26 && i > 0) || "audio/mpeg".equals(str2) || "audio/3gpp".equals(str2) || "audio/amr-wb".equals(str2) || "audio/mp4a-latm".equals(str2) || "audio/vorbis".equals(str2) || "audio/opus".equals(str2) || "audio/raw".equals(str2) || "audio/flac".equals(str2) || "audio/g711-alaw".equals(str2) || "audio/g711-mlaw".equals(str2) || "audio/gsm".equals(str2))) {
            return i;
        }
        if ("audio/ac3".equals(str2)) {
            i2 = 6;
        } else {
            i2 = "audio/eac3".equals(str2) ? 16 : 30;
        }
        p12.i("MediaCodecInfo", "AssumedMaxChannelAdjustment: " + str + ", [" + i + " to " + i2 + "]");
        return i2;
    }

    public static Point c(MediaCodecInfo.VideoCapabilities videoCapabilities, int i, int i2) {
        int widthAlignment = videoCapabilities.getWidthAlignment();
        int heightAlignment = videoCapabilities.getHeightAlignment();
        return new Point(androidx.media3.common.util.b.l(i, widthAlignment) * widthAlignment, androidx.media3.common.util.b.l(i2, heightAlignment) * heightAlignment);
    }

    public static boolean d(MediaCodecInfo.VideoCapabilities videoCapabilities, int i, int i2, double d) {
        Point c = c(videoCapabilities, i, i2);
        int i3 = c.x;
        int i4 = c.y;
        if (d != -1.0d && d >= 1.0d) {
            return videoCapabilities.areSizeAndRateSupported(i3, i4, Math.floor(d));
        }
        return videoCapabilities.isSizeSupported(i3, i4);
    }

    public static MediaCodecInfo.CodecProfileLevel[] f(MediaCodecInfo.CodecCapabilities codecCapabilities) {
        MediaCodecInfo.VideoCapabilities videoCapabilities;
        int intValue = (codecCapabilities == null || (videoCapabilities = codecCapabilities.getVideoCapabilities()) == null) ? 0 : videoCapabilities.getBitrateRange().getUpper().intValue();
        int i = intValue >= 180000000 ? RecyclerView.a0.FLAG_ADAPTER_FULLUPDATE : intValue >= 120000000 ? RecyclerView.a0.FLAG_ADAPTER_POSITION_UNKNOWN : intValue >= 60000000 ? 256 : intValue >= 30000000 ? 128 : intValue >= 18000000 ? 64 : intValue >= 12000000 ? 32 : intValue >= 7200000 ? 16 : intValue >= 3600000 ? 8 : intValue >= 1800000 ? 4 : intValue >= 800000 ? 2 : 1;
        MediaCodecInfo.CodecProfileLevel codecProfileLevel = new MediaCodecInfo.CodecProfileLevel();
        codecProfileLevel.profile = 1;
        codecProfileLevel.level = i;
        return new MediaCodecInfo.CodecProfileLevel[]{codecProfileLevel};
    }

    public static boolean h(MediaCodecInfo.CodecCapabilities codecCapabilities) {
        return androidx.media3.common.util.b.a >= 19 && i(codecCapabilities);
    }

    public static boolean i(MediaCodecInfo.CodecCapabilities codecCapabilities) {
        return codecCapabilities.isFeatureSupported("adaptive-playback");
    }

    public static boolean q(MediaCodecInfo.CodecCapabilities codecCapabilities) {
        return androidx.media3.common.util.b.a >= 21 && r(codecCapabilities);
    }

    public static boolean r(MediaCodecInfo.CodecCapabilities codecCapabilities) {
        return codecCapabilities.isFeatureSupported("secure-playback");
    }

    public static boolean s(MediaCodecInfo.CodecCapabilities codecCapabilities) {
        return androidx.media3.common.util.b.a >= 21 && t(codecCapabilities);
    }

    public static boolean t(MediaCodecInfo.CodecCapabilities codecCapabilities) {
        return codecCapabilities.isFeatureSupported("tunneled-playback");
    }

    public static boolean x(String str) {
        return "audio/opus".equals(str);
    }

    public static boolean y(String str) {
        return androidx.media3.common.util.b.d.startsWith("SM-T230") && "OMX.MARVELL.VIDEO.HW.CODA7542DECODER".equals(str);
    }

    public static boolean z(String str) {
        if (androidx.media3.common.util.b.a <= 22) {
            String str2 = androidx.media3.common.util.b.d;
            if (("ODROID-XU3".equals(str2) || "Nexus 10".equals(str2)) && ("OMX.Exynos.AVC.Decoder".equals(str) || "OMX.Exynos.AVC.Decoder.secure".equals(str))) {
                return true;
            }
        }
        return false;
    }

    public Point b(int i, int i2) {
        MediaCodecInfo.VideoCapabilities videoCapabilities;
        MediaCodecInfo.CodecCapabilities codecCapabilities = this.d;
        if (codecCapabilities == null || (videoCapabilities = codecCapabilities.getVideoCapabilities()) == null) {
            return null;
        }
        return c(videoCapabilities, i, i2);
    }

    public jf0 e(j jVar, j jVar2) {
        int i = !androidx.media3.common.util.b.c(jVar.p0, jVar2.p0) ? 8 : 0;
        if (this.h) {
            if (jVar.x0 != jVar2.x0) {
                i |= RecyclerView.a0.FLAG_ADAPTER_FULLUPDATE;
            }
            if (!this.e && (jVar.u0 != jVar2.u0 || jVar.v0 != jVar2.v0)) {
                i |= RecyclerView.a0.FLAG_ADAPTER_POSITION_UNKNOWN;
            }
            if (!androidx.media3.common.util.b.c(jVar.B0, jVar2.B0)) {
                i |= 2048;
            }
            if (y(this.a) && !jVar.g(jVar2)) {
                i |= 2;
            }
            if (i == 0) {
                return new jf0(this.a, jVar, jVar2, jVar.g(jVar2) ? 3 : 2, 0);
            }
        } else {
            if (jVar.C0 != jVar2.C0) {
                i |= 4096;
            }
            if (jVar.D0 != jVar2.D0) {
                i |= 8192;
            }
            if (jVar.E0 != jVar2.E0) {
                i |= Http2.INITIAL_MAX_FRAME_SIZE;
            }
            if (i == 0 && "audio/mp4a-latm".equals(this.b)) {
                Pair<Integer, Integer> q = MediaCodecUtil.q(jVar);
                Pair<Integer, Integer> q2 = MediaCodecUtil.q(jVar2);
                if (q != null && q2 != null) {
                    int intValue = ((Integer) q.first).intValue();
                    int intValue2 = ((Integer) q2.first).intValue();
                    if (intValue == 42 && intValue2 == 42) {
                        return new jf0(this.a, jVar, jVar2, 3, 0);
                    }
                }
            }
            if (!jVar.g(jVar2)) {
                i |= 32;
            }
            if (x(this.b)) {
                i |= 2;
            }
            if (i == 0) {
                return new jf0(this.a, jVar, jVar2, 1, 0);
            }
        }
        return new jf0(this.a, jVar, jVar2, 0, i);
    }

    public MediaCodecInfo.CodecProfileLevel[] g() {
        MediaCodecInfo.CodecProfileLevel[] codecProfileLevelArr;
        MediaCodecInfo.CodecCapabilities codecCapabilities = this.d;
        return (codecCapabilities == null || (codecProfileLevelArr = codecCapabilities.profileLevels) == null) ? new MediaCodecInfo.CodecProfileLevel[0] : codecProfileLevelArr;
    }

    public boolean j(int i) {
        MediaCodecInfo.CodecCapabilities codecCapabilities = this.d;
        if (codecCapabilities == null) {
            w("channelCount.caps");
            return false;
        }
        MediaCodecInfo.AudioCapabilities audioCapabilities = codecCapabilities.getAudioCapabilities();
        if (audioCapabilities == null) {
            w("channelCount.aCaps");
            return false;
        } else if (a(this.a, this.b, audioCapabilities.getMaxInputChannelCount()) < i) {
            w("channelCount.support, " + i);
            return false;
        } else {
            return true;
        }
    }

    public boolean k(int i) {
        MediaCodecInfo.CodecCapabilities codecCapabilities = this.d;
        if (codecCapabilities == null) {
            w("sampleRate.caps");
            return false;
        }
        MediaCodecInfo.AudioCapabilities audioCapabilities = codecCapabilities.getAudioCapabilities();
        if (audioCapabilities == null) {
            w("sampleRate.aCaps");
            return false;
        } else if (audioCapabilities.isSampleRateSupported(i)) {
            return true;
        } else {
            w("sampleRate.support, " + i);
            return false;
        }
    }

    public final boolean l(j jVar) {
        Pair<Integer, Integer> q;
        if (jVar.m0 == null || (q = MediaCodecUtil.q(jVar)) == null) {
            return true;
        }
        int intValue = ((Integer) q.first).intValue();
        int intValue2 = ((Integer) q.second).intValue();
        if ("video/dolby-vision".equals(jVar.p0)) {
            if ("video/avc".equals(this.b)) {
                intValue = 8;
            } else {
                intValue = "video/hevc".equals(this.b) ? 2 : 2;
            }
            intValue2 = 0;
        }
        if (this.h || intValue == 42) {
            MediaCodecInfo.CodecProfileLevel[] g = g();
            if (androidx.media3.common.util.b.a <= 23 && "video/x-vnd.on2.vp9".equals(this.b) && g.length == 0) {
                g = f(this.d);
            }
            for (MediaCodecInfo.CodecProfileLevel codecProfileLevel : g) {
                if (codecProfileLevel.profile == intValue && codecProfileLevel.level >= intValue2 && !A(this.b, intValue)) {
                    return true;
                }
            }
            w("codec.profileLevel, " + jVar.m0 + ", " + this.c);
            return false;
        }
        return true;
    }

    public boolean m(j jVar) throws MediaCodecUtil.DecoderQueryException {
        int i;
        if (o(jVar) && l(jVar)) {
            if (this.h) {
                int i2 = jVar.u0;
                if (i2 <= 0 || (i = jVar.v0) <= 0) {
                    return true;
                }
                if (androidx.media3.common.util.b.a >= 21) {
                    return u(i2, i, jVar.w0);
                }
                boolean z = i2 * i <= MediaCodecUtil.N();
                if (!z) {
                    w("legacyFrameSize, " + jVar.u0 + "x" + jVar.v0);
                }
                return z;
            }
            if (androidx.media3.common.util.b.a >= 21) {
                int i3 = jVar.D0;
                if (i3 != -1 && !k(i3)) {
                    return false;
                }
                int i4 = jVar.C0;
                if (i4 != -1 && !j(i4)) {
                    return false;
                }
            }
            return true;
        }
        return false;
    }

    public boolean n() {
        if (androidx.media3.common.util.b.a >= 29 && "video/x-vnd.on2.vp9".equals(this.b)) {
            for (MediaCodecInfo.CodecProfileLevel codecProfileLevel : g()) {
                if (codecProfileLevel.profile == 16384) {
                    return true;
                }
            }
        }
        return false;
    }

    public final boolean o(j jVar) {
        return this.b.equals(jVar.p0) || this.b.equals(MediaCodecUtil.m(jVar));
    }

    public boolean p(j jVar) {
        if (this.h) {
            return this.e;
        }
        Pair<Integer, Integer> q = MediaCodecUtil.q(jVar);
        return q != null && ((Integer) q.first).intValue() == 42;
    }

    public String toString() {
        return this.a;
    }

    public boolean u(int i, int i2, double d) {
        MediaCodecInfo.CodecCapabilities codecCapabilities = this.d;
        if (codecCapabilities == null) {
            w("sizeAndRate.caps");
            return false;
        }
        MediaCodecInfo.VideoCapabilities videoCapabilities = codecCapabilities.getVideoCapabilities();
        if (videoCapabilities == null) {
            w("sizeAndRate.vCaps");
            return false;
        } else if (d(videoCapabilities, i, i2, d)) {
            return true;
        } else {
            if (i < i2 && B(this.a) && d(videoCapabilities, i2, i, d)) {
                v("sizeAndRate.rotated, " + i + "x" + i2 + "x" + d);
                return true;
            }
            w("sizeAndRate.support, " + i + "x" + i2 + "x" + d);
            return false;
        }
    }

    public final void v(String str) {
        p12.b("MediaCodecInfo", "AssumedSupport [" + str + "] [" + this.a + ", " + this.b + "] [" + androidx.media3.common.util.b.e + "]");
    }

    public final void w(String str) {
        p12.b("MediaCodecInfo", "NoSupport [" + str + "] [" + this.a + ", " + this.b + "] [" + androidx.media3.common.util.b.e + "]");
    }
}
