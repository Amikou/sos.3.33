package androidx.media3.exoplayer.mediacodec;

import android.media.MediaCodec;
import android.media.MediaFormat;
import android.os.Bundle;
import android.os.Handler;
import android.view.Surface;
import androidx.media3.exoplayer.mediacodec.d;
import androidx.media3.exoplayer.mediacodec.g;
import java.io.IOException;
import java.nio.ByteBuffer;

/* compiled from: SynchronousMediaCodecAdapter.java */
/* loaded from: classes.dex */
public final class g implements d {
    public final MediaCodec a;
    public ByteBuffer[] b;
    public ByteBuffer[] c;

    /* compiled from: SynchronousMediaCodecAdapter.java */
    /* loaded from: classes.dex */
    public static class b implements d.b {
        @Override // androidx.media3.exoplayer.mediacodec.d.b
        public d a(d.a aVar) throws IOException {
            MediaCodec mediaCodec = null;
            try {
                MediaCodec b = b(aVar);
                try {
                    v74.a("configureCodec");
                    b.configure(aVar.b, aVar.d, aVar.e, aVar.f);
                    v74.c();
                    v74.a("startCodec");
                    b.start();
                    v74.c();
                    return new g(b);
                } catch (IOException | RuntimeException e) {
                    e = e;
                    mediaCodec = b;
                    if (mediaCodec != null) {
                        mediaCodec.release();
                    }
                    throw e;
                }
            } catch (IOException e2) {
                e = e2;
            } catch (RuntimeException e3) {
                e = e3;
            }
        }

        public MediaCodec b(d.a aVar) throws IOException {
            ii.e(aVar.a);
            String str = aVar.a.a;
            v74.a("createCodec:" + str);
            MediaCodec createByCodecName = MediaCodec.createByCodecName(str);
            v74.c();
            return createByCodecName;
        }
    }

    /* JADX INFO: Access modifiers changed from: private */
    public /* synthetic */ void q(d.c cVar, MediaCodec mediaCodec, long j, long j2) {
        cVar.a(this, j, j2);
    }

    @Override // androidx.media3.exoplayer.mediacodec.d
    public void a() {
        this.b = null;
        this.c = null;
        this.a.release();
    }

    @Override // androidx.media3.exoplayer.mediacodec.d
    public boolean b() {
        return false;
    }

    @Override // androidx.media3.exoplayer.mediacodec.d
    public void c(final d.c cVar, Handler handler) {
        this.a.setOnFrameRenderedListener(new MediaCodec.OnFrameRenderedListener() { // from class: m24
            @Override // android.media.MediaCodec.OnFrameRenderedListener
            public final void onFrameRendered(MediaCodec mediaCodec, long j, long j2) {
                g.this.q(cVar, mediaCodec, j, j2);
            }
        }, handler);
    }

    @Override // androidx.media3.exoplayer.mediacodec.d
    public void d(int i, int i2, ra0 ra0Var, long j, int i3) {
        this.a.queueSecureInputBuffer(i, i2, ra0Var.a(), j, i3);
    }

    @Override // androidx.media3.exoplayer.mediacodec.d
    public MediaFormat e() {
        return this.a.getOutputFormat();
    }

    @Override // androidx.media3.exoplayer.mediacodec.d
    public void f(Bundle bundle) {
        this.a.setParameters(bundle);
    }

    @Override // androidx.media3.exoplayer.mediacodec.d
    public void flush() {
        this.a.flush();
    }

    @Override // androidx.media3.exoplayer.mediacodec.d
    public void g(int i, long j) {
        this.a.releaseOutputBuffer(i, j);
    }

    @Override // androidx.media3.exoplayer.mediacodec.d
    public int h() {
        return this.a.dequeueInputBuffer(0L);
    }

    @Override // androidx.media3.exoplayer.mediacodec.d
    public int i(MediaCodec.BufferInfo bufferInfo) {
        int dequeueOutputBuffer;
        do {
            dequeueOutputBuffer = this.a.dequeueOutputBuffer(bufferInfo, 0L);
            if (dequeueOutputBuffer == -3 && androidx.media3.common.util.b.a < 21) {
                this.c = this.a.getOutputBuffers();
                continue;
            }
        } while (dequeueOutputBuffer == -3);
        return dequeueOutputBuffer;
    }

    @Override // androidx.media3.exoplayer.mediacodec.d
    public void j(int i, boolean z) {
        this.a.releaseOutputBuffer(i, z);
    }

    @Override // androidx.media3.exoplayer.mediacodec.d
    public void k(int i) {
        this.a.setVideoScalingMode(i);
    }

    @Override // androidx.media3.exoplayer.mediacodec.d
    public ByteBuffer l(int i) {
        if (androidx.media3.common.util.b.a >= 21) {
            return this.a.getInputBuffer(i);
        }
        return ((ByteBuffer[]) androidx.media3.common.util.b.j(this.b))[i];
    }

    @Override // androidx.media3.exoplayer.mediacodec.d
    public void m(Surface surface) {
        this.a.setOutputSurface(surface);
    }

    @Override // androidx.media3.exoplayer.mediacodec.d
    public void n(int i, int i2, int i3, long j, int i4) {
        this.a.queueInputBuffer(i, i2, i3, j, i4);
    }

    @Override // androidx.media3.exoplayer.mediacodec.d
    public ByteBuffer o(int i) {
        if (androidx.media3.common.util.b.a >= 21) {
            return this.a.getOutputBuffer(i);
        }
        return ((ByteBuffer[]) androidx.media3.common.util.b.j(this.c))[i];
    }

    public g(MediaCodec mediaCodec) {
        this.a = mediaCodec;
        if (androidx.media3.common.util.b.a < 21) {
            this.b = mediaCodec.getInputBuffers();
            this.c = mediaCodec.getOutputBuffers();
        }
    }
}
