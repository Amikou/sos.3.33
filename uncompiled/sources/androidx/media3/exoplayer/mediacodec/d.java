package androidx.media3.exoplayer.mediacodec;

import android.media.MediaCodec;
import android.media.MediaCrypto;
import android.media.MediaFormat;
import android.os.Bundle;
import android.os.Handler;
import android.view.Surface;
import androidx.media3.common.j;
import java.io.IOException;
import java.nio.ByteBuffer;

/* compiled from: MediaCodecAdapter.java */
/* loaded from: classes.dex */
public interface d {

    /* compiled from: MediaCodecAdapter.java */
    /* loaded from: classes.dex */
    public static final class a {
        public final e a;
        public final MediaFormat b;
        public final j c;
        public final Surface d;
        public final MediaCrypto e;
        public final int f;

        public a(e eVar, MediaFormat mediaFormat, j jVar, Surface surface, MediaCrypto mediaCrypto, int i) {
            this.a = eVar;
            this.b = mediaFormat;
            this.c = jVar;
            this.d = surface;
            this.e = mediaCrypto;
            this.f = i;
        }

        public static a a(e eVar, MediaFormat mediaFormat, j jVar, MediaCrypto mediaCrypto) {
            return new a(eVar, mediaFormat, jVar, null, mediaCrypto, 0);
        }

        public static a b(e eVar, MediaFormat mediaFormat, j jVar, Surface surface, MediaCrypto mediaCrypto) {
            return new a(eVar, mediaFormat, jVar, surface, mediaCrypto, 0);
        }
    }

    /* compiled from: MediaCodecAdapter.java */
    /* loaded from: classes.dex */
    public interface b {
        d a(a aVar) throws IOException;
    }

    /* compiled from: MediaCodecAdapter.java */
    /* loaded from: classes.dex */
    public interface c {
        void a(d dVar, long j, long j2);
    }

    void a();

    boolean b();

    void c(c cVar, Handler handler);

    void d(int i, int i2, ra0 ra0Var, long j, int i3);

    MediaFormat e();

    void f(Bundle bundle);

    void flush();

    void g(int i, long j);

    int h();

    int i(MediaCodec.BufferInfo bufferInfo);

    void j(int i, boolean z);

    void k(int i);

    ByteBuffer l(int i);

    void m(Surface surface);

    void n(int i, int i2, int i3, long j, int i4);

    ByteBuffer o(int i);
}
