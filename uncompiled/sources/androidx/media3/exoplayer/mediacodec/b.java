package androidx.media3.exoplayer.mediacodec;

import android.media.MediaCodec;
import android.os.Handler;
import android.os.HandlerThread;
import android.os.Looper;
import android.os.Message;
import java.util.ArrayDeque;
import java.util.Arrays;
import java.util.concurrent.atomic.AtomicReference;

/* compiled from: AsynchronousMediaCodecBufferEnqueuer.java */
/* loaded from: classes.dex */
public class b {
    public static final ArrayDeque<C0038b> g = new ArrayDeque<>();
    public static final Object h = new Object();
    public final MediaCodec a;
    public final HandlerThread b;
    public Handler c;
    public final AtomicReference<RuntimeException> d;
    public final androidx.media3.common.util.a e;
    public boolean f;

    /* compiled from: AsynchronousMediaCodecBufferEnqueuer.java */
    /* loaded from: classes.dex */
    public class a extends Handler {
        public a(Looper looper) {
            super(looper);
        }

        @Override // android.os.Handler
        public void handleMessage(Message message) {
            b.this.f(message);
        }
    }

    /* compiled from: AsynchronousMediaCodecBufferEnqueuer.java */
    /* renamed from: androidx.media3.exoplayer.mediacodec.b$b  reason: collision with other inner class name */
    /* loaded from: classes.dex */
    public static class C0038b {
        public int a;
        public int b;
        public int c;
        public final MediaCodec.CryptoInfo d = new MediaCodec.CryptoInfo();
        public long e;
        public int f;

        public void a(int i, int i2, int i3, long j, int i4) {
            this.a = i;
            this.b = i2;
            this.c = i3;
            this.e = j;
            this.f = i4;
        }
    }

    public b(MediaCodec mediaCodec, HandlerThread handlerThread) {
        this(mediaCodec, handlerThread, new androidx.media3.common.util.a());
    }

    public static void c(ra0 ra0Var, MediaCodec.CryptoInfo cryptoInfo) {
        cryptoInfo.numSubSamples = ra0Var.f;
        cryptoInfo.numBytesOfClearData = e(ra0Var.d, cryptoInfo.numBytesOfClearData);
        cryptoInfo.numBytesOfEncryptedData = e(ra0Var.e, cryptoInfo.numBytesOfEncryptedData);
        cryptoInfo.key = (byte[]) ii.e(d(ra0Var.b, cryptoInfo.key));
        cryptoInfo.iv = (byte[]) ii.e(d(ra0Var.a, cryptoInfo.iv));
        cryptoInfo.mode = ra0Var.c;
        if (androidx.media3.common.util.b.a >= 24) {
            cryptoInfo.setPattern(new MediaCodec.CryptoInfo.Pattern(ra0Var.g, ra0Var.h));
        }
    }

    public static byte[] d(byte[] bArr, byte[] bArr2) {
        if (bArr == null) {
            return bArr2;
        }
        if (bArr2 != null && bArr2.length >= bArr.length) {
            System.arraycopy(bArr, 0, bArr2, 0, bArr.length);
            return bArr2;
        }
        return Arrays.copyOf(bArr, bArr.length);
    }

    public static int[] e(int[] iArr, int[] iArr2) {
        if (iArr == null) {
            return iArr2;
        }
        if (iArr2 != null && iArr2.length >= iArr.length) {
            System.arraycopy(iArr, 0, iArr2, 0, iArr.length);
            return iArr2;
        }
        return Arrays.copyOf(iArr, iArr.length);
    }

    public static C0038b k() {
        ArrayDeque<C0038b> arrayDeque = g;
        synchronized (arrayDeque) {
            if (arrayDeque.isEmpty()) {
                return new C0038b();
            }
            return arrayDeque.removeFirst();
        }
    }

    public static void o(C0038b c0038b) {
        ArrayDeque<C0038b> arrayDeque = g;
        synchronized (arrayDeque) {
            arrayDeque.add(c0038b);
        }
    }

    public final void b() throws InterruptedException {
        this.e.c();
        ((Handler) ii.e(this.c)).obtainMessage(2).sendToTarget();
        this.e.a();
    }

    public final void f(Message message) {
        int i = message.what;
        C0038b c0038b = null;
        if (i == 0) {
            c0038b = (C0038b) message.obj;
            g(c0038b.a, c0038b.b, c0038b.c, c0038b.e, c0038b.f);
        } else if (i == 1) {
            c0038b = (C0038b) message.obj;
            h(c0038b.a, c0038b.b, c0038b.d, c0038b.e, c0038b.f);
        } else if (i != 2) {
            this.d.compareAndSet(null, new IllegalStateException(String.valueOf(message.what)));
        } else {
            this.e.e();
        }
        if (c0038b != null) {
            o(c0038b);
        }
    }

    public final void g(int i, int i2, int i3, long j, int i4) {
        try {
            this.a.queueInputBuffer(i, i2, i3, j, i4);
        } catch (RuntimeException e) {
            this.d.compareAndSet(null, e);
        }
    }

    public final void h(int i, int i2, MediaCodec.CryptoInfo cryptoInfo, long j, int i3) {
        try {
            synchronized (h) {
                this.a.queueSecureInputBuffer(i, i2, cryptoInfo, j, i3);
            }
        } catch (RuntimeException e) {
            this.d.compareAndSet(null, e);
        }
    }

    public void i() {
        if (this.f) {
            try {
                j();
            } catch (InterruptedException e) {
                Thread.currentThread().interrupt();
                throw new IllegalStateException(e);
            }
        }
    }

    public final void j() throws InterruptedException {
        ((Handler) ii.e(this.c)).removeCallbacksAndMessages(null);
        b();
    }

    public final void l() {
        RuntimeException andSet = this.d.getAndSet(null);
        if (andSet != null) {
            throw andSet;
        }
    }

    public void m(int i, int i2, int i3, long j, int i4) {
        l();
        C0038b k = k();
        k.a(i, i2, i3, j, i4);
        ((Handler) androidx.media3.common.util.b.j(this.c)).obtainMessage(0, k).sendToTarget();
    }

    public void n(int i, int i2, ra0 ra0Var, long j, int i3) {
        l();
        C0038b k = k();
        k.a(i, i2, 0, j, i3);
        c(ra0Var, k.d);
        ((Handler) androidx.media3.common.util.b.j(this.c)).obtainMessage(1, k).sendToTarget();
    }

    public void p() {
        if (this.f) {
            i();
            this.b.quit();
        }
        this.f = false;
    }

    public void q() {
        if (this.f) {
            return;
        }
        this.b.start();
        this.c = new a(this.b.getLooper());
        this.f = true;
    }

    public void r() throws InterruptedException {
        b();
    }

    public b(MediaCodec mediaCodec, HandlerThread handlerThread, androidx.media3.common.util.a aVar) {
        this.a = mediaCodec;
        this.b = handlerThread;
        this.e = aVar;
        this.d = new AtomicReference<>();
    }
}
