package androidx.media3.exoplayer.mediacodec;

import android.annotation.TargetApi;
import android.media.MediaCodec;
import android.media.MediaCrypto;
import android.media.MediaCryptoException;
import android.media.MediaFormat;
import android.media.metrics.LogSessionId;
import android.os.Bundle;
import android.os.SystemClock;
import androidx.media3.common.PlaybackException;
import androidx.media3.common.j;
import androidx.media3.decoder.DecoderInputBuffer;
import androidx.media3.exoplayer.ExoPlaybackException;
import androidx.media3.exoplayer.drm.DrmSession;
import androidx.media3.exoplayer.mediacodec.MediaCodecUtil;
import androidx.media3.exoplayer.mediacodec.d;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.util.ArrayDeque;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;
import zendesk.support.request.CellBase;

/* loaded from: classes.dex */
public abstract class MediaCodecRenderer extends androidx.media3.exoplayer.c {
    public static final byte[] I1 = {0, 0, 1, 103, 66, -64, 11, -38, 37, -112, 0, 0, 1, 104, -50, 15, 19, 32, 0, 0, 1, 101, -120, -124, 13, -50, 113, 24, -96, 0, 47, -65, 28, 49, -61, 39, 93, 120};
    public final MediaCodec.BufferInfo A0;
    public boolean A1;
    public final long[] B0;
    public boolean B1;
    public final long[] C0;
    public boolean C1;
    public final long[] D0;
    public ExoPlaybackException D1;
    public j E0;
    public hf0 E1;
    public j F0;
    public long F1;
    public DrmSession G0;
    public long G1;
    public DrmSession H0;
    public int H1;
    public MediaCrypto I0;
    public boolean J0;
    public long K0;
    public float L0;
    public float M0;
    public d N0;
    public j O0;
    public MediaFormat P0;
    public boolean Q0;
    public float R0;
    public ArrayDeque<e> S0;
    public DecoderInitializationException T0;
    public e U0;
    public int V0;
    public boolean W0;
    public boolean X0;
    public boolean Y0;
    public boolean Z0;
    public boolean a1;
    public boolean b1;
    public boolean c1;
    public boolean d1;
    public boolean e1;
    public boolean f1;
    public et g1;
    public long h1;
    public int i1;
    public int j1;
    public ByteBuffer k1;
    public boolean l1;
    public boolean m1;
    public boolean n1;
    public boolean o1;
    public boolean p1;
    public final d.b q0;
    public boolean q1;
    public final f r0;
    public int r1;
    public final boolean s0;
    public int s1;
    public final float t0;
    public int t1;
    public final DecoderInputBuffer u0;
    public boolean u1;
    public final DecoderInputBuffer v0;
    public boolean v1;
    public final DecoderInputBuffer w0;
    public boolean w1;
    public final ko x0;
    public long x1;
    public final e64<j> y0;
    public long y1;
    public final ArrayList<Long> z0;
    public boolean z1;

    /* loaded from: classes.dex */
    public static final class a {
        public static void a(d.a aVar, ks2 ks2Var) {
            LogSessionId a = ks2Var.a();
            if (a.equals(LogSessionId.LOG_SESSION_ID_NONE)) {
                return;
            }
            aVar.b.setString("log-session-id", a.getStringId());
        }
    }

    public MediaCodecRenderer(int i, d.b bVar, f fVar, boolean z, float f) {
        super(i);
        this.q0 = bVar;
        this.r0 = (f) ii.e(fVar);
        this.s0 = z;
        this.t0 = f;
        this.u0 = DecoderInputBuffer.A();
        this.v0 = new DecoderInputBuffer(0);
        this.w0 = new DecoderInputBuffer(2);
        ko koVar = new ko();
        this.x0 = koVar;
        this.y0 = new e64<>();
        this.z0 = new ArrayList<>();
        this.A0 = new MediaCodec.BufferInfo();
        this.L0 = 1.0f;
        this.M0 = 1.0f;
        this.K0 = CellBase.ID_SYSTEM_MESSAGE_REQUEST_CLOSED;
        this.B0 = new long[10];
        this.C0 = new long[10];
        this.D0 = new long[10];
        this.F1 = CellBase.ID_SYSTEM_MESSAGE_REQUEST_CLOSED;
        this.G1 = CellBase.ID_SYSTEM_MESSAGE_REQUEST_CLOSED;
        koVar.v(0);
        koVar.g0.order(ByteOrder.nativeOrder());
        this.R0 = -1.0f;
        this.V0 = 0;
        this.r1 = 0;
        this.i1 = -1;
        this.j1 = -1;
        this.h1 = CellBase.ID_SYSTEM_MESSAGE_REQUEST_CLOSED;
        this.x1 = CellBase.ID_SYSTEM_MESSAGE_REQUEST_CLOSED;
        this.y1 = CellBase.ID_SYSTEM_MESSAGE_REQUEST_CLOSED;
        this.s1 = 0;
        this.t1 = 0;
    }

    public static boolean D0(IllegalStateException illegalStateException) {
        if (androidx.media3.common.util.b.a < 21 || !E0(illegalStateException)) {
            StackTraceElement[] stackTrace = illegalStateException.getStackTrace();
            return stackTrace.length > 0 && stackTrace[0].getClassName().equals("android.media.MediaCodec");
        }
        return true;
    }

    public static boolean E0(IllegalStateException illegalStateException) {
        return illegalStateException instanceof MediaCodec.CodecException;
    }

    public static boolean F0(IllegalStateException illegalStateException) {
        if (illegalStateException instanceof MediaCodec.CodecException) {
            return ((MediaCodec.CodecException) illegalStateException).isRecoverable();
        }
        return false;
    }

    public static boolean U(String str, j jVar) {
        return androidx.media3.common.util.b.a < 21 && jVar.r0.isEmpty() && "OMX.MTK.VIDEO.DECODER.AVC".equals(str);
    }

    public static boolean V(String str) {
        if (androidx.media3.common.util.b.a < 21 && "OMX.SEC.mp3.dec".equals(str) && "samsung".equals(androidx.media3.common.util.b.c)) {
            String str2 = androidx.media3.common.util.b.b;
            if (str2.startsWith("baffin") || str2.startsWith("grand") || str2.startsWith("fortuna") || str2.startsWith("gprimelte") || str2.startsWith("j2y18lte") || str2.startsWith("ms01")) {
                return true;
            }
        }
        return false;
    }

    public static boolean W(String str) {
        int i = androidx.media3.common.util.b.a;
        if (i > 23 || !"OMX.google.vorbis.decoder".equals(str)) {
            if (i <= 19) {
                String str2 = androidx.media3.common.util.b.b;
                if (("hb2000".equals(str2) || "stvm8".equals(str2)) && ("OMX.amlogic.avc.decoder.awesome".equals(str) || "OMX.amlogic.avc.decoder.awesome.secure".equals(str))) {
                }
            }
            return false;
        }
        return true;
    }

    public static boolean X(String str) {
        return androidx.media3.common.util.b.a == 21 && "OMX.google.aac.decoder".equals(str);
    }

    public static boolean Y(e eVar) {
        String str = eVar.a;
        int i = androidx.media3.common.util.b.a;
        return (i <= 25 && "OMX.rk.video_decoder.avc".equals(str)) || (i <= 17 && "OMX.allwinner.video.decoder.avc".equals(str)) || ((i <= 29 && ("OMX.broadcom.video_decoder.tunnel".equals(str) || "OMX.broadcom.video_decoder.tunnel.secure".equals(str))) || ("Amazon".equals(androidx.media3.common.util.b.c) && "AFTS".equals(androidx.media3.common.util.b.d) && eVar.f));
    }

    public static boolean Z(String str) {
        int i = androidx.media3.common.util.b.a;
        return i < 18 || (i == 18 && ("OMX.SEC.avc.dec".equals(str) || "OMX.SEC.avc.dec.secure".equals(str))) || (i == 19 && androidx.media3.common.util.b.d.startsWith("SM-G800") && ("OMX.Exynos.avc.dec".equals(str) || "OMX.Exynos.avc.dec.secure".equals(str)));
    }

    public static boolean a0(String str, j jVar) {
        return androidx.media3.common.util.b.a <= 18 && jVar.C0 == 1 && "OMX.MTK.AUDIO.DECODER.MP3".equals(str);
    }

    public static boolean b0(String str) {
        return androidx.media3.common.util.b.a == 29 && "c2.android.aac.decoder".equals(str);
    }

    public static boolean k1(j jVar) {
        int i = jVar.I0;
        return i == 0 || i == 2;
    }

    public final void A0(j jVar) {
        d0();
        String str = jVar.p0;
        if (!"audio/mp4a-latm".equals(str) && !"audio/mpeg".equals(str) && !"audio/opus".equals(str)) {
            this.x0.J(1);
        } else {
            this.x0.J(32);
        }
        this.n1 = true;
    }

    public final void B0(e eVar, MediaCrypto mediaCrypto) throws Exception {
        String str = eVar.a;
        int i = androidx.media3.common.util.b.a;
        float r0 = i < 23 ? -1.0f : r0(this.M0, this.E0, E());
        float f = r0 > this.t0 ? r0 : -1.0f;
        long elapsedRealtime = SystemClock.elapsedRealtime();
        d.a v0 = v0(eVar, this.E0, mediaCrypto, f);
        if (i >= 31) {
            a.a(v0, D());
        }
        try {
            v74.a("createCodec:" + str);
            this.N0 = this.q0.a(v0);
            v74.c();
            long elapsedRealtime2 = SystemClock.elapsedRealtime();
            this.U0 = eVar;
            this.R0 = f;
            this.O0 = this.E0;
            this.V0 = T(str);
            this.W0 = U(str, this.O0);
            this.X0 = Z(str);
            this.Y0 = b0(str);
            this.Z0 = W(str);
            this.a1 = X(str);
            this.b1 = V(str);
            this.c1 = a0(str, this.O0);
            this.f1 = Y(eVar) || q0();
            if (this.N0.b()) {
                this.q1 = true;
                this.r1 = 1;
                this.d1 = this.V0 != 0;
            }
            if ("c2.android.mp3.decoder".equals(eVar.a)) {
                this.g1 = new et();
            }
            if (getState() == 2) {
                this.h1 = SystemClock.elapsedRealtime() + 1000;
            }
            this.E1.a++;
            J0(str, v0, elapsedRealtime2, elapsedRealtime2 - elapsedRealtime);
        } catch (Throwable th) {
            v74.c();
            throw th;
        }
    }

    public final boolean C0(long j) {
        int size = this.z0.size();
        for (int i = 0; i < size; i++) {
            if (this.z0.get(i).longValue() == j) {
                this.z0.remove(i);
                return true;
            }
        }
        return false;
    }

    @Override // androidx.media3.exoplayer.c
    public void G() {
        this.E0 = null;
        this.F1 = CellBase.ID_SYSTEM_MESSAGE_REQUEST_CLOSED;
        this.G1 = CellBase.ID_SYSTEM_MESSAGE_REQUEST_CLOSED;
        this.H1 = 0;
        m0();
    }

    public final void G0() throws ExoPlaybackException {
        j jVar;
        if (this.N0 != null || this.n1 || (jVar = this.E0) == null) {
            return;
        }
        if (this.H0 == null && i1(jVar)) {
            A0(this.E0);
            return;
        }
        b1(this.H0);
        String str = this.E0.p0;
        DrmSession drmSession = this.G0;
        if (drmSession != null) {
            if (this.I0 == null) {
                cc1 u0 = u0(drmSession);
                if (u0 == null) {
                    if (this.G0.getError() == null) {
                        return;
                    }
                } else {
                    try {
                        MediaCrypto mediaCrypto = new MediaCrypto(u0.a, u0.b);
                        this.I0 = mediaCrypto;
                        this.J0 = !u0.c && mediaCrypto.requiresSecureDecoderComponent(str);
                    } catch (MediaCryptoException e) {
                        throw y(e, this.E0, PlaybackException.ERROR_CODE_DRM_SYSTEM_ERROR);
                    }
                }
            }
            if (cc1.d) {
                int state = this.G0.getState();
                if (state == 1) {
                    DrmSession.DrmSessionException drmSessionException = (DrmSession.DrmSessionException) ii.e(this.G0.getError());
                    throw y(drmSessionException, this.E0, drmSessionException.errorCode);
                } else if (state != 4) {
                    return;
                }
            }
        }
        try {
            H0(this.I0, this.J0);
        } catch (DecoderInitializationException e2) {
            throw y(e2, this.E0, PlaybackException.ERROR_CODE_DECODER_INIT_FAILED);
        }
    }

    @Override // androidx.media3.exoplayer.c
    public void H(boolean z, boolean z2) throws ExoPlaybackException {
        this.E1 = new hf0();
    }

    /* JADX WARN: Removed duplicated region for block: B:33:0x009c  */
    /* JADX WARN: Removed duplicated region for block: B:34:0x009f  */
    /* JADX WARN: Removed duplicated region for block: B:52:0x00ae A[SYNTHETIC] */
    /* JADX WARN: Removed duplicated region for block: B:58:0x0049 A[SYNTHETIC] */
    /*
        Code decompiled incorrectly, please refer to instructions dump.
        To view partially-correct code enable 'Show inconsistent code' option in preferences
    */
    public final void H0(android.media.MediaCrypto r8, boolean r9) throws androidx.media3.exoplayer.mediacodec.MediaCodecRenderer.DecoderInitializationException {
        /*
            r7 = this;
            java.util.ArrayDeque<androidx.media3.exoplayer.mediacodec.e> r0 = r7.S0
            r1 = 0
            if (r0 != 0) goto L39
            java.util.List r0 = r7.n0(r9)     // Catch: androidx.media3.exoplayer.mediacodec.MediaCodecUtil.DecoderQueryException -> L2d
            java.util.ArrayDeque r2 = new java.util.ArrayDeque     // Catch: androidx.media3.exoplayer.mediacodec.MediaCodecUtil.DecoderQueryException -> L2d
            r2.<init>()     // Catch: androidx.media3.exoplayer.mediacodec.MediaCodecUtil.DecoderQueryException -> L2d
            r7.S0 = r2     // Catch: androidx.media3.exoplayer.mediacodec.MediaCodecUtil.DecoderQueryException -> L2d
            boolean r3 = r7.s0     // Catch: androidx.media3.exoplayer.mediacodec.MediaCodecUtil.DecoderQueryException -> L2d
            if (r3 == 0) goto L18
            r2.addAll(r0)     // Catch: androidx.media3.exoplayer.mediacodec.MediaCodecUtil.DecoderQueryException -> L2d
            goto L2a
        L18:
            boolean r2 = r0.isEmpty()     // Catch: androidx.media3.exoplayer.mediacodec.MediaCodecUtil.DecoderQueryException -> L2d
            if (r2 != 0) goto L2a
            java.util.ArrayDeque<androidx.media3.exoplayer.mediacodec.e> r2 = r7.S0     // Catch: androidx.media3.exoplayer.mediacodec.MediaCodecUtil.DecoderQueryException -> L2d
            r3 = 0
            java.lang.Object r0 = r0.get(r3)     // Catch: androidx.media3.exoplayer.mediacodec.MediaCodecUtil.DecoderQueryException -> L2d
            androidx.media3.exoplayer.mediacodec.e r0 = (androidx.media3.exoplayer.mediacodec.e) r0     // Catch: androidx.media3.exoplayer.mediacodec.MediaCodecUtil.DecoderQueryException -> L2d
            r2.add(r0)     // Catch: androidx.media3.exoplayer.mediacodec.MediaCodecUtil.DecoderQueryException -> L2d
        L2a:
            r7.T0 = r1     // Catch: androidx.media3.exoplayer.mediacodec.MediaCodecUtil.DecoderQueryException -> L2d
            goto L39
        L2d:
            r8 = move-exception
            androidx.media3.exoplayer.mediacodec.MediaCodecRenderer$DecoderInitializationException r0 = new androidx.media3.exoplayer.mediacodec.MediaCodecRenderer$DecoderInitializationException
            androidx.media3.common.j r1 = r7.E0
            r2 = -49998(0xffffffffffff3cb2, float:NaN)
            r0.<init>(r1, r8, r9, r2)
            throw r0
        L39:
            java.util.ArrayDeque<androidx.media3.exoplayer.mediacodec.e> r0 = r7.S0
            boolean r0 = r0.isEmpty()
            if (r0 != 0) goto Lb4
            java.util.ArrayDeque<androidx.media3.exoplayer.mediacodec.e> r0 = r7.S0
            java.lang.Object r0 = r0.peekFirst()
            androidx.media3.exoplayer.mediacodec.e r0 = (androidx.media3.exoplayer.mediacodec.e) r0
        L49:
            androidx.media3.exoplayer.mediacodec.d r2 = r7.N0
            if (r2 != 0) goto Lb1
            java.util.ArrayDeque<androidx.media3.exoplayer.mediacodec.e> r2 = r7.S0
            java.lang.Object r2 = r2.peekFirst()
            androidx.media3.exoplayer.mediacodec.e r2 = (androidx.media3.exoplayer.mediacodec.e) r2
            boolean r3 = r7.g1(r2)
            if (r3 != 0) goto L5c
            return
        L5c:
            r7.B0(r2, r8)     // Catch: java.lang.Exception -> L60
            goto L49
        L60:
            r3 = move-exception
            java.lang.String r4 = "MediaCodecRenderer"
            if (r2 != r0) goto L73
            java.lang.String r3 = "Preferred decoder instantiation failed. Sleeping for 50ms then retrying."
            defpackage.p12.i(r4, r3)     // Catch: java.lang.Exception -> L74
            r5 = 50
            java.lang.Thread.sleep(r5)     // Catch: java.lang.Exception -> L74
            r7.B0(r2, r8)     // Catch: java.lang.Exception -> L74
            goto L49
        L73:
            throw r3     // Catch: java.lang.Exception -> L74
        L74:
            r3 = move-exception
            java.lang.StringBuilder r5 = new java.lang.StringBuilder
            r5.<init>()
            java.lang.String r6 = "Failed to initialize decoder: "
            r5.append(r6)
            r5.append(r2)
            java.lang.String r5 = r5.toString()
            defpackage.p12.j(r4, r5, r3)
            java.util.ArrayDeque<androidx.media3.exoplayer.mediacodec.e> r4 = r7.S0
            r4.removeFirst()
            androidx.media3.exoplayer.mediacodec.MediaCodecRenderer$DecoderInitializationException r4 = new androidx.media3.exoplayer.mediacodec.MediaCodecRenderer$DecoderInitializationException
            androidx.media3.common.j r5 = r7.E0
            r4.<init>(r5, r3, r9, r2)
            r7.I0(r4)
            androidx.media3.exoplayer.mediacodec.MediaCodecRenderer$DecoderInitializationException r2 = r7.T0
            if (r2 != 0) goto L9f
            r7.T0 = r4
            goto La5
        L9f:
            androidx.media3.exoplayer.mediacodec.MediaCodecRenderer$DecoderInitializationException r2 = androidx.media3.exoplayer.mediacodec.MediaCodecRenderer.DecoderInitializationException.access$000(r2, r4)
            r7.T0 = r2
        La5:
            java.util.ArrayDeque<androidx.media3.exoplayer.mediacodec.e> r2 = r7.S0
            boolean r2 = r2.isEmpty()
            if (r2 != 0) goto Lae
            goto L49
        Lae:
            androidx.media3.exoplayer.mediacodec.MediaCodecRenderer$DecoderInitializationException r8 = r7.T0
            throw r8
        Lb1:
            r7.S0 = r1
            return
        Lb4:
            androidx.media3.exoplayer.mediacodec.MediaCodecRenderer$DecoderInitializationException r8 = new androidx.media3.exoplayer.mediacodec.MediaCodecRenderer$DecoderInitializationException
            androidx.media3.common.j r0 = r7.E0
            r2 = -49999(0xffffffffffff3cb1, float:NaN)
            r8.<init>(r0, r1, r9, r2)
            throw r8
        */
        throw new UnsupportedOperationException("Method not decompiled: androidx.media3.exoplayer.mediacodec.MediaCodecRenderer.H0(android.media.MediaCrypto, boolean):void");
    }

    @Override // androidx.media3.exoplayer.c
    public void I(long j, boolean z) throws ExoPlaybackException {
        this.z1 = false;
        this.A1 = false;
        this.C1 = false;
        if (this.n1) {
            this.x0.h();
            this.w0.h();
            this.o1 = false;
        } else {
            l0();
        }
        if (this.y0.l() > 0) {
            this.B1 = true;
        }
        this.y0.c();
        int i = this.H1;
        if (i != 0) {
            this.G1 = this.C0[i - 1];
            this.F1 = this.B0[i - 1];
            this.H1 = 0;
        }
    }

    public abstract void I0(Exception exc);

    @Override // androidx.media3.exoplayer.c
    public void J() {
        try {
            d0();
            V0();
        } finally {
            e1(null);
        }
    }

    public abstract void J0(String str, d.a aVar, long j, long j2);

    @Override // androidx.media3.exoplayer.c
    public void K() {
    }

    public abstract void K0(String str);

    @Override // androidx.media3.exoplayer.c
    public void L() {
    }

    /* JADX WARN: Code restructure failed: missing block: B:37:0x0080, code lost:
        if (g0() == false) goto L34;
     */
    /* JADX WARN: Code restructure failed: missing block: B:56:0x00b2, code lost:
        if (g0() == false) goto L34;
     */
    /* JADX WARN: Code restructure failed: missing block: B:68:0x00ce, code lost:
        r7 = 2;
     */
    /*
        Code decompiled incorrectly, please refer to instructions dump.
        To view partially-correct code enable 'Show inconsistent code' option in preferences
    */
    public defpackage.jf0 L0(defpackage.y81 r12) throws androidx.media3.exoplayer.ExoPlaybackException {
        /*
            Method dump skipped, instructions count: 247
            To view this dump change 'Code comments level' option to 'DEBUG'
        */
        throw new UnsupportedOperationException("Method not decompiled: androidx.media3.exoplayer.mediacodec.MediaCodecRenderer.L0(y81):jf0");
    }

    @Override // androidx.media3.exoplayer.c
    public void M(j[] jVarArr, long j, long j2) throws ExoPlaybackException {
        if (this.G1 == CellBase.ID_SYSTEM_MESSAGE_REQUEST_CLOSED) {
            ii.g(this.F1 == CellBase.ID_SYSTEM_MESSAGE_REQUEST_CLOSED);
            this.F1 = j;
            this.G1 = j2;
            return;
        }
        int i = this.H1;
        if (i == this.C0.length) {
            p12.i("MediaCodecRenderer", "Too many stream changes, so dropping offset: " + this.C0[this.H1 - 1]);
        } else {
            this.H1 = i + 1;
        }
        long[] jArr = this.B0;
        int i2 = this.H1;
        jArr[i2 - 1] = j;
        this.C0[i2 - 1] = j2;
        this.D0[i2 - 1] = this.x1;
    }

    public abstract void M0(j jVar, MediaFormat mediaFormat) throws ExoPlaybackException;

    public void N0(long j) {
        while (true) {
            int i = this.H1;
            if (i == 0 || j < this.D0[0]) {
                return;
            }
            long[] jArr = this.B0;
            this.F1 = jArr[0];
            this.G1 = this.C0[0];
            int i2 = i - 1;
            this.H1 = i2;
            System.arraycopy(jArr, 1, jArr, 0, i2);
            long[] jArr2 = this.C0;
            System.arraycopy(jArr2, 1, jArr2, 0, this.H1);
            long[] jArr3 = this.D0;
            System.arraycopy(jArr3, 1, jArr3, 0, this.H1);
            O0();
        }
    }

    public void O0() {
    }

    public abstract void P0(DecoderInputBuffer decoderInputBuffer) throws ExoPlaybackException;

    public final void Q() throws ExoPlaybackException {
        ii.g(!this.z1);
        y81 B = B();
        this.w0.h();
        do {
            this.w0.h();
            int N = N(B, this.w0, 0);
            if (N == -5) {
                L0(B);
                return;
            } else if (N != -4) {
                if (N != -3) {
                    throw new IllegalStateException();
                }
                return;
            } else if (this.w0.p()) {
                this.z1 = true;
                return;
            } else {
                if (this.B1) {
                    j jVar = (j) ii.e(this.E0);
                    this.F0 = jVar;
                    M0(jVar, null);
                    this.B1 = false;
                }
                this.w0.x();
            }
        } while (this.x0.C(this.w0));
        this.o1 = true;
    }

    @TargetApi(23)
    public final void Q0() throws ExoPlaybackException {
        int i = this.t1;
        if (i == 1) {
            k0();
        } else if (i == 2) {
            k0();
            m1();
        } else if (i != 3) {
            this.A1 = true;
            W0();
        } else {
            U0();
        }
    }

    public final boolean R(long j, long j2) throws ExoPlaybackException {
        boolean z;
        ii.g(!this.A1);
        if (this.x0.I()) {
            ko koVar = this.x0;
            if (!R0(j, j2, null, koVar.g0, this.j1, 0, koVar.H(), this.x0.E(), this.x0.o(), this.x0.p(), this.F0)) {
                return false;
            }
            N0(this.x0.G());
            this.x0.h();
            z = false;
        } else {
            z = false;
        }
        if (this.z1) {
            this.A1 = true;
            return z;
        }
        if (this.o1) {
            ii.g(this.x0.C(this.w0));
            this.o1 = z;
        }
        if (this.p1) {
            if (this.x0.I()) {
                return true;
            }
            d0();
            this.p1 = z;
            G0();
            if (!this.n1) {
                return z;
            }
        }
        Q();
        if (this.x0.I()) {
            this.x0.x();
        }
        if (this.x0.I() || this.z1 || this.p1) {
            return true;
        }
        return z;
    }

    public abstract boolean R0(long j, long j2, d dVar, ByteBuffer byteBuffer, int i, int i2, int i3, long j3, boolean z, boolean z2, j jVar) throws ExoPlaybackException;

    public abstract jf0 S(e eVar, j jVar, j jVar2);

    public final void S0() {
        this.w1 = true;
        MediaFormat e = this.N0.e();
        if (this.V0 != 0 && e.getInteger("width") == 32 && e.getInteger("height") == 32) {
            this.e1 = true;
            return;
        }
        if (this.c1) {
            e.setInteger("channel-count", 1);
        }
        this.P0 = e;
        this.Q0 = true;
    }

    public final int T(String str) {
        int i = androidx.media3.common.util.b.a;
        if (i <= 25 && "OMX.Exynos.avc.dec.secure".equals(str)) {
            String str2 = androidx.media3.common.util.b.d;
            if (str2.startsWith("SM-T585") || str2.startsWith("SM-A510") || str2.startsWith("SM-A520") || str2.startsWith("SM-J700")) {
                return 2;
            }
        }
        if (i < 24) {
            if ("OMX.Nvidia.h264.decode".equals(str) || "OMX.Nvidia.h264.decode.secure".equals(str)) {
                String str3 = androidx.media3.common.util.b.b;
                return ("flounder".equals(str3) || "flounder_lte".equals(str3) || "grouper".equals(str3) || "tilapia".equals(str3)) ? 1 : 0;
            }
            return 0;
        }
        return 0;
    }

    public final boolean T0(int i) throws ExoPlaybackException {
        y81 B = B();
        this.u0.h();
        int N = N(B, this.u0, i | 4);
        if (N == -5) {
            L0(B);
            return true;
        } else if (N == -4 && this.u0.p()) {
            this.z1 = true;
            Q0();
            return false;
        } else {
            return false;
        }
    }

    public final void U0() throws ExoPlaybackException {
        V0();
        G0();
    }

    /* JADX WARN: Multi-variable type inference failed */
    /* JADX WARN: Type inference failed for: r0v0, types: [androidx.media3.exoplayer.drm.DrmSession, android.media.MediaCrypto] */
    public void V0() {
        try {
            d dVar = this.N0;
            if (dVar != null) {
                dVar.a();
                this.E1.b++;
                K0(this.U0.a);
            }
            this.N0 = null;
            try {
                MediaCrypto mediaCrypto = this.I0;
                if (mediaCrypto != null) {
                    mediaCrypto.release();
                }
            } finally {
            }
        } catch (Throwable th) {
            this.N0 = null;
            try {
                MediaCrypto mediaCrypto2 = this.I0;
                if (mediaCrypto2 != null) {
                    mediaCrypto2.release();
                }
                throw th;
            } finally {
            }
        }
    }

    public void W0() throws ExoPlaybackException {
    }

    public void X0() {
        Z0();
        a1();
        this.h1 = CellBase.ID_SYSTEM_MESSAGE_REQUEST_CLOSED;
        this.v1 = false;
        this.u1 = false;
        this.d1 = false;
        this.e1 = false;
        this.l1 = false;
        this.m1 = false;
        this.z0.clear();
        this.x1 = CellBase.ID_SYSTEM_MESSAGE_REQUEST_CLOSED;
        this.y1 = CellBase.ID_SYSTEM_MESSAGE_REQUEST_CLOSED;
        et etVar = this.g1;
        if (etVar != null) {
            etVar.c();
        }
        this.s1 = 0;
        this.t1 = 0;
        this.r1 = this.q1 ? 1 : 0;
    }

    public void Y0() {
        X0();
        this.D1 = null;
        this.g1 = null;
        this.S0 = null;
        this.U0 = null;
        this.O0 = null;
        this.P0 = null;
        this.Q0 = false;
        this.w1 = false;
        this.R0 = -1.0f;
        this.V0 = 0;
        this.W0 = false;
        this.X0 = false;
        this.Y0 = false;
        this.Z0 = false;
        this.a1 = false;
        this.b1 = false;
        this.c1 = false;
        this.f1 = false;
        this.q1 = false;
        this.r1 = 0;
        this.J0 = false;
    }

    public final void Z0() {
        this.i1 = -1;
        this.v0.g0 = null;
    }

    @Override // androidx.media3.exoplayer.n
    public final int a(j jVar) throws ExoPlaybackException {
        try {
            return j1(this.r0, jVar);
        } catch (MediaCodecUtil.DecoderQueryException e) {
            throw y(e, jVar, PlaybackException.ERROR_CODE_DECODER_QUERY_FAILED);
        }
    }

    public final void a1() {
        this.j1 = -1;
        this.k1 = null;
    }

    public final void b1(DrmSession drmSession) {
        or0.a(this.G0, drmSession);
        this.G0 = drmSession;
    }

    public MediaCodecDecoderException c0(Throwable th, e eVar) {
        return new MediaCodecDecoderException(th, eVar);
    }

    public final void c1() {
        this.C1 = true;
    }

    @Override // androidx.media3.exoplayer.m
    public boolean d() {
        return this.A1;
    }

    public final void d0() {
        this.p1 = false;
        this.x0.h();
        this.w0.h();
        this.o1 = false;
        this.n1 = false;
    }

    public final void d1(ExoPlaybackException exoPlaybackException) {
        this.D1 = exoPlaybackException;
    }

    public final boolean e0() {
        if (this.u1) {
            this.s1 = 1;
            if (!this.X0 && !this.Z0) {
                this.t1 = 1;
            } else {
                this.t1 = 3;
                return false;
            }
        }
        return true;
    }

    public final void e1(DrmSession drmSession) {
        or0.a(this.H0, drmSession);
        this.H0 = drmSession;
    }

    @Override // androidx.media3.exoplayer.m
    public boolean f() {
        return this.E0 != null && (F() || z0() || (this.h1 != CellBase.ID_SYSTEM_MESSAGE_REQUEST_CLOSED && SystemClock.elapsedRealtime() < this.h1));
    }

    public final void f0() throws ExoPlaybackException {
        if (this.u1) {
            this.s1 = 1;
            this.t1 = 3;
            return;
        }
        U0();
    }

    public final boolean f1(long j) {
        return this.K0 == CellBase.ID_SYSTEM_MESSAGE_REQUEST_CLOSED || SystemClock.elapsedRealtime() - j < this.K0;
    }

    @TargetApi(23)
    public final boolean g0() throws ExoPlaybackException {
        if (this.u1) {
            this.s1 = 1;
            if (!this.X0 && !this.Z0) {
                this.t1 = 2;
            } else {
                this.t1 = 3;
                return false;
            }
        } else {
            m1();
        }
        return true;
    }

    public boolean g1(e eVar) {
        return true;
    }

    public final boolean h0(long j, long j2) throws ExoPlaybackException {
        boolean z;
        boolean R0;
        int i;
        if (!z0()) {
            if (this.a1 && this.v1) {
                try {
                    i = this.N0.i(this.A0);
                } catch (IllegalStateException unused) {
                    Q0();
                    if (this.A1) {
                        V0();
                    }
                    return false;
                }
            } else {
                i = this.N0.i(this.A0);
            }
            if (i < 0) {
                if (i == -2) {
                    S0();
                    return true;
                }
                if (this.f1 && (this.z1 || this.s1 == 2)) {
                    Q0();
                }
                return false;
            } else if (this.e1) {
                this.e1 = false;
                this.N0.j(i, false);
                return true;
            } else {
                MediaCodec.BufferInfo bufferInfo = this.A0;
                if (bufferInfo.size == 0 && (bufferInfo.flags & 4) != 0) {
                    Q0();
                    return false;
                }
                this.j1 = i;
                ByteBuffer o = this.N0.o(i);
                this.k1 = o;
                if (o != null) {
                    o.position(this.A0.offset);
                    ByteBuffer byteBuffer = this.k1;
                    MediaCodec.BufferInfo bufferInfo2 = this.A0;
                    byteBuffer.limit(bufferInfo2.offset + bufferInfo2.size);
                }
                if (this.b1) {
                    MediaCodec.BufferInfo bufferInfo3 = this.A0;
                    if (bufferInfo3.presentationTimeUs == 0 && (bufferInfo3.flags & 4) != 0) {
                        long j3 = this.x1;
                        if (j3 != CellBase.ID_SYSTEM_MESSAGE_REQUEST_CLOSED) {
                            bufferInfo3.presentationTimeUs = j3;
                        }
                    }
                }
                this.l1 = C0(this.A0.presentationTimeUs);
                long j4 = this.y1;
                long j5 = this.A0.presentationTimeUs;
                this.m1 = j4 == j5;
                n1(j5);
            }
        }
        if (this.a1 && this.v1) {
            try {
                d dVar = this.N0;
                ByteBuffer byteBuffer2 = this.k1;
                int i2 = this.j1;
                MediaCodec.BufferInfo bufferInfo4 = this.A0;
                z = false;
                try {
                    R0 = R0(j, j2, dVar, byteBuffer2, i2, bufferInfo4.flags, 1, bufferInfo4.presentationTimeUs, this.l1, this.m1, this.F0);
                } catch (IllegalStateException unused2) {
                    Q0();
                    if (this.A1) {
                        V0();
                    }
                    return z;
                }
            } catch (IllegalStateException unused3) {
                z = false;
            }
        } else {
            z = false;
            d dVar2 = this.N0;
            ByteBuffer byteBuffer3 = this.k1;
            int i3 = this.j1;
            MediaCodec.BufferInfo bufferInfo5 = this.A0;
            R0 = R0(j, j2, dVar2, byteBuffer3, i3, bufferInfo5.flags, 1, bufferInfo5.presentationTimeUs, this.l1, this.m1, this.F0);
        }
        if (R0) {
            N0(this.A0.presentationTimeUs);
            boolean z2 = (this.A0.flags & 4) != 0 ? true : z;
            a1();
            if (!z2) {
                return true;
            }
            Q0();
        }
        return z;
    }

    public boolean h1() {
        return false;
    }

    public final boolean i0(e eVar, j jVar, DrmSession drmSession, DrmSession drmSession2) throws ExoPlaybackException {
        cc1 u0;
        if (drmSession == drmSession2) {
            return false;
        }
        if (drmSession2 == null || drmSession == null || androidx.media3.common.util.b.a < 23) {
            return true;
        }
        UUID uuid = ft.e;
        if (uuid.equals(drmSession.b()) || uuid.equals(drmSession2.b()) || (u0 = u0(drmSession2)) == null) {
            return true;
        }
        return !eVar.f && (u0.c ? false : drmSession2.f(jVar.p0));
    }

    public boolean i1(j jVar) {
        return false;
    }

    public final boolean j0() throws ExoPlaybackException {
        int i;
        if (this.N0 == null || (i = this.s1) == 2 || this.z1) {
            return false;
        }
        if (i == 0 && h1()) {
            f0();
        }
        if (this.i1 < 0) {
            int h = this.N0.h();
            this.i1 = h;
            if (h < 0) {
                return false;
            }
            this.v0.g0 = this.N0.l(h);
            this.v0.h();
        }
        if (this.s1 == 1) {
            if (!this.f1) {
                this.v1 = true;
                this.N0.n(this.i1, 0, 0, 0L, 4);
                Z0();
            }
            this.s1 = 2;
            return false;
        } else if (this.d1) {
            this.d1 = false;
            ByteBuffer byteBuffer = this.v0.g0;
            byte[] bArr = I1;
            byteBuffer.put(bArr);
            this.N0.n(this.i1, 0, bArr.length, 0L, 0);
            Z0();
            this.u1 = true;
            return true;
        } else {
            if (this.r1 == 1) {
                for (int i2 = 0; i2 < this.O0.r0.size(); i2++) {
                    this.v0.g0.put(this.O0.r0.get(i2));
                }
                this.r1 = 2;
            }
            int position = this.v0.g0.position();
            y81 B = B();
            try {
                int N = N(B, this.v0, 0);
                if (i()) {
                    this.y1 = this.x1;
                }
                if (N == -3) {
                    return false;
                }
                if (N == -5) {
                    if (this.r1 == 2) {
                        this.v0.h();
                        this.r1 = 1;
                    }
                    L0(B);
                    return true;
                } else if (this.v0.p()) {
                    if (this.r1 == 2) {
                        this.v0.h();
                        this.r1 = 1;
                    }
                    this.z1 = true;
                    if (!this.u1) {
                        Q0();
                        return false;
                    }
                    try {
                        if (!this.f1) {
                            this.v1 = true;
                            this.N0.n(this.i1, 0, 0, 0L, 4);
                            Z0();
                        }
                        return false;
                    } catch (MediaCodec.CryptoException e) {
                        throw y(e, this.E0, androidx.media3.common.util.b.R(e.getErrorCode()));
                    }
                } else if (!this.u1 && !this.v0.s()) {
                    this.v0.h();
                    if (this.r1 == 2) {
                        this.r1 = 1;
                    }
                    return true;
                } else {
                    boolean y = this.v0.y();
                    if (y) {
                        this.v0.f0.b(position);
                    }
                    if (this.W0 && !y) {
                        wc2.b(this.v0.g0);
                        if (this.v0.g0.position() == 0) {
                            return true;
                        }
                        this.W0 = false;
                    }
                    DecoderInputBuffer decoderInputBuffer = this.v0;
                    long j = decoderInputBuffer.i0;
                    et etVar = this.g1;
                    if (etVar != null) {
                        j = etVar.d(this.E0, decoderInputBuffer);
                        this.x1 = Math.max(this.x1, this.g1.b(this.E0));
                    }
                    long j2 = j;
                    if (this.v0.o()) {
                        this.z0.add(Long.valueOf(j2));
                    }
                    if (this.B1) {
                        this.y0.a(j2, this.E0);
                        this.B1 = false;
                    }
                    this.x1 = Math.max(this.x1, j2);
                    this.v0.x();
                    if (this.v0.l()) {
                        y0(this.v0);
                    }
                    P0(this.v0);
                    try {
                        if (y) {
                            this.N0.d(this.i1, 0, this.v0.f0, j2, 0);
                        } else {
                            this.N0.n(this.i1, 0, this.v0.g0.limit(), j2, 0);
                        }
                        Z0();
                        this.u1 = true;
                        this.r1 = 0;
                        this.E1.c++;
                        return true;
                    } catch (MediaCodec.CryptoException e2) {
                        throw y(e2, this.E0, androidx.media3.common.util.b.R(e2.getErrorCode()));
                    }
                }
            } catch (DecoderInputBuffer.InsufficientCapacityException e3) {
                I0(e3);
                T0(0);
                k0();
                return true;
            }
        }
    }

    public abstract int j1(f fVar, j jVar) throws MediaCodecUtil.DecoderQueryException;

    public final void k0() {
        try {
            this.N0.flush();
        } finally {
            X0();
        }
    }

    public final boolean l0() throws ExoPlaybackException {
        boolean m0 = m0();
        if (m0) {
            G0();
        }
        return m0;
    }

    public final boolean l1(j jVar) throws ExoPlaybackException {
        if (androidx.media3.common.util.b.a >= 23 && this.N0 != null && this.t1 != 3 && getState() != 0) {
            float r0 = r0(this.M0, jVar, E());
            float f = this.R0;
            if (f == r0) {
                return true;
            }
            if (r0 == -1.0f) {
                f0();
                return false;
            } else if (f == -1.0f && r0 <= this.t0) {
                return true;
            } else {
                Bundle bundle = new Bundle();
                bundle.putFloat("operating-rate", r0);
                this.N0.f(bundle);
                this.R0 = r0;
            }
        }
        return true;
    }

    public boolean m0() {
        if (this.N0 == null) {
            return false;
        }
        int i = this.t1;
        if (i != 3 && !this.X0 && ((!this.Y0 || this.w1) && (!this.Z0 || !this.v1))) {
            if (i == 2) {
                int i2 = androidx.media3.common.util.b.a;
                ii.g(i2 >= 23);
                if (i2 >= 23) {
                    try {
                        m1();
                    } catch (ExoPlaybackException e) {
                        p12.j("MediaCodecRenderer", "Failed to update the DRM session, releasing the codec instead.", e);
                        V0();
                        return true;
                    }
                }
            }
            k0();
            return false;
        }
        V0();
        return true;
    }

    public final void m1() throws ExoPlaybackException {
        try {
            this.I0.setMediaDrmSession(u0(this.H0).b);
            b1(this.H0);
            this.s1 = 0;
            this.t1 = 0;
        } catch (MediaCryptoException e) {
            throw y(e, this.E0, PlaybackException.ERROR_CODE_DRM_SYSTEM_ERROR);
        }
    }

    public final List<e> n0(boolean z) throws MediaCodecUtil.DecoderQueryException {
        List<e> t0 = t0(this.r0, this.E0, z);
        if (t0.isEmpty() && z) {
            t0 = t0(this.r0, this.E0, false);
            if (!t0.isEmpty()) {
                p12.i("MediaCodecRenderer", "Drm session requires secure decoder for " + this.E0.p0 + ", but no secure decoder available. Trying to proceed with " + t0 + ".");
            }
        }
        return t0;
    }

    public final void n1(long j) throws ExoPlaybackException {
        boolean z;
        j j2 = this.y0.j(j);
        if (j2 == null && this.Q0) {
            j2 = this.y0.i();
        }
        if (j2 != null) {
            this.F0 = j2;
            z = true;
        } else {
            z = false;
        }
        if (z || (this.Q0 && this.F0 != null)) {
            M0(this.F0, this.P0);
            this.Q0 = false;
        }
    }

    @Override // androidx.media3.exoplayer.c, androidx.media3.exoplayer.m
    public void o(float f, float f2) throws ExoPlaybackException {
        this.L0 = f;
        this.M0 = f2;
        l1(this.O0);
    }

    public final d o0() {
        return this.N0;
    }

    public final e p0() {
        return this.U0;
    }

    @Override // androidx.media3.exoplayer.c, androidx.media3.exoplayer.n
    public final int q() {
        return 8;
    }

    public boolean q0() {
        return false;
    }

    @Override // androidx.media3.exoplayer.m
    public void r(long j, long j2) throws ExoPlaybackException {
        boolean z = false;
        if (this.C1) {
            this.C1 = false;
            Q0();
        }
        ExoPlaybackException exoPlaybackException = this.D1;
        if (exoPlaybackException == null) {
            try {
                if (this.A1) {
                    W0();
                    return;
                } else if (this.E0 != null || T0(2)) {
                    G0();
                    if (this.n1) {
                        v74.a("bypassRender");
                        while (R(j, j2)) {
                        }
                        v74.c();
                    } else if (this.N0 != null) {
                        long elapsedRealtime = SystemClock.elapsedRealtime();
                        v74.a("drainAndFeed");
                        while (h0(j, j2) && f1(elapsedRealtime)) {
                        }
                        while (j0() && f1(elapsedRealtime)) {
                        }
                        v74.c();
                    } else {
                        this.E1.d += P(j);
                        T0(1);
                    }
                    this.E1.c();
                    return;
                } else {
                    return;
                }
            } catch (IllegalStateException e) {
                if (D0(e)) {
                    I0(e);
                    if (androidx.media3.common.util.b.a >= 21 && F0(e)) {
                        z = true;
                    }
                    if (z) {
                        V0();
                    }
                    throw z(c0(e, p0()), this.E0, z, PlaybackException.ERROR_CODE_DECODING_FAILED);
                }
                throw e;
            }
        }
        this.D1 = null;
        throw exoPlaybackException;
    }

    public abstract float r0(float f, j jVar, j[] jVarArr);

    public final MediaFormat s0() {
        return this.P0;
    }

    public abstract List<e> t0(f fVar, j jVar, boolean z) throws MediaCodecUtil.DecoderQueryException;

    public final cc1 u0(DrmSession drmSession) throws ExoPlaybackException {
        qa0 g = drmSession.g();
        if (g != null && !(g instanceof cc1)) {
            throw y(new IllegalArgumentException("Expecting FrameworkCryptoConfig but found: " + g), this.E0, PlaybackException.ERROR_CODE_DRM_SCHEME_UNSUPPORTED);
        }
        return (cc1) g;
    }

    public abstract d.a v0(e eVar, j jVar, MediaCrypto mediaCrypto, float f);

    public final long w0() {
        return this.G1;
    }

    public float x0() {
        return this.L0;
    }

    public void y0(DecoderInputBuffer decoderInputBuffer) throws ExoPlaybackException {
    }

    public final boolean z0() {
        return this.j1 >= 0;
    }

    /* loaded from: classes.dex */
    public static class DecoderInitializationException extends Exception {
        public final e codecInfo;
        public final String diagnosticInfo;
        public final DecoderInitializationException fallbackDecoderInitializationException;
        public final String mimeType;
        public final boolean secureDecoderRequired;

        public DecoderInitializationException(j jVar, Throwable th, boolean z, int i) {
            this("Decoder init failed: [" + i + "], " + jVar, th, jVar.p0, z, null, a(i), null);
        }

        public static String a(int i) {
            String str = i < 0 ? "neg_" : "";
            return "com.google.android.exoplayer2.mediacodec.MediaCodecRenderer_" + str + Math.abs(i);
        }

        public static String c(Throwable th) {
            if (th instanceof MediaCodec.CodecException) {
                return ((MediaCodec.CodecException) th).getDiagnosticInfo();
            }
            return null;
        }

        public final DecoderInitializationException b(DecoderInitializationException decoderInitializationException) {
            return new DecoderInitializationException(getMessage(), getCause(), this.mimeType, this.secureDecoderRequired, this.codecInfo, this.diagnosticInfo, decoderInitializationException);
        }

        public DecoderInitializationException(j jVar, Throwable th, boolean z, e eVar) {
            this("Decoder init failed: " + eVar.a + ", " + jVar, th, jVar.p0, z, eVar, androidx.media3.common.util.b.a >= 21 ? c(th) : null, null);
        }

        public DecoderInitializationException(String str, Throwable th, String str2, boolean z, e eVar, String str3, DecoderInitializationException decoderInitializationException) {
            super(str, th);
            this.mimeType = str2;
            this.secureDecoderRequired = z;
            this.codecInfo = eVar;
            this.diagnosticInfo = str3;
            this.fallbackDecoderInitializationException = decoderInitializationException;
        }
    }
}
