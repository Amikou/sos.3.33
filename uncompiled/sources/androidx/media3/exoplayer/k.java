package androidx.media3.exoplayer;

import android.os.Looper;
import androidx.media3.common.u;
import java.util.concurrent.TimeoutException;
import zendesk.support.request.CellBase;

/* compiled from: PlayerMessage.java */
/* loaded from: classes.dex */
public final class k {
    public final b a;
    public final a b;
    public final tz c;
    public final u d;
    public int e;
    public Object f;
    public Looper g;
    public int h;
    public long i = CellBase.ID_SYSTEM_MESSAGE_REQUEST_CLOSED;
    public boolean j = true;
    public boolean k;
    public boolean l;
    public boolean m;
    public boolean n;

    /* compiled from: PlayerMessage.java */
    /* loaded from: classes.dex */
    public interface a {
        void c(k kVar);
    }

    /* compiled from: PlayerMessage.java */
    /* loaded from: classes.dex */
    public interface b {
        void s(int i, Object obj) throws ExoPlaybackException;
    }

    public k(a aVar, b bVar, u uVar, int i, tz tzVar, Looper looper) {
        this.b = aVar;
        this.a = bVar;
        this.d = uVar;
        this.g = looper;
        this.c = tzVar;
        this.h = i;
    }

    public synchronized boolean a(long j) throws InterruptedException, TimeoutException {
        boolean z;
        ii.g(this.k);
        ii.g(this.g.getThread() != Thread.currentThread());
        long b2 = this.c.b() + j;
        while (true) {
            z = this.m;
            if (z || j <= 0) {
                break;
            }
            this.c.d();
            wait(j);
            j = b2 - this.c.b();
        }
        if (!z) {
            throw new TimeoutException("Message delivery timed out.");
        }
        return this.l;
    }

    public boolean b() {
        return this.j;
    }

    public Looper c() {
        return this.g;
    }

    public int d() {
        return this.h;
    }

    public Object e() {
        return this.f;
    }

    public long f() {
        return this.i;
    }

    public b g() {
        return this.a;
    }

    public u h() {
        return this.d;
    }

    public int i() {
        return this.e;
    }

    public synchronized boolean j() {
        return this.n;
    }

    public synchronized void k(boolean z) {
        this.l = z | this.l;
        this.m = true;
        notifyAll();
    }

    public k l() {
        ii.g(!this.k);
        if (this.i == CellBase.ID_SYSTEM_MESSAGE_REQUEST_CLOSED) {
            ii.a(this.j);
        }
        this.k = true;
        this.b.c(this);
        return this;
    }

    public k m(Object obj) {
        ii.g(!this.k);
        this.f = obj;
        return this;
    }

    public k n(int i) {
        ii.g(!this.k);
        this.e = i;
        return this;
    }
}
