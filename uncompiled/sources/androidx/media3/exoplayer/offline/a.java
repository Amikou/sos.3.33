package androidx.media3.exoplayer.offline;

import android.net.Uri;
import androidx.media3.common.StreamKey;
import androidx.media3.exoplayer.upstream.d;
import defpackage.l41;
import java.io.IOException;
import java.io.InputStream;
import java.util.List;

/* compiled from: FilteringManifestParser.java */
/* loaded from: classes.dex */
public final class a<T extends l41<T>> implements d.a<T> {
    public final d.a<? extends T> a;
    public final List<StreamKey> b;

    public a(d.a<? extends T> aVar, List<StreamKey> list) {
        this.a = aVar;
        this.b = list;
    }

    @Override // androidx.media3.exoplayer.upstream.d.a
    /* renamed from: b */
    public T a(Uri uri, InputStream inputStream) throws IOException {
        T a = this.a.a(uri, inputStream);
        List<StreamKey> list = this.b;
        return (list == null || list.isEmpty()) ? a : (T) a.a(this.b);
    }
}
