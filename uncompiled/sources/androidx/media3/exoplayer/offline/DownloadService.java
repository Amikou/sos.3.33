package androidx.media3.exoplayer.offline;

import android.app.Service;
import android.content.Intent;
import android.os.IBinder;
import androidx.media3.common.util.b;
import java.util.HashMap;

/* loaded from: classes.dex */
public abstract class DownloadService extends Service {
    public static final HashMap<Class<? extends DownloadService>, a> m0 = new HashMap<>();
    public final String a;
    public final int f0;
    public final int g0;
    public a h0;
    public int i0;
    public boolean j0;
    public boolean k0;
    public boolean l0;

    /* loaded from: classes.dex */
    public static final class a {
        public final lq0 a;
        public DownloadService b;

        public void b(DownloadService downloadService) {
            ii.g(this.b == null);
            this.b = downloadService;
            throw null;
        }

        public void c(DownloadService downloadService) {
            ii.g(this.b == downloadService);
            this.b = null;
        }
    }

    public abstract lq0 a();

    @Override // android.app.Service
    public final IBinder onBind(Intent intent) {
        throw new UnsupportedOperationException();
    }

    @Override // android.app.Service
    public void onCreate() {
        String str = this.a;
        if (str != null) {
            si2.a(this, str, this.f0, this.g0, 2);
        }
        a aVar = m0.get(getClass());
        if (aVar != null) {
            this.h0 = aVar;
            aVar.b(this);
            return;
        }
        int i = b.a;
        a();
        throw null;
    }

    @Override // android.app.Service
    public void onDestroy() {
        ((a) ii.e(this.h0)).c(this);
    }

    /* JADX WARN: Can't fix incorrect switch cases order, some code will duplicate */
    /* JADX WARN: Code restructure failed: missing block: B:48:0x009a, code lost:
        if (r2.equals("androidx.media3.exoplayer.downloadService.action.REMOVE_DOWNLOAD") == false) goto L14;
     */
    @Override // android.app.Service
    /*
        Code decompiled incorrectly, please refer to instructions dump.
        To view partially-correct code enable 'Show inconsistent code' option in preferences
    */
    public int onStartCommand(android.content.Intent r8, int r9, int r10) {
        /*
            Method dump skipped, instructions count: 348
            To view this dump change 'Code comments level' option to 'DEBUG'
        */
        throw new UnsupportedOperationException("Method not decompiled: androidx.media3.exoplayer.offline.DownloadService.onStartCommand(android.content.Intent, int, int):int");
    }

    @Override // android.app.Service
    public void onTaskRemoved(Intent intent) {
        this.k0 = true;
    }
}
