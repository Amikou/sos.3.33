package androidx.media3.exoplayer.offline;

import android.net.Uri;
import android.os.Parcel;
import android.os.Parcelable;
import androidx.media3.common.StreamKey;
import androidx.media3.common.util.b;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

/* loaded from: classes.dex */
public final class DownloadRequest implements Parcelable {
    public static final Parcelable.Creator<DownloadRequest> CREATOR = new a();
    public final String a;
    public final Uri f0;
    public final String g0;
    public final List<StreamKey> h0;
    public final byte[] i0;
    public final String j0;
    public final byte[] k0;

    /* loaded from: classes.dex */
    public class a implements Parcelable.Creator<DownloadRequest> {
        @Override // android.os.Parcelable.Creator
        /* renamed from: a */
        public DownloadRequest createFromParcel(Parcel parcel) {
            return new DownloadRequest(parcel);
        }

        @Override // android.os.Parcelable.Creator
        /* renamed from: b */
        public DownloadRequest[] newArray(int i) {
            return new DownloadRequest[i];
        }
    }

    public DownloadRequest(Parcel parcel) {
        this.a = (String) b.j(parcel.readString());
        this.f0 = Uri.parse((String) b.j(parcel.readString()));
        this.g0 = parcel.readString();
        int readInt = parcel.readInt();
        ArrayList arrayList = new ArrayList(readInt);
        for (int i = 0; i < readInt; i++) {
            arrayList.add((StreamKey) parcel.readParcelable(StreamKey.class.getClassLoader()));
        }
        this.h0 = Collections.unmodifiableList(arrayList);
        this.i0 = parcel.createByteArray();
        this.j0 = parcel.readString();
        this.k0 = (byte[]) b.j(parcel.createByteArray());
    }

    @Override // android.os.Parcelable
    public int describeContents() {
        return 0;
    }

    public boolean equals(Object obj) {
        if (obj instanceof DownloadRequest) {
            DownloadRequest downloadRequest = (DownloadRequest) obj;
            return this.a.equals(downloadRequest.a) && this.f0.equals(downloadRequest.f0) && b.c(this.g0, downloadRequest.g0) && this.h0.equals(downloadRequest.h0) && Arrays.equals(this.i0, downloadRequest.i0) && b.c(this.j0, downloadRequest.j0) && Arrays.equals(this.k0, downloadRequest.k0);
        }
        return false;
    }

    public final int hashCode() {
        int hashCode = ((this.a.hashCode() * 31 * 31) + this.f0.hashCode()) * 31;
        String str = this.g0;
        int hashCode2 = (((((hashCode + (str != null ? str.hashCode() : 0)) * 31) + this.h0.hashCode()) * 31) + Arrays.hashCode(this.i0)) * 31;
        String str2 = this.j0;
        return ((hashCode2 + (str2 != null ? str2.hashCode() : 0)) * 31) + Arrays.hashCode(this.k0);
    }

    public String toString() {
        return this.g0 + ":" + this.a;
    }

    @Override // android.os.Parcelable
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeString(this.a);
        parcel.writeString(this.f0.toString());
        parcel.writeString(this.g0);
        parcel.writeInt(this.h0.size());
        for (int i2 = 0; i2 < this.h0.size(); i2++) {
            parcel.writeParcelable(this.h0.get(i2), 0);
        }
        parcel.writeByteArray(this.i0);
        parcel.writeString(this.j0);
        parcel.writeByteArray(this.k0);
    }
}
