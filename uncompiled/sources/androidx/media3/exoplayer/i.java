package androidx.media3.exoplayer;

import androidx.media3.common.u;
import androidx.media3.exoplayer.source.j;
import androidx.media3.exoplayer.source.r;
import zendesk.support.request.CellBase;

/* compiled from: MediaPeriodHolder.java */
/* loaded from: classes.dex */
public final class i {
    public final androidx.media3.exoplayer.source.i a;
    public final Object b;
    public final r[] c;
    public boolean d;
    public boolean e;
    public k62 f;
    public boolean g;
    public final boolean[] h;
    public final n[] i;
    public final androidx.media3.exoplayer.trackselection.f j;
    public final j k;
    public i l;
    public c84 m;
    public androidx.media3.exoplayer.trackselection.g n;
    public long o;

    public i(n[] nVarArr, long j, androidx.media3.exoplayer.trackselection.f fVar, gb gbVar, j jVar, k62 k62Var, androidx.media3.exoplayer.trackselection.g gVar) {
        this.i = nVarArr;
        this.o = j;
        this.j = fVar;
        this.k = jVar;
        j.b bVar = k62Var.a;
        this.b = bVar.a;
        this.f = k62Var;
        this.m = c84.h0;
        this.n = gVar;
        this.c = new r[nVarArr.length];
        this.h = new boolean[nVarArr.length];
        this.a = e(bVar, jVar, gbVar, k62Var.b, k62Var.d);
    }

    public static androidx.media3.exoplayer.source.i e(j.b bVar, j jVar, gb gbVar, long j, long j2) {
        androidx.media3.exoplayer.source.i h = jVar.h(bVar, gbVar, j);
        return j2 != CellBase.ID_SYSTEM_MESSAGE_REQUEST_CLOSED ? new androidx.media3.exoplayer.source.c(h, true, 0L, j2) : h;
    }

    public static void u(j jVar, androidx.media3.exoplayer.source.i iVar) {
        try {
            if (iVar instanceof androidx.media3.exoplayer.source.c) {
                jVar.z(((androidx.media3.exoplayer.source.c) iVar).a);
            } else {
                jVar.z(iVar);
            }
        } catch (RuntimeException e) {
            p12.d("MediaPeriodHolder", "Period release failed.", e);
        }
    }

    public void A() {
        androidx.media3.exoplayer.source.i iVar = this.a;
        if (iVar instanceof androidx.media3.exoplayer.source.c) {
            long j = this.f.d;
            if (j == CellBase.ID_SYSTEM_MESSAGE_REQUEST_CLOSED) {
                j = Long.MIN_VALUE;
            }
            ((androidx.media3.exoplayer.source.c) iVar).u(0L, j);
        }
    }

    public long a(androidx.media3.exoplayer.trackselection.g gVar, long j, boolean z) {
        return b(gVar, j, z, new boolean[this.i.length]);
    }

    public long b(androidx.media3.exoplayer.trackselection.g gVar, long j, boolean z, boolean[] zArr) {
        int i = 0;
        while (true) {
            boolean z2 = true;
            if (i >= gVar.a) {
                break;
            }
            boolean[] zArr2 = this.h;
            if (z || !gVar.b(this.n, i)) {
                z2 = false;
            }
            zArr2[i] = z2;
            i++;
        }
        g(this.c);
        f();
        this.n = gVar;
        h();
        long n = this.a.n(gVar.c, this.h, this.c, zArr, j);
        c(this.c);
        this.e = false;
        int i2 = 0;
        while (true) {
            r[] rVarArr = this.c;
            if (i2 >= rVarArr.length) {
                return n;
            }
            if (rVarArr[i2] != null) {
                ii.g(gVar.c(i2));
                if (this.i[i2].h() != -2) {
                    this.e = true;
                }
            } else {
                ii.g(gVar.c[i2] == null);
            }
            i2++;
        }
    }

    public final void c(r[] rVarArr) {
        int i = 0;
        while (true) {
            n[] nVarArr = this.i;
            if (i >= nVarArr.length) {
                return;
            }
            if (nVarArr[i].h() == -2 && this.n.c(i)) {
                rVarArr[i] = new tu0();
            }
            i++;
        }
    }

    public void d(long j) {
        ii.g(r());
        this.a.d(y(j));
    }

    public final void f() {
        if (!r()) {
            return;
        }
        int i = 0;
        while (true) {
            androidx.media3.exoplayer.trackselection.g gVar = this.n;
            if (i >= gVar.a) {
                return;
            }
            boolean c = gVar.c(i);
            androidx.media3.exoplayer.trackselection.c cVar = this.n.c[i];
            if (c && cVar != null) {
                cVar.g();
            }
            i++;
        }
    }

    public final void g(r[] rVarArr) {
        int i = 0;
        while (true) {
            n[] nVarArr = this.i;
            if (i >= nVarArr.length) {
                return;
            }
            if (nVarArr[i].h() == -2) {
                rVarArr[i] = null;
            }
            i++;
        }
    }

    public final void h() {
        if (!r()) {
            return;
        }
        int i = 0;
        while (true) {
            androidx.media3.exoplayer.trackselection.g gVar = this.n;
            if (i >= gVar.a) {
                return;
            }
            boolean c = gVar.c(i);
            androidx.media3.exoplayer.trackselection.c cVar = this.n.c[i];
            if (c && cVar != null) {
                cVar.k();
            }
            i++;
        }
    }

    public long i() {
        if (!this.d) {
            return this.f.b;
        }
        long g = this.e ? this.a.g() : Long.MIN_VALUE;
        return g == Long.MIN_VALUE ? this.f.e : g;
    }

    public i j() {
        return this.l;
    }

    public long k() {
        if (this.d) {
            return this.a.a();
        }
        return 0L;
    }

    public long l() {
        return this.o;
    }

    public long m() {
        return this.f.b + this.o;
    }

    public c84 n() {
        return this.m;
    }

    public androidx.media3.exoplayer.trackselection.g o() {
        return this.n;
    }

    public void p(float f, u uVar) throws ExoPlaybackException {
        this.d = true;
        this.m = this.a.r();
        androidx.media3.exoplayer.trackselection.g v = v(f, uVar);
        k62 k62Var = this.f;
        long j = k62Var.b;
        long j2 = k62Var.e;
        if (j2 != CellBase.ID_SYSTEM_MESSAGE_REQUEST_CLOSED && j >= j2) {
            j = Math.max(0L, j2 - 1);
        }
        long a = a(v, j, false);
        long j3 = this.o;
        k62 k62Var2 = this.f;
        this.o = j3 + (k62Var2.b - a);
        this.f = k62Var2.b(a);
    }

    public boolean q() {
        return this.d && (!this.e || this.a.g() == Long.MIN_VALUE);
    }

    public final boolean r() {
        return this.l == null;
    }

    public void s(long j) {
        ii.g(r());
        if (this.d) {
            this.a.h(y(j));
        }
    }

    public void t() {
        f();
        u(this.k, this.a);
    }

    public androidx.media3.exoplayer.trackselection.g v(float f, u uVar) throws ExoPlaybackException {
        androidx.media3.exoplayer.trackselection.c[] cVarArr;
        androidx.media3.exoplayer.trackselection.g h = this.j.h(this.i, n(), this.f.a, uVar);
        for (androidx.media3.exoplayer.trackselection.c cVar : h.c) {
            if (cVar != null) {
                cVar.p(f);
            }
        }
        return h;
    }

    public void w(i iVar) {
        if (iVar == this.l) {
            return;
        }
        f();
        this.l = iVar;
        h();
    }

    public void x(long j) {
        this.o = j;
    }

    public long y(long j) {
        return j - l();
    }

    public long z(long j) {
        return j + l();
    }
}
