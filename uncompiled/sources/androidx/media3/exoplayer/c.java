package androidx.media3.exoplayer;

import androidx.media3.decoder.DecoderInputBuffer;
import androidx.media3.exoplayer.source.r;
import java.io.IOException;

/* compiled from: BaseRenderer.java */
/* loaded from: classes.dex */
public abstract class c implements m, n {
    public final int a;
    public v63 g0;
    public int h0;
    public ks2 i0;
    public int j0;
    public r k0;
    public androidx.media3.common.j[] l0;
    public long m0;
    public boolean o0;
    public boolean p0;
    public final y81 f0 = new y81();
    public long n0 = Long.MIN_VALUE;

    public c(int i) {
        this.a = i;
    }

    public final v63 A() {
        return (v63) ii.e(this.g0);
    }

    public final y81 B() {
        this.f0.a();
        return this.f0;
    }

    public final int C() {
        return this.h0;
    }

    public final ks2 D() {
        return (ks2) ii.e(this.i0);
    }

    public final androidx.media3.common.j[] E() {
        return (androidx.media3.common.j[]) ii.e(this.l0);
    }

    public final boolean F() {
        return i() ? this.o0 : ((r) ii.e(this.k0)).f();
    }

    public abstract void G();

    public void H(boolean z, boolean z2) throws ExoPlaybackException {
    }

    public abstract void I(long j, boolean z) throws ExoPlaybackException;

    public void J() {
    }

    public void K() throws ExoPlaybackException {
    }

    public void L() {
    }

    public abstract void M(androidx.media3.common.j[] jVarArr, long j, long j2) throws ExoPlaybackException;

    public final int N(y81 y81Var, DecoderInputBuffer decoderInputBuffer, int i) {
        int p = ((r) ii.e(this.k0)).p(y81Var, decoderInputBuffer, i);
        if (p == -4) {
            if (decoderInputBuffer.p()) {
                this.n0 = Long.MIN_VALUE;
                return this.o0 ? -4 : -3;
            }
            long j = decoderInputBuffer.i0 + this.m0;
            decoderInputBuffer.i0 = j;
            this.n0 = Math.max(this.n0, j);
        } else if (p == -5) {
            androidx.media3.common.j jVar = (androidx.media3.common.j) ii.e(y81Var.b);
            if (jVar.t0 != Long.MAX_VALUE) {
                y81Var.b = jVar.b().i0(jVar.t0 + this.m0).E();
            }
        }
        return p;
    }

    public final void O(long j, boolean z) throws ExoPlaybackException {
        this.o0 = false;
        this.n0 = j;
        I(j, z);
    }

    public int P(long j) {
        return ((r) ii.e(this.k0)).m(j - this.m0);
    }

    @Override // androidx.media3.exoplayer.m
    public final r c() {
        return this.k0;
    }

    @Override // androidx.media3.exoplayer.m
    public final void g() {
        ii.g(this.j0 == 1);
        this.f0.a();
        this.j0 = 0;
        this.k0 = null;
        this.l0 = null;
        this.o0 = false;
        G();
    }

    @Override // androidx.media3.exoplayer.m
    public final int getState() {
        return this.j0;
    }

    @Override // androidx.media3.exoplayer.m, androidx.media3.exoplayer.n
    public final int h() {
        return this.a;
    }

    @Override // androidx.media3.exoplayer.m
    public final boolean i() {
        return this.n0 == Long.MIN_VALUE;
    }

    @Override // androidx.media3.exoplayer.m
    public final void j(v63 v63Var, androidx.media3.common.j[] jVarArr, r rVar, long j, boolean z, boolean z2, long j2, long j3) throws ExoPlaybackException {
        ii.g(this.j0 == 0);
        this.g0 = v63Var;
        this.j0 = 1;
        H(z, z2);
        l(jVarArr, rVar, j2, j3);
        O(j, z);
    }

    @Override // androidx.media3.exoplayer.m
    public final void k() {
        this.o0 = true;
    }

    @Override // androidx.media3.exoplayer.m
    public final void l(androidx.media3.common.j[] jVarArr, r rVar, long j, long j2) throws ExoPlaybackException {
        ii.g(!this.o0);
        this.k0 = rVar;
        if (this.n0 == Long.MIN_VALUE) {
            this.n0 = j;
        }
        this.l0 = jVarArr;
        this.m0 = j2;
        M(jVarArr, j, j2);
    }

    @Override // androidx.media3.exoplayer.m
    public final n m() {
        return this;
    }

    @Override // androidx.media3.exoplayer.m
    public /* synthetic */ void o(float f, float f2) {
        l.a(this, f, f2);
    }

    @Override // androidx.media3.exoplayer.m
    public final void p(int i, ks2 ks2Var) {
        this.h0 = i;
        this.i0 = ks2Var;
    }

    @Override // androidx.media3.exoplayer.n
    public int q() throws ExoPlaybackException {
        return 0;
    }

    @Override // androidx.media3.exoplayer.m
    public final void reset() {
        ii.g(this.j0 == 0);
        this.f0.a();
        J();
    }

    @Override // androidx.media3.exoplayer.k.b
    public void s(int i, Object obj) throws ExoPlaybackException {
    }

    @Override // androidx.media3.exoplayer.m
    public final void start() throws ExoPlaybackException {
        ii.g(this.j0 == 1);
        this.j0 = 2;
        K();
    }

    @Override // androidx.media3.exoplayer.m
    public final void stop() {
        ii.g(this.j0 == 2);
        this.j0 = 1;
        L();
    }

    @Override // androidx.media3.exoplayer.m
    public final void t() throws IOException {
        ((r) ii.e(this.k0)).b();
    }

    @Override // androidx.media3.exoplayer.m
    public final long u() {
        return this.n0;
    }

    @Override // androidx.media3.exoplayer.m
    public final void v(long j) throws ExoPlaybackException {
        O(j, false);
    }

    @Override // androidx.media3.exoplayer.m
    public final boolean w() {
        return this.o0;
    }

    @Override // androidx.media3.exoplayer.m
    public u52 x() {
        return null;
    }

    public final ExoPlaybackException y(Throwable th, androidx.media3.common.j jVar, int i) {
        return z(th, jVar, false, i);
    }

    public final ExoPlaybackException z(Throwable th, androidx.media3.common.j jVar, boolean z, int i) {
        int i2;
        if (jVar != null && !this.p0) {
            this.p0 = true;
            try {
                i2 = u63.f(a(jVar));
            } catch (ExoPlaybackException unused) {
            } finally {
                this.p0 = false;
            }
            return ExoPlaybackException.createForRenderer(th, getName(), C(), jVar, i2, z, i);
        }
        i2 = 4;
        return ExoPlaybackException.createForRenderer(th, getName(), C(), jVar, i2, z, i);
    }
}
