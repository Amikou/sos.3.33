package androidx.media3.exoplayer.source;

import androidx.media3.common.u;
import androidx.media3.exoplayer.source.j;
import java.io.IOException;
import java.util.ArrayList;
import zendesk.support.request.CellBase;

/* loaded from: classes.dex */
public final class ClippingMediaSource extends d<Void> {
    public final j k;
    public final long l;
    public final long m;
    public final boolean n;
    public final boolean o;
    public final boolean p;
    public final ArrayList<c> q;
    public final u.c r;
    public a s;
    public IllegalClippingException t;
    public long u;
    public long v;

    /* loaded from: classes.dex */
    public static final class IllegalClippingException extends IOException {
        public static final int REASON_INVALID_PERIOD_COUNT = 0;
        public static final int REASON_NOT_SEEKABLE_TO_START = 1;
        public static final int REASON_START_EXCEEDS_END = 2;
        public final int reason;

        public IllegalClippingException(int i) {
            super("Illegal clipping: " + a(i));
            this.reason = i;
        }

        public static String a(int i) {
            return i != 0 ? i != 1 ? i != 2 ? "unknown" : "start exceeds end" : "not seekable to start" : "invalid period count";
        }
    }

    /* loaded from: classes.dex */
    public static final class a extends l91 {
        public final long g0;
        public final long h0;
        public final long i0;
        public final boolean j0;

        public a(androidx.media3.common.u uVar, long j, long j2) throws IllegalClippingException {
            super(uVar);
            boolean z = false;
            if (uVar.i() == 1) {
                u.c n = uVar.n(0, new u.c());
                long max = Math.max(0L, j);
                if (!n.p0 && max != 0 && !n.l0) {
                    throw new IllegalClippingException(1);
                }
                long max2 = j2 == Long.MIN_VALUE ? n.r0 : Math.max(0L, j2);
                long j3 = n.r0;
                if (j3 != CellBase.ID_SYSTEM_MESSAGE_REQUEST_CLOSED) {
                    max2 = max2 > j3 ? j3 : max2;
                    if (max > max2) {
                        throw new IllegalClippingException(2);
                    }
                }
                this.g0 = max;
                this.h0 = max2;
                int i = (max2 > CellBase.ID_SYSTEM_MESSAGE_REQUEST_CLOSED ? 1 : (max2 == CellBase.ID_SYSTEM_MESSAGE_REQUEST_CLOSED ? 0 : -1));
                this.i0 = i == 0 ? -9223372036854775807L : max2 - max;
                if (n.m0 && (i == 0 || (j3 != CellBase.ID_SYSTEM_MESSAGE_REQUEST_CLOSED && max2 == j3))) {
                    z = true;
                }
                this.j0 = z;
                return;
            }
            throw new IllegalClippingException(0);
        }

        @Override // defpackage.l91, androidx.media3.common.u
        public u.b g(int i, u.b bVar, boolean z) {
            this.f0.g(0, bVar, z);
            long p = bVar.p() - this.g0;
            long j = this.i0;
            return bVar.u(bVar.a, bVar.f0, 0, j == CellBase.ID_SYSTEM_MESSAGE_REQUEST_CLOSED ? -9223372036854775807L : j - p, p);
        }

        @Override // defpackage.l91, androidx.media3.common.u
        public u.c o(int i, u.c cVar, long j) {
            this.f0.o(0, cVar, 0L);
            long j2 = cVar.u0;
            long j3 = this.g0;
            cVar.u0 = j2 + j3;
            cVar.r0 = this.i0;
            cVar.m0 = this.j0;
            long j4 = cVar.q0;
            if (j4 != CellBase.ID_SYSTEM_MESSAGE_REQUEST_CLOSED) {
                long max = Math.max(j4, j3);
                cVar.q0 = max;
                long j5 = this.h0;
                if (j5 != CellBase.ID_SYSTEM_MESSAGE_REQUEST_CLOSED) {
                    max = Math.min(max, j5);
                }
                cVar.q0 = max;
                cVar.q0 = max - this.g0;
            }
            long U0 = androidx.media3.common.util.b.U0(this.g0);
            long j6 = cVar.i0;
            if (j6 != CellBase.ID_SYSTEM_MESSAGE_REQUEST_CLOSED) {
                cVar.i0 = j6 + U0;
            }
            long j7 = cVar.j0;
            if (j7 != CellBase.ID_SYSTEM_MESSAGE_REQUEST_CLOSED) {
                cVar.j0 = j7 + U0;
            }
            return cVar;
        }
    }

    public ClippingMediaSource(j jVar, long j, long j2, boolean z, boolean z2, boolean z3) {
        ii.a(j >= 0);
        this.k = (j) ii.e(jVar);
        this.l = j;
        this.m = j2;
        this.n = z;
        this.o = z2;
        this.p = z3;
        this.q = new ArrayList<>();
        this.r = new u.c();
    }

    @Override // androidx.media3.exoplayer.source.d, androidx.media3.exoplayer.source.a
    public void A() {
        super.A();
        this.t = null;
        this.s = null;
    }

    @Override // androidx.media3.exoplayer.source.d
    /* renamed from: I */
    public void G(Void r1, j jVar, androidx.media3.common.u uVar) {
        if (this.t != null) {
            return;
        }
        J(uVar);
    }

    public final void J(androidx.media3.common.u uVar) {
        long j;
        long j2;
        uVar.n(0, this.r);
        long h = this.r.h();
        if (this.s != null && !this.q.isEmpty() && !this.o) {
            long j3 = this.u - h;
            j2 = this.m != Long.MIN_VALUE ? this.v - h : Long.MIN_VALUE;
            j = j3;
        } else {
            long j4 = this.l;
            long j5 = this.m;
            if (this.p) {
                long f = this.r.f();
                j4 += f;
                j5 += f;
            }
            this.u = h + j4;
            this.v = this.m != Long.MIN_VALUE ? h + j5 : Long.MIN_VALUE;
            int size = this.q.size();
            for (int i = 0; i < size; i++) {
                this.q.get(i).u(this.u, this.v);
            }
            j = j4;
            j2 = j5;
        }
        try {
            a aVar = new a(uVar, j, j2);
            this.s = aVar;
            z(aVar);
        } catch (IllegalClippingException e) {
            this.t = e;
            for (int i2 = 0; i2 < this.q.size(); i2++) {
                this.q.get(i2).p(this.t);
            }
        }
    }

    @Override // androidx.media3.exoplayer.source.j
    public i e(j.b bVar, gb gbVar, long j) {
        c cVar = new c(this.k.e(bVar, gbVar, j), this.n, this.u, this.v);
        this.q.add(cVar);
        return cVar;
    }

    @Override // androidx.media3.exoplayer.source.j
    public androidx.media3.common.m i() {
        return this.k.i();
    }

    @Override // androidx.media3.exoplayer.source.d, androidx.media3.exoplayer.source.j
    public void j() throws IOException {
        IllegalClippingException illegalClippingException = this.t;
        if (illegalClippingException == null) {
            super.j();
            return;
        }
        throw illegalClippingException;
    }

    @Override // androidx.media3.exoplayer.source.j
    public void o(i iVar) {
        ii.g(this.q.remove(iVar));
        this.k.o(((c) iVar).a);
        if (!this.q.isEmpty() || this.o) {
            return;
        }
        J(((a) ii.e(this.s)).f0);
    }

    @Override // androidx.media3.exoplayer.source.d, androidx.media3.exoplayer.source.a
    public void y(fa4 fa4Var) {
        super.y(fa4Var);
        H(null, this.k);
    }
}
