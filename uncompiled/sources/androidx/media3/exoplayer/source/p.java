package androidx.media3.exoplayer.source;

import androidx.media3.decoder.DecoderInputBuffer;
import androidx.media3.exoplayer.source.q;
import defpackage.f84;
import defpackage.gb;
import java.io.EOFException;
import java.io.IOException;
import java.nio.ByteBuffer;
import java.util.Arrays;

/* compiled from: SampleDataQueue.java */
/* loaded from: classes.dex */
public class p {
    public final gb a;
    public final int b;
    public final op2 c;
    public a d;
    public a e;
    public a f;
    public long g;

    /* compiled from: SampleDataQueue.java */
    /* loaded from: classes.dex */
    public static final class a implements gb.a {
        public long a;
        public long b;
        public fb c;
        public a d;

        public a(long j, int i) {
            d(j, i);
        }

        @Override // defpackage.gb.a
        public fb a() {
            return (fb) ii.e(this.c);
        }

        public a b() {
            this.c = null;
            a aVar = this.d;
            this.d = null;
            return aVar;
        }

        public void c(fb fbVar, a aVar) {
            this.c = fbVar;
            this.d = aVar;
        }

        public void d(long j, int i) {
            ii.g(this.c == null);
            this.a = j;
            this.b = j + i;
        }

        public int e(long j) {
            return ((int) (j - this.a)) + this.c.b;
        }

        @Override // defpackage.gb.a
        public gb.a next() {
            a aVar = this.d;
            if (aVar == null || aVar.c == null) {
                return null;
            }
            return aVar;
        }
    }

    public p(gb gbVar) {
        this.a = gbVar;
        int e = gbVar.e();
        this.b = e;
        this.c = new op2(32);
        a aVar = new a(0L, e);
        this.d = aVar;
        this.e = aVar;
        this.f = aVar;
    }

    public static a d(a aVar, long j) {
        while (j >= aVar.b) {
            aVar = aVar.d;
        }
        return aVar;
    }

    public static a i(a aVar, long j, ByteBuffer byteBuffer, int i) {
        a d = d(aVar, j);
        while (i > 0) {
            int min = Math.min(i, (int) (d.b - j));
            byteBuffer.put(d.c.a, d.e(j), min);
            i -= min;
            j += min;
            if (j == d.b) {
                d = d.d;
            }
        }
        return d;
    }

    public static a j(a aVar, long j, byte[] bArr, int i) {
        a d = d(aVar, j);
        int i2 = i;
        while (i2 > 0) {
            int min = Math.min(i2, (int) (d.b - j));
            System.arraycopy(d.c.a, d.e(j), bArr, i - i2, min);
            i2 -= min;
            j += min;
            if (j == d.b) {
                d = d.d;
            }
        }
        return d;
    }

    public static a k(a aVar, DecoderInputBuffer decoderInputBuffer, q.b bVar, op2 op2Var) {
        long j = bVar.b;
        int i = 1;
        op2Var.L(1);
        a j2 = j(aVar, j, op2Var.d(), 1);
        long j3 = j + 1;
        byte b = op2Var.d()[0];
        boolean z = (b & 128) != 0;
        int i2 = b & Byte.MAX_VALUE;
        ra0 ra0Var = decoderInputBuffer.f0;
        byte[] bArr = ra0Var.a;
        if (bArr == null) {
            ra0Var.a = new byte[16];
        } else {
            Arrays.fill(bArr, (byte) 0);
        }
        a j4 = j(j2, j3, ra0Var.a, i2);
        long j5 = j3 + i2;
        if (z) {
            op2Var.L(2);
            j4 = j(j4, j5, op2Var.d(), 2);
            j5 += 2;
            i = op2Var.J();
        }
        int i3 = i;
        int[] iArr = ra0Var.d;
        if (iArr == null || iArr.length < i3) {
            iArr = new int[i3];
        }
        int[] iArr2 = iArr;
        int[] iArr3 = ra0Var.e;
        if (iArr3 == null || iArr3.length < i3) {
            iArr3 = new int[i3];
        }
        int[] iArr4 = iArr3;
        if (z) {
            int i4 = i3 * 6;
            op2Var.L(i4);
            j4 = j(j4, j5, op2Var.d(), i4);
            j5 += i4;
            op2Var.P(0);
            for (int i5 = 0; i5 < i3; i5++) {
                iArr2[i5] = op2Var.J();
                iArr4[i5] = op2Var.H();
            }
        } else {
            iArr2[0] = 0;
            iArr4[0] = bVar.a - ((int) (j5 - bVar.b));
        }
        f84.a aVar2 = (f84.a) androidx.media3.common.util.b.j(bVar.c);
        ra0Var.c(i3, iArr2, iArr4, aVar2.b, ra0Var.a, aVar2.a, aVar2.c, aVar2.d);
        long j6 = bVar.b;
        int i6 = (int) (j5 - j6);
        bVar.b = j6 + i6;
        bVar.a -= i6;
        return j4;
    }

    public static a l(a aVar, DecoderInputBuffer decoderInputBuffer, q.b bVar, op2 op2Var) {
        if (decoderInputBuffer.y()) {
            aVar = k(aVar, decoderInputBuffer, bVar, op2Var);
        }
        if (decoderInputBuffer.l()) {
            op2Var.L(4);
            a j = j(aVar, bVar.b, op2Var.d(), 4);
            int H = op2Var.H();
            bVar.b += 4;
            bVar.a -= 4;
            decoderInputBuffer.v(H);
            a i = i(j, bVar.b, decoderInputBuffer.g0, H);
            bVar.b += H;
            int i2 = bVar.a - H;
            bVar.a = i2;
            decoderInputBuffer.B(i2);
            return i(i, bVar.b, decoderInputBuffer.j0, bVar.a);
        }
        decoderInputBuffer.v(bVar.a);
        return i(aVar, bVar.b, decoderInputBuffer.g0, bVar.a);
    }

    public final void a(a aVar) {
        if (aVar.c == null) {
            return;
        }
        this.a.a(aVar);
        aVar.b();
    }

    public void b(long j) {
        a aVar;
        if (j == -1) {
            return;
        }
        while (true) {
            aVar = this.d;
            if (j < aVar.b) {
                break;
            }
            this.a.b(aVar.c);
            this.d = this.d.b();
        }
        if (this.e.a < aVar.a) {
            this.e = aVar;
        }
    }

    public void c(long j) {
        ii.a(j <= this.g);
        this.g = j;
        if (j != 0) {
            a aVar = this.d;
            if (j != aVar.a) {
                while (this.g > aVar.b) {
                    aVar = aVar.d;
                }
                a aVar2 = (a) ii.e(aVar.d);
                a(aVar2);
                a aVar3 = new a(aVar.b, this.b);
                aVar.d = aVar3;
                if (this.g == aVar.b) {
                    aVar = aVar3;
                }
                this.f = aVar;
                if (this.e == aVar2) {
                    this.e = aVar3;
                    return;
                }
                return;
            }
        }
        a(this.d);
        a aVar4 = new a(this.g, this.b);
        this.d = aVar4;
        this.e = aVar4;
        this.f = aVar4;
    }

    public long e() {
        return this.g;
    }

    public void f(DecoderInputBuffer decoderInputBuffer, q.b bVar) {
        l(this.e, decoderInputBuffer, bVar, this.c);
    }

    public final void g(int i) {
        long j = this.g + i;
        this.g = j;
        a aVar = this.f;
        if (j == aVar.b) {
            this.f = aVar.d;
        }
    }

    public final int h(int i) {
        a aVar = this.f;
        if (aVar.c == null) {
            aVar.c(this.a.c(), new a(this.f.b, this.b));
        }
        return Math.min(i, (int) (this.f.b - this.g));
    }

    public void m(DecoderInputBuffer decoderInputBuffer, q.b bVar) {
        this.e = l(this.e, decoderInputBuffer, bVar, this.c);
    }

    public void n() {
        a(this.d);
        this.d.d(0L, this.b);
        a aVar = this.d;
        this.e = aVar;
        this.f = aVar;
        this.g = 0L;
        this.a.d();
    }

    public void o() {
        this.e = this.d;
    }

    public int p(androidx.media3.common.g gVar, int i, boolean z) throws IOException {
        int h = h(i);
        a aVar = this.f;
        int read = gVar.read(aVar.c.a, aVar.e(this.g), h);
        if (read != -1) {
            g(read);
            return read;
        } else if (z) {
            return -1;
        } else {
            throw new EOFException();
        }
    }

    public void q(op2 op2Var, int i) {
        while (i > 0) {
            int h = h(i);
            a aVar = this.f;
            op2Var.j(aVar.c.a, aVar.e(this.g), h);
            i -= h;
            g(h);
        }
    }
}
