package androidx.media3.exoplayer.source;

import androidx.media3.decoder.DecoderInputBuffer;
import androidx.media3.exoplayer.source.ClippingMediaSource;
import androidx.media3.exoplayer.source.i;
import java.io.IOException;
import zendesk.support.request.CellBase;

/* compiled from: ClippingMediaPeriod.java */
/* loaded from: classes.dex */
public final class c implements i, i.a {
    public final i a;
    public i.a f0;
    public a[] g0 = new a[0];
    public long h0;
    public long i0;
    public long j0;
    public ClippingMediaSource.IllegalClippingException k0;

    /* compiled from: ClippingMediaPeriod.java */
    /* loaded from: classes.dex */
    public final class a implements r {
        public final r a;
        public boolean f0;

        public a(r rVar) {
            this.a = rVar;
        }

        public void a() {
            this.f0 = false;
        }

        @Override // androidx.media3.exoplayer.source.r
        public void b() throws IOException {
            this.a.b();
        }

        @Override // androidx.media3.exoplayer.source.r
        public boolean f() {
            return !c.this.j() && this.a.f();
        }

        @Override // androidx.media3.exoplayer.source.r
        public int m(long j) {
            if (c.this.j()) {
                return -3;
            }
            return this.a.m(j);
        }

        @Override // androidx.media3.exoplayer.source.r
        public int p(y81 y81Var, DecoderInputBuffer decoderInputBuffer, int i) {
            if (c.this.j()) {
                return -3;
            }
            if (this.f0) {
                decoderInputBuffer.t(4);
                return -4;
            }
            int p = this.a.p(y81Var, decoderInputBuffer, i);
            if (p == -5) {
                androidx.media3.common.j jVar = (androidx.media3.common.j) ii.e(y81Var.b);
                int i2 = jVar.F0;
                if (i2 != 0 || jVar.G0 != 0) {
                    c cVar = c.this;
                    if (cVar.i0 != 0) {
                        i2 = 0;
                    }
                    y81Var.b = jVar.b().N(i2).O(cVar.j0 == Long.MIN_VALUE ? jVar.G0 : 0).E();
                }
                return -5;
            }
            c cVar2 = c.this;
            long j = cVar2.j0;
            if (j == Long.MIN_VALUE || ((p != -4 || decoderInputBuffer.i0 < j) && !(p == -3 && cVar2.g() == Long.MIN_VALUE && !decoderInputBuffer.h0))) {
                return p;
            }
            decoderInputBuffer.h();
            decoderInputBuffer.t(4);
            this.f0 = true;
            return -4;
        }
    }

    public c(i iVar, boolean z, long j, long j2) {
        this.a = iVar;
        this.h0 = z ? j : CellBase.ID_SYSTEM_MESSAGE_REQUEST_CLOSED;
        this.i0 = j;
        this.j0 = j2;
    }

    public static boolean s(long j, androidx.media3.exoplayer.trackselection.c[] cVarArr) {
        if (j != 0) {
            for (androidx.media3.exoplayer.trackselection.c cVar : cVarArr) {
                if (cVar != null) {
                    androidx.media3.common.j n = cVar.n();
                    if (!y82.a(n.p0, n.m0)) {
                        return true;
                    }
                }
            }
        }
        return false;
    }

    @Override // androidx.media3.exoplayer.source.i, androidx.media3.exoplayer.source.s
    public long a() {
        long a2 = this.a.a();
        if (a2 != Long.MIN_VALUE) {
            long j = this.j0;
            if (j == Long.MIN_VALUE || a2 < j) {
                return a2;
            }
        }
        return Long.MIN_VALUE;
    }

    public final xi3 b(long j, xi3 xi3Var) {
        long r = androidx.media3.common.util.b.r(xi3Var.a, 0L, j - this.i0);
        long j2 = xi3Var.b;
        long j3 = this.j0;
        long r2 = androidx.media3.common.util.b.r(j2, 0L, j3 == Long.MIN_VALUE ? Long.MAX_VALUE : j3 - j);
        return (r == xi3Var.a && r2 == xi3Var.b) ? xi3Var : new xi3(r, r2);
    }

    @Override // androidx.media3.exoplayer.source.i
    public long c(long j, xi3 xi3Var) {
        long j2 = this.i0;
        if (j == j2) {
            return j2;
        }
        return this.a.c(j, b(j, xi3Var));
    }

    @Override // androidx.media3.exoplayer.source.i, androidx.media3.exoplayer.source.s
    public boolean d(long j) {
        return this.a.d(j);
    }

    @Override // androidx.media3.exoplayer.source.i, androidx.media3.exoplayer.source.s
    public boolean e() {
        return this.a.e();
    }

    @Override // androidx.media3.exoplayer.source.i.a
    public void f(i iVar) {
        if (this.k0 != null) {
            return;
        }
        ((i.a) ii.e(this.f0)).f(this);
    }

    @Override // androidx.media3.exoplayer.source.i, androidx.media3.exoplayer.source.s
    public long g() {
        long g = this.a.g();
        if (g != Long.MIN_VALUE) {
            long j = this.j0;
            if (j == Long.MIN_VALUE || g < j) {
                return g;
            }
        }
        return Long.MIN_VALUE;
    }

    @Override // androidx.media3.exoplayer.source.i, androidx.media3.exoplayer.source.s
    public void h(long j) {
        this.a.h(j);
    }

    public boolean j() {
        return this.h0 != CellBase.ID_SYSTEM_MESSAGE_REQUEST_CLOSED;
    }

    @Override // androidx.media3.exoplayer.source.i
    public void k() throws IOException {
        ClippingMediaSource.IllegalClippingException illegalClippingException = this.k0;
        if (illegalClippingException == null) {
            this.a.k();
            return;
        }
        throw illegalClippingException;
    }

    /* JADX WARN: Code restructure failed: missing block: B:15:0x0032, code lost:
        if (r0 > r6) goto L18;
     */
    @Override // androidx.media3.exoplayer.source.i
    /*
        Code decompiled incorrectly, please refer to instructions dump.
        To view partially-correct code enable 'Show inconsistent code' option in preferences
    */
    public long l(long r6) {
        /*
            r5 = this;
            r0 = -9223372036854775807(0x8000000000000001, double:-4.9E-324)
            r5.h0 = r0
            androidx.media3.exoplayer.source.c$a[] r0 = r5.g0
            int r1 = r0.length
            r2 = 0
            r3 = r2
        Lc:
            if (r3 >= r1) goto L18
            r4 = r0[r3]
            if (r4 == 0) goto L15
            r4.a()
        L15:
            int r3 = r3 + 1
            goto Lc
        L18:
            androidx.media3.exoplayer.source.i r0 = r5.a
            long r0 = r0.l(r6)
            int r6 = (r0 > r6 ? 1 : (r0 == r6 ? 0 : -1))
            if (r6 == 0) goto L34
            long r6 = r5.i0
            int r6 = (r0 > r6 ? 1 : (r0 == r6 ? 0 : -1))
            if (r6 < 0) goto L35
            long r6 = r5.j0
            r3 = -9223372036854775808
            int r3 = (r6 > r3 ? 1 : (r6 == r3 ? 0 : -1))
            if (r3 == 0) goto L34
            int r6 = (r0 > r6 ? 1 : (r0 == r6 ? 0 : -1))
            if (r6 > 0) goto L35
        L34:
            r2 = 1
        L35:
            defpackage.ii.g(r2)
            return r0
        */
        throw new UnsupportedOperationException("Method not decompiled: androidx.media3.exoplayer.source.c.l(long):long");
    }

    @Override // androidx.media3.exoplayer.source.s.a
    /* renamed from: m */
    public void i(i iVar) {
        ((i.a) ii.e(this.f0)).i(this);
    }

    /* JADX WARN: Code restructure failed: missing block: B:24:0x0062, code lost:
        if (r2 > r4) goto L26;
     */
    /* JADX WARN: Removed duplicated region for block: B:19:0x0052  */
    /* JADX WARN: Removed duplicated region for block: B:31:0x006e  */
    @Override // androidx.media3.exoplayer.source.i
    /*
        Code decompiled incorrectly, please refer to instructions dump.
        To view partially-correct code enable 'Show inconsistent code' option in preferences
    */
    public long n(androidx.media3.exoplayer.trackselection.c[] r13, boolean[] r14, androidx.media3.exoplayer.source.r[] r15, boolean[] r16, long r17) {
        /*
            r12 = this;
            r0 = r12
            r1 = r15
            int r2 = r1.length
            androidx.media3.exoplayer.source.c$a[] r2 = new androidx.media3.exoplayer.source.c.a[r2]
            r0.g0 = r2
            int r2 = r1.length
            androidx.media3.exoplayer.source.r[] r9 = new androidx.media3.exoplayer.source.r[r2]
            r10 = 0
            r2 = r10
        Lc:
            int r3 = r1.length
            r11 = 0
            if (r2 >= r3) goto L25
            androidx.media3.exoplayer.source.c$a[] r3 = r0.g0
            r4 = r1[r2]
            androidx.media3.exoplayer.source.c$a r4 = (androidx.media3.exoplayer.source.c.a) r4
            r3[r2] = r4
            r4 = r3[r2]
            if (r4 == 0) goto L20
            r3 = r3[r2]
            androidx.media3.exoplayer.source.r r11 = r3.a
        L20:
            r9[r2] = r11
            int r2 = r2 + 1
            goto Lc
        L25:
            androidx.media3.exoplayer.source.i r2 = r0.a
            r3 = r13
            r4 = r14
            r5 = r9
            r6 = r16
            r7 = r17
            long r2 = r2.n(r3, r4, r5, r6, r7)
            boolean r4 = r12.j()
            if (r4 == 0) goto L47
            long r4 = r0.i0
            int r6 = (r17 > r4 ? 1 : (r17 == r4 ? 0 : -1))
            if (r6 != 0) goto L47
            r6 = r13
            boolean r4 = s(r4, r13)
            if (r4 == 0) goto L47
            r4 = r2
            goto L4c
        L47:
            r4 = -9223372036854775807(0x8000000000000001, double:-4.9E-324)
        L4c:
            r0.h0 = r4
            int r4 = (r2 > r17 ? 1 : (r2 == r17 ? 0 : -1))
            if (r4 == 0) goto L67
            long r4 = r0.i0
            int r4 = (r2 > r4 ? 1 : (r2 == r4 ? 0 : -1))
            if (r4 < 0) goto L65
            long r4 = r0.j0
            r6 = -9223372036854775808
            int r6 = (r4 > r6 ? 1 : (r4 == r6 ? 0 : -1))
            if (r6 == 0) goto L67
            int r4 = (r2 > r4 ? 1 : (r2 == r4 ? 0 : -1))
            if (r4 > 0) goto L65
            goto L67
        L65:
            r4 = r10
            goto L68
        L67:
            r4 = 1
        L68:
            defpackage.ii.g(r4)
        L6b:
            int r4 = r1.length
            if (r10 >= r4) goto L97
            r4 = r9[r10]
            if (r4 != 0) goto L77
            androidx.media3.exoplayer.source.c$a[] r4 = r0.g0
            r4[r10] = r11
            goto L8e
        L77:
            androidx.media3.exoplayer.source.c$a[] r4 = r0.g0
            r5 = r4[r10]
            if (r5 == 0) goto L85
            r5 = r4[r10]
            androidx.media3.exoplayer.source.r r5 = r5.a
            r6 = r9[r10]
            if (r5 == r6) goto L8e
        L85:
            androidx.media3.exoplayer.source.c$a r5 = new androidx.media3.exoplayer.source.c$a
            r6 = r9[r10]
            r5.<init>(r6)
            r4[r10] = r5
        L8e:
            androidx.media3.exoplayer.source.c$a[] r4 = r0.g0
            r4 = r4[r10]
            r1[r10] = r4
            int r10 = r10 + 1
            goto L6b
        L97:
            return r2
        */
        throw new UnsupportedOperationException("Method not decompiled: androidx.media3.exoplayer.source.c.n(androidx.media3.exoplayer.trackselection.c[], boolean[], androidx.media3.exoplayer.source.r[], boolean[], long):long");
    }

    @Override // androidx.media3.exoplayer.source.i
    public long o() {
        if (j()) {
            long j = this.h0;
            this.h0 = CellBase.ID_SYSTEM_MESSAGE_REQUEST_CLOSED;
            long o = o();
            return o != CellBase.ID_SYSTEM_MESSAGE_REQUEST_CLOSED ? o : j;
        }
        long o2 = this.a.o();
        if (o2 == CellBase.ID_SYSTEM_MESSAGE_REQUEST_CLOSED) {
            return CellBase.ID_SYSTEM_MESSAGE_REQUEST_CLOSED;
        }
        boolean z = true;
        ii.g(o2 >= this.i0);
        long j2 = this.j0;
        if (j2 != Long.MIN_VALUE && o2 > j2) {
            z = false;
        }
        ii.g(z);
        return o2;
    }

    public void p(ClippingMediaSource.IllegalClippingException illegalClippingException) {
        this.k0 = illegalClippingException;
    }

    @Override // androidx.media3.exoplayer.source.i
    public void q(i.a aVar, long j) {
        this.f0 = aVar;
        this.a.q(this, j);
    }

    @Override // androidx.media3.exoplayer.source.i
    public c84 r() {
        return this.a.r();
    }

    @Override // androidx.media3.exoplayer.source.i
    public void t(long j, boolean z) {
        this.a.t(j, z);
    }

    public void u(long j, long j2) {
        this.i0 = j;
        this.j0 = j2;
    }
}
