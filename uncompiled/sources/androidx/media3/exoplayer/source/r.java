package androidx.media3.exoplayer.source;

import androidx.media3.decoder.DecoderInputBuffer;
import java.io.IOException;

/* compiled from: SampleStream.java */
/* loaded from: classes.dex */
public interface r {
    void b() throws IOException;

    boolean f();

    int m(long j);

    int p(y81 y81Var, DecoderInputBuffer decoderInputBuffer, int i);
}
