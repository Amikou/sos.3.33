package androidx.media3.exoplayer.source;

import androidx.media3.common.DrmInitData;
import androidx.media3.decoder.DecoderInputBuffer;
import androidx.media3.exoplayer.drm.DrmSession;
import androidx.media3.exoplayer.drm.b;
import androidx.media3.exoplayer.drm.c;
import defpackage.f84;
import java.io.IOException;

/* compiled from: SampleQueue.java */
/* loaded from: classes.dex */
public class q implements f84 {
    public androidx.media3.common.j A;
    public androidx.media3.common.j B;
    public int C;
    public boolean D;
    public boolean E;
    public long F;
    public boolean G;
    public final p a;
    public final androidx.media3.exoplayer.drm.c d;
    public final b.a e;
    public d f;
    public androidx.media3.common.j g;
    public DrmSession h;
    public int p;
    public int q;
    public int r;
    public int s;
    public boolean w;
    public boolean z;
    public final b b = new b();
    public int i = 1000;
    public int[] j = new int[1000];
    public long[] k = new long[1000];
    public long[] n = new long[1000];
    public int[] m = new int[1000];
    public int[] l = new int[1000];
    public f84.a[] o = new f84.a[1000];
    public final ir3<c> c = new ir3<>(cc3.a);
    public long t = Long.MIN_VALUE;
    public long u = Long.MIN_VALUE;
    public long v = Long.MIN_VALUE;
    public boolean y = true;
    public boolean x = true;

    /* compiled from: SampleQueue.java */
    /* loaded from: classes.dex */
    public static final class b {
        public int a;
        public long b;
        public f84.a c;
    }

    /* compiled from: SampleQueue.java */
    /* loaded from: classes.dex */
    public static final class c {
        public final androidx.media3.common.j a;
        public final c.b b;

        public c(androidx.media3.common.j jVar, c.b bVar) {
            this.a = jVar;
            this.b = bVar;
        }
    }

    /* compiled from: SampleQueue.java */
    /* loaded from: classes.dex */
    public interface d {
        void b(androidx.media3.common.j jVar);
    }

    public q(gb gbVar, androidx.media3.exoplayer.drm.c cVar, b.a aVar) {
        this.d = cVar;
        this.e = aVar;
        this.a = new p(gbVar);
    }

    public static /* synthetic */ void L(c cVar) {
        cVar.b.a();
    }

    public static q k(gb gbVar, androidx.media3.exoplayer.drm.c cVar, b.a aVar) {
        return new q(gbVar, (androidx.media3.exoplayer.drm.c) ii.e(cVar), (b.a) ii.e(aVar));
    }

    public static q l(gb gbVar) {
        return new q(gbVar, null, null);
    }

    public final synchronized long A() {
        return Math.max(this.u, B(this.s));
    }

    public final long B(int i) {
        long j = Long.MIN_VALUE;
        if (i == 0) {
            return Long.MIN_VALUE;
        }
        int D = D(i - 1);
        for (int i2 = 0; i2 < i; i2++) {
            j = Math.max(j, this.n[D]);
            if ((this.m[D] & 1) != 0) {
                break;
            }
            D--;
            if (D == -1) {
                D = this.i - 1;
            }
        }
        return j;
    }

    public final int C() {
        return this.q + this.s;
    }

    public final int D(int i) {
        int i2 = this.r + i;
        int i3 = this.i;
        return i2 < i3 ? i2 : i2 - i3;
    }

    public final synchronized int E(long j, boolean z) {
        int D = D(this.s);
        if (H() && j >= this.n[D]) {
            if (j > this.v && z) {
                return this.p - this.s;
            }
            int v = v(D, this.p - this.s, j, true);
            if (v == -1) {
                return 0;
            }
            return v;
        }
        return 0;
    }

    public final synchronized androidx.media3.common.j F() {
        return this.y ? null : this.B;
    }

    public final int G() {
        return this.q + this.p;
    }

    public final boolean H() {
        return this.s != this.p;
    }

    public final void I() {
        this.z = true;
    }

    public final synchronized boolean J() {
        return this.w;
    }

    public synchronized boolean K(boolean z) {
        androidx.media3.common.j jVar;
        boolean z2 = true;
        if (!H()) {
            if (!z && !this.w && ((jVar = this.B) == null || jVar == this.g)) {
                z2 = false;
            }
            return z2;
        } else if (this.c.e(C()).a != this.g) {
            return true;
        } else {
            return M(D(this.s));
        }
    }

    public final boolean M(int i) {
        DrmSession drmSession = this.h;
        return drmSession == null || drmSession.getState() == 4 || ((this.m[i] & 1073741824) == 0 && this.h.c());
    }

    public void N() throws IOException {
        DrmSession drmSession = this.h;
        if (drmSession != null && drmSession.getState() == 1) {
            throw ((DrmSession.DrmSessionException) ii.e(this.h.getError()));
        }
    }

    public final void O(androidx.media3.common.j jVar, y81 y81Var) {
        androidx.media3.common.j jVar2 = this.g;
        boolean z = jVar2 == null;
        DrmInitData drmInitData = z ? null : jVar2.s0;
        this.g = jVar;
        DrmInitData drmInitData2 = jVar.s0;
        androidx.media3.exoplayer.drm.c cVar = this.d;
        y81Var.b = cVar != null ? jVar.c(cVar.e(jVar)) : jVar;
        y81Var.a = this.h;
        if (this.d == null) {
            return;
        }
        if (z || !androidx.media3.common.util.b.c(drmInitData, drmInitData2)) {
            DrmSession drmSession = this.h;
            DrmSession c2 = this.d.c(this.e, jVar);
            this.h = c2;
            y81Var.a = c2;
            if (drmSession != null) {
                drmSession.e(this.e);
            }
        }
    }

    public final synchronized int P(y81 y81Var, DecoderInputBuffer decoderInputBuffer, boolean z, boolean z2, b bVar) {
        decoderInputBuffer.h0 = false;
        if (!H()) {
            if (!z2 && !this.w) {
                androidx.media3.common.j jVar = this.B;
                if (jVar == null || (!z && jVar == this.g)) {
                    return -3;
                }
                O((androidx.media3.common.j) ii.e(jVar), y81Var);
                return -5;
            }
            decoderInputBuffer.t(4);
            return -4;
        }
        androidx.media3.common.j jVar2 = this.c.e(C()).a;
        if (!z && jVar2 == this.g) {
            int D = D(this.s);
            if (!M(D)) {
                decoderInputBuffer.h0 = true;
                return -3;
            }
            decoderInputBuffer.t(this.m[D]);
            long j = this.n[D];
            decoderInputBuffer.i0 = j;
            if (j < this.t) {
                decoderInputBuffer.g(Integer.MIN_VALUE);
            }
            bVar.a = this.l[D];
            bVar.b = this.k[D];
            bVar.c = this.o[D];
            return -4;
        }
        O(jVar2, y81Var);
        return -5;
    }

    public void Q() {
        r();
        T();
    }

    public int R(y81 y81Var, DecoderInputBuffer decoderInputBuffer, int i, boolean z) {
        int P = P(y81Var, decoderInputBuffer, (i & 2) != 0, z, this.b);
        if (P == -4 && !decoderInputBuffer.p()) {
            boolean z2 = (i & 1) != 0;
            if ((i & 4) == 0) {
                if (z2) {
                    this.a.f(decoderInputBuffer, this.b);
                } else {
                    this.a.m(decoderInputBuffer, this.b);
                }
            }
            if (!z2) {
                this.s++;
            }
        }
        return P;
    }

    public void S() {
        V(true);
        T();
    }

    public final void T() {
        DrmSession drmSession = this.h;
        if (drmSession != null) {
            drmSession.e(this.e);
            this.h = null;
            this.g = null;
        }
    }

    public final void U() {
        V(false);
    }

    public void V(boolean z) {
        this.a.n();
        this.p = 0;
        this.q = 0;
        this.r = 0;
        this.s = 0;
        this.x = true;
        this.t = Long.MIN_VALUE;
        this.u = Long.MIN_VALUE;
        this.v = Long.MIN_VALUE;
        this.w = false;
        this.c.b();
        if (z) {
            this.A = null;
            this.B = null;
            this.y = true;
        }
    }

    public final synchronized void W() {
        this.s = 0;
        this.a.o();
    }

    public final synchronized boolean X(int i) {
        W();
        int i2 = this.q;
        if (i >= i2 && i <= this.p + i2) {
            this.t = Long.MIN_VALUE;
            this.s = i - i2;
            return true;
        }
        return false;
    }

    public final synchronized boolean Y(long j, boolean z) {
        W();
        int D = D(this.s);
        if (H() && j >= this.n[D] && (j <= this.v || z)) {
            int v = v(D, this.p - this.s, j, true);
            if (v == -1) {
                return false;
            }
            this.t = j;
            this.s += v;
            return true;
        }
        return false;
    }

    public final void Z(long j) {
        if (this.F != j) {
            this.F = j;
            I();
        }
    }

    @Override // defpackage.f84
    public /* synthetic */ void a(op2 op2Var, int i) {
        e84.b(this, op2Var, i);
    }

    public final void a0(long j) {
        this.t = j;
    }

    /* JADX WARN: Removed duplicated region for block: B:27:0x0059  */
    @Override // defpackage.f84
    /*
        Code decompiled incorrectly, please refer to instructions dump.
        To view partially-correct code enable 'Show inconsistent code' option in preferences
    */
    public void b(long r12, int r14, int r15, int r16, defpackage.f84.a r17) {
        /*
            r11 = this;
            r8 = r11
            boolean r0 = r8.z
            if (r0 == 0) goto L10
            androidx.media3.common.j r0 = r8.A
            java.lang.Object r0 = defpackage.ii.i(r0)
            androidx.media3.common.j r0 = (androidx.media3.common.j) r0
            r11.f(r0)
        L10:
            r0 = r14 & 1
            r1 = 0
            r2 = 1
            if (r0 == 0) goto L18
            r3 = r2
            goto L19
        L18:
            r3 = r1
        L19:
            boolean r4 = r8.x
            if (r4 == 0) goto L22
            if (r3 != 0) goto L20
            return
        L20:
            r8.x = r1
        L22:
            long r4 = r8.F
            long r4 = r4 + r12
            boolean r6 = r8.D
            if (r6 == 0) goto L54
            long r6 = r8.t
            int r6 = (r4 > r6 ? 1 : (r4 == r6 ? 0 : -1))
            if (r6 >= 0) goto L30
            return
        L30:
            if (r0 != 0) goto L54
            boolean r0 = r8.E
            if (r0 != 0) goto L50
            java.lang.StringBuilder r0 = new java.lang.StringBuilder
            r0.<init>()
            java.lang.String r6 = "Overriding unexpected non-sync sample for format: "
            r0.append(r6)
            androidx.media3.common.j r6 = r8.B
            r0.append(r6)
            java.lang.String r0 = r0.toString()
            java.lang.String r6 = "SampleQueue"
            defpackage.p12.i(r6, r0)
            r8.E = r2
        L50:
            r0 = r14 | 1
            r6 = r0
            goto L55
        L54:
            r6 = r14
        L55:
            boolean r0 = r8.G
            if (r0 == 0) goto L66
            if (r3 == 0) goto L65
            boolean r0 = r11.h(r4)
            if (r0 != 0) goto L62
            goto L65
        L62:
            r8.G = r1
            goto L66
        L65:
            return
        L66:
            androidx.media3.exoplayer.source.p r0 = r8.a
            long r0 = r0.e()
            r7 = r15
            long r2 = (long) r7
            long r0 = r0 - r2
            r2 = r16
            long r2 = (long) r2
            long r9 = r0 - r2
            r0 = r11
            r1 = r4
            r3 = r6
            r4 = r9
            r6 = r15
            r7 = r17
            r0.i(r1, r3, r4, r6, r7)
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: androidx.media3.exoplayer.source.q.b(long, int, int, int, f84$a):void");
    }

    public final synchronized boolean b0(androidx.media3.common.j jVar) {
        this.y = false;
        if (androidx.media3.common.util.b.c(jVar, this.B)) {
            return false;
        }
        if (!this.c.g() && this.c.f().a.equals(jVar)) {
            this.B = this.c.f().a;
        } else {
            this.B = jVar;
        }
        androidx.media3.common.j jVar2 = this.B;
        this.D = y82.a(jVar2.p0, jVar2.m0);
        this.E = false;
        return true;
    }

    @Override // defpackage.f84
    public final void c(op2 op2Var, int i, int i2) {
        this.a.q(op2Var, i);
    }

    public final void c0(d dVar) {
        this.f = dVar;
    }

    @Override // defpackage.f84
    public /* synthetic */ int d(androidx.media3.common.g gVar, int i, boolean z) {
        return e84.a(this, gVar, i, z);
    }

    public final synchronized void d0(int i) {
        boolean z;
        if (i >= 0) {
            try {
                if (this.s + i <= this.p) {
                    z = true;
                    ii.a(z);
                    this.s += i;
                }
            } catch (Throwable th) {
                throw th;
            }
        }
        z = false;
        ii.a(z);
        this.s += i;
    }

    @Override // defpackage.f84
    public final int e(androidx.media3.common.g gVar, int i, boolean z, int i2) throws IOException {
        return this.a.p(gVar, i, z);
    }

    @Override // defpackage.f84
    public final void f(androidx.media3.common.j jVar) {
        androidx.media3.common.j w = w(jVar);
        this.z = false;
        this.A = jVar;
        boolean b0 = b0(w);
        d dVar = this.f;
        if (dVar == null || !b0) {
            return;
        }
        dVar.b(w);
    }

    public final synchronized boolean h(long j) {
        if (this.p == 0) {
            return j > this.u;
        } else if (A() >= j) {
            return false;
        } else {
            t(this.q + j(j));
            return true;
        }
    }

    public final synchronized void i(long j, int i, long j2, int i2, f84.a aVar) {
        c.b bVar;
        int i3 = this.p;
        if (i3 > 0) {
            int D = D(i3 - 1);
            ii.a(this.k[D] + ((long) this.l[D]) <= j2);
        }
        this.w = (536870912 & i) != 0;
        this.v = Math.max(this.v, j);
        int D2 = D(this.p);
        this.n[D2] = j;
        this.k[D2] = j2;
        this.l[D2] = i2;
        this.m[D2] = i;
        this.o[D2] = aVar;
        this.j[D2] = this.C;
        if (this.c.g() || !this.c.f().a.equals(this.B)) {
            androidx.media3.exoplayer.drm.c cVar = this.d;
            if (cVar != null) {
                bVar = cVar.f(this.e, this.B);
            } else {
                bVar = c.b.a;
            }
            this.c.a(G(), new c((androidx.media3.common.j) ii.e(this.B), bVar));
        }
        int i4 = this.p + 1;
        this.p = i4;
        int i5 = this.i;
        if (i4 == i5) {
            int i6 = i5 + 1000;
            int[] iArr = new int[i6];
            long[] jArr = new long[i6];
            long[] jArr2 = new long[i6];
            int[] iArr2 = new int[i6];
            int[] iArr3 = new int[i6];
            f84.a[] aVarArr = new f84.a[i6];
            int i7 = this.r;
            int i8 = i5 - i7;
            System.arraycopy(this.k, i7, jArr, 0, i8);
            System.arraycopy(this.n, this.r, jArr2, 0, i8);
            System.arraycopy(this.m, this.r, iArr2, 0, i8);
            System.arraycopy(this.l, this.r, iArr3, 0, i8);
            System.arraycopy(this.o, this.r, aVarArr, 0, i8);
            System.arraycopy(this.j, this.r, iArr, 0, i8);
            int i9 = this.r;
            System.arraycopy(this.k, 0, jArr, i8, i9);
            System.arraycopy(this.n, 0, jArr2, i8, i9);
            System.arraycopy(this.m, 0, iArr2, i8, i9);
            System.arraycopy(this.l, 0, iArr3, i8, i9);
            System.arraycopy(this.o, 0, aVarArr, i8, i9);
            System.arraycopy(this.j, 0, iArr, i8, i9);
            this.k = jArr;
            this.n = jArr2;
            this.m = iArr2;
            this.l = iArr3;
            this.o = aVarArr;
            this.j = iArr;
            this.r = 0;
            this.i = i6;
        }
    }

    public final int j(long j) {
        int i = this.p;
        int D = D(i - 1);
        while (i > this.s && this.n[D] >= j) {
            i--;
            D--;
            if (D == -1) {
                D = this.i - 1;
            }
        }
        return i;
    }

    public final synchronized long m(long j, boolean z, boolean z2) {
        int i;
        int i2 = this.p;
        if (i2 != 0) {
            long[] jArr = this.n;
            int i3 = this.r;
            if (j >= jArr[i3]) {
                if (z2 && (i = this.s) != i2) {
                    i2 = i + 1;
                }
                int v = v(i3, i2, j, z);
                if (v == -1) {
                    return -1L;
                }
                return p(v);
            }
        }
        return -1L;
    }

    public final synchronized long n() {
        int i = this.p;
        if (i == 0) {
            return -1L;
        }
        return p(i);
    }

    public synchronized long o() {
        int i = this.s;
        if (i == 0) {
            return -1L;
        }
        return p(i);
    }

    public final long p(int i) {
        int i2;
        this.u = Math.max(this.u, B(i));
        this.p -= i;
        int i3 = this.q + i;
        this.q = i3;
        int i4 = this.r + i;
        this.r = i4;
        int i5 = this.i;
        if (i4 >= i5) {
            this.r = i4 - i5;
        }
        int i6 = this.s - i;
        this.s = i6;
        if (i6 < 0) {
            this.s = 0;
        }
        this.c.d(i3);
        if (this.p == 0) {
            int i7 = this.r;
            if (i7 == 0) {
                i7 = this.i;
            }
            return this.k[i7 - 1] + this.l[i2];
        }
        return this.k[this.r];
    }

    public final void q(long j, boolean z, boolean z2) {
        this.a.b(m(j, z, z2));
    }

    public final void r() {
        this.a.b(n());
    }

    public final void s() {
        this.a.b(o());
    }

    public final long t(int i) {
        int D;
        int G = G() - i;
        boolean z = false;
        ii.a(G >= 0 && G <= this.p - this.s);
        int i2 = this.p - G;
        this.p = i2;
        this.v = Math.max(this.u, B(i2));
        if (G == 0 && this.w) {
            z = true;
        }
        this.w = z;
        this.c.c(i);
        int i3 = this.p;
        if (i3 != 0) {
            return this.k[D(i3 - 1)] + this.l[D];
        }
        return 0L;
    }

    public final void u(int i) {
        this.a.c(t(i));
    }

    public final int v(int i, int i2, long j, boolean z) {
        int i3 = -1;
        for (int i4 = 0; i4 < i2; i4++) {
            long[] jArr = this.n;
            if (jArr[i] > j) {
                return i3;
            }
            if (!z || (this.m[i] & 1) != 0) {
                if (jArr[i] == j) {
                    return i4;
                }
                i3 = i4;
            }
            i++;
            if (i == this.i) {
                i = 0;
            }
        }
        return i3;
    }

    public androidx.media3.common.j w(androidx.media3.common.j jVar) {
        return (this.F == 0 || jVar.t0 == Long.MAX_VALUE) ? jVar : jVar.b().i0(jVar.t0 + this.F).E();
    }

    public final int x() {
        return this.q;
    }

    public final synchronized long y() {
        return this.p == 0 ? Long.MIN_VALUE : this.n[this.r];
    }

    public final synchronized long z() {
        return this.v;
    }
}
