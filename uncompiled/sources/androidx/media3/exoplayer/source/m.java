package androidx.media3.exoplayer.source;

import android.net.Uri;
import java.io.IOException;
import java.util.List;
import java.util.Map;

/* compiled from: ProgressiveMediaExtractor.java */
/* loaded from: classes.dex */
public interface m {

    /* compiled from: ProgressiveMediaExtractor.java */
    /* loaded from: classes.dex */
    public interface a {
        m a(ks2 ks2Var);
    }

    void a();

    void b(androidx.media3.common.g gVar, Uri uri, Map<String, List<String>> map, long j, long j2, r11 r11Var) throws IOException;

    void c(long j, long j2);

    long d();

    void e();

    int f(ot2 ot2Var) throws IOException;
}
