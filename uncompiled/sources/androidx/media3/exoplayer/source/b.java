package androidx.media3.exoplayer.source;

import java.io.IOException;

/* compiled from: BundledExtractorsAdapter.java */
/* loaded from: classes.dex */
public final class b implements m {
    public final u11 a;
    public p11 b;
    public q11 c;

    public b(u11 u11Var) {
        this.a = u11Var;
    }

    @Override // androidx.media3.exoplayer.source.m
    public void a() {
        p11 p11Var = this.b;
        if (p11Var != null) {
            p11Var.a();
            this.b = null;
        }
        this.c = null;
    }

    /* JADX WARN: Code restructure failed: missing block: B:18:0x003f, code lost:
        if (r6.getPosition() != r11) goto L28;
     */
    /* JADX WARN: Code restructure failed: missing block: B:31:0x0061, code lost:
        if (r6.getPosition() != r11) goto L28;
     */
    /* JADX WARN: Code restructure failed: missing block: B:33:0x0064, code lost:
        r1 = false;
     */
    @Override // androidx.media3.exoplayer.source.m
    /*
        Code decompiled incorrectly, please refer to instructions dump.
        To view partially-correct code enable 'Show inconsistent code' option in preferences
    */
    public void b(androidx.media3.common.g r8, android.net.Uri r9, java.util.Map<java.lang.String, java.util.List<java.lang.String>> r10, long r11, long r13, defpackage.r11 r15) throws java.io.IOException {
        /*
            r7 = this;
            hj0 r6 = new hj0
            r0 = r6
            r1 = r8
            r2 = r11
            r4 = r13
            r0.<init>(r1, r2, r4)
            r7.c = r6
            p11 r8 = r7.b
            if (r8 == 0) goto L10
            return
        L10:
            u11 r8 = r7.a
            p11[] r8 = r8.b(r9, r10)
            int r10 = r8.length
            r13 = 0
            r14 = 1
            if (r10 != r14) goto L20
            r8 = r8[r13]
            r7.b = r8
            goto L74
        L20:
            int r10 = r8.length
            r0 = r13
        L22:
            if (r0 >= r10) goto L70
            r1 = r8[r0]
            boolean r2 = r1.g(r6)     // Catch: java.lang.Throwable -> L42 java.io.EOFException -> L57
            if (r2 == 0) goto L35
            r7.b = r1     // Catch: java.lang.Throwable -> L42 java.io.EOFException -> L57
            defpackage.ii.g(r14)
            r6.j()
            goto L70
        L35:
            p11 r1 = r7.b
            if (r1 != 0) goto L66
            long r1 = r6.getPosition()
            int r1 = (r1 > r11 ? 1 : (r1 == r11 ? 0 : -1))
            if (r1 != 0) goto L64
            goto L66
        L42:
            r8 = move-exception
            p11 r9 = r7.b
            if (r9 != 0) goto L4f
            long r9 = r6.getPosition()
            int r9 = (r9 > r11 ? 1 : (r9 == r11 ? 0 : -1))
            if (r9 != 0) goto L50
        L4f:
            r13 = r14
        L50:
            defpackage.ii.g(r13)
            r6.j()
            throw r8
        L57:
            p11 r1 = r7.b
            if (r1 != 0) goto L66
            long r1 = r6.getPosition()
            int r1 = (r1 > r11 ? 1 : (r1 == r11 ? 0 : -1))
            if (r1 != 0) goto L64
            goto L66
        L64:
            r1 = r13
            goto L67
        L66:
            r1 = r14
        L67:
            defpackage.ii.g(r1)
            r6.j()
            int r0 = r0 + 1
            goto L22
        L70:
            p11 r10 = r7.b
            if (r10 == 0) goto L7a
        L74:
            p11 r8 = r7.b
            r8.j(r15)
            return
        L7a:
            androidx.media3.exoplayer.source.UnrecognizedInputFormatException r10 = new androidx.media3.exoplayer.source.UnrecognizedInputFormatException
            java.lang.StringBuilder r11 = new java.lang.StringBuilder
            r11.<init>()
            java.lang.String r12 = "None of the available extractors ("
            r11.append(r12)
            java.lang.String r8 = androidx.media3.common.util.b.I(r8)
            r11.append(r8)
            java.lang.String r8 = ") could read the stream."
            r11.append(r8)
            java.lang.String r8 = r11.toString()
            java.lang.Object r9 = defpackage.ii.e(r9)
            android.net.Uri r9 = (android.net.Uri) r9
            r10.<init>(r8, r9)
            throw r10
        */
        throw new UnsupportedOperationException("Method not decompiled: androidx.media3.exoplayer.source.b.b(androidx.media3.common.g, android.net.Uri, java.util.Map, long, long, r11):void");
    }

    @Override // androidx.media3.exoplayer.source.m
    public void c(long j, long j2) {
        ((p11) ii.e(this.b)).c(j, j2);
    }

    @Override // androidx.media3.exoplayer.source.m
    public long d() {
        q11 q11Var = this.c;
        if (q11Var != null) {
            return q11Var.getPosition();
        }
        return -1L;
    }

    @Override // androidx.media3.exoplayer.source.m
    public void e() {
        p11 p11Var = this.b;
        if (p11Var instanceof ea2) {
            ((ea2) p11Var).k();
        }
    }

    @Override // androidx.media3.exoplayer.source.m
    public int f(ot2 ot2Var) throws IOException {
        return ((p11) ii.e(this.b)).f((q11) ii.e(this.c), ot2Var);
    }
}
