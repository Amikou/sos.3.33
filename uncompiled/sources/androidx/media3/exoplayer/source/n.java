package androidx.media3.exoplayer.source;

import android.net.Uri;
import android.os.Handler;
import androidx.media3.common.Metadata;
import androidx.media3.common.ParserException;
import androidx.media3.common.j;
import androidx.media3.common.v;
import androidx.media3.decoder.DecoderInputBuffer;
import androidx.media3.exoplayer.drm.b;
import androidx.media3.exoplayer.source.f;
import androidx.media3.exoplayer.source.i;
import androidx.media3.exoplayer.source.k;
import androidx.media3.exoplayer.source.n;
import androidx.media3.exoplayer.source.q;
import androidx.media3.exoplayer.upstream.Loader;
import androidx.media3.exoplayer.upstream.b;
import androidx.media3.extractor.metadata.icy.IcyHeaders;
import defpackage.je0;
import defpackage.wi3;
import java.io.IOException;
import java.io.InterruptedIOException;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;
import zendesk.support.request.CellBase;

/* compiled from: ProgressiveMediaPeriod.java */
/* loaded from: classes.dex */
public final class n implements i, r11, Loader.b<a>, Loader.f, q.d {
    public static final Map<String, String> Q0 = L();
    public static final androidx.media3.common.j R0 = new j.b().S("icy").e0("application/x-icy").E();
    public boolean A0;
    public e B0;
    public wi3 C0;
    public boolean E0;
    public boolean G0;
    public boolean H0;
    public int I0;
    public boolean J0;
    public long K0;
    public boolean M0;
    public int N0;
    public boolean O0;
    public boolean P0;
    public final Uri a;
    public final androidx.media3.datasource.b f0;
    public final androidx.media3.exoplayer.drm.c g0;
    public final androidx.media3.exoplayer.upstream.b h0;
    public final k.a i0;
    public final b.a j0;
    public final b k0;
    public final gb l0;
    public final String m0;
    public final long n0;
    public final m p0;
    public i.a u0;
    public IcyHeaders v0;
    public boolean y0;
    public boolean z0;
    public final Loader o0 = new Loader("ProgressiveMediaPeriod");
    public final androidx.media3.common.util.a q0 = new androidx.media3.common.util.a();
    public final Runnable r0 = new Runnable() { // from class: uv2
        @Override // java.lang.Runnable
        public final void run() {
            n.this.U();
        }
    };
    public final Runnable s0 = new Runnable() { // from class: tv2
        @Override // java.lang.Runnable
        public final void run() {
            n.this.R();
        }
    };
    public final Handler t0 = androidx.media3.common.util.b.v();
    public d[] x0 = new d[0];
    public q[] w0 = new q[0];
    public long L0 = CellBase.ID_SYSTEM_MESSAGE_REQUEST_CLOSED;
    public long D0 = CellBase.ID_SYSTEM_MESSAGE_REQUEST_CLOSED;
    public int F0 = 1;

    /* compiled from: ProgressiveMediaPeriod.java */
    /* loaded from: classes.dex */
    public final class a implements Loader.e, f.a {
        public final Uri b;
        public final androidx.media3.datasource.f c;
        public final m d;
        public final r11 e;
        public final androidx.media3.common.util.a f;
        public volatile boolean h;
        public long j;
        public f84 l;
        public boolean m;
        public final ot2 g = new ot2();
        public boolean i = true;
        public final long a = u02.a();
        public je0 k = i(0);

        public a(Uri uri, androidx.media3.datasource.b bVar, m mVar, r11 r11Var, androidx.media3.common.util.a aVar) {
            this.b = uri;
            this.c = new androidx.media3.datasource.f(bVar);
            this.d = mVar;
            this.e = r11Var;
            this.f = aVar;
        }

        @Override // androidx.media3.exoplayer.upstream.Loader.e
        public void a() throws IOException {
            int i = 0;
            while (i == 0 && !this.h) {
                try {
                    long j = this.g.a;
                    je0 i2 = i(j);
                    this.k = i2;
                    long b = this.c.b(i2);
                    if (b != -1) {
                        b += j;
                        n.this.Z();
                    }
                    long j2 = b;
                    n.this.v0 = IcyHeaders.a(this.c.i());
                    androidx.media3.common.g gVar = this.c;
                    if (n.this.v0 != null && n.this.v0.j0 != -1) {
                        gVar = new f(this.c, n.this.v0.j0, this);
                        f84 O = n.this.O();
                        this.l = O;
                        O.f(n.R0);
                    }
                    long j3 = j;
                    this.d.b(gVar, this.b, this.c.i(), j, j2, this.e);
                    if (n.this.v0 != null) {
                        this.d.e();
                    }
                    if (this.i) {
                        this.d.c(j3, this.j);
                        this.i = false;
                    }
                    while (true) {
                        long j4 = j3;
                        while (i == 0 && !this.h) {
                            try {
                                this.f.a();
                                i = this.d.f(this.g);
                                j3 = this.d.d();
                                if (j3 > n.this.n0 + j4) {
                                    break;
                                }
                            } catch (InterruptedException unused) {
                                throw new InterruptedIOException();
                            }
                        }
                        this.f.c();
                        n.this.t0.post(n.this.s0);
                    }
                    if (i == 1) {
                        i = 0;
                    } else if (this.d.d() != -1) {
                        this.g.a = this.d.d();
                    }
                    he0.a(this.c);
                } catch (Throwable th) {
                    if (i != 1 && this.d.d() != -1) {
                        this.g.a = this.d.d();
                    }
                    he0.a(this.c);
                    throw th;
                }
            }
        }

        @Override // androidx.media3.exoplayer.source.f.a
        public void b(op2 op2Var) {
            long max;
            if (this.m) {
                max = Math.max(n.this.N(true), this.j);
            } else {
                max = this.j;
            }
            int a = op2Var.a();
            f84 f84Var = (f84) ii.e(this.l);
            f84Var.a(op2Var, a);
            f84Var.b(max, 1, a, 0, null);
            this.m = true;
        }

        @Override // androidx.media3.exoplayer.upstream.Loader.e
        public void c() {
            this.h = true;
        }

        public final je0 i(long j) {
            return new je0.b().i(this.b).h(j).f(n.this.m0).b(6).e(n.Q0).a();
        }

        public final void j(long j, long j2) {
            this.g.a = j;
            this.j = j2;
            this.i = true;
            this.m = false;
        }
    }

    /* compiled from: ProgressiveMediaPeriod.java */
    /* loaded from: classes.dex */
    public interface b {
        void h(long j, boolean z, boolean z2);
    }

    /* compiled from: ProgressiveMediaPeriod.java */
    /* loaded from: classes.dex */
    public final class c implements r {
        public final int a;

        public c(int i) {
            this.a = i;
        }

        @Override // androidx.media3.exoplayer.source.r
        public void b() throws IOException {
            n.this.Y(this.a);
        }

        @Override // androidx.media3.exoplayer.source.r
        public boolean f() {
            return n.this.Q(this.a);
        }

        @Override // androidx.media3.exoplayer.source.r
        public int m(long j) {
            return n.this.i0(this.a, j);
        }

        @Override // androidx.media3.exoplayer.source.r
        public int p(y81 y81Var, DecoderInputBuffer decoderInputBuffer, int i) {
            return n.this.e0(this.a, y81Var, decoderInputBuffer, i);
        }
    }

    /* compiled from: ProgressiveMediaPeriod.java */
    /* loaded from: classes.dex */
    public static final class d {
        public final int a;
        public final boolean b;

        public d(int i, boolean z) {
            this.a = i;
            this.b = z;
        }

        public boolean equals(Object obj) {
            if (this == obj) {
                return true;
            }
            if (obj == null || d.class != obj.getClass()) {
                return false;
            }
            d dVar = (d) obj;
            return this.a == dVar.a && this.b == dVar.b;
        }

        public int hashCode() {
            return (this.a * 31) + (this.b ? 1 : 0);
        }
    }

    /* compiled from: ProgressiveMediaPeriod.java */
    /* loaded from: classes.dex */
    public static final class e {
        public final c84 a;
        public final boolean[] b;
        public final boolean[] c;
        public final boolean[] d;

        public e(c84 c84Var, boolean[] zArr) {
            this.a = c84Var;
            this.b = zArr;
            int i = c84Var.a;
            this.c = new boolean[i];
            this.d = new boolean[i];
        }
    }

    public n(Uri uri, androidx.media3.datasource.b bVar, m mVar, androidx.media3.exoplayer.drm.c cVar, b.a aVar, androidx.media3.exoplayer.upstream.b bVar2, k.a aVar2, b bVar3, gb gbVar, String str, int i) {
        this.a = uri;
        this.f0 = bVar;
        this.g0 = cVar;
        this.j0 = aVar;
        this.h0 = bVar2;
        this.i0 = aVar2;
        this.k0 = bVar3;
        this.l0 = gbVar;
        this.m0 = str;
        this.n0 = i;
        this.p0 = mVar;
    }

    public static Map<String, String> L() {
        HashMap hashMap = new HashMap();
        hashMap.put("Icy-MetaData", "1");
        return Collections.unmodifiableMap(hashMap);
    }

    /* JADX INFO: Access modifiers changed from: private */
    public /* synthetic */ void R() {
        if (this.P0) {
            return;
        }
        ((i.a) ii.e(this.u0)).i(this);
    }

    /* JADX INFO: Access modifiers changed from: private */
    public /* synthetic */ void S() {
        this.J0 = true;
    }

    public final void J() {
        ii.g(this.z0);
        ii.e(this.B0);
        ii.e(this.C0);
    }

    public final boolean K(a aVar, int i) {
        wi3 wi3Var;
        if (!this.J0 && ((wi3Var = this.C0) == null || wi3Var.i() == CellBase.ID_SYSTEM_MESSAGE_REQUEST_CLOSED)) {
            if (this.z0 && !k0()) {
                this.M0 = true;
                return false;
            }
            this.H0 = this.z0;
            this.K0 = 0L;
            this.N0 = 0;
            for (q qVar : this.w0) {
                qVar.U();
            }
            aVar.j(0L, 0L);
            return true;
        }
        this.N0 = i;
        return true;
    }

    public final int M() {
        int i = 0;
        for (q qVar : this.w0) {
            i += qVar.G();
        }
        return i;
    }

    public final long N(boolean z) {
        long j = Long.MIN_VALUE;
        for (int i = 0; i < this.w0.length; i++) {
            if (z || ((e) ii.e(this.B0)).c[i]) {
                j = Math.max(j, this.w0[i].z());
            }
        }
        return j;
    }

    public f84 O() {
        return d0(new d(0, true));
    }

    public final boolean P() {
        return this.L0 != CellBase.ID_SYSTEM_MESSAGE_REQUEST_CLOSED;
    }

    public boolean Q(int i) {
        return !k0() && this.w0[i].K(this.O0);
    }

    public final void U() {
        Metadata a2;
        if (this.P0 || this.z0 || !this.y0 || this.C0 == null) {
            return;
        }
        for (q qVar : this.w0) {
            if (qVar.F() == null) {
                return;
            }
        }
        this.q0.c();
        int length = this.w0.length;
        v[] vVarArr = new v[length];
        boolean[] zArr = new boolean[length];
        for (int i = 0; i < length; i++) {
            androidx.media3.common.j jVar = (androidx.media3.common.j) ii.e(this.w0[i].F());
            String str = jVar.p0;
            boolean m = y82.m(str);
            boolean z = m || y82.q(str);
            zArr[i] = z;
            this.A0 = z | this.A0;
            IcyHeaders icyHeaders = this.v0;
            if (icyHeaders != null) {
                if (m || this.x0[i].b) {
                    Metadata metadata = jVar.n0;
                    if (metadata == null) {
                        a2 = new Metadata(icyHeaders);
                    } else {
                        a2 = metadata.a(icyHeaders);
                    }
                    jVar = jVar.b().X(a2).E();
                }
                if (m && jVar.j0 == -1 && jVar.k0 == -1 && icyHeaders.a != -1) {
                    jVar = jVar.b().G(icyHeaders.a).E();
                }
            }
            vVarArr[i] = new v(Integer.toString(i), jVar.c(this.g0.e(jVar)));
        }
        this.B0 = new e(new c84(vVarArr), zArr);
        this.z0 = true;
        ((i.a) ii.e(this.u0)).f(this);
    }

    public final void V(int i) {
        J();
        e eVar = this.B0;
        boolean[] zArr = eVar.d;
        if (zArr[i]) {
            return;
        }
        androidx.media3.common.j c2 = eVar.a.b(i).c(0);
        this.i0.i(y82.i(c2.p0), c2, 0, null, this.K0);
        zArr[i] = true;
    }

    public final void W(int i) {
        J();
        boolean[] zArr = this.B0.b;
        if (this.M0 && zArr[i]) {
            if (this.w0[i].K(false)) {
                return;
            }
            this.L0 = 0L;
            this.M0 = false;
            this.H0 = true;
            this.K0 = 0L;
            this.N0 = 0;
            for (q qVar : this.w0) {
                qVar.U();
            }
            ((i.a) ii.e(this.u0)).i(this);
        }
    }

    public void X() throws IOException {
        this.o0.k(this.h0.c(this.F0));
    }

    public void Y(int i) throws IOException {
        this.w0[i].N();
        X();
    }

    public final void Z() {
        this.t0.post(new Runnable() { // from class: sv2
            @Override // java.lang.Runnable
            public final void run() {
                n.this.S();
            }
        });
    }

    @Override // androidx.media3.exoplayer.source.i, androidx.media3.exoplayer.source.s
    public long a() {
        return g();
    }

    @Override // androidx.media3.exoplayer.upstream.Loader.b
    /* renamed from: a0 */
    public void u(a aVar, long j, long j2, boolean z) {
        androidx.media3.datasource.f fVar = aVar.c;
        u02 u02Var = new u02(aVar.a, aVar.k, fVar.p(), fVar.q(), j, j2, fVar.o());
        this.h0.b(aVar.a);
        this.i0.r(u02Var, 1, -1, null, 0, null, aVar.j, this.D0);
        if (z) {
            return;
        }
        for (q qVar : this.w0) {
            qVar.U();
        }
        if (this.I0 > 0) {
            ((i.a) ii.e(this.u0)).i(this);
        }
    }

    @Override // androidx.media3.exoplayer.source.q.d
    public void b(androidx.media3.common.j jVar) {
        this.t0.post(this.r0);
    }

    @Override // androidx.media3.exoplayer.upstream.Loader.b
    /* renamed from: b0 */
    public void s(a aVar, long j, long j2) {
        wi3 wi3Var;
        if (this.D0 == CellBase.ID_SYSTEM_MESSAGE_REQUEST_CLOSED && (wi3Var = this.C0) != null) {
            boolean e2 = wi3Var.e();
            long N = N(true);
            long j3 = N == Long.MIN_VALUE ? 0L : N + 10000;
            this.D0 = j3;
            this.k0.h(j3, e2, this.E0);
        }
        androidx.media3.datasource.f fVar = aVar.c;
        u02 u02Var = new u02(aVar.a, aVar.k, fVar.p(), fVar.q(), j, j2, fVar.o());
        this.h0.b(aVar.a);
        this.i0.u(u02Var, 1, -1, null, 0, null, aVar.j, this.D0);
        this.O0 = true;
        ((i.a) ii.e(this.u0)).i(this);
    }

    @Override // androidx.media3.exoplayer.source.i
    public long c(long j, xi3 xi3Var) {
        J();
        if (this.C0.e()) {
            wi3.a h = this.C0.h(j);
            return xi3Var.a(j, h.a.a, h.b.a);
        }
        return 0L;
    }

    @Override // androidx.media3.exoplayer.upstream.Loader.b
    /* renamed from: c0 */
    public Loader.c j(a aVar, long j, long j2, IOException iOException, int i) {
        boolean z;
        a aVar2;
        Loader.c cVar;
        androidx.media3.datasource.f fVar = aVar.c;
        u02 u02Var = new u02(aVar.a, aVar.k, fVar.p(), fVar.q(), j, j2, fVar.o());
        long a2 = this.h0.a(new b.c(u02Var, new g62(1, -1, null, 0, null, androidx.media3.common.util.b.U0(aVar.j), androidx.media3.common.util.b.U0(this.D0)), iOException, i));
        if (a2 == CellBase.ID_SYSTEM_MESSAGE_REQUEST_CLOSED) {
            cVar = Loader.e;
        } else {
            int M = M();
            if (M > this.N0) {
                aVar2 = aVar;
                z = true;
            } else {
                z = false;
                aVar2 = aVar;
            }
            if (K(aVar2, M)) {
                cVar = Loader.h(z, a2);
            } else {
                cVar = Loader.d;
            }
        }
        boolean z2 = !cVar.c();
        this.i0.w(u02Var, 1, -1, null, 0, null, aVar.j, this.D0, iOException, z2);
        if (z2) {
            this.h0.b(aVar.a);
        }
        return cVar;
    }

    @Override // androidx.media3.exoplayer.source.i, androidx.media3.exoplayer.source.s
    public boolean d(long j) {
        if (this.O0 || this.o0.i() || this.M0) {
            return false;
        }
        if (this.z0 && this.I0 == 0) {
            return false;
        }
        boolean e2 = this.q0.e();
        if (this.o0.j()) {
            return e2;
        }
        j0();
        return true;
    }

    public final f84 d0(d dVar) {
        int length = this.w0.length;
        for (int i = 0; i < length; i++) {
            if (dVar.equals(this.x0[i])) {
                return this.w0[i];
            }
        }
        q k = q.k(this.l0, this.g0, this.j0);
        k.c0(this);
        int i2 = length + 1;
        d[] dVarArr = (d[]) Arrays.copyOf(this.x0, i2);
        dVarArr[length] = dVar;
        this.x0 = (d[]) androidx.media3.common.util.b.k(dVarArr);
        q[] qVarArr = (q[]) Arrays.copyOf(this.w0, i2);
        qVarArr[length] = k;
        this.w0 = (q[]) androidx.media3.common.util.b.k(qVarArr);
        return k;
    }

    @Override // androidx.media3.exoplayer.source.i, androidx.media3.exoplayer.source.s
    public boolean e() {
        return this.o0.j() && this.q0.d();
    }

    public int e0(int i, y81 y81Var, DecoderInputBuffer decoderInputBuffer, int i2) {
        if (k0()) {
            return -3;
        }
        V(i);
        int R = this.w0[i].R(y81Var, decoderInputBuffer, i2, this.O0);
        if (R == -3) {
            W(i);
        }
        return R;
    }

    @Override // defpackage.r11
    public f84 f(int i, int i2) {
        return d0(new d(i, false));
    }

    public void f0() {
        if (this.z0) {
            for (q qVar : this.w0) {
                qVar.Q();
            }
        }
        this.o0.m(this);
        this.t0.removeCallbacksAndMessages(null);
        this.u0 = null;
        this.P0 = true;
    }

    @Override // androidx.media3.exoplayer.source.i, androidx.media3.exoplayer.source.s
    public long g() {
        long j;
        J();
        if (this.O0 || this.I0 == 0) {
            return Long.MIN_VALUE;
        }
        if (P()) {
            return this.L0;
        }
        if (this.A0) {
            int length = this.w0.length;
            j = Long.MAX_VALUE;
            for (int i = 0; i < length; i++) {
                e eVar = this.B0;
                if (eVar.b[i] && eVar.c[i] && !this.w0[i].J()) {
                    j = Math.min(j, this.w0[i].z());
                }
            }
        } else {
            j = Long.MAX_VALUE;
        }
        if (j == Long.MAX_VALUE) {
            j = N(false);
        }
        return j == Long.MIN_VALUE ? this.K0 : j;
    }

    public final boolean g0(boolean[] zArr, long j) {
        int length = this.w0.length;
        for (int i = 0; i < length; i++) {
            if (!this.w0[i].Y(j, false) && (zArr[i] || !this.A0)) {
                return false;
            }
        }
        return true;
    }

    @Override // androidx.media3.exoplayer.source.i, androidx.media3.exoplayer.source.s
    public void h(long j) {
    }

    /* renamed from: h0 */
    public final void T(wi3 wi3Var) {
        this.C0 = this.v0 == null ? wi3Var : new wi3.b(CellBase.ID_SYSTEM_MESSAGE_REQUEST_CLOSED);
        this.D0 = wi3Var.i();
        boolean z = !this.J0 && wi3Var.i() == CellBase.ID_SYSTEM_MESSAGE_REQUEST_CLOSED;
        this.E0 = z;
        this.F0 = z ? 7 : 1;
        this.k0.h(this.D0, wi3Var.e(), this.E0);
        if (this.z0) {
            return;
        }
        U();
    }

    @Override // androidx.media3.exoplayer.upstream.Loader.f
    public void i() {
        for (q qVar : this.w0) {
            qVar.S();
        }
        this.p0.a();
    }

    public int i0(int i, long j) {
        if (k0()) {
            return 0;
        }
        V(i);
        q qVar = this.w0[i];
        int E = qVar.E(j, this.O0);
        qVar.d0(E);
        if (E == 0) {
            W(i);
        }
        return E;
    }

    public final void j0() {
        a aVar = new a(this.a, this.f0, this.p0, this, this.q0);
        if (this.z0) {
            ii.g(P());
            long j = this.D0;
            if (j == CellBase.ID_SYSTEM_MESSAGE_REQUEST_CLOSED || this.L0 <= j) {
                aVar.j(((wi3) ii.e(this.C0)).h(this.L0).a.b, this.L0);
                for (q qVar : this.w0) {
                    qVar.a0(this.L0);
                }
                this.L0 = CellBase.ID_SYSTEM_MESSAGE_REQUEST_CLOSED;
            } else {
                this.O0 = true;
                this.L0 = CellBase.ID_SYSTEM_MESSAGE_REQUEST_CLOSED;
                return;
            }
        }
        this.N0 = M();
        this.i0.A(new u02(aVar.a, aVar.k, this.o0.n(aVar, this, this.h0.c(this.F0))), 1, -1, null, 0, null, aVar.j, this.D0);
    }

    @Override // androidx.media3.exoplayer.source.i
    public void k() throws IOException {
        X();
        if (this.O0 && !this.z0) {
            throw ParserException.createForMalformedContainer("Loading finished before preparation is complete.", null);
        }
    }

    public final boolean k0() {
        return this.H0 || P();
    }

    @Override // androidx.media3.exoplayer.source.i
    public long l(long j) {
        J();
        boolean[] zArr = this.B0.b;
        if (!this.C0.e()) {
            j = 0;
        }
        int i = 0;
        this.H0 = false;
        this.K0 = j;
        if (P()) {
            this.L0 = j;
            return j;
        } else if (this.F0 == 7 || !g0(zArr, j)) {
            this.M0 = false;
            this.L0 = j;
            this.O0 = false;
            if (this.o0.j()) {
                q[] qVarArr = this.w0;
                int length = qVarArr.length;
                while (i < length) {
                    qVarArr[i].r();
                    i++;
                }
                this.o0.f();
            } else {
                this.o0.g();
                q[] qVarArr2 = this.w0;
                int length2 = qVarArr2.length;
                while (i < length2) {
                    qVarArr2[i].U();
                    i++;
                }
            }
            return j;
        } else {
            return j;
        }
    }

    @Override // defpackage.r11
    public void m() {
        this.y0 = true;
        this.t0.post(this.r0);
    }

    @Override // androidx.media3.exoplayer.source.i
    public long n(androidx.media3.exoplayer.trackselection.c[] cVarArr, boolean[] zArr, r[] rVarArr, boolean[] zArr2, long j) {
        J();
        e eVar = this.B0;
        c84 c84Var = eVar.a;
        boolean[] zArr3 = eVar.c;
        int i = this.I0;
        int i2 = 0;
        for (int i3 = 0; i3 < cVarArr.length; i3++) {
            if (rVarArr[i3] != null && (cVarArr[i3] == null || !zArr[i3])) {
                int i4 = ((c) rVarArr[i3]).a;
                ii.g(zArr3[i4]);
                this.I0--;
                zArr3[i4] = false;
                rVarArr[i3] = null;
            }
        }
        boolean z = !this.G0 ? j == 0 : i != 0;
        for (int i5 = 0; i5 < cVarArr.length; i5++) {
            if (rVarArr[i5] == null && cVarArr[i5] != null) {
                androidx.media3.exoplayer.trackselection.c cVar = cVarArr[i5];
                ii.g(cVar.length() == 1);
                ii.g(cVar.l(0) == 0);
                int c2 = c84Var.c(cVar.c());
                ii.g(!zArr3[c2]);
                this.I0++;
                zArr3[c2] = true;
                rVarArr[i5] = new c(c2);
                zArr2[i5] = true;
                if (!z) {
                    q qVar = this.w0[c2];
                    z = (qVar.Y(j, true) || qVar.C() == 0) ? false : true;
                }
            }
        }
        if (this.I0 == 0) {
            this.M0 = false;
            this.H0 = false;
            if (this.o0.j()) {
                q[] qVarArr = this.w0;
                int length = qVarArr.length;
                while (i2 < length) {
                    qVarArr[i2].r();
                    i2++;
                }
                this.o0.f();
            } else {
                q[] qVarArr2 = this.w0;
                int length2 = qVarArr2.length;
                while (i2 < length2) {
                    qVarArr2[i2].U();
                    i2++;
                }
            }
        } else if (z) {
            j = l(j);
            while (i2 < rVarArr.length) {
                if (rVarArr[i2] != null) {
                    zArr2[i2] = true;
                }
                i2++;
            }
        }
        this.G0 = true;
        return j;
    }

    @Override // androidx.media3.exoplayer.source.i
    public long o() {
        if (this.H0) {
            if (this.O0 || M() > this.N0) {
                this.H0 = false;
                return this.K0;
            }
            return CellBase.ID_SYSTEM_MESSAGE_REQUEST_CLOSED;
        }
        return CellBase.ID_SYSTEM_MESSAGE_REQUEST_CLOSED;
    }

    @Override // defpackage.r11
    public void p(final wi3 wi3Var) {
        this.t0.post(new Runnable() { // from class: vv2
            @Override // java.lang.Runnable
            public final void run() {
                n.this.T(wi3Var);
            }
        });
    }

    @Override // androidx.media3.exoplayer.source.i
    public void q(i.a aVar, long j) {
        this.u0 = aVar;
        this.q0.e();
        j0();
    }

    @Override // androidx.media3.exoplayer.source.i
    public c84 r() {
        J();
        return this.B0.a;
    }

    @Override // androidx.media3.exoplayer.source.i
    public void t(long j, boolean z) {
        J();
        if (P()) {
            return;
        }
        boolean[] zArr = this.B0.c;
        int length = this.w0.length;
        for (int i = 0; i < length; i++) {
            this.w0[i].q(j, z, zArr[i]);
        }
    }
}
