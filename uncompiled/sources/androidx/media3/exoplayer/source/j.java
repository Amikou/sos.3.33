package androidx.media3.exoplayer.source;

import android.os.Handler;
import java.io.IOException;

/* compiled from: MediaSource.java */
/* loaded from: classes.dex */
public interface j {

    /* compiled from: MediaSource.java */
    /* loaded from: classes.dex */
    public interface a {
        j a(androidx.media3.common.m mVar);

        a b(androidx.media3.exoplayer.upstream.b bVar);

        a c(zr0 zr0Var);
    }

    /* compiled from: MediaSource.java */
    /* loaded from: classes.dex */
    public static final class b extends j62 {
        public b(Object obj) {
            super(obj);
        }

        public b c(Object obj) {
            return new b(super.a(obj));
        }

        public b(Object obj, long j) {
            super(obj, j);
        }

        public b(Object obj, long j, int i) {
            super(obj, j, i);
        }

        public b(Object obj, int i, int i2, long j) {
            super(obj, i, i2, j);
        }

        public b(j62 j62Var) {
            super(j62Var);
        }
    }

    /* compiled from: MediaSource.java */
    /* loaded from: classes.dex */
    public interface c {
        void a(j jVar, androidx.media3.common.u uVar);
    }

    void a(Handler handler, k kVar);

    void b(c cVar);

    void c(c cVar, fa4 fa4Var, ks2 ks2Var);

    void d(k kVar);

    i e(b bVar, gb gbVar, long j);

    void f(c cVar);

    void g(c cVar);

    androidx.media3.common.m i();

    void j() throws IOException;

    boolean k();

    androidx.media3.common.u l();

    void m(Handler handler, androidx.media3.exoplayer.drm.b bVar);

    void n(androidx.media3.exoplayer.drm.b bVar);

    void o(i iVar);
}
