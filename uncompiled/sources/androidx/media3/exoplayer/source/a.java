package androidx.media3.exoplayer.source;

import android.os.Handler;
import android.os.Looper;
import androidx.media3.exoplayer.drm.b;
import androidx.media3.exoplayer.source.j;
import androidx.media3.exoplayer.source.k;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.Iterator;

/* compiled from: BaseMediaSource.java */
/* loaded from: classes.dex */
public abstract class a implements j {
    public final ArrayList<j.c> a = new ArrayList<>(1);
    public final HashSet<j.c> b = new HashSet<>(1);
    public final k.a c = new k.a();
    public final b.a d = new b.a();
    public Looper e;
    public androidx.media3.common.u f;
    public ks2 g;

    public abstract void A();

    @Override // androidx.media3.exoplayer.source.j
    public final void a(Handler handler, k kVar) {
        ii.e(handler);
        ii.e(kVar);
        this.c.g(handler, kVar);
    }

    @Override // androidx.media3.exoplayer.source.j
    public final void b(j.c cVar) {
        ii.e(this.e);
        boolean isEmpty = this.b.isEmpty();
        this.b.add(cVar);
        if (isEmpty) {
            v();
        }
    }

    @Override // androidx.media3.exoplayer.source.j
    public final void c(j.c cVar, fa4 fa4Var, ks2 ks2Var) {
        Looper myLooper = Looper.myLooper();
        Looper looper = this.e;
        ii.a(looper == null || looper == myLooper);
        this.g = ks2Var;
        androidx.media3.common.u uVar = this.f;
        this.a.add(cVar);
        if (this.e == null) {
            this.e = myLooper;
            this.b.add(cVar);
            y(fa4Var);
        } else if (uVar != null) {
            b(cVar);
            cVar.a(this, uVar);
        }
    }

    @Override // androidx.media3.exoplayer.source.j
    public final void d(k kVar) {
        this.c.C(kVar);
    }

    @Override // androidx.media3.exoplayer.source.j
    public final void f(j.c cVar) {
        this.a.remove(cVar);
        if (this.a.isEmpty()) {
            this.e = null;
            this.f = null;
            this.g = null;
            this.b.clear();
            A();
            return;
        }
        g(cVar);
    }

    @Override // androidx.media3.exoplayer.source.j
    public final void g(j.c cVar) {
        boolean z = !this.b.isEmpty();
        this.b.remove(cVar);
        if (z && this.b.isEmpty()) {
            u();
        }
    }

    @Override // androidx.media3.exoplayer.source.j
    public /* synthetic */ boolean k() {
        return r62.b(this);
    }

    @Override // androidx.media3.exoplayer.source.j
    public /* synthetic */ androidx.media3.common.u l() {
        return r62.a(this);
    }

    @Override // androidx.media3.exoplayer.source.j
    public final void m(Handler handler, androidx.media3.exoplayer.drm.b bVar) {
        ii.e(handler);
        ii.e(bVar);
        this.d.g(handler, bVar);
    }

    @Override // androidx.media3.exoplayer.source.j
    public final void n(androidx.media3.exoplayer.drm.b bVar) {
        this.d.t(bVar);
    }

    public final b.a p(int i, j.b bVar) {
        return this.d.u(i, bVar);
    }

    public final b.a q(j.b bVar) {
        return this.d.u(0, bVar);
    }

    public final k.a r(int i, j.b bVar, long j) {
        return this.c.F(i, bVar, j);
    }

    public final k.a s(j.b bVar) {
        return this.c.F(0, bVar, 0L);
    }

    public final k.a t(j.b bVar, long j) {
        ii.e(bVar);
        return this.c.F(0, bVar, j);
    }

    public void u() {
    }

    public void v() {
    }

    public final ks2 w() {
        return (ks2) ii.i(this.g);
    }

    public final boolean x() {
        return !this.b.isEmpty();
    }

    public abstract void y(fa4 fa4Var);

    public final void z(androidx.media3.common.u uVar) {
        this.f = uVar;
        Iterator<j.c> it = this.a.iterator();
        while (it.hasNext()) {
            it.next().a(this, uVar);
        }
    }
}
