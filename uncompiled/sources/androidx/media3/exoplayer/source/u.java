package androidx.media3.exoplayer.source;

import android.net.Uri;
import androidx.media3.common.j;
import androidx.media3.common.m;
import androidx.media3.datasource.b;
import androidx.media3.exoplayer.source.j;
import com.google.common.collect.ImmutableList;
import defpackage.je0;

/* compiled from: SingleSampleMediaSource.java */
/* loaded from: classes.dex */
public final class u extends androidx.media3.exoplayer.source.a {
    public final je0 h;
    public final b.a i;
    public final androidx.media3.common.j j;
    public final long k;
    public final androidx.media3.exoplayer.upstream.b l;
    public final boolean m;
    public final androidx.media3.common.u n;
    public final androidx.media3.common.m o;
    public fa4 p;

    /* compiled from: SingleSampleMediaSource.java */
    /* loaded from: classes.dex */
    public static final class b {
        public final b.a a;
        public androidx.media3.exoplayer.upstream.b b = new androidx.media3.exoplayer.upstream.a();
        public boolean c = true;
        public Object d;
        public String e;

        public b(b.a aVar) {
            this.a = (b.a) ii.e(aVar);
        }

        public u a(m.l lVar, long j) {
            return new u(this.e, lVar, this.a, j, this.b, this.c, this.d);
        }

        public b b(androidx.media3.exoplayer.upstream.b bVar) {
            if (bVar == null) {
                bVar = new androidx.media3.exoplayer.upstream.a();
            }
            this.b = bVar;
            return this;
        }
    }

    @Override // androidx.media3.exoplayer.source.a
    public void A() {
    }

    @Override // androidx.media3.exoplayer.source.j
    public i e(j.b bVar, gb gbVar, long j) {
        return new t(this.h, this.i, this.p, this.j, this.k, this.l, s(bVar), this.m);
    }

    @Override // androidx.media3.exoplayer.source.j
    public androidx.media3.common.m i() {
        return this.o;
    }

    @Override // androidx.media3.exoplayer.source.j
    public void j() {
    }

    @Override // androidx.media3.exoplayer.source.j
    public void o(i iVar) {
        ((t) iVar).p();
    }

    @Override // androidx.media3.exoplayer.source.a
    public void y(fa4 fa4Var) {
        this.p = fa4Var;
        z(this.n);
    }

    public u(String str, m.l lVar, b.a aVar, long j, androidx.media3.exoplayer.upstream.b bVar, boolean z, Object obj) {
        this.i = aVar;
        this.k = j;
        this.l = bVar;
        this.m = z;
        androidx.media3.common.m a2 = new m.c().g(Uri.EMPTY).d(lVar.a.toString()).e(ImmutableList.of(lVar)).f(obj).a();
        this.o = a2;
        j.b U = new j.b().e0((String) s92.a(lVar.b, "text/x-unknown")).V(lVar.c).g0(lVar.d).c0(lVar.e).U(lVar.f);
        String str2 = lVar.g;
        this.j = U.S(str2 == null ? str : str2).E();
        this.h = new je0.b().i(lVar.a).b(1).a();
        this.n = new vp3(j, true, false, false, null, a2);
    }
}
