package androidx.media3.exoplayer.source;

/* compiled from: SequenceableLoader.java */
/* loaded from: classes.dex */
public interface s {

    /* compiled from: SequenceableLoader.java */
    /* loaded from: classes.dex */
    public interface a<T extends s> {
        void i(T t);
    }

    long a();

    boolean d(long j);

    boolean e();

    long g();

    void h(long j);
}
