package androidx.media3.exoplayer.source;

import android.os.Handler;
import androidx.media3.exoplayer.source.j;
import androidx.media3.exoplayer.source.k;
import java.io.IOException;
import java.util.Iterator;
import java.util.concurrent.CopyOnWriteArrayList;
import zendesk.support.request.CellBase;

/* compiled from: MediaSourceEventListener.java */
/* loaded from: classes.dex */
public interface k {

    /* compiled from: MediaSourceEventListener.java */
    /* loaded from: classes.dex */
    public static class a {
        public final int a;
        public final j.b b;
        public final CopyOnWriteArrayList<C0039a> c;
        public final long d;

        /* compiled from: MediaSourceEventListener.java */
        /* renamed from: androidx.media3.exoplayer.source.k$a$a  reason: collision with other inner class name */
        /* loaded from: classes.dex */
        public static final class C0039a {
            public Handler a;
            public k b;

            public C0039a(Handler handler, k kVar) {
                this.a = handler;
                this.b = kVar;
            }
        }

        public a() {
            this(new CopyOnWriteArrayList(), 0, null, 0L);
        }

        /* JADX INFO: Access modifiers changed from: private */
        public /* synthetic */ void k(k kVar, g62 g62Var) {
            kVar.S(this.a, this.b, g62Var);
        }

        /* JADX INFO: Access modifiers changed from: private */
        public /* synthetic */ void l(k kVar, u02 u02Var, g62 g62Var) {
            kVar.O(this.a, this.b, u02Var, g62Var);
        }

        /* JADX INFO: Access modifiers changed from: private */
        public /* synthetic */ void m(k kVar, u02 u02Var, g62 g62Var) {
            kVar.i0(this.a, this.b, u02Var, g62Var);
        }

        /* JADX INFO: Access modifiers changed from: private */
        public /* synthetic */ void n(k kVar, u02 u02Var, g62 g62Var, IOException iOException, boolean z) {
            kVar.b0(this.a, this.b, u02Var, g62Var, iOException, z);
        }

        /* JADX INFO: Access modifiers changed from: private */
        public /* synthetic */ void o(k kVar, u02 u02Var, g62 g62Var) {
            kVar.f0(this.a, this.b, u02Var, g62Var);
        }

        /* JADX INFO: Access modifiers changed from: private */
        public /* synthetic */ void p(k kVar, j.b bVar, g62 g62Var) {
            kVar.W(this.a, bVar, g62Var);
        }

        public void A(u02 u02Var, int i, int i2, androidx.media3.common.j jVar, int i3, Object obj, long j, long j2) {
            B(u02Var, new g62(i, i2, jVar, i3, obj, h(j), h(j2)));
        }

        public void B(final u02 u02Var, final g62 g62Var) {
            Iterator<C0039a> it = this.c.iterator();
            while (it.hasNext()) {
                C0039a next = it.next();
                final k kVar = next.b;
                androidx.media3.common.util.b.G0(next.a, new Runnable() { // from class: t62
                    @Override // java.lang.Runnable
                    public final void run() {
                        k.a.this.o(kVar, u02Var, g62Var);
                    }
                });
            }
        }

        public void C(k kVar) {
            Iterator<C0039a> it = this.c.iterator();
            while (it.hasNext()) {
                C0039a next = it.next();
                if (next.b == kVar) {
                    this.c.remove(next);
                }
            }
        }

        public void D(int i, long j, long j2) {
            E(new g62(1, i, null, 3, null, h(j), h(j2)));
        }

        public void E(final g62 g62Var) {
            final j.b bVar = (j.b) ii.e(this.b);
            Iterator<C0039a> it = this.c.iterator();
            while (it.hasNext()) {
                C0039a next = it.next();
                final k kVar = next.b;
                androidx.media3.common.util.b.G0(next.a, new Runnable() { // from class: y62
                    @Override // java.lang.Runnable
                    public final void run() {
                        k.a.this.p(kVar, bVar, g62Var);
                    }
                });
            }
        }

        public a F(int i, j.b bVar, long j) {
            return new a(this.c, i, bVar, j);
        }

        public void g(Handler handler, k kVar) {
            ii.e(handler);
            ii.e(kVar);
            this.c.add(new C0039a(handler, kVar));
        }

        public final long h(long j) {
            long U0 = androidx.media3.common.util.b.U0(j);
            return U0 == CellBase.ID_SYSTEM_MESSAGE_REQUEST_CLOSED ? CellBase.ID_SYSTEM_MESSAGE_REQUEST_CLOSED : this.d + U0;
        }

        public void i(int i, androidx.media3.common.j jVar, int i2, Object obj, long j) {
            j(new g62(1, i, jVar, i2, obj, h(j), CellBase.ID_SYSTEM_MESSAGE_REQUEST_CLOSED));
        }

        public void j(final g62 g62Var) {
            Iterator<C0039a> it = this.c.iterator();
            while (it.hasNext()) {
                C0039a next = it.next();
                final k kVar = next.b;
                androidx.media3.common.util.b.G0(next.a, new Runnable() { // from class: x62
                    @Override // java.lang.Runnable
                    public final void run() {
                        k.a.this.k(kVar, g62Var);
                    }
                });
            }
        }

        public void q(u02 u02Var, int i) {
            r(u02Var, i, -1, null, 0, null, CellBase.ID_SYSTEM_MESSAGE_REQUEST_CLOSED, CellBase.ID_SYSTEM_MESSAGE_REQUEST_CLOSED);
        }

        public void r(u02 u02Var, int i, int i2, androidx.media3.common.j jVar, int i3, Object obj, long j, long j2) {
            s(u02Var, new g62(i, i2, jVar, i3, obj, h(j), h(j2)));
        }

        public void s(final u02 u02Var, final g62 g62Var) {
            Iterator<C0039a> it = this.c.iterator();
            while (it.hasNext()) {
                C0039a next = it.next();
                final k kVar = next.b;
                androidx.media3.common.util.b.G0(next.a, new Runnable() { // from class: u62
                    @Override // java.lang.Runnable
                    public final void run() {
                        k.a.this.l(kVar, u02Var, g62Var);
                    }
                });
            }
        }

        public void t(u02 u02Var, int i) {
            u(u02Var, i, -1, null, 0, null, CellBase.ID_SYSTEM_MESSAGE_REQUEST_CLOSED, CellBase.ID_SYSTEM_MESSAGE_REQUEST_CLOSED);
        }

        public void u(u02 u02Var, int i, int i2, androidx.media3.common.j jVar, int i3, Object obj, long j, long j2) {
            v(u02Var, new g62(i, i2, jVar, i3, obj, h(j), h(j2)));
        }

        public void v(final u02 u02Var, final g62 g62Var) {
            Iterator<C0039a> it = this.c.iterator();
            while (it.hasNext()) {
                C0039a next = it.next();
                final k kVar = next.b;
                androidx.media3.common.util.b.G0(next.a, new Runnable() { // from class: v62
                    @Override // java.lang.Runnable
                    public final void run() {
                        k.a.this.m(kVar, u02Var, g62Var);
                    }
                });
            }
        }

        public void w(u02 u02Var, int i, int i2, androidx.media3.common.j jVar, int i3, Object obj, long j, long j2, IOException iOException, boolean z) {
            y(u02Var, new g62(i, i2, jVar, i3, obj, h(j), h(j2)), iOException, z);
        }

        public void x(u02 u02Var, int i, IOException iOException, boolean z) {
            w(u02Var, i, -1, null, 0, null, CellBase.ID_SYSTEM_MESSAGE_REQUEST_CLOSED, CellBase.ID_SYSTEM_MESSAGE_REQUEST_CLOSED, iOException, z);
        }

        public void y(final u02 u02Var, final g62 g62Var, final IOException iOException, final boolean z) {
            Iterator<C0039a> it = this.c.iterator();
            while (it.hasNext()) {
                C0039a next = it.next();
                final k kVar = next.b;
                androidx.media3.common.util.b.G0(next.a, new Runnable() { // from class: w62
                    @Override // java.lang.Runnable
                    public final void run() {
                        k.a.this.n(kVar, u02Var, g62Var, iOException, z);
                    }
                });
            }
        }

        public void z(u02 u02Var, int i) {
            A(u02Var, i, -1, null, 0, null, CellBase.ID_SYSTEM_MESSAGE_REQUEST_CLOSED, CellBase.ID_SYSTEM_MESSAGE_REQUEST_CLOSED);
        }

        public a(CopyOnWriteArrayList<C0039a> copyOnWriteArrayList, int i, j.b bVar, long j) {
            this.c = copyOnWriteArrayList;
            this.a = i;
            this.b = bVar;
            this.d = j;
        }
    }

    void O(int i, j.b bVar, u02 u02Var, g62 g62Var);

    void S(int i, j.b bVar, g62 g62Var);

    void W(int i, j.b bVar, g62 g62Var);

    void b0(int i, j.b bVar, u02 u02Var, g62 g62Var, IOException iOException, boolean z);

    void f0(int i, j.b bVar, u02 u02Var, g62 g62Var);

    void i0(int i, j.b bVar, u02 u02Var, g62 g62Var);
}
