package androidx.media3.exoplayer.source;

import androidx.media3.common.v;
import androidx.media3.decoder.DecoderInputBuffer;
import androidx.media3.exoplayer.source.chunk.MediaChunkIterator;
import androidx.media3.exoplayer.source.i;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.IdentityHashMap;
import java.util.List;
import zendesk.support.request.CellBase;

/* compiled from: MergingMediaPeriod.java */
/* loaded from: classes.dex */
public final class l implements i, i.a {
    public final i[] a;
    public final q40 g0;
    public i.a j0;
    public c84 k0;
    public s m0;
    public final ArrayList<i> h0 = new ArrayList<>();
    public final HashMap<v, v> i0 = new HashMap<>();
    public final IdentityHashMap<r, Integer> f0 = new IdentityHashMap<>();
    public i[] l0 = new i[0];

    /* compiled from: MergingMediaPeriod.java */
    /* loaded from: classes.dex */
    public static final class a implements androidx.media3.exoplayer.trackselection.c {
        public final androidx.media3.exoplayer.trackselection.c a;
        public final v b;

        public a(androidx.media3.exoplayer.trackselection.c cVar, v vVar) {
            this.a = cVar;
            this.b = vVar;
        }

        @Override // androidx.media3.exoplayer.trackselection.c
        public void a(long j, long j2, long j3, List<? extends s52> list, MediaChunkIterator[] mediaChunkIteratorArr) {
            this.a.a(j, j2, j3, list, mediaChunkIteratorArr);
        }

        @Override // defpackage.h84
        public int b(androidx.media3.common.j jVar) {
            return this.a.b(jVar);
        }

        @Override // defpackage.h84
        public v c() {
            return this.b;
        }

        @Override // androidx.media3.exoplayer.trackselection.c
        public int d() {
            return this.a.d();
        }

        @Override // androidx.media3.exoplayer.trackselection.c
        public boolean e(long j, my myVar, List<? extends s52> list) {
            return this.a.e(j, myVar, list);
        }

        public boolean equals(Object obj) {
            if (this == obj) {
                return true;
            }
            if (obj instanceof a) {
                a aVar = (a) obj;
                return this.a.equals(aVar.a) && this.b.equals(aVar.b);
            }
            return false;
        }

        @Override // androidx.media3.exoplayer.trackselection.c
        public boolean f(int i, long j) {
            return this.a.f(i, j);
        }

        @Override // androidx.media3.exoplayer.trackselection.c
        public void g() {
            this.a.g();
        }

        @Override // androidx.media3.exoplayer.trackselection.c
        public boolean h(int i, long j) {
            return this.a.h(i, j);
        }

        public int hashCode() {
            return ((527 + this.b.hashCode()) * 31) + this.a.hashCode();
        }

        @Override // androidx.media3.exoplayer.trackselection.c
        public void i(boolean z) {
            this.a.i(z);
        }

        @Override // defpackage.h84
        public androidx.media3.common.j j(int i) {
            return this.a.j(i);
        }

        @Override // androidx.media3.exoplayer.trackselection.c
        public void k() {
            this.a.k();
        }

        @Override // defpackage.h84
        public int l(int i) {
            return this.a.l(i);
        }

        @Override // defpackage.h84
        public int length() {
            return this.a.length();
        }

        @Override // androidx.media3.exoplayer.trackselection.c
        public int m(long j, List<? extends s52> list) {
            return this.a.m(j, list);
        }

        @Override // androidx.media3.exoplayer.trackselection.c
        public androidx.media3.common.j n() {
            return this.a.n();
        }

        @Override // androidx.media3.exoplayer.trackselection.c
        public int o() {
            return this.a.o();
        }

        @Override // androidx.media3.exoplayer.trackselection.c
        public void p(float f) {
            this.a.p(f);
        }

        @Override // androidx.media3.exoplayer.trackselection.c
        public Object q() {
            return this.a.q();
        }

        @Override // androidx.media3.exoplayer.trackselection.c
        public void r() {
            this.a.r();
        }

        @Override // androidx.media3.exoplayer.trackselection.c
        public void s() {
            this.a.s();
        }

        @Override // defpackage.h84
        public int t(int i) {
            return this.a.t(i);
        }
    }

    /* compiled from: MergingMediaPeriod.java */
    /* loaded from: classes.dex */
    public static final class b implements i, i.a {
        public final i a;
        public final long f0;
        public i.a g0;

        public b(i iVar, long j) {
            this.a = iVar;
            this.f0 = j;
        }

        @Override // androidx.media3.exoplayer.source.i, androidx.media3.exoplayer.source.s
        public long a() {
            long a = this.a.a();
            if (a == Long.MIN_VALUE) {
                return Long.MIN_VALUE;
            }
            return this.f0 + a;
        }

        @Override // androidx.media3.exoplayer.source.i
        public long c(long j, xi3 xi3Var) {
            return this.a.c(j - this.f0, xi3Var) + this.f0;
        }

        @Override // androidx.media3.exoplayer.source.i, androidx.media3.exoplayer.source.s
        public boolean d(long j) {
            return this.a.d(j - this.f0);
        }

        @Override // androidx.media3.exoplayer.source.i, androidx.media3.exoplayer.source.s
        public boolean e() {
            return this.a.e();
        }

        @Override // androidx.media3.exoplayer.source.i.a
        public void f(i iVar) {
            ((i.a) ii.e(this.g0)).f(this);
        }

        @Override // androidx.media3.exoplayer.source.i, androidx.media3.exoplayer.source.s
        public long g() {
            long g = this.a.g();
            if (g == Long.MIN_VALUE) {
                return Long.MIN_VALUE;
            }
            return this.f0 + g;
        }

        @Override // androidx.media3.exoplayer.source.i, androidx.media3.exoplayer.source.s
        public void h(long j) {
            this.a.h(j - this.f0);
        }

        @Override // androidx.media3.exoplayer.source.s.a
        /* renamed from: j */
        public void i(i iVar) {
            ((i.a) ii.e(this.g0)).i(this);
        }

        @Override // androidx.media3.exoplayer.source.i
        public void k() throws IOException {
            this.a.k();
        }

        @Override // androidx.media3.exoplayer.source.i
        public long l(long j) {
            return this.a.l(j - this.f0) + this.f0;
        }

        @Override // androidx.media3.exoplayer.source.i
        public long n(androidx.media3.exoplayer.trackselection.c[] cVarArr, boolean[] zArr, r[] rVarArr, boolean[] zArr2, long j) {
            r[] rVarArr2 = new r[rVarArr.length];
            int i = 0;
            while (true) {
                r rVar = null;
                if (i >= rVarArr.length) {
                    break;
                }
                c cVar = (c) rVarArr[i];
                if (cVar != null) {
                    rVar = cVar.a();
                }
                rVarArr2[i] = rVar;
                i++;
            }
            long n = this.a.n(cVarArr, zArr, rVarArr2, zArr2, j - this.f0);
            for (int i2 = 0; i2 < rVarArr.length; i2++) {
                r rVar2 = rVarArr2[i2];
                if (rVar2 == null) {
                    rVarArr[i2] = null;
                } else if (rVarArr[i2] == null || ((c) rVarArr[i2]).a() != rVar2) {
                    rVarArr[i2] = new c(rVar2, this.f0);
                }
            }
            return n + this.f0;
        }

        @Override // androidx.media3.exoplayer.source.i
        public long o() {
            long o = this.a.o();
            return o == CellBase.ID_SYSTEM_MESSAGE_REQUEST_CLOSED ? CellBase.ID_SYSTEM_MESSAGE_REQUEST_CLOSED : this.f0 + o;
        }

        @Override // androidx.media3.exoplayer.source.i
        public void q(i.a aVar, long j) {
            this.g0 = aVar;
            this.a.q(this, j - this.f0);
        }

        @Override // androidx.media3.exoplayer.source.i
        public c84 r() {
            return this.a.r();
        }

        @Override // androidx.media3.exoplayer.source.i
        public void t(long j, boolean z) {
            this.a.t(j - this.f0, z);
        }
    }

    /* compiled from: MergingMediaPeriod.java */
    /* loaded from: classes.dex */
    public static final class c implements r {
        public final r a;
        public final long f0;

        public c(r rVar, long j) {
            this.a = rVar;
            this.f0 = j;
        }

        public r a() {
            return this.a;
        }

        @Override // androidx.media3.exoplayer.source.r
        public void b() throws IOException {
            this.a.b();
        }

        @Override // androidx.media3.exoplayer.source.r
        public boolean f() {
            return this.a.f();
        }

        @Override // androidx.media3.exoplayer.source.r
        public int m(long j) {
            return this.a.m(j - this.f0);
        }

        @Override // androidx.media3.exoplayer.source.r
        public int p(y81 y81Var, DecoderInputBuffer decoderInputBuffer, int i) {
            int p = this.a.p(y81Var, decoderInputBuffer, i);
            if (p == -4) {
                decoderInputBuffer.i0 = Math.max(0L, decoderInputBuffer.i0 + this.f0);
            }
            return p;
        }
    }

    public l(q40 q40Var, long[] jArr, i... iVarArr) {
        this.g0 = q40Var;
        this.a = iVarArr;
        this.m0 = q40Var.a(new s[0]);
        for (int i = 0; i < iVarArr.length; i++) {
            if (jArr[i] != 0) {
                this.a[i] = new b(iVarArr[i], jArr[i]);
            }
        }
    }

    @Override // androidx.media3.exoplayer.source.i, androidx.media3.exoplayer.source.s
    public long a() {
        return this.m0.a();
    }

    public i b(int i) {
        i[] iVarArr = this.a;
        if (iVarArr[i] instanceof b) {
            return ((b) iVarArr[i]).a;
        }
        return iVarArr[i];
    }

    @Override // androidx.media3.exoplayer.source.i
    public long c(long j, xi3 xi3Var) {
        i[] iVarArr = this.l0;
        return (iVarArr.length > 0 ? iVarArr[0] : this.a[0]).c(j, xi3Var);
    }

    @Override // androidx.media3.exoplayer.source.i, androidx.media3.exoplayer.source.s
    public boolean d(long j) {
        if (!this.h0.isEmpty()) {
            int size = this.h0.size();
            for (int i = 0; i < size; i++) {
                this.h0.get(i).d(j);
            }
            return false;
        }
        return this.m0.d(j);
    }

    @Override // androidx.media3.exoplayer.source.i, androidx.media3.exoplayer.source.s
    public boolean e() {
        return this.m0.e();
    }

    @Override // androidx.media3.exoplayer.source.i.a
    public void f(i iVar) {
        this.h0.remove(iVar);
        if (!this.h0.isEmpty()) {
            return;
        }
        int i = 0;
        for (i iVar2 : this.a) {
            i += iVar2.r().a;
        }
        v[] vVarArr = new v[i];
        int i2 = 0;
        int i3 = 0;
        while (true) {
            i[] iVarArr = this.a;
            if (i2 < iVarArr.length) {
                c84 r = iVarArr[i2].r();
                int i4 = r.a;
                int i5 = 0;
                while (i5 < i4) {
                    v b2 = r.b(i5);
                    v b3 = b2.b(i2 + ":" + b2.f0);
                    this.i0.put(b3, b2);
                    vVarArr[i3] = b3;
                    i5++;
                    i3++;
                }
                i2++;
            } else {
                this.k0 = new c84(vVarArr);
                ((i.a) ii.e(this.j0)).f(this);
                return;
            }
        }
    }

    @Override // androidx.media3.exoplayer.source.i, androidx.media3.exoplayer.source.s
    public long g() {
        return this.m0.g();
    }

    @Override // androidx.media3.exoplayer.source.i, androidx.media3.exoplayer.source.s
    public void h(long j) {
        this.m0.h(j);
    }

    @Override // androidx.media3.exoplayer.source.s.a
    /* renamed from: j */
    public void i(i iVar) {
        ((i.a) ii.e(this.j0)).i(this);
    }

    @Override // androidx.media3.exoplayer.source.i
    public void k() throws IOException {
        for (i iVar : this.a) {
            iVar.k();
        }
    }

    @Override // androidx.media3.exoplayer.source.i
    public long l(long j) {
        long l = this.l0[0].l(j);
        int i = 1;
        while (true) {
            i[] iVarArr = this.l0;
            if (i >= iVarArr.length) {
                return l;
            }
            if (iVarArr[i].l(l) != l) {
                throw new IllegalStateException("Unexpected child seekToUs result.");
            }
            i++;
        }
    }

    @Override // androidx.media3.exoplayer.source.i
    public long n(androidx.media3.exoplayer.trackselection.c[] cVarArr, boolean[] zArr, r[] rVarArr, boolean[] zArr2, long j) {
        androidx.media3.exoplayer.trackselection.c cVar;
        int[] iArr = new int[cVarArr.length];
        int[] iArr2 = new int[cVarArr.length];
        int i = 0;
        while (true) {
            cVar = null;
            if (i >= cVarArr.length) {
                break;
            }
            Integer num = rVarArr[i] != null ? this.f0.get(rVarArr[i]) : null;
            iArr[i] = num == null ? -1 : num.intValue();
            iArr2[i] = -1;
            if (cVarArr[i] != null) {
                v vVar = (v) ii.e(this.i0.get(cVarArr[i].c()));
                int i2 = 0;
                while (true) {
                    i[] iVarArr = this.a;
                    if (i2 >= iVarArr.length) {
                        break;
                    } else if (iVarArr[i2].r().c(vVar) != -1) {
                        iArr2[i] = i2;
                        break;
                    } else {
                        i2++;
                    }
                }
            }
            i++;
        }
        this.f0.clear();
        int length = cVarArr.length;
        r[] rVarArr2 = new r[length];
        r[] rVarArr3 = new r[cVarArr.length];
        androidx.media3.exoplayer.trackselection.c[] cVarArr2 = new androidx.media3.exoplayer.trackselection.c[cVarArr.length];
        ArrayList arrayList = new ArrayList(this.a.length);
        long j2 = j;
        int i3 = 0;
        while (i3 < this.a.length) {
            for (int i4 = 0; i4 < cVarArr.length; i4++) {
                rVarArr3[i4] = iArr[i4] == i3 ? rVarArr[i4] : cVar;
                if (iArr2[i4] == i3) {
                    androidx.media3.exoplayer.trackselection.c cVar2 = (androidx.media3.exoplayer.trackselection.c) ii.e(cVarArr[i4]);
                    cVarArr2[i4] = new a(cVar2, (v) ii.e(this.i0.get(cVar2.c())));
                } else {
                    cVarArr2[i4] = cVar;
                }
            }
            int i5 = i3;
            ArrayList arrayList2 = arrayList;
            androidx.media3.exoplayer.trackselection.c[] cVarArr3 = cVarArr2;
            long n = this.a[i3].n(cVarArr2, zArr, rVarArr3, zArr2, j2);
            if (i5 == 0) {
                j2 = n;
            } else if (n != j2) {
                throw new IllegalStateException("Children enabled at different positions.");
            }
            boolean z = false;
            for (int i6 = 0; i6 < cVarArr.length; i6++) {
                if (iArr2[i6] == i5) {
                    rVarArr2[i6] = rVarArr3[i6];
                    this.f0.put((r) ii.e(rVarArr3[i6]), Integer.valueOf(i5));
                    z = true;
                } else if (iArr[i6] == i5) {
                    ii.g(rVarArr3[i6] == null);
                }
            }
            if (z) {
                arrayList2.add(this.a[i5]);
            }
            i3 = i5 + 1;
            arrayList = arrayList2;
            cVarArr2 = cVarArr3;
            cVar = null;
        }
        System.arraycopy(rVarArr2, 0, rVarArr, 0, length);
        i[] iVarArr2 = (i[]) arrayList.toArray(new i[0]);
        this.l0 = iVarArr2;
        this.m0 = this.g0.a(iVarArr2);
        return j2;
    }

    @Override // androidx.media3.exoplayer.source.i
    public long o() {
        i[] iVarArr;
        i[] iVarArr2;
        long j = -9223372036854775807L;
        for (i iVar : this.l0) {
            long o = iVar.o();
            if (o != CellBase.ID_SYSTEM_MESSAGE_REQUEST_CLOSED) {
                if (j == CellBase.ID_SYSTEM_MESSAGE_REQUEST_CLOSED) {
                    for (i iVar2 : this.l0) {
                        if (iVar2 == iVar) {
                            break;
                        } else if (iVar2.l(o) != o) {
                            throw new IllegalStateException("Unexpected child seekToUs result.");
                        }
                    }
                    j = o;
                } else if (o != j) {
                    throw new IllegalStateException("Conflicting discontinuities.");
                }
            } else if (j != CellBase.ID_SYSTEM_MESSAGE_REQUEST_CLOSED && iVar.l(j) != j) {
                throw new IllegalStateException("Unexpected child seekToUs result.");
            }
        }
        return j;
    }

    @Override // androidx.media3.exoplayer.source.i
    public void q(i.a aVar, long j) {
        this.j0 = aVar;
        Collections.addAll(this.h0, this.a);
        for (i iVar : this.a) {
            iVar.q(this, j);
        }
    }

    @Override // androidx.media3.exoplayer.source.i
    public c84 r() {
        return (c84) ii.e(this.k0);
    }

    @Override // androidx.media3.exoplayer.source.i
    public void t(long j, boolean z) {
        for (i iVar : this.l0) {
            iVar.t(j, z);
        }
    }
}
