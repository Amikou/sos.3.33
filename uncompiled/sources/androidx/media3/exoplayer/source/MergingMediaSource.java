package androidx.media3.exoplayer.source;

import androidx.media3.common.m;
import androidx.media3.common.u;
import androidx.media3.exoplayer.source.j;
import com.google.common.collect.MultimapBuilder;
import java.io.IOException;
import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import zendesk.support.request.CellBase;

/* loaded from: classes.dex */
public final class MergingMediaSource extends d<Integer> {
    public static final androidx.media3.common.m v = new m.c().d("MergingMediaSource").a();
    public final boolean k;
    public final boolean l;
    public final j[] m;
    public final androidx.media3.common.u[] n;
    public final ArrayList<j> o;
    public final q40 p;
    public final Map<Object, Long> q;
    public final wa2<Object, c> r;
    public int s;
    public long[][] t;
    public IllegalMergeException u;

    /* loaded from: classes.dex */
    public static final class IllegalMergeException extends IOException {
        public static final int REASON_PERIOD_COUNT_MISMATCH = 0;
        public final int reason;

        public IllegalMergeException(int i) {
            this.reason = i;
        }
    }

    /* loaded from: classes.dex */
    public static final class a extends l91 {
        public final long[] g0;
        public final long[] h0;

        public a(androidx.media3.common.u uVar, Map<Object, Long> map) {
            super(uVar);
            int p = uVar.p();
            this.h0 = new long[uVar.p()];
            u.c cVar = new u.c();
            for (int i = 0; i < p; i++) {
                this.h0[i] = uVar.n(i, cVar).r0;
            }
            int i2 = uVar.i();
            this.g0 = new long[i2];
            u.b bVar = new u.b();
            for (int i3 = 0; i3 < i2; i3++) {
                uVar.g(i3, bVar, true);
                long longValue = ((Long) ii.e(map.get(bVar.f0))).longValue();
                long[] jArr = this.g0;
                jArr[i3] = longValue == Long.MIN_VALUE ? bVar.h0 : longValue;
                long j = bVar.h0;
                if (j != CellBase.ID_SYSTEM_MESSAGE_REQUEST_CLOSED) {
                    long[] jArr2 = this.h0;
                    int i4 = bVar.g0;
                    jArr2[i4] = jArr2[i4] - (j - jArr[i3]);
                }
            }
        }

        @Override // defpackage.l91, androidx.media3.common.u
        public u.b g(int i, u.b bVar, boolean z) {
            super.g(i, bVar, z);
            bVar.h0 = this.g0[i];
            return bVar;
        }

        @Override // defpackage.l91, androidx.media3.common.u
        public u.c o(int i, u.c cVar, long j) {
            long j2;
            super.o(i, cVar, j);
            long j3 = this.h0[i];
            cVar.r0 = j3;
            if (j3 != CellBase.ID_SYSTEM_MESSAGE_REQUEST_CLOSED) {
                long j4 = cVar.q0;
                if (j4 != CellBase.ID_SYSTEM_MESSAGE_REQUEST_CLOSED) {
                    j2 = Math.min(j4, j3);
                    cVar.q0 = j2;
                    return cVar;
                }
            }
            j2 = cVar.q0;
            cVar.q0 = j2;
            return cVar;
        }
    }

    public MergingMediaSource(j... jVarArr) {
        this(false, jVarArr);
    }

    @Override // androidx.media3.exoplayer.source.d, androidx.media3.exoplayer.source.a
    public void A() {
        super.A();
        Arrays.fill(this.n, (Object) null);
        this.s = -1;
        this.u = null;
        this.o.clear();
        Collections.addAll(this.o, this.m);
    }

    public final void I() {
        u.b bVar = new u.b();
        for (int i = 0; i < this.s; i++) {
            long j = -this.n[0].f(i, bVar).p();
            int i2 = 1;
            while (true) {
                androidx.media3.common.u[] uVarArr = this.n;
                if (i2 < uVarArr.length) {
                    this.t[i][i2] = j - (-uVarArr[i2].f(i, bVar).p());
                    i2++;
                }
            }
        }
    }

    @Override // androidx.media3.exoplayer.source.d
    /* renamed from: J */
    public j.b C(Integer num, j.b bVar) {
        if (num.intValue() == 0) {
            return bVar;
        }
        return null;
    }

    @Override // androidx.media3.exoplayer.source.d
    /* renamed from: K */
    public void G(Integer num, j jVar, androidx.media3.common.u uVar) {
        if (this.u != null) {
            return;
        }
        if (this.s == -1) {
            this.s = uVar.i();
        } else if (uVar.i() != this.s) {
            this.u = new IllegalMergeException(0);
            return;
        }
        if (this.t.length == 0) {
            this.t = (long[][]) Array.newInstance(long.class, this.s, this.n.length);
        }
        this.o.remove(jVar);
        this.n[num.intValue()] = uVar;
        if (this.o.isEmpty()) {
            if (this.k) {
                I();
            }
            a aVar = this.n[0];
            if (this.l) {
                L();
                aVar = new a(aVar, this.q);
            }
            z(aVar);
        }
    }

    public final void L() {
        androidx.media3.common.u[] uVarArr;
        u.b bVar = new u.b();
        for (int i = 0; i < this.s; i++) {
            int i2 = 0;
            long j = Long.MIN_VALUE;
            while (true) {
                uVarArr = this.n;
                if (i2 >= uVarArr.length) {
                    break;
                }
                long l = uVarArr[i2].f(i, bVar).l();
                if (l != CellBase.ID_SYSTEM_MESSAGE_REQUEST_CLOSED) {
                    long j2 = l + this.t[i][i2];
                    if (j == Long.MIN_VALUE || j2 < j) {
                        j = j2;
                    }
                }
                i2++;
            }
            Object m = uVarArr[0].m(i);
            this.q.put(m, Long.valueOf(j));
            for (c cVar : this.r.get(m)) {
                cVar.u(0L, j);
            }
        }
    }

    @Override // androidx.media3.exoplayer.source.j
    public i e(j.b bVar, gb gbVar, long j) {
        int length = this.m.length;
        i[] iVarArr = new i[length];
        int b = this.n[0].b(bVar.a);
        for (int i = 0; i < length; i++) {
            iVarArr[i] = this.m[i].e(bVar.c(this.n[i].m(b)), gbVar, j - this.t[b][i]);
        }
        l lVar = new l(this.p, this.t[b], iVarArr);
        if (this.l) {
            c cVar = new c(lVar, true, 0L, ((Long) ii.e(this.q.get(bVar.a))).longValue());
            this.r.put(bVar.a, cVar);
            return cVar;
        }
        return lVar;
    }

    @Override // androidx.media3.exoplayer.source.j
    public androidx.media3.common.m i() {
        j[] jVarArr = this.m;
        return jVarArr.length > 0 ? jVarArr[0].i() : v;
    }

    @Override // androidx.media3.exoplayer.source.d, androidx.media3.exoplayer.source.j
    public void j() throws IOException {
        IllegalMergeException illegalMergeException = this.u;
        if (illegalMergeException == null) {
            super.j();
            return;
        }
        throw illegalMergeException;
    }

    @Override // androidx.media3.exoplayer.source.j
    public void o(i iVar) {
        if (this.l) {
            c cVar = (c) iVar;
            Iterator<Map.Entry<Object, c>> it = this.r.entries().iterator();
            while (true) {
                if (!it.hasNext()) {
                    break;
                }
                Map.Entry<Object, c> next = it.next();
                if (next.getValue().equals(cVar)) {
                    this.r.remove(next.getKey(), next.getValue());
                    break;
                }
            }
            iVar = cVar.a;
        }
        l lVar = (l) iVar;
        int i = 0;
        while (true) {
            j[] jVarArr = this.m;
            if (i >= jVarArr.length) {
                return;
            }
            jVarArr[i].o(lVar.b(i));
            i++;
        }
    }

    @Override // androidx.media3.exoplayer.source.d, androidx.media3.exoplayer.source.a
    public void y(fa4 fa4Var) {
        super.y(fa4Var);
        for (int i = 0; i < this.m.length; i++) {
            H(Integer.valueOf(i), this.m[i]);
        }
    }

    public MergingMediaSource(boolean z, j... jVarArr) {
        this(z, false, jVarArr);
    }

    public MergingMediaSource(boolean z, boolean z2, j... jVarArr) {
        this(z, z2, new ni0(), jVarArr);
    }

    public MergingMediaSource(boolean z, boolean z2, q40 q40Var, j... jVarArr) {
        this.k = z;
        this.l = z2;
        this.m = jVarArr;
        this.p = q40Var;
        this.o = new ArrayList<>(Arrays.asList(jVarArr));
        this.s = -1;
        this.n = new androidx.media3.common.u[jVarArr.length];
        this.t = new long[0];
        this.q = new HashMap();
        this.r = MultimapBuilder.a().a().e();
    }
}
