package androidx.media3.exoplayer.source;

import androidx.media3.exoplayer.source.i;
import androidx.media3.exoplayer.source.j;
import java.io.IOException;
import zendesk.support.request.CellBase;

/* compiled from: MaskingMediaPeriod.java */
/* loaded from: classes.dex */
public final class g implements i, i.a {
    public final j.b a;
    public final long f0;
    public final gb g0;
    public j h0;
    public i i0;
    public i.a j0;
    public a k0;
    public boolean l0;
    public long m0 = CellBase.ID_SYSTEM_MESSAGE_REQUEST_CLOSED;

    /* compiled from: MaskingMediaPeriod.java */
    /* loaded from: classes.dex */
    public interface a {
        void a(j.b bVar, IOException iOException);

        void b(j.b bVar);
    }

    public g(j.b bVar, gb gbVar, long j) {
        this.a = bVar;
        this.g0 = gbVar;
        this.f0 = j;
    }

    @Override // androidx.media3.exoplayer.source.i, androidx.media3.exoplayer.source.s
    public long a() {
        return ((i) androidx.media3.common.util.b.j(this.i0)).a();
    }

    public void b(j.b bVar) {
        long p = p(this.f0);
        i e = ((j) ii.e(this.h0)).e(bVar, this.g0, p);
        this.i0 = e;
        if (this.j0 != null) {
            e.q(this, p);
        }
    }

    @Override // androidx.media3.exoplayer.source.i
    public long c(long j, xi3 xi3Var) {
        return ((i) androidx.media3.common.util.b.j(this.i0)).c(j, xi3Var);
    }

    @Override // androidx.media3.exoplayer.source.i, androidx.media3.exoplayer.source.s
    public boolean d(long j) {
        i iVar = this.i0;
        return iVar != null && iVar.d(j);
    }

    @Override // androidx.media3.exoplayer.source.i, androidx.media3.exoplayer.source.s
    public boolean e() {
        i iVar = this.i0;
        return iVar != null && iVar.e();
    }

    @Override // androidx.media3.exoplayer.source.i.a
    public void f(i iVar) {
        ((i.a) androidx.media3.common.util.b.j(this.j0)).f(this);
        a aVar = this.k0;
        if (aVar != null) {
            aVar.b(this.a);
        }
    }

    @Override // androidx.media3.exoplayer.source.i, androidx.media3.exoplayer.source.s
    public long g() {
        return ((i) androidx.media3.common.util.b.j(this.i0)).g();
    }

    @Override // androidx.media3.exoplayer.source.i, androidx.media3.exoplayer.source.s
    public void h(long j) {
        ((i) androidx.media3.common.util.b.j(this.i0)).h(j);
    }

    public long j() {
        return this.m0;
    }

    @Override // androidx.media3.exoplayer.source.i
    public void k() throws IOException {
        try {
            i iVar = this.i0;
            if (iVar != null) {
                iVar.k();
            } else {
                j jVar = this.h0;
                if (jVar != null) {
                    jVar.j();
                }
            }
        } catch (IOException e) {
            a aVar = this.k0;
            if (aVar != null) {
                if (this.l0) {
                    return;
                }
                this.l0 = true;
                aVar.a(this.a, e);
                return;
            }
            throw e;
        }
    }

    @Override // androidx.media3.exoplayer.source.i
    public long l(long j) {
        return ((i) androidx.media3.common.util.b.j(this.i0)).l(j);
    }

    public long m() {
        return this.f0;
    }

    @Override // androidx.media3.exoplayer.source.i
    public long n(androidx.media3.exoplayer.trackselection.c[] cVarArr, boolean[] zArr, r[] rVarArr, boolean[] zArr2, long j) {
        long j2;
        long j3 = this.m0;
        if (j3 == CellBase.ID_SYSTEM_MESSAGE_REQUEST_CLOSED || j != this.f0) {
            j2 = j;
        } else {
            this.m0 = CellBase.ID_SYSTEM_MESSAGE_REQUEST_CLOSED;
            j2 = j3;
        }
        return ((i) androidx.media3.common.util.b.j(this.i0)).n(cVarArr, zArr, rVarArr, zArr2, j2);
    }

    @Override // androidx.media3.exoplayer.source.i
    public long o() {
        return ((i) androidx.media3.common.util.b.j(this.i0)).o();
    }

    public final long p(long j) {
        long j2 = this.m0;
        return j2 != CellBase.ID_SYSTEM_MESSAGE_REQUEST_CLOSED ? j2 : j;
    }

    @Override // androidx.media3.exoplayer.source.i
    public void q(i.a aVar, long j) {
        this.j0 = aVar;
        i iVar = this.i0;
        if (iVar != null) {
            iVar.q(this, p(this.f0));
        }
    }

    @Override // androidx.media3.exoplayer.source.i
    public c84 r() {
        return ((i) androidx.media3.common.util.b.j(this.i0)).r();
    }

    @Override // androidx.media3.exoplayer.source.s.a
    /* renamed from: s */
    public void i(i iVar) {
        ((i.a) androidx.media3.common.util.b.j(this.j0)).i(this);
    }

    @Override // androidx.media3.exoplayer.source.i
    public void t(long j, boolean z) {
        ((i) androidx.media3.common.util.b.j(this.i0)).t(j, z);
    }

    public void u(long j) {
        this.m0 = j;
    }

    public void v() {
        if (this.i0 != null) {
            ((j) ii.e(this.h0)).o(this.i0);
        }
    }

    public void w(j jVar) {
        ii.g(this.h0 == null);
        this.h0 = jVar;
    }
}
