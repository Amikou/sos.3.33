package androidx.media3.exoplayer.source;

import android.content.Context;
import android.net.Uri;
import androidx.media3.common.j;
import androidx.media3.common.m;
import androidx.media3.datasource.b;
import androidx.media3.datasource.d;
import androidx.media3.exoplayer.source.e;
import androidx.media3.exoplayer.source.j;
import androidx.media3.exoplayer.source.o;
import androidx.media3.exoplayer.source.u;
import com.google.common.collect.ImmutableList;
import defpackage.wi3;
import java.io.IOException;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import zendesk.support.request.CellBase;

/* compiled from: DefaultMediaSourceFactory.java */
/* loaded from: classes.dex */
public final class e implements j.a {
    public final a a;
    public b.a b;
    public j.a c;
    public androidx.media3.exoplayer.upstream.b d;
    public long e;
    public long f;
    public long g;
    public float h;
    public float i;
    public boolean j;

    /* compiled from: DefaultMediaSourceFactory.java */
    /* loaded from: classes.dex */
    public static final class a {
        public final u11 a;
        public final Map<Integer, dw3<j.a>> b = new HashMap();
        public final Set<Integer> c = new HashSet();
        public final Map<Integer, j.a> d = new HashMap();
        public b.a e;
        public zr0 f;
        public androidx.media3.exoplayer.upstream.b g;

        public a(u11 u11Var) {
            this.a = u11Var;
        }

        /* JADX INFO: Access modifiers changed from: private */
        public /* synthetic */ j.a k(b.a aVar) {
            return new o.b(aVar, this.a);
        }

        public j.a f(int i) {
            j.a aVar = this.d.get(Integer.valueOf(i));
            if (aVar != null) {
                return aVar;
            }
            dw3<j.a> l = l(i);
            if (l == null) {
                return null;
            }
            j.a aVar2 = l.get();
            zr0 zr0Var = this.f;
            if (zr0Var != null) {
                aVar2.c(zr0Var);
            }
            androidx.media3.exoplayer.upstream.b bVar = this.g;
            if (bVar != null) {
                aVar2.b(bVar);
            }
            this.d.put(Integer.valueOf(i), aVar2);
            return aVar2;
        }

        /* JADX WARN: Removed duplicated region for block: B:25:0x0082  */
        /*
            Code decompiled incorrectly, please refer to instructions dump.
            To view partially-correct code enable 'Show inconsistent code' option in preferences
        */
        public final defpackage.dw3<androidx.media3.exoplayer.source.j.a> l(int r5) {
            /*
                r4 = this;
                java.lang.Class<androidx.media3.exoplayer.source.j$a> r0 = androidx.media3.exoplayer.source.j.a.class
                java.util.Map<java.lang.Integer, dw3<androidx.media3.exoplayer.source.j$a>> r1 = r4.b
                java.lang.Integer r2 = java.lang.Integer.valueOf(r5)
                boolean r1 = r1.containsKey(r2)
                if (r1 == 0) goto L1b
                java.util.Map<java.lang.Integer, dw3<androidx.media3.exoplayer.source.j$a>> r0 = r4.b
                java.lang.Integer r5 = java.lang.Integer.valueOf(r5)
                java.lang.Object r5 = r0.get(r5)
                dw3 r5 = (defpackage.dw3) r5
                return r5
            L1b:
                r1 = 0
                androidx.media3.datasource.b$a r2 = r4.e
                java.lang.Object r2 = defpackage.ii.e(r2)
                androidx.media3.datasource.b$a r2 = (androidx.media3.datasource.b.a) r2
                if (r5 == 0) goto L6b
                r3 = 1
                if (r5 == r3) goto L5b
                r3 = 2
                if (r5 == r3) goto L4b
                r3 = 3
                if (r5 == r3) goto L3a
                r0 = 4
                if (r5 == r0) goto L33
                goto L77
            L33:
                ak0 r0 = new ak0     // Catch: java.lang.ClassNotFoundException -> L77
                r0.<init>()     // Catch: java.lang.ClassNotFoundException -> L77
                r1 = r0
                goto L77
            L3a:
                java.lang.String r2 = "androidx.media3.exoplayer.rtsp.RtspMediaSource$Factory"
                java.lang.Class r2 = java.lang.Class.forName(r2)     // Catch: java.lang.ClassNotFoundException -> L77
                java.lang.Class r0 = r2.asSubclass(r0)     // Catch: java.lang.ClassNotFoundException -> L77
                bk0 r2 = new bk0     // Catch: java.lang.ClassNotFoundException -> L77
                r2.<init>()     // Catch: java.lang.ClassNotFoundException -> L77
                r1 = r2
                goto L77
            L4b:
                java.lang.String r3 = "androidx.media3.exoplayer.hls.HlsMediaSource$Factory"
                java.lang.Class r3 = java.lang.Class.forName(r3)     // Catch: java.lang.ClassNotFoundException -> L77
                java.lang.Class r0 = r3.asSubclass(r0)     // Catch: java.lang.ClassNotFoundException -> L77
                dk0 r3 = new dk0     // Catch: java.lang.ClassNotFoundException -> L77
                r3.<init>()     // Catch: java.lang.ClassNotFoundException -> L77
                goto L76
            L5b:
                java.lang.String r3 = "androidx.media3.exoplayer.smoothstreaming.SsMediaSource$Factory"
                java.lang.Class r3 = java.lang.Class.forName(r3)     // Catch: java.lang.ClassNotFoundException -> L77
                java.lang.Class r0 = r3.asSubclass(r0)     // Catch: java.lang.ClassNotFoundException -> L77
                ck0 r3 = new ck0     // Catch: java.lang.ClassNotFoundException -> L77
                r3.<init>()     // Catch: java.lang.ClassNotFoundException -> L77
                goto L76
            L6b:
                java.lang.Class<androidx.media3.exoplayer.dash.DashMediaSource$Factory> r3 = androidx.media3.exoplayer.dash.DashMediaSource.Factory.class
                java.lang.Class r0 = r3.asSubclass(r0)     // Catch: java.lang.ClassNotFoundException -> L77
                ek0 r3 = new ek0     // Catch: java.lang.ClassNotFoundException -> L77
                r3.<init>()     // Catch: java.lang.ClassNotFoundException -> L77
            L76:
                r1 = r3
            L77:
                java.util.Map<java.lang.Integer, dw3<androidx.media3.exoplayer.source.j$a>> r0 = r4.b
                java.lang.Integer r2 = java.lang.Integer.valueOf(r5)
                r0.put(r2, r1)
                if (r1 == 0) goto L8b
                java.util.Set<java.lang.Integer> r0 = r4.c
                java.lang.Integer r5 = java.lang.Integer.valueOf(r5)
                r0.add(r5)
            L8b:
                return r1
            */
            throw new UnsupportedOperationException("Method not decompiled: androidx.media3.exoplayer.source.e.a.l(int):dw3");
        }

        public void m(b.a aVar) {
            if (aVar != this.e) {
                this.e = aVar;
                this.b.clear();
                this.d.clear();
            }
        }

        public void n(zr0 zr0Var) {
            this.f = zr0Var;
            for (j.a aVar : this.d.values()) {
                aVar.c(zr0Var);
            }
        }

        public void o(androidx.media3.exoplayer.upstream.b bVar) {
            this.g = bVar;
            for (j.a aVar : this.d.values()) {
                aVar.b(bVar);
            }
        }
    }

    /* compiled from: DefaultMediaSourceFactory.java */
    /* loaded from: classes.dex */
    public static final class b implements p11 {
        public final androidx.media3.common.j a;

        public b(androidx.media3.common.j jVar) {
            this.a = jVar;
        }

        @Override // defpackage.p11
        public void a() {
        }

        @Override // defpackage.p11
        public void c(long j, long j2) {
        }

        @Override // defpackage.p11
        public int f(q11 q11Var, ot2 ot2Var) throws IOException {
            return q11Var.g(Integer.MAX_VALUE) == -1 ? -1 : 0;
        }

        @Override // defpackage.p11
        public boolean g(q11 q11Var) {
            return true;
        }

        @Override // defpackage.p11
        public void j(r11 r11Var) {
            f84 f = r11Var.f(0, 3);
            r11Var.p(new wi3.b(CellBase.ID_SYSTEM_MESSAGE_REQUEST_CLOSED));
            r11Var.m();
            f.f(this.a.b().e0("text/x-unknown").I(this.a.p0).E());
        }
    }

    public e(Context context, u11 u11Var) {
        this(new d.a(context), u11Var);
    }

    public static /* synthetic */ j.a e(Class cls) {
        return j(cls);
    }

    public static /* synthetic */ j.a f(Class cls, b.a aVar) {
        return k(cls, aVar);
    }

    public static /* synthetic */ p11[] g(androidx.media3.common.j jVar) {
        p11 bVar;
        p11[] p11VarArr = new p11[1];
        sv3 sv3Var = sv3.a;
        if (sv3Var.a(jVar)) {
            bVar = new androidx.media3.extractor.text.b(sv3Var.b(jVar), jVar);
        } else {
            bVar = new b(jVar);
        }
        p11VarArr[0] = bVar;
        return p11VarArr;
    }

    public static j h(androidx.media3.common.m mVar, j jVar) {
        m.d dVar = mVar.i0;
        long j = dVar.a;
        if (j == 0 && dVar.f0 == Long.MIN_VALUE && !dVar.h0) {
            return jVar;
        }
        long y0 = androidx.media3.common.util.b.y0(j);
        long y02 = androidx.media3.common.util.b.y0(mVar.i0.f0);
        m.d dVar2 = mVar.i0;
        return new ClippingMediaSource(jVar, y0, y02, !dVar2.i0, dVar2.g0, dVar2.h0);
    }

    public static j.a j(Class<? extends j.a> cls) {
        try {
            return cls.getConstructor(new Class[0]).newInstance(new Object[0]);
        } catch (Exception e) {
            throw new IllegalStateException(e);
        }
    }

    public static j.a k(Class<? extends j.a> cls, b.a aVar) {
        try {
            return cls.getConstructor(b.a.class).newInstance(aVar);
        } catch (Exception e) {
            throw new IllegalStateException(e);
        }
    }

    @Override // androidx.media3.exoplayer.source.j.a
    public j a(androidx.media3.common.m mVar) {
        ii.e(mVar.f0);
        String scheme = mVar.f0.a.getScheme();
        if (scheme != null && scheme.equals("ssai")) {
            return ((j.a) ii.e(this.c)).a(mVar);
        }
        m.h hVar = mVar.f0;
        int m0 = androidx.media3.common.util.b.m0(hVar.a, hVar.b);
        j.a f = this.a.f(m0);
        ii.j(f, "No suitable media source factory found for content type: " + m0);
        m.g.a b2 = mVar.g0.b();
        if (mVar.g0.a == CellBase.ID_SYSTEM_MESSAGE_REQUEST_CLOSED) {
            b2.k(this.e);
        }
        if (mVar.g0.h0 == -3.4028235E38f) {
            b2.j(this.h);
        }
        if (mVar.g0.i0 == -3.4028235E38f) {
            b2.h(this.i);
        }
        if (mVar.g0.f0 == CellBase.ID_SYSTEM_MESSAGE_REQUEST_CLOSED) {
            b2.i(this.f);
        }
        if (mVar.g0.g0 == CellBase.ID_SYSTEM_MESSAGE_REQUEST_CLOSED) {
            b2.g(this.g);
        }
        m.g f2 = b2.f();
        if (!f2.equals(mVar.g0)) {
            mVar = mVar.b().c(f2).a();
        }
        j a2 = f.a(mVar);
        ImmutableList<m.l> immutableList = ((m.h) androidx.media3.common.util.b.j(mVar.f0)).g;
        if (!immutableList.isEmpty()) {
            j[] jVarArr = new j[immutableList.size() + 1];
            jVarArr[0] = a2;
            for (int i = 0; i < immutableList.size(); i++) {
                if (this.j) {
                    final androidx.media3.common.j E = new j.b().e0(immutableList.get(i).b).V(immutableList.get(i).c).g0(immutableList.get(i).d).c0(immutableList.get(i).e).U(immutableList.get(i).f).S(immutableList.get(i).g).E();
                    o.b bVar = new o.b(this.b, new u11() { // from class: zj0
                        @Override // defpackage.u11
                        public final p11[] a() {
                            p11[] g;
                            g = e.g(androidx.media3.common.j.this);
                            return g;
                        }

                        @Override // defpackage.u11
                        public /* synthetic */ p11[] b(Uri uri, Map map) {
                            return t11.a(this, uri, map);
                        }
                    });
                    androidx.media3.exoplayer.upstream.b bVar2 = this.d;
                    if (bVar2 != null) {
                        bVar.b(bVar2);
                    }
                    jVarArr[i + 1] = bVar.a(androidx.media3.common.m.e(immutableList.get(i).a.toString()));
                } else {
                    u.b bVar3 = new u.b(this.b);
                    androidx.media3.exoplayer.upstream.b bVar4 = this.d;
                    if (bVar4 != null) {
                        bVar3.b(bVar4);
                    }
                    jVarArr[i + 1] = bVar3.a(immutableList.get(i), CellBase.ID_SYSTEM_MESSAGE_REQUEST_CLOSED);
                }
            }
            a2 = new MergingMediaSource(jVarArr);
        }
        return i(mVar, h(mVar, a2));
    }

    public final j i(androidx.media3.common.m mVar, j jVar) {
        ii.e(mVar.f0);
        m.b bVar = mVar.f0.d;
        return jVar;
    }

    @Override // androidx.media3.exoplayer.source.j.a
    /* renamed from: l */
    public e c(zr0 zr0Var) {
        this.a.n((zr0) ii.f(zr0Var, "MediaSource.Factory#setDrmSessionManagerProvider no longer handles null by instantiating a new DefaultDrmSessionManagerProvider. Explicitly construct and pass an instance in order to retain the old behavior."));
        return this;
    }

    @Override // androidx.media3.exoplayer.source.j.a
    /* renamed from: m */
    public e b(androidx.media3.exoplayer.upstream.b bVar) {
        this.d = (androidx.media3.exoplayer.upstream.b) ii.f(bVar, "MediaSource.Factory#setLoadErrorHandlingPolicy no longer handles null by instantiating a new DefaultLoadErrorHandlingPolicy. Explicitly construct and pass an instance in order to retain the old behavior.");
        this.a.o(bVar);
        return this;
    }

    public e(b.a aVar, u11 u11Var) {
        this.b = aVar;
        a aVar2 = new a(u11Var);
        this.a = aVar2;
        aVar2.m(aVar);
        this.e = CellBase.ID_SYSTEM_MESSAGE_REQUEST_CLOSED;
        this.f = CellBase.ID_SYSTEM_MESSAGE_REQUEST_CLOSED;
        this.g = CellBase.ID_SYSTEM_MESSAGE_REQUEST_CLOSED;
        this.h = -3.4028235E38f;
        this.i = -3.4028235E38f;
    }
}
