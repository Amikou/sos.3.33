package androidx.media3.exoplayer.source;

import androidx.media3.common.v;
import androidx.media3.datasource.b;
import androidx.media3.decoder.DecoderInputBuffer;
import androidx.media3.exoplayer.source.i;
import androidx.media3.exoplayer.source.k;
import androidx.media3.exoplayer.upstream.Loader;
import androidx.media3.exoplayer.upstream.b;
import androidx.recyclerview.widget.RecyclerView;
import java.io.IOException;
import java.nio.ByteBuffer;
import java.util.ArrayList;
import java.util.Arrays;
import zendesk.support.request.CellBase;

/* compiled from: SingleSampleMediaPeriod.java */
/* loaded from: classes.dex */
public final class t implements i, Loader.b<c> {
    public final je0 a;
    public final b.a f0;
    public final fa4 g0;
    public final androidx.media3.exoplayer.upstream.b h0;
    public final k.a i0;
    public final c84 j0;
    public final long l0;
    public final androidx.media3.common.j n0;
    public final boolean o0;
    public boolean p0;
    public byte[] q0;
    public int r0;
    public final ArrayList<b> k0 = new ArrayList<>();
    public final Loader m0 = new Loader("SingleSampleMediaPeriod");

    /* compiled from: SingleSampleMediaPeriod.java */
    /* loaded from: classes.dex */
    public final class b implements r {
        public int a;
        public boolean f0;

        public b() {
        }

        public final void a() {
            if (this.f0) {
                return;
            }
            t.this.i0.i(y82.i(t.this.n0.p0), t.this.n0, 0, null, 0L);
            this.f0 = true;
        }

        @Override // androidx.media3.exoplayer.source.r
        public void b() throws IOException {
            t tVar = t.this;
            if (tVar.o0) {
                return;
            }
            tVar.m0.b();
        }

        public void c() {
            if (this.a == 2) {
                this.a = 1;
            }
        }

        @Override // androidx.media3.exoplayer.source.r
        public boolean f() {
            return t.this.p0;
        }

        @Override // androidx.media3.exoplayer.source.r
        public int m(long j) {
            a();
            if (j <= 0 || this.a == 2) {
                return 0;
            }
            this.a = 2;
            return 1;
        }

        @Override // androidx.media3.exoplayer.source.r
        public int p(y81 y81Var, DecoderInputBuffer decoderInputBuffer, int i) {
            a();
            t tVar = t.this;
            boolean z = tVar.p0;
            if (z && tVar.q0 == null) {
                this.a = 2;
            }
            int i2 = this.a;
            if (i2 == 2) {
                decoderInputBuffer.g(4);
                return -4;
            } else if ((i & 2) != 0 || i2 == 0) {
                y81Var.b = tVar.n0;
                this.a = 1;
                return -5;
            } else if (z) {
                ii.e(tVar.q0);
                decoderInputBuffer.g(1);
                decoderInputBuffer.i0 = 0L;
                if ((i & 4) == 0) {
                    decoderInputBuffer.v(t.this.r0);
                    ByteBuffer byteBuffer = decoderInputBuffer.g0;
                    t tVar2 = t.this;
                    byteBuffer.put(tVar2.q0, 0, tVar2.r0);
                }
                if ((i & 1) == 0) {
                    this.a = 2;
                }
                return -4;
            } else {
                return -3;
            }
        }
    }

    /* compiled from: SingleSampleMediaPeriod.java */
    /* loaded from: classes.dex */
    public static final class c implements Loader.e {
        public final long a = u02.a();
        public final je0 b;
        public final androidx.media3.datasource.f c;
        public byte[] d;

        public c(je0 je0Var, androidx.media3.datasource.b bVar) {
            this.b = je0Var;
            this.c = new androidx.media3.datasource.f(bVar);
        }

        @Override // androidx.media3.exoplayer.upstream.Loader.e
        public void a() throws IOException {
            this.c.r();
            try {
                this.c.b(this.b);
                int i = 0;
                while (i != -1) {
                    int o = (int) this.c.o();
                    byte[] bArr = this.d;
                    if (bArr == null) {
                        this.d = new byte[RecyclerView.a0.FLAG_ADAPTER_FULLUPDATE];
                    } else if (o == bArr.length) {
                        this.d = Arrays.copyOf(bArr, bArr.length * 2);
                    }
                    androidx.media3.datasource.f fVar = this.c;
                    byte[] bArr2 = this.d;
                    i = fVar.read(bArr2, o, bArr2.length - o);
                }
            } finally {
                he0.a(this.c);
            }
        }

        @Override // androidx.media3.exoplayer.upstream.Loader.e
        public void c() {
        }
    }

    public t(je0 je0Var, b.a aVar, fa4 fa4Var, androidx.media3.common.j jVar, long j, androidx.media3.exoplayer.upstream.b bVar, k.a aVar2, boolean z) {
        this.a = je0Var;
        this.f0 = aVar;
        this.g0 = fa4Var;
        this.n0 = jVar;
        this.l0 = j;
        this.h0 = bVar;
        this.i0 = aVar2;
        this.o0 = z;
        this.j0 = new c84(new v(jVar));
    }

    @Override // androidx.media3.exoplayer.source.i, androidx.media3.exoplayer.source.s
    public long a() {
        return (this.p0 || this.m0.j()) ? Long.MIN_VALUE : 0L;
    }

    @Override // androidx.media3.exoplayer.source.i
    public long c(long j, xi3 xi3Var) {
        return j;
    }

    @Override // androidx.media3.exoplayer.source.i, androidx.media3.exoplayer.source.s
    public boolean d(long j) {
        if (this.p0 || this.m0.j() || this.m0.i()) {
            return false;
        }
        androidx.media3.datasource.b a2 = this.f0.a();
        fa4 fa4Var = this.g0;
        if (fa4Var != null) {
            a2.c(fa4Var);
        }
        c cVar = new c(this.a, a2);
        this.i0.A(new u02(cVar.a, this.a, this.m0.n(cVar, this, this.h0.c(1))), 1, -1, this.n0, 0, null, 0L, this.l0);
        return true;
    }

    @Override // androidx.media3.exoplayer.source.i, androidx.media3.exoplayer.source.s
    public boolean e() {
        return this.m0.j();
    }

    @Override // androidx.media3.exoplayer.upstream.Loader.b
    /* renamed from: f */
    public void u(c cVar, long j, long j2, boolean z) {
        androidx.media3.datasource.f fVar = cVar.c;
        u02 u02Var = new u02(cVar.a, cVar.b, fVar.p(), fVar.q(), j, j2, fVar.o());
        this.h0.b(cVar.a);
        this.i0.r(u02Var, 1, -1, null, 0, null, 0L, this.l0);
    }

    @Override // androidx.media3.exoplayer.source.i, androidx.media3.exoplayer.source.s
    public long g() {
        return this.p0 ? Long.MIN_VALUE : 0L;
    }

    @Override // androidx.media3.exoplayer.source.i, androidx.media3.exoplayer.source.s
    public void h(long j) {
    }

    @Override // androidx.media3.exoplayer.upstream.Loader.b
    /* renamed from: i */
    public void s(c cVar, long j, long j2) {
        this.r0 = (int) cVar.c.o();
        this.q0 = (byte[]) ii.e(cVar.d);
        this.p0 = true;
        androidx.media3.datasource.f fVar = cVar.c;
        u02 u02Var = new u02(cVar.a, cVar.b, fVar.p(), fVar.q(), j, j2, this.r0);
        this.h0.b(cVar.a);
        this.i0.u(u02Var, 1, -1, this.n0, 0, null, 0L, this.l0);
    }

    @Override // androidx.media3.exoplayer.source.i
    public void k() {
    }

    @Override // androidx.media3.exoplayer.source.i
    public long l(long j) {
        for (int i = 0; i < this.k0.size(); i++) {
            this.k0.get(i).c();
        }
        return j;
    }

    @Override // androidx.media3.exoplayer.upstream.Loader.b
    /* renamed from: m */
    public Loader.c j(c cVar, long j, long j2, IOException iOException, int i) {
        Loader.c cVar2;
        androidx.media3.datasource.f fVar = cVar.c;
        u02 u02Var = new u02(cVar.a, cVar.b, fVar.p(), fVar.q(), j, j2, fVar.o());
        long a2 = this.h0.a(new b.c(u02Var, new g62(1, -1, this.n0, 0, null, 0L, androidx.media3.common.util.b.U0(this.l0)), iOException, i));
        int i2 = (a2 > CellBase.ID_SYSTEM_MESSAGE_REQUEST_CLOSED ? 1 : (a2 == CellBase.ID_SYSTEM_MESSAGE_REQUEST_CLOSED ? 0 : -1));
        boolean z = i2 == 0 || i >= this.h0.c(1);
        if (this.o0 && z) {
            p12.j("SingleSampleMediaPeriod", "Loading failed, treating as end-of-stream.", iOException);
            this.p0 = true;
            cVar2 = Loader.d;
        } else if (i2 != 0) {
            cVar2 = Loader.h(false, a2);
        } else {
            cVar2 = Loader.e;
        }
        Loader.c cVar3 = cVar2;
        boolean z2 = !cVar3.c();
        this.i0.w(u02Var, 1, -1, this.n0, 0, null, 0L, this.l0, iOException, z2);
        if (z2) {
            this.h0.b(cVar.a);
        }
        return cVar3;
    }

    @Override // androidx.media3.exoplayer.source.i
    public long n(androidx.media3.exoplayer.trackselection.c[] cVarArr, boolean[] zArr, r[] rVarArr, boolean[] zArr2, long j) {
        for (int i = 0; i < cVarArr.length; i++) {
            if (rVarArr[i] != null && (cVarArr[i] == null || !zArr[i])) {
                this.k0.remove(rVarArr[i]);
                rVarArr[i] = null;
            }
            if (rVarArr[i] == null && cVarArr[i] != null) {
                b bVar = new b();
                this.k0.add(bVar);
                rVarArr[i] = bVar;
                zArr2[i] = true;
            }
        }
        return j;
    }

    @Override // androidx.media3.exoplayer.source.i
    public long o() {
        return CellBase.ID_SYSTEM_MESSAGE_REQUEST_CLOSED;
    }

    public void p() {
        this.m0.l();
    }

    @Override // androidx.media3.exoplayer.source.i
    public void q(i.a aVar, long j) {
        aVar.f(this);
    }

    @Override // androidx.media3.exoplayer.source.i
    public c84 r() {
        return this.j0;
    }

    @Override // androidx.media3.exoplayer.source.i
    public void t(long j, boolean z) {
    }
}
