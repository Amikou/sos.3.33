package androidx.media3.exoplayer.source;

import android.os.Looper;
import androidx.media3.common.m;
import androidx.media3.common.u;
import androidx.media3.datasource.b;
import androidx.media3.exoplayer.source.j;
import androidx.media3.exoplayer.source.m;
import androidx.media3.exoplayer.source.n;
import androidx.media3.exoplayer.source.o;
import zendesk.support.request.CellBase;

/* compiled from: ProgressiveMediaSource.java */
/* loaded from: classes.dex */
public final class o extends androidx.media3.exoplayer.source.a implements n.b {
    public final androidx.media3.common.m h;
    public final m.h i;
    public final b.a j;
    public final m.a k;
    public final androidx.media3.exoplayer.drm.c l;
    public final androidx.media3.exoplayer.upstream.b m;
    public final int n;
    public boolean o;
    public long p;
    public boolean q;
    public boolean r;
    public fa4 s;

    /* compiled from: ProgressiveMediaSource.java */
    /* loaded from: classes.dex */
    public class a extends l91 {
        public a(o oVar, androidx.media3.common.u uVar) {
            super(uVar);
        }

        @Override // defpackage.l91, androidx.media3.common.u
        public u.b g(int i, u.b bVar, boolean z) {
            super.g(i, bVar, z);
            bVar.j0 = true;
            return bVar;
        }

        @Override // defpackage.l91, androidx.media3.common.u
        public u.c o(int i, u.c cVar, long j) {
            super.o(i, cVar, j);
            cVar.p0 = true;
            return cVar;
        }
    }

    /* compiled from: ProgressiveMediaSource.java */
    /* loaded from: classes.dex */
    public static final class b implements j.a {
        public final b.a a;
        public m.a b;
        public zr0 c;
        public androidx.media3.exoplayer.upstream.b d;
        public int e;
        public String f;
        public Object g;

        public b(b.a aVar, final u11 u11Var) {
            this(aVar, new m.a() { // from class: wv2
                @Override // androidx.media3.exoplayer.source.m.a
                public final androidx.media3.exoplayer.source.m a(ks2 ks2Var) {
                    androidx.media3.exoplayer.source.m f;
                    f = o.b.f(u11.this, ks2Var);
                    return f;
                }
            });
        }

        public static /* synthetic */ m f(u11 u11Var, ks2 ks2Var) {
            return new androidx.media3.exoplayer.source.b(u11Var);
        }

        @Override // androidx.media3.exoplayer.source.j.a
        /* renamed from: e */
        public o a(androidx.media3.common.m mVar) {
            ii.e(mVar.f0);
            m.h hVar = mVar.f0;
            boolean z = true;
            boolean z2 = hVar.h == null && this.g != null;
            if (hVar.f != null || this.f == null) {
                z = false;
            }
            if (z2 && z) {
                mVar = mVar.b().f(this.g).b(this.f).a();
            } else if (z2) {
                mVar = mVar.b().f(this.g).a();
            } else if (z) {
                mVar = mVar.b().b(this.f).a();
            }
            androidx.media3.common.m mVar2 = mVar;
            return new o(mVar2, this.a, this.b, this.c.a(mVar2), this.d, this.e, null);
        }

        @Override // androidx.media3.exoplayer.source.j.a
        /* renamed from: g */
        public b c(zr0 zr0Var) {
            this.c = (zr0) ii.f(zr0Var, "MediaSource.Factory#setDrmSessionManagerProvider no longer handles null by instantiating a new DefaultDrmSessionManagerProvider. Explicitly construct and pass an instance in order to retain the old behavior.");
            return this;
        }

        @Override // androidx.media3.exoplayer.source.j.a
        /* renamed from: h */
        public b b(androidx.media3.exoplayer.upstream.b bVar) {
            this.d = (androidx.media3.exoplayer.upstream.b) ii.f(bVar, "MediaSource.Factory#setLoadErrorHandlingPolicy no longer handles null by instantiating a new DefaultLoadErrorHandlingPolicy. Explicitly construct and pass an instance in order to retain the old behavior.");
            return this;
        }

        public b(b.a aVar, m.a aVar2) {
            this(aVar, aVar2, new androidx.media3.exoplayer.drm.a(), new androidx.media3.exoplayer.upstream.a(), 1048576);
        }

        public b(b.a aVar, m.a aVar2, zr0 zr0Var, androidx.media3.exoplayer.upstream.b bVar, int i) {
            this.a = aVar;
            this.b = aVar2;
            this.c = zr0Var;
            this.d = bVar;
            this.e = i;
        }
    }

    public /* synthetic */ o(androidx.media3.common.m mVar, b.a aVar, m.a aVar2, androidx.media3.exoplayer.drm.c cVar, androidx.media3.exoplayer.upstream.b bVar, int i, a aVar3) {
        this(mVar, aVar, aVar2, cVar, bVar, i);
    }

    @Override // androidx.media3.exoplayer.source.a
    public void A() {
        this.l.a();
    }

    public final void B() {
        androidx.media3.common.u vp3Var = new vp3(this.p, this.q, false, this.r, null, this.h);
        if (this.o) {
            vp3Var = new a(this, vp3Var);
        }
        z(vp3Var);
    }

    @Override // androidx.media3.exoplayer.source.j
    public i e(j.b bVar, gb gbVar, long j) {
        androidx.media3.datasource.b a2 = this.j.a();
        fa4 fa4Var = this.s;
        if (fa4Var != null) {
            a2.c(fa4Var);
        }
        return new n(this.i.a, a2, this.k.a(w()), this.l, q(bVar), this.m, s(bVar), this, gbVar, this.i.f, this.n);
    }

    @Override // androidx.media3.exoplayer.source.n.b
    public void h(long j, boolean z, boolean z2) {
        if (j == CellBase.ID_SYSTEM_MESSAGE_REQUEST_CLOSED) {
            j = this.p;
        }
        if (!this.o && this.p == j && this.q == z && this.r == z2) {
            return;
        }
        this.p = j;
        this.q = z;
        this.r = z2;
        this.o = false;
        B();
    }

    @Override // androidx.media3.exoplayer.source.j
    public androidx.media3.common.m i() {
        return this.h;
    }

    @Override // androidx.media3.exoplayer.source.j
    public void j() {
    }

    @Override // androidx.media3.exoplayer.source.j
    public void o(i iVar) {
        ((n) iVar).f0();
    }

    @Override // androidx.media3.exoplayer.source.a
    public void y(fa4 fa4Var) {
        this.s = fa4Var;
        this.l.d();
        this.l.b((Looper) ii.e(Looper.myLooper()), w());
        B();
    }

    public o(androidx.media3.common.m mVar, b.a aVar, m.a aVar2, androidx.media3.exoplayer.drm.c cVar, androidx.media3.exoplayer.upstream.b bVar, int i) {
        this.i = (m.h) ii.e(mVar.f0);
        this.h = mVar;
        this.j = aVar;
        this.k = aVar2;
        this.l = cVar;
        this.m = bVar;
        this.n = i;
        this.o = true;
        this.p = CellBase.ID_SYSTEM_MESSAGE_REQUEST_CLOSED;
    }
}
