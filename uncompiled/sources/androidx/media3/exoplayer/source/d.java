package androidx.media3.exoplayer.source;

import android.os.Handler;
import androidx.media3.common.u;
import androidx.media3.exoplayer.drm.b;
import androidx.media3.exoplayer.source.d;
import androidx.media3.exoplayer.source.j;
import androidx.media3.exoplayer.source.k;
import java.io.IOException;
import java.util.HashMap;

/* compiled from: CompositeMediaSource.java */
/* loaded from: classes.dex */
public abstract class d<T> extends androidx.media3.exoplayer.source.a {
    public final HashMap<T, b<T>> h = new HashMap<>();
    public Handler i;
    public fa4 j;

    /* compiled from: CompositeMediaSource.java */
    /* loaded from: classes.dex */
    public final class a implements k, androidx.media3.exoplayer.drm.b {
        public final T a;
        public k.a f0;
        public b.a g0;

        public a(T t) {
            this.f0 = d.this.s(null);
            this.g0 = d.this.q(null);
            this.a = t;
        }

        @Override // androidx.media3.exoplayer.drm.b
        public void K(int i, j.b bVar) {
            if (b(i, bVar)) {
                this.g0.j();
            }
        }

        @Override // androidx.media3.exoplayer.source.k
        public void O(int i, j.b bVar, u02 u02Var, g62 g62Var) {
            if (b(i, bVar)) {
                this.f0.s(u02Var, i(g62Var));
            }
        }

        @Override // androidx.media3.exoplayer.source.k
        public void S(int i, j.b bVar, g62 g62Var) {
            if (b(i, bVar)) {
                this.f0.j(i(g62Var));
            }
        }

        @Override // androidx.media3.exoplayer.drm.b
        public void U(int i, j.b bVar) {
            if (b(i, bVar)) {
                this.g0.h();
            }
        }

        @Override // androidx.media3.exoplayer.source.k
        public void W(int i, j.b bVar, g62 g62Var) {
            if (b(i, bVar)) {
                this.f0.E(i(g62Var));
            }
        }

        public final boolean b(int i, j.b bVar) {
            j.b bVar2;
            if (bVar != null) {
                bVar2 = d.this.C(this.a, bVar);
                if (bVar2 == null) {
                    return false;
                }
            } else {
                bVar2 = null;
            }
            int E = d.this.E(this.a, i);
            k.a aVar = this.f0;
            if (aVar.a != E || !androidx.media3.common.util.b.c(aVar.b, bVar2)) {
                this.f0 = d.this.r(E, bVar2, 0L);
            }
            b.a aVar2 = this.g0;
            if (aVar2.a == E && androidx.media3.common.util.b.c(aVar2.b, bVar2)) {
                return true;
            }
            this.g0 = d.this.p(E, bVar2);
            return true;
        }

        @Override // androidx.media3.exoplayer.source.k
        public void b0(int i, j.b bVar, u02 u02Var, g62 g62Var, IOException iOException, boolean z) {
            if (b(i, bVar)) {
                this.f0.y(u02Var, i(g62Var), iOException, z);
            }
        }

        @Override // androidx.media3.exoplayer.drm.b
        public void d0(int i, j.b bVar) {
            if (b(i, bVar)) {
                this.g0.m();
            }
        }

        @Override // androidx.media3.exoplayer.source.k
        public void f0(int i, j.b bVar, u02 u02Var, g62 g62Var) {
            if (b(i, bVar)) {
                this.f0.B(u02Var, i(g62Var));
            }
        }

        @Override // androidx.media3.exoplayer.drm.b
        public void g0(int i, j.b bVar, int i2) {
            if (b(i, bVar)) {
                this.g0.k(i2);
            }
        }

        public final g62 i(g62 g62Var) {
            long D = d.this.D(this.a, g62Var.f);
            long D2 = d.this.D(this.a, g62Var.g);
            return (D == g62Var.f && D2 == g62Var.g) ? g62Var : new g62(g62Var.a, g62Var.b, g62Var.c, g62Var.d, g62Var.e, D, D2);
        }

        @Override // androidx.media3.exoplayer.source.k
        public void i0(int i, j.b bVar, u02 u02Var, g62 g62Var) {
            if (b(i, bVar)) {
                this.f0.v(u02Var, i(g62Var));
            }
        }

        @Override // androidx.media3.exoplayer.drm.b
        public /* synthetic */ void j0(int i, j.b bVar) {
            pr0.a(this, i, bVar);
        }

        @Override // androidx.media3.exoplayer.drm.b
        public void n0(int i, j.b bVar) {
            if (b(i, bVar)) {
                this.g0.i();
            }
        }

        @Override // androidx.media3.exoplayer.drm.b
        public void o0(int i, j.b bVar, Exception exc) {
            if (b(i, bVar)) {
                this.g0.l(exc);
            }
        }
    }

    /* compiled from: CompositeMediaSource.java */
    /* loaded from: classes.dex */
    public static final class b<T> {
        public final j a;
        public final j.c b;
        public final d<T>.a c;

        public b(j jVar, j.c cVar, d<T>.a aVar) {
            this.a = jVar;
            this.b = cVar;
            this.c = aVar;
        }
    }

    @Override // androidx.media3.exoplayer.source.a
    public void A() {
        for (b<T> bVar : this.h.values()) {
            bVar.a.f(bVar.b);
            bVar.a.d(bVar.c);
            bVar.a.n(bVar.c);
        }
        this.h.clear();
    }

    public j.b C(T t, j.b bVar) {
        return bVar;
    }

    public long D(T t, long j) {
        return j;
    }

    public int E(T t, int i) {
        return i;
    }

    /* renamed from: G */
    public abstract void F(T t, j jVar, androidx.media3.common.u uVar);

    public final void H(final T t, j jVar) {
        ii.a(!this.h.containsKey(t));
        j.c cVar = new j.c() { // from class: o40
            @Override // androidx.media3.exoplayer.source.j.c
            public final void a(j jVar2, u uVar) {
                d.this.F(t, jVar2, uVar);
            }
        };
        a aVar = new a(t);
        this.h.put(t, new b<>(jVar, cVar, aVar));
        jVar.a((Handler) ii.e(this.i), aVar);
        jVar.m((Handler) ii.e(this.i), aVar);
        jVar.c(cVar, this.j, w());
        if (x()) {
            return;
        }
        jVar.g(cVar);
    }

    @Override // androidx.media3.exoplayer.source.j
    public void j() throws IOException {
        for (b<T> bVar : this.h.values()) {
            bVar.a.j();
        }
    }

    @Override // androidx.media3.exoplayer.source.a
    public void u() {
        for (b<T> bVar : this.h.values()) {
            bVar.a.g(bVar.b);
        }
    }

    @Override // androidx.media3.exoplayer.source.a
    public void v() {
        for (b<T> bVar : this.h.values()) {
            bVar.a.b(bVar.b);
        }
    }

    @Override // androidx.media3.exoplayer.source.a
    public void y(fa4 fa4Var) {
        this.j = fa4Var;
        this.i = androidx.media3.common.util.b.v();
    }
}
