package androidx.media3.exoplayer.source;

import androidx.media3.common.u;
import androidx.media3.exoplayer.source.j;
import zendesk.support.request.CellBase;

/* compiled from: MaskingMediaSource.java */
/* loaded from: classes.dex */
public final class h extends d<Void> {
    public final j k;
    public final boolean l;
    public final u.c m;
    public final u.b n;
    public a o;
    public g p;
    public boolean q;
    public boolean r;
    public boolean s;

    /* compiled from: MaskingMediaSource.java */
    /* loaded from: classes.dex */
    public static final class a extends l91 {
        public static final Object i0 = new Object();
        public final Object g0;
        public final Object h0;

        public a(androidx.media3.common.u uVar, Object obj, Object obj2) {
            super(uVar);
            this.g0 = obj;
            this.h0 = obj2;
        }

        public static a w(androidx.media3.common.m mVar) {
            return new a(new b(mVar), u.c.v0, i0);
        }

        public static a x(androidx.media3.common.u uVar, Object obj, Object obj2) {
            return new a(uVar, obj, obj2);
        }

        @Override // defpackage.l91, androidx.media3.common.u
        public int b(Object obj) {
            Object obj2;
            androidx.media3.common.u uVar = this.f0;
            if (i0.equals(obj) && (obj2 = this.h0) != null) {
                obj = obj2;
            }
            return uVar.b(obj);
        }

        @Override // defpackage.l91, androidx.media3.common.u
        public u.b g(int i, u.b bVar, boolean z) {
            this.f0.g(i, bVar, z);
            if (androidx.media3.common.util.b.c(bVar.f0, this.h0) && z) {
                bVar.f0 = i0;
            }
            return bVar;
        }

        @Override // defpackage.l91, androidx.media3.common.u
        public Object m(int i) {
            Object m = this.f0.m(i);
            return androidx.media3.common.util.b.c(m, this.h0) ? i0 : m;
        }

        @Override // defpackage.l91, androidx.media3.common.u
        public u.c o(int i, u.c cVar, long j) {
            this.f0.o(i, cVar, j);
            if (androidx.media3.common.util.b.c(cVar.a, this.g0)) {
                cVar.a = u.c.v0;
            }
            return cVar;
        }

        public a v(androidx.media3.common.u uVar) {
            return new a(uVar, this.g0, this.h0);
        }
    }

    /* compiled from: MaskingMediaSource.java */
    /* loaded from: classes.dex */
    public static final class b extends androidx.media3.common.u {
        public final androidx.media3.common.m f0;

        public b(androidx.media3.common.m mVar) {
            this.f0 = mVar;
        }

        @Override // androidx.media3.common.u
        public int b(Object obj) {
            return obj == a.i0 ? 0 : -1;
        }

        @Override // androidx.media3.common.u
        public u.b g(int i, u.b bVar, boolean z) {
            bVar.v(z ? 0 : null, z ? a.i0 : null, 0, CellBase.ID_SYSTEM_MESSAGE_REQUEST_CLOSED, 0L, androidx.media3.common.a.k0, true);
            return bVar;
        }

        @Override // androidx.media3.common.u
        public int i() {
            return 1;
        }

        @Override // androidx.media3.common.u
        public Object m(int i) {
            return a.i0;
        }

        @Override // androidx.media3.common.u
        public u.c o(int i, u.c cVar, long j) {
            cVar.k(u.c.v0, this.f0, null, CellBase.ID_SYSTEM_MESSAGE_REQUEST_CLOSED, CellBase.ID_SYSTEM_MESSAGE_REQUEST_CLOSED, CellBase.ID_SYSTEM_MESSAGE_REQUEST_CLOSED, false, true, null, 0L, CellBase.ID_SYSTEM_MESSAGE_REQUEST_CLOSED, 0, 0, 0L);
            cVar.p0 = true;
            return cVar;
        }

        @Override // androidx.media3.common.u
        public int p() {
            return 1;
        }
    }

    public h(j jVar, boolean z) {
        this.k = jVar;
        this.l = z && jVar.k();
        this.m = new u.c();
        this.n = new u.b();
        androidx.media3.common.u l = jVar.l();
        if (l != null) {
            this.o = a.x(l, null, null);
            this.s = true;
            return;
        }
        this.o = a.w(jVar.i());
    }

    @Override // androidx.media3.exoplayer.source.d, androidx.media3.exoplayer.source.a
    public void A() {
        this.r = false;
        this.q = false;
        super.A();
    }

    @Override // androidx.media3.exoplayer.source.j
    /* renamed from: I */
    public g e(j.b bVar, gb gbVar, long j) {
        g gVar = new g(bVar, gbVar, j);
        gVar.w(this.k);
        if (this.r) {
            gVar.b(bVar.c(K(bVar.a)));
        } else {
            this.p = gVar;
            if (!this.q) {
                this.q = true;
                H(null, this.k);
            }
        }
        return gVar;
    }

    public final Object J(Object obj) {
        return (this.o.h0 == null || !this.o.h0.equals(obj)) ? obj : a.i0;
    }

    public final Object K(Object obj) {
        return (this.o.h0 == null || !obj.equals(a.i0)) ? obj : this.o.h0;
    }

    @Override // androidx.media3.exoplayer.source.d
    /* renamed from: L */
    public j.b C(Void r1, j.b bVar) {
        return bVar.c(J(bVar.a));
    }

    public androidx.media3.common.u M() {
        return this.o;
    }

    /* JADX WARN: Removed duplicated region for block: B:22:0x008d  */
    /* JADX WARN: Removed duplicated region for block: B:23:0x0094  */
    /* JADX WARN: Removed duplicated region for block: B:26:0x009e  */
    /* JADX WARN: Removed duplicated region for block: B:30:0x00bb  */
    /* JADX WARN: Removed duplicated region for block: B:32:? A[RETURN, SYNTHETIC] */
    @Override // androidx.media3.exoplayer.source.d
    /* renamed from: N */
    /*
        Code decompiled incorrectly, please refer to instructions dump.
        To view partially-correct code enable 'Show inconsistent code' option in preferences
    */
    public void G(java.lang.Void r13, androidx.media3.exoplayer.source.j r14, androidx.media3.common.u r15) {
        /*
            r12 = this;
            boolean r13 = r12.r
            if (r13 == 0) goto L19
            androidx.media3.exoplayer.source.h$a r13 = r12.o
            androidx.media3.exoplayer.source.h$a r13 = r13.v(r15)
            r12.o = r13
            androidx.media3.exoplayer.source.g r13 = r12.p
            if (r13 == 0) goto Lae
            long r13 = r13.j()
            r12.O(r13)
            goto Lae
        L19:
            boolean r13 = r15.q()
            if (r13 == 0) goto L36
            boolean r13 = r12.s
            if (r13 == 0) goto L2a
            androidx.media3.exoplayer.source.h$a r13 = r12.o
            androidx.media3.exoplayer.source.h$a r13 = r13.v(r15)
            goto L32
        L2a:
            java.lang.Object r13 = androidx.media3.common.u.c.v0
            java.lang.Object r14 = androidx.media3.exoplayer.source.h.a.i0
            androidx.media3.exoplayer.source.h$a r13 = androidx.media3.exoplayer.source.h.a.x(r15, r13, r14)
        L32:
            r12.o = r13
            goto Lae
        L36:
            androidx.media3.common.u$c r13 = r12.m
            r14 = 0
            r15.n(r14, r13)
            androidx.media3.common.u$c r13 = r12.m
            long r0 = r13.f()
            androidx.media3.common.u$c r13 = r12.m
            java.lang.Object r13 = r13.a
            androidx.media3.exoplayer.source.g r2 = r12.p
            if (r2 == 0) goto L74
            long r2 = r2.m()
            androidx.media3.exoplayer.source.h$a r4 = r12.o
            androidx.media3.exoplayer.source.g r5 = r12.p
            androidx.media3.exoplayer.source.j$b r5 = r5.a
            java.lang.Object r5 = r5.a
            androidx.media3.common.u$b r6 = r12.n
            r4.h(r5, r6)
            androidx.media3.common.u$b r4 = r12.n
            long r4 = r4.p()
            long r4 = r4 + r2
            androidx.media3.exoplayer.source.h$a r2 = r12.o
            androidx.media3.common.u$c r3 = r12.m
            androidx.media3.common.u$c r14 = r2.n(r14, r3)
            long r2 = r14.f()
            int r14 = (r4 > r2 ? 1 : (r4 == r2 ? 0 : -1))
            if (r14 == 0) goto L74
            r10 = r4
            goto L75
        L74:
            r10 = r0
        L75:
            androidx.media3.common.u$c r7 = r12.m
            androidx.media3.common.u$b r8 = r12.n
            r9 = 0
            r6 = r15
            android.util.Pair r14 = r6.j(r7, r8, r9, r10)
            java.lang.Object r0 = r14.first
            java.lang.Object r14 = r14.second
            java.lang.Long r14 = (java.lang.Long) r14
            long r1 = r14.longValue()
            boolean r14 = r12.s
            if (r14 == 0) goto L94
            androidx.media3.exoplayer.source.h$a r13 = r12.o
            androidx.media3.exoplayer.source.h$a r13 = r13.v(r15)
            goto L98
        L94:
            androidx.media3.exoplayer.source.h$a r13 = androidx.media3.exoplayer.source.h.a.x(r15, r13, r0)
        L98:
            r12.o = r13
            androidx.media3.exoplayer.source.g r13 = r12.p
            if (r13 == 0) goto Lae
            r12.O(r1)
            androidx.media3.exoplayer.source.j$b r13 = r13.a
            java.lang.Object r14 = r13.a
            java.lang.Object r14 = r12.K(r14)
            androidx.media3.exoplayer.source.j$b r13 = r13.c(r14)
            goto Laf
        Lae:
            r13 = 0
        Laf:
            r14 = 1
            r12.s = r14
            r12.r = r14
            androidx.media3.exoplayer.source.h$a r14 = r12.o
            r12.z(r14)
            if (r13 == 0) goto Lc6
            androidx.media3.exoplayer.source.g r14 = r12.p
            java.lang.Object r14 = defpackage.ii.e(r14)
            androidx.media3.exoplayer.source.g r14 = (androidx.media3.exoplayer.source.g) r14
            r14.b(r13)
        Lc6:
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: androidx.media3.exoplayer.source.h.G(java.lang.Void, androidx.media3.exoplayer.source.j, androidx.media3.common.u):void");
    }

    public final void O(long j) {
        g gVar = this.p;
        int b2 = this.o.b(gVar.a.a);
        if (b2 == -1) {
            return;
        }
        long j2 = this.o.f(b2, this.n).h0;
        if (j2 != CellBase.ID_SYSTEM_MESSAGE_REQUEST_CLOSED && j >= j2) {
            j = Math.max(0L, j2 - 1);
        }
        gVar.u(j);
    }

    @Override // androidx.media3.exoplayer.source.j
    public androidx.media3.common.m i() {
        return this.k.i();
    }

    @Override // androidx.media3.exoplayer.source.d, androidx.media3.exoplayer.source.j
    public void j() {
    }

    @Override // androidx.media3.exoplayer.source.j
    public void o(i iVar) {
        ((g) iVar).v();
        if (iVar == this.p) {
            this.p = null;
        }
    }

    @Override // androidx.media3.exoplayer.source.d, androidx.media3.exoplayer.source.a
    public void y(fa4 fa4Var) {
        super.y(fa4Var);
        if (this.l) {
            return;
        }
        this.q = true;
        H(null, this.k);
    }
}
