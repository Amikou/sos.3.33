package androidx.media3.exoplayer.source;

import android.net.Uri;
import java.io.IOException;
import java.util.List;
import java.util.Map;

/* compiled from: IcyDataSource.java */
/* loaded from: classes.dex */
public final class f implements androidx.media3.datasource.b {
    public final androidx.media3.datasource.b a;
    public final int b;
    public final a c;
    public final byte[] d;
    public int e;

    /* compiled from: IcyDataSource.java */
    /* loaded from: classes.dex */
    public interface a {
        void b(op2 op2Var);
    }

    public f(androidx.media3.datasource.b bVar, int i, a aVar) {
        ii.a(i > 0);
        this.a = bVar;
        this.b = i;
        this.c = aVar;
        this.d = new byte[1];
        this.e = i;
    }

    @Override // androidx.media3.datasource.b
    public long b(je0 je0Var) {
        throw new UnsupportedOperationException();
    }

    @Override // androidx.media3.datasource.b
    public void c(fa4 fa4Var) {
        ii.e(fa4Var);
        this.a.c(fa4Var);
    }

    @Override // androidx.media3.datasource.b
    public void close() {
        throw new UnsupportedOperationException();
    }

    @Override // androidx.media3.datasource.b
    public Map<String, List<String>> i() {
        return this.a.i();
    }

    @Override // androidx.media3.datasource.b
    public Uri m() {
        return this.a.m();
    }

    public final boolean o() throws IOException {
        if (this.a.read(this.d, 0, 1) == -1) {
            return false;
        }
        int i = (this.d[0] & 255) << 4;
        if (i == 0) {
            return true;
        }
        byte[] bArr = new byte[i];
        int i2 = i;
        int i3 = 0;
        while (i2 > 0) {
            int read = this.a.read(bArr, i3, i2);
            if (read == -1) {
                return false;
            }
            i3 += read;
            i2 -= read;
        }
        while (i > 0 && bArr[i - 1] == 0) {
            i--;
        }
        if (i > 0) {
            this.c.b(new op2(bArr, i));
        }
        return true;
    }

    @Override // androidx.media3.common.g
    public int read(byte[] bArr, int i, int i2) throws IOException {
        if (this.e == 0) {
            if (!o()) {
                return -1;
            }
            this.e = this.b;
        }
        int read = this.a.read(bArr, i, Math.min(this.e, i2));
        if (read != -1) {
            this.e -= read;
        }
        return read;
    }
}
