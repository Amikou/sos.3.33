package androidx.media3.exoplayer.source;

import androidx.media3.exoplayer.source.s;
import java.io.IOException;

/* compiled from: MediaPeriod.java */
/* loaded from: classes.dex */
public interface i extends s {

    /* compiled from: MediaPeriod.java */
    /* loaded from: classes.dex */
    public interface a extends s.a<i> {
        void f(i iVar);
    }

    @Override // androidx.media3.exoplayer.source.s
    long a();

    long c(long j, xi3 xi3Var);

    @Override // androidx.media3.exoplayer.source.s
    boolean d(long j);

    @Override // androidx.media3.exoplayer.source.s
    boolean e();

    @Override // androidx.media3.exoplayer.source.s
    long g();

    @Override // androidx.media3.exoplayer.source.s
    void h(long j);

    void k() throws IOException;

    long l(long j);

    long n(androidx.media3.exoplayer.trackselection.c[] cVarArr, boolean[] zArr, r[] rVarArr, boolean[] zArr2, long j);

    long o();

    void q(a aVar, long j);

    c84 r();

    void t(long j, boolean z);
}
