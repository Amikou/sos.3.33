package androidx.media3.exoplayer;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Handler;

/* compiled from: AudioBecomingNoisyManager.java */
/* loaded from: classes.dex */
public final class a {
    public final Context a;
    public final RunnableC0030a b;
    public boolean c;

    /* compiled from: AudioBecomingNoisyManager.java */
    /* renamed from: androidx.media3.exoplayer.a$a  reason: collision with other inner class name */
    /* loaded from: classes.dex */
    public final class RunnableC0030a extends BroadcastReceiver implements Runnable {
        public final b a;
        public final Handler f0;

        public RunnableC0030a(Handler handler, b bVar) {
            this.f0 = handler;
            this.a = bVar;
        }

        @Override // android.content.BroadcastReceiver
        public void onReceive(Context context, Intent intent) {
            if ("android.media.AUDIO_BECOMING_NOISY".equals(intent.getAction())) {
                this.f0.post(this);
            }
        }

        @Override // java.lang.Runnable
        public void run() {
            if (a.this.c) {
                this.a.s();
            }
        }
    }

    /* compiled from: AudioBecomingNoisyManager.java */
    /* loaded from: classes.dex */
    public interface b {
        void s();
    }

    public a(Context context, Handler handler, b bVar) {
        this.a = context.getApplicationContext();
        this.b = new RunnableC0030a(handler, bVar);
    }

    public void b(boolean z) {
        if (z && !this.c) {
            this.a.registerReceiver(this.b, new IntentFilter("android.media.AUDIO_BECOMING_NOISY"));
            this.c = true;
        } else if (z || !this.c) {
        } else {
            this.a.unregisterReceiver(this.b);
            this.c = false;
        }
    }
}
