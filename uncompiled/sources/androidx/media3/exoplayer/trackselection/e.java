package androidx.media3.exoplayer.trackselection;

import androidx.media3.common.v;
import androidx.media3.common.y;
import androidx.media3.exoplayer.trackselection.d;
import com.google.common.collect.ImmutableList;
import java.util.Arrays;
import java.util.List;

/* compiled from: TrackSelectionUtil.java */
/* loaded from: classes.dex */
public final class e {
    public static y a(d.a aVar, h84[] h84VarArr) {
        List[] listArr = new List[h84VarArr.length];
        for (int i = 0; i < h84VarArr.length; i++) {
            h84 h84Var = h84VarArr[i];
            listArr[i] = h84Var != null ? ImmutableList.of(h84Var) : ImmutableList.of();
        }
        return b(aVar, listArr);
    }

    public static y b(d.a aVar, List<? extends TrackSelection>[] listArr) {
        boolean z;
        ImmutableList.a aVar2 = new ImmutableList.a();
        for (int i = 0; i < aVar.d(); i++) {
            c84 f = aVar.f(i);
            List<? extends TrackSelection> list = listArr[i];
            for (int i2 = 0; i2 < f.a; i2++) {
                v b = f.b(i2);
                boolean z2 = aVar.a(i, i2, false) != 0;
                int i3 = b.a;
                int[] iArr = new int[i3];
                boolean[] zArr = new boolean[i3];
                for (int i4 = 0; i4 < b.a; i4++) {
                    iArr[i4] = aVar.g(i, i2, i4);
                    int i5 = 0;
                    while (true) {
                        if (i5 >= list.size()) {
                            z = false;
                            break;
                        }
                        h84 h84Var = list.get(i5);
                        if (h84Var.c().equals(b) && h84Var.t(i4) != -1) {
                            z = true;
                            break;
                        }
                        i5++;
                    }
                    zArr[i4] = z;
                }
                aVar2.a(new y.a(b, z2, iArr, zArr));
            }
        }
        c84 h = aVar.h();
        for (int i6 = 0; i6 < h.a; i6++) {
            v b2 = h.b(i6);
            int[] iArr2 = new int[b2.a];
            Arrays.fill(iArr2, 0);
            aVar2.a(new y.a(b2, false, iArr2, new boolean[b2.a]));
        }
        return new y(aVar2.l());
    }
}
