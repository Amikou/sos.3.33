package androidx.media3.exoplayer.trackselection;

import android.util.Pair;
import androidx.media3.common.u;
import androidx.media3.common.v;
import androidx.media3.exoplayer.ExoPlaybackException;
import androidx.media3.exoplayer.RendererConfiguration;
import androidx.media3.exoplayer.n;
import androidx.media3.exoplayer.source.j;
import java.util.Arrays;

/* compiled from: MappingTrackSelector.java */
/* loaded from: classes.dex */
public abstract class d extends f {

    /* compiled from: MappingTrackSelector.java */
    /* loaded from: classes.dex */
    public static final class a {
        public final int a;
        public final int[] b;
        public final c84[] c;
        public final int[] d;
        public final int[][][] e;
        public final c84 f;

        public a(String[] strArr, int[] iArr, c84[] c84VarArr, int[] iArr2, int[][][] iArr3, c84 c84Var) {
            this.b = iArr;
            this.c = c84VarArr;
            this.e = iArr3;
            this.d = iArr2;
            this.f = c84Var;
            this.a = iArr.length;
        }

        public int a(int i, int i2, boolean z) {
            int i3 = this.c[i].b(i2).a;
            int[] iArr = new int[i3];
            int i4 = 0;
            for (int i5 = 0; i5 < i3; i5++) {
                int g = g(i, i2, i5);
                if (g == 4 || (z && g == 3)) {
                    iArr[i4] = i5;
                    i4++;
                }
            }
            return b(i, i2, Arrays.copyOf(iArr, i4));
        }

        public int b(int i, int i2, int[] iArr) {
            int i3 = 0;
            int i4 = 16;
            String str = null;
            boolean z = false;
            int i5 = 0;
            while (i3 < iArr.length) {
                String str2 = this.c[i].b(i2).c(iArr[i3]).p0;
                int i6 = i5 + 1;
                if (i5 == 0) {
                    str = str2;
                } else {
                    z |= !androidx.media3.common.util.b.c(str, str2);
                }
                i4 = Math.min(i4, u63.d(this.e[i][i2][i3]));
                i3++;
                i5 = i6;
            }
            return z ? Math.min(i4, this.d[i]) : i4;
        }

        public int c(int i, int i2, int i3) {
            return this.e[i][i2][i3];
        }

        public int d() {
            return this.a;
        }

        public int e(int i) {
            return this.b[i];
        }

        public c84 f(int i) {
            return this.c[i];
        }

        public int g(int i, int i2, int i3) {
            return u63.f(c(i, i2, i3));
        }

        public c84 h() {
            return this.f;
        }
    }

    public static int k(n[] nVarArr, v vVar, int[] iArr, boolean z) throws ExoPlaybackException {
        int length = nVarArr.length;
        boolean z2 = true;
        int i = 0;
        for (int i2 = 0; i2 < nVarArr.length; i2++) {
            n nVar = nVarArr[i2];
            int i3 = 0;
            for (int i4 = 0; i4 < vVar.a; i4++) {
                i3 = Math.max(i3, u63.f(nVar.a(vVar.c(i4))));
            }
            boolean z3 = iArr[i2] == 0;
            if (i3 > i || (i3 == i && z && !z2 && z3)) {
                length = i2;
                z2 = z3;
                i = i3;
            }
        }
        return length;
    }

    public static int[] l(n nVar, v vVar) throws ExoPlaybackException {
        int[] iArr = new int[vVar.a];
        for (int i = 0; i < vVar.a; i++) {
            iArr[i] = nVar.a(vVar.c(i));
        }
        return iArr;
    }

    public static int[] m(n[] nVarArr) throws ExoPlaybackException {
        int length = nVarArr.length;
        int[] iArr = new int[length];
        for (int i = 0; i < length; i++) {
            iArr[i] = nVarArr[i].q();
        }
        return iArr;
    }

    @Override // androidx.media3.exoplayer.trackselection.f
    public final void f(Object obj) {
        a aVar = (a) obj;
    }

    @Override // androidx.media3.exoplayer.trackselection.f
    public final g h(n[] nVarArr, c84 c84Var, j.b bVar, u uVar) throws ExoPlaybackException {
        int[] l;
        int[] iArr = new int[nVarArr.length + 1];
        int length = nVarArr.length + 1;
        v[][] vVarArr = new v[length];
        int[][][] iArr2 = new int[nVarArr.length + 1][];
        for (int i = 0; i < length; i++) {
            int i2 = c84Var.a;
            vVarArr[i] = new v[i2];
            iArr2[i] = new int[i2];
        }
        int[] m = m(nVarArr);
        for (int i3 = 0; i3 < c84Var.a; i3++) {
            v b = c84Var.b(i3);
            int k = k(nVarArr, b, iArr, b.g0 == 5);
            if (k == nVarArr.length) {
                l = new int[b.a];
            } else {
                l = l(nVarArr[k], b);
            }
            int i4 = iArr[k];
            vVarArr[k][i4] = b;
            iArr2[k][i4] = l;
            iArr[k] = iArr[k] + 1;
        }
        c84[] c84VarArr = new c84[nVarArr.length];
        String[] strArr = new String[nVarArr.length];
        int[] iArr3 = new int[nVarArr.length];
        for (int i5 = 0; i5 < nVarArr.length; i5++) {
            int i6 = iArr[i5];
            c84VarArr[i5] = new c84((v[]) androidx.media3.common.util.b.C0(vVarArr[i5], i6));
            iArr2[i5] = (int[][]) androidx.media3.common.util.b.C0(iArr2[i5], i6);
            strArr[i5] = nVarArr[i5].getName();
            iArr3[i5] = nVarArr[i5].h();
        }
        a aVar = new a(strArr, iArr3, c84VarArr, m, iArr2, new c84((v[]) androidx.media3.common.util.b.C0(vVarArr[nVarArr.length], iArr[nVarArr.length])));
        Pair<RendererConfiguration[], c[]> n = n(aVar, iArr2, m, bVar, uVar);
        return new g((v63[]) n.first, (c[]) n.second, e.a(aVar, (h84[]) n.second), aVar);
    }

    public abstract Pair<RendererConfiguration[], c[]> n(a aVar, int[][][] iArr, int[] iArr2, j.b bVar, u uVar) throws ExoPlaybackException;
}
