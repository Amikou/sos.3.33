package androidx.media3.exoplayer.trackselection;

import androidx.media3.common.u;
import androidx.media3.common.v;
import androidx.media3.exoplayer.source.chunk.MediaChunkIterator;
import androidx.media3.exoplayer.source.j;
import androidx.media3.exoplayer.trackselection.c;
import com.github.mikephil.charting.utils.Utils;
import com.google.common.collect.ImmutableList;
import com.google.common.collect.MultimapBuilder;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.List;
import zendesk.support.request.CellBase;

/* compiled from: AdaptiveTrackSelection.java */
/* loaded from: classes.dex */
public class a extends ao {
    public final gm g;
    public final long h;
    public final long i;
    public final long j;
    public final int k;
    public final int l;
    public final float m;
    public final float n;
    public final ImmutableList<C0040a> o;
    public final tz p;
    public float q;
    public int r;
    public int s;
    public long t;
    public s52 u;

    /* compiled from: AdaptiveTrackSelection.java */
    /* renamed from: androidx.media3.exoplayer.trackselection.a$a  reason: collision with other inner class name */
    /* loaded from: classes.dex */
    public static final class C0040a {
        public final long a;
        public final long b;

        public C0040a(long j, long j2) {
            this.a = j;
            this.b = j2;
        }

        public boolean equals(Object obj) {
            if (this == obj) {
                return true;
            }
            if (obj instanceof C0040a) {
                C0040a c0040a = (C0040a) obj;
                return this.a == c0040a.a && this.b == c0040a.b;
            }
            return false;
        }

        public int hashCode() {
            return (((int) this.a) * 31) + ((int) this.b);
        }
    }

    /* compiled from: AdaptiveTrackSelection.java */
    /* loaded from: classes.dex */
    public static class b implements c.b {
        public final int a;
        public final int b;
        public final int c;
        public final int d;
        public final int e;
        public final float f;
        public final float g;
        public final tz h;

        public b() {
            this(10000, 25000, 25000, 0.7f);
        }

        @Override // androidx.media3.exoplayer.trackselection.c.b
        public final c[] a(c.a[] aVarArr, gm gmVar, j.b bVar, u uVar) {
            c b;
            ImmutableList A = a.A(aVarArr);
            c[] cVarArr = new c[aVarArr.length];
            for (int i = 0; i < aVarArr.length; i++) {
                c.a aVar = aVarArr[i];
                if (aVar != null) {
                    int[] iArr = aVar.b;
                    if (iArr.length != 0) {
                        if (iArr.length == 1) {
                            b = new j61(aVar.a, iArr[0], aVar.c);
                        } else {
                            b = b(aVar.a, iArr, aVar.c, gmVar, (ImmutableList) A.get(i));
                        }
                        cVarArr[i] = b;
                    }
                }
            }
            return cVarArr;
        }

        public a b(v vVar, int[] iArr, int i, gm gmVar, ImmutableList<C0040a> immutableList) {
            return new a(vVar, iArr, i, gmVar, this.a, this.b, this.c, this.d, this.e, this.f, this.g, immutableList, this.h);
        }

        public b(int i, int i2, int i3, float f) {
            this(i, i2, i3, 1279, 719, f, 0.75f, tz.a);
        }

        public b(int i, int i2, int i3, int i4, int i5, float f, float f2, tz tzVar) {
            this.a = i;
            this.b = i2;
            this.c = i3;
            this.d = i4;
            this.e = i5;
            this.f = f;
            this.g = f2;
            this.h = tzVar;
        }
    }

    public a(v vVar, int[] iArr, int i, gm gmVar, long j, long j2, long j3, int i2, int i3, float f, float f2, List<C0040a> list, tz tzVar) {
        super(vVar, iArr, i);
        gm gmVar2;
        long j4;
        if (j3 < j) {
            p12.i("AdaptiveTrackSelection", "Adjusting minDurationToRetainAfterDiscardMs to be at least minDurationForQualityIncreaseMs");
            gmVar2 = gmVar;
            j4 = j;
        } else {
            gmVar2 = gmVar;
            j4 = j3;
        }
        this.g = gmVar2;
        this.h = j * 1000;
        this.i = j2 * 1000;
        this.j = j4 * 1000;
        this.k = i2;
        this.l = i3;
        this.m = f;
        this.n = f2;
        this.o = ImmutableList.copyOf((Collection) list);
        this.p = tzVar;
        this.q = 1.0f;
        this.s = 0;
        this.t = CellBase.ID_SYSTEM_MESSAGE_REQUEST_CLOSED;
    }

    public static ImmutableList<ImmutableList<C0040a>> A(c.a[] aVarArr) {
        ArrayList arrayList = new ArrayList();
        for (int i = 0; i < aVarArr.length; i++) {
            if (aVarArr[i] != null && aVarArr[i].b.length > 1) {
                ImmutableList.a builder = ImmutableList.builder();
                builder.a(new C0040a(0L, 0L));
                arrayList.add(builder);
            } else {
                arrayList.add(null);
            }
        }
        long[][] F = F(aVarArr);
        int[] iArr = new int[F.length];
        long[] jArr = new long[F.length];
        for (int i2 = 0; i2 < F.length; i2++) {
            jArr[i2] = F[i2].length == 0 ? 0L : F[i2][0];
        }
        x(arrayList, jArr);
        ImmutableList<Integer> G = G(F);
        for (int i3 = 0; i3 < G.size(); i3++) {
            int intValue = G.get(i3).intValue();
            int i4 = iArr[intValue] + 1;
            iArr[intValue] = i4;
            jArr[intValue] = F[intValue][i4];
            x(arrayList, jArr);
        }
        for (int i5 = 0; i5 < aVarArr.length; i5++) {
            if (arrayList.get(i5) != null) {
                jArr[i5] = jArr[i5] * 2;
            }
        }
        x(arrayList, jArr);
        ImmutableList.a builder2 = ImmutableList.builder();
        for (int i6 = 0; i6 < arrayList.size(); i6++) {
            ImmutableList.a aVar = (ImmutableList.a) arrayList.get(i6);
            builder2.a(aVar == null ? ImmutableList.of() : aVar.l());
        }
        return builder2.l();
    }

    public static long[][] F(c.a[] aVarArr) {
        int[] iArr;
        long[][] jArr = new long[aVarArr.length];
        for (int i = 0; i < aVarArr.length; i++) {
            c.a aVar = aVarArr[i];
            if (aVar == null) {
                jArr[i] = new long[0];
            } else {
                jArr[i] = new long[aVar.b.length];
                int i2 = 0;
                while (true) {
                    if (i2 >= aVar.b.length) {
                        break;
                    }
                    jArr[i][i2] = aVar.a.c(iArr[i2]).l0;
                    i2++;
                }
                Arrays.sort(jArr[i]);
            }
        }
        return jArr;
    }

    public static ImmutableList<Integer> G(long[][] jArr) {
        wa2 e = MultimapBuilder.c().a().e();
        for (int i = 0; i < jArr.length; i++) {
            if (jArr[i].length > 1) {
                int length = jArr[i].length;
                double[] dArr = new double[length];
                int i2 = 0;
                while (true) {
                    int length2 = jArr[i].length;
                    double d = Utils.DOUBLE_EPSILON;
                    if (i2 >= length2) {
                        break;
                    }
                    if (jArr[i][i2] != -1) {
                        d = Math.log(jArr[i][i2]);
                    }
                    dArr[i2] = d;
                    i2++;
                }
                int i3 = length - 1;
                double d2 = dArr[i3] - dArr[0];
                int i4 = 0;
                while (i4 < i3) {
                    double d3 = dArr[i4];
                    i4++;
                    e.put(Double.valueOf(d2 == Utils.DOUBLE_EPSILON ? 1.0d : (((d3 + dArr[i4]) * 0.5d) - dArr[0]) / d2), Integer.valueOf(i));
                }
            }
        }
        return ImmutableList.copyOf(e.values());
    }

    public static void x(List<ImmutableList.a<C0040a>> list, long[] jArr) {
        long j = 0;
        for (long j2 : jArr) {
            j += j2;
        }
        for (int i = 0; i < list.size(); i++) {
            ImmutableList.a<C0040a> aVar = list.get(i);
            if (aVar != null) {
                aVar.a(new C0040a(j, jArr[i]));
            }
        }
    }

    public final long B(long j) {
        long j2;
        long H = H(j);
        if (this.o.isEmpty()) {
            return H;
        }
        int i = 1;
        while (i < this.o.size() - 1 && this.o.get(i).a < H) {
            i++;
        }
        C0040a c0040a = this.o.get(i - 1);
        C0040a c0040a2 = this.o.get(i);
        long j3 = c0040a.a;
        return c0040a.b + ((((float) (H - j3)) / ((float) (c0040a2.a - j3))) * ((float) (c0040a2.b - j2)));
    }

    public final long C(List<? extends s52> list) {
        if (list.isEmpty()) {
            return CellBase.ID_SYSTEM_MESSAGE_REQUEST_CLOSED;
        }
        s52 s52Var = (s52) lt1.d(list);
        long j = s52Var.g;
        if (j != CellBase.ID_SYSTEM_MESSAGE_REQUEST_CLOSED) {
            long j2 = s52Var.h;
            return j2 != CellBase.ID_SYSTEM_MESSAGE_REQUEST_CLOSED ? j2 - j : CellBase.ID_SYSTEM_MESSAGE_REQUEST_CLOSED;
        }
        return CellBase.ID_SYSTEM_MESSAGE_REQUEST_CLOSED;
    }

    public long D() {
        return this.j;
    }

    public final long E(MediaChunkIterator[] mediaChunkIteratorArr, List<? extends s52> list) {
        int i = this.r;
        if (i < mediaChunkIteratorArr.length && mediaChunkIteratorArr[i].next()) {
            MediaChunkIterator mediaChunkIterator = mediaChunkIteratorArr[this.r];
            return mediaChunkIterator.b() - mediaChunkIterator.a();
        }
        for (MediaChunkIterator mediaChunkIterator2 : mediaChunkIteratorArr) {
            if (mediaChunkIterator2.next()) {
                return mediaChunkIterator2.b() - mediaChunkIterator2.a();
            }
        }
        return C(list);
    }

    public final long H(long j) {
        long b2;
        long h = ((float) this.g.h()) * this.m;
        if (this.g.b() != CellBase.ID_SYSTEM_MESSAGE_REQUEST_CLOSED && j != CellBase.ID_SYSTEM_MESSAGE_REQUEST_CLOSED) {
            float f = (float) j;
            return (((float) h) * Math.max((f / this.q) - ((float) b2), (float) Utils.FLOAT_EPSILON)) / f;
        }
        return ((float) h) / this.q;
    }

    public final long I(long j, long j2) {
        if (j == CellBase.ID_SYSTEM_MESSAGE_REQUEST_CLOSED) {
            return this.h;
        }
        if (j2 != CellBase.ID_SYSTEM_MESSAGE_REQUEST_CLOSED) {
            j -= j2;
        }
        return Math.min(((float) j) * this.n, this.h);
    }

    public boolean J(long j, List<? extends s52> list) {
        long j2 = this.t;
        return j2 == CellBase.ID_SYSTEM_MESSAGE_REQUEST_CLOSED || j - j2 >= 1000 || !(list.isEmpty() || ((s52) lt1.d(list)).equals(this.u));
    }

    @Override // androidx.media3.exoplayer.trackselection.c
    public void a(long j, long j2, long j3, List<? extends s52> list, MediaChunkIterator[] mediaChunkIteratorArr) {
        long b2 = this.p.b();
        long E = E(mediaChunkIteratorArr, list);
        int i = this.s;
        if (i == 0) {
            this.s = 1;
            this.r = z(b2, E);
            return;
        }
        int i2 = this.r;
        int b3 = list.isEmpty() ? -1 : b(((s52) lt1.d(list)).d);
        if (b3 != -1) {
            i = ((s52) lt1.d(list)).e;
            i2 = b3;
        }
        int z = z(b2, E);
        if (!h(i2, b2)) {
            androidx.media3.common.j j4 = j(i2);
            androidx.media3.common.j j5 = j(z);
            long I = I(j3, E);
            int i3 = j5.l0;
            int i4 = j4.l0;
            if ((i3 > i4 && j2 < I) || (i3 < i4 && j2 >= this.i)) {
                z = i2;
            }
        }
        if (z != i2) {
            i = 3;
        }
        this.s = i;
        this.r = z;
    }

    @Override // androidx.media3.exoplayer.trackselection.c
    public int d() {
        return this.r;
    }

    @Override // defpackage.ao, androidx.media3.exoplayer.trackselection.c
    public void g() {
        this.u = null;
    }

    @Override // defpackage.ao, androidx.media3.exoplayer.trackselection.c
    public void k() {
        this.t = CellBase.ID_SYSTEM_MESSAGE_REQUEST_CLOSED;
        this.u = null;
    }

    @Override // defpackage.ao, androidx.media3.exoplayer.trackselection.c
    public int m(long j, List<? extends s52> list) {
        int i;
        int i2;
        long b2 = this.p.b();
        if (!J(b2, list)) {
            return list.size();
        }
        this.t = b2;
        this.u = list.isEmpty() ? null : (s52) lt1.d(list);
        if (list.isEmpty()) {
            return 0;
        }
        int size = list.size();
        long b0 = androidx.media3.common.util.b.b0(list.get(size - 1).g - j, this.q);
        long D = D();
        if (b0 < D) {
            return size;
        }
        androidx.media3.common.j j2 = j(z(b2, C(list)));
        for (int i3 = 0; i3 < size; i3++) {
            s52 s52Var = list.get(i3);
            androidx.media3.common.j jVar = s52Var.d;
            if (androidx.media3.common.util.b.b0(s52Var.g - j, this.q) >= D && jVar.l0 < j2.l0 && (i = jVar.v0) != -1 && i <= this.l && (i2 = jVar.u0) != -1 && i2 <= this.k && i < j2.v0) {
                return i3;
            }
        }
        return size;
    }

    @Override // androidx.media3.exoplayer.trackselection.c
    public int o() {
        return this.s;
    }

    @Override // defpackage.ao, androidx.media3.exoplayer.trackselection.c
    public void p(float f) {
        this.q = f;
    }

    @Override // androidx.media3.exoplayer.trackselection.c
    public Object q() {
        return null;
    }

    public boolean y(androidx.media3.common.j jVar, int i, long j) {
        return ((long) i) <= j;
    }

    public final int z(long j, long j2) {
        long B = B(j2);
        int i = 0;
        for (int i2 = 0; i2 < this.b; i2++) {
            if (j == Long.MIN_VALUE || !h(i2, j)) {
                androidx.media3.common.j j3 = j(i2);
                if (y(j3, j3.l0, B)) {
                    return i2;
                }
                i = i2;
            }
        }
        return i;
    }
}
