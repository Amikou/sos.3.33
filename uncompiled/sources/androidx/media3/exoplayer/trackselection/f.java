package androidx.media3.exoplayer.trackselection;

import androidx.media3.common.u;
import androidx.media3.common.x;
import androidx.media3.exoplayer.ExoPlaybackException;
import androidx.media3.exoplayer.n;
import androidx.media3.exoplayer.source.j;

/* compiled from: TrackSelector.java */
/* loaded from: classes.dex */
public abstract class f {
    public a a;
    public gm b;

    /* compiled from: TrackSelector.java */
    /* loaded from: classes.dex */
    public interface a {
        void a();
    }

    public final gm a() {
        return (gm) ii.i(this.b);
    }

    public x b() {
        return x.E0;
    }

    public void c(a aVar, gm gmVar) {
        this.a = aVar;
        this.b = gmVar;
    }

    public final void d() {
        a aVar = this.a;
        if (aVar != null) {
            aVar.a();
        }
    }

    public boolean e() {
        return false;
    }

    public abstract void f(Object obj);

    public void g() {
        this.a = null;
        this.b = null;
    }

    public abstract g h(n[] nVarArr, c84 c84Var, j.b bVar, u uVar) throws ExoPlaybackException;

    public void i(androidx.media3.common.b bVar) {
    }

    public void j(x xVar) {
    }
}
