package androidx.media3.exoplayer.trackselection;

import androidx.media3.common.u;
import androidx.media3.common.v;
import androidx.media3.exoplayer.source.chunk.MediaChunkIterator;
import androidx.media3.exoplayer.source.j;
import java.util.List;

/* compiled from: ExoTrackSelection.java */
/* loaded from: classes.dex */
public interface c extends h84 {

    /* compiled from: ExoTrackSelection.java */
    /* loaded from: classes.dex */
    public static final class a {
        public final v a;
        public final int[] b;
        public final int c;

        public a(v vVar, int... iArr) {
            this(vVar, iArr, 0);
        }

        public a(v vVar, int[] iArr, int i) {
            if (iArr.length == 0) {
                p12.d("ETSDefinition", "Empty tracks are not allowed", new IllegalArgumentException());
            }
            this.a = vVar;
            this.b = iArr;
            this.c = i;
        }
    }

    /* compiled from: ExoTrackSelection.java */
    /* loaded from: classes.dex */
    public interface b {
        c[] a(a[] aVarArr, gm gmVar, j.b bVar, u uVar);
    }

    void a(long j, long j2, long j3, List<? extends s52> list, MediaChunkIterator[] mediaChunkIteratorArr);

    int d();

    boolean e(long j, my myVar, List<? extends s52> list);

    boolean f(int i, long j);

    void g();

    boolean h(int i, long j);

    void i(boolean z);

    void k();

    int m(long j, List<? extends s52> list);

    androidx.media3.common.j n();

    int o();

    void p(float f);

    Object q();

    void r();

    void s();
}
