package androidx.media3.exoplayer.trackselection;

import androidx.media3.common.y;

/* compiled from: TrackSelectorResult.java */
/* loaded from: classes.dex */
public final class g {
    public final int a;
    public final v63[] b;
    public final c[] c;
    public final y d;
    public final Object e;

    public g(v63[] v63VarArr, c[] cVarArr, y yVar, Object obj) {
        this.b = v63VarArr;
        this.c = (c[]) cVarArr.clone();
        this.d = yVar;
        this.e = obj;
        this.a = v63VarArr.length;
    }

    public boolean a(g gVar) {
        if (gVar == null || gVar.c.length != this.c.length) {
            return false;
        }
        for (int i = 0; i < this.c.length; i++) {
            if (!b(gVar, i)) {
                return false;
            }
        }
        return true;
    }

    public boolean b(g gVar, int i) {
        return gVar != null && androidx.media3.common.util.b.c(this.b[i], gVar.b[i]) && androidx.media3.common.util.b.c(this.c[i], gVar.c[i]);
    }

    public boolean c(int i) {
        return this.b[i] != null;
    }
}
