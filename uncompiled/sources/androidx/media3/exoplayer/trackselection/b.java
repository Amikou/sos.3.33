package androidx.media3.exoplayer.trackselection;

import android.content.Context;
import android.graphics.Point;
import android.media.AudioFormat;
import android.media.AudioManager;
import android.media.Spatializer;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.text.TextUtils;
import android.util.Pair;
import android.util.SparseArray;
import android.util.SparseBooleanArray;
import androidx.media3.common.PlaybackException;
import androidx.media3.common.e;
import androidx.media3.common.j;
import androidx.media3.common.u;
import androidx.media3.common.v;
import androidx.media3.common.w;
import androidx.media3.common.x;
import androidx.media3.exoplayer.ExoPlaybackException;
import androidx.media3.exoplayer.RendererConfiguration;
import androidx.media3.exoplayer.source.j;
import androidx.media3.exoplayer.trackselection.a;
import androidx.media3.exoplayer.trackselection.b;
import androidx.media3.exoplayer.trackselection.c;
import androidx.media3.exoplayer.trackselection.d;
import com.google.common.collect.ImmutableList;
import com.google.common.collect.Ordering;
import com.google.common.primitives.Ints;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.RandomAccess;
import okhttp3.internal.http2.Http2;
import okhttp3.internal.ws.WebSocketProtocol;

/* compiled from: DefaultTrackSelector.java */
/* loaded from: classes.dex */
public class b extends androidx.media3.exoplayer.trackselection.d {
    public static final Ordering<Integer> j = Ordering.from(el0.a);
    public static final Ordering<Integer> k = Ordering.from(fl0.a);
    public final Object c;
    public final Context d;
    public final c.b e;
    public final boolean f;
    public d g;
    public f h;
    public androidx.media3.common.b i;

    /* compiled from: DefaultTrackSelector.java */
    /* renamed from: androidx.media3.exoplayer.trackselection.b$b  reason: collision with other inner class name */
    /* loaded from: classes.dex */
    public static final class C0041b extends h<C0041b> implements Comparable<C0041b> {
        public final int i0;
        public final boolean j0;
        public final String k0;
        public final d l0;
        public final boolean m0;
        public final int n0;
        public final int o0;
        public final int p0;
        public final boolean q0;
        public final int r0;
        public final int s0;
        public final boolean t0;
        public final int u0;
        public final int v0;
        public final int w0;
        public final int x0;
        public final boolean y0;
        public final boolean z0;

        public C0041b(int i, v vVar, int i2, d dVar, int i3, boolean z, gu2<j> gu2Var) {
            super(i, vVar, i2);
            int i4;
            int i5;
            int i6;
            this.l0 = dVar;
            this.k0 = b.S(this.h0.g0);
            this.m0 = b.K(i3, false);
            int i7 = 0;
            while (true) {
                i4 = Integer.MAX_VALUE;
                if (i7 >= dVar.r0.size()) {
                    i5 = 0;
                    i7 = Integer.MAX_VALUE;
                    break;
                }
                i5 = b.C(this.h0, dVar.r0.get(i7), false);
                if (i5 > 0) {
                    break;
                }
                i7++;
            }
            this.o0 = i7;
            this.n0 = i5;
            this.p0 = b.G(this.h0.i0, dVar.s0);
            j jVar = this.h0;
            int i8 = jVar.i0;
            this.q0 = i8 == 0 || (i8 & 1) != 0;
            this.t0 = (jVar.h0 & 1) != 0;
            int i9 = jVar.C0;
            this.u0 = i9;
            this.v0 = jVar.D0;
            int i10 = jVar.l0;
            this.w0 = i10;
            this.j0 = (i10 == -1 || i10 <= dVar.u0) && (i9 == -1 || i9 <= dVar.t0) && gu2Var.apply(jVar);
            String[] e0 = androidx.media3.common.util.b.e0();
            int i11 = 0;
            while (true) {
                if (i11 >= e0.length) {
                    i6 = 0;
                    i11 = Integer.MAX_VALUE;
                    break;
                }
                i6 = b.C(this.h0, e0[i11], false);
                if (i6 > 0) {
                    break;
                }
                i11++;
            }
            this.r0 = i11;
            this.s0 = i6;
            int i12 = 0;
            while (true) {
                if (i12 < dVar.v0.size()) {
                    String str = this.h0.p0;
                    if (str != null && str.equals(dVar.v0.get(i12))) {
                        i4 = i12;
                        break;
                    }
                    i12++;
                } else {
                    break;
                }
            }
            this.x0 = i4;
            this.y0 = u63.e(i3) == 128;
            this.z0 = u63.g(i3) == 64;
            this.i0 = h(i3, z);
        }

        public static int e(List<C0041b> list, List<C0041b> list2) {
            return ((C0041b) Collections.max(list)).compareTo((C0041b) Collections.max(list2));
        }

        public static ImmutableList<C0041b> g(int i, v vVar, d dVar, int[] iArr, boolean z, gu2<j> gu2Var) {
            ImmutableList.a builder = ImmutableList.builder();
            for (int i2 = 0; i2 < vVar.a; i2++) {
                builder.a(new C0041b(i, vVar, i2, dVar, iArr[i2], z, gu2Var));
            }
            return builder.l();
        }

        @Override // androidx.media3.exoplayer.trackselection.b.h
        public int a() {
            return this.i0;
        }

        @Override // java.lang.Comparable
        /* renamed from: f */
        public int compareTo(C0041b c0041b) {
            Ordering reverse = (this.j0 && this.m0) ? b.j : b.j.reverse();
            k30 e = k30.i().f(this.m0, c0041b.m0).e(Integer.valueOf(this.o0), Integer.valueOf(c0041b.o0), Ordering.natural().reverse()).d(this.n0, c0041b.n0).d(this.p0, c0041b.p0).f(this.t0, c0041b.t0).f(this.q0, c0041b.q0).e(Integer.valueOf(this.r0), Integer.valueOf(c0041b.r0), Ordering.natural().reverse()).d(this.s0, c0041b.s0).f(this.j0, c0041b.j0).e(Integer.valueOf(this.x0), Integer.valueOf(c0041b.x0), Ordering.natural().reverse()).e(Integer.valueOf(this.w0), Integer.valueOf(c0041b.w0), this.l0.A0 ? b.j.reverse() : b.k).f(this.y0, c0041b.y0).f(this.z0, c0041b.z0).e(Integer.valueOf(this.u0), Integer.valueOf(c0041b.u0), reverse).e(Integer.valueOf(this.v0), Integer.valueOf(c0041b.v0), reverse);
            Integer valueOf = Integer.valueOf(this.w0);
            Integer valueOf2 = Integer.valueOf(c0041b.w0);
            if (!androidx.media3.common.util.b.c(this.k0, c0041b.k0)) {
                reverse = b.k;
            }
            return e.e(valueOf, valueOf2, reverse).h();
        }

        public final int h(int i, boolean z) {
            if (b.K(i, this.l0.P0)) {
                if (this.j0 || this.l0.J0) {
                    if (b.K(i, false) && this.j0 && this.h0.l0 != -1) {
                        d dVar = this.l0;
                        if (!dVar.B0 && !dVar.A0 && (dVar.R0 || !z)) {
                            return 2;
                        }
                    }
                    return 1;
                }
                return 0;
            }
            return 0;
        }

        @Override // androidx.media3.exoplayer.trackselection.b.h
        /* renamed from: j */
        public boolean d(C0041b c0041b) {
            int i;
            String str;
            int i2;
            d dVar = this.l0;
            if ((dVar.M0 || ((i2 = this.h0.C0) != -1 && i2 == c0041b.h0.C0)) && (dVar.K0 || ((str = this.h0.p0) != null && TextUtils.equals(str, c0041b.h0.p0)))) {
                d dVar2 = this.l0;
                if ((dVar2.L0 || ((i = this.h0.D0) != -1 && i == c0041b.h0.D0)) && (dVar2.N0 || (this.y0 == c0041b.y0 && this.z0 == c0041b.z0))) {
                    return true;
                }
            }
            return false;
        }
    }

    /* compiled from: DefaultTrackSelector.java */
    /* loaded from: classes.dex */
    public static final class c implements Comparable<c> {
        public final boolean a;
        public final boolean f0;

        public c(j jVar, int i) {
            this.a = (jVar.h0 & 1) != 0;
            this.f0 = b.K(i, false);
        }

        @Override // java.lang.Comparable
        /* renamed from: a */
        public int compareTo(c cVar) {
            return k30.i().f(this.f0, cVar.f0).f(this.a, cVar.a).h();
        }
    }

    /* compiled from: DefaultTrackSelector.java */
    /* loaded from: classes.dex */
    public static final class d extends x {
        public static final d U0 = new a().A();
        public final boolean F0;
        public final boolean G0;
        public final boolean H0;
        public final boolean I0;
        public final boolean J0;
        public final boolean K0;
        public final boolean L0;
        public final boolean M0;
        public final boolean N0;
        public final boolean O0;
        public final boolean P0;
        public final boolean Q0;
        public final boolean R0;
        public final SparseArray<Map<c84, e>> S0;
        public final SparseBooleanArray T0;

        /* compiled from: DefaultTrackSelector.java */
        /* loaded from: classes.dex */
        public static final class a extends x.a {
            public boolean A;
            public boolean B;
            public boolean C;
            public boolean D;
            public boolean E;
            public boolean F;
            public boolean G;
            public boolean H;
            public boolean I;
            public boolean J;
            public boolean K;
            public boolean L;
            public boolean M;
            public final SparseArray<Map<c84, e>> N;
            public final SparseBooleanArray O;

            public static SparseArray<Map<c84, e>> c0(SparseArray<Map<c84, e>> sparseArray) {
                SparseArray<Map<c84, e>> sparseArray2 = new SparseArray<>();
                for (int i = 0; i < sparseArray.size(); i++) {
                    sparseArray2.put(sparseArray.keyAt(i), new HashMap(sparseArray.valueAt(i)));
                }
                return sparseArray2;
            }

            @Override // androidx.media3.common.x.a
            /* renamed from: a0 */
            public d A() {
                return new d(this);
            }

            @Override // androidx.media3.common.x.a
            /* renamed from: b0 */
            public a B(int i) {
                super.B(i);
                return this;
            }

            public final void d0() {
                this.A = true;
                this.B = false;
                this.C = true;
                this.D = false;
                this.E = true;
                this.F = false;
                this.G = false;
                this.H = false;
                this.I = false;
                this.J = true;
                this.K = true;
                this.L = false;
                this.M = true;
            }

            public a e0(x xVar) {
                super.D(xVar);
                return this;
            }

            @Override // androidx.media3.common.x.a
            /* renamed from: f0 */
            public a E(int i) {
                super.E(i);
                return this;
            }

            @Override // androidx.media3.common.x.a
            /* renamed from: g0 */
            public a F(w wVar) {
                super.F(wVar);
                return this;
            }

            @Override // androidx.media3.common.x.a
            /* renamed from: h0 */
            public a G(Context context) {
                super.G(context);
                return this;
            }

            @Override // androidx.media3.common.x.a
            /* renamed from: i0 */
            public a I(int i, boolean z) {
                super.I(i, z);
                return this;
            }

            @Override // androidx.media3.common.x.a
            /* renamed from: j0 */
            public a J(int i, int i2, boolean z) {
                super.J(i, i2, z);
                return this;
            }

            @Override // androidx.media3.common.x.a
            /* renamed from: k0 */
            public a K(Context context, boolean z) {
                super.K(context, z);
                return this;
            }

            @Deprecated
            public a() {
                this.N = new SparseArray<>();
                this.O = new SparseBooleanArray();
                d0();
            }

            public a(Context context) {
                super(context);
                this.N = new SparseArray<>();
                this.O = new SparseBooleanArray();
                d0();
            }

            public a(d dVar) {
                super(dVar);
                this.A = dVar.F0;
                this.B = dVar.G0;
                this.C = dVar.H0;
                this.D = dVar.I0;
                this.E = dVar.J0;
                this.F = dVar.K0;
                this.G = dVar.L0;
                this.H = dVar.M0;
                this.I = dVar.N0;
                this.J = dVar.O0;
                this.K = dVar.P0;
                this.L = dVar.Q0;
                this.M = dVar.R0;
                this.N = c0(dVar.S0);
                this.O = dVar.T0.clone();
            }
        }

        public static boolean e(SparseBooleanArray sparseBooleanArray, SparseBooleanArray sparseBooleanArray2) {
            int size = sparseBooleanArray.size();
            if (sparseBooleanArray2.size() != size) {
                return false;
            }
            for (int i = 0; i < size; i++) {
                if (sparseBooleanArray2.indexOfKey(sparseBooleanArray.keyAt(i)) < 0) {
                    return false;
                }
            }
            return true;
        }

        public static boolean f(SparseArray<Map<c84, e>> sparseArray, SparseArray<Map<c84, e>> sparseArray2) {
            int size = sparseArray.size();
            if (sparseArray2.size() != size) {
                return false;
            }
            for (int i = 0; i < size; i++) {
                int indexOfKey = sparseArray2.indexOfKey(sparseArray.keyAt(i));
                if (indexOfKey < 0 || !g(sparseArray.valueAt(i), sparseArray2.valueAt(indexOfKey))) {
                    return false;
                }
            }
            return true;
        }

        /* JADX WARN: Removed duplicated region for block: B:8:0x001a  */
        /*
            Code decompiled incorrectly, please refer to instructions dump.
            To view partially-correct code enable 'Show inconsistent code' option in preferences
        */
        public static boolean g(java.util.Map<defpackage.c84, androidx.media3.exoplayer.trackselection.b.e> r4, java.util.Map<defpackage.c84, androidx.media3.exoplayer.trackselection.b.e> r5) {
            /*
                int r0 = r4.size()
                int r1 = r5.size()
                r2 = 0
                if (r1 == r0) goto Lc
                return r2
            Lc:
                java.util.Set r4 = r4.entrySet()
                java.util.Iterator r4 = r4.iterator()
            L14:
                boolean r0 = r4.hasNext()
                if (r0 == 0) goto L3b
                java.lang.Object r0 = r4.next()
                java.util.Map$Entry r0 = (java.util.Map.Entry) r0
                java.lang.Object r1 = r0.getKey()
                c84 r1 = (defpackage.c84) r1
                boolean r3 = r5.containsKey(r1)
                if (r3 == 0) goto L3a
                java.lang.Object r0 = r0.getValue()
                java.lang.Object r1 = r5.get(r1)
                boolean r0 = androidx.media3.common.util.b.c(r0, r1)
                if (r0 != 0) goto L14
            L3a:
                return r2
            L3b:
                r4 = 1
                return r4
            */
            throw new UnsupportedOperationException("Method not decompiled: androidx.media3.exoplayer.trackselection.b.d.g(java.util.Map, java.util.Map):boolean");
        }

        public static d i(Context context) {
            return new a(context).A();
        }

        public static int[] j(SparseBooleanArray sparseBooleanArray) {
            int[] iArr = new int[sparseBooleanArray.size()];
            for (int i = 0; i < sparseBooleanArray.size(); i++) {
                iArr[i] = sparseBooleanArray.keyAt(i);
            }
            return iArr;
        }

        public static void n(Bundle bundle, SparseArray<Map<c84, e>> sparseArray) {
            ArrayList arrayList = new ArrayList();
            ArrayList arrayList2 = new ArrayList();
            SparseArray sparseArray2 = new SparseArray();
            for (int i = 0; i < sparseArray.size(); i++) {
                int keyAt = sparseArray.keyAt(i);
                for (Map.Entry<c84, e> entry : sparseArray.valueAt(i).entrySet()) {
                    e value = entry.getValue();
                    if (value != null) {
                        sparseArray2.put(arrayList2.size(), value);
                    }
                    arrayList2.add(entry.getKey());
                    arrayList.add(Integer.valueOf(keyAt));
                }
                bundle.putIntArray(x.b(1010), Ints.k(arrayList));
                bundle.putParcelableArrayList(x.b(1011), is.c(arrayList2));
                bundle.putSparseParcelableArray(x.b(1012), is.d(sparseArray2));
            }
        }

        @Override // androidx.media3.common.x
        public boolean equals(Object obj) {
            if (this == obj) {
                return true;
            }
            if (obj == null || d.class != obj.getClass()) {
                return false;
            }
            d dVar = (d) obj;
            return super.equals(dVar) && this.F0 == dVar.F0 && this.G0 == dVar.G0 && this.H0 == dVar.H0 && this.I0 == dVar.I0 && this.J0 == dVar.J0 && this.K0 == dVar.K0 && this.L0 == dVar.L0 && this.M0 == dVar.M0 && this.N0 == dVar.N0 && this.O0 == dVar.O0 && this.P0 == dVar.P0 && this.Q0 == dVar.Q0 && this.R0 == dVar.R0 && e(this.T0, dVar.T0) && f(this.S0, dVar.S0);
        }

        @Override // androidx.media3.common.x
        /* renamed from: h */
        public a a() {
            return new a();
        }

        @Override // androidx.media3.common.x
        public int hashCode() {
            return ((((((((((((((((((((((((((super.hashCode() + 31) * 31) + (this.F0 ? 1 : 0)) * 31) + (this.G0 ? 1 : 0)) * 31) + (this.H0 ? 1 : 0)) * 31) + (this.I0 ? 1 : 0)) * 31) + (this.J0 ? 1 : 0)) * 31) + (this.K0 ? 1 : 0)) * 31) + (this.L0 ? 1 : 0)) * 31) + (this.M0 ? 1 : 0)) * 31) + (this.N0 ? 1 : 0)) * 31) + (this.O0 ? 1 : 0)) * 31) + (this.P0 ? 1 : 0)) * 31) + (this.Q0 ? 1 : 0)) * 31) + (this.R0 ? 1 : 0);
        }

        public boolean k(int i) {
            return this.T0.get(i);
        }

        @Deprecated
        public e l(int i, c84 c84Var) {
            Map<c84, e> map = this.S0.get(i);
            if (map != null) {
                return map.get(c84Var);
            }
            return null;
        }

        @Deprecated
        public boolean m(int i, c84 c84Var) {
            Map<c84, e> map = this.S0.get(i);
            return map != null && map.containsKey(c84Var);
        }

        @Override // androidx.media3.common.x, androidx.media3.common.e
        public Bundle toBundle() {
            Bundle bundle = super.toBundle();
            bundle.putBoolean(x.b(1000), this.F0);
            bundle.putBoolean(x.b(1001), this.G0);
            bundle.putBoolean(x.b(PlaybackException.ERROR_CODE_BEHIND_LIVE_WINDOW), this.H0);
            bundle.putBoolean(x.b(1014), this.I0);
            bundle.putBoolean(x.b(PlaybackException.ERROR_CODE_TIMEOUT), this.J0);
            bundle.putBoolean(x.b(PlaybackException.ERROR_CODE_FAILED_RUNTIME_CHECK), this.K0);
            bundle.putBoolean(x.b(WebSocketProtocol.CLOSE_NO_STATUS_CODE), this.L0);
            bundle.putBoolean(x.b(1006), this.M0);
            bundle.putBoolean(x.b(1015), this.N0);
            bundle.putBoolean(x.b(1016), this.O0);
            bundle.putBoolean(x.b(1007), this.P0);
            bundle.putBoolean(x.b(1008), this.Q0);
            bundle.putBoolean(x.b(1009), this.R0);
            n(bundle, this.S0);
            bundle.putIntArray(x.b(1013), j(this.T0));
            return bundle;
        }

        public d(a aVar) {
            super(aVar);
            this.F0 = aVar.A;
            this.G0 = aVar.B;
            this.H0 = aVar.C;
            this.I0 = aVar.D;
            this.J0 = aVar.E;
            this.K0 = aVar.F;
            this.L0 = aVar.G;
            this.M0 = aVar.H;
            this.N0 = aVar.I;
            this.O0 = aVar.J;
            this.P0 = aVar.K;
            this.Q0 = aVar.L;
            this.R0 = aVar.M;
            this.S0 = aVar.N;
            this.T0 = aVar.O;
        }
    }

    /* compiled from: DefaultTrackSelector.java */
    /* loaded from: classes.dex */
    public static final class e implements androidx.media3.common.e {
        public static final e.a<e> h0 = jl0.a;
        public final int a;
        public final int[] f0;
        public final int g0;

        public e(int i, int[] iArr, int i2) {
            this.a = i;
            int[] copyOf = Arrays.copyOf(iArr, iArr.length);
            this.f0 = copyOf;
            this.g0 = i2;
            Arrays.sort(copyOf);
        }

        public static String b(int i) {
            return Integer.toString(i, 36);
        }

        public static /* synthetic */ e c(Bundle bundle) {
            boolean z = false;
            int i = bundle.getInt(b(0), -1);
            int[] intArray = bundle.getIntArray(b(1));
            int i2 = bundle.getInt(b(2), -1);
            if (i >= 0 && i2 >= 0) {
                z = true;
            }
            ii.a(z);
            ii.e(intArray);
            return new e(i, intArray, i2);
        }

        public boolean equals(Object obj) {
            if (this == obj) {
                return true;
            }
            if (obj == null || e.class != obj.getClass()) {
                return false;
            }
            e eVar = (e) obj;
            return this.a == eVar.a && Arrays.equals(this.f0, eVar.f0) && this.g0 == eVar.g0;
        }

        public int hashCode() {
            return (((this.a * 31) + Arrays.hashCode(this.f0)) * 31) + this.g0;
        }

        @Override // androidx.media3.common.e
        public Bundle toBundle() {
            Bundle bundle = new Bundle();
            bundle.putInt(b(0), this.a);
            bundle.putIntArray(b(1), this.f0);
            bundle.putInt(b(2), this.g0);
            return bundle;
        }
    }

    /* compiled from: DefaultTrackSelector.java */
    /* loaded from: classes.dex */
    public static class f {
        public final Spatializer a;
        public final boolean b;
        public Handler c;
        public Spatializer.OnSpatializerStateChangedListener d;

        /* compiled from: DefaultTrackSelector.java */
        /* loaded from: classes.dex */
        public class a implements Spatializer.OnSpatializerStateChangedListener {
            public a(f fVar, b bVar) {
            }
        }

        public f(Spatializer spatializer) {
            this.a = spatializer;
            this.b = spatializer.getImmersiveAudioLevel() != 0;
        }

        public static f g(Context context) {
            AudioManager audioManager = (AudioManager) context.getSystemService("audio");
            if (audioManager == null) {
                return null;
            }
            return new f(audioManager.getSpatializer());
        }

        public boolean a(androidx.media3.common.b bVar, j jVar) {
            AudioFormat.Builder channelMask = new AudioFormat.Builder().setEncoding(2).setChannelMask(androidx.media3.common.util.b.E(("audio/eac3-joc".equals(jVar.p0) && jVar.C0 == 16) ? 12 : jVar.C0));
            int i = jVar.D0;
            if (i != -1) {
                channelMask.setSampleRate(i);
            }
            return this.a.canBeSpatialized(bVar.a().a, channelMask.build());
        }

        public void b(b bVar, Looper looper) {
            if (this.d == null && this.c == null) {
                this.d = new a(this, bVar);
                Handler handler = new Handler(looper);
                this.c = handler;
                Spatializer spatializer = this.a;
                Objects.requireNonNull(handler);
                spatializer.addOnSpatializerStateChangedListener(new zh0(handler), this.d);
            }
        }

        public boolean c() {
            return this.a.isAvailable();
        }

        public boolean d() {
            return this.a.isEnabled();
        }

        public boolean e() {
            return this.b;
        }

        public void f() {
            Spatializer.OnSpatializerStateChangedListener onSpatializerStateChangedListener = this.d;
            if (onSpatializerStateChangedListener == null || this.c == null) {
                return;
            }
            this.a.removeOnSpatializerStateChangedListener(onSpatializerStateChangedListener);
            ((Handler) androidx.media3.common.util.b.j(this.c)).removeCallbacksAndMessages(null);
            this.c = null;
            this.d = null;
        }
    }

    /* compiled from: DefaultTrackSelector.java */
    /* loaded from: classes.dex */
    public static final class g extends h<g> implements Comparable<g> {
        public final int i0;
        public final boolean j0;
        public final boolean k0;
        public final boolean l0;
        public final int m0;
        public final int n0;
        public final int o0;
        public final int p0;
        public final boolean q0;

        public g(int i, v vVar, int i2, d dVar, int i3, String str) {
            super(i, vVar, i2);
            ImmutableList<String> immutableList;
            int i4;
            int i5 = 0;
            this.j0 = b.K(i3, false);
            int i6 = this.h0.h0 & (~dVar.y0);
            this.k0 = (i6 & 1) != 0;
            this.l0 = (i6 & 2) != 0;
            int i7 = Integer.MAX_VALUE;
            if (dVar.w0.isEmpty()) {
                immutableList = ImmutableList.of("");
            } else {
                immutableList = dVar.w0;
            }
            int i8 = 0;
            while (true) {
                if (i8 >= immutableList.size()) {
                    i4 = 0;
                    break;
                }
                i4 = b.C(this.h0, immutableList.get(i8), dVar.z0);
                if (i4 > 0) {
                    i7 = i8;
                    break;
                }
                i8++;
            }
            this.m0 = i7;
            this.n0 = i4;
            int G = b.G(this.h0.i0, dVar.x0);
            this.o0 = G;
            this.q0 = (this.h0.i0 & 1088) != 0;
            int C = b.C(this.h0, str, b.S(str) == null);
            this.p0 = C;
            boolean z = i4 > 0 || (dVar.w0.isEmpty() && G > 0) || this.k0 || (this.l0 && C > 0);
            if (b.K(i3, dVar.P0) && z) {
                i5 = 1;
            }
            this.i0 = i5;
        }

        public static int e(List<g> list, List<g> list2) {
            return list.get(0).compareTo(list2.get(0));
        }

        public static ImmutableList<g> g(int i, v vVar, d dVar, int[] iArr, String str) {
            ImmutableList.a builder = ImmutableList.builder();
            for (int i2 = 0; i2 < vVar.a; i2++) {
                builder.a(new g(i, vVar, i2, dVar, iArr[i2], str));
            }
            return builder.l();
        }

        @Override // androidx.media3.exoplayer.trackselection.b.h
        public int a() {
            return this.i0;
        }

        @Override // java.lang.Comparable
        /* renamed from: f */
        public int compareTo(g gVar) {
            k30 d = k30.i().f(this.j0, gVar.j0).e(Integer.valueOf(this.m0), Integer.valueOf(gVar.m0), Ordering.natural().reverse()).d(this.n0, gVar.n0).d(this.o0, gVar.o0).f(this.k0, gVar.k0).e(Boolean.valueOf(this.l0), Boolean.valueOf(gVar.l0), this.n0 == 0 ? Ordering.natural() : Ordering.natural().reverse()).d(this.p0, gVar.p0);
            if (this.o0 == 0) {
                d = d.g(this.q0, gVar.q0);
            }
            return d.h();
        }

        @Override // androidx.media3.exoplayer.trackselection.b.h
        /* renamed from: h */
        public boolean d(g gVar) {
            return false;
        }
    }

    /* compiled from: DefaultTrackSelector.java */
    /* loaded from: classes.dex */
    public static abstract class h<T extends h<T>> {
        public final int a;
        public final v f0;
        public final int g0;
        public final j h0;

        /* compiled from: DefaultTrackSelector.java */
        /* loaded from: classes.dex */
        public interface a<T extends h<T>> {
            List<T> a(int i, v vVar, int[] iArr);
        }

        public h(int i, v vVar, int i2) {
            this.a = i;
            this.f0 = vVar;
            this.g0 = i2;
            this.h0 = vVar.c(i2);
        }

        public abstract int a();

        public abstract boolean d(T t);
    }

    /* compiled from: DefaultTrackSelector.java */
    /* loaded from: classes.dex */
    public static final class i extends h<i> {
        public final boolean i0;
        public final d j0;
        public final boolean k0;
        public final boolean l0;
        public final int m0;
        public final int n0;
        public final int o0;
        public final int p0;
        public final boolean q0;
        public final boolean r0;
        public final int s0;
        public final boolean t0;
        public final boolean u0;
        public final int v0;

        /* JADX WARN: Removed duplicated region for block: B:54:0x00a0  */
        /* JADX WARN: Removed duplicated region for block: B:62:0x00b5  */
        /* JADX WARN: Removed duplicated region for block: B:70:0x00d6  */
        /* JADX WARN: Removed duplicated region for block: B:71:0x00d8  */
        /* JADX WARN: Removed duplicated region for block: B:75:0x00e4  */
        /* JADX WARN: Removed duplicated region for block: B:78:0x00cc A[EDGE_INSN: B:78:0x00cc->B:68:0x00cc ?: BREAK  , SYNTHETIC] */
        /*
            Code decompiled incorrectly, please refer to instructions dump.
            To view partially-correct code enable 'Show inconsistent code' option in preferences
        */
        public i(int r5, androidx.media3.common.v r6, int r7, androidx.media3.exoplayer.trackselection.b.d r8, int r9, int r10, boolean r11) {
            /*
                Method dump skipped, instructions count: 248
                To view this dump change 'Code comments level' option to 'DEBUG'
            */
            throw new UnsupportedOperationException("Method not decompiled: androidx.media3.exoplayer.trackselection.b.i.<init>(int, androidx.media3.common.v, int, androidx.media3.exoplayer.trackselection.b$d, int, int, boolean):void");
        }

        public static int g(i iVar, i iVar2) {
            k30 f = k30.i().f(iVar.l0, iVar2.l0).d(iVar.p0, iVar2.p0).f(iVar.q0, iVar2.q0).f(iVar.i0, iVar2.i0).f(iVar.k0, iVar2.k0).e(Integer.valueOf(iVar.o0), Integer.valueOf(iVar2.o0), Ordering.natural().reverse()).f(iVar.t0, iVar2.t0).f(iVar.u0, iVar2.u0);
            if (iVar.t0 && iVar.u0) {
                f = f.d(iVar.v0, iVar2.v0);
            }
            return f.h();
        }

        public static int h(i iVar, i iVar2) {
            Ordering reverse = (iVar.i0 && iVar.l0) ? b.j : b.j.reverse();
            return k30.i().e(Integer.valueOf(iVar.m0), Integer.valueOf(iVar2.m0), iVar.j0.A0 ? b.j.reverse() : b.k).e(Integer.valueOf(iVar.n0), Integer.valueOf(iVar2.n0), reverse).e(Integer.valueOf(iVar.m0), Integer.valueOf(iVar2.m0), reverse).h();
        }

        public static int j(List<i> list, List<i> list2) {
            return k30.i().e((i) Collections.max(list, ll0.a), (i) Collections.max(list2, ll0.a), ll0.a).d(list.size(), list2.size()).e((i) Collections.max(list, kl0.a), (i) Collections.max(list2, kl0.a), kl0.a).h();
        }

        public static ImmutableList<i> k(int i, v vVar, d dVar, int[] iArr, int i2) {
            int D = b.D(vVar, dVar.m0, dVar.n0, dVar.o0);
            ImmutableList.a builder = ImmutableList.builder();
            for (int i3 = 0; i3 < vVar.a; i3++) {
                int f = vVar.c(i3).f();
                builder.a(new i(i, vVar, i3, dVar, iArr[i3], i2, D == Integer.MAX_VALUE || (f != -1 && f <= D)));
            }
            return builder.l();
        }

        @Override // androidx.media3.exoplayer.trackselection.b.h
        public int a() {
            return this.s0;
        }

        public final int l(int i, int i2) {
            if ((this.h0.i0 & Http2.INITIAL_MAX_FRAME_SIZE) == 0 && b.K(i, this.j0.P0)) {
                if (this.i0 || this.j0.F0) {
                    if (b.K(i, false) && this.k0 && this.i0 && this.h0.l0 != -1) {
                        d dVar = this.j0;
                        if (!dVar.B0 && !dVar.A0 && (i & i2) != 0) {
                            return 2;
                        }
                    }
                    return 1;
                }
                return 0;
            }
            return 0;
        }

        @Override // androidx.media3.exoplayer.trackselection.b.h
        /* renamed from: o */
        public boolean d(i iVar) {
            return (this.r0 || androidx.media3.common.util.b.c(this.h0.p0, iVar.h0.p0)) && (this.j0.I0 || (this.t0 == iVar.t0 && this.u0 == iVar.u0));
        }
    }

    public b(Context context) {
        this(context, new a.b());
    }

    public static void A(d.a aVar, x xVar, c.a[] aVarArr) {
        int d2 = aVar.d();
        HashMap hashMap = new HashMap();
        for (int i2 = 0; i2 < d2; i2++) {
            B(aVar.f(i2), xVar, hashMap);
        }
        B(aVar.h(), xVar, hashMap);
        for (int i3 = 0; i3 < d2; i3++) {
            w wVar = (w) hashMap.get(Integer.valueOf(aVar.e(i3)));
            if (wVar != null) {
                aVarArr[i3] = (wVar.f0.isEmpty() || aVar.f(i3).c(wVar.a) == -1) ? null : new c.a(wVar.a, Ints.k(wVar.f0));
            }
        }
    }

    public static void B(c84 c84Var, x xVar, Map<Integer, w> map) {
        w wVar;
        for (int i2 = 0; i2 < c84Var.a; i2++) {
            w wVar2 = xVar.C0.get(c84Var.b(i2));
            if (wVar2 != null && ((wVar = map.get(Integer.valueOf(wVar2.b()))) == null || (wVar.f0.isEmpty() && !wVar2.f0.isEmpty()))) {
                map.put(Integer.valueOf(wVar2.b()), wVar2);
            }
        }
    }

    public static int C(j jVar, String str, boolean z) {
        if (TextUtils.isEmpty(str) || !str.equals(jVar.g0)) {
            String S = S(str);
            String S2 = S(jVar.g0);
            if (S2 == null || S == null) {
                return (z && S2 == null) ? 1 : 0;
            } else if (S2.startsWith(S) || S.startsWith(S2)) {
                return 3;
            } else {
                return androidx.media3.common.util.b.M0(S2, "-")[0].equals(androidx.media3.common.util.b.M0(S, "-")[0]) ? 2 : 0;
            }
        }
        return 4;
    }

    public static int D(v vVar, int i2, int i3, boolean z) {
        int i4;
        int i5 = Integer.MAX_VALUE;
        if (i2 != Integer.MAX_VALUE && i3 != Integer.MAX_VALUE) {
            for (int i6 = 0; i6 < vVar.a; i6++) {
                j c2 = vVar.c(i6);
                int i7 = c2.u0;
                if (i7 > 0 && (i4 = c2.v0) > 0) {
                    Point E = E(z, i2, i3, i7, i4);
                    int i8 = c2.u0;
                    int i9 = c2.v0;
                    int i10 = i8 * i9;
                    if (i8 >= ((int) (E.x * 0.98f)) && i9 >= ((int) (E.y * 0.98f)) && i10 < i5) {
                        i5 = i10;
                    }
                }
            }
        }
        return i5;
    }

    /* JADX WARN: Code restructure failed: missing block: B:10:0x000d, code lost:
        if ((r6 > r7) != (r4 > r5)) goto L8;
     */
    /*
        Code decompiled incorrectly, please refer to instructions dump.
        To view partially-correct code enable 'Show inconsistent code' option in preferences
    */
    public static android.graphics.Point E(boolean r3, int r4, int r5, int r6, int r7) {
        /*
            if (r3 == 0) goto L10
            r3 = 1
            r0 = 0
            if (r6 <= r7) goto L8
            r1 = r3
            goto L9
        L8:
            r1 = r0
        L9:
            if (r4 <= r5) goto Lc
            goto Ld
        Lc:
            r3 = r0
        Ld:
            if (r1 == r3) goto L10
            goto L13
        L10:
            r2 = r5
            r5 = r4
            r4 = r2
        L13:
            int r3 = r6 * r4
            int r0 = r7 * r5
            if (r3 < r0) goto L23
            android.graphics.Point r3 = new android.graphics.Point
            int r4 = androidx.media3.common.util.b.l(r0, r6)
            r3.<init>(r5, r4)
            return r3
        L23:
            android.graphics.Point r5 = new android.graphics.Point
            int r3 = androidx.media3.common.util.b.l(r3, r7)
            r5.<init>(r3, r4)
            return r5
        */
        throw new UnsupportedOperationException("Method not decompiled: androidx.media3.exoplayer.trackselection.b.E(boolean, int, int, int, int):android.graphics.Point");
    }

    public static int G(int i2, int i3) {
        if (i2 == 0 || i2 != i3) {
            return Integer.bitCount(i2 & i3);
        }
        return Integer.MAX_VALUE;
    }

    public static int H(String str) {
        if (str == null) {
            return 0;
        }
        char c2 = 65535;
        switch (str.hashCode()) {
            case -1662735862:
                if (str.equals("video/av01")) {
                    c2 = 0;
                    break;
                }
                break;
            case -1662541442:
                if (str.equals("video/hevc")) {
                    c2 = 1;
                    break;
                }
                break;
            case 1331836730:
                if (str.equals("video/avc")) {
                    c2 = 2;
                    break;
                }
                break;
            case 1599127257:
                if (str.equals("video/x-vnd.on2.vp9")) {
                    c2 = 3;
                    break;
                }
                break;
        }
        switch (c2) {
            case 0:
                return 4;
            case 1:
                return 3;
            case 2:
                return 1;
            case 3:
                return 2;
            default:
                return 0;
        }
    }

    public static boolean J(j jVar) {
        String str = jVar.p0;
        if (str == null) {
            return false;
        }
        str.hashCode();
        char c2 = 65535;
        switch (str.hashCode()) {
            case -2123537834:
                if (str.equals("audio/eac3-joc")) {
                    c2 = 0;
                    break;
                }
                break;
            case 187078296:
                if (str.equals("audio/ac3")) {
                    c2 = 1;
                    break;
                }
                break;
            case 187078297:
                if (str.equals("audio/ac4")) {
                    c2 = 2;
                    break;
                }
                break;
            case 1504578661:
                if (str.equals("audio/eac3")) {
                    c2 = 3;
                    break;
                }
                break;
        }
        switch (c2) {
            case 0:
            case 1:
            case 2:
            case 3:
                return true;
            default:
                return false;
        }
    }

    public static boolean K(int i2, boolean z) {
        int f2 = u63.f(i2);
        return f2 == 4 || (z && f2 == 3);
    }

    /* JADX INFO: Access modifiers changed from: private */
    public /* synthetic */ List L(d dVar, boolean z, int i2, v vVar, int[] iArr) {
        return C0041b.g(i2, vVar, dVar, iArr, z, new gu2() { // from class: al0
            @Override // defpackage.gu2
            public final boolean apply(Object obj) {
                boolean I;
                I = b.this.I((j) obj);
                return I;
            }
        });
    }

    public static /* synthetic */ List N(d dVar, int[] iArr, int i2, v vVar, int[] iArr2) {
        return i.k(i2, vVar, dVar, iArr2, iArr[i2]);
    }

    public static /* synthetic */ int O(Integer num, Integer num2) {
        if (num.intValue() == -1) {
            return num2.intValue() == -1 ? 0 : -1;
        } else if (num2.intValue() == -1) {
            return 1;
        } else {
            return num.intValue() - num2.intValue();
        }
    }

    public static /* synthetic */ int P(Integer num, Integer num2) {
        return 0;
    }

    public static void Q(d.a aVar, int[][][] iArr, v63[] v63VarArr, androidx.media3.exoplayer.trackselection.c[] cVarArr) {
        boolean z;
        boolean z2 = false;
        int i2 = -1;
        int i3 = -1;
        for (int i4 = 0; i4 < aVar.d(); i4++) {
            int e2 = aVar.e(i4);
            androidx.media3.exoplayer.trackselection.c cVar = cVarArr[i4];
            if ((e2 == 1 || e2 == 2) && cVar != null && T(iArr[i4], aVar.f(i4), cVar)) {
                if (e2 == 1) {
                    if (i3 != -1) {
                        z = false;
                        break;
                    }
                    i3 = i4;
                } else if (i2 != -1) {
                    z = false;
                    break;
                } else {
                    i2 = i4;
                }
            }
        }
        z = true;
        if (i3 != -1 && i2 != -1) {
            z2 = true;
        }
        if (z && z2) {
            v63 v63Var = new v63(true);
            v63VarArr[i3] = v63Var;
            v63VarArr[i2] = v63Var;
        }
    }

    public static String S(String str) {
        if (TextUtils.isEmpty(str) || TextUtils.equals(str, "und")) {
            return null;
        }
        return str;
    }

    public static boolean T(int[][] iArr, c84 c84Var, androidx.media3.exoplayer.trackselection.c cVar) {
        if (cVar == null) {
            return false;
        }
        int c2 = c84Var.c(cVar.c());
        for (int i2 = 0; i2 < cVar.length(); i2++) {
            if (u63.h(iArr[c2][cVar.l(i2)]) != 32) {
                return false;
            }
        }
        return true;
    }

    public static void z(d.a aVar, d dVar, c.a[] aVarArr) {
        int d2 = aVar.d();
        for (int i2 = 0; i2 < d2; i2++) {
            c84 f2 = aVar.f(i2);
            if (dVar.m(i2, f2)) {
                e l = dVar.l(i2, f2);
                aVarArr[i2] = (l == null || l.f0.length == 0) ? null : new c.a(f2.b(l.a), l.f0, l.g0);
            }
        }
    }

    @Override // androidx.media3.exoplayer.trackselection.f
    /* renamed from: F */
    public d b() {
        d dVar;
        synchronized (this.c) {
            dVar = this.g;
        }
        return dVar;
    }

    public final boolean I(j jVar) {
        boolean z;
        f fVar;
        f fVar2;
        synchronized (this.c) {
            z = !this.g.O0 || this.f || jVar.C0 <= 2 || (J(jVar) && (androidx.media3.common.util.b.a < 32 || (fVar2 = this.h) == null || !fVar2.e())) || (androidx.media3.common.util.b.a >= 32 && (fVar = this.h) != null && fVar.e() && this.h.c() && this.h.d() && this.h.a(this.i, jVar));
        }
        return z;
    }

    public final void R() {
        boolean z;
        f fVar;
        synchronized (this.c) {
            z = this.g.O0 && !this.f && androidx.media3.common.util.b.a >= 32 && (fVar = this.h) != null && fVar.e();
        }
        if (z) {
            d();
        }
    }

    public c.a[] U(d.a aVar, int[][][] iArr, int[] iArr2, d dVar) throws ExoPlaybackException {
        String str;
        int d2 = aVar.d();
        c.a[] aVarArr = new c.a[d2];
        Pair<c.a, Integer> Z = Z(aVar, iArr, iArr2, dVar);
        if (Z != null) {
            aVarArr[((Integer) Z.second).intValue()] = (c.a) Z.first;
        }
        Pair<c.a, Integer> V = V(aVar, iArr, iArr2, dVar);
        if (V != null) {
            aVarArr[((Integer) V.second).intValue()] = (c.a) V.first;
        }
        if (V == null) {
            str = null;
        } else {
            Object obj = V.first;
            str = ((c.a) obj).a.c(((c.a) obj).b[0]).g0;
        }
        Pair<c.a, Integer> X = X(aVar, iArr, dVar, str);
        if (X != null) {
            aVarArr[((Integer) X.second).intValue()] = (c.a) X.first;
        }
        for (int i2 = 0; i2 < d2; i2++) {
            int e2 = aVar.e(i2);
            if (e2 != 2 && e2 != 1 && e2 != 3) {
                aVarArr[i2] = W(e2, aVar.f(i2), iArr[i2], dVar);
            }
        }
        return aVarArr;
    }

    public Pair<c.a, Integer> V(d.a aVar, int[][][] iArr, int[] iArr2, final d dVar) throws ExoPlaybackException {
        final boolean z = false;
        int i2 = 0;
        while (true) {
            if (i2 < aVar.d()) {
                if (2 == aVar.e(i2) && aVar.f(i2).a > 0) {
                    z = true;
                    break;
                }
                i2++;
            } else {
                break;
            }
        }
        return Y(1, aVar, iArr, new h.a() { // from class: dl0
            @Override // androidx.media3.exoplayer.trackselection.b.h.a
            public final List a(int i3, v vVar, int[] iArr3) {
                List L;
                L = b.this.L(dVar, z, i3, vVar, iArr3);
                return L;
            }
        }, gl0.a);
    }

    public c.a W(int i2, c84 c84Var, int[][] iArr, d dVar) throws ExoPlaybackException {
        v vVar = null;
        c cVar = null;
        int i3 = 0;
        for (int i4 = 0; i4 < c84Var.a; i4++) {
            v b = c84Var.b(i4);
            int[] iArr2 = iArr[i4];
            for (int i5 = 0; i5 < b.a; i5++) {
                if (K(iArr2[i5], dVar.P0)) {
                    c cVar2 = new c(b.c(i5), iArr2[i5]);
                    if (cVar == null || cVar2.compareTo(cVar) > 0) {
                        vVar = b;
                        i3 = i5;
                        cVar = cVar2;
                    }
                }
            }
        }
        if (vVar == null) {
            return null;
        }
        return new c.a(vVar, i3);
    }

    public Pair<c.a, Integer> X(d.a aVar, int[][][] iArr, final d dVar, final String str) throws ExoPlaybackException {
        return Y(3, aVar, iArr, new h.a() { // from class: bl0
            @Override // androidx.media3.exoplayer.trackselection.b.h.a
            public final List a(int i2, v vVar, int[] iArr2) {
                List g2;
                g2 = b.g.g(i2, vVar, b.d.this, iArr2, str);
                return g2;
            }
        }, hl0.a);
    }

    public final <T extends h<T>> Pair<c.a, Integer> Y(int i2, d.a aVar, int[][][] iArr, h.a<T> aVar2, Comparator<List<T>> comparator) {
        int i3;
        RandomAccess randomAccess;
        d.a aVar3 = aVar;
        ArrayList arrayList = new ArrayList();
        int d2 = aVar.d();
        int i4 = 0;
        while (i4 < d2) {
            if (i2 == aVar3.e(i4)) {
                c84 f2 = aVar3.f(i4);
                for (int i5 = 0; i5 < f2.a; i5++) {
                    v b = f2.b(i5);
                    List<T> a2 = aVar2.a(i4, b, iArr[i4][i5]);
                    boolean[] zArr = new boolean[b.a];
                    int i6 = 0;
                    while (i6 < b.a) {
                        T t = a2.get(i6);
                        int a3 = t.a();
                        if (zArr[i6] || a3 == 0) {
                            i3 = d2;
                        } else {
                            if (a3 == 1) {
                                randomAccess = ImmutableList.of(t);
                                i3 = d2;
                            } else {
                                ArrayList arrayList2 = new ArrayList();
                                arrayList2.add(t);
                                int i7 = i6 + 1;
                                while (i7 < b.a) {
                                    T t2 = a2.get(i7);
                                    int i8 = d2;
                                    if (t2.a() == 2 && t.d(t2)) {
                                        arrayList2.add(t2);
                                        zArr[i7] = true;
                                    }
                                    i7++;
                                    d2 = i8;
                                }
                                i3 = d2;
                                randomAccess = arrayList2;
                            }
                            arrayList.add(randomAccess);
                        }
                        i6++;
                        d2 = i3;
                    }
                }
            }
            i4++;
            aVar3 = aVar;
            d2 = d2;
        }
        if (arrayList.isEmpty()) {
            return null;
        }
        List list = (List) Collections.max(arrayList, comparator);
        int[] iArr2 = new int[list.size()];
        for (int i9 = 0; i9 < list.size(); i9++) {
            iArr2[i9] = ((h) list.get(i9)).g0;
        }
        h hVar = (h) list.get(0);
        return Pair.create(new c.a(hVar.f0, iArr2), Integer.valueOf(hVar.a));
    }

    public Pair<c.a, Integer> Z(d.a aVar, int[][][] iArr, final int[] iArr2, final d dVar) throws ExoPlaybackException {
        return Y(2, aVar, iArr, new h.a() { // from class: cl0
            @Override // androidx.media3.exoplayer.trackselection.b.h.a
            public final List a(int i2, v vVar, int[] iArr3) {
                List N;
                N = b.N(b.d.this, iArr2, i2, vVar, iArr3);
                return N;
            }
        }, il0.a);
    }

    public final void a0(d dVar) {
        boolean z;
        ii.e(dVar);
        synchronized (this.c) {
            z = !this.g.equals(dVar);
            this.g = dVar;
        }
        if (z) {
            if (dVar.O0 && this.d == null) {
                p12.i("DefaultTrackSelector", "Audio channel count constraints cannot be applied without reference to Context. Build the track selector instance with one of the non-deprecated constructors that take a Context argument.");
            }
            d();
        }
    }

    @Override // androidx.media3.exoplayer.trackselection.f
    public boolean e() {
        return true;
    }

    @Override // androidx.media3.exoplayer.trackselection.f
    public void g() {
        f fVar;
        synchronized (this.c) {
            if (androidx.media3.common.util.b.a >= 32 && (fVar = this.h) != null) {
                fVar.f();
            }
        }
        super.g();
    }

    @Override // androidx.media3.exoplayer.trackselection.f
    public void i(androidx.media3.common.b bVar) {
        boolean z;
        synchronized (this.c) {
            z = !this.i.equals(bVar);
            this.i = bVar;
        }
        if (z) {
            R();
        }
    }

    @Override // androidx.media3.exoplayer.trackselection.f
    public void j(x xVar) {
        if (xVar instanceof d) {
            a0((d) xVar);
        }
        a0(new d.a().e0(xVar).A());
    }

    @Override // androidx.media3.exoplayer.trackselection.d
    public final Pair<RendererConfiguration[], androidx.media3.exoplayer.trackselection.c[]> n(d.a aVar, int[][][] iArr, int[] iArr2, j.b bVar, u uVar) throws ExoPlaybackException {
        d dVar;
        f fVar;
        synchronized (this.c) {
            dVar = this.g;
            if (dVar.O0 && androidx.media3.common.util.b.a >= 32 && (fVar = this.h) != null) {
                fVar.b(this, (Looper) ii.i(Looper.myLooper()));
            }
        }
        int d2 = aVar.d();
        c.a[] U = U(aVar, iArr, iArr2, dVar);
        A(aVar, dVar, U);
        z(aVar, dVar, U);
        for (int i2 = 0; i2 < d2; i2++) {
            int e2 = aVar.e(i2);
            if (dVar.k(i2) || dVar.D0.contains(Integer.valueOf(e2))) {
                U[i2] = null;
            }
        }
        androidx.media3.exoplayer.trackselection.c[] a2 = this.e.a(U, a(), bVar, uVar);
        v63[] v63VarArr = new v63[d2];
        for (int i3 = 0; i3 < d2; i3++) {
            boolean z = true;
            if ((dVar.k(i3) || dVar.D0.contains(Integer.valueOf(aVar.e(i3)))) || (aVar.e(i3) != -2 && a2[i3] == null)) {
                z = false;
            }
            v63VarArr[i3] = z ? v63.b : null;
        }
        if (dVar.Q0) {
            Q(aVar, iArr, v63VarArr, a2);
        }
        return Pair.create(v63VarArr, a2);
    }

    public b(Context context, c.b bVar) {
        this(context, d.i(context), bVar);
    }

    public b(Context context, x xVar, c.b bVar) {
        this(xVar, bVar, context);
    }

    public b(x xVar, c.b bVar, Context context) {
        this.c = new Object();
        this.d = context != null ? context.getApplicationContext() : null;
        this.e = bVar;
        if (xVar instanceof d) {
            this.g = (d) xVar;
        } else {
            this.g = (context == null ? d.U0 : d.i(context)).a().e0(xVar).A();
        }
        this.i = androidx.media3.common.b.k0;
        boolean z = context != null && androidx.media3.common.util.b.t0(context);
        this.f = z;
        if (!z && context != null && androidx.media3.common.util.b.a >= 32) {
            this.h = f.g(context);
        }
        if (this.g.O0 && context == null) {
            p12.i("DefaultTrackSelector", "Audio channel count constraints cannot be applied without reference to Context. Build the track selector instance with one of the non-deprecated constructors that take a Context argument.");
        }
    }
}
