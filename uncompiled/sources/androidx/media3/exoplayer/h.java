package androidx.media3.exoplayer;

import android.os.Handler;
import android.os.HandlerThread;
import android.os.Looper;
import android.os.Message;
import android.util.Pair;
import androidx.media3.common.Metadata;
import androidx.media3.common.ParserException;
import androidx.media3.common.PlaybackException;
import androidx.media3.common.m;
import androidx.media3.common.p;
import androidx.media3.common.u;
import androidx.media3.datasource.DataSourceException;
import androidx.media3.exoplayer.drm.DrmSession;
import androidx.media3.exoplayer.e;
import androidx.media3.exoplayer.h;
import androidx.media3.exoplayer.j;
import androidx.media3.exoplayer.k;
import androidx.media3.exoplayer.m;
import androidx.media3.exoplayer.source.BehindLiveWindowException;
import androidx.media3.exoplayer.source.i;
import androidx.media3.exoplayer.source.j;
import androidx.media3.exoplayer.source.r;
import androidx.media3.exoplayer.trackselection.f;
import com.google.common.collect.ImmutableList;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Set;
import java.util.concurrent.atomic.AtomicBoolean;
import zendesk.support.request.CellBase;

/* compiled from: ExoPlayerImplInternal.java */
/* loaded from: classes.dex */
public final class h implements Handler.Callback, i.a, f.a, j.d, e.a, k.a {
    public xi3 A0;
    public lr2 B0;
    public e C0;
    public boolean D0;
    public boolean E0;
    public boolean F0;
    public boolean G0;
    public boolean H0;
    public int I0;
    public boolean J0;
    public boolean K0;
    public boolean L0;
    public boolean M0;
    public int N0;
    public C0036h O0;
    public long P0;
    public int Q0;
    public boolean R0;
    public ExoPlaybackException S0;
    public long T0 = CellBase.ID_SYSTEM_MESSAGE_REQUEST_CLOSED;
    public final m[] a;
    public final Set<m> f0;
    public final n[] g0;
    public final androidx.media3.exoplayer.trackselection.f h0;
    public final androidx.media3.exoplayer.trackselection.g i0;
    public final s02 j0;
    public final gm k0;
    public final pj1 l0;
    public final HandlerThread m0;
    public final Looper n0;
    public final u.c o0;
    public final u.b p0;
    public final long q0;
    public final boolean r0;
    public final androidx.media3.exoplayer.e s0;
    public final ArrayList<d> t0;
    public final tz u0;
    public final f v0;
    public final m62 w0;
    public final j x0;
    public final r02 y0;
    public final long z0;

    /* compiled from: ExoPlayerImplInternal.java */
    /* loaded from: classes.dex */
    public class a implements m.a {
        public a() {
        }

        @Override // androidx.media3.exoplayer.m.a
        public void a() {
            h.this.L0 = true;
        }

        @Override // androidx.media3.exoplayer.m.a
        public void b() {
            h.this.l0.f(2);
        }
    }

    /* compiled from: ExoPlayerImplInternal.java */
    /* loaded from: classes.dex */
    public static final class b {
        public final List<j.c> a;
        public final qo3 b;
        public final int c;
        public final long d;

        public /* synthetic */ b(List list, qo3 qo3Var, int i, long j, a aVar) {
            this(list, qo3Var, i, j);
        }

        public b(List<j.c> list, qo3 qo3Var, int i, long j) {
            this.a = list;
            this.b = qo3Var;
            this.c = i;
            this.d = j;
        }
    }

    /* compiled from: ExoPlayerImplInternal.java */
    /* loaded from: classes.dex */
    public static class c {
        public final int a;
        public final int b;
        public final int c;
        public final qo3 d;
    }

    /* compiled from: ExoPlayerImplInternal.java */
    /* loaded from: classes.dex */
    public static final class d implements Comparable<d> {
        public final k a;
        public int f0;
        public long g0;
        public Object h0;

        public d(k kVar) {
            this.a = kVar;
        }

        @Override // java.lang.Comparable
        /* renamed from: a */
        public int compareTo(d dVar) {
            Object obj = this.h0;
            if ((obj == null) != (dVar.h0 == null)) {
                return obj != null ? -1 : 1;
            } else if (obj == null) {
                return 0;
            } else {
                int i = this.f0 - dVar.f0;
                return i != 0 ? i : androidx.media3.common.util.b.o(this.g0, dVar.g0);
            }
        }

        public void d(int i, long j, Object obj) {
            this.f0 = i;
            this.g0 = j;
            this.h0 = obj;
        }
    }

    /* compiled from: ExoPlayerImplInternal.java */
    /* loaded from: classes.dex */
    public static final class e {
        public boolean a;
        public lr2 b;
        public int c;
        public boolean d;
        public int e;
        public boolean f;
        public int g;

        public e(lr2 lr2Var) {
            this.b = lr2Var;
        }

        public void b(int i) {
            this.a |= i > 0;
            this.c += i;
        }

        public void c(int i) {
            this.a = true;
            this.f = true;
            this.g = i;
        }

        public void d(lr2 lr2Var) {
            this.a |= this.b != lr2Var;
            this.b = lr2Var;
        }

        public void e(int i) {
            if (this.d && this.e != 5) {
                ii.a(i == 5);
                return;
            }
            this.a = true;
            this.d = true;
            this.e = i;
        }
    }

    /* compiled from: ExoPlayerImplInternal.java */
    /* loaded from: classes.dex */
    public interface f {
        void a(e eVar);
    }

    /* compiled from: ExoPlayerImplInternal.java */
    /* loaded from: classes.dex */
    public static final class g {
        public final j.b a;
        public final long b;
        public final long c;
        public final boolean d;
        public final boolean e;
        public final boolean f;

        public g(j.b bVar, long j, long j2, boolean z, boolean z2, boolean z3) {
            this.a = bVar;
            this.b = j;
            this.c = j2;
            this.d = z;
            this.e = z2;
            this.f = z3;
        }
    }

    /* compiled from: ExoPlayerImplInternal.java */
    /* renamed from: androidx.media3.exoplayer.h$h  reason: collision with other inner class name */
    /* loaded from: classes.dex */
    public static final class C0036h {
        public final u a;
        public final int b;
        public final long c;

        public C0036h(u uVar, int i, long j) {
            this.a = uVar;
            this.b = i;
            this.c = j;
        }
    }

    public h(m[] mVarArr, androidx.media3.exoplayer.trackselection.f fVar, androidx.media3.exoplayer.trackselection.g gVar, s02 s02Var, gm gmVar, int i, boolean z, jb jbVar, xi3 xi3Var, r02 r02Var, long j, boolean z2, Looper looper, tz tzVar, f fVar2, ks2 ks2Var) {
        this.v0 = fVar2;
        this.a = mVarArr;
        this.h0 = fVar;
        this.i0 = gVar;
        this.j0 = s02Var;
        this.k0 = gmVar;
        this.I0 = i;
        this.J0 = z;
        this.A0 = xi3Var;
        this.y0 = r02Var;
        this.z0 = j;
        this.E0 = z2;
        this.u0 = tzVar;
        this.q0 = s02Var.b();
        this.r0 = s02Var.a();
        lr2 j2 = lr2.j(gVar);
        this.B0 = j2;
        this.C0 = new e(j2);
        this.g0 = new n[mVarArr.length];
        for (int i2 = 0; i2 < mVarArr.length; i2++) {
            mVarArr[i2].p(i2, ks2Var);
            this.g0[i2] = mVarArr[i2].m();
        }
        this.s0 = new androidx.media3.exoplayer.e(this, tzVar);
        this.t0 = new ArrayList<>();
        this.f0 = com.google.common.collect.h.h();
        this.o0 = new u.c();
        this.p0 = new u.b();
        fVar.c(this, gmVar);
        this.R0 = true;
        Handler handler = new Handler(looper);
        this.w0 = new m62(jbVar, handler);
        this.x0 = new j(this, jbVar, handler, ks2Var);
        HandlerThread handlerThread = new HandlerThread("ExoPlayer:Playback", -16);
        this.m0 = handlerThread;
        handlerThread.start();
        Looper looper2 = handlerThread.getLooper();
        this.n0 = looper2;
        this.l0 = tzVar.c(looper2, this);
    }

    public static boolean N(boolean z, j.b bVar, long j, j.b bVar2, u.b bVar3, long j2) {
        if (!z && j == j2 && bVar.a.equals(bVar2.a)) {
            return (bVar.b() && bVar3.s(bVar.b)) ? (bVar3.j(bVar.b, bVar.c) == 4 || bVar3.j(bVar.b, bVar.c) == 2) ? false : true : bVar2.b() && bVar3.s(bVar2.b);
        }
        return false;
    }

    public static boolean P(m mVar) {
        return mVar.getState() != 0;
    }

    public static boolean R(lr2 lr2Var, u.b bVar) {
        j.b bVar2 = lr2Var.b;
        u uVar = lr2Var.a;
        return uVar.q() || uVar.h(bVar2.a, bVar).j0;
    }

    /* JADX INFO: Access modifiers changed from: private */
    public /* synthetic */ Boolean S() {
        return Boolean.valueOf(this.D0);
    }

    /* JADX INFO: Access modifiers changed from: private */
    public /* synthetic */ void T(k kVar) {
        try {
            l(kVar);
        } catch (ExoPlaybackException e2) {
            p12.d("ExoPlayerImplInternal", "Unexpected error delivering message on external thread.", e2);
            throw new RuntimeException(e2);
        }
    }

    public static void s0(u uVar, d dVar, u.c cVar, u.b bVar) {
        int i = uVar.n(uVar.h(dVar.h0, bVar).g0, cVar).t0;
        Object obj = uVar.g(i, bVar, true).f0;
        long j = bVar.h0;
        dVar.d(i, j != CellBase.ID_SYSTEM_MESSAGE_REQUEST_CLOSED ? j - 1 : Long.MAX_VALUE, obj);
    }

    public static boolean t0(d dVar, u uVar, u uVar2, int i, boolean z, u.c cVar, u.b bVar) {
        Object obj = dVar.h0;
        if (obj == null) {
            Pair<Object, Long> w0 = w0(uVar, new C0036h(dVar.a.h(), dVar.a.d(), dVar.a.f() == Long.MIN_VALUE ? CellBase.ID_SYSTEM_MESSAGE_REQUEST_CLOSED : androidx.media3.common.util.b.y0(dVar.a.f())), false, i, z, cVar, bVar);
            if (w0 == null) {
                return false;
            }
            dVar.d(uVar.b(w0.first), ((Long) w0.second).longValue(), w0.first);
            if (dVar.a.f() == Long.MIN_VALUE) {
                s0(uVar, dVar, cVar, bVar);
            }
            return true;
        }
        int b2 = uVar.b(obj);
        if (b2 == -1) {
            return false;
        }
        if (dVar.a.f() == Long.MIN_VALUE) {
            s0(uVar, dVar, cVar, bVar);
            return true;
        }
        dVar.f0 = b2;
        uVar2.h(dVar.h0, bVar);
        if (bVar.j0 && uVar2.n(bVar.g0, cVar).s0 == uVar2.b(dVar.h0)) {
            Pair<Object, Long> j = uVar.j(cVar, bVar, uVar.h(dVar.h0, bVar).g0, dVar.g0 + bVar.p());
            dVar.d(uVar.b(j.first), ((Long) j.second).longValue(), j.first);
        }
        return true;
    }

    /* JADX WARN: Removed duplicated region for block: B:50:0x0157  */
    /* JADX WARN: Removed duplicated region for block: B:51:0x0175  */
    /* JADX WARN: Removed duplicated region for block: B:75:0x01c1  */
    /*
        Code decompiled incorrectly, please refer to instructions dump.
        To view partially-correct code enable 'Show inconsistent code' option in preferences
    */
    public static androidx.media3.exoplayer.h.g v0(androidx.media3.common.u r30, defpackage.lr2 r31, androidx.media3.exoplayer.h.C0036h r32, defpackage.m62 r33, int r34, boolean r35, androidx.media3.common.u.c r36, androidx.media3.common.u.b r37) {
        /*
            Method dump skipped, instructions count: 492
            To view this dump change 'Code comments level' option to 'DEBUG'
        */
        throw new UnsupportedOperationException("Method not decompiled: androidx.media3.exoplayer.h.v0(androidx.media3.common.u, lr2, androidx.media3.exoplayer.h$h, m62, int, boolean, androidx.media3.common.u$c, androidx.media3.common.u$b):androidx.media3.exoplayer.h$g");
    }

    public static androidx.media3.common.j[] w(androidx.media3.exoplayer.trackselection.c cVar) {
        int length = cVar != null ? cVar.length() : 0;
        androidx.media3.common.j[] jVarArr = new androidx.media3.common.j[length];
        for (int i = 0; i < length; i++) {
            jVarArr[i] = cVar.j(i);
        }
        return jVarArr;
    }

    public static Pair<Object, Long> w0(u uVar, C0036h c0036h, boolean z, int i, boolean z2, u.c cVar, u.b bVar) {
        Pair<Object, Long> j;
        Object x0;
        u uVar2 = c0036h.a;
        if (uVar.q()) {
            return null;
        }
        u uVar3 = uVar2.q() ? uVar : uVar2;
        try {
            j = uVar3.j(cVar, bVar, c0036h.b, c0036h.c);
        } catch (IndexOutOfBoundsException unused) {
        }
        if (uVar.equals(uVar3)) {
            return j;
        }
        if (uVar.b(j.first) != -1) {
            return (uVar3.h(j.first, bVar).j0 && uVar3.n(bVar.g0, cVar).s0 == uVar3.b(j.first)) ? uVar.j(cVar, bVar, uVar.h(j.first, bVar).g0, c0036h.c) : j;
        }
        if (z && (x0 = x0(cVar, bVar, i, z2, j.first, uVar3, uVar)) != null) {
            return uVar.j(cVar, bVar, uVar.h(x0, bVar).g0, CellBase.ID_SYSTEM_MESSAGE_REQUEST_CLOSED);
        }
        return null;
    }

    public static Object x0(u.c cVar, u.b bVar, int i, boolean z, Object obj, u uVar, u uVar2) {
        int b2 = uVar.b(obj);
        int i2 = uVar.i();
        int i3 = b2;
        int i4 = -1;
        for (int i5 = 0; i5 < i2 && i4 == -1; i5++) {
            i3 = uVar.d(i3, bVar, cVar, i, z);
            if (i3 == -1) {
                break;
            }
            i4 = uVar2.b(uVar.m(i3));
        }
        if (i4 == -1) {
            return null;
        }
        return uVar2.m(i4);
    }

    public Looper A() {
        return this.n0;
    }

    public final void A0(boolean z) throws ExoPlaybackException {
        j.b bVar = this.w0.p().f.a;
        long D0 = D0(bVar, this.B0.r, true, false);
        if (D0 != this.B0.r) {
            lr2 lr2Var = this.B0;
            this.B0 = K(bVar, D0, lr2Var.c, lr2Var.d, z, 5);
        }
    }

    public final long B() {
        return C(this.B0.p);
    }

    /* JADX WARN: Removed duplicated region for block: B:24:0x00ac A[Catch: all -> 0x0148, TryCatch #1 {all -> 0x0148, blocks: (B:22:0x00a2, B:24:0x00ac, B:27:0x00b2, B:29:0x00b8, B:30:0x00bb, B:32:0x00c1, B:34:0x00cb, B:36:0x00d3, B:40:0x00db, B:42:0x00e5, B:44:0x00f5, B:48:0x00ff, B:52:0x0111, B:56:0x011a), top: B:74:0x00a2 }] */
    /* JADX WARN: Removed duplicated region for block: B:25:0x00af  */
    /*
        Code decompiled incorrectly, please refer to instructions dump.
        To view partially-correct code enable 'Show inconsistent code' option in preferences
    */
    public final void B0(androidx.media3.exoplayer.h.C0036h r19) throws androidx.media3.exoplayer.ExoPlaybackException {
        /*
            Method dump skipped, instructions count: 345
            To view this dump change 'Code comments level' option to 'DEBUG'
        */
        throw new UnsupportedOperationException("Method not decompiled: androidx.media3.exoplayer.h.B0(androidx.media3.exoplayer.h$h):void");
    }

    public final long C(long j) {
        i j2 = this.w0.j();
        if (j2 == null) {
            return 0L;
        }
        return Math.max(0L, j - j2.y(this.P0));
    }

    public final long C0(j.b bVar, long j, boolean z) throws ExoPlaybackException {
        return D0(bVar, j, this.w0.p() != this.w0.q(), z);
    }

    public final void D(androidx.media3.exoplayer.source.i iVar) {
        if (this.w0.v(iVar)) {
            this.w0.y(this.P0);
            U();
        }
    }

    public final long D0(j.b bVar, long j, boolean z, boolean z2) throws ExoPlaybackException {
        h1();
        this.G0 = false;
        if (z2 || this.B0.e == 3) {
            Y0(2);
        }
        i p = this.w0.p();
        i iVar = p;
        while (iVar != null && !bVar.equals(iVar.f.a)) {
            iVar = iVar.j();
        }
        if (z || p != iVar || (iVar != null && iVar.z(j) < 0)) {
            for (m mVar : this.a) {
                m(mVar);
            }
            if (iVar != null) {
                while (this.w0.p() != iVar) {
                    this.w0.b();
                }
                this.w0.z(iVar);
                iVar.x(1000000000000L);
                q();
            }
        }
        if (iVar != null) {
            this.w0.z(iVar);
            if (!iVar.d) {
                iVar.f = iVar.f.b(j);
            } else if (iVar.e) {
                long l = iVar.a.l(j);
                iVar.a.t(l - this.q0, this.r0);
                j = l;
            }
            r0(j);
            U();
        } else {
            this.w0.f();
            r0(j);
        }
        F(false);
        this.l0.f(2);
        return j;
    }

    public final void E(IOException iOException, int i) {
        ExoPlaybackException createForSource = ExoPlaybackException.createForSource(iOException, i);
        i p = this.w0.p();
        if (p != null) {
            createForSource = createForSource.copyWithMediaPeriodId(p.f.a);
        }
        p12.d("ExoPlayerImplInternal", "Playback error", createForSource);
        g1(false, false);
        this.B0 = this.B0.e(createForSource);
    }

    public final void E0(k kVar) throws ExoPlaybackException {
        if (kVar.f() == CellBase.ID_SYSTEM_MESSAGE_REQUEST_CLOSED) {
            F0(kVar);
        } else if (this.B0.a.q()) {
            this.t0.add(new d(kVar));
        } else {
            d dVar = new d(kVar);
            u uVar = this.B0.a;
            if (t0(dVar, uVar, uVar, this.I0, this.J0, this.o0, this.p0)) {
                this.t0.add(dVar);
                Collections.sort(this.t0);
                return;
            }
            kVar.k(false);
        }
    }

    public final void F(boolean z) {
        long i;
        i j = this.w0.j();
        j.b bVar = j == null ? this.B0.b : j.f.a;
        boolean z2 = !this.B0.k.equals(bVar);
        if (z2) {
            this.B0 = this.B0.b(bVar);
        }
        lr2 lr2Var = this.B0;
        if (j == null) {
            i = lr2Var.r;
        } else {
            i = j.i();
        }
        lr2Var.p = i;
        this.B0.q = B();
        if ((z2 || z) && j != null && j.d) {
            j1(j.n(), j.o());
        }
    }

    public final void F0(k kVar) throws ExoPlaybackException {
        if (kVar.c() == this.n0) {
            l(kVar);
            int i = this.B0.e;
            if (i == 3 || i == 2) {
                this.l0.f(2);
                return;
            }
            return;
        }
        this.l0.j(15, kVar).a();
    }

    public final void G(u uVar, boolean z) throws ExoPlaybackException {
        int i;
        int i2;
        boolean z2;
        g v0 = v0(uVar, this.B0, this.O0, this.w0, this.I0, this.J0, this.o0, this.p0);
        j.b bVar = v0.a;
        long j = v0.c;
        boolean z3 = v0.d;
        long j2 = v0.b;
        boolean z4 = (this.B0.b.equals(bVar) && j2 == this.B0.r) ? false : true;
        C0036h c0036h = null;
        long j3 = CellBase.ID_SYSTEM_MESSAGE_REQUEST_CLOSED;
        try {
            if (v0.e) {
                if (this.B0.e != 1) {
                    Y0(4);
                }
                p0(false, false, false, true);
            }
            try {
                if (!z4) {
                    try {
                        i2 = 4;
                        z2 = false;
                        if (!this.w0.F(uVar, this.P0, y())) {
                            A0(false);
                        }
                    } catch (Throwable th) {
                        th = th;
                        i = 4;
                        lr2 lr2Var = this.B0;
                        u uVar2 = lr2Var.a;
                        j.b bVar2 = lr2Var.b;
                        if (v0.f) {
                            j3 = j2;
                        }
                        C0036h c0036h2 = c0036h;
                        m1(uVar, bVar, uVar2, bVar2, j3);
                        if (z4 || j != this.B0.c) {
                            lr2 lr2Var2 = this.B0;
                            Object obj = lr2Var2.b.a;
                            u uVar3 = lr2Var2.a;
                            this.B0 = K(bVar, j2, j, this.B0.d, z4 && z && !uVar3.q() && !uVar3.h(obj, this.p0).j0, uVar.b(obj) == -1 ? i : 3);
                        }
                        q0();
                        u0(uVar, this.B0.a);
                        this.B0 = this.B0.i(uVar);
                        if (!uVar.q()) {
                            this.O0 = c0036h2;
                        }
                        F(false);
                        throw th;
                    }
                } else {
                    i2 = 4;
                    z2 = false;
                    if (!uVar.q()) {
                        for (i p = this.w0.p(); p != null; p = p.j()) {
                            if (p.f.a.equals(bVar)) {
                                p.f = this.w0.r(uVar, p.f);
                                p.A();
                            }
                        }
                        j2 = C0(bVar, j2, z3);
                    }
                }
                lr2 lr2Var3 = this.B0;
                m1(uVar, bVar, lr2Var3.a, lr2Var3.b, v0.f ? j2 : -9223372036854775807L);
                if (z4 || j != this.B0.c) {
                    lr2 lr2Var4 = this.B0;
                    Object obj2 = lr2Var4.b.a;
                    u uVar4 = lr2Var4.a;
                    this.B0 = K(bVar, j2, j, this.B0.d, (!z4 || !z || uVar4.q() || uVar4.h(obj2, this.p0).j0) ? z2 : true, uVar.b(obj2) == -1 ? i2 : 3);
                }
                q0();
                u0(uVar, this.B0.a);
                this.B0 = this.B0.i(uVar);
                if (!uVar.q()) {
                    this.O0 = null;
                }
                F(z2);
            } catch (Throwable th2) {
                th = th2;
                c0036h = null;
            }
        } catch (Throwable th3) {
            th = th3;
            i = 4;
        }
    }

    public final void G0(final k kVar) {
        Looper c2 = kVar.c();
        if (!c2.getThread().isAlive()) {
            p12.i("TAG", "Trying to send message on a dead thread.");
            kVar.k(false);
            return;
        }
        this.u0.c(c2, null).c(new Runnable() { // from class: t01
            @Override // java.lang.Runnable
            public final void run() {
                h.this.T(kVar);
            }
        });
    }

    public final void H(androidx.media3.exoplayer.source.i iVar) throws ExoPlaybackException {
        if (this.w0.v(iVar)) {
            i j = this.w0.j();
            j.p(this.s0.e().a, this.B0.a);
            j1(j.n(), j.o());
            if (j == this.w0.p()) {
                r0(j.f.b);
                q();
                lr2 lr2Var = this.B0;
                j.b bVar = lr2Var.b;
                long j2 = j.f.b;
                this.B0 = K(bVar, j2, lr2Var.c, j2, false, 5);
            }
            U();
        }
    }

    public final void H0(long j) {
        m[] mVarArr;
        for (m mVar : this.a) {
            if (mVar.c() != null) {
                I0(mVar, j);
            }
        }
    }

    public final void I(p pVar, float f2, boolean z, boolean z2) throws ExoPlaybackException {
        m[] mVarArr;
        if (z) {
            if (z2) {
                this.C0.b(1);
            }
            this.B0 = this.B0.f(pVar);
        }
        n1(pVar.a);
        for (m mVar : this.a) {
            if (mVar != null) {
                mVar.o(f2, pVar.a);
            }
        }
    }

    public final void I0(m mVar, long j) {
        mVar.k();
        if (mVar instanceof o44) {
            ((o44) mVar).Y(j);
        }
    }

    public final void J(p pVar, boolean z) throws ExoPlaybackException {
        I(pVar, pVar.a, true, z);
    }

    public final void J0(boolean z, AtomicBoolean atomicBoolean) {
        m[] mVarArr;
        if (this.K0 != z) {
            this.K0 = z;
            if (!z) {
                for (m mVar : this.a) {
                    if (!P(mVar) && this.f0.remove(mVar)) {
                        mVar.reset();
                    }
                }
            }
        }
        if (atomicBoolean != null) {
            synchronized (this) {
                atomicBoolean.set(true);
                notifyAll();
            }
        }
    }

    /* JADX WARN: Multi-variable type inference failed */
    public final lr2 K(j.b bVar, long j, long j2, long j3, boolean z, int i) {
        List list;
        c84 c84Var;
        androidx.media3.exoplayer.trackselection.g gVar;
        c84 n;
        androidx.media3.exoplayer.trackselection.g o;
        this.R0 = (!this.R0 && j == this.B0.r && bVar.equals(this.B0.b)) ? false : true;
        q0();
        lr2 lr2Var = this.B0;
        c84 c84Var2 = lr2Var.h;
        androidx.media3.exoplayer.trackselection.g gVar2 = lr2Var.i;
        List list2 = lr2Var.j;
        if (this.x0.s()) {
            i p = this.w0.p();
            if (p == null) {
                n = c84.h0;
            } else {
                n = p.n();
            }
            if (p == null) {
                o = this.i0;
            } else {
                o = p.o();
            }
            List u = u(o.c);
            if (p != null) {
                k62 k62Var = p.f;
                if (k62Var.c != j2) {
                    p.f = k62Var.a(j2);
                }
            }
            c84Var = n;
            gVar = o;
            list = u;
        } else if (bVar.equals(this.B0.b)) {
            list = list2;
            c84Var = c84Var2;
            gVar = gVar2;
        } else {
            c84Var = c84.h0;
            gVar = this.i0;
            list = ImmutableList.of();
        }
        if (z) {
            this.C0.e(i);
        }
        return this.B0.c(bVar, j, j2, j3, B(), c84Var, gVar, list);
    }

    public final void K0(b bVar) throws ExoPlaybackException {
        this.C0.b(1);
        if (bVar.c != -1) {
            this.O0 = new C0036h(new ls2(bVar.a, bVar.b), bVar.c, bVar.d);
        }
        G(this.x0.C(bVar.a, bVar.b), false);
    }

    public final boolean L(m mVar, i iVar) {
        i j = iVar.j();
        return iVar.f.f && j.d && ((mVar instanceof o44) || (mVar instanceof p82) || mVar.u() >= j.m());
    }

    public void L0(List<j.c> list, int i, long j, qo3 qo3Var) {
        this.l0.j(17, new b(list, qo3Var, i, j, null)).a();
    }

    public final boolean M() {
        i q = this.w0.q();
        if (q.d) {
            int i = 0;
            while (true) {
                m[] mVarArr = this.a;
                if (i >= mVarArr.length) {
                    return true;
                }
                m mVar = mVarArr[i];
                r rVar = q.c[i];
                if (mVar.c() != rVar || (rVar != null && !mVar.i() && !L(mVar, q))) {
                    break;
                }
                i++;
            }
            return false;
        }
        return false;
    }

    public final void M0(boolean z) {
        if (z == this.M0) {
            return;
        }
        this.M0 = z;
        if (z || !this.B0.o) {
            return;
        }
        this.l0.f(2);
    }

    public final void N0(boolean z) throws ExoPlaybackException {
        this.E0 = z;
        q0();
        if (!this.F0 || this.w0.q() == this.w0.p()) {
            return;
        }
        A0(true);
        F(false);
    }

    public final boolean O() {
        i j = this.w0.j();
        return (j == null || j.k() == Long.MIN_VALUE) ? false : true;
    }

    public void O0(boolean z, int i) {
        this.l0.a(1, z ? 1 : 0, i).a();
    }

    public final void P0(boolean z, int i, boolean z2, int i2) throws ExoPlaybackException {
        this.C0.b(z2 ? 1 : 0);
        this.C0.c(i2);
        this.B0 = this.B0.d(z, i);
        this.G0 = false;
        e0(z);
        if (!b1()) {
            h1();
            l1();
            return;
        }
        int i3 = this.B0.e;
        if (i3 == 3) {
            e1();
            this.l0.f(2);
        } else if (i3 == 2) {
            this.l0.f(2);
        }
    }

    public final boolean Q() {
        i p = this.w0.p();
        long j = p.f.e;
        return p.d && (j == CellBase.ID_SYSTEM_MESSAGE_REQUEST_CLOSED || this.B0.r < j || !b1());
    }

    public void Q0(p pVar) {
        this.l0.j(4, pVar).a();
    }

    public final void R0(p pVar) throws ExoPlaybackException {
        this.s0.b(pVar);
        J(this.s0.e(), true);
    }

    public void S0(int i) {
        this.l0.a(11, i, 0).a();
    }

    public final void T0(int i) throws ExoPlaybackException {
        this.I0 = i;
        if (!this.w0.G(this.B0.a, i)) {
            A0(true);
        }
        F(false);
    }

    public final void U() {
        boolean a1 = a1();
        this.H0 = a1;
        if (a1) {
            this.w0.j().d(this.P0);
        }
        i1();
    }

    public final void U0(xi3 xi3Var) {
        this.A0 = xi3Var;
    }

    public final void V() {
        this.C0.d(this.B0);
        if (this.C0.a) {
            this.v0.a(this.C0);
            this.C0 = new e(this.B0);
        }
    }

    public void V0(boolean z) {
        this.l0.a(12, z ? 1 : 0, 0).a();
    }

    /* JADX WARN: Removed duplicated region for block: B:15:0x0047  */
    /* JADX WARN: Removed duplicated region for block: B:25:0x006a  */
    /* JADX WARN: Removed duplicated region for block: B:28:0x0076  */
    /* JADX WARN: Removed duplicated region for block: B:37:0x0090 A[LOOP:1: B:27:0x0074->B:37:0x0090, LOOP_END] */
    /* JADX WARN: Removed duplicated region for block: B:39:0x009b  */
    /* JADX WARN: Removed duplicated region for block: B:78:0x0073 A[SYNTHETIC] */
    /* JADX WARN: Removed duplicated region for block: B:84:0x00dc A[SYNTHETIC] */
    /* JADX WARN: Removed duplicated region for block: B:87:0x00d3 A[SYNTHETIC] */
    /* JADX WARN: Unsupported multi-entry loop pattern (BACK_EDGE: B:13:0x0044 -> B:14:0x0045). Please submit an issue!!! */
    /* JADX WARN: Unsupported multi-entry loop pattern (BACK_EDGE: B:26:0x0073 -> B:27:0x0074). Please submit an issue!!! */
    /*
        Code decompiled incorrectly, please refer to instructions dump.
        To view partially-correct code enable 'Show inconsistent code' option in preferences
    */
    public final void W(long r8, long r10) throws androidx.media3.exoplayer.ExoPlaybackException {
        /*
            Method dump skipped, instructions count: 248
            To view this dump change 'Code comments level' option to 'DEBUG'
        */
        throw new UnsupportedOperationException("Method not decompiled: androidx.media3.exoplayer.h.W(long, long):void");
    }

    public final void W0(boolean z) throws ExoPlaybackException {
        this.J0 = z;
        if (!this.w0.H(this.B0.a, z)) {
            A0(true);
        }
        F(false);
    }

    public final void X() throws ExoPlaybackException {
        k62 o;
        this.w0.y(this.P0);
        if (this.w0.D() && (o = this.w0.o(this.P0, this.B0)) != null) {
            i g2 = this.w0.g(this.g0, this.h0, this.j0.h(), this.x0, o, this.i0);
            g2.a.q(this, o.b);
            if (this.w0.p() == g2) {
                r0(o.b);
            }
            F(false);
        }
        if (this.H0) {
            this.H0 = O();
            i1();
            return;
        }
        U();
    }

    public final void X0(qo3 qo3Var) throws ExoPlaybackException {
        this.C0.b(1);
        G(this.x0.D(qo3Var), false);
    }

    public final void Y() throws ExoPlaybackException {
        boolean z;
        boolean z2 = false;
        while (Z0()) {
            if (z2) {
                V();
            }
            i iVar = (i) ii.e(this.w0.b());
            if (this.B0.b.a.equals(iVar.f.a.a)) {
                j.b bVar = this.B0.b;
                if (bVar.b == -1) {
                    j.b bVar2 = iVar.f.a;
                    if (bVar2.b == -1 && bVar.e != bVar2.e) {
                        z = true;
                        k62 k62Var = iVar.f;
                        j.b bVar3 = k62Var.a;
                        long j = k62Var.b;
                        this.B0 = K(bVar3, j, k62Var.c, j, !z, 0);
                        q0();
                        l1();
                        z2 = true;
                    }
                }
            }
            z = false;
            k62 k62Var2 = iVar.f;
            j.b bVar32 = k62Var2.a;
            long j2 = k62Var2.b;
            this.B0 = K(bVar32, j2, k62Var2.c, j2, !z, 0);
            q0();
            l1();
            z2 = true;
        }
    }

    public final void Y0(int i) {
        lr2 lr2Var = this.B0;
        if (lr2Var.e != i) {
            if (i != 2) {
                this.T0 = CellBase.ID_SYSTEM_MESSAGE_REQUEST_CLOSED;
            }
            this.B0 = lr2Var.g(i);
        }
    }

    public final void Z() {
        i q = this.w0.q();
        if (q == null) {
            return;
        }
        int i = 0;
        if (q.j() != null && !this.F0) {
            if (M()) {
                if (q.j().d || this.P0 >= q.j().m()) {
                    androidx.media3.exoplayer.trackselection.g o = q.o();
                    i c2 = this.w0.c();
                    androidx.media3.exoplayer.trackselection.g o2 = c2.o();
                    u uVar = this.B0.a;
                    m1(uVar, c2.f.a, uVar, q.f.a, CellBase.ID_SYSTEM_MESSAGE_REQUEST_CLOSED);
                    if (c2.d && c2.a.o() != CellBase.ID_SYSTEM_MESSAGE_REQUEST_CLOSED) {
                        H0(c2.m());
                        return;
                    }
                    for (int i2 = 0; i2 < this.a.length; i2++) {
                        boolean c3 = o.c(i2);
                        boolean c4 = o2.c(i2);
                        if (c3 && !this.a[i2].w()) {
                            boolean z = this.g0[i2].h() == -2;
                            v63 v63Var = o.b[i2];
                            v63 v63Var2 = o2.b[i2];
                            if (!c4 || !v63Var2.equals(v63Var) || z) {
                                I0(this.a[i2], c2.m());
                            }
                        }
                    }
                }
            }
        } else if (q.f.i || this.F0) {
            while (true) {
                m[] mVarArr = this.a;
                if (i >= mVarArr.length) {
                    return;
                }
                m mVar = mVarArr[i];
                r rVar = q.c[i];
                if (rVar != null && mVar.c() == rVar && mVar.i()) {
                    long j = q.f.e;
                    I0(mVar, (j == CellBase.ID_SYSTEM_MESSAGE_REQUEST_CLOSED || j == Long.MIN_VALUE) ? -9223372036854775807L : q.l() + q.f.e);
                }
                i++;
            }
        }
    }

    public final boolean Z0() {
        i p;
        i j;
        return b1() && !this.F0 && (p = this.w0.p()) != null && (j = p.j()) != null && this.P0 >= j.m() && j.g;
    }

    @Override // androidx.media3.exoplayer.trackselection.f.a
    public void a() {
        this.l0.f(10);
    }

    public final void a0() throws ExoPlaybackException {
        i q = this.w0.q();
        if (q == null || this.w0.p() == q || q.g || !n0()) {
            return;
        }
        q();
    }

    public final boolean a1() {
        long y;
        if (O()) {
            i j = this.w0.j();
            long C = C(j.k());
            if (j == this.w0.p()) {
                y = j.y(this.P0);
            } else {
                y = j.y(this.P0) - j.f.b;
            }
            return this.j0.g(y, C, this.s0.e().a);
        }
        return false;
    }

    @Override // androidx.media3.exoplayer.j.d
    public void b() {
        this.l0.f(22);
    }

    public final void b0() throws ExoPlaybackException {
        G(this.x0.i(), true);
    }

    public final boolean b1() {
        lr2 lr2Var = this.B0;
        return lr2Var.l && lr2Var.m == 0;
    }

    @Override // androidx.media3.exoplayer.k.a
    public synchronized void c(k kVar) {
        if (!this.D0 && this.m0.isAlive()) {
            this.l0.j(14, kVar).a();
            return;
        }
        p12.i("ExoPlayerImplInternal", "Ignoring messages sent after release.");
        kVar.k(false);
    }

    public final void c0(c cVar) throws ExoPlaybackException {
        this.C0.b(1);
        G(this.x0.v(cVar.a, cVar.b, cVar.c, cVar.d), false);
    }

    public final boolean c1(boolean z) {
        if (this.N0 == 0) {
            return Q();
        }
        if (z) {
            lr2 lr2Var = this.B0;
            if (lr2Var.g) {
                long c2 = d1(lr2Var.a, this.w0.p().f.a) ? this.y0.c() : CellBase.ID_SYSTEM_MESSAGE_REQUEST_CLOSED;
                i j = this.w0.j();
                return (j.q() && j.f.i) || (j.f.a.b() && !j.d) || this.j0.f(B(), this.s0.e().a, this.G0, c2);
            }
            return true;
        }
        return false;
    }

    public final void d0() {
        androidx.media3.exoplayer.trackselection.c[] cVarArr;
        for (i p = this.w0.p(); p != null; p = p.j()) {
            for (androidx.media3.exoplayer.trackselection.c cVar : p.o().c) {
                if (cVar != null) {
                    cVar.r();
                }
            }
        }
    }

    public final boolean d1(u uVar, j.b bVar) {
        if (bVar.b() || uVar.q()) {
            return false;
        }
        uVar.n(uVar.h(bVar.a, this.p0).g0, this.o0);
        if (this.o0.i()) {
            u.c cVar = this.o0;
            return cVar.m0 && cVar.j0 != CellBase.ID_SYSTEM_MESSAGE_REQUEST_CLOSED;
        }
        return false;
    }

    public final void e0(boolean z) {
        androidx.media3.exoplayer.trackselection.c[] cVarArr;
        for (i p = this.w0.p(); p != null; p = p.j()) {
            for (androidx.media3.exoplayer.trackselection.c cVar : p.o().c) {
                if (cVar != null) {
                    cVar.i(z);
                }
            }
        }
    }

    public final void e1() throws ExoPlaybackException {
        m[] mVarArr;
        this.G0 = false;
        this.s0.g();
        for (m mVar : this.a) {
            if (P(mVar)) {
                mVar.start();
            }
        }
    }

    @Override // androidx.media3.exoplayer.source.i.a
    public void f(androidx.media3.exoplayer.source.i iVar) {
        this.l0.j(8, iVar).a();
    }

    public final void f0() {
        androidx.media3.exoplayer.trackselection.c[] cVarArr;
        for (i p = this.w0.p(); p != null; p = p.j()) {
            for (androidx.media3.exoplayer.trackselection.c cVar : p.o().c) {
                if (cVar != null) {
                    cVar.s();
                }
            }
        }
    }

    public void f1() {
        this.l0.d(6).a();
    }

    @Override // androidx.media3.exoplayer.source.s.a
    /* renamed from: g0 */
    public void i(androidx.media3.exoplayer.source.i iVar) {
        this.l0.j(9, iVar).a();
    }

    public final void g1(boolean z, boolean z2) {
        p0(z || !this.K0, false, true, false);
        this.C0.b(z2 ? 1 : 0);
        this.j0.i();
        Y0(1);
    }

    public void h0() {
        this.l0.d(0).a();
    }

    public final void h1() throws ExoPlaybackException {
        m[] mVarArr;
        this.s0.h();
        for (m mVar : this.a) {
            if (P(mVar)) {
                s(mVar);
            }
        }
    }

    @Override // android.os.Handler.Callback
    public boolean handleMessage(Message message) {
        i q;
        int i;
        int i2 = 1000;
        try {
            switch (message.what) {
                case 0:
                    i0();
                    break;
                case 1:
                    P0(message.arg1 != 0, message.arg2, true, 1);
                    break;
                case 2:
                    o();
                    break;
                case 3:
                    B0((C0036h) message.obj);
                    break;
                case 4:
                    R0((p) message.obj);
                    break;
                case 5:
                    U0((xi3) message.obj);
                    break;
                case 6:
                    g1(false, true);
                    break;
                case 7:
                    k0();
                    return true;
                case 8:
                    H((androidx.media3.exoplayer.source.i) message.obj);
                    break;
                case 9:
                    D((androidx.media3.exoplayer.source.i) message.obj);
                    break;
                case 10:
                    o0();
                    break;
                case 11:
                    T0(message.arg1);
                    break;
                case 12:
                    W0(message.arg1 != 0);
                    break;
                case 13:
                    J0(message.arg1 != 0, (AtomicBoolean) message.obj);
                    break;
                case 14:
                    E0((k) message.obj);
                    break;
                case 15:
                    G0((k) message.obj);
                    break;
                case 16:
                    J((p) message.obj, false);
                    break;
                case 17:
                    K0((b) message.obj);
                    break;
                case 18:
                    j((b) message.obj, message.arg1);
                    break;
                case 19:
                    c0((c) message.obj);
                    break;
                case 20:
                    l0(message.arg1, message.arg2, (qo3) message.obj);
                    break;
                case 21:
                    X0((qo3) message.obj);
                    break;
                case 22:
                    b0();
                    break;
                case 23:
                    N0(message.arg1 != 0);
                    break;
                case 24:
                    M0(message.arg1 == 1);
                    break;
                case 25:
                    k();
                    break;
                default:
                    return false;
            }
        } catch (ParserException e2) {
            int i3 = e2.dataType;
            if (i3 == 1) {
                i = e2.contentIsMalformed ? PlaybackException.ERROR_CODE_PARSING_CONTAINER_MALFORMED : PlaybackException.ERROR_CODE_PARSING_CONTAINER_UNSUPPORTED;
            } else {
                if (i3 == 4) {
                    i = e2.contentIsMalformed ? PlaybackException.ERROR_CODE_PARSING_MANIFEST_MALFORMED : PlaybackException.ERROR_CODE_PARSING_MANIFEST_UNSUPPORTED;
                }
                E(e2, i2);
            }
            i2 = i;
            E(e2, i2);
        } catch (DataSourceException e3) {
            E(e3, e3.reason);
        } catch (ExoPlaybackException e4) {
            e = e4;
            if (e.type == 1 && (q = this.w0.q()) != null) {
                e = e.copyWithMediaPeriodId(q.f.a);
            }
            if (e.isRecoverable && this.S0 == null) {
                p12.j("ExoPlayerImplInternal", "Recoverable renderer error", e);
                this.S0 = e;
                pj1 pj1Var = this.l0;
                pj1Var.b(pj1Var.j(25, e));
            } else {
                ExoPlaybackException exoPlaybackException = this.S0;
                if (exoPlaybackException != null) {
                    exoPlaybackException.addSuppressed(e);
                    e = this.S0;
                }
                p12.d("ExoPlayerImplInternal", "Playback error", e);
                g1(true, false);
                this.B0 = this.B0.e(e);
            }
        } catch (DrmSession.DrmSessionException e5) {
            E(e5, e5.errorCode);
        } catch (BehindLiveWindowException e6) {
            E(e6, PlaybackException.ERROR_CODE_BEHIND_LIVE_WINDOW);
        } catch (IOException e7) {
            E(e7, PlaybackException.ERROR_CODE_IO_UNSPECIFIED);
        } catch (RuntimeException e8) {
            if ((e8 instanceof IllegalStateException) || (e8 instanceof IllegalArgumentException)) {
                i2 = PlaybackException.ERROR_CODE_FAILED_RUNTIME_CHECK;
            }
            ExoPlaybackException createForUnexpected = ExoPlaybackException.createForUnexpected(e8, i2);
            p12.d("ExoPlayerImplInternal", "Playback error", createForUnexpected);
            g1(true, false);
            this.B0 = this.B0.e(createForUnexpected);
        }
        V();
        return true;
    }

    public final void i0() {
        this.C0.b(1);
        p0(false, false, false, true);
        this.j0.c();
        Y0(this.B0.a.q() ? 4 : 2);
        this.x0.w(this.k0.f());
        this.l0.f(2);
    }

    public final void i1() {
        i j = this.w0.j();
        boolean z = this.H0 || (j != null && j.a.e());
        lr2 lr2Var = this.B0;
        if (z != lr2Var.g) {
            this.B0 = lr2Var.a(z);
        }
    }

    public final void j(b bVar, int i) throws ExoPlaybackException {
        this.C0.b(1);
        j jVar = this.x0;
        if (i == -1) {
            i = jVar.q();
        }
        G(jVar.f(i, bVar.a, bVar.b), false);
    }

    public synchronized boolean j0() {
        if (!this.D0 && this.m0.isAlive()) {
            this.l0.f(7);
            o1(new dw3() { // from class: s01
                @Override // defpackage.dw3
                public final Object get() {
                    Boolean S;
                    S = h.this.S();
                    return S;
                }
            }, this.z0);
            return this.D0;
        }
        return true;
    }

    public final void j1(c84 c84Var, androidx.media3.exoplayer.trackselection.g gVar) {
        this.j0.d(this.a, c84Var, gVar.c);
    }

    public final void k() throws ExoPlaybackException {
        A0(true);
    }

    public final void k0() {
        p0(true, false, true, false);
        this.j0.e();
        Y0(1);
        this.m0.quit();
        synchronized (this) {
            this.D0 = true;
            notifyAll();
        }
    }

    public final void k1() throws ExoPlaybackException, IOException {
        if (this.B0.a.q() || !this.x0.s()) {
            return;
        }
        X();
        Z();
        a0();
        Y();
    }

    public final void l(k kVar) throws ExoPlaybackException {
        if (kVar.j()) {
            return;
        }
        try {
            kVar.g().s(kVar.i(), kVar.e());
        } finally {
            kVar.k(true);
        }
    }

    public final void l0(int i, int i2, qo3 qo3Var) throws ExoPlaybackException {
        this.C0.b(1);
        G(this.x0.A(i, i2, qo3Var), false);
    }

    public final void l1() throws ExoPlaybackException {
        i p = this.w0.p();
        if (p == null) {
            return;
        }
        long o = p.d ? p.a.o() : -9223372036854775807L;
        if (o != CellBase.ID_SYSTEM_MESSAGE_REQUEST_CLOSED) {
            r0(o);
            if (o != this.B0.r) {
                lr2 lr2Var = this.B0;
                this.B0 = K(lr2Var.b, o, lr2Var.c, o, true, 5);
            }
        } else {
            long i = this.s0.i(p != this.w0.q());
            this.P0 = i;
            long y = p.y(i);
            W(this.B0.r, y);
            this.B0.r = y;
        }
        this.B0.p = this.w0.j().i();
        this.B0.q = B();
        lr2 lr2Var2 = this.B0;
        if (lr2Var2.l && lr2Var2.e == 3 && d1(lr2Var2.a, lr2Var2.b) && this.B0.n.a == 1.0f) {
            float b2 = this.y0.b(v(), B());
            if (this.s0.e().a != b2) {
                this.s0.b(this.B0.n.c(b2));
                I(this.B0.n, this.s0.e().a, false, false);
            }
        }
    }

    public final void m(m mVar) throws ExoPlaybackException {
        if (P(mVar)) {
            this.s0.a(mVar);
            s(mVar);
            mVar.g();
            this.N0--;
        }
    }

    public void m0(int i, int i2, qo3 qo3Var) {
        this.l0.g(20, i, i2, qo3Var).a();
    }

    public final void m1(u uVar, j.b bVar, u uVar2, j.b bVar2, long j) {
        if (!d1(uVar, bVar)) {
            p pVar = bVar.b() ? p.h0 : this.B0.n;
            if (this.s0.e().equals(pVar)) {
                return;
            }
            this.s0.b(pVar);
            return;
        }
        uVar.n(uVar.h(bVar.a, this.p0).g0, this.o0);
        this.y0.a((m.g) androidx.media3.common.util.b.j(this.o0.o0));
        if (j != CellBase.ID_SYSTEM_MESSAGE_REQUEST_CLOSED) {
            this.y0.e(x(uVar, bVar.a, j));
            return;
        }
        if (androidx.media3.common.util.b.c(uVar2.q() ? null : uVar2.n(uVar2.h(bVar2.a, this.p0).g0, this.o0).a, this.o0.a)) {
            return;
        }
        this.y0.e(CellBase.ID_SYSTEM_MESSAGE_REQUEST_CLOSED);
    }

    @Override // androidx.media3.exoplayer.e.a
    public void n(p pVar) {
        this.l0.j(16, pVar).a();
    }

    public final boolean n0() throws ExoPlaybackException {
        i q = this.w0.q();
        androidx.media3.exoplayer.trackselection.g o = q.o();
        int i = 0;
        boolean z = false;
        while (true) {
            m[] mVarArr = this.a;
            if (i >= mVarArr.length) {
                return !z;
            }
            m mVar = mVarArr[i];
            if (P(mVar)) {
                boolean z2 = mVar.c() != q.c[i];
                if (!o.c(i) || z2) {
                    if (!mVar.w()) {
                        mVar.l(w(o.c[i]), q.c[i], q.m(), q.l());
                    } else if (mVar.d()) {
                        m(mVar);
                    } else {
                        z = true;
                    }
                }
            }
            i++;
        }
    }

    public final void n1(float f2) {
        androidx.media3.exoplayer.trackselection.c[] cVarArr;
        for (i p = this.w0.p(); p != null; p = p.j()) {
            for (androidx.media3.exoplayer.trackselection.c cVar : p.o().c) {
                if (cVar != null) {
                    cVar.p(f2);
                }
            }
        }
    }

    /* JADX WARN: Removed duplicated region for block: B:109:0x0180  */
    /* JADX WARN: Removed duplicated region for block: B:110:0x0183  */
    /* JADX WARN: Removed duplicated region for block: B:130:0x01c3  */
    /*
        Code decompiled incorrectly, please refer to instructions dump.
        To view partially-correct code enable 'Show inconsistent code' option in preferences
    */
    public final void o() throws androidx.media3.exoplayer.ExoPlaybackException, java.io.IOException {
        /*
            Method dump skipped, instructions count: 504
            To view this dump change 'Code comments level' option to 'DEBUG'
        */
        throw new UnsupportedOperationException("Method not decompiled: androidx.media3.exoplayer.h.o():void");
    }

    public final void o0() throws ExoPlaybackException {
        float f2 = this.s0.e().a;
        i q = this.w0.q();
        boolean z = true;
        for (i p = this.w0.p(); p != null && p.d; p = p.j()) {
            androidx.media3.exoplayer.trackselection.g v = p.v(f2, this.B0.a);
            if (!v.a(p.o())) {
                if (z) {
                    i p2 = this.w0.p();
                    boolean z2 = this.w0.z(p2);
                    boolean[] zArr = new boolean[this.a.length];
                    long b2 = p2.b(v, this.B0.r, z2, zArr);
                    lr2 lr2Var = this.B0;
                    boolean z3 = (lr2Var.e == 4 || b2 == lr2Var.r) ? false : true;
                    lr2 lr2Var2 = this.B0;
                    this.B0 = K(lr2Var2.b, b2, lr2Var2.c, lr2Var2.d, z3, 5);
                    if (z3) {
                        r0(b2);
                    }
                    boolean[] zArr2 = new boolean[this.a.length];
                    int i = 0;
                    while (true) {
                        m[] mVarArr = this.a;
                        if (i >= mVarArr.length) {
                            break;
                        }
                        m mVar = mVarArr[i];
                        zArr2[i] = P(mVar);
                        r rVar = p2.c[i];
                        if (zArr2[i]) {
                            if (rVar != mVar.c()) {
                                m(mVar);
                            } else if (zArr[i]) {
                                mVar.v(this.P0);
                            }
                        }
                        i++;
                    }
                    r(zArr2);
                } else {
                    this.w0.z(p);
                    if (p.d) {
                        p.a(v, Math.max(p.f.b, p.y(this.P0)), false);
                    }
                }
                F(true);
                if (this.B0.e != 4) {
                    U();
                    l1();
                    this.l0.f(2);
                    return;
                }
                return;
            }
            if (p == q) {
                z = false;
            }
        }
    }

    public final synchronized void o1(dw3<Boolean> dw3Var, long j) {
        long b2 = this.u0.b() + j;
        boolean z = false;
        while (!dw3Var.get().booleanValue() && j > 0) {
            try {
                this.u0.d();
                wait(j);
            } catch (InterruptedException unused) {
                z = true;
            }
            j = b2 - this.u0.b();
        }
        if (z) {
            Thread.currentThread().interrupt();
        }
    }

    public final void p(int i, boolean z) throws ExoPlaybackException {
        m mVar = this.a[i];
        if (P(mVar)) {
            return;
        }
        i q = this.w0.q();
        boolean z2 = q == this.w0.p();
        androidx.media3.exoplayer.trackselection.g o = q.o();
        v63 v63Var = o.b[i];
        androidx.media3.common.j[] w = w(o.c[i]);
        boolean z3 = b1() && this.B0.e == 3;
        boolean z4 = !z && z3;
        this.N0++;
        this.f0.add(mVar);
        mVar.j(v63Var, w, q.c[i], this.P0, z4, z2, q.m(), q.l());
        mVar.s(11, new a());
        this.s0.c(mVar);
        if (z3) {
            mVar.start();
        }
    }

    /* JADX WARN: Removed duplicated region for block: B:38:0x00b9  */
    /* JADX WARN: Removed duplicated region for block: B:41:0x00bf  */
    /* JADX WARN: Removed duplicated region for block: B:42:0x00c2  */
    /* JADX WARN: Removed duplicated region for block: B:45:0x00c7  */
    /* JADX WARN: Removed duplicated region for block: B:46:0x00ca  */
    /* JADX WARN: Removed duplicated region for block: B:49:0x00cf  */
    /* JADX WARN: Removed duplicated region for block: B:50:0x00d4  */
    /* JADX WARN: Removed duplicated region for block: B:53:0x00f8  */
    /* JADX WARN: Removed duplicated region for block: B:66:? A[RETURN, SYNTHETIC] */
    /*
        Code decompiled incorrectly, please refer to instructions dump.
        To view partially-correct code enable 'Show inconsistent code' option in preferences
    */
    public final void p0(boolean r29, boolean r30, boolean r31, boolean r32) {
        /*
            Method dump skipped, instructions count: 254
            To view this dump change 'Code comments level' option to 'DEBUG'
        */
        throw new UnsupportedOperationException("Method not decompiled: androidx.media3.exoplayer.h.p0(boolean, boolean, boolean, boolean):void");
    }

    public final void q() throws ExoPlaybackException {
        r(new boolean[this.a.length]);
    }

    public final void q0() {
        i p = this.w0.p();
        this.F0 = p != null && p.f.h && this.E0;
    }

    public final void r(boolean[] zArr) throws ExoPlaybackException {
        i q = this.w0.q();
        androidx.media3.exoplayer.trackselection.g o = q.o();
        for (int i = 0; i < this.a.length; i++) {
            if (!o.c(i) && this.f0.remove(this.a[i])) {
                this.a[i].reset();
            }
        }
        for (int i2 = 0; i2 < this.a.length; i2++) {
            if (o.c(i2)) {
                p(i2, zArr[i2]);
            }
        }
        q.g = true;
    }

    public final void r0(long j) throws ExoPlaybackException {
        m[] mVarArr;
        i p = this.w0.p();
        long z = p == null ? j + 1000000000000L : p.z(j);
        this.P0 = z;
        this.s0.d(z);
        for (m mVar : this.a) {
            if (P(mVar)) {
                mVar.v(this.P0);
            }
        }
        d0();
    }

    public final void s(m mVar) throws ExoPlaybackException {
        if (mVar.getState() == 2) {
            mVar.stop();
        }
    }

    public void t(long j) {
    }

    public final ImmutableList<Metadata> u(androidx.media3.exoplayer.trackselection.c[] cVarArr) {
        ImmutableList.a aVar = new ImmutableList.a();
        boolean z = false;
        for (androidx.media3.exoplayer.trackselection.c cVar : cVarArr) {
            if (cVar != null) {
                Metadata metadata = cVar.j(0).n0;
                if (metadata == null) {
                    aVar.a(new Metadata(new Metadata.Entry[0]));
                } else {
                    aVar.a(metadata);
                    z = true;
                }
            }
        }
        return z ? aVar.l() : ImmutableList.of();
    }

    public final void u0(u uVar, u uVar2) {
        if (uVar.q() && uVar2.q()) {
            return;
        }
        for (int size = this.t0.size() - 1; size >= 0; size--) {
            if (!t0(this.t0.get(size), uVar, uVar2, this.I0, this.J0, this.o0, this.p0)) {
                this.t0.get(size).a.k(false);
                this.t0.remove(size);
            }
        }
        Collections.sort(this.t0);
    }

    public final long v() {
        lr2 lr2Var = this.B0;
        return x(lr2Var.a, lr2Var.b.a, lr2Var.r);
    }

    public final long x(u uVar, Object obj, long j) {
        uVar.n(uVar.h(obj, this.p0).g0, this.o0);
        u.c cVar = this.o0;
        if (cVar.j0 != CellBase.ID_SYSTEM_MESSAGE_REQUEST_CLOSED && cVar.i()) {
            u.c cVar2 = this.o0;
            if (cVar2.m0) {
                return androidx.media3.common.util.b.y0(cVar2.d() - this.o0.j0) - (j + this.p0.p());
            }
        }
        return CellBase.ID_SYSTEM_MESSAGE_REQUEST_CLOSED;
    }

    public final long y() {
        i q = this.w0.q();
        if (q == null) {
            return 0L;
        }
        long l = q.l();
        if (!q.d) {
            return l;
        }
        int i = 0;
        while (true) {
            m[] mVarArr = this.a;
            if (i >= mVarArr.length) {
                return l;
            }
            if (P(mVarArr[i]) && this.a[i].c() == q.c[i]) {
                long u = this.a[i].u();
                if (u == Long.MIN_VALUE) {
                    return Long.MIN_VALUE;
                }
                l = Math.max(u, l);
            }
            i++;
        }
    }

    public final void y0(long j, long j2) {
        this.l0.h(2, j + j2);
    }

    public final Pair<j.b, Long> z(u uVar) {
        if (uVar.q()) {
            return Pair.create(lr2.k(), 0L);
        }
        Pair<Object, Long> j = uVar.j(this.o0, this.p0, uVar.a(this.J0), CellBase.ID_SYSTEM_MESSAGE_REQUEST_CLOSED);
        j.b B = this.w0.B(uVar, j.first, 0L);
        long longValue = ((Long) j.second).longValue();
        if (B.b()) {
            uVar.h(B.a, this.p0);
            longValue = B.c == this.p0.m(B.b) ? this.p0.i() : 0L;
        }
        return Pair.create(B, Long.valueOf(longValue));
    }

    public void z0(u uVar, int i, long j) {
        this.l0.j(3, new C0036h(uVar, i, j)).a();
    }
}
