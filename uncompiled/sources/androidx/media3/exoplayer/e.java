package androidx.media3.exoplayer;

import androidx.media3.common.p;

/* compiled from: DefaultMediaClock.java */
/* loaded from: classes.dex */
public final class e implements u52 {
    public final ms3 a;
    public final a f0;
    public m g0;
    public u52 h0;
    public boolean i0 = true;
    public boolean j0;

    /* compiled from: DefaultMediaClock.java */
    /* loaded from: classes.dex */
    public interface a {
        void n(p pVar);
    }

    public e(a aVar, tz tzVar) {
        this.f0 = aVar;
        this.a = new ms3(tzVar);
    }

    public void a(m mVar) {
        if (mVar == this.g0) {
            this.h0 = null;
            this.g0 = null;
            this.i0 = true;
        }
    }

    @Override // defpackage.u52
    public void b(p pVar) {
        u52 u52Var = this.h0;
        if (u52Var != null) {
            u52Var.b(pVar);
            pVar = this.h0.e();
        }
        this.a.b(pVar);
    }

    public void c(m mVar) throws ExoPlaybackException {
        u52 u52Var;
        u52 x = mVar.x();
        if (x == null || x == (u52Var = this.h0)) {
            return;
        }
        if (u52Var == null) {
            this.h0 = x;
            this.g0 = mVar;
            x.b(this.a.e());
            return;
        }
        throw ExoPlaybackException.createForUnexpected(new IllegalStateException("Multiple renderer media clocks enabled."));
    }

    public void d(long j) {
        this.a.a(j);
    }

    @Override // defpackage.u52
    public p e() {
        u52 u52Var = this.h0;
        if (u52Var != null) {
            return u52Var.e();
        }
        return this.a.e();
    }

    public final boolean f(boolean z) {
        m mVar = this.g0;
        return mVar == null || mVar.d() || (!this.g0.f() && (z || this.g0.i()));
    }

    public void g() {
        this.j0 = true;
        this.a.c();
    }

    public void h() {
        this.j0 = false;
        this.a.d();
    }

    public long i(boolean z) {
        j(z);
        return n();
    }

    public final void j(boolean z) {
        if (f(z)) {
            this.i0 = true;
            if (this.j0) {
                this.a.c();
                return;
            }
            return;
        }
        u52 u52Var = (u52) ii.e(this.h0);
        long n = u52Var.n();
        if (this.i0) {
            if (n < this.a.n()) {
                this.a.d();
                return;
            }
            this.i0 = false;
            if (this.j0) {
                this.a.c();
            }
        }
        this.a.a(n);
        p e = u52Var.e();
        if (e.equals(this.a.e())) {
            return;
        }
        this.a.b(e);
        this.f0.n(e);
    }

    @Override // defpackage.u52
    public long n() {
        if (this.i0) {
            return this.a.n();
        }
        return ((u52) ii.e(this.h0)).n();
    }
}
