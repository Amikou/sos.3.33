package androidx.media3.exoplayer;

import android.content.Context;
import android.media.AudioFocusRequest;
import android.media.AudioManager;
import android.os.Handler;
import androidx.media3.exoplayer.b;

/* compiled from: AudioFocusManager.java */
/* loaded from: classes.dex */
public final class b {
    public final AudioManager a;
    public final a b;
    public InterfaceC0032b c;
    public androidx.media3.common.b d;
    public int f;
    public AudioFocusRequest h;
    public boolean i;
    public float g = 1.0f;
    public int e = 0;

    /* compiled from: AudioFocusManager.java */
    /* loaded from: classes.dex */
    public class a implements AudioManager.OnAudioFocusChangeListener {
        public final Handler a;

        public a(Handler handler) {
            this.a = handler;
        }

        /* JADX INFO: Access modifiers changed from: private */
        public /* synthetic */ void b(int i) {
            b.this.h(i);
        }

        @Override // android.media.AudioManager.OnAudioFocusChangeListener
        public void onAudioFocusChange(final int i) {
            this.a.post(new Runnable() { // from class: gj
                @Override // java.lang.Runnable
                public final void run() {
                    b.a.this.b(i);
                }
            });
        }
    }

    /* compiled from: AudioFocusManager.java */
    /* renamed from: androidx.media3.exoplayer.b$b  reason: collision with other inner class name */
    /* loaded from: classes.dex */
    public interface InterfaceC0032b {
        void A(float f);

        void D(int i);
    }

    public b(Context context, Handler handler, InterfaceC0032b interfaceC0032b) {
        this.a = (AudioManager) ii.e((AudioManager) context.getApplicationContext().getSystemService("audio"));
        this.c = interfaceC0032b;
        this.b = new a(handler);
    }

    /* JADX WARN: Can't fix incorrect switch cases order, some code will duplicate */
    public static int e(androidx.media3.common.b bVar) {
        if (bVar == null) {
            return 0;
        }
        switch (bVar.g0) {
            case 0:
                p12.i("AudioFocusManager", "Specify a proper usage in the audio attributes for audio focus handling. Using AUDIOFOCUS_GAIN by default.");
                return 1;
            case 1:
            case 14:
                return 1;
            case 2:
            case 4:
                return 2;
            case 3:
                return 0;
            case 5:
            case 6:
            case 7:
            case 8:
            case 9:
            case 10:
            case 12:
            case 13:
                break;
            case 11:
                if (bVar.a == 1) {
                    return 2;
                }
                break;
            case 15:
            default:
                p12.i("AudioFocusManager", "Unidentified audio usage: " + bVar.g0);
                return 0;
            case 16:
                return androidx.media3.common.util.b.a >= 19 ? 4 : 2;
        }
        return 3;
    }

    public final void a() {
        this.a.abandonAudioFocus(this.b);
    }

    public final void b() {
        if (this.e == 0) {
            return;
        }
        if (androidx.media3.common.util.b.a >= 26) {
            c();
        } else {
            a();
        }
        n(0);
    }

    public final void c() {
        AudioFocusRequest audioFocusRequest = this.h;
        if (audioFocusRequest != null) {
            this.a.abandonAudioFocusRequest(audioFocusRequest);
        }
    }

    public final void f(int i) {
        InterfaceC0032b interfaceC0032b = this.c;
        if (interfaceC0032b != null) {
            interfaceC0032b.D(i);
        }
    }

    public float g() {
        return this.g;
    }

    public final void h(int i) {
        if (i == -3 || i == -2) {
            if (i != -2 && !q()) {
                n(3);
                return;
            }
            f(0);
            n(2);
        } else if (i == -1) {
            f(-1);
            b();
        } else if (i != 1) {
            p12.i("AudioFocusManager", "Unknown focus change type: " + i);
        } else {
            n(1);
            f(1);
        }
    }

    public void i() {
        this.c = null;
        b();
    }

    public final int j() {
        if (this.e == 1) {
            return 1;
        }
        if ((androidx.media3.common.util.b.a >= 26 ? l() : k()) == 1) {
            n(1);
            return 1;
        }
        n(0);
        return -1;
    }

    public final int k() {
        return this.a.requestAudioFocus(this.b, androidx.media3.common.util.b.c0(((androidx.media3.common.b) ii.e(this.d)).g0), this.f);
    }

    public final int l() {
        AudioFocusRequest.Builder builder;
        AudioFocusRequest audioFocusRequest = this.h;
        if (audioFocusRequest == null || this.i) {
            if (audioFocusRequest == null) {
                builder = new AudioFocusRequest.Builder(this.f);
            } else {
                builder = new AudioFocusRequest.Builder(this.h);
            }
            this.h = builder.setAudioAttributes(((androidx.media3.common.b) ii.e(this.d)).a().a).setWillPauseWhenDucked(q()).setOnAudioFocusChangeListener(this.b).build();
            this.i = false;
        }
        return this.a.requestAudioFocus(this.h);
    }

    public void m(androidx.media3.common.b bVar) {
        if (androidx.media3.common.util.b.c(this.d, bVar)) {
            return;
        }
        this.d = bVar;
        int e = e(bVar);
        this.f = e;
        boolean z = true;
        if (e != 1 && e != 0) {
            z = false;
        }
        ii.b(z, "Automatic handling of audio focus is only available for USAGE_MEDIA and USAGE_GAME.");
    }

    public final void n(int i) {
        if (this.e == i) {
            return;
        }
        this.e = i;
        float f = i == 3 ? 0.2f : 1.0f;
        if (this.g == f) {
            return;
        }
        this.g = f;
        InterfaceC0032b interfaceC0032b = this.c;
        if (interfaceC0032b != null) {
            interfaceC0032b.A(f);
        }
    }

    public final boolean o(int i) {
        return i == 1 || this.f != 1;
    }

    public int p(boolean z, int i) {
        if (o(i)) {
            b();
            return z ? 1 : -1;
        } else if (z) {
            return j();
        } else {
            return -1;
        }
    }

    public final boolean q() {
        androidx.media3.common.b bVar = this.d;
        return bVar != null && bVar.a == 1;
    }
}
