package androidx.media3.exoplayer;

import android.os.Bundle;
import android.os.SystemClock;
import android.text.TextUtils;
import androidx.media3.common.PlaybackException;
import androidx.media3.common.e;
import java.io.IOException;
import okhttp3.internal.ws.WebSocketProtocol;

/* loaded from: classes.dex */
public final class ExoPlaybackException extends PlaybackException {
    public static final e.a<ExoPlaybackException> CREATOR = ez0.a;
    public static final int TYPE_REMOTE = 3;
    public static final int TYPE_RENDERER = 1;
    public static final int TYPE_SOURCE = 0;
    public static final int TYPE_UNEXPECTED = 2;
    public final boolean isRecoverable;
    public final j62 mediaPeriodId;
    public final androidx.media3.common.j rendererFormat;
    public final int rendererFormatSupport;
    public final int rendererIndex;
    public final String rendererName;
    public final int type;

    public ExoPlaybackException(int i, Throwable th, int i2) {
        this(i, th, null, i2, null, -1, null, 4, false);
    }

    public static ExoPlaybackException createForRemote(String str) {
        return new ExoPlaybackException(3, null, str, 1001, null, -1, null, 4, false);
    }

    public static ExoPlaybackException createForRenderer(Throwable th, String str, int i, androidx.media3.common.j jVar, int i2, boolean z, int i3) {
        return new ExoPlaybackException(1, th, null, i3, str, i, jVar, jVar == null ? 4 : i2, z);
    }

    public static ExoPlaybackException createForSource(IOException iOException, int i) {
        return new ExoPlaybackException(0, iOException, i);
    }

    @Deprecated
    public static ExoPlaybackException createForUnexpected(RuntimeException runtimeException) {
        return createForUnexpected(runtimeException, 1000);
    }

    public static /* synthetic */ ExoPlaybackException d(Bundle bundle) {
        return new ExoPlaybackException(bundle);
    }

    public static String e(int i, String str, String str2, int i2, androidx.media3.common.j jVar, int i3) {
        String str3;
        if (i == 0) {
            str3 = "Source error";
        } else if (i != 1) {
            str3 = i != 3 ? "Unexpected runtime error" : "Remote error";
        } else {
            str3 = str2 + " error, index=" + i2 + ", format=" + jVar + ", format_supported=" + androidx.media3.common.util.b.T(i3);
        }
        if (TextUtils.isEmpty(str)) {
            return str3;
        }
        return str3 + ": " + str;
    }

    public ExoPlaybackException copyWithMediaPeriodId(j62 j62Var) {
        return new ExoPlaybackException((String) androidx.media3.common.util.b.j(getMessage()), getCause(), this.errorCode, this.type, this.rendererName, this.rendererIndex, this.rendererFormat, this.rendererFormatSupport, j62Var, this.timestampMs, this.isRecoverable);
    }

    @Override // androidx.media3.common.PlaybackException
    public boolean errorInfoEquals(PlaybackException playbackException) {
        if (super.errorInfoEquals(playbackException)) {
            ExoPlaybackException exoPlaybackException = (ExoPlaybackException) androidx.media3.common.util.b.j(playbackException);
            return this.type == exoPlaybackException.type && androidx.media3.common.util.b.c(this.rendererName, exoPlaybackException.rendererName) && this.rendererIndex == exoPlaybackException.rendererIndex && androidx.media3.common.util.b.c(this.rendererFormat, exoPlaybackException.rendererFormat) && this.rendererFormatSupport == exoPlaybackException.rendererFormatSupport && androidx.media3.common.util.b.c(this.mediaPeriodId, exoPlaybackException.mediaPeriodId) && this.isRecoverable == exoPlaybackException.isRecoverable;
        }
        return false;
    }

    public Exception getRendererException() {
        ii.g(this.type == 1);
        return (Exception) ii.e(getCause());
    }

    public IOException getSourceException() {
        ii.g(this.type == 0);
        return (IOException) ii.e(getCause());
    }

    public RuntimeException getUnexpectedException() {
        ii.g(this.type == 2);
        return (RuntimeException) ii.e(getCause());
    }

    @Override // androidx.media3.common.PlaybackException, androidx.media3.common.e
    public Bundle toBundle() {
        Bundle bundle = super.toBundle();
        bundle.putInt(PlaybackException.keyForField(1001), this.type);
        bundle.putString(PlaybackException.keyForField(PlaybackException.ERROR_CODE_BEHIND_LIVE_WINDOW), this.rendererName);
        bundle.putInt(PlaybackException.keyForField(PlaybackException.ERROR_CODE_TIMEOUT), this.rendererIndex);
        if (this.rendererFormat != null) {
            bundle.putBundle(PlaybackException.keyForField(PlaybackException.ERROR_CODE_FAILED_RUNTIME_CHECK), this.rendererFormat.toBundle());
        }
        bundle.putInt(PlaybackException.keyForField(WebSocketProtocol.CLOSE_NO_STATUS_CODE), this.rendererFormatSupport);
        bundle.putBoolean(PlaybackException.keyForField(1006), this.isRecoverable);
        return bundle;
    }

    public ExoPlaybackException(int i, Throwable th, String str, int i2, String str2, int i3, androidx.media3.common.j jVar, int i4, boolean z) {
        this(e(i, str, str2, i3, jVar, i4), th, i2, i, str2, i3, jVar, i4, null, SystemClock.elapsedRealtime(), z);
    }

    public static ExoPlaybackException createForUnexpected(RuntimeException runtimeException, int i) {
        return new ExoPlaybackException(2, runtimeException, i);
    }

    public ExoPlaybackException(Bundle bundle) {
        super(bundle);
        this.type = bundle.getInt(PlaybackException.keyForField(1001), 2);
        this.rendererName = bundle.getString(PlaybackException.keyForField(PlaybackException.ERROR_CODE_BEHIND_LIVE_WINDOW));
        this.rendererIndex = bundle.getInt(PlaybackException.keyForField(PlaybackException.ERROR_CODE_TIMEOUT), -1);
        Bundle bundle2 = bundle.getBundle(PlaybackException.keyForField(PlaybackException.ERROR_CODE_FAILED_RUNTIME_CHECK));
        this.rendererFormat = bundle2 == null ? null : androidx.media3.common.j.L0.a(bundle2);
        this.rendererFormatSupport = bundle.getInt(PlaybackException.keyForField(WebSocketProtocol.CLOSE_NO_STATUS_CODE), 4);
        this.isRecoverable = bundle.getBoolean(PlaybackException.keyForField(1006), false);
        this.mediaPeriodId = null;
    }

    public ExoPlaybackException(String str, Throwable th, int i, int i2, String str2, int i3, androidx.media3.common.j jVar, int i4, j62 j62Var, long j, boolean z) {
        super(str, th, i, j);
        boolean z2 = false;
        ii.a(!z || i2 == 1);
        ii.a((th != null || i2 == 3) ? true : z2);
        this.type = i2;
        this.rendererName = str2;
        this.rendererIndex = i3;
        this.rendererFormat = jVar;
        this.rendererFormatSupport = i4;
        this.mediaPeriodId = j62Var;
        this.isRecoverable = z;
    }
}
