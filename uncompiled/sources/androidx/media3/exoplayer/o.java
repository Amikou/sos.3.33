package androidx.media3.exoplayer;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.media.AudioManager;
import android.os.Handler;
import androidx.media3.exoplayer.o;

/* compiled from: StreamVolumeManager.java */
/* loaded from: classes.dex */
public final class o {
    public final Context a;
    public final Handler b;
    public final b c;
    public final AudioManager d;
    public c e;
    public int f;
    public int g;
    public boolean h;

    /* compiled from: StreamVolumeManager.java */
    /* loaded from: classes.dex */
    public interface b {
        void G(int i, boolean z);

        void n(int i);
    }

    /* compiled from: StreamVolumeManager.java */
    /* loaded from: classes.dex */
    public final class c extends BroadcastReceiver {
        public c() {
        }

        @Override // android.content.BroadcastReceiver
        public void onReceive(Context context, Intent intent) {
            Handler handler = o.this.b;
            final o oVar = o.this;
            handler.post(new Runnable() { // from class: gu3
                @Override // java.lang.Runnable
                public final void run() {
                    o.b(o.this);
                }
            });
        }
    }

    public o(Context context, Handler handler, b bVar) {
        Context applicationContext = context.getApplicationContext();
        this.a = applicationContext;
        this.b = handler;
        this.c = bVar;
        AudioManager audioManager = (AudioManager) ii.i((AudioManager) applicationContext.getSystemService("audio"));
        this.d = audioManager;
        this.f = 3;
        this.g = f(audioManager, 3);
        this.h = e(audioManager, this.f);
        c cVar = new c();
        try {
            applicationContext.registerReceiver(cVar, new IntentFilter("android.media.VOLUME_CHANGED_ACTION"));
            this.e = cVar;
        } catch (RuntimeException e) {
            p12.j("StreamVolumeManager", "Error registering stream volume receiver", e);
        }
    }

    public static /* synthetic */ void b(o oVar) {
        oVar.i();
    }

    public static boolean e(AudioManager audioManager, int i) {
        if (androidx.media3.common.util.b.a >= 23) {
            return audioManager.isStreamMute(i);
        }
        return f(audioManager, i) == 0;
    }

    public static int f(AudioManager audioManager, int i) {
        try {
            return audioManager.getStreamVolume(i);
        } catch (RuntimeException e) {
            p12.j("StreamVolumeManager", "Could not retrieve stream volume for stream type " + i, e);
            return audioManager.getStreamMaxVolume(i);
        }
    }

    public int c() {
        return this.d.getStreamMaxVolume(this.f);
    }

    public int d() {
        if (androidx.media3.common.util.b.a >= 28) {
            return this.d.getStreamMinVolume(this.f);
        }
        return 0;
    }

    public void g() {
        c cVar = this.e;
        if (cVar != null) {
            try {
                this.a.unregisterReceiver(cVar);
            } catch (RuntimeException e) {
                p12.j("StreamVolumeManager", "Error unregistering stream volume receiver", e);
            }
            this.e = null;
        }
    }

    public void h(int i) {
        if (this.f == i) {
            return;
        }
        this.f = i;
        i();
        this.c.n(i);
    }

    public final void i() {
        int f = f(this.d, this.f);
        boolean e = e(this.d, this.f);
        if (this.g == f && this.h == e) {
            return;
        }
        this.g = f;
        this.h = e;
        this.c.G(f, e);
    }
}
