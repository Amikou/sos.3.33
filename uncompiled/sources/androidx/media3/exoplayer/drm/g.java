package androidx.media3.exoplayer.drm;

import android.media.DeniedByServerException;
import android.media.MediaCryptoException;
import android.media.MediaDrmException;
import android.media.NotProvisionedException;
import androidx.media3.common.DrmInitData;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;

/* compiled from: ExoMediaDrm.java */
/* loaded from: classes.dex */
public interface g {

    /* compiled from: ExoMediaDrm.java */
    /* loaded from: classes.dex */
    public static final class a {
        public final byte[] a;
        public final String b;

        public a(byte[] bArr, String str, int i) {
            this.a = bArr;
            this.b = str;
        }

        public byte[] a() {
            return this.a;
        }

        public String b() {
            return this.b;
        }
    }

    /* compiled from: ExoMediaDrm.java */
    /* loaded from: classes.dex */
    public interface b {
        void a(g gVar, byte[] bArr, int i, int i2, byte[] bArr2);
    }

    /* compiled from: ExoMediaDrm.java */
    /* loaded from: classes.dex */
    public interface c {
        g a(UUID uuid);
    }

    /* compiled from: ExoMediaDrm.java */
    /* loaded from: classes.dex */
    public static final class d {
        public final byte[] a;
        public final String b;

        public d(byte[] bArr, String str) {
            this.a = bArr;
            this.b = str;
        }

        public byte[] a() {
            return this.a;
        }

        public String b() {
            return this.b;
        }
    }

    void a();

    Map<String, String> b(byte[] bArr);

    d c();

    qa0 d(byte[] bArr) throws MediaCryptoException;

    byte[] e() throws MediaDrmException;

    boolean f(byte[] bArr, String str);

    void g(byte[] bArr, byte[] bArr2);

    void h(byte[] bArr);

    byte[] i(byte[] bArr, byte[] bArr2) throws NotProvisionedException, DeniedByServerException;

    void j(b bVar);

    void k(byte[] bArr) throws DeniedByServerException;

    a l(byte[] bArr, List<DrmInitData.SchemeData> list, int i, HashMap<String, String> hashMap) throws NotProvisionedException;

    int m();

    void n(byte[] bArr, ks2 ks2Var);
}
