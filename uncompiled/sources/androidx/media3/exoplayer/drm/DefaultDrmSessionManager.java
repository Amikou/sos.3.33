package androidx.media3.exoplayer.drm;

import android.annotation.SuppressLint;
import android.media.ResourceBusyException;
import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import android.os.SystemClock;
import androidx.media3.common.DrmInitData;
import androidx.media3.common.PlaybackException;
import androidx.media3.exoplayer.drm.DefaultDrmSession;
import androidx.media3.exoplayer.drm.DefaultDrmSessionManager;
import androidx.media3.exoplayer.drm.DrmSession;
import androidx.media3.exoplayer.drm.b;
import androidx.media3.exoplayer.drm.c;
import androidx.media3.exoplayer.drm.g;
import com.google.common.collect.ImmutableList;
import com.google.common.collect.ImmutableSet;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Set;
import java.util.UUID;
import zendesk.support.request.CellBase;

/* loaded from: classes.dex */
public class DefaultDrmSessionManager implements androidx.media3.exoplayer.drm.c {
    public final UUID b;
    public final g.c c;
    public final j d;
    public final HashMap<String, String> e;
    public final boolean f;
    public final int[] g;
    public final boolean h;
    public final f i;
    public final androidx.media3.exoplayer.upstream.b j;
    public final g k;
    public final long l;
    public final List<DefaultDrmSession> m;
    public final Set<e> n;
    public final Set<DefaultDrmSession> o;
    public int p;
    public androidx.media3.exoplayer.drm.g q;
    public DefaultDrmSession r;
    public DefaultDrmSession s;
    public Looper t;
    public Handler u;
    public int v;
    public byte[] w;
    public ks2 x;
    public volatile d y;

    /* loaded from: classes.dex */
    public static final class MissingSchemeDataException extends Exception {
        public MissingSchemeDataException(UUID uuid) {
            super("Media does not support uuid: " + uuid);
        }
    }

    /* loaded from: classes.dex */
    public static final class b {
        public boolean d;
        public boolean f;
        public final HashMap<String, String> a = new HashMap<>();
        public UUID b = ft.d;
        public g.c c = h.d;
        public androidx.media3.exoplayer.upstream.b g = new androidx.media3.exoplayer.upstream.a();
        public int[] e = new int[0];
        public long h = 300000;

        public DefaultDrmSessionManager a(j jVar) {
            return new DefaultDrmSessionManager(this.b, this.c, jVar, this.a, this.d, this.e, this.f, this.g, this.h);
        }

        public b b(boolean z) {
            this.d = z;
            return this;
        }

        public b c(boolean z) {
            this.f = z;
            return this;
        }

        public b d(int... iArr) {
            for (int i : iArr) {
                boolean z = true;
                if (i != 2 && i != 1) {
                    z = false;
                }
                ii.a(z);
            }
            this.e = (int[]) iArr.clone();
            return this;
        }

        public b e(UUID uuid, g.c cVar) {
            this.b = (UUID) ii.e(uuid);
            this.c = (g.c) ii.e(cVar);
            return this;
        }
    }

    /* loaded from: classes.dex */
    public class c implements g.b {
        public c() {
        }

        @Override // androidx.media3.exoplayer.drm.g.b
        public void a(androidx.media3.exoplayer.drm.g gVar, byte[] bArr, int i, int i2, byte[] bArr2) {
            ((d) ii.e(DefaultDrmSessionManager.this.y)).obtainMessage(i, bArr).sendToTarget();
        }
    }

    @SuppressLint({"HandlerLeak"})
    /* loaded from: classes.dex */
    public class d extends Handler {
        public d(Looper looper) {
            super(looper);
        }

        @Override // android.os.Handler
        public void handleMessage(Message message) {
            byte[] bArr = (byte[]) message.obj;
            if (bArr == null) {
                return;
            }
            for (DefaultDrmSession defaultDrmSession : DefaultDrmSessionManager.this.m) {
                if (defaultDrmSession.p(bArr)) {
                    defaultDrmSession.x(message.what);
                    return;
                }
            }
        }
    }

    /* loaded from: classes.dex */
    public class e implements c.b {
        public final b.a b;
        public DrmSession c;
        public boolean d;

        public e(b.a aVar) {
            this.b = aVar;
        }

        /* JADX INFO: Access modifiers changed from: private */
        public /* synthetic */ void e(androidx.media3.common.j jVar) {
            if (DefaultDrmSessionManager.this.p == 0 || this.d) {
                return;
            }
            DefaultDrmSessionManager defaultDrmSessionManager = DefaultDrmSessionManager.this;
            this.c = defaultDrmSessionManager.u((Looper) ii.e(defaultDrmSessionManager.t), this.b, jVar, false);
            DefaultDrmSessionManager.this.n.add(this);
        }

        /* JADX INFO: Access modifiers changed from: private */
        public /* synthetic */ void f() {
            if (this.d) {
                return;
            }
            DrmSession drmSession = this.c;
            if (drmSession != null) {
                drmSession.e(this.b);
            }
            DefaultDrmSessionManager.this.n.remove(this);
            this.d = true;
        }

        @Override // androidx.media3.exoplayer.drm.c.b
        public void a() {
            androidx.media3.common.util.b.G0((Handler) ii.e(DefaultDrmSessionManager.this.u), new Runnable() { // from class: zi0
                @Override // java.lang.Runnable
                public final void run() {
                    DefaultDrmSessionManager.e.this.f();
                }
            });
        }

        public void d(final androidx.media3.common.j jVar) {
            ((Handler) ii.e(DefaultDrmSessionManager.this.u)).post(new Runnable() { // from class: aj0
                @Override // java.lang.Runnable
                public final void run() {
                    DefaultDrmSessionManager.e.this.e(jVar);
                }
            });
        }
    }

    /* loaded from: classes.dex */
    public class f implements DefaultDrmSession.a {
        public final Set<DefaultDrmSession> a = new HashSet();
        public DefaultDrmSession b;

        public f(DefaultDrmSessionManager defaultDrmSessionManager) {
        }

        @Override // androidx.media3.exoplayer.drm.DefaultDrmSession.a
        public void a(Exception exc, boolean z) {
            this.b = null;
            ImmutableList copyOf = ImmutableList.copyOf((Collection) this.a);
            this.a.clear();
            af4 it = copyOf.iterator();
            while (it.hasNext()) {
                ((DefaultDrmSession) it.next()).z(exc, z);
            }
        }

        @Override // androidx.media3.exoplayer.drm.DefaultDrmSession.a
        public void b() {
            this.b = null;
            ImmutableList copyOf = ImmutableList.copyOf((Collection) this.a);
            this.a.clear();
            af4 it = copyOf.iterator();
            while (it.hasNext()) {
                ((DefaultDrmSession) it.next()).y();
            }
        }

        @Override // androidx.media3.exoplayer.drm.DefaultDrmSession.a
        public void c(DefaultDrmSession defaultDrmSession) {
            this.a.add(defaultDrmSession);
            if (this.b != null) {
                return;
            }
            this.b = defaultDrmSession;
            defaultDrmSession.D();
        }

        public void d(DefaultDrmSession defaultDrmSession) {
            this.a.remove(defaultDrmSession);
            if (this.b == defaultDrmSession) {
                this.b = null;
                if (this.a.isEmpty()) {
                    return;
                }
                DefaultDrmSession next = this.a.iterator().next();
                this.b = next;
                next.D();
            }
        }
    }

    /* loaded from: classes.dex */
    public class g implements DefaultDrmSession.b {
        public g() {
        }

        @Override // androidx.media3.exoplayer.drm.DefaultDrmSession.b
        public void a(DefaultDrmSession defaultDrmSession, int i) {
            if (DefaultDrmSessionManager.this.l != CellBase.ID_SYSTEM_MESSAGE_REQUEST_CLOSED) {
                DefaultDrmSessionManager.this.o.remove(defaultDrmSession);
                ((Handler) ii.e(DefaultDrmSessionManager.this.u)).removeCallbacksAndMessages(defaultDrmSession);
            }
        }

        @Override // androidx.media3.exoplayer.drm.DefaultDrmSession.b
        public void b(final DefaultDrmSession defaultDrmSession, int i) {
            if (i == 1 && DefaultDrmSessionManager.this.p > 0 && DefaultDrmSessionManager.this.l != CellBase.ID_SYSTEM_MESSAGE_REQUEST_CLOSED) {
                DefaultDrmSessionManager.this.o.add(defaultDrmSession);
                ((Handler) ii.e(DefaultDrmSessionManager.this.u)).postAtTime(new Runnable() { // from class: bj0
                    @Override // java.lang.Runnable
                    public final void run() {
                        DefaultDrmSession.this.e(null);
                    }
                }, defaultDrmSession, SystemClock.uptimeMillis() + DefaultDrmSessionManager.this.l);
            } else if (i == 0) {
                DefaultDrmSessionManager.this.m.remove(defaultDrmSession);
                if (DefaultDrmSessionManager.this.r == defaultDrmSession) {
                    DefaultDrmSessionManager.this.r = null;
                }
                if (DefaultDrmSessionManager.this.s == defaultDrmSession) {
                    DefaultDrmSessionManager.this.s = null;
                }
                DefaultDrmSessionManager.this.i.d(defaultDrmSession);
                if (DefaultDrmSessionManager.this.l != CellBase.ID_SYSTEM_MESSAGE_REQUEST_CLOSED) {
                    ((Handler) ii.e(DefaultDrmSessionManager.this.u)).removeCallbacksAndMessages(defaultDrmSession);
                    DefaultDrmSessionManager.this.o.remove(defaultDrmSession);
                }
            }
            DefaultDrmSessionManager.this.D();
        }
    }

    public static boolean v(DrmSession drmSession) {
        return drmSession.getState() == 1 && (androidx.media3.common.util.b.a < 19 || (((DrmSession.DrmSessionException) ii.e(drmSession.getError())).getCause() instanceof ResourceBusyException));
    }

    public static List<DrmInitData.SchemeData> z(DrmInitData drmInitData, UUID uuid, boolean z) {
        ArrayList arrayList = new ArrayList(drmInitData.h0);
        for (int i = 0; i < drmInitData.h0; i++) {
            DrmInitData.SchemeData e2 = drmInitData.e(i);
            if ((e2.d(uuid) || (ft.c.equals(uuid) && e2.d(ft.b))) && (e2.i0 != null || z)) {
                arrayList.add(e2);
            }
        }
        return arrayList;
    }

    public final synchronized void A(Looper looper) {
        Looper looper2 = this.t;
        if (looper2 == null) {
            this.t = looper;
            this.u = new Handler(looper);
        } else {
            ii.g(looper2 == looper);
            ii.e(this.u);
        }
    }

    public final DrmSession B(int i, boolean z) {
        androidx.media3.exoplayer.drm.g gVar = (androidx.media3.exoplayer.drm.g) ii.e(this.q);
        if ((gVar.m() == 2 && cc1.d) || androidx.media3.common.util.b.v0(this.g, i) == -1 || gVar.m() == 1) {
            return null;
        }
        DefaultDrmSession defaultDrmSession = this.r;
        if (defaultDrmSession == null) {
            DefaultDrmSession y = y(ImmutableList.of(), true, null, z);
            this.m.add(y);
            this.r = y;
        } else {
            defaultDrmSession.a(null);
        }
        return this.r;
    }

    public final void C(Looper looper) {
        if (this.y == null) {
            this.y = new d(looper);
        }
    }

    public final void D() {
        if (this.q != null && this.p == 0 && this.m.isEmpty() && this.n.isEmpty()) {
            ((androidx.media3.exoplayer.drm.g) ii.e(this.q)).a();
            this.q = null;
        }
    }

    public final void E() {
        af4 it = ImmutableSet.copyOf((Collection) this.o).iterator();
        while (it.hasNext()) {
            ((DrmSession) it.next()).e(null);
        }
    }

    public final void F() {
        af4 it = ImmutableSet.copyOf((Collection) this.n).iterator();
        while (it.hasNext()) {
            ((e) it.next()).a();
        }
    }

    public void G(int i, byte[] bArr) {
        ii.g(this.m.isEmpty());
        if (i == 1 || i == 3) {
            ii.e(bArr);
        }
        this.v = i;
        this.w = bArr;
    }

    public final void H(DrmSession drmSession, b.a aVar) {
        drmSession.e(aVar);
        if (this.l != CellBase.ID_SYSTEM_MESSAGE_REQUEST_CLOSED) {
            drmSession.e(null);
        }
    }

    @Override // androidx.media3.exoplayer.drm.c
    public final void a() {
        int i = this.p - 1;
        this.p = i;
        if (i != 0) {
            return;
        }
        if (this.l != CellBase.ID_SYSTEM_MESSAGE_REQUEST_CLOSED) {
            ArrayList arrayList = new ArrayList(this.m);
            for (int i2 = 0; i2 < arrayList.size(); i2++) {
                ((DefaultDrmSession) arrayList.get(i2)).e(null);
            }
        }
        F();
        D();
    }

    @Override // androidx.media3.exoplayer.drm.c
    public void b(Looper looper, ks2 ks2Var) {
        A(looper);
        this.x = ks2Var;
    }

    @Override // androidx.media3.exoplayer.drm.c
    public DrmSession c(b.a aVar, androidx.media3.common.j jVar) {
        ii.g(this.p > 0);
        ii.i(this.t);
        return u(this.t, aVar, jVar, true);
    }

    @Override // androidx.media3.exoplayer.drm.c
    public final void d() {
        int i = this.p;
        this.p = i + 1;
        if (i != 0) {
            return;
        }
        if (this.q == null) {
            androidx.media3.exoplayer.drm.g a2 = this.c.a(this.b);
            this.q = a2;
            a2.j(new c());
        } else if (this.l != CellBase.ID_SYSTEM_MESSAGE_REQUEST_CLOSED) {
            for (int i2 = 0; i2 < this.m.size(); i2++) {
                this.m.get(i2).a(null);
            }
        }
    }

    @Override // androidx.media3.exoplayer.drm.c
    public int e(androidx.media3.common.j jVar) {
        int m = ((androidx.media3.exoplayer.drm.g) ii.e(this.q)).m();
        DrmInitData drmInitData = jVar.s0;
        if (drmInitData == null) {
            if (androidx.media3.common.util.b.v0(this.g, y82.i(jVar.p0)) != -1) {
                return m;
            }
            return 0;
        } else if (w(drmInitData)) {
            return m;
        } else {
            return 1;
        }
    }

    @Override // androidx.media3.exoplayer.drm.c
    public c.b f(b.a aVar, androidx.media3.common.j jVar) {
        ii.g(this.p > 0);
        ii.i(this.t);
        e eVar = new e(aVar);
        eVar.d(jVar);
        return eVar;
    }

    public final DrmSession u(Looper looper, b.a aVar, androidx.media3.common.j jVar, boolean z) {
        List<DrmInitData.SchemeData> list;
        C(looper);
        DrmInitData drmInitData = jVar.s0;
        if (drmInitData == null) {
            return B(y82.i(jVar.p0), z);
        }
        DefaultDrmSession defaultDrmSession = null;
        if (this.w == null) {
            list = z((DrmInitData) ii.e(drmInitData), this.b, false);
            if (list.isEmpty()) {
                MissingSchemeDataException missingSchemeDataException = new MissingSchemeDataException(this.b);
                p12.d("DefaultDrmSessionMgr", "DRM error", missingSchemeDataException);
                if (aVar != null) {
                    aVar.l(missingSchemeDataException);
                }
                return new androidx.media3.exoplayer.drm.f(new DrmSession.DrmSessionException(missingSchemeDataException, PlaybackException.ERROR_CODE_DRM_CONTENT_ERROR));
            }
        } else {
            list = null;
        }
        if (!this.f) {
            defaultDrmSession = this.s;
        } else {
            Iterator<DefaultDrmSession> it = this.m.iterator();
            while (true) {
                if (!it.hasNext()) {
                    break;
                }
                DefaultDrmSession next = it.next();
                if (androidx.media3.common.util.b.c(next.a, list)) {
                    defaultDrmSession = next;
                    break;
                }
            }
        }
        if (defaultDrmSession == null) {
            defaultDrmSession = y(list, false, aVar, z);
            if (!this.f) {
                this.s = defaultDrmSession;
            }
            this.m.add(defaultDrmSession);
        } else {
            defaultDrmSession.a(aVar);
        }
        return defaultDrmSession;
    }

    public final boolean w(DrmInitData drmInitData) {
        if (this.w != null) {
            return true;
        }
        if (z(drmInitData, this.b, true).isEmpty()) {
            if (drmInitData.h0 != 1 || !drmInitData.e(0).d(ft.b)) {
                return false;
            }
            p12.i("DefaultDrmSessionMgr", "DrmInitData only contains common PSSH SchemeData. Assuming support for: " + this.b);
        }
        String str = drmInitData.g0;
        if (str == null || "cenc".equals(str)) {
            return true;
        }
        return "cbcs".equals(str) ? androidx.media3.common.util.b.a >= 25 : ("cbc1".equals(str) || "cens".equals(str)) ? false : true;
    }

    public final DefaultDrmSession x(List<DrmInitData.SchemeData> list, boolean z, b.a aVar) {
        ii.e(this.q);
        DefaultDrmSession defaultDrmSession = new DefaultDrmSession(this.b, this.q, this.i, this.k, list, this.v, this.h | z, z, this.w, this.e, this.d, (Looper) ii.e(this.t), this.j, (ks2) ii.e(this.x));
        defaultDrmSession.a(aVar);
        if (this.l != CellBase.ID_SYSTEM_MESSAGE_REQUEST_CLOSED) {
            defaultDrmSession.a(null);
        }
        return defaultDrmSession;
    }

    public final DefaultDrmSession y(List<DrmInitData.SchemeData> list, boolean z, b.a aVar, boolean z2) {
        DefaultDrmSession x = x(list, z, aVar);
        if (v(x) && !this.o.isEmpty()) {
            E();
            H(x, aVar);
            x = x(list, z, aVar);
        }
        if (v(x) && z2 && !this.n.isEmpty()) {
            F();
            if (!this.o.isEmpty()) {
                E();
            }
            H(x, aVar);
            return x(list, z, aVar);
        }
        return x;
    }

    public DefaultDrmSessionManager(UUID uuid, g.c cVar, j jVar, HashMap<String, String> hashMap, boolean z, int[] iArr, boolean z2, androidx.media3.exoplayer.upstream.b bVar, long j) {
        ii.e(uuid);
        ii.b(!ft.b.equals(uuid), "Use C.CLEARKEY_UUID instead");
        this.b = uuid;
        this.c = cVar;
        this.d = jVar;
        this.e = hashMap;
        this.f = z;
        this.g = iArr;
        this.h = z2;
        this.j = bVar;
        this.i = new f(this);
        this.k = new g();
        this.v = 0;
        this.m = new ArrayList();
        this.n = com.google.common.collect.h.h();
        this.o = com.google.common.collect.h.h();
        this.l = j;
    }
}
