package androidx.media3.exoplayer.drm;

import androidx.media3.exoplayer.drm.g;
import java.util.UUID;

/* compiled from: MediaDrmCallback.java */
/* loaded from: classes.dex */
public interface j {
    byte[] a(UUID uuid, g.d dVar) throws MediaDrmCallbackException;

    byte[] b(UUID uuid, g.a aVar) throws MediaDrmCallbackException;
}
