package androidx.media3.exoplayer.drm;

import android.media.DeniedByServerException;
import android.media.MediaDrm;
import android.media.MediaDrmResetException;
import android.media.NotProvisionedException;
import androidx.media3.common.PlaybackException;
import androidx.media3.exoplayer.drm.DefaultDrmSessionManager;

/* compiled from: DrmUtil.java */
/* loaded from: classes.dex */
public final class d {

    /* compiled from: DrmUtil.java */
    /* loaded from: classes.dex */
    public static final class a {
        public static boolean a(Throwable th) {
            return th instanceof DeniedByServerException;
        }

        public static boolean b(Throwable th) {
            return th instanceof NotProvisionedException;
        }
    }

    /* compiled from: DrmUtil.java */
    /* loaded from: classes.dex */
    public static final class b {
        public static boolean a(Throwable th) {
            return th instanceof MediaDrm.MediaDrmStateException;
        }

        public static int b(Throwable th) {
            return androidx.media3.common.util.b.R(androidx.media3.common.util.b.S(((MediaDrm.MediaDrmStateException) th).getDiagnosticInfo()));
        }
    }

    /* compiled from: DrmUtil.java */
    /* loaded from: classes.dex */
    public static final class c {
        public static boolean a(Throwable th) {
            return th instanceof MediaDrmResetException;
        }
    }

    public static int a(Exception exc, int i) {
        int i2 = androidx.media3.common.util.b.a;
        if (i2 >= 21 && b.a(exc)) {
            return b.b(exc);
        }
        if (i2 < 23 || !c.a(exc)) {
            if (i2 < 18 || !a.b(exc)) {
                if (i2 < 18 || !a.a(exc)) {
                    if (exc instanceof UnsupportedDrmException) {
                        return PlaybackException.ERROR_CODE_DRM_SCHEME_UNSUPPORTED;
                    }
                    if (exc instanceof DefaultDrmSessionManager.MissingSchemeDataException) {
                        return PlaybackException.ERROR_CODE_DRM_CONTENT_ERROR;
                    }
                    if (exc instanceof KeysExpiredException) {
                        return PlaybackException.ERROR_CODE_DRM_LICENSE_EXPIRED;
                    }
                    if (i == 1) {
                        return PlaybackException.ERROR_CODE_DRM_SYSTEM_ERROR;
                    }
                    if (i == 2) {
                        return PlaybackException.ERROR_CODE_DRM_LICENSE_ACQUISITION_FAILED;
                    }
                    if (i == 3) {
                        return PlaybackException.ERROR_CODE_DRM_PROVISIONING_FAILED;
                    }
                    throw new IllegalArgumentException();
                }
                return PlaybackException.ERROR_CODE_DRM_DEVICE_REVOKED;
            }
            return PlaybackException.ERROR_CODE_DRM_PROVISIONING_FAILED;
        }
        return PlaybackException.ERROR_CODE_DRM_SYSTEM_ERROR;
    }
}
