package androidx.media3.exoplayer.drm;

import android.annotation.SuppressLint;
import android.media.NotProvisionedException;
import android.os.Handler;
import android.os.HandlerThread;
import android.os.Looper;
import android.os.Message;
import android.os.SystemClock;
import android.util.Pair;
import androidx.media3.common.DrmInitData;
import androidx.media3.exoplayer.drm.DrmSession;
import androidx.media3.exoplayer.drm.b;
import androidx.media3.exoplayer.drm.g;
import androidx.media3.exoplayer.upstream.b;
import java.io.IOException;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;
import zendesk.support.request.CellBase;

/* loaded from: classes.dex */
public class DefaultDrmSession implements DrmSession {
    public final List<DrmInitData.SchemeData> a;
    public final g b;
    public final a c;
    public final b d;
    public final int e;
    public final boolean f;
    public final boolean g;
    public final HashMap<String, String> h;
    public final t80<b.a> i;
    public final androidx.media3.exoplayer.upstream.b j;
    public final ks2 k;
    public final j l;
    public final UUID m;
    public final e n;
    public int o;
    public int p;
    public HandlerThread q;
    public c r;
    public qa0 s;
    public DrmSession.DrmSessionException t;
    public byte[] u;
    public byte[] v;
    public g.a w;
    public g.d x;

    /* loaded from: classes.dex */
    public static final class UnexpectedDrmSessionException extends IOException {
        public UnexpectedDrmSessionException(Throwable th) {
            super(th);
        }
    }

    /* loaded from: classes.dex */
    public interface a {
        void a(Exception exc, boolean z);

        void b();

        void c(DefaultDrmSession defaultDrmSession);
    }

    /* loaded from: classes.dex */
    public interface b {
        void a(DefaultDrmSession defaultDrmSession, int i);

        void b(DefaultDrmSession defaultDrmSession, int i);
    }

    @SuppressLint({"HandlerLeak"})
    /* loaded from: classes.dex */
    public class c extends Handler {
        public boolean a;

        public c(Looper looper) {
            super(looper);
        }

        public final boolean a(Message message, MediaDrmCallbackException mediaDrmCallbackException) {
            IOException unexpectedDrmSessionException;
            d dVar = (d) message.obj;
            if (dVar.b) {
                int i = dVar.e + 1;
                dVar.e = i;
                if (i > DefaultDrmSession.this.j.c(3)) {
                    return false;
                }
                u02 u02Var = new u02(dVar.a, mediaDrmCallbackException.dataSpec, mediaDrmCallbackException.uriAfterRedirects, mediaDrmCallbackException.responseHeaders, SystemClock.elapsedRealtime(), SystemClock.elapsedRealtime() - dVar.c, mediaDrmCallbackException.bytesLoaded);
                g62 g62Var = new g62(3);
                if (mediaDrmCallbackException.getCause() instanceof IOException) {
                    unexpectedDrmSessionException = (IOException) mediaDrmCallbackException.getCause();
                } else {
                    unexpectedDrmSessionException = new UnexpectedDrmSessionException(mediaDrmCallbackException.getCause());
                }
                long a = DefaultDrmSession.this.j.a(new b.c(u02Var, g62Var, unexpectedDrmSessionException, dVar.e));
                if (a == CellBase.ID_SYSTEM_MESSAGE_REQUEST_CLOSED) {
                    return false;
                }
                synchronized (this) {
                    if (this.a) {
                        return false;
                    }
                    sendMessageDelayed(Message.obtain(message), a);
                    return true;
                }
            }
            return false;
        }

        public void b(int i, Object obj, boolean z) {
            obtainMessage(i, new d(u02.a(), z, SystemClock.elapsedRealtime(), obj)).sendToTarget();
        }

        public synchronized void c() {
            removeCallbacksAndMessages(null);
            this.a = true;
        }

        /* JADX WARN: Multi-variable type inference failed */
        /* JADX WARN: Type inference failed for: r1v0, types: [java.lang.Throwable, java.lang.Exception] */
        @Override // android.os.Handler
        public void handleMessage(Message message) {
            byte[] bArr;
            d dVar = (d) message.obj;
            try {
                int i = message.what;
                if (i == 0) {
                    DefaultDrmSession defaultDrmSession = DefaultDrmSession.this;
                    bArr = defaultDrmSession.l.a(defaultDrmSession.m, (g.d) dVar.d);
                } else if (i == 1) {
                    DefaultDrmSession defaultDrmSession2 = DefaultDrmSession.this;
                    bArr = defaultDrmSession2.l.b(defaultDrmSession2.m, (g.a) dVar.d);
                } else {
                    throw new RuntimeException();
                }
            } catch (MediaDrmCallbackException e) {
                boolean a = a(message, e);
                bArr = e;
                if (a) {
                    return;
                }
            } catch (Exception e2) {
                p12.j("DefaultDrmSession", "Key/provisioning request produced an unexpected exception. Not retrying.", e2);
                bArr = e2;
            }
            DefaultDrmSession.this.j.b(dVar.a);
            synchronized (this) {
                if (!this.a) {
                    DefaultDrmSession.this.n.obtainMessage(message.what, Pair.create(dVar.d, bArr)).sendToTarget();
                }
            }
        }
    }

    /* loaded from: classes.dex */
    public static final class d {
        public final long a;
        public final boolean b;
        public final long c;
        public final Object d;
        public int e;

        public d(long j, boolean z, long j2, Object obj) {
            this.a = j;
            this.b = z;
            this.c = j2;
            this.d = obj;
        }
    }

    @SuppressLint({"HandlerLeak"})
    /* loaded from: classes.dex */
    public class e extends Handler {
        public e(Looper looper) {
            super(looper);
        }

        @Override // android.os.Handler
        public void handleMessage(Message message) {
            Pair pair = (Pair) message.obj;
            Object obj = pair.first;
            Object obj2 = pair.second;
            int i = message.what;
            if (i == 0) {
                DefaultDrmSession.this.A(obj, obj2);
            } else if (i != 1) {
            } else {
                DefaultDrmSession.this.u(obj, obj2);
            }
        }
    }

    public DefaultDrmSession(UUID uuid, g gVar, a aVar, b bVar, List<DrmInitData.SchemeData> list, int i, boolean z, boolean z2, byte[] bArr, HashMap<String, String> hashMap, j jVar, Looper looper, androidx.media3.exoplayer.upstream.b bVar2, ks2 ks2Var) {
        if (i == 1 || i == 3) {
            ii.e(bArr);
        }
        this.m = uuid;
        this.c = aVar;
        this.d = bVar;
        this.b = gVar;
        this.e = i;
        this.f = z;
        this.g = z2;
        if (bArr != null) {
            this.v = bArr;
            this.a = null;
        } else {
            this.a = Collections.unmodifiableList((List) ii.e(list));
        }
        this.h = hashMap;
        this.l = jVar;
        this.i = new t80<>();
        this.j = bVar2;
        this.k = ks2Var;
        this.o = 2;
        this.n = new e(looper);
    }

    public final void A(Object obj, Object obj2) {
        if (obj == this.x) {
            if (this.o == 2 || q()) {
                this.x = null;
                if (obj2 instanceof Exception) {
                    this.c.a((Exception) obj2, false);
                    return;
                }
                try {
                    this.b.k((byte[]) obj2);
                    this.c.b();
                } catch (Exception e2) {
                    this.c.a(e2, true);
                }
            }
        }
    }

    public final boolean B() {
        if (q()) {
            return true;
        }
        try {
            byte[] e2 = this.b.e();
            this.u = e2;
            this.b.n(e2, this.k);
            this.s = this.b.d(this.u);
            this.o = 3;
            m(new k60() { // from class: ui0
                @Override // defpackage.k60
                public final void accept(Object obj) {
                    ((b.a) obj).k(r1);
                }
            });
            ii.e(this.u);
            return true;
        } catch (NotProvisionedException unused) {
            this.c.c(this);
            return false;
        } catch (Exception e3) {
            t(e3, 1);
            return false;
        }
    }

    public final void C(byte[] bArr, int i, boolean z) {
        try {
            this.w = this.b.l(bArr, this.a, i, this.h);
            ((c) androidx.media3.common.util.b.j(this.r)).b(1, ii.e(this.w), z);
        } catch (Exception e2) {
            v(e2, true);
        }
    }

    public void D() {
        this.x = this.b.c();
        ((c) androidx.media3.common.util.b.j(this.r)).b(0, ii.e(this.x), true);
    }

    public final boolean E() {
        try {
            this.b.g(this.u, this.v);
            return true;
        } catch (Exception e2) {
            t(e2, 1);
            return false;
        }
    }

    @Override // androidx.media3.exoplayer.drm.DrmSession
    public void a(b.a aVar) {
        if (this.p < 0) {
            p12.c("DefaultDrmSession", "Session reference count less than zero: " + this.p);
            this.p = 0;
        }
        if (aVar != null) {
            this.i.e(aVar);
        }
        int i = this.p + 1;
        this.p = i;
        if (i == 1) {
            ii.g(this.o == 2);
            HandlerThread handlerThread = new HandlerThread("ExoPlayer:DrmRequestHandler");
            this.q = handlerThread;
            handlerThread.start();
            this.r = new c(this.q.getLooper());
            if (B()) {
                n(true);
            }
        } else if (aVar != null && q() && this.i.count(aVar) == 1) {
            aVar.k(this.o);
        }
        this.d.a(this, this.p);
    }

    @Override // androidx.media3.exoplayer.drm.DrmSession
    public final UUID b() {
        return this.m;
    }

    @Override // androidx.media3.exoplayer.drm.DrmSession
    public boolean c() {
        return this.f;
    }

    @Override // androidx.media3.exoplayer.drm.DrmSession
    public Map<String, String> d() {
        byte[] bArr = this.u;
        if (bArr == null) {
            return null;
        }
        return this.b.b(bArr);
    }

    @Override // androidx.media3.exoplayer.drm.DrmSession
    public void e(b.a aVar) {
        int i = this.p;
        if (i <= 0) {
            p12.c("DefaultDrmSession", "release() called on a session that's already fully released.");
            return;
        }
        int i2 = i - 1;
        this.p = i2;
        if (i2 == 0) {
            this.o = 0;
            ((e) androidx.media3.common.util.b.j(this.n)).removeCallbacksAndMessages(null);
            ((c) androidx.media3.common.util.b.j(this.r)).c();
            this.r = null;
            ((HandlerThread) androidx.media3.common.util.b.j(this.q)).quit();
            this.q = null;
            this.s = null;
            this.t = null;
            this.w = null;
            this.x = null;
            byte[] bArr = this.u;
            if (bArr != null) {
                this.b.h(bArr);
                this.u = null;
            }
        }
        if (aVar != null) {
            this.i.i(aVar);
            if (this.i.count(aVar) == 0) {
                aVar.m();
            }
        }
        this.d.b(this, this.p);
    }

    @Override // androidx.media3.exoplayer.drm.DrmSession
    public boolean f(String str) {
        return this.b.f((byte[]) ii.i(this.u), str);
    }

    @Override // androidx.media3.exoplayer.drm.DrmSession
    public final qa0 g() {
        return this.s;
    }

    @Override // androidx.media3.exoplayer.drm.DrmSession
    public final DrmSession.DrmSessionException getError() {
        if (this.o == 1) {
            return this.t;
        }
        return null;
    }

    @Override // androidx.media3.exoplayer.drm.DrmSession
    public final int getState() {
        return this.o;
    }

    public final void m(k60<b.a> k60Var) {
        for (b.a aVar : this.i.elementSet()) {
            k60Var.accept(aVar);
        }
    }

    public final void n(boolean z) {
        if (this.g) {
            return;
        }
        byte[] bArr = (byte[]) androidx.media3.common.util.b.j(this.u);
        int i = this.e;
        if (i != 0 && i != 1) {
            if (i == 2) {
                if (this.v == null || E()) {
                    C(bArr, 2, z);
                }
            } else if (i != 3) {
            } else {
                ii.e(this.v);
                ii.e(this.u);
                C(this.v, 3, z);
            }
        } else if (this.v == null) {
            C(bArr, 1, z);
        } else if (this.o == 4 || E()) {
            long o = o();
            if (this.e != 0 || o > 60) {
                if (o <= 0) {
                    t(new KeysExpiredException(), 2);
                    return;
                }
                this.o = 4;
                m(yi0.a);
                return;
            }
            p12.b("DefaultDrmSession", "Offline license has expired or will expire soon. Remaining seconds: " + o);
            C(bArr, 2, z);
        }
    }

    public final long o() {
        if (ft.d.equals(this.m)) {
            Pair pair = (Pair) ii.e(ap4.b(this));
            return Math.min(((Long) pair.first).longValue(), ((Long) pair.second).longValue());
        }
        return Long.MAX_VALUE;
    }

    public boolean p(byte[] bArr) {
        return Arrays.equals(this.u, bArr);
    }

    public final boolean q() {
        int i = this.o;
        return i == 3 || i == 4;
    }

    public final void t(final Exception exc, int i) {
        this.t = new DrmSession.DrmSessionException(exc, androidx.media3.exoplayer.drm.d.a(exc, i));
        p12.d("DefaultDrmSession", "DRM session error", exc);
        m(new k60() { // from class: vi0
            @Override // defpackage.k60
            public final void accept(Object obj) {
                ((b.a) obj).l(exc);
            }
        });
        if (this.o != 4) {
            this.o = 1;
        }
    }

    public final void u(Object obj, Object obj2) {
        if (obj == this.w && q()) {
            this.w = null;
            if (obj2 instanceof Exception) {
                v((Exception) obj2, false);
                return;
            }
            try {
                byte[] bArr = (byte[]) obj2;
                if (this.e == 3) {
                    this.b.i((byte[]) androidx.media3.common.util.b.j(this.v), bArr);
                    m(xi0.a);
                    return;
                }
                byte[] i = this.b.i(this.u, bArr);
                int i2 = this.e;
                if ((i2 == 2 || (i2 == 0 && this.v != null)) && i != null && i.length != 0) {
                    this.v = i;
                }
                this.o = 4;
                m(wi0.a);
            } catch (Exception e2) {
                v(e2, true);
            }
        }
    }

    public final void v(Exception exc, boolean z) {
        if (exc instanceof NotProvisionedException) {
            this.c.c(this);
        } else {
            t(exc, z ? 1 : 2);
        }
    }

    public final void w() {
        if (this.e == 0 && this.o == 4) {
            androidx.media3.common.util.b.j(this.u);
            n(false);
        }
    }

    public void x(int i) {
        if (i != 2) {
            return;
        }
        w();
    }

    public void y() {
        if (B()) {
            n(true);
        }
    }

    public void z(Exception exc, boolean z) {
        t(exc, z ? 1 : 3);
    }
}
