package androidx.media3.exoplayer.drm;

import android.media.MediaDrmException;
import androidx.media3.common.DrmInitData;
import androidx.media3.exoplayer.drm.g;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/* compiled from: DummyExoMediaDrm.java */
/* loaded from: classes.dex */
public final class e implements g {
    @Override // androidx.media3.exoplayer.drm.g
    public void a() {
    }

    @Override // androidx.media3.exoplayer.drm.g
    public Map<String, String> b(byte[] bArr) {
        throw new IllegalStateException();
    }

    @Override // androidx.media3.exoplayer.drm.g
    public g.d c() {
        throw new IllegalStateException();
    }

    @Override // androidx.media3.exoplayer.drm.g
    public qa0 d(byte[] bArr) {
        throw new IllegalStateException();
    }

    @Override // androidx.media3.exoplayer.drm.g
    public byte[] e() throws MediaDrmException {
        throw new MediaDrmException("Attempting to open a session using a dummy ExoMediaDrm.");
    }

    @Override // androidx.media3.exoplayer.drm.g
    public boolean f(byte[] bArr, String str) {
        throw new IllegalStateException();
    }

    @Override // androidx.media3.exoplayer.drm.g
    public void g(byte[] bArr, byte[] bArr2) {
        throw new IllegalStateException();
    }

    @Override // androidx.media3.exoplayer.drm.g
    public void h(byte[] bArr) {
    }

    @Override // androidx.media3.exoplayer.drm.g
    public byte[] i(byte[] bArr, byte[] bArr2) {
        throw new IllegalStateException();
    }

    @Override // androidx.media3.exoplayer.drm.g
    public void j(g.b bVar) {
    }

    @Override // androidx.media3.exoplayer.drm.g
    public void k(byte[] bArr) {
        throw new IllegalStateException();
    }

    @Override // androidx.media3.exoplayer.drm.g
    public g.a l(byte[] bArr, List<DrmInitData.SchemeData> list, int i, HashMap<String, String> hashMap) {
        throw new IllegalStateException();
    }

    @Override // androidx.media3.exoplayer.drm.g
    public int m() {
        return 1;
    }

    @Override // androidx.media3.exoplayer.drm.g
    public /* synthetic */ void n(byte[] bArr, ks2 ks2Var) {
        dz0.a(this, bArr, ks2Var);
    }
}
