package androidx.media3.exoplayer.drm;

import android.os.Looper;
import androidx.media3.common.PlaybackException;
import androidx.media3.exoplayer.drm.DrmSession;
import androidx.media3.exoplayer.drm.b;

/* compiled from: DrmSessionManager.java */
/* loaded from: classes.dex */
public interface c {
    public static final c a = new a();

    /* compiled from: DrmSessionManager.java */
    /* loaded from: classes.dex */
    public class a implements c {
        @Override // androidx.media3.exoplayer.drm.c
        public /* synthetic */ void a() {
            wr0.c(this);
        }

        @Override // androidx.media3.exoplayer.drm.c
        public void b(Looper looper, ks2 ks2Var) {
        }

        @Override // androidx.media3.exoplayer.drm.c
        public DrmSession c(b.a aVar, androidx.media3.common.j jVar) {
            if (jVar.s0 == null) {
                return null;
            }
            return new f(new DrmSession.DrmSessionException(new UnsupportedDrmException(1), PlaybackException.ERROR_CODE_DRM_SCHEME_UNSUPPORTED));
        }

        @Override // androidx.media3.exoplayer.drm.c
        public /* synthetic */ void d() {
            wr0.b(this);
        }

        @Override // androidx.media3.exoplayer.drm.c
        public int e(androidx.media3.common.j jVar) {
            return jVar.s0 != null ? 1 : 0;
        }

        @Override // androidx.media3.exoplayer.drm.c
        public /* synthetic */ b f(b.a aVar, androidx.media3.common.j jVar) {
            return wr0.a(this, aVar, jVar);
        }
    }

    /* compiled from: DrmSessionManager.java */
    /* loaded from: classes.dex */
    public interface b {
        public static final b a = xr0.b;

        void a();
    }

    void a();

    void b(Looper looper, ks2 ks2Var);

    DrmSession c(b.a aVar, androidx.media3.common.j jVar);

    void d();

    int e(androidx.media3.common.j jVar);

    b f(b.a aVar, androidx.media3.common.j jVar);
}
