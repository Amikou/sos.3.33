package androidx.media3.exoplayer.drm;

import android.os.Handler;
import androidx.media3.exoplayer.drm.b;
import androidx.media3.exoplayer.source.j;
import java.util.Iterator;
import java.util.concurrent.CopyOnWriteArrayList;

/* compiled from: DrmSessionEventListener.java */
/* loaded from: classes.dex */
public interface b {

    /* compiled from: DrmSessionEventListener.java */
    /* loaded from: classes.dex */
    public static class a {
        public final int a;
        public final j.b b;
        public final CopyOnWriteArrayList<C0035a> c;

        /* compiled from: DrmSessionEventListener.java */
        /* renamed from: androidx.media3.exoplayer.drm.b$a$a  reason: collision with other inner class name */
        /* loaded from: classes.dex */
        public static final class C0035a {
            public Handler a;
            public b b;

            public C0035a(Handler handler, b bVar) {
                this.a = handler;
                this.b = bVar;
            }
        }

        public a() {
            this(new CopyOnWriteArrayList(), 0, null);
        }

        /* JADX INFO: Access modifiers changed from: private */
        public /* synthetic */ void n(b bVar) {
            bVar.U(this.a, this.b);
        }

        /* JADX INFO: Access modifiers changed from: private */
        public /* synthetic */ void o(b bVar) {
            bVar.n0(this.a, this.b);
        }

        /* JADX INFO: Access modifiers changed from: private */
        public /* synthetic */ void p(b bVar) {
            bVar.K(this.a, this.b);
        }

        /* JADX INFO: Access modifiers changed from: private */
        public /* synthetic */ void q(b bVar, int i) {
            bVar.j0(this.a, this.b);
            bVar.g0(this.a, this.b, i);
        }

        /* JADX INFO: Access modifiers changed from: private */
        public /* synthetic */ void r(b bVar, Exception exc) {
            bVar.o0(this.a, this.b, exc);
        }

        /* JADX INFO: Access modifiers changed from: private */
        public /* synthetic */ void s(b bVar) {
            bVar.d0(this.a, this.b);
        }

        public void g(Handler handler, b bVar) {
            ii.e(handler);
            ii.e(bVar);
            this.c.add(new C0035a(handler, bVar));
        }

        public void h() {
            Iterator<C0035a> it = this.c.iterator();
            while (it.hasNext()) {
                C0035a next = it.next();
                final b bVar = next.b;
                androidx.media3.common.util.b.G0(next.a, new Runnable() { // from class: tr0
                    @Override // java.lang.Runnable
                    public final void run() {
                        b.a.this.n(bVar);
                    }
                });
            }
        }

        public void i() {
            Iterator<C0035a> it = this.c.iterator();
            while (it.hasNext()) {
                C0035a next = it.next();
                final b bVar = next.b;
                androidx.media3.common.util.b.G0(next.a, new Runnable() { // from class: qr0
                    @Override // java.lang.Runnable
                    public final void run() {
                        b.a.this.o(bVar);
                    }
                });
            }
        }

        public void j() {
            Iterator<C0035a> it = this.c.iterator();
            while (it.hasNext()) {
                C0035a next = it.next();
                final b bVar = next.b;
                androidx.media3.common.util.b.G0(next.a, new Runnable() { // from class: rr0
                    @Override // java.lang.Runnable
                    public final void run() {
                        b.a.this.p(bVar);
                    }
                });
            }
        }

        public void k(final int i) {
            Iterator<C0035a> it = this.c.iterator();
            while (it.hasNext()) {
                C0035a next = it.next();
                final b bVar = next.b;
                androidx.media3.common.util.b.G0(next.a, new Runnable() { // from class: ur0
                    @Override // java.lang.Runnable
                    public final void run() {
                        b.a.this.q(bVar, i);
                    }
                });
            }
        }

        public void l(final Exception exc) {
            Iterator<C0035a> it = this.c.iterator();
            while (it.hasNext()) {
                C0035a next = it.next();
                final b bVar = next.b;
                androidx.media3.common.util.b.G0(next.a, new Runnable() { // from class: vr0
                    @Override // java.lang.Runnable
                    public final void run() {
                        b.a.this.r(bVar, exc);
                    }
                });
            }
        }

        public void m() {
            Iterator<C0035a> it = this.c.iterator();
            while (it.hasNext()) {
                C0035a next = it.next();
                final b bVar = next.b;
                androidx.media3.common.util.b.G0(next.a, new Runnable() { // from class: sr0
                    @Override // java.lang.Runnable
                    public final void run() {
                        b.a.this.s(bVar);
                    }
                });
            }
        }

        public void t(b bVar) {
            Iterator<C0035a> it = this.c.iterator();
            while (it.hasNext()) {
                C0035a next = it.next();
                if (next.b == bVar) {
                    this.c.remove(next);
                }
            }
        }

        public a u(int i, j.b bVar) {
            return new a(this.c, i, bVar);
        }

        public a(CopyOnWriteArrayList<C0035a> copyOnWriteArrayList, int i, j.b bVar) {
            this.c = copyOnWriteArrayList;
            this.a = i;
            this.b = bVar;
        }
    }

    void K(int i, j.b bVar);

    void U(int i, j.b bVar);

    void d0(int i, j.b bVar);

    void g0(int i, j.b bVar, int i2);

    @Deprecated
    void j0(int i, j.b bVar);

    void n0(int i, j.b bVar);

    void o0(int i, j.b bVar, Exception exc);
}
