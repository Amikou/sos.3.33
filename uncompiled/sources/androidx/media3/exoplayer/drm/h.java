package androidx.media3.exoplayer.drm;

import android.annotation.SuppressLint;
import android.media.DeniedByServerException;
import android.media.MediaCrypto;
import android.media.MediaCryptoException;
import android.media.MediaDrm;
import android.media.MediaDrmException;
import android.media.NotProvisionedException;
import android.media.UnsupportedSchemeException;
import android.media.metrics.LogSessionId;
import android.text.TextUtils;
import androidx.media3.common.DrmInitData;
import androidx.media3.exoplayer.drm.g;
import androidx.media3.exoplayer.drm.h;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.nio.charset.Charset;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;

/* compiled from: FrameworkMediaDrm.java */
/* loaded from: classes.dex */
public final class h implements g {
    public static final g.c d = ec1.a;
    public final UUID a;
    public final MediaDrm b;
    public int c;

    /* compiled from: FrameworkMediaDrm.java */
    /* loaded from: classes.dex */
    public static class a {
        public static boolean a(MediaDrm mediaDrm, String str) {
            return mediaDrm.requiresSecureDecoder(str);
        }

        public static void b(MediaDrm mediaDrm, byte[] bArr, ks2 ks2Var) {
            LogSessionId a = ks2Var.a();
            if (a.equals(LogSessionId.LOG_SESSION_ID_NONE)) {
                return;
            }
            ((MediaDrm.PlaybackComponent) ii.e(mediaDrm.getPlaybackComponent(bArr))).setLogSessionId(a);
        }
    }

    public h(UUID uuid) throws UnsupportedSchemeException {
        ii.e(uuid);
        ii.b(!ft.b.equals(uuid), "Use C.CLEARKEY_UUID instead");
        this.a = uuid;
        MediaDrm mediaDrm = new MediaDrm(u(uuid));
        this.b = mediaDrm;
        this.c = 1;
        if (ft.d.equals(uuid) && B()) {
            w(mediaDrm);
        }
    }

    public static /* synthetic */ g A(UUID uuid) {
        try {
            return C(uuid);
        } catch (UnsupportedDrmException unused) {
            p12.c("FrameworkMediaDrm", "Failed to instantiate a FrameworkMediaDrm for uuid: " + uuid + ".");
            return new e();
        }
    }

    public static boolean B() {
        return "ASUS_Z00AD".equals(androidx.media3.common.util.b.d);
    }

    public static h C(UUID uuid) throws UnsupportedDrmException {
        try {
            return new h(uuid);
        } catch (UnsupportedSchemeException e) {
            throw new UnsupportedDrmException(1, e);
        } catch (Exception e2) {
            throw new UnsupportedDrmException(2, e2);
        }
    }

    public static byte[] q(byte[] bArr) {
        int indexOf;
        op2 op2Var = new op2(bArr);
        int q = op2Var.q();
        short s = op2Var.s();
        short s2 = op2Var.s();
        if (s == 1 && s2 == 1) {
            short s3 = op2Var.s();
            Charset charset = cy.d;
            String B = op2Var.B(s3, charset);
            if (B.contains("<LA_URL>")) {
                return bArr;
            }
            if (B.indexOf("</DATA>") == -1) {
                p12.i("FrameworkMediaDrm", "Could not find the </DATA> tag. Skipping LA_URL workaround.");
            }
            String str = B.substring(0, indexOf) + "<LA_URL>https://x</LA_URL>" + B.substring(indexOf);
            int i = q + 52;
            ByteBuffer allocate = ByteBuffer.allocate(i);
            allocate.order(ByteOrder.LITTLE_ENDIAN);
            allocate.putInt(i);
            allocate.putShort(s);
            allocate.putShort(s2);
            allocate.putShort((short) (str.length() * 2));
            allocate.put(str.getBytes(charset));
            return allocate.array();
        }
        p12.f("FrameworkMediaDrm", "Unexpected record count or type. Skipping LA_URL workaround.");
        return bArr;
    }

    public static byte[] r(UUID uuid, byte[] bArr) {
        return ft.c.equals(uuid) ? gz.a(bArr) : bArr;
    }

    /* JADX WARN: Code restructure failed: missing block: B:24:0x0056, code lost:
        if ("AFTT".equals(r0) == false) goto L15;
     */
    /*
        Code decompiled incorrectly, please refer to instructions dump.
        To view partially-correct code enable 'Show inconsistent code' option in preferences
    */
    public static byte[] s(java.util.UUID r3, byte[] r4) {
        /*
            java.util.UUID r0 = defpackage.ft.e
            boolean r1 = r0.equals(r3)
            if (r1 == 0) goto L18
            byte[] r1 = defpackage.mw2.e(r4, r3)
            if (r1 != 0) goto Lf
            goto L10
        Lf:
            r4 = r1
        L10:
            byte[] r4 = q(r4)
            byte[] r4 = defpackage.mw2.a(r0, r4)
        L18:
            int r1 = androidx.media3.common.util.b.a
            r2 = 23
            if (r1 >= r2) goto L26
            java.util.UUID r1 = defpackage.ft.d
            boolean r1 = r1.equals(r3)
            if (r1 != 0) goto L58
        L26:
            boolean r0 = r0.equals(r3)
            if (r0 == 0) goto L5f
            java.lang.String r0 = androidx.media3.common.util.b.c
            java.lang.String r1 = "Amazon"
            boolean r0 = r1.equals(r0)
            if (r0 == 0) goto L5f
            java.lang.String r0 = androidx.media3.common.util.b.d
            java.lang.String r1 = "AFTB"
            boolean r1 = r1.equals(r0)
            if (r1 != 0) goto L58
            java.lang.String r1 = "AFTS"
            boolean r1 = r1.equals(r0)
            if (r1 != 0) goto L58
            java.lang.String r1 = "AFTM"
            boolean r1 = r1.equals(r0)
            if (r1 != 0) goto L58
            java.lang.String r1 = "AFTT"
            boolean r0 = r1.equals(r0)
            if (r0 == 0) goto L5f
        L58:
            byte[] r3 = defpackage.mw2.e(r4, r3)
            if (r3 == 0) goto L5f
            return r3
        L5f:
            return r4
        */
        throw new UnsupportedOperationException("Method not decompiled: androidx.media3.exoplayer.drm.h.s(java.util.UUID, byte[]):byte[]");
    }

    public static String t(UUID uuid, String str) {
        return (androidx.media3.common.util.b.a < 26 && ft.c.equals(uuid) && ("video/mp4".equals(str) || "audio/mp4".equals(str))) ? "cenc" : str;
    }

    public static UUID u(UUID uuid) {
        return (androidx.media3.common.util.b.a >= 27 || !ft.c.equals(uuid)) ? uuid : ft.b;
    }

    public static void w(MediaDrm mediaDrm) {
        mediaDrm.setPropertyString("securityLevel", "L3");
    }

    public static DrmInitData.SchemeData y(UUID uuid, List<DrmInitData.SchemeData> list) {
        boolean z;
        if (!ft.d.equals(uuid)) {
            return list.get(0);
        }
        if (androidx.media3.common.util.b.a >= 28 && list.size() > 1) {
            DrmInitData.SchemeData schemeData = list.get(0);
            int i = 0;
            for (int i2 = 0; i2 < list.size(); i2++) {
                DrmInitData.SchemeData schemeData2 = list.get(i2);
                byte[] bArr = (byte[]) ii.e(schemeData2.i0);
                if (!androidx.media3.common.util.b.c(schemeData2.h0, schemeData.h0) || !androidx.media3.common.util.b.c(schemeData2.g0, schemeData.g0) || !mw2.c(bArr)) {
                    z = false;
                    break;
                }
                i += bArr.length;
            }
            z = true;
            if (z) {
                byte[] bArr2 = new byte[i];
                int i3 = 0;
                for (int i4 = 0; i4 < list.size(); i4++) {
                    byte[] bArr3 = (byte[]) ii.e(list.get(i4).i0);
                    int length = bArr3.length;
                    System.arraycopy(bArr3, 0, bArr2, i3, length);
                    i3 += length;
                }
                return schemeData.b(bArr2);
            }
        }
        for (int i5 = 0; i5 < list.size(); i5++) {
            DrmInitData.SchemeData schemeData3 = list.get(i5);
            int g = mw2.g((byte[]) ii.e(schemeData3.i0));
            int i6 = androidx.media3.common.util.b.a;
            if (i6 < 23 && g == 0) {
                return schemeData3;
            }
            if (i6 >= 23 && g == 1) {
                return schemeData3;
            }
        }
        return list.get(0);
    }

    /* JADX INFO: Access modifiers changed from: private */
    public /* synthetic */ void z(g.b bVar, MediaDrm mediaDrm, byte[] bArr, int i, int i2, byte[] bArr2) {
        bVar.a(this, bArr, i, i2, bArr2);
    }

    @Override // androidx.media3.exoplayer.drm.g
    public synchronized void a() {
        int i = this.c - 1;
        this.c = i;
        if (i == 0) {
            this.b.release();
        }
    }

    @Override // androidx.media3.exoplayer.drm.g
    public Map<String, String> b(byte[] bArr) {
        return this.b.queryKeyStatus(bArr);
    }

    @Override // androidx.media3.exoplayer.drm.g
    public g.d c() {
        MediaDrm.ProvisionRequest provisionRequest = this.b.getProvisionRequest();
        return new g.d(provisionRequest.getData(), provisionRequest.getDefaultUrl());
    }

    @Override // androidx.media3.exoplayer.drm.g
    public byte[] e() throws MediaDrmException {
        return this.b.openSession();
    }

    @Override // androidx.media3.exoplayer.drm.g
    public boolean f(byte[] bArr, String str) {
        if (androidx.media3.common.util.b.a >= 31) {
            return a.a(this.b, str);
        }
        try {
            MediaCrypto mediaCrypto = new MediaCrypto(this.a, bArr);
            try {
                return mediaCrypto.requiresSecureDecoderComponent(str);
            } finally {
                mediaCrypto.release();
            }
        } catch (MediaCryptoException unused) {
            return true;
        }
    }

    @Override // androidx.media3.exoplayer.drm.g
    public void g(byte[] bArr, byte[] bArr2) {
        this.b.restoreKeys(bArr, bArr2);
    }

    @Override // androidx.media3.exoplayer.drm.g
    public void h(byte[] bArr) {
        this.b.closeSession(bArr);
    }

    @Override // androidx.media3.exoplayer.drm.g
    public byte[] i(byte[] bArr, byte[] bArr2) throws NotProvisionedException, DeniedByServerException {
        if (ft.c.equals(this.a)) {
            bArr2 = gz.b(bArr2);
        }
        return this.b.provideKeyResponse(bArr, bArr2);
    }

    @Override // androidx.media3.exoplayer.drm.g
    public void j(final g.b bVar) {
        this.b.setOnEventListener(bVar == null ? null : new MediaDrm.OnEventListener() { // from class: dc1
            @Override // android.media.MediaDrm.OnEventListener
            public final void onEvent(MediaDrm mediaDrm, byte[] bArr, int i, int i2, byte[] bArr2) {
                h.this.z(bVar, mediaDrm, bArr, i, i2, bArr2);
            }
        });
    }

    @Override // androidx.media3.exoplayer.drm.g
    public void k(byte[] bArr) throws DeniedByServerException {
        this.b.provideProvisionResponse(bArr);
    }

    @Override // androidx.media3.exoplayer.drm.g
    @SuppressLint({"WrongConstant"})
    public g.a l(byte[] bArr, List<DrmInitData.SchemeData> list, int i, HashMap<String, String> hashMap) throws NotProvisionedException {
        byte[] bArr2;
        String str;
        DrmInitData.SchemeData schemeData = null;
        if (list != null) {
            schemeData = y(this.a, list);
            bArr2 = s(this.a, (byte[]) ii.e(schemeData.i0));
            str = t(this.a, schemeData.h0);
        } else {
            bArr2 = null;
            str = null;
        }
        MediaDrm.KeyRequest keyRequest = this.b.getKeyRequest(bArr, bArr2, str, i, hashMap);
        byte[] r = r(this.a, keyRequest.getData());
        String defaultUrl = keyRequest.getDefaultUrl();
        if ("https://x".equals(defaultUrl)) {
            defaultUrl = "";
        }
        if (TextUtils.isEmpty(defaultUrl) && schemeData != null && !TextUtils.isEmpty(schemeData.g0)) {
            defaultUrl = schemeData.g0;
        }
        return new g.a(r, defaultUrl, androidx.media3.common.util.b.a >= 23 ? keyRequest.getRequestType() : Integer.MIN_VALUE);
    }

    @Override // androidx.media3.exoplayer.drm.g
    public int m() {
        return 2;
    }

    @Override // androidx.media3.exoplayer.drm.g
    public void n(byte[] bArr, ks2 ks2Var) {
        if (androidx.media3.common.util.b.a >= 31) {
            try {
                a.b(this.b, bArr, ks2Var);
            } catch (UnsupportedOperationException unused) {
                p12.i("FrameworkMediaDrm", "setLogSessionId failed.");
            }
        }
    }

    @Override // androidx.media3.exoplayer.drm.g
    /* renamed from: v */
    public cc1 d(byte[] bArr) throws MediaCryptoException {
        return new cc1(u(this.a), bArr, androidx.media3.common.util.b.a < 21 && ft.d.equals(this.a) && "L3".equals(x("securityLevel")));
    }

    public String x(String str) {
        return this.b.getPropertyString(str);
    }
}
