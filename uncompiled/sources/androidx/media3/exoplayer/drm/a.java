package androidx.media3.exoplayer.drm;

import android.net.Uri;
import androidx.media3.common.m;
import androidx.media3.datasource.b;
import androidx.media3.datasource.e;
import androidx.media3.exoplayer.drm.DefaultDrmSessionManager;
import com.google.common.primitives.Ints;
import java.util.Map;

/* compiled from: DefaultDrmSessionManagerProvider.java */
/* loaded from: classes.dex */
public final class a implements zr0 {
    public final Object a = new Object();
    public m.f b;
    public c c;
    public b.a d;
    public String e;

    @Override // defpackage.zr0
    public c a(m mVar) {
        c cVar;
        ii.e(mVar.f0);
        m.f fVar = mVar.f0.c;
        if (fVar != null && androidx.media3.common.util.b.a >= 18) {
            synchronized (this.a) {
                if (!androidx.media3.common.util.b.c(fVar, this.b)) {
                    this.b = fVar;
                    this.c = b(fVar);
                }
                cVar = (c) ii.e(this.c);
            }
            return cVar;
        }
        return c.a;
    }

    public final c b(m.f fVar) {
        b.a aVar = this.d;
        if (aVar == null) {
            aVar = new e.b().c(this.e);
        }
        Uri uri = fVar.b;
        i iVar = new i(uri == null ? null : uri.toString(), fVar.f, aVar);
        af4<Map.Entry<String, String>> it = fVar.c.entrySet().iterator();
        while (it.hasNext()) {
            Map.Entry<String, String> next = it.next();
            iVar.e(next.getKey(), next.getValue());
        }
        DefaultDrmSessionManager a = new DefaultDrmSessionManager.b().e(fVar.a, h.d).b(fVar.d).c(fVar.e).d(Ints.k(fVar.g)).a(iVar);
        a.G(0, fVar.c());
        return a;
    }
}
