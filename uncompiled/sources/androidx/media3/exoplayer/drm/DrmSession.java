package androidx.media3.exoplayer.drm;

import androidx.media3.exoplayer.drm.b;
import java.io.IOException;
import java.util.Map;
import java.util.UUID;

/* loaded from: classes.dex */
public interface DrmSession {

    /* loaded from: classes.dex */
    public static class DrmSessionException extends IOException {
        public final int errorCode;

        public DrmSessionException(Throwable th, int i) {
            super(th);
            this.errorCode = i;
        }
    }

    void a(b.a aVar);

    UUID b();

    boolean c();

    Map<String, String> d();

    void e(b.a aVar);

    boolean f(String str);

    qa0 g();

    DrmSessionException getError();

    int getState();
}
