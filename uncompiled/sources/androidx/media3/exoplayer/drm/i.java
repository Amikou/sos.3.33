package androidx.media3.exoplayer.drm;

import android.net.Uri;
import android.text.TextUtils;
import androidx.media3.datasource.HttpDataSource$InvalidResponseCodeException;
import androidx.media3.datasource.b;
import androidx.media3.exoplayer.drm.g;
import com.google.common.collect.ImmutableMap;
import defpackage.je0;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;
import zendesk.core.Constants;

/* compiled from: HttpMediaDrmCallback.java */
/* loaded from: classes.dex */
public final class i implements j {
    public final b.a a;
    public final String b;
    public final boolean c;
    public final Map<String, String> d;

    public i(String str, boolean z, b.a aVar) {
        ii.a((z && TextUtils.isEmpty(str)) ? false : true);
        this.a = aVar;
        this.b = str;
        this.c = z;
        this.d = new HashMap();
    }

    public static byte[] c(b.a aVar, String str, byte[] bArr, Map<String, String> map) throws MediaDrmCallbackException {
        androidx.media3.datasource.f fVar = new androidx.media3.datasource.f(aVar.a());
        je0 a = new je0.b().j(str).e(map).d(2).c(bArr).b(1).a();
        int i = 0;
        je0 je0Var = a;
        while (true) {
            try {
                androidx.media3.datasource.c cVar = new androidx.media3.datasource.c(fVar, je0Var);
                try {
                    byte[] Q0 = androidx.media3.common.util.b.Q0(cVar);
                    androidx.media3.common.util.b.n(cVar);
                    return Q0;
                } catch (HttpDataSource$InvalidResponseCodeException e) {
                    String d = d(e, i);
                    if (d != null) {
                        i++;
                        je0Var = je0Var.a().j(d).a();
                        androidx.media3.common.util.b.n(cVar);
                    } else {
                        throw e;
                    }
                }
            } catch (Exception e2) {
                throw new MediaDrmCallbackException(a, (Uri) ii.e(fVar.p()), fVar.i(), fVar.o(), e2);
            }
        }
    }

    public static String d(HttpDataSource$InvalidResponseCodeException httpDataSource$InvalidResponseCodeException, int i) {
        Map<String, List<String>> map;
        List<String> list;
        int i2 = httpDataSource$InvalidResponseCodeException.responseCode;
        if (!((i2 == 307 || i2 == 308) && i < 5) || (map = httpDataSource$InvalidResponseCodeException.headerFields) == null || (list = map.get("Location")) == null || list.isEmpty()) {
            return null;
        }
        return list.get(0);
    }

    @Override // androidx.media3.exoplayer.drm.j
    public byte[] a(UUID uuid, g.d dVar) throws MediaDrmCallbackException {
        return c(this.a, dVar.b() + "&signedRequest=" + androidx.media3.common.util.b.B(dVar.a()), null, Collections.emptyMap());
    }

    @Override // androidx.media3.exoplayer.drm.j
    public byte[] b(UUID uuid, g.a aVar) throws MediaDrmCallbackException {
        String str;
        String b = aVar.b();
        if (this.c || TextUtils.isEmpty(b)) {
            b = this.b;
        }
        if (!TextUtils.isEmpty(b)) {
            HashMap hashMap = new HashMap();
            UUID uuid2 = ft.e;
            if (uuid2.equals(uuid)) {
                str = "text/xml";
            } else {
                str = ft.c.equals(uuid) ? Constants.APPLICATION_JSON : "application/octet-stream";
            }
            hashMap.put("Content-Type", str);
            if (uuid2.equals(uuid)) {
                hashMap.put("SOAPAction", "http://schemas.microsoft.com/DRM/2007/03/protocols/AcquireLicense");
            }
            synchronized (this.d) {
                hashMap.putAll(this.d);
            }
            return c(this.a, b, aVar.a(), hashMap);
        }
        throw new MediaDrmCallbackException(new je0.b().i(Uri.EMPTY).a(), Uri.EMPTY, ImmutableMap.of(), 0L, new IllegalStateException("No license URL"));
    }

    public void e(String str, String str2) {
        ii.e(str);
        ii.e(str2);
        synchronized (this.d) {
            this.d.put(str, str2);
        }
    }
}
