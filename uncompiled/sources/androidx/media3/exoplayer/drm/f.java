package androidx.media3.exoplayer.drm;

import androidx.media3.exoplayer.drm.DrmSession;
import androidx.media3.exoplayer.drm.b;
import java.util.Map;
import java.util.UUID;

/* compiled from: ErrorStateDrmSession.java */
/* loaded from: classes.dex */
public final class f implements DrmSession {
    public final DrmSession.DrmSessionException a;

    public f(DrmSession.DrmSessionException drmSessionException) {
        this.a = (DrmSession.DrmSessionException) ii.e(drmSessionException);
    }

    @Override // androidx.media3.exoplayer.drm.DrmSession
    public void a(b.a aVar) {
    }

    @Override // androidx.media3.exoplayer.drm.DrmSession
    public final UUID b() {
        return ft.a;
    }

    @Override // androidx.media3.exoplayer.drm.DrmSession
    public boolean c() {
        return false;
    }

    @Override // androidx.media3.exoplayer.drm.DrmSession
    public Map<String, String> d() {
        return null;
    }

    @Override // androidx.media3.exoplayer.drm.DrmSession
    public void e(b.a aVar) {
    }

    @Override // androidx.media3.exoplayer.drm.DrmSession
    public boolean f(String str) {
        return false;
    }

    @Override // androidx.media3.exoplayer.drm.DrmSession
    public qa0 g() {
        return null;
    }

    @Override // androidx.media3.exoplayer.drm.DrmSession
    public DrmSession.DrmSessionException getError() {
        return this.a;
    }

    @Override // androidx.media3.exoplayer.drm.DrmSession
    public int getState() {
        return 1;
    }
}
