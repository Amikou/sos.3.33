package androidx.media3.exoplayer;

import android.content.Context;
import android.os.Looper;
import androidx.media3.common.q;
import androidx.media3.exoplayer.d;
import androidx.media3.exoplayer.f;
import androidx.media3.exoplayer.source.j;

/* compiled from: ExoPlayer.java */
/* loaded from: classes.dex */
public interface f extends q {

    /* compiled from: ExoPlayer.java */
    /* loaded from: classes.dex */
    public interface a {
        void y(boolean z);
    }

    /* compiled from: ExoPlayer.java */
    /* loaded from: classes.dex */
    public static final class b {
        public boolean A;
        public boolean B;
        public final Context a;
        public tz b;
        public long c;
        public dw3<w63> d;
        public dw3<j.a> e;
        public dw3<androidx.media3.exoplayer.trackselection.f> f;
        public dw3<s02> g;
        public dw3<gm> h;
        public jd1<tz, jb> i;
        public Looper j;
        public xu2 k;
        public androidx.media3.common.b l;
        public boolean m;
        public int n;
        public boolean o;
        public boolean p;
        public int q;
        public int r;
        public boolean s;
        public xi3 t;
        public long u;
        public long v;
        public r02 w;
        public long x;
        public long y;
        public boolean z;

        public b(final Context context) {
            this(context, new dw3() { // from class: iz0
                @Override // defpackage.dw3
                public final Object get() {
                    w63 f;
                    f = f.b.f(context);
                    return f;
                }
            }, new dw3() { // from class: jz0
                @Override // defpackage.dw3
                public final Object get() {
                    j.a g;
                    g = f.b.g(context);
                    return g;
                }
            });
        }

        public static /* synthetic */ w63 f(Context context) {
            return new kk0(context);
        }

        public static /* synthetic */ j.a g(Context context) {
            return new androidx.media3.exoplayer.source.e(context, new kj0());
        }

        public static /* synthetic */ androidx.media3.exoplayer.trackselection.f h(Context context) {
            return new androidx.media3.exoplayer.trackselection.b(context);
        }

        public f e() {
            ii.g(!this.B);
            this.B = true;
            return new g(this, null);
        }

        public b(final Context context, dw3<w63> dw3Var, dw3<j.a> dw3Var2) {
            this(context, dw3Var, dw3Var2, new dw3() { // from class: hz0
                @Override // defpackage.dw3
                public final Object get() {
                    androidx.media3.exoplayer.trackselection.f h;
                    h = f.b.h(context);
                    return h;
                }
            }, kz0.a, new dw3() { // from class: gz0
                @Override // defpackage.dw3
                public final Object get() {
                    gm n;
                    n = bi0.n(context);
                    return n;
                }
            }, fz0.a);
        }

        public b(Context context, dw3<w63> dw3Var, dw3<j.a> dw3Var2, dw3<androidx.media3.exoplayer.trackselection.f> dw3Var3, dw3<s02> dw3Var4, dw3<gm> dw3Var5, jd1<tz, jb> jd1Var) {
            this.a = context;
            this.d = dw3Var;
            this.e = dw3Var2;
            this.f = dw3Var3;
            this.g = dw3Var4;
            this.h = dw3Var5;
            this.i = jd1Var;
            this.j = androidx.media3.common.util.b.M();
            this.l = androidx.media3.common.b.k0;
            this.n = 0;
            this.q = 1;
            this.r = 0;
            this.s = true;
            this.t = xi3.d;
            this.u = 5000L;
            this.v = u84.DEFAULT_POLLING_FREQUENCY;
            this.w = new d.b().a();
            this.b = tz.a;
            this.x = 500L;
            this.y = 2000L;
            this.A = true;
        }
    }
}
