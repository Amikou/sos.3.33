package androidx.media3.exoplayer.scheduler;

import android.app.job.JobParameters;
import android.app.job.JobService;
import android.content.Intent;
import android.os.PersistableBundle;
import androidx.media3.common.util.b;

/* loaded from: classes.dex */
public final class PlatformScheduler$PlatformSchedulerService extends JobService {
    @Override // android.app.job.JobService
    public boolean onStartJob(JobParameters jobParameters) {
        PersistableBundle extras = jobParameters.getExtras();
        int b = new Requirements(extras.getInt("requirements")).b(this);
        if (b == 0) {
            b.O0(this, new Intent((String) ii.e(extras.getString("service_action"))).setPackage((String) ii.e(extras.getString("service_package"))));
            return false;
        }
        p12.i("PlatformScheduler", "Requirements not met: " + b);
        jobFinished(jobParameters, true);
        return false;
    }

    @Override // android.app.job.JobService
    public boolean onStopJob(JobParameters jobParameters) {
        return false;
    }
}
