package androidx.media3.exoplayer.scheduler;

import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.net.ConnectivityManager;
import android.net.Network;
import android.net.NetworkCapabilities;
import android.net.NetworkInfo;
import android.os.Parcel;
import android.os.Parcelable;
import android.os.PowerManager;
import androidx.media3.common.util.b;

/* loaded from: classes.dex */
public final class Requirements implements Parcelable {
    public static final Parcelable.Creator<Requirements> CREATOR = new a();
    public final int a;

    /* loaded from: classes.dex */
    public class a implements Parcelable.Creator<Requirements> {
        @Override // android.os.Parcelable.Creator
        /* renamed from: a */
        public Requirements createFromParcel(Parcel parcel) {
            return new Requirements(parcel.readInt());
        }

        @Override // android.os.Parcelable.Creator
        /* renamed from: b */
        public Requirements[] newArray(int i) {
            return new Requirements[i];
        }
    }

    public Requirements(int i) {
        this.a = (i & 2) != 0 ? i | 1 : i;
    }

    public static boolean g(ConnectivityManager connectivityManager) {
        if (b.a < 24) {
            return true;
        }
        Network activeNetwork = connectivityManager.getActiveNetwork();
        if (activeNetwork == null) {
            return false;
        }
        try {
            NetworkCapabilities networkCapabilities = connectivityManager.getNetworkCapabilities(activeNetwork);
            if (networkCapabilities != null) {
                if (networkCapabilities.hasCapability(16)) {
                    return true;
                }
            }
            return false;
        } catch (SecurityException unused) {
            return true;
        }
    }

    public final int a(Context context) {
        if (h()) {
            ConnectivityManager connectivityManager = (ConnectivityManager) ii.e(context.getSystemService("connectivity"));
            NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
            if (activeNetworkInfo != null && activeNetworkInfo.isConnected() && g(connectivityManager)) {
                return (k() && connectivityManager.isActiveNetworkMetered()) ? 2 : 0;
            }
            return this.a & 3;
        }
        return 0;
    }

    public int b(Context context) {
        int a2 = a(context);
        if (c() && !d(context)) {
            a2 |= 8;
        }
        if (f() && !e(context)) {
            a2 |= 4;
        }
        return (!j() || i(context)) ? a2 : a2 | 16;
    }

    public boolean c() {
        return (this.a & 8) != 0;
    }

    public final boolean d(Context context) {
        Intent registerReceiver = context.registerReceiver(null, new IntentFilter("android.intent.action.BATTERY_CHANGED"));
        if (registerReceiver == null) {
            return false;
        }
        int intExtra = registerReceiver.getIntExtra("status", -1);
        return intExtra == 2 || intExtra == 5;
    }

    @Override // android.os.Parcelable
    public int describeContents() {
        return 0;
    }

    public final boolean e(Context context) {
        PowerManager powerManager = (PowerManager) ii.e(context.getSystemService("power"));
        int i = b.a;
        if (i >= 23) {
            return powerManager.isDeviceIdleMode();
        }
        if (i >= 20) {
            if (!powerManager.isInteractive()) {
                return true;
            }
        } else if (!powerManager.isScreenOn()) {
            return true;
        }
        return false;
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        return obj != null && Requirements.class == obj.getClass() && this.a == ((Requirements) obj).a;
    }

    public boolean f() {
        return (this.a & 4) != 0;
    }

    public boolean h() {
        return (this.a & 1) != 0;
    }

    public int hashCode() {
        return this.a;
    }

    public final boolean i(Context context) {
        return context.registerReceiver(null, new IntentFilter("android.intent.action.DEVICE_STORAGE_LOW")) == null;
    }

    public boolean j() {
        return (this.a & 16) != 0;
    }

    public boolean k() {
        return (this.a & 2) != 0;
    }

    @Override // android.os.Parcelable
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeInt(this.a);
    }
}
