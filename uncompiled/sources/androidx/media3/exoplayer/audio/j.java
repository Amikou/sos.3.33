package androidx.media3.exoplayer.audio;

import androidx.media3.exoplayer.audio.AudioProcessor;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.nio.ShortBuffer;
import okhttp3.internal.ws.RealWebSocket;

/* compiled from: SonicAudioProcessor.java */
/* loaded from: classes.dex */
public final class j implements AudioProcessor {
    public int b;
    public float c = 1.0f;
    public float d = 1.0f;
    public AudioProcessor.a e;
    public AudioProcessor.a f;
    public AudioProcessor.a g;
    public AudioProcessor.a h;
    public boolean i;
    public er3 j;
    public ByteBuffer k;
    public ShortBuffer l;
    public ByteBuffer m;
    public long n;
    public long o;
    public boolean p;

    public j() {
        AudioProcessor.a aVar = AudioProcessor.a.e;
        this.e = aVar;
        this.f = aVar;
        this.g = aVar;
        this.h = aVar;
        ByteBuffer byteBuffer = AudioProcessor.a;
        this.k = byteBuffer;
        this.l = byteBuffer.asShortBuffer();
        this.m = byteBuffer;
        this.b = -1;
    }

    @Override // androidx.media3.exoplayer.audio.AudioProcessor
    public AudioProcessor.a a(AudioProcessor.a aVar) throws AudioProcessor.UnhandledAudioFormatException {
        if (aVar.c == 2) {
            int i = this.b;
            if (i == -1) {
                i = aVar.a;
            }
            this.e = aVar;
            AudioProcessor.a aVar2 = new AudioProcessor.a(i, aVar.b, 2);
            this.f = aVar2;
            this.i = true;
            return aVar2;
        }
        throw new AudioProcessor.UnhandledAudioFormatException(aVar);
    }

    @Override // androidx.media3.exoplayer.audio.AudioProcessor
    public boolean b() {
        return this.f.a != -1 && (Math.abs(this.c - 1.0f) >= 1.0E-4f || Math.abs(this.d - 1.0f) >= 1.0E-4f || this.f.a != this.e.a);
    }

    @Override // androidx.media3.exoplayer.audio.AudioProcessor
    public void c(ByteBuffer byteBuffer) {
        if (byteBuffer.hasRemaining()) {
            ShortBuffer asShortBuffer = byteBuffer.asShortBuffer();
            int remaining = byteBuffer.remaining();
            this.n += remaining;
            ((er3) ii.e(this.j)).t(asShortBuffer);
            byteBuffer.position(byteBuffer.position() + remaining);
        }
    }

    @Override // androidx.media3.exoplayer.audio.AudioProcessor
    public boolean d() {
        er3 er3Var;
        return this.p && ((er3Var = this.j) == null || er3Var.k() == 0);
    }

    @Override // androidx.media3.exoplayer.audio.AudioProcessor
    public void e() {
        er3 er3Var = this.j;
        if (er3Var != null) {
            er3Var.s();
        }
        this.p = true;
    }

    public long f(long j) {
        if (this.o >= RealWebSocket.DEFAULT_MINIMUM_DEFLATE_SIZE) {
            long l = this.n - ((er3) ii.e(this.j)).l();
            int i = this.h.a;
            int i2 = this.g.a;
            if (i == i2) {
                return androidx.media3.common.util.b.J0(j, l, this.o);
            }
            return androidx.media3.common.util.b.J0(j, l * i, this.o * i2);
        }
        return (long) (this.c * j);
    }

    @Override // androidx.media3.exoplayer.audio.AudioProcessor
    public void flush() {
        if (b()) {
            AudioProcessor.a aVar = this.e;
            this.g = aVar;
            AudioProcessor.a aVar2 = this.f;
            this.h = aVar2;
            if (this.i) {
                this.j = new er3(aVar.a, aVar.b, this.c, this.d, aVar2.a);
            } else {
                er3 er3Var = this.j;
                if (er3Var != null) {
                    er3Var.i();
                }
            }
        }
        this.m = AudioProcessor.a;
        this.n = 0L;
        this.o = 0L;
        this.p = false;
    }

    public void g(float f) {
        if (this.d != f) {
            this.d = f;
            this.i = true;
        }
    }

    @Override // androidx.media3.exoplayer.audio.AudioProcessor
    public ByteBuffer getOutput() {
        int k;
        er3 er3Var = this.j;
        if (er3Var != null && (k = er3Var.k()) > 0) {
            if (this.k.capacity() < k) {
                ByteBuffer order = ByteBuffer.allocateDirect(k).order(ByteOrder.nativeOrder());
                this.k = order;
                this.l = order.asShortBuffer();
            } else {
                this.k.clear();
                this.l.clear();
            }
            er3Var.j(this.l);
            this.o += k;
            this.k.limit(k);
            this.m = this.k;
        }
        ByteBuffer byteBuffer = this.m;
        this.m = AudioProcessor.a;
        return byteBuffer;
    }

    public void h(float f) {
        if (this.c != f) {
            this.c = f;
            this.i = true;
        }
    }

    @Override // androidx.media3.exoplayer.audio.AudioProcessor
    public void reset() {
        this.c = 1.0f;
        this.d = 1.0f;
        AudioProcessor.a aVar = AudioProcessor.a.e;
        this.e = aVar;
        this.f = aVar;
        this.g = aVar;
        this.h = aVar;
        ByteBuffer byteBuffer = AudioProcessor.a;
        this.k = byteBuffer;
        this.l = byteBuffer.asShortBuffer();
        this.m = byteBuffer;
        this.b = -1;
        this.i = false;
        this.j = null;
        this.n = 0L;
        this.o = 0L;
        this.p = false;
    }
}
