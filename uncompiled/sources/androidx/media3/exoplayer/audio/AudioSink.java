package androidx.media3.exoplayer.audio;

import androidx.media3.common.p;
import java.nio.ByteBuffer;

/* loaded from: classes.dex */
public interface AudioSink {

    /* loaded from: classes.dex */
    public static final class InitializationException extends Exception {
        public final int audioTrackState;
        public final androidx.media3.common.j format;
        public final boolean isRecoverable;

        /* JADX WARN: Illegal instructions before constructor call */
        /*
            Code decompiled incorrectly, please refer to instructions dump.
            To view partially-correct code enable 'Show inconsistent code' option in preferences
        */
        public InitializationException(int r3, int r4, int r5, int r6, androidx.media3.common.j r7, boolean r8, java.lang.Exception r9) {
            /*
                r2 = this;
                java.lang.StringBuilder r0 = new java.lang.StringBuilder
                r0.<init>()
                java.lang.String r1 = "AudioTrack init failed "
                r0.append(r1)
                r0.append(r3)
                java.lang.String r1 = " "
                r0.append(r1)
                java.lang.String r1 = "Config("
                r0.append(r1)
                r0.append(r4)
                java.lang.String r4 = ", "
                r0.append(r4)
                r0.append(r5)
                r0.append(r4)
                r0.append(r6)
                java.lang.String r4 = ")"
                r0.append(r4)
                if (r8 == 0) goto L32
                java.lang.String r4 = " (recoverable)"
                goto L34
            L32:
                java.lang.String r4 = ""
            L34:
                r0.append(r4)
                java.lang.String r4 = r0.toString()
                r2.<init>(r4, r9)
                r2.audioTrackState = r3
                r2.isRecoverable = r8
                r2.format = r7
                return
            */
            throw new UnsupportedOperationException("Method not decompiled: androidx.media3.exoplayer.audio.AudioSink.InitializationException.<init>(int, int, int, int, androidx.media3.common.j, boolean, java.lang.Exception):void");
        }
    }

    /* loaded from: classes.dex */
    public static final class UnexpectedDiscontinuityException extends Exception {
        public final long actualPresentationTimeUs;
        public final long expectedPresentationTimeUs;

        public UnexpectedDiscontinuityException(long j, long j2) {
            super("Unexpected audio track timestamp discontinuity: expected " + j2 + ", got " + j);
            this.actualPresentationTimeUs = j;
            this.expectedPresentationTimeUs = j2;
        }
    }

    /* loaded from: classes.dex */
    public static final class WriteException extends Exception {
        public final int errorCode;
        public final androidx.media3.common.j format;
        public final boolean isRecoverable;

        public WriteException(int i, androidx.media3.common.j jVar, boolean z) {
            super("AudioTrack write failed: " + i);
            this.isRecoverable = z;
            this.errorCode = i;
            this.format = jVar;
        }
    }

    /* loaded from: classes.dex */
    public interface a {
        void a(long j);

        void b(boolean z);

        void c(Exception exc);

        void d();

        void e(int i, long j, long j2);

        void f();

        void g();
    }

    boolean a(androidx.media3.common.j jVar);

    void b(p pVar);

    void c();

    boolean d();

    p e();

    void f();

    void flush();

    void g(androidx.media3.common.j jVar, int i, int[] iArr) throws ConfigurationException;

    void h(androidx.media3.common.b bVar);

    void i() throws WriteException;

    boolean j();

    void k(int i);

    void l(a aVar);

    long m(boolean z);

    void n();

    void o();

    void p(float f);

    void q(ks2 ks2Var);

    void r();

    void reset();

    int s(androidx.media3.common.j jVar);

    void t(ql qlVar);

    boolean u(ByteBuffer byteBuffer, long j, int i) throws InitializationException, WriteException;

    void v();

    void w(boolean z);

    /* loaded from: classes.dex */
    public static final class ConfigurationException extends Exception {
        public final androidx.media3.common.j format;

        public ConfigurationException(Throwable th, androidx.media3.common.j jVar) {
            super(th);
            this.format = jVar;
        }

        public ConfigurationException(String str, androidx.media3.common.j jVar) {
            super(str);
            this.format = jVar;
        }
    }
}
