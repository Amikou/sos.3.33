package androidx.media3.exoplayer.audio;

import androidx.media3.exoplayer.audio.DefaultAudioSink;
import com.google.common.primitives.Ints;

/* compiled from: DefaultAudioTrackBufferSizeProvider.java */
/* loaded from: classes.dex */
public class e implements DefaultAudioSink.d {
    public final int b;
    public final int c;
    public final int d;
    public final int e;
    public final int f;
    public final int g;

    /* compiled from: DefaultAudioTrackBufferSizeProvider.java */
    /* loaded from: classes.dex */
    public static class a {
        public int a = 250000;
        public int b = 750000;
        public int c = 4;
        public int d = 250000;
        public int e = 50000000;
        public int f = 2;

        public e g() {
            return new e(this);
        }
    }

    public e(a aVar) {
        this.b = aVar.a;
        this.c = aVar.b;
        this.d = aVar.c;
        this.e = aVar.d;
        this.f = aVar.e;
        this.g = aVar.f;
    }

    public static int b(int i, int i2, int i3) {
        return Ints.d(((i * i2) * i3) / 1000000);
    }

    public static int d(int i) {
        switch (i) {
            case 5:
                return 80000;
            case 6:
            case 18:
                return 768000;
            case 7:
                return 192000;
            case 8:
                return 2250000;
            case 9:
                return 40000;
            case 10:
                return 100000;
            case 11:
                return 16000;
            case 12:
                return 7000;
            case 13:
            default:
                throw new IllegalArgumentException();
            case 14:
                return 3062500;
            case 15:
                return 8000;
            case 16:
                return 256000;
            case 17:
                return 336000;
        }
    }

    @Override // androidx.media3.exoplayer.audio.DefaultAudioSink.d
    public int a(int i, int i2, int i3, int i4, int i5, double d) {
        return (((Math.max(i, (int) (c(i, i2, i3, i4, i5) * d)) + i4) - 1) / i4) * i4;
    }

    public int c(int i, int i2, int i3, int i4, int i5) {
        if (i3 != 0) {
            if (i3 != 1) {
                if (i3 == 2) {
                    return f(i2);
                }
                throw new IllegalArgumentException();
            }
            return e(i2);
        }
        return g(i, i5, i4);
    }

    public int e(int i) {
        return Ints.d((this.f * d(i)) / 1000000);
    }

    public int f(int i) {
        int i2 = this.e;
        if (i == 5) {
            i2 *= this.g;
        }
        return Ints.d((i2 * d(i)) / 1000000);
    }

    public int g(int i, int i2, int i3) {
        return androidx.media3.common.util.b.q(i * this.d, b(this.b, i2, i3), b(this.c, i2, i3));
    }
}
