package androidx.media3.exoplayer.audio;

import android.annotation.SuppressLint;
import android.media.AudioAttributes;
import android.media.AudioFormat;
import android.media.AudioManager;
import android.media.AudioTrack;
import android.media.PlaybackParams;
import android.media.metrics.LogSessionId;
import android.os.Handler;
import android.os.SystemClock;
import android.util.Pair;
import androidx.media3.common.PlaybackException;
import androidx.media3.common.p;
import androidx.media3.exoplayer.audio.AudioProcessor;
import androidx.media3.exoplayer.audio.AudioSink;
import androidx.media3.exoplayer.audio.b;
import androidx.media3.exoplayer.audio.e;
import androidx.recyclerview.widget.RecyclerView;
import com.github.mikephil.charting.utils.Utils;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.util.ArrayDeque;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Objects;
import zendesk.support.request.CellBase;

/* loaded from: classes.dex */
public final class DefaultAudioSink implements AudioSink {
    public static boolean c0 = false;
    public int A;
    public long B;
    public long C;
    public long D;
    public long E;
    public int F;
    public boolean G;
    public boolean H;
    public long I;
    public float J;
    public AudioProcessor[] K;
    public ByteBuffer[] L;
    public ByteBuffer M;
    public int N;
    public ByteBuffer O;
    public byte[] P;
    public int Q;
    public int R;
    public boolean S;
    public boolean T;
    public boolean U;
    public boolean V;
    public int W;
    public ql X;
    public boolean Y;
    public long Z;
    public final fj a;
    public boolean a0;
    public final c b;
    public boolean b0;
    public final boolean c;
    public final androidx.media3.exoplayer.audio.d d;
    public final androidx.media3.exoplayer.audio.k e;
    public final AudioProcessor[] f;
    public final AudioProcessor[] g;
    public final androidx.media3.common.util.a h;
    public final androidx.media3.exoplayer.audio.b i;
    public final ArrayDeque<h> j;
    public final boolean k;
    public final int l;
    public k m;
    public final i<AudioSink.InitializationException> n;
    public final i<AudioSink.WriteException> o;
    public final d p;
    public ks2 q;
    public AudioSink.a r;
    public f s;
    public f t;
    public AudioTrack u;
    public androidx.media3.common.b v;
    public h w;
    public h x;
    public p y;
    public ByteBuffer z;

    /* loaded from: classes.dex */
    public static final class InvalidAudioTrackTimestampException extends RuntimeException {
        public /* synthetic */ InvalidAudioTrackTimestampException(String str, a aVar) {
            this(str);
        }

        public InvalidAudioTrackTimestampException(String str) {
            super(str);
        }
    }

    /* loaded from: classes.dex */
    public class a extends Thread {
        public final /* synthetic */ AudioTrack a;

        /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
        public a(String str, AudioTrack audioTrack) {
            super(str);
            this.a = audioTrack;
        }

        @Override // java.lang.Thread, java.lang.Runnable
        public void run() {
            try {
                this.a.flush();
                this.a.release();
            } finally {
                DefaultAudioSink.this.h.e();
            }
        }
    }

    /* loaded from: classes.dex */
    public static final class b {
        public static void a(AudioTrack audioTrack, ks2 ks2Var) {
            LogSessionId a = ks2Var.a();
            if (a.equals(LogSessionId.LOG_SESSION_ID_NONE)) {
                return;
            }
            audioTrack.setLogSessionId(a);
        }
    }

    /* loaded from: classes.dex */
    public interface c {
        long a(long j);

        long b();

        boolean c(boolean z);

        AudioProcessor[] d();

        p e(p pVar);
    }

    /* loaded from: classes.dex */
    public interface d {
        public static final d a = new e.a().g();

        int a(int i, int i2, int i3, int i4, int i5, double d);
    }

    /* loaded from: classes.dex */
    public static final class e {
        public c b;
        public boolean c;
        public boolean d;
        public fj a = fj.c;
        public int e = 0;
        public d f = d.a;

        public DefaultAudioSink f() {
            if (this.b == null) {
                this.b = new g(new AudioProcessor[0]);
            }
            return new DefaultAudioSink(this, null);
        }

        public e g(fj fjVar) {
            ii.e(fjVar);
            this.a = fjVar;
            return this;
        }

        public e h(boolean z) {
            this.d = z;
            return this;
        }

        public e i(boolean z) {
            this.c = z;
            return this;
        }

        public e j(int i) {
            this.e = i;
            return this;
        }
    }

    /* loaded from: classes.dex */
    public static final class f {
        public final androidx.media3.common.j a;
        public final int b;
        public final int c;
        public final int d;
        public final int e;
        public final int f;
        public final int g;
        public final int h;
        public final AudioProcessor[] i;

        public f(androidx.media3.common.j jVar, int i, int i2, int i3, int i4, int i5, int i6, int i7, AudioProcessor[] audioProcessorArr) {
            this.a = jVar;
            this.b = i;
            this.c = i2;
            this.d = i3;
            this.e = i4;
            this.f = i5;
            this.g = i6;
            this.h = i7;
            this.i = audioProcessorArr;
        }

        public static AudioAttributes i(androidx.media3.common.b bVar, boolean z) {
            if (z) {
                return j();
            }
            return bVar.a().a;
        }

        public static AudioAttributes j() {
            return new AudioAttributes.Builder().setContentType(3).setFlags(16).setUsage(1).build();
        }

        public AudioTrack a(boolean z, androidx.media3.common.b bVar, int i) throws AudioSink.InitializationException {
            try {
                AudioTrack d = d(z, bVar, i);
                int state = d.getState();
                if (state == 1) {
                    return d;
                }
                try {
                    d.release();
                } catch (Exception unused) {
                }
                throw new AudioSink.InitializationException(state, this.e, this.f, this.h, this.a, l(), null);
            } catch (IllegalArgumentException | UnsupportedOperationException e) {
                throw new AudioSink.InitializationException(0, this.e, this.f, this.h, this.a, l(), e);
            }
        }

        public boolean b(f fVar) {
            return fVar.c == this.c && fVar.g == this.g && fVar.e == this.e && fVar.f == this.f && fVar.d == this.d;
        }

        public f c(int i) {
            return new f(this.a, this.b, this.c, this.d, this.e, this.f, this.g, i, this.i);
        }

        public final AudioTrack d(boolean z, androidx.media3.common.b bVar, int i) {
            int i2 = androidx.media3.common.util.b.a;
            if (i2 >= 29) {
                return f(z, bVar, i);
            }
            if (i2 >= 21) {
                return e(z, bVar, i);
            }
            return g(bVar, i);
        }

        public final AudioTrack e(boolean z, androidx.media3.common.b bVar, int i) {
            return new AudioTrack(i(bVar, z), DefaultAudioSink.M(this.e, this.f, this.g), this.h, 1, i);
        }

        public final AudioTrack f(boolean z, androidx.media3.common.b bVar, int i) {
            return new AudioTrack.Builder().setAudioAttributes(i(bVar, z)).setAudioFormat(DefaultAudioSink.M(this.e, this.f, this.g)).setTransferMode(1).setBufferSizeInBytes(this.h).setSessionId(i).setOffloadedPlayback(this.c == 1).build();
        }

        public final AudioTrack g(androidx.media3.common.b bVar, int i) {
            int c0 = androidx.media3.common.util.b.c0(bVar.g0);
            if (i == 0) {
                return new AudioTrack(c0, this.e, this.f, this.g, this.h, 1);
            }
            return new AudioTrack(c0, this.e, this.f, this.g, this.h, 1, i);
        }

        public long h(long j) {
            return (j * 1000000) / this.e;
        }

        public long k(long j) {
            return (j * 1000000) / this.a.D0;
        }

        public boolean l() {
            return this.c == 1;
        }
    }

    /* loaded from: classes.dex */
    public static class g implements c {
        public final AudioProcessor[] a;
        public final androidx.media3.exoplayer.audio.i b;
        public final androidx.media3.exoplayer.audio.j c;

        public g(AudioProcessor... audioProcessorArr) {
            this(audioProcessorArr, new androidx.media3.exoplayer.audio.i(), new androidx.media3.exoplayer.audio.j());
        }

        @Override // androidx.media3.exoplayer.audio.DefaultAudioSink.c
        public long a(long j) {
            return this.c.f(j);
        }

        @Override // androidx.media3.exoplayer.audio.DefaultAudioSink.c
        public long b() {
            return this.b.o();
        }

        @Override // androidx.media3.exoplayer.audio.DefaultAudioSink.c
        public boolean c(boolean z) {
            this.b.u(z);
            return z;
        }

        @Override // androidx.media3.exoplayer.audio.DefaultAudioSink.c
        public AudioProcessor[] d() {
            return this.a;
        }

        @Override // androidx.media3.exoplayer.audio.DefaultAudioSink.c
        public p e(p pVar) {
            this.c.h(pVar.a);
            this.c.g(pVar.f0);
            return pVar;
        }

        public g(AudioProcessor[] audioProcessorArr, androidx.media3.exoplayer.audio.i iVar, androidx.media3.exoplayer.audio.j jVar) {
            AudioProcessor[] audioProcessorArr2 = new AudioProcessor[audioProcessorArr.length + 2];
            this.a = audioProcessorArr2;
            System.arraycopy(audioProcessorArr, 0, audioProcessorArr2, 0, audioProcessorArr.length);
            this.b = iVar;
            this.c = jVar;
            audioProcessorArr2[audioProcessorArr.length] = iVar;
            audioProcessorArr2[audioProcessorArr.length + 1] = jVar;
        }
    }

    /* loaded from: classes.dex */
    public static final class h {
        public final p a;
        public final boolean b;
        public final long c;
        public final long d;

        public /* synthetic */ h(p pVar, boolean z, long j, long j2, a aVar) {
            this(pVar, z, j, j2);
        }

        public h(p pVar, boolean z, long j, long j2) {
            this.a = pVar;
            this.b = z;
            this.c = j;
            this.d = j2;
        }
    }

    /* loaded from: classes.dex */
    public static final class i<T extends Exception> {
        public final long a;
        public T b;
        public long c;

        public i(long j) {
            this.a = j;
        }

        public void a() {
            this.b = null;
        }

        public void b(T t) throws Exception {
            long elapsedRealtime = SystemClock.elapsedRealtime();
            if (this.b == null) {
                this.b = t;
                this.c = this.a + elapsedRealtime;
            }
            if (elapsedRealtime >= this.c) {
                T t2 = this.b;
                if (t2 != t) {
                    t2.addSuppressed(t);
                }
                T t3 = this.b;
                a();
                throw t3;
            }
        }
    }

    /* loaded from: classes.dex */
    public final class j implements b.a {
        public j() {
        }

        @Override // androidx.media3.exoplayer.audio.b.a
        public void a(long j) {
            if (DefaultAudioSink.this.r != null) {
                DefaultAudioSink.this.r.a(j);
            }
        }

        @Override // androidx.media3.exoplayer.audio.b.a
        public void b(int i, long j) {
            if (DefaultAudioSink.this.r != null) {
                DefaultAudioSink.this.r.e(i, j, SystemClock.elapsedRealtime() - DefaultAudioSink.this.Z);
            }
        }

        @Override // androidx.media3.exoplayer.audio.b.a
        public void c(long j) {
            p12.i("DefaultAudioSink", "Ignoring impossibly large audio latency: " + j);
        }

        @Override // androidx.media3.exoplayer.audio.b.a
        public void d(long j, long j2, long j3, long j4) {
            String str = "Spurious audio timestamp (frame position mismatch): " + j + ", " + j2 + ", " + j3 + ", " + j4 + ", " + DefaultAudioSink.this.T() + ", " + DefaultAudioSink.this.U();
            if (!DefaultAudioSink.c0) {
                p12.i("DefaultAudioSink", str);
                return;
            }
            throw new InvalidAudioTrackTimestampException(str, null);
        }

        @Override // androidx.media3.exoplayer.audio.b.a
        public void e(long j, long j2, long j3, long j4) {
            String str = "Spurious audio timestamp (system clock mismatch): " + j + ", " + j2 + ", " + j3 + ", " + j4 + ", " + DefaultAudioSink.this.T() + ", " + DefaultAudioSink.this.U();
            if (!DefaultAudioSink.c0) {
                p12.i("DefaultAudioSink", str);
                return;
            }
            throw new InvalidAudioTrackTimestampException(str, null);
        }

        public /* synthetic */ j(DefaultAudioSink defaultAudioSink, a aVar) {
            this();
        }
    }

    /* loaded from: classes.dex */
    public final class k {
        public final Handler a = new Handler();
        public final AudioTrack.StreamEventCallback b;

        /* loaded from: classes.dex */
        public class a extends AudioTrack.StreamEventCallback {
            public a(DefaultAudioSink defaultAudioSink) {
            }

            @Override // android.media.AudioTrack.StreamEventCallback
            public void onDataRequest(AudioTrack audioTrack, int i) {
                ii.g(audioTrack == DefaultAudioSink.this.u);
                if (DefaultAudioSink.this.r == null || !DefaultAudioSink.this.U) {
                    return;
                }
                DefaultAudioSink.this.r.g();
            }

            @Override // android.media.AudioTrack.StreamEventCallback
            public void onTearDown(AudioTrack audioTrack) {
                ii.g(audioTrack == DefaultAudioSink.this.u);
                if (DefaultAudioSink.this.r == null || !DefaultAudioSink.this.U) {
                    return;
                }
                DefaultAudioSink.this.r.g();
            }
        }

        public k() {
            this.b = new a(DefaultAudioSink.this);
        }

        public void a(AudioTrack audioTrack) {
            Handler handler = this.a;
            Objects.requireNonNull(handler);
            audioTrack.registerStreamEventCallback(new zh0(handler), this.b);
        }

        public void b(AudioTrack audioTrack) {
            audioTrack.unregisterStreamEventCallback(this.b);
            this.a.removeCallbacksAndMessages(null);
        }
    }

    public /* synthetic */ DefaultAudioSink(e eVar, a aVar) {
        this(eVar);
    }

    public static AudioFormat M(int i2, int i3, int i4) {
        return new AudioFormat.Builder().setSampleRate(i2).setChannelMask(i3).setEncoding(i4).build();
    }

    public static int O(int i2, int i3, int i4) {
        int minBufferSize = AudioTrack.getMinBufferSize(i2, i3, i4);
        ii.g(minBufferSize != -2);
        return minBufferSize;
    }

    public static int P(int i2, ByteBuffer byteBuffer) {
        switch (i2) {
            case 5:
            case 6:
            case 18:
                return t5.d(byteBuffer);
            case 7:
            case 8:
                return hs0.e(byteBuffer);
            case 9:
                int m = la2.m(androidx.media3.common.util.b.G(byteBuffer, byteBuffer.position()));
                if (m != -1) {
                    return m;
                }
                throw new IllegalArgumentException();
            case 10:
                return RecyclerView.a0.FLAG_ADAPTER_FULLUPDATE;
            case 11:
            case 12:
                return 2048;
            case 13:
            default:
                throw new IllegalStateException("Unexpected audio encoding: " + i2);
            case 14:
                int a2 = t5.a(byteBuffer);
                if (a2 == -1) {
                    return 0;
                }
                return t5.h(byteBuffer, a2) * 16;
            case 15:
                return RecyclerView.a0.FLAG_ADAPTER_POSITION_UNKNOWN;
            case 16:
                return RecyclerView.a0.FLAG_ADAPTER_FULLUPDATE;
            case 17:
                return x5.c(byteBuffer);
        }
    }

    public static boolean W(int i2) {
        return (androidx.media3.common.util.b.a >= 24 && i2 == -6) || i2 == -32;
    }

    public static boolean Y(AudioTrack audioTrack) {
        return androidx.media3.common.util.b.a >= 29 && audioTrack.isOffloadedPlayback();
    }

    public static void h0(AudioTrack audioTrack, float f2) {
        audioTrack.setVolume(f2);
    }

    public static void i0(AudioTrack audioTrack, float f2) {
        audioTrack.setStereoVolume(f2, f2);
    }

    public static int o0(AudioTrack audioTrack, ByteBuffer byteBuffer, int i2) {
        return audioTrack.write(byteBuffer, i2, 1);
    }

    public final void F(long j2) {
        p pVar;
        if (k0()) {
            pVar = this.b.e(N());
        } else {
            pVar = p.h0;
        }
        p pVar2 = pVar;
        boolean c2 = k0() ? this.b.c(S()) : false;
        this.j.add(new h(pVar2, c2, Math.max(0L, j2), this.t.h(U()), null));
        j0();
        AudioSink.a aVar = this.r;
        if (aVar != null) {
            aVar.b(c2);
        }
    }

    public final long G(long j2) {
        while (!this.j.isEmpty() && j2 >= this.j.getFirst().d) {
            this.x = this.j.remove();
        }
        h hVar = this.x;
        long j3 = j2 - hVar.d;
        if (hVar.a.equals(p.h0)) {
            return this.x.c + j3;
        }
        if (this.j.isEmpty()) {
            return this.x.c + this.b.a(j3);
        }
        h first = this.j.getFirst();
        return first.c - androidx.media3.common.util.b.W(first.d - j2, this.x.a.a);
    }

    public final long H(long j2) {
        return j2 + this.t.h(this.b.b());
    }

    public final AudioTrack I(f fVar) throws AudioSink.InitializationException {
        try {
            return fVar.a(this.Y, this.v, this.W);
        } catch (AudioSink.InitializationException e2) {
            AudioSink.a aVar = this.r;
            if (aVar != null) {
                aVar.c(e2);
            }
            throw e2;
        }
    }

    public final AudioTrack J() throws AudioSink.InitializationException {
        try {
            return I((f) ii.e(this.t));
        } catch (AudioSink.InitializationException e2) {
            f fVar = this.t;
            if (fVar.h > 1000000) {
                f c2 = fVar.c(PlaybackException.CUSTOM_ERROR_CODE_BASE);
                try {
                    AudioTrack I = I(c2);
                    this.t = c2;
                    return I;
                } catch (AudioSink.InitializationException e3) {
                    e2.addSuppressed(e3);
                    Z();
                    throw e2;
                }
            }
            Z();
            throw e2;
        }
    }

    /* JADX WARN: Removed duplicated region for block: B:16:0x002f  */
    /* JADX WARN: Removed duplicated region for block: B:9:0x0018  */
    /* JADX WARN: Unsupported multi-entry loop pattern (BACK_EDGE: B:15:0x0029 -> B:5:0x0009). Please submit an issue!!! */
    /*
        Code decompiled incorrectly, please refer to instructions dump.
        To view partially-correct code enable 'Show inconsistent code' option in preferences
    */
    public final boolean K() throws androidx.media3.exoplayer.audio.AudioSink.WriteException {
        /*
            r9 = this;
            int r0 = r9.R
            r1 = -1
            r2 = 1
            r3 = 0
            if (r0 != r1) goto Lb
            r9.R = r3
        L9:
            r0 = r2
            goto Lc
        Lb:
            r0 = r3
        Lc:
            int r4 = r9.R
            androidx.media3.exoplayer.audio.AudioProcessor[] r5 = r9.K
            int r6 = r5.length
            r7 = -9223372036854775807(0x8000000000000001, double:-4.9E-324)
            if (r4 >= r6) goto L2f
            r4 = r5[r4]
            if (r0 == 0) goto L1f
            r4.e()
        L1f:
            r9.b0(r7)
            boolean r0 = r4.d()
            if (r0 != 0) goto L29
            return r3
        L29:
            int r0 = r9.R
            int r0 = r0 + r2
            r9.R = r0
            goto L9
        L2f:
            java.nio.ByteBuffer r0 = r9.O
            if (r0 == 0) goto L3b
            r9.n0(r0, r7)
            java.nio.ByteBuffer r0 = r9.O
            if (r0 == 0) goto L3b
            return r3
        L3b:
            r9.R = r1
            return r2
        */
        throw new UnsupportedOperationException("Method not decompiled: androidx.media3.exoplayer.audio.DefaultAudioSink.K():boolean");
    }

    public final void L() {
        int i2 = 0;
        while (true) {
            AudioProcessor[] audioProcessorArr = this.K;
            if (i2 >= audioProcessorArr.length) {
                return;
            }
            AudioProcessor audioProcessor = audioProcessorArr[i2];
            audioProcessor.flush();
            this.L[i2] = audioProcessor.getOutput();
            i2++;
        }
    }

    public final p N() {
        return Q().a;
    }

    public final h Q() {
        h hVar = this.w;
        if (hVar != null) {
            return hVar;
        }
        if (!this.j.isEmpty()) {
            return this.j.getLast();
        }
        return this.x;
    }

    @SuppressLint({"InlinedApi"})
    public final int R(AudioFormat audioFormat, AudioAttributes audioAttributes) {
        int i2 = androidx.media3.common.util.b.a;
        if (i2 >= 31) {
            return AudioManager.getPlaybackOffloadSupport(audioFormat, audioAttributes);
        }
        if (AudioManager.isOffloadedPlaybackSupported(audioFormat, audioAttributes)) {
            return (i2 == 30 && androidx.media3.common.util.b.d.startsWith("Pixel")) ? 2 : 1;
        }
        return 0;
    }

    public boolean S() {
        return Q().b;
    }

    public final long T() {
        f fVar = this.t;
        if (fVar.c == 0) {
            return this.B / fVar.b;
        }
        return this.C;
    }

    public final long U() {
        f fVar = this.t;
        if (fVar.c == 0) {
            return this.D / fVar.d;
        }
        return this.E;
    }

    public final boolean V() throws AudioSink.InitializationException {
        ks2 ks2Var;
        if (this.h.d()) {
            AudioTrack J = J();
            this.u = J;
            if (Y(J)) {
                c0(this.u);
                if (this.l != 3) {
                    AudioTrack audioTrack = this.u;
                    androidx.media3.common.j jVar = this.t.a;
                    audioTrack.setOffloadDelayPadding(jVar.F0, jVar.G0);
                }
            }
            if (androidx.media3.common.util.b.a >= 31 && (ks2Var = this.q) != null) {
                b.a(this.u, ks2Var);
            }
            this.W = this.u.getAudioSessionId();
            androidx.media3.exoplayer.audio.b bVar = this.i;
            AudioTrack audioTrack2 = this.u;
            f fVar = this.t;
            bVar.s(audioTrack2, fVar.c == 2, fVar.g, fVar.d, fVar.h);
            g0();
            int i2 = this.X.a;
            if (i2 != 0) {
                this.u.attachAuxEffect(i2);
                this.u.setAuxEffectSendLevel(this.X.b);
            }
            this.H = true;
            return true;
        }
        return false;
    }

    public final boolean X() {
        return this.u != null;
    }

    public final void Z() {
        if (this.t.l()) {
            this.a0 = true;
        }
    }

    @Override // androidx.media3.exoplayer.audio.AudioSink
    public boolean a(androidx.media3.common.j jVar) {
        return s(jVar) != 0;
    }

    public final void a0() {
        if (this.T) {
            return;
        }
        this.T = true;
        this.i.g(U());
        this.u.stop();
        this.A = 0;
    }

    @Override // androidx.media3.exoplayer.audio.AudioSink
    public void b(p pVar) {
        p pVar2 = new p(androidx.media3.common.util.b.p(pVar.a, 0.1f, 8.0f), androidx.media3.common.util.b.p(pVar.f0, 0.1f, 8.0f));
        if (this.k && androidx.media3.common.util.b.a >= 23) {
            f0(pVar2);
        } else {
            e0(pVar2, S());
        }
    }

    public final void b0(long j2) throws AudioSink.WriteException {
        ByteBuffer byteBuffer;
        int length = this.K.length;
        int i2 = length;
        while (i2 >= 0) {
            if (i2 > 0) {
                byteBuffer = this.L[i2 - 1];
            } else {
                byteBuffer = this.M;
                if (byteBuffer == null) {
                    byteBuffer = AudioProcessor.a;
                }
            }
            if (i2 == length) {
                n0(byteBuffer, j2);
            } else {
                AudioProcessor audioProcessor = this.K[i2];
                if (i2 > this.R) {
                    audioProcessor.c(byteBuffer);
                }
                ByteBuffer output = audioProcessor.getOutput();
                this.L[i2] = output;
                if (output.hasRemaining()) {
                    i2++;
                }
            }
            if (byteBuffer.hasRemaining()) {
                return;
            }
            i2--;
        }
    }

    @Override // androidx.media3.exoplayer.audio.AudioSink
    public void c() {
        this.U = false;
        if (X() && this.i.p()) {
            this.u.pause();
        }
    }

    public final void c0(AudioTrack audioTrack) {
        if (this.m == null) {
            this.m = new k();
        }
        this.m.a(audioTrack);
    }

    @Override // androidx.media3.exoplayer.audio.AudioSink
    public boolean d() {
        return !X() || (this.S && !j());
    }

    public final void d0() {
        this.B = 0L;
        this.C = 0L;
        this.D = 0L;
        this.E = 0L;
        this.b0 = false;
        this.F = 0;
        this.x = new h(N(), S(), 0L, 0L, null);
        this.I = 0L;
        this.w = null;
        this.j.clear();
        this.M = null;
        this.N = 0;
        this.O = null;
        this.T = false;
        this.S = false;
        this.R = -1;
        this.z = null;
        this.A = 0;
        this.e.m();
        L();
    }

    @Override // androidx.media3.exoplayer.audio.AudioSink
    public p e() {
        if (this.k) {
            return this.y;
        }
        return N();
    }

    public final void e0(p pVar, boolean z) {
        h Q = Q();
        if (pVar.equals(Q.a) && z == Q.b) {
            return;
        }
        h hVar = new h(pVar, z, CellBase.ID_SYSTEM_MESSAGE_REQUEST_CLOSED, CellBase.ID_SYSTEM_MESSAGE_REQUEST_CLOSED, null);
        if (X()) {
            this.w = hVar;
        } else {
            this.x = hVar;
        }
    }

    @Override // androidx.media3.exoplayer.audio.AudioSink
    public void f() {
        this.U = true;
        if (X()) {
            this.i.u();
            this.u.play();
        }
    }

    public final void f0(p pVar) {
        if (X()) {
            try {
                this.u.setPlaybackParams(new PlaybackParams().allowDefaults().setSpeed(pVar.a).setPitch(pVar.f0).setAudioFallbackMode(2));
            } catch (IllegalArgumentException e2) {
                p12.j("DefaultAudioSink", "Failed to set playback params", e2);
            }
            pVar = new p(this.u.getPlaybackParams().getSpeed(), this.u.getPlaybackParams().getPitch());
            this.i.t(pVar.a);
        }
        this.y = pVar;
    }

    @Override // androidx.media3.exoplayer.audio.AudioSink
    public void flush() {
        if (X()) {
            d0();
            if (this.i.i()) {
                this.u.pause();
            }
            if (Y(this.u)) {
                ((k) ii.e(this.m)).b(this.u);
            }
            AudioTrack audioTrack = this.u;
            this.u = null;
            if (androidx.media3.common.util.b.a < 21 && !this.V) {
                this.W = 0;
            }
            f fVar = this.s;
            if (fVar != null) {
                this.t = fVar;
                this.s = null;
            }
            this.i.q();
            this.h.c();
            new a("ExoPlayer:AudioTrackReleaseThread", audioTrack).start();
        }
        this.o.a();
        this.n.a();
    }

    @Override // androidx.media3.exoplayer.audio.AudioSink
    public void g(androidx.media3.common.j jVar, int i2, int[] iArr) throws AudioSink.ConfigurationException {
        AudioProcessor[] audioProcessorArr;
        int i3;
        int i4;
        int i5;
        int i6;
        int intValue;
        int i7;
        int i8;
        int a2;
        AudioProcessor[] audioProcessorArr2;
        int[] iArr2;
        if ("audio/raw".equals(jVar.p0)) {
            ii.a(androidx.media3.common.util.b.q0(jVar.E0));
            i3 = androidx.media3.common.util.b.a0(jVar.E0, jVar.C0);
            if (l0(jVar.E0)) {
                audioProcessorArr2 = this.g;
            } else {
                audioProcessorArr2 = this.f;
            }
            this.e.n(jVar.F0, jVar.G0);
            if (androidx.media3.common.util.b.a < 21 && jVar.C0 == 8 && iArr == null) {
                iArr2 = new int[6];
                for (int i9 = 0; i9 < 6; i9++) {
                    iArr2[i9] = i9;
                }
            } else {
                iArr2 = iArr;
            }
            this.d.l(iArr2);
            AudioProcessor.a aVar = new AudioProcessor.a(jVar.D0, jVar.C0, jVar.E0);
            for (AudioProcessor audioProcessor : audioProcessorArr2) {
                try {
                    AudioProcessor.a a3 = audioProcessor.a(aVar);
                    if (audioProcessor.b()) {
                        aVar = a3;
                    }
                } catch (AudioProcessor.UnhandledAudioFormatException e2) {
                    throw new AudioSink.ConfigurationException(e2, jVar);
                }
            }
            int i10 = aVar.c;
            int i11 = aVar.a;
            int E = androidx.media3.common.util.b.E(aVar.b);
            audioProcessorArr = audioProcessorArr2;
            i6 = 0;
            i4 = androidx.media3.common.util.b.a0(i10, aVar.b);
            i7 = i10;
            i5 = i11;
            intValue = E;
        } else {
            AudioProcessor[] audioProcessorArr3 = new AudioProcessor[0];
            int i12 = jVar.D0;
            if (m0(jVar, this.v)) {
                audioProcessorArr = audioProcessorArr3;
                i3 = -1;
                i4 = -1;
                i5 = i12;
                i7 = y82.d((String) ii.e(jVar.p0), jVar.m0);
                intValue = androidx.media3.common.util.b.E(jVar.C0);
                i6 = 1;
            } else {
                Pair<Integer, Integer> f2 = this.a.f(jVar);
                if (f2 != null) {
                    int intValue2 = ((Integer) f2.first).intValue();
                    audioProcessorArr = audioProcessorArr3;
                    i3 = -1;
                    i4 = -1;
                    i5 = i12;
                    i6 = 2;
                    intValue = ((Integer) f2.second).intValue();
                    i7 = intValue2;
                } else {
                    throw new AudioSink.ConfigurationException("Unable to configure passthrough for: " + jVar, jVar);
                }
            }
        }
        if (i2 != 0) {
            a2 = i2;
            i8 = i7;
        } else {
            i8 = i7;
            a2 = this.p.a(O(i5, intValue, i7), i7, i6, i4, i5, this.k ? 8.0d : 1.0d);
        }
        if (i8 == 0) {
            throw new AudioSink.ConfigurationException("Invalid output encoding (mode=" + i6 + ") for: " + jVar, jVar);
        } else if (intValue != 0) {
            this.a0 = false;
            f fVar = new f(jVar, i3, i6, i4, i5, intValue, i8, a2, audioProcessorArr);
            if (X()) {
                this.s = fVar;
            } else {
                this.t = fVar;
            }
        } else {
            throw new AudioSink.ConfigurationException("Invalid output channel config (mode=" + i6 + ") for: " + jVar, jVar);
        }
    }

    public final void g0() {
        if (X()) {
            if (androidx.media3.common.util.b.a >= 21) {
                h0(this.u, this.J);
            } else {
                i0(this.u, this.J);
            }
        }
    }

    @Override // androidx.media3.exoplayer.audio.AudioSink
    public void h(androidx.media3.common.b bVar) {
        if (this.v.equals(bVar)) {
            return;
        }
        this.v = bVar;
        if (this.Y) {
            return;
        }
        flush();
    }

    @Override // androidx.media3.exoplayer.audio.AudioSink
    public void i() throws AudioSink.WriteException {
        if (!this.S && X() && K()) {
            a0();
            this.S = true;
        }
    }

    @Override // androidx.media3.exoplayer.audio.AudioSink
    public boolean j() {
        return X() && this.i.h(U());
    }

    public final void j0() {
        AudioProcessor[] audioProcessorArr = this.t.i;
        ArrayList arrayList = new ArrayList();
        for (AudioProcessor audioProcessor : audioProcessorArr) {
            if (audioProcessor.b()) {
                arrayList.add(audioProcessor);
            } else {
                audioProcessor.flush();
            }
        }
        int size = arrayList.size();
        this.K = (AudioProcessor[]) arrayList.toArray(new AudioProcessor[size]);
        this.L = new ByteBuffer[size];
        L();
    }

    @Override // androidx.media3.exoplayer.audio.AudioSink
    public void k(int i2) {
        if (this.W != i2) {
            this.W = i2;
            this.V = i2 != 0;
            flush();
        }
    }

    public final boolean k0() {
        return (this.Y || !"audio/raw".equals(this.t.a.p0) || l0(this.t.a.E0)) ? false : true;
    }

    @Override // androidx.media3.exoplayer.audio.AudioSink
    public void l(AudioSink.a aVar) {
        this.r = aVar;
    }

    public final boolean l0(int i2) {
        return this.c && androidx.media3.common.util.b.p0(i2);
    }

    @Override // androidx.media3.exoplayer.audio.AudioSink
    public long m(boolean z) {
        if (!X() || this.H) {
            return Long.MIN_VALUE;
        }
        return H(G(Math.min(this.i.d(z), this.t.h(U()))));
    }

    public final boolean m0(androidx.media3.common.j jVar, androidx.media3.common.b bVar) {
        int d2;
        int E;
        int R;
        if (androidx.media3.common.util.b.a < 29 || this.l == 0 || (d2 = y82.d((String) ii.e(jVar.p0), jVar.m0)) == 0 || (E = androidx.media3.common.util.b.E(jVar.C0)) == 0 || (R = R(M(jVar.D0, E, d2), bVar.a().a)) == 0) {
            return false;
        }
        if (R == 1) {
            return ((jVar.F0 != 0 || jVar.G0 != 0) && (this.l == 1)) ? false : true;
        } else if (R == 2) {
            return true;
        } else {
            throw new IllegalStateException();
        }
    }

    @Override // androidx.media3.exoplayer.audio.AudioSink
    public void n() {
        if (this.Y) {
            this.Y = false;
            flush();
        }
    }

    public final void n0(ByteBuffer byteBuffer, long j2) throws AudioSink.WriteException {
        int o0;
        AudioSink.a aVar;
        if (byteBuffer.hasRemaining()) {
            ByteBuffer byteBuffer2 = this.O;
            if (byteBuffer2 != null) {
                ii.a(byteBuffer2 == byteBuffer);
            } else {
                this.O = byteBuffer;
                if (androidx.media3.common.util.b.a < 21) {
                    int remaining = byteBuffer.remaining();
                    byte[] bArr = this.P;
                    if (bArr == null || bArr.length < remaining) {
                        this.P = new byte[remaining];
                    }
                    int position = byteBuffer.position();
                    byteBuffer.get(this.P, 0, remaining);
                    byteBuffer.position(position);
                    this.Q = 0;
                }
            }
            int remaining2 = byteBuffer.remaining();
            if (androidx.media3.common.util.b.a < 21) {
                int c2 = this.i.c(this.D);
                if (c2 > 0) {
                    o0 = this.u.write(this.P, this.Q, Math.min(remaining2, c2));
                    if (o0 > 0) {
                        this.Q += o0;
                        byteBuffer.position(byteBuffer.position() + o0);
                    }
                } else {
                    o0 = 0;
                }
            } else if (this.Y) {
                ii.g(j2 != CellBase.ID_SYSTEM_MESSAGE_REQUEST_CLOSED);
                o0 = p0(this.u, byteBuffer, remaining2, j2);
            } else {
                o0 = o0(this.u, byteBuffer, remaining2);
            }
            this.Z = SystemClock.elapsedRealtime();
            if (o0 < 0) {
                boolean W = W(o0);
                if (W) {
                    Z();
                }
                AudioSink.WriteException writeException = new AudioSink.WriteException(o0, this.t.a, W);
                AudioSink.a aVar2 = this.r;
                if (aVar2 != null) {
                    aVar2.c(writeException);
                }
                if (!writeException.isRecoverable) {
                    this.o.b(writeException);
                    return;
                }
                throw writeException;
            }
            this.o.a();
            if (Y(this.u)) {
                if (this.E > 0) {
                    this.b0 = false;
                }
                if (this.U && (aVar = this.r) != null && o0 < remaining2 && !this.b0) {
                    aVar.d();
                }
            }
            int i2 = this.t.c;
            if (i2 == 0) {
                this.D += o0;
            }
            if (o0 == remaining2) {
                if (i2 != 0) {
                    ii.g(byteBuffer == this.M);
                    this.E += this.F * this.N;
                }
                this.O = null;
            }
        }
    }

    @Override // androidx.media3.exoplayer.audio.AudioSink
    public void o() {
        this.G = true;
    }

    @Override // androidx.media3.exoplayer.audio.AudioSink
    public void p(float f2) {
        if (this.J != f2) {
            this.J = f2;
            g0();
        }
    }

    public final int p0(AudioTrack audioTrack, ByteBuffer byteBuffer, int i2, long j2) {
        if (androidx.media3.common.util.b.a >= 26) {
            return audioTrack.write(byteBuffer, i2, 1, j2 * 1000);
        }
        if (this.z == null) {
            ByteBuffer allocate = ByteBuffer.allocate(16);
            this.z = allocate;
            allocate.order(ByteOrder.BIG_ENDIAN);
            this.z.putInt(1431633921);
        }
        if (this.A == 0) {
            this.z.putInt(4, i2);
            this.z.putLong(8, j2 * 1000);
            this.z.position(0);
            this.A = i2;
        }
        int remaining = this.z.remaining();
        if (remaining > 0) {
            int write = audioTrack.write(this.z, remaining, 1);
            if (write < 0) {
                this.A = 0;
                return write;
            } else if (write < remaining) {
                return 0;
            }
        }
        int o0 = o0(audioTrack, byteBuffer, i2);
        if (o0 < 0) {
            this.A = 0;
            return o0;
        }
        this.A -= o0;
        return o0;
    }

    @Override // androidx.media3.exoplayer.audio.AudioSink
    public void q(ks2 ks2Var) {
        this.q = ks2Var;
    }

    @Override // androidx.media3.exoplayer.audio.AudioSink
    public void r() {
        ii.g(androidx.media3.common.util.b.a >= 21);
        ii.g(this.V);
        if (this.Y) {
            return;
        }
        this.Y = true;
        flush();
    }

    @Override // androidx.media3.exoplayer.audio.AudioSink
    public void reset() {
        flush();
        for (AudioProcessor audioProcessor : this.f) {
            audioProcessor.reset();
        }
        for (AudioProcessor audioProcessor2 : this.g) {
            audioProcessor2.reset();
        }
        this.U = false;
        this.a0 = false;
    }

    @Override // androidx.media3.exoplayer.audio.AudioSink
    public int s(androidx.media3.common.j jVar) {
        if (!"audio/raw".equals(jVar.p0)) {
            return ((this.a0 || !m0(jVar, this.v)) && !this.a.h(jVar)) ? 0 : 2;
        } else if (!androidx.media3.common.util.b.q0(jVar.E0)) {
            p12.i("DefaultAudioSink", "Invalid PCM encoding: " + jVar.E0);
            return 0;
        } else {
            int i2 = jVar.E0;
            return (i2 == 2 || (this.c && i2 == 4)) ? 2 : 1;
        }
    }

    @Override // androidx.media3.exoplayer.audio.AudioSink
    public void t(ql qlVar) {
        if (this.X.equals(qlVar)) {
            return;
        }
        int i2 = qlVar.a;
        float f2 = qlVar.b;
        AudioTrack audioTrack = this.u;
        if (audioTrack != null) {
            if (this.X.a != i2) {
                audioTrack.attachAuxEffect(i2);
            }
            if (i2 != 0) {
                this.u.setAuxEffectSendLevel(f2);
            }
        }
        this.X = qlVar;
    }

    @Override // androidx.media3.exoplayer.audio.AudioSink
    public boolean u(ByteBuffer byteBuffer, long j2, int i2) throws AudioSink.InitializationException, AudioSink.WriteException {
        ByteBuffer byteBuffer2 = this.M;
        ii.a(byteBuffer2 == null || byteBuffer == byteBuffer2);
        if (this.s != null) {
            if (!K()) {
                return false;
            }
            if (!this.s.b(this.t)) {
                a0();
                if (j()) {
                    return false;
                }
                flush();
            } else {
                this.t = this.s;
                this.s = null;
                if (Y(this.u) && this.l != 3) {
                    if (this.u.getPlayState() == 3) {
                        this.u.setOffloadEndOfStream();
                    }
                    AudioTrack audioTrack = this.u;
                    androidx.media3.common.j jVar = this.t.a;
                    audioTrack.setOffloadDelayPadding(jVar.F0, jVar.G0);
                    this.b0 = true;
                }
            }
            F(j2);
        }
        if (!X()) {
            try {
                if (!V()) {
                    return false;
                }
            } catch (AudioSink.InitializationException e2) {
                if (!e2.isRecoverable) {
                    this.n.b(e2);
                    return false;
                }
                throw e2;
            }
        }
        this.n.a();
        if (this.H) {
            this.I = Math.max(0L, j2);
            this.G = false;
            this.H = false;
            if (this.k && androidx.media3.common.util.b.a >= 23) {
                f0(this.y);
            }
            F(j2);
            if (this.U) {
                f();
            }
        }
        if (this.i.k(U())) {
            if (this.M == null) {
                ii.a(byteBuffer.order() == ByteOrder.LITTLE_ENDIAN);
                if (!byteBuffer.hasRemaining()) {
                    return true;
                }
                f fVar = this.t;
                if (fVar.c != 0 && this.F == 0) {
                    int P = P(fVar.g, byteBuffer);
                    this.F = P;
                    if (P == 0) {
                        return true;
                    }
                }
                if (this.w != null) {
                    if (!K()) {
                        return false;
                    }
                    F(j2);
                    this.w = null;
                }
                long k2 = this.I + this.t.k(T() - this.e.l());
                if (!this.G && Math.abs(k2 - j2) > 200000) {
                    this.r.c(new AudioSink.UnexpectedDiscontinuityException(j2, k2));
                    this.G = true;
                }
                if (this.G) {
                    if (!K()) {
                        return false;
                    }
                    long j3 = j2 - k2;
                    this.I += j3;
                    this.G = false;
                    F(j2);
                    AudioSink.a aVar = this.r;
                    if (aVar != null && j3 != 0) {
                        aVar.f();
                    }
                }
                if (this.t.c == 0) {
                    this.B += byteBuffer.remaining();
                } else {
                    this.C += this.F * i2;
                }
                this.M = byteBuffer;
                this.N = i2;
            }
            b0(j2);
            if (!this.M.hasRemaining()) {
                this.M = null;
                this.N = 0;
                return true;
            } else if (this.i.j(U())) {
                p12.i("DefaultAudioSink", "Resetting stalled audio track");
                flush();
                return true;
            } else {
                return false;
            }
        }
        return false;
    }

    @Override // androidx.media3.exoplayer.audio.AudioSink
    public void v() {
        if (androidx.media3.common.util.b.a < 25) {
            flush();
            return;
        }
        this.o.a();
        this.n.a();
        if (X()) {
            d0();
            if (this.i.i()) {
                this.u.pause();
            }
            this.u.flush();
            this.i.q();
            androidx.media3.exoplayer.audio.b bVar = this.i;
            AudioTrack audioTrack = this.u;
            f fVar = this.t;
            bVar.s(audioTrack, fVar.c == 2, fVar.g, fVar.d, fVar.h);
            this.H = true;
        }
    }

    @Override // androidx.media3.exoplayer.audio.AudioSink
    public void w(boolean z) {
        e0(N(), z);
    }

    public DefaultAudioSink(e eVar) {
        this.a = eVar.a;
        c cVar = eVar.b;
        this.b = cVar;
        int i2 = androidx.media3.common.util.b.a;
        this.c = i2 >= 21 && eVar.c;
        this.k = i2 >= 23 && eVar.d;
        this.l = i2 >= 29 ? eVar.e : 0;
        this.p = eVar.f;
        androidx.media3.common.util.a aVar = new androidx.media3.common.util.a(tz.a);
        this.h = aVar;
        aVar.e();
        this.i = new androidx.media3.exoplayer.audio.b(new j(this, null));
        androidx.media3.exoplayer.audio.d dVar = new androidx.media3.exoplayer.audio.d();
        this.d = dVar;
        androidx.media3.exoplayer.audio.k kVar = new androidx.media3.exoplayer.audio.k();
        this.e = kVar;
        ArrayList arrayList = new ArrayList();
        Collections.addAll(arrayList, new androidx.media3.exoplayer.audio.h(), dVar, kVar);
        Collections.addAll(arrayList, cVar.d());
        this.f = (AudioProcessor[]) arrayList.toArray(new AudioProcessor[0]);
        this.g = new AudioProcessor[]{new androidx.media3.exoplayer.audio.f()};
        this.J = 1.0f;
        this.v = androidx.media3.common.b.k0;
        this.W = 0;
        this.X = new ql(0, Utils.FLOAT_EPSILON);
        p pVar = p.h0;
        this.x = new h(pVar, false, 0L, 0L, null);
        this.y = pVar;
        this.R = -1;
        this.K = new AudioProcessor[0];
        this.L = new ByteBuffer[0];
        this.j = new ArrayDeque<>();
        this.n = new i<>(100L);
        this.o = new i<>(100L);
    }
}
