package androidx.media3.exoplayer.audio;

import android.annotation.SuppressLint;
import android.content.Context;
import android.media.MediaCrypto;
import android.media.MediaFormat;
import android.os.Handler;
import androidx.media3.common.PlaybackException;
import androidx.media3.common.j;
import androidx.media3.common.p;
import androidx.media3.decoder.DecoderInputBuffer;
import androidx.media3.exoplayer.ExoPlaybackException;
import androidx.media3.exoplayer.audio.AudioSink;
import androidx.media3.exoplayer.audio.a;
import androidx.media3.exoplayer.m;
import androidx.media3.exoplayer.mediacodec.MediaCodecRenderer;
import androidx.media3.exoplayer.mediacodec.MediaCodecUtil;
import androidx.media3.exoplayer.mediacodec.d;
import com.google.common.collect.ImmutableList;
import java.nio.ByteBuffer;
import java.util.Collection;
import java.util.List;

/* compiled from: MediaCodecAudioRenderer.java */
/* loaded from: classes.dex */
public class g extends MediaCodecRenderer implements u52 {
    public final Context J1;
    public final a.C0031a K1;
    public final AudioSink L1;
    public int M1;
    public boolean N1;
    public androidx.media3.common.j O1;
    public long P1;
    public boolean Q1;
    public boolean R1;
    public boolean S1;
    public boolean T1;
    public m.a U1;

    /* compiled from: MediaCodecAudioRenderer.java */
    /* loaded from: classes.dex */
    public final class b implements AudioSink.a {
        public b() {
        }

        @Override // androidx.media3.exoplayer.audio.AudioSink.a
        public void a(long j) {
            g.this.K1.B(j);
        }

        @Override // androidx.media3.exoplayer.audio.AudioSink.a
        public void b(boolean z) {
            g.this.K1.C(z);
        }

        @Override // androidx.media3.exoplayer.audio.AudioSink.a
        public void c(Exception exc) {
            p12.d("MediaCodecAudioRenderer", "Audio sink error", exc);
            g.this.K1.l(exc);
        }

        @Override // androidx.media3.exoplayer.audio.AudioSink.a
        public void d() {
            if (g.this.U1 != null) {
                g.this.U1.a();
            }
        }

        @Override // androidx.media3.exoplayer.audio.AudioSink.a
        public void e(int i, long j, long j2) {
            g.this.K1.D(i, j, j2);
        }

        @Override // androidx.media3.exoplayer.audio.AudioSink.a
        public void f() {
            g.this.w1();
        }

        @Override // androidx.media3.exoplayer.audio.AudioSink.a
        public void g() {
            if (g.this.U1 != null) {
                g.this.U1.b();
            }
        }
    }

    public g(Context context, d.b bVar, androidx.media3.exoplayer.mediacodec.f fVar, boolean z, Handler handler, androidx.media3.exoplayer.audio.a aVar, AudioSink audioSink) {
        super(1, bVar, fVar, z, 44100.0f);
        this.J1 = context.getApplicationContext();
        this.L1 = audioSink;
        this.K1 = new a.C0031a(handler, aVar);
        audioSink.l(new b());
    }

    public static boolean q1(String str) {
        if (androidx.media3.common.util.b.a < 24 && "OMX.SEC.aac.dec".equals(str) && "samsung".equals(androidx.media3.common.util.b.c)) {
            String str2 = androidx.media3.common.util.b.b;
            if (str2.startsWith("zeroflte") || str2.startsWith("herolte") || str2.startsWith("heroqlte")) {
                return true;
            }
        }
        return false;
    }

    public static boolean r1() {
        if (androidx.media3.common.util.b.a == 23) {
            String str = androidx.media3.common.util.b.d;
            if ("ZTE B2017G".equals(str) || "AXON 7 mini".equals(str)) {
                return true;
            }
        }
        return false;
    }

    public static List<androidx.media3.exoplayer.mediacodec.e> u1(androidx.media3.exoplayer.mediacodec.f fVar, androidx.media3.common.j jVar, boolean z, AudioSink audioSink) throws MediaCodecUtil.DecoderQueryException {
        androidx.media3.exoplayer.mediacodec.e v;
        String str = jVar.p0;
        if (str == null) {
            return ImmutableList.of();
        }
        if (audioSink.a(jVar) && (v = MediaCodecUtil.v()) != null) {
            return ImmutableList.of(v);
        }
        List<androidx.media3.exoplayer.mediacodec.e> a2 = fVar.a(str, z, false);
        String m = MediaCodecUtil.m(jVar);
        if (m == null) {
            return ImmutableList.copyOf((Collection) a2);
        }
        return ImmutableList.builder().j(a2).j(fVar.a(m, z, false)).l();
    }

    @Override // androidx.media3.exoplayer.mediacodec.MediaCodecRenderer, androidx.media3.exoplayer.c
    public void G() {
        this.S1 = true;
        try {
            this.L1.flush();
            try {
                super.G();
            } finally {
            }
        } catch (Throwable th) {
            try {
                super.G();
                throw th;
            } finally {
            }
        }
    }

    @Override // androidx.media3.exoplayer.mediacodec.MediaCodecRenderer, androidx.media3.exoplayer.c
    public void H(boolean z, boolean z2) throws ExoPlaybackException {
        super.H(z, z2);
        this.K1.p(this.E1);
        if (A().a) {
            this.L1.r();
        } else {
            this.L1.n();
        }
        this.L1.q(D());
    }

    @Override // androidx.media3.exoplayer.mediacodec.MediaCodecRenderer, androidx.media3.exoplayer.c
    public void I(long j, boolean z) throws ExoPlaybackException {
        super.I(j, z);
        if (this.T1) {
            this.L1.v();
        } else {
            this.L1.flush();
        }
        this.P1 = j;
        this.Q1 = true;
        this.R1 = true;
    }

    @Override // androidx.media3.exoplayer.mediacodec.MediaCodecRenderer
    public void I0(Exception exc) {
        p12.d("MediaCodecAudioRenderer", "Audio codec error", exc);
        this.K1.k(exc);
    }

    @Override // androidx.media3.exoplayer.mediacodec.MediaCodecRenderer, androidx.media3.exoplayer.c
    public void J() {
        try {
            super.J();
        } finally {
            if (this.S1) {
                this.S1 = false;
                this.L1.reset();
            }
        }
    }

    @Override // androidx.media3.exoplayer.mediacodec.MediaCodecRenderer
    public void J0(String str, d.a aVar, long j, long j2) {
        this.K1.m(str, j, j2);
    }

    @Override // androidx.media3.exoplayer.mediacodec.MediaCodecRenderer, androidx.media3.exoplayer.c
    public void K() {
        super.K();
        this.L1.f();
    }

    @Override // androidx.media3.exoplayer.mediacodec.MediaCodecRenderer
    public void K0(String str) {
        this.K1.n(str);
    }

    @Override // androidx.media3.exoplayer.mediacodec.MediaCodecRenderer, androidx.media3.exoplayer.c
    public void L() {
        x1();
        this.L1.c();
        super.L();
    }

    @Override // androidx.media3.exoplayer.mediacodec.MediaCodecRenderer
    public jf0 L0(y81 y81Var) throws ExoPlaybackException {
        jf0 L0 = super.L0(y81Var);
        this.K1.q(y81Var.b, L0);
        return L0;
    }

    @Override // androidx.media3.exoplayer.mediacodec.MediaCodecRenderer
    public void M0(androidx.media3.common.j jVar, MediaFormat mediaFormat) throws ExoPlaybackException {
        int Y;
        int i;
        androidx.media3.common.j jVar2 = this.O1;
        int[] iArr = null;
        if (jVar2 != null) {
            jVar = jVar2;
        } else if (o0() != null) {
            if ("audio/raw".equals(jVar.p0)) {
                Y = jVar.E0;
            } else if (androidx.media3.common.util.b.a >= 24 && mediaFormat.containsKey("pcm-encoding")) {
                Y = mediaFormat.getInteger("pcm-encoding");
            } else {
                Y = mediaFormat.containsKey("v-bits-per-sample") ? androidx.media3.common.util.b.Y(mediaFormat.getInteger("v-bits-per-sample")) : 2;
            }
            androidx.media3.common.j E = new j.b().e0("audio/raw").Y(Y).N(jVar.F0).O(jVar.G0).H(mediaFormat.getInteger("channel-count")).f0(mediaFormat.getInteger("sample-rate")).E();
            if (this.N1 && E.C0 == 6 && (i = jVar.C0) < 6) {
                iArr = new int[i];
                for (int i2 = 0; i2 < jVar.C0; i2++) {
                    iArr[i2] = i2;
                }
            }
            jVar = E;
        }
        try {
            this.L1.g(jVar, 0, iArr);
        } catch (AudioSink.ConfigurationException e) {
            throw y(e, e.format, PlaybackException.ERROR_CODE_AUDIO_TRACK_INIT_FAILED);
        }
    }

    @Override // androidx.media3.exoplayer.mediacodec.MediaCodecRenderer
    public void O0() {
        super.O0();
        this.L1.o();
    }

    @Override // androidx.media3.exoplayer.mediacodec.MediaCodecRenderer
    public void P0(DecoderInputBuffer decoderInputBuffer) {
        if (!this.Q1 || decoderInputBuffer.o()) {
            return;
        }
        if (Math.abs(decoderInputBuffer.i0 - this.P1) > 500000) {
            this.P1 = decoderInputBuffer.i0;
        }
        this.Q1 = false;
    }

    @Override // androidx.media3.exoplayer.mediacodec.MediaCodecRenderer
    public boolean R0(long j, long j2, androidx.media3.exoplayer.mediacodec.d dVar, ByteBuffer byteBuffer, int i, int i2, int i3, long j3, boolean z, boolean z2, androidx.media3.common.j jVar) throws ExoPlaybackException {
        ii.e(byteBuffer);
        if (this.O1 != null && (i2 & 2) != 0) {
            ((androidx.media3.exoplayer.mediacodec.d) ii.e(dVar)).j(i, false);
            return true;
        } else if (z) {
            if (dVar != null) {
                dVar.j(i, false);
            }
            this.E1.f += i3;
            this.L1.o();
            return true;
        } else {
            try {
                if (this.L1.u(byteBuffer, j3, i3)) {
                    if (dVar != null) {
                        dVar.j(i, false);
                    }
                    this.E1.e += i3;
                    return true;
                }
                return false;
            } catch (AudioSink.InitializationException e) {
                throw z(e, e.format, e.isRecoverable, PlaybackException.ERROR_CODE_AUDIO_TRACK_INIT_FAILED);
            } catch (AudioSink.WriteException e2) {
                throw z(e2, jVar, e2.isRecoverable, PlaybackException.ERROR_CODE_AUDIO_TRACK_WRITE_FAILED);
            }
        }
    }

    @Override // androidx.media3.exoplayer.mediacodec.MediaCodecRenderer
    public jf0 S(androidx.media3.exoplayer.mediacodec.e eVar, androidx.media3.common.j jVar, androidx.media3.common.j jVar2) {
        jf0 e = eVar.e(jVar, jVar2);
        int i = e.e;
        if (s1(eVar, jVar2) > this.M1) {
            i |= 64;
        }
        int i2 = i;
        return new jf0(eVar.a, jVar, jVar2, i2 != 0 ? 0 : e.d, i2);
    }

    @Override // androidx.media3.exoplayer.mediacodec.MediaCodecRenderer
    public void W0() throws ExoPlaybackException {
        try {
            this.L1.i();
        } catch (AudioSink.WriteException e) {
            throw z(e, e.format, e.isRecoverable, PlaybackException.ERROR_CODE_AUDIO_TRACK_WRITE_FAILED);
        }
    }

    @Override // defpackage.u52
    public void b(p pVar) {
        this.L1.b(pVar);
    }

    @Override // androidx.media3.exoplayer.mediacodec.MediaCodecRenderer, androidx.media3.exoplayer.m
    public boolean d() {
        return super.d() && this.L1.d();
    }

    @Override // defpackage.u52
    public p e() {
        return this.L1.e();
    }

    @Override // androidx.media3.exoplayer.mediacodec.MediaCodecRenderer, androidx.media3.exoplayer.m
    public boolean f() {
        return this.L1.j() || super.f();
    }

    @Override // androidx.media3.exoplayer.m, androidx.media3.exoplayer.n
    public String getName() {
        return "MediaCodecAudioRenderer";
    }

    @Override // androidx.media3.exoplayer.mediacodec.MediaCodecRenderer
    public boolean i1(androidx.media3.common.j jVar) {
        return this.L1.a(jVar);
    }

    @Override // androidx.media3.exoplayer.mediacodec.MediaCodecRenderer
    public int j1(androidx.media3.exoplayer.mediacodec.f fVar, androidx.media3.common.j jVar) throws MediaCodecUtil.DecoderQueryException {
        boolean z;
        if (!y82.m(jVar.p0)) {
            return u63.a(0);
        }
        int i = androidx.media3.common.util.b.a >= 21 ? 32 : 0;
        boolean z2 = true;
        boolean z3 = jVar.I0 != 0;
        boolean k1 = MediaCodecRenderer.k1(jVar);
        int i2 = 8;
        if (k1 && this.L1.a(jVar) && (!z3 || MediaCodecUtil.v() != null)) {
            return u63.b(4, 8, i);
        }
        if ("audio/raw".equals(jVar.p0) && !this.L1.a(jVar)) {
            return u63.a(1);
        }
        if (!this.L1.a(androidx.media3.common.util.b.Z(2, jVar.C0, jVar.D0))) {
            return u63.a(1);
        }
        List<androidx.media3.exoplayer.mediacodec.e> u1 = u1(fVar, jVar, false, this.L1);
        if (u1.isEmpty()) {
            return u63.a(1);
        }
        if (!k1) {
            return u63.a(2);
        }
        androidx.media3.exoplayer.mediacodec.e eVar = u1.get(0);
        boolean m = eVar.m(jVar);
        if (!m) {
            for (int i3 = 1; i3 < u1.size(); i3++) {
                androidx.media3.exoplayer.mediacodec.e eVar2 = u1.get(i3);
                if (eVar2.m(jVar)) {
                    z = false;
                    eVar = eVar2;
                    break;
                }
            }
        }
        z = true;
        z2 = m;
        int i4 = z2 ? 4 : 3;
        if (z2 && eVar.p(jVar)) {
            i2 = 16;
        }
        return u63.c(i4, i2, i, eVar.g ? 64 : 0, z ? 128 : 0);
    }

    @Override // defpackage.u52
    public long n() {
        if (getState() == 2) {
            x1();
        }
        return this.P1;
    }

    @Override // androidx.media3.exoplayer.mediacodec.MediaCodecRenderer
    public float r0(float f, androidx.media3.common.j jVar, androidx.media3.common.j[] jVarArr) {
        int i = -1;
        for (androidx.media3.common.j jVar2 : jVarArr) {
            int i2 = jVar2.D0;
            if (i2 != -1) {
                i = Math.max(i, i2);
            }
        }
        if (i == -1) {
            return -1.0f;
        }
        return f * i;
    }

    @Override // androidx.media3.exoplayer.c, androidx.media3.exoplayer.k.b
    public void s(int i, Object obj) throws ExoPlaybackException {
        if (i == 2) {
            this.L1.p(((Float) obj).floatValue());
        } else if (i == 3) {
            this.L1.h((androidx.media3.common.b) obj);
        } else if (i != 6) {
            switch (i) {
                case 9:
                    this.L1.w(((Boolean) obj).booleanValue());
                    return;
                case 10:
                    this.L1.k(((Integer) obj).intValue());
                    return;
                case 11:
                    this.U1 = (m.a) obj;
                    return;
                default:
                    super.s(i, obj);
                    return;
            }
        } else {
            this.L1.t((ql) obj);
        }
    }

    public final int s1(androidx.media3.exoplayer.mediacodec.e eVar, androidx.media3.common.j jVar) {
        int i;
        if (!"OMX.google.raw.decoder".equals(eVar.a) || (i = androidx.media3.common.util.b.a) >= 24 || (i == 23 && androidx.media3.common.util.b.t0(this.J1))) {
            return jVar.q0;
        }
        return -1;
    }

    @Override // androidx.media3.exoplayer.mediacodec.MediaCodecRenderer
    public List<androidx.media3.exoplayer.mediacodec.e> t0(androidx.media3.exoplayer.mediacodec.f fVar, androidx.media3.common.j jVar, boolean z) throws MediaCodecUtil.DecoderQueryException {
        return MediaCodecUtil.u(u1(fVar, jVar, z, this.L1), jVar);
    }

    public int t1(androidx.media3.exoplayer.mediacodec.e eVar, androidx.media3.common.j jVar, androidx.media3.common.j[] jVarArr) {
        int s1 = s1(eVar, jVar);
        if (jVarArr.length == 1) {
            return s1;
        }
        for (androidx.media3.common.j jVar2 : jVarArr) {
            if (eVar.e(jVar, jVar2).d != 0) {
                s1 = Math.max(s1, s1(eVar, jVar2));
            }
        }
        return s1;
    }

    @Override // androidx.media3.exoplayer.mediacodec.MediaCodecRenderer
    public d.a v0(androidx.media3.exoplayer.mediacodec.e eVar, androidx.media3.common.j jVar, MediaCrypto mediaCrypto, float f) {
        this.M1 = t1(eVar, jVar, E());
        this.N1 = q1(eVar.a);
        MediaFormat v1 = v1(jVar, eVar.c, this.M1, f);
        this.O1 = "audio/raw".equals(eVar.b) && !"audio/raw".equals(jVar.p0) ? jVar : null;
        return d.a.a(eVar, v1, jVar, mediaCrypto);
    }

    @SuppressLint({"InlinedApi"})
    public MediaFormat v1(androidx.media3.common.j jVar, String str, int i, float f) {
        MediaFormat mediaFormat = new MediaFormat();
        mediaFormat.setString("mime", str);
        mediaFormat.setInteger("channel-count", jVar.C0);
        mediaFormat.setInteger("sample-rate", jVar.D0);
        a62.e(mediaFormat, jVar.r0);
        a62.d(mediaFormat, "max-input-size", i);
        int i2 = androidx.media3.common.util.b.a;
        if (i2 >= 23) {
            mediaFormat.setInteger("priority", 0);
            if (f != -1.0f && !r1()) {
                mediaFormat.setFloat("operating-rate", f);
            }
        }
        if (i2 <= 28 && "audio/ac4".equals(jVar.p0)) {
            mediaFormat.setInteger("ac4-is-sync", 1);
        }
        if (i2 >= 24 && this.L1.s(androidx.media3.common.util.b.Z(4, jVar.C0, jVar.D0)) == 2) {
            mediaFormat.setInteger("pcm-encoding", 4);
        }
        if (i2 >= 32) {
            mediaFormat.setInteger("max-output-channel-count", 99);
        }
        return mediaFormat;
    }

    public void w1() {
        this.R1 = true;
    }

    @Override // androidx.media3.exoplayer.c, androidx.media3.exoplayer.m
    public u52 x() {
        return this;
    }

    public final void x1() {
        long m = this.L1.m(d());
        if (m != Long.MIN_VALUE) {
            if (!this.R1) {
                m = Math.max(this.P1, m);
            }
            this.P1 = m;
            this.R1 = false;
        }
    }
}
