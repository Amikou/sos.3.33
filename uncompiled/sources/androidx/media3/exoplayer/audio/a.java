package androidx.media3.exoplayer.audio;

import android.os.Handler;
import androidx.media3.exoplayer.audio.a;

/* compiled from: AudioRendererEventListener.java */
/* loaded from: classes.dex */
public interface a {

    /* compiled from: AudioRendererEventListener.java */
    /* renamed from: androidx.media3.exoplayer.audio.a$a  reason: collision with other inner class name */
    /* loaded from: classes.dex */
    public static final class C0031a {
        public final Handler a;
        public final a b;

        public C0031a(Handler handler, a aVar) {
            this.a = aVar != null ? (Handler) ii.e(handler) : null;
            this.b = aVar;
        }

        /* JADX INFO: Access modifiers changed from: private */
        public /* synthetic */ void A(int i, long j, long j2) {
            ((a) androidx.media3.common.util.b.j(this.b)).z(i, j, j2);
        }

        /* JADX INFO: Access modifiers changed from: private */
        public /* synthetic */ void r(Exception exc) {
            ((a) androidx.media3.common.util.b.j(this.b)).w(exc);
        }

        /* JADX INFO: Access modifiers changed from: private */
        public /* synthetic */ void s(Exception exc) {
            ((a) androidx.media3.common.util.b.j(this.b)).c(exc);
        }

        /* JADX INFO: Access modifiers changed from: private */
        public /* synthetic */ void t(String str, long j, long j2) {
            ((a) androidx.media3.common.util.b.j(this.b)).m(str, j, j2);
        }

        /* JADX INFO: Access modifiers changed from: private */
        public /* synthetic */ void u(String str) {
            ((a) androidx.media3.common.util.b.j(this.b)).l(str);
        }

        /* JADX INFO: Access modifiers changed from: private */
        public /* synthetic */ void v(hf0 hf0Var) {
            hf0Var.c();
            ((a) androidx.media3.common.util.b.j(this.b)).j(hf0Var);
        }

        /* JADX INFO: Access modifiers changed from: private */
        public /* synthetic */ void w(hf0 hf0Var) {
            ((a) androidx.media3.common.util.b.j(this.b)).B(hf0Var);
        }

        /* JADX INFO: Access modifiers changed from: private */
        public /* synthetic */ void x(androidx.media3.common.j jVar, jf0 jf0Var) {
            ((a) androidx.media3.common.util.b.j(this.b)).a(jVar);
            ((a) androidx.media3.common.util.b.j(this.b)).q(jVar, jf0Var);
        }

        /* JADX INFO: Access modifiers changed from: private */
        public /* synthetic */ void y(long j) {
            ((a) androidx.media3.common.util.b.j(this.b)).v(j);
        }

        /* JADX INFO: Access modifiers changed from: private */
        public /* synthetic */ void z(boolean z) {
            ((a) androidx.media3.common.util.b.j(this.b)).b(z);
        }

        public void B(final long j) {
            Handler handler = this.a;
            if (handler != null) {
                handler.post(new Runnable() { // from class: jj
                    @Override // java.lang.Runnable
                    public final void run() {
                        a.C0031a.this.y(j);
                    }
                });
            }
        }

        public void C(final boolean z) {
            Handler handler = this.a;
            if (handler != null) {
                handler.post(new Runnable() { // from class: rj
                    @Override // java.lang.Runnable
                    public final void run() {
                        a.C0031a.this.z(z);
                    }
                });
            }
        }

        public void D(final int i, final long j, final long j2) {
            Handler handler = this.a;
            if (handler != null) {
                handler.post(new Runnable() { // from class: ij
                    @Override // java.lang.Runnable
                    public final void run() {
                        a.C0031a.this.A(i, j, j2);
                    }
                });
            }
        }

        public void k(final Exception exc) {
            Handler handler = this.a;
            if (handler != null) {
                handler.post(new Runnable() { // from class: oj
                    @Override // java.lang.Runnable
                    public final void run() {
                        a.C0031a.this.r(exc);
                    }
                });
            }
        }

        public void l(final Exception exc) {
            Handler handler = this.a;
            if (handler != null) {
                handler.post(new Runnable() { // from class: nj
                    @Override // java.lang.Runnable
                    public final void run() {
                        a.C0031a.this.s(exc);
                    }
                });
            }
        }

        public void m(final String str, final long j, final long j2) {
            Handler handler = this.a;
            if (handler != null) {
                handler.post(new Runnable() { // from class: qj
                    @Override // java.lang.Runnable
                    public final void run() {
                        a.C0031a.this.t(str, j, j2);
                    }
                });
            }
        }

        public void n(final String str) {
            Handler handler = this.a;
            if (handler != null) {
                handler.post(new Runnable() { // from class: pj
                    @Override // java.lang.Runnable
                    public final void run() {
                        a.C0031a.this.u(str);
                    }
                });
            }
        }

        public void o(final hf0 hf0Var) {
            hf0Var.c();
            Handler handler = this.a;
            if (handler != null) {
                handler.post(new Runnable() { // from class: kj
                    @Override // java.lang.Runnable
                    public final void run() {
                        a.C0031a.this.v(hf0Var);
                    }
                });
            }
        }

        public void p(final hf0 hf0Var) {
            Handler handler = this.a;
            if (handler != null) {
                handler.post(new Runnable() { // from class: lj
                    @Override // java.lang.Runnable
                    public final void run() {
                        a.C0031a.this.w(hf0Var);
                    }
                });
            }
        }

        public void q(final androidx.media3.common.j jVar, final jf0 jf0Var) {
            Handler handler = this.a;
            if (handler != null) {
                handler.post(new Runnable() { // from class: mj
                    @Override // java.lang.Runnable
                    public final void run() {
                        a.C0031a.this.x(jVar, jf0Var);
                    }
                });
            }
        }
    }

    void B(hf0 hf0Var);

    @Deprecated
    void a(androidx.media3.common.j jVar);

    void b(boolean z);

    void c(Exception exc);

    void j(hf0 hf0Var);

    void l(String str);

    void m(String str, long j, long j2);

    void q(androidx.media3.common.j jVar, jf0 jf0Var);

    void v(long j);

    void w(Exception exc);

    void z(int i, long j, long j2);
}
