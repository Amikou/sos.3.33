package androidx.media3.exoplayer.audio;

import android.media.AudioTrack;
import android.os.SystemClock;
import java.lang.reflect.Method;
import zendesk.support.request.CellBase;

/* compiled from: AudioTrackPositionTracker.java */
/* loaded from: classes.dex */
public final class b {
    public long A;
    public long B;
    public long C;
    public boolean D;
    public long E;
    public long F;
    public final a a;
    public final long[] b;
    public AudioTrack c;
    public int d;
    public int e;
    public sj f;
    public int g;
    public boolean h;
    public long i;
    public float j;
    public boolean k;
    public long l;
    public long m;
    public Method n;
    public long o;
    public boolean p;
    public boolean q;
    public long r;
    public long s;
    public long t;
    public long u;
    public int v;
    public int w;
    public long x;
    public long y;
    public long z;

    /* compiled from: AudioTrackPositionTracker.java */
    /* loaded from: classes.dex */
    public interface a {
        void a(long j);

        void b(int i, long j);

        void c(long j);

        void d(long j, long j2, long j3, long j4);

        void e(long j, long j2, long j3, long j4);
    }

    public b(a aVar) {
        this.a = (a) ii.e(aVar);
        if (androidx.media3.common.util.b.a >= 18) {
            try {
                this.n = AudioTrack.class.getMethod("getLatency", null);
            } catch (NoSuchMethodException unused) {
            }
        }
        this.b = new long[10];
    }

    public static boolean o(int i) {
        return androidx.media3.common.util.b.a < 23 && (i == 5 || i == 6);
    }

    public final boolean a() {
        return this.h && ((AudioTrack) ii.e(this.c)).getPlayState() == 2 && e() == 0;
    }

    public final long b(long j) {
        return (j * 1000000) / this.g;
    }

    public int c(long j) {
        return this.e - ((int) (j - (e() * this.d)));
    }

    public long d(boolean z) {
        long j;
        if (((AudioTrack) ii.e(this.c)).getPlayState() == 3) {
            m();
        }
        long nanoTime = System.nanoTime() / 1000;
        sj sjVar = (sj) ii.e(this.f);
        boolean d = sjVar.d();
        if (d) {
            j = b(sjVar.b()) + androidx.media3.common.util.b.W(nanoTime - sjVar.c(), this.j);
        } else {
            if (this.w == 0) {
                j = f();
            } else {
                j = this.l + nanoTime;
            }
            if (!z) {
                j = Math.max(0L, j - this.o);
            }
        }
        if (this.D != d) {
            this.F = this.C;
            this.E = this.B;
        }
        long j2 = nanoTime - this.F;
        if (j2 < 1000000) {
            long j3 = (j2 * 1000) / 1000000;
            j = ((j * j3) + ((1000 - j3) * (this.E + androidx.media3.common.util.b.W(j2, this.j)))) / 1000;
        }
        if (!this.k) {
            long j4 = this.B;
            if (j > j4) {
                this.k = true;
                this.a.a(System.currentTimeMillis() - androidx.media3.common.util.b.U0(androidx.media3.common.util.b.b0(androidx.media3.common.util.b.U0(j - j4), this.j)));
            }
        }
        this.C = nanoTime;
        this.B = j;
        this.D = d;
        return j;
    }

    public final long e() {
        AudioTrack audioTrack = (AudioTrack) ii.e(this.c);
        if (this.x != CellBase.ID_SYSTEM_MESSAGE_REQUEST_CLOSED) {
            return Math.min(this.A, this.z + ((((SystemClock.elapsedRealtime() * 1000) - this.x) * this.g) / 1000000));
        }
        int playState = audioTrack.getPlayState();
        if (playState == 1) {
            return 0L;
        }
        long playbackHeadPosition = 4294967295L & audioTrack.getPlaybackHeadPosition();
        if (this.h) {
            if (playState == 2 && playbackHeadPosition == 0) {
                this.u = this.s;
            }
            playbackHeadPosition += this.u;
        }
        if (androidx.media3.common.util.b.a <= 29) {
            if (playbackHeadPosition == 0 && this.s > 0 && playState == 3) {
                if (this.y == CellBase.ID_SYSTEM_MESSAGE_REQUEST_CLOSED) {
                    this.y = SystemClock.elapsedRealtime();
                }
                return this.s;
            }
            this.y = CellBase.ID_SYSTEM_MESSAGE_REQUEST_CLOSED;
        }
        if (this.s > playbackHeadPosition) {
            this.t++;
        }
        this.s = playbackHeadPosition;
        return playbackHeadPosition + (this.t << 32);
    }

    public final long f() {
        return b(e());
    }

    public void g(long j) {
        this.z = e();
        this.x = SystemClock.elapsedRealtime() * 1000;
        this.A = j;
    }

    public boolean h(long j) {
        return j > e() || a();
    }

    public boolean i() {
        return ((AudioTrack) ii.e(this.c)).getPlayState() == 3;
    }

    public boolean j(long j) {
        return this.y != CellBase.ID_SYSTEM_MESSAGE_REQUEST_CLOSED && j > 0 && SystemClock.elapsedRealtime() - this.y >= 200;
    }

    public boolean k(long j) {
        int playState = ((AudioTrack) ii.e(this.c)).getPlayState();
        if (this.h) {
            if (playState == 2) {
                this.p = false;
                return false;
            } else if (playState == 1 && e() == 0) {
                return false;
            }
        }
        boolean z = this.p;
        boolean h = h(j);
        this.p = h;
        if (z && !h && playState != 1) {
            this.a.b(this.e, androidx.media3.common.util.b.U0(this.i));
        }
        return true;
    }

    public final void l(long j, long j2) {
        sj sjVar = (sj) ii.e(this.f);
        if (sjVar.e(j)) {
            long c = sjVar.c();
            long b = sjVar.b();
            if (Math.abs(c - j) > 5000000) {
                this.a.e(b, c, j, j2);
                sjVar.f();
            } else if (Math.abs(b(b) - j2) > 5000000) {
                this.a.d(b, c, j, j2);
                sjVar.f();
            } else {
                sjVar.a();
            }
        }
    }

    public final void m() {
        long f = f();
        if (f == 0) {
            return;
        }
        long nanoTime = System.nanoTime() / 1000;
        if (nanoTime - this.m >= 30000) {
            long[] jArr = this.b;
            int i = this.v;
            jArr[i] = f - nanoTime;
            this.v = (i + 1) % 10;
            int i2 = this.w;
            if (i2 < 10) {
                this.w = i2 + 1;
            }
            this.m = nanoTime;
            this.l = 0L;
            int i3 = 0;
            while (true) {
                int i4 = this.w;
                if (i3 >= i4) {
                    break;
                }
                this.l += this.b[i3] / i4;
                i3++;
            }
        }
        if (this.h) {
            return;
        }
        l(nanoTime, f);
        n(nanoTime);
    }

    public final void n(long j) {
        Method method;
        if (!this.q || (method = this.n) == null || j - this.r < 500000) {
            return;
        }
        try {
            long intValue = (((Integer) androidx.media3.common.util.b.j((Integer) method.invoke(ii.e(this.c), new Object[0]))).intValue() * 1000) - this.i;
            this.o = intValue;
            long max = Math.max(intValue, 0L);
            this.o = max;
            if (max > 5000000) {
                this.a.c(max);
                this.o = 0L;
            }
        } catch (Exception unused) {
            this.n = null;
        }
        this.r = j;
    }

    public boolean p() {
        r();
        if (this.x == CellBase.ID_SYSTEM_MESSAGE_REQUEST_CLOSED) {
            ((sj) ii.e(this.f)).g();
            return true;
        }
        return false;
    }

    public void q() {
        r();
        this.c = null;
        this.f = null;
    }

    public final void r() {
        this.l = 0L;
        this.w = 0;
        this.v = 0;
        this.m = 0L;
        this.C = 0L;
        this.F = 0L;
        this.k = false;
    }

    public void s(AudioTrack audioTrack, boolean z, int i, int i2, int i3) {
        this.c = audioTrack;
        this.d = i2;
        this.e = i3;
        this.f = new sj(audioTrack);
        this.g = audioTrack.getSampleRate();
        this.h = z && o(i);
        boolean q0 = androidx.media3.common.util.b.q0(i);
        this.q = q0;
        this.i = q0 ? b(i3 / i2) : -9223372036854775807L;
        this.s = 0L;
        this.t = 0L;
        this.u = 0L;
        this.p = false;
        this.x = CellBase.ID_SYSTEM_MESSAGE_REQUEST_CLOSED;
        this.y = CellBase.ID_SYSTEM_MESSAGE_REQUEST_CLOSED;
        this.r = 0L;
        this.o = 0L;
        this.j = 1.0f;
    }

    public void t(float f) {
        this.j = f;
        sj sjVar = this.f;
        if (sjVar != null) {
            sjVar.g();
        }
    }

    public void u() {
        ((sj) ii.e(this.f)).g();
    }
}
