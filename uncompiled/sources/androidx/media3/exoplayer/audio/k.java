package androidx.media3.exoplayer.audio;

import androidx.media3.exoplayer.audio.AudioProcessor;
import java.nio.ByteBuffer;

/* compiled from: TrimmingAudioProcessor.java */
/* loaded from: classes.dex */
public final class k extends c {
    public int i;
    public int j;
    public boolean k;
    public int l;
    public byte[] m = androidx.media3.common.util.b.f;
    public int n;
    public long o;

    @Override // androidx.media3.exoplayer.audio.AudioProcessor
    public void c(ByteBuffer byteBuffer) {
        int position = byteBuffer.position();
        int limit = byteBuffer.limit();
        int i = limit - position;
        if (i == 0) {
            return;
        }
        int min = Math.min(i, this.l);
        this.o += min / this.b.d;
        this.l -= min;
        byteBuffer.position(position + min);
        if (this.l > 0) {
            return;
        }
        int i2 = i - min;
        int length = (this.n + i2) - this.m.length;
        ByteBuffer k = k(length);
        int q = androidx.media3.common.util.b.q(length, 0, this.n);
        k.put(this.m, 0, q);
        int q2 = androidx.media3.common.util.b.q(length - q, 0, i2);
        byteBuffer.limit(byteBuffer.position() + q2);
        k.put(byteBuffer);
        byteBuffer.limit(limit);
        int i3 = i2 - q2;
        int i4 = this.n - q;
        this.n = i4;
        byte[] bArr = this.m;
        System.arraycopy(bArr, q, bArr, 0, i4);
        byteBuffer.get(this.m, this.n, i3);
        this.n += i3;
        k.flip();
    }

    @Override // androidx.media3.exoplayer.audio.c, androidx.media3.exoplayer.audio.AudioProcessor
    public boolean d() {
        return super.d() && this.n == 0;
    }

    @Override // androidx.media3.exoplayer.audio.c
    public AudioProcessor.a g(AudioProcessor.a aVar) throws AudioProcessor.UnhandledAudioFormatException {
        if (aVar.c == 2) {
            this.k = true;
            return (this.i == 0 && this.j == 0) ? AudioProcessor.a.e : aVar;
        }
        throw new AudioProcessor.UnhandledAudioFormatException(aVar);
    }

    @Override // androidx.media3.exoplayer.audio.c, androidx.media3.exoplayer.audio.AudioProcessor
    public ByteBuffer getOutput() {
        int i;
        if (super.d() && (i = this.n) > 0) {
            k(i).put(this.m, 0, this.n).flip();
            this.n = 0;
        }
        return super.getOutput();
    }

    @Override // androidx.media3.exoplayer.audio.c
    public void h() {
        if (this.k) {
            this.k = false;
            int i = this.j;
            int i2 = this.b.d;
            this.m = new byte[i * i2];
            this.l = this.i * i2;
        }
        this.n = 0;
    }

    @Override // androidx.media3.exoplayer.audio.c
    public void i() {
        int i;
        if (this.k) {
            if (this.n > 0) {
                this.o += i / this.b.d;
            }
            this.n = 0;
        }
    }

    @Override // androidx.media3.exoplayer.audio.c
    public void j() {
        this.m = androidx.media3.common.util.b.f;
    }

    public long l() {
        return this.o;
    }

    public void m() {
        this.o = 0L;
    }

    public void n(int i, int i2) {
        this.i = i;
        this.j = i2;
    }
}
