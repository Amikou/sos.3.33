package androidx.media3.exoplayer.audio;

import androidx.media3.exoplayer.audio.AudioProcessor;
import java.nio.ByteBuffer;

/* compiled from: ChannelMappingAudioProcessor.java */
/* loaded from: classes.dex */
public final class d extends c {
    public int[] i;
    public int[] j;

    @Override // androidx.media3.exoplayer.audio.AudioProcessor
    public void c(ByteBuffer byteBuffer) {
        int[] iArr = (int[]) ii.e(this.j);
        int position = byteBuffer.position();
        int limit = byteBuffer.limit();
        ByteBuffer k = k(((limit - position) / this.b.d) * this.c.d);
        while (position < limit) {
            for (int i : iArr) {
                k.putShort(byteBuffer.getShort((i * 2) + position));
            }
            position += this.b.d;
        }
        byteBuffer.position(limit);
        k.flip();
    }

    @Override // androidx.media3.exoplayer.audio.c
    public AudioProcessor.a g(AudioProcessor.a aVar) throws AudioProcessor.UnhandledAudioFormatException {
        int[] iArr = this.i;
        if (iArr == null) {
            return AudioProcessor.a.e;
        }
        if (aVar.c == 2) {
            boolean z = aVar.b != iArr.length;
            int i = 0;
            while (i < iArr.length) {
                int i2 = iArr[i];
                if (i2 >= aVar.b) {
                    throw new AudioProcessor.UnhandledAudioFormatException(aVar);
                }
                z |= i2 != i;
                i++;
            }
            if (z) {
                return new AudioProcessor.a(aVar.a, iArr.length, 2);
            }
            return AudioProcessor.a.e;
        }
        throw new AudioProcessor.UnhandledAudioFormatException(aVar);
    }

    @Override // androidx.media3.exoplayer.audio.c
    public void h() {
        this.j = this.i;
    }

    @Override // androidx.media3.exoplayer.audio.c
    public void j() {
        this.j = null;
        this.i = null;
    }

    public void l(int[] iArr) {
        this.i = iArr;
    }
}
