package androidx.media3.exoplayer.video;

import android.content.Context;
import android.opengl.GLES20;
import android.opengl.GLSurfaceView;
import android.util.AttributeSet;
import androidx.media3.common.util.GlUtil;
import com.github.mikephil.charting.utils.Utils;
import java.nio.Buffer;
import java.nio.ByteBuffer;
import java.nio.FloatBuffer;
import java.util.concurrent.atomic.AtomicReference;
import javax.microedition.khronos.egl.EGLConfig;
import javax.microedition.khronos.opengles.GL10;
import okhttp3.internal.http2.Http2;

/* loaded from: classes.dex */
public final class VideoDecoderGLSurfaceView extends GLSurfaceView implements kh4 {
    public final a a;

    /* loaded from: classes.dex */
    public static final class a implements GLSurfaceView.Renderer {
        public static final float[] o0 = {1.164f, 1.164f, 1.164f, Utils.FLOAT_EPSILON, -0.392f, 2.017f, 1.596f, -0.813f, Utils.FLOAT_EPSILON};
        public static final float[] p0 = {1.164f, 1.164f, 1.164f, Utils.FLOAT_EPSILON, -0.213f, 2.112f, 1.793f, -0.533f, Utils.FLOAT_EPSILON};
        public static final float[] q0 = {1.168f, 1.168f, 1.168f, Utils.FLOAT_EPSILON, -0.188f, 2.148f, 1.683f, -0.652f, Utils.FLOAT_EPSILON};
        public static final String[] r0 = {"y_tex", "u_tex", "v_tex"};
        public static final FloatBuffer s0 = GlUtil.e(new float[]{-1.0f, 1.0f, -1.0f, -1.0f, 1.0f, 1.0f, 1.0f, -1.0f});
        public final GLSurfaceView a;
        public final int[] f0 = new int[3];
        public final int[] g0 = new int[3];
        public final int[] h0 = new int[3];
        public final int[] i0 = new int[3];
        public final AtomicReference<jh4> j0 = new AtomicReference<>();
        public final FloatBuffer[] k0 = new FloatBuffer[3];
        public hg1 l0;
        public int m0;
        public jh4 n0;

        public a(GLSurfaceView gLSurfaceView) {
            this.a = gLSurfaceView;
            for (int i = 0; i < 3; i++) {
                int[] iArr = this.h0;
                this.i0[i] = -1;
                iArr[i] = -1;
            }
        }

        public void a(jh4 jh4Var) {
            jh4 andSet = this.j0.getAndSet(jh4Var);
            if (andSet != null) {
                andSet.u();
            }
            this.a.requestRender();
        }

        public final void b() {
            GLES20.glGenTextures(3, this.f0, 0);
            for (int i = 0; i < 3; i++) {
                GLES20.glUniform1i(this.l0.j(r0[i]), i);
                GLES20.glActiveTexture(33984 + i);
                GlUtil.a(3553, this.f0[i]);
            }
            GlUtil.c();
        }

        @Override // android.opengl.GLSurfaceView.Renderer
        public void onDrawFrame(GL10 gl10) {
            jh4 andSet = this.j0.getAndSet(null);
            if (andSet == null && this.n0 == null) {
                return;
            }
            if (andSet != null) {
                jh4 jh4Var = this.n0;
                if (jh4Var != null) {
                    jh4Var.u();
                }
                this.n0 = andSet;
            }
            jh4 jh4Var2 = (jh4) ii.e(this.n0);
            float[] fArr = p0;
            int i = jh4Var2.k0;
            if (i == 1) {
                fArr = o0;
            } else if (i == 3) {
                fArr = q0;
            }
            GLES20.glUniformMatrix3fv(this.m0, 1, false, fArr, 0);
            int[] iArr = (int[]) ii.e(jh4Var2.j0);
            ByteBuffer[] byteBufferArr = (ByteBuffer[]) ii.e(jh4Var2.i0);
            int i2 = 0;
            while (i2 < 3) {
                int i3 = i2 == 0 ? jh4Var2.h0 : (jh4Var2.h0 + 1) / 2;
                GLES20.glActiveTexture(33984 + i2);
                GLES20.glBindTexture(3553, this.f0[i2]);
                GLES20.glPixelStorei(3317, 1);
                GLES20.glTexImage2D(3553, 0, 6409, iArr[i2], i3, 0, 6409, 5121, byteBufferArr[i2]);
                i2++;
            }
            int i4 = (r3[0] + 1) / 2;
            int[] iArr2 = {jh4Var2.g0, i4, i4};
            for (int i5 = 0; i5 < 3; i5++) {
                if (this.h0[i5] != iArr2[i5] || this.i0[i5] != iArr[i5]) {
                    ii.g(iArr[i5] != 0);
                    float f = iArr2[i5] / iArr[i5];
                    this.k0[i5] = GlUtil.e(new float[]{Utils.FLOAT_EPSILON, Utils.FLOAT_EPSILON, Utils.FLOAT_EPSILON, 1.0f, f, Utils.FLOAT_EPSILON, f, 1.0f});
                    GLES20.glVertexAttribPointer(this.g0[i5], 2, 5126, false, 0, (Buffer) this.k0[i5]);
                    this.h0[i5] = iArr2[i5];
                    this.i0[i5] = iArr[i5];
                }
            }
            GLES20.glClear(Http2.INITIAL_MAX_FRAME_SIZE);
            GLES20.glDrawArrays(5, 0, 4);
            GlUtil.c();
        }

        @Override // android.opengl.GLSurfaceView.Renderer
        public void onSurfaceChanged(GL10 gl10, int i, int i2) {
            GLES20.glViewport(0, 0, i, i2);
        }

        @Override // android.opengl.GLSurfaceView.Renderer
        public void onSurfaceCreated(GL10 gl10, EGLConfig eGLConfig) {
            hg1 hg1Var = new hg1("varying vec2 interp_tc_y;\nvarying vec2 interp_tc_u;\nvarying vec2 interp_tc_v;\nattribute vec4 in_pos;\nattribute vec2 in_tc_y;\nattribute vec2 in_tc_u;\nattribute vec2 in_tc_v;\nvoid main() {\n  gl_Position = in_pos;\n  interp_tc_y = in_tc_y;\n  interp_tc_u = in_tc_u;\n  interp_tc_v = in_tc_v;\n}\n", "precision mediump float;\nvarying vec2 interp_tc_y;\nvarying vec2 interp_tc_u;\nvarying vec2 interp_tc_v;\nuniform sampler2D y_tex;\nuniform sampler2D u_tex;\nuniform sampler2D v_tex;\nuniform mat3 mColorConversion;\nvoid main() {\n  vec3 yuv;\n  yuv.x = texture2D(y_tex, interp_tc_y).r - 0.0625;\n  yuv.y = texture2D(u_tex, interp_tc_u).r - 0.5;\n  yuv.z = texture2D(v_tex, interp_tc_v).r - 0.5;\n  gl_FragColor = vec4(mColorConversion * yuv, 1.0);\n}\n");
            this.l0 = hg1Var;
            GLES20.glVertexAttribPointer(hg1Var.e("in_pos"), 2, 5126, false, 0, (Buffer) s0);
            this.g0[0] = this.l0.e("in_tc_y");
            this.g0[1] = this.l0.e("in_tc_u");
            this.g0[2] = this.l0.e("in_tc_v");
            this.m0 = this.l0.j("mColorConversion");
            GlUtil.c();
            b();
            GlUtil.c();
        }
    }

    public VideoDecoderGLSurfaceView(Context context) {
        this(context, null);
    }

    @Deprecated
    public kh4 getVideoDecoderOutputBufferRenderer() {
        return this;
    }

    public void setOutputBuffer(jh4 jh4Var) {
        this.a.a(jh4Var);
    }

    public VideoDecoderGLSurfaceView(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        a aVar = new a(this);
        this.a = aVar;
        setPreserveEGLContextOnPause(true);
        setEGLContextClientVersion(2);
        setRenderer(aVar);
        setRenderMode(0);
    }
}
