package androidx.media3.exoplayer.video;

import android.annotation.SuppressLint;
import android.annotation.TargetApi;
import android.content.Context;
import android.graphics.Point;
import android.media.MediaCrypto;
import android.media.MediaFormat;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.os.SystemClock;
import android.util.Pair;
import android.view.Surface;
import androidx.media3.common.j;
import androidx.media3.common.z;
import androidx.media3.decoder.DecoderInputBuffer;
import androidx.media3.exoplayer.ExoPlaybackException;
import androidx.media3.exoplayer.mediacodec.MediaCodecDecoderException;
import androidx.media3.exoplayer.mediacodec.MediaCodecRenderer;
import androidx.media3.exoplayer.mediacodec.MediaCodecUtil;
import androidx.media3.exoplayer.mediacodec.d;
import androidx.media3.exoplayer.mediacodec.e;
import androidx.media3.exoplayer.mediacodec.f;
import androidx.media3.exoplayer.video.b;
import com.google.common.collect.ImmutableList;
import java.nio.ByteBuffer;
import java.util.Collection;
import java.util.List;
import zendesk.support.request.CellBase;

/* compiled from: MediaCodecVideoRenderer.java */
/* loaded from: classes.dex */
public class a extends MediaCodecRenderer {
    public static final int[] s2 = {1920, 1600, 1440, 1280, 960, 854, 640, 540, 480};
    public static boolean t2;
    public static boolean u2;
    public final Context J1;
    public final nh4 K1;
    public final b.a L1;
    public final long M1;
    public final int N1;
    public final boolean O1;
    public C0043a P1;
    public boolean Q1;
    public boolean R1;
    public Surface S1;
    public PlaceholderSurface T1;
    public boolean U1;
    public int V1;
    public boolean W1;
    public boolean X1;
    public boolean Y1;
    public long Z1;
    public long a2;
    public long b2;
    public int c2;
    public int d2;
    public int e2;
    public long f2;
    public long g2;
    public long h2;
    public int i2;
    public int j2;
    public int k2;
    public int l2;
    public float m2;
    public z n2;
    public boolean o2;
    public int p2;
    public b q2;
    public lh4 r2;

    /* compiled from: MediaCodecVideoRenderer.java */
    /* renamed from: androidx.media3.exoplayer.video.a$a  reason: collision with other inner class name */
    /* loaded from: classes.dex */
    public static final class C0043a {
        public final int a;
        public final int b;
        public final int c;

        public C0043a(int i, int i2, int i3) {
            this.a = i;
            this.b = i2;
            this.c = i3;
        }
    }

    /* compiled from: MediaCodecVideoRenderer.java */
    /* loaded from: classes.dex */
    public final class b implements d.c, Handler.Callback {
        public final Handler a;

        public b(d dVar) {
            Handler w = androidx.media3.common.util.b.w(this);
            this.a = w;
            dVar.c(this, w);
        }

        @Override // androidx.media3.exoplayer.mediacodec.d.c
        public void a(d dVar, long j, long j2) {
            if (androidx.media3.common.util.b.a < 30) {
                this.a.sendMessageAtFrontOfQueue(Message.obtain(this.a, 0, (int) (j >> 32), (int) j));
                return;
            }
            b(j);
        }

        public final void b(long j) {
            a aVar = a.this;
            if (this != aVar.q2) {
                return;
            }
            if (j == Long.MAX_VALUE) {
                aVar.O1();
                return;
            }
            try {
                aVar.N1(j);
            } catch (ExoPlaybackException e) {
                a.this.d1(e);
            }
        }

        @Override // android.os.Handler.Callback
        public boolean handleMessage(Message message) {
            if (message.what != 0) {
                return false;
            }
            b(androidx.media3.common.util.b.R0(message.arg1, message.arg2));
            return true;
        }
    }

    public a(Context context, d.b bVar, f fVar, long j, boolean z, Handler handler, androidx.media3.exoplayer.video.b bVar2, int i) {
        this(context, bVar, fVar, j, z, handler, bVar2, i, 30.0f);
    }

    public static List<e> A1(f fVar, j jVar, boolean z, boolean z2) throws MediaCodecUtil.DecoderQueryException {
        String str = jVar.p0;
        if (str == null) {
            return ImmutableList.of();
        }
        List<e> a = fVar.a(str, z, z2);
        String m = MediaCodecUtil.m(jVar);
        if (m == null) {
            return ImmutableList.copyOf((Collection) a);
        }
        return ImmutableList.builder().j(a).j(fVar.a(m, z, z2)).l();
    }

    public static int B1(e eVar, j jVar) {
        if (jVar.q0 != -1) {
            int size = jVar.r0.size();
            int i = 0;
            for (int i2 = 0; i2 < size; i2++) {
                i += jVar.r0.get(i2).length;
            }
            return jVar.q0 + i;
        }
        return x1(eVar, jVar);
    }

    public static boolean D1(long j) {
        return j < -30000;
    }

    public static boolean E1(long j) {
        return j < -500000;
    }

    public static void S1(d dVar, byte[] bArr) {
        Bundle bundle = new Bundle();
        bundle.putByteArray("hdr10-plus-info", bArr);
        dVar.f(bundle);
    }

    public static void t1(MediaFormat mediaFormat, int i) {
        mediaFormat.setFeatureEnabled("tunneled-playback", true);
        mediaFormat.setInteger("audio-session-id", i);
    }

    public static boolean u1() {
        return "NVIDIA".equals(androidx.media3.common.util.b.c);
    }

    /* JADX WARN: Can't fix incorrect switch cases order, some code will duplicate */
    /* JADX WARN: Code restructure failed: missing block: B:581:0x07ce, code lost:
        if (r0.equals("PGN528") == false) goto L42;
     */
    /* JADX WARN: Code restructure failed: missing block: B:616:0x083c, code lost:
        if (r0.equals("AFTN") == false) goto L46;
     */
    /*
        Code decompiled incorrectly, please refer to instructions dump.
        To view partially-correct code enable 'Show inconsistent code' option in preferences
    */
    public static boolean w1() {
        /*
            Method dump skipped, instructions count: 3062
            To view this dump change 'Code comments level' option to 'DEBUG'
        */
        throw new UnsupportedOperationException("Method not decompiled: androidx.media3.exoplayer.video.a.w1():boolean");
    }

    /* JADX WARN: Can't fix incorrect switch cases order, some code will duplicate */
    /* JADX WARN: Code restructure failed: missing block: B:40:0x007b, code lost:
        if (r3.equals("video/av01") == false) goto L16;
     */
    /*
        Code decompiled incorrectly, please refer to instructions dump.
        To view partially-correct code enable 'Show inconsistent code' option in preferences
    */
    public static int x1(androidx.media3.exoplayer.mediacodec.e r10, androidx.media3.common.j r11) {
        /*
            Method dump skipped, instructions count: 256
            To view this dump change 'Code comments level' option to 'DEBUG'
        */
        throw new UnsupportedOperationException("Method not decompiled: androidx.media3.exoplayer.video.a.x1(androidx.media3.exoplayer.mediacodec.e, androidx.media3.common.j):int");
    }

    public static Point y1(e eVar, j jVar) {
        int[] iArr;
        int i = jVar.v0;
        int i2 = jVar.u0;
        boolean z = i > i2;
        int i3 = z ? i : i2;
        if (z) {
            i = i2;
        }
        float f = i / i3;
        for (int i4 : s2) {
            int i5 = (int) (i4 * f);
            if (i4 <= i3 || i5 <= i) {
                break;
            }
            if (androidx.media3.common.util.b.a >= 21) {
                int i6 = z ? i5 : i4;
                if (!z) {
                    i4 = i5;
                }
                Point b2 = eVar.b(i6, i4);
                if (eVar.u(b2.x, b2.y, jVar.w0)) {
                    return b2;
                }
            } else {
                try {
                    int l = androidx.media3.common.util.b.l(i4, 16) * 16;
                    int l2 = androidx.media3.common.util.b.l(i5, 16) * 16;
                    if (l * l2 <= MediaCodecUtil.N()) {
                        int i7 = z ? l2 : l;
                        if (!z) {
                            l = l2;
                        }
                        return new Point(i7, l);
                    }
                } catch (MediaCodecUtil.DecoderQueryException unused) {
                }
            }
        }
        return null;
    }

    @SuppressLint({"InlinedApi"})
    @TargetApi(21)
    public MediaFormat C1(j jVar, String str, C0043a c0043a, float f, boolean z, int i) {
        Pair<Integer, Integer> q;
        MediaFormat mediaFormat = new MediaFormat();
        mediaFormat.setString("mime", str);
        mediaFormat.setInteger("width", jVar.u0);
        mediaFormat.setInteger("height", jVar.v0);
        a62.e(mediaFormat, jVar.r0);
        a62.c(mediaFormat, "frame-rate", jVar.w0);
        a62.d(mediaFormat, "rotation-degrees", jVar.x0);
        a62.b(mediaFormat, jVar.B0);
        if ("video/dolby-vision".equals(jVar.p0) && (q = MediaCodecUtil.q(jVar)) != null) {
            a62.d(mediaFormat, "profile", ((Integer) q.first).intValue());
        }
        mediaFormat.setInteger("max-width", c0043a.a);
        mediaFormat.setInteger("max-height", c0043a.b);
        a62.d(mediaFormat, "max-input-size", c0043a.c);
        if (androidx.media3.common.util.b.a >= 23) {
            mediaFormat.setInteger("priority", 0);
            if (f != -1.0f) {
                mediaFormat.setFloat("operating-rate", f);
            }
        }
        if (z) {
            mediaFormat.setInteger("no-post-process", 1);
            mediaFormat.setInteger("auto-frc", 0);
        }
        if (i != 0) {
            t1(mediaFormat, i);
        }
        return mediaFormat;
    }

    public boolean F1(long j, boolean z) throws ExoPlaybackException {
        int P = P(j);
        if (P == 0) {
            return false;
        }
        if (z) {
            hf0 hf0Var = this.E1;
            hf0Var.d += P;
            hf0Var.f += this.e2;
        } else {
            this.E1.j++;
            b2(P, this.e2);
        }
        l0();
        return true;
    }

    @Override // androidx.media3.exoplayer.mediacodec.MediaCodecRenderer, androidx.media3.exoplayer.c
    public void G() {
        r1();
        q1();
        this.U1 = false;
        this.q2 = null;
        try {
            super.G();
        } finally {
            this.L1.m(this.E1);
        }
    }

    public final void G1() {
        if (this.c2 > 0) {
            long elapsedRealtime = SystemClock.elapsedRealtime();
            this.L1.n(this.c2, elapsedRealtime - this.b2);
            this.c2 = 0;
            this.b2 = elapsedRealtime;
        }
    }

    @Override // androidx.media3.exoplayer.mediacodec.MediaCodecRenderer, androidx.media3.exoplayer.c
    public void H(boolean z, boolean z2) throws ExoPlaybackException {
        super.H(z, z2);
        boolean z3 = A().a;
        ii.g((z3 && this.p2 == 0) ? false : true);
        if (this.o2 != z3) {
            this.o2 = z3;
            V0();
        }
        this.L1.o(this.E1);
        this.X1 = z2;
        this.Y1 = false;
    }

    public void H1() {
        this.Y1 = true;
        if (this.W1) {
            return;
        }
        this.W1 = true;
        this.L1.A(this.S1);
        this.U1 = true;
    }

    @Override // androidx.media3.exoplayer.mediacodec.MediaCodecRenderer, androidx.media3.exoplayer.c
    public void I(long j, boolean z) throws ExoPlaybackException {
        super.I(j, z);
        q1();
        this.K1.j();
        this.f2 = CellBase.ID_SYSTEM_MESSAGE_REQUEST_CLOSED;
        this.Z1 = CellBase.ID_SYSTEM_MESSAGE_REQUEST_CLOSED;
        this.d2 = 0;
        if (z) {
            T1();
        } else {
            this.a2 = CellBase.ID_SYSTEM_MESSAGE_REQUEST_CLOSED;
        }
    }

    @Override // androidx.media3.exoplayer.mediacodec.MediaCodecRenderer
    public void I0(Exception exc) {
        p12.d("MediaCodecVideoRenderer", "Video codec error", exc);
        this.L1.C(exc);
    }

    public final void I1() {
        int i = this.i2;
        if (i != 0) {
            this.L1.B(this.h2, i);
            this.h2 = 0L;
            this.i2 = 0;
        }
    }

    @Override // androidx.media3.exoplayer.mediacodec.MediaCodecRenderer, androidx.media3.exoplayer.c
    @TargetApi(17)
    public void J() {
        try {
            super.J();
        } finally {
            if (this.T1 != null) {
                P1();
            }
        }
    }

    @Override // androidx.media3.exoplayer.mediacodec.MediaCodecRenderer
    public void J0(String str, d.a aVar, long j, long j2) {
        this.L1.k(str, j, j2);
        this.Q1 = s1(str);
        this.R1 = ((e) ii.e(p0())).n();
        if (androidx.media3.common.util.b.a < 23 || !this.o2) {
            return;
        }
        this.q2 = new b((d) ii.e(o0()));
    }

    public final void J1() {
        int i = this.j2;
        if (i == -1 && this.k2 == -1) {
            return;
        }
        z zVar = this.n2;
        if (zVar != null && zVar.a == i && zVar.f0 == this.k2 && zVar.g0 == this.l2 && zVar.h0 == this.m2) {
            return;
        }
        z zVar2 = new z(this.j2, this.k2, this.l2, this.m2);
        this.n2 = zVar2;
        this.L1.D(zVar2);
    }

    @Override // androidx.media3.exoplayer.mediacodec.MediaCodecRenderer, androidx.media3.exoplayer.c
    public void K() {
        super.K();
        this.c2 = 0;
        this.b2 = SystemClock.elapsedRealtime();
        this.g2 = SystemClock.elapsedRealtime() * 1000;
        this.h2 = 0L;
        this.i2 = 0;
        this.K1.k();
    }

    @Override // androidx.media3.exoplayer.mediacodec.MediaCodecRenderer
    public void K0(String str) {
        this.L1.l(str);
    }

    public final void K1() {
        if (this.U1) {
            this.L1.A(this.S1);
        }
    }

    @Override // androidx.media3.exoplayer.mediacodec.MediaCodecRenderer, androidx.media3.exoplayer.c
    public void L() {
        this.a2 = CellBase.ID_SYSTEM_MESSAGE_REQUEST_CLOSED;
        G1();
        I1();
        this.K1.l();
        super.L();
    }

    @Override // androidx.media3.exoplayer.mediacodec.MediaCodecRenderer
    public jf0 L0(y81 y81Var) throws ExoPlaybackException {
        jf0 L0 = super.L0(y81Var);
        this.L1.p(y81Var.b, L0);
        return L0;
    }

    public final void L1() {
        z zVar = this.n2;
        if (zVar != null) {
            this.L1.D(zVar);
        }
    }

    @Override // androidx.media3.exoplayer.mediacodec.MediaCodecRenderer
    public void M0(j jVar, MediaFormat mediaFormat) {
        int integer;
        int integer2;
        d o0 = o0();
        if (o0 != null) {
            o0.k(this.V1);
        }
        if (this.o2) {
            this.j2 = jVar.u0;
            this.k2 = jVar.v0;
        } else {
            ii.e(mediaFormat);
            boolean z = mediaFormat.containsKey("crop-right") && mediaFormat.containsKey("crop-left") && mediaFormat.containsKey("crop-bottom") && mediaFormat.containsKey("crop-top");
            if (z) {
                integer = (mediaFormat.getInteger("crop-right") - mediaFormat.getInteger("crop-left")) + 1;
            } else {
                integer = mediaFormat.getInteger("width");
            }
            this.j2 = integer;
            if (z) {
                integer2 = (mediaFormat.getInteger("crop-bottom") - mediaFormat.getInteger("crop-top")) + 1;
            } else {
                integer2 = mediaFormat.getInteger("height");
            }
            this.k2 = integer2;
        }
        float f = jVar.y0;
        this.m2 = f;
        if (androidx.media3.common.util.b.a >= 21) {
            int i = jVar.x0;
            if (i == 90 || i == 270) {
                int i2 = this.j2;
                this.j2 = this.k2;
                this.k2 = i2;
                this.m2 = 1.0f / f;
            }
        } else {
            this.l2 = jVar.x0;
        }
        this.K1.g(jVar.w0);
    }

    public final void M1(long j, long j2, j jVar) {
        lh4 lh4Var = this.r2;
        if (lh4Var != null) {
            lh4Var.e(j, j2, jVar, s0());
        }
    }

    @Override // androidx.media3.exoplayer.mediacodec.MediaCodecRenderer
    public void N0(long j) {
        super.N0(j);
        if (this.o2) {
            return;
        }
        this.e2--;
    }

    public void N1(long j) throws ExoPlaybackException {
        n1(j);
        J1();
        this.E1.e++;
        H1();
        N0(j);
    }

    @Override // androidx.media3.exoplayer.mediacodec.MediaCodecRenderer
    public void O0() {
        super.O0();
        q1();
    }

    public final void O1() {
        c1();
    }

    @Override // androidx.media3.exoplayer.mediacodec.MediaCodecRenderer
    public void P0(DecoderInputBuffer decoderInputBuffer) throws ExoPlaybackException {
        boolean z = this.o2;
        if (!z) {
            this.e2++;
        }
        if (androidx.media3.common.util.b.a >= 23 || !z) {
            return;
        }
        N1(decoderInputBuffer.i0);
    }

    public final void P1() {
        Surface surface = this.S1;
        PlaceholderSurface placeholderSurface = this.T1;
        if (surface == placeholderSurface) {
            this.S1 = null;
        }
        placeholderSurface.release();
        this.T1 = null;
    }

    public void Q1(d dVar, int i, long j) {
        J1();
        v74.a("releaseOutputBuffer");
        dVar.j(i, true);
        v74.c();
        this.g2 = SystemClock.elapsedRealtime() * 1000;
        this.E1.e++;
        this.d2 = 0;
        H1();
    }

    @Override // androidx.media3.exoplayer.mediacodec.MediaCodecRenderer
    public boolean R0(long j, long j2, d dVar, ByteBuffer byteBuffer, int i, int i2, int i3, long j3, boolean z, boolean z2, j jVar) throws ExoPlaybackException {
        boolean z3;
        long j4;
        ii.e(dVar);
        if (this.Z1 == CellBase.ID_SYSTEM_MESSAGE_REQUEST_CLOSED) {
            this.Z1 = j;
        }
        if (j3 != this.f2) {
            this.K1.h(j3);
            this.f2 = j3;
        }
        long w0 = w0();
        long j5 = j3 - w0;
        if (z && !z2) {
            a2(dVar, i, j5);
            return true;
        }
        double x0 = x0();
        boolean z4 = getState() == 2;
        long elapsedRealtime = SystemClock.elapsedRealtime() * 1000;
        long j6 = (long) ((j3 - j) / x0);
        if (z4) {
            j6 -= elapsedRealtime - j2;
        }
        if (this.S1 == this.T1) {
            if (D1(j6)) {
                a2(dVar, i, j5);
                c2(j6);
                return true;
            }
            return false;
        }
        long j7 = elapsedRealtime - this.g2;
        if (this.Y1 ? this.W1 : !(z4 || this.X1)) {
            j4 = j7;
            z3 = false;
        } else {
            z3 = true;
            j4 = j7;
        }
        if (this.a2 == CellBase.ID_SYSTEM_MESSAGE_REQUEST_CLOSED && j >= w0 && (z3 || (z4 && Y1(j6, j4)))) {
            long nanoTime = System.nanoTime();
            M1(j5, nanoTime, jVar);
            if (androidx.media3.common.util.b.a >= 21) {
                R1(dVar, i, j5, nanoTime);
            } else {
                Q1(dVar, i, j5);
            }
            c2(j6);
            return true;
        }
        if (z4 && j != this.Z1) {
            long nanoTime2 = System.nanoTime();
            long b2 = this.K1.b((j6 * 1000) + nanoTime2);
            long j8 = (b2 - nanoTime2) / 1000;
            boolean z5 = this.a2 != CellBase.ID_SYSTEM_MESSAGE_REQUEST_CLOSED;
            if (W1(j8, j2, z2) && F1(j, z5)) {
                return false;
            }
            if (X1(j8, j2, z2)) {
                if (z5) {
                    a2(dVar, i, j5);
                } else {
                    v1(dVar, i, j5);
                }
                c2(j8);
                return true;
            } else if (androidx.media3.common.util.b.a >= 21) {
                if (j8 < 50000) {
                    M1(j5, b2, jVar);
                    R1(dVar, i, j5, b2);
                    c2(j8);
                    return true;
                }
            } else if (j8 < 30000) {
                if (j8 > 11000) {
                    try {
                        Thread.sleep((j8 - 10000) / 1000);
                    } catch (InterruptedException unused) {
                        Thread.currentThread().interrupt();
                        return false;
                    }
                }
                M1(j5, b2, jVar);
                Q1(dVar, i, j5);
                c2(j8);
                return true;
            }
        }
        return false;
    }

    public void R1(d dVar, int i, long j, long j2) {
        J1();
        v74.a("releaseOutputBuffer");
        dVar.g(i, j2);
        v74.c();
        this.g2 = SystemClock.elapsedRealtime() * 1000;
        this.E1.e++;
        this.d2 = 0;
        H1();
    }

    @Override // androidx.media3.exoplayer.mediacodec.MediaCodecRenderer
    public jf0 S(e eVar, j jVar, j jVar2) {
        jf0 e = eVar.e(jVar, jVar2);
        int i = e.e;
        int i2 = jVar2.u0;
        C0043a c0043a = this.P1;
        if (i2 > c0043a.a || jVar2.v0 > c0043a.b) {
            i |= 256;
        }
        if (B1(eVar, jVar2) > this.P1.c) {
            i |= 64;
        }
        int i3 = i;
        return new jf0(eVar.a, jVar, jVar2, i3 != 0 ? 0 : e.d, i3);
    }

    public final void T1() {
        this.a2 = this.M1 > 0 ? SystemClock.elapsedRealtime() + this.M1 : CellBase.ID_SYSTEM_MESSAGE_REQUEST_CLOSED;
    }

    /* JADX WARN: Multi-variable type inference failed */
    /* JADX WARN: Type inference failed for: r4v0, types: [androidx.media3.exoplayer.video.a, androidx.media3.exoplayer.c, androidx.media3.exoplayer.mediacodec.MediaCodecRenderer] */
    /* JADX WARN: Type inference failed for: r5v8, types: [android.view.Surface] */
    public final void U1(Object obj) throws ExoPlaybackException {
        PlaceholderSurface placeholderSurface = obj instanceof Surface ? (Surface) obj : null;
        if (placeholderSurface == null) {
            PlaceholderSurface placeholderSurface2 = this.T1;
            if (placeholderSurface2 != null) {
                placeholderSurface = placeholderSurface2;
            } else {
                e p0 = p0();
                if (p0 != null && Z1(p0)) {
                    placeholderSurface = PlaceholderSurface.c(this.J1, p0.f);
                    this.T1 = placeholderSurface;
                }
            }
        }
        if (this.S1 != placeholderSurface) {
            this.S1 = placeholderSurface;
            this.K1.m(placeholderSurface);
            this.U1 = false;
            int state = getState();
            d o0 = o0();
            if (o0 != null) {
                if (androidx.media3.common.util.b.a >= 23 && placeholderSurface != null && !this.Q1) {
                    V1(o0, placeholderSurface);
                } else {
                    V0();
                    G0();
                }
            }
            if (placeholderSurface != null && placeholderSurface != this.T1) {
                L1();
                q1();
                if (state == 2) {
                    T1();
                    return;
                }
                return;
            }
            r1();
            q1();
        } else if (placeholderSurface == null || placeholderSurface == this.T1) {
        } else {
            L1();
            K1();
        }
    }

    public void V1(d dVar, Surface surface) {
        dVar.m(surface);
    }

    public boolean W1(long j, long j2, boolean z) {
        return E1(j) && !z;
    }

    @Override // androidx.media3.exoplayer.mediacodec.MediaCodecRenderer
    public void X0() {
        super.X0();
        this.e2 = 0;
    }

    public boolean X1(long j, long j2, boolean z) {
        return D1(j) && !z;
    }

    public boolean Y1(long j, long j2) {
        return D1(j) && j2 > 100000;
    }

    public final boolean Z1(e eVar) {
        return androidx.media3.common.util.b.a >= 23 && !this.o2 && !s1(eVar.a) && (!eVar.f || PlaceholderSurface.b(this.J1));
    }

    public void a2(d dVar, int i, long j) {
        v74.a("skipVideoBuffer");
        dVar.j(i, false);
        v74.c();
        this.E1.f++;
    }

    public void b2(int i, int i2) {
        hf0 hf0Var = this.E1;
        hf0Var.h += i;
        int i3 = i + i2;
        hf0Var.g += i3;
        this.c2 += i3;
        int i4 = this.d2 + i3;
        this.d2 = i4;
        hf0Var.i = Math.max(i4, hf0Var.i);
        int i5 = this.N1;
        if (i5 <= 0 || this.c2 < i5) {
            return;
        }
        G1();
    }

    @Override // androidx.media3.exoplayer.mediacodec.MediaCodecRenderer
    public MediaCodecDecoderException c0(Throwable th, e eVar) {
        return new MediaCodecVideoDecoderException(th, eVar, this.S1);
    }

    public void c2(long j) {
        this.E1.a(j);
        this.h2 += j;
        this.i2++;
    }

    @Override // androidx.media3.exoplayer.mediacodec.MediaCodecRenderer, androidx.media3.exoplayer.m
    public boolean f() {
        PlaceholderSurface placeholderSurface;
        if (super.f() && (this.W1 || (((placeholderSurface = this.T1) != null && this.S1 == placeholderSurface) || o0() == null || this.o2))) {
            this.a2 = CellBase.ID_SYSTEM_MESSAGE_REQUEST_CLOSED;
            return true;
        } else if (this.a2 == CellBase.ID_SYSTEM_MESSAGE_REQUEST_CLOSED) {
            return false;
        } else {
            if (SystemClock.elapsedRealtime() < this.a2) {
                return true;
            }
            this.a2 = CellBase.ID_SYSTEM_MESSAGE_REQUEST_CLOSED;
            return false;
        }
    }

    @Override // androidx.media3.exoplayer.mediacodec.MediaCodecRenderer
    public boolean g1(e eVar) {
        return this.S1 != null || Z1(eVar);
    }

    @Override // androidx.media3.exoplayer.m, androidx.media3.exoplayer.n
    public String getName() {
        return "MediaCodecVideoRenderer";
    }

    @Override // androidx.media3.exoplayer.mediacodec.MediaCodecRenderer
    public int j1(f fVar, j jVar) throws MediaCodecUtil.DecoderQueryException {
        boolean z;
        int i = 0;
        if (!y82.q(jVar.p0)) {
            return u63.a(0);
        }
        boolean z2 = jVar.s0 != null;
        List<e> A1 = A1(fVar, jVar, z2, false);
        if (z2 && A1.isEmpty()) {
            A1 = A1(fVar, jVar, false, false);
        }
        if (A1.isEmpty()) {
            return u63.a(1);
        }
        if (!MediaCodecRenderer.k1(jVar)) {
            return u63.a(2);
        }
        e eVar = A1.get(0);
        boolean m = eVar.m(jVar);
        if (!m) {
            for (int i2 = 1; i2 < A1.size(); i2++) {
                e eVar2 = A1.get(i2);
                if (eVar2.m(jVar)) {
                    z = false;
                    m = true;
                    eVar = eVar2;
                    break;
                }
            }
        }
        z = true;
        int i3 = m ? 4 : 3;
        int i4 = eVar.p(jVar) ? 16 : 8;
        int i5 = eVar.g ? 64 : 0;
        int i6 = z ? 128 : 0;
        if (m) {
            List<e> A12 = A1(fVar, jVar, z2, true);
            if (!A12.isEmpty()) {
                e eVar3 = MediaCodecUtil.u(A12, jVar).get(0);
                if (eVar3.m(jVar) && eVar3.p(jVar)) {
                    i = 32;
                }
            }
        }
        return u63.c(i3, i4, i, i5, i6);
    }

    @Override // androidx.media3.exoplayer.mediacodec.MediaCodecRenderer, androidx.media3.exoplayer.c, androidx.media3.exoplayer.m
    public void o(float f, float f2) throws ExoPlaybackException {
        super.o(f, f2);
        this.K1.i(f);
    }

    @Override // androidx.media3.exoplayer.mediacodec.MediaCodecRenderer
    public boolean q0() {
        return this.o2 && androidx.media3.common.util.b.a < 23;
    }

    public final void q1() {
        d o0;
        this.W1 = false;
        if (androidx.media3.common.util.b.a < 23 || !this.o2 || (o0 = o0()) == null) {
            return;
        }
        this.q2 = new b(o0);
    }

    @Override // androidx.media3.exoplayer.mediacodec.MediaCodecRenderer
    public float r0(float f, j jVar, j[] jVarArr) {
        float f2 = -1.0f;
        for (j jVar2 : jVarArr) {
            float f3 = jVar2.w0;
            if (f3 != -1.0f) {
                f2 = Math.max(f2, f3);
            }
        }
        if (f2 == -1.0f) {
            return -1.0f;
        }
        return f2 * f;
    }

    public final void r1() {
        this.n2 = null;
    }

    @Override // androidx.media3.exoplayer.c, androidx.media3.exoplayer.k.b
    public void s(int i, Object obj) throws ExoPlaybackException {
        if (i == 1) {
            U1(obj);
        } else if (i == 7) {
            this.r2 = (lh4) obj;
        } else if (i == 10) {
            int intValue = ((Integer) obj).intValue();
            if (this.p2 != intValue) {
                this.p2 = intValue;
                if (this.o2) {
                    V0();
                }
            }
        } else if (i != 4) {
            if (i != 5) {
                super.s(i, obj);
            } else {
                this.K1.o(((Integer) obj).intValue());
            }
        } else {
            this.V1 = ((Integer) obj).intValue();
            d o0 = o0();
            if (o0 != null) {
                o0.k(this.V1);
            }
        }
    }

    public boolean s1(String str) {
        if (str.startsWith("OMX.google")) {
            return false;
        }
        synchronized (a.class) {
            if (!t2) {
                u2 = w1();
                t2 = true;
            }
        }
        return u2;
    }

    @Override // androidx.media3.exoplayer.mediacodec.MediaCodecRenderer
    public List<e> t0(f fVar, j jVar, boolean z) throws MediaCodecUtil.DecoderQueryException {
        return MediaCodecUtil.u(A1(fVar, jVar, z, this.o2), jVar);
    }

    @Override // androidx.media3.exoplayer.mediacodec.MediaCodecRenderer
    @TargetApi(17)
    public d.a v0(e eVar, j jVar, MediaCrypto mediaCrypto, float f) {
        PlaceholderSurface placeholderSurface = this.T1;
        if (placeholderSurface != null && placeholderSurface.a != eVar.f) {
            P1();
        }
        String str = eVar.c;
        C0043a z1 = z1(eVar, jVar, E());
        this.P1 = z1;
        MediaFormat C1 = C1(jVar, str, z1, f, this.O1, this.o2 ? this.p2 : 0);
        if (this.S1 == null) {
            if (Z1(eVar)) {
                if (this.T1 == null) {
                    this.T1 = PlaceholderSurface.c(this.J1, eVar.f);
                }
                this.S1 = this.T1;
            } else {
                throw new IllegalStateException();
            }
        }
        return d.a.b(eVar, C1, jVar, this.S1, mediaCrypto);
    }

    public void v1(d dVar, int i, long j) {
        v74.a("dropVideoBuffer");
        dVar.j(i, false);
        v74.c();
        b2(0, 1);
    }

    @Override // androidx.media3.exoplayer.mediacodec.MediaCodecRenderer
    @TargetApi(29)
    public void y0(DecoderInputBuffer decoderInputBuffer) throws ExoPlaybackException {
        if (this.R1) {
            ByteBuffer byteBuffer = (ByteBuffer) ii.e(decoderInputBuffer.j0);
            if (byteBuffer.remaining() >= 7) {
                byte b2 = byteBuffer.get();
                short s = byteBuffer.getShort();
                short s3 = byteBuffer.getShort();
                byte b3 = byteBuffer.get();
                byte b4 = byteBuffer.get();
                byteBuffer.position(0);
                if (b2 == -75 && s == 60 && s3 == 1 && b3 == 4 && b4 == 0) {
                    byte[] bArr = new byte[byteBuffer.remaining()];
                    byteBuffer.get(bArr);
                    byteBuffer.position(0);
                    S1(o0(), bArr);
                }
            }
        }
    }

    public C0043a z1(e eVar, j jVar, j[] jVarArr) {
        int x1;
        int i = jVar.u0;
        int i2 = jVar.v0;
        int B1 = B1(eVar, jVar);
        if (jVarArr.length == 1) {
            if (B1 != -1 && (x1 = x1(eVar, jVar)) != -1) {
                B1 = Math.min((int) (B1 * 1.5f), x1);
            }
            return new C0043a(i, i2, B1);
        }
        int length = jVarArr.length;
        boolean z = false;
        for (int i3 = 0; i3 < length; i3++) {
            j jVar2 = jVarArr[i3];
            if (jVar.B0 != null && jVar2.B0 == null) {
                jVar2 = jVar2.b().J(jVar.B0).E();
            }
            if (eVar.e(jVar, jVar2).d != 0) {
                int i4 = jVar2.u0;
                z |= i4 == -1 || jVar2.v0 == -1;
                i = Math.max(i, i4);
                i2 = Math.max(i2, jVar2.v0);
                B1 = Math.max(B1, B1(eVar, jVar2));
            }
        }
        if (z) {
            p12.i("MediaCodecVideoRenderer", "Resolutions unknown. Codec max resolution: " + i + "x" + i2);
            Point y1 = y1(eVar, jVar);
            if (y1 != null) {
                i = Math.max(i, y1.x);
                i2 = Math.max(i2, y1.y);
                B1 = Math.max(B1, x1(eVar, jVar.b().j0(i).Q(i2).E()));
                p12.i("MediaCodecVideoRenderer", "Codec max resolution adjusted to: " + i + "x" + i2);
            }
        }
        return new C0043a(i, i2, B1);
    }

    public a(Context context, d.b bVar, f fVar, long j, boolean z, Handler handler, androidx.media3.exoplayer.video.b bVar2, int i, float f) {
        super(2, bVar, fVar, z, f);
        this.M1 = j;
        this.N1 = i;
        Context applicationContext = context.getApplicationContext();
        this.J1 = applicationContext;
        this.K1 = new nh4(applicationContext);
        this.L1 = new b.a(handler, bVar2);
        this.O1 = u1();
        this.a2 = CellBase.ID_SYSTEM_MESSAGE_REQUEST_CLOSED;
        this.j2 = -1;
        this.k2 = -1;
        this.m2 = -1.0f;
        this.V1 = 1;
        this.p2 = 0;
        r1();
    }
}
