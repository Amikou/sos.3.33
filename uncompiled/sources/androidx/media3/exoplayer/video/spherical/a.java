package androidx.media3.exoplayer.video.spherical;

import androidx.media3.common.j;
import androidx.media3.decoder.DecoderInputBuffer;
import androidx.media3.exoplayer.ExoPlaybackException;
import java.nio.ByteBuffer;

/* compiled from: CameraMotionRenderer.java */
/* loaded from: classes.dex */
public final class a extends androidx.media3.exoplayer.c {
    public final DecoderInputBuffer q0;
    public final op2 r0;
    public long s0;
    public ev t0;
    public long u0;

    public a() {
        super(6);
        this.q0 = new DecoderInputBuffer(1);
        this.r0 = new op2();
    }

    @Override // androidx.media3.exoplayer.c
    public void G() {
        R();
    }

    @Override // androidx.media3.exoplayer.c
    public void I(long j, boolean z) {
        this.u0 = Long.MIN_VALUE;
        R();
    }

    @Override // androidx.media3.exoplayer.c
    public void M(j[] jVarArr, long j, long j2) {
        this.s0 = j2;
    }

    public final float[] Q(ByteBuffer byteBuffer) {
        if (byteBuffer.remaining() != 16) {
            return null;
        }
        this.r0.N(byteBuffer.array(), byteBuffer.limit());
        this.r0.P(byteBuffer.arrayOffset() + 4);
        float[] fArr = new float[3];
        for (int i = 0; i < 3; i++) {
            fArr[i] = Float.intBitsToFloat(this.r0.q());
        }
        return fArr;
    }

    public final void R() {
        ev evVar = this.t0;
        if (evVar != null) {
            evVar.b();
        }
    }

    @Override // androidx.media3.exoplayer.n
    public int a(j jVar) {
        if ("application/x-camera-motion".equals(jVar.p0)) {
            return u63.a(4);
        }
        return u63.a(0);
    }

    @Override // androidx.media3.exoplayer.m
    public boolean d() {
        return i();
    }

    @Override // androidx.media3.exoplayer.m
    public boolean f() {
        return true;
    }

    @Override // androidx.media3.exoplayer.m, androidx.media3.exoplayer.n
    public String getName() {
        return "CameraMotionRenderer";
    }

    @Override // androidx.media3.exoplayer.m
    public void r(long j, long j2) {
        while (!i() && this.u0 < 100000 + j) {
            this.q0.h();
            if (N(B(), this.q0, 0) != -4 || this.q0.p()) {
                return;
            }
            DecoderInputBuffer decoderInputBuffer = this.q0;
            this.u0 = decoderInputBuffer.i0;
            if (this.t0 != null && !decoderInputBuffer.o()) {
                this.q0.x();
                float[] Q = Q((ByteBuffer) androidx.media3.common.util.b.j(this.q0.g0));
                if (Q != null) {
                    ((ev) androidx.media3.common.util.b.j(this.t0)).a(this.u0 - this.s0, Q);
                }
            }
        }
    }

    @Override // androidx.media3.exoplayer.c, androidx.media3.exoplayer.k.b
    public void s(int i, Object obj) throws ExoPlaybackException {
        if (i == 8) {
            this.t0 = (ev) obj;
        } else {
            super.s(i, obj);
        }
    }
}
