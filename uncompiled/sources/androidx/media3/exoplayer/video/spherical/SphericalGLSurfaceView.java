package androidx.media3.exoplayer.video.spherical;

import android.content.Context;
import android.graphics.PointF;
import android.graphics.SurfaceTexture;
import android.hardware.Sensor;
import android.hardware.SensorManager;
import android.opengl.GLES20;
import android.opengl.GLSurfaceView;
import android.opengl.Matrix;
import android.os.Handler;
import android.os.Looper;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.view.Surface;
import android.view.WindowManager;
import androidx.media3.exoplayer.video.spherical.SphericalGLSurfaceView;
import androidx.media3.exoplayer.video.spherical.b;
import androidx.media3.exoplayer.video.spherical.c;
import com.github.mikephil.charting.utils.Utils;
import java.util.Iterator;
import java.util.concurrent.CopyOnWriteArrayList;
import javax.microedition.khronos.egl.EGLConfig;
import javax.microedition.khronos.opengles.GL10;

/* loaded from: classes.dex */
public final class SphericalGLSurfaceView extends GLSurfaceView {
    public final CopyOnWriteArrayList<b> a;
    public final SensorManager f0;
    public final Sensor g0;
    public final androidx.media3.exoplayer.video.spherical.b h0;
    public final Handler i0;
    public final c j0;
    public final yc3 k0;
    public SurfaceTexture l0;
    public Surface m0;
    public boolean n0;
    public boolean o0;
    public boolean p0;

    /* loaded from: classes.dex */
    public final class a implements GLSurfaceView.Renderer, c.a, b.a {
        public final yc3 a;
        public final float[] h0;
        public final float[] i0;
        public final float[] j0;
        public float k0;
        public float l0;
        public final float[] f0 = new float[16];
        public final float[] g0 = new float[16];
        public final float[] m0 = new float[16];
        public final float[] n0 = new float[16];

        public a(yc3 yc3Var) {
            float[] fArr = new float[16];
            this.h0 = fArr;
            float[] fArr2 = new float[16];
            this.i0 = fArr2;
            float[] fArr3 = new float[16];
            this.j0 = fArr3;
            this.a = yc3Var;
            Matrix.setIdentityM(fArr, 0);
            Matrix.setIdentityM(fArr2, 0);
            Matrix.setIdentityM(fArr3, 0);
            this.l0 = 3.1415927f;
        }

        @Override // androidx.media3.exoplayer.video.spherical.b.a
        public synchronized void a(float[] fArr, float f) {
            float[] fArr2 = this.h0;
            System.arraycopy(fArr, 0, fArr2, 0, fArr2.length);
            this.l0 = -f;
            d();
        }

        @Override // androidx.media3.exoplayer.video.spherical.c.a
        public synchronized void b(PointF pointF) {
            this.k0 = pointF.y;
            d();
            Matrix.setRotateM(this.j0, 0, -pointF.x, Utils.FLOAT_EPSILON, 1.0f, Utils.FLOAT_EPSILON);
        }

        public final float c(float f) {
            if (f > 1.0f) {
                return (float) (Math.toDegrees(Math.atan(Math.tan(Math.toRadians(45.0d)) / f)) * 2.0d);
            }
            return 90.0f;
        }

        public final void d() {
            Matrix.setRotateM(this.i0, 0, -this.k0, (float) Math.cos(this.l0), (float) Math.sin(this.l0), Utils.FLOAT_EPSILON);
        }

        @Override // android.opengl.GLSurfaceView.Renderer
        public void onDrawFrame(GL10 gl10) {
            synchronized (this) {
                Matrix.multiplyMM(this.n0, 0, this.h0, 0, this.j0, 0);
                Matrix.multiplyMM(this.m0, 0, this.i0, 0, this.n0, 0);
            }
            Matrix.multiplyMM(this.g0, 0, this.f0, 0, this.m0, 0);
            this.a.d(this.g0, false);
        }

        @Override // androidx.media3.exoplayer.video.spherical.c.a
        public boolean onSingleTapUp(MotionEvent motionEvent) {
            return SphericalGLSurfaceView.this.performClick();
        }

        @Override // android.opengl.GLSurfaceView.Renderer
        public void onSurfaceChanged(GL10 gl10, int i, int i2) {
            GLES20.glViewport(0, 0, i, i2);
            float f = i / i2;
            Matrix.perspectiveM(this.f0, 0, c(f), f, 0.1f, 100.0f);
        }

        @Override // android.opengl.GLSurfaceView.Renderer
        public synchronized void onSurfaceCreated(GL10 gl10, EGLConfig eGLConfig) {
            SphericalGLSurfaceView.this.g(this.a.f());
        }
    }

    /* loaded from: classes.dex */
    public interface b {
        void E(Surface surface);

        void F(Surface surface);
    }

    public SphericalGLSurfaceView(Context context) {
        this(context, null);
    }

    /* JADX INFO: Access modifiers changed from: private */
    public /* synthetic */ void e() {
        Surface surface = this.m0;
        if (surface != null) {
            Iterator<b> it = this.a.iterator();
            while (it.hasNext()) {
                it.next().E(surface);
            }
        }
        h(this.l0, surface);
        this.l0 = null;
        this.m0 = null;
    }

    /* JADX INFO: Access modifiers changed from: private */
    public /* synthetic */ void f(SurfaceTexture surfaceTexture) {
        SurfaceTexture surfaceTexture2 = this.l0;
        Surface surface = this.m0;
        Surface surface2 = new Surface(surfaceTexture);
        this.l0 = surfaceTexture;
        this.m0 = surface2;
        Iterator<b> it = this.a.iterator();
        while (it.hasNext()) {
            it.next().F(surface2);
        }
        h(surfaceTexture2, surface);
    }

    public static void h(SurfaceTexture surfaceTexture, Surface surface) {
        if (surfaceTexture != null) {
            surfaceTexture.release();
        }
        if (surface != null) {
            surface.release();
        }
    }

    public void d(b bVar) {
        this.a.add(bVar);
    }

    public final void g(final SurfaceTexture surfaceTexture) {
        this.i0.post(new Runnable() { // from class: or3
            @Override // java.lang.Runnable
            public final void run() {
                SphericalGLSurfaceView.this.f(surfaceTexture);
            }
        });
    }

    public ev getCameraMotionListener() {
        return this.k0;
    }

    public lh4 getVideoFrameMetadataListener() {
        return this.k0;
    }

    public Surface getVideoSurface() {
        return this.m0;
    }

    public void i(b bVar) {
        this.a.remove(bVar);
    }

    public final void j() {
        boolean z = this.n0 && this.o0;
        Sensor sensor = this.g0;
        if (sensor == null || z == this.p0) {
            return;
        }
        if (z) {
            this.f0.registerListener(this.h0, sensor, 0);
        } else {
            this.f0.unregisterListener(this.h0);
        }
        this.p0 = z;
    }

    @Override // android.opengl.GLSurfaceView, android.view.SurfaceView, android.view.View
    public void onDetachedFromWindow() {
        super.onDetachedFromWindow();
        this.i0.post(new Runnable() { // from class: nr3
            @Override // java.lang.Runnable
            public final void run() {
                SphericalGLSurfaceView.this.e();
            }
        });
    }

    @Override // android.opengl.GLSurfaceView
    public void onPause() {
        this.o0 = false;
        j();
        super.onPause();
    }

    @Override // android.opengl.GLSurfaceView
    public void onResume() {
        super.onResume();
        this.o0 = true;
        j();
    }

    public void setDefaultStereoMode(int i) {
        this.k0.h(i);
    }

    public void setUseSensorRotation(boolean z) {
        this.n0 = z;
        j();
    }

    public SphericalGLSurfaceView(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        this.a = new CopyOnWriteArrayList<>();
        this.i0 = new Handler(Looper.getMainLooper());
        SensorManager sensorManager = (SensorManager) ii.e(context.getSystemService("sensor"));
        this.f0 = sensorManager;
        Sensor defaultSensor = androidx.media3.common.util.b.a >= 18 ? sensorManager.getDefaultSensor(15) : null;
        this.g0 = defaultSensor == null ? sensorManager.getDefaultSensor(11) : defaultSensor;
        yc3 yc3Var = new yc3();
        this.k0 = yc3Var;
        a aVar = new a(yc3Var);
        c cVar = new c(context, aVar, 25.0f);
        this.j0 = cVar;
        this.h0 = new androidx.media3.exoplayer.video.spherical.b(((WindowManager) ii.e((WindowManager) context.getSystemService("window"))).getDefaultDisplay(), cVar, aVar);
        this.n0 = true;
        setEGLContextClientVersion(2);
        setRenderer(aVar);
        setOnTouchListener(cVar);
    }
}
