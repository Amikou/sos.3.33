package androidx.media3.exoplayer.video.spherical;

import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.opengl.Matrix;
import android.view.Display;
import com.github.mikephil.charting.utils.Utils;

/* compiled from: OrientationListener.java */
/* loaded from: classes.dex */
public final class b implements SensorEventListener {
    public final float[] a = new float[16];
    public final float[] b = new float[16];
    public final float[] c = new float[16];
    public final float[] d = new float[3];
    public final Display e;
    public final a[] f;
    public boolean g;

    /* compiled from: OrientationListener.java */
    /* loaded from: classes.dex */
    public interface a {
        void a(float[] fArr, float f);
    }

    public b(Display display, a... aVarArr) {
        this.e = display;
        this.f = aVarArr;
    }

    public static void e(float[] fArr) {
        Matrix.rotateM(fArr, 0, 90.0f, 1.0f, Utils.FLOAT_EPSILON, Utils.FLOAT_EPSILON);
    }

    public final float a(float[] fArr) {
        SensorManager.remapCoordinateSystem(fArr, 1, 131, this.b);
        SensorManager.getOrientation(this.b, this.d);
        return this.d[2];
    }

    public final void b(float[] fArr, float f) {
        for (a aVar : this.f) {
            aVar.a(fArr, f);
        }
    }

    public final void c(float[] fArr) {
        if (!this.g) {
            zb1.a(this.c, fArr);
            this.g = true;
        }
        float[] fArr2 = this.b;
        System.arraycopy(fArr, 0, fArr2, 0, fArr2.length);
        Matrix.multiplyMM(fArr, 0, this.b, 0, this.c, 0);
    }

    public final void d(float[] fArr, int i) {
        if (i != 0) {
            int i2 = 130;
            int i3 = 129;
            if (i == 1) {
                i2 = 2;
            } else if (i == 2) {
                i3 = 130;
                i2 = 129;
            } else if (i != 3) {
                throw new IllegalStateException();
            } else {
                i3 = 1;
            }
            float[] fArr2 = this.b;
            System.arraycopy(fArr, 0, fArr2, 0, fArr2.length);
            SensorManager.remapCoordinateSystem(this.b, i2, i3, fArr);
        }
    }

    @Override // android.hardware.SensorEventListener
    public void onAccuracyChanged(Sensor sensor, int i) {
    }

    @Override // android.hardware.SensorEventListener
    public void onSensorChanged(SensorEvent sensorEvent) {
        SensorManager.getRotationMatrixFromVector(this.a, sensorEvent.values);
        d(this.a, this.e.getRotation());
        float a2 = a(this.a);
        e(this.a);
        c(this.a);
        b(this.a, a2);
    }
}
