package androidx.media3.exoplayer.video;

import android.os.Handler;
import android.os.SystemClock;
import androidx.media3.common.j;
import androidx.media3.common.z;
import androidx.media3.exoplayer.video.b;

/* compiled from: VideoRendererEventListener.java */
/* loaded from: classes.dex */
public interface b {

    /* compiled from: VideoRendererEventListener.java */
    /* loaded from: classes.dex */
    public static final class a {
        public final Handler a;
        public final b b;

        public a(Handler handler, b bVar) {
            this.a = bVar != null ? (Handler) ii.e(handler) : null;
            this.b = bVar;
        }

        /* JADX INFO: Access modifiers changed from: private */
        public /* synthetic */ void q(String str, long j, long j2) {
            ((b) androidx.media3.common.util.b.j(this.b)).f(str, j, j2);
        }

        /* JADX INFO: Access modifiers changed from: private */
        public /* synthetic */ void r(String str) {
            ((b) androidx.media3.common.util.b.j(this.b)).e(str);
        }

        /* JADX INFO: Access modifiers changed from: private */
        public /* synthetic */ void s(hf0 hf0Var) {
            hf0Var.c();
            ((b) androidx.media3.common.util.b.j(this.b)).g(hf0Var);
        }

        /* JADX INFO: Access modifiers changed from: private */
        public /* synthetic */ void t(int i, long j) {
            ((b) androidx.media3.common.util.b.j(this.b)).p(i, j);
        }

        /* JADX INFO: Access modifiers changed from: private */
        public /* synthetic */ void u(hf0 hf0Var) {
            ((b) androidx.media3.common.util.b.j(this.b)).h(hf0Var);
        }

        /* JADX INFO: Access modifiers changed from: private */
        public /* synthetic */ void v(j jVar, jf0 jf0Var) {
            ((b) androidx.media3.common.util.b.j(this.b)).i(jVar);
            ((b) androidx.media3.common.util.b.j(this.b)).d(jVar, jf0Var);
        }

        /* JADX INFO: Access modifiers changed from: private */
        public /* synthetic */ void w(Object obj, long j) {
            ((b) androidx.media3.common.util.b.j(this.b)).r(obj, j);
        }

        /* JADX INFO: Access modifiers changed from: private */
        public /* synthetic */ void x(long j, int i) {
            ((b) androidx.media3.common.util.b.j(this.b)).C(j, i);
        }

        /* JADX INFO: Access modifiers changed from: private */
        public /* synthetic */ void y(Exception exc) {
            ((b) androidx.media3.common.util.b.j(this.b)).x(exc);
        }

        /* JADX INFO: Access modifiers changed from: private */
        public /* synthetic */ void z(z zVar) {
            ((b) androidx.media3.common.util.b.j(this.b)).k(zVar);
        }

        public void A(final Object obj) {
            if (this.a != null) {
                final long elapsedRealtime = SystemClock.elapsedRealtime();
                this.a.post(new Runnable() { // from class: wh4
                    @Override // java.lang.Runnable
                    public final void run() {
                        b.a.this.w(obj, elapsedRealtime);
                    }
                });
            }
        }

        public void B(final long j, final int i) {
            Handler handler = this.a;
            if (handler != null) {
                handler.post(new Runnable() { // from class: qh4
                    @Override // java.lang.Runnable
                    public final void run() {
                        b.a.this.x(j, i);
                    }
                });
            }
        }

        public void C(final Exception exc) {
            Handler handler = this.a;
            if (handler != null) {
                handler.post(new Runnable() { // from class: vh4
                    @Override // java.lang.Runnable
                    public final void run() {
                        b.a.this.y(exc);
                    }
                });
            }
        }

        public void D(final z zVar) {
            Handler handler = this.a;
            if (handler != null) {
                handler.post(new Runnable() { // from class: uh4
                    @Override // java.lang.Runnable
                    public final void run() {
                        b.a.this.z(zVar);
                    }
                });
            }
        }

        public void k(final String str, final long j, final long j2) {
            Handler handler = this.a;
            if (handler != null) {
                handler.post(new Runnable() { // from class: yh4
                    @Override // java.lang.Runnable
                    public final void run() {
                        b.a.this.q(str, j, j2);
                    }
                });
            }
        }

        public void l(final String str) {
            Handler handler = this.a;
            if (handler != null) {
                handler.post(new Runnable() { // from class: xh4
                    @Override // java.lang.Runnable
                    public final void run() {
                        b.a.this.r(str);
                    }
                });
            }
        }

        public void m(final hf0 hf0Var) {
            hf0Var.c();
            Handler handler = this.a;
            if (handler != null) {
                handler.post(new Runnable() { // from class: sh4
                    @Override // java.lang.Runnable
                    public final void run() {
                        b.a.this.s(hf0Var);
                    }
                });
            }
        }

        public void n(final int i, final long j) {
            Handler handler = this.a;
            if (handler != null) {
                handler.post(new Runnable() { // from class: ph4
                    @Override // java.lang.Runnable
                    public final void run() {
                        b.a.this.t(i, j);
                    }
                });
            }
        }

        public void o(final hf0 hf0Var) {
            Handler handler = this.a;
            if (handler != null) {
                handler.post(new Runnable() { // from class: rh4
                    @Override // java.lang.Runnable
                    public final void run() {
                        b.a.this.u(hf0Var);
                    }
                });
            }
        }

        public void p(final j jVar, final jf0 jf0Var) {
            Handler handler = this.a;
            if (handler != null) {
                handler.post(new Runnable() { // from class: th4
                    @Override // java.lang.Runnable
                    public final void run() {
                        b.a.this.v(jVar, jf0Var);
                    }
                });
            }
        }
    }

    void C(long j, int i);

    void d(j jVar, jf0 jf0Var);

    void e(String str);

    void f(String str, long j, long j2);

    void g(hf0 hf0Var);

    void h(hf0 hf0Var);

    @Deprecated
    void i(j jVar);

    void k(z zVar);

    void p(int i, long j);

    void r(Object obj, long j);

    void x(Exception exc);
}
