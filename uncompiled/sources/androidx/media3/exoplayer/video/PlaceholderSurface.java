package androidx.media3.exoplayer.video;

import android.content.Context;
import android.graphics.SurfaceTexture;
import android.os.Handler;
import android.os.HandlerThread;
import android.os.Message;
import android.view.Surface;
import androidx.media3.common.util.EGLSurfaceTexture;
import androidx.media3.common.util.GlUtil;

/* loaded from: classes.dex */
public final class PlaceholderSurface extends Surface {
    public static int h0;
    public static boolean i0;
    public final boolean a;
    public final b f0;
    public boolean g0;

    /* loaded from: classes.dex */
    public static class b extends HandlerThread implements Handler.Callback {
        public EGLSurfaceTexture a;
        public Handler f0;
        public Error g0;
        public RuntimeException h0;
        public PlaceholderSurface i0;

        public b() {
            super("ExoPlayer:PlaceholderSurface");
        }

        public PlaceholderSurface a(int i) {
            boolean z;
            start();
            this.f0 = new Handler(getLooper(), this);
            this.a = new EGLSurfaceTexture(this.f0);
            synchronized (this) {
                z = false;
                this.f0.obtainMessage(1, i, 0).sendToTarget();
                while (this.i0 == null && this.h0 == null && this.g0 == null) {
                    try {
                        wait();
                    } catch (InterruptedException unused) {
                        z = true;
                    }
                }
            }
            if (z) {
                Thread.currentThread().interrupt();
            }
            RuntimeException runtimeException = this.h0;
            if (runtimeException == null) {
                Error error = this.g0;
                if (error == null) {
                    return (PlaceholderSurface) ii.e(this.i0);
                }
                throw error;
            }
            throw runtimeException;
        }

        public final void b(int i) {
            ii.e(this.a);
            this.a.h(i);
            this.i0 = new PlaceholderSurface(this, this.a.g(), i != 0);
        }

        public void c() {
            ii.e(this.f0);
            this.f0.sendEmptyMessage(2);
        }

        public final void d() {
            ii.e(this.a);
            this.a.i();
        }

        @Override // android.os.Handler.Callback
        public boolean handleMessage(Message message) {
            int i = message.what;
            try {
                if (i != 1) {
                    if (i != 2) {
                        return true;
                    }
                    try {
                        d();
                    } finally {
                        try {
                            return true;
                        } finally {
                        }
                    }
                    return true;
                }
                try {
                    b(message.arg1);
                    synchronized (this) {
                        notify();
                    }
                } catch (Error e) {
                    p12.d("PlaceholderSurface", "Failed to initialize placeholder surface", e);
                    this.g0 = e;
                    synchronized (this) {
                        notify();
                    }
                } catch (RuntimeException e2) {
                    p12.d("PlaceholderSurface", "Failed to initialize placeholder surface", e2);
                    this.h0 = e2;
                    synchronized (this) {
                        notify();
                    }
                }
                return true;
            } catch (Throwable th) {
                synchronized (this) {
                    notify();
                    throw th;
                }
            }
        }
    }

    public static int a(Context context) {
        if (GlUtil.h(context)) {
            return GlUtil.i() ? 1 : 2;
        }
        return 0;
    }

    public static synchronized boolean b(Context context) {
        boolean z;
        synchronized (PlaceholderSurface.class) {
            if (!i0) {
                h0 = a(context);
                i0 = true;
            }
            z = h0 != 0;
        }
        return z;
    }

    public static PlaceholderSurface c(Context context, boolean z) {
        ii.g(!z || b(context));
        return new b().a(z ? h0 : 0);
    }

    @Override // android.view.Surface
    public void release() {
        super.release();
        synchronized (this.f0) {
            if (!this.g0) {
                this.f0.c();
                this.g0 = true;
            }
        }
    }

    public PlaceholderSurface(b bVar, SurfaceTexture surfaceTexture, boolean z) {
        super(surfaceTexture);
        this.f0 = bVar;
        this.a = z;
    }
}
