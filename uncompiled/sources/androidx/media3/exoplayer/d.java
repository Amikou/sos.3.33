package androidx.media3.exoplayer;

import android.os.SystemClock;
import androidx.media3.common.m;
import com.github.mikephil.charting.utils.Utils;
import zendesk.support.request.CellBase;

/* compiled from: DefaultLivePlaybackSpeedControl.java */
/* loaded from: classes.dex */
public final class d implements r02 {
    public final float a;
    public final float b;
    public final long c;
    public final float d;
    public final long e;
    public final long f;
    public final float g;
    public long h;
    public long i;
    public long j;
    public long k;
    public long l;
    public long m;
    public float n;
    public float o;
    public float p;
    public long q;
    public long r;
    public long s;

    /* compiled from: DefaultLivePlaybackSpeedControl.java */
    /* loaded from: classes.dex */
    public static final class b {
        public float a = 0.97f;
        public float b = 1.03f;
        public long c = 1000;
        public float d = 1.0E-7f;
        public long e = androidx.media3.common.util.b.y0(20);
        public long f = androidx.media3.common.util.b.y0(500);
        public float g = 0.999f;

        public d a() {
            return new d(this.a, this.b, this.c, this.d, this.e, this.f, this.g);
        }
    }

    public static long h(long j, long j2, float f) {
        return (((float) j) * f) + ((1.0f - f) * ((float) j2));
    }

    @Override // defpackage.r02
    public void a(m.g gVar) {
        this.h = androidx.media3.common.util.b.y0(gVar.a);
        this.k = androidx.media3.common.util.b.y0(gVar.f0);
        this.l = androidx.media3.common.util.b.y0(gVar.g0);
        float f = gVar.h0;
        if (f == -3.4028235E38f) {
            f = this.a;
        }
        this.o = f;
        float f2 = gVar.i0;
        if (f2 == -3.4028235E38f) {
            f2 = this.b;
        }
        this.n = f2;
        if (f == 1.0f && f2 == 1.0f) {
            this.h = CellBase.ID_SYSTEM_MESSAGE_REQUEST_CLOSED;
        }
        g();
    }

    @Override // defpackage.r02
    public float b(long j, long j2) {
        if (this.h == CellBase.ID_SYSTEM_MESSAGE_REQUEST_CLOSED) {
            return 1.0f;
        }
        i(j, j2);
        if (this.q != CellBase.ID_SYSTEM_MESSAGE_REQUEST_CLOSED && SystemClock.elapsedRealtime() - this.q < this.c) {
            return this.p;
        }
        this.q = SystemClock.elapsedRealtime();
        f(j);
        long j3 = j - this.m;
        if (Math.abs(j3) < this.e) {
            this.p = 1.0f;
        } else {
            this.p = androidx.media3.common.util.b.p((this.d * ((float) j3)) + 1.0f, this.o, this.n);
        }
        return this.p;
    }

    @Override // defpackage.r02
    public long c() {
        return this.m;
    }

    @Override // defpackage.r02
    public void d() {
        long j = this.m;
        if (j == CellBase.ID_SYSTEM_MESSAGE_REQUEST_CLOSED) {
            return;
        }
        long j2 = j + this.f;
        this.m = j2;
        long j3 = this.l;
        if (j3 != CellBase.ID_SYSTEM_MESSAGE_REQUEST_CLOSED && j2 > j3) {
            this.m = j3;
        }
        this.q = CellBase.ID_SYSTEM_MESSAGE_REQUEST_CLOSED;
    }

    @Override // defpackage.r02
    public void e(long j) {
        this.i = j;
        g();
    }

    public final void f(long j) {
        long j2 = this.r + (this.s * 3);
        if (this.m > j2) {
            float y0 = (float) androidx.media3.common.util.b.y0(this.c);
            this.m = l22.b(j2, this.j, this.m - (((this.p - 1.0f) * y0) + ((this.n - 1.0f) * y0)));
            return;
        }
        long r = androidx.media3.common.util.b.r(j - (Math.max((float) Utils.FLOAT_EPSILON, this.p - 1.0f) / this.d), this.m, j2);
        this.m = r;
        long j3 = this.l;
        if (j3 == CellBase.ID_SYSTEM_MESSAGE_REQUEST_CLOSED || r <= j3) {
            return;
        }
        this.m = j3;
    }

    public final void g() {
        long j = this.h;
        if (j != CellBase.ID_SYSTEM_MESSAGE_REQUEST_CLOSED) {
            long j2 = this.i;
            if (j2 != CellBase.ID_SYSTEM_MESSAGE_REQUEST_CLOSED) {
                j = j2;
            }
            long j3 = this.k;
            if (j3 != CellBase.ID_SYSTEM_MESSAGE_REQUEST_CLOSED && j < j3) {
                j = j3;
            }
            long j4 = this.l;
            if (j4 != CellBase.ID_SYSTEM_MESSAGE_REQUEST_CLOSED && j > j4) {
                j = j4;
            }
        } else {
            j = -9223372036854775807L;
        }
        if (this.j == j) {
            return;
        }
        this.j = j;
        this.m = j;
        this.r = CellBase.ID_SYSTEM_MESSAGE_REQUEST_CLOSED;
        this.s = CellBase.ID_SYSTEM_MESSAGE_REQUEST_CLOSED;
        this.q = CellBase.ID_SYSTEM_MESSAGE_REQUEST_CLOSED;
    }

    public final void i(long j, long j2) {
        long j3 = j - j2;
        long j4 = this.r;
        if (j4 == CellBase.ID_SYSTEM_MESSAGE_REQUEST_CLOSED) {
            this.r = j3;
            this.s = 0L;
            return;
        }
        long max = Math.max(j3, h(j4, j3, this.g));
        this.r = max;
        this.s = h(this.s, Math.abs(j3 - max), this.g);
    }

    public d(float f, float f2, long j, float f3, long j2, long j3, float f4) {
        this.a = f;
        this.b = f2;
        this.c = j;
        this.d = f3;
        this.e = j2;
        this.f = j3;
        this.g = f4;
        this.h = CellBase.ID_SYSTEM_MESSAGE_REQUEST_CLOSED;
        this.i = CellBase.ID_SYSTEM_MESSAGE_REQUEST_CLOSED;
        this.k = CellBase.ID_SYSTEM_MESSAGE_REQUEST_CLOSED;
        this.l = CellBase.ID_SYSTEM_MESSAGE_REQUEST_CLOSED;
        this.o = f;
        this.n = f2;
        this.p = 1.0f;
        this.q = CellBase.ID_SYSTEM_MESSAGE_REQUEST_CLOSED;
        this.j = CellBase.ID_SYSTEM_MESSAGE_REQUEST_CLOSED;
        this.m = CellBase.ID_SYSTEM_MESSAGE_REQUEST_CLOSED;
        this.r = CellBase.ID_SYSTEM_MESSAGE_REQUEST_CLOSED;
        this.s = CellBase.ID_SYSTEM_MESSAGE_REQUEST_CLOSED;
    }
}
