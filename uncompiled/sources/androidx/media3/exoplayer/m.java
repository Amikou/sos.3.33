package androidx.media3.exoplayer;

import androidx.media3.exoplayer.k;
import androidx.media3.exoplayer.source.r;
import java.io.IOException;

/* compiled from: Renderer.java */
/* loaded from: classes.dex */
public interface m extends k.b {

    /* compiled from: Renderer.java */
    /* loaded from: classes.dex */
    public interface a {
        void a();

        void b();
    }

    r c();

    boolean d();

    boolean f();

    void g();

    String getName();

    int getState();

    int h();

    boolean i();

    void j(v63 v63Var, androidx.media3.common.j[] jVarArr, r rVar, long j, boolean z, boolean z2, long j2, long j3) throws ExoPlaybackException;

    void k();

    void l(androidx.media3.common.j[] jVarArr, r rVar, long j, long j2) throws ExoPlaybackException;

    n m();

    void o(float f, float f2) throws ExoPlaybackException;

    void p(int i, ks2 ks2Var);

    void r(long j, long j2) throws ExoPlaybackException;

    void reset();

    void start() throws ExoPlaybackException;

    void stop();

    void t() throws IOException;

    long u();

    void v(long j) throws ExoPlaybackException;

    boolean w();

    u52 x();
}
