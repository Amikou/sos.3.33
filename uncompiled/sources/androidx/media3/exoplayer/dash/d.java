package androidx.media3.exoplayer.dash;

import androidx.media3.common.j;
import androidx.media3.decoder.DecoderInputBuffer;
import androidx.media3.exoplayer.source.r;
import java.io.IOException;
import zendesk.support.request.CellBase;

/* compiled from: EventSampleStream.java */
/* loaded from: classes.dex */
public final class d implements r {
    public final j a;
    public long[] g0;
    public boolean h0;
    public jy0 i0;
    public boolean j0;
    public int k0;
    public final androidx.media3.extractor.metadata.emsg.a f0 = new androidx.media3.extractor.metadata.emsg.a();
    public long l0 = CellBase.ID_SYSTEM_MESSAGE_REQUEST_CLOSED;

    public d(jy0 jy0Var, j jVar, boolean z) {
        this.a = jVar;
        this.i0 = jy0Var;
        this.g0 = jy0Var.b;
        d(jy0Var, z);
    }

    public String a() {
        return this.i0.a();
    }

    @Override // androidx.media3.exoplayer.source.r
    public void b() throws IOException {
    }

    public void c(long j) {
        boolean z = true;
        int e = androidx.media3.common.util.b.e(this.g0, j, true, false);
        this.k0 = e;
        if (!this.h0 || e != this.g0.length) {
            z = false;
        }
        if (!z) {
            j = CellBase.ID_SYSTEM_MESSAGE_REQUEST_CLOSED;
        }
        this.l0 = j;
    }

    public void d(jy0 jy0Var, boolean z) {
        int i = this.k0;
        long j = i == 0 ? -9223372036854775807L : this.g0[i - 1];
        this.h0 = z;
        this.i0 = jy0Var;
        long[] jArr = jy0Var.b;
        this.g0 = jArr;
        long j2 = this.l0;
        if (j2 != CellBase.ID_SYSTEM_MESSAGE_REQUEST_CLOSED) {
            c(j2);
        } else if (j != CellBase.ID_SYSTEM_MESSAGE_REQUEST_CLOSED) {
            this.k0 = androidx.media3.common.util.b.e(jArr, j, false, false);
        }
    }

    @Override // androidx.media3.exoplayer.source.r
    public boolean f() {
        return true;
    }

    @Override // androidx.media3.exoplayer.source.r
    public int m(long j) {
        int max = Math.max(this.k0, androidx.media3.common.util.b.e(this.g0, j, true, false));
        int i = max - this.k0;
        this.k0 = max;
        return i;
    }

    @Override // androidx.media3.exoplayer.source.r
    public int p(y81 y81Var, DecoderInputBuffer decoderInputBuffer, int i) {
        int i2 = this.k0;
        boolean z = i2 == this.g0.length;
        if (z && !this.h0) {
            decoderInputBuffer.t(4);
            return -4;
        } else if ((i & 2) != 0 || !this.j0) {
            y81Var.b = this.a;
            this.j0 = true;
            return -5;
        } else if (z) {
            return -3;
        } else {
            if ((i & 1) == 0) {
                this.k0 = i2 + 1;
            }
            if ((i & 4) == 0) {
                byte[] a = this.f0.a(this.i0.a[i2]);
                decoderInputBuffer.v(a.length);
                decoderInputBuffer.g0.put(a);
            }
            decoderInputBuffer.i0 = this.g0[i2];
            decoderInputBuffer.t(1);
            return -4;
        }
    }
}
