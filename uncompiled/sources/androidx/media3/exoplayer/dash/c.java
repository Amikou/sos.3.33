package androidx.media3.exoplayer.dash;

import android.os.SystemClock;
import androidx.media3.common.j;
import androidx.media3.datasource.HttpDataSource$InvalidResponseCodeException;
import androidx.media3.datasource.b;
import androidx.media3.exoplayer.dash.a;
import androidx.media3.exoplayer.dash.e;
import androidx.media3.exoplayer.source.BehindLiveWindowException;
import androidx.media3.exoplayer.source.chunk.MediaChunkIterator;
import androidx.media3.exoplayer.upstream.b;
import defpackage.ny;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import zendesk.support.request.CellBase;

/* compiled from: DefaultDashChunkSource.java */
/* loaded from: classes.dex */
public class c implements androidx.media3.exoplayer.dash.a {
    public final androidx.media3.exoplayer.upstream.c a;
    public final Cdo b;
    public final int[] c;
    public final int d;
    public final androidx.media3.datasource.b e;
    public final long f;
    public final int g;
    public final e.c h;
    public final b[] i;
    public androidx.media3.exoplayer.trackselection.c j;
    public sd0 k;
    public int l;
    public IOException m;
    public boolean n;

    /* compiled from: DefaultDashChunkSource.java */
    /* loaded from: classes.dex */
    public static final class a implements a.InterfaceC0033a {
        public final b.a a;
        public final int b;
        public final ny.a c;

        public a(b.a aVar) {
            this(aVar, 1);
        }

        @Override // androidx.media3.exoplayer.dash.a.InterfaceC0033a
        public androidx.media3.exoplayer.dash.a a(androidx.media3.exoplayer.upstream.c cVar, sd0 sd0Var, Cdo cdo, int i, int[] iArr, androidx.media3.exoplayer.trackselection.c cVar2, int i2, long j, boolean z, List<j> list, e.c cVar3, fa4 fa4Var, ks2 ks2Var) {
            androidx.media3.datasource.b a = this.a.a();
            if (fa4Var != null) {
                a.c(fa4Var);
            }
            return new c(this.c, cVar, sd0Var, cdo, i, iArr, cVar2, i2, a, j, this.b, z, list, cVar3, ks2Var);
        }

        public a(b.a aVar, int i) {
            this(ks.n0, aVar, i);
        }

        public a(ny.a aVar, b.a aVar2, int i) {
            this.c = aVar;
            this.a = aVar2;
            this.b = i;
        }
    }

    /* compiled from: DefaultDashChunkSource.java */
    /* loaded from: classes.dex */
    public static final class b {
        public final ny a;
        public final b73 b;
        public final bo c;
        public final wd0 d;
        public final long e;
        public final long f;

        public b(long j, b73 b73Var, bo boVar, ny nyVar, long j2, wd0 wd0Var) {
            this.e = j;
            this.b = b73Var;
            this.c = boVar;
            this.f = j2;
            this.a = nyVar;
            this.d = wd0Var;
        }

        public b b(long j, b73 b73Var) throws BehindLiveWindowException {
            long g;
            long g2;
            wd0 l = this.b.l();
            wd0 l2 = b73Var.l();
            if (l == null) {
                return new b(j, b73Var, this.c, this.a, this.f, l);
            }
            if (!l.h()) {
                return new b(j, b73Var, this.c, this.a, this.f, l2);
            }
            long j2 = l.j(j);
            if (j2 == 0) {
                return new b(j, b73Var, this.c, this.a, this.f, l2);
            }
            long i = l.i();
            long b = l.b(i);
            long j3 = (j2 + i) - 1;
            long i2 = l2.i();
            long b2 = l2.b(i2);
            long j4 = this.f;
            int i3 = ((l.b(j3) + l.c(j3, j)) > b2 ? 1 : ((l.b(j3) + l.c(j3, j)) == b2 ? 0 : -1));
            if (i3 == 0) {
                g = j3 + 1;
            } else if (i3 < 0) {
                throw new BehindLiveWindowException();
            } else {
                if (b2 < b) {
                    g2 = j4 - (l2.g(b, j) - i);
                    return new b(j, b73Var, this.c, this.a, g2, l2);
                }
                g = l.g(b2, j);
            }
            g2 = j4 + (g - i2);
            return new b(j, b73Var, this.c, this.a, g2, l2);
        }

        public b c(wd0 wd0Var) {
            return new b(this.e, this.b, this.c, this.a, this.f, wd0Var);
        }

        public b d(bo boVar) {
            return new b(this.e, this.b, boVar, this.a, this.f, this.d);
        }

        public long e(long j) {
            return this.d.d(this.e, j) + this.f;
        }

        public long f() {
            return this.d.i() + this.f;
        }

        public long g(long j) {
            return (e(j) + this.d.k(this.e, j)) - 1;
        }

        public long h() {
            return this.d.j(this.e);
        }

        public long i(long j) {
            return k(j) + this.d.c(j - this.f, this.e);
        }

        public long j(long j) {
            return this.d.g(j, this.e) + this.f;
        }

        public long k(long j) {
            return this.d.b(j - this.f);
        }

        public s33 l(long j) {
            return this.d.f(j - this.f);
        }

        public boolean m(long j, long j2) {
            return this.d.h() || j2 == CellBase.ID_SYSTEM_MESSAGE_REQUEST_CLOSED || i(j) <= j2;
        }
    }

    /* compiled from: DefaultDashChunkSource.java */
    /* renamed from: androidx.media3.exoplayer.dash.c$c  reason: collision with other inner class name */
    /* loaded from: classes.dex */
    public static final class C0034c extends mn {
        public final b e;

        public C0034c(b bVar, long j, long j2, long j3) {
            super(j, j2);
            this.e = bVar;
        }

        @Override // defpackage.t52
        public long a() {
            c();
            return this.e.k(d());
        }

        @Override // defpackage.t52
        public long b() {
            c();
            return this.e.i(d());
        }
    }

    public c(ny.a aVar, androidx.media3.exoplayer.upstream.c cVar, sd0 sd0Var, Cdo cdo, int i, int[] iArr, androidx.media3.exoplayer.trackselection.c cVar2, int i2, androidx.media3.datasource.b bVar, long j, int i3, boolean z, List<j> list, e.c cVar3, ks2 ks2Var) {
        this.a = cVar;
        this.k = sd0Var;
        this.b = cdo;
        this.c = iArr;
        this.j = cVar2;
        this.d = i2;
        this.e = bVar;
        this.l = i;
        this.f = j;
        this.g = i3;
        this.h = cVar3;
        long g = sd0Var.g(i);
        ArrayList<b73> n = n();
        this.i = new b[cVar2.length()];
        int i4 = 0;
        while (i4 < this.i.length) {
            b73 b73Var = n.get(cVar2.l(i4));
            bo j2 = cdo.j(b73Var.b);
            b[] bVarArr = this.i;
            if (j2 == null) {
                j2 = b73Var.b.get(0);
            }
            int i5 = i4;
            bVarArr[i5] = new b(g, b73Var, j2, aVar.a(i2, b73Var.a, z, list, cVar3, ks2Var), 0L, b73Var.l());
            i4 = i5 + 1;
        }
    }

    @Override // defpackage.sy
    public void a() {
        for (b bVar : this.i) {
            ny nyVar = bVar.a;
            if (nyVar != null) {
                nyVar.a();
            }
        }
    }

    @Override // defpackage.sy
    public void b() throws IOException {
        IOException iOException = this.m;
        if (iOException == null) {
            this.a.b();
            return;
        }
        throw iOException;
    }

    @Override // defpackage.sy
    public long c(long j, xi3 xi3Var) {
        b[] bVarArr;
        for (b bVar : this.i) {
            if (bVar.d != null) {
                long j2 = bVar.j(j);
                long k = bVar.k(j2);
                long h = bVar.h();
                return xi3Var.a(j, k, (k >= j || (h != -1 && j2 >= (bVar.f() + h) - 1)) ? k : bVar.k(j2 + 1));
            }
        }
        return j;
    }

    @Override // defpackage.sy
    public void d(my myVar) {
        py e;
        if (myVar instanceof pq1) {
            int b2 = this.j.b(((pq1) myVar).d);
            b bVar = this.i[b2];
            if (bVar.d == null && (e = bVar.a.e()) != null) {
                this.i[b2] = bVar.c(new yd0(e, bVar.b.c));
            }
        }
        e.c cVar = this.h;
        if (cVar != null) {
            cVar.i(myVar);
        }
    }

    @Override // defpackage.sy
    public boolean e(long j, my myVar, List<? extends s52> list) {
        if (this.m != null) {
            return false;
        }
        return this.j.e(j, myVar, list);
    }

    @Override // defpackage.sy
    public void f(long j, long j2, List<? extends s52> list, oy oyVar) {
        int i;
        int i2;
        t52[] t52VarArr;
        long j3;
        long j4;
        if (this.m != null) {
            return;
        }
        long j5 = j2 - j;
        long y0 = androidx.media3.common.util.b.y0(this.k.a) + androidx.media3.common.util.b.y0(this.k.d(this.l).b) + j2;
        e.c cVar = this.h;
        if (cVar == null || !cVar.h(y0)) {
            long y02 = androidx.media3.common.util.b.y0(androidx.media3.common.util.b.X(this.f));
            long m = m(y02);
            s52 s52Var = list.isEmpty() ? null : list.get(list.size() - 1);
            int length = this.j.length();
            MediaChunkIterator[] mediaChunkIteratorArr = new t52[length];
            int i3 = 0;
            while (i3 < length) {
                b bVar = this.i[i3];
                if (bVar.d == null) {
                    mediaChunkIteratorArr[i3] = t52.a;
                    i = i3;
                    i2 = length;
                    t52VarArr = mediaChunkIteratorArr;
                    j3 = j5;
                    j4 = y02;
                } else {
                    long e = bVar.e(y02);
                    long g = bVar.g(y02);
                    i = i3;
                    i2 = length;
                    t52VarArr = mediaChunkIteratorArr;
                    j3 = j5;
                    j4 = y02;
                    long o = o(bVar, s52Var, j2, e, g);
                    if (o < e) {
                        t52VarArr[i] = t52.a;
                    } else {
                        t52VarArr[i] = new C0034c(r(i), o, g, m);
                    }
                }
                i3 = i + 1;
                y02 = j4;
                mediaChunkIteratorArr = t52VarArr;
                length = i2;
                j5 = j3;
            }
            long j6 = j5;
            long j7 = y02;
            this.j.a(j, j6, l(j7, j), list, mediaChunkIteratorArr);
            b r = r(this.j.d());
            ny nyVar = r.a;
            if (nyVar != null) {
                b73 b73Var = r.b;
                s33 n = nyVar.d() == null ? b73Var.n() : null;
                s33 m2 = r.d == null ? b73Var.m() : null;
                if (n != null || m2 != null) {
                    oyVar.a = p(r, this.e, this.j.n(), this.j.o(), this.j.q(), n, m2);
                    return;
                }
            }
            long j8 = r.e;
            long j9 = CellBase.ID_SYSTEM_MESSAGE_REQUEST_CLOSED;
            int i4 = (j8 > CellBase.ID_SYSTEM_MESSAGE_REQUEST_CLOSED ? 1 : (j8 == CellBase.ID_SYSTEM_MESSAGE_REQUEST_CLOSED ? 0 : -1));
            boolean z = i4 != 0;
            if (r.h() == 0) {
                oyVar.b = z;
                return;
            }
            long e2 = r.e(j7);
            long g2 = r.g(j7);
            long o2 = o(r, s52Var, j2, e2, g2);
            if (o2 < e2) {
                this.m = new BehindLiveWindowException();
                return;
            }
            int i5 = (o2 > g2 ? 1 : (o2 == g2 ? 0 : -1));
            if (i5 <= 0 && (!this.n || i5 < 0)) {
                if (z && r.k(o2) >= j8) {
                    oyVar.b = true;
                    return;
                }
                int min = (int) Math.min(this.g, (g2 - o2) + 1);
                if (i4 != 0) {
                    while (min > 1 && r.k((min + o2) - 1) >= j8) {
                        min--;
                    }
                }
                int i6 = min;
                if (list.isEmpty()) {
                    j9 = j2;
                }
                oyVar.a = q(r, this.e, this.d, this.j.n(), this.j.o(), this.j.q(), o2, i6, j9, m);
                return;
            }
            oyVar.b = z;
        }
    }

    @Override // defpackage.sy
    public boolean g(my myVar, boolean z, b.c cVar, androidx.media3.exoplayer.upstream.b bVar) {
        b.C0042b d;
        if (z) {
            e.c cVar2 = this.h;
            if (cVar2 == null || !cVar2.j(myVar)) {
                if (!this.k.d && (myVar instanceof s52)) {
                    IOException iOException = cVar.a;
                    if ((iOException instanceof HttpDataSource$InvalidResponseCodeException) && ((HttpDataSource$InvalidResponseCodeException) iOException).responseCode == 404) {
                        b bVar2 = this.i[this.j.b(myVar.d)];
                        long h = bVar2.h();
                        if (h != -1 && h != 0) {
                            if (((s52) myVar).f() > (bVar2.f() + h) - 1) {
                                this.n = true;
                                return true;
                            }
                        }
                    }
                }
                b bVar3 = this.i[this.j.b(myVar.d)];
                bo j = this.b.j(bVar3.b.b);
                if (j == null || bVar3.c.equals(j)) {
                    b.a k = k(this.j, bVar3.b.b);
                    if ((k.a(2) || k.a(1)) && (d = bVar.d(k, cVar)) != null && k.a(d.a)) {
                        int i = d.a;
                        if (i == 2) {
                            androidx.media3.exoplayer.trackselection.c cVar3 = this.j;
                            return cVar3.f(cVar3.b(myVar.d), d.b);
                        } else if (i == 1) {
                            this.b.e(bVar3.c, d.b);
                            return true;
                        } else {
                            return false;
                        }
                    }
                    return false;
                }
                return true;
            }
            return true;
        }
        return false;
    }

    @Override // androidx.media3.exoplayer.dash.a
    public void h(androidx.media3.exoplayer.trackselection.c cVar) {
        this.j = cVar;
    }

    @Override // androidx.media3.exoplayer.dash.a
    public void i(sd0 sd0Var, int i) {
        try {
            this.k = sd0Var;
            this.l = i;
            long g = sd0Var.g(i);
            ArrayList<b73> n = n();
            for (int i2 = 0; i2 < this.i.length; i2++) {
                b[] bVarArr = this.i;
                bVarArr[i2] = bVarArr[i2].b(g, n.get(this.j.l(i2)));
            }
        } catch (BehindLiveWindowException e) {
            this.m = e;
        }
    }

    @Override // defpackage.sy
    public int j(long j, List<? extends s52> list) {
        if (this.m == null && this.j.length() >= 2) {
            return this.j.m(j, list);
        }
        return list.size();
    }

    public final b.a k(androidx.media3.exoplayer.trackselection.c cVar, List<bo> list) {
        long elapsedRealtime = SystemClock.elapsedRealtime();
        int length = cVar.length();
        int i = 0;
        for (int i2 = 0; i2 < length; i2++) {
            if (cVar.h(i2, elapsedRealtime)) {
                i++;
            }
        }
        int f = Cdo.f(list);
        return new b.a(f, f - this.b.g(list), length, i);
    }

    public final long l(long j, long j2) {
        if (this.k.d) {
            return Math.max(0L, Math.min(m(j), this.i[0].i(this.i[0].g(j))) - j2);
        }
        return CellBase.ID_SYSTEM_MESSAGE_REQUEST_CLOSED;
    }

    public final long m(long j) {
        sd0 sd0Var = this.k;
        long j2 = sd0Var.a;
        return j2 == CellBase.ID_SYSTEM_MESSAGE_REQUEST_CLOSED ? CellBase.ID_SYSTEM_MESSAGE_REQUEST_CLOSED : j - androidx.media3.common.util.b.y0(j2 + sd0Var.d(this.l).b);
    }

    public final ArrayList<b73> n() {
        List<i8> list = this.k.d(this.l).c;
        ArrayList<b73> arrayList = new ArrayList<>();
        for (int i : this.c) {
            arrayList.addAll(list.get(i).c);
        }
        return arrayList;
    }

    public final long o(b bVar, s52 s52Var, long j, long j2, long j3) {
        if (s52Var != null) {
            return s52Var.f();
        }
        return androidx.media3.common.util.b.r(bVar.j(j), j2, j3);
    }

    public my p(b bVar, androidx.media3.datasource.b bVar2, j jVar, int i, Object obj, s33 s33Var, s33 s33Var2) {
        s33 s33Var3 = s33Var;
        b73 b73Var = bVar.b;
        if (s33Var3 != null) {
            s33 a2 = s33Var3.a(s33Var2, bVar.c.a);
            if (a2 != null) {
                s33Var3 = a2;
            }
        } else {
            s33Var3 = s33Var2;
        }
        return new pq1(bVar2, xd0.a(b73Var, bVar.c.a, s33Var3, 0), jVar, i, obj, bVar.a);
    }

    public my q(b bVar, androidx.media3.datasource.b bVar2, int i, j jVar, int i2, Object obj, long j, int i3, long j2, long j3) {
        b73 b73Var = bVar.b;
        long k = bVar.k(j);
        s33 l = bVar.l(j);
        if (bVar.a == null) {
            return new wp3(bVar2, xd0.a(b73Var, bVar.c.a, l, bVar.m(j, j3) ? 0 : 8), jVar, i2, obj, k, bVar.i(j), j, i, jVar);
        }
        int i4 = 1;
        int i5 = 1;
        while (i4 < i3) {
            s33 a2 = l.a(bVar.l(i4 + j), bVar.c.a);
            if (a2 == null) {
                break;
            }
            i5++;
            i4++;
            l = a2;
        }
        long j4 = (i5 + j) - 1;
        long i6 = bVar.i(j4);
        long j5 = bVar.e;
        return new d70(bVar2, xd0.a(b73Var, bVar.c.a, l, bVar.m(j4, j3) ? 0 : 8), jVar, i2, obj, k, i6, j2, (j5 == CellBase.ID_SYSTEM_MESSAGE_REQUEST_CLOSED || j5 > i6) ? -9223372036854775807L : j5, j, i5, -b73Var.c, bVar.a);
    }

    public final b r(int i) {
        b bVar = this.i[i];
        bo j = this.b.j(bVar.b.b);
        if (j == null || j.equals(bVar.c)) {
            return bVar;
        }
        b d = bVar.d(j);
        this.i[i] = d;
        return d;
    }
}
