package androidx.media3.exoplayer.dash;

import androidx.media3.common.j;
import androidx.media3.exoplayer.dash.e;
import java.util.List;

/* compiled from: DashChunkSource.java */
/* loaded from: classes.dex */
public interface a extends sy {

    /* compiled from: DashChunkSource.java */
    /* renamed from: androidx.media3.exoplayer.dash.a$a  reason: collision with other inner class name */
    /* loaded from: classes.dex */
    public interface InterfaceC0033a {
        a a(androidx.media3.exoplayer.upstream.c cVar, sd0 sd0Var, Cdo cdo, int i, int[] iArr, androidx.media3.exoplayer.trackselection.c cVar2, int i2, long j, boolean z, List<j> list, e.c cVar3, fa4 fa4Var, ks2 ks2Var);
    }

    void h(androidx.media3.exoplayer.trackselection.c cVar);

    void i(sd0 sd0Var, int i);
}
