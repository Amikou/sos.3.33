package androidx.media3.exoplayer.dash;

import android.util.Pair;
import android.util.SparseArray;
import android.util.SparseIntArray;
import androidx.media3.common.j;
import androidx.media3.common.v;
import androidx.media3.exoplayer.dash.a;
import androidx.media3.exoplayer.dash.e;
import androidx.media3.exoplayer.drm.b;
import androidx.media3.exoplayer.source.chunk.ChunkSampleStream;
import androidx.media3.exoplayer.source.i;
import androidx.media3.exoplayer.source.k;
import androidx.media3.exoplayer.source.r;
import androidx.media3.exoplayer.source.s;
import com.google.common.primitives.Ints;
import defpackage.ry;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.IdentityHashMap;
import java.util.Iterator;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import zendesk.support.request.CellBase;

/* compiled from: DashMediaPeriod.java */
/* loaded from: classes.dex */
public final class b implements i, s.a<ry<androidx.media3.exoplayer.dash.a>>, ry.b<androidx.media3.exoplayer.dash.a> {
    public static final Pattern C0 = Pattern.compile("CC([1-4])=(.+)");
    public static final Pattern D0 = Pattern.compile("([1-4])=lang:(\\w+)(,.+)?");
    public int A0;
    public List<jy0> B0;
    public final int a;
    public final a.InterfaceC0033a f0;
    public final fa4 g0;
    public final androidx.media3.exoplayer.drm.c h0;
    public final androidx.media3.exoplayer.upstream.b i0;
    public final Cdo j0;
    public final long k0;
    public final androidx.media3.exoplayer.upstream.c l0;
    public final gb m0;
    public final c84 n0;
    public final a[] o0;
    public final q40 p0;
    public final e q0;
    public final k.a s0;
    public final b.a t0;
    public final ks2 u0;
    public i.a v0;
    public s y0;
    public sd0 z0;
    public ChunkSampleStream<androidx.media3.exoplayer.dash.a>[] w0 = D(0);
    public d[] x0 = new d[0];
    public final IdentityHashMap<ry<androidx.media3.exoplayer.dash.a>, e.c> r0 = new IdentityHashMap<>();

    /* compiled from: DashMediaPeriod.java */
    /* loaded from: classes.dex */
    public static final class a {
        public final int[] a;
        public final int b;
        public final int c;
        public final int d;
        public final int e;
        public final int f;
        public final int g;

        public a(int i, int i2, int[] iArr, int i3, int i4, int i5, int i6) {
            this.b = i;
            this.a = iArr;
            this.c = i2;
            this.e = i3;
            this.f = i4;
            this.g = i5;
            this.d = i6;
        }

        public static a a(int[] iArr, int i) {
            return new a(3, 1, iArr, i, -1, -1, -1);
        }

        public static a b(int[] iArr, int i) {
            return new a(5, 1, iArr, i, -1, -1, -1);
        }

        public static a c(int i) {
            return new a(5, 2, new int[0], -1, -1, -1, i);
        }

        public static a d(int i, int[] iArr, int i2, int i3, int i4) {
            return new a(i, 0, iArr, i2, i3, i4, -1);
        }
    }

    public b(int i, sd0 sd0Var, Cdo cdo, int i2, a.InterfaceC0033a interfaceC0033a, fa4 fa4Var, androidx.media3.exoplayer.drm.c cVar, b.a aVar, androidx.media3.exoplayer.upstream.b bVar, k.a aVar2, long j, androidx.media3.exoplayer.upstream.c cVar2, gb gbVar, q40 q40Var, e.b bVar2, ks2 ks2Var) {
        this.a = i;
        this.z0 = sd0Var;
        this.j0 = cdo;
        this.A0 = i2;
        this.f0 = interfaceC0033a;
        this.g0 = fa4Var;
        this.h0 = cVar;
        this.t0 = aVar;
        this.i0 = bVar;
        this.s0 = aVar2;
        this.k0 = j;
        this.l0 = cVar2;
        this.m0 = gbVar;
        this.p0 = q40Var;
        this.u0 = ks2Var;
        this.q0 = new e(sd0Var, bVar2, gbVar);
        this.y0 = q40Var.a(this.w0);
        jq2 d = sd0Var.d(i2);
        List<jy0> list = d.d;
        this.B0 = list;
        Pair<c84, a[]> s = s(cVar, d.c, list);
        this.n0 = (c84) s.first;
        this.o0 = (a[]) s.second;
    }

    public static boolean B(List<i8> list, int[] iArr) {
        for (int i : iArr) {
            List<b73> list2 = list.get(i).c;
            for (int i2 = 0; i2 < list2.size(); i2++) {
                if (!list2.get(i2).d.isEmpty()) {
                    return true;
                }
            }
        }
        return false;
    }

    public static int C(int i, List<i8> list, int[][] iArr, boolean[] zArr, j[][] jVarArr) {
        int i2 = 0;
        for (int i3 = 0; i3 < i; i3++) {
            if (B(list, iArr[i3])) {
                zArr[i3] = true;
                i2++;
            }
            jVarArr[i3] = x(list, iArr[i3]);
            if (jVarArr[i3].length != 0) {
                i2++;
            }
        }
        return i2;
    }

    public static ChunkSampleStream<androidx.media3.exoplayer.dash.a>[] D(int i) {
        return new ry[i];
    }

    public static j[] F(nm0 nm0Var, Pattern pattern, j jVar) {
        String str = nm0Var.b;
        if (str == null) {
            return new j[]{jVar};
        }
        String[] L0 = androidx.media3.common.util.b.L0(str, ";");
        j[] jVarArr = new j[L0.length];
        for (int i = 0; i < L0.length; i++) {
            Matcher matcher = pattern.matcher(L0[i]);
            if (!matcher.matches()) {
                return new j[]{jVar};
            }
            int parseInt = Integer.parseInt(matcher.group(1));
            jVarArr[i] = jVar.b().S(jVar.a + ":" + parseInt).F(parseInt).V(matcher.group(2)).E();
        }
        return jVarArr;
    }

    public static void j(List<jy0> list, v[] vVarArr, a[] aVarArr, int i) {
        jy0 jy0Var;
        int i2 = 0;
        while (i2 < list.size()) {
            vVarArr[i] = new v(jy0Var.a() + ":" + i2, new j.b().S(list.get(i2).a()).e0("application/x-emsg").E());
            aVarArr[i] = a.c(i2);
            i2++;
            i++;
        }
    }

    public static int m(androidx.media3.exoplayer.drm.c cVar, List<i8> list, int[][] iArr, int i, boolean[] zArr, j[][] jVarArr, v[] vVarArr, a[] aVarArr) {
        int i2;
        int i3;
        int i4 = 0;
        int i5 = 0;
        while (i4 < i) {
            int[] iArr2 = iArr[i4];
            ArrayList arrayList = new ArrayList();
            for (int i6 : iArr2) {
                arrayList.addAll(list.get(i6).c);
            }
            int size = arrayList.size();
            j[] jVarArr2 = new j[size];
            for (int i7 = 0; i7 < size; i7++) {
                j jVar = ((b73) arrayList.get(i7)).a;
                jVarArr2[i7] = jVar.c(cVar.e(jVar));
            }
            i8 i8Var = list.get(iArr2[0]);
            int i8 = i8Var.a;
            String num = i8 != -1 ? Integer.toString(i8) : "unset:" + i4;
            int i9 = i5 + 1;
            if (zArr[i4]) {
                i2 = i9 + 1;
            } else {
                i2 = i9;
                i9 = -1;
            }
            if (jVarArr[i4].length != 0) {
                i3 = i2 + 1;
            } else {
                i3 = i2;
                i2 = -1;
            }
            vVarArr[i5] = new v(num, jVarArr2);
            aVarArr[i5] = a.d(i8Var.b, iArr2, i5, i9, i2);
            if (i9 != -1) {
                String str = num + ":emsg";
                vVarArr[i9] = new v(str, new j.b().S(str).e0("application/x-emsg").E());
                aVarArr[i9] = a.b(iArr2, i5);
            }
            if (i2 != -1) {
                vVarArr[i2] = new v(num + ":cc", jVarArr[i4]);
                aVarArr[i2] = a.a(iArr2, i5);
            }
            i4++;
            i5 = i3;
        }
        return i5;
    }

    public static Pair<c84, a[]> s(androidx.media3.exoplayer.drm.c cVar, List<i8> list, List<jy0> list2) {
        int[][] y = y(list);
        int length = y.length;
        boolean[] zArr = new boolean[length];
        j[][] jVarArr = new j[length];
        int C = C(length, list, y, zArr, jVarArr) + length + list2.size();
        v[] vVarArr = new v[C];
        a[] aVarArr = new a[C];
        j(list2, vVarArr, aVarArr, m(cVar, list, y, length, zArr, jVarArr, vVarArr, aVarArr));
        return Pair.create(new c84(vVarArr), aVarArr);
    }

    public static nm0 u(List<nm0> list) {
        return v(list, "urn:mpeg:dash:adaptation-set-switching:2016");
    }

    public static nm0 v(List<nm0> list, String str) {
        for (int i = 0; i < list.size(); i++) {
            nm0 nm0Var = list.get(i);
            if (str.equals(nm0Var.a)) {
                return nm0Var;
            }
        }
        return null;
    }

    public static nm0 w(List<nm0> list) {
        return v(list, "http://dashif.org/guidelines/trickmode");
    }

    public static j[] x(List<i8> list, int[] iArr) {
        for (int i : iArr) {
            i8 i8Var = list.get(i);
            List<nm0> list2 = list.get(i).d;
            for (int i2 = 0; i2 < list2.size(); i2++) {
                nm0 nm0Var = list2.get(i2);
                if ("urn:scte:dash:cc:cea-608:2015".equals(nm0Var.a)) {
                    return F(nm0Var, C0, new j.b().e0("application/cea-608").S(i8Var.a + ":cea608").E());
                } else if ("urn:scte:dash:cc:cea-708:2015".equals(nm0Var.a)) {
                    return F(nm0Var, D0, new j.b().e0("application/cea-708").S(i8Var.a + ":cea708").E());
                }
            }
        }
        return new j[0];
    }

    public static int[][] y(List<i8> list) {
        int i;
        nm0 u;
        int size = list.size();
        SparseIntArray sparseIntArray = new SparseIntArray(size);
        ArrayList arrayList = new ArrayList(size);
        SparseArray sparseArray = new SparseArray(size);
        for (int i2 = 0; i2 < size; i2++) {
            sparseIntArray.put(list.get(i2).a, i2);
            ArrayList arrayList2 = new ArrayList();
            arrayList2.add(Integer.valueOf(i2));
            arrayList.add(arrayList2);
            sparseArray.put(i2, arrayList2);
        }
        for (int i3 = 0; i3 < size; i3++) {
            i8 i8Var = list.get(i3);
            nm0 w = w(i8Var.e);
            if (w == null) {
                w = w(i8Var.f);
            }
            if (w == null || (i = sparseIntArray.get(Integer.parseInt(w.b), -1)) == -1) {
                i = i3;
            }
            if (i == i3 && (u = u(i8Var.f)) != null) {
                for (String str : androidx.media3.common.util.b.L0(u.b, ",")) {
                    int i4 = sparseIntArray.get(Integer.parseInt(str), -1);
                    if (i4 != -1) {
                        i = Math.min(i, i4);
                    }
                }
            }
            if (i != i3) {
                List list2 = (List) sparseArray.get(i3);
                List list3 = (List) sparseArray.get(i);
                list3.addAll(list2);
                sparseArray.put(i3, list3);
                arrayList.remove(list2);
            }
        }
        int size2 = arrayList.size();
        int[][] iArr = new int[size2];
        for (int i5 = 0; i5 < size2; i5++) {
            iArr[i5] = Ints.k((Collection) arrayList.get(i5));
            Arrays.sort(iArr[i5]);
        }
        return iArr;
    }

    public final int[] A(androidx.media3.exoplayer.trackselection.c[] cVarArr) {
        int[] iArr = new int[cVarArr.length];
        for (int i = 0; i < cVarArr.length; i++) {
            if (cVarArr[i] != null) {
                iArr[i] = this.n0.c(cVarArr[i].c());
            } else {
                iArr[i] = -1;
            }
        }
        return iArr;
    }

    @Override // androidx.media3.exoplayer.source.s.a
    /* renamed from: E */
    public void i(ry<androidx.media3.exoplayer.dash.a> ryVar) {
        this.v0.i(this);
    }

    public void G() {
        this.q0.o();
        for (ry ryVar : this.w0) {
            ryVar.P(this);
        }
        this.v0 = null;
    }

    public final void H(androidx.media3.exoplayer.trackselection.c[] cVarArr, boolean[] zArr, r[] rVarArr) {
        for (int i = 0; i < cVarArr.length; i++) {
            if (cVarArr[i] == null || !zArr[i]) {
                if (rVarArr[i] instanceof ry) {
                    ((ry) rVarArr[i]).P(this);
                } else if (rVarArr[i] instanceof ry.a) {
                    ((ry.a) rVarArr[i]).c();
                }
                rVarArr[i] = null;
            }
        }
    }

    public final void I(androidx.media3.exoplayer.trackselection.c[] cVarArr, r[] rVarArr, int[] iArr) {
        boolean z;
        for (int i = 0; i < cVarArr.length; i++) {
            if ((rVarArr[i] instanceof tu0) || (rVarArr[i] instanceof ry.a)) {
                int z2 = z(i, iArr);
                if (z2 == -1) {
                    z = rVarArr[i] instanceof tu0;
                } else {
                    z = (rVarArr[i] instanceof ry.a) && ((ry.a) rVarArr[i]).a == rVarArr[z2];
                }
                if (!z) {
                    if (rVarArr[i] instanceof ry.a) {
                        ((ry.a) rVarArr[i]).c();
                    }
                    rVarArr[i] = null;
                }
            }
        }
    }

    public final void J(androidx.media3.exoplayer.trackselection.c[] cVarArr, r[] rVarArr, boolean[] zArr, long j, int[] iArr) {
        for (int i = 0; i < cVarArr.length; i++) {
            androidx.media3.exoplayer.trackselection.c cVar = cVarArr[i];
            if (cVar != null) {
                if (rVarArr[i] == null) {
                    zArr[i] = true;
                    a aVar = this.o0[iArr[i]];
                    int i2 = aVar.c;
                    if (i2 == 0) {
                        rVarArr[i] = p(aVar, cVar, j);
                    } else if (i2 == 2) {
                        rVarArr[i] = new d(this.B0.get(aVar.d), cVar.c().c(0), this.z0.d);
                    }
                } else if (rVarArr[i] instanceof ry) {
                    ((androidx.media3.exoplayer.dash.a) ((ry) rVarArr[i]).E()).h(cVar);
                }
            }
        }
        for (int i3 = 0; i3 < cVarArr.length; i3++) {
            if (rVarArr[i3] == null && cVarArr[i3] != null) {
                a aVar2 = this.o0[iArr[i3]];
                if (aVar2.c == 1) {
                    int z = z(i3, iArr);
                    if (z == -1) {
                        rVarArr[i3] = new tu0();
                    } else {
                        rVarArr[i3] = ((ry) rVarArr[z]).S(j, aVar2.b);
                    }
                }
            }
        }
    }

    public void K(sd0 sd0Var, int i) {
        d[] dVarArr;
        this.z0 = sd0Var;
        this.A0 = i;
        this.q0.q(sd0Var);
        ry[] ryVarArr = this.w0;
        if (ryVarArr != null) {
            for (ry ryVar : ryVarArr) {
                ((androidx.media3.exoplayer.dash.a) ryVar.E()).i(sd0Var, i);
            }
            this.v0.i(this);
        }
        this.B0 = sd0Var.d(i).d;
        for (d dVar : this.x0) {
            Iterator<jy0> it = this.B0.iterator();
            while (true) {
                if (it.hasNext()) {
                    jy0 next = it.next();
                    if (next.a().equals(dVar.a())) {
                        boolean z = true;
                        dVar.d(next, (sd0Var.d && i == sd0Var.e() - 1) ? false : false);
                    }
                }
            }
        }
    }

    @Override // androidx.media3.exoplayer.source.i, androidx.media3.exoplayer.source.s
    public long a() {
        return this.y0.a();
    }

    @Override // defpackage.ry.b
    public synchronized void b(ry<androidx.media3.exoplayer.dash.a> ryVar) {
        e.c remove = this.r0.remove(ryVar);
        if (remove != null) {
            remove.n();
        }
    }

    @Override // androidx.media3.exoplayer.source.i
    public long c(long j, xi3 xi3Var) {
        ry[] ryVarArr;
        for (ry ryVar : this.w0) {
            if (ryVar.a == 2) {
                return ryVar.c(j, xi3Var);
            }
        }
        return j;
    }

    @Override // androidx.media3.exoplayer.source.i, androidx.media3.exoplayer.source.s
    public boolean d(long j) {
        return this.y0.d(j);
    }

    @Override // androidx.media3.exoplayer.source.i, androidx.media3.exoplayer.source.s
    public boolean e() {
        return this.y0.e();
    }

    @Override // androidx.media3.exoplayer.source.i, androidx.media3.exoplayer.source.s
    public long g() {
        return this.y0.g();
    }

    @Override // androidx.media3.exoplayer.source.i, androidx.media3.exoplayer.source.s
    public void h(long j) {
        this.y0.h(j);
    }

    @Override // androidx.media3.exoplayer.source.i
    public void k() throws IOException {
        this.l0.b();
    }

    @Override // androidx.media3.exoplayer.source.i
    public long l(long j) {
        for (ry ryVar : this.w0) {
            ryVar.R(j);
        }
        for (d dVar : this.x0) {
            dVar.c(j);
        }
        return j;
    }

    @Override // androidx.media3.exoplayer.source.i
    public long n(androidx.media3.exoplayer.trackselection.c[] cVarArr, boolean[] zArr, r[] rVarArr, boolean[] zArr2, long j) {
        int[] A = A(cVarArr);
        H(cVarArr, zArr, rVarArr);
        I(cVarArr, rVarArr, A);
        J(cVarArr, rVarArr, zArr2, j, A);
        ArrayList arrayList = new ArrayList();
        ArrayList arrayList2 = new ArrayList();
        for (r rVar : rVarArr) {
            if (rVar instanceof ry) {
                arrayList.add((ry) rVar);
            } else if (rVar instanceof d) {
                arrayList2.add((d) rVar);
            }
        }
        ChunkSampleStream<androidx.media3.exoplayer.dash.a>[] D = D(arrayList.size());
        this.w0 = D;
        arrayList.toArray(D);
        d[] dVarArr = new d[arrayList2.size()];
        this.x0 = dVarArr;
        arrayList2.toArray(dVarArr);
        this.y0 = this.p0.a(this.w0);
        return j;
    }

    @Override // androidx.media3.exoplayer.source.i
    public long o() {
        return CellBase.ID_SYSTEM_MESSAGE_REQUEST_CLOSED;
    }

    public final ry<androidx.media3.exoplayer.dash.a> p(a aVar, androidx.media3.exoplayer.trackselection.c cVar, long j) {
        int i;
        v vVar;
        v vVar2;
        int i2;
        int i3 = aVar.f;
        boolean z = i3 != -1;
        e.c cVar2 = null;
        if (z) {
            vVar = this.n0.b(i3);
            i = 1;
        } else {
            i = 0;
            vVar = null;
        }
        int i4 = aVar.g;
        boolean z2 = i4 != -1;
        if (z2) {
            vVar2 = this.n0.b(i4);
            i += vVar2.a;
        } else {
            vVar2 = null;
        }
        j[] jVarArr = new j[i];
        int[] iArr = new int[i];
        if (z) {
            jVarArr[0] = vVar.c(0);
            iArr[0] = 5;
            i2 = 1;
        } else {
            i2 = 0;
        }
        ArrayList arrayList = new ArrayList();
        if (z2) {
            for (int i5 = 0; i5 < vVar2.a; i5++) {
                jVarArr[i2] = vVar2.c(i5);
                iArr[i2] = 3;
                arrayList.add(jVarArr[i2]);
                i2++;
            }
        }
        if (this.z0.d && z) {
            cVar2 = this.q0.k();
        }
        e.c cVar3 = cVar2;
        ry<androidx.media3.exoplayer.dash.a> ryVar = new ry<>(aVar.b, iArr, jVarArr, this.f0.a(this.l0, this.z0, this.j0, this.A0, aVar.a, cVar, aVar.b, this.k0, z, arrayList, cVar3, this.g0, this.u0), this, this.m0, j, this.h0, this.t0, this.i0, this.s0);
        synchronized (this) {
            this.r0.put(ryVar, cVar3);
        }
        return ryVar;
    }

    @Override // androidx.media3.exoplayer.source.i
    public void q(i.a aVar, long j) {
        this.v0 = aVar;
        aVar.f(this);
    }

    @Override // androidx.media3.exoplayer.source.i
    public c84 r() {
        return this.n0;
    }

    @Override // androidx.media3.exoplayer.source.i
    public void t(long j, boolean z) {
        for (ry ryVar : this.w0) {
            ryVar.t(j, z);
        }
    }

    public final int z(int i, int[] iArr) {
        int i2 = iArr[i];
        if (i2 == -1) {
            return -1;
        }
        int i3 = this.o0[i2].e;
        for (int i4 = 0; i4 < iArr.length; i4++) {
            int i5 = iArr[i4];
            if (i5 == i3 && this.o0[i5].c == 0) {
                return i4;
            }
        }
        return -1;
    }
}
