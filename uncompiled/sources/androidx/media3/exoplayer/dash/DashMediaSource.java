package androidx.media3.exoplayer.dash;

import android.net.Uri;
import android.os.Handler;
import android.os.Looper;
import android.os.SystemClock;
import android.text.TextUtils;
import android.util.SparseArray;
import androidx.media3.common.ParserException;
import androidx.media3.common.StreamKey;
import androidx.media3.common.m;
import androidx.media3.common.u;
import androidx.media3.datasource.b;
import androidx.media3.exoplayer.dash.DashMediaSource;
import androidx.media3.exoplayer.dash.a;
import androidx.media3.exoplayer.dash.c;
import androidx.media3.exoplayer.dash.e;
import androidx.media3.exoplayer.source.i;
import androidx.media3.exoplayer.source.j;
import androidx.media3.exoplayer.source.k;
import androidx.media3.exoplayer.upstream.Loader;
import androidx.media3.exoplayer.upstream.b;
import androidx.media3.exoplayer.upstream.c;
import androidx.media3.exoplayer.upstream.d;
import defpackage.zq3;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.math.RoundingMode;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.List;
import java.util.Locale;
import java.util.TimeZone;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import org.slf4j.Marker;
import zendesk.support.request.CellBase;

/* loaded from: classes.dex */
public final class DashMediaSource extends androidx.media3.exoplayer.source.a {
    public Loader A;
    public fa4 B;
    public IOException C;
    public Handler D;
    public m.g E;
    public Uri F;
    public Uri G;
    public sd0 H;
    public boolean I;
    public long J;
    public long K;
    public long L;
    public int M;
    public long N;
    public int O;
    public final m h;
    public final boolean i;
    public final b.a j;
    public final a.InterfaceC0033a k;
    public final q40 l;
    public final androidx.media3.exoplayer.drm.c m;
    public final androidx.media3.exoplayer.upstream.b n;
    public final Cdo o;
    public final long p;
    public final k.a q;
    public final d.a<? extends sd0> r;
    public final e s;
    public final Object t;
    public final SparseArray<androidx.media3.exoplayer.dash.b> u;
    public final Runnable v;
    public final Runnable w;
    public final e.b x;
    public final androidx.media3.exoplayer.upstream.c y;
    public androidx.media3.datasource.b z;

    /* loaded from: classes.dex */
    public static final class Factory implements j.a {
        public final a.InterfaceC0033a a;
        public final b.a b;
        public zr0 c;
        public q40 d;
        public androidx.media3.exoplayer.upstream.b e;
        public long f;
        public d.a<? extends sd0> g;

        public Factory(b.a aVar) {
            this(new c.a(aVar), aVar);
        }

        @Override // androidx.media3.exoplayer.source.j.a
        /* renamed from: d */
        public DashMediaSource a(m mVar) {
            ii.e(mVar.f0);
            d.a aVar = this.g;
            if (aVar == null) {
                aVar = new td0();
            }
            List<StreamKey> list = mVar.f0.e;
            return new DashMediaSource(mVar, null, this.b, !list.isEmpty() ? new androidx.media3.exoplayer.offline.a(aVar, list) : aVar, this.a, this.d, this.c.a(mVar), this.e, this.f, null);
        }

        @Override // androidx.media3.exoplayer.source.j.a
        /* renamed from: e */
        public Factory c(zr0 zr0Var) {
            this.c = (zr0) ii.f(zr0Var, "MediaSource.Factory#setDrmSessionManagerProvider no longer handles null by instantiating a new DefaultDrmSessionManagerProvider. Explicitly construct and pass an instance in order to retain the old behavior.");
            return this;
        }

        @Override // androidx.media3.exoplayer.source.j.a
        /* renamed from: f */
        public Factory b(androidx.media3.exoplayer.upstream.b bVar) {
            this.e = (androidx.media3.exoplayer.upstream.b) ii.f(bVar, "MediaSource.Factory#setLoadErrorHandlingPolicy no longer handles null by instantiating a new DefaultLoadErrorHandlingPolicy. Explicitly construct and pass an instance in order to retain the old behavior.");
            return this;
        }

        public Factory(a.InterfaceC0033a interfaceC0033a, b.a aVar) {
            this.a = (a.InterfaceC0033a) ii.e(interfaceC0033a);
            this.b = aVar;
            this.c = new androidx.media3.exoplayer.drm.a();
            this.e = new androidx.media3.exoplayer.upstream.a();
            this.f = 30000L;
            this.d = new ni0();
        }
    }

    /* loaded from: classes.dex */
    public class a implements zq3.b {
        public a() {
        }

        @Override // defpackage.zq3.b
        public void a(IOException iOException) {
            DashMediaSource.this.W(iOException);
        }

        @Override // defpackage.zq3.b
        public void b() {
            DashMediaSource.this.X(zq3.h());
        }
    }

    /* loaded from: classes.dex */
    public static final class b extends u {
        public final long f0;
        public final long g0;
        public final long h0;
        public final int i0;
        public final long j0;
        public final long k0;
        public final long l0;
        public final sd0 m0;
        public final m n0;
        public final m.g o0;

        public b(long j, long j2, long j3, int i, long j4, long j5, long j6, sd0 sd0Var, m mVar, m.g gVar) {
            ii.g(sd0Var.d == (gVar != null));
            this.f0 = j;
            this.g0 = j2;
            this.h0 = j3;
            this.i0 = i;
            this.j0 = j4;
            this.k0 = j5;
            this.l0 = j6;
            this.m0 = sd0Var;
            this.n0 = mVar;
            this.o0 = gVar;
        }

        public static boolean v(sd0 sd0Var) {
            return sd0Var.d && sd0Var.e != CellBase.ID_SYSTEM_MESSAGE_REQUEST_CLOSED && sd0Var.b == CellBase.ID_SYSTEM_MESSAGE_REQUEST_CLOSED;
        }

        @Override // androidx.media3.common.u
        public int b(Object obj) {
            int intValue;
            if ((obj instanceof Integer) && (intValue = ((Integer) obj).intValue() - this.i0) >= 0 && intValue < i()) {
                return intValue;
            }
            return -1;
        }

        @Override // androidx.media3.common.u
        public u.b g(int i, u.b bVar, boolean z) {
            ii.c(i, 0, i());
            return bVar.u(z ? this.m0.d(i).a : null, z ? Integer.valueOf(this.i0 + i) : null, 0, this.m0.g(i), androidx.media3.common.util.b.y0(this.m0.d(i).b - this.m0.d(0).b) - this.j0);
        }

        @Override // androidx.media3.common.u
        public int i() {
            return this.m0.e();
        }

        @Override // androidx.media3.common.u
        public Object m(int i) {
            ii.c(i, 0, i());
            return Integer.valueOf(this.i0 + i);
        }

        @Override // androidx.media3.common.u
        public u.c o(int i, u.c cVar, long j) {
            ii.c(i, 0, 1);
            long u = u(j);
            Object obj = u.c.v0;
            m mVar = this.n0;
            sd0 sd0Var = this.m0;
            return cVar.k(obj, mVar, sd0Var, this.f0, this.g0, this.h0, true, v(sd0Var), this.o0, u, this.k0, 0, i() - 1, this.j0);
        }

        @Override // androidx.media3.common.u
        public int p() {
            return 1;
        }

        public final long u(long j) {
            wd0 l;
            long j2 = this.l0;
            if (v(this.m0)) {
                if (j > 0) {
                    j2 += j;
                    if (j2 > this.k0) {
                        return CellBase.ID_SYSTEM_MESSAGE_REQUEST_CLOSED;
                    }
                }
                long j3 = this.j0 + j2;
                long g = this.m0.g(0);
                int i = 0;
                while (i < this.m0.e() - 1 && j3 >= g) {
                    j3 -= g;
                    i++;
                    g = this.m0.g(i);
                }
                jq2 d = this.m0.d(i);
                int a = d.a(2);
                return (a == -1 || (l = d.c.get(a).c.get(0).l()) == null || l.j(g) == 0) ? j2 : (j2 + l.b(l.g(j3, g))) - j3;
            }
            return j2;
        }
    }

    /* loaded from: classes.dex */
    public final class c implements e.b {
        public c() {
        }

        @Override // androidx.media3.exoplayer.dash.e.b
        public void a() {
            DashMediaSource.this.Q();
        }

        @Override // androidx.media3.exoplayer.dash.e.b
        public void b(long j) {
            DashMediaSource.this.P(j);
        }

        public /* synthetic */ c(DashMediaSource dashMediaSource, a aVar) {
            this();
        }
    }

    /* loaded from: classes.dex */
    public static final class d implements d.a<Long> {
        public static final Pattern a = Pattern.compile("(.+?)(Z|((\\+|-|−)(\\d\\d)(:?(\\d\\d))?))");

        @Override // androidx.media3.exoplayer.upstream.d.a
        /* renamed from: b */
        public Long a(Uri uri, InputStream inputStream) throws IOException {
            String readLine = new BufferedReader(new InputStreamReader(inputStream, cy.c)).readLine();
            try {
                Matcher matcher = a.matcher(readLine);
                if (matcher.matches()) {
                    String group = matcher.group(1);
                    SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss", Locale.US);
                    simpleDateFormat.setTimeZone(TimeZone.getTimeZone("UTC"));
                    long time = simpleDateFormat.parse(group).getTime();
                    if (!"Z".equals(matcher.group(2))) {
                        long j = Marker.ANY_NON_NULL_MARKER.equals(matcher.group(4)) ? 1L : -1L;
                        long parseLong = Long.parseLong(matcher.group(5));
                        String group2 = matcher.group(7);
                        time -= j * ((((parseLong * 60) + (TextUtils.isEmpty(group2) ? 0L : Long.parseLong(group2))) * 60) * 1000);
                    }
                    return Long.valueOf(time);
                }
                throw ParserException.createForMalformedManifest("Couldn't parse timestamp: " + readLine, null);
            } catch (ParseException e) {
                throw ParserException.createForMalformedManifest(null, e);
            }
        }
    }

    /* loaded from: classes.dex */
    public final class e implements Loader.b<androidx.media3.exoplayer.upstream.d<sd0>> {
        public e() {
        }

        @Override // androidx.media3.exoplayer.upstream.Loader.b
        /* renamed from: a */
        public void u(androidx.media3.exoplayer.upstream.d<sd0> dVar, long j, long j2, boolean z) {
            DashMediaSource.this.R(dVar, j, j2);
        }

        @Override // androidx.media3.exoplayer.upstream.Loader.b
        /* renamed from: b */
        public void s(androidx.media3.exoplayer.upstream.d<sd0> dVar, long j, long j2) {
            DashMediaSource.this.S(dVar, j, j2);
        }

        @Override // androidx.media3.exoplayer.upstream.Loader.b
        /* renamed from: c */
        public Loader.c j(androidx.media3.exoplayer.upstream.d<sd0> dVar, long j, long j2, IOException iOException, int i) {
            return DashMediaSource.this.T(dVar, j, j2, iOException, i);
        }

        public /* synthetic */ e(DashMediaSource dashMediaSource, a aVar) {
            this();
        }
    }

    /* loaded from: classes.dex */
    public final class f implements androidx.media3.exoplayer.upstream.c {
        public f() {
        }

        public final void a() throws IOException {
            if (DashMediaSource.this.C != null) {
                throw DashMediaSource.this.C;
            }
        }

        @Override // androidx.media3.exoplayer.upstream.c
        public void b() throws IOException {
            DashMediaSource.this.A.b();
            a();
        }
    }

    /* loaded from: classes.dex */
    public final class g implements Loader.b<androidx.media3.exoplayer.upstream.d<Long>> {
        public g() {
        }

        @Override // androidx.media3.exoplayer.upstream.Loader.b
        /* renamed from: a */
        public void u(androidx.media3.exoplayer.upstream.d<Long> dVar, long j, long j2, boolean z) {
            DashMediaSource.this.R(dVar, j, j2);
        }

        @Override // androidx.media3.exoplayer.upstream.Loader.b
        /* renamed from: b */
        public void s(androidx.media3.exoplayer.upstream.d<Long> dVar, long j, long j2) {
            DashMediaSource.this.U(dVar, j, j2);
        }

        @Override // androidx.media3.exoplayer.upstream.Loader.b
        /* renamed from: c */
        public Loader.c j(androidx.media3.exoplayer.upstream.d<Long> dVar, long j, long j2, IOException iOException, int i) {
            return DashMediaSource.this.V(dVar, j, j2, iOException);
        }

        public /* synthetic */ g(DashMediaSource dashMediaSource, a aVar) {
            this();
        }
    }

    /* loaded from: classes.dex */
    public static final class h implements d.a<Long> {
        public h() {
        }

        @Override // androidx.media3.exoplayer.upstream.d.a
        /* renamed from: b */
        public Long a(Uri uri, InputStream inputStream) throws IOException {
            return Long.valueOf(androidx.media3.common.util.b.E0(new BufferedReader(new InputStreamReader(inputStream)).readLine()));
        }

        public /* synthetic */ h(a aVar) {
            this();
        }
    }

    static {
        f62.a("media3.exoplayer.dash");
    }

    public /* synthetic */ DashMediaSource(m mVar, sd0 sd0Var, b.a aVar, d.a aVar2, a.InterfaceC0033a interfaceC0033a, q40 q40Var, androidx.media3.exoplayer.drm.c cVar, androidx.media3.exoplayer.upstream.b bVar, long j, a aVar3) {
        this(mVar, sd0Var, aVar, aVar2, interfaceC0033a, q40Var, cVar, bVar, j);
    }

    public static long H(jq2 jq2Var, long j, long j2) {
        long y0 = androidx.media3.common.util.b.y0(jq2Var.b);
        boolean L = L(jq2Var);
        long j3 = Long.MAX_VALUE;
        for (int i = 0; i < jq2Var.c.size(); i++) {
            i8 i8Var = jq2Var.c.get(i);
            List<b73> list = i8Var.c;
            if ((!L || i8Var.b != 3) && !list.isEmpty()) {
                wd0 l = list.get(0).l();
                if (l == null) {
                    return y0 + j;
                }
                long k = l.k(j, j2);
                if (k == 0) {
                    return y0;
                }
                long d2 = (l.d(j, j2) + k) - 1;
                j3 = Math.min(j3, l.c(d2, j) + l.b(d2) + y0);
            }
        }
        return j3;
    }

    public static long I(jq2 jq2Var, long j, long j2) {
        long y0 = androidx.media3.common.util.b.y0(jq2Var.b);
        boolean L = L(jq2Var);
        long j3 = y0;
        for (int i = 0; i < jq2Var.c.size(); i++) {
            i8 i8Var = jq2Var.c.get(i);
            List<b73> list = i8Var.c;
            if ((!L || i8Var.b != 3) && !list.isEmpty()) {
                wd0 l = list.get(0).l();
                if (l == null || l.k(j, j2) == 0) {
                    return y0;
                }
                j3 = Math.max(j3, l.b(l.d(j, j2)) + y0);
            }
        }
        return j3;
    }

    public static long J(sd0 sd0Var, long j) {
        wd0 l;
        int e2 = sd0Var.e() - 1;
        jq2 d2 = sd0Var.d(e2);
        long y0 = androidx.media3.common.util.b.y0(d2.b);
        long g2 = sd0Var.g(e2);
        long y02 = androidx.media3.common.util.b.y0(j);
        long y03 = androidx.media3.common.util.b.y0(sd0Var.a);
        long y04 = androidx.media3.common.util.b.y0(5000L);
        for (int i = 0; i < d2.c.size(); i++) {
            List<b73> list = d2.c.get(i).c;
            if (!list.isEmpty() && (l = list.get(0).l()) != null) {
                long e3 = ((y03 + y0) + l.e(g2, y02)) - y02;
                if (e3 < y04 - 100000 || (e3 > y04 && e3 < y04 + 100000)) {
                    y04 = e3;
                }
            }
        }
        return h22.a(y04, 1000L, RoundingMode.CEILING);
    }

    public static boolean L(jq2 jq2Var) {
        for (int i = 0; i < jq2Var.c.size(); i++) {
            int i2 = jq2Var.c.get(i).b;
            if (i2 == 1 || i2 == 2) {
                return true;
            }
        }
        return false;
    }

    public static boolean M(jq2 jq2Var) {
        for (int i = 0; i < jq2Var.c.size(); i++) {
            wd0 l = jq2Var.c.get(i).c.get(0).l();
            if (l == null || l.h()) {
                return true;
            }
        }
        return false;
    }

    /* JADX INFO: Access modifiers changed from: private */
    public /* synthetic */ void N() {
        Y(false);
    }

    @Override // androidx.media3.exoplayer.source.a
    public void A() {
        this.I = false;
        this.z = null;
        Loader loader = this.A;
        if (loader != null) {
            loader.l();
            this.A = null;
        }
        this.J = 0L;
        this.K = 0L;
        this.H = this.i ? this.H : null;
        this.F = this.G;
        this.C = null;
        Handler handler = this.D;
        if (handler != null) {
            handler.removeCallbacksAndMessages(null);
            this.D = null;
        }
        this.L = CellBase.ID_SYSTEM_MESSAGE_REQUEST_CLOSED;
        this.M = 0;
        this.N = CellBase.ID_SYSTEM_MESSAGE_REQUEST_CLOSED;
        this.O = 0;
        this.u.clear();
        this.o.i();
        this.m.a();
    }

    public final long K() {
        return Math.min((this.M - 1) * 1000, 5000);
    }

    public final void O() {
        zq3.j(this.A, new a());
    }

    public void P(long j) {
        long j2 = this.N;
        if (j2 == CellBase.ID_SYSTEM_MESSAGE_REQUEST_CLOSED || j2 < j) {
            this.N = j;
        }
    }

    public void Q() {
        this.D.removeCallbacks(this.w);
        e0();
    }

    public void R(androidx.media3.exoplayer.upstream.d<?> dVar, long j, long j2) {
        u02 u02Var = new u02(dVar.a, dVar.b, dVar.f(), dVar.d(), j, j2, dVar.b());
        this.n.b(dVar.a);
        this.q.q(u02Var, dVar.c);
    }

    /* JADX WARN: Removed duplicated region for block: B:24:0x00aa  */
    /* JADX WARN: Removed duplicated region for block: B:29:0x00ca  */
    /*
        Code decompiled incorrectly, please refer to instructions dump.
        To view partially-correct code enable 'Show inconsistent code' option in preferences
    */
    public void S(androidx.media3.exoplayer.upstream.d<defpackage.sd0> r19, long r20, long r22) {
        /*
            Method dump skipped, instructions count: 285
            To view this dump change 'Code comments level' option to 'DEBUG'
        */
        throw new UnsupportedOperationException("Method not decompiled: androidx.media3.exoplayer.dash.DashMediaSource.S(androidx.media3.exoplayer.upstream.d, long, long):void");
    }

    public Loader.c T(androidx.media3.exoplayer.upstream.d<sd0> dVar, long j, long j2, IOException iOException, int i) {
        Loader.c h2;
        u02 u02Var = new u02(dVar.a, dVar.b, dVar.f(), dVar.d(), j, j2, dVar.b());
        long a2 = this.n.a(new b.c(u02Var, new g62(dVar.c), iOException, i));
        if (a2 == CellBase.ID_SYSTEM_MESSAGE_REQUEST_CLOSED) {
            h2 = Loader.e;
        } else {
            h2 = Loader.h(false, a2);
        }
        boolean z = !h2.c();
        this.q.x(u02Var, dVar.c, iOException, z);
        if (z) {
            this.n.b(dVar.a);
        }
        return h2;
    }

    public void U(androidx.media3.exoplayer.upstream.d<Long> dVar, long j, long j2) {
        u02 u02Var = new u02(dVar.a, dVar.b, dVar.f(), dVar.d(), j, j2, dVar.b());
        this.n.b(dVar.a);
        this.q.t(u02Var, dVar.c);
        X(dVar.e().longValue() - j);
    }

    public Loader.c V(androidx.media3.exoplayer.upstream.d<Long> dVar, long j, long j2, IOException iOException) {
        this.q.x(new u02(dVar.a, dVar.b, dVar.f(), dVar.d(), j, j2, dVar.b()), dVar.c, iOException, true);
        this.n.b(dVar.a);
        W(iOException);
        return Loader.d;
    }

    public final void W(IOException iOException) {
        p12.d("DashMediaSource", "Failed to resolve time offset.", iOException);
        Y(true);
    }

    public final void X(long j) {
        this.L = j;
        Y(true);
    }

    public final void Y(boolean z) {
        jq2 jq2Var;
        long j;
        long j2;
        for (int i = 0; i < this.u.size(); i++) {
            int keyAt = this.u.keyAt(i);
            if (keyAt >= this.O) {
                this.u.valueAt(i).K(this.H, keyAt - this.O);
            }
        }
        jq2 d2 = this.H.d(0);
        int e2 = this.H.e() - 1;
        jq2 d3 = this.H.d(e2);
        long g2 = this.H.g(e2);
        long y0 = androidx.media3.common.util.b.y0(androidx.media3.common.util.b.X(this.L));
        long I = I(d2, this.H.g(0), y0);
        long H = H(d3, g2, y0);
        boolean z2 = this.H.d && !M(d3);
        if (z2) {
            long j3 = this.H.f;
            if (j3 != CellBase.ID_SYSTEM_MESSAGE_REQUEST_CLOSED) {
                I = Math.max(I, H - androidx.media3.common.util.b.y0(j3));
            }
        }
        long j4 = H - I;
        sd0 sd0Var = this.H;
        if (sd0Var.d) {
            ii.g(sd0Var.a != CellBase.ID_SYSTEM_MESSAGE_REQUEST_CLOSED);
            long y02 = (y0 - androidx.media3.common.util.b.y0(this.H.a)) - I;
            f0(y02, j4);
            long U0 = this.H.a + androidx.media3.common.util.b.U0(I);
            long y03 = y02 - androidx.media3.common.util.b.y0(this.E.a);
            long min = Math.min(5000000L, j4 / 2);
            j = U0;
            j2 = y03 < min ? min : y03;
            jq2Var = d2;
        } else {
            jq2Var = d2;
            j = CellBase.ID_SYSTEM_MESSAGE_REQUEST_CLOSED;
            j2 = 0;
        }
        long y04 = I - androidx.media3.common.util.b.y0(jq2Var.b);
        sd0 sd0Var2 = this.H;
        z(new b(sd0Var2.a, j, this.L, this.O, y04, j4, j2, sd0Var2, this.h, sd0Var2.d ? this.E : null));
        if (this.i) {
            return;
        }
        this.D.removeCallbacks(this.w);
        if (z2) {
            this.D.postDelayed(this.w, J(this.H, androidx.media3.common.util.b.X(this.L)));
        }
        if (this.I) {
            e0();
        } else if (z) {
            sd0 sd0Var3 = this.H;
            if (sd0Var3.d) {
                long j5 = sd0Var3.e;
                if (j5 != CellBase.ID_SYSTEM_MESSAGE_REQUEST_CLOSED) {
                    if (j5 == 0) {
                        j5 = 5000;
                    }
                    c0(Math.max(0L, (this.J + j5) - SystemClock.elapsedRealtime()));
                }
            }
        }
    }

    public final void Z(ig4 ig4Var) {
        String str = ig4Var.a;
        if (!androidx.media3.common.util.b.c(str, "urn:mpeg:dash:utc:direct:2014") && !androidx.media3.common.util.b.c(str, "urn:mpeg:dash:utc:direct:2012")) {
            if (!androidx.media3.common.util.b.c(str, "urn:mpeg:dash:utc:http-iso:2014") && !androidx.media3.common.util.b.c(str, "urn:mpeg:dash:utc:http-iso:2012")) {
                if (!androidx.media3.common.util.b.c(str, "urn:mpeg:dash:utc:http-xsdate:2014") && !androidx.media3.common.util.b.c(str, "urn:mpeg:dash:utc:http-xsdate:2012")) {
                    if (!androidx.media3.common.util.b.c(str, "urn:mpeg:dash:utc:ntp:2014") && !androidx.media3.common.util.b.c(str, "urn:mpeg:dash:utc:ntp:2012")) {
                        W(new IOException("Unsupported UTC timing scheme"));
                        return;
                    } else {
                        O();
                        return;
                    }
                }
                b0(ig4Var, new h(null));
                return;
            }
            b0(ig4Var, new d());
            return;
        }
        a0(ig4Var);
    }

    public final void a0(ig4 ig4Var) {
        try {
            X(androidx.media3.common.util.b.E0(ig4Var.b) - this.K);
        } catch (ParserException e2) {
            W(e2);
        }
    }

    public final void b0(ig4 ig4Var, d.a<Long> aVar) {
        d0(new androidx.media3.exoplayer.upstream.d(this.z, Uri.parse(ig4Var.b), 5, aVar), new g(this, null), 1);
    }

    public final void c0(long j) {
        this.D.postDelayed(this.v, j);
    }

    public final <T> void d0(androidx.media3.exoplayer.upstream.d<T> dVar, Loader.b<androidx.media3.exoplayer.upstream.d<T>> bVar, int i) {
        this.q.z(new u02(dVar.a, dVar.b, this.A.n(dVar, bVar, i)), dVar.c);
    }

    @Override // androidx.media3.exoplayer.source.j
    public i e(j.b bVar, gb gbVar, long j) {
        int intValue = ((Integer) bVar.a).intValue() - this.O;
        k.a t = t(bVar, this.H.d(intValue).b);
        androidx.media3.exoplayer.dash.b bVar2 = new androidx.media3.exoplayer.dash.b(intValue + this.O, this.H, this.o, intValue, this.k, this.B, this.m, q(bVar), this.n, t, this.L, this.y, gbVar, this.l, this.x, w());
        this.u.put(bVar2.a, bVar2);
        return bVar2;
    }

    public final void e0() {
        Uri uri;
        this.D.removeCallbacks(this.v);
        if (this.A.i()) {
            return;
        }
        if (this.A.j()) {
            this.I = true;
            return;
        }
        synchronized (this.t) {
            uri = this.F;
        }
        this.I = false;
        d0(new androidx.media3.exoplayer.upstream.d(this.z, uri, 4, this.r), this.s, this.n.c(4));
    }

    /* JADX WARN: Removed duplicated region for block: B:19:0x0046  */
    /* JADX WARN: Removed duplicated region for block: B:22:0x0056  */
    /* JADX WARN: Removed duplicated region for block: B:23:0x005b  */
    /* JADX WARN: Removed duplicated region for block: B:30:0x006f  */
    /* JADX WARN: Removed duplicated region for block: B:34:0x0079  */
    /* JADX WARN: Removed duplicated region for block: B:45:0x0094  */
    /* JADX WARN: Removed duplicated region for block: B:48:0x0099  */
    /* JADX WARN: Removed duplicated region for block: B:52:0x00be  */
    /* JADX WARN: Removed duplicated region for block: B:59:0x00cf  */
    /*
        Code decompiled incorrectly, please refer to instructions dump.
        To view partially-correct code enable 'Show inconsistent code' option in preferences
    */
    public final void f0(long r18, long r20) {
        /*
            Method dump skipped, instructions count: 273
            To view this dump change 'Code comments level' option to 'DEBUG'
        */
        throw new UnsupportedOperationException("Method not decompiled: androidx.media3.exoplayer.dash.DashMediaSource.f0(long, long):void");
    }

    @Override // androidx.media3.exoplayer.source.j
    public m i() {
        return this.h;
    }

    @Override // androidx.media3.exoplayer.source.j
    public void j() throws IOException {
        this.y.b();
    }

    @Override // androidx.media3.exoplayer.source.j
    public void o(i iVar) {
        androidx.media3.exoplayer.dash.b bVar = (androidx.media3.exoplayer.dash.b) iVar;
        bVar.G();
        this.u.remove(bVar.a);
    }

    @Override // androidx.media3.exoplayer.source.a
    public void y(fa4 fa4Var) {
        this.B = fa4Var;
        this.m.d();
        this.m.b(Looper.myLooper(), w());
        if (this.i) {
            Y(false);
            return;
        }
        this.z = this.j.a();
        this.A = new Loader("DashMediaSource");
        this.D = androidx.media3.common.util.b.v();
        e0();
    }

    public DashMediaSource(m mVar, sd0 sd0Var, b.a aVar, d.a<? extends sd0> aVar2, a.InterfaceC0033a interfaceC0033a, q40 q40Var, androidx.media3.exoplayer.drm.c cVar, androidx.media3.exoplayer.upstream.b bVar, long j) {
        this.h = mVar;
        this.E = mVar.g0;
        this.F = ((m.h) ii.e(mVar.f0)).a;
        this.G = mVar.f0.a;
        this.H = sd0Var;
        this.j = aVar;
        this.r = aVar2;
        this.k = interfaceC0033a;
        this.m = cVar;
        this.n = bVar;
        this.p = j;
        this.l = q40Var;
        this.o = new Cdo();
        boolean z = sd0Var != null;
        this.i = z;
        this.q = s(null);
        this.t = new Object();
        this.u = new SparseArray<>();
        this.x = new c(this, null);
        this.N = CellBase.ID_SYSTEM_MESSAGE_REQUEST_CLOSED;
        this.L = CellBase.ID_SYSTEM_MESSAGE_REQUEST_CLOSED;
        if (z) {
            ii.g(true ^ sd0Var.d);
            this.s = null;
            this.v = null;
            this.w = null;
            this.y = new c.a();
            return;
        }
        this.s = new e(this, null);
        this.y = new f();
        this.v = new Runnable() { // from class: vd0
            @Override // java.lang.Runnable
            public final void run() {
                DashMediaSource.this.e0();
            }
        };
        this.w = new Runnable() { // from class: ud0
            @Override // java.lang.Runnable
            public final void run() {
                DashMediaSource.this.N();
            }
        };
    }
}
