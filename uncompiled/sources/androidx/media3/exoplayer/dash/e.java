package androidx.media3.exoplayer.dash;

import android.os.Handler;
import android.os.Message;
import androidx.media3.common.Metadata;
import androidx.media3.common.ParserException;
import androidx.media3.common.g;
import androidx.media3.common.j;
import androidx.media3.exoplayer.source.q;
import androidx.media3.extractor.metadata.emsg.EventMessage;
import defpackage.f84;
import java.io.IOException;
import java.util.Iterator;
import java.util.Map;
import java.util.TreeMap;
import zendesk.support.request.CellBase;

/* compiled from: PlayerEmsgHandler.java */
/* loaded from: classes.dex */
public final class e implements Handler.Callback {
    public final gb a;
    public final b f0;
    public sd0 j0;
    public long k0;
    public boolean l0;
    public boolean m0;
    public boolean n0;
    public final TreeMap<Long, Long> i0 = new TreeMap<>();
    public final Handler h0 = androidx.media3.common.util.b.w(this);
    public final by0 g0 = new by0();

    /* compiled from: PlayerEmsgHandler.java */
    /* loaded from: classes.dex */
    public static final class a {
        public final long a;
        public final long b;

        public a(long j, long j2) {
            this.a = j;
            this.b = j2;
        }
    }

    /* compiled from: PlayerEmsgHandler.java */
    /* loaded from: classes.dex */
    public interface b {
        void a();

        void b(long j);
    }

    /* compiled from: PlayerEmsgHandler.java */
    /* loaded from: classes.dex */
    public final class c implements f84 {
        public final q a;
        public final y81 b = new y81();
        public final n82 c = new n82();
        public long d = CellBase.ID_SYSTEM_MESSAGE_REQUEST_CLOSED;

        public c(gb gbVar) {
            this.a = q.l(gbVar);
        }

        @Override // defpackage.f84
        public /* synthetic */ void a(op2 op2Var, int i) {
            e84.b(this, op2Var, i);
        }

        @Override // defpackage.f84
        public void b(long j, int i, int i2, int i3, f84.a aVar) {
            this.a.b(j, i, i2, i3, aVar);
            l();
        }

        @Override // defpackage.f84
        public void c(op2 op2Var, int i, int i2) {
            this.a.a(op2Var, i);
        }

        @Override // defpackage.f84
        public /* synthetic */ int d(g gVar, int i, boolean z) {
            return e84.a(this, gVar, i, z);
        }

        @Override // defpackage.f84
        public int e(g gVar, int i, boolean z, int i2) throws IOException {
            return this.a.d(gVar, i, z);
        }

        @Override // defpackage.f84
        public void f(j jVar) {
            this.a.f(jVar);
        }

        public final n82 g() {
            this.c.h();
            if (this.a.R(this.b, this.c, 0, false) == -4) {
                this.c.x();
                return this.c;
            }
            return null;
        }

        public boolean h(long j) {
            return e.this.j(j);
        }

        public void i(my myVar) {
            long j = this.d;
            if (j == CellBase.ID_SYSTEM_MESSAGE_REQUEST_CLOSED || myVar.h > j) {
                this.d = myVar.h;
            }
            e.this.m(myVar);
        }

        public boolean j(my myVar) {
            long j = this.d;
            return e.this.n(j != CellBase.ID_SYSTEM_MESSAGE_REQUEST_CLOSED && j < myVar.g);
        }

        public final void k(long j, long j2) {
            e.this.h0.sendMessage(e.this.h0.obtainMessage(1, new a(j, j2)));
        }

        public final void l() {
            while (this.a.K(false)) {
                n82 g = g();
                if (g != null) {
                    long j = g.i0;
                    Metadata a = e.this.g0.a(g);
                    if (a != null) {
                        EventMessage eventMessage = (EventMessage) a.c(0);
                        if (e.h(eventMessage.a, eventMessage.f0)) {
                            m(j, eventMessage);
                        }
                    }
                }
            }
            this.a.s();
        }

        public final void m(long j, EventMessage eventMessage) {
            long f = e.f(eventMessage);
            if (f == CellBase.ID_SYSTEM_MESSAGE_REQUEST_CLOSED) {
                return;
            }
            k(j, f);
        }

        public void n() {
            this.a.S();
        }
    }

    public e(sd0 sd0Var, b bVar, gb gbVar) {
        this.j0 = sd0Var;
        this.f0 = bVar;
        this.a = gbVar;
    }

    public static long f(EventMessage eventMessage) {
        try {
            return androidx.media3.common.util.b.E0(androidx.media3.common.util.b.B(eventMessage.i0));
        } catch (ParserException unused) {
            return CellBase.ID_SYSTEM_MESSAGE_REQUEST_CLOSED;
        }
    }

    public static boolean h(String str, String str2) {
        return "urn:mpeg:dash:event:2012".equals(str) && ("1".equals(str2) || "2".equals(str2) || "3".equals(str2));
    }

    public final Map.Entry<Long, Long> e(long j) {
        return this.i0.ceilingEntry(Long.valueOf(j));
    }

    public final void g(long j, long j2) {
        Long l = this.i0.get(Long.valueOf(j2));
        if (l == null) {
            this.i0.put(Long.valueOf(j2), Long.valueOf(j));
        } else if (l.longValue() > j) {
            this.i0.put(Long.valueOf(j2), Long.valueOf(j));
        }
    }

    @Override // android.os.Handler.Callback
    public boolean handleMessage(Message message) {
        if (this.n0) {
            return true;
        }
        if (message.what != 1) {
            return false;
        }
        a aVar = (a) message.obj;
        g(aVar.a, aVar.b);
        return true;
    }

    public final void i() {
        if (this.l0) {
            this.m0 = true;
            this.l0 = false;
            this.f0.a();
        }
    }

    public boolean j(long j) {
        sd0 sd0Var = this.j0;
        boolean z = false;
        if (sd0Var.d) {
            if (this.m0) {
                return true;
            }
            Map.Entry<Long, Long> e = e(sd0Var.h);
            if (e != null && e.getValue().longValue() < j) {
                this.k0 = e.getKey().longValue();
                l();
                z = true;
            }
            if (z) {
                i();
            }
            return z;
        }
        return false;
    }

    public c k() {
        return new c(this.a);
    }

    public final void l() {
        this.f0.b(this.k0);
    }

    public void m(my myVar) {
        this.l0 = true;
    }

    public boolean n(boolean z) {
        if (this.j0.d) {
            if (this.m0) {
                return true;
            }
            if (z) {
                i();
                return true;
            }
            return false;
        }
        return false;
    }

    public void o() {
        this.n0 = true;
        this.h0.removeCallbacksAndMessages(null);
    }

    public final void p() {
        Iterator<Map.Entry<Long, Long>> it = this.i0.entrySet().iterator();
        while (it.hasNext()) {
            if (it.next().getKey().longValue() < this.j0.h) {
                it.remove();
            }
        }
    }

    public void q(sd0 sd0Var) {
        this.m0 = false;
        this.k0 = CellBase.ID_SYSTEM_MESSAGE_REQUEST_CLOSED;
        this.j0 = sd0Var;
        p();
    }
}
