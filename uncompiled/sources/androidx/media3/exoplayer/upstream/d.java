package androidx.media3.exoplayer.upstream;

import android.net.Uri;
import androidx.media3.datasource.f;
import androidx.media3.exoplayer.upstream.Loader;
import defpackage.je0;
import java.io.IOException;
import java.io.InputStream;
import java.util.List;
import java.util.Map;

/* compiled from: ParsingLoadable.java */
/* loaded from: classes.dex */
public final class d<T> implements Loader.e {
    public final long a;
    public final je0 b;
    public final int c;
    public final f d;
    public final a<? extends T> e;
    public volatile T f;

    /* compiled from: ParsingLoadable.java */
    /* loaded from: classes.dex */
    public interface a<T> {
        T a(Uri uri, InputStream inputStream) throws IOException;
    }

    public d(androidx.media3.datasource.b bVar, Uri uri, int i, a<? extends T> aVar) {
        this(bVar, new je0.b().i(uri).b(1).a(), i, aVar);
    }

    @Override // androidx.media3.exoplayer.upstream.Loader.e
    public final void a() throws IOException {
        this.d.r();
        androidx.media3.datasource.c cVar = new androidx.media3.datasource.c(this.d, this.b);
        try {
            cVar.b();
            this.f = this.e.a((Uri) ii.e(this.d.m()), cVar);
        } finally {
            androidx.media3.common.util.b.n(cVar);
        }
    }

    public long b() {
        return this.d.o();
    }

    @Override // androidx.media3.exoplayer.upstream.Loader.e
    public final void c() {
    }

    public Map<String, List<String>> d() {
        return this.d.q();
    }

    public final T e() {
        return this.f;
    }

    public Uri f() {
        return this.d.p();
    }

    public d(androidx.media3.datasource.b bVar, je0 je0Var, int i, a<? extends T> aVar) {
        this.d = new f(bVar);
        this.b = je0Var;
        this.c = i;
        this.e = aVar;
        this.a = u02.a();
    }
}
