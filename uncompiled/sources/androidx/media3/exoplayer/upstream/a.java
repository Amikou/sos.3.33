package androidx.media3.exoplayer.upstream;

import androidx.media3.common.ParserException;
import androidx.media3.datasource.DataSourceException;
import androidx.media3.datasource.HttpDataSource$CleartextNotPermittedException;
import androidx.media3.datasource.HttpDataSource$InvalidResponseCodeException;
import androidx.media3.exoplayer.upstream.Loader;
import androidx.media3.exoplayer.upstream.b;
import java.io.FileNotFoundException;
import java.io.IOException;
import zendesk.support.request.CellBase;

/* compiled from: DefaultLoadErrorHandlingPolicy.java */
/* loaded from: classes.dex */
public class a implements b {
    public final int a;

    public a() {
        this(-1);
    }

    @Override // androidx.media3.exoplayer.upstream.b
    public long a(b.c cVar) {
        IOException iOException = cVar.a;
        return ((iOException instanceof ParserException) || (iOException instanceof FileNotFoundException) || (iOException instanceof HttpDataSource$CleartextNotPermittedException) || (iOException instanceof Loader.UnexpectedLoaderException) || DataSourceException.isCausedByPositionOutOfRange(iOException)) ? CellBase.ID_SYSTEM_MESSAGE_REQUEST_CLOSED : Math.min((cVar.b - 1) * 1000, 5000);
    }

    @Override // androidx.media3.exoplayer.upstream.b
    public /* synthetic */ void b(long j) {
        t02.a(this, j);
    }

    @Override // androidx.media3.exoplayer.upstream.b
    public int c(int i) {
        int i2 = this.a;
        return i2 == -1 ? i == 7 ? 6 : 3 : i2;
    }

    @Override // androidx.media3.exoplayer.upstream.b
    public b.C0042b d(b.a aVar, b.c cVar) {
        if (e(cVar.a)) {
            if (aVar.a(1)) {
                return new b.C0042b(1, 300000L);
            }
            if (aVar.a(2)) {
                return new b.C0042b(2, 60000L);
            }
            return null;
        }
        return null;
    }

    public boolean e(IOException iOException) {
        if (iOException instanceof HttpDataSource$InvalidResponseCodeException) {
            int i = ((HttpDataSource$InvalidResponseCodeException) iOException).responseCode;
            return i == 403 || i == 404 || i == 410 || i == 416 || i == 500 || i == 503;
        }
        return false;
    }

    public a(int i) {
        this.a = i;
    }
}
