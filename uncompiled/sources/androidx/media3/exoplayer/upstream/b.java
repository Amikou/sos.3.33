package androidx.media3.exoplayer.upstream;

import java.io.IOException;

/* compiled from: LoadErrorHandlingPolicy.java */
/* loaded from: classes.dex */
public interface b {

    /* compiled from: LoadErrorHandlingPolicy.java */
    /* loaded from: classes.dex */
    public static final class a {
        public final int a;
        public final int b;
        public final int c;
        public final int d;

        public a(int i, int i2, int i3, int i4) {
            this.a = i;
            this.b = i2;
            this.c = i3;
            this.d = i4;
        }

        public boolean a(int i) {
            if (i == 1) {
                if (this.a - this.b <= 1) {
                    return false;
                }
            } else if (this.c - this.d <= 1) {
                return false;
            }
            return true;
        }
    }

    /* compiled from: LoadErrorHandlingPolicy.java */
    /* renamed from: androidx.media3.exoplayer.upstream.b$b  reason: collision with other inner class name */
    /* loaded from: classes.dex */
    public static final class C0042b {
        public final int a;
        public final long b;

        public C0042b(int i, long j) {
            ii.a(j >= 0);
            this.a = i;
            this.b = j;
        }
    }

    /* compiled from: LoadErrorHandlingPolicy.java */
    /* loaded from: classes.dex */
    public static final class c {
        public final IOException a;
        public final int b;

        public c(u02 u02Var, g62 g62Var, IOException iOException, int i) {
            this.a = iOException;
            this.b = i;
        }
    }

    long a(c cVar);

    void b(long j);

    int c(int i);

    C0042b d(a aVar, c cVar);
}
