package androidx.media3.exoplayer.upstream;

import android.annotation.SuppressLint;
import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import android.os.SystemClock;
import java.io.IOException;
import java.util.concurrent.ExecutorService;
import zendesk.support.request.CellBase;

/* loaded from: classes.dex */
public final class Loader implements androidx.media3.exoplayer.upstream.c {
    public static final c d;
    public static final c e;
    public final ExecutorService a;
    public d<? extends e> b;
    public IOException c;

    /* loaded from: classes.dex */
    public static final class UnexpectedLoaderException extends IOException {
        public UnexpectedLoaderException(Throwable th) {
            super("Unexpected " + th.getClass().getSimpleName() + ": " + th.getMessage(), th);
        }
    }

    /* loaded from: classes.dex */
    public interface b<T extends e> {
        c j(T t, long j, long j2, IOException iOException, int i);

        void s(T t, long j, long j2);

        void u(T t, long j, long j2, boolean z);
    }

    /* loaded from: classes.dex */
    public static final class c {
        public final int a;
        public final long b;

        public boolean c() {
            int i = this.a;
            return i == 0 || i == 1;
        }

        public c(int i, long j) {
            this.a = i;
            this.b = j;
        }
    }

    @SuppressLint({"HandlerLeak"})
    /* loaded from: classes.dex */
    public final class d<T extends e> extends Handler implements Runnable {
        public final int a;
        public final T f0;
        public final long g0;
        public b<T> h0;
        public IOException i0;
        public int j0;
        public Thread k0;
        public boolean l0;
        public volatile boolean m0;

        public d(Looper looper, T t, b<T> bVar, int i, long j) {
            super(looper);
            this.f0 = t;
            this.h0 = bVar;
            this.a = i;
            this.g0 = j;
        }

        public void a(boolean z) {
            this.m0 = z;
            this.i0 = null;
            if (hasMessages(0)) {
                this.l0 = true;
                removeMessages(0);
                if (!z) {
                    sendEmptyMessage(1);
                }
            } else {
                synchronized (this) {
                    this.l0 = true;
                    this.f0.c();
                    Thread thread = this.k0;
                    if (thread != null) {
                        thread.interrupt();
                    }
                }
            }
            if (z) {
                c();
                long elapsedRealtime = SystemClock.elapsedRealtime();
                ((b) ii.e(this.h0)).u(this.f0, elapsedRealtime, elapsedRealtime - this.g0, true);
                this.h0 = null;
            }
        }

        public final void b() {
            this.i0 = null;
            Loader.this.a.execute((Runnable) ii.e(Loader.this.b));
        }

        public final void c() {
            Loader.this.b = null;
        }

        public final long d() {
            return Math.min((this.j0 - 1) * 1000, 5000);
        }

        public void e(int i) throws IOException {
            IOException iOException = this.i0;
            if (iOException != null && this.j0 > i) {
                throw iOException;
            }
        }

        public void f(long j) {
            ii.g(Loader.this.b == null);
            Loader.this.b = this;
            if (j > 0) {
                sendEmptyMessageDelayed(0, j);
            } else {
                b();
            }
        }

        @Override // android.os.Handler
        public void handleMessage(Message message) {
            long d;
            if (this.m0) {
                return;
            }
            int i = message.what;
            if (i == 0) {
                b();
            } else if (i != 3) {
                c();
                long elapsedRealtime = SystemClock.elapsedRealtime();
                long j = elapsedRealtime - this.g0;
                b bVar = (b) ii.e(this.h0);
                if (this.l0) {
                    bVar.u(this.f0, elapsedRealtime, j, false);
                    return;
                }
                int i2 = message.what;
                if (i2 == 1) {
                    try {
                        bVar.s(this.f0, elapsedRealtime, j);
                    } catch (RuntimeException e) {
                        p12.d("LoadTask", "Unexpected exception handling load completed", e);
                        Loader.this.c = new UnexpectedLoaderException(e);
                    }
                } else if (i2 != 2) {
                } else {
                    IOException iOException = (IOException) message.obj;
                    this.i0 = iOException;
                    int i3 = this.j0 + 1;
                    this.j0 = i3;
                    c j2 = bVar.j(this.f0, elapsedRealtime, j, iOException, i3);
                    if (j2.a != 3) {
                        if (j2.a != 2) {
                            if (j2.a == 1) {
                                this.j0 = 1;
                            }
                            if (j2.b != CellBase.ID_SYSTEM_MESSAGE_REQUEST_CLOSED) {
                                d = j2.b;
                            } else {
                                d = d();
                            }
                            f(d);
                            return;
                        }
                        return;
                    }
                    Loader.this.c = this.i0;
                }
            } else {
                throw ((Error) message.obj);
            }
        }

        @Override // java.lang.Runnable
        public void run() {
            boolean z;
            try {
                synchronized (this) {
                    z = !this.l0;
                    this.k0 = Thread.currentThread();
                }
                if (z) {
                    v74.a("load:" + this.f0.getClass().getSimpleName());
                    try {
                        this.f0.a();
                        v74.c();
                    } catch (Throwable th) {
                        v74.c();
                        throw th;
                    }
                }
                synchronized (this) {
                    this.k0 = null;
                    Thread.interrupted();
                }
                if (this.m0) {
                    return;
                }
                sendEmptyMessage(1);
            } catch (IOException e) {
                if (this.m0) {
                    return;
                }
                obtainMessage(2, e).sendToTarget();
            } catch (Exception e2) {
                if (this.m0) {
                    return;
                }
                p12.d("LoadTask", "Unexpected exception loading stream", e2);
                obtainMessage(2, new UnexpectedLoaderException(e2)).sendToTarget();
            } catch (OutOfMemoryError e3) {
                if (this.m0) {
                    return;
                }
                p12.d("LoadTask", "OutOfMemory error loading stream", e3);
                obtainMessage(2, new UnexpectedLoaderException(e3)).sendToTarget();
            } catch (Error e4) {
                if (!this.m0) {
                    p12.d("LoadTask", "Unexpected error loading stream", e4);
                    obtainMessage(3, e4).sendToTarget();
                }
                throw e4;
            }
        }
    }

    /* loaded from: classes.dex */
    public interface e {
        void a() throws IOException;

        void c();
    }

    /* loaded from: classes.dex */
    public interface f {
        void i();
    }

    /* loaded from: classes.dex */
    public static final class g implements Runnable {
        public final f a;

        public g(f fVar) {
            this.a = fVar;
        }

        @Override // java.lang.Runnable
        public void run() {
            this.a.i();
        }
    }

    static {
        h(false, CellBase.ID_SYSTEM_MESSAGE_REQUEST_CLOSED);
        h(true, CellBase.ID_SYSTEM_MESSAGE_REQUEST_CLOSED);
        d = new c(2, CellBase.ID_SYSTEM_MESSAGE_REQUEST_CLOSED);
        e = new c(3, CellBase.ID_SYSTEM_MESSAGE_REQUEST_CLOSED);
    }

    public Loader(String str) {
        this.a = androidx.media3.common.util.b.z0("ExoPlayer:Loader:" + str);
    }

    public static c h(boolean z, long j) {
        return new c(z ? 1 : 0, j);
    }

    @Override // androidx.media3.exoplayer.upstream.c
    public void b() throws IOException {
        k(Integer.MIN_VALUE);
    }

    public void f() {
        ((d) ii.i(this.b)).a(false);
    }

    public void g() {
        this.c = null;
    }

    public boolean i() {
        return this.c != null;
    }

    public boolean j() {
        return this.b != null;
    }

    public void k(int i) throws IOException {
        IOException iOException = this.c;
        if (iOException == null) {
            d<? extends e> dVar = this.b;
            if (dVar != null) {
                if (i == Integer.MIN_VALUE) {
                    i = dVar.a;
                }
                dVar.e(i);
                return;
            }
            return;
        }
        throw iOException;
    }

    public void l() {
        m(null);
    }

    public void m(f fVar) {
        d<? extends e> dVar = this.b;
        if (dVar != null) {
            dVar.a(true);
        }
        if (fVar != null) {
            this.a.execute(new g(fVar));
        }
        this.a.shutdown();
    }

    public <T extends e> long n(T t, b<T> bVar, int i) {
        this.c = null;
        long elapsedRealtime = SystemClock.elapsedRealtime();
        new d((Looper) ii.i(Looper.myLooper()), t, bVar, i, elapsedRealtime).f(0L);
        return elapsedRealtime;
    }
}
