package androidx.media3.exoplayer.upstream;

import java.io.IOException;

/* compiled from: LoaderErrorThrower.java */
/* loaded from: classes.dex */
public interface c {

    /* compiled from: LoaderErrorThrower.java */
    /* loaded from: classes.dex */
    public static final class a implements c {
        @Override // androidx.media3.exoplayer.upstream.c
        public void b() {
        }
    }

    void b() throws IOException;
}
