package androidx.media3.exoplayer;

/* compiled from: RendererCapabilities.java */
/* loaded from: classes.dex */
public interface n {
    int a(androidx.media3.common.j jVar) throws ExoPlaybackException;

    String getName();

    int h();

    int q() throws ExoPlaybackException;
}
