package androidx.media3.exoplayer;

import android.annotation.SuppressLint;
import android.content.Context;
import android.graphics.Rect;
import android.graphics.SurfaceTexture;
import android.media.AudioTrack;
import android.media.MediaFormat;
import android.media.metrics.LogSessionId;
import android.os.Handler;
import android.os.Looper;
import android.util.Pair;
import android.view.Surface;
import android.view.SurfaceHolder;
import android.view.SurfaceView;
import android.view.TextureView;
import androidx.media3.common.IllegalSeekPositionException;
import androidx.media3.common.Metadata;
import androidx.media3.common.PlaybackException;
import androidx.media3.common.i;
import androidx.media3.common.m;
import androidx.media3.common.n;
import androidx.media3.common.p;
import androidx.media3.common.q;
import androidx.media3.common.u;
import androidx.media3.common.x;
import androidx.media3.common.y;
import androidx.media3.common.z;
import androidx.media3.exoplayer.a;
import androidx.media3.exoplayer.b;
import androidx.media3.exoplayer.f;
import androidx.media3.exoplayer.g;
import androidx.media3.exoplayer.h;
import androidx.media3.exoplayer.j;
import androidx.media3.exoplayer.k;
import androidx.media3.exoplayer.o;
import androidx.media3.exoplayer.source.j;
import androidx.media3.exoplayer.video.spherical.SphericalGLSurfaceView;
import com.google.common.collect.ImmutableList;
import defpackage.o02;
import defpackage.qo3;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.concurrent.CopyOnWriteArraySet;
import java.util.concurrent.TimeoutException;
import zendesk.support.request.CellBase;

/* compiled from: ExoPlayerImpl.java */
/* loaded from: classes.dex */
public final class g extends androidx.media3.common.c implements f {
    public final androidx.media3.exoplayer.b A;
    public final o B;
    public final pl4 C;
    public final ep4 D;
    public final long E;
    public int F;
    public boolean G;
    public int H;
    public int I;
    public boolean J;
    public int K;
    public xi3 L;
    public qo3 M;
    public boolean N;
    public q.b O;
    public androidx.media3.common.n P;
    public androidx.media3.common.j Q;
    public androidx.media3.common.j R;
    public AudioTrack S;
    public Object T;
    public Surface U;
    public SurfaceHolder V;
    public SphericalGLSurfaceView W;
    public boolean X;
    public TextureView Y;
    public int Z;
    public int a0;
    public final androidx.media3.exoplayer.trackselection.g b;
    public int b0;
    public final q.b c;
    public int c0;
    public final androidx.media3.common.util.a d;
    public hf0 d0;
    public final Context e;
    public hf0 e0;
    public final q f;
    public int f0;
    public final m[] g;
    public androidx.media3.common.b g0;
    public final androidx.media3.exoplayer.trackselection.f h;
    public float h0;
    public final pj1 i;
    public boolean i0;
    public final h.f j;
    public nb0 j0;
    public final h k;
    public boolean k0;
    public final o02<q.d> l;
    public boolean l0;
    public final CopyOnWriteArraySet<f.a> m;
    public xu2 m0;
    public final u.b n;
    public boolean n0;
    public final List<e> o;
    public androidx.media3.common.h o0;
    public final boolean p;
    public z p0;
    public final j.a q;
    public androidx.media3.common.n q0;
    public final jb r;
    public lr2 r0;
    public final Looper s;
    public int s0;
    public final gm t;
    public int t0;
    public final long u;
    public long u0;
    public final long v;
    public final tz w;
    public final c x;
    public final d y;
    public final androidx.media3.exoplayer.a z;

    /* compiled from: ExoPlayerImpl.java */
    /* loaded from: classes.dex */
    public static final class b {
        public static ks2 a(Context context, g gVar, boolean z) {
            i62 A0 = i62.A0(context);
            if (A0 == null) {
                p12.i("ExoPlayerImpl", "MediaMetricsService unavailable.");
                return new ks2(LogSessionId.LOG_SESSION_ID_NONE);
            }
            if (z) {
                gVar.p1(A0);
            }
            return new ks2(A0.H0());
        }
    }

    /* compiled from: ExoPlayerImpl.java */
    /* loaded from: classes.dex */
    public final class c implements androidx.media3.exoplayer.video.b, androidx.media3.exoplayer.audio.a, n44, o82, SurfaceHolder.Callback, TextureView.SurfaceTextureListener, SphericalGLSurfaceView.b, b.InterfaceC0032b, a.b, o.b, f.a {
        public c() {
        }

        /* JADX INFO: Access modifiers changed from: private */
        public /* synthetic */ void R(q.d dVar) {
            dVar.T(g.this.P);
        }

        @Override // androidx.media3.exoplayer.b.InterfaceC0032b
        public void A(float f) {
            g.this.r2();
        }

        @Override // androidx.media3.exoplayer.audio.a
        public void B(hf0 hf0Var) {
            g.this.e0 = hf0Var;
            g.this.r.B(hf0Var);
        }

        @Override // androidx.media3.exoplayer.video.b
        public void C(long j, int i) {
            g.this.r.C(j, i);
        }

        @Override // androidx.media3.exoplayer.b.InterfaceC0032b
        public void D(int i) {
            boolean k = g.this.k();
            g.this.A2(k, i, g.E1(k, i));
        }

        @Override // androidx.media3.exoplayer.video.spherical.SphericalGLSurfaceView.b
        public void E(Surface surface) {
            g.this.w2(null);
        }

        @Override // androidx.media3.exoplayer.video.spherical.SphericalGLSurfaceView.b
        public void F(Surface surface) {
            g.this.w2(surface);
        }

        @Override // androidx.media3.exoplayer.o.b
        public void G(final int i, final boolean z) {
            g.this.l.l(30, new o02.a() { // from class: j01
                @Override // defpackage.o02.a
                public final void invoke(Object obj) {
                    ((q.d) obj).Q(i, z);
                }
            });
        }

        @Override // androidx.media3.exoplayer.audio.a
        public /* synthetic */ void a(androidx.media3.common.j jVar) {
            hj.a(this, jVar);
        }

        @Override // androidx.media3.exoplayer.audio.a
        public void b(final boolean z) {
            if (g.this.i0 == z) {
                return;
            }
            g.this.i0 = z;
            g.this.l.l(23, new o02.a() { // from class: q01
                @Override // defpackage.o02.a
                public final void invoke(Object obj) {
                    ((q.d) obj).b(z);
                }
            });
        }

        @Override // androidx.media3.exoplayer.audio.a
        public void c(Exception exc) {
            g.this.r.c(exc);
        }

        @Override // androidx.media3.exoplayer.video.b
        public void d(androidx.media3.common.j jVar, jf0 jf0Var) {
            g.this.Q = jVar;
            g.this.r.d(jVar, jf0Var);
        }

        @Override // androidx.media3.exoplayer.video.b
        public void e(String str) {
            g.this.r.e(str);
        }

        @Override // androidx.media3.exoplayer.video.b
        public void f(String str, long j, long j2) {
            g.this.r.f(str, j, j2);
        }

        @Override // androidx.media3.exoplayer.video.b
        public void g(hf0 hf0Var) {
            g.this.r.g(hf0Var);
            g.this.Q = null;
            g.this.d0 = null;
        }

        @Override // androidx.media3.exoplayer.video.b
        public void h(hf0 hf0Var) {
            g.this.d0 = hf0Var;
            g.this.r.h(hf0Var);
        }

        @Override // androidx.media3.exoplayer.video.b
        public /* synthetic */ void i(androidx.media3.common.j jVar) {
            oh4.a(this, jVar);
        }

        @Override // androidx.media3.exoplayer.audio.a
        public void j(hf0 hf0Var) {
            g.this.r.j(hf0Var);
            g.this.R = null;
            g.this.e0 = null;
        }

        @Override // androidx.media3.exoplayer.video.b
        public void k(final z zVar) {
            g.this.p0 = zVar;
            g.this.l.l(25, new o02.a() { // from class: n01
                @Override // defpackage.o02.a
                public final void invoke(Object obj) {
                    ((q.d) obj).k(z.this);
                }
            });
        }

        @Override // androidx.media3.exoplayer.audio.a
        public void l(String str) {
            g.this.r.l(str);
        }

        @Override // androidx.media3.exoplayer.audio.a
        public void m(String str, long j, long j2) {
            g.this.r.m(str, j, j2);
        }

        @Override // androidx.media3.exoplayer.o.b
        public void n(int i) {
            final androidx.media3.common.h v1 = g.v1(g.this.B);
            if (v1.equals(g.this.o0)) {
                return;
            }
            g.this.o0 = v1;
            g.this.l.l(29, new o02.a() { // from class: l01
                @Override // defpackage.o02.a
                public final void invoke(Object obj) {
                    ((q.d) obj).Z(androidx.media3.common.h.this);
                }
            });
        }

        @Override // defpackage.n44
        public void o(final nb0 nb0Var) {
            g.this.j0 = nb0Var;
            g.this.l.l(27, new o02.a() { // from class: k01
                @Override // defpackage.o02.a
                public final void invoke(Object obj) {
                    ((q.d) obj).o(nb0.this);
                }
            });
        }

        @Override // android.view.TextureView.SurfaceTextureListener
        public void onSurfaceTextureAvailable(SurfaceTexture surfaceTexture, int i, int i2) {
            g.this.v2(surfaceTexture);
            g.this.l2(i, i2);
        }

        @Override // android.view.TextureView.SurfaceTextureListener
        public boolean onSurfaceTextureDestroyed(SurfaceTexture surfaceTexture) {
            g.this.w2(null);
            g.this.l2(0, 0);
            return true;
        }

        @Override // android.view.TextureView.SurfaceTextureListener
        public void onSurfaceTextureSizeChanged(SurfaceTexture surfaceTexture, int i, int i2) {
            g.this.l2(i, i2);
        }

        @Override // android.view.TextureView.SurfaceTextureListener
        public void onSurfaceTextureUpdated(SurfaceTexture surfaceTexture) {
        }

        @Override // androidx.media3.exoplayer.video.b
        public void p(int i, long j) {
            g.this.r.p(i, j);
        }

        @Override // androidx.media3.exoplayer.audio.a
        public void q(androidx.media3.common.j jVar, jf0 jf0Var) {
            g.this.R = jVar;
            g.this.r.q(jVar, jf0Var);
        }

        @Override // androidx.media3.exoplayer.video.b
        public void r(Object obj, long j) {
            g.this.r.r(obj, j);
            if (g.this.T == obj) {
                g.this.l.l(26, r01.a);
            }
        }

        @Override // androidx.media3.exoplayer.a.b
        public void s() {
            g.this.A2(false, -1, 3);
        }

        @Override // android.view.SurfaceHolder.Callback
        public void surfaceChanged(SurfaceHolder surfaceHolder, int i, int i2, int i3) {
            g.this.l2(i2, i3);
        }

        @Override // android.view.SurfaceHolder.Callback
        public void surfaceCreated(SurfaceHolder surfaceHolder) {
            if (g.this.X) {
                g.this.w2(surfaceHolder.getSurface());
            }
        }

        @Override // android.view.SurfaceHolder.Callback
        public void surfaceDestroyed(SurfaceHolder surfaceHolder) {
            if (g.this.X) {
                g.this.w2(null);
            }
            g.this.l2(0, 0);
        }

        @Override // defpackage.o82
        public void t(final Metadata metadata) {
            g gVar = g.this;
            gVar.q0 = gVar.q0.b().I(metadata).F();
            androidx.media3.common.n s1 = g.this.s1();
            if (!s1.equals(g.this.P)) {
                g.this.P = s1;
                g.this.l.i(14, new o02.a() { // from class: o01
                    @Override // defpackage.o02.a
                    public final void invoke(Object obj) {
                        g.c.this.R((q.d) obj);
                    }
                });
            }
            g.this.l.i(28, new o02.a() { // from class: m01
                @Override // defpackage.o02.a
                public final void invoke(Object obj) {
                    ((q.d) obj).t(Metadata.this);
                }
            });
            g.this.l.f();
        }

        @Override // defpackage.n44
        public void u(final List<kb0> list) {
            g.this.l.l(27, new o02.a() { // from class: p01
                @Override // defpackage.o02.a
                public final void invoke(Object obj) {
                    ((q.d) obj).u(list);
                }
            });
        }

        @Override // androidx.media3.exoplayer.audio.a
        public void v(long j) {
            g.this.r.v(j);
        }

        @Override // androidx.media3.exoplayer.audio.a
        public void w(Exception exc) {
            g.this.r.w(exc);
        }

        @Override // androidx.media3.exoplayer.video.b
        public void x(Exception exc) {
            g.this.r.x(exc);
        }

        @Override // androidx.media3.exoplayer.f.a
        public void y(boolean z) {
            g.this.D2();
        }

        @Override // androidx.media3.exoplayer.audio.a
        public void z(int i, long j, long j2) {
            g.this.r.z(i, j, j2);
        }
    }

    /* compiled from: ExoPlayerImpl.java */
    /* loaded from: classes.dex */
    public static final class d implements lh4, ev, k.b {
        public lh4 a;
        public ev f0;
        public lh4 g0;
        public ev h0;

        public d() {
        }

        @Override // defpackage.ev
        public void a(long j, float[] fArr) {
            ev evVar = this.h0;
            if (evVar != null) {
                evVar.a(j, fArr);
            }
            ev evVar2 = this.f0;
            if (evVar2 != null) {
                evVar2.a(j, fArr);
            }
        }

        @Override // defpackage.ev
        public void b() {
            ev evVar = this.h0;
            if (evVar != null) {
                evVar.b();
            }
            ev evVar2 = this.f0;
            if (evVar2 != null) {
                evVar2.b();
            }
        }

        @Override // defpackage.lh4
        public void e(long j, long j2, androidx.media3.common.j jVar, MediaFormat mediaFormat) {
            lh4 lh4Var = this.g0;
            if (lh4Var != null) {
                lh4Var.e(j, j2, jVar, mediaFormat);
            }
            lh4 lh4Var2 = this.a;
            if (lh4Var2 != null) {
                lh4Var2.e(j, j2, jVar, mediaFormat);
            }
        }

        @Override // androidx.media3.exoplayer.k.b
        public void s(int i, Object obj) {
            if (i == 7) {
                this.a = (lh4) obj;
            } else if (i == 8) {
                this.f0 = (ev) obj;
            } else if (i != 10000) {
            } else {
                SphericalGLSurfaceView sphericalGLSurfaceView = (SphericalGLSurfaceView) obj;
                if (sphericalGLSurfaceView == null) {
                    this.g0 = null;
                    this.h0 = null;
                    return;
                }
                this.g0 = sphericalGLSurfaceView.getVideoFrameMetadataListener();
                this.h0 = sphericalGLSurfaceView.getCameraMotionListener();
            }
        }
    }

    /* compiled from: ExoPlayerImpl.java */
    /* loaded from: classes.dex */
    public static final class e implements z62 {
        public final Object a;
        public u b;

        public e(Object obj, u uVar) {
            this.a = obj;
            this.b = uVar;
        }

        @Override // defpackage.z62
        public Object a() {
            return this.a;
        }

        @Override // defpackage.z62
        public u b() {
            return this.b;
        }
    }

    static {
        f62.a("media3.exoplayer");
    }

    @SuppressLint({"HandlerLeak"})
    public g(f.b bVar, q qVar) {
        g gVar;
        Context applicationContext;
        jb apply;
        c cVar;
        d dVar;
        Handler handler;
        androidx.media3.exoplayer.trackselection.f fVar;
        gm gmVar;
        Looper looper;
        int i;
        ks2 a2;
        h hVar;
        androidx.media3.common.util.a aVar = new androidx.media3.common.util.a();
        this.d = aVar;
        try {
            p12.f("ExoPlayerImpl", "Init " + Integer.toHexString(System.identityHashCode(this)) + " [AndroidXMedia3/1.0.0-beta02] [" + androidx.media3.common.util.b.e + "]");
            applicationContext = bVar.a.getApplicationContext();
            this.e = applicationContext;
            apply = bVar.i.apply(bVar.b);
            this.r = apply;
            this.m0 = bVar.k;
            this.g0 = bVar.l;
            this.Z = bVar.q;
            this.a0 = bVar.r;
            this.i0 = bVar.p;
            this.E = bVar.y;
            cVar = new c();
            this.x = cVar;
            dVar = new d();
            this.y = dVar;
            handler = new Handler(bVar.j);
            m[] a3 = bVar.d.get().a(handler, cVar, cVar, cVar, cVar);
            this.g = a3;
            ii.g(a3.length > 0);
            fVar = bVar.f.get();
            this.h = fVar;
            this.q = bVar.e.get();
            gmVar = bVar.h.get();
            this.t = gmVar;
            this.p = bVar.s;
            this.L = bVar.t;
            this.u = bVar.u;
            this.v = bVar.v;
            this.N = bVar.z;
            looper = bVar.j;
            this.s = looper;
            tz tzVar = bVar.b;
            this.w = tzVar;
            q qVar2 = qVar == null ? this : qVar;
            this.f = qVar2;
            this.l = new o02<>(looper, tzVar, new o02.b() { // from class: yz0
                @Override // defpackage.o02.b
                public final void a(Object obj, i iVar) {
                    g.this.N1((q.d) obj, iVar);
                }
            });
            this.m = new CopyOnWriteArraySet<>();
            this.o = new ArrayList();
            this.M = new qo3.a(0);
            androidx.media3.exoplayer.trackselection.g gVar2 = new androidx.media3.exoplayer.trackselection.g(new v63[a3.length], new androidx.media3.exoplayer.trackselection.c[a3.length], y.f0, null);
            this.b = gVar2;
            this.n = new u.b();
            q.b e2 = new q.b.a().c(1, 2, 3, 13, 14, 15, 16, 17, 18, 19, 31, 20, 30, 21, 22, 23, 24, 25, 26, 27, 28).d(29, fVar.e()).e();
            this.c = e2;
            this.O = new q.b.a().b(e2).a(4).a(10).e();
            this.i = tzVar.c(looper, null);
            h.f fVar2 = new h.f() { // from class: zz0
                @Override // androidx.media3.exoplayer.h.f
                public final void a(h.e eVar) {
                    g.this.P1(eVar);
                }
            };
            this.j = fVar2;
            this.r0 = lr2.j(gVar2);
            apply.e0(qVar2, looper);
            i = androidx.media3.common.util.b.a;
            if (i < 31) {
                a2 = new ks2();
            } else {
                a2 = b.a(applicationContext, this, bVar.A);
            }
            try {
                hVar = new h(a3, fVar, gVar2, bVar.g.get(), gmVar, this.F, this.G, apply, this.L, bVar.w, bVar.x, this.N, looper, tzVar, fVar2, a2);
                gVar = this;
            } catch (Throwable th) {
                th = th;
                gVar = this;
            }
        } catch (Throwable th2) {
            th = th2;
            gVar = this;
        }
        try {
            gVar.k = hVar;
            gVar.h0 = 1.0f;
            gVar.F = 0;
            androidx.media3.common.n nVar = androidx.media3.common.n.K0;
            gVar.P = nVar;
            gVar.q0 = nVar;
            gVar.s0 = -1;
            if (i < 21) {
                gVar.f0 = gVar.K1(0);
            } else {
                gVar.f0 = androidx.media3.common.util.b.D(applicationContext);
            }
            gVar.j0 = nb0.f0;
            gVar.k0 = true;
            gVar.O(apply);
            gmVar.c(new Handler(looper), apply);
            gVar.q1(cVar);
            long j = bVar.c;
            if (j > 0) {
                hVar.t(j);
            }
            androidx.media3.exoplayer.a aVar2 = new androidx.media3.exoplayer.a(bVar.a, handler, cVar);
            gVar.z = aVar2;
            aVar2.b(bVar.o);
            androidx.media3.exoplayer.b bVar2 = new androidx.media3.exoplayer.b(bVar.a, handler, cVar);
            gVar.A = bVar2;
            bVar2.m(bVar.m ? gVar.g0 : null);
            o oVar = new o(bVar.a, handler, cVar);
            gVar.B = oVar;
            oVar.h(androidx.media3.common.util.b.c0(gVar.g0.g0));
            pl4 pl4Var = new pl4(bVar.a);
            gVar.C = pl4Var;
            pl4Var.a(bVar.n != 0);
            ep4 ep4Var = new ep4(bVar.a);
            gVar.D = ep4Var;
            ep4Var.a(bVar.n == 2);
            gVar.o0 = v1(oVar);
            gVar.p0 = z.i0;
            fVar.i(gVar.g0);
            gVar.q2(1, 10, Integer.valueOf(gVar.f0));
            gVar.q2(2, 10, Integer.valueOf(gVar.f0));
            gVar.q2(1, 3, gVar.g0);
            gVar.q2(2, 4, Integer.valueOf(gVar.Z));
            gVar.q2(2, 5, Integer.valueOf(gVar.a0));
            gVar.q2(1, 9, Boolean.valueOf(gVar.i0));
            gVar.q2(2, 7, dVar);
            gVar.q2(6, 8, dVar);
            aVar.e();
        } catch (Throwable th3) {
            th = th3;
            gVar.d.e();
            throw th;
        }
    }

    public static int E1(boolean z, int i) {
        return (!z || i == 1) ? 1 : 2;
    }

    public static long I1(lr2 lr2Var) {
        u.c cVar = new u.c();
        u.b bVar = new u.b();
        lr2Var.a.h(lr2Var.b.a, bVar);
        if (lr2Var.c == CellBase.ID_SYSTEM_MESSAGE_REQUEST_CLOSED) {
            return lr2Var.a.n(bVar.g0, cVar).f();
        }
        return bVar.p() + lr2Var.c;
    }

    public static boolean L1(lr2 lr2Var) {
        return lr2Var.e == 3 && lr2Var.l && lr2Var.m == 0;
    }

    /* JADX INFO: Access modifiers changed from: private */
    public /* synthetic */ void N1(q.d dVar, androidx.media3.common.i iVar) {
        dVar.J(this.f, new q.c(iVar));
    }

    /* JADX INFO: Access modifiers changed from: private */
    public /* synthetic */ void P1(final h.e eVar) {
        this.i.c(new Runnable() { // from class: a01
            @Override // java.lang.Runnable
            public final void run() {
                g.this.O1(eVar);
            }
        });
    }

    public static /* synthetic */ void Q1(q.d dVar) {
        dVar.h0(ExoPlaybackException.createForUnexpected(new ExoTimeoutException(1), PlaybackException.ERROR_CODE_TIMEOUT));
    }

    /* JADX INFO: Access modifiers changed from: private */
    public /* synthetic */ void U1(q.d dVar) {
        dVar.l0(this.O);
    }

    public static /* synthetic */ void V1(lr2 lr2Var, int i, q.d dVar) {
        dVar.L(lr2Var.a, i);
    }

    public static /* synthetic */ void W1(int i, q.e eVar, q.e eVar2, q.d dVar) {
        dVar.F(i);
        dVar.m0(eVar, eVar2, i);
    }

    public static /* synthetic */ void Y1(lr2 lr2Var, q.d dVar) {
        dVar.c0(lr2Var.f);
    }

    public static /* synthetic */ void Z1(lr2 lr2Var, q.d dVar) {
        dVar.h0(lr2Var.f);
    }

    public static /* synthetic */ void a2(lr2 lr2Var, q.d dVar) {
        dVar.Y(lr2Var.i.d);
    }

    public static /* synthetic */ void c2(lr2 lr2Var, q.d dVar) {
        dVar.E(lr2Var.g);
        dVar.H(lr2Var.g);
    }

    public static /* synthetic */ void d2(lr2 lr2Var, q.d dVar) {
        dVar.R(lr2Var.l, lr2Var.e);
    }

    public static /* synthetic */ void e2(lr2 lr2Var, q.d dVar) {
        dVar.i(lr2Var.e);
    }

    public static /* synthetic */ void f2(lr2 lr2Var, int i, q.d dVar) {
        dVar.y(lr2Var.l, i);
    }

    public static /* synthetic */ void g2(lr2 lr2Var, q.d dVar) {
        dVar.D(lr2Var.m);
    }

    public static /* synthetic */ void h2(lr2 lr2Var, q.d dVar) {
        dVar.p0(L1(lr2Var));
    }

    public static /* synthetic */ void i2(lr2 lr2Var, q.d dVar) {
        dVar.n(lr2Var.n);
    }

    public static androidx.media3.common.h v1(o oVar) {
        return new androidx.media3.common.h(0, oVar.d(), oVar.c());
    }

    public boolean A1() {
        E2();
        return this.r0.o;
    }

    public final void A2(boolean z, int i, int i2) {
        int i3 = 0;
        boolean z2 = z && i != -1;
        if (z2 && i != 1) {
            i3 = 1;
        }
        lr2 lr2Var = this.r0;
        if (lr2Var.l == z2 && lr2Var.m == i3) {
            return;
        }
        this.H++;
        lr2 d2 = lr2Var.d(z2, i3);
        this.k.O0(z2, i3);
        B2(d2, 0, i2, false, false, 5, CellBase.ID_SYSTEM_MESSAGE_REQUEST_CLOSED, -1);
    }

    @Override // androidx.media3.common.q
    public int B() {
        E2();
        return this.r0.e;
    }

    public final long B1(lr2 lr2Var) {
        if (lr2Var.a.q()) {
            return androidx.media3.common.util.b.y0(this.u0);
        }
        if (lr2Var.b.b()) {
            return lr2Var.r;
        }
        return m2(lr2Var.a, lr2Var.b, lr2Var.r);
    }

    public final void B2(final lr2 lr2Var, final int i, final int i2, boolean z, boolean z2, final int i3, long j, int i4) {
        lr2 lr2Var2 = this.r0;
        this.r0 = lr2Var;
        Pair<Boolean, Integer> z1 = z1(lr2Var, lr2Var2, z2, i3, !lr2Var2.a.equals(lr2Var.a));
        boolean booleanValue = ((Boolean) z1.first).booleanValue();
        final int intValue = ((Integer) z1.second).intValue();
        androidx.media3.common.n nVar = this.P;
        if (booleanValue) {
            r3 = lr2Var.a.q() ? null : lr2Var.a.n(lr2Var.a.h(lr2Var.b.a, this.n).g0, this.a).g0;
            this.q0 = androidx.media3.common.n.K0;
        }
        if (booleanValue || !lr2Var2.j.equals(lr2Var.j)) {
            this.q0 = this.q0.b().J(lr2Var.j).F();
            nVar = s1();
        }
        boolean z3 = !nVar.equals(this.P);
        this.P = nVar;
        boolean z4 = lr2Var2.l != lr2Var.l;
        boolean z5 = lr2Var2.e != lr2Var.e;
        if (z5 || z4) {
            D2();
        }
        boolean z6 = lr2Var2.g;
        boolean z7 = lr2Var.g;
        boolean z8 = z6 != z7;
        if (z8) {
            C2(z7);
        }
        if (!lr2Var2.a.equals(lr2Var.a)) {
            this.l.i(0, new o02.a() { // from class: pz0
                @Override // defpackage.o02.a
                public final void invoke(Object obj) {
                    g.V1(lr2.this, i, (q.d) obj);
                }
            });
        }
        if (z2) {
            final q.e H1 = H1(i3, lr2Var2, i4);
            final q.e G1 = G1(j);
            this.l.i(11, new o02.a() { // from class: b01
                @Override // defpackage.o02.a
                public final void invoke(Object obj) {
                    g.W1(i3, H1, G1, (q.d) obj);
                }
            });
        }
        if (booleanValue) {
            this.l.i(1, new o02.a() { // from class: qz0
                @Override // defpackage.o02.a
                public final void invoke(Object obj) {
                    ((q.d) obj).a0(m.this, intValue);
                }
            });
        }
        if (lr2Var2.f != lr2Var.f) {
            this.l.i(10, new o02.a() { // from class: f01
                @Override // defpackage.o02.a
                public final void invoke(Object obj) {
                    g.Y1(lr2.this, (q.d) obj);
                }
            });
            if (lr2Var.f != null) {
                this.l.i(10, new o02.a() { // from class: mz0
                    @Override // defpackage.o02.a
                    public final void invoke(Object obj) {
                        g.Z1(lr2.this, (q.d) obj);
                    }
                });
            }
        }
        androidx.media3.exoplayer.trackselection.g gVar = lr2Var2.i;
        androidx.media3.exoplayer.trackselection.g gVar2 = lr2Var.i;
        if (gVar != gVar2) {
            this.h.f(gVar2.e);
            this.l.i(2, new o02.a() { // from class: nz0
                @Override // defpackage.o02.a
                public final void invoke(Object obj) {
                    g.a2(lr2.this, (q.d) obj);
                }
            });
        }
        if (z3) {
            final androidx.media3.common.n nVar2 = this.P;
            this.l.i(14, new o02.a() { // from class: rz0
                @Override // defpackage.o02.a
                public final void invoke(Object obj) {
                    ((q.d) obj).T(n.this);
                }
            });
        }
        if (z8) {
            this.l.i(3, new o02.a() { // from class: c01
                @Override // defpackage.o02.a
                public final void invoke(Object obj) {
                    g.c2(lr2.this, (q.d) obj);
                }
            });
        }
        if (z5 || z4) {
            this.l.i(-1, new o02.a() { // from class: d01
                @Override // defpackage.o02.a
                public final void invoke(Object obj) {
                    g.d2(lr2.this, (q.d) obj);
                }
            });
        }
        if (z5) {
            this.l.i(4, new o02.a() { // from class: h01
                @Override // defpackage.o02.a
                public final void invoke(Object obj) {
                    g.e2(lr2.this, (q.d) obj);
                }
            });
        }
        if (z4) {
            this.l.i(5, new o02.a() { // from class: oz0
                @Override // defpackage.o02.a
                public final void invoke(Object obj) {
                    g.f2(lr2.this, i2, (q.d) obj);
                }
            });
        }
        if (lr2Var2.m != lr2Var.m) {
            this.l.i(6, new o02.a() { // from class: i01
                @Override // defpackage.o02.a
                public final void invoke(Object obj) {
                    g.g2(lr2.this, (q.d) obj);
                }
            });
        }
        if (L1(lr2Var2) != L1(lr2Var)) {
            this.l.i(7, new o02.a() { // from class: e01
                @Override // defpackage.o02.a
                public final void invoke(Object obj) {
                    g.h2(lr2.this, (q.d) obj);
                }
            });
        }
        if (!lr2Var2.n.equals(lr2Var.n)) {
            this.l.i(12, new o02.a() { // from class: g01
                @Override // defpackage.o02.a
                public final void invoke(Object obj) {
                    g.i2(lr2.this, (q.d) obj);
                }
            });
        }
        if (z) {
            this.l.i(-1, xz0.a);
        }
        z2();
        this.l.f();
        if (lr2Var2.o != lr2Var.o) {
            Iterator<f.a> it = this.m.iterator();
            while (it.hasNext()) {
                it.next().y(lr2Var.o);
            }
        }
    }

    @Override // androidx.media3.common.q
    public y C() {
        E2();
        return this.r0.i.d;
    }

    public final int C1() {
        if (this.r0.a.q()) {
            return this.s0;
        }
        lr2 lr2Var = this.r0;
        return lr2Var.a.h(lr2Var.b.a, this.n).g0;
    }

    public final void C2(boolean z) {
        xu2 xu2Var = this.m0;
        if (xu2Var != null) {
            if (z && !this.n0) {
                xu2Var.a(0);
                this.n0 = true;
            } else if (z || !this.n0) {
            } else {
                xu2Var.b(0);
                this.n0 = false;
            }
        }
    }

    public final Pair<Object, Long> D1(u uVar, u uVar2) {
        long y = y();
        if (!uVar.q() && !uVar2.q()) {
            Pair<Object, Long> j = uVar.j(this.a, this.n, I(), androidx.media3.common.util.b.y0(y));
            Object obj = ((Pair) androidx.media3.common.util.b.j(j)).first;
            if (uVar2.b(obj) != -1) {
                return j;
            }
            Object x0 = h.x0(this.a, this.n, this.F, this.G, obj, uVar, uVar2);
            if (x0 != null) {
                uVar2.h(x0, this.n);
                int i = this.n.g0;
                return k2(uVar2, i, uVar2.n(i, this.a).e());
            }
            return k2(uVar2, -1, CellBase.ID_SYSTEM_MESSAGE_REQUEST_CLOSED);
        }
        boolean z = !uVar.q() && uVar2.q();
        int C1 = z ? -1 : C1();
        if (z) {
            y = -9223372036854775807L;
        }
        return k2(uVar2, C1, y);
    }

    public final void D2() {
        int B = B();
        boolean z = true;
        if (B != 1) {
            if (B == 2 || B == 3) {
                boolean A1 = A1();
                pl4 pl4Var = this.C;
                if (!k() || A1) {
                    z = false;
                }
                pl4Var.b(z);
                this.D.b(k());
                return;
            } else if (B != 4) {
                throw new IllegalStateException();
            }
        }
        this.C.b(false);
        this.D.b(false);
    }

    public final void E2() {
        this.d.b();
        if (Thread.currentThread() != T().getThread()) {
            String A = androidx.media3.common.util.b.A("Player is accessed on the wrong thread.\nCurrent thread: '%s'\nExpected thread: '%s'\nSee https://exoplayer.dev/issues/player-accessed-on-wrong-thread", Thread.currentThread().getName(), T().getThread().getName());
            if (!this.k0) {
                p12.j("ExoPlayerImpl", A, this.l0 ? null : new IllegalStateException());
                this.l0 = true;
                return;
            }
            throw new IllegalStateException(A);
        }
    }

    @Override // androidx.media3.common.q
    public nb0 F() {
        E2();
        return this.j0;
    }

    @Override // androidx.media3.common.q
    /* renamed from: F1 */
    public ExoPlaybackException v() {
        E2();
        return this.r0.f;
    }

    @Override // androidx.media3.common.q
    public void G(q.d dVar) {
        ii.e(dVar);
        this.l.k(dVar);
    }

    public final q.e G1(long j) {
        int i;
        androidx.media3.common.m mVar;
        Object obj;
        int I = I();
        Object obj2 = null;
        if (this.r0.a.q()) {
            i = -1;
            mVar = null;
            obj = null;
        } else {
            lr2 lr2Var = this.r0;
            Object obj3 = lr2Var.b.a;
            lr2Var.a.h(obj3, this.n);
            i = this.r0.a.b(obj3);
            obj = obj3;
            obj2 = this.r0.a.n(I, this.a).a;
            mVar = this.a.g0;
        }
        long U0 = androidx.media3.common.util.b.U0(j);
        long U02 = this.r0.b.b() ? androidx.media3.common.util.b.U0(I1(this.r0)) : U0;
        j.b bVar = this.r0.b;
        return new q.e(obj2, I, mVar, obj, i, U0, U02, bVar.b, bVar.c);
    }

    @Override // androidx.media3.common.q
    public int H() {
        E2();
        if (g()) {
            return this.r0.b.b;
        }
        return -1;
    }

    public final q.e H1(int i, lr2 lr2Var, int i2) {
        int i3;
        int i4;
        Object obj;
        androidx.media3.common.m mVar;
        Object obj2;
        long j;
        long I1;
        u.b bVar = new u.b();
        if (lr2Var.a.q()) {
            i3 = i2;
            i4 = -1;
            obj = null;
            mVar = null;
            obj2 = null;
        } else {
            Object obj3 = lr2Var.b.a;
            lr2Var.a.h(obj3, bVar);
            int i5 = bVar.g0;
            i3 = i5;
            obj2 = obj3;
            i4 = lr2Var.a.b(obj3);
            obj = lr2Var.a.n(i5, this.a).a;
            mVar = this.a.g0;
        }
        if (i == 0) {
            if (lr2Var.b.b()) {
                j.b bVar2 = lr2Var.b;
                j = bVar.d(bVar2.b, bVar2.c);
                I1 = I1(lr2Var);
            } else {
                if (lr2Var.b.e != -1) {
                    j = I1(this.r0);
                } else {
                    j = bVar.i0 + bVar.h0;
                }
                I1 = j;
            }
        } else if (lr2Var.b.b()) {
            j = lr2Var.r;
            I1 = I1(lr2Var);
        } else {
            j = bVar.i0 + lr2Var.r;
            I1 = j;
        }
        long U0 = androidx.media3.common.util.b.U0(j);
        long U02 = androidx.media3.common.util.b.U0(I1);
        j.b bVar3 = lr2Var.b;
        return new q.e(obj, i3, mVar, obj2, i4, U0, U02, bVar3.b, bVar3.c);
    }

    @Override // androidx.media3.common.q
    public int I() {
        E2();
        int C1 = C1();
        if (C1 == -1) {
            return 0;
        }
        return C1;
    }

    /* renamed from: J1 */
    public final void O1(h.e eVar) {
        long j;
        boolean z;
        long j2;
        int i = this.H - eVar.c;
        this.H = i;
        boolean z2 = true;
        if (eVar.d) {
            this.I = eVar.e;
            this.J = true;
        }
        if (eVar.f) {
            this.K = eVar.g;
        }
        if (i == 0) {
            u uVar = eVar.b.a;
            if (!this.r0.a.q() && uVar.q()) {
                this.s0 = -1;
                this.u0 = 0L;
                this.t0 = 0;
            }
            if (!uVar.q()) {
                List<u> G = ((ls2) uVar).G();
                ii.g(G.size() == this.o.size());
                for (int i2 = 0; i2 < G.size(); i2++) {
                    this.o.get(i2).b = G.get(i2);
                }
            }
            if (this.J) {
                if (eVar.b.b.equals(this.r0.b) && eVar.b.d == this.r0.r) {
                    z2 = false;
                }
                if (z2) {
                    if (!uVar.q() && !eVar.b.b.b()) {
                        lr2 lr2Var = eVar.b;
                        j2 = m2(uVar, lr2Var.b, lr2Var.d);
                    } else {
                        j2 = eVar.b.d;
                    }
                    j = j2;
                } else {
                    j = -9223372036854775807L;
                }
                z = z2;
            } else {
                j = -9223372036854775807L;
                z = false;
            }
            this.J = false;
            B2(eVar.b, 1, this.K, false, z, this.I, j, -1);
        }
    }

    @Override // androidx.media3.common.q
    public void K(final int i) {
        E2();
        if (this.F != i) {
            this.F = i;
            this.k.S0(i);
            this.l.i(8, new o02.a() { // from class: lz0
                @Override // defpackage.o02.a
                public final void invoke(Object obj) {
                    ((q.d) obj).s(i);
                }
            });
            z2();
            this.l.f();
        }
    }

    public final int K1(int i) {
        AudioTrack audioTrack = this.S;
        if (audioTrack != null && audioTrack.getAudioSessionId() != i) {
            this.S.release();
            this.S = null;
        }
        if (this.S == null) {
            this.S = new AudioTrack(3, 4000, 4, 2, 2, 0, i);
        }
        return this.S.getAudioSessionId();
    }

    @Override // androidx.media3.common.q
    public void L(final x xVar) {
        E2();
        if (!this.h.e() || xVar.equals(this.h.b())) {
            return;
        }
        this.h.j(xVar);
        this.l.l(19, new o02.a() { // from class: sz0
            @Override // defpackage.o02.a
            public final void invoke(Object obj) {
                ((q.d) obj).V(x.this);
            }
        });
    }

    @Override // androidx.media3.common.q
    public void M(SurfaceView surfaceView) {
        E2();
        u1(surfaceView == null ? null : surfaceView.getHolder());
    }

    @Override // androidx.media3.common.q
    public void O(q.d dVar) {
        ii.e(dVar);
        this.l.c(dVar);
    }

    @Override // androidx.media3.common.q
    public int P() {
        E2();
        return this.r0.m;
    }

    @Override // androidx.media3.common.q
    public int Q() {
        E2();
        return this.F;
    }

    @Override // androidx.media3.common.q
    public long R() {
        E2();
        if (g()) {
            lr2 lr2Var = this.r0;
            j.b bVar = lr2Var.b;
            lr2Var.a.h(bVar.a, this.n);
            return androidx.media3.common.util.b.U0(this.n.d(bVar.b, bVar.c));
        }
        return f0();
    }

    @Override // androidx.media3.common.q
    public u S() {
        E2();
        return this.r0.a;
    }

    @Override // androidx.media3.common.q
    public Looper T() {
        return this.s;
    }

    @Override // androidx.media3.common.q
    public boolean U() {
        E2();
        return this.G;
    }

    @Override // androidx.media3.common.q
    public x V() {
        E2();
        return this.h.b();
    }

    @Override // androidx.media3.common.q
    public long W() {
        E2();
        if (this.r0.a.q()) {
            return this.u0;
        }
        lr2 lr2Var = this.r0;
        if (lr2Var.k.d != lr2Var.b.d) {
            return lr2Var.a.n(I(), this.a).g();
        }
        long j = lr2Var.p;
        if (this.r0.k.b()) {
            lr2 lr2Var2 = this.r0;
            u.b h = lr2Var2.a.h(lr2Var2.k.a, this.n);
            long h2 = h.h(this.r0.k.b);
            j = h2 == Long.MIN_VALUE ? h.h0 : h2;
        }
        lr2 lr2Var3 = this.r0;
        return androidx.media3.common.util.b.U0(m2(lr2Var3.a, lr2Var3.k, j));
    }

    @Override // androidx.media3.common.q
    public void Z(TextureView textureView) {
        E2();
        if (textureView == null) {
            t1();
            return;
        }
        p2();
        this.Y = textureView;
        if (textureView.getSurfaceTextureListener() != null) {
            p12.i("ExoPlayerImpl", "Replacing existing SurfaceTextureListener.");
        }
        textureView.setSurfaceTextureListener(this.x);
        SurfaceTexture surfaceTexture = textureView.isAvailable() ? textureView.getSurfaceTexture() : null;
        if (surfaceTexture == null) {
            w2(null);
            l2(0, 0);
            return;
        }
        v2(surfaceTexture);
        l2(textureView.getWidth(), textureView.getHeight());
    }

    @Override // androidx.media3.common.q
    public void a() {
        AudioTrack audioTrack;
        p12.f("ExoPlayerImpl", "Release " + Integer.toHexString(System.identityHashCode(this)) + " [AndroidXMedia3/1.0.0-beta02] [" + androidx.media3.common.util.b.e + "] [" + f62.b() + "]");
        E2();
        if (androidx.media3.common.util.b.a < 21 && (audioTrack = this.S) != null) {
            audioTrack.release();
            this.S = null;
        }
        this.z.b(false);
        this.B.g();
        this.C.b(false);
        this.D.b(false);
        this.A.i();
        if (!this.k.j0()) {
            this.l.l(10, vz0.a);
        }
        this.l.j();
        this.i.k(null);
        this.t.g(this.r);
        lr2 g = this.r0.g(1);
        this.r0 = g;
        lr2 b2 = g.b(g.b);
        this.r0 = b2;
        b2.p = b2.r;
        this.r0.q = 0L;
        this.r.a();
        this.h.g();
        p2();
        Surface surface = this.U;
        if (surface != null) {
            surface.release();
            this.U = null;
        }
        if (this.n0) {
            ((xu2) ii.e(this.m0)).b(0);
            this.n0 = false;
        }
        this.j0 = nb0.f0;
    }

    @Override // androidx.media3.common.q
    public void b(p pVar) {
        E2();
        if (pVar == null) {
            pVar = p.h0;
        }
        if (this.r0.n.equals(pVar)) {
            return;
        }
        lr2 f = this.r0.f(pVar);
        this.H++;
        this.k.Q0(pVar);
        B2(f, 0, 1, false, false, 5, CellBase.ID_SYSTEM_MESSAGE_REQUEST_CLOSED, -1);
    }

    @Override // androidx.media3.common.q
    public androidx.media3.common.n b0() {
        E2();
        return this.P;
    }

    @Override // androidx.media3.common.q
    public long c0() {
        E2();
        return androidx.media3.common.util.b.U0(B1(this.r0));
    }

    @Override // androidx.media3.common.q
    public void d() {
        E2();
        boolean k = k();
        int p = this.A.p(k, 2);
        A2(k, p, E1(k, p));
        lr2 lr2Var = this.r0;
        if (lr2Var.e != 1) {
            return;
        }
        lr2 e2 = lr2Var.e(null);
        lr2 g = e2.g(e2.a.q() ? 4 : 2);
        this.H++;
        this.k.h0();
        B2(g, 1, 1, false, false, 5, CellBase.ID_SYSTEM_MESSAGE_REQUEST_CLOSED, -1);
    }

    @Override // androidx.media3.common.q
    public long d0() {
        E2();
        return this.u;
    }

    @Override // androidx.media3.common.q
    public p e() {
        E2();
        return this.r0.n;
    }

    @Override // androidx.media3.common.q
    public boolean g() {
        E2();
        return this.r0.b.b();
    }

    @Override // androidx.media3.common.q
    public long h() {
        E2();
        return androidx.media3.common.util.b.U0(this.r0.q);
    }

    @Override // androidx.media3.common.q
    public void i(int i, long j) {
        E2();
        this.r.M();
        u uVar = this.r0.a;
        if (i >= 0 && (uVar.q() || i < uVar.p())) {
            this.H++;
            if (g()) {
                p12.i("ExoPlayerImpl", "seekTo ignored because an ad is playing");
                h.e eVar = new h.e(this.r0);
                eVar.b(1);
                this.j.a(eVar);
                return;
            }
            int i2 = B() != 1 ? 2 : 1;
            int I = I();
            lr2 j2 = j2(this.r0.g(i2), uVar, k2(uVar, i, j));
            this.k.z0(uVar, i, androidx.media3.common.util.b.y0(j));
            B2(j2, 0, 1, true, true, 1, B1(j2), I);
            return;
        }
        throw new IllegalSeekPositionException(uVar, i, j);
    }

    @Override // androidx.media3.common.q
    public q.b j() {
        E2();
        return this.O;
    }

    public final lr2 j2(lr2 lr2Var, u uVar, Pair<Object, Long> pair) {
        int i;
        long j;
        ii.a(uVar.q() || pair != null);
        u uVar2 = lr2Var.a;
        lr2 i2 = lr2Var.i(uVar);
        if (uVar.q()) {
            j.b k = lr2.k();
            long y0 = androidx.media3.common.util.b.y0(this.u0);
            lr2 b2 = i2.c(k, y0, y0, y0, 0L, c84.h0, this.b, ImmutableList.of()).b(k);
            b2.p = b2.r;
            return b2;
        }
        Object obj = i2.b.a;
        boolean z = !obj.equals(((Pair) androidx.media3.common.util.b.j(pair)).first);
        j.b bVar = z ? new j.b(pair.first) : i2.b;
        long longValue = ((Long) pair.second).longValue();
        long y02 = androidx.media3.common.util.b.y0(y());
        if (!uVar2.q()) {
            y02 -= uVar2.h(obj, this.n).p();
        }
        if (z || longValue < y02) {
            ii.g(!bVar.b());
            lr2 b3 = i2.c(bVar, longValue, longValue, longValue, 0L, z ? c84.h0 : i2.h, z ? this.b : i2.i, z ? ImmutableList.of() : i2.j).b(bVar);
            b3.p = longValue;
            return b3;
        }
        if (i == 0) {
            int b4 = uVar.b(i2.k.a);
            if (b4 == -1 || uVar.f(b4, this.n).g0 != uVar.h(bVar.a, this.n).g0) {
                uVar.h(bVar.a, this.n);
                if (bVar.b()) {
                    j = this.n.d(bVar.b, bVar.c);
                } else {
                    j = this.n.h0;
                }
                i2 = i2.c(bVar, i2.r, i2.r, i2.d, j - i2.r, i2.h, i2.i, i2.j).b(bVar);
                i2.p = j;
            }
        } else {
            ii.g(!bVar.b());
            long max = Math.max(0L, i2.q - (longValue - y02));
            long j2 = i2.p;
            if (i2.k.equals(i2.b)) {
                j2 = longValue + max;
            }
            i2 = i2.c(bVar, longValue, longValue, longValue, max, i2.h, i2.i, i2.j);
            i2.p = j2;
        }
        return i2;
    }

    @Override // androidx.media3.common.q
    public boolean k() {
        E2();
        return this.r0.l;
    }

    public final Pair<Object, Long> k2(u uVar, int i, long j) {
        if (uVar.q()) {
            this.s0 = i;
            if (j == CellBase.ID_SYSTEM_MESSAGE_REQUEST_CLOSED) {
                j = 0;
            }
            this.u0 = j;
            this.t0 = 0;
            return null;
        }
        if (i == -1 || i >= uVar.p()) {
            i = uVar.a(this.G);
            j = uVar.n(i, this.a).e();
        }
        return uVar.j(this.a, this.n, i, androidx.media3.common.util.b.y0(j));
    }

    @Override // androidx.media3.common.q
    public void l(final boolean z) {
        E2();
        if (this.G != z) {
            this.G = z;
            this.k.V0(z);
            this.l.i(9, new o02.a() { // from class: uz0
                @Override // defpackage.o02.a
                public final void invoke(Object obj) {
                    ((q.d) obj).N(z);
                }
            });
            z2();
            this.l.f();
        }
    }

    public final void l2(final int i, final int i2) {
        if (i == this.b0 && i2 == this.c0) {
            return;
        }
        this.b0 = i;
        this.c0 = i2;
        this.l.l(24, new o02.a() { // from class: wz0
            @Override // defpackage.o02.a
            public final void invoke(Object obj) {
                ((q.d) obj).k0(i, i2);
            }
        });
    }

    @Override // androidx.media3.common.q
    public long m() {
        E2();
        return 3000L;
    }

    public final long m2(u uVar, j.b bVar, long j) {
        uVar.h(bVar.a, this.n);
        return j + this.n.p();
    }

    @Override // androidx.media3.common.q
    public int n() {
        E2();
        if (this.r0.a.q()) {
            return this.t0;
        }
        lr2 lr2Var = this.r0;
        return lr2Var.a.b(lr2Var.b.a);
    }

    public final lr2 n2(int i, int i2) {
        boolean z = false;
        ii.a(i >= 0 && i2 >= i && i2 <= this.o.size());
        int I = I();
        u S = S();
        int size = this.o.size();
        this.H++;
        o2(i, i2);
        u w1 = w1();
        lr2 j2 = j2(this.r0, w1, D1(S, w1));
        int i3 = j2.e;
        if (i3 != 1 && i3 != 4 && i < i2 && i2 == size && I >= j2.a.p()) {
            z = true;
        }
        if (z) {
            j2 = j2.g(4);
        }
        this.k.m0(i, i2, this.M);
        return j2;
    }

    @Override // androidx.media3.common.q
    public void o(TextureView textureView) {
        E2();
        if (textureView == null || textureView != this.Y) {
            return;
        }
        t1();
    }

    public final void o2(int i, int i2) {
        for (int i3 = i2 - 1; i3 >= i; i3--) {
            this.o.remove(i3);
        }
        this.M = this.M.a(i, i2);
    }

    @Override // androidx.media3.common.q
    public z p() {
        E2();
        return this.p0;
    }

    public void p1(tb tbVar) {
        ii.e(tbVar);
        this.r.P(tbVar);
    }

    public final void p2() {
        if (this.W != null) {
            y1(this.y).n(10000).m(null).l();
            this.W.i(this.x);
            this.W = null;
        }
        TextureView textureView = this.Y;
        if (textureView != null) {
            if (textureView.getSurfaceTextureListener() != this.x) {
                p12.i("ExoPlayerImpl", "SurfaceTextureListener already unset or replaced.");
            } else {
                this.Y.setSurfaceTextureListener(null);
            }
            this.Y = null;
        }
        SurfaceHolder surfaceHolder = this.V;
        if (surfaceHolder != null) {
            surfaceHolder.removeCallback(this.x);
            this.V = null;
        }
    }

    @Override // androidx.media3.common.q
    public void q(List<androidx.media3.common.m> list, boolean z) {
        E2();
        s2(x1(list), z);
    }

    public void q1(f.a aVar) {
        this.m.add(aVar);
    }

    public final void q2(int i, int i2, Object obj) {
        m[] mVarArr;
        for (m mVar : this.g) {
            if (mVar.h() == i) {
                y1(mVar).n(i2).m(obj).l();
            }
        }
    }

    public final List<j.c> r1(int i, List<androidx.media3.exoplayer.source.j> list) {
        ArrayList arrayList = new ArrayList();
        for (int i2 = 0; i2 < list.size(); i2++) {
            j.c cVar = new j.c(list.get(i2), this.p);
            arrayList.add(cVar);
            this.o.add(i2 + i, new e(cVar.b, cVar.a.M()));
        }
        this.M = this.M.e(i, arrayList.size());
        return arrayList;
    }

    public final void r2() {
        q2(1, 2, Float.valueOf(this.h0 * this.A.g()));
    }

    @Override // androidx.media3.common.q
    public int s() {
        E2();
        if (g()) {
            return this.r0.b.c;
        }
        return -1;
    }

    public final androidx.media3.common.n s1() {
        u S = S();
        if (S.q()) {
            return this.q0;
        }
        return this.q0.b().H(S.n(I(), this.a).g0.h0).F();
    }

    public void s2(List<androidx.media3.exoplayer.source.j> list, boolean z) {
        E2();
        t2(list, -1, CellBase.ID_SYSTEM_MESSAGE_REQUEST_CLOSED, z);
    }

    @Override // androidx.media3.common.q
    public void t(SurfaceView surfaceView) {
        E2();
        if (surfaceView instanceof kh4) {
            p2();
            w2(surfaceView);
            u2(surfaceView.getHolder());
        } else if (surfaceView instanceof SphericalGLSurfaceView) {
            p2();
            this.W = (SphericalGLSurfaceView) surfaceView;
            y1(this.y).n(10000).m(this.W).l();
            this.W.d(this.x);
            w2(this.W.getVideoSurface());
            u2(surfaceView.getHolder());
        } else {
            x2(surfaceView == null ? null : surfaceView.getHolder());
        }
    }

    public void t1() {
        E2();
        p2();
        w2(null);
        l2(0, 0);
    }

    public final void t2(List<androidx.media3.exoplayer.source.j> list, int i, long j, boolean z) {
        int i2;
        long j2;
        int C1 = C1();
        long c0 = c0();
        boolean z2 = true;
        this.H++;
        if (!this.o.isEmpty()) {
            o2(0, this.o.size());
        }
        List<j.c> r1 = r1(0, list);
        u w1 = w1();
        if (!w1.q() && i >= w1.p()) {
            throw new IllegalSeekPositionException(w1, i, j);
        }
        if (z) {
            int a2 = w1.a(this.G);
            j2 = CellBase.ID_SYSTEM_MESSAGE_REQUEST_CLOSED;
            i2 = a2;
        } else if (i == -1) {
            i2 = C1;
            j2 = c0;
        } else {
            i2 = i;
            j2 = j;
        }
        lr2 j22 = j2(this.r0, w1, k2(w1, i2, j2));
        int i3 = j22.e;
        if (i2 != -1 && i3 != 1) {
            i3 = (w1.q() || i2 >= w1.p()) ? 4 : 2;
        }
        lr2 g = j22.g(i3);
        this.k.L0(r1, i2, androidx.media3.common.util.b.y0(j2), this.M);
        if (this.r0.b.a.equals(g.b.a) || this.r0.a.q()) {
            z2 = false;
        }
        B2(g, 0, 1, false, z2, 4, B1(g), -1);
    }

    public void u1(SurfaceHolder surfaceHolder) {
        E2();
        if (surfaceHolder == null || surfaceHolder != this.V) {
            return;
        }
        t1();
    }

    public final void u2(SurfaceHolder surfaceHolder) {
        this.X = false;
        this.V = surfaceHolder;
        surfaceHolder.addCallback(this.x);
        Surface surface = this.V.getSurface();
        if (surface != null && surface.isValid()) {
            Rect surfaceFrame = this.V.getSurfaceFrame();
            l2(surfaceFrame.width(), surfaceFrame.height());
            return;
        }
        l2(0, 0);
    }

    public final void v2(SurfaceTexture surfaceTexture) {
        Surface surface = new Surface(surfaceTexture);
        w2(surface);
        this.U = surface;
    }

    @Override // androidx.media3.common.q
    public void w(boolean z) {
        E2();
        int p = this.A.p(z, B());
        A2(z, p, E1(z, p));
    }

    public final u w1() {
        return new ls2(this.o, this.M);
    }

    public final void w2(Object obj) {
        boolean z;
        ArrayList<k> arrayList = new ArrayList();
        m[] mVarArr = this.g;
        int length = mVarArr.length;
        int i = 0;
        while (true) {
            z = true;
            if (i >= length) {
                break;
            }
            m mVar = mVarArr[i];
            if (mVar.h() == 2) {
                arrayList.add(y1(mVar).n(1).m(obj).l());
            }
            i++;
        }
        Object obj2 = this.T;
        if (obj2 == null || obj2 == obj) {
            z = false;
        } else {
            try {
                for (k kVar : arrayList) {
                    kVar.a(this.E);
                }
            } catch (InterruptedException unused) {
                Thread.currentThread().interrupt();
            } catch (TimeoutException unused2) {
            }
            z = false;
            Object obj3 = this.T;
            Surface surface = this.U;
            if (obj3 == surface) {
                surface.release();
                this.U = null;
            }
        }
        this.T = obj;
        if (z) {
            y2(false, ExoPlaybackException.createForUnexpected(new ExoTimeoutException(3), PlaybackException.ERROR_CODE_TIMEOUT));
        }
    }

    @Override // androidx.media3.common.q
    public long x() {
        E2();
        return this.v;
    }

    public final List<androidx.media3.exoplayer.source.j> x1(List<androidx.media3.common.m> list) {
        ArrayList arrayList = new ArrayList();
        for (int i = 0; i < list.size(); i++) {
            arrayList.add(this.q.a(list.get(i)));
        }
        return arrayList;
    }

    public void x2(SurfaceHolder surfaceHolder) {
        E2();
        if (surfaceHolder == null) {
            t1();
            return;
        }
        p2();
        this.X = true;
        this.V = surfaceHolder;
        surfaceHolder.addCallback(this.x);
        Surface surface = surfaceHolder.getSurface();
        if (surface != null && surface.isValid()) {
            w2(surface);
            Rect surfaceFrame = surfaceHolder.getSurfaceFrame();
            l2(surfaceFrame.width(), surfaceFrame.height());
            return;
        }
        w2(null);
        l2(0, 0);
    }

    @Override // androidx.media3.common.q
    public long y() {
        E2();
        if (g()) {
            lr2 lr2Var = this.r0;
            lr2Var.a.h(lr2Var.b.a, this.n);
            lr2 lr2Var2 = this.r0;
            if (lr2Var2.c == CellBase.ID_SYSTEM_MESSAGE_REQUEST_CLOSED) {
                return lr2Var2.a.n(I(), this.a).e();
            }
            return this.n.o() + androidx.media3.common.util.b.U0(this.r0.c);
        }
        return c0();
    }

    public final k y1(k.b bVar) {
        int C1 = C1();
        h hVar = this.k;
        u uVar = this.r0.a;
        if (C1 == -1) {
            C1 = 0;
        }
        return new k(hVar, bVar, uVar, C1, this.w, hVar.A());
    }

    public final void y2(boolean z, ExoPlaybackException exoPlaybackException) {
        lr2 b2;
        if (z) {
            b2 = n2(0, this.o.size()).e(null);
        } else {
            lr2 lr2Var = this.r0;
            b2 = lr2Var.b(lr2Var.b);
            b2.p = b2.r;
            b2.q = 0L;
        }
        lr2 g = b2.g(1);
        if (exoPlaybackException != null) {
            g = g.e(exoPlaybackException);
        }
        lr2 lr2Var2 = g;
        this.H++;
        this.k.f1();
        B2(lr2Var2, 0, 1, false, lr2Var2.a.q() && !this.r0.a.q(), 4, B1(lr2Var2), -1);
    }

    public final Pair<Boolean, Integer> z1(lr2 lr2Var, lr2 lr2Var2, boolean z, int i, boolean z2) {
        u uVar = lr2Var2.a;
        u uVar2 = lr2Var.a;
        if (uVar2.q() && uVar.q()) {
            return new Pair<>(Boolean.FALSE, -1);
        }
        int i2 = 3;
        if (uVar2.q() != uVar.q()) {
            return new Pair<>(Boolean.TRUE, 3);
        }
        if (!uVar.n(uVar.h(lr2Var2.b.a, this.n).g0, this.a).a.equals(uVar2.n(uVar2.h(lr2Var.b.a, this.n).g0, this.a).a)) {
            if (z && i == 0) {
                i2 = 1;
            } else if (z && i == 1) {
                i2 = 2;
            } else if (!z2) {
                throw new IllegalStateException();
            }
            return new Pair<>(Boolean.TRUE, Integer.valueOf(i2));
        } else if (z && i == 0 && lr2Var2.b.d < lr2Var.b.d) {
            return new Pair<>(Boolean.TRUE, 0);
        } else {
            return new Pair<>(Boolean.FALSE, -1);
        }
    }

    public final void z2() {
        q.b bVar = this.O;
        q.b F = androidx.media3.common.util.b.F(this.f, this.c);
        this.O = F;
        if (F.equals(bVar)) {
            return;
        }
        this.l.i(13, new o02.a() { // from class: tz0
            @Override // defpackage.o02.a
            public final void invoke(Object obj) {
                g.this.U1((q.d) obj);
            }
        });
    }
}
