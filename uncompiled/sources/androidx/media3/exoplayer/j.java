package androidx.media3.exoplayer;

import android.os.Handler;
import androidx.media3.common.u;
import androidx.media3.exoplayer.drm.b;
import androidx.media3.exoplayer.source.j;
import androidx.media3.exoplayer.source.k;
import defpackage.qo3;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.IdentityHashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

/* compiled from: MediaSourceList.java */
/* loaded from: classes.dex */
public final class j {
    public final ks2 a;
    public final d e;
    public final k.a f;
    public final b.a g;
    public final HashMap<c, b> h;
    public final Set<c> i;
    public boolean k;
    public fa4 l;
    public qo3 j = new qo3.a(0);
    public final IdentityHashMap<androidx.media3.exoplayer.source.i, c> c = new IdentityHashMap<>();
    public final Map<Object, c> d = new HashMap();
    public final List<c> b = new ArrayList();

    /* compiled from: MediaSourceList.java */
    /* loaded from: classes.dex */
    public final class a implements androidx.media3.exoplayer.source.k, androidx.media3.exoplayer.drm.b {
        public final c a;
        public k.a f0;
        public b.a g0;

        public a(c cVar) {
            this.f0 = j.this.f;
            this.g0 = j.this.g;
            this.a = cVar;
        }

        @Override // androidx.media3.exoplayer.drm.b
        public void K(int i, j.b bVar) {
            if (b(i, bVar)) {
                this.g0.j();
            }
        }

        @Override // androidx.media3.exoplayer.source.k
        public void O(int i, j.b bVar, u02 u02Var, g62 g62Var) {
            if (b(i, bVar)) {
                this.f0.s(u02Var, g62Var);
            }
        }

        @Override // androidx.media3.exoplayer.source.k
        public void S(int i, j.b bVar, g62 g62Var) {
            if (b(i, bVar)) {
                this.f0.j(g62Var);
            }
        }

        @Override // androidx.media3.exoplayer.drm.b
        public void U(int i, j.b bVar) {
            if (b(i, bVar)) {
                this.g0.h();
            }
        }

        @Override // androidx.media3.exoplayer.source.k
        public void W(int i, j.b bVar, g62 g62Var) {
            if (b(i, bVar)) {
                this.f0.E(g62Var);
            }
        }

        public final boolean b(int i, j.b bVar) {
            j.b bVar2;
            if (bVar != null) {
                bVar2 = j.n(this.a, bVar);
                if (bVar2 == null) {
                    return false;
                }
            } else {
                bVar2 = null;
            }
            int r = j.r(this.a, i);
            k.a aVar = this.f0;
            if (aVar.a != r || !androidx.media3.common.util.b.c(aVar.b, bVar2)) {
                this.f0 = j.this.f.F(r, bVar2, 0L);
            }
            b.a aVar2 = this.g0;
            if (aVar2.a == r && androidx.media3.common.util.b.c(aVar2.b, bVar2)) {
                return true;
            }
            this.g0 = j.this.g.u(r, bVar2);
            return true;
        }

        @Override // androidx.media3.exoplayer.source.k
        public void b0(int i, j.b bVar, u02 u02Var, g62 g62Var, IOException iOException, boolean z) {
            if (b(i, bVar)) {
                this.f0.y(u02Var, g62Var, iOException, z);
            }
        }

        @Override // androidx.media3.exoplayer.drm.b
        public void d0(int i, j.b bVar) {
            if (b(i, bVar)) {
                this.g0.m();
            }
        }

        @Override // androidx.media3.exoplayer.source.k
        public void f0(int i, j.b bVar, u02 u02Var, g62 g62Var) {
            if (b(i, bVar)) {
                this.f0.B(u02Var, g62Var);
            }
        }

        @Override // androidx.media3.exoplayer.drm.b
        public void g0(int i, j.b bVar, int i2) {
            if (b(i, bVar)) {
                this.g0.k(i2);
            }
        }

        @Override // androidx.media3.exoplayer.source.k
        public void i0(int i, j.b bVar, u02 u02Var, g62 g62Var) {
            if (b(i, bVar)) {
                this.f0.v(u02Var, g62Var);
            }
        }

        @Override // androidx.media3.exoplayer.drm.b
        public /* synthetic */ void j0(int i, j.b bVar) {
            pr0.a(this, i, bVar);
        }

        @Override // androidx.media3.exoplayer.drm.b
        public void n0(int i, j.b bVar) {
            if (b(i, bVar)) {
                this.g0.i();
            }
        }

        @Override // androidx.media3.exoplayer.drm.b
        public void o0(int i, j.b bVar, Exception exc) {
            if (b(i, bVar)) {
                this.g0.l(exc);
            }
        }
    }

    /* compiled from: MediaSourceList.java */
    /* loaded from: classes.dex */
    public static final class b {
        public final androidx.media3.exoplayer.source.j a;
        public final j.c b;
        public final a c;

        public b(androidx.media3.exoplayer.source.j jVar, j.c cVar, a aVar) {
            this.a = jVar;
            this.b = cVar;
            this.c = aVar;
        }
    }

    /* compiled from: MediaSourceList.java */
    /* loaded from: classes.dex */
    public static final class c implements z62 {
        public final androidx.media3.exoplayer.source.h a;
        public int d;
        public boolean e;
        public final List<j.b> c = new ArrayList();
        public final Object b = new Object();

        public c(androidx.media3.exoplayer.source.j jVar, boolean z) {
            this.a = new androidx.media3.exoplayer.source.h(jVar, z);
        }

        @Override // defpackage.z62
        public Object a() {
            return this.b;
        }

        @Override // defpackage.z62
        public u b() {
            return this.a.M();
        }

        public void c(int i) {
            this.d = i;
            this.e = false;
            this.c.clear();
        }
    }

    /* compiled from: MediaSourceList.java */
    /* loaded from: classes.dex */
    public interface d {
        void b();
    }

    public j(d dVar, jb jbVar, Handler handler, ks2 ks2Var) {
        this.a = ks2Var;
        this.e = dVar;
        k.a aVar = new k.a();
        this.f = aVar;
        b.a aVar2 = new b.a();
        this.g = aVar2;
        this.h = new HashMap<>();
        this.i = new HashSet();
        aVar.g(handler, jbVar);
        aVar2.g(handler, jbVar);
    }

    public static Object m(Object obj) {
        return s4.x(obj);
    }

    public static j.b n(c cVar, j.b bVar) {
        for (int i = 0; i < cVar.c.size(); i++) {
            if (cVar.c.get(i).d == bVar.d) {
                return bVar.c(p(cVar, bVar.a));
            }
        }
        return null;
    }

    public static Object o(Object obj) {
        return s4.y(obj);
    }

    public static Object p(c cVar, Object obj) {
        return s4.A(cVar.b, obj);
    }

    public static int r(c cVar, int i) {
        return i + cVar.d;
    }

    /* JADX INFO: Access modifiers changed from: private */
    public /* synthetic */ void t(androidx.media3.exoplayer.source.j jVar, u uVar) {
        this.e.b();
    }

    public u A(int i, int i2, qo3 qo3Var) {
        ii.a(i >= 0 && i <= i2 && i2 <= q());
        this.j = qo3Var;
        B(i, i2);
        return i();
    }

    public final void B(int i, int i2) {
        for (int i3 = i2 - 1; i3 >= i; i3--) {
            c remove = this.b.remove(i3);
            this.d.remove(remove.b);
            g(i3, -remove.a.M().p());
            remove.e = true;
            if (this.k) {
                u(remove);
            }
        }
    }

    public u C(List<c> list, qo3 qo3Var) {
        B(0, this.b.size());
        return f(this.b.size(), list, qo3Var);
    }

    public u D(qo3 qo3Var) {
        int q = q();
        if (qo3Var.getLength() != q) {
            qo3Var = qo3Var.g().e(0, q);
        }
        this.j = qo3Var;
        return i();
    }

    public u f(int i, List<c> list, qo3 qo3Var) {
        if (!list.isEmpty()) {
            this.j = qo3Var;
            for (int i2 = i; i2 < list.size() + i; i2++) {
                c cVar = list.get(i2 - i);
                if (i2 > 0) {
                    c cVar2 = this.b.get(i2 - 1);
                    cVar.c(cVar2.d + cVar2.a.M().p());
                } else {
                    cVar.c(0);
                }
                g(i2, cVar.a.M().p());
                this.b.add(i2, cVar);
                this.d.put(cVar.b, cVar);
                if (this.k) {
                    x(cVar);
                    if (this.c.isEmpty()) {
                        this.i.add(cVar);
                    } else {
                        j(cVar);
                    }
                }
            }
        }
        return i();
    }

    public final void g(int i, int i2) {
        while (i < this.b.size()) {
            this.b.get(i).d += i2;
            i++;
        }
    }

    public androidx.media3.exoplayer.source.i h(j.b bVar, gb gbVar, long j) {
        Object o = o(bVar.a);
        j.b c2 = bVar.c(m(bVar.a));
        c cVar = (c) ii.e(this.d.get(o));
        l(cVar);
        cVar.c.add(c2);
        androidx.media3.exoplayer.source.g e = cVar.a.e(c2, gbVar, j);
        this.c.put(e, cVar);
        k();
        return e;
    }

    public u i() {
        if (this.b.isEmpty()) {
            return u.a;
        }
        int i = 0;
        for (int i2 = 0; i2 < this.b.size(); i2++) {
            c cVar = this.b.get(i2);
            cVar.d = i;
            i += cVar.a.M().p();
        }
        return new ls2(this.b, this.j);
    }

    public final void j(c cVar) {
        b bVar = this.h.get(cVar);
        if (bVar != null) {
            bVar.a.g(bVar.b);
        }
    }

    public final void k() {
        Iterator<c> it = this.i.iterator();
        while (it.hasNext()) {
            c next = it.next();
            if (next.c.isEmpty()) {
                j(next);
                it.remove();
            }
        }
    }

    public final void l(c cVar) {
        this.i.add(cVar);
        b bVar = this.h.get(cVar);
        if (bVar != null) {
            bVar.a.b(bVar.b);
        }
    }

    public int q() {
        return this.b.size();
    }

    public boolean s() {
        return this.k;
    }

    public final void u(c cVar) {
        if (cVar.e && cVar.c.isEmpty()) {
            b bVar = (b) ii.e(this.h.remove(cVar));
            bVar.a.f(bVar.b);
            bVar.a.d(bVar.c);
            bVar.a.n(bVar.c);
            this.i.remove(cVar);
        }
    }

    public u v(int i, int i2, int i3, qo3 qo3Var) {
        ii.a(i >= 0 && i <= i2 && i2 <= q() && i3 >= 0);
        this.j = qo3Var;
        if (i != i2 && i != i3) {
            int min = Math.min(i, i3);
            int max = Math.max(((i2 - i) + i3) - 1, i2 - 1);
            int i4 = this.b.get(min).d;
            androidx.media3.common.util.b.x0(this.b, i, i2, i3);
            while (min <= max) {
                c cVar = this.b.get(min);
                cVar.d = i4;
                i4 += cVar.a.M().p();
                min++;
            }
            return i();
        }
        return i();
    }

    public void w(fa4 fa4Var) {
        ii.g(!this.k);
        this.l = fa4Var;
        for (int i = 0; i < this.b.size(); i++) {
            c cVar = this.b.get(i);
            x(cVar);
            this.i.add(cVar);
        }
        this.k = true;
    }

    public final void x(c cVar) {
        androidx.media3.exoplayer.source.h hVar = cVar.a;
        j.c cVar2 = new j.c() { // from class: a72
            @Override // androidx.media3.exoplayer.source.j.c
            public final void a(j jVar, u uVar) {
                androidx.media3.exoplayer.j.this.t(jVar, uVar);
            }
        };
        a aVar = new a(cVar);
        this.h.put(cVar, new b(hVar, cVar2, aVar));
        hVar.a(androidx.media3.common.util.b.x(), aVar);
        hVar.m(androidx.media3.common.util.b.x(), aVar);
        hVar.c(cVar2, this.l, this.a);
    }

    public void y() {
        for (b bVar : this.h.values()) {
            try {
                bVar.a.f(bVar.b);
            } catch (RuntimeException e) {
                p12.d("MediaSourceList", "Failed to release child source.", e);
            }
            bVar.a.d(bVar.c);
            bVar.a.n(bVar.c);
        }
        this.h.clear();
        this.i.clear();
        this.k = false;
    }

    public void z(androidx.media3.exoplayer.source.i iVar) {
        c cVar = (c) ii.e(this.c.remove(iVar));
        cVar.a.o(iVar);
        cVar.c.remove(((androidx.media3.exoplayer.source.g) iVar).a);
        if (!this.c.isEmpty()) {
            k();
        }
        u(cVar);
    }
}
