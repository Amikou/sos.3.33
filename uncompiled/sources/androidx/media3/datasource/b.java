package androidx.media3.datasource;

import android.net.Uri;
import androidx.media3.common.g;
import java.io.IOException;
import java.util.List;
import java.util.Map;

/* compiled from: DataSource.java */
/* loaded from: classes.dex */
public interface b extends g {

    /* compiled from: DataSource.java */
    /* loaded from: classes.dex */
    public interface a {
        b a();
    }

    long b(je0 je0Var) throws IOException;

    void c(fa4 fa4Var);

    void close() throws IOException;

    Map<String, List<String>> i();

    Uri m();
}
