package androidx.media3.datasource;

import android.net.Uri;
import android.util.Base64;
import androidx.media3.common.ParserException;
import java.io.IOException;
import java.net.URLDecoder;

/* compiled from: DataSchemeDataSource.java */
/* loaded from: classes.dex */
public final class a extends fn {
    public je0 e;
    public byte[] f;
    public int g;
    public int h;

    public a() {
        super(false);
    }

    @Override // androidx.media3.datasource.b
    public long b(je0 je0Var) throws IOException {
        q(je0Var);
        this.e = je0Var;
        Uri uri = je0Var.a;
        String scheme = uri.getScheme();
        boolean equals = "data".equals(scheme);
        ii.b(equals, "Unsupported scheme: " + scheme);
        String[] L0 = androidx.media3.common.util.b.L0(uri.getSchemeSpecificPart(), ",");
        if (L0.length == 2) {
            String str = L0[1];
            if (L0[0].contains(";base64")) {
                try {
                    this.f = Base64.decode(str, 0);
                } catch (IllegalArgumentException e) {
                    throw ParserException.createForMalformedDataOfUnknownType("Error while parsing Base64 encoded string: " + str, e);
                }
            } else {
                this.f = androidx.media3.common.util.b.j0(URLDecoder.decode(str, cy.a.name()));
            }
            long j = je0Var.f;
            byte[] bArr = this.f;
            if (j <= bArr.length) {
                int i = (int) j;
                this.g = i;
                int length = bArr.length - i;
                this.h = length;
                long j2 = je0Var.g;
                if (j2 != -1) {
                    this.h = (int) Math.min(length, j2);
                }
                r(je0Var);
                long j3 = je0Var.g;
                return j3 != -1 ? j3 : this.h;
            }
            this.f = null;
            throw new DataSourceException(2008);
        }
        throw ParserException.createForMalformedDataOfUnknownType("Unexpected URI format: " + uri, null);
    }

    @Override // androidx.media3.datasource.b
    public void close() {
        if (this.f != null) {
            this.f = null;
            p();
        }
        this.e = null;
    }

    @Override // androidx.media3.datasource.b
    public Uri m() {
        je0 je0Var = this.e;
        if (je0Var != null) {
            return je0Var.a;
        }
        return null;
    }

    @Override // androidx.media3.common.g
    public int read(byte[] bArr, int i, int i2) {
        if (i2 == 0) {
            return 0;
        }
        int i3 = this.h;
        if (i3 == 0) {
            return -1;
        }
        int min = Math.min(i2, i3);
        System.arraycopy(androidx.media3.common.util.b.j(this.f), this.g, bArr, i, min);
        this.g += min;
        this.h -= min;
        o(min);
        return min;
    }
}
