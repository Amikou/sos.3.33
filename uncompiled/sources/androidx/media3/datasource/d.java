package androidx.media3.datasource;

import android.content.Context;
import android.net.Uri;
import androidx.media3.datasource.b;
import androidx.media3.datasource.e;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import org.web3j.ens.contracts.generated.PublicResolver;

/* compiled from: DefaultDataSource.java */
/* loaded from: classes.dex */
public final class d implements b {
    public final Context a;
    public final List<fa4> b = new ArrayList();
    public final b c;
    public b d;
    public b e;
    public b f;
    public b g;
    public b h;
    public b i;
    public b j;
    public b k;

    /* compiled from: DefaultDataSource.java */
    /* loaded from: classes.dex */
    public static final class a implements b.a {
        public final Context a;
        public final b.a b;
        public fa4 c;

        public a(Context context) {
            this(context, new e.b());
        }

        @Override // androidx.media3.datasource.b.a
        /* renamed from: b */
        public d a() {
            d dVar = new d(this.a, this.b.a());
            fa4 fa4Var = this.c;
            if (fa4Var != null) {
                dVar.c(fa4Var);
            }
            return dVar;
        }

        public a(Context context, b.a aVar) {
            this.a = context.getApplicationContext();
            this.b = aVar;
        }
    }

    public d(Context context, b bVar) {
        this.a = context.getApplicationContext();
        this.c = (b) ii.e(bVar);
    }

    @Override // androidx.media3.datasource.b
    public long b(je0 je0Var) throws IOException {
        ii.g(this.k == null);
        String scheme = je0Var.a.getScheme();
        if (androidx.media3.common.util.b.s0(je0Var.a)) {
            String path = je0Var.a.getPath();
            if (path != null && path.startsWith("/android_asset/")) {
                this.k = p();
            } else {
                this.k = s();
            }
        } else if ("asset".equals(scheme)) {
            this.k = p();
        } else if (PublicResolver.FUNC_CONTENT.equals(scheme)) {
            this.k = q();
        } else if ("rtmp".equals(scheme)) {
            this.k = u();
        } else if ("udp".equals(scheme)) {
            this.k = v();
        } else if ("data".equals(scheme)) {
            this.k = r();
        } else if (!"rawresource".equals(scheme) && !"android.resource".equals(scheme)) {
            this.k = this.c;
        } else {
            this.k = t();
        }
        return this.k.b(je0Var);
    }

    @Override // androidx.media3.datasource.b
    public void c(fa4 fa4Var) {
        ii.e(fa4Var);
        this.c.c(fa4Var);
        this.b.add(fa4Var);
        w(this.d, fa4Var);
        w(this.e, fa4Var);
        w(this.f, fa4Var);
        w(this.g, fa4Var);
        w(this.h, fa4Var);
        w(this.i, fa4Var);
        w(this.j, fa4Var);
    }

    @Override // androidx.media3.datasource.b
    public void close() throws IOException {
        b bVar = this.k;
        if (bVar != null) {
            try {
                bVar.close();
            } finally {
                this.k = null;
            }
        }
    }

    @Override // androidx.media3.datasource.b
    public Map<String, List<String>> i() {
        b bVar = this.k;
        return bVar == null ? Collections.emptyMap() : bVar.i();
    }

    @Override // androidx.media3.datasource.b
    public Uri m() {
        b bVar = this.k;
        if (bVar == null) {
            return null;
        }
        return bVar.m();
    }

    public final void o(b bVar) {
        for (int i = 0; i < this.b.size(); i++) {
            bVar.c(this.b.get(i));
        }
    }

    public final b p() {
        if (this.e == null) {
            AssetDataSource assetDataSource = new AssetDataSource(this.a);
            this.e = assetDataSource;
            o(assetDataSource);
        }
        return this.e;
    }

    public final b q() {
        if (this.f == null) {
            ContentDataSource contentDataSource = new ContentDataSource(this.a);
            this.f = contentDataSource;
            o(contentDataSource);
        }
        return this.f;
    }

    public final b r() {
        if (this.i == null) {
            androidx.media3.datasource.a aVar = new androidx.media3.datasource.a();
            this.i = aVar;
            o(aVar);
        }
        return this.i;
    }

    @Override // androidx.media3.common.g
    public int read(byte[] bArr, int i, int i2) throws IOException {
        return ((b) ii.e(this.k)).read(bArr, i, i2);
    }

    public final b s() {
        if (this.d == null) {
            FileDataSource fileDataSource = new FileDataSource();
            this.d = fileDataSource;
            o(fileDataSource);
        }
        return this.d;
    }

    public final b t() {
        if (this.j == null) {
            RawResourceDataSource rawResourceDataSource = new RawResourceDataSource(this.a);
            this.j = rawResourceDataSource;
            o(rawResourceDataSource);
        }
        return this.j;
    }

    public final b u() {
        if (this.g == null) {
            try {
                b bVar = (b) Class.forName("androidx.media3.datasource.rtmp.RtmpDataSource").getConstructor(new Class[0]).newInstance(new Object[0]);
                this.g = bVar;
                o(bVar);
            } catch (ClassNotFoundException unused) {
                p12.i("DefaultDataSource", "Attempting to play RTMP stream without depending on the RTMP extension");
            } catch (Exception e) {
                throw new RuntimeException("Error instantiating RTMP extension", e);
            }
            if (this.g == null) {
                this.g = this.c;
            }
        }
        return this.g;
    }

    public final b v() {
        if (this.h == null) {
            UdpDataSource udpDataSource = new UdpDataSource();
            this.h = udpDataSource;
            o(udpDataSource);
        }
        return this.h;
    }

    public final void w(b bVar, fa4 fa4Var) {
        if (bVar != null) {
            bVar.c(fa4Var);
        }
    }
}
