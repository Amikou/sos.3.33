package androidx.media3.datasource;

import android.net.Uri;
import androidx.media3.common.PlaybackException;
import androidx.media3.datasource.b;
import com.google.common.collect.ImmutableMap;
import com.google.common.collect.h;
import java.io.IOException;
import java.io.InputStream;
import java.io.InterruptedIOException;
import java.io.OutputStream;
import java.lang.reflect.Method;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.NoRouteToHostException;
import java.net.URL;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.zip.GZIPInputStream;
import zendesk.core.Constants;

/* compiled from: DefaultHttpDataSource.java */
/* loaded from: classes.dex */
public class e extends fn {
    public final boolean e;
    public final int f;
    public final int g;
    public final String h;
    public final gl1 i;
    public final gl1 j;
    public final boolean k;
    public gu2<String> l;
    public je0 m;
    public HttpURLConnection n;
    public InputStream o;
    public boolean p;
    public int q;
    public long r;
    public long s;

    /* compiled from: DefaultHttpDataSource.java */
    /* loaded from: classes.dex */
    public static final class b implements b.a {
        public fa4 b;
        public gu2<String> c;
        public String d;
        public boolean g;
        public boolean h;
        public final gl1 a = new gl1();
        public int e = 8000;
        public int f = 8000;

        @Override // androidx.media3.datasource.b.a
        /* renamed from: b */
        public e a() {
            e eVar = new e(this.d, this.e, this.f, this.g, this.a, this.c, this.h);
            fa4 fa4Var = this.b;
            if (fa4Var != null) {
                eVar.c(fa4Var);
            }
            return eVar;
        }

        public b c(String str) {
            this.d = str;
            return this;
        }
    }

    /* compiled from: DefaultHttpDataSource.java */
    /* loaded from: classes.dex */
    public static class c extends h91<String, List<String>> {
        public final Map<String, List<String>> a;

        public c(Map<String, List<String>> map) {
            this.a = map;
        }

        public static /* synthetic */ boolean d(Map.Entry entry) {
            return entry.getKey() != null;
        }

        public static /* synthetic */ boolean e(String str) {
            return str != null;
        }

        @Override // defpackage.h91, java.util.Map
        /* renamed from: c */
        public List<String> get(Object obj) {
            if (obj == null) {
                return null;
            }
            return (List) super.get(obj);
        }

        @Override // defpackage.h91, java.util.Map
        public boolean containsKey(Object obj) {
            return obj != null && super.containsKey(obj);
        }

        @Override // defpackage.h91, java.util.Map
        public boolean containsValue(Object obj) {
            return super.standardContainsValue(obj);
        }

        @Override // defpackage.h91, java.util.Map
        public Set<Map.Entry<String, List<String>>> entrySet() {
            return h.b(super.entrySet(), tj0.a);
        }

        @Override // defpackage.h91, java.util.Map
        public boolean equals(Object obj) {
            return obj != null && super.standardEquals(obj);
        }

        @Override // defpackage.h91, java.util.Map
        public int hashCode() {
            return super.standardHashCode();
        }

        @Override // defpackage.h91, java.util.Map
        public boolean isEmpty() {
            if (super.isEmpty()) {
                return true;
            }
            return super.size() == 1 && super.containsKey(null);
        }

        @Override // defpackage.h91, java.util.Map
        public Set<String> keySet() {
            return h.b(super.keySet(), sj0.a);
        }

        @Override // defpackage.h91, java.util.Map
        public int size() {
            return super.size() - (super.containsKey(null) ? 1 : 0);
        }

        @Override // defpackage.h91, defpackage.i91
        public Map<String, List<String>> delegate() {
            return this.a;
        }
    }

    public static boolean u(HttpURLConnection httpURLConnection) {
        return "gzip".equalsIgnoreCase(httpURLConnection.getHeaderField("Content-Encoding"));
    }

    public static void x(HttpURLConnection httpURLConnection, long j) {
        int i;
        if (httpURLConnection != null && (i = androidx.media3.common.util.b.a) >= 19 && i <= 20) {
            try {
                InputStream inputStream = httpURLConnection.getInputStream();
                if (j == -1) {
                    if (inputStream.read() == -1) {
                        return;
                    }
                } else if (j <= 2048) {
                    return;
                }
                String name = inputStream.getClass().getName();
                if (!"com.android.okhttp.internal.http.HttpTransport$ChunkedInputStream".equals(name) && !"com.android.okhttp.internal.http.HttpTransport$FixedLengthInputStream".equals(name)) {
                    return;
                }
                Method declaredMethod = ((Class) ii.e(inputStream.getClass().getSuperclass())).getDeclaredMethod("unexpectedEndOfInput", new Class[0]);
                declaredMethod.setAccessible(true);
                declaredMethod.invoke(inputStream, new Object[0]);
            } catch (Exception unused) {
            }
        }
    }

    public final void A(long j, je0 je0Var) throws IOException {
        if (j == 0) {
            return;
        }
        byte[] bArr = new byte[4096];
        while (j > 0) {
            int read = ((InputStream) androidx.media3.common.util.b.j(this.o)).read(bArr, 0, (int) Math.min(j, 4096));
            if (Thread.currentThread().isInterrupted()) {
                throw new HttpDataSource$HttpDataSourceException(new InterruptedIOException(), je0Var, (int) PlaybackException.ERROR_CODE_IO_UNSPECIFIED, 1);
            }
            if (read != -1) {
                j -= read;
                o(read);
            } else {
                throw new HttpDataSource$HttpDataSourceException(je0Var, 2008, 1);
            }
        }
    }

    @Override // androidx.media3.datasource.b
    public long b(final je0 je0Var) throws HttpDataSource$HttpDataSourceException {
        byte[] bArr;
        this.m = je0Var;
        long j = 0;
        this.s = 0L;
        this.r = 0L;
        q(je0Var);
        try {
            HttpURLConnection v = v(je0Var);
            this.n = v;
            this.q = v.getResponseCode();
            String responseMessage = v.getResponseMessage();
            int i = this.q;
            if (i >= 200 && i <= 299) {
                final String contentType = v.getContentType();
                gu2<String> gu2Var = this.l;
                if (gu2Var != null && !gu2Var.apply(contentType)) {
                    s();
                    throw new HttpDataSource$HttpDataSourceException(contentType, je0Var) { // from class: androidx.media3.datasource.HttpDataSource$InvalidContentTypeException
                        public final String contentType;

                        {
                            super("Invalid content type: " + contentType, je0Var, (int) PlaybackException.ERROR_CODE_IO_INVALID_HTTP_CONTENT_TYPE, 1);
                            this.contentType = contentType;
                        }
                    };
                }
                if (this.q == 200) {
                    long j2 = je0Var.f;
                    if (j2 != 0) {
                        j = j2;
                    }
                }
                boolean u = u(v);
                if (!u) {
                    long j3 = je0Var.g;
                    if (j3 != -1) {
                        this.r = j3;
                    } else {
                        long b2 = nl1.b(v.getHeaderField("Content-Length"), v.getHeaderField("Content-Range"));
                        this.r = b2 != -1 ? b2 - j : -1L;
                    }
                } else {
                    this.r = je0Var.g;
                }
                try {
                    this.o = v.getInputStream();
                    if (u) {
                        this.o = new GZIPInputStream(this.o);
                    }
                    this.p = true;
                    r(je0Var);
                    try {
                        A(j, je0Var);
                        return this.r;
                    } catch (IOException e) {
                        s();
                        if (e instanceof HttpDataSource$HttpDataSourceException) {
                            throw ((HttpDataSource$HttpDataSourceException) e);
                        }
                        throw new HttpDataSource$HttpDataSourceException(e, je0Var, (int) PlaybackException.ERROR_CODE_IO_UNSPECIFIED, 1);
                    }
                } catch (IOException e2) {
                    s();
                    throw new HttpDataSource$HttpDataSourceException(e2, je0Var, (int) PlaybackException.ERROR_CODE_IO_UNSPECIFIED, 1);
                }
            }
            Map<String, List<String>> headerFields = v.getHeaderFields();
            if (this.q == 416) {
                if (je0Var.f == nl1.c(v.getHeaderField("Content-Range"))) {
                    this.p = true;
                    r(je0Var);
                    long j4 = je0Var.g;
                    if (j4 != -1) {
                        return j4;
                    }
                    return 0L;
                }
            }
            InputStream errorStream = v.getErrorStream();
            try {
                bArr = errorStream != null ? androidx.media3.common.util.b.Q0(errorStream) : androidx.media3.common.util.b.f;
            } catch (IOException unused) {
                bArr = androidx.media3.common.util.b.f;
            }
            byte[] bArr2 = bArr;
            s();
            throw new HttpDataSource$InvalidResponseCodeException(this.q, responseMessage, this.q == 416 ? new DataSourceException(2008) : null, headerFields, je0Var, bArr2);
        } catch (IOException e3) {
            s();
            throw HttpDataSource$HttpDataSourceException.createForIOException(e3, je0Var, 1);
        }
    }

    @Override // androidx.media3.datasource.b
    public void close() throws HttpDataSource$HttpDataSourceException {
        try {
            InputStream inputStream = this.o;
            if (inputStream != null) {
                long j = this.r;
                long j2 = -1;
                if (j != -1) {
                    j2 = j - this.s;
                }
                x(this.n, j2);
                try {
                    inputStream.close();
                } catch (IOException e) {
                    throw new HttpDataSource$HttpDataSourceException(e, (je0) androidx.media3.common.util.b.j(this.m), (int) PlaybackException.ERROR_CODE_IO_UNSPECIFIED, 3);
                }
            }
        } finally {
            this.o = null;
            s();
            if (this.p) {
                this.p = false;
                p();
            }
        }
    }

    @Override // defpackage.fn, androidx.media3.datasource.b
    public Map<String, List<String>> i() {
        HttpURLConnection httpURLConnection = this.n;
        if (httpURLConnection == null) {
            return ImmutableMap.of();
        }
        return new c(httpURLConnection.getHeaderFields());
    }

    @Override // androidx.media3.datasource.b
    public Uri m() {
        HttpURLConnection httpURLConnection = this.n;
        if (httpURLConnection == null) {
            return null;
        }
        return Uri.parse(httpURLConnection.getURL().toString());
    }

    @Override // androidx.media3.common.g
    public int read(byte[] bArr, int i, int i2) throws HttpDataSource$HttpDataSourceException {
        try {
            return z(bArr, i, i2);
        } catch (IOException e) {
            throw HttpDataSource$HttpDataSourceException.createForIOException(e, (je0) androidx.media3.common.util.b.j(this.m), 2);
        }
    }

    public final void s() {
        HttpURLConnection httpURLConnection = this.n;
        if (httpURLConnection != null) {
            try {
                httpURLConnection.disconnect();
            } catch (Exception e) {
                p12.d("DefaultHttpDataSource", "Unexpected error while disconnecting", e);
            }
            this.n = null;
        }
    }

    public final URL t(URL url, String str, je0 je0Var) throws HttpDataSource$HttpDataSourceException {
        if (str != null) {
            try {
                URL url2 = new URL(url, str);
                String protocol = url2.getProtocol();
                if (!"https".equals(protocol) && !"http".equals(protocol)) {
                    throw new HttpDataSource$HttpDataSourceException("Unsupported protocol redirect: " + protocol, je0Var, (int) PlaybackException.ERROR_CODE_IO_NETWORK_CONNECTION_FAILED, 1);
                } else if (this.e || protocol.equals(url.getProtocol())) {
                    return url2;
                } else {
                    throw new HttpDataSource$HttpDataSourceException("Disallowed cross-protocol redirect (" + url.getProtocol() + " to " + protocol + ")", je0Var, (int) PlaybackException.ERROR_CODE_IO_NETWORK_CONNECTION_FAILED, 1);
                }
            } catch (MalformedURLException e) {
                throw new HttpDataSource$HttpDataSourceException(e, je0Var, (int) PlaybackException.ERROR_CODE_IO_NETWORK_CONNECTION_FAILED, 1);
            }
        }
        throw new HttpDataSource$HttpDataSourceException("Null location redirect", je0Var, (int) PlaybackException.ERROR_CODE_IO_NETWORK_CONNECTION_FAILED, 1);
    }

    public final HttpURLConnection v(je0 je0Var) throws IOException {
        HttpURLConnection w;
        URL url = new URL(je0Var.a.toString());
        int i = je0Var.c;
        byte[] bArr = je0Var.d;
        long j = je0Var.f;
        long j2 = je0Var.g;
        boolean d = je0Var.d(1);
        if (this.e || this.k) {
            URL url2 = url;
            int i2 = i;
            byte[] bArr2 = bArr;
            int i3 = 0;
            while (true) {
                int i4 = i3 + 1;
                if (i3 <= 20) {
                    int i5 = i2;
                    long j3 = j;
                    URL url3 = url2;
                    long j4 = j2;
                    w = w(url2, i2, bArr2, j, j2, d, false, je0Var.e);
                    int responseCode = w.getResponseCode();
                    String headerField = w.getHeaderField("Location");
                    if ((i5 == 1 || i5 == 3) && (responseCode == 300 || responseCode == 301 || responseCode == 302 || responseCode == 303 || responseCode == 307 || responseCode == 308)) {
                        w.disconnect();
                        url2 = t(url3, headerField, je0Var);
                        i2 = i5;
                    } else if (i5 != 2 || (responseCode != 300 && responseCode != 301 && responseCode != 302 && responseCode != 303)) {
                        break;
                    } else {
                        w.disconnect();
                        if (this.k && responseCode == 302) {
                            i2 = i5;
                        } else {
                            bArr2 = null;
                            i2 = 1;
                        }
                        url2 = t(url3, headerField, je0Var);
                    }
                    i3 = i4;
                    j = j3;
                    j2 = j4;
                } else {
                    throw new HttpDataSource$HttpDataSourceException(new NoRouteToHostException("Too many redirects: " + i4), je0Var, (int) PlaybackException.ERROR_CODE_IO_NETWORK_CONNECTION_FAILED, 1);
                }
            }
            return w;
        }
        return w(url, i, bArr, j, j2, d, true, je0Var.e);
    }

    public final HttpURLConnection w(URL url, int i, byte[] bArr, long j, long j2, boolean z, boolean z2, Map<String, String> map) throws IOException {
        HttpURLConnection y = y(url);
        y.setConnectTimeout(this.f);
        y.setReadTimeout(this.g);
        HashMap hashMap = new HashMap();
        gl1 gl1Var = this.i;
        if (gl1Var != null) {
            hashMap.putAll(gl1Var.a());
        }
        hashMap.putAll(this.j.a());
        hashMap.putAll(map);
        for (Map.Entry entry : hashMap.entrySet()) {
            y.setRequestProperty((String) entry.getKey(), (String) entry.getValue());
        }
        String a2 = nl1.a(j, j2);
        if (a2 != null) {
            y.setRequestProperty("Range", a2);
        }
        String str = this.h;
        if (str != null) {
            y.setRequestProperty(Constants.USER_AGENT_HEADER_KEY, str);
        }
        y.setRequestProperty("Accept-Encoding", z ? "gzip" : "identity");
        y.setInstanceFollowRedirects(z2);
        y.setDoOutput(bArr != null);
        y.setRequestMethod(je0.c(i));
        if (bArr != null) {
            y.setFixedLengthStreamingMode(bArr.length);
            y.connect();
            OutputStream outputStream = y.getOutputStream();
            outputStream.write(bArr);
            outputStream.close();
        } else {
            y.connect();
        }
        return y;
    }

    public HttpURLConnection y(URL url) throws IOException {
        return (HttpURLConnection) url.openConnection();
    }

    public final int z(byte[] bArr, int i, int i2) throws IOException {
        if (i2 == 0) {
            return 0;
        }
        long j = this.r;
        if (j != -1) {
            long j2 = j - this.s;
            if (j2 == 0) {
                return -1;
            }
            i2 = (int) Math.min(i2, j2);
        }
        int read = ((InputStream) androidx.media3.common.util.b.j(this.o)).read(bArr, i, i2);
        if (read == -1) {
            return -1;
        }
        this.s += read;
        o(read);
        return read;
    }

    public e(String str, int i, int i2, boolean z, gl1 gl1Var, gu2<String> gu2Var, boolean z2) {
        super(true);
        this.h = str;
        this.f = i;
        this.g = i2;
        this.e = z;
        this.i = gl1Var;
        this.l = gu2Var;
        this.j = new gl1();
        this.k = z2;
    }
}
