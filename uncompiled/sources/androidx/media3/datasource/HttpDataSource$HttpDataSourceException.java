package androidx.media3.datasource;

import androidx.media3.common.PlaybackException;
import java.io.IOException;
import java.io.InterruptedIOException;
import java.net.SocketTimeoutException;

/* loaded from: classes.dex */
public class HttpDataSource$HttpDataSourceException extends DataSourceException {
    public static final int TYPE_CLOSE = 3;
    public static final int TYPE_OPEN = 1;
    public static final int TYPE_READ = 2;
    public final je0 dataSpec;
    public final int type;

    @Deprecated
    public HttpDataSource$HttpDataSourceException(je0 je0Var, int i) {
        this(je0Var, (int) PlaybackException.ERROR_CODE_IO_UNSPECIFIED, i);
    }

    public static int a(int i, int i2) {
        return (i == 2000 && i2 == 1) ? PlaybackException.ERROR_CODE_IO_NETWORK_CONNECTION_FAILED : i;
    }

    public static HttpDataSource$HttpDataSourceException createForIOException(final IOException iOException, final je0 je0Var, int i) {
        int i2;
        String message = iOException.getMessage();
        if (iOException instanceof SocketTimeoutException) {
            i2 = PlaybackException.ERROR_CODE_IO_NETWORK_CONNECTION_TIMEOUT;
        } else if (iOException instanceof InterruptedIOException) {
            i2 = PlaybackException.ERROR_CODE_FAILED_RUNTIME_CHECK;
        } else {
            i2 = (message == null || !ei.e(message).matches("cleartext.*not permitted.*")) ? PlaybackException.ERROR_CODE_IO_NETWORK_CONNECTION_FAILED : 2007;
        }
        if (i2 == 2007) {
            return new HttpDataSource$HttpDataSourceException(iOException, je0Var) { // from class: androidx.media3.datasource.HttpDataSource$CleartextNotPermittedException
            };
        }
        return new HttpDataSource$HttpDataSourceException(iOException, je0Var, i2, i);
    }

    public HttpDataSource$HttpDataSourceException(je0 je0Var, int i, int i2) {
        super(a(i, i2));
        this.dataSpec = je0Var;
        this.type = i2;
    }

    @Deprecated
    public HttpDataSource$HttpDataSourceException(String str, je0 je0Var, int i) {
        this(str, je0Var, (int) PlaybackException.ERROR_CODE_IO_UNSPECIFIED, i);
    }

    public HttpDataSource$HttpDataSourceException(String str, je0 je0Var, int i, int i2) {
        super(str, a(i, i2));
        this.dataSpec = je0Var;
        this.type = i2;
    }

    @Deprecated
    public HttpDataSource$HttpDataSourceException(IOException iOException, je0 je0Var, int i) {
        this(iOException, je0Var, (int) PlaybackException.ERROR_CODE_IO_UNSPECIFIED, i);
    }

    public HttpDataSource$HttpDataSourceException(IOException iOException, je0 je0Var, int i, int i2) {
        super(iOException, a(i, i2));
        this.dataSpec = je0Var;
        this.type = i2;
    }

    @Deprecated
    public HttpDataSource$HttpDataSourceException(String str, IOException iOException, je0 je0Var, int i) {
        this(str, iOException, je0Var, PlaybackException.ERROR_CODE_IO_UNSPECIFIED, i);
    }

    public HttpDataSource$HttpDataSourceException(String str, IOException iOException, je0 je0Var, int i, int i2) {
        super(str, iOException, a(i, i2));
        this.dataSpec = je0Var;
        this.type = i2;
    }
}
