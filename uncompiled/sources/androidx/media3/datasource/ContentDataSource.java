package androidx.media3.datasource;

import android.content.ContentResolver;
import android.content.Context;
import android.content.res.AssetFileDescriptor;
import android.net.Uri;
import android.os.Bundle;
import androidx.media3.common.PlaybackException;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.nio.channels.FileChannel;
import org.web3j.ens.contracts.generated.PublicResolver;

/* loaded from: classes.dex */
public final class ContentDataSource extends fn {
    public final ContentResolver e;
    public Uri f;
    public AssetFileDescriptor g;
    public FileInputStream h;
    public long i;
    public boolean j;

    /* loaded from: classes.dex */
    public static class ContentDataSourceException extends DataSourceException {
        @Deprecated
        public ContentDataSourceException(IOException iOException) {
            this(iOException, PlaybackException.ERROR_CODE_IO_UNSPECIFIED);
        }

        public ContentDataSourceException(IOException iOException, int i) {
            super(iOException, i);
        }
    }

    public ContentDataSource(Context context) {
        super(false);
        this.e = context.getContentResolver();
    }

    @Override // androidx.media3.datasource.b
    public long b(je0 je0Var) throws ContentDataSourceException {
        AssetFileDescriptor openAssetFileDescriptor;
        int i = PlaybackException.ERROR_CODE_IO_UNSPECIFIED;
        try {
            Uri uri = je0Var.a;
            this.f = uri;
            q(je0Var);
            if (PublicResolver.FUNC_CONTENT.equals(je0Var.a.getScheme())) {
                Bundle bundle = new Bundle();
                bundle.putBoolean("android.provider.extra.ACCEPT_ORIGINAL_MEDIA_FORMAT", true);
                openAssetFileDescriptor = this.e.openTypedAssetFileDescriptor(uri, "*/*", bundle);
            } else {
                openAssetFileDescriptor = this.e.openAssetFileDescriptor(uri, "r");
            }
            this.g = openAssetFileDescriptor;
            if (openAssetFileDescriptor != null) {
                long length = openAssetFileDescriptor.getLength();
                FileInputStream fileInputStream = new FileInputStream(openAssetFileDescriptor.getFileDescriptor());
                this.h = fileInputStream;
                int i2 = (length > (-1L) ? 1 : (length == (-1L) ? 0 : -1));
                if (i2 != 0 && je0Var.f > length) {
                    throw new ContentDataSourceException(null, 2008);
                }
                long startOffset = openAssetFileDescriptor.getStartOffset();
                long skip = fileInputStream.skip(je0Var.f + startOffset) - startOffset;
                if (skip == je0Var.f) {
                    if (i2 == 0) {
                        FileChannel channel = fileInputStream.getChannel();
                        long size = channel.size();
                        if (size == 0) {
                            this.i = -1L;
                        } else {
                            long position = size - channel.position();
                            this.i = position;
                            if (position < 0) {
                                throw new ContentDataSourceException(null, 2008);
                            }
                        }
                    } else {
                        long j = length - skip;
                        this.i = j;
                        if (j < 0) {
                            throw new ContentDataSourceException(null, 2008);
                        }
                    }
                    long j2 = je0Var.g;
                    if (j2 != -1) {
                        long j3 = this.i;
                        if (j3 != -1) {
                            j2 = Math.min(j3, j2);
                        }
                        this.i = j2;
                    }
                    this.j = true;
                    r(je0Var);
                    long j4 = je0Var.g;
                    return j4 != -1 ? j4 : this.i;
                }
                throw new ContentDataSourceException(null, 2008);
            }
            throw new ContentDataSourceException(new IOException("Could not open file descriptor for: " + uri), PlaybackException.ERROR_CODE_IO_UNSPECIFIED);
        } catch (ContentDataSourceException e) {
            throw e;
        } catch (IOException e2) {
            if (e2 instanceof FileNotFoundException) {
                i = PlaybackException.ERROR_CODE_IO_FILE_NOT_FOUND;
            }
            throw new ContentDataSourceException(e2, i);
        }
    }

    @Override // androidx.media3.datasource.b
    public void close() throws ContentDataSourceException {
        this.f = null;
        try {
            try {
                FileInputStream fileInputStream = this.h;
                if (fileInputStream != null) {
                    fileInputStream.close();
                }
                this.h = null;
                try {
                    try {
                        AssetFileDescriptor assetFileDescriptor = this.g;
                        if (assetFileDescriptor != null) {
                            assetFileDescriptor.close();
                        }
                    } catch (IOException e) {
                        throw new ContentDataSourceException(e, PlaybackException.ERROR_CODE_IO_UNSPECIFIED);
                    }
                } finally {
                    this.g = null;
                    if (this.j) {
                        this.j = false;
                        p();
                    }
                }
            } catch (Throwable th) {
                this.h = null;
                try {
                    try {
                        AssetFileDescriptor assetFileDescriptor2 = this.g;
                        if (assetFileDescriptor2 != null) {
                            assetFileDescriptor2.close();
                        }
                        this.g = null;
                        if (this.j) {
                            this.j = false;
                            p();
                        }
                        throw th;
                    } finally {
                        this.g = null;
                        if (this.j) {
                            this.j = false;
                            p();
                        }
                    }
                } catch (IOException e2) {
                    throw new ContentDataSourceException(e2, PlaybackException.ERROR_CODE_IO_UNSPECIFIED);
                }
            }
        } catch (IOException e3) {
            throw new ContentDataSourceException(e3, PlaybackException.ERROR_CODE_IO_UNSPECIFIED);
        }
    }

    @Override // androidx.media3.datasource.b
    public Uri m() {
        return this.f;
    }

    @Override // androidx.media3.common.g
    public int read(byte[] bArr, int i, int i2) throws ContentDataSourceException {
        if (i2 == 0) {
            return 0;
        }
        long j = this.i;
        if (j == 0) {
            return -1;
        }
        if (j != -1) {
            try {
                i2 = (int) Math.min(j, i2);
            } catch (IOException e) {
                throw new ContentDataSourceException(e, PlaybackException.ERROR_CODE_IO_UNSPECIFIED);
            }
        }
        int read = ((FileInputStream) androidx.media3.common.util.b.j(this.h)).read(bArr, i, i2);
        if (read == -1) {
            return -1;
        }
        long j2 = this.i;
        if (j2 != -1) {
            this.i = j2 - read;
        }
        o(read);
        return read;
    }
}
