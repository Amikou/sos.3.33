package androidx.media3.datasource;

import android.content.Context;
import android.content.res.AssetManager;
import android.net.Uri;
import androidx.media3.common.PlaybackException;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;

/* loaded from: classes.dex */
public final class AssetDataSource extends fn {
    public final AssetManager e;
    public Uri f;
    public InputStream g;
    public long h;
    public boolean i;

    /* loaded from: classes.dex */
    public static final class AssetDataSourceException extends DataSourceException {
        @Deprecated
        public AssetDataSourceException(IOException iOException) {
            super(iOException, (int) PlaybackException.ERROR_CODE_IO_UNSPECIFIED);
        }

        public AssetDataSourceException(Throwable th, int i) {
            super(th, i);
        }
    }

    public AssetDataSource(Context context) {
        super(false);
        this.e = context.getAssets();
    }

    @Override // androidx.media3.datasource.b
    public long b(je0 je0Var) throws AssetDataSourceException {
        try {
            Uri uri = je0Var.a;
            this.f = uri;
            String str = (String) ii.e(uri.getPath());
            if (str.startsWith("/android_asset/")) {
                str = str.substring(15);
            } else if (str.startsWith("/")) {
                str = str.substring(1);
            }
            q(je0Var);
            InputStream open = this.e.open(str, 1);
            this.g = open;
            if (open.skip(je0Var.f) >= je0Var.f) {
                long j = je0Var.g;
                if (j != -1) {
                    this.h = j;
                } else {
                    long available = this.g.available();
                    this.h = available;
                    if (available == 2147483647L) {
                        this.h = -1L;
                    }
                }
                this.i = true;
                r(je0Var);
                return this.h;
            }
            throw new AssetDataSourceException(null, 2008);
        } catch (AssetDataSourceException e) {
            throw e;
        } catch (IOException e2) {
            throw new AssetDataSourceException(e2, e2 instanceof FileNotFoundException ? PlaybackException.ERROR_CODE_IO_FILE_NOT_FOUND : PlaybackException.ERROR_CODE_IO_UNSPECIFIED);
        }
    }

    @Override // androidx.media3.datasource.b
    public void close() throws AssetDataSourceException {
        this.f = null;
        try {
            try {
                InputStream inputStream = this.g;
                if (inputStream != null) {
                    inputStream.close();
                }
            } catch (IOException e) {
                throw new AssetDataSourceException(e, PlaybackException.ERROR_CODE_IO_UNSPECIFIED);
            }
        } finally {
            this.g = null;
            if (this.i) {
                this.i = false;
                p();
            }
        }
    }

    @Override // androidx.media3.datasource.b
    public Uri m() {
        return this.f;
    }

    @Override // androidx.media3.common.g
    public int read(byte[] bArr, int i, int i2) throws AssetDataSourceException {
        if (i2 == 0) {
            return 0;
        }
        long j = this.h;
        if (j == 0) {
            return -1;
        }
        if (j != -1) {
            try {
                i2 = (int) Math.min(j, i2);
            } catch (IOException e) {
                throw new AssetDataSourceException(e, PlaybackException.ERROR_CODE_IO_UNSPECIFIED);
            }
        }
        int read = ((InputStream) androidx.media3.common.util.b.j(this.g)).read(bArr, i, i2);
        if (read == -1) {
            return -1;
        }
        long j2 = this.h;
        if (j2 != -1) {
            this.h = j2 - read;
        }
        o(read);
        return read;
    }
}
