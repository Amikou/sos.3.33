package androidx.media3.datasource;

import androidx.media3.common.PlaybackException;
import java.io.IOException;
import java.util.List;
import java.util.Map;

/* loaded from: classes.dex */
public final class HttpDataSource$InvalidResponseCodeException extends HttpDataSource$HttpDataSourceException {
    public final Map<String, List<String>> headerFields;
    public final byte[] responseBody;
    public final int responseCode;
    public final String responseMessage;

    @Deprecated
    public HttpDataSource$InvalidResponseCodeException(int i, Map<String, List<String>> map, je0 je0Var) {
        this(i, null, null, map, je0Var, androidx.media3.common.util.b.f);
    }

    @Deprecated
    public HttpDataSource$InvalidResponseCodeException(int i, String str, Map<String, List<String>> map, je0 je0Var) {
        this(i, str, null, map, je0Var, androidx.media3.common.util.b.f);
    }

    public HttpDataSource$InvalidResponseCodeException(int i, String str, IOException iOException, Map<String, List<String>> map, je0 je0Var, byte[] bArr) {
        super("Response code: " + i, iOException, je0Var, PlaybackException.ERROR_CODE_IO_BAD_HTTP_STATUS, 1);
        this.responseCode = i;
        this.responseMessage = str;
        this.headerFields = map;
        this.responseBody = bArr;
    }
}
