package androidx.media3.datasource;

import android.net.Uri;
import android.system.ErrnoException;
import android.system.OsConstants;
import android.text.TextUtils;
import androidx.media3.common.PlaybackException;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.RandomAccessFile;

/* loaded from: classes.dex */
public final class FileDataSource extends fn {
    public RandomAccessFile e;
    public Uri f;
    public long g;
    public boolean h;

    /* loaded from: classes.dex */
    public static class FileDataSourceException extends DataSourceException {
        @Deprecated
        public FileDataSourceException(Exception exc) {
            super(exc, (int) PlaybackException.ERROR_CODE_IO_UNSPECIFIED);
        }

        @Deprecated
        public FileDataSourceException(String str, IOException iOException) {
            super(str, iOException, PlaybackException.ERROR_CODE_IO_UNSPECIFIED);
        }

        public FileDataSourceException(Throwable th, int i) {
            super(th, i);
        }

        public FileDataSourceException(String str, Throwable th, int i) {
            super(str, th, i);
        }
    }

    /* loaded from: classes.dex */
    public static final class a {
        /* JADX INFO: Access modifiers changed from: private */
        public static boolean b(Throwable th) {
            return (th instanceof ErrnoException) && ((ErrnoException) th).errno == OsConstants.EACCES;
        }
    }

    public FileDataSource() {
        super(false);
    }

    public static RandomAccessFile s(Uri uri) throws FileDataSourceException {
        int i = PlaybackException.ERROR_CODE_IO_NO_PERMISSION;
        try {
            return new RandomAccessFile((String) ii.e(uri.getPath()), "r");
        } catch (FileNotFoundException e) {
            if (TextUtils.isEmpty(uri.getQuery()) && TextUtils.isEmpty(uri.getFragment())) {
                if (androidx.media3.common.util.b.a < 21 || !a.b(e.getCause())) {
                    i = PlaybackException.ERROR_CODE_IO_FILE_NOT_FOUND;
                }
                throw new FileDataSourceException(e, i);
            }
            throw new FileDataSourceException(String.format("uri has query and/or fragment, which are not supported. Did you call Uri.parse() on a string containing '?' or '#'? Use Uri.fromFile(new File(path)) to avoid this. path=%s,query=%s,fragment=%s", uri.getPath(), uri.getQuery(), uri.getFragment()), e, PlaybackException.ERROR_CODE_FAILED_RUNTIME_CHECK);
        } catch (SecurityException e2) {
            throw new FileDataSourceException(e2, (int) PlaybackException.ERROR_CODE_IO_NO_PERMISSION);
        } catch (RuntimeException e3) {
            throw new FileDataSourceException(e3, (int) PlaybackException.ERROR_CODE_IO_UNSPECIFIED);
        }
    }

    @Override // androidx.media3.datasource.b
    public long b(je0 je0Var) throws FileDataSourceException {
        Uri uri = je0Var.a;
        this.f = uri;
        q(je0Var);
        RandomAccessFile s = s(uri);
        this.e = s;
        try {
            s.seek(je0Var.f);
            long j = je0Var.g;
            if (j == -1) {
                j = this.e.length() - je0Var.f;
            }
            this.g = j;
            if (j >= 0) {
                this.h = true;
                r(je0Var);
                return this.g;
            }
            throw new FileDataSourceException(null, null, 2008);
        } catch (IOException e) {
            throw new FileDataSourceException(e, (int) PlaybackException.ERROR_CODE_IO_UNSPECIFIED);
        }
    }

    @Override // androidx.media3.datasource.b
    public void close() throws FileDataSourceException {
        this.f = null;
        try {
            try {
                RandomAccessFile randomAccessFile = this.e;
                if (randomAccessFile != null) {
                    randomAccessFile.close();
                }
            } catch (IOException e) {
                throw new FileDataSourceException(e, (int) PlaybackException.ERROR_CODE_IO_UNSPECIFIED);
            }
        } finally {
            this.e = null;
            if (this.h) {
                this.h = false;
                p();
            }
        }
    }

    @Override // androidx.media3.datasource.b
    public Uri m() {
        return this.f;
    }

    @Override // androidx.media3.common.g
    public int read(byte[] bArr, int i, int i2) throws FileDataSourceException {
        if (i2 == 0) {
            return 0;
        }
        if (this.g == 0) {
            return -1;
        }
        try {
            int read = ((RandomAccessFile) androidx.media3.common.util.b.j(this.e)).read(bArr, i, (int) Math.min(this.g, i2));
            if (read > 0) {
                this.g -= read;
                o(read);
            }
            return read;
        } catch (IOException e) {
            throw new FileDataSourceException(e, (int) PlaybackException.ERROR_CODE_IO_UNSPECIFIED);
        }
    }
}
