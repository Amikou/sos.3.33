package androidx.media3.datasource;

import android.net.Uri;
import java.io.IOException;
import java.util.Collections;
import java.util.List;
import java.util.Map;

/* compiled from: StatsDataSource.java */
/* loaded from: classes.dex */
public final class f implements b {
    public final b a;
    public long b;
    public Uri c = Uri.EMPTY;
    public Map<String, List<String>> d = Collections.emptyMap();

    public f(b bVar) {
        this.a = (b) ii.e(bVar);
    }

    @Override // androidx.media3.datasource.b
    public long b(je0 je0Var) throws IOException {
        this.c = je0Var.a;
        this.d = Collections.emptyMap();
        long b = this.a.b(je0Var);
        this.c = (Uri) ii.e(m());
        this.d = i();
        return b;
    }

    @Override // androidx.media3.datasource.b
    public void c(fa4 fa4Var) {
        ii.e(fa4Var);
        this.a.c(fa4Var);
    }

    @Override // androidx.media3.datasource.b
    public void close() throws IOException {
        this.a.close();
    }

    @Override // androidx.media3.datasource.b
    public Map<String, List<String>> i() {
        return this.a.i();
    }

    @Override // androidx.media3.datasource.b
    public Uri m() {
        return this.a.m();
    }

    public long o() {
        return this.b;
    }

    public Uri p() {
        return this.c;
    }

    public Map<String, List<String>> q() {
        return this.d;
    }

    public void r() {
        this.b = 0L;
    }

    @Override // androidx.media3.common.g
    public int read(byte[] bArr, int i, int i2) throws IOException {
        int read = this.a.read(bArr, i, i2);
        if (read != -1) {
            this.b += read;
        }
        return read;
    }
}
