package androidx.media3.datasource;

import java.io.IOException;
import java.io.InputStream;

/* compiled from: DataSourceInputStream.java */
/* loaded from: classes.dex */
public final class c extends InputStream {
    public final b a;
    public final je0 f0;
    public long j0;
    public boolean h0 = false;
    public boolean i0 = false;
    public final byte[] g0 = new byte[1];

    public c(b bVar, je0 je0Var) {
        this.a = bVar;
        this.f0 = je0Var;
    }

    public final void a() throws IOException {
        if (this.h0) {
            return;
        }
        this.a.b(this.f0);
        this.h0 = true;
    }

    public void b() throws IOException {
        a();
    }

    @Override // java.io.InputStream, java.io.Closeable, java.lang.AutoCloseable
    public void close() throws IOException {
        if (this.i0) {
            return;
        }
        this.a.close();
        this.i0 = true;
    }

    @Override // java.io.InputStream
    public int read() throws IOException {
        if (read(this.g0) == -1) {
            return -1;
        }
        return this.g0[0] & 255;
    }

    @Override // java.io.InputStream
    public int read(byte[] bArr) throws IOException {
        return read(bArr, 0, bArr.length);
    }

    @Override // java.io.InputStream
    public int read(byte[] bArr, int i, int i2) throws IOException {
        ii.g(!this.i0);
        a();
        int read = this.a.read(bArr, i, i2);
        if (read == -1) {
            return -1;
        }
        this.j0 += read;
        return read;
    }
}
