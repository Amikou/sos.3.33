package androidx.media3.extractor.metadata.scte35;

import android.os.Parcel;
import android.os.Parcelable;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import zendesk.support.request.CellBase;

/* loaded from: classes.dex */
public final class SpliceInsertCommand extends SpliceCommand {
    public static final Parcelable.Creator<SpliceInsertCommand> CREATOR = new a();
    public final long a;
    public final boolean f0;
    public final boolean g0;
    public final boolean h0;
    public final boolean i0;
    public final long j0;
    public final long k0;
    public final List<b> l0;
    public final boolean m0;
    public final long n0;
    public final int o0;
    public final int p0;
    public final int q0;

    /* loaded from: classes.dex */
    public class a implements Parcelable.Creator<SpliceInsertCommand> {
        @Override // android.os.Parcelable.Creator
        /* renamed from: a */
        public SpliceInsertCommand createFromParcel(Parcel parcel) {
            return new SpliceInsertCommand(parcel, null);
        }

        @Override // android.os.Parcelable.Creator
        /* renamed from: b */
        public SpliceInsertCommand[] newArray(int i) {
            return new SpliceInsertCommand[i];
        }
    }

    /* loaded from: classes.dex */
    public static final class b {
        public final int a;
        public final long b;
        public final long c;

        public /* synthetic */ b(int i, long j, long j2, a aVar) {
            this(i, j, j2);
        }

        public static b a(Parcel parcel) {
            return new b(parcel.readInt(), parcel.readLong(), parcel.readLong());
        }

        public void b(Parcel parcel) {
            parcel.writeInt(this.a);
            parcel.writeLong(this.b);
            parcel.writeLong(this.c);
        }

        public b(int i, long j, long j2) {
            this.a = i;
            this.b = j;
            this.c = j2;
        }
    }

    public /* synthetic */ SpliceInsertCommand(Parcel parcel, a aVar) {
        this(parcel);
    }

    public static SpliceInsertCommand a(op2 op2Var, long j, h64 h64Var) {
        List list;
        boolean z;
        boolean z2;
        long j2;
        boolean z3;
        long j3;
        int i;
        int i2;
        int i3;
        boolean z4;
        boolean z5;
        long j4;
        long F = op2Var.F();
        boolean z6 = (op2Var.D() & 128) != 0;
        List emptyList = Collections.emptyList();
        if (z6) {
            list = emptyList;
            z = false;
            z2 = false;
            j2 = CellBase.ID_SYSTEM_MESSAGE_REQUEST_CLOSED;
            z3 = false;
            j3 = CellBase.ID_SYSTEM_MESSAGE_REQUEST_CLOSED;
            i = 0;
            i2 = 0;
            i3 = 0;
            z4 = false;
        } else {
            int D = op2Var.D();
            boolean z7 = (D & 128) != 0;
            boolean z8 = (D & 64) != 0;
            boolean z9 = (D & 32) != 0;
            boolean z10 = (D & 16) != 0;
            long b2 = (!z8 || z10) ? CellBase.ID_SYSTEM_MESSAGE_REQUEST_CLOSED : TimeSignalCommand.b(op2Var, j);
            if (!z8) {
                int D2 = op2Var.D();
                ArrayList arrayList = new ArrayList(D2);
                for (int i4 = 0; i4 < D2; i4++) {
                    int D3 = op2Var.D();
                    long b3 = !z10 ? TimeSignalCommand.b(op2Var, j) : CellBase.ID_SYSTEM_MESSAGE_REQUEST_CLOSED;
                    arrayList.add(new b(D3, b3, h64Var.b(b3), null));
                }
                emptyList = arrayList;
            }
            if (z9) {
                long D4 = op2Var.D();
                boolean z11 = (128 & D4) != 0;
                j4 = ((((D4 & 1) << 32) | op2Var.F()) * 1000) / 90;
                z5 = z11;
            } else {
                z5 = false;
                j4 = CellBase.ID_SYSTEM_MESSAGE_REQUEST_CLOSED;
            }
            i = op2Var.J();
            z4 = z8;
            i2 = op2Var.D();
            i3 = op2Var.D();
            list = emptyList;
            long j5 = b2;
            z3 = z5;
            j3 = j4;
            z2 = z10;
            z = z7;
            j2 = j5;
        }
        return new SpliceInsertCommand(F, z6, z, z4, z2, j2, h64Var.b(j2), list, z3, j3, i, i2, i3);
    }

    @Override // android.os.Parcelable
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeLong(this.a);
        parcel.writeByte(this.f0 ? (byte) 1 : (byte) 0);
        parcel.writeByte(this.g0 ? (byte) 1 : (byte) 0);
        parcel.writeByte(this.h0 ? (byte) 1 : (byte) 0);
        parcel.writeByte(this.i0 ? (byte) 1 : (byte) 0);
        parcel.writeLong(this.j0);
        parcel.writeLong(this.k0);
        int size = this.l0.size();
        parcel.writeInt(size);
        for (int i2 = 0; i2 < size; i2++) {
            this.l0.get(i2).b(parcel);
        }
        parcel.writeByte(this.m0 ? (byte) 1 : (byte) 0);
        parcel.writeLong(this.n0);
        parcel.writeInt(this.o0);
        parcel.writeInt(this.p0);
        parcel.writeInt(this.q0);
    }

    public SpliceInsertCommand(long j, boolean z, boolean z2, boolean z3, boolean z4, long j2, long j3, List<b> list, boolean z5, long j4, int i, int i2, int i3) {
        this.a = j;
        this.f0 = z;
        this.g0 = z2;
        this.h0 = z3;
        this.i0 = z4;
        this.j0 = j2;
        this.k0 = j3;
        this.l0 = Collections.unmodifiableList(list);
        this.m0 = z5;
        this.n0 = j4;
        this.o0 = i;
        this.p0 = i2;
        this.q0 = i3;
    }

    public SpliceInsertCommand(Parcel parcel) {
        this.a = parcel.readLong();
        this.f0 = parcel.readByte() == 1;
        this.g0 = parcel.readByte() == 1;
        this.h0 = parcel.readByte() == 1;
        this.i0 = parcel.readByte() == 1;
        this.j0 = parcel.readLong();
        this.k0 = parcel.readLong();
        int readInt = parcel.readInt();
        ArrayList arrayList = new ArrayList(readInt);
        for (int i = 0; i < readInt; i++) {
            arrayList.add(b.a(parcel));
        }
        this.l0 = Collections.unmodifiableList(arrayList);
        this.m0 = parcel.readByte() == 1;
        this.n0 = parcel.readLong();
        this.o0 = parcel.readInt();
        this.p0 = parcel.readInt();
        this.q0 = parcel.readInt();
    }
}
