package androidx.media3.extractor.metadata.scte35;

import android.os.Parcel;
import android.os.Parcelable;
import zendesk.support.request.CellBase;

/* loaded from: classes.dex */
public final class TimeSignalCommand extends SpliceCommand {
    public static final Parcelable.Creator<TimeSignalCommand> CREATOR = new a();
    public final long a;
    public final long f0;

    /* loaded from: classes.dex */
    public class a implements Parcelable.Creator<TimeSignalCommand> {
        @Override // android.os.Parcelable.Creator
        /* renamed from: a */
        public TimeSignalCommand createFromParcel(Parcel parcel) {
            return new TimeSignalCommand(parcel.readLong(), parcel.readLong(), null);
        }

        @Override // android.os.Parcelable.Creator
        /* renamed from: b */
        public TimeSignalCommand[] newArray(int i) {
            return new TimeSignalCommand[i];
        }
    }

    public /* synthetic */ TimeSignalCommand(long j, long j2, a aVar) {
        this(j, j2);
    }

    public static TimeSignalCommand a(op2 op2Var, long j, h64 h64Var) {
        long b = b(op2Var, j);
        return new TimeSignalCommand(b, h64Var.b(b));
    }

    public static long b(op2 op2Var, long j) {
        long D = op2Var.D();
        return (128 & D) != 0 ? 8589934591L & ((((D & 1) << 32) | op2Var.F()) + j) : CellBase.ID_SYSTEM_MESSAGE_REQUEST_CLOSED;
    }

    @Override // android.os.Parcelable
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeLong(this.a);
        parcel.writeLong(this.f0);
    }

    public TimeSignalCommand(long j, long j2) {
        this.a = j;
        this.f0 = j2;
    }
}
