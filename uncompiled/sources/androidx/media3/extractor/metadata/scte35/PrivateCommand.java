package androidx.media3.extractor.metadata.scte35;

import android.os.Parcel;
import android.os.Parcelable;
import androidx.media3.common.util.b;

/* loaded from: classes.dex */
public final class PrivateCommand extends SpliceCommand {
    public static final Parcelable.Creator<PrivateCommand> CREATOR = new a();
    public final long a;
    public final long f0;
    public final byte[] g0;

    /* loaded from: classes.dex */
    public class a implements Parcelable.Creator<PrivateCommand> {
        @Override // android.os.Parcelable.Creator
        /* renamed from: a */
        public PrivateCommand createFromParcel(Parcel parcel) {
            return new PrivateCommand(parcel, null);
        }

        @Override // android.os.Parcelable.Creator
        /* renamed from: b */
        public PrivateCommand[] newArray(int i) {
            return new PrivateCommand[i];
        }
    }

    public /* synthetic */ PrivateCommand(Parcel parcel, a aVar) {
        this(parcel);
    }

    public static PrivateCommand a(op2 op2Var, int i, long j) {
        long F = op2Var.F();
        int i2 = i - 4;
        byte[] bArr = new byte[i2];
        op2Var.j(bArr, 0, i2);
        return new PrivateCommand(F, bArr, j);
    }

    @Override // android.os.Parcelable
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeLong(this.a);
        parcel.writeLong(this.f0);
        parcel.writeByteArray(this.g0);
    }

    public PrivateCommand(long j, byte[] bArr, long j2) {
        this.a = j2;
        this.f0 = j;
        this.g0 = bArr;
    }

    public PrivateCommand(Parcel parcel) {
        this.a = parcel.readLong();
        this.f0 = parcel.readLong();
        this.g0 = (byte[]) b.j(parcel.createByteArray());
    }
}
