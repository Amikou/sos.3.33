package androidx.media3.extractor.metadata.scte35;

import android.os.Parcel;
import android.os.Parcelable;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import zendesk.support.request.CellBase;

/* loaded from: classes.dex */
public final class SpliceScheduleCommand extends SpliceCommand {
    public static final Parcelable.Creator<SpliceScheduleCommand> CREATOR = new a();
    public final List<c> a;

    /* loaded from: classes.dex */
    public class a implements Parcelable.Creator<SpliceScheduleCommand> {
        @Override // android.os.Parcelable.Creator
        /* renamed from: a */
        public SpliceScheduleCommand createFromParcel(Parcel parcel) {
            return new SpliceScheduleCommand(parcel, null);
        }

        @Override // android.os.Parcelable.Creator
        /* renamed from: b */
        public SpliceScheduleCommand[] newArray(int i) {
            return new SpliceScheduleCommand[i];
        }
    }

    /* loaded from: classes.dex */
    public static final class b {
        public final int a;
        public final long b;

        public /* synthetic */ b(int i, long j, a aVar) {
            this(i, j);
        }

        public static b c(Parcel parcel) {
            return new b(parcel.readInt(), parcel.readLong());
        }

        public final void d(Parcel parcel) {
            parcel.writeInt(this.a);
            parcel.writeLong(this.b);
        }

        public b(int i, long j) {
            this.a = i;
            this.b = j;
        }
    }

    public /* synthetic */ SpliceScheduleCommand(Parcel parcel, a aVar) {
        this(parcel);
    }

    public static SpliceScheduleCommand a(op2 op2Var) {
        int D = op2Var.D();
        ArrayList arrayList = new ArrayList(D);
        for (int i = 0; i < D; i++) {
            arrayList.add(c.e(op2Var));
        }
        return new SpliceScheduleCommand(arrayList);
    }

    @Override // android.os.Parcelable
    public void writeToParcel(Parcel parcel, int i) {
        int size = this.a.size();
        parcel.writeInt(size);
        for (int i2 = 0; i2 < size; i2++) {
            this.a.get(i2).f(parcel);
        }
    }

    public SpliceScheduleCommand(List<c> list) {
        this.a = Collections.unmodifiableList(list);
    }

    public SpliceScheduleCommand(Parcel parcel) {
        int readInt = parcel.readInt();
        ArrayList arrayList = new ArrayList(readInt);
        for (int i = 0; i < readInt; i++) {
            arrayList.add(c.d(parcel));
        }
        this.a = Collections.unmodifiableList(arrayList);
    }

    /* loaded from: classes.dex */
    public static final class c {
        public final long a;
        public final boolean b;
        public final boolean c;
        public final boolean d;
        public final long e;
        public final List<b> f;
        public final boolean g;
        public final long h;
        public final int i;
        public final int j;
        public final int k;

        public c(long j, boolean z, boolean z2, boolean z3, List<b> list, long j2, boolean z4, long j3, int i, int i2, int i3) {
            this.a = j;
            this.b = z;
            this.c = z2;
            this.d = z3;
            this.f = Collections.unmodifiableList(list);
            this.e = j2;
            this.g = z4;
            this.h = j3;
            this.i = i;
            this.j = i2;
            this.k = i3;
        }

        public static c d(Parcel parcel) {
            return new c(parcel);
        }

        public static c e(op2 op2Var) {
            ArrayList arrayList;
            boolean z;
            long j;
            boolean z2;
            long j2;
            int i;
            int i2;
            int i3;
            boolean z3;
            boolean z4;
            long j3;
            long F = op2Var.F();
            boolean z5 = (op2Var.D() & 128) != 0;
            ArrayList arrayList2 = new ArrayList();
            if (z5) {
                arrayList = arrayList2;
                z = false;
                j = CellBase.ID_SYSTEM_MESSAGE_REQUEST_CLOSED;
                z2 = false;
                j2 = CellBase.ID_SYSTEM_MESSAGE_REQUEST_CLOSED;
                i = 0;
                i2 = 0;
                i3 = 0;
                z3 = false;
            } else {
                int D = op2Var.D();
                boolean z6 = (D & 128) != 0;
                boolean z7 = (D & 64) != 0;
                boolean z8 = (D & 32) != 0;
                long F2 = z7 ? op2Var.F() : CellBase.ID_SYSTEM_MESSAGE_REQUEST_CLOSED;
                if (!z7) {
                    int D2 = op2Var.D();
                    ArrayList arrayList3 = new ArrayList(D2);
                    for (int i4 = 0; i4 < D2; i4++) {
                        arrayList3.add(new b(op2Var.D(), op2Var.F(), null));
                    }
                    arrayList2 = arrayList3;
                }
                if (z8) {
                    long D3 = op2Var.D();
                    boolean z9 = (128 & D3) != 0;
                    j3 = ((((D3 & 1) << 32) | op2Var.F()) * 1000) / 90;
                    z4 = z9;
                } else {
                    z4 = false;
                    j3 = CellBase.ID_SYSTEM_MESSAGE_REQUEST_CLOSED;
                }
                int J = op2Var.J();
                int D4 = op2Var.D();
                z3 = z7;
                i3 = op2Var.D();
                j2 = j3;
                arrayList = arrayList2;
                long j4 = F2;
                i = J;
                i2 = D4;
                j = j4;
                boolean z10 = z6;
                z2 = z4;
                z = z10;
            }
            return new c(F, z5, z, z3, arrayList, j, z2, j2, i, i2, i3);
        }

        public final void f(Parcel parcel) {
            parcel.writeLong(this.a);
            parcel.writeByte(this.b ? (byte) 1 : (byte) 0);
            parcel.writeByte(this.c ? (byte) 1 : (byte) 0);
            parcel.writeByte(this.d ? (byte) 1 : (byte) 0);
            int size = this.f.size();
            parcel.writeInt(size);
            for (int i = 0; i < size; i++) {
                this.f.get(i).d(parcel);
            }
            parcel.writeLong(this.e);
            parcel.writeByte(this.g ? (byte) 1 : (byte) 0);
            parcel.writeLong(this.h);
            parcel.writeInt(this.i);
            parcel.writeInt(this.j);
            parcel.writeInt(this.k);
        }

        public c(Parcel parcel) {
            this.a = parcel.readLong();
            this.b = parcel.readByte() == 1;
            this.c = parcel.readByte() == 1;
            this.d = parcel.readByte() == 1;
            int readInt = parcel.readInt();
            ArrayList arrayList = new ArrayList(readInt);
            for (int i = 0; i < readInt; i++) {
                arrayList.add(b.c(parcel));
            }
            this.f = Collections.unmodifiableList(arrayList);
            this.e = parcel.readLong();
            this.g = parcel.readByte() == 1;
            this.h = parcel.readLong();
            this.i = parcel.readInt();
            this.j = parcel.readInt();
            this.k = parcel.readInt();
        }
    }
}
