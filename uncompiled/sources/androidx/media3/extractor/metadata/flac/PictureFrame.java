package androidx.media3.extractor.metadata.flac;

import android.os.Parcel;
import android.os.Parcelable;
import androidx.media3.common.Metadata;
import androidx.media3.common.j;
import androidx.media3.common.n;
import androidx.media3.common.util.b;
import java.util.Arrays;

/* loaded from: classes.dex */
public final class PictureFrame implements Metadata.Entry {
    public static final Parcelable.Creator<PictureFrame> CREATOR = new a();
    public final int a;
    public final String f0;
    public final String g0;
    public final int h0;
    public final int i0;
    public final int j0;
    public final int k0;
    public final byte[] l0;

    /* loaded from: classes.dex */
    public class a implements Parcelable.Creator<PictureFrame> {
        @Override // android.os.Parcelable.Creator
        /* renamed from: a */
        public PictureFrame createFromParcel(Parcel parcel) {
            return new PictureFrame(parcel);
        }

        @Override // android.os.Parcelable.Creator
        /* renamed from: b */
        public PictureFrame[] newArray(int i) {
            return new PictureFrame[i];
        }
    }

    public PictureFrame(int i, String str, String str2, int i2, int i3, int i4, int i5, byte[] bArr) {
        this.a = i;
        this.f0 = str;
        this.g0 = str2;
        this.h0 = i2;
        this.i0 = i3;
        this.j0 = i4;
        this.k0 = i5;
        this.l0 = bArr;
    }

    public static PictureFrame a(op2 op2Var) {
        int n = op2Var.n();
        String B = op2Var.B(op2Var.n(), cy.a);
        String A = op2Var.A(op2Var.n());
        int n2 = op2Var.n();
        int n3 = op2Var.n();
        int n4 = op2Var.n();
        int n5 = op2Var.n();
        int n6 = op2Var.n();
        byte[] bArr = new byte[n6];
        op2Var.j(bArr, 0, n6);
        return new PictureFrame(n, B, A, n2, n3, n4, n5, bArr);
    }

    @Override // android.os.Parcelable
    public int describeContents() {
        return 0;
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null || PictureFrame.class != obj.getClass()) {
            return false;
        }
        PictureFrame pictureFrame = (PictureFrame) obj;
        return this.a == pictureFrame.a && this.f0.equals(pictureFrame.f0) && this.g0.equals(pictureFrame.g0) && this.h0 == pictureFrame.h0 && this.i0 == pictureFrame.i0 && this.j0 == pictureFrame.j0 && this.k0 == pictureFrame.k0 && Arrays.equals(this.l0, pictureFrame.l0);
    }

    @Override // androidx.media3.common.Metadata.Entry
    public /* synthetic */ j f0() {
        return h82.b(this);
    }

    public int hashCode() {
        return ((((((((((((((527 + this.a) * 31) + this.f0.hashCode()) * 31) + this.g0.hashCode()) * 31) + this.h0) * 31) + this.i0) * 31) + this.j0) * 31) + this.k0) * 31) + Arrays.hashCode(this.l0);
    }

    @Override // androidx.media3.common.Metadata.Entry
    public void r0(n.b bVar) {
        bVar.G(this.l0, this.a);
    }

    public String toString() {
        return "Picture: mimeType=" + this.f0 + ", description=" + this.g0;
    }

    @Override // android.os.Parcelable
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeInt(this.a);
        parcel.writeString(this.f0);
        parcel.writeString(this.g0);
        parcel.writeInt(this.h0);
        parcel.writeInt(this.i0);
        parcel.writeInt(this.j0);
        parcel.writeInt(this.k0);
        parcel.writeByteArray(this.l0);
    }

    @Override // androidx.media3.common.Metadata.Entry
    public /* synthetic */ byte[] y1() {
        return h82.a(this);
    }

    public PictureFrame(Parcel parcel) {
        this.a = parcel.readInt();
        this.f0 = (String) b.j(parcel.readString());
        this.g0 = (String) b.j(parcel.readString());
        this.h0 = parcel.readInt();
        this.i0 = parcel.readInt();
        this.j0 = parcel.readInt();
        this.k0 = parcel.readInt();
        this.l0 = (byte[]) b.j(parcel.createByteArray());
    }
}
