package androidx.media3.extractor.metadata.flac;

import android.os.Parcel;
import android.os.Parcelable;
import androidx.media3.common.Metadata;
import androidx.media3.common.j;
import androidx.media3.common.n;
import androidx.media3.common.util.b;

@Deprecated
/* loaded from: classes.dex */
public class VorbisComment implements Metadata.Entry {
    public static final Parcelable.Creator<VorbisComment> CREATOR = new a();
    public final String a;
    public final String f0;

    /* loaded from: classes.dex */
    public class a implements Parcelable.Creator<VorbisComment> {
        @Override // android.os.Parcelable.Creator
        /* renamed from: a */
        public VorbisComment createFromParcel(Parcel parcel) {
            return new VorbisComment(parcel);
        }

        @Override // android.os.Parcelable.Creator
        /* renamed from: b */
        public VorbisComment[] newArray(int i) {
            return new VorbisComment[i];
        }
    }

    public VorbisComment(String str, String str2) {
        this.a = str;
        this.f0 = str2;
    }

    @Override // android.os.Parcelable
    public int describeContents() {
        return 0;
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null || getClass() != obj.getClass()) {
            return false;
        }
        VorbisComment vorbisComment = (VorbisComment) obj;
        return this.a.equals(vorbisComment.a) && this.f0.equals(vorbisComment.f0);
    }

    @Override // androidx.media3.common.Metadata.Entry
    public /* synthetic */ j f0() {
        return h82.b(this);
    }

    public int hashCode() {
        return ((527 + this.a.hashCode()) * 31) + this.f0.hashCode();
    }

    @Override // androidx.media3.common.Metadata.Entry
    public void r0(n.b bVar) {
        String str = this.a;
        str.hashCode();
        char c = 65535;
        switch (str.hashCode()) {
            case 62359119:
                if (str.equals("ALBUM")) {
                    c = 0;
                    break;
                }
                break;
            case 79833656:
                if (str.equals("TITLE")) {
                    c = 1;
                    break;
                }
                break;
            case 428414940:
                if (str.equals("DESCRIPTION")) {
                    c = 2;
                    break;
                }
                break;
            case 1746739798:
                if (str.equals("ALBUMARTIST")) {
                    c = 3;
                    break;
                }
                break;
            case 1939198791:
                if (str.equals("ARTIST")) {
                    c = 4;
                    break;
                }
                break;
        }
        switch (c) {
            case 0:
                bVar.L(this.f0);
                return;
            case 1:
                bVar.i0(this.f0);
                return;
            case 2:
                bVar.S(this.f0);
                return;
            case 3:
                bVar.K(this.f0);
                return;
            case 4:
                bVar.M(this.f0);
                return;
            default:
                return;
        }
    }

    public String toString() {
        return "VC: " + this.a + "=" + this.f0;
    }

    @Override // android.os.Parcelable
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeString(this.a);
        parcel.writeString(this.f0);
    }

    @Override // androidx.media3.common.Metadata.Entry
    public /* synthetic */ byte[] y1() {
        return h82.a(this);
    }

    public VorbisComment(Parcel parcel) {
        this.a = (String) b.j(parcel.readString());
        this.f0 = (String) b.j(parcel.readString());
    }
}
