package androidx.media3.extractor.metadata.emsg;

import androidx.recyclerview.widget.RecyclerView;
import java.io.ByteArrayOutputStream;
import java.io.DataOutputStream;
import java.io.IOException;

/* compiled from: EventMessageEncoder.java */
/* loaded from: classes.dex */
public final class a {
    public final ByteArrayOutputStream a;
    public final DataOutputStream b;

    public a() {
        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream(RecyclerView.a0.FLAG_ADAPTER_POSITION_UNKNOWN);
        this.a = byteArrayOutputStream;
        this.b = new DataOutputStream(byteArrayOutputStream);
    }

    public static void b(DataOutputStream dataOutputStream, String str) throws IOException {
        dataOutputStream.writeBytes(str);
        dataOutputStream.writeByte(0);
    }

    public byte[] a(EventMessage eventMessage) {
        this.a.reset();
        try {
            b(this.b, eventMessage.a);
            String str = eventMessage.f0;
            if (str == null) {
                str = "";
            }
            b(this.b, str);
            this.b.writeLong(eventMessage.g0);
            this.b.writeLong(eventMessage.h0);
            this.b.write(eventMessage.i0);
            this.b.flush();
            return this.a.toByteArray();
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }
}
