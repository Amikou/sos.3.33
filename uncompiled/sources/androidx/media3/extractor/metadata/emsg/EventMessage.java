package androidx.media3.extractor.metadata.emsg;

import android.os.Parcel;
import android.os.Parcelable;
import androidx.media3.common.Metadata;
import androidx.media3.common.j;
import androidx.media3.common.n;
import androidx.media3.common.util.b;
import java.util.Arrays;

/* loaded from: classes.dex */
public final class EventMessage implements Metadata.Entry {
    public final String a;
    public final String f0;
    public final long g0;
    public final long h0;
    public final byte[] i0;
    public int j0;
    public static final j k0 = new j.b().e0("application/id3").E();
    public static final j l0 = new j.b().e0("application/x-scte35").E();
    public static final Parcelable.Creator<EventMessage> CREATOR = new a();

    /* loaded from: classes.dex */
    public class a implements Parcelable.Creator<EventMessage> {
        @Override // android.os.Parcelable.Creator
        /* renamed from: a */
        public EventMessage createFromParcel(Parcel parcel) {
            return new EventMessage(parcel);
        }

        @Override // android.os.Parcelable.Creator
        /* renamed from: b */
        public EventMessage[] newArray(int i) {
            return new EventMessage[i];
        }
    }

    public EventMessage(String str, String str2, long j, long j2, byte[] bArr) {
        this.a = str;
        this.f0 = str2;
        this.g0 = j;
        this.h0 = j2;
        this.i0 = bArr;
    }

    @Override // android.os.Parcelable
    public int describeContents() {
        return 0;
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null || EventMessage.class != obj.getClass()) {
            return false;
        }
        EventMessage eventMessage = (EventMessage) obj;
        return this.g0 == eventMessage.g0 && this.h0 == eventMessage.h0 && b.c(this.a, eventMessage.a) && b.c(this.f0, eventMessage.f0) && Arrays.equals(this.i0, eventMessage.i0);
    }

    @Override // androidx.media3.common.Metadata.Entry
    public j f0() {
        String str = this.a;
        str.hashCode();
        char c = 65535;
        switch (str.hashCode()) {
            case -1468477611:
                if (str.equals("urn:scte:scte35:2014:bin")) {
                    c = 0;
                    break;
                }
                break;
            case -795945609:
                if (str.equals("https://aomedia.org/emsg/ID3")) {
                    c = 1;
                    break;
                }
                break;
            case 1303648457:
                if (str.equals("https://developer.apple.com/streaming/emsg-id3")) {
                    c = 2;
                    break;
                }
                break;
        }
        switch (c) {
            case 0:
                return l0;
            case 1:
            case 2:
                return k0;
            default:
                return null;
        }
    }

    public int hashCode() {
        if (this.j0 == 0) {
            String str = this.a;
            int hashCode = (527 + (str != null ? str.hashCode() : 0)) * 31;
            String str2 = this.f0;
            int hashCode2 = str2 != null ? str2.hashCode() : 0;
            long j = this.g0;
            long j2 = this.h0;
            this.j0 = ((((((hashCode + hashCode2) * 31) + ((int) (j ^ (j >>> 32)))) * 31) + ((int) (j2 ^ (j2 >>> 32)))) * 31) + Arrays.hashCode(this.i0);
        }
        return this.j0;
    }

    @Override // androidx.media3.common.Metadata.Entry
    public /* synthetic */ void r0(n.b bVar) {
        h82.c(this, bVar);
    }

    public String toString() {
        return "EMSG: scheme=" + this.a + ", id=" + this.h0 + ", durationMs=" + this.g0 + ", value=" + this.f0;
    }

    @Override // android.os.Parcelable
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeString(this.a);
        parcel.writeString(this.f0);
        parcel.writeLong(this.g0);
        parcel.writeLong(this.h0);
        parcel.writeByteArray(this.i0);
    }

    @Override // androidx.media3.common.Metadata.Entry
    public byte[] y1() {
        if (f0() != null) {
            return this.i0;
        }
        return null;
    }

    public EventMessage(Parcel parcel) {
        this.a = (String) b.j(parcel.readString());
        this.f0 = (String) b.j(parcel.readString());
        this.g0 = parcel.readLong();
        this.h0 = parcel.readLong();
        this.i0 = (byte[]) b.j(parcel.createByteArray());
    }
}
