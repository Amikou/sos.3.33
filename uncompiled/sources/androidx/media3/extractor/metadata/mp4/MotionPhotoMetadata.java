package androidx.media3.extractor.metadata.mp4;

import android.os.Parcel;
import android.os.Parcelable;
import androidx.media3.common.Metadata;
import androidx.media3.common.j;
import androidx.media3.common.n;

/* loaded from: classes.dex */
public final class MotionPhotoMetadata implements Metadata.Entry {
    public static final Parcelable.Creator<MotionPhotoMetadata> CREATOR = new a();
    public final long a;
    public final long f0;
    public final long g0;
    public final long h0;
    public final long i0;

    /* loaded from: classes.dex */
    public class a implements Parcelable.Creator<MotionPhotoMetadata> {
        @Override // android.os.Parcelable.Creator
        /* renamed from: a */
        public MotionPhotoMetadata createFromParcel(Parcel parcel) {
            return new MotionPhotoMetadata(parcel, null);
        }

        @Override // android.os.Parcelable.Creator
        /* renamed from: b */
        public MotionPhotoMetadata[] newArray(int i) {
            return new MotionPhotoMetadata[i];
        }
    }

    public /* synthetic */ MotionPhotoMetadata(Parcel parcel, a aVar) {
        this(parcel);
    }

    @Override // android.os.Parcelable
    public int describeContents() {
        return 0;
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null || MotionPhotoMetadata.class != obj.getClass()) {
            return false;
        }
        MotionPhotoMetadata motionPhotoMetadata = (MotionPhotoMetadata) obj;
        return this.a == motionPhotoMetadata.a && this.f0 == motionPhotoMetadata.f0 && this.g0 == motionPhotoMetadata.g0 && this.h0 == motionPhotoMetadata.h0 && this.i0 == motionPhotoMetadata.i0;
    }

    @Override // androidx.media3.common.Metadata.Entry
    public /* synthetic */ j f0() {
        return h82.b(this);
    }

    public int hashCode() {
        return ((((((((527 + l22.a(this.a)) * 31) + l22.a(this.f0)) * 31) + l22.a(this.g0)) * 31) + l22.a(this.h0)) * 31) + l22.a(this.i0);
    }

    @Override // androidx.media3.common.Metadata.Entry
    public /* synthetic */ void r0(n.b bVar) {
        h82.c(this, bVar);
    }

    public String toString() {
        return "Motion photo metadata: photoStartPosition=" + this.a + ", photoSize=" + this.f0 + ", photoPresentationTimestampUs=" + this.g0 + ", videoStartPosition=" + this.h0 + ", videoSize=" + this.i0;
    }

    @Override // android.os.Parcelable
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeLong(this.a);
        parcel.writeLong(this.f0);
        parcel.writeLong(this.g0);
        parcel.writeLong(this.h0);
        parcel.writeLong(this.i0);
    }

    @Override // androidx.media3.common.Metadata.Entry
    public /* synthetic */ byte[] y1() {
        return h82.a(this);
    }

    public MotionPhotoMetadata(long j, long j2, long j3, long j4, long j5) {
        this.a = j;
        this.f0 = j2;
        this.g0 = j3;
        this.h0 = j4;
        this.i0 = j5;
    }

    public MotionPhotoMetadata(Parcel parcel) {
        this.a = parcel.readLong();
        this.f0 = parcel.readLong();
        this.g0 = parcel.readLong();
        this.h0 = parcel.readLong();
        this.i0 = parcel.readLong();
    }
}
