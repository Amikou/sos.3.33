package androidx.media3.extractor.metadata.mp4;

import android.os.Parcel;
import android.os.Parcelable;
import androidx.media3.common.Metadata;
import androidx.media3.common.j;
import androidx.media3.common.n;
import androidx.media3.common.util.b;
import java.util.ArrayList;
import java.util.List;

/* loaded from: classes.dex */
public final class SlowMotionData implements Metadata.Entry {
    public static final Parcelable.Creator<SlowMotionData> CREATOR = new a();
    public final List<Segment> a;

    /* loaded from: classes.dex */
    public static final class Segment implements Parcelable {
        public static final Parcelable.Creator<Segment> CREATOR = new a();
        public final long a;
        public final long f0;
        public final int g0;

        /* loaded from: classes.dex */
        public class a implements Parcelable.Creator<Segment> {
            @Override // android.os.Parcelable.Creator
            /* renamed from: a */
            public Segment createFromParcel(Parcel parcel) {
                return new Segment(parcel.readLong(), parcel.readLong(), parcel.readInt());
            }

            @Override // android.os.Parcelable.Creator
            /* renamed from: b */
            public Segment[] newArray(int i) {
                return new Segment[i];
            }
        }

        public Segment(long j, long j2, int i) {
            ii.a(j < j2);
            this.a = j;
            this.f0 = j2;
            this.g0 = i;
        }

        @Override // android.os.Parcelable
        public int describeContents() {
            return 0;
        }

        public boolean equals(Object obj) {
            if (this == obj) {
                return true;
            }
            if (obj == null || Segment.class != obj.getClass()) {
                return false;
            }
            Segment segment = (Segment) obj;
            return this.a == segment.a && this.f0 == segment.f0 && this.g0 == segment.g0;
        }

        public int hashCode() {
            return ql2.b(Long.valueOf(this.a), Long.valueOf(this.f0), Integer.valueOf(this.g0));
        }

        public String toString() {
            return b.A("Segment: startTimeMs=%d, endTimeMs=%d, speedDivisor=%d", Long.valueOf(this.a), Long.valueOf(this.f0), Integer.valueOf(this.g0));
        }

        @Override // android.os.Parcelable
        public void writeToParcel(Parcel parcel, int i) {
            parcel.writeLong(this.a);
            parcel.writeLong(this.f0);
            parcel.writeInt(this.g0);
        }
    }

    /* loaded from: classes.dex */
    public class a implements Parcelable.Creator<SlowMotionData> {
        @Override // android.os.Parcelable.Creator
        /* renamed from: a */
        public SlowMotionData createFromParcel(Parcel parcel) {
            ArrayList arrayList = new ArrayList();
            parcel.readList(arrayList, Segment.class.getClassLoader());
            return new SlowMotionData(arrayList);
        }

        @Override // android.os.Parcelable.Creator
        /* renamed from: b */
        public SlowMotionData[] newArray(int i) {
            return new SlowMotionData[i];
        }
    }

    public SlowMotionData(List<Segment> list) {
        this.a = list;
        ii.a(!a(list));
    }

    public static boolean a(List<Segment> list) {
        if (list.isEmpty()) {
            return false;
        }
        long j = list.get(0).f0;
        for (int i = 1; i < list.size(); i++) {
            if (list.get(i).a < j) {
                return true;
            }
            j = list.get(i).f0;
        }
        return false;
    }

    @Override // android.os.Parcelable
    public int describeContents() {
        return 0;
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null || SlowMotionData.class != obj.getClass()) {
            return false;
        }
        return this.a.equals(((SlowMotionData) obj).a);
    }

    @Override // androidx.media3.common.Metadata.Entry
    public /* synthetic */ j f0() {
        return h82.b(this);
    }

    public int hashCode() {
        return this.a.hashCode();
    }

    @Override // androidx.media3.common.Metadata.Entry
    public /* synthetic */ void r0(n.b bVar) {
        h82.c(this, bVar);
    }

    public String toString() {
        return "SlowMotion: segments=" + this.a;
    }

    @Override // android.os.Parcelable
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeList(this.a);
    }

    @Override // androidx.media3.common.Metadata.Entry
    public /* synthetic */ byte[] y1() {
        return h82.a(this);
    }
}
