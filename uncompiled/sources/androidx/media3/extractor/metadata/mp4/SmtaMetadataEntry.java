package androidx.media3.extractor.metadata.mp4;

import android.os.Parcel;
import android.os.Parcelable;
import androidx.media3.common.Metadata;
import androidx.media3.common.j;
import androidx.media3.common.n;

/* loaded from: classes.dex */
public final class SmtaMetadataEntry implements Metadata.Entry {
    public static final Parcelable.Creator<SmtaMetadataEntry> CREATOR = new a();
    public final float a;
    public final int f0;

    /* loaded from: classes.dex */
    public class a implements Parcelable.Creator<SmtaMetadataEntry> {
        @Override // android.os.Parcelable.Creator
        /* renamed from: a */
        public SmtaMetadataEntry createFromParcel(Parcel parcel) {
            return new SmtaMetadataEntry(parcel, (a) null);
        }

        @Override // android.os.Parcelable.Creator
        /* renamed from: b */
        public SmtaMetadataEntry[] newArray(int i) {
            return new SmtaMetadataEntry[i];
        }
    }

    public /* synthetic */ SmtaMetadataEntry(Parcel parcel, a aVar) {
        this(parcel);
    }

    @Override // android.os.Parcelable
    public int describeContents() {
        return 0;
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null || SmtaMetadataEntry.class != obj.getClass()) {
            return false;
        }
        SmtaMetadataEntry smtaMetadataEntry = (SmtaMetadataEntry) obj;
        return this.a == smtaMetadataEntry.a && this.f0 == smtaMetadataEntry.f0;
    }

    @Override // androidx.media3.common.Metadata.Entry
    public /* synthetic */ j f0() {
        return h82.b(this);
    }

    public int hashCode() {
        return ((527 + h71.a(this.a)) * 31) + this.f0;
    }

    @Override // androidx.media3.common.Metadata.Entry
    public /* synthetic */ void r0(n.b bVar) {
        h82.c(this, bVar);
    }

    public String toString() {
        return "smta: captureFrameRate=" + this.a + ", svcTemporalLayerCount=" + this.f0;
    }

    @Override // android.os.Parcelable
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeFloat(this.a);
        parcel.writeInt(this.f0);
    }

    @Override // androidx.media3.common.Metadata.Entry
    public /* synthetic */ byte[] y1() {
        return h82.a(this);
    }

    public SmtaMetadataEntry(float f, int i) {
        this.a = f;
        this.f0 = i;
    }

    public SmtaMetadataEntry(Parcel parcel) {
        this.a = parcel.readFloat();
        this.f0 = parcel.readInt();
    }
}
