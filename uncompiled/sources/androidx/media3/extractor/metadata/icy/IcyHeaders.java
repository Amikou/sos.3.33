package androidx.media3.extractor.metadata.icy;

import android.os.Parcel;
import android.os.Parcelable;
import androidx.media3.common.Metadata;
import androidx.media3.common.j;
import androidx.media3.common.n;
import androidx.media3.common.util.b;

/* loaded from: classes.dex */
public final class IcyHeaders implements Metadata.Entry {
    public static final Parcelable.Creator<IcyHeaders> CREATOR = new a();
    public final int a;
    public final String f0;
    public final String g0;
    public final String h0;
    public final boolean i0;
    public final int j0;

    /* loaded from: classes.dex */
    public class a implements Parcelable.Creator<IcyHeaders> {
        @Override // android.os.Parcelable.Creator
        /* renamed from: a */
        public IcyHeaders createFromParcel(Parcel parcel) {
            return new IcyHeaders(parcel);
        }

        @Override // android.os.Parcelable.Creator
        /* renamed from: b */
        public IcyHeaders[] newArray(int i) {
            return new IcyHeaders[i];
        }
    }

    public IcyHeaders(int i, String str, String str2, String str3, boolean z, int i2) {
        ii.a(i2 == -1 || i2 > 0);
        this.a = i;
        this.f0 = str;
        this.g0 = str2;
        this.h0 = str3;
        this.i0 = z;
        this.j0 = i2;
    }

    /* JADX WARN: Removed duplicated region for block: B:18:0x005c  */
    /* JADX WARN: Removed duplicated region for block: B:19:0x0065  */
    /* JADX WARN: Removed duplicated region for block: B:22:0x0070  */
    /* JADX WARN: Removed duplicated region for block: B:23:0x0079  */
    /* JADX WARN: Removed duplicated region for block: B:26:0x0084  */
    /* JADX WARN: Removed duplicated region for block: B:27:0x008d  */
    /* JADX WARN: Removed duplicated region for block: B:30:0x0098  */
    /* JADX WARN: Removed duplicated region for block: B:31:0x00a7  */
    /* JADX WARN: Removed duplicated region for block: B:34:0x00b2  */
    /* JADX WARN: Removed duplicated region for block: B:44:0x00ea  */
    /* JADX WARN: Removed duplicated region for block: B:56:? A[RETURN, SYNTHETIC] */
    /*
        Code decompiled incorrectly, please refer to instructions dump.
        To view partially-correct code enable 'Show inconsistent code' option in preferences
    */
    public static androidx.media3.extractor.metadata.icy.IcyHeaders a(java.util.Map<java.lang.String, java.util.List<java.lang.String>> r13) {
        /*
            Method dump skipped, instructions count: 247
            To view this dump change 'Code comments level' option to 'DEBUG'
        */
        throw new UnsupportedOperationException("Method not decompiled: androidx.media3.extractor.metadata.icy.IcyHeaders.a(java.util.Map):androidx.media3.extractor.metadata.icy.IcyHeaders");
    }

    @Override // android.os.Parcelable
    public int describeContents() {
        return 0;
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null || IcyHeaders.class != obj.getClass()) {
            return false;
        }
        IcyHeaders icyHeaders = (IcyHeaders) obj;
        return this.a == icyHeaders.a && b.c(this.f0, icyHeaders.f0) && b.c(this.g0, icyHeaders.g0) && b.c(this.h0, icyHeaders.h0) && this.i0 == icyHeaders.i0 && this.j0 == icyHeaders.j0;
    }

    @Override // androidx.media3.common.Metadata.Entry
    public /* synthetic */ j f0() {
        return h82.b(this);
    }

    public int hashCode() {
        int i = (527 + this.a) * 31;
        String str = this.f0;
        int hashCode = (i + (str != null ? str.hashCode() : 0)) * 31;
        String str2 = this.g0;
        int hashCode2 = (hashCode + (str2 != null ? str2.hashCode() : 0)) * 31;
        String str3 = this.h0;
        return ((((hashCode2 + (str3 != null ? str3.hashCode() : 0)) * 31) + (this.i0 ? 1 : 0)) * 31) + this.j0;
    }

    @Override // androidx.media3.common.Metadata.Entry
    public void r0(n.b bVar) {
        String str = this.g0;
        if (str != null) {
            bVar.g0(str);
        }
        String str2 = this.f0;
        if (str2 != null) {
            bVar.X(str2);
        }
    }

    public String toString() {
        return "IcyHeaders: name=\"" + this.g0 + "\", genre=\"" + this.f0 + "\", bitrate=" + this.a + ", metadataInterval=" + this.j0;
    }

    @Override // android.os.Parcelable
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeInt(this.a);
        parcel.writeString(this.f0);
        parcel.writeString(this.g0);
        parcel.writeString(this.h0);
        b.V0(parcel, this.i0);
        parcel.writeInt(this.j0);
    }

    @Override // androidx.media3.common.Metadata.Entry
    public /* synthetic */ byte[] y1() {
        return h82.a(this);
    }

    public IcyHeaders(Parcel parcel) {
        this.a = parcel.readInt();
        this.f0 = parcel.readString();
        this.g0 = parcel.readString();
        this.h0 = parcel.readString();
        this.i0 = b.H0(parcel);
        this.j0 = parcel.readInt();
    }
}
