package androidx.media3.extractor.metadata.id3;

import androidx.media3.common.Metadata;
import androidx.media3.common.j;
import androidx.media3.common.n;

/* loaded from: classes.dex */
public abstract class Id3Frame implements Metadata.Entry {
    public final String a;

    public Id3Frame(String str) {
        this.a = str;
    }

    @Override // android.os.Parcelable
    public int describeContents() {
        return 0;
    }

    @Override // androidx.media3.common.Metadata.Entry
    public /* synthetic */ j f0() {
        return h82.b(this);
    }

    @Override // androidx.media3.common.Metadata.Entry
    public /* synthetic */ void r0(n.b bVar) {
        h82.c(this, bVar);
    }

    public String toString() {
        return this.a;
    }

    @Override // androidx.media3.common.Metadata.Entry
    public /* synthetic */ byte[] y1() {
        return h82.a(this);
    }
}
