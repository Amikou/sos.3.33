package androidx.media3.extractor.metadata.id3;

import android.os.Parcel;
import android.os.Parcelable;
import androidx.media3.common.n;
import androidx.media3.common.util.b;
import java.util.Arrays;

/* loaded from: classes.dex */
public final class ApicFrame extends Id3Frame {
    public static final Parcelable.Creator<ApicFrame> CREATOR = new a();
    public final String f0;
    public final String g0;
    public final int h0;
    public final byte[] i0;

    /* loaded from: classes.dex */
    public class a implements Parcelable.Creator<ApicFrame> {
        @Override // android.os.Parcelable.Creator
        /* renamed from: a */
        public ApicFrame createFromParcel(Parcel parcel) {
            return new ApicFrame(parcel);
        }

        @Override // android.os.Parcelable.Creator
        /* renamed from: b */
        public ApicFrame[] newArray(int i) {
            return new ApicFrame[i];
        }
    }

    public ApicFrame(String str, String str2, int i, byte[] bArr) {
        super("APIC");
        this.f0 = str;
        this.g0 = str2;
        this.h0 = i;
        this.i0 = bArr;
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null || ApicFrame.class != obj.getClass()) {
            return false;
        }
        ApicFrame apicFrame = (ApicFrame) obj;
        return this.h0 == apicFrame.h0 && b.c(this.f0, apicFrame.f0) && b.c(this.g0, apicFrame.g0) && Arrays.equals(this.i0, apicFrame.i0);
    }

    public int hashCode() {
        int i = (527 + this.h0) * 31;
        String str = this.f0;
        int hashCode = (i + (str != null ? str.hashCode() : 0)) * 31;
        String str2 = this.g0;
        return ((hashCode + (str2 != null ? str2.hashCode() : 0)) * 31) + Arrays.hashCode(this.i0);
    }

    @Override // androidx.media3.extractor.metadata.id3.Id3Frame, androidx.media3.common.Metadata.Entry
    public void r0(n.b bVar) {
        bVar.G(this.i0, this.h0);
    }

    @Override // androidx.media3.extractor.metadata.id3.Id3Frame
    public String toString() {
        return this.a + ": mimeType=" + this.f0 + ", description=" + this.g0;
    }

    @Override // android.os.Parcelable
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeString(this.f0);
        parcel.writeString(this.g0);
        parcel.writeInt(this.h0);
        parcel.writeByteArray(this.i0);
    }

    public ApicFrame(Parcel parcel) {
        super("APIC");
        this.f0 = (String) b.j(parcel.readString());
        this.g0 = parcel.readString();
        this.h0 = parcel.readInt();
        this.i0 = (byte[]) b.j(parcel.createByteArray());
    }
}
