package androidx.media3.extractor.metadata.id3;

import android.os.Parcel;
import android.os.Parcelable;
import androidx.media3.common.util.b;
import java.util.Arrays;

/* loaded from: classes.dex */
public final class MlltFrame extends Id3Frame {
    public static final Parcelable.Creator<MlltFrame> CREATOR = new a();
    public final int f0;
    public final int g0;
    public final int h0;
    public final int[] i0;
    public final int[] j0;

    /* loaded from: classes.dex */
    public class a implements Parcelable.Creator<MlltFrame> {
        @Override // android.os.Parcelable.Creator
        /* renamed from: a */
        public MlltFrame createFromParcel(Parcel parcel) {
            return new MlltFrame(parcel);
        }

        @Override // android.os.Parcelable.Creator
        /* renamed from: b */
        public MlltFrame[] newArray(int i) {
            return new MlltFrame[i];
        }
    }

    public MlltFrame(int i, int i2, int i3, int[] iArr, int[] iArr2) {
        super("MLLT");
        this.f0 = i;
        this.g0 = i2;
        this.h0 = i3;
        this.i0 = iArr;
        this.j0 = iArr2;
    }

    @Override // androidx.media3.extractor.metadata.id3.Id3Frame, android.os.Parcelable
    public int describeContents() {
        return 0;
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null || MlltFrame.class != obj.getClass()) {
            return false;
        }
        MlltFrame mlltFrame = (MlltFrame) obj;
        return this.f0 == mlltFrame.f0 && this.g0 == mlltFrame.g0 && this.h0 == mlltFrame.h0 && Arrays.equals(this.i0, mlltFrame.i0) && Arrays.equals(this.j0, mlltFrame.j0);
    }

    public int hashCode() {
        return ((((((((527 + this.f0) * 31) + this.g0) * 31) + this.h0) * 31) + Arrays.hashCode(this.i0)) * 31) + Arrays.hashCode(this.j0);
    }

    @Override // android.os.Parcelable
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeInt(this.f0);
        parcel.writeInt(this.g0);
        parcel.writeInt(this.h0);
        parcel.writeIntArray(this.i0);
        parcel.writeIntArray(this.j0);
    }

    public MlltFrame(Parcel parcel) {
        super("MLLT");
        this.f0 = parcel.readInt();
        this.g0 = parcel.readInt();
        this.h0 = parcel.readInt();
        this.i0 = (int[]) b.j(parcel.createIntArray());
        this.j0 = (int[]) b.j(parcel.createIntArray());
    }
}
