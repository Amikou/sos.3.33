package androidx.media3.extractor.metadata.id3;

import android.os.Parcel;
import android.os.Parcelable;
import androidx.media3.common.util.b;
import java.util.Arrays;

/* loaded from: classes.dex */
public final class PrivFrame extends Id3Frame {
    public static final Parcelable.Creator<PrivFrame> CREATOR = new a();
    public final String f0;
    public final byte[] g0;

    /* loaded from: classes.dex */
    public class a implements Parcelable.Creator<PrivFrame> {
        @Override // android.os.Parcelable.Creator
        /* renamed from: a */
        public PrivFrame createFromParcel(Parcel parcel) {
            return new PrivFrame(parcel);
        }

        @Override // android.os.Parcelable.Creator
        /* renamed from: b */
        public PrivFrame[] newArray(int i) {
            return new PrivFrame[i];
        }
    }

    public PrivFrame(String str, byte[] bArr) {
        super("PRIV");
        this.f0 = str;
        this.g0 = bArr;
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null || PrivFrame.class != obj.getClass()) {
            return false;
        }
        PrivFrame privFrame = (PrivFrame) obj;
        return b.c(this.f0, privFrame.f0) && Arrays.equals(this.g0, privFrame.g0);
    }

    public int hashCode() {
        String str = this.f0;
        return ((527 + (str != null ? str.hashCode() : 0)) * 31) + Arrays.hashCode(this.g0);
    }

    @Override // androidx.media3.extractor.metadata.id3.Id3Frame
    public String toString() {
        return this.a + ": owner=" + this.f0;
    }

    @Override // android.os.Parcelable
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeString(this.f0);
        parcel.writeByteArray(this.g0);
    }

    public PrivFrame(Parcel parcel) {
        super("PRIV");
        this.f0 = (String) b.j(parcel.readString());
        this.g0 = (byte[]) b.j(parcel.createByteArray());
    }
}
