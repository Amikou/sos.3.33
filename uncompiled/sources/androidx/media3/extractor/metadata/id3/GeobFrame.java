package androidx.media3.extractor.metadata.id3;

import android.os.Parcel;
import android.os.Parcelable;
import androidx.media3.common.util.b;
import java.util.Arrays;

/* loaded from: classes.dex */
public final class GeobFrame extends Id3Frame {
    public static final Parcelable.Creator<GeobFrame> CREATOR = new a();
    public final String f0;
    public final String g0;
    public final String h0;
    public final byte[] i0;

    /* loaded from: classes.dex */
    public class a implements Parcelable.Creator<GeobFrame> {
        @Override // android.os.Parcelable.Creator
        /* renamed from: a */
        public GeobFrame createFromParcel(Parcel parcel) {
            return new GeobFrame(parcel);
        }

        @Override // android.os.Parcelable.Creator
        /* renamed from: b */
        public GeobFrame[] newArray(int i) {
            return new GeobFrame[i];
        }
    }

    public GeobFrame(String str, String str2, String str3, byte[] bArr) {
        super("GEOB");
        this.f0 = str;
        this.g0 = str2;
        this.h0 = str3;
        this.i0 = bArr;
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null || GeobFrame.class != obj.getClass()) {
            return false;
        }
        GeobFrame geobFrame = (GeobFrame) obj;
        return b.c(this.f0, geobFrame.f0) && b.c(this.g0, geobFrame.g0) && b.c(this.h0, geobFrame.h0) && Arrays.equals(this.i0, geobFrame.i0);
    }

    public int hashCode() {
        String str = this.f0;
        int hashCode = (527 + (str != null ? str.hashCode() : 0)) * 31;
        String str2 = this.g0;
        int hashCode2 = (hashCode + (str2 != null ? str2.hashCode() : 0)) * 31;
        String str3 = this.h0;
        return ((hashCode2 + (str3 != null ? str3.hashCode() : 0)) * 31) + Arrays.hashCode(this.i0);
    }

    @Override // androidx.media3.extractor.metadata.id3.Id3Frame
    public String toString() {
        return this.a + ": mimeType=" + this.f0 + ", filename=" + this.g0 + ", description=" + this.h0;
    }

    @Override // android.os.Parcelable
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeString(this.f0);
        parcel.writeString(this.g0);
        parcel.writeString(this.h0);
        parcel.writeByteArray(this.i0);
    }

    public GeobFrame(Parcel parcel) {
        super("GEOB");
        this.f0 = (String) b.j(parcel.readString());
        this.g0 = (String) b.j(parcel.readString());
        this.h0 = (String) b.j(parcel.readString());
        this.i0 = (byte[]) b.j(parcel.createByteArray());
    }
}
