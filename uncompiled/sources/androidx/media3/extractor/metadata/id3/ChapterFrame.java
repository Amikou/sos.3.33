package androidx.media3.extractor.metadata.id3;

import android.os.Parcel;
import android.os.Parcelable;
import androidx.media3.common.util.b;
import java.util.Arrays;

/* loaded from: classes.dex */
public final class ChapterFrame extends Id3Frame {
    public static final Parcelable.Creator<ChapterFrame> CREATOR = new a();
    public final String f0;
    public final int g0;
    public final int h0;
    public final long i0;
    public final long j0;
    public final Id3Frame[] k0;

    /* loaded from: classes.dex */
    public class a implements Parcelable.Creator<ChapterFrame> {
        @Override // android.os.Parcelable.Creator
        /* renamed from: a */
        public ChapterFrame createFromParcel(Parcel parcel) {
            return new ChapterFrame(parcel);
        }

        @Override // android.os.Parcelable.Creator
        /* renamed from: b */
        public ChapterFrame[] newArray(int i) {
            return new ChapterFrame[i];
        }
    }

    public ChapterFrame(String str, int i, int i2, long j, long j2, Id3Frame[] id3FrameArr) {
        super("CHAP");
        this.f0 = str;
        this.g0 = i;
        this.h0 = i2;
        this.i0 = j;
        this.j0 = j2;
        this.k0 = id3FrameArr;
    }

    @Override // androidx.media3.extractor.metadata.id3.Id3Frame, android.os.Parcelable
    public int describeContents() {
        return 0;
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null || ChapterFrame.class != obj.getClass()) {
            return false;
        }
        ChapterFrame chapterFrame = (ChapterFrame) obj;
        return this.g0 == chapterFrame.g0 && this.h0 == chapterFrame.h0 && this.i0 == chapterFrame.i0 && this.j0 == chapterFrame.j0 && b.c(this.f0, chapterFrame.f0) && Arrays.equals(this.k0, chapterFrame.k0);
    }

    public int hashCode() {
        int i = (((((((527 + this.g0) * 31) + this.h0) * 31) + ((int) this.i0)) * 31) + ((int) this.j0)) * 31;
        String str = this.f0;
        return i + (str != null ? str.hashCode() : 0);
    }

    @Override // android.os.Parcelable
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeString(this.f0);
        parcel.writeInt(this.g0);
        parcel.writeInt(this.h0);
        parcel.writeLong(this.i0);
        parcel.writeLong(this.j0);
        parcel.writeInt(this.k0.length);
        for (Id3Frame id3Frame : this.k0) {
            parcel.writeParcelable(id3Frame, 0);
        }
    }

    public ChapterFrame(Parcel parcel) {
        super("CHAP");
        this.f0 = (String) b.j(parcel.readString());
        this.g0 = parcel.readInt();
        this.h0 = parcel.readInt();
        this.i0 = parcel.readLong();
        this.j0 = parcel.readLong();
        int readInt = parcel.readInt();
        this.k0 = new Id3Frame[readInt];
        for (int i = 0; i < readInt; i++) {
            this.k0[i] = (Id3Frame) parcel.readParcelable(Id3Frame.class.getClassLoader());
        }
    }
}
