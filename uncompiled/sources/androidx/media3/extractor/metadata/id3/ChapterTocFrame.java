package androidx.media3.extractor.metadata.id3;

import android.os.Parcel;
import android.os.Parcelable;
import androidx.media3.common.util.b;
import java.util.Arrays;

/* loaded from: classes.dex */
public final class ChapterTocFrame extends Id3Frame {
    public static final Parcelable.Creator<ChapterTocFrame> CREATOR = new a();
    public final String f0;
    public final boolean g0;
    public final boolean h0;
    public final String[] i0;
    public final Id3Frame[] j0;

    /* loaded from: classes.dex */
    public class a implements Parcelable.Creator<ChapterTocFrame> {
        @Override // android.os.Parcelable.Creator
        /* renamed from: a */
        public ChapterTocFrame createFromParcel(Parcel parcel) {
            return new ChapterTocFrame(parcel);
        }

        @Override // android.os.Parcelable.Creator
        /* renamed from: b */
        public ChapterTocFrame[] newArray(int i) {
            return new ChapterTocFrame[i];
        }
    }

    public ChapterTocFrame(String str, boolean z, boolean z2, String[] strArr, Id3Frame[] id3FrameArr) {
        super("CTOC");
        this.f0 = str;
        this.g0 = z;
        this.h0 = z2;
        this.i0 = strArr;
        this.j0 = id3FrameArr;
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null || ChapterTocFrame.class != obj.getClass()) {
            return false;
        }
        ChapterTocFrame chapterTocFrame = (ChapterTocFrame) obj;
        return this.g0 == chapterTocFrame.g0 && this.h0 == chapterTocFrame.h0 && b.c(this.f0, chapterTocFrame.f0) && Arrays.equals(this.i0, chapterTocFrame.i0) && Arrays.equals(this.j0, chapterTocFrame.j0);
    }

    public int hashCode() {
        int i = (((527 + (this.g0 ? 1 : 0)) * 31) + (this.h0 ? 1 : 0)) * 31;
        String str = this.f0;
        return i + (str != null ? str.hashCode() : 0);
    }

    @Override // android.os.Parcelable
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeString(this.f0);
        parcel.writeByte(this.g0 ? (byte) 1 : (byte) 0);
        parcel.writeByte(this.h0 ? (byte) 1 : (byte) 0);
        parcel.writeStringArray(this.i0);
        parcel.writeInt(this.j0.length);
        for (Id3Frame id3Frame : this.j0) {
            parcel.writeParcelable(id3Frame, 0);
        }
    }

    public ChapterTocFrame(Parcel parcel) {
        super("CTOC");
        this.f0 = (String) b.j(parcel.readString());
        this.g0 = parcel.readByte() != 0;
        this.h0 = parcel.readByte() != 0;
        this.i0 = (String[]) b.j(parcel.createStringArray());
        int readInt = parcel.readInt();
        this.j0 = new Id3Frame[readInt];
        for (int i = 0; i < readInt; i++) {
            this.j0[i] = (Id3Frame) parcel.readParcelable(Id3Frame.class.getClassLoader());
        }
    }
}
