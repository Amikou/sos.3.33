package androidx.media3.extractor.metadata.id3;

import androidx.media3.common.Metadata;
import com.google.zxing.qrcode.encoder.Encoder;
import java.io.UnsupportedEncodingException;
import java.nio.ByteBuffer;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Locale;

/* compiled from: Id3Decoder.java */
/* loaded from: classes.dex */
public final class a extends kp3 {
    public static final InterfaceC0044a b = hn1.a;
    public final InterfaceC0044a a;

    /* compiled from: Id3Decoder.java */
    /* renamed from: androidx.media3.extractor.metadata.id3.a$a  reason: collision with other inner class name */
    /* loaded from: classes.dex */
    public interface InterfaceC0044a {
        boolean a(int i, int i2, int i3, int i4, int i5);
    }

    /* compiled from: Id3Decoder.java */
    /* loaded from: classes.dex */
    public static final class b {
        public final int a;
        public final boolean b;
        public final int c;

        public b(int i, boolean z, int i2) {
            this.a = i;
            this.b = z;
            this.c = i2;
        }
    }

    public a() {
        this(null);
    }

    public static int A(op2 op2Var, int i) {
        byte[] d = op2Var.d();
        int e = op2Var.e();
        int i2 = e;
        while (true) {
            int i3 = i2 + 1;
            if (i3 >= e + i) {
                return i;
            }
            if ((d[i2] & 255) == 255 && d[i3] == 0) {
                System.arraycopy(d, i2 + 2, d, i3, (i - (i2 - e)) - 2);
                i--;
            }
            i2 = i3;
        }
    }

    /* JADX WARN: Code restructure failed: missing block: B:31:0x0076, code lost:
        if ((r10 & 1) != 0) goto L33;
     */
    /* JADX WARN: Code restructure failed: missing block: B:33:0x0079, code lost:
        r4 = false;
     */
    /* JADX WARN: Code restructure failed: missing block: B:40:0x0086, code lost:
        if ((r10 & 128) != 0) goto L33;
     */
    /*
        Code decompiled incorrectly, please refer to instructions dump.
        To view partially-correct code enable 'Show inconsistent code' option in preferences
    */
    public static boolean B(defpackage.op2 r18, int r19, int r20, boolean r21) {
        /*
            r1 = r18
            r0 = r19
            int r2 = r18.e()
        L8:
            int r3 = r18.a()     // Catch: java.lang.Throwable -> Laf
            r4 = 1
            r5 = r20
            if (r3 < r5) goto Lab
            r3 = 3
            r6 = 0
            if (r0 < r3) goto L22
            int r7 = r18.n()     // Catch: java.lang.Throwable -> Laf
            long r8 = r18.F()     // Catch: java.lang.Throwable -> Laf
            int r10 = r18.J()     // Catch: java.lang.Throwable -> Laf
            goto L2c
        L22:
            int r7 = r18.G()     // Catch: java.lang.Throwable -> Laf
            int r8 = r18.G()     // Catch: java.lang.Throwable -> Laf
            long r8 = (long) r8
            r10 = r6
        L2c:
            r11 = 0
            if (r7 != 0) goto L3a
            int r7 = (r8 > r11 ? 1 : (r8 == r11 ? 0 : -1))
            if (r7 != 0) goto L3a
            if (r10 != 0) goto L3a
            r1.P(r2)
            return r4
        L3a:
            r7 = 4
            if (r0 != r7) goto L6b
            if (r21 != 0) goto L6b
            r13 = 8421504(0x808080, double:4.160776E-317)
            long r13 = r13 & r8
            int r11 = (r13 > r11 ? 1 : (r13 == r11 ? 0 : -1))
            if (r11 == 0) goto L4b
            r1.P(r2)
            return r6
        L4b:
            r11 = 255(0xff, double:1.26E-321)
            long r13 = r8 & r11
            r15 = 8
            long r15 = r8 >> r15
            long r15 = r15 & r11
            r17 = 7
            long r15 = r15 << r17
            long r13 = r13 | r15
            r15 = 16
            long r15 = r8 >> r15
            long r15 = r15 & r11
            r17 = 14
            long r15 = r15 << r17
            long r13 = r13 | r15
            r15 = 24
            long r8 = r8 >> r15
            long r8 = r8 & r11
            r11 = 21
            long r8 = r8 << r11
            long r8 = r8 | r13
        L6b:
            if (r0 != r7) goto L7b
            r3 = r10 & 64
            if (r3 == 0) goto L73
            r3 = r4
            goto L74
        L73:
            r3 = r6
        L74:
            r7 = r10 & 1
            if (r7 == 0) goto L79
            goto L8b
        L79:
            r4 = r6
            goto L8b
        L7b:
            if (r0 != r3) goto L89
            r3 = r10 & 32
            if (r3 == 0) goto L83
            r3 = r4
            goto L84
        L83:
            r3 = r6
        L84:
            r7 = r10 & 128(0x80, float:1.8E-43)
            if (r7 == 0) goto L79
            goto L8b
        L89:
            r3 = r6
            r4 = r3
        L8b:
            if (r4 == 0) goto L8f
            int r3 = r3 + 4
        L8f:
            long r3 = (long) r3
            int r3 = (r8 > r3 ? 1 : (r8 == r3 ? 0 : -1))
            if (r3 >= 0) goto L98
            r1.P(r2)
            return r6
        L98:
            int r3 = r18.a()     // Catch: java.lang.Throwable -> Laf
            long r3 = (long) r3
            int r3 = (r3 > r8 ? 1 : (r3 == r8 ? 0 : -1))
            if (r3 >= 0) goto La5
            r1.P(r2)
            return r6
        La5:
            int r3 = (int) r8
            r1.Q(r3)     // Catch: java.lang.Throwable -> Laf
            goto L8
        Lab:
            r1.P(r2)
            return r4
        Laf:
            r0 = move-exception
            r1.P(r2)
            throw r0
        */
        throw new UnsupportedOperationException("Method not decompiled: androidx.media3.extractor.metadata.id3.a.B(op2, int, int, boolean):boolean");
    }

    public static byte[] d(byte[] bArr, int i, int i2) {
        if (i2 <= i) {
            return androidx.media3.common.util.b.f;
        }
        return Arrays.copyOfRange(bArr, i, i2);
    }

    public static ApicFrame f(op2 op2Var, int i, int i2) throws UnsupportedEncodingException {
        int y;
        String str;
        int D = op2Var.D();
        String v = v(D);
        int i3 = i - 1;
        byte[] bArr = new byte[i3];
        op2Var.j(bArr, 0, i3);
        if (i2 == 2) {
            str = "image/" + ei.e(new String(bArr, 0, 3, Encoder.DEFAULT_BYTE_MODE_ENCODING));
            if ("image/jpg".equals(str)) {
                str = "image/jpeg";
            }
            y = 2;
        } else {
            y = y(bArr, 0);
            String e = ei.e(new String(bArr, 0, y, Encoder.DEFAULT_BYTE_MODE_ENCODING));
            if (e.indexOf(47) == -1) {
                str = "image/" + e;
            } else {
                str = e;
            }
        }
        int i4 = y + 2;
        int x = x(bArr, i4, D);
        return new ApicFrame(str, new String(bArr, i4, x - i4, v), bArr[y + 1] & 255, d(bArr, x + u(D), i3));
    }

    public static BinaryFrame g(op2 op2Var, int i, String str) {
        byte[] bArr = new byte[i];
        op2Var.j(bArr, 0, i);
        return new BinaryFrame(str, bArr);
    }

    public static ChapterFrame h(op2 op2Var, int i, int i2, boolean z, int i3, InterfaceC0044a interfaceC0044a) throws UnsupportedEncodingException {
        int e = op2Var.e();
        int y = y(op2Var.d(), e);
        String str = new String(op2Var.d(), e, y - e, Encoder.DEFAULT_BYTE_MODE_ENCODING);
        op2Var.P(y + 1);
        int n = op2Var.n();
        int n2 = op2Var.n();
        long F = op2Var.F();
        long j = F == 4294967295L ? -1L : F;
        long F2 = op2Var.F();
        long j2 = F2 == 4294967295L ? -1L : F2;
        ArrayList arrayList = new ArrayList();
        int i4 = e + i;
        while (op2Var.e() < i4) {
            Id3Frame k = k(i2, op2Var, z, i3, interfaceC0044a);
            if (k != null) {
                arrayList.add(k);
            }
        }
        return new ChapterFrame(str, n, n2, j, j2, (Id3Frame[]) arrayList.toArray(new Id3Frame[0]));
    }

    public static ChapterTocFrame i(op2 op2Var, int i, int i2, boolean z, int i3, InterfaceC0044a interfaceC0044a) throws UnsupportedEncodingException {
        int e = op2Var.e();
        int y = y(op2Var.d(), e);
        String str = new String(op2Var.d(), e, y - e, Encoder.DEFAULT_BYTE_MODE_ENCODING);
        op2Var.P(y + 1);
        int D = op2Var.D();
        boolean z2 = (D & 2) != 0;
        boolean z3 = (D & 1) != 0;
        int D2 = op2Var.D();
        String[] strArr = new String[D2];
        for (int i4 = 0; i4 < D2; i4++) {
            int e2 = op2Var.e();
            int y2 = y(op2Var.d(), e2);
            strArr[i4] = new String(op2Var.d(), e2, y2 - e2, Encoder.DEFAULT_BYTE_MODE_ENCODING);
            op2Var.P(y2 + 1);
        }
        ArrayList arrayList = new ArrayList();
        int i5 = e + i;
        while (op2Var.e() < i5) {
            Id3Frame k = k(i2, op2Var, z, i3, interfaceC0044a);
            if (k != null) {
                arrayList.add(k);
            }
        }
        return new ChapterTocFrame(str, z2, z3, strArr, (Id3Frame[]) arrayList.toArray(new Id3Frame[0]));
    }

    public static CommentFrame j(op2 op2Var, int i) throws UnsupportedEncodingException {
        if (i < 4) {
            return null;
        }
        int D = op2Var.D();
        String v = v(D);
        byte[] bArr = new byte[3];
        op2Var.j(bArr, 0, 3);
        String str = new String(bArr, 0, 3);
        int i2 = i - 4;
        byte[] bArr2 = new byte[i2];
        op2Var.j(bArr2, 0, i2);
        int x = x(bArr2, 0, D);
        String str2 = new String(bArr2, 0, x, v);
        int u = x + u(D);
        return new CommentFrame(str, str2, p(bArr2, u, x(bArr2, u, D), v));
    }

    /* JADX WARN: Code restructure failed: missing block: B:131:0x0190, code lost:
        if (r13 == 67) goto L98;
     */
    /*
        Code decompiled incorrectly, please refer to instructions dump.
        To view partially-correct code enable 'Show inconsistent code' option in preferences
    */
    public static androidx.media3.extractor.metadata.id3.Id3Frame k(int r19, defpackage.op2 r20, boolean r21, int r22, androidx.media3.extractor.metadata.id3.a.InterfaceC0044a r23) {
        /*
            Method dump skipped, instructions count: 558
            To view this dump change 'Code comments level' option to 'DEBUG'
        */
        throw new UnsupportedOperationException("Method not decompiled: androidx.media3.extractor.metadata.id3.a.k(int, op2, boolean, int, androidx.media3.extractor.metadata.id3.a$a):androidx.media3.extractor.metadata.id3.Id3Frame");
    }

    public static GeobFrame l(op2 op2Var, int i) throws UnsupportedEncodingException {
        int D = op2Var.D();
        String v = v(D);
        int i2 = i - 1;
        byte[] bArr = new byte[i2];
        op2Var.j(bArr, 0, i2);
        int y = y(bArr, 0);
        String str = new String(bArr, 0, y, Encoder.DEFAULT_BYTE_MODE_ENCODING);
        int i3 = y + 1;
        int x = x(bArr, i3, D);
        String p = p(bArr, i3, x, v);
        int u = x + u(D);
        int x2 = x(bArr, u, D);
        return new GeobFrame(str, p, p(bArr, u, x2, v), d(bArr, x2 + u(D), i2));
    }

    public static b m(op2 op2Var) {
        int G;
        if (op2Var.a() < 10) {
            p12.i("Id3Decoder", "Data too short to be an ID3 tag");
            return null;
        }
        boolean z = false;
        if (op2Var.G() != 4801587) {
            p12.i("Id3Decoder", "Unexpected first three bytes of ID3 tag header: 0x" + String.format("%06X", Integer.valueOf(G)));
            return null;
        }
        int D = op2Var.D();
        op2Var.Q(1);
        int D2 = op2Var.D();
        int C = op2Var.C();
        if (D == 2) {
            if ((D2 & 64) != 0) {
                p12.i("Id3Decoder", "Skipped ID3 tag with majorVersion=2 and undefined compression scheme");
                return null;
            }
        } else if (D == 3) {
            if ((D2 & 64) != 0) {
                int n = op2Var.n();
                op2Var.Q(n);
                C -= n + 4;
            }
        } else if (D != 4) {
            p12.i("Id3Decoder", "Skipped ID3 tag with unsupported majorVersion=" + D);
            return null;
        } else {
            if ((D2 & 64) != 0) {
                int C2 = op2Var.C();
                op2Var.Q(C2 - 4);
                C -= C2;
            }
            if ((D2 & 16) != 0) {
                C -= 10;
            }
        }
        if (D < 4 && (D2 & 128) != 0) {
            z = true;
        }
        return new b(D, z, C);
    }

    public static MlltFrame n(op2 op2Var, int i) {
        int J = op2Var.J();
        int G = op2Var.G();
        int G2 = op2Var.G();
        int D = op2Var.D();
        int D2 = op2Var.D();
        np2 np2Var = new np2();
        np2Var.m(op2Var);
        int i2 = ((i - 10) * 8) / (D + D2);
        int[] iArr = new int[i2];
        int[] iArr2 = new int[i2];
        for (int i3 = 0; i3 < i2; i3++) {
            int h = np2Var.h(D);
            int h2 = np2Var.h(D2);
            iArr[i3] = h;
            iArr2[i3] = h2;
        }
        return new MlltFrame(J, G, G2, iArr, iArr2);
    }

    public static PrivFrame o(op2 op2Var, int i) throws UnsupportedEncodingException {
        byte[] bArr = new byte[i];
        op2Var.j(bArr, 0, i);
        int y = y(bArr, 0);
        return new PrivFrame(new String(bArr, 0, y, Encoder.DEFAULT_BYTE_MODE_ENCODING), d(bArr, y + 1, i));
    }

    public static String p(byte[] bArr, int i, int i2, String str) throws UnsupportedEncodingException {
        return (i2 <= i || i2 > bArr.length) ? "" : new String(bArr, i, i2 - i, str);
    }

    public static TextInformationFrame q(op2 op2Var, int i, String str) throws UnsupportedEncodingException {
        if (i < 1) {
            return null;
        }
        int D = op2Var.D();
        String v = v(D);
        int i2 = i - 1;
        byte[] bArr = new byte[i2];
        op2Var.j(bArr, 0, i2);
        return new TextInformationFrame(str, null, new String(bArr, 0, x(bArr, 0, D), v));
    }

    public static TextInformationFrame r(op2 op2Var, int i) throws UnsupportedEncodingException {
        if (i < 1) {
            return null;
        }
        int D = op2Var.D();
        String v = v(D);
        int i2 = i - 1;
        byte[] bArr = new byte[i2];
        op2Var.j(bArr, 0, i2);
        int x = x(bArr, 0, D);
        String str = new String(bArr, 0, x, v);
        int u = x + u(D);
        return new TextInformationFrame("TXXX", str, p(bArr, u, x(bArr, u, D), v));
    }

    public static UrlLinkFrame s(op2 op2Var, int i, String str) throws UnsupportedEncodingException {
        byte[] bArr = new byte[i];
        op2Var.j(bArr, 0, i);
        return new UrlLinkFrame(str, null, new String(bArr, 0, y(bArr, 0), Encoder.DEFAULT_BYTE_MODE_ENCODING));
    }

    public static UrlLinkFrame t(op2 op2Var, int i) throws UnsupportedEncodingException {
        if (i < 1) {
            return null;
        }
        int D = op2Var.D();
        String v = v(D);
        int i2 = i - 1;
        byte[] bArr = new byte[i2];
        op2Var.j(bArr, 0, i2);
        int x = x(bArr, 0, D);
        String str = new String(bArr, 0, x, v);
        int u = x + u(D);
        return new UrlLinkFrame("WXXX", str, p(bArr, u, y(bArr, u), Encoder.DEFAULT_BYTE_MODE_ENCODING));
    }

    public static int u(int i) {
        return (i == 0 || i == 3) ? 1 : 2;
    }

    public static String v(int i) {
        return i != 1 ? i != 2 ? i != 3 ? Encoder.DEFAULT_BYTE_MODE_ENCODING : "UTF-8" : "UTF-16BE" : "UTF-16";
    }

    public static String w(int i, int i2, int i3, int i4, int i5) {
        return i == 2 ? String.format(Locale.US, "%c%c%c", Integer.valueOf(i2), Integer.valueOf(i3), Integer.valueOf(i4)) : String.format(Locale.US, "%c%c%c%c", Integer.valueOf(i2), Integer.valueOf(i3), Integer.valueOf(i4), Integer.valueOf(i5));
    }

    public static int x(byte[] bArr, int i, int i2) {
        int y = y(bArr, i);
        if (i2 == 0 || i2 == 3) {
            return y;
        }
        while (y < bArr.length - 1) {
            if ((y - i) % 2 == 0 && bArr[y + 1] == 0) {
                return y;
            }
            y = y(bArr, y + 1);
        }
        return bArr.length;
    }

    public static int y(byte[] bArr, int i) {
        while (i < bArr.length) {
            if (bArr[i] == 0) {
                return i;
            }
            i++;
        }
        return bArr.length;
    }

    public static /* synthetic */ boolean z(int i, int i2, int i3, int i4, int i5) {
        return false;
    }

    @Override // defpackage.kp3
    public Metadata b(n82 n82Var, ByteBuffer byteBuffer) {
        return e(byteBuffer.array(), byteBuffer.limit());
    }

    public Metadata e(byte[] bArr, int i) {
        ArrayList arrayList = new ArrayList();
        op2 op2Var = new op2(bArr, i);
        b m = m(op2Var);
        if (m == null) {
            return null;
        }
        int e = op2Var.e();
        int i2 = m.a == 2 ? 6 : 10;
        int i3 = m.c;
        if (m.b) {
            i3 = A(op2Var, m.c);
        }
        op2Var.O(e + i3);
        boolean z = false;
        if (!B(op2Var, m.a, i2, false)) {
            if (m.a != 4 || !B(op2Var, 4, i2, true)) {
                p12.i("Id3Decoder", "Failed to validate ID3 tag with majorVersion=" + m.a);
                return null;
            }
            z = true;
        }
        while (op2Var.a() >= i2) {
            Id3Frame k = k(m.a, op2Var, z, i2, this.a);
            if (k != null) {
                arrayList.add(k);
            }
        }
        return new Metadata(arrayList);
    }

    public a(InterfaceC0044a interfaceC0044a) {
        this.a = interfaceC0044a;
    }
}
