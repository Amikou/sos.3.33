package androidx.media3.extractor.text;

import androidx.recyclerview.widget.RecyclerView;
import java.nio.ByteBuffer;

/* compiled from: SimpleSubtitleDecoder.java */
/* loaded from: classes.dex */
public abstract class a extends androidx.media3.decoder.b<tv3, uv3, SubtitleDecoderException> implements rv3 {

    /* compiled from: SimpleSubtitleDecoder.java */
    /* renamed from: androidx.media3.extractor.text.a$a  reason: collision with other inner class name */
    /* loaded from: classes.dex */
    public class C0045a extends uv3 {
        public C0045a() {
        }

        @Override // defpackage.if0
        public void u() {
            a.this.s(this);
        }
    }

    public a(String str) {
        super(new tv3[2], new uv3[2]);
        v(RecyclerView.a0.FLAG_ADAPTER_FULLUPDATE);
    }

    public abstract qv3 A(byte[] bArr, int i, boolean z) throws SubtitleDecoderException;

    @Override // androidx.media3.decoder.b
    /* renamed from: B */
    public final SubtitleDecoderException k(tv3 tv3Var, uv3 uv3Var, boolean z) {
        try {
            ByteBuffer byteBuffer = (ByteBuffer) ii.e(tv3Var.g0);
            uv3Var.v(tv3Var.i0, A(byteBuffer.array(), byteBuffer.limit(), z), tv3Var.m0);
            uv3Var.j(Integer.MIN_VALUE);
            return null;
        } catch (SubtitleDecoderException e) {
            return e;
        }
    }

    @Override // defpackage.rv3
    public void b(long j) {
    }

    @Override // androidx.media3.decoder.b
    /* renamed from: x */
    public final tv3 h() {
        return new tv3();
    }

    @Override // androidx.media3.decoder.b
    /* renamed from: y */
    public final uv3 i() {
        return new C0045a();
    }

    @Override // androidx.media3.decoder.b
    /* renamed from: z */
    public final SubtitleDecoderException j(Throwable th) {
        return new SubtitleDecoderException("Unexpected decode error", th);
    }
}
