package androidx.media3.extractor.text;

import androidx.media3.common.ParserException;
import androidx.media3.common.j;
import androidx.recyclerview.widget.RecyclerView;
import com.google.common.primitives.Ints;
import java.io.IOException;
import java.io.InterruptedIOException;
import java.util.ArrayList;
import java.util.List;
import zendesk.support.request.CellBase;

/* compiled from: SubtitleExtractor.java */
/* loaded from: classes.dex */
public class b implements p11 {
    public final rv3 a;
    public final j d;
    public r11 g;
    public f84 h;
    public int i;
    public final mb0 b = new mb0();
    public final op2 c = new op2();
    public final List<Long> e = new ArrayList();
    public final List<op2> f = new ArrayList();
    public int j = 0;
    public long k = CellBase.ID_SYSTEM_MESSAGE_REQUEST_CLOSED;

    public b(rv3 rv3Var, j jVar) {
        this.a = rv3Var;
        this.d = jVar.b().e0("text/x-exoplayer-cues").I(jVar.p0).E();
    }

    @Override // defpackage.p11
    public void a() {
        if (this.j == 5) {
            return;
        }
        this.a.a();
        this.j = 5;
    }

    public final void b() throws IOException {
        try {
            tv3 d = this.a.d();
            while (d == null) {
                Thread.sleep(5L);
                d = this.a.d();
            }
            d.v(this.i);
            d.g0.put(this.c.d(), 0, this.i);
            d.g0.limit(this.i);
            this.a.e(d);
            uv3 c = this.a.c();
            while (c == null) {
                Thread.sleep(5L);
                c = this.a.c();
            }
            for (int i = 0; i < c.f(); i++) {
                byte[] a = this.b.a(c.e(c.d(i)));
                this.e.add(Long.valueOf(c.d(i)));
                this.f.add(new op2(a));
            }
            c.u();
        } catch (SubtitleDecoderException e) {
            throw ParserException.createForMalformedContainer("SubtitleDecoder failed.", e);
        } catch (InterruptedException unused) {
            Thread.currentThread().interrupt();
            throw new InterruptedIOException();
        }
    }

    @Override // defpackage.p11
    public void c(long j, long j2) {
        int i = this.j;
        ii.g((i == 0 || i == 5) ? false : true);
        this.k = j2;
        if (this.j == 2) {
            this.j = 1;
        }
        if (this.j == 4) {
            this.j = 3;
        }
    }

    public final boolean d(q11 q11Var) throws IOException {
        int b = this.c.b();
        int i = this.i;
        if (b == i) {
            this.c.c(i + RecyclerView.a0.FLAG_ADAPTER_FULLUPDATE);
        }
        int read = q11Var.read(this.c.d(), this.i, this.c.b() - this.i);
        if (read != -1) {
            this.i += read;
        }
        long length = q11Var.getLength();
        return (length != -1 && ((long) this.i) == length) || read == -1;
    }

    public final boolean e(q11 q11Var) throws IOException {
        return q11Var.g((q11Var.getLength() > (-1L) ? 1 : (q11Var.getLength() == (-1L) ? 0 : -1)) != 0 ? Ints.d(q11Var.getLength()) : RecyclerView.a0.FLAG_ADAPTER_FULLUPDATE) == -1;
    }

    @Override // defpackage.p11
    public int f(q11 q11Var, ot2 ot2Var) throws IOException {
        int i = this.j;
        ii.g((i == 0 || i == 5) ? false : true);
        if (this.j == 1) {
            this.c.L(q11Var.getLength() != -1 ? Ints.d(q11Var.getLength()) : RecyclerView.a0.FLAG_ADAPTER_FULLUPDATE);
            this.i = 0;
            this.j = 2;
        }
        if (this.j == 2 && d(q11Var)) {
            b();
            h();
            this.j = 4;
        }
        if (this.j == 3 && e(q11Var)) {
            h();
            this.j = 4;
        }
        return this.j == 4 ? -1 : 0;
    }

    @Override // defpackage.p11
    public boolean g(q11 q11Var) throws IOException {
        return true;
    }

    public final void h() {
        ii.i(this.h);
        ii.g(this.e.size() == this.f.size());
        long j = this.k;
        for (int g = j == CellBase.ID_SYSTEM_MESSAGE_REQUEST_CLOSED ? 0 : androidx.media3.common.util.b.g(this.e, Long.valueOf(j), true, true); g < this.f.size(); g++) {
            op2 op2Var = this.f.get(g);
            op2Var.P(0);
            int length = op2Var.d().length;
            this.h.a(op2Var, length);
            this.h.b(this.e.get(g).longValue(), 1, length, 0, null);
        }
    }

    @Override // defpackage.p11
    public void j(r11 r11Var) {
        ii.g(this.j == 0);
        this.g = r11Var;
        this.h = r11Var.f(0, 3);
        this.g.m();
        this.g.p(new jq1(new long[]{0}, new long[]{0}, CellBase.ID_SYSTEM_MESSAGE_REQUEST_CLOSED));
        this.h.f(this.d);
        this.j = 1;
    }
}
