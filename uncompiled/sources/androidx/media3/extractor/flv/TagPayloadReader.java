package androidx.media3.extractor.flv;

import androidx.media3.common.ParserException;

/* loaded from: classes.dex */
public abstract class TagPayloadReader {
    public final f84 a;

    /* loaded from: classes.dex */
    public static final class UnsupportedFormatException extends ParserException {
        public UnsupportedFormatException(String str) {
            super(str, null, false, 1);
        }
    }

    public TagPayloadReader(f84 f84Var) {
        this.a = f84Var;
    }

    public final boolean a(op2 op2Var, long j) throws ParserException {
        return b(op2Var) && c(op2Var, j);
    }

    public abstract boolean b(op2 op2Var) throws ParserException;

    public abstract boolean c(op2 op2Var, long j) throws ParserException;
}
