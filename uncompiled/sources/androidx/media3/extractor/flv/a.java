package androidx.media3.extractor.flv;

import androidx.media3.common.ParserException;
import androidx.media3.common.j;
import androidx.media3.extractor.flv.TagPayloadReader;
import defpackage.l4;
import java.util.Collections;

/* compiled from: AudioTagPayloadReader.java */
/* loaded from: classes.dex */
public final class a extends TagPayloadReader {
    public static final int[] e = {5512, 11025, 22050, 44100};
    public boolean b;
    public boolean c;
    public int d;

    public a(f84 f84Var) {
        super(f84Var);
    }

    @Override // androidx.media3.extractor.flv.TagPayloadReader
    public boolean b(op2 op2Var) throws TagPayloadReader.UnsupportedFormatException {
        if (!this.b) {
            int D = op2Var.D();
            int i = (D >> 4) & 15;
            this.d = i;
            if (i == 2) {
                this.a.f(new j.b().e0("audio/mpeg").H(1).f0(e[(D >> 2) & 3]).E());
                this.c = true;
            } else if (i == 7 || i == 8) {
                this.a.f(new j.b().e0(i == 7 ? "audio/g711-alaw" : "audio/g711-mlaw").H(1).f0(8000).E());
                this.c = true;
            } else if (i != 10) {
                throw new TagPayloadReader.UnsupportedFormatException("Audio format not supported: " + this.d);
            }
            this.b = true;
        } else {
            op2Var.Q(1);
        }
        return true;
    }

    @Override // androidx.media3.extractor.flv.TagPayloadReader
    public boolean c(op2 op2Var, long j) throws ParserException {
        if (this.d == 2) {
            int a = op2Var.a();
            this.a.a(op2Var, a);
            this.a.b(j, 1, a, 0, null);
            return true;
        }
        int D = op2Var.D();
        if (D == 0 && !this.c) {
            int a2 = op2Var.a();
            byte[] bArr = new byte[a2];
            op2Var.j(bArr, 0, a2);
            l4.b e2 = l4.e(bArr);
            this.a.f(new j.b().e0("audio/mp4a-latm").I(e2.c).H(e2.b).f0(e2.a).T(Collections.singletonList(bArr)).E());
            this.c = true;
            return false;
        } else if (this.d != 10 || D == 1) {
            int a3 = op2Var.a();
            this.a.a(op2Var, a3);
            this.a.b(j, 1, a3, 0, null);
            return true;
        } else {
            return false;
        }
    }
}
