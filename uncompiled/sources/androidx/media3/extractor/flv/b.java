package androidx.media3.extractor.flv;

import defpackage.wi3;
import java.io.IOException;
import zendesk.support.request.CellBase;

/* compiled from: FlvExtractor.java */
/* loaded from: classes.dex */
public final class b implements p11 {
    public r11 f;
    public boolean h;
    public long i;
    public int j;
    public int k;
    public int l;
    public long m;
    public boolean n;
    public a o;
    public c p;
    public final op2 a = new op2(4);
    public final op2 b = new op2(9);
    public final op2 c = new op2(11);
    public final op2 d = new op2();
    public final xd3 e = new xd3();
    public int g = 1;

    static {
        h81 h81Var = h81.b;
    }

    public static /* synthetic */ p11[] h() {
        return new p11[]{new b()};
    }

    @Override // defpackage.p11
    public void a() {
    }

    @Override // defpackage.p11
    public void c(long j, long j2) {
        if (j == 0) {
            this.g = 1;
            this.h = false;
        } else {
            this.g = 3;
        }
        this.j = 0;
    }

    public final void d() {
        if (this.n) {
            return;
        }
        this.f.p(new wi3.b(CellBase.ID_SYSTEM_MESSAGE_REQUEST_CLOSED));
        this.n = true;
    }

    public final long e() {
        if (this.h) {
            return this.i + this.m;
        }
        if (this.e.d() == CellBase.ID_SYSTEM_MESSAGE_REQUEST_CLOSED) {
            return 0L;
        }
        return this.m;
    }

    @Override // defpackage.p11
    public int f(q11 q11Var, ot2 ot2Var) throws IOException {
        ii.i(this.f);
        while (true) {
            int i = this.g;
            if (i != 1) {
                if (i == 2) {
                    n(q11Var);
                } else if (i != 3) {
                    if (i == 4) {
                        if (l(q11Var)) {
                            return 0;
                        }
                    } else {
                        throw new IllegalStateException();
                    }
                } else if (!m(q11Var)) {
                    return -1;
                }
            } else if (!k(q11Var)) {
                return -1;
            }
        }
    }

    @Override // defpackage.p11
    public boolean g(q11 q11Var) throws IOException {
        q11Var.n(this.a.d(), 0, 3);
        this.a.P(0);
        if (this.a.G() != 4607062) {
            return false;
        }
        q11Var.n(this.a.d(), 0, 2);
        this.a.P(0);
        if ((this.a.J() & 250) != 0) {
            return false;
        }
        q11Var.n(this.a.d(), 0, 4);
        this.a.P(0);
        int n = this.a.n();
        q11Var.j();
        q11Var.f(n);
        q11Var.n(this.a.d(), 0, 4);
        this.a.P(0);
        return this.a.n() == 0;
    }

    public final op2 i(q11 q11Var) throws IOException {
        if (this.l > this.d.b()) {
            op2 op2Var = this.d;
            op2Var.N(new byte[Math.max(op2Var.b() * 2, this.l)], 0);
        } else {
            this.d.P(0);
        }
        this.d.O(this.l);
        q11Var.readFully(this.d.d(), 0, this.l);
        return this.d;
    }

    @Override // defpackage.p11
    public void j(r11 r11Var) {
        this.f = r11Var;
    }

    public final boolean k(q11 q11Var) throws IOException {
        if (q11Var.a(this.b.d(), 0, 9, true)) {
            this.b.P(0);
            this.b.Q(4);
            int D = this.b.D();
            boolean z = (D & 4) != 0;
            boolean z2 = (D & 1) != 0;
            if (z && this.o == null) {
                this.o = new a(this.f.f(8, 1));
            }
            if (z2 && this.p == null) {
                this.p = new c(this.f.f(9, 2));
            }
            this.f.m();
            this.j = (this.b.n() - 9) + 4;
            this.g = 2;
            return true;
        }
        return false;
    }

    /* JADX WARN: Removed duplicated region for block: B:26:0x0087  */
    /* JADX WARN: Removed duplicated region for block: B:27:0x008b  */
    /*
        Code decompiled incorrectly, please refer to instructions dump.
        To view partially-correct code enable 'Show inconsistent code' option in preferences
    */
    public final boolean l(defpackage.q11 r10) throws java.io.IOException {
        /*
            r9 = this;
            long r0 = r9.e()
            int r2 = r9.k
            r3 = -9223372036854775807(0x8000000000000001, double:-4.9E-324)
            r5 = 0
            r6 = 1
            r7 = 8
            if (r2 != r7) goto L24
            androidx.media3.extractor.flv.a r7 = r9.o
            if (r7 == 0) goto L24
            r9.d()
            androidx.media3.extractor.flv.a r2 = r9.o
            op2 r10 = r9.i(r10)
            boolean r5 = r2.a(r10, r0)
        L22:
            r10 = r6
            goto L75
        L24:
            r7 = 9
            if (r2 != r7) goto L3a
            androidx.media3.extractor.flv.c r7 = r9.p
            if (r7 == 0) goto L3a
            r9.d()
            androidx.media3.extractor.flv.c r2 = r9.p
            op2 r10 = r9.i(r10)
            boolean r5 = r2.a(r10, r0)
            goto L22
        L3a:
            r7 = 18
            if (r2 != r7) goto L6f
            boolean r2 = r9.n
            if (r2 != 0) goto L6f
            xd3 r2 = r9.e
            op2 r10 = r9.i(r10)
            boolean r5 = r2.a(r10, r0)
            xd3 r10 = r9.e
            long r0 = r10.d()
            int r10 = (r0 > r3 ? 1 : (r0 == r3 ? 0 : -1))
            if (r10 == 0) goto L22
            r11 r10 = r9.f
            jq1 r2 = new jq1
            xd3 r7 = r9.e
            long[] r7 = r7.e()
            xd3 r8 = r9.e
            long[] r8 = r8.f()
            r2.<init>(r7, r8, r0)
            r10.p(r2)
            r9.n = r6
            goto L22
        L6f:
            int r0 = r9.l
            r10.k(r0)
            r10 = r5
        L75:
            boolean r0 = r9.h
            if (r0 != 0) goto L8f
            if (r5 == 0) goto L8f
            r9.h = r6
            xd3 r0 = r9.e
            long r0 = r0.d()
            int r0 = (r0 > r3 ? 1 : (r0 == r3 ? 0 : -1))
            if (r0 != 0) goto L8b
            long r0 = r9.m
            long r0 = -r0
            goto L8d
        L8b:
            r0 = 0
        L8d:
            r9.i = r0
        L8f:
            r0 = 4
            r9.j = r0
            r0 = 2
            r9.g = r0
            return r10
        */
        throw new UnsupportedOperationException("Method not decompiled: androidx.media3.extractor.flv.b.l(q11):boolean");
    }

    public final boolean m(q11 q11Var) throws IOException {
        if (q11Var.a(this.c.d(), 0, 11, true)) {
            this.c.P(0);
            this.k = this.c.D();
            this.l = this.c.G();
            this.m = this.c.G();
            this.m = ((this.c.D() << 24) | this.m) * 1000;
            this.c.Q(3);
            this.g = 4;
            return true;
        }
        return false;
    }

    public final void n(q11 q11Var) throws IOException {
        q11Var.k(this.j);
        this.j = 0;
        this.g = 3;
    }
}
