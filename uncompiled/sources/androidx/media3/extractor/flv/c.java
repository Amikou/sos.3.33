package androidx.media3.extractor.flv;

import androidx.media3.common.ParserException;
import androidx.media3.common.j;
import androidx.media3.extractor.flv.TagPayloadReader;

/* compiled from: VideoTagPayloadReader.java */
/* loaded from: classes.dex */
public final class c extends TagPayloadReader {
    public final op2 b;
    public final op2 c;
    public int d;
    public boolean e;
    public boolean f;
    public int g;

    public c(f84 f84Var) {
        super(f84Var);
        this.b = new op2(wc2.a);
        this.c = new op2(4);
    }

    @Override // androidx.media3.extractor.flv.TagPayloadReader
    public boolean b(op2 op2Var) throws TagPayloadReader.UnsupportedFormatException {
        int D = op2Var.D();
        int i = (D >> 4) & 15;
        int i2 = D & 15;
        if (i2 == 7) {
            this.g = i;
            return i != 5;
        }
        throw new TagPayloadReader.UnsupportedFormatException("Video format not supported: " + i2);
    }

    @Override // androidx.media3.extractor.flv.TagPayloadReader
    public boolean c(op2 op2Var, long j) throws ParserException {
        int D = op2Var.D();
        long o = j + (op2Var.o() * 1000);
        if (D == 0 && !this.e) {
            op2 op2Var2 = new op2(new byte[op2Var.a()]);
            op2Var.j(op2Var2.d(), 0, op2Var.a());
            rl b = rl.b(op2Var2);
            this.d = b.b;
            this.a.f(new j.b().e0("video/avc").I(b.f).j0(b.c).Q(b.d).a0(b.e).T(b.a).E());
            this.e = true;
            return false;
        } else if (D == 1 && this.e) {
            int i = this.g == 1 ? 1 : 0;
            if (this.f || i != 0) {
                byte[] d = this.c.d();
                d[0] = 0;
                d[1] = 0;
                d[2] = 0;
                int i2 = 4 - this.d;
                int i3 = 0;
                while (op2Var.a() > 0) {
                    op2Var.j(this.c.d(), i2, this.d);
                    this.c.P(0);
                    int H = this.c.H();
                    this.b.P(0);
                    this.a.a(this.b, 4);
                    this.a.a(op2Var, H);
                    i3 = i3 + 4 + H;
                }
                this.a.b(o, i, i3, 0, null);
                this.f = true;
                return true;
            }
            return false;
        } else {
            return false;
        }
    }
}
