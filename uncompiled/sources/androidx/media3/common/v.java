package androidx.media3.common;

import android.os.Bundle;
import androidx.media3.common.e;
import com.google.common.collect.ImmutableList;
import com.google.common.collect.Lists;
import java.util.ArrayList;
import java.util.Arrays;
import okhttp3.internal.http2.Http2;

/* compiled from: TrackGroup.java */
/* loaded from: classes.dex */
public final class v implements e {
    public static final e.a<v> j0 = a84.a;
    public final int a;
    public final String f0;
    public final int g0;
    public final j[] h0;
    public int i0;

    public v(j... jVarArr) {
        this("", jVarArr);
    }

    public static String e(int i) {
        return Integer.toString(i, 36);
    }

    public static /* synthetic */ v f(Bundle bundle) {
        ImmutableList b;
        ArrayList parcelableArrayList = bundle.getParcelableArrayList(e(0));
        if (parcelableArrayList == null) {
            b = ImmutableList.of();
        } else {
            b = is.b(j.L0, parcelableArrayList);
        }
        return new v(bundle.getString(e(1), ""), (j[]) b.toArray(new j[0]));
    }

    public static void g(String str, String str2, String str3, int i) {
        p12.d("TrackGroup", "", new IllegalStateException("Different " + str + " combined in one TrackGroup: '" + str2 + "' (track 0) and '" + str3 + "' (track " + i + ")"));
    }

    public static String h(String str) {
        return (str == null || str.equals("und")) ? "" : str;
    }

    public static int i(int i) {
        return i | Http2.INITIAL_MAX_FRAME_SIZE;
    }

    public v b(String str) {
        return new v(str, this.h0);
    }

    public j c(int i) {
        return this.h0[i];
    }

    public int d(j jVar) {
        int i = 0;
        while (true) {
            j[] jVarArr = this.h0;
            if (i >= jVarArr.length) {
                return -1;
            }
            if (jVar == jVarArr[i]) {
                return i;
            }
            i++;
        }
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null || v.class != obj.getClass()) {
            return false;
        }
        v vVar = (v) obj;
        return this.f0.equals(vVar.f0) && Arrays.equals(this.h0, vVar.h0);
    }

    public int hashCode() {
        if (this.i0 == 0) {
            this.i0 = ((527 + this.f0.hashCode()) * 31) + Arrays.hashCode(this.h0);
        }
        return this.i0;
    }

    public final void j() {
        String h = h(this.h0[0].g0);
        int i = i(this.h0[0].i0);
        int i2 = 1;
        while (true) {
            j[] jVarArr = this.h0;
            if (i2 >= jVarArr.length) {
                return;
            }
            if (!h.equals(h(jVarArr[i2].g0))) {
                j[] jVarArr2 = this.h0;
                g("languages", jVarArr2[0].g0, jVarArr2[i2].g0, i2);
                return;
            } else if (i != i(this.h0[i2].i0)) {
                g("role flags", Integer.toBinaryString(this.h0[0].i0), Integer.toBinaryString(this.h0[i2].i0), i2);
                return;
            } else {
                i2++;
            }
        }
    }

    @Override // androidx.media3.common.e
    public Bundle toBundle() {
        Bundle bundle = new Bundle();
        bundle.putParcelableArrayList(e(0), is.c(Lists.k(this.h0)));
        bundle.putString(e(1), this.f0);
        return bundle;
    }

    public v(String str, j... jVarArr) {
        ii.a(jVarArr.length > 0);
        this.f0 = str;
        this.h0 = jVarArr;
        this.a = jVarArr.length;
        int i = y82.i(jVarArr[0].p0);
        this.g0 = i == -1 ? y82.i(jVarArr[0].o0) : i;
        j();
    }
}
