package androidx.media3.common;

import android.media.AudioAttributes;
import android.os.Bundle;

/* compiled from: AudioAttributes.java */
/* loaded from: classes.dex */
public final class b implements androidx.media3.common.e {
    public static final b k0 = new e().a();
    public final int a;
    public final int f0;
    public final int g0;
    public final int h0;
    public final int i0;
    public d j0;

    /* compiled from: AudioAttributes.java */
    /* renamed from: androidx.media3.common.b$b  reason: collision with other inner class name */
    /* loaded from: classes.dex */
    public static final class C0029b {
        public static void a(AudioAttributes.Builder builder, int i) {
            builder.setAllowedCapturePolicy(i);
        }
    }

    /* compiled from: AudioAttributes.java */
    /* loaded from: classes.dex */
    public static final class c {
        public static void a(AudioAttributes.Builder builder, int i) {
            builder.setSpatializationBehavior(i);
        }
    }

    /* compiled from: AudioAttributes.java */
    /* loaded from: classes.dex */
    public static final class d {
        public final AudioAttributes a;

        public d(b bVar) {
            AudioAttributes.Builder usage = new AudioAttributes.Builder().setContentType(bVar.a).setFlags(bVar.f0).setUsage(bVar.g0);
            int i = androidx.media3.common.util.b.a;
            if (i >= 29) {
                C0029b.a(usage, bVar.h0);
            }
            if (i >= 32) {
                c.a(usage, bVar.i0);
            }
            this.a = usage.build();
        }
    }

    /* compiled from: AudioAttributes.java */
    /* loaded from: classes.dex */
    public static final class e {
        public int a = 0;
        public int b = 0;
        public int c = 1;
        public int d = 1;
        public int e = 0;

        public b a() {
            return new b(this.a, this.b, this.c, this.d, this.e);
        }
    }

    public static String b(int i) {
        return Integer.toString(i, 36);
    }

    public d a() {
        if (this.j0 == null) {
            this.j0 = new d();
        }
        return this.j0;
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null || b.class != obj.getClass()) {
            return false;
        }
        b bVar = (b) obj;
        return this.a == bVar.a && this.f0 == bVar.f0 && this.g0 == bVar.g0 && this.h0 == bVar.h0 && this.i0 == bVar.i0;
    }

    public int hashCode() {
        return ((((((((527 + this.a) * 31) + this.f0) * 31) + this.g0) * 31) + this.h0) * 31) + this.i0;
    }

    @Override // androidx.media3.common.e
    public Bundle toBundle() {
        Bundle bundle = new Bundle();
        bundle.putInt(b(0), this.a);
        bundle.putInt(b(1), this.f0);
        bundle.putInt(b(2), this.g0);
        bundle.putInt(b(3), this.h0);
        bundle.putInt(b(4), this.i0);
        return bundle;
    }

    public b(int i, int i2, int i3, int i4, int i5) {
        this.a = i;
        this.f0 = i2;
        this.g0 = i3;
        this.h0 = i4;
        this.i0 = i5;
    }
}
