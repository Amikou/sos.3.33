package androidx.media3.common;

import android.os.Bundle;
import androidx.media3.common.e;
import com.google.common.collect.ImmutableList;
import java.util.Arrays;
import java.util.Collection;
import java.util.List;

/* compiled from: Tracks.java */
/* loaded from: classes.dex */
public final class y implements e {
    public static final y f0 = new y(ImmutableList.of());
    public final ImmutableList<a> a;

    /* compiled from: Tracks.java */
    /* loaded from: classes.dex */
    public static final class a implements e {
        public static final e.a<a> j0 = k84.a;
        public final int a;
        public final v f0;
        public final boolean g0;
        public final int[] h0;
        public final boolean[] i0;

        public a(v vVar, boolean z, int[] iArr, boolean[] zArr) {
            int i = vVar.a;
            this.a = i;
            boolean z2 = false;
            ii.a(i == iArr.length && i == zArr.length);
            this.f0 = vVar;
            if (z && i > 1) {
                z2 = true;
            }
            this.g0 = z2;
            this.h0 = (int[]) iArr.clone();
            this.i0 = (boolean[]) zArr.clone();
        }

        public static String j(int i) {
            return Integer.toString(i, 36);
        }

        public static /* synthetic */ a k(Bundle bundle) {
            v a = v.j0.a((Bundle) ii.e(bundle.getBundle(j(0))));
            return new a(a, bundle.getBoolean(j(4), false), (int[]) s92.a(bundle.getIntArray(j(1)), new int[a.a]), (boolean[]) s92.a(bundle.getBooleanArray(j(3)), new boolean[a.a]));
        }

        public v b() {
            return this.f0;
        }

        public j c(int i) {
            return this.f0.c(i);
        }

        public int d() {
            return this.f0.g0;
        }

        public boolean e() {
            return this.g0;
        }

        public boolean equals(Object obj) {
            if (this == obj) {
                return true;
            }
            if (obj == null || a.class != obj.getClass()) {
                return false;
            }
            a aVar = (a) obj;
            return this.g0 == aVar.g0 && this.f0.equals(aVar.f0) && Arrays.equals(this.h0, aVar.h0) && Arrays.equals(this.i0, aVar.i0);
        }

        public boolean f() {
            return cr.b(this.i0, true);
        }

        public boolean g(int i) {
            return this.i0[i];
        }

        public boolean h(int i) {
            return i(i, false);
        }

        public int hashCode() {
            return (((((this.f0.hashCode() * 31) + (this.g0 ? 1 : 0)) * 31) + Arrays.hashCode(this.h0)) * 31) + Arrays.hashCode(this.i0);
        }

        public boolean i(int i, boolean z) {
            int[] iArr = this.h0;
            return iArr[i] == 4 || (z && iArr[i] == 3);
        }

        @Override // androidx.media3.common.e
        public Bundle toBundle() {
            Bundle bundle = new Bundle();
            bundle.putBundle(j(0), this.f0.toBundle());
            bundle.putIntArray(j(1), this.h0);
            bundle.putBooleanArray(j(3), this.i0);
            bundle.putBoolean(j(4), this.g0);
            return bundle;
        }
    }

    public y(List<a> list) {
        this.a = ImmutableList.copyOf((Collection) list);
    }

    public static String d(int i) {
        return Integer.toString(i, 36);
    }

    public ImmutableList<a> a() {
        return this.a;
    }

    public boolean b() {
        return this.a.isEmpty();
    }

    public boolean c(int i) {
        for (int i2 = 0; i2 < this.a.size(); i2++) {
            a aVar = this.a.get(i2);
            if (aVar.f() && aVar.d() == i) {
                return true;
            }
        }
        return false;
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null || y.class != obj.getClass()) {
            return false;
        }
        return this.a.equals(((y) obj).a);
    }

    public int hashCode() {
        return this.a.hashCode();
    }

    @Override // androidx.media3.common.e
    public Bundle toBundle() {
        Bundle bundle = new Bundle();
        bundle.putParcelableArrayList(d(0), is.c(this.a));
        return bundle;
    }
}
