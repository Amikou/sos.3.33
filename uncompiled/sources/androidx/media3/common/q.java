package androidx.media3.common;

import android.os.Bundle;
import android.os.Looper;
import android.view.SurfaceView;
import android.view.TextureView;
import androidx.media3.common.i;
import java.util.ArrayList;
import java.util.List;

/* compiled from: Player.java */
/* loaded from: classes.dex */
public interface q {

    /* compiled from: Player.java */
    /* loaded from: classes.dex */
    public static final class b implements androidx.media3.common.e {
        public static final b f0 = new a().e();
        public final i a;

        /* compiled from: Player.java */
        /* loaded from: classes.dex */
        public static final class a {
            public final i.b a = new i.b();

            public a a(int i) {
                this.a.a(i);
                return this;
            }

            public a b(b bVar) {
                this.a.b(bVar.a);
                return this;
            }

            public a c(int... iArr) {
                this.a.c(iArr);
                return this;
            }

            public a d(int i, boolean z) {
                this.a.d(i, z);
                return this;
            }

            public b e() {
                return new b(this.a.e());
            }
        }

        public static String c(int i) {
            return Integer.toString(i, 36);
        }

        public boolean b(int i) {
            return this.a.a(i);
        }

        public boolean equals(Object obj) {
            if (this == obj) {
                return true;
            }
            if (obj instanceof b) {
                return this.a.equals(((b) obj).a);
            }
            return false;
        }

        public int hashCode() {
            return this.a.hashCode();
        }

        @Override // androidx.media3.common.e
        public Bundle toBundle() {
            Bundle bundle = new Bundle();
            ArrayList<Integer> arrayList = new ArrayList<>();
            for (int i = 0; i < this.a.d(); i++) {
                arrayList.add(Integer.valueOf(this.a.c(i)));
            }
            bundle.putIntegerArrayList(c(0), arrayList);
            return bundle;
        }

        public b(i iVar) {
            this.a = iVar;
        }
    }

    /* compiled from: Player.java */
    /* loaded from: classes.dex */
    public static final class c {
        public final i a;

        public c(i iVar) {
            this.a = iVar;
        }

        public boolean a(int i) {
            return this.a.a(i);
        }

        public boolean b(int... iArr) {
            return this.a.b(iArr);
        }

        public boolean equals(Object obj) {
            if (this == obj) {
                return true;
            }
            if (obj instanceof c) {
                return this.a.equals(((c) obj).a);
            }
            return false;
        }

        public int hashCode() {
            return this.a.hashCode();
        }
    }

    /* compiled from: Player.java */
    /* loaded from: classes.dex */
    public interface d {
        void D(int i);

        @Deprecated
        void E(boolean z);

        @Deprecated
        void F(int i);

        void H(boolean z);

        @Deprecated
        void I();

        void J(q qVar, c cVar);

        void L(u uVar, int i);

        void N(boolean z);

        void Q(int i, boolean z);

        @Deprecated
        void R(boolean z, int i);

        void T(n nVar);

        void V(x xVar);

        void X();

        void Y(y yVar);

        void Z(h hVar);

        void a0(m mVar, int i);

        void b(boolean z);

        void c0(PlaybackException playbackException);

        void h0(PlaybackException playbackException);

        void i(int i);

        void k(z zVar);

        void k0(int i, int i2);

        void l0(b bVar);

        void m0(e eVar, e eVar2, int i);

        void n(p pVar);

        void o(nb0 nb0Var);

        void p0(boolean z);

        void s(int i);

        void t(Metadata metadata);

        @Deprecated
        void u(List<kb0> list);

        void y(boolean z, int i);
    }

    /* compiled from: Player.java */
    /* loaded from: classes.dex */
    public static final class e implements androidx.media3.common.e {
        public final Object a;
        public final int f0;
        public final m g0;
        public final Object h0;
        public final int i0;
        public final long j0;
        public final long k0;
        public final int l0;
        public final int m0;

        public e(Object obj, int i, m mVar, Object obj2, int i2, long j, long j2, int i3, int i4) {
            this.a = obj;
            this.f0 = i;
            this.g0 = mVar;
            this.h0 = obj2;
            this.i0 = i2;
            this.j0 = j;
            this.k0 = j2;
            this.l0 = i3;
            this.m0 = i4;
        }

        public static String a(int i) {
            return Integer.toString(i, 36);
        }

        public boolean equals(Object obj) {
            if (this == obj) {
                return true;
            }
            if (obj == null || e.class != obj.getClass()) {
                return false;
            }
            e eVar = (e) obj;
            return this.f0 == eVar.f0 && this.i0 == eVar.i0 && this.j0 == eVar.j0 && this.k0 == eVar.k0 && this.l0 == eVar.l0 && this.m0 == eVar.m0 && ql2.a(this.a, eVar.a) && ql2.a(this.h0, eVar.h0) && ql2.a(this.g0, eVar.g0);
        }

        public int hashCode() {
            return ql2.b(this.a, Integer.valueOf(this.f0), this.g0, this.h0, Integer.valueOf(this.i0), Long.valueOf(this.j0), Long.valueOf(this.k0), Integer.valueOf(this.l0), Integer.valueOf(this.m0));
        }

        @Override // androidx.media3.common.e
        public Bundle toBundle() {
            Bundle bundle = new Bundle();
            bundle.putInt(a(0), this.f0);
            if (this.g0 != null) {
                bundle.putBundle(a(1), this.g0.toBundle());
            }
            bundle.putInt(a(2), this.i0);
            bundle.putLong(a(3), this.j0);
            bundle.putLong(a(4), this.k0);
            bundle.putInt(a(5), this.l0);
            bundle.putInt(a(6), this.m0);
            return bundle;
        }
    }

    void A(m mVar);

    int B();

    y C();

    boolean D();

    boolean E();

    nb0 F();

    void G(d dVar);

    int H();

    int I();

    boolean J(int i);

    void K(int i);

    void L(x xVar);

    void M(SurfaceView surfaceView);

    boolean N();

    void O(d dVar);

    int P();

    int Q();

    long R();

    u S();

    Looper T();

    boolean U();

    x V();

    long W();

    void X();

    void Y();

    void Z(TextureView textureView);

    void a();

    void a0();

    void b(p pVar);

    n b0();

    void c();

    long c0();

    void d();

    long d0();

    p e();

    boolean e0();

    void f();

    boolean g();

    long h();

    void i(int i, long j);

    b j();

    boolean k();

    void l(boolean z);

    long m();

    int n();

    void o(TextureView textureView);

    z p();

    void q(List<m> list, boolean z);

    boolean r();

    int s();

    void t(SurfaceView surfaceView);

    void u();

    PlaybackException v();

    void w(boolean z);

    long x();

    long y();

    boolean z();
}
