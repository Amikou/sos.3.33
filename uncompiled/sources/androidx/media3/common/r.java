package androidx.media3.common;

import android.os.Bundle;
import androidx.media3.common.e;

/* compiled from: Rating.java */
/* loaded from: classes.dex */
public abstract class r implements e {
    public static final e.a<r> a = v33.a;

    public static r b(Bundle bundle) {
        int i = bundle.getInt(c(0), -1);
        if (i != 0) {
            if (i != 1) {
                if (i != 2) {
                    if (i == 3) {
                        return t.h0.a(bundle);
                    }
                    throw new IllegalArgumentException("Unknown RatingType: " + i);
                }
                return s.h0.a(bundle);
            }
            return o.g0.a(bundle);
        }
        return l.h0.a(bundle);
    }

    public static String c(int i) {
        return Integer.toString(i, 36);
    }
}
