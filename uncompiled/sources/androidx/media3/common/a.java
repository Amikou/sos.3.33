package androidx.media3.common;

import android.net.Uri;
import android.os.Bundle;
import android.os.Parcelable;
import androidx.media3.common.e;
import java.util.ArrayList;
import java.util.Arrays;
import zendesk.support.request.CellBase;

/* compiled from: AdPlaybackState.java */
/* loaded from: classes.dex */
public final class a implements e {
    public static final a k0 = new a(null, new C0028a[0], 0, CellBase.ID_SYSTEM_MESSAGE_REQUEST_CLOSED, 0);
    public static final C0028a l0 = new C0028a(0).j(0);
    public static final e.a<a> m0 = g8.a;
    public final Object a;
    public final int f0;
    public final long g0;
    public final long h0;
    public final int i0;
    public final C0028a[] j0;

    /* compiled from: AdPlaybackState.java */
    /* renamed from: androidx.media3.common.a$a  reason: collision with other inner class name */
    /* loaded from: classes.dex */
    public static final class C0028a implements e {
        public static final e.a<C0028a> l0 = h8.a;
        public final long a;
        public final int f0;
        public final Uri[] g0;
        public final int[] h0;
        public final long[] i0;
        public final long j0;
        public final boolean k0;

        public C0028a(long j) {
            this(j, -1, new int[0], new Uri[0], new long[0], 0L, false);
        }

        public static long[] b(long[] jArr, int i) {
            int length = jArr.length;
            int max = Math.max(i, length);
            long[] copyOf = Arrays.copyOf(jArr, max);
            Arrays.fill(copyOf, length, max, (long) CellBase.ID_SYSTEM_MESSAGE_REQUEST_CLOSED);
            return copyOf;
        }

        public static int[] c(int[] iArr, int i) {
            int length = iArr.length;
            int max = Math.max(i, length);
            int[] copyOf = Arrays.copyOf(iArr, max);
            Arrays.fill(copyOf, length, max, 0);
            return copyOf;
        }

        public static C0028a d(Bundle bundle) {
            long j = bundle.getLong(h(0));
            int i = bundle.getInt(h(1), -1);
            ArrayList parcelableArrayList = bundle.getParcelableArrayList(h(2));
            int[] intArray = bundle.getIntArray(h(3));
            long[] longArray = bundle.getLongArray(h(4));
            long j2 = bundle.getLong(h(5));
            boolean z = bundle.getBoolean(h(6));
            if (intArray == null) {
                intArray = new int[0];
            }
            return new C0028a(j, i, intArray, parcelableArrayList == null ? new Uri[0] : (Uri[]) parcelableArrayList.toArray(new Uri[0]), longArray == null ? new long[0] : longArray, j2, z);
        }

        public static String h(int i) {
            return Integer.toString(i, 36);
        }

        public int e() {
            return f(-1);
        }

        public boolean equals(Object obj) {
            if (this == obj) {
                return true;
            }
            if (obj == null || C0028a.class != obj.getClass()) {
                return false;
            }
            C0028a c0028a = (C0028a) obj;
            return this.a == c0028a.a && this.f0 == c0028a.f0 && Arrays.equals(this.g0, c0028a.g0) && Arrays.equals(this.h0, c0028a.h0) && Arrays.equals(this.i0, c0028a.i0) && this.j0 == c0028a.j0 && this.k0 == c0028a.k0;
        }

        public int f(int i) {
            int i2 = i + 1;
            while (true) {
                int[] iArr = this.h0;
                if (i2 >= iArr.length || this.k0 || iArr[i2] == 0 || iArr[i2] == 1) {
                    break;
                }
                i2++;
            }
            return i2;
        }

        public boolean g() {
            if (this.f0 == -1) {
                return true;
            }
            for (int i = 0; i < this.f0; i++) {
                int[] iArr = this.h0;
                if (iArr[i] == 0 || iArr[i] == 1) {
                    return true;
                }
            }
            return false;
        }

        public int hashCode() {
            long j = this.a;
            long j2 = this.j0;
            return (((((((((((this.f0 * 31) + ((int) (j ^ (j >>> 32)))) * 31) + Arrays.hashCode(this.g0)) * 31) + Arrays.hashCode(this.h0)) * 31) + Arrays.hashCode(this.i0)) * 31) + ((int) (j2 ^ (j2 >>> 32)))) * 31) + (this.k0 ? 1 : 0);
        }

        public boolean i() {
            return this.f0 == -1 || e() < this.f0;
        }

        public C0028a j(int i) {
            int[] c = c(this.h0, i);
            long[] b = b(this.i0, i);
            return new C0028a(this.a, i, c, (Uri[]) Arrays.copyOf(this.g0, i), b, this.j0, this.k0);
        }

        @Override // androidx.media3.common.e
        public Bundle toBundle() {
            Bundle bundle = new Bundle();
            bundle.putLong(h(0), this.a);
            bundle.putInt(h(1), this.f0);
            bundle.putParcelableArrayList(h(2), new ArrayList<>(Arrays.asList(this.g0)));
            bundle.putIntArray(h(3), this.h0);
            bundle.putLongArray(h(4), this.i0);
            bundle.putLong(h(5), this.j0);
            bundle.putBoolean(h(6), this.k0);
            return bundle;
        }

        public C0028a(long j, int i, int[] iArr, Uri[] uriArr, long[] jArr, long j2, boolean z) {
            ii.a(iArr.length == uriArr.length);
            this.a = j;
            this.f0 = i;
            this.h0 = iArr;
            this.g0 = uriArr;
            this.i0 = jArr;
            this.j0 = j2;
            this.k0 = z;
        }
    }

    public a(Object obj, C0028a[] c0028aArr, long j, long j2, int i) {
        this.a = obj;
        this.g0 = j;
        this.h0 = j2;
        this.f0 = c0028aArr.length + i;
        this.j0 = c0028aArr;
        this.i0 = i;
    }

    public static a b(Bundle bundle) {
        C0028a[] c0028aArr;
        ArrayList parcelableArrayList = bundle.getParcelableArrayList(g(1));
        if (parcelableArrayList == null) {
            c0028aArr = new C0028a[0];
        } else {
            C0028a[] c0028aArr2 = new C0028a[parcelableArrayList.size()];
            for (int i = 0; i < parcelableArrayList.size(); i++) {
                c0028aArr2[i] = C0028a.l0.a((Bundle) parcelableArrayList.get(i));
            }
            c0028aArr = c0028aArr2;
        }
        return new a(null, c0028aArr, bundle.getLong(g(2), 0L), bundle.getLong(g(3), CellBase.ID_SYSTEM_MESSAGE_REQUEST_CLOSED), bundle.getInt(g(4)));
    }

    public static String g(int i) {
        return Integer.toString(i, 36);
    }

    public C0028a c(int i) {
        int i2 = this.i0;
        if (i < i2) {
            return l0;
        }
        return this.j0[i - i2];
    }

    public int d(long j, long j2) {
        if (j != Long.MIN_VALUE) {
            if (j2 == CellBase.ID_SYSTEM_MESSAGE_REQUEST_CLOSED || j < j2) {
                int i = this.i0;
                while (i < this.f0 && ((c(i).a != Long.MIN_VALUE && c(i).a <= j) || !c(i).i())) {
                    i++;
                }
                if (i < this.f0) {
                    return i;
                }
                return -1;
            }
            return -1;
        }
        return -1;
    }

    public int e(long j, long j2) {
        int i = this.f0 - 1;
        while (i >= 0 && f(j, j2, i)) {
            i--;
        }
        if (i < 0 || !c(i).g()) {
            return -1;
        }
        return i;
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null || a.class != obj.getClass()) {
            return false;
        }
        a aVar = (a) obj;
        return androidx.media3.common.util.b.c(this.a, aVar.a) && this.f0 == aVar.f0 && this.g0 == aVar.g0 && this.h0 == aVar.h0 && this.i0 == aVar.i0 && Arrays.equals(this.j0, aVar.j0);
    }

    public final boolean f(long j, long j2, int i) {
        if (j == Long.MIN_VALUE) {
            return false;
        }
        long j3 = c(i).a;
        return j3 == Long.MIN_VALUE ? j2 == CellBase.ID_SYSTEM_MESSAGE_REQUEST_CLOSED || j < j2 : j < j3;
    }

    public int hashCode() {
        int i = this.f0 * 31;
        Object obj = this.a;
        return ((((((((i + (obj == null ? 0 : obj.hashCode())) * 31) + ((int) this.g0)) * 31) + ((int) this.h0)) * 31) + this.i0) * 31) + Arrays.hashCode(this.j0);
    }

    @Override // androidx.media3.common.e
    public Bundle toBundle() {
        Bundle bundle = new Bundle();
        ArrayList<? extends Parcelable> arrayList = new ArrayList<>();
        for (C0028a c0028a : this.j0) {
            arrayList.add(c0028a.toBundle());
        }
        bundle.putParcelableArrayList(g(1), arrayList);
        bundle.putLong(g(2), this.g0);
        bundle.putLong(g(3), this.h0);
        bundle.putInt(g(4), this.i0);
        return bundle;
    }

    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("AdPlaybackState(adsId=");
        sb.append(this.a);
        sb.append(", adResumePositionUs=");
        sb.append(this.g0);
        sb.append(", adGroups=[");
        for (int i = 0; i < this.j0.length; i++) {
            sb.append("adGroup(timeUs=");
            sb.append(this.j0[i].a);
            sb.append(", ads=[");
            for (int i2 = 0; i2 < this.j0[i].h0.length; i2++) {
                sb.append("ad(state=");
                int i3 = this.j0[i].h0[i2];
                if (i3 == 0) {
                    sb.append('_');
                } else if (i3 == 1) {
                    sb.append('R');
                } else if (i3 == 2) {
                    sb.append('S');
                } else if (i3 == 3) {
                    sb.append('P');
                } else if (i3 != 4) {
                    sb.append('?');
                } else {
                    sb.append('!');
                }
                sb.append(", durationUs=");
                sb.append(this.j0[i].i0[i2]);
                sb.append(')');
                if (i2 < this.j0[i].h0.length - 1) {
                    sb.append(", ");
                }
            }
            sb.append("])");
            if (i < this.j0.length - 1) {
                sb.append(", ");
            }
        }
        sb.append("])");
        return sb.toString();
    }
}
