package androidx.media3.common;

import android.os.Bundle;
import androidx.media3.common.e;

/* compiled from: HeartRating.java */
/* loaded from: classes.dex */
public final class l extends r {
    public static final e.a<l> h0 = kk1.a;
    public final boolean f0;
    public final boolean g0;

    public l() {
        this.f0 = false;
        this.g0 = false;
    }

    public static String c(int i) {
        return Integer.toString(i, 36);
    }

    public static l e(Bundle bundle) {
        ii.a(bundle.getInt(c(0), -1) == 0);
        if (bundle.getBoolean(c(1), false)) {
            return new l(bundle.getBoolean(c(2), false));
        }
        return new l();
    }

    public boolean equals(Object obj) {
        if (obj instanceof l) {
            l lVar = (l) obj;
            return this.g0 == lVar.g0 && this.f0 == lVar.f0;
        }
        return false;
    }

    public int hashCode() {
        return ql2.b(Boolean.valueOf(this.f0), Boolean.valueOf(this.g0));
    }

    @Override // androidx.media3.common.e
    public Bundle toBundle() {
        Bundle bundle = new Bundle();
        bundle.putInt(c(0), 0);
        bundle.putBoolean(c(1), this.f0);
        bundle.putBoolean(c(2), this.g0);
        return bundle;
    }

    public l(boolean z) {
        this.f0 = true;
        this.g0 = z;
    }
}
