package androidx.media3.common;

import android.os.Looper;
import android.view.SurfaceView;
import android.view.TextureView;
import androidx.media3.common.q;
import java.util.List;

/* compiled from: ForwardingPlayer.java */
/* loaded from: classes.dex */
public class k implements q {
    public final q a;

    /* compiled from: ForwardingPlayer.java */
    /* loaded from: classes.dex */
    public static final class a implements q.d {
        public final k a;
        public final q.d f0;

        public a(k kVar, q.d dVar) {
            this.a = kVar;
            this.f0 = dVar;
        }

        @Override // androidx.media3.common.q.d
        public void D(int i) {
            this.f0.D(i);
        }

        @Override // androidx.media3.common.q.d
        public void E(boolean z) {
            this.f0.H(z);
        }

        @Override // androidx.media3.common.q.d
        public void F(int i) {
            this.f0.F(i);
        }

        @Override // androidx.media3.common.q.d
        public void H(boolean z) {
            this.f0.H(z);
        }

        @Override // androidx.media3.common.q.d
        public void I() {
            this.f0.I();
        }

        @Override // androidx.media3.common.q.d
        public void J(q qVar, q.c cVar) {
            this.f0.J(this.a, cVar);
        }

        @Override // androidx.media3.common.q.d
        public void L(u uVar, int i) {
            this.f0.L(uVar, i);
        }

        @Override // androidx.media3.common.q.d
        public void N(boolean z) {
            this.f0.N(z);
        }

        @Override // androidx.media3.common.q.d
        public void Q(int i, boolean z) {
            this.f0.Q(i, z);
        }

        @Override // androidx.media3.common.q.d
        public void R(boolean z, int i) {
            this.f0.R(z, i);
        }

        @Override // androidx.media3.common.q.d
        public void T(n nVar) {
            this.f0.T(nVar);
        }

        @Override // androidx.media3.common.q.d
        public void V(x xVar) {
            this.f0.V(xVar);
        }

        @Override // androidx.media3.common.q.d
        public void X() {
            this.f0.X();
        }

        @Override // androidx.media3.common.q.d
        public void Y(y yVar) {
            this.f0.Y(yVar);
        }

        @Override // androidx.media3.common.q.d
        public void Z(h hVar) {
            this.f0.Z(hVar);
        }

        @Override // androidx.media3.common.q.d
        public void a0(m mVar, int i) {
            this.f0.a0(mVar, i);
        }

        @Override // androidx.media3.common.q.d
        public void b(boolean z) {
            this.f0.b(z);
        }

        @Override // androidx.media3.common.q.d
        public void c0(PlaybackException playbackException) {
            this.f0.c0(playbackException);
        }

        public boolean equals(Object obj) {
            if (this == obj) {
                return true;
            }
            if (obj instanceof a) {
                a aVar = (a) obj;
                if (this.a.equals(aVar.a)) {
                    return this.f0.equals(aVar.f0);
                }
                return false;
            }
            return false;
        }

        @Override // androidx.media3.common.q.d
        public void h0(PlaybackException playbackException) {
            this.f0.h0(playbackException);
        }

        public int hashCode() {
            return (this.a.hashCode() * 31) + this.f0.hashCode();
        }

        @Override // androidx.media3.common.q.d
        public void i(int i) {
            this.f0.i(i);
        }

        @Override // androidx.media3.common.q.d
        public void k(z zVar) {
            this.f0.k(zVar);
        }

        @Override // androidx.media3.common.q.d
        public void k0(int i, int i2) {
            this.f0.k0(i, i2);
        }

        @Override // androidx.media3.common.q.d
        public void l0(q.b bVar) {
            this.f0.l0(bVar);
        }

        @Override // androidx.media3.common.q.d
        public void m0(q.e eVar, q.e eVar2, int i) {
            this.f0.m0(eVar, eVar2, i);
        }

        @Override // androidx.media3.common.q.d
        public void n(p pVar) {
            this.f0.n(pVar);
        }

        @Override // androidx.media3.common.q.d
        public void o(nb0 nb0Var) {
            this.f0.o(nb0Var);
        }

        @Override // androidx.media3.common.q.d
        public void p0(boolean z) {
            this.f0.p0(z);
        }

        @Override // androidx.media3.common.q.d
        public void s(int i) {
            this.f0.s(i);
        }

        @Override // androidx.media3.common.q.d
        public void t(Metadata metadata) {
            this.f0.t(metadata);
        }

        @Override // androidx.media3.common.q.d
        public void u(List<kb0> list) {
            this.f0.u(list);
        }

        @Override // androidx.media3.common.q.d
        public void y(boolean z, int i) {
            this.f0.y(z, i);
        }
    }

    @Override // androidx.media3.common.q
    public int B() {
        return this.a.B();
    }

    @Override // androidx.media3.common.q
    public y C() {
        return this.a.C();
    }

    @Override // androidx.media3.common.q
    public boolean D() {
        return this.a.D();
    }

    @Override // androidx.media3.common.q
    public boolean E() {
        return this.a.E();
    }

    @Override // androidx.media3.common.q
    public nb0 F() {
        return this.a.F();
    }

    @Override // androidx.media3.common.q
    public void G(q.d dVar) {
        this.a.G(new a(this, dVar));
    }

    @Override // androidx.media3.common.q
    public int H() {
        return this.a.H();
    }

    @Override // androidx.media3.common.q
    public int I() {
        return this.a.I();
    }

    @Override // androidx.media3.common.q
    public boolean J(int i) {
        return this.a.J(i);
    }

    @Override // androidx.media3.common.q
    public void K(int i) {
        this.a.K(i);
    }

    @Override // androidx.media3.common.q
    public void L(x xVar) {
        this.a.L(xVar);
    }

    @Override // androidx.media3.common.q
    public void M(SurfaceView surfaceView) {
        this.a.M(surfaceView);
    }

    @Override // androidx.media3.common.q
    public boolean N() {
        return this.a.N();
    }

    @Override // androidx.media3.common.q
    public void O(q.d dVar) {
        this.a.O(new a(this, dVar));
    }

    @Override // androidx.media3.common.q
    public int P() {
        return this.a.P();
    }

    @Override // androidx.media3.common.q
    public int Q() {
        return this.a.Q();
    }

    @Override // androidx.media3.common.q
    public long R() {
        return this.a.R();
    }

    @Override // androidx.media3.common.q
    public u S() {
        return this.a.S();
    }

    @Override // androidx.media3.common.q
    public Looper T() {
        return this.a.T();
    }

    @Override // androidx.media3.common.q
    public boolean U() {
        return this.a.U();
    }

    @Override // androidx.media3.common.q
    public x V() {
        return this.a.V();
    }

    @Override // androidx.media3.common.q
    public long W() {
        return this.a.W();
    }

    @Override // androidx.media3.common.q
    public void X() {
        this.a.X();
    }

    @Override // androidx.media3.common.q
    public void Y() {
        this.a.Y();
    }

    @Override // androidx.media3.common.q
    public void Z(TextureView textureView) {
        this.a.Z(textureView);
    }

    @Override // androidx.media3.common.q
    public void a0() {
        this.a.a0();
    }

    @Override // androidx.media3.common.q
    public void b(p pVar) {
        this.a.b(pVar);
    }

    @Override // androidx.media3.common.q
    public n b0() {
        return this.a.b0();
    }

    @Override // androidx.media3.common.q
    public void c() {
        this.a.c();
    }

    @Override // androidx.media3.common.q
    public long c0() {
        return this.a.c0();
    }

    @Override // androidx.media3.common.q
    public void d() {
        this.a.d();
    }

    @Override // androidx.media3.common.q
    public long d0() {
        return this.a.d0();
    }

    @Override // androidx.media3.common.q
    public p e() {
        return this.a.e();
    }

    @Override // androidx.media3.common.q
    public boolean e0() {
        return this.a.e0();
    }

    @Override // androidx.media3.common.q
    public void f() {
        this.a.f();
    }

    public q f0() {
        return this.a;
    }

    @Override // androidx.media3.common.q
    public boolean g() {
        return this.a.g();
    }

    @Override // androidx.media3.common.q
    public long h() {
        return this.a.h();
    }

    @Override // androidx.media3.common.q
    public void i(int i, long j) {
        this.a.i(i, j);
    }

    @Override // androidx.media3.common.q
    public boolean k() {
        return this.a.k();
    }

    @Override // androidx.media3.common.q
    public void l(boolean z) {
        this.a.l(z);
    }

    @Override // androidx.media3.common.q
    public int n() {
        return this.a.n();
    }

    @Override // androidx.media3.common.q
    public void o(TextureView textureView) {
        this.a.o(textureView);
    }

    @Override // androidx.media3.common.q
    public z p() {
        return this.a.p();
    }

    @Override // androidx.media3.common.q
    public boolean r() {
        return this.a.r();
    }

    @Override // androidx.media3.common.q
    public int s() {
        return this.a.s();
    }

    @Override // androidx.media3.common.q
    public void t(SurfaceView surfaceView) {
        this.a.t(surfaceView);
    }

    @Override // androidx.media3.common.q
    public void u() {
        this.a.u();
    }

    @Override // androidx.media3.common.q
    public PlaybackException v() {
        return this.a.v();
    }

    @Override // androidx.media3.common.q
    public long x() {
        return this.a.x();
    }

    @Override // androidx.media3.common.q
    public long y() {
        return this.a.y();
    }

    @Override // androidx.media3.common.q
    public boolean z() {
        return this.a.z();
    }
}
