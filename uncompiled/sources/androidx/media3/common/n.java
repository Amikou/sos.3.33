package androidx.media3.common;

import android.net.Uri;
import android.os.Bundle;
import androidx.media3.common.e;
import java.util.Arrays;
import java.util.List;

/* compiled from: MediaMetadata.java */
/* loaded from: classes.dex */
public final class n implements e {
    public static final n K0 = new b().F();
    public static final e.a<n> L0 = h62.a;
    public final Integer A0;
    public final CharSequence B0;
    public final CharSequence C0;
    public final CharSequence D0;
    public final Integer E0;
    public final Integer F0;
    public final CharSequence G0;
    public final CharSequence H0;
    public final CharSequence I0;
    public final Bundle J0;
    public final CharSequence a;
    public final CharSequence f0;
    public final CharSequence g0;
    public final CharSequence h0;
    public final CharSequence i0;
    public final CharSequence j0;
    public final CharSequence k0;
    public final r l0;
    public final r m0;
    public final byte[] n0;
    public final Integer o0;
    public final Uri p0;
    public final Integer q0;
    public final Integer r0;
    public final Integer s0;
    public final Boolean t0;
    @Deprecated
    public final Integer u0;
    public final Integer v0;
    public final Integer w0;
    public final Integer x0;
    public final Integer y0;
    public final Integer z0;

    /* compiled from: MediaMetadata.java */
    /* loaded from: classes.dex */
    public static final class b {
        public Integer A;
        public CharSequence B;
        public CharSequence C;
        public CharSequence D;
        public Bundle E;
        public CharSequence a;
        public CharSequence b;
        public CharSequence c;
        public CharSequence d;
        public CharSequence e;
        public CharSequence f;
        public CharSequence g;
        public r h;
        public r i;
        public byte[] j;
        public Integer k;
        public Uri l;
        public Integer m;
        public Integer n;
        public Integer o;
        public Boolean p;
        public Integer q;
        public Integer r;
        public Integer s;
        public Integer t;
        public Integer u;
        public Integer v;
        public CharSequence w;
        public CharSequence x;
        public CharSequence y;
        public Integer z;

        public n F() {
            return new n(this);
        }

        public b G(byte[] bArr, int i) {
            if (this.j == null || androidx.media3.common.util.b.c(Integer.valueOf(i), 3) || !androidx.media3.common.util.b.c(this.k, 3)) {
                this.j = (byte[]) bArr.clone();
                this.k = Integer.valueOf(i);
            }
            return this;
        }

        public b H(n nVar) {
            if (nVar == null) {
                return this;
            }
            CharSequence charSequence = nVar.a;
            if (charSequence != null) {
                i0(charSequence);
            }
            CharSequence charSequence2 = nVar.f0;
            if (charSequence2 != null) {
                M(charSequence2);
            }
            CharSequence charSequence3 = nVar.g0;
            if (charSequence3 != null) {
                L(charSequence3);
            }
            CharSequence charSequence4 = nVar.h0;
            if (charSequence4 != null) {
                K(charSequence4);
            }
            CharSequence charSequence5 = nVar.i0;
            if (charSequence5 != null) {
                U(charSequence5);
            }
            CharSequence charSequence6 = nVar.j0;
            if (charSequence6 != null) {
                h0(charSequence6);
            }
            CharSequence charSequence7 = nVar.k0;
            if (charSequence7 != null) {
                S(charSequence7);
            }
            r rVar = nVar.l0;
            if (rVar != null) {
                m0(rVar);
            }
            r rVar2 = nVar.m0;
            if (rVar2 != null) {
                Z(rVar2);
            }
            byte[] bArr = nVar.n0;
            if (bArr != null) {
                N(bArr, nVar.o0);
            }
            Uri uri = nVar.p0;
            if (uri != null) {
                O(uri);
            }
            Integer num = nVar.q0;
            if (num != null) {
                l0(num);
            }
            Integer num2 = nVar.r0;
            if (num2 != null) {
                k0(num2);
            }
            Integer num3 = nVar.s0;
            if (num3 != null) {
                W(num3);
            }
            Boolean bool = nVar.t0;
            if (bool != null) {
                Y(bool);
            }
            Integer num4 = nVar.u0;
            if (num4 != null) {
                c0(num4);
            }
            Integer num5 = nVar.v0;
            if (num5 != null) {
                c0(num5);
            }
            Integer num6 = nVar.w0;
            if (num6 != null) {
                b0(num6);
            }
            Integer num7 = nVar.x0;
            if (num7 != null) {
                a0(num7);
            }
            Integer num8 = nVar.y0;
            if (num8 != null) {
                f0(num8);
            }
            Integer num9 = nVar.z0;
            if (num9 != null) {
                e0(num9);
            }
            Integer num10 = nVar.A0;
            if (num10 != null) {
                d0(num10);
            }
            CharSequence charSequence8 = nVar.B0;
            if (charSequence8 != null) {
                n0(charSequence8);
            }
            CharSequence charSequence9 = nVar.C0;
            if (charSequence9 != null) {
                Q(charSequence9);
            }
            CharSequence charSequence10 = nVar.D0;
            if (charSequence10 != null) {
                R(charSequence10);
            }
            Integer num11 = nVar.E0;
            if (num11 != null) {
                T(num11);
            }
            Integer num12 = nVar.F0;
            if (num12 != null) {
                j0(num12);
            }
            CharSequence charSequence11 = nVar.G0;
            if (charSequence11 != null) {
                X(charSequence11);
            }
            CharSequence charSequence12 = nVar.H0;
            if (charSequence12 != null) {
                P(charSequence12);
            }
            CharSequence charSequence13 = nVar.I0;
            if (charSequence13 != null) {
                g0(charSequence13);
            }
            Bundle bundle = nVar.J0;
            if (bundle != null) {
                V(bundle);
            }
            return this;
        }

        public b I(Metadata metadata) {
            for (int i = 0; i < metadata.d(); i++) {
                metadata.c(i).r0(this);
            }
            return this;
        }

        public b J(List<Metadata> list) {
            for (int i = 0; i < list.size(); i++) {
                Metadata metadata = list.get(i);
                for (int i2 = 0; i2 < metadata.d(); i2++) {
                    metadata.c(i2).r0(this);
                }
            }
            return this;
        }

        public b K(CharSequence charSequence) {
            this.d = charSequence;
            return this;
        }

        public b L(CharSequence charSequence) {
            this.c = charSequence;
            return this;
        }

        public b M(CharSequence charSequence) {
            this.b = charSequence;
            return this;
        }

        public b N(byte[] bArr, Integer num) {
            this.j = bArr == null ? null : (byte[]) bArr.clone();
            this.k = num;
            return this;
        }

        public b O(Uri uri) {
            this.l = uri;
            return this;
        }

        public b P(CharSequence charSequence) {
            this.C = charSequence;
            return this;
        }

        public b Q(CharSequence charSequence) {
            this.x = charSequence;
            return this;
        }

        public b R(CharSequence charSequence) {
            this.y = charSequence;
            return this;
        }

        public b S(CharSequence charSequence) {
            this.g = charSequence;
            return this;
        }

        public b T(Integer num) {
            this.z = num;
            return this;
        }

        public b U(CharSequence charSequence) {
            this.e = charSequence;
            return this;
        }

        public b V(Bundle bundle) {
            this.E = bundle;
            return this;
        }

        public b W(Integer num) {
            this.o = num;
            return this;
        }

        public b X(CharSequence charSequence) {
            this.B = charSequence;
            return this;
        }

        public b Y(Boolean bool) {
            this.p = bool;
            return this;
        }

        public b Z(r rVar) {
            this.i = rVar;
            return this;
        }

        public b a0(Integer num) {
            this.s = num;
            return this;
        }

        public b b0(Integer num) {
            this.r = num;
            return this;
        }

        public b c0(Integer num) {
            this.q = num;
            return this;
        }

        public b d0(Integer num) {
            this.v = num;
            return this;
        }

        public b e0(Integer num) {
            this.u = num;
            return this;
        }

        public b f0(Integer num) {
            this.t = num;
            return this;
        }

        public b g0(CharSequence charSequence) {
            this.D = charSequence;
            return this;
        }

        public b h0(CharSequence charSequence) {
            this.f = charSequence;
            return this;
        }

        public b i0(CharSequence charSequence) {
            this.a = charSequence;
            return this;
        }

        public b j0(Integer num) {
            this.A = num;
            return this;
        }

        public b k0(Integer num) {
            this.n = num;
            return this;
        }

        public b l0(Integer num) {
            this.m = num;
            return this;
        }

        public b m0(r rVar) {
            this.h = rVar;
            return this;
        }

        public b n0(CharSequence charSequence) {
            this.w = charSequence;
            return this;
        }

        public b() {
        }

        public b(n nVar) {
            this.a = nVar.a;
            this.b = nVar.f0;
            this.c = nVar.g0;
            this.d = nVar.h0;
            this.e = nVar.i0;
            this.f = nVar.j0;
            this.g = nVar.k0;
            this.h = nVar.l0;
            this.i = nVar.m0;
            this.j = nVar.n0;
            this.k = nVar.o0;
            this.l = nVar.p0;
            this.m = nVar.q0;
            this.n = nVar.r0;
            this.o = nVar.s0;
            this.p = nVar.t0;
            this.q = nVar.v0;
            this.r = nVar.w0;
            this.s = nVar.x0;
            this.t = nVar.y0;
            this.u = nVar.z0;
            this.v = nVar.A0;
            this.w = nVar.B0;
            this.x = nVar.C0;
            this.y = nVar.D0;
            this.z = nVar.E0;
            this.A = nVar.F0;
            this.B = nVar.G0;
            this.C = nVar.H0;
            this.D = nVar.I0;
            this.E = nVar.J0;
        }
    }

    public static n c(Bundle bundle) {
        Bundle bundle2;
        Bundle bundle3;
        b bVar = new b();
        bVar.i0(bundle.getCharSequence(d(0))).M(bundle.getCharSequence(d(1))).L(bundle.getCharSequence(d(2))).K(bundle.getCharSequence(d(3))).U(bundle.getCharSequence(d(4))).h0(bundle.getCharSequence(d(5))).S(bundle.getCharSequence(d(6))).N(bundle.getByteArray(d(10)), bundle.containsKey(d(29)) ? Integer.valueOf(bundle.getInt(d(29))) : null).O((Uri) bundle.getParcelable(d(11))).n0(bundle.getCharSequence(d(22))).Q(bundle.getCharSequence(d(23))).R(bundle.getCharSequence(d(24))).X(bundle.getCharSequence(d(27))).P(bundle.getCharSequence(d(28))).g0(bundle.getCharSequence(d(30))).V(bundle.getBundle(d(1000)));
        if (bundle.containsKey(d(8)) && (bundle3 = bundle.getBundle(d(8))) != null) {
            bVar.m0(r.a.a(bundle3));
        }
        if (bundle.containsKey(d(9)) && (bundle2 = bundle.getBundle(d(9))) != null) {
            bVar.Z(r.a.a(bundle2));
        }
        if (bundle.containsKey(d(12))) {
            bVar.l0(Integer.valueOf(bundle.getInt(d(12))));
        }
        if (bundle.containsKey(d(13))) {
            bVar.k0(Integer.valueOf(bundle.getInt(d(13))));
        }
        if (bundle.containsKey(d(14))) {
            bVar.W(Integer.valueOf(bundle.getInt(d(14))));
        }
        if (bundle.containsKey(d(15))) {
            bVar.Y(Boolean.valueOf(bundle.getBoolean(d(15))));
        }
        if (bundle.containsKey(d(16))) {
            bVar.c0(Integer.valueOf(bundle.getInt(d(16))));
        }
        if (bundle.containsKey(d(17))) {
            bVar.b0(Integer.valueOf(bundle.getInt(d(17))));
        }
        if (bundle.containsKey(d(18))) {
            bVar.a0(Integer.valueOf(bundle.getInt(d(18))));
        }
        if (bundle.containsKey(d(19))) {
            bVar.f0(Integer.valueOf(bundle.getInt(d(19))));
        }
        if (bundle.containsKey(d(20))) {
            bVar.e0(Integer.valueOf(bundle.getInt(d(20))));
        }
        if (bundle.containsKey(d(21))) {
            bVar.d0(Integer.valueOf(bundle.getInt(d(21))));
        }
        if (bundle.containsKey(d(25))) {
            bVar.T(Integer.valueOf(bundle.getInt(d(25))));
        }
        if (bundle.containsKey(d(26))) {
            bVar.j0(Integer.valueOf(bundle.getInt(d(26))));
        }
        return bVar.F();
    }

    public static String d(int i) {
        return Integer.toString(i, 36);
    }

    public b b() {
        return new b();
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null || n.class != obj.getClass()) {
            return false;
        }
        n nVar = (n) obj;
        return androidx.media3.common.util.b.c(this.a, nVar.a) && androidx.media3.common.util.b.c(this.f0, nVar.f0) && androidx.media3.common.util.b.c(this.g0, nVar.g0) && androidx.media3.common.util.b.c(this.h0, nVar.h0) && androidx.media3.common.util.b.c(this.i0, nVar.i0) && androidx.media3.common.util.b.c(this.j0, nVar.j0) && androidx.media3.common.util.b.c(this.k0, nVar.k0) && androidx.media3.common.util.b.c(this.l0, nVar.l0) && androidx.media3.common.util.b.c(this.m0, nVar.m0) && Arrays.equals(this.n0, nVar.n0) && androidx.media3.common.util.b.c(this.o0, nVar.o0) && androidx.media3.common.util.b.c(this.p0, nVar.p0) && androidx.media3.common.util.b.c(this.q0, nVar.q0) && androidx.media3.common.util.b.c(this.r0, nVar.r0) && androidx.media3.common.util.b.c(this.s0, nVar.s0) && androidx.media3.common.util.b.c(this.t0, nVar.t0) && androidx.media3.common.util.b.c(this.v0, nVar.v0) && androidx.media3.common.util.b.c(this.w0, nVar.w0) && androidx.media3.common.util.b.c(this.x0, nVar.x0) && androidx.media3.common.util.b.c(this.y0, nVar.y0) && androidx.media3.common.util.b.c(this.z0, nVar.z0) && androidx.media3.common.util.b.c(this.A0, nVar.A0) && androidx.media3.common.util.b.c(this.B0, nVar.B0) && androidx.media3.common.util.b.c(this.C0, nVar.C0) && androidx.media3.common.util.b.c(this.D0, nVar.D0) && androidx.media3.common.util.b.c(this.E0, nVar.E0) && androidx.media3.common.util.b.c(this.F0, nVar.F0) && androidx.media3.common.util.b.c(this.G0, nVar.G0) && androidx.media3.common.util.b.c(this.H0, nVar.H0) && androidx.media3.common.util.b.c(this.I0, nVar.I0);
    }

    public int hashCode() {
        return ql2.b(this.a, this.f0, this.g0, this.h0, this.i0, this.j0, this.k0, this.l0, this.m0, Integer.valueOf(Arrays.hashCode(this.n0)), this.o0, this.p0, this.q0, this.r0, this.s0, this.t0, this.v0, this.w0, this.x0, this.y0, this.z0, this.A0, this.B0, this.C0, this.D0, this.E0, this.F0, this.G0, this.H0, this.I0);
    }

    @Override // androidx.media3.common.e
    public Bundle toBundle() {
        Bundle bundle = new Bundle();
        bundle.putCharSequence(d(0), this.a);
        bundle.putCharSequence(d(1), this.f0);
        bundle.putCharSequence(d(2), this.g0);
        bundle.putCharSequence(d(3), this.h0);
        bundle.putCharSequence(d(4), this.i0);
        bundle.putCharSequence(d(5), this.j0);
        bundle.putCharSequence(d(6), this.k0);
        bundle.putByteArray(d(10), this.n0);
        bundle.putParcelable(d(11), this.p0);
        bundle.putCharSequence(d(22), this.B0);
        bundle.putCharSequence(d(23), this.C0);
        bundle.putCharSequence(d(24), this.D0);
        bundle.putCharSequence(d(27), this.G0);
        bundle.putCharSequence(d(28), this.H0);
        bundle.putCharSequence(d(30), this.I0);
        if (this.l0 != null) {
            bundle.putBundle(d(8), this.l0.toBundle());
        }
        if (this.m0 != null) {
            bundle.putBundle(d(9), this.m0.toBundle());
        }
        if (this.q0 != null) {
            bundle.putInt(d(12), this.q0.intValue());
        }
        if (this.r0 != null) {
            bundle.putInt(d(13), this.r0.intValue());
        }
        if (this.s0 != null) {
            bundle.putInt(d(14), this.s0.intValue());
        }
        if (this.t0 != null) {
            bundle.putBoolean(d(15), this.t0.booleanValue());
        }
        if (this.v0 != null) {
            bundle.putInt(d(16), this.v0.intValue());
        }
        if (this.w0 != null) {
            bundle.putInt(d(17), this.w0.intValue());
        }
        if (this.x0 != null) {
            bundle.putInt(d(18), this.x0.intValue());
        }
        if (this.y0 != null) {
            bundle.putInt(d(19), this.y0.intValue());
        }
        if (this.z0 != null) {
            bundle.putInt(d(20), this.z0.intValue());
        }
        if (this.A0 != null) {
            bundle.putInt(d(21), this.A0.intValue());
        }
        if (this.E0 != null) {
            bundle.putInt(d(25), this.E0.intValue());
        }
        if (this.F0 != null) {
            bundle.putInt(d(26), this.F0.intValue());
        }
        if (this.o0 != null) {
            bundle.putInt(d(29), this.o0.intValue());
        }
        if (this.J0 != null) {
            bundle.putBundle(d(1000), this.J0);
        }
        return bundle;
    }

    public n(b bVar) {
        this.a = bVar.a;
        this.f0 = bVar.b;
        this.g0 = bVar.c;
        this.h0 = bVar.d;
        this.i0 = bVar.e;
        this.j0 = bVar.f;
        this.k0 = bVar.g;
        this.l0 = bVar.h;
        this.m0 = bVar.i;
        this.n0 = bVar.j;
        this.o0 = bVar.k;
        this.p0 = bVar.l;
        this.q0 = bVar.m;
        this.r0 = bVar.n;
        this.s0 = bVar.o;
        this.t0 = bVar.p;
        this.u0 = bVar.q;
        this.v0 = bVar.q;
        this.w0 = bVar.r;
        this.x0 = bVar.s;
        this.y0 = bVar.t;
        this.z0 = bVar.u;
        this.A0 = bVar.v;
        this.B0 = bVar.w;
        this.C0 = bVar.x;
        this.D0 = bVar.y;
        this.E0 = bVar.z;
        this.F0 = bVar.A;
        this.G0 = bVar.B;
        this.H0 = bVar.C;
        this.I0 = bVar.D;
        this.J0 = bVar.E;
    }
}
