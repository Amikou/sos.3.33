package androidx.media3.common;

import android.util.SparseBooleanArray;

/* compiled from: FlagSet.java */
/* loaded from: classes.dex */
public final class i {
    public final SparseBooleanArray a;

    /* compiled from: FlagSet.java */
    /* loaded from: classes.dex */
    public static final class b {
        public final SparseBooleanArray a = new SparseBooleanArray();
        public boolean b;

        public b a(int i) {
            ii.g(!this.b);
            this.a.append(i, true);
            return this;
        }

        public b b(i iVar) {
            for (int i = 0; i < iVar.d(); i++) {
                a(iVar.c(i));
            }
            return this;
        }

        public b c(int... iArr) {
            for (int i : iArr) {
                a(i);
            }
            return this;
        }

        public b d(int i, boolean z) {
            return z ? a(i) : this;
        }

        public i e() {
            ii.g(!this.b);
            this.b = true;
            return new i(this.a);
        }
    }

    public boolean a(int i) {
        return this.a.get(i);
    }

    public boolean b(int... iArr) {
        for (int i : iArr) {
            if (a(i)) {
                return true;
            }
        }
        return false;
    }

    public int c(int i) {
        ii.c(i, 0, d());
        return this.a.keyAt(i);
    }

    public int d() {
        return this.a.size();
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj instanceof i) {
            i iVar = (i) obj;
            if (androidx.media3.common.util.b.a < 24) {
                if (d() != iVar.d()) {
                    return false;
                }
                for (int i = 0; i < d(); i++) {
                    if (c(i) != iVar.c(i)) {
                        return false;
                    }
                }
                return true;
            }
            return this.a.equals(iVar.a);
        }
        return false;
    }

    public int hashCode() {
        if (androidx.media3.common.util.b.a < 24) {
            int d = d();
            for (int i = 0; i < d(); i++) {
                d = (d * 31) + c(i);
            }
            return d;
        }
        return this.a.hashCode();
    }

    public i(SparseBooleanArray sparseBooleanArray) {
        this.a = sparseBooleanArray;
    }
}
