package androidx.media3.common;

import android.os.Bundle;

/* compiled from: DeviceInfo.java */
/* loaded from: classes.dex */
public final class h implements e {
    public final int a;
    public final int f0;
    public final int g0;

    static {
        new h(0, 0, 0);
    }

    public h(int i, int i2, int i3) {
        this.a = i;
        this.f0 = i2;
        this.g0 = i3;
    }

    public static String a(int i) {
        return Integer.toString(i, 36);
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj instanceof h) {
            h hVar = (h) obj;
            return this.a == hVar.a && this.f0 == hVar.f0 && this.g0 == hVar.g0;
        }
        return false;
    }

    public int hashCode() {
        return ((((527 + this.a) * 31) + this.f0) * 31) + this.g0;
    }

    @Override // androidx.media3.common.e
    public Bundle toBundle() {
        Bundle bundle = new Bundle();
        bundle.putInt(a(0), this.a);
        bundle.putInt(a(1), this.f0);
        bundle.putInt(a(2), this.g0);
        return bundle;
    }
}
