package androidx.media3.common;

import android.os.Binder;
import android.os.Bundle;
import android.os.IBinder;
import android.os.Parcel;
import android.os.RemoteException;
import com.google.common.collect.ImmutableList;
import java.util.Collection;
import java.util.List;

/* compiled from: BundleListRetriever.java */
/* loaded from: classes.dex */
public final class d extends Binder {
    public static final int b;
    public final ImmutableList<Bundle> a;

    static {
        b = androidx.media3.common.util.b.a >= 30 ? IBinder.getSuggestedMaxIpcSizeBytes() : 65536;
    }

    public d(List<Bundle> list) {
        this.a = ImmutableList.copyOf((Collection) list);
    }

    @Override // android.os.Binder
    public boolean onTransact(int i, Parcel parcel, Parcel parcel2, int i2) throws RemoteException {
        if (i != 1) {
            return super.onTransact(i, parcel, parcel2, i2);
        }
        if (parcel2 == null) {
            return false;
        }
        int size = this.a.size();
        int readInt = parcel.readInt();
        while (readInt < size && parcel2.dataSize() < b) {
            parcel2.writeInt(1);
            parcel2.writeBundle(this.a.get(readInt));
            readInt++;
        }
        parcel2.writeInt(readInt < size ? 2 : 0);
        return true;
    }
}
