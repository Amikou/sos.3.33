package androidx.media3.common;

import androidx.media3.common.u;
import java.util.Collections;
import java.util.List;
import zendesk.support.request.CellBase;

/* compiled from: BasePlayer.java */
/* loaded from: classes.dex */
public abstract class c implements q {
    public final u.c a = new u.c();

    @Override // androidx.media3.common.q
    public final void A(m mVar) {
        p0(Collections.singletonList(mVar));
    }

    @Override // androidx.media3.common.q
    public final boolean D() {
        return g0() != -1;
    }

    @Override // androidx.media3.common.q
    public final boolean E() {
        return B() == 3 && k() && P() == 0;
    }

    @Override // androidx.media3.common.q
    public final boolean J(int i) {
        return j().b(i);
    }

    @Override // androidx.media3.common.q
    public final boolean N() {
        u S = S();
        return !S.q() && S.n(I(), this.a).m0;
    }

    @Override // androidx.media3.common.q
    public final void X() {
        if (S().q() || g()) {
            return;
        }
        if (D()) {
            m0();
        } else if (e0() && N()) {
            k0();
        }
    }

    @Override // androidx.media3.common.q
    public final void Y() {
        n0(x());
    }

    @Override // androidx.media3.common.q
    public final void a0() {
        n0(-d0());
    }

    @Override // androidx.media3.common.q
    public final void c() {
        w(false);
    }

    @Override // androidx.media3.common.q
    public final boolean e0() {
        u S = S();
        return !S.q() && S.n(I(), this.a).i();
    }

    @Override // androidx.media3.common.q
    public final void f() {
        w(true);
    }

    public final long f0() {
        u S = S();
        return S.q() ? CellBase.ID_SYSTEM_MESSAGE_REQUEST_CLOSED : S.n(I(), this.a).g();
    }

    public final int g0() {
        u S = S();
        if (S.q()) {
            return -1;
        }
        return S.e(I(), i0(), U());
    }

    public final int h0() {
        u S = S();
        if (S.q()) {
            return -1;
        }
        return S.l(I(), i0(), U());
    }

    public final int i0() {
        int Q = Q();
        if (Q == 1) {
            return 0;
        }
        return Q;
    }

    public final void j0(long j) {
        i(I(), j);
    }

    public final void k0() {
        l0(I());
    }

    public final void l0(int i) {
        i(i, CellBase.ID_SYSTEM_MESSAGE_REQUEST_CLOSED);
    }

    public final void m0() {
        int g0 = g0();
        if (g0 != -1) {
            l0(g0);
        }
    }

    public final void n0(long j) {
        long c0 = c0() + j;
        long R = R();
        if (R != CellBase.ID_SYSTEM_MESSAGE_REQUEST_CLOSED) {
            c0 = Math.min(c0, R);
        }
        j0(Math.max(c0, 0L));
    }

    public final void o0() {
        int h0 = h0();
        if (h0 != -1) {
            l0(h0);
        }
    }

    public final void p0(List<m> list) {
        q(list, true);
    }

    @Override // androidx.media3.common.q
    public final boolean r() {
        return h0() != -1;
    }

    @Override // androidx.media3.common.q
    public final void u() {
        if (S().q() || g()) {
            return;
        }
        boolean r = r();
        if (e0() && !z()) {
            if (r) {
                o0();
            }
        } else if (r && c0() <= m()) {
            o0();
        } else {
            j0(0L);
        }
    }

    @Override // androidx.media3.common.q
    public final boolean z() {
        u S = S();
        return !S.q() && S.n(I(), this.a).l0;
    }
}
