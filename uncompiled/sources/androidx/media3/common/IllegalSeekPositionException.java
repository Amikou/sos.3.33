package androidx.media3.common;

/* loaded from: classes.dex */
public final class IllegalSeekPositionException extends IllegalStateException {
    public final long positionMs;
    public final u timeline;
    public final int windowIndex;

    public IllegalSeekPositionException(u uVar, int i, long j) {
        this.timeline = uVar;
        this.windowIndex = i;
        this.positionMs = j;
    }
}
