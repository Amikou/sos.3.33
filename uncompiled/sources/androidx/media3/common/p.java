package androidx.media3.common;

import android.os.Bundle;
import com.github.mikephil.charting.utils.Utils;

/* compiled from: PlaybackParameters.java */
/* loaded from: classes.dex */
public final class p implements e {
    public static final p h0 = new p(1.0f);
    public final float a;
    public final float f0;
    public final int g0;

    public p(float f) {
        this(f, 1.0f);
    }

    public static String b(int i) {
        return Integer.toString(i, 36);
    }

    public long a(long j) {
        return j * this.g0;
    }

    public p c(float f) {
        return new p(f, this.f0);
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null || p.class != obj.getClass()) {
            return false;
        }
        p pVar = (p) obj;
        return this.a == pVar.a && this.f0 == pVar.f0;
    }

    public int hashCode() {
        return ((527 + Float.floatToRawIntBits(this.a)) * 31) + Float.floatToRawIntBits(this.f0);
    }

    @Override // androidx.media3.common.e
    public Bundle toBundle() {
        Bundle bundle = new Bundle();
        bundle.putFloat(b(0), this.a);
        bundle.putFloat(b(1), this.f0);
        return bundle;
    }

    public String toString() {
        return androidx.media3.common.util.b.A("PlaybackParameters(speed=%.2f, pitch=%.2f)", Float.valueOf(this.a), Float.valueOf(this.f0));
    }

    public p(float f, float f2) {
        ii.a(f > Utils.FLOAT_EPSILON);
        ii.a(f2 > Utils.FLOAT_EPSILON);
        this.a = f;
        this.f0 = f2;
        this.g0 = Math.round(f * 1000.0f);
    }
}
