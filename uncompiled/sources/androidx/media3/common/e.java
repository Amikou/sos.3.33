package androidx.media3.common;

import android.os.Bundle;

/* compiled from: Bundleable.java */
/* loaded from: classes.dex */
public interface e {

    /* compiled from: Bundleable.java */
    /* loaded from: classes.dex */
    public interface a<T extends e> {
        T a(Bundle bundle);
    }

    Bundle toBundle();
}
