package androidx.media3.common;

import android.net.Uri;
import android.os.Bundle;
import androidx.media3.common.e;
import com.github.mikephil.charting.utils.Utils;
import com.google.common.collect.ImmutableList;
import com.google.common.collect.ImmutableMap;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.UUID;
import zendesk.support.request.CellBase;

/* compiled from: MediaItem.java */
/* loaded from: classes.dex */
public final class m implements androidx.media3.common.e {
    public static final m k0 = new c().a();
    public static final e.a<m> l0 = b62.a;
    public final String a;
    public final h f0;
    public final g g0;
    public final n h0;
    public final d i0;
    public final j j0;

    /* compiled from: MediaItem.java */
    /* loaded from: classes.dex */
    public static final class b {
    }

    /* compiled from: MediaItem.java */
    /* loaded from: classes.dex */
    public static final class c {
        public String a;
        public Uri b;
        public String c;
        public d.a d;
        public f.a e;
        public List<StreamKey> f;
        public String g;
        public ImmutableList<l> h;
        public b i;
        public Object j;
        public n k;
        public g.a l;
        public j m;

        public m a() {
            i iVar;
            ii.g(this.e.b == null || this.e.a != null);
            Uri uri = this.b;
            if (uri != null) {
                iVar = new i(uri, this.c, this.e.a != null ? this.e.i() : null, this.i, this.f, this.g, this.h, this.j);
            } else {
                iVar = null;
            }
            String str = this.a;
            if (str == null) {
                str = "";
            }
            String str2 = str;
            e g = this.d.g();
            g f = this.l.f();
            n nVar = this.k;
            if (nVar == null) {
                nVar = n.K0;
            }
            return new m(str2, g, iVar, f, nVar, this.m);
        }

        public c b(String str) {
            this.g = str;
            return this;
        }

        public c c(g gVar) {
            this.l = gVar.b();
            return this;
        }

        public c d(String str) {
            this.a = (String) ii.e(str);
            return this;
        }

        public c e(List<l> list) {
            this.h = ImmutableList.copyOf((Collection) list);
            return this;
        }

        public c f(Object obj) {
            this.j = obj;
            return this;
        }

        public c g(Uri uri) {
            this.b = uri;
            return this;
        }

        public c h(String str) {
            return g(str == null ? null : Uri.parse(str));
        }

        public c() {
            this.d = new d.a();
            this.e = new f.a();
            this.f = Collections.emptyList();
            this.h = ImmutableList.of();
            this.l = new g.a();
            this.m = j.h0;
        }

        public c(m mVar) {
            this();
            f.a aVar;
            this.d = mVar.i0.b();
            this.a = mVar.a;
            this.k = mVar.h0;
            this.l = mVar.g0.b();
            this.m = mVar.j0;
            h hVar = mVar.f0;
            if (hVar != null) {
                this.g = hVar.f;
                this.c = hVar.b;
                this.b = hVar.a;
                this.f = hVar.e;
                this.h = hVar.g;
                this.j = hVar.h;
                f fVar = hVar.c;
                if (fVar != null) {
                    aVar = fVar.b();
                } else {
                    aVar = new f.a();
                }
                this.e = aVar;
            }
        }
    }

    /* compiled from: MediaItem.java */
    /* loaded from: classes.dex */
    public static class d implements androidx.media3.common.e {
        public static final e.a<e> j0;
        public final long a;
        public final long f0;
        public final boolean g0;
        public final boolean h0;
        public final boolean i0;

        /* compiled from: MediaItem.java */
        /* loaded from: classes.dex */
        public static final class a {
            public long a;
            public long b;
            public boolean c;
            public boolean d;
            public boolean e;

            public d f() {
                return g();
            }

            @Deprecated
            public e g() {
                return new e(this);
            }

            public a h(long j) {
                ii.a(j == Long.MIN_VALUE || j >= 0);
                this.b = j;
                return this;
            }

            public a i(boolean z) {
                this.d = z;
                return this;
            }

            public a j(boolean z) {
                this.c = z;
                return this;
            }

            public a k(long j) {
                ii.a(j >= 0);
                this.a = j;
                return this;
            }

            public a l(boolean z) {
                this.e = z;
                return this;
            }

            public a() {
                this.b = Long.MIN_VALUE;
            }

            public a(d dVar) {
                this.a = dVar.a;
                this.b = dVar.f0;
                this.c = dVar.g0;
                this.d = dVar.h0;
                this.e = dVar.i0;
            }
        }

        static {
            new a().f();
            j0 = c62.a;
        }

        public static String c(int i) {
            return Integer.toString(i, 36);
        }

        public static /* synthetic */ e d(Bundle bundle) {
            return new a().k(bundle.getLong(c(0), 0L)).h(bundle.getLong(c(1), Long.MIN_VALUE)).j(bundle.getBoolean(c(2), false)).i(bundle.getBoolean(c(3), false)).l(bundle.getBoolean(c(4), false)).g();
        }

        public a b() {
            return new a();
        }

        public boolean equals(Object obj) {
            if (this == obj) {
                return true;
            }
            if (obj instanceof d) {
                d dVar = (d) obj;
                return this.a == dVar.a && this.f0 == dVar.f0 && this.g0 == dVar.g0 && this.h0 == dVar.h0 && this.i0 == dVar.i0;
            }
            return false;
        }

        public int hashCode() {
            long j = this.a;
            long j2 = this.f0;
            return (((((((((int) (j ^ (j >>> 32))) * 31) + ((int) ((j2 >>> 32) ^ j2))) * 31) + (this.g0 ? 1 : 0)) * 31) + (this.h0 ? 1 : 0)) * 31) + (this.i0 ? 1 : 0);
        }

        @Override // androidx.media3.common.e
        public Bundle toBundle() {
            Bundle bundle = new Bundle();
            bundle.putLong(c(0), this.a);
            bundle.putLong(c(1), this.f0);
            bundle.putBoolean(c(2), this.g0);
            bundle.putBoolean(c(3), this.h0);
            bundle.putBoolean(c(4), this.i0);
            return bundle;
        }

        public d(a aVar) {
            this.a = aVar.a;
            this.f0 = aVar.b;
            this.g0 = aVar.c;
            this.h0 = aVar.d;
            this.i0 = aVar.e;
        }
    }

    /* compiled from: MediaItem.java */
    @Deprecated
    /* loaded from: classes.dex */
    public static final class e extends d {
        public static final e k0 = new d.a().g();

        public e(d.a aVar) {
            super(aVar);
        }
    }

    /* compiled from: MediaItem.java */
    /* loaded from: classes.dex */
    public static final class f {
        public final UUID a;
        public final Uri b;
        public final ImmutableMap<String, String> c;
        public final boolean d;
        public final boolean e;
        public final boolean f;
        public final ImmutableList<Integer> g;
        public final byte[] h;

        public a b() {
            return new a();
        }

        public byte[] c() {
            byte[] bArr = this.h;
            if (bArr != null) {
                return Arrays.copyOf(bArr, bArr.length);
            }
            return null;
        }

        public boolean equals(Object obj) {
            if (this == obj) {
                return true;
            }
            if (obj instanceof f) {
                f fVar = (f) obj;
                return this.a.equals(fVar.a) && androidx.media3.common.util.b.c(this.b, fVar.b) && androidx.media3.common.util.b.c(this.c, fVar.c) && this.d == fVar.d && this.f == fVar.f && this.e == fVar.e && this.g.equals(fVar.g) && Arrays.equals(this.h, fVar.h);
            }
            return false;
        }

        public int hashCode() {
            int hashCode = this.a.hashCode() * 31;
            Uri uri = this.b;
            return ((((((((((((hashCode + (uri != null ? uri.hashCode() : 0)) * 31) + this.c.hashCode()) * 31) + (this.d ? 1 : 0)) * 31) + (this.f ? 1 : 0)) * 31) + (this.e ? 1 : 0)) * 31) + this.g.hashCode()) * 31) + Arrays.hashCode(this.h);
        }

        /* compiled from: MediaItem.java */
        /* loaded from: classes.dex */
        public static final class a {
            public UUID a;
            public Uri b;
            public ImmutableMap<String, String> c;
            public boolean d;
            public boolean e;
            public boolean f;
            public ImmutableList<Integer> g;
            public byte[] h;

            public f i() {
                return new f(this);
            }

            @Deprecated
            public a() {
                this.c = ImmutableMap.of();
                this.g = ImmutableList.of();
            }

            public a(f fVar) {
                this.a = fVar.a;
                this.b = fVar.b;
                this.c = fVar.c;
                this.d = fVar.d;
                this.e = fVar.e;
                this.f = fVar.f;
                this.g = fVar.g;
                this.h = fVar.h;
            }
        }

        public f(a aVar) {
            ii.g((aVar.f && aVar.b == null) ? false : true);
            this.a = (UUID) ii.e(aVar.a);
            this.b = aVar.b;
            ImmutableMap unused = aVar.c;
            this.c = aVar.c;
            this.d = aVar.d;
            this.f = aVar.f;
            this.e = aVar.e;
            ImmutableList unused2 = aVar.g;
            this.g = aVar.g;
            this.h = aVar.h != null ? Arrays.copyOf(aVar.h, aVar.h.length) : null;
        }
    }

    /* compiled from: MediaItem.java */
    /* loaded from: classes.dex */
    public static final class g implements androidx.media3.common.e {
        public static final g j0 = new a().f();
        public static final e.a<g> k0 = d62.a;
        public final long a;
        public final long f0;
        public final long g0;
        public final float h0;
        public final float i0;

        /* compiled from: MediaItem.java */
        /* loaded from: classes.dex */
        public static final class a {
            public long a;
            public long b;
            public long c;
            public float d;
            public float e;

            public g f() {
                return new g(this);
            }

            public a g(long j) {
                this.c = j;
                return this;
            }

            public a h(float f) {
                this.e = f;
                return this;
            }

            public a i(long j) {
                this.b = j;
                return this;
            }

            public a j(float f) {
                this.d = f;
                return this;
            }

            public a k(long j) {
                this.a = j;
                return this;
            }

            public a() {
                this.a = CellBase.ID_SYSTEM_MESSAGE_REQUEST_CLOSED;
                this.b = CellBase.ID_SYSTEM_MESSAGE_REQUEST_CLOSED;
                this.c = CellBase.ID_SYSTEM_MESSAGE_REQUEST_CLOSED;
                this.d = -3.4028235E38f;
                this.e = -3.4028235E38f;
            }

            public a(g gVar) {
                this.a = gVar.a;
                this.b = gVar.f0;
                this.c = gVar.g0;
                this.d = gVar.h0;
                this.e = gVar.i0;
            }
        }

        public static String c(int i) {
            return Integer.toString(i, 36);
        }

        public static /* synthetic */ g d(Bundle bundle) {
            return new g(bundle.getLong(c(0), CellBase.ID_SYSTEM_MESSAGE_REQUEST_CLOSED), bundle.getLong(c(1), CellBase.ID_SYSTEM_MESSAGE_REQUEST_CLOSED), bundle.getLong(c(2), CellBase.ID_SYSTEM_MESSAGE_REQUEST_CLOSED), bundle.getFloat(c(3), -3.4028235E38f), bundle.getFloat(c(4), -3.4028235E38f));
        }

        public a b() {
            return new a();
        }

        public boolean equals(Object obj) {
            if (this == obj) {
                return true;
            }
            if (obj instanceof g) {
                g gVar = (g) obj;
                return this.a == gVar.a && this.f0 == gVar.f0 && this.g0 == gVar.g0 && this.h0 == gVar.h0 && this.i0 == gVar.i0;
            }
            return false;
        }

        public int hashCode() {
            long j = this.a;
            long j2 = this.f0;
            long j3 = this.g0;
            int i = ((((((int) (j ^ (j >>> 32))) * 31) + ((int) (j2 ^ (j2 >>> 32)))) * 31) + ((int) ((j3 >>> 32) ^ j3))) * 31;
            float f = this.h0;
            int floatToIntBits = (i + (f != Utils.FLOAT_EPSILON ? Float.floatToIntBits(f) : 0)) * 31;
            float f2 = this.i0;
            return floatToIntBits + (f2 != Utils.FLOAT_EPSILON ? Float.floatToIntBits(f2) : 0);
        }

        @Override // androidx.media3.common.e
        public Bundle toBundle() {
            Bundle bundle = new Bundle();
            bundle.putLong(c(0), this.a);
            bundle.putLong(c(1), this.f0);
            bundle.putLong(c(2), this.g0);
            bundle.putFloat(c(3), this.h0);
            bundle.putFloat(c(4), this.i0);
            return bundle;
        }

        public g(a aVar) {
            this(aVar.a, aVar.b, aVar.c, aVar.d, aVar.e);
        }

        @Deprecated
        public g(long j, long j2, long j3, float f, float f2) {
            this.a = j;
            this.f0 = j2;
            this.g0 = j3;
            this.h0 = f;
            this.i0 = f2;
        }
    }

    /* compiled from: MediaItem.java */
    /* loaded from: classes.dex */
    public static class h {
        public final Uri a;
        public final String b;
        public final f c;
        public final b d;
        public final List<StreamKey> e;
        public final String f;
        public final ImmutableList<l> g;
        public final Object h;

        public boolean equals(Object obj) {
            if (this == obj) {
                return true;
            }
            if (obj instanceof h) {
                h hVar = (h) obj;
                return this.a.equals(hVar.a) && androidx.media3.common.util.b.c(this.b, hVar.b) && androidx.media3.common.util.b.c(this.c, hVar.c) && androidx.media3.common.util.b.c(this.d, hVar.d) && this.e.equals(hVar.e) && androidx.media3.common.util.b.c(this.f, hVar.f) && this.g.equals(hVar.g) && androidx.media3.common.util.b.c(this.h, hVar.h);
            }
            return false;
        }

        public int hashCode() {
            int hashCode = this.a.hashCode() * 31;
            String str = this.b;
            int hashCode2 = (hashCode + (str == null ? 0 : str.hashCode())) * 31;
            f fVar = this.c;
            int hashCode3 = (((((hashCode2 + (fVar == null ? 0 : fVar.hashCode())) * 31) + 0) * 31) + this.e.hashCode()) * 31;
            String str2 = this.f;
            int hashCode4 = (((hashCode3 + (str2 == null ? 0 : str2.hashCode())) * 31) + this.g.hashCode()) * 31;
            Object obj = this.h;
            return hashCode4 + (obj != null ? obj.hashCode() : 0);
        }

        public h(Uri uri, String str, f fVar, b bVar, List<StreamKey> list, String str2, ImmutableList<l> immutableList, Object obj) {
            this.a = uri;
            this.b = str;
            this.c = fVar;
            this.e = list;
            this.f = str2;
            this.g = immutableList;
            ImmutableList.a builder = ImmutableList.builder();
            for (int i = 0; i < immutableList.size(); i++) {
                builder.a(immutableList.get(i).a().i());
            }
            builder.l();
            this.h = obj;
        }
    }

    /* compiled from: MediaItem.java */
    @Deprecated
    /* loaded from: classes.dex */
    public static final class i extends h {
        public i(Uri uri, String str, f fVar, b bVar, List<StreamKey> list, String str2, ImmutableList<l> immutableList, Object obj) {
            super(uri, str, fVar, bVar, list, str2, immutableList, obj);
        }
    }

    /* compiled from: MediaItem.java */
    /* loaded from: classes.dex */
    public static final class j implements androidx.media3.common.e {
        public static final j h0 = new a().d();
        public static final e.a<j> i0 = e62.a;
        public final Uri a;
        public final String f0;
        public final Bundle g0;

        /* compiled from: MediaItem.java */
        /* loaded from: classes.dex */
        public static final class a {
            public Uri a;
            public String b;
            public Bundle c;

            public j d() {
                return new j(this);
            }

            public a e(Bundle bundle) {
                this.c = bundle;
                return this;
            }

            public a f(Uri uri) {
                this.a = uri;
                return this;
            }

            public a g(String str) {
                this.b = str;
                return this;
            }
        }

        public static String b(int i) {
            return Integer.toString(i, 36);
        }

        public static /* synthetic */ j c(Bundle bundle) {
            return new a().f((Uri) bundle.getParcelable(b(0))).g(bundle.getString(b(1))).e(bundle.getBundle(b(2))).d();
        }

        public boolean equals(Object obj) {
            if (this == obj) {
                return true;
            }
            if (obj instanceof j) {
                j jVar = (j) obj;
                return androidx.media3.common.util.b.c(this.a, jVar.a) && androidx.media3.common.util.b.c(this.f0, jVar.f0);
            }
            return false;
        }

        public int hashCode() {
            Uri uri = this.a;
            int hashCode = (uri == null ? 0 : uri.hashCode()) * 31;
            String str = this.f0;
            return hashCode + (str != null ? str.hashCode() : 0);
        }

        @Override // androidx.media3.common.e
        public Bundle toBundle() {
            Bundle bundle = new Bundle();
            if (this.a != null) {
                bundle.putParcelable(b(0), this.a);
            }
            if (this.f0 != null) {
                bundle.putString(b(1), this.f0);
            }
            if (this.g0 != null) {
                bundle.putBundle(b(2), this.g0);
            }
            return bundle;
        }

        public j(a aVar) {
            this.a = aVar.a;
            this.f0 = aVar.b;
            this.g0 = aVar.c;
        }
    }

    /* compiled from: MediaItem.java */
    @Deprecated
    /* loaded from: classes.dex */
    public static final class k extends l {
        public k(l.a aVar) {
            super(aVar);
        }
    }

    /* compiled from: MediaItem.java */
    /* loaded from: classes.dex */
    public static class l {
        public final Uri a;
        public final String b;
        public final String c;
        public final int d;
        public final int e;
        public final String f;
        public final String g;

        /* compiled from: MediaItem.java */
        /* loaded from: classes.dex */
        public static final class a {
            public Uri a;
            public String b;
            public String c;
            public int d;
            public int e;
            public String f;
            public String g;

            public final k i() {
                return new k(this);
            }

            public a(l lVar) {
                this.a = lVar.a;
                this.b = lVar.b;
                this.c = lVar.c;
                this.d = lVar.d;
                this.e = lVar.e;
                this.f = lVar.f;
                this.g = lVar.g;
            }
        }

        public a a() {
            return new a();
        }

        public boolean equals(Object obj) {
            if (this == obj) {
                return true;
            }
            if (obj instanceof l) {
                l lVar = (l) obj;
                return this.a.equals(lVar.a) && androidx.media3.common.util.b.c(this.b, lVar.b) && androidx.media3.common.util.b.c(this.c, lVar.c) && this.d == lVar.d && this.e == lVar.e && androidx.media3.common.util.b.c(this.f, lVar.f) && androidx.media3.common.util.b.c(this.g, lVar.g);
            }
            return false;
        }

        public int hashCode() {
            int hashCode = this.a.hashCode() * 31;
            String str = this.b;
            int hashCode2 = (hashCode + (str == null ? 0 : str.hashCode())) * 31;
            String str2 = this.c;
            int hashCode3 = (((((hashCode2 + (str2 == null ? 0 : str2.hashCode())) * 31) + this.d) * 31) + this.e) * 31;
            String str3 = this.f;
            int hashCode4 = (hashCode3 + (str3 == null ? 0 : str3.hashCode())) * 31;
            String str4 = this.g;
            return hashCode4 + (str4 != null ? str4.hashCode() : 0);
        }

        public l(a aVar) {
            this.a = aVar.a;
            this.b = aVar.b;
            this.c = aVar.c;
            this.d = aVar.d;
            this.e = aVar.e;
            this.f = aVar.f;
            this.g = aVar.g;
        }
    }

    public static m c(Bundle bundle) {
        g a2;
        n a3;
        e a4;
        j a5;
        String str = (String) ii.e(bundle.getString(f(0), ""));
        Bundle bundle2 = bundle.getBundle(f(1));
        if (bundle2 == null) {
            a2 = g.j0;
        } else {
            a2 = g.k0.a(bundle2);
        }
        g gVar = a2;
        Bundle bundle3 = bundle.getBundle(f(2));
        if (bundle3 == null) {
            a3 = n.K0;
        } else {
            a3 = n.L0.a(bundle3);
        }
        n nVar = a3;
        Bundle bundle4 = bundle.getBundle(f(3));
        if (bundle4 == null) {
            a4 = e.k0;
        } else {
            a4 = d.j0.a(bundle4);
        }
        e eVar = a4;
        Bundle bundle5 = bundle.getBundle(f(4));
        if (bundle5 == null) {
            a5 = j.h0;
        } else {
            a5 = j.i0.a(bundle5);
        }
        return new m(str, eVar, null, gVar, nVar, a5);
    }

    public static m d(Uri uri) {
        return new c().g(uri).a();
    }

    public static m e(String str) {
        return new c().h(str).a();
    }

    public static String f(int i2) {
        return Integer.toString(i2, 36);
    }

    public c b() {
        return new c();
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj instanceof m) {
            m mVar = (m) obj;
            return androidx.media3.common.util.b.c(this.a, mVar.a) && this.i0.equals(mVar.i0) && androidx.media3.common.util.b.c(this.f0, mVar.f0) && androidx.media3.common.util.b.c(this.g0, mVar.g0) && androidx.media3.common.util.b.c(this.h0, mVar.h0) && androidx.media3.common.util.b.c(this.j0, mVar.j0);
        }
        return false;
    }

    public int hashCode() {
        int hashCode = this.a.hashCode() * 31;
        h hVar = this.f0;
        return ((((((((hashCode + (hVar != null ? hVar.hashCode() : 0)) * 31) + this.g0.hashCode()) * 31) + this.i0.hashCode()) * 31) + this.h0.hashCode()) * 31) + this.j0.hashCode();
    }

    @Override // androidx.media3.common.e
    public Bundle toBundle() {
        Bundle bundle = new Bundle();
        bundle.putString(f(0), this.a);
        bundle.putBundle(f(1), this.g0.toBundle());
        bundle.putBundle(f(2), this.h0.toBundle());
        bundle.putBundle(f(3), this.i0.toBundle());
        bundle.putBundle(f(4), this.j0.toBundle());
        return bundle;
    }

    public m(String str, e eVar, i iVar, g gVar, n nVar, j jVar) {
        this.a = str;
        this.f0 = iVar;
        this.g0 = gVar;
        this.h0 = nVar;
        this.i0 = eVar;
        this.j0 = jVar;
    }
}
