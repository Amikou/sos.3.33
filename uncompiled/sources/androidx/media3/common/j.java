package androidx.media3.common;

import android.os.Bundle;
import androidx.media3.common.e;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

/* compiled from: Format.java */
/* loaded from: classes.dex */
public final class j implements e {
    public static final j K0 = new b().E();
    public static final e.a<j> L0 = w81.a;
    public final int A0;
    public final f B0;
    public final int C0;
    public final int D0;
    public final int E0;
    public final int F0;
    public final int G0;
    public final int H0;
    public final int I0;
    public int J0;
    public final String a;
    public final String f0;
    public final String g0;
    public final int h0;
    public final int i0;
    public final int j0;
    public final int k0;
    public final int l0;
    public final String m0;
    public final Metadata n0;
    public final String o0;
    public final String p0;
    public final int q0;
    public final List<byte[]> r0;
    public final DrmInitData s0;
    public final long t0;
    public final int u0;
    public final int v0;
    public final float w0;
    public final int x0;
    public final float y0;
    public final byte[] z0;

    /* compiled from: Format.java */
    /* loaded from: classes.dex */
    public static final class b {
        public int A;
        public int B;
        public int C;
        public int D;
        public String a;
        public String b;
        public String c;
        public int d;
        public int e;
        public int f;
        public int g;
        public String h;
        public Metadata i;
        public String j;
        public String k;
        public int l;
        public List<byte[]> m;
        public DrmInitData n;
        public long o;
        public int p;
        public int q;
        public float r;
        public int s;
        public float t;
        public byte[] u;
        public int v;
        public f w;
        public int x;
        public int y;
        public int z;

        public j E() {
            return new j(this);
        }

        public b F(int i) {
            this.C = i;
            return this;
        }

        public b G(int i) {
            this.f = i;
            return this;
        }

        public b H(int i) {
            this.x = i;
            return this;
        }

        public b I(String str) {
            this.h = str;
            return this;
        }

        public b J(f fVar) {
            this.w = fVar;
            return this;
        }

        public b K(String str) {
            this.j = str;
            return this;
        }

        public b L(int i) {
            this.D = i;
            return this;
        }

        public b M(DrmInitData drmInitData) {
            this.n = drmInitData;
            return this;
        }

        public b N(int i) {
            this.A = i;
            return this;
        }

        public b O(int i) {
            this.B = i;
            return this;
        }

        public b P(float f) {
            this.r = f;
            return this;
        }

        public b Q(int i) {
            this.q = i;
            return this;
        }

        public b R(int i) {
            this.a = Integer.toString(i);
            return this;
        }

        public b S(String str) {
            this.a = str;
            return this;
        }

        public b T(List<byte[]> list) {
            this.m = list;
            return this;
        }

        public b U(String str) {
            this.b = str;
            return this;
        }

        public b V(String str) {
            this.c = str;
            return this;
        }

        public b W(int i) {
            this.l = i;
            return this;
        }

        public b X(Metadata metadata) {
            this.i = metadata;
            return this;
        }

        public b Y(int i) {
            this.z = i;
            return this;
        }

        public b Z(int i) {
            this.g = i;
            return this;
        }

        public b a0(float f) {
            this.t = f;
            return this;
        }

        public b b0(byte[] bArr) {
            this.u = bArr;
            return this;
        }

        public b c0(int i) {
            this.e = i;
            return this;
        }

        public b d0(int i) {
            this.s = i;
            return this;
        }

        public b e0(String str) {
            this.k = str;
            return this;
        }

        public b f0(int i) {
            this.y = i;
            return this;
        }

        public b g0(int i) {
            this.d = i;
            return this;
        }

        public b h0(int i) {
            this.v = i;
            return this;
        }

        public b i0(long j) {
            this.o = j;
            return this;
        }

        public b j0(int i) {
            this.p = i;
            return this;
        }

        public b() {
            this.f = -1;
            this.g = -1;
            this.l = -1;
            this.o = Long.MAX_VALUE;
            this.p = -1;
            this.q = -1;
            this.r = -1.0f;
            this.t = 1.0f;
            this.v = -1;
            this.x = -1;
            this.y = -1;
            this.z = -1;
            this.C = -1;
            this.D = 0;
        }

        public b(j jVar) {
            this.a = jVar.a;
            this.b = jVar.f0;
            this.c = jVar.g0;
            this.d = jVar.h0;
            this.e = jVar.i0;
            this.f = jVar.j0;
            this.g = jVar.k0;
            this.h = jVar.m0;
            this.i = jVar.n0;
            this.j = jVar.o0;
            this.k = jVar.p0;
            this.l = jVar.q0;
            this.m = jVar.r0;
            this.n = jVar.s0;
            this.o = jVar.t0;
            this.p = jVar.u0;
            this.q = jVar.v0;
            this.r = jVar.w0;
            this.s = jVar.x0;
            this.t = jVar.y0;
            this.u = jVar.z0;
            this.v = jVar.A0;
            this.w = jVar.B0;
            this.x = jVar.C0;
            this.y = jVar.D0;
            this.z = jVar.E0;
            this.A = jVar.F0;
            this.B = jVar.G0;
            this.C = jVar.H0;
            this.D = jVar.I0;
        }
    }

    public static <T> T d(T t, T t2) {
        return t != null ? t : t2;
    }

    public static j e(Bundle bundle) {
        b bVar = new b();
        is.a(bundle);
        int i = 0;
        String string = bundle.getString(h(0));
        j jVar = K0;
        bVar.S((String) d(string, jVar.a)).U((String) d(bundle.getString(h(1)), jVar.f0)).V((String) d(bundle.getString(h(2)), jVar.g0)).g0(bundle.getInt(h(3), jVar.h0)).c0(bundle.getInt(h(4), jVar.i0)).G(bundle.getInt(h(5), jVar.j0)).Z(bundle.getInt(h(6), jVar.k0)).I((String) d(bundle.getString(h(7)), jVar.m0)).X((Metadata) d((Metadata) bundle.getParcelable(h(8)), jVar.n0)).K((String) d(bundle.getString(h(9)), jVar.o0)).e0((String) d(bundle.getString(h(10)), jVar.p0)).W(bundle.getInt(h(11), jVar.q0));
        ArrayList arrayList = new ArrayList();
        while (true) {
            byte[] byteArray = bundle.getByteArray(i(i));
            if (byteArray == null) {
                break;
            }
            arrayList.add(byteArray);
            i++;
        }
        b M = bVar.T(arrayList).M((DrmInitData) bundle.getParcelable(h(13)));
        String h = h(14);
        j jVar2 = K0;
        M.i0(bundle.getLong(h, jVar2.t0)).j0(bundle.getInt(h(15), jVar2.u0)).Q(bundle.getInt(h(16), jVar2.v0)).P(bundle.getFloat(h(17), jVar2.w0)).d0(bundle.getInt(h(18), jVar2.x0)).a0(bundle.getFloat(h(19), jVar2.y0)).b0(bundle.getByteArray(h(20))).h0(bundle.getInt(h(21), jVar2.A0));
        Bundle bundle2 = bundle.getBundle(h(22));
        if (bundle2 != null) {
            bVar.J(f.j0.a(bundle2));
        }
        bVar.H(bundle.getInt(h(23), jVar2.C0)).f0(bundle.getInt(h(24), jVar2.D0)).Y(bundle.getInt(h(25), jVar2.E0)).N(bundle.getInt(h(26), jVar2.F0)).O(bundle.getInt(h(27), jVar2.G0)).F(bundle.getInt(h(28), jVar2.H0)).L(bundle.getInt(h(29), jVar2.I0));
        return bVar.E();
    }

    public static String h(int i) {
        return Integer.toString(i, 36);
    }

    public static String i(int i) {
        return h(12) + "_" + Integer.toString(i, 36);
    }

    public b b() {
        return new b();
    }

    public j c(int i) {
        return b().L(i).E();
    }

    public boolean equals(Object obj) {
        int i;
        if (this == obj) {
            return true;
        }
        if (obj == null || j.class != obj.getClass()) {
            return false;
        }
        j jVar = (j) obj;
        int i2 = this.J0;
        if (i2 == 0 || (i = jVar.J0) == 0 || i2 == i) {
            return this.h0 == jVar.h0 && this.i0 == jVar.i0 && this.j0 == jVar.j0 && this.k0 == jVar.k0 && this.q0 == jVar.q0 && this.t0 == jVar.t0 && this.u0 == jVar.u0 && this.v0 == jVar.v0 && this.x0 == jVar.x0 && this.A0 == jVar.A0 && this.C0 == jVar.C0 && this.D0 == jVar.D0 && this.E0 == jVar.E0 && this.F0 == jVar.F0 && this.G0 == jVar.G0 && this.H0 == jVar.H0 && this.I0 == jVar.I0 && Float.compare(this.w0, jVar.w0) == 0 && Float.compare(this.y0, jVar.y0) == 0 && androidx.media3.common.util.b.c(this.a, jVar.a) && androidx.media3.common.util.b.c(this.f0, jVar.f0) && androidx.media3.common.util.b.c(this.m0, jVar.m0) && androidx.media3.common.util.b.c(this.o0, jVar.o0) && androidx.media3.common.util.b.c(this.p0, jVar.p0) && androidx.media3.common.util.b.c(this.g0, jVar.g0) && Arrays.equals(this.z0, jVar.z0) && androidx.media3.common.util.b.c(this.n0, jVar.n0) && androidx.media3.common.util.b.c(this.B0, jVar.B0) && androidx.media3.common.util.b.c(this.s0, jVar.s0) && g(jVar);
        }
        return false;
    }

    public int f() {
        int i;
        int i2 = this.u0;
        if (i2 == -1 || (i = this.v0) == -1) {
            return -1;
        }
        return i2 * i;
    }

    public boolean g(j jVar) {
        if (this.r0.size() != jVar.r0.size()) {
            return false;
        }
        for (int i = 0; i < this.r0.size(); i++) {
            if (!Arrays.equals(this.r0.get(i), jVar.r0.get(i))) {
                return false;
            }
        }
        return true;
    }

    public int hashCode() {
        if (this.J0 == 0) {
            String str = this.a;
            int hashCode = (527 + (str == null ? 0 : str.hashCode())) * 31;
            String str2 = this.f0;
            int hashCode2 = (hashCode + (str2 != null ? str2.hashCode() : 0)) * 31;
            String str3 = this.g0;
            int hashCode3 = (((((((((hashCode2 + (str3 == null ? 0 : str3.hashCode())) * 31) + this.h0) * 31) + this.i0) * 31) + this.j0) * 31) + this.k0) * 31;
            String str4 = this.m0;
            int hashCode4 = (hashCode3 + (str4 == null ? 0 : str4.hashCode())) * 31;
            Metadata metadata = this.n0;
            int hashCode5 = (hashCode4 + (metadata == null ? 0 : metadata.hashCode())) * 31;
            String str5 = this.o0;
            int hashCode6 = (hashCode5 + (str5 == null ? 0 : str5.hashCode())) * 31;
            String str6 = this.p0;
            this.J0 = ((((((((((((((((((((((((((((((hashCode6 + (str6 != null ? str6.hashCode() : 0)) * 31) + this.q0) * 31) + ((int) this.t0)) * 31) + this.u0) * 31) + this.v0) * 31) + Float.floatToIntBits(this.w0)) * 31) + this.x0) * 31) + Float.floatToIntBits(this.y0)) * 31) + this.A0) * 31) + this.C0) * 31) + this.D0) * 31) + this.E0) * 31) + this.F0) * 31) + this.G0) * 31) + this.H0) * 31) + this.I0;
        }
        return this.J0;
    }

    public j j(j jVar) {
        String str;
        Metadata b2;
        if (this == jVar) {
            return this;
        }
        int i = y82.i(this.p0);
        String str2 = jVar.a;
        String str3 = jVar.f0;
        if (str3 == null) {
            str3 = this.f0;
        }
        String str4 = this.g0;
        if ((i == 3 || i == 1) && (str = jVar.g0) != null) {
            str4 = str;
        }
        int i2 = this.j0;
        if (i2 == -1) {
            i2 = jVar.j0;
        }
        int i3 = this.k0;
        if (i3 == -1) {
            i3 = jVar.k0;
        }
        String str5 = this.m0;
        if (str5 == null) {
            String H = androidx.media3.common.util.b.H(jVar.m0, i);
            if (androidx.media3.common.util.b.N0(H).length == 1) {
                str5 = H;
            }
        }
        Metadata metadata = this.n0;
        if (metadata == null) {
            b2 = jVar.n0;
        } else {
            b2 = metadata.b(jVar.n0);
        }
        float f = this.w0;
        if (f == -1.0f && i == 2) {
            f = jVar.w0;
        }
        int i4 = this.h0 | jVar.h0;
        return b().S(str2).U(str3).V(str4).g0(i4).c0(this.i0 | jVar.i0).G(i2).Z(i3).I(str5).X(b2).M(DrmInitData.d(jVar.s0, this.s0)).P(f).E();
    }

    @Override // androidx.media3.common.e
    public Bundle toBundle() {
        Bundle bundle = new Bundle();
        bundle.putString(h(0), this.a);
        bundle.putString(h(1), this.f0);
        bundle.putString(h(2), this.g0);
        bundle.putInt(h(3), this.h0);
        bundle.putInt(h(4), this.i0);
        bundle.putInt(h(5), this.j0);
        bundle.putInt(h(6), this.k0);
        bundle.putString(h(7), this.m0);
        bundle.putParcelable(h(8), this.n0);
        bundle.putString(h(9), this.o0);
        bundle.putString(h(10), this.p0);
        bundle.putInt(h(11), this.q0);
        for (int i = 0; i < this.r0.size(); i++) {
            bundle.putByteArray(i(i), this.r0.get(i));
        }
        bundle.putParcelable(h(13), this.s0);
        bundle.putLong(h(14), this.t0);
        bundle.putInt(h(15), this.u0);
        bundle.putInt(h(16), this.v0);
        bundle.putFloat(h(17), this.w0);
        bundle.putInt(h(18), this.x0);
        bundle.putFloat(h(19), this.y0);
        bundle.putByteArray(h(20), this.z0);
        bundle.putInt(h(21), this.A0);
        if (this.B0 != null) {
            bundle.putBundle(h(22), this.B0.toBundle());
        }
        bundle.putInt(h(23), this.C0);
        bundle.putInt(h(24), this.D0);
        bundle.putInt(h(25), this.E0);
        bundle.putInt(h(26), this.F0);
        bundle.putInt(h(27), this.G0);
        bundle.putInt(h(28), this.H0);
        bundle.putInt(h(29), this.I0);
        return bundle;
    }

    public String toString() {
        return "Format(" + this.a + ", " + this.f0 + ", " + this.o0 + ", " + this.p0 + ", " + this.m0 + ", " + this.l0 + ", " + this.g0 + ", [" + this.u0 + ", " + this.v0 + ", " + this.w0 + "], [" + this.C0 + ", " + this.D0 + "])";
    }

    public j(b bVar) {
        this.a = bVar.a;
        this.f0 = bVar.b;
        this.g0 = androidx.media3.common.util.b.A0(bVar.c);
        this.h0 = bVar.d;
        this.i0 = bVar.e;
        int i = bVar.f;
        this.j0 = i;
        int i2 = bVar.g;
        this.k0 = i2;
        this.l0 = i2 != -1 ? i2 : i;
        this.m0 = bVar.h;
        this.n0 = bVar.i;
        this.o0 = bVar.j;
        this.p0 = bVar.k;
        this.q0 = bVar.l;
        this.r0 = bVar.m == null ? Collections.emptyList() : bVar.m;
        DrmInitData drmInitData = bVar.n;
        this.s0 = drmInitData;
        this.t0 = bVar.o;
        this.u0 = bVar.p;
        this.v0 = bVar.q;
        this.w0 = bVar.r;
        this.x0 = bVar.s == -1 ? 0 : bVar.s;
        this.y0 = bVar.t == -1.0f ? 1.0f : bVar.t;
        this.z0 = bVar.u;
        this.A0 = bVar.v;
        this.B0 = bVar.w;
        this.C0 = bVar.x;
        this.D0 = bVar.y;
        this.E0 = bVar.z;
        this.F0 = bVar.A == -1 ? 0 : bVar.A;
        this.G0 = bVar.B != -1 ? bVar.B : 0;
        this.H0 = bVar.C;
        if (bVar.D != 0 || drmInitData == null) {
            this.I0 = bVar.D;
        } else {
            this.I0 = 1;
        }
    }
}
