package androidx.media3.common;

import android.content.Context;
import android.graphics.Point;
import android.os.Bundle;
import android.os.Looper;
import android.view.accessibility.CaptioningManager;
import com.google.common.collect.ImmutableList;
import com.google.common.collect.ImmutableMap;
import com.google.common.collect.ImmutableSet;
import com.google.common.primitives.Ints;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Locale;
import java.util.Map;

/* compiled from: TrackSelectionParameters.java */
/* loaded from: classes.dex */
public class x implements e {
    public static final x E0 = new a().A();
    public final boolean A0;
    public final boolean B0;
    public final ImmutableMap<v, w> C0;
    public final ImmutableSet<Integer> D0;
    public final int a;
    public final int f0;
    public final int g0;
    public final int h0;
    public final int i0;
    public final int j0;
    public final int k0;
    public final int l0;
    public final int m0;
    public final int n0;
    public final boolean o0;
    public final ImmutableList<String> p0;
    public final int q0;
    public final ImmutableList<String> r0;
    public final int s0;
    public final int t0;
    public final int u0;
    public final ImmutableList<String> v0;
    public final ImmutableList<String> w0;
    public final int x0;
    public final int y0;
    public final boolean z0;

    public x(a aVar) {
        this.a = aVar.a;
        this.f0 = aVar.b;
        this.g0 = aVar.c;
        this.h0 = aVar.d;
        this.i0 = aVar.e;
        this.j0 = aVar.f;
        this.k0 = aVar.g;
        this.l0 = aVar.h;
        this.m0 = aVar.i;
        this.n0 = aVar.j;
        this.o0 = aVar.k;
        this.p0 = aVar.l;
        this.q0 = aVar.m;
        this.r0 = aVar.n;
        this.s0 = aVar.o;
        this.t0 = aVar.p;
        this.u0 = aVar.q;
        this.v0 = aVar.r;
        this.w0 = aVar.s;
        this.x0 = aVar.t;
        this.y0 = aVar.u;
        this.z0 = aVar.v;
        this.A0 = aVar.w;
        this.B0 = aVar.x;
        this.C0 = ImmutableMap.copyOf((Map) aVar.y);
        this.D0 = ImmutableSet.copyOf((Collection) aVar.z);
    }

    public static String b(int i) {
        return Integer.toString(i, 36);
    }

    public a a() {
        return new a(this);
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null || getClass() != obj.getClass()) {
            return false;
        }
        x xVar = (x) obj;
        return this.a == xVar.a && this.f0 == xVar.f0 && this.g0 == xVar.g0 && this.h0 == xVar.h0 && this.i0 == xVar.i0 && this.j0 == xVar.j0 && this.k0 == xVar.k0 && this.l0 == xVar.l0 && this.o0 == xVar.o0 && this.m0 == xVar.m0 && this.n0 == xVar.n0 && this.p0.equals(xVar.p0) && this.q0 == xVar.q0 && this.r0.equals(xVar.r0) && this.s0 == xVar.s0 && this.t0 == xVar.t0 && this.u0 == xVar.u0 && this.v0.equals(xVar.v0) && this.w0.equals(xVar.w0) && this.x0 == xVar.x0 && this.y0 == xVar.y0 && this.z0 == xVar.z0 && this.A0 == xVar.A0 && this.B0 == xVar.B0 && this.C0.equals(xVar.C0) && this.D0.equals(xVar.D0);
    }

    public int hashCode() {
        return ((((((((((((((((((((((((((((((((((((((((((((((((((this.a + 31) * 31) + this.f0) * 31) + this.g0) * 31) + this.h0) * 31) + this.i0) * 31) + this.j0) * 31) + this.k0) * 31) + this.l0) * 31) + (this.o0 ? 1 : 0)) * 31) + this.m0) * 31) + this.n0) * 31) + this.p0.hashCode()) * 31) + this.q0) * 31) + this.r0.hashCode()) * 31) + this.s0) * 31) + this.t0) * 31) + this.u0) * 31) + this.v0.hashCode()) * 31) + this.w0.hashCode()) * 31) + this.x0) * 31) + this.y0) * 31) + (this.z0 ? 1 : 0)) * 31) + (this.A0 ? 1 : 0)) * 31) + (this.B0 ? 1 : 0)) * 31) + this.C0.hashCode()) * 31) + this.D0.hashCode();
    }

    @Override // androidx.media3.common.e
    public Bundle toBundle() {
        Bundle bundle = new Bundle();
        bundle.putInt(b(6), this.a);
        bundle.putInt(b(7), this.f0);
        bundle.putInt(b(8), this.g0);
        bundle.putInt(b(9), this.h0);
        bundle.putInt(b(10), this.i0);
        bundle.putInt(b(11), this.j0);
        bundle.putInt(b(12), this.k0);
        bundle.putInt(b(13), this.l0);
        bundle.putInt(b(14), this.m0);
        bundle.putInt(b(15), this.n0);
        bundle.putBoolean(b(16), this.o0);
        bundle.putStringArray(b(17), (String[]) this.p0.toArray(new String[0]));
        bundle.putInt(b(25), this.q0);
        bundle.putStringArray(b(1), (String[]) this.r0.toArray(new String[0]));
        bundle.putInt(b(2), this.s0);
        bundle.putInt(b(18), this.t0);
        bundle.putInt(b(19), this.u0);
        bundle.putStringArray(b(20), (String[]) this.v0.toArray(new String[0]));
        bundle.putStringArray(b(3), (String[]) this.w0.toArray(new String[0]));
        bundle.putInt(b(4), this.x0);
        bundle.putInt(b(26), this.y0);
        bundle.putBoolean(b(5), this.z0);
        bundle.putBoolean(b(21), this.A0);
        bundle.putBoolean(b(22), this.B0);
        bundle.putParcelableArrayList(b(23), is.c(this.C0.values()));
        bundle.putIntArray(b(24), Ints.k(this.D0));
        return bundle;
    }

    /* compiled from: TrackSelectionParameters.java */
    /* loaded from: classes.dex */
    public static class a {
        public int a;
        public int b;
        public int c;
        public int d;
        public int e;
        public int f;
        public int g;
        public int h;
        public int i;
        public int j;
        public boolean k;
        public ImmutableList<String> l;
        public int m;
        public ImmutableList<String> n;
        public int o;
        public int p;
        public int q;
        public ImmutableList<String> r;
        public ImmutableList<String> s;
        public int t;
        public int u;
        public boolean v;
        public boolean w;
        public boolean x;
        public HashMap<v, w> y;
        public HashSet<Integer> z;

        @Deprecated
        public a() {
            this.a = Integer.MAX_VALUE;
            this.b = Integer.MAX_VALUE;
            this.c = Integer.MAX_VALUE;
            this.d = Integer.MAX_VALUE;
            this.i = Integer.MAX_VALUE;
            this.j = Integer.MAX_VALUE;
            this.k = true;
            this.l = ImmutableList.of();
            this.m = 0;
            this.n = ImmutableList.of();
            this.o = 0;
            this.p = Integer.MAX_VALUE;
            this.q = Integer.MAX_VALUE;
            this.r = ImmutableList.of();
            this.s = ImmutableList.of();
            this.t = 0;
            this.u = 0;
            this.v = false;
            this.w = false;
            this.x = false;
            this.y = new HashMap<>();
            this.z = new HashSet<>();
        }

        public x A() {
            return new x(this);
        }

        public a B(int i) {
            Iterator<w> it = this.y.values().iterator();
            while (it.hasNext()) {
                if (it.next().b() == i) {
                    it.remove();
                }
            }
            return this;
        }

        public final void C(x xVar) {
            this.a = xVar.a;
            this.b = xVar.f0;
            this.c = xVar.g0;
            this.d = xVar.h0;
            this.e = xVar.i0;
            this.f = xVar.j0;
            this.g = xVar.k0;
            this.h = xVar.l0;
            this.i = xVar.m0;
            this.j = xVar.n0;
            this.k = xVar.o0;
            this.l = xVar.p0;
            this.m = xVar.q0;
            this.n = xVar.r0;
            this.o = xVar.s0;
            this.p = xVar.t0;
            this.q = xVar.u0;
            this.r = xVar.v0;
            this.s = xVar.w0;
            this.t = xVar.x0;
            this.u = xVar.y0;
            this.v = xVar.z0;
            this.w = xVar.A0;
            this.x = xVar.B0;
            this.z = new HashSet<>(xVar.D0);
            this.y = new HashMap<>(xVar.C0);
        }

        public a D(x xVar) {
            C(xVar);
            return this;
        }

        public a E(int i) {
            this.u = i;
            return this;
        }

        public a F(w wVar) {
            B(wVar.b());
            this.y.put(wVar.a, wVar);
            return this;
        }

        public a G(Context context) {
            if (androidx.media3.common.util.b.a >= 19) {
                H(context);
            }
            return this;
        }

        public final void H(Context context) {
            CaptioningManager captioningManager;
            if ((androidx.media3.common.util.b.a >= 23 || Looper.myLooper() != null) && (captioningManager = (CaptioningManager) context.getSystemService("captioning")) != null && captioningManager.isEnabled()) {
                this.t = 1088;
                Locale locale = captioningManager.getLocale();
                if (locale != null) {
                    this.s = ImmutableList.of(androidx.media3.common.util.b.U(locale));
                }
            }
        }

        public a I(int i, boolean z) {
            if (z) {
                this.z.add(Integer.valueOf(i));
            } else {
                this.z.remove(Integer.valueOf(i));
            }
            return this;
        }

        public a J(int i, int i2, boolean z) {
            this.i = i;
            this.j = i2;
            this.k = z;
            return this;
        }

        public a K(Context context, boolean z) {
            Point K = androidx.media3.common.util.b.K(context);
            return J(K.x, K.y, z);
        }

        public a(Context context) {
            this();
            G(context);
            K(context, true);
        }

        public a(x xVar) {
            C(xVar);
        }
    }
}
