package androidx.media3.common;

import android.os.Bundle;
import androidx.media3.common.e;
import java.util.Arrays;

/* compiled from: ColorInfo.java */
/* loaded from: classes.dex */
public final class f implements e {
    public static final e.a<f> j0 = w20.a;
    public final int a;
    public final int f0;
    public final int g0;
    public final byte[] h0;
    public int i0;

    public f(int i, int i2, int i3, byte[] bArr) {
        this.a = i;
        this.f0 = i2;
        this.g0 = i3;
        this.h0 = bArr;
    }

    public static int b(int i) {
        if (i != 1) {
            if (i != 9) {
                return (i == 4 || i == 5 || i == 6 || i == 7) ? 2 : -1;
            }
            return 6;
        }
        return 1;
    }

    public static int c(int i) {
        if (i != 1) {
            if (i != 16) {
                if (i != 18) {
                    return (i == 6 || i == 7) ? 3 : -1;
                }
                return 7;
            }
            return 6;
        }
        return 3;
    }

    public static String d(int i) {
        return Integer.toString(i, 36);
    }

    public static /* synthetic */ f e(Bundle bundle) {
        return new f(bundle.getInt(d(0), -1), bundle.getInt(d(1), -1), bundle.getInt(d(2), -1), bundle.getByteArray(d(3)));
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null || f.class != obj.getClass()) {
            return false;
        }
        f fVar = (f) obj;
        return this.a == fVar.a && this.f0 == fVar.f0 && this.g0 == fVar.g0 && Arrays.equals(this.h0, fVar.h0);
    }

    public int hashCode() {
        if (this.i0 == 0) {
            this.i0 = ((((((527 + this.a) * 31) + this.f0) * 31) + this.g0) * 31) + Arrays.hashCode(this.h0);
        }
        return this.i0;
    }

    @Override // androidx.media3.common.e
    public Bundle toBundle() {
        Bundle bundle = new Bundle();
        bundle.putInt(d(0), this.a);
        bundle.putInt(d(1), this.f0);
        bundle.putInt(d(2), this.g0);
        bundle.putByteArray(d(3), this.h0);
        return bundle;
    }

    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("ColorInfo(");
        sb.append(this.a);
        sb.append(", ");
        sb.append(this.f0);
        sb.append(", ");
        sb.append(this.g0);
        sb.append(", ");
        sb.append(this.h0 != null);
        sb.append(")");
        return sb.toString();
    }
}
