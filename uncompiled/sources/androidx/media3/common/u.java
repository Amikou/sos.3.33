package androidx.media3.common;

import android.net.Uri;
import android.os.Bundle;
import android.util.Pair;
import androidx.media3.common.a;
import androidx.media3.common.e;
import androidx.media3.common.m;
import java.util.ArrayList;
import zendesk.support.request.CellBase;

/* compiled from: Timeline.java */
/* loaded from: classes.dex */
public abstract class u implements e {
    public static final u a = new a();

    /* compiled from: Timeline.java */
    /* loaded from: classes.dex */
    public class a extends u {
        @Override // androidx.media3.common.u
        public int b(Object obj) {
            return -1;
        }

        @Override // androidx.media3.common.u
        public b g(int i, b bVar, boolean z) {
            throw new IndexOutOfBoundsException();
        }

        @Override // androidx.media3.common.u
        public int i() {
            return 0;
        }

        @Override // androidx.media3.common.u
        public Object m(int i) {
            throw new IndexOutOfBoundsException();
        }

        @Override // androidx.media3.common.u
        public c o(int i, c cVar, long j) {
            throw new IndexOutOfBoundsException();
        }

        @Override // androidx.media3.common.u
        public int p() {
            return 0;
        }
    }

    /* compiled from: Timeline.java */
    /* loaded from: classes.dex */
    public static final class b implements e {
        public static final e.a<b> l0 = f64.a;
        public Object a;
        public Object f0;
        public int g0;
        public long h0;
        public long i0;
        public boolean j0;
        public androidx.media3.common.a k0 = androidx.media3.common.a.k0;

        public static b b(Bundle bundle) {
            androidx.media3.common.a aVar;
            int i = bundle.getInt(t(0), 0);
            long j = bundle.getLong(t(1), CellBase.ID_SYSTEM_MESSAGE_REQUEST_CLOSED);
            long j2 = bundle.getLong(t(2), 0L);
            boolean z = bundle.getBoolean(t(3));
            Bundle bundle2 = bundle.getBundle(t(4));
            if (bundle2 != null) {
                aVar = androidx.media3.common.a.m0.a(bundle2);
            } else {
                aVar = androidx.media3.common.a.k0;
            }
            androidx.media3.common.a aVar2 = aVar;
            b bVar = new b();
            bVar.v(null, null, i, j, j2, aVar2, z);
            return bVar;
        }

        public static String t(int i) {
            return Integer.toString(i, 36);
        }

        public int c(int i) {
            return this.k0.c(i).f0;
        }

        public long d(int i, int i2) {
            a.C0028a c = this.k0.c(i);
            return c.f0 != -1 ? c.i0[i2] : CellBase.ID_SYSTEM_MESSAGE_REQUEST_CLOSED;
        }

        public int e() {
            return this.k0.f0;
        }

        public boolean equals(Object obj) {
            if (this == obj) {
                return true;
            }
            if (obj == null || !b.class.equals(obj.getClass())) {
                return false;
            }
            b bVar = (b) obj;
            return androidx.media3.common.util.b.c(this.a, bVar.a) && androidx.media3.common.util.b.c(this.f0, bVar.f0) && this.g0 == bVar.g0 && this.h0 == bVar.h0 && this.i0 == bVar.i0 && this.j0 == bVar.j0 && androidx.media3.common.util.b.c(this.k0, bVar.k0);
        }

        public int f(long j) {
            return this.k0.d(j, this.h0);
        }

        public int g(long j) {
            return this.k0.e(j, this.h0);
        }

        public long h(int i) {
            return this.k0.c(i).a;
        }

        public int hashCode() {
            Object obj = this.a;
            int hashCode = (217 + (obj == null ? 0 : obj.hashCode())) * 31;
            Object obj2 = this.f0;
            int hashCode2 = obj2 != null ? obj2.hashCode() : 0;
            long j = this.h0;
            long j2 = this.i0;
            return ((((((((((hashCode + hashCode2) * 31) + this.g0) * 31) + ((int) (j ^ (j >>> 32)))) * 31) + ((int) (j2 ^ (j2 >>> 32)))) * 31) + (this.j0 ? 1 : 0)) * 31) + this.k0.hashCode();
        }

        public long i() {
            return this.k0.g0;
        }

        public int j(int i, int i2) {
            a.C0028a c = this.k0.c(i);
            if (c.f0 != -1) {
                return c.h0[i2];
            }
            return 0;
        }

        public long k(int i) {
            return this.k0.c(i).j0;
        }

        public long l() {
            return this.h0;
        }

        public int m(int i) {
            return this.k0.c(i).e();
        }

        public int n(int i, int i2) {
            return this.k0.c(i).f(i2);
        }

        public long o() {
            return androidx.media3.common.util.b.U0(this.i0);
        }

        public long p() {
            return this.i0;
        }

        public int q() {
            return this.k0.i0;
        }

        public boolean r(int i) {
            return !this.k0.c(i).g();
        }

        public boolean s(int i) {
            return this.k0.c(i).k0;
        }

        @Override // androidx.media3.common.e
        public Bundle toBundle() {
            Bundle bundle = new Bundle();
            bundle.putInt(t(0), this.g0);
            bundle.putLong(t(1), this.h0);
            bundle.putLong(t(2), this.i0);
            bundle.putBoolean(t(3), this.j0);
            bundle.putBundle(t(4), this.k0.toBundle());
            return bundle;
        }

        public b u(Object obj, Object obj2, int i, long j, long j2) {
            return v(obj, obj2, i, j, j2, androidx.media3.common.a.k0, false);
        }

        public b v(Object obj, Object obj2, int i, long j, long j2, androidx.media3.common.a aVar, boolean z) {
            this.a = obj;
            this.f0 = obj2;
            this.g0 = i;
            this.h0 = j;
            this.i0 = j2;
            this.k0 = aVar;
            this.j0 = z;
            return this;
        }
    }

    /* compiled from: Timeline.java */
    /* loaded from: classes.dex */
    public static final class c implements e {
        public static final Object v0 = new Object();
        public static final Object w0 = new Object();
        public static final m x0 = new m.c().d("androidx.media3.common.Timeline").g(Uri.EMPTY).a();
        public static final e.a<c> y0 = g64.a;
        @Deprecated
        public Object f0;
        public Object h0;
        public long i0;
        public long j0;
        public long k0;
        public boolean l0;
        public boolean m0;
        @Deprecated
        public boolean n0;
        public m.g o0;
        public boolean p0;
        public long q0;
        public long r0;
        public int s0;
        public int t0;
        public long u0;
        public Object a = v0;
        public m g0 = x0;

        public static c c(Bundle bundle) {
            Bundle bundle2 = bundle.getBundle(j(1));
            m a = bundle2 != null ? m.l0.a(bundle2) : null;
            long j = bundle.getLong(j(2), CellBase.ID_SYSTEM_MESSAGE_REQUEST_CLOSED);
            long j2 = bundle.getLong(j(3), CellBase.ID_SYSTEM_MESSAGE_REQUEST_CLOSED);
            long j3 = bundle.getLong(j(4), CellBase.ID_SYSTEM_MESSAGE_REQUEST_CLOSED);
            boolean z = bundle.getBoolean(j(5), false);
            boolean z2 = bundle.getBoolean(j(6), false);
            Bundle bundle3 = bundle.getBundle(j(7));
            m.g a2 = bundle3 != null ? m.g.k0.a(bundle3) : null;
            boolean z3 = bundle.getBoolean(j(8), false);
            long j4 = bundle.getLong(j(9), 0L);
            long j5 = bundle.getLong(j(10), CellBase.ID_SYSTEM_MESSAGE_REQUEST_CLOSED);
            int i = bundle.getInt(j(11), 0);
            int i2 = bundle.getInt(j(12), 0);
            long j6 = bundle.getLong(j(13), 0L);
            c cVar = new c();
            cVar.k(w0, a, null, j, j2, j3, z, z2, a2, j4, j5, i, i2, j6);
            cVar.p0 = z3;
            return cVar;
        }

        public static String j(int i) {
            return Integer.toString(i, 36);
        }

        public long d() {
            return androidx.media3.common.util.b.X(this.k0);
        }

        public long e() {
            return androidx.media3.common.util.b.U0(this.q0);
        }

        public boolean equals(Object obj) {
            if (this == obj) {
                return true;
            }
            if (obj == null || !c.class.equals(obj.getClass())) {
                return false;
            }
            c cVar = (c) obj;
            return androidx.media3.common.util.b.c(this.a, cVar.a) && androidx.media3.common.util.b.c(this.g0, cVar.g0) && androidx.media3.common.util.b.c(this.h0, cVar.h0) && androidx.media3.common.util.b.c(this.o0, cVar.o0) && this.i0 == cVar.i0 && this.j0 == cVar.j0 && this.k0 == cVar.k0 && this.l0 == cVar.l0 && this.m0 == cVar.m0 && this.p0 == cVar.p0 && this.q0 == cVar.q0 && this.r0 == cVar.r0 && this.s0 == cVar.s0 && this.t0 == cVar.t0 && this.u0 == cVar.u0;
        }

        public long f() {
            return this.q0;
        }

        public long g() {
            return androidx.media3.common.util.b.U0(this.r0);
        }

        public long h() {
            return this.u0;
        }

        public int hashCode() {
            int hashCode = (((217 + this.a.hashCode()) * 31) + this.g0.hashCode()) * 31;
            Object obj = this.h0;
            int hashCode2 = (hashCode + (obj == null ? 0 : obj.hashCode())) * 31;
            m.g gVar = this.o0;
            int hashCode3 = gVar != null ? gVar.hashCode() : 0;
            long j = this.i0;
            long j2 = this.j0;
            long j3 = this.k0;
            long j4 = this.q0;
            long j5 = this.r0;
            long j6 = this.u0;
            return ((((((((((((((((((((((hashCode2 + hashCode3) * 31) + ((int) (j ^ (j >>> 32)))) * 31) + ((int) (j2 ^ (j2 >>> 32)))) * 31) + ((int) (j3 ^ (j3 >>> 32)))) * 31) + (this.l0 ? 1 : 0)) * 31) + (this.m0 ? 1 : 0)) * 31) + (this.p0 ? 1 : 0)) * 31) + ((int) (j4 ^ (j4 >>> 32)))) * 31) + ((int) (j5 ^ (j5 >>> 32)))) * 31) + this.s0) * 31) + this.t0) * 31) + ((int) (j6 ^ (j6 >>> 32)));
        }

        public boolean i() {
            ii.g(this.n0 == (this.o0 != null));
            return this.o0 != null;
        }

        public c k(Object obj, m mVar, Object obj2, long j, long j2, long j3, boolean z, boolean z2, m.g gVar, long j4, long j5, int i, int i2, long j6) {
            m.h hVar;
            this.a = obj;
            this.g0 = mVar != null ? mVar : x0;
            this.f0 = (mVar == null || (hVar = mVar.f0) == null) ? null : hVar.h;
            this.h0 = obj2;
            this.i0 = j;
            this.j0 = j2;
            this.k0 = j3;
            this.l0 = z;
            this.m0 = z2;
            this.n0 = gVar != null;
            this.o0 = gVar;
            this.q0 = j4;
            this.r0 = j5;
            this.s0 = i;
            this.t0 = i2;
            this.u0 = j6;
            this.p0 = false;
            return this;
        }

        public final Bundle l(boolean z) {
            Bundle bundle = new Bundle();
            bundle.putBundle(j(1), (z ? m.k0 : this.g0).toBundle());
            bundle.putLong(j(2), this.i0);
            bundle.putLong(j(3), this.j0);
            bundle.putLong(j(4), this.k0);
            bundle.putBoolean(j(5), this.l0);
            bundle.putBoolean(j(6), this.m0);
            m.g gVar = this.o0;
            if (gVar != null) {
                bundle.putBundle(j(7), gVar.toBundle());
            }
            bundle.putBoolean(j(8), this.p0);
            bundle.putLong(j(9), this.q0);
            bundle.putLong(j(10), this.r0);
            bundle.putInt(j(11), this.s0);
            bundle.putInt(j(12), this.t0);
            bundle.putLong(j(13), this.u0);
            return bundle;
        }

        @Override // androidx.media3.common.e
        public Bundle toBundle() {
            return l(false);
        }
    }

    public static String s(int i) {
        return Integer.toString(i, 36);
    }

    public int a(boolean z) {
        return q() ? -1 : 0;
    }

    public abstract int b(Object obj);

    public int c(boolean z) {
        if (q()) {
            return -1;
        }
        return p() - 1;
    }

    public final int d(int i, b bVar, c cVar, int i2, boolean z) {
        int i3 = f(i, bVar).g0;
        if (n(i3, cVar).t0 == i) {
            int e = e(i3, i2, z);
            if (e == -1) {
                return -1;
            }
            return n(e, cVar).s0;
        }
        return i + 1;
    }

    public int e(int i, int i2, boolean z) {
        if (i2 == 0) {
            if (i == c(z)) {
                return -1;
            }
            return i + 1;
        } else if (i2 != 1) {
            if (i2 == 2) {
                return i == c(z) ? a(z) : i + 1;
            }
            throw new IllegalStateException();
        } else {
            return i;
        }
    }

    public boolean equals(Object obj) {
        int c2;
        if (this == obj) {
            return true;
        }
        if (obj instanceof u) {
            u uVar = (u) obj;
            if (uVar.p() == p() && uVar.i() == i()) {
                c cVar = new c();
                b bVar = new b();
                c cVar2 = new c();
                b bVar2 = new b();
                for (int i = 0; i < p(); i++) {
                    if (!n(i, cVar).equals(uVar.n(i, cVar2))) {
                        return false;
                    }
                }
                for (int i2 = 0; i2 < i(); i2++) {
                    if (!g(i2, bVar, true).equals(uVar.g(i2, bVar2, true))) {
                        return false;
                    }
                }
                int a2 = a(true);
                if (a2 == uVar.a(true) && (c2 = c(true)) == uVar.c(true)) {
                    while (a2 != c2) {
                        int e = e(a2, 0, true);
                        if (e != uVar.e(a2, 0, true)) {
                            return false;
                        }
                        a2 = e;
                    }
                    return true;
                }
                return false;
            }
            return false;
        }
        return false;
    }

    public final b f(int i, b bVar) {
        return g(i, bVar, false);
    }

    public abstract b g(int i, b bVar, boolean z);

    public b h(Object obj, b bVar) {
        return g(b(obj), bVar, true);
    }

    public int hashCode() {
        c cVar = new c();
        b bVar = new b();
        int p = 217 + p();
        for (int i = 0; i < p(); i++) {
            p = (p * 31) + n(i, cVar).hashCode();
        }
        int i2 = (p * 31) + i();
        for (int i3 = 0; i3 < i(); i3++) {
            i2 = (i2 * 31) + g(i3, bVar, true).hashCode();
        }
        int a2 = a(true);
        while (a2 != -1) {
            i2 = (i2 * 31) + a2;
            a2 = e(a2, 0, true);
        }
        return i2;
    }

    public abstract int i();

    public final Pair<Object, Long> j(c cVar, b bVar, int i, long j) {
        return (Pair) ii.e(k(cVar, bVar, i, j, 0L));
    }

    public final Pair<Object, Long> k(c cVar, b bVar, int i, long j, long j2) {
        ii.c(i, 0, p());
        o(i, cVar, j2);
        if (j == CellBase.ID_SYSTEM_MESSAGE_REQUEST_CLOSED) {
            j = cVar.f();
            if (j == CellBase.ID_SYSTEM_MESSAGE_REQUEST_CLOSED) {
                return null;
            }
        }
        int i2 = cVar.s0;
        f(i2, bVar);
        while (i2 < cVar.t0 && bVar.i0 != j) {
            int i3 = i2 + 1;
            if (f(i3, bVar).i0 > j) {
                break;
            }
            i2 = i3;
        }
        g(i2, bVar, true);
        long j3 = j - bVar.i0;
        long j4 = bVar.h0;
        if (j4 != CellBase.ID_SYSTEM_MESSAGE_REQUEST_CLOSED) {
            j3 = Math.min(j3, j4 - 1);
        }
        return Pair.create(ii.e(bVar.f0), Long.valueOf(Math.max(0L, j3)));
    }

    public int l(int i, int i2, boolean z) {
        if (i2 == 0) {
            if (i == a(z)) {
                return -1;
            }
            return i - 1;
        } else if (i2 != 1) {
            if (i2 == 2) {
                return i == a(z) ? c(z) : i - 1;
            }
            throw new IllegalStateException();
        } else {
            return i;
        }
    }

    public abstract Object m(int i);

    public final c n(int i, c cVar) {
        return o(i, cVar, 0L);
    }

    public abstract c o(int i, c cVar, long j);

    public abstract int p();

    public final boolean q() {
        return p() == 0;
    }

    public final boolean r(int i, b bVar, c cVar, int i2, boolean z) {
        return d(i, bVar, cVar, i2, z) == -1;
    }

    public final Bundle t(boolean z) {
        ArrayList arrayList = new ArrayList();
        int p = p();
        c cVar = new c();
        for (int i = 0; i < p; i++) {
            arrayList.add(o(i, cVar, 0L).l(z));
        }
        ArrayList arrayList2 = new ArrayList();
        int i2 = i();
        b bVar = new b();
        for (int i3 = 0; i3 < i2; i3++) {
            arrayList2.add(g(i3, bVar, false).toBundle());
        }
        int[] iArr = new int[p];
        if (p > 0) {
            iArr[0] = a(true);
        }
        for (int i4 = 1; i4 < p; i4++) {
            iArr[i4] = e(iArr[i4 - 1], 0, true);
        }
        Bundle bundle = new Bundle();
        hs.a(bundle, s(0), new d(arrayList));
        hs.a(bundle, s(1), new d(arrayList2));
        bundle.putIntArray(s(2), iArr);
        return bundle;
    }

    @Override // androidx.media3.common.e
    public final Bundle toBundle() {
        return t(false);
    }
}
