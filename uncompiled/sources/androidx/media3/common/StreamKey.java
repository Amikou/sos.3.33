package androidx.media3.common;

import android.os.Parcel;
import android.os.Parcelable;

/* loaded from: classes.dex */
public final class StreamKey implements Comparable<StreamKey>, Parcelable {
    public static final Parcelable.Creator<StreamKey> CREATOR = new a();
    public final int a;
    public final int f0;
    public final int g0;

    /* loaded from: classes.dex */
    public class a implements Parcelable.Creator<StreamKey> {
        @Override // android.os.Parcelable.Creator
        /* renamed from: a */
        public StreamKey createFromParcel(Parcel parcel) {
            return new StreamKey(parcel);
        }

        @Override // android.os.Parcelable.Creator
        /* renamed from: b */
        public StreamKey[] newArray(int i) {
            return new StreamKey[i];
        }
    }

    public StreamKey(int i, int i2, int i3) {
        this.a = i;
        this.f0 = i2;
        this.g0 = i3;
    }

    @Override // java.lang.Comparable
    /* renamed from: a */
    public int compareTo(StreamKey streamKey) {
        int i = this.a - streamKey.a;
        if (i == 0) {
            int i2 = this.f0 - streamKey.f0;
            return i2 == 0 ? this.g0 - streamKey.g0 : i2;
        }
        return i;
    }

    @Override // android.os.Parcelable
    public int describeContents() {
        return 0;
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null || StreamKey.class != obj.getClass()) {
            return false;
        }
        StreamKey streamKey = (StreamKey) obj;
        return this.a == streamKey.a && this.f0 == streamKey.f0 && this.g0 == streamKey.g0;
    }

    public int hashCode() {
        return (((this.a * 31) + this.f0) * 31) + this.g0;
    }

    public String toString() {
        return this.a + "." + this.f0 + "." + this.g0;
    }

    @Override // android.os.Parcelable
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeInt(this.a);
        parcel.writeInt(this.f0);
        parcel.writeInt(this.g0);
    }

    public StreamKey(Parcel parcel) {
        this.a = parcel.readInt();
        this.f0 = parcel.readInt();
        this.g0 = parcel.readInt();
    }
}
