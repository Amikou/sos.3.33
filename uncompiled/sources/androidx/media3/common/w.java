package androidx.media3.common;

import android.os.Bundle;
import androidx.media3.common.e;
import com.google.common.collect.ImmutableList;
import com.google.common.primitives.Ints;
import java.util.Collection;
import java.util.Collections;
import java.util.List;

/* compiled from: TrackSelectionOverride.java */
/* loaded from: classes.dex */
public final class w implements e {
    public static final e.a<w> g0 = i84.a;
    public final v a;
    public final ImmutableList<Integer> f0;

    public w(v vVar, List<Integer> list) {
        if (!list.isEmpty() && (((Integer) Collections.min(list)).intValue() < 0 || ((Integer) Collections.max(list)).intValue() >= vVar.a)) {
            throw new IndexOutOfBoundsException();
        }
        this.a = vVar;
        this.f0 = ImmutableList.copyOf((Collection) list);
    }

    public static String c(int i) {
        return Integer.toString(i, 36);
    }

    public static /* synthetic */ w d(Bundle bundle) {
        return new w(v.j0.a((Bundle) ii.e(bundle.getBundle(c(0)))), Ints.c((int[]) ii.e(bundle.getIntArray(c(1)))));
    }

    public int b() {
        return this.a.g0;
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null || w.class != obj.getClass()) {
            return false;
        }
        w wVar = (w) obj;
        return this.a.equals(wVar.a) && this.f0.equals(wVar.f0);
    }

    public int hashCode() {
        return this.a.hashCode() + (this.f0.hashCode() * 31);
    }

    @Override // androidx.media3.common.e
    public Bundle toBundle() {
        Bundle bundle = new Bundle();
        bundle.putBundle(c(0), this.a.toBundle());
        bundle.putIntArray(c(1), Ints.k(this.f0));
        return bundle;
    }
}
