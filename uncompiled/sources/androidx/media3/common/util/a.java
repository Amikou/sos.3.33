package androidx.media3.common.util;

/* compiled from: ConditionVariable.java */
/* loaded from: classes.dex */
public class a {
    public final tz a;
    public boolean b;

    public a() {
        this(tz.a);
    }

    public synchronized void a() throws InterruptedException {
        while (!this.b) {
            wait();
        }
    }

    public synchronized void b() {
        boolean z = false;
        while (!this.b) {
            try {
                wait();
            } catch (InterruptedException unused) {
                z = true;
            }
        }
        if (z) {
            Thread.currentThread().interrupt();
        }
    }

    public synchronized boolean c() {
        boolean z;
        z = this.b;
        this.b = false;
        return z;
    }

    public synchronized boolean d() {
        return this.b;
    }

    public synchronized boolean e() {
        if (this.b) {
            return false;
        }
        this.b = true;
        notifyAll();
        return true;
    }

    public a(tz tzVar) {
        this.a = tzVar;
    }
}
