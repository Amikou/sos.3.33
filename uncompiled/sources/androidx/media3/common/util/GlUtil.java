package androidx.media3.common.util;

import android.content.Context;
import android.opengl.EGL14;
import android.opengl.GLES20;
import android.opengl.GLU;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.nio.FloatBuffer;

/* loaded from: classes.dex */
public final class GlUtil {
    public static boolean a = false;

    /* loaded from: classes.dex */
    public static final class GlException extends RuntimeException {
        public GlException(String str) {
            super(str);
        }
    }

    public static void a(int i, int i2) {
        GLES20.glBindTexture(i, i2);
        c();
        GLES20.glTexParameteri(i, 10240, 9729);
        c();
        GLES20.glTexParameteri(i, 10241, 9729);
        c();
        GLES20.glTexParameteri(i, 10242, 33071);
        c();
        GLES20.glTexParameteri(i, 10243, 33071);
        c();
    }

    public static void b(boolean z, String str) {
        if (z) {
            return;
        }
        j(str);
    }

    public static void c() {
        int i = 0;
        while (true) {
            int glGetError = GLES20.glGetError();
            if (glGetError == 0) {
                break;
            }
            p12.c("GlUtil", "glError: " + GLU.gluErrorString(glGetError));
            i = glGetError;
        }
        if (i != 0) {
            j("glError: " + GLU.gluErrorString(i));
        }
    }

    public static FloatBuffer d(int i) {
        return ByteBuffer.allocateDirect(i * 4).order(ByteOrder.nativeOrder()).asFloatBuffer();
    }

    public static FloatBuffer e(float[] fArr) {
        return (FloatBuffer) d(fArr.length).put(fArr).flip();
    }

    public static int f() {
        int g = g();
        a(36197, g);
        return g;
    }

    public static int g() {
        b(!b.c(EGL14.eglGetCurrentContext(), EGL14.EGL_NO_CONTEXT), "No current context");
        int[] iArr = new int[1];
        GLES20.glGenTextures(1, iArr, 0);
        c();
        return iArr[0];
    }

    public static boolean h(Context context) {
        String eglQueryString;
        int i = b.a;
        if (i < 24) {
            return false;
        }
        if (i >= 26 || !("samsung".equals(b.c) || "XT1650".equals(b.d))) {
            return (i >= 26 || context.getPackageManager().hasSystemFeature("android.hardware.vr.high_performance")) && (eglQueryString = EGL14.eglQueryString(EGL14.eglGetDisplay(0), 12373)) != null && eglQueryString.contains("EGL_EXT_protected_content");
        }
        return false;
    }

    public static boolean i() {
        String eglQueryString;
        return b.a >= 17 && (eglQueryString = EGL14.eglQueryString(EGL14.eglGetDisplay(0), 12373)) != null && eglQueryString.contains("EGL_KHR_surfaceless_context");
    }

    public static void j(String str) {
        if (!a) {
            p12.c("GlUtil", str);
            return;
        }
        throw new GlException(str);
    }
}
