package androidx.media3.common;

import android.os.Parcel;
import android.os.Parcelable;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Comparator;
import java.util.List;
import java.util.UUID;

/* loaded from: classes.dex */
public final class DrmInitData implements Comparator<SchemeData>, Parcelable {
    public static final Parcelable.Creator<DrmInitData> CREATOR = new a();
    public final SchemeData[] a;
    public int f0;
    public final String g0;
    public final int h0;

    /* loaded from: classes.dex */
    public static final class SchemeData implements Parcelable {
        public static final Parcelable.Creator<SchemeData> CREATOR = new a();
        public int a;
        public final UUID f0;
        public final String g0;
        public final String h0;
        public final byte[] i0;

        /* loaded from: classes.dex */
        public class a implements Parcelable.Creator<SchemeData> {
            @Override // android.os.Parcelable.Creator
            /* renamed from: a */
            public SchemeData createFromParcel(Parcel parcel) {
                return new SchemeData(parcel);
            }

            @Override // android.os.Parcelable.Creator
            /* renamed from: b */
            public SchemeData[] newArray(int i) {
                return new SchemeData[i];
            }
        }

        public SchemeData(UUID uuid, String str, byte[] bArr) {
            this(uuid, null, str, bArr);
        }

        public boolean a(SchemeData schemeData) {
            return c() && !schemeData.c() && d(schemeData.f0);
        }

        public SchemeData b(byte[] bArr) {
            return new SchemeData(this.f0, this.g0, this.h0, bArr);
        }

        public boolean c() {
            return this.i0 != null;
        }

        public boolean d(UUID uuid) {
            return ft.a.equals(this.f0) || uuid.equals(this.f0);
        }

        @Override // android.os.Parcelable
        public int describeContents() {
            return 0;
        }

        public boolean equals(Object obj) {
            if (obj instanceof SchemeData) {
                if (obj == this) {
                    return true;
                }
                SchemeData schemeData = (SchemeData) obj;
                return androidx.media3.common.util.b.c(this.g0, schemeData.g0) && androidx.media3.common.util.b.c(this.h0, schemeData.h0) && androidx.media3.common.util.b.c(this.f0, schemeData.f0) && Arrays.equals(this.i0, schemeData.i0);
            }
            return false;
        }

        public int hashCode() {
            if (this.a == 0) {
                int hashCode = this.f0.hashCode() * 31;
                String str = this.g0;
                this.a = ((((hashCode + (str == null ? 0 : str.hashCode())) * 31) + this.h0.hashCode()) * 31) + Arrays.hashCode(this.i0);
            }
            return this.a;
        }

        @Override // android.os.Parcelable
        public void writeToParcel(Parcel parcel, int i) {
            parcel.writeLong(this.f0.getMostSignificantBits());
            parcel.writeLong(this.f0.getLeastSignificantBits());
            parcel.writeString(this.g0);
            parcel.writeString(this.h0);
            parcel.writeByteArray(this.i0);
        }

        public SchemeData(UUID uuid, String str, String str2, byte[] bArr) {
            this.f0 = (UUID) ii.e(uuid);
            this.g0 = str;
            this.h0 = (String) ii.e(str2);
            this.i0 = bArr;
        }

        public SchemeData(Parcel parcel) {
            this.f0 = new UUID(parcel.readLong(), parcel.readLong());
            this.g0 = parcel.readString();
            this.h0 = (String) androidx.media3.common.util.b.j(parcel.readString());
            this.i0 = parcel.createByteArray();
        }
    }

    /* loaded from: classes.dex */
    public class a implements Parcelable.Creator<DrmInitData> {
        @Override // android.os.Parcelable.Creator
        /* renamed from: a */
        public DrmInitData createFromParcel(Parcel parcel) {
            return new DrmInitData(parcel);
        }

        @Override // android.os.Parcelable.Creator
        /* renamed from: b */
        public DrmInitData[] newArray(int i) {
            return new DrmInitData[i];
        }
    }

    public DrmInitData(List<SchemeData> list) {
        this(null, false, (SchemeData[]) list.toArray(new SchemeData[0]));
    }

    public static boolean b(ArrayList<SchemeData> arrayList, int i, UUID uuid) {
        for (int i2 = 0; i2 < i; i2++) {
            if (arrayList.get(i2).f0.equals(uuid)) {
                return true;
            }
        }
        return false;
    }

    public static DrmInitData d(DrmInitData drmInitData, DrmInitData drmInitData2) {
        String str;
        SchemeData[] schemeDataArr;
        SchemeData[] schemeDataArr2;
        ArrayList arrayList = new ArrayList();
        if (drmInitData != null) {
            str = drmInitData.g0;
            for (SchemeData schemeData : drmInitData.a) {
                if (schemeData.c()) {
                    arrayList.add(schemeData);
                }
            }
        } else {
            str = null;
        }
        if (drmInitData2 != null) {
            if (str == null) {
                str = drmInitData2.g0;
            }
            int size = arrayList.size();
            for (SchemeData schemeData2 : drmInitData2.a) {
                if (schemeData2.c() && !b(arrayList, size, schemeData2.f0)) {
                    arrayList.add(schemeData2);
                }
            }
        }
        if (arrayList.isEmpty()) {
            return null;
        }
        return new DrmInitData(str, arrayList);
    }

    @Override // java.util.Comparator
    /* renamed from: a */
    public int compare(SchemeData schemeData, SchemeData schemeData2) {
        UUID uuid = ft.a;
        if (uuid.equals(schemeData.f0)) {
            return uuid.equals(schemeData2.f0) ? 0 : 1;
        }
        return schemeData.f0.compareTo(schemeData2.f0);
    }

    public DrmInitData c(String str) {
        return androidx.media3.common.util.b.c(this.g0, str) ? this : new DrmInitData(str, false, this.a);
    }

    @Override // android.os.Parcelable
    public int describeContents() {
        return 0;
    }

    public SchemeData e(int i) {
        return this.a[i];
    }

    @Override // java.util.Comparator
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null || DrmInitData.class != obj.getClass()) {
            return false;
        }
        DrmInitData drmInitData = (DrmInitData) obj;
        return androidx.media3.common.util.b.c(this.g0, drmInitData.g0) && Arrays.equals(this.a, drmInitData.a);
    }

    public int hashCode() {
        if (this.f0 == 0) {
            String str = this.g0;
            this.f0 = ((str == null ? 0 : str.hashCode()) * 31) + Arrays.hashCode(this.a);
        }
        return this.f0;
    }

    @Override // android.os.Parcelable
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeString(this.g0);
        parcel.writeTypedArray(this.a, 0);
    }

    public DrmInitData(String str, List<SchemeData> list) {
        this(str, false, (SchemeData[]) list.toArray(new SchemeData[0]));
    }

    public DrmInitData(SchemeData... schemeDataArr) {
        this((String) null, schemeDataArr);
    }

    public DrmInitData(String str, SchemeData... schemeDataArr) {
        this(str, true, schemeDataArr);
    }

    public DrmInitData(String str, boolean z, SchemeData... schemeDataArr) {
        this.g0 = str;
        schemeDataArr = z ? (SchemeData[]) schemeDataArr.clone() : schemeDataArr;
        this.a = schemeDataArr;
        this.h0 = schemeDataArr.length;
        Arrays.sort(schemeDataArr, this);
    }

    public DrmInitData(Parcel parcel) {
        this.g0 = parcel.readString();
        SchemeData[] schemeDataArr = (SchemeData[]) androidx.media3.common.util.b.j((SchemeData[]) parcel.createTypedArray(SchemeData.CREATOR));
        this.a = schemeDataArr;
        this.h0 = schemeDataArr.length;
    }
}
