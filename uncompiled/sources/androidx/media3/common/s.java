package androidx.media3.common;

import android.os.Bundle;
import androidx.media3.common.e;
import com.github.mikephil.charting.utils.Utils;

/* compiled from: StarRating.java */
/* loaded from: classes.dex */
public final class s extends r {
    public static final e.a<s> h0 = os3.a;
    public final int f0;
    public final float g0;

    public s(int i) {
        ii.b(i > 0, "maxStars must be a positive integer");
        this.f0 = i;
        this.g0 = -1.0f;
    }

    public static String c(int i) {
        return Integer.toString(i, 36);
    }

    public static s e(Bundle bundle) {
        ii.a(bundle.getInt(c(0), -1) == 2);
        int i = bundle.getInt(c(1), 5);
        float f = bundle.getFloat(c(2), -1.0f);
        if (f == -1.0f) {
            return new s(i);
        }
        return new s(i, f);
    }

    public boolean equals(Object obj) {
        if (obj instanceof s) {
            s sVar = (s) obj;
            return this.f0 == sVar.f0 && this.g0 == sVar.g0;
        }
        return false;
    }

    public int hashCode() {
        return ql2.b(Integer.valueOf(this.f0), Float.valueOf(this.g0));
    }

    @Override // androidx.media3.common.e
    public Bundle toBundle() {
        Bundle bundle = new Bundle();
        bundle.putInt(c(0), 2);
        bundle.putInt(c(1), this.f0);
        bundle.putFloat(c(2), this.g0);
        return bundle;
    }

    public s(int i, float f) {
        boolean z = true;
        ii.b(i > 0, "maxStars must be a positive integer");
        ii.b((f < Utils.FLOAT_EPSILON || f > ((float) i)) ? false : z, "starRating is out of range [0, maxStars]");
        this.f0 = i;
        this.g0 = f;
    }
}
