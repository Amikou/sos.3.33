package androidx.media3.common;

import java.io.IOException;

/* compiled from: DataReader.java */
/* loaded from: classes.dex */
public interface g {
    int read(byte[] bArr, int i, int i2) throws IOException;
}
