package androidx.media3.common;

import android.os.Bundle;
import androidx.media3.common.e;

/* compiled from: ThumbRating.java */
/* loaded from: classes.dex */
public final class t extends r {
    public static final e.a<t> h0 = q54.a;
    public final boolean f0;
    public final boolean g0;

    public t() {
        this.f0 = false;
        this.g0 = false;
    }

    public static String c(int i) {
        return Integer.toString(i, 36);
    }

    public static t e(Bundle bundle) {
        ii.a(bundle.getInt(c(0), -1) == 3);
        if (bundle.getBoolean(c(1), false)) {
            return new t(bundle.getBoolean(c(2), false));
        }
        return new t();
    }

    public boolean equals(Object obj) {
        if (obj instanceof t) {
            t tVar = (t) obj;
            return this.g0 == tVar.g0 && this.f0 == tVar.f0;
        }
        return false;
    }

    public int hashCode() {
        return ql2.b(Boolean.valueOf(this.f0), Boolean.valueOf(this.g0));
    }

    @Override // androidx.media3.common.e
    public Bundle toBundle() {
        Bundle bundle = new Bundle();
        bundle.putInt(c(0), 3);
        bundle.putBoolean(c(1), this.f0);
        bundle.putBoolean(c(2), this.g0);
        return bundle;
    }

    public t(boolean z) {
        this.f0 = true;
        this.g0 = z;
    }
}
