package androidx.media3.common;

import android.os.Bundle;
import androidx.media3.common.e;
import com.github.mikephil.charting.utils.Utils;

/* compiled from: PercentageRating.java */
/* loaded from: classes.dex */
public final class o extends r {
    public static final e.a<o> g0 = iq2.a;
    public final float f0;

    public o() {
        this.f0 = -1.0f;
    }

    public static String c(int i) {
        return Integer.toString(i, 36);
    }

    public static o e(Bundle bundle) {
        ii.a(bundle.getInt(c(0), -1) == 1);
        float f = bundle.getFloat(c(1), -1.0f);
        return f == -1.0f ? new o() : new o(f);
    }

    public boolean equals(Object obj) {
        return (obj instanceof o) && this.f0 == ((o) obj).f0;
    }

    public int hashCode() {
        return ql2.b(Float.valueOf(this.f0));
    }

    @Override // androidx.media3.common.e
    public Bundle toBundle() {
        Bundle bundle = new Bundle();
        bundle.putInt(c(0), 1);
        bundle.putFloat(c(1), this.f0);
        return bundle;
    }

    public o(float f) {
        ii.b(f >= Utils.FLOAT_EPSILON && f <= 100.0f, "percent must be in the range of [0, 100]");
        this.f0 = f;
    }
}
