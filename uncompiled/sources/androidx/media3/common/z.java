package androidx.media3.common;

import android.os.Bundle;

/* compiled from: VideoSize.java */
/* loaded from: classes.dex */
public final class z implements e {
    public static final z i0 = new z(0, 0);
    public final int a;
    public final int f0;
    public final int g0;
    public final float h0;

    public z(int i, int i2) {
        this(i, i2, 0, 1.0f);
    }

    public static String a(int i) {
        return Integer.toString(i, 36);
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj instanceof z) {
            z zVar = (z) obj;
            return this.a == zVar.a && this.f0 == zVar.f0 && this.g0 == zVar.g0 && this.h0 == zVar.h0;
        }
        return false;
    }

    public int hashCode() {
        return ((((((217 + this.a) * 31) + this.f0) * 31) + this.g0) * 31) + Float.floatToRawIntBits(this.h0);
    }

    @Override // androidx.media3.common.e
    public Bundle toBundle() {
        Bundle bundle = new Bundle();
        bundle.putInt(a(0), this.a);
        bundle.putInt(a(1), this.f0);
        bundle.putInt(a(2), this.g0);
        bundle.putFloat(a(3), this.h0);
        return bundle;
    }

    public z(int i, int i2, int i3, float f) {
        this.a = i;
        this.f0 = i2;
        this.g0 = i3;
        this.h0 = f;
    }
}
