package androidx.media3.decoder;

import java.nio.ByteBuffer;

/* loaded from: classes.dex */
public class DecoderInputBuffer extends sr {
    public final ra0 f0;
    public ByteBuffer g0;
    public boolean h0;
    public long i0;
    public ByteBuffer j0;
    public final int k0;
    public final int l0;

    /* loaded from: classes.dex */
    public static final class InsufficientCapacityException extends IllegalStateException {
        public final int currentCapacity;
        public final int requiredCapacity;

        public InsufficientCapacityException(int i, int i2) {
            super("Buffer too small (" + i + " < " + i2 + ")");
            this.currentCapacity = i;
            this.requiredCapacity = i2;
        }
    }

    static {
        f62.a("media3.decoder");
    }

    public DecoderInputBuffer(int i) {
        this(i, 0);
    }

    public static DecoderInputBuffer A() {
        return new DecoderInputBuffer(0);
    }

    public void B(int i) {
        ByteBuffer byteBuffer = this.j0;
        if (byteBuffer != null && byteBuffer.capacity() >= i) {
            this.j0.clear();
        } else {
            this.j0 = ByteBuffer.allocate(i);
        }
    }

    @Override // defpackage.sr
    public void h() {
        super.h();
        ByteBuffer byteBuffer = this.g0;
        if (byteBuffer != null) {
            byteBuffer.clear();
        }
        ByteBuffer byteBuffer2 = this.j0;
        if (byteBuffer2 != null) {
            byteBuffer2.clear();
        }
        this.h0 = false;
    }

    public final ByteBuffer u(int i) {
        int i2 = this.k0;
        if (i2 == 1) {
            return ByteBuffer.allocate(i);
        }
        if (i2 == 2) {
            return ByteBuffer.allocateDirect(i);
        }
        ByteBuffer byteBuffer = this.g0;
        throw new InsufficientCapacityException(byteBuffer == null ? 0 : byteBuffer.capacity(), i);
    }

    public void v(int i) {
        int i2 = i + this.l0;
        ByteBuffer byteBuffer = this.g0;
        if (byteBuffer == null) {
            this.g0 = u(i2);
            return;
        }
        int capacity = byteBuffer.capacity();
        int position = byteBuffer.position();
        int i3 = i2 + position;
        if (capacity >= i3) {
            this.g0 = byteBuffer;
            return;
        }
        ByteBuffer u = u(i3);
        u.order(byteBuffer.order());
        if (position > 0) {
            byteBuffer.flip();
            u.put(byteBuffer);
        }
        this.g0 = u;
    }

    public final void x() {
        ByteBuffer byteBuffer = this.g0;
        if (byteBuffer != null) {
            byteBuffer.flip();
        }
        ByteBuffer byteBuffer2 = this.j0;
        if (byteBuffer2 != null) {
            byteBuffer2.flip();
        }
    }

    public final boolean y() {
        return k(1073741824);
    }

    public DecoderInputBuffer(int i, int i2) {
        this.f0 = new ra0();
        this.k0 = i;
        this.l0 = i2;
    }
}
