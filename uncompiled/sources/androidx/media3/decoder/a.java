package androidx.media3.decoder;

import androidx.media3.decoder.DecoderException;

/* compiled from: Decoder.java */
/* loaded from: classes.dex */
public interface a<I, O, E extends DecoderException> {
    void a();

    O c() throws DecoderException;

    I d() throws DecoderException;

    void e(I i) throws DecoderException;

    void flush();
}
