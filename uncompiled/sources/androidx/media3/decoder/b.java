package androidx.media3.decoder;

import androidx.media3.decoder.DecoderException;
import androidx.media3.decoder.DecoderInputBuffer;
import defpackage.if0;
import java.util.ArrayDeque;

/* compiled from: SimpleDecoder.java */
/* loaded from: classes.dex */
public abstract class b<I extends DecoderInputBuffer, O extends if0, E extends DecoderException> implements androidx.media3.decoder.a<I, O, E> {
    public final Thread a;
    public final Object b = new Object();
    public final ArrayDeque<I> c = new ArrayDeque<>();
    public final ArrayDeque<O> d = new ArrayDeque<>();
    public final I[] e;
    public final O[] f;
    public int g;
    public int h;
    public I i;
    public E j;
    public boolean k;
    public boolean l;
    public int m;

    /* compiled from: SimpleDecoder.java */
    /* loaded from: classes.dex */
    public class a extends Thread {
        public a(String str) {
            super(str);
        }

        @Override // java.lang.Thread, java.lang.Runnable
        public void run() {
            b.this.u();
        }
    }

    public b(I[] iArr, O[] oArr) {
        this.e = iArr;
        this.g = iArr.length;
        for (int i = 0; i < this.g; i++) {
            this.e[i] = h();
        }
        this.f = oArr;
        this.h = oArr.length;
        for (int i2 = 0; i2 < this.h; i2++) {
            this.f[i2] = i();
        }
        a aVar = new a("ExoPlayer:SimpleDecoder");
        this.a = aVar;
        aVar.start();
    }

    @Override // androidx.media3.decoder.a
    public void a() {
        synchronized (this.b) {
            this.l = true;
            this.b.notify();
        }
        try {
            this.a.join();
        } catch (InterruptedException unused) {
            Thread.currentThread().interrupt();
        }
    }

    @Override // androidx.media3.decoder.a
    public final void flush() {
        synchronized (this.b) {
            this.k = true;
            this.m = 0;
            I i = this.i;
            if (i != null) {
                r(i);
                this.i = null;
            }
            while (!this.c.isEmpty()) {
                r(this.c.removeFirst());
            }
            while (!this.d.isEmpty()) {
                this.d.removeFirst().u();
            }
        }
    }

    public final boolean g() {
        return !this.c.isEmpty() && this.h > 0;
    }

    public abstract I h();

    public abstract O i();

    public abstract E j(Throwable th);

    public abstract E k(I i, O o, boolean z);

    public final boolean l() throws InterruptedException {
        E j;
        synchronized (this.b) {
            while (!this.l && !g()) {
                this.b.wait();
            }
            if (this.l) {
                return false;
            }
            I removeFirst = this.c.removeFirst();
            O[] oArr = this.f;
            int i = this.h - 1;
            this.h = i;
            O o = oArr[i];
            boolean z = this.k;
            this.k = false;
            if (removeFirst.p()) {
                o.g(4);
            } else {
                if (removeFirst.o()) {
                    o.g(Integer.MIN_VALUE);
                }
                if (removeFirst.r()) {
                    o.g(134217728);
                }
                try {
                    j = k(removeFirst, o, z);
                } catch (OutOfMemoryError e) {
                    j = j(e);
                } catch (RuntimeException e2) {
                    j = j(e2);
                }
                if (j != null) {
                    synchronized (this.b) {
                        this.j = j;
                    }
                    return false;
                }
            }
            synchronized (this.b) {
                if (this.k) {
                    o.u();
                } else if (o.o()) {
                    this.m++;
                    o.u();
                } else {
                    this.m = 0;
                    this.d.addLast(o);
                }
                r(removeFirst);
            }
            return true;
        }
    }

    @Override // androidx.media3.decoder.a
    /* renamed from: m */
    public final I d() throws DecoderException {
        I i;
        synchronized (this.b) {
            p();
            ii.g(this.i == null);
            int i2 = this.g;
            if (i2 == 0) {
                i = null;
            } else {
                I[] iArr = this.e;
                int i3 = i2 - 1;
                this.g = i3;
                i = iArr[i3];
            }
            this.i = i;
        }
        return i;
    }

    @Override // androidx.media3.decoder.a
    /* renamed from: n */
    public final O c() throws DecoderException {
        synchronized (this.b) {
            p();
            if (this.d.isEmpty()) {
                return null;
            }
            return this.d.removeFirst();
        }
    }

    public final void o() {
        if (g()) {
            this.b.notify();
        }
    }

    public final void p() throws DecoderException {
        E e = this.j;
        if (e != null) {
            throw e;
        }
    }

    @Override // androidx.media3.decoder.a
    /* renamed from: q */
    public final void e(I i) throws DecoderException {
        synchronized (this.b) {
            p();
            ii.a(i == this.i);
            this.c.addLast(i);
            o();
            this.i = null;
        }
    }

    public final void r(I i) {
        i.h();
        I[] iArr = this.e;
        int i2 = this.g;
        this.g = i2 + 1;
        iArr[i2] = i;
    }

    public void s(O o) {
        synchronized (this.b) {
            t(o);
            o();
        }
    }

    public final void t(O o) {
        o.h();
        O[] oArr = this.f;
        int i = this.h;
        this.h = i + 1;
        oArr[i] = o;
    }

    public final void u() {
        do {
            try {
            } catch (InterruptedException e) {
                throw new IllegalStateException(e);
            }
        } while (l());
    }

    public final void v(int i) {
        ii.g(this.g == this.e.length);
        for (I i2 : this.e) {
            i2.v(i);
        }
    }
}
