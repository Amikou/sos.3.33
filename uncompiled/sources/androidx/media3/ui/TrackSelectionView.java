package androidx.media3.ui;

import android.content.Context;
import android.content.res.TypedArray;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckedTextView;
import android.widget.LinearLayout;
import androidx.media3.common.j;
import androidx.media3.common.v;
import androidx.media3.common.w;
import androidx.media3.common.y;
import com.google.common.collect.ImmutableList;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/* loaded from: classes.dex */
public class TrackSelectionView extends LinearLayout {
    public final int a;
    public final LayoutInflater f0;
    public final CheckedTextView g0;
    public final CheckedTextView h0;
    public final b i0;
    public final List<y.a> j0;
    public final Map<v, w> k0;
    public boolean l0;
    public boolean m0;
    public d84 n0;
    public CheckedTextView[][] o0;
    public boolean p0;
    public Comparator<c> q0;
    public d r0;

    /* loaded from: classes.dex */
    public class b implements View.OnClickListener {
        public b() {
        }

        @Override // android.view.View.OnClickListener
        public void onClick(View view) {
            TrackSelectionView.this.c(view);
        }
    }

    /* loaded from: classes.dex */
    public static final class c {
        public final y.a a;
        public final int b;

        public c(y.a aVar, int i) {
            this.a = aVar;
            this.b = i;
        }

        public j a() {
            return this.a.c(this.b);
        }
    }

    /* loaded from: classes.dex */
    public interface d {
        void a(boolean z, Map<v, w> map);
    }

    public TrackSelectionView(Context context) {
        this(context, null);
    }

    public static Map<v, w> b(Map<v, w> map, List<y.a> list, boolean z) {
        HashMap hashMap = new HashMap();
        for (int i = 0; i < list.size(); i++) {
            w wVar = map.get(list.get(i).b());
            if (wVar != null && (z || hashMap.isEmpty())) {
                hashMap.put(wVar.a, wVar);
            }
        }
        return hashMap;
    }

    public final void c(View view) {
        if (view == this.g0) {
            e();
        } else if (view == this.h0) {
            d();
        } else {
            f(view);
        }
        i();
        d dVar = this.r0;
        if (dVar != null) {
            dVar.a(getIsDisabled(), getOverrides());
        }
    }

    public final void d() {
        this.p0 = false;
        this.k0.clear();
    }

    public final void e() {
        this.p0 = true;
        this.k0.clear();
    }

    public final void f(View view) {
        boolean z = false;
        this.p0 = false;
        c cVar = (c) ii.e(view.getTag());
        v b2 = cVar.a.b();
        int i = cVar.b;
        w wVar = this.k0.get(b2);
        if (wVar == null) {
            if (!this.m0 && this.k0.size() > 0) {
                this.k0.clear();
            }
            this.k0.put(b2, new w(b2, ImmutableList.of(Integer.valueOf(i))));
            return;
        }
        ArrayList arrayList = new ArrayList(wVar.f0);
        boolean isChecked = ((CheckedTextView) view).isChecked();
        boolean g = g(cVar.a);
        z = (g || h()) ? true : true;
        if (isChecked && z) {
            arrayList.remove(Integer.valueOf(i));
            if (arrayList.isEmpty()) {
                this.k0.remove(b2);
            } else {
                this.k0.put(b2, new w(b2, arrayList));
            }
        } else if (isChecked) {
        } else {
            if (g) {
                arrayList.add(Integer.valueOf(i));
                this.k0.put(b2, new w(b2, arrayList));
                return;
            }
            this.k0.put(b2, new w(b2, ImmutableList.of(Integer.valueOf(i))));
        }
    }

    public final boolean g(y.a aVar) {
        return this.l0 && aVar.e();
    }

    public boolean getIsDisabled() {
        return this.p0;
    }

    public Map<v, w> getOverrides() {
        return this.k0;
    }

    public final boolean h() {
        return this.m0 && this.j0.size() > 1;
    }

    public final void i() {
        this.g0.setChecked(this.p0);
        this.h0.setChecked(!this.p0 && this.k0.size() == 0);
        for (int i = 0; i < this.o0.length; i++) {
            w wVar = this.k0.get(this.j0.get(i).b());
            int i2 = 0;
            while (true) {
                CheckedTextView[][] checkedTextViewArr = this.o0;
                if (i2 < checkedTextViewArr[i].length) {
                    if (wVar != null) {
                        this.o0[i][i2].setChecked(wVar.f0.contains(Integer.valueOf(((c) ii.e(checkedTextViewArr[i][i2].getTag())).b)));
                    } else {
                        checkedTextViewArr[i][i2].setChecked(false);
                    }
                    i2++;
                }
            }
        }
    }

    public final void j() {
        for (int childCount = getChildCount() - 1; childCount >= 3; childCount--) {
            removeViewAt(childCount);
        }
        if (this.j0.isEmpty()) {
            this.g0.setEnabled(false);
            this.h0.setEnabled(false);
            return;
        }
        this.g0.setEnabled(true);
        this.h0.setEnabled(true);
        this.o0 = new CheckedTextView[this.j0.size()];
        boolean h = h();
        for (int i = 0; i < this.j0.size(); i++) {
            y.a aVar = this.j0.get(i);
            boolean g = g(aVar);
            CheckedTextView[][] checkedTextViewArr = this.o0;
            int i2 = aVar.a;
            checkedTextViewArr[i] = new CheckedTextView[i2];
            c[] cVarArr = new c[i2];
            for (int i3 = 0; i3 < aVar.a; i3++) {
                cVarArr[i3] = new c(aVar, i3);
            }
            Comparator<c> comparator = this.q0;
            if (comparator != null) {
                Arrays.sort(cVarArr, comparator);
            }
            for (int i4 = 0; i4 < i2; i4++) {
                if (i4 == 0) {
                    addView(this.f0.inflate(w03.exo_list_divider, (ViewGroup) this, false));
                }
                CheckedTextView checkedTextView = (CheckedTextView) this.f0.inflate((g || h) ? 17367056 : 17367055, (ViewGroup) this, false);
                checkedTextView.setBackgroundResource(this.a);
                checkedTextView.setText(this.n0.a(cVarArr[i4].a()));
                checkedTextView.setTag(cVarArr[i4]);
                if (aVar.h(i4)) {
                    checkedTextView.setFocusable(true);
                    checkedTextView.setOnClickListener(this.i0);
                } else {
                    checkedTextView.setFocusable(false);
                    checkedTextView.setEnabled(false);
                }
                this.o0[i][i4] = checkedTextView;
                addView(checkedTextView);
            }
        }
        i();
    }

    public void setAllowAdaptiveSelections(boolean z) {
        if (this.l0 != z) {
            this.l0 = z;
            j();
        }
    }

    public void setAllowMultipleOverrides(boolean z) {
        if (this.m0 != z) {
            this.m0 = z;
            if (!z && this.k0.size() > 1) {
                Map<v, w> b2 = b(this.k0, this.j0, false);
                this.k0.clear();
                this.k0.putAll(b2);
            }
            j();
        }
    }

    public void setShowDisableOption(boolean z) {
        this.g0.setVisibility(z ? 0 : 8);
    }

    public void setTrackNameProvider(d84 d84Var) {
        this.n0 = (d84) ii.e(d84Var);
        j();
    }

    public TrackSelectionView(Context context, AttributeSet attributeSet) {
        this(context, attributeSet, 0);
    }

    public TrackSelectionView(Context context, AttributeSet attributeSet, int i) {
        super(context, attributeSet, i);
        setOrientation(1);
        setSaveFromParentEnabled(false);
        TypedArray obtainStyledAttributes = context.getTheme().obtainStyledAttributes(new int[]{16843534});
        int resourceId = obtainStyledAttributes.getResourceId(0, 0);
        this.a = resourceId;
        obtainStyledAttributes.recycle();
        LayoutInflater from = LayoutInflater.from(context);
        this.f0 = from;
        b bVar = new b();
        this.i0 = bVar;
        this.n0 = new zk0(getResources());
        this.j0 = new ArrayList();
        this.k0 = new HashMap();
        CheckedTextView checkedTextView = (CheckedTextView) from.inflate(17367055, (ViewGroup) this, false);
        this.g0 = checkedTextView;
        checkedTextView.setBackgroundResource(resourceId);
        checkedTextView.setText(m13.exo_track_selection_none);
        checkedTextView.setEnabled(false);
        checkedTextView.setFocusable(true);
        checkedTextView.setOnClickListener(bVar);
        checkedTextView.setVisibility(8);
        addView(checkedTextView);
        addView(from.inflate(w03.exo_list_divider, (ViewGroup) this, false));
        CheckedTextView checkedTextView2 = (CheckedTextView) from.inflate(17367055, (ViewGroup) this, false);
        this.h0 = checkedTextView2;
        checkedTextView2.setBackgroundResource(resourceId);
        checkedTextView2.setText(m13.exo_track_selection_auto);
        checkedTextView2.setEnabled(false);
        checkedTextView2.setFocusable(true);
        checkedTextView2.setOnClickListener(bVar);
        addView(checkedTextView2);
    }
}
