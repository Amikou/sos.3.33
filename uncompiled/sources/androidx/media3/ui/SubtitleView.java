package androidx.media3.ui;

import android.content.Context;
import android.content.res.Resources;
import android.util.AttributeSet;
import android.util.TypedValue;
import android.view.View;
import android.view.accessibility.CaptioningManager;
import android.widget.FrameLayout;
import defpackage.kb0;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/* loaded from: classes.dex */
public final class SubtitleView extends FrameLayout {
    public List<kb0> a;
    public zv f0;
    public int g0;
    public float h0;
    public float i0;
    public boolean j0;
    public boolean k0;
    public int l0;
    public a m0;
    public View n0;

    /* loaded from: classes.dex */
    public interface a {
        void a(List<kb0> list, zv zvVar, float f, int i, float f2);
    }

    public SubtitleView(Context context) {
        this(context, null);
    }

    private List<kb0> getCuesWithStylingPreferencesApplied() {
        if (this.j0 && this.k0) {
            return this.a;
        }
        ArrayList arrayList = new ArrayList(this.a.size());
        for (int i = 0; i < this.a.size(); i++) {
            arrayList.add(a(this.a.get(i)));
        }
        return arrayList;
    }

    private float getUserCaptionFontScale() {
        CaptioningManager captioningManager;
        if (androidx.media3.common.util.b.a < 19 || isInEditMode() || (captioningManager = (CaptioningManager) getContext().getSystemService("captioning")) == null || !captioningManager.isEnabled()) {
            return 1.0f;
        }
        return captioningManager.getFontScale();
    }

    private zv getUserCaptionStyle() {
        if (androidx.media3.common.util.b.a >= 19 && !isInEditMode()) {
            CaptioningManager captioningManager = (CaptioningManager) getContext().getSystemService("captioning");
            if (captioningManager != null && captioningManager.isEnabled()) {
                return zv.a(captioningManager.getUserStyle());
            }
            return zv.g;
        }
        return zv.g;
    }

    private <T extends View & a> void setView(T t) {
        removeView(this.n0);
        View view = this.n0;
        if (view instanceof WebViewSubtitleOutput) {
            ((WebViewSubtitleOutput) view).g();
        }
        this.n0 = t;
        this.m0 = t;
        addView(t);
    }

    public final kb0 a(kb0 kb0Var) {
        kb0.b b = kb0Var.b();
        if (!this.j0) {
            yv3.e(b);
        } else if (!this.k0) {
            yv3.f(b);
        }
        return b.a();
    }

    public final void b(int i, float f) {
        this.g0 = i;
        this.h0 = f;
        c();
    }

    public final void c() {
        this.m0.a(getCuesWithStylingPreferencesApplied(), this.f0, this.h0, this.g0, this.i0);
    }

    public void setApplyEmbeddedFontSizes(boolean z) {
        this.k0 = z;
        c();
    }

    public void setApplyEmbeddedStyles(boolean z) {
        this.j0 = z;
        c();
    }

    public void setBottomPaddingFraction(float f) {
        this.i0 = f;
        c();
    }

    public void setCues(List<kb0> list) {
        if (list == null) {
            list = Collections.emptyList();
        }
        this.a = list;
        c();
    }

    public void setFixedTextSize(int i, float f) {
        Resources resources;
        Context context = getContext();
        if (context == null) {
            resources = Resources.getSystem();
        } else {
            resources = context.getResources();
        }
        b(2, TypedValue.applyDimension(i, f, resources.getDisplayMetrics()));
    }

    public void setFractionalTextSize(float f) {
        setFractionalTextSize(f, false);
    }

    public void setStyle(zv zvVar) {
        this.f0 = zvVar;
        c();
    }

    public void setUserDefaultStyle() {
        setStyle(getUserCaptionStyle());
    }

    public void setUserDefaultTextSize() {
        setFractionalTextSize(getUserCaptionFontScale() * 0.0533f);
    }

    public void setViewType(int i) {
        if (this.l0 == i) {
            return;
        }
        if (i == 1) {
            setView(new CanvasSubtitleOutput(getContext()));
        } else if (i == 2) {
            setView(new WebViewSubtitleOutput(getContext()));
        } else {
            throw new IllegalArgumentException();
        }
        this.l0 = i;
    }

    public SubtitleView(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        this.a = Collections.emptyList();
        this.f0 = zv.g;
        this.g0 = 0;
        this.h0 = 0.0533f;
        this.i0 = 0.08f;
        this.j0 = true;
        this.k0 = true;
        CanvasSubtitleOutput canvasSubtitleOutput = new CanvasSubtitleOutput(context);
        this.m0 = canvasSubtitleOutput;
        this.n0 = canvasSubtitleOutput;
        addView(canvasSubtitleOutput);
        this.l0 = 1;
    }

    public void setFractionalTextSize(float f, boolean z) {
        b(z ? 1 : 0, f);
    }
}
