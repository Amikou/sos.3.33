package androidx.media3.ui;

import android.content.Context;
import android.content.res.TypedArray;
import android.util.AttributeSet;
import android.view.View;
import android.widget.FrameLayout;
import com.github.mikephil.charting.utils.Utils;

/* loaded from: classes.dex */
public final class AspectRatioFrameLayout extends FrameLayout {
    public final c a;
    public b f0;
    public float g0;
    public int h0;

    /* loaded from: classes.dex */
    public interface b {
        void a(float f, float f2, boolean z);
    }

    /* loaded from: classes.dex */
    public final class c implements Runnable {
        public float a;
        public float f0;
        public boolean g0;
        public boolean h0;

        public c() {
        }

        public void a(float f, float f2, boolean z) {
            this.a = f;
            this.f0 = f2;
            this.g0 = z;
            if (this.h0) {
                return;
            }
            this.h0 = true;
            AspectRatioFrameLayout.this.post(this);
        }

        @Override // java.lang.Runnable
        public void run() {
            this.h0 = false;
            if (AspectRatioFrameLayout.this.f0 == null) {
                return;
            }
            AspectRatioFrameLayout.this.f0.a(this.a, this.f0, this.g0);
        }
    }

    public AspectRatioFrameLayout(Context context) {
        this(context, null);
    }

    public int getResizeMode() {
        return this.h0;
    }

    @Override // android.widget.FrameLayout, android.view.View
    public void onMeasure(int i, int i2) {
        float f;
        float f2;
        super.onMeasure(i, i2);
        if (this.g0 <= Utils.FLOAT_EPSILON) {
            return;
        }
        int measuredWidth = getMeasuredWidth();
        int measuredHeight = getMeasuredHeight();
        float f3 = measuredWidth;
        float f4 = measuredHeight;
        float f5 = f3 / f4;
        float f6 = (this.g0 / f5) - 1.0f;
        if (Math.abs(f6) <= 0.01f) {
            this.a.a(this.g0, f5, false);
            return;
        }
        int i3 = this.h0;
        if (i3 != 0) {
            if (i3 != 1) {
                if (i3 == 2) {
                    f = this.g0;
                } else if (i3 == 4) {
                    if (f6 > Utils.FLOAT_EPSILON) {
                        f = this.g0;
                    } else {
                        f2 = this.g0;
                    }
                }
                measuredWidth = (int) (f4 * f);
            } else {
                f2 = this.g0;
            }
            measuredHeight = (int) (f3 / f2);
        } else if (f6 > Utils.FLOAT_EPSILON) {
            f2 = this.g0;
            measuredHeight = (int) (f3 / f2);
        } else {
            f = this.g0;
            measuredWidth = (int) (f4 * f);
        }
        this.a.a(this.g0, f5, true);
        super.onMeasure(View.MeasureSpec.makeMeasureSpec(measuredWidth, 1073741824), View.MeasureSpec.makeMeasureSpec(measuredHeight, 1073741824));
    }

    public void setAspectRatio(float f) {
        if (this.g0 != f) {
            this.g0 = f;
            requestLayout();
        }
    }

    public void setAspectRatioListener(b bVar) {
        this.f0 = bVar;
    }

    public void setResizeMode(int i) {
        if (this.h0 != i) {
            this.h0 = i;
            requestLayout();
        }
    }

    public AspectRatioFrameLayout(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        this.h0 = 0;
        if (attributeSet != null) {
            TypedArray obtainStyledAttributes = context.getTheme().obtainStyledAttributes(attributeSet, b33.AspectRatioFrameLayout, 0, 0);
            try {
                this.h0 = obtainStyledAttributes.getInt(b33.AspectRatioFrameLayout_resize_mode, 0);
            } finally {
                obtainStyledAttributes.recycle();
            }
        }
        this.a = new c();
    }
}
